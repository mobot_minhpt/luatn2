require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CProfessionTransferMgr = import "L10.Game.CProfessionTransferMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumClassTransferCheckKey = import "L10.Game.EnumClassTransferCheckKey"
local UISprite = import "UISprite"
local Color = import "UnityEngine.Color"
local Constants = import "L10.Game.Constants"
local LocalString = import "LocalString"

LuaProfessionTransferRequirementWnd = class()
RegistClassMember(LuaProfessionTransferRequirementWnd,"closeBtn")
RegistClassMember(LuaProfessionTransferRequirementWnd,"scrollView")
RegistClassMember(LuaProfessionTransferRequirementWnd,"table")
RegistClassMember(LuaProfessionTransferRequirementWnd,"itemTpl")
RegistClassMember(LuaProfessionTransferRequirementWnd,"nextStepBtn")

function LuaProfessionTransferRequirementWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaProfessionTransferRequirementWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
	self.scrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
	self.table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
	self.itemTpl = self.transform:Find("Anchor/ScrollView/ItemTemplate").gameObject
	self.nextStepBtn = self.transform:Find("Anchor/NextStepBtn"):GetComponent(typeof(CButton))
	
	self.itemTpl:SetActive(false)
	
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.nextStepBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnNextStepButtonClick(go) end), false)
	
	self.nextStepBtn.Enabled = false
	
	local allSatisfied = true
	--前7项与EnumClassTransferCheckKey定义中的前七项目对应
	--其后都归为其他
	for i=1,8 do
		local instance = CUICommonDef.AddChild(self.table.gameObject, self.itemTpl)
		instance:SetActive(true)
		local textLabel = instance.transform:Find("TextLabel"):GetComponent(typeof(UILabel))
		local wrongIcon = instance.transform:Find("SatisfyIcon_Wrong")
		local rightIcon = instance.transform:Find("SatisfyIcon_Right")

		local satisfied = true
		local text = ""
		if i<8 then
			satisfied = CProfessionTransferMgr.Inst:IsRequirementSatisfied(i)
			text = CProfessionTransferMgr.Inst:GetRequirementText(i)
		else
			local notFitText = nil
			for j=EnumToInt(EnumClassTransferCheckKey.eOtherStart),EnumToInt(EnumClassTransferCheckKey.eOtherEnd) do
				if not CProfessionTransferMgr.Inst:IsRequirementSatisfied(j) then
					notFitText = CProfessionTransferMgr.Inst:GetRequirementText(j)
					break 
				end
			end

			if notFitText then
				satisfied = false
				text = SafeStringFormat3(LocalString.GetString("其他(%s)"), notFitText)
			else
				satisfied = true
				text = LocalString.GetString("其他")
			end
		end
		
		textLabel.text = text
		rightIcon.gameObject:SetActive(satisfied)
		wrongIcon.gameObject:SetActive(not satisfied)
		if satisfied then
			textLabel.color = Color.white
		else
			textLabel.color = Color.red
		end
		
		if not satisfied then
			allSatisfied = false
		end
	end
	self.table:Reposition()
	self.scrollView:ResetPosition()
	self.nextStepBtn.Enabled = allSatisfied
end

function LuaProfessionTransferRequirementWnd:OnNextStepButtonClick(go)
	 CProfessionTransferMgr.Inst:ShowProfessionTransferRuleWnd()
     self:Close()
end
return LuaProfessionTransferRequirementWnd