-- Auto Generated!!
local CIMMgr = import "L10.Game.CIMMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZhongQiuSelectFriendItem = import "L10.UI.CZhongQiuSelectFriendItem"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local L10 = import "L10"
local LocalString = import "LocalString"
local Object = import "System.Object"
CZhongQiuSelectFriendItem.m_Init_CS2LuaHook = function (this, info)
    this.PlayerId = info.ID
    this.PlayerName = info.Name
    this.nameLabel.text = info.Name
    local portraitName = L10.UI.CUICommonDef.GetPortraitName(info.Class, info.Gender, info.Expression)
    this.portrait:LoadNPCPortrait(portraitName, false)
    this.PortraitName = portraitName
    this.levelLabel.text = System.String.Format(LocalString.GetString("{0}级"), info.Level)
    if CIMMgr.Inst:IsOnline(this.PlayerId) then
        CUICommonDef.SetActive(this.headGo, true, true)
    else
        CUICommonDef.SetActive(this.headGo, false, true)
    end

    -- 明星邀请赛复用
    if CLuaStarBiwuMgr.m_bSelectFriend then
      this.transform:Find("BgSprite/OpButton/Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("邀请")
    end
    --告白烟花复用
    if LuaFireWorkPartyMgr.SelectFriendOpenForFP then
        this.transform:Find("BgSprite/OpButton/Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("选择")
    end
end
CZhongQiuSelectFriendItem.m_OnOpButtonClick_CS2LuaHook = function (this, go)
    if this.OnOpButtonClickDelegate ~= nil then
        GenericDelegateInvoke(this.OnOpButtonClickDelegate, Table2ArrayWithCount({this.PlayerId}, 1, MakeArrayClass(Object)))
    end

    if LuaFireWorkPartyMgr.SelectFriendOpenForFP then--告白烟花复用
        g_ScriptEvent:BroadcastInLua("SelectFriendToSendLoveFirework",this.PlayerId, this.PlayerName,this.PortraitName)
    else
        EventManager.BroadcastInternalForLua(EnumEventType.ZhongQiuSelectPlayer, {this.PlayerId, this.PlayerName, this.PortraitName})
    end
    
    CUIManager.CloseUI("ZhongQiuSelectFriendWnd")
end
