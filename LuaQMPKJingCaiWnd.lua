local Shop_PlayScore=import "L10.Game.Shop_PlayScore"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
local EnumQMPKZhanduiMemberWndType = import "L10.Game.EnumQMPKZhanduiMemberWndType"
local CNPCShopInfoMgr=import "L10.UI.CNPCShopInfoMgr"
local QnRadioBox=import "L10.UI.QnRadioBox"
local QnTabView=import "L10.UI.QnTabView"
local QnButton=import "L10.UI.QnButton"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaQMPKJingCaiWnd = class()
RegistClassMember(CLuaQMPKJingCaiWnd,"m_RadioBox")
RegistClassMember(CLuaQMPKJingCaiWnd,"m_JingCaiItemTemplate")
RegistClassMember(CLuaQMPKJingCaiWnd,"m_JingCaiGrid")

RegistClassMember(CLuaQMPKJingCaiWnd,"m_JiLuItemTemplate")
RegistClassMember(CLuaQMPKJingCaiWnd,"m_JiLuGrid")

RegistClassMember(CLuaQMPKJingCaiWnd,"m_ScoreLabel")
RegistClassMember(CLuaQMPKJingCaiWnd,"m_DaiBiLabel")
RegistClassMember(CLuaQMPKJingCaiWnd,"m_TimeLabel")
RegistClassMember(CLuaQMPKJingCaiWnd,"m_JingCaiTipLabel")
RegistClassMember(CLuaQMPKJingCaiWnd,"m_JiLuTipLabel")


function CLuaQMPKJingCaiWnd:Init()

    local jingcai=FindChild(self.transform,"JingCai")
    self.m_TimeLabel=FindChild(jingcai,"TimeLabel"):GetComponent(typeof(UILabel))
    self.m_JingCaiTipLabel=FindChild(jingcai,"TipLabel"):GetComponent(typeof(UILabel))
    self.m_JingCaiTipLabel.gameObject:SetActive(false)
    -- self.m_JingCaiTipLabel.text=nil

    self.m_RadioBox=FindChild(jingcai,"RadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_RadioBox.OnSelect=DelegateFactory.Action_QnButton_int(function(btn,index)
        if index==0 then
            if CLuaQMPKMgr.m_JingCaiListInfo[4] then
                self:InitJingCaiList(CLuaQMPKMgr.m_JingCaiListInfo[4])
            else
                Gac2Gas.RequestQmpkJingCaiList(4)--淘汰赛4、总决赛5
            end
            self.m_TimeLabel.text=QuanMinPK_Setting.GetData().JingCaiTaoTaiSaiTime
        elseif index==1 then
            if CLuaQMPKMgr.m_JingCaiListInfo[5] then
                self:InitJingCaiList(CLuaQMPKMgr.m_JingCaiListInfo[5])
            else
                Gac2Gas.RequestQmpkJingCaiList(5)--淘汰赛4、总决赛5
            end
            self.m_TimeLabel.text=QuanMinPK_Setting.GetData().JingCaiZongJueSaiTime
        end
    end)
    local index = CLuaQMPKMgr.m_JingCaiPlayStage == 5 and 1 or 0
    self.m_RadioBox:ChangeTo(index,true)
    self.m_JingCaiItemTemplate=FindChild(jingcai,"ItemTemplate").gameObject
    self.m_JingCaiItemTemplate:SetActive(false)

    self.m_JingCaiGrid=FindChild(jingcai,"Grid")


    local tabView=FindChild(self.transform,"QnTabView"):GetComponent(typeof(QnTabView))
    tabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function(btn,index)
        if index==1 then
            --请求竞猜记录
            -- if CLuaQMPKMgr.m_JingCaiRecord then
            --     self:OnReplyQmpkJingCaiRecord(CLuaQMPKMgr.m_JingCaiRecord)
            -- else
                Gac2Gas.RequestQmpkJingCaiRecord()
            -- end
        end
    end)

    local jilu=FindChild(self.transform,"JiLu")
    self.m_JiLuTipLabel=FindChild(jilu,"TipLabel"):GetComponent(typeof(UILabel))
    self.m_JiLuTipLabel.gameObject:SetActive(false)
    self.m_JiLuItemTemplate=FindChild(jilu,"ItemTemplate").gameObject
    self.m_JiLuItemTemplate:SetActive(false)
    self.m_JiLuGrid=FindChild(jilu,"Grid")

    local openShopButton=FindChild(jilu,"OpenShopButton").gameObject
    UIEventListener.Get(openShopButton).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OpenShop()
    end)

    local tipButton=FindChild(jilu,"TipButton").gameObject
    UIEventListener.Get(tipButton).onClick=DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("QMPK_JingCai_Tip")
    end)

    self.m_ScoreLabel=FindChild(jilu,"ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_ScoreLabel.text=""
    self.m_DaiBiLabel=FindChild(jilu,"DaiBiLabel"):GetComponent(typeof(UILabel))
    self.m_DaiBiLabel.text="0"

    if CClientMainPlayer.Inst then
        local data=CClientMainPlayer.Inst.PlayProp.TempPlayData
        local tempData=CommonDefs.DictGetValue(data,typeof(UInt32),89)
        if tempData then
            local t=MsgPackImpl.unpack(tempData.Data.Data)
            self.m_DaiBiLabel.text=tostring(t[1])
        end
    end

    self:OnMainPlayerPlayPropUpdate()
end
function CLuaQMPKJingCaiWnd:OpenShop()
    local data=Shop_PlayScore.GetData(34000058)
    if data then
        CNPCShopInfoMgr.LastOpenShopName=data.Name
        CLuaNPCShopInfoMgr.ShowScoreShopById(34000058)
    end
end


function CLuaQMPKJingCaiWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyQmpkJingCaiList", self, "OnReplyQmpkJingCaiList")
    g_ScriptEvent:AddListener("ReplyQmpkJingCaiRecord", self, "OnReplyQmpkJingCaiRecord")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "OnMainPlayerPlayPropUpdate")

    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")

end
function CLuaQMPKJingCaiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyQmpkJingCaiList", self, "OnReplyQmpkJingCaiList")
    g_ScriptEvent:RemoveListener("ReplyQmpkJingCaiRecord", self, "OnReplyQmpkJingCaiRecord")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "OnMainPlayerPlayPropUpdate")

    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
end
--代币刷新
function CLuaQMPKJingCaiWnd:OnUpdateTempPlayDataWithKey(args)
    if CClientMainPlayer.Inst then
        local data=CClientMainPlayer.Inst.PlayProp.TempPlayData
        local tempData=CommonDefs.DictGetValue(data,typeof(UInt32),89)
        if tempData then
            local t=MsgPackImpl.unpack(tempData.Data.Data)
            self.m_DaiBiLabel.text=tostring(t[1])
            CLuaQMPKMgr.m_DaiBi = t[1]
        end
    end
end
function CLuaQMPKJingCaiWnd:OnMainPlayerPlayPropUpdate()
    if CClientMainPlayer.Inst then
        local score = 0
        local info = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayScore, EnumTempPlayScoreKey_lua.QmpkJingCai)
        if info then
            if info.ExpiredTime and info.ExpiredTime < CServerTimeMgr.Inst.timeStamp then
                score = 0 
            else 
                score = info.Score
            end
        end
        self.m_ScoreLabel.text = tostring(score)
    end
end

function CLuaQMPKJingCaiWnd:OnDestroy()
    CLuaQMPKMgr.m_JingCaiListInfo={}
end
function CLuaQMPKJingCaiWnd:OnReplyQmpkJingCaiList(playStage,list)
    self:InitJingCaiList(list)
end
function CLuaQMPKJingCaiWnd:InitJingCaiList(list)
    CUICommonDef.ClearTransform(self.m_JingCaiGrid)
    for i,v in ipairs(list) do
        -- {playIndex=playIndex,zhanduiId1=zhanduiId1,zhanduiName1=zhanduiName1,zhanduiId2=zhanduiName2}
        local go=NGUITools.AddChild(self.m_JingCaiGrid.gameObject,self.m_JingCaiItemTemplate)
        go:SetActive(true)
        local matchNameLabel=FindChild(go.transform,"MatchNameLabel"):GetComponent(typeof(UILabel))
        matchNameLabel.text=CLuaQMPKMgr.GetMatchName(v.playIndex)
        local nameLabel1=FindChild(go.transform,"NameLabel1"):GetComponent(typeof(UILabel))
        nameLabel1.text=v.zhanduiName1
        local nameLabel2=FindChild(go.transform,"NameLabel2"):GetComponent(typeof(UILabel))
        nameLabel2.text=v.zhanduiName2
        local btn=FindChild(go.transform,"JingCaiButton"):GetComponent(typeof(QnButton))
        local playIndex=v.playIndex

        local zhandui1=FindChild(go.transform,"Zhandui1").gameObject
        local zhandui2=FindChild(go.transform,"Zhandui2").gameObject
        local Desc2=FindChild(go.transform, "Desc2").gameObject
        UIEventListener.Get(zhandui1).onClick=DelegateFactory.VoidDelegate(function(p)
            CQuanMinPKMgr.Inst:ShowZhanDuiMemberWnd(0, EnumQMPKZhanduiMemberWndType.eOneZhandui, v.zhanduiId1,true)
        end)
        UIEventListener.Get(zhandui2).onClick=DelegateFactory.VoidDelegate(function(p)
            CQuanMinPKMgr.Inst:ShowZhanDuiMemberWnd(0, EnumQMPKZhanduiMemberWndType.eOneZhandui, v.zhanduiId2,true)
        end)

        btn.OnClick=DelegateFactory.Action_QnButton(function(qnbtn)
            CLuaQMPKMgr.m_PlayIndex=playIndex
            CUIManager.ShowUI(CLuaUIResources.QMPKJingCaiContentWnd)
        end)

        if v.zhanduiId2 == -1 then
            zhandui2.gameObject:SetActive(false)
            Desc2.gameObject:SetActive(true)
        else
            zhandui2.gameObject:SetActive(true)
            Desc2.gameObject:SetActive(false)
        end
    end
    if #list>0 then
        self.m_JingCaiTipLabel.gameObject:SetActive(false)
    else
        self.m_JingCaiTipLabel.gameObject:SetActive(true)
    end

    self.m_JingCaiGrid:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaQMPKJingCaiWnd:OnReplyQmpkJingCaiRecord(list)
    CUICommonDef.ClearTransform(self.m_JiLuGrid)
    for i,v in ipairs(list) do

        local go=NGUITools.AddChild(self.m_JiLuGrid.gameObject,self.m_JiLuItemTemplate)
        go:SetActive(true)
        local bgSprite=go:GetComponent(typeof(UISprite))
        if i%2==0 then
            bgSprite.spriteName="common_textbg_02_dark"
        else
            bgSprite.spriteName="common_textbg_02_light"
        end
        local roundLabel=FindChild(go.transform,"RoundLabel"):GetComponent(typeof(UILabel))
        roundLabel.text=CLuaQMPKMgr.GetMatchName(v.playIndex)
        local zhanduiLabel=FindChild(go.transform,"ZhanDuiLabel"):GetComponent(typeof(UILabel))

        if v.zhanduiId2 == -1 then
            zhanduiLabel.text=SafeStringFormat3( "%s vs [c][FFFFFF]%s[-][/c]",v.zhanduiName1, LocalString.GetString("暂未公布"))
        else
            zhanduiLabel.text=SafeStringFormat3( "%s vs %s",v.zhanduiName1, v.zhanduiName2)
        end

        local scoreLabel=FindChild(go.transform,"ScoreLabel"):GetComponent(typeof(UILabel))
        scoreLabel.text=v.hasResult>0 and tostring(v.score) or LocalString.GetString("待定")

        local xiazhuLabel=FindChild(go.transform,"XiaZhuLabel"):GetComponent(typeof(UILabel))
        xiazhuLabel.text=tostring(v.touzhuNum)..LocalString.GetString("万")

        local topicLabel=FindChild(go.transform,"TopicLabel"):GetComponent(typeof(UILabel))
        local jingcaiData=QuanMinPK_JingCai.GetData(v.jingcaiIndex)
        topicLabel.text=jingcaiData.Topic

        local choiceLabel=FindChild(go.transform,"ChoiceLabel"):GetComponent(typeof(UILabel))
        choiceLabel.text = v.selectIndex==1 and jingcaiData.Choice1 or jingcaiData.Choice2

        local resultSprite=FindChild(go.transform,"ResultSprite"):GetComponent(typeof(UISprite))
        if  v.lunkong>0 then--如果轮空
            resultSprite.spriteName=nil
        else
            resultSprite.enabled=v.hasResult>0
            if v.score==0 then
                resultSprite.spriteName="common_fight_result_lose"
            else
                resultSprite.spriteName="common_fight_result_win"
            end
        end

    end
    self.m_JiLuGrid:GetComponent(typeof(UIGrid)):Reposition()
    if #list>0 then
        self.m_JiLuTipLabel.gameObject:SetActive(false)
    else
        self.m_JiLuTipLabel.gameObject:SetActive(true)
    end
end

return CLuaQMPKJingCaiWnd
