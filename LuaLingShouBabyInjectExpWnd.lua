require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local Gac2Gas=import "L10.Game.Gac2Gas"

CLuaLingShouBabyInjectExpWnd=class()
RegistClassMember(CLuaLingShouBabyInjectExpWnd,"m_NeedExpLabel")
RegistClassMember(CLuaLingShouBabyInjectExpWnd,"m_ParentExpLabel")

function CLuaLingShouBabyInjectExpWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"OkButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --注入经验
        Gac2Gas.RequestLingShouBabyTakeParentExp(CLingShouMgr.Inst.selectedLingShou)
    end)
    local g = LuaGameObject.GetChildNoGC(self.transform,"CancleButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        --注入经验
        CUIManager.CloseUI(CLuaUIResources.LingShouBabyInjectExpWnd)
    end)

    LuaGameObject.GetChildNoGC(self.transform,"TitleLable").label.text=LocalString.GetString("注入父代经验快速升级")
    LuaGameObject.GetChildNoGC(self.transform,"Label1").label.text=LocalString.GetString("宝宝升级所需经验")
    LuaGameObject.GetChildNoGC(self.transform,"Label2").label.text=LocalString.GetString("父代当前等级经验")

    self.m_NeedExpLabel=LuaGameObject.GetChildNoGC(self.transform,"NeedExpLabel").label
    self.m_NeedExpLabel.text="0"
    self.m_ParentExpLabel=LuaGameObject.GetChildNoGC(self.transform,"ParentExpLabel").label
    self.m_ParentExpLabel.text="0"

    self:InitData()
end
function CLuaLingShouBabyInjectExpWnd:InitData()
    local lingShouId=CLingShouMgr.Inst.selectedLingShou
    if lingShouId~=nil or lingShouId~="" then
        local details=CLingShouMgr.Inst:GetLingShouDetails(lingShouId)
        if details~=nil then
            self.m_ParentExpLabel.text=tostring(details.data.Exp)
            local babyInfo = details.data.Props.Baby
            if babyInfo.BornTime>0 then
                local needExp=LingShouBaby_BabyExp.Exists(babyInfo.Level) and LingShouBaby_BabyExp.GetData(babyInfo.Level).ExpFull or 0
                -- print("exp",needExp,babyInfo.Exp)
                needExp=math.max(needExp-babyInfo.Exp,0)
                self.m_NeedExpLabel.text=tostring(needExp)
            end
        end
    end
end

function CLuaLingShouBabyInjectExpWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
    g_ScriptEvent:AddListener("UpdateLingShouExp", self, "OnUpdateLingShouExp")
end

function CLuaLingShouBabyInjectExpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
    g_ScriptEvent:RemoveListener("UpdateLingShouExp", self, "OnUpdateLingShouExp")
end
function CLuaLingShouBabyInjectExpWnd:OnUpdateLingShouExp(args)
    local lingshouId=args[0]
    if CLingShouMgr.Inst.selectedLingShou==lingshouId then
        self:InitData()
    end
end
function CLuaLingShouBabyInjectExpWnd:OnUpdateLingShouBabyInfo(args)
    local lingshouId=args[0]
    if CLingShouMgr.Inst.selectedLingShou==lingshouId then
        --升级所需经验
        -- print("CLuaLingShouBabyInjectExpWnd:OnUpdateLingShouBabyInfo")
        self:InitData()
    end
end

return CLuaLingShouBabyInjectExpWnd