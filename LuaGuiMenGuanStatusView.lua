local UIProgressBar=import "UIProgressBar"


CLuaGuiMenGuanStatusView = class()
RegistClassMember(CLuaGuiMenGuanStatusView,"m_BossCountLabel")
RegistClassMember(CLuaGuiMenGuanStatusView,"m_TimeBar")
RegistClassMember(CLuaGuiMenGuanStatusView,"m_TimeLabel")


function CLuaGuiMenGuanStatusView:Awake()
    self.m_BossCountLabel = FindChild(self.transform,"BossCountLabel"):GetComponent(typeof(UILabel))
    self.m_BossCountLabel.text = LocalString.GetString("BOSS 0/0")

    self.m_TimeLabel = FindChild(self.transform,"TimeLabel"):GetComponent(typeof(UILabel))
    self.m_TimeLabel.text = nil

    self.m_TimeBar = FindChild(self.transform,"TimeBar"):GetComponent(typeof(UIProgressBar))
    self.m_TimeBar.value = 0

    CommonDefs.AddOnClickListener(self.gameObject, DelegateFactory.Action_GameObject(function(go)
        CUIManager.ShowUI(CUIResources.GuiMenGuanPlayInfoWnd)
    end), false)
end

function CLuaGuiMenGuanStatusView:Init()
    self.m_TimeLabel.text = SafeStringFormat(LocalString.GetString("%02d:%02d"), math.floor(CLuaGuiMenGuanMgr.s_RemainTime / 60), CLuaGuiMenGuanMgr.s_RemainTime % 60)
    self.m_TimeBar.value = CLuaGuiMenGuanMgr.s_RemainTime / CLuaGuiMenGuanMgr.s_FightDuration
    self.m_BossCountLabel.text = SafeStringFormat(LocalString.GetString("BOSS %d/%d"), CLuaGuiMenGuanMgr.s_LiveBossCount, CLuaGuiMenGuanMgr.s_TotalBossCount)
end

function CLuaGuiMenGuanStatusView:OnEnable()
    g_ScriptEvent:AddListener("SyncGuiMenGuanInfo", self, "Init")
end

function CLuaGuiMenGuanStatusView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncGuiMenGuanInfo", self, "Init")
end

return CLuaGuiMenGuanStatusView
