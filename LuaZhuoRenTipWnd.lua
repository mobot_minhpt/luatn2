
LuaZhuoRenTipWnd = class()
RegistChildComponent(LuaZhuoRenTipWnd, "Label1", "Label1", UILabel)
RegistChildComponent(LuaZhuoRenTipWnd, "Label2", "Label2", UILabel)
RegistChildComponent(LuaZhuoRenTipWnd, "Label3", "Label3", UILabel)
RegistChildComponent(LuaZhuoRenTipWnd, "Label4", "Label4", UILabel)
RegistChildComponent(LuaZhuoRenTipWnd, "Label5", "Label5", UILabel)

function LuaZhuoRenTipWnd:Awake()
end

function LuaZhuoRenTipWnd:Init()
    self.Label1.text = g_MessageMgr:FormatMessage("ZhuoRen_Tip_01")
    self.Label2.text = g_MessageMgr:FormatMessage("ZhuoRen_Tip_02")
    self.Label3.text = g_MessageMgr:FormatMessage("ZhuoRen_Tip_03")
    self.Label4.text = g_MessageMgr:FormatMessage("ZhuoRen_Tip_04")
    self.Label5.text = g_MessageMgr:FormatMessage("ZhuoRen_Tip_05")
end

--@region UIEvent

--@endregion
