local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Color = import "UnityEngine.Color"
local UICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Profession = import "L10.Game.Profession"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EnumClass = import "L10.Game.EnumClass"
local EShareType = import "L10.UI.EShareType"
local Animator = import "UnityEngine.Animator"

LuaHuanHunShanZhuangResultWnd = class()
LuaHuanHunShanZhuangResultWnd.s_IsWin = nil
LuaHuanHunShanZhuangResultWnd.s_IsNormal = nil
LuaHuanHunShanZhuangResultWnd.s_Duration = nil
LuaHuanHunShanZhuangResultWnd.s_TeamInfo = nil
LuaHuanHunShanZhuangResultWnd.s_RewardType = 1

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end
RegistClassMember(LuaHuanHunShanZhuangResultWnd, "WndPanel")
RegistClassMember(LuaHuanHunShanZhuangResultWnd, "CloseButton")
RegistClassMember(LuaHuanHunShanZhuangResultWnd, "ShareButton")
RegistClassMember(LuaHuanHunShanZhuangResultWnd, "AchiButton")
RegistClassMember(LuaHuanHunShanZhuangResultWnd, "FightAgainButton")

function LuaHuanHunShanZhuangResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    
    self:InitUI()
end

function LuaHuanHunShanZhuangResultWnd:InitUI()
    self.WndPanel = self.transform:GetComponent(typeof(UIPanel))
    self.CloseButton = self.transform:Find("CloseButton").gameObject
    self.ShareButton = self.transform:Find("Anchor/InfoView/Bottom/ShareButton").gameObject
    self.FightAgainButton = self.transform:Find("Anchor/InfoView/Bottom/FightAgainButton").gameObject
    self.AchiButton = self.transform:Find("Anchor/InfoView/Bottom/AchiButton").gameObject
    --self.WndPanel.alpha = 0

    CommonDefs.AddOnClickListener(
        self.FightAgainButton, 
        DelegateFactory.Action_GameObject(function()
            CUIManager.CloseUI(CLuaUIResources.HuanHunShanZhuangResultWnd)
            --改成常驻玩法了
            CUIManager.ShowUI(CLuaUIResources.HuanHunShanZhuangSignUpWnd)
            --Gac2Gas.RequestHuanHunShanZhuangPlayTimes()
        end), 
        false
    )

    CommonDefs.AddOnClickListener(
        self.AchiButton, 
        DelegateFactory.Action_GameObject(function()
            LuaAchievementMgr.m_CurAchieveTab = LocalString.GetString("系统玩法")
            LuaAchievementMgr.m_CurAchieveSubTab = LocalString.GetString("还魂山庄")
            LuaAchievementMgr.m_CurGroupId = 0
            CUIManager.ShowUI("AchievementDetailWnd")
        end), 
        false
    )

    CommonDefs.AddOnClickListener(
        self.ShareButton, 
        DelegateFactory.Action_GameObject(function()
            self.FightAgainButton:SetActive(false)
            self.ShareButton:SetActive(false)
            self.CloseButton:SetActive(false)
            self.AchiButton:SetActive(false)
            CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                self.FightAgainButton:SetActive(true)
                self.ShareButton:SetActive(true)
                self.CloseButton:SetActive(true)
                self.AchiButton:SetActive(true)
            end))
        end), 
        false
    )

    -- 用m_InitFxPath才能保证OnLoadFxFinish在CUIFx的Start后触发，填在Prefab里
    --self.transform:Find("Anchor/Fx3").localPosition = Vector3(55, 55, 0)
    --self.transform:Find("Anchor/Fx3"):GetComponent(typeof(CUIFx)).OnLoadFxFinish = DelegateFactory.Action(function()
    --    self.transform:Find("Anchor/Fx3"):GetComponent(typeof(UITexture)).alpha = 0
    --end)
end

function LuaHuanHunShanZhuangResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaHuanHunShanZhuangResultWnd:Init()
    self:ShowResult(LuaHuanHunShanZhuangResultWnd.s_IsWin, LuaHuanHunShanZhuangResultWnd.s_IsNormal)
end

--@region UIEvent
--@endregion UIEvent

function LuaHuanHunShanZhuangResultWnd:ShowResult(win, normal)
    --self.transform:Find("Anchor/Fx2").gameObject:SetActive(not normal)
    --self.transform:Find("Anchor/Fx3").gameObject:SetActive(not normal)
    --if win then
    --    self.transform:Find("Anchor/Fx1"):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_shuqijiesuanjiemian_win.prefab")
    --else
    --    self.transform:Find("Anchor/Fx1"):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_shuqijiesuanjiemian_lose.prefab")
    --end
    
    self.transform:Find("WndBg/Success").gameObject:SetActive(win)
    self.transform:Find("WndBg/Fail").gameObject:SetActive(not win)

    self.transform:Find("Title/Success").gameObject:SetActive(win)
    self.transform:Find("Title/Fail").gameObject:SetActive(not win)

    --self.transform:Find("BossView/Portrait/Normal").gameObject:SetActive(normal)
    --self.transform:Find("BossView/Portrait/Hard").gameObject:SetActive(not normal)

    --self.transform:Find("Anchor/BossView/Bg/Normal").gameObject:SetActive(win and normal)
    --self.transform:Find("Anchor/BossView/Bg/Hard").gameObject:SetActive(win and not normal)
    --self.transform:Find("Anchor/BossView/Bg/Fail").gameObject:SetActive(not win)

    --self.transform:Find("Anchor/InfoView/Title/Mode/NormalLabel").gameObject:SetActive(normal)
    --self.transform:Find("Anchor/InfoView/Title/Mode/HardLabel").gameObject:SetActive(not normal)

    local hour = LuaHuanHunShanZhuangResultWnd.s_Duration and math.floor(LuaHuanHunShanZhuangResultWnd.s_Duration / 60) or 0
    local minute = LuaHuanHunShanZhuangResultWnd.s_Duration and LuaHuanHunShanZhuangResultWnd.s_Duration % 60 or 0
    hour = hour < 10 and "0"..hour or hour
    minute = minute < 10 and "0"..minute or minute
    self.transform:Find("Anchor/InfoView/Title/PrefixLabel/TimeLabel"):GetComponent(typeof(UILabel)).text = win and hour..":"..minute or "--:--"

    --local reward = self.transform:Find("Anchor/InfoView/Bottom/RewardItem").gameObject
    --reward:SetActive(normal)
    --if normal then
    --    self:InitOneItem(reward, ShuJia2022_Setting.GetData().EasyPlayRewardItem[LuaHuanHunShanZhuangResultWnd.s_RewardType - 1])
    --end

    local grid = self.transform:Find("Anchor/InfoView/Content/Grid"):GetComponent(typeof(UIGrid))
    local template = self.transform:Find("Anchor/InfoView/Content/Template").gameObject
    template:SetActive(false)
    Extensions.RemoveAllChildren(grid.transform)
    if LuaHuanHunShanZhuangResultWnd.s_TeamInfo then
        for i = 1, #LuaHuanHunShanZhuangResultWnd.s_TeamInfo do
            local member = LuaHuanHunShanZhuangResultWnd.s_TeamInfo[i]
            if member then
                local curItem = CUICommonDef.AddChild(grid.gameObject, template)
                curItem:SetActive(true)

                curItem.transform:Find("Leader").gameObject:SetActive(member.isLeader)
                curItem.transform:Find("Class"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), member.cls))
                
                curItem.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = member.name
                if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == member.id then
                    curItem.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).color = Color.green
                end
                
                curItem.transform:Find("LevelLabel"):GetComponent(typeof(UILabel)).text = "Lv."..member.lv
                if member.hasFeiSheng then
                    curItem.transform:Find("LevelLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor("D37D41", 0)
                end
                
                --curItem.transform:Find("CountLabel").gameObject:SetActive(not normal)
                --curItem.transform:Find("CountLabel"):GetComponent(typeof(UILabel)).text = not normal and LocalString.GetString("通关")..member.hardTimes..LocalString.GetString("次") or ""
            end
        end
    end
    grid:Reposition()

    self.transform:GetComponent(typeof(Animator)):Play("huanhunshanzhuangsignupwnd_shown_"..(win and 1 or 2))
end
