-- Auto Generated!!
local CGuildChallengeSituationWnd = import "L10.UI.CGuildChallengeSituationWnd"
local Int32 = import "System.Int32"
local QnTabButton = import "L10.UI.QnTabButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildChallengeSituationWnd.m_Init_CS2LuaHook = function (this) 

    if not CGuildChallengeSituationWnd.OpenGuildChallengeOccupy then
        this.tabView:SetTabsNotOpen(Table2ArrayWithCount({1}, 1, MakeArrayClass(System.Int32)))
    end

    this.tabView.OnSelect = MakeDelegateFromCSFunction(this.OnTabViewSelect, MakeGenericClass(Action2, QnTabButton, Int32), this)
    this.fightDetail:Init()

    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
end
CGuildChallengeSituationWnd.m_OnTabViewSelect_CS2LuaHook = function (this, button, index) 
    repeat
        local default = index
        if default == (0) then
            this.fightDetail:Init()
            break
        elseif default == (1) then
            this.occupy:Init()
            break
        else
            break
        end
    until 1
end
