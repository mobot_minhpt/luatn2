local QnTabView=import "L10.UI.QnTabView"
local QnTabButton = import "L10.UI.QnTabButton"
local QnButton = import "L10.UI.QnButton"
local EnumRequestSource = import "L10.UI.EnumRequestSource"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local UITexture = import "UITexture"

CLuaHongBaoWnd = class()
CLuaHongBaoWnd.Path = "ui/hongbao/LuaHongBaoWnd"
RegistClassMember(CLuaHongBaoWnd,"m_HistoryButton")
RegistClassMember(CLuaHongBaoWnd,"m_RefreshButton")
RegistClassMember(CLuaHongBaoWnd,"m_AwardButton")
RegistClassMember(CLuaHongBaoWnd,"m_HuaKuiButton")
RegistClassMember(CLuaHongBaoWnd,"m_TipButton")
RegistClassMember(CLuaHongBaoWnd,"m_HongbaoTable")
RegistClassMember(CLuaHongBaoWnd,"m_TitleLabel")
RegistClassMember(CLuaHongBaoWnd,"m_WorldTabBtn")
RegistClassMember(CLuaHongBaoWnd,"m_GuildTabBtn")
RegistClassMember(CLuaHongBaoWnd,"m_GiftTabBtn")
RegistClassMember(CLuaHongBaoWnd,"m_SectTabBtn")
RegistClassMember(CLuaHongBaoWnd,"m_AwardLabel")
RegistClassMember(CLuaHongBaoWnd,"m_HongBaoList")
RegistClassMember(CLuaHongBaoWnd, "m_TabIndex")

RegistClassMember(CLuaHongBaoWnd, "m_NormalTabTextureTable")
RegistClassMember(CLuaHongBaoWnd, "m_HighlightTabTextureTable")

function CLuaHongBaoWnd:Awake()
    self.m_HistoryButton = self.transform:Find("HistoryBtn"):GetComponent(typeof(QnButton))
    self.m_RefreshButton = self.transform:Find("RefreshBtn"):GetComponent(typeof(QnButton))
    self.m_AwardButton = self.transform:Find("AwardBtn"):GetComponent(typeof(QnButton))
    self.m_HuaKuiButton = self.transform:Find("HuaKuiBtn"):GetComponent(typeof(QnButton))
    self.m_TipButton = self.transform:Find("TipBtn"):GetComponent(typeof(QnButton))
    self.m_HongbaoTable = self.transform:Find("HongbaoTable"):GetComponent(typeof(QnTableView))
    --self.m_TitleLabel = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_WorldTabBtn = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/World"):GetComponent(typeof(QnTabButton))
    self.m_GuildTabBtn = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Guild"):GetComponent(typeof(QnTabButton))
    self.m_GiftTabBtn = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Gift"):GetComponent(typeof(QnTabButton))
    self.m_SectTabBtn = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Sect"):GetComponent(typeof(QnTabButton))
    self.m_SectTabBtn.gameObject:SetActive(LuaZongMenMgr.m_HongBaoIsOpen)
    self.m_NormalTabTextureTable = {self.m_WorldTabBtn, self.m_GuildTabBtn, self.m_GiftTabBtn, self.m_SectTabBtn}
    self.m_HighlightTabTextureTable = {}
    for i = 1, 4 do
        self.m_HighlightTabTextureTable[i] = self.m_NormalTabTextureTable[i].transform:Find("Selected"):GetComponent(typeof(UITexture))
        self.m_NormalTabTextureTable[i] = self.m_NormalTabTextureTable[i].gameObject:GetComponent(typeof(UITexture))
    end
    self.m_AwardLabel = self.transform:Find("AwardBtn/Label"):GetComponent(typeof(UILabel))
    self.m_HongBaoList = nil

end

function CLuaHongBaoWnd:Init( )
    self.m_HongbaoTable.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            if self.m_HongBaoList then
                return #self.m_HongBaoList
            end
            return 0
        end,
        function(view,row)
            local item
            local index = 0

            if self.m_HongBaoList[row+1].m_Status ~= 1 then
                index = 1
            end
            if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
                index = index + 2
            elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao then
                index = index + 4
            end

            item = view:GetFromPool(index)
            item:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:UpdateData(self.m_HongBaoList[row+1])
            return item
        end)

    local default = CHongBaoMgr.m_HongbaoType
    if default == EnumHongbaoType.WorldHongbao then
        self:OnWorldTabClick(self.m_WorldTabBtn)
    elseif default == EnumHongbaoType.GuildHongbao then
        self:OnGuildTabClick(self.m_GuildTabBtn)
    elseif default == EnumHongbaoType.SystemHongbao then
        self:OnWorldTabClick(self.m_WorldTabBtn)
    elseif default == EnumHongbaoType.Gift then
        self:OnGiftTabClick(self.m_GiftTabBtn)
    elseif default == EnumHongbaoType.SectHongbao then
        self:OnSectTabClick(self.m_SectTabBtn)
    end

    self.m_HistoryButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CHongBaoMgr.ShowHongBaoHistoryWnd()
    end)

    self.m_RefreshButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnRefreshButtonClick(btn)
    end)
    self.m_AwardButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnAwardButtonClick(btn)
    end)

    local tabView = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetComponent(typeof(QnTabView))
    tabView.OnSelect = DelegateFactory.Action_QnTabButton_int(
        function(btn,index)
            if index==0 then
                self:OnWorldTabClick(btn)
            elseif index==1 then
                self:OnGuildTabClick(btn)
            elseif index==2 then
                self:OnGiftTabClick(btn)
            elseif index==3 then
                self:OnSectTabClick(btn)
            end
        end)
    self.m_TipButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnTipButtonClick(btn)
    end)

    self.m_HuaKuiButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        Gac2Gas.RequestShowSendBangHuaHongBao()
    end)

    self:InitCoverBtn()
end
function CLuaHongBaoWnd:OnEnable( )
    g_ScriptEvent:AddListener("ReplyRecentHongBaoInfo",self,"UpdateHongBaoList")
    g_ScriptEvent:AddListener("ReplyLiBaoState",self,"UpdateLiBaoState")
    g_ScriptEvent:AddListener("UpdateHongBaoCoverInfo",self,"UpdateCoverBtnAlert")
end
function CLuaHongBaoWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyRecentHongBaoInfo",self,"UpdateHongBaoList")
    g_ScriptEvent:RemoveListener("ReplyLiBaoState",self,"UpdateLiBaoState")
    g_ScriptEvent:RemoveListener("UpdateHongBaoCoverInfo",self,"UpdateCoverBtnAlert")
end
function CLuaHongBaoWnd:OnWorldTabClick( button)
    self:UpdateTabStatus(1)
    self.m_HuaKuiButton.gameObject:SetActive(false)
    self.m_AwardLabel.text = LocalString.GetString("我要发红包")
    self.m_AwardButton.gameObject:SetActive(true)
    CHongBaoMgr.m_HongbaoType = EnumHongbaoType.WorldHongbao

    self.m_HongBaoList = CLuaHongBaoMgr.m_HongbaoDataTable[self.m_TabIndex]

    self.m_HongbaoTable:ReloadData(false, false)
    CHongBaoMgr.m_RequestSource = EnumRequestSource.RefreshRequest
    Gac2Gas.RequestRecentHongBaoInfo(EnumToInt(CHongBaoMgr.m_HongbaoType))
end
function CLuaHongBaoWnd:OnGuildTabClick( button)
    self:UpdateTabStatus(2)
    self.m_HuaKuiButton.gameObject:SetActive(true)
    self.m_AwardLabel.text = LocalString.GetString("我要发红包")
    self.m_AwardButton.gameObject:SetActive(true)
    CHongBaoMgr.m_HongbaoType = EnumHongbaoType.GuildHongbao
    self.m_HongBaoList = CLuaHongBaoMgr.m_HongbaoDataTable[self.m_TabIndex]

    self.m_HongbaoTable:ReloadData(false, false)
    -- 没有帮会时，服务器不会返回红包信息
    CHongBaoMgr.m_RequestSource = EnumRequestSource.RefreshRequest
    Gac2Gas.RequestRecentHongBaoInfo(EnumToInt(CHongBaoMgr.m_HongbaoType))
end
function CLuaHongBaoWnd:OnGiftTabClick( button)
    self:UpdateTabStatus(3)
    self.m_HuaKuiButton.gameObject:SetActive(false)
    self.m_AwardLabel.text = LocalString.GetString("我要发礼包")
    --self.m_TitleLabel.text = LocalString.GetString("礼包领取")
    if not CHongBaoMgr.Inst:IsLiBaoEnable() then
        self.m_AwardButton.gameObject:SetActive(false)
    else
        self.m_AwardButton.gameObject:SetActive(true)
    end
    CHongBaoMgr.m_HongbaoType = EnumHongbaoType.Gift
    self.m_HongBaoList = CLuaHongBaoMgr.m_HongbaoDataTable[self.m_TabIndex]

    self.m_HongbaoTable:ReloadData(false, false)
    CHongBaoMgr.m_RequestSource = EnumRequestSource.RefreshRequest
    Gac2Gas.RequestRecentDaoJuLiBaoInfo()
end
function CLuaHongBaoWnd:OnSectTabClick(button)
    self:UpdateTabStatus(4)
    self.m_HuaKuiButton.gameObject:SetActive(false)
    self.m_AwardLabel.text = LocalString.GetString("我要发福利")
    self.m_AwardButton.gameObject:SetActive(true)
    CHongBaoMgr.m_HongbaoType = EnumHongbaoType.SectHongbao
    self.m_HongBaoList = CLuaHongBaoMgr.m_HongbaoDataTable[self.m_TabIndex]
    self.m_HongbaoTable:ReloadData(false, false)
    CHongBaoMgr.m_RequestSource = EnumRequestSource.RefreshRequest
    Gac2Gas.RequestSectItemRedPackOverview()
end
function CLuaHongBaoWnd:UpdateLiBaoState( args )
    local state = args[0]
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        if state then
            self.m_AwardButton.gameObject:SetActive(true)
        else
            self.m_AwardButton.gameObject:SetActive(false)
        end
    end
end
function CLuaHongBaoWnd:OnTipButtonClick( button)
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.WorldHongbao then
        g_MessageMgr:ShowMessage("HongBao_Rules_World")
    elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.GuildHongbao then
        g_MessageMgr:ShowMessage("HongBao_Rules_Guild")
    elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        g_MessageMgr:ShowMessage("LiBao_Rules")
    elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao then
        g_MessageMgr:ShowMessage("SectHongbao_Rules")
    else
        -- CLogMgr.Log("Error: No hongbao type!")
    end
end
function CLuaHongBaoWnd:OnRefreshButtonClick( button)
    CHongBaoMgr.m_RequestSource = EnumRequestSource.RefreshRequest

    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao then
        Gac2Gas.RequestSectItemRedPackOverview()
    elseif CHongBaoMgr.m_HongbaoType ~= EnumHongbaoType.Gift then
        Gac2Gas.RequestRecentHongBaoInfo(EnumToInt(CHongBaoMgr.m_HongbaoType))
    else
        Gac2Gas.RequestRecentDaoJuLiBaoInfo()
    end
end
function CLuaHongBaoWnd:OnAwardButtonClick( button)
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        CUIManager.ShowUI(CIndirectUIResources.AwardGiftWnd)
    elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao then
        CUIManager.ShowUI(CLuaUIResources.ZongMenHongBaoSetWnd)
    else
        CHongBaoMgr.ShowAwardHongbaoWnd()
    end
end
function CLuaHongBaoWnd:UpdateHongBaoList()
    self.m_HongBaoList = CLuaHongBaoMgr.m_HongbaoDataTable[self.m_TabIndex]
    if CHongBaoMgr.m_RequestSource == EnumRequestSource.RefreshRequest then
        self.m_HongbaoTable:ReloadData(false, false)
    else
        self.m_HongbaoTable:ReloadData(true, true)
    end
end

function CLuaHongBaoWnd:UpdateTabStatus(index)
    self.m_TabIndex = index
    for i = 1, 4 do
        self.m_NormalTabTextureTable[i].enabled = index ~= i
        self.m_HighlightTabTextureTable[i].enabled = index == i
    end
end

function CLuaHongBaoWnd:InitCoverBtn()
    local CoverBtn = self.transform:Find("HongBaoCoverBtn").gameObject:GetComponent(typeof(QnButton))
    local needShowAlert = CLuaHongBaoMgr.HasNewCover()
    local alert = CoverBtn.transform:Find("Alert").gameObject
    alert:SetActive(needShowAlert)
    CoverBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CUIManager.ShowUI(CLuaUIResources.HongBaoCoverWnd)
        alert:SetActive(false)
    end)
end

function CLuaHongBaoWnd:UpdateCoverBtnAlert()
    local alert = self.transform:Find("HongBaoCoverBtn/Alert").gameObject
    local needShowAlert = CLuaHongBaoMgr.HasNewCover()
    alert:SetActive(needShowAlert)
end