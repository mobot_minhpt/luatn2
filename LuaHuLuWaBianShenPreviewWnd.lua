local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"

LuaHuLuWaBianShenPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SkillItem1", "SkillItem1", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SkillItem2", "SkillItem2", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SkillItem3", "SkillItem3", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SkillItem4", "SkillItem4", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SkillItem5", "SkillItem5", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SkillItem6", "SkillItem6", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "ModelTexture", "ModelTexture", CUITexture)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "PlayerName", "PlayerName", UILabel)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SelectBtn", "SelectBtn", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "HuLuWaName", "HuLuWaName", UILabel)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaHuLuWaBianShenPreviewWnd, "SkillLevel", "SkillLevel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHuLuWaBianShenPreviewWnd,"m_HuLuWaChooseList")
RegistClassMember(LuaHuLuWaBianShenPreviewWnd, "m_ModelTextureLoader")
RegistClassMember(LuaHuLuWaBianShenPreviewWnd, "m_SelectRowIndex")
RegistClassMember(LuaHuLuWaBianShenPreviewWnd, "m_BuffId")
function LuaHuLuWaBianShenPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.SelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectBtnClick()
	end)


	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
    self.m_HuLuWaChooseList = {}
    HuluBrothers_Transform.Foreach(function(id,data)
        table.insert(self.m_HuLuWaChooseList,id)
    end)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_HuLuWaChooseList
        end,
        function(item,row)
            self:InitItem(item,row)
        end
    )
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)       
    end)

    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)
    LuaHuLuWa2022Mgr.m_HuLuWaChooseData = {}
    
    self.SelectBtn:SetActive(true)
    self.PlayerName.gameObject:SetActive(true)
    local msgLabel = self.transform:Find("MsgLabel").gameObject
    if LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.eQiaoDuoRuYi then
        Gac2Gas.QueryHuluwaTransformData()
    elseif LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.eRuYiDong then
        Gac2Gas.QueryHuluwaTransformData()
    elseif LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.eTravel then
        msgLabel:SetActive(false)
        local randomIdx = UnityEngine_Random(1,8)
        self.TableView:SetSelectRow(randomIdx,true)
    elseif LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.ePreview then 
        msgLabel:SetActive(false)
        self.SelectBtn:SetActive(false)
        self.PlayerName.gameObject:SetActive(false)
    end
end

function LuaHuLuWaBianShenPreviewWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncHuluwaBuffLevel", self, "OnSyncHuluwaBuffLevel")
    g_ScriptEvent:AddListener("QueryHuluwaTransformDataResult", self, "OnQueryHuluwaTransformDataResult")
end

function LuaHuLuWaBianShenPreviewWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHuluwaBuffLevel", self, "OnSyncHuluwaBuffLevel")
    g_ScriptEvent:RemoveListener("QueryHuluwaTransformDataResult", self, "OnQueryHuluwaTransformDataResult")
end

function LuaHuLuWaBianShenPreviewWnd:Init()
    self.m_BuffId2HuluIndex = {
        [64505101] = 1,
        [64505201] = 2,
        [64505301] = 3,
        [64505401] = 4,
        [64505501] = 5,
        [64505601] = 6,
        [64505701] = 7,
    }
    self.m_HuluIndex2BuffId = {
        [1] = 64505101,
        [2] = 64505201,
        [3] = 64505301,
        [4] = 64505401,
        [5] = 64505501,
        [6] = 64505601,
        [7] = 64505701,
    }

    local curHuLuWaIdx = LuaHuLuWa2022Mgr.m_TravelingHuluwaId
    if LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.eTravel and curHuLuWaIdx then
        self.TableView:SetSelectRow(curHuLuWaIdx-1,true)
    else
        self.TableView:SetSelectRow(0,true)
    end

    self.TableView:ReloadData(false,false)
    self.ModelTexture.mainTexture=CUIManager.CreateModelTexture(self.gameObject:GetInstanceID(), self.m_ModelTextureLoader,180,0,-0.37,3,false,true,1)
end

function LuaHuLuWaBianShenPreviewWnd:InitItem(item,row)
    local nameLabel = item.transform:Find("NamLabel"):GetComponent(typeof(UILabel))
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local selectedTag = item.transform:Find("xuanze").gameObject

    local id = self.m_HuLuWaChooseList[row+1]
    local data = HuluBrothers_Transform.GetData(id)
    if not data then return end
    local monsterId = data.MonsterID
    local monster = Monster_Monster.GetData(monsterId)
    nameLabel.text = monster.Name

    local portrait = data.Portrait
    portrait = SafeStringFormat3("UI/Texture/Portrait/Material/%s.mat",portrait)
    icon:LoadMaterial(portrait)
    local memberInfo = LuaHuLuWa2022Mgr.m_HuLuWaChooseData[id]
    selectedTag:SetActive(memberInfo~=nil)

end

function LuaHuLuWaBianShenPreviewWnd:OnSyncHuluwaBuffLevel(buffId,level)
    self.Level = level
    local lookUpIdx = self.m_BuffId2HuluIndex[buffId]
    self.m_LookUpSkill = HuluBrothers_Transform.GetData(lookUpIdx).Skill[3]
    if lookUpIdx == self.m_SelectRowIndex+1 then
        self.SkillLevel.text = SafeStringFormat3("Lv.%d",level)
    else
        self.SkillLevel.text = nil
    end  
end

function LuaHuLuWaBianShenPreviewWnd:OnSelectAtRow(row)
    local id = self.m_HuLuWaChooseList[row+1]
    local data = HuluBrothers_Transform.GetData(id)
    if not data then return end
    local monsterId = data.MonsterID
    local monster = Monster_Monster.GetData(monsterId)
    self.HuLuWaName.text = monster.Name
    --HeadIcon
    local skillIds = data.Skill
    for i=0,skillIds.Length-1,1 do
        local item = self[SafeStringFormat3("SkillItem%d",i+1)]
        self:InitSkillItem(item,skillIds[i])
    end
    self.m_BuffId = self.m_HuluIndex2BuffId[id]
    Gac2Gas.QueryHuluwaBuffLevel(self.m_BuffId)

    self.m_SelectRowIndex = row
    --
    local prefabname = SafeStringFormat3("Character/NPC/lnpc209/Prefab/lnpc209_0%d.prefab",data.Model)
    if self.m_PreviewRo then
        self.m_PreviewRo:LoadMain(prefabname)
    end

    if LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.ePreview then
        return
    end
    local memberInfo = LuaHuLuWa2022Mgr.m_HuLuWaChooseData[id]
    if memberInfo then--被人选了
        self.SelectBtn:SetActive(false)
        self.PlayerName.gameObject:SetActive(true)
        self.PlayerName.text = SafeStringFormat3(LocalString.GetString("%s 已选"),memberInfo.playerName)
    else--没有人选
        self.SelectBtn:SetActive(true)
        self.PlayerName.gameObject:SetActive(false)
    end
end

function LuaHuLuWaBianShenPreviewWnd:InitSkillItem(item,skillId)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local corner = item.transform:Find("Corner")
    local skill = Skill_AllSkills.GetData(skillId)
    
    if skill then
        icon:LoadSkillIcon(skill.SkillIcon)
    end
    if CLuaDesignMgr.Skill_AllSkills_GetIsPassiveSkill(skill) then
        corner.gameObject:SetActive(true)
    else
        corner.gameObject:SetActive(false)
    end
    UIEventListener.Get(item.transform.gameObject).onClick = nil
    UIEventListener.Get(item.transform.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if skillId == self.m_LookUpSkill then
            CSkillInfoMgr.ShowSkillInfoWnd(skillId+self.Level-1, false,go.transform.position, CSkillInfoMgr.EnumSkillInfoContext.Default,true,0,0,nil)
        else
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, false,go.transform.position, CSkillInfoMgr.EnumSkillInfoContext.Default,true,0,0,nil)
        end
	end)
end

function LuaHuLuWaBianShenPreviewWnd:LoadModel(ro) 
    local id = self.m_HuLuWaChooseList[self.m_SelectRowIndex+1]
    local data = HuluBrothers_Transform.GetData(id)
    if not data then return end

    local prefabname = SafeStringFormat3("Character/NPC/lnpc209/Prefab/lnpc209_0%d.prefab",data.Model)
    self.m_PreviewRo = ro
    ro:LoadMain(prefabname)
end

function LuaHuLuWaBianShenPreviewWnd:OnDestroy()
    self.ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture(self.gameObject:GetInstanceID())
end

function LuaHuLuWaBianShenPreviewWnd:OnQueryHuluwaTransformDataResult()
    self.TableView:ReloadData(false,false)
end

--@region UIEvent

function LuaHuLuWaBianShenPreviewWnd:OnSelectBtnClick()
    --
    local id = self.m_HuLuWaChooseList[self.m_SelectRowIndex+1]
    local data = HuluBrothers_Transform.GetData(id)
    if not data then return end
    local targetId
    if LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.eQiaoDuoRuYi then
        targetId = data.NewMonsterID
        Gac2Gas.RequestTransformInHulu(targetId)
    elseif LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.eRuYiDong then
        targetId = data.MonsterID
        Gac2Gas.RequestTransformInHulu(targetId)
    elseif LuaHuLuWa2022Mgr.m_CurPreviewType == LuaHuLuWa2022Mgr.EnumPreviewType.eTravel then
        targetId = id
        Gac2Gas.RequestHuluwaAdventureTransform(targetId)
        LuaHuLuWa2022Mgr.m_TravelingHuluwaId = targetId
        g_ScriptEvent:BroadcastInLua("InitHuluwaAdventureTransform")
    end
    
    CUIManager.CloseUI(CLuaUIResources.HuLuWaBianShenPreviewWnd)
end


function LuaHuLuWaBianShenPreviewWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("YongChuangRuYiDong_Transform_Tip")
end


--@endregion UIEvent

