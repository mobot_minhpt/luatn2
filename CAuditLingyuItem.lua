-- Auto Generated!!
local CAuditLingyuItem = import "L10.UI.CAuditLingyuItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CAuditLingyuItem.m_SetData_CS2LuaHook = function (this, count, startTime, endTime) 
    this.CountLabel.text = tostring(count)
    local d = CServerTimeMgr.ConvertTimeStampToZone8Time(startTime)
    this.StartDateLabel.text = ToStringWrap(d, "yyyy-MM-dd HH:mm")
    d = CServerTimeMgr.ConvertTimeStampToZone8Time(endTime)
    this.EndLabel.text = ToStringWrap(d, "yyyy-MM-dd HH:mm")
end
