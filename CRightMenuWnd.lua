-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local CGuideMessager = import "L10.Game.Guide.CGuideMessager"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local CGuildQueenMgr = import "L10.Game.CGuildQueenMgr"
local CHouseUIMgr = import "L10.UI.CHouseUIMgr"
local CLogMgr = import "L10.CLogMgr"
local CMenPaiChuangGuanMgr = import "L10.Game.CMenPaiChuangGuanMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CRightMenuWnd = import "L10.UI.CRightMenuWnd"
local CSettingInfoMgr = import "L10.UI.CSettingInfoMgr"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"
local EHouseMainWndTab = import "L10.UI.EHouseMainWndTab"
local EnumEventType = import "EnumEventType"
local EnumMenuItem = import "L10.Game.EnumMenuItem"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local L10 = import "L10"
local LocalString = import "LocalString"
local LuaTweenUtils = import "LuaTweenUtils"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local PlayerSettings = import "L10.Game.PlayerSettings"
local RotateMode = import "DG.Tweening.RotateMode"
local Screen = import "UnityEngine.Screen"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UIRoot = import "UIRoot"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CRightMenuWnd.m_Awake_CS2LuaHook = function (this) 
    if CRightMenuWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!" .. this:GetType():ToString())
    end
    CRightMenuWnd.Instance = this

    this.originPackagePos = this.packageBtn.transform.localPosition
end
CRightMenuWnd.m_Start_CS2LuaHook = function (this) 

    UIEventListener.Get(this.expandBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.expandBtn).onClick, MakeDelegateFromCSFunction(this.OnExpandButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.enhanceBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.enhanceBtn).onClick, MakeDelegateFromCSFunction(this.OnEnhanceButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.skillBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.skillBtn).onClick, MakeDelegateFromCSFunction(this.OnSkillButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.lingshouBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.lingshouBtn).onClick, MakeDelegateFromCSFunction(this.OnLingShouButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.guildBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.guildBtn).onClick, MakeDelegateFromCSFunction(this.OnGuildButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.rankBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rankBtn).onClick, MakeDelegateFromCSFunction(this.OnRankButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.settingsBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.settingsBtn).onClick, MakeDelegateFromCSFunction(this.OnSettingsButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.zuoqiBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.zuoqiBtn).onClick, MakeDelegateFromCSFunction(this.OnZuoQiButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.packageBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.packageBtn).onClick, MakeDelegateFromCSFunction(this.OnPackageButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.houseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.houseBtn).onClick, MakeDelegateFromCSFunction(this.OnHouseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.babyBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.babyBtn).onClick, MakeDelegateFromCSFunction(this.OnBabyButtonClick, VoidDelegate, this), true)
end
CRightMenuWnd.m_OnEnable_CS2LuaHook = function (this) 

    EventManager.AddListener(EnumEventType.OnMainPlayerInGamePlayCopyInfoChange, MakeDelegateFromCSFunction(this.OnMainPlayerInGamePlayCopyInfoChange, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnGetHouseNewsRewardSuccess, MakeDelegateFromCSFunction(this.OnGetHouseNewsRewardSuccess, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.BabyNotification, MakeDelegateFromCSFunction(this.BabyNotification, MakeGenericClass(Action1, Boolean), this))
    if CClientHouseMgr.Inst.mCurFurnitureProp3 and CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify then
        this.houseAlert:SetActive(CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify == 1)
    else
        this.houseAlert:SetActive(false)
    end
end
CRightMenuWnd.m_OnDisable_CS2LuaHook = function (this) 

    EventManager.RemoveListener(EnumEventType.OnMainPlayerInGamePlayCopyInfoChange, MakeDelegateFromCSFunction(this.OnMainPlayerInGamePlayCopyInfoChange, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnGetHouseNewsRewardSuccess, MakeDelegateFromCSFunction(this.OnGetHouseNewsRewardSuccess, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.BabyNotification, MakeDelegateFromCSFunction(this.BabyNotification, MakeGenericClass(Action1, Boolean), this))
end
CRightMenuWnd.m_hookOnGetHouseNewsRewardSuccess = function(this,newsId)
    local needShow = CClientHouseMgr.Inst:CheckNeedAlert()
    if CClientHouseMgr.Inst.mCurFurnitureProp3 and CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify then
        needShow = needShow or CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify == 1
    end
    this.houseAlert:SetActive(needShow)
end
CRightMenuWnd.m_OnSyncMapiInfo_CS2LuaHook = function (this, zuoqiId) 
    local bNeedAlert = false

    if CZuoQiMgr.Inst.zuoqis ~= nil then
        CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value) 
            local v = ___value
            if v.mapiInfo ~= nil and v.mapiInfo.Life < 172800 --[[48*3600]] and v.mapiInfo.Alerted <= 0 then
                bNeedAlert = true
                return
            end
        end))
    end

    this.zuoqiAlert:SetActive(bNeedAlert)
end
CRightMenuWnd.m_GetPackageButtonVisible_CS2LuaHook = function (this) 
    if not COpenEntryMgr.Inst:IsMenuItemVisible(EnumMenuItem.Package) then
        return false
    end
    if CLuaAnQiDaoMgr.IsInPlay() then
        return false
    else
        return true
    end
end
CRightMenuWnd.m_Init_CS2LuaHook = function (this) 
    --TODO
    this:OnGetHouseNewsRewardSuccess(0)
    this:SetVisibleMenuItems()
    this:OnMainPlayerInGamePlayCopyInfoChange()
    this:OnSyncMapiInfo(nil)
end
CRightMenuWnd.m_OnExpandButtonClick_CS2LuaHook = function (this, go) 
    PlayerSettings.RightMenuExpanded = not PlayerSettings.RightMenuExpanded
    LuaTweenUtils.DOLocalRotate(this.expandBtn.transform, Vector3(0, 0, PlayerSettings.RightMenuExpanded and 45 or 0), CRightMenuWnd.ExpandAniDuration, RotateMode.Fast)
    local cornerIndex = 0
    local default
    default, cornerIndex = this:GetAdjustedCellPadding()
    local adjustedPadding = default

    if PlayerSettings.RightMenuExpanded then
        local n = this.menuRoot.childCount
        local visibleIndex = 0
        do
            local i = 0
            while i < n do
                local child = this.menuRoot:GetChild(i)
                child.localPosition = Vector3.zero
                if COpenEntryMgr.Inst:IsMenuItemVisible(this:GetMenuItemType(i)) then
                    if not child.gameObject.activeSelf then
                        child.gameObject:SetActive(true)
                    end
                    local extern = visibleIndex
                    visibleIndex = extern + 1
                    child.transform.localPosition = this:GetChildPos(Vector3.zero, adjustedPadding, extern, cornerIndex)
                    -- child.DOLocalPath(GetExpandWayPoints(Vector3.zero, adjustedPadding, visibleIndex++, cornerIndex), ExpandAniDuration, PathType.Linear, PathMode.Ignore, 10).SetEase(Ease.InOutCubic);
                else
                    child.gameObject:SetActive(false)
                end
                i = i + 1
            end
        end
        CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(this.menuRoot, this.originOffsetX, CRightMenuWnd.ExpandAniDuration, false), Ease.InOutCubic)
        CSkillButtonBoardWnd.Hide(true)
    else
        --int n = menuRoot.childCount;
        --int visibleIndex = 0;
        --for (int i = 0; i < n; ++i)
        --{
        --    Transform child = menuRoot.GetChild(i);
        --    if (visibleDict[i] && child.gameObject.activeSelf)
        --    {
        --        child.DOLocalPath(GetCollapseWayPoints(Vector3.zero, adjustedPadding, visibleIndex++, cornerIndex), ExpandAniDuration, PathType.Linear, PathMode.Ignore, 10).SetEase(Ease.InOutCubic).OnComplete(() =>
        --        {
        --            child.gameObject.SetActive(false);
        --        });
        --    }
        --}
        CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(this.menuRoot, this.actualHiddenOffsetX, CRightMenuWnd.ExpandAniDuration, false), Ease.InOutCubic)
        CSkillButtonBoardWnd.Show(true)
    end
    EventManager.Broadcast(EnumEventType.OnRightMenuWndExpandedStatusChanged)
    --#region Guide
    local cmp = CommonDefs.GetComponent_GameObject_Type(this.expandBtn, typeof(CGuideMessager))
    if cmp ~= nil then
        cmp:Click()
    end

    --有时候会点穿，所以加保险。只有在主界面的时候才会往外抛事件
    if CUIManager.instance:GetTopPopUI() == nil then
        if this.IsExpanded then
            EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {"RightMenuWnd"})
        else
            EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {"UnexpandRightMenu"})
        end
    end
    --#endregion
end
CRightMenuWnd.m_OnSettingsButtonClick_CS2LuaHook = function (this, go) 
    CSettingInfoMgr.ShowSettingWnd(0)
end
CRightMenuWnd.m_OnHouseButtonClick_CS2LuaHook = function (this, go) 

    if not CSwitchMgr.EnableHouse then
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end

    if CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse() then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("正在巫先生家园，无法查看自己的家园信息"))
        return
    end

    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.IsCrossServerPlayer then
            g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
            return
        end
        local houseid = CClientMainPlayer.Inst.ItemProp.HouseId
        if houseid ~= nil and houseid ~= "" then
            CHouseUIMgr.OpenHouseMainWnd(EHouseMainWndTab.EInfoTab)
        else
            local message = g_MessageMgr:FormatMessage("BUY_HOUSE_TIP")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                CTrackMgr.Inst:FindNPC(Constants.JiayuanNpcId, Constants.HangZhouID, 89, 38, nil, nil)
            end), nil, LocalString.GetString("前往"), LocalString.GetString("取消"), false)
        end
    end
end
CRightMenuWnd.m_SetExpandedWithoutAni_CS2LuaHook = function (this) 
    local cornerIndex = 0
    local default
    default, cornerIndex = this:GetAdjustedCellPadding()
    local adjustedPadding = default
    if PlayerSettings.RightMenuExpanded then
        local n = this.menuRoot.childCount
        local visibleIndex = 0
        do
            local i = 0
            while i < n do
                local child = this.menuRoot:GetChild(i)
                child.localPosition = Vector3.zero
                if COpenEntryMgr.Inst:IsMenuItemVisible(this:GetMenuItemType(i)) then
                    if not child.gameObject.activeSelf then
                        child.gameObject:SetActive(true)
                    end
                    local extern = visibleIndex
                    visibleIndex = extern + 1
                    child.localPosition = this:GetChildPos(Vector3.zero, adjustedPadding, extern, cornerIndex)
                else
                    child.gameObject:SetActive(false)
                end
                i = i + 1
            end
        end
        Extensions.SetLocalPositionX(this.aniRoot, this.originOffsetX)
        CSkillButtonBoardWnd.Hide(false)
    else
        --int n = menuRoot.childCount;
        --for (int i = 0; i < n; ++i)
        --{
        --    Transform child = menuRoot.GetChild(i);
        --    if (child.gameObject.activeSelf)
        --    {
        --        child.gameObject.SetActive(false);
        --        child.localPosition = Vector3.zero;
        --    }
        --}
        Extensions.SetLocalPositionX(this.aniRoot, this.actualHiddenOffsetX)
        CSkillButtonBoardWnd.Show(false)
    end
    Extensions.SetLocalRotationZ(this.expandBtn.transform, PlayerSettings.RightMenuExpanded and 45 or 0)
end
CRightMenuWnd.m_GetAvailableHeightSpace_CS2LuaHook = function (this) 
    --为了避免在Anchor更新之前执行，需要手工调用一次,但是下面这句会导致屏幕缩放视图时，切换场景后menuroot位置不正确
    -- anchorWidget.ResetAndUpdateAnchors();

    local screenPos = (CUIManager.UIMainCamera == nil) and Vector3.zero or CUIManager.UIMainCamera:WorldToScreenPoint(this.menuRoot.position)
    if screenPos.y <= 0 then
        return 0
    end
    local scale = UIRoot.GetPixelSizeAdjustment(this.gameObject)
    local virtualScreenWidth = Screen.width * scale
    local virtualScreenHeight = Screen.height * scale
    if UIRoot.EnableIPhoneXAdaptation then
        return (screenPos.y / Screen.height) * virtualScreenHeight
    end
    return (screenPos.y / Screen.height) * virtualScreenHeight
end
CRightMenuWnd.m_SetVisibleMenuItems_CS2LuaHook = function (this) 
    this.packageBtn:SetActive(this:GetPackageButtonVisible())
    do
        local i = 0
        while i < this.menuRoot.childCount do
            this.menuRoot:GetChild(i).gameObject:SetActive(COpenEntryMgr.Inst:IsMenuItemVisible(this:GetMenuItemType(i)))
            i = i + 1
        end
    end
    this:SetExpandedWithoutAni()
end
CRightMenuWnd.m_ActivateMenuItem_CS2LuaHook = function (this, item, initalWorldPos, callback) 
    if item == EnumMenuItem.Package then
        this.packageBtn.transform.position = initalWorldPos
        local destWorldPos = this.packageBtn.transform.parent:TransformPoint(this.originPackagePos)
        this.packageBtn:SetActive(true)
        CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(this.packageBtn.transform, destWorldPos, CRightMenuWnd.InsertAniDuration, false), Ease.InOutCubic), DelegateFactory.TweenCallback(function () 
            GenericDelegateInvoke(callback, Table2ArrayWithCount({true}, 1, MakeArrayClass(Object)))
        end))
    else
        local n = this.menuRoot.transform.childCount
        local index = - 1
        CommonDefs.DictIterate(this.menuIndex2ItemTypeDict, DelegateFactory.Action_object_object(function (___key, ___value) 
            local pair = {}
            pair.Key = ___key
            pair.Value = ___value
            if pair.Value == EnumToInt(item) then
                index = pair.Key
                return
            end
        end))
        if index >= 0 and index < n and COpenEntryMgr.Inst:IsMenuItemVisible(item) then
            if not PlayerSettings.RightMenuExpanded then
                PlayerSettings.RightMenuExpanded = true
                this:SetExpandedWithoutAni()
            end

            local child = this.menuRoot.transform:GetChild(index)
            child.position = initalWorldPos
            child.gameObject:SetActive(true)
            local visibleIndex = 0

            local cornerIndex = 0
            local default
            default, cornerIndex = this:GetAdjustedCellPadding()
            local adjustedPadding = default

            do
                local i = 0
                while i < n do
                    local t = this.menuRoot:GetChild(i)
                    if COpenEntryMgr.Inst:IsMenuItemVisible(this:GetMenuItemType(i)) and t.gameObject.activeSelf then
                        if i > index then
                            LuaTweenUtils.DOLuaLocalPathOnce(t, this:GetExpandWayPoints(Vector3.zero, adjustedPadding, visibleIndex, cornerIndex), CRightMenuWnd.InsertAniDuration, PathType.Linear, PathMode.Ignore, CRightMenuWnd.InsertAniDuration, Ease.InOutCubic)
                        elseif i == index then
                            local localPos = this:GetChildPos(Vector3.zero, adjustedPadding, visibleIndex, cornerIndex)
                            local worldPos = this.menuRoot:TransformPoint(localPos)
                            CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(t, worldPos, CRightMenuWnd.InsertAniDuration, false), Ease.InOutCubic), DelegateFactory.TweenCallback(function () 
                                GenericDelegateInvoke(callback, Table2ArrayWithCount({true}, 1, MakeArrayClass(Object)))
                            end))
                        end
                        visibleIndex = visibleIndex + 1
                    end
                    i = i + 1
                end
            end
        else
            GenericDelegateInvoke(callback, Table2ArrayWithCount({false}, 1, MakeArrayClass(Object)))
        end
    end
end
CRightMenuWnd.m_GetMenuItemGo_CS2LuaHook = function (this, item) 
    repeat
        local default = item
        if default == EnumMenuItem.Package then
            return this.packageBtn
        elseif default == EnumMenuItem.Enhance then
            return this.enhanceBtn
        elseif default == EnumMenuItem.Skill then
            return this.skillBtn
        elseif default == EnumMenuItem.LingShou then
            return this.lingshouBtn
        elseif default == EnumMenuItem.Guild then
            return this.guildBtn
        elseif default == EnumMenuItem.Rank then
            return this.rankBtn
        elseif default == EnumMenuItem.Setting then
            return this.settingsBtn
        elseif default == EnumMenuItem.ZuoQi then
            return this.zuoqiBtn
        elseif default == EnumMenuItem.House then
            return this.houseBtn
        elseif default == EnumMenuItem.Baby then
            return this.babyBtn
        end
    until 1
    return nil
end
CRightMenuWnd.m_Hide_CS2LuaHook = function () 
    if PlayerSettings.RightMenuExpanded then
        PlayerSettings.RightMenuExpanded = false
        if CUIManager.IsLoaded(CUIResources.RightMenuWnd) then
            CRightMenuWnd.Instance:SetExpandedWithoutAni()
        end
    end
    PlayerSettings.RightMenuExpanded = false
end
CRightMenuWnd.m_Show_CS2LuaHook = function () 
    if not PlayerSettings.RightMenuExpanded then
        PlayerSettings.RightMenuExpanded = true
        if CUIManager.IsLoaded(CUIResources.RightMenuWnd) then
            CRightMenuWnd.Instance:SetExpandedWithoutAni()
        end
    end
    PlayerSettings.RightMenuExpanded = true
end
CRightMenuWnd.m_GetGuildButton_CS2LuaHook = function (this) 
    --帮会引导用到的，如果已经有帮会了，那么就不进行后面的引导了
    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(L10.Game.Guide.GuideDefine.Phase_Guild) and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsInGuild() then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        return nil
    elseif L10.Game.Guide.CGuideMgr.Inst:IsInPhase(L10.Game.Guide.GuideDefine.Phase_FirstEnterGuild) and CClientMainPlayer.Inst ~= nil and not CClientMainPlayer.Inst:IsInGuild() then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        return nil
    else
        return this.guildBtn
    end
end
CRightMenuWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "GetLingShouButton" then
        return this.lingshouBtn
    elseif methodName == "GetSkillButton" then
        return this.skillBtn
    elseif methodName == "GetGuildButton" then
        return this:GetGuildButton()
    elseif methodName == "GetEnhanceButton" then
        return this.enhanceBtn
    elseif methodName == "GetExpandButton" then
        return this.expandBtn
    elseif methodName == "GetSettingButton" then
        return this.settingsBtn
    elseif methodName == "GetPackageButton" then
        return this.packageBtn
    elseif methodName == "GetZuoQiButton" then
        return this.zuoqiBtn
    elseif methodName == "GetHouseButton" then
        return this:GetHouseButton()
    elseif methodName == "GetBabyButton" then
        return this.babyBtn
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end
CRightMenuWnd.m_GetHouseButton_CS2LuaHook = function (this) 

    if not CSwitchMgr.EnableHouse then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        return nil
    end

    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(L10.Game.Guide.GuideDefine.Phase_GetHouse) and not CClientHouseMgr.Inst:HaveHouse() then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        return nil
    end
    return this.houseBtn
end
