local CChatLinkMgr=import "CChatLinkMgr"
local CUIManager = import "L10.UI.CUIManager"
local QnAdvanceTableView = import "L10.UI.QnAdvanceTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UILabel = import "UILabel"
local UICamera = import "UICamera"
local LuaTweenUtils = import "LuaTweenUtils"
local Color = import "UnityEngine.Color"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"

CLuaDanMuViewWnd = class()
RegistClassMember(CLuaDanMuViewWnd, "m_Background")
RegistClassMember(CLuaDanMuViewWnd, "m_Root")
RegistClassMember(CLuaDanMuViewWnd, "m_AllAdvanceView")
RegistClassMember(CLuaDanMuViewWnd, "m_ClientAdvanceView")
RegistClassMember(CLuaDanMuViewWnd, "m_AllScrollView")
RegistClassMember(CLuaDanMuViewWnd, "m_ClientScrollView")
RegistClassMember(CLuaDanMuViewWnd, "m_AllBulletTable")
RegistClassMember(CLuaDanMuViewWnd, "m_ClientBulletTable")
RegistClassMember(CLuaDanMuViewWnd, "m_ClosingTick")
RegistClassMember(CLuaDanMuViewWnd, "m_Inited")
RegistClassMember(CLuaDanMuViewWnd, "m_SelectedRow")
RegistClassMember(CLuaDanMuViewWnd, "m_NormalScrollViewCollider")
RegistClassMember(CLuaDanMuViewWnd, "m_ClientScrollViewCollider")
RegistClassMember(CLuaDanMuViewWnd, "m_AllBulletRoot")
RegistClassMember(CLuaDanMuViewWnd, "m_ClientBulletRoot")
RegistClassMember(CLuaDanMuViewWnd, "m_ClientViewReturnButton")
RegistClassMember(CLuaDanMuViewWnd, "m_CurrentView") -- 0 所有弹幕， 1 用户弹幕
RegistClassMember(CLuaDanMuViewWnd, "m_CurrentLookingClient")
RegistClassMember(CLuaDanMuViewWnd, "m_LeftTip") -- 左上角文字

function CLuaDanMuViewWnd:Init()
    if(not self.m_Inited)then
        self.m_Inited = true
        self.m_AllBulletTable = {}
        self.m_ClientBulletTable = {}
        self.m_SelectedRow = -1
        self.m_CurrentLookingClient = nil

        --初始化弹幕列表
        for k, v in ipairs(LuaDanMuMgr.Data) do
           -- print(v)
            table.insert(self.m_AllBulletTable, v)
        end

        self.m_ClientAdvanceView.m_DataSource = DefaultTableViewDataSource.Create(
            function() return self:GetClientDataNum() end,
            function(item,row) self:InitClientItem(item,row) end
        )

        self.m_AllAdvanceView.m_DataSource = DefaultTableViewDataSource.Create(
            function() return self:GetAllDataNum() end,
            function(item,row) self:InitAllItem(item,row) end
        )
        self.m_AllAdvanceView:ReloadData(true, true)
        self.m_AllAdvanceView.m_ScrollView:InvalidateBounds()
        self.m_AllAdvanceView:ScrollToRow(self:GetAllDataNum() - 1)
        self.m_Root.transform.localPosition = Vector3(640, 0, 0)
        LuaTweenUtils.TweenPosition( self.m_Root.transform, 0, 0, 0, 0.5)

        UIEventListener.Get(self.m_NormalScrollViewCollider).onClick = DelegateFactory.VoidDelegate(function(g)
            self:ResetSelection()
        end)
        UIEventListener.Get(self.m_ClientScrollViewCollider).onClick = DelegateFactory.VoidDelegate(function(g)
            self:ResetSelection()
        end)

        UIEventListener.Get(self.m_NormalScrollViewCollider).onDragStart = DelegateFactory.VoidDelegate(function(g)
            self:ResetSelection()
        end)
        UIEventListener.Get(self.m_ClientScrollViewCollider).onDragStart = DelegateFactory.VoidDelegate(function(g)
            self:ResetSelection()
        end)

        --self:InitTestWnd()
    end
end

function CLuaDanMuViewWnd:GetAllDataNum()
    return #self.m_AllBulletTable
end

function CLuaDanMuViewWnd:GetClientDataNum()
    return #self.m_ClientBulletTable
end

function CLuaDanMuViewWnd:ConvertContentStr(_data)
    if(CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == tonumber(_data.playerId))then
        return LocalString.GetString(_data.data)
    else
        return "[url=DANMU]"..LocalString.GetString(_data.data).."[/url]"
    end
end

function CLuaDanMuViewWnd:ConvertPlayerStr(_player)
    if(CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == tonumber(_player))then
        return ("[00c932]["..CClientMainPlayer.Inst.RealName.."][-]")
    else
        return CChatLinkMgr.ChatPlayerLink.GenerateLink(_player, LuaDanMuMgr:GetPlayerName(_player)).displayTag
    end
end

function CLuaDanMuViewWnd:OnEnable()
    self.m_Inited = false
    self.m_CurrentView = 0
    g_ScriptEvent:AddListener("OnSyncDanMu", self, "OnRefreshDanMu")
    g_ScriptEvent:AddListener("OnRefreshDanMuNames", self, "OnRefreshDatas")
    g_ScriptEvent:AddListener("OnCleanDanMu", self, "OnClean")

    g_ScriptEvent:AddListener("OnInitDanMu", self, "InitDanMu")
    self.m_Background = self.transform:Find("Background").gameObject
    self.m_Root = self.transform:Find("Background/ViewRoot").gameObject--:GetComponent(typeof(QnAdvanceTableView))

    self.m_AllAdvanceView = self.m_Root.transform:Find("AllView/ScrollView"):GetComponent(typeof(QnAdvanceTableView))
    self.m_ClientAdvanceView = self.m_Root.transform:Find("ClientView/ScrollView"):GetComponent(typeof(QnAdvanceTableView))

    self.m_AllScrollView = self.m_Root.transform:Find("AllView/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    self.m_ClientScrollView = self.m_Root.transform:Find("ClientView/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    
    self.m_NormalScrollViewCollider = self.m_Root.transform:Find("AllView/ScrollViewCollider").gameObject
    self.m_ClientScrollViewCollider = self.m_Root.transform:Find("ClientView/ScrollViewCollider").gameObject
        
    self.m_AllBulletRoot = self.m_Root.transform:Find("AllView").gameObject
    self.m_ClientBulletRoot = self.m_Root.transform:Find("ClientView").gameObject

    self.m_ClientViewReturnButton = self.m_ClientBulletRoot.transform:Find("ReturnButton").gameObject

    self.m_LeftTip = self.m_Root.transform:Find("TopLeftTip"):GetComponent(typeof(UILabel))
    self.m_LeftTip.text = LocalString.GetString("当前弹幕列表")

    self.m_ClosingTick = nil

    --local mainplayer = CClientMainPlayer.Inst
    --local playerInfo = ChatPlayerLink.GenerateLink(mainplayer.PlayProp.PlayId, "TESTTESTNAME").displayTag

    UIEventListener.Get(self.m_ClientViewReturnButton).onClick = DelegateFactory.VoidDelegate(function(g)
        self.m_CurrentView = 0
        self.m_SelectedRow = -1
        self.m_CurrentLookingClient = nil
        --self.m_LeftTip.text = LocalString.GetString("当前弹幕列表")
        self:ResetView()
    end)

    UIEventListener.Get(self.m_Background).onClick = DelegateFactory.VoidDelegate(function(g)

        self:OnClean()
    end)

    self:ResetView()
end

function CLuaDanMuViewWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("OnSyncDanMu", self, "OnRefreshDanMu")
    g_ScriptEvent:RemoveListener("OnRefreshDanMuNames", self, "OnRefreshDatas")
    g_ScriptEvent:RemoveListener("OnCleanDanMu", self, "OnClean")
    g_ScriptEvent:RemoveListener("OnInitDanMu", self, "InitDanMu")
    if(self.m_ClosingTick)then
        UnRegisterTick(self.m_ClosingTick)
        self.m_ClosingTick = nil
    end
end

function CLuaDanMuViewWnd:InitDanMu()

    --初始化弹幕列表
    self.m_AllBulletTable = {}
    self.m_ClientBulletTable = {}
    for k, v in ipairs(LuaDanMuMgr.Data) do
        -- print(v)
        table.insert(self.m_AllBulletTable, v)
    end
    CLuaDanMuViewWnd:ResetView()
end

function CLuaDanMuViewWnd:OnClean()
    if(not self.m_ClosingTick) then
        self.m_ClosingTick = RegisterTickOnce(function()
            CUIManager.CloseUI(CLuaUIResources.DanMuViewWnd)
        end, 500)
        LuaTweenUtils.TweenPosition( self.m_Root.transform, 640, 0, 0, 0.5)
    end
end

function CLuaDanMuViewWnd:ResetSelection()
   -- print("RESET")
    if(self.m_SelectedRow ~= -1)then
        self.m_SelectedRow = -1
        if(self.m_CurrentView == 0)then
            self.m_AllAdvanceView:ReloadData(false, true)
        elseif(self.m_CurrentView == 1)then
            self.m_ClientAdvanceView:ReloadData(false, true)
        end
    end
end

function CLuaDanMuViewWnd:InitAllItem(_item, _row)
    local data = self.m_AllBulletTable[_row + 1]
    _item.m_Label.text = self:ConvertPlayerStr(data.playerId)..self:ConvertContentStr(data)
    if(self.m_SelectedRow == -1 or _row == self.m_SelectedRow)then
        _item.m_Label.color = Color.white
    else
        _item.m_Label.color = Color.gray
    end
    UIEventListener.Get(_item.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local index = _item.m_Label:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
        if(index ~= 0)then --index为零时说明为空
            --local url = _item.m_Label:GetUrlAtPosition(UICamera.lastWorldPosition)
            local url = _item.m_Label:GetUrlAtCharacterIndex(index)
            if url ~= nil then
                self:OnNormalDanMuClick(_item.gameObject, url, _row)
            else
                self:ResetSelection()
            end
        elseif(#data.data < 6)then
            self:OnNormalDanMuClick(_item.gameObject, "DANMU", _row)
        else
            self:ResetSelection()
        end
    end)
    UIEventListener.Get(_item.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:ResetSelection()
    end)
end

function CLuaDanMuViewWnd:InitClientItem(_item, _row)
    local data = self.m_ClientBulletTable[_row + 1]
    _item.m_Label.text = self:ConvertPlayerStr(data.playerId)..self:ConvertContentStr(data)
    if(self.m_SelectedRow == -1 or _row == self.m_SelectedRow)then
        _item.m_Label.color = Color.white
    else
        _item.m_Label.color = Color.gray
    end
    UIEventListener.Get(_item.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local index = _item.m_Label:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
        if(index ~= 0)then --index为零时说明为空
            --local url = _item.m_Label:GetUrlAtPosition(UICamera.lastWorldPosition)
            local url = _item.m_Label:GetUrlAtCharacterIndex(index)
            if url ~= nil then
                self:OnClientDanMuClick(_item.gameObject, url, _row)
            else
                self:ResetSelection()
            end
        elseif(#data.data < 6)then
            self:OnClientDanMuClick(_item.gameObject, "DANMU", _row)
        else
            self:ResetSelection()
        end
    end)
    UIEventListener.Get(_item.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:ResetSelection()
    end)
end

function CLuaDanMuViewWnd:OnNormalDanMuClick(_go, _url, _row)

    self.m_SelectedRow = _row
    if(_url == "DANMU")then

        local menulist = {
            PopupMenuItemData(LocalString.GetString("举报弹幕"), DelegateFactory.Action_int(function(index) self:OnNormalMenuClick(index) end), false, nil, EnumPopupMenuItemStyle.Default),
            PopupMenuItemData(LocalString.GetString("查看该玩家弹幕"), DelegateFactory.Action_int(function(index) self:OnNormalMenuClick(index) end), false, nil, EnumPopupMenuItemStyle.Default)
        }
        CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menulist,MakeArrayClass(PopupMenuItemData)), _go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 430, true, 290)
    else

        CChatLinkMgr.ProcessLinkClick(_url, nil)
    end
    self.m_AllAdvanceView:ReloadData(false, true)
end

function CLuaDanMuViewWnd:OnClientDanMuClick(_go, _url, _row)

    self.m_SelectedRow = _row
    if(_url == "DANMU")then

        local menulist = {
            PopupMenuItemData(LocalString.GetString("举报弹幕"), DelegateFactory.Action_int(function(index) self:OnClientMenuClick(index) end), false, nil, EnumPopupMenuItemStyle.Default)
        }
        CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(menulist,MakeArrayClass(PopupMenuItemData)), _go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 430, true, 290)
    else
        CChatLinkMgr.ProcessLinkClick(_url, nil)
    end
    self.m_ClientAdvanceView:ReloadData(false, true)
end

function CLuaDanMuViewWnd:OnNormalMenuClick(_index)
    if(_index == 0)then --举报弹幕
        local playerid = self.m_AllBulletTable[self.m_SelectedRow + 1].playerId
        self:SetKeJuDanMuReportData(playerid, LuaDanMuMgr:GetPlayerName(playerid), self.m_AllBulletTable[self.m_SelectedRow + 1].data)
    elseif(_index == 1)then --查看玩家其他弹幕
        self.m_CurrentView = 1
        self.m_CurrentLookingClient = self.m_AllBulletTable[self.m_SelectedRow + 1].playerId
        self:ResetView()
    end
end

function CLuaDanMuViewWnd:OnClientMenuClick(_index)
    if(_index == 0)then --举报弹幕
        local playerid = self.m_ClientBulletTable[self.m_SelectedRow + 1].playerId
        self:SetKeJuDanMuReportData(playerid, LuaDanMuMgr:GetPlayerName(playerid), self.m_ClientBulletTable[self.m_SelectedRow + 1].data)
    end
end

function CLuaDanMuViewWnd:SetKeJuDanMuReportData(_playerId, _display_name, _danmu_content)
    LuaPlayerReportMgr:ShowReportWnd(_playerId, _display_name,
        LocalString.GetString("举报说书人"),
        EnumPlayerInfoContext.Keju,
        LocalString.GetString("举报科举弹幕内容：").._danmu_content,
        nil, nil, LocalString.GetString("科举直播"))
end

function CLuaDanMuViewWnd:ResetView()
    self.m_SelectedRow = -1
    if(self.m_CurrentView == 0)then --所有弹幕
        self.m_AllBulletRoot:SetActive(true)
        self.m_ClientBulletRoot:SetActive(false)
        self.m_LeftTip.text = LocalString.GetString("当前弹幕列表")
        self.m_AllAdvanceView:ReloadData(true, true)
    elseif(self.m_CurrentView == 1)then --用户弹幕
        self:FilterClientBullets()
        self.m_LeftTip.text = LocalString.GetString("找到")..#self.m_ClientBulletTable..LocalString.GetString("条弹幕")
        self.m_AllBulletRoot:SetActive(false)
        self.m_ClientBulletRoot:SetActive(true)
        self.m_ClientAdvanceView:ReloadData(true, true)
        self.m_ClientAdvanceView.m_ScrollView:InvalidateBounds()
        self.m_ClientAdvanceView:ScrollToRow(self:GetClientDataNum() - 1)
    end
end

function CLuaDanMuViewWnd:OnRefreshDanMu(_newDanMu)
    local last_visible = false
    if(self.m_CurrentView == 0)then --所有弹幕
        last_visible = self:IsAllLastVisible()
    elseif(self.m_CurrentView == 1)then --用户弹幕
        last_visible = self:IsClientLastVisible()
    end
    last_visible = last_visible and (self.m_SelectedRow == -1)
    for k, v in ipairs(_newDanMu) do
        table.insert(self.m_AllBulletTable, v)
        self:TryInsertClientBullet(v)
    end
    if(self.m_CurrentView == 0)then --所有弹幕
        self.m_AllAdvanceView:ReloadData(true, true)
        if(last_visible)then

            --因为加了新东西，在滑动至最底前需重构scroll view的边界
            self.m_AllAdvanceView.m_ScrollView:InvalidateBounds()
            self.m_AllAdvanceView:ScrollToRow(self:GetAllDataNum() - 1)
        end
    elseif(self.m_CurrentView == 1)then --用户弹幕
        self.m_LeftTip.text = LocalString.GetString("找到")..#self.m_ClientBulletTable..LocalString.GetString("条弹幕")
        self.m_ClientAdvanceView:ReloadData(true, true)
        if(last_visible)then
            self.m_ClientAdvanceView.m_ScrollView:InvalidateBounds()
            self.m_ClientAdvanceView:ScrollToRow(self:GetClientDataNum() - 1)
        end
    end
end

function CLuaDanMuViewWnd:OnRefreshDatas()
    if(self.m_CurrentView == 0)then --所有弹幕
        self.m_AllAdvanceView:ReloadData(true, true)
    elseif(self.m_CurrentView == 1)then --用户弹幕
        self.m_ClientAdvanceView:ReloadData(true, true)
    end
end

--用户弹幕相关，过滤方式待修改

function CLuaDanMuViewWnd:FilterClientBullets()
    self.m_ClientBulletTable = {}
    if(self.m_CurrentLookingClient)then
        for k, v in ipairs(self.m_AllBulletTable)do
            if(v.playerId == self.m_CurrentLookingClient)then                             --判断该条弹幕id是否与指定玩家id一致
                table.insert(self.m_ClientBulletTable, v)
            end
        end
    end
end

function CLuaDanMuViewWnd:TryInsertClientBullet(_dan_mu)
    if(self.m_CurrentLookingClient ~= nil and _dan_mu.playerId == self.m_CurrentLookingClient)then             --判断该条弹幕id是否与指定玩家id一致
        table.insert(self.m_ClientBulletTable, _dan_mu)
    end
end

function CLuaDanMuViewWnd:IsAllLastVisible()
    if(#self.m_AllBulletTable == 0)then
        return false
    else
        local item = self.m_AllAdvanceView:GetItemAtRow(#self.m_AllBulletTable - 1)
        if(item ~= nil)then
            return self.m_AllAdvanceView.m_Panel:IsVisible(item.m_Widget)
        end
    end
end

function CLuaDanMuViewWnd:IsClientLastVisible()
    if(#self.m_ClientBulletTable == 0)then
        return false
    else
        local item = self.m_ClientAdvanceView:GetItemAtRow(#self.m_ClientBulletTable - 1)
        if(item ~= nil)then
            return self.m_ClientAdvanceView.m_Panel:IsVisible(item.m_Widget)
        end
    end
end


-- --测试
-- function CLuaDanMuViewWnd:InitTestWnd()
--     local wnd = self.transform:Find("HorizontalTest").gameObject:GetComponent(typeof(QnAdvanceTableView))
--     wnd.m_DataSource = DefaultTableViewDataSource.Create(
--         function() return 100 end,
--         function(item,row) 
--             local label = item.gameObject:GetComponent(typeof(UILabel))
--             local str = "TEST"
--             local times = math.mod(row, 2)
--             for i = 0, times, 1 do
--                 str = str.."TEST"
--             end
--             str = str..row
--             label.text = str
--             if(times == 0)then
--                 label.color = Color.green
--             elseif(times == 1)then
--                 label.color = Color.red 
--             end
--         end
--     )
--     wnd:ReloadData(true, true)
--     wnd:ScrollToRow(0)
-- end
