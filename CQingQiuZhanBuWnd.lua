-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuZhanBuWnd = import "L10.UI.CQingQiuZhanBuWnd"
local Divination_QianWen = import "L10.Game.Divination_QianWen"
local Object1 = import "UnityEngine.Object"
local SoundManager = import "SoundManager"
local Time = import "UnityEngine.Time"
local Vector3 = import "UnityEngine.Vector3"
local CWinBottomPanel = import "L10.UI.CWinBottomPanel"
CQingQiuZhanBuWnd.m_Init_CS2LuaHook = function (this) 
    local HorizontalLabel = this.transform:Find("Ani/QianYu/HorizontalLabel"):GetComponent(typeof(UILabel))
    HorizontalLabel.gameObject:SetActive(not LocalString.isCN)
    this.qianyuLabel.gameObject:SetActive(LocalString.isCN)
    local qianyuSelectLabel = LocalString.isCN and this.qianyuLabel or HorizontalLabel
    
    
    this.isShaked = false
    this:SamleAni(this.qianyuAni, this.QianYuClipName, 0)
    this:SamleAni(this.qiantongAni, this.QianTongClipName, 0)
    this:SamleAni(this.qianAni, this.QianClipName, 0)
    this.shareBtn:SetActive(false)

    local data = Divination_QianWen.GetData(CQingQiuMgr.Inst.m_ZhanBuId)
    if data ~= nil then
        this.qianName:LoadMaterial(data.NameIcon)
        qianyuSelectLabel.text = LocalString.isCN and data.QianWenContent or string.gsub(data.QianWenContent, "#r", "\n")
        do
            local i = 0 local indexCnt = data.TaoHuaIndex local objCnt = this.m_TaoHuaObj.Length
            while i < objCnt do
                this.m_TaoHuaObj[i]:SetActive(i < indexCnt)
                i = i + 1
            end
        end
    end
    this.m_LeftObj:SetActive(false)
    local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
    local default
    if gameServer == nil then
        default = nil
    else
        default = gameServer.name
    end
    this.m_ServerNameLabel.text = default
    if CClientMainPlayer.Inst ~= nil then
        this.m_CharacterNameLabel.text = CClientMainPlayer.Inst.Name
    end
    if not CommonDefs.IS_VN_CLIENT then
        this:GenerateQR(CWinBottomPanel.m_QRCodeUrl)
    end
end
CQingQiuZhanBuWnd.m_SamleAni_CS2LuaHook = function (this, anim, clipName, time) 

    anim:get_Item(clipName).time = time
    anim:get_Item(clipName).enabled = true
    anim:get_Item(clipName).weight = 1
    anim:Sample()
    anim:get_Item(clipName).enabled = false
end
--CQingQiuZhanBuWnd.m_GenerateQR_CS2LuaHook = function (this, data) 
--    --设置二维码大小
--    if this.m_QRImage == nil then
--        this.m_QRImage = CreateFromClass(Texture2D, CQingQiuZhanBuWnd.s_ImageSize, CQingQiuZhanBuWnd.s_ImageSize)
--    end
--    --二维码边框
--    local BIT
--
--    local hints = CreateFromClass(MakeGenericClass(Dictionary, EncodeHintType, Object))
--
--    --设置编码方式
--    CommonDefs.DictAdd(hints, typeof(EncodeHintType), EncodeHintType.CHARACTER_SET, typeof(Object), "UTF-8")
--    CommonDefs.DictAdd(hints, typeof(EncodeHintType), EncodeHintType.MARGIN, typeof(Int32), CQingQiuZhanBuWnd.s_ImageMargin)
--
--    BIT = CreateFromClass(MultiFormatWriter):encode(data, BarcodeFormat.QR_CODE, CQingQiuZhanBuWnd.s_ImageSize, CQingQiuZhanBuWnd.s_ImageSize, hints)
--    local width = BIT.Width
--    local height = BIT.Width
--
--    do
--        local x = 0
--        while x < height do
--            do
--                local y = 0
--                while y < width do
--                    if BIT:get_Item(x) then
--                        this.m_QRImage:SetPixel(y, x, Color.black)
--                    else
--                        this.m_QRImage:SetPixel(y, x, Color.white)
--                    end
--                    y = y + 1
--                end
--            end
--            x = x + 1
--        end
--    end
--    this.m_QRImage:Apply()
--
--    this.m_DownloadQRCodeTex.mainTexture = this.m_QRImage
--end
CQingQiuZhanBuWnd.m_ShowShareBtn_CS2LuaHook = function (this) 
    if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
        this.shareBtn:SetActive(false)
    else
        this.shareBtn:SetActive(true)
    end
    this.m_IsShaking = true
end
CQingQiuZhanBuWnd.m_OnShareButtonClick_CS2LuaHook = function (this, go) 

    --限制按钮点击频率
    if CQingQiuZhanBuWnd.s_EnableRestrictEvaluateFrequency then
        if Time.realtimeSinceStartup - this.m_LastEvaluateTime < this.m_EvaluateInterval then
            --MessageMgr.Inst.ShowMessage("RPC_TOO_FREQUENT");
            return
        end
        this.m_LastEvaluateTime = Time.realtimeSinceStartup
    end

    this.shareBtn:SetActive(false)
    this.m_LeftObj:SetActive(true)

    this:StartCoroutine(this:DoShare())
end
CQingQiuZhanBuWnd.m_ClearTexture_CS2LuaHook = function (this) 
    if this.m_CachedTexture ~= nil then
        Object1.Destroy(this.m_CachedTexture)
    end
end
CQingQiuZhanBuWnd.m_OnPlayerShakeDevice_CS2LuaHook = function (this) 

    if this.isShaked then
        return
    end
    this.isShaked = true

    this:StartShake()
end
CQingQiuZhanBuWnd.m_StartShake_CS2LuaHook = function (this) 
    this.qianyuAni:get_Item(this.QianYuClipName).time = 0
    this.qiantongAni:get_Item(this.QianTongClipName).time = 0
    this.qianAni:get_Item(this.QianClipName).time = 0
    this.qiantongAni:Play()
    this.qianyuAni:Play()
    this.qianAni:Play()
    if not CQingQiuZhanBuWnd.s_EnableAnimCallback then
        SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.YuanDanYaoQianound, Vector3.zero, nil, 0)
    end
end
