local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSigninMgr     = import "L10.Game.CSigninMgr"

if rawget(_G, "LuaWorldCup2022Mgr") then
    g_ScriptEvent:RemoveListener("MainPlayerDestroyed", LuaWorldCup2022Mgr, "OnMainPlayerDestroyed")
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaWorldCup2022Mgr, "OnGasDisconnect")
end

LuaWorldCup2022Mgr = {}
LuaWorldCup2022Mgr.GJZLInfo = {}
LuaWorldCup2022Mgr.lotteryInfo = {}

g_ScriptEvent:AddListener("MainPlayerDestroyed", LuaWorldCup2022Mgr, "OnMainPlayerDestroyed")
g_ScriptEvent:AddListener("GasDisconnect", LuaWorldCup2022Mgr, "OnGasDisconnect")

function LuaWorldCup2022Mgr:OnMainPlayerDestroyed()
    self.GJZLInfo = {}
end

function LuaWorldCup2022Mgr:OnGasDisconnect()
    self.lotteryInfo = {}
    self.lotteryInfo.odds = {}
    self.lotteryInfo.groupRank = 0
    self.lotteryInfo.taotaiStage = 0
end

--#region 冠军之路

-- 同步阶段
function LuaWorldCup2022Mgr:GJZL_SyncStage(stage, countDownTime)
    self.GJZLInfo.stage = stage
    self.GJZLInfo.countDownTime = countDownTime
    g_ScriptEvent:BroadcastInLua("GJZL_SyncStage")
end

-- 同步进度
function LuaWorldCup2022Mgr:GJZL_SyncProgress(progress, fullProgress)
    self.GJZLInfo.progress = progress
    self.GJZLInfo.fullProgress = fullProgress
    g_ScriptEvent:BroadcastInLua("GJZL_SyncProgress")
end

-- 在玩法中
function LuaWorldCup2022Mgr:IsInGamePlay()
    if not CClientMainPlayer.Inst then return false end
	return CClientMainPlayer.Inst.PlayProp.PlayId == WorldCup2022_GJZLSetting.GetData().GamePlayId
end

-- 同步位置选择
function LuaWorldCup2022Mgr:GJZL_SyncSelectPosition(posInfo_U)
    self.GJZLInfo.posInfo = g_MessagePack.unpack(posInfo_U)
    g_ScriptEvent:BroadcastInLua("GJZL_SyncSelectPosition")
end

-- 同步玩家自己选择位置
function LuaWorldCup2022Mgr:GJZL_SelectPositionSuccess(myPos)
    self.GJZLInfo.myPos = myPos
    g_ScriptEvent:BroadcastInLua("GJZL_SelectPositionSuccess")
end

-- 打开选择界面
function LuaWorldCup2022Mgr:OpenGJZLSelectWnd(data)
    local rawData = g_MessagePack.unpack(data)
    self.GJZLInfo.posInfo = rawData[1]
    self.GJZLInfo.selectEndTimeStamp = rawData[2]
    CUIManager.ShowUI(CLuaUIResources.WorldCup2022GJZLSelectWnd)
end

-- 触发冠军之路技能引导
function LuaWorldCup2022Mgr:TryTriggerGJZLSkillGuide(skillId, buttonId)
    if buttonId ~= 5 then return end

    local zhongFengSkillId = WorldCup2022_GJZLPlace.GetData(1).SkillId
    local bianFengSkillId = WorldCup2022_GJZLPlace.GetData(2).SkillId
    local zhongChangSkillId = WorldCup2022_GJZLPlace.GetData(4).SkillId

    if skillId == zhongFengSkillId then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WorldCupGJZLZhongFengSkillGuide)
    elseif skillId == bianFengSkillId then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WorldCupGJZLBianFengSkillGuide)
    elseif skillId == zhongChangSkillId then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WorldCupGJZLZhongChangSkillGuide)
    end
end

--#endregion 冠军之路

--#region 竞彩

-- 竞彩投注结果
EnumWorldCupJingCaiVoteOption = {
    HostTeamWin = "1", -- 主队胜
    HostTeamLose = "2", -- 主队负
    Draw = "3", -- 平局
}

LuaWorldCup2022Mgr.lotterySelectInfo = {}
LuaWorldCup2022Mgr.lotteryInfo.odds = {}
LuaWorldCup2022Mgr.lotteryInfo.groupRank = 0
LuaWorldCup2022Mgr.lotteryInfo.taotaiStage = 0

function LuaWorldCup2022Mgr:UpdateAllJingCaiData(allPeriodResultData_U, allRoundResultData_U, odds_U, jingCaiData_U, groupRank, taotaiStage)
    self.lotteryInfo.allPeriodResultData = g_MessagePack.unpack(allPeriodResultData_U)
    self.lotteryInfo.allRoundResultData = g_MessagePack.unpack(allRoundResultData_U)
    self.lotteryInfo.odds = g_MessagePack.unpack(odds_U)
    self.lotteryInfo.jingCaiData = g_MessagePack.unpack(jingCaiData_U)
    self.lotteryInfo.groupRank = groupRank
    self.lotteryInfo.taotaiStage = taotaiStage

    self:UpdateLotteryRedDot()
    g_ScriptEvent:BroadcastInLua("WorldCupJingCai_UpdateAllJingCaiData")
end

function LuaWorldCup2022Mgr:SendSelfJingCaiData(jingCaiData_U)
    self.lotteryInfo.jingCaiData = g_MessagePack.unpack(jingCaiData_U)
    self:UpdateLotteryRedDot()
end

function LuaWorldCup2022Mgr:SendPeriodAndRoundData(allPeriodResultData_U, allRoundResultData_U)
    self.lotteryInfo.allPeriodResultData = g_MessagePack.unpack(allPeriodResultData_U)
    self.lotteryInfo.allRoundResultData = g_MessagePack.unpack(allRoundResultData_U)
    self:UpdateLotteryRedDot()
    g_ScriptEvent:BroadcastInLua("WorldCupJingCai_SendPeriodAndRoundData")
end

function LuaWorldCup2022Mgr:SendPeriodRoundStageData(allPeriodResultData_U, allRoundResultData_U, taotaiStage)
    self.lotteryInfo.allPeriodResultData = g_MessagePack.unpack(allPeriodResultData_U)
    self.lotteryInfo.allRoundResultData = g_MessagePack.unpack(allRoundResultData_U)
    self.lotteryInfo.taotaiStage = taotaiStage
    self:UpdateLotteryRedDot()
end

-- 更新竞猜红点
function LuaWorldCup2022Mgr:UpdateLotteryRedDot()
    if not self.lotteryInfo.jingCaiData or not self.lotteryInfo.allPeriodResultData then return end

    local redDotChange = false
    local hasLotteryAward = false
    for id, data in pairs(self.lotteryInfo.jingCaiData.m_JingCai) do
        if data[4] == 0 then
            local result = self.lotteryInfo.allPeriodResultData[id]
            if result and tostring(result) == tostring(data[2]) then
                hasLotteryAward = true
                break
            end
        end
    end
    if not self.lotteryInfo.hasLotteryAward or self.lotteryInfo.hasLotteryAward ~= hasLotteryAward then
        self.lotteryInfo.hasLotteryAward = hasLotteryAward
        redDotChange = true
    end

    local hasHomeTeamAward = false
    local homeTeamId = self.lotteryInfo.jingCaiData.m_HomeTeamId
    local taotaiStage = self.lotteryInfo.taotaiStage
    if homeTeamId > 0 then
        local curStage = 1
        if taotaiStage > 0 then
            curStage = taotaiStage
        else
            for id, data in pairs(self.lotteryInfo.allRoundResultData) do
                if data[1] == homeTeamId or data[2] == homeTeamId then
                    WorldCup_HomeTeamReward.Foreach(function(key, value)
                        local range = value.MatchRange
                        local needStage = value.NeedStage
                        if id >= range[0] and id <= range[1] then
                            if needStage == 0 then
                                curStage = math.max(curStage, key)
                            end
                        end
                    end)
                end
            end
        end
        self.lotteryInfo.curStage = curStage
        for i = 1, curStage do
            if bit.band(self.lotteryInfo.jingCaiData.m_LastRoundId, bit.lshift(1, i)) == 0 then
                hasHomeTeamAward = true
                break
            end
        end
    end
    if not self.lotteryInfo.hasHomeTeamAward or self.lotteryInfo.hasHomeTeamAward ~= hasHomeTeamAward then
        self.lotteryInfo.hasHomeTeamAward = hasHomeTeamAward
        redDotChange = true
    end

    if redDotChange then g_ScriptEvent:BroadcastInLua("WorldCup2022UpdateLotteryRedDot") end
end

function LuaWorldCup2022Mgr:JingCaiVoteSuccess(pid, value, num, allPeriodResultData_U, allRoundResultData_U, odds_U)
    self.lotteryInfo.allPeriodResultData = g_MessagePack.unpack(allPeriodResultData_U)
    self.lotteryInfo.allRoundResultData = g_MessagePack.unpack(allRoundResultData_U)
    self.lotteryInfo.odds = g_MessagePack.unpack(odds_U)
    if not self.lotteryInfo.jingCaiData.m_JingCai then
        self.lotteryInfo.jingCaiData.m_JingCai = {}
    end
    self.lotteryInfo.jingCaiData.m_JingCai[pid] = {0, tostring(value), num, 0, 0}

    self:UpdateLotteryRedDot()
    g_ScriptEvent:BroadcastInLua("WorldCup2022JingCaiVoteSuccess")

    if CUIManager.IsLoaded(CLuaUIResources.WorldCup2022LotterySelectWnd) and self.lotterySelectInfo.type == "lottery"
        and self.lotterySelectInfo.playId == pid then
            CUIManager.CloseUI(CLuaUIResources.WorldCup2022LotterySelectWnd)
    end
end

function LuaWorldCup2022Mgr:OnReceiveJingCaiAwardSuccess()
    g_ScriptEvent:BroadcastInLua("WorldCup2022OnReceiveJingCaiAwardSuccess")
end

-- 淘汰结果
function LuaWorldCup2022Mgr:JingCaiReplyTaoTaiInfoResult(taoTaiInfo_U)
    self.lotteryInfo.taoTaiInfo = g_MessagePack.unpack(taoTaiInfo_U)
    local jingCaiData = self.lotteryInfo.jingCaiData
    if jingCaiData and jingCaiData.m_HomeTeamId > 0 then
        for k, v in pairs(self.lotteryInfo.taoTaiInfo) do
            if k == jingCaiData.m_HomeTeamId then
                self.lotteryInfo.taotaiStage = v
                break
            end
        end
        self:UpdateLotteryRedDot()
    end

    g_ScriptEvent:BroadcastInLua("WorldCupJingCai_ReplyTaoTaiInfoResult")
end

-- 打开竞彩选择界面
function LuaWorldCup2022Mgr:ShowLotterySelectWnd(playId)
    self.lotterySelectInfo = {}
    self.lotterySelectInfo.type = "lottery"
    self.lotterySelectInfo.playId = playId
    CUIManager.ShowUI(CLuaUIResources.WorldCup2022LotterySelectWnd)
end

-- 打开主队选择界面
function LuaWorldCup2022Mgr:ShowHomeTeamSelectWnd(teamId)
    self.lotterySelectInfo = {}
    self.lotterySelectInfo.type = "homeTeam"
    self.lotterySelectInfo.teamId = teamId
    CUIManager.ShowUI(CLuaUIResources.WorldCup2022LotterySelectWnd)
end

-- 获取当前时间
function LuaWorldCup2022Mgr:GetCurrentTime()
    return CServerTimeMgr.Inst.timeStamp
end

--#endregion 竞彩

--#region 世界杯活动主页

-- 任务开启
function LuaWorldCup2022Mgr:IsTaskOpen(taskId)
    local jieRiId = WorldCup2022_Setting.GetData().JieRiId

    local openTaskIds = CLuaScheduleMgr.m_OpenFestivalTasks[jieRiId]
    if not openTaskIds then return false end

    for _, val in pairs(openTaskIds) do
        if val == taskId then
            return true
        end
    end
    return false
end

function LuaWorldCup2022Mgr:WorldCup2022GJZLCheck()
    if not CClientMainPlayer.Inst or CClientMainPlayer.Inst.MaxLevel < 60 then return false end

    local times = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWorldCup2022GJZL_RewardTime)
    local maxCount = WorldCup2022_GJZLSetting.GetData().WeeklyMaxCount
    if times >= maxCount then return false end

    local dayOfWeek = EnumToInt(CServerTimeMgr.Inst:GetZone8Time().DayOfWeek)
    if dayOfWeek ~= 0 and dayOfWeek ~= 5 and dayOfWeek ~= 6 then return false end

    local taskId = WorldCup2022_Setting.GetData().GuanJunZhiLuTaskId
    return self:IsTaskOpen(taskId[0]) or self:IsTaskOpen(taskId[1])
end

function LuaWorldCup2022Mgr:WorldCup2022JZLYCheck()
    if not CClientMainPlayer.Inst or CClientMainPlayer.Inst.MaxLevel < CuJu_Setting.GetData().SignUpGradeLimit then
        return false
    end

    local activityTime = WorldCup2022_Setting.GetData().JueZhanLvYinActivityTime
    local startHour, startMin, endHour, endMin = string.match(activityTime, "(%d+):(%d+)-(%d+):(%d+)")
    local startTime = startHour * 3600 + startMin * 60
    local endTime = endHour * 3600 + endMin * 60
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local nowTime = (now.Hour * 60 + now.Minute) * 60 + now.Second
    if nowTime <= startTime or nowTime >= endTime then return false end

    local taskId = WorldCup2022_Setting.GetData().JueZhanLvYinTaskId
    return self:IsTaskOpen(taskId[0]) or self:IsTaskOpen(taskId[1])
end

function LuaWorldCup2022Mgr:IsTianTianJingCaiOpen()
    return self:IsTaskOpen(WorldCup2022_Setting.GetData().TianTianJingCaiTaskId)
end

function LuaWorldCup2022Mgr:WorldCup2022TTJCCheck()
    return self:IsTianTianJingCaiOpen() and (self.lotteryInfo.hasLotteryAward or self.lotteryInfo.hasHomeTeamAward)
end

function LuaWorldCup2022Mgr:IsQianDaoYouLiOpen()
    return self:IsTaskOpen(WorldCup2022_Setting.GetData().QianDaoYouLiTaskId)
end

function LuaWorldCup2022Mgr:WorldCup2022QDYLCheck()
    if not self:IsQianDaoYouLiOpen() then return false end

    local holiday = CSigninMgr.Inst.holidayInfo
	if holiday and holiday.showSignin and not holiday.hasSignGift and holiday.todayInfo and holiday.todayInfo.Open == 1 then
		return true
	else
		return false
	end
end

--#endregion 世界杯活动主页
