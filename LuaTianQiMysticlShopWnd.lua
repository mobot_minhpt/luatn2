require("common/common_include")
local Gac2Gas=import "L10.Game.Gac2Gas"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local TianQiMysticalShop_SaledItem = import "L10.Game.TianQiMysticalShop_SaledItem"
local TianQiMysticalShop_Setting = import "L10.Game.TianQiMysticalShop_Setting"
local DelegateFactory = import "DelegateFactory"
local CShopMallGoodsItem = import "L10.UI.CShopMallGoodsItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local ShopMallTemlate = import "L10.UI.ShopMallTemlate"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CShopMallItemPreviewer = import "L10.UI.CShopMallItemPreviewer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local LocalString = import "LocalString"
local PlayerSettings = import "L10.Game.PlayerSettings"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MessageMgr = import "L10.Game.MessageMgr"
local CItem=import "L10.Game.CItem"
local CExpressionActionMgr = import  "L10.Game.CExpressionActionMgr"


CLuaTianQiMysticlShopWnd=class()
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_TableView")
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_ItemPreviewer")
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_BuyCountButton")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_MoneyCtrl")
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_BuyButton")
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_RefreshButton")
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_LeftTimeLabel")
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_AddTimeButton")
RegistClassMember(CLuaTianQiMysticlShopWnd,"m_CloseBtn")

RegistClassMember(CLuaTianQiMysticlShopWnd, "m_CurrentSelectedItem")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_Ids")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_Items")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_ItemMoneyTypes")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_ItemScoreTypes")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_TimeStamp")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_CountDownTick")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_HasOpenedDelayMessage")
RegistClassMember(CLuaTianQiMysticlShopWnd, "m_ItemId2ExpressionId")
RegistChildComponent(CLuaTianQiMysticlShopWnd,"m_SettingButton","SettingButton", GameObject)

TianQiShopWnd_TimeStamp = 0
TianQiShopWnd_ItemData = {}
TianQiShopWnd_PriceData = {}
TianQiShopWnd_ExpressionId = {}

function CLuaTianQiMysticlShopWnd:Init()
	self.m_BuyCountButton = self.m_BuyCountButton:GetComponent(typeof(QnAddSubAndInputButton))
	self.m_MoneyCtrl = self.m_MoneyCtrl:GetComponent(typeof(CCurentMoneyCtrl))
	self.m_ItemPreviewer = self.m_ItemPreviewer:GetComponent(typeof(CShopMallItemPreviewer))
	self.m_CurrentSelectedItem = 0
	self.m_Items = {}
	self.m_Ids = {}
	self.m_ItemId2ExpressionId = {}
	self.m_ItemMoneyTypes = {}
	self.m_ItemScoreTypes = {}
	UIEventListener.Get(self.m_SettingButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnSettingButtonClicked()
    end)
	local itemsData = TianQiShopWnd_ItemData
	local pricesData = TianQiShopWnd_PriceData
	self.m_TimeStamp = TianQiShopWnd_TimeStamp
	local TimeLimitItem = TianQiMysticalShop_Setting.GetData().TimeLimitItem

	--CommonDefs.DictIterate(itemsData, DelegateFactory.Action_object_object(
	--	function(key, value)
		for key, value in pairs(itemsData) do
			local data = TianQiMysticalShop_SaledItem.GetData(key)
			local itemTemplate = ShopMallTemlate()
			itemTemplate.ItemId = data.ItemId
			--价格折扣设置
			if pricesData[data.ID] < data.CostNum then
				itemTemplate.Discount = 10 * pricesData[data.ID]/data.CostNum
			else
				itemTemplate.Discount = 0;
			end
			itemTemplate.Price = pricesData[data.ID]
			itemTemplate.AvalibleCount = value
			itemTemplate.Status = 0
			table.insert(self.m_Items, itemTemplate)
			table.insert(self.m_Ids, data.ID)
			table.insert(self.m_ItemMoneyTypes, data.CostType)
			table.insert(self.m_ItemScoreTypes, data.ScoreType)
			self.m_ItemId2ExpressionId[data.ItemId] = TianQiShopWnd_ExpressionId[key] or 0
		end 
	--))

	if self.m_TableView.m_DataSource == nil then

		local OnItemAt = function (template, row)
			local item = template.gameObject:GetComponent(typeof(CShopMallGoodsItem))
			local shopMallTemlate = self.m_Items[row + 1]
			item:UpdateData(shopMallTemlate, self.m_ItemMoneyTypes[row + 1], true, false)
			item.m_NameLabel.Color = CItem.GetColor(shopMallTemlate.ItemId)

			if item.m_CurrencySprite and self.m_ItemMoneyTypes[row + 1] == EnumToInt(EnumMoneyType.Score) then
				item.m_CurrencySprite.spriteName = Money.GetIconName(EnumMoneyType.Score, CommonDefs.ConvertIntToEnum(typeof(EnumPlayScoreKey), self.m_ItemScoreTypes[row + 1]))
			end
			
			CommonDefs.GetComponent_Component_Type(item.transform:Find("TimeLimit"), typeof(UILabel)).gameObject:SetActive(false)

			if shopMallTemlate.Discount == 0 then
				local istimelimit = false
				if TimeLimitItem and CommonDefs.DictContains(TimeLimitItem, typeof(uint), shopMallTemlate.ItemId) then
					istimelimit = true
				end
				if istimelimit then
					CommonDefs.GetComponent_Component_Type(item.transform:Find("TimeLimit"), typeof(UILabel)).gameObject:SetActive(true)
				end
			end

			local expressionGroupId = self.m_ItemId2ExpressionId[shopMallTemlate.ItemId]
			if  expressionGroupId > 0 then

				if CExpressionActionMgr.Inst.UnLockGroupInfo[expressionGroupId] and CExpressionActionMgr.Inst.UnLockGroupInfo[expressionGroupId] == 1 then
					CommonDefs.GetComponent_Component_Type(item.transform:Find("Texture/Soldout"), typeof(UISprite)).gameObject:SetActive(true)
					CommonDefs.GetComponent_Component_Type(item.transform:Find("Texture/Soldout/Label"), typeof(UILabel)).text = LocalString.GetString("已学会")
				end
			end
		end

		self.m_TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(#self.m_Items, OnItemAt)
	end
	self.m_AddTimeButton.OnClick = DelegateFactory.Action_QnButton(function(button) self:OnAddTimeButtonClick(button) end)
	self.m_RefreshButton.OnClick = DelegateFactory.Action_QnButton(function(button) self:OnRefreshButtonClick(button) end)
	self.m_BuyButton.OnClick = DelegateFactory.Action_QnButton(function(button) self:OnBuyButtonClick(button) end)
 	
 	UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function(button) self:OnCloseButtonClick(button) end)


	self:updateCountDown()
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
	self.m_CountDownTick = RegisterTick(function() self:updateCountDown() end, 1000)
	self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row) self:OnItemClick(row) end)
	self.m_TableView:ReloadData(true, false)
	self.m_TableView:SetSelectRow(0, true)
end

function CLuaTianQiMysticlShopWnd:OnSettingButtonClicked()
	CUIManager.ShowUI(CLuaUIResources.TianQiMysticalShopSettingWnd)
end

function CLuaTianQiMysticlShopWnd:HasRaleItem()
	local hasRaleItem = false
	local itemsData = TianQiShopWnd_ItemData
	for key, value in pairs(itemsData) do
		local data = TianQiMysticalShop_SaledItem.GetData(key)
		--记录是否有稀有物品
		if CClientMainPlayer.Inst then
			local specialItemConfirmID = data.SpecialItemConfirmID
			local specialItemConfirmData = TianQiMysticalShop_SpecialItemConfirm.GetData(specialItemConfirmID)
			if specialItemConfirmData and not CClientMainPlayer.Inst.PlayProp.TianQiSpecialItemSkipConfirm:GetBit(specialItemConfirmID) 
				and (data.SpecialItem > 0) then
				hasRaleItem = true
			end
		end
	end
	return hasRaleItem
end

--确认延长时间
function CLuaTianQiMysticlShopWnd:confirmAddTime()
	local cost = TianQiMysticalShop_Setting.GetData().ShopWndLast[0]
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.TianQiJiFenData.ShangBanNian  + CClientMainPlayer.Inst.PlayProp.TianQiJiFenData.XiaBanNian >= cost then
		Gac2Gas.ExtendTianQiShopWnd()
	else
		MessageMgr.Inst:ShowMessage("TIANQI_JIFEN_NOT_ENOUGH",{})
	end
end

function CLuaTianQiMysticlShopWnd:OnAddTimeButtonClick(button)
	local msg = LocalString.GetString("是否消耗积分延长时间？")
	local cost = TianQiMysticalShop_Setting.GetData().ShopWndLast[0]
	local okAction = DelegateFactory.Action(function() self:confirmAddTime() end)
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.Score, msg, cost, okAction, nil, false, true, EnumPlayScoreKey.TianQiJiFen, false)
end

--确认刷新
function CLuaTianQiMysticlShopWnd:comfirmRefresh()
	if self:HasRaleItem() then
		local msg = g_MessageMgr:FormatMessage("TIANQI_SHOP_CLOSE_WND_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
			function()
				Gac2Gas.OpenSecretShop(EnumSecretShopType.eTianQiShop)
			end
		), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	else
		Gac2Gas.OpenSecretShop(EnumSecretShopType.eTianQiShop)
	end
end

--刷新按钮点击
function CLuaTianQiMysticlShopWnd:OnRefreshButtonClick(button)
	if PlayerSettings.NotNoticeTianQiJiFen then
		self:comfirmRefresh()
	else
		local msg = LocalString.GetString("是否消耗积分用以刷新商店")
		local cost = TianQiMysticalShop_Setting.GetData().OpenShopCost[0]
		local okAction = DelegateFactory.Action(function() self:comfirmRefresh() end)
		QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.Score, msg, cost, okAction, nil, false, true, EnumPlayScoreKey.TianQiJiFen, true)
	end
end
--购买按钮点击
function CLuaTianQiMysticlShopWnd:OnBuyButtonClick(button)
	if self.m_CurrentSelectedItem >= 0 and self.m_CurrentSelectedItem < #self.m_Items then
		Gac2Gas.RequestBuySecretShopItem(EnumSecretShopType.eTianQiShop, self.m_Ids[self.m_CurrentSelectedItem + 1])
	end
end

function CLuaTianQiMysticlShopWnd:OnCloseButtonClick(button)
	if self:HasRaleItem() then
		local msg = g_MessageMgr:FormatMessage("TIANQI_SHOP_CLOSE_WND_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
			function()
				CUIManager.CloseUI(CUIResources.TianQiMysticalShopWnd)
			end
		), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	else
		CUIManager.CloseUI(CUIResources.TianQiMysticalShopWnd)
	end
end


--更新倒计时
function CLuaTianQiMysticlShopWnd:updateCountDown()
	local deltaTime = math.floor(self.m_TimeStamp - CServerTimeMgr.Inst.timeStamp)
	if(deltaTime < 0) then
		CUIManager.CloseUI(CUIResources.TianQiMysticalShopWnd)
	else
		if deltaTime > 10 then
		 	self.m_HasOpenedDelayMessage = false
		else
			if not self.m_HasOpenedDelayMessage then
				self.m_HasOpenedDelayMessage = true
				local cost = TianQiMysticalShop_Setting.GetData().ShopWndLast[0]
				local msg = SafeStringFormat3(LocalString.GetString("商店即将关闭,是否使用%d积分来延时?"), cost)
				MessageWndManager.ShowConfirmMessage(msg, 5, false, DelegateFactory.Action(function() self:confirmAddTime() end), nil)
			end
		end
		local fen = math.floor(deltaTime / 60)
		local miao = deltaTime % 60
		if fen > 0 then
			self.m_LeftTimeLabel.text = SafeStringFormat3("[00ff00]%02d:%02d[-]", fen, miao)
		else
			self.m_LeftTimeLabel.text = SafeStringFormat3("[ff0000]%02d:%02d[-]", fen, miao)
		end
	end
end

--选中某件物品
function CLuaTianQiMysticlShopWnd:OnItemClick(row)
	self.m_CurrentSelectedItem = row
    self.m_BuyCountButton:SetValue(1, true)
    self.m_MoneyCtrl:SetType(self.m_ItemMoneyTypes[row + 1], self.m_ItemScoreTypes[row + 1], false)
    self:UpdateTotalPrice()
    self:UpdatePreview(row)
end

function CLuaTianQiMysticlShopWnd:UpdatePreview(row)
	local itemData = self.m_Items[row + 1]
	self.m_BuyCountButton:SetMinMax(1, 1, 1)
	self.m_ItemPreviewer:UpdateData(itemData, 0, "")
end

function CLuaTianQiMysticlShopWnd:UpdateTotalPrice(count)
	local totalprice = 0;
    if self.m_CurrentSelectedItem >=0 and self.m_CurrentSelectedItem < #self.m_Items then
        totalprice = self.m_Items[self.m_CurrentSelectedItem + 1].Price * self.m_BuyCountButton:GetValue()
    end
    self.m_MoneyCtrl:SetCost(totalprice)
end

function CLuaTianQiMysticlShopWnd:OnEnable()
	g_ScriptEvent:AddListener("BuyItemFromSecretShopSuccess", self, "BuyItemFromSecretShopSuccess")
end

function CLuaTianQiMysticlShopWnd:OnDisable()
	g_ScriptEvent:RemoveListener("BuyItemFromSecretShopSuccess", self, "BuyItemFromSecretShopSuccess")
end

function CLuaTianQiMysticlShopWnd:BuyItemFromSecretShopSuccess()
	local msg = LocalString.GetString("是否消耗积分用以刷新商店")
	local cost = TianQiMysticalShop_Setting.GetData().OpenShopCost[0]
	local okAction = DelegateFactory.Action(function() Gac2Gas.OpenSecretShop(EnumSecretShopType.eTianQiShop) end)
	local cancelAction = DelegateFactory.Action(function() CUIManager.CloseUI(CUIResources.TianQiMysticalShopWnd) end)
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.Score, msg, cost, okAction, cancelAction, false, true, EnumPlayScoreKey.TianQiJiFen, false)
end

function CLuaTianQiMysticlShopWnd:OnDestroy()
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end

	TianQiShopWnd_ItemData = nil
	TianQiShopWnd_PriceData = nil
	TianQiShopWnd_ExpressionId = nil
end

function Gas2Gac.OpenTianQiShopWnd(itemInfoTblUD)
	local list = itemInfoTblUD and MsgPackImpl.unpack(itemInfoTblUD)
	if not list then return end

	TianQiShopWnd_TimeStamp = list[0]

	TianQiShopWnd_ItemData = {}
	TianQiShopWnd_PriceData = {}
	TianQiShopWnd_ExpressionId = {}

	local to = list.Count - 1
	for i=1, to, 4 do
		local id = list[i]
		local count = list[i+1]
		TianQiShopWnd_ItemData[id] = count
		local price = list[i+2]

		TianQiShopWnd_PriceData[id] = price

		local expressionId = list[i+3]
		TianQiShopWnd_ExpressionId[id] = expressionId
		if expressionId > 0  then
		end
	end
	CUIManager.CloseUI(CUIResources.TianQiMysticalShop_Entry);
	CUIManager.ShowUI(CUIResources.TianQiMysticalShopWnd);
end

return CLuaTianQiMysticlShopWnd
