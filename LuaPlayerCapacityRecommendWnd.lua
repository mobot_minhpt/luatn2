local UICamera = import "UICamera"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CPlayerCapacityMgr = import "L10.Game.CPlayerCapacityMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumClass = import "L10.Game.EnumClass"
local CTrackMgr = import "L10.Game.CTrackMgr"

CLuaPlayerCapacityRecommendWnd = class()
RegistClassMember(CLuaPlayerCapacityRecommendWnd,"m_Table")
RegistClassMember(CLuaPlayerCapacityRecommendWnd,"m_ItemTemplate")
RegistClassMember(CLuaPlayerCapacityRecommendWnd,"m_SplitTemplate")
RegistClassMember(CLuaPlayerCapacityRecommendWnd,"m_Actions")

CLuaPlayerCapacityRecommendWnd.fromKey = ""

function CLuaPlayerCapacityRecommendWnd:Awake()
    self.m_ItemTemplate=FindChild(self.transform,"Item").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_Table=FindChild(self.transform,"Table").gameObject
    self.m_SplitTemplate = self.transform:Find("Anchor/SplitItem").gameObject
    self.m_SplitTemplate:SetActive(false)
end

function CLuaPlayerCapacityRecommendWnd:Init( )
    local sub = nil
    local key = CLuaPlayerCapacityRecommendWnd.fromKey
    for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
        if v.key==key then
            sub = v.sub
        end
    end

    self.m_Actions = {
        ["EquipValue_Basic"] = function()
            if CommonDefs.IS_CN_CLIENT and CLuaPlayerCapacityView.s_ShowEquipScoreImproveWnd then
                CPlayerCapacityMgr.Inst:ShowEquipScoreImproveWnd(CPlayerCapacityMgr.EnumEquipScoreImproveType.BasicScore)
            else
                CEquipmentProcessMgr.Show(0, 0)
            end
        end,
        ["EquipValue_Hole"] = function()
            if CommonDefs.IS_CN_CLIENT and CLuaPlayerCapacityView.s_ShowEquipScoreImproveWnd then
                CPlayerCapacityMgr.Inst:ShowEquipScoreImproveWnd(CPlayerCapacityMgr.EnumEquipScoreImproveType.HoleScore)
            else
                CEquipmentProcessMgr.Show(2, 0)
            end
        end,
        ["EquipValue_Intensify"] = function()
            if CommonDefs.IS_CN_CLIENT and CLuaPlayerCapacityView.s_ShowEquipScoreImproveWnd then
                CPlayerCapacityMgr.Inst:ShowEquipScoreImproveWnd(CPlayerCapacityMgr.EnumEquipScoreImproveType.IntensifyScore)
            else
                CEquipmentProcessMgr.Show(2, 0)
            end
        end,
        ["ShiMenValue_XiuWei"] = function()
            CSkillInfoMgr.ShowSkillUpgradeWnd()
        end,
        ["LingShouValue_ChuZhan"] = function()
            CLingShouMgr.TryOpenLingShouMainWnd()
        end,
    }

    if IsZongPaiIterationOpen() then
        self.m_Actions["ShiMenValue_MiShu"] = function()
            local isJoinedSect = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0
            if isJoinedSect then
                LuaZongMenMgr.m_MainWndTabIndex = 2
		        LuaZongMenMgr.m_ZongMenSkillViewIndex = 1
                CUIManager.ShowUI(CLuaUIResources.ZongMenMainWnd)
            else
                local msg = g_MessageMgr:FormatMessage("ZONGMEN_SKILLVIEW_SHIMENMISHU_UPGRADE_NEED_SECT")
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                    local npcId = tonumber(Menpai_Setting.GetData("YuanYiXianShengID").Value)
                    local sceneData = g_LuaUtil:StrSplit(Menpai_Setting.GetData("YuanYiXianShengXunLu").Value,",")
                    CTrackMgr.Inst:FindNPC(npcId, tonumber(sceneData[1]), tonumber(sceneData[2]), tonumber(sceneData[3]), nil, nil)
                end), nil, nil, nil, false)
            end
        end
    end

    if sub then
        for i,v in ipairs(sub) do
            local pass = true
            if v.condition then pass = v.condition() end--判断一下是否显示
            if pass then
                
                if self.m_Table.transform.childCount>0 then
                    local split = NGUITools.AddChild(self.m_Table,self.m_SplitTemplate)
                    split:SetActive(true)
                end
                local go = NGUITools.AddChild(self.m_Table,self.m_ItemTemplate)
                go:SetActive(true)

                local subkey = v[1]

                local button = go.transform:Find("Button").gameObject
                if not self.m_Actions[v[1]] then
                    button:SetActive(false)
                end
                if button.activeSelf then
                    self:CheckButtonVisibility(subkey, button)
                end

                UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(g)
                    local default = v[1]
                    if self.m_Actions[subkey] then
                        self.m_Actions[subkey]()
                    end
                end)

                local titleLabel = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
                titleLabel.text = v[2]

                local tabel = go.transform:Find("Table").gameObject
                local childTemplate = go.transform:Find("Template").gameObject
                childTemplate:SetActive(false)

                local curVal = CLuaPlayerCapacityMgr.GetValue(v[1])
                local haveRecommend, isHigh, recommendVal,percent = CLuaPlayerCapacityMgr.GetRecommendValue(v[1], curVal)

                local recommendLabel = go.transform:Find("RecommendLabel"):GetComponent(typeof(UILabel))

                if haveRecommend then
                    if isHigh then
                        recommendLabel.text = SafeStringFormat3( "[c][fffa00](%s)[-][/c]",LocalString.GetString("建议提升") )
                    else
                        recommendLabel.text = SafeStringFormat3( "[c][ff0000](%s)[-][/c]",LocalString.GetString("亟需提升") )
                    end
                else
                    recommendLabel.text = nil
                end

                local tips = CLuaPlayerCapacityMgr.GetRecommendDesc(v[1],isHigh)
                if tips then
                    for i,v in ipairs(tips) do
                        local child=NGUITools.AddChild(tabel,childTemplate)
                        child:SetActive(true)
                        local label = child.transform:Find("Label"):GetComponent(typeof(UILabel))
                        label.text = v
                        UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
                            local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
                            if url ~= nil then
                                CChatLinkMgr.ProcessLinkClick(url, nil)
                            end
                        end)
                    end
                    tabel:GetComponent(typeof(UITable)):Reposition()
                end
            end
        end
        self.m_Table:GetComponent(typeof(UITable)):Reposition()
    end
end
function CLuaPlayerCapacityRecommendWnd:CheckButtonVisibility(key, button)
    --留给以后新职业刚上的时候精灵没有推荐数据暂时屏蔽之需
    if key == "EquipValue_Basic" or key ==  "EquipValue_Hole" or key == "EquipValue_Intensify" then

      button.gameObject:SetActive(CClientMainPlayer.Inst~=nil and EnumToInt(CClientMainPlayer.Inst.Class) <= EnumToInt(EnumClass.DieKe)) --隐藏按钮
    end
end