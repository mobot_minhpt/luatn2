local UIAlertIcon = import "L10.UI.UIAlertIcon"
local UIDefaultTableViewCell = import "L10.UI.UIDefaultTableViewCell"
local CTaskMgr = import "L10.Game.CTaskMgr"

LuaTaskMasterListItem = class()

RegistClassMember(LuaTaskMasterListItem, "m_AlertIcon")     --红点提示
RegistClassMember(LuaTaskMasterListItem, "m_NameLabel")     --标题
RegistClassMember(LuaTaskMasterListItem, "m_VisibleIcon")   --可以显示在任务追踪处的标记

RegistClassMember(LuaTaskMasterListItem, "m_TableVeiwCell") -- UIDefaultTableViewCell组件

RegistClassMember(LuaTaskMasterListItem, "m_ItemType")      --类型，0 Row 1 Section
RegistClassMember(LuaTaskMasterListItem, "m_TaskIds")       --对应的任务ID
RegistClassMember(LuaTaskMasterListItem, "m_IsReceivedTask")--是否为已接任务

function LuaTaskMasterListItem:Awake()
    self.m_AlertIcon = self.transform:Find("Alert"):GetComponent(typeof(UIAlertIcon))
    self.m_NameLabel = self.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_VisibleIcon = self.transform:Find("VisibleIcon").gameObject
    self.m_TableVeiwCell = self.transform:GetComponent(typeof(UIDefaultTableViewCell))

    self.m_ItemType = 0
    self.m_TaskIds = {}
end

function LuaTaskMasterListItem:Init(args) --注意 这里被CTaskMasterView调用
    local type = args[0]
    local taskIds = args[1]
    local isReceivedTask = args[2] and args[2] or false
    self.m_ItemType = type
    self.m_TaskIds = {}
    for i=0,taskIds.Count-1 do
        table.insert(self.m_TaskIds, taskIds[i])
    end
    self.m_IsReceivedTask = isReceivedTask
    self:OnUpdateAcceptableTaskReadStatus()
    self:RefreshVisibleIcon()
end

function LuaTaskMasterListItem:OnEnable()
    self:OnUpdateAcceptableTaskReadStatus()
    g_ScriptEvent:AddListener("OnUpdateAcceptableTaskReadStatus", self, "OnUpdateAcceptableTaskReadStatus")
    g_ScriptEvent:AddListener("OnUpdateTaskVisibleInTrackPanelStatus", self, "OnUpdateTaskVisibleInTrackPanelStatus")
end

function LuaTaskMasterListItem:OnDisable()
    g_ScriptEvent:RemoveListener("OnUpdateAcceptableTaskReadStatus", self, "OnUpdateAcceptableTaskReadStatus")
    g_ScriptEvent:RemoveListener("OnUpdateTaskVisibleInTrackPanelStatus", self, "OnUpdateTaskVisibleInTrackPanelStatus")
end

function LuaTaskMasterListItem:OnUpdateAcceptableTaskReadStatus(args)
    if self.m_TaskIds == nil or #self.m_TaskIds == 0 then
        self.m_AlertIcon.Visible = false
    else
        for __,id in pairs(self.m_TaskIds) do
            if CTaskMgr.Inst:IsNewAccetableTask(id) then
                self.m_AlertIcon.Visible = true
                return
            end
        end
        self.m_AlertIcon.Visible = false
    end
end

function LuaTaskMasterListItem:OnUpdateTaskVisibleInTrackPanelStatus(taskId)
    if self.m_ItemType == 0 and self.m_TaskIds then
        for __,id in pairs(self.m_TaskIds) do
            if id == taskId then
                self:RefreshVisibleIcon()
                break
            end
        end
    end
end

function LuaTaskMasterListItem:RefreshVisibleIcon()
    if self.m_ItemType == 0 and self.m_TaskIds and self.m_IsReceivedTask then
        for __,id in pairs(self.m_TaskIds) do
            --第一个id即可
            local hiddenIds = CLuaPlayerSettings.GetHiddenInTaskTrackPanelIds()
            self.m_VisibleIcon:SetActive(not CLuaTaskMgr.HiddenInTaskTrackPanel(id, hiddenIds))
            break
        end
    else
        self.m_VisibleIcon:SetActive(false)
    end
end
