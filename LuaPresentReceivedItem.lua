require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local Item_Item = import "L10.Game.Item_Item"
local IdPartition = import "L10.Game.IdPartition"
local CItem = import "L10.Game.CItem"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CEquipment = import "L10.Game.CEquipment"
local DelegateFactory = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaPresentReceivedItem = class()
RegistClassMember(LuaPresentReceivedItem,"gameObject")
RegistClassMember(LuaPresentReceivedItem,"transform")
RegistClassMember(LuaPresentReceivedItem,"iconTexture")
RegistClassMember(LuaPresentReceivedItem,"amountLabel")
RegistClassMember(LuaPresentReceivedItem,"qualitySprite")
RegistClassMember(LuaPresentReceivedItem,"disableSprite")
RegistClassMember(LuaPresentReceivedItem,"ItemTemplateId")

function LuaPresentReceivedItem:Init(gameObject, itemTemplateId, count)
	self.gameObject = gameObject
	self.transform = gameObject.transform
	self.ItemTemplateId = itemTemplateId
	self.iconTexture = self.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	self.iconTexture:Clear()
	self.amountLabel = self.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
	self.amountLabel.text = nil
	self.qualitySprite = self.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
	self.disableSprite = self.transform:Find("DisableSprite"):GetComponent(typeof(UISprite))
	self.disableSprite.enabled = false

	if IdPartition.IdIsItem(itemTemplateId) then
		local item = Item_Item.GetData(itemTemplateId)
		if item then
			self.iconTexture:LoadMaterial(item.Icon)
			if count > 0 then
				self.amountLabel.text = tostring(count)
			else
				self.amountLabel.text = nil
			end
			self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
			self.disableSprite.enabled = (not CItem.GetMainPlayerIsFit(itemTemplateId))
		end
	elseif IdPartition.IdIsEquip(itemTemplateId) then
		local equip = EquipmentTemplate_Equip.GetData(itemTemplateId)
		if equip then
			self.iconTexture:LoadMaterial(equip.Icon)
			if count > 1 then
				self.amountLabel.text = tostring(count)
			else
				self.amountLabel.text = nil
			end
			self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumFromInt(EnumQualityType, equip.Color))
			self.disableSprite.enabled = (not CEquipment.GetMainPlayerIsFit(itemTemplateId))
		end
	end
	
	CommonDefs.AddOnClickListener(self.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go) end), false)
end

function LuaPresentReceivedItem:OnItemClick(go)
	if not self.ItemTemplateId then return end
	CItemInfoMgr.ShowLinkItemTemplateInfo(self.ItemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
end

return LuaPresentReceivedItem