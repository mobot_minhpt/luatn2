local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaFuLiQuan2022ExchangeWnd = class()

function LuaFuLiQuan2022ExchangeWnd:Awake()
    self:InitUIComponent()
    self:InitUIEvent()
    self:InitUIData()
    self:RefreshUIWidget()
end

function LuaFuLiQuan2022ExchangeWnd:Init()

end

function LuaFuLiQuan2022ExchangeWnd:InitUIComponent()
    self.cardObjectList = {}
    table.insert(self.cardObjectList, self.transform:Find("Anchor/CardContent/FirstCardButton").gameObject)
    table.insert(self.cardObjectList, self.transform:Find("Anchor/CardContent/SecondCardButton").gameObject)
    table.insert(self.cardObjectList, self.transform:Find("Anchor/CardContent/ThirdCardButton").gameObject)
    table.insert(self.cardObjectList, self.transform:Find("Anchor/CardContent/FourthCardButton").gameObject)

    self.exchangeButton = self.transform:Find("Anchor/ExchangeButton")
    
    self.remainNumberLabel = self.transform:Find("Anchor/RemainNumberLabel"):GetComponent(typeof(UILabel))
end

function LuaFuLiQuan2022ExchangeWnd:InitUIEvent()
    UIEventListener.Get(self.exchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickExchangeButton()
    end)
end

function LuaFuLiQuan2022ExchangeWnd:OnClickExchangeButton()
    local totalCount = CItemMgr.Inst:GetItemCount(self.specialCardItemId)
    if totalCount > 0 then
        -- 万能卡数量充足
        if self.formerSelectedCardIndex then
            -- 有选定一张卡牌
            local cardItemId = self.normalCardItemIdList[self.formerSelectedCardIndex]
            local cardItemData = Item_Item.GetData(cardItemId)
            local cardName = cardItemData and cardItemData.Name or ""
            local specialCardItemData = Item_Item.GetData(self.specialCardItemId)
            local specialCardName = specialCardItemData and specialCardItemData.Name or ""
            local msg = System.String.Format("您正在使用一张{0}兑换一张{1}，是否确认兑换？", specialCardName, cardName)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
                function()
                    Gac2Gas.RequestExchangeItems(108+self.formerSelectedCardIndex,1)
                end
            ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        else    
            -- 卡牌都没选, show Message
            g_MessageMgr:ShowMessage("FuliQuan_Exchange_Not_Select_Reminder")
        end
    else   
        -- 万能卡数量为零, show Message
        g_MessageMgr:ShowMessage("FuLiQuan_Exchange_No_SpecialCard_Reminder")
    end
end

function LuaFuLiQuan2022ExchangeWnd:InitUIData()
    local settingConfigData = Double11_Setting.GetData()
    self.specialCardItemId = settingConfigData.WannengCardItemId
    self.normalCardItemIdList = {settingConfigData.CardItemId[0], settingConfigData.CardItemId[1], settingConfigData.CardItemId[2], settingConfigData.CardItemId[3]}
    
    self.formerSelectedCardIndex = nil
end

function LuaFuLiQuan2022ExchangeWnd:RefreshUIWidget()
    for i = 1, #self.normalCardItemIdList do
        self:RefreshCardData(self.cardObjectList[i], self.normalCardItemIdList[i], i)
    end
    
    self.remainNumberLabel.text = CItemMgr.Inst:GetItemCount(self.specialCardItemId)
end

function LuaFuLiQuan2022ExchangeWnd:RefreshCardData(cardObject, cardItemId, cardIndex)
    local totalCount = CItemMgr.Inst:GetItemCount(cardItemId)
    local numberLabel = cardObject.transform:Find("Background/NumberLabel"):GetComponent(typeof(UILabel))
    numberLabel.text = LocalString.GetString("拥有  ") .. totalCount

    local showGrayState = (totalCount == 0)
    local bgObject = cardObject.transform:Find("bg_Texture").gameObject
    CUICommonDef.SetActive(bgObject, not showGrayState)

    UIEventListener.Get(cardObject).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.formerSelectedCardIndex and self.formerSelectedCardIndex ~= cardIndex then
            self.cardObjectList[self.formerSelectedCardIndex].transform:Find("selectbg_Texture").gameObject:SetActive(false)
        end
        cardObject.transform:Find("selectbg_Texture").gameObject:SetActive(true)
        self.formerSelectedCardIndex = cardIndex
    end)
end

function LuaFuLiQuan2022ExchangeWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnPlayerItemChange")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnPlayerItemChange")

end

function LuaFuLiQuan2022ExchangeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnPlayerItemChange")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnPlayerItemChange")
end

function LuaFuLiQuan2022ExchangeWnd:OnPlayerItemChange()
    self:RefreshUIWidget()
end
