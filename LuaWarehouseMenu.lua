local Vector3 = import "UnityEngine.Vector3"
local UIEventListener = import "UIEventListener"
local NGUITools = import "NGUITools"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local CWarehouseMenuItem = import "L10.UI.CWarehouseMenuItem"
local CUIResources = import "L10.UI.CUIResources"
local CUIManager = import "L10.UI.CUIManager"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"


CLuaWarehouseMenu = class()
RegistClassMember(CLuaWarehouseMenu,"bgSprite")
RegistClassMember(CLuaWarehouseMenu,"layoutGrid")
RegistClassMember(CLuaWarehouseMenu,"btnTemplate")
RegistClassMember(CLuaWarehouseMenu,"itemBtns")
RegistClassMember(CLuaWarehouseMenu,"m_Wnd")
RegistClassMember(CLuaWarehouseMenu,"storeRoomTemplate")

function CLuaWarehouseMenu:Awake()
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    self.bgSprite = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))
    self.layoutGrid = self.transform:Find("Anchor/Background/Grid"):GetComponent(typeof(UIGrid))
    self.btnTemplate = self.transform:Find("Anchor/Background/TemplateButton").gameObject
    self.btnTemplate:SetActive(false)
    self.storeRoomTemplate = self.transform:Find("Anchor/Background/StoreRoomTemplate").gameObject
    self.storeRoomTemplate:SetActive(false)
    self.itemBtns = {}
end

-- Auto Generated!!
function CLuaWarehouseMenu:Init( )
    Extensions.RemoveAllChildren(self.layoutGrid.transform)
    if CClientMainPlayer.Inst == nil then
        return
    end
    self.itemBtns = {}

    do
        local i = 0
        if CLuaWarehouseMenuMgr.UseStoreHouseTemplate then
        else
        end

        while i < #CLuaWarehouseMenuMgr.Items do
            local btnTemplate = CLuaWarehouseMenuMgr.UseStoreHouseTemplate and self.storeRoomTemplate or self.btnTemplate
            local child = NGUITools.AddChild(self.layoutGrid.gameObject, btnTemplate)
            child:SetActive(true)
            if CLuaWarehouseMenuMgr.UseStoreHouseTemplate then
                self:InitStoreRoomTemplate(child,CLuaWarehouseMenuMgr.Items[i + 1].text, CLuaWarehouseMenuMgr.Items[i + 1].locked)
            else
                local menuItem = CommonDefs.GetComponent_GameObject_Type(child, typeof(CWarehouseMenuItem))
                menuItem:Init(CLuaWarehouseMenuMgr.Items[i + 1].text, CLuaWarehouseMenuMgr.Items[i + 1].locked)
                menuItem.Selected = (i == CLuaWarehouseMenuMgr.DefaultSelectedIndex)
            end
            
            
            table.insert(self.itemBtns, child)
            UIEventListener.Get(child).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(child).onClick, DelegateFactory.VoidDelegate(
                function(go)
                    self:OnItemClick(go)
                end
            ), true)
            i = i + 1
        end
    end
    self.layoutGrid:Reposition()
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.layoutGrid.transform)
    self.bgSprite.height = math.floor((b.size.y + 20))
    self.bgSprite.width = math.floor((b.size.x + 20))
    self.layoutGrid.transform.localPosition = Vector3.zero
    local topLocalPos = self.bgSprite.transform.parent:InverseTransformPoint(CLuaWarehouseMenuMgr.TopWorldPos)
    self.bgSprite.transform.localPosition = Vector3(topLocalPos.x, topLocalPos.y - self.bgSprite.height * 0.5, 0)
end

function CLuaWarehouseMenu:OnItemClick( go) 
    do
        local i = 0
        while i < #self.itemBtns do
            if go == self.itemBtns[i + 1] then
                if CLuaWarehouseMenuMgr.Listener ~= nil then
                    CLuaWarehouseMenuMgr.Listener:OnMenuItemClick(i)
                end
                break
            end
            i = i + 1
        end
    end
end

function CLuaWarehouseMenu:InitStoreRoomTemplate(go,text,isLocked)
    local lockIcon = go.transform:Find("Icon")
    local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = text
    lockIcon.gameObject:SetActive(isLocked)
end

function CLuaWarehouseMenu:OnEnable( )
    if CLuaWarehouseMenuMgr.Listener ~= nil then
        CLuaWarehouseMenuMgr.Listener:OnMenuAppear()
    end
end
function CLuaWarehouseMenu:OnDisable( )
    if CLuaWarehouseMenuMgr.Listener ~= nil then
        CLuaWarehouseMenuMgr.Listener:OnMenuDisappear()
    end
end

CLuaWarehouseMenuMgr = {}
CLuaWarehouseMenuMgr.Listener = nil
CLuaWarehouseMenuMgr.Items = {}
CLuaWarehouseMenuMgr.DefaultSelectedIndex = 0
CLuaWarehouseMenuMgr.TopWorldPos = nil
CLuaWarehouseMenuMgr.UseStoreHouseTemplate = nil
--menu item data = {text, locked}

function CLuaWarehouseMenuMgr:ShowOrCloseMenu(defaultSelectedIndex, items, listener, topWorldPos,isStoreRoom)
    if(CUIManager.IsLoaded(CUIResources.WarehouseMenu))then
        CUIManager.CloseUI(CUIResources.WarehouseMenu)
        return
    end
    self.Items = items
    self.Listener = listener
    self.DefaultSelectedIndex = defaultSelectedIndex
    self.TopWorldPos = topWorldPos
    self.UseStoreHouseTemplate = isStoreRoom
    CUIManager.ShowUI(CUIResources.WarehouseMenu)
end

function CLuaWarehouseMenuMgr:CloseMenu()
    CUIManager.CloseUI(CUIResources.WarehouseMenu)
end

function CLuaWarehouseMenu:Update()
    self.m_Wnd:ClickThroughToClose()
end



