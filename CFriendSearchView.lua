-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFriendListItem = import "L10.UI.CFriendListItem"
local CFriendSearchListItem = import "L10.UI.CFriendSearchListItem"
local CFriendSearchView = import "L10.UI.CFriendSearchView"
local CIMMgr = import "L10.Game.CIMMgr"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CRelationshipBasicPlayerInfo = import "L10.Game.CRelationshipBasicPlayerInfo"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local FriendItemData = import "FriendItemData"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local String = import "System.String"
local UInt64 = import "System.UInt64"
CFriendSearchView.m_Start_CS2LuaHook = function (this) 
    this.friendItemTemplate:SetActive(false)
    this.searchBar.OnSearch = MakeDelegateFromCSFunction(this.OnSearch, MakeGenericClass(Action1, String), this)
    this.searchBar.OnChange = MakeDelegateFromCSFunction(this.OnChange, MakeGenericClass(Action1, String), this)
end
CFriendSearchView.m_OnEnable_CS2LuaHook = function (this) 
    this:Clear()
    EventManager.AddListenerInternal(EnumEventType.SearchPlayerSuccess, MakeDelegateFromCSFunction(this.OnReturnSearchSuccess, MakeGenericClass(Action1, MakeGenericClass(List, CRelationshipBasicPlayerInfo)), this))
    EventManager.AddListener(EnumEventType.SearchPlayerFail, MakeDelegateFromCSFunction(this.OnReturnSearchFail, Action0, this))
    EventManager.AddListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))

    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddFriend, MakeDelegateFromCSFunction(this.OnAddFriend, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddFriendSpecialId, MakeDelegateFromCSFunction(this.OnAddFriend, MakeGenericClass(Action1, UInt64), this))
end
CFriendSearchView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.SearchPlayerSuccess, MakeDelegateFromCSFunction(this.OnReturnSearchSuccess, MakeGenericClass(Action1, MakeGenericClass(List, CRelationshipBasicPlayerInfo)), this))
    EventManager.RemoveListener(EnumEventType.SearchPlayerFail, MakeDelegateFromCSFunction(this.OnReturnSearchFail, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))

    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddFriend, MakeDelegateFromCSFunction(this.OnAddFriend, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddFriendSpecialId, MakeDelegateFromCSFunction(this.OnAddFriend, MakeGenericClass(Action1, UInt64), this))
end
CFriendSearchView.m_OnSearch_CS2LuaHook = function (this, keywords) 
    if not this.enabled then
        return
    end
    keywords = StringTrim(keywords)
    if System.String.IsNullOrEmpty(keywords) or CUICommonDef.GetStrByteLength(keywords) < Constants.MaxNumOfFriendSearchBytes then
        CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("关键词长度太短！"), 1)
        return
    end
    this.lastSearchKey = keywords
    CIMMgr.Inst:SearchOnlinePlayer(keywords)
end
CFriendSearchView.m_OnReturnSearchSuccess_CS2LuaHook = function (this, results) 
    if not this.enabled then
        return
    end
    local itemInfos = CreateFromClass(MakeGenericClass(List, FriendItemData))
    local val = 0
    local default
    default, val = System.UInt32.TryParse(this.lastSearchKey)
    if default and val == Constants.YangyangID then
        local info = CreateFromClass(FriendItemData, Constants.YangyangID, 1, GameSetting_Common_Wapper.Inst.YangYangDisplayName, GameSetting_Common_Wapper.Inst.YangYangPortrait, CFriendListItem.ItemType.ChatBot, true)
        CommonDefs.ListAdd(itemInfos, typeof(FriendItemData), info)
    end
    do
        local i = 0
        while i < results.Count do
            local continue
            repeat
                if results[i].ID == CClientMainPlayer.Inst.Id then
                    continue = true
                    break
                end
                local info = CreateFromClass(FriendItemData, results[i])
                info.isOnline = true
                CommonDefs.ListAdd(itemInfos, typeof(FriendItemData), info)
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    this:LoadSearchResult(itemInfos)
    this.searchFailedLabel.enabled = (itemInfos.Count == 0)
end
CFriendSearchView.m_OnReturnSearchFail_CS2LuaHook = function (this) 
    if not this.enabled then
        return
    end
    local val = 0
    local default
    default, val = System.UInt32.TryParse(this.lastSearchKey)
    if default and val == Constants.YangyangID then
        local itemInfos = CreateFromClass(MakeGenericClass(List, FriendItemData))
        local info = CreateFromClass(FriendItemData, Constants.YangyangID, 1, GameSetting_Common_Wapper.Inst.YangYangDisplayName, GameSetting_Common_Wapper.Inst.YangYangPortrait, CFriendListItem.ItemType.ChatBot, true)
        CommonDefs.ListAdd(itemInfos, typeof(FriendItemData), info)
        this:LoadSearchResult(itemInfos)
        this.searchFailedLabel.enabled = false
        return
    end
    Extensions.RemoveAllChildren(this.table.transform)
    this.searchFailedLabel.enabled = true
end
CFriendSearchView.m_OnMainPlayerRelationshipPropUpdate_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.items.Count do
            this.items[i].IsMyFriend = CIMMgr.Inst:IsMyFriend(this.items[i].PlayerId)
            i = i + 1
        end
    end
end
CFriendSearchView.m_OnAddFriend_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.items.Count do
            if this.items[i].PlayerId == playerId then
                this.items[i].IsMyFriend = CIMMgr.Inst:IsMyFriend(playerId)
                break
            end
            i = i + 1
        end
    end
end
CFriendSearchView.m_LoadSearchResult_CS2LuaHook = function (this, data) 
    this:Clear()
    do
        local i = 0
        while i < data.Count do
            local GO = NGUITools.AddChild(this.table.gameObject, this.friendItemTemplate)
            GO:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(GO, typeof(CFriendSearchListItem))

            --搜索关键词高亮显示
            local name = data[i].name
            if not System.String.IsNullOrEmpty(this.lastSearchKey) then
                local index = CommonDefs.StringIndexOf_String(name, this.lastSearchKey)
                if index >= 0 and index < CommonDefs.StringLength(name) then
                    name = CommonDefs.StringReplace(name, this.lastSearchKey, System.String.Format("[c][FF9052]{0}[-][/c]", this.lastSearchKey))
                end
            end
            item:Init(true, data[i].playerId, name, data[i].portraitName, data[i].expressionTxt, data[i].profileFrame, data[i].level, false, data[i].isInXianShenStatus)
            item.IsMyFriend = CIMMgr.Inst:IsMyFriend(data[i].playerId)
            item.OnAddFriendDelegate = MakeDelegateFromCSFunction(this.OnAddFriendButtonClick, MakeGenericClass(Action1, UInt64), this)
            CommonDefs.ListAdd(this.items, typeof(CFriendSearchListItem), item)
            i = i + 1
        end
    end
    this.table:Reposition()
end
CFriendSearchView.m_Clear_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.table.transform)
    CommonDefs.ListClear(this.items)
    this.searchFailedLabel.enabled = false
end
CFriendSearchView.m_OnChange_CS2LuaHook = function (this, val) 
    --//如果是代言人的id
    --//TODO:
    if val == tostring(Constants.YangyangID) then
        --L10.Game.Guide.CGuideMessager cmp = inputRegion.GetComponent<L10.Game.Guide.CGuideMessager>();
        --if (cmp!= null)
        --{
        --    cmp.Click();
        --}
        if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(16) then
            L10.Game.Guide.CGuideMgr.Inst:TriggerGuide(5)
        end
    end
end
CFriendSearchView.m_GetAddFriendButton_CS2LuaHook = function (this) 
    if this.table.transform.childCount > 0 then
        local cmp = CommonDefs.GetComponent_Component_Type(this.table.transform:GetChild(0), typeof(CFriendSearchListItem))
        if cmp.PlayerId == Constants.YangyangID then
            local get = cmp:GetAddFriendButton()
            --特殊处理，如果已经加过了，则中断改引导
            if get.activeInHierarchy then
                return get
            else
                --这个按钮没激活，说明已经添加过洋洋了
                L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
            end
        end
    end
    return nil
end
