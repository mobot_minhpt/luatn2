-- Auto Generated!!
local CChatInputLink = import "CChatInputLink"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CommentStruct = import "L10.UI.CPersonalSpaceDetailMoment+CommentStruct"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPersonalSpace_CircleGenerateType = import "L10.Game.CPersonalSpace_CircleGenerateType"
local CPersonalSpace_GetMomentItem = import "L10.Game.CPersonalSpace_GetMomentItem"
local CPersonalSpaceDetailMoment = import "L10.UI.CPersonalSpaceDetailMoment"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CQnSymbolParser = import "CQnSymbolParser"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local NGUITools = import "NGUITools"
local Object1 = import "UnityEngine.Object"
local Time = import "UnityEngine.Time"
local TweenPosition = import "TweenPosition"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local UIScrollView = import "UIScrollView"
local UISprite = import "UISprite"
local UITable = import "UITable"
local UITexture = import "UITexture"
local UIWidget = import "UIWidget"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"

local LuaPersonalSpaceDetail = {}

CPersonalSpaceDetailMoment.m_ResetInputInfo_CS2LuaHook = function (this)
    this.ReplyPersonId = 0
    this.ReplyPersonName = ""
    CommonDefs.DictClear(this.existingLinks)
    this.sendStatusPanel_statusInput.value = ""
end
CPersonalSpaceDetailMoment.m_SetInputDefaultValue_CS2LuaHook = function (this, roleid, rolename, node, info)
    if not info.commentable then
        g_MessageMgr:ShowMessage("CANNOT_COMMENT")
        return
    end

    this.ReplyPersonId = roleid
    this.ReplyPersonName = rolename
    this.sendStatusPanel_statusInput.value = ""
    this.sendStatusPanel_statusText.text = CPersonalSpaceDetailMoment.ReplyPrefix .. this.ReplyPersonName
    this.sendStatusPanel_statusInput.defaultText = CPersonalSpaceDetailMoment.ReplyPrefix .. this.ReplyPersonName
    if LuaPersonalSpaceDetail.barNode then
      CommonDefs.GetComponent_Component_Type(LuaPersonalSpaceDetail.barNode, typeof(TweenPosition)):PlayForward()
    end
end
CPersonalSpaceDetailMoment.m_ShowInputNode_CS2LuaHook = function (this, node, info)
    if info.commentable then
        local defaultReplyName = info.rolename
        if System.String.IsNullOrEmpty(defaultReplyName) then
            defaultReplyName = tostring(info.roleid)
        end

        this.sendStatusPanel_statusInput.value = ""
        this.sendStatusPanel_statusText.text = CPersonalSpaceDetailMoment.ReplyPrefix .. defaultReplyName
        this.sendStatusPanel_statusInput.defaultText = CPersonalSpaceDetailMoment.ReplyPrefix .. defaultReplyName
        if LuaPersonalSpaceDetail.barNode then
          CommonDefs.GetComponent_Component_Type(LuaPersonalSpaceDetail.barNode, typeof(TweenPosition)):PlayForward()
        end
    else
        g_MessageMgr:ShowMessage("CANNOT_COMMENT")
    end
end
CPersonalSpaceDetailMoment.m_InitCommentList_CS2LuaHook = function (this, savePosSign)

    this.scrollView.contentPivot = UIWidget.Pivot.Top

    if math.floor(this.scrollView.bounds.size.y - this.scrollView.panel.baseClipRegion.w + 0.5) > 0 then
        this.scrollView.contentPivot = this.saveDefaultPivot
    end

    local savePos = this.scrollView.transform.localPosition
    local saveOffsetY = this.panel.clipOffset.y

    this.table:Reposition()
    this.scrollView:ResetPosition()

    if savePosSign then
        this.scrollView.transform.localPosition = savePos
        this.panel.clipOffset = Vector2(this.panel.clipOffset.x, saveOffsetY)
    end
end
CPersonalSpaceDetailMoment.m_InitCommentListNode_CS2LuaHook = function (this, commentlist, node, info, reNew, savePos)
    if reNew then
        Extensions.RemoveAllChildren(node.transform)
    end

    if commentlist ~= nil and commentlist.Length > 0 then
        do
            local com_i = 0
            while com_i < commentlist.Length do
                local comment_info = commentlist[com_i]
                local messageNode = NGUITools.AddChild(this.table.gameObject, this.messageTemplateNode)
                messageNode:SetActive(true)
                if this.SaveCommentList.Count == 0 then
                    messageNode.transform:Find("icon").gameObject:SetActive(true)
                else
                    messageNode.transform:Find("icon").gameObject:SetActive(false)
                end

                local showText = CPersonalSpaceMgr.GeneratePlayerName(comment_info.roleid, comment_info.rolename)
                if comment_info.replyinfo.roleid ~= 0 then
                    showText = showText .. (CPersonalSpaceDetailMoment.ReplayMidfix .. CPersonalSpaceMgr.GeneratePlayerName(comment_info.replyinfo.roleid, comment_info.replyinfo.rolename))
                end

                showText = showText .. comment_info.text
                local textNode = messageNode.transform:Find("text")
                local textNodeLabel = CommonDefs.GetComponent_Component_Type(textNode, typeof(UILabel))
                showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
                textNodeLabel.text = CChatLinkMgr.TranslateToNGUIText(showText, false)

                UIEventListener.Get(textNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                    local index = textNodeLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
                    if index == 0 then
                        index = - 1
                    end

                    local url = textNodeLabel:GetUrlAtCharacterIndex(index)
                    if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                        CPersonalSpaceMgr.Inst:SetPlayerReportDefaultInfo(EnumReportId_lua.eMengDaoMoment, info.text, info.imglist, info.id)
                        return
                    else
                        this:SetInputDefaultValue(comment_info.roleid, comment_info.rolename, this.gameObject, info)
                    end
                end)

                local commentStruct = CreateFromClass(CommentStruct)
                commentStruct.id = comment_info.id
                commentStruct.node = messageNode
                CommonDefs.ListAdd(this.SaveCommentList, typeof(CommentStruct), commentStruct)

                local deleteBtn = messageNode.transform:Find("deletebutton").gameObject
                deleteBtn:SetActive(false)
                if comment_info.roleid == CClientMainPlayer.Inst.Id or info.roleid == CClientMainPlayer.Inst.Id then
                    deleteBtn:SetActive(true)
                    UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                        MessageWndManager.ShowConfirmMessage(CPersonalSpaceDetailMoment.DeleteCommentAlertString, 10, false, DelegateFactory.Action(function ()
                            CPersonalSpaceMgr.Inst:DelComment(comment_info.id, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)
                                if not this then
                                    return
                                end
                                if data.code == 0 then
                                    if this.gameObject.activeSelf then
                                        CPersonalSpaceMgr.Inst:GetSingleMomentInfo(this.m_Info.id, DelegateFactory.Action_CPersonalSpace_GetSingleMoment_Ret(function (ret)
                                            if not this then
                                                return
                                            end
                                            if ret.code == 0 then
                                                local updateList = Table2ArrayWithCount({ret.data}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))
                                                this.personalSpaceWnd:updateMomentCache(updateList, false, this.m_Info.type, this.m_FormerNode, true)
                                                --Init(m_Info, m_FormerNode, UIWidget.Pivot.Top, false, false);

                                                if this.SaveCommentList.Count > 0 then
                                                    commentStruct.node.transform.parent = nil
                                                    CommonDefs.ListRemove(this.SaveCommentList, typeof(CommentStruct), commentStruct)
                                                    Object1.Destroy(commentStruct.node)
                                                    this:InitCommentList(true)
                                                end
                                            end
                                        end), nil)
                                    end
                                end
                            end), nil)
                        end), nil)
                    end)
                end
                com_i = com_i + 1
            end
        end
    end

    this:InitCommentList(savePos)
end
CPersonalSpaceDetailMoment.m_SetShowMoreBtn_CS2LuaHook = function (this, showMoreBtn, info)
    UIEventListener.Get(showMoreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        showMoreBtn:SetActive(false)
        local lastId = 0
        if this.SaveCommentList.Count > 0 then
            local cstruct = this.SaveCommentList[this.SaveCommentList.Count - 1]
            lastId = cstruct.id
        end
        CPersonalSpaceMgr.Inst:GetComments(lastId, DelegateFactory.Action_CPersonalSpace_GetComments_Ret(function (data)
            if not this or this == nil then
                return
            end
            if data.data ~= nil then
                if data.data.Length > 0 then
                    if data.data.Length < CPersonalSpaceDetailMoment.MaxEveryGetNum then
                        this.scrollViewIndicator.enableShowNode = false
                    end

                    this:InitCommentListNode(data.data, this.table.gameObject, info, false, true)
                else
                    this.scrollViewIndicator.enableShowNode = false
                end
            else
                this.scrollViewIndicator.enableShowNode = false
                --TODO show no more
            end
        end), nil)
    end)
end
CPersonalSpaceDetailMoment.m_BackFunction_CS2LuaHook = function (this, info_type)
--    local barNode = this.transform:Find("contain/Table/InfoItem/subInfo/bar/node")
--    if barNode.localPosition.x ~= 0 then
--        CommonDefs.GetComponent_Component_Type(barNode, typeof(TweenPosition)):ResetToBeginning()
--        barNode.localPosition = Vector3(0, barNode.localPosition.y, barNode.localPosition.z)
--    end
    this.gameObject:SetActive(false)
    this.formerPaneNode:SetActive(true)

    if info_type == CPersonalSpace_CircleGenerateType.HotMoments then
        this.personalSpaceWnd:ReposFriendCircleList(CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.scrollView, nil, nil)
    elseif info_type == CPersonalSpace_CircleGenerateType.AllHotMoments then
        this.personalSpaceWnd:ReposFriendCircleList(CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.scrollView, nil, nil)
    else
        this.personalSpaceWnd:ReposFriendCircleList(nil, nil, nil)
    end
end
CPersonalSpaceDetailMoment.m_Init_CS2LuaHook = function (this, _info, formerNode, defaultPivot, showTalk, totalReNew)
    this.saveDefaultPivot = defaultPivot
    this.SaveCommentList = CreateFromClass(MakeGenericClass(List, CommentStruct))

    local info = _info

    if _info.type == CPersonalSpace_CircleGenerateType.HotMoments then
        if not CommonDefs.DictContains(CPersonalSpaceMgr.Inst.hotMomentDic, typeof(UInt64), _info.id) then
            this:BackFunction(_info.type)
            return
        end
        info = CommonDefs.DictGetValue(CPersonalSpaceMgr.Inst.hotMomentDic, typeof(UInt64), _info.id).data
    elseif _info.type == CPersonalSpace_CircleGenerateType.AllHotMoments then
        if not CommonDefs.DictContains(CPersonalSpaceMgr.Inst.allhotMomentDic, typeof(UInt64), _info.id) then
            this:BackFunction(info.type)
            return
        end
        info = CommonDefs.DictGetValue(CPersonalSpaceMgr.Inst.allhotMomentDic, typeof(UInt64), _info.id).data
    else
        --if (CPersonalSpaceMgr.Inst.momentCacheDic.ContainsKey(_info.id))
        --{
        --    info = CPersonalSpaceMgr.Inst.momentCacheDic[_info.id].data;
        --}
        --else if (CPersonalSpaceMgr.Inst.otherMomentCacheDic.ContainsKey(_info.id))
        --{
        --    info = CPersonalSpaceMgr.Inst.otherMomentCacheDic[_info.id].data;
        --}
        --else
        --{
        --    //BackFunction(info.type);
        --    //return;
        --}

        info = _info
    end

    this.m_Info = info
    this.m_FormerNode = formerNode

    Extensions.RemoveAllChildren(this.table.transform)

    local nodeTemplate = this.transform:Find("InfoItem").gameObject
    nodeTemplate:SetActive(false)

    local node = NGUITools.AddChild(this.table.gameObject, nodeTemplate)
    node:SetActive(true)
    this.sendStatusPanel_submitBtn = node.transform:Find("subInfo/bar/node/SendStatus/SubmitButton").gameObject
    this.sendStatusPanel_cancelBtn = node.transform:Find("subInfo/bar/node/SendStatus/BackButton").gameObject
    this.sendStatusPanel_statusText = node.transform:Find("subInfo/bar/node/SendStatus/StatusText/Label"):GetComponent(typeof(UILabel))
    this.sendStatusPanel_emotionBtn = node.transform:Find("subInfo/bar/node/SendStatus/EmotionButton").gameObject
    this.sendStatusPanel_statusInput = node.transform:Find("subInfo/bar/node/SendStatus/StatusText"):GetComponent(typeof(UIInput))
    LuaPersonalSpaceDetail.barNode = node.transform:Find("subInfo/bar/node")

    local oldNode = this.gameObject
    local statusLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel))

    local subInfo = node.transform:Find("subInfo")
    totalReNew = true
    if totalReNew then
        this:ResetInputInfo()

        this.favorTemplateNode:SetActive(false)
        this.messageTemplateNode:SetActive(false)
        this.formerPaneNode = formerNode
        this.formerPaneNode:SetActive(false)

        UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            this:BackFunction(info.type)
        end)


        if System.String.IsNullOrEmpty(info.rolename) then
            CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/name"), typeof(UILabel)).text = tostring(info.roleid)
        else
            CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/name"), typeof(UILabel)).text = info.rolename
        end

        local vipNode = oldNode.transform:Find("InfoItemOld/name/vipicon")
        if vipNode ~= nil then
            CPersonalSpaceMgr.Inst:SetSpLevelNode(info.splevel, vipNode.gameObject)
        end

        if info.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
            --node.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
            CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/lv"), typeof(UILabel)).text = ""
            CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
        elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.roleid, oldNode.transform:Find("InfoItemOld/icon").gameObject, CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/lv"), typeof(UILabel))) then
        else
            --oldNode.transform.FindChild("InfoItemOld/lv").GetComponent<UILabel>().text = info.grade.ToString();
            local levelLabel = CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("InfoItemOld/lv"), typeof(UILabel))
            local default
            if info.xianfanstatus > 0 then
                default = L10.Game.Constants.ColorOfFeiSheng
            else
                default = NGUIText.EncodeColor24(levelLabel.color)
            end
            levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, "lv." .. info.grade)

            --if (!string.IsNullOrEmpty(info.clazz) && !string.IsNullOrEmpty(info.gender))
            --{
            --    uint cls = uint.Parse(info.clazz);
            --    uint gender = uint.Parse(info.gender);
            --    oldNode.transform.FindChild("InfoItemOld/icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
            --}
            CPersonalSpaceMgr.LoadPortraitPic(info.photo, info.clazz, info.gender, oldNode.transform:Find("InfoItemOld/icon").gameObject, info.expression_base)

            if info.roleid ~= CPersonalSpaceWnd.Instance.SelfRoleInfo.id then
                CPersonalSpaceMgr.Inst:addPlayerLinkBtn(oldNode.transform:Find("InfoItemOld/icon/button").gameObject, info.roleid, EnumReportId_lua.eMengDaoMoment, info.text, info.imglist, info.id, 0)
            end
        end

        local showText = string.gsub(string.gsub(info.text, "&lt;", "<"), "&gt;", ">")
        showText = showText .. CPersonalSpaceMgr.GenerateForwardString(info.previousforwards)
        showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
        statusLabel.text = SafeStringFormat3('[c][FFFFFF]%s[-][/c]',CChatLinkMgr.TranslateToNGUIText(showText, false))

        UIEventListener.Get(statusLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                return
            end
        end)

        --subInfo.localPosition = new Vector3(subInfo.localPosition.x, -statusLabel.height, subInfo.localPosition.z);

        local yShift = - statusLabel.height

        --GameObject picNode1 = node.transform.FindChild("pic1").gameObject;
        --GameObject picNode2 = node.transform.FindChild("pic2").gameObject;
        --GameObject picNode3 = node.transform.FindChild("pic3").gameObject;
        local speNode = node.transform:Find("spepic").gameObject

        --GameObject[] picNodeArray = new GameObject[] { picNode1, picNode2, picNode3 };

        --picNode1.SetActive(false);
        --picNode2.SetActive(false);
        --picNode3.SetActive(false);
        local picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
        for pic_i = 1, 6 do
            local picNode = node.transform:Find("pic" .. tostring(pic_i))
            if picNode ~= nil then
                picNode.gameObject:SetActive(false)
                CommonDefs.ListAdd(picNodeArray, typeof(GameObject), picNode.gameObject)
            end
        end

        speNode:SetActive(false)

        if info.speimage ~= nil and not System.String.IsNullOrEmpty(info.speimage.url) then
          speNode:SetActive(true)
          CPersonalSpaceMgr.DownLoadPic(info.speimage.url, CommonDefs.GetComponent_GameObject_Type(speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
            if not this.favorTemplateNode then
              return
            end
            UIEventListener.Get(speNode).onClick = DelegateFactory.VoidDelegate(function (p)
              CWebBrowserMgr.Inst:OpenUrl(info.speimage.jump_url)
            end)
          end), false, true)
          speNode.transform.localPosition = Vector3(speNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, speNode.transform.localPosition.z)
          yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
        elseif info.videolist and info.videolist.Length > 0 then
          local videoNode = node.transform:Find('pic1').gameObject
          LuaPersonalSpaceMgrReal.SetVideoPlayPic(videoNode,100)
          local videoData = info.videolist[0]
          videoNode:SetActive(true)
          CPersonalSpaceMgr.DownLoadPic(videoData.snapshot, CommonDefs.GetComponent_GameObject_Type(videoNode, typeof(UITexture)), 160, 250, DelegateFactory.Action(function ()
            if not this.favorTemplateNode then
              return
            end
            UIEventListener.Get(videoNode).onClick = DelegateFactory.VoidDelegate(function (p)
              LuaPersonalSpaceMgrReal.MoviePlayUrl = videoData.url
              CUIManager.ShowUI(CLuaUIResources.DetailMovieShowWnd)
            end)
          end), true, true)
          videoNode.transform.localPosition = Vector3(videoNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, videoNode.transform.localPosition.z)
          yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
        else
          if info.imglist ~= nil and info.imglist.Length > 0 then
            do
              local i = 0
              while i < info.imglist.Length do
                local imageInfo = info.imglist[i]
                local picNode = picNodeArray[i]
                picNode:SetActive(true)
                CPersonalSpaceWnd.Instance:SetMomentPic(picNode, info.imglist, i)
                if i < 3 then
                  picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, picNode.transform.localPosition.z)
                else
                  picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight, picNode.transform.localPosition.z)
                end
                i = i + 1
              end
            end
            if info.imglist.Length <= 3 then
              yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
            else
              yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
              yShift = yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight
            end
          end
        end

        local forwardNode = node.transform:Find("forwardinfo").gameObject
        local forward_bgBtn = node.transform:Find("forwardinfo/forwardbg").gameObject
        if info.forwardmoment.id > 0 then
            forwardNode:SetActive(true)

            if System.String.IsNullOrEmpty(info.forwardmoment.rolename) then
                CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = tostring(info.forwardmoment.roleid)
            else
                CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = info.forwardmoment.rolename
            end

            local forwardVipNode = forwardNode.transform:Find("name/vipicon")
            if forwardVipNode ~= nil then
                CPersonalSpaceMgr.Inst:SetSpLevelNode(info.forwardmoment.splevel, forwardVipNode.gameObject)
            end

            if CClientMainPlayer.Inst:GetMyServerId() ~= info.forwardmoment.server_id and info.forwardmoment.server_id ~= 0 then
                CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text .. (CPersonalSpaceMgr.ServerSplitChar .. info.forwardmoment.server_name)
            end

            if info.forwardmoment.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
                CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel)).text = ""
                CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
            elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.forwardmoment.roleid, forwardNode.transform:Find("icon").gameObject, CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))) then
            else
                --forwardNode.transform.FindChild("lv").GetComponent<UILabel>().text = info.forwardmoment.grade.ToString();
                local levelLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))
                local extern
                if info.forwardmoment.xianfanstatus > 0 then
                    extern = L10.Game.Constants.ColorOfFeiSheng
                else
                    extern = NGUIText.EncodeColor24(levelLabel.color)
                end
                levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), extern, "lv." .. info.forwardmoment.grade)

                --if (!string.IsNullOrEmpty(info.forwardmoment.clazz) && !string.IsNullOrEmpty(info.forwardmoment.gender))
                --{
                --    uint cls = uint.Parse(info.forwardmoment.clazz);
                --    uint gender = uint.Parse(info.forwardmoment.gender);
                --    forwardNode.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
                --}
                CPersonalSpaceMgr.LoadPortraitPic(info.forwardmoment.photo, info.forwardmoment.clazz, info.forwardmoment.gender, forwardNode.transform:Find("icon").gameObject, info.forwardmoment.expression_base)

                if info.forwardmoment.roleid ~= this.personalSpaceWnd.SelfRoleInfo.id then
                    CPersonalSpaceMgr.Inst:addPlayerLinkBtn(forwardNode.transform:Find("icon/button").gameObject, info.forwardmoment.roleid, EnumReportId_lua.eMengDaoMoment, info.forwardmoment.text, info.forwardmoment.imglist, info.forwardmoment.id, 0)
                end
            end

            local forward_showText = string.gsub(string.gsub(info.forwardmoment.text, "&lt;", "<"), "&gt;", ">")
            forward_showText = CChatMgr.Inst:FilterYangYangEmotion(forward_showText)
            local forwardLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("status"), typeof(UILabel))
            --forwardLabel.text = CChatLinkMgr.TranslateToNGUIText(forward_showText);
            CPersonalSpaceMgr.SetTextMore(forward_showText, forwardLabel)

            UIEventListener.Get(forwardLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
                if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                    return
                else
                    this.personalSpaceWnd:ShowMomentDetailThroughFriendCircleById(info.forwardmoment.id, false, formerNode)
                end
            end)

            --GameObject forward_picNode1 = forwardNode.transform.FindChild("pic1").gameObject;
            --GameObject forward_picNode2 = forwardNode.transform.FindChild("pic2").gameObject;
            --GameObject forward_picNode3 = forwardNode.transform.FindChild("pic3").gameObject;
            local forward_speNode = forwardNode.transform:Find("spepic").gameObject

            --GameObject[] forward_picNodeArray = new GameObject[] { forward_picNode1, forward_picNode2, forward_picNode3 };
            --forward_picNode1.SetActive(false);
            --forward_picNode2.SetActive(false);
            --forward_picNode3.SetActive(false);
            forward_speNode:SetActive(false)

            local forward_picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
            for pic_i = 1, 6 do
                local picNode = forwardNode.transform:Find("pic" .. tostring(pic_i))
                if picNode ~= nil then
                    picNode.gameObject:SetActive(false)
                    CommonDefs.ListAdd(forward_picNodeArray, typeof(GameObject), picNode.gameObject)
                end
            end

            forward_bgBtn:SetActive(true)

            UIEventListener.Get(forward_bgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                this.personalSpaceWnd:ShowMomentDetailThroughFriendCircleById(info.forwardmoment.id, false, formerNode)
            end)

            forwardNode.transform.localPosition = Vector3(forwardNode.transform.localPosition.x, this.defaultPicHeight + yShift, forwardNode.transform.localPosition.z)
            --forward_bgBtn.transform.localPosition = forwardNode.transform.localPosition;

            local newHeight = 0
            local newHeight1 = 0

            if info.forwardmoment.speimage ~= nil and not System.String.IsNullOrEmpty(info.forwardmoment.speimage.url) then
              forward_speNode:SetActive(true)
              CPersonalSpaceMgr.DownLoadPic(info.forwardmoment.speimage.url, CommonDefs.GetComponent_GameObject_Type(forward_speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
                if not this.favorTemplateNode then
                  return
                end
                UIEventListener.Get(forward_speNode).onClick = DelegateFactory.VoidDelegate(function (p)
                  CWebBrowserMgr.Inst:OpenUrl(info.forwardmoment.speimage.jump_url)
                end)
              end), false, true)

              --if this.maxInfoHeight <= CPersonalSpaceMgr.ForwardInfoHeight2 - yShift then
              --    newHeight = this.maxInfoHeight + yShift
              --else
              newHeight = CPersonalSpaceMgr.ForwardInfoHeight2
              --end
              newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight2
            elseif info.forwardmoment.videolist and info.forwardmoment.videolist.Length > 0 then
              local videoNode = forwardNode.transform:Find('pic1').gameObject
              LuaPersonalSpaceMgrReal.SetVideoPlayPic(videoNode,100)
              local videoData = info.forwardmoment.videolist[0]
              videoNode:SetActive(true)
              CPersonalSpaceMgr.DownLoadPic(videoData.snapshot, CommonDefs.GetComponent_GameObject_Type(videoNode, typeof(UITexture)), 160, 250, DelegateFactory.Action(function ()
                if not this.favorTemplateNode then
                  return
                end
                UIEventListener.Get(videoNode).onClick = DelegateFactory.VoidDelegate(function (p)
                  LuaPersonalSpaceMgrReal.MoviePlayUrl = videoData.url
                  CUIManager.ShowUI(CLuaUIResources.DetailMovieShowWnd)
                end)
              end), true, true)
              newHeight = CPersonalSpaceMgr.ForwardInfoHeight2
              newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight2
            else
              if info.forwardmoment.imglist ~= nil and info.forwardmoment.imglist.Length > 0 then
                do
                  local i = 0
                  while i < info.forwardmoment.imglist.Length do
                    local imageInfo = info.forwardmoment.imglist[i]
                    local picNode = forward_picNodeArray[i]
                    picNode:SetActive(true)
                    this.personalSpaceWnd:SetMomentPic(picNode, info.forwardmoment.imglist, i)
                    i = i + 1
                  end
                end
                if info.forwardmoment.imglist.Length <= 3 then
                  newHeight = CPersonalSpaceMgr.ForwardInfoHeight2
                  newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight2
                else
                  newHeight = CPersonalSpaceMgr.ForwardInfoHeight2 + CPersonalSpaceWnd.DefaultTwoRowPicHeight
                  newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight2 + CPersonalSpaceWnd.DefaultTwoRowPicHeight
                end
              else
                newHeight = CPersonalSpaceMgr.ForwardInfoHeight1
                newHeight1 = CPersonalSpaceMgr.ForwardInfoHeight1
              end
            end

            if newHeight <= 0 then
                forward_bgBtn:SetActive(false)
                forwardNode:SetActive(false)
            else
                CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = newHeight1
                yShift = yShift - newHeight
            end

            CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)):ResizeCollider()
        else
            forwardNode:SetActive(false)
            forward_bgBtn:SetActive(false)
        end

        --CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("containbg"), typeof(UISprite)).height = - yShift + 15
        --CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("contain"), typeof(UIPanel)):UpdateAnchors()



        subInfo.localPosition = Vector3(subInfo.localPosition.x, yShift - 260, subInfo.localPosition.z)

        --local scrollViewPanel = CommonDefs.GetComponent_GameObject_Type(this.scrollView.gameObject, typeof(UIPanel))
        --scrollViewPanel.SetRect(scrollViewPanel.transform.position.x, scrollViewPanel.transform.position.y + statusLabel.height / 2, scrollViewPanel.width, defaultTableHeight - statusLabel.height);
        --local commentHeight = this.defaultTableHeight + yShift
        --if commentHeight < 500 then
         -- commentHeight = 500
        --end

        --CommonDefs.GetComponent_Component_Type(subInfo:Find("bg"), typeof(UISprite)).height = commentHeight

        --scrollViewPanel:ResetAndUpdateAnchors()
        --this.scrollViewIndicator:Layout()

        --///input

        local defaultReplyName = info.rolename
        if System.String.IsNullOrEmpty(defaultReplyName) then
            defaultReplyName = tostring(info.roleid)
        end
        this.sendStatusPanel_statusText.text = CPersonalSpaceDetailMoment.ReplyPrefix .. defaultReplyName
        this.sendStatusPanel_statusInput.defaultText = CPersonalSpaceDetailMoment.ReplyPrefix .. defaultReplyName

        if showTalk then
            this:ShowInputNode(subInfo.gameObject, info)
        end

        UIEventListener.Get(subInfo:Find("bar/node/leavemessagebutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            this:ShowInputNode(subInfo.gameObject, info)
        end)

        UIEventListener.Get(this.sendStatusPanel_cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node"), typeof(TweenPosition)):PlayReverse()
        end)

        UIEventListener.Get(this.sendStatusPanel_emotionBtn).onClick = MakeDelegateFromCSFunction(this.OnEmoticonButtonClick, VoidDelegate, this)
    end


    local infoTime = info.createtime / 1000
    local nowTime = CServerTimeMgr.Inst.timeStamp
    local disTime = nowTime - infoTime
    if disTime < 3600 then
        CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/time"), typeof(UILabel)).text = math.ceil(disTime / 60) .. LocalString.GetString("分钟前")
    elseif disTime < 86400 --[[24 * 3600]] then
        CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/time"), typeof(UILabel)).text = math.ceil(disTime / 3600) .. LocalString.GetString("小时前")
    else
        local infoTimeDate = CServerTimeMgr.ConvertTimeStampToZone8Time(infoTime)
        CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/time"), typeof(UILabel)).text = (((infoTimeDate.Year .. "-") .. infoTimeDate.Month) .. "-") .. infoTimeDate.Day
    end

    --//favor
    local favorIcon = subInfo:Find("bar/node/favorbutton/favoricon").gameObject
    local favorNumLabel = CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/favorbutton/favornum"), typeof(UILabel))
    favorIcon:SetActive(false)
    local favorText = ""

    if info.zanlist ~= nil and info.zancount > 0 then
        if info.is_user_liked then
            favorIcon:SetActive(true)
        else
            favorIcon:SetActive(false)
        end

        favorNumLabel.text = tostring(info.zancount)
        local favorNode = NGUITools.AddChild(this.table.gameObject, this.favorTemplateNode)
        favorNode:SetActive(true)
        CommonDefs.GetComponent_Component_Type(favorNode.transform:Find("text"), typeof(UILabel)).text = ""
        do
            local zan_i = 0
            while zan_i < info.zanlist.Length do
                local zanInfo = info.zanlist[zan_i]
                local name = zanInfo.rolename
                if System.String.IsNullOrEmpty(name) then
                    name = tostring(zanInfo.roleid)
                end
                favorText = favorText .. CPersonalSpaceMgr.GeneratePlayerName(zanInfo.roleid, name)
                zan_i = zan_i + 1
            end
        end
        local textNodeLabel = CommonDefs.GetComponent_Component_Type(favorNode.transform:Find("text"), typeof(UILabel))

        textNodeLabel.text = CChatLinkMgr.TranslateToNGUIText(favorText, false)
        UIEventListener.Get(textNodeLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            local index = textNodeLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
            if index == 0 then
                index = - 1
            end

            local url = textNodeLabel:GetUrlAtCharacterIndex(index)

            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                return
            end
        end)

        this:InitCommentList(false)
    else
        favorNumLabel.text = "0"
    end


    local showMoreBtn = this.scrollViewIndicator.transform:Find("ShowNode").gameObject
    this:SetShowMoreBtn(showMoreBtn, info)

    --comment
    local leaveMessageNumText = CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/leavemessagebutton/leavemessagenum"), typeof(UILabel))
    if info.commentlist ~= nil and info.commentlist.Length > 0 then
        leaveMessageNumText.text = tostring(info.commentcount)
        this:InitCommentListNode(info.commentlist, this.table.gameObject, info, false, false)
        if info.commentlist.Length >= CPersonalSpaceDetailMoment.MaxEveryGetNum then
            this.scrollViewIndicator.enableShowNode = true
        else
            this.scrollViewIndicator.enableShowNode = false
        end
    else
        this.scrollViewIndicator.enableShowNode = false
        leaveMessageNumText.text = "0"
    end

    local forwardBtn = subInfo:Find("bar/node/forwardbutton").gameObject
    local forwardNum = CommonDefs.GetComponent_Component_Type(subInfo:Find("bar/node/forwardbutton/num"), typeof(UILabel))
    forwardNum.text = tostring(info.forwardcount)
    UIEventListener.Get(forwardBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if info.forwardmoment.id > 0 and info.forwardmoment.status == - 1 then
            g_MessageMgr:ShowMessage("PersonalSpaceCannotForward")
        else
            CPersonalSpaceMgr.Inst.saveForwardInfo = info
            CUIManager.ShowUI(CUIResources.PersonalSpaceForwardWnd)
        end
    end)

    local favorbuttonSign = subInfo:Find("bar/node/favorbutton/favoricon/UI_aixin").gameObject
    UIEventListener.Get(subInfo:Find("bar/node/favorbutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CPersonalSpaceMgr.Inst:LikeMoment(info.id, favorIcon.activeSelf, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)
            if this.gameObject == nil then
                return
            end
            if data.code == 0 then
                CPersonalSpaceMgr.Inst:GetSingleMomentInfo(info.id, DelegateFactory.Action_CPersonalSpace_GetSingleMoment_Ret(function (ret)
                    if not this or not this.sendStatusPanel_statusInput then
                        return
                    end
                    if ret.code == 0 then
                        local newItem = ret.data
                        if not favorbuttonSign.activeSelf and newItem.is_user_liked then
                            newItem.showFavorSign = true
                        else
                            newItem.showFavorSign = false
                        end

                        newItem.type = info.type

                        local updateList = Table2ArrayWithCount({newItem}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))

                        CPersonalSpaceWnd.Instance:updateMomentCache(updateList, false, info.type, formerNode, true)

                        this:Init(newItem, formerNode, UIWidget.Pivot.Top, false, false)
                    end
                end), nil)
            end
        end), DelegateFactory.Action(function ()
        end))
    end)

    UIEventListener.Get(this.sendStatusPanel_submitBtn).onClick = MakeDelegateFromCSFunction(this.OnSubmitBtnClick, VoidDelegate, this)
    this.sendStatusPanel_statusInput.onReturnKeyPressed = MakeDelegateFromCSFunction(this.OnSubmitBtnClick, MakeGenericClass(Action1, GameObject), this)

    CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("contain/Table"), typeof(UITable)):Reposition()
    CommonDefs.GetComponent_Component_Type(oldNode.transform:Find("contain"), typeof(UIScrollView)):ResetPosition()
end
CPersonalSpaceDetailMoment.m_OnSubmitBtnClick_CS2LuaHook = function (this, go)
    if CClientMainPlayer.Inst == nil then
        return
    end

    local sendInfo = this.sendStatusPanel_statusInput.value
    if System.String.IsNullOrEmpty(sendInfo) then
        g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
        return
    end
    this.sendStatusPanel_statusInput.value = CQnSymbolParser.FilterExceededEmoticons(StringTrim(sendInfo), Constants.MaxEmotionNum)
    local msg = this.sendStatusPanel_statusInput.value
    if System.String.IsNullOrEmpty(msg) then
        this:ResetInputInfo()
        return
    end
    if Time.realtimeSinceStartup - this.lastSendTime < 1 then
        return
    end
    this.lastSendTime = Time.realtimeSinceStartup
    local sendText = CChatInputLink.Encapsulate(msg, this.existingLinks)
    sendText = CChatMgr.Inst:FilterYangYangEmotion(sendText)
    local roleInfo = CPersonalSpaceMgr.Inst:GetRoleInfo()
    --if (ReplyPersonId == ulong.Parse(roleInfo.id))
    --   ReplyPersonId = 0;
    local replyId = this.ReplyPersonId
    local replyName = this.ReplyPersonName

    CPersonalSpaceMgr.Inst:AddComment(this.m_Info.id, sendText, this.ReplyPersonId, DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data)
        if not this then
            return
        end
        if data.code == 0 then
            if this.gameObject.activeSelf then
                CPersonalSpaceMgr.Inst:GetSingleMomentInfo(this.m_Info.id, DelegateFactory.Action_CPersonalSpace_GetSingleMoment_Ret(function (ret)
                    if not this then
                        return
                    end
                    if ret.code == 0 then
                        if replyId ~= 0 then
                            this:SetInputDefaultValue(replyId, replyName, this.gameObject, this.m_Info)
                        end

                        --
                        local newItem = ret.data
                        newItem.type = this.m_Info.type
                        local updateList = Table2ArrayWithCount({newItem}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))

                        --CPersonalSpaceWnd.Instance.ReposFriendCircleList();
                        this.personalSpaceWnd:updateMomentCache(updateList, false, this.m_Info.type, this.m_FormerNode, true)

                        this:Init(newItem, this.m_FormerNode, UIWidget.Pivot.Top, false, false)
                    end
                end), nil)
            end
        end
    end), nil)
    this:ResetInputInfo()
end
