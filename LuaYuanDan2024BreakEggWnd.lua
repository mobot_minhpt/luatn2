local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Animation = import "UnityEngine.Animation"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr         = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CConversationMgr = import "L10.Game.CConversationMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CTrackMgr = import "L10.Game.CTrackMgr"
local EnumObjectType = import "L10.Game.EnumObjectType"
local CPublicMapMgr = import "L10.Game.CPublicMapMgr"

LuaYuanDan2024BreakEggWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2024BreakEggWnd, "NoHammer", "NoHammer", GameObject)
RegistChildComponent(LuaYuanDan2024BreakEggWnd, "Hammer", "Hammer", GameObject)
RegistChildComponent(LuaYuanDan2024BreakEggWnd, "BrokenEgg", "BrokenEgg", GameObject)
RegistChildComponent(LuaYuanDan2024BreakEggWnd, "FullEgg", "FullEgg", GameObject)
RegistChildComponent(LuaYuanDan2024BreakEggWnd, "NPCCoordinate", "NPCCoordinate", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanDan2024BreakEggWnd, "m_Animation")
RegistClassMember(LuaYuanDan2024BreakEggWnd, "m_AnimationTick")
RegistClassMember(LuaYuanDan2024BreakEggWnd, "m_BreakEggSound")
RegistClassMember(LuaYuanDan2024BreakEggWnd, "npcTemplateId")
RegistClassMember(LuaYuanDan2024BreakEggWnd, "sceneTemplateId")

function LuaYuanDan2024BreakEggWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    
	UIEventListener.Get(self.NPCCoordinate.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNPCCoordinateClick()
	end)


    --@endregion EventBind end
end

function LuaYuanDan2024BreakEggWnd:Init()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    self:CheckHammer()

    local YuanDanEggTaskNpc = YuanDan2024_Setting.GetData().YuanDanEggTaskNpc
    local TemplateId = g_LuaUtil:StrSplit(YuanDanEggTaskNpc,",")
    self.npcTemplateId = TemplateId[1]
    self.sceneTemplateId = g_LuaUtil:StrSplitAdv(TemplateId[2],";")[1]

    local sceneName = PublicMap_PublicMap.GetData(self.sceneTemplateId).Name

    local locations = CPublicMapMgr.Instance:GetObjectPositon(EnumObjectType.NPC, tonumber(self.npcTemplateId));
    local gridPos = {x = 0, y = 0}
    if locations ~= nil and CommonDefs.DictContains_LuaCall(locations, tonumber(self.sceneTemplateId)) then
        gridPos = locations[tonumber(self.sceneTemplateId)][0];
    end

    self.NPCCoordinate.text = SafeStringFormat3("[%s,%s,%s]", sceneName, gridPos.x, gridPos.y)
end


function LuaYuanDan2024BreakEggWnd:CheckHammer()
    local itemId = 21052444
    local have, pos, id = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Task, itemId)
    if have then
        self.Hammer:SetActive(true)
        self.NoHammer:SetActive(false)
        UIEventListener.Get(self.FullEgg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnFullEggClick()
        end)
        self.m_Animation:Play("yuandan2024breakeggwnd_show")
    else
        self.Hammer:SetActive(false)
        self.NoHammer:SetActive(true)
    end
end

function LuaYuanDan2024BreakEggWnd:BreakEgg()
    local itemId = 21052444
    local info = CConversationMgr.Inst.DialogInfo
    local have, pos, id = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Task, itemId)
    if have then
        if info ~= nil and info.m_NpcEngineId ~= nil then
            Gac2Gas.RequestUseItem(EnumItemPlace_lua.Task, pos, id, SafeStringFormat("%d", info.m_NpcEngineId))
        else
            Gac2Gas.RequestUseItem(EnumItemPlace_lua.Task, pos, id, "")
        end
        
    end
    if PlayerSettings.SoundEnabled then
        local sound = "event:/L10/L10_UI/2023ShiZhuangChouJiang/2023ShiZhuangChouJiang_6s"
        if self.m_BreakEggSound then SoundManager.Inst:StopSound(self.m_BreakEggSound) end
        self.m_BreakEggSound = SoundManager.Inst:PlayOneShot(sound, Vector3.zero, nil, 0)
    end
    
end

function LuaYuanDan2024BreakEggWnd:OnSendYuanDanJinChuiReward(rewards)
    self.m_Animation:Play("yuandan2024breakeggwnd_show_1")
    local rewardsInfo = g_MessagePack.unpack(rewards)
    local itemList = {}

    for i = 1, #rewardsInfo, 2 do
        table.insert(itemList, {ItemID = rewardsInfo[i], Count = rewardsInfo[i + 1]})
    end

    LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
    LuaCommonGetRewardWnd.m_Reward1List = itemList
    LuaCommonGetRewardWnd.m_Reward2Label = ""
    LuaCommonGetRewardWnd.m_Reward2List = {  }
    LuaCommonGetRewardWnd.m_hint = ""
    LuaCommonGetRewardWnd.m_button = {
        {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
    }
    
    self.m_AnimationTick = RegisterTickOnce(function ()
        CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
        CUIManager.CloseUI(CLuaUIResources.YuanDan2024BreakEggWnd);
    end, 0.5 * 1000)
end

function LuaYuanDan2024BreakEggWnd:OnEnable()
    g_ScriptEvent:AddListener("SendYuanDanJinChuiReward", self, "OnSendYuanDanJinChuiReward")
end

function LuaYuanDan2024BreakEggWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendYuanDanJinChuiReward", self, "OnSendYuanDanJinChuiReward")
    UnRegisterTick(self.m_AnimationTick)
end



--@region UIEvent

function LuaYuanDan2024BreakEggWnd:OnFullEggClick()
    self:BreakEgg()
end

function LuaYuanDan2024BreakEggWnd:OnNPCCoordinateClick()
    CTrackMgr.Inst:FindNPC(self.npcTemplateId, self.sceneTemplateId, 0, 0, nil, nil)
end


--@endregion UIEvent

