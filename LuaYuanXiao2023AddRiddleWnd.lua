LuaYuanXiao2023AddRiddleWnd = class()
LuaYuanXiao2023AddRiddleWnd.s_npcEngineId = nil

RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "node1")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "node2")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "node3")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "node4")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "submitBtn")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "refreshBtn")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "answerLabel")

RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "chooseIndex")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "chooseNode")
RegistClassMember(LuaYuanXiao2023AddRiddleWnd, "chooseTable")


function LuaYuanXiao2023AddRiddleWnd:Awake()
    self.chooseTable = {}

    for i = 1, 4 do
        self["node"..i] = self.transform:Find("ShowArea/Puzzle/chooseArea/node"..i).gameObject
    end
    
    self.submitBtn = self.transform:Find("ShowArea/Puzzle/submitBtn").gameObject
    self.refreshBtn = self.transform:Find("ShowArea/Puzzle/refreshBtn").gameObject
    self.answerLabel = self.transform:Find("ShowArea/Puzzle/answerLabel"):GetComponent(typeof(UILabel))
end

function LuaYuanXiao2023AddRiddleWnd:Init()
    self:RefreshRiddle()
    UIEventListener.Get(self.submitBtn).onClick = LuaUtils.VoidDelegate(function (go)
        self:SubmitInfo()
    end)
    UIEventListener.Get(self.node1).onClick = LuaUtils.VoidDelegate(function (go)
        self:ChooseNode(go, 1)
    end)
    UIEventListener.Get(self.node2).onClick = LuaUtils.VoidDelegate(function (go)
        self:ChooseNode(go, 2)
    end)
    UIEventListener.Get(self.node3).onClick = LuaUtils.VoidDelegate(function (go)
        self:ChooseNode(go, 3)
    end)
    UIEventListener.Get(self.node4).onClick = LuaUtils.VoidDelegate(function (go)
        self:ChooseNode(go, 4)
    end)
    UIEventListener.Get(self.refreshBtn).onClick = LuaUtils.VoidDelegate(function (go)
        self:RefreshRiddle()
    end)
end

function LuaYuanXiao2023AddRiddleWnd:RefreshRiddle() 
    self.chooseIndex = nil
    self.answerLabel.text = ""
    self:GetRandomRiddle()
    self:InitNode(self.node1, 1)
    self:InitNode(self.node2, 2)
    self:InitNode(self.node3, 3)
    self:InitNode(self.node4, 4)
end

function LuaYuanXiao2023AddRiddleWnd:GetRandomRiddle()
    local exist = {}
    for i = 1, #self.chooseTable do
        exist[self.chooseTable[i]] = true
    end

    self.chooseTable = {}
    
    for i = 1, 4 do
        while not self.chooseTable[i] or exist[self.chooseTable[i]] do
            self.chooseTable[i] = math.random(1, YuanXiao_QuestionPool2.GetDataCount())
        end
        exist[self.chooseTable[i]] = true
    end
end

function LuaYuanXiao2023AddRiddleWnd:SubmitInfo()
    if self.chooseIndex and LuaYuanXiao2023AddRiddleWnd.s_npcEngineId then
        local riddleId = self.chooseTable[self.chooseIndex]
        if riddleId then
            Gac2Gas.ConfirmCommitRiddle2023(LuaYuanXiao2023AddRiddleWnd.s_npcEngineId, riddleId)
            CUIManager.CloseUI(CLuaUIResources.YuanXiao2024AddRiddleWnd)
        end
    elseif not self.chooseIndex then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个灯谜!"))
    end
end

function LuaYuanXiao2023AddRiddleWnd:ChooseNode(node, index)
    local idIndex = self.chooseTable[index]
    if not idIndex then
        return
    end

    local data = YuanXiao_QuestionPool2.GetData(idIndex)
    if data then
        local lightNode = node.transform:Find("light").gameObject
        if self.chooseNode then
            self.chooseNode:SetActive(false)
        end
        self.chooseNode = lightNode
        lightNode:SetActive(true)
        self.chooseIndex = index
        self.answerLabel.text = data.Question
    end
end

function LuaYuanXiao2023AddRiddleWnd:InitNode(node,index)
    local idIndex = self.chooseTable[index]
    if not idIndex then
        return
    end

    local data = YuanXiao_QuestionPool2.GetData(idIndex)
    if data then
        local nameLabel = node.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local answer = {}
        for str in string.gmatch(data.Answer, "([^;]+)") do
            table.insert(answer, str)
        end
        nameLabel.text = answer[math.random(1, #answer)]
        local lightNode = node.transform:Find("light").gameObject
        lightNode:SetActive(false)
    end
end
