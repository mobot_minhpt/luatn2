-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildDevelopDetailView_Academy = import "L10.UI.CGuildDevelopDetailView_Academy"
local CGuildMgr = import "L10.Game.CGuildMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Guild_SanctumUpdate = import "L10.Game.Guild_SanctumUpdate"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildDevelopDetailView_Academy.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.startBuildBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickStartBuildButton, VoidDelegate, this)
    UIEventListener.Get(this.trainSettingBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickTrainSettingButton, VoidDelegate, this)
    this.descLabel.text = g_MessageMgr:FormatMessage("GUILD_SHUYUAN")
end
CGuildDevelopDetailView_Academy.m_OnClickStartBuildButton_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.BasicProp.GuildId ~= 0 then
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "Sanctum", "", "")
    end
end
CGuildDevelopDetailView_Academy.m_OnRequestOperationInGuildSucceed_CS2LuaHook = function (this, requestType, paramStr) 
    -- 房屋 建设成功 
    -- 房屋 建设成功 
    if (("Sanctum") == requestType) then
        CGuildMgr.Inst.canUpdateSanctum = false
        local updateTime = CGuildMgr.Inst:GetUpdateTime(CGuildDevelopDetailView_Academy.UPDATE_TYPE)
        local nowTime = CServerTimeMgr.Inst:GetZone8Time()
        local d = nowTime:AddSeconds(updateTime - 1)
        local duration = CommonDefs.op_Subtraction_DateTime_DateTime(d, nowTime)

        this:_UpdateSilder(duration, this.startBuildBtn, LocalString.GetString("正在建设"), 0)
        this.buildTimeLabel.text = System.String.Format(CGuildDevelopDetailView_Academy.TimeFormat, duration.Days, duration.Hours, duration.Minutes)

        if CGuildMgr.Inst.m_GuildInfo ~= nil then
            CGuildMgr.Inst.m_GuildInfo.CurBuild = requestType
            CGuildMgr.Inst.m_GuildInfo.CurBuildStartTime = math.floor(CServerTimeMgr.Inst.timeStamp)
            CGuildMgr.Inst:GetMyGuildInfo()
        end
    end
end
CGuildDevelopDetailView_Academy.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.RequestOperationInGuildSucceed, MakeDelegateFromCSFunction(this.OnRequestOperationInGuildSucceed, MakeGenericClass(Action2, String, String), this))
    if CGuildMgr.Inst.m_GuildInfo == nil then
        return
    end

    -- 修炼说明
    local sanctumNum = CGuildMgr.Inst.m_GuildInfo.SanctumNum
    local xiulian1 = Guild_SanctumUpdate.GetData(sanctumNum + 1).BasePractice
    local xiulian2 = Guild_SanctumUpdate.GetData(sanctumNum + 1).AntiControlPractice
    local xiulian3 = Guild_SanctumUpdate.GetData(sanctumNum + 1).EnhanceControlPractice
    local xiulian4 = Guild_SanctumUpdate.GetData(sanctumNum + 1).AntiElementPractice
    local xiulian5 = Guild_SanctumUpdate.GetData(sanctumNum + 1).IgnoreElementPractice
    local message = g_MessageMgr:FormatMessage("Guild_Construct_Sanctum_Intro", xiulian1, xiulian2, xiulian3, xiulian4, xiulian5)

    this.settingLabel.text = CChatLinkMgr.TranslateToNGUIText(message, false)

    -- 等级变量
    this.buildNumLabel.text = System.String.Format(LocalString.GetString("书院数量{0}/{1}"), CGuildMgr.Inst.m_GuildInfo.SanctumNum, CGuildMgr.Inst:GetMaxBuilding())

    -- 升级资金
    local costSilver = CGuildMgr.Inst:GetUpdateCost(CGuildDevelopDetailView_Academy.UPDATE_TYPE)
    if CGuildMgr.Inst:GetSilver() >= costSilver then
        this.startBuildBtn.Enabled = true
        this.costLabel.text = System.String.Format(LocalString.GetString("帮会资金[00ff00]{0}[-]"), costSilver)
    else
        this.startBuildBtn.Enabled = false
        this.costLabel.text = System.String.Format(LocalString.GetString("帮会资金[ff0000]{0}[-]"), costSilver)
    end

    CGuildMgr.Inst.canUpdateSanctum = true
    -- 升级条件1, 帮会资金
    local totalSilver = costSilver + 2 * CGuildMgr.Inst:GetMaintainCost()
    --requireLabel1.text = "帮会资金达到 " + totalSilver;
    local satify = true
    if CGuildMgr.Inst:GetSilver() >= totalSilver then
    else
        satify = false
    end
    this.needLabel.text = System.String.Format("{0}/{1}", CGuildMgr.Inst:GetSilver(), totalSilver)
    this.needSlider.value = (CGuildMgr.Inst:GetSilver() / totalSilver)

    -- 帮会等级
    local sizeLevelneedMin = CGuildMgr.Inst:GetMinSizeLevelForBuildingNum(CGuildMgr.Inst.m_GuildInfo.SanctumNum + 1)
    local sizeLevelneed = CGuildMgr.Inst.m_GuildInfo.Scale
    if CGuildMgr.Inst.m_GuildInfo.SanctumNum >= CGuildMgr.Inst:GetMaxBuilding() then
        satify = false
        sizeLevelneed = sizeLevelneed + 1
        CGuildMgr.Inst.canUpdateSanctum = false
        sizeLevelneedMin = sizeLevelneed

        satify = false
    end

    if sizeLevelneedMin > CGuildMgr.Inst.m_GuildInfo.Scale then
        this.requireLabel.color = Color.red
    else
        this.requireLabel.color = Color.green
    end

    this.requireLabel.text = System.String.Format(LocalString.GetString("帮会等级{0}级"), sizeLevelneedMin)

    if satify then
    else
        this.startBuildBtn.Enabled = false
        --requireLabel.color = Color.red;
        if CGuildMgr.Inst ~= nil then
            CGuildMgr.Inst.canUpdateSanctum = false
        end
    end

    -- 升级时间
    local updateTime = CGuildMgr.Inst:GetUpdateTime(CGuildDevelopDetailView_Academy.UPDATE_TYPE)

    if (CGuildMgr.Inst.m_GuildInfo.CurBuild == CGuildMgr.BUILD_TYPE_Sanctum) then
        local startTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CGuildMgr.Inst.m_GuildInfo.CurBuildStartTime)
        local nowTime = CServerTimeMgr.Inst:GetZone8Time()
        local duration = CommonDefs.op_Subtraction_DateTime_DateTime(startTime:AddSeconds(updateTime), nowTime)
        local totalSeconds = duration.TotalSeconds

        this.startBuildBtn.label.text = LocalString.GetString("正在建设")
        if totalSeconds > 0 then
            CGuildMgr.Inst.canUpdateSanctum = false
            this.buildTimeLabel.text = System.String.Format(CGuildDevelopDetailView_Academy.TimeFormat, duration.Days, duration.Hours, duration.Minutes)
        end
    else
        local timeStr = LocalString.GetString("需要")
        if math.floor(updateTime / 3600) > 0 then
            if math.floor((updateTime % 3600) / 60) > 0 then
                timeStr = SafeStringFormat3(LocalString.GetString("需要%s小时%s分钟"), tostring(math.floor(updateTime / 3600)), tostring((math.floor((updateTime % 3600) / 60))))
            else
                timeStr = SafeStringFormat3(LocalString.GetString("需要%s小时"), tostring(math.floor(updateTime / 3600)))
            end
        end

        this.buildTimeLabel.text = timeStr
        this.startBuildBtn.label.text = LocalString.GetString("开始建设")
    end

    -- 升级按钮是否可按
    local curBuild = CGuildMgr.Inst.m_GuildInfo.CurBuild
    if not CGuildMgr.Inst.canUpdateSanctum or not ((""==curBuild or curBuild==nil) or (CGuildMgr.BUILD_TYPE_Sanctum == curBuild)) then
        this.startBuildBtn.Enabled = false
    end

    -- 判断满级的情况
    local stringLimitMax = LocalString.GetString("已满级")
    if CGuildMgr.Inst.m_GuildInfo.SanctumNum >= CGuildMgr.Inst:GetMaxBuilding_All() then
        this.requireLabel.text = stringLimitMax
        this.requireLabel.color = Color.green
        --requireLabel2.gameObject.SetActive(false);
        --labelBuildingTime.text = stringLimitMax;
        this.startBuildBtn.Enabled = false
    end
end
