-- Auto Generated!!
local CFriendPopupMenuItem = import "L10.UI.CFriendPopupMenuItem"
CFriendPopupMenuItem.m_Init_CS2LuaHook = function (this, text, bgSpriteName, iconName) 
    this.label.text = text
    this.button:SetBackgroundSprite(bgSpriteName)
    if this.iconSprite ~= nil then
        if not System.String.IsNullOrEmpty(iconName) then
            this.iconSprite.enabled = true
            this.iconSprite.spriteName = iconName
        else
            this.iconSprite.enabled = false
        end
    end
end
