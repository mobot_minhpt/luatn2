local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local Item_Item = import "L10.Game.Item_Item" -- 为了用一下CUICommonDef.GetItemCellBorder方法
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local CTrackMgr = import "L10.Game.CTrackMgr"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"

LuaFishCreelWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFishCreelWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaFishCreelWnd, "AddBtn", "AddBtn", GameObject)
RegistChildComponent(LuaFishCreelWnd, "DetailView", "DetailView", GameObject)
RegistChildComponent(LuaFishCreelWnd, "DetailIcon", "DetailIcon", CUITexture)
RegistChildComponent(LuaFishCreelWnd, "DetailBorder", "DetailBorder", UISprite)
RegistChildComponent(LuaFishCreelWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaFishCreelWnd, "LevelLabl", "LevelLabl", UILabel)
RegistChildComponent(LuaFishCreelWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaFishCreelWnd, "LingQiLabel", "LingQiLabel", UILabel)
RegistChildComponent(LuaFishCreelWnd, "ShuXingLabel", "ShuXingLabel", UILabel)
RegistChildComponent(LuaFishCreelWnd, "TitleTemplate", "TitleTemplate", UILabel)
RegistChildComponent(LuaFishCreelWnd, "LeftBackground", "LeftBackground", UISprite)

--@endregion RegistChildComponent end
RegistClassMember(LuaFishCreelWnd, "m_ShuXingYuList")
RegistClassMember(LuaFishCreelWnd, "m_SelectedFishInedx")
RegistClassMember(LuaFishCreelWnd, "m_OpenType")
RegistClassMember(LuaFishCreelWnd, "m_AddHeight")
RegistClassMember(LuaFishCreelWnd, "m_PreHeight")
RegistClassMember(LuaFishCreelWnd, "m_FullDuration")
RegistClassMember(LuaFishCreelWnd, "m_HintLabel")
RegistClassMember(LuaFishCreelWnd, "m_IsListEmpty")

function LuaFishCreelWnd:Awake()
	self.m_ShuXingYuList = {}
	self.m_SelectedFishInedx = nil
	self.m_FullDuration = HouseFish_Setting.GetData().ZhengzhaiFishDuration
	self.m_IsListEmpty = false
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.AddBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddBtnClick()
	end)
    --@endregion EventBind end
	self.m_HintLabel = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
	self.m_HintLabel.gameObject:SetActive(false)
end

function LuaFishCreelWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateFishBasket",self,"OnUpdateFishBasket")
end

function LuaFishCreelWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateFishBasket",self,"OnUpdateFishBasket")
end

function LuaFishCreelWnd:InitFishBasket()
	self.m_ShuXingYuList = {}

	if self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Put then
		local fishbasket = CClientMainPlayer.Inst.ItemProp.FishBasket
		if not fishbasket then return end 
		local itemIdArray = fishbasket.Fishs
		local countArray = fishbasket.Count
		--添加鱼篓里的属性鱼
		for i=0,itemIdArray.Length-1,1 do
			local t = {}
			t.itemId = itemIdArray[i]
			t.count = countArray[i]
			t.fishbagpos = i
			local data = HouseFish_AllFishes.GetData(t.itemId)
			if data and data.IsZhenZhai == 1 then
				t.duration = self.m_FullDuration
				table.insert(self.m_ShuXingYuList,t)
			end
		end
		--添加包裹里的属性鱼
		local n = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
		for i=0,n-1,1 do
			local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
			if itemId then
				local item = CItemMgr.Inst:GetById(itemId)
				local data = HouseFish_AllFishes.GetData(item.TemplateId)
				if data and data.IsZhenZhai == 1 then
					local t = {}
					t.itemId = item.TemplateId
					t.fishbagpos = nil
					t.bagPos = i+1
					t.count = item.Item.Count
					t.stringId = itemId
					--包裹裏的魚是有单独耐久的 
					if item.Item.ExtraVarData and item.Item.ExtraVarData.Data then
						local zhenzhaiyu = CZhengzhaiFishType()
						if zhenzhaiyu:LoadFromString(item.Item.ExtraVarData.Data) then
							t.duration = zhenzhaiyu.Duration
							t.wordId = zhenzhaiyu.WordId
						end
					end
					table.insert(self.m_ShuXingYuList,t)
				end
			end
		end
		if #self.m_ShuXingYuList == 0 then
			self.m_HintLabel.gameObject:SetActive(true)
			self.m_HintLabel.text = CUICommonDef.TranslateToNGUIText(LocalString.GetString("前往[ffc800]隐仙湾[-]海钓,获得的[ff5050]红[-],[ff88ff]紫[-]品质观赏鱼可作为佑宅福鱼"))
			self:InitButtons(true)
		end
	elseif self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Attach then
		for i,data in ipairs(LuaSeaFishingMgr.m_PlacedZhenZhaiYu) do
			local count = data.count
			if not data.attachedFaBaoId or data.attachedFaBaoId == 0 then
				for c=1,count,1 do
					local t = {}
					t.fishmapId = data.fishmapId
					t.itemId = Zhuangshiwu_Zhuangshiwu.GetData(data.itemId).ItemId
					t.count = count
					t.duration = data.duration
					t.wordId = data.wordId
					table.insert(self.m_ShuXingYuList,t)
				end
			end
		end
		if #self.m_ShuXingYuList == 0 then
			self.m_HintLabel.gameObject:SetActive(true)
			self:InitButtons(true)
		end
	elseif self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Clear or self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Inherit then 
		--添加包裹里的属性鱼
		local n = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
		for i=0,n-1,1 do
			local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
			if itemId then
				local item = CItemMgr.Inst:GetById(itemId)
				local data = HouseFish_AllFishes.GetData(item.TemplateId)
				if data and data.IsZhenZhai == 1 then
					local t = {}
					t.itemId = item.TemplateId
					t.fishbagpos = nil
					t.bagPos = i+1
					t.count = item.Item.Count
					t.stringId = itemId
					--包裹裏的魚是有单独耐久的
					if item.Item.ExtraVarData and item.Item.ExtraVarData.Data then
						local zhenzhaiyu = CZhengzhaiFishType()
						if zhenzhaiyu:LoadFromString(item.Item.ExtraVarData.Data) then
							t.duration = zhenzhaiyu.Duration
							t.wordId = zhenzhaiyu.WordId
						end
					end
					table.insert(self.m_ShuXingYuList,t)
				end
			end
		end
		--镇宅鱼界面里的鱼也需要添加进来
		if CClientHouseMgr.Inst.mCurFurnitureProp2 and CClientMainPlayer.Inst then
			local fishInfoType = CClientHouseMgr.Inst.mCurFurnitureProp2.FishInfo
			local dic = fishInfoType.ZhengzhaiFish
    		local playerId = CClientMainPlayer.Inst.Id
			if CommonDefs.DictContains_LuaCall(dic, playerId) then
				local zhengzhaiFishInfoType = CommonDefs.DictGetValue_LuaCall(dic, playerId)
				local zhenzhaiFishMap = zhengzhaiFishInfoType.AllFishes
				CommonDefs.DictIterate(zhenzhaiFishMap, DelegateFactory.Action_object_object(function (___key, ___value) 
					local data = {}
					data.fishmapId = tonumber(___key)
					data.zswId = ___value.TemplateId
					data.duration = ___value.Duration
					data.wordId = ___value.WordId
					data.attachedFaBaoId = ___value.AttachedFaBaoId
					data.count = 1				
					if Zhuangshiwu_Zhuangshiwu.GetData(data.zswId) then
						data.itemId = Zhuangshiwu_Zhuangshiwu.GetData(data.zswId).ItemId
						table.insert(self.m_ShuXingYuList,data)
					end
				end))
			end
		end
	end

end

function LuaFishCreelWnd:Init()
	self.m_PreHeight = self.DescLabel.height
	self.m_OpenType = LuaZhenZhaiYuMgr.m_FishCreelOpenType
	if self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Attach then
		self.TitleTemplate.text = SafeStringFormat3(LocalString.GetString("选择关联佑宅福鱼"))
	else
		self.TitleTemplate.text = SafeStringFormat3(LocalString.GetString("选择佑宅福鱼"))
	end
	self:InitFishBasket()
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_ShuXingYuList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

	self:OnRefreshDetailView(0)
	--self.TableView:SetSelectRow(0,true)
	self.TableView:ReloadData(false, false)
end

--@region UIEvent

function LuaFishCreelWnd:OnAddBtnClick()
	if self.m_IsListEmpty and self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Attach then
		self:OnEditPoolBtnClick()
		return
	elseif self.m_IsListEmpty and self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Put then
		self:OnGoFishingBtnClick()
		return
	end

	if self.m_SelectedFishInedx then
		if self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Put then
			local fish = self.m_ShuXingYuList[self.m_SelectedFishInedx]
			if fish.count <=0 then return end
			--从鱼篓
			if fish.fishbagpos then
				Gac2Gas.PlaceZhengzhaiFish(tonumber(fish.fishbagpos))
			--从包裹
			elseif fish.bagPos then
				local place = EnumItemPlace.Bag
				local pos = fish.bagPos
				local itemId = fish.stringId
				Gac2Gas.PlaceZhengzhaiFishItem(place,pos,itemId)-- place, pos, itemId
			end
		elseif self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Attach then
			local fish = self.m_ShuXingYuList[self.m_SelectedFishInedx]
			local fishId = Zhuangshiwu_Zhuangshiwu.GetDataBySubKey("ItemId",fish.itemId).ID
			local fabaoId = LuaZhenZhaiYuMgr.m_AttachedFabaoId
			local fishmapId = fish.fishmapId
			Gac2Gas.AttachZhengzhaiFishToFabao(fishmapId,fabaoId)--fishId,fabaoId
		elseif self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Clear then
			local fish = self.m_ShuXingYuList[self.m_SelectedFishInedx]
			if not fish.wordId or fish.wordId == 0 then
				g_MessageMgr:ShowMessage("Please_Choose_Zhenzhaiyu_With_Word")
			else
				g_ScriptEvent:BroadcastInLua("SelectFishForClearing",fish)
				CUIManager.CloseUI(CLuaUIResources.FishCreelWnd)
			end
		elseif self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Inherit then
			local fish = self.m_ShuXingYuList[self.m_SelectedFishInedx]
			if not fish.wordId or fish.wordId == 0 then
				g_ScriptEvent:BroadcastInLua("SelectFishForInheriting",fish)
				CUIManager.CloseUI(CLuaUIResources.FishCreelWnd)
			else
				g_MessageMgr:ShowMessage("Please_Choose_Zhenzhaiyu_Without_Word")
			end
		end
	end
end

function LuaFishCreelWnd:OnEditPoolBtnClick()
	g_ScriptEvent:BroadcastInLua("ShowZhenZhaiFishEditingView")
end

function LuaFishCreelWnd:OnGoFishingBtnClick()
	local location = HouseFish_Setting.GetData().GoFishingLocation
	local sceneId = HouseFish_Fishing.GetData().FishingSceneTempId
	local gridPos = CPos(location[0],location[1])
	local pixelPos = Utility.GridPos2PixelPos(gridPos)

	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Go_YinXianWan_Fishing_Comfirm"), 
        DelegateFactory.Action(function ()            
            CTrackMgr.Inst:Track(nil, sceneId, pixelPos, Utility.Grid2Pixel(2.0), nil, nil, nil, nil, true)
        end),
        nil,
    	LocalString.GetString("前往"), LocalString.GetString("取消"), false)
end


--@endregion UIEvent
function LuaFishCreelWnd:InitButtons(isEmpty)
	self.m_IsListEmpty = isEmpty
	local label = self.AddBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
	if self.m_IsListEmpty and self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Attach then
		label.text = LocalString.GetString("编辑鱼池")
	elseif self.m_IsListEmpty and self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Put then
		label.text = LocalString.GetString("前往钓鱼")
	else
		label.text = LocalString.GetString("添加")
	end
end

function LuaFishCreelWnd:InitItem(item, row)
	local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local border = item.transform:Find("Border"):GetComponent(typeof(UISprite))
	local countLabel = item.transform:Find("Count"):GetComponent(typeof(UILabel))
	local conflic = item.transform:Find("Conflic").gameObject
	local conflicLabel = item.transform:Find("Conflic/Label").gameObject

	local fish = self.m_ShuXingYuList[row+1]
	local itemId = fish.itemId
	local count = fish.count
	countLabel.text = count
	conflic:SetActive(false)

	local itemdata = Item_Item.GetData(itemId)
	if itemdata then
		local path = itemdata.Icon
		icon:LoadMaterial(path)
		border.spriteName = CUICommonDef.GetItemCellBorder(itemdata.NameColor)
	end
	if count <=0 then
		item.gameObject:SetActive(false)
	end

	if self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Attach then
		local isConflct = false
		local fishWord = fish.wordId
		if fishWord and fishWord ~= 0 then
			local fisgCls = math.floor(fishWord/100)
			local data = HouseFish_WordConflict.GetData(fisgCls)
			local curSuitWords = LuaZhenZhaiYuMgr.m_AttachSuitWords
			--	是否和法宝套装属性互斥
			if data then
				local conflicts = data.Conflict
				for i=0,conflicts.Length-1,1 do
					local cls = conflicts[i]
					for j=1,#curSuitWords,1 do
						if cls == math.floor(curSuitWords[j]/100) then
							isConflct = true
							break
						end
					end
					if isConflct then
						break
					end
				end
			end
			-- 是否和已关联的镇宅鱼属性相同
			local attachedFishs = LuaZhenZhaiYuMgr.m_AttachedFishs
			for i,data in pairs(attachedFishs) do
				local fworldId = data.wordId
				if fisgCls == math.floor(fworldId/100) then
					isConflct = true
				end
			end
		end
		if isConflct then
			conflic:SetActive(true)
		end
	elseif self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Clear then
		conflic:SetActive(not fish.wordId or fish.wordId == 0)
		conflicLabel:SetActive(false)
	elseif self.m_OpenType == LuaZhenZhaiYuMgr.FrishCreelType.Inherit then
		conflic:SetActive(fish.wordId and fish.wordId ~= 0)
		conflicLabel:SetActive(false)
	end

	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    local fish = self.m_ShuXingYuList[row+1]
		if fish.count <= 0 then
			self:OnRefreshDetailView(0)
			return
		end
		self.m_SelectedFishInedx = row + 1
		self:OnRefreshDetailView(row + 1)
		self.TableView:SetSelectRow(row,true)
	end)

end

function LuaFishCreelWnd:OnRefreshDetailView(index)
	if index <= 0 then
		self.DetailView:SetActive(false)
		return
	end
	if not self.DetailView.activeSelf then
		self.DetailView:SetActive(true)
	end
	local fish = self.m_ShuXingYuList[index]
	local itemId = fish.itemId
	local count = fish.count
	local itemdata = Item_Item.GetData(itemId)

	if itemdata then
		local fishdata = HouseFish_AllFishes.GetData(itemId)
		local path = itemdata.Icon
		self.DetailIcon:LoadMaterial(path)
		self.DetailBorder.spriteName = CUICommonDef.GetItemCellBorder(itemdata.NameColor)
		self.NameLabel.text = itemdata.Name
        self.NameLabel.color = GameSetting_Common_Wapper.Inst:GetColor(itemdata.NameColor)
		self.LevelLabl.text = SafeStringFormat3(LocalString.GetString("%d级"),fishdata.Level)
		local preHeight = self.DescLabel.height
		local desc = System.String.Format(itemdata.Description, fishdata.Score)
		self.DescLabel.text = CChatLinkMgr.TranslateToNGUIText(desc,false)
		local addHeight = self.DescLabel.height - preHeight
		self.LeftBackground.height = self.LeftBackground.height + addHeight
		local pos1 = self.LingQiLabel.transform.localPosition
		pos1.y = pos1.y - addHeight
		self.LingQiLabel.transform.localPosition = pos1
		local pos2 = self.ShuXingLabel.transform.localPosition
		pos2.y = pos2.y - addHeight
		self.ShuXingLabel.transform.localPosition = pos2

		--根据供奉的法宝来的
		local word = fish.wordId
        if word and word ~= 0 then
            self.ShuXingLabel.text = Word_Word.GetData(word).Description
		else
			self.ShuXingLabel.text = LocalString.GetString("暂无")
        end
		local duration = fish.duration
		if duration and duration ~=0 then
			self.LingQiLabel.text = duration
		else
			self.LingQiLabel.text = 0
		end 
	end
end

function LuaFishCreelWnd:OnUpdateFishBasket(fishBasketType)
	CClientMainPlayer.Inst.ItemProp.FishBasket = fishBasketType
	self:InitFishBasket()
	self.TableView:ReloadData(false,false)
end
