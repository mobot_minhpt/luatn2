-- Auto Generated!!
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CSkillUpgradeTopView = import "L10.UI.CSkillUpgradeTopView"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local Single = import "System.Single"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local SkillCategory = import "L10.Game.SkillCategory"
local SkillKind = import "L10.Game.SkillKind"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UIntUIntKeyValuePair = import "L10.Game.UIntUIntKeyValuePair"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local EnumClass = import "L10.Game.EnumClass"
local __Skill_AllSkills_Template = import "L10.Game.__Skill_AllSkills_Template"

CSkillUpgradeTopView.m_Start_CS2LuaHook = function (this) 

    this.skillTemplate:SetActive(false)
    this.scaleTemplate:SetActive(false)
    this.arrowTemplate:SetActive(false)
end
CSkillUpgradeTopView.m_Init_CS2LuaHook = function (this, index) 

    if not this.inited or this.skillSeries == nil then
        this.inited = true
        this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
        if CClientMainPlayer.Inst ~= nil then
            if this.skillSeries == nil then
                this.skillSeries = CClientMainPlayer.Inst.SkillSeries
            end
            if this.skillSeries ~= nil then
                local labels = CreateFromClass(MakeArrayClass(System.String), this.skillSeries.Length)

                do
                    local i = 0
                    while i < labels.Length do
                        labels[i] = this.skillSeries[i].Value
                        i = i + 1
                    end
                end
                this.tabBar:InitWithLabels(labels)
            end
        end
    end
    this.tabBar:ChangeTab(index, false)
end
CSkillUpgradeTopView.m_OnTabChange_CS2LuaHook = function (this, go, index) 

    this:LoadSkills()
end
CSkillUpgradeTopView.m_OnEnable_CS2LuaHook = function (this) 

    EventManager.AddListener(EnumEventType.MainPlayerSkillPropUpdate, MakeDelegateFromCSFunction(this.SkillPropUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CSkillUpgradeTopView.m_OnDisable_CS2LuaHook = function (this) 

    EventManager.RemoveListener(EnumEventType.MainPlayerSkillPropUpdate, MakeDelegateFromCSFunction(this.SkillPropUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CSkillUpgradeTopView.m_LoadSkills_CS2LuaHook = function (this) 

    this:Clear()
    if this.skillSeries == nil or this.tabBar.SelectedIndex < 0 or this.tabBar.SelectedIndex >= this.skillSeries.Length then
        return
    end
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        --获取mainPlayer的所有职业技能
        local prefix = 900 + EnumToInt(mainPlayer.Class)
        local professionSkillIds = CreateFromClass(MakeGenericClass(List, UIntUIntKeyValuePair))
        local unlockLevels = CreateFromClass(MakeGenericClass(List, UInt32))
        local layoutDict = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Single))
        local unlockLevels_Special ={}
        local isYingLingJueJi = ((mainPlayer.Class == EnumClass.YingLing) and (this.skillSeries[this.tabBar.SelectedIndex].Key == "4"))

        __Skill_AllSkills_Template.ForeachKey(DelegateFactory.Action_object(function (cls)
            if math.floor(cls / 1000) == prefix then
                --以(900+ClassId)开头，以01结尾
                local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1)
                if not CLuaDesignMgr.Skill_AllSkills_IsOneLevelSkill(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) then
                    local data = Skill_AllSkills.GetData(skillId)
                    if (data.ECategory == SkillCategory.Active or data.ECategory == SkillCategory.Passive) and data.EKind == SkillKind.CommonSkill then
                        if data.SeriesName ~= this.skillSeries[this.tabBar.SelectedIndex].Key then
                            return
                        end

                        if CSkillMgr.Inst:IsColorSystemBaseSkill(cls) then
                            --对技能进行替换
                            local replaceSkillCls = mainPlayer.SkillProp:GetInEffectColorSystemSkill(cls)
                            skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(replaceSkillCls, 1)
                            data = Skill_AllSkills.GetData(skillId)
                            cls = replaceSkillCls
                        elseif CSkillMgr.Inst:IsColorSystemSkill(cls) then
                            return
                        end

                        if isYingLingJueJi then
                            table.insert(unlockLevels_Special, {["layout"] = data.Layout[1] / 1000, ["level"] = data.PlayerLevel})
                        elseif not CommonDefs.ListContains(unlockLevels, typeof(UInt32), data.PlayerLevel) then
                            CommonDefs.ListAdd(unlockLevels, typeof(UInt32), data.PlayerLevel)
                            CommonDefs.DictAdd(layoutDict, typeof(UInt32), data.PlayerLevel, typeof(Single), data.Layout[1] / 1000)
                        end
                        if mainPlayer.SkillProp:IsOriginalSkillClsExist(cls) then
                            CommonDefs.ListAdd(professionSkillIds, typeof(UIntUIntKeyValuePair), CreateFromClass(UIntUIntKeyValuePair, mainPlayer.SkillProp:GetOriginalSkillIdByCls(cls), data.PlayerLevel))
                        else
                            CommonDefs.ListAdd(professionSkillIds, typeof(UIntUIntKeyValuePair), CreateFromClass(UIntUIntKeyValuePair, skillId, data.PlayerLevel))
                        end
                    end
                end
            end
        end))
        if unlockLevels.Count == 0 and not isYingLingJueJi then
            return
        end
        CommonDefs.ListSort1(unlockLevels, typeof(UInt32), DelegateFactory.Comparison_uint(function (level1, level2) 
            return NumberCompareTo(level1, level2)
        end))

        local horizontalGap = 900 / math.max(2, unlockLevels.Count - 1)

        if isYingLingJueJi then
            for i = 1,#unlockLevels_Special do
                local item = NGUITools.AddChild(this.scaleLine.gameObject, this.scaleTemplate)
                item:SetActive(true)
                Extensions.SetLocalPositionX(item.transform, unlockLevels_Special[i]["layout"] * 900 --[[TotalLength]] + 50 --[[LeftOffset]])
                CommonDefs.GetComponent_GameObject_Type(item, typeof(UILabel)).text = tostring(unlockLevels_Special[i]["level"])
                CommonDefs.GetComponentInChildren_GameObject_Type(item, typeof(UISprite)):ResetAndUpdateAnchors()
            end
        else
            do
                local i = 0
                while i < unlockLevels.Count do
                    local item = NGUITools.AddChild(this.scaleLine.gameObject, this.scaleTemplate)
                    item:SetActive(true)
                    Extensions.SetLocalPositionX(item.transform, CommonDefs.DictGetValue(layoutDict, typeof(UInt32), unlockLevels[i]) * 900 --[[TotalLength]] + 50 --[[LeftOffset]])
                    CommonDefs.GetComponent_GameObject_Type(item, typeof(UILabel)).text = tostring(unlockLevels[i])
                    CommonDefs.GetComponentInChildren_GameObject_Type(item, typeof(UISprite)):ResetAndUpdateAnchors()
                    i = i + 1
                end
            end
        end

        if mainPlayer.Class == EnumClass.YingLing then
            professionSkillIds = LuaSkillMgr.GetYingLingNewProfessionSkillIds(professionSkillIds,  EnumToInt(mainPlayer.CurYingLingState))
        end

        do
            local i = 0

            --去除影灵的普通攻击
            if mainPlayer.Class == EnumClass.YingLing and this.skillSeries[this.tabBar.SelectedIndex].Key == "1" and EnumToInt(mainPlayer.CurYingLingState) ~= 1 then
                i = 1
            end
            while i < professionSkillIds.Count do
                local item = NGUITools.AddChild(this.scaleLine.gameObject, this.skillTemplate)
                item:SetActive(true)
                local data = Skill_AllSkills.GetData(professionSkillIds[i].Key)
                local cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CSkillItemCell))
                local clsExists = mainPlayer.SkillProp:IsOriginalSkillClsExist(data.SkillClass)
                local originSkillId = clsExists and mainPlayer.SkillProp:GetOriginalSkillIdByCls(data.SkillClass) or 0
                local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
                local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(data.SkillClass, mainPlayer.Level)) or 0

                local canLearnText = nil
                if originWithDeltaLevel == 0 then
                    local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
                    if CommonDefs.DictContains(skillBooks, typeof(UInt32), data.SkillClass) then
                        local book = Item_Item.GetData(CommonDefs.DictGetValue(skillBooks, typeof(UInt32), data.SkillClass).Key)
                        if book ~= nil then
                            --此时data为1级的技能
                            local unlock = LuaSkillMgr.PreLevelConditionIsFit(data) and LuaSkillMgr.PreSkillConditionIsFit(data)
                            if unlock then
                                local pos
                                local bookId
                                local default
                                default, pos, bookId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, book.ID)
                                if default then
                                    canLearnText = CSkillUpgradeTopView.CanLearnSkillText
                                else
                                    canLearnText = CSkillUpgradeTopView.CanNotLearnSkillText
                                end
                            end
                        end
                    end
                end

                local delta = clsExists and originWithDeltaLevel - mainPlayer.SkillProp:GetFeiShengModifyLevel(originSkillId, data.Kind, mainPlayer.Level) or 0
                cell:Init(data.SkillClass, originLevel, delta, CSkillInfoMgr.s_ShowSkillDeltaLevelInSkillUpgradeView, data.SkillIcon, originWithDeltaLevel == 0, canLearnText)
                UIEventListener.Get(item).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(item).onClick, MakeDelegateFromCSFunction(this.OnSkillItemClick, VoidDelegate, this), true)
                item.transform.localPosition = Vector3((data.Layout[1] / 1000) * 900 --[[TotalLength]] + 50 --[[LeftOffset]], data.Layout[0] > 0 and 100 or - 100, 0)
                CommonDefs.DictAdd(this.itemCellDict, typeof(UInt32), cell.ClassId, typeof(CSkillItemCell), cell)
                CommonDefs.ListAdd(this.itemCells, typeof(CSkillItemCell), cell)
                i = i + 1
            end
        end

        do
            local i = 0
            while i < professionSkillIds.Count do
                local data = Skill_AllSkills.GetData(professionSkillIds[i].Key)
                local _pairs = data.AdjustedPreSkills
                if _pairs ~= nil and _pairs.Length > 0 then
                    do
                        local j = 0
                        while j < _pairs.Length do
                            local realFromSkillCls = _pairs[j][0]
                            if CSkillMgr.Inst:IsColorSystemBaseSkill(realFromSkillCls) then
                                realFromSkillCls = mainPlayer.SkillProp:GetInEffectColorSystemSkill(realFromSkillCls)
                            end
                            -- 影灵破云击特殊处理
                            if LuaSkillMgr.GetYingLingSkillsCache() then
                                local cls = LuaSkillMgr.GetYingLingRealSkillCls(realFromSkillCls,EnumToInt(mainPlayer.CurYingLingState))
                                if cls then
                                    realFromSkillCls = cls
                                end
                            end

                            if CommonDefs.DictContains(this.itemCellDict, typeof(UInt32), realFromSkillCls) then
                                --生成一条箭头
                                local item = NGUITools.AddChild(this.scaleLine.gameObject, this.arrowTemplate)
                                item:SetActive(true)
                                local sprite = CommonDefs.GetComponent_GameObject_Type(item, typeof(UISprite))
                                local from = CommonDefs.DictGetValue(this.itemCellDict, typeof(UInt32), realFromSkillCls)
                                local to = CommonDefs.DictGetValue(this.itemCellDict, typeof(UInt32), CLuaDesignMgr.Skill_AllSkills_GetClass(professionSkillIds[i].Key))
                                sprite.width = math.floor((to.transform.localPosition.x - from.transform.localPosition.x - to.Width * 0.5 - from.Width * 0.5))
                                sprite.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(from.transform.localPosition, CommonDefs.op_Multiply_Vector3_Single(CommonDefs.op_Multiply_Vector3_Single(Vector3.right, from.Width), 0.5))
                            end
                            j = j + 1
                        end
                    end
                end
                i = i + 1
            end
        end

        if this.itemCells.Count > 0 then
            local idx = 0
            --for (int i = 0; i < items.Count; ++i)
            --{
            --    if (items[i].ClassId == CSkillInfoMgr.updateSkillInfo.skillClass)
            --    {
            --        idx = i;
            --        break;
            --    }
            --}
            this:OnSkillItemClick_WithShowTipOption(this.itemCells[idx].gameObject, false)
        end
    end
end
CSkillUpgradeTopView.m_SkillPropUpdate_CS2LuaHook = function (this) 

    this:UpdateSkills()
end
CSkillUpgradeTopView.m_OnSendItem_CS2LuaHook = function (this, itemId) 

    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.IsItem and (item.Item.Type == EnumItemType_lua.Book or item.Item.Type == EnumItemType_lua.Jueji) then
        this:UpdateSkills()
    end
end
CSkillUpgradeTopView.m_OnSetItemAt_CS2LuaHook = function (this, place, pos, oldId, newId) 

    local item = CItemMgr.Inst:GetById(newId)
    if item ~= nil and item.IsItem and (item.Item.Type == EnumItemType_lua.Book or item.Item.Type == EnumItemType_lua.Jueji) then
        this:UpdateSkills()
    end
end
CSkillUpgradeTopView.m_UpdateSkills_CS2LuaHook = function (this) 

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        do
            local i = 0
            while i < this.itemCells.Count do
                local classId = this.itemCells[i].ClassId

                if CSkillMgr.Inst:IsColorSystemBaseSkill(classId) or CSkillMgr.Inst:IsColorSystemSkill(classId) then
                    --对技能进行替换
                    local replaceSkillCls = mainPlayer.SkillProp:GetInEffectColorSystemSkill(classId)
                    if replaceSkillCls ~= classId then
                        --色系技能发生了变化
                        classId = replaceSkillCls
                        this.itemCells[i]:UpdateColorSystemSkillIcon(classId)
                    end
                end

                local clsExists = mainPlayer.SkillProp:IsOriginalSkillClsExist(classId)
                local originSkillId = clsExists and mainPlayer.SkillProp:GetOriginalSkillIdByCls(classId) or 0
                local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
                local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(classId, mainPlayer.Level)) or 0

                local kind = clsExists and Skill_AllSkills.GetData(originSkillId).Kind or 0
                local delta = clsExists and originWithDeltaLevel - mainPlayer.SkillProp:GetFeiShengModifyLevel(originSkillId, kind, mainPlayer.Level) or 0

                local canLearnText = nil
                if originWithDeltaLevel == 0 then
                    local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
                    if CommonDefs.DictContains(skillBooks, typeof(UInt32), classId) then
                        local data = Skill_AllSkills.GetData(classId * 100 + 1)
                        local book = Item_Item.GetData(CommonDefs.DictGetValue(skillBooks, typeof(UInt32), classId).Key)
                        if book ~= nil then
                            --此时data为1级的技能
                            local unlock = LuaSkillMgr.PreLevelConditionIsFit(data) and LuaSkillMgr.PreSkillConditionIsFit(data)
                            if unlock then
                                local pos
                                local bookId
                                local default
                                default, pos, bookId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, book.ID)
                                if default then
                                    canLearnText = CSkillUpgradeTopView.CanLearnSkillText
                                else
                                    canLearnText = CSkillUpgradeTopView.CanNotLearnSkillText
                                end
                            end
                        end
                    end
                end

                this.itemCells[i]:UpdateLevel(classId, originLevel, delta, CSkillInfoMgr.s_ShowSkillDeltaLevelInSkillUpgradeView, originLevel == 0, canLearnText)
                i = i + 1
            end
        end
    end
    if this.OnSkillClassSelect ~= nil then
        local cls = 0
        if this.selectedIndex >= 0 and this.selectedIndex < this.itemCells.Count then
            cls = this.itemCells[this.selectedIndex].ClassId
        end
        GenericDelegateInvoke(this.OnSkillClassSelect, Table2ArrayWithCount({cls}, 1, MakeArrayClass(Object)))
    end
end
CSkillUpgradeTopView.m_Clear_CS2LuaHook = function (this) 

    Extensions.RemoveAllChildren(this.scaleLine.transform)
    CommonDefs.DictClear(this.itemCellDict)
    CommonDefs.ListClear(this.itemCells)
end
CSkillUpgradeTopView.m_OnSkillItemClick_WithShowTipOption_CS2LuaHook = function (this, go, showTip) 

    do
        local i = 0
        while i < this.itemCells.Count do
            if go:Equals(this.itemCells[i].gameObject) then
                this.selectedIndex = i
                this.itemCells[i].Selected = true
                --if (showTip && s_EnableShowSkillTip && CClientMainPlayer.Inst != null && CClientMainPlayer.Inst.Class == EnumClass.HuaHun)
                --    CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(itemCells[i].ClassId, itemCells[i].OriginWithDeltaLevel == 0 ? 1 : itemCells[i].OriginWithDeltaLevel), tipTopPos.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillUpdate);
            else
                this.itemCells[i].Selected = false
            end
            i = i + 1
        end
    end
    if this.OnSkillClassSelect ~= nil then
        GenericDelegateInvoke(this.OnSkillClassSelect, Table2ArrayWithCount({this.itemCells[this.selectedIndex].ClassId}, 1, MakeArrayClass(Object)))
    end
end
CSkillUpgradeTopView.m_GetSkillTab_CS2LuaHook = function (this, skillClass) 

    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(skillClass) then
            local skillId = CClientMainPlayer.Inst.SkillProp:GetOriginalSkillIdByCls(skillClass)
            local data = Skill_AllSkills.GetData(skillId)
            if data ~= nil then
                return this.tabBar:GetTabGoByIndex(data.SeriesIndex - 1)
            end
        end
    end
    return nil
end
CSkillUpgradeTopView.m_GetSkillItem_CS2LuaHook = function (this, skillClass) 

    if this.itemCells.Count > 0 then
        do
            local i = 0
            while i < this.itemCells.Count do
                if this.itemCells[i].ClassId == skillClass then
                    return this.itemCells[i].gameObject
                end
                i = i + 1
            end
        end
    end
    return nil
end

CSkillUpgradeTopView.m_hookGetTargetUpgradeSkillIcon = function(this)
    if CClientMainPlayer.Inst==nil then return nil end
    local clsId = EnumToInt(CClientMainPlayer.Inst.Class)
    local skillCls = Guide_Class.GetData(clsId).LevelUpSkillId
    local item = CommonDefs.DictGetValue_LuaCall(this.itemCellDict,skillCls)
    if item then
        if not item.selectedSprite.enabled then
            return item.gameObject
        end
    end
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.UpgradeSkill) then
        CGuideMgr.Inst:TriggerGuide(6)
    end
    return nil
end
CSkillUpgradeTopView.m_hookGetTargetUpgradeSkillTab = function(this)
    if CClientMainPlayer.Inst==nil then return nil end
    local clsId = EnumToInt(CClientMainPlayer.Inst.Class)
    local skillCls = Guide_Class.GetData(clsId).LevelUpSkillId
    local idx = __Skill_AllSkills_Template.GetData(skillCls).SeriesName-1
    if idx==this.tabBar.SelectedIndex then
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.UpgradeSkill) then
            CGuideMgr.Inst:TriggerGuide(5)
            return nil
        end
    end

    local go = this.tabBar:GetTabGoByIndex(idx)
    return go
end