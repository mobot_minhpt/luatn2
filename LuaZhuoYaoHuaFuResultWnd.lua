local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local LayerDefine = import "L10.Engine.LayerDefine"

LuaZhuoYaoHuaFuResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "BottomButton", "BottomButton", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "SuccessView", "SuccessView", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "QualityLabel", "QualityLabel", UILabel)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "FailedView", "FailedView", GameObject)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "ItemCountLabel", "ItemCountLabel", UILabel)
RegistChildComponent(LuaZhuoYaoHuaFuResultWnd, "ItemNameLabel", "ItemNameLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhuoYaoHuaFuResultWnd, "m_Tweener")

function LuaZhuoYaoHuaFuResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BottomButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBottomButtonClick()
	end)

	self.SuccessView.transform:Find("chenggong/zhuoyaochenggong/zhuoyaochenggong (1)").gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
	self.SuccessView.transform:Find("chenggong/xinjilu/xinjilu (1)").gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
	self.SuccessView.transform:Find("chenggong/shoucihuode/shoucihuode (1)").gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)


	--@endregion EventBind end
end

function LuaZhuoYaoHuaFuResultWnd:OnEnable()
	local t = LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo
	if not t then
		return
	end
	self.SuccessView.gameObject:SetActive(t.bSuccess)
	self.FailedView.gameObject:SetActive(not t.bSuccess)
	if t.yaoguaiTempId then
		Gac2Gas.QueryYaoguaiCatchInfo_Ite(t.yaoguaiTempId)
	end
	if t.bSuccess then
		local tujianData = ZhuoYao_TuJian.GetData(t.yaoguaiTempId)
		self.Portrait:LoadNPCPortrait(tujianData.Icon)
		self.NameLabel.text = tujianData.Name
		
		self.m_Tweener = LuaTweenUtils.TweenFloat(0, 1, 2, function ( val )
            self.QualityLabel.text = SafeStringFormat3(LocalString.GetString("品质 %d"), math.floor(t.quality * val))
        end)
		LuaTweenUtils.SetTarget(self.m_Tweener,self.transform)
		self.SuccessView.transform:Find("chenggong/zhuoyaochenggong").gameObject:SetActive(t.bravo == 0)
		self.SuccessView.transform:Find("chenggong/xinjilu").gameObject:SetActive(t.bravo == 1)
		self.SuccessView.transform:Find("chenggong/shoucihuode").gameObject:SetActive(t.bravo == 2)
	else
		self.ItemCountLabel.text = t.returnItemCount
		local itemData = Item_Item.GetData(t.returnItemTemplateId)
		if itemData then
			self.ItemNameLabel.text = itemData.Name
			self.Icon:LoadMaterial(itemData.Icon)
		end
	end
	-- NGUITools.SetLayer(self.SuccessView.transform:Find("Panel").gameObject,LayerDefine.UI)
	-- NGUITools.SetLayer(self.FailedView.transform:Find("Panel").gameObject,LayerDefine.UI)
end

function LuaZhuoYaoHuaFuResultWnd:OnDisable()
	LuaTweenUtils.DOKill(self.transform, false)
end

--@region UIEvent

function LuaZhuoYaoHuaFuResultWnd:OnBottomButtonClick()
	CUIManager.CloseUI(CLuaUIResources.ZhuoYaoHuaFuWnd)
end

--@endregion UIEvent

