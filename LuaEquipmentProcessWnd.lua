local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local EventManager = import "EventManager"
local EPropStatus = import "L10.Game.EPropStatus"
local EnumEventType = import "EnumEventType"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemComposeWnd=import "L10.UI.CItemComposeWnd"
local CEquipBaptizeWnd=import "L10.UI.CEquipBaptizeWnd"
local CEquipIntensifyWnd=import "L10.UI.CEquipIntensifyWnd"
local QnTabView=import "L10.UI.QnTabView"
local QnTableView=import "L10.UI.QnTableView"
local UIScrollView=import "UIScrollView"
local CUITexture=import "L10.UI.CUITexture"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

CLuaEquipmentProcessWnd = class()
RegistClassMember(CLuaEquipmentProcessWnd,"closeButton")
RegistClassMember(CLuaEquipmentProcessWnd,"qnTabView")
RegistClassMember(CLuaEquipmentProcessWnd,"inlayStoneWnd")
RegistClassMember(CLuaEquipmentProcessWnd,"composeWnd")
RegistClassMember(CLuaEquipmentProcessWnd,"baptizeWnd")
RegistClassMember(CLuaEquipmentProcessWnd,"Title")
RegistClassMember(CLuaEquipmentProcessWnd,"masterView")
RegistClassMember(CLuaEquipmentProcessWnd,"curTabIndex")
RegistClassMember(CLuaEquipmentProcessWnd,"intensifyWnd")
RegistClassMember(CLuaEquipmentProcessWnd,"listRegion")
RegistClassMember(CLuaEquipmentProcessWnd,"guideRoutine")
RegistClassMember(CLuaEquipmentProcessWnd,"intensifyTab")

RegistClassMember(CLuaEquipmentProcessWnd,"scrollView")
RegistClassMember(CLuaEquipmentProcessWnd,"table")

function CLuaEquipmentProcessWnd:Awake()
    self.closeButton = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:onCloseWndClick(go)
    end)
    self.qnTabView = self.transform:Find("Anchor/offset/QnTabView"):GetComponent(typeof(QnTabView))
    -- self.inlayStoneWnd = self.transform:Find("Anchor/offset/QnTabView/TabWindow/InlayStoneWnd"):GetComponent(typeof(CEquipInlayStoneWnd))
    self.composeWnd = self.transform:Find("Anchor/offset/QnTabView/TabWindow/ComposeWnd"):GetComponent(typeof(CItemComposeWnd))
    self.baptizeWnd = self.transform:Find("Anchor/offset/QnTabView/TabWindow/EquipBaptizeWnd"):GetComponent(typeof(CEquipBaptizeWnd))
    self.Title = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.masterView = self.transform:Find("Anchor/offset/QnTabView/TabWindow/MasterView")
    self.table = self.masterView:GetComponent(typeof(QnTableView))
    self.scrollView = self.masterView:Find("ScrollView"):GetComponent(typeof(UIScrollView))


    self.curTabIndex = 0
    self.intensifyWnd = self.transform:Find("Anchor/offset/QnTabView/TabWindow/EquipIntensifyWnd"):GetComponent(typeof(CEquipIntensifyWnd))
    self.listRegion = self.transform:Find("Anchor/offset/QnTabView/TabWindow/MasterView/ListRegion").gameObject
    local tf = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs")
    self.intensifyTab = tf:GetChild(1).gameObject
end

function CLuaEquipmentProcessWnd:Init( )
    self.masterView.gameObject:SetActive(true)
    self.qnTabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function(tabButton,index)
        self:OnTabSelected(tabButton,index)
    end)
    CStatusMgr.Inst:SetStatusWithSync(CClientMainPlayer.Inst, EPropStatus.GetId("EquipForge"), 1)
    self.qnTabView:ChangeTo(CEquipmentProcessMgr.Inst.TabIndex)

    CUIManager.CloseUI(CLuaUIResources.RecreateExtraEquipWnd)
end

function CLuaEquipmentProcessWnd:Start()
    self.table.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return CEquipmentProcessMgr.Inst:GetCount()
        end,
        function(item,row) 
            local info = CEquipmentProcessMgr.Inst:GetItem(row)
            -- item:Init(info)
            self:InitItem(item.transform,info)
        end)

    self.table.OnSelectAtRow = DelegateFactory.Action_int(function (row) 
        local item = CEquipmentProcessMgr.Inst:GetItem(row)
        if item ~= nil then
            CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize = item
            CEquipmentProcessMgr.Inst.SelectEquipment = item
            EventManager.BroadcastInternalForLua(EnumEventType.SelectEquipment, {item.itemId})
        else
            CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize = nil
            CEquipmentProcessMgr.Inst.SelectEquipment = nil
        end
    end)
    self.table:ReloadData(true, false)
    self.table:SetSelectRow(0, true)
end

function CLuaEquipmentProcessWnd:OnEquipChange()
    EventManager.BroadcastInternalForLua(EnumEventType.SwitchEquipmentTabIndex, {0, CEquipmentProcessMgr.Inst.SubTabIndex})
    self.baptizeWnd:Init(CEquipmentProcessMgr.Inst.SubTabIndex)
end

function CLuaEquipmentProcessWnd:onCloseWndClick( go) 
    CUIManager.CloseUI(CUIResources.EquipmentProcessWnd)
    if CGuideMgr.Inst:IsInPhase(38) and CGuideMgr.Inst:IsInSubPhase(1) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
end
function CLuaEquipmentProcessWnd:OnTabSelected( button, selectedIndex) 
    if selectedIndex == 0 then
        self.Title.text = LocalString.GetString("装备洗炼")
        EventManager.BroadcastInternalForLua(EnumEventType.SwitchEquipmentTabIndex, {0, CEquipmentProcessMgr.Inst.SubTabIndex})
        self.baptizeWnd:Init(CEquipmentProcessMgr.Inst.SubTabIndex)
    elseif selectedIndex == 1 then
        self.Title.text = LocalString.GetString("装备强化")
        EventManager.BroadcastInternalForLua(EnumEventType.SwitchEquipmentTabIndex, {1, 0})
        self.intensifyWnd:Init()
    elseif selectedIndex == 2 then
        self.Title.text = LocalString.GetString("宝石镶嵌")
        EventManager.BroadcastInternalForLua(EnumEventType.SwitchEquipmentTabIndex, {2, 0})
    elseif selectedIndex == 3 then
        self.Title.text = LocalString.GetString("合成")
        EventManager.BroadcastInternalForLua(EnumEventType.SwitchEquipmentTabIndex, {3, 0})
        self.composeWnd:Init()
    end
    EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {CUIResources.EquipmentProcessWnd.Name})
end

function CLuaEquipmentProcessWnd:GetGuideGo( methodName) 
    if methodName == "GetIntensifyButton" then
        return self.intensifyWnd.intensifyDetailView:GetIntensifyButton()
    elseif methodName == "GetIntensifyTab" then
        return self.intensifyTab
    elseif methodName == "GetMiwenshou" then
        local count = CEquipmentProcessMgr.Inst:GetCount()
        for i=1,count do
            local item = self.table:GetItemAtRow(i-1)
            local info = CEquipmentProcessMgr.Inst:GetItem(i-1)
            if info and CLuaGuideMgr.EquipIntensifyIds[info.templateId] then
                --重置位置
                CUICommonDef.SetFullyVisible(item.gameObject, self.scrollView)
                return item.gameObject
            end
        end
        if count > 0 then
            CGuideMgr.Inst:EndCurrentPhase()
        end
        return nil
    elseif methodName == "GetListRegion" then
        g_ScriptEvent:RemoveListener("SelectEquipment", self, "OnSelectEquipment")
        g_ScriptEvent:AddListener("SelectEquipment", self, "OnSelectEquipment")
        return self.listRegion
    elseif methodName == "Get2ndTab" then
        return self.qnTabView:GetTabGameObject(1)
    elseif methodName == "GetForgeResetBtn" then
        local btn = self.baptizeWnd.transform:Find("EquipBaptizeDetailView/EquipForgePane/ResetBtn").gameObject
        return btn
    else
        return nil
    end
end
function CLuaEquipmentProcessWnd:GetTabIndex()
    return self.qnTabView.CurrentSelectTab
end

function CLuaEquipmentProcessWnd:OnSelectEquipment(args)
    --引导阶段
    if CGuideMgr.Inst:IsInPhase(38) and CGuideMgr.Inst:IsInSubPhase(1) then
        CGuideMgr.Inst:TriggerGuide(6)
    end
end

function CLuaEquipmentProcessWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("SelectEquipment", self, "OnSelectEquipment")

    CEquipmentProcessMgr.Inst.TabIndex = 0
    CEquipmentProcessMgr.Inst.SubTabIndex = 0
    CStatusMgr.Inst:SetStatusWithSync(CClientMainPlayer.Inst, EPropStatus.GetId("EquipForge"), 0)

    CEquipmentProcessMgr.Inst.SelectEquipment = nil

    CLuaEquipMgr:ClearExtraEquipInfo()
    -- //关掉相关的界面
    CUIManager.CloseUI(CUIResources.EquipAutoIntensifyWnd)
    CUIManager.CloseUI(CUIResources.EquipChouBingWnd)
    CUIManager.CloseUI(CUIResources.EquipHandIntensifyWnd)
    CUIManager.CloseUI(CUIResources.EquipWordAutoBaptizeConditionWnd)
    CUIManager.CloseUI(CUIResources.EquipWordAutoBaptizeWnd)
    CUIManager.CloseUI(CUIResources.EquipWordBaptizeWnd)
    CUIManager.CloseUI(CUIResources.PickerWnd)

    CUIManager.CloseUI(CUIResources.CheckGemGroupWnd)
    CUIManager.CloseUI(CUIResources.GemUpgradeWnd)
    CUIManager.CloseUI(CUIResources.InlayGemListWnd)
    CUIManager.CloseUI(CUIResources.RemoveInlayGemWnd)
    CUIManager.CloseUI(CUIResources.GemGroupInfoWnd)
    CUIManager.CloseUI(CUIResources.MakeEquipHoleWnd)
    CUIManager.CloseUI(CUIResources.InlayGemListWnd)
    CUIManager.CloseUI(CUIResources.ItemComposeSelectWnd)

    CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
    CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
end

function CLuaEquipmentProcessWnd:OnEnable()
    g_ScriptEvent:AddListener("SwitchEquipmentTabIndex", self, "OnSwitchEquipmentTabIndex")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("CompositeExtraEquipWordResult", self, "OnEquipChange")
    
end
function CLuaEquipmentProcessWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SwitchEquipmentTabIndex", self, "OnSwitchEquipmentTabIndex")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("CompositeExtraEquipWordResult", self, "OnEquipChange")
end

function CLuaEquipmentProcessWnd:OnSwitchEquipmentTabIndex(args )
    local index, subIndex=args[0],args[1]

    local originSelect = CEquipmentProcessMgr.Inst.SelectEquipment and CEquipmentProcessMgr.Inst.SelectEquipment.itemId or ""
    CEquipmentProcessMgr.Inst:InitDataSource(index, subIndex)

    if index == 3 then
        self.scrollView.gameObject:SetActive(false)
    else
        if index == 1 and subIndex == 1 then
            self.scrollView.gameObject:SetActive(false)
            --强化转移
        else
            if not self.scrollView.gameObject.activeInHierarchy then
                self.scrollView.gameObject:SetActive(true)
            end

            local selectIndex = self.table.m_CurrentSelectRow
            
            -- 处理洗练切换tab不刷新
            if originSelect ~= "" then
                local count = CEquipmentProcessMgr.Inst:GetCount()

                for i=1,count do
                    local info = CEquipmentProcessMgr.Inst:GetItem(i-1)
                    if info.itemId == originSelect then
                        selectIndex = i-1
                    end
                end
            end
            self.table:ReloadData(true, false)
            self.table:SetSelectRow(selectIndex, true)     
        end
    end

    self.m_Index = index
    self.m_SubIndex = subIndex
end

function CLuaEquipmentProcessWnd:InitItem(transform,equip)
    local nameLabel = transform:Find("EquipNameLabel"):GetComponent(typeof(UILabel))
    local qualitySprite = transform:Find("BorderSprite"):GetComponent(typeof(UISprite))
    local iconTexture=transform:Find("BorderSprite/EquipIcon"):GetComponent(typeof(CUITexture))
    local levelLabel = transform:Find("BorderSprite/Level"):GetComponent(typeof(UILabel))
    local bindSprite = transform:Find("BorderSprite/BindSprite"):GetComponent(typeof(UISprite))
    local stoneTable = transform:Find("StoneTable"):GetComponent(typeof(UITable))
    local stoneTemplate = transform:Find("InlayStoneTemplate").gameObject

    local iconGo = transform:Find("BorderSprite").gameObject


    local itemId = equip.itemId

    --m_EquipmentHoleList.Clear();
    nameLabel.text = ""
    iconTexture.material = nil
    levelLabel.text = ""
    bindSprite.spriteName = nil
    Extensions.RemoveAllChildren(stoneTable.transform)
    stoneTemplate:SetActive(false)
    local item = CItemMgr.Inst:GetById(equip.itemId)
    -- commonItem = item
    if item == nil then
        return
    end


    UIEventListener.Get(iconGo).onClick = DelegateFactory.VoidDelegate(function (p) 
        local getItem = CItemMgr.Inst:GetById(itemId)
        CItemInfoMgr.ShowLinkItemInfo(getItem, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
        -- On_Click(nil)
    end)

    nameLabel.text = LocalString.TranslateAndFormatText(item.Equip.DisplayName)
    nameLabel.color = item.Equip.DisplayColor
    -- QualityColor.GetRGBValue(item.Equip.QualityType, item.Equip.WordsCount);
    local icon = item.Icon
    iconTexture:LoadMaterial(icon)
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
    levelLabel.text = tostring(item.Equip.NeedLevel)
    bindSprite.spriteName = item.BindOrEquipCornerMark

    self:InitItemStoneList(transform,itemId)
end

function CLuaEquipmentProcessWnd:InitItemStoneList(transform,itemId)
    local stoneTable = transform:Find("StoneTable"):GetComponent(typeof(UITable))
    local stoneTemplate = transform:Find("InlayStoneTemplate").gameObject

    Extensions.RemoveAllChildren(stoneTable.transform)

    local item = CItemMgr.Inst:GetById(itemId)
	local templateId = item.Equip.TemplateId
    local maxHole = CItemMgr.Inst:GetEquipmentMaxHole(templateId)
    local Hole = item.Equip.Hole
    local holeItems = {}
    if item.Equip.ActiveHoleSetIdx==2 then
        for i=1,Hole do
            local id = item.Equip.SecondaryHoleItems[i]
            if id>0 and id<100 then
                table.insert( holeItems, item.Equip.HoleItems[id] )
            else
                table.insert( holeItems, id )
            end
        end
    else
        for i=1,Hole do
            table.insert( holeItems, item.Equip.HoleItems[i] )
        end
    end
    local is2ndSet = item.Equip.ActiveHoleSetIdx==2
    for i=1,Hole do
        local stone = CItemMgr.Inst:GetItemTemplate(holeItems[i])
        local stoneGo = NGUITools.AddChild(stoneTable.gameObject, stoneTemplate)
        stoneGo:SetActive(true)
        local locktrans = FindChild(stoneGo.transform,"lock")
        if item.Equip.SubHand > 0 then
            if locktrans then
                locktrans.gameObject:SetActive(true)
            end
            stoneGo:GetComponent(typeof(CUITexture)):Clear()
        else
            if locktrans then
                locktrans.gameObject:SetActive(false)
            end
            if stone ~= nil then
                stoneGo:GetComponent(typeof(CUITexture)):LoadMaterial(stone.Icon)
            else
                stoneGo:GetComponent(typeof(CUITexture)):Clear()
            end
        end
    end
    
    stoneTable:Reposition()
end

function CLuaEquipmentProcessWnd:RefreshItem(transform,itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        local nameLabel = transform:Find("EquipNameLabel"):GetComponent(typeof(UILabel))
        local qualitySprite = transform:Find("BorderSprite"):GetComponent(typeof(UISprite))
        local levelLabel = transform:Find("BorderSprite/Level"):GetComponent(typeof(UILabel))
        local bindSprite = transform:Find("BorderSprite/BindSprite"):GetComponent(typeof(UISprite))

        nameLabel.text = item.Equip.DisplayName
        nameLabel.color = item.Equip.DisplayColor

        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
        levelLabel.text = tostring(item.Equip.NeedLevel)
        bindSprite.spriteName = item.BindOrEquipCornerMark
        --宝石刷新
        self:InitItemStoneList(transform,itemId)
    end
end

function CLuaEquipmentProcessWnd:OnSendItem(args)
    local itemId = args[0]
    local count = CEquipmentProcessMgr.Inst:GetCount()
    for i=1,count do
        local info = CEquipmentProcessMgr.Inst:GetItem(i-1)
        if info.itemId == itemId then
            local item = self.table:GetItemAtRow(i-1)
            if item then
                self:RefreshItem(item.transform,itemId)
                break
            end
        end
    end
end
