-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemConsumeWndMgr = import "L10.UI.CItemConsumeWndMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouUnlockWnd = import "L10.UI.CLingShouUnlockWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLingShouUnlockWnd.m_Init_CS2LuaHook = function (this) 
    --uint templateId = CLingShouMgr.Inst.SheJiWuItem;
    --uint templateId = CLingShouMgr.Inst.SheJiWuItem;
    local templateId = CItemConsumeWndMgr.itemTemplateId
    this.buttonLabel.text = CItemConsumeWndMgr.buttonLabel
    this.countUpdate.templateId = templateId
    this.descLabel.templateId = templateId
    this.countUpdate.format = "{0}/" .. tostring(CItemConsumeWndMgr.consumeAmount)
    this.descLabel.format = ((CItemConsumeWndMgr.wndTitle .. "({0}/") .. tostring(CItemConsumeWndMgr.consumeAmount)) .. ")"
    this.countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        if val < CItemConsumeWndMgr.consumeAmount then
            this.countUpdate.format = "[ff0000]{0}[-]/" .. tostring(CItemConsumeWndMgr.consumeAmount)
            this.descLabel.format = ((CItemConsumeWndMgr.wndTitle .. "([ff0000]{0}[-]/") .. tostring(CItemConsumeWndMgr.consumeAmount)) .. ")"
        else
            this.countUpdate.format = "{0}/" .. tostring(CItemConsumeWndMgr.consumeAmount)
            this.descLabel.format = ((CItemConsumeWndMgr.wndTitle .. "({0}/") .. tostring(CItemConsumeWndMgr.consumeAmount)) .. ")"
        end

        this.getItemIcon:SetActive(val < CItemConsumeWndMgr.consumeAmount)
    end)
    this.countUpdate:UpdateCount()
    this.descLabel:UpdateCount()

    local data = CItemMgr.Inst:GetItemTemplate(templateId)
    if data ~= nil then
        --descLabel.text = "解锁需要消耗"+data.Name;
        this.icon:LoadMaterial(data.Icon)
        UIEventListener.Get(this.icon.gameObject).onClick = MakeDelegateFromCSFunction(this.OnIconClick, VoidDelegate, this)
    else
        this.icon:Clear()
    end

    local amount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, templateId)
    this.getItemIcon:SetActive(amount < CItemConsumeWndMgr.consumeAmount)
end
CLingShouUnlockWnd.m_OnIconClick_CS2LuaHook = function (this, go) 
    local amount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, CItemConsumeWndMgr.itemTemplateId)
    if CItemConsumeWndMgr.consumeAmount > amount then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(CItemConsumeWndMgr.itemTemplateId, false, go.transform, AlignType.Right)
        return
    end

    local item = CItemMgr.Inst:GetItemTemplate(CItemConsumeWndMgr.itemTemplateId)
    if item ~= nil then
        CItemInfoMgr.ShowLinkItemTemplateInfo(CItemConsumeWndMgr.itemTemplateId, false, nil, AlignType1.Default, 0, 0, 0, 0)
    end
end
CLingShouUnlockWnd.m_Unlock_CS2LuaHook = function (this, go) 
    if this.countUpdate.count < CItemConsumeWndMgr.consumeAmount and CItemConsumeWndMgr.itemNotEnoughAction ~= nil then
        --CUIManager.ShowUI(CUIResources.LingShouPurchaseWnd);
        invoke(CItemConsumeWndMgr.itemNotEnoughAction)
    elseif this.countUpdate.count >= CItemConsumeWndMgr.consumeAmount and CItemConsumeWndMgr.itemEnoughAction ~= nil then
        --Gac2Gas.AddNewLingShouMaxNumber();
        invoke(CItemConsumeWndMgr.itemEnoughAction)
    end
    this:Close()
end

CItemConsumeWndMgr.m_ShowItemConsumeWnd_CS2LuaHook = function (templateId, amount, title, label, enough, notEnough, purchase) 
    CItemConsumeWndMgr.itemTemplateId = templateId
    CItemConsumeWndMgr.consumeAmount = amount
    CItemConsumeWndMgr.wndTitle = title
    CItemConsumeWndMgr.buttonLabel = label
    CItemConsumeWndMgr.itemEnoughAction = enough
    CItemConsumeWndMgr.itemNotEnoughAction = notEnough
    CItemConsumeWndMgr.purchaseAction = purchase
    CUIManager.ShowUI(CUIResources.LingShouUnlockWnd)
end
CItemConsumeWndMgr.m_ClearData_CS2LuaHook = function () 
    CItemConsumeWndMgr.itemTemplateId = 0
    CItemConsumeWndMgr.consumeAmount = 0
    CItemConsumeWndMgr.wndTitle = ""
    CItemConsumeWndMgr.buttonLabel = ""
    CItemConsumeWndMgr.itemEnoughAction = nil
    CItemConsumeWndMgr.itemNotEnoughAction = nil
    CItemConsumeWndMgr.purchaseAction = nil
end
