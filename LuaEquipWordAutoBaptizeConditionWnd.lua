local BoxCollider = import "UnityEngine.BoxCollider"
local Vector3 = import "UnityEngine.Vector3"
local CItemMgr = import "L10.Game.CItemMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CAutoBaptizeConditionItem = import "L10.UI.CAutoBaptizeConditionItem"


CLuaEquipWordAutoBaptizeConditionWnd = class()
RegistClassMember(CLuaEquipWordAutoBaptizeConditionWnd,"autoBaptizeConditionItem")
RegistClassMember(CLuaEquipWordAutoBaptizeConditionWnd,"conditionTf")
RegistClassMember(CLuaEquipWordAutoBaptizeConditionWnd,"confirmBtn")

function CLuaEquipWordAutoBaptizeConditionWnd:Init()
    -- local superConfirm = self.transform:Find("Anchor/BackGround/SuperConfirm").gameObject
    -- local vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level
    -- local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
    -- local equipType = EquipmentTemplate_Equip.GetData(equipData.TemplateId).Type
    -- if --vipLevel >= GameSetting_Common.GetData().AutoXiLianWordVIP and
    --  (equipData.IsGhostEquipment or equipData.IsPurpleEquipment) then
    --     if equipType ~= EnumBodyPosition_lua.Necklace then
    --         superConfirm:SetActive(true)
    --         self.confirmBtn:SetActive(false)
    --     end
    -- end
end
function CLuaEquipWordAutoBaptizeConditionWnd:Awake( )
    self.autoBaptizeConditionItem = self.transform:Find("AutoBaptizeConditionItem").gameObject
    self.conditionTf = self.transform:Find("Anchor/BackGround/ConditionTf")
    self.confirmBtn = self.transform:Find("Anchor/BackGround/ConfirmBtn").gameObject

    self.autoBaptizeConditionItem:SetActive(false)
    self:InitList()

    UIEventListener.Get(self.confirmBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        -- self:Close()
        CUIManager.CloseUI(CUIResources.EquipWordAutoBaptizeConditionWnd)
    end)

    local superConfirmBtn = self.transform:Find("Anchor/BackGround/SuperConfirm/ConfirmBtn").gameObject
    local superBaptizeBtn = self.transform:Find("Anchor/BackGround/SuperConfirm/SuperBaptizeBtn").gameObject
    UIEventListener.Get(superConfirmBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        -- self:Close()
        CUIManager.CloseUI(CUIResources.EquipWordAutoBaptizeConditionWnd)
    end)
    UIEventListener.Get(superBaptizeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        local place = equip.place
        local pos = equip.pos
        local equipId = equip.itemId
        Gac2Gas.QueryAutoBaptizeTargetWordCondList(place, pos, equipId)
    end)
end
function CLuaEquipWordAutoBaptizeConditionWnd:InitList( )
    --数据初始化
    --CEquipmentBaptizeMgr.Inst.InitCondition();

    --数据初始化
    --CEquipmentBaptizeMgr.Inst.InitCondition();

    local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize

    local equipTemplate = CEquipmentProcessMgr.Inst.BaptizingEquipmentTemplate
    if equipTemplate ~= nil then
        local countOption = EquipBaptize_WordCountOption.GetData(EnumToInt(equipData.Color))
        if countOption ~= nil then
            CommonDefs.EnumerableIterate(countOption.WordCount, DelegateFactory.Action_object(function (___value) 
                local item = ___value
                local go = CommonDefs.Object_Instantiate(self.autoBaptizeConditionItem)
                go.transform.parent = self.conditionTf
                go.transform.localScale = Vector3.one
                go:SetActive(true)
                local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAutoBaptizeConditionItem))
                cmp:InitCount(item)
            end))
        else
            -- CLogMgr.LogError(LocalString.GetString("找不到"))
        end

        local option = CEquipmentProcessMgr.Inst:GetWordOption(equipTemplate.Type, equipTemplate.SubType)
        if option ~= nil then
            CommonDefs.EnumerableIterate(option.Word, DelegateFactory.Action_object(function (___value) 
                local item = ___value
                local go = CommonDefs.Object_Instantiate(self.autoBaptizeConditionItem)
                go.transform.parent = self.conditionTf
                go.transform.localScale = Vector3.one
                go:SetActive(true)
                local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAutoBaptizeConditionItem))
                cmp:InitWord(item)
            end))
        else
            -- CLogMgr.LogError(LocalString.GetString("找不到"))
        end

        if not equipData.IsExtraEquipment then
            self:TryAdd137Ghost()
        end
        self:AddScoreConditionItem()
    end
    --adj size
    local height = 0
    local childLists = self.conditionTf:GetComponent(typeof(UITable)):GetChildList()
    local cellHeight = self.autoBaptizeConditionItem.transform:Find("Toggle"):GetComponent(typeof(BoxCollider)).size.y
    local padding = self.conditionTf:GetComponent(typeof(UITable)).padding.y
    height = (childLists.Count -1) * cellHeight + (childLists.Count + 1) * padding
  
    local preHeight = self.transform:Find("Anchor/BackGround"):GetComponent(typeof(UISprite)).height
    local preTfHeight = self.transform:Find("PlusBackGround/BgTexture"):GetComponent(typeof(UITexture)).height
    local tfHeight = preTfHeight
    height = height + preHeight

    -- if CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam6() ~= "" then
    --     local _,itemList = LuaAutoBaptizeNecessaryConditionMgr:GetSubmitConfirmInfo()
    --     local plusConditionTf = self.transform:Find("PlusBackGround/ConditionTf").gameObject
    --     local tfPadding = plusConditionTf.transform:GetComponent(typeof(UITable)).padding.y
    --     if next(itemList) then
    --         local titleGo = NGUITools.AddChild(plusConditionTf, self.transform:Find("NecessaryTitleLabel").gameObject)
    --         titleGo:SetActive(true)
    --         tfHeight = tfHeight + titleGo.transform:GetComponent(typeof(UILabel)).height + tfPadding*2
    --         for i,info in ipairs(itemList) do
    --             local item = NGUITools.AddChild(plusConditionTf, self.transform:Find("NecessaryConditionItem").gameObject)
    --             item:SetActive(true)
    --             item.transform:GetComponent(typeof(UILabel)).text = info
    --             tfHeight = tfHeight + item.transform:GetComponent(typeof(UILabel)).height + tfPadding*2
    --         end

    --         local plusBackGroung = self.transform:Find("PlusBackGround")

    --         self.transform:Find("PlusBackGround/BgTexture"):GetComponent(typeof(UITexture)).height = tfHeight
    --         local tf = NGUITools.AddChild(self.conditionTf.gameObject,plusBackGroung.gameObject)
    --         tf:SetActive(true)
    --     end
    --     height =  height + tfHeight      
    -- end   
    self.transform:Find("Anchor/BackGround"):GetComponent(typeof(UISprite)).height = height
end
function CLuaEquipWordAutoBaptizeConditionWnd:TryAdd137Ghost( )
    --137鬼装
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local commonItem = CItemMgr.Inst:GetById(equipInfo.itemId)
    if commonItem ~= nil and CEquipmentBaptizeMgr.Is137GhostEquip(commonItem.Equip) then
        local go = CommonDefs.Object_Instantiate(self.autoBaptizeConditionItem)
        go.transform.parent = self.conditionTf
        go.transform.localScale = Vector3.one
        go:SetActive(true)
        local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAutoBaptizeConditionItem))
        cmp:InitSpecialWord(CEquipmentBaptizeMgr.Word_QingHong_8)
    end
end
function CLuaEquipWordAutoBaptizeConditionWnd:AddScoreConditionItem( )
    --装备评分高于当前
    local go = NGUITools.AddChild(self.conditionTf.gameObject, self.autoBaptizeConditionItem)
    go:SetActive(true)
    local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAutoBaptizeConditionItem))
    cmp:InitCheckScore()
end

