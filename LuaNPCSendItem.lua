local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CChatHelper = import "L10.UI.CChatHelper"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
LuaNPCSendItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNPCSendItem, "ChannelNameLabel", "ChannelNameLabel", UILabel)
RegistChildComponent(LuaNPCSendItem, "SenderNameLabel", "SenderNameLabel", UILabel)
RegistChildComponent(LuaNPCSendItem, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaNPCSendItem, "GiftItem", "GiftItem", GameObject)
RegistChildComponent(LuaNPCSendItem, "RedPackage", "RedPackage", GameObject)
RegistChildComponent(LuaNPCSendItem, "TaskItem", "TaskItem", GameObject)
RegistChildComponent(LuaNPCSendItem, "EventItem", "EventItem", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaNPCSendItem, "m_Data")
RegistClassMember(LuaNPCSendItem, "m_EventItem")
RegistClassMember(LuaNPCSendItem, "m_EventId")
RegistClassMember(LuaNPCSendItem, "m_EventType")
function LuaNPCSendItem:Awake()
    self.GiftItem:SetActive(false)
    self.RedPackage:SetActive(false)
    self.TaskItem:SetActive(false)
    self.m_EventItem = nil
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end
---FinishNpcChatEventSet
function LuaNPCSendItem:Init(data)
    self.m_Data = data
    self.GiftItem:SetActive(false)
    self.RedPackage:SetActive(false)
    self.TaskItem:SetActive(false)
    self:InitSenderInfo()
    self.m_EventId = tonumber(data.text)
    local EventInfo = NpcChat_ChatEvent.GetData(self.m_EventId)
    if not EventInfo then return end
    local EventType = EventInfo.Type
    self.m_EventType = EventType
    local MainPlayer = CClientMainPlayer.Inst
    if not MainPlayer then return end
    local HasFinished = MainPlayer.RelationshipProp.FinishNpcChatEventSet:GetBit(NpcChat_ChatEvent.GetData(self.m_EventId).SN)
    local NPCID = data.senderId
    local args = nil
    if EventType == EnumNPCChatEventType.RedPackage then
        self.m_EventItem = self.RedPackage
        args = { AwardAmount = EventInfo.RewardSilver }
    elseif EventType == EnumNPCChatEventType.Gift then
        self.m_EventItem = self.GiftItem
        local itemInfo = g_LuaUtil:StrSplit(EventInfo.RewardItem,",")
        args = {ItemID = itemInfo[1],ItemNum = itemInfo[2], isBind = itemInfo[3]}
    elseif EventType == EnumNPCChatEventType.Task then
        self.m_EventItem = self.TaskItem
    end
    UIEventListener.Get(self.EventItem.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        if EventType == EnumNPCChatEventType.RedPackage or EventType == EnumNPCChatEventType.Gift then
            LuaNPCChatMgr:OpenNPCGiftWnd(EventType,args,NPCID,self.m_EventId)
        elseif EventType == EnumNPCChatEventType.Task then
            local HasFinished = MainPlayer.RelationshipProp.FinishNpcChatEventSet:GetBit(NpcChat_ChatEvent.GetData(self.m_EventId).SN)
            if not HasFinished then
                Gac2Gas.RequestFinishNpcChatEvent(self.m_Data.senderId,self.m_EventId)
            else
                g_MessageMgr:ShowMessage("NPCTask_HasAccept")
            end
        end
    end)
    self:InitEventItem(HasFinished)
    
end

function LuaNPCSendItem:InitSenderInfo()
    self.ChannelNameLabel.text = SafeStringFormat3("[%s]%s[-]",CChatHelper.GetChannelColor24(self.m_Data.channel), self.m_Data.channelName)
    self.SenderNameLabel.text = self.m_Data.senderName;
    self.Portrait:LoadNPCPortrait(self.m_Data.senderPortrait)
end

function LuaNPCSendItem:InitEventItem(hasFinished)
    if not self.m_EventItem then return end
    local Receive = self.m_EventItem.transform:Find("Receive")
    local UnReceive = self.m_EventItem.transform:Find("UnReceive")
    Receive.gameObject:SetActive(hasFinished)
    UnReceive.gameObject:SetActive(not hasFinished)
    self.m_EventItem:SetActive(true)
    self.EventItem.transform:Find("ChatBubble/Label"):GetComponent(typeof(UILabel)).text = hasFinished and LocalString.GetString("已领取") or LocalString.GetString("点击领取")

    local Fx = self.EventItem.transform:Find("fx"):GetComponent(typeof(CUIFx))
    Fx:DestroyFx()
    if not hasFinished then
        if self.m_EventType == EnumNPCChatEventType.RedPackage then
            Fx:LoadFx("fx/ui/prefab/UI_liaotian_hongbao.prefab")
            LuaUtils.SetLocalScale(Fx.transform,1,1,1)
        elseif self.m_EventType == EnumNPCChatEventType.Gift then
            Fx:LoadFx("fx/ui/prefab/UI_liaotian_liwu.prefab")
            LuaUtils.SetLocalScale(Fx.transform,-1,1,1)   -- 礼物的特效是反的，这里单独把礼物特效反一下
        elseif self.m_EventType == EnumNPCChatEventType.Task then
            Fx:LoadFx("fx/ui/prefab/UI_liaotian_renwushu.prefab")
            LuaUtils.SetLocalScale(Fx.transform,1,1,1)
        end
    end
end

function LuaNPCSendItem:OnSetNpcChatEventFinished(NPCID,eventID)
    if eventID == self.m_EventId and self.m_Data.senderId == NPCID then
        self:InitEventItem(true)
    end
end

function LuaNPCSendItem:OnEnable()
    g_ScriptEvent:AddListener("SetNpcChatEventFinished", self, "OnSetNpcChatEventFinished")
end

function LuaNPCSendItem:OnDisable()
    g_ScriptEvent:RemoveListener("SetNpcChatEventFinished", self, "OnSetNpcChatEventFinished")
end
--@region UIEvent

--@endregion UIEvent

