-- Auto Generated!!
local Buff_Buff = import "L10.Game.Buff_Buff"
local CDaDiZiBuffIcon = import "L10.UI.CDaDiZiBuffIcon"
local CommonDefs = import "L10.Game.CommonDefs"
local GameSetting_Client_Wapper = import "L10.Game.GameSetting_Client_Wapper"
local Int32 = import "System.Int32"
CDaDiZiBuffIcon.m_Init_CS2LuaHook = function (this, buffId) 
    local buff = Buff_Buff.GetData(buffId)
    if buff ~= nil then
        if buff.Kind > 0 and CommonDefs.DictContains(GameSetting_Client_Wapper.Inst.BuffGroup2IconPathDict, typeof(Int32), buff.Kind) then
            this.icon:LoadMaterial(CommonDefs.DictGetValue(GameSetting_Client_Wapper.Inst.BuffGroup2IconPathDict, typeof(Int32), buff.Kind))
        else
            this.icon:LoadMaterial(buff.Icon)
        end
    else
        this.icon:LoadMaterial(nil)
    end
end
