
LuaChunJie2024BaiNianWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024BaiNianWnd, "timeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaChunJie2024BaiNianWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaChunJie2024BaiNianWnd, "template", "Template", GameObject)
--@endregion RegistChildComponent end

function LuaChunJie2024BaiNianWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
    --@endregion EventBind end
end

function LuaChunJie2024BaiNianWnd:Init()

end

--@region UIEvent

function LuaChunJie2024BaiNianWnd:OnTipButtonClick()
end

--@endregion UIEvent
