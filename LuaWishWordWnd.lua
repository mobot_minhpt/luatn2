local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local CChatInput = import "L10.UI.CChatInput"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local EChatPanel = import "L10.Game.EChatPanel"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local Constants = import "L10.Game.Constants"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaWishWordWnd = class()
RegistChildComponent(LuaWishWordWnd,"CloseButton", GameObject)
RegistChildComponent(LuaWishWordWnd,"emotionBtn", GameObject)
RegistChildComponent(LuaWishWordWnd,"statusInput", CChatInput)
RegistChildComponent(LuaWishWordWnd,"addNode", GameObject)

RegistClassMember(LuaWishWordWnd, "m_DefaultChatInputListener")

function LuaWishWordWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.WishWordWnd)
end

function LuaWishWordWnd:OnEnable()
end

function LuaWishWordWnd:OnDisable()
end

function LuaWishWordWnd:OnEmotionButtonClick(go)
  CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, go, EChatPanel.Undefined, 0, 0)
end

function LuaWishWordWnd:OnSendZhufu(msg)
	msg = CUICommonDef.Trim(msg)
  if System.String.IsNullOrEmpty(msg) then
    --g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
    g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString('请先输入祝福语哦'))
    return
  end

	msg = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(msg, true)
  if System.String.IsNullOrEmpty(msg) then
		g_MessageMgr:ShowMessage("Speech_Violation")
    return
  end
	msg = CChatMgr.Inst:FilterYangYangEmotion(msg)

  LuaPersonalSpaceMgrReal.AddWishComment(LuaWishMgr.CurrentWishData.wishId,msg,function(data)
    self:AddWordBack(data)
  end)

end

function LuaWishWordWnd:AddWordBack(data)
 --
 g_ScriptEvent:BroadcastInLua("WishAddHelpUpdate",LuaWishMgr.CurrentWishData.wishId)
 self:Close()
end

function LuaWishWordWnd:InitAddNode()
  local data = LuaWishMgr.CurrentWishData
	self.addNode.transform:Find('name'):GetComponent(typeof(UILabel)).text = data.roleName
  local xianFanStatus = data.xianFanStatus or 0
  self.addNode.transform:Find('level'):GetComponent(typeof(UILabel)).text = xianFanStatus > 0 and SafeStringFormat3("[c][%s]lv.%d[-][/c]", Constants.ColorOfFeiSheng, data.grade) or SafeStringFormat3("lv.%d", data.grade)
	self.addNode.transform:Find('desc'):GetComponent(typeof(UILabel)).text = data.text
	self.addNode.transform:Find('pic'):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(data.clazz, data.gender, -1),false)
end

function LuaWishWordWnd:SendBtnClick()
	local msg = self.statusInput.transform:Find('StatusText'):GetComponent(typeof(UIInput)).value
	self:OnSendZhufu(msg)
end

function LuaWishWordWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  if not LuaWishMgr.CurrentWishData then
    self:Close()
    return
  end

  if not self.m_DefaultChatInputListener then
    self.m_DefaultChatInputListener = LuaPersonalSpaceMgrReal.GetChatListener(self.statusInput)
  end

  self:InitAddNode()

  UIEventListener.Get(self.emotionBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    self:OnEmotionButtonClick()
  end)

  self.statusInput.OnSend = DelegateFactory.Action_string(function (msg)
    self:OnSendZhufu(msg)
  end)

--  local sendBtn = self.statusInput.transform:Find('SendMsgButton').gameObject
--  UIEventListener.Get(sendBtn).onClick = DelegateFactory.VoidDelegate(function (p)
--    self:SendBtnClick()
--  end)
end

return LuaWishWordWnd
