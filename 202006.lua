--[[
	若功能中需要改动c#，并在Lua下访问此C#改动请在下方进行登记，详细注意事项参见
	https://confluence.leihuo.netease.com/pages/viewpage.action?pageId=47937723


	1.  CCPlayer.cs新增了iOS/Android/PC接口CCPlayerPluginOpenCCAppWithProtocol和CCPlayerPluginOpenCCAppWithRoomId，在LuaCCLiveWnd.lua中进行了引用
	2.  NGUIText的ParseEmotion方法增加了一个Hook节点m_hookParseEmotion，方便以后出问题进行patch
	3.  过场动画增加AMDayAndNightChangeBehavior.cs和CSDayAndNightChangeBehavior.cs两个类，在lua下没有引用。
		热更新上来的版本观看最新版本的过场动画，会丢失新做的效果，不会报错，剩余动画效果可以正常播放。
	4.  设置帮会帮主雕像污染度功能，SyncGuildStatuePolluteValueChange这个rpc会调用c#中新加的SetGuildDirtyValue函数。
	5.  EventManager新增OnApplicationPause试剑，被common_include引用，用于以后在Lua下处理应用程序进入后台的情况。
	6.  CCMiniAPIMgr.cs增加一个用于线上屏蔽特定消息的接口，线上运营活动临时会要求禁止在某些频道中显示CC公屏发的消息，可以利用这个接口做patch处理
	7.  PostEffectMgr.cs和CClientObject.cs里增加了用于支持场景的第二种后期处理功能，玩家走到特定区域会使用第二张lut图作为校色处理
	8.  增加了WatermarkHelper.cs 和 WatermarkPostEffect.cs 两个C#文件，用于处理盲水印功能，不和其他脚本产生引用关系，通过patch开启。
]]

--[[
	chenguangzhong 2020/05/29
	根据VN416239 416020 416019 416018 416015
	修改webpay的跳转地址
	非GP包，ip地址改为域名访问
	本次修改均包含在IS_VN_CLIENT宏下，不影响iOS热更
	M /trunk/L10/Development/QnMobile/Assets/Plugins/Launch/CLaunchPatchWnd.cs
	M /trunk/L10/Development/QnMobile/Assets/Plugins/Launch/PatchMgr.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Engine/Main/GameLauncher.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Game/Common/HTTPHelper.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Game/GameVideo/CGameVideoMgr.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Game/Login/LoginMgr.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Game/UniSdk/PayMgr.cs

	chenguangzhong 2020/06/16
	下载基础资源进度条设置上限修改，避免超过上限
	本次修改只是修改表现，ios不做更新不影响
	M /trunk/L10/Development/QnMobile/Assets/Plugins/Launch/PatchMgr.cs

	chenguangzhong 2020/06/17
	svn：418362 418372 新增性能数据上报功能，热更不会增加这个功能，但是不会报错
	Main.cs 如果发现 Utility.persistentDataPath目录下存在Preformance.Log，则会将这个文件上传
	NetworkMgr_MainThread.cs 增加获取RPC队列数量的接口
	NetworkMgr_NetworkThread.cs 增加获取RPC队列数量的接口
	CPerformanceMgr.cs 增加获取FPS的接口
	A /trunk/L10/Development/QnMobile/Assets/Editor/PerformanceLogReader.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Engine/Main/Main.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Engine/Network/NetworkMgr_MainThread.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Engine/Network/NetworkMgr_NetworkThread.cs
	M /trunk/L10/Development/QnMobile/Assets/Scripts/Engine/Performance/CPerformanceMgr.cs
]]

--修复观战时报错
local GameVideoLuaRecord = import "L10.Game.GameVideoLuaRecord"
local CGameVideoCharacterMgr = import "L10.Game.CGameVideoCharacterMgr"
local Lua = import "L10.Engine.Lua"
GameVideoLuaRecord.m_hookShowSkillActionState = function(this, data)
	if CGameVideoCharacterMgr.Inst:GetFakeEngineId(data[1]) > 0 then
		Lua.s_IsHook = false
	end
end

-- 修复阿乌使用竹蜻蜓坐骑时，特效看不见的问题
local CClientObject = import "L10.Game.CClientObject"
CClientObject.m_hookGetInVehicleAsDriver = function (driverRO, vehicleId, ...)
	Lua.s_IsHook = false
	if (vehicleId == 46000216 or vehicleId == 46000217 or vehicleId == 46000218) and driverRO.MainPath then 
		local fxPrefab = string.match(driverRO.MainPath, "xnpc_yingling") and "fx/npc/prefab/znpc062_fx01_zuoqi_xnpc.prefab" or "fx/npc/prefab/znpc062_fx01_zuoqi.prefab"
		Fx_FX.PatchField(88801542, "Prefab", fxPrefab)
	end
end