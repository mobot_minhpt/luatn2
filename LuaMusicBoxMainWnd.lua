local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType3 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

CLuaMusicBoxMainWnd = class()
CLuaMusicBoxMainWnd.Path = "ui/musicbox/LuaMusicBoxMainWnd"

RegistClassMember(CLuaMusicBoxMainWnd, "m_ConfigRootObj")
RegistClassMember(CLuaMusicBoxMainWnd, "m_MusicBoxRO")
RegistClassMember(CLuaMusicBoxMainWnd, "m_ModelCameraName")

RegistClassMember(CLuaMusicBoxMainWnd, "m_NoPlayRootObj")
RegistClassMember(CLuaMusicBoxMainWnd, "m_PlayingRootObj")
RegistClassMember(CLuaMusicBoxMainWnd, "m_PlayingNameLabel")
RegistClassMember(CLuaMusicBoxMainWnd, "m_PlayingLeftTimeLabel")

--Config
RegistClassMember(CLuaMusicBoxMainWnd, "m_PlayStyleCheckBoxTable")
RegistClassMember(CLuaMusicBoxMainWnd, "m_PlayIntervalCheckBoxTable")
RegistClassMember(CLuaMusicBoxMainWnd, "m_PlayOtherCheckBox")

RegistClassMember(CLuaMusicBoxMainWnd, "m_HaveMusic")

RegistClassMember(CLuaMusicBoxMainWnd, "m_PlayBtnSprite")

RegistClassMember(CLuaMusicBoxMainWnd, "m_IsPlaying")
RegistClassMember(CLuaMusicBoxMainWnd, "m_ModelTexture")

function CLuaMusicBoxMainWnd:Init()
  self.m_ModelCameraName = "__MusicBox__"
  UIEventListener.Get(self.transform:Find("Anchor/TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
    g_MessageMgr:ShowMessage("MusicBox_Use_Tip")
  end)
  self.m_ConfigRootObj = self.transform:Find("Anchor/ConfigRoot").gameObject
  self.m_ConfigRootObj:SetActive(false)
  UIEventListener.Get(self.transform:Find("Anchor/ConfigBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
    self.m_ConfigRootObj:SetActive(true)
  end)
  self.m_NoPlayRootObj = self.transform:Find("Anchor/Top").gameObject
  self.m_PlayingRootObj = self.transform:Find("Anchor/PlayingTitle").gameObject
  self.m_PlayingNameLabel = self.m_PlayingRootObj.transform:Find("Name"):GetComponent(typeof(UILabel))
  self.m_PlayingLeftTimeLabel = self.m_PlayingRootObj.transform:Find("Time"):GetComponent(typeof(UILabel))
  UIEventListener.Get(self.m_NoPlayRootObj.transform:Find("Sprite").gameObject).onClick = LuaUtils.VoidDelegate(function()
    self:AddMusic()
  end)

  self.m_PlayBtnSprite = self.transform:Find("Anchor/PlayBtn/Sprite"):GetComponent(typeof(UISprite))
  UIEventListener.Get(self.transform:Find("Anchor/PlayBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
    self:OnPlayBtnClick()
  end)
  UIEventListener.Get(self.transform:Find("Anchor/Texture").gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    self:DeleteMusic(go)
  end)

  self:InitBg()
  self:InitConfig()
  self:InitMusic()
end

function CLuaMusicBoxMainWnd:InitMusic()
  self.m_HaveMusic = CLuaMusicBoxMgr.MusicIdOrUrl and CLuaMusicBoxMgr.MusicIdOrUrl ~= "" and CLuaMusicBoxMgr.ExpireTime > CServerTimeMgr.Inst.timeStamp
  self.m_IsPlaying = (CLuaMusicBoxMgr.IsOn or false) and self.m_HaveMusic
  self:InitPlayButtonStatus(not self.m_IsPlaying)
  self:Rotate(self.m_IsPlaying)

  self.m_NoPlayRootObj:SetActive(not self.m_HaveMusic)
  self.m_PlayingRootObj:SetActive(self.m_HaveMusic)
  self.m_ModelTexture.gameObject:SetActive(self.m_HaveMusic)
  if self.m_HaveMusic then
    self.m_PlayingNameLabel.text = CLuaMusicBoxMgr.Name
  end

  if self.m_HaveMusic then
    self.m_PlayingLeftTimeLabel.text = LocalString.GetString("剩余 ")..CLuaMusicBoxMgr:GetMusicLeftTimeStr(CLuaMusicBoxMgr.ExpireTime)
  end
end

function CLuaMusicBoxMainWnd:OnPlayBtnClick()
  if not self.m_HaveMusic then g_MessageMgr:ShowMessage("MusicBox_Need_XHZS") return end
  self.m_IsPlaying = not self.m_IsPlaying

  --force turn on MusicEnabled if it is BG music or voice
  if self.m_IsPlaying then
    PlayerSettings.MusicEnabled = true
  end

  self:InitPlayButtonStatus(not self.m_IsPlaying)
  Gac2Gas.DiskPlayerPlay(CLuaMusicBoxMgr.FurnitureId, self.m_IsPlaying)
  self:Rotate(self.m_IsPlaying)
end

function CLuaMusicBoxMainWnd:DeleteMusic(go)
  if not self.m_HaveMusic then return end
  local popupMenuItemTable = {}
  table.insert(popupMenuItemTable, PopupMenuItemData(LocalString.GetString("取出"), DelegateFactory.Action_int(function (index)
    Gac2Gas.DiskPlayerRemoveRecordItem(CLuaMusicBoxMgr.FurnitureId)
  end), false, nil, EnumPopupMenuItemStyle.Default))
  local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
  CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType3.Right, 1, nil, 600, true, 296)
end

function CLuaMusicBoxMainWnd:InitPlayButtonStatus(bPlay)
  self.m_PlayBtnSprite.spriteName = bPlay and "socialwnd_play" or "socialwnd_pause"
end

function CLuaMusicBoxMainWnd:AddMusic()
  if not CClientMainPlayer.Inst then return end
  local dict = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, cs_string)))
  local list = CreateFromClass(MakeGenericClass(List, cs_string))

  local recordItemTable = {}
  local size = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
  if size > 0 then
    for i = 1, size do
      local item = CItemMgr.Inst:GetById(CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i))
      if item and item.TemplateId == 21030608 then
        CommonDefs.ListAdd(list, typeof(cs_string), item.Id)
      end
      if item and item.TemplateId == StarBiWuShow_MusicBoxSetting.GetData().DiskItemTemplateIdBeforeRecord then
        table.insert(recordItemTable, {itemId = item.Id, itemPos = i})
      end
    end
  end
  CommonDefs.DictAdd(dict, TypeOfInt32, 0, typeof(MakeGenericClass(List, cs_string)), list)

  if list.Count <= 0 then
    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("MusicBox", dict, LocalString.GetString("请选择星痕之砂"), 0, false, LocalString.GetString("选择"), LocalString.GetString("前往制作"), DelegateFactory.Action(function ( ... )
        if #recordItemTable <= 0 then
          CYuanbaoMarketMgr.ShowPlayerShop(21030607)
        else
          CLuaMusicBoxMgr:OpenRecordWnd(recordItemTable[1].itemId, EnumItemPlace.Bag, recordItemTable[1].itemPos)
        end
      end), LocalString.GetString("暂无可播放的星痕之砂"))
  else
    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("MusicBox", dict, LocalString.GetString("请选择星痕之砂"), 0, false, LocalString.GetString("选择"), LocalString.GetString("前往制作"), nil, LocalString.GetString("暂无可播放的星痕之砂"))
  end
end

function CLuaMusicBoxMainWnd:InitConfig()
  local rootTrans = self.transform:Find("Anchor/ConfigRoot/Bg")
  -- Style Config
  self.m_PlayStyleCheckBoxTable = {}
  for i = 1, 2 do
    local checkbox = rootTrans:Find("Style/QnCheckbox"..i):GetComponent(typeof(QnCheckBox))
    table.insert(self.m_PlayStyleCheckBoxTable, checkbox)
    checkbox:SetSelected(CLuaMusicBoxMgr.PlayType + 1 == i, true)
    checkbox.OnValueChanged = DelegateFactory.Action_bool(function(val)
      if val then
        for k, v in ipairs(self.m_PlayStyleCheckBoxTable) do
          if v == checkbox then
            CLuaMusicBoxMgr.PlayType = k - 1
          else
            v:SetSelected(false, true)
          end
        end
      else
        checkbox:SetSelected(true, true)
      end
    end)
  end

  -- Interval Config
  self.m_PlayIntervalCheckBoxTable = {}
  for i = 1, 2 do
    local checkbox = rootTrans:Find("Interval/QnCheckbox"..i):GetComponent(typeof(QnCheckBox))
    table.insert(self.m_PlayIntervalCheckBoxTable, checkbox)
    checkbox:SetSelected(CLuaMusicBoxMgr.PlayLoop + 1 == i, true)
    checkbox.OnValueChanged = DelegateFactory.Action_bool(function(val)
      if val then
        for k, v in ipairs(self.m_PlayIntervalCheckBoxTable) do
          if v == checkbox then
            CLuaMusicBoxMgr.PlayLoop = k - 1
          else
            v:SetSelected(false, true)
          end
        end
      else
        checkbox:SetSelected(true, true)
      end
    end)
  end
--[[
  -- Other Config
  self.m_PlayOtherCheckBox = rootTrans:Find("Other/QnCheckbox1"):GetComponent(typeof(QnCheckBox))
  self.m_PlayOtherCheckBox:SetSelected(CLuaMusicBoxMgr.StopBGM, true)
  self.m_PlayOtherCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(val)
    CLuaMusicBoxMgr.StopBGM = val
  end)
--]]
  UIEventListener.Get(rootTrans:Find("SaveBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
    Gac2Gas.DiskPlayerSettingWhenPlayVoice(CLuaMusicBoxMgr.FurnitureId, CLuaMusicBoxMgr.PlayType, CLuaMusicBoxMgr.PlayLoop, CLuaMusicBoxMgr.StopBGM)
  end)
end

function CLuaMusicBoxMainWnd:InitBg()
  local interface = LuaDefaultModelTextureLoader.Create(function (ro)
    self.m_MusicBoxRO = ro
    ro:LoadMain("Assets/Res/Character/Jiayuan/jyjiaju_yinyuehe/Prefab/jyjiaju_yinyuehe_01.prefab", nil, false, false)
  end)
  self.m_ModelTexture = self.transform:Find("Anchor/Texture"):GetComponent(typeof(UITexture))
  self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_ModelCameraName, interface, 180, 0, -0.51, 2.17, false, true, 1.0)
end

function CLuaMusicBoxMainWnd:Rotate(bRotate)
  if not self.m_MusicBoxRO then return end
  self.m_MusicBoxRO:DoAni(bRotate and "rotate01" or "stand01", true, 0, 1, 0.15, true, 1.0)
  self.m_MusicBoxRO:RemoveFX("MusicBoxFx")
  if bRotate then
    local fx = CEffectMgr.Inst:AddObjectFX(88801031, self.m_MusicBoxRO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
    if fx then self.m_MusicBoxRO:AddFX("MusicBoxFx", fx) end
  end
end

function CLuaMusicBoxMainWnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.m_ModelCameraName)
end

function CLuaMusicBoxMainWnd:AddItem(args)
	if args[1] ~= "MusicBox" then return end

	local item = CItemMgr.Inst:GetById(args[0])
	if item then
		local itemId = args[0]
		local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    Gac2Gas.DiskPlayerAddRecordedDiskItem(CLuaMusicBoxMgr.FurnitureId, EnumItemPlace.Bag, pos, itemId)
  end
end

function CLuaMusicBoxMainWnd:OnEnable()
  g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "AddItem")
  g_ScriptEvent:AddListener("ResponseDiskPlayerData", self, "InitMusic")
end

function CLuaMusicBoxMainWnd:OnDisable()
  g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "AddItem")
  g_ScriptEvent:RemoveListener("ResponseDiskPlayerData", self, "InitMusic")
end
