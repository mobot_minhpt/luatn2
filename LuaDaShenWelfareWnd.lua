require("common/common_include")
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"

LuaDaShenWelfareWnd = class()

RegistClassMember(LuaDaShenWelfareWnd, "m_CloseBtn")
RegistClassMember(LuaDaShenWelfareWnd, "m_BindingLabel")
RegistClassMember(LuaDaShenWelfareWnd, "m_BindingBtn")
RegistClassMember(LuaDaShenWelfareWnd, "m_SpecTable")
RegistClassMember(LuaDaShenWelfareWnd, "m_SpecTemplate")

RegistClassMember(LuaDaShenWelfareWnd, "m_RewardTable")
RegistClassMember(LuaDaShenWelfareWnd, "m_RewardTemplate")
RegistClassMember(LuaDaShenWelfareWnd, "m_ItemTemplate")

function LuaDaShenWelfareWnd:Awake()
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    self.m_BindingLabel = self.transform:Find("Anchor/SpecificationView/BindingLabel"):GetComponent(typeof(UILabel))
    self.m_BindingBtn = self.transform:Find("Anchor/SpecificationView/BindingBtn"):GetComponent(typeof(CButton))
    self.m_SpecTable = self.transform:Find("Anchor/SpecificationView/SpecTable").gameObject
    self.m_SpecTemplate = self.transform:Find("Anchor/SpecificationView/SpecTemplate").gameObject

    self.m_RewardTable = self.transform:Find("Anchor/RewardView/ScrollView/Table").gameObject
    self.m_RewardTemplate = self.transform:Find("Anchor/RewardView/RewardTemplate").gameObject
    self.m_ItemTemplate = self.transform:Find("Anchor/RewardView/ItemTemplate").gameObject
end

function LuaDaShenWelfareWnd:Init()
    self.m_ItemTemplate:SetActive(false)
    self.m_RewardTemplate:SetActive(false)
    self.m_SpecTemplate:SetActive(false)

    --是否绑定大神
    self:ChangeBindingStatus(false)
    local achievementID = DaShen_Setting.GetData().AchievementID
    self:ChangeBindingStatus(
        CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(achievementID)
    )

    self:InitRewards()
    self:InitSpecification()
    
end

function LuaDaShenWelfareWnd:Start()
    CommonDefs.AddOnClickListener(
        self.m_CloseBtn,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnCloseButtonClick(go)
            end
        ),
        false
    )

    CommonDefs.AddOnClickListener(
        self.m_BindingBtn.gameObject,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnBindingBtnClick(go)
            end
        ),
        false
    )
end

function LuaDaShenWelfareWnd:InitSpecification()
    Extensions.RemoveAllChildren(self.m_SpecTable.transform)

    local rawStr = g_MessageMgr:FormatMessage("DASHEN_REWARD_SPECIFICATION", nil)

    local tipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(rawStr)

    if not tipInfo then return end

    local InfoList = tipInfo.paragraphs

    for i = 0, InfoList.Count-1 do
        local curSpec = UICommonDef.AddChild(self.m_SpecTable, self.m_SpecTemplate)
        curSpec:SetActive(true)
        local label = curSpec.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = InfoList[i]
        label.text = CChatLinkMgr.TranslateToNGUIText(InfoList[i], false)
    end

    self.m_SpecTable:GetComponent(typeof(UITable)):Reposition()
end

function LuaDaShenWelfareWnd:InitRewards()
    Extensions.RemoveAllChildren(self.m_RewardTable.transform)

    local rewardCount = DaShen_Reward.GetDataCount()
    for i = 1, rewardCount do
        local curCell = UICommonDef.AddChild(self.m_RewardTable, self.m_RewardTemplate)
        curCell:SetActive(true)
        self:InitOneReward(curCell,DaShen_Reward.GetData(i))
    end

    self.m_RewardTable:GetComponent(typeof(UITable)):Reposition()
end

function LuaDaShenWelfareWnd:InitOneReward(curCell, rewardInfo)
    if not rewardInfo then return end

    local rewardLabel = curCell.transform:Find("RewardTitle/label"):GetComponent(typeof(UILabel))
    local itemTable = curCell.transform:Find("ScrollView/ItemTable").gameObject
    local obtainBtn = curCell.transform:Find("ObtainBtn").gameObject

    rewardLabel.text = rewardInfo.Name

    Extensions.RemoveAllChildren(itemTable.transform)
    local itemArray = rewardInfo.RewardItems
    if itemArray then
        for i = 0, itemArray.Length-1 do
            local curItem = UICommonDef.AddChild(itemTable, self.m_ItemTemplate)
            curItem:SetActive(true)
            self:InitOneItem(curItem, itemArray[i])
        end
    end

    itemTable:GetComponent(typeof(UITable)):Reposition()
    
    CommonDefs.AddOnClickListener(
        obtainBtn,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnObtainBtnClick(go)
            end
        ),
        false
    )
end

function LuaDaShenWelfareWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaDaShenWelfareWnd:OnEnable()
end

function LuaDaShenWelfareWnd:OnDisable()
end

function LuaDaShenWelfareWnd:OnDestroy()
end

function LuaDaShenWelfareWnd:ChangeBindingStatus(bound)
    if bound then
        self.m_BindingLabel.text = LocalString.GetString("当前角色已绑定")
        self.m_BindingBtn.Enabled = false
    else
        self.m_BindingLabel.text = LocalString.GetString("当前尚未绑定角色")
        self.m_BindingBtn.Enabled = true
    end
end

--CallBacks
function LuaDaShenWelfareWnd:OnCloseButtonClick(go)
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaDaShenWelfareWnd:OnBindingBtnClick(go)
    local url = "https://app.16163.com/ds/ulinks/?utm_source=qnsy&utm_medium=game&utm_campaign=gift_center&utm_term=qnsy_wyds"
    CommonDefs.OpenURL(url)
end

function LuaDaShenWelfareWnd:OnObtainBtnClick(go)
    g_MessageMgr:ShowMessage("DASHEN_REWARD_OBTAIN")
end
