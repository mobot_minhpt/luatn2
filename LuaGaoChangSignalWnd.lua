local DelegateFactory  = import "DelegateFactory"
local ChatLocationLink = import "CChatLinkMgr+ChatLocationLink"
local CScene           = import "L10.Game.CScene"
local EChatPanel       = import "L10.Game.EChatPanel"
local CChatHelper      = import "L10.UI.CChatHelper"
local CMiniMap         = import "L10.UI.CMiniMap"

LuaGaoChangSignalWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaGaoChangSignalWnd, "grid")
RegistClassMember(LuaGaoChangSignalWnd, "template")

RegistClassMember(LuaGaoChangSignalWnd, "signalData")

function LuaGaoChangSignalWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitActive()
end

function LuaGaoChangSignalWnd:InitUIComponents()
    self.grid = self.transform:Find("Anchor/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.template = self.transform:Find("Anchor/Template").gameObject
end

function LuaGaoChangSignalWnd:InitActive()
    self.template:SetActive(false)
end


function LuaGaoChangSignalWnd:Init()
    self:InitData()
    self:InitGrid()
end

function LuaGaoChangSignalWnd:InitGrid()
    Extensions.RemoveAllChildren(self.grid.transform)

    for i = 1, #self.signalData do
        local data = self.signalData[i]
        local child = CUICommonDef.AddChild(self.grid.gameObject, self.template)
        child:SetActive(true)

        local channelLabel = child.transform:Find("Channel"):GetComponent(typeof(UILabel))
        local channelSprite = child.transform:Find("Channel/Sprite"):GetComponent(typeof(UISprite))
        if data.channel == EChatPanel.Team then
            channelLabel.text = LocalString.GetString("队伍")
            channelSprite.spriteName = g_sprites.TeamChannelSprite
        elseif data.channel == EChatPanel.Current then
            channelLabel.text = LocalString.GetString("当前")
            channelSprite.spriteName = g_sprites.WorldChannelSprite
        end

        child.transform:Find("Content"):GetComponent(typeof(UILabel)).text = data.content
        child.transform:Find("Location").gameObject:SetActive(data.needLocation)

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnItemClick(i)
        end)
    end
    self.grid:Reposition()
end

-- 初始化数据
function LuaGaoChangSignalWnd:InitData()
    self.signalData = {}
    GaoChangCross_Signal.Foreach(function (id, data)
        local channel
        if data.Channel == "team" then
            channel = EChatPanel.Team
        elseif data.Channel == "current" then
            channel = EChatPanel.Current
        else
            return
        end

        table.insert(self.signalData, {channel = channel, content = data.Content, needLocation = (data.NeedLocation == 1)})
    end)
end


--@region UIEvent

function LuaGaoChangSignalWnd:OnItemClick(i)
    if not CClientMainPlayer.Inst or not CScene.MainScene then return end

    local data = self.signalData[i]

    local msg
    if data.needLocation then
        local pos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
        local mapIdx = CMiniMap.Instance ~= nil and CMiniMap.Instance.mapIdx or 0
        msg = data.content..ChatLocationLink.GenerateLink(CScene.MainScene.SceneTemplateId, CScene.MainScene.SceneName, pos.x, pos.y, CScene.MainScene.SceneId, mapIdx).logicTag
    else
        msg = data.content
    end
    CChatHelper.SendMsg(data.channel, msg, 0)

    CUIManager.CloseUI(CLuaUIResources.GaoChangSignalWnd)
end

--@endregion UIEvent

