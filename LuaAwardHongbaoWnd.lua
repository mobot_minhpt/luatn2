require("common/common_include")

local UILabel = import "UILabel"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local EnumRequestSource = import "L10.UI.EnumRequestSource"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CHongBaoSettings = import "L10.UI.CHongBaoSettings"
local AlignType = import "L10.UI.CTooltip+AlignType"
local EnumAwardHongbaoType = import "L10.UI.EnumAwardHongbaoType"
local LuaUtils = import "LuaUtils"
local CGuildHongbaoSetting = import "L10.UI.CGuildHongbaoSetting"
local QnInput = import "L10.UI.QnInput"
local CButton = import "L10.UI.CButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Application = import "UnityEngine.Application"

CLuaAwardHongbaoWnd = class()
CLuaAwardHongbaoWnd.Path = "ui/hongbao/LuaAwardHongbaoWnd"

RegistClassMember(CLuaAwardHongbaoWnd, "m_JadeButton")
RegistClassMember(CLuaAwardHongbaoWnd, "m_HongbaoButton")
RegistClassMember(CLuaAwardHongbaoWnd, "m_SilverLabel")
RegistClassMember(CLuaAwardHongbaoWnd, "m_GuildSetting")
RegistClassMember(CLuaAwardHongbaoWnd, "m_ChooseButtonTable")
RegistClassMember(CLuaAwardHongbaoWnd, "m_ChooseRootTable")
RegistClassMember(CLuaAwardHongbaoWnd, "m_GuildBigType")
RegistClassMember(CLuaAwardHongbaoWnd, "m_CheckBoxTable")
RegistClassMember(CLuaAwardHongbaoWnd, "m_CheckBoxRootTable")
RegistClassMember(CLuaAwardHongbaoWnd, "m_GuildVoiceType")

RegistClassMember(CLuaAwardHongbaoWnd, "m_WorldInput")
RegistClassMember(CLuaAwardHongbaoWnd, "m_GuildNormalInput")
RegistClassMember(CLuaAwardHongbaoWnd, "m_CommandContentInput")
RegistClassMember(CLuaAwardHongbaoWnd, "m_QuestionContentInput")
RegistClassMember(CLuaAwardHongbaoWnd, "m_AnswerContentInput")

RegistClassMember(CLuaAwardHongbaoWnd, "m_CurSelectHongBaoCover")	-- 当前选择的红包封面
RegistClassMember(CLuaAwardHongbaoWnd, "m_WorldRootCoverSelect")
RegistClassMember(CLuaAwardHongbaoWnd, "m_GuildRootCoverSelect")
RegistClassMember(CLuaAwardHongbaoWnd, "m_HongBaoCoverDataList")

function CLuaAwardHongbaoWnd:Init( ... )
	local titleLabel = self.transform:Find("Wnd_Bg_Secondary_3/TitleLabel"):GetComponent(typeof(UILabel))
	titleLabel.text = CHongBaoMgr.m_HongbaoType == EnumHongbaoType.WorldHongbao and LocalString.GetString("世界红包") or LocalString.GetString("帮会红包")
	Gac2Gas.RequestRemainHongBaoSendTimes(EnumToInt(CHongBaoMgr.m_HongbaoType))
	local worldRootTrans = self.transform:Find("WorldRoot")
	local guildRootTrans = self.transform:Find("GuildRoot")
	guildRootTrans.gameObject:SetActive(CHongBaoMgr.m_HongbaoType == EnumHongbaoType.GuildHongbao and CHongBaoMgr.Inst.m_AwardHongbaoType == EnumAwardHongbaoType.GuildNormal)
	worldRootTrans.gameObject:SetActive(CHongBaoMgr.m_HongbaoType == EnumHongbaoType.WorldHongbao or CHongBaoMgr.Inst.m_AwardHongbaoType == EnumAwardHongbaoType.GuildHuaKui)
	local tipLabel = self.transform:Find("TipLabel"):GetComponent(typeof(UILabel))
	tipLabel.text = CHongBaoSettings.Rules_Description
	local tipBtnObj = self.transform:Find("TipBtn").gameObject
	tipBtnObj:SetActive(true)
	UIEventListener.Get(tipBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnTipBtnClick()
	end)

	if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.WorldHongbao then
		self.m_SilverLabel = worldRootTrans:Find("SilverAmount/SilverLabel"):GetComponent(typeof(UILabel))
		self.m_JadeButton = worldRootTrans:Find("JadeAmount/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
		self.m_JadeButton.onValueChanged = DelegateFactory.Action_uint(function (value)
			self:OnJadeValueChanged(value)
		end)
		self.m_JadeButton:SetMinMax(tonumber(CHongBaoSettings.Value_Min_World), tonumber(CHongBaoSettings.Value_Max_World), 1)
		self.m_JadeButton:SetValue(tonumber(CHongBaoSettings.Value_Min_World), true)
		self.m_HongbaoButton = worldRootTrans:Find("HongBaoCount/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
		self.m_HongbaoButton:SetMinMax(tonumber(CHongBaoSettings.Number_Min_World), tonumber(CHongBaoSettings.Number_Max_World), 1)
		self.m_HongbaoButton:SetValue(tonumber(CHongBaoSettings.Number_Min_World), true)

		self.m_WorldInput = worldRootTrans:Find("WishInput"):GetComponent(typeof(QnInput))
		worldRootTrans:Find("WishInput/ContentLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("请输入红包留言（最多%d个字）"), CHongBaoSettings.Max_Contents_Length)
	elseif CHongBaoMgr.Inst.m_AwardHongbaoType == EnumAwardHongbaoType.GuildHuaKui then
		titleLabel.text = LocalString.GetString("帮花红包")
		self.m_SilverLabel = worldRootTrans:Find("SilverAmount/SilverLabel"):GetComponent(typeof(UILabel))
		self.m_JadeButton = worldRootTrans:Find("JadeAmount/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
		self.m_JadeButton.onValueChanged = DelegateFactory.Action_uint(function (value)
			self:OnJadeValueChanged(value)
		end)
		self.m_JadeButton:SetMinMax(tonumber(HuaKui_Setting.GetData().BangHuaHongBaoAmount), tonumber(HuaKui_Setting.GetData().BangHuaHongBaoAmount), 1)
		self.m_JadeButton:SetValue(tonumber(HuaKui_Setting.GetData().BangHuaHongBaoAmount), true)
		self.m_HongbaoButton = worldRootTrans:Find("HongBaoCount/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
		self.m_HongbaoButton:SetMinMax(tonumber(HuaKui_Setting.GetData().BangHuaHongBaoNum), tonumber(HuaKui_Setting.GetData().BangHuaHongBaoNum), 1)
		self.m_HongbaoButton:SetValue(tonumber(HuaKui_Setting.GetData().BangHuaHongBaoNum), true)
		tipLabel.text = LocalString.GetString("帮花红包固定为1000灵玉，帮花大作战后1小时一个帮会可发一个。灵玉将以100：570000的比例转换为等值银两包入红包。")
		tipBtnObj:SetActive(false)

		self.m_WorldInput = worldRootTrans:Find("WishInput"):GetComponent(typeof(QnInput))
		worldRootTrans:Find("WishInput/ContentLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("请输入红包留言（最多%d个字）"), CHongBaoSettings.Max_Contents_Length)
	else
		self.m_GuildSetting = guildRootTrans:Find("GuildSetting/SettingPanel"):GetComponent(typeof(CGuildHongbaoSetting))
		self.m_GuildSetting.gameObject:SetActive(false)
		UIEventListener.Get(guildRootTrans:Find("GuildSetting").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
			self.m_GuildSetting:Show()
		end)
		self.m_SilverLabel = guildRootTrans:Find("SilverAmount/SilverLabel"):GetComponent(typeof(UILabel))
		self.m_JadeButton = guildRootTrans:Find("JadeAmount/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
		self.m_JadeButton.onValueChanged = DelegateFactory.Action_uint(function (value)
			self:OnJadeValueChanged(value)
		end)
		self.m_JadeButton:SetMinMax(tonumber(CHongBaoSettings.Value_Min_Guild), tonumber(CHongBaoSettings.Value_Max_Guild), 1)
		self.m_JadeButton:SetValue(tonumber(CHongBaoSettings.Value_Min_Guild), true)
		self.m_HongbaoButton = guildRootTrans:Find("HongBaoCount/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
		self.m_HongbaoButton:SetMinMax(tonumber(CHongBaoSettings.Number_Min_Guild), tonumber(CHongBaoSettings.Number_Max_Guild), 1)
		self.m_HongbaoButton:SetValue(tonumber(CHongBaoSettings.Number_Min_Guild), true)

		self:InitGuild(guildRootTrans)
	end

	self.m_JadeButton:SetNumberInputAlignType(AlignType.Right)
	self.m_HongbaoButton:SetNumberInputAlignType(AlignType.Right)

	UIEventListener.Get(self.transform:Find("AwardBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnAwardButtonClick()
	end)

	self:InitSelectBtn()
end

function CLuaAwardHongbaoWnd:InitGuild( guildRootTrans )
	local normalBtn = guildRootTrans:Find("ChooseButtonRoot/Normal"):GetComponent(typeof(CButton))
	local voiceBtn = guildRootTrans:Find("ChooseButtonRoot/Voice"):GetComponent(typeof(CButton))
	self.m_ChooseButtonTable = {normalBtn, voiceBtn}
	local normalRoot = guildRootTrans:Find("NormalRoot").gameObject
	local voiceRoot = guildRootTrans:Find("VoiceRoot").gameObject

	--海外版本不支持语音红包
	if not CommonDefs.IS_CN_CLIENT and Application.platform ~= RuntimePlatform.WindowsEditor then
		voiceBtn.gameObject:SetActive(false)
	end

	self.m_ChooseRootTable = {normalRoot, voiceRoot}
	for i = 1, #self.m_ChooseButtonTable do
		UIEventListener.Get(self.m_ChooseButtonTable[i].gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:OnChooseBtnClick(...)
		end)
	end

	self:OnChooseBtnClick(normalBtn.gameObject)

	local commandCheckBox = guildRootTrans:Find("VoiceRoot/CheckBoxGroup/CommandHongbao"):GetComponent(typeof(QnCheckBox))
	local questionCheckBox = guildRootTrans:Find("VoiceRoot/CheckBoxGroup/QuestionHongbao"):GetComponent(typeof(QnCheckBox))
	self.m_CheckBoxTable = {commandCheckBox, questionCheckBox}
	local commandRoot = guildRootTrans:Find("VoiceRoot/CommandContentInput").gameObject
	local questionRoot = guildRootTrans:Find("VoiceRoot/QuestionContentRoot").gameObject
	self.m_CheckBoxRootTable = {commandRoot, questionRoot}

	for i = 1, #self.m_CheckBoxTable do
		self.m_CheckBoxTable[i].OnValueChanged = DelegateFactory.Action_bool(function ( value )
			if value then
				self:OnCheckBoxClick(i)
			else
				self.m_CheckBoxTable[i]:SetSelected(true, true)
			end
		end)
	end

	self:OnCheckBoxClick(1)

	self.m_GuildNormalInput = guildRootTrans:Find("NormalRoot/ContentInput"):GetComponent(typeof(QnInput))
	self.m_GuildNormalInput.transform:Find("ContentLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("请输入红包留言（最多%d个字）"), CHongBaoSettings.Max_Contents_Length)

	self.m_CommandContentInput = guildRootTrans:Find("VoiceRoot/CommandContentInput"):GetComponent(typeof(QnInput))
	self.m_CommandContentInput.transform:Find("ContentLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("请输入口令（最多%d个字）"), HongBao_Setting.GetData().Max_Answer_Length)

	self.m_QuestionContentInput = guildRootTrans:Find("VoiceRoot/QuestionContentRoot/QuestionContentInput"):GetComponent(typeof(QnInput))
	self.m_QuestionContentInput.transform:Find("ContentLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("请输入提示（最多%d个字）"), CHongBaoSettings.Max_Contents_Length)

	self.m_AnswerContentInput = guildRootTrans:Find("VoiceRoot/QuestionContentRoot/AnswerContentInput"):GetComponent(typeof(QnInput))
	self.m_AnswerContentInput.transform:Find("ContentLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("请输入答案\n（最多%d个字）"), HongBao_Setting.GetData().Max_Answer_Length)

end

function CLuaAwardHongbaoWnd:CheckAndFilter(content, lengthLimit, msg)
	local len = string.len(content)
	if len > lengthLimit * 3 then
		if not msg then msg = "Hongbao_Input_Limit" end
		g_MessageMgr:ShowMessage(msg, lengthLimit)
		return false, nil
	end
	if len <= 0 then return true, nil end
	local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(content, true)
	if not ret.msg then return false, nil end
	if ret.shouldBeIgnore then g_MessageMgr:ShowMessage("Speech_Violation") return false, nil end

	local retStr = CChatMgr.s_EnableFilterNewLine and CChatMgr.Inst:FilterNewLine(ret.msg, 0, " ") or ret.msg
	retStr = CChatMgr.Inst:FilterYangYangEmotion(retStr)
	return true, retStr
end

function CLuaAwardHongbaoWnd:CheckAnswer(str)
	local i, len = 1, #str
	while true do
		if i > len then break end
		local c = string.byte(str, i)
		if not c then return false end
		-- digit
		if c >= 48 and c <= 57 then
			i = i + 1
		elseif c >= 228 and c <= 233 then
			local c1, c2 = string.byte(str, i + 1), string.byte(str, i + 2)
			if (not c1) or (not c2) then return false end
			if c == 228 then
				if c1 < 184 then
					return false
				elseif c1 == 184 then
					if c2 < 128 then
						return false
					end
				end
			elseif c == 233 then
				if c1 > 190 then
					return false
				elseif c1 == 190 then
					if c2 > 165 then
						return false
					end
				end
			end
			i = i + 3
		else
			return false
		end
	end
	return true
end

function CLuaAwardHongbaoWnd:OnAwardButtonClick( ... )
	local content = LocalString.GetString("恭喜发财")
	local answer = ""

	if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.GuildHongbao and CHongBaoMgr.Inst.m_AwardHongbaoType == EnumAwardHongbaoType.GuildNormal then
		-- Guild Voice Hongbao
		if self.m_GuildBigType == 2 then
			-- Guild Command Hongbao
			if self.m_GuildVoiceType == 1 then
				local bRet, retStr = self:CheckAndFilter(self.m_CommandContentInput.Text, HongBao_Setting.GetData().Max_Answer_Length)
				if not bRet then return end
				if not retStr or retStr == "" then g_MessageMgr:ShowMessage("HongBao_No_KouLing") return end
				if retStr then
					if not self:CheckAnswer(retStr) then g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("口令中只允许有中文和数字")) return end
					answer = retStr
					retStr = SafeStringFormat3(LocalString.GetString("请说出\"%s\""), retStr)
					content = retStr
				end
			else
				local bRet, retStr = self:CheckAndFilter(self.m_QuestionContentInput.Text, CHongBaoSettings.Max_Contents_Length, "Hongbao_Question_Input_Limit")
				if not bRet then return end
				if not retStr or retStr == "" then g_MessageMgr:ShowMessage("HongBao_No_Question") return end
				if retStr then
					retStr = SafeStringFormat3(LocalString.GetString("根据提示\"%s\"回答语音口令"), retStr)
					content = retStr
				end

				bRet, retStr = self:CheckAndFilter(self.m_AnswerContentInput.Text, HongBao_Setting.GetData().Max_Answer_Length, "Hongbao_Answer_Input_Limit")
				if not bRet then return end
				if not retStr or retStr == "" then g_MessageMgr:ShowMessage("HongBao_No_Answer") return end
				if retStr then
					if not self:CheckAnswer(retStr) then g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("答案中只允许有中文和数字")) return end
					answer = retStr
				end
			end
		else
			local bRet, retStr = self:CheckAndFilter(self.m_GuildNormalInput.Text, CHongBaoSettings.Max_Contents_Length)
			if not bRet then return end
			if retStr then content = retStr end
		end

	else
		local bRet, retStr = self:CheckAndFilter(self.m_WorldInput.Text, CHongBaoSettings.Max_Contents_Length)
		if not bRet then return end
		if retStr then content = retStr end
	end


	local text = SafeStringFormat3(LocalString.GetString("是否花费%d灵玉发红包？"), self.m_JadeButton:GetValue())
	g_MessageMgr:ShowOkCancelMessage(text, function ( ... )
		if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.GuildHongbao and CHongBaoMgr.Inst.m_AwardHongbaoType == EnumAwardHongbaoType.GuildHuaKui then
			Gac2Gas.RequestSendBangHuaHongBao(content)
		else
			Gac2Gas.RequestSendHongBao(self.m_JadeButton:GetValue(), self.m_HongbaoButton:GetValue(), content, CommonDefs.Convert_ToUInt32(CHongBaoMgr.m_HongbaoType), self.m_GuildSetting and self.m_GuildSetting:GetGuildSettingId() or 0, answer,self.m_CurSelectHongBaoCover)
		end
	end, nil, nil, nil, false)
end

function CLuaAwardHongbaoWnd:OnChooseBtnClick( go )
	for i = 1, #self.m_ChooseButtonTable do
		local isCur = self.m_ChooseButtonTable[i].gameObject == go
		self.m_ChooseButtonTable[i].Selected = isCur
		self.m_ChooseRootTable[i]:SetActive(isCur)
		if isCur then self.m_GuildBigType = i end
	end
end

function CLuaAwardHongbaoWnd:OnCheckBoxClick( index )
	for i = 1, #self.m_CheckBoxRootTable do
		self.m_CheckBoxRootTable[i]:SetActive(i == index)
		self.m_CheckBoxTable[i]:SetSelected(i == index, true)
	end
	self.m_GuildVoiceType = index
end

function CLuaAwardHongbaoWnd:OnTipBtnClick( ... )
	if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.WorldHongbao then
		g_MessageMgr:ShowMessage("Send_HongBao_Rules_World")
	elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.GuildHongbao then
		if self.m_GuildBigType == 2 then
			g_MessageMgr:ShowMessage("Send_Voice_HongBao_Rules_Guild")
		else
			g_MessageMgr:ShowMessage("Send_HongBao_Rules_Guild")
		end
	end
end

function CLuaAwardHongbaoWnd:OnJadeValueChanged( value )
	self.m_SilverLabel.text = tostring(value * CHongBaoSettings.LingYuEqualYinLiang)
end

function CLuaAwardHongbaoWnd:ReplySendHongBaoResult( argv )
	if argv[0] ~= 0 then return end
	CHongBaoMgr.m_RequestSource = EnumRequestSource.RefreshRequest
	Gac2Gas.RequestRecentHongBaoInfo(EnumToInt(CHongBaoMgr.m_HongbaoType))
	CUIManager.CloseUI(CIndirectUIResources.AwardHongBaoWnd)
end

function CLuaAwardHongbaoWnd:ReplyRemainHongBaoSendTimes( argv )
	if argv[0] == EnumToInt(CHongBaoMgr.m_HongbaoType) then
		self.transform:Find("LeftCountLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("今日还可发%d次"), argv[1])
	end
end

function CLuaAwardHongbaoWnd:InitSelectBtn()
	self.m_CurSelectHongBaoCover = CLuaHongBaoMgr.GetCurHongBaoCover()
	self.m_WorldRootCoverSelect = self.transform:Find("WorldRoot/CoverSelect/CoverSelectBtn").gameObject
	self.m_GuildRootCoverSelect = self.transform:Find("GuildRoot/CoverSelectBtn").gameObject
	if CHongBaoMgr.Inst.m_AwardHongbaoType == EnumAwardHongbaoType.GuildHuaKui then
		self.transform:Find("WorldRoot/CoverSelect").gameObject:SetActive(false)
		self.m_GuildRootCoverSelect.gameObject:SetActive(false)
		return
	end
	--self.m_QuickChatButton.transform.position
	local name = HongBao_Cover.GetData(self.m_CurSelectHongBaoCover).Name
	self.m_WorldRootCoverSelect.transform:Find("Label").gameObject:GetComponent(typeof(UILabel)).text = name
	self.m_GuildRootCoverSelect.transform:Find("Label").gameObject:GetComponent(typeof(UILabel)).text = name
	CommonDefs.AddOnClickListener(self.m_WorldRootCoverSelect, DelegateFactory.Action_GameObject(function (go)
        self:OnSelectBtnClick(go.transform.position)
    end), false)
	CommonDefs.AddOnClickListener(self.m_GuildRootCoverSelect, DelegateFactory.Action_GameObject(function (go)
        self:OnSelectBtnClick(go.transform.position)
    end), false)
end

function CLuaAwardHongbaoWnd:OnSelectBtnClick(worldPos)
	if not self.m_HongBaoCoverDataList then
		self.m_HongBaoCoverDataList = {}
		local curDataInfo = self:GetCoverData(self.m_CurSelectHongBaoCover,HongBao_Cover.GetData(self.m_CurSelectHongBaoCover))
		table.insert(self.m_HongBaoCoverDataList,curDataInfo) -- 把当前选中的红包放在第一位
		HongBao_Cover.Foreach(function(k,v)
			if k ~= self.m_CurSelectHongBaoCover and CLuaHongBaoMgr.HasGetHongBaoCover(k) then
				table.insert(self.m_HongBaoCoverDataList,self:GetCoverData(k,v))
			end
		end)
	end
	CLuaHongBaoMgr.ShowPopupMenu(worldPos,"AwardHongBaoCoverList",1,self.m_HongBaoCoverDataList) 
end

function CLuaAwardHongbaoWnd:GetCoverData(key,coverInfo)
	local data = {}
	data.key = key
	data.texturePath = "UI/Texture/Transparent/Material/"..coverInfo.MainResource..".mat" 
	data.name = coverInfo.Name
	local endTime = coverInfo.EndTime
	if System.String.IsNullOrEmpty(endTime) then
		data.describe = LocalString.GetString("永久有效")
	else
		local timeList = g_LuaUtil:StrSplit(endTime," ")
		data.describe = SafeStringFormat3(LocalString.GetString("有效期至 %s"),timeList[1])
	end
	return data
end

function CLuaAwardHongbaoWnd:OnSelectHongBaoCoverResult(index)
	if CLuaHongBaoMgr.m_PopupMenuData.type == "AwardHongBaoCoverList" then
		if not self.m_HongBaoCoverDataList then return end
		local CurData = self.m_HongBaoCoverDataList[index]
		self.m_CurSelectHongBaoCover = CurData.key
		table.remove(self.m_HongBaoCoverDataList,index)
		table.insert(self.m_HongBaoCoverDataList,1,CurData)
		self.m_WorldRootCoverSelect.transform:Find("Label").gameObject:GetComponent(typeof(UILabel)).text = CurData.name
		self.m_GuildRootCoverSelect.transform:Find("Label").gameObject:GetComponent(typeof(UILabel)).text = CurData.name
	end
end

function CLuaAwardHongbaoWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("ReplySendHongBaoResult", self, "ReplySendHongBaoResult")
	g_ScriptEvent:AddListener("ReplyRemainHongBaoSendTimes", self, "ReplyRemainHongBaoSendTimes")
	g_ScriptEvent:AddListener("ClickPopupMenuWithPic",self,"OnSelectHongBaoCoverResult")
end

function CLuaAwardHongbaoWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("ReplySendHongBaoResult", self, "ReplySendHongBaoResult")
	g_ScriptEvent:RemoveListener("ReplyRemainHongBaoSendTimes", self, "ReplyRemainHongBaoSendTimes")
	g_ScriptEvent:RemoveListener("ClickPopupMenuWithPic",self,"OnSelectHongBaoCoverResult")
end
