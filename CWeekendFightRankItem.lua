-- Auto Generated!!
local Constants = import "L10.Game.Constants"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local CWeekendFightRankItem = import "L10.UI.CWeekendFightRankItem"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local Profession = import "L10.Game.Profession"
CWeekendFightRankItem.m_Init_CS2LuaHook = function (this, info, row) 
    CWeekendFightMgr.Inst:SetRankLabel(this.rankNode, info.Rank)


    local extraList = MsgPackImpl.unpack(info.extraData)
    local score1 = 0
    local score2 = 0

    if extraList ~= nil then
        local sllist = TypeAs(extraList, typeof(MakeGenericClass(List, Object)))
        if sllist.Count == 2 then
            score1 = math.floor(tonumber(sllist[0] or 0))
            score2 = math.floor(tonumber(sllist[1] or 0))
        end
    end

    this.score1Label.text = tostring(score1)
    this.score2Label.text = tostring(score2)
    this.scoreTotal.text = tostring(info.Value)
    this.playerName.text = info.Name
    this.classSprite.spriteName = Profession.GetIcon(info.Job)

    if row >= 0 then
        if row % 2 == 0 then
            this:SetBackgroundTexture(Constants.EvenBgSprite)
        else
            this:SetBackgroundTexture(Constants.OddBgSpirite)
        end
    end
end
