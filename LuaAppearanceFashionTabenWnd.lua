local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local EnumPreviewType = import "L10.UI.EnumPreviewType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CFashionMgr = import "L10.Game.CFashionMgr"

LuaAppearanceFashionTabenWnd = class()

RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_SourceItem", "SourceItem", GameObject)
RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_TargetItem", "TargetItem", GameObject)
RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_ConsumeItem", "ConsumeItem", GameObject)
RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_LingYuCheckBox", "CheckBox", QnCheckBox)
RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_MoneyCtrl", "MoneyCtrl", CCurentMoneyCtrl)
RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_PreviewButton", "PreviewButton", GameObject)
RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_MakeTabenButton", "MakeTabenButton", GameObject)
RegistChildComponent(LuaAppearanceFashionTabenWnd, "m_InfoButton", "InfoButton", GameObject)

RegistClassMember(LuaAppearanceFashionTabenWnd, "m_SourceItemIcon")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_SourceItemBorder")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_SourceItemAddGo")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_TargetItemIcon")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_TargetItemBorder")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_ConsumeItemIcon")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_ConsumeItemAmountLabel")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_ConsumeItemAcquireGo")

RegistClassMember(LuaAppearanceFashionTabenWnd, "m_TabenTranslateItemId")
RegistClassMember(LuaAppearanceFashionTabenWnd, "m_CurEquip")
-- 逻辑仿照原有的CFashionTabenDetailWnd来处理
function LuaAppearanceFashionTabenWnd:Awake()
    self.m_TabenTranslateItemId = tonumber(Fashion_Setting.GetData("Taben_Translate_ItemId").Value)

    print(self.m_SourceItem)
    self.m_SourceItemIcon = self.m_SourceItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    self.m_SourceItemBorder = self.m_SourceItem.transform:Find("Border"):GetComponent(typeof(UISprite))
    self.m_SourceItemAddGo = self.m_SourceItem.transform:Find("Add").gameObject
    self.m_TargetItemIcon = self.m_TargetItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    self.m_TargetItemBorder = self.m_TargetItem.transform:Find("Border"):GetComponent(typeof(UISprite))
    self.m_ConsumeItemIcon = self.m_ConsumeItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    self.m_ConsumeItemAmountLabel = self.m_ConsumeItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    self.m_ConsumeItemAcquireGo = self.m_ConsumeItem.transform:Find("Acquire").gameObject

    self.m_LingYuCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (checked) self:OnLingYuCheckBoxChanged(checked) end)

   
    UIEventListener.Get(self.m_SourceItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSourceItemClick() end)
    UIEventListener.Get(self.m_ConsumeItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnConsumItemClick() end)
    UIEventListener.Get(self.m_PreviewButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnPreviewButtonClick() end)
    UIEventListener.Get(self.m_MakeTabenButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnMakeTabenButtonClick() end)
    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnInfoButtonClick() end)
end

function LuaAppearanceFashionTabenWnd:Init()
    local item = CItemMgr.Inst:GetItemTemplate(self.m_TabenTranslateItemId)
    local mallData = Mall_LingYuMall.GetData(self.m_TabenTranslateItemId)
    self.m_MoneyCtrl:SetType(mallData.CanUseBindJade>0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    self.m_MoneyCtrl:SetCost(mallData.Jade)
    self.m_ConsumeItemIcon:LoadMaterial(item.Icon)
    self:UpdateConsumItemAmount()
    --默认不自动消耗灵玉
    self.m_LingYuCheckBox.Selected = false
end

function LuaAppearanceFashionTabenWnd:OnLingYuCheckBoxChanged(checked)
    local bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_TabenTranslateItemId)
    if bindCount+notBindCount ==0 and checked then
        self.m_ConsumeItem:SetActive(false) 
        self.m_MoneyCtrl.gameObject:SetActive(true)
    else
        self.m_ConsumeItem:SetActive(true) 
        self.m_MoneyCtrl.gameObject:SetActive(false)
    end
end

function LuaAppearanceFashionTabenWnd:UpdateConsumItemAmount()
    local bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_TabenTranslateItemId)
    if bindCount+notBindCount > 0 then
        self.m_ConsumeItemAmountLabel.text = SafeStringFormat3("%d/1",bindCount + notBindCount)
        self.m_ConsumeItemAcquireGo:SetActive(false)
    else
        self.m_ConsumeItemAmountLabel.text = "[c][FF0000]0[-]/1"
        self.m_ConsumeItemAcquireGo:SetActive(true)
    end
end

function LuaAppearanceFashionTabenWnd:OnSourceItemClick()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local equipList = CreateFromClass(MakeGenericClass(List, cs_string))
    local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i=1,bagSize do
        local id = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        local equip = id and CItemMgr.Inst:GetById(id) or nil
        if equip and equip.IsEquip then
            local template = EquipmentTemplate_Equip.GetData(equip.TemplateId)
            local validtype = CFashionMgr.IsFashionTypePos(template.Type)
            if equip.MainPlayerIsFit and validtype and not equip.Equip.IsShenBing then
                CommonDefs.ListAdd_LuaCall(equipList, id)
            end
        end
    end
    if (equipList.Count > 0) then
        local dict = CreateFromClass(MakeGenericClass(Dictionary, int, MakeGenericClass(List, cs_string)))
        CommonDefs.DictAdd_LuaCall(dict, 0, equipList)
        CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("AppearanceFashionTaben", dict, LocalString.GetString("选择拓本原材料"), 0, false, LocalString.GetString("选择"), "", nil, "")
    else
        g_MessageMgr:ShowMessage("NO_EQUIP_FOR_TABEN")
    end
end

function LuaAppearanceFashionTabenWnd:OnConsumItemClick()
    local bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.m_TabenTranslateItemId)
    if bindCount+notBindCount > 0 then
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_TabenTranslateItemId)
    else
        LuaItemAccessListMgr:ShowItemAccessInfoAtRight(self.m_TabenTranslateItemId, false, self.m_ConsumeItemIcon.transform)
    end
end

function LuaAppearanceFashionTabenWnd:OnPreviewButtonClick()
    if self.m_CurEquip then
        CFashionPreviewMgr.ShowFashionPreview(self.m_CurEquip.TemplateId, EnumPreviewType.PreviewWeaponTaben, self.m_CurEquip.Equip.ForgeLevel)
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("还没有选择拓本原料呢！"))
    end
end

function LuaAppearanceFashionTabenWnd:OnMakeTabenButtonClick()
    if self.m_CurEquip==nil then
        g_MessageMgr:ShowMessage("Taben_Equip_Material_Not_Exist")
        return
    end
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("TABEN_CONFIRM_MESSAGE", self.m_CurEquip.Name), function ()
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, self.m_CurEquip.Id)
        Gac2Gas.RequestCompositeTaben_Permanent(EnumToInt(EnumItemPlace.Bag), pos, self.m_LingYuCheckBox.Selected)
        CUIManager.CloseUI("AppearanceFashionTabenWnd")
    end, nil, nil, nil, false)
end

function LuaAppearanceFashionTabenWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("TABEN_NOTICE")
end

function LuaAppearanceFashionTabenWnd:OnEnable()
    g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected",self,"OnFreightSubmitEquipSelected")
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
end

function LuaAppearanceFashionTabenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected",self,"OnFreightSubmitEquipSelected")
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
end

function LuaAppearanceFashionTabenWnd:OnFreightSubmitEquipSelected(args)
    local id = args[0]
    local key = args[1]
    if key == "AppearanceFashionTaben" then
        local equip = CItemMgr.Inst:GetById(id)
        if equip and equip.IsEquip then
            self.m_CurEquip = equip
            self.m_SourceItemIcon:LoadMaterial(equip.Icon)
            self.m_SourceItemBorder.spriteName = CUICommonDef.GetItemCellBorder(equip.Equip.QualityType)
            self.m_TargetItemIcon:LoadMaterial(equip.Icon)
            self.m_SourceItemAddGo:SetActive(false)
            --self.m_TargetItemNameLabel.text = cs_string.Format(LocalString.GetString("{0}·拓本"), equip.Name)
        elseif self.m_CurEquip==nil then
            self.m_SourceItemIcon:Clear()
            self.m_SourceItemBorder.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
            self.m_TargetItemIcon:Clear()
            self.m_SourceItemAddGo:SetActive(true)
        end
    end
end

function LuaAppearanceFashionTabenWnd:OnSendItem(args)
    self:UpdateConsumItemAmount()
end

function LuaAppearanceFashionTabenWnd:OnSetItemAt(args)
    self:UpdateConsumItemAmount()
end