local QnTableView=import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"

CLuaZswTemplateComponentListWnd = class()

RegistChildComponent(CLuaZswTemplateComponentListWnd, "m_TableView","TableView", QnTableView)
RegistChildComponent(CLuaZswTemplateComponentListWnd, "m_TemplateName","TemplateName", UILabel)

RegistClassMember(CLuaZswTemplateComponentListWnd, "m_ComponentId2Count")
RegistClassMember(CLuaZswTemplateComponentListWnd, "m_List")

CLuaZswTemplateComponentListWnd.s_IsDefault = false

function CLuaZswTemplateComponentListWnd:Awake()
    self.m_List = {}
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_List     
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_ComponentId2Count = {}

    if CLuaZswTemplateComponentListWnd.s_IsDefault then
        self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("默认装饰物模板")
    end
end

function CLuaZswTemplateComponentListWnd:Init()
    local titleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))
    --给自定义装饰物模板
    if CLuaZswTemplateMgr.CustomIndexForListWnd then
        self:InitForCustom()
        titleLabel.text = LocalString.GetString("自定义装饰物模板")
        return
    end

    if not CLuaZswTemplateMgr.CurPreviewTemplateId then return end
    local templateId = CLuaZswTemplateMgr.CurPreviewTemplateId
    local tdata = Zhuangshiwu_ZswTemplate.GetData(templateId)
    local zdata = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    if not tdata or not zdata then return end

    self.m_TemplateName.text = zdata.Name
    for i=0,tdata.DefaultComponents.Length - 1,1 do 
        local id = tdata.DefaultComponents[i]
        if self.m_ComponentId2Count[id] then
            self.m_ComponentId2Count[id] = self.m_ComponentId2Count[id] + 1
        else
            self.m_ComponentId2Count[id] = 1
            table.insert(self.m_List,id)
        end
    end
    self.m_TableView:ReloadData(false, false)

end

function CLuaZswTemplateComponentListWnd:InitForCustom()
    local templateData = CLuaZswTemplateMgr.GetCustomTemplateByIndex(CLuaZswTemplateMgr.CustomIndexForListWnd)
    print(CLuaZswTemplateMgr.CustomIndexForListWnd,templateData)
    if not templateData then return end
    self.m_TemplateName.text = templateData.name
    for i=1,#templateData.components,1 do 
        local component = templateData.components[i]
        local id = component.id
        if self.m_ComponentId2Count[id] then
            self.m_ComponentId2Count[id] = self.m_ComponentId2Count[id] + 1
        else
            self.m_ComponentId2Count[id] = 1
            table.insert(self.m_List,id)
        end
    end
    self.m_TableView:ReloadData(false, false)

end

function CLuaZswTemplateComponentListWnd:InitItem(item, row)
    local id = self.m_List[row + 1]

    local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local type = item.transform:Find("Type"):GetComponent(typeof(UILabel))
    local count = item.transform:Find("Count"):GetComponent(typeof(UILabel))

    local zdata = Zhuangshiwu_Zhuangshiwu.GetData(id)
    if zdata then
        name.text = zdata.Name
        local typeName = CClientFurnitureMgr.GetTypeNameByType(zdata.Type)
        type.text = SafeStringFormat3(LocalString.GetString("%d级%s"),zdata.Grade,typeName)
        count.text = self.m_ComponentId2Count[id]
    end
end

function CLuaZswTemplateComponentListWnd:OnDestroy()
    CLuaZswTemplateComponentListWnd.s_IsDefault = false
    CLuaZswTemplateMgr.CustomIndexForListWnd = nil
end