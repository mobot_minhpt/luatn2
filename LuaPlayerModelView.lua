local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CPropertySkill = import "L10.Game.CPropertySkill"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local EGetPlayerAppearanceContext = import "L10.Game.EGetPlayerAppearanceContext"


LuaPlayerModelView = class()

RegistChildComponent(LuaPlayerModelView, "ModelTexture", "ModelTexture", CUITexture)

RegistClassMember(LuaPlayerModelView, "m_PlayerId")
RegistClassMember(LuaPlayerModelView, "m_AppearanceData")
RegistClassMember(LuaPlayerModelView, "m_Class")
RegistClassMember(LuaPlayerModelView, "m_Gender")
RegistClassMember(LuaPlayerModelView, "m_ModelTextureLoader")

function LuaPlayerModelView:Init(playerId,cls,gender)
    self.m_PlayerId = playerId
    self.m_Class = cls
    self.m_Gender = gender

    self.ModelTexture.mainTexture = nil

    --先加载默认的模型
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_RO = ro
        self:LoadModel(self.m_RO)
    end)
    self.ModelTexture.mainTexture = CUIManager.CreateModelTexture(self:GetIdentifier(), self.m_ModelTextureLoader, 180, 0.05, -1, 4.66, false, true, 1)

    --初始不显示默认角色模型
    if self.m_PlayerId>0 then
        CClientPlayerMgr.Inst:RequestGetPlayerAppearance(playerId, EGetPlayerAppearanceContext.UpdateTeamMember)
    end
end

function LuaPlayerModelView:GetIdentifier()
    return string.format("__%s__", self.gameObject:GetInstanceID())
end

function LuaPlayerModelView:OnDestroy()
    self.ModelTexture.mainTexture = nil
    CUIManager.DestroyModelTexture(self:GetIdentifier())
end

function LuaPlayerModelView:OnEnable()
    g_ScriptEvent:AddListener("PlayerAppearanceUpdate", self, "OnPlayerAppearanceUpdate")

end
function LuaPlayerModelView:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerAppearanceUpdate", self, "OnPlayerAppearanceUpdate")
end

function LuaPlayerModelView:OnPlayerAppearanceUpdate(args)
    local playerId = tonumber(args[0])

    if playerId == self.m_PlayerId then
        -- CUIManager.DestroyModelTexture(self:GetIdentifier())
        self.m_AppearanceData = CClientPlayerMgr.Inst:GetPlayerAppearance(self.m_PlayerId)
        self.ModelTexture.mainTexture = CUIManager.CreateModelTexture(self:GetIdentifier(), self.m_ModelTextureLoader, 180, 0.05, -1, 4.66, false, true, 1)
    end

end
function LuaPlayerModelView:LoadModel(ro)
    if self.m_AppearanceData then
        CClientMainPlayer.LoadResource(ro, self.m_AppearanceData, true, 1, 0, false, self.m_AppearanceData.ResourceId, false, 0, false, nil)

        if self.m_AppearanceData.ResourceId ~= 0 then
            local scale = 1
            local data = Transform_Transform.GetData(self.m_AppearanceData.ResourceId)
            if data ~= nil then
                scale = data.PreviewScale
            end
            ro.Scale = scale
        end
    else
        local init = Initialization_Init.GetData(self.m_Class * 100 + self.m_Gender)
        if init ~= nil then
            local isInXianShenStatus = false

            local cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_Class)
            local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), self.m_Gender)
            
            local curAppearanceData = CPropertyAppearance.GetDataFromCustom(cls, gender, 
                CPropertySkill.GetDefaultYingLingStateByGender(gender), init.NewRoleEquip3, nil, 0, init.Head, 0, 0, 
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, isInXianShenStatus and 1 or 0, nil, nil, nil, nil, 0, 0, 0, 0, 0, 0, nil)
            CClientMainPlayer.LoadResource(ro, curAppearanceData, true, 1, 0, false, 0, false, 0, false, nil)
        end
    end
end