-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CShiTuAskWnd = import "L10.UI.CShiTuAskWnd"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CShiTuAskWnd.m_OkBtnFunc_CS2LuaHook = function (this, go) 
    this:Close()
    CShiTuMgr.Inst.CurrentEnterType = CShiTuMgr.ShiTuChooseType.TU
    CommonDefs.DictClear(CShiTuMgr.Inst.SaveResultValue)
    CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
    if this.sign.activeSelf then
        Gac2Gas.NeverRemindBaiShiOnLevelUp()
    end
end
CShiTuAskWnd.m_CloseBtnFunc_CS2LuaHook = function (this, go) 
    this:Close()
    Gac2Gas.NeedNotBaiShi()
    if this.sign.activeSelf then
        Gac2Gas.NeverRemindBaiShiOnLevelUp()
    end
end
CShiTuAskWnd.m_ClickBtnFunc_CS2LuaHook = function (this, go) 
    if this.sign.activeSelf then
        this.sign:SetActive(false)
    else
        g_MessageMgr:ShowMessage("NeverRemindBaiShi")
        this.sign:SetActive(true)
    end
end
CShiTuAskWnd.m_Init_CS2LuaHook = function (this) 
    if this.sign == nil then
        this.sign = this.clickBtn.transform:Find("node").gameObject
    end
    this.sign:SetActive(false)
    UIEventListener.Get(this.okBtn).onClick = MakeDelegateFromCSFunction(this.OkBtnFunc, VoidDelegate, this)

    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.CloseBtnFunc, VoidDelegate, this)

    UIEventListener.Get(this.clickBtn).onClick = MakeDelegateFromCSFunction(this.ClickBtnFunc, VoidDelegate, this)
end
