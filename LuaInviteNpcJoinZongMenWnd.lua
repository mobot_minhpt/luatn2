local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local QnTableView=import "L10.UI.QnTableView"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUITexture = import "L10.UI.CUITexture"
local UIInput = import "UIInput"
local QnTableItem = import "L10.UI.QnTableItem"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local UInt32 = import "System.UInt32"
local Object = import "System.Object"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CButton = import "L10.UI.CButton"

LuaInviteNpcJoinZongMenWnd = class()

RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "SortbyLabel", "SortbyLabel", UILabel)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "ShowSortbyViewBtn", "ShowSortbyViewBtn", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "NormalView", "NormalView", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "SearchResultView", "SearchResultView", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "JoinedView", "JoinedView", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "FreeNpcTableView", "FreeNpcTableView", QnTableView)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "InputView", "InputView", UIInput)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "SeachBtn", "SeachBtn", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "InviteBtn", "InviteBtn", CButton)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "ResultTable", "ResultTable", UIGrid)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "FreeNpcItem", "FreeNpcItem", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "JoinedNpcItem", "JoinedNpcItem", GameObject)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "InviteTimeInfoLabel", "InviteTimeInfoLabel", UILabel)
RegistChildComponent(LuaInviteNpcJoinZongMenWnd, "DeleteInputViewBtn", "DeleteInputViewBtn", GameObject)

RegistClassMember(LuaInviteNpcJoinZongMenWnd,"m_SortByIndex")
RegistClassMember(LuaInviteNpcJoinZongMenWnd,"m_JoinedList")
RegistClassMember(LuaInviteNpcJoinZongMenWnd,"m_UnJoinList")
RegistClassMember(LuaInviteNpcJoinZongMenWnd,"m_NameToData")
RegistClassMember(LuaInviteNpcJoinZongMenWnd,"m_NpcIdToData")
RegistClassMember(LuaInviteNpcJoinZongMenWnd,"m_SelectNpcId")
RegistClassMember(LuaInviteNpcJoinZongMenWnd,"m_LastSelectItem")

function LuaInviteNpcJoinZongMenWnd:OnEnable()
    g_ScriptEvent:AddListener("SendAllSectNpcData",self,"OnSendAllSectNpcData")
end
function LuaInviteNpcJoinZongMenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendAllSectNpcData",self,"OnSendAllSectNpcData")
    LuaInviteNpcMgr.MySectNpcId = nil
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.InviteNpcJoinSectGuide) then
        if CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), 22111234) then
            local currentTask = CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), 22111234)
            local completed = currentTask.CanSubmit == 1
            if not completed then
                local empty = CreateFromClass(MakeGenericClass(List, Object))
                empty = MsgPackImpl.pack(empty)
                Gac2Gas.FinishClientTaskEvent(22111234, "FinishSectInviteNpcGuide", empty)
            end
        end
    end
end

function LuaInviteNpcJoinZongMenWnd:Awake()
    self.m_UnJoinList = {}
    self.m_NameToData = {}
    self.m_NpcIdToData = {}
    self.m_SelectNpcId = nil

    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("InviteNpc_Join_ZongMen_Tip")
    end)
    UIEventListener.Get(self.ShowSortbyViewBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickShowSortbyViewBtn(go)
    end)
    UIEventListener.Get(self.InviteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnInviteBtnClick()
    end)
    UIEventListener.Get(self.SeachBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSearchBtnClick()
    end)
    UIEventListener.Get(self.DeleteInputViewBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnDeleteInputViewBtnClick()
    end)
    self.SearchResultView:SetActive(false)
    self:OnSendAllSectNpcData()
    self:UpdateInviteTimeInfoLabel()
end

function LuaInviteNpcJoinZongMenWnd:Init()
    self.TabBar:ChangeTab(0, false)
    self.FreeNpcTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_UnJoinList
        end,
        function(item,row)
            local info = self.m_UnJoinList[row + 1]
            self:InitFreeNpcItem(item,0,info)
        end)
    
    self.FreeNpcTableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)

    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self.InputView.value = ""
        if self.SearchResultView.activeSelf then
            self.SearchResultView:SetActive(false)
            self.NormalView:SetActive(true)    
        end
        self.FreeNpcTableView:ReloadData(true,true)
        self.m_SelectNpcId = nil
        self.m_LastSelectItem = nil
    end)

    self:OnSortNpcItem(0)
end

function LuaInviteNpcJoinZongMenWnd:OnSelectAtRow(row)
    local info = self.m_UnJoinList[row+1]
    if info and CClientMainPlayer.Inst then
        self.m_SelectNpcId = info.npcId
		local isMySect = info.sectId == CClientMainPlayer.Inst.BasicProp.SectId
		local isInvited = LuaInviteNpcMgr.MySectNpcId ~= 0
		local isMyInvitedNpc = LuaInviteNpcMgr.MySectNpcId == info.npcId
        local inviteBtnState = (isInvited and not isMyInvitedNpc) or (info.isJoined and isMyInvitedNpc and isMySect)
        self.InviteBtn.Enabled = not inviteBtnState
        self.InviteBtn.Text = (self.InviteBtn.Enabled and self.m_SelectNpcId == LuaInviteNpcMgr.MySectNpcId) and 
			LocalString.GetString("取消邀请") or LocalString.GetString("邀请")
    end
end

function LuaInviteNpcJoinZongMenWnd:UpdateInviteTimeInfoLabel()
    if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
        self.InviteTimeInfoLabel.text = ""
        return 
    end
    local nowTime = CServerTimeMgr.Inst:GetZone8Time()
    local timeInfoMsgs = SectInviteNpc_Setting.GetData().TimeInfoMsgs
    local timArr = {tonumber(timeInfoMsgs[0]),tonumber(timeInfoMsgs[2]),tonumber(timeInfoMsgs[4])}
    local inInviteTime = LuaInviteNpcMgr.InCompetition
    local isZhangeMen = LuaInviteNpcMgr.IsSectLeader
    self.InviteBtn.Enabled = isZhangeMen
    if inInviteTime then
        for index,hour in pairs(timArr) do
            if nowTime.Hour < hour then
                self.InviteTimeInfoLabel.text = timeInfoMsgs[index * 2 - 1]
                break
            end
        end
    else
        self.InviteTimeInfoLabel.text = timeInfoMsgs[6]
    end
end

function LuaInviteNpcJoinZongMenWnd:InitFreeNpcItem(item,row,temp)
    local info
    if temp then
        info = temp
    else
        info = self.m_UnJoinList[row+1]
    end
    local grade = info.grade
    local index2GradeDict = {5,4,3,2}
    if self.TabBar.SelectedIndex > 0 then
        if grade ~= index2GradeDict[self.TabBar.SelectedIndex] then
            item.gameObject:SetActive(false)
            return
        end
    else
        if grade == 1 then
            item.gameObject:SetActive(false)
            return
        end
    end
    local portrait = item.transform:Find("Border/Portrait"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local locationLabel = item.transform:Find("LocationLabel"):GetComponent(typeof(UILabel))
    local qualityLabel = item.transform:Find("QualityLabel"):GetComponent(typeof(UILabel))
    local corner = item.transform:Find("Corner").gameObject
    local cornerLabel = item.transform:Find("Corner/Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaInviteNpcMgr.NpcInfo = info
        Gac2Gas.QuerySectInvitingNpcData(info.npcId)
    end)
    local npc = NPC_NPC.GetData(info.npcId)
    nameLabel.text = info.name
    local qualityTextArr = {"",LocalString.GetString("黄"),LocalString.GetString("玄"),LocalString.GetString("地"),LocalString.GetString("天")}
    local qualityColorArr = {"ffffff","FFFE91","519FFF","FF5050","FF88FF"}
    qualityLabel.text = CUICommonDef.TranslateToNGUIText(qualityTextArr[grade])
    qualityLabel.color = NGUIText.ParseColor24( qualityColorArr[grade], 0)
    portrait:LoadNPCPortrait(npc.Portrait)
    locationLabel.text = PublicMap_PublicMap.GetData(info.sceneTemplateId).Name
    cornerLabel.text = info.isJoined and LocalString.GetString("已入驻") or ((info.inviterCount == 1) and LocalString.GetString("已邀请") or LocalString.GetString("已竞争"))
    corner:SetActive(false)
    if info.npcId == LuaInviteNpcMgr.MySectNpcId or info.isJoined then
        corner:SetActive(true)
    end
end

--@region UIEvent
function LuaInviteNpcJoinZongMenWnd:OnClickShowSortbyViewBtn(go)
    local function SelectAction(index)
        self:OnSortNpcItem(index)    
    end

    local selectShareItem=DelegateFactory.Action_int(SelectAction)
    local t={}
    local item=PopupMenuItemData(LocalString.GetString("品级"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("竞争热度"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("身处位置"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Bottom,1,nil,600,true,266)
end

function LuaInviteNpcJoinZongMenWnd:OnInviteBtnClick()
    local isZhangeMen = LuaInviteNpcMgr.IsSectLeader
    if not isZhangeMen then
        g_MessageMgr:ShowMessage("SECTNPC_INVITED_NOT_MENZHU")
        return
    end
    if self.m_SelectNpcId and self.m_NpcIdToData[self.m_SelectNpcId] then
        if self.m_SelectNpcId == LuaInviteNpcMgr.MySectNpcId then
            local msg = g_MessageMgr:FormatMessage("CancelInviteNpcJoinSect_Confirm")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                Gac2Gas.RequestNpcLeaveSect(self.m_SelectNpcId)
            end), nil, nil, nil, false)
            return
        end
        local msg = g_MessageMgr:FormatMessage("InviteNpcJoinSect_Confirm",self.m_NpcIdToData[self.m_SelectNpcId].name)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            Gac2Gas.RequestInviteNpcJoinSect(self.m_SelectNpcId)
        end), nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("SECTNPC_INVITED_NONE")
    end
end

function LuaInviteNpcJoinZongMenWnd:OnDeleteInputViewBtnClick()
    self.InputView.value = ""
    if self.SearchResultView.activeSelf then
        self.SearchResultView:SetActive(false)
        self.NormalView:SetActive(true)    
    end
end

function LuaInviteNpcJoinZongMenWnd:OnSearchBtnClick()
    Extensions.RemoveAllChildren(self.ResultTable.transform)
    if self.TabBar.selectedIndex ~= -1 then
        self.TabBar.tabButtons[self.TabBar.SelectedIndex].Selected = false
    end
    self.TabBar.selectedIndex = -1
    self.SearchResultView:SetActive(true)
    self.NormalView:SetActive(false)
    local content = self.InputView.value
    local isFind = false
    for name,info in pairs(self.m_NameToData) do
        if string.find(name,content) and content and content~="" then
            isFind = true
            local item
            item = NGUITools.AddChild(self.ResultTable.gameObject,self.FreeNpcItem)
            item:SetActive(true)
            self:InitFreeNpcItem(item,0,info)
            UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(go)
                if self.m_LastSelectItem then
                    self.m_LastSelectItem:SetSelected(false,false)
                end
                self.m_SelectNpcId = info.npcId
                item.transform:GetComponent(typeof(QnTableItem)):SetSelected(true,false)
                self.m_LastSelectItem = item.transform:GetComponent(typeof(QnTableItem))
            end)
        end
    end
    if not isFind then
        g_MessageMgr:ShowMessage("InviteNpc_Search_NotFind",content)
    end
    self.ResultTable:Reposition()
end
--@endregion
function LuaInviteNpcJoinZongMenWnd:OnSortNpcItem(index)
    self.m_SortByIndex = index
    local propertyArr = {"grade","inviterCount","sceneTemplateId"}
    local property = propertyArr[index + 1]
    local function compare(a,b)
        if a.npcId == LuaInviteNpcMgr.MySectNpcId then 
            return true 
        elseif b.npcId == LuaInviteNpcMgr.MySectNpcId then 
            return false
        elseif a.isJoined and not b.isJoined then 
            return true 
        elseif b.isJoined and not a.isJoined  then 
            return false
        end
        local aValue = a[property] and a[property] or 0
        local bValue = b[property] and b[property] or 0
        return aValue > bValue
    end
    table.sort(self.m_UnJoinList, compare)
    self.FreeNpcTableView:ReloadData(true,true)
    local textArray = {LocalString.GetString("品级"),LocalString.GetString("竞争热度"),LocalString.GetString("身处位置")}
    self.SortbyLabel.text = textArray[index + 1]
end

function LuaInviteNpcJoinZongMenWnd:OnSendAllSectNpcData()

    local joinedList = LuaInviteNpcMgr.JoinedList
    local unjoinList = LuaInviteNpcMgr.UnJoinedList
    self.m_UnJoinList = {}
    self.m_NameToData = {}
    self.m_NpcIdToData = {}
    -- 已被邀请走的NPC
    for i = 0, joinedList.Count - 1, 3 do
        local t = {}
        t.npcId = joinedList[i]
        t.sectId = joinedList[i+1]   -- 宗门ID
        t.sectName = joinedList[i+2] -- 宗门名
        t.isJoined = true
        local data = SectInviteNpc_NPC.GetData(t.npcId)
        if data then
            t.grade = data.Grade
            t.name = data.Name
            t.sceneTemplateId = data.PosInScene[0]
        end
        self.m_NameToData[t.name] = t
        self.m_NpcIdToData[t.npcId] = t
    end
    -- 还未被邀请走的NPC
    for i = 0, unjoinList.Count - 1, 2 do
        local t = self.m_NpcIdToData[unjoinList[i]] and self.m_NpcIdToData[unjoinList[i]] or {}
        t.npcId = unjoinList[i]
        t.inviterCount = unjoinList[i+1]
        local data = SectInviteNpc_NPC.GetData(t.npcId)
        if data then
            t.grade = data.Grade
            t.name = data.Name
            t.sceneTemplateId = data.PosInScene[0]
            self.m_NameToData[t.name] = t
            self.m_NpcIdToData[t.npcId] = t
        end
    end

    SectInviteNpc_NPC.Foreach(function(k, v)
        if not self.m_NpcIdToData[k] then
            local t = {}
            t.npcId = k
            t.inviterCount = 0
            t.isJoined = false
            t.grade = v.Grade
            t.name = v.Name
            t.sceneTemplateId = v.PosInScene[0]
            self.m_NameToData[t.name] = t
            self.m_NpcIdToData[t.npcId] = t
        end
    end)
    for npcId, t in pairs(self.m_NpcIdToData) do
        if t.grade ~= 1 then
            table.insert(self.m_UnJoinList,t)
        end
    end
    self:OnSortNpcItem(self.m_SortByIndex and self.m_SortByIndex or 0)
end
