local CPropertyBuff = import "L10.Game.CPropertyBuff"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local EPlayerFightProp=import "L10.Game.EPlayerFightProp"


CLuaHouseBuffListItem = class()

RegistClassMember(CLuaHouseBuffListItem,"lastUpdateTime")
RegistClassMember(CLuaHouseBuffListItem,"playerId")
RegistClassMember(CLuaHouseBuffListItem,"OnDescLabelChanged")
RegistClassMember(CLuaHouseBuffListItem,"fatherWnd")

RegistChildComponent(CLuaHouseBuffListItem,"nameLabel","NameLabel",UILabel)
RegistChildComponent(CLuaHouseBuffListItem,"descLabel","DescLabel",UILabel)
RegistChildComponent(CLuaHouseBuffListItem,"subNameLabel","SubNameLabel",UILabel)
RegistChildComponent(CLuaHouseBuffListItem,"subDescLabel","SubDescLabel",UILabel)
RegistChildComponent(CLuaHouseBuffListItem,"descBg","DesBg",UIWidget)
RegistChildComponent(CLuaHouseBuffListItem,"subNameLabel2","SubNameLabel2",UILabel)
RegistChildComponent(CLuaHouseBuffListItem,"subDescLabel2","SubDescLabel2",UILabel)

RegistClassMember(CLuaHouseBuffListItem,"ValueColor")
RegistClassMember(CLuaHouseBuffListItem,"NameColor")

function CLuaHouseBuffListItem:Awake()
    self.lastUpdateTime = 0
    self.playerId = 0
    self.OnDescLabelChanged = nil

    self.NameColor = "FFED5F"
    self.ValueColor = "E8D0AA"
end

function CLuaHouseBuffListItem:OnEnable()
    g_ScriptEvent:AddListener("OnQueryPlayerHousePrayWordsDone",self,"OnQueryOtherPlayerPrayResult")
end
function CLuaHouseBuffListItem:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryPlayerHousePrayWordsDone",self,"OnQueryOtherPlayerPrayResult")
end

function CLuaHouseBuffListItem:Init( playerId, wnd) 
    self.playerId = playerId
    self.fatherWnd = wnd
    self.nameLabel.text = nil
    self.descLabel.text = nil
    self.subNameLabel.text = nil
    self.subDescLabel.text = nil

    self.descBg.height = math.floor(math.abs(self.descLabel.transform.localPosition.y - self.descLabel.localSize.y - 4))
    return self:InitHouseBuff(playerId)
end

function CLuaHouseBuffListItem:OnQueryOtherPlayerPrayResult( playerId, info, buffObj) 
    if self.playerId == playerId and info ~= nil then
        if info.StartTime == System.UInt32.MaxValue then
            if self.fatherWnd ~= nil then
                self.fatherWnd:CloseWnd()
                g_MessageMgr:ShowMessage("PLAYER_OFFLINE")
            end
            return
        end
        local words = {}
        CommonDefs.DictIterate(info.Words, DelegateFactory.Action_object_object(function (___key, ___value) 
            table.insert( words,___key )
        end))

        if #words == 0 or info.BuffId <= 0 then
            if self.fatherWnd ~= nil then
                self.fatherWnd:CloseWnd()
                g_MessageMgr:ShowMessage("HAVE_NO_QIFU_SHOW")
            end
            return
        end

        local workedZhenZhaiFishWords = {}
        CommonDefs.DictIterate(info.WorkedZhengzhaiFishWord, DelegateFactory.Action_object_object(function (___key, ___value) 
            table.insert( workedZhenZhaiFishWords,___key )
        end))

        local buffLevel = info.BuffId % 100
        self.nameLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]%s级家园[-][/c]"), self.NameColor, buffLevel)
        self:InitHousePrayBuff(info.BuffId, info.Score, words,buffObj,workedZhenZhaiFishWords)
    else
        if self.fatherWnd ~= nil then
            self.fatherWnd:CloseWnd()
        end
        return
    end
end
function CLuaHouseBuffListItem:InitHouseBuff( playerId) 
    if CClientMainPlayer.Inst == nil then
        return nil
    end
    if CClientMainPlayer.Inst.Id == playerId then
        local exist = false
        if CClientMainPlayer.Inst.BuffProp == nil then
            if self.fatherWnd ~= nil then
                self.fatherWnd:CloseWnd()
                g_MessageMgr:ShowMessage("HAVE_NO_QIFU_SHOW_SELF")
            end
            return nil
        end

        local words = {}
        CommonDefs.DictIterate(CClientMainPlayer.Inst.PlayProp.HousePrayWords.Words, DelegateFactory.Action_object_object(function (___key, ___value) 
            table.insert( words,___key )
        end))
        local workedZhenZhaiFishWords = {}
        CommonDefs.DictIterate(CClientMainPlayer.Inst.PlayProp.HousePrayWords.WorkedZhengzhaiFishWord, DelegateFactory.Action_object_object(function (___key, ___value) 
            table.insert( workedZhenZhaiFishWords,___key )
        end))

        CommonDefs.DictIterate(CClientMainPlayer.Inst.BuffProp.Buffs, DelegateFactory.Action_object_object(function (___key, ___value) 
            local b = Buff_Buff.GetData(___key)
            if b ~= nil and b.NeedDisplay > 0 and CLuaHouseMgr.IsHouseBuff(b.ID) then
                exist = true
            end
        end))
        if exist then
            if #words == 0 or CClientMainPlayer.Inst.PlayProp.HousePrayWords.BuffId <= 0 then
                if self.fatherWnd ~= nil then
                    self.fatherWnd:CloseWnd()
                    g_MessageMgr:ShowMessage("HAVE_NO_QIFU_SHOW_SELF")
                end
                return nil
            end
            local buffLevel = CClientMainPlayer.Inst.PlayProp.HousePrayWords.BuffId % 100
            self.nameLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]%s级家园[-][/c]"), self.NameColor, buffLevel)
            return self:InitHousePrayBuff(CClientMainPlayer.Inst.PlayProp.HousePrayWords.BuffId, CClientMainPlayer.Inst.PlayProp.HousePrayWords.Score, words,nil,workedZhenZhaiFishWords)
        else
            if self.fatherWnd ~= nil then
                self.fatherWnd:CloseWnd()
                g_MessageMgr:ShowMessage("HAVE_NO_QIFU_SHOW_SELF")
            end
            return nil
        end
    else
        Gac2Gas.QueryPlayerInfo(playerId)
    end
    return nil
end

function CLuaHouseBuffListItem:InitHousePrayBuff( buffId, score, words, buffObj,workedZhenZhaiFishWords) 
    local buff = Buff_Buff.GetData(buffId)
    if buff == nil then
        return nil
    end

    local obj = CClientPlayerMgr.Inst:GetPlayer(self.playerId)
    local engineId = obj ~= nil and obj.EngineId or 0
    if obj then
        self.descLabel.text = CPropertyBuff.GetBuffDisplay(buffId, engineId)
    else
        self.descLabel.text = ""
        if buffObj then
            local display = Buff_Buff.GetData(buffId)
            local text = string.gsub(buff.Display,[=[<EffectId=(%d+),Prop="(%w+)">]=],function(a,b)
                local data = CommonDefs.DictGetValue_LuaCall(buffObj.EffectValues,a)
                return CommonDefs.DictGetValue_LuaCall(data.Values,EnumToInt(EPlayerFightProp[b]))
            end )
            self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(text,false)
        end
    end

    self.descLabel.transform.localPosition = CreateFromClass(Vector3, self.descLabel.transform.localPosition.x, self.nameLabel.transform.localPosition.y - self.nameLabel.height, self.descLabel.transform.localPosition.z)
    self.subNameLabel.text = CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("[c][%s]传家之宝(评分:%s)[-]"), self.NameColor, score), false)
    self.subNameLabel.transform.localPosition = CreateFromClass(Vector3, self.subNameLabel.transform.localPosition.x, self.descLabel.transform.localPosition.y - self.descLabel.height, self.subNameLabel.transform.localPosition.z)

    local t = {}
    local color = CClientChuanJiaBaoMgr.Inst:GetChuanjiaboBuffColor(#words)

    for i=1,#words do
        local wd = Word_Word.GetData(words[i])
        if wd ~= nil then
            table.insert( t, SafeStringFormat3(LocalString.GetString("[c][%s]【%s】%s[-]"), color, wd.Name, wd.Description) )
        end
    end
    local builder = table.concat( t,"\n" )

    self.subDescLabel.text = CChatLinkMgr.TranslateToNGUIText(builder, false)
    self.subDescLabel.transform.localPosition = CreateFromClass(Vector3, self.subDescLabel.transform.localPosition.x, self.subNameLabel.transform.localPosition.y - self.subNameLabel.height, self.subDescLabel.transform.localPosition.z)

    --佑宅福鱼
    self.subNameLabel2.text = CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("[c][%s]佑宅福鱼[-]"), self.NameColor), false)
    self.subNameLabel2.transform.localPosition = CreateFromClass(Vector3, self.subNameLabel2.transform.localPosition.x, self.subDescLabel.transform.localPosition.y - self.subDescLabel.height-30, self.subNameLabel2.transform.localPosition.z)
    if workedZhenZhaiFishWords and #workedZhenZhaiFishWords > 0 then
        local t = {}
        for i=1,#workedZhenZhaiFishWords do
            local wd = Word_Word.GetData(workedZhenZhaiFishWords[i])
            if wd ~= nil then
                table.insert( t, wd.Description )
            end
        end
        local builder = table.concat( t,"\n" )
        self.subDescLabel2.text = CChatLinkMgr.TranslateToNGUIText(builder, false)
    else
        self.subDescLabel2.text = LocalString.GetString("暂无")
    end
    self.subDescLabel2.transform.localPosition = CreateFromClass(Vector3, self.subDescLabel2.transform.localPosition.x, self.subNameLabel2.transform.localPosition.y - self.subNameLabel2.height, self.subDescLabel2.transform.localPosition.z)

    self.descBg.height = math.floor(math.abs(self.nameLabel.height + self.descLabel.height + self.subNameLabel.height + self.subDescLabel.height + 4))

    if self.OnDescLabelChanged ~= nil then
        self.OnDescLabelChanged()
    end

    return color
end

