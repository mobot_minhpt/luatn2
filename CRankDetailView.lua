-- Auto Generated!!
local CGuildLeagueMgr=import "L10.Game.CGuildLeagueMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CJieBaiRankInfoMgr = import "L10.UI.CJieBaiRankInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CRankData = import "L10.UI.CRankData"
local CRankDetailView = import "L10.UI.CRankDetailView"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumClass = import "L10.Game.EnumClass"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
local Rank_JieBai = import "L10.Game.Rank_JieBai"
local Rank_Player = import "L10.Game.Rank_Player"
local Rank_Setting = import "L10.Game.Rank_Setting"
local String = import "System.String"
local StringSplitOptions = import "System.StringSplitOptions"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CRankDetailView.m_OnMainPlayerInfoClick_CS2LuaHook = function (this, go) 
    local jiebai = Rank_JieBai.GetData(CRankData.Inst.MainPlayerRankInfo.rankId)
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.RelationshipProp.JieBaiMembers ~= nil and CJieBaiMgr.Inst:IsInJieBaiRelationship() and jiebai ~= nil and jiebai.MemberCount == CJieBaiMgr.Inst:GetJieBaiMemberCount() then
        CJieBaiRankInfoMgr.jiebaiTitle = CRankData.Inst.MainPlayerRankInfo.jiebaiTitle
        Gac2Gas.QueryRankJieBaiInfo(CRankData.Inst.MainPlayerRankInfo.jiebaiKey)
    end
end
CRankDetailView.m_UpdateMyGuildInfo_CS2LuaHook = function (this) 
    local PlayerRankInfo = CRankData.Inst.MainPlayerRankInfo
    if PlayerRankInfo.rankSheet == EnumRankSheet.ePlayer and CGuildMgr.Inst.m_GuildInfo ~= nil then
        this.rankMainPlayerInfo:InitUnit(3, CGuildMgr.Inst.m_GuildInfo.Name)
    end
end
CRankDetailView.m_UpdateInfo_CS2LuaHook = function (this) 
    if CLuaRankData.m_CurRankId ~= CRankData.Inst.RankId then return end
    if IsOpenNewRankWnd() then
        CLuaRankDetailView:ProcessNoneTip(this)
    end
    --更新主角排行榜信息
    local PlayerRankInfo = CRankData.Inst.MainPlayerRankInfo
    local mainPlayerInfo = CreateFromClass(MakeGenericClass(List, String))
    local showTag = false
    local showJiebaiList = false
    local needHideHeaderNameColumn = System.String.IsNullOrEmpty(CRankData.GetTitle.HeaderName) --是否隐藏最后一列
    if PlayerRankInfo.rankSheet == EnumRankSheet.ePlayer then
        showTag = true
        local rankTempStr = PlayerRankInfo.Rank > 0 and tostring(PlayerRankInfo.Rank) or LocalString.GetString("未上榜")
        for i = 1, EnumToInt(EnumClass.Undefined)-1 do
            local o = CommonDefs.Enum_ToObject_Type_Int32(typeof(EnumClass), i)
            if o ~= nil and Profession.GetFullName(o) == this.tabName and Profession.GetFullName(CClientMainPlayer.Inst.Class) ~= this.tabName then
                rankTempStr = ""
                break
            end
        end
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), rankTempStr)
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), CClientMainPlayer.Inst.Name)

        local isSpecial = CLuaRankDetailView:ProcessSpecialPlayerRankSheet(PlayerRankInfo, mainPlayerInfo) 
        if not isSpecial then
            if not IsOpenNewRankWnd() then
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), Profession.GetFullName(CClientMainPlayer.Inst.Class))
            end
            if not CClientMainPlayer.Inst:IsInGuild() then
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            else
                if CGuildMgr.Inst.m_GuildInfo ~= nil then
                    CommonDefs.ListAdd(mainPlayerInfo, typeof(String), CGuildMgr.Inst.m_GuildInfo.Name)
                else
                    Gac2Gas.RequestGetGuildInfo(CGuildMgr.RPC_TYPE_MyGuildInfo)
                    CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
                end
            end

            if PlayerRankInfo.Value > 0 then
                local rankPlayer = Rank_Player.GetData(PlayerRankInfo.rankId)
                local result = ""
                if rankPlayer.Point == 0 then
                    result = CUICommonDef.ConvertLargeNumber2String(math.floor(PlayerRankInfo.Value))
                elseif rankPlayer.Point == 1 then
                    result = CUICommonDef.ConvertDouble2String(PlayerRankInfo.Value, 1)
                elseif rankPlayer.Point == 2 then
                    result = CUICommonDef.ConvertDouble2String(PlayerRankInfo.Value + 0.05, 1)
                elseif rankPlayer.Point == 3 then
                    result = CUICommonDef.ConvertDouble2String(PlayerRankInfo.Value + 0.005, 2)
                end
                if rankPlayer.Display == 1 then
                    result = System.String.Format("{0}%", result)
                end
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), result)
            else
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            end
        end
    elseif PlayerRankInfo.rankSheet == EnumRankSheet.eGuild then
        if PlayerRankInfo.Guild_Name ~= "" then
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.Rank > 0 and tostring(PlayerRankInfo.Rank) or LocalString.GetString("未上榜"))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.Guild_Name)
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), tostring(PlayerRankInfo.Member))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), tostring(PlayerRankInfo.Level))
            if PlayerRankInfo.rankId == 41100004 then
                local val = PlayerRankInfo.Value
                local levelstring = LuaGuildLeagueMgr.GetLevelStr(math.floor(val/100))
                local str = levelstring..SafeStringFormat3( LocalString.GetString("第%s名"),val%100 )
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), str)
            elseif PlayerRankInfo.rankId == 41100020 then
                local val = PlayerRankInfo.Value
                local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(val)
                local str = System.String.Format("{0:yyyy-MM-dd}", dateTime)
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), str)
            else
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), needHideHeaderNameColumn and "" or CUICommonDef.ConvertLargeNumber2String(math.floor(PlayerRankInfo.Value)))
            end
        else
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
        end
    elseif PlayerRankInfo.rankSheet == EnumRankSheet.eLingShou or PlayerRankInfo.rankSheet == EnumRankSheet.ePlayerShop or PlayerRankInfo.rankSheet == EnumRankSheet.eMapi then
        showTag = true
        local rankTempStr = PlayerRankInfo.Rank > 0 and tostring(PlayerRankInfo.Rank) or LocalString.GetString("未上榜")
        for i = 1, EnumToInt(EnumClass.Undefined)-1 do
            local o = CommonDefs.Enum_ToObject_Type_Int32(typeof(EnumClass), i)
            if o ~= nil and Profession.GetFullName(o) == this.tabName and Profession.GetFullName(CClientMainPlayer.Inst.Class) ~= this.tabName then
                rankTempStr = ""
                break
            end
        end
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), rankTempStr)
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), CClientMainPlayer.Inst.Name)
        if not IsOpenNewRankWnd() then
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), Profession.GetFullName(CClientMainPlayer.Inst.Class))
        end
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.LingshouName)
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), CUICommonDef.ConvertLargeNumber2String(math.floor(PlayerRankInfo.Value)))
    elseif PlayerRankInfo.rankSheet == EnumRankSheet.eHouse then
        showTag = true
        local rankTemStr = PlayerRankInfo.Rank > 0 and tostring(PlayerRankInfo.Rank) or LocalString.GetString("未上榜")
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), rankTemStr)
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.ownerName1)
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.ownerName2)
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.HouseName)
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), CUICommonDef.ConvertLargeNumber2String(math.floor(PlayerRankInfo.Value)))
    elseif PlayerRankInfo.rankSheet == EnumRankSheet.eJieBai then
        showTag = true
        local jiebai = Rank_JieBai.GetData(PlayerRankInfo.rankId)
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.RelationshipProp.JieBaiMembers ~= nil and CJieBaiMgr.Inst:IsInJieBaiRelationship() and jiebai ~= nil and jiebai.MemberCount == CJieBaiMgr.Inst:GetJieBaiMemberCount() then
            local rankTemStr = PlayerRankInfo.Rank > 0 and tostring(PlayerRankInfo.Rank) or LocalString.GetString("未上榜")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), rankTemStr)
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.jiebaiTitle)
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            if PlayerRankInfo.Value > 0 then
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), CUICommonDef.ConvertLargeNumber2String(math.floor(PlayerRankInfo.Value)))
            else
                CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString("—"))
            end
            showJiebaiList = true
        else
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString("—"))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString("—"))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString("—"))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString("—"))
        end
    elseif PlayerRankInfo.rankSheet == EnumRankSheet.eBaby then
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.Rank > 0 and tostring(PlayerRankInfo.Rank) or LocalString.GetString("未上榜"))
        CommonDefs.ListAdd(mainPlayerInfo, typeof(String), CClientMainPlayer.Inst.Name)
        if PlayerRankInfo.data ~= nil and PlayerRankInfo.data.Count == 4 then
            --mainPlayerInfo.Add(PlayerRankInfo.data[0].ToString());
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), ToStringWrap(PlayerRankInfo.data[1]))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), ToStringWrap(PlayerRankInfo.data[2]))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), ToStringWrap(PlayerRankInfo.data[3]))
        else
            --mainPlayerInfo.Add(LocalString.GetString("—"));
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString(""))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString(""))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString(""))
        end
    elseif PlayerRankInfo.rankSheet == EnumRankSheet.eSect then
        if PlayerRankInfo.Guild_Name ~= "" then
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.Rank > 0 and tostring(PlayerRankInfo.Rank) or LocalString.GetString("未上榜"))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), PlayerRankInfo.Guild_Name)
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), tostring(PlayerRankInfo.Member))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), tostring(PlayerRankInfo.Level))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), needHideHeaderNameColumn and "" or CUICommonDef.ConvertLargeNumber2String(math.floor(PlayerRankInfo.Value)))
        else
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), "")
        end
    end
    --更新TableHeader信息
    local widthList = this:InitHeaderInfo(PlayerRankInfo.rankSheet)
    local default
    if showJiebaiList then
        default = PlayerRankInfo.jiebaiClassList
    else
        default = nil
    end
    local setting = Rank_SettingNew.GetData(CRankData.GetRankSheetKey(PlayerRankInfo.rankSheet))
    local job = setting.ShowModel > 0 and EnumToInt(PlayerRankInfo.Job) or 0
    this.rankMainPlayerInfo:Init(mainPlayerInfo, showTag, widthList, default, job, PlayerRankInfo.countryCode)
    --更新排行榜列表信息
    this.rankTableBody:InitListInfo(widthList)
end
CRankDetailView.m_InitHeaderInfo_CS2LuaHook = function (this, sheet) 
    local headerInfo = CreateFromClass(MakeGenericClass(List, String))
    local widthInfo = CreateFromClass(MakeGenericClass(List, Int32))
    local setting
    if IsOpenNewRankWnd() then
        setting = Rank_SettingNew.GetData(CRankData.GetRankSheetKey(sheet))
    else
        setting = Rank_Setting.GetData(CRankData.GetRankSheetKey(sheet))
    end
    if setting ~= nil then
        local infos = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(setting.Value, Table2ArrayWithCount({",", ";"}, 2, MakeArrayClass(String)), StringSplitOptions.RemoveEmptyEntries)
        if infos ~= nil and infos.Length % 2 == 0 then
            local length = math.floor(infos.Length / 2)
            do
                local i = 0
                while i < length * 2 do
                    if i == (length - 3) * 2 then
                        CommonDefs.ListAdd(headerInfo, typeof(String), infos[i])
                    elseif i == (length - 2) * 2 then
                        local headerName2 = CLuaRankDetailView:GetSpecialHeaderName2(CRankData.Inst.MainPlayerRankInfo.rankId)
                        CommonDefs.ListAdd(headerInfo, typeof(String), headerName2 or CRankData.GetTitle.HeaderName2)
                    elseif i == (length - 1) * 2 then
                        CommonDefs.ListAdd(headerInfo, typeof(String), CRankData.GetTitle.HeaderName)
                    else
                        CommonDefs.ListAdd(headerInfo, typeof(String), infos[i])
                    end
                    CommonDefs.ListAdd(widthInfo, typeof(Int32), System.Int32.Parse(infos[i + 1]))
                    i = i + 2
                end
            end
            this.rankTableHeader:Init(headerInfo, widthInfo)
        end
    end
    return widthInfo
end

CLuaRankDetailView = {}

-- 倒数第二列表头名
function CLuaRankDetailView:GetSpecialHeaderName2(id)
    -- 倒卖玩法
    if 41000254 <= id and id <= 41000257 then
        return LocalString.GetString("天数/健康值")
    end

    return nil
end

function CLuaRankDetailView:ProcessSpecialPlayerRankSheet(PlayerRankInfo, mainPlayerInfo)
    -- 倒卖玩法
    if 41000254 <= PlayerRankInfo.rankId and PlayerRankInfo.rankId <= 41000257 then
        local list = MsgPackImpl.unpack(PlayerRankInfo.extraData)
        if list and list.Count == 4 then
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String),
                SafeStringFormat3("%s/%s", ToStringWrap(list[2]), ToStringWrap(list[1])))
            CommonDefs.ListAdd(info, typeof(String), list[0] < 10000000000 and SafeStringFormat3("%.f", list[0]) or
                SafeStringFormat3(LocalString.GetString("%.f亿"), math.floor(list[0] / 100000000)))
        else
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString(""))
            CommonDefs.ListAdd(mainPlayerInfo, typeof(String), LocalString.GetString(""))
        end
        return true
    end

    return false
end

function CLuaRankDetailView:ProcessNoneTip(this)
    local isShowNoneView = CRankData.Inst.RankList.Count == 0

    local noneView = this.transform:Find("RankList/Bg/NoneTip")
    if noneView then
        noneView.gameObject:SetActive(isShowNoneView)
    end

    return isShowNoneView
end
