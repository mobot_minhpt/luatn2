require("common/common_include")

local CUIManager = import "L10.UI.CUIManager"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

EnumXianZhiStatus = {
	DistrictRegion = 1,	-- 区界小神
	CityRegion = 2,		-- 城界众神
	SanJie = 3, 		-- 三界众上
	NotFengXian = 4,	-- 未封仙
}

EnumFengXianStatus = {
	eEnd = 1,				-- 未开启
	eSignUp = 2,			-- 报名
}

EnumXianZhiOperateType = {
	eNone = 0,
	ePromote = 1,
	eWeaken = 2,
	eForbid = 3,
}

CLuaXianzhiMgr = {}

CLuaXianzhiMgr.m_OpenXianZhiWndMark = false
-- 打开玩家的仙职界面
function CLuaXianzhiMgr.OpenXianZhiWnd()
	CLuaXianzhiMgr.m_OpenXianZhiWndMark = true
	Gac2Gas.RequestOpenXianZhiWnd()
end

-- 打开仙职的详细描述
function CLuaXianzhiMgr.OpenDetailXianZhiDescWnd(titleId)
	CUIManager.ShowUI(CLuaUIResources.XianZhiDetailDescWnd)
end

-- 打开仙职类型的详细描述
CLuaXianzhiMgr.m_XianZhiTypeId = 0
function CLuaXianzhiMgr.OpenXianZhiTypeDescWnd(xianZhiTypeId)
	CLuaXianzhiMgr.m_XianZhiTypeId = xianZhiTypeId
	CUIManager.ShowUI(CLuaUIResources.XianZhiTypeDescWnd)
end

-- 获得仙职的位置
function CLuaXianzhiMgr.GetXianZhiLocation()
	if not CClientMainPlayer.Inst then return end
	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData

	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	if status == EnumXianZhiStatus.SanJie then
		return LocalString.GetString("三界总")
	elseif status == EnumXianZhiStatus.CityRegion then
		local regionId = xianZhiData.CityRegionId
		local region = XianZhi_Region.GetData(regionId)
		return region.Region
	elseif status == EnumXianZhiStatus.DistrictRegion then
		local districtRegionId = xianZhiData.DistrictRegionId
		local districtRegion = XianZhi_Region.GetData(districtRegionId)
		local cityRegionId = districtRegion.BelongRegionId
		local cityRegion = XianZhi_Region.GetData(cityRegionId)

		return SafeStringFormat3("%s%s", cityRegion.Region, districtRegion.Region)
	end
	return ""
end

function CLuaXianzhiMgr.GetXianZhiPositionByRegion(regionId)
	local region = XianZhi_Region.GetData(regionId)
	if not region or region.Stage == 3 then
		return LocalString.GetString("三界总")
	elseif region.Stage == 2 then
		return region.Region
	elseif region.Stage == 1 then
		local belongRegion = XianZhi_Region.GetData(region.BelongRegionId)
		return SafeStringFormat3("%s%s", belongRegion.Region, region.Region)
	end
	return ""
end

function CLuaXianzhiMgr.GetXianZhiNameByRegionAndTitle(regionId, titleId)
	local xianZhiPos = CLuaXianzhiMgr.GetXianZhiPositionByRegion(regionId)
	local title = XianZhi_Title.GetData(titleId)
	return SafeStringFormat3("%s%s", xianZhiPos, title.TitleName)
end

-- 获得仙职的具体名称（雨神、风神）
function CLuaXianzhiMgr.GetXianZhiTitleName()
	if not CClientMainPlayer.Inst then return EnumXianZhiStatus.NotFengXian end

	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData

	local titleId = 0
	if xianZhiData.SanJieTitleId ~= 0 then
		titleId = xianZhiData.SanJieTitleId
	elseif xianZhiData.CityTitleId ~= 0 then
		titleId = xianZhiData.CityTitleId
	elseif xianZhiData.DistrictTitleId ~= 0 then
		titleId = xianZhiData.DistrictTitleId
	end

	local title = XianZhi_Title.GetData(titleId)
	if not title then
		return title.TitleName
	end
end

function CLuaXianzhiMgr.GetXianZhiLocationByRegID()
	local regionid = CLuaXianzhiMgr.XzRegionID
	local regiondata = XianZhi_Region.GetData(regionid)
	if regiondata ~= nil then
		local stage = regiondata.Stage
		if stage == 3 then 
			return LocalString.GetString("三界总")
		elseif stage == 2 then
			return regiondata.Region
		elseif stage == 1 then
			local str = regiondata.Region
			local subid = regiondata.BelongRegionId
			local subdata = XianZhi_Region.GetData(subid)
			if subdata ~= nil then
				return subdata.Region..str
			end
		end
	end
	return ""
end


-- 获得仙职名称(<=16)
function CLuaXianzhiMgr.GetXianZhiFullName()
	if not CClientMainPlayer.Inst then return end

	-- 三界总 + title名
	-- 齐齐哈尔市 + title名
	-- 齐齐哈尔市 富拉尔基区 + 送子娘娘

	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData
	if status == EnumXianZhiStatus.SanJie then
		local titleId = xianZhiData.SanJieTitleId
		local title = XianZhi_Title.GetData(titleId)
		return SafeStringFormat3(LocalString.GetString("三界总%s"), title.TitleName)
	elseif status == EnumXianZhiStatus.CityRegion then
		local regionId = xianZhiData.CityRegionId
		local titleId = xianZhiData.CityTitleId

		local region = XianZhi_Region.GetData(regionId)
		local title = XianZhi_Title.GetData(titleId)
		return SafeStringFormat3("%s%s", region.Region, title.TitleName)

	elseif status == EnumXianZhiStatus.DistrictRegion then
		
		local districtRegionId = xianZhiData.DistrictRegionId
		local districtTitleId = xianZhiData.DistrictTitleId

		local districtRegion = XianZhi_Region.GetData(districtRegionId)
		local districtTitle = XianZhi_Title.GetData(districtTitleId)
		local cityRegionId = districtRegion.BelongRegionId
		local cityRegion = XianZhi_Region.GetData(cityRegionId)

		return SafeStringFormat3("%s%s%s", cityRegion.Region, districtRegion.Region, districtTitle.TitleName)
	end
	return LocalString.GetString("默认仙职名称")
end


-- 获得仙职的阶段
function CLuaXianzhiMgr.GetXianZhiStatus()
	if not CClientMainPlayer.Inst then return EnumXianZhiStatus.NotFengXian end

	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData
	if not xianZhiData or xianZhiData.FengXianFinished ~= 1 then return EnumXianZhiStatus.NotFengXian end

	if xianZhiData.SanJieTitleId ~= 0 then
		return EnumXianZhiStatus.SanJie
	end

	if xianZhiData.CityTitleId ~= 0 then
		return EnumXianZhiStatus.CityRegion
	end

	if xianZhiData.DistrictTitleId ~= 0 then
		return EnumXianZhiStatus.DistrictRegion
	end

	return EnumXianZhiStatus.NotFengXian
end

function CLuaXianzhiMgr.GetXianZhiStatusByRegionId(regionId)
	local region = XianZhi_Region.GetData(regionId)
	if not region or region.Stage == 3 then
		return EnumXianZhiStatus.SanJie
	elseif region.Stage == 2 then
		return EnumXianZhiStatus.CityRegion
	elseif region.Stage == 1 then
		return EnumXianZhiStatus.DistrictRegion
	end
	return EnumXianZhiStatus.NotFengXian
end

-- 获得玩家仙职的TitleId（需要逐次检查三个TitleId）
function CLuaXianzhiMgr.GetValidTitleId()
	if CClientMainPlayer.Inst == nil then return 0 end
	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData
	if not xianZhiData or xianZhiData.FengXianFinished ~= 1 then return 0 end

	if xianZhiData.SanJieTitleId ~= 0 then
		return xianZhiData.SanJieTitleId
	end

	if xianZhiData.CityTitleId ~= 0 then
		return xianZhiData.CityTitleId
	end

	if xianZhiData.DistrictTitleId ~= 0 then
		return xianZhiData.DistrictTitleId
	end

	return 0
end

-- 获得仙职技能
function CLuaXianzhiMgr.GetXianZhiSKillId()
	if CClientMainPlayer.Inst == nil then return 0 end
	--return 96000501
	local xianzhiTitleId = CLuaXianzhiMgr.GetValidTitleId()
	local title = XianZhi_Title.GetData(xianzhiTitleId)
	if title then

		local skillCls = title.SkillCls
		return skillCls * 100 + CClientMainPlayer.Inst.PlayProp.XianZhiData.XianZhiLevel
	end
	return 0
end

-- 获得仙职技能（经过操作）
function CLuaXianzhiMgr.GetXianZhiSkillByDelta()
	if CClientMainPlayer.Inst == nil then return 0 end

	local xianzhiTitleId = CLuaXianzhiMgr.GetValidTitleId()
	local title = XianZhi_Title.GetData(xianzhiTitleId)
	local status =  CLuaXianzhiMgr.GetXianZhiStatus()
	if title then
		local setting = XianZhi_Setting.GetData()
		local skillCls = title.SkillCls
		local skillLevel = CClientMainPlayer.Inst.PlayProp.XianZhiData.XianZhiLevel

		local operateType = CClientMainPlayer.Inst.PlayProp.XianZhiData.OperateType
		if operateType == EnumXianZhiOperateType.eNone then
			return skillCls * 100 + skillLevel

		elseif operateType == EnumXianZhiOperateType.ePromote then
			local maxLevel = CLuaXianzhiMgr.GetXianZhiSkillMaxLevel(status)
			if skillLevel < maxLevel then
				return skillCls * 100 + skillLevel + setting.SkillLevelDelta
			else
				return skillCls * 100 + skillLevel
			end
			
		elseif operateType == EnumXianZhiOperateType.eWeaken then
			if skillLevel == 1 then
				return skillCls * 100 + skillLevel
			else
				return skillCls * 100 + skillLevel - setting.SkillLevelDelta
			end
		else
			return 0
		end
	end
	return 0
end

function CLuaXianzhiMgr.GetXianZhiSkillMaxLevel(status)
	if status == EnumXianZhiStatus.NotFengXian then
		return 0
	else
		local setting = XianZhi_Setting.GetData()
        local maxLevel = setting.LevelUpperLimit[status-1]
        return maxLevel
	end
end

function CLuaXianzhiMgr.GetXianZhiLifeSkillCastTime(castTime, lifeSkillType)
	if CClientMainPlayer.Inst == nil then return castTime end

	if lifeSkillType ~= EnumClientLifeSkillType.eFishing and lifeSkillType ~= EnumClientLifeSkillType.eGathering then return castTime end

	local xianzhiTitleId = CLuaXianzhiMgr.GetValidTitleId()
	if xianzhiTitleId == 0 then return castTime end

	local title = XianZhi_Title.GetData(xianzhiTitleId)
	if not title then return castTime end

	local key
	if lifeSkillType == EnumClientLifeSkillType.eFishing then
		key = "FishCasting"
	elseif lifeSkillType == EnumClientLifeSkillType.eGathering then
		key = "GatherCasting"
	end

	local specialSkillTypeId = tonumber(title.SpecialSkillType)
	local specialSkillTypeData = specialSkillTypeId and XianZhi_SpecialSkillType.GetData(specialSkillTypeId)
	if not specialSkillTypeId or not specialSkillTypeData or key ~= specialSkillTypeData.Key then
		return castTime
	end

	if not title.SpecialSkillParam or title.SpecialSkillParam == "" then
		return castTime
	end

	local status, func = pcall(loadstring("local max,min,floor,ceil,random = math.max,math.min,math.floor,math.ceil,math.random;return function(lv, val) " .. title.SpecialSkillParam .. " end"))
	if not status or type(func) ~= "function" then return castTime end

	local skillLv = CClientMainPlayer.Inst.PlayProp.XianZhiData.XianZhiLevel
	return func(skillLv, castTime)
end

CLuaXianzhiMgr.m_IsOpenXianZhiExtend = false
-- 打开仙职晋升(返回时间可能较长)
function CLuaXianzhiMgr.OpenXianZhiPromotion()
	CLuaXianzhiMgr.m_IsOpenXianZhiExtend = false
	CUIManager.ShowUI(CLuaUIResources.XianZhiPromotionWnd)
end

-- 打开任期延长
function CLuaXianzhiMgr.OpenXianZhiExtend()
	CLuaXianzhiMgr.m_IsOpenXianZhiExtend = true
	CUIManager.ShowUI(CLuaUIResources.XianZhiExtendWnd)
	--CUIManager.ShowUI(CLuaUIResources.XianZhiPromotionWnd)
end

-- 更新任期剩余时间
CLuaXianzhiMgr.m_RemainXianZhiTime = 0
function CLuaXianzhiMgr.UpdateRemainXianZhiTime(time)
	CLuaXianzhiMgr.m_RemainXianZhiTime = time
	g_ScriptEvent:BroadcastInLua("UpdateRemainXianZhiTime")
end

--------------------- 封仙大会相关 ---------------------

function CLuaXianzhiMgr.InFengXian()
	if not CClientMainPlayer.Inst then return false end
	local setting = XianZhi_Setting.GetData()
	return CClientMainPlayer.Inst.PlayProp.PlayId == setting.GamePlayId
end

-- openwnd 处理
function CLuaXianzhiMgr.FengXianDaHuiConform()
	local msg = g_MessageMgr:FormatMessage("Feng_Xian_Join_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
		-- 寻路
		CTrackMgr.Inst:FindNPC(20003287, 16000017, 129, 88, nil, nil)
	end), nil, nil, nil, false)
end

-- 封仙大会报名
CLuaXianzhiMgr.m_CurPlayIndex = 0	-- 0 一场都未开始
CLuaXianzhiMgr.m_SignUpPlayIndex = 0 -- 0 未报名
function CLuaXianzhiMgr.OpenFengXianSignUpWnd(curPlayIndex, signUpPlayIndex)
	CLuaXianzhiMgr.m_CurPlayIndex = curPlayIndex
	CLuaXianzhiMgr.m_SignUpPlayIndex = signUpPlayIndex
	if CUIManager.IsLoaded(CLuaUIResources.FengXianSignUpWnd) then
		g_ScriptEvent:BroadcastInLua("FengXianSignUpUpdate")
	else
		CUIManager.ShowUI(CLuaUIResources.FengXianSignUpWnd)
	end
end

-- 请求打开邀请界面
function CLuaXianzhiMgr.OpenFengXianInviteWnd()
	Gac2Gas.QueryInvitedPlayerIds()
end

-- 更新封仙大会邀请界面
CLuaXianzhiMgr.m_FengXianInvitedIds = {}
function CLuaXianzhiMgr.UpdateFengXianInvited(playerIds)
	CLuaXianzhiMgr.m_FengXianInvitedIds = playerIds
	if CUIManager.IsLoaded(CLuaUIResources.FengXianInviteWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateFengXianInvited")
	else
		CUIManager.ShowUI(CLuaUIResources.FengXianInviteWnd)
	end
end

-- 邀请函界面(处理link相关信息)
CLuaXianzhiMgr.m_InvitationCardPlayerId = nil
CLuaXianzhiMgr.m_InvitationCardPlayId = nil
CLuaXianzhiMgr.m_InvitationAppearance = nil
function CLuaXianzhiMgr.OpenFengXianInvitationCard(playerId, playId)
	CLuaXianzhiMgr.m_InvitationCardPlayerId = playerId
	CLuaXianzhiMgr.m_InvitationCardPlayId = playId
	Gac2Gas.QueryFengXianPlayerAppearance(playerId)
end

-- 获得外观界面后打开邀请函
function CLuaXianzhiMgr.OnGetFengXianInvitationAppearance(inviterId, appearance)
	CLuaXianzhiMgr.m_InvitationCardPlayerId = inviterId
	CLuaXianzhiMgr.m_InvitationAppearance = appearance
	CUIManager.ShowUI(CLuaUIResources.FengXianInvitationCardWnd)
end


-- 听封界面
CLuaXianzhiMgr.m_TingFengPlayerName = nil
CLuaXianzhiMgr.m_TingFengXianZhiName = nil
CLuaXianzhiMgr.m_TingFengSkillName = nil
function CLuaXianzhiMgr.OpenTingFengWnd(playerName, xianzhiName, skillName)
	CLuaXianzhiMgr.m_TingFengPlayerName = playerName
	CLuaXianzhiMgr.m_TingFengXianZhiName = xianzhiName
	CLuaXianzhiMgr.m_TingFengSkillName = skillName
	if CUIManager.IsLoaded(CLuaUIResources.FengXianTingFengWnd) then
		CUIManager.CloseUI(CLuaUIResources.FengXianTingFengWnd)
	end
	CUIManager.ShowUI(CLuaUIResources.FengXianTingFengWnd)
end

function CLuaXianzhiMgr.UpdateFengXianTaskView()
	g_ScriptEvent:BroadcastInLua("FengXianStatusChanged")
end

--------------------- end 封仙大会相关 ---------------------


--------------------- 仙职谱相关 ---------------------

function CLuaXianzhiMgr.ParseXianZhiPuInfo(infoTblUD)
	local list = MsgPackImpl.unpack(infoTblUD)
	local infos = {}
	for i = 0, list.Count - 1, 12 do
		local regionId = list[i+0]
		local titleId = list[i+1]
		local playerId = list[i+2]
		local playerName = list[i+3]
		local playerCls = list[i+4]
		local playerGender = list[i+5]
		local skillLevel = list[i+6]
		local operateType = list[i+7]
		local operatePlayerName = list[i+8]
		local operateExpiredTime = list[i+9]
		local operatePlayerStage = list[i+10]
		local serverGroupId = list[i+11]

		table.insert(infos, {
			regionId = regionId,
			titleId = titleId,
			playerId = playerId,
			playerName = playerName,
			playerCls = playerCls,
			playerGender = playerGender,
			skillLevel = skillLevel,
			operateType = operateType,
			operatePlayerName = operatePlayerName,
			operateExpiredTime = operateExpiredTime,
			operatePlayerStage = operatePlayerStage,
			serverGroupId = serverGroupId,
		})
	end

	-- 此处进行排序
	table.sort(infos, function (a, b)
		if a.playerId == CClientMainPlayer.Inst.Id then
			return true
		elseif b.playerId == CClientMainPlayer.Inst.Id then
			return false
		else
			return a.regionId < b.regionId
		end
	end)
	return infos
end

CLuaXianzhiMgr.m_SanJiePlayerInfos = nil
CLuaXianzhiMgr.m_CityPlayerInfos = nil
CLuaXianzhiMgr.m_DistrictPlayerInfos = nil
CLuaXianzhiMgr.m_OperationTimes = 0

function CLuaXianzhiMgr.OpenXianZhiPuWnd(sanjieInfos, cityInfos, districtInfos, operateTimes)
	CLuaXianzhiMgr.m_SanJiePlayerInfos = sanjieInfos
	CLuaXianzhiMgr.m_CityPlayerInfos = cityInfos
	CLuaXianzhiMgr.m_DistrictPlayerInfos = districtInfos
	CLuaXianzhiMgr.m_OperationTimes = operateTimes
	if CUIManager.IsLoaded(CLuaUIResources.XianZhiPuWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateXianZhiPu")
	else
		CUIManager.ShowUI(CLuaUIResources.XianZhiPuWnd)
	end
	
end

-- 玩家playId是否可以被管理仙职
function CLuaXianzhiMgr.CanBeXianZhiManaged(playerId)
	local playerInfo = CLuaXianzhiMgr.GetPlayerToManage(playerId)
	return playerInfo ~= nil
end

-- 仙职技能给管理
CLuaXianzhiMgr.m_XianZhiManagePlayer = nil
function CLuaXianzhiMgr.OpenPlayerXianZhiManage(playerId)
	local playerInfo = CLuaXianzhiMgr.GetPlayerToManage(playerId)
	if playerInfo then
		CLuaXianzhiMgr.m_XianZhiManagePlayer = playerInfo
		CUIManager.ShowUI(CLuaUIResources.XianZhiManageWnd)
	end
end

function CLuaXianzhiMgr.GetPlayerToManage(playerId)
	local status = CLuaXianzhiMgr.GetXianZhiStatus()

	if status == EnumXianZhiStatus.SanJie then
		if CLuaXianzhiMgr.m_CityPlayerInfos then
			for i = 1, #CLuaXianzhiMgr.m_CityPlayerInfos do
				if CLuaXianzhiMgr.m_CityPlayerInfos[i].playerId == playerId then
					return CLuaXianzhiMgr.m_CityPlayerInfos[i]
				end
			end
		end

		if CLuaXianzhiMgr.m_DistrictPlayerInfos then
			for i = 1, #CLuaXianzhiMgr.m_DistrictPlayerInfos do
				if CLuaXianzhiMgr.m_DistrictPlayerInfos[i].playerId == playerId then
					return CLuaXianzhiMgr.m_DistrictPlayerInfos[i]
				end
			end
		end
		return nil
	elseif status == EnumXianZhiStatus.CityRegion then
		if CLuaXianzhiMgr.m_DistrictPlayerInfos then
			for i = 1, #CLuaXianzhiMgr.m_DistrictPlayerInfos do
				if CLuaXianzhiMgr.m_DistrictPlayerInfos[i].playerId == playerId then
					return CLuaXianzhiMgr.m_DistrictPlayerInfos[i]
				end
			end
		end
		return nil
	end
	return nil
end

function CLuaXianzhiMgr.OpenShenZhiWnd(targetId, operationType)
	CLuaXianzhiMgr.m_OperateTargetId = targetId
	CLuaXianzhiMgr.m_OperateType = operationType
	CUIManager.ShowUI(CLuaUIResources.XianZhiShenZhiWnd)
end

CLuaXianzhiMgr.m_OperateTargetId = nil
CLuaXianzhiMgr.m_OperateType = 0
function CLuaXianzhiMgr.RequestOperateXianZhi()
	Gac2Gas.RequestOperateXianZhi(CLuaXianzhiMgr.m_OperateTargetId, CLuaXianzhiMgr.m_OperateType)
end


--------------------- end 仙职谱相关 ---------------------

function CLuaXianzhiMgr.OpenXianzhiChooseImageTaskWnd(taskid)
	CLuaXianzhiMgr.Taskid = taskid
	CUIManager.ShowUI(CLuaUIResources.XianzhiChooseImageTaskWnd)
end

-- 仙职选择画任务,index取值[1,4]
-- to server
function CLuaXianzhiMgr.FinishXianZhiPictureTask(index)
	Gac2Gas.FinishXianZhiPictureTask(CLuaXianzhiMgr.Taskid, index)
end


--------------------- Gac2Gas ---------------------

function Gas2Gac.SendXianZhiPossibleInfo(regionId, possibleInfoU)
	local xianzhiIdTbl = {}
	local list = MsgPackImpl.unpack(possibleInfoU)
	for i = 0, list.Count-1 do
		table.insert(xianzhiIdTbl, list[i])
	end
	CLuaXianzhiMgr.Xzs = xianzhiIdTbl
	CLuaXianzhiMgr.XzRegionID = regionId
	CUIManager.ShowUI(CLuaUIResources.XianzhiTaskCompletedWnd)
end


-- curPlayIndex代表当前已经举行到第几场了，比如0的话说明第一场都还没开始，1的话说明第一场已经开始或结束了，第二场还没开始
-- 如果signUpPlayIndex为0说明还未报名，报名成功和取消报名也都会发这条rpc下来，用于刷新一下界面
function Gas2Gac.QueryXianZhiSignUpDone(curPlayIndex, signUpPlayIndex)
	CLuaXianzhiMgr.OpenFengXianSignUpWnd(curPlayIndex, signUpPlayIndex)
end

function Gas2Gac.SyncPlayerXianZhiData(xianZhiData)
	if not CClientMainPlayer.Inst then
		return
	end
	
	CClientMainPlayer.Inst.PlayProp.XianZhiData:LoadFromString(xianZhiData)
end

function Gas2Gac.PlayerFengXianSuccess(xianzhiSkillId)
	CLuaGuideMgr.TryXianZhiSkillGuide()
end

-- 升级仙职技能成功的回调
function Gas2Gac.LevelUpXianZhiSuccess()
	g_ScriptEvent:BroadcastInLua("LevelUpXianZhiSuccess")
end

-- 封仙大会已邀请玩家id回调
function Gas2Gac.QueryInvitedPlayerIdsDone(playerIdTblU)
	local list = MsgPackImpl.unpack(playerIdTblU)
	local playerIds = {}
	for i = 0, list.Count-1 do
		local id = list[i]
		table.insert(playerIds, id)
	end
	CLuaXianzhiMgr.UpdateFengXianInvited(playerIds)
end

function Gas2Gac.InviteWatchFengXianSuccess()
	Gac2Gas.QueryInvitedPlayerIds()
end

function Gas2Gac.QueryFengXianPlayerAppearanceDone(inviterId, appearanceData)
	local list = MsgPackImpl.unpack(appearanceData)
	local appearance = CreateFromClass(CPropertyAppearance)
	appearance:LoadFromString(list[0], EnumClass.Undefined, EnumGender.Undefined)
	CLuaXianzhiMgr.OnGetFengXianInvitationAppearance(inviterId, appearance)
end

function Gas2Gac.SendXianZhiPuInfo(sanJiePlayerInfoTblUD, cityPlayerInfoTblUD, districtPlayerInfoTblUD, operateTimes)
	local sanJieInfos = CLuaXianzhiMgr.ParseXianZhiPuInfo(sanJiePlayerInfoTblUD)
	local cityInfos = CLuaXianzhiMgr.ParseXianZhiPuInfo(cityPlayerInfoTblUD)
	local districtInfos = CLuaXianzhiMgr.ParseXianZhiPuInfo(districtPlayerInfoTblUD)
	
	CLuaXianzhiMgr.OpenXianZhiPuWnd(sanJieInfos, cityInfos, districtInfos, operateTimes)
end

function Gas2Gac.QueryRemainXianZhiTimeDone(expiredTime)
	if CLuaXianzhiMgr.m_OpenXianZhiWndMark and not CUIManager.IsLoaded(CLuaUIResources.XianZhiWnd) then
		if expiredTime == 0 then
			g_MessageMgr:ShowMessage("XIANZHI_OPEN_WND_NO_XIANZHI_OR_EXPIRED")
		else
			CUIManager.ShowUI(CLuaUIResources.XianZhiWnd)
		end
	end
	CLuaXianzhiMgr.m_OpenXianZhiWndMark = false
	CLuaXianzhiMgr.UpdateRemainXianZhiTime(expiredTime)
end

-- 延期成功的回调
function Gas2Gac.ExtendXianZhiSuccess(expiredTime)
	CLuaXianzhiMgr.UpdateRemainXianZhiTime(expiredTime)
	CUIManager.CloseUI(CLuaUIResources.XianZhiExtendWnd)
end

-- 晋升成功的回答
function Gas2Gac.AllocateXianZhiSuccess()
	g_ScriptEvent:BroadcastInLua("AllocateXianZhiSuccess")
end