local QnAdvanceGridView      = import "L10.UI.QnAdvanceGridView"
local QnAdvanceTableView     = import "L10.UI.QnAdvanceTableView"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CButton                = import "L10.UI.CButton"
local PopupMenuItemData      = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local AlignType              = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr      = import "L10.UI.CPopupMenuInfoMgr"
local EnumPlayerInfoContext  = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel             = import "L10.Game.EChatPanel"
local CPlayerInfoMgr         = import "CPlayerInfoMgr"
local PlayerAlignType        = import "CPlayerInfoMgr+AlignType"
local Ease                   = import "DG.Tweening.Ease"
local Item_Item              = import "L10.Game.Item_Item"
local CItemInfoMgr           = import "L10.UI.CItemInfoMgr"

LuaArenaPassportWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaPassportWnd, "tipButton", "TipButton", GameObject)
RegistChildComponent(LuaArenaPassportWnd, "rankButton", "Button", CButton)
RegistChildComponent(LuaArenaPassportWnd, "rankArrow", "Arrow", UISprite)
RegistChildComponent(LuaArenaPassportWnd, "myRank_Rank", "Rank", UILabel)
RegistChildComponent(LuaArenaPassportWnd, "myRank_Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaArenaPassportWnd, "myRank_Name", "Name", UILabel)
RegistChildComponent(LuaArenaPassportWnd, "myRank_Level", "Level", UILabel)
RegistChildComponent(LuaArenaPassportWnd, "reward_ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaArenaPassportWnd, "returnButton", "ReturnButton", UITexture)
RegistChildComponent(LuaArenaPassportWnd, "milestone", "Milestone", GameObject)
RegistChildComponent(LuaArenaPassportWnd, "milestone_ReturnAward", "Award", CQnReturnAwardTemplate)
RegistChildComponent(LuaArenaPassportWnd, "opened", "Opened", GameObject)
RegistChildComponent(LuaArenaPassportWnd, "openButton", "OpenButton", GameObject)
RegistChildComponent(LuaArenaPassportWnd, "passport_Slider", "Slider", UISlider)
RegistChildComponent(LuaArenaPassportWnd, "oneClickReceiveButton", "OneClickReceiveButton", CButton)
RegistChildComponent(LuaArenaPassportWnd, "buyButton", "BuyButton", GameObject)
RegistChildComponent(LuaArenaPassportWnd, "giveButton", "GiveButton", GameObject)
RegistChildComponent(LuaArenaPassportWnd, "passport_Level", "PassportLevel", UILabel)
RegistChildComponent(LuaArenaPassportWnd, "milestone_Level", "MilestoneLevel", UILabel)
RegistChildComponent(LuaArenaPassportWnd, "passport_Exp", "Exp", UILabel)
RegistChildComponent(LuaArenaPassportWnd, "passport_Star", "Star", UITexture)
RegistChildComponent(LuaArenaPassportWnd, "awardShowParent", "AwardShow", Transform)
RegistChildComponent(LuaArenaPassportWnd, "rankTableView", "RankTableView", QnAdvanceGridView)
RegistChildComponent(LuaArenaPassportWnd, "reward_TableView", "RewardTableView", QnAdvanceTableView)
RegistChildComponent(LuaArenaPassportWnd, "milestone_Bind", "Bind", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaArenaPassportWnd, "oneClickReceiveButtonFx")

RegistClassMember(LuaArenaPassportWnd, "rankArray")
RegistClassMember(LuaArenaPassportWnd, "rankType")
RegistClassMember(LuaArenaPassportWnd, "rankTypeTbl")
RegistClassMember(LuaArenaPassportWnd, "rankSpriteTbl")
RegistClassMember(LuaArenaPassportWnd, "rankList")

RegistClassMember(LuaArenaPassportWnd, "isAllRewardShowed")
RegistClassMember(LuaArenaPassportWnd, "rewardChildCount")
RegistClassMember(LuaArenaPassportWnd, "curLevel")
RegistClassMember(LuaArenaPassportWnd, "rewardedLevel")
RegistClassMember(LuaArenaPassportWnd, "currentMilestoneLevel")

function LuaArenaPassportWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	-- UIEventListener.Get(self.rankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	--     self:OnRankButtonClick()
	-- end)

	UIEventListener.Get(self.returnButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReturnButtonClick()
	end)

	UIEventListener.Get(self.openButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenButtonClick()
	end)

	UIEventListener.Get(self.oneClickReceiveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOneClickReceiveButtonClick()
	end)

	UIEventListener.Get(self.buyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyButtonClick()
	end)

	UIEventListener.Get(self.giveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiveButtonClick()
	end)

    --@endregion EventBind end
    self.reward_TableView.m_Panel.onClipMove = LuaUtils.OnClippingMoved(function(panel)
        self:OnRewardPanelClipMove()
    end)
    self.reward_ItemTemplate:SetActive(false)
    self.rankButton.gameObject:SetActive(false)
    self.oneClickReceiveButtonFx = self.oneClickReceiveButton.transform:Find("Fx").gameObject
end

function LuaArenaPassportWnd:OnEnable()
    g_ScriptEvent:AddListener("SendArenaPassportInfo", self, "OnSendArenaPassportInfo")
    g_ScriptEvent:AddListener("SendArenaPassportRankInfo", self, "OnSendArenaPassportRankInfo")
end

function LuaArenaPassportWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendArenaPassportInfo", self, "OnSendArenaPassportInfo")
    g_ScriptEvent:RemoveListener("SendArenaPassportRankInfo", self, "OnSendArenaPassportRankInfo")
end

function LuaArenaPassportWnd:OnSendArenaPassportInfo(curExp, curLevel, rewardedLevel, weekExp, weekExpLimit)
    self.myRank_Level.text = SafeStringFormat3(LocalString.GetString("%d级"), curLevel)

    self.opened:SetActive(true)
    -- self.openButton:SetActive(curLevel == 0)
    local maxLevel = Arena_PassportLevelReward.GetDataCount()
    self.passport_Level.text = curLevel
    local nextLevel = curLevel < maxLevel and curLevel + 1 or maxLevel
    local needExp = Arena_PassportLevelReward.GetData(nextLevel).NeedExp
    self.passport_Exp.text = SafeStringFormat3("%d/%d", curExp, needExp)
    self.passport_Slider.value = math.min(curExp / needExp, 1)
    self.passport_Star:ResetAndUpdateAnchors()

    self.curLevel = curLevel
    self.rewardedLevel = rewardedLevel
    self:UpdateReward()
    self:UpdateOneClickReceiveButton()
    -- self:UpdateAwardShow()
end

function LuaArenaPassportWnd:OnSendArenaPassportRankInfo(myRank, rankType, rankData)
    self:UpdateRank(myRank, rankType, rankData)
end

function LuaArenaPassportWnd:UpdateRank(myRank, rankType, rankData)
    self.myRank_Rank.text = myRank > 0 and myRank or LocalString.GetString("未上榜")

    self.rankType = rankType
    -- self.rankButton.Text = self.rankTypeTbl[rankType]

    self.rankList = g_MessagePack.unpack(rankData)
    self.rankTableView:ReloadData(true, false)
end

function LuaArenaPassportWnd:Init()
    self.myRank_Portrait:LoadPortrait(CClientMainPlayer.Inst and CClientMainPlayer.Inst.PortraitName, false)
    self.myRank_Name.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    self.myRank_Level.text = ""
    self.myRank_Rank.text = ""

    -- 排行榜类型，目前只有本服，所以先屏蔽掉这段逻辑
    -- self.rankTypeTbl = {LocalString.GetString("本服"), LocalString.GetString("好友")}
    self.rankType = 1
    -- self.rankButton.Text = self.rankTypeTbl[1]
    self.rankSpriteTbl = {"headinfownd_jiashang_01", "headinfownd_jiashang_02", "headinfownd_jiashang_03"}
    self:InitRankTableView()

    self.opened:SetActive(false)
    self.openButton:SetActive(false)

    self:InitAwardShow()
    self.returnButton.gameObject:SetActive(false)
    self.milestone:SetActive(false)
    self:InitRewardTableView()
    self.currentMilestoneLevel = 0

    Gac2Gas.QueryArenaPassportInfo()
    Gac2Gas.QueryArenaPassportRankInfo(1)
end

function LuaArenaPassportWnd:InitRankTableView()
    self.rankList = {}
    self.rankTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankList
        end,
        function(item, index)
            self:InitRankItem(item, index)
        end
    )

    self.rankTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        CPlayerInfoMgr.ShowPlayerPopupMenu(self.rankList[row + 1].playerId, EnumPlayerInfoContext.Arena, EChatPanel.Undefined, "", nil, Vector3.zero, PlayerAlignType.Default)
    end)
end

function LuaArenaPassportWnd:InitRankItem(item, index)
    local rank = index + 1
    local data = self.rankList[rank]
    local extra = data.extra and g_MessagePack.unpack_string(data.extra) or {}
    local gender = extra and extra.gender or 0
    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    portrait:LoadPortrait(CUICommonDef.GetPortraitName(data.class, gender, -1), true)
    item.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.name
    local level = item.transform:Find("Level"):GetComponent(typeof(UILabel))
    level.text = SafeStringFormat3(LocalString.GetString("%d级"), data.passportLevel)

    local rankNum = item.transform:Find("Rank"):GetComponent(typeof(UILabel))
    local rankSprite = item.transform:Find("Rank/Sprite"):GetComponent(typeof(UISprite))
    rankNum.text = ""
    rankSprite.spriteName = ""
    if rank >= 1 and rank <= 3 then
        rankSprite.spriteName = self.rankSpriteTbl[rank]
    else
        rankNum.text = rank
    end
end

function LuaArenaPassportWnd:InitRewardTableView()
    self.rewardChildCount = 0
    self.reward_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.rewardChildCount
        end,
        function(item, index)
            self:InitRewardItem(item, index)
        end
    )
end

function LuaArenaPassportWnd:InitRewardItem(item, index)
    local level = index + 1
    local widget = item.transform:GetComponent(typeof(UIWidget))
    local show = item.transform:Find("Show")
    local upgradeTip = item.transform:Find("UpgradeTip")
    if level == self.rewardChildCount and not self.isAllRewardShowed then
        show.gameObject:SetActive(false)
        upgradeTip.gameObject:SetActive(true)
        widget.width = upgradeTip:GetComponent(typeof(UILabel)).width
    else
        show.gameObject:SetActive(true)
        upgradeTip.gameObject:SetActive(false)

        local levelLabel = show:Find("Level"):GetComponent(typeof(UILabel))
        levelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), level)
        show:Find("CheckMark").gameObject:SetActive(level <= self.rewardedLevel)
        local canReceive = level > self.rewardedLevel and level <= self.curLevel
        local receiveButton = show:Find("ReceiveButton").gameObject
        receiveButton:SetActive(canReceive)
        levelLabel.gameObject:SetActive(not canReceive)
        if canReceive then
            UIEventListener.Get(receiveButton).onClick = DelegateFactory.VoidDelegate(function (go)
                Gac2Gas.RequestGetArenaPassportLevelReward(level, false)
            end)
        end

        local itemTable = show:Find("Table"):GetComponent(typeof(UITable))
        local itemRewards = LuaArenaMgr:GetPassportItemRewards(level)
        self:AddChild(itemTable.gameObject, self.reward_ItemTemplate, #itemRewards)
        for i = 1, #itemRewards do
            local data = itemRewards[i]
            local child = itemTable.transform:GetChild(i - 1)
            local returnAward = child:GetComponent(typeof(CQnReturnAwardTemplate))
            local itemData = Item_Item.GetData(data.itemId)
            returnAward:Init(itemData, data.count)
            child:Find("Bind").gameObject:SetActive(true)
            child:Find("Mask").gameObject:SetActive(level > self.curLevel)
        end
        itemTable:Reposition()

        local bounds = NGUIMath.CalculateRelativeWidgetBounds(itemTable.transform, itemTable.transform)
        local tableWidth = bounds.max.x - bounds.min.x
        LuaUtils.SetLocalPositionX(show:Find("Line").transform, tableWidth / 2 + 30)
        widget.width = tableWidth + 2 * 28
    end
end

function LuaArenaPassportWnd:InitAwardShow()
    local level = Arena_PassportSetting.GetData().BigMilestoneId
    if level <= 0 then return end
    local itemRewards = LuaArenaMgr:GetPassportItemRewards(level)
    local itemId = itemRewards[1].itemId
    local extraAttribute = Item_Item.GetData(itemId).ExtraAttribute
    local count, itemIdStr = string.match(extraAttribute, "SelectItem:(%d+);([^;]+);")
	local itemIds = {}
	for str in string.gmatch(itemIdStr, "([^,]+)") do
		local id, iCount = string.match(str, "(%d+)%|?(%d*)")
        table.insert(itemIds, tonumber(id))
	end

    for i = 1, 3 do
        local child = self.awardShowParent:Find("Award_" .. i)
        if itemIds[i] then
            local itemData = Item_Item.GetData(itemIds[i])
            child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
            child:Find("Info/Name"):GetComponent(typeof(UILabel)).text = itemData.Name
            child:Find("Info/Received").gameObject:SetActive(false)

            UIEventListener.Get(child:Find("Icon/Bg").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemIds[i])
            end)
        end
    end

    self.awardShowParent:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("ARENA_PASSPORT_BIG_MILESTONE_AWARD", level)
end

function LuaArenaPassportWnd:UpdateAwardShow()
    local received = self.rewardedLevel >= Arena_PassportSetting.GetData().BigMilestoneId
    for i = 1, 3 do
        local child = self.awardShowParent:Find("Award_" .. i)
        child:Find("Info/Received").gameObject:SetActive(received)
    end
end

function LuaArenaPassportWnd:UpdateReward()
    local maxLevel = Arena_PassportLevelReward.GetDataCount()
    local maxLevelDisplay = (self.curLevel + 200) > maxLevel and maxLevel or self.curLevel + 200
    self.isAllRewardShowed = maxLevelDisplay == maxLevel
    self.rewardChildCount = self.isAllRewardShowed and maxLevel or maxLevelDisplay + 1
    self.reward_TableView:ReloadData(true, false)
    self:ReturnToCurrentLevel()
end

function LuaArenaPassportWnd:UpdateOneClickReceiveButton()
    local sprite = self.oneClickReceiveButton.transform:GetComponent(typeof(UISprite))
    if self.curLevel > self.rewardedLevel then
        sprite.spriteName = g_sprites.GreenHandGiftWnd_OrangeButtonName
        self.oneClickReceiveButton.label.color = NGUIText.ParseColor24("351B01", 0)
        self.oneClickReceiveButtonFx:SetActive(true)
    else
        sprite.spriteName = g_sprites.GreenHandGiftWnd_BlueButtonName
        self.oneClickReceiveButton.label.color = NGUIText.ParseColor24("0E3254", 0)
        self.oneClickReceiveButtonFx:SetActive(false)
    end
end

function LuaArenaPassportWnd:AddChild(parent, template, num)
    local childCount = parent.transform.childCount
    if childCount < num then
        for i = childCount + 1, num do
            NGUITools.AddChild(parent, template)
        end
    end

    childCount = parent.transform.childCount
    for i = 0, childCount - 1 do
        parent.transform:GetChild(i).gameObject:SetActive(i < num)
    end
end

--@region UIEvent

function LuaArenaPassportWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("ARENA_PASSPORT_TIP")
end

function LuaArenaPassportWnd:OnRankButtonClick()
    if not self.rankArray then
        local tbl = {}
        for i = 1, #self.rankTypeTbl do
            table.insert(tbl, PopupMenuItemData(self.rankTypeTbl[i], DelegateFactory.Action_int(function (_)
                self.rankType = i
                self.rankButton.Text = self.rankTypeTbl[i]
                Gac2Gas.QueryArenaPassportRankInfo(i)
            end), false, nil, EnumPopupMenuItemStyle.Default))
        end
        self.rankArray = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
    end

    self.rankArrow.flip = UIBasicSprite.Flip.Vertically
    CPopupMenuInfoMgr.ShowPopupMenu(self.rankArray, self.rankButton.transform, AlignType.Bottom, 1, DelegateFactory.Action(function ( ... )
        self.rankArrow.flip = UIBasicSprite.Flip.Nothing
    end), 300, true, 296)
end

function LuaArenaPassportWnd:OnReturnButtonClick()
    self:ReturnToCurrentLevel()
end

function LuaArenaPassportWnd:OnOpenButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ArenaPassportOpenWnd)
end

function LuaArenaPassportWnd:OnOneClickReceiveButtonClick()
    Gac2Gas.RequestGetArenaPassportLevelReward(0, true)
end

function LuaArenaPassportWnd:OnBuyButtonClick()
    if self.curLevel == Arena_PassportLevelReward.GetDataCount() then
        g_MessageMgr:ShowMessage("ARENA_PASSPORT_HAS_REACHED_MAX_LEVEL")
        return
    end

    LuaArenaMgr.buyLevelInfo = {}
    LuaArenaMgr.buyLevelInfo.level = self.curLevel
    CUIManager.ShowUI(CLuaUIResources.ArenaPassportBuyLevelWnd)
end

function LuaArenaPassportWnd:OnGiveButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ArenaPassportGiftingWnd)
end

--@endregion UIEvent

function LuaArenaPassportWnd:OnRewardPanelClipMove()
    self:UpdateMilestone()
end

function LuaArenaPassportWnd:UpdateMilestone()
    local list = self.reward_TableView:GetVisibleItems()
    local min = 100000
    local max = 0
    for i = 0, list.Count - 1 do
        local item = list[i]
        if item.Row ~= 0 and item.Row ~= self.rewardChildCount - 1 then
            min = math.min(min, item.Row)
            max = math.max(max, item.Row)
        end
    end

    local maxLevel = Arena_PassportLevelReward.GetDataCount()
    local maxLevelDisplay = (self.curLevel + 200) > maxLevel and maxLevel or self.curLevel + 200
    local milestoneLevel = 0
    for i = max, maxLevelDisplay do
        local data = Arena_PassportLevelReward.GetData(i)
        if data and data.IsMilestoneReward == 1 then
            milestoneLevel = i
            break
        end
    end
    self.milestone:SetActive(milestoneLevel > 0)
    if milestoneLevel > 0 and self.currentMilestoneLevel ~= milestoneLevel then
        local itemRewards = LuaArenaMgr:GetPassportItemRewards(milestoneLevel)
        self.milestone_Level.text = SafeStringFormat3(LocalString.GetString("%d级"), milestoneLevel)
        local itemData = Item_Item.GetData(itemRewards[1].itemId)
        self.milestone_ReturnAward:Init(itemData, itemRewards[1].count)
        self.milestone_Bind:SetActive(true)
        self.currentMilestoneLevel = milestoneLevel
    end

    if self.curLevel < min - 2 then
        self.returnButton.gameObject:SetActive(true)
        self.returnButton.flip = UIBasicSprite.Flip.Nothing
    elseif self.curLevel > max + 2 then
        self.returnButton.gameObject:SetActive(true)
        self.returnButton.flip = UIBasicSprite.Flip.Horizontally
    else
        self.returnButton.gameObject:SetActive(false)
    end
end

function LuaArenaPassportWnd:ReturnToCurrentLevel()
    local level = self.curLevel > 0 and self.curLevel - 1 or 1
    self.reward_TableView:ScrollToRow(level)
    self:UpdateMilestone()
end
