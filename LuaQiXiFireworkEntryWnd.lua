local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local GameObject = import "UnityEngine.GameObject"
local CTeamMatchLevelPicker = import "L10.UI.CTeamMatchLevelPicker"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaQiXiFireworkEntryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXiFireworkEntryWnd, "LbTitle", "LbTitle", UILabel)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "TexFirework", "TexFirework", CUITexture)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "LbMusicNames", "LbMusicNames", UILabel)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "LbExtDec", "LbExtDec", UILabel)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "BtnStart", "BtnStart", GameObject)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "PickerHour", "PickerHour", CTeamMatchLevelPicker)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "SceneGo", "SceneGo", QnCheckBoxGroup)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "TypeGo", "TypeGo", QnCheckBoxGroup)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "PickerMinute", "PickerMinute", CTeamMatchLevelPicker)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "LbTimeDes", "LbTimeDes", UILabel)
RegistChildComponent(LuaQiXiFireworkEntryWnd, "BtnHelp", "BtnHelp", GameObject)

--@endregion RegistChildComponent end

function LuaQiXiFireworkEntryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.BtnStart.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnStartClick(go)
	end)

	self.SceneGo.OnSelect = DelegateFactory.Action_QnCheckBox_int(function (checkbox,value)
	    self:OnSceneGoSelect(checkbox,value)
	end)

	self.TypeGo.OnSelect = DelegateFactory.Action_QnCheckBox_int(function (checkbox,value)
	    self:OnTypeGoSelect(checkbox,value)
	end)

	UIEventListener.Get(self.BtnHelp.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnHelpClick(go)
	end)

    --@endregion EventBind end

	self.PickerHour.OnValueChanged = DelegateFactory.Action_int(function(value)
		self:OnPickerHourValueChanged(value)
	end)

	self.PickerMinute.OnValueChanged = DelegateFactory.Action_int(function(value)
		self:OnPickerMinuteValueChanged(value)
	end)

	self.QnCostAndOwnMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
end

function LuaQiXiFireworkEntryWnd:OnPickerHourValueChanged(value)
	self:CheckTime(value,self.Minute)
end

function LuaQiXiFireworkEntryWnd:OnPickerMinuteValueChanged(value)
	self:CheckTime(self.Hour,value)
end

function LuaQiXiFireworkEntryWnd:IsValidTime(hour,minute)
	local nowstamp = CServerTimeMgr.Inst.timeStamp--当前的nowstamp
	local data = QiXi_FireWork2021.GetData()
	local min = data.AppointmentTimeMin--5
	local max = data.AppointmentTimeMax--30
	local minstamp = nowstamp + (min - 1) * 60
	local maxstamp = nowstamp + max * 60

	local selectstamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(hour,minute,0)
	local nowtime = CServerTimeMgr.ConvertTimeStampToZone8Time(nowstamp)
	if hour < nowtime.Hour or (hour == nowtime.Hour and minute < nowtime.Minute) then --时间小于当前时间，认定玩家选择的是下一天的
		selectstamp = selectstamp + 24*3600--加一天的timestamp		
	end
	if selectstamp > maxstamp or selectstamp < minstamp then
		return false
	end
	return true
end

function LuaQiXiFireworkEntryWnd:CheckTime(hour,minute)
	if not self.Inited then return end
	self.Hour = hour
	self.Minute = minute
	if not self:IsValidTime(hour,minute) then
		self.LbTimeDes.color = Color.red
	else
		self.LbTimeDes.color = Color.white
	end
end

function LuaQiXiFireworkEntryWnd:Init()
	self.SceneType = 0
	self.FireType = 0
	self.Hour = 0
	self.Minute = 0
	self:RefreshAll(true)
	self.Inited = true
end

function LuaQiXiFireworkEntryWnd:RefreshAll(isinit)
	local waitTime = 10--额外的时间
	local datetime = CServerTimeMgr.Inst:GetZone8Time()
	datetime = datetime:AddMinutes(waitTime)
	self.Hour = datetime.Hour
	self.Minute = datetime.Minute
	if isinit then
		self.PickerHour:Init(0,23,datetime.Hour)
		self.PickerMinute:Init(0,59,datetime.Minute)
	else
		self:CheckTime(self.Hour,self.Minute)
	end
	self.SceneGo:SetSelect(self.SceneType,true,true)
	self.TypeGo:SetSelect(self.FireType,true,true)

	self:RefreshSelect()
end

function LuaQiXiFireworkEntryWnd:RefreshSelect()

	local info = self:GetFireInfo()

	self.QnCostAndOwnMoney:SetCost(info.cost)

	self.LbTitle.text = info.firename
	self.TexFirework:LoadMaterial(info.tex)
	self.LbMusicNames.text = info.musicdes
	self.LbExtDec.text = SafeStringFormat3(LocalString.GetString("可邀请%d人观看"),info.count)

end

function LuaQiXiFireworkEntryWnd:GetFireInfo()
	local info = {}
	info.cost = 0
	info.count = 0
	info.firename = ""
	info.gameplayid = 0
	info.musicdes = ""
	info.tex = ""

	local data = QiXi_FireWork2021.GetData()
	local typename = ""
	if self.FireType == 0 then
		typename = LocalString.GetString("经典")
		info.cost = data.LowPrice
		info.count = data.LowPriceVisitor
	elseif self.FireType == 1 then
		typename = LocalString.GetString("豪华")
		info.cost = data.HighPrice
		info.count = data.HighPriceVisitor
	end

	local sceneName = ""
	if self.SceneType == 0 then
		sceneName = LocalString.GetString("杭州")
		info.gameplayid = data.HZGameplayId
		if self.FireType == 0 then
			info.musicdes = g_MessageMgr:FormatMessage("2021QiXi_Firework_HZLowMusic")
			info.tex = data.HZLowPicture
		elseif self.FireType == 1 then
			info.musicdes = g_MessageMgr:FormatMessage("2021QiXi_Firework_HZHighMusic")
			info.tex = data.HZHighPicture
		end
	elseif self.SceneType == 1 then
		sceneName = LocalString.GetString("金沙镜")
		info.gameplayid = data.JSJGameplayId
		if self.FireType == 0 then
			info.musicdes = g_MessageMgr:FormatMessage("2021QiXi_Firework_JSJLowMusic")
			info.tex = data.JSJLowPicture
		elseif self.FireType == 1 then
			info.musicdes = g_MessageMgr:FormatMessage("2021QiXi_Firework_JSJHighMusic")
			info.tex = data.JSJHighPicture
		end
	end

	info.firename = sceneName..typename..LocalString.GetString("烟花")
	return info
end

--@region UIEvent

function LuaQiXiFireworkEntryWnd:OnBtnStartClick(go)

	if not self:IsValidTime(self.Hour,self.Minute) then
		g_MessageMgr:ShowMessage("2021QiXi_Firework_AppointmentTime")
		return
	end

	local info = self:GetFireInfo()
	local timestr = self.Hour..":"..self.Minute
	local msg = g_MessageMgr:FormatMessage("2021QiXi_Firework_OpenkGameplay", info.cost,info.firename,timestr)
	local okfunc = function()
		local timestamp = CServerTimeMgr.Inst:GetTodayTimeStampByTime(self.Hour,self.Minute,0)
		Gac2Gas.RequestEnterYanHuaPlay(info.gameplayid,self.FireType,timestamp)
		CUIManager.CloseUI(CLuaUIResources.QiXiFireworkEntryWnd)
	end
	g_MessageMgr:ShowOkCancelMessage(msg,okfunc, nil, nil, nil, false)
end

function LuaQiXiFireworkEntryWnd:OnSceneGoSelect(checkbox,value)
	self.SceneType = value
	self:RefreshSelect()
end

function LuaQiXiFireworkEntryWnd:OnTypeGoSelect(checkbox,value)
	self.FireType = value
	self:RefreshSelect()
end

function LuaQiXiFireworkEntryWnd:OnBtnHelpClick(go)
	g_MessageMgr:ShowMessage("2021QiXi_Firework_Rules")
end


--@endregion UIEvent

