-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CFreightEquipmentCell = import "L10.UI.CFreightEquipmentCell"
local CFreightEquipments = import "L10.UI.CFreightEquipments"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIMath = import "NGUIMath"
local NGUIText = import "NGUIText"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CFreightEquipments.m_Init_CS2LuaHook = function (this, index) 
    CommonDefs.ListClear(this.equipList)
    this.equipmentCellTpl.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(this.table.transform)
    do
        local i = 0
        while i < CommonDefs.DictGetValue(CFreightEquipSubmitMgr.submitEquipList, typeof(Int32), index).Count do
            local equip = NGUITools.AddChild(this.table.gameObject, this.equipmentCellTpl)
            CommonDefs.GetComponent_GameObject_Type(equip, typeof(CFreightEquipmentCell)):Init(CommonDefs.DictGetValue(CFreightEquipSubmitMgr.submitEquipList, typeof(Int32), index)[i])
            equip:SetActive(true)
            CommonDefs.ListAdd(this.equipList, typeof(GameObject), equip)
            UIEventListener.Get(equip).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(equip).onClick, MakeDelegateFromCSFunction(this.OnEquipClicked, VoidDelegate, this), true)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollview:ResetPosition()
    if this.equipList.Count > 0 then
        this.emptyLabel.text = ""
        this:OnEquipClicked(this.equipList[0])
    else
        this.emptyLabel.text = CFreightEquipSubmitMgr.emptyLabel
    end
end
CFreightEquipments.m_OnEquipClicked_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.equipList.Count do
            CommonDefs.GetComponent_GameObject_Type(this.equipList[i], typeof(CFreightEquipmentCell)).Selected = this.equipList[i] == go
            if this.equipList[i]:Equals(go) then
                if this.OnEquipSelected ~= nil then
                    GenericDelegateInvoke(this.OnEquipSelected, Table2ArrayWithCount({CommonDefs.GetComponent_GameObject_Type(this.equipList[i], typeof(CFreightEquipmentCell)).equipmentId}, 1, MakeArrayClass(Object)))
                end
                local b1 = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform)
                local height = b1.size.y
                local worldCenterY = this.background.transform:TransformPoint(b1.center).y
                local width = b1.size.x
                local worldCenterX = this.background.transform:TransformPoint(b1.center).x

                CItemInfoMgr.ShowLinkItemInfo(CommonDefs.GetComponent_GameObject_Type(this.equipList[i], typeof(CFreightEquipmentCell)).equipmentId, false, this, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
            end
            i = i + 1
        end
    end
end
CFreightEquipments.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local applyAction = CreateFromClass(StringActionKeyValuePair, CFreightEquipSubmitMgr.buttonLabel, MakeDelegateFromCSFunction(this.OnFill, Action0, this))
    this.curSelectId = itemId
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)

    return actionPairs
end
CFreightEquipments.m_OnFill_CS2LuaHook = function (this) 
    local item = CItemMgr.Inst:GetById(this.curSelectId)
    if item == nil then
        return
    end
    if CFreightEquipSubmitMgr.showConfirm then
        local text = ((((LocalString.GetString("确定要将") .. "[") .. NGUIText.EncodeColor24(item.Equip.DisplayColor)) .. "]") .. item.Name) .. LocalString.GetString("[-]装入货箱吗？")
        MessageWndManager.ShowOKCancelMessage(CChatLinkMgr.TranslateToNGUIText(text, false), DelegateFactory.Action(function () 
            EventManager.BroadcastInternalForLua(EnumEventType.OnFreightSubmitEquipSelected, {this.curSelectId, "GuildFreightEquipFill"})
            CUIManager.CloseUI(CUIResources.FreightEquipSubmitWnd)
            CItemInfoMgr.CloseItemInfoWnd()
        end), nil, nil, nil, false)
    else
        EventManager.BroadcastInternalForLua(EnumEventType.OnFreightSubmitEquipSelected, {this.curSelectId, CFreightEquipSubmitMgr.managerBroadcastKey})
        this.select = true
        CUIManager.CloseUI(CUIResources.FreightEquipSubmitWnd)
        CItemInfoMgr.CloseItemInfoWnd()
    end
end
