-- Auto Generated!!
local CBWDHWatchGroupItem = import "L10.UI.CBWDHWatchGroupItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CRankData = import "L10.UI.CRankData"
local CWeekendFightRankItem = import "L10.UI.CWeekendFightRankItem"
local CWeekendFightRankWnd = import "L10.UI.CWeekendFightRankWnd"
local Int32 = import "System.Int32"
local UIEventListener = import "UIEventListener"
local UIPanel = import "UIPanel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local XianZongShan_Setting = import "L10.Game.XianZongShan_Setting"
CWeekendFightRankWnd.m_Awake_CS2LuaHook = function (this) 
    this.rankArray = Table2ArrayWithCount({XianZongShan_Setting.GetData().Rank69, XianZongShan_Setting.GetData().Rank89, XianZongShan_Setting.GetData().Rank109, XianZongShan_Setting.GetData().Rank129, XianZongShan_Setting.GetData().Rank160}, 5, MakeArrayClass(System.UInt32))
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    this.selfNode:SetActive(false)
    this.groupView.m_DataSource = this
    this.contentView.m_DataSource = this
    this.groupView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)
    this.groupView:ReloadData(true, false)
    this.groupView:SetSelectRow(0, true)
end
CWeekendFightRankWnd.m_OnQueryBiWuChampionInfoResult_CS2LuaHook = function (this) 
    if CRankData.Inst.MainPlayerRankInfo.Rank > 0 then
        this.bgSprite.height = this.height2
        this.selfNode:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.selfNode, typeof(CWeekendFightRankItem)):Init(CRankData.Inst.MainPlayerRankInfo, - 1)
    else
        this.selfNode:SetActive(false)
        this.bgSprite.height = this.height1
    end
    CommonDefs.GetComponent_GameObject_Type(this.contentView.gameObject, typeof(UIPanel)):UpdateAnchors()

    this.contentList = CRankData.Inst.RankList
    this.contentView:ReloadData(true, false)
end
CWeekendFightRankWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if view == this.groupView then
        return 5
        -- CBiWuDaHuiMgr.GroupArray.Length;
    elseif view == this.contentView then
        if this.contentList ~= nil then
            return this.contentList.Count
        end
    end
    return 0
end
CWeekendFightRankWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if view == this.groupView then
        local cmp = TypeAs(this.groupView:GetFromPool(0), typeof(CBWDHWatchGroupItem))
        cmp:Init(row + 1)
        return cmp
    elseif view == this.contentView then
        local cmp = TypeAs(this.contentView:GetFromPool(0), typeof(CWeekendFightRankItem))
        cmp:Init(this.contentList[row], row)
        return cmp
    end
    return nil
end
