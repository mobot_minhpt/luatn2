local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local UIWidget = import "UIWidget"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType2 = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"

LuaWuLiangCompanionSkillFosterRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "GetLabel", "GetLabel", GameObject)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "LevelUpBtn", "LevelUpBtn", GameObject)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "ResetBtn", "ResetBtn", GameObject)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "ResNumLabel", "ResNumLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "AlreadyFullLevel", "AlreadyFullLevel", GameObject)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "SkillLabel", "SkillLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "SkillScrolView", "SkillScrolView", CUIRestrictScrollView)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "CurrentDescLabel", "CurrentDescLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "NextDescLabel", "NextDescLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "Table", "Table", UITable)
RegistChildComponent(LuaWuLiangCompanionSkillFosterRoot, "SkillTable", "SkillTable", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangCompanionSkillFosterRoot:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

	for i=1,5 do
		local label = self.SkillTable.transform:Find(tostring(i).."/LevelLabel"):GetComponent(typeof(UILabel))
		label.pivot = UIWidget.Pivot.Right
	end
end

function LuaWuLiangCompanionSkillFosterRoot:InitShow()
	
	if self.huoBanId ~= LuaWuLiangMgr.huobanSelectId or self.CurrentSelectIndex == nil then
		self.CurrentSelectIndex = 1
	end

	self.huoBanId = LuaWuLiangMgr.huobanSelectId
    self.huoBanInfo = LuaWuLiangMgr.huobanData.HuoBan[LuaWuLiangMgr.huobanSelectId]

	local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(self.huoBanId)
	local huoBanData = self.huoBanInfo

	print(self.huoBanId, self.huoBanInfo, LuaWuLiangMgr.huobanData.HuoBan)
	-- 显示技能信息
    LuaWuLiangMgr:ShowSkill(self.SkillTable, self.huoBanId, self.huoBanInfo.SkillLevelTbl, function(index, skillId)
		-- 点击回调
		print(index, skillId)
		self:SetCurrentSelect(index)
	end)

	self:SetCurrentSelect(self.CurrentSelectIndex)
end

function LuaWuLiangCompanionSkillFosterRoot:SetCurrentSelect(index)
	self.CurrentSelectIndex = index

	local setting = XinBaiLianDong_Setting.GetData()

	local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(self.huoBanId)
	local skillId = huoBanData.SkillList[index-1]
	local skillData = XinBaiLianDong_WuLiangSkill.GetData(skillId)

	local curLevel = 1
	if self.huoBanInfo.SkillLevelTbl[skillId] then
		curLevel = self.huoBanInfo.SkillLevelTbl[skillId] + 1
	end

	local curSkillData = Skill_AllSkills.GetData(skillId*100 + curLevel)

	local hasFullLevel = curLevel >= setting.WuLiangShenJing_HuoBanSkillMaxLv

	-- 高亮
	for i=1,5 do
		self.SkillTable.transform:Find(tostring(i).."/Highlight").gameObject:SetActive(i == index)
	end
	-- 升级显示
	self.NextDescLabel.gameObject:SetActive(not hasFullLevel)
	self.ItemCell.gameObject:SetActive(not hasFullLevel)
	self.LevelUpBtn.gameObject:SetActive(not hasFullLevel)
	self.ResNumLabel.gameObject:SetActive(not hasFullLevel)

	-- 满级显示
	self.AlreadyFullLevel.gameObject:SetActive(hasFullLevel)

	self.CurrentDescLabel.text = CUICommonDef.TranslateToNGUIText(curSkillData.Display)
	self.SkillLabel.text = SafeStringFormat3(LocalString.GetString("%s %s级"), curSkillData.Name, tostring(curLevel))

	-- 消耗数据
	local costData = self:GetSkillCostData(skillData.Cost, curLevel)
	local resetCostData = self:GetSkillCostData(skillData.Cost, curLevel-1)

	if not hasFullLevel then
		local itemId = setting.WuLiangShenJing_HuoBanSkillScore

		local nextSkillData = Skill_AllSkills.GetData(skillId*100 + curLevel+1)
		self.NextDescLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("[FFFF00]【需求等级%s】[-]%s"), 
			costData.NeedPlayerLevel, nextSkillData.Display))

		local needScore = costData.NeedScore
		local currentScore = LuaWuLiangMgr.huobanData.SkillScore

		local notEnough = needScore > currentScore
		if notEnough then
			self.ResNumLabel.text =  SafeStringFormat3(LocalString.GetString("[FFFF00]%s[-]/%s"), tostring(currentScore), tostring(needScore))
		else
			self.ResNumLabel.text =  SafeStringFormat3(LocalString.GetString("%s/%s"), tostring(currentScore), tostring(needScore)) 
		end

		self.GetLabel.gameObject:SetActive(notEnough)

		-- 物品图标
		local itemData = Item_Item.GetData(itemId)
		local icon = self.ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
		icon:LoadMaterial(itemData.Icon)

		-- 物品点击显示
		UIEventListener.Get(self.ItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			if notEnough then
				-- 获取方式
				CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, nil, AlignType2.Right)
			else
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end
		end)

		-- 升级
		UIEventListener.Get(self.LevelUpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			if notEnough then
				g_MessageMgr:ShowMessage("WuLiang_Skill_Score_Not_Enough")
			else
				Gac2Gas.WuLiangShenJing_UpgradeSkill(self.huoBanId, skillId)
			end
		end)
	end

	-- 重置
	UIEventListener.Get(self.ResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if curLevel == 1 then
			g_MessageMgr:ShowMessage("WuLiang_Skill_Not_Upgraded")
		else
			local str = g_MessageMgr:FormatMessage("WuLiang_Skill_Reset_Confirm", resetCostData.ResetMoney)

			g_MessageMgr:ShowOkCancelMessage(str, function()
				Gac2Gas.WuLiangShenJing_ResetHuoBanSkill(self.huoBanId, skillId)
			end, nil, nil, nil, false)
		end
	end)

	self.Table:Reposition()
	self.SkillScrolView:ResetPosition()
end

-- 根据不同的技能 消耗也不一样
function LuaWuLiangCompanionSkillFosterRoot:GetSkillCostData(index, curLevel)
	if index == 1 then
		return XinBaiLianDong_WuLiangSkillCost_1.GetData(curLevel)
	elseif index == 2 then
		return XinBaiLianDong_WuLiangSkillCost_2.GetData(curLevel)
	elseif index == 3 then
		return XinBaiLianDong_WuLiangSkillCost_3.GetData(curLevel)
	elseif index == 4 then
		return XinBaiLianDong_WuLiangSkillCost_4.GetData(curLevel)
	elseif index == 5 then
		return XinBaiLianDong_WuLiangSkillCost_5.GetData(curLevel)
	end
end

function LuaWuLiangCompanionSkillFosterRoot:OnEnable()
	self.CurrentSelectIndex = 1
	self:InitShow()
    g_ScriptEvent:AddListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitShow")
end

function LuaWuLiangCompanionSkillFosterRoot:OnDisable()
    g_ScriptEvent:RemoveListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitShow")
end

--@region UIEvent

--@endregion UIEvent
