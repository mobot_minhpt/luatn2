local CGuildMgr=import "L10.Game.CGuildMgr"
local CGuildLeagueMgr=import "L10.Game.CGuildLeagueMgr"
local EnumCommonForce=import "L10.UI.EnumCommonForce"
local GuildLeagueMainBattleResultInfo=import "L10.Game.GuildLeagueMainBattleResultInfo"
local GuildLeagueViceBattleResultInfo=import "L10.Game.GuildLeagueViceBattleResultInfo"
local GuildLeaguePlayerInfo=import "L10.Game.GuildLeaguePlayerInfo"

LuaGuildLeagueMgr = {}

LuaGuildLeagueMgr.m_PlayersToInvite = {}

LuaGuildLeagueMgr.m_AltarBuffHistoryInfo = {}

LuaGuildLeagueMgr.m_CurrentSituationForWatch = false

LuaGuildLeagueMgr.m_HomeHpInfo = nil

local t = {
	LocalString.GetString("甲级"),
	LocalString.GetString("乙级"),
	LocalString.GetString("丙级"),
	LocalString.GetString("丁级"),
	LocalString.GetString("戊级"),
	LocalString.GetString("己级"),
	LocalString.GetString("庚级"),
	LocalString.GetString("辛级"),
	LocalString.GetString("壬级"),
	LocalString.GetString("癸级"),
	
	LocalString.GetString("子级"),
	LocalString.GetString("丑级"),
	LocalString.GetString("寅级"),
	LocalString.GetString("卯级"),
	LocalString.GetString("辰级"),
	LocalString.GetString("巳级"),
	LocalString.GetString("午级"),
	LocalString.GetString("未级"),
	LocalString.GetString("申级"),
	LocalString.GetString("酉级"),
	LocalString.GetString("戌级"),
	LocalString.GetString("亥级"),
}
function LuaGuildLeagueMgr.GetLevelStr(level)
	return t[level] or ""
end
function LuaGuildLeagueMgr.GetPreLeagueDesc()
	if CGuildMgr.Inst.m_GuildDynamicInfo then
		local level = CGuildMgr.Inst.m_GuildDynamicInfo.GuildLeagueLevel
		local rank = CGuildMgr.Inst.m_GuildDynamicInfo.GuildLeagueRank
		local levelStr = LuaGuildLeagueMgr.GetLevelStr(level)
		if level == 0 then
			return LocalString.GetString("非联赛成员")
		else
			--如果在联赛中
			return SafeStringFormat3(LocalString.GetString("%s联赛第%s名"), levelStr, Extensions.ToChinese(rank))
		end
	end
	return ""
end

function LuaGuildLeagueMgr:ShowGuildLeagueAltarBuffHistoryWnd(historyTbl, defBuffLeftCount, attBuffLeftCount)
	self.m_AltarBuffHistoryInfo.historyTbl = historyTbl
	self.m_AltarBuffHistoryInfo.defBuffLeftCount = defBuffLeftCount
	self.m_AltarBuffHistoryInfo.attBuffLeftCount = attBuffLeftCount
	CUIManager.ShowUI("GuildLeagueAltarBuffHistoryWnd")
end

function LuaGuildLeagueMgr:ShowGuildLeagueSituationWnd(bWatch)
	self.m_CurrentSituationForWatch = bWatch
	CUIManager.ShowUI(CUIResources.GuildLeagueBattleResultWnd)
end

-------------
-- 跨服联赛英雄任免 不止跨服联赛用，所以放到这里
-------------
LuaGuildLeagueMgr.m_CurrentHeroInfo = nil
LuaGuildLeagueMgr.m_HasAppointHeroRight = false
LuaGuildLeagueMgr.m_AppointHeroMemberInfoTbl = nil
function LuaGuildLeagueMgr:OpenGuildLeagueHeroInfoWnd(canSetHero, info_U)
	--table.insert(ret, {heroId, memberInfo.m_Name, memberInfo.m_Class, memberInfo.m_Gender, memberInfo.m_Grade, memberInfo.m_XianFanStatus})
	local tmp = g_MessagePack.unpack(info_U)
	local heroInfo = {}
	for __, data in pairs(tmp) do
		table.insert(heroInfo, {id=data[1], name=data[2], class=data[3], gender=data[4], level=data[5], isInXianShenStatus=data[6]})
	end
	self.m_CurrentHeroInfo = heroInfo
	self.m_HasAppointHeroRight = canSetHero
	CUIManager.ShowUI("GuildLeagueAppointHeroWnd")
end

function LuaGuildLeagueMgr:RequestOpenGuildLeagueAppointHeroDetailWnd()
	Gac2Gas.OpenGuildLeagueAppointHeroWnd()
end

function LuaGuildLeagueMgr:QueryGuildLeagueSetHeroInfoResult(ret_U)
	--table.insert(ret, {memberId, online, info.m_Name, info.m_Class, info.m_Grade, info.m_XianFanStatus, info.m_XiuWeiGrade, info.m_XiuLianGrade, info.m_EquipScore, guildLeagueTimes, guildLeagueDuration, bHero})
	local tmp = g_MessagePack.unpack(ret_U)
	local memberInfo = {}
	for __, data in pairs(tmp) do
		table.insert(memberInfo, {id=data[1], online=data[2], name=data[3], class=data[4], level=data[5], 
			isInXianShenStatus=data[6], xiuweiGrade=data[7], xiulianGrade=data[8], equipScore=data[9], times=data[10], duration=data[11], bHero=data[12]})
	end
	self.m_AppointHeroMemberInfoTbl = memberInfo
	CUIManager.ShowUI("GuildLeagueAppointHeroDetailWnd")
    CUIManager.CloseUI("GuildLeagueAppointHeroWnd")
end

function LuaGuildLeagueMgr:RequestAppointGuildLeagueHero(destPlayerId, bHero)
	Gac2Gas.RequestAppointGuildLeagueHero(destPlayerId, bHero)
end

function LuaGuildLeagueMgr:AppointGuildLeagueHeroSuccess(destPlayerId, bHero)
	g_ScriptEvent:BroadcastInLua("AppointGuildLeagueHeroSuccess", destPlayerId, bHero)
end



function Gas2Gac.ShowPlayerInfoNotInTeamGroupInGuildLeague(playerInfoTbl_U)
	local infoList = MsgPackImpl.unpack(playerInfoTbl_U)
    if not infoList then return end

    LuaGuildLeagueMgr.m_PlayersToInvite = {}

    for i = 0, infoList.Count - 1, 1 do
    	local info = infoList[i]
    	if info.Count >= 3 then
    		local playerName = info[0]
    		local playerId = info[1]
    		local playerJob = info[2]
    		table.insert(LuaGuildLeagueMgr.m_PlayersToInvite, {
    			playerName = playerName,
    			playerId = playerId,
    			playerJob = playerJob,
    			})
    	end
    end

    if CUIManager.IsLoaded(CLuaUIResources.GuildLeagueInviteWnd) then
        g_ScriptEvent:BroadcastInLua("UpdateGuildLeagueInvite", LuaGuildLeagueMgr.m_PlayersToInvite)
    else
        CUIManager.ShowUI(CLuaUIResources.GuildLeagueInviteWnd)
    end

end
--老家血量 这个rpc被联赛和联赛观战所复用，所以改为了通过广播消息刷新界面
function Gas2Gac.UpdateGuildLeagueHomeHp(hpInfo)
	local datas = MsgPackImpl.unpack(hpInfo)
	local tbl = {}
	--场景中左攻右守（对应EnumCommonForce 0是守1是攻）左将军威(攻方，1)右玄霄怒(守方，0)
	tbl.force1 = math.floor(tonumber(datas[0])+0.5)
	tbl.curHp1 = math.floor(tonumber(datas[1])+0.5)
	tbl.fullHp1 = math.floor(tonumber(datas[2])+0.5)
	tbl.force2 = math.floor(tonumber(datas[3])+0.5)
	tbl.curHp2 = math.floor(tonumber(datas[4])+0.5)
	tbl.fullHp2 = math.floor(tonumber(datas[5])+0.5)
	LuaGuildLeagueMgr.m_HomeHpInfo = tbl
	g_ScriptEvent:BroadcastInLua("OnUpdateGuildLeagueHomeHp")
end

function Gas2Gac.QueryGuildLeagueInfoResult(headInfo1, playerInfo1, headInfo2, playerInfo2, bMainScene)
	if bMainScene then
		local myForce = CGuildLeagueMgr.Inst:GetMyForce()
		local shenshouInfo = {}
		local function parse(result,headInfo,playerInfo)
			local datas = MsgPackImpl.unpack(headInfo)
			result.force = tonumber(datas[0])
			result.name = tostring(datas[1])
			result.towerNum = tonumber(datas[2])
			result.mainScenePlayerNum = tonumber(datas[3])
			result.hp = tonumber(datas[4])--当前血量
			result.maxHp = tonumber(datas[5])--血量上限
			result.serverName = ""
			result.hasResult = false
			result.isWin = false
			if datas.Count > 6 then
				result.serverName = tostring(datas[6])
			end
			if datas.Count>7 then
				local val = tonumber(datas[7])
				result.hasResult = val ~= EnumToInt(EnumCommonForce.eNeutral)
				result.isWin = val == result.force
			end
			if myForce==result.force and datas.Count>8 then
				local list = datas[8]
				for i=1,list.Count do
					table.insert(shenshouInfo,list[i-1])
				end
			end

			local datas = MsgPackImpl.unpack(playerInfo)
			for i=0,datas.Count-1,12 do
				local item = GuildLeaguePlayerInfo()

				item.playerName = tostring(datas[i])
				item.level = tostring(datas[i+1])
				item.killCount = tostring(datas[i+2])
				item.deadCount = tostring(datas[i+3])
				item.dpsNum = tostring(datas[i+4])
				item.healNum = tostring(datas[i+5])
				item.underDamage = tostring(datas[i+6])
				item.fuhuoCount = tostring(datas[i+7])
				item.baoshiCount = tostring(datas[i+8])
				item.controlCount = tostring(datas[i+9])
				item.clazz = tostring(datas[i+10])
				item.rmControlCount = tostring(datas[i+11])
				CommonDefs.ListAdd_LuaCall(result.playerInfos,item)
			end
		end

		local info1 = GuildLeagueMainBattleResultInfo()
		parse(info1,headInfo1, playerInfo1)
		local info2 = GuildLeagueMainBattleResultInfo()
		parse(info2,headInfo2, playerInfo2)
		g_ScriptEvent:BroadcastInLua("GetGuildLeagueInfoResult",true, info1, info2)
		g_ScriptEvent:BroadcastInLua("RemoteSummonGuildLeagueMainPlayBossSuccess",shenshouInfo)
	else
		local function parse(result,headInfo,playerInfo)
			local datas = MsgPackImpl.unpack(headInfo)

			result.force = tonumber(datas[0])
			result.name = tostring(datas[1])
			result.killBossNum = tonumber(datas[2])
			result.viceScenePlayerNum = tonumber(datas[3])
			result.hp = tonumber(datas[4])--当前血量
			result.maxHp = tonumber(datas[5])--血量上限
			result.serverName = ""
			result.hasResult = false
			result.isWin = false
			result.killSmallBossNum = 0
			if (datas.Count > 6) then
				result.serverName = tostring(datas[6])
			end
			if (datas.Count > 7) then
				local val = tonumber(datas[7])
				result.hasResult = val ~= EnumToInt(EnumCommonForce.eNeutral)
				result.isWin = val == result.force;
			end
			if (datas.Count > 8) then
				result.killSmallBossNum = tonumber(datas[8])
			end


			local datas = MsgPackImpl.unpack(playerInfo)
			for i=0,datas.Count-1,13 do
				local item = GuildLeaguePlayerInfo()
				item.playerName = tostring(datas[i])
				item.level = tostring(datas[i+1])
				item.killCount = tostring(datas[i+2])
				item.deadCount = tostring(datas[i+3])
				item.repairCount = tostring(datas[i+4])
				item.dpsNum = tostring(datas[i+5])
				item.healNum = tostring(datas[i+6])
				item.underDamage = tostring(datas[i+7])
				item.fuhuoCount = tostring(datas[i+8])
				item.baoshiCount = tostring(datas[i+9])
				item.controlCount = tostring(datas[i+10])
				item.clazz = tostring(datas[i+11])
				item.rmControlCount = tostring(datas[i+12])

				CommonDefs.ListAdd_LuaCall(result.playerInfos,item)
			end
		end
		local info1 = GuildLeagueViceBattleResultInfo()
		parse(info1,headInfo1, playerInfo1)
		local info2 = GuildLeagueViceBattleResultInfo()
		parse(info2,headInfo2, playerInfo2)
		g_ScriptEvent:BroadcastInLua("GetGuildLeagueInfoResult",false, info1, info2)
	end
end
function Gas2Gac.RemoteSummonGuildLeagueMainPlayBossSuccess(U)
	local list = MsgPackImpl.unpack(U)
	local t = {}
	for i=1,list.Count do
		table.insert( t,list[i-1] )
	end
	g_ScriptEvent:BroadcastInLua("RemoteSummonGuildLeagueMainPlayBossSuccess",t)
end