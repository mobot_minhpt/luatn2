require("common/common_include")
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatLinkMgr = import "CChatLinkMgr"
local NGUIText = import "NGUIText"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaPreExistenceMarridgeDetailWnd = class()
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_NumberLabel")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_NumberCornerLabel")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_AvartarTexture")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_BriefLabel")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_MainContentLabel")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_Table")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_ShareGroup")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_ShareBtn1")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_ShareBtn2")
RegistClassMember(LuaPreExistenceMarridgeDetailWnd, "m_CloseBtn")

function LuaPreExistenceMarridgeDetailWnd:Awake()
    self.m_NumberLabel = self.transform:Find("Anchor/NumberBG/NumberLabel"):GetComponent(typeof(UILabel))
    self.m_NumberCornerLabel = self.transform:Find("Anchor/NumberBG/NumberTexture/Label"):GetComponent(typeof(UILabel))
    self.m_AvartarTexture = self.transform:Find("Anchor/AvatarCell/AvatarTexture"):GetComponent(typeof(CUITexture))
    self.m_MainContentLabel = self.transform:Find("Anchor/ScrollView/MainContentLabel"):GetComponent(typeof(UILabel))
    self.m_ShareGroup = self.transform:Find("Anchor/ShareGroup").gameObject
    self.m_ShareBtn1 = self.transform:Find("Anchor/ShareGroup/ShareBtn1").gameObject
    self.m_ShareBtn2 = self.transform:Find("Anchor/ShareGroup/ShareBtn2").gameObject
    self.m_CloseBtn = self.transform:Find("background/CloseButton").gameObject
end

function LuaPreExistenceMarridgeDetailWnd:Init()
    local GenerationNum = LuaValentine2019Mgr.m_PreExistenceDetailGeneration
    local data = Valentine2019_QSYYEnding.GetData(LuaValentine2019Mgr.m_PreExistenceDetailTemplateID)

    if not data then
        return
    end

    local ChineseNum = Extensions.ToChinese(GenerationNum)
    self.m_NumberLabel.text = LocalString.GetString("第") .. ChineseNum .. LocalString.GetString("世")
    self.m_NumberCornerLabel.text = ChineseNum

    self.m_AvartarTexture:LoadNPCPortrait(data.Icon, false)

    local contentStr = data.Doc or ""
    if CClientMainPlayer.Inst then
        local linkStr = SafeStringFormat3("<link player=%s,%s>", LuaValentine2019Mgr.m_PreExistencePlayerID, LuaValentine2019Mgr.m_PreExistencePlayerName)
        self.m_MainContentLabel.text = SafeStringFormat3(
            "[c][%s]%s[-][/c]",
            NGUIText.EncodeColor24(self.m_MainContentLabel.color),
            CChatLinkMgr.TranslateToNGUIText(
                System.String.Format(contentStr, CChatLinkMgr.TranslateToNGUIText(linkStr, false)),
                false
            )
        )
    end

    CommonDefs.AddOnClickListener(
        self.m_MainContentLabel.gameObject,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnClickContentLable(self.m_MainContentLabel)
            end
        ),
        false
    )

    local showSharebtn = LuaValentine2019Mgr.m_ShowPreExistenceShare

    self.m_ShareGroup:SetActive(showSharebtn)
    if showSharebtn then
        CommonDefs.AddOnClickListener(
            self.m_ShareBtn1,
            DelegateFactory.Action_GameObject(
                function(go)
                    self:OnClickShareToChatButton(go)
                end
            ),
            false
        )

        CommonDefs.AddOnClickListener(
            self.m_ShareBtn2,
            DelegateFactory.Action_GameObject(
                function(go)
                    self:OnClickNormalShareButton(go)
                end
            ),
            false
        )
    end

    CommonDefs.AddOnClickListener(
        self.m_CloseBtn,
        DelegateFactory.Action_GameObject(
            function(go)
                self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
                if LuaValentine2019Mgr.m_AlbumWnd then
                    LuaValentine2019Mgr.m_AlbumWnd:SetActive(true)
                end
            end
        ),
        false
    )
end

function LuaPreExistenceMarridgeDetailWnd:OnClickContentLable(label)
    local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function LuaPreExistenceMarridgeDetailWnd:OnClickShareToChatButton(go)
    local buttonName = g_MessageMgr:FormatMessage("SHARE_QSYY_DETAIL_TOCHAT", nil)

    local msg =
        SafeStringFormat3(
        "<link button=%s,PreExistenceDetailShareClick,%s,%s,%s,%s>",
        buttonName,
        tostring(LuaValentine2019Mgr.m_PreExistenceDetailGeneration),
        tostring(LuaValentine2019Mgr.m_PreExistenceDetailTemplateID),
        tostring(LuaValentine2019Mgr.m_PreExistencePlayerID),
        tostring(LuaValentine2019Mgr.m_PreExistencePlayerName)
    )
    CChatHelper.SendMsgWithFilterOption(EChatPanel.World, msg, 0, true)
end

function LuaPreExistenceMarridgeDetailWnd:OnClickNormalShareButton(go)
    UICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.m_ShareGroup,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaPreExistenceMarridgeDetailWnd:OnEnable()
end

function LuaPreExistenceMarridgeDetailWnd:OnDisable()
end

function LuaPreExistenceMarridgeDetailWnd:OnDestory()
end
