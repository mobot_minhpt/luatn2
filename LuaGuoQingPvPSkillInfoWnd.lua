local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"

local UITabBar = import "L10.UI.UITabBar"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local GameObject = import "UnityEngine.GameObject"
local Extensions = import "Extensions"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
LuaGuoQingPvPSkillInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "CommonSkillGird", "CommonSkillGird", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "SpecialSkillGird", "SpecialSkillGird", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "Commonskill", "Commonskill", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "Specialskill", "Specialskill", GameObject)
RegistChildComponent(LuaGuoQingPvPSkillInfoWnd, "MoreContentButton", "MoreContentButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuoQingPvPSkillInfoWnd, "m_RankData")
RegistClassMember(LuaGuoQingPvPSkillInfoWnd, "MyTeam")
RegistClassMember(LuaGuoQingPvPSkillInfoWnd, "EnemyTeam")
RegistClassMember(LuaGuoQingPvPSkillInfoWnd, "m_SkillButtons")

function LuaGuoQingPvPSkillInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    CUIManager.CloseUI(CLuaUIResources.GuoQingPvPSkillSelectWnd)
    UIEventListener.Get(self.MoreContentButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMoreContentButtonClick(go)
	end)
    self.m_SkillButtons = {}
    local myplayerid = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    self.MyTeam =  LuaGuoQingPvPMgr.AttackTeamSkillInfoData
    self.EnemyTeam = LuaGuoQingPvPMgr.DefendTeamSkillInfoData
    for i = 1,#LuaGuoQingPvPMgr.DefendTeamSkillInfoData do
        local data = LuaGuoQingPvPMgr.DefendTeamSkillInfoData[i]
        local playerid = data[1]
        if playerid == myplayerid then
            self.MyTeam =  LuaGuoQingPvPMgr.DefendTeamSkillInfoData
            self.EnemyTeam = LuaGuoQingPvPMgr.AttackTeamSkillInfoData
            break
        end
    end

    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self:NumberOfRows()
        end,
        function(item, index)
            return self:ItemAt(item,index)
        end)
    local callback = DelegateFactory.Action_int(function(row) self:OnSelectAtRow(row) end)
	self.TableView.OnSelectAtRow = callback
end

function LuaGuoQingPvPSkillInfoWnd:Init()
    self.Tabs:ChangeTab(0, false)
end

--@region UIEvent
function LuaGuoQingPvPSkillInfoWnd:OnMoreContentButtonClick(go)
    g_MessageMgr:ShowMessage("JinLuHunYuanZhan_Fight_Rule")
end
function LuaGuoQingPvPSkillInfoWnd:OnTabsTabChange(index)
    self.m_SkillButtons = {}
    if index == 0 then
        self.m_RankData = {}
        for i = 1,#self.MyTeam do 
            table.insert(self.m_RankData,self.MyTeam[i])
        end
    else
        self.m_RankData = {}
        for i = 1,#self.EnemyTeam do 
            table.insert(self.m_RankData,self.EnemyTeam[i])
        end
    end
	self.TableView:ReloadData(true, true)
end

function LuaGuoQingPvPSkillInfoWnd:NumberOfRows()
	return self.m_RankData and #self.m_RankData or 0
end

function LuaGuoQingPvPSkillInfoWnd:ItemAt(item,index)
	if not self.m_RankData then return end
	local info = self.m_RankData[index+1]
	self:InitItem(item.transform,info)
end

function LuaGuoQingPvPSkillInfoWnd:OnSelectAtRow(row)

end

function LuaGuoQingPvPSkillInfoWnd:InitItem(transform, info)
    
    
    local playerid = info[1]
    local name =info[2]
    local class = info[3]
    local gender = info[4]
    local playerheadicon = CUICommonDef.GetPortraitName(class, gender)

    local Info = transform:Find("Info").gameObject
    local headicon = FindChild(Info.transform,"Icon").gameObject:GetComponent(typeof(CUITexture))

    headicon:LoadNPCPortrait(playerheadicon)
    local nameobj = FindChild(Info.transform,"name").gameObject:GetComponent(typeof(UILabel))
    nameobj.text = name

    local CommonSkillGird = transform:Find("CommonSkillGird").gameObject
    Extensions.RemoveAllChildren(CommonSkillGird.transform)
    for j = 1,5
    do  
        local skillid = nil
        if info[j+5] ~= 0 then
            skillid = info[j+5]
        end
        local go=NGUITools.AddChild(CommonSkillGird,self.Commonskill)
        
        local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        if skillid ~= nil then
            local skilldata = Skill_AllSkills.GetData(skillid)
            if skilldata ~= nil then
                icon:LoadSkillIcon(skilldata.SkillIcon)
            end 
        else
            icon:Clear()
        end
        table.insert(self.m_SkillButtons,{go,skillid})
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (x)
            self:OnSkillIconClick(x)
        end)
        go:SetActive(true)
    end 

    CommonSkillGird:GetComponent(typeof(UIGrid)):Reposition()

    local SpecialSkillGird = transform:Find("SpecialSkillGird").gameObject
    Extensions.RemoveAllChildren(SpecialSkillGird.transform)
    for j = 6,7
    do  
        local skillid = nil
        if info[j+5] ~= 0 then
            skillid = info[j+5]
        end
        local go=NGUITools.AddChild(SpecialSkillGird,self.Specialskill)
        
        local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    
        if skillid ~= nil then
            local skilldata = Skill_AllSkills.GetData(skillid)
            if skilldata ~= nil then
                icon:LoadSkillIcon(skilldata.SkillIcon)
            end 
        else
            icon:Clear()
        end
        table.insert(self.m_SkillButtons,{go,skillid})
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (x)
            self:OnSkillIconClick(x)
        end)

        go:SetActive(true)
    end 
    SpecialSkillGird:GetComponent(typeof(UIGrid)):Reposition()
end

function LuaGuoQingPvPSkillInfoWnd:OnSkillIconClick(go)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end
    for i = 1,#self.m_SkillButtons do
        if self.m_SkillButtons[i][1] == go then
            local skillId = self.m_SkillButtons[i][2]
            if skillId == nil or skillId == SkillButtonSkillType_lua.NoSkill then
                return
            end
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
            break
        end
    end
end
--@endregion UIEvent

