local Transform = import "UnityEngine.Transform"

local UITable = import "UITable"

local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

local UITexture = import "UITexture"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaChristmas2023XueHuaXuYuGetGiftWnd = class()
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "PlayerImage")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "WishLabel")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "NameLabel")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "Gift")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "TipLabel")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "IconTexture")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "Overlay")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "HasGot")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "Highlight")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "NameLeft")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "SendBtn")
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_WishId")     -- 祝福的id
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_HasGift")    -- 是否有礼物
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_ItemId")     -- 礼物id
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_GiftIndex")  -- 礼物种类
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_GiftCount")  -- 礼物个数
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_PlayerId")   -- 玩家id
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_PlayerName") -- 玩家名称
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_WishText")   -- 祝福文字
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_HasGotGift") -- 是否领取过礼物
RegistClassMember(LuaChristmas2023XueHuaXuYuGetGiftWnd, "m_PlayerInfo") -- 玩家信息

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023XueHuaXuYuGetGiftWnd, "Content_HasGift", "Content_HasGift", Transform)
RegistChildComponent(LuaChristmas2023XueHuaXuYuGetGiftWnd, "Content_NoGift", "Content_NoGift", Transform)

--@endregion RegistChildComponent end

function LuaChristmas2023XueHuaXuYuGetGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWishInfo()
    self.Content_HasGift.gameObject:SetActive(self.m_HasGift)
    self.Content_NoGift.gameObject:SetActive(not self.m_HasGift)
    local tf = (self.m_HasGift and self.Content_HasGift or self.Content_NoGift)
    self.PlayerImage = tf:Find("PlayerImage"):GetComponent(typeof(CUITexture))
    self.WishLabel = tf:Find("WishLabel"):GetComponent(typeof(UILabel))
    self.NameLabel = tf:Find("PlayerName/NameLabel"):GetComponent(typeof(UILabel))
    self.NameLeft = tf:Find("PlayerName/TextureL"):GetComponent(typeof(UITexture))
    if self.m_HasGift then
        self.Gift = tf:Find("Gift").gameObject
        self.TipLabel = tf:Find("Gift/TipLabel"):GetComponent(typeof(UILabel))
        self.IconTexture = tf:Find("Gift/GiftInfo/IconTexture"):GetComponent(typeof(CUITexture))
        self.Overlay = tf:Find("Gift/GiftInfo/Overlay").gameObject
        self.HasGot = tf:Find("Gift/GiftInfo/Overlay/HasGot").gameObject
        self.Highlight = tf:Find("Gift/GiftInfo/Background/Highlight").gameObject
    end
    self.SendBtn = tf:Find("SendBtn").gameObject

    UIEventListener.Get(self.SendBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaChristmas2023Mgr:SendGift(LuaChristmas2023Mgr.m_posx, LuaChristmas2023Mgr.m_posy)
    end)

    LuaChristmas2023Mgr.m_PlayerImage = self.PlayerImage
end
-- 是否获取过当前祝福的礼物
function LuaChristmas2023XueHuaXuYuGetGiftWnd:CheckPlayerId(dict)
    if not dict then return false end
    if CClientMainPlayer.Inst then
        local playerid = CClientMainPlayer.Inst.Id
        if CommonDefs.IsDic(dict) and CommonDefs.DictTryGet(dict, typeof(String), tostring(playerid), typeof(UInt64)) then
            return true
        end
    end
    return false
end
-- 初始化祝福信息
function LuaChristmas2023XueHuaXuYuGetGiftWnd:InitWishInfo()
    local info = LuaChristmas2023Mgr.m_WishTable
    if not info then return end
    self.m_WishId = info.Id
    self.m_HasGift = false
    self.m_GiftIndex = 0
    self.m_GiftCount = 0
    if info.Gifts and info.Gifts.Count >= 2 then
        for i = 0, info.Gifts.Count - 1, 1 do
            if info.Gifts[i] > 0 then
                self.m_HasGift = true
                self.m_GiftIndex = i
                self.m_GiftCount = info.Gifts[i]
                if info.Sended and info.Sended.Count > i then
                    self.m_GiftCount = self.m_GiftCount - info.Sended[i]
                end
                break
            end
        end
    end
    self.m_PlayerId = info.PlayerId and info.PlayerId or ""
    self.m_PlayerName = info.PlayerName and info.PlayerName or ""
    self.m_WishText = info.Msg and info.Msg or ""
    self.m_HasGotGift = self:CheckPlayerId(info.SendPlayerId)
    self.m_PlayerInfo = info.PlayerInfo and info.PlayerInfo or nil
end
function LuaChristmas2023XueHuaXuYuGetGiftWnd:Init()
    LuaChristmas2023Mgr:LoadPlayerImage(self.m_PlayerInfo)
    self.NameLabel.text = self.m_PlayerName
    self.WishLabel.text = self.m_WishText
    self.NameLeft:UpdateAnchors()

    if not self.m_HasGift then return end
    local itemid = Christmas2021_Setting.GetData().CardGift[self.m_GiftIndex * 3]
    self.m_ItemId = itemid
    local item = Item_Item.GetData(itemid)
    if item then self.IconTexture:LoadMaterial(item.Icon) end
    if self.m_HasGotGift then
        -- 礼物领取过了
        self.Overlay:SetActive(true)
        self.HasGot:SetActive(true)
        self.Highlight:SetActive(false)
        self.TipLabel.text = LocalString.GetString("已成功领取礼物~")
        UIEventListener.Get(self.IconTexture.gameObject).onClick = nil
    elseif self.m_GiftCount > 0 then
        -- 还有礼物并且还没有领取过
        self.Overlay:SetActive(false)
        self.Highlight:SetActive(true)
        self.TipLabel.text = LocalString.GetString("点击图标领取礼物~")
        UIEventListener.Get(self.IconTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.RequestGetCardGift(self.m_WishId, self.m_ItemId)
        end)
    else
        -- 没礼物了
        self.Overlay:SetActive(true)
        self.HasGot:SetActive(false)
        self.Highlight:SetActive(false)
        self.TipLabel.text = LocalString.GetString("手慢了，礼物已经被领完啦！")
        UIEventListener.Get(self.IconTexture.gameObject).onClick = nil
    end
end
function LuaChristmas2023XueHuaXuYuGetGiftWnd:OnGetChrismas2021CardGiftSuccess()
    self.Overlay:SetActive(true)
    self.HasGot:SetActive(true)
    self.Highlight:SetActive(false)
    self.TipLabel.text = LocalString.GetString("已成功领取礼物~")
    UIEventListener.Get(self.IconTexture.gameObject).onClick = nil
end
function LuaChristmas2023XueHuaXuYuGetGiftWnd:OnEnable()
    g_ScriptEvent:AddListener("GetChrismas2021CardGiftSuccess",self,"OnGetChrismas2021CardGiftSuccess")
end
function LuaChristmas2023XueHuaXuYuGetGiftWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetChrismas2021CardGiftSuccess",self,"OnGetChrismas2021CardGiftSuccess")
    LuaChristmas2023Mgr.m_PlayerImage = nil
end

--@region UIEvent

--@endregion UIEvent

