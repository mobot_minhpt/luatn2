local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local CIMMgr = import "L10.Game.CIMMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local Utility = import "L10.Engine.Utility"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CPos = import "L10.Engine.CPos"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local Color = import "UnityEngine.Color"
local ShaderEx = import "ShaderEx"
local Main = import "L10.Engine.Main"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CCPlayerMgr = import "L10.Game.CCPlayerMgr"
local CCPlayer = import "CCPlayer"
local CScene = import "L10.Game.CScene"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"
local CClientCCVideoObj = import "L10.Game.CClientCCVideoObj"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local CLoginMgr = import "L10.Game.CLoginMgr"

if rawget(_G, "LuaYuanXiao2023Mgr") then
    g_ScriptEvent:RemoveListener("PlayerLogin", LuaYuanXiao2023Mgr, "OnPlayerLogin")
end

LuaYuanXiao2023Mgr = {}

g_ScriptEvent:AddListener("PlayerLogin", LuaYuanXiao2023Mgr, "OnPlayerLogin")

LuaYuanXiao2023Mgr.FirstEnterMainWnd = false

function LuaYuanXiao2023Mgr:OnPlayerLogin()
    LuaYuanXiao2023Mgr.FirstEnterMainWnd = false
end

--#region 秦淮灯会

function LuaYuanXiao2023Mgr.ShowHideUI(show)
    local hideExcepttb = {"ThumbnailChatWindow","CCDanMuWnd","ZhiBoScreenLookWnd"}
    local hideExcept = Table2List(hideExcepttb, MakeGenericClass(List, cs_string))
    if show then
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false)
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)
    else
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, hideExcept, false, false, false)
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, false)
    end
end

LuaYuanXiao2023Mgr.PreMaxSpHeight = 0

function LuaYuanXiao2023Mgr:QHLanternShowChangeView(partyId)
    self.PreMaxSpHeight = CameraFollow.Inst.MaxSpHeight
    CameraFollow.Inst.MaxSpHeight = YuanXiao2023_Setting.GetData().MaxSpHeight
    local CameraView = YuanXiao2023_Setting.GetData().CameraView
    local vec3 = Vector3(CameraView[0], CameraView[1], CameraView[2])
    CameraFollow.Inst:SetMode(CameraMode.E3DPlus)
    PlayerSettings.Saved3DMode = 2
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)

    self.ShowHideUI(false)

    g_ScriptEvent:AddListener("MainCameraAngleChanged", self, "OnMainCameraAngleChanged")
end

function LuaYuanXiao2023Mgr:OnMainCameraAngleChanged()
    g_ScriptEvent:RemoveListener("MainCameraAngleChanged", self, "OnMainCameraAngleChanged")
    CameraFollow.Inst.MaxSpHeight = self.PreMaxSpHeight

    self.ShowHideUI(true)
end

LuaYuanXiao2023Mgr.IsPlaying = false
LuaYuanXiao2023Mgr.CameraLock = false
LuaYuanXiao2023Mgr.CurPartyId = nil

--直播大屏
LuaYuanXiao2023Mgr.ZhiBoObj = nil

LuaYuanXiao2023Mgr.VolumeTick = nil
LuaYuanXiao2023Mgr.LastVolume = -1

function LuaYuanXiao2023Mgr:AddListener()
    g_ScriptEvent:AddListener("OnCCPlayerBeginPlay",self,"OnCCPlayerBeginPlay")
    g_ScriptEvent:AddListener("GasDisconnect", self, "OnGasDisconnect")
    g_ScriptEvent:AddListener("RenderSceneInit", self, "OnRenderSceneInit")
    g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "OnPlayerMove")
    g_ScriptEvent:AddListener("OnSoundEnableChanged",self,"OnSoundEnableChanged")

    self.LastVolume = -1
    UnRegisterTick(self.VolumeTick)
    self.VolumeTick = RegisterTick(function()
        if CCPlayerMgr.Inst.m_Player:IsPlaying() and PlayerSettings.SoundEnabled and PlayerSettings.VolumeSetting ~= self.LastVolume then
            self.LastVolume = PlayerSettings.VolumeSetting
            CCPlayerMgr.Inst.m_Player:SetVolume(PlayerSettings.VolumeSetting)
        end
    end, 333)
end

function LuaYuanXiao2023Mgr:RemoveListener()
    g_ScriptEvent:RemoveListener("OnCCPlayerBeginPlay",self,"OnCCPlayerBeginPlay")
    g_ScriptEvent:RemoveListener("GasDisconnect", self, "OnGasDisconnect")
    g_ScriptEvent:RemoveListener("RenderSceneInit", self, "OnRenderSceneInit")
    g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "OnPlayerMove")
    g_ScriptEvent:RemoveListener("OnSoundEnableChanged",self,"OnSoundEnableChanged")

    g_ScriptEvent:RemoveListener("MainCameraAngleChanged", self, "OnMainCameraAngleChanged")

    UnRegisterTick(self.VolumeTick)
    self.VolumeTick = nil
end

function LuaYuanXiao2023Mgr:OnCCPlayerBeginPlay( )
    if self.ZhiBoObj then 
        self.ZhiBoObj:Init()
        return
    end

    --@策划 todo
    local res = YuanXiao_FireworkPartySetting.GetData().ScreenObjRes
    if not self.CurPartyId then return end 

    local party = YuanXiao2023_LanternShowSchedule.GetData(self.CurPartyId)
    if not party then return end 

    local transforms = party.ScreenTransform

    local valuetable = g_LuaUtil:StrSplitAdv(transforms, ";")
    local vtable = g_LuaUtil:StrSplitAdv(valuetable[1], ",")

    local obj = CClientCCVideoObj.Create(res,vtable[1],vtable[2],vtable[3],vtable[4],vtable[5])
    self.ZhiBoObj = obj
    self:OnSoundEnableChanged()

    local tree = GameObject.Find("zw_shu_03_02 (15)")
    if tree then tree:SetActive(false) end
end

function LuaYuanXiao2023Mgr:StopZhiBo()
    local tree = GameObject.Find("zw_shu_03_02 (15)")
    if tree then tree:SetActive(true) end
    self:LeavePartyScene()
    CScene.MainScene.AllowZuoQi = true
end

function LuaYuanXiao2023Mgr:OnGasDisconnect()
    self:StopZhiBo()
end

function LuaYuanXiao2023Mgr:QHLanternShowStart(partyId, endTime)

end

function LuaYuanXiao2023Mgr:QHLanternShowEnd(partyId)
    CameraFollow.Inst.MaxSpHeight = 0
end

function LuaYuanXiao2023Mgr:QHLanternShowZhiBo_SyncChannelInfo(id, zhiboInfo)
    self.CurPartyId = id
    self:LeavePartyScene()
    self.IsPlaying = true
    CCCChatMgr.Inst:InitCurrentStation(zhiboInfo)
    local url = CCCChatMgr.Inst.CCLiveURL

    SoundManager.Inst:StopBGMusic()
    self:AddListener()

    self:PlayCC(url, PlayerSettings.SoundEnabled and PlayerSettings.VolumeSetting or 0)
    CCPlayerMgr.Inst:Lock("FIREWORKPARTY_ZHIBO_CONFLICT")
end

function LuaYuanXiao2023Mgr:PlayCC(url, volume)
    CCPlayerMgr.Inst.m_Url = url
    local uid = 0
    if CClientMainPlayer.Inst then uid = CClientMainPlayer.Inst.Id end
    local loginInfo = CLoginMgr.Inst.m_UniSdkLoginInfo
    local authInfo = CLoginMgr.Inst.m_UniSdkSauthInfo
    local src = "qnm"
    local deviceId = authInfo.udid
    if System.String.IsNullOrEmpty(deviceId) then deviceId = loginInfo.UniSdk_DeviceId end
    local urs = CLoginMgr.Inst:GetExtendAccountId()
    local extraInfo = CCPlayerMgr.Inst:GetExtraInfo()
    if CCPlayerMgr.Inst.m_Player:IsPlaying() then
        CCPlayerMgr.Inst.m_Player:Stop()
    end
    CCPlayerMgr.Inst.m_Player:Play(url, src, urs, uid, deviceId, CCPlayerMgr.Inst.m_CurrentVbr == nil and "" or CCPlayerMgr.Inst.m_CurrentVbr, extraInfo, false, volume)
    CCPlayerMgr.Inst.m_NeedUpdateVbr = true
    CCPlayerMgr.Inst.m_IsLoading = true
    CCPlayerMgr.Inst.m_index = CCPlayerMgr.Inst.m_index + 1;
end

function LuaYuanXiao2023Mgr:QHLanternShowZhiBo_ZhiBoEnd()
    self:StopZhiBo()
end

function LuaYuanXiao2023Mgr:LeavePartyScene()
    self:RemoveListener()
    self:OnSoundEnableChanged()--重新设置CC直播是否开启语音

    self:OnPlayerMove()

    if self.ZhiBoObj then 
        self.ZhiBoObj.RO:Destroy()
    end 
    self.ZhiBoObj = nil

    CCPlayerMgr.Inst:Unlock()
    if CCPlayer.Inst:IsPlaying() then
        CCPlayerMgr.Inst:Stop()
    end

    CClientCCVideoObj.Inited = false
    self.IsPlaying = false
    SoundManager.Inst:StartBGMusic()
end

function LuaYuanXiao2023Mgr:OnSoundEnableChanged()
    local setting = PlayerSettings.SoundEnabled
    if not CCPlayer.Inst:IsPlaying() then return end
    
    if self.CameraLock then
        CCPlayer.Inst:MuteAudio(false)
    else
        CCPlayer.Inst:MuteAudio(not setting)
    end
end

function LuaYuanXiao2023Mgr:OnPlayerMove()  
    self.ShowHideUI(true)
    CameraFollow.Inst.MaxSpHeight = self.PreMaxSpHeight
    local t = CameraFollow.Inst.TValue
    if  t <= NormalCamera.s_HideUINearBar or t > NormalCamera.m_HideUIFarBar then 
        return 
    end
    self:ExitCameraLock()
end

function LuaYuanXiao2023Mgr:ExitCameraLock()
    if self.CameraLock and CClientMainPlayer.Inst then
        CameraFollow.Inst:SetFollowObj(CClientMainPlayer.Inst.RO, nil)
        CameraFollow.Inst:EnableZoom(true)
        --LuaFireWorkPartyMgr.ShowHideUI(true)
        self.CameraLock = false
        self:OnSoundEnableChanged()
    end
end

function LuaYuanXiao2023Mgr:OnRenderSceneInit()
    if self.CurPartyId then
        local party = YuanXiao2023_LanternShowSchedule.GetData(self.CurPartyId)
        if party then
            local partySceneId = party.MapId
            if CScene.MainScene and CScene.MainScene.SceneTemplateId == partySceneId then
                return
            end
        end
    end

    self:LeavePartyScene()
end

function LuaYuanXiao2023Mgr:SetUpCamera()
    CameraFollow.Inst:ResetToDefault(true,false)
    self.CameraLock = true
    self:OnSoundEnableChanged()
    --停止寻路
    Gas2Gac.TryStopTrack()
end

--#endregion


--#region 元宵放灯

LuaYuanXiao2023Mgr.fangDengFxList = {}
LuaYuanXiao2023Mgr.fangDengMatList = {}
LuaYuanXiao2023Mgr.FangDeng = true

function LuaYuanXiao2023Mgr:ShowPlayListWnd()
    local player = CClientMainPlayer.Inst
    if not player then
        return
    end

    CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("选择对象")
    CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("选择")
    CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
    CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

    -- 先加自己
    local data = CreateFromClass(CCommonPlayerDisplayData, player.Id, "[00FF2B]"..player.Name.."[-]", player.Level, player.Class, player.Gender, player.BasicProp.Expression, true)
    CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)

    -- 再加在线好友
    local friendlinessTbl = {}
    CommonDefs.DictIterate(player.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
        if CIMMgr.Inst:IsOnline(k) and CIMMgr.Inst:IsSameServerFriend(k) then
            table.insert(friendlinessTbl, {k, v.Friendliness})
        end
    end))
    table.sort(friendlinessTbl, function(v1, v2) return v1[2] > v2[2] end)

    for _, info in pairs(friendlinessTbl) do
        local basicInfo = CIMMgr.Inst:GetBasicInfo(info[1])
        if basicInfo then
            local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
            CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
        end
    end

    CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(function(playerId)
        g_ScriptEvent:BroadcastInLua("YuanXiao2020_SelectPlayer", playerId)
        CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
    end)
    CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function LuaYuanXiao2023Mgr:RequestFangDeng(SelectedTargetPlayerId, SelectTargetName, SelectedTargetFangDengIndex, isUseVoucher, wishMsg)
    CUIManager.CloseUI(CLuaUIResources.YuanXiao2023QiYuanWnd)
    Gac2Gas.YuanXiaoLanternSendLantern(SelectedTargetPlayerId, SelectTargetName, SelectedTargetFangDengIndex, isUseVoucher, wishMsg)
end

function LuaYuanXiao2023Mgr:TryOpenFangDeng3D()
    --local CameraView = YuanXiao_LanternSetting.GetData().CameraView
    --local r, z, y = CameraView[0], CameraView[1], CameraView[2]
    --local vec3 = Vector3(r, z, y)
    if CameraFollow.Inst.CurMode ~= CameraMode.E3DPlus then
        local message = g_MessageMgr:FormatMessage("YuanXiao2020_3DPlus_Confirm")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
            CameraFollow.Inst:SetMode(CameraMode.E3DPlus)
            PlayerSettings.Saved3DMode = 2
            --CameraFollow.Inst.targetRZY = vec3
            --CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        return
    end
    --CameraFollow.Inst.targetRZY = vec3
    --CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function LuaYuanXiao2023Mgr:TryTrackOpenFangDeng3D(sceneId, sceneTemplateId, x, y)
    if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("TeamFollow")) == 1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("在组队跟随状态下不能这样做"))
        return
    end
    local pixelPos = Utility.GridPos2PixelPos(CPos(x, y))
    CTrackMgr.Inst:Track(sceneId, sceneTemplateId, pixelPos, 0, DelegateFactory.Action(function()
        self:TryOpenFangDeng3D()
    end), nil, nil, nil, false)
end

function LuaYuanXiao2023Mgr:SetFangDengShader(mat)
    if Main.Inst.EngineVersion >= ShaderEx.s_MinVersion or Main.Inst.EngineVersion == -1 then
        mat.shader = ShaderEx.Find("L10/FX/Additive")
    end
end

function LuaYuanXiao2023Mgr:ResetFangDengShader(mat)
    if Main.Inst.EngineVersion >= ShaderEx.s_MinVersion or Main.Inst.EngineVersion == -1 then
        mat.shader = ShaderEx.Find("L10/FX/Additive")
        mat:SetColor("_Color", Color.white)
    end
end

-- 开启FadeOut特效
function LuaYuanXiao2023Mgr:FadeOutFangDeng(kind, releaseAfterDestory)
    if self.fangDengMatList[kind]~=nil and #self.fangDengFxList[kind]>0  and (Main.Inst.EngineVersion >= ShaderEx.s_MinVersion or Main.Inst.EngineVersion == -1) then
        local amount = 1
        local tick
        local color = Color.white
        for i=1,#self.fangDengMatList[kind] do
            LuaYuanXiao2023Mgr:SetFangDengShader(self.fangDengMatList[kind][i])
        end
        tick = CTickMgr.Register(DelegateFactory.Action(function ()
            amount = amount - 0.01
            if amount<=0 then
                for i=1,#self.fangDengMatList[kind] do
                    LuaYuanXiao2023Mgr:ResetFangDengShader(self.fangDengMatList[kind][i])
                end
                -- 消失完之后，销毁之前的灯笼
                self:DestoryLantern(kind)
                if releaseAfterDestory then
                    self:ReleaseLantern(kind)
                end
                tick:Invoke()
            else
                for i=1,#self.fangDengMatList[kind] do
                    color.a = amount
                    self.fangDengMatList[kind][i]:SetColor("_Color", color)
                end
            end
        end), 50, ETickType.Loop)
    else
        self:DestoryLantern(kind)
        if releaseAfterDestory then
            self:ReleaseLantern(kind)
        end
    end
end

-- 销毁灯笼 并清空数组
function LuaYuanXiao2023Mgr:DestoryLantern(kind)
    for k, v in pairs(self.fangDengFxList) do
        if (kind == nil) or (k == kind) then
            for _, fx in pairs(v) do
                fx:Destroy()
            end
            self.fangDengFxList[k] = nil
        end
    end
end

-- kind表示种类 =nil时代表全部种类
function LuaYuanXiao2023Mgr:StopFangDeng(kind, releaseAfterDestory)
    for k, v in pairs(self.fangDengFxList) do
        if (kind == nil) or (k == kind) then
            LuaYuanXiao2023Mgr:FadeOutFangDeng(k, releaseAfterDestory)
        end
    end
end

function LuaYuanXiao2023Mgr:StartFangDeng(kind)
    -- 判断当前是否有灯存在
    -- 只消除同种的灯笼
    if self.fangDengFxList[kind] and #self.fangDengFxList[kind] >0 then
        self:StopFangDeng(kind, true)
    else
        LuaYuanXiao2023Mgr:LoadMat(kind)
        LuaYuanXiao2023Mgr:ReleaseLantern(kind)
    end
end

function LuaYuanXiao2023Mgr:LoadMat(kind)
    if self.fangDengMatList[kind]~= nil then
        return
    end

    local f = DelegateFactory.Action_Material(function (mat)
        if mat then
            LuaYuanXiao2023Mgr:ResetFangDengShader(mat)
            table.insert(self.fangDengMatList[kind], mat)
        end
    end)

    if kind == 1 then
        self.fangDengMatList[kind] = {}
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/lnpc979_01_alpha01.mat", f)
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/lnpc979_01_body01.mat", f)
        --CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/a_point201a_ad02.mat", f)
        --CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/effect_glow_effect_jn_qwq_03.mat", f)
    elseif kind == 2 then
        self.fangDengMatList[kind] = {}
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/lnpc979_02_alpha01.mat", f)
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/lnpc979_02_body01.mat", f)
        --CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/a_point201a_ad02.mat", f)
        --CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/effect_glow_effect_jn_qwq_03.mat", f)
    elseif kind == 3 then
        self.fangDengMatList[kind] = {}
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/lnpc979_03.mat", f)
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/lnpc979_04.mat", f)
        CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/lnpc979_05.mat", f)
        --CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/a_point201a_ad02.mat", f)
        --CSharpResourceLoader.Inst:LoadMaterial("Assets/Res/Fx/Materials/effect_glow_effect_jn_qwq_03.mat", f)
    end
end

function LuaYuanXiao2023Mgr:ReleaseLantern(kind)
    self.fangDengFxList[kind] = {}
    local lanternType = YuanXiao_LanternType.GetData(kind)
    local len = lanternType.Pos.Length
    for i = 0,len - 1,2 do
        local pos, fxId = CPos(lanternType.Pos[i] * 64,lanternType.Pos[i + 1] * 64), lanternType.FxId
        local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
        table.insert(self.fangDengFxList[kind], fx)
    end
end

--#endregion
