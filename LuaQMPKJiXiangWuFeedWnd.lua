local CItemMgr=import "L10.Game.CItemMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUILongPressButton=import "L10.UI.CUILongPressButton"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CUITexture=import "L10.UI.CUITexture"
local UIScrollView=import "UIScrollView"
local Item_Item=import "L10.Game.Item_Item"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local UIScrollViewIndicator=import "UIScrollViewIndicator"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CItem=import "L10.Game.CItem"

CLuaQMPKJiXiangWuFeedWnd = class()
RegistClassMember(CLuaQMPKJiXiangWuFeedWnd,"m_Template")
CLuaQMPKJiXiangWuFeedWnd.s_ItemId = 0
CLuaQMPKJiXiangWuFeedWnd.s_Infos = nil
CLuaQMPKJiXiangWuFeedWnd.s_TargetTransform = nil

function CLuaQMPKJiXiangWuFeedWnd:Init()
    self.m_Template=FindChild(self.transform,"Template").gameObject
    self.m_Template:SetActive(false)

    self.m_Template:AddComponent(typeof(CUILongPressButton))
    local cmp=self.m_Template:AddComponent(typeof(CItemCountUpdate))
    cmp.countLabel=self.m_Template.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))

    local button = FindChild(self.transform,"Button").gameObject
    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
        --一键喂食
        local msg = g_MessageMgr:FormatMessage("QMPK_JIXIANGWU_YIJIANWEISHI")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
            function()
                local itemInfo = CItemMgr.Inst:GetItemInfo(CLuaQMPKJiXiangWuFeedWnd.s_ItemId)
                if itemInfo then
                    Gac2Gas.JiXiangWuRequestTrainOneKey(EnumToInt(itemInfo.place), itemInfo.pos, CLuaQMPKJiXiangWuFeedWnd.s_ItemId)
                end
            end
        ), nil, nil,nil, false)
        CUIManager.CloseUI(CLuaUIResources.QMPKJiXiangWuFeedWnd)
    end)


    local grid=FindChild(self.transform,"Grid")
    for i,v in ipairs(CLuaQMPKJiXiangWuFeedWnd.s_Infos) do
        local go=NGUITools.AddChild(grid.gameObject,self.m_Template)
        go:SetActive(true)
        self:InitItem(go,v,i)
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

    local panel=FindChild(self.transform,"ScrollView"):GetComponent(typeof(UIPanel))
    panel:GetComponent(typeof(UIScrollView)).mCalculatedBounds=false
    
    local bg=FindChild(self.transform,"BackGround"):GetComponent(typeof(UISprite))
	bg.height = math.min(650, NGUIMath.CalculateRelativeWidgetBounds(grid).size.y + math.abs(panel.topAnchor.absolute) + math.abs(panel.bottomAnchor.absolute)+20)

    local bounds=NGUIMath.CalculateRelativeWidgetBounds(CLuaQMPKJiXiangWuFeedWnd.s_TargetTransform)
    local size=bounds.size
	local newNGUIWorldCenter = CUICommonDef.AdjustPosition(AlignType.Top, bg.width, bg.height, CLuaQMPKJiXiangWuFeedWnd.s_TargetTransform.position, size.x,size.y)
    bg.transform.localPosition = newNGUIWorldCenter

    local indicator=FindChild(self.transform,"ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    indicator:Layout()
end

function CLuaQMPKJiXiangWuFeedWnd:InitItem(go,data,index)
    local tf=go.transform
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
    local templateId=data.itemTemplateId
    local itemTemplateData = Item_Item.GetData(templateId)
    icon:LoadMaterial(itemTemplateData.Icon)

    local mask=tf:Find("Mask").gameObject
    mask:SetActive(false)

    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel=tf:Find("DescLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=itemTemplateData.Name
    
    descLabel.text= data.desc and data.desc or CItem.GetItemDescription(templateId,true)

    local countUpdate=go:GetComponent(typeof(CItemCountUpdate))
    countUpdate.templateId=templateId
    countUpdate.format="{0}"

    local function OnCountChange(val)
        if val>=1 then
            mask:SetActive(false)
            countUpdate.format="{0}"
        else
            mask:SetActive(true)
            countUpdate.format="[ff0000]{0}[-]"
        end
    end
    countUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    countUpdate:UpdateCount()

    local function longPressCallback(go)
        if countUpdate.count==0 then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, go.transform, CTooltipAlignType.Left)
        elseif countUpdate.count>0 then
            local info = CLuaQMPKJiXiangWuFeedWnd.s_Infos[index]
            local itemInfo = CItemMgr.Inst:GetItemInfo(CLuaQMPKJiXiangWuFeedWnd.s_ItemId)
            if itemInfo then
                Gac2Gas.JiXiangWuRequestTrain(EnumToInt(itemInfo.place), itemInfo.pos, CLuaQMPKJiXiangWuFeedWnd.s_ItemId, info.itemTemplateId)
            end
        end
    end
    local longPressButton=go:GetComponent(typeof(CUILongPressButton))
    longPressButton.longPressShift = true
    longPressButton.longPressShiftDuration = 3
    longPressButton.minDelta = 1.0 / 30
    longPressButton.callback=DelegateFactory.Action_GameObject(longPressCallback)
    
end
