-- Auto Generated!!
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_ZhushabiSearch = import "L10.UI.CPlayerShop_ZhushabiSearch"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local DelegateFactory = import "DelegateFactory"
local House_JiandingZhushabiIds = import "L10.Game.House_JiandingZhushabiIds"
local Int32 = import "System.Int32"
local ItemSearchParams = import "L10.UI.ItemSearchParams"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UInt32 = import "System.UInt32"
CPlayerShop_ZhushabiSearch.m_OnEnable_CS2LuaHook = function (this) 
    this.m_LevelButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_LevelButton.OnClick, MakeDelegateFromCSFunction(this.OnLevelButtonOnClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_TypeButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TypeButton.OnClick, MakeDelegateFromCSFunction(this.OnTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_StarWithSubTypeButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_StarWithSubTypeButton.OnClick, MakeDelegateFromCSFunction(this.OnStarWithSubTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_StarButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_StarButton.OnClick, MakeDelegateFromCSFunction(this.OnStarButtonOnClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_SubTypeButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SubTypeButton.OnClick, MakeDelegateFromCSFunction(this.OnSubTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_SearchButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SearchButton.OnClick, MakeDelegateFromCSFunction(this.OnSearchButtonOnClick, MakeGenericClass(Action1, QnButton), this), true)
end
CPlayerShop_ZhushabiSearch.m_OnDisable_CS2LuaHook = function (this) 
    this.m_LevelButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_LevelButton.OnClick, MakeDelegateFromCSFunction(this.OnLevelButtonOnClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_TypeButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TypeButton.OnClick, MakeDelegateFromCSFunction(this.OnTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_StarWithSubTypeButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_StarWithSubTypeButton.OnClick, MakeDelegateFromCSFunction(this.OnStarWithSubTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_StarButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_StarButton.OnClick, MakeDelegateFromCSFunction(this.OnStarButtonOnClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_SubTypeButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SubTypeButton.OnClick, MakeDelegateFromCSFunction(this.OnSubTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_SearchButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SearchButton.OnClick, MakeDelegateFromCSFunction(this.OnSearchButtonOnClick, MakeGenericClass(Action1, QnButton), this), false)
end
CPlayerShop_ZhushabiSearch.m_ReloadLevelData_CS2LuaHook = function (this)
    this.m_LevelActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetZhushabiLevelNames(), MakeDelegateFromCSFunction(this.LevelAction, MakeGenericClass(Action1, Int32), this))

    if this.m_LevelActions.Count > 0 then
        this.SelectLevel = math.min(math.max(this.SelectLevel, 0), this.m_LevelActions.Count - 1)
        this.m_LevelButton.Text = this.m_LevelActions[this.SelectLevel].text
        this:ReloadTypeData()
    end
end
CPlayerShop_ZhushabiSearch.m_ReloadTypeData_CS2LuaHook = function (this)    this.m_TypeActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetZhushabiTypeNames(), MakeDelegateFromCSFunction(this.TypeAction, MakeGenericClass(Action1, Int32), this))

    if this.m_TypeActions.Count > 0 then
        this.SelectType = math.min(math.max(this.SelectType, 0), this.m_TypeActions.Count - 1)
        this.m_TypeButton.Text = this.m_TypeActions[this.SelectType].text

        local selectTypeStr = this.m_TypeActions[this.SelectType].text
        local selectedType = CPlayerShopMgr.Inst:GetZhushabiTypeByName(selectTypeStr)

        this:DisableDynamicBtn()
        this:ReloadStarData()
        this:ReloadSubTypeData()
        if selectedType == EChuanjiabaoType_lua.RenGe then
            this.m_SubTypeGO:SetActive(true)
            this.m_StarWithSubTypeGO:SetActive(true)
        else
            this.m_StarGO:SetActive(true)
        end
    end
end
CPlayerShop_ZhushabiSearch.m_ReloadSubTypeData_CS2LuaHook = function (this)    this.m_SubTypeActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetZhushabiSubTypeNames(), MakeDelegateFromCSFunction(this.SubTypeAction, MakeGenericClass(Action1, Int32), this))

    if this.m_SubTypeActions.Count > 0 then
        this.SelectSubType = math.min(math.max(this.SelectSubType, 0), this.m_SubTypeActions.Count - 1)
        this.m_SubTypeButton.Text = this.m_SubTypeActions[this.SelectSubType].text
        this:ReloadStarData()
    end
end
CPlayerShop_ZhushabiSearch.m_ReloadStarData_CS2LuaHook = function (this)    this.m_StarActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetZhushabiStarNames(this.IsPublicity), MakeDelegateFromCSFunction(this.StarAction, MakeGenericClass(Action1, Int32), this))

    if this.m_StarActions.Count > 0 then
        this.SelectStar = math.min(math.max(this.SelectStar, 0), this.m_StarActions.Count - 1)
        this.m_StarButton.Text = this.m_StarActions[this.SelectStar].text
        this.m_StarWithSubTypeButton.Text = this.m_StarActions[this.SelectStar].text
    end

    this:RefreshCandidateList()
end
CPlayerShop_ZhushabiSearch.m_RefreshCandidateList_CS2LuaHook = function (this) 
    CommonDefs.DictClear(this.m_CurrentCandidateItems)

    if not this:IsSelectionLegal() then
        MessageWndManager.ShowOKMessage(LocalString.GetString("没有选择所有的朱砂笔分类,请重新选择！"), nil)
    end

    local levelStr = this.m_LevelActions[this.SelectLevel].text
    local levelKey = CPlayerShopMgr.Inst:GetZhushabiLevelByName(levelStr)

    if not CommonDefs.DictContains(House_JiandingZhushabiIds.GetData().AllZhushabiTempId, typeof(String), levelKey) then
        return
    end

    local allTypeIds = CommonDefs.DictGetValue(House_JiandingZhushabiIds.GetData().AllZhushabiTempId, typeof(String), levelKey).AllTypeZhushabiTempId

    local typeStr = this.m_TypeActions[this.SelectType].text
    local typeIdx = CPlayerShopMgr.Inst:GetZhushabiTypeByName(typeStr)

    if not CommonDefs.DictContains(allTypeIds, typeof(UInt32), typeIdx) then
        return
    end

    local allStarIds = CommonDefs.DictGetValue(allTypeIds, typeof(UInt32), typeIdx).AllStarZhushabiTempId
    local starStartNum = 4
    if this.IsPublicity then
        starStartNum = this.m_PulicitySearchStarNumStart
    end
    for starNum = starStartNum, 8 do
        local allSubTypeIds = CommonDefs.DictGetValue(allStarIds, typeof(UInt32), starNum).ZhushabiTempId

        local itemId = 0

        if typeIdx == EChuanjiabaoType_lua.RenGe then
            local subTypeStr = this.m_SubTypeActions[this.SelectSubType].text
            local subTypeIdx = CPlayerShopMgr.Inst:GetZhushabiSubTypeByName(subTypeStr)
            if not CommonDefs.DictContains(allSubTypeIds, typeof(UInt32), subTypeIdx) then
                return
            end
            itemId = CommonDefs.DictGetValue(allSubTypeIds, typeof(UInt32), subTypeIdx)
        else
            if not CommonDefs.DictContains(allSubTypeIds, typeof(UInt32), 0) then
                return
            end
            itemId = CommonDefs.DictGetValue(allSubTypeIds, typeof(UInt32), 0)
        end

        if itemId ~= 0 then
            CommonDefs.DictAdd(this.m_CurrentCandidateItems, typeof(UInt32), itemId, typeof(UInt32), starNum)
        end
    end
end
CPlayerShop_ZhushabiSearch.m_OnTypeButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_TypeActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_TypeActions.Count), nil, nil, DelegateFactory.Action(function () 
        this.m_TypeButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_ZhushabiSearch.m_OnStarWithSubTypeButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_StarActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_StarActions.Count), nil, CreateFromClass(ItemSearchParams, InitializeListWithCollection(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, this.m_CurrentCandidateItems.Keys), this.IsPublicity, this.option), DelegateFactory.Action(function () 
        this.m_StarWithSubTypeButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_ZhushabiSearch.m_OnStarButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_StarActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_StarActions.Count), nil, CreateFromClass(ItemSearchParams, InitializeListWithCollection(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, this.m_CurrentCandidateItems.Keys), this.IsPublicity, this.option), DelegateFactory.Action(function () 
        this.m_StarButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_ZhushabiSearch.m_OnSubTypeButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_SubTypeActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_StarActions.Count), nil, nil, DelegateFactory.Action(function () 
        this.m_SubTypeButton:SetTipStatus(false)
    end), 600,420)
end
CPlayerShop_ZhushabiSearch.m_IsSelectionLegal_CS2LuaHook = function (this) 
    if not (this.SelectLevel >= 0 and this.SelectLevel < this.m_LevelActions.Count) then
        return false
    end
    if not (this.SelectType >= 0 and this.SelectType < this.m_TypeActions.Count) then
        return false
    end
    local selectTypeStr = this.m_TypeActions[this.SelectType].text
    local selectTypeIdx = CPlayerShopMgr.Inst:GetZhushabiSubTypeByName(selectTypeStr)
    if selectTypeIdx == EChuanjiabaoType_lua.RenGe then
        if not (this.SelectSubType >= 0 and this.SelectSubType < this.m_SubTypeActions.Count) then
            return false
        end
    end
    if not (this.SelectStar >= 0 and this.SelectStar < this.m_StarActions.Count) then
        return false
    end
    return true
end
CPlayerShop_ZhushabiSearch.m_OnSearchButtonOnClick_CS2LuaHook = function (this, button) 
    if not this:IsSelectionLegal() then
        MessageWndManager.ShowOKMessage(LocalString.GetString("没有选择所有的朱砂笔分类,请重新选择！"), nil)
    end

    local levelStr = this.m_LevelActions[this.SelectLevel].text
    local levelKey = CPlayerShopMgr.Inst:GetZhushabiLevelByName(levelStr)

    if not CommonDefs.DictContains(House_JiandingZhushabiIds.GetData().AllZhushabiTempId, typeof(String), levelKey) then
        return
    end

    local allTypeIds = CommonDefs.DictGetValue(House_JiandingZhushabiIds.GetData().AllZhushabiTempId, typeof(String), levelKey).AllTypeZhushabiTempId

    local typeStr = this.m_TypeActions[this.SelectType].text
    local typeIdx = CPlayerShopMgr.Inst:GetZhushabiTypeByName(typeStr)

    if not CommonDefs.DictContains(allTypeIds, typeof(UInt32), typeIdx) then
        return
    end

    local allStarIds = CommonDefs.DictGetValue(allTypeIds, typeof(UInt32), typeIdx).AllStarZhushabiTempId

    local starStr = this.m_StarActions[this.SelectStar].text
    local starIdx = CPlayerShopMgr.Inst:GetZhushabiStarByName(starStr)

    if not CommonDefs.DictContains(allStarIds, typeof(UInt32), starIdx) then
        return
    end

    local allSubTypeIds = CommonDefs.DictGetValue(allStarIds, typeof(UInt32), starIdx).ZhushabiTempId

    local itemId = 0

    if typeIdx == EChuanjiabaoType_lua.RenGe then
        local subTypeStr = this.m_SubTypeActions[this.SelectSubType].text
        local subTypeIdx = CPlayerShopMgr.Inst:GetZhushabiSubTypeByName(subTypeStr)
        if not CommonDefs.DictContains(allSubTypeIds, typeof(UInt32), subTypeIdx) then
            return
        end
        itemId = CommonDefs.DictGetValue(allSubTypeIds, typeof(UInt32), subTypeIdx)
    else
        if not CommonDefs.DictContains(allSubTypeIds, typeof(UInt32), 0) then
            return
        end
        itemId = CommonDefs.DictGetValue(allSubTypeIds, typeof(UInt32), 0)
    end

    if itemId == 0 then
        return
    end

    GenericDelegateInvoke(this.OnSearchCallback, Table2ArrayWithCount({itemId, this.option, false}, 3, MakeArrayClass(Object)))
end
CPlayerShop_ZhushabiSearch.m_LevelAction_CS2LuaHook = function (this, row) 
    if this.m_LevelButton ~= nil and this.m_LevelActions ~= nil and this.m_LevelActions.Count > row then
        local levelName = this.m_LevelActions[row].text
        this.SelectLevel = row
        this.m_LevelButton.Text = levelName
        this:RefreshCandidateList()
    end
end
CPlayerShop_ZhushabiSearch.m_TypeAction_CS2LuaHook = function (this, row) 
    local selectedStr = ""
    if this.m_TypeButton ~= nil and this.m_TypeActions ~= nil and this.m_TypeActions.Count > row then
        local typeStr = this.m_TypeActions[row].text
        this.SelectType = row
        this.m_TypeButton.Text = typeStr
        selectedStr = typeStr
    end

    this:DisableDynamicBtn()

    local selectedType = CPlayerShopMgr.Inst:GetZhushabiTypeByName(selectedStr)
    if selectedType == EChuanjiabaoType_lua.RenGe then
        this.m_SubTypeGO:SetActive(true)
        this.m_StarWithSubTypeGO:SetActive(true)
    else
        this.m_StarGO:SetActive(true)
    end
    this:RefreshCandidateList()
end
CPlayerShop_ZhushabiSearch.m_SubTypeAction_CS2LuaHook = function (this, row) 
    if this.m_SubTypeButton ~= nil and this.m_SubTypeActions ~= nil and this.m_SubTypeActions.Count > row then
        local subTypeName = this.m_SubTypeActions[row].text
        this.SelectSubType = row
        this.m_SubTypeButton.Text = subTypeName
        this:RefreshCandidateList()
    end
end
CPlayerShop_ZhushabiSearch.m_StarAction_CS2LuaHook = function (this, row) 
    if this.m_StarButton ~= nil and this.m_StarActions ~= nil and this.m_StarActions.Count > row then
        local starName = this.m_StarActions[row].text
        this.SelectStar = row
        this.m_StarButton.Text = starName
        this.m_StarWithSubTypeButton.Text = starName
    end
end
CPlayerShop_ZhushabiSearch.m_GetColumnCount_CS2LuaHook = function (this, count) 
    local columns = 1
    if count >= 6 then
        columns = 2
    end
    return columns
end
