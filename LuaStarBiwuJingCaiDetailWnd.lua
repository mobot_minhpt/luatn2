local UILabel = import "UILabel"
local NGUIText = import "NGUIText"
local Color = import "UnityEngine.Color"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local AlignType = import "L10.UI.CTooltip.AlignType"
local WndType = import "L10.UI.WndType"
LuaStarBiwuJingCaiDetailWnd=class()
LuaStarBiwuJingCaiDetailWnd.Path = "ui/starbiwu/LuaStarBiwuJingCaiDetailWnd"
RegistChildComponent(LuaStarBiwuJingCaiDetailWnd,"m_HuiBaoBeiShuLabel","HuiBaoBeiShuLabel", UILabel)

RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_TitleLabel")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_Resultabel")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_RecordTable")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_CurrentIndex")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_jingcaiItemList")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_selfChoiceList")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_rightChoiceList")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_LeftArrow")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd,"m_RightArrow")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd, "m_PhaseBetPool")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd, "m_Choice2BetList")

RegistClassMember(LuaStarBiwuJingCaiDetailWnd, "m_TimeLabel")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd, "m_StageLabel")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd, "m_VSLabel")
RegistClassMember(LuaStarBiwuJingCaiDetailWnd, "m_VSRootObj")

function LuaStarBiwuJingCaiDetailWnd:Awake()
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_Resultabel = self.transform:Find("ShowArea/Label"):GetComponent(typeof(UILabel))
    self.m_LeftArrow = self.transform:Find("ShowArea/LeftArrow").gameObject
    self.m_RightArrow = self.transform:Find("ShowArea/RightArrow").gameObject

    self.m_TimeLabel = self.transform:Find("ShowArea/Time/Label"):GetComponent(typeof(UILabel))
    self.m_StageLabel = self.transform:Find("ShowArea/Progress/Label"):GetComponent(typeof(UILabel))
    self.m_VSRootObj = self.transform:Find("ShowArea/VS").gameObject
    self.m_VSLabel = self.m_VSRootObj.transform:Find("Label"):GetComponent(typeof(UILabel))

    self.m_RecordTable = CLuaStarBiwuMgr.JingCaiRecordTable
    self.m_CurrentIndex = CLuaStarBiwuMgr.JingCaiRecordCurrentIndex
    self:RefreshData()

    UIEventListener.Get(self.m_LeftArrow).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_CurrentIndex > 1 then
            self.m_CurrentIndex = self.m_CurrentIndex - 1
            self:RefreshData()
        end
    end)
    UIEventListener.Get(self.m_RightArrow).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_CurrentIndex < #self.m_RecordTable then
            self.m_CurrentIndex = self.m_CurrentIndex + 1
            self:RefreshData()
        end
    end)
end

function LuaStarBiwuJingCaiDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("StarBiwuSendJingCaiRecordDetail", self, "StarBiwuSendJingCaiRecordDetail")
end

function LuaStarBiwuJingCaiDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("StarBiwuSendJingCaiRecordDetail", self, "StarBiwuSendJingCaiRecordDetail")
end

function LuaStarBiwuJingCaiDetailWnd:RefreshData()
    Gac2Gas.StarBiwuQueryJingCaiRecordDetail(self.m_RecordTable[self.m_CurrentIndex].jingcaiPhaseNum,self.m_RecordTable[self.m_CurrentIndex].jingcaiBetTime,self.m_RecordTable[self.m_CurrentIndex].recordIdx)
end

function LuaStarBiwuJingCaiDetailWnd:StarBiwuSendJingCaiRecordDetail(jingcaiItemList, selfChoiceList,rightChoiceList, jingcaiStage, targetDate, matchInfoList, phaseBetPool, choice2BetList)
    self.m_jingcaiItemList = jingcaiItemList
    self.m_selfChoiceList = selfChoiceList
    self.m_rightChoiceList = rightChoiceList
    self.m_PhaseBetPool = phaseBetPool
    self.m_Choice2BetList = choice2BetList
    self:UpdatePanel(matchInfoList)

    -- update the top info
    self.m_TimeLabel.text = targetDate
    local stage = StarBiWuShow_Stage.GetData(jingcaiStage)
    if stage then
        self.m_StageLabel.text = StarBiWuShow_Stage.GetData(jingcaiStage).StageName
    else
        stage = StarBiWuShow_FinalStage.GetData(jingcaiStage)
        if stage then 
            self.m_StageLabel.text = StarBiWuShow_FinalStage.GetData(jingcaiStage).StageName
        else
            self.m_StageLabel.text = nil
        end
    end
    --self.m_VSRootObj:SetActive(jingcaiStage == 3)
    --if jingcaiStage == 3 then
    --    if matchInfoList[0] == "" or matchInfoList[1] == "" then
    --        self.m_VSLabel.text = LocalString.GetString("无")
    --    else
    --        self.m_VSLabel.text = SafeStringFormat3(LocalString.GetString("%s[ffed5f]VS[-]%s"), matchInfoList[0], matchInfoList[1])
    --    end
    --end
end

function LuaStarBiwuJingCaiDetailWnd:UpdatePanel(mathinfo)
    self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("第%s次竞猜详情"), self.m_RecordTable[self.m_CurrentIndex].recordIdx)

    local resultColor = NGUIText.ParseColor24("00ff60", 0)
    local resultStr = LocalString.GetString("本次竞猜成功")
    local resultTable = {}
    local showHuiBaoBeiShu = true
    for i=1,self.m_jingcaiItemList.Count do
        local Content = self.transform:Find("ShowArea/ContentRoot/Content"..i)
        local jingcaiText = StarBiWuShow_JingCai.GetData(tonumber(self.m_jingcaiItemList[i-1]))
        local name1 = mathinfo and mathinfo[i] and mathinfo[i][1] or ""
        local name2 = mathinfo and mathinfo[i] and mathinfo[i][2] or ""
        local defaultValue = jingcaiText.DefaultValue
        if System.String.IsNullOrEmpty(name1) then
            name1 = defaultValue and defaultValue.Length >=1 and  defaultValue[0] or ""
        end
        if System.String.IsNullOrEmpty(name2) then
            name2 = defaultValue and defaultValue.Length >=2 and  defaultValue[1] or ""
        end
        if jingcaiText.ID > 24 then
            Content:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1,  name2)
        elseif not System.String.IsNullOrEmpty(jingcaiText.Threshold) then
            Content:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1, tostring(jingcaiText.Threshold))
        else
            Content:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString(jingcaiText.Message), name1)
        end
        local Y_N = LocalString.GetString("是")
        if tonumber(self.m_selfChoiceList[i-1]) == 0 then
            Y_N = LocalString.GetString("否")
        elseif tonumber(self.m_selfChoiceList[i - 1]) == 2 then
            Y_N = LocalString.GetString("不选")
            showHuiBaoBeiShu = false
        end
        Content:Find("Result"):GetComponent(typeof(UILabel)).text = Y_N
        local color = NGUIText.ParseColor24("acf8ff", 0)
        if self.m_RecordTable[self.m_CurrentIndex].bJingcaiRes then
            if tonumber(self.m_selfChoiceList[i - 1]) == 2 or tonumber(self.m_selfChoiceList[i-1]) == tonumber(self.m_rightChoiceList[i-1]) then
                color = NGUIText.ParseColor24("00ff60", 0)
            else
                color = NGUIText.ParseColor24("ff5050", 0)
                resultColor = NGUIText.ParseColor24("ff5050", 0)
                resultStr = LocalString.GetString("本次竞猜失败")
            end
        else
            resultColor = Color.yellow
            resultStr = LocalString.GetString("本次竞猜结果尚未揭晓")
        end
        table.insert(resultTable, self.m_selfChoiceList[i - 1])
        Content:Find("Result"):GetComponent(typeof(UILabel)).color = color
        local btn = Content.transform:Find("DetailBtn")
        btn.gameObject:SetActive(false)
        if not System.String.IsNullOrEmpty(jingcaiText.Tip) then
            btn.gameObject:SetActive(true)
            UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                self:ShowTipMessage(go,jingcaiText.Tip)
            end)
        end
    end
    self.m_HuiBaoBeiShuLabel.text = showHuiBaoBeiShu and CLuaStarBiwuMgr:GetHuiBaoBeiShu(self.m_jingcaiItemList.Count, resultTable, self.m_Choice2BetList, self.m_PhaseBetPool) or ""
    self.m_Resultabel.text = resultStr
    self.m_Resultabel.color = resultColor
end
function LuaStarBiwuJingCaiDetailWnd:ShowTipMessage(btn,msg)
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info then
        CMessageTipMgr.Inst:Display(WndType.Tooltip, false, "", info.paragraphs, 640, btn.transform, AlignType.Top, 4294967295)
    end
end