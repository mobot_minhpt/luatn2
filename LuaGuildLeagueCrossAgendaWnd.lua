local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local UILabel = import "UILabel"
local UITable = import "UITable"
local UITabBar = import "L10.UI.UITabBar"
local Extensions = import "Extensions"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumDayName = import "L10.Game.EnumDayName"

LuaGuildLeagueCrossAgendaWnd = class()

RegistClassMember(LuaGuildLeagueCrossAgendaWnd, "m_RuleView")     --规则介绍
RegistClassMember(LuaGuildLeagueCrossAgendaWnd, "m_AgendaView")   --赛程安排
RegistClassMember(LuaGuildLeagueCrossAgendaWnd, "m_TabBar")       --子页面切换TabBar
RegistClassMember(LuaGuildLeagueCrossAgendaWnd, "m_TitleLabel")   --标题

function LuaGuildLeagueCrossAgendaWnd:Awake()
    self.m_RuleView = self.transform:Find("ShowArea/RuleView").gameObject
    self.m_AgendaView = self.transform:Find("ShowArea/AgendaView").gameObject
    self.m_TabBar = self.transform:GetComponent(typeof(UITabBar))
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))

    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTabChange(go, index)
    end), true)
end

function LuaGuildLeagueCrossAgendaWnd:Init()
    --激活以便初始化
    self.m_RuleView:SetActive(true)
    self.m_AgendaView:SetActive(true)
    self:InitRuleView()
    self:InitAgendaView()
    self.m_TabBar:ChangeTab(0, false)
end

function LuaGuildLeagueCrossAgendaWnd:OnTabChange(go, index)
    self.m_TitleLabel.text = string.gsub(CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel)).text, "\n", "")
    self.m_RuleView:SetActive(index==0)
    self.m_AgendaView.gameObject:SetActive(index==1)
end

function LuaGuildLeagueCrossAgendaWnd:InitRuleView()
    local ruleTable = self.m_RuleView.transform:Find("Scrollview/Table"):GetComponent(typeof(UITable))
    local ruleItem = self.m_RuleView.transform:Find("Item").gameObject
    ruleItem:SetActive(false)

    Extensions.RemoveAllChildren(ruleTable.transform)
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(g_MessageMgr:FormatMessage("Guild_League_Cross_Rule_Description"))
    if info == nil then
        return
    end

    for i=0, info.paragraphs.Count-1 do
        local child = CUICommonDef.AddChild(ruleTable.gameObject, ruleItem)
        child:SetActive(true)
        child.transform:Find("Sprite/Title"):GetComponent(typeof(UILabel)).text= info.paragraph_names[i]
        child.transform:Find("Content"):GetComponent(typeof(UILabel)).text= info.paragraphs[i]
    end
    
    ruleTable:Reposition()
end

function LuaGuildLeagueCrossAgendaWnd:InitAgendaView()
    local tbl = {}
    local groupTable = self.m_AgendaView.transform:Find("Group/Table") --小组赛
    for i=0, groupTable.childCount-1 do
        local child = groupTable:GetChild(i)
        table.insert(tbl, child)
    end
    local rankTable = self.m_AgendaView.transform:Find("Rank/Table") --排位赛
    for i=0, rankTable.childCount-1 do
        local child = rankTable:GetChild(i)
        table.insert(tbl, child)
    end
    local finalTable = self.m_AgendaView.transform:Find("Final/Table") --排位赛
    for i=0, finalTable.childCount-1 do
        local child = finalTable:GetChild(i)
        table.insert(tbl, child)
    end

    for i=1,#tbl do
        local nameLabel = tbl[i]:Find("NameLabel"):GetComponent(typeof(UILabel))
        local timeLabel = tbl[i]:Find("TimeLabel"):GetComponent(typeof(UILabel))
        local data = GuildLeagueCross_Agenda.GetData(i)
        if data then
            local beginTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.BeginTime)
            local enterTimestamp = beginTimestamp - GuildLeagueCross_Setting.GetData().PrepareTime
            local beginTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp)
            local enterTime = CServerTimeMgr.ConvertTimeStampToZone8Time(enterTimestamp)
            local weekdayStr = EnumDayName.GetDay(beginTime.DayOfWeek)
            if data.Type == 3 then
                nameLabel.text = weekdayStr
            else 
                nameLabel.text = SafeStringFormat3(LocalString.GetString("第%d轮（%s）"), data.Round, weekdayStr)
            end
            timeLabel.text = ToStringWrap(enterTime, LocalString.GetString("MM月dd日 HH:mm ")) .. LocalString.GetString("入场；") ..ToStringWrap(beginTime, LocalString.GetString("HH:mm ")) .. LocalString.GetString("开赛")
        end
    end
end
