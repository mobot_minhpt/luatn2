-- Auto Generated!!
local CIMMgr = import "L10.Game.CIMMgr"
local CInviteFanyuFriendItem = import "L10.UI.CInviteFanyuFriendItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
local Object = import "System.Object"
CInviteFanyuFriendItem.m_Init_CS2LuaHook = function (this, playerId, act) 
    this.actBtnClick = act
    this.pid = playerId

    local info = CIMMgr.Inst:GetBasicInfo(this.pid)
    if info ~= nil then
        this.nameLabel.text = info.Name

        this.groupLabel.text = CIMMgr.Inst:GetFriendGroupName(CIMMgr.Inst:GetFriendGroup(playerId))
        this.levelLabel.text = tostring(info.Level)
        this.friendInessLabel.text = System.String.Format(LocalString.GetString("友好度:{0}"), CIMMgr.Inst:GetFriendliness(playerId))
        local pname = CUICommonDef.GetPortraitName(info.Class, info.Gender, info.Expression)
        this.portraitTexture:LoadNPCPortrait(pname, false)
    end
end
CInviteFanyuFriendItem.m_OnClickInviteBtn_CS2LuaHook = function (this, go) 
    if this.actBtnClick ~= nil then
        GenericDelegateInvoke(this.actBtnClick, Table2ArrayWithCount({this.pid}, 1, MakeArrayClass(Object)))
    end
end
