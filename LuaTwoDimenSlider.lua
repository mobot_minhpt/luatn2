local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaTwoDimenSlider = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTwoDimenSlider, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaTwoDimenSlider, "Thumb", "Thumb", GameObject)
RegistChildComponent(LuaTwoDimenSlider, "CoordinateAxisBg", "CoordinateAxisBg", UIWidget)
RegistChildComponent(LuaTwoDimenSlider, "Label1", "Label1", UILabel)
RegistChildComponent(LuaTwoDimenSlider, "Label2", "Label2", UILabel)
RegistChildComponent(LuaTwoDimenSlider, "Label3", "Label3", UILabel)
RegistChildComponent(LuaTwoDimenSlider, "Label4", "Label4", UILabel)
RegistChildComponent(LuaTwoDimenSlider, "ResetXYBtn", "ResetXYBtn", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaTwoDimenSlider, "m_IsInit")
RegistClassMember(LuaTwoDimenSlider, "m_IsDragging")
RegistClassMember(LuaTwoDimenSlider, "m_HalfCoordinateAxisLength")
RegistClassMember(LuaTwoDimenSlider, "m_OnValueChangedAction")
RegistClassMember(LuaTwoDimenSlider, "m_OnDragStartAction")
RegistClassMember(LuaTwoDimenSlider, "m_OnDragEndAction")
RegistClassMember(LuaTwoDimenSlider, "m_LastPos")
RegistClassMember(LuaTwoDimenSlider, "m_CurXValue")
RegistClassMember(LuaTwoDimenSlider, "m_CurYValue")
RegistClassMember(LuaTwoDimenSlider, "m_OnResetXYBtnClickedAction")

function LuaTwoDimenSlider:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.CoordinateAxisBg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCoordinateAxisBgClick()
	end)


    --@endregion EventBind end
	UIEventListener.Get(self.Thumb).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragThumbStart(go)
    end)
    
    UIEventListener.Get(self.Thumb).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragThumbEnd(go)
    end)

	UIEventListener.Get(self.ResetXYBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResetXYBtnClick()
	end)
end

--@region Public
function LuaTwoDimenSlider:Init(onValueChangedAction, onDragStartAction, onDragEndAction)
    self.m_IsDragging = false
    self.m_IsInit = true
	self.m_HalfCoordinateAxisLength = self.CoordinateAxisBg.width * 0.5
	self.m_OnValueChangedAction = onValueChangedAction
    self.m_OnDragStartAction = onDragStartAction
    self.m_OnDragEndAction = onDragEndAction
end

function LuaTwoDimenSlider:SetLabels(title, desArr)
    if title and self.TitleLabel then
        self.TitleLabel.text = title
    end
    if desArr then
        local labelArr = {self.Label1, self.Label2, self.Label3, self.Label4}
        for i, text in pairs(desArr) do
            labelArr[i].text = text
        end
    end
end

--x、y取值在[-1, 1]之间
function LuaTwoDimenSlider:SetValue(x, y, triggerCallback) 
    if not self.m_IsInit then
        return
    end
    self.m_CurXValue = math.min(1, math.max(-1, x))
    self.m_CurYValue = math.min(1, math.max(-1, y))
    local halfLength = self.m_HalfCoordinateAxisLength
    self.Thumb.transform.localPosition = Vector3(self.m_CurXValue * halfLength, self.m_CurYValue * halfLength, 0)
    if triggerCallback and self.m_OnValueChangedAction then
        self.m_OnValueChangedAction(self.m_CurXValue, self.m_CurYValue)
    end
end

--返回取值在[-1, 1]之间的x、y
function LuaTwoDimenSlider:GetValue()
    if self.m_CurXValue and self.m_CurYValue then
        return self.m_CurXValue, self.m_CurYValue
    end
    local halfLength = self.m_HalfCoordinateAxisLength
    local pos = self.Thumb.transform.localPosition
    local x = pos.x / halfLength
    local y = pos.y / halfLength
    return x, y
end

function LuaTwoDimenSlider:SetResetXYAction(action)
    self.m_OnResetXYBtnClickedAction = action
end
--@endregion InitTemplate

function LuaTwoDimenSlider:Update()
    if not self.m_IsDragging or not self.m_IsInit then return end
    local screenPos = self:GetCurScreenPos()
    if self.m_LastPos then
        if math.abs(screenPos.x-self.m_LastPos.x)+ math.abs(screenPos.y-self.m_LastPos.y)< 0.01 then
            return
        end
    end
    self:SetThumbScreenPos(screenPos)
    self.m_LastPos = screenPos
end

function LuaTwoDimenSlider:GetCurScreenPos()
	local screenPos = Input.mousePosition
	if CommonDefs.IsInMobileDevice() then
		screenPos = Input.GetTouch(0).position
		screenPos = Vector3(screenPos.x, screenPos.y, 0)
    end
	return screenPos
end

function LuaTwoDimenSlider:SetThumbScreenPos(screenPos)
    if not self.m_IsInit or not CUIManager.UIMainCamera then
        return
    end
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    local relLocalPos = self.Thumb.transform.parent:InverseTransformPoint(worldPos)
    local halfLength = self.m_HalfCoordinateAxisLength
    local x = relLocalPos.x / halfLength
    local y = relLocalPos.y / halfLength
    self:SetValue(x, y, true)
end

--@region UIEvent
function LuaTwoDimenSlider:OnCoordinateAxisBgClick()
	local screenPos = self:GetCurScreenPos()
    self:SetThumbScreenPos(screenPos)
end

function LuaTwoDimenSlider:OnDragThumbStart(go)
	self.m_IsDragging = true
    if self.m_OnDragStartAction then
        self.m_OnDragStartAction(self:GetValue())
    end
end

function LuaTwoDimenSlider:OnDragThumbEnd(go)
	self.m_IsDragging = false
    if self.m_OnDragEndAction then
        self.m_OnDragEndAction(self:GetValue())
    end
end

function LuaTwoDimenSlider:OnResetXYBtnClick()
    if self.m_OnResetXYBtnClickedAction then
        self.m_OnResetXYBtnClickedAction()
        return
    end
	self:SetValue(0, 0, true)
    if self.m_OnDragEndAction then
        self.m_OnDragEndAction(self:GetValue())
    end
end
--@endregion UIEvent

