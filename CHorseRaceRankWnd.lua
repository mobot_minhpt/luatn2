-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHorseRaceMgr = import "L10.Game.CHorseRaceMgr"
local CHorseRaceRankTemplate = import "L10.UI.CHorseRaceRankTemplate"
local CHorseRaceRankWnd = import "L10.UI.CHorseRaceRankWnd"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CHorseRaceRankWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
    Extensions.RemoveAllChildren(this.table.transform)
    this.rankTemplate:SetActive(false)
    do
        local i = 0
        while i < CHorseRaceMgr.Inst.rank.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.rankTemplate)
            instance:SetActive(true)
            local rankInfo = CHorseRaceMgr.Inst.rank[i]
            local list = CreateFromClass(MakeGenericClass(List, String))
            CommonDefs.ListAdd(list, typeof(String), tostring(rankInfo.rank))
            CommonDefs.ListAdd(list, typeof(String), rankInfo.playerName)
            CommonDefs.ListAdd(list, typeof(String), rankInfo.horseName)
            CommonDefs.ListAdd(list, typeof(String), System.String.Format("{0}%", rankInfo.progress))
            CommonDefs.ListAdd(list, typeof(String), System.String.Format(LocalString.GetString("{0}分{1}秒"), math.floor(math.floor(rankInfo.time) / 60), math.floor(rankInfo.time) % 60))
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CHorseRaceRankTemplate))
            if template ~= nil then
                template:Init(list, CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Name == rankInfo.playerName and Color.green or Color.white)
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollview:ResetPosition()
end
