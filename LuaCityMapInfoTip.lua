require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"

CLuaCityMapInfoTip = class()
CLuaCityMapInfoTip.Path = "ui/citywar/LuaCityMapInfoTip"

RegistClassMember(CLuaCityMapInfoTip, "m_Tick")
RegistClassMember(CLuaCityMapInfoTip, "m_StatusLabel")
RegistClassMember(CLuaCityMapInfoTip, "m_Time")

function CLuaCityMapInfoTip:Init()
	local isMajorCity = CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.CityInfoTemplateId)
	local childName = {"CityName", "CityLevel", "GuildName", "ServerName", "AssetAmount", "BuildingCount", "CityStatus"}
	local cityDData = CityWar_MapCity.GetData(CLuaCityWarMgr.CityInfoTemplateId)
	local cityName = cityDData and cityDData.Name or ""
	local status = CLuaCityWarMgr.StatusNameTable[CLuaCityWarMgr.CityInfoStatus]
	if CLuaCityWarMgr.CityInfoStatus == 1 then
		if isMajorCity and CityWar_Map.GetData(CLuaCityWarMgr.CityInfoMapTemplateId).Level >= 2 then
			status = status[2]
		else
			status = status[1]
		end
	end
	
	local content = {cityName, SafeStringFormat3(LocalString.GetString("%s级"), tostring(CLuaCityWarMgr.CityInfoLevel)), CLuaCityWarMgr.CityInfoGuildName, CLuaCityWarMgr.CityInfoServerName, CLuaCityWarMgr.CityInfoAssetAmount, CLuaCityWarMgr.CityInfoBuildingAmount, status}
	for i = 1, #childName do
		self.transform:Find("Offset/"..childName[i]):GetComponent(typeof(UILabel)).text = content[i]
	end

	if CLuaCityWarMgr.CityInfoStatus == 9 or CLuaCityWarMgr.CityInfoStatus == 6 or CLuaCityWarMgr.CityInfoStatus == 4 then
		self:InitTick()
	end

	local mapId = cityDData.MapID
	local bShowBtn = isMajorCity and CLuaCityWarMgr.CityInfoGuildId > 0 and CityWar_Map.GetData(mapId).Level > 1 and CLuaCityWarMgr:IsMajorCity(CLuaCityWarMgr.MapInfoMyGuildCityId) and CLuaCityWarMgr:IsNeighbor(CLuaCityWarMgr.MapInfoMyGuildMapId, mapId)
	self.transform:Find("Offset/MajorCityRoot").gameObject:SetActive(bShowBtn)
	if bShowBtn then
		local challengeObj = self.transform:Find("Offset/MajorCityRoot/Challenge").gameObject
		UIEventListener.Get(challengeObj).onClick = LuaUtils.VoidDelegate(function ( ... )
			CLuaCityWarMgr.RequestDeclareMirrorWar(CLuaCityWarMgr.CityInfoGuildId, CLuaCityWarMgr.CityInfoGuildName, CLuaCityWarMgr.CityInfoMapTemplateId, CLuaCityWarMgr.CityInfoTemplateId)
		end)
	end
end

function CLuaCityMapInfoTip:UpdateTime(time)
	local h, m, s = math.floor(time / 3600), math.floor((time % 3600) / 60), math.floor(time % 60)
	self.m_StatusLabel.text = CLuaCityWarMgr.StatusNameTable[CLuaCityWarMgr.CityInfoStatus]..SafeStringFormat3(LocalString.GetString("(%02d:%02d:%02d)"), h, m, s)
end

function CLuaCityMapInfoTip:InitTick()
	self.m_StatusLabel = self.transform:Find("Offset/CityStatus"):GetComponent(typeof(UILabel))
	self.m_Time = CLuaCityWarMgr.CityInfoDuration
	self:UpdateTime(self.m_Time)
	self.m_Tick = RegisterTickWithDuration(function ()
		self.m_Time = self.m_Time - 1
		self:UpdateTime(self.m_Time)
	end, 1000, self.m_Time * 1000)
end

function CLuaCityMapInfoTip:OnDestroy( ... )
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
