require("3rdParty/ScriptEvent")
require("common/common_include")

local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local MessageMgr=import "L10.Game.MessageMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CChangYongSheBeiMgr = import "L10.Game.CChangYongSheBeiMgr"


CLuaChangYongSheBeiWnd=class()
RegistClassMember(CLuaChangYongSheBeiWnd,"m_DescLabel")
RegistClassMember(CLuaChangYongSheBeiWnd,"m_OkButton")

RegistClassMember(CLuaChangYongSheBeiWnd,"m_ValidateBtn")

function CLuaChangYongSheBeiWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"OkBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CIndirectUIResources.CysbNotifyWnd)
    end)

    local g = LuaGameObject.GetChildNoGC(self.transform,"ValidateBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestOpenVerifyBindPhoneWnd()
        CUIManager.CloseUI(CIndirectUIResources.CysbNotifyWnd)
    end)

    self.m_DescLabel = LuaGameObject.GetChildNoGC(self.transform,"Description").label
    self.m_OkButton=LuaGameObject.GetChildNoGC(self.transform,"OkBtn").gameObject
    self.m_ValidateBtn = LuaGameObject.GetChildNoGC(self.transform,"ValidateBtn").gameObject

    if CChangYongSheBeiMgr.Inst.SetPhone then
	self.m_ValidateBtn:SetActive(true)
	self.m_OkButton:SetActive(false)
	self.m_DescLabel.text = MessageMgr.Inst:FormatMessage("CYSB_NOT_CHANGYONG_NEED_CHECK_PHONE", {})
    else
	self.m_ValidateBtn:SetActive(false)
	self.m_OkButton:SetActive(true)
	self.m_DescLabel.text = MessageMgr.Inst:FormatMessage("CYSB_NOT_CHANGYONG_NEED_SET_PHONE", {})
    end
end

return CLuaChangYongSheBeiWnd
