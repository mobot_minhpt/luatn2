-- Auto Generated!!
local CMapiViewSimpleItem = import "L10.UI.CMapiViewSimpleItem"
CMapiViewSimpleItem.m_Init_CS2LuaHook = function (this, texturePath, name, level, showAdd, lockClickAction, addClickAction, textureClickAction, qualitySpriteName) 
    if this.texture ~= nil then
        if texturePath ~= nil then
            this.texture.gameObject:SetActive(true)
            this.texture:LoadMaterial(texturePath)
        else
            this.texture.gameObject:SetActive(false)
        end
    end

    if name ~= nil and this.nameLabel ~= nil then
        this.nameLabel.text = name
    end

    if this.levelLabel ~= nil then
        this.levelLabel.gameObject:SetActive(level ~= 0)
        this.levelLabel.text = tostring(level)
    end

    if this.addBtn ~= nil then
        this.addBtn:SetActive(showAdd and texturePath == nil)
    end

    if this.lockBtn ~= nil then
        this.lockBtn:SetActive(not showAdd and texturePath == nil)
    end

    if this.selectedEffect ~= nil then
        this.selectedEffect:SetActive(false)
    end

    this.onLockClickAction = lockClickAction
    this.onAddClickAction = addClickAction
    this.onTextureClickAction = textureClickAction

    if this.qualitySprite ~= nil then
        this.qualitySprite.gameObject:SetActive(qualitySpriteName ~= nil)
        if qualitySpriteName ~= nil then
            this.qualitySprite.spriteName = qualitySpriteName
        end
    end
end
CMapiViewSimpleItem.m_OnClicked_CS2LuaHook = function (this, go) 
    if this.onTextureClickAction ~= nil then
        invoke(this.onTextureClickAction)
    elseif this.onAddClickAction ~= nil then
        invoke(this.onAddClickAction)
    elseif this.onLockClickAction ~= nil then
        invoke(this.onLockClickAction)
    end
end
CMapiViewSimpleItem.m_SetSelected_CS2LuaHook = function (this, bSel) 
    if this.selectedEffect ~= nil then
        this.selectedEffect:SetActive(bSel)
    end
end
