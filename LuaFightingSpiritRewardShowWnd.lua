local QnExpandListBox = import "L10.UI.QnExpandListBox"

local CPackageItemCell = import "L10.UI.CPackageItemCell"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UITabBar = import "L10.UI.UITabBar"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"

LuaFightingSpiritRewardShowWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritRewardShowWnd, "TabRoot", "TabRoot", UITabBar)
RegistChildComponent(LuaFightingSpiritRewardShowWnd, "Content", "Content", UIGrid)
RegistChildComponent(LuaFightingSpiritRewardShowWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaFightingSpiritRewardShowWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaFightingSpiritRewardShowWnd, "LevelSelectBox", "LevelSelectBox", QnExpandListBox)
RegistChildComponent(LuaFightingSpiritRewardShowWnd, "SelfGropuMark", "SelfGropuMark", GameObject)
RegistChildComponent(LuaFightingSpiritRewardShowWnd, "Grid", "Grid", GameObject)

--@endregion RegistChildComponent end

function LuaFightingSpiritRewardShowWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaFightingSpiritRewardShowWnd:Init()
    self.m_Level = 0

    -- 斗魂已经开始报名 才会发送查询rpc
    local date = DouHunTan_Cron.GetData("SignUp").Value
	local _, _, mm, hh, dd, MM = string.find(date, "(%d+) (%d+) (%d+) (%d+) *")
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local startTime = CreateFromClass(DateTime, now.Year, MM, dd, hh, mm, 0)
	if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, startTime) then
		Gac2Gas.QueryDouHunStage()
	end

    self.DescLabel.text = g_MessageMgr:FormatMessage("DounHun_Reward_Show_Wnd_Desc")
    self.TabRoot.OnTabChange = DelegateFactory.Action_GameObject_int(function(obj, index)
        if index == 0 then
            -- 初始化淘汰赛
            self:InitTaotaiShow()
        elseif index == 1 then
            -- 初始化循环赛
            self:InitXunhuanShow()
        elseif index==2 then
            self:InitFuneiShow()
        end
    end)

    self.ItemCell.gameObject:SetActive(false)

    -- 设置组名下拉菜单
    local boxNameList = self:GetBoxNameList()
    self.LevelSelectBox:SetItems(boxNameList)

    self.LevelSelectBox:SetNameIndex(0)
    -- level索引从1开始
    self.LevelSelectBox.ClickCallback = DelegateFactory.Action_int(
        function(index)
            self:SetTaotaiShow(index)
        end
    )
    self.LevelSelectBox.ExpandScrollViewCallback = DelegateFactory.Action_QnExpandListBox(
        function(box)
            self:OnExpand()
        end
    )

    self.m_MessageList = {"DouHun_Reward_Show_Desc_1","DouHun_Reward_Show_Desc_2","DouHun_Reward_Show_Desc_3"}

    self:InitTaotaiShow()
end

function LuaFightingSpiritRewardShowWnd:GetBoxNameList()
    local nameList = DouHunCross_Setting.GetData().LevelName
    local serverId = CClientMainPlayer.Inst:GetMyServerId()
    local guanWangServer = DouHunCross_GuanWangServer.GetData(serverId)
    local yingHeServer = DouHunCross_YingHeServer.GetData(serverId)
    local serverCount = 8   -- 至少显示一个
    if guanWangServer ~= nil then
        serverCount = DouHunCross_GuanWangServer.GetDataCount()
    elseif yingHeServer ~= nil then
        serverCount = DouHunCross_YingHeServer.GetDataCount()
    end
    local nameCount = math.min(math.ceil(serverCount / 8), nameList.Length)
    local boxNameList = CreateFromClass(MakeGenericClass(List, String))
    for i = 1, nameCount do
        CommonDefs.ListAdd(boxNameList, typeof(String), nameList[i - 1])
    end
    local bg = self.transform:Find("LevelSelectBox/BG"):GetComponent(typeof(UISprite))
    bg.height = bg.height / 6 * nameCount
    return boxNameList
end

function LuaFightingSpiritRewardShowWnd:SetStage(stage, level)
    
    -- 初始化第一格
    if level == 0 or stage < 6 then
        level = 0
    end
    self.m_Level = level

    print(level)
    -- 如果是淘汰赛
    if self.LevelSelectBox.gameObject.activeSelf then
        if self.m_Level > 0 then
            self:SetTaotaiShow(self.m_Level -1)
        else
            self:SetTaotaiShow(0)
        end
        
    end
end

function LuaFightingSpiritRewardShowWnd:OnExpand()
    for i=1, self.Grid.transform.childCount do
        local item = self.Grid.transform:GetChild(i - 1)

        local mark = item:Find("SelfGropuMark").gameObject
        mark:SetActive(self.m_Level == i)
    end
end

-- 淘汰赛
function LuaFightingSpiritRewardShowWnd:InitTaotaiShow()
    self.LevelSelectBox.gameObject:SetActive(true)

    if self.m_Level > 0 then
        self.LevelSelectBox:SetIndex(self.m_Level-1, true)
    else
        self.LevelSelectBox:SetIndex(0, true)
    end

    self.DescLabel.text = g_MessageMgr:FormatMessage(self.m_MessageList[1])
end

-- 设置淘汰赛的某一级奖励
-- 0-5
function LuaFightingSpiritRewardShowWnd:SetTaotaiShow(index)
    -- 默认显示
    self.TabRoot:ChangeTab(0, true)

    self.SelfGropuMark:SetActive(self.m_Level == index + 1)
        
    local levelName = (DouHunCross_Setting.GetData().LevelName)[index]
    local nameList = {LocalString.GetString("冠军"), LocalString.GetString("亚军"), LocalString.GetString("季军"), 
        LocalString.GetString("第四名"), LocalString.GetString("第五到八名")
    }

    local rewardData = {}
    DouHunTan_PlayerReward.Foreach(function(key, data)
        if data.Group == index+1 then
            rewardData[data.Rank] = data.Reward
        end
    end)

    for i = 1, 5 do
        local item = self.Content.transform:Find(tostring(i))
        item.gameObject:SetActive(true)
    end

    for i = 1, 5 do
        local label = self.Content.transform:Find(tostring(i).."/Label"):GetComponent(typeof(UILabel))
        label.text = levelName .. nameList[i]

        local grid = self.Content.transform:Find(tostring(i).."/ItemGrid"):GetComponent(typeof(UIGrid))
        local data = rewardData[i]

        Extensions.RemoveAllChildren(grid.transform)
   
        for i=0, data.Length-2, 2 do
            local g = NGUITools.AddChild(grid.gameObject, self.ItemCell.gameObject)
            g:SetActive(true)

            local itemId = data[i]
            local count = data[i+1]
            local data = Item_Item.GetData(itemId)

            local iconTex = g.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            local numLabel = g.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
            numLabel.text = tostring(count)
            iconTex:LoadMaterial(data.Icon)

            UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function ()
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        end
        
        grid:Reposition()
    end
end

-- 循环赛的奖励比较简单
function LuaFightingSpiritRewardShowWnd:InitXunhuanShow()
    self.LevelSelectBox.gameObject:SetActive(false)
    self.TabRoot:ChangeTab(1, true)

    local nameList = {LocalString.GetString("每失败一场"), LocalString.GetString("每胜利一场")}

    local rewardData = {}
    -- 0失败 1胜利 2晋级
    DouHunTan_PlayerReward.Foreach(function(key, data)
        if data.Group == 0 then
            rewardData[data.Rank] = data.Reward
        end
    end)
    
    for i = 1, 5 do
        local item = self.Content.transform:Find(tostring(i))
        item.gameObject:SetActive(false)
    end

    for i = 1, 2 do
        local item = self.Content.transform:Find(tostring(i))
        item.gameObject:SetActive(true)

        local label = self.Content.transform:Find(tostring(i).."/Label"):GetComponent(typeof(UILabel))
        label.text = nameList[i]

        local grid = self.Content.transform:Find(tostring(i).."/ItemGrid"):GetComponent(typeof(UIGrid))
        local data = rewardData[i-1]

        Extensions.RemoveAllChildren(grid.transform)
   
        for i=0, data.Length-2, 2 do
            local g = NGUITools.AddChild(grid.gameObject, self.ItemCell.gameObject)
            g:SetActive(true)

            local itemId = data[i]
            local count = data[i+1]
            local data = Item_Item.GetData(itemId)

            local iconTex = g.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            local numLabel = g.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
            numLabel.text = tostring(count)
            iconTex:LoadMaterial(data.Icon)

            UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function ()
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        end
        
        grid:Reposition()
    end
    self.DescLabel.text = g_MessageMgr:FormatMessage(self.m_MessageList[2])
end

function LuaFightingSpiritRewardShowWnd:InitFuneiShow()
    self.LevelSelectBox.gameObject:SetActive(false)
    self.TabRoot:ChangeTab(2, true)

    local nameList = {LocalString.GetString("晋级循环赛"), LocalString.GetString("未晋级循环赛"), LocalString.GetString("投票积分前10")}

    local rewardData = {}
    -- 0失败 1胜利 2晋级
    DouHunTan_PlayerReward.Foreach(function(key, data)
        if data.Group == 0 then
            rewardData[data.Rank] = data.Reward
        end
    end)
    
    for i = 1, 5 do
        local item = self.Content.transform:Find(tostring(i))
        item.gameObject:SetActive(false)
    end

    for i = 1, 3 do
        local item = self.Content.transform:Find(tostring(i))
        item.gameObject:SetActive(true)

        local label = self.Content.transform:Find(tostring(i).."/Label"):GetComponent(typeof(UILabel))
        label.text = nameList[i]

        local grid = self.Content.transform:Find(tostring(i).."/ItemGrid"):GetComponent(typeof(UIGrid))
        local data = rewardData[i+1]

        Extensions.RemoveAllChildren(grid.transform)
   
        for i=0, data.Length-2, 2 do
            local g = NGUITools.AddChild(grid.gameObject, self.ItemCell.gameObject)
            g:SetActive(true)

            local itemId = data[i]
            local count = data[i+1]
            local data = Item_Item.GetData(itemId)

            local iconTex = g.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            local numLabel = g.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
            numLabel.text = tostring(count)
            iconTex:LoadMaterial(data.Icon)

            UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function ()
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        end
        
        grid:Reposition()
    end
    self.DescLabel.text = g_MessageMgr:FormatMessage(self.m_MessageList[3])
end

function LuaFightingSpiritRewardShowWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryDouHunStageResult", self, "SetStage")
end

function LuaFightingSpiritRewardShowWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryDouHunStageResult", self, "SetStage")
end

--@region UIEvent

--@endregion UIEvent

