local CUICommonDef = import "L10.UI.CUICommonDef"

LuaYingLingSwitchingTable = class()

function LuaYingLingSwitchingTable:Update()
    if not CUICommonDef.CheckPressWhenButtonDown(self.transform) then
        self.gameObject:SetActive(false)
    end
end

function LuaYingLingSwitchingTable:OnDisable()
    self.transform.parent:Find("Bg").gameObject:SetActive(false)
end