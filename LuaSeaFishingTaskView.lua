

local UITable = import "UITable"
local UISprite = import "UISprite"

local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local UISlider = import "UISlider"

LuaSeaFishingTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSeaFishingTaskView, "SectionTable", "SectionTable", UITable)
RegistChildComponent(LuaSeaFishingTaskView, "BgTexture", "BgTexture", UISprite)
RegistChildComponent(LuaSeaFishingTaskView, "StageNameLabel", "StageNameLabel", UILabel)
RegistChildComponent(LuaSeaFishingTaskView, "StageDescLabel", "StageDescLabel", UILabel)
RegistChildComponent(LuaSeaFishingTaskView, "ConditionLabel", "ConditionLabel", UILabel)
RegistChildComponent(LuaSeaFishingTaskView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaSeaFishingTaskView, "LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaSeaFishingTaskView, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaSeaFishingTaskView, "Progress", "Progress", UISprite)
RegistChildComponent(LuaSeaFishingTaskView, "ProgressSlider", "ProgressSlider", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaSeaFishingTaskView, "m_ProgressLabel")
RegistClassMember(LuaSeaFishingTaskView, "m_StageInfoCount")
function LuaSeaFishingTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick()
	end)

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

    --@endregion EventBind end
	self.m_StageInfoCount = 0
end

function LuaSeaFishingTaskView:OnEnable()
    g_ScriptEvent:AddListener("InitTaskViewWithStage",self,"Init")--OnInitTaskViewWithStage
	g_ScriptEvent:AddListener("UpdateSeaFishingPlayFishScore",self,"onUpdateSeaFishingPlayFishScore")
end

function LuaSeaFishingTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("InitTaskViewWithStage",self,"Init")--OnInitTaskViewWithStage
	g_ScriptEvent:RemoveListener("UpdateSeaFishingPlayFishScore",self,"onUpdateSeaFishingPlayFishScore")
end

function LuaSeaFishingTaskView:Init()
    self.ConditionLabel.gameObject:SetActive(false)
    self.ProgressSlider.gameObject:SetActive(false)
	if LuaSeaFishingMgr.m_CurHaiDiaoGameplayId and LuaSeaFishingMgr.m_CurHaiDiaoStageId then
		self:OnInitTaskViewWithStage(LuaSeaFishingMgr.m_CurHaiDiaoStageId,LuaSeaFishingMgr.m_CurHaiDiaoGameplayId,LuaSeaFishingMgr.m_CurParams)
	end
	self.StageDescLabel.gameObject:SetActive(false)
end

--@region UIEvent

function LuaSeaFishingTaskView:OnLeaveBtnClick()
	CGamePlayMgr.Inst:LeavePlay()
end

function LuaSeaFishingTaskView:OnRuleBtnClick()
	local specialGameplayId = HaiDiaoFuBen_Settings.GetData().SpecialGameplayId
	if LuaSeaFishingMgr.m_CurHaiDiaoGameplayId and tonumber(LuaSeaFishingMgr.m_CurHaiDiaoGameplayId) == tonumber(specialGameplayId) then
		g_MessageMgr:ShowMessage("HuanHaiYaoWu_Rule")
	else
		g_MessageMgr:ShowMessage("HaiDiaoFuBen_Rule")
	end
end


--@endregion UIEvent

function LuaSeaFishingTaskView:OnInitTaskViewWithStage(stageId,gamePlayId,params)
	self.m_StageInfoCount = 0
	local curKey = nil
	local list = nil
	if params then
		list = MsgPackImpl.unpack(params)
	end

	Extensions.RemoveAllChildren(self.SectionTable.transform)
	HaiDiaoFuBen_StageInfo.ForeachKey(function (key) 
        local data = HaiDiaoFuBen_StageInfo.GetData(key)		
		if stageId == data.StageId then
			local gameplayIdArray = data.GameplayId
			for i=0,gameplayIdArray.Length-1,1 do
				local gameplayid = gameplayIdArray[i]
				if gameplayid == gamePlayId then
					curKey = key
				end
			end
		end
    end)
	if not curKey then
		return
	end
	local data = HaiDiaoFuBen_StageInfo.GetData(curKey)	
	self.StageNameLabel.text = data.StageName
	local gamePlayData = Gameplay_Gameplay.GetData(gamePlayId)
	if gamePlayData then
		self.TaskName.text = gamePlayData.Name
	end
	--self.StageDescLabel.text = data.description
	
	local descLabel = NGUITools.AddChild(self.SectionTable.gameObject,self.StageDescLabel.gameObject)
	descLabel:SetActive(true)
	descLabel.transform:GetComponent(typeof(UILabel)).text = data.description
	local conditionArray = data.Condition

	if conditionArray then
		local count = 0
		for i=0,conditionArray.Length-1,2 do
			local condition = conditionArray[i]
			local needCount = tonumber(conditionArray[i+1])
			local curCount = (list and list[count]) and list[count] or 0
			curCount = tonumber(curCount)
			count = count + 1
			if string.find(condition,"<progress>") then
				local f = string.find(condition,">")
				local slider = NGUITools.AddChild(self.SectionTable.gameObject,self.ProgressSlider)
				slider:SetActive(true)
				condition = string.sub(condition,f+1)
				self.m_ProgressLabel = slider.transform:Find("ValueLabe"):GetComponent(typeof(UILabel))
				self.m_ProgressLabel.text = SafeStringFormat3(LocalString.GetString("%s %d"),condition,curCount)..[[%]]
				local sprite = slider.transform:Find("Progress"):GetComponent(typeof(UISprite))
				
				if curCount <= 50 and curCount > 10 then
					sprite.color = NGUIText.ParseColor24("E6CB59", 0)
				elseif curCount <= 10 then
					sprite.color = NGUIText.ParseColor24("F51233", 0)
				else
					sprite.color = NGUIText.ParseColor24("3E92D9", 0)
				end

				--sprite.fillAmount = curCount/100
				local uislider = slider.transform:GetComponent(typeof(UISlider))
				uislider.value = curCount/100
			else
				local sectionItem = NGUITools.AddChild(self.SectionTable.gameObject,self.ConditionLabel.gameObject)
				sectionItem:SetActive(true)
				if needCount == 0 then
					sectionItem.transform:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s (%d)"),condition,curCount)
				else
					sectionItem.transform:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s (%d/%d)"),condition,curCount,needCount)
				end
			end
			self.m_StageInfoCount = self.m_StageInfoCount + 1
		end
	end
	self.SectionTable:Reposition()
	local height = NGUIMath.CalculateRelativeWidgetBounds(self.SectionTable.transform).size.y
	height = height + self.StageDescLabel.height + 135
	self.BgTexture.height = height
	local y = self.TaskName.transform.localPosition.y
	local pos = self.RuleBtn.transform.localPosition
	self.RuleBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)
	pos = self.LeaveBtn.transform.localPosition
	self.LeaveBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)
end

function LuaSeaFishingTaskView:onUpdateSeaFishingPlayFishScore(finishTime,fishingInfo)
	local startIndex = self.m_StageInfoCount
	local specialItemCount = (fishingInfo and next(fishingInfo)) and (#fishingInfo + 1) or 1
	local index = 0
	for i = startIndex ,startIndex + specialItemCount - 1, 1 do
		index = index + 1
		local sectionItem
		if i >= self.SectionTable.transform.childCount then
			sectionItem = NGUITools.AddChild(self.SectionTable.gameObject,self.ConditionLabel.gameObject)
			sectionItem:SetActive(true)
		else
			sectionItem = self.SectionTable.transform.GetChild(i).gameObject
		end
		local label = sectionItem.transform:GetComponent(typeof(UILabel))
		local info = fishingInfo[index]
		if info and info.playerName then
			label.text = SafeStringFormat3("%s %d",info.playerName,info.score and info.score or 0)  
		else
			label.text = ""
		end
	end
	self.SectionTable:Reposition()
	local height = NGUIMath.CalculateRelativeWidgetBounds(self.SectionTable.transform).size.y
	height = height + 220
	self.BgTexture.height = height
	local y = self.TaskName.transform.localPosition.y
	local pos = self.RuleBtn.transform.localPosition
	self.RuleBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)
	pos = self.LeaveBtn.transform.localPosition
	self.LeaveBtn.transform.localPosition = Vector3(pos.x,y - height,pos.z)
end
