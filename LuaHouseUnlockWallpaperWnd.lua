local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

local CShopMallMgr = import "L10.UI.CShopMallMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Constants = import "L10.Game.Constants"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItemMgr = import "L10.Game.CItemMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"

LuaHouseUnlockWallpaperWnd = class()

RegistChildComponent(LuaHouseUnlockWallpaperWnd, "HintLabel", "HintLabel", UILabel)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "PreviewTexture", "PreviewTexture", CUITexture)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "UnlockButton", "UnlockButton", CButton)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "MoneyCtrl", "MoneyCtrl", CCurentMoneyCtrl)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "XiaoHaoPing", "XiaoHaoPing", CUITexture)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "Mask", "Mask", GameObject)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "GetMoneyHint", "GetMoneyHint", UILabel)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "DurationLimitLabel", "DurationLimitLabel", UILabel)
RegistChildComponent(LuaHouseUnlockWallpaperWnd, "DurationLimitBg", "DurationLimitBg", UITexture)

RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_IsMoneyEnough")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_IsFreeMoneyEnough")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_IsConsumeEnough")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_MoneyType")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_ExtSliver")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_UseJade")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_Pos")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_PrePaperId")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_NextPaperId")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_selectIndex")
RegistClassMember(LuaHouseUnlockWallpaperWnd,"m_Wallpapers")

function LuaHouseUnlockWallpaperWnd:OnEnable()
    Gac2Gas.QueryHouseData()
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:AddListener("OnSendHouseData", self, "OnUpdateHouseResource")
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
end

function LuaHouseUnlockWallpaperWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:RemoveListener("OnSendHouseData", self, "OnUpdateHouseResource")
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
end

function LuaHouseUnlockWallpaperWnd:Awake()
    self.m_Pos = self.MoneyCtrl.transform.localPosition
end

function LuaHouseUnlockWallpaperWnd:Init()
    self.m_IsMoneyEnough = true
    self.m_IsConsumeEnough = true
    self.m_IsFreeMoneyEnough = true
    self.m_ExtSliver = 0
    self.m_UseJade = false
    local paperData = House_WallPaper.GetData(CLuaHouseMgr.m_SelectedPaperId)
    if not paperData then 
        CUIManager.CloseUI(CLuaUIResources.HouseUnlockWallpaperWnd)
        return
    end

    local paperItem = Item_Item.GetData(paperData.ItemID)
    if not paperItem then 
        CUIManager.CloseUI(CLuaUIResources.HouseUnlockWallpaperWnd)
        return
    end
    local colorStr = self:GetTitleColor(paperData.ItemID)
    self.HintLabel.text = SafeStringFormat3(LocalString.GetString("解锁[%s]%s[-]效果"), colorStr,paperItem.Name)
    
    
    local path = SafeStringFormat3("assets/res/%s", paperData.Preview)
    self.PreviewTexture:LoadMaterial(path)

    
    self:UpdateButtonStatus()
    self:InitDurationLimit()

    CommonDefs.AddOnClickListener(self.UnlockButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnUnlockButtonClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.PreviewTexture.gameObject, DelegateFactory.Action_GameObject(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(paperData.ItemID, false, nil, AlignType.Default, 0, 0, 0, 0)
    end), false)

    CommonDefs.AddOnClickListener(self.XiaoHaoPing.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnGetButtonClicked()
    end), false)

    CommonDefs.AddOnClickListener(self.LeftBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnLeftBtnClick()
    end), false)

    CommonDefs.AddOnClickListener(self.RightBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnRightBtnClick()
    end), false)
    
end

--@region UIEvent
function LuaHouseUnlockWallpaperWnd:OnUnlockButtonClicked(go)
    if not self.m_IsConsumeEnough then
        local itemdata = Item_Item.GetData(self.m_ConsumeItemId)
        local itemName = itemdata.Name
        g_MessageMgr:ShowMessage("BUY_WALLPAPER_NO_ITEM",itemName)
        return
    end
    
    if not self.m_IsMoneyEnough then
        if self.m_UseJade then
            local qnPoint = CClientMainPlayer.Inst and CClientMainPlayer.Inst.QnPoint or 0
            local txt = g_MessageMgr:FormatMessage("NotEnough_Jade", qnPoint)
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                CShopMallMgr.ShowChargeWnd()
            end), nil, LocalString.GetString("充值"), LocalString.GetString("取消"), false)
        else
            local msg = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
        end
        return
    end
    if self.m_IsMoneyEnough and not self.m_IsFreeMoneyEnough then
        local msg = g_MessageMgr:FormatMessage("Unenough_FreeSliver_Use_Silver_To_Fill",self.m_ExtSliver)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			Gac2Gas.RequestBuyWallOrFloorPaperType(CLuaHouseMgr.m_SelectedPaperId)
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
        return
    end

    Gac2Gas.RequestBuyWallOrFloorPaperType(CLuaHouseMgr.m_SelectedPaperId)
end

function LuaHouseUnlockWallpaperWnd:OnGetButtonClicked(go)
    -- 选中五石仓
    if self.m_IsConsumeEnough then return end
    CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_ConsumeItemId, true, nil, AlignType1.Left)
end

function LuaHouseUnlockWallpaperWnd:OnLeftBtnClick()
    if self.m_PrePaperId then
        CLuaHouseMgr.m_SelectedPaperId = self.m_PrePaperId
        self:Init()
    end
end

function LuaHouseUnlockWallpaperWnd:OnRightBtnClick()
    if self.m_NextPaperId then
        CLuaHouseMgr.m_SelectedPaperId = self.m_NextPaperId
        self:Init()
    end
end
--@endregion
function LuaHouseUnlockWallpaperWnd:UpdateButtonStatus()
    self.m_PrePaperId = nil
    self.m_NextPaperId = nil
    local yinpiao = CClientMainPlayer.Inst.FreeSilver
    local lingyu = CClientMainPlayer.Inst.Jade
    local silver = CClientMainPlayer.Inst.Silver
    local paperData = House_WallPaper.GetData(CLuaHouseMgr.m_SelectedPaperId)

    if paperData.YinPiaoPrice and paperData.YinPiaoPrice ~=0 then
        self.m_UseJade = false
        self.m_MoneyType = EnumMoneyType.YinPiao
        
        self.MoneyCtrl:SetCost(paperData.YinPiaoPrice)
        if yinpiao >= paperData.YinPiaoPrice then
            self.MoneyCtrl:SetType(self.m_MoneyType, EnumPlayScoreKey.NONE, false)
            self.m_IsMoneyEnough = true
            self.m_IsFreeMoneyEnough = true
            self.GetMoneyHint.gameObject:SetActive(false)
        else
            self.MoneyCtrl:SetType(self.m_MoneyType, EnumPlayScoreKey.NONE, true)
            self.m_IsFreeMoneyEnough = false
            self.GetMoneyHint.gameObject:SetActive(true)
            --银票不足，将消耗银两XXXXX，是否确认消耗？
            if paperData.YinPiaoPrice > yinpiao and yinpiao + silver >= paperData.YinPiaoPrice then
                self.m_IsMoneyEnough = true
                self.m_ExtSliver = paperData.YinPiaoPrice - yinpiao
                self.GetMoneyHint.text = SafeStringFormat3(LocalString.GetString("[c][FFED5F]不足的部分将从#287中补足[-][/c]"))
            else
                self.m_IsMoneyEnough = false
                self.GetMoneyHint.text = SafeStringFormat3(LocalString.GetString("[c][FF0000]不足的部分无法从#287中补足[-][/c]"))
            end
        end
    elseif paperData.JadePrice and paperData.JadePrice ~= 0 then
        self.m_UseJade = true
        self.m_MoneyType = EnumMoneyType.LingYu
        self.MoneyCtrl:SetType(self.m_MoneyType, EnumPlayScoreKey.NONE, true)
        self.MoneyCtrl:SetCost(paperData.JadePrice)
        if lingyu >= paperData.JadePrice then
            self.m_IsMoneyEnough = true
        else
            self.m_IsMoneyEnough = false
        end
        self.GetMoneyHint.gameObject:SetActive(false)
    end
    
    local consume = paperData.ConsumeItme
    if consume and consume.Length >=2 then
        local consumeItemId = consume[0]
        local needCount = consume[1]
        local bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(consumeItemId)
        local ownCount = bindCount + notbindCount
        local itemdata = Item_Item.GetData(consumeItemId)
        self.m_ConsumeItemId = consumeItemId
        if itemdata then
            self.XiaoHaoPing:LoadMaterial(itemdata.Icon)
        end
        local colorstr = "FFFFFF"
        if ownCount >= needCount then
            self.m_IsConsumeEnough = true
            self.Mask:SetActive(false)
            colorstr = "FFFFFF"
        else
            self.m_IsConsumeEnough = false
            self.Mask:SetActive(true)
            colorstr = "FF0000"
        end
        self.CountLabel.text = SafeStringFormat3("[c][%s]%d[-][/c]/%d",colorstr,ownCount,needCount)--ownCount[c][FFFFFF]%s[-][/c]
        self.XiaoHaoPing.gameObject:SetActive(true)
        self.MoneyCtrl.transform.localPosition = self.m_Pos
    else
        self.m_IsConsumeEnough = true
        self.XiaoHaoPing.gameObject:SetActive(false)
        local pos = Vector3(self.m_Pos.x+ 60 ,self.m_Pos.y,self.m_Pos.z)
        self.MoneyCtrl.transform.localPosition = pos
    end

    self.m_Wallpapers = {}
    House_WallPaper.Foreach(function (k,v)
        table.insert(self.m_Wallpapers,v.Id)
    end)
    table.sort(self.m_Wallpapers,function(id1,id2)
        local wallPaper1 = House_WallPaper.GetData(id1)
        local wallPaper2 = House_WallPaper.GetData(id2)
        local isUnlock1 = CLuaHouseMgr.IsWallPaperUnlock(id1)
        local isUnlock2 = CLuaHouseMgr.IsWallPaperUnlock(id2)
        if isUnlock1 and not isUnlock2 then
            return true
        elseif not isUnlock1 and isUnlock2 then
            return false
        end
        return wallPaper1.Order < wallPaper2.Order 
    end)

    self.m_selectIndex = 1
    for index,id in ipairs(self.m_Wallpapers) do
        if CLuaHouseMgr.m_SelectedPaperId == id then
            self.m_selectIndex = index
        end
    end

    local preIndex = self.m_selectIndex - 1
    local nextIndex = self.m_selectIndex + 1

    if preIndex >=1 and preIndex <= #self.m_Wallpapers then
        local findLeft = nil
        for left = preIndex,1,-1 do
            local id = self.m_Wallpapers[left]
            local isUnlock = CLuaHouseMgr.IsWallPaperUnlock(id)
            if not isUnlock then
                findLeft = left
                break
            end
        end
        self.LeftBtn:SetActive(findLeft ~= nil)
        if findLeft then
            self.m_PrePaperId = self.m_Wallpapers[findLeft]
        end
    else
        self.LeftBtn:SetActive(false)
    end

    if nextIndex >=1 and nextIndex <= #self.m_Wallpapers then
        local findRight = nil
        for right = nextIndex,#self.m_Wallpapers,1 do
            local id = self.m_Wallpapers[right]
            local isUnlock = CLuaHouseMgr.IsWallPaperUnlock(id)
            if not isUnlock then
                findRight = right
                break
            end
        end
        self.RightBtn:SetActive(findRight ~= nil)
        if findRight then
            self.m_NextPaperId = self.m_Wallpapers[findRight]
        end
    else
        self.RightBtn:SetActive(false)
    end
end

function LuaHouseUnlockWallpaperWnd:GetTitleColor(itemId)
    local nameColor = Item_Item.GetData(itemId).NameColor
    local setting = GameSetting_Common.GetData()
    return setting.COLOR_VALUE[nameColor]
end

function LuaHouseUnlockWallpaperWnd:InitDurationLimit()
    local paperData = House_WallPaper.GetData(CLuaHouseMgr.m_SelectedPaperId)
    if paperData.Duration ~= -1 then
        self.DurationLimitLabel.gameObject:SetActive(true)
    else
        self.DurationLimitLabel.gameObject:SetActive(false)
        return
    end
    local color = NGUIText.ParseColor24("58FF23", 0)
    self.DurationLimitLabel.text = SafeStringFormat3(LocalString.GetString("%d天"),paperData.Duration)
    self.DurationLimitBg.color = color
end

function LuaHouseUnlockWallpaperWnd:OnMainPlayerUpdateMoney()
    self:Init()
end

function LuaHouseUnlockWallpaperWnd:OnUpdateHouseResource(args)
    self:Init()
end

function LuaHouseUnlockWallpaperWnd:OnSendItem(args)
    self:Init()
end
function LuaHouseUnlockWallpaperWnd:OnSetItemAt(args)
    self:Init()
end
