local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Constants = import "L10.Game.Constants"
local EnumWarehouseItemCellLockStatus = import "L10.UI.EnumWarehouseItemCellLockStatus"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local CWarehousePackageItemCell = import "L10.UI.CWarehousePackageItemCell"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"

CLuaWarehousePage = class()
RegistChildComponent(CLuaWarehousePage, "ItemCell", GameObject)
RegistClassMember(CLuaWarehousePage,"grid")
RegistClassMember(CLuaWarehousePage,"itemCells")
RegistClassMember(CLuaWarehousePage,"m_AvailableLabel")
RegistClassMember(CLuaWarehousePage,"OnRequestMoveItem")
RegistClassMember(CLuaWarehousePage,"selectedItemId")
RegistClassMember(CLuaWarehousePage,"selectedBindRepoCellId")
RegistClassMember(CLuaWarehousePage,"selectedPos")
RegistClassMember(CLuaWarehousePage,"warehouseIndex")
RegistClassMember(CLuaWarehousePage, "m_ActionDataSourc")
RegistClassMember(CLuaWarehousePage, "m_OnSetWarehouseItemAtEvent")
RegistClassMember(CLuaWarehousePage, "m_OnUpdateItemCountEvent")
RegistClassMember(CLuaWarehousePage, "m_OnSyncZuoqiExtraBagSizeEvent")
RegistClassMember(CLuaWarehousePage, "m_ItemLoaded")
CLuaWarehousePage.s_EnableDoubleClick = true
--TODO place holders衔接策划及程序之前的绑仓可用格及可解锁格位置
CLuaWarehousePage.m_BangCangUsable = 20
CLuaWarehousePage.m_BangCangAvailable = 40

function CLuaWarehousePage.GetZuoQiCarrySize(zuoqi_info)
    local table = ZuoQi_ZuoQi.GetData(zuoqi_info.templateId)
    if(table)then
        return table.CarryBagSize
    end
    return 0
end

function CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
    local has_zuoqi = false
    local carrysize = 0
    CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value) 
        local zuoqi = ___value
        if(zuoqi.expTime <= 0 or zuoqi.expTime >= CServerTimeMgr.Inst.timeStamp)then
            has_zuoqi = true
            carrysize = math.max(carrysize, CLuaWarehousePage.GetZuoQiCarrySize(zuoqi))
        end
    end))
    return carrysize, has_zuoqi
end

--绑仓开放的数量
function CLuaWarehousePage.GetUsableSize()
    local bindrepo_setting = BindRepo_Setting.GetData()
    local ret = 0
    if CClientMainPlayer.Inst then
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        ret = (data.UsableSize and data.UsableSize or 0) + bindrepo_setting.FreeSlotNum
    else
        ret = bindrepo_setting.FreeSlotNum
    end
    return ret
end

function CLuaWarehousePage.BindRepoUnlockPrice()
    local price = ""
    if(CClientMainPlayer.Inst)then
        local bindrepo_setting = BindRepo_Setting.GetData()
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        local price_formula = AllFormulas.Action_Formula[tonumber(bindrepo_setting.SlotPriceFormula)] and AllFormulas.Action_Formula[tonumber(bindrepo_setting.SlotPriceFormula)].Formula or nil
        if(price_formula)then
            price = price_formula(nil, nil, {data.UsableSize})
        end
    end
    return price
end

function CLuaWarehousePage.GetRepertoryPosStatus(repoIdx)
    local info = Charge_RepertoryInfo.GetData(CClientMainPlayer.Inst.ItemProp.Vip.Level)
    if(info)then
        local size1 = info.Size1
        local size2 = info.Size2 + Constants.SingeWarehouseSize
        local size3 = info.Size3 + Constants.SingeWarehouseSize * 2

        if(repoIdx > 0 and repoIdx <= 25 and repoIdx <= size1)then
            return EnumWarehouseItemCellLockStatus.Unlocked
        elseif(repoIdx > 25 and repoIdx <= 50 and repoIdx <= size2 and CClientMainPlayer.Inst.ItemProp.Repertory2Open == 1)then
            return EnumWarehouseItemCellLockStatus.Unlocked
        elseif(repoIdx > 50 and repoIdx <= 75 and repoIdx <= size3 and CClientMainPlayer.Inst.ItemProp.Repertory3Open == 1)then
            return EnumWarehouseItemCellLockStatus.Unlocked
        elseif(CSwitchMgr.IsEnableRiderPackage)then         --坐骑包裹
            local carry_size = nil
            local has_zuoqi = false
            carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
            if(has_zuoqi and repoIdx >= Constants.SingeWarehouseSize * 3 + 1 and repoIdx <= Constants.SingeWarehouseSize * (3 + CLuaWarehouseView.m_XingNangNum))then
                local deltaIdx = repoIdx - Constants.SingeWarehouseSize * 3
                local zuoqi_bag_size = carry_size

                local zuoqi_extra_bag_size = CClientMainPlayer.Inst.ItemProp.ExtraRiderBagSize
                local zuoqi_max_bag_size = math.max(ZuoQi_Setting.GetData().ExtraBagConsumeItemNum.Length - (CLuaWarehouseView.m_MaxXingNangNum - CLuaWarehouseView.m_XingNangNum) * Constants.SingeWarehouseSize, zuoqi_extra_bag_size)
                if(deltaIdx <=(zuoqi_bag_size + zuoqi_extra_bag_size))then
                    return EnumWarehouseItemCellLockStatus.Unlocked
                elseif(deltaIdx <=(zuoqi_bag_size + zuoqi_max_bag_size))then
                    return EnumWarehouseItemCellLockStatus.Locked
                else              
                    return EnumWarehouseItemCellLockStatus.UnlockDisabled
                end
            --绑仓开启之后就是可见的
            elseif(repoIdx >= CLuaWarehouseView.m_BangCangStartId * Constants.SingeWarehouseSize + 1 
                and repoIdx <= (CLuaWarehouseView.m_BangCangStartId + CLuaWarehouseView.m_BangCangNum) * Constants.SingeWarehouseSize)then
                --返回未解锁，但需要手动添加“可开启”标识
                --TODO 与服务器衔接
                --local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
                local localIdx = repoIdx - (CLuaWarehouseView.m_BangCangStartId * Constants.SingeWarehouseSize + 1)
                local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
                -- local usable_size = CLuaWarehousePage.GetUsableSize()
                local items = data.Items
                local counts = data.Count

                if(localIdx < CLuaWarehousePage.m_BangCangAvailable)then
                    return EnumWarehouseItemCellLockStatus.Unlocked
                else
                    return EnumWarehouseItemCellLockStatus.Locked
                end
            elseif((not has_zuoqi) and repoIdx >= Constants.SingeWarehouseSize * 3 + 1 and repoIdx <= Constants.SingeWarehouseSize * (3 + CLuaWarehouseView.m_XingNangNum))then
                return EnumWarehouseItemCellLockStatus.UnlockDisabled
            end
        else
            return EnumWarehouseItemCellLockStatus.Locked
        end
    end
    return EnumWarehouseItemCellLockStatus.Locked
end

function CLuaWarehousePage.IsRepertoryPosValid(repoIdx)
    local info = Charge_RepertoryInfo.GetData(CClientMainPlayer.Inst.ItemProp.Vip.Level)
    if(info)then
        local size1 = info.Size1
        local size2 = info.Size2 + Constants.SingeWarehouseSize
        local size3 = info.Size3 + Constants.SingeWarehouseSize * 2

        if(repoIdx > 0 and repoIdx <= 25 and repoIdx <= size1)then
            return true
        elseif(repoIdx > 25 and repoIdx <= 50 and repoIdx <= size2 and CClientMainPlayer.Inst.ItemProp.Repertory2Open == 1)then
            return true
        elseif(repoIdx > 50 and repoIdx <= 75 and repoIdx <= size3 and CClientMainPlayer.Inst.ItemProp.Repertory3Open == 1)then
            return true
        elseif(CSwitchMgr.IsEnableRiderPackage)then--坐骑包裹
            local carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
            if(has_zuoqi and repoIdx >= Constants.SingeWarehouseSize * 3 + 1 and repoIdx <= Constants.SingeWarehouseSize * (3 + CLuaWarehouseView.m_XingNangNum))then
                local deltaIdx = repoIdx - Constants.SingeWarehouseSize * 3
                local zuoqi_bag_size = carry_size
                local zuoqi_extra_bag_size = CClientMainPlayer.Inst.ItemProp.ExtraRiderBagSize
                return deltaIdx <=(zuoqi_bag_size + zuoqi_extra_bag_size)
            end
        end
    end
    return false
end

function CLuaWarehousePage:RefreshBindRepoData()
    -- print("RefreshBindRepoData")
    -- local carry_size = 0
    -- local has_zuoqi = false
    -- carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
    -- if(has_zuoqi)then
    local bindrepo_setting = BindRepo_Setting.GetData()
    local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
    local unlock_formula = AllFormulas.Action_Formula[tonumber(bindrepo_setting.UnlockFormula)] and AllFormulas.Action_Formula[tonumber(bindrepo_setting.UnlockFormula)].Formula or nil
    if(unlock_formula)then
        local arg = CClientMainPlayer.Inst.ItemProp.ExtraRiderBagSize-- + carry_size
        CLuaWarehousePage.m_BangCangAvailable = math.max(unlock_formula(nil, nil, {arg}), CLuaWarehousePage.GetUsableSize())
        CLuaWarehousePage.m_BangCangUsable = CLuaWarehousePage.GetUsableSize()
    end

    -- end
    self:OnUpdateBindRepo()
end

function CLuaWarehousePage:Awake()
    self.grid = self.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.itemCells = {}
    self.OnRequestMoveItem = nil
    self.selectedItemId = nil
    self.selectedBindRepoCellId = 0
    self.selectedPos = 0
    self.warehouseIndex = 0
    self.ItemCell:SetActive(false)
    self.m_ItemLoaded = false
    self.m_ActionDataSourc = DefaultItemActionDataSource.Create(
        1,
        {
            function() 
                self:OnTakeOut()
            end
        },
        {
            LocalString.GetString("取出")
        }
    )
    self.m_ActionDataSourc2 = DefaultItemActionDataSource.Create(
        2,
        {
            function() 
                self:OnTakeOut()
            end,
            function() 
                self:OnTakeOutInToAnNang()
            end
        },
        {
            LocalString.GetString("取出"),
            LocalString.GetString("放入鞍囊")
        }
    )
    
    
end


function CLuaWarehousePage:InitWarehouse( _warehouseIndex)


    self.warehouseIndex = _warehouseIndex
    self.selectedItemId = nil
    self.selectedPos = 0
    Extensions.RemoveAllChildren(self.grid.transform)
    self.itemCells = {}

    self:RefreshBindRepoData()
    --self:LoadItems()
end

--获取页面类型：0--仓库，1--行囊，2--绑仓
function CLuaWarehousePage:GetPageType()
    if(self.warehouseIndex >= 0 and self.warehouseIndex < 3)then
        return 0
    elseif(self.warehouseIndex >= CLuaWarehouseView.m_XinNangStartId and self.warehouseIndex < (CLuaWarehouseView.m_XinNangStartId + CLuaWarehouseView.m_XingNangNum))then
        return 1
    elseif(self.warehouseIndex >= CLuaWarehouseView.m_BangCangStartId and self.warehouseIndex < (CLuaWarehouseView.m_BangCangStartId + CLuaWarehouseView.m_BangCangNum))then
        return 2
    else
        return 3
    end
end
--TODO与服务器衔接,判断是否为可开启绑仓格子
function CLuaWarehousePage:ModifyItemCellForBangCang(_cell, local_id)
    if(self:GetPageType() == 2)then--如果是绑仓
        local bangcang_cellid = (self.warehouseIndex - CLuaWarehouseView.m_BangCangStartId) * Constants.SingeWarehouseSize + local_id
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        -- local bindrepo_setting = BindRepo_Setting.GetData()
        local usable_size = CLuaWarehousePage.GetUsableSize()

        if(bangcang_cellid >= usable_size and bangcang_cellid < CLuaWarehousePage.m_BangCangAvailable)then
            local label = _cell.transform:Find("TextLabel"):GetComponent(typeof(UILabel))
            label.text = "[ACF8FF]"..LocalString.GetString("可开启").."[-]"
        end
    end
end

function CLuaWarehousePage:LoadItems()
    if(CClientMainPlayer.Inst and not self.m_ItemLoaded)then
        local type = self:GetPageType()
        local carry_size = nil
        local has_zuoqi = false
        carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
        self.m_AvailableLabel = self.transform:Find("AvailableLabel"):GetComponent(typeof(UILabel))



        local bangcangItemCount=0
        local xingnangItemCount=0
        if type==1 then
            xingnangItemCount = CLuaWarehouseMgr.GetXingNangItemCount()
        elseif type==2 then
            bangcangItemCount=CLuaBindRepoMgr.GetBangCangItemCount()
        end
        if type==1 and (not has_zuoqi and xingnangItemCount==0) then--没坐骑 没物品
            local messageformat = MessageMgr.Inst:GetMessageFormat("Set_Defult_ZuoQi", true)
            self.m_AvailableLabel.text = messageformat--LocalString.GetString("行囊尚未激活，设置坐骑后可以激活行囊哦")
            self.m_AvailableLabel.gameObject:SetActive(true)
        elseif type==2 and (not CLuaBindRepoMgr.IsLevelMatch() or (not has_zuoqi and bangcangItemCount==0) ) then --没有坐骑、没有物品
            local messageformat = MessageMgr.Inst:GetMessageFormat("BDCK_STATUS_UNOPEN", true)
            self.m_AvailableLabel.text = messageformat--LocalString.GetString("绑仓尚未激活，设置坐骑后可以激活绑仓哦")
        else
            self.m_AvailableLabel.gameObject:SetActive(false)
            
            for i = 1, Constants.SingeWarehouseSize, 1 do
                local go = NGUITools.AddChild(self.grid.gameObject, self.ItemCell)
                go:SetActive(true)
                local cell = go:GetComponent(typeof(CWarehousePackageItemCell))
                cell.OnItemClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
                    function(item_cell)
                        self:OnItemClick(item_cell)
                    end
                )
                cell.OnItemDoubleClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
                    function(item_cell)
                        self:OnItemDoubleClick(item_cell)
                    end
                )
                table.insert(self.itemCells, cell)
            end
            
            self.grid:Reposition()
            self.m_ItemLoaded = true
            if(type == 0 or type == 1)then --仓库和行囊
                local i = 0
                while(i < #self.itemCells)do
                    local pos_id = i + 1 + self.warehouseIndex * Constants.SingeWarehouseSize
                    local itemId = CClientMainPlayer.Inst.RepertoryProp:GetItemAt(pos_id)
                    self.itemCells[i + 1]:Init(itemId, CLuaWarehousePage.GetRepertoryPosStatus(pos_id))

                    --TODO绑仓有独特的拿取方法
                    -- self:ModifyItemCellForBangCang(self.itemCells[i + 1], i)

                    i = i + 1
                end
            elseif(type == 2)then --绑仓
                self:OnUpdateBindRepo()
            end
        end
    end
end

function CLuaWarehousePage:OnItemClick(_itemCell)
    if CClientMainPlayer.Inst == nil then
        return
    end
    do
        local i = 0
        while i < #self.itemCells do
            if self.itemCells[i + 1] == _itemCell then
                local repoIndex = (i + 1 + self.warehouseIndex * Constants.SingeWarehouseSize)
                if System.String.IsNullOrEmpty(_itemCell.ItemId) then
                    local page_type = self:GetPageType()
                    --格子是锁定状态
                    if _itemCell.LockStatus == EnumWarehouseItemCellLockStatus.Locked then
                        if (page_type == 0) then
                            local minVipLevel = CClientMainPlayer.Inst:MinVipLevelToAccessRepertoryPos(repoIndex)
                            g_MessageMgr:ShowMessage("VIP_UNLOCK_THE_LINE", minVipLevel)
                        elseif(page_type == 1)then
                            self:ExpandZuoQiBagSize()
                        elseif(page_type == 2)then
                            --TODO 与服务器衔接，已锁定绑仓格子提示
                            g_MessageMgr:ShowMessage("BDCK_OPENLATTICE_ERROR_UNOPEN", "1", tostring(CLuaWarehousePage.BindRepoUnlockPrice()))
                        end
                    elseif(page_type == 2)then
                        local localIdx = repoIndex - (CLuaWarehouseView.m_BangCangStartId * Constants.SingeWarehouseSize + 1)
                        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
                        local usable_size = CLuaWarehousePage.GetUsableSize()
                        --TODO 与服务器衔接，判断是否为可开启绑仓格子
                        if(localIdx < usable_size)then
                            if(localIdx < data.Items.Length - 1)then
                                local template_id = data.Items[localIdx + 1]
                                if(template_id ~= 0)then
                                    self.selectedBindRepoCellId = localIdx + 1        
                                    CItemInfoMgr.ShowLinkItemTemplateInfo(template_id, false, self.m_ActionDataSourc, CItemInfoMgr.AlignType.ScreenRight, 0, 0, 0, 0)
                                end
                            end
                        else
                            self:ExpandBangCangSize()
                        end
                    end
                else
                    self.selectedItemId = _itemCell.ItemId
                    self.selectedPos = repoIndex
                    --是否是可以放入强化鞍囊的物品
                    local canPutInSimpleRepo = self:CheckCanPutInSimpleRepo(self.selectedItemId)
                    CItemInfoMgr.ShowLinkItemInfo(_itemCell.ItemId, false, canPutInSimpleRepo and self.m_ActionDataSourc2 or self.m_ActionDataSourc, CItemInfoMgr.AlignType.ScreenRight, 0, 0, 0, 0)
                end
                break
            end
            i = i + 1
        end
    end
end

function CLuaWarehousePage:CheckCanPutInSimpleRepo(itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    --TODO 读表判断该物品是否可以放入绑仓
    local bindrepo_not_allow = true
    if (item) and item.IsItem then
        bindrepo_not_allow = (BindRepo_AllowedItems.GetData(item.TemplateId) == nil) 
    else
        return false
    end
    local item_should_mask = ((item and ((not item.IsBinded) or (CLuaBindRepoMgr.GetItemExpiredTime(item) > 0) or bindrepo_not_allow)))
    if (item_should_mask) then
        return false
    end
    return true
end

function CLuaWarehousePage:ExpandZuoQiBagSize()
    if(CClientMainPlayer.Inst)then
        --首先判断是否有坐骑
        local carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()

        if has_zuoqi and carry_size==0 then
            --坐骑的格子数是0，这个时候不允许开格子
            g_MessageMgr:ShowMessage("XingNang_ZuoQi_NoSize_Unlock")
            return
        end

        local zuoqi_extra_bag_size = CClientMainPlayer.Inst.ItemProp.ExtraRiderBagSize
        local firstUnlockedIndex = zuoqi_extra_bag_size + 1
        local consumeCount = CClientMainPlayer.Inst:GetRiderExtraBagSizePrice(firstUnlockedIndex)
        if(consumeCount > 0)then
            local messageformat = MessageMgr.Inst:GetMessageFormat("XINGNANG_UNLOCK_TIPS", true)
            LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(ZuoQi_Setting.GetData().ExtraBagConsumeItemId, consumeCount, messageformat, true, false, LocalString.GetString("解锁"), function (enough)
                Gac2Gas.RequestIncRiderExtraBagSize(1)
            end, nil, false)
        end
    end
end

function CLuaWarehousePage:ExpandBangCangSize()
    local carry_size, has_zuoqi = CLuaWarehousePage.TryGetMaxZuoQiCarrySize()
    if not has_zuoqi then
        --没有坐骑的时候 点击“可开启”按钮提示一条消息
        g_MessageMgr:ShowMessage("ADD_SIMPLE_ITEM_NO_ZUOQI")
        return
    end


    -- --TODO 服务器衔接,替换银票数值
    local cost = CLuaWarehousePage.BindRepoUnlockPrice()
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    if freeSilver<cost then
        --BindRepo_Open_NeedYinLiang
        local message = g_MessageMgr:FormatMessage("BindRepo_Open_NeedYinLiang", cost-freeSilver)
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            Gac2Gas.RequestBuyOneSimpleItemSlot()
        end), nil, LocalString.GetString("开启"), nil, false)
    end

    local message = g_MessageMgr:FormatMessage("BDCK_OPENLATTICE_CONFIRM", tostring(CLuaWarehousePage.BindRepoUnlockPrice()))
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
        Gac2Gas.RequestBuyOneSimpleItemSlot()
    end), nil, LocalString.GetString("开启"), nil, false)
end

function CLuaWarehousePage:OnItemDoubleClick( itemCell) 
    if not CLuaWarehousePage.s_EnableDoubleClick then
        return
    end
    local type = self:GetPageType()
    if(type == 0 or type == 1)then
        if itemCell.ItemId == self.selectedItemId then
            self:OnTakeOut()
        end
    elseif(type == 2)then
        for k, v in ipairs(self.itemCells)do
            if(v == itemCell and k == self.selectedBindRepoCellId)then
                self:OnTakeOut()
            end
        end
    end
end

function CLuaWarehousePage:OnEnable()
    if(not self.m_OnSetWarehouseItemAtEvent)then
        self.m_OnSetWarehouseItemAtEvent = DelegateFactory.Action_uint_string(
            function(pos, itemId)
                self:OnSetWarehouseItemAt(pos, itemId)
            end
        )
    end
    EventManager.AddListenerInternal(EnumEventType.SetWarehouseItemAt, self.m_OnSetWarehouseItemAtEvent)

    if(not self.m_OnUpdateItemCountEvent)then
        self.m_OnUpdateItemCountEvent = DelegateFactory.Action_string(
            function(itemId)
                self:OnUpdateItemCount(itemId)
            end
        )
    end
    EventManager.AddListenerInternal(EnumEventType.SendItem, self.m_OnUpdateItemCountEvent)

    if(not self.m_OnSyncZuoqiExtraBagSizeEvent)then
        self.m_OnSyncZuoqiExtraBagSizeEvent = DelegateFactory.Action(
            function()
                self:OnSyncZuoqiExtraBagSize()
            end
        )
    end
    EventManager.AddListener(EnumEventType.SyncZuoqiExtraBagSize, self.m_OnSyncZuoqiExtraBagSizeEvent)

    g_ScriptEvent:AddListener("OnUpdateAllBindRepo", self, "OnUpdateBindRepo")
end

function CLuaWarehousePage:OnDisable()
    if(self.m_OnSetWarehouseItemAtEvent)then
        EventManager.RemoveListenerInternal(EnumEventType.SetWarehouseItemAt, self.m_OnSetWarehouseItemAtEvent)
    end

    if(self.m_OnUpdateItemCountEvent)then
        EventManager.RemoveListenerInternal(EnumEventType.SendItem, self.m_OnUpdateItemCountEvent)
    end

    if(self.m_OnSyncZuoqiExtraBagSizeEvent)then
        EventManager.RemoveListener(EnumEventType.SyncZuoqiExtraBagSize, self.m_OnSyncZuoqiExtraBagSizeEvent)
    end

    g_ScriptEvent:RemoveListener("OnUpdateAllBindRepo", self, "OnUpdateBindRepo")
end

function CLuaWarehousePage:OnDestroy()

    self.OnRequestMoveItem = nil
end

--普通仓库、行囊的更新
function CLuaWarehousePage:OnSetWarehouseItemAt(pos, itemId)
    if(CClientMainPlayer.Inst and self.m_ItemLoaded)then
        if( (pos > self.warehouseIndex * Constants.SingeWarehouseSize) and (pos <= (self.warehouseIndex + 1) * Constants.SingeWarehouseSize) )then
            self.itemCells[(pos - self.warehouseIndex * Constants.SingeWarehouseSize)]:Init(itemId, CLuaWarehousePage.GetRepertoryPosStatus(pos))
        end
    end
end

function CLuaWarehousePage:OnUpdateItemCount(itemId)
    if(itemId == nil or itemId == "" or (not self.m_ItemLoaded))then
        return
    end
    local n = #self.itemCells
    for i = 1, n, 1 do
        if(self.itemCells[i].ItemId == itemId)then
            self.itemCells[i]:UpdateAmount()
            break
        end
    end
end

function CLuaWarehousePage:InitBindRepoItem(cell_id, _templateid, _count)
    local item_cell = self.itemCells[cell_id + 1]
    item_cell:Init("", CLuaWarehousePage.GetRepertoryPosStatus(cell_id + 1 + self.warehouseIndex * Constants.SingeWarehouseSize))
    if(_templateid ~= 0)then
        local item_template = CItemMgr.Inst:GetItemTemplate(_templateid)
        local icon_texture = item_cell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local bind_sprite = item_cell.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
        local amount_label = item_cell.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
        local quality_sprite = item_cell.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
        icon_texture:LoadMaterial(item_template.Icon)
        bind_sprite.spriteName = Constants.ItemBindedSpriteName
        quality_sprite.spriteName = CUICommonDef.GetItemCellBorder(item_template.NameColor)
        amount_label.text = (_count > 1) and tostring(_count) or ""
    end
end

--绑仓更新
function CLuaWarehousePage:OnUpdateBindRepo()
    local type = self:GetPageType()
    if(self.m_ItemLoaded and type == 2)then
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        local items = data.Items
        local counts = data.Count
        -- local num = data.UsableSize
        local bangcang_start_id = (self.warehouseIndex - CLuaWarehouseView.m_BangCangStartId) * Constants.SingeWarehouseSize + 1--item array的长度似乎比总长度大1
        do
            local cell_id = 0
            local item_count = items and items.Length or 0
            while(cell_id < Constants.SingeWarehouseSize and bangcang_start_id < item_count)do
                local item_id = items[bangcang_start_id]
                self:InitBindRepoItem(cell_id, item_id, counts[bangcang_start_id])
                self:ModifyItemCellForBangCang(self.itemCells[cell_id + 1], cell_id)
                cell_id = cell_id + 1
                bangcang_start_id = bangcang_start_id + 1
            end

            while(cell_id < Constants.SingeWarehouseSize)do
                self.itemCells[cell_id + 1]:Init(nil, CLuaWarehousePage.GetRepertoryPosStatus(cell_id + 1 + self.warehouseIndex * Constants.SingeWarehouseSize))
                self:ModifyItemCellForBangCang(self.itemCells[cell_id + 1], cell_id)
                cell_id = cell_id + 1
            end
        end 
    end
end

function CLuaWarehousePage:OnSyncZuoqiExtraBagSize()
    if(CClientMainPlayer.Inst and self.m_ItemLoaded and (self:GetPageType() == 1))then
        local n = #self.itemCells
        for i = 1, n, 1 do
            local repoIndex = (i + self.warehouseIndex * Constants.SingeWarehouseSize)
            if(not CClientMainPlayer.Inst:IsRegularRepertoryPos(repoIndex))then
                self.itemCells[i]:Init(self.itemCells[i].ItemId, CLuaWarehousePage.GetRepertoryPosStatus(repoIndex))
            end
        end
        self:RefreshBindRepoData()
    end
end

function CLuaWarehousePage:OnTakeOut()
    local type = self:GetPageType()
    if(type == 0 or type == 1)then
        local item = CItemMgr.Inst:GetById(self.selectedItemId)
        if(item ~= nil and CClientMainPlayer.Inst)then
            if(self.OnRequestMoveItem ~= nil)then
                self.OnRequestMoveItem(self.selectedPos, self.selectedItemId)
            end
            CItemInfoMgr.CloseItemInfoWnd()
        end
    elseif(type == 2)then
        local data = CClientMainPlayer.Inst.ItemProp.SimpleItems
        local items = data.Items
        local counts = data.Count
        if(self.selectedBindRepoCellId > 0 and items[self.selectedBindRepoCellId] ~= 0)then
            Gac2Gas.MoveSimpleItemToNormalItem(self.selectedBindRepoCellId, items[self.selectedBindRepoCellId], counts[self.selectedBindRepoCellId])
            CItemInfoMgr.CloseItemInfoWnd()
        end
    end
end

function CLuaWarehousePage:OnTakeOutInToAnNang()
    local repoIdx = self.selectedPos
    local itemId = self.selectedItemId
    Gac2Gas.MoveItemFromRepoToSimpleRepo(repoIdx, itemId)
    CItemInfoMgr.CloseItemInfoWnd()
end