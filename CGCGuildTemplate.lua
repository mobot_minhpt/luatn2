-- Auto Generated!!
local CGCGuildTemplate = import "L10.UI.CGCGuildTemplate"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumGuildChallengeStatus = import "L10.Game.EnumGuildChallengeStatus"
local LocalString = import "LocalString"
CGCGuildTemplate.m_Init_CS2LuaHook = function (this, info) 
    this.baseInfo = info
    this.idLabel.text = ""
    this.nameLabel.text = ""
    this.numLabel.text = ""
    this.leaderLabel.text = ""
    this.levelLabel.text = ""
    this.relationLabel.text = ""
    this.timeLabel.text = ""
    if info == nil then
        return
    end

    this.idLabel.text = tostring(info.guildId)
    this.nameLabel.text = info.guildName
    this.numLabel.text = System.String.Format("{0}/{1}", info.memberNum, info.maxMemberNum)
    this.leaderLabel.text = info.leaderName
    this.levelLabel.text = tostring(info.guildLevel)
    this:UpdateStatus()
end
CGCGuildTemplate.m_GetFightTimeString_CS2LuaHook = function (this) 
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(this.baseInfo.fightTime)
    local str = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("[c][{4}](预约{0}月{1}日{2}:{3})"), {time.Month, time.Day, time.Hour, math.floor(time.Minute / 10) > 1 and tostring(time.Minute) or System.String.Format("0{0}", time.Minute), this.statusInviteColor})
    return str
end
CGCGuildTemplate.m_UpdateStatus_CS2LuaHook = function (this) 
    repeat
        local default = this.baseInfo.status
        if default == (EnumGuildChallengeStatus.eInvited) then
            this.relationLabel.text = System.String.Format(LocalString.GetString("[c][{0}]已宣战待响应\n"), this.statusInviteColor)
            this.timeLabel.text = this:GetFightTimeString()
            break
        elseif default == (EnumGuildChallengeStatus.eInviting) then
            this.relationLabel.text = System.String.Format(LocalString.GetString("[c][{0}]被宣战待响应\n"), this.statusInviteColor)
            this.timeLabel.text = this:GetFightTimeString()
            break
        elseif default == (EnumGuildChallengeStatus.eFighting) or default == (EnumGuildChallengeStatus.eWaitForFight) then
            this.relationLabel.text = System.String.Format(LocalString.GetString("[c][{0}]宣战中\n"), this.statusInviteColor)
            this.timeLabel.text = this:GetFightTimeString()
            break
        else
            this.relationLabel.text = System.String.Format(LocalString.GetString("[c][{0}]无"), this.statusDefaultColor)
            this.timeLabel.text = ""
            break
        end
    until 1
end
