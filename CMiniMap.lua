-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCoroutineMgr = import "L10.Engine.CCoroutineMgr"
local CMiniMap = import "L10.UI.CMiniMap"
local CMiniMapMarkTemplate = import "L10.UI.CMiniMapMarkTemplate"
local CommonDefs = import "L10.Game.CommonDefs"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CScene = import "L10.Game.CScene"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CUITexture = import "L10.UI.CUITexture"
local CWorldEventMgr = import "L10.Game.CWorldEventMgr"
local DelegateFactory = import "DelegateFactory"
local EMinimapCategory = import "L10.UI.EMinimapCategory"
local EMinimapMarkType = import "L10.UI.EMinimapMarkType"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local L10 = import "L10"
local LocalString = import "LocalString"
local Object = import "System.Object"
local Quaternion = import "UnityEngine.Quaternion"
local Setting = import "L10.Engine.Setting"
local ShiJieShiJian_CronMessage2019 = import "L10.Game.ShiJieShiJian_CronMessage2019"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local Utility = import "L10.Engine.Utility"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local IdPartition = import "L10.Game.IdPartition"
local UISprite = import "UISprite"
local NGUIText = import "NGUIText"
local QnButton = import "L10.UI.QnButton"
local CWeatherMgr = import "L10.Game.CWeatherMgr"
local NGUITools = import "NGUITools"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local UITexture = import "UITexture"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local CPos = import "L10.Engine.CPos"
local PlayerInfo = import "L10.UI.CMiniMap+PlayerInfo"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CResourceMgr = import "L10.Engine.CResourceMgr"

CMiniMap.m_OnEnable_CS2LuaHook = function (this)

    CLuaMiniMap:OnEnable(this)
    g_ScriptEvent:AddListener("UpdateYaYunBiaoCheMonster", CLuaMiniMap, "UpdateYaYunBiaoCheMonster")
    g_ScriptEvent:AddListener("WeatherIsChanged", CLuaMiniMap, "OnWeatherIsChanged")
    g_ScriptEvent:AddListener("MainPlayerCreated", CLuaMiniMap, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("MainPlayerSoulCoreCreate", CLuaMiniMap, "TryShowMainPlayerSoulCorePos")
    g_ScriptEvent:AddListener("OnSyncSectChangeName", CLuaMiniMap, "OnSyncSectChangeName")
    EventManager.AddListenerInternal(EnumEventType.ClientObjVisibleChanged, MakeDelegateFromCSFunction(this.OnClientObjVisibleChanged, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.ClientObjCreate, MakeDelegateFromCSFunction(this.OnClientObjCreate, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.ClientObjDestroy, MakeDelegateFromCSFunction(this.OnClientObjDestroy, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.ClientObjDie, MakeDelegateFromCSFunction(this.OnClientObjDie, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.AddListener(EnumEventType.UpdatePlayerForceInfo, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.AddListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.AddListener(EnumEventType.AtmosphereUpdate, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerMoveStepped, MakeDelegateFromCSFunction(this.UpdatePlayerMapPos, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.ChangeTarget, MakeDelegateFromCSFunction(this.ChangeTarget, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListener(EnumEventType.OnMainPlayerInGamePlayCopyInfoChange, MakeDelegateFromCSFunction(this.UpdateBranchName, Action0, this))
    EventManager.AddListener(EnumEventType.OnSendExtraTrapInfo, MakeDelegateFromCSFunction(this.InitTeleport, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.LLWSyncWindDirection, MakeDelegateFromCSFunction(this.LLWSyncWindDirection, MakeGenericClass(Action1, String), this))
    EventManager.AddListener(EnumEventType.TeamInfoChange, MakeDelegateFromCSFunction(this.InitMiniMapObjs, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.UpdateGuildLeagueFlagPos, MakeDelegateFromCSFunction(this.OnUpdateGuildLeagueFlagPos, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))

    EventManager.AddListenerInternal(EnumEventType.UpdateObjPosInfo, MakeDelegateFromCSFunction(this.UpdatePlayerInfos, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))

    EventManager.AddListenerInternal(EnumEventType.UpdateWeekendFightStatus, MakeDelegateFromCSFunction(this.UpdateWeekendFightStatus, MakeGenericClass(Action1, Boolean), this))

    EventManager.AddListener(EnumEventType.UpdateWorldEvent, MakeDelegateFromCSFunction(this.UpdateFxInfo, Action0, this))

    UIEventListener.Get(this.bgTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.bgTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnMapClick, VoidDelegate, this), true)
    
    this.m_UpdateNpcTick = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function ()
        this:OnUpdatePos()
    end), 1000, ETickType.Loop)

    this.sceneNameButton.OnClick = DelegateFactory.Action_QnButton(function (button)
        if LuaGuildTerritorialWarsMgr:IsInPlay() then
            CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsSceneChosenWnd)
            return
        end

        if LuaJuDianBattleMgr:IsInGameplay() then
            --CUIManager.ShowUI(CLuaUIResources.JuDianBattleSceneChooseWnd)
            return
        else
            Gac2Gas.RequestTeleportPublicCopy()
        end
        
    end)
	this:UpdateFxInfo()
    this:InitMiniMap()
end
CMiniMap.m_OnDisable_CS2LuaHook = function (this)

    CLuaMiniMap:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateYaYunBiaoCheMonster", CLuaMiniMap, "UpdateYaYunBiaoCheMonster")
    g_ScriptEvent:RemoveListener("WeatherIsChanged", CLuaMiniMap, "OnWeatherIsChanged")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", CLuaMiniMap, "OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("MainPlayerDestroyed", CLuaMiniMap, "OnMainPlayerDestroyed")
    g_ScriptEvent:RemoveListener("MainPlayerSoulCoreCreate", CLuaMiniMap, "TryShowMainPlayerSoulCorePos")
    g_ScriptEvent:RemoveListener("OnSyncSectChangeName", CLuaMiniMap, "OnSyncSectChangeName")
    EventManager.RemoveListenerInternal(EnumEventType.ClientObjVisibleChanged, MakeDelegateFromCSFunction(this.OnClientObjVisibleChanged, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.ClientObjCreate, MakeDelegateFromCSFunction(this.OnClientObjCreate, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.ClientObjDestroy, MakeDelegateFromCSFunction(this.OnClientObjDestroy, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.ClientObjDie, MakeDelegateFromCSFunction(this.OnClientObjDie, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.RemoveListener(EnumEventType.UpdatePlayerForceInfo, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.RemoveListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.RemoveListener(EnumEventType.AtmosphereUpdate, MakeDelegateFromCSFunction(this.InitMiniMap, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerMoveStepped, MakeDelegateFromCSFunction(this.UpdatePlayerMapPos, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.ChangeTarget, MakeDelegateFromCSFunction(this.ChangeTarget, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListener(EnumEventType.OnMainPlayerInGamePlayCopyInfoChange, MakeDelegateFromCSFunction(this.UpdateBranchName, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnSendExtraTrapInfo, MakeDelegateFromCSFunction(this.InitTeleport, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.LLWSyncWindDirection, MakeDelegateFromCSFunction(this.LLWSyncWindDirection, MakeGenericClass(Action1, String), this))

    EventManager.RemoveListenerInternal(EnumEventType.UpdateGuildLeagueFlagPos, MakeDelegateFromCSFunction(this.OnUpdateGuildLeagueFlagPos, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))
    EventManager.RemoveListener(EnumEventType.TeamInfoChange, MakeDelegateFromCSFunction(this.InitMiniMapObjs, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateObjPosInfo, MakeDelegateFromCSFunction(this.UpdatePlayerInfos, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))

    EventManager.RemoveListenerInternal(EnumEventType.UpdateWeekendFightStatus, MakeDelegateFromCSFunction(this.UpdateWeekendFightStatus, MakeGenericClass(Action1, Boolean), this))

    EventManager.RemoveListener(EnumEventType.UpdateWorldEvent, MakeDelegateFromCSFunction(this.UpdateFxInfo, Action0, this))

    if this.bgTexture ~= nil then
        UIEventListener.Get(this.bgTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.bgTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnMapClick, VoidDelegate, this), false)
    end
    if this.sceneNameButton ~= nil then
        this.sceneNameButton.OnClick = nil
    end
    if this.m_UpdateNpcTick ~= nil then
        invoke(this.m_UpdateNpcTick)
        this.m_UpdateNpcTick = nil
    end
    if this.m_QueryPlayerTick ~= nil then
        invoke(this.m_QueryPlayerTick)
        this.m_QueryPlayerTick = nil
    end
    CommonDefs.DictIterate(this.m_PlayerMapMarkDict, DelegateFactory.Action_object_object(function (___key, ___value)
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        if kv.Value ~= nil then
            this:RecycleMipmapMark(kv.Value.gameObject)
        end
    end))
    CommonDefs.DictClear(this.m_PlayerInfos)
    CommonDefs.DictClear(this.m_PlayerMapMarkDict)
end
CMiniMap.m_UpdateFxInfo_CS2LuaHook = function (this)
    if CWorldEventMgr.Inst.needPlayFx then
        if CScene.MainScene and IdPartition.IdIsCopyMap(CScene.MainScene.SceneTemplateId) then
            this.m_FxNode:DestroyFx()
            return
        end

        local data = ShiJieShiJian_CronMessage2019.GetData(CWorldEventMgr.Inst.worldMessageIdx)
        if data ~= nil then
            if not System.String.IsNullOrEmpty(data.MinimapEffect) then
                this.m_FxNode:LoadFx(data.MinimapEffect)
            end
		else
			this.m_FxNode:DestroyFx()
        end

    end

	local trans = this.transform:Find("Anchor/Offset/Notice")
	if trans ~= nil then
		local go = trans.gameObject
		go:SetActive(CLuaWorldEventMgr.isShowRedPoint)
		if CLuaWorldEventMgr.isShowRedPoint then
			local noticeclick = function (go)
				CLuaWorldEventMgr.OpenWorldEventHisWnd()
				go:SetActive(false)
			end
			CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(noticeclick), false)
		end
	end
end

--自动转Lua的时候，千万要把这个加回来啊，要不然会出事
CMiniMap.m_OnMapClick_CS2LuaHook = function (this)
    local CUIManager = import "L10.UI.CUIManager"
    local CUIResources = import "L10.UI.CUIResources"
    local CScene = import "L10.Game.CScene"
    local CCityWarMgr = import "L10.Game.CCityWarMgr"
    if CScene.MainScene and CCityWarMgr.Inst:IsInCityWarScene() then
        CLuaCityWarMgr:OpenSecondaryMap(CScene.MainScene.SceneTemplateId)
        return
    end
    if CCityWarMgr.Inst:IsInCityWarCopyScene() then
        CUIManager.ShowUI(CLuaUIResources.CityWarCopySecondaryMapWnd)
        return
    end
    if LuaGuildTerritorialWarsMgr:IsInPlay() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay() or LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
        LuaGuildTerritorialWarsMgr:ShowSmallMap()
        return
    end
    --2020七夕副本不允许点击
    --2022万圣节红绿灯玩法不允许点击
    if CScene.MainScene and (CScene.MainScene.GamePlayDesignId == 51101665 or CScene.MainScene.GamePlayDesignId == 51102975) then
        return
    end

    if CScene.MainScene and CScene.MainScene.GamePlayDesignId == NavalWar_Setting.GetData().PvpSeaPlayId then
        CUIManager.ShowUI(CLuaUIResources.TreasureSeaMiniMapWnd)
        return
    end


    CUIManager.ShowUI(CUIResources.MiniMapPopWnd)
end

CMiniMap.m_hookUpdateGuanNingCommanderInfo = function (this, playerData)
    --[[
    if not CGuanNingMgr.Inst.inGnjc then
        return
    end
    if playerData then
        for i = 0, playerData.Count - 1, 6 do
            local playerId = math.floor(tonumber(playerData[i] or 0))
            --local engineId = math.floor(tonumber(playerData[i + 1] or 0))
            local x = math.floor(tonumber(playerData[i + 2] or 0))
            local z = math.floor(tonumber(playerData[i + 3] or 0))

            --检查关宁指挥的位置
            CLuaGuanNingMgr:UpdateCommanderMapIcon(playerId, x, z, this, nil)
        end
    end
    --]]              
end

--------------------
-- 扩展CMiniMap,用于监听事件
--------------------

CLuaMiniMap = {}
CLuaMiniMap.m_Wnd = nil
CLuaMiniMap.m_PinPoints = nil
CLuaMiniMap.m_PinPointColors = nil

CLuaMiniMap.m_WeatherPanel = nil
CLuaMiniMap.m_WeatherBtn = nil
CLuaMiniMap.m_WeatherNameLab = nil
CLuaMiniMap.m_WeatherIcon = nil
CLuaMiniMap.m_WeatherIconUpBg1 = nil
CLuaMiniMap.m_WeatherIconDownBg1 = nil
CLuaMiniMap.m_WeatherIconUpBg2 = nil
CLuaMiniMap.m_WeatherIconDownBg2 = nil
CLuaMiniMap.m_WeatherUpEffect = nil
CLuaMiniMap.m_WeatherDownEffect = nil
CLuaMiniMap.m_SoulCoreObjInfo = nil
CLuaMiniMap.m_CommonMarks = {}

function CLuaMiniMap:GetGuideGo( _go, methodName)
    if(self.m_Wnd ~= nil)then
        if (methodName == "GetCoordinateLabel") then
            return self.m_Wnd.sceneNameButton.gameObject
        elseif (methodName == "GetSelfNode") then
            return self.m_Wnd.bgTexture.gameObject
        elseif methodName == "GetSelfNodeWithGnjcIconExist" then
            if CGuanNingMgr.Inst.inGnjc and CLuaGuanNingMgr.m_InCounterattackGuide then
                return self.m_Wnd.bgTexture.gameObject
            else
                return nil
            end
        else
            return nil
        end
    else
        return nil
    end
end

function CLuaMiniMap:OnEnable(wnd)
    self.m_Wnd = wnd

    CLuaMiniMap.m_WeatherPanel      = wnd.transform:Find("Anchor/Offset/WeatherPanel").gameObject
    CLuaMiniMap.m_WeatherBtn        = wnd.transform:Find("Anchor/Offset/WeatherPanel/Weather"):GetComponent(typeof(QnButton))
    CLuaMiniMap.m_WeatherNameLab    = wnd.transform:Find("Anchor/Offset/WeatherPanel/Weather/Name"):GetComponent(typeof(UILabel))
    CLuaMiniMap.m_WeatherIcon       = wnd.transform:Find("Anchor/Offset/WeatherPanel/Weather/Icon"):GetComponent(typeof(CUITexture))
    CLuaMiniMap.m_WeatherIconUpBg1  = wnd.transform:Find("Anchor/Offset/WeatherPanel/Weather/Icon/UpBg"):GetComponent(typeof(UITexture))
    CLuaMiniMap.m_WeatherIconDownBg1= wnd.transform:Find("Anchor/Offset/WeatherPanel/Weather/Icon/DownBg"):GetComponent(typeof(UITexture))
    CLuaMiniMap.m_WeatherIconUpBg2  = wnd.transform:Find("Anchor/Offset/WeatherPanel/UpBg"):GetComponent(typeof(UITexture))
    CLuaMiniMap.m_WeatherIconDownBg2= wnd.transform:Find("Anchor/Offset/WeatherPanel/DownBg"):GetComponent(typeof(UITexture))
    CLuaMiniMap.m_WeatherUpEffect   = wnd.transform:Find("Anchor/Offset/WeatherPanel/Weather/UpEffect").gameObject
    CLuaMiniMap.m_WeatherDownEffect = wnd.transform:Find("Anchor/Offset/WeatherPanel/Weather/DownEffect").gameObject

    CLuaMiniMap.m_WeatherBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        CUIManager.ShowUI(CLuaUIResources.MiniMapWeatherTipWnd)
    end)

    g_ScriptEvent:AddListener("SyncWeddingSkyBox", self, "OnSyncWeddingSkyBox")
    g_ScriptEvent:AddListener("SyncHuluwaBossPosition", self, "SyncHuluwaBossPosition")
    g_ScriptEvent:AddListener("SyncGnjcZhiHuiFaZhenPosition", self, "OnSyncGnjcZhiHuiFaZhenPosition")
    g_ScriptEvent:AddListener("SendGnjcCounterattackNpcInfo", self, "OnSendGnjcCounterattackNpcInfo")
    g_ScriptEvent:AddListener("SFEQ_XW_SyncTargetPos", self, "OnSyncQiXi2022TargetPos")
    g_ScriptEvent:AddListener("SyncMiWuLingFogClear", self, "OnSyncMiWuLingFogClear")
    g_ScriptEvent:AddListener("UpdateMiniMapMark", self, "UpdateMiniMapMark")
    g_ScriptEvent:AddListener("RefreshMiniMapMarks", self, "RefreshMiniMapMarks")
end

function CLuaMiniMap:OnDisable()
    self.m_Wnd = nil
    g_ScriptEvent:RemoveListener("SyncWeddingSkyBox", self, "OnSyncWeddingSkyBox")
    g_ScriptEvent:RemoveListener("SyncHuluwaBossPosition", self, "SyncHuluwaBossPosition")
    g_ScriptEvent:RemoveListener("SyncGnjcZhiHuiFaZhenPosition", self, "OnSyncGnjcZhiHuiFaZhenPosition")
    g_ScriptEvent:RemoveListener("SendGnjcCounterattackNpcInfo", self, "OnSendGnjcCounterattackNpcInfo")
    g_ScriptEvent:RemoveListener("SFEQ_XW_SyncTargetPos", self, "OnSyncQiXi2022TargetPos")
    g_ScriptEvent:RemoveListener("SyncMiWuLingFogClear", self, "OnSyncMiWuLingFogClear")
    g_ScriptEvent:RemoveListener("UpdateMiniMapMark", self, "UpdateMiniMapMark")
    g_ScriptEvent:RemoveListener("RefreshMiniMapMarks", self, "RefreshMiniMapMarks")
end

function CLuaMiniMap:OnSyncQiXi2022TargetPos(targetX, targetY, show)
    local this = self.m_Wnd
    local worldPos = Utility.GridPos2WorldPos(targetX, targetY)
    local playerMapPos = this:CalculateMapPos(worldPos)
    local root = this.transform:Find('Anchor/Offset/Panel/Map/QiXi2022Target')
    if root then
        root.gameObject:SetActive(show)
        root.transform.localPosition = playerMapPos
    end
end

function CLuaMiniMap:OnSyncGnjcZhiHuiFaZhenPosition(x1, y1, x2, y2) -- 分别是指挥和法阵的位置
    --Commander
    if x1 and y1 then
        if not CLuaGuanNingMgr.m_CommanderMapIcon[1] then
            local icon = { GuanNing_Setting.GetData().CommandIconResourceBlue, GuanNing_Setting.GetData().CommandIconResourceRed }
            local templateNode = self.m_Wnd.mapTexture.transform:Find("CommonItemNode").gameObject
            templateNode:SetActive(false)
            local go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, templateNode)
            go:SetActive(false)
            local i = CLuaGuanNingMgr:GetMyForce() + 1
            if icon[i] then
                go.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadMaterial(icon[i])
                local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
                uiTexture.width = 32
                uiTexture.height = 32
            end
            CLuaGuanNingMgr.m_CommanderMapIcon[1] = go
        end
        if x1 == 0 and y1 == 0 then
            CLuaGuanNingMgr.m_CommanderMapIcon[1]:SetActive(false)
        else
            local Pos3d = Utility.GridPos2WorldPos(x1, y1)
            local lcPos = self.m_Wnd:CalculateMapPos(Pos3d)
            CLuaGuanNingMgr.m_CommanderMapIcon[1].transform.localPosition = lcPos
            CLuaGuanNingMgr.m_CommanderMapIcon[1].transform.localRotation = Quaternion.identity
            CLuaGuanNingMgr.m_CommanderMapIcon[1].transform.localScale = Vector3.one
            CLuaGuanNingMgr.m_CommanderMapIcon[1]:SetActive(true)
        end
    end
    --FaZhen
    if x2 and y2 then
        if not CLuaGuanNingMgr.m_CommanderMapIcon[2] then
            local templateNode = self.m_Wnd.mapTexture.transform:Find("CommonItemNode").gameObject
            templateNode:SetActive(false)
            local go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, templateNode)
            go:SetActive(false)
            go.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/guanningcommandwnd_zengyifazhen.mat")
            local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
            uiTexture.width = 32
            uiTexture.height = 32
            CLuaGuanNingMgr.m_CommanderMapIcon[2] = go
        end
        if x2 == 0 and y2 == 0 then
            CLuaGuanNingMgr.m_CommanderMapIcon[2]:SetActive(false)
        else
            local Pos3d = Utility.GridPos2WorldPos(x2, y2)
            local lcPos = self.m_Wnd:CalculateMapPos(Pos3d)
            CLuaGuanNingMgr.m_CommanderMapIcon[2].transform.localPosition = lcPos
            CLuaGuanNingMgr.m_CommanderMapIcon[2].transform.localRotation = Quaternion.identity
            CLuaGuanNingMgr.m_CommanderMapIcon[2].transform.localScale = Vector3.one
            CLuaGuanNingMgr.m_CommanderMapIcon[2]:SetActive(true)
        end
    end
end

function CLuaMiniMap:OnSendGnjcCounterattackNpcInfo(index, force, gridX, gridY)
    --[[if not CLuaGuanNingMgr.m_CounterattackMapIcon[index] then
        local icon = { "UI/Texture/Transparent/Material/minimap_haojiao.mat", "UI/Texture/Transparent/Material/minimap_zhangu.mat" }
        local templateNode = self.m_Wnd.mapTexture.transform:Find("CommonItemNode").gameObject
        templateNode:SetActive(false)
        local go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, templateNode)
        go:SetActive(false)
        if icon[index] then
            go.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadMaterial(icon[index])
            local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
            uiTexture.width = 32
            uiTexture.height = 32
        end
        CLuaGuanNingMgr.m_CounterattackMapIcon[index] = go
    end
    local Pos3d = Utility.GridPos2WorldPos(gridX, gridY)
    local lcPos = self.m_Wnd:CalculateMapPos(Pos3d)
    CLuaGuanNingMgr.m_CounterattackMapIcon[index].transform.localPosition = lcPos
    CLuaGuanNingMgr.m_CounterattackMapIcon[index].transform.localRotation = Quaternion.identity
    CLuaGuanNingMgr.m_CounterattackMapIcon[index].transform.localScale = Vector3.one
    CLuaGuanNingMgr.m_CounterattackMapIcon[index]:SetActive(true)
    --]]
    CLuaGuanNingMgr.m_InCounterattackGuide = true
    if CLuaGuanNingMgr:GetMyForce() == force then
        CLuaGuideMgr.TryTriggerGuanNingCounterattackWeak()
    else
        CLuaGuideMgr.TryTriggerGuanNingCounterattackStrong()
    end
end

function CLuaMiniMap:SyncHuluwaBossPosition(templateId,x,y)
    self:ClearHuluwaBoss()
    if not LuaHuLuWa2022Mgr.IsInRuYiDongPlay() then
        return
    end
    local data = Monster_Monster.GetData(templateId)
    if data then
        local templateNode = self.m_Wnd.mapTexture.transform:Find("CommonItemNode").gameObject
        templateNode:SetActive(false)
        local go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, templateNode)
        go:SetActive(true)
        local Pos3d = Utility.GridPos2WorldPos(x, y)
        go.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
        go.transform.localRotation = Quaternion.identity
        go.transform.localScale = Vector3.one
        go.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadMaterial(SafeStringFormat3("UI/Texture/Portrait/Material/%s.mat",data.HeadIcon))
        local uiTexture = go.transform:Find('icon'):GetComponent(typeof(UITexture))
        uiTexture.width = 64
        uiTexture.height = 64
        CLuaMiniMap.m_HuluwaBossMark = go
    end
end
function CLuaMiniMap:ClearHuluwaBoss()
    if CLuaMiniMap.m_HuluwaBossMark then
        GameObject.Destroy(CLuaMiniMap.m_HuluwaBossMark)
        CLuaMiniMap.m_HuluwaBossMark = nil
    end
end

function CLuaMiniMap:UpdateYaYunBiaoCheMonster()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local engineId = mainplayer.PlayProp.YaYunData.TeamBiaoCheEngineId

    self.m_Wnd:OnClientObjCreate(engineId)
end

function CLuaMiniMap:OnWeatherIsChanged()
    self:UpdateZongPaiWeather()
end

function CLuaMiniMap:OnMainPlayerCreated()
    self:UpdateZongPaiWeather()
end

function CLuaMiniMap:OnMainPlayerDestroyed()
    for _, obj in pairs(self.m_CommonMarks) do
        if not CommonDefs.IsNull(obj) then
            CResourceMgr.Inst:Destroy(obj)
        end
    end
    self.m_CommonMarks = {}
end

function CLuaMiniMap:InitWuJianDiYu()
    local gamePlayId=CScene.MainScene.GamePlayDesignId
    if (gamePlayId == LuaWuJianDiYuMgr.gamePlayId) then
        self.m_PinPointColors = {"e9cb3c", "f97e33", "4dccf7", "94d43f", "9f79ec"}
        local pin_icon_template = self.m_Wnd.mapTexture.transform:Find("PinPointTemplate").gameObject
        pin_icon_template:SetActive(false)
        if(CLuaMiniMap.m_PinPoints)then
            for k, v in ipairs(CLuaMiniMap.m_PinPoints) do
                GameObject.Destroy(v)
            end
        end
        CLuaMiniMap.m_PinPoints = {}
        for i = 1, 5, 1 do
            local go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, pin_icon_template)
            go:SetActive(false)
            table.insert(CLuaMiniMap.m_PinPoints, go)
        end
        self:UpdateWuJianDiYuPinPoints()
    end
end

function CLuaMiniMap:FreePinPoints()
    if(CLuaMiniMap.m_PinPoints)then
        for k, v in ipairs(CLuaMiniMap.m_PinPoints) do
            GameObject.Destroy(v)
        end
        CLuaMiniMap.m_PinPoints = nil
        self.m_PinPointColors = nil
    end
end

function CLuaMiniMap:UpdateWuJianDiYuPinPoints()
    if(self.m_Wnd and LuaWuJianDiYuMgr.m_PinDataCache and CLuaMiniMap.m_PinPoints)then
        local team_list = CTeamMgr.Inst.Members
        local _w = self.m_Wnd.mapTexture.width
        local _h = self.m_Wnd.mapTexture.height
        for k, v in ipairs(CLuaMiniMap.m_PinPoints) do
            v:SetActive(false)
        end
        for k, v in ipairs(LuaWuJianDiYuMgr.m_PinDataCache)do
            CLuaMiniMap.m_PinPoints[k].transform.localPosition = Vector3((v.x - 500) * _w / 1000, (v.y - 500) * _h / 1000, 0)
            CLuaMiniMap.m_PinPoints[k]:SetActive(true)
            local sprite = CommonDefs.GetComponentInChildren_GameObject_Type(CLuaMiniMap.m_PinPoints[k], typeof(UISprite))

            do

                for i = 0, (team_list.Count - 1), 1 do
                    if(team_list[i].m_MemberId == v.playerId)then
                        sprite.color = NGUIText.ParseColor24(self.m_PinPointColors[i + 1], 0)
                        break
                    end
                end
            end

        end
    end
end

CLuaMiniMap.m_SanxingFlags = nil

function CLuaMiniMap:InitSanxingFlag()
  local templateNode = self.m_Wnd.mapTexture.transform:Find("CommonItemNode").gameObject
  templateNode:SetActive(false)
  self:ClearSanxingFlag()
  CLuaMiniMap.m_SanxingFlags = {}
  for i = 1, 3 do
    local go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, templateNode)
    go:SetActive(false)
    table.insert(CLuaMiniMap.m_SanxingFlags, go)
  end
  self:UpdateSanxingFlagPos()
end

function CLuaMiniMap:ClearSanxingFlag()
  if CLuaMiniMap.m_SanxingFlags then
    for i,v in pairs(CLuaMiniMap.m_SanxingFlags) do
      GameObject.Destroy(v)
    end
    CLuaMiniMap.m_SanxingFlags = nil
  end
end

function CLuaMiniMap:UpdateSanxingFlagPos()
  if not LuaSanxingGamePlayMgr.FlagPos then
    return
  end

  local textureTable = {}
  textureTable[37000165] = 'UI/Texture/Transparent/Material/sanxing_qizhi_02.mat'
  textureTable[37000166] = 'UI/Texture/Transparent/Material/sanxing_qizhi_01.mat'
  textureTable[37000167] = 'UI/Texture/Transparent/Material/sanxing_qizhi_01.mat'

  local count = 1
  for i,v in pairs(LuaSanxingGamePlayMgr.FlagPos) do
    local node = CLuaMiniMap.m_SanxingFlags[count]
    count = count + 1
    if node ~= nil then
      node:SetActive(true)
      local Pos3d = Utility.GridPos2WorldPos(v[1], v[2])
      node.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
      node.transform.localRotation = Quaternion.identity
      node.transform.localScale = Vector3.one
      node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadMaterial(textureTable[i])
      local uiTexture = node.transform:Find('icon'):GetComponent(typeof(UITexture))
      uiTexture.width = 64
      uiTexture.height = 64
    end
  end
end
-- 宗派场景天气
function CLuaMiniMap:UpdateZongPaiWeather()
    local isZongPaiWeather = false
    local currentWeathers = ((CWeatherMgr.Inst or {}).m_CurrentWeather or {}).Weathers
    if currentWeathers ~= nil and currentWeathers.Length > 0 and not (CScene.MainScene and CScene.MainScene.SceneTemplateId ~= 16101788) then
        --宗门天气同一时间应只会存在一种
        local currentWeather = currentWeathers[0]

        local data = SoulCore_ZongPaiWeather.GetData(EnumToInt(currentWeather))
        if data ~= nil then
            isZongPaiWeather = true
            local icon = SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat", data.Icon)
            local type = data.Type

            self.m_WeatherIcon:LoadMaterial(icon)
            self.m_WeatherNameLab.text = data.Name

            local isUpBg = false
            if type == 1 then
                isUpBg = true
                self.m_WeatherDownEffect.gameObject:SetActive(false)

            elseif type == 2 then
                isUpBg = true

                local mainPlayer = (CClientMainPlayer.Inst or {})
                local wuXing = (mainPlayer.BasicProp or {}).MingGe
                self.m_WeatherDownEffect.gameObject:SetActive(wuXing ~= nil and CommonDefs.ListContains(data.ImpactList, typeof(UInt32), wuXing))

            elseif type == 3 then
                isUpBg = false
                self.m_WeatherDownEffect.gameObject:SetActive(true)
            end

            self.m_WeatherIconUpBg1.gameObject:SetActive(isUpBg)
            self.m_WeatherIconUpBg2.gameObject:SetActive(isUpBg)
            self.m_WeatherIconDownBg1.gameObject:SetActive(not isUpBg)
            self.m_WeatherIconDownBg2.gameObject:SetActive(not isUpBg)
        end
    end

    self.m_WeatherPanel:SetActive(isZongPaiWeather)
end

CMiniMap.m_InitMiniMap_CS2LuaHook = function (this)
    this:InitRandomMiniMapSetting()
    if not CRenderScene.Inst or not CClientMainPlayer.Inst or not CScene.MainScene then return end
    if this.m_LoadMiniMapCoroutine then
        CCoroutineMgr.Inst:CancelCoroutine(this.m_LoadMiniMapCoroutine)
    end
    local sceneName = CRenderScene.Inst:GetMiniMapResName()
    if sceneName == Setting.sDengLu_Level then return end
    if CommonDefs.IS_PUB_RELEASE and String.IsNullOrEmpty(sceneName) then
        return
    end
    this.mapTexture.transform:Find("SectView").gameObject:SetActive(false)
    if string.find(sceneName,"zilishimen") then
        local sectTrapList = CRenderScene.Inst:GetExtraSectTrapInfo()
        if sectTrapList and sectTrapList.Count > 0 then
            this.mapTexture.transform:Find("SectView").gameObject:SetActive(true)
        end
    end

    this.m_LoadMiniMapCoroutine = CCoroutineMgr.Inst:StartCoroutine(this:LoadMiniMap(sceneName))
    if this.m_LoadMiniMapCoroutine:IsReady() then
        this.m_LoadMiniMapCoroutine = nil
    end
    CLuaMiniMap:OnSyncMiWuLingFogClear(LuaHuanHunShanZhuangMgr.ShowFog)
    CLuaMiniMap:OnSyncWeddingSkyBox()
    CLuaMiniMap:InitQiXi2022Map(this)
    CLuaMiniMap:TryShowMainPlayerSoulCorePos()

    LuaGamePlayMgr:RefreshMiniMapMarks()
end

function CLuaMiniMap:InitQiXi2022Map(this)
    local root = this.transform:Find('Anchor/Offset/Panel/Map/QiXi2022Target')
    if root then
        root.gameObject:SetActive(false)
    end
end

function CLuaMiniMap:TryShowMainPlayerSoulCorePos()
    --判断一下场景
    if not LuaZongMenMgr:IsMySectScene() or LuaZongMenMgr.m_SelfSoulCoreGo == nil then
        if self.m_SoulCoreObjInfo ~= nil then
            GameObject.Destroy(self.m_SoulCoreObjInfo)
            self.m_SoulCoreObjInfo = nil
        end
        return
    end
    local soulcoreName = SafeStringFormat3(LocalString.GetString("%s的灵核"), (CClientMainPlayer.Inst or {}).Name)

    local obj = self.m_SoulCoreObjInfo
    if not obj then
        local markTemplate = self.m_Wnd.transform:Find("Anchor/Offset/Panel/Map/markTemplate").gameObject
        local go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, markTemplate)
        local miniMapMark = go:GetComponent(typeof(CMiniMapMarkTemplate))
        miniMapMark.EngineId = 1
        miniMapMark:UpdateView(
            EMinimapCategory.Normal, 
            EMinimapMarkType.Npc_Concerned, 
            soulcoreName, 
            "")
        go:SetActive(true)
        self.m_SoulCoreObjInfo = go
        obj = go
    end

    obj.transform.localPosition = self.m_Wnd:CalculateMapPos(LuaZongMenMgr.m_SelfSoulCoreGo.transform.position)
end

function CLuaMiniMap:OnSyncSectChangeName(newName)
    if self.m_Wnd then
        self.m_Wnd.sceneNameButton.Text = newName
    end
end

-- 根据婚礼场景天空盒更新缩略图
function CLuaMiniMap:OnSyncWeddingSkyBox()
    local skyboxId = LuaWeddingIterationMgr.skyboxId

    if self.m_Wnd and CScene.MainScene and CScene.MainScene.Res == "jiehun01" and skyboxId then
        local data = WeddingIteration_Skybox.GetData(skyboxId)
        if data then
            if self.m_Wnd.m_LoadMiniMapCoroutine then
                CCoroutineMgr.Inst:CancelCoroutine(self.m_Wnd.m_LoadMiniMapCoroutine)
            end
            self.m_Wnd.m_LoadMiniMapCoroutine = CCoroutineMgr.Inst:StartCoroutine(self.m_Wnd:LoadMiniMap(data.Res))
        end
    end
end

function CLuaMiniMap:OnSyncMiWuLingFogClear(showFog)
    if self.m_Wnd and LuaHuanHunShanZhuangMgr:IsInMiWuLing() then
        local res = showFog and "miwuling_miwu" or "miwuling"
        if self.m_Wnd.m_LoadMiniMapCoroutine then
            CCoroutineMgr.Inst:CancelCoroutine(self.m_Wnd.m_LoadMiniMapCoroutine)
        end
        self.m_Wnd.m_LoadMiniMapCoroutine = CCoroutineMgr.Inst:StartCoroutine(self.m_Wnd:LoadMiniMap(res))
    end
end

function CLuaMiniMap:UpdateMiniMapMark(engineId, info) 
    local go = self.m_CommonMarks[engineId]
    if info == nil then 
        if not CommonDefs.IsNull(go) then CResourceMgr.Inst:Destroy(go) end
        self.m_CommonMarks[engineId] = nil
    else
        self:SetMapMark(engineId, info)
    end
end

function CLuaMiniMap:RefreshMiniMapMarks(markInfos)
    if not markInfos then return end

    for id, obj in pairs(self.m_CommonMarks) do
        if not markInfos[id] then
            if not CommonDefs.IsNull(obj) then CResourceMgr.Inst:Destroy(obj) end
            self.m_CommonMarks[id] = nil
        end
    end

    for id, info in pairs(markInfos) do
        self:SetMapMark(id, info)
    end
end

function CLuaMiniMap:SetMapMark(id, info)
    local go = self.m_CommonMarks[id]

    if CommonDefs.IsNull(go) then
        local templateNode = self.m_Wnd.mapTexture.transform:Find("CommonItemNode").gameObject
        templateNode:SetActive(false)
        go = NGUITools.AddChild(self.m_Wnd.mapTexture.gameObject, templateNode)
        self.m_CommonMarks[id] = go
    end

    go.transform.localPosition = self.m_Wnd:CalculateMapPos(Utility.GridPos2WorldPos(info.x or 0, info.y or 0))
    go.transform.localRotation = Quaternion.Euler(0, 0, info.rotZ or 0)
    go.transform.localScale = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, info.scale or 1)

    local icon = go.transform:Find("icon"):GetComponent(typeof(CUITexture))
    icon.transform.localPosition = Vector3(info.offset and info.offset[1] and info.offset[1].x or 0, info.offset and info.offset[1] and info.offset[1].y or 0, 0)
    icon:LoadMaterial(info.res or "")
    if info.color then icon.color = info.color end
    if info.alpha then icon.alpha = info.alpha end
    if info.depth then icon.texture.depth = info.depth end

    local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
    uiTexture.width = info.size and info.size[1] and info.size[1].width or 32
    uiTexture.height = info.size and info.size[1] and info.size[1].height or 32
    
    go:SetActive(info.show == nil and true or info.show)
end