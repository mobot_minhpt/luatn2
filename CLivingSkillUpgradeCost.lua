-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CLivingSkillUpgradeCost = import "L10.UI.CLivingSkillUpgradeCost"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
CLivingSkillUpgradeCost.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.learnButton).onClick = MakeDelegateFromCSFunction(this.OnClickLearnButton, VoidDelegate, this)
    this.guildContriCtl.updateAction = MakeDelegateFromCSFunction(this.UpdateStatus, Action0, this)
    this.yinPiaoCtl.updateAction = MakeDelegateFromCSFunction(this.UpdateStatus, Action0, this)

    CLuaLifeSkillMgr.m_NeedCheckCostSilver = true
end
CLivingSkillUpgradeCost.m_OnClickLearnButton_CS2LuaHook = function (this, p) 

    local skillTypeData = LifeSkill_SkillType.GetData(CLivingSkillMgr.Inst.selectedSkill)
    local skillType = skillTypeData.Type
    if skillType == 5 then
        if not CSwitchMgr.EnableHouse then
            g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
            return
        end
    end

    local needSilver = CLivingSkillMgr.Inst:GetLivingSkillUpgradeMoney(CLivingSkillMgr.Inst.selectedSkillId)
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    if needSilver > freeSilver and CLuaLifeSkillMgr.m_NeedCheckCostSilver then
        --银票不足，将消耗银两XXXXX，是否确认消耗？
        local msg = SafeStringFormat3(LocalString.GetString("银票不足，将消耗银两%d，是否确认消耗？"), needSilver - freeSilver)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            CLuaLifeSkillMgr.m_NeedCheckCostSilver=false
            this:Learn()
            -- MakeDelegateFromCSFunction(this.Learn, Action0, this), 
        end),
        nil, nil, nil, false)
    else
        this:Learn()
    end
end
CLivingSkillUpgradeCost.m_Learn_CS2LuaHook = function (this) 
    local needSilver = CLivingSkillMgr.Inst:GetLivingSkillUpgradeMoney(CLivingSkillMgr.Inst.selectedSkillId)
    local silver = CClientMainPlayer.Inst.Silver + CClientMainPlayer.Inst.FreeSilver
    if silver >= needSilver then
        CLivingSkillMgr.Inst:UpgradeLifeSkill(CLivingSkillMgr.Inst.selectedSkill)
    end
end
CLivingSkillUpgradeCost.m_UpdateStatus_CS2LuaHook = function (this) 
    if this.yinPiaoCtl.moneyEnough and this.guildContriCtl.moneyEnough then
        CUICommonDef.SetActive(this.learnButton, true, true)
    else
        CUICommonDef.SetActive(this.learnButton, false, true)
        --生活技能引导阶段，如果不可用，则中断
        if CGuideMgr.Inst:IsInPhase(30) then
            CGuideMgr.Inst:EndCurrentPhase()
        end
    end
    local needSilver = CLivingSkillMgr.Inst:GetLivingSkillUpgradeMoney(CLivingSkillMgr.Inst.selectedSkillId)
    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    if freeSilver > needSilver then
        this.getMoneyHint:SetActive(false)
    else
        --顶级文字显示时 不显示银两提示
        if not this.topLevelLabel.gameObject.activeSelf then
            this.getMoneyHint:SetActive(true)
        end
    end
end
CLivingSkillUpgradeCost.m_UpdateGuildContribute_CS2LuaHook = function (this) 
    if CLivingSkillMgr.Inst:IsLivingSkillTopLevel(CLivingSkillMgr.Inst.selectedSkillId) then
        this.topLevelLabel.gameObject:SetActive(true)
        this.guildContriCtl.gameObject:SetActive(false)
        this.yinPiaoCtl.gameObject:SetActive(false)
        this.learnButton:SetActive(false)

        this.getMoneyHint:SetActive(false)
    else
        this.topLevelLabel.gameObject:SetActive(false)
        this.guildContriCtl.gameObject:SetActive(true)
        this.yinPiaoCtl.gameObject:SetActive(true)
        this.learnButton:SetActive(true)
        this.getMoneyHint:SetActive(true)
    end
    local cost = CLivingSkillMgr.Inst:GetLivingSkillUpgradeContri(CLivingSkillMgr.Inst.selectedSkillId)
    this.guildContriCtl:SetCost(cost)
    local cost2 = CLivingSkillMgr.Inst:GetLivingSkillUpgradeMoney(CLivingSkillMgr.Inst.selectedSkillId)
    this.yinPiaoCtl:SetCost(cost2)

    this:UpdateStatus()
end
