-- Auto Generated!!
local CGuildLeagueGuildInfoItem = import "L10.UI.CGuildLeagueGuildInfoItem"
local Constants = import "L10.Game.Constants"
CGuildLeagueGuildInfoItem.m_Init_CS2LuaHook = function (this, data, rowIndex) 
    if rowIndex % 2 == 0 then
        this.bgSprite.spriteName = Constants.EvenBgSprite
    else
        this.bgSprite.spriteName = Constants.OddBgSpirite
    end

    this.rankLabel.text = tostring(data.rank)
    this.nameLabel.text = data.guildName
    this.levelLabel.text = tostring(data.guildScale)
    this.numLabel.text = tostring(data.guildMemNum)
end
