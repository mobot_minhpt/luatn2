local CPostEffectMgr=import "L10.Engine.CPostEffectMgr"
local UIGrid=import "UIGrid"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local Vector3 = import "UnityEngine.Vector3"
local UILabel = import "UILabel"
local UIInput = import "UIInput"
local SoundManager = import "SoundManager"
local Object = import "System.Object"
local NGUIText = import "NGUIText"
local Extensions = import "Extensions"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CUITexture = import "L10.UI.CUITexture"
local CTaskDialogInfo = import "L10.Game.CConversationMgr+CTaskDialogInfo"
local Constants = import "L10.Game.Constants"
local Color = import "UnityEngine.Color"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CClientNpc = import "L10.Game.CClientNpc"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local UIRoot = import "UIRoot"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"

--这里面的选项其实没啥用
CLuaTaskDialogWnd = class()
RegistClassMember(CLuaTaskDialogWnd,"nameLabel")
RegistClassMember(CLuaTaskDialogWnd,"headPortrait")
RegistClassMember(CLuaTaskDialogWnd,"conversation")
RegistClassMember(CLuaTaskDialogWnd,"selectionsTable")
RegistClassMember(CLuaTaskDialogWnd,"template")
RegistClassMember(CLuaTaskDialogWnd,"minContentRegion")
RegistClassMember(CLuaTaskDialogWnd,"maxContentRegion")
RegistClassMember(CLuaTaskDialogWnd,"InputRegion")
RegistClassMember(CLuaTaskDialogWnd,"InputButton")
RegistClassMember(CLuaTaskDialogWnd,"WriteInput")
RegistClassMember(CLuaTaskDialogWnd,"AnchorNode")
RegistClassMember(CLuaTaskDialogWnd,"AnchorBigNode")
RegistClassMember(CLuaTaskDialogWnd,"dialog")
RegistClassMember(CLuaTaskDialogWnd,"progress")
RegistClassMember(CLuaTaskDialogWnd,"cancelAutoPlayTick")
RegistClassMember(CLuaTaskDialogWnd,"useBigShow")
RegistClassMember(CLuaTaskDialogWnd,"m_ShowChoices")
RegistClassMember(CLuaTaskDialogWnd,"m_ContainsBigNpcPortrait") --对话中是否包含半身立绘，即含有bigNpc:附带不小于0的数字
RegistClassMember(CLuaTaskDialogWnd,"m_CancelSoundPlayAction")
RegistClassMember(CLuaTaskDialogWnd,"m_NormalSoundButton")
RegistClassMember(CLuaTaskDialogWnd,"m_BigShowSoundLeftButton")
RegistClassMember(CLuaTaskDialogWnd,"m_BigShowSoundRightButton")
RegistClassMember(CLuaTaskDialogWnd,"m_NormalSoundSettingButton")
RegistClassMember(CLuaTaskDialogWnd,"m_BigShowSoundSettingLeftButton")
RegistClassMember(CLuaTaskDialogWnd,"m_BigShowSoundSettingRightButton")
RegistClassMember(CLuaTaskDialogWnd,"m_IsToricBegin")
RegistClassMember(CLuaTaskDialogWnd,"m_Npc2Expression")
RegistClassMember(CLuaTaskDialogWnd, "m_AnchorBigLabelDefaultY") -- AnchorBig对话正文的默认Y坐标
RegistClassMember(CLuaTaskDialogWnd, "m_AdaptationOffset") -- 安全区适配偏移量

function CLuaTaskDialogWnd:Awake()
    self.nameLabel = self.transform:Find("Anchor/Title/Label"):GetComponent(typeof(UILabel))
    self.headPortrait = self.transform:Find("Anchor/Portrait"):GetComponent(typeof(CUITexture))
    self.conversation = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
    self.selectionsTable = self.transform:Find("Anchor/Selections"):GetComponent(typeof(UITable))
    self.template = self.transform:Find("Anchor/ItemTemplate").gameObject
    self.template:SetActive(false)
    self.minContentRegion = {
        x = -823,
        y = 52,
        z = 1147,
        w = 127
    }
    self.maxContentRegion = {
        x = -878,
        y = 79,
        z = 1790,
        w = 180
    }
    self.InputRegion = self.transform:Find("Anchor/InputBottom").gameObject
    self.InputButton = self.transform:Find("Anchor/InputBottom/ZhanKai/SendButton").gameObject
    self.WriteInput = self.transform:Find("Anchor/InputBottom/ZhanKai/ChatInput"):GetComponent(typeof(UIInput))
    self.AnchorNode = self.transform:Find("Anchor").gameObject
    self.AnchorBigNode = self.transform:Find("AnchorBig").gameObject
    self.m_AnchorBigLabelDefaultY = self.AnchorBigNode.transform:Find("ContentLabel").localPosition.y
    self.dialog = {}
    self.progress = 0
    self.cancelAutoPlayTick = nil
    self.useBigShow = false
    self.m_IsToricBegin = false
    self.m_Npc2Expression = {}

    self.m_NormalSoundButton = self.transform:Find("Anchor/SoundButton").gameObject
    self.m_BigShowSoundLeftButton = self.transform:Find("AnchorBig/SoundButtonLeft").gameObject
    self.m_BigShowSoundRightButton = self.transform:Find("AnchorBig/SoundButtonRight").gameObject

    self.m_NormalSoundSettingButton = self.transform:Find("Anchor/SoundSettingButton").gameObject
    self.m_BigShowSoundSettingLeftButton = self.transform:Find("AnchorBig/SoundSettingButtonLeft").gameObject
    self.m_BigShowSoundSettingRightButton = self.transform:Find("AnchorBig/SoundSettingButtonRight").gameObject

    UIEventListener.Get(self.m_NormalSoundButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundButtonClick(go)
    end)
    UIEventListener.Get(self.m_BigShowSoundLeftButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundButtonClick(go)
    end)
    UIEventListener.Get(self.m_BigShowSoundRightButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundButtonClick(go)
    end)
    UIEventListener.Get(self.m_NormalSoundSettingButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, false)
    end)
    UIEventListener.Get(self.m_BigShowSoundSettingLeftButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, false)
    end)
    UIEventListener.Get(self.m_BigShowSoundSettingRightButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, true)
    end)
end

function CLuaTaskDialogWnd:IPhoneXAdaptation()
    if UIRoot.EnableIPhoneXAdaptation then
        self.m_AdaptationOffset = CThumbnailChatWnd.GetHeightOffsetForIphoneX()
    elseif CommonDefs.IsAndroidPlatform() then
        self.m_AdaptationOffset = 50
    else
        self.m_AdaptationOffset = 0
    end
    if self.m_AdaptationOffset > 0 then
        local w = self.InputRegion:GetComponent(typeof(UIWidget))
        if w then
            w.bottomAnchor.absolute = -47 + self.m_AdaptationOffset - 27
            w.topAnchor.absolute = 53 + self.m_AdaptationOffset - 27
            w:ResetAndUpdateAnchors()
        end
        w = self.AnchorBigNode.transform:Find("InputBottom"):GetComponent(typeof(UIWidget))
        if w then
            w.bottomAnchor.absolute = -25 + self.m_AdaptationOffset
            w.topAnchor.absolute = -23 + self.m_AdaptationOffset
            w:ResetAndUpdateAnchors()
        end
    end
end

function CLuaTaskDialogWnd:OnEnable()
    self:IPhoneXAdaptation()
    g_ScriptEvent:AddListener("ClientObjCreate", self, "OnClientObjCreate")

    local info = CConversationMgr.Inst.BaseInfo
    if info and TypeIs(info, typeof(CTaskDialogInfo)) then
        CLuaGuideMgr.TryTriggerEquipTipGuide(info.m_TaskId)
    end
end

function CLuaTaskDialogWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ClientObjCreate", self, "OnClientObjCreate")
end

function CLuaTaskDialogWnd:OnClientObjCreate(args)
	local engineId = args[0]
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        if TypeIs(obj, typeof(CClientNpc)) and self.m_Npc2Expression[obj.TemplateId] then
            obj:ShowExpressionActionState(self.m_Npc2Expression[obj.TemplateId])
        elseif TypeIs(obj, typeof(CClientMainPlayer)) and self.m_Npc2Expression[0] then
            obj:ShowExpressionActionState(self.m_Npc2Expression[0])
        end
    end
end

function CLuaTaskDialogWnd:SetSoundButtonVisibility(isBigShow, paragraph)
    local hasSound = paragraph and paragraph.fmodEvent~=nil and paragraph.fmodEvent~=""
    self.m_NormalSoundButton:SetActive(not isBigShow and hasSound)
    self.m_BigShowSoundLeftButton:SetActive(isBigShow and hasSound and cs_string.IsNullOrEmpty(paragraph.otherPortrait))
    self.m_BigShowSoundRightButton:SetActive(isBigShow and hasSound and (not cs_string.IsNullOrEmpty(paragraph.otherPortrait)))
    self.m_NormalSoundSettingButton:SetActive(not isBigShow and hasSound)
    self.m_BigShowSoundSettingLeftButton:SetActive(isBigShow and hasSound and cs_string.IsNullOrEmpty(paragraph.otherPortrait))
    self.m_BigShowSoundSettingRightButton:SetActive(isBigShow and hasSound and (not cs_string.IsNullOrEmpty(paragraph.otherPortrait)))
end

function CLuaTaskDialogWnd:OnSoundButtonClick()
    local paragraph = self.dialog and self.progress and self.dialog[self.progress+1] or nil
    if paragraph and paragraph.fmodEvent and paragraph.fmodEvent~="" then
        SoundManager.Inst:StartDialogSoundAndEnableSoundSetting(paragraph.fmodEvent)
    end
end

function CLuaTaskDialogWnd:OnSoundSettingButtonClick(go, isLeft)
    if not CUIManager.IsLoaded(CLuaUIResources.MiniVolumeSettingsWnd) then
        LuaMiniVolumeSettingsMgr:OpenWnd(go, go.transform.position, UIWidget.Pivot.Left)
    else
        CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
    end
end

function CLuaTaskDialogWnd:EnableBigNpcMode()
     -- 根据策划的需求：只要对话中有一句bigNpc不为-1，才开启虚化
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        if self.m_ContainsBigNpcPortrait and postEffectCtrl then
            postEffectCtrl.DepthOfFieldEnable = true
        end
    else
        if self.m_ContainsBigNpcPortrait and CPostEffectMgr.Instance then
            CPostEffectMgr.Instance.DepthOfFieldEnable = true
        end
    end
     
    -- CClientObjectRoot.Inst.gameObject:SetActive(false)
    self.useBigShow = true
    CameraFollow.Inst.m_IsBigNpcTalk = true

    CameraFollow.Inst:SetUIVisiable(false)
	local List_String = MakeGenericClass(List, cs_string)
    CUIManager.SetPopViewsVisiable(false, Table2List({"TaskDialog","ShowPictureWnd","BusinessMainWnd"}, List_String),"task_dialog")
end
function CLuaTaskDialogWnd:DisableBigNpcMode()
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        if self.m_ContainsBigNpcPortrait and postEffectCtrl then
            postEffectCtrl.DepthOfFieldEnable = false
        end
    else
        if self.m_ContainsBigNpcPortrait and CPostEffectMgr.Instance then
            CPostEffectMgr.Instance.DepthOfFieldEnable = false
        end
    end
    -- CClientObjectRoot.Inst.gameObject:SetActive(true)
    self.useBigShow = false
    CameraFollow.Inst.m_IsBigNpcTalk = false

    if not CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) then
        if not CameraFollow.Inst.IsAICamera and not CameraFollow.Inst.IsBezierCamera and not CameraFollow.Inst.IsToricSpaceCamera then
            CameraFollow.Inst:SetUIVisiable(true)
            CUIManager.SetPopViewsVisiable(true, nil,"task_dialog")
        else
            --aicamera的时候把headinfownd，commonuifxwnd显示出来
            CUIManager.ShowUIByChangeLayer(CUIResources.HeadInfoWnd,false,false)
            CUIManager.ShowUIByChangeLayer(CUIResources.CommonUIFxWnd,false,false)
        end
    end
end

function CLuaTaskDialogWnd:Init( )
    self.m_ShowChoices = false

    UIEventListener.Get(self.transform:Find("_BgMask_").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBgMaskClick(go)
    end)
    local info = CConversationMgr.Inst.BaseInfo
    local text = info.message or ""

    local regex = "<popup%s(%w+):(-?%d+,?%d*)%s?([^>]*)>([^<]*)</popup>"
    if not (text==nil or text=="") and not (string.find(text,"popup")) then
        text = SafeStringFormat3("<popup npc:0>%s</popup>", text)
    end

    self.dialog = {}
    self:CheckContainsBigNpcPortrait(text)
    if string.find(text,"bigNpc") then
        self:EnableBigNpcMode()
    end

    for item1,item2,item3,item4 in string.gmatch(text, regex) do
        local p = {
            playerType = 0,
            emotionId = 0,
            writeId = 0,
            otherPlayerType = 0,
            otherEmotionId = 0,
            dialogBgType = 0,
            expression = {},
        }
        if (item1 == "player") then
            p.playerType = 1
        elseif (item1 == "npc") then
            p.playerType = 2
        elseif (item1 == "bigNpc") then
            p.playerType = 3
        else
            error("wrong playerType",item1)
        end

        p.name = nil
        p.portrait = Constants.DefaultNPCPortrait
        p.emotionId = 0
        p.fmodEvent = nil
        p.toricCameraParams = nil

        local id = 0
        local firstTalk = item2
        if (string.find(firstTalk, ",") ~= nil) then
            local emotionId
            id, emotionId = string.match(firstTalk, "(%d+),(%d+)")
            id, emotionId = tonumber(id or 0),tonumber(emotionId or 0)
            p.emotionId = emotionId
        else
            id = tonumber(firstTalk)
        end

        local extraInfo =item3
        if not (extraInfo==nil or extraInfo=="") then
            local fmodEvent = string.match(extraInfo, "audio:(event:.+)")
            if fmodEvent then
                p.fmodEvent = fmodEvent
            end

            local emotionId = string.match(extraInfo, "emotion:(%d+)")
            if emotionId then
                p.emotionId = math.floor(tonumber(emotionId))
            end

            local writeId = string.match(extraInfo, "write:(%d+)")
            if writeId then
                p.writeId = math.floor(tonumber(writeId))
            end

            local npcId1,npcId2,alpha,theta,phi,duration = string.match(extraInfo, "toriccamera:(%d+),(%d+),([0-9%.]+),(-?[0-9%.]+),(-?[0-9%.]+),([0-9%.]+)")
            
            if npcId1 and npcId2 and alpha and theta and phi and duration then
                local toricCameraParams = {}
                toricCameraParams.npcId1 = tonumber(npcId1)
                toricCameraParams.npcId2 = tonumber(npcId2)
                toricCameraParams.alpha = alpha
                toricCameraParams.theta = theta
                toricCameraParams.phi = phi
                toricCameraParams.duration = duration
                local noSway = false
                _,_,_,_,_,_,noSway = string.match(extraInfo, "toriccamera:(%d+),(%d+),([0-9%.]+),(-?[0-9%.]+),(-?[0-9%.]+),([0-9%.]+),(%d+)")
                if noSway and noSway == "1" then
                    noSway = true
                else
                    noSway = false
                end
                toricCameraParams.noSway = noSway
                p.toricCameraParams = toricCameraParams
            end

            local expressionStr = string.match(extraInfo, "expression:([^%s]+)")
            if expressionStr then
                for expressionId, npcStr in string.gmatch(expressionStr, "(%d+),([^;]+)") do
                    for npcId in string.gmatch(npcStr, "%d+") do
                        p.expression[math.floor(tonumber(npcId))] = math.floor(tonumber(expressionId))
                    end
                end
            end

            local p1,p2 = string.match(extraInfo, "other:(%w+):(%d+,?%d*)")
            if p1 then
                if p1 == "player" then
                    p.otherPlayerType = 1
                elseif p1 == "npc" then
                    p.otherPlayerType = 2
                elseif p1 == "bigNpc" then
                    p.otherPlayerType = 3
                end

                local other_id = 0
                local otherTalk = p2
                if (string.find(otherTalk, ",", 1, true) ~= nil) then
                    local otherEmotionId
                    other_id, otherEmotionId = string.match(otherTalk, "(%d+),(%d+)")
                    other_id, otherEmotionId = tonumber(other_id or 0),tonumber(otherEmotionId or 0)
                    p.otherEmotionId = otherEmotionId
                else
                    other_id = tonumber(otherTalk)
                end

                local other_name, other_portrait = self:GetPortraitName(p.otherPlayerType, other_id)
                p.otherPortrait = other_portrait
                p.otherName = other_name

                if p.otherPortrait ~= Constants.DefaultNPCPortrait and p.otherEmotionId > 0 then
                    p.otherPortrait = SafeStringFormat3("%s_%02d", p.otherPortrait, p.otherEmotionId)
                end
            end
        end

        --设置头像及名称
        local name, portrait = self:GetPortraitName(p.playerType, id)
        p.portrait = portrait
        p.name = name

        if p.portrait ~= Constants.DefaultNPCPortrait and p.emotionId > 0 then
            p.portrait = SafeStringFormat3("%s_%02d", p.portrait, p.emotionId)
        end
        --设置内容
        p.content = item4
        table.insert( self.dialog,p )
    end

    self.progress = 0
    self:ShowContent(false)
    if self.cancelAutoPlayTick ~= nil then
        UnRegisterTick(self.cancelAutoPlayTick)
        self.cancelAutoPlayTick = nil
    end
    local storyDialogInfo = CConversationMgr.Inst.StoryDialogInfo
    if storyDialogInfo ~= nil and storyDialogInfo.autoPlay then
        self.cancelAutoPlayTick = RegisterTick(function () 
            self:ShowContent(true)
        end, storyDialogInfo.duration)
    end
end
function CLuaTaskDialogWnd:CheckContainsBigNpcPortrait(text)
    if string.find(text,"bigNpc:%d+") then
        self.m_ContainsBigNpcPortrait = true
    else
        self.m_ContainsBigNpcPortrait = false
    end
end
function CLuaTaskDialogWnd:GetPortraitName( _type, id) 
    --填-1则不显示
    if id==-1 then return end
    local name = ""
    local portrait = ""
    if _type == 1 then --player
        if id == 0 then
            --主角自己的头像
            if CClientMainPlayer.Inst ~= nil then
                name = CClientMainPlayer.Inst.Name
                if TypeIs(CConversationMgr.Inst.BaseInfo, typeof(CTaskDialogInfo)) then
                    local taskID = 0
                    if CZhuJueJuQingMgr.Instance:IsTaskZhujuejuqing(taskID) then
                        name = CZhuJueJuQingMgr.Instance:GetSelfJuqing().Name
                    end
                end
                portrait = CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1)
                --使用默认头像
            end
        else
            --从Player表读取其他角色头像
            local data = Initialization_Init.GetData(id)
            if data ~= nil then
                name = data.Name
                portrait = data.ResName
            end
        end
    elseif _type >= 2 then
        --从NPC表读取头像
        local curNpcId = 0
        local curNpcEngineId = 0
        if TypeIs(CConversationMgr.Inst.BaseInfo, typeof(CTaskDialogInfo)) then
            curNpcId = (TypeAs(CConversationMgr.Inst.BaseInfo, typeof(CTaskDialogInfo))).m_NpcId
            curNpcEngineId = (TypeAs(CConversationMgr.Inst.BaseInfo, typeof(CTaskDialogInfo))).m_NpcEngineId
        end
        local npcId = id == 0 and curNpcId or id

        local data = NPC_NPC.GetData(npcId)
        if data ~= nil then
            name = data.Name
            if _type == 2 then
                if data.BabyAppearance == 1 then
                    local npc = CClientObjectMgr.Inst:GetObject(curNpcEngineId)
                    if npc ~= nil then
                        portrait = npc.PortraitName
                    end
                else
                    portrait = data.Portrait
                end
            elseif _type == 3 then
                portrait = data.BigPortrait
            end
        end
    end
    -- print(name,portrait)
    return name, portrait
end
function CLuaTaskDialogWnd:ShowBigNpc(paragraph)
    self.AnchorNode:SetActive(false)
    self.AnchorBigNode:SetActive(true)

    local portrait = self.AnchorBigNode.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    if paragraph.portrait then
        portrait:LoadBigNPCPortrait(paragraph.portrait, false)
    else
        portrait:Clear()
    end

    local nameLabel = self.AnchorBigNode.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = paragraph.name
    local nameLabel2 = self.AnchorBigNode.transform:Find("NameLabel2"):GetComponent(typeof(UILabel))
    local portrait2 = self.AnchorBigNode.transform:Find("Portrait2"):GetComponent(typeof(CUITexture))
    local portrait3 = self.AnchorBigNode.transform:Find("Portrait3"):GetComponent(typeof(CUITexture))
    local contentLabel = self.AnchorBigNode.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))

    if paragraph.otherPortrait==nil or paragraph.otherPortrait=="" then
        portrait.color = Color.white
        self.AnchorBigNode.transform:Find("Portrait2").gameObject:SetActive(false)
        nameLabel.gameObject:SetActive(true)
        nameLabel2.gameObject:SetActive(false)
        portrait2.gameObject:SetActive(false)
        portrait3.gameObject:SetActive(false)
    else
        portrait.color = Color(0.22, 0.22, 0.22, 0.86)--半透明
        nameLabel.gameObject:SetActive(false)
        nameLabel2.gameObject:SetActive(true)
        nameLabel2.text = paragraph.otherName

        if paragraph.otherPlayerType==3 then--bignpc
            portrait2.gameObject:SetActive(false)
            portrait3.gameObject:SetActive(true)
            portrait3:LoadBigNPCPortrait(paragraph.otherPortrait, false)
        else
            portrait2.gameObject:SetActive(true)
            portrait3.gameObject:SetActive(false)
            portrait2:LoadNPCPortrait(paragraph.otherPortrait, false)
        end
    end

    local write = ZhuJueJuQing_Write.GetData(paragraph.writeId)
    if write ~= nil and write.NeedInput == 1 then
        self.AnchorBigNode.transform:Find("InputBottom").gameObject:SetActive(true)
        UIEventListener.Get(self.AnchorBigNode.transform:Find("InputBottom/ZhanKai/SendButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnInputButtonClick(go)
        end)
        CommonDefs.GetComponent_Component_Type(self.AnchorBigNode.transform:Find("InputBottom/ZhanKai/ChatInput"), typeof(UIInput)).onReturnKeyPressed = DelegateFactory.Action_GameObject(function(go)
            self:OnInputButtonClick(go)
        end)
        CommonDefs.GetComponent_Component_Type(self.AnchorBigNode.transform:Find("InputBottom/ZhanKai/ChatInput"), typeof(UIInput)).value = ""
    elseif write ~= nil and write.NeedInput == 0 then
        self.AnchorBigNode.transform:Find("InputBottom").gameObject:SetActive(false)
        self:ShowHandWriteEffect(write)
    else
        self.AnchorBigNode.transform:Find("InputBottom").gameObject:SetActive(false)
    end
    -- 输入框出现时，正文提示输入的内容也要适配安全区
    if not self.m_AnchorBigLabelDefaultY then
        self.m_AnchorBigLabelDefaultY = contentLabel.transform.localPosition.y
    end
    if self.m_AdaptationOffset > 0 and write and write.NeedInput == 1 then
        LuaUtils.SetLocalPositionY(contentLabel.transform, self.m_AnchorBigLabelDefaultY + self.m_AdaptationOffset)
    else
        LuaUtils.SetLocalPositionY(contentLabel.transform, self.m_AnchorBigLabelDefaultY)
    end

    if paragraph.content then
        contentLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(self.conversation.color), CChatLinkMgr.TranslateToNGUIText(paragraph.content, false))
    else
        contentLabel.text = nil
    end

    if paragraph.fmodEvent then
        self.m_CancelSoundPlayAction = CLoadingWnd.Inst:PushDelayAction(DelegateFactory.Action(function () 
            SoundManager.Inst:StartDialogSound(paragraph.fmodEvent)
        end))
    end

    local selectionsTf = self.AnchorBigNode.transform:Find("Selections")
    local template = self.AnchorBigNode.transform:Find("ItemTemplate").gameObject
    template:SetActive(false)
    Extensions.RemoveAllChildren(selectionsTf)

    local selections = CConversationMgr.Inst.BaseInfo.selections
    if self.progress == #self.dialog - 1 and selections ~= nil and selections.Count > 0 then
        for i=1,selections.Count do
            local selectionGo = NGUITools.AddChild(selectionsTf.gameObject,template)
            selectionGo:SetActive(true)
            --是否显示图标
            local label = selectionGo.transform:Find("Label"):GetComponent(typeof(UILabel))
            local text = selections[i-1]
            if string.sub(text,1,5)=="<icon" then
                local a,b=string.match(text, "<icon%s(.+)/>(.*)")
                label.text = b
                local icon = selectionGo.transform:Find("Icon"):GetComponent(typeof(CUITexture))
                icon:LoadMaterial(a)
            else
                label.text = text
            end

            UIEventListener.Get(selectionGo).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnSelectionClick(go)
            end)
        end
        selectionsTf:GetComponent(typeof(UIGrid)):Reposition()
    end
end


function CLuaTaskDialogWnd:ShowContent( moveNext) 
    if not CUIManager.IsLoaded(CUIResources.TaskDialogWnd) then return end
    if moveNext then
        self.progress = self.progress + 1
    end
    if self.progress < #self.dialog then
        local paragraph = self.dialog[self.progress+1]
        self:SetSoundButtonVisibility(self.useBigShow, paragraph)
        if self.useBigShow then
            self:ShowBigNpc(paragraph)
        else
            self.AnchorNode:SetActive(true)
            self.AnchorBigNode:SetActive(false)

            self.nameLabel.text = paragraph.name
            self:LoadPortrait(paragraph.portrait)

            local write = ZhuJueJuQing_Write.GetData(paragraph.writeId)
            if write ~= nil and write.NeedInput == 1 then
                self.InputRegion:SetActive(true)
                UIEventListener.Get(self.InputButton).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnInputButtonClick(go)
                end)
                self.WriteInput.onReturnKeyPressed = DelegateFactory.Action_GameObject(function(go)
                    self:OnInputButtonClick(go)
                end)
                self.WriteInput.value = ""
            elseif write ~= nil and write.NeedInput == 0 then
                self.InputRegion:SetActive(false)
                self:ShowHandWriteEffect(write)
            else
                self.InputRegion:SetActive(false)
            end

            Extensions.RemoveAllChildren(self.selectionsTable.transform)
            local selections = CConversationMgr.Inst.BaseInfo.selections
            if self.progress == #self.dialog - 1 and selections ~= nil and selections.Count > 0 then
                self.conversation.transform.localPosition = Vector3(self.minContentRegion.x, self.minContentRegion.y, 0)
                self.conversation.width = math.floor(self.minContentRegion.z)
                self.conversation.height = math.floor(self.minContentRegion.w)

                for i=1,selections.Count do
                    local selectionGo = NGUITools.AddChild(self.selectionsTable.gameObject,self.template)
                    selectionGo:SetActive(true)
                    -- selectionGo.transform:Find("Label"):GetComponent(typeof(UILabel)).text = selections[i-1]
                    local label = selectionGo.transform:Find("Label"):GetComponent(typeof(UILabel))
                    local text = selections[i-1]
                    if string.sub(text,1,5)=="<icon" then
                        local a,b=string.match(text, "<icon%s(.+)/>(.*)")
                        label.text = b
                        local icon = selectionGo.transform:Find("Icon"):GetComponent(typeof(CUITexture))
                        icon:LoadMaterial(a)
                    else
                        label.text = text
                    end

                    UIEventListener.Get(selectionGo).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:OnSelectionClick(go)
                    end)
                end
                self.selectionsTable:Reposition()
            else
                self.conversation.transform.localPosition = Vector3(self.maxContentRegion.x, self.maxContentRegion.y, 0)
                self.conversation.width = math.floor(self.maxContentRegion.z)
                self.conversation.height = math.floor(self.maxContentRegion.w)
            end
            self.conversation.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(self.conversation.color), CChatLinkMgr.TranslateToNGUIText(paragraph.content, false))
            self.m_CancelSoundPlayAction = CLoadingWnd.Inst:PushDelayAction(DelegateFactory.Action(function () 
                SoundManager.Inst:StartDialogSound(paragraph.fmodEvent)
            end))
        end
        self:ProcessToricCamera(paragraph)
        self:ProcessExpression(paragraph)
    else
        self:StopSound()

        if CConversationMgr.Inst.BaseInfo and CConversationMgr.Inst.BaseInfo.onFinish ~= nil then
            invoke(CConversationMgr.Inst.BaseInfo.onFinish)
        end

        if self.cancelAutoPlayTick ~= nil then
            UnRegisterTick(self.cancelAutoPlayTick)
            self.cancelAutoPlayTick=nil
        end
        CConversationMgr.Inst:CloseAutoPlay()
    end
end
function CLuaTaskDialogWnd:OnSelectionClick( go) 
    local children = self.selectionsTable:GetChildList()
    do
        local i = 0
        while i < children.Count do
            if children[i]:Equals(go.transform) then
                if CConversationMgr.Inst.BaseInfo.onSelect ~= nil then
                    GenericDelegateInvoke(CConversationMgr.Inst.BaseInfo.onSelect, Table2ArrayWithCount({i}, 1, MakeArrayClass(Object)))
                end
                break
            end
            i = i + 1
        end
    end
end
function CLuaTaskDialogWnd:OnInputButtonClick( go) 
    local write = ZhuJueJuQing_Write.GetData(self.dialog[self.progress+1].writeId)
    if write ~= nil then
        local inputStr = StringTrim(self.WriteInput.value)
        if self.useBigShow then
            inputStr = StringTrim(CommonDefs.GetComponent_Component_Type(self.AnchorBigNode.transform:Find("InputBottom/ZhanKai/ChatInput"), typeof(UIInput)).value)
        end
        -- 替换所有空格, 空格等等
        string.gsub(inputStr, LocalString.GetString(" "), "")
        string.gsub(inputStr, LocalString.GetString("，"), "")
        string.gsub(inputStr, LocalString.GetString("。"), "")

        local needStr = StringTrim(write.Text)

        if (string.find(inputStr, needStr, 1, true) ~= nil) then
            self:ShowHandWriteEffect(write)
        else
            g_MessageMgr:ShowMessage("INPUT_NOT_MATCH")
        end
    end
end

function CLuaTaskDialogWnd:ShowHandWriteEffect(write)
    local textMat = write.TextMat
    local width = write.Width
    local height = write.Height
    CLuaHandWriteEffectMgr.ShowHandWriteEffect(self.dialog[self.progress+1].writeId, function() self:ShowContent(true) end)
end

function CLuaTaskDialogWnd:OnBgMaskClick( go) 
    local info = CConversationMgr.Inst.StoryDialogInfo
    if info ~= nil and info.autoPlay then
        return
    end
    --有选项的时候必须进行选择
    if self.progress == #self.dialog - 1 and CConversationMgr.Inst.BaseInfo.selections ~= nil and CConversationMgr.Inst.BaseInfo.selections.Count > 0 then
        return
    end
    --要求玩家输入/播放特效时屏蔽点击相应
    if self.progress < #self.dialog then
        if self.dialog[self.progress+1].writeId ~= 0 then
            return
        end
    end
    self:ShowContent(true)
end

function CLuaTaskDialogWnd:OnDestroy()
    if self.cancelAutoPlayTick ~= nil then
        UnRegisterTick(self.cancelAutoPlayTick)
        self.cancelAutoPlayTick = nil
    end
    if CConversationMgr.Inst.BaseInfo then
        Gac2Gas.EndConversation()
    end
    if self.useBigShow then
        self:DisableBigNpcMode()
    end

    self:StopSound()
    LuaDialogCameraMgr.EndDialogCamera()
end

function CLuaTaskDialogWnd:LoadPortrait(portraitName)
    self.headPortrait:LoadPortrait(portraitName, false)
end

function CLuaTaskDialogWnd:StopSound()
    if self.m_CancelSoundPlayAction then
        invoke(self.m_CancelSoundPlayAction)
        self.m_CancelSoundPlayAction = nil
    end
    SoundManager.Inst:StopDialogSound()
end

function CLuaTaskDialogWnd:ProcessToricCamera(paragraph)
    local toric = paragraph.toricCameraParams
    if toric then
        LuaDialogCameraMgr.ProcessToricCamera(toric.npcId1,toric.npcId2,toric.alpha,toric.theta,toric.phi,toric.duration,toric.noSway)
    end
end

function CLuaTaskDialogWnd:ProcessExpression(paragraph)
    if next(paragraph.expression) ~= nil then
        for k, v in pairs(paragraph.expression) do
            self.m_Npc2Expression[k] = v
        end
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if TypeIs(obj, typeof(CClientNpc)) and paragraph.expression[obj.TemplateId] then
                obj:ShowExpressionActionState(paragraph.expression[obj.TemplateId])
            elseif TypeIs(obj, typeof(CClientMainPlayer)) and paragraph.expression[0] then
                obj:ShowExpressionActionState(paragraph.expression[0])
            end
        end))
    end
end
