require("common/common_include")

local UILabel = import "UILabel"
local GameObject  = import "UnityEngine.GameObject"

LuaYuanxiaoChoosePuzzleWnd = class()

RegistChildComponent(LuaYuanxiaoChoosePuzzleWnd, "closeBtn", GameObject)
RegistChildComponent(LuaYuanxiaoChoosePuzzleWnd, "node1", GameObject)
RegistChildComponent(LuaYuanxiaoChoosePuzzleWnd, "node2", GameObject)
RegistChildComponent(LuaYuanxiaoChoosePuzzleWnd, "node3", GameObject)
RegistChildComponent(LuaYuanxiaoChoosePuzzleWnd, "node4", GameObject)
RegistChildComponent(LuaYuanxiaoChoosePuzzleWnd, "submitBtn", GameObject)
RegistChildComponent(LuaYuanxiaoChoosePuzzleWnd, "answerLabel", UILabel)

RegistClassMember(LuaYuanxiaoChoosePuzzleWnd, "chooseIndex")
RegistClassMember(LuaYuanxiaoChoosePuzzleWnd, "chooseNode")
--RegistClassMember(LuaYuanxiaoChoosePuzzleWnd, "m_Tick")
function LuaYuanxiaoChoosePuzzleWnd:SubmitInfo()
    if self.chooseIndex and LuaYuanxiaoMgr.npcEngineId then
      local riddleId = LuaYuanxiaoMgr.chooseTable[self.chooseIndex]
      if riddleId then
	      Gac2Gas.ConfirmCommitRiddle(LuaYuanxiaoMgr.npcEngineId, riddleId)
        CUIManager.CloseUI("YuanxiaoChoosePuzzleWnd")
      end
     elseif not self.chooseIndex then
       g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个灯谜!"))
    end
end

function LuaYuanxiaoChoosePuzzleWnd:ChooseNode(node,index)
  if not LuaYuanxiaoMgr.chooseTable then
    return
  end

  local idIndex = LuaYuanxiaoMgr.chooseTable[index]
  if not idIndex then
    return
  end

  local data = YuanXiao_QuestionPool2.GetData(idIndex)
  if data then
    local lightNode = node.transform:Find("light").gameObject
    if self.chooseNode then
      self.chooseNode:SetActive(false)
    end
    self.chooseNode = lightNode
    lightNode:SetActive(true)
    self.chooseIndex = index
    self.answerLabel.text = data.Question
  end
end

function LuaYuanxiaoChoosePuzzleWnd:InitNode(node,index)
  if not LuaYuanxiaoMgr.chooseTable then
    return
  end
  local idIndex = LuaYuanxiaoMgr.chooseTable[index]
  if not idIndex then
    return
  end

  local data = YuanXiao_QuestionPool2.GetData(idIndex)
  if data then
    local nameLabel = node.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = data.Answer
    local lightNode = node.transform:Find("light").gameObject
    lightNode:SetActive(false)
  end
end

function LuaYuanxiaoChoosePuzzleWnd:Init()
  UIEventListener.Get(self.submitBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:SubmitInfo()
	end)
  UIEventListener.Get(self.node1).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:ChooseNode(self.node1,1)
	end)
  UIEventListener.Get(self.node2).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:ChooseNode(self.node2,2)
	end)
  UIEventListener.Get(self.node3).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:ChooseNode(self.node3,3)
	end)
  UIEventListener.Get(self.node4).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:ChooseNode(self.node4,4)
	end)

  self:InitNode(self.node1,1)
  self:InitNode(self.node2,2)
  self:InitNode(self.node3,3)
  self:InitNode(self.node4,4)

  self.answerLabel.text = ""
end
return LuaYuanxiaoChoosePuzzleWnd
