local CButton 							= import "L10.UI.CButton"
local UIScrollView 						= import "UIScrollView"
local UITable							= import "UITable"
local CTipParagraphItem		        	= import "L10.UI.CTipParagraphItem"
local CMessageTipMgr 					= import "L10.UI.CMessageTipMgr"
local CommonDefs 						= import "L10.Game.CommonDefs"
local DelegateFactory  					= import "DelegateFactory"
local GameObject 						= import "UnityEngine.GameObject"

LuaShengChenGangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengChenGangWnd, "BtnJoyin", "BtnJoyin", CButton)
RegistChildComponent(LuaShengChenGangWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaShengChenGangWnd, "Table", "Table", UITable)
RegistChildComponent(LuaShengChenGangWnd, "ScrollView", "ScrollView", UIScrollView)

--@endregion RegistChildComponent end

function LuaShengChenGangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BtnJoyin.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnJoyinClick()
	end)


    --@endregion EventBind end
end

function LuaShengChenGangWnd:Init()
	self:ParseRuleText()							-- 初始化显示规则文本
end

--@region UIEvent
--玩家点击参加
function LuaShengChenGangWnd:OnBtnJoyinClick()
	Gac2Gas.RequestEnterShengChenGangPlay()					-- 向服务器发送生辰纲玩法开始信息
	CUIManager.CloseUI(CLuaUIResources.ShengChenGangWnd)		-- 关闭界面
end


--@endregion UIEvent

function LuaShengChenGangWnd:ParseRuleText()
	Extensions.RemoveAllChildren(self.Table.transform)		-- 清空组件下的所有子物体
	local msg =	g_MessageMgr:FormatMessage("ZNQ_HUOYUN_RULE",{});	-- 根据消息key获得信息
    if System.String.IsNullOrEmpty(msg) then				-- 判断是否为空
        return
    end

	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)	-- 标题和段

	if info == nil then
		return
	end

	do 
		local i = 0
		while i < info.paragraphs.Count do							-- 根据信息创建并显示段落
			local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate) -- 添加段落Go
			paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
			tip:Init(info.paragraphs[i], 4294967295)				-- 初始化段落，color = 4294967295（0xFFFFFFFF）
			i = i + 1
		end
	end

	self.ScrollView:ResetPosition()									-- 重置滚动框位置
	self.Table:Reposition()
end
return LuaShengChenGangWnd
