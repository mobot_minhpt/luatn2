local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"
local CScene=import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Profession = import "L10.Game.Profession"
LuaGuoQingPvPPlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQingPvPPlayingWnd, "Team1", "Team1", GameObject)
RegistChildComponent(LuaGuoQingPvPPlayingWnd, "Team2", "Team2", GameObject)
RegistChildComponent(LuaGuoQingPvPPlayingWnd, "Info", "Info", GameObject)
RegistChildComponent(LuaGuoQingPvPPlayingWnd, "CountDown", "CountDown", GameObject)
RegistChildComponent(LuaGuoQingPvPPlayingWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaGuoQingPvPPlayingWnd, "Content", "Content", GameObject)

--@endregion RegistChildComponent end

function LuaGuoQingPvPPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.Content:SetActive(true)
    --for i = 1,3 do
    --    local P = self.Info.transform:Find(tostring(i)).gameObject
    --    P:SetActive(false)
    --end
end

function LuaGuoQingPvPPlayingWnd:Init()
    self:ShowInfo()
end


function LuaGuoQingPvPPlayingWnd:OnEnable()
    
    --g_ScriptEvent:AddListener("SyncJinLuHunYuanZhanKillInfo", self, "OnSyncJinLuHunYuanZhanKillInfo")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
end
function LuaGuoQingPvPPlayingWnd:OnDisable()
    --g_ScriptEvent:RemoveListener("SyncJinLuHunYuanZhanKillInfo", self, "OnSyncJinLuHunYuanZhanKillInfo")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
end
--@region UIEvent
function LuaGuoQingPvPPlayingWnd:ShowInfo()
    local attackdata = LuaGuoQingPvPMgr.attackdata
    local defenddata = LuaGuoQingPvPMgr.defenddata
    local defend_kill_number = 0
    local attack_kill_number = 0
    for i=1, #defenddata, 4 do
        defend_kill_number = defend_kill_number + defenddata[i+3]
    end
    for i=1, #attackdata, 4 do
        attack_kill_number = attack_kill_number + attackdata[i+3]
    end
    self.Team1.transform:Find("Count"):GetComponent(typeof(UILabel)).text = tostring(defend_kill_number)
    self.Team2.transform:Find("Count"):GetComponent(typeof(UILabel)).text = tostring(attack_kill_number)
    local myplayerid = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local mydata = defenddata
    self.Team1.transform:Find("OwnerIcon").gameObject:SetActive(true)
    self.Team2.transform:Find("OwnerIcon").gameObject:SetActive(false)
    for i=1, #attackdata, 4 do
        if myplayerid == attackdata[i] then
            mydata = attackdata
            self.Team1.transform:Find("OwnerIcon").gameObject:SetActive(false)
            self.Team2.transform:Find("OwnerIcon").gameObject:SetActive(true)
            break
        end
    end
    local Data = {}
    for i=1, #mydata, 4 do
        local now = {}
        now.playerid = mydata[i]
        now.name = mydata[i+1]
        now.class = mydata[i+2]
        now.kill = mydata[i+3]
        table.insert(Data,now)
    end
    
    table.sort(Data, function(a, b)
        return a.kill > b.kill
    end)
    for i = 1,3 do
        local P = self.Info.transform:Find(tostring(i)).gameObject
        P:SetActive(false)
    end
    for i = 1,#Data do
        if i > 3 then
            break
        end
        local info = Data[i]
        local color = ""
		if info.playerid == myplayerid then
			color = "[00FF60]"
		end
        
        local playerItem = self.Info.transform:Find(tostring(i))
        playerItem.gameObject:SetActive(true)
		local clsSp = playerItem:Find("ClsSprite"):GetComponent(typeof(UISprite))
		local name = playerItem:Find("Name"):GetComponent(typeof(UILabel))
		local playerCount = playerItem:Find("Count"):GetComponent(typeof(UILabel))
        local playerLabel = FindChild(playerItem.transform,"Label"):GetComponent(typeof(UILabel))
        name.text = color.. info.name
		playerCount.text = color..info.kill
        playerLabel.text = color..LocalString.GetString("击杀")
		clsSp.spriteName = Profession.GetIconByNumber(info.class)
    end
end
function LuaGuoQingPvPPlayingWnd:OnRemainTimeUpdate()
    local mainScene = CScene.MainScene
    if mainScene then
        self.RemainTimeLabel.text = self:GetRemainTimeText(mainScene.ShowTime)
    end
end

function LuaGuoQingPvPPlayingWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),  
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60), 
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
--@endregion UIEvent

