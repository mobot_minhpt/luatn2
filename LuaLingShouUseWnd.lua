local EnumItemPlace = import "L10.Game.EnumItemPlace"
local LingShou_Setting=import "L10.Game.LingShou_Setting"
local CUITexture=import "L10.UI.CUITexture"
local CUILongPressButton=import "L10.UI.CUILongPressButton"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltip = import "L10.UI.CTooltip"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"


CLuaLingShouUseWnd = class()
RegistClassMember(CLuaLingShouUseWnd,"anchorNode")
RegistClassMember(CLuaLingShouUseWnd,"itemPrefab")
RegistClassMember(CLuaLingShouUseWnd,"grid")
RegistClassMember(CLuaLingShouUseWnd,"bgSprite")

CLuaLingShouUseWnd.useType = 0 --0 exp 1 shouming

function CLuaLingShouUseWnd:Awake()
    self.anchorNode = self.transform:Find("Node")
    self.itemPrefab = self.transform:Find("Node/Item").gameObject
    self.itemPrefab:SetActive(false)
    self.grid = self.transform:Find("Node/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.bgSprite = self.transform:Find("Node"):GetComponent(typeof(UISprite))
end

-- Auto Generated!!
function CLuaLingShouUseWnd:Init( )
    if CLuaLingShouUseWnd.useType == 0 then
        self.anchorNode.localPosition = Vector3(654, 387,0)
        local items = LingShou_Setting.GetData().FeedItemIds
        for i=1,items.Length do
            local obj = NGUITools.AddChild(self.grid.gameObject, self.itemPrefab)
            obj:SetActive(true)
            self:InitItem(obj.transform,items[i-1])
        end
        --检查宝宝放生经验丹
        local bindItemCountInBag, notBindItemCountInBag
        bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(21050024)
        local count = bindItemCountInBag + notBindItemCountInBag
        if count > 0 then
            local obj = NGUITools.AddChild(self.grid.gameObject, self.itemPrefab)
            obj:SetActive(true)
            self:InitItem(obj.transform,21050024)

            self.bgSprite.height = 715
            self.anchorNode.localPosition = Vector3( 654, 540,0)
        end
    elseif CLuaLingShouUseWnd.useType == 1 then
        self.anchorNode.localPosition = Vector3(654, 285,0)

        local items = LingShou_Setting.GetData().ShouMingItemIds
        for i=1,items.Length do
            local obj = NGUITools.AddChild(self.grid.gameObject,self.itemPrefab)
            obj:SetActive(true)
            self:InitItem(obj.transform,items[i-1])
        end
    end
    self.grid:Reposition()
end


function CLuaLingShouUseWnd:InitItem(transform,templateId)
    local icon = transform:Find("Sprite/Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local expLabel = transform:Find("ExpLabel"):GetComponent(typeof(UILabel))
    local countUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local getNode = transform:Find("Sprite/GetNode")
    local longPressButton = transform:GetComponent(typeof(CUILongPressButton))

    getNode.gameObject:SetActive(false)

    local function callback(go)
        if countUpdate.count == 0 then
            if CLuaLingShouUseWnd.useType == 0 then
                CItemAccessListMgr.Inst.absoluteLocalPosition = Vector3(32, 277,0)
            elseif CLuaLingShouUseWnd.useType == 1 then
                CItemAccessListMgr.Inst.absoluteLocalPosition = Vector3(33, 216,0)
            end
            CItemAccessListMgr.Inst:ShowItemAccessInfo(countUpdate.templateId, false, nil, CTooltip.AlignType.Absolute)
        elseif countUpdate.count > 0 then
            local lingshouId = CLingShouMgr.Inst.selectedLingShou
            local details = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
			if details then
				local  marryInfo = details.data.Props.MarryInfo
				local isDongFang = marryInfo.IsInDongfang>0
				local isLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp
				if isDongFang then
					g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("当前灵兽生宝宝中，无法进行灵兽功能相应的操作"))
					return
				elseif isLeave then
					g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("当前灵兽离家出走中，无法进行灵兽功能相应的操作"))
					return
				end
            end
            
            if templateId ==21050024 then
                CUIManager.ShowUI(CLuaUIResources.LingShouConsumeBabyExpItemWnd)
            else
                local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, templateId)
                if default then
                    if itemId and itemId~="" and lingshouId and lingshouId~="" then
                        Gac2Gas.RequestFeedLingShou(lingshouId, EnumItemPlace_lua.Bag, pos, itemId)
                    end
                end
            end
        end
    end

    longPressButton.callback = DelegateFactory.Action_GameObject(callback)
    longPressButton.delta = 0.33 / 2

    -- self.templateId = templateId
    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    if template ~= nil then
        icon:LoadMaterial(template.Icon)
        nameLabel.text = template.Name
        expLabel.text = CUICommonDef.TranslateToNGUIText(CItem.GetItemDescription(templateId,true))
    else
        -- Debug.Log("not find " .. templateId)
        nameLabel.text = ""
        expLabel.text = ""
    end

    countUpdate.templateId = templateId
    countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        if val == 0 then
            countUpdate.format = "[ff0000]{0}[-]"
            getNode.gameObject:SetActive(true)

            longPressButton.enableLongPressAction = false
        else
            countUpdate.format = "{0}"
            getNode.gameObject:SetActive(false)

            longPressButton.enableLongPressAction = true
        end
    end)
    countUpdate:UpdateCount()
    
end
