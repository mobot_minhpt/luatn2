local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CFashionPreviewTextureLoader = import "L10.UI.CFashionPreviewTextureLoader"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local EnumPreviewType = import "L10.UI.EnumPreviewType"
local CFashionMgr = import "L10.Game.CFashionMgr"
local EnumShopMallFashionPreviewType = import "L10.UI.EnumShopMallFashionPreviewType"
local CPuppetClothesMgr = import "L10.UI.CPuppetClothesMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local EnumYingLingState = import "L10.Game.EnumYingLingState"
local CPuppetFashionInfo = import "L10.Game.CPuppetFashionInfo"
local Time = import "UnityEngine.Time"
local CVehicleList = import "L10.UI.CVehicleList"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CClientFurniture = import "L10.Game.CClientFurniture"

CFashionPreviewTextureLoader.m_LoadModel_CS2LuaHook = function(this)
    if not CClientMainPlayer.Inst then return end
    local fashion = Fashion_Fashion.GetData(CFashionPreviewMgr.FashionID)
    local scale = 1
    if (CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewWeaponTaben) and (CFashionMgr.Inst:IsWeaponKuiLei(CFashionPreviewMgr.FashionID)) then
        scale = this.kuileiPreviewScale
    end
    if (((CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewFashion) or (CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewFashionItem))
            and fashion and (fashion.Position == 0)) then
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(
                this.identifier, this, this.rotation,
                0, CClientMainPlayer.Inst.CameraHeightForPreviewHeadFashion, 1.7,
                this.multiColor, true, 1, false, false)
    elseif (CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewWeaponTaben) then
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(
                this.identifier, this, 180,
                0.05, -1, 4.66,
                this.multiColor, not CFashionMgr.Inst:IsWeaponKuiLei(CFashionPreviewMgr.FashionID), scale, false, false)
    elseif ((CFashionPreviewMgr.curShopMallPreviewType == EnumShopMallFashionPreviewType.PreviewAllFashion) or
            (CFashionPreviewMgr.curShopMallPreviewType == EnumShopMallFashionPreviewType.TakeOffAlllFashion)) then
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(
                this.identifier, this, this.rotation,
                0.05, -1, 4.66,
                this.multiColor, true, 1, true, LuaShopMallFashionPreviewMgr.enableMultiLight)
    elseif (CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewUmbrella) then
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(
                this.identifier, this, 180,
                0.05, -1.31, 5.28,
                this.multiColor, true, 1, false, false)
    elseif CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewZsw then
        local x = LuaFashionPreviewTextureLoaderMgr.PreviewZswRootPos[1] or 0.05 
        local y = LuaFashionPreviewTextureLoaderMgr.PreviewZswRootPos[2] or -1.31
        local z = LuaFashionPreviewTextureLoaderMgr.PreviewZswRootPos[3] or 5.28 
        LuaFashionPreviewTextureLoaderMgr.PreviewZswRootPos = {}
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(
                this.identifier, this, 180,
                x, y, z,
                this.multiColor, true, 1, false, false)
    else
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(
                this.identifier, this, this.rotation,
                0.05, -1, 4.66,
                this.multiColor, true, 1, false, false)
    end

end

CFashionPreviewTextureLoader.m_Init_CS2LuaHook = function(this, multiColor)
    if this.needShowAppearance == nil then
        if CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewPuppetWeaponTaben then
            if this.suitPreivew then
                this.suitPreivew.gameObject:SetActive(false)
            end

            this.needShowAppearance = CPropertyAppearance()
            local idx = CPuppetClothesMgr.selectIdx
            if idx == 0 then return end
            local playerPuppetInfo = CClientHouseMgr.Inst.mCurFurnitureProp.PuppetInfo[idx]
            if playerPuppetInfo then
                local cppuppet = HousePuppet_CPPuppet.GetData(playerPuppetInfo.Id)
                if cppuppet then
                    local cpData = Initialization_Init.GetData(cppuppet.Class * 100 + cppuppet.Gender)
                    if cpData then
                        this.needShowAppearance.BodyFashionId = playerPuppetInfo.FashionInfo.BodyFashionId / 10000
                        this.needShowAppearance.BodyId = cpData.Body
                        this.needShowAppearance.Equipment = cpData.InitEquip
                        this.needShowAppearance.HeadFashionId = (playerPuppetInfo.FashionInfo.HeadFashionId / 10000)
                        this.needShowAppearance.HeadId = cpData.CPHeadID
                        this.needShowAppearance.HideBodyFashionEffect = playerPuppetInfo.FashionInfo.HideBodyFashion
                        this.needShowAppearance.HideHeadFashionEffect = playerPuppetInfo.FashionInfo.HideHeadFashion
                        this.needShowAppearance.HideHelmetEffect = 0
                        this.needShowAppearance.HideWeaponFashionEffect = playerPuppetInfo.FashionInfo.HideWeaponFashion
                        this.needShowAppearance.WeaponFashionId = (playerPuppetInfo.FashionInfo.WeaponFashionId / 10000)
                        this.needShowClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), cppuppet.Class)
                        this.needShowGender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), cppuppet.Gender)
                        this.needShowYingLingState = EnumYingLingState.eDefault
                        local __try_get_result, fashion = CommonDefs.DictTryGet(playerPuppetInfo.FashionInfo.Fashions, typeof(UInt64), playerPuppetInfo.FashionInfo.BodyFashionId, typeof(CPuppetFashionInfo))
                        if __try_get_result then
                            this.puppet_hue = fashion.SeXiang
                            this.puppet_saturate = fashion.BaoHeDu
                            this.puppet_bright = fashion.MingDu
                        end
                    end
                end
            end
        else
            this.needShowAppearance = CClientMainPlayer.Inst.AppearanceProp:Clone()
            this.needShowClass = CClientMainPlayer.Inst.Class
            this.needShowGender = CClientMainPlayer.Inst.Gender
            this.needShowYingLingState = CommonDefs.ConvertIntToEnum(typeof(EnumYingLingState), this.needShowAppearance.YingLingState)
        end
    end
    this.needShowAppearance.HideTouSha = 0

    if CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewFashionItem then
        if this.suitPreivew then
            this.suitPreivew.gameObject:SetActive(false)
            this.suitPreivew:SetSelected(CFashionPreviewMgr.SuitID ~= 0)
        end
    end

    this.multiColor = multiColor
    this.deltaTime = Time.deltaTime / 2
    this.identifier = cs_string.Format("__{0}__", this:GetInstanceID())
    this.rotation = 180

    if CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewVehicle then
        if this.suitPreivew then
            this.suitPreivew.gameObject:SetActive(false)
        end
        this.rotation = CVehicleList.sDefaultRotation
        this.mDefaultRotation = CVehicleList.sDefaultRotation
    end

    if CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewBraceletTaben then
        LuaFashionPreviewTextureLoaderMgr:InitPreviewBraceletTaben(this)
    end

    this:InitSuitPreviewSwitchButton()
    this:LoadModel()
end

CFashionPreviewTextureLoader.m_Load_CS2LuaHook = function (this, renderObj)
    local mainPlayer = CClientMainPlayer.Inst
    local enablecloth = QnModelPreviewer.EnableCloth
    local appearanceData = this:GetDefaultAppearance()
    if mainPlayer ~= nil and EnumShopMallFashionPreviewType.TakeOffAlllFashion == CFashionPreviewMgr.curShopMallPreviewType then
        if this.suitPreivew then
            this.suitPreivew.gameObject:SetActive(false)
        end
        appearanceData.HideHeadFashionEffect = CFashionPreviewMgr.shopMallPreview_HideHeadFashionEffect
        appearanceData.HideBodyFashionEffect = CFashionPreviewMgr.shopMallPreview_HideBodyFashionEffect
        appearanceData.HideHelmetEffect = 0
        this.m_FashionPreviewAppearanceData = appearanceData
        this.m_RO = renderObj
        CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, true, 0, false, 0, false, nil, false, enablecloth)
    elseif mainPlayer ~= nil and EnumShopMallFashionPreviewType.PreviewAllFashion == CFashionPreviewMgr.curShopMallPreviewType then
        local expression = Expression_Define.GetData(CFashionPreviewMgr.UmbrellaFashionID)
        local zuoqi = ZuoQi_ZuoQi.GetData(CFashionPreviewMgr.ZuoQiFashionID)
        local headFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.HeadFashionID)
        local bodyFashion = Fashion_Fashion.GetData(CFashionPreviewMgr.BodyFashionID)

        appearanceData.HeadFashionId = headFashion ~= nil and CFashionPreviewMgr.HeadFashionID or this.needShowAppearance.HeadFashionId
        appearanceData.BodyFashionId = bodyFashion ~= nil and CFashionPreviewMgr.BodyFashionID or this.needShowAppearance.BodyFashionId
        appearanceData.HideTouSha = this.needShowAppearance.HideTouSha
        local monsterId = 0
        if IsTransformableFashionOpen() then
            if headFashion ~= nil and bodyFashion ~= nil and bodyFashion.Appearance == headFashion.Appearance and LuaShopMallFashionPreviewMgr.isTransformed then
                if CClientMainPlayer.Inst.Gender == EnumGender.Male then
                    if  bodyFashion.TransformMonsterMale ~= 0 and headFashion.TransformMonsterMale ~= 0 and bodyFashion.TransformMonsterMale == headFashion.TransformMonsterMale then
                        monsterId = bodyFashion.TransformMonsterMale
                    end
                else
                    if bodyFashion.TransformMonsterFemale ~= 0 and headFashion.TransformMonsterFemale ~= 0 and bodyFashion.TransformMonsterFemale == headFashion.TransformMonsterFemale then
                        monsterId = bodyFashion.TransformMonsterFemale
                    end
                end
                appearanceData.ResourceId = monsterId
            else 
                if CFashionPreviewMgr.HeadFashionID == 0 and CFashionPreviewMgr.BodyFashionID == 0 and CFashionPreviewMgr.UmbrellaFashionID == 0 then
                    monsterId = LuaAppearancePreviewMgr.GetCurrentTransformMonsterId()
                    appearanceData.ResourceId = monsterId
                else 
                    appearanceData.ResourceId = this.needShowAppearance.ResourceId
                end
            end
        end
        appearanceData.HideHeadFashionEffect = CFashionPreviewMgr.shopMallPreview_HideHeadFashionEffect
        appearanceData.HideBodyFashionEffect = CFashionPreviewMgr.shopMallPreview_HideBodyFashionEffect

        local bpfid = LuaShopMallFashionPreviewMgr.backpendantFashionId --预览的背饰时装
        if bpfid > 0 then
            local beishiFashion = Fashion_Fashion.GetData(bpfid)
            if beishiFashion then --预览的背饰存在
                appearanceData.BackPendantFashionId = bpfid
                appearanceData.HideBackPendant = 0 --强制显示背饰
                appearanceData.HideBackPendantFashionEffect = 0
            end
        end

        appearanceData.HideHelmetEffect = 0
        this.m_FashionPreviewAppearanceData = appearanceData
        this.m_RO = renderObj
        CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, zuoqi ~= nil and zuoqi.ID or 0, true, monsterId, false, expression ~= nil and expression.ID or 0, false, nil, false, enablecloth)

    elseif mainPlayer ~= nil and (CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewFashion or CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewWeaponTaben or CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewPuppetWeaponTaben or CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewFashionItem) then
        local scale = 1
        if CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewWeaponTaben and CFashionMgr.Inst:IsWeaponKuiLei(CFashionPreviewMgr.FashionID) then
            scale = this.kuileiPreviewScale
        end
        if this.suitPreivew then
            this.suitPreivew.gameObject:SetActive(CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewFashion)
        end
        local hue = 0 local saturate = 0 local bright = 0
        hue = this.needShowAppearance.BodyFashionCustomColorInfo[1]
        saturate = this.needShowAppearance.BodyFashionCustomColorInfo[2]
        bright = this.needShowAppearance.BodyFashionCustomColorInfo[3]
        if hue ~= 0 or saturate ~= 0 or bright ~= 0 then
        elseif CFashionPreviewMgr.curPreviewType == EnumPreviewType.PreviewPuppetWeaponTaben then
            hue = this.puppet_hue
            saturate = this.puppet_saturate
            bright = this.puppet_bright
        end

        local fashion = Fashion_Fashion.GetData(CFashionPreviewMgr.FashionID)
        local equip = this.needShowAppearance.Equipment
        local hairId = this.needShowAppearance.HeadId
        local headFashion = false
        local bodyFashion = false
        local weaponFashion = false
        local beishiFashion = false
        if fashion ~= nil then
            headFashion = fashion.Position == 0
            bodyFashion = fashion.Position == 1
            beishiFashion = fashion.Position == 3
        else
            local template = EquipmentTemplate_Equip.GetData(CFashionPreviewMgr.FashionID)
            if template ~= nil then
                headFashion = template.Type == EnumBodyPosition_lua.Casque
                bodyFashion = template.Type == EnumBodyPosition_lua.Armour
                weaponFashion = template.Type == EnumBodyPosition_lua.Weapon
                beishiFashion = template.Type == EnumBodyPosition_lua.BackPendant
            end
        end
        if headFashion then
            local hideBody = 0
            local isselect = this.suitPreivew and this.suitPreivew:isSeleted()
            if not isselect then
                hideBody = this.needShowAppearance.HideBodyFashionEffect
            end
            appearanceData.BodyFashionId = isselect and CFashionPreviewMgr.SuitID or this.needShowAppearance.BodyFashionId
            appearanceData.HideBodyFashionEffect = hideBody
            appearanceData.HeadFashionId = CFashionPreviewMgr.FashionID
            appearanceData.BodyFashionCustomColorInfo[1] = math.floor(hue)
            appearanceData.BodyFashionCustomColorInfo[2] = math.floor(saturate)
            appearanceData.BodyFashionCustomColorInfo[3] = math.floor(bright)
            CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, false, 0, false, 0, isselect, nil, false, enablecloth)
        elseif beishiFashion then
            appearanceData.HideBackPendant = 0 --强制显示背饰
            appearanceData.HideBackPendantFashionEffect = 0 --强制显示背饰时装
            appearanceData.BackPendantFashionId = CFashionPreviewMgr.FashionID --强制更改为当前预览的拓本ID
            appearanceData.BackPendantFashionAppearance =  LuaFashionMgr.GetBackPendantAppearence(CFashionPreviewMgr.FashionID, CFashionPreviewMgr.FashionLevel)
            CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, false, 0, false, 0, false, nil, false, enablecloth)
        elseif bodyFashion then
            local hideHelmet = 0
            local hideHead = 0
            local isselect = this.suitPreivew and this.suitPreivew:isSeleted()
            if not isselect then
                hideHelmet = this.needShowAppearance.HideHelmetEffect
                hideHead = this.needShowAppearance.HideHeadFashionEffect
            end
            appearanceData.HeadFashionId = isselect and CFashionPreviewMgr.SuitID or this.needShowAppearance.HeadFashionId
            appearanceData.BodyFashionId = CFashionPreviewMgr.FashionID
            appearanceData.HideHeadFashionEffect = hideHead
            appearanceData.HideHelmetEffect = hideHelmet
            appearanceData.BodyFashionCustomColorInfo[1] = math.floor(hue)
            appearanceData.BodyFashionCustomColorInfo[2] = math.floor(saturate)
            appearanceData.BodyFashionCustomColorInfo[3] = math.floor(bright)
            CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, false, 0, false, 0, not this.multiColor, nil, false, enablecloth)
        elseif weaponFashion then
            LuaFashionPreviewTextureLoaderMgr:PreviewWeapon(this,renderObj,hue,saturate,bright,scale,enablecloth)
        end
    elseif mainPlayer ~= nil and EnumPreviewType.PreviewVehicle == CFashionPreviewMgr.curPreviewType then
        if this.suitPreivew then
            this.suitPreivew.gameObject:SetActive(false)
        end
        local data = ZuoQi_ZuoQi.GetData(CZuoQiMgr.Inst.PreviewVehicleId)
        if data ~= nil then
            CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, CZuoQiMgr.Inst.PreviewVehicleId, true, 0, false, 0, false, nil, false, enablecloth)
            renderObj:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (ro)
                if ro.VehicleRO then
                    local PreviewScale = 1
                    local zuoqi = ZuoQi_ZuoQi.GetData(CZuoQiMgr.Inst.PreviewVehicleId)
                    if zuoqi then
                        PreviewScale = zuoqi.PreviewScale
                    end
                    if zuoqi then
                        CUIManager.SetModelScale(this.identifier, Vector3(PreviewScale,PreviewScale,PreviewScale))
                    end
                end
            end))
        end
    elseif EnumPreviewType.PreviewUmbrella == CFashionPreviewMgr.curPreviewType then
        LuaFashionPreviewTextureLoaderMgr:PreviewUmbrella(this,renderObj)
    elseif EnumPreviewType.PreviewBraceletTaben == CFashionPreviewMgr.curPreviewType then
        LuaFashionPreviewTextureLoaderMgr:PreviewBraceletTaben(this, renderObj)
    elseif EnumPreviewType.PreviewZsw == CFashionPreviewMgr.curPreviewType then
        LuaFashionPreviewTextureLoaderMgr:PreviewZsw(this, renderObj)
    end
    
    --时装带的buff特效特殊处理，这里appearanceData如果是进入PreviewUmbrella/PreviewBraceletTaben/PreviewZsw有点浪费，方法内部也会clone appearance，暂时先这样
    LuaFashionPreviewTextureLoaderMgr:AddFashionBuffFx(this, renderObj, appearanceData)
end

LuaFashionPreviewTextureLoaderMgr = {}

LuaFashionPreviewTextureLoaderMgr.ModelNum = 1
LuaFashionPreviewTextureLoaderMgr.CurModelIdx = 1

LuaFashionPreviewTextureLoaderMgr.PreviewZswID = 0
LuaFashionPreviewTextureLoaderMgr.PreviewZswRootPos = {}

function LuaFashionPreviewTextureLoaderMgr:PreviewWeapon(this,renderObj,hue,saturate,bright,scale,enablecloth)
    local appearanceData = this:GetDefaultAppearance()
    this:CloneEquips(appearanceData)
    appearanceData.HideWeaponFashionEffect = 0
    appearanceData.WeaponFashionId = CFashionPreviewMgr.FashionID
    appearanceData.Equipment[1] = CFashionPreviewMgr.FashionID
    if CommonDefs.IsZhanKuang(appearanceData.Class) then
        appearanceData.Equipment[2] = CFashionPreviewMgr.FashionID
    end

    appearanceData.BodyFashionCustomColorInfo[1] = math.floor(hue)
    appearanceData.BodyFashionCustomColorInfo[2] = math.floor(saturate)
    appearanceData.BodyFashionCustomColorInfo[3] = math.floor(bright)
    appearanceData.WeaponFashionId = CFashionPreviewMgr.FashionID
    CClientMainPlayer.LoadResource(renderObj, appearanceData, true, scale, 0, false, 0, false, 0, false, nil, false, enablecloth)
end

function LuaFashionPreviewTextureLoaderMgr:PreviewUmbrella(this,renderObj)
    if this.suitPreivew then
        this.suitPreivew.gameObject:SetActive(false)
    end
    local expression = Expression_Define.GetData(CFashionPreviewMgr.UmbrellaFashionID)

    local appearanceData = this:GetDefaultAppearance()
    this.m_FashionPreviewAppearanceData = appearanceData
    this.m_RO = renderObj
    local enablecloth = QnModelPreviewer.EnableCloth
    CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, true, 0, false, expression ~= nil and expression.ID or 0, false, nil, false, enablecloth)
end

function LuaFashionPreviewTextureLoaderMgr:InitPreviewBraceletTaben(this)
    if this.suitPreivew then
        this.suitPreivew.gameObject:SetActive(false)
    end
    this.resetButton:SetActive(false)
    this.leftRotate:SetActive(false)
    this.rightRotate:SetActive(false)
    this.transform:Find("Model/Label").gameObject:SetActive(false)
    this.transform:Find("Model/Title/Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("手镯拓本预览")
    
    local changeModel = this.transform:Find("Model/ChangeModel")
    changeModel.gameObject:SetActive(true)
    local modelLabel = changeModel:Find("RideLabel"):GetComponent(typeof(UILabel))

    local uitable = changeModel:Find("Table"):GetComponent(typeof(UITable))
    local template = changeModel:Find("SpriteTemplate").gameObject
    template:SetActive(false)
    Extensions.RemoveAllChildren(uitable.transform)
    for i = 1, self.ModelNum do
        local dot = NGUITools.AddChild(uitable.gameObject, template)
        dot:SetActive(true)
        dot:GetComponent(typeof(UISprite)).enabled = self.CurModelIdx == i
    end
    uitable:Reposition()

    local taben = ZhouNianQing2023_PSZJSetting.GetData().TabenItemId
    local name = {}
    for i = 0, taben.Length - 1 do
        table.insert(name, Item_Item.GetData(taben[i]).Name)
    end
    modelLabel.text = name[self.CurModelIdx]

    local refresh = function()
        this:LoadModel()
        modelLabel.text = name[self.CurModelIdx]
        for i = 0, uitable.transform.childCount - 1 do
            uitable.transform:GetChild(i):GetComponent(typeof(UISprite)).enabled = self.CurModelIdx == i + 1
        end
    end
    
    UIEventListener.Get(changeModel:Find("NextPageBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self.CurModelIdx = self.CurModelIdx + 1
        if self.CurModelIdx > self.ModelNum then
            self.CurModelIdx = 1
        end
        refresh()
    end)

    UIEventListener.Get(changeModel:Find("LastPageBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self.CurModelIdx = self.CurModelIdx - 1
        if self.CurModelIdx < 1 then
            self.CurModelIdx = self.ModelNum
        end
        refresh()
    end)
end

function LuaFashionPreviewTextureLoaderMgr:PreviewBraceletTaben(this, renderObj)
    local appearanceData = this:GetDefaultAppearance()
    this.m_FashionPreviewAppearanceData = appearanceData
    this.m_RO = renderObj
    local enablecloth = QnModelPreviewer.EnableCloth

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        CClientMainPlayer.LoadResource(renderObj, mainPlayer.AppearanceProp, true, 1, 0, true, mainPlayer.AppearanceProp.ResourceId, false, 0, false, nil, false, enablecloth)
        -- 移除已经在玩家装备上生效的手镯拓本特效
        renderObj:RemoveFX("eAnniv2023PSZJ_LeftHand") 
        renderObj:RemoveFX("eAnniv2023PSZJ_RightHand")
        local scale = 1
        local data = Transform_Transform.GetData(mainPlayer.AppearanceProp.ResourceId)
        if data then
            scale = data.PreviewScale
        end
        renderObj.Scale = scale

        --RegisterTickOnce(function()
            if not renderObj then 
                return 
            end
            local data = ZhouNianQing2023_Setting.GetData()
            local fxId1 = self.CurModelIdx == 1 and data.Shouzhuo_Lan_L or data.Shouzhuo_Hong_L
            local fxId2 = self.CurModelIdx == 1 and data.Shouzhuo_Lan_R or data.Shouzhuo_Hong_R
            local fx1 = CEffectMgr.Inst:AddObjectFX(fxId1,renderObj,0, 1, 1, nil, false, EnumWarnFXType.None,Vector3.zero,Vector3.zero,nil)
            local fx2 = CEffectMgr.Inst:AddObjectFX(fxId2,renderObj,0, 1, 1, nil, false, EnumWarnFXType.None,Vector3.zero,Vector3.zero,nil)
            renderObj:RemoveFX(fxId1)
            renderObj:RemoveFX(fxId2)
            renderObj:AddFX(fxId1,fx1)
            renderObj:AddFX(fxId2,fx2)
        --end,33)
    end
end

function LuaFashionPreviewTextureLoaderMgr:PreviewZsw(this, renderObj) 
    if this.suitPreivew then
        this.suitPreivew.gameObject:SetActive(false)
    end
    local data = Zhuangshiwu_Zhuangshiwu.GetData(LuaFashionPreviewTextureLoaderMgr.PreviewZswID)
    if data ~= nil then
        renderObj:LoadMain(CClientFurniture.GetPrefabPath(data.ResName, data.ResId, data.ID))
        if data.FX and data.FX ~= 0 then
            local fx = CEffectMgr.Inst:AddObjectFX(data.FX, renderObj, 0, 1, 1, nil, false, EnumWarnFXType.None,Vector3.zero,Vector3.zero,nil)
            renderObj:AddFX(data.FX, fx)
        end
    end
end

function LuaFashionPreviewTextureLoaderMgr:AddFashionBuffFx(this, renderObj, fakeAppear)
    local buffFxArray = LuaAppearancePreviewMgr:GetFashionBuffFxByAppear(fakeAppear)
    if buffFxArray then
        for i=0,buffFxArray.Length-1 do
            local fx = CEffectMgr.Inst:AddObjectFX(buffFxArray[i], renderObj, 0, renderObj.Scale, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil, false)
		    renderObj:AddFX(buffFxArray[i], fx)            
        end
    end
end