local CMainCamera = import "L10.Engine.CMainCamera"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local Color = import "UnityEngine.Color"
local Physics = import "UnityEngine.Physics"
local Vector3 = import "UnityEngine.Vector3"
local MeshFilter = import "UnityEngine.MeshFilter"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local Mesh = import "UnityEngine.Mesh"
local Material = import "UnityEngine.Material"
local ShadowCastingMode = import "UnityEngine.Rendering.ShadowCastingMode"
local Tags = import "L10.Game.Tags"
local CRenderObject = import "L10.Engine.CRenderObject"
local CUIResources = import "L10.UI.CUIResources"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaCakeCuttingWnd=class()
RegistChildComponent(LuaCakeCuttingWnd,"closeBtn", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"showTexture", UITexture)
RegistChildComponent(LuaCakeCuttingWnd,"leftOpNode", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"top1Node", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"top2Node", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"top3Node", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"selfNode", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"leftTimeLabel", UILabel)
RegistChildComponent(LuaCakeCuttingWnd,"skillNode", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"rankBtn", GameObject)
RegistChildComponent(LuaCakeCuttingWnd,"topLeftArea", GameObject)

RegistClassMember(LuaCakeCuttingWnd, "m_OriginalShowAllObject")
RegistClassMember(LuaCakeCuttingWnd, "cameraNode")
RegistClassMember(LuaCakeCuttingWnd, "cameraFollowNode")
RegistClassMember(LuaCakeCuttingWnd, "cameraScript")

RegistClassMember(LuaCakeCuttingWnd, "screenWidthRatio")
RegistClassMember(LuaCakeCuttingWnd, "screenHeightRatio")

RegistClassMember(LuaCakeCuttingWnd, "mapNodeTable")
RegistClassMember(LuaCakeCuttingWnd, "mapNodeCache")
RegistClassMember(LuaCakeCuttingWnd, "colorIndexTable")
RegistClassMember(LuaCakeCuttingWnd, "colorPathIndexTable")
RegistClassMember(LuaCakeCuttingWnd, "worldHeight")
RegistClassMember(LuaCakeCuttingWnd, "xShift")
RegistClassMember(LuaCakeCuttingWnd, "yShift")
RegistClassMember(LuaCakeCuttingWnd, "skillCD")
RegistClassMember(LuaCakeCuttingWnd, "skillCDSprite")

RegistClassMember(LuaCakeCuttingWnd, "needInitNodeTable")

RegistClassMember(LuaCakeCuttingWnd, "fightHisShowTable")
RegistClassMember(LuaCakeCuttingWnd, "fightHisShowTime")
RegistClassMember(LuaCakeCuttingWnd, "fightHisShowTickTime")

function LuaCakeCuttingWnd:GenFightShowText(info)
	--{playerId = playerId,killedByPlayerId = killedByPlayerId,continueKillNum = continueKillNum}
	local playerId = info.playerId
	local killedByPlayerId = info.killedByPlayerId
	local continueKillNum = info.continueKillNum

	if playerId == killedByPlayerId then
		local playerInfo = LuaCakeCuttingMgr.PlayerInfo[playerId]

		if killedByPlayerId == CClientMainPlayer.Inst.Id then
			return g_MessageMgr:FormatMessage("ZNQ_CAKECUTTING_KILL_6", playerInfo.Name)
		else
			return g_MessageMgr:FormatMessage("ZNQ_CAKECUTTING_KILL_1", playerInfo.Name)
		end
	else
		local playerInfo = LuaCakeCuttingMgr.PlayerInfo[playerId]
		local killPlayerInfo = LuaCakeCuttingMgr.PlayerInfo[killedByPlayerId]
		if killedByPlayerId == CClientMainPlayer.Inst.Id or playerId == CClientMainPlayer.Inst.Id then
			if continueKillNum < 2 then
				return g_MessageMgr:FormatMessage("ZNQ_CAKECUTTING_KILL_3", killPlayerInfo.Name,playerInfo.Name)
			else
				return g_MessageMgr:FormatMessage("ZNQ_CAKECUTTING_KILL_5", killPlayerInfo.Name,playerInfo.Name,continueKillNum)
			end
		else
			if continueKillNum < 2 then
				return g_MessageMgr:FormatMessage("ZNQ_CAKECUTTING_KILL_2", killPlayerInfo.Name,playerInfo.Name)
			else
				return g_MessageMgr:FormatMessage("ZNQ_CAKECUTTING_KILL_4", killPlayerInfo.Name,playerInfo.Name,continueKillNum)
			end
		end
	end

	return ''
end

function LuaCakeCuttingWnd:InitFightInfo()
	if not self.fightHisShowTable then
		return
	end

	local textNodeTable = {self.topLeftArea.transform:Find('Info/text1'):GetComponent(typeof(UILabel)),
	self.topLeftArea.transform:Find('Info/text2'):GetComponent(typeof(UILabel)),
	self.topLeftArea.transform:Find('Info/text3'):GetComponent(typeof(UILabel)),
	self.topLeftArea.transform:Find('Info/text4'):GetComponent(typeof(UILabel))}
	for i,v in ipairs(textNodeTable)	do
		v.text = ''
	end

	local count = 1
	for i=#self.fightHisShowTable,1,-1 do
		local node = textNodeTable[count]
		if node then
			node.text = self:GenFightShowText(self.fightHisShowTable[i])
		else
			break
		end
		count = count + 1
	end
end

function LuaCakeCuttingWnd:UpdateFightInfo()
	if not self.fightHisShowTickTime then
		self.fightHisShowTickTime = 1
	end

	local nowTime = os.time()
	if not self.fightHisShowTime then
		self.fightHisShowTime = os.time()
	end

	if nowTime - self.fightHisShowTime >= self.fightHisShowTickTime then
		if LuaCakeCuttingMgr.KillInfoTable and #LuaCakeCuttingMgr.KillInfoTable > 0 then
			local info = table.remove(LuaCakeCuttingMgr.KillInfoTable,1)
			if info then
				if not self.fightHisShowTable then
					self.fightHisShowTable = {}
				end
				table.insert(self.fightHisShowTable,info)
				self:InitFightInfo()
				self.fightHisShowTime = nowTime
			end
		end
	end
end

function LuaCakeCuttingWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateStarInviteAudienceRelease", self, "UpdateStarInviteAudienceRelease")
	g_ScriptEvent:AddListener("CakeCuttingAddPathNode", self, "CakeCuttingAddNode")
	g_ScriptEvent:AddListener("CakeCuttingClearPath", self, "CakeCuttingClearPath")
	g_ScriptEvent:AddListener("CakeCuttingClearColor", self, "CakeCuttingClearColor")
	g_ScriptEvent:AddListener("CakeCuttingSyncDiff", self, "CakeCuttingSyncDiff")
	g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateScreen")
  g_ScriptEvent:AddListener('CakeCuttingUpdateShowInfo', self, 'UpdateShowInfo')
  g_ScriptEvent:AddListener('CakeCuttingEnd', self, 'GameEnd')
end

function LuaCakeCuttingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateStarInviteAudienceRelease", self, "UpdateStarInviteAudienceRelease")
	g_ScriptEvent:RemoveListener("CakeCuttingAddPathNode", self, "CakeCuttingAddNode")
	g_ScriptEvent:RemoveListener("CakeCuttingClearPath", self, "CakeCuttingClearPath")
	g_ScriptEvent:RemoveListener("CakeCuttingClearColor", self, "CakeCuttingClearColor")
	g_ScriptEvent:RemoveListener("CakeCuttingSyncDiff", self, "CakeCuttingSyncDiff")
	g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateScreen")
  g_ScriptEvent:RemoveListener('CakeCuttingUpdateShowInfo', self, 'UpdateShowInfo')
  g_ScriptEvent:RemoveListener('CakeCuttingEnd', self, 'GameEnd')
end

function LuaCakeCuttingWnd:GameEnd()
	local infoNode = self.top1Node.transform.parent.gameObject
	if infoNode then
		infoNode:SetActive(false)
	end
end

function LuaCakeCuttingWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaCakeCuttingWnd:UpdateScreen()
	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
	end
	self.cameraNode = nil
	local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end
	self.screenWidthRatio = self.showTexture.width / screenWidth
	self.screenHeightRatio = self.showTexture.height / screenHeight

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__CakeCuttingCamera__")
	self.cameraFollowNode = GameObject("__CakeCuttingFollowNode__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
	cameraScript.clearFlags = CameraClearFlags.SolidColor
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC + 2^LayerDefine.MainPlayer + 2^LayerDefine.OtherPlayer
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 36
  cameraScript.farClipPlane = 40
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = self.cameraFollowNode.transform
  self.cameraFollowNode.transform.parent = CUIManager.instance.transform
	--local settingData = StarBiWuShow_Setting.GetData()
	--local cameraPos = LuaStarInviteMgr.split(settingData.CameraPos,',')
	--local cameraRatation = LuaStarInviteMgr.split(settingData.CameraRotate,',')
	self.cameraNode.transform.position = Vector3(0,15,-15)
  self.cameraNode.transform.localRotation = Quaternion.Euler(45,0,0)
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript
end

function LuaCakeCuttingWnd:CakeCuttingSyncDiff()
	if LuaCakeCuttingMgr.MapDiffTable then
		while true do
			local diffData = table.remove(LuaCakeCuttingMgr.MapDiffTable,1)
			if diffData then
				local count = diffData.Count
				for i=0,count-1 do
					local data = diffData[i]
					local x = tonumber(data[0])
					local y = tonumber(data[1])
					local value = tonumber(data[2])
					--if not self.needInitNodeTable then
					--self.needInitNodeTable = {}
					--end
					--table.insert(self.needInitNodeTable,{x=x,y=y,value=value})
					self:AddNodeAtPos(x,y,nil,value,2)
				end
			else
				break
			end
		end
	end
  g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowInfo')
end

function LuaCakeCuttingWnd:LeaveWnd()
	CUIManager.CloseUI(CLuaUIResources.CakeCuttingWnd)
	Gac2Gas.RequestLeavePlay()
end

function LuaCakeCuttingWnd:AddNodeToCache(nodeInfoTable)
	if not nodeInfoTable then
		return
	end

	if not self.mapNodeCache then
		self.mapNodeCache = {}
	end
	if nodeInfoTable.oriPlayerId then
		nodeInfoTable.oriPlayerId = nil
	end
	table.insert(self.mapNodeCache,nodeInfoTable)
end

function LuaCakeCuttingWnd:CakeCuttingClearColor(colorIndex)
	if self.mapNodeTable then
		local playerId = self:GetPlayerIdThoughColorIdx(colorIndex)
		for i,v in pairs(self.mapNodeTable) do
			if v.oriPlayerId and v.oriPlayerId == playerId and v.ntype == 1 then
				v.oriPlayerId = nil
			elseif v.colorIndex == colorIndex and v.ntype == 2 then
				if v.node then
					v.node.gameObject:SetActive(false)
					self:AddNodeToCache(v)
				end
				self.mapNodeTable[i] = nil
			end
		end
	end
	g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowInfo')
end

function LuaCakeCuttingWnd:CakeCuttingClearPath(playerId)
	if self.mapNodeTable then
		for i,v in pairs(self.mapNodeTable) do
			if v.playerId == playerId and v.ntype == 1 then
				local oriPlayerId = v.oriPlayerId
				if v.node then
					v.node.gameObject:SetActive(false)
					self:AddNodeToCache(v)
				end
				self.mapNodeTable[i] = nil
				if oriPlayerId then
					local oriColorIndex = LuaCakeCuttingMgr.PlayerInfo[oriPlayerId].ColorIdx
					self:AddNodeAtPos(v.x,v.y,oriPlayerId,oriColorIndex,2)
				end
			end
		end
	end
end

function LuaCakeCuttingWnd:GetNodeIndex(x,y)
 return	x * LuaCakeCuttingMgr.MapWidth + y
end

function LuaCakeCuttingWnd:CakeCuttingAddNode(nodeInfo)
	local playerId = nodeInfo.playerId
	local x = nodeInfo.x
	local y = nodeInfo.y
	self:AddNodeAtPos(x,y,playerId,nil,1)
end

function LuaCakeCuttingWnd:SetNodeInfo(data,x,y,playerId,nodeType)
	--nodeType : 1 path 2 grid
	if data then
		if LuaCakeCuttingMgr.PlayerInfo[playerId] then
			local colorIndex = LuaCakeCuttingMgr.PlayerInfo[playerId].ColorIdx
			local color = self.colorIndexTable[colorIndex]
			if nodeType == 1 then
				color = self.colorPathIndexTable[colorIndex]
			end
			if color then
				--if data.ntype == 1 and data.node.gameObject.activeSelf and nodeType == 2 then
				--	return
				--end
				self:SetNodeColor(data.node,color)
				data.node.gameObject:SetActive(true)
				data.playerId = playerId
				data.x = x
				data.y = y
				data.ntype = nodeType
				data.colorIndex = colorIndex
				if nodeType == 2 then
					data.oriPlayerId = playerId
				end
				data.node.transform.position = Vector3(x + self.xShift,self.worldHeight,y + self.yShift)
				local nodeIndex = self:GetNodeIndex(x,y)
				self.mapNodeTable[nodeIndex] = data
			end
		end
	end
end

function LuaCakeCuttingWnd:SetNodeColor(node, color)
	node.sharedMaterial:SetColor('_Color', color)
end

function LuaCakeCuttingWnd:GenNewNode(x,y,playerId,__colorIndex,nodeType)
	local nodeIndex = self:GetNodeIndex(x,y)
	local colorIndex = nil
	if playerId then
		if LuaCakeCuttingMgr.PlayerInfo[playerId] then
			colorIndex = tonumber(LuaCakeCuttingMgr.PlayerInfo[playerId].ColorIdx)
		end
	elseif __colorIndex then
		colorIndex = __colorIndex
		if not playerId then
			for i,v in pairs(LuaCakeCuttingMgr.PlayerInfo) do
				if tonumber(v.ColorIdx) == colorIndex then
					playerId = tonumber(v.PlayerId)
				end
			end
		end
	end

	if colorIndex then
		local color = self.colorIndexTable[colorIndex]
		if nodeType and nodeType == 1 then
			color = self.colorPathIndexTable[colorIndex]
		end
		if color then
			local node = GameObject("__GridNode__")
			node.transform.position = Vector3(x + self.xShift,self.worldHeight,y + self.yShift)
			node.transform.localEulerAngles = Vector3.zero
			local mf = node:AddComponent(typeof(MeshFilter))
			local mMesh = CreateFromClass(Mesh)
			mMesh.name = 'grid'
			local mVTable = {Vector3(-0.5,0,-0.5),Vector3(-0.5,0,0.5),Vector3(0.5,0,0.5),Vector3(0.5,0,-0.5)}--,Vector3(-0.5,0.1,-0.5),Vector3(-0.5,0.1,0.5),Vector3(0.5,0.1,0.5),Vector3(0.5,0.1,-0.5)}
			local mTTable = {
				0,1,2,
				0,2,3,
			}
				--0,7,4,
				--0,3,7,
				--7,3,6,
				--3,2,6,
				--1,5,6,
				--2,1,6,
				--0,4,5,
				--0,5,1,
				--4,7,5,
				--5,7,6}
			local mNormalTable = {Vector3(0,0,1),Vector3(0,0,1),Vector3(0,0,1),Vector3(0,0,1)}
			--		for i=1,4*count do
			--			table.insert(mVTable,CreateFromClass(Vector3))
			--		end
			--		for i=1,6*count do
			--			table.insert(mTTable,0)
			--		end
			local mVertices = Table2ArrayWithCount(mVTable, #mVTable, MakeArrayClass(Vector3))
			local mTriangles = Table2ArrayWithCount(mTTable, #mTTable, MakeArrayClass(int))
			local mNormals = Table2ArrayWithCount(mNormalTable, #mNormalTable, MakeArrayClass(Vector3))
			mMesh.vertices = mVertices
			mMesh.triangles = mTriangles
			mMesh.normals = mNormals
			mf.sharedMesh = mMesh

			local mr = node:AddComponent(typeof(MeshRenderer))
			mr.sharedMaterial = CreateFromClass(Material,CRenderObject.s_CakeCuttingMaterial)
			self:SetNodeColor(mr,color)
			--mr.sharedMaterial:SetColor('_GridColor', color)
			mr.shadowCastingMode = ShadowCastingMode.Off
			mr.receiveShadows = false
			--if not self.mapNodeTable then
			--	self.mapNodeTable = {}
			--end
			self.mapNodeTable[nodeIndex].node = mr -- = {node = mr,playerId = playerId,oriPlayerId = playerId,ntype = nodeType,colorIndex = colorIndex}
			self.mapNodeTable[nodeIndex].genFinish = true

		end
	end
end

function LuaCakeCuttingWnd:GetPlayerIdThoughColorIdx(colorIdx)
	if LuaCakeCuttingMgr.PlayerInfo and colorIdx then
		for i,v in pairs(LuaCakeCuttingMgr.PlayerInfo) do
			if tonumber(v.ColorIdx) == colorIdx then
				return tonumber(v.PlayerId)
			end
		end
	end
end

function LuaCakeCuttingWnd:AddNodeAtPos(x,y,playerId,__colorIndex,nodeType)
	local nodeIndex = self:GetNodeIndex(x,y)
	if not playerId then
		playerId = self:GetPlayerIdThoughColorIdx(__colorIndex)
	end

	if not self.mapNodeTable then
		self.mapNodeTable = {}
	end
	if not self.mapNodeTable[nodeIndex] then
		if self.mapNodeCache and self.mapNodeCache[#self.mapNodeCache] then
			local data = self.mapNodeCache[#self.mapNodeCache]
			self:SetNodeInfo(data,x,y,playerId,nodeType)
			table.remove(self.mapNodeCache)
		else
			local data = {nodeIndex = nodeIndex,x=x,y=y,playerId = playerId,ntype = nodeType,colorIndex = __colorIndex}
			if nodeType == 2 then
				data.oriPlayerId = playerId
			end
			self.mapNodeTable[nodeIndex] = data
			if not self.needInitNodeTable then
				self.needInitNodeTable = {}
			end
			table.insert(self.needInitNodeTable,data)
		end
	else
		local data = self.mapNodeTable[nodeIndex]
		if data.node then
			self:SetNodeInfo(data,x,y,playerId,nodeType)
		else
			data.playerId = playerId
			data.colorIndex = __colorIndex

		end
	end

end

function LuaCakeCuttingWnd:InitArea()
	local count = LuaCakeCuttingMgr.MapHeight * LuaCakeCuttingMgr.MapWidth
	for i=1,count do
		local x = i % LuaCakeCuttingMgr.MapWidth
		local y = (i - x) / LuaCakeCuttingMgr.MapHeight + 1
		local colorIndex = LuaCakeCuttingMgr.MapInitData[i]
		if colorIndex and colorIndex > 0 then
			self:AddNodeAtPos(x,y,nil,colorIndex,2)
		end
	end
end

function LuaCakeCuttingWnd:Init()
	local onCloseClick = function(go)
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出吗？"), DelegateFactory.Action(function ()
				self:LeaveWnd()
		end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	CPreDrawMgr.m_bEnableRectPreDraw = false

	local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end
	self.screenWidthRatio = self.showTexture.width / screenWidth
	self.screenHeightRatio = self.showTexture.height / screenHeight

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__CakeCuttingCamera__")
	self.cameraFollowNode = GameObject("__CakeCuttingFollowNode__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)

	cameraScript.clearFlags = CameraClearFlags.SolidColor
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC + 2^LayerDefine.MainPlayer + 2^LayerDefine.OtherPlayer
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 36
  cameraScript.farClipPlane = 400
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = self.cameraFollowNode.transform
  self.cameraFollowNode.transform.parent = CUIManager.instance.transform
	--local settingData = StarBiWuShow_Setting.GetData()
	--local cameraPos = LuaStarInviteMgr.split(settingData.CameraPos,',')
	--local cameraRatation = LuaStarInviteMgr.split(settingData.CameraRotate,',')
	self.cameraNode.transform.position = Vector3(0,15,-15)
  self.cameraNode.transform.localRotation = Quaternion.Euler(45,0,0)
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript
	--lineNode = GameObject("__KitePlayLine__")
	--local lineRenderScript = lineNode:AddComponent(typeof(LineRenderer))
	--lineRenderScript.material:SetColor("_Color", Color.white)
	--lineRenderScript:SetWidth(1,1)
	--lineRenderScript:SetPosition(0,cameraNode.transform.position)
	--lineRenderScript:SetPosition(1,Vector3(selfData.kitePos[1],selfData.kitePos[2],selfData.kitePos[3]))

	self.m_OriginalShowAllObject = CRenderScene.Inst.ShowAllObject
	CRenderScene.Inst.ShowAllObject = true

	if CClientMainPlayer.Inst then
		self.cameraFollowNode.transform.position = CClientMainPlayer.Inst.WorldPos
		self.worldHeight = self.cameraFollowNode.transform.position.y + 0.1
	end

	self.xShift = 0.5 + ZhouNianQing_CakeCutting.GetData().OffsetX
	self.yShift = 0.5 + ZhouNianQing_CakeCutting.GetData().OffsetY
	self.skillCD = ZhouNianQing_CakeCutting.GetData().SpeedUpCD
	self.skillCDSprite = self.skillNode.transform:Find('Mask/CD'):GetComponent(typeof(UISprite))
	self.skillCDSprite.fillAmount = 0

	self:InitOpNode()
	self:InitArea()
	self:UpdateShowInfo()
	self:InitPlayerPath()
	self:CakeCuttingSyncDiff()

	CUIManager.CloseUI(CUIResources.ThumbnailChatWindow)
	CUIManager.ShowUI(CLuaUIResources.CakeCuttingChatWnd)
end

function LuaCakeCuttingWnd:InitPlayerPath()
	for i,v in pairs(LuaCakeCuttingMgr.PlayerInfo) do
		if v.Path then
			local count = v.Path.Count -1
			local playerId = v.PlayerId
			for i=0,count do
				local data = v.Path[i]
				local x = data.x
				local y = data.y
				self:AddNodeAtPos(x,y,playerId,nil,1)
			end
		end
	end
end

local KeyCode = import "UnityEngine.KeyCode"
local Input = import "UnityEngine.Input"

function LuaCakeCuttingWnd:Update()
	if CClientMainPlayer.Inst then
		self.cameraFollowNode.transform.position = CClientMainPlayer.Inst.WorldPos
	end
	if LuaCakeCuttingMgr.PlayerEndTime then
		local restTime = LuaCakeCuttingMgr.PlayerEndTime - CServerTimeMgr.Inst.timeStamp
		if restTime <= 0 then
			--self:LeaveWnd()
			self.leftTimeLabel.text = '0:00'
		else
			local min = math.floor(restTime/60)
			local sec = math.floor(restTime - min * 60)
			if sec < 10 then
				sec = '0'..sec
			end
			local s = min .. ':' .. sec
			self.leftTimeLabel.text = s
		end
	end
	if LuaCakeCuttingMgr.SkillNextTime and LuaCakeCuttingMgr.SkillNextTime > 0 then
		local restTime = LuaCakeCuttingMgr.SkillNextTime - CServerTimeMgr.Inst.timeStamp
		if restTime <= 0 then
			self.skillCDSprite.fillAmount = 0
			--self.skillNode:GetComponent(typeof(BoxCollider)).enabled = true
			LuaCakeCuttingMgr.SkillNextTime = 0
		else
			local percent = restTime / self.skillCD
			self.skillCDSprite.fillAmount = percent
		end
	end
	if self.needInitNodeTable then
		local count = #self.needInitNodeTable
		local start = count - 5
		if start < 1 then
			start = 1
		end
		for i=count,start,-1 do
			local data = self.needInitNodeTable[i]
			if self.mapNodeTable[data.nodeIndex] then
				self:GenNewNode(data.x,data.y,data.playerId,data.colorIndex,data.ntype)
			end
			--local playerId = 0
			--for i,v in pairs(LuaCakeCuttingMgr.PlayerInfo) do
				--if tonumber(v.ColorIdx) == colorIndex then
					--playerId = tonumber(v.PlayerId)
				--end
			--end
			--self:CakeCuttingAddNode({x=data.x,y=data.y,playerId=playerId})
			table.remove(self.needInitNodeTable,i)
		end
		if self.needInitNodeTable and not next(self.needInitNodeTable) then
			g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowInfo')
			self.needInitNodeTable = nil
		end
	end
	--
	if Input.GetKey(KeyCode.W) then
		Gac2Gas.CakeCuttingPlaySetDirection(0,1)
	elseif Input.GetKey(KeyCode.S) then
		Gac2Gas.CakeCuttingPlaySetDirection(0,-1)
	elseif Input.GetKey(KeyCode.A) then
		Gac2Gas.CakeCuttingPlaySetDirection(-1,0)
	elseif Input.GetKey(KeyCode.D) then
		Gac2Gas.CakeCuttingPlaySetDirection(1,0)
	end

	--
	self:UpdateFightInfo()
end

function LuaCakeCuttingWnd:WorldPos2ScreenPos(worldPos, camera)
	local screenPos = camera:WorldToScreenPoint(worldPos)
	local localPos = {x = screenPos.x, y = screenPos.y}
	return Vector3(localPos.x - self.showTexture.width/2, localPos.y - self.showTexture.height/2,0)
end

function LuaCakeCuttingWnd:ScreenPoint2WorldPos(screenPoint,watchCamera)
	local hit = nil
	local ray = watchCamera:ScreenPointToRay(screenPoint)
	local dist = watchCamera.farClipPlane
	local hits = Physics.RaycastAll(ray, dist, watchCamera.cullingMask)
	if not hits or hits.Length == 0 then
		return nil
	end
	local hitDis = watchCamera.farClipPlane
	for i=0,hits.Length - 1 do
		local hitData = hits[i]
		if hitData.collider:CompareTag(Tags.Ground) then
			if hitDis > hitData.distance then
				hit = hitData
				hitDis = hitData.distance
			end
		end
	end

	if hit then
		--local pixelPos = Utility.WorldPos2PixelPos(hit.point)
		return hit.point --{x = hit.point.x, z = hit.point.z}
	end
end

function LuaCakeCuttingWnd:UpdateShowInfo()
	self:SetShowData()
end

function LuaCakeCuttingWnd:SetShowData()
	local topNodeTable = {self.top1Node,self.top2Node,self.top3Node}

	--self.mapNodeTable[nodeIndex] = {node = mr,playerId = playerId,ntype = 1,colorIndex = colorIndex}
	local selfValue = 0
	local selfId = CClientMainPlayer.Inst.Id
	if self.mapNodeTable then
		local mapPTable = {}
		for i,v in pairs(self.mapNodeTable) do
			if v.oriPlayerId then
					if not mapPTable[v.oriPlayerId] then
						mapPTable[v.oriPlayerId] = 0
					end
					mapPTable[v.oriPlayerId] = mapPTable[v.oriPlayerId] + 1
					if v.oriPlayerId == selfId then
						selfValue = selfValue + 1
					end
			end
		end

		local mapSTable = {}
		for i,v in pairs(mapPTable) do
			local playerInfo = LuaCakeCuttingMgr.PlayerInfo[i]
			local killNum = 0
			if playerInfo then
				killNum = playerInfo.KillNum
			end
			table.insert(mapSTable,{playerId = i,value = v,killNum = killNum})
		end
		table.sort(mapSTable,function(a,b)
			if a.value ~= b.value then
				return a.value > b.value
			elseif a.killNum ~= b.killNum then
				return a.killNum > b.killNum
			else
				return a.playerId < b.playerId
			end
		end)
		LuaCakeCuttingMgr.PlayerRankTable = mapSTable
	end

	local maxValue = 1
	if LuaCakeCuttingMgr.PlayerRankTable and #LuaCakeCuttingMgr.PlayerRankTable > 0 then
		maxValue = LuaCakeCuttingMgr.PlayerRankTable[1].value
	end

	local totalPixelNum = LuaCakeCuttingMgr.MapHeight * LuaCakeCuttingMgr.MapWidth
	for i,v in ipairs(topNodeTable) do
		if LuaCakeCuttingMgr.PlayerRankTable and LuaCakeCuttingMgr.PlayerRankTable[i] then
			local playerInfo = LuaCakeCuttingMgr.PlayerInfo[tonumber(LuaCakeCuttingMgr.PlayerRankTable[i].playerId)]
			v.transform:Find('name'):GetComponent(typeof(UILabel)).text = playerInfo.Name
			local value = LuaCakeCuttingMgr.PlayerRankTable[i].value
			v.transform:Find('percent'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%.2f%%', value * 100 / totalPixelNum)
			v.transform:Find('slider'):GetComponent(typeof(UISlider)).value = value / maxValue
			if self.colorIndexTable[playerInfo.ColorIdx] then
				 v.transform:Find('slider/bar'):GetComponent(typeof(UISprite)).color = self.colorIndexTable[playerInfo.ColorIdx]
			end
		else
			v.transform:Find('name'):GetComponent(typeof(UILabel)).text = ''
			v.transform:Find('percent'):GetComponent(typeof(UILabel)).text = ''
			v.transform:Find('slider'):GetComponent(typeof(UISlider)).value = 0
		end
	end

	if LuaCakeCuttingMgr.PlayerRankTable then
		for i,v in pairs(LuaCakeCuttingMgr.PlayerRankTable) do
			if v.playerId == selfId then
				local playerInfo = LuaCakeCuttingMgr.PlayerInfo[selfId]
				self.selfNode.transform:Find('name'):GetComponent(typeof(UILabel)).text = playerInfo.Name
				--playerInfo.KillNum
				self.selfNode.transform:Find('percent'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%.2f%%', v.value * 100 / totalPixelNum)
				self.selfNode.transform:Find('slider'):GetComponent(typeof(UISlider)).value = v.value / maxValue
				if self.colorIndexTable[playerInfo.ColorIdx] then
					self.selfNode.transform:Find('slider/bar'):GetComponent(typeof(UISprite)).color = self.colorIndexTable[playerInfo.ColorIdx]
				end
				self.selfNode.transform:Find('rank'):GetComponent(typeof(UILabel)).text = i
			end
		end
	end

end

function LuaCakeCuttingWnd:InitOpNode()
	self.colorIndexTable = {}
	self.colorPathIndexTable = {}
	for i = 1,10 do
		local colorString = ZhouNianQing_CakeCuttingColor.GetData(i).Color
		local colorTable = self:Split(colorString,',')
		self.colorIndexTable[i] = Color(colorTable[1]/255,colorTable[2]/255,colorTable[3]/255)

		local colorPathString = ZhouNianQing_CakeCuttingColor.GetData(i).PathColor
		local colorPathTable = self:Split(colorPathString,',')
		self.colorPathIndexTable[i] = Color(colorPathTable[1]/255,colorPathTable[2]/255,colorPathTable[3]/255)
	end

	local upBtn = self.leftOpNode.transform:Find('upBtn').gameObject
	local downBtn = self.leftOpNode.transform:Find('downBtn').gameObject
	local rightBtn = self.leftOpNode.transform:Find('rightBtn').gameObject
	local leftBtn = self.leftOpNode.transform:Find('leftBtn').gameObject

	local onUpClick = function(go)
		Gac2Gas.CakeCuttingPlaySetDirection(0,1)
	end
	CommonDefs.AddOnClickListener(upBtn,DelegateFactory.Action_GameObject(onUpClick),false)
	local onDownClick = function(go)
		Gac2Gas.CakeCuttingPlaySetDirection(0,-1)
	end
	CommonDefs.AddOnClickListener(downBtn,DelegateFactory.Action_GameObject(onDownClick),false)
	local onLeftClick = function(go)
		Gac2Gas.CakeCuttingPlaySetDirection(-1,0)
	end
	CommonDefs.AddOnClickListener(leftBtn,DelegateFactory.Action_GameObject(onLeftClick),false)
	local onRightClick = function(go)
		Gac2Gas.CakeCuttingPlaySetDirection(1,0)
	end
	CommonDefs.AddOnClickListener(rightBtn,DelegateFactory.Action_GameObject(onRightClick),false)

	local skillId = ZhouNianQing_CakeCutting.GetData().TransformSkillId
	if skillId then
		local skillData = Skill_AllSkills.GetData(skillId)
		if skillData then
			self.skillNode.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadSkillIcon(skillData.SkillIcon)
			local skillClickFunction = function(go)
				Gac2Gas.CakeCuttingPlaySpeedUp()
				--self.skillNode:GetComponent(typeof(BoxCollider)).enabled = true
			end
			CommonDefs.AddOnClickListener(self.skillNode,DelegateFactory.Action_GameObject(skillClickFunction),false)
		end
	end

	local onRankBtnClick = function(go)
		CUIManager.ShowUI(CLuaUIResources.CakeCuttingFightInfoWnd)
	end
	CommonDefs.AddOnClickListener(self.rankBtn,DelegateFactory.Action_GameObject(onRankBtnClick),false)
end

function LuaCakeCuttingWnd:OnDestroy()
	CPreDrawMgr.m_bEnableRectPreDraw = true
	CRenderScene.Inst.ShowAllObject = self.m_OriginalShowAllObject

	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
	end
	self.cameraNode = nil
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)

	if self.cameraFollowNode then
		GameObject.Destroy(self.cameraFollowNode)
	end
	self.cameraFollowNode = nil

	if self.mapNodeTable then
		for i,v in pairs(self.mapNodeTable) do
			if v.node then
				GameObject.Destroy(v.node.gameObject)
			end
		end
	end
	self.mapNodeTable = nil

	if self.mapNodeCache then
		for i,v in pairs(self.mapNodeCache) do
			if v.node then
				GameObject.Destroy(v.node.gameObject)
			end
		end
	end
	self.mapNodeCache = nil
	LuaCakeCuttingMgr.PlayerInfo = {}
	LuaCakeCuttingMgr.MapHeight = 0
	LuaCakeCuttingMgr.MapWidth = 0
	LuaCakeCuttingMgr.MapInitData = nil
	LuaCakeCuttingMgr.PlayerEndTime = nil
	LuaCakeCuttingMgr.SkillNextTime = nil
	LuaCakeCuttingMgr.PlayerRankTable = nil
	LuaCakeCuttingMgr.MapDiffTable = {}
	LuaCakeCuttingMgr.PlayerRankTable = {}

	CUIManager.CloseUI(CLuaUIResources.CakeCuttingChatWnd)
	CUIManager.ShowUI(CUIResources.ThumbnailChatWindow)
end

return LuaCakeCuttingWnd
