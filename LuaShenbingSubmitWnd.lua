local LuaGameObject=import "LuaGameObject"
require("design/Equipment/ShenBing")

local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"

CLuaShenbingSubmitWnd = class()
RegistClassMember(CLuaShenbingSubmitWnd, "m_ProgressSliderTable")
RegistClassMember(CLuaShenbingSubmitWnd, "m_ItemTemplateTable")
RegistClassMember(CLuaShenbingSubmitWnd, "m_ItemOwnCountTable")
RegistClassMember(CLuaShenbingSubmitWnd, "m_ItemOwnCountLabelTable")
RegistClassMember(CLuaShenbingSubmitWnd, "m_ItemCurrentCountTable")
RegistClassMember(CLuaShenbingSubmitWnd, "m_TotalCountLabel")
RegistClassMember(CLuaShenbingSubmitWnd, "m_ItemInputTable")
RegistClassMember(CLuaShenbingSubmitWnd, "m_ItemTextureTable")
RegistClassMember(CLuaShenbingSubmitWnd, "m_CurrentSelectedItem")
RegistClassMember(CLuaShenbingSubmitWnd, "m_SubmitQnbtn")
RegistClassMember(CLuaShenbingSubmitWnd, "m_GotObjTable")

RegistClassMember(CLuaShenbingSubmitWnd, "m_QianyuanTemplateIdTable")

function CLuaShenbingSubmitWnd:ParseDesignData()
	self.m_QianyuanTemplateIdTable = {}
	for i = 1, 3 do
		local templateId = 0
		if i == 1 then templateId = ShenBing_Setting.GetData().Qy1ItemId
		elseif i == 2 then templateId = ShenBing_Setting.GetData().Qy2ItemId
		else templateId = ShenBing_Setting.GetData().Qy3ItemId end
		self.m_QianyuanTemplateIdTable[i] = templateId
	end
end

function CLuaShenbingSubmitWnd:Init( ... )
	if CommonDefs.IS_VN_CLIENT then
		local totalCountLabel = LuaGameObject.GetChildNoGC(self.transform, "TotalCountLabel")
		totalCountLabel.transform.localPosition = Vector3(-83, 332, 0)
	end
	
	self:ParseDesignData()
	local itemNameTable = {}
	self.m_ItemTemplateTable = {}
	self.m_ItemOwnCountTable = {}
	self.m_ItemInputTable = {}
	self.m_ItemTextureTable = {}
	self.m_ItemOwnCountLabelTable = {}
	LuaShenbingMgr.ShenbingQianyuanItemTable = {}
	self.m_ProgressSliderTable = {}
	self.m_GotObjTable = {}

	-- progress
	for i = 1, 3 do
		self.m_ProgressSliderTable[i] = LuaGameObject.GetChildNoGC(self.transform, "Progress"..i).slider
	end

	local equip = CItemMgr.Inst:GetById(LuaShenbingMgr.ShenbingQianyuanEquipItemId)
	if not equip then
		CUIManager.CloseUI(CLuaUIResources.ShenBingSubmitWnd)
		return
	end

	-- buttons
	local g = LuaGameObject.GetChildNoGC(self.transform, "CancelBtn").gameObject
	UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
		CUIManager.CloseUI(CLuaUIResources.ShenBingSubmitWnd)
	end)

	local submitBtn = LuaGameObject.GetChildNoGC(self.transform, "SubmitBtn")
	UIEventListener.Get(submitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		for i = 1, 3 do
			LuaShenbingMgr.ShenbingQianyuanItemTable[i] = self.m_ItemInputTable[i]:GetValue()
		end
		CUIManager.CloseUI(CLuaUIResources.ShenBingSubmitWnd)
		g_ScriptEvent:BroadcastInLua("ShenbingQianyuanSumitSuccess")
	end)
	self.m_SubmitQnbtn = submitBtn.qnButton
	self.m_SubmitQnbtn.Enabled = false

	local itemRootTrans = LuaGameObject.GetChildNoGC(self.transform, "ItemRoot").transform
	for i = 1, 3 do
		local templateId = 0
		if i == 1 then templateId = ShenBing_Setting.GetData().Qy1ItemId
		elseif i == 2 then templateId = ShenBing_Setting.GetData().Qy2ItemId
		else templateId = ShenBing_Setting.GetData().Qy3ItemId end
		table.insert(self.m_ItemTemplateTable, templateId)
		local itemData = Item_Item.GetData(self.m_ItemTemplateTable[i])
		table.insert(itemNameTable, itemData.Name)

		local itemTrans = LuaGameObject.GetChildNoGC(self.transform, "Item"..i).transform
		local texture = LuaGameObject.GetChildNoGC(itemTrans, "Texture")
		UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
			local default = DefaultItemActionDataSource.Create(1, {function ( ... )
				CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
				CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_ItemTemplateTable[self.m_CurrentSelectedItem], false, self.m_ItemTextureTable[self.m_CurrentSelectedItem].transform, AlignType.Right)
			end}, {LocalString.GetString("获取")})
			for j = 1, 3 do
				if go == self.m_ItemTextureTable[j] then
					CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_ItemTemplateTable[j], false, default, AlignType2.Right, 0, 0, 0, 0)
					self.m_CurrentSelectedItem = j
					break
				end
			end
		end)
		table.insert(self.m_ItemTextureTable, texture.gameObject)
		texture.cTexture:LoadMaterial(itemData.Icon)
		table.insert(self.m_ItemOwnCountLabelTable, LuaGameObject.GetChildNoGC(itemTrans, "Count").label)
		self.m_ItemOwnCountLabelTable[i].text = ""
		LuaGameObject.GetChildNoGC(itemTrans, "Name").label.text = itemNameTable[i]
		local btn = LuaGameObject.GetChildNoGC(itemTrans, "Input").qnAddSubAndInputButton
		btn.onValueChanged = DelegateFactory.Action_uint(function ( value )
			self:RefreshCount()
		end)
		table.insert(self.m_ItemInputTable, btn)
		LuaGameObject.GetChildNoGC(itemTrans, "History").label.text = cs_string.Format(LocalString.GetString("历史提交 {0}"), CommonDefs.DictContains_LuaCall(equip.Equip.ConsumeItemCount, self.m_QianyuanTemplateIdTable[i]) and CommonDefs.DictGetValue_LuaCall(equip.Equip.ConsumeItemCount, self.m_QianyuanTemplateIdTable[i]) or 0)

		--Got
		local got = LuaGameObject.GetChildNoGC(itemTrans, "Got")
		UIEventListener.Get(got.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
			local default = DefaultItemActionDataSource.Create(1, {function ( ... )
				CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
				CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_ItemTemplateTable[self.m_CurrentSelectedItem], false, self.m_ItemTextureTable[self.m_CurrentSelectedItem].transform, AlignType.Right)
			end}, {LocalString.GetString("获取")})
			for j = 1, 3 do
				if go == self.m_GotObjTable[j] then
					CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_ItemTemplateTable[j], false, default, AlignType2.Right, 0, 0, 0, 0)
					self.m_CurrentSelectedItem = j
					break
				end
			end
		end)
		table.insert(self.m_GotObjTable, got.gameObject)

	end
	self.m_TotalCountLabel = LuaGameObject.GetChildNoGC(self.transform, "TotalCountLabel").label
	self:RefreshItem()
	for i = 1, 3 do
		self.m_ItemInputTable[i]:SetValue(0, true)
	end

	--local tipStr = String.Format(LocalString.GetString("需要前缘材料({0}+{1}+{2}总和)"), itemNameTable[1], itemNameTable[2], itemNameTable[3])
	--LuaGameObject.GetChildNoGC(self.transform, "TipLabel").label.text = tipStr
end

function CLuaShenbingSubmitWnd:RefreshItem( args )
	self.m_ItemOwnCountTable = {}
	for i = 1, 3 do
		local cnt = CItemMgr.Inst:GetItemCount(self.m_ItemTemplateTable[i])
		table.insert(self.m_ItemOwnCountTable, cnt)
		self.m_ItemOwnCountLabelTable[i].text = cnt
		self.m_ItemInputTable[i]:SetMinMax(0, cnt, 1)
		self.m_GotObjTable[i]:SetActive(cnt <= 0)
	end
end

function CLuaShenbingSubmitWnd:RefreshCount()
	local cnt = 0
	for i = 1, #self.m_ItemInputTable do
		local val = self.m_ItemInputTable[i]:GetValue()
		cnt = cnt + val
		--self.m_ItemInputTable[i]:OverrideText(val.."/"..self.m_ItemOwnCountTable[i])
		self.m_ProgressSliderTable[i].value = cnt / LuaShenbingMgr.ShenbingQianyuanItemTotalCnt
	end
	local needCnt = LuaShenbingMgr.ShenbingQianyuanItemTotalCnt - cnt
	for i = 1 , 3 do
		local maxVal = self.m_ItemInputTable[i]:GetValue() + needCnt
		self.m_ItemInputTable[i]:SetMinMax(0, maxVal < self.m_ItemOwnCountTable[i] and maxVal or self.m_ItemOwnCountTable[i], 1)
	end
	self.m_TotalCountLabel.text = cnt.."/"..LuaShenbingMgr.ShenbingQianyuanItemTotalCnt
	self.m_SubmitQnbtn.Enabled = (cnt >= LuaShenbingMgr.ShenbingQianyuanItemTotalCnt)
end

function CLuaShenbingSubmitWnd:OnEnable()
	g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "AddEquip")
	g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
end

function CLuaShenbingSubmitWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "AddEquip")
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
end

return CLuaShenbingSubmitWnd
