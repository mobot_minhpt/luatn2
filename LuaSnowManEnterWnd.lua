local UITabBar = import "L10.UI.UITabBar"
local Animation = import "UnityEngine.Animation"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTabView = import "L10.UI.QnTabView"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaSnowManEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowManEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaSnowManEnterWnd, "FormLabel", "FormLabel", UILabel)
RegistChildComponent(LuaSnowManEnterWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaSnowManEnterWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaSnowManEnterWnd, "CountDescLabel", "CountDescLabel", UILabel)
RegistChildComponent(LuaSnowManEnterWnd, "ItemCell1", "ItemCell1", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "ItemCell2", "ItemCell2", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "ItemCell3", "ItemCell3", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "ItemCell4", "ItemCell4", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "StartBtn", "StartBtn", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "TypeBtn", "TypeBtn", UITabBar)
RegistChildComponent(LuaSnowManEnterWnd, "ShopBtn", "ShopBtn", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "Normal_Bg", "Normal_Bg", GameObject)
RegistChildComponent(LuaSnowManEnterWnd, "Hard_Bg", "Hard_Bg", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaSnowManEnterWnd, "m_hard")
RegistClassMember(LuaSnowManEnterWnd, "m_anim")

function LuaSnowManEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    UIEventListener.Get(self.StartBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnStartBtnClick()
        end
    )

    UIEventListener.Get(self.RuleBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnRuleBtnClick()
        end
    )

    self.TypeBtn.OnTabChange =
        DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTypeBtnTabChange(index)
        end
    )

    UIEventListener.Get(self.ShopBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnShopBtnClick()
        end
    )

    --@endregion EventBind end

    self.m_anim = self.gameObject:GetComponent(typeof(Animation))
end

function LuaSnowManEnterWnd:Init()
    self.m_hard = 0

    local rewardCount = 0
    if CClientMainPlayer.Inst then
        local key = EnumPlayTimesKey_lua.eHanJia2024FurySnowman_RewardTimes
        rewardCount = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(key)
    end
    local setting = HanJia2024_Setting.GetData()
    self.TimeLabel.text = setting.SnowManWndTimeDes
    self.FormLabel.text = setting.SnowManWndTypeDes
    self.DescLabel.text = setting.SnowManWndTaskDes
    self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(setting.SnowManLevelLimit))
    self.CountDescLabel.text = g_MessageMgr:Format("SNOWMAN_REWARD_DESC",rewardCount,setting.SnowManRewardMaxCount)
    local itemctrls = {
        self.ItemCell1,
        self.ItemCell2,
        self.ItemCell3,
        self.ItemCell4
    }
    local rewards = setting.SnowManWndTaskRewards
    local ct = rewards and rewards.Length or 0
    for i = 1, #itemctrls do
        local index = i - 1
        if index < ct then
            itemctrls[i]:SetActive(true)
            local itemid = setting.SnowManWndTaskRewards[i]
            self:InitItem(itemctrls[i], itemid)
        else
            itemctrls[i]:SetActive(false)
        end
    end

    local maxhard = self:GetMaxHard()
    local shopenable = self:IsShopOpen()
    local lockctrl = FindChildWithType(self.ShopBtn.transform, "LockMask", typeof(GameObject))
    lockctrl:SetActive(not shopenable)

    if maxhard == 0 then
        self.m_hard = 0
    else
        self.m_hard = 1
    end
    self.TypeBtn:ChangeTab(self.m_hard)

    self.m_anim:Play("snowmanenterwnd_show")
end

function LuaSnowManEnterWnd:InitItem(ctrlgo, itemid)
    local trans = ctrlgo.transform
    local bound = FindChildWithType(trans, "", typeof(UISprite))
    local icon = FindChildWithType(trans, "", typeof(CUITexture))

    local cfg = Item_Item.GetData(itemid)
    bound.spriteName = CUICommonDef.GetItemCellBorder(cfg.Quality)
    icon:LoadMaterial(cfg.Icon)

    UIEventListener.get(ctrlgo).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    )
end

--[[
    @desc: 通过的最高难度
    author:Codee
    time:2023-10-23 17:14:18
    @return:0未通关 1通关普通 2通关困难
]]
function LuaSnowManEnterWnd:GetMaxHard()
    local maxhard = 0
    if CClientMainPlayer.Inst then
        local key = EnumPlayTimesKey_lua.eHanJia2024FurySnowman_PassStatus
        maxhard = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(key)
    end
    return maxhard
end

--[[
    @desc: 商店是否开放
    author:Codee
    time:2023-10-23 17:14:48
    @return:true/false
]]
function LuaSnowManEnterWnd:IsShopOpen()
    local maxhard = self:GetMaxHard()
    return maxhard >= 2 --通过困难模式以上
end

--@region UIEvent

function LuaSnowManEnterWnd:OnStartBtnClick()
    local ishardmode = self.m_hard == 1
    Gac2Gas.HanJia2024FurySnowman_RequestEnterPlay(ishardmode)
end

function LuaSnowManEnterWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("SNOWMAN_RULE")
end

function LuaSnowManEnterWnd:OnShopBtnClick()
    local setting = HanJia2024_Setting.GetData()
    local isshopopen = self:IsShopOpen()
    if isshopopen then
        Gac2Gas.OpenShop(setting.SnowManShopID)
    else
        g_MessageMgr:ShowMessage("SNOWMAN_NEED_HARD_COMPLETED")
    end
end

--[[
    @desc: 切换难度
    author:Codee
    time:2023-09-25 14:20:54
    --@index: 0简单，1困难
    @return:
]]
function LuaSnowManEnterWnd:OnTypeBtnTabChange(index)
    self.m_hard = index
    local aniname = index == 0 and "snowmanenterwnd_switch02" or "snowmanenterwnd_switch01"
    self.m_anim:Play(aniname)
    local color = index == 0 and Color(12 / 255, 34 / 255, 81 / 255, 1) or Color(178 / 255, 203 / 255, 255 / 255, 1)
    self.TimeLabel.color = color
    self.FormLabel.color = color
    self.LevelLabel.color = color
    self.DescLabel.color = color
end

--@endregion UIEvent
