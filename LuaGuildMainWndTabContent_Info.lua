local CGuildPreLeagueTaskPointMgr = import "L10.UI.CGuildPreLeagueTaskPointMgr"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTooltip = import "L10.UI.CTooltip"


CLuaGuildMainWndTabContent_Info = class()
RegistClassMember(CLuaGuildMainWndTabContent_Info,"nameLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"scaleSprite")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"idLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"masterLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"numLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"equipPointLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"goldLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"houseNumLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"academyNumLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"founderLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"memberNumLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"livenessLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"chargeLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"bankNumLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"shareLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"rankLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"mottoLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"buildBtn")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"histroyBtn")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"rankBtn")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"backBtn")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"renameBtn")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"changeMottoBtn")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"leagueScoreLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"leagueLevelLabel")
RegistClassMember(CLuaGuildMainWndTabContent_Info,"addLeagueScoreBtn")

function CLuaGuildMainWndTabContent_Info:Awake()
    self.nameLabel = self.transform:Find("Left/Top/NameLabel"):GetComponent(typeof(UILabel))
    self.scaleSprite = self.transform:Find("Left/Center/LeftLabel/ScaleSprite"):GetComponent(typeof(UISprite))


    self.idLabel = self.transform:Find("Left/Center/RightLabel/IdLabel"):GetComponent(typeof(UILabel))
    self.masterLabel = self.transform:Find("Left/Center/LeftLabel/MasterLabel"):GetComponent(typeof(UILabel))
    self.numLabel = self.transform:Find("Left/Center/LeftLabel/NumLabel"):GetComponent(typeof(UILabel))
    self.equipPointLabel = self.transform:Find("Left/Center/LeftLabel/EquipPointLabel"):GetComponent(typeof(UILabel))
    self.goldLabel = self.transform:Find("Left/Center/LeftLabel/GoldLabel"):GetComponent(typeof(UILabel))
    self.houseNumLabel = self.transform:Find("Left/Center/LeftLabel/HouseNumLabel"):GetComponent(typeof(UILabel))
    self.academyNumLabel = self.transform:Find("Left/Center/LeftLabel/AcademyNumLabel"):GetComponent(typeof(UILabel))
    self.founderLabel = self.transform:Find("Left/Center/RightLabel/FounderLabel"):GetComponent(typeof(UILabel))
    self.memberNumLabel = self.transform:Find("Left/Center/RightLabel/MemberNumLabel"):GetComponent(typeof(UILabel))
    self.livenessLabel = self.transform:Find("Left/Center/RightLabel/LivenessLabel"):GetComponent(typeof(UILabel))
    self.chargeLabel = self.transform:Find("Left/Center/RightLabel/ChargeLabel"):GetComponent(typeof(UILabel))
    self.bankNumLabel = self.transform:Find("Left/Center/RightLabel/BankNumLabel"):GetComponent(typeof(UILabel))
    self.shareLabel = self.transform:Find("Left/Center/RightLabel/ShareLabel"):GetComponent(typeof(UILabel))
    self.rankLabel = self.transform:Find("Left/Center/RightLabel/RankLabel"):GetComponent(typeof(UILabel))
    self.mottoLabel = self.transform:Find("Right/MottoLabel"):GetComponent(typeof(UILabel))
    self.buildBtn = self.transform:Find("Left/Bottom/BuildBtn").gameObject
    self.histroyBtn = self.transform:Find("Left/Bottom/HistoryBtn").gameObject
    self.rankBtn = self.transform:Find("Left/Bottom/RankBtn").gameObject
    self.backBtn = self.transform:Find("Right/BackBtn").gameObject
    self.renameBtn = self.transform:Find("Left/Top/RenameBtn").gameObject
    self.changeMottoBtn = self.transform:Find("Right/ChangeMottoBtn").gameObject
    self.leagueScoreLabel = self.transform:Find("Left/Center/RightLabel/LeagueScoreLabel"):GetComponent(typeof(UILabel))
    self.leagueLevelLabel = self.transform:Find("Left/Center/LeftLabel/LeagueLevelLabel"):GetComponent(typeof(UILabel))
    self.addLeagueScoreBtn = self.transform:Find("Left/Center/RightLabel/LeagueScoreLabel/AddLeagueScoreBtn").gameObject

    self.anDingDuLabel = self.transform:Find("Left/Center/LeftLabel/AnDingDuLabel"):GetComponent(typeof(UILabel))
    self.anDingDuLaeblBtn = self.transform:Find("Left/Center/Static/InfoLabel/AnDingDuBtn").gameObject
end

-- -- Auto Generated!!
-- function CLuaGuildMainWndTabContent_Info:Awake( )
--     -- self.transform.localPosition = Vector3.zero


-- end
function CLuaGuildMainWndTabContent_Info:OnEnable( )
    CGuildMgr.Inst:GetMyGuildInfo()
    g_ScriptEvent:AddListener("MyGuildInfoReceived", self, "OnMyGuildInfoReceived")
    g_ScriptEvent:AddListener("GuildBuildBonusReceived", self, "OnGuildBuildInfoReceive")
    -- EventManager.AddListener(EnumEventType.MyGuildInfoReceived, MakeDelegateFromCSFunction(self.OnMyGuildInfoReceived, Action0, self))
    -- EventManager.AddListenerInternal(EnumEventType.GuildInfoReceived, MakeDelegateFromCSFunction(self.OnGuildInfoReceived, MakeGenericClass(Action1, String), self))
    -- EventManager.AddListenerInternal(EnumEventType.GuildBuildBonusReceived, MakeDelegateFromCSFunction(self.OnGuildBuildInfoReceive, MakeGenericClass(Action1, Double), self))
end
function CLuaGuildMainWndTabContent_Info:OnDisable()
    g_ScriptEvent:RemoveListener("MyGuildInfoReceived", self, "OnMyGuildInfoReceived")
    g_ScriptEvent:RemoveListener("GuildBuildBonusReceived", self, "OnGuildBuildInfoReceive")

end
function CLuaGuildMainWndTabContent_Info:OnGuildBuildInfoReceive(v)
    self.shareLabel.text=tostring(v)
end

function CLuaGuildMainWndTabContent_Info:GetMaintainCost()
  local ret = 0
  if not (CGuildMgr.Inst.m_GuildInfo and CGuildMgr.Inst.m_GuildDynamicInfo) then
    return ret
  end

  local scale = CGuildMgr.Inst.m_GuildInfo.Scale
  local wingRoomNum = CGuildMgr.Inst.m_GuildInfo.WingRoomNum
  local silverStoreNum = CGuildMgr.Inst.m_GuildInfo.SilverStoreNum
  local sanctumNum = CGuildMgr.Inst.m_GuildInfo.SanctumNum

  local maintainCostScale = Guild_SizeLevelUpdate.GetData(scale).MaintainCost
  local maintainCostWingRoom = Guild_WingRoomUpdate.GetData(wingRoomNum + 1).MaintainCost
  local maintainCostSilverStore = Guild_SilverStoreUpdate.GetData(silverStoreNum + 1).MaintainCost
  local maintainCostSanctum = Guild_SanctumUpdate.GetData(sanctumNum + 1).MaintainCost
  ret = (maintainCostScale + maintainCostWingRoom + maintainCostSilverStore + maintainCostSanctum)

  local stability = CGuildMgr.Inst.m_GuildDynamicInfo.Stability
  local stabilityString = Guild_Setting.GetData().StabilityAddMaintainCost
  local stabilityTable = g_LuaUtil:StrSplit(stabilityString,";")
  local add = 0
  for i,v in ipairs(stabilityTable) do
    if v then
      local dataTable = g_LuaUtil:StrSplit(v,",")
      if dataTable and #dataTable == 2 then
        local num = tonumber(dataTable[1])
        local percent = tonumber(dataTable[2])
        if stability <= num then
          add = percent
          break
        end
      end
    end
  end

  ret = math.ceil(ret * (1 + add/100))
  return ret
end

-- function CLuaGuildMainWndTabContent_Info:OnGuildInfoReceived( rpcType)
-- end
function CLuaGuildMainWndTabContent_Info:OnMyGuildInfoReceived( )
    if CGuildMgr.Inst ~= nil and CClientMainPlayer.Inst ~= nil and CGuildMgr.Inst.m_GuildInfo ~= nil and CGuildMgr.Inst.m_GuildDynamicInfo ~= nil then
        local maxBuildingNumStr = tostring(CGuildMgr.Inst:GetMaxBuilding())

        self.nameLabel.text = CGuildMgr.Inst.m_GuildInfo.Name
        self.idLabel.text = tostring(CClientMainPlayer.Inst.BasicProp.GuildId)
        self.scaleSprite.spriteName = "guildwnd_grade_0"..tostring(CGuildMgr.Inst.m_GuildInfo.Scale)
        self.masterLabel.text = CGuildMgr.Inst.m_GuildDynamicInfo.LeaderName
        self.numLabel.text = (((tostring(CGuildMgr.Inst.m_GuildDynamicInfo.OnlineMemberNum) .. "/") .. tostring(CGuildMgr.Inst.m_GuildDynamicInfo.MemberNum)) .. "/") .. CGuildMgr.Inst:GetMaxSize()
        self.equipPointLabel.text = tostring(CGuildMgr.Inst.m_GuildDynamicInfo.AllMembersEquipScore)
        self.goldLabel.text = tostring(CGuildMgr.Inst.m_GuildInfo.Silver)
        self.houseNumLabel.text = (tostring(CGuildMgr.Inst.m_GuildInfo.WingRoomNum) .. "/") .. maxBuildingNumStr
        self.academyNumLabel.text = (tostring(CGuildMgr.Inst.m_GuildInfo.SanctumNum) .. "/") .. maxBuildingNumStr
        --prestigeLabel.text = "未实现"; // TODO zzm
        self.founderLabel.text = CGuildMgr.Inst.m_GuildDynamicInfo.FounderName
        self.memberNumLabel.text = (((tostring(CGuildMgr.Inst.m_GuildDynamicInfo.OnlineTraineeNum) .. "/") .. tostring(CGuildMgr.Inst.m_GuildDynamicInfo.TraineeNum)) .. "/") .. CGuildMgr.Inst:GetMaxSizeTrainee()
        --livenessLabel.text = CGuildMgr.Inst.m_GuildInfo.Activity.ToString();
        self.livenessLabel.text = tostring(CGuildMgr.Inst.m_GuildDynamicInfo.LastWeekActivity)
        --self.chargeLabel.text = CGuildMgr.Inst:GetMaintainCost() .. LocalString.GetString("/天")
        self.chargeLabel.text = self:GetMaintainCost() .. LocalString.GetString("/天")
        self.bankNumLabel.text = (tostring(CGuildMgr.Inst.m_GuildInfo.SilverStoreNum) .. "/") .. maxBuildingNumStr
        --       shareLabel.text = CGuildMgr.Inst.m_GuildInfo.Bonus.ToString();
        self.rankLabel.text = LocalString.GetString("未实现")
        -- TODO zzm
        self.mottoLabel.text = CGuildMgr.Inst.m_GuildInfo.Mission


        self.leagueScoreLabel.text = System.String.Format("{0}/{1}", CGuildPreLeagueTaskPointMgr.Inst.preLeagueTaskPoint, GuildLeague_Setting.GetData().BeiZhanPointGuildMax)
        --GetPreLeagueTaskTotalPoint
        self.leagueLevelLabel.text = LuaGuildLeagueMgr.GetPreLeagueDesc()

        self.anDingDuLabel.text = CGuildMgr.Inst.m_GuildDynamicInfo.Stability
    end
end
function CLuaGuildMainWndTabContent_Info:Start( )
    UIEventListener.Get(self.buildBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI(CUIResources.GuildMainWnd)
        CGuildMgr.Inst:ShowGuildBuildWnd(0)
    end)

    UIEventListener.Get(self.histroyBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CUIResources.GuildHistoryWnd)
    end)

    UIEventListener.Get(self.renameBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CUIResources.GuildRenameWnd)
    end)
    UIEventListener.Get(self.changeMottoBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CUIResources.GuildChangeMottoWnd)
    end)

    UIEventListener.Get(self.rankBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CUIResources.GuildRankWnd)
    end)
    UIEventListener.Get(self.backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CGuildMgr.Inst:GoBackGuild()
    end)

    UIEventListener.Get(self.addLeagueScoreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CUIResources.GetGuildPreLeagueTaskPointWnd)
    end)

    UIEventListener.Get(self.anDingDuLaeblBtn).onClick = DelegateFactory.VoidDelegate(function (p)
      --g_MessageMgr:ShowMessage('AnDingDuTip')
      CTooltip.Show(g_MessageMgr:FormatMessage('AnDingDuTip'), p.transform, CTooltip.AlignType.Top)
    end)
end
