local UIGrid=import "UIGrid"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"

CLuaAnQiDaoSignalWnd=class()
RegistClassMember(CLuaAnQiDaoSignalWnd,"m_ItemTemplate")
RegistClassMember(CLuaAnQiDaoSignalWnd,"m_Grid")


function CLuaAnQiDaoSignalWnd:Init()
    local worldPos=CLuaGuanNingMgr.m_ClickSignalWorldPos
    local localPos=self.transform:InverseTransformPoint(worldPos)
    LuaUtils.SetLocalPosition(FindChild(self.transform,"Anchor"),localPos.x-300+20,localPos.y-200,0)
    
    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)

    self.m_Grid=FindChild(self.transform,"Grid").gameObject

    AnQiIsland_QuickCommand.Foreach(function(k,v)
        local item = NGUITools.AddChild(self.m_Grid, self.m_ItemTemplate)
        item:SetActive(true)
        FindChild(item.transform,"Content"):GetComponent(typeof(UILabel)).text=v.Content
        
        UIEventListener.Get(item).onClick=LuaUtils.VoidDelegate(function(go)
            self:OnClick(go)
        end)
    end)
    self.m_Grid:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaAnQiDaoSignalWnd:OnClick(go)
    local cdtime=6
    if CServerTimeMgr.Inst.timeStamp - CLuaGuanNingMgr.m_LastSendSignalTime < cdtime then
        local delta = cdtime - math.floor(CServerTimeMgr.Inst.timeStamp - CLuaGuanNingMgr.m_LastSendSignalTime)
        g_MessageMgr:ShowMessage("PVPCommand_CDTime",delta)
        CUIManager.CloseUI(CLuaUIResources.AnQiDaoSignalWnd)
        return
    end
    CLuaGuanNingMgr.m_LastSendSignalTime=CServerTimeMgr.Inst.timeStamp

    local index=go.transform:GetSiblingIndex()
    CUIManager.CloseUI(CLuaUIResources.AnQiDaoSignalWnd)
    Gac2Gas.SendPVPCommand(index+1)
end