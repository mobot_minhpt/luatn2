-- Auto Generated!!
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CftfTApplyList = import "L10.UI.CftfTApplyList"
local CftfTApplyListTpl = import "L10.UI.CftfTApplyListTpl"
local CIMMgr = import "L10.Game.CIMMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTradeApplyInfo = import "L10.Game.CTradeApplyInfo"
local CTradeMgr = import "L10.Game.CTradeMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local DelegateFactory = import "DelegateFactory"
local EnumTradeSellType = import "L10.Game.EnumTradeSellType"
local Extensions = import "Extensions"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CftfTApplyList.m_Init_CS2LuaHook = function (this)
    this.applyList = CTradeMgr.Inst.tradeApplyList
    CommonDefs.ListClear(this.applyInfoList)
    CommonDefs.ListClear(this.lists)
    if this.scrollView == nil or this.applyListTpl == nil or this.applyList == nil then
        return
    end
    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.DictIterate(this.applyList, DelegateFactory.Action_object_object(function (___key, ___value)
        local player = {}
        player.Key = ___key
        player.Value = ___value
        local continue
        repeat
            local target = TypeAs(CClientObjectMgr.Inst:GetObject(player.Value.engineId), typeof(CClientOtherPlayer))
            if target == nil then
                continue = true
                break
            end
            local applyUnit = NGUITools.AddChild(this.grid.gameObject, this.applyListTpl)
            applyUnit:SetActive(true)
            local isFriend = CIMMgr.Inst:IsMyFriend(player.Key)
            local default
            if isFriend then
                default = LocalString.GetString("好友")
            else
                default = LocalString.GetString("陌生人")
            end
            local applyInfo = Table2ArrayWithCount({target.Name, System.String.Format("lv.{0}", target.Level), L10.Game.Profession.GetFullName(target.Class), default}, 4, MakeArrayClass(System.String))
            CommonDefs.ListAdd(this.applyInfoList, typeof(MakeArrayClass(String)), applyInfo)

            local title = ""
            local color = Color.cyan
            if player.Value:IsTrade() then
                local info = TypeAs(player.Value, typeof(CTradeApplyInfo))
                local extern
                if info.type == EnumTradeSellType.Precious then
                    extern = LocalString.GetString("出售")
                else
                    extern = LocalString.GetString("购买")
                end
                title = extern
                color = info.type == EnumTradeSellType.Precious and Color.cyan or Color.yellow
            else
                title = LocalString.GetString("赠送")
            end

            CommonDefs.GetComponent_GameObject_Type(applyUnit, typeof(CftfTApplyListTpl)):Init(applyInfo, player.Key, player.Value.engineId, isFriend, title, color)
            CommonDefs.ListAdd(this.lists, typeof(QnSelectableButton), CommonDefs.GetComponent_GameObject_Type(applyUnit, typeof(QnSelectableButton)))
            UIEventListener.Get(applyUnit).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(applyUnit).onClick, MakeDelegateFromCSFunction(this.OnBtnSelected, VoidDelegate, this), true)
            continue = true
        until 1
        if not continue then
            return
        end
    end))
    this.grid:Reposition()
    CommonDefs.GetComponent_GameObject_Type(this.scrollView, typeof(CUIRestrictScrollView)):ResetPosition()
end
CftfTApplyList.m_OnBtnSelected_CS2LuaHook = function (this, go)
    do
        local i = 0
        while i < this.lists.Count do
            if go ~= this.lists[i].gameObject then
                this.lists[i]:SetSelected(false, false)
            end
            i = i + 1
        end
    end
end
CftfTApplyList.m_AcceptApply_CS2LuaHook = function (this)
    local hasSelected = false
    do
        local i = 0
        while i < this.lists.Count do
            if this.lists[i]:isSeleted() then
                --CTradeMgr.Inst.targetPlayerId = lists[i].GetComponent<CftfTApplyListTpl>().playerId;
                CTradeMgr.Inst.targetEngineId = CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).engingId
                CTradeMgr.Inst.targetInfo = this.applyInfoList[i]

                if CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId):IsTrade() then
                    local info = TypeAs(CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId), typeof(CTradeApplyInfo))

                    local type = info.type
                    if type == EnumTradeSellType.Precious then
                        Gac2Gas.ConfirmStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId, EnumTradeSellType_lua.Silver)
                    else
                        Gac2Gas.ConfirmStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId, EnumTradeSellType_lua.Precious)
                    end
                else
                    Gac2Gas.ConfirmStartGift(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
                end

                hasSelected = true
                break
            end
            i = i + 1
        end
    end

    if hasSelected then
        do
            local i = 0
            while i < this.lists.Count do
                if not this.lists[i]:isSeleted() then
                  if CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId):IsTrade() then
                    Gac2Gas.RefuseStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
                  else
                    Gac2Gas.RefuseStartGift(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
                  end
                end
                i = i + 1
            end
        end
        CUIManager.CloseUI(CUIResources.ftfTransactionListWnd)
    else
        if this.lists.Count == 1 then
            if CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId):IsTrade() then
                local info = TypeAs(CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId), typeof(CTradeApplyInfo))
                local type = info.type
                if type == EnumTradeSellType.Precious then
                    Gac2Gas.ConfirmStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId, EnumTradeSellType_lua.Silver)
                else
                    Gac2Gas.ConfirmStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId, EnumTradeSellType_lua.Precious)
                end
            else
                Gac2Gas.ConfirmStartGift(CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId)
            end

            --CTradeMgr.Inst.targetPlayerId = lists[0].GetComponent<CftfTApplyListTpl>().playerId;
            CTradeMgr.Inst.targetEngineId = CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).engingId
            CTradeMgr.Inst.targetInfo = this.applyInfoList[0]
            CUIManager.CloseUI(CUIResources.ftfTransactionListWnd)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个交易对象"))
        end
    end
end
CftfTApplyList.m_RejectApply_CS2LuaHook = function (this)
    do
        local i = 0
        while i < this.lists.Count do
            if this.lists[i]:isSeleted() then
              if CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId):IsTrade() then
                Gac2Gas.RefuseStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
              else
                Gac2Gas.RefuseStartGift(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
              end
                CommonDefs.DictRemove(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
                CommonDefs.ListRemove(this.lists, typeof(QnSelectableButton), this.lists[i])
                if this.lists.Count == 0 then
                    CUIManager.CloseUI(CUIResources.ftfTransactionListWnd)
                else
                    this:Init()
                end
                return
            end
            i = i + 1
        end
    end
    if this.lists.Count == 1 then
      if CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId):IsTrade() then
        Gac2Gas.RefuseStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId)
      else
        Gac2Gas.RefuseStartGift(CommonDefs.GetComponent_Component_Type(this.lists[0], typeof(CftfTApplyListTpl)).playerId)
      end
      CUIManager.CloseUI(CUIResources.ftfTransactionListWnd)
      CommonDefs.ListClear(this.lists)
      CommonDefs.DictClear(CTradeMgr.Inst.tradeApplyList)
    else
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个交易对象"))
    end
end
CftfTApplyList.m_RejectAllApply_CS2LuaHook = function (this)
    do
        local i = 0
        while i < this.lists.Count do
            if CommonDefs.DictGetValue(CTradeMgr.Inst.tradeApplyList, typeof(UInt64), CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId):IsTrade() then
              Gac2Gas.RefuseStartTrade(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
            else
              Gac2Gas.RefuseStartGift(CommonDefs.GetComponent_Component_Type(this.lists[i], typeof(CftfTApplyListTpl)).playerId)
            end
            i = i + 1
        end
    end
    CTradeMgr.Inst.tradeApplyList:Clear()
    --this:Init()
    CUIManager.CloseUI(CUIResources.ftfTransactionListWnd)
end
