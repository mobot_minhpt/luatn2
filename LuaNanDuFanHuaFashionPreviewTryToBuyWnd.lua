local QnButton = import "L10.UI.QnButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local String = import "System.String"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local QnLabel = import "L10.UI.QnLabel"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CUITexture = import "L10.UI.CUITexture"
local IdPartition = import "L10.Game.IdPartition"
local CItem = import "L10.Game.CItem"
local CEquipment = import "L10.Game.CEquipment"
local CItemMgr = import "L10.Game.CItemMgr"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIGrid = import "UIGrid"
local UIScrollView = import "UIScrollView"
local QnCheckBox = import "L10.UI.QnCheckBox"
local Collider = import "UnityEngine.Collider"
local NGUIMath = import "NGUIMath"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local UITexture = import "UITexture"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaNanDuFanHuaFashionPreviewTryToBuyWnd = class()
RegistChildComponent(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"qnCostAndOwnMoney",  CCurentMoneyCtrl)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"buyBtn", QnButton)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"buyArea", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"templateItem", GameObject)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"disableSprite", UISprite)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"grid", UIGrid)
RegistChildComponent(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"scrollView", UIScrollView)

RegistClassMember(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"curSelectableSequence")
RegistClassMember(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"curCostMoney")
RegistClassMember(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"isShowOneItem")
RegistClassMember(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"bg")
RegistClassMember(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"m_OwnedShopItems")
RegistClassMember(LuaNanDuFanHuaFashionPreviewTryToBuyWnd,"m_ConfirmMessageList")

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:Init()
    self.fashionData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.fashionData = data[5]
        end
    end
    
    self.bg = self.transform:Find("Wnd_Bg_Secondary_2"):GetComponent(typeof(UITexture))

    self.curSelectableSequence = {}
    local validNum = 1
    for k, v in pairs( LuaShopMallFashionPreviewMgr.selectableSequence) do
        local isOwned = false
        local cfgId = v.ID
        if v.FashionId then
            --时装类
            isOwned = bit.band(self.fashionData[4] and self.fashionData[4] or 0, bit.lshift(1, cfgId - 1)) > 0
        elseif v.ZuoQiId then
            isOwned = bit.band(self.fashionData[5] and self.fashionData[5] or 0, bit.lshift(1, cfgId - 1)) > 0
        end
        if not isOwned then
            self.curSelectableSequence[validNum] = v
            validNum = validNum + 1
        end
    end

    local count = 0
    for itemID,shopMallTemlate in pairs(self.curSelectableSequence) do
        if shopMallTemlate.Price > 0 then
            count = count + 1
        end
    end

    self:InitShopRoot()

    self.isShowOneItem = count == 1

    if self.isShowOneItem then
        self.scrollView.gameObject:SetActive(false)
        self.templateItem:SetActive(true)
    end

    self:UpdateCostMoney()
    self.buyBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnBuyButtonClick()
    end)

end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:OnUpdatePlayProp()
    EventManager.Broadcast(EnumEventType.MainPlayerUpdateMoney)
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:UpdateCostMoney()
    self.curCostMoney = 0
    local useBindJade = false
    for itemID, shopMallTemlate in pairs(self.curSelectableSequence) do
        self.curCostMoney = self.curCostMoney + shopMallTemlate.Price
    end
    self.qnCostAndOwnMoney:SetType(EnumMoneyType.NanDuTongBao)
    self.qnCostAndOwnMoney:SetCost(self.curCostMoney)

    self.buyBtn:GetComponent(typeof(Collider)).enabled = self.curCostMoney ~= 0
    self.disableSprite.enabled = self.curCostMoney == 0
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:InitShopRoot()
    Extensions.RemoveAllChildren(self.grid.transform)
    for index,shopMallTemlate in ipairs(self.curSelectableSequence) do
        local obj = NGUITools.AddChild(self.grid.gameObject, self.templateItem)
        self:InitShopItem(obj, shopMallTemlate)

        if index == 1 then
            self:InitShopItem(self.templateItem, shopMallTemlate)
        end
    end

    self.templateItem:SetActive(false)
    self.grid:Reposition()
    self.scrollView:ResetPosition()

    if #self.curSelectableSequence > 2 then
        self:UpdateWndHeight()
    end
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:UpdateWndHeight()
    --local top = math.abs(self.scrollView.panel.topAnchor.absolute)
    --local bottom = math.abs(self.scrollView.panel.bottomAnchor.absolute)
    --local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.grid.transform)
    --local totalHeight = top + bottom + bounds.size.y
    --local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    --local virtualScreenHeight = Screen.height * scale
    --totalHeight = (totalHeight > virtualScreenHeight) and virtualScreenHeight or totalHeight
    --self.bg.height = totalHeight
    --self.scrollView.panel:ResetAndUpdateAnchors()
    --self.scrollView:ResetPosition()
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:InitShopItem(itemGo, data)
    local templateId = data.ItemId
    --local isOwned = self.m_OwnedShopItems[templateId]
    local isOwned = false
    local cfgId = data.ID
    if data.FashionId then
        --时装类
        isOwned = bit.band(self.fashionData[4] and self.fashionData[4] or 0, bit.lshift(1, cfgId - 1)) > 0
    elseif data.ZuoQiId then
        isOwned = bit.band(self.fashionData[5] and self.fashionData[5] or 0, bit.lshift(1, cfgId - 1)) > 0
    end
    
    local discountLabel = itemGo.transform:Find("Discount"):GetComponent(typeof(QnLabel))
    discountLabel.gameObject:SetActive(false)
    
    local btn = itemGo:GetComponent(typeof(QnSelectableButton))
    btn:SetSelected(true)
    --btn.OnButtonSelected = DelegateFactory.Action_bool(function (isSelected)
    --    self:OnItemSelected(btn, data, isSelected)
    --end)

    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    local texture = itemGo.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    texture:LoadMaterial(item.Icon)

    itemGo.transform:Find("Name"):GetComponent(typeof(QnLabel)).Text = item.Name
    itemGo.transform:Find("Price"):GetComponent(typeof(QnLabel)).Text = tostring(data.Price)

    local disableSprite = itemGo.transform:Find("Texture/DisableSprite"):GetComponent(typeof(UISprite))
    disableSprite.enabled = false
    --if IdPartition.IdIsItem(templateId) then
    --    disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
    --elseif IdPartition.IdIsEquip(templateId) then
    --    disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, true)
    --end

    itemGo.transform:Find("Owner").gameObject:SetActive(isOwned)

    itemGo:SetActive(true)
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:OnItemSelected(btn, data, isSelected)
    --self.curSelectableSequence[data.ItemId] = isSelected and data or nil
    --
    --btn.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox)):SetSelected(isSelected, true)
    --self:UpdateCostMoney()
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:OnBuyButtonClick()
    if self.curCostMoney <= 0 then
        return
    end

    if self.qnCostAndOwnMoney:GetOwn() >= self.curCostMoney then
        self.m_ConfirmMessageList = {}
        for ItemID,item in pairs(self.curSelectableSequence) do
            if not System.String.IsNullOrEmpty(item.ConfirmMessage) then
                table.insert(self.m_ConfirmMessageList, item.ConfirmMessage)
            end
        end
        self:ConfirmBuyMallItem()
    else
        CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaExchangeMoneyWnd)
    end
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:ConfirmBuyMallItem()
    if self.m_ConfirmMessageList and #self.m_ConfirmMessageList > 0 then
        local message = self.m_ConfirmMessageList[1]
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(message), DelegateFactory.Action(function ()
            table.remove(self.m_ConfirmMessageList, 1)
            self:ConfirmBuyMallItem()
        end), nil, nil, nil, false)
    else
        self:BuyAllMallItems()
    end
end

function LuaNanDuFanHuaFashionPreviewTryToBuyWnd:BuyAllMallItems()
    local realBuyFunc = function(_isZuoQi, cfgId)
        if _isZuoQi then
            Gac2Gas.UnlockNanDuLordZuoQi(cfgId)
        else
            Gac2Gas.UnlockNanDuLordFashion(cfgId)
        end
    end
    
    local sendNum = 0
    for k, v in pairs( self.curSelectableSequence) do
        local cfgId = v.ID
        local isZuoqi = false
        if v.FashionId then
            --时装类
            isZuoqi = false
        elseif v.ZuoQiId then
            isZuoqi = true
        end
        RegisterTickOnce(function()
            realBuyFunc(isZuoqi, cfgId)
        end,sendNum*500+1)
        sendNum = sendNum + 1
    end
end
