local CChatLinkActions=import "CChatLinkActions"
local CChatHelper = import "L10.UI.CChatHelper"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType3 = import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CJoinCCChanelContext = import "L10.Game.CJoinCCChanelContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CJoinCCChannelRequestSourceInfo = import "L10.Game.CJoinCCChannelRequestSourceInfo"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CPayMgr = import "L10.Game.CPayMgr"
local Charge_Charge = import "L10.Game.Charge_Charge"
local MessageWndManager=import "L10.UI.MessageWndManager"
local TaskLocation = import "L10.Game.TaskLocation"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CScene = import "L10.Game.CScene"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local StringStringKeyValuePair = import "L10.Game.StringStringKeyValuePair"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local Utility = import "L10.Engine.Utility"
local CPos = import "L10.Engine.CPos"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CChargeMgr = import "L10.Game.CChargeMgr"
local Object=import "System.Object"
local CIMMgr = import "L10.Game.CIMMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CHouseUIMgr = import "L10.UI.CHouseUIMgr"
local EHouseMainWndTab = import "L10.UI.EHouseMainWndTab"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local Constants = import "L10.Game.Constants"

g_ChatLinkActionCallback={}
CChatLinkActions.m_FallbackAction_Lua = function (funcName, args, source, env)
	--严格的link参数之前不允许有空格出现，但是目前斗魂坛有一处程序特殊处理，为了兼容，这里对方法进行空白内容trim操作
	funcName = StringTrim(funcName) -- svn revision 373961 OpenDouHunTeam
	if g_ChatLinkActionCallback[funcName] then
		g_ChatLinkActionCallback[funcName](funcName, args)
	end
end

g_ChatLinkActionCallback["BabyClick"] = function(funcName, args)--养育宝宝链接 by shengfang refs #109055
	local babyId,babyName,playerId,playerName = args[0],args[1],args[2],args[3]
	CLuaOtherBabyInfoWnd.m_PlayerId = playerId
	CLuaOtherBabyInfoWnd.m_BabyId = babyId
	CUIManager.ShowUI(CLuaUIResources.OtherBabyInfoWnd)
end

g_ChatLinkActionCallback["LingShouClick"] = function(funcName, args)--灵兽相关 by shegnfang svn revision 363481
	local lingshouId,lingshouName,playerId,playerName = args[0],args[1],args[2],args[3]

	CLuaLingShouOtherMgr.lingShouId = lingshouId
	CLuaLingShouOtherMgr.playerId = playerId
	CLuaLingShouOtherMgr.playerName = playerName
	CLuaLingShouOtherMgr.requestType = LuaEnumOtherLingShouRequestType.SingleLingShou
	CLuaLingShouOtherMgr.ClearLingShouData()
	Gac2Gas.QueryAllPlayerLingShou(playerId, 0)--主要是为了请求结伴信息
	Gac2Gas.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, CLuaLingShouOtherMgr.lingShouId, 0)
end

g_ChatLinkActionCallback["LingShouBabyClick"] = function(funcName, args)--灵兽相关 by shegnfang svn revision 363481
	local lingshouId,lingshouName,playerId,playerName = args[0],args[1],args[2],args[3]

	CLuaLingShouOtherMgr.lingShouId = lingshouId
	CLuaLingShouOtherMgr.playerId = playerId
	CLuaLingShouOtherMgr.playerName = playerName
	CLuaLingShouOtherMgr.requestType = LuaEnumOtherLingShouRequestType.LingShouBaby
	CLuaLingShouOtherMgr.ClearLingShouData()
	Gac2Gas.QueryLingShouInfo(CLuaLingShouOtherMgr.playerId, CLuaLingShouOtherMgr.lingShouId, 0)
end

g_ChatLinkActionCallback["PreExistenceShareClick"] = function(funcName, args)--2019情人节 by luozhaoxiang refs #112128
	local infoString, playerId, playerName = args[0], tonumber(args[1]), args[2]
	LuaValentine2019Mgr.ShowPreExistenceMarridgeAlbumWnd(false,infoString, playerId, playerName)
end

g_ChatLinkActionCallback["PreExistenceDetailShareClick"] = function(funcName, args)--2019情人节 by luozhaoxiang refs #112128
	local generationNum, templateID, playerId, playerName = tonumber(args[0]), tonumber(args[1]), tonumber(args[2]), args[3]
	LuaValentine2019Mgr.ShowPreExistenceMarridgeDetailWnd(false, generationNum, templateID, playerId, playerName)
end

g_ChatLinkActionCallback["PSHotTalk"] = function(funcName, args)--梦岛热门话题 by zhangqing refs #111276
	--funcName对应LuaPersonalSpaceMgrReal.HotTalkBtnName
	if CUIManager.IsLoaded(CLuaUIResources.PersonalSpaceHotTalkWnd) then
		return
	end
	local topId = tonumber(args[0])
	LuaPersonalSpaceMgrReal.ShowInfoTopicDefaultId = topId
	CUIManager.ShowUI(CLuaUIResources.PersonalSpaceHotTalkWnd)
end

g_ChatLinkActionCallback["WishDetail"] = function(funcName, args)--
	--funcName对应LuaPersonalSpaceMgrReal.HotTalkBtnName
	local wishId = args[0]
  --LuaPersonalSpaceMgrReal.GetDetailWishInfo(wishId)
	LuaPersonalSpaceMgrReal.GetSingleDetailWishInfo(wishId)
end

g_ChatLinkActionCallback["HuiLiuBindWnd"] = function(funcName, args)--
	local arg1 = tonumber(args[0])
	local arg2 = args[1]
	Gac2Gas.RequestAcceptBindWithInvitee(arg1,arg2)
end

g_ChatLinkActionCallback["BabyPlantAndHarvestClick"] = function(funcName, args)--宝宝种菜收菜 by zhoujiaqi refs #114894
	Gac2Gas.RequestQuickPlantAndHarvest()
end

g_ChatLinkActionCallback["OpenActivityUrl"] = function(funcName, args)--倩女名人堂活动 by zhaolei refs #114567
	local activityName = args[0]
	if activityName=="JiaYuan2022Index" or string.find(activityName,"JiaYuan2022Index@") then
		CLuaWebBrowserMgr.OpenHouseActivityUrl(activityName)
		return
	end
	CWebBrowserMgr.Inst:OpenActivityUrl(activityName)
end

g_ChatLinkActionCallback["PlayRemoteVideo"] = function(funcName, args)--精灵短视频 by fengkaixuan refs #116941
	LuaJingLingMgr.PlayRemoteVideo(args[0])
end

g_ChatLinkActionCallback["JingLingVoiceClick"] = function(funcName, args)--精灵语音 by fengkaixuan refs #115075
	LuaVoiceMgr.PlayJingLingVoice(args[0])-- <link button=精灵语音,JingLingVoiceClick,voiceName>
end

g_ChatLinkActionCallback["VoiceAchievement"] = function(funcName, args)--语音成就
	LuaVoiceAchievementMgr.OpenAchievementWnd(args[0])-- <link button=成就名称,VoiceAchievement,vachiId>
end

g_ChatLinkActionCallback["FengXianInivite"] = function(funcName, args)--封仙邀请 by liuyujia refs #116863
	local playerId, playId = tonumber(args[0]), tonumber(args[1])
	CLuaXianzhiMgr.OpenFengXianInvitationCard(playerId, playId)
end

g_ChatLinkActionCallback["OpenDouHunTeam"] = function(funcName, args)--查看斗魂坛战队 by liuyujia refs #117010
	local teamId = StringTrim(args[0])
	Gac2Gas.QueryDouHunChlgTeamInfoById(teamId)
end

g_ChatLinkActionCallback["OpenSDKJingLing"] = function(funcName, args)--打开精灵SDK by fengkaixuan refs 119734
	LuaJingLingMgr.OpenSDKJingLing(args[0]) --<link button=显示内容,OpenSDKJingLing,问题>
end

g_ChatLinkActionCallback["StarBiwuViewZhandui"] = function(funcName, args)--跨服明星赛 by daijianwen refs #119835
	local zhanduiId = tonumber(args[0])
    CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, zhanduiId, 1)
end

g_ChatLinkActionCallback["StarBiwuDetailInfoWnd"] = function(funcName, args)
	if args and args.Length >= 1 then
    	CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(tonumber(args[0]))
	else
		CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(0)
	end
end

g_ChatLinkActionCallback["StarBiwuZhanDuiPlatformWnd"] = function(funcName, args)
	if args and args.Length >= 1 then
    	CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(tonumber(args[0]))
	else
		CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(0)
	end
end

g_ChatLinkActionCallback["ShowQYCode"] = function(funcName, args)--迁移码 by zhaolei1 refs #123524
	Gac2Gas.GetAccountQYCode()
end

g_ChatLinkActionCallback["LingShouZhaoQin"] = function(funcName, args)--灵兽招亲 by shengfang refs #131311
	local playerId,lingshouId,lingshouName,templateId = tonumber(args[0]),args[1],args[2],tonumber(args[3])

	if CClientMainPlayer.Inst then
		if CClientMainPlayer.Inst.Id == playerId then
			g_MessageMgr:ShowMessage("LingShou_ZhaoQin_Not_Self")
			return
		end
	end
	CLuaLingShouMgr.ZhaoQinPlayerId = playerId
	CLuaLingShouMgr.ZhaoQinLingShouId = lingshouId
	CLuaLingShouMgr.ZhaoQinLingShouTemplateId = templateId
	CLuaLingShouMgr.ZhaoQinLingShouName = lingshouName
	CUIManager.ShowUI(CUIResources.LingShouMarriageMatchWnd)
end

g_ChatLinkActionCallback["AcceptLingShouEngageRequest"] = function(funcName, args)--接受灵兽求亲 by shengfang refs #131311
	local requesterId,requestLingShouId,playerId,lingshouId = tonumber(args[0]),args[1],tonumber(args[2]),args[3]
	Gac2Gas.AcceptLingShouEngageRequest(lingshouId, requesterId, requestLingShouId)
end

g_ChatLinkActionCallback["ApplyJoinRemoteDouDiZhu"] = function(funcName, args)-- 申请加入远程斗地主 by yangkai1 refs #133387
	local containerId, gasId, serverId = args[0], tonumber(args[1]), tonumber(args[2])
	Gac2Gas.ApplyJoinRemoteDouDiZhu(containerId, gasId, serverId)
end

g_ChatLinkActionCallback["ItemGet"] = function(funcName, args)-- 显示道具获取途径 by tongtiexin refs #135168
	local itemId = args[0]
	CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, true, nil, AlignType.Right)
end

g_ChatLinkActionCallback["ItemInfo"] = function(funcName, args)-- 显示template道具信息 by tongtiexin refs #154083
	local templateId = args[0]
	CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
end

g_ChatLinkActionCallback["GoHomeAndOpenQishuWnd"] = function(funcName, args)-- 气数装饰物折叠消息
	if not CClientHouseMgr.Inst:IsInOwnHouse() then
        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("在自己家中才能进行操作，是否现在回家？"), DelegateFactory.Action(function () 
            Gac2Gas.RequestEnterHome()
        end), nil, nil, nil, false)
	else
		CHouseUIMgr.OpenHouseMainWnd(EHouseMainWndTab.EQishuTab)
    end
end
g_ChatLinkActionCallback["QiShuFoldExpand"] = function(funcName, args)-- 气数装饰物折叠消息 <link button=展开/收起,QiShuFoldExpand>
	local uuid = tonumber(args[0])
	local isExpand = tonumber(args[1])
	local imMsg = CIMMgr.Inst:GetIMMsgByUUID(uuid)
	g_ScriptEvent:BroadcastInLua("QiShuFoldExpand",uuid,isExpand==1)
end

g_ChatLinkActionCallback["JoinCCChannel"] = function(funcName, args) -- name,ccid[,type,option,idFlag,extraUrl] 打开cc直播    <link button=观看直播,JoinCCChannel,zhangsan,101,1,1,0>
	if not args or args.Length<2 then
		return
	end
	local name = tostring(args[0])
	local id = tonumber(args[1])
	if not name or not id then return end
	local context = (args.Length>2 and (tostring(args[2]) == "1") and CJoinCCChanelContext.Video) or CJoinCCChanelContext.Audio
	local option = (args.Length>3 and tonumber(args[3])) or 0
	-- idFlag 取值 0 ccid为主播id 1 ccid为频道id
	local idFlag = (args.Length>4 and tonumber(args[4])) or 0
	if idFlag ~= 0 and idFlag ~= 1 then
		idFlag = 0
	end
	-- extraUrl 自定义的地址，当需要打开网页时，如果配置了此字段，则使用该字段作为url
	local extraUrl = (args.Length>5 and tostring(args[5])) or nil
	-- option 取值 1 内置直播 2 桌面版网页，移动端内置 3 桌面版网页，官方移动端内置，非官方移动端网页 4 全部网页
	local bOpenUrl = CLuaActivityZhiBoMgr.ShouldOpenUrl(option)
	if bOpenUrl then
		CCCChatMgr.Inst:AddWinWebCCLog(idFlag == 1 and id or 0, idFlag == 0 and id or 0, "from_link")
		if extraUrl~=nil then
			CWebBrowserMgr.Inst:OpenUrl(extraUrl)
			return
		elseif idFlag == 0 then
			CWebBrowserMgr.Inst:OpenUrl(SafeStringFormat3("https://cc.163.com/%d/",id))
			return
		elseif idFlag == 1 then
			CWebBrowserMgr.Inst:OpenUrl(SafeStringFormat3("https://cc.163.com/17/%d/",id))
			return
		end
	end

	local channel = EChatPanel.Undefined
	if CChatLinkActions.EnvContains("chatchannel") then
		channel = CommonDefs.ConvertIntToEnum(typeof(EChatPanel), tonumber(CChatLinkActions.EnvGetValue("chatchannel")))
	end

	local sourceInfo = ""
	if channel ~= EChatPanel.Undefined then
		sourceInfo = (channel == EChatPanel.System and CJoinCCChannelRequestSourceInfo.SystemChannel) or CJoinCCChannelRequestSourceInfo.PlayerChatInChannelOrIM
	end

	if CChatLinkActions.EnvContains("context") then
		local value = tostring(CChatLinkActions.EnvGetValue("context"))
		if value ==  CJoinCCChannelRequestSourceInfo.Mail then
			sourceInfo = CJoinCCChannelRequestSourceInfo.Mail
		elseif value == CJoinCCChannelRequestSourceInfo.Announcement then
			sourceInfo = CJoinCCChannelRequestSourceInfo.Announcement
		end
	end

	if idFlag == 0 then
		CCCChatMgr.Inst:JoinCCChannnelViaLink(name, id, context, sourceInfo)
	elseif idFlag == 1 then
		CCCChatMgr.Inst:JoinCCChannelByCId(id, context, sourceInfo)
	end

end

g_ChatLinkActionCallback["SelectPlayerAndShowMenu"] = function(funcName, args) -- playerId, playerName
	if not args or args.Length<2 then
		return
	end

	local playerId = tonumber(args[0])
	local playerName = tostring(args[1])
	--这里复用一下玩家链接的逻辑，避免重复处理context信息
	CChatLinkActions.PlayerClick(playerId, playerName)
	--尝试选中目标，不一定成功
	local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
	if player then
		CClientMainPlayer.Inst.Target = player
	end
end

g_ChatLinkActionCallback["EmenySectTransport"] = function(funcName, args) -- sectId 请求传送到敌对宗派
	if not args or args.Length<1 then
		return
	end

	local sectId = tonumber(args[0])
	if sectId then
		Gac2Gas.RequestTrackEnemySect(sectId)
	end
end

g_ChatLinkActionCallback["SectTransport"] = function(funcName, args) -- sectId 请求传送到宗派
	if not args or args.Length<1 then
		return
	end

	local sectId = tonumber(args[0])
	if sectId and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
		Gac2Gas.RequestTrackToSectEntrance()
	elseif sectId then
		Gac2Gas.RequestTrackEnemySect(sectId)
	end
end

g_ChatLinkActionCallback["LookUpGuildWish"] = function(funcName, args) -- 查看某人分享到帮会的圣诞祈愿
	if not args or args.Length<2 then
		return
	end

	local guildId = tonumber(args[0])
	local cardId = tonumber(args[1])
	Gac2Gas.QueryOtherChristmasCardInGuild(guildId,cardId)
end

g_ChatLinkActionCallback["AskSomeoneHelpLiftUp"] = function(funcName, args) -- 魅香楼醉酒 求助信息
	if not args or args.Length<3 then
		return
	end

	local sceneTemplateId = tonumber(args[0])--MeiXiangLou_Setting.GetData().MeiXiangLouSceneId
	local posx = tonumber(args[1])
	local posy = tonumber(args[2])
	CTrackMgr.Inst:FindLocation(nil, sceneTemplateId, posx, posy, nil, nil, nil, 0)
end

g_ChatLinkActionCallback["TrackToSect"] = function(funcName, args)
	if not args or args.Length<6 then
		return
	end

	local sceneTemplateId = tonumber(args[0])
	local sceneName = tostring(args[1])
	local posX = tonumber(args[2])
	local posY = tonumber(args[3])
	local sceneId = tostring(args[4])
	local sectId = tonumber(args[5])
	local loc = CreateFromClass(TaskLocation)
	loc.taskId = 0
	loc.trackInfo = ""
	loc.eventName = "TrackToSect"
	loc.sceneTemplateId = sceneTemplateId
	loc.targetX = posX
	loc.targetY = posY
	loc.destEvent = 0
	loc.conditionType = 0

	CTaskMgr.Inst.m_TrackingLocation = loc
	CTrackMgr.Inst:SyncSectTaskDestSceneId(loc.taskId, sectId, "", sceneId)
    if CScene.MainScene and sceneId ~= CScene.MainScene.SceneId then
        if sectId and CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
            Gac2Gas.RequestTrackToSectEntrance()
        else
            g_MessageMgr:ShowMessage("ROUTE_UNREACHABLE")
        end
    end
end

g_ChatLinkActionCallback["FindUnArrivDirectlyNpc"] = function(funcName, args)
	local targetNpcId = tonumber(args[0])
	local targetSceneTemplateId = tonumber(args[2])
	local targetPosx = tonumber(args[4])
	local targetPosy = tonumber(args[5])

	local connectNpcId = tonumber(args[6])
	local connectSceneTemplateId = tonumber(args[8])
	local connectPosx = tonumber(args[10])
	local connectPosy = tonumber(args[11])
	--如果当前就在目标场景
	if CScene.MainScene and CScene.MainScene.SceneTemplateId == targetSceneTemplateId then
		local pos = CPos(targetPosx, targetPosy)
        pos = Utility.GridPos2PixelPos(pos)
		CTrackMgr.Inst:Track(pos, Utility.Grid2Pixel(2.0), nil, nil)
	else
		--先去中转站
		CTrackMgr.Inst:FindNPC(connectNpcId, connectSceneTemplateId, 0, 0, nil, DelegateFactory.Action(function ()
			CTrackMgr.Inst:FindNPC(targetNpcId, targetSceneTemplateId, 0, 0, nil, nil)
		end))
	end
end

g_ChatLinkActionCallback["BigAmountPay"] = function(funcName, args)
	if not args or args.Length<1 then
		return
	end

	local pid = tostring(args[0])
	local isValid = false
	for i = 0, CPayMgr.ChargeItems.Count - 1 do
		local id = CPayMgr.ChargeItems[i]
		local data = Charge_Charge.GetData(id)
		if data and data.PID == pid then
			isValid = true
			break
		end
	end
	if not isValid then return end

	CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(pid), 0) -- <link button=大额支付,BigAmountPay,pid>
end


g_ChatLinkActionCallback["JoinZongMen"] = function(funcName, args) -- <link button=点击加入,JoinZongMen,zongMenId,%d>
	if not args or args.Length < 2 then
		return
	end

	local zongMenId = tonumber(args[0])
	local sendLinkPlayerId = tonumber(args[1])
	--Gac2Gas.QueryToJoinSectInfo(zongMenId)
	-- 采用 QuerySectName 代替,返回 SyncSectName
	if CClientMainPlayer.Inst then
		if zongMenId == CClientMainPlayer.Inst.BasicProp.SectId then 
			g_MessageMgr:ShowMessage("HasZongMen_Cant_Join_ZongMen")
			return 
		end
		LuaZongMenMgr.m_JoinZongMenSendLinkPlayerId = sendLinkPlayerId
		LuaZongMenMgr.m_JoinZongMenBIM = true
		Gac2Gas.QuerySectName(zongMenId)
	end
end

g_ChatLinkActionCallback["JoinZongMenChannel"] = function(funcName, args) -- <link button=点击加入,JoinZongMenChannel,zongMenId,%d>
	if not args or args.Length < 2 then
		return
	end

	local zongMenId = tonumber(args[0])
	local sendLinkPlayerId = tonumber(args[1])
	--Gac2Gas.QueryToJoinSectInfo(zongMenId)
	-- 采用 QuerySectName 代替,返回 SyncSectName
	if CClientMainPlayer.Inst then
		if zongMenId == CClientMainPlayer.Inst.BasicProp.SectId then 
			g_MessageMgr:ShowMessage("HasZongMen_Cant_Join_ZongMen")
			return 
		end
		LuaZongMenMgr.m_JoinZongMenSendLinkPlayerId = sendLinkPlayerId
		LuaZongMenMgr.m_JoinZongMenBIM = false
		Gac2Gas.QuerySectName(zongMenId)
	end
end

g_ChatLinkActionCallback["EnterSect"] = function(funcName, args) -- <link button=点击进入%s,EnterSect,%s>]
	if not args or args.Length < 1 then
		return
	end

	local enemySectId = tonumber(args[0])
	if CClientMainPlayer.Inst then
		if enemySectId == CClientMainPlayer.Inst.BasicProp.SectId then 
			Gac2Gas.RequestEnterSelfSectScene(enemySectId)
			return 
		end

		Gac2Gas.RequestTrackEnemySect(enemySectId)
	end
end

g_ChatLinkActionCallback["FuXiDanceShare"] = function(funcName, args) -- <link button=点击查看预览,FuXiDanceShare,%s>
	if not args or args.Length < 1 then
		return
	end

	LuaFuxiAniMgr.OpenFromShare(tonumber(args[0]))
end

g_ChatLinkActionCallback["ChatRoomShare"] = function(funcName, args) -- <link button=xxx,ChatRoomShare,roomId,roomName>]
	if not args or args.Length < 2 then
		return
	end

	local roomId = tonumber(args[0])
	local roomName = tostring(args[1])
	local password = tostring(args[2] or "")
	local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_ENTER_ROOM_CONFIRM", roomName)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		LuaClubHouseMgr:EnterRoom(roomId, roomName, password, true, true)
	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

g_ChatLinkActionCallback["ApplySectLeader"] = function(funcName, args) -- <link button=点击申请掌门人,ApplySectLeader>]
	if not args or args.Length < 1 then
		return
	end

	local zongMenId = tonumber(args[0])

	if CClientMainPlayer.Inst then
		if zongMenId ~= CClientMainPlayer.Inst.BasicProp.SectId then 
			g_MessageMgr:ShowMessage("SET_SECT_NOT_SELF_SECT")
			return
		end
		Gac2Gas.RequestApplySectLeader(zongMenId)
	end
end

g_ChatLinkActionCallback["LinyuShoppingMall"] = function(funcName, args) 
	if not args or args.Length < 1 then
		return
	end
	local templateId = tonumber(args[0])
	CShopMallMgr.ShowLinyuShoppingMall(templateId)
end

g_ChatLinkActionCallback["ShowFriendWnd"] = function(funcName, args) 
	if not args or args.Length < 2 then
		return
	end
	local ownerId = tonumber(args[0])
	local ownerName = args[1]
	CChatHelper.ShowFriendWnd(ownerId, ownerName)
end

g_ChatLinkActionCallback["ShowFriendWndAndSendMsg"] = function(funcName, args) 
	if not args or args.Length < 2 then
		return
	end
	local ownerId = tonumber(args[0])
	local ownerName = args[1]
	local msg = args[2]
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == ownerId then
		g_MessageMgr:ShowMessage("CHAT_SELF_CANT_SEND_MSG")
		return
	end
	CChatHelper.ShowFriendWnd(ownerId, ownerName)
	CChatHelper.SendMsg(EChatPanel.Friend, g_MessageMgr:FormatMessage(msg), ownerId)
end

g_ChatLinkActionCallback["ContactSectManager"] = function(funcName, args) 
	if not args or args.Length < 1 then
		return
	end
	local sectId = tonumber(args[0])
	Gac2Gas.RequestContactSectManager(sectId)
end

g_ChatLinkActionCallback["JoinYanHuaPlay"] = function(funcName, args)
	if not args or args.Length < 1 then
		return
	end
	local playId = tonumber(args[0])
	local msg = g_MessageMgr:FormatMessage("2021QiXi_Firework_Invited")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestJoinYanHuaPlayInvited(playId)
	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)

end

g_ChatLinkActionCallback["YaoGuaiInfo"] = function(funcName, args)
	if not args or args.Length < 4 then return end
	local tujianId = tonumber(args[0])
	local quality  = tonumber(args[1])
	local catchTime = args[2]
	local level = tonumber(args[3])
	local tujiandata = ZhuoYao_TuJian.GetData(tujianId)
	if not tujiandata then return end
	local labels = {}
	table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("评级"), tostring(tujiandata.Grade)))
	table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("怪物品质"), tostring(quality)))
	table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("捕获时间"), catchTime))
	local data = {TemplateId = tujianId, Quality = quality, Level = level}
	LuaZhuoYaoMgr:ShowTip(data, labels)
end

g_ChatLinkActionCallback["OpenHMXSUrl"] = function(funcName, args)--盒马鲜生
	if not args or args.Length < 1 then return end
	local url
	if args[0] == "HeMa2021Index" then
		url = "https://qnm.163.com/2021/hmxs/mh/"
	elseif args[0] == "HeMa2021Prize" then
		url = "https://qnm.163.com/2021/hmxs/mh//#/prize/"
	elseif args[0] == "HeMa2021Rank" then
		url = "https://qnm.163.com/2021/hmxs/mh//#/rank/"
	elseif args[0] == "HeMa2021Personal" then
		url = "https://qnm.163.com/2021/hmxs/mh//#/personal/"
	end
	if url then
		CWebBrowserMgr.Inst:OpenUrl(url)
	end
end

g_ChatLinkActionCallback["OpenHongBaoWnd"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	local hongbaotype = tonumber(args[0])
	CHongBaoMgr.ShowHongBaoWnd(CommonDefs.ConvertIntToEnum(typeof(EnumHongbaoType), hongbaotype))
end

g_ChatLinkActionCallback["OnRequestOpenNewInvitation"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	LuaWeddingIterationMgr:OpenInvitationCard(args)
end

g_ChatLinkActionCallback["PlotDiscussionPlayerClick"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	local id = tonumber(args[0])
	if not CClientMainPlayer.IsPlayerId(id) then
		CPlayerInfoMgr.ShowPlayerPopupMenu(id, EnumPlayerInfoContext.PlotDiscussion, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType3.Default)
	end
end

g_ChatLinkActionCallback["ShowXinShengHuajiPaintSetWnd"] = function(funcName, args)
	CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiPaintSetWnd)
end

g_ChatLinkActionCallback["OpenPlayerShopBuyView"] = function(funcName, args)
	if not args or args.Length < 2 then return end
	CYuanbaoMarketMgr.TabViewIndex = 1
	CYuanbaoMarketMgr.PlayerShopBuySectionIdx = args[0]
    CYuanbaoMarketMgr.PlayerShopBuyRowIdx = args[1]
	CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
end

g_ChatLinkActionCallback["RequestTerritoryWarMakeAlliance"] = function(funcName, args)
	if not args or args.Length < 2 then return end
	local guildName = args[0]
	local guild = tonumber(args[1])
	local msg = g_MessageMgr:FormatMessage("TerritoryWarMakeAlliance_Confirm",guildName)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarMakeAlliance(guild)
	end),DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarBreakAlliance(guild)
	end),LocalString.GetString("确认"),LocalString.GetString("拒绝"),false)
end

g_ChatLinkActionCallback["GuildExternalAidWnd"] = function(funcName, args)
	LuaGuildExternalAidMgr:ShowGuildExternalAidWnd()
end

g_ChatLinkActionCallback["GuildExternalAidCardWnd"] = function(funcName, args)
	LuaGuildExternalAidMgr:ShowGuildExternalAidCardWnd()
end


g_ChatLinkActionCallback["RequestTerritoryWarTeleport"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	local gridId = tonumber(args[0])
	Gac2Gas.RequestTerritoryWarTeleport(gridId, 0)
end

g_ChatLinkActionCallback["GuildTerritorialWarsOccupyWnd"] = function(funcName, args)
	Gac2Gas.RequestOpenTerritoryWarPickBornRegionWnd()
end

g_ChatLinkActionCallback["HuaYiGongShangShare"] = function(funcName, args)
	LuaWuMenHuaShiMgr:OpenSharedHuaYiGongShangWnd(args)
end

g_ChatLinkActionCallback["Guide2023WndShare"] = function(funcName, args)
	LuaGuide2023Wnd.m_firstOpenThirdMenuIndex = tonumber(args[0])
	CUIManager.ShowUI(CLuaUIResources.Guide2023Wnd)
end

g_ChatLinkActionCallback["H5ChangeChannel"] = function(funcName, args)
	Gac2Gas.GetTicketForChangeChannel()
end

g_ChatLinkActionCallback["GuildTerritorialWarsJieMengWnd"] = function(funcName, args)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsJieMengWnd)
end

g_ChatLinkActionCallback["GuildTerritorialWarsMapWnd"] = function(funcName, args)
	Gac2Gas.RequestTerritoryWarMapOverview()
end

g_ChatLinkActionCallback["QiXi2022PlantTreesWnd"] = function(funcName, args)
	Gac2Gas.YYS_RequestOpenTab()
	CUIManager.ShowUI(CLuaUIResources.QiXi2022PlantTreesWnd)
end

g_ChatLinkActionCallback["ChuShiGiftWnd"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	local tudiID = tonumber(args[0])
	Gac2Gas.RequestChuShiGiftInfo(tudiID)
end

g_ChatLinkActionCallback["OpenShenYaoSetting"] = function(funcName, args)
	Gac2Gas.ShenYao_QueryRejectLevel()
end

g_ChatLinkActionCallback["OpenShiTuGiftGivingWnd"] = function(funcName, args)
	LuaShiTuMgr:OpenShiTuGiftGivingWnd(CShiTuMgr.Inst:GetCurrentShiFuId(), true)
end

g_ChatLinkActionCallback["GuildTerritorialWarsRankWnd"] = function(wnd,data)
	Gac2Gas.RequsetTerritoryWarRankInfo(1)
end

g_ChatLinkActionCallback["JuDian"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	local mapId = tonumber(args[0])
	Gac2Gas.GuildJuDianTeleportToTagScene(mapId)
end
--<link button=邀请,InviteToDoShiTuTask,shifuId,taskId>
g_ChatLinkActionCallback["InviteToDoShiTuTask"] = function(funcName, args)
	if not args or args.Length < 2 then return end
	local shifuId = tonumber(args[0])
	local taskId = tonumber(args[1])
	LuaShiTuMgr:HandleInviteToDoShiTuTask(shifuId, taskId)
end

g_ChatLinkActionCallback["CopyToClipboard"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	CUICommonDef.clipboardText = args[0]
	if CLoginMgr.Inst:IsInGame() then
		g_MessageMgr:ShowMessage("Copy_Success_Tips")
	else
		local msg = g_MessageMgr:FormatMessage("Copy_Success_Tips1")
		MessageWndManager.ShowOKMessage(msg, nil)
	end
end

g_ChatLinkActionCallback["ShanYaoJieYuanShare"] = function(funcName, args)
	LuaXinBaiLianDongMgr:OpenShanYaoJieYuanShareWnd(args)
end

g_ChatLinkActionCallback["FindNPC"] = function(funcName, args)
	if not args or args.Length < 4 then return end
	local npcTemplateId = tonumber(args[0])
	local sceneTemplateId = tonumber(args[1])
	local posX = tonumber(args[2])
	local posY = tonumber(args[3])
	CTrackMgr.Inst:FindNPC(npcTemplateId, sceneTemplateId, posX, posY, nil, nil)
end
--<link button=加入,JoinShiTuWuZiQi,%d,%s,%d>
g_ChatLinkActionCallback["JoinShiTuWuZiQi"] = function(funcName, args)
	if not args or args.Length < 3 then return end
	local ownerId = tonumber(args[0])
	local playerName = tostring(args[1])
	local playId = tonumber(args[2])
	LuaShiTuMgr:AcceptInviteJoinWuZiQi(playId, ownerId)
end
--<link button=加入,WatchShiTuWuZiQi,%d,%s,%d>
g_ChatLinkActionCallback["WatchShiTuWuZiQi"] = function(funcName, args)
	if not args or args.Length < 3 then return end
	local ownerId = tonumber(args[0])
	local playerName = tostring(args[1])
	local playId = tonumber(args[2])
	LuaShiTuMgr:AcceptWatchWuZiQiInvite(playId, ownerId)
end

g_ChatLinkActionCallback["ShowHwMuTouRenEnterWnd"] = function(funcName, args)
	local isInHalloweenTime = LuaHalloween2022Mgr.isInHalloweenHongBaoCoverTime()
	local isInTime = LuaHalloween2022Mgr.IsInTrafficLightPlayTime()
	local timeStr = Halloween2022_Setting.GetData().TrafficPlayTimeString
	if isInTime and isInHalloweenTime then
		CUIManager.ShowUI(CLuaUIResources.HwMuTouRenEnterWnd)
	else
		local mesg = g_MessageMgr:FormatMessage("TRAFFICLIGHT_OPENTIME_NOTICE",timeStr)
        g_MessageMgr:ShowCustomMsg(mesg)
	end
end

g_ChatLinkActionCallback["Hallow2022HongBaoCoverWnd"] = function(funcName, args)
	local isInTime = LuaHalloween2022Mgr.isInHalloweenHongBaoCoverTime()
	if isInTime then 
		CUIManager.ShowUI(CLuaUIResources.HalloweenHongBaoCoverWnd)
	else
		g_MessageMgr:ShowCustomMsg(g_MessageMgr:FormatMessage("HALLOWEEN2022_HongBaoCover_AFTERAUTOSEND"))
	end
end

g_ChatLinkActionCallback["ShowGuildLeagueCrossMatchDetailInfoWnd"] = function(funcName, args)
	LuaGuildLeagueCrossMgr:ShowServerMatchDetailInfoWnd(tonumber(args[0]))	-- <link button=点击查看,ShowGuildLeagueCrossMatchDetailInfoWnd,matchIndex>
end

g_ChatLinkActionCallback["ShowGuildLeagueCrossGambleWnd"] = function(funcName, args)
	LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossGambleInfo()	-- <link button=打开竞猜界面,ShowGuildLeagueCrossGambleWnd>
end

g_ChatLinkActionCallback["ShowChargeWnd_BuyMonthCardForSelf"] = function(funcName, args)
	CChargeMgr.EnableBuyMonthCardForFriend = false
	CShopMallMgr.ShowChargeWnd()
	if args and args.Length > 0 then 
		local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
		CommonDefs.DictAdd(dict, typeof(String), "key", typeof(Object), args[0])
		Gac2Gas.RequestRecordClientLog("ShowChargeWnd_BuyMonthCardForSelf", MsgPackImpl.pack(dict))
	end
end

g_ChatLinkActionCallback["StarBiwuReShenBattleStatusWnd"] = function(funcName, args)
	CUIManager.ShowUI(CLuaUIResources.StarBiwuReShenBattleStatusWnd)
end

g_ChatLinkActionCallback["ZongMenMainWndApplicationList"] = function(funcName, args)
	if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.BasicProp.SectId == 0 then
		g_MessageMgr:ShowMessage("Link_TimeOut")
        return
    end
	LuaZongMenMgr.m_MainWndTabIndex = 1
	LuaZongMenMgr.m_ZongMenMemberViewTabIndex = 1
	CUIManager.ShowUI(CLuaUIResources.ZongMenMainWnd)
end

g_ChatLinkActionCallback["OpenWnd"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	local windowName = args[0]
	if CLuaUIResources[windowName] then
		CUIManager.ShowUI(CLuaUIResources[windowName])
	end
end

g_ChatLinkActionCallback["DetailPicShowWnd"] = function(funcName, args)
	if not args or args.Length < 1 then return end
	local pid = args[0]
	local width = args[1]
	local height = args[2]
	local PicData = import "L10.Game.PicData"
	local LoadPicMgr = import "L10.Game.LoadPicMgr"
	local pdata = CreateFromClass(PicData)
	pdata.small = false
	pdata.width = width
	pdata.height = height
	pdata.pid = pid
	LoadPicMgr.Inst.SaveLoadPicData = pdata
	CUIManager.ShowUI(CUIResources.DetailPicShowWnd)
end

g_ChatLinkActionCallback["ZhouNianQingLifeFriendWnd"] = function(funcName, args)
	if CServerTimeMgr.Inst.timeStamp < CServerTimeMgr.Inst:GetTimeStampByStr(ZhouNianQing2023_PSZJSetting.GetData().PlayCloseTime) then
		CUIManager.ShowUI(CLuaUIResources.ZhouNianQingLifeFriendWnd)
	end
end

g_ChatLinkActionCallback["GuildPuzzleWnd"] = function(funcName, args)
	if args and args.Length > 0 then
		CLuaGuildMgr.m_PinTuNeedOpenId = tonumber(args[0])
	end

	if CScene.MainScene and CScene.MainScene.SceneId == CClientMainPlayer.Inst.GuildCityId then
		Gac2Gas.QueryGuildPinTuData()
	else
		CTrackMgr.Inst:Track("", Constants.GuildSceneId, CPos(2144,4192), 0, DelegateFactory.Action(function()
			Gac2Gas.QueryGuildPinTuData()
		end), nil, nil, nil, false)
	end
end

g_ChatLinkActionCallback["ClubHousePlayer"] = function(funcName, args) -- <link button=xxx,ClubHousePlayer,PlayerId,AccId,AppName>] 茶室的PlayerPopupMenu
	if args and args.Length == 3 then
		local playerId = tonumber(args[0])
		local accId = tonumber(args[1])
		local appName = args[2]
		local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
		CommonDefs.DictAdd_LuaCall(extraInfo, "AccId", accId)
		CommonDefs.DictAdd_LuaCall(extraInfo, "AppName", appName)
		if not CClientMainPlayer.IsPlayerId(playerId) then
			CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.ChatRoom, EChatPanel.Undefined, nil, nil, extraInfo, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType3.Default)
		end
	end
end

g_ChatLinkActionCallback["FashionLotteryWnd"] = function(funcName, args)
	local viewName = args and args.Length > 0 and args[0] or ""
	LuaFashionLotteryMgr:OpenFashionLotteryWnd(viewName)
end