-- Auto Generated!!
local CAppStoreReviewBox = import "L10.UI.CAppStoreReviewBox"
local CClientFormula = import "L10.Game.CClientFormula"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouProp = import "L10.Game.CLingShouProp"
local CLingShouPropDisplay = import "L10.UI.CLingShouPropDisplay"
local CLingShouWashCost = import "L10.UI.CLingShouWashCost"
local CLingShouWashResultWnd = import "L10.UI.CLingShouWashResultWnd"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local ELingShouFightProp = import "L10.Game.ELingShouFightProp"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumLingShouType = import "L10.Game.EnumLingShouType"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local EventManager = import "EventManager"
local GameObject = import "UnityEngine.GameObject"
local LingShouDetails = import "L10.Game.CLingShouBaseMgr+LingShouDetails"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
CLingShouWashResultWnd.m_Init_CS2LuaHook = function (this) 
    this.mSelectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if System.String.IsNullOrEmpty(this.mSelectedLingShou) then
        return
    end
    --throw new NotImplementedException();
    local details = CLingShouMgr.Inst:GetLingShouDetails(this.mSelectedLingShou)
    this.display1:Init(details)
    this:SetOldValues(details)
    this:HideArrows()

    Gac2Gas.RequestLastLingShouWashResult(this.mSelectedLingShou)
end
CLingShouWashResultWnd.m_SetOldValues_CS2LuaHook = function (this, details) 
    this.display2.numCompareDisplays[0]:SetOldValue(details.fightProperty:GetParam(EPlayerFightProp.CorZizhi))
    this.display2.numCompareDisplays[1]:SetOldValue(details.fightProperty:GetParam(EPlayerFightProp.StaZizhi))
    this.display2.numCompareDisplays[2]:SetOldValue(details.fightProperty:GetParam(EPlayerFightProp.StrZizhi))
    this.display2.numCompareDisplays[3]:SetOldValue(details.fightProperty:GetParam(EPlayerFightProp.IntZizhi))
    this.display2.numCompareDisplays[4]:SetOldValue(details.fightProperty:GetParam(EPlayerFightProp.AgiZizhi))
    this.display2.numCompareDisplays[5]:SetOldValue(details.chengzhang)
end
CLingShouWashResultWnd.m_HideArrows_CS2LuaHook = function (this) 
    this.display2.numCompareDisplays[0]:HideArrow()
    this.display2.numCompareDisplays[1]:HideArrow()
    this.display2.numCompareDisplays[2]:HideArrow()
    this.display2.numCompareDisplays[3]:HideArrow()
    this.display2.numCompareDisplays[4]:HideArrow()
    this.display2.numCompareDisplays[5]:HideArrow()
end
CLingShouWashResultWnd.m_SetNewValues_CS2LuaHook = function (this, prop, fightProp) 
    this.display2.numCompareDisplays[0]:SetNewValue(fightProp:GetParam(CommonDefs.EnumToEnum(ELingShouFightProp.CorZizhi, typeof(EPlayerFightProp))))
    this.display2.numCompareDisplays[1]:SetNewValue(fightProp:GetParam(CommonDefs.EnumToEnum(ELingShouFightProp.StaZizhi, typeof(EPlayerFightProp))))
    this.display2.numCompareDisplays[2]:SetNewValue(fightProp:GetParam(CommonDefs.EnumToEnum(ELingShouFightProp.StrZizhi, typeof(EPlayerFightProp))))
    this.display2.numCompareDisplays[3]:SetNewValue(fightProp:GetParam(CommonDefs.EnumToEnum(ELingShouFightProp.IntZizhi, typeof(EPlayerFightProp))))
    this.display2.numCompareDisplays[4]:SetNewValue(fightProp:GetParam(CommonDefs.EnumToEnum(ELingShouFightProp.AgiZizhi, typeof(EPlayerFightProp))))
    this.display2.numCompareDisplays[5]:SetNewValue(prop.Chengzhang)
end
CLingShouWashResultWnd.m_InitGameObject_CS2LuaHook = function (this) 
    local go = TypeAs(CommonDefs.Object_Instantiate(this.display1.gameObject), typeof(GameObject))
    go.transform.parent = this.display1.transform.parent
    go.transform.localScale = Vector3.one
    local displayY = this.display1.transform.localPosition.y
    go.transform.localPosition = Vector3(- 154, displayY)
    this.display2 = CommonDefs.GetComponent_GameObject_Type(go, typeof(CLingShouPropDisplay))
    CommonDefs.GetComponent_Component_Type(this.display2.transform:Find("Title/TitleLabel"), typeof(UILabel)).text = LocalString.GetString("新属性")
    this.display2:Clear()
end
CLingShouWashResultWnd.m_Awake_CS2LuaHook = function (this) 
    this:InitGameObject()

    CUICommonDef.SetActive(this.updateBtn, false, true)
    --////////////////////////////////////////////////////////////////////////

    --recommendSprite.enabled = false;
    this.Recommend = false
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.hasResult then
            local msg = g_MessageMgr:FormatMessage("Close_XiLingShou")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                this:Close()
            end), nil, nil, nil, false)
        else
            this:Close()
        end
    end)
    UIEventListener.Get(this.updateBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --选择新模板
        Gac2Gas.AcceptWashedLingShou(this.mSelectedLingShou)
    end)

    UIEventListener.Get(this.continueBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.Recommend then
            if not this.waitConfirm then
                this.waitConfirm = true
                local content = g_MessageMgr:FormatMessage("LINGSHOU_XIBAOBAO_CONFIRM")
                MessageWndManager.ShowOKCancelMessage(content, DelegateFactory.Action(function () 
                    this.waitConfirm = false
                    this:RequestWash()
                end), DelegateFactory.Action(function () 
                    this.waitConfirm = false
                end), nil, nil, false)
            end
        else
            if not this.waitConfirm then
                this.waitConfirm = true
                this:RequestWash()
            end
        end
    end)
end
CLingShouWashResultWnd.m_RequestWash_CS2LuaHook = function (this) 
    --继续洗宝宝
    --继续洗宝宝
    local pos
    local itemId
    local default
    default, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdWithNotExpired(EnumItemPlace.Bag, CLingShouMgr.Inst.washTemplateId)
    local find = default
    if find then
        local extern
        if itemId ~= nil then
            extern = itemId
        else
            extern = ""
        end
        Gac2Gas.RequestWashLingShou(this.mSelectedLingShou, EnumItemPlace_lua.Bag, pos, extern, false)
        this.waitConfirm = true
        return
    else
        if not CLingShouWashCost.autoBuy then
            g_MessageMgr:ShowMessage("LINGSHOU_NOT_HAVE_WASH_ITEM")
            this.waitConfirm = false
            return
        else
            --自动购买
            --看看灵玉够不够
            if this.washCost.jadeEnough then
                local ref
                if itemId ~= nil then
                    ref = itemId
                else
                    ref = ""
                end
                Gac2Gas.RequestWashLingShou(this.mSelectedLingShou, EnumItemPlace_lua.Bag, pos, ref, CLingShouWashCost.autoBuy)
                this.waitConfirm = true
            else
                CGetMoneyMgr.Inst:GetLingYu(this.washCost.LingYuCost, this.washCost.TemplateId)
                this.waitConfirm = false
            end
        end
    end
end
CLingShouWashResultWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.LingShouWashResult, MakeDelegateFromCSFunction(this.OnLingShouWashResult, MakeGenericClass(Action2, String, CLingShouProp), this))
    EventManager.AddListenerInternal(EnumEventType.GetLingShouDetails, MakeDelegateFromCSFunction(this.UpdateDetails, MakeGenericClass(Action2, String, LingShouDetails), this))

    EventManager.AddListenerInternal(EnumEventType.AcceptWashedLingShou, MakeDelegateFromCSFunction(this.AcceptWashedLingShou, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.RequestLastLingShouWashNoResult, MakeDelegateFromCSFunction(this.OnRequestLastLingShouWashNoResult, MakeGenericClass(Action1, String), this))

    EventManager.AddListenerInternal(EnumEventType.StopAutoWashLingShou, MakeDelegateFromCSFunction(this.OnStopWashLingShou, MakeGenericClass(Action1, String), this))
end
CLingShouWashResultWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.LingShouWashResult, MakeDelegateFromCSFunction(this.OnLingShouWashResult, MakeGenericClass(Action2, String, CLingShouProp), this))
    EventManager.RemoveListenerInternal(EnumEventType.GetLingShouDetails, MakeDelegateFromCSFunction(this.UpdateDetails, MakeGenericClass(Action2, String, LingShouDetails), this))
    EventManager.RemoveListenerInternal(EnumEventType.AcceptWashedLingShou, MakeDelegateFromCSFunction(this.AcceptWashedLingShou, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.RequestLastLingShouWashNoResult, MakeDelegateFromCSFunction(this.OnRequestLastLingShouWashNoResult, MakeGenericClass(Action1, String), this))

    EventManager.RemoveListenerInternal(EnumEventType.StopAutoWashLingShou, MakeDelegateFromCSFunction(this.OnStopWashLingShou, MakeGenericClass(Action1, String), this))
end
CLingShouWashResultWnd.m_AcceptWashedLingShou_CS2LuaHook = function (this, lingshouId) 
    this.hasResult = false

    this.hasResult = false

    if this.mSelectedLingShou == lingshouId then
        CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("替换成功"), 1)
        this:ClearRightPane()
        CUICommonDef.SetActive(this.updateBtn, false, true)

        this:HideArrows()
    end
end
CLingShouWashResultWnd.m_UpdateDetails_CS2LuaHook = function (this, lingshouId, details) 
    if this.mSelectedLingShou == lingshouId then
        this.display1:Init(details)
        this:SetOldValues(details)
    end
end
CLingShouWashResultWnd.m_ClearRightPane_CS2LuaHook = function (this) 
    --替换成功之后就不需要显示推荐图标
    --替换成功之后就不需要显示推荐图标
    --this.recommendSprite.enabled = false
    this.recommendSprite.gameObject:SetActive(false)

    this.display2:Clear()
end
CLingShouWashResultWnd.m_OnLingShouWashResult_CS2LuaHook = function (this, lingshouId, newProp) 
    this.hasResult = true

    CUICommonDef.SetActive(this.updateBtn, true, true)
    this.waitConfirm = false

    if this.pauseToggle.value then
        --非本初停留一秒
        --if (CLingShouMgr.GetEvolveGradeDesc(newProp.EvolveGrade) == "二变")
        if newProp.EvolveGrade >= 3 then
            this:StartCoroutine(this:HideButton())
        end
    end

    local fightProp = CLingShouMgr.Inst:GetLingShouFightPropByLingShouProp(lingshouId, newProp)
    if fightProp == nil then
        return
    end
    this.display2:Init(newProp, fightProp)
    this.display2:InitPropDif(CLingShouMgr.Inst:GetLingShouDetails(lingshouId), fightProp)
    this:SetNewValues(newProp, fightProp)


    local pinzhi = EnumToInt(CLingShouBaseMgr.GetQuality(newProp.Quality))
    if pinzhi >= EnumLingShouQuality_lua.Yi then
        CAppStoreReviewBox.Show()
    end

    if this:GetCompareResult(lingshouId, newProp) then
        this.Recommend = true
    else
        this.Recommend = false
    end
end
CLingShouWashResultWnd.m_GetCompareResult_CS2LuaHook = function (this, lingshouId, newProp) 
    local fightProp = CLingShouMgr.Inst:GetLingShouFightPropByLingShouProp(lingshouId, newProp)

    local oldVal = 0
    local newVal = 0
    local details = CLingShouMgr.Inst:GetLingShouDetails(this.mSelectedLingShou)

    if details.data.Props.Quality < newProp.Quality then
        return true
    else
        return false
    end
    --Recommend = false;

    local type = CommonDefs.ConvertIntToEnum(typeof(EnumLingShouType), newProp.Type)

    local oldGrowFactor = details.fightProperty:GetParam(EPlayerFightProp.GrowFactor)
    local newGrowFactor = fightProp:GetParam(EPlayerFightProp.GrowFactor)
    if type == EnumLingShouType.PAttack then
        oldVal = CClientFormula.Inst:Formula_195(oldGrowFactor, details.fightProperty:GetParam(EPlayerFightProp.InitStrZizhi))
        newVal = CClientFormula.Inst:Formula_195(newGrowFactor, fightProp:GetParam(EPlayerFightProp.InitStrZizhi))
    elseif type == EnumLingShouType.MAttack then
        oldVal = CClientFormula.Inst:Formula_196(oldGrowFactor, details.fightProperty:GetParam(EPlayerFightProp.InitIntZizhi))
        newVal = CClientFormula.Inst:Formula_196(newGrowFactor, fightProp:GetParam(EPlayerFightProp.InitIntZizhi))
    elseif type == EnumLingShouType.PAType or type == EnumLingShouType.MAType then
        oldVal = CClientFormula.Inst:Formula_210(oldGrowFactor, details.fightProperty:GetParam(EPlayerFightProp.InitAgiZizhi))
        newVal = CClientFormula.Inst:Formula_210(newGrowFactor, fightProp:GetParam(EPlayerFightProp.InitAgiZizhi))
    else
        oldVal = CClientFormula.Inst:Formula_194(oldGrowFactor, details.fightProperty:GetParam(EPlayerFightProp.InitCorZizhi))
        newVal = CClientFormula.Inst:Formula_194(newGrowFactor, fightProp:GetParam(EPlayerFightProp.InitCorZizhi))
    end
    if newVal > oldVal then
        return true
    else
        return false
    end
end
