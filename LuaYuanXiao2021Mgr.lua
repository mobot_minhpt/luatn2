local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local CIMMgr = import "L10.Game.CIMMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local Utility = import "L10.Engine.Utility"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CPos = import "L10.Engine.CPos"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CScene = import "L10.Game.CScene"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local Color = import "UnityEngine.Color"
local ShaderEx = import "ShaderEx"
local Main = import "L10.Engine.Main"


LuaYuanXiao2021Mgr = class()
LuaYuanXiao2021Mgr.tangyuanDuration = 0
LuaYuanXiao2021Mgr.tangyuanScore = 0
LuaYuanXiao2021Mgr.tangYuanPlayerCount = 0
LuaYuanXiao2021Mgr.todayTangYuanScore = 0
LuaYuanXiao2021Mgr.tangYuanRank = 0
----------------------------------------
---进击的汤圆
----------------------------------------
function LuaYuanXiao2021Mgr:IsInTangYuanPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51101949 then
            return true
        end
    end
    return false
end

function LuaYuanXiao2021Mgr:TangYuan2021PlayerEnd(duration, todayScore, score, rank)
    LuaYuanXiao2021Mgr.tangyuanDuration = duration
    LuaYuanXiao2021Mgr.tangyuanScore = score
    LuaYuanXiao2021Mgr.todayTangYuanScore = todayScore
    LuaYuanXiao2021Mgr.tangYuanRank = rank
    CUIManager.ShowUI(CLuaUIResources.YuanXiao2021TangYuanSettlementWnd)
end

function LuaYuanXiao2021Mgr:TodayTangYuanScore(score)
    LuaYuanXiao2021Mgr.todayTangYuanScore = score
    g_ScriptEvent:BroadcastInLua("TodayTangYuanScore", score)
end

function LuaYuanXiao2021Mgr:UpdateTangYuan2021PlayInfo(data)
    local count = data[0]
    local maxHp = data[1]
    LuaYuanXiao2021Mgr.tangYuanPlayerCount = count
    for i=2, data.Count-2, 2 do
        local playerId = data[i]
        local playerHp = data[i+1]
        LuaGamePlayHpMgr:AddHpInfo(playerId, playerHp, maxHp, false, 0, true)
    end

    g_ScriptEvent:BroadcastInLua("TangYuanPlayerCountUpdate")
end

----------------------------------------
---元宵放灯
----------------------------------------
LuaYuanXiao2021Mgr.fangDengFxList = {}
LuaYuanXiao2021Mgr.fangDengMatList = {}

function LuaYuanXiao2021Mgr:ShowPlayListWnd()
    local player = CClientMainPlayer.Inst
    if not player then
        return
    end

    CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("祈愿对象")
    CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("选择")
    CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
    CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

    -- 先加自己
    local data = CreateFromClass(CCommonPlayerDisplayData, player.Id, "[00FF2B]"..player.Name.."[-]", player.Level, player.Class, player.Gender, player.BasicProp.Expression, true)
    CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)

    -- 再加在线好友
    local friendlinessTbl = {}
    CommonDefs.DictIterate(player.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
        if CIMMgr.Inst:IsOnline(k) and CIMMgr.Inst:IsSameServerFriend(k) then
            table.insert(friendlinessTbl, {k, v.Friendliness})
        end
    end))
    table.sort(friendlinessTbl, function(v1, v2) return v1[2] > v2[2] end)

    for _, info in pairs(friendlinessTbl) do
        local basicInfo = CIMMgr.Inst:GetBasicInfo(info[1])
        if basicInfo then
            local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
            CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
        end
    end

    CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(function(playerId)
        g_ScriptEvent:BroadcastInLua("YuanXiao2020_SelectPlayer",playerId)
        CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
    end)
    CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function LuaYuanXiao2021Mgr:RequestFangDeng(SelectedTargetPlayerId, SelectTargetName, SelectedTargetFangDengIndex, isUseVoucher,wishMsg)
    CUIManager.CloseUI(CLuaUIResources.YuanXiao2021QiYuanWnd)
    Gac2Gas.YuanXiaoLanternSendLantern(SelectedTargetPlayerId, SelectTargetName, SelectedTargetFangDengIndex, isUseVoucher,wishMsg)
end

function LuaYuanXiao2021Mgr:TryOpenFangDeng3D()
    local CameraView = YuanXiao_LanternSetting.GetData().CameraView
    local r, z, y = CameraView[0], CameraView[1], CameraView[2]
    local vec3 = Vector3(r, z, y)
    if CameraFollow.Inst.CurMode ~= CameraMode.E3DPlus then
        local message = g_MessageMgr:FormatMessage("YuanXiao2020_3DPlus_Confirm")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
            CameraFollow.Inst:SetMode(CameraMode.E3DPlus)
            PlayerSettings.Saved3DMode = 2
            CameraFollow.Inst.targetRZY = vec3
            CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        return
    end
    CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function LuaYuanXiao2021Mgr:TryTrackOpenFangDeng3D(sceneId, sceneTemplateId, x, y)
    local pixelPos = Utility.GridPos2PixelPos(CPos(x, y))
    CTrackMgr.Inst:Track(sceneId, sceneTemplateId, pixelPos, 0, DelegateFactory.Action(function()
        self:TryOpenFangDeng3D()
    end), nil, nil, nil, false)
end

function LuaYuanXiao2021Mgr:SetFangDengShader(mat)
    if Main.Inst.EngineVersion >= ShaderEx.s_MinVersion or Main.Inst.EngineVersion == -1 then
        mat.shader = ShaderEx.Find("L10/Scene/AlphaBlend-Advance")
    end
end

function LuaYuanXiao2021Mgr:ResetFangDengShader(mat)
    if Main.Inst.EngineVersion >= ShaderEx.s_MinVersion or Main.Inst.EngineVersion == -1 then
        mat.shader = ShaderEx.Find("L10/Scene/StandardEnvironment-Advance")
        mat:SetColor("_Color", Color.white)
    end
end

-- 开启FadeOut特效
function LuaYuanXiao2021Mgr:FadeOutFangDeng(kind, releaseAfterDestory)
    if self.fangDengMatList[kind]~=nil and #self.fangDengFxList[kind]>0  and (Main.Inst.EngineVersion >= ShaderEx.s_MinVersion or Main.Inst.EngineVersion == -1) then
        local amount = 1
        local tick
        local color = Color.white
        for i=1,#self.fangDengMatList[kind] do
            LuaYuanXiao2021Mgr:SetFangDengShader(self.fangDengMatList[kind][i])
        end
        tick = CTickMgr.Register(DelegateFactory.Action(function ()
            amount = amount - 0.01
            if amount<=0 then
                for i=1,#self.fangDengMatList[kind] do
                    LuaYuanXiao2021Mgr:ResetFangDengShader(self.fangDengMatList[kind][i])
                end
                -- 消失完之后，销毁之前的灯笼
                self:DestoryLantern(kind)
                if releaseAfterDestory then
                    self:ReleaseLantern(kind)
                end
                tick:Invoke()
            else
                for i=1,#self.fangDengMatList[kind] do
                    color.a = amount
                    self.fangDengMatList[kind][i]:SetColor("_Color", color)
                end
            end
        end), 50, ETickType.Loop)
    else
        self:DestoryLantern(kind)
        if releaseAfterDestory then
            self:ReleaseLantern(kind)
        end
    end
end

-- 销毁灯笼 并清空数组
function LuaYuanXiao2021Mgr:DestoryLantern(kind)
    for k, v in pairs(self.fangDengFxList) do
        if (kind == nil) or (k == kind) then
            for _, fx in pairs(v) do
                fx:Destroy()
            end
            self.fangDengFxList[k] = nil
        end
    end
end

-- kind表示种类 =nil时代表全部种类
function LuaYuanXiao2021Mgr:StopFangDeng(kind, releaseAfterDestory)
    for k, v in pairs(self.fangDengFxList) do
        if (kind == nil) or (k == kind) then
            LuaYuanXiao2021Mgr:FadeOutFangDeng(k, releaseAfterDestory)
        end
    end
end

function LuaYuanXiao2021Mgr:StartFangDeng(kind)
    -- 判断当前是否有灯存在
    -- 只消除同种的灯笼
    if self.fangDengFxList[kind] and #self.fangDengFxList[kind] >0 then
        self:StopFangDeng(kind, true)
    else
        LuaYuanXiao2021Mgr:LoadMat(kind)
        LuaYuanXiao2021Mgr:ReleaseLantern(kind)
    end
end

function LuaYuanXiao2021Mgr:LoadMat(kind)
    if self.fangDengMatList[kind]~= nil then
        return
    end

    local f = DelegateFactory.Action_Material(function (mat)
        if mat then
            LuaYuanXiao2021Mgr:ResetFangDengShader(mat)
            table.insert(self.fangDengMatList[kind], mat)
        end
    end)

    if kind == 1 then
        self.fangDengMatList[kind] = {}
        local matPath = "Assets/Res/Scenes/Models/Common/misc/deng/Materials/dj_denglong_011_001_d_04.mat"
        CSharpResourceLoader.Inst:LoadMaterial(matPath, f)
    elseif kind == 2 then
        self.fangDengMatList[kind] = {}
        local matPath = "Assets/Res/Scenes/Models/Common/misc/deng/Materials/dj_denglong_011_001_d_05.mat"
        CSharpResourceLoader.Inst:LoadMaterial(matPath, f)
    elseif kind == 3 then
        self.fangDengMatList[kind] = {}
        local matPaths = {}
        table.insert(matPaths, "Assets/Res/Scenes/Models/Common/misc/deng/Materials/dj_denglong_011_001_d.mat")
        table.insert(matPaths, "Assets/Res/Scenes/Models/Common/misc/deng/Materials/dj_denglong_011_001_d_01.mat")
        table.insert(matPaths, "Assets/Res/Scenes/Models/Common/misc/deng/Materials/dj_denglong_011_001_d_02.mat")
        table.insert(matPaths, "Assets/Res/Scenes/Models/Common/misc/deng/Materials/dj_denglong_011_001_d_03.mat")
        for i=1,#matPaths do
            CSharpResourceLoader.Inst:LoadMaterial(matPaths[i], f)
        end
    end
end

function LuaYuanXiao2021Mgr:ReleaseLantern(kind)
    self.fangDengFxList[kind] = {}
    local lanternType = YuanXiao_LanternType.GetData(kind)
    local len = lanternType.Pos.Length
    for i = 0,len - 1,2 do
        local pos, fxId = CPos(lanternType.Pos[i] * 64,lanternType.Pos[i + 1] * 64), lanternType.FxId
        local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
        table.insert(self.fangDengFxList[kind], fx)
    end
end
