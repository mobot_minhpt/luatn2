require("common/common_include")

local UIGrid = import "UIGrid"
local UIProgressBar = import "UIProgressBar"
local Baby_Feeding = import "L10.Game.Baby_Feeding"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Color = import "UnityEngine.Color"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local Baby_BabyExp = import "L10.Game.Baby_BabyExp"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Baby_Setting = import "L10.Game.Baby_Setting"

LuaBabyFeedWnd = class()

RegistClassMember(LuaBabyFeedWnd, "BabyGradeLabel")
RegistClassMember(LuaBabyFeedWnd, "ExpProgress")
RegistClassMember(LuaBabyFeedWnd, "FullfillDesc")
RegistClassMember(LuaBabyFeedWnd, "ExpFX")
RegistClassMember(LuaBabyFeedWnd, "HintLabel")

RegistClassMember(LuaBabyFeedWnd, "ItemGrid")
RegistClassMember(LuaBabyFeedWnd, "ItemTemplate")

RegistClassMember(LuaBabyFeedWnd, "NeedItems") --templateId, go
RegistClassMember(LuaBabyFeedWnd, "ItemList")

function LuaBabyFeedWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyFeedWnd:InitClassMembers()

	self.BabyGradeLabel = self.transform:Find("Anchor/Fullfill/BabyGradeLabel"):GetComponent(typeof(UILabel))
	self.ExpProgress = self.transform:Find("Anchor/Fullfill/ExpProgress"):GetComponent(typeof(UIProgressBar))
	self.FullfillDesc = self.transform:Find("Anchor/Fullfill/ExpProgress/FullfillDesc"):GetComponent(typeof(UILabel))
	self.ExpFX =  self.transform:Find("Anchor/Fullfill/ExpProgress/ExpFX"):GetComponent(typeof(CUIFx))
	self.HintLabel = self.transform:Find("Anchor/Fullfill/HintLabel"):GetComponent(typeof(UILabel))

	self.ItemGrid = self.transform:Find("Anchor/ItemGrid"):GetComponent(typeof(UIGrid))
	self.ItemTemplate = self.transform:Find("Anchor/ItemTemplate").gameObject
	self.ItemTemplate:SetActive(false)

end

function LuaBabyFeedWnd:InitValues()

	self:UpdateBabyGradeAndExp(LuaBabyMgr.m_SelectedBabyNPCGrade, LuaBabyMgr.m_SelectedBabyNPCExp, LuaBabyMgr.m_SelectedBabyNPCLeftFeedTimes, true)
	self:UpdateFoodItemGrid()

end

function LuaBabyFeedWnd:UpdateBabyGradeAndExp(grade, exp, leftTimes, isInit)
	local babyExp = Baby_BabyExp.GetData(grade)
	if babyExp then
		self.BabyGradeLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), grade)
		if exp > babyExp.Value then
			exp = babyExp.Value
		end
		self.FullfillDesc.text = SafeStringFormat3("%d/%d", exp, babyExp.Value)
		self.ExpProgress.value = exp / babyExp.Value
		if not isInit then
			self.ExpFX:DestroyFx()
			self.ExpFX:LoadFx(CUIFxPaths.GQJCTeamNumChangeFx)
		end
	else
		self.BabyGradeLabel.text = LocalString.GetString("0级")
	end
	local setting = Baby_Setting.GetData()
	self.HintLabel.text = SafeStringFormat3(LocalString.GetString("今日剩余喂养%s/%s次"), tostring(leftTimes), tostring(setting.FeedTimesMax))
end

function LuaBabyFeedWnd:UpdateFoodItemGrid()

	self.NeedItems = {}
	self.ItemList = {}

	CUICommonDef.ClearTransform(self.ItemGrid.transform)

	local notZeroItemList = {}
	local zeroItemList = {}

	Baby_Feeding.ForeachKey(DelegateFactory.Action_object(function (key) 
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, key)
        if count > 0 then
        	table.insert(notZeroItemList, key)
        else
        	table.insert(zeroItemList, key)
        end
    end))

	self:InitFoodItemList(notZeroItemList)
	self:InitFoodItemList(zeroItemList)
    self.ItemGrid:Reposition()

end

function LuaBabyFeedWnd:InitFoodItemList(idList)
	if not idList then return end
	for i = 1, #idList do
		local data = Baby_Feeding.GetData(idList[i])
        local go = NGUITools.AddChild(self.ItemGrid.gameObject, self.ItemTemplate)
        self:InitItem(go, idList[i], data)
        go:SetActive(true)

        self.NeedItems[idList[i]] = go
        table.insert(self.ItemList, go)
	end
end

function LuaBabyFeedWnd:InitItem(go, templateId, feed)
	local NameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local Description = go.transform:Find("Description"):GetComponent(typeof(UILabel))
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local Mask = go.transform:Find("Mask").gameObject
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local ItemBtn = go.transform:Find("Icon").gameObject
	local ItemGo = go
	local ItemSelectedSprite = go.transform:Find("ItemSelectedSprite").gameObject
	local BtnSelectSprite = go.transform:Find("BtnSelectSprite").gameObject

	local item = Item_Item.GetData(templateId)
	local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, templateId)

	Icon:LoadMaterial(item.Icon)
	NameLabel.text = item.Name
	Description.text = SafeStringFormat3(LocalString.GetString("增加%d经验"), feed.AddExpValue)
	CountLabel.text = tostring(count)
	Mask:SetActive(count <= 0)
	CountLabel.color = Color.white
	if count <= 0 then
		CountLabel.color = Color.red
	end

	local onItemConsumeClicked = function (go)
		self:OnItemConsumeClicked(go, templateId)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onItemConsumeClicked), false)

	local onItemGetClicked = function (go)
		self:OnItemGetClicked(go, templateId)
	end
	CommonDefs.AddOnClickListener(ItemBtn, DelegateFactory.Action_GameObject(onItemGetClicked), false)
end

function LuaBabyFeedWnd:OnItemConsumeClicked(go, templateId)
	-- 设置是否选中
	for i = 1, #self.ItemList do
		self:SelectItem(self.ItemList[i], self.ItemList[i] == go)
	end
	local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, templateId)
	if count > 0 then
		if not LuaBabyMgr.m_SelectedBabyNPCEngineId then
			g_MessageMgr:ShowMessage("SELECT_BABY_BEFORE_FEED")
		else
			Gac2Gas.RequestFeedingBabyNpc(LuaBabyMgr.m_SelectedBabyNPCEngineId, templateId)
		end
	else
		g_MessageMgr:ShowMessage("BABY_FEED_ITEM_NOT_ENOUGH")
	end
	
end

function LuaBabyFeedWnd:SelectItem(go, isSelected)
	local ItemSelectedSprite = go.transform:Find("ItemSelectedSprite").gameObject
	local BtnSelectSprite = go.transform:Find("BtnSelectSprite").gameObject

	if isSelected then
		ItemSelectedSprite:SetActive(true)
	else
		ItemSelectedSprite:SetActive(false)
		BtnSelectSprite:SetActive(false)
	end
end

function LuaBabyFeedWnd:OnItemGetClicked(go, templateId)

	local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, templateId)
	if count > 0 then
		CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
	else
		CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, go.transform, CTooltipAlignType.Left)
	end
end

function LuaBabyFeedWnd:SendItem(args)
	local item = CItemMgr.Inst:GetById(args[0])
	if not item then return end

	local go = self.NeedItems[item.TemplateId]
	if go then
		local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, item.TemplateId)
		local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
		local Mask = go.transform:Find("Mask").gameObject

		CountLabel.text = tostring(count)
		Mask:SetActive(count <= 0)
	end
end


function LuaBabyFeedWnd:SetItemAt(place, position, oldItemId, newItemId)
	self:UpdateFoodItemGrid()
end

function LuaBabyFeedWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBabyGradeAndExp", self, "UpdateBabyGradeAndExp")
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
end

function LuaBabyFeedWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBabyGradeAndExp", self, "UpdateBabyGradeAndExp")
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
end

return LuaBabyFeedWnd
