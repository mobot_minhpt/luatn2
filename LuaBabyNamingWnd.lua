require("common/common_include")

local UIInput = import "UIInput"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local BabyMgr = import "L10.Game.BabyMgr"
local Baby_Setting = import "L10.Game.Baby_Setting"

LuaBabyNamingWnd = class()

RegistClassMember(LuaBabyNamingWnd, "Input")
RegistClassMember(LuaBabyNamingWnd, "RenameButton")
RegistClassMember(LuaBabyNamingWnd, "CancelButton")
RegistClassMember(LuaBabyNamingWnd, "QnCostAndOwnMoney")

-- data --
RegistClassMember(LuaBabyNamingWnd, "SelectedBaby")


function LuaBabyNamingWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyNamingWnd:InitClassMembers()
	self.Input = self.transform:Find("Anchor/Input"):GetComponent(typeof(UIInput))
	self.RenameButton = self.transform:Find("Anchor/RenameButton").gameObject
	self.CancelButton = self.transform:Find("Anchor/CancelButton").gameObject
	self.QnCostAndOwnMoney = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
end

function LuaBabyNamingWnd:InitValues()

	local baby = BabyMgr.Inst:GetSelectedBaby()
	if not baby then 
		CUIManager.CloseUI(CLuaUIResources.BabyNamingWnd)
		return
	end

	self.SelectedBaby = baby
	local isFirstNaming = baby.Props.ExtraData.Renamed == 0
	self.QnCostAndOwnMoney:SetType(1, 0, true)

	if isFirstNaming then
		self.QnCostAndOwnMoney:SetCost(0)
	else
		self.QnCostAndOwnMoney:SetCost(200000)
	end

	local onNameButtonClicked = function (go)
		self:OnNameButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.RenameButton, DelegateFactory.Action_GameObject(onNameButtonClicked), false)

	local onCancelButtonClicked = function (go)
		self:OnCancelButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.CancelButton, DelegateFactory.Action_GameObject(onCancelButtonClicked), false)
end

function LuaBabyNamingWnd:OnCancelButtonClicked(go)
	CUIManager.CloseUI(CLuaUIResources.BabyNamingWnd)
end

function LuaBabyNamingWnd:OnNameButtonClicked(go)
	local nameStr = self.Input.value
	local setting = Baby_Setting.GetData()

	local length = CUICommonDef.GetStrByteLength(nameStr)

	if not nameStr or nameStr == "" then
		g_MessageMgr:ShowMessage("BABY_NO_NAME")
		return
	end

	if not CWordFilterMgr.Inst:CheckName(nameStr, LocalString.GetString("宝宝名")) then
		g_MessageMgr:ShowMessage("Name_Violation")
		return
	end

	if length > setting.NameMaxLength then
		g_MessageMgr:ShowMessage("BABY_NAME_TOO_LONG")
		return
	end

	if not self.QnCostAndOwnMoney.moneyEnough then
		g_MessageMgr:ShowMessage("NOT_ENOUGH_MONEY_RENAME_BABY")
		return
	end
	Gac2Gas.RequestRenameBaby(self.SelectedBaby.Id, nameStr)

end

return LuaBabyNamingWnd
