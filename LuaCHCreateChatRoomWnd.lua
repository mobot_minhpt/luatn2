local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local UITabBar = import "L10.UI.UITabBar"
local UIInput = import "UIInput"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUITexture      = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaCHCreateChatRoomWnd = class()

RegistClassMember(LuaCHCreateChatRoomWnd, "m_RoomTagTabbar")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_RoomTagTabTable")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_RoomTagTemplate")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_Input")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_CreateButton")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_RuleButton")

RegistClassMember(LuaCHCreateChatRoomWnd, "m_PublicRoomTip")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_Password")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_PasswordInput")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_NoPasswardTip")

RegistClassMember(LuaCHCreateChatRoomWnd, "m_PrivateRoomLicense")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_TypeId")
RegistClassMember(LuaCHCreateChatRoomWnd, "m_TypeItemList")

function LuaCHCreateChatRoomWnd:Awake()
    self.m_RoomTagTabbar = self.transform:Find("Anchor/TabBar"):GetComponent(typeof(UITabBar))
    self.m_RoomTagTabTable = self.transform:Find("Anchor/TabBar"):GetComponent(typeof(UITable))
    self.m_RoomTagTemplate = self.transform:Find("Anchor/TagTemplate").gameObject
    self.m_Input = self.transform:Find("Anchor/NameInput"):GetComponent(typeof(UIInput))
    self.m_CreateButton = self.transform:Find("Anchor/CreateButton"):GetComponent(typeof(CButton))
    self.m_RuleButton = self.transform:Find("Anchor/RuleButton").gameObject

    self.m_RoomTagTabbar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(go, index)
    end)
    UIEventListener.Get(self.m_CreateButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCreateButtonClick()
    end)
    
    -- 私密房间密码部分
    self.m_PublicRoomTip = self.transform:Find("Anchor/PublicRoomTip").gameObject
    self.m_Password = self.transform:Find("Anchor/Password").gameObject
    self.m_PasswordInput = self.transform:Find("Anchor/Password/PasswordInput"):GetComponent(typeof(UIInput))
    self.m_NoPasswardTip = self.transform:Find("Anchor/Password/NoPasswardTip").gameObject
    CommonDefs.AddEventDelegate(self.m_PasswordInput.onChange, DelegateFactory.Action(function ()
        self:OnPasswordInputValueChanged()
    end))
    UIEventListener.Get(self.m_RuleButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleButtonClick()
    end)
end

function LuaCHCreateChatRoomWnd:Init()
    self.m_RoomTagTemplate:SetActive(false)
    self.m_TypeItemList = {}
    Extensions.RemoveAllChildren(self.m_RoomTagTabTable.transform)
    for i = 1, GameplayItem_ClubHouseTag.GetDataCount() do
        local tagData = GameplayItem_ClubHouseTag.GetData(i)
        local type = NGUITools.AddChild(self.m_RoomTagTabTable.gameObject, self.m_RoomTagTemplate)
        type.gameObject:SetActive(true)
        type:GetComponent(typeof(UISprite)).spriteName = tagData.TagIcon
        table.insert(self.m_TypeItemList, type)
    end
    local setData = GameplayItem_Setting.GetData()
    self.m_PrivateRoomLicense = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= setData.ClubHouse_Qn_Private_Owner_LevelLimit 
                                and CClientMainPlayer.Inst.ItemProp.Vip.Level >= setData.ClubHouse_Qn_Private_Owner_VipLimit

    self.m_Input.characterLimit = setData.ClubHouse_RoomNameLimit or 30
    self.m_RoomTagTabTable:Reposition()
    self.m_RoomTagTabbar:ReloadTabButtons()
    self.m_RoomTagTabbar:ChangeTab(0, false)
    self.m_PasswordInput.value = ""
end

function LuaCHCreateChatRoomWnd:OnEnable()
    g_ScriptEvent:AddListener("ClubHouse_CreateRoom_Result", self, "OnClubHouseCreateRoomResult")
end

function LuaCHCreateChatRoomWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ClubHouse_CreateRoom_Result", self, "OnClubHouseCreateRoomResult")
end

function LuaCHCreateChatRoomWnd:OnClubHouseCreateRoomResult(playerId, bSuccess)
    if bSuccess then
        self:Close()
    end
end

function LuaCHCreateChatRoomWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end


function LuaCHCreateChatRoomWnd:OnTabChange(go, index)
    if index + 1 == #self.m_TypeItemList then
        -- 选择私密，判断是否符合资格
        if not self.m_PrivateRoomLicense then
            g_MessageMgr:ShowMessage("CLUBHOUSE_CANT_CREATE_PRIVATEROOM")
            self.m_RoomTagTabbar:ChangeTab(self.m_TypeId - 1, false)    -- 复原选择
            return
        else
            self:OnPasswordInputValueChanged()
        end
    else
        self.m_CreateButton.Text = LocalString.GetString("创  建")
        CUICommonDef.SetActive(self.m_CreateButton.gameObject, true, true)
    end

    self.m_TypeId = index + 1
    for i = 1, #self.m_TypeItemList do
        self.m_TypeItemList[i].transform:Find("HighLight").gameObject:SetActive(i == self.m_TypeId)
    end
    self.m_PublicRoomTip:SetActive(self.m_TypeId ~= #self.m_TypeItemList)
    self.m_Password:SetActive(self.m_TypeId == #self.m_TypeItemList)
    if self.m_TypeId == #self.m_TypeItemList then self:SetPrivateRoomDefalutPassword() end
end

function LuaCHCreateChatRoomWnd:SetPrivateRoomDefalutPassword()
    -- 显示上一次创建私密房间的密码
    if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eClubHousePassWord) then
        local prePassword = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eClubHousePassWord].StringData
        self.m_PasswordInput.text = prePassword
    end
end

function LuaCHCreateChatRoomWnd:OnCreateButtonClick()
    local roomName = self.m_Input.value
    --检查是否为空
	roomName = CUICommonDef.Trim(roomName)
    if roomName == nil or roomName == "" then 
        g_MessageMgr:ShowMessage("CLUBHOUSE_CREATE_ROOM_NAME_EMPTY")
        return
    end
    --检查是否有敏感词
    if not CWordFilterMgr.Inst:CheckName(roomName) then
        g_MessageMgr:ShowMessage("CLUBHOUSE_CREATE_ROOM_NAME_INVALID")
        return
    end
    if LuaClubHouseMgr:CheckIfInCCMiniCapturingForCreateRoom() then
        return
    end
    local roomType = self.m_TypeId == #self.m_TypeItemList and EnumCHRoomType.private or EnumCHRoomType.apponly
    LuaClubHouseMgr:CreateRoom(roomName, roomType, self.m_TypeId, self.m_PasswordInput.value or "")
end

function LuaCHCreateChatRoomWnd:OnRuleButtonClick()
    g_MessageMgr:ShowMessage("CLUBHOUSE_CREATE_PRIVATE_ROOM_RULE_TIP")
end

function LuaCHCreateChatRoomWnd:OnPasswordInputValueChanged()
    local str = self.m_PasswordInput.value
    self.m_NoPasswardTip:SetActive(System.String.IsNullOrEmpty(str))
    if CommonDefs.StringLength(str) >= 6 then
        self.m_CreateButton.Text = LocalString.GetString("创  建")
        CUICommonDef.SetActive(self.m_CreateButton.gameObject, true, true)
    else
        self.m_CreateButton.Text = LocalString.GetString("未设置密码")
        CUICommonDef.SetActive(self.m_CreateButton.gameObject, false, true)
    end
end