local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"

LuaAntiProfessionUnlockWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAntiProfessionUnlockWnd, "SelectView", "SelectView", GameObject)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "ScrollView", "ScrollView", QnTableView)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "Lab", "Lab", UILabel)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "SeqLab", "SeqLab", UILabel)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "RateLab", "RateLab", UILabel)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "Cost1", "Cost1", GameObject)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "Cost2", "Cost2", GameObject)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaAntiProfessionUnlockWnd, "TipBtn", "TipBtn", QnButton)

RegistClassMember(LuaAntiProfessionUnlockWnd, "m_TargetBtn")
RegistClassMember(LuaAntiProfessionUnlockWnd, "m_NeedScore")
RegistClassMember(LuaAntiProfessionUnlockWnd, "m_Seq")
RegistClassMember(LuaAntiProfessionUnlockWnd, "m_SelectedCls")
RegistClassMember(LuaAntiProfessionUnlockWnd, "m_UnlockableCls")

--@endregion RegistChildComponent end

function LuaAntiProfessionUnlockWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitComponents()
    self.Icon.gameObject:SetActive(false)
    self.Cost1:SetActive(false)
    self.Cost2:SetActive(false)
    self.Lab.text = ""
end

function LuaAntiProfessionUnlockWnd:Init()
    self.m_Seq = CClientMainPlayer.Inst.SkillProp.AntiProfessionSlot.CurSlotNum + 1
    local data = SoulCore_AntiProfessionSlot.GetData(self.m_Seq)
    self.SeqLab.text = SafeStringFormat3(LocalString.GetString("第[c][ffff00]%d[-][/c]个"), self.m_Seq)
    self.RateLab.text = data.UnlockSuccessRate >= 1 and SafeStringFormat3("[c][00ff60]%.f%%[-][/c]", data.UnlockSuccessRate * 100) or SafeStringFormat3("[c][ffff00]%.f%%[-][/c]", data.UnlockSuccessRate * 100)
    self.m_NeedScore = data.UnlocScore

    -- self.m_TargetBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
    --     self.SelectView:SetActive(true)
    -- end)

    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if self.m_SelectedCls ~= nil then
            Gac2Gas.AntiProfession_UnlockSlot(self.m_SelectedCls)
        else 
            g_MessageMgr:ShowMessage("ANTIPROFESSION_UNLOCKVIEW_NOSELECT")
        end
    end)

    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("ANTIPROFESSION_UNLOCKVIEW_TIP")
    end)

    self:InitUnlockableItems()
end

--@region UIEvent

--@endregion UIEvent

function LuaAntiProfessionUnlockWnd:InitComponents()
    self.m_TargetBtn = self.transform:Find("Anchor/UnlockView/Top/Target"):GetComponent(typeof(QnButton))
end

function LuaAntiProfessionUnlockWnd:OnEnable()
    g_ScriptEvent:AddListener("AntiProfessionUnlockSlotResult", self, "OnAntiProfessionUnlockSlotResult")
    g_ScriptEvent:AddListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
end

function LuaAntiProfessionUnlockWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AntiProfessionUnlockSlotResult", self, "OnAntiProfessionUnlockSlotResult")
    g_ScriptEvent:RemoveListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
end

function LuaAntiProfessionUnlockWnd:OnAntiProfessionUnlockSlotResult(bSuccess, Index, Profession, MaxLevel, FailTimes)
    if bSuccess == true then
        CUIManager.CloseUI(CLuaUIResources.AntiProfessionUnlockWnd)
    end
end

function LuaAntiProfessionUnlockWnd:OnAntiProfessionUpdateScore(profession, score)
    if self.m_SelectedCls then
        self:InitCost(self.Cost1, self.m_SelectedCls)
    end
    self.ScrollView:ReloadData(false, false)
end

function LuaAntiProfessionUnlockWnd:InitUnlockInfo(profession)
    self.m_SelectedCls = profession

    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)
    local proName = Profession.GetFullName(proClass)
    self.Icon.gameObject:SetActive(true)
    self.Icon:LoadMaterial(Profession.GetLargeIcon(proClass))
    self.Lab.text = SoulCore_Settings.GetData().AntiProfessionName[profession - 1]

    self.Cost1:SetActive(true)
    self:InitCost(self.Cost1, profession)
end

function LuaAntiProfessionUnlockWnd:InitCost(item, profession)
    local btn       = item:GetComponent(typeof(QnButton))
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local getLab    = item.transform:Find("GetLab"):GetComponent(typeof(UILabel))
    local numLab    = item.transform:Find("NumLab"):GetComponent(typeof(UILabel))

    icon:LoadMaterial(LuaZongMenMgr:GetProfessionScoreIconPath(profession))
    btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        local itemId = SoulCore_Settings.GetData().AntiProfessionItemId[profession - 1][1]
        if getLab.gameObject.activeSelf then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, true, btn.transform, AlignType.Right)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
        end
    end)

    local ownScore = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, profession) and CClientMainPlayer.Inst.SkillProp.AntiProfessionScore[profession] or 0

    if ownScore >= self.m_NeedScore then
        getLab.gameObject:SetActive(false)
        numLab.text = SafeStringFormat3("%d/%d", ownScore, self.m_NeedScore)
    else
        getLab.gameObject:SetActive(true)
        numLab.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", ownScore, self.m_NeedScore)
    end
end

function LuaAntiProfessionUnlockWnd:InitUnlockableItems()
    self.ScrollView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_UnlockableCls
        end,
        function(item, index)
            self:InitItem(item, self.m_UnlockableCls[index + 1])
        end
    )

    self.m_UnlockableCls = {}

    local unlockedProfessions = {}
    for i = 1, self.m_Seq - 1 do
        local unlockedProfession = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i) and CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[i].Profession or nil
        unlockedProfessions[unlockedProfession] = true
    end

    for i = 1, (EnumToInt(EnumClass.Undefined) - 1) do
        if not unlockedProfessions[i] then
            table.insert(self.m_UnlockableCls, i)
        end
    end

    self.ScrollView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        CGuideMgr.Inst:TriggerGuide(3)
        self:InitUnlockInfo(self.m_UnlockableCls[row + 1])
    end)
    
    self.ScrollView:ReloadData(true, false)

    --只有一个时自动选中
    if #self.m_UnlockableCls == 1 then
        self.ScrollView:SetSelectRow(0, true)
    end
end

function LuaAntiProfessionUnlockWnd:InitItem(item, profession)
    local btn = item:GetComponent(typeof(QnButton))
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local contentLab = item.transform:Find("Content"):GetComponent(typeof(UILabel))

    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)
    local proName = Profession.GetFullName(proClass)
    icon:LoadMaterial(Profession.GetLargeIcon(proClass))
    contentLab.text = SafeStringFormat3(LocalString.GetString("克制%s"), proName)

    local ownScore = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, profession) and CClientMainPlayer.Inst.SkillProp.AntiProfessionScore[profession] or 0
    if ownScore >= self.m_NeedScore then
        icon.alpha = 1
        contentLab.alpha = 1
    else
        icon.alpha = 0.392
        contentLab.alpha = 0.392
    end
end

function LuaAntiProfessionUnlockWnd:GetGuideGo(methodName)
    if methodName=="GetUnlockSelectTable" then
		return self.SelectView
	end
	return nil
end
