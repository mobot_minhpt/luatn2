local CScene=import "L10.Game.CScene"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CActionStateMgr = import "L10.Game.CActionStateMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local Quaternion = import "UnityEngine.Quaternion"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CButton = import "L10.UI.CButton"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local EnumWarnFXType=import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CUIFx = import "L10.UI.CUIFx"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientNpc = import "L10.Game.CClientNpc"

LuaTangYuan2022Mgr = {}

function LuaTangYuan2022Mgr.OpenSignWnd()
    CUIManager.ShowUI(CLuaUIResources.RunningWildTangYuanSignWnd)
end

function LuaTangYuan2022Mgr.OpenRankWnd()
    CUIManager.ShowUI(CLuaUIResources.RunningWildTangYuanRankWnd)
end

function LuaTangYuan2022Mgr.OpenResultWnd(finishNum, score, rankInPlay, oldDayMaxScore)
    LuaTangYuan2022Mgr.m_CurFinishNum = finishNum
    LuaTangYuan2022Mgr.m_CurScore = score
    LuaTangYuan2022Mgr.m_RankInPlay = rankInPlay
    LuaTangYuan2022Mgr.m_RecordedScore = oldDayMaxScore
    CUIManager.ShowUI(CLuaUIResources.RunningWildTangYuanResultWnd)
end

function LuaTangYuan2022Mgr.IsInPlay()
    if not CScene.MainScene then return 
        false 
    end

    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local isIn = false
    if YuanXiao_TangYuan2022Setting.GetData().GamePlayId == gamePlayId then
        isIn = true
    end
    return isIn
end

function LuaTangYuan2022Mgr:AddListener()
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")

    g_ScriptEvent:RemoveListener("SetActiveTempSkillSuccess", self, "OnSetActiveTempSkillSuccess")
    g_ScriptEvent:AddListener("SetActiveTempSkillSuccess", self, "OnSetActiveTempSkillSuccess")

    g_ScriptEvent:RemoveListener("InitAdditionalSkill", self, "OnInitAdditionalSkill")
    g_ScriptEvent:AddListener("InitAdditionalSkill", self, "OnInitAdditionalSkill")

    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")

    g_ScriptEvent:RemoveListener("ClientObjCreate", self, "ClientObjCreate")
    g_ScriptEvent:AddListener("ClientObjCreate", self, "ClientObjCreate")

    EventManager.AddListenerInternal(EnumEventType.ClientObjCreate, LuaTangYuan2022Mgr.m_OnClientCreateAction)

end

LuaTangYuan2022Mgr.m_OnClientCreateAction = DelegateFactory.Action_uint(function(engineId)
    if not LuaTangYuan2022Mgr.IsInPlay() then return end
    local playerId = LuaTangYuan2022Mgr.TangYuanAttachedPlayerDict[engineId]
    if playerId then
        local tangyuan =  CClientObjectMgr.Inst:GetObject(engineId)
        LuaTangYuan2022Mgr.PlayerGrabTangYuan(playerId, engineId)
    end
end)


function LuaTangYuan2022Mgr:OnMainPlayerSkillPropUpdate()
    self:OnInitAdditionalSkill()

end

function LuaTangYuan2022Mgr:OnSetActiveTempSkillSuccess()
    self:OnInitAdditionalSkill()
end

LuaTangYuan2022Mgr.PlayerInfo = {}
function LuaTangYuan2022Mgr:OnMainPlayerCreated()
    if not LuaTangYuan2022Mgr.IsInPlay() then
        LuaTangYuan2022Mgr.PlayerInfo = {}
        return 
    end
end

function LuaTangYuan2022Mgr:ClientObjCreate(args)
    if not LuaTangYuan2022Mgr.IsInPlay() then 
        return 
    end
    if not LuaTangYuan2022Mgr.PlayerAttachedTangYuanDict then
        return
    end
    --created obj
	local engineId = args[0]
	if not engineId then return end

	local playerId = nil
	local tangyuanId = nil
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if not obj then return end

	if TypeAs(obj, typeof(CClientMainPlayer)) then
		playerId = engineId
        tangyuanId = LuaTangYuan2022Mgr.PlayerAttachedTangYuanDict[playerId]
	elseif TypeAs(obj, typeof(CClientOtherPlayer)) then
		playerId = engineId
        tangyuanId = LuaTangYuan2022Mgr.PlayerAttachedTangYuanDict[playerId]
	elseif TypeAs(obj, typeof(CClientNpc)) then
		tangyuanId = engineId
        playerId = LuaTangYuan2022Mgr.TangYuanAttachedPlayerDict[tangyuanId]
	end

	if playerId and tangyuanId then
        LuaTangYuan2022Mgr.PlayerGrabTangYuan(playerId, engineId)
    end
end


LuaTangYuan2022Mgr.GraySkillIdDict = nil
function LuaTangYuan2022Mgr:InitGraySkillIdDict()
    if not LuaTangYuan2022Mgr.GraySkillIdDict then
        LuaTangYuan2022Mgr.GraySkillIdDict = {}
        local tempSkillIds = YuanXiao_TangYuan2022Setting.GetData().TempSkillIds
        for s=0,tempSkillIds.Length-1,1 do
            local tid = tempSkillIds[s]
            tid = math.floor(tid / 100)
            LuaTangYuan2022Mgr.GraySkillIdDict[tid]= true
        end
    end
end

function LuaTangYuan2022Mgr:OnInitAdditionalSkill()
    if LuaHanJia2023Mgr.IsInXuePoLiXianPlay() then
        return
    end
    
    local fxPath = YuanXiao_TangYuan2022Setting.GetData().GetSkillFx
    LuaTangYuan2022Mgr:InitGraySkillIdDict()
    if CSkillButtonBoardWnd.Instance then     
        local addtionalSkillIds = CSkillButtonBoardWnd.Instance.m_AdditionalSkillIds
        for i=0,addtionalSkillIds.Length-1,1 do 
            local skillId = addtionalSkillIds[i]
            skillId = CClientMainPlayer.Inst.SkillProp:GetAlternativeSkillIdWithDelta(skillId, CClientMainPlayer.Inst.Level)
            skillId = math.floor(skillId / 100)
            local btn = CSkillButtonBoardWnd.Instance.additionalButtonInfos[i]
            if btn then
                    local hitFx = btn.transform:Find("HitFx")
                    if hitFx then
                        hitFx=hitFx:GetComponent(typeof(CUIFx))
                        hitFx:DestroyFx()
                    end
                   
                    local icon = btn.transform:Find("Mask/Icon")
                    local pos = btn.skillIcon.transform.localPosition
                    if LuaTangYuan2022Mgr.GraySkillIdDict[skillId] then
                        btn.skillIcon.color = NGUIText.ParseColor24("383434", 0)
                        CUICommonDef.SetActive(btn.gameObject, false, true)
                    else
                        btn.skillIcon.color = Color.white
                        CUICommonDef.SetActive(btn.gameObject, true, true)                     
                        if hitFx and i~= 0 then
                            hitFx:LoadFx(fxPath)
                        end
                    end
                    local cbtn = btn.transform:GetComponent(typeof(CButton))
            end
        end
    end
end


LuaTangYuan2022Mgr:AddListener()

--初始化赛道空气墙特效
LuaTangYuan2022Mgr.KongQiQiangRos= {}
function LuaTangYuan2022Mgr.OnEnterPlay()
    if not LuaTangYuan2022Mgr.IsInPlay() then return end
    LuaTangYuan2022Mgr.LeaveScene()

    LuaTangYuan2022Mgr.TangyuanId2Scale = {}

    local setting = YuanXiao_TangYuan2022Setting.GetData()
    local barriers = setting.DynamicBarrierRects
    local fxId = setting.DynamicBarrierFx
    local points = {}
    local len = 2
    local logicHeight = 0

    LuaTangYuan2022Mgr.KongQiQiangRoot = GameObject("KongQiQiang_Root")
    for i=0,barriers.Length-4,4 do 
        local x1 = barriers[i] + 1
        local y1 = barriers[i+1]
        local x2 = barriers[i+2]
        local y2 = barriers[i+3]
        logicHeight = CScene.MainScene:GetLogicHeight(math.floor(x1 * 64), math.floor(y1 * 64))
        local count = math.abs(y2-y1)/len
        for c=0,count-1,1 do 
            local go = GameObject("KongQiQiang")
            go.transform.parent = LuaTangYuan2022Mgr.KongQiQiangRoot.transform
            local ro = CommonDefs.AddComponent_GameObject_Type(go, typeof(CRenderObject))
            ro:LoadMain("fx/common/prefab/kongqiqiang03.prefab",nil,false,false)
            ro.Position = Vector3(x1,logicHeight,y1+c*len)
            ro.transform.localRotation = Quaternion.Euler(Vector3(0,90,0)) 
            table.insert(LuaTangYuan2022Mgr.KongQiQiangRos,ro)

            local go2 = GameObject("KongQiQiang")
            go2.transform.parent = LuaTangYuan2022Mgr.KongQiQiangRoot.transform
            local ro2 = CommonDefs.AddComponent_GameObject_Type(go2, typeof(CRenderObject))
            ro2:LoadMain("fx/common/prefab/kongqiqiang03.prefab",nil,false,false)
            ro2.Position = Vector3(x2,logicHeight,y1+c*len)
            ro2.transform.localRotation = Quaternion.Euler(Vector3(0,90,0)) 
            table.insert(LuaTangYuan2022Mgr.KongQiQiangRos,ro2)
        end
    end
end

function LuaTangYuan2022Mgr.LeaveScene()
    --print("LuaTangYuan2022Mgr.LeaveScene")
    if LuaTangYuan2022Mgr.KongQiQiangRos and next(LuaTangYuan2022Mgr.KongQiQiangRos) then
        for i,ro in ipairs(LuaTangYuan2022Mgr.KongQiQiangRos) do 
            if ro and ro.transform then
                ro:Destroy()
            end
        end
    end
    -- if LuaTangYuan2022Mgr.KongQiQiangRoot then
    --     GameObject.Destroy(LuaTangYuan2022Mgr.KongQiQiangRoot)
    --     LuaTangYuan2022Mgr.KongQiQiangRoot = nil
    -- end
    LuaTangYuan2022Mgr.KongQiQiangRos = {}
end

--抓汤圆
LuaTangYuan2022Mgr.PlayerAttachedTangYuanDict = {}
LuaTangYuan2022Mgr.TangYuanAttachedPlayerDict = {}
function LuaTangYuan2022Mgr.PlayerGrabTangYuan(playerId, engineId)
    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    local tangyuan =  CClientObjectMgr.Inst:GetObject(engineId)

    if not player or not player.RO then
        return 
    end
    if not tangyuan or not tangyuan.RO then
        return 
    end

    LuaTangYuan2022Mgr.OverrideaChangeClip(tangyuan.RO)
    local scale = LuaTangYuan2022Mgr.TangyuanId2Scale[engineId] and LuaTangYuan2022Mgr.TangyuanId2Scale[engineId] or 1
    player.RO:AttachRO(tangyuan.RO, "Head", -0.2, -0.1, 0, 270, 90, 0, scale)
    LuaTangYuan2022Mgr.PlayerAttachedTangYuanDict[playerId] = engineId
    CActionStateMgr.Inst:RefreshMoveState(player) 
end

function LuaTangYuan2022Mgr.PlayerReleaseTangYuan(playerId)
    local engineId = LuaTangYuan2022Mgr.PlayerAttachedTangYuanDict[playerId]
    if not engineId then
        return
    end
    LuaTangYuan2022Mgr.TangYuanAttachedPlayerDict[engineId] = nil
    LuaTangYuan2022Mgr.PlayerAttachedTangYuanDict[playerId] = nil

    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    local tangyuan =  CClientObjectMgr.Inst:GetObject(engineId)
    if not player or not player.RO then
        return 
    end
    if not tangyuan or not tangyuan.RO then
        return 
    end

    local scale = LuaTangYuan2022Mgr.TangyuanId2Scale[engineId] and LuaTangYuan2022Mgr.TangyuanId2Scale[engineId] or 1
    player.RO:DetachRO(tangyuan.RO, 0, 0, 0, 0, 0, 0, scale)
end

function LuaTangYuan2022Mgr.OverrideaChangeClip(ro)
    local clip = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    clip:Clear()
    CommonDefs.DictAdd(clip, typeof(String), "walk01", typeof(String), "jingzhidaiji")
    CommonDefs.DictAdd(clip, typeof(String), "stand01", typeof(String), "jingzhidaiji")
    CommonDefs.DictAdd(clip, typeof(String), "run01", typeof(String), "jingzhidaiji")
    ro.m_AnimationChangeCashDic = clip
end

--同步所有其他玩家的当前汤圆分数用来维护汤圆大小 同时维护汤圆buff特效
LuaTangYuan2022Mgr.TangyuanId2Scale = {}
function LuaTangYuan2022Mgr.SyncTangYuan2022RoundScore(playerId, roundScore,enginedId)
    if not enginedId then return end
    local tangyuan = CClientObjectMgr.Inst:GetObject(enginedId)
    if not tangyuan or not tangyuan.RO then return end
    local scoreandScale = YuanXiao_TangYuan2022Setting.GetData().TangYuanRoundScoreScales
    local scale = scoreandScale[1]
    for i=0,scoreandScale.Length-2,2 do 
        local score = scoreandScale[i]
        local val = scoreandScale[i+1]
        if roundScore >= score then
            scale = val
        else
            break
        end
    end

    local olsScale = LuaTangYuan2022Mgr.TangyuanId2Scale[enginedId] and LuaTangYuan2022Mgr.TangyuanId2Scale[enginedId] or 1
    tangyuan.RO.Scale = scale
    LuaTangYuan2022Mgr.TangyuanId2Scale[enginedId] = scale

    local fxId 
    if scale > olsScale then
        fxId = YuanXiao_TangYuan2022Setting.GetData().TangYuanBecomeBigerFxId
    elseif scale < olsScale then
        fxId = YuanXiao_TangYuan2022Setting.GetData().TangYuanBecomeSmallerFxId
    end
    if fxId then
        local fx = CEffectMgr.Inst:AddObjectFX(fxId, tangyuan.RO, 0, scale, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
    end
end

--玩家变更时头顶冒加减分用 
function LuaTangYuan2022Mgr.UpdateTangYuan2022RoundScore(engineId, roundScore, deltaScore)
    local playerObj = CClientObjectMgr.Inst:GetObject(engineId)
    if playerObj and playerObj.RO then
		local c = Color.red
		if deltaScore > 0 then
			c = Color.green
		end
        local textSize = YuanXiao_TangYuan2022Setting.GetData().ScoreTextSize
		playerObj.RO:ShowScoreTextWithSize(deltaScore, c, textSize)
	end
end

--规则界面
function LuaTangYuan2022Mgr.ShowImageRuleWnd()
    local setting = YuanXiao_TangYuan2022Setting.GetData()
    local imagePaths = {
        "UI/Texture/NonTransparent/Material/kuangbentangyuan_01.mat",
        "UI/Texture/NonTransparent/Material/kuangbentangyuan_02.mat",
        "UI/Texture/NonTransparent/Material/kuangbentangyuan_03.mat",
        "UI/Texture/NonTransparent/Material/kuangbentangyuan_04.mat",
    }
    local msgs = {
        setting.KuangBenTangYuan_Image_Rule_Desc_1,
        setting.KuangBenTangYuan_Image_Rule_Desc_2,
        setting.KuangBenTangYuan_Image_Rule_Desc_3,
        setting.KuangBenTangYuan_Image_Rule_Desc_4,
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end
