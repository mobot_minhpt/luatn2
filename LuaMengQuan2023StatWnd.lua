require("common/common_include")
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local Profession = import "L10.Game.Profession"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"

LuaMengQuan2023StatWnd = class()
LuaMengQuan2023StatWnd.battleStat = nil
LuaMengQuan2023StatWnd.battleRank = nil
--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaMengQuan2023StatWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:RefreshData()
end

function LuaMengQuan2023StatWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaMengQuan2023StatWnd:RefreshData()
    local statData = LuaMengQuan2023StatWnd.battleStat
    local iCnt = 0
    local myRankItem = self.transform:Find("Anchor/Rank/MyRankItem")
    local myPlayerId = CClientMainPlayer.Inst and (CClientMainPlayer.Inst.Id) or 0
    for k, v in pairs(statData or {}) do
        local playerId = v[1]
        if playerId == myPlayerId then
            myRankItem.transform:GetComponent(typeof(UISprite)).spriteName = (iCnt % 2 == 1 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite)
            self:InitItem(myRankItem.transform, v, iCnt % 2 == 0, true)
            iCnt = iCnt + 1
        end
    end
    
    local templateObj = self.transform:Find("Anchor/Rank/AdvView/Pool/Template").gameObject
    templateObj:SetActive(false)
    local grid = self.transform:Find("Anchor/Rank/AdvView/Scroll View/Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(grid.transform)
    
    local teamScore = {}
    local teamRank = {}
    for k, v in pairs(LuaMengQuan2023StatWnd.battleRank) do
        teamScore[v[1]] = v[2]
        teamRank[v[1]] = k
    end
    if statData ~= nil then
        table.sort(statData, function (a, b)
            local aTeamId = a[3]
            local bTeamId = b[3]
            if teamScore[aTeamId] ~= teamScore[bTeamId] then
                return teamScore[aTeamId] > teamScore[bTeamId]
            else
                if teamRank[aTeamId] ~= teamRank[bTeamId] then
                    return teamRank[aTeamId] < teamRank[bTeamId]
                else
                    --同排名同积分用playerid来排序
                    return a[1] > b[1]
                end
            end
        end)
    end
    for k, v in pairs(statData or {}) do
        local obj = NGUITools.AddChild(grid.gameObject, templateObj)
        obj:SetActive(true)
        obj.transform:GetComponent(typeof(UISprite)).spriteName = (iCnt % 2 == 1 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite)
        self:InitItem(obj.transform, v, iCnt % 2 == 0, false)
        iCnt = iCnt + 1
    end
    grid:Reposition()
end

function LuaMengQuan2023StatWnd:InitItem(rootTrans, data, isOdd, needHighlight)
    local playerId = data[1]
    local isMe = CClientMainPlayer.Inst and (playerId == CClientMainPlayer.Inst.Id) or false
    local labelColor = (isMe and needHighlight) and Color.green or Color.white

    local name = data[2]
    local teamId = data[3]
    local dps = data[4]
    local ctrlScore = data[5]
    local healScore = data[6]
    local reviveScore = data[7]
    local careerNumber = data[8]
    local totalScore = 0
    local myRank = 1
    
    for k, v in pairs(LuaMengQuan2023StatWnd.battleRank) do
        if v[1] == teamId then
            totalScore = v[2]
            myRank = k
        end
    end

    rootTrans:Find("MineBg").gameObject:SetActive(isMe and needHighlight)
    --rootTrans:Find("OtherBg").gameObject:SetActive(not isMe)

    rootTrans:Find("Rank"):GetComponent(typeof(UISprite)).spriteName = "clubhouse_paihang_0" .. myRank
    rootTrans:Find("PlayerCarrer"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(careerNumber)

    rootTrans:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = name
    rootTrans:Find("KillLabel"):GetComponent(typeof(UILabel)).text = math.floor(dps)
    rootTrans:Find("ControlLabel"):GetComponent(typeof(UILabel)).text = math.floor(ctrlScore)
    if healScore >= 10000 then
        rootTrans:Find("HealLabel"):GetComponent(typeof(UILabel)).text = math.floor(healScore / 10000) .. LocalString.GetString("万")
    else
        rootTrans:Find("HealLabel"):GetComponent(typeof(UILabel)).text = math.floor(healScore)
    end
    rootTrans:Find("ReviveLabel"):GetComponent(typeof(UILabel)).text = math.floor(reviveScore)
    rootTrans:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = math.floor(totalScore)
    
    rootTrans:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("KillLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("ControlLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("HealLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("ReviveLabel"):GetComponent(typeof(UILabel)).color = labelColor
    rootTrans:Find("ScoreLabel"):GetComponent(typeof(UILabel)).color = labelColor
end 