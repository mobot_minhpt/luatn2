require("common/common_include")

local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local LuaTweenUtils = import "LuaTweenUtils"
local Vector3 = import "UnityEngine.Vector3"

LuaParentExamResultWnd = class()

RegistChildComponent(LuaParentExamResultWnd, "BG_Pass", GameObject)
RegistChildComponent(LuaParentExamResultWnd, "BG_NotPass", GameObject)
RegistChildComponent(LuaParentExamResultWnd, "ExamResultSprite", GameObject)

RegistChildComponent(LuaParentExamResultWnd, "ScoreLabel", UILabel)
RegistChildComponent(LuaParentExamResultWnd, "HintLabel", UILabel)

RegistChildComponent(LuaParentExamResultWnd, "ResultFx", CUIFx)
RegistChildComponent(LuaParentExamResultWnd, "LandMineView", GameObject)

RegistClassMember(LuaParentExamResultWnd, "TweenList")


function LuaParentExamResultWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaParentExamResultWnd:InitClassMembers()

	self.LandMineView:SetActive(false)

	self.BG_Pass:SetActive(false)
	self.BG_NotPass:SetActive(false)
	self.ExamResultSprite:SetActive(false)
	self.ResultFx:DestroyFx()
	self.TweenList = {}
end

function LuaParentExamResultWnd:InitValues()
	self:UpdateParentExamResult()
end

function LuaParentExamResultWnd:UpdateParentExamResult()
	local isPass = (LuaBabyMgr.m_FinalScore >= LuaBabyMgr.m_PassScore)
	if not LuaBabyMgr.m_IsLandMine then
		if isPass then
			self.HintLabel.text = LocalString.GetString("恭喜你通过了养育资格测试！")
		else
			self.HintLabel.text = LocalString.GetString("很遗憾，你没有通过此次测试。")
		end
	
		-- 设置背景
		self.ExamResultSprite:SetActive(true)
		self.BG_Pass:SetActive(isPass)
		self.BG_NotPass:SetActive(not isPass)

		-- 设置文字、特效、提示
		self.ScoreLabel.text = tostring(LuaBabyMgr.m_FinalScore)
		local scoreTween = LuaTweenUtils.TweenScale(self.ScoreLabel.transform, Vector3(1.2, 1.2, 1.2), Vector3(1.0, 1.0, 1.0), 0.5)
		LuaTweenUtils.OnComplete(scoreTween, function ()
			if isPass then
				self.ResultFx:LoadFx(CUIFxPaths.ParentExamPass)
			else
				self.ResultFx:LoadFx(CUIFxPaths.ParentExamNotPass)
			end
		end)
		table.insert(self.TweenList, scoreTween)

		self.HintLabel.alpha = 0
		local hintTween = LuaTweenUtils.TweenAlpha(self.HintLabel.transform, 0, 1, 0.5)
		LuaTweenUtils.SetDelay(hintTween, 1.3)
		table.insert(self.TweenList, hintTween)
	else
		self.ExamResultSprite:SetActive(false)
		self.HintLabel.text = LuaBabyMgr.m_LandMineMsg
		self.BG_Pass:SetActive(isPass)
		self.BG_NotPass:SetActive(not isPass)
		self.ScoreLabel.text = ""
		self.LandMineView:SetActive(true)
	end
	
end

function LuaParentExamResultWnd:OnEnable()
	
end

function LuaParentExamResultWnd:OnDisable()
	if not self.TweenList then return end

	for i = 1, #self.TweenList, 1 do
		LuaTweenUtils.Kill(self.TweenList[i], false)
	end
end


return LuaParentExamResultWnd