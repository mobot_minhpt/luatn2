local DelegateFactory  = import "DelegateFactory"
local CPos             = import "L10.Engine.CPos"
local MsgPackImpl      = import "MsgPackImpl"
local CommonDefs       = import "L10.Game.CommonDefs"
local Object           = import "System.Object"
local MessageMgr       = import "L10.Game.MessageMgr"
local CItemMgr         = import "L10.Game.CItemMgr"
local EnumItemPlace    = import "L10.Game.EnumItemPlace"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CUIWidgetHSV     = import "L10.UI.CUIWidgetHSV"
local CUIManager       = import "L10.UI.CUIManager"
local UISprite         = import "UISprite"
local CUICommonDef     = import "L10.UI.CUICommonDef"
local Vector3          = import "UnityEngine.Vector3"
local CServerTimeMgr   = import "L10.Game.CServerTimeMgr"
local EnumShowGreyType = import "L10.UI.CUITexture.EnumShowGreyType"

LuaWuMenHuaShiMgr = {}

LuaWuMenHuaShiMgr.classfiedItemTbl = nil
LuaWuMenHuaShiMgr.uploadPics       = nil

LuaWuMenHuaShiMgr.depthPlus = 20
LuaWuMenHuaShiMgr.drawTime = 30
LuaWuMenHuaShiMgr.curShowPicId = 0
LuaWuMenHuaShiMgr.sexId = 1

LuaWuMenHuaShiMgr.taskId = -1
LuaWuMenHuaShiMgr.isDerivativePlot = false
LuaWuMenHuaShiMgr.isPictureShow = false
LuaWuMenHuaShiMgr.showTime = 3
LuaWuMenHuaShiMgr.taskPicData = nil
LuaWuMenHuaShiMgr.isTaskPicDataShow = false

-- 用作梦岛头像
LuaWuMenHuaShiMgr.avatarFilePath = ""

-- 抽签玩法
LuaWuMenHuaShiMgr.lotId = nil

-- 曲水流觞玩法
LuaWuMenHuaShiMgr.qslsTaskId = -1

-- 初始化数据表
function LuaWuMenHuaShiMgr:InitItemTbl()
    if self.classfiedItemTbl then return end
    local tbl = {}
    local nameIdMap = {}

    local sexType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType
    local sexChoiceList = WuMenHuaShi_XinShengHuaJi.GetData(sexType).Choice
    local sexChoice = self:Array2Tbl(sexChoiceList)

    for i = 1, #sexChoice do
        local sexId = sexChoice[i]
        tbl[sexId] = {}
        nameIdMap[sexId] = {count = 0}
    end

    WuMenHuaShi_XinShengHuaJi.Foreach(function(type, data)
        local c1 = LocalString.GetString(data.FirstCategory)
        if c1 == nil or c1 == "" then return end
        local c2 = LocalString.GetString(data.SecondCategory)
        local c3 = LocalString.GetString(data.ThirdCategory)
        local allowEmpty = tonumber(data.AllowEmpty)
        local depth = self:Array2Tbl(data.Depth)
        local affectedByColorType = self:Array2Tbl(data.AffectedByColorType)
        local needSN = data.NeedSN > 0 and true or false

        local choice = self:Array2Tbl(data.Choice)
        if choice == nil then return end

        for i = 1, #choice do
            local choiceId = choice[i]

            local itemData
            if depth == nil or #depth == 0 then
                itemData = WuMenHuaShi_ItemColor
            else
                itemData = WuMenHuaShi_XinShengHuaJiDrawItem
            end

            local sex = itemData.GetData(choiceId).Sex

            -- 建立类名与数字1,2,3之间的映射关系
            self:UpdateMap(nameIdMap[sex], c1)
            self:UpdateMap(nameIdMap[sex][c1], c2)
            self:UpdateMap(nameIdMap[sex][c1][c2], c3)

            -- 以数字1,2,3为索引建立项目表
            local id1 = self:UpdateItemTbl(tbl[sex], c1, nameIdMap[sex])
            local id2 = self:UpdateItemTbl(tbl[sex][id1], c2, nameIdMap[sex][c1])

            local subTbl = nil
            if c3 == nil or c3 == "" then
                if tbl[sex][id1][id2][0] == nil then
                    tbl[sex][id1][id2][0] = {}
                end
                subTbl = tbl[sex][id1][id2][0]
            else
                local id3 = self:UpdateItemTbl(tbl[sex][id1][id2], c3, nameIdMap[sex][c1][c2])
                subTbl = tbl[sex][id1][id2][id3]
            end

            -- 记录type值
            if subTbl.type == nil then
                subTbl.type = type
            end

            -- 记录深度值
            if depth ~= nil and subTbl.depth == nil then
                subTbl.depth = depth
            end

            -- 记录是否是颜色
            if subTbl.isColor == nil then
                if subTbl.depth ~= nil then
                    subTbl.isColor = false
                else
                    subTbl.isColor = true
                end
            end

            -- 记录typechange值
            if affectedByColorType ~= nil and subTbl.affectedByColorType == nil then
                subTbl.affectedByColorType = affectedByColorType
            end

            -- 记录NeedSN值
            if subTbl.needSN == nil then
                subTbl.needSN = needSN
            end

            -- 如果允许空，则在开头插入一个-1
            if subTbl.choice == nil then
                subTbl.choice = {}
                if allowEmpty == 1 then
                    table.insert(subTbl.choice, -1)
                end
            end
            table.insert(subTbl.choice, choiceId)
        end
    end)
    self.classfiedItemTbl = tbl
end

-- 更新映射表
function LuaWuMenHuaShiMgr:UpdateMap(tbl, id)
    if id == nil or id == "" then return end
    if tbl[id] == nil then
        tbl[id] = {count = 0, id = tbl.count + 1}
        tbl.count = tbl.count + 1
    end
end

-- 更新项目表
function LuaWuMenHuaShiMgr:UpdateItemTbl(tbl, name, categoryTbl)
    local id = categoryTbl[name].id
    if tbl[id] == nil then
        tbl[id] = {name = name}
    end
    return id
end

-- 请求打开捏脸界面
function LuaWuMenHuaShiMgr:QueryOpenXinShengHuaJiDrawWnd(data)
    if not CClientMainPlayer.Inst then return end

    local raw = MsgPackImpl.unpack(data)
    local args = raw["args"]
    self:SetDrawType(false, false, false, false)
    self.sexId = tonumber(args[0])

    local taskProperty = CClientMainPlayer.Inst.TaskProp
    local taskId = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiTaskId
    if not taskProperty:IsMainPlayerTaskFinished(taskId) then
        g_MessageMgr:ShowMessage("XinShengHuaJi_AddPicture_Need_Task")
    else
        CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiDrawWnd)
    end
end

-- 请求打开画集界面
function LuaWuMenHuaShiMgr:QueryOpenXinShengHuaJiPaintSetWnd()
    if not CClientMainPlayer.Inst then return end

    local taskProperty = CClientMainPlayer.Inst.TaskProp
    local taskId = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiTaskId
    if not taskProperty:IsMainPlayerTaskFinished(taskId) then
        g_MessageMgr:ShowMessage("XinShengHuaJi_OpenPaintSet_Need_Task")
    else
        CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiPaintSetWnd)
    end
end

-- 上传一张图像
function LuaWuMenHuaShiMgr:UploadPic(classfiedItemTbl, choiceIds, flip)
    self:UploadPic2(self:GetDrawnPicData(classfiedItemTbl, choiceIds, flip))
end

-- 从一个list中获取一个随机数
function LuaWuMenHuaShiMgr:GetRandomNumFromList(list)
    local tbl = self:Array2Tbl(list)
    local id = self:RandomNum(1, #tbl)
    return tbl[id]
end

-- 随机一个数
function LuaWuMenHuaShiMgr:RandomNum(a, b)
    if a == b then
        return a
    else
        return math.random(a, b)
    end
end

-- 添加图像成功
function LuaWuMenHuaShiMgr:AddPictureSuccess(time, data_U)
    local data = MsgPackImpl.unpack(data_U)
	if not data then
        return
    end

    self:CloseXinShengHuaJiDrawWnd()
    -- 播放dialog
    local dialogId = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSubmitDialogId
    local dialogMsg = Dialog_TaskDialog.GetData(dialogId).Dialog
    CConversationMgr.Inst:OpenStoryDialog(dialogMsg, nil)

    g_ScriptEvent:BroadcastInLua("XinShengHuaJiAddPictureSuccess", time, data)
end

-- 解析图像数据
function LuaWuMenHuaShiMgr:ParsePicData(data)
    local pic = {}
    CommonDefs.DictIterate(data, DelegateFactory.Action_object_object(function (key, value)
        local list = {}
        CommonDefs.ListIterate(value, DelegateFactory.Action_object(function (_value)
            table.insert(list, tonumber(_value))
        end))
        pic[tonumber(key)] = list[1]
    end))
    return pic
end

-- 按时间排序
function LuaWuMenHuaShiMgr:UploadPicsSort()
    table.sort(self.uploadPics, function(a, b)
        return a.time > b.time
    end)
end

-- 删除图像成功
function LuaWuMenHuaShiMgr:DelPictureSuccess(time)
    g_ScriptEvent:BroadcastInLua("XinShengHuaJiDelPictureSuccess", time)
end

-- 添加图像失败
function LuaWuMenHuaShiMgr:AddPictureFailed(reason, DetailInfo_U)
    local reason = tostring(reason)
    if reason == "UploadTimesLimit" then
        local messageformat = MessageMgr.Inst:GetMessageFormat("USE_ZHIYIHUABEN_CONFIRM", true)
        self:AddUploadTimesConfirm(messageformat)
    elseif reason == "NumLimit" then
        local messageformat = MessageMgr.Inst:GetMessageFormat("USE_MINGXINJIAN_CONFIRM", true)
        self:ExpandConfirm(messageformat)
    end
end

-- 新增状态改变成功
function LuaWuMenHuaShiMgr:ReadPictureSuccess(time)
    g_ScriptEvent:BroadcastInLua("XinShengHuaJiReadPictureSuccess", time)
end

-- 画集数量变化
function LuaWuMenHuaShiMgr:SetHuaJiNum(num)
    CClientMainPlayer.Inst.PlayProp.XinShengHuaJiNum = num
    g_ScriptEvent:BroadcastInLua("XinShengHuaJiSetHuaJiNum", num)
end

-- 消耗志异画本确认窗口
function LuaWuMenHuaShiMgr:AddUploadTimesConfirm(message)
    local itemId = WuMenHuaShi_XinShengHuaJiSetting.GetData().ZhiYiHuaBenItemId
    LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(itemId, 1, message, false, false, 
        LocalString.GetString("确定"), function (enough)
            self:OnConfirmClick(enough, itemId)
        end, nil, true)
end

-- 消耗铭心笺确认窗口
function LuaWuMenHuaShiMgr:ExpandConfirm(message)
    local itemId = WuMenHuaShi_XinShengHuaJiSetting.GetData().MingXinJianItemId
    LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(itemId, 1, message, false, false, 
        LocalString.GetString("扩容"), function (enough)
            self:OnConfirmClick(enough, itemId)
        end, nil, true)
end

-- 点击了确认按键
function LuaWuMenHuaShiMgr:OnConfirmClick(enough, itemId)
    if enough then
        local default, pos, id = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, itemId)
        if default then
            Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, pos, id, "")
        else
            LuaItemInfoMgr:CloseItemConsumeWnd()
        end
    end
end

-- 解锁部件
function LuaWuMenHuaShiMgr:UnlockSuccess(choice, sn)
    CClientMainPlayer.Inst.PlayProp.XinShengHuaJiUnlockInfo:SetBit(sn, true)
    g_ScriptEvent:BroadcastInLua("XinShengHuaJiUnlockSuccess", choice)
end

-- 解锁失败
function LuaWuMenHuaShiMgr:UnlockFail(choice, templateId, lackCount, needCount)
end


-- 关闭心生画集的绘图界面
function LuaWuMenHuaShiMgr:CloseXinShengHuaJiDrawWnd()
    CUIManager.CloseUI(CLuaUIResources.XinShengHuaJiDrawWnd)
end

-- 关闭心生画集的画集界面
function LuaWuMenHuaShiMgr:CloseXinShengHuaJiPaintSetWnd()
    CUIManager.CloseUI(CLuaUIResources.XinShengHuaJiPaintSetWnd)
end

-- 关闭心生画集的大图显示界面
function LuaWuMenHuaShiMgr:CloseXinShengHuaJiBigPicWnd()
    CUIManager.CloseUI(CLuaUIResources.XinShengHuaJiBigPicWnd)
end

-- 同步图像
function LuaWuMenHuaShiMgr:SyncPictures(data_U)
    local dict = MsgPackImpl.unpack(data_U)
	if not dict then
        return
    end

    self.uploadPics = {}
    if dict.Count > 0 then
        CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, value)
            table.insert(self.uploadPics, {time = tonumber(key), pic = self:ParsePicData(value)})
        end))
    end
    self:UploadPicsSort()

    g_ScriptEvent:BroadcastInLua("XinShengHuaJiSyncPictures")
end

-- 生成一张图像
function LuaWuMenHuaShiMgr:GeneratePic(panelTrans, partGo, id)
    local picData = self.uploadPics[id].pic
    local panel = panelTrans:GetComponent(typeof(UIPanel))

    local portraitTrans = panelTrans:Find("Portrait")
    local portraitGo = portraitTrans.gameObject
    self:GeneratePortrait(portraitGo, partGo, picData)

    local setting = WuMenHuaShi_XinShengHuaJiSetting.GetData()

    -- 设置背景
    local bgType = setting.XinShengHuaJiBgType
    local bgId = picData[bgType]
    local bgName = WuMenHuaShi_XinShengHuaJiBg.GetData(bgId).Name
    local bgCTex = panelTrans:Find("Bg"):GetComponent(typeof(CUITexture))
    local bgDepth = self:Array2Tbl(WuMenHuaShi_XinShengHuaJi.GetData(bgType).Depth)[1]
    self:SetMatAndDepth(bgCTex, bgName, bgDepth, setting.XinShengHuaJiBgPath)

    -- 计算缩放比例
    local bgSize = self:ParseXY(setting.XinShengHuaJiBgSize)[1]
    local clipRegion = panel.finalClipRegion
    local scale = self:GetScale(bgSize.x, bgSize.y, clipRegion.z, clipRegion.w)

    -- 背景缩放
    local bgTex = bgCTex.texture
    bgTex.width = bgSize.x * scale
    bgTex.height = bgSize.y * scale

    -- 设置NPC
    local npcType = setting.XinShengHuaJiNpcType
    local npcId = picData[npcType]
    local npcPic = WuMenHuaShi_RuMengJiNpc.GetData(npcId).PaintSetPicture
    local npcCTex = panelTrans:Find("Npc"):GetComponent(typeof(CUITexture))
    local npcDepth = self:Array2Tbl(WuMenHuaShi_XinShengHuaJi.GetData(npcType).Depth)[1]
    self:SetMatAndDepth(npcCTex, npcPic, npcDepth, setting.RuMengJiNpcMatPath)

    -- NPC缩放
    local npcSize = self:ParseXY(setting.XinShengHuaJiNpcSize)[1]
    local npcTex = npcCTex.texture
    npcTex.width = npcSize.x * scale
    npcTex.height = npcSize.y * scale

    -- 调整NPC和画像的位置
    local sex = picData[setting.XinShengHuaJiSexType]
    local flip = picData[setting.XinShengHuaJiFlipType] and picData[setting.XinShengHuaJiFlipType] or 0

    local modelId = picData[setting.XinShengHuaJiModelType]
    local modelData = WuMenHuaShi_XinShengHuaJiModel.GetData(modelId)
    local npcSex = WuMenHuaShi_RuMengJiNpc.GetData(npcId).Sex

    local npcPosStr = npcSex == 0 and modelData.MaleNpcPosition or modelData.FemaleNpcPosition
    local npcPos = self:ToCenterOffsetPos(self:ParseXY(npcPosStr)[sex + 1])
    LuaUtils.SetLocalPosition(npcCTex.transform, npcPos.x * scale, npcPos.y * scale, 0)

    local portraitPos = self:ToCenterOffsetPos(self:ParseXY(modelData.PortraitPosition)[sex + 1])
    local portraitSize = self:ParseXY(setting.XinShengHuaJiPortraitSize)[1]
    LuaUtils.SetLocalPosition(portraitTrans, (portraitPos.x + portraitSize.x * flip) * scale, portraitPos.y * scale, 0)
    Extensions.SetLocalRotationY(portraitTrans, 180 * flip)
    Extensions.SetPositionZ(portraitTrans, 0.1)

    -- 缩放画像
    local pScale = scale * portraitSize.x / 780
    portraitTrans.localScale = Vector3(pScale, pScale, 1)
end

-- 生成人物画像
function LuaWuMenHuaShiMgr:GeneratePortrait(portrait, part, picData, onFinishFunc)
    local sexType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType

    local partCount = 0
    for type, choiceId in pairs(picData) do
        if type >= sexType then
            local typeData = WuMenHuaShi_XinShengHuaJi.GetData(type)
            local affectedByColorType = self:Array2Tbl(typeData.AffectedByColorType)
            local depth = self:Array2Tbl(typeData.Depth)

            if depth ~= nil then
                -- 获取图像、位置和尺寸信息
                local drawItem = WuMenHuaShi_XinShengHuaJiDrawItem.GetData(choiceId)
                local pic = self:Array2Tbl(drawItem.Picture)
                local pos = self:ParseXY(drawItem.Position)
                local size = self:ParseXY(drawItem.Size)
                local isTexOrSprite = drawItem.TextureOrSprite

                for i = 1, #pic do
                    local child = nil
                    if portrait.transform.childCount > partCount then
                        child = portrait.transform:GetChild(partCount).gameObject
                    else
                        child = CUICommonDef.AddChild(portrait, part)
                    end
                    child:SetActive(true)
                    partCount = partCount + 1

                    local basicSprite = self:SetMatPosAndSize(child.transform, isTexOrSprite, pic[i], pos[i], size[i], picData[sexType] or 1,
                        onFinishFunc and DelegateFactory.Action_bool(function(isFinish)
                            if isFinish then
                                onFinishFunc(type)
                            end
                        end))
                    local depthFinal = depth[i] + self.depthPlus
                    basicSprite.depth = depthFinal

                    local cuiWidgetHSV = basicSprite.transform:GetComponent(typeof(CUIWidgetHSV))
                    cuiWidgetHSV:SetHSVOffset(0, 0, 0)

                    -- 设置颜色
                    if affectedByColorType ~= nil and affectedByColorType[i] ~= nil then
                        if affectedByColorType[i] >= sexType then
                            local colorId = picData[affectedByColorType[i]]
                            if colorId ~= nil and colorId >= 0 then
                                local data = WuMenHuaShi_ItemColor.GetData(colorId)
                                cuiWidgetHSV:SetHSVOffset(data.Hue, data.Saturation, data.Brightness)
                            end
                        end
                    end
                end
            end
        end
    end

    local childCount = portrait.transform.childCount
    if partCount < childCount then
        for i = partCount + 1, childCount do
            portrait.transform:GetChild(i - 1).gameObject:SetActive(false)
        end
    end
end

-- 计算缩放尺度
function LuaWuMenHuaShiMgr:GetScale(xFrom, yFrom, xTo, yTo)
    if xFrom <= 0 or yFrom <= 0 then return 0 end
    return math.max(xTo / xFrom, yTo / yFrom)
end

-- array转成table
function LuaWuMenHuaShiMgr:Array2Tbl(array)
    if array == nil or array.Length == 0 then return nil end
    local tbl = {}
    for i = 0, array.Length - 1 do
        table.insert(tbl, array[i])
    end
    return tbl
end

-- 解析xy
function LuaWuMenHuaShiMgr:ParseXY(str)
    if str == nil or str == "" then return nil end
    local xy = {}
    for xSign, x, ySign, y in string.gmatch(str, "(%-?)(%d+),(%-?)(%d+)") do
        x = tonumber(x)
        y = tonumber(y)
        if xSign == "-" then x = x * -1 end
        if ySign == "-" then y = y * -1 end
        table.insert(xy, CPos(x, y))
    end
    return xy
end

-- 将左上角坐标转换为距离中心点的坐标
function LuaWuMenHuaShiMgr:ToCenterOffsetPos(pos)
    local x = pos.x - 1920 / 2
    local y = pos.y - 1080 / 2
    return CPos(x, -1 * y)
end

-- 设置材料和深度 用于NPC和背景
function LuaWuMenHuaShiMgr:SetMatAndDepth(cTex, name, depth, path)
    local mat = SafeStringFormat3("%s/%s.mat", path, name)
    cTex:LoadMaterial(mat)
    cTex.texture.depth = depth + self.depthPlus
end

-- 修改材料位置和尺寸 用于人物部件
function LuaWuMenHuaShiMgr:SetMatPosAndSize(trans, isTexOrSprite, pic, pos, size, sex, onFinishFunc)
    local basicSprite = nil
    local texTrans = trans:Find("Texture")
    texTrans.gameObject:SetActive(false)
    local spriteTrans = trans:Find("Sprite")
    spriteTrans.gameObject:SetActive(false)

    if isTexOrSprite == 0 then
        local matPath = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiPicturePath
        local mat = SafeStringFormat3("%s/%s.mat", matPath, pic)
        local cTex = texTrans:GetComponent(typeof(CUITexture))
        if onFinishFunc then
            cTex:LoadMaterial(mat, EnumShowGreyType.DefaultSetting, onFinishFunc)
        else
            cTex:LoadMaterial(mat)
        end
        basicSprite = cTex.texture
    else
        basicSprite = spriteTrans:GetComponent(typeof(UISprite))
        basicSprite.spriteName = pic
    end
    LuaUtils.SetLocalPosition(basicSprite.transform, pos.x + (sex and sex or 0) * LuaWuMenHuaShiMgr.posFineTune, -1 * pos.y, 0)
    basicSprite.width = size.x
    basicSprite.height = size.y
    basicSprite.gameObject:SetActive(true)
    return basicSprite
end


-- 抽签玩法
function LuaWuMenHuaShiMgr:OpenDrawLotsWnd(data)
    local args = MsgPackImpl.unpack(data)
	if args == nil or args.Count <= 0 then return end

    self.lotId = args[0]
    CUIManager.ShowUI(CLuaUIResources.WuMenHuaShiDrawLotsWnd)
end


-- 入梦集
-- 同步半身立绘解锁状态
function LuaWuMenHuaShiMgr:SyncRuMengJiNpcStateResult(npcState_U)
    local dict = MsgPackImpl.unpack(npcState_U)
    local result = {}

	if dict and dict.Count > 0 then
        CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, value)
            result[tonumber(key)] = tonumber(value)
        end))
    end

    g_ScriptEvent:BroadcastInLua("SyncRuMengJiNpcStateResult", result)
end

-- 同步设置状态的结果
function LuaWuMenHuaShiMgr:SyncRuMengJiNpcSetState(npcChoice, state)
    g_ScriptEvent:BroadcastInLua("SyncRuMengJiNpcSetState", npcChoice, state)
end

-- 衍生剧情
-- 打开界面
function LuaWuMenHuaShiMgr:OpenXinShengHuaJiDerivativePlotWnd(taskId)
    self:SetDrawType(true, false, false, false)
    self.taskId = taskId
    self.sexId = WuMenHuaShi_DerivativePlot.GetData(taskId).Sex
    CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiDrawWnd)
end

function LuaWuMenHuaShiMgr:NeedOpenDerivativePlotWnd(eventName, taskId)
    local data = WuMenHuaShi_DerivativePlot.GetData(taskId)
    if data and data.TrackEvent == eventName then
        return true
    else
        return false
    end
end

-- 关闭界面
function LuaWuMenHuaShiMgr:CloseDerivativePlotWnd()
    self:CloseXinShengHuaJiDrawWnd()

    local taskId = self.taskId
    local derivativePlot = WuMenHuaShi_DerivativePlot.GetData(taskId)
    if not derivativePlot or not derivativePlot.FinishEvent or derivativePlot.FinishEvent == "" then
        return
    end

    local finishEvent = derivativePlot.FinishEvent
    if taskId == WuMenHuaShi_XinShengHuaJiSetting.GetData().NeedSavePictureTaskId then
        Gac2Gas.XinShengHuaJi_NotifyFlowChart("ShowHideNpc")

        local taskDialogId = 2191
        local dialogMsg = Dialog_TaskDialog.GetData(taskDialogId).Dialog
        CConversationMgr.Inst:OpenStoryDialog(dialogMsg, DelegateFactory.Action(function ()
            self:SendFinishEvent(taskId, finishEvent, false)
        end))
    else
        self:SendFinishEvent(taskId, finishEvent, true)
    end
end

-- 发送完成事件 由于FinishClientTaskEvent不会通知流程图，所以流程图无法实现断线重连，因此需要额外发送事件通知给流程图
function LuaWuMenHuaShiMgr:SendFinishEvent(taskId, event, needSend2FlowChart)
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    Gac2Gas.FinishClientTaskEvent(taskId, event, MsgPackImpl.pack(empty))
    if needSend2FlowChart then
        Gac2Gas.XinShengHuaJi_NotifyFlowChart(event)
    end
end

-- 展示捏脸界面
function LuaWuMenHuaShiMgr:ShowPictureWnd(data)
    local args = MsgPackImpl.unpack(data)
    local taskId = tonumber(args[0])
    self:SetDrawType(true, true, false, false)
    self.taskId = taskId
    self.showTime = tonumber(args[1])
    self.sexId = WuMenHuaShi_DerivativePlot.GetData(taskId).Sex
    CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiDrawWnd)
end

-- 同步添加了任务图像
function LuaWuMenHuaShiMgr:SyncAddTaskPic(taskId)
    g_ScriptEvent:BroadcastInLua("XinShengHuaJiAddTaskPic", taskId)
end

-- 展示图像
function LuaWuMenHuaShiMgr:ShowTaskPic(data_U, taskId, duration)
    local dict = MsgPackImpl.unpack(data_U)
	if not dict then
        return
    end

    self.taskPicData = self:ParsePicData(dict)
    self:SetDrawType(true, true, true, false)
    self.taskId = taskId
    self.showTime = duration
    self.sexId = WuMenHuaShi_DerivativePlot.GetData(taskId).Sex
    CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiDrawWnd)
end

-- 曲水流觞
-- 打开曲水流觞界面
function LuaWuMenHuaShiMgr:OpenQuShuiLiuShangWnd(taskId)
    self.qslsTaskId = taskId
    CUIManager.ShowUI(CLuaUIResources.QuShuiLiuShangWnd)
end

------------------- 画意共赏 ------------------

LuaWuMenHuaShiMgr.seasonInfo = {}
LuaWuMenHuaShiMgr.sharedPlayerId = nil -- 分享的玩家Id
LuaWuMenHuaShiMgr.isHYGSDraw = false
LuaWuMenHuaShiMgr.oldHYGSPicData = nil -- 原作数据
LuaWuMenHuaShiMgr.newHYGSPicData = nil -- 新作数据
LuaWuMenHuaShiMgr.posFineTune = -40 -- 女捏脸画像位置微调
LuaWuMenHuaShiMgr.allowCustomTitle = true -- 允许自定义标题 (方便Patch控制关闭游戏中的自定义内容)

function LuaWuMenHuaShiMgr:OpenHuaYiGongShangWnd()
    self.seasonInfo.id, self.seasonInfo.endTime = self:GetSeason()
    self.sharedPlayerId = nil
    if not self.seasonInfo.id then
        g_MessageMgr:ShowMessage("XinShengHuaJi_Not_In_Rank_Period")
        return
    end
    CUIManager.ShowUI(CLuaUIResources.HuaYiGongShangWnd)
end

function LuaWuMenHuaShiMgr:OpenSharedHuaYiGongShangWnd(args)
    if not args or args.Length < 1 then
		return
	end

    self.seasonInfo.id, self.seasonInfo.endTime = self:GetSeason()
    self.sharedPlayerId = tonumber(args[0])
    if not self.seasonInfo.id then
        g_MessageMgr:ShowMessage("XinShengHuaJi_Not_In_Rank_Period")
        return
    end
    CUIManager.ShowUI(CLuaUIResources.HuaYiGongShangWnd)
end

function LuaWuMenHuaShiMgr:GetCurrentTime()
    return CServerTimeMgr.Inst.timeStamp
end

function LuaWuMenHuaShiMgr:GetSeason()
    local time = self:GetCurrentTime()
    local seasonId = nil
    local seasonEndTime = nil
    WuMenHuaShi_Season.Foreach(function (id, data)
        local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.StartTime)
        local endTime =  CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
        if time > startTime and time <= endTime then
            seasonId = id
            seasonEndTime = endTime
        end
    end)
    return seasonId, seasonEndTime
end

-- 获取主题名称
function LuaWuMenHuaShiMgr:GetThemeName()
    local id = math.floor(self.seasonInfo.id / 100)
    return __WuMenHuaShi_Season_Template.GetData(id).Name
end

function LuaWuMenHuaShiMgr:QueryRankPicResult(playerId, name, pic_U, rankData_U)
    local rankData = {}
    local rawData = MsgPackImpl.unpack(rankData_U)
    if CommonDefs.IsDic(rawData) then
        CommonDefs.DictIterate(rawData, DelegateFactory.Action_object_object(function (key, value)
            rankData[tostring(key)] = value
        end))
    end

    local picData = self:ParsePicData(MsgPackImpl.unpack(pic_U))
    g_ScriptEvent:BroadcastInLua("XinShengHuaJiQueryRankPicResult", playerId, name, picData, rankData.Score and rankData.Score or 0,
        System.String.IsNullOrEmpty(rankData.Title) and LocalString.GetString("无题") or rankData.Title)
end

-- 画意共赏-打开捏脸界面
function LuaWuMenHuaShiMgr:OpenHuaYiGongShangDrawWnd(picData)
    self.oldHYGSPicData = picData
    self:SetDrawType(false, false, false, true)
    self.sexId = picData and picData[WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType] or math.random(0, 1)
    CUIManager.ShowUI(CLuaUIResources.XinShengHuaJiDrawWnd)
end

-- 设置绘制类型
function LuaWuMenHuaShiMgr:SetDrawType(isDerivativePlot, isPictureShow, isTaskPicDataShow, isHYGSDraw)
    self.isDerivativePlot = isDerivativePlot and isDerivativePlot or false
    self.isPictureShow = isPictureShow and isPictureShow or false
    self.isTaskPicDataShow = isTaskPicDataShow and isTaskPicDataShow or false
    self.isHYGSDraw = isHYGSDraw and isHYGSDraw or false
end

-- 获取绘制的图像数据
function LuaWuMenHuaShiMgr:GetDrawnPicData(classfiedItemTbl, choiceIds, flip)
    local tbl = {}
    for i1 = 1, #choiceIds do
        for i2 = 1, #choiceIds[i1] do
            local subChoice = choiceIds[i1][i2]
            local subTbl = classfiedItemTbl[i1][i2]
            if subTbl[0] ~= nil then
                if subChoice[0] >= 0 then
                    local type = subTbl[0].type
                    tbl[type] = subChoice[0]
                end
            else
                for i3 = 1, #subChoice do
                    if subChoice[i3] and subChoice[i3] >= 0 then
                        local type = subTbl[i3].type
                        tbl[type] = subChoice[i3]
                    end
                end
            end
        end
    end
    local setting = WuMenHuaShi_XinShengHuaJiSetting.GetData()
    tbl[setting.XinShengHuaJiSexType] = self.sexId
    tbl[setting.XinShengHuaJiFlipType] = flip and flip or 0
    return tbl
end

-- 上传图像
function LuaWuMenHuaShiMgr:UploadPic2(picData, title)
    local tbl = picData

    if not self.isDerivativePlot and not self.isHYGSDraw then
        local setting = WuMenHuaShi_XinShengHuaJiSetting.GetData()
        -- 背景
        local bgType = setting.XinShengHuaJiBgType
        local bgChoice = WuMenHuaShi_XinShengHuaJi.GetData(bgType).Choice
        tbl[bgType] = self:GetRandomNumFromList(bgChoice)

        -- NPC
        local npcType = setting.XinShengHuaJiNpcType
        local npcChoice = WuMenHuaShi_XinShengHuaJi.GetData(npcType).Choice
        tbl[npcType] = self:GetRandomNumFromList(npcChoice)

        -- 模板
        local modelType = setting.XinShengHuaJiModelType
        local modelChoice = WuMenHuaShi_XinShengHuaJi.GetData(modelType).Choice
        tbl[modelType] = self:GetRandomNumFromList(modelChoice)

        -- 已读
        tbl[setting.XinShengHuaJiReadType] = 0
    end

    local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
    for k, v in pairs(tbl) do
        local list = CreateFromClass(MakeGenericClass(List, Object))
        CommonDefs.ListAdd(list, typeof(UInt32), v)
        CommonDefs.DictAdd(dict, typeof(String), tostring(k), typeof(Object), list)
    end

    if self.isDerivativePlot then
        Gac2Gas.XinShengHuaJi_AddTaskPic(self.taskId, MsgPackImpl.pack(dict))
    elseif self.isHYGSDraw then
        Gac2Gas.XinShengHuaJi_AddRankPic(MsgPackImpl.pack(dict), title)
    else
        Gac2Gas.XinShengHuaJi_AddPicture(MsgPackImpl.pack(dict))
    end
end

-- 投票结果
function LuaWuMenHuaShiMgr:VoteResult(playerId, voteNum)
    g_ScriptEvent:BroadcastInLua("XinShengHuaJiVoteResult", playerId, voteNum)
end

-- 上传图像结果
function LuaWuMenHuaShiMgr:AddRankPicResult(bSuccess)
    if bSuccess then
        g_ScriptEvent:BroadcastInLua("XinShengHuaJiAddRankPicSuccess")
    end
end

-- 生成带翻转的画像
function LuaWuMenHuaShiMgr:GeneratePortraitWithFlip(portrait, part, picData, normalX, onFinishFunc)
    self:GeneratePortrait(portrait, part, picData, onFinishFunc)
    local flipType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiFlipType
    local flip = picData[flipType] and picData[flipType] or 0

    Extensions.SetLocalRotationY(portrait.transform, 180 * flip)
    local scale = portrait.transform.localScale.x
    Extensions.SetLocalPositionX(portrait.transform, normalX + flip * scale * 780)
    Extensions.SetPositionZ(portrait.transform, 0.1)
end
