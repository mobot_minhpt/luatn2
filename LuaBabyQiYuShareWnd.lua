local ShareMgr=import "ShareMgr"
local CUITexture = import "L10.UI.CUITexture"
local BabyMgr = import "L10.Game.BabyMgr"
local Baby_QiYu = import "L10.Game.Baby_QiYu"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local System = import "System"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local ETickType = import "L10.Engine.ETickType"
local EShareType = import "L10.UI.EShareType"
local CFileTools = import "CFileTools"
local CTickMgr = import "L10.Engine.CTickMgr"
local Main = import "L10.Engine.Main"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Texture = import "UnityEngine.Texture"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local CJingLingWebImageMgr = import "L10.Game.CJingLingWebImageMgr"
local Application = import "UnityEngine.Application"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaBabyQiYuShareWnd = class()

RegistClassMember(LuaBabyQiYuShareWnd, "QiYuPic")

RegistClassMember(LuaBabyQiYuShareWnd, "TimeLabel")
RegistClassMember(LuaBabyQiYuShareWnd, "DescLabel")
RegistClassMember(LuaBabyQiYuShareWnd, "GiftButton")
RegistClassMember(LuaBabyQiYuShareWnd, "ShareDest")
RegistClassMember(LuaBabyQiYuShareWnd, "ShareButton")
RegistClassMember(LuaBabyQiYuShareWnd, "ShareMengDao")
RegistClassMember(LuaBabyQiYuShareWnd, "SharePingTai")

-- data --
RegistClassMember(LuaBabyQiYuShareWnd, "SelectedBaby")
RegistClassMember(LuaBabyQiYuShareWnd, "PicCoroutine")

function LuaBabyQiYuShareWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyQiYuShareWnd:InitClassMembers()
	self.QiYuPic = self.transform:Find("Anchor/QiYuPic"):GetComponent(typeof(CUITexture))
	self.TimeLabel = self.transform:Find("Anchor/Bottom/TimeLabel"):GetComponent(typeof(UILabel))
	self.DescLabel = self.transform:Find("Anchor/Bottom/DescLabel"):GetComponent(typeof(UILabel))
	self.GiftButton = self.transform:Find("Anchor/Bottom/GiftButton"):GetComponent(typeof(CButton))
	self.ShareButton = self.transform:Find("Anchor/Bottom/ShareButton").gameObject
	if CommonDefs.IS_VN_CLIENT then
		self.ShareButton:SetActive(false)
	end

	self.ShareDest = self.transform:Find("Anchor/Bottom/ShareDest").gameObject
	self.ShareDest:SetActive(false)
	self.ShareMengDao = self.transform:Find("Anchor/Bottom/ShareDest/ShareMengDao").gameObject
	self.SharePingTai = self.transform:Find("Anchor/Bottom/ShareDest/SharePingTai").gameObject

end

function LuaBabyQiYuShareWnd:InitValues()

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyQiYuShareWnd)
		return
	end
	self.SelectedBaby = selectedBaby

	local onGiftButtonClicked = function (go)
		self:OnGiftButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.GiftButton.gameObject, DelegateFactory.Action_GameObject(onGiftButtonClicked), false)

	local onShareButtonClicked = function (go)
		if CommonDefs.IS_HMT_CLIENT then
			self:OnShareMengDaoClicked(go)
		else
			self:OnShareButtonClicked(go)
		end
	end
	CommonDefs.AddOnClickListener(self.ShareButton, DelegateFactory.Action_GameObject(onShareButtonClicked), false)

	local onShareMengDaoClicked = function (go)
		self:OnShareMengDaoClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ShareMengDao, DelegateFactory.Action_GameObject(onShareMengDaoClicked), false)

	local onSharePingTaiClicked = function (go)
		self:OnSharePingTaiClicked()
	end
	CommonDefs.AddOnClickListener(self.SharePingTai, DelegateFactory.Action_GameObject(onSharePingTaiClicked), false)

	self:UpdateImage()
	self:UpdateQiYuShare()

end

function LuaBabyQiYuShareWnd:UpdateImage()
	local qiyu = Baby_QiYu.GetData(LuaBabyMgr.m_SelectedQiYuId)
	if not qiyu then return end
	
	local picPath = qiyu.MalePicPathShare
    if self.SelectedBaby.Gender == 1 then
       	picPath = qiyu.FemalePicPathShare
    end
	self.QiYuPic:Clear()
    self:DownLoadPic(self, picPath, self.QiYuPic.texture)
end

function LuaBabyQiYuShareWnd:UpdateQiYuShare()
	local qiyu = Baby_QiYu.GetData(LuaBabyMgr.m_SelectedQiYuId)
	if not qiyu then return end

    self.TimeLabel.text = ""
	self.DescLabel.text = qiyu.Description

	local qiyuDict = self.SelectedBaby.Props.ScheduleData.QiYuData
	if not qiyuDict then return end

	local isGifted = false
    if CommonDefs.DictContains_LuaCall(qiyuDict, LuaBabyMgr.m_SelectedQiYuId) then
    	local qiyuInfo = qiyuDict[LuaBabyMgr.m_SelectedQiYuId]
       	isGifted = qiyuInfo.ReceivedAward == 0
       	self.TimeLabel.text = CServerTimeMgr.ConvertTimeStampToZone8Time(qiyuInfo.FinishTime):ToString("yyyy-MM-dd")
    end

    self.GiftButton.Enabled = isGifted
end

function LuaBabyQiYuShareWnd:DownLoadPic(go, path, uiTexture)
	path = ShareMgr. GetWebImagePath(path)
	Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(path, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
		if not go then return end
        local _texture = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGBA32, false)
		CommonDefs.LoadImage(_texture,bytes)
        local texture = TypeAs(_texture, typeof(Texture))
        uiTexture.mainTexture = texture
    end)))
end

function LuaBabyQiYuShareWnd:OnGiftButtonClicked(go)
	if not self.SelectedBaby then return end
	Gac2Gas.RequestReceiveQiYuAward(self.SelectedBaby.Id, LuaBabyMgr.m_SelectedQiYuId)
end

function LuaBabyQiYuShareWnd:OnShareButtonClicked(go)
	self.ShareDest:SetActive(not self.ShareDest.activeSelf)
end

function LuaBabyQiYuShareWnd:OnShareMengDaoClicked(go)
	local qiyu = Baby_QiYu.GetData(LuaBabyMgr.m_SelectedQiYuId)
	if not qiyu then return end
	
	local picPath = qiyu.MalePicPathShare
    if self.SelectedBaby.Gender == 1 then
       	picPath = qiyu.FemalePicPathShare
    end
	local url = ShareMgr.GetWebImagePath(picPath)
	ShareMgr.DownloadImageWebImage2Share(url,DelegateFactory.Action(function ()
        local fileName = CJingLingWebImageMgr.Inst:GetFileNameByUrl(url)
        local filePath = Application.temporaryCachePath .. fileName
        ShareMgr.Share2PersonalSpaceFile = filePath
        ShareMgr.Share2PersonalSpaceUrl = ""
        ShareMgr.CurShare2PersonalSpaceImageType = ShareMgr.EnumShare2PersonalSpaceImageType.Local
        ShareMgr.onShareSuccess = DelegateFactory.Action(function ()
            if CClientMainPlayer.Inst ~= nil then
                CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(CClientMainPlayer.Inst.Id, 0)
            end
        end)
        CUIManager.ShowUI(CUIResources.Share2PersonalSpaceWnd)
    end))
end

function LuaBabyQiYuShareWnd:OnSharePingTaiClicked(go)
	local qiyu = Baby_QiYu.GetData(LuaBabyMgr.m_SelectedQiYuId)
	if not qiyu then return end
	
	local picPath = qiyu.MalePicPathShare
    if self.SelectedBaby.Gender == 1 then
       	picPath = qiyu.FemalePicPathShare
    end
    self:ShareQiYuImage(picPath)

end

function LuaBabyQiYuShareWnd:ShareQiYuImage(imageUrl)
	if self.PicCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.PicCoroutine)
        self.PicCoroutine = nil
    end
	imageUrl = ShareMgr.GetWebImagePath(imageUrl)
    self.PicCoroutine = Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(imageUrl, DelegateFactory.Action_bool_byte_Array(function (picResult, picData) 
        local filename = Utility.persistentDataPath .. "/BabyQiYuShare.jpg"
        System.IO.File.WriteAllBytes(filename, picData)
        CFileTools.SetFileNoBackup(filename)

        CTickMgr.Register(DelegateFactory.Action(function () 
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, {filename})
        end), 1000, ETickType.Once)
    end)))

end

function LuaBabyQiYuShareWnd:ReceiveQiYuAwardSuccess(babyId)
	if self.SelectedBaby.Id == babyId then
		self:UpdateQiYuShare()
	end
end

function LuaBabyQiYuShareWnd:OnBabyInfoDelete()
	CUIManager.CloseUI(CLuaUIResources.BabyQiYuShareWnd)
end


function LuaBabyQiYuShareWnd:OnEnable()
	g_ScriptEvent:AddListener("ReceiveQiYuAwardSuccess", self, "ReceiveQiYuAwardSuccess")
	g_ScriptEvent:AddListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

function LuaBabyQiYuShareWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ReceiveQiYuAwardSuccess", self, "ReceiveQiYuAwardSuccess")
	g_ScriptEvent:RemoveListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

return LuaBabyQiYuShareWnd
