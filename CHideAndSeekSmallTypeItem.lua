-- Auto Generated!!
local CHideAndSeekSmallTypeItem = import "L10.UI.CHideAndSeekSmallTypeItem"
local Item_Item = import "L10.Game.Item_Item"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
CHideAndSeekSmallTypeItem.m_Init_CS2LuaHook = function (this, typeId) 

    local data = Zhuangshiwu_Zhuangshiwu.GetData(typeId)
    if data == nil then
        return
    end
    this.TemplateId = typeId

    if data.Type ~= EnumFurnitureType_lua.eJianzhu then
        local itemtid = data.ItemId
        local itemdata = Item_Item.GetData(itemtid)
        if itemdata == nil then
            return
        end
        if this.iconTexture ~= nil then
            this.iconTexture:LoadMaterial(itemdata.Icon)
        end
    else
        this.iconTexture:LoadMaterial(CHideAndSeekSmallTypeItem.GetJianzhuIcon(typeId))
    end
    this.SelectedSprite:SetActive(false)
end
CHideAndSeekSmallTypeItem.m_GetJianzhuIcon_CS2LuaHook = function (templateId) 
    local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    if data == nil then
        return ""
    end
    return data.Icon
end
