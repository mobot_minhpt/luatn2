--拨浪鼓
local CUITexture=import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Flip = import "UIBasicSprite+Flip"
local Screen=import "UnityEngine.Screen"
local MsgPackImpl=import "MsgPackImpl"
local Object=import "System.Object"
local SoundManager = import "SoundManager"
local PlayerSettings=import "L10.Game.PlayerSettings"

CLuaBoLangGuWnd=class()
RegistClassMember(CLuaBoLangGuWnd,"m_Rotation")
RegistClassMember(CLuaBoLangGuWnd,"m_Arrow")
RegistClassMember(CLuaBoLangGuWnd,"m_Finger")
RegistClassMember(CLuaBoLangGuWnd,"m_SoundText")
RegistClassMember(CLuaBoLangGuWnd,"m_Tick")
RegistClassMember(CLuaBoLangGuWnd,"m_Tick0")
RegistClassMember(CLuaBoLangGuWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaBoLangGuWnd, "m_ModelTexture")
RegistClassMember(CLuaBoLangGuWnd, "m_RO")
RegistClassMember(CLuaBoLangGuWnd, "m_IsPlaying")
RegistClassMember(CLuaBoLangGuWnd, "m_IsPressing")
RegistClassMember(CLuaBoLangGuWnd, "m_Ani")
RegistClassMember(CLuaBoLangGuWnd,"m_Tick1")
RegistClassMember(CLuaBoLangGuWnd,"m_EndTick")
RegistClassMember(CLuaBoLangGuWnd,"m_SoundTicks")
RegistClassMember(CLuaBoLangGuWnd,"m_SoundEvents")

CLuaBoLangGuWnd.m_Key = nil
function CLuaBoLangGuWnd:Init()
    self.m_IsPlaying = false

    self.m_Arrow = self.transform:Find("Anchor/Arrow").gameObject
    self.m_Finger = self.transform:Find("Anchor/Finger").gameObject
    self.m_SoundText = self.transform:Find("Anchor/SoundText"):GetComponent(typeof(CUIFx))
    self.m_Rotation = 0
    
    self:Play()
    self.m_Tick = RegisterTick(function()
        self:Play()
    end,2500)

    
    self.m_ModelTexture = FindChild(self.transform,"ModelTexture"):GetComponent(typeof(CUITexture))

    UIEventListener.Get(self.m_ModelTexture.gameObject).onPress = DelegateFactory.BoolDelegate(function(g,b)
        self.m_IsPressing = b
    end)

    UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
        self.m_Rotation = self.m_Rotation - delta.x/Screen.width*360

        CUIManager.SetModelRotation("__NpcPreview__", self.m_Rotation)

        if self.m_RO then
            local ani = nil
            if self.m_Rotation>30 then
                ani = "shakeright01"
            elseif self.m_Rotation<-30 then
                ani = "shakeleft01"
            end
            if not self.m_IsPlaying and ani~=self.m_Ani and ani then
                self.m_Ani = ani
                self.m_RO:DoAni(ani,false,0,1,0.15,true,0.8)
                self.m_IsPlaying = true
                self:PlaySound()

                if ani == "shakeright01" then
                    self.m_Tick1 = RegisterTickOnce(function()
                        self.m_RO:DoAni("shake01",true,0,0.8,0.2,true,1)
                        self:FinishEvent()
                        self.m_EndTick = RegisterTickOnce(function()
                            self:OnEnd()
                        end, 5000)
                    end,667)
                else
                    self.m_Tick1 = RegisterTickOnce(function()
                        self.m_RO:DoAni("shake01",true,0,1,0.2,true,1)
                        self:FinishEvent()
                        self.m_EndTick = RegisterTickOnce(function()
                            self:OnEnd()
                        end, 5000)
                    end,533)
                end

                if self.m_Tick then
                    UnRegisterTick(self.m_Tick)
                    self.m_Tick=nil
                end
                if self.m_Tick0 then
                    UnRegisterTick(self.m_Tick0)
                    self.m_Tick0=nil
                end

                self.m_SoundText.gameObject:SetActive(true)
            elseif self.m_IsPlaying and ani~=self.m_Ani then
                
            end
        end
    end)
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        ro.Scale = 7
        self.m_RO = ro
        ro:LoadMain("Character/NPC/lnpc627/Prefab/lnpc627_01.prefab",nil,false,false)
    end)
    self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__NpcPreview__", self.m_ModelTextureLoader,155,0.05,-1,4.66,false,true,1,false)
end

function CLuaBoLangGuWnd:PlaySound()
    if PlayerSettings.MusicEnabled then
        self.m_SoundEvents = {}
        self.m_SoundTicks = {}
        for i = 0, 3, 1 do
            local new_tick = RegisterTickOnce(function()
                local evt = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/UI_BoLangGu",Vector3.zero, nil, 0)
                table.insert( self.m_SoundEvents,evt )
            end,1000 * i + 10)
            table.insert(self.m_SoundTicks, new_tick)
        end
    end
end

function CLuaBoLangGuWnd:FinishEvent()
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    empty = MsgPackImpl.pack(empty)
    if CLuaBoLangGuWnd.m_Key == "BoLangGu1" then
        Gac2Gas.FinishClientTaskEvent(22203814,CLuaBoLangGuWnd.m_Key,empty)
    elseif CLuaBoLangGuWnd.m_Key == "BoLangGu2" then
        Gac2Gas.FinishClientTaskEvent(22203848,CLuaBoLangGuWnd.m_Key,empty)
    elseif CLuaBoLangGuWnd.m_Key == "BoLangGu3" then
        Gac2Gas.FinishClientTaskEvent(22203866,CLuaBoLangGuWnd.m_Key,empty)
    elseif CLuaBoLangGuWnd.m_Key == "BoLangGu4" then
        Gac2Gas.FinishClientTaskEvent(22111202,CLuaBoLangGuWnd.m_Key,empty)
    elseif CLuaBoLangGuWnd.m_Key == "BoLangGu5" then
        Gac2Gas.FinishClientTaskEvent(22111203,CLuaBoLangGuWnd.m_Key,empty)
    elseif CLuaBoLangGuWnd.m_Key == "BoLangGu7" then
        Gac2Gas.FinishClientTaskEvent(22207165,CLuaBoLangGuWnd.m_Key,empty)
    end
end

function CLuaBoLangGuWnd:OnEnd()
    if self.m_EndTick then
        UnRegisterTick(self.m_EndTick)
        self.m_EndTick=nil
    end

    CUIManager.CloseUI(CLuaUIResources.BoLangGuWnd)
end

function CLuaBoLangGuWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
    if self.m_Tick0 then
        UnRegisterTick(self.m_Tick0)
        self.m_Tick0=nil
    end
    if self.m_Tick1 then
        UnRegisterTick(self.m_Tick1)
        self.m_Tick1=nil
    end
    if self.m_EndTick then
        UnRegisterTick(self.m_EndTick)
        self.m_EndTick=nil
    end
    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__NpcPreview__")

    if self.m_SoundTicks then
        for k, v in ipairs(self.m_SoundTicks)do
            UnRegisterTick(v)
        end
        for i,v in ipairs(self.m_SoundEvents) do
            SoundManager.Inst:StopSound(v)
        end
        self.m_SoundTicks=nil
    end
end

function CLuaBoLangGuWnd:Play()
    --手指移动
    self.m_Arrow:GetComponent(typeof(UISprite)).flip = Flip.Vertically
    LuaTweenUtils.TweenAlpha(self.m_Finger.transform,0,1,0.2)
    LuaTweenUtils.TweenAlpha(self.m_Arrow.transform,0,1,0.2)

    LuaUtils.SetLocalPosition(self.m_Finger.transform,324,6,0)
    LuaTweenUtils.TweenPosition(self.m_Finger.transform,65,-70,0,0.5)

    --小时
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(self.m_Finger.transform,1,0,0.3),1.2)

    --箭头反方向
    if self.m_Tick0 then
        UnRegisterTick(self.m_Tick0)
    end
    self.m_Tick0 = RegisterTickOnce(function()
        self.m_Arrow:GetComponent(typeof(UISprite)).flip = Flip.Nothing
    end,700)

    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenPosition(self.m_Finger.transform,324,6,0,0.5),0.7)
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(self.m_Finger.transform,1,0,0.3),1.2)
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(self.m_Arrow.transform,1,0,0.3),1.2)
end
