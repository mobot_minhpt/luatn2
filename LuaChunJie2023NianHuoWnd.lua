local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local NGUIText = import "NGUIText"
local Color = import "UnityEngine.Color"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaChunJie2023NianHuoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023NianHuoWnd, "NPC_unlock", GameObject)
RegistChildComponent(LuaChunJie2023NianHuoWnd, "TaskLst", GameObject)

RegistClassMember(LuaChunJie2023NianHuoWnd, "unlock_List")
RegistClassMember(LuaChunJie2023NianHuoWnd, "task_List")
RegistClassMember(LuaChunJie2023NianHuoWnd, "taskName_List")
RegistClassMember(LuaChunJie2023NianHuoWnd, "taskOpenTime_List")
--@endregion RegistChildComponent end

function LuaChunJie2023NianHuoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaChunJie2023NianHuoWnd:Init()
    self.unlock_List = {}
    for i = 0, self.NPC_unlock.transform.childCount - 1 do
        table.insert(self.unlock_List, self.NPC_unlock.transform:GetChild(i))
    end
    self.task_List = {}
    self.taskName_List = {}
    for i = 0, self.TaskLst.transform.childCount - 1 do
        local taskInfo = self.TaskLst.transform:GetChild(i)
        local title = taskInfo:GetComponent(typeof(UILabel)).text
        local taskName = string.match(title, LocalString.GetString("寻回.+")) or title
        table.insert(self.task_List, taskInfo)
        table.insert(self.taskName_List, taskName)
    end
    self.taskOpenTime_List = ChunJie_ZJNHSetting.GetData().TaskOpenTimeList
    self:InitFinalReward()
    self:InitNPC()
    self:InitTask()
end

function LuaChunJie2023NianHuoWnd:InitFinalReward()
    local item = self.task_List[#self.task_List]:Find("Sprite")
    local itemID = ChunJie_ZJNHSetting.GetData().FinalRewardItemId
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then item:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(ItemData.Icon) end
    CommonDefs.AddOnClickListener(
        item.gameObject,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaChunJie2023NianHuoWnd:InitNPC()
    for i = 1, #self.unlock_List do
        local isFinish = LuaChunJie2023Mgr.ZZNH_TaskLst[i].isFinish
        self.unlock_List[i].gameObject:SetActive(isFinish)
    end
end

function LuaChunJie2023NianHuoWnd:InitTask()
    for i = 1, math.min(#self.task_List, #LuaChunJie2023Mgr.ZZNH_TaskLst) do
        local taskId = LuaChunJie2023Mgr.ZZNH_TaskLst[i].firstTaskId
        local isOpen = LuaChunJie2023Mgr.ZZNH_TaskLst[i].isOpen
        local isFinish = LuaChunJie2023Mgr.ZZNH_TaskLst[i].isFinish
        local instance = self.task_List[i]
        instance:Find("Sprite/mask").gameObject:SetActive(isFinish)
        if isFinish then
            -- 任务链已完成
            self:InitTaskTitle(instance, SafeStringFormat3(LocalString.GetString("已%s"), self.taskName_List[i]), 40, NGUIText.ParseColor24("ffcb8e", 0), false)
        elseif not isOpen then
            -- 时间未到
            self:InitTaskTitle(instance, self.taskName_List[i], 40, Color.white, false)
            self:ShowTaskStateLable(instance, SafeStringFormat3(LocalString.GetString("%s开启"), self.taskOpenTime_List[i-1]), false)
        elseif i > 1 and not LuaChunJie2023Mgr.ZZNH_TaskLst[i-1].isFinish then
            -- 存在前置任务链且未完成
            self:InitTaskTitle(instance, self.taskName_List[i], 40, Color.white, false)
            self:ShowTaskStateLable(instance, SafeStringFormat3(LocalString.GetString("需要%s"), self.taskName_List[i - 1]), false)
        elseif isOpen then
            -- 可领取
            self:InitTaskTitle(instance, self.taskName_List[i], 46, Color.white, true)
            local hasReceived = CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), taskId)
            local hasFinished = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskId)
            self:ShowGetTaskBtn(instance, not hasReceived and not hasFinished, taskId)
        end
    end
end

function LuaChunJie2023NianHuoWnd:InitTaskTitle(instance, titleStr, size, color, highLight)
    local label = instance:GetComponent(typeof(UILabel))
    label.text = titleStr
    label.fontSize = size
    label.color = color
    instance:Find("Normal").gameObject:SetActive(not highLight)
    instance:Find("HighLight").gameObject:SetActive(highLight)
    instance:Find("Button").gameObject:SetActive(false)
    instance:Find("StateLb").gameObject:SetActive(false)
    local label = instance:Find("Label")
    if label then label.gameObject:SetActive(false) end
end

function LuaChunJie2023NianHuoWnd:ShowGetTaskBtn(instance, canClick, taskId)
    local label = instance:Find("Label")
    if label then label.gameObject:SetActive(false) end
    instance:Find("StateLb").gameObject:SetActive(false)
    local btn = instance:Find("Button"):GetComponent(typeof(CButton))
    btn.gameObject:SetActive(true)
    if canClick then
        btn.Enabled = true
        btn.label.text = LocalString.GetString("领取任务")
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnGetTaskBtnClick(taskId)
        end)
    else
        btn.Enabled = false
        btn.label.text = LocalString.GetString("任务进行中")
    end
end

function LuaChunJie2023NianHuoWnd:ShowTaskStateLable(instance, statusStr)
    instance:Find("Button").gameObject:SetActive(false)
    local label = instance:Find("Label")
    if label then label.gameObject:SetActive(true) end
    local StateLb = instance:Find("StateLb"):GetComponent(typeof(UILabel))
    StateLb.gameObject:SetActive(true)
    StateLb.text = statusStr
end

function LuaChunJie2023NianHuoWnd:OnUpdateTask(args)
    local taskId = args[0]
    if LuaChunJie2023Mgr:IsZZNHTaskId(taskId) then
        LuaChunJie2023Mgr:OnUpdateZJNHTaskStatus()
        self:InitTask()
    end
end

--@region UIEvent
function LuaChunJie2023NianHuoWnd:OnGetTaskBtnClick(taskId)
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.Level < 15 then
            g_MessageMgr:ShowMessage("ChunJie2023_ZJNH_LEVEL_NOT_ENOUGH")
        else
            Gac2Gas.Spring2023ZJNH_AcceptTask(taskId)
        end
    end
end

--@endregion UIEvent
function LuaChunJie2023NianHuoWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTask", self, "OnUpdateTask")
end

function LuaChunJie2023NianHuoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTask", self, "OnUpdateTask")
end