local UISprite = import "UISprite"
local LocalString = import "LocalString"
local CommonDefs = import "L10.Game.CommonDefs"
local UIWidget = import "UIWidget"
local Vector3 = import "UnityEngine.Vector3"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CBaseWnd = import "L10.UI.CBaseWnd"
local NGUIMath = import "NGUIMath"

LuaAchievementAwardsTipWnd = class()
RegistChildComponent(LuaAchievementAwardsTipWnd, "m_Bg", "Background", UIWidget)
RegistChildComponent(LuaAchievementAwardsTipWnd, "m_ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaAchievementAwardsTipWnd, "m_Table", "Table", UITable)
RegistChildComponent(LuaAchievementAwardsTipWnd, "m_Bottom", "Bottom", GameObject)
RegistChildComponent(LuaAchievementAwardsTipWnd, "m_ViewButton", "ViewButton", GameObject)

RegistClassMember(LuaAchievementAwardsTipWnd, "m_BottomHeight")

function LuaAchievementAwardsTipWnd:Awake()
    self.m_ItemTemplate:SetActive(false)
    self.m_BottomHeight = self.m_Bg.height + self.m_Bottom.transform.localPosition.y --需要确保m_Bottom默认可见
    CommonDefs.AddOnClickListener(self.m_ViewButton, DelegateFactory.Action_GameObject(function(go) self:OnViewButtonClick(go) end), false)
end

function LuaAchievementAwardsTipWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaAchievementAwardsTipWnd:OnViewButtonClick(go)
    LuaAchievementMgr:ShowDetailWndByAchievementId(LuaAchievementMgr.m_CurAchievementId)
    self:Close()
end

function LuaAchievementAwardsTipWnd:Init()

    local achievementId = LuaAchievementMgr.m_CurAchievementId
    local achievement = Achievement_Achievement.GetData(achievementId)

    local dataTbl = {}
    table.insert(dataTbl, {icon="packagewnd_yuanbao", value=achievement.YuanBaoReward}) --元宝必有
    if achievement.TitleReward>0 then
        table.insert(dataTbl, {icon="packagewnd_chenghao", value=Title_Title.GetData(achievement.TitleReward).Name}) --称号
    end
    if achievement.SilverReward>0 then
        table.insert(dataTbl, {icon="packagewnd_yinliang", value=achievement.SilverReward}) --银两
    end
    if achievement.FreeSilverReward>0 then
        table.insert(dataTbl, {icon="packagewnd_yinpiao", value=achievement.FreeSilverReward}) --银票
    end
    if achievement.ExpReward>0 then
        table.insert(dataTbl, {icon="common_player_exp", value=achievement.ExpReward}) --经验
    end
    if achievement.YueLiReward>0 then
        table.insert(dataTbl, {icon="talismanfxexchangewnd_dou", value=achievement.YueLiReward}) --阅历积分
    end

    Extensions.RemoveAllChildren(self.m_Table.transform)

    for i,data in pairs(dataTbl) do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
        child:SetActive(true)
        child:GetComponent(typeof(UILabel)).text = tostring(data.value)
        child.transform:Find("Icon"):GetComponent(typeof(UISprite)).spriteName = data.icon
        child.transform:Find("Split"):GetComponent(typeof(UISprite)).alpha = (i~=#dataTbl) and 1 or 0.01
    end
    self.m_Table:Reposition()

    local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_Table.transform)
    local bgHeight = - self.m_Table.transform.localPosition.y + b.size.y
    if LuaAchievementMgr.m_ShowViewButton then
        self.m_Bottom:SetActive(true)
        bgHeight = bgHeight + self.m_BottomHeight
    else
        self.m_Bottom:SetActive(false)
        bgHeight = bgHeight + 20 --为了美观加点间隙
    end
    self.m_Bg.height = bgHeight

    self:Layout()
end

function LuaAchievementAwardsTipWnd:Layout()
    local centerNGUIPos = CUICommonDef.AdjustPosition(CPopupMenuInfoMgrAlignType.Right, self.m_Bg.width, self.m_Bg.height,
        LuaAchievementMgr.m_AwardsTipTargetWorldPos, LuaAchievementMgr.m_AwardsTipTargetWidth, LuaAchievementMgr.m_AwardsTipTargetHeight)

    local v = self.m_Bg.transform.localPosition
    self.m_Bg.transform.localPosition = Vector3(centerNGUIPos.x, centerNGUIPos.y+self.m_Bg.height*0.5, v.z)
end
