local CScene = import "L10.Game.CScene"

LuaWuYi2023Mgr = {}
LuaWuYi2023Mgr.m_TempSkillWndCloseTime = 0
LuaWuYi2023Mgr.m_SelfForce = 0
LuaWuYi2023Mgr.m_PlayResultInfo = {}
LuaWuYi2023Mgr.m_ImagePaths = nil
LuaWuYi2023Mgr.m_Msgs = nil
LuaWuYi2023Mgr.m_GameInfo = {}

function LuaWuYi2023Mgr:GetCommonImageRuleData()
    if self.m_ImagePaths == nil then
        self.m_ImagePaths = {}
        for i = 0, WuYi2023_FenYongZhenXian.GetData().RuleImagePaths.Length - 1 do
            table.insert(self.m_ImagePaths, WuYi2023_FenYongZhenXian.GetData().RuleImagePaths[i])
        end
    end
    if self.m_Msgs == nil then
        self.m_Msgs = {}
        for i = 0, WuYi2023_FenYongZhenXian.GetData().RuleMsgs.Length - 1 do
            table.insert(self.m_Msgs, g_MessageMgr:FormatMessage(WuYi2023_FenYongZhenXian.GetData().RuleMsgs[i]))
        end
    end
    return self.m_ImagePaths, self.m_Msgs
end

function LuaWuYi2023Mgr:ShowCommonImageRuleWnd()
    local imagePaths, msgs = self:GetCommonImageRuleData()
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

function LuaWuYi2023Mgr:IsInFYZXPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == WuYi2023_FenYongZhenXian.GetData().GamePlayId then
            return true
        end
    end
    return false
end

function LuaWuYi2023Mgr:OnMainPlayerCreated(playId)
    if self:IsInFYZXPlay() then
        EventManager.BroadcastInternalForLua(EnumEventType.AcceptTask,{1})
    end
end

function LuaWuYi2023Mgr:OnMainPlayerDestroyed()
end

--rpc

function LuaWuYi2023Mgr:FenYongZhenXianShowRuleWnd()
    self:ShowCommonImageRuleWnd()
end

function LuaWuYi2023Mgr:FenYongZhenXianSyncGameInfo(selfForce, gameInfoU)
    self.m_SelfForce = 0
    local gameInfo = g_MessagePack.unpack(gameInfoU)
    self.m_GameInfo = {{},{}}
    for i = 1, #gameInfo, 5 do
        local force, score, remain, usetime, totalMeters = gameInfo[i], gameInfo[i + 1], gameInfo[i + 2], gameInfo[i + 3], gameInfo[i + 4]
        if force then
            self.m_GameInfo[force] = {score = score, remain = remain, usetime = usetime, totalMeters = totalMeters}
        end
    end
    g_ScriptEvent:BroadcastInLua("OnFenYongZhenXianSyncGameInfo", selfForce, gameInfo)
end

function LuaWuYi2023Mgr:FenYongZhenXianHorseKilled(attackerId, attackerForce, attackerClass, attackerGender)
    g_ScriptEvent:BroadcastInLua("OnFenYongZhenXianHorseKilled",attackerId, attackerForce, attackerClass, attackerGender)
end

function LuaWuYi2023Mgr:FenYongZhenXianHorseCountDown(engineId, horseRebornTime, horseForce)
    g_ScriptEvent:BroadcastInLua("OnFenYongZhenXianHorseCountDown", engineId, horseRebornTime, horseForce)
end

function LuaWuYi2023Mgr:FenYongZhenXianPlayerKilled(attackerId, attackerForce, attackerClass, attackerGender, defenderId, defenderForce, defenderClass, defenderGender)
    g_ScriptEvent:BroadcastInLua("OnFenYongZhenXianPlayerKilled", attackerId, attackerForce, attackerClass, attackerGender, defenderId, defenderForce, defenderClass, defenderGender)
end

function LuaWuYi2023Mgr:FenYongZhenXianReachScoreLimit(score)
    g_ScriptEvent:BroadcastInLua("OnFenYongZhenXianReachScoreLimit", score)
end

function LuaWuYi2023Mgr:FenYongZhenXianSendPlayResult(win, winReward, loseReward, extraReward, combo, teamInfoU, playerInfoU)
    self.m_PlayResultInfo = {win = win, winReward = winReward, loseReward = loseReward, extraReward = extraReward, combo = combo}
    local teamInfo = g_MessagePack.unpack(teamInfoU)
    self.m_PlayResultInfo.teamInfo = {}
    for i = 1, #teamInfo, 5 do
        local force, score, remain, usetime, total = teamInfo[i], teamInfo[i + 1], teamInfo[i + 2], teamInfo[i + 3], teamInfo[i + 4]
        if force then
            self.m_PlayResultInfo.teamInfo[force] = {score = score, remain = remain, usetime = usetime, total = total}
        end
    end
    self.m_PlayResultInfo.playerInfo = {}
    local playerInfo = g_MessagePack.unpack(playerInfoU)
    for i = 1, #playerInfo,12 do
        local playerId, playerName, class, gender, level, isFeiSheng, 
        force, chooseSkillId, killEnemy, killHorce, control, protect = 
            playerInfo[i], playerInfo[i + 1], playerInfo[i + 2], playerInfo[i + 3], playerInfo[i + 4], playerInfo[i + 5], 
            playerInfo[i + 6], playerInfo[i + 7], playerInfo[i + 8], playerInfo[i + 9], playerInfo[i + 10], playerInfo[i + 11]
        self.m_PlayResultInfo.playerInfo[playerId] = { 
            playerName = playerName, class = class, gender = gender, 
            isFeiSheng = isFeiSheng, force = force, chooseSkillId = chooseSkillId, 
            killEnemy = killEnemy, killHorce = killHorce, playerId = playerId,
            control = control, protect = protect,level = level
        }
    end
    CUIManager.ShowUI(CLuaUIResources.WuYi2023FYZXResultWnd)
end

function LuaWuYi2023Mgr:FenYongZhenXianShowSkillChooseWnd(closeTime)
    self.m_TempSkillWndCloseTime = closeTime
    CUIManager.CloseUI("CommonImageRuleWnd")
    CUIManager.ShowUI(CLuaUIResources.WuYi2023FYZXChooseTempSkillWnd)
end

LuaWuYi2023Mgr.m_LotteryInfo = {}
LuaWuYi2023Mgr.m_AdvancedResult = {}
LuaWuYi2023Mgr.m_CommonResult = {}
LuaWuYi2023Mgr.m_ItemId2Category = nil
LuaWuYi2023Mgr.m_SilverCanGive = 0
LuaWuYi2023Mgr.m_GetRewardWndType = 0
LuaWuYi2023Mgr.m_IsExchangeGold = false
LuaWuYi2023Mgr.m_XXSCDefaultIndex = nil     -- 君子六艺打开心想事成界面编号

function LuaWuYi2023Mgr:GetItemId2Category()
    if self.m_ItemId2Category == nil then
        self.m_ItemId2Category = {}
        WuYi2023_AdvancedRewards.Foreach(function(key, data)
            self.m_ItemId2Category[key] = data.Category
        end)
    end
end

function LuaWuYi2023Mgr:XinXiangShiChengSendLotteryInfo(haveGold, haveSilver, hasBronze, numberOfDraws, needGold, needSilver, allTakeNeedGold, allTakeNeedSilver, itemsSelected)
    print("XinXiangShiChengSendLotteryInfo", haveGold, haveSilver, numberOfDraws, needGold, needSilver, allTakeNeedGold, allTakeNeedSilver)
    local itemSelectInfo = g_MessagePack.unpack(itemsSelected)  -- itemId, status(0=未获取 1=已获取到)
    local itemInfo = {}
    for i = 1, #itemSelectInfo, 2 do
        local itemId, status = itemSelectInfo[i], itemSelectInfo[i+1]
        local advancedRewardsData = WuYi2023_AdvancedRewards.GetData(itemId)
        table.insert(itemInfo,{itemId = itemId, status = status, category = advancedRewardsData.Category})
    end
    table.sort(itemInfo, function (a,b)
        if a.category == b.category then
            return a.itemId < b.itemId
        end
        return a.category < b.category
    end)
    self.m_LotteryInfo = {
        haveGold = haveGold, haveSilver = haveSilver, numberOfDraws = numberOfDraws, 
        needGold = needGold, needSilver = needSilver, allTakeNeedGold = allTakeNeedGold, 
        allTakeNeedSilver = allTakeNeedSilver, itemInfo = itemInfo,hasBronze = hasBronze,
    }
    if not CUIManager.IsLoaded(CLuaUIResources.WuYi2023XXSCMainWnd) then
        CUIManager.ShowUI(CLuaUIResources.WuYi2023XXSCMainWnd)
        return 
    end
    g_ScriptEvent:BroadcastInLua("OnXinXiangShiChengSendLotteryInfo")
    g_ScriptEvent:BroadcastInLua("XinXiangShiChengSendCoinAmount", haveGold, hasBronze)     -- 君子六艺左侧更新
end

function LuaWuYi2023Mgr:XinXiangShiChengCommonResult(itemsInfoU)
    print("XinXiangShiChengCommonResult")
    local itemsInfo = g_MessagePack.unpack(itemsInfoU)
    local itemList = {}
    for i=1, #itemsInfo, 2 do
        table.insert(itemList,{ItemID = itemsInfo[i], Count = itemsInfo[i+1]})
    end
    table.sort(itemList,function(a,b) 
        local category1 = self.m_ItemId2Category[a.ItemID]
        local category2 = self.m_ItemId2Category[b.ItemID]
        if category1 == category2 then
            return a.ItemID < b.ItemID
        end
        return category1 < category2
    end)
    self.m_CommonResult = itemList
    g_ScriptEvent:BroadcastInLua("OnXinXiangShiChengCommonResult")
end

function LuaWuYi2023Mgr:XinXiangShiChengAdvancedResult(silverCanGive, itemsInfoU)
    print("XinXiangShiChengAdvancedResult", silverCanGive)
    self.m_SilverCanGive = silverCanGive
    local itemsInfo = g_MessagePack.unpack(itemsInfoU)
    local itemList = {}
    for i=1, #itemsInfo, 2 do
        table.insert(itemList,{ItemID = itemsInfo[i], Count = itemsInfo[i+1]})
    end
    self:GetItemId2Category()
    table.sort(itemList,function(a,b) 
        local category1 = self.m_ItemId2Category[a.ItemID]
        local category2 = self.m_ItemId2Category[b.ItemID]
        if category1 == category2 then
            return a.ItemID < b.ItemID
        end
        return category1 < category2
    end)
    self.m_AdvancedResult = itemList
    g_ScriptEvent:BroadcastInLua("OnXinXiangShiChengAdvancedResult")
end

function LuaWuYi2023Mgr:XinXiangShiChengSendCoinSuccess(friendId)
    g_ScriptEvent:BroadcastInLua("OnXinXiangShiChengSendCoinSuccess",friendId)
end