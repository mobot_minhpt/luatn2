local CommonDefs = import "L10.Game.CommonDefs"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

LuaYuanDan2022FuDanReadMeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaYuanDan2022FuDanReadMeWnd, "ContentScrollView", "ContentScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaYuanDan2022FuDanReadMeWnd, "Table", "Table", UITable)
RegistChildComponent(LuaYuanDan2022FuDanReadMeWnd, "ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanReadMeWnd, "Bubble1Label", "Bubble1Label", UILabel)
RegistChildComponent(LuaYuanDan2022FuDanReadMeWnd, "Bubble2Label", "Bubble2Label", UILabel)

--@endregion RegistChildComponent end

function LuaYuanDan2022FuDanReadMeWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaYuanDan2022FuDanReadMeWnd:Init()
    self.Bubble1Label.text = g_MessageMgr:FormatMessage("YuanDan2022FuDanReadMeWnd_Bubble1")
    self.Bubble2Label.text = g_MessageMgr:FormatMessage("YuanDan2022FuDanReadMeWnd_Bubble2")
    self.ParagraphTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Table.transform)
	local msg = g_MessageMgr:FormatMessage("YuanDan2022FuDanReadMeWnd_ReadMe")
	if System.String.IsNullOrEmpty(msg) then
        return
    end
	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
	for i = 0,info.paragraphs.Count - 1 do
		local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
		paragraphGo:SetActive(true)
		CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
	end
	self.ContentScrollView:ResetPosition()
    self.Table:Reposition()
end

--@region UIEvent

--@endregion UIEvent

