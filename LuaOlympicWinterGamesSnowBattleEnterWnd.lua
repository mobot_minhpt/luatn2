local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CButton = import "L10.UI.CButton"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaOlympicWinterGamesSnowBattleEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOlympicWinterGamesSnowBattleEnterWnd, "MatchButton", "MatchButton", CButton)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleEnterWnd, "ShopButton", "ShopButton", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleEnterWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleEnterWnd, "Table", "Table", UITable)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleEnterWnd, "TemplateNode", "TemplateNode", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleEnterWnd, "TimesLabel", "TimesLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaOlympicWinterGamesSnowBattleEnterWnd,"m_IsSignUp")

function LuaOlympicWinterGamesSnowBattleEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.MatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMatchButtonClick()
	end)

	UIEventListener.Get(self.ShopButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShopButtonClick()
	end)

    --@endregion EventBind end
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:Init()
	self:InitParagraphItems()
	local times = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSnowballFightRewardTimes)
	self.TimesLabel.text = SafeStringFormat3(LocalString.GetString("今日获得礼包%d/%d"),times,WinterOlympic_SnowballFightSetting.GetData().RewardTimesLimit)
	if CClientMainPlayer.Inst then
		Gac2Gas.GlobalMatch_RequestCheckSignUp(WinterOlympic_SnowballFightSetting.GetData().PlayDesignId,CClientMainPlayer.Inst.Id)
	end
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId == WinterOlympic_SnowballFightSetting.GetData().PlayDesignId then
        self:UpdateMatchButton(isInMatching)
    end
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == WinterOlympic_SnowballFightSetting.GetData().PlayDesignId and success then
        self:UpdateMatchButton(true)
    end
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId == WinterOlympic_SnowballFightSetting.GetData().PlayDesignId and success then
        self:UpdateMatchButton(false)
    end
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:UpdateMatchButton(isMatching)
	self.m_IsSignUp = isMatching
	self.MatchButton.Text = self.m_IsSignUp and LocalString.GetString("匹配中...") or LocalString.GetString("报名参与")
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:InitParagraphItems()
	self.TemplateNode:SetActive(false)
	Extensions.RemoveAllChildren(self.Table.transform)
	local msg = g_MessageMgr:FormatMessage("OlympicWinterGamesSnowBattleEnterWnd_ReadMe")
	if System.String.IsNullOrEmpty(msg) then
        return
    end
	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
	for i = 0,info.paragraphs.Count - 1 do
		local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.TemplateNode)
		paragraphGo:SetActive(true)
		CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
	end
	self.ScrollView:ResetPosition()
    self.Table:Reposition()
end

--@region UIEvent

function LuaOlympicWinterGamesSnowBattleEnterWnd:OnMatchButtonClick()
	local playId = WinterOlympic_SnowballFightSetting.GetData().PlayDesignId 
	if self.m_IsSignUp then
		Gac2Gas.GlobalMatch_RequestCancelSignUp(playId)
	else
		Gac2Gas.GlobalMatch_RequestSignUp(playId)
	end
end

function LuaOlympicWinterGamesSnowBattleEnterWnd:OnShopButtonClick()
	CLuaNPCShopInfoMgr.ShowScoreShopById(WinterOlympic_SnowballFightSetting.GetData().ShopId)
end

--@endregion UIEvent

