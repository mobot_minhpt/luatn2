local QnPopupList = import "L10.UI.QnPopupList"
local QnNewSlider = import "L10.UI.QnNewSlider"
local QnButton = import "L10.UI.QnButton"
--local QuanMinPK_StatueAnimation = import "L10.Game.QuanMinPK_StatueAnimation"
local RotateMode = import "DG.Tweening.RotateMode"
local Random = import "UnityEngine.Random"
local CMainCamera = import "L10.Engine.CMainCamera"
local Physics = import "UnityEngine.Physics"
local Tags = import "L10.Game.Tags"
local CAnimationMgr = import "L10.Game.CAnimationMgr"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local CRenderObject = import "L10.Engine.CRenderObject"
local LineRenderer = import "UnityEngine.LineRenderer"
local Material = import "UnityEngine.Material"
local ShadowCastingMode = import "UnityEngine.Rendering.ShadowCastingMode"
local Color = import "UnityEngine.Color"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CScreenCaptureWnd = import "L10.UI.CScreenCaptureWnd"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"

CLuaStarBiwuStatueBuildWnd=class()
CLuaStarBiwuStatueBuildWnd.Path = "ui/starbiwu/LuaStarBiwuStatueBuildWnd"
CLuaStarBiwuStatueBuildWnd.XMin = 51
CLuaStarBiwuStatueBuildWnd.XMax = 64
CLuaStarBiwuStatueBuildWnd.ZMin = 51
CLuaStarBiwuStatueBuildWnd.ZMax = 65
CLuaStarBiwuStatueBuildWnd.Y = 10.81674

RegistClassMember(CLuaStarBiwuStatueBuildWnd, "AniPopupList")
RegistClassMember(CLuaStarBiwuStatueBuildWnd, "AniSlider")
RegistClassMember(CLuaStarBiwuStatueBuildWnd, "RandomButton")
RegistClassMember(CLuaStarBiwuStatueBuildWnd, "SaveButton")

RegistClassMember(CLuaStarBiwuStatueBuildWnd, "AniList")
RegistClassMember(CLuaStarBiwuStatueBuildWnd, "CurAniIndex")


RegistClassMember(CLuaStarBiwuStatueBuildWnd, "StatueAniId")
RegistClassMember(CLuaStarBiwuStatueBuildWnd, "StatuePreviewTime")

RegistClassMember(CLuaStarBiwuStatueBuildWnd, "m_CurrentStatueObj")

RegistClassMember(CLuaStarBiwuStatueBuildWnd, "m_TopMenuTrans")

--Data
RegistClassMember(CLuaStarBiwuStatueBuildWnd, "m_Id2NewStatueDataTable")

RegistClassMember(CLuaStarBiwuStatueBuildWnd, "m_IsMoving")
RegistClassMember(CLuaStarBiwuStatueBuildWnd, "m_LineObjTable")

RegistClassMember(CLuaStarBiwuStatueBuildWnd, "m_SaveBtnSprite")

RegistClassMember(CLuaStarBiwuStatueBuildWnd, "m_OriginalIsShowUI")

function CLuaStarBiwuStatueBuildWnd:Restore()
	local StarBiwuStatueData = import "L10.Game.StarBiwuStatueData"
	for k, v in pairs(self.m_Id2NewStatueDataTable) do
		local clientObj = CClientObjectMgr.Inst:GetObject(k)
		if clientObj and clientObj.RO then
			local fakeAppear = clientObj.m_FakeAppear
			clientObj:ApplyStarBiwuStatueData(fakeAppear, StarBiwuStatueData.UnPack(fakeAppear.ExtraData.Data))
			clientObj.RO.Position = Vector3(clientObj.Pos.x / 64, clientObj.RO.Position.y, clientObj.Pos.y / 64)
			clientObj.Dir = clientObj.Dir
		end
	end
end

function CLuaStarBiwuStatueBuildWnd:Awake()
	--为了让拉近的时候不会显示其它界面
	CScreenCaptureWnd.IsInCaptureMode = true

	self.m_TopMenuTrans = self.transform:Find("Anchor/Node")
	self.m_TopMenuTrans.gameObject:SetActive(false)
	UIEventListener.Get(self.m_TopMenuTrans:Find("Panel/LeftRotateBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
		self:Rotate(true)
	end)
	UIEventListener.Get(self.m_TopMenuTrans:Find("Panel/RightRotateBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
		self:Rotate(false)
	end)
	self.m_IsMoving = false
	UIEventListener.Get(self.m_TopMenuTrans:Find("Panel/MoveBtn").gameObject).onPress = LuaUtils.BoolDelegate(function (go, isPressed)
		self.m_IsMoving = isPressed
	end)

	self.m_SaveBtnSprite = self.transform:Find("Anchor/SaveButton"):GetComponent(typeof(UISprite))
	local resetButton = self.transform:Find("Anchor/ResetButton")
	if resetButton then resetButton.gameObject:SetActive(false) end
	UIEventListener.Get(self.transform:Find("Anchor/CloseButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
		g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("是否保存对雕像的修改？"), function ()
			self:OnSaveButtonClicked(nil)
		end, function()
			self:Restore()
			CUIManager.CloseUI(CLuaUIResources.StarBiwuStatueBuildWnd)
		end, LocalString.GetString("保存"), LocalString.GetString("不保存"), false)
	end)
end

function CLuaStarBiwuStatueBuildWnd:SetButtonDirty(bDirty)
	self.m_SaveBtnSprite.spriteName = bDirty and "common_btn_01_yellow" or "common_btn_01_blue"
end

function CLuaStarBiwuStatueBuildWnd:Rotate(bInc)
	if not self.m_CurrentStatueObj or not self.m_CurrentStatueObj.RO then return end
	local dir = self.m_CurrentStatueObj.RO.Direction + (bInc and 10 or -10)
	self.m_CurrentStatueObj.RO.Direction = dir

	self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId].Data.Dir = dir
	self:SetButtonDirty(true)
end

function CLuaStarBiwuStatueBuildWnd:InitMember()
	local StarBiwuStatueData = import "L10.Game.StarBiwuStatueData"
	self.m_CurrentStatueObj = CLuaStarBiwuMgr.m_CurrentStatueObj
	if not self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId] then
		self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId] = {X = self.m_CurrentStatueObj.Pos.x, Z = self.m_CurrentStatueObj.Pos.y, Dir = self.m_CurrentStatueObj.Dir, Data = StarBiwuStatueData.UnPack(self.m_CurrentStatueObj.FakeAppear.ExtraData.Data)}
	end

	self:InitClassMember()
end

function CLuaStarBiwuStatueBuildWnd:Init()
	self.AniPopupList = self.transform:Find("Anchor/QnPopupList"):GetComponent(typeof(QnPopupList))
	self.AniSlider = self.transform:Find("Anchor/Label/NumberSlider"):GetComponent(typeof(QnNewSlider))
	self.RandomButton = self.transform:Find("Anchor/Label/RandomButton"):GetComponent(typeof(QnButton))
	self.SaveButton = self.transform:Find("Anchor/SaveButton"):GetComponent(typeof(QnButton))
	--初始化动作下拉按钮
	self.AniPopupList.OnValueChanged = DelegateFactory.Action_int_string(function (index, name)
		self:OnAniValueChanged(index, name)
	end)
	-- 初始化帧进度条
	self.AniSlider.OnValueChanged = DelegateFactory.Action_float(function (percent)
		self:OnSliderValueChanged(percent)
	end)
	-- 随机数按钮
	self.RandomButton.OnClick = DelegateFactory.Action_QnButton(function (qnButton)
		self:OnRandomAniButtonClicked(qnButton)
	end)
	-- 保存设置
	self.SaveButton.OnClick = DelegateFactory.Action_QnButton(function (qnButton)
		self:OnSaveButtonClicked(qnButton)
	end)

	self.m_Id2NewStatueDataTable = {}

	CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, true, false, false)
	CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false, false)
	self:InitLine()

	self:InitMember()
end

function CLuaStarBiwuStatueBuildWnd:InitLine()
	self.m_LineObjTable = {}

	local linePos = {Vector3(CLuaStarBiwuStatueBuildWnd.XMin, CLuaStarBiwuStatueBuildWnd.Y, CLuaStarBiwuStatueBuildWnd.ZMin),
								   Vector3(CLuaStarBiwuStatueBuildWnd.XMax, CLuaStarBiwuStatueBuildWnd.Y, CLuaStarBiwuStatueBuildWnd.ZMin),
									 Vector3(CLuaStarBiwuStatueBuildWnd.XMax, CLuaStarBiwuStatueBuildWnd.Y, CLuaStarBiwuStatueBuildWnd.ZMax),
									 Vector3(CLuaStarBiwuStatueBuildWnd.XMin, CLuaStarBiwuStatueBuildWnd.Y, CLuaStarBiwuStatueBuildWnd.ZMax),
									 Vector3(CLuaStarBiwuStatueBuildWnd.XMin, CLuaStarBiwuStatueBuildWnd.Y, CLuaStarBiwuStatueBuildWnd.ZMin)}

	for i=1,4 do
		local lineNode = GameObject("__StarBiwuLine__" .. i)
		local lineRenderScript = lineNode:AddComponent(typeof(LineRenderer))
		lineRenderScript:SetWidth(0.1,0.1)
		lineRenderScript:SetPosition(0,linePos[i])
		lineRenderScript:SetPosition(1,linePos[i+1])
		lineRenderScript.receiveShadows = false

		lineRenderScript.sharedMaterial = CreateFromClass(Material,CRenderObject.s_OutlinedEffectMaterial)
		lineRenderScript.sharedMaterial:SetColor('_OutlineColor', Color.green)
		lineRenderScript.sharedMaterial:SetFloat('_LoopTime', 0)
		lineRenderScript.shadowCastingMode = ShadowCastingMode.Off
		lineRenderScript.receiveShadows = false
		table.insert(self.m_LineObjTable, lineNode)
	end
end

function CLuaStarBiwuStatueBuildWnd:InitClassMember()
	self.CurAniIndex = -1

	self.AniList = CreateFromClass(MakeGenericClass(List, UInt32))

	local aniNames = CreateFromClass(MakeGenericClass(List, cs_string))
	local cls = EnumToInt(self.m_CurrentStatueObj.FakeAppear.Class)

	-- 类别
	QuanMinPK_StatueAnimation.ForeachKey(function (key)
		local data = QuanMinPK_StatueAnimation.GetData(key)
		if data.Class == cls then
			self.AniList:Add(key)
			aniNames:Add(data.AniName)
		end
    end)

    self.AniPopupList:Init(aniNames:ToArray())
    self.AniPopupList.m_IsFold = false
		self.AniPopupList.m_Table.gameObject:SetActive(false)

		-- Init animation panel
		local aniId = self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId].Data.AniId
		local previewTime = self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId].Data.PreviewTime
		local index = 0
		for k = 0, self.AniList.Count - 1 do
			if self.AniList[k] == aniId then
				index = k
				break
			end
		end
		self.AniPopupList:ChangeTo(index)
		local data = QuanMinPK_StatueAnimation.GetData(aniId)
		if data then
			local ani = self:GetAniName(data.Ani)
			self.StatueAniId = aniId
			self.StatuePreviewTime = previewTime
			self.AniSlider.m_Value = previewTime / self:GetAniLength(ani)
		end
end

function CLuaStarBiwuStatueBuildWnd:GetAniLength(aniName)
	if not self.m_CurrentStatueObj or not self.m_CurrentStatueObj.RO then return 1 end
	return self.m_CurrentStatueObj.RO:GetAniLength(aniName)
end

function CLuaStarBiwuStatueBuildWnd:GetAniName(aniName)
	if not self.m_CurrentStatueObj then return "" end
	local tabenId = self.m_CurrentStatueObj.FakeAppear.HideWeaponFashionEffect == 0 and self.m_CurrentStatueObj.FakeAppear.WeaponFashionId or 0
	local name = CAnimationMgr.Inst:GetAniName(self.m_CurrentStatueObj.FakeAppear.Class, self.m_CurrentStatueObj.FakeAppear.Gender, self.m_CurrentStatueObj.FakeAppear.Equipment, aniName, tabenId, false)
	return name
end

function CLuaStarBiwuStatueBuildWnd:DoAni(aniName, speed, hideWeapon)
	if not self.m_CurrentStatueObj or not self.m_CurrentStatueObj.RO then return end

	local name = self:GetAniName(aniName)
	self.m_CurrentStatueObj.RO:SetWeaponVisible(EWeaponVisibleReason.Expression, not hideWeapon)
	self.m_CurrentStatueObj.RO.Frozen = false;
	self.m_CurrentStatueObj.RO:DoAni(name, false, 0, speed, 0.15, true, 1.0)
end

function CLuaStarBiwuStatueBuildWnd:PreviewAni(aniName, startTime, hideWeapon)
	if not self.m_CurrentStatueObj or not self.m_CurrentStatueObj.RO then return end
	self.m_CurrentStatueObj.RO:SetWeaponVisible(EWeaponVisibleReason.Expression, not hideWeapon)
	self.m_CurrentStatueObj.RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (ro)
		ro:Preview(aniName, startTime)
	end))
end

-- 切换动作
function CLuaStarBiwuStatueBuildWnd:OnAniValueChanged(index, name)
	self.CurAniIndex = index
	local aniId = self.AniList[index]
	self.StatueAniId = aniId
	local data = QuanMinPK_StatueAnimation.GetData(aniId)
	if data then
		self.AniSlider.m_Value =  Random.value
		self:OnSliderValueChanged(self.AniSlider.m_Value)
	end
end

-- 切换帧
function CLuaStarBiwuStatueBuildWnd:OnSliderValueChanged(percent)
	if self.CurAniIndex >= 0 and self.CurAniIndex <= self.AniList.Count then
		local aniId = self.AniList[self.CurAniIndex]
		local data = QuanMinPK_StatueAnimation.GetData(aniId)
		if data then
			local ani = self:GetAniName(data.Ani)
			self.StatueAniId = aniId
			self.StatuePreviewTime = percent * self:GetAniLength(ani)
			self:PreviewAni(ani, self.StatuePreviewTime, data.HideWeapon > 0)

			-- Save data temporary
			self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId].Data.AniId = self.StatueAniId
			self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId].Data.PreviewTime = self.StatuePreviewTime
			self:SetButtonDirty(true)
		end
	end
end

-- 随机数
function CLuaStarBiwuStatueBuildWnd:OnRandomAniButtonClicked(go)
	if go then
		self:DoRandomAnimation(go.gameObject, 0)
	end
	self.AniPopupList:ChangeTo(Random.Range(0, self.AniList.Count))
	self.AniSlider.m_Value = Random.value
end

-- 筛子的动画
function CLuaStarBiwuStatueBuildWnd:DoRandomAnimation(go, times)
	local v = go.transform.eulerAngles
	v.z = v.z - 180
	CommonDefs.OnComplete_Tweener(LuaTweenUtils.DORotate(go.transform, v, 0.05, RotateMode.Fast), DelegateFactory.TweenCallback(function ()
        if times < 6 then
        	self:DoRandomAnimation(go, times + 1)
        end
    end))
end

-- 保存
function CLuaStarBiwuStatueBuildWnd:OnSaveButtonClicked(go)
	local StarBiwuStatueData = import "L10.Game.StarBiwuStatueData"
	for k, v in pairs(self.m_Id2NewStatueDataTable) do
		local dir = v.Data.Dir
		dir = 90 - dir
		if dir < 0 then dir = dir + 360 end
		Gac2Gas.RequestModifyStarBiwuShowStatue(v.X, v.Z, dir, StarBiwuStatueData.Pack(v.Data))
	end
	CUIManager.CloseUI(CLuaUIResources.StarBiwuStatueBuildWnd)
end

function CLuaStarBiwuStatueBuildWnd:OnDestroy()
	CScreenCaptureWnd.IsInCaptureMode = false
	CameraFollow.Inst:ResetToDefault(true)

	CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, false)
	CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, true, false)

	if self.m_LineObjTable then
		for i,v in pairs(self.m_LineObjTable) do
			GameObject.Destroy(v)
		end
		self.m_LineObjTable = nil
	end
end


-- move npc
function CLuaStarBiwuStatueBuildWnd:Update()
	if self.m_IsMoving then
		local posX = Input.mousePosition.x
		local posY = Input.mousePosition.y
		local screenPos = Vector3(posX,posY,0)
		local gridPos = self:ScreenPoint2WorldPos(screenPos)
		if gridPos and self.m_CurrentStatueObj and self.m_CurrentStatueObj.RO then
			if not (gridPos.x < CLuaStarBiwuStatueBuildWnd.XMin or gridPos.x > CLuaStarBiwuStatueBuildWnd.XMax or gridPos.z < CLuaStarBiwuStatueBuildWnd.ZMin or gridPos.z > CLuaStarBiwuStatueBuildWnd.ZMax) then
				self.m_CurrentStatueObj.RO.Position = gridPos
				self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId].X = math.floor(gridPos.x * 64)
				self.m_Id2NewStatueDataTable[self.m_CurrentStatueObj.EngineId].Z = math.floor(gridPos.z * 64)
				self:SetButtonDirty(true)
			end
		end
	end

	self.m_TopMenuTrans.gameObject:SetActive(self.m_CurrentStatueObj)
	if self.m_CurrentStatueObj and self.m_CurrentStatueObj.RO then
		local screenPos = CMainCamera.Main:WorldToScreenPoint(self.m_CurrentStatueObj.RO:GetSlotTransform("TopAnchor").position)
		screenPos.z = 0
		local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
		self.m_TopMenuTrans.position = pos
	end

	self:CheckMainplayerPos()
end

function CLuaStarBiwuStatueBuildWnd:CheckMainplayerPos()
	if not CClientMainPlayer.Inst then
		CUIManager.CloseUI(CLuaUIResources.StarBiwuStatueBuildWnd)
		return
	end
	local x, z = CClientMainPlayer.Inst.RO.Position.x, CClientMainPlayer.Inst.RO.Position.z
	if x > CLuaStarBiwuStatueBuildWnd.XMax + 10 or z < CLuaStarBiwuStatueBuildWnd.ZMin - 10 then
		CUIManager.CloseUI(CLuaUIResources.StarBiwuStatueBuildWnd)
		return
	end
end

function CLuaStarBiwuStatueBuildWnd:ScreenPoint2WorldPos(screenPoint)
	local hit = nil
	local ray = CMainCamera.Main:ScreenPointToRay(screenPoint)
	local dist = CMainCamera.Main.farClipPlane
	local hits = Physics.RaycastAll(ray, dist, CMainCamera.Main.cullingMask)
	if not hits or hits.Length == 0 then
		return nil
	end
	local hitDis = CMainCamera.Main.farClipPlane
	for i=0,hits.Length - 1 do
		local hitData = hits[i]
		if hitData.collider:CompareTag(Tags.Ground) then
			if hitDis > hitData.distance then
				hit = hitData
				hitDis = hitData.distance
			end
		end
	end

	if hit then
		return hit.point
	end
end

function CLuaStarBiwuStatueBuildWnd:StarBiwuStatueClicked()
	self:InitMember()
end

function CLuaStarBiwuStatueBuildWnd:OnEnable()
	g_ScriptEvent:AddListener("StarBiwuStatueClicked", self, "StarBiwuStatueClicked")
end

function CLuaStarBiwuStatueBuildWnd:OnDisable()
	g_ScriptEvent:RemoveListener("StarBiwuStatueClicked", self, "StarBiwuStatueClicked")
end
