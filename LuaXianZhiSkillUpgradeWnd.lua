require("common/common_include")

local UIProgressBar = import "UIProgressBar"
local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local MessageMgr = import "L10.Game.MessageMgr"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Money = import "L10.Game.Money"
local QnCheckBox = import "L10.UI.QnCheckBox"

LuaXianZhiSkillUpgradeWnd = class()

RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "DescLabel", UILabel)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "ExpProgress", UIProgressBar)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "NeedItemGrid", UIGrid)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "ItemTemplate", GameObject)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "MingWangControl", CCurentMoneyCtrl)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "OKButton", GameObject)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "ExpCostSprite", UISprite)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "ExpOwnSprite", UISprite)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "ExpCostLabel", UILabel)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "ExpOwnLabel", UILabel)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "CostHint", UILabel)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "MingWangCheckBox", QnCheckBox)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "BangGongCheckBox", QnCheckBox)
RegistChildComponent(LuaXianZhiSkillUpgradeWnd, "BangGongControl", CCurentMoneyCtrl)

RegistClassMember(LuaXianZhiSkillUpgradeWnd, "IsEnough")
RegistClassMember(LuaXianZhiSkillUpgradeWnd, "IsMingWang")

function LuaXianZhiSkillUpgradeWnd:Init()
	if not CClientMainPlayer.Inst then
		CUIManager.CloseUI(CLuaUIResources.XianZhiSkillUpgradeWnd)
		return
	end

	if self.IsMingWang == nil then
		self.IsMingWang = true
	end

	self.ItemTemplate:SetActive(false)

	self.MingWangCheckBox.OnValueChanged = DelegateFactory.Action_bool(
		function(select)
			self.MingWangControl.gameObject:SetActive(select)
			self.BangGongControl.gameObject:SetActive(not select)
			self.IsMingWang = select
			self.BangGongCheckBox:SetSelected(not select, true)
			self:UpdateSkillExp()
        end
	)
	
	self.BangGongCheckBox.OnValueChanged = DelegateFactory.Action_bool(
		function(select)
			self.MingWangControl.gameObject:SetActive(not select)
			self.BangGongControl.gameObject:SetActive(select)
			self.IsMingWang = not select
			self.MingWangCheckBox:SetSelected(not select, true)
			self:UpdateSkillExp()
        end
	)
	self.MingWangCheckBox:SetSelected(self.IsMingWang, true)

	self:UpdateXianZhiLevel()
	self.MingWangControl:SetType(EnumMoneyType.MingWang, EnumPlayScoreKey.NONE, true)
	self.BangGongControl:SetType(EnumMoneyType.GuildContri, EnumPlayScoreKey.NONE, true)
	self:UpdateSkillExp()

	local xianZhiData = CClientMainPlayer.Inst.PlayProp.XianZhiData
	local level = XianZhi_Level.GetData(xianZhiData.XianZhiLevel)

	if not level then
		CUIManager.CloseUI(CLuaUIResources.XianZhiSkillUpgradeWnd)
		return
	end

	self:UpdateNeedItems(level.NeedItems)
	self.MingWangControl:SetCost(level.NeedMingWang)
	self.BangGongControl:SetCost(level.GuildContribution)

	CommonDefs.AddOnClickListener(self.OKButton, DelegateFactory.Action_GameObject(function (go)
		self:OnOKButtonClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.CancelButton, DelegateFactory.Action_GameObject(function (go)
		self:OnCancelButtonClicked(go)
	end), false)
	
end

-- 更新仙职技能等级相关信息
function LuaXianZhiSkillUpgradeWnd:UpdateXianZhiLevel()

	local status = CLuaXianzhiMgr.GetXianZhiStatus()
	if status == EnumXianZhiStatus.NotFengXian then
		return
	end

	local xianZhiData =  CClientMainPlayer.Inst.PlayProp.XianZhiData
	local level = xianZhiData.XianZhiLevel
	local setting = XianZhi_Setting.GetData()
	local maxLevel = setting.LevelUpperLimit[status-1]

	self.DescLabel.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]当前[-]%s级/[ACF9FF]最高[-]%s级"), tostring(level), tostring(maxLevel))
	self.ExpProgress.value = level / maxLevel
end

-- 更新仙职技能所需经验相关
function LuaXianZhiSkillUpgradeWnd:UpdateSkillExp()
	local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    local qianshiExp = CClientMainPlayer.Inst.QianShiExp

    local xianZhiData = CClientMainPlayer.Inst.PlayProp.XianZhiData
	local level = XianZhi_Level.GetData(xianZhiData.XianZhiLevel)

	if not level then
		CUIManager.CloseUI(CLuaUIResources.XianZhiSkillUpgradeWnd)
		return
	end

	self.CostHint.text = nil
	local needExp = level.NeedExp
	if not self.IsMingWang then
		needExp = level.GuildContributionNeedExp
	end

	local expEnough = (skillPrivateExp + exp + qianshiExp) >= needExp
	if expEnough then
		-- 优先消耗专有经验
		if CClientMainPlayer.Inst.SkillProp.SkillPrivateExp > 0 then
			self.ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.PrivateExp, EnumPlayScoreKey.NONE)
			self.ExpCostSprite.spriteName = self.ExpOwnSprite.spriteName
			self.ExpOwnLabel.text = tostring(skillPrivateExp)
		elseif CClientMainPlayer.Inst.QianShiExp > 0 then
			self.ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.QianShiExp, EnumPlayScoreKey.NONE)
			self.ExpCostSprite.spriteName = self.ExpOwnSprite.spriteName
			self.ExpOwnLabel.text = tostring(qianshiExp)
		else
			self.ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
			self.ExpCostSprite.spriteName = self.ExpOwnSprite.spriteName
			self.ExpOwnLabel.text = tostring(exp)
		end

		local icons = ""
		if skillPrivateExp == 0 then
			if qianshiExp == 0 then
				-- 角色
			else
				-- 前世+角色
				if qianshiExp < needExp then
					icons = icons.."#290"
				end
			end
		else
			if skillPrivateExp >= needExp then
				-- 专用
			elseif qianshiExp == 0 then
				-- 专用+角色
				icons = icons .. "#290"
			else
				if (skillPrivateExp + qianshiExp) < needExp then
					-- 专用+前世+角色
					icons = icons .. "#292"
					icons = icons .. "#290"
				else
					-- 专用+前世
					icons = icons .. "#292"
				end
			end
		end
		if not System.String.IsNullOrEmpty(icons) then
			self.CostHint.text = System.String.Format(LocalString.GetString("不足的部分将从{0}中补足"), icons)
		else
			self.CostHint.text = nil
		end
	else
		self.ExpOwnSprite.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
		self.ExpCostSprite.spriteName = self.ExpOwnSprite.spriteName
		self.ExpOwnLabel.text = tostring(exp)
		self.CostHint.text = nil
	end
	self.ExpCostLabel.text = tostring(needExp)
end

function LuaXianZhiSkillUpgradeWnd:UpdateNeedItems(needItems)
	CUICommonDef.ClearTransform(self.NeedItemGrid.transform)
	self.IsEnough = true

	for i = 0, needItems.Length-1, 2 do

		local itemId = needItems[i]
		local count = needItems[i+1]
		local item = NGUITools.AddChild(self.NeedItemGrid.gameObject, self.ItemTemplate)
		self:InitNeedTemplate(item, itemId, count)

		local currentCount = CItemMgr.Inst:GetItemCount(itemId)
		self.IsEnough = self.IsEnough and (currentCount>= count)

		item:SetActive(true)
	end
	self.NeedItemGrid:Reposition()
end


function LuaXianZhiSkillUpgradeWnd:InitNeedTemplate(go, itemId, needCount)
	local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local DisableIcon = go.transform:Find("DisableIcon").gameObject

	if IdPartition.IdIsItem(itemId) then
		local item = Item_Item.GetData(itemId)
		Icon:LoadMaterial(item.Icon)

	else
		local template = EquipmentTemplate_Equip.GetData(itemId)
		Icon:LoadMaterial(template.Icon)
	end

	local count = CItemMgr.Inst:GetItemCount(itemId)
	CountLabel.text = SafeStringFormat3("%s/%s", tostring(count), tostring(needCount))
	DisableIcon:SetActive(count < needCount)

	CommonDefs.AddOnClickListener(Icon.gameObject, DelegateFactory.Action_GameObject(function (go)
		if count >= needCount then
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
		else
			CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
		end
	end), false)

end

function LuaXianZhiSkillUpgradeWnd:OnOKButtonClicked(go)
	if not self.IsEnough then
		MessageMgr.Inst:ShowMessage("XianZhi_Skill_Upgrade_Not_Enough", {})
		return
	end

	if self.IsMingWang and not self.MingWangControl.moneyEnough then
		MessageMgr.Inst:ShowMessage("XianZhi_Skill_Upgrade_MingWang_Not_Enough", {})
		return
	end

	if not self.IsMingWang and not self.BangGongControl.moneyEnough then
		MessageMgr.Inst:ShowMessage("XianZhi_Skill_Upgrade_BangGong_Not_Enough", {})
		return
	end

	local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    local qianshiExp = CClientMainPlayer.Inst.QianShiExp

    local xianZhiData = CClientMainPlayer.Inst.PlayProp.XianZhiData
	local level = XianZhi_Level.GetData(xianZhiData.XianZhiLevel)

	local expEnough = (skillPrivateExp + exp + qianshiExp) >= level.NeedExp
	if not expEnough then
		MessageMgr.Inst:ShowMessage("XianZhi_Skill_Upgrade_Exp_Not_Enough", {})
		return
	end
	
	if self.IsMingWang then
		Gac2Gas.RequestLevelUpXianZhi(false)
	else
		Gac2Gas.RequestLevelUpXianZhi(true)
	end
end

function LuaXianZhiSkillUpgradeWnd:OnCancelButtonClicked(go)
	CUIManager.CloseUI(CLuaUIResources.XianZhiSkillUpgradeWnd)
end

function LuaXianZhiSkillUpgradeWnd:OnLevelUpXianZhiSuccess()
	self:Init()
end

function LuaXianZhiSkillUpgradeWnd:SendItem(args)
	self:Init()
end

function LuaXianZhiSkillUpgradeWnd:SetItemAt(args)
	self:Init()
end

function LuaXianZhiSkillUpgradeWnd:OnEnable()
	g_ScriptEvent:AddListener("LevelUpXianZhiSuccess", self, "OnLevelUpXianZhiSuccess")
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
end

function LuaXianZhiSkillUpgradeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("LevelUpXianZhiSuccess", self, "OnLevelUpXianZhiSuccess")
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
end

return LuaXianZhiSkillUpgradeWnd
