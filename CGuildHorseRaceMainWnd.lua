-- Auto Generated!!
local CGuildHorseRaceMainWnd = import "L10.UI.CGuildHorseRaceMainWnd"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumEventType = import "EnumEventType"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local GuildHorseRaceMainGroupInfo = import "L10.Game.CGuildHorseRaceMgr+GuildHorseRaceMainGroupInfo"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MaPiTengYunSai_Setting = import "L10.Game.MaPiTengYunSai_Setting"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildHorseRaceMainWnd.m_Init_CS2LuaHook = function (this) 
    this.listView:Init(CGuildHorseRaceMgr.Inst.mainGroupList, true)
    local color = CGuildHorseRaceMgr.Inst.upTimes > 0 and "00FF00" or "FF0000"
    this.upTimeLabel.text = System.String.Format(LocalString.GetString("助威([c][{0}]{1}[-]/{2})"), color, CGuildHorseRaceMgr.Inst.upTimes, MaPiTengYunSai_Setting.GetData().CheerCount)

    if CGuildHorseRaceMgr.Inst.upTimes <= 0 then
        this.upButton.Enabled = false
    end

    this.yinpiao:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    this.yinpiao:SetCost(MaPiTengYunSai_Setting.GetData().CheerCostSilver)
    this.seatchView.OnSearch = MakeDelegateFromCSFunction(this.OnSearch, MakeGenericClass(Action1, String), this)

    if CGuildHorseRaceMgr.Inst:InPreelection() then
        this.buttons[0]:SetSelected(true, false)
        this.buttons[1]:SetSelected(false, false)
    else
        this.buttons[1]:SetSelected(true, false)
        this.buttons[0]:SetSelected(false, false)
    end
    this:InitStatus()
    this.isInit = false
end
CGuildHorseRaceMainWnd.m_InitStatus_CS2LuaHook = function (this) 

    repeat
        local default = CGuildHorseRaceMgr.Inst.mainStatus
        if default == (1) then
            this.statusLabel.text = g_MessageMgr:FormatMessage("GUILDHORSERACE_PREELECT_NOT_OPEN")
            if CGuildHorseRaceMgr.Inst:InPreelection() then
                this.upButton.Enabled = true
            else
                this.upButton.Enabled = false
            end
            break
        elseif default == (4) then
            this.statusLabel.text = g_MessageMgr:FormatMessage("GUILDHORSERACE_RANKED_NOT_OPEN")
            if CGuildHorseRaceMgr.Inst:InFinals() then
                this.upButton.Enabled = true
            else
                this.upButton.Enabled = false
            end
            break
        elseif default == (2) then
            this.statusLabel.text = g_MessageMgr:FormatMessage("GUILDHORSERACE_PREELECT_OPEN")
            this.upButton.Enabled = false
            break
        elseif default == (5) then
            this.statusLabel.text = g_MessageMgr:FormatMessage("GUILDHORSERACE_RANKED_OPEN")
            this.upButton.Enabled = false
            break
        elseif default == (3) then
            this.statusLabel.text = g_MessageMgr:FormatMessage("GUILDHORSERACE_PREELECT_FINISH")
            this.upButton.Enabled = false
            break
        elseif default == (6) then
            this.statusLabel.text = g_MessageMgr:FormatMessage("GUILDHORSERACE_RANKED_FINISH")
            this.upButton.Enabled = false
            break
        else
            break
        end
    until 1
end
CGuildHorseRaceMainWnd.m_OnEnable_CS2LuaHook = function (this) 
    this.radioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.radioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxSelect, MakeGenericClass(Action2, QnButton, Int32), this), true)
    UIEventListener.Get(this.upButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.upButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnUpButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tips).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tips).onClick, MakeDelegateFromCSFunction(this.OnTipsClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.OnGuildHorseRaceSceduleUpdate, MakeDelegateFromCSFunction(this.UpdateSchedule, Action0, this))
end
CGuildHorseRaceMainWnd.m_OnDisable_CS2LuaHook = function (this) 
    this.radioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.radioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxSelect, MakeGenericClass(Action2, QnButton, Int32), this), false)
    UIEventListener.Get(this.upButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.upButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnUpButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.tips).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tips).onClick, MakeDelegateFromCSFunction(this.OnTipsClick, VoidDelegate, this), false)
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.OnGuildHorseRaceSceduleUpdate, MakeDelegateFromCSFunction(this.UpdateSchedule, Action0, this))
end
CGuildHorseRaceMainWnd.m_UpdateSchedule_CS2LuaHook = function (this) 
    this.listView:Init(CGuildHorseRaceMgr.Inst.mainGroupList, false)
    local color = CGuildHorseRaceMgr.Inst.upTimes > 0 and "00FF00" or "FF0000"
    this.upTimeLabel.text = System.String.Format(LocalString.GetString("助威([c][{0}]{1}[-]/{2})"), color, CGuildHorseRaceMgr.Inst.upTimes, MaPiTengYunSai_Setting.GetData().CheerCount)
    if CGuildHorseRaceMgr.Inst.upTimes <= 0 then
        this.upButton.Enabled = false
    end
    this:InitStatus()
end
CGuildHorseRaceMainWnd.m_OnRadioBoxSelect_CS2LuaHook = function (this, button, index) 
    if not this.isInit then
        this.listView:ClearSelectedPlayerAndGroup()
        Gac2Gas.MPTYSRequestScheduleInfo(index + 1)
    end
end
CGuildHorseRaceMainWnd.m_OnUpButtonClick_CS2LuaHook = function (this, go) 
    if CGuildHorseRaceMgr.Inst.upTimes <= 0 then
        g_MessageMgr:ShowMessage("MPTYS_CHEER_LIMIT")
        return
    end
    if this.listView.selectPlayerId > 0 then
        Gac2Gas.MPTYSRequestCheer(1, this.listView.selectPlayerId)
    else
        g_MessageMgr:ShowMessage("GUILDHORSERACE_SELECT_PLAYER_TO_CHEER")
    end
end
CGuildHorseRaceMainWnd.m_OnSearch_CS2LuaHook = function (this, text) 
    if System.String.IsNullOrEmpty(text) then
        local list = CreateFromClass(MakeGenericClass(List, GuildHorseRaceMainGroupInfo))
        do
            local i = 0
            while i < CGuildHorseRaceMgr.Inst.mainGroupList.Count do
                local info = CGuildHorseRaceMgr.Inst.mainGroupList[i]
                CommonDefs.ListAdd(list, typeof(GuildHorseRaceMainGroupInfo), info)
                i = i + 1
            end
        end
        this.listView:Init(list, true)
    else
        local list = CreateFromClass(MakeGenericClass(List, GuildHorseRaceMainGroupInfo))
        do
            local i = 0
            while i < CGuildHorseRaceMgr.Inst.mainGroupList.Count do
                local info = CGuildHorseRaceMgr.Inst.mainGroupList[i]
                do
                    local j = 0
                    while j < info.playerList.Count do
                        if (string.find(info.playerList[j].playerName, text, 1, true) ~= nil) then
                            CommonDefs.ListAdd(list, typeof(GuildHorseRaceMainGroupInfo), info)
                            break
                        end
                        j = j + 1
                    end
                end
                i = i + 1
            end
        end
        this.listView:Init(list, true)
    end
end
