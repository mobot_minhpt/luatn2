-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTaskInfoMgr = import "L10.UI.CTaskInfoMgr"
local CTaskListItem = import "L10.UI.CTaskListItem"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUIManager = import "L10.UI.CUIManager"
local Ease = import "DG.Tweening.Ease"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gameplay_Gameplay = import "L10.Game.Gameplay_Gameplay"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local LuaTweenUtils = import "LuaTweenUtils"
local CTrackMgr = import "L10.Game.CTrackMgr"
local NGUIText = import "NGUIText"
local CPos = import "L10.Engine.CPos"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Task_Task = import "L10.Game.Task_Task"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UIWidget = import "UIWidget"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"
local FXClientInfoReporter = import "L10.Game.FXClientInfoReporter"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local NGUIText = import "NGUIText"
local Utility = import "L10.Engine.Utility"
CTaskListItem.m_InitActivity_CS2LuaHook = function (this, act, scrollView)
    this.isTask = false
    this.template = nil
    this.taskNameLabel.text = act.activityName
    this.taskInfo = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(this.taskInfoLabel.color), CChatLinkMgr.TranslateToNGUIText(act.activityInfo, false))
    this.taskInfoLabel.text = this.taskInfo
    this.fx.ScrollView = scrollView
    this.yitiaoProgress.gameObject:SetActive(false)
    this.shituProgress.gameObject:SetActive(false)
    this.warmChristmasEveProgress.gameObject:SetActive(false)
    UIEventListener.Get(this.gameObject).onClick = MakeDelegateFromCSFunction(this.OnActivityClick, VoidDelegate, this)

    this.act = act
    if act.timeLimit then
        this:OnUpdateActivityTick()
        this.cancel = CTickMgr.Register(MakeDelegateFromCSFunction(this.OnUpdateActivityTick, Action0, this), 1000, ETickType.Loop)
    end
    if this.taskItem ~= nil then
        this.taskItemIcon:Clear()
        this.taskItemFx:DestroyFx()
        this.taskItem:SetActive(false)
    end
    local func = CLuaTaskListItem.m_ProcessActivityCallbackMap[act.gameplayId]
    if func then
        func(this, act)
    end
end
CTaskListItem.m_OnUpdateActivityTick_CS2LuaHook = function (this)
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(this.act.endTime)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local delta = math.floor(time:Subtract(now).TotalSeconds)

    if delta > 0 then
        if this.act.notShowTime then
            this.taskInfoLabel.text = this.taskInfo
        else
            this.taskInfoLabel.text = this.taskInfo .. this:formatTime(delta, "00ff00")
        end
    else
        if this.act.notShowTime then
            this.taskInfoLabel.text = LocalString.GetString("该任务已过期，请放弃")
        else
            this.taskInfoLabel.text = this.taskInfo .. LocalString.GetString("\n剩余：[ff0000]0:00[-]")
        end
        if this.cancel ~= nil then
            invoke(this.cancel)
        end
    end
end
CTaskListItem.m_Init_CS2LuaHook = function (this, task, taskName, taskInfo, completed, scrollView)
    this.isTask = true
    UIEventListener.Get(this.gameObject).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
    this.Task = task
    this.template = task and task.TaskTemplate or nil
    this.taskNameLabel.text = taskName
    this.taskInfo = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(this.taskInfoLabel.color), CChatLinkMgr.TranslateToNGUIText(taskInfo, false))
    this.Completed = completed
    this.taskInfoLabel.text = this.taskInfo
    this.fx.ScrollView = scrollView
    this.lastUpdateTime = 0

    local mainTaskId = task.TemplateId
    local mainTask = this.template

    if task.MainTaskId > 0 then
        mainTaskId = task.MainTaskId
        mainTask = Task_Task.GetData(mainTaskId)
    end

    CLuaTaskListItem:InitYiTiaoLong(this, task, mainTaskId, mainTask)
    CLuaTaskListItem:InitShiTuQing(this, task, mainTaskId, mainTask)

    if this.taskItem ~= nil then
        --这里仅做初步判断，不去检查任务栏是不是存在道具，可以简化处理
        if this.template == nil or not Item_Item.Exists(this.template.QuickUseItem) then
            this.taskItemIcon:Clear()
            this.taskItemFx:DestroyFx()
            this.taskItem:SetActive(false)
        else
            this.taskItem:SetActive(true)
            this.taskItemFx:LoadFx(CUIFxPaths.ZhanLongTaskItemFx)
            this.taskItemIcon:LoadMaterial(Item_Item.GetData(this.template.QuickUseItem).Icon)
        end
    end
    CLuaTaskListItem:Init(this)
end
CTaskListItem.m_OnDisable_CS2LuaHook = function (this)
    EventManager.RemoveListener(EnumEventType.OnUpdateCurrentTaskReadStatus, MakeDelegateFromCSFunction(this.OnUpdateCurrentTaskReadStatus, Action0, this))
    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    CLuaTaskListItem:OnDisable(this)
end
CTaskListItem.m_OnUpdateYiTiaoLongTaskFinishedTimes_CS2LuaHook = function (this, taskId, round, subtimes, todayTimes)
    if this.TaskId ~= taskId then
        return
    end
    for i=0,this.yitiaoSplits.Length-1 do
        this.yitiaoSplits[i].color = (i < subtimes) and CTaskListItem.yitiaolongHightlightColor or CTaskListItem.yitiaolongDarkColor
    end
end
CTaskListItem.m_OnUpdateShiTuQingTaskFinishedTimes_CS2LuaHook = function (this, taskId, subTimes)
    if this.TaskId ~= taskId then
        return
    end
    for i=0,this.shituSplits.Length-1 do
        this.shituSplits[i].enabled = (i < subTimes)
    end
end
CTaskListItem.m_Resize_CS2LuaHook = function (this)
    this:Update()
    --更新文本内容
    local nameHeight = this.taskNameLabel.localSize.y
    --UILabel用height可能更新不及时
    local verticalGap = 14
    local infoHeight = this.taskInfoLabel.localSize.y
    --UILabel用height可能更新不及时
    local progressHeight = 0
    if this.yitiaoProgress.gameObject.activeSelf then
        progressHeight = this.yitiaoProgress.height + verticalGap
    elseif this.shituProgress.gameObject.activeSelf then
        progressHeight = this.shituProgress.height + verticalGap
    elseif this.warmChristmasEveProgress.gameObject.activeSelf then
        progressHeight = this.warmChristmasEveProgress.height + verticalGap
    end
    local toalHeight = nameHeight + infoHeight + verticalGap * 3 + progressHeight
    this.background.height = math.floor(toalHeight + 0.5)
    Extensions.SetLocalPositionY(this.taskNameLabel.transform, this.background.height * 0.5 - nameHeight * 0.5 - verticalGap)
    Extensions.SetLocalPositionY(this.taskInfoLabel.transform, this.background.height * 0.5 - nameHeight - infoHeight * 0.5 - verticalGap * 2)
    if this.yitiaoProgress.gameObject.activeSelf then
        Extensions.SetLocalPositionY(this.yitiaoProgress.transform, this.taskInfoLabel.transform.localPosition.y - infoHeight * 0.5 - verticalGap - this.yitiaoProgress.height * 0.5)
    elseif this.shituProgress.gameObject.activeSelf then
        Extensions.SetLocalPositionY(this.shituProgress.transform, this.taskInfoLabel.transform.localPosition.y - infoHeight * 0.5 - verticalGap - this.shituProgress.height * 0.5)
    elseif this.warmChristmasEveProgress.gameObject.activeSelf then
        Extensions.SetLocalPositionY(this.warmChristmasEveProgress.transform, this.taskInfoLabel.transform.localPosition.y - infoHeight * 0.5 - verticalGap - this.warmChristmasEveProgress.height * 0.5)
    end
    this:OnUpdateCurrentTaskReadStatus()
    if this.taskItem ~= nil then
        CommonDefs.GetComponent_GameObject_Type(this.taskItem, typeof(UIWidget)):ResetAndUpdateAnchors()
    end
end
CTaskListItem.m_PrecessAppHelper_CS2LuaHook = function (this)
    local taskProp = CClientMainPlayer.Inst.TaskProp
    if this.TaskId == Constants.QNZSGuideTaskId and CommonDefs.DictContains(taskProp.CurrentTasks, typeof(UInt32), this.TaskId) and CommonDefs.DictGetValue(taskProp.CurrentTasks, typeof(UInt32), this.TaskId).CanSubmit == 0 then
        Gac2Gas.RequestCheckBindAppHelper(this.TaskId)
        return true
    end
    return false
end
CTaskListItem.m_OnItemClick_CS2LuaHook = function (this, go)
    if CClientMainPlayer.Inst == nil or this.Task == nil then
        return
    end
    local taskProp = CClientMainPlayer.Inst.TaskProp
    if this:PrecessAppHelper() then
        return
    end
    if this.Task.OverGrade <= 0 then
        CTaskMgr.Inst:SetReceivedTaskReadStatus(this.TaskId, false)
    end
    if CommonDefs.DictContains(taskProp.CurrentTasks, typeof(UInt32), this.TaskId) then
        local task = CommonDefs.DictGetValue(taskProp.CurrentTasks, typeof(UInt32), this.TaskId)
        if task.IsFailed then
            --点击失败的任务时，弹出任务界面，方便放弃任务操作
            CTaskInfoMgr.ShowTaskWnd(this.TaskId, 0)
            g_MessageMgr:ShowMessage("TASK_FAILURE_NEED_GIVE_UP")
            return
        end

        if CTaskMgr.Inst:IsFestivalTaskTimeOut(task) then
            --
            CTaskInfoMgr.ShowTaskWnd(this.TaskId, 0)
            return
        end
        -- 遍历特殊响应逻辑，满足任何一个就立即返回
        for _, repsonsefunc in pairs(g_TaskListItemClickResponseList) do
            if repsonsefunc(this.TaskId, this.Task, this.template) then
                print(_, repsonsefunc, this.TaskId)
                return
            end
        end
    end

    if CTaskMgr.Inst:IsTrackingToDoTask(this.TaskId) then
        return
    end
    if Task_Task.Exists(this.TaskId) then
		FXClientInfoReporter.Inst:ReportTaskListItemClick(this.TaskId)
        CTaskMgr.Inst:TrackToDoTask(this.TaskId)
    end
    if this.Task.OverGrade > 0 then
        CUIManager.ShowUI("ScheduleWnd")
    end
end
CTaskListItem.m_DoAni_CS2LuaHook = function (this, bounds, isEllipsePath)
    LuaTweenUtils.DOKill(this.fx.FxRoot, false)
    local waypoints = CUICommonDef.GetWayPoints(bounds, 1, isEllipsePath)
    this.fx.FxRoot.localPosition = waypoints[0]
    LuaTweenUtils.DOLuaLocalPath(this.fx.FxRoot, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
end

CLuaTaskListItem = {}
CLuaTaskListItem.m_UpdateTaskDescriptionLabelCallbackMap = {}
CLuaTaskListItem.m_ProcessTaskCallbackMap = {}
CLuaTaskListItem.PaoShangItem = nil
CLuaTaskListItem.sceneTemplateId = nil
CLuaTaskListItem.posX = nil
CLuaTaskListItem.posY = nil

function CLuaTaskListItem:Init(this)
    if this.template then
        if this.template.GamePlay == "PaoShang" then
            CLuaTaskListItem.PaoShangItem = this
            CLuaTaskListItem:UpdatePaoShangPanel()
        elseif string.find(this.template.GamePlay, "SchoolSect") or this.template.ID == 22029597 or this.template.ID == 22029603 or this.template.ID == 22029604 then
            self:UpdateTaskDescriptionLabel(this.Task,this.template.TaskDescription,this.taskInfoLabel,this.template.GamePlay)
        elseif this.template.GamePlay == "ShenYao" then
            LuaXinBaiLianDongMgr:InitExclusiveShenYaoTask(this.Task, this.taskInfoLabel, this.template.TaskDescription)
        elseif this.template.ID == 22112292 or this.template.ID == 22112294 then -- 暑假2023世界事件 [隐仙湾之劫]倭魂 [隐仙湾之劫]觅痕
            local desc = this.template.TaskDescription
            local str = ShuJia2023_Setting.GetData("NeedEventsDesc").Value
            local tbl = {}
            for eventName in string.gmatch(str, "([^;]+)") do
                for event, name in string.gmatch(eventName, "([^,]+),([^,]+)") do
                    tbl[event] = name
                end
            end
            CommonDefs.DictIterate(this.template.NeedEnvets, DelegateFactory.Action_object_object(function(key,val)
                local ret, info = CommonDefs.DictTryGet_LuaCall(this.Task.NeedEnvets, key)
                local prefix = tbl[key] or key
                if ret then
                    desc = desc.."\n"..prefix.."("..info.Count.."/"..val.Count..")"
                end
            end))
            this.taskInfoLabel.text = CUICommonDef.TranslateToNGUIText(desc)
        end
    end
end

function CLuaTaskListItem:OnDisable(this)
    if this.template and this.template.GamePlay == "PaoShang" then
    	CLuaTaskListItem.PaoShangItem = nil
	end
end
--更新跑商taskitem
function CLuaTaskListItem:UpdatePaoShangPanel()
    if CLuaTaskListItem.PaoShangItem and CLuaTaskListItem.sceneTemplateId and CLuaTaskListItem.posX and CLuaTaskListItem.posY then
        local mapData = PublicMap_PublicMap.GetData(CLuaTaskListItem.sceneTemplateId)
        if mapData then
            local str = SafeStringFormat3("[c][ffff00]%s[-][/c]",CChatLinkMgr.TranslateToNGUIText(CLuaTaskListItem.PaoShangItem.taskInfo.. SafeStringFormat3(LocalString.GetString("\n货车位置：(%s,%s,%s)"), mapData.Name,CLuaTaskListItem.posX,CLuaTaskListItem.posY), false))    
            CLuaTaskListItem.PaoShangItem.taskInfoLabel.text = str
            CLuaTaskListItem.PaoShangItem:Resize()
            CLuaTaskListItem.PaoShangItem.fx.scrollView:UpdatePosition();
        end
    end
end

-- 同步跑商镖车位置
function Gas2Gac.SyncPaoShangBiaoChePosInfo(sceneTemplateId, posX, posY)
    CLuaTaskListItem.sceneTemplateId = sceneTemplateId
    CLuaTaskListItem.posX = posX
    CLuaTaskListItem.posY = posY
    if CLuaTaskListItem.PaoShangItem then        
        CLuaTaskListItem:UpdatePaoShangPanel()
    end
end

function CLuaTaskListItem:ProcessSchoolSectTask(task)
    local taskId = task.TemplateId
    local taskDesignData = task.TaskTemplate

    if (taskId == 22029527 or taskId == 22029531) and LuaZongMenMgr.m_OpenNewSystem then
        Gac2Gas.RequestAutoFinishSectTask(taskId)
    end

    if taskId == 22112172 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0 then
        Gac2Gas.RequestAutoFinishSectTask(taskId)
    end

    -- 处理宗门提交寻路
    if task.CanSubmit == 1 and Menpai_ShiWuTask.GetData(task.TemplateId) ~= nil then
        if not LuaZongMenMgr:IsMySectScene() then
            if CClientMainPlayer.Inst then
                Gac2Gas.RequestEnterSelfSectScene(CClientMainPlayer.Inst.BasicProp.SectId)
            end

            -- 塞入跨场景寻路信息
            local location = CTaskMgr.Inst:GetTaskTrackPosInterval(task)
            CTrackMgr.Inst.m_Track:RecordCrossingMap("", location.sceneTemplateId, Utility.GridPos2PixelPos(CPos(location.targetX, location.targetY)), 32, DelegateFactory.Action(function()
                if Task_Task.Exists(taskId) then
                    CTaskMgr.Inst:TrackToDoTask(taskId)
                end
            end), nil, nil, nil)
        else
            if Task_Task.Exists(taskId) then
                CTaskMgr.Inst:TrackToDoTask(taskId)
            end
        end
        return true
    end

    if (taskDesignData.GamePlay == "eSectFuZhiTask" or string.find(taskDesignData.GamePlay, "SchoolSect")) then
        local func = CLuaTaskListItem.m_ProcessTaskCallbackMap[taskDesignData.GamePlay]
        if func then
            return func(task)
        end
    end
    return false
end

function CLuaTaskListItem:UpdateTaskDescriptionLabel(task,taskDescription,label,gamePlay)
    local func = self.m_UpdateTaskDescriptionLabelCallbackMap[gamePlay]
    if func then
        func(task,taskDescription,label)
    end
end

function CLuaTaskListItem:CommonProcessSchoolSectTask(task)
    local extraData = task.ExtraData
    local taskDesignData = task.TaskTemplate
    if extraData and extraData.Data then
        local data = extraData.Data
        local list = MsgPackImpl.unpack(data)
        local dataCount = list.Count
        if dataCount == 3 and task.TemplateId == 22029597 then
            local sectId, sectName, locationId = tonumber(list[0]), list[1], tonumber(list[2])
            local taskLocationData = Menpai_TaskLocation.GetData(locationId)
            if taskLocationData and taskLocationData.Location.Length == 3 then 
                local sceneTemplateId, targetX, targetY = taskLocationData.Location[0],taskLocationData.Location[1],taskLocationData.Location[2]
                CTrackMgr.Inst:TaskTrack(task.TemplateId,"", sceneTemplateId, 0, CPos(targetX * 64,targetY * 64), taskLocationData.Range, DelegateFactory.Action(function () 
                    if taskDesignData.TaskLocations.Length > 0 then
                        CZuoQiMgr.Inst:TryRideOff()
                        CExpressionActionMgr.ShowThumbnailExpressionsInNeed(taskDesignData.TaskLocations[0].ConditionObjectId)
                    end
                end), nil)
                return true
            end
        elseif dataCount >= 2 then
            local isItemTask = #taskDesignData.SubmitItem > 0 or #taskDesignData.FindItem > 0 and task.CanSubmit ~= 1
            if isItemTask then
                -- 处理一下提交道具的任务
                print(debug.traceback())
                return false
            end

            local sectId, sectName = tonumber(list[0]), list[1]
            local loc = CTaskMgr.Inst:GetTaskTrackPosInterval(task)
            if loc then
                if loc.InteractWithNpcId ~= 0 and loc.sceneTemplateId ~= tonumber(Menpai_Setting.GetData("RaidMapId").Value) then
                    return false
                end
                CTaskMgr.Inst.m_TrackingLocation = loc
                Gac2Gas.TaskTrackToSectScene(task.TemplateId, sectId)
                return true   
            end  
        end
    end
    return false
end

--SchoolSect Task callback
CLuaTaskListItem.m_UpdateTaskDescriptionLabelCallbackMap["SchoolSectEntrance"] = function(task,taskDescription,label)
    if task.ExtraData and task.ExtraData.Data then
        local data = task.ExtraData.Data
        local list = MsgPackImpl.unpack(data)
        local dataCount = list.Count
        if dataCount == 3 then
            local sectId, sectName, locationId = tonumber(list[0]),list[1],tonumber(list[2])
            local taskLocationData = Menpai_TaskLocation.GetData(locationId)
            if taskLocationData then
                local str = SafeStringFormat3(taskDescription, taskLocationData.Content)
                label.text = str
            end
        end
    end
end

CLuaTaskListItem.m_UpdateTaskDescriptionLabelCallbackMap["SchoolSectVisit"] = function(task,taskDescription,label)
    if task.ExtraData and task.ExtraData.Data then
        local data = task.ExtraData.Data
        local list = MsgPackImpl.unpack(data)
        local dataCount = list.Count
        if dataCount >= 2 then
            local sectId, sectName = tonumber(list[0]),list[1]
            local str = SafeStringFormat3(taskDescription, sectName)
            label.text = str
        end
    end
end

CLuaTaskListItem.m_UpdateTaskDescriptionLabelCallbackMap["eSectFuZhiTask"] = function(task,taskDescription,label)
    if task.ExtraData and task.ExtraData.Data then
        local data = task.ExtraData.Data
        local list = MsgPackImpl.unpack(data)
        local dataCount = list.Count
        if dataCount == 3 and task.TemplateId == 22029597 then
            local sectId, sectName, locationId = tonumber(list[0]), list[1], tonumber(list[2])
            local taskLocationData = Menpai_TaskLocation.GetData(locationId)
            if taskLocationData and taskLocationData.Location.Length == 3 then 
                local str = SafeStringFormat3(taskDescription, taskLocationData.Content)
                label.text = str
            end
        elseif dataCount >= 2 then
            local sectId, sectName = tonumber(list[0]),list[1]
            local str = SafeStringFormat3(taskDescription, sectName)
            label.text = str
        end
    end
end

CLuaTaskListItem.m_ProcessTaskCallbackMap["SchoolSectEntrance"] = function(task)
    local extraData = task.ExtraData
    local taskDesignData = task.TaskTemplate
    if extraData and extraData.Data then
        local data = extraData.Data
        local list = MsgPackImpl.unpack(data)
        local dataCount = list.Count
        if dataCount == 3 then
            local sectId, sectName, locationId = tonumber(list[0]), list[1], tonumber(list[2])
            local taskLocationData = Menpai_TaskLocation.GetData(locationId)
            if taskLocationData and taskLocationData.Location.Length == 3 then 
                local sceneTemplateId, targetX, targetY = taskLocationData.Location[0],taskLocationData.Location[1],taskLocationData.Location[2]
                CTrackMgr.Inst:TaskTrack(task.TemplateId,"", sceneTemplateId, 0, CPos(targetX * 64,targetY * 64), taskLocationData.Range, DelegateFactory.Action(function () 
                    if taskDesignData.TaskLocations.Length > 0 then
                        CZuoQiMgr.Inst:TryRideOff()
                        CExpressionActionMgr.ShowThumbnailExpressionsInNeed(taskDesignData.TaskLocations[0].ConditionObjectId)
                    end
                end), nil)
                return true
            end
        end
    end
    return false
end

CLuaTaskListItem.m_ProcessTaskCallbackMap["SchoolSectVisit"] = function(task)
    return CLuaTaskListItem:CommonProcessSchoolSectTask(task)
end

CLuaTaskListItem.m_ProcessTaskCallbackMap["SchoolSectWuxing"] = function(task)
    return CLuaTaskListItem:CommonProcessSchoolSectTask(task)
end

CLuaTaskListItem.m_ProcessTaskCallbackMap["SchoolSectSoulcore"] = function(task)
    return CLuaTaskListItem:CommonProcessSchoolSectTask(task)
end

CLuaTaskListItem.m_ProcessTaskCallbackMap["SchoolSectTransform"] = function(task)
    return CLuaTaskListItem:CommonProcessSchoolSectTask(task)
end

CLuaTaskListItem.m_ProcessTaskCallbackMap["eSectFuZhiTask"] = function(task)
    return CLuaTaskListItem:CommonProcessSchoolSectTask(task)
end

------------------
--一条龙 & 情义春秋
------------------
function CLuaTaskListItem:InitYiTiaoLong(this, task, mainTaskId, mainTask)
    local mainplayer = CClientMainPlayer.Inst
    if mainTask.GamePlay == "YiTiaoLong" then
        this.yitiaoProgress.gameObject:SetActive(true)
        if mainplayer and CommonDefs.DictContains_LuaCall(mainplayer.TaskProp.RepeatableTasks, mainTaskId) then
            local repeatableTaskInfo = CommonDefs.DictGetValue_LuaCall(mainplayer.TaskProp.RepeatableTasks, mainTaskId)
            local finishedSubTimes = repeatableTaskInfo.FinishedSubTimes
            for i=0,this.yitiaoSplits.Length-1 do
                this.yitiaoSplits[i].color = (i < finishedSubTimes) and CTaskListItem.yitiaolongHightlightColor or CTaskListItem.yitiaolongDarkColor
            end

            if CScene.MainScene ~= nil and mainplayer.IsInGamePlay then
                local gamePlayId = CScene.MainScene.GamePlayDesignId
                local play = Gameplay_Gameplay.GetData(gamePlayId)
                if play ~= nil and play.TaskId == task.TemplateId then
                    this.DontDisplayDuration = true
                end
            end
        end
    else
        this.yitiaoProgress.gameObject:SetActive(false)
    end
end
function CLuaTaskListItem:InitShiTuQing(this, task, mainTaskId, mainTask)
    local mainplayer = CClientMainPlayer.Inst
    if mainTask.GamePlay == "ShiTuQing" then
        this.shituProgress.gameObject:SetActive(true)
        local finishedSubTimes = task:GetShiTuQingFinishTimes()
        for i=0,this.shituSplits.Length-1 do
            this.shituSplits[i].enabled = (i < finishedSubTimes)
        end
    else
        this.shituProgress.gameObject:SetActive(false)
    end
end
------------------------------------------
--点击任务栏任务自定义特殊处理逻辑，有两种解决办法
-- 方案1 ： 在下面的g_TaskListItemClickResponseList顺序追加自己玩法的逻辑，return true表示不需要后续处理
-- 方案2 ： 借助task表的doevent方案来处理，如果涉及到寻路，推荐第二种方案
------------------------------------------
g_TaskListItemClickResponseList = {} 
--神兵 by hzzhanghonglei #103778  #101569
table.insert(g_TaskListItemClickResponseList, 1, function(taskId, task, taskTemplate)
    if taskTemplate.GamePlay == "ShenBingKillTask" and task.CanSubmit ~= 1 then
        g_MessageMgr:ShowMessage("SHEN_BING_KILL_TASK_INFORMATION_MSG")
        return true
    end
end)
--跨服城战战龙 by hzzhanghonglei #107787 【跨服城战】战龙任务寻路优化
table.insert(g_TaskListItemClickResponseList, 2, function(taskId, task, taskTemplate)
    if CTaskMgr.Inst:IsCityWarZhanLongTask(taskTemplate) then
        g_MessageMgr:ShowMessage("CITY_WAR_ZHAN_LONG_CANNOT_XUNLU")
        return true
    end
end)
--2019清明 by hzzhangqing #114330 杏酒饮春愁点击任务打开界面处理
table.insert(g_TaskListItemClickResponseList, 3, function(taskId, task, taskTemplate)
    if LuaQingming2019Mgr.CheckTask(task.TemplateId) then
        return true
    end
end)
--2019中秋 by chengguangzhong #123403 【中秋】青睐度任务没有完成的时候点击打开送礼界面
table.insert(g_TaskListItemClickResponseList, 4, function(taskId, task, taskTemplate)
    if taskId >= 22120159 and taskId <= 22120166 then --中秋2019活动，
        if task.CanSubmit ~= 1 then
            CUIManager.ShowUI(CLuaUIResources.ZQYaoBanShangYueWnd)
            return true
        end
    end
end)
--2020中元节 by linhao1 #138085 【中元节】洗冤灵谱客户端程序需求
table.insert(g_TaskListItemClickResponseList, 5, function(taskId, task, taskTemplate)
    if ZhongYuanJie2020Mgr:ProcessXYLPTask(task) then
        return true
    end
end)
--无量蜃境 by xieshiheng #177085 已经完成的任务支持点击不自动寻路提交
table.insert(g_TaskListItemClickResponseList, 6, function(taskId, task, taskTemplate)
    if LuaWuLiangMgr:ProcessTask(task) then
        return true
    end
end)
--尚食 by linhao1 #163934 【客户端】尚食食谱制作和烹饪任务未完成时点击任务弹出活动界面
table.insert(g_TaskListItemClickResponseList, 7, function(taskId, task, taskTemplate)
    if LuaCookBookMgr:ProcessCookbookTask(task) then 
        return true
    end
end)
--npc加入师门 by huangxinyu #148785 NPC加入各个师门任务处理
table.insert(g_TaskListItemClickResponseList, 8, function(taskId, task, taskTemplate)
    if (LuaInviteNpcMgr.IsFriendlinessTask(task.TemplateId) and task.CanSubmit == 1) or task.TemplateId == SectInviteNpc_Setting.GetData().VisitTaskId then
        local extraStr = task.ExtraData.StringData
        if extraStr then
            local trackNpcId = string.match(extraStr, "sinpc:(%d+);")
            if trackNpcId then
                LuaInviteNpcMgr.TrackToNpc(trackNpcId)
                return true
            end
        end
    end
end)
--宗门 by linhao1 #156726 宗派任务
table.insert(g_TaskListItemClickResponseList, 9, function(taskId, task, taskTemplate)
    if CLuaTaskListItem:ProcessSchoolSectTask(task) then
        return true
    end
end)
--蜃妖 by wuyihang #171711 【专属蜃妖】任务栏
table.insert(g_TaskListItemClickResponseList, 10, function(taskId, task, taskTemplate)
    if taskTemplate.GamePlay == "ShenYao" then
        LuaXinBaiLianDongMgr:ProcessExclusiveShenYaoTask()
        return true
    end
end)
table.insert(g_TaskListItemClickResponseList, 11, function(taskId, task, taskTemplate)
    if taskTemplate.GamePlay == "GTWRelated" and not LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
        Gac2Gas.RequestTerritoryWarTeleport(0, 0)
        return true
    end
end)
table.insert(g_TaskListItemClickResponseList, 12, function(taskId, task, taskTemplate)
    if CScene.MainScene then
        local playTemplate = Gameplay_Gameplay.GetData(CScene.MainScene.GamePlayDesignId)
        if playTemplate and playTemplate.SubGameplay == "GTWRelated" and taskTemplate.GamePlay == "QianShi" then
            local location = CTaskMgr.Inst:GetTaskTrackPos(task)
            CTaskMgr.Inst:TrackToTaskLocation(location)
            return true
        end
    end
end)
-- 天地棋局战龙任务
table.insert(g_TaskListItemClickResponseList, 13, function(taskId, task, taskTemplate)
    if taskTemplate.GamePlay == "GuildJuDianZhanLong" then
        local location = CTaskMgr.Inst:GetTaskTrackPos(task)
        if location.sceneTemplateId > 0 then
            Gac2Gas.GuildJuDianTaskTeleportToPrepare()
        else
            CTaskMgr.Inst:TrackToTaskLocation(location)
        end
        return true
    end
end)

CLuaTaskListItem.m_ProcessActivityCallbackMap = {} 

CLuaTaskListItem.m_ProcessActivityCallbackMap[51103060] = function(this, act)
    this.warmChristmasEveProgress.gameObject:SetActive(true)
    local finishedSubTimes = LuaChristmas2022Mgr.m_CurGiftTimes or 0
    for i=0,this.warmChristmasEveSplits.Length-1 do
        local stage = math.floor(i / 3) + 1
        local offset = (i - (stage-1)*3) 
        local show = offset < (LuaChristmas2022Mgr.m_CurGiftTimes[stage] or 0)
        this.warmChristmasEveSplits[i].spriteName = show and "main_playerwnd_achievement_star_highlight" or "main_playerwnd_achievement_star_normal"
    end
    for i=0,this.warmChristmasEvePhases.Length-1 do
        CUICommonDef.SetGrey(this.warmChristmasEvePhases[i].gameObject,i >= (LuaChristmas2022Mgr.m_PlayStage or 1))
    end
end