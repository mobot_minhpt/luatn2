-- Auto Generated!!
local BoxCollider = import "UnityEngine.BoxCollider"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CKeJuQuestionTemplate = import "L10.UI.CKeJuQuestionTemplate"
local CommonDefs = import "L10.Game.CommonDefs"
local CScene = import "L10.Game.CScene"
local EnumKeJuStatus = import "L10.Game.EnumKeJuStatus"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local KeJu_Setting = import "L10.Game.KeJu_Setting"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local SoundManager = import "SoundManager"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local XiaLvPK_Setting = import "L10.Game.XiaLvPK_Setting"
CKeJuQuestionTemplate.m_Init_CS2LuaHook = function (this, isAudio) 
    this:InitWithPlayOption(isAudio, false)
end
CKeJuQuestionTemplate.m_InitWithPlayOption_CS2LuaHook = function (this, isAudio, autoPlay) 

    CommonDefs.ListClear(this.answerList)
    this.topicScrollView:ResetPosition()
    this.optionTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.optionTable.transform)

    do
        local i = 0
        while i < CKeJuMgr.Inst.AnswerList.Count do
            local option = NGUITools.AddChild(this.optionTable.gameObject, this.optionTemplate)
            option:SetActive(true)
            local optionLabel = ""
            repeat
                local default = i
                if default == (0) then
                    optionLabel = "A."
                    break
                elseif default == (1) then
                    optionLabel = "B."
                    break
                elseif default == (2) then
                    optionLabel = "C."
                    break
                elseif default == (3) then
                    optionLabel = "D."
                    break
                else
                    optionLabel = ""
                    break
                end
            until 1
            optionLabel = optionLabel .. CKeJuMgr.Inst.AnswerList[i]
            local script = CommonDefs.GetComponent_GameObject_Type(option, typeof(CCommonLuaScript))
            script:Init({optionLabel, i})
            CommonDefs.ListAdd(this.answerList, typeof(GameObject), option)
            if (not CKeJuMgr.Inst.huishiValid and not CKeJuMgr.Inst.wenshiValid) and CKeJuMgr.Inst.CurrentStatus ~= EnumKeJuStatus.xiangshi then
                CommonDefs.GetComponent_GameObject_Type(option, typeof(BoxCollider)).size = Vector3(1, 1, 1)
            else
                UIEventListener.Get(option).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(option).onClick, MakeDelegateFromCSFunction(this.OnAnswerClick, VoidDelegate, this), true)
                CommonDefs.GetComponent_GameObject_Type(option, typeof(BoxCollider)).size = Vector3(CommonDefs.GetComponent_GameObject_Type(option, typeof(UISprite)).width, CommonDefs.GetComponent_GameObject_Type(option, typeof(UISprite)).height, 1)
            end
            i = i + 1
        end
    end
    this.optionTable:Reposition()
    this:UpdateKejuQuestion(isAudio, autoPlay)

    if System.String.IsNullOrEmpty(CKeJuMgr.Inst.lastComment) then
        this.commentGo:SetActive(false)
    else
        this.comments.text = LocalString.GetString(CKeJuMgr.Inst.lastComment)
        this.commentGo:SetActive(true)
    end
    this.topicScrollView:ResetPosition()

    this.curNumberLabel.text = ""
    this.totalQuestionCount = 0
    repeat
        local extern = CKeJuMgr.Inst.CurrentStatus
        if extern == (EnumKeJuStatus.xiangshi) then
            this.totalQuestionCount = KeJu_Setting.GetData().XiangShiDailyQuestionNum
            break
        elseif extern == (EnumKeJuStatus.huishi) then
            this.totalQuestionCount = KeJu_Setting.GetData().HuiShiQuestionNum
            break
        elseif extern == (EnumKeJuStatus.dianshi) then
            this.totalQuestionCount = KeJu_Setting.GetData().DianShiQuestionNum
            break
        elseif extern == (EnumKeJuStatus.wenshi) then
            this.totalQuestionCount = XiaLvPK_Setting.GetData().WenShiQuestionNum
            break
        else
            break
        end
    until 1
    if CKeJuMgr.Inst.currentQuestionIndex > 0 and CKeJuMgr.Inst.currentQuestionIndex <= this.totalQuestionCount then
        this.curNumberLabel.text = System.String.Format(LocalString.GetString("第{0}/{1}题"), CKeJuMgr.Inst.currentQuestionIndex, this.totalQuestionCount)
    end
end
CKeJuQuestionTemplate.m_UpdateKejuQuestion_CS2LuaHook = function (this, isAudio, autoPlay)
    SoundManager.Inst:ChangeBgMusicVolume("kejuplayingvoice", 0.5, true) 
    this.audioButton:SetActive(isAudio)
    if not isAudio then
        this.topicLabel.text = CKeJuMgr.Inst.Question
    else
        local info = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(CKeJuMgr.Inst.Question, Table2ArrayWithCount({"|"}, 1, MakeArrayClass(String)), System.StringSplitOptions.RemoveEmptyEntries)
        if info ~= nil and info.Length == 2 then
            this.topicLabel.text = info[0]
            Extensions.SetLocalPositionY(this.audioButton.transform, - this.topicLabel.height)
            this.audioPath = info[1]
        else
            this.topicLabel.text = ""
            Extensions.SetLocalPositionY(this.audioButton.transform, 0)
            this.audioPath = CKeJuMgr.Inst.Question
        end
        if autoPlay then
            this:OnAudioButtonClick(this.audioButton)
        end
    end
end
CKeJuQuestionTemplate.m_OnAnswerClick_CS2LuaHook = function (this, go) 

    --todo:
    do
        local i = 0
        while i < this.answerList.Count do
            CommonDefs.GetComponent_GameObject_Type(this.answerList[i], typeof(BoxCollider)).size = Vector3(1, 1, 1)
            UIEventListener.Get(this.answerList[i]).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.answerList[i]).onClick, MakeDelegateFromCSFunction(this.OnAnswerClick, VoidDelegate, this), false)
            if go == this.answerList[i] then
                this.selectIndex = (i + 1)
                repeat
                    local default = CKeJuMgr.Inst.CurrentStatus
                    if default == (EnumKeJuStatus.xiangshi) then
                        Gac2Gas.SubmitXiangShiAnswer(this.selectIndex)
                        break
                    elseif default == (EnumKeJuStatus.huishi) then
                        Gac2Gas.SubmitHuiShiAnswer(CKeJuMgr.Inst.currentQuestionIndex, this.selectIndex)
                        break
                    elseif default == (EnumKeJuStatus.dianshi) then
                        Gac2Gas.SubmitDianShiAnswer(CKeJuMgr.Inst.currentQuestionIndex + KeJu_Setting.GetData().HuiShiQuestionNum, this.selectIndex)
                        break
                    elseif default == (EnumKeJuStatus.wenshi) then
                        do
                            local setting = XiaLvPK_Setting.GetData()
                            if CScene.MainScene ~= nil and CScene.MainScene.GamePlayDesignId == setting.CrossGamePlayId then
                                Gac2Gas.CrossXiaLvPkSubmitWenShiAnswer(CKeJuMgr.Inst.currentQuestionIndex, this.selectIndex)
                            else
                                Gac2Gas.XiaLvPkSubmitWenShiAnswer(CKeJuMgr.Inst.currentQuestionIndex, this.selectIndex)
                            end
                            break
                        end
                    end
                until 1
            end
            i = i + 1
        end
    end
end
CKeJuQuestionTemplate.m_OnAudioButtonClick_CS2LuaHook = function (this, go) 
    if this.playEvent then
        SoundManager.Inst:StopSound(this.playEvent)
    end
    this.playEvent = SoundManager.Inst:PlayOneShotWithMaxSameEventLimit(this.audioPath, 1, Vector3.zero, nil, 0, 1, true)
    this.audioAnim:PlayForward()
    SoundManager.Inst:ChangeBgMusicVolume("kejuplayingvoice", 0.5, false)
end
CKeJuQuestionTemplate.m_OnQuestionAnswerUpdate_CS2LuaHook = function (this) 

    if CKeJuMgr.Inst.CurrentStatus ~= EnumKeJuStatus.xiangshi and not CKeJuMgr.Inst.huishiValid and not CKeJuMgr.Inst.wenshiValid then
        this:StartCoroutine(this:ShowRightAnswer())
    elseif this.selectIndex ~= CKeJuMgr.Inst.rightAnswerIndex then
        --MessageMgr.Inst.ShowMessage("KEJU_WRONG_ANSWER");
        this:StartCoroutine(this:ShowRightAnswer())
    else
        if CKeJuMgr.Inst.CurrentStatus == EnumKeJuStatus.xiangshi and CKeJuMgr.Inst.xiangshiCorrectCount == KeJu_Setting.GetData().JoinHuiShiQuestionNum then
            g_MessageMgr:ShowMessage("KEJU_PASS_XIANGSHI")
        end
        if CKeJuMgr.Inst.CurrentStatus == EnumKeJuStatus.xiangshi then
            this:InitWithPlayOption(CKeJuMgr.Inst.bVoice, true)
            if this.CorrectCountUpdate ~= nil then
                invoke(this.CorrectCountUpdate)
            end
        else
            this:StartCoroutine(this:ShowRightAnswer())
        end
    end
end
CKeJuQuestionTemplate.m_fixShowRightAnswer = function (this)
    local children = Extensions.GetChildren(this.optionTable, false)
    for i = 0,  children.Count - 1 do
        local option = children[i]:GetComponent(typeof(CCommonLuaScript))
        local collider = children[i]:GetComponent(typeof(BoxCollider))
        if (i == (this.selectIndex - 1)) then
            if option then
                option.m_LuaSelf:SetSelected()
            end
        end
        if ((i ~= CKeJuMgr.Inst.rightAnswerIndex - 1) and ( i == (this.selectIndex - 1))) then
            if option then
                option.m_LuaSelf:SetResult(false)
            end
        elseif ((i ~= CKeJuMgr.Inst.rightAnswerIndex - 1) and (i ~= (this.selectIndex - 1))) then
            if collider then
                collider.size = Vector3.one
            end
        else
            if collider then
                collider.size = Vector3.one
            end
            if option then
                option.m_LuaSelf:SetResult(true)
            end
        end

    end
end
