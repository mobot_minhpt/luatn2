-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerProSaveMgr = import "L10.Game.CPlayerProSaveMgr"
local CTalismanEquipView = import "L10.UI.CTalismanEquipView"
local CTalismanMenuMgr = import "L10.UI.CTalismanMenuMgr"
local CTalismanPositionTemplate = import "L10.UI.CTalismanPositionTemplate"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local IdPartition = import "L10.Game.IdPartition"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local SoundManager = import "SoundManager"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanEquipView.m_Init_CS2LuaHook = function (this, index) 

    this.curSelectMenuIndex = index
    this:InitMenu()
    this.equipScoreLabel.text = nil
    if CClientMainPlayer.Inst ~= nil then
        this.lastEquipScore = CClientMainPlayer.Inst.EquipScore
        this.equipScoreLabel.text = tostring(CClientMainPlayer.Inst.EquipScore)
    end
    Extensions.RemoveAllChildren(this.table.transform)
    this.talismanTemplate:SetActive(false)
    CommonDefs.ListClear(this.positionList)

    this:InitTalisman()

    this.table:Reposition()
end
CTalismanEquipView.m_InitTalisman_CS2LuaHook = function (this) 
    this:InitPoint()
    CPlayerProSaveMgr.Inst:savePro()
    local startIdx = EnumBodyPosition_lua.TalismanQian + 8 * this.curSelectMenuIndex
    local endIdx = EnumBodyPosition_lua.TalismanDui + 8 * this.curSelectMenuIndex
    do
        local i = startIdx
        while i <= endIdx do
            local instance = NGUITools.AddChild(this.table.gameObject, this.talismanTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanPositionTemplate))
            template:Init(i)
            CommonDefs.ListAdd(this.positionList, typeof(CTalismanPositionTemplate), template)
            UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnPositionClicked, VoidDelegate, this), true)
            i = i + 1
        end
    end
end
CTalismanEquipView.m_InitMenu_CS2LuaHook = function (this) 
    this.menuLabel.text = System.String.Format(LocalString.GetString("法宝栏{0}"), this.curSelectMenuIndex + 1)
    this.menuButton.spriteName = this.highlightSpriteName
    Extensions.SetLocalRotationZ(this.menuArrow, 0)
    UIEventListener.Get(this.menuButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnMenuButtonClick, VoidDelegate, this)
end
CTalismanEquipView.m_InitPoint_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.pointTable.transform)
    this.pointTemplate:SetActive(false)
    if CTalismanMenuMgr.talismanMenu2Open and CClientMainPlayer.Inst then
        local len = CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanNum 
        for i = 0, len - 1 do
            local instance = NGUITools.AddChild(this.pointTable.gameObject, this.pointTemplate)
            instance:SetActive(true)
            local sprite = CommonDefs.GetComponent_GameObject_Type(instance, typeof(UISprite))
            if sprite ~= nil then
                sprite.spriteName = this.normalPointName
                if i == CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanCurIndex then
                    sprite.spriteName = this.highlightPointName
                end
            end
        end
        this.pointTable:Reposition()
    end
end
CTalismanEquipView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.MainPlayerTotalEquipScoreUpdate, MakeDelegateFromCSFunction(this.UpdateEquipScore, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnTalismanMenuSelect, MakeDelegateFromCSFunction(this.Init, MakeGenericClass(Action1, Int32), this))
    EventManager.RemoveListener(EnumEventType.OnTalismanMenuClose, MakeDelegateFromCSFunction(this.InitMenu, Action0, this))
    CUIManager.CloseUI(CUIResources.TalismanMenu)
end
CTalismanEquipView.m_UpdateTalisman_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < this.positionList.Count do
            this.positionList[i]:Init(this.positionList[i].position)
            i = i + 1
        end
    end
end
CTalismanEquipView.m_OnPositionClicked_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.positionList.Count do
            if go == this.positionList[i].gameObject then
                local itemProp = CClientMainPlayer.Inst.ItemProp
                local itemId = itemProp:GetItemAt(EnumItemPlace.Body, this.positionList[i].position)
                local item = CItemMgr.Inst:GetById(itemId)
                if item ~= nil then
                    CItemInfoMgr.ShowLinkItemInfo(item, false, this, AlignType.Default, 0, 0, 0, 0, 0)
                end
            end
            i = i + 1
        end
    end
end
CTalismanEquipView.m_UpdateEquipScore_CS2LuaHook = function (this) 

    if CClientMainPlayer.Inst ~= nil then
        local newEquipScore = CClientMainPlayer.Inst.EquipScore
        if this.lastEquipScore ~= newEquipScore then
            this:StopAllCoroutines()
            this:StartCoroutine(this:DoNumberChangeAni(this.lastEquipScore, newEquipScore, 1))
            this.lastEquipScore = newEquipScore
        end
    end
    CPlayerProSaveMgr.Inst:updatePro()
end
CTalismanEquipView.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    this.SelectedItemId = itemId
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local equipAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("卸下"), MakeDelegateFromCSFunction(this.OnTakeOff, Action0, this))
    local baptizeAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("打造"), MakeDelegateFromCSFunction(this.OnBaptize, Action0, this))
    local baptizeExtraAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("打造"), MakeDelegateFromCSFunction(this.OnBaptizeExtraWord, Action0, this))
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.IsEquip and IdPartition.IdIsTalisman(item.TemplateId) then
        this.SelectPos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, itemId)
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), equipAction)
        if item.Equip.IsBlueEquipment then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), baptizeExtraAction)
        elseif item.Equip.IsFairyTalisman or item.Equip.IsPremierTalisman then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), baptizeAction)
        end
    end
    return actionPairs
end
CTalismanEquipView.m_OnTakeOff_CS2LuaHook = function (this) 

    CItemInfoMgr.CloseItemInfoWnd()
    if this.SelectedItemId == nil then
        return
    end
    local item = CItemMgr.Inst:GetById(this.SelectedItemId)
    if item == nil then
        return
    end

    if not IdPartition.IdIsEquip(item.TemplateId) then
        return
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Equip, Vector3.zero, nil, 0)
    Gac2Gas.RequestTakeOffEquip(EnumItemPlace_lua.Body, this.SelectPos, this.SelectedItemId)
end
