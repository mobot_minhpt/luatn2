local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Color = import "UnityEngine.Color"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientNpc = import "L10.Game.CClientNpc"
local CameraFollow=import "L10.Engine.CameraControl.CameraFollow"
local EKickOutType = import "L10.Engine.EKickOutType"
local CRenderObject=import "L10.Engine.CRenderObject"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CMainCamera = import "L10.Engine.CMainCamera"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaMengQuanHengXingTopRightWnd = class()

function LuaMengQuanHengXingTopRightWnd:Awake()
    self:InitUIComponent()
    self:InitUIData()
    UIEventListener.Get(self.explandBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnExplandBtnClick()
    end)
end

function LuaMengQuanHengXingTopRightWnd:InitUIComponent()
    self.topNode1 = self.transform:Find("Anchor/tipPanel/node/TopNode1")
    self.topNode2 = self.transform:Find("Anchor/tipPanel/node/TopNode2")
    self.topNode3 = self.transform:Find("Anchor/tipPanel/node/TopNode3")
    self.topNode4 = self.transform:Find("Anchor/tipPanel/node/TopNode4")
    self.explandBtn = self.transform:Find("Anchor/Tip/ExplandBtn")
    self.rankNode = self.transform:Find("Anchor/tipPanel")

    self.countDown = self.transform:Find("Anchor/tipPanel/node/CountDown")
    self.remainTimeLabel = self.transform:Find("Anchor/tipPanel/node/CountDown/RemainTimeLabel"):GetComponent(typeof(UILabel))
end

function LuaMengQuanHengXingTopRightWnd:InitUIData()
    self.mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    self.myLastScroe = nil
    
    local MQHXConfigData = Double11_MQHX.GetData()
    self.dogNpcIds = {}
    for i = 1, MQHXConfigData.DogsNpc.Length do
        local NpcId = MQHXConfigData.DogsNpc[i-1][0]
        self.dogNpcIds[NpcId] = true
    end
    
    self.colliderDistance = MQHXConfigData.ColliderCheckDistance
    self.movementDistance = MQHXConfigData.ColliderMovementDistance
    self.movementRatio = MQHXConfigData.MovementCombinationRatio / (MQHXConfigData.MovementCombinationRatio + 1.0)
    self.flyGravity = MQHXConfigData.flyGravity

    if LuaShuangshiyi2022Mgr.MQHXPlayInfo then
        self:OnSyncMengQuanHengXingRankInfo(LuaShuangshiyi2022Mgr.MQHXPlayInfo.RankTbl_U)
        local playState = LuaShuangshiyi2022Mgr.MQHXPlayInfo.playState
        local endTime = LuaShuangshiyi2022Mgr.MQHXPlayInfo.endTime
        if playState == EnumDouble11MQHXPlayState.Normal then
            self:OnSyncMengQuanHengXingEndTime(endTime + Double11_MQHX.GetData().PlayCrazyDuration, true)
        elseif playState == EnumDouble11MQHXPlayState.Normal then
            self:OnSyncMengQuanHengXingEndTime(Double11_MQHX.GetData().PlayCrazyDuration, false)
        elseif playState == EnumDouble11MQHXPlayState.Normal then
            self:OnSyncMengQuanHengXingEndTime(endTime, true)
        end
    end
end

function LuaMengQuanHengXingTopRightWnd:OnExplandBtnClick()
    self.rankNode.gameObject:SetActive(false)
    self.explandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaMengQuanHengXingTopRightWnd:OnHideTopAndRightTipWnd()
    self.rankNode.gameObject:SetActive(true)
    self.explandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaMengQuanHengXingTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncMengQuanHengXingRankInfo", self, "OnSyncMengQuanHengXingRankInfo")
    g_ScriptEvent:AddListener("Double11MQHX_BroadCastAction", self, "OnReceiveMQHXBroadCastAction")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:AddListener("SyncMengQuanHengXingEndTime", self, "OnSyncMengQuanHengXingEndTime")
end

function LuaMengQuanHengXingTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncMengQuanHengXingRankInfo", self, "OnSyncMengQuanHengXingRankInfo")
    g_ScriptEvent:RemoveListener("Double11MQHX_BroadCastAction", self, "OnReceiveMQHXBroadCastAction")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("SyncMengQuanHengXingEndTime", self, "OnSyncMengQuanHengXingEndTime")

end

function LuaMengQuanHengXingTopRightWnd:Init()
end

function LuaMengQuanHengXingTopRightWnd:OnSyncMengQuanHengXingRankInfo(rankData)
    local unpackRankData = g_MessagePack.unpack(rankData)
    for i = 1, #unpackRankData do
        self:RefreshTopNode(i, unpackRankData[i])
    end
    for i = #unpackRankData+1, 4 do
        self:RefreshTopNode(i, nil)
    end

    if CUIManager.IsLoaded(CLuaUIResources.Shuangshiyi2022MainWnd) then
        CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2022MainWnd)
    end
end

function LuaMengQuanHengXingTopRightWnd:RefreshTopNode(rank, info)
    local node = self["topNode" .. rank]
    if info then
        local nameLabel = node.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
        local scoreLabel = node.transform:Find("Score"):GetComponent(typeof(UILabel))
        local isMeBg = node.transform:Find("isMeBg")
        local isSelf = info.PlayerId == self.mainPlayerId
        nameLabel.text = info.Name
        nameLabel.color = isSelf and NGUIText.ParseColor("00ff16", 0) or NGUIText.ParseColor("ffffff", 0)
        scoreLabel.text = info.Score
        scoreLabel.color = isSelf and NGUIText.ParseColor("00ff16", 0) or NGUIText.ParseColor("ffffff", 0)
        isMeBg.gameObject:SetActive(isSelf)
        if isSelf then
            --判断一下是不是自己得分了, 得分的话播一下粒子特效
            if self.myLastScroe == nil then
                --初始化分数
                self.myLastScroe = info.Score
            else
                if info.Score > self.myLastScroe then
                    --得分啦
                    self:PlayScoreEffect(node.transform)
                    self.myLastScroe = info.Score
                end
            end
        end
        node.gameObject:SetActive(true)
    else
        node.gameObject:SetActive(false)
    end
end

function LuaMengQuanHengXingTopRightWnd:PlayScoreEffect(nodeTransform)
    local moveTime = 1
    local screenPos = CMainCamera.Main:WorldToScreenPoint(CClientMainPlayer.Inst.RO.Position)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    CCommonUIFxWnd.Instance:PlayLineAni(nguiWorldPos, nodeTransform.position, moveTime, DelegateFactory.Action(function (_)
    end))
end

function LuaMengQuanHengXingTopRightWnd:Update()
    local mainPlayerObj = CClientMainPlayer.Inst
    if mainPlayerObj and mainPlayerObj.IsDie then
        self.skipCheckColliderUntilDeath = false
    end
    if mainPlayerObj and self.skipCheckColliderUntilDeath then
        return
    end
    if mainPlayerObj and (not mainPlayerObj.IsDie) then
        local mainPlayerPosition = mainPlayerObj and mainPlayerObj.RO.Position
        local mainPlayerForwardDir = mainPlayerObj and CommonDefs.op_Multiply_Vector3_Single(mainPlayerObj.RO.transform.forward, 1-self.movementRatio)
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if TypeIs(obj, typeof(CClientNpc)) then
                if obj and self.dogNpcIds[obj.TemplateId] then
                    --check Collider
                    local npcWorldPosition = obj.RO.transform.position
                    local npcForwardDir = CommonDefs.op_Multiply_Vector3_Single(obj.RO.transform.forward, self.movementRatio)
                    local distance = Vector3.Distance(mainPlayerPosition, npcWorldPosition)
                    if distance <= self.colliderDistance then
                        --碰撞发生啦
                        CameraFollow.Inst:StopFollow()
                        local movementDirection = CommonDefs.op_Addition_Vector3_Vector3(mainPlayerForwardDir, npcForwardDir)
                        local moveOffset = Vector3.zero
                        --检查飞行过程中的碰撞
                        for curCheckDistance = 1, self.movementDistance do
                            local checkMoveOffset = CommonDefs.op_Multiply_Vector3_Single(movementDirection, curCheckDistance)
                            local dx = mainPlayerPosition.x + checkMoveOffset.x
                            local dy = mainPlayerPosition.z + checkMoveOffset.z
                            local barrier = CoreScene.MainCoreScene:GetBarrierv(dx, dy, true)
                            if BarrierType2Int[barrier] == 0 or BarrierType2Int[barrier] == 1 then
                                moveOffset = checkMoveOffset
                            else
                                break
                            end
                        end
                        mainPlayerObj.RO:SetKickOutStatus(true, EKickOutType.ParabolaWithoutRotate, 0.5, -1, self.flyGravity)
                        mainPlayerObj.RO.Position = Vector3(mainPlayerPosition.x + moveOffset.x, mainPlayerPosition.y,
                                mainPlayerPosition.z + moveOffset.z)
                        mainPlayerObj.RO:SetKickOutStatus(false, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -20)
                        local broadcastPosition = {math.floor(64*(mainPlayerPosition.x+moveOffset.x)), math.floor(64*(mainPlayerPosition.y)), math.floor(64*(mainPlayerPosition.z+moveOffset.z))}
                        self.skipCheckColliderUntilDeath = true
                        Gac2Gas.Double11MQHX_BroadCastAction(EnumDouble11MQHXAction.Fly, 
                                g_MessagePack.pack(broadcastPosition))
                        return
                    end
                end
            end
        end))
    end
end 

function LuaMengQuanHengXingTopRightWnd:OnReceiveMQHXBroadCastAction(playerId, action, data_U)
    if action == EnumDouble11MQHXAction.Fly then
        --把PlayerId的玩家，做一个飞起的表现，坐标为data_U中的位置
        local unpackPosition = g_MessagePack.unpack(data_U)
        local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if obj and TypeIs(obj, typeof(CClientOtherPlayer)) then
            obj.RO:SetKickOutStatus(true, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -50)
            obj.RO.Position = Vector3(unpackPosition[1]/64.0, unpackPosition[2]/64.0, unpackPosition[3]/64.0)
            obj.RO:SetKickOutStatus(false, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -20)
        end
    end
end

function LuaMengQuanHengXingTopRightWnd:OnRemainTimeUpdate()
    local mainScene = CScene.MainScene
    if mainScene then
        if self.runClock then
            self.countDown.gameObject:SetActive(true)
            self.remainTimeLabel.text = self:GetRemainTimeText(self.phaseFinishTime - CServerTimeMgr.Inst.timeStamp)
        else
            self.remainTimeLabel.text = self:GetRemainTimeText(self.phaseFinishTime or 0)
        end
    end
end

function LuaMengQuanHengXingTopRightWnd:OnSyncMengQuanHengXingEndTime(phaseFinishTime, runClock)
    self.phaseFinishTime = phaseFinishTime
    self.runClock = runClock
end

function LuaMengQuanHengXingTopRightWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),
                math.floor(totalSeconds / 3600),
                math.floor((totalSeconds % 3600) / 60),
                totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end