-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMPTZClassListItem = import "L10.UI.CMPTZClassListItem"
local CMPTZRankMasterView = import "L10.UI.CMPTZRankMasterView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
--local Initialization_Init = import "L10.Game.Initialization_Init"
local Object = import "System.Object"
local Profession = import "L10.Game.Profession"
local UInt32 = import "System.UInt32"
CMPTZRankMasterView.m_Init_CS2LuaHook = function (this) 

    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    CommonDefs.ListClear(this.classes)
    Initialization_Init.Foreach(function (key, data) 
        if data.Status == 0 then
            if not CommonDefs.ListContains(this.classes, typeof(UInt32), data.Class) then
                CommonDefs.ListAdd(this.classes, typeof(UInt32), data.Class)
            end
        end
    end)
    CommonDefs.ListSort1(this.classes, typeof(UInt32), DelegateFactory.Comparison_uint(function (val1, val2) 
        return NumberCompareTo(val1, val2)
    end))
    --从小到大排列
    local selectedIndex = 0
    if CClientMainPlayer.Inst ~= nil then
        local myClass = EnumToInt(CClientMainPlayer.Inst.Class)
        do
            local i = 0
            while i < this.classes.Count do
                if myClass == this.classes[i] then
                    selectedIndex = i
                    break
                end
                i = i + 1
            end
        end
    end
    this.tableView:LoadData(0, false)
    this.tableView.SelectedIndex = selectedIndex
    this:OnRowSelected(selectedIndex)
end
CMPTZRankMasterView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    if index < 0 and index >= this.classes.Count then
        return nil
    end
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.itemTemplate, cellIdentifier)
    end
    CommonDefs.GetComponent_GameObject_Type(cell, typeof(CMPTZClassListItem)).Text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.classes[index]))

    return cell
end
CMPTZRankMasterView.m_OnRowSelected_CS2LuaHook = function (this, index) 

    if index >= 0 and index < this.classes.Count then
        if this.OnClassSelected ~= nil then
            GenericDelegateInvoke(this.OnClassSelected, Table2ArrayWithCount({this.classes[index]}, 1, MakeArrayClass(Object)))
        end
    end
end
