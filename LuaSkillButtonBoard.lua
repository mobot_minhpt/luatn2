local CQiaoQiaoBan        = import "L10.Game.CQiaoQiaoBan"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local DelegateFactory     = import "DelegateFactory"
local GameObject          = import "UnityEngine.GameObject"
local EUIModuleGroup      = import "L10.UI.CUIManager+EUIModuleGroup"
local CZuoQiMgr           = import "L10.Game.CZuoQiMgr"
local EChatPanel          = import "L10.Game.EChatPanel"
local CGuanNingMgr        = import "L10.Game.CGuanNingMgr"
local CUIFx               = import "L10.UI.CUIFx"
local Extensions          = import "Extensions"
local UIProgressBar       = import "UIProgressBar"
local Vector3             = import "UnityEngine.Vector3"
local BoxCollider         = import "UnityEngine.BoxCollider"
local CStatusMgr          = import "L10.Game.CStatusMgr"
local EPropStatus         = import "L10.Game.EPropStatus"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"

LuaSkillButtonBoard = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSkillButtonBoard, "m_QiaoQiaoBanButton", "QiaoQiaoBanButton", GameObject)
RegistChildComponent(LuaSkillButtonBoard, "m_SeaFishingButton", "SeaFishingButton", GameObject)
RegistChildComponent(LuaSkillButtonBoard, "m_WeddingHelpAnswerButton", "WeddingHelpAnswerButton", GameObject)
RegistChildComponent(LuaSkillButtonBoard, "m_InviteWineHelpBtn", "InviteWineHelpBtn", GameObject)
RegistChildComponent(LuaSkillButtonBoard, "m_GuanNingZhiHuiFaZhenButton", "GuanNingZhiHuiFaZhenButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaSkillButtonBoard,"m_QiaoQiaoBanId")
RegistClassMember(LuaSkillButtonBoard,"m_LovingShowButton")

function LuaSkillButtonBoard:Awake()
    self.m_QiaoQiaoBanButton:SetActive(false)
    self.m_SeaFishingButton:SetActive(false)
    self.m_WeddingHelpAnswerButton:SetActive(false)
    self.m_InviteWineHelpBtn:SetActive(false)
    self.m_GuanNingZhiHuiFaZhenButton:SetActive(false)

    self.m_LovingShowButton = self.transform:Find("Anchor/Offset/Buttons/Valentine2023LovingShowButton").gameObject
    self.m_LovingShowButton:SetActive(CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("LovingShow")) == 1)

    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.m_QiaoQiaoBanButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQiaoQiaoBanButtonClick()
	end)
    UIEventListener.Get(self.m_SeaFishingButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSeaFishingButton()
	end)

    --@endregion EventBind end
    UIEventListener.Get(self.m_InviteWineHelpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInviteWineBtnClick()
	end)
    UIEventListener.Get(self.m_GuanNingZhiHuiFaZhenButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaGuanNingMgr:RequestPerformGnjcZhiHuiFaZhen()
        local curProgress = 0
        local totalProgress = GuanNing_Setting.GetData().ZhiHuiFaZhenTotalProgress
        if CLuaGuanNingMgr.m_ZhiHuiFaZhenProgressValue < 1 then
            curProgress = CLuaGuanNingMgr.m_ZhiHuiFaZhenProgressValue * totalProgress
        end
        self:OnSyncGnjcZhiHuiFaZhenProgress(curProgress, totalProgress)
	end)
end

function LuaSkillButtonBoard:Init()
    self.m_SeaFishingButton:SetActive(LuaSeaFishingMgr.m_IsShowEntryButton)
    self.m_GuanNingZhiHuiFaZhenButton:SetActive(CGuanNingMgr.Inst.inGnjc and CLuaGuanNingMgr:IsCommander())
end

--@region UIEvent

function LuaSkillButtonBoard:OnQiaoQiaoBanButtonClick()
	Gac2Gas.RequestPressQiaoqiaoban(self.m_QiaoQiaoBanId)
end

function LuaSkillButtonBoard:OnSeaFishingButton()
    local cls = HouseFish_Setting.GetData().FishSkillId
    if not CClientMainPlayer.Inst then return end
	local skillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(cls, CClientMainPlayer.Inst.Level)
	local fishingSkillLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(skillId)
    --在坐骑上不能进入钓鱼模式
    if CZuoQiMgr.IsOnZuoqi() then
        g_MessageMgr:ShowMessage("SeaFishing_CanNot_OnZuoQi")
        return
    end
    if fishingSkillLevel and fishingSkillLevel > 0 then
        local excepts = {"MiddleNoticeCenter","GuideWnd"}
        local List_String = MakeGenericClass(List,cs_string)
        CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
        CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
        
        local list
        if CUIManager.instance then
            list = CUIManager.instance.popupUIStack
        end
        if list then
            for i=0,list.Count-1,1 do
                local uiModule = list[i]
                CUIManager.CloseUI(uiModule)
            end

        end 
        CUIManager.ShowUI(CUIResources.HouseFishingWnd)
    else
        g_MessageMgr:ShowMessage("Please_Learn_SeaFishingSkill_First")
    end
end

--@endregion UIEvent


function LuaSkillButtonBoard:OnEnable()
    g_ScriptEvent:AddListener("PlayerPressQiaoqiaoban", self, "OnPlayerPressQiaoqiaoban")
    g_ScriptEvent:AddListener("MainPlayerAttachFurniture", self, "OnMainPlayerAttachFurniture")
    g_ScriptEvent:AddListener("MainPlayerDetachFurniture", self, "OnMainPlayerDetachFurniture")
    g_ScriptEvent:AddListener("InitAdditionalSkill", self, "OnInitAdditionalSkill")
    g_ScriptEvent:AddListener("OnSwitchFishingPannel", self, "OnSwitchFishingPannel")--OnSwitchInviteWinePannel
    g_ScriptEvent:AddListener("OnSwitchInviteWinePannel", self, "OnSwitchInviteWinePannel")
    g_ScriptEvent:AddListener("SwitchGuanNingPanel", self, "OnSwitchGuanNingPanel")

    g_ScriptEvent:AddListener("SetWeddingHelpAnswerButtonActive", self, "OnSetWeddingHelpAnswerButtonActive")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("SyncGnjcZhiHuiFaZhenProgress", self, "OnSyncGnjcZhiHuiFaZhenProgress")

    g_ScriptEvent:AddListener("UpdateSkillButtonBoardState", self, "OnUpdateSkillButtonBoardState")
end

function LuaSkillButtonBoard:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerPressQiaoqiaoban", self, "OnPlayerPressQiaoqiaoban")
    g_ScriptEvent:RemoveListener("MainPlayerAttachFurniture", self, "OnMainPlayerAttachFurniture")
    g_ScriptEvent:RemoveListener("MainPlayerDetachFurniture", self, "OnMainPlayerDetachFurniture")
    g_ScriptEvent:RemoveListener("InitAdditionalSkill", self, "OnInitAdditionalSkill")
    g_ScriptEvent:RemoveListener("OnSwitchFishingPannel", self, "OnSwitchFishingPannel")
    g_ScriptEvent:RemoveListener("OnSwitchInviteWinePannel", self, "OnSwitchInviteWinePannel")
    g_ScriptEvent:RemoveListener("SwitchGuanNingPanel", self, "OnSwitchGuanNingPanel")

    g_ScriptEvent:RemoveListener("SetWeddingHelpAnswerButtonActive", self, "OnSetWeddingHelpAnswerButtonActive")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("SyncGnjcZhiHuiFaZhenProgress", self, "OnSyncGnjcZhiHuiFaZhenProgress")

    g_ScriptEvent:RemoveListener("UpdateSkillButtonBoardState", self, "OnUpdateSkillButtonBoardState")
end

function LuaSkillButtonBoard:OnUpdateSkillButtonBoardState(state)
    if CSkillButtonBoardWnd.Instance then
        if state.show ~= nil then
            if state.show then CSkillButtonBoardWnd.Show(false)
            else CSkillButtonBoardWnd.Hide(false) end
        end
        if state.showGuaJiBtn ~= nil then
            local guajiBtn = CSkillButtonBoardWnd.Instance.guajiBtn
            if not CommonDefs.IsNull(guajiBtn) then
                guajiBtn:SetActive(state.showGuaJiBtn)
            end
        end
        if state.showSwitchTargetBtn ~= nil then
            local switchTargetBtn = CSkillButtonBoardWnd.Instance:GetSwitchTargetButton()
            if not CommonDefs.IsNull(switchTargetBtn) then
                switchTargetBtn:SetActive(state.showSwitchTargetBtn)
            end
        end
        if state.showHpRecoverBtn ~= nil then
            local hpRecoverButton = CSkillButtonBoardWnd.Instance:GetHpRecoverButton()
            if not CommonDefs.IsNull(hpRecoverButton) then
                hpRecoverButton:SetActive(state.showHpRecoverBtn)
            end
        end
    end
end

function LuaSkillButtonBoard:OnSyncGnjcZhiHuiFaZhenProgress(currentProgress, totalProgress)
    if not CGuanNingMgr.Inst.inGnjc then return end

    local value = currentProgress / totalProgress
    CLuaGuanNingMgr.m_ZhiHuiFaZhenProgressValue = value
    self.m_GuanNingZhiHuiFaZhenButton:SetActive(CLuaGuanNingMgr:IsCommander())

    self.m_GuanNingZhiHuiFaZhenButton.transform:Find("ProgressBar"):GetComponent(typeof(UIProgressBar)).value = value
    Extensions.SetLocalPositionZ(self.m_GuanNingZhiHuiFaZhenButton.transform:Find("Mask").transform, value < 1 and -1 or 0 )
    local fx = self.m_GuanNingZhiHuiFaZhenButton.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    if value >= 1 then
        fx:LoadFx("fx/ui/prefab/UI_Guanning_fazhen.prefab")
    else
        fx:DestroyFx()
    end
    local angle = math.lerp(90, -270, value)
    fx = self.m_GuanNingZhiHuiFaZhenButton.transform:Find("ThumbFx"):GetComponent(typeof(CUIFx))
    fx.transform.localPosition = CommonDefs.op_Multiply_Single_Vector3(50, Vector3(math.cos(angle * Deg2Rad), math.sin(angle * Deg2Rad),0))
    fx.gameObject:SetActive(value < 1)
end

function LuaSkillButtonBoard:OnPlayerPressQiaoqiaoban(fid, playerId)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(fid)
    if fur and fur.Children then
        for i=1,fur.Children.Length do
            local id = fur.Children[i-1]
            if id==playerId then
                local ro = fur.RO
                if ro then
                    local cmp = ro:GetComponentInChildren(typeof(CQiaoQiaoBan))
                    if cmp then
                        cmp:AddForce(i-1)
                    end
                end
                break
            end
        end
    end
end

function LuaSkillButtonBoard:OnMainPlayerAttachFurniture(furnitureId,slotIdx)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if fur and fur.TemplateId==9305 then--跷跷板
        self.m_QiaoQiaoBanButton:SetActive(true)
        self.m_QiaoQiaoBanId = furnitureId
    end
end
function LuaSkillButtonBoard:OnMainPlayerDetachFurniture()
    --不显示按钮
    self.m_QiaoQiaoBanButton:SetActive(false)
end
function LuaSkillButtonBoard:OnInitAdditionalSkill(this)
    local minX = 23
    local node = this.transform:Find("Anchor/Offset/Buttons")
    for i=1,node.childCount do
        local child = node:GetChild(i-1)
        local needProcess = true
        if this.YanHuaViewBtn == child.gameObject then
            --这个烟花按钮实现有点特殊
            if CLuaYanHuaEditorMgr.ViewEnterTime then
                needProcess = false
            end
        end
        if needProcess and child.gameObject.activeSelf and (child.gameObject.name ~= "XianZhiButton" or child:GetComponent(typeof(BoxCollider)).enabled) -- 仙职技能隐藏时需要判断
        and child.gameObject~=self.m_QiaoQiaoBanButton and child.gameObject~=self.m_SeaFishingButton and child.gameObject~=self.m_InviteWineHelpBtn and child.gameObject~=self.m_GuanNingZhiHuiFaZhenButton then
            if child.localPosition.x<minX then
                minX=child.localPosition.x
            end
        end
    end
    local pos = self.m_QiaoQiaoBanButton.transform.localPosition
    self.m_QiaoQiaoBanButton.transform.localPosition = Vector3(minX-140,pos.y,pos.z)
    self.m_SeaFishingButton.transform.localPosition = Vector3(minX-140,pos.y,pos.z)
    self.m_InviteWineHelpBtn.transform.localPosition = Vector3(minX-140,pos.y,pos.z)
    self.m_GuanNingZhiHuiFaZhenButton.transform.localPosition = Vector3(minX-140,pos.y,pos.z)
end

function LuaSkillButtonBoard:OnSwitchGuanNingPanel(bIsOpen)
    self.m_GuanNingZhiHuiFaZhenButton:SetActive(bIsOpen)
end

function LuaSkillButtonBoard:OnSwitchFishingPannel(bIsOpen)
    self.m_SeaFishingButton:SetActive(bIsOpen)
end

function LuaSkillButtonBoard:OnSwitchInviteWinePannel(bIsOpen)
    if not LuaMeiXiangLouMgr.s_OpenPlay then
        return
    end
    self.m_InviteWineHelpBtn:SetActive(bIsOpen)
end

function LuaSkillButtonBoard:OnInviteWineBtnClick()
    LuaMeiXiangLouMgr.AskSomeoneHelp(EChatPanel.Current)
end

function LuaSkillButtonBoard:GetGuideGo(methodName)
    if methodName == "GetSeaFishingButton" then
        return self.m_SeaFishingButton.gameObject
    elseif methodName == "GetWeddingHelpAnswerButton" then
        return self.m_WeddingHelpAnswerButton.gameObject
    elseif methodName == "GetFaZhenBtn" then
		return self.m_GuanNingZhiHuiFaZhenButton
	end
    return nil
end

-- 婚礼助答按钮
function LuaSkillButtonBoard:OnSetWeddingHelpAnswerButtonActive(active)
    self.m_WeddingHelpAnswerButton:SetActive(active)
end

function LuaSkillButtonBoard:OnMainPlayerCreated()
    self.m_LovingShowButton:SetActive(LuaValentine2023Mgr:IsInYWQSScene())
end
