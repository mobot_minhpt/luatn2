local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CRankData = import "L10.UI.CRankData"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaYuanXiaoDefenseSignUpWnd = class()

RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "TimeLabel")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "TypeLabel")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "NeedLabel")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "RankLabel")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "DescLabel")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "RankBtn")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "Item1")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "Item2")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "Item3")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "RuleBtn")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "GoBtn")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "isMatching")
RegistClassMember(LuaYuanXiaoDefenseSignUpWnd, "RewardHint")

function LuaYuanXiaoDefenseSignUpWnd:Awake()
    self.TimeLabel = self.transform:Find("Anchor/RightView/Content/BasicInfo/Time/Info"):GetComponent(typeof(UILabel))
    self.TypeLabel = self.transform:Find("Anchor/RightView/Content/BasicInfo/Type/Info"):GetComponent(typeof(UILabel))
    self.NeedLabel = self.transform:Find("Anchor/RightView/Content/BasicInfo/Need/Info"):GetComponent(typeof(UILabel))
    self.RankLabel = self.transform:Find("Anchor/LeftView/Rank/Label"):GetComponent(typeof(UILabel))
    self.DescLabel = self.transform:Find("Anchor/RightView/Content/BasicInfo/Desc/Info"):GetComponent(typeof(UILabel))
    self.RankBtn = self.transform:Find("Anchor/LeftView/Rank/RankButton").gameObject
    self.RankBtn:GetComponent(typeof(BoxCollider)).size = Vector3(100, 100, 0)
    self.RuleBtn = self.transform:Find("Anchor/RightView/RuleButton").gameObject
    self.GoBtn = self.transform:Find("Anchor/RightView/GoButton").gameObject
    self.Item1 = self.transform:Find("Anchor/RightView/Content/DailyReward/Item1").gameObject
    self.Item2 = self.transform:Find("Anchor/RightView/Content/DailyReward/Item2").gameObject
    self.Item3 = self.transform:Find("Anchor/RightView/Content/DailyReward/Item3").gameObject
    self.RewardHint = self.transform:Find("Anchor/RightView/Content/DailyReward/Hint"):GetComponent(typeof(UILabel))
end

function LuaYuanXiaoDefenseSignUpWnd:Init()
    local setting = YuanXiao2023_NaoYuanXiao.GetData()
    self.TimeLabel.text = setting.PlayTime
    self.TypeLabel.text = setting.TaskMode
    self.NeedLabel.text = setting.GradeLimit
    self.DescLabel.text = setting.TaskDescription

    UIEventListener.Get(self.RankBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    CUIManager.ShowUI(CLuaUIResources.YuanXiaoDefenseRankWnd)
	end)

    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    g_MessageMgr:ShowMessage("YuanXiao2023_NaoYuanXiao_Rule")
	end)

    UIEventListener.Get(self.GoBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSignUpBtnClick()
	end)

    local naoyuanxiaoAward = setting.NaoyuanxiaoAward
    local rewardInfo = {}
    for reward in string.gmatch(naoyuanxiaoAward, "([^;]+)") do
        table.insert(rewardInfo, {})
        for str in string.gmatch(reward, "([^,]+)") do
            table.insert(rewardInfo[#rewardInfo], str)
        end
    end
    rewardInfo[1] = {
        rewardInfo[1][1]..LocalString.GetString("名"),
        tonumber(rewardInfo[1][2])
    }
    rewardInfo[2] = {
        rewardInfo[2][1]..LocalString.GetString("名"),
        tonumber(rewardInfo[2][2])
    }
    rewardInfo[3] = { 
        rewardInfo[3][1],
        tonumber(rewardInfo[3][2])
    }
    for i = 1, 3 do
        self:InitOneItem(self["Item"..i], rewardInfo[i])
    end

    Gac2Gas.QueryNaoYuanXiaoRemainRewardTimes()
    Gac2Gas.QueryRank(setting.RankId)
    if CClientMainPlayer.Inst ~= nil then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(setting.GameplayId, CClientMainPlayer.Inst.Id)
    end
end 

function LuaYuanXiaoDefenseSignUpWnd:InitOneItem(curItem, info)
    curItem.transform:Find("Sprite/Label"):GetComponent(typeof(UILabel)).text = info[1]

    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(info[2])

    if ItemData then 
        texture:LoadMaterial(ItemData.Icon) 
    end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(info[2], false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaYuanXiaoDefenseSignUpWnd:OnEnable()
    g_ScriptEvent:AddListener("ReturnNaoYuanXiaoRemainRewardTimes", self, "ReturnNaoYuanXiaoRemainRewardTimes")
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaYuanXiaoDefenseSignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReturnNaoYuanXiaoRemainRewardTimes", self, "ReturnNaoYuanXiaoRemainRewardTimes")
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaYuanXiaoDefenseSignUpWnd:ReturnNaoYuanXiaoRemainRewardTimes(remainTimes)
    self.RewardHint.text = LocalString.GetString("排名奖 ")..(2 - remainTimes[1] - remainTimes[2]).."/1   "..LocalString.GetString("挑战奖 ")..(1 - remainTimes[3]).."/1"
    self.Item1.transform:Find("Mask").gameObject:SetActive(remainTimes[1] == 0 or remainTimes[2] == 0)
    self.Item2.transform:Find("Mask").gameObject:SetActive(remainTimes[1] == 0 or remainTimes[2] == 0)
    self.Item3.transform:Find("Mask").gameObject:SetActive(remainTimes[3] == 0)
    if remainTimes[1] == 1 then
        self.Item1.transform:Find("Mask/CheckBoxSprite").gameObject:SetActive(false)
    end
    if remainTimes[2] == 1 then
        self.Item2.transform:Find("Mask/CheckBoxSprite").gameObject:SetActive(false)
    end
end

function LuaYuanXiaoDefenseSignUpWnd:OnRankDataReady()
    local rank = CRankData.Inst.MainPlayerRankInfo.Rank
    self.RankLabel.text = LocalString.GetString("当前排名 ")..(rank > 0 and rank or LocalString.GetString("未上榜"))
end

function LuaYuanXiaoDefenseSignUpWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId == YuanXiao2023_NaoYuanXiao.GetData().GameplayId and playerId == CClientMainPlayer.Inst.Id then
	    self:OnState(isInMatching)
    end
end

function LuaYuanXiaoDefenseSignUpWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
	if playId == YuanXiao2023_NaoYuanXiao.GetData().GameplayId and success then
		self:OnState(true)
	end
end

function LuaYuanXiaoDefenseSignUpWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
	if playId == YuanXiao2023_NaoYuanXiao.GetData().GameplayId and success then
		self:OnState(false)
	end
end

function LuaYuanXiaoDefenseSignUpWnd:OnState(isMatching)
	self.isMatching = isMatching
	--self.MatchLabel.gameObject:SetActive(isMatching)

	local btnSp = self.GoBtn.transform:GetComponent(typeof(UISprite))
    local btnLabel = self.GoBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
	if not isMatching then
		btnLabel.text = LocalString.GetString("报名参加")
		btnSp.spriteName = "common_btn_01_yellow"
	else
		btnLabel.text = LocalString.GetString("取消报名")
		btnSp.spriteName = "common_btn_01_blue"
	end
end

function LuaYuanXiaoDefenseSignUpWnd:OnSignUpBtnClick()
    if self.isMatching then
		Gac2Gas.GlobalMatch_RequestCancelSignUp(YuanXiao2023_NaoYuanXiao.GetData().GameplayId)
	else
		Gac2Gas.GlobalMatch_RequestSignUp(YuanXiao2023_NaoYuanXiao.GetData().GameplayId)
	end
end
