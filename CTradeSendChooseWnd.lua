-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CTradeSendChooseWnd = import "L10.UI.CTradeSendChooseWnd"
local CTradeSendMgr = import "L10.Game.CTradeSendMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EnumTradeSendType = import "L10.Game.EnumTradeSendType"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local PlayerInfo = import "L10.UI.CTradeSendChooseWnd+PlayerInfo"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
CTradeSendChooseWnd.m_GenerateNode_CS2LuaHook = function (this, data)
    local node = NGUITools.AddChild(this.table.gameObject, this.template)
    node:SetActive(true)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel)).text = data.name
    CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (data.clazz)))
    CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = "lv." .. data.level
    local iconbutton = node.transform:Find("icon/button").gameObject
    local button = node.transform:Find("button").gameObject

    UIEventListener.Get(iconbutton).onClick = DelegateFactory.VoidDelegate(function (p)
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)

    UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function (p)
        if CTradeSendMgr.Inst.type == EnumTradeSendType.LingYuTradeSend then
          LuaTradeSendMgr.OnTradeSend(data.playerId, data.name)
        else
          CTradeSendMgr.Inst:OnTradeSend(data.playerId, data.name)
        end
    end)

    CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(data.clazz, data.gender, -1), false)
end
CTradeSendChooseWnd.m_Init_CS2LuaHook = function (this)
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:Close()
    end)

    UIEventListener.Get(this.infoBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("message_shangchengzengsong_readme")
    end)

    this.restNum.text = tostring(CTradeSendMgr.Inst.monthRest)
    if LuaTradeSendMgr.SendItemDiscount then
      this.transform:Find('Anchor/QuestionNode/Info/discountText'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('本次赠送额度享%s折'),LuaTradeSendMgr.SendItemDiscount)
    else
      this.transform:Find('Anchor/QuestionNode/Info/discountText'):GetComponent(typeof(UILabel)).text = ''
    end

    this.template:SetActive(false)

    this:LoadData()

    Gac2Gas.QueryMonthPresentLimit()
    if CTradeSendMgr.Inst.type == EnumTradeSendType.LingYuTradeSend then
        this.titleLabel.text = LocalString.GetString("赠送")
        Gac2Gas.QueryOnlineReverseFriends(EnumQueryOnlineReverseFriendsContext_lua.LingYuTradeSend)
    else
        this.titleLabel.text = LocalString.GetString("赠送月度礼包")
        Gac2Gas.QueryOnlineReverseFriends(EnumQueryOnlineReverseFriendsContext_lua.MonthCardGiven)
    end
end
CTradeSendChooseWnd.m_LoadData_CS2LuaHook = function (this)
    local playerInfoList = CreateFromClass(MakeGenericClass(List, PlayerInfo))
    CommonDefs.ListSort1(CTradeSendMgr.Inst.sendPlayerList, typeof(UInt64), DelegateFactory.Comparison_ulong(function (x, y)
        local xFriendliness = CIMMgr.Inst:GetFriendliness(x)
        local yFriendliness = CIMMgr.Inst:GetFriendliness(y)
        if xFriendliness ~= yFriendliness then
            return NumberCompareTo(yFriendliness, xFriendliness)
        else
            return NumberCompareTo(x, y)
        end
        --其次根据playerId从小到大排序
    end))

    CommonDefs.ListIterate(CTradeSendMgr.Inst.sendPlayerList, DelegateFactory.Action_object(function (___value)
        local playerid = ___value
        if CIMMgr.Inst:IsMyFriend(playerid) then
            local _info = CIMMgr.Inst:GetBasicInfo(playerid)
            local info = CreateFromClass(PlayerInfo)
            info.clazz = (_info.Class)
            info.gender = (_info.Gender)
            info.level = _info.Level
            info.name = _info.Name
            info.playerId = _info.ID
            CommonDefs.ListAdd(playerInfoList, typeof(PlayerInfo), info)
        end
    end))

    Extensions.RemoveAllChildren(this.table.transform)

    if playerInfoList.Count > 0 then
        do
            local i = 0
            while i < playerInfoList.Count do
                local info = playerInfoList[i]
                this:GenerateNode(info)
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
