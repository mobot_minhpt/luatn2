local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIToggle = import "UIToggle"
local UIInput = import "UIInput"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EventDelegate = import "EventDelegate"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local Object = import "System.Object"
local MessageMgr = import "L10.Game.MessageMgr"
local Message_Message = import "L10.Game.Message_Message"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local CIMMgr = import "L10.Game.CIMMgr"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"

LuaQingJiuLaBaWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQingJiuLaBaWnd, "DrinkerNameLabel", "DrinkerNameLabel", UILabel)
RegistChildComponent(LuaQingJiuLaBaWnd, "ChooseDrinkerBtn", "ChooseDrinkerBtn", GameObject)
RegistChildComponent(LuaQingJiuLaBaWnd, "SendBtn", "SendBtn", GameObject)
RegistChildComponent(LuaQingJiuLaBaWnd, "Toggle1", "Toggle1", UIToggle)
RegistChildComponent(LuaQingJiuLaBaWnd, "Toggle2", "Toggle2", UIToggle)
RegistChildComponent(LuaQingJiuLaBaWnd, "Toggle3", "Toggle3", UIToggle)
RegistChildComponent(LuaQingJiuLaBaWnd, "FreeToggle", "FreeToggle", UIToggle)
RegistChildComponent(LuaQingJiuLaBaWnd, "TiggleLabel1", "TiggleLabel1", UILabel)
RegistChildComponent(LuaQingJiuLaBaWnd, "TiggleLabel2", "TiggleLabel2", UILabel)
RegistChildComponent(LuaQingJiuLaBaWnd, "TiggleLabel3", "TiggleLabel3", UILabel)
RegistChildComponent(LuaQingJiuLaBaWnd, "Input", "Input", UIInput)

--@endregion RegistChildComponent end
RegistClassMember(LuaQingJiuLaBaWnd,"m_SelectDrinkerId")
RegistClassMember(LuaQingJiuLaBaWnd,"m_SelectDrinkerName")
RegistClassMember(LuaQingJiuLaBaWnd,"m_SelectToggleIndex")--1,2,3,4
RegistClassMember(LuaQingJiuLaBaWnd,"m_TitleLabel")
function LuaQingJiuLaBaWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ChooseDrinkerBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseDrinkerBtnClick()
	end)

	UIEventListener.Get(self.SendBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendBtnClick()
	end)

    --@endregion EventBind end
    self.m_SelectToggleIndex = 1
    self.TiggleLabel1.text =g_MessageMgr:FormatMessage("MXL_QingJiuLaBa_Msg1")
    self.TiggleLabel2.text = g_MessageMgr:FormatMessage("MXL_QingJiuLaBa_Msg2")
    self.TiggleLabel3.text = g_MessageMgr:FormatMessage("MXL_QingJiuLaBa_Msg3")

    EventDelegate.Add(self.Toggle1.onChange,DelegateFactory.Callback(function () 
        self:OnToggleChange(1,self.Toggle1.value)
    end))
    EventDelegate.Add(self.Toggle2.onChange,DelegateFactory.Callback(function () 
        self:OnToggleChange(2,self.Toggle2.value)
    end))
    EventDelegate.Add(self.Toggle3.onChange,DelegateFactory.Callback(function () 
        self:OnToggleChange(3,self.Toggle3.value)
    end))
    EventDelegate.Add(self.FreeToggle.onChange,DelegateFactory.Callback(function () 
        self:OnToggleChange(4,self.FreeToggle.value)
    end))
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))
    
end

function LuaQingJiuLaBaWnd:OnEnable()
    g_ScriptEvent:AddListener("SelectFriendToMeiXiangLouQingJiu",self,"OnSelectFriendToMeiXiangLouQingJiu")
end

function LuaQingJiuLaBaWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SelectFriendToMeiXiangLouQingJiu",self,"OnSelectFriendToMeiXiangLouQingJiu")
end

function LuaQingJiuLaBaWnd:Init()
    local freeInviteMsg = CPlayerDataMgr.Inst:LoadPlayerData("InviteWineValue")
    if freeInviteMsg then
        self.m_SelectToggleIndex = 4
        self.Toggle1.value = false
        self.FreeToggle.value = true
        self.Input.value = freeInviteMsg
    end
    if self.m_SelectDrinkerName then
        self.DrinkerNameLabel.text = self.m_SelectDrinkerName
    else
        self.DrinkerNameLabel.text = LocalString.GetString("(可不添加)")
    end
    local id = LuaMeiXiangLouMgr.m_LaBaId
    if id and Item_Item.GetData(id) then
        local data = Item_Item.GetData(id)
        self.m_TitleLabel.text = data.Name
    end
end

function LuaQingJiuLaBaWnd:OnSelectFriendToMeiXiangLouQingJiu(playerId,playerName)
    self.m_SelectDrinkerId = playerId
    self.m_SelectDrinkerName = playerName
    self.DrinkerNameLabel.text = self.m_SelectDrinkerName
end

function LuaQingJiuLaBaWnd:OnToggleChange(index,value)
    if value then
        self.m_SelectToggleIndex = index
    end
end

--@region UIEvent

function LuaQingJiuLaBaWnd:OnChooseDrinkerBtnClick()
    --[[LuaMeiXiangLouMgr.SelectFriendOpenForMXL = true
    CUIManager.ShowUI("ZhongQiuSelectFriendWnd")--]]
    local player = CClientMainPlayer.Inst
    if not player then
        return
    end

    CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("选择对象")
    CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("选择")
    CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
    CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)

    -- 加在线好友
    local friendlinessTbl = {}
    CommonDefs.DictIterate(player.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
        if CIMMgr.Inst:IsOnline(k) and CIMMgr.Inst:IsSameServerFriend(k) then
            table.insert(friendlinessTbl, {k, v.Friendliness})
        end
    end))
    table.sort(friendlinessTbl, function(v1, v2) return v1[2] > v2[2] end)

    for _, info in pairs(friendlinessTbl) do
        local basicInfo = CIMMgr.Inst:GetBasicInfo(info[1])
        if basicInfo then
            local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
            CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
        end
    end

    CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(function(playerId)
        local playerName = ""
        for i = 0,CCommonPlayerListMgr.Inst.allData.Count-1,1 do
            if (CCommonPlayerListMgr.Inst.allData[i].playerId == playerId) then
                playerName = CCommonPlayerListMgr.Inst.allData[i].playerName
                break
            end
        end
        g_ScriptEvent:BroadcastInLua("SelectFriendToMeiXiangLouQingJiu",playerId,playerName)
        CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
    end)
    CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
    
end

function LuaQingJiuLaBaWnd:OnSendBtnClick()
    local msg = ""
    local ret
    if self.m_SelectToggleIndex == 4 then
        msg = self.Input.value
	    CPlayerDataMgr.Inst:SavePlayerData("InviteWineValue",msg)
        ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(msg, true)
        if not ret or not ret.msg then
            return
        end
    elseif self.m_SelectToggleIndex >= 1 then
        local msgname = "MXL_QingJiuLaBa_Msg"..self.m_SelectToggleIndex
        local messageId = MessageMgr.Inst:GetMessageIdByName(msgname)
        local data = Message_Message.GetData(messageId)
        msg = data.Message
    end
    -- 玩家数据
    local id = LuaMeiXiangLouMgr.m_LaBaId
    local itemId = LuaMeiXiangLouMgr.m_LaBaItemId
    local place = LuaMeiXiangLouMgr.m_LaBaPlace
    local pos = LuaMeiXiangLouMgr.m_LaBaPos

    local playerId = self.m_SelectDrinkerId
    local playerName =  self.m_SelectDrinkerName

    local packInfo = CreateFromClass(MakeGenericClass(List, Object))
    CommonDefs.ListAdd_LuaCall(packInfo,playerId)
    CommonDefs.ListAdd_LuaCall(packInfo,playerName)

    
    CChatMgr.Inst:UseLaba(id, itemId, place, pos, msg, packInfo)
    CUIManager.CloseUI(CLuaUIResources.QingJiuLaBaWnd)
end


--@endregion UIEvent

