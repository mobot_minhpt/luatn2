
local UILabel                       = import "UILabel"
local UIGrid                        = import "UIGrid"
local UISprite                      = import "UISprite"
local EnumQualityType               = import "L10.Game.EnumQualityType"
local MsgPackImpl                   = import "MsgPackImpl"
local Object                        = import "System.Object"
-- local Item_Item                     = import "L10.Game.Item_Item"
local CItemInfoMgr                  = import "L10.UI.CItemInfoMgr"
local AlignType                     = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr                      = import "L10.Game.CItemMgr"
local CItemAccessListMgr            = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType             = import "L10.UI.CTooltip+AlignType"
local Extensions                    = import "Extensions"

LuaGuildVoteSelectWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaGuildVoteSelectWnd,         "ItemTemplate",         GameObject)
RegistChildComponent(LuaGuildVoteSelectWnd,         "Grid",                 UIGrid)
RegistChildComponent(LuaGuildVoteSelectWnd,         "VoteBtn",              GameObject)
RegistChildComponent(LuaGuildVoteSelectWnd,         "Item",                 GameObject)
RegistChildComponent(LuaGuildVoteSelectWnd,         "itemIcon",             CUITexture)
RegistChildComponent(LuaGuildVoteSelectWnd,         "Quality",              UISprite)
RegistChildComponent(LuaGuildVoteSelectWnd,         "Acquire",              GameObject)
RegistChildComponent(LuaGuildVoteSelectWnd,         "Count",                UILabel)
RegistChildComponent(LuaGuildVoteSelectWnd,         "DesLable",             UILabel)

---------RegistClassMember-------   
RegistClassMember(LuaGuildVoteSelectWnd,            "m_VotePlayerTable")
RegistClassMember(LuaGuildVoteSelectWnd,            "m_VotePlayerArray")
RegistClassMember(LuaGuildVoteSelectWnd,            "m_SelectNum")
RegistClassMember(LuaGuildVoteSelectWnd,            "m_SelectMinNum")
RegistClassMember(LuaGuildVoteSelectWnd,            "m_SelectMaxNum")
RegistClassMember(LuaGuildVoteSelectWnd,            "m_CostItemId")

function LuaGuildVoteSelectWnd:Init()
    self.m_VotePlayerTable = {}
    self.m_VotePlayerArray = {}
    self.m_SelectMinNum = 3
    self.m_SelectMaxNum = 10
    self.m_SelectNum = 0
    self.m_CostItemId = 21020595
    self.ItemTemplate:SetActive(false)
    Gac2Gas.RequestGuildPlayerIdAndName()
    self.DesLable.text = luaGuildVoteMgr.SendContent
    UIEventListener.Get(self.VoteBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        if self.m_SelectNum < self.m_SelectMinNum then
            g_MessageMgr:ShowMessage("Guild_Vote_Is_Min")
            return
        end
        
        for k,v in pairs(self.m_VotePlayerTable) do
            table.insert( self.m_VotePlayerArray,v)
        end
        local cslist = Table2List(self.m_VotePlayerArray, MakeGenericClass(List, Object))
        local data = MsgPackImpl.pack(cslist)
        Gac2Gas.RequestYuanDanGuildVoteCreate(luaGuildVoteMgr.SendContent,luaGuildVoteMgr.SendTitle,data)
        CUIManager.CloseUI(CLuaUIResources.GuildVoteSelectWnd)
        CUIManager.CloseUI(CLuaUIResources.GuildVoteEditWnd)
    end)
    self:InitItemBtn()
end

function LuaGuildVoteSelectWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "RefreshCostType")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshCostType")
    g_ScriptEvent:AddListener("SendGuildMemberIdAndName", self, "SendGuildMemberIdAndName")
end

function LuaGuildVoteSelectWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshCostType")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshCostType")
    g_ScriptEvent:RemoveListener("SendGuildMemberIdAndName", self, "SendGuildMemberIdAndName")
    luaGuildVoteMgr:ClearEditData()
end


function LuaGuildVoteSelectWnd:InitItemBtn()
    local ItemData = Item_Item.GetData(self.m_CostItemId)
    self.itemIcon:LoadMaterial(ItemData.Icon)
    self.Quality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    
    UIEventListener.Get(self.Item).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_CostItemId, false, nil, AlignType.Right, 0, 0, 0, 0)
    end)
    UIEventListener.Get(self.Acquire).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_CostItemId, true,  self.Acquire.transform, CTooltipAlignType.Right)
    end)
    self:RefreshCostType()
end

function LuaGuildVoteSelectWnd:RefreshCostType()
    
    if CItemMgr.Inst:GetItemCount(self.m_CostItemId) == 0 then
        self.Acquire:SetActive(true)
        self.Count.text = SafeStringFormat3("[c][FF0000]%s[-][/c]/1",CItemMgr.Inst:GetItemCount(self.m_CostItemId))
    else
        self.Acquire:SetActive(false)
        self.Count.text = SafeStringFormat3("[c][FFFFFF]%s[-][/c]/1",CItemMgr.Inst:GetItemCount(self.m_CostItemId))
    end
end


function LuaGuildVoteSelectWnd:SendGuildMemberIdAndName()
    self:InitScrollView()
end

function LuaGuildVoteSelectWnd:InitScrollView()
    -- Instantiate
    Extensions.RemoveAllChildren(self.Grid.transform)
    for i=1,#luaGuildVoteMgr.GuildMemberTable do
        local item = CUICommonDef.AddChild(self.Grid.gameObject,self.ItemTemplate)
        self:InitItem(item,i)
        item:SetActive(true)
    end
    self.Grid:Reposition()
end

function LuaGuildVoteSelectWnd:InitItem(item,index)
    item.name = index
    local PlayNameLable = item.transform:Find("PlayNameLable"):GetComponent(typeof(UILabel))
    local Checked = item.transform:Find("Checked").gameObject
    Checked:SetActive(false)
    local background = item.transform:Find("background"):GetComponent(typeof(UISprite))
    PlayNameLable.text = luaGuildVoteMgr.GuildMemberTable[index].memberName
    local isClick = true
    local memberId = luaGuildVoteMgr.GuildMemberTable[index].memberId
    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(p)
        if isClick then
            if self.m_SelectNum >= self.m_SelectMaxNum then
                g_MessageMgr:ShowMessage("Guild_Vote_Is_Max")
                return
            end
            background.color = NGUIText.ParseColor24("E1CB78", 0)
            self.m_VotePlayerTable[memberId] = memberId
            self.m_SelectNum = self.m_SelectNum + 1
            Checked:SetActive(true)
        else
            background.color = NGUIText.ParseColor24("CBB58C", 0)
            Checked:SetActive(false)
            table.remove( self.m_VotePlayerTable, memberId )
            self.m_SelectNum = self.m_SelectNum - 1
        end
        isClick = not isClick
    end)
end
