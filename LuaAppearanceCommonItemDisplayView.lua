local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"

LuaAppearanceCommonItemDisplayView = class()
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_NoneLabel", "None", UILabel)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_ContentRoot", "Content", GameObject)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_IconTexture", "Item", CUITexture)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_DisabledGo", "Disabled", GameObject)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_LockedGo", "Locked", GameObject)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_NameLabel2", "NameLabel2", UILabel)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_DescLabel21", "DescLabel21", UILabel)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_DescLabel22", "DescLabel22", UILabel)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_RegularButtonRoot", "RegularButtonRoot", Transform)
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_BigButton", "BigButton", CButton) --显示一个按钮时，使用该按钮
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_LeftButton", "LeftButton", CButton) --显示两个按钮时，左边使用该按钮
RegistChildComponent(LuaAppearanceCommonItemDisplayView,"m_RightButton", "RightButton", CButton) --显示两个按钮时，右边使用该按钮

function LuaAppearanceCommonItemDisplayView:Awake()
end

function LuaAppearanceCommonItemDisplayView:Clear()
    self.m_NoneLabel.gameObject:SetActive(true)
    self.m_ContentRoot:SetActive(false)
end

-- buttonTbl每个元素的格式 {text=xxx, isYellow=xxx, action=xxx}
function LuaAppearanceCommonItemDisplayView:Init(icon, name, desc1, desc2, bShow2Desc, disabled, locked, buttonTbl)
    self.m_NoneLabel.gameObject:SetActive(false)
    self.m_ContentRoot:SetActive(true)
    self.m_IconTexture:LoadMaterial(icon)
    --根据最新策划需求，不显示disable和lock状态
    self.m_DisabledGo:SetActive(false)
    self.m_LockedGo:SetActive(false)
    if bShow2Desc then --显示三行文本
        self.m_NameLabel.text = ""
        self.m_DescLabel.text = "" 
        self.m_NameLabel2.text = name
        self.m_DescLabel21.text = desc1
        self.m_DescLabel22.text = desc2                                     
    else --显示两行文本
        self.m_NameLabel.text = name
        self.m_DescLabel.text = desc1 
        self.m_NameLabel2.text = ""
        self.m_DescLabel21.text = ""
        self.m_DescLabel22.text = ""  
    end

    self.m_BigButton.gameObject:SetActive(false)
    self.m_LeftButton.gameObject:SetActive(false)
    self.m_RightButton.gameObject:SetActive(false)
    local tbl = {}
    --最少显示0个按钮，最多显示2个按钮
    if buttonTbl and #buttonTbl>0 and #buttonTbl<3 then
        if #buttonTbl==1 then
            table.insert(tbl, self.m_BigButton)
        else
            table.insert(tbl, self.m_LeftButton)
            table.insert(tbl, self.m_RightButton)
        end
    end

    for i=1,#buttonTbl do
        local btn = tbl[i]
        local info = buttonTbl[i]
        btn.gameObject:SetActive(true)
        btn.Text = info.text
        btn:SetBackgroundSprite(info.isYellow and g_sprites.AppearanceCommonOrangeButtonBg or g_sprites.AppearanceCommonBlueButtonBg)
        btn:SetFontTextColor(info.isYellow and NGUIText.ParseColor24("754923", 0) or NGUIText.ParseColor24("1C4165", 0))
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if info.action then
                info.action(go)
            end
        end)
    end
end



