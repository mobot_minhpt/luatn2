local GameObject			= import "UnityEngine.GameObject"
local UITable				= import "UITable"
local UIScrollView			= import "UIScrollView"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CTipTitleItem			= import "CTipTitleItem"

local DelegateFactory		= import "DelegateFactory"
local Extensions			= import "Extensions"

local CUIManager			= import "L10.UI.CUIManager"
local CUICommonDef			= import "L10.UI.CUICommonDef"
local CMessageTipMgr		= import "L10.UI.CMessageTipMgr"
local CTipParagraphItem		= import "L10.UI.CTipParagraphItem"

local CommonDefs			= import "L10.Game.CommonDefs"
local CScene                = import "L10.Game.CScene"

--中秋月神殿入口界面
CLuaZQYueShenDianDescWnd = class()

RegistChildComponent(CLuaZQYueShenDianDescWnd, "Table",				UITable)
RegistChildComponent(CLuaZQYueShenDianDescWnd, "TitleTemplate",		GameObject)
RegistChildComponent(CLuaZQYueShenDianDescWnd, "ParagraphTemplate",	GameObject)
RegistChildComponent(CLuaZQYueShenDianDescWnd, "ScrollView",		UIScrollView)
RegistChildComponent(CLuaZQYueShenDianDescWnd, "Indicator",			UIScrollViewIndicator)
RegistChildComponent(CLuaZQYueShenDianDescWnd, "EnterBtn",			GameObject)

RegistClassMember(CLuaZQYueShenDianDescWnd, "delayTick")

CLuaZQYueShenDianDescWnd.onlyrule = false

function CLuaZQYueShenDianDescWnd:Init()
	self:ParseRuleText()
end

function CLuaZQYueShenDianDescWnd:OnEnable()
    if CLuaZQYueShenDianDescWnd.onlyrule then
        self.EnterBtn:SetActive(false)

        local autoclose = function()
             CUIManager.CloseUI(CLuaUIResources.ZQYueShenDianDescWnd)
        end
        self:DoTickOnce(autoclose,3)
        return
    end
    local submitClick = function ( go )
		self:OnSignUpBtnClick()
    end
    CommonDefs.AddOnClickListener(self.EnterBtn, DelegateFactory.Action_GameObject(submitClick), false)
end

function CLuaZQYueShenDianDescWnd:OnDisable()
    CLuaZQYueShenDianDescWnd.onlyrule = false
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
end

function CLuaZQYueShenDianDescWnd:OnSignUpBtnClick()
    Gac2Gas.RequestStartYueShenTemple()
    CUIManager.CloseUI(CLuaUIResources.ZQYueShenDianDescWnd)
end


function CLuaZQYueShenDianDescWnd:ParseRuleText()
	Extensions.RemoveAllChildren(self.Table.transform)
    local msg =	g_MessageMgr:FormatMessage("ZQYueShenDian_Rule");
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
        return
    end

    if info.titleVisible then
        local titleGo = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.ParagraphTemplate)
            paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
            tip:Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ScrollView:ResetPosition()
    self.Indicator:Layout()
    self.Table:Reposition()
end

function CLuaZQYueShenDianDescWnd:DoTickOnce(callback,second)
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
	self.delayTick = RegisterTickOnce(callback,second * 1000)
end

function CLuaZQYueShenDianDescWnd:OnRenderSceneInit()

    local gameplay = 51101137
    local gpdata = Gameplay_Gameplay.GetData(gameplay)
    if gpdata == nil then return end

    local cmap = CScene.MainScene
    if cmap == nil then return end
    if cmap.SceneTemplateId == gpdata.MapId then
        CLuaZQYueShenDianDescWnd.onlyrule = true
        CUIManager.ShowUI(CLuaUIResources.ZQYueShenDianDescWnd)
    end
end

g_ScriptEvent:AddListener("RenderSceneInit", CLuaZQYueShenDianDescWnd, "OnRenderSceneInit")
