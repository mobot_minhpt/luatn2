local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CUITexture = import "L10.UI.CUITexture"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Animation = import "UnityEngine.Animation"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local UISprite = import "UISprite"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local BoxCollider = import "UnityEngine.BoxCollider"
local EasyTouch = import "EasyTouch"

LuaZYPDSignUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZYPDSignUpWnd, "NormalBtn", "NormalBtn", QnButton)
RegistChildComponent(LuaZYPDSignUpWnd, "HardBtn", "HardBtn", QnButton)
RegistChildComponent(LuaZYPDSignUpWnd, "SignUpBtn", "SignUpBtn", QnButton)
RegistChildComponent(LuaZYPDSignUpWnd, "Time", "Time", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "TaskForm", "TaskForm", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "Level", "Level", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "TaskDetails", "TaskDetails", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "Reward", "Reward", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "Matching", "Matching", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "HardTips", "HardTips", GameObject)
RegistChildComponent(LuaZYPDSignUpWnd, "SignUpLab", "SignUpLab", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "RewardNumber", "RewardNumber", UILabel)
RegistChildComponent(LuaZYPDSignUpWnd, "NormalSelect", "NormalSelect", GameObject)
RegistChildComponent(LuaZYPDSignUpWnd, "NormalUnselect", "NormalUnselect", GameObject)
RegistChildComponent(LuaZYPDSignUpWnd, "HardSelect", "HardSelect", GameObject)
RegistChildComponent(LuaZYPDSignUpWnd, "HardUnselect", "HardUnselect", GameObject)
RegistChildComponent(LuaZYPDSignUpWnd, "RewardBtn", "RewardBtn", QnButton)
RegistChildComponent(LuaZYPDSignUpWnd, "RuleBtn", "RuleBtn", QnButton)
RegistChildComponent(LuaZYPDSignUpWnd, "HardBg", "HardBg", GameObject)
RegistChildComponent(LuaZYPDSignUpWnd, "NormalBg", "NormalBg", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaZYPDSignUpWnd, "signUpSprite")
RegistClassMember(LuaZYPDSignUpWnd, "rulewnd")
RegistClassMember(LuaZYPDSignUpWnd, "m_Animation")
RegistClassMember(LuaZYPDSignUpWnd,"m_DragDirection")

function LuaZYPDSignUpWnd:Awake()
	--@region EventBind: Dont Modify Manually!


	UIEventListener.Get(self.NormalBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnNormalBtnClick()
	end)



	UIEventListener.Get(self.HardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnHardBtnClick()
	end)


	
	UIEventListener.Get(self.SignUpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnSignUpBtnClick()
	end)
	
	
	
	UIEventListener.Get(self.RewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnRewardBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnRuleBtnClick()
	end)

	--@endregion EventBind end

	self.signUpSprite = self.SignUpBtn.gameObject:GetComponent(typeof(UISprite))

	self.rulewnd = self.transform:Find("CommonImageRuleWnd"):GetComponent(typeof(CCommonLuaScript))
	self.m_Animation = self.transform:GetComponent(typeof(Animation))

	self.m_DragDirection = 1
	UIEventListener.Get(self.rulewnd.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
		self:OnScreenDrag(go, delta)
	end)
	UIEventListener.Get(self.rulewnd.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
		self:OnDragFish()
	end)
end

function LuaZYPDSignUpWnd:Init()
	self.m_Animation:Play("zypdsignupwnd_show")
	self.Matching.gameObject:SetActive(false)
	self:InitSelectMode()
	local selectMode = LuaZhongYuanJie2023Mgr.ZYPDSelectMode
	self:ShowSelectMode(selectMode)
	if CClientMainPlayer.Inst ~= nil then
		if LuaZhongYuanJie2023Mgr:IsSignUp(selectMode) == nil then
			Gac2Gas.GlobalMatch_RequestCheckSignUp(LuaZhongYuanJie2023Mgr:GetGamePlayId(selectMode), CClientMainPlayer.Inst.Id)
		else
			self:SetSignUpBtn(LuaZhongYuanJie2023Mgr:IsSignUp(selectMode), selectMode)
		end
	end
	self:SetReward(LuaZhongYuanJie2023Mgr.ZYPDSelectMode)
	self:ShowGuide()
	self:ShowBaseInfo()
	self:DisableRuleImageCollider()
end

function LuaZYPDSignUpWnd:DisableRuleImageCollider()
	for i=0,self.rulewnd.m_LuaSelf.ImageRoot.transform.childCount-1,1 do
		local go = self.rulewnd.m_LuaSelf.ImageRoot.transform:GetChild(i).gameObject
		go:GetComponent(typeof(BoxCollider)).enabled = false
	end
end

function LuaZYPDSignUpWnd:ShowBaseInfo()
	self.Level.text = SafeStringFormat3(LocalString.GetString("%d级"), ZhongYuanJie_ZhongYuanPuDuSetting.GetData().MinGrade)
	self.Time.text = ZhongYuanJie_ZhongYuanPuDuSetting.GetData().ActiveTime_Show
	self.TaskDetails.text = ZhongYuanJie_ZhongYuanPuDuSetting.GetData().ActiveInfo_Show
end

function LuaZYPDSignUpWnd:ShowGuide()
	if self.rulewnd then
		local imagePaths = LuaZhongYuanJie2023Mgr:GetRulePic()
		local msgs = LuaZhongYuanJie2023Mgr:GetGuideData()

		if imagePaths==nil or #imagePaths==0 then return end

		self.rulewnd.m_LuaSelf:Init(imagePaths, msgs)
	end
end

function LuaZYPDSignUpWnd:InitSelectMode()
	--默认为普通难度，如果普通难度领取过奖励并且困难难度还未获得过奖励则优选为困难难度
	local selectMode = 1
	if LuaZhongYuanJie2023Mgr:GetReceivedRewardNum(1) > 0 then
		selectMode = 2
	end
	LuaZhongYuanJie2023Mgr.ZYPDSelectMode = selectMode

end

function LuaZYPDSignUpWnd:ShowSelectMode(selectMode)
	self.NormalSelect:SetActive(selectMode == 1)
	self.NormalUnselect:SetActive(selectMode ~= 1)
	self.NormalBg:SetActive(selectMode == 1)
	self.HardSelect:SetActive(selectMode == 2)
	self.HardUnselect:SetActive(selectMode ~= 2)
	self.HardTips:SetActive(selectMode == 2)
	self.HardBg:SetActive(selectMode == 2)
	self.TaskForm.text = selectMode == 1 and ZhongYuanJie_ZhongYuanPuDuSetting.GetData().ActiveTask_Show_Normal or selectMode == 2 and ZhongYuanJie_ZhongYuanPuDuSetting.GetData().ActiveTask_Show_Hard
end

function LuaZYPDSignUpWnd:OnEnable()
	g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
	g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnSignUpPlayResult")
	g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnCancelSignUpResult")
end

function LuaZYPDSignUpWnd:OnDisable()
	LuaZhongYuanJie2023Mgr:SetIsSignUp(1, nil)
	LuaZhongYuanJie2023Mgr:SetIsSignUp(2, nil)
	g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
	g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnSignUpPlayResult")
	g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnCancelSignUpResult")
end

function LuaZYPDSignUpWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
	if playId == LuaZhongYuanJie2023Mgr:GetGamePlayId(1) then
		LuaZhongYuanJie2023Mgr:SetIsSignUp(1, isInMatching)
	end
	if playId == LuaZhongYuanJie2023Mgr:GetGamePlayId(2) then
		LuaZhongYuanJie2023Mgr:SetIsSignUp(2, isInMatching)
	end
	self:SetSignUpBtn(isInMatching, LuaZhongYuanJie2023Mgr.ZYPDSelectMode)
end

function LuaZYPDSignUpWnd:OnSignUpPlayResult(playId, success)
	if playId == LuaZhongYuanJie2023Mgr:GetGamePlayId(1) and success then
		LuaZhongYuanJie2023Mgr:SetIsSignUp(1, true)
	elseif playId == LuaZhongYuanJie2023Mgr:GetGamePlayId(2) and success then
		LuaZhongYuanJie2023Mgr:SetIsSignUp(2, true)
	end
	self:SetSignUpBtn(true, LuaZhongYuanJie2023Mgr.ZYPDSelectMode)
end

function LuaZYPDSignUpWnd:OnCancelSignUpResult(playId, success)
	if playId == LuaZhongYuanJie2023Mgr:GetGamePlayId(1) and success then
		LuaZhongYuanJie2023Mgr:SetIsSignUp(1,false)
	elseif playId == LuaZhongYuanJie2023Mgr:GetGamePlayId(2) and success then
		LuaZhongYuanJie2023Mgr:SetIsSignUp(2,false)
	end
	self:SetSignUpBtn(false, LuaZhongYuanJie2023Mgr.ZYPDSelectMode)
end

function LuaZYPDSignUpWnd:SetReward(mode)
	if mode == 1 then
		self.Reward.text = LocalString.GetString("今日普通模式奖励")
	else
		self.Reward.text = LocalString.GetString("今日困难模式奖励")
	end
	local totalReward = LuaZhongYuanJie2023Mgr:GetTotalRewardNum(mode)
	local receivedReward = LuaZhongYuanJie2023Mgr:GetReceivedRewardNum(mode)
	if totalReward == receivedReward then
		self.RewardNumber.text = SafeStringFormat3(LocalString.GetString("[A45061]%d[-][FFFE91]/%d[-]"), receivedReward, totalReward)
	else
		self.RewardNumber.text = SafeStringFormat3(LocalString.GetString("[FFFE91]%d/%d[-]"), receivedReward, totalReward)
	end
	
	local itemTex = self.RewardBtn:GetComponent(typeof(CUITexture))
	local itemTemplateId = LuaZhongYuanJie2023Mgr:GetRewardID(LuaZhongYuanJie2023Mgr.ZYPDSelectMode)

    local item = CItemMgr.Inst:GetItemTemplate(itemTemplateId)
    itemTex:LoadMaterial(item.Icon)
end

function LuaZYPDSignUpWnd:SetSignUpBtn(isSignUp, mode)
	if isSignUp then
		self.signUpSprite.spriteName = "common_btn_01_blue"
		self.SignUpBtn.Text = LocalString.GetString("取消报名")
		self.Matching.gameObject:SetActive(true)
		if mode == 1 then
			self.Matching.text = LocalString.GetString("[fffe91]普通模式匹配中[-]...")
		else
			self.Matching.text = LocalString.GetString("[ff88ff]困难模式匹配中[-]...")
		end
	else
		self.signUpSprite.spriteName = "common_btn_01_yellow"
		if mode == 1 then
			self.SignUpBtn.Text = LocalString.GetString("报名普通模式")
		else
			self.SignUpBtn.Text = LocalString.GetString("报名困难模式")
		end
		self.Matching.gameObject:SetActive(false)
	end
end

--@region UIEvent

function LuaZYPDSignUpWnd:OnNormalBtnClick()
	if LuaZhongYuanJie2023Mgr.ZYPDSelectMode == 1 then
		return
	end
	LuaZhongYuanJie2023Mgr.ZYPDSelectMode = 1
	self.m_Animation:CrossFade("zypdsignupwnd_switchtonormal",0.2)
	self:ShowSelectMode(1)
	self:SetReward(1)
	self:SetSignUpBtn(LuaZhongYuanJie2023Mgr:IsSignUp(1), 1)
	if LuaZhongYuanJie2023Mgr:IsSignUp(1) == nil then
		Gac2Gas.GlobalMatch_RequestCheckSignUp(LuaZhongYuanJie2023Mgr:GetGamePlayId(LuaZhongYuanJie2023Mgr.ZYPDSelectMode), CClientMainPlayer.Inst.Id)
	end
end

function LuaZYPDSignUpWnd:OnHardBtnClick()
	if LuaZhongYuanJie2023Mgr.ZYPDSelectMode == 2 then
		return
	end
	LuaZhongYuanJie2023Mgr.ZYPDSelectMode = 2
	self.m_Animation:CrossFade("zypdsignupwnd_switchtohard",0.2)
	self:ShowSelectMode(2)
	self:SetReward(2)
	self:SetSignUpBtn(LuaZhongYuanJie2023Mgr:IsSignUp(2), 2)
	if LuaZhongYuanJie2023Mgr:IsSignUp(2) == nil then
		Gac2Gas.GlobalMatch_RequestCheckSignUp(LuaZhongYuanJie2023Mgr:GetGamePlayId(LuaZhongYuanJie2023Mgr.ZYPDSelectMode), CClientMainPlayer.Inst.Id)
	end
end

function LuaZYPDSignUpWnd:OnSignUpBtnClick()
	local selectMode = LuaZhongYuanJie2023Mgr.ZYPDSelectMode
	if LuaZhongYuanJie2023Mgr:IsSignUp(selectMode) then
		Gac2Gas.GlobalMatch_RequestCancelSignUp(LuaZhongYuanJie2023Mgr:GetGamePlayId(selectMode))
	else
		Gac2Gas.GlobalMatch_RequestSignUp(LuaZhongYuanJie2023Mgr:GetGamePlayId(selectMode))
	end
end

function LuaZYPDSignUpWnd:OnRewardBtnClick()
	local itemTemplateId = LuaZhongYuanJie2023Mgr:GetRewardID(LuaZhongYuanJie2023Mgr.ZYPDSelectMode)
	CItemInfoMgr.ShowLinkItemTemplateInfo(itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
end

function LuaZYPDSignUpWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("ZhongYuanJie_2023_ZYPD_RULE")
end


--@endregion UIEvent
function LuaZYPDSignUpWnd:OnScreenDrag(go, delta)
    if EasyTouch.GetTouchCount() == 1 then
        self.m_DragDirection = (delta.x < 0) and 1 or -1
    end
end

function LuaZYPDSignUpWnd:OnDragFish()
	self.rulewnd.m_LuaSelf:UpdatePage(self.m_DragDirection)
end

