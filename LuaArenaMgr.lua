local CIMMgr         = import "L10.Game.CIMMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaArenaMgr = {}

LuaArenaMgr.isFunctionOpen = true

LuaArenaMgr.hasPlayedExpression = false
LuaArenaMgr.playerInfo = {}
LuaArenaMgr.fashionId = 0
LuaArenaMgr.buyLevelInfo = {}
LuaArenaMgr.playResultInfo = {}

LuaArenaMgr.smallDanMatPaths = {
    "UI/Texture/Transparent/Material/arenaicon_yi.mat",
    "UI/Texture/Transparent/Material/arenaicon_er.mat",
    "UI/Texture/Transparent/Material/arenaicon_san.mat",
    "UI/Texture/Transparent/Material/arenaicon_si.mat",
    "UI/Texture/Transparent/Material/arenaicon_wu.mat"
}

-- 获取大段位和小段位
function LuaArenaMgr:GetLargeAndSmallDan(rankScore)
    if rankScore <= Arena_DanInfo.GetData(1).Score then
        return 1, 1
    end

    local count = Arena_DanInfo.GetDataCount()
    local largeDan, smallDan
    for i = 1, count do
        local score1 = Arena_DanInfo.GetData(i).Score
        if rankScore >= score1 and (i == count or rankScore < Arena_DanInfo.GetData(i + 1).Score) then
            largeDan = i
            local interval = Arena_DanInfo.GetData(i).SubDanInterval
            smallDan = #LuaArenaMgr.smallDanMatPaths
            for j = 1, #LuaArenaMgr.smallDanMatPaths do
                if rankScore >= score1 + (j - 1) * interval and rankScore < score1 + j * interval then
                    smallDan = j
                    break
                end
            end
        end
    end
    return largeDan, smallDan
end

function LuaArenaMgr:ShowPlayerInfoWnd(playerId)
    self.playerInfo.id = playerId
    Gac2Gas.QueryPlayerArenaDetails(playerId)
end

-- 获取所有好友
LuaArenaMgr.friendInfos = {}
function LuaArenaMgr:UpdateFriendInfos(friendData)
    if not CClientMainPlayer.Inst then return end

    local friendDataDict = {}
    for _, data in ipairs(friendData) do
        friendDataDict[data.id] = data
    end

    local friendInfos = {}
    local friends = CClientMainPlayer.Inst.RelationshipProp.Friends
    CommonDefs.DictIterate(friends, DelegateFactory.Action_object_object(function (key, value)
        local data = friendDataDict[key]
        local isOnline = CIMMgr.Inst:IsOnline(key)
        local state = data and data.state or 0
        local rankScore = data and data.rankScore or 0
        local friendliness = CIMMgr.Inst:GetFriendliness(key)

        table.insert(friendInfos, {id = key, isOnline = isOnline, state = state, rankScore = rankScore, friendliness = friendliness})
	end))

    table.sort(friendInfos, function (data1, data2)
        if data1.isOnline ~= data2.isOnline then
            return data1.isOnline
        end
        if data1.state ~= data2.state then
            return data1.state > data2.state
        end
        if data1.friendliness ~= data2.friendliness then
            return data1.friendliness > data2.friendliness
        end
        return data1.id > data2.id
    end)
    self.friendInfos = friendInfos
    g_ScriptEvent:BroadcastInLua("ArenaUpdateFriendInfos")
end

function LuaArenaMgr:GetPassportItemRewards(level)
    local itemRewards = Arena_PassportLevelReward.GetData(level).ItemRewards
    local tbl = {}
    for itemId, count, order in string.gmatch(itemRewards, "(%d+),(%d+),(%d+);") do
        table.insert(tbl, {
            itemId = tonumber(itemId),
            count = tonumber(count),
            order = tonumber(order),
        })
    end
    table.sort(tbl, function (a, b)
        return a.order > b.order
    end)
    return tbl
end

function LuaArenaMgr:GetPassportItemRewardsInLevelRange(minLevel, maxLevel)
    local dict = {}
    for level = minLevel, maxLevel do
        local itemRewards = Arena_PassportLevelReward.GetData(level).ItemRewards
        for itemId, count, order in string.gmatch(itemRewards, "(%d+),(%d+),(%d+);") do
            if not dict[itemId] then
                dict[itemId] = {
                    count = tonumber(count),
                    order = tonumber(order),
                }
            else
                dict[itemId].count = dict[itemId].count + tonumber(count)
            end
        end
    end
    local tbl = {}
    for itemId, data in pairs(dict) do
        table.insert(tbl, {
            itemId = itemId,
            count = data.count,
            order = data.order,
        })
    end
    table.sort(tbl, function (a, b)
        return a.order > b.order
    end)
    -- 以最大999进行堆叠
    local newTbl = {}
    for i = 1, #tbl do
        local data = tbl[i]
        local count = data.count
        while count > 0 do
            local groupNum = count > 999 and 999 or count
            table.insert(newTbl, {
                itemId = data.itemId,
                count = groupNum,
                order = data.order,
            })
            count = count - groupNum
        end
    end
    return newTbl
end

function LuaArenaMgr:GetStateText(state)
    local count = Arena_PlayerState.GetDataCount()
    for j = 1, count do
        local stateData = Arena_PlayerState.GetData(j)
        local stateBegin = stateData.State
        if state >= stateBegin and (j == count or state < Arena_PlayerState.GetData(j + 1).State) then
            return SafeStringFormat3("[%s]%s[-]", stateData.TextColor, stateData.Text)
        end
    end
end

function LuaArenaMgr:GetWeekStarDict()
    local weekStarDict = {}
    Arena_TaskWeekStarReward.Foreach(function(key, data)
        local week = data.Week
        if not weekStarDict[week] then
            weekStarDict[week] = {}
        end
        weekStarDict[week][data.Star] = data.Text
    end)
    return weekStarDict
end

--#region Gas2Gac

function LuaArenaMgr:SendNewArenaInfo(rankScore, shopScore, winCount, favCount, isPlayOpen, matchType, passportData, taskData, friendData)
    g_ScriptEvent:BroadcastInLua("SendNewArenaInfo", rankScore, shopScore, winCount, favCount, isPlayOpen, matchType, passportData, taskData, friendData)
end

function LuaArenaMgr:SendPlayerArenaDetails(targetId, name, gender, class, level, isFeiSheng, appearanceProp, arenaDetails, mateData)
    if self.playerInfo.id and self.playerInfo.id ~= targetId then return end

    self.playerInfo = {
        id = targetId,
        name = name,
        gender = gender,
        class = class,
        level = level,
        isFeiSheng = isFeiSheng,
        appearanceProp = appearanceProp,
        arenaDetails = g_MessagePack.unpack(arenaDetails),
        mateData = g_MessagePack.unpack(mateData)
    }
    CUIManager.ShowUI(CLuaUIResources.ArenaPlayerInfoWnd)
end

function LuaArenaMgr:SendArenaOpenInfo(matchType, openedTypeData)
    g_ScriptEvent:BroadcastInLua("SendArenaOpenInfo", matchType, openedTypeData)
end

function LuaArenaMgr:SendNewArenaRankInfo(myScore, myRank, seasonId, levelType, isCross, page, totalPage, rankData)
    g_ScriptEvent:BroadcastInLua("SendNewArenaRankInfo", myScore, myRank, seasonId, levelType, isCross, page, totalPage, rankData)
end

function LuaArenaMgr:SyncArenaSignupState(matching, matchType)
    if self.isFunctionOpen then
        matchType = matching and matchType or 0
        g_ScriptEvent:BroadcastInLua("SyncArenaSignupState", matchType)
    else
        LuaColiseumMgr:SyncArenaSignupState(matching, matchType)
    end
end

function LuaArenaMgr:SendArenaPassportInfo(curExp, curLevel, rewardedLevel, weekExp, weekExpLimit)
    g_ScriptEvent:BroadcastInLua("SendArenaPassportInfo", curExp, curLevel, rewardedLevel, weekExp, weekExpLimit)
end

function LuaArenaMgr:SendArenaPassportRankInfo(myRank, rankType, rankData)
    g_ScriptEvent:BroadcastInLua("SendArenaPassportRankInfo", myRank, rankType, rankData)
end

function LuaArenaMgr:SendArenaPassportGiftInfo(jadeLimit, gotLevel, friendData)
    g_ScriptEvent:BroadcastInLua("SendArenaPassportGiftInfo", jadeLimit, gotLevel, friendData)
end

function LuaArenaMgr:SendArenaTaskInfo(currentWeek, openedWeek, activedWeek, starData, taskData, finishedWeekData)
    g_ScriptEvent:BroadcastInLua("SendArenaTaskInfo", currentWeek, openedWeek, activedWeek, starData, taskData, finishedWeekData)
end

function LuaArenaMgr:SendNewArenaPlayResult(matchType, winType, changeRankScore, newRankScore, addShopScore, totalShopScore, winComboTimes, playStartTime, weekPassportExp, weekPassportExpLimit, teamInfo, playerData, taskData)
    teamInfo = MsgPackImpl.unpack(teamInfo)
    local teamTbl = {}
    local mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    CommonDefs.ListIterate(teamInfo, DelegateFactory.Action_object(function(val)
        local playerId = val.playerId
        if playerId == mainPlayerId then
			table.insert(teamTbl, 1, val)
		else
			table.insert(teamTbl, val)
		end
	end))

    self.playResultInfo = {
        matchType = matchType,
        winType = winType,
        changeRankScore = changeRankScore,
        newRankScore = newRankScore,
        addShopScore = addShopScore,
        totalShopScore = totalShopScore,
        playStartTime = playStartTime,
        weekPassportExp = weekPassportExp,
        weekPassportExpLimit = weekPassportExpLimit,
        winComboTimes = winComboTimes,
        teamInfo = teamTbl,
        playerData = g_MessagePack.unpack(playerData),
        taskData = g_MessagePack.unpack(taskData),
    }

    RegisterTickOnce(function()
        if CClientMainPlayer.Inst then
            CUIManager.ShowUI(CLuaUIResources.ArenaResultWnd)
        end
    end, 3000)
end

function LuaArenaMgr:SetArenaActiveTaskWeekResult(activedWeek)
    g_ScriptEvent:BroadcastInLua("SetArenaActiveTaskWeekResult", activedWeek)
end

--#endregion

function LuaArenaMgr:GetSeasonInfoClass()
    if CommonDefs.IS_HMT_CLIENT then
        return Arena_SeasonInfo_HMT
    elseif CommonDefs.IS_VN_CLIENT then
        return Arena_SeasonInfo_VN
    else
        return Arena_SeasonInfo
    end
end

function LuaArenaMgr:GetCurrentSeasonId()
    local seasonInfo = self:GetSeasonInfoClass()
    local nowTimeStamp = CServerTimeMgr.Inst.timeStamp
    local currentSeasonId = 0
    seasonInfo.Foreach(function(key, data)
        local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.StartTime)
        local endTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
        if nowTimeStamp >= startTime and nowTimeStamp <= endTime then
            currentSeasonId = key
        end
    end)
    return currentSeasonId
end
