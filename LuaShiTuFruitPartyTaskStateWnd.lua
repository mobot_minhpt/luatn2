local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"

LuaShiTuFruitPartyTaskStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShiTuFruitPartyTaskStateWnd, "m_ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaShiTuFruitPartyTaskStateWnd, "m_WaterMelons", "Watermelons", GameObject)
RegistChildComponent(LuaShiTuFruitPartyTaskStateWnd, "m_ComboRoot", "ComboRoot", GameObject)
RegistChildComponent(LuaShiTuFruitPartyTaskStateWnd, "m_Label", "Label", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuFruitPartyTaskStateWnd, "m_StartTime")
RegistClassMember(LuaShiTuFruitPartyTaskStateWnd, "m_EndTime")
RegistClassMember(LuaShiTuFruitPartyTaskStateWnd, "m_Tick")

function LuaShiTuFruitPartyTaskStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.m_ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuFruitPartyTaskStateWnd:Init()
	self.m_ComboRoot:SetActive(false)
	self:RefreshWaterMelons()
	self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
end

--@region UIEvent

function LuaShiTuFruitPartyTaskStateWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

function LuaShiTuFruitPartyTaskStateWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncShuiGuoPaiDuiXiGuaNum", self, "OnSyncShuiGuoPaiDuiXiGuaNum")
	g_ScriptEvent:AddListener("SyncShuiGuoPaiDuiLianJi", self, "OnSyncShuiGuoPaiDuiLianJi")
end

function LuaShiTuFruitPartyTaskStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncShuiGuoPaiDuiXiGuaNum", self, "OnSyncShuiGuoPaiDuiXiGuaNum")
	g_ScriptEvent:RemoveListener("SyncShuiGuoPaiDuiLianJi", self, "OnSyncShuiGuoPaiDuiLianJi")
end

function LuaShiTuFruitPartyTaskStateWnd:OnSyncShuiGuoPaiDuiXiGuaNum(xiGuaNum, needXiGuaNum, bAddXiGua)
	self:RefreshWaterMelons()
end

function LuaShiTuFruitPartyTaskStateWnd:OnSyncShuiGuoPaiDuiLianJi(lianJiCount)
	self:ShowCombo(lianJiCount, 2)
end

function LuaShiTuFruitPartyTaskStateWnd:RefreshWaterMelons()
	local curNum = LuaShiTuMgr.m_XiGuaInfo.curNum and LuaShiTuMgr.m_XiGuaInfo.curNum or 0
	for i = 0, self.m_WaterMelons.transform.childCount - 1 do
		local item = self.m_WaterMelons.transform:GetChild(i).gameObject
		CUICommonDef.SetGrey(item, i>=curNum)
	end
end

function LuaShiTuFruitPartyTaskStateWnd:ShowCombo(num, time)
	self.m_StartTime = CServerTimeMgr.Inst:GetZone8Time()
    self.m_EndTime = self.m_StartTime:AddSeconds(time)
	self.m_Label.text = num
	self.m_ComboRoot:SetActive(true)
end

function LuaShiTuFruitPartyTaskStateWnd:OnTick()
	local now = CServerTimeMgr.Inst:GetZone8Time()
	if self.m_EndTime and DateTime.Compare(now, self.m_EndTime) > 0 then
		self.m_ComboRoot:SetActive(false)
	end
end

function LuaShiTuFruitPartyTaskStateWnd:OnDestroy()
    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
