-- Auto Generated!!
local CCallBackFriendTemplate = import "L10.UI.CCallBackFriendTemplate"
local CCallBackFriendWnd = import "L10.UI.CCallBackFriendWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CSigninMgr = import "L10.Game.CSigninMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Huiliu_Setting = import "L10.Game.Huiliu_Setting"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Shop_PlayScore = import "L10.Game.Shop_PlayScore"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CCallBackFriendWnd.m_Init_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.table.transform)
    this.friendTemplate:SetActive(false)
    this.titleLabel.text = Huiliu_Setting.GetData("ZhaohuiNotice").Value
    this.totalPointLabel.text = ""
    this.tipsLabel.text = LocalString.GetString("暂无可召回的好友~")
    this:RefreshInviteHuiLiuScore()

    Gac2Gas.GetInvitedHuiLiuPlayerInfo()
end
CCallBackFriendWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.obtainPointButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.obtainPointButton).onClick, MakeDelegateFromCSFunction(this.OnObtainButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.exchangeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.exchangeButton).onClick, MakeDelegateFromCSFunction(this.OnExchangeButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.OnCallBackPlayerInfoUpdate, MakeDelegateFromCSFunction(this.OnInfoUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerPlayPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerPlayPropUpdate, Action0, this))
end
CCallBackFriendWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.obtainPointButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.obtainPointButton).onClick, MakeDelegateFromCSFunction(this.OnObtainButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.exchangeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.exchangeButton).onClick, MakeDelegateFromCSFunction(this.OnExchangeButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.OnCallBackPlayerInfoUpdate, MakeDelegateFromCSFunction(this.OnInfoUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerPlayPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerPlayPropUpdate, Action0, this))
end
CCallBackFriendWnd.m_RefreshInviteHuiLiuScore_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        local val = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.InviteHuiLiu)
        this.totalPointLabel.text = tostring(val)
    end
end
CCallBackFriendWnd.m_OnExchangeButtonClick_CS2LuaHook = function (this, go) 
    --打开积分兑换商店
    Shop_PlayScore.ForeachKey(DelegateFactory.Action_object(function (key) 
        local playScore = Shop_PlayScore.GetData(key)
        if playScore.Key == ToStringWrap(EnumPlayScoreKey.InviteHuiLiu) then
            --CNPCShopInfoMgr.LastOpenShopName = playScore.Name;
            CLuaNPCShopInfoMgr.ShowScoreShopById(key)
            return
        end
    end))
end
CCallBackFriendWnd.m_OnInfoUpdate_CS2LuaHook = function (this) 
    if CSigninMgr.Inst.callBackFriendList.Count == 0 then
        return
    end
    this.tipsLabel.text = ""
    do
        local i = 0
        while i < CSigninMgr.Inst.callBackFriendList.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.friendTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CCallBackFriendTemplate))
            if template ~= nil then
                template:Init(CSigninMgr.Inst.callBackFriendList[i])
            end
            i = i + 1
        end
    end
    this.table:Reposition()
end
