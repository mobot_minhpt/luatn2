
local UISprite          = import "UISprite"
local UILabel           = import "UILabel"
local CChatLinkMgr      = import "CChatLinkMgr"

LuaFamilyTagDetailWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaFamilyTagDetailWnd,         "TagLable",            UILabel)
RegistChildComponent(LuaFamilyTagDetailWnd,         "HeartButton",         GameObject)
RegistChildComponent(LuaFamilyTagDetailWnd,         "heart",               UISprite)
RegistChildComponent(LuaFamilyTagDetailWnd,         "FavorNum",            UILabel)
RegistChildComponent(LuaFamilyTagDetailWnd,         "AddName",             UILabel)
RegistChildComponent(LuaFamilyTagDetailWnd,         "FavorsName",          UILabel)
RegistChildComponent(LuaFamilyTagDetailWnd,         "Background",          UISprite)
RegistChildComponent(LuaFamilyTagDetailWnd,         "FavorFX",             GameObject)

---------RegistClassMember-------
RegistClassMember(LuaFamilyTagDetailWnd,            "FixHeight")

function LuaFamilyTagDetailWnd:Awake()
    self.FixHeight = 270    --除了点赞列表外ui的固定高度

    self.FavorFX:SetActive(false)
    UIEventListener.Get(self.HeartButton).onClick = DelegateFactory.VoidDelegate(function(p)
        Gac2Gas.FavorPlayerFamilyTreeTag(luaFamilyMgr.TagCenterPlayerID,luaFamilyMgr.DetailTable.tagId,luaFamilyMgr.m_favoredTagId ~= luaFamilyMgr.DetailTable.tagId)
    end)

    self.TagLable.text = luaFamilyMgr.DetailTable.tagName
    if not luaFamilyMgr.DetailTable.writerName or luaFamilyMgr.DetailTable.writerName == '' then
      luaFamilyMgr.DetailTable.writerName = luaFamilyMgr.DetailTable.writerId
    end
    self.AddName.text = CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3("<link player=%s,%s>",luaFamilyMgr.DetailTable.writerId,luaFamilyMgr.DetailTable.writerName),false)
    UIEventListener.Get(self.AddName.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)

    self:RefreshTag(luaFamilyMgr.DetailTable.tagId==luaFamilyMgr.m_favoredTagId,luaFamilyMgr.DetailTable.favorNum)
    self:RefreshFavorsName()
    self:RefreshBgSize()
end

function LuaFamilyTagDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("FavorPlayerFamilyTreeTagSuccess", self, "FavorPlayerFamilyTreeTagSuccess")
end

function LuaFamilyTagDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FavorPlayerFamilyTreeTagSuccess", self, "FavorPlayerFamilyTreeTagSuccess")
    luaFamilyMgr.DetailTable = {}
end

function LuaFamilyTagDetailWnd:RefreshFavorsName()
    self.FavorsName.text = ""
    if #luaFamilyMgr.DetailTable.favorTable ~= 0 then
        for i,v in ipairs(luaFamilyMgr.DetailTable.favorTable) do
          if not v.name or v.name == '' then
            v.name = v.id
          end
            self.FavorsName.text = self.FavorsName.text..CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3("<link player=%s,%s>",v.id,v.name ),false)
            UIEventListener.Get(self.FavorsName.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                -- local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)     --  这种方式在label空白处点击会获取为第一个url

                local index = p:GetComponent(typeof(UILabel)):GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
                if index == 0 then
                    index = - 1
                end
                local url = p:GetComponent(typeof(UILabel)):GetUrlAtCharacterIndex(index)
                if url ~= nil then
                    CChatLinkMgr.ProcessLinkClick(url, nil)
                end
            end)
        end
        if luaFamilyMgr.DetailTable.favorNum >20 then
            self.FavorsName.text = self.FavorsName.text.."..."
        end
        self.FavorsName.color = Color.white
    else
        self.FavorsName.text = LocalString.GetString("暂无")
        self.FavorsName.color = Color.gray
    end
end

function LuaFamilyTagDetailWnd:RefreshTag(favor,favorNum)
    if favor then
        self.heart.spriteName = "personalspacewnd_heart_2"
    else
        self.heart.spriteName = "personalspacewnd_heart_1"
    end
    self.FavorNum.text = favorNum
end

function LuaFamilyTagDetailWnd:RefreshBgSize()
    self.Background.height = self.FavorsName.localSize.y + self.FixHeight
end

function LuaFamilyTagDetailWnd:FavorPlayerFamilyTreeTagSuccess(playerId, tagId, favor, favorNum)
    if playerId == luaFamilyMgr.TagCenterPlayerID then
        if favor then
            self.FavorFX:SetActive(true)
        else
            self.FavorFX:SetActive(false)
        end
        self:RefreshTag(favor,favorNum)
        self:RefreshFavorsName()
        self:RefreshBgSize()
    end
end
