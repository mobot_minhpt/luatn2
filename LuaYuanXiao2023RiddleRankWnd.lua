local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"
local Constants = import "L10.Game.Constants"
local CIMMgr = import "L10.Game.CIMMgr"
local UISearchBar = import "L10.UI.UISearchBar"

LuaYuanXiao2023RiddleRankWnd = class()

RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_TableView")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_RankList")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_FullRankList")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_MyRank")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_MyRankLabel")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_MyRankSprite")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_MyNameLabel")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_MyNumLabel")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"m_TableViewDataSource")

RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"ClearBtn")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"SearchBtn")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"SearchBar")
RegistClassMember(LuaYuanXiao2023RiddleRankWnd,"Input")

function LuaYuanXiao2023RiddleRankWnd:Init()
    self.m_MyRankLabel = self.transform:Find("TableView/Pool/MainPlayerInfo/RankLabel"):GetComponent(typeof(UILabel))
    self.m_MyRankSprite = self.m_MyRankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    self.m_MyNameLabel = self.transform:Find("TableView/Pool/MainPlayerInfo/NameLabel"):GetComponent(typeof(UILabel))
    self.m_MyNumLabel = self.transform:Find("TableView/Pool/MainPlayerInfo/NumLabel"):GetComponent(typeof(UILabel))
    
    self.m_MyRank = 0
    self.m_RankList = {}
        
    self.m_MyNameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    self.m_MyRankLabel.text = LocalString.GetString("未上榜")
    self.m_MyNumLabel.text = LocalString.GetString("—")
    
    self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    self.m_TableViewDataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item, index)
            self:InitItem(item, index, self.m_RankList[index + 1])
        end
    )
    self.m_TableView.m_DataSource = self.m_TableViewDataSource

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)  
        local data = self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)

    self.SearchBar = self.transform:Find("Bottom/SearchBar"):GetComponent(typeof(UISearchBar))
    self.Input = self.transform:Find("Bottom/SearchBar/Input"):GetComponent(typeof(UIInput))
    self.ClearBtn = self.transform:Find("Bottom/SearchBar/ClearBtn"):GetComponent(typeof(CButton))
    self.SearchBtn = self.transform:Find("Bottom/SearchBar/SearchBtn"):GetComponent(typeof(CButton))

    self.ClearBtn.gameObject:SetActive(false)
    self.SearchBar.OnChange = DelegateFactory.Action_string(function(str)
		self.ClearBtn.gameObject:SetActive(str ~= "")
	end)

    UIEventListener.Get(self.ClearBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearBtnClick()
	end)
	UIEventListener.Get(self.SearchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSearchBtnClick()
	end)

    Gac2Gas.QueryRank(YuanXiao2023_Setting.GetData().RiddleRankId)
end

function LuaYuanXiao2023RiddleRankWnd:InitItem(item, index, info)
    item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

    local tf = item.transform
    local numLabel = tf:Find("NumLabel"):GetComponent(typeof(UILabel))
    numLabel.text = ""
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = ""
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text = ""
    local rankSprite = tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName = ""

    local rank = info.Rank

    if rank == self.m_MyRank then 
        numLabel.color = Color.green 
        nameLabel.color = Color.green 
        rankLabel.color = Color.green 
    else
        numLabel.color = Color.white 
        nameLabel.color = Color.white 
        rankLabel.color = Color.white 
    end

    rankSprite.gameObject:SetActive(true)
    if rank == 1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankSprite.spriteName = "Rank_No.3png"
    else
        rankSprite.gameObject:SetActive(false)
        rankLabel.text = tostring(rank)
    end

    nameLabel.text = info.Name
    numLabel.text = info.Value
end

function LuaYuanXiao2023RiddleRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    
end
function LuaYuanXiao2023RiddleRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaYuanXiao2023RiddleRankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    self.m_MyRank = myInfo.Rank
    if myInfo.Rank > 0 then
        local rank = myInfo.Rank
        self.m_MyRankLabel.text = ""
        self.m_MyRankSprite.gameObject:SetActive(true)
        if rank == 1 then
            self.m_MyRankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            self.m_MyRankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            self.m_MyRankSprite.spriteName = "Rank_No.3png"
        else
            self.m_MyRankSprite.gameObject:SetActive(false)
            self.m_MyRankLabel.text = tostring(rank)
        end
    else
        self.m_MyRankLabel.text = LocalString.GetString("未上榜")
    end

    self.m_MyNameLabel.text = myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end

    if myInfo.Value > 0 then
        self.m_MyNumLabel.text = myInfo.Value
    else
        self.m_MyNumLabel.text = LocalString.GetString("—")
    end

    self.m_RankList = {}
    self.m_FullRankList = {}
    for i = 1, CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i - 1])
        table.insert(self.m_FullRankList, CRankData.Inst.RankList[i - 1])
    end

    self.m_TableView:ReloadData(true, false)
end

function LuaYuanXiao2023RiddleRankWnd:OnSearchBtnClick()
	local keywords = StringTrim(self.Input.value)
    self.m_RankList = {}
    if System.String.IsNullOrEmpty(keywords) then
        for i = 1, #self.m_FullRankList do
            table.insert(self.m_RankList, self.m_FullRankList[i])
        end
    else
        for i = 1, #self.m_FullRankList do
            if string.find(self.m_FullRankList[i].Name, keywords) then
                table.insert(self.m_RankList, self.m_FullRankList[i])
            end
        end
    end
    self.m_TableView:ReloadData(true, false)
end

function LuaYuanXiao2023RiddleRankWnd:OnClearBtnClick()
	self.Input.value = ""
end
