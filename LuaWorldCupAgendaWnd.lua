require("common/common_include")

local QnRadioBox = import "L10.UI.QnRadioBox" 

require("ui/worldcup/LuaWorldCupXiaoZuSaiView")
require("ui/worldcup/LuaWorldCupJueSaiView")

CLuaWorldCupAgendaWnd=class()
RegistClassMember(CLuaWorldCupAgendaWnd, "m_XiaoZuSaiView")
RegistClassMember(CLuaWorldCupAgendaWnd, "m_JueSaiView")
RegistClassMember(CLuaWorldCupAgendaWnd, "m_XiaoZuSaiViewGameObject")
RegistClassMember(CLuaWorldCupAgendaWnd, "m_JueSaiViewGameObject")
RegistClassMember(CLuaWorldCupAgendaWnd, "m_RadioBox")

function CLuaWorldCupAgendaWnd:Init()
    self.m_XiaoZuSaiView = CLuaWorldCupXiaoZuSaiView:new()
    self.m_XiaoZuSaiView:Init(FindChild(self.transform, "XiaoZuSaiView"))

    self.m_JueSaiView = CLuaWorldCupJueSaiView:new()
    self.m_JueSaiView:Init(FindChild(self.transform, "JueSaiView"))

    self.m_XiaoZuSaiViewGameObject = FindChild(self.transform, "XiaoZuSaiView").gameObject
    self.m_JueSaiViewGameObject = FindChild(self.transform, "JueSaiView").gameObject

    self.m_RadioBox = FindChild(self.transform, "QnRadioBox"):GetComponent(typeof(QnRadioBox))

    self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function (p1,p2)
        if p2==0 then
            self.m_XiaoZuSaiViewGameObject:SetActive(true)
            self.m_JueSaiViewGameObject:SetActive(false)
            self.m_XiaoZuSaiView:OnSelected()
        elseif p2==1 then
            self.m_XiaoZuSaiViewGameObject:SetActive(false)
            self.m_JueSaiViewGameObject:SetActive(true)
            self.m_JueSaiView:OnSelected()
        end
    end)

    self.m_XiaoZuSaiViewGameObject:SetActive(true)
    self.m_JueSaiViewGameObject:SetActive(false)
    self.m_XiaoZuSaiView:OnSelected()
end

function CLuaWorldCupAgendaWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyWorldCupAgendaInfo", self, "ReplyWorldCupAgendaInfo")
end

function CLuaWorldCupAgendaWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyWorldCupAgendaInfo", self, "ReplyWorldCupAgendaInfo")
end

function CLuaWorldCupAgendaWnd:ReplyWorldCupAgendaInfo(data)
    if self.m_JueSaiView then
        self.m_JueSaiView:Refresh(data)
    end
end

return CLuaWorldCupAgendaWnd
