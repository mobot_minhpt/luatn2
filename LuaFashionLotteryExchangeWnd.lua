local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr=import "L10.Game.CItemMgr"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItem = import "L10.Game.CItem"
local QualityColor = import "L10.Game.QualityColor"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local QnTableItem = import "L10.UI.QnTableItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaFashionLotteryExchangeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFashionLotteryExchangeWnd, "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)          
RegistChildComponent(LuaFashionLotteryExchangeWnd, "CostAndOwn", GameObject)
RegistChildComponent(LuaFashionLotteryExchangeWnd, "ItemGrid", UIGrid)
RegistChildComponent(LuaFashionLotteryExchangeWnd, "ItemTemplate", GameObject)
RegistChildComponent(LuaFashionLotteryExchangeWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaFashionLotteryExchangeWnd, "ConfirmBtn", GameObject)

RegistClassMember(LuaFashionLotteryExchangeWnd, "m_ItemList")
RegistClassMember(LuaFashionLotteryExchangeWnd, "m_UniTicketId")
RegistClassMember(LuaFashionLotteryExchangeWnd, "m_UniTicketIcon")
RegistClassMember(LuaFashionLotteryExchangeWnd, "m_UniTicketCostLabel")
RegistClassMember(LuaFashionLotteryExchangeWnd, "m_UniTicketOwnLabel")

RegistClassMember(LuaFashionLotteryExchangeWnd, "m_SelectItemId")
RegistClassMember(LuaFashionLotteryExchangeWnd, "m_Price")
RegistClassMember(LuaFashionLotteryExchangeWnd, "m_UniTicketCount")
--@endregion RegistChildComponent end

function LuaFashionLotteryExchangeWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:OnValueChanged(val)
    end)
    UIEventListener.Get(self.RuleBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.ConfirmBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnConfirmBtnClick()
    end)
end

function LuaFashionLotteryExchangeWnd:Init()
    self.ItemTemplate:SetActive(false)
    local data = FashionLottery_Activity.GetData(LuaFashionLotteryMgr.m_ActivityId)
    self.m_UniTicketId = data.UniTicketId
    self:InitCostAndOwn()
    self:InitItemGrid()
end

function LuaFashionLotteryExchangeWnd:InitCostAndOwn()
    local ticketData = CItemMgr.Inst:GetItemTemplate(self.m_UniTicketId)
    if ticketData then
        self.m_UniTicketIcon = ticketData.Icon
        self.m_UniTicketCostLabel = self.CostAndOwn.transform:Find("Cost"):GetComponent(typeof(UILabel))
        self.m_UniTicketOwnLabel = self.CostAndOwn.transform:Find("Own"):GetComponent(typeof(UILabel))
        self.CostAndOwn.transform:Find("Cost/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(self.m_UniTicketIcon)
        self.CostAndOwn.transform:Find("Own/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(self.m_UniTicketIcon)
        self.m_UniTicketCostLabel.text = tostring(0)
        self:UpdateTicketCount()
    end
end

function LuaFashionLotteryExchangeWnd:UpdateTicketCount()
    self.m_UniTicketCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_UniTicketId)
    self.m_UniTicketOwnLabel.text = tostring(self.m_UniTicketCount)
end

function LuaFashionLotteryExchangeWnd:InitItemGrid()
    self.m_ItemList = {}
    Extensions.RemoveAllChildren(self.ItemGrid.transform)
    local data = FashionLottery_ItemPreview.GetData(1)
    if data and data.Items then
        local itemInfoList = data.Items
        for i = 0, itemInfoList.Length - 1 do
            local cell = NGUITools.AddChild(self.ItemGrid.gameObject, self.ItemTemplate)
            self:InitOneItem(cell, i + 1, itemInfoList[i][0])
        end
    end
    self:OnItemClick(1)
    self.ItemGrid:Reposition()
end

function LuaFashionLotteryExchangeWnd:InitOneItem(cell, index, templateId)
    local itemData = CItemMgr.Inst:GetItemTemplate(templateId)
    local exchangeData = FashionLottery_Exchange.GetData(templateId)
    if itemData and exchangeData then
        cell.gameObject:SetActive(true)
        local nameLabel = cell.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = QualityColor.GetRGBValue(CItem.GetQualityType(templateId)) 
        nameLabel.text = itemData.Name
        local icon = cell.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        icon:LoadMaterial(itemData.Icon)
        cell.transform:Find("QualitySprite"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(itemData, nil, true)
        cell.transform:Find("CostIcon"):GetComponent(typeof(CUITexture)):LoadMaterial(self.m_UniTicketIcon)
        cell.transform:Find("PriceLabel"):GetComponent(typeof(UILabel)).text = tostring(exchangeData.TicketNum)
        UIEventListener.Get(cell).onClick =  DelegateFactory.VoidDelegate(function ()
            self:OnItemClick(index)
        end)
        UIEventListener.Get(icon.gameObject).onClick =  DelegateFactory.VoidDelegate(function ()
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
            self:OnItemClick(index)
        end)
        self.m_ItemList[index] = {id = templateId, cell = cell:GetComponent(typeof(QnTableItem)), price = exchangeData.TicketNum}
    end
end

function LuaFashionLotteryExchangeWnd:OnValueChanged(val)
    self.m_UniTicketCostLabel.text = tostring(val * self.m_Price)
end

--@region UIEvent
function LuaFashionLotteryExchangeWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("FashionLottery_Exchange_Rule_Tip")
end

function LuaFashionLotteryExchangeWnd:OnConfirmBtnClick()
    if self.m_SelectItemId then
        Gac2Gas.FashionLotteryExchangeFashion(self.m_SelectItemId, self.QnIncreseAndDecreaseButton:GetValue())
    end
end

function LuaFashionLotteryExchangeWnd:OnItemClick(index)
    for i, item in pairs(self.m_ItemList) do
        item.cell:SetSelected(i == index)
    end
    self.m_SelectItemId = self.m_ItemList[index].id
    self.m_Price = self.m_ItemList[index].price
    local maxCount = math.floor(self.m_UniTicketCount / self.m_Price)
    self.QnIncreseAndDecreaseButton:SetMinMax(1, math.max(1, maxCount), 1)
    self.QnIncreseAndDecreaseButton:SetValue(1)
    self:OnValueChanged(1)
end
--@endregion UIEvent

function LuaFashionLotteryExchangeWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "UpdateTicketCount")
	g_ScriptEvent:AddListener("SetItemAt", self, "UpdateTicketCount")
   
end

function LuaFashionLotteryExchangeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "UpdateTicketCount")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateTicketCount")
end