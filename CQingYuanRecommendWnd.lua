-- Auto Generated!!
local CChatHelper = import "L10.UI.CChatHelper"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CQingYuanMgr = import "L10.UI.CQingYuanMgr"
local CQingYuanRecommendItem = import "L10.UI.CQingYuanRecommendItem"
local CQingYuanRecommendWnd = import "L10.UI.CQingYuanRecommendWnd"
CQingYuanRecommendWnd.m_Init_CS2LuaHook = function (this) 
    this.tableView.m_DataSource = this

    if CQingYuanMgr.infoList ~= nil then
        this:OnRandomCanAcceptQingYuanPlayersResult()
    else
        this:Refresh(nil)
    end
end
CQingYuanRecommendWnd.m_Contact_CS2LuaHook = function (this, go) 
    if this.tableView.currentSelectRow >= 0 then
        local info = CQingYuanMgr.infoList[this.tableView.currentSelectRow]

        CChatHelper.ShowFriendWnd(math.floor(info.playerId), info.name)


        if CClientMainPlayer.Inst == nil then
            return
        end
        local msg = g_MessageMgr:FormatMessage("MATCH_PARTNER_MES")
        CIMMgr.Inst:SendMsg(info.playerId, CClientMainPlayer.Inst.Id, CClientMainPlayer.Inst.EngineId, CClientMainPlayer.Inst.Name, math.floor(EnumToInt(CClientMainPlayer.Inst.Class) or 0), math.floor(EnumToInt(CClientMainPlayer.Inst.Gender) or 0), msg, false)
    end
end
CQingYuanRecommendWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local cmp = TypeAs(view:GetFromPool(0), typeof(CQingYuanRecommendItem))
    if cmp ~= nil then
        cmp:Init(CQingYuanMgr.infoList[row])
        return cmp
    end
    return nil
end

