local Item_Item = import "L10.Game.Item_Item"

CLuaZhuangshiwuPreviewMgr = {}

CLuaZhuangshiwuPreviewMgr.TemplateId = nil

function CLuaZhuangshiwuPreviewMgr.OpenPreviewWnd(templateId)
    CLuaZhuangshiwuPreviewMgr.TemplateId = templateId
    local data = Item_Item.GetData(templateId)
    if data ~= nil then
        CUIManager.ShowUI(CLuaUIResources.ZhuangshiwuPreviewWnd)
    end
end

function CLuaZhuangshiwuPreviewMgr.Reset()
    CLuaZhuangshiwuPreviewMgr.TemplateId = nil
end
