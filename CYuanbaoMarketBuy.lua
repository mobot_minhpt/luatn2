-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CMarketItemInfo = import "L10.Game.CMarketItemInfo"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallGoodsItem = import "L10.UI.CShopMallGoodsItem"
local CYuanbaoMarketBuy = import "L10.UI.CYuanbaoMarketBuy"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
local EnumItemType = import "L10.Game.EnumItemType"
local EventManager = import "EventManager"
local EYuanBaoMarketType = import "L10.UI.EYuanBaoMarketType"
local Int32 = import "System.Int32"
local Mall_YuanBaoMarket = import "L10.Game.Mall_YuanBaoMarket"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local System = import "System"
local UInt32 = import "System.UInt32"
CYuanbaoMarketBuy.m_Init_CS2LuaHook = function (this)
    if not this.inited and CClientMainPlayer.Inst ~= nil and CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), CFreightMgr.Inst.taskId) and not CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CFreightMgr.Inst.taskId).IsFailed and (CFreightMgr.Inst.playerId == 0 or CFreightMgr.Inst.playerId == CClientMainPlayer.Inst.Id) then
        this.inited = true
        CFreightMgr.Inst.bIsOpenWnd = false
        Gac2Gas.GetCurrentFreightItems(CClientMainPlayer.Inst.Id)
        return
    end

    this.ownItemAmountLabel.gameObject:SetActive(false)
    this.m_SelectMarketType = EYuanBaoMarketType.WuShanShi
    this.m_SelectMarketItemIndex = 0
    this.m_BuyTableView:Init()

    Gac2Gas.RequestMarketItems()
    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.EYuanbaoMarket)
end
CYuanbaoMarketBuy.m_OnEnable_CS2LuaHook = function (this) 
    this.m_MarketTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_MarketTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectMarketItem, MakeGenericClass(Action1, Int32), this), true)
    this.m_BuyTableView.OnBuySectionSelect = CommonDefs.CombineListner_Action_int_int(this.m_BuyTableView.OnBuySectionSelect, MakeDelegateFromCSFunction(this.OnSelectMarketRadioBox, MakeGenericClass(Action2, Int32, Int32), this), true)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), true)

    EventManager.AddListenerInternal(EnumEventType.MarketItemInfoUpdate, MakeDelegateFromCSFunction(this.OnMarketItemUpdate, MakeGenericClass(Action1, CMarketItemInfo), this))
    EventManager.AddListenerInternal(EnumEventType.MarketItemInfoUpdateEnd, MakeDelegateFromCSFunction(this.OnMarketItemUpdateEnd, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.OneMarketItemInfoUpdate, MakeDelegateFromCSFunction(this.OnOneMarketItemUpdate, MakeGenericClass(Action1, CMarketItemInfo), this))
    EventManager.AddListenerInternal(EnumEventType.SendMallMarketItemLimitUpdate, MakeDelegateFromCSFunction(this.OnMarketItemLimitUpdate, MakeGenericClass(Action2, Int32, MakeGenericClass(List, Object)), this))
    EventManager.AddListenerInternal(EnumEventType.MarketItemRequestSuccess, MakeDelegateFromCSFunction(this.BuyRequestSuccess, MakeGenericClass(Action2, String, String), this))
    EventManager.AddListener(EnumEventType.FreightItemInfoGet, MakeDelegateFromCSFunction(this.Init, Action0, this))
end
CYuanbaoMarketBuy.m_OnDisable_CS2LuaHook = function (this) 
    this.m_MarketTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_MarketTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectMarketItem, MakeGenericClass(Action1, Int32), this), false)
    this.m_BuyTableView.OnBuySectionSelect = CommonDefs.CombineListner_Action_int_int(this.m_BuyTableView.OnBuySectionSelect, MakeDelegateFromCSFunction(this.OnSelectMarketRadioBox, MakeGenericClass(Action2, Int32, Int32), this), false)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), false)

    EventManager.RemoveListenerInternal(EnumEventType.MarketItemInfoUpdate, MakeDelegateFromCSFunction(this.OnMarketItemUpdate, MakeGenericClass(Action1, CMarketItemInfo), this))
    EventManager.RemoveListenerInternal(EnumEventType.MarketItemInfoUpdateEnd, MakeDelegateFromCSFunction(this.OnMarketItemUpdateEnd, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.OneMarketItemInfoUpdate, MakeDelegateFromCSFunction(this.OnOneMarketItemUpdate, MakeGenericClass(Action1, CMarketItemInfo), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendMallMarketItemLimitUpdate, MakeDelegateFromCSFunction(this.OnMarketItemLimitUpdate, MakeGenericClass(Action2, Int32, MakeGenericClass(List, Object)), this))
    EventManager.RemoveListenerInternal(EnumEventType.MarketItemRequestSuccess, MakeDelegateFromCSFunction(this.BuyRequestSuccess, MakeGenericClass(Action2, String, String), this))
    EventManager.RemoveListener(EnumEventType.FreightItemInfoGet, MakeDelegateFromCSFunction(this.Init, Action0, this))

    CYuanbaoMarketMgr.ClearYuanbaoMarketBuyData()
end
CYuanbaoMarketBuy.m_OnSelectMarketItem_CS2LuaHook = function (this, row)
    this.m_SelectMarketItemIndex = row
    if this.m_SelectMarketItemIndex >= 0 and this.m_SelectMarketItemIndex < this.mCurrentInfos.Count then
        local info = this.mCurrentInfos[this.m_SelectMarketItemIndex]
        local bindCount, notBindCount
        bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(info.ItemId)
        local count = bindCount + notBindCount
        this.ownItemAmountLabel.gameObject:SetActive(true)
        this.ownItemAmountLabel.text = tostring(count)
    end
end
CYuanbaoMarketBuy.m_OnBuyButtonClick_CS2LuaHook = function (this, button)    --买东西按钮点击,先去查询当前的Item状态再确定买不买
    if this.mCurrentInfos == nil then
        return
    end
    if this.m_SelectMarketItemIndex >= 0 and this.m_SelectMarketItemIndex < this.mCurrentInfos.Count then
        local info = this.mCurrentInfos[this.m_SelectMarketItemIndex]
        this.isBuying = true
        Gac2Gas.RequestMarketItem(info.ItemId)
    end
end
CYuanbaoMarketBuy.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if this.mCurrentInfos ~= nil then
        return this.mCurrentInfos.Count
    end
    return 0
end
CYuanbaoMarketBuy.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(view:GetFromPool(0), typeof(CShopMallGoodsItem))
    this:UpdateMarketItem(row, item)

    --点击图标的时候也选中该行
    item.OnClickItemTexture = DelegateFactory.Action(function () 
        view:SetSelectRow(row, true)
    end)
    return item
end
CYuanbaoMarketBuy.m_RefreshMarketTable_CS2LuaHook = function (this, type, selectRow, KeepLastScrollPos) 
    local __try_get_result
    __try_get_result, this.mCurrentInfos = CommonDefs.DictTryGet(this.m_MarketItems, typeof(Int32), EnumToInt(this.m_SelectMarketType), typeof(MakeGenericClass(List, CMarketItemInfo)))
    if CFeiShengMgr.Inst.IsOpen and type == EYuanBaoMarketType.Others then
        this.m_OtherBuyGuide:Init(EnumItemType.Other, 0)
    else
        this.m_OtherBuyGuide:Init(EnumItemType.Invalid, 0)
    end
    this.m_MarketTable:ReloadData(false, KeepLastScrollPos)
    this.m_MarketTable:SetSelectRow(selectRow, true)
end
CYuanbaoMarketBuy.m_UpdateMarketItem_CS2LuaHook = function (this, row, item)
    local info = this.mCurrentInfos[row]
    local templateId = info.ItemId
    local price = info.YuanBaoPrice
    local discountInfo = this:GetPriceChangeStr(info.YuanBaoPrice, info.YuanBaoPriceLastDay)
    local avaliableCount = - 1
    if CommonDefs.DictContains(this.m_MarketItemLimits, typeof(UInt32), info.ItemId) and info.SystemStock == 0 then
        avaliableCount = CommonDefs.DictGetValue(this.m_MarketItemLimits, typeof(UInt32), info.ItemId)
    end
    item:UpdateData(templateId, price, discountInfo, avaliableCount, true, false)

    if CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), CFreightMgr.Inst.taskId) and not CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CFreightMgr.Inst.taskId).IsFailed then
        local needTag = false
        do
            local i = 0
            while i < CFreightMgr.Inst.cargoList.Count do
                if CFreightMgr.Inst.cargoList[i].templateId == templateId and (CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilled or CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilledNeedHelp) then
                    needTag = true
                    break
                end
                i = i + 1
            end
        end
        item:UpdateData(templateId, price, discountInfo, avaliableCount, true, needTag)
    end

    local discountLabelColor = Color.white
    if info.YuanBaoPrice > info.YuanBaoPriceLastDay then
        discountLabelColor = Color.red
    elseif info.YuanBaoPrice < info.YuanBaoPriceLastDay then
        discountLabelColor = Color.green
    end
    item:UpdateDiscountColor(discountLabelColor)
end
CYuanbaoMarketBuy.m_GetPriceChangeStr_CS2LuaHook = function (this, currentPrice, lastdayPrice) 
    local priceInfo = 1 * currentPrice / lastdayPrice - 1
    if priceInfo > 0 then
        return System.String.Format("{0:F2}%", math.abs(priceInfo) * 100)
    elseif priceInfo < 0 then
        return System.String.Format("{0:F2}%", math.abs(priceInfo) * 100)
    else
        return ""
    end
end
CYuanbaoMarketBuy.m_AddOrUpdateItem_CS2LuaHook = function (this, info, type)
    local e_Type = EnumToInt(type)
    if not CommonDefs.DictContains(this.m_MarketItems, typeof(Int32), e_Type) then
        CommonDefs.DictAdd(this.m_MarketItems, typeof(Int32), e_Type, typeof(MakeGenericClass(List, CMarketItemInfo)), CreateFromClass(MakeGenericClass(List, CMarketItemInfo)))
    end
    if not CommonDefs.DictContains(this.m_MarketItemSet, typeof(Int32), e_Type) then
        CommonDefs.DictAdd(this.m_MarketItemSet, typeof(Int32), e_Type, typeof(MakeGenericClass(HashSet, UInt32)), CreateFromClass(MakeGenericClass(HashSet, UInt32)))
    end

    local find = false
    local index = 0
    do
        local i = 0
        while i < CommonDefs.DictGetValue(this.m_MarketItems, typeof(Int32), e_Type).Count do
            if CommonDefs.DictGetValue(this.m_MarketItems, typeof(Int32), e_Type)[i].ItemId == info.ItemId then
                index = i
                find = true
                CommonDefs.DictGetValue(this.m_MarketItems, typeof(Int32), e_Type)[index] = info
                if EnumToInt(this.m_SelectMarketType) == e_Type then
                    local item = TypeAs(this.m_MarketTable:GetItemAtRow(index), typeof(CShopMallGoodsItem))
                    if item ~= nil then
                        this:UpdateMarketItem(index, item)
                    end
                end
                break
            end
            i = i + 1
        end
    end

    if not find then
        this:RefreshMarketTable(this.m_SelectMarketType, 0, true)
    end
end
CYuanbaoMarketBuy.m_OnMarketItemUpdate_CS2LuaHook = function (this, info)
    local data = Mall_YuanBaoMarket.GetData(info.ItemId)
    local type = ((data.Category - 1) * 10 + data.Category2 - 1)
    if not CommonDefs.DictContains(this.m_MarketItems, typeof(Int32), type) then
        CommonDefs.DictAdd(this.m_MarketItems, typeof(Int32), type, typeof(MakeGenericClass(List, CMarketItemInfo)), CreateFromClass(MakeGenericClass(List, CMarketItemInfo)))
    end
    if not CommonDefs.DictContains(this.m_MarketItemSet, typeof(Int32), type) then
        CommonDefs.DictAdd(this.m_MarketItemSet, typeof(Int32), type, typeof(MakeGenericClass(HashSet, UInt32)), CreateFromClass(MakeGenericClass(HashSet, UInt32)))
    end
    if not CommonDefs.HashSetContains(CommonDefs.DictGetValue(this.m_MarketItemSet, typeof(Int32), type), typeof(UInt32), info.ItemId) then
        CommonDefs.HashSetAdd(CommonDefs.DictGetValue(this.m_MarketItemSet, typeof(Int32), type), typeof(UInt32), info.ItemId)
        CommonDefs.ListAdd(CommonDefs.DictGetValue(this.m_MarketItems, typeof(Int32), type), typeof(CMarketItemInfo), info)
    end
end
CYuanbaoMarketBuy.m_OnMarketItemUpdateEnd_CS2LuaHook = function (this, param)    CommonDefs.DictIterate(this.m_MarketItems, DelegateFactory.Action_object_object(function (___key, ___value) 
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        CommonDefs.ListSort1(kv.Value, typeof(CMarketItemInfo), DelegateFactory.Comparison_CMarketItemInfo(function (a, b) 
            local contain1 = false
            local contain2 = false
            if CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), CFreightMgr.Inst.taskId) and not CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CFreightMgr.Inst.taskId).IsFailed then
                do
                    local i = 0
                    while i < CFreightMgr.Inst.cargoList.Count do
                        if CFreightMgr.Inst.cargoList[i].templateId == a.ItemId then
                            contain1 = true
                        end
                        if CFreightMgr.Inst.cargoList[i].templateId == b.ItemId then
                            contain2 = true
                        end
                        i = i + 1
                    end
                end
            end
            local buy1 = CYuanbaoMarketMgr.BuyItemTemplateId == a.ItemId
            local buy2 = CYuanbaoMarketMgr.BuyItemTemplateId == b.ItemId

            local first = - 1
            if buy1 and not buy2 then
                return first
            elseif not buy1 and buy2 then
                return - first
            elseif contain1 and not contain2 then
                return first
            elseif not contain1 and contain2 then
                return - first
            else
                local index1 = Mall_YuanBaoMarket.GetData(a.ItemId).Index
                local index2 = Mall_YuanBaoMarket.GetData(b.ItemId).Index
                return index1 - index2
            end
        end))
    end))
    this:RefreshMarketTable(this.m_SelectMarketType, 0, true)
    if CYuanbaoMarketMgr.BuyWindowSectionIdx ~= 0 or CYuanbaoMarketMgr.BuyWindowRowIdx ~= 0 then
        this.m_BuyTableView:SetSelected(CYuanbaoMarketMgr.BuyWindowSectionIdx, CYuanbaoMarketMgr.BuyWindowRowIdx)
    end
end
CYuanbaoMarketBuy.m_OnOneMarketItemUpdate_CS2LuaHook = function (this, info)
    local data = Mall_YuanBaoMarket.GetData(info.ItemId)
    local type = CommonDefs.ConvertIntToEnum(typeof(EYuanBaoMarketType), ((data.Category - 1) * 10 + data.Category2 - 1))
    this:AddOrUpdateItem(info, type)

    if this.isBuying then
        this:OnBuy(info)
    end
    this.isBuying = false
end
CYuanbaoMarketBuy.m_OnMarketItemLimitUpdate_CS2LuaHook = function (this, regionId, limitInfos)
    if regionId == EShopMallRegion_lua.EYuanbaoMarket then
        CommonDefs.DictClear(this.m_MarketItemLimits)
        do
            local i = 0
            while i < limitInfos.Count do
                local key = math.floor(tonumber(limitInfos[i] or 0))
                local val = math.floor(tonumber(limitInfos[i + 1] or 0))
                CommonDefs.DictAdd(this.m_MarketItemLimits, typeof(UInt32), key, typeof(UInt32), val)
                i = i + 2
            end
        end
        this:RefreshMarketTable(this.m_SelectMarketType, this.m_SelectMarketItemIndex, true)
    end
end
CYuanbaoMarketBuy.m_BuyRequestSuccess_CS2LuaHook = function (this, param1, param2)
    local info = this.mCurrentInfos[this.m_SelectMarketItemIndex]
    Gac2Gas.RequestMarketItem(info.ItemId)
    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.EYuanbaoMarket)
end
