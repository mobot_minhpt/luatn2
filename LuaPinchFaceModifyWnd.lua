local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local CUITexture = import "L10.UI.CUITexture"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"

LuaPinchFaceModifyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaPinchFaceModifyWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaPinchFaceModifyWnd, "YingLingTipLabel", "YingLingTipLabel", UILabel)
RegistChildComponent(LuaPinchFaceModifyWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaPinchFaceModifyWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaPinchFaceModifyWnd, "Preselection1Texture", "Preselection1Texture", CUITexture)
RegistChildComponent(LuaPinchFaceModifyWnd, "Preselection2Texture", "Preselection2Texture", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFaceModifyWnd, "m_TabIndex")

function LuaPinchFaceModifyWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


    --@endregion EventBind end
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabBarChange(go, index)
	end)
end

function LuaPinchFaceModifyWnd:Init()
	self:InitTabBar()
	self.TabBar:ChangeTab((CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.FeiShengAppearanceXianFanStatus>0) and 1 or 0, false)
	self.ReadMeLabel.text = g_MessageMgr:FormatMessage("PinchFaceModifyWnd_ReadMe")
	self.YingLingTipLabel.text = g_MessageMgr:FormatMessage("PinchFaceModifyWnd_YingLingTip")
	self.YingLingTipLabel.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Class == EnumClass.YingLing)
	CUIManager.CloseUI(CUIResources.DialogWnd)
end

function LuaPinchFaceModifyWnd:InitTabBar()
	if not CClientMainPlayer.Inst then return end
	local id = EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.Gender)
	local skinData = PinchFace_HeadSkin.GetData(id)
	local hasFeisheng = CClientMainPlayer.Inst.HasFeiSheng
	if skinData then
		self.Preselection1Texture:LoadMaterial(skinData.Face1)
		self.Preselection2Texture:LoadMaterial(hasFeisheng and skinData.Face3 or skinData.Face2)
	end
end

--@region UIEvent

function LuaPinchFaceModifyWnd:OnOkButtonClick()
	local gender = CClientMainPlayer.Inst.AppearanceProp.Gender
	if EnumGender.Monster == gender then
		gender = CClientMainPlayer.Inst.Gender
	end
	LuaPinchFaceMgr:ShowModifyWnd(CClientMainPlayer.Inst.Class, gender, self.m_TabIndex == 1)
	CUIManager.CloseUI(CLuaUIResources.PinchFaceModifyWnd)
end

function LuaPinchFaceModifyWnd:OnTabBarChange(go, index)
	self.m_TabIndex = index
end
--@endregion UIEvent

