local UITable = import "UITable"
local QnTabView = import "L10.UI.QnTabView"
local Profession  = import "L10.Game.Profession"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local QnTabButton = import "L10.UI.QnTabButton"

LuaWuLiangCompanionInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangCompanionInfoWnd, "CompanionTable", "CompanionTable", UITable)
RegistChildComponent(LuaWuLiangCompanionInfoWnd, "ShowArea", "ShowArea", QnTabView)

--@endregion RegistChildComponent end

function LuaWuLiangCompanionInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaWuLiangCompanionInfoWnd:Init()
    Gac2Gas.WuLiangShenJing_QueryHuoBanData()
end

function LuaWuLiangCompanionInfoWnd:OnData(data)
    LuaWuLiangMgr.huobanData = data

    LuaWuLiangMgr.huobanSelectId = 0
    local idList = {}
    for id, huoBanInfo in pairs(LuaWuLiangMgr.huobanData.HuoBan) do
        table.insert(idList, id)
    end
    table.sort(idList)

    for _, id  in ipairs(idList) do
        if LuaWuLiangMgr.huobanSelectId == 0 then 
            LuaWuLiangMgr.huobanSelectId = id
            g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex", id)
        end
    end

    self:InitCompanionTable()
    self.ShowArea:ChangeTo(0)
end

-- 显示伙伴信息
function LuaWuLiangCompanionInfoWnd:InitCompanionTable()
    local data = LuaWuLiangMgr.huobanData.HuoBan

    for i=1,6 do
        local item = self.CompanionTable.transform:Find(tostring(i))
        if item then
            item.gameObject:SetActive(false)
        end
    end

    local idList = {}
    for id, huoBanInfo in pairs(data) do
        table.insert(idList, id)
    end

    table.sort(idList)

    local index = 1
    for _, id in ipairs(idList) do
        local huoBanInfo = data[id]

        local item = self.CompanionTable.transform:Find(tostring(index))
        index = index+1

        if item == nil then
            item = self.CompanionTable.transform:Find("Guide_Temp/"..tostring(index))
        end
        if item then
            item.gameObject:SetActive(true)

            local clsSprite = item:Find("ZhiYeSprite"):GetComponent(typeof(UISprite))
            local nameLabel = item:Find("NameLabel"):GetComponent(typeof(UILabel))
            local attributeLabel = item:Find("AttributeLabel"):GetComponent(typeof(UILabel))
            local skillLabel = item:Find("SkillLabel"):GetComponent(typeof(UILabel))
            local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
            local mark = item:Find("ZhanIcon").gameObject
    
            local monsterData = Monster_Monster.GetData(id)
            local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(id)
    
            clsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), huoBanData.Class))
            icon:LoadNPCPortrait(monsterData.HeadIcon, false)
            nameLabel.text = huoBanData.Name
    
            local skillCount = 0
            if huoBanInfo.SkillLevelTbl then
                for k, v in pairs(huoBanInfo.SkillLevelTbl) do
                    skillCount = skillCount + v
                end
            end
            skillLabel.text = tostring(skillCount)
            attributeLabel.text = tostring(huoBanInfo.InheritLevel or 0)
    
            mark:SetActive(self:HasChuZhan(id))
    
            if LuaWuLiangMgr.huobanSelectId == id then
                item:GetComponent(typeof(QnTabButton)).Selected = true
            else
                item:GetComponent(typeof(QnTabButton)).Selected = false
            end
    
            local btnIndex = index - 1
            -- 点击发送一个广播
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                for i=1,6 do
                    local item = self.CompanionTable.transform:Find(tostring(i))
                    if item == nil then
                        item = self.CompanionTable.transform:Find("Guide_Temp/"..tostring(index))
                    end
                    if item then
                        local btn = item:GetComponent(typeof(QnTabButton))
                        if btn then
                            btn.Selected = (i == btnIndex)
                        end
                    end
                end
                LuaWuLiangMgr.huobanSelectId = id
                g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex", id)
            end)
        end
       
    end
end

function LuaWuLiangCompanionInfoWnd:HasChuZhan(monsterId)
    for i, id in pairs(LuaWuLiangMgr.huobanData.ChuZhan) do
        if monsterId == id then
            return true
        end
    end
    return false
end

function LuaWuLiangCompanionInfoWnd:GetMonsterBtn(monsterId)
    local data = LuaWuLiangMgr.huobanData and LuaWuLiangMgr.huobanData.HuoBan or nil
    local index = 1
    local selectIndex = 1
    if data then
        -- 先排序
        local idList = {}
        for id, huoBanInfo in pairs(data) do
            table.insert(idList, id)
        end
        table.sort(idList)

        for _, id  in ipairs(idList) do
            if id == monsterId then
                selectIndex = index
            end
            index = index + 1
        end
    
        local item = self.CompanionTable.transform:Find(tostring(selectIndex))
        
        if item then
            return item.gameObject
        end
    end
    
    return nil
end

function LuaWuLiangCompanionInfoWnd:GetGuideGo(methodName)
    if methodName == "GetHuoBan1" then
        local monsterid = 18007722
        return self:GetMonsterBtn(monsterid)
    elseif methodName == "GetHuoBan2" then
        -- 白素贞
        local monsterid = 18007723
        return self:GetMonsterBtn(monsterid)
    elseif methodName == "GetChuZhan" then
        local item = self.transform:Find("ShowArea/WuLiangCompanionAttributeRoot/CompanionRoot/CompanionBtn")
        return item.gameObject
    elseif methodName == "GetSkillBtn" then
        local item = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/SkillFosterTab")
        return item.gameObject
    elseif methodName == "GetAttrBtn" then
        local item = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/AtrributeFosterTab")
        return item.gameObject
    elseif methodName == "GetAttrItem" then
        local item = self.transform:Find("ShowArea/WuLiangCompanionAttributeFosterRoot/LevelUp/ItemCell")
        return item.gameObject
    elseif methodName == "GetSkillItem" then
        local item = self.transform:Find("ShowArea/WuLiangCompanionSkillFosterRoot/SkillInfo/LevelUp/ItemCell")
        return item.gameObject
	end
    return nil
end

function LuaWuLiangCompanionInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("WuLiangShenJing_QueryHuoBanDataResult", self, "OnData")
    g_ScriptEvent:AddListener("WuLiangShenJing_SetChuZhan", self, "InitCompanionTable")
    g_ScriptEvent:AddListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitCompanionTable")
end

function LuaWuLiangCompanionInfoWnd:OnDisable()
    LuaWuLiangMgr.huobanSelectId = 0
    g_ScriptEvent:RemoveListener("WuLiangShenJing_QueryHuoBanDataResult", self, "OnData")
    g_ScriptEvent:RemoveListener("WuLiangShenJing_SetChuZhan", self, "InitCompanionTable")
    g_ScriptEvent:RemoveListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitCompanionTable")
end

--@region UIEvent

--@endregion UIEvent

