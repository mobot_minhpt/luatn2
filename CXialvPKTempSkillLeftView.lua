-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local CUITexture = import "L10.UI.CUITexture"
local CXialvPKTempSkillLeftView = import "L10.UI.CXialvPKTempSkillLeftView"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local QnTableItem = import "L10.UI.QnTableItem"
CXialvPKTempSkillLeftView.m_Awake_CS2LuaHook = function (this)
    this.m_ClassTable.m_DataSource = this
    this.m_ClassTable:ReloadData(false, false)
    this.m_ClassTable:SetSelectRow(0, true)
end
CXialvPKTempSkillLeftView.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(this.m_ClassTable:GetFromPool(0), typeof(QnTableItem))
    local button = CommonDefs.GetComponent_Component_Type(item, typeof(CButton))
    if button ~= nil then
        button.Text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.dataSource[row]))
    end
    local portrait = CommonDefs.GetComponentInChildren_Component_Type(item, typeof(CUITexture))
    if portrait ~= nil then
        local icon = Profession.GetLargeIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.dataSource[row]))
        portrait:LoadMaterial(icon)
    end
    return item
end
