local Vector3 = import "UnityEngine.Vector3"
local DelegateFactory = import "DelegateFactory"
local CUIResources = import "L10.UI.CUIResources"

CLuaChristmasCreateSnowmanStateWnd = class()

RegistClassMember(CLuaChristmasCreateSnowmanStateWnd,"m_Label")
RegistClassMember(CLuaChristmasCreateSnowmanStateWnd,"m_ExpandButton")
RegistClassMember(CLuaChristmasCreateSnowmanStateWnd,"m_SnowmanInfos")

function CLuaChristmasCreateSnowmanStateWnd:Awake()
    self:InitComponents()
end

function CLuaChristmasCreateSnowmanStateWnd:InitComponents()
    self.m_Label = self.transform:Find("Anchor/State/Label"):GetComponent(typeof(UILabel))
    self.m_ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
end

function CLuaChristmasCreateSnowmanStateWnd:Init()
    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function(p)
        self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    self.m_SnowmanInfos = {}
    ShengDan_SnowBall.Foreach(function(k, v)
        local info = {needBall = v.Disappeared, name = v.NpcName, stage = v.SpiritId}
        table.insert(self.m_SnowmanInfos, info)
    end)
end

function CLuaChristmasCreateSnowmanStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncShengDanCatchXueQiuNum", self, "OnSyncShengDanCatchXueQiuNum")
end

function CLuaChristmasCreateSnowmanStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncShengDanCatchXueQiuNum", self, "OnSyncShengDanCatchXueQiuNum")
end

function CLuaChristmasCreateSnowmanStateWnd:OnHideTopAndRightTipWnd( )
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function CLuaChristmasCreateSnowmanStateWnd:OnSyncShengDanCatchXueQiuNum(xueQiuNum)
    local nextName = self.m_SnowmanInfos[#self.m_SnowmanInfos].name
    local needBall = 0
    for i = 2, #self.m_SnowmanInfos do
        local info = self.m_SnowmanInfos[i]
        local lastStage = self.m_SnowmanInfos[i - 1].stage

        if info.needBall > xueQiuNum and lastStage ~= info.stage then
            nextName = info.name
            needBall = info.needBall - xueQiuNum
            break
        end
    end

    self.m_Label.text = g_MessageMgr:FormatMessage("Christmas_CreateSnowman_Spirit", xueQiuNum, needBall, nextName)
end
