local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CHideAndSeekSkillItem = import "L10.UI.CHideAndSeekSkillItem"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaCakeCuttingShowWnd = class()
RegistChildComponent(LuaCakeCuttingShowWnd,"closeBtn", GameObject)
RegistChildComponent(LuaCakeCuttingShowWnd,"showTextLabel", UILabel)
RegistChildComponent(LuaCakeCuttingShowWnd,"skillNode", CHideAndSeekSkillItem)
RegistChildComponent(LuaCakeCuttingShowWnd,"table", UITable)
RegistChildComponent(LuaCakeCuttingShowWnd,"scrollView", UIScrollView)
RegistChildComponent(LuaCakeCuttingShowWnd,"tableLabel", UILabel)
RegistChildComponent(LuaCakeCuttingShowWnd,"costNumLabel", UILabel)
RegistChildComponent(LuaCakeCuttingShowWnd,"attendBtn", GameObject)
RegistChildComponent(LuaCakeCuttingShowWnd,"cancelBtn", GameObject)
RegistChildComponent(LuaCakeCuttingShowWnd,"freeNode", GameObject)
RegistChildComponent(LuaCakeCuttingShowWnd,"costNode", GameObject)
--RegistClassMember(LuaCakeCuttingShowWnd, "maxChooseNum")

function LuaCakeCuttingShowWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CakeCuttingShowWnd)
end

function LuaCakeCuttingShowWnd:OnEnable()
	g_ScriptEvent:AddListener("CakeCuttingUpdateShowBtn", self, "SetButtonState")
end

function LuaCakeCuttingShowWnd:OnDisable()
	g_ScriptEvent:RemoveListener("CakeCuttingUpdateShowBtn", self, "SetButtonState")
end

function LuaCakeCuttingShowWnd:SetButtonState(result)
	self.attendBtn:SetActive(result)
	self.cancelBtn:SetActive(not result)
end

function LuaCakeCuttingShowWnd:Init()
	Gac2Gas.CakeCuttingCheckInMatching()

	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  self.showTextLabel.text = g_MessageMgr:FormatMessage("ZNQ_CakeCutting_Rules", nil)
	local skillId = ZhouNianQing_CakeCutting.GetData().TransformSkillId
  if skillId then
    self.skillNode:Init(skillId)
  end

	self.tableLabel.text = g_MessageMgr:FormatMessage("ZNQ_CakeCutting_Tips", nil)
  self.table:Reposition()
  self.scrollView:ResetPosition()

  local playId = 80
  local todayPlayNum = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(playId)

  if todayPlayNum <= 0 then
		self.freeNode:SetActive(true)
		self.costNode:SetActive(false)
  else
		self.freeNode:SetActive(false)
		self.costNode:SetActive(true)
    self.costNumLabel.text = ZhouNianQing_CakeCutting.GetData().SignUpCost
  end

	local onAttendClick = function(go)
		Gac2Gas.CakeCuttingSignUpPlay()
		--self.attendBtn:SetActive(false)
		--self.cancelBtn:SetActive(true)
	end
	CommonDefs.AddOnClickListener(self.attendBtn,DelegateFactory.Action_GameObject(onAttendClick),false)
	local onCancelClick = function(go)
		Gac2Gas.CakeCuttingCancelSignUp()
		--self.attendBtn:SetActive(true)
		--self.cancelBtn:SetActive(false)
	end
	CommonDefs.AddOnClickListener(self.cancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)
	self.attendBtn:SetActive(false)
	self.cancelBtn:SetActive(false)
end

return LuaCakeCuttingShowWnd
