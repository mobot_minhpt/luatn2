require("common/common_include")

local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local TweenAlpha=import "TweenAlpha"
local CItemMgr=import "L10.Game.CItemMgr"
CLuaHuoGuoInfoWnd=class()
RegistClassMember(CLuaHuoGuoInfoWnd,"m_TitleLabel")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_WarningSprites")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_Silders")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_RangeLowerLimit")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_RangeUpperLimit")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_MeterialUpperLimit")

RegistClassMember(CLuaHuoGuoInfoWnd,"m_SlidersTable")

RegistClassMember(CLuaHuoGuoInfoWnd,"m_MatNames")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_NameLabels")

RegistClassMember(CLuaHuoGuoInfoWnd,"m_SubmitButton1")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_SubmitButton2")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_SubmitButton3")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_BaJiaoTemplateId")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_HuaJiaoTemplateId")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_GuiPiTemplateId")
RegistClassMember(CLuaHuoGuoInfoWnd,"m_AddMaterialTipLabels")

function CLuaHuoGuoInfoWnd:Awake()
    self.m_BaJiaoTemplateId=21004221
    self.m_HuaJiaoTemplateId=21004222
    self.m_GuiPiTemplateId=21004223
end
function CLuaHuoGuoInfoWnd:Init()


    local tipTf=LuaGameObject.GetChildNoGC(self.transform,"Tips").transform
    self.m_AddMaterialTipLabels={}
    self.m_AddMaterialTipLabels[1]=LuaGameObject.GetChildNoGC(tipTf,"1").label
    self.m_AddMaterialTipLabels[2]=LuaGameObject.GetChildNoGC(tipTf,"2").label
    self.m_AddMaterialTipLabels[3]=LuaGameObject.GetChildNoGC(tipTf,"3").label

    self.m_NameLabels={}
    self.m_NameLabels[1]=LuaGameObject.GetChildNoGC(self.transform,"Name1").label
    self.m_NameLabels[2]=LuaGameObject.GetChildNoGC(self.transform,"Name2").label
    self.m_NameLabels[3]=LuaGameObject.GetChildNoGC(self.transform,"Name3").label
    
    self.m_MatNames={LocalString.GetString("八角"),LocalString.GetString("花椒"),LocalString.GetString("桂皮")}
    self.m_TitleLabel=LuaGameObject.GetChildNoGC(self.transform,"TitleLabel").label

    local spritesTf=LuaGameObject.GetChildNoGC(self.transform,"Warning").transform
    self.m_WarningSprites={}
    self.m_WarningSprites[1]=LuaGameObject.GetChildNoGC(spritesTf,"1").sprite
    self.m_WarningSprites[2]=LuaGameObject.GetChildNoGC(spritesTf,"2").sprite
    self.m_WarningSprites[3]=LuaGameObject.GetChildNoGC(spritesTf,"3").sprite

    local tf1=LuaGameObject.GetChildNoGC(self.transform,"Sliders1").transform
    local tf2=LuaGameObject.GetChildNoGC(self.transform,"Sliders2").transform
    local tf3=LuaGameObject.GetChildNoGC(self.transform,"Sliders3").transform
    local baJiaoSliders={}
    baJiaoSliders[1]=LuaGameObject.GetChildNoGC(tf1,"Slider1").slider
    baJiaoSliders[1].value=0
    baJiaoSliders[2]=LuaGameObject.GetChildNoGC(tf1,"Slider2").slider
    baJiaoSliders[2].value=0
    baJiaoSliders[3]=LuaGameObject.GetChildNoGC(tf1,"Slider3").slider
    baJiaoSliders[3].value=0
    local huaJiaoSliders={}
    huaJiaoSliders[1]=LuaGameObject.GetChildNoGC(tf2,"Slider1").slider
    huaJiaoSliders[1].value=0
    huaJiaoSliders[2]=LuaGameObject.GetChildNoGC(tf2,"Slider2").slider
    huaJiaoSliders[2].value=0
    huaJiaoSliders[3]=LuaGameObject.GetChildNoGC(tf2,"Slider3").slider
    huaJiaoSliders[3].value=0
    local guiPiSliders={}
    guiPiSliders[1]=LuaGameObject.GetChildNoGC(tf3,"Slider1").slider
    guiPiSliders[1].value=0
    guiPiSliders[2]=LuaGameObject.GetChildNoGC(tf3,"Slider2").slider
    guiPiSliders[2].value=0
    guiPiSliders[3]=LuaGameObject.GetChildNoGC(tf3,"Slider3").slider
    guiPiSliders[3].value=0
    self.m_SlidersTable={baJiaoSliders,huaJiaoSliders,guiPiSliders}

    self.m_SubmitButton1=LuaGameObject.GetChildNoGC(self.transform,"Button1").gameObject
    self.m_SubmitButton2=LuaGameObject.GetChildNoGC(self.transform,"Button2").gameObject
    self.m_SubmitButton3=LuaGameObject.GetChildNoGC(self.transform,"Button3").gameObject
    local function OnClick(go)
        self:OnClickAddButton(go)
    end
    CommonDefs.AddOnClickListener(self.m_SubmitButton1,DelegateFactory.Action_GameObject(OnClick),false)
    CommonDefs.AddOnClickListener(self.m_SubmitButton2,DelegateFactory.Action_GameObject(OnClick),false)
    CommonDefs.AddOnClickListener(self.m_SubmitButton3,DelegateFactory.Action_GameObject(OnClick),false)


    self.m_RangeLowerLimit=30
    self.m_RangeUpperLimit=50
    self.m_MeterialUpperLimit=100
    self.m_WarningSprites[1].gameObject:SetActive(false)
    self.m_WarningSprites[2].gameObject:SetActive(false)
    self.m_WarningSprites[3].gameObject:SetActive(false)
    Gac2Gas.QueryCurrentHuoGuoMeterial()

    -- self:RefreshUI({10,70,40})
    local count=CItemMgr.Inst:GetItemCount(self.m_BaJiaoTemplateId)
    self.m_NameLabels[1].text=SafeStringFormat3( "%s(%d)",self.m_MatNames[1],count )
    count=CItemMgr.Inst:GetItemCount(self.m_HuaJiaoTemplateId)
    self.m_NameLabels[2].text=SafeStringFormat3( "%s(%d)",self.m_MatNames[2],count )
    count=CItemMgr.Inst:GetItemCount(self.m_GuiPiTemplateId)
    self.m_NameLabels[3].text=SafeStringFormat3( "%s(%d)",self.m_MatNames[3],count )
end
function CLuaHuoGuoInfoWnd:OnClickAddButton(go)
    if go==self.m_SubmitButton1 then
        -- self.m_AddMaterialTipLabels[1].alpha=1
        -- TweenAlpha.Begin(self.m_AddMaterialTipLabels[1].gameObject,1,0)
        Gac2Gas.SubmitHuoGuoMeterial(self.m_BaJiaoTemplateId)
    elseif go==self.m_SubmitButton2 then
        -- self.m_AddMaterialTipLabels[2].alpha=1
        -- TweenAlpha.Begin(self.m_AddMaterialTipLabels[2].gameObject,1,0)
        Gac2Gas.SubmitHuoGuoMeterial(self.m_HuaJiaoTemplateId)
    elseif go==self.m_SubmitButton3 then
        -- self.m_AddMaterialTipLabels[3].alpha=1
        -- TweenAlpha.Begin(self.m_AddMaterialTipLabels[3].gameObject,1,0)
        Gac2Gas.SubmitHuoGuoMeterial(self.m_GuiPiTemplateId)
    end
    Gac2Gas.QueryCurrentHuoGuoMeterial()
end

function CLuaHuoGuoInfoWnd:OnUpdateItemCount(args)
    local templateId=args[0]
    local count=args[1]
    if templateId==self.m_BaJiaoTemplateId then
        self.m_NameLabels[1].text=SafeStringFormat3( "%s(%d)",self.m_MatNames[1],count )
    elseif templateId==self.m_HuaJiaoTemplateId then
        self.m_NameLabels[2].text=SafeStringFormat3( "%s(%d)",self.m_MatNames[2],count )
    elseif templateId==self.m_GuiPiTemplateId then
        self.m_NameLabels[3].text=SafeStringFormat3( "%s(%d)",self.m_MatNames[3],count )
    end
end
function CLuaHuoGuoInfoWnd:OnEnable()
    CItemMgr.Inst:AddNeedUpdateCountItemTemplateId(self.m_BaJiaoTemplateId)
    CItemMgr.Inst:AddNeedUpdateCountItemTemplateId(self.m_HuaJiaoTemplateId)
    CItemMgr.Inst:AddNeedUpdateCountItemTemplateId(self.m_GuiPiTemplateId)

    g_ScriptEvent:AddListener("SendCurrentHuoGuoMeterial", self, "OnSendCurrentHuoGuoMeterial")
    g_ScriptEvent:AddListener("UpdateItemCount", self, "OnUpdateItemCount")
    g_ScriptEvent:AddListener("SubmitHuoGuoMeterialSuccess", self, "OnSubmitHuoGuoMeterialSuccess")
    
end

function CLuaHuoGuoInfoWnd:OnDisable()
    CItemMgr.Inst:RemoveNeedUpdateCountItemTemplateId(self.m_BaJiaoTemplateId)
    CItemMgr.Inst:RemoveNeedUpdateCountItemTemplateId(self.m_HuaJiaoTemplateId)
    CItemMgr.Inst:RemoveNeedUpdateCountItemTemplateId(self.m_GuiPiTemplateId)

    g_ScriptEvent:RemoveListener("SendCurrentHuoGuoMeterial", self, "OnSendCurrentHuoGuoMeterial")
    g_ScriptEvent:RemoveListener("UpdateItemCount", self, "OnUpdateItemCount")
    g_ScriptEvent:RemoveListener("SubmitHuoGuoMeterialSuccess", self, "OnSubmitHuoGuoMeterialSuccess")
end

function CLuaHuoGuoInfoWnd:OnSubmitHuoGuoMeterialSuccess(args)
    local templateId=args[0]
    if templateId==self.m_BaJiaoTemplateId then
        self.m_AddMaterialTipLabels[1].alpha=1
        TweenAlpha.Begin(self.m_AddMaterialTipLabels[1].gameObject,1,0)
    elseif templateId==self.m_HuaJiaoTemplateId then
        self.m_AddMaterialTipLabels[2].alpha=1
        TweenAlpha.Begin(self.m_AddMaterialTipLabels[2].gameObject,1,0)
    elseif templateId==self.m_GuiPiTemplateId then
        self.m_AddMaterialTipLabels[3].alpha=1
        TweenAlpha.Begin(self.m_AddMaterialTipLabels[3].gameObject,1,0)
    end
end

function CLuaHuoGuoInfoWnd:RefreshUI(matVal)
    local length=350
    local len1=length*self.m_RangeLowerLimit/self.m_MeterialUpperLimit
    local len2=length*(self.m_RangeUpperLimit-self.m_RangeLowerLimit)/self.m_MeterialUpperLimit
    local len3=length*(self.m_MeterialUpperLimit-self.m_RangeUpperLimit)/self.m_MeterialUpperLimit
    local lenTable={len1,len2,len3}
    for i,v in ipairs(self.m_SlidersTable) do
        LuaGameObject.GetChildNoGC(v[1].transform,"Sprite1").sprite.height=lenTable[1]
        LuaGameObject.GetChildNoGC(v[1].transform,"Sprite2").sprite.height=lenTable[1]
        LuaGameObject.GetChildNoGC(v[2].transform,"Sprite1").sprite.height=lenTable[2]
        LuaGameObject.GetChildNoGC(v[2].transform,"Sprite2").sprite.height=lenTable[2]
        LuaGameObject.GetChildNoGC(v[3].transform,"Sprite1").sprite.height=lenTable[3]
        LuaGameObject.GetChildNoGC(v[3].transform,"Sprite2").sprite.height=lenTable[3]
        v[2].transform.localPosition=Vector3(0,len1,0)
        v[3].transform.localPosition=Vector3(0,len1+len2,0)
    end    

    local tooless={}--太少
    local toomuch={}--太多

    for i,v in ipairs(matVal) do
        if v<self.m_RangeLowerLimit then
            table.insert( tooless,self.m_MatNames[i] )
            self.m_SlidersTable[i][1].value=v/self.m_RangeLowerLimit
            self.m_SlidersTable[i][2].value=0
            self.m_SlidersTable[i][3].value=0
            self.m_WarningSprites[i].gameObject:SetActive(true)
            self.m_WarningSprites[i].spriteName="common_notice_mark_yellow"
        elseif v>self.m_RangeUpperLimit then
            table.insert( toomuch,self.m_MatNames[i] )
            self.m_SlidersTable[i][1].value=1
            self.m_SlidersTable[i][2].value=1
            self.m_SlidersTable[i][3].value=(v-self.m_RangeUpperLimit)/(self.m_MeterialUpperLimit-self.m_RangeUpperLimit)
            self.m_WarningSprites[i].gameObject:SetActive(true)
            self.m_WarningSprites[i].spriteName="common_notice_mark_red"
        else
            self.m_SlidersTable[i][1].value=1
            self.m_SlidersTable[i][2].value=(v-self.m_RangeLowerLimit)/(self.m_RangeUpperLimit-self.m_RangeLowerLimit)
            self.m_SlidersTable[i][3].value=0
            self.m_WarningSprites[i].gameObject:SetActive(false)
        end
    end
    local desc=""
    if #tooless==0 and #toomuch==0 then
        desc=LocalString.GetString("一切正常哦！")
        self.m_WarningSprites[1].gameObject:SetActive(false)
        self.m_WarningSprites[2].gameObject:SetActive(false)
        self.m_WarningSprites[3].gameObject:SetActive(false)
    else

        if #tooless>0 then
            for i,v in ipairs(tooless) do
                desc=desc..v
                if i<#tooless then
                    desc=desc.."，"
                end
            end
            desc=desc..LocalString.GetString("太少了！")
        end
        if #toomuch>0 then
            for i,v in ipairs(toomuch) do
                desc=desc..v
                if i<#toomuch then
                    desc=desc.."，"
                end
            end
            desc=desc..LocalString.GetString("太多了！")
        end

    end
    self.m_TitleLabel.text=desc

end
function CLuaHuoGuoInfoWnd:OnSendCurrentHuoGuoMeterial(args)
    local array=args[0]
    self.m_RangeLowerLimit=array[0] or 30
    self.m_RangeUpperLimit=array[1] or 50
    self.m_MeterialUpperLimit=array[2] or 100

    local bajiao=array[3]
    local huajiao=array[4]
    local guipi=array[5]
    local matVal={array[3],array[4],array[5]}

    self:RefreshUI(matVal)
end

return CLuaHuoGuoInfoWnd
