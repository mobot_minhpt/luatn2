local Vector3 = import "UnityEngine.Vector3"
local Screen = import "UnityEngine.Screen"
local GameObject = import "UnityEngine.GameObject"
local EnumHongBaoRainType = import "L10.UI.EnumHongBaoRainType"
local CUITexture = import "L10.UI.CUITexture"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CLoudSpeakerNoticeWnd = import "L10.UI.CLoudSpeakerNoticeWnd"

CLuaHongBaoRain = class()
RegistClassMember(CLuaHongBaoRain,"m_HongBaoTemplate")
RegistClassMember(CLuaHongBaoRain,"m_ShengDanWaTemplate")--圣诞袜

RegistClassMember(CLuaHongBaoRain,"m_ShengDanTemplate")
RegistClassMember(CLuaHongBaoRain,"m_LaternTemplate")
-- RegistClassMember(CLuaHongBaoRain,"m_LiBaoTemplate")
RegistClassMember(CLuaHongBaoRain,"m_ButterflyTemplate")
RegistClassMember(CLuaHongBaoRain,"m_EmitIntervalTime")
RegistClassMember(CLuaHongBaoRain,"m_EmitTime")
RegistClassMember(CLuaHongBaoRain,"m_Time")
RegistClassMember(CLuaHongBaoRain,"m_TotalTime")
-- RegistClassMember(CLuaHongBaoRain,"m_LiBaoTemplateCnt")
RegistClassMember(CLuaHongBaoRain,"m_RainType")
RegistClassMember(CLuaHongBaoRain,"m_HongBaoPrefab")

RegistClassMember(CLuaHongBaoRain,"ButterFlyEmitIntervalTime")
RegistClassMember(CLuaHongBaoRain,"ButterFlyEmitTime")
RegistClassMember(CLuaHongBaoRain,"LaternEmitIntervalTime")
RegistClassMember(CLuaHongBaoRain,"LaternEmitTime")
-- 趣味喇叭
RegistClassMember(CLuaHongBaoRain,"m_QuweiLabaTemplate")

-- 婚礼糖果
RegistClassMember(CLuaHongBaoRain,"m_WeddingCandyTemplate")
-- 银两
RegistClassMember(CLuaHongBaoRain,"m_YinLiangTemplate")

function CLuaHongBaoRain:Awake()
    self.ButterFlyEmitIntervalTime=0.4
    self.ButterFlyEmitTime=15

    self.m_HongBaoTemplate = self.transform:Find("HongBaoItem").gameObject
    self.m_ShengDanTemplate = self.transform:Find("ShengDanItem").gameObject
    self.m_LaternTemplate = self.transform:Find("LaternItem").gameObject
    self.m_QuweiLabaTemplate = self.transform:Find("QuweiLabaItem").gameObject
    self.m_WeddingCandyTemplate = self.transform:Find("WeddingCandy").gameObject
    self.m_YinLiangTemplate = self.transform:Find("YinLiangItem").gameObject

    self.m_ShengDanWaTemplate = self.transform:Find("HongBaoShengDanWa").gameObject
    self.m_ShengDanWaTemplate:SetActive(false)

    --self.m_LiBaoTemplate = ?
    self.m_ButterflyTemplate = {}
    local tf=self.transform:Find("ButterflyNode")
    for i=1,5 do
        self.m_ButterflyTemplate[i] = tf:GetChild(i-1).gameObject
    end

    self.m_EmitIntervalTime = 0.15
    self.m_EmitTime = 10
    self.m_Time = 0
    self.m_TotalTime = 0
    -- self.m_LiBaoTemplateCnt = 0
    self.m_RainType = nil
    self.m_HongBaoPrefab = nil
end

function CLuaHongBaoRain:StartPlay( args)
    local type=args[0] 
    self.m_Time = 0
    self.m_TotalTime = 0
    if type == EnumHongBaoRainType.Default then
        self.m_HongBaoPrefab = self.m_HongBaoTemplate

        if CLuaHongBaoMgr.IsShengDan() then
            self.m_HongBaoPrefab = self.m_ShengDanWaTemplate
        else
            -- 红包图标分等级，红包雨特效时显示最后一种图标
            local texture = CommonDefs.GetComponent_GameObject_Type(self.m_HongBaoPrefab, typeof(CUITexture))
            if texture ~= nil then
                texture:LoadMaterial(CHongBaoMgr.Inst:GetHongBaoIconByAmount(CHongBaoMgr.Inst.m_SystemHongBaoAmount, true,CHongBaoMgr.Inst.m_HongbaoCoverType))
            end
        end
    elseif type == EnumHongBaoRainType.Chirstmas then
        self.m_HongBaoPrefab = self.m_ShengDanTemplate
    elseif type == EnumHongBaoRainType.Latern then
        self.m_HongBaoPrefab = self.m_LaternTemplate
        self.m_EmitIntervalTime = self.LaternEmitIntervalTime
        self.m_EmitTime = self.LaternEmitTime
    elseif type == EnumHongBaoRainType.Laba then
        if CLoudSpeakerNoticeWnd.Inst and CLoudSpeakerNoticeWnd.Inst.msg then
            local i = CLoudSpeakerNoticeWnd.Inst.msg.subType
            local texture = self.m_QuweiLabaTemplate.transform:GetComponent(typeof(CUITexture))
            if i>0 and i<4 then
                texture:LoadMaterial("UI/Texture/Transparent/Material/quweilabawnd_yingnang_0".. i .. ".mat")
            end
        end
        self.m_HongBaoPrefab = self.m_QuweiLabaTemplate
    elseif type == EnumHongBaoRainType.WeddingCandy then
        self.m_EmitIntervalTime = 0.4
        self.m_HongBaoPrefab = self.m_WeddingCandyTemplate
    elseif type == EnumHongBaoRainType.YinLiang then
        self.m_HongBaoPrefab = self.m_YinLiangTemplate
    end

    self.m_HongBaoTemplate:SetActive(false)
    self.m_ShengDanTemplate:SetActive(false)
    self.m_LaternTemplate:SetActive(false)
    self.m_QuweiLabaTemplate:SetActive(false)
    self.m_WeddingCandyTemplate:SetActive(false)
    self.m_YinLiangTemplate:SetActive(false)

    --// 红包图标分等级，红包雨特效时显示最后一种图标
    --CUITexture texture = m_HongBaoPrefab.GetComponent<CUITexture>();
    --if (texture != null)
    --{
    --    if (type == EnumHongBaoRainType.Chirstmas)
    --        texture.LoadMaterial(ShengDan_Setting.GetData().AwardRainIcon);
    --    else
    --        texture.LoadMaterial(CHongBaoMgr.Inst.GetHongBaoIconByAmount(CHongBaoMgr.Inst.m_SystemHongBaoAmount, true));
    --}

    self.m_RainType = type
    if type == EnumHongBaoRainType.Gift then
        --m_LiBaoTemplateCnt = m_LiBaoTemplate.Length;
        -- self.m_LiBaoTemplateCnt = self.m_ButterflyTemplate.Length

        self.m_EmitIntervalTime = self.ButterFlyEmitIntervalTime
        self.m_EmitTime = self.ButterFlyEmitTime
    end
end

function CLuaHongBaoRain:Update()
    if self.m_TotalTime > self.m_EmitTime then
        return
    end
    self.m_TotalTime = self.m_TotalTime+ Time.deltaTime
    self.m_Time = self.m_Time+Time.deltaTime

    if self.m_Time > self.m_EmitIntervalTime then
        self.m_Time = 0
        self:EmitHongBao()
    end
end

function CLuaHongBaoRain:EmitHongBao( )
    local pos = Vector3.zero
    if self.m_RainType == EnumHongBaoRainType.Gift then
        pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(math.floor(Screen.width / 2), UnityEngine_Random(0, Screen.height), 0))
        self.m_HongBaoPrefab = self.m_ButterflyTemplate[math.random( #self.m_ButterflyTemplate )]
    elseif self.m_RainType == EnumHongBaoRainType.Latern then
        pos = CommonDefs.op_Subtraction_Vector3_Vector3(CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(UnityEngine_Random(0, Screen.width), 0, 0)), Vector3(0, 0.2, 0))
    else
        pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(UnityEngine_Random(0, Screen.width), Screen.height, 0))
    end

    local hongBao = GameObject.Instantiate(self.m_HongBaoPrefab, pos, self.gameObject.transform.rotation)
    if hongBao == nil then
        return
    end
    hongBao.transform.parent = self.transform
    hongBao.transform.localScale = Vector3.one
    hongBao:SetActive(true)

    -- 婚礼糖果 点击消失
    if self.m_RainType == EnumHongBaoRainType.WeddingCandy then
        UIEventListener.Get(hongBao).onClick = DelegateFactory.VoidDelegate(function (go)
            LuaWeddingIterationMgr:SnatchWeddingCandy()
            GameObject.Destroy(hongBao)
        end)
    end
end
