local LuaGameObject=import "LuaGameObject"
require("design/Equipment/ShenBing")

local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CEquipItem = import "L10.UI.CEquipItem"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local UITabBar = import "L10.UI.UITabBar"
local NGUITools = import "NGUITools"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EquipmentTemplate_SubType = import "L10.Game.EquipmentTemplate_SubType"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local Object = import "System.Object"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local UIEventListener = import "UIEventListener"
local DelegateFactory = import "DelegateFactory"
local Screen = import "UnityEngine.Screen"
local Extensions = import "Extensions"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local PlayerShowDifProType = import "L10.Game.PlayerShowDifProType"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local Color = import "UnityEngine.Color"
local Time = import "UnityEngine.Time"
local CUIFx = import "L10.UI.CUIFx"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
local QnCheckBox = import "L10.UI.QnCheckBox"
local MoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CFreightEquipSubmitWnd = import "L10.UI.CFreightEquipSubmitWnd"
local UIGrid = import "UIGrid"
local ShenBingColorChange = import "L10.Game.ShenBingColorChange"
local IAvatarColorChange = import "L10.Engine.IAvatarColorChange"

CLuaShenbingPeiyangWnd = class()
CLuaShenbingPeiyangWnd.PlayerPrefKey = "ShenBingRecastCostLingYu"
RegistClassMember(CLuaShenbingPeiyangWnd, "m_TableView")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipList")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_PeiyangRootObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ChongzhuRootObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_NameLabel")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipGradeLabel")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentEquipTemplateId")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ScoreLabel")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_OperateBtnLabel")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_OperateBtnObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_TipBtnObj")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_CostGrid")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipRootObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ItemRootObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ItemRootObj2")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_ItemTemplateId")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ItemTemplateId2")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_RecastItemTemplateId")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_QianyuanRootObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_PeiyangLevelLabel")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_PeiyangLevelSlider")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_BasicAttrTable")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_BasicAttrRootTrans")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_BasicAttrTemplate")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipTexture")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipAddObj")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentNeedJqzCount") --培养需要的九曲珠数量
RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentNeedDgzCount") --培养需要的洞光珠数量
RegistClassMember(CLuaShenbingPeiyangWnd, "m_RecastNeedJqzCount") --重铸需要的九曲珠数量

RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentRowIndex") --当前页签

RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentNeedQianyuanCount") --培养或者重铸需要的前缘数量

RegistClassMember(CLuaShenbingPeiyangWnd, "m_BreakLevelTable")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_MaxTrainLevel")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentTabIndex")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentRowIndex")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_FullLevelTipObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_CostRootTrans")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_WordsTable")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_WordsTemplate")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_QianyuanTemplateIdTable")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_QianyuanCountLabelTable")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_QianyuanAddObj")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipId")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipPlace")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_EquipPos")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_BreakEquipId")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_BreakEquipPlace")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_BreakEquipPos")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_NeedBreak")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ViewBtnObj")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_ModelTexture")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ModelUpdateFx")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_WordUpdateFx")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_BreakFx")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentRotation")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ModelCameraName")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ImageTexture")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_ShowArrow")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentEquipTrainLevel")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_CurrentEquipBreakLevel")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_Light")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_NextLevelTipLabel")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_ChangeAppearanceLevelTable")

RegistClassMember(CLuaShenbingPeiyangWnd, "m_CanFloat")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ModelMoveTime")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_ModelInitPosition")

-- HD Texture
RegistClassMember(CLuaShenbingPeiyangWnd, "m_HDModelTexture")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_HDRootObj")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_HDEquipNameLabel")

-- Train Fx
RegistClassMember(CLuaShenbingPeiyangWnd, "m_FxPath")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_Fx")

-- Store it to destroy fx
RegistClassMember(CLuaShenbingPeiyangWnd, "m_NewRenderObject")

-- Recast with Lingyu
RegistClassMember(CLuaShenbingPeiyangWnd, "m_RecastLingyuMoneyCtrl")
RegistClassMember(CLuaShenbingPeiyangWnd, "m_RecastSettingCheckbox")

function CLuaShenbingPeiyangWnd:ParseDesignData()
	self.m_BreakLevelTable = {}
	for breakLv, trainLv in string.gmatch(ShenBing_Setting.GetData().BreakToMaxGrowLevel, "(%d+),(%d+);?") do
		self.m_BreakLevelTable[tonumber(breakLv)] = tonumber(trainLv)
		self.m_MaxTrainLevel = tonumber(trainLv)
	end
	self.m_QianyuanTemplateIdTable = {}
	for i = 1, 3 do
		local templateId = 0
		if i == 1 then templateId = ShenBing_Setting.GetData().Qy1ItemId
		elseif i == 2 then templateId = ShenBing_Setting.GetData().Qy2ItemId
		else templateId = ShenBing_Setting.GetData().Qy3ItemId end
		self.m_QianyuanTemplateIdTable[i] = templateId
	end
	self.m_ChangeAppearanceLevelTable = {}
	for level in string.gmatch(ShenBing_Setting.GetData().ChangeAppearLevel, "(%d+);?") do
		table.insert(self.m_ChangeAppearanceLevelTable, tonumber(level))
	end
end

function CLuaShenbingPeiyangWnd:Operate()
	if self.m_CurrentTabIndex == 0 then
		if self.m_NeedBreak then
			if not self.m_BreakEquipId then
				g_MessageMgr:ShowMessage("ShenBing_Break_Need_Equip", {})
				return
			end
			Gac2Gas.RequestBreakShenBingTrainLevel(self.m_EquipPlace, self.m_EquipPos, self.m_EquipId, self.m_BreakEquipPlace, self.m_BreakEquipPos, self.m_BreakEquipId)
		else
			if self.m_CurrentEquipTrainLevel >= ShenBing_Setting.GetData().MaxGrowLevel then
				g_MessageMgr:ShowMessage("ShenBing_Reach_Max_Grow_Level")
				return
			end
			local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
			for k, v in pairs(LuaShenbingMgr.ShenbingQianyuanItemTable) do
				if v > 0 then
					CommonDefs.DictAdd_LuaCall(dict, tostring(self.m_QianyuanTemplateIdTable[k]), v)
				end
			end

			local cando = true
			if dict.Count <= 0 then
				cando = false
			else
				local jqzcnt = CItemMgr.Inst:GetItemCount(self.m_ItemTemplateId)
				if jqzcnt < self.m_CurrentNeedJqzCount then 
					cando = false
				else
					if self.m_CurrentNeedDgzCount >0 then
						local dgzcount = CItemMgr.Inst:GetItemCount(self.m_ItemTemplateId2)
						if dgzcount < self.m_CurrentNeedDgzCount then 
							cando = false
						end
					end
				end
			end

			if not cando then
				g_MessageMgr:ShowMessage("ShenBing_Train_Need_Material", {})
			else
				Gac2Gas.RequestTrainShenBing(self.m_EquipPlace, self.m_EquipPos, self.m_EquipId, MsgPackImpl.pack(dict))
			end
		end
	elseif self.m_CurrentTabIndex == 1 then
		self:RecastEquip()
	end
end

function CLuaShenbingPeiyangWnd:OpenSelectEquipWnd()
	if not CClientMainPlayer.Inst then
			return
	end
	local currentEquip = CItemMgr.Inst:GetById(self.m_EquipId)
	local currentEquipName = currentEquip.Equip.Name
	local oriIsFake = currentEquip.Equip.IsFake
	local oriEquipName = currentEquipName
	local currentEquipTemplateId = currentEquip.Equip.TemplateId
	local otherTemplateId = 0
	if (currentEquip.Equip.HolyBreakLevel == 0 or currentEquip.Equip.HolyBreakLevel == 1) and ShenBing_Tupo1st150.GetData(currentEquipTemplateId) then
		otherTemplateId = tonumber(ShenBing_Tupo1st150.GetData(currentEquipTemplateId).ReplaceEquipmentId)
	end
	if otherTemplateId > 0 then
		local data = EquipmentTemplate_Equip.GetData(otherTemplateId)
		if data then
			currentEquipName = data.Name
		end
	end

	local dict = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, cs_string)))
	local list = CreateFromClass(MakeGenericClass(List, cs_string))
	-- find
	local placeTable = {EnumItemPlace.Bag}
	for i = 1, #placeTable do
		local size = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(placeTable[i])
		if size > 0 then
			for j = 1, size do
				local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(placeTable[i], j)
				local equip = CItemMgr.Inst:GetById(id)
				--if equip ~= nil and equip.IsEquip and not equip.Equip.IsShenBing then
				if equip ~= nil and equip.IsEquip and not equip.Equip.IsShenBing then
					if (equip.Equip.IsFake == oriIsFake and equip.TemplateId == currentEquipTemplateId) or equip.TemplateId == otherTemplateId then
						CommonDefs.ListAdd(list, typeof(cs_string), id)
					end
				end
			end
		end
	end
	CommonDefs.DictAdd(dict, TypeOfInt32, 0, typeof(MakeGenericClass(List, cs_string)), list)
	if list.Count > 0 then
		CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("ShenbingPeiyang", dict, currentEquipName, 0, false, LocalString.GetString("选择"), LocalString.GetString("选择"), DelegateFactory.Action(function (...)
			local wndGameObject = CommonDefs.DictGetValue_LuaCall(CUIManager.instance.loadedUIs, CUIResources.FreightEquipSubmitWnd)
			if wndGameObject then
				local wndScript = wndGameObject:GetComponent(typeof(CFreightEquipSubmitWnd))
				if wndScript then
					self:AddEquipInternal(wndScript.curSelectId)
				end
			end
			CUIManager.CloseUI(CUIResources.FreightEquipSubmitWnd)
		end), LocalString.GetString("包裹中没有该装备"))
	else
		CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("ShenbingPeiyang", dict, currentEquipName, 0, false, LocalString.GetString("选择"), LocalString.GetString("前往购买"), DelegateFactory.Action(function ( ... )
				CYuanbaoMarketMgr.ShowPlayerShopBuySection(1, 1)
			end), LocalString.GetString("包裹中没有该装备"))
	end
end

function CLuaShenbingPeiyangWnd:DragModel(delta)
	local deltaVal = -delta.x / Screen.width * 360
	self.m_CurrentRotation = self.m_CurrentRotation + deltaVal
	CUIManager.SetModelRotation(self.m_ModelCameraName, self.m_CurrentRotation)
	self.m_CanFloat = false
end

function CLuaShenbingPeiyangWnd:Init()
	self:ParseDesignData()
	self.m_ModelCameraName = "__ShenBingPeiYangWnd__"
	self.m_CurrentRotation = 0
	self.m_ShowArrow = false
	self.m_Light = nil
	self.m_CanFloat = false

	self.m_TipBtnObj = LuaGameObject.GetChildNoGC(self.transform, "TipBtn").gameObject
	UIEventListener.Get(self.m_TipBtnObj).onClick = DelegateFactory.VoidDelegate(function(go)
		if self.m_CurrentTabIndex == 0 then
			g_MessageMgr:ShowMessage("ShenBing_PeiYang_Tip")
		else
			g_MessageMgr:ShowMessage("ShenBing_ChongZhu_Tip")
		end
	end)
	self.m_OperateBtnObj = LuaGameObject.GetChildNoGC(self.transform, "DevelopBtn").gameObject
	UIEventListener.Get(self.m_OperateBtnObj).onClick = DelegateFactory.VoidDelegate(function(go)
		self:Operate()
	end)

	self.m_OperateBtnLabel = LuaGameObject.GetChildNoGC(self.m_OperateBtnObj.transform, "Label").label


	-- right
	self.m_PeiyangRootObj = LuaGameObject.GetChildNoGC(self.transform, "PeiyangRoot").gameObject
	self.m_ChongzhuRootObj = LuaGameObject.GetChildNoGC(self.transform, "ChongZhuRoot").gameObject
	self.m_WordUpdateFx = LuaGameObject.GetChildNoGC(self.m_ChongzhuRootObj.transform, "WordUpdateFx").uiFx
	self.m_BreakFx = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "BreakFx").uiFx
	self.m_PeiyangLevelLabel = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "GradeLabel").label
	self.m_PeiyangLevelSlider = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "GradeSlider").slider
	self.m_BasicAttrTable = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "WordTable").table
	self.m_BasicAttrRootTrans = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "Basic").transform
	self.m_NextLevelTipLabel = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "NextLevelTip").label
	self.m_BasicAttrTemplate = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "WordTemplate").gameObject
	self.m_BasicAttrTemplate:SetActive(false)
	self.m_FullLevelTipObj = LuaGameObject.GetChildNoGC(self.transform, "FullLevelTip").gameObject
	self.m_QianyuanCountLabelTable = {}
	for i = 1, 3 do
		local itemData = Item_Item.GetData(self.m_QianyuanTemplateIdTable[i])
		if itemData then
			local nameLabel = LuaGameObject.GetChildNoGC(self.m_PeiyangRootObj.transform, "QianyuanItem"..i).label
			nameLabel.text = itemData.Name
			self.m_QianyuanCountLabelTable[i] = LuaGameObject.GetChildNoGC(nameLabel.transform, "Label").label
		end
	end

	self.m_WordsTable = LuaGameObject.GetChildNoGC(self.m_ChongzhuRootObj.transform, "Table").table
	self.m_WordsTemplate = LuaGameObject.GetChildNoGC(self.m_ChongzhuRootObj.transform, "WordTemplate").gameObject
	self.m_WordsTemplate:SetActive(false)


	local showRootTrans = LuaGameObject.GetChildNoGC(self.transform, "ShowRoot").transform
	local name = LuaGameObject.GetChildNoGC(showRootTrans, "Name")
	self.m_NameLabel = name.label
	self.m_EquipGradeLabel = LuaGameObject.GetChildNoGC(name.transform, "Label").label

	self.m_ModelTexture = LuaGameObject.GetChildNoGC(showRootTrans, "ModelTexture").texture
	self.m_ModelUpdateFx = LuaGameObject.GetChildNoGC(self.m_ModelTexture.transform, "ModelUpdateFx").uiFx
	self.m_ImageTexture = LuaGameObject.GetChildNoGC(showRootTrans, "ImageTexture").cTexture
	UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:DragModel(delta)
	end)
	self.m_ViewBtnObj = LuaGameObject.GetChildNoGC(showRootTrans, "ViewBtn").gameObject
	UIEventListener.Get(self.m_ViewBtnObj).onClick = DelegateFactory.VoidDelegate(function(go)
		self.m_HDRootObj:SetActive(true)
	end)
	self.m_ScoreLabel = LuaGameObject.GetChildNoGC(showRootTrans, "SocreLabel").label

	self:InitCost()

	-- init HD Texture
	self.m_HDRootObj = LuaGameObject.GetChildNoGC(showRootTrans, "HDTexture").gameObject
	local g = LuaGameObject.GetChildNoGC(self.m_HDRootObj.transform, "CloseButton").gameObject
	UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
		self.m_HDRootObj:SetActive(false)
	end)

	self.m_HDModelTexture = LuaGameObject.GetChildNoGC(self.m_HDRootObj.transform, "ModelTexture").texture
	UIEventListener.Get(self.m_HDModelTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:DragModel(delta)
	end)
	self.m_HDEquipNameLabel = LuaGameObject.GetChildNoGC(self.m_HDRootObj.transform, "EquipName").label
	self.m_HDRootObj:SetActive(false)

	self:InitTabs()
	self:InitEquipList()

	-- init train fx
	self:InitTrainFx()

	self:InitRecastLingyuCost()
end

function CLuaShenbingPeiyangWnd:InitCost()
	self.m_ItemTemplateId = ShenBing_Setting.GetData().JqzItemId
	self.m_ItemTemplateId2 = ShenBing_Setting.GetData().DgzItemId
	self.m_RecastItemTemplateId = ShenBing_Setting.GetData().RecastItemId
	self.m_CostRootTrans = LuaGameObject.GetChildNoGC(self.transform, "Cost").transform
	self.m_CostGrid = self.m_CostRootTrans.gameObject:GetComponent(typeof(UIGrid))

	-- item
	self.m_ItemRootObj = FindChild(self.m_CostRootTrans, "Item").gameObject

	--item2
	self.m_ItemRootObj2 = FindChild(self.m_CostRootTrans, "Item2").gameObject

	--self:RefreshItem()

	-- equip
	local addEquipAction = function ( ... )
		self:OpenSelectEquipWnd()
	end

	local equipTrans = LuaGameObject.GetChildNoGC(self.m_CostRootTrans, "Equip").transform
	local equipTex = LuaGameObject.GetChildNoGC(equipTrans, "Texture")
	UIEventListener.Get(equipTex.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		addEquipAction(go)
	end)
	self.m_EquipTexture = equipTex.cTexture
	self.m_EquipAddObj = LuaGameObject.GetChildNoGC(equipTrans, "Add").gameObject
	UIEventListener.Get(self.m_EquipAddObj).onClick = DelegateFactory.VoidDelegate(function(go)
		addEquipAction(go)
	end)
	self.m_EquipRootObj = equipTrans.gameObject

	-- qianyuan
	self.m_QianyuanRootObj = LuaGameObject.GetChildNoGC(self.m_CostRootTrans, "Qianyuan").gameObject
	local g = LuaGameObject.GetChildNoGC(self.m_QianyuanRootObj.transform, "Add").gameObject
	UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
		LuaShenbingMgr.ShenbingQianyuanEquipItemId = self.m_EquipList[self.m_CurrentRowIndex + 1].itemId
		CUIManager.ShowUI(CLuaUIResources.ShenBingSubmitWnd)
	end)
	self.m_QianyuanAddObj = LuaGameObject.GetChildNoGC(self.m_QianyuanRootObj.transform, "Add").gameObject
end

function CLuaShenbingPeiyangWnd:SetCostItem(ctrlgo, itemtid)
    if itemtid <= 0 then
        return
    end

    local itemdata = Item_Item.GetData(itemtid)
    if not itemdata then
        return
    end

	local ctrltrans = ctrlgo.transform
    local label = FindChildWithType(ctrltrans, "Name", typeof(UILabel))
    label.text = itemdata.Name

    local tex = FindChildWithType(ctrltrans, "Texture", typeof(CUITexture))
    tex:LoadMaterial(itemdata.Icon)

    local showItemInfoAction = function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemtid, false, nil, AlignType2.Default, 0, 0, 0, 0)
    end
    UIEventListener.Get(tex.gameObject).onClick = DelegateFactory.VoidDelegate(showItemInfoAction)
end

function CLuaShenbingPeiyangWnd:SetCostItemCount(ctrlgo, itemtid, needct)
	if not needct then needct = 0 end
    local ctrltrans = ctrlgo.transform

    local ctlabel = FindChildWithType(ctrltrans, "Label", typeof(UILabel))
    local ct = CItemMgr.Inst:GetItemCount(itemtid)
    ctlabel.text = ct .. "/" .. needct

    local mask = FindChildWithType(ctrltrans, "Got", typeof(GameObject))
    mask:SetActive(ct < needct)
    if ct < needct then
        local gotItemAction = function(go)
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemtid, false, go.transform, AlignType.Right)
        end
        UIEventListener.Get(mask.gameObject).onClick = DelegateFactory.VoidDelegate(gotItemAction)
    else
        UIEventListener.Get(mask.gameObject).onClick = nil
    end
end

function CLuaShenbingPeiyangWnd:InitRecastLingyuCost()
	self.m_RecastLingyuMoneyCtrl = self.m_ChongzhuRootObj.transform:Find("ChongZhuCost"):GetComponent(typeof(MoneyCtrl))
	self.m_RecastLingyuMoneyCtrl:SetType(4, 0, false)
	self.m_RecastSettingCheckbox = self.m_ChongzhuRootObj.transform:Find("ChongZhuSetting"):GetComponent(typeof(QnCheckBox))
	self.m_RecastSettingCheckbox.OnValueChanged = DelegateFactory.Action_bool(function(v)
		local cnt = CItemMgr.Inst:GetItemCount(self.m_CurrentTabIndex == 0 and self.m_ItemTemplateId or self.m_RecastItemTemplateId)
        local bShowItem = cnt >= self.m_RecastNeedJqzCount or (not v)
		self.m_CostRootTrans.gameObject:SetActive(bShowItem)
		self.m_RecastLingyuMoneyCtrl.gameObject:SetActive(not bShowItem)
		PlayerPrefs.SetInt(CLuaShenbingPeiyangWnd.PlayerPrefKey, v and 1 or 0)
	end)
end

function CLuaShenbingPeiyangWnd:InitTrainFx( ... )
	local beginTrans = self.transform:Find("PeiyangFx/BeginPoint")
	local endTrans = self.transform:Find("PeiyangFx/EndPoint")
	self.m_Fx = self.transform:Find("PeiyangFx/Fx"):GetComponent(typeof(CUIFx))
	self.m_Fx.OnLoadFxFinish = DelegateFactory.Action(function ( ... )

	end)
	local path = {beginTrans.localPosition, endTrans.localPosition}
	self.m_FxPath = Table2Array(path, MakeArrayClass(Vector3))
end

function CLuaShenbingPeiyangWnd:ShowTrainFx( ... )
	self.m_Fx.transform.localPosition = self.m_FxPath[0]
	self.m_Fx:DestroyFx()
	self.m_Fx:LoadFx("Fx/UI/Prefab/UI_kuang_blue02.prefab")
	LuaTweenUtils.DOLocalPathOnce(self.m_Fx.transform, self.m_FxPath , 0.5, Ease.Linear)
	self.m_BreakFx:DestroyFx()
	self.m_BreakFx:LoadFx("Fx/UI/Prefab/UI_huanranyixin_cizhui.prefab")
end

function CLuaShenbingPeiyangWnd:InitTabs()
	local tabBar = CommonDefs.GetComponent_GameObject_Type(self.gameObject, typeof(UITabBar))
	if tabBar ~= nil then
		tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
			self.m_CurrentTabIndex = index
			self.m_PeiyangRootObj:SetActive(index == 0)
			self.m_ChongzhuRootObj:SetActive(index == 1)
			self:InitDifferentArea()
			-- Clear the fx
			self.m_WordUpdateFx:DestroyFx()
			self.m_BreakFx:DestroyFx()
		end)
		tabBar:ChangeTab(0)
	end
end

function CLuaShenbingPeiyangWnd:RecastEquip()
	if not self.m_CurrentRowIndex then
		return
	end
	local cnt = CItemMgr.Inst:GetItemCount(self.m_RecastItemTemplateId)
	local bCostLingyu = self.m_RecastSettingCheckbox.Selected
	if cnt < self.m_RecastNeedJqzCount and (not bCostLingyu) then
		g_MessageMgr:ShowMessage("ShenBing_Recast_No_Item")
		return
	end
	Gac2Gas.RequestRecastShenBing(self.m_EquipPlace, self.m_EquipPos, self.m_EquipId, bCostLingyu)
end

function CLuaShenbingPeiyangWnd:InitEquipList()
	self.m_EquipList = {}
	local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
	for i = 1, #placeTable do
		local equipList = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(placeTable[i])
		for i = 0, equipList.Count - 1 do
			local equip = CItemMgr.Inst:GetById(equipList[i].itemId)
			if equip ~= nil and equip.IsEquip and equip.Equip.HolyTrainLevel > 0 then
				table.insert(self.m_EquipList, equipList[i])
			end
		end
	end

	if #self.m_EquipList <= 0 then
		g_MessageMgr:ShowMessage("ShenBing_PeiYang_No_Equip")
		CUIManager.CloseUI(CLuaUIResources.ShenBingPeiYangWnd)
		return
	end

	self.m_TableView = LuaGameObject.GetChildNoGC(self.transform, "MasterView").tableView
	local initItem = function (item, index)
        if item ~= nil then
        	item:Init(self.m_EquipList[index + 1])
        end
    end

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(#self.m_EquipList, initItem)
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.m_TableView:ReloadData(true, false)
    self.m_TableView:SetSelectRow(0, true)
end

function CLuaShenbingPeiyangWnd:InitItem(item, index)
	local equipItem = CommonDefs.GetComponent_GameObject_Type(item.gameObject, typeof(CEquipItem))
	if equipItem ~= nil then
		equipItem:Init(self.m_EquipList[index + 1])
	end
end

function CLuaShenbingPeiyangWnd:GetTypeStr(item)
	local equipment = EquipmentTemplate_Equip.GetData(item.TemplateId)
	local subTypeTemplate = EquipmentTemplate_SubType.GetData(equipment.Type * 100 + equipment.SubType)
    if subTypeTemplate ~= nil then
        if equipment.Type == EnumBodyPosition.Weapon then
            local default
            if equipment.BothHand == 1 then
                default = LocalString.GetString("双手")
            else
                default = LocalString.GetString("单手")
            end
            return System.String.Format("{0} ({1})", subTypeTemplate.Name, default)
        else
            return subTypeTemplate.Name
        end
    end
    return ""
end

function CLuaShenbingPeiyangWnd:GetGrowTips(equipType, trainLevel)
	if not ShenBing_GrowTips.GetData(trainLevel) then return "" end
	local tips = ShenBing_GrowTips.GetData(trainLevel).TipsForNextOther
	if equipType == 1 then
		tips = ShenBing_GrowTips.GetData(trainLevel).TipsForNext
	elseif equipType == 3 or equipType == 4 then
		tips = ShenBing_GrowTips.GetData(trainLevel).TipsForNextClothing
	end
	return tips
end


function CLuaShenbingPeiyangWnd:ShowPeiyangTab(equip, equipData)
    local trainLevel = self.m_CurrentEquipTrainLevel
    local breakLevel = self.m_CurrentEquipBreakLevel
    

    self.m_PeiyangLevelLabel.text = LocalString.GetString("等级 ") .. trainLevel .. "/" .. self.m_BreakLevelTable[breakLevel]
    self.m_PeiyangLevelSlider.value = trainLevel / self.m_BreakLevelTable[breakLevel]
    self:InitBasicAttr(equip)
    self.m_ShowArrow = false
    self.m_OperateBtnLabel.text = LocalString.GetString("培养")

    local isFullLevel = trainLevel >= self.m_MaxTrainLevel
    if trainLevel >= ShenBing_Setting.GetData().MaxGrowLevel then
        isFullLevel = true
        local label = self.m_FullLevelTipObj:GetComponent(typeof(UILabel))
        label.text = LocalString.GetString("已培养至当前开放上限")
        label.width = 400
    end
    self.m_FullLevelTipObj:SetActive(isFullLevel)
    self.m_CostRootTrans.gameObject:SetActive(not isFullLevel)
    self.m_OperateBtnObj:SetActive(not isFullLevel)
    self.m_TipBtnObj:SetActive(not isFullLevel)
    Extensions.SetLocalPositionY(self.m_BasicAttrRootTrans, isFullLevel and -167 or -231)
    self.m_NextLevelTipLabel.gameObject:SetActive(not isFullLevel)
    if not isFullLevel then
        self.m_NextLevelTipLabel.text = LocalString.GetString("培养至下一级：[808080]") .. self:GetGrowTips(equipData.Type, trainLevel)
    end

    for i = 1, 3 do
        local cnt = 0
        local qyt = self.m_QianyuanTemplateIdTable[i]
        if CommonDefs.DictContains_LuaCall(equip.Equip.ConsumeItemCount, qyt) then
            cnt = CommonDefs.DictGetValue_LuaCall(equip.Equip.ConsumeItemCount, qyt)
        end
        self.m_QianyuanCountLabelTable[i].text = cnt
    end
    if isFullLevel then
        return
    end
    if not isFullLevel then
        local needBreak = trainLevel == self.m_BreakLevelTable[breakLevel]
        self.m_NeedBreak = needBreak
        self.m_EquipRootObj:SetActive(needBreak)
        self.m_ItemRootObj:SetActive(not needBreak)
        
        self.m_QianyuanRootObj:SetActive(not needBreak)
        if not needBreak then
			self:SetCostItem(self.m_ItemRootObj, self.m_ItemTemplateId)
			self:SetCostItem(self.m_ItemRootObj2, self.m_ItemTemplateId2)
            for i = 1, ShenBing_Grow.GetDataCount() do
                local data = ShenBing_Grow.GetData(i)
                if data.GrowLevel == trainLevel + 1 and equipData.Grade >= data.MinOriEquipLevel and equipData.Grade <= ShenBing_Grow.GetData(i).MaxOriEquipLevel then
                    self.m_CurrentNeedJqzCount = data.JqzCount
                    self.m_CurrentNeedDgzCount = data.DgzCount
                    self.m_CurrentNeedQianyuanCount = data.QyTotalCount
                    LuaShenbingMgr.ShenbingQianyuanItemTotalCnt = data.QyTotalCount
                    break
                end
            end
			self.m_ItemRootObj2:SetActive(not needBreak and self.m_CurrentNeedDgzCount > 0)
            self:RefreshItem()
            self.m_QianyuanAddObj:SetActive(#LuaShenbingMgr.ShenbingQianyuanItemTable <= 0)
        else
			self.m_ItemRootObj2:SetActive(false)
            self.m_OperateBtnLabel.text = LocalString.GetString("突破")
            self.m_EquipAddObj:SetActive(self.m_BreakEquipId == nil)
        end
    end
end

function CLuaShenbingPeiyangWnd:InitDifferentArea()
	if not self.m_CurrentRowIndex then
		return
	end
	local itemId = self.m_EquipList[self.m_CurrentRowIndex + 1].itemId
	local equip = CItemMgr.Inst:GetById(itemId)
	if not equip then
		return
	end
	local equipData = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	if not equipData then
		return
	end
	self.m_CurrentEquipTemplateId = equip.TemplateId

	self.m_ScoreLabel.text = equip.Equip.Score

	local trainLevel = equip.Equip.HolyTrainLevel
	local breakLevel = equip.Equip.HolyBreakLevel

	self.m_CurrentEquipTrainLevel = trainLevel
	self.m_CurrentEquipBreakLevel = breakLevel

	if self.m_CurrentTabIndex == 0 then
		self:ShowPeiyangTab(equip,equipData)
	elseif self.m_CurrentTabIndex == 1 then
		self:SetCostItem(self.m_ItemRootObj,self.m_RecastItemTemplateId)
		self.m_OperateBtnLabel.text = LocalString.GetString("重铸")
		self.m_CostRootTrans.gameObject:SetActive(true)
		self.m_ItemRootObj:SetActive(true)
		self.m_ItemRootObj2:SetActive(false)
		self.m_EquipRootObj:SetActive(false)
		self.m_QianyuanRootObj:SetActive(false)
		self.m_FullLevelTipObj:SetActive(false)
		self.m_OperateBtnObj:SetActive(true)
		self.m_TipBtnObj:SetActive(true)
		for i = 1, ShenBing_Recast.GetDataCount() do
			if equipData.Grade >= ShenBing_Recast.GetData(i).MinOriEquipLevel and equipData.Grade <= ShenBing_Recast.GetData(i).MaxOriEquipLevel then
				self.m_RecastNeedJqzCount = ShenBing_Recast.GetData(i).Count
				break
			end
		end

		self.m_RecastLingyuMoneyCtrl:SetCost(Mall_LingYuMall.GetData(self.m_RecastItemTemplateId).Jade * self.m_RecastNeedJqzCount)
		self.m_RecastSettingCheckbox:SetSelected(PlayerPrefs.GetInt(CLuaShenbingPeiyangWnd.PlayerPrefKey, 0) > 0, false)
		self:RefreshItem()
		local holyTable = Array2Table(equip.Equip.HolyWordContents)
		
		local isMax = CLuaEquipMgr:EquipWordIdIsMaxWord(itemId,holyTable,true)

		Extensions.RemoveAllChildren(self.m_WordsTable.transform)
        for i = 0, equip.Equip.HolyWordContents.Length - 1 do
        	if equip.Equip.HolyWordContents[i] > 0 then
                local desc = self:GetWordDescription(equip.Equip.HolyWordContents[i],isMax[i+1])
                if not System.String.IsNullOrEmpty(desc) then
                	local obj = NGUITools.AddChild(self.m_WordsTable.gameObject, self.m_WordsTemplate)
                	obj:SetActive(true)
                	local label = LuaGameObject.GetChildNoGC(obj.transform, "Label").label
 			        label.text = desc
 			        label.color = equip.Equip.DisplayColor
 			        LuaGameObject.GetChildNoGC(obj.transform, "Sprite").sprite:ResetAndUpdateAnchors()
                end
            end
        end
        self.m_WordsTable:Reposition()
	end
	self.m_CostGrid:Reposition()
end
function CLuaShenbingPeiyangWnd:GetWordDescription(id,isMax)
	local data = Word_Word.GetData(id)
	if not data then return "" end
	return SafeStringFormat3(LocalString.GetString("【%s】%s%s"),data.Name,isMax and CLuaEquipMgr.WordMaxMark or "",data.Description)
end

function CLuaShenbingPeiyangWnd:Clear()
	self.m_BreakEquipId = nil
	self.m_EquipTexture:Clear()
end

function CLuaShenbingPeiyangWnd:OnSelectAtRow(row)
	if self.m_CurrentRowIndex ~= row then
		self:Clear()
	end
	local equip = CItemMgr.Inst:GetById(self.m_EquipList[row + 1].itemId)
	if nil == equip then
		return
	end
	LuaShenbingMgr.ShenbingQianyuanItemTable = {}
	self.m_CurrentRowIndex = row

	local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
	for i = 1, #placeTable do
		local pos = CItemMgr.Inst:FindItemPosition(placeTable[i], equip.Id)
		if pos > 0 then
			--Gac2Gas.RequestRecastShenBing(placeTable[i], pos, self.m_EquipList[self.m_CurrentRowIndex + 1].itemId)
			self.m_EquipPlace = placeTable[i]
			self.m_EquipId = equip.Id
			self.m_EquipPos = pos
			break
		end
	end

	local trainLevel = equip.Equip.HolyTrainLevel
	local breakLevel = equip.Equip.HolyBreakLevel
	self.m_EquipRootObj:SetActive(false)
	self.m_ItemRootObj:SetActive(false)
	self.m_ItemRootObj2:SetActive(false)
	self.m_QianyuanRootObj:SetActive(false)
	self.m_FullLevelTipObj:SetActive(false)
	self:InitDifferentArea()
	self:InitAppearance()
end

function CLuaShenbingPeiyangWnd:InitAppearance()
	local equip = CItemMgr.Inst:GetById(self.m_EquipList[self.m_CurrentRowIndex + 1].itemId)
	if not equip then
		return
	end
	self.m_NameLabel.text = equip.Equip.DisplayName
	self.m_NameLabel.color = equip.Equip.DisplayColor
	self.m_HDEquipNameLabel.text = equip.Equip.DisplayName
	self.m_HDEquipNameLabel.color = equip.Equip.DisplayColor
	self.m_EquipGradeLabel.text = self:GetTypeStr(equip).." ".. SafeStringFormat3(LocalString.GetString("%s级"), tostring(equip.Grade))

	local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	if not data then
		return
	end

	self.m_ViewBtnObj:SetActive(data.Type == 1)
	self.m_ModelTexture.gameObject:SetActive(data.Type == 1)
	self.m_ImageTexture.gameObject:SetActive(data.Type ~= 1)
	if data.Type == 1 then
		if self.m_NewRenderObject then
			self.m_NewRenderObject:Destroy()
		end
		local interface = LuaDefaultModelTextureLoader.Create(function (ro)
			self:LoadModel(ro)
		end)
		self.m_CurrentRotation = 0
		local tx = CUIManager.CreateModelTexture(self.m_ModelCameraName, interface, self.m_CurrentRotation, 0.05, 0, ShenBing_Rotation.GetData(data.ShenBingType).Scale, false, true, 1.0)
		self.m_ModelTexture.mainTexture = tx
		self.m_HDModelTexture.mainTexture = tx

		CUIManager.SetModelRotation(self.m_ModelCameraName, self.m_CurrentRotation)
		self.m_ModelInitPosition = Vector3(0.05, 0, ShenBing_Rotation.GetData(data.ShenBingType).Scale)
		CUIManager.SetModelPosition(self.m_ModelCameraName, self.m_ModelInitPosition)
	else
		self.m_ImageTexture:LoadMaterial(equip.Equip.BigIcon)
		local height = equip.Equip.UseSquareBigIcon and 256 or 384
		local width = equip.Equip.UseSquareBigIcon and 256 or 194
		self.m_ImageTexture.uiTexture.height = height
		self.m_ImageTexture.uiTexture.width = width
	end
end

function CLuaShenbingPeiyangWnd:InitRO(ro, angle, pos, shenbingType)
	-- Skin weapon
	--if shenbingType == 17 or shenbingType == 20 or shenbingType == 21 then
	--	for i = 0, ro.m_Renderers.Count - 1 do
	--		local mats = ro.m_Renderers[i].materials
	--		for j = 0, mats.Length - 1 do
	--			mats[j]:DisableKeyword("SKIN_ON")
	--		end
	--	end
	--end
	if ro.m_Renderers then
		for i = 0, ro.m_Renderers.Count - 1 do
			local mats = ro.m_Renderers[i].materials
			for j = 0, mats.Length - 1 do
				mats[j]:SetFloat("_Hue", 0)
				mats[j]:SetFloat("_Saturation", 0)
				mats[j]:SetFloat("_Brightness", 0)
			end
		end
	end
	ro.transform.localPosition = pos
	ro.transform.localEulerAngles = angle
	self.m_CanFloat = true
	self.m_ModelMoveTime = 0
end

function CLuaShenbingPeiyangWnd:Update()
	if not self.m_CanFloat then
		return
	end
	--self:DragModel(Vector2(10, 0))
	self.m_ModelMoveTime  = self.m_ModelMoveTime + Time.deltaTime
	self.m_ModelInitPosition.y = 0.05 * math.sin(self.m_ModelMoveTime * 1.7)
	CUIManager.SetModelPosition(self.m_ModelCameraName, self.m_ModelInitPosition)
end

function CLuaShenbingPeiyangWnd:InitLight(ro, shenbingType)
	-- if not self.m_Light then
		-- local instanceLight = CUIManager.instance.modelDirectionalLight
		-- if not instanceLight then
			-- instanceLight = CUIManager.instance.transform:Find("ModelDirectionallight")
		-- end
		-- local light = GameObject.Instantiate(instanceLight.gameObject)
		-- light:SetActive(true)
		-- light.transform.parent = ro.transform.parent.parent
		-- light.transform.localPosition = Vector3.zero
		-- light.transform.localEulerAngles = Vector3.zero
		-- light.transform.localScale = Vector3.one
		-- self.m_Light = CommonDefs.GetComponent_GameObject_Type(light, typeof(Light))
	-- end
	-- local a, b, c = string.match(ShenBing_Rotation.GetData(shenbingType).LightDir, "([^;]+);([^;]+);([^;]+)")
	-- self.m_Light.transform.localEulerAngles = Vector3(a, b, c)
	-- self.m_Light.color = NGUIText.ParseColor32(ShenBing_Rotation.GetData(shenbingType).LightColor, 0)
	-- self.m_Light.intensity = ShenBing_Rotation.GetData(shenbingType).LightIntensity

	CUIManager.ModelLightEnabled = false
end

function CLuaShenbingPeiyangWnd:LoadSkinModel(ro, shenbingType, firstId, secondId, oldPrefab, color)
	local resName = shenbingType == 17 and "gnpc_nys" or (shenbingType == 22 and "bnpc_yingling" or "gnpc_huashi")
	local skeletonResPath = SafeStringFormat3("Character/Player_New/%s/Prefab/%s.prefab", resName, resName)
	ro:LoadMain(skeletonResPath, nil, false, false)
	if firstId > 0 then
		local cfgdata = ShenBing_WeaponModel.GetData(firstId)
		local pattern = "Item/Weapon_New/Prefab/%s_new_%s.prefab"
		if shenbingType == 22 then
			-- 笛子未拆分成两部分
			pattern = "Item/Weapon_New/Prefab/%s_nan.prefab"
			if cfgdata then
				ro:AddChild(SafeStringFormat3(pattern, cfgdata.Prefab), "weapon01", "weapon01")
			end
			ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function ( ... )
				local hasColor1, color1, hasColor2, color2 = self:GetCustomColor(color)
				ro:SetColorChanger("weapon01", CreateFromClass(ShenBingColorChange, hasColor1, color1, hasColor2, color2, true, true))
			end))
		else
			if cfgdata and cfgdata.Stage >= 3 then --3阶以上
				pattern = "Item/Weapon_New/Prefab/%s_new.prefab"
				local path = SafeStringFormat3(pattern,cfgdata.Prefab)
				ro:AddChild(path, "weapon01", "weapon01")
				ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function ( ... )
					local hasColor1, color1, hasColor2, color2 = self:GetCustomColor(color)
					local wp1 = ro.m_MainObject
					if wp1 then CClientMainPlayer.SetShenbingCustomColorInternal(wp1, hasColor1, color1, hasColor1, color1) end
				end))
			else
				if cfgdata then
					ro:AddChild(SafeStringFormat3(pattern, cfgdata.Prefab, cfgdata.NewMaterial > 0 and "wp1_01" or "wp1_02"), "weapon01", "weapon01")
				end
				local cfgdata2 = ShenBing_WeaponModel.GetData(secondId)
				if cfgdata2 then
					ro:AddChild(SafeStringFormat3(pattern, cfgdata2.Prefab, cfgdata.NewMaterial > 0 and "wp2_01" or "wp2_02"), "weapon01_01", "weapon01")
				end
				ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function ( ... )
					local hasColor1, color1, hasColor2, color2 = self:GetCustomColor(color)
					ro:SetColorChanger("weapon01", CreateFromClass(ShenBingColorChange, hasColor1, color1, hasColor2, color2, true, false))
					ro:SetColorChanger("weapon01_01", CreateFromClass(ShenBingColorChange, hasColor1, color1, hasColor2, color2, false, false))
				end))
			end
		end
	else
		-- 笛子
		if shenbingType == 22 then
			ro:AddChild(oldPrefab, "weapon01", "weapon01")
		else
			ro:AddChild(string.gsub(oldPrefab, "%.prefab", "_new.prefab"), "weapon01", "weapon01")
		end
	end
	if shenbingType == 17 or shenbingType == 22 then
		ro:DoAni("stand04", true, 0, 1.0, 0, true, 1.0)
	end
end

-- 32 bit two colors
function CLuaShenbingPeiyangWnd:GetCustomColor(color)
	local hasColor1, hasColor2 = false, false
	local color1, color2 = Vector4.one, Vector4.one
	if bit.band(color, 0x8000) > 0 then
		hasColor1 = true
		local c = bit.band(color, 0x7FFF)
		color1 = Vector4(bit.rshift(c, 10) / 31, bit.band(bit.rshift(c, 5), 0x1F) / 31, bit.band(c, 0x1F) / 31, 1)
	end
	if bit.band(bit.rshift(color, 16), 0x8000) > 0 then
		hasColor2 = true
		local c = bit.band(bit.rshift(color, 16), 0x7FFF)
		color2 = Vector4(bit.rshift(c, 10) / 31, bit.band(bit.rshift(c, 5), 0x1F) / 31, bit.band(c, 0x1F) / 31, 1)
	end
	return hasColor1, color1, hasColor2, color2
end

function CLuaShenbingPeiyangWnd:LoadModel(ro)
	self.m_CanFloat = false

	local equip = CItemMgr.Inst:GetById(self.m_EquipList[self.m_CurrentRowIndex + 1].itemId)
	local color = equip.Equip.ShenBingColor
	local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	if not data then
		return
	end

	self:InitLight(ro, data.ShenBingType)

	local a, b, c = string.match(ShenBing_Rotation.GetData(data.ShenBingType).OriAngle, "([^;]+);([^;]+);([^;]+)")
	local angle = Vector3(tonumber(a), tonumber(b), tonumber(c))
	local e, f, g = string.match(ShenBing_Rotation.GetData(data.ShenBingType).OriPos, "([^;]+);([^;]+);([^;]+)")
	local pos = Vector3(tonumber(e), tonumber(f), tonumber(g))

	local newRO = CRenderObject.CreateRenderObject(ro.gameObject, "newRenderObject")
	self.m_NewRenderObject = newRO
	newRO.NeedUpdateAABB = true
	newRO.Layer = LayerDefine.ModelForNGUI_3D
	newRO.EnableDelayLoad = false

	local firstId = equip.Equip.HolyFirstModelId
	local secondId = equip.Equip.HolySecondModelId
	local cfgdata = nil
	if firstId > 0 then
		cfgdata = ShenBing_WeaponModel.GetData(firstId)
	end

	-- Skin weapon
	if data.ShenBingType == 17 or data.ShenBingType == 20 or data.ShenBingType == 21 or data.ShenBingType == 22 then
		self:LoadSkinModel(newRO, data.ShenBingType, firstId, secondId, data.Prefab, color)
	else
		if firstId > 0 then
			local hasColor1, color1, hasColor2, color2 = self:GetCustomColor(color)
			-- 双手偃甲
			if data.ShenBingType == 19 then
				newRO:LoadMain(SafeStringFormat3("Item/AniWeapon/%s/Prefab/%s_0%d.prefab", cfgdata.Prefab, cfgdata.Prefab, cfgdata.NewMaterial > 0 and 1 or 2))
				--if color > 0 then
				if cfgdata.Stage < 3 then
					newRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function ( ... )
						local trans = newRO.m_MainObject.transform
						local wp1 = trans:Find("wp01")
						if wp1 then 
							CClientMainPlayer.SetShenbingCustomColorInternal(wp1.gameObject, hasColor1, color1, hasColor1, color1) 
						end
						local wp2 = trans:Find("wp02")
						if wp2 then 
							CClientMainPlayer.SetShenbingCustomColorInternal(wp2.gameObject, hasColor2, color2, hasColor2, color2) 
						end
					end))
				else
					newRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function ( ... )
						local wp1 = newRO.m_MainObject
						if wp1 then CClientMainPlayer.SetShenbingCustomColorInternal(wp1, hasColor1, color1, hasColor1, color1) end
					end))
				end
			else
				if cfgdata.Stage < 3 then
					local pattern = "Item/Weapon_New/Prefab/%s_%s.prefab"
					if cfgdata then
						newRO:LoadMain(SafeStringFormat3(pattern, cfgdata.Prefab, cfgdata.NewMaterial > 0 and "wp1_01" or "wp1_02"), nil, false, false)
					end
					local cfg2 = ShenBing_WeaponModel.GetData(secondId)
					if cfg2 then
						newRO:AddChild(SafeStringFormat3(pattern,cfg2.Prefab, cfgdata.NewMaterial > 0 and "wp2_01" or "wp2_02"), "wp2", "wp2")
					end

					newRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function ( ... )
						local wp1 = newRO.m_MainObject
						if wp1 then 
							newRO:ProcessChangeColor(Table2List({CreateFromClass(ShenBingColorChange, hasColor1, color1, hasColor2, color2, true, false)}, MakeGenericClass(List, IAvatarColorChange)), wp1)
						end
						local wp2 = newRO:GetChild("wp2")
						if wp2 then 
							newRO:ProcessChangeColor(Table2List({CreateFromClass(ShenBingColorChange, hasColor1, color1, hasColor2, color2, false, false)}, MakeGenericClass(List, IAvatarColorChange)), wp2)
						end
					end))

				else--3阶以上模型
					local pattern = "Item/Weapon_New/Prefab/%s.prefab"
					local path = SafeStringFormat3(pattern,cfgdata.Prefab)
					newRO:LoadMain(path, nil, false, false)
					newRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function ( ... )
						local wp1 = newRO.m_MainObject
						if wp1 then CClientMainPlayer.SetShenbingCustomColorInternal(wp1, hasColor1, color1, hasColor1, color1) end
					end))
				end
			end
		else
			newRO:LoadMain(data.Prefab, nil, false, false)
		end
	end

	newRO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (ro)
		self:InitRO(ro, angle, pos, data.ShenBingType)
	end))

	local colors = nil
	if CClientMainPlayer.s_EnableShenbingFxColor then
		local hasColor1, color1, hasColor2, color2 = self:GetCustomColor(color)
		if hasColor1 then
			colors = Table2Array({Color(color1.x, color1.y, color1.z, 0.5), Color(color1.x, color1.y, color1.z, 0.5)}, MakeArrayClass(Color))
		end
	end
	
	if cfgdata then
		if cfgdata.Stage < 3 then
			LuaShenbingMgr:AddWeaponFxToObj(newRO,data.ShenBingType,self.m_CurrentEquipTrainLevel,colors)
		else
			LuaShenbingMgr:AddAdvWeaponFx(newRO,cfgdata.FxId,colors)
		end
	end
end

function CLuaShenbingPeiyangWnd:InitBasicAttr(item)
	local oriIsAdjusted = item.Equip.IsFeiShengAdjusted
	item.Equip.IsFeiShengAdjusted = 0
	Extensions.RemoveAllChildren(self.m_BasicAttrTable.transform)

	local ref = CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), EquipmentTemplate_Equip.GetData(item.TemplateId).Type)
    if ref == EnumBodyPosition.Weapon then
        --物理攻击、法术攻击、攻击速度
        self:AddBasicAttr(self:AppendPhysicalAttackHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalAttackHoly(item.Equip))
    elseif ref == EnumBodyPosition.Shield then
        --格挡、物理防御、法术防御
        self:AddBasicAttr(self:AppendPhysicalDefHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalDefHoly(item.Equip))
        self:AddBasicAttr(self:AppendBlockHoly(item.Equip))
        self:AddBasicAttr(self:AppendBlockDamageHoly(item.Equip))
    elseif ref == EnumBodyPosition.Casque then
        --物理防御、法术防御
        self:AddBasicAttr(self:AppendPhysicalDefHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalDefHoly(item.Equip))
    elseif ref == EnumBodyPosition.Armour then
        --物理防御、法术防御
        self:AddBasicAttr(self:AppendPhysicalDefHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalDefHoly(item.Equip))
    elseif ref == EnumBodyPosition.Belt then
        --气血上限
        self:AddBasicAttr(self:AppendHpFullHoly(item.Equip))
    elseif ref == EnumBodyPosition.Gloves then
        --气血上限
        self:AddBasicAttr(self:AppendHpFullHoly(item.Equip))
    elseif ref == EnumBodyPosition.Shoes then
        --物理防御、法术防御、物理躲避、法术躲避
        self:AddBasicAttr(self:AppendPhysicalDefHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalDefHoly(item.Equip))
        self:AddBasicAttr(self:AppendPhysicalMissHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalMissHoly(item.Equip))
    elseif ref == EnumBodyPosition.Ring then
        --物理攻击、法术攻击
        self:AddBasicAttr(self:AppendPhysicalAttackHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalAttackHoly(item.Equip))
    elseif ref == EnumBodyPosition.Bracelet then
        --物理命中、法术命中
        self:AddBasicAttr(self:AppendPhysicalHitHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalHitHoly(item.Equip))
    elseif ref == EnumBodyPosition.Necklace then
        --物理命中、法术命中、物理躲避、法术躲避
        self:AddBasicAttr(self:AppendPhysicalHitHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalHitHoly(item.Equip))
        self:AddBasicAttr(self:AppendPhysicalMissHoly(item.Equip))
        self:AddBasicAttr(self:AppendMagicalMissHoly(item.Equip))
    end
    self.m_BasicAttrTable:Reposition()
    item.Equip.IsFeiShengAdjusted = oriIsAdjusted
end

function CLuaShenbingPeiyangWnd:AddBasicAttr(desc)
	local obj = NGUITools.AddChild(self.m_BasicAttrTable.gameObject, self.m_BasicAttrTemplate)
	obj:SetActive(true)
	CommonDefs.GetComponent_GameObject_Type(obj, typeof(UILabel)).text = desc
	LuaGameObject.GetChildNoGC(obj.transform, "up").gameObject:SetActive(false)
end

function CLuaShenbingPeiyangWnd:AppendPhysicalAttackHoly(equipment)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = System.String.Format("+{0}%", intensifyTotalValue)
    end

	return System.String.Format(LocalString.GetString("[FFFFFF]物理攻击 {0}-{1}[-][00FFFF]{2}[-]"), equipment.AdjpAttMinHoly_Current, equipment.AdjpAttMaxHoly_Current, strength)
end
function CLuaShenbingPeiyangWnd:AppendMagicalAttackHoly(equipment)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = System.String.Format("+{0}%", intensifyTotalValue)
    end

    return System.String.Format(LocalString.GetString("[FFFFFF]法术攻击 {0}-{1}[-][00FFFF]{2}[-]"), equipment.AdjmAttMinHoly_Current, equipment.AdjmAttMaxHoly_Current, strength)
end
function CLuaShenbingPeiyangWnd:AppendPhysicalDefHoly(equipment)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = System.String.Format("+{0}%", intensifyTotalValue)
    end

    return System.String.Format(LocalString.GetString("[FFFFFF]物理防御 {0}[-][00FFFF]{1}[-]"), equipment.AdjpDefHoly_Current, strength)
end
function CLuaShenbingPeiyangWnd:AppendMagicalDefHoly(equipment)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = System.String.Format("+{0}%", intensifyTotalValue)
    end

    return System.String.Format(LocalString.GetString("[FFFFFF]法术防御 {0}[-][00FFFF]{1}[-]"), equipment.AdjmDefHoly_Current, strength)
end
function CLuaShenbingPeiyangWnd:AppendBlockHoly(equipment)
    return System.String.Format(LocalString.GetString("[FFFFFF]格挡 {0}[-]"), equipment.AdjBlockHoly_Current)
end
function CLuaShenbingPeiyangWnd:AppendBlockDamageHoly(equipment)
    return System.String.Format(LocalString.GetString("[FFFFFF]格挡伤害减免 {0}[-]"), equipment.AdjBlockDamageHoly_Current)
end
function CLuaShenbingPeiyangWnd:AppendPhysicalHitHoly(equipment)
    return System.String.Format(LocalString.GetString("[FFFFFF]物理命中 {0}[-]"), equipment.AdjpHitHoly_Current)
end
function CLuaShenbingPeiyangWnd:AppendMagicalHitHoly(equipment)
    return System.String.Format(LocalString.GetString("[FFFFFF]法术命中 {0}[-]"), equipment.AdjmHitHoly_Current)
end
function CLuaShenbingPeiyangWnd:AppendPhysicalMissHoly(equipment)
    return System.String.Format(LocalString.GetString("[FFFFFF]物理躲避 {0}[-]"), equipment.AdjpMissHoly_Current)
end
function CLuaShenbingPeiyangWnd:AppendMagicalMissHoly(equipment)
    return System.String.Format(LocalString.GetString("[FFFFFF]法术躲避 {0}[-]"), equipment.AdjmMissHoly_Current)
end
function CLuaShenbingPeiyangWnd:AppendHpFullHoly(equipment)
    local strength = ""
    local intensifyTotalValue = equipment:GetIntensifyTotalValue()
    if intensifyTotalValue > 0 then
        strength = System.String.Format("+{0}%", intensifyTotalValue)
    end

    return System.String.Format(LocalString.GetString("[FFFFFF]气血上限 {0:f3}%[-][00FFFF]{1}[-]"), equipment.MulHpFullHoly_Current * 100, strength)
end

function CLuaShenbingPeiyangWnd:RefreshItem(args)
    if self.m_CurrentTabIndex == 0 then
        self:SetCostItemCount(self.m_ItemRootObj,self.m_ItemTemplateId,self.m_CurrentNeedJqzCount)
        self:SetCostItemCount(self.m_ItemRootObj2,self.m_ItemTemplateId2,self.m_CurrentNeedDgzCount)
    else
		self:SetCostItemCount(self.m_ItemRootObj,self.m_RecastItemTemplateId,self.m_RecastNeedJqzCount)
    end
end

function CLuaShenbingPeiyangWnd:AddEquip(args)
	if args[1] ~= "ShenbingPeiyang" then
		return
	end
	self:AddEquipInternal(args[0])
end

function CLuaShenbingPeiyangWnd:AddEquipInternal(id)
	local equip = CItemMgr.Inst:GetById(id)
	-- filter equip with intensify and gems
	if equip.Equip.IntensifyLevel > 0 or equip.Equip.HasGems then
		g_MessageMgr:ShowMessage("Shenbing_Tupo_Equip_Illegal")
		return
	end
	self.m_EquipTexture:LoadMaterial(equip.Icon)
	self.m_EquipAddObj:SetActive(false)
	local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
	for _, v in pairs(placeTable) do
		local pos = CItemMgr.Inst:FindItemPosition(v, equip.Id)
		if pos > 0 then
			self.m_BreakEquipPlace = v
			self.m_BreakEquipPos = pos
			self.m_BreakEquipId = equip.Id
			break
		end
	end
end

function CLuaShenbingPeiyangWnd:RequestRecastShenBingSuccess()
	self:InitDifferentArea()
	self.m_WordUpdateFx:DestroyFx()
	self.m_WordUpdateFx:LoadFx("Fx/UI/Prefab/UI_huanranyixin_cizhui.prefab")
end

function CLuaShenbingPeiyangWnd:ShenbingQianyuanSumitSuccess()
	self:InitDifferentArea()
end

function CLuaShenbingPeiyangWnd:RequestTrainShenBingSuccess()
	LuaShenbingMgr.ShenbingQianyuanItemTable = {}
	self.m_ShowArrow = true
	self:InitDifferentArea()
	self:ShowEquipDiffProperty()
	for i = 1, #self.m_ChangeAppearanceLevelTable do
		if self.m_ChangeAppearanceLevelTable[i] == self.m_CurrentEquipTrainLevel then
			self:ChangeAppearance()
			break
		end
	end
	local equipData = EquipmentTemplate_Equip.GetData(self.m_CurrentEquipTemplateId)
	if equipData ~= nil then
		g_MessageMgr:ShowMessage("Shenbing_Grow_Success_Msg", self.m_CurrentEquipTrainLevel, self:GetGrowTips(equipData.Type, self.m_CurrentEquipTrainLevel - 1))
	end
	self:ShowTrainFx()
end

function CLuaShenbingPeiyangWnd:ShowEquipDiffProperty()
	local equip = CItemMgr.Inst:GetById(self.m_EquipId)
	if not equip then
		return
	end
	local equipData = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	if not equipData then
		return
	end

	local lastGrowRatio = 0
	local curGrowRatio = 0
	for i = 1, ShenBing_Grow.GetDataCount() do
		if equipData.Grade >= ShenBing_Grow.GetData(i).MinOriEquipLevel and equipData.Grade <= ShenBing_Grow.GetData(i).MaxOriEquipLevel then
			if ShenBing_Grow.GetData(i).GrowLevel <= self.m_CurrentEquipTrainLevel then
				curGrowRatio = curGrowRatio + ShenBing_Grow.GetData(i).BaseGrowRatio
			end
			if ShenBing_Grow.GetData(i).GrowLevel <= self.m_CurrentEquipTrainLevel - 1 then
				lastGrowRatio = lastGrowRatio + ShenBing_Grow.GetData(i).BaseGrowRatio
			end
		end
	end
	local list = CreateFromClass(MakeGenericClass(List, PlayerShowDifProType))
	equip = equip.Equip
	local oriIsAdjusted = equip.IsFeiShengAdjusted
	equip.IsFeiShengAdjusted = 0
	local attrValue = {equip.AdjpAttMinHoly_Current, equip.AdjpAttMaxHoly_Current, equip.AdjmAttMinHoly_Current, equip.AdjmAttMaxHoly_Current, equip.AdjpDefHoly_Current, equip.AdjmDefHoly_Current, equip.AdjBlockHoly_Current, equip.AdjBlockDamageHoly_Current, equip.AdjpHitHoly_Current, equip.AdjmHitHoly_Current, equip.AdjpMissHoly_Current, equip.AdjmMissHoly_Current, equip.MulHpFullHoly_Current}
	equip.IsFeiShengAdjusted = oriIsAdjusted
	local attrName = {LocalString.GetString("最小物攻"), LocalString.GetString("最大物攻"), LocalString.GetString("最小法攻"), LocalString.GetString("最大法攻"), LocalString.GetString("物理防御"), LocalString.GetString("法术防御"), LocalString.GetString("格挡"), LocalString.GetString("格挡伤害减免"), LocalString.GetString("物理命中"), LocalString.GetString("法术命中"), LocalString.GetString("物理躲避"), LocalString.GetString("法术躲避"), LocalString.GetString("气血上限")}
	local maxRatio = ShenBing_Setting.GetData().MaxBasePropertyRatio
	for i = 1, #attrValue do
		if attrValue[i] > 1 then
			local base = attrValue[i] / (maxRatio *(curGrowRatio))
			local lastAttr = base * lastGrowRatio * maxRatio
			-- skip same value
			if math.abs(attrValue[i] - lastAttr) > 0.00001 then
				if attrName[i] ~= LocalString.GetString("气血上限") then
					lastAttr = math.max(1, lastAttr)
				end
				local diffPro = PlayerShowDifProType(lastAttr, attrValue[i], attrName[i])
				if attrName[i] == LocalString.GetString("气血上限") then
					diffPro.showPercent = true
				end
				CommonDefs.ListAdd_LuaCall(list, diffPro)
			end
		end
	end
	if list.Count > 0 then
		CPropertyDifMgr.ShowEquipDifWnd(list)
	end
end

function CLuaShenbingPeiyangWnd:ChangeAppearance()
	self:InitAppearance()
	self.m_ModelUpdateFx:DestroyFx()
	self.m_ModelUpdateFx:LoadFx("Fx/UI/Prefab/UI_huanranyixin.prefab")
end

function CLuaShenbingPeiyangWnd:RequestBreakShenBingTrainLevelSuccess()
	self:Clear()
	self:InitDifferentArea()
	g_MessageMgr:ShowMessage("ShenBing_Break_Success")
	self.m_BreakFx:DestroyFx()
	self.m_BreakFx:LoadFx("Fx/UI/Prefab/UI_huanranyixin_cizhui.prefab")
end

function CLuaShenbingPeiyangWnd:OnEnable()
	g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "AddEquip")
	g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:AddListener("RequestRecastShenBingSuccess", self, "RequestRecastShenBingSuccess")
	g_ScriptEvent:AddListener("ShenbingQianyuanSumitSuccess", self, "ShenbingQianyuanSumitSuccess")
	g_ScriptEvent:AddListener("RequestTrainShenBingSuccess", self, "RequestTrainShenBingSuccess")
	g_ScriptEvent:AddListener("RequestBreakShenBingTrainLevelSuccess", self, "RequestBreakShenBingTrainLevelSuccess")
end

function CLuaShenbingPeiyangWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "AddEquip")
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("RequestRecastShenBingSuccess", self, "RequestRecastShenBingSuccess")
	g_ScriptEvent:RemoveListener("ShenbingQianyuanSumitSuccess", self, "ShenbingQianyuanSumitSuccess")
	g_ScriptEvent:RemoveListener("RequestTrainShenBingSuccess", self, "RequestTrainShenBingSuccess")
	g_ScriptEvent:RemoveListener("RequestBreakShenBingTrainLevelSuccess", self, "RequestBreakShenBingTrainLevelSuccess")
end

function CLuaShenbingPeiyangWnd:OnDestroy()
	LuaShenbingMgr.ShenbingQianyuanItemTable =  {}
	CUIManager.DestroyModelTexture(self.m_ModelCameraName)
	CUIManager.ModelLightEnabled = true
	if self.m_NewRenderObject then
		self.m_NewRenderObject:Destroy()
	end
end
