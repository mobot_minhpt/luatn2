-- Auto Generated!!
local CLingShouModelTextureLoader = import "L10.UI.CLingShouModelTextureLoader"
local CLingshouOverViewModels = import "L10.UI.CLingshouOverViewModels"
local Extensions = import "Extensions"
local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
local LocalString = import "LocalString"
local Vector3 = import "UnityEngine.Vector3"
CLingshouOverViewModels.m_Init_CS2LuaHook = function (this, lingshouId) 
    this.varyIndex = 0
    this.lingshou = LingShou_LingShou.GetData(lingshouId)
    if this.lingshou == nil then
        return
    end
    this:InitModels()
    this.nameLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), this.lingshou.Name, this.vary[0])
    this.shenshouTag.enabled = this.lingshou.ShenShou ~= 0

    this.angels = CreateFromClass(MakeArrayClass(System.Single), this.modelTexture.Length)
    this.interDeg = 360 / this.angels.Length
    do
        local i = 0
        while i < this.modelTexture.Length do
            this.angels[i] = i * this.interDeg
            i = i + 1
        end
    end
    this:UpdatePosition()
end
CLingshouOverViewModels.m_InitModels_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.modelTexture.Length do
            local loader = CommonDefs.GetComponent_Component_Type(this.modelTexture[i], typeof(CLingShouModelTextureLoader))
            if loader ~= nil then
                loader:Init(this.lingshou.ID, 0, i + 1)
            end
            i = i + 1
        end
    end
end
CLingshouOverViewModels.m_UpdatePosition_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.modelTexture.Length do
            local offsetX = math.sin(Deg2Rad * this.angels[i]) * this.localOffsetX
            if not System.Single.IsNaN(offsetX) then
                Extensions.SetLocalPositionX(this.modelTexture[i].transform, math.sin(Deg2Rad * this.angels[i]) * this.localOffsetX)
            end
            local scale = math.cos(Deg2Rad * this.angels[i]) * 0.5 + 0.55
            if not System.Single.IsNaN(scale) then
                this.modelTexture[i].transform.localScale = Vector3(scale, scale, 1)
            end
            if scale < 0.8 and scale > 0.2 then
                this.modelTexture[i].texture.depth = 3
            elseif scale > 0.8 then
                this.modelTexture[i].texture.depth = 4
            else
                this.modelTexture[i].texture.depth = 2
            end
            i = i + 1
        end
    end
end
CLingshouOverViewModels.m_OnDrag_CS2LuaHook = function (this, go, delta) 
    local moveRad = math.asin(delta.x / this.localOffsetX)
    local moveDeg = Rad2Deg * moveRad
    do
        local i = 0
        while i < this.angels.Length do
            local a = math.floor(math.floor((this.angels[i] + moveDeg)) / 360)
            this.angels[i] = this.angels[i] + moveDeg - 360 * a
            i = i + 1
        end
    end
    this:UpdatePosition()
end
CLingshouOverViewModels.m_OnDragEnd_CS2LuaHook = function (this, go) 
    local selectIndex = 0
    do
        local i = 0
        while i < this.angels.Length do
            if (this.angels[i] <= this.interDeg / 2 and this.angels[i] > - this.interDeg / 2) or (this.angels[i] <= 360 and this.angels[i] > 360 - this.interDeg / 2) then
                selectIndex = i
                break
            end
            i = i + 1
        end
    end
    this:UpdateName(selectIndex)
    do
        local i = 0
        while i < this.angels.Length do
            local moveDeg = (i - selectIndex) * this.interDeg
            local moveTo = math.sin(Deg2Rad * moveDeg) * this.localOffsetX
            local moveToScale = math.cos(Deg2Rad * moveDeg) * 0.5 + 0.5
            --float moveFrom = Mathf.Sin(Deg2Rad * angels[i]) * localOffsetX;
            --fx.transform.DOLocalPath(waypoints, 2f, PathType.Linear, PathMode.Ignore, 10).SetLoops(-1).SetEase(Ease.Linear);
            LuaTweenUtils.DOLocalMoveX(this.modelTexture[i].transform, moveTo, this.moveDuration, false)
            LuaTweenUtils.DOScale(this.modelTexture[i].transform, Vector3(moveToScale, moveToScale, 1), this.moveDuration)
            this.angels[i] = moveDeg
            i = i + 1
        end
    end
end
CLingshouOverViewModels.m_UpdateName_CS2LuaHook = function (this, selectIndex) 
    if this.lingshou ~= nil then
        this.varyIndex = selectIndex
        this.nameLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), this.lingshou.Name, this.vary[selectIndex])
    end
end
