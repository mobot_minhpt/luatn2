local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CChatInput = import "L10.UI.CChatInput"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local Quaternion = import "UnityEngine.Quaternion"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local CEffectMgr = import "L10.Game.CEffectMgr"

LuaOffWorldPassChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPassChooseWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaOffWorldPassChooseWnd, "node1", "node1", GameObject)
RegistChildComponent(LuaOffWorldPassChooseWnd, "node2", "node2", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaOffWorldPassChooseWnd, "btnTable")
RegistClassMember(LuaOffWorldPassChooseWnd, "chooseTable")
RegistClassMember(LuaOffWorldPassChooseWnd, "m_ModelTextureLoader")
RegistClassMember(LuaOffWorldPassChooseWnd, "m_ModelName")
RegistClassMember(LuaOffWorldPassChooseWnd, "weaponRo")
RegistClassMember(LuaOffWorldPassChooseWnd, "weaponName")
RegistClassMember(LuaOffWorldPassChooseWnd, "swShowArgs")

function LuaOffWorldPassChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaOffWorldPassChooseWnd:Init()
  local btnTable = {
    self.node1.transform:Find('btn1').gameObject,
    self.node1.transform:Find('btn2').gameObject,
    self.node1.transform:Find('btn3').gameObject,
    self.node1.transform:Find('btn4').gameObject,
    self.node1.transform:Find('btn5').gameObject,
  }
  self.btnTable = btnTable
  self:InitQuestion1()
end

--@region UIEvent

--@endregion UIEvent

function LuaOffWorldPassChooseWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateOffWorldPlayGetSw", self, "UpdateGetSW")
end

function LuaOffWorldPassChooseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlayGetSw", self, "UpdateGetSW")
end

function LuaOffWorldPassChooseWnd:InitQuestion1()
	CUIManager.DestroyModelTexture(self.m_ModelName)
  self.node1:SetActive(true)
  self.node2:SetActive(false)

  local quesLabel = self.node1.transform:Find('question'):GetComponent(typeof(UILabel))
  local quesData = OffWorldPass_SelfEditedOffWorldFashion.GetData(1)
  quesLabel.text = quesData.Question
  local chooseAns = {
    {quesData.Answer1,quesData.Wuxing1},
    {quesData.Answer2,quesData.Wuxing2},
    {quesData.Answer3,quesData.Wuxing3},
    {quesData.Answer4,quesData.Wuxing4},
    {quesData.Answer5,quesData.Wuxing5},
  }
  for i,v in ipairs(self.btnTable) do
    local node = v
    node:SetActive(true)
    node.transform:Find('Label'):GetComponent(typeof(UILabel)).text = chooseAns[i][1]
    UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function()
      self.chooseTable = {}
      self.chooseTable[1] = tonumber(chooseAns[i][2])
      self:InitQuestion2()
    end)
  end
end

function LuaOffWorldPassChooseWnd:InitQuestion2()
  local quesLabel = self.node1.transform:Find('question'):GetComponent(typeof(UILabel))
  local quesData = OffWorldPass_SelfEditedOffWorldFashion.GetData(2)
  quesLabel.text = quesData.Question

  local chooseAns = {
    {quesData.Answer2,quesData.Type2},
    {quesData.Answer3,quesData.Type3},
    {quesData.Answer4,quesData.Type4},
  }

  
  if EnumToInt(CClientMainPlayer.Inst.Class) == 1 then
    table.insert(chooseAns, {quesData.Answer5, quesData.Type5})
  elseif EnumToInt(CClientMainPlayer.Inst.Class) == 7 and EnumToInt(CClientMainPlayer.Inst.Gender) == 0 then
    table.insert(chooseAns, {quesData.Answer1, quesData.Type1})
  end

  for i,v in ipairs(self.btnTable) do
    local node = v
    if chooseAns[i] ~= nil then
      node:SetActive(true)
      node.transform:Find('Label'):GetComponent(typeof(UILabel)).text = chooseAns[i][1]
      UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function()
        self.chooseTable[2] = tonumber(chooseAns[i][2])
        self:InitSWShow()
      end)
    else
      node:SetActive(false)
    end
  end
end

function LuaOffWorldPassChooseWnd:LoadModel(ro, prefabname, data)
    ro:LoadMain(prefabname, nil, false, false)
		ro.transform.localRotation = Quaternion.Euler(self.swShowArgs[5],self.swShowArgs[6],self.swShowArgs[7])
		self.weaponRo = ro
    -- 加特效
    -- 武器：五行：特效
    local index = (data.weaponType-1)*5 + data.wuxing-1
    if OffWorldPass_Setting.GetData().SelfEditFashionFxId.Length > index*3+2 then
      local fxId = OffWorldPass_Setting.GetData().SelfEditFashionFxId[index*3+2]
      CEffectMgr.Inst:AddObjectFX(fxId, ro,0, 1, 1, nil, false, EnumWarnFXType.None,Vector3.zero,Vector3.zero,nil)
    end
end

function LuaOffWorldPassChooseWnd:InitSWModel(data)
  local equipmentId = nil
  OffWorldPass_Fashion.Foreach(function(i,v)
    if v.type == data.weaponType and v.wuxing == data.wuxing then
      equipmentId = v.equipmentId
    end
  end)

  if not equipmentId then
    return
  end

  local equipData = EquipmentTemplate_Equip.GetData(equipmentId)

  local name = equipData.Name
  local titleLabel = self.node2.transform:Find('question'):GetComponent(typeof(UILabel))
  local colorTable = {
    LocalString.GetString('[c][fdab2c]金属性%s[-][/c]'),
    LocalString.GetString('[c][15b600]木属性%s[-][/c]'),
    LocalString.GetString('[c][0189b6]水属性%s[-][/c]'),
    LocalString.GetString('[c][d04500]火属性%s[-][/c]'),
    LocalString.GetString('[c][bf6700]土属性%s[-][/c]'),
  }
  self.weaponName = SafeStringFormat3(colorTable[data.wuxing],name)
  titleLabel.text = SafeStringFormat3(LocalString.GetString('恭喜你，最适合你的神武是%s，是否选择？'),self.weaponName)

  self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
    self:LoadModel(ro, equipData.Prefab, data)
  end)

  local weapon = self.node2.transform:Find('weapon').gameObject
  local m_FakeModel = weapon:GetComponent(typeof(UITexture))
  self.m_ModelName = "_OffWorldPassWeaponGetPreview_"
  self.swShowArgs = LuaOffWorldPassMgr.GetWeaponShowArgs(data)
  --m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,90,0,-0.3,3.04,false,true,1,true)
  m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,self.swShowArgs[1],self.swShowArgs[2],self.swShowArgs[3],self.swShowArgs[4],false,true,1,true)
	local onDrag = function(go,delta)
		self.weaponRo.transform:Rotate(Vector3.right,(delta.x+delta.y+delta.z))
	end
	CommonDefs.AddOnDragListener(weapon,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
end

function LuaOffWorldPassChooseWnd:InitSWShow()
  self.node1:SetActive(false)
  self.node2:SetActive(true)

  local wuxing = self.chooseTable[1]
  local qtype = self.chooseTable[2]
  local btn1 = self.node2.transform:Find('btn1').gameObject
  local btn2 = self.node2.transform:Find('btn2').gameObject
  btn1:SetActive(true)
  btn2:SetActive(true)
  local chatInputSc = self.node2.transform:Find('ChatInput'):GetComponent(typeof(CChatInput))
  chatInputSc.gameObject:SetActive(false)

  UIEventListener.Get(btn2).onClick = DelegateFactory.VoidDelegate(function()
    self:InitQuestion1()
  end)
  UIEventListener.Get(btn1).onClick = DelegateFactory.VoidDelegate(function()
    MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("确定要选择%s吗？确定后不可更改"),self.weaponName), DelegateFactory.Action(function ()
      self:InitSWNameInput()
    end), nil, nil, nil, false)
  end)

  self:InitSWModel({wuxing = wuxing, weaponType = qtype})
end

function LuaOffWorldPassChooseWnd:InitSWNameInput()
  local btn1 = self.node2.transform:Find('btn1').gameObject
  local btn2 = self.node2.transform:Find('btn2').gameObject
  btn1:SetActive(false)
  btn2:SetActive(false)
  local chatInputSc = self.node2.transform:Find('ChatInput'):GetComponent(typeof(CChatInput))
  chatInputSc.gameObject:SetActive(true)
  local titleLabel = self.node2.transform:Find('question'):GetComponent(typeof(UILabel))
  titleLabel.text = LocalString.GetString('请为你的神武命名')

  chatInputSc.input.defaultText = LocalString.GetString('点击输名字(限七个字)')

  chatInputSc.OnSend = DelegateFactory.Action_string(function (msg)
    self:OnSubmitBtnClick(msg)
  end)

  local randomBtn = self.node2.transform:Find('ChatInput/RandomButton').gameObject
  UIEventListener.Get(randomBtn).onClick = DelegateFactory.VoidDelegate(function()
    self:RandomNameInput(chatInputSc)
  end)
end

function LuaOffWorldPassChooseWnd:RandomNameInput(chatInputSc)
  local num = ShenBing_WeaponSufName.GetDataCount()
  local chooseIndex = math.random(1,num)
  local chooseName = nil

  local count = 1
  ShenBing_WeaponSufName.Foreach(function(k,v)
    if count == chooseIndex then
      chooseName = k
    end
    count = count + 1
  end)

  chatInputSc.input.value = chooseName
end

function LuaOffWorldPassChooseWnd:GetStringLength(inputstr)
   local lenInByte = #inputstr
   local width = 0
   local i = 1
   while (i<=lenInByte)
    do
        local curByte = string.byte(inputstr, i)
        local byteCount = 1
        if curByte>0 and curByte<=127 then
            byteCount = 1
        elseif curByte>=192 and curByte<223 then
            byteCount = 2
        elseif curByte>=224 and curByte<239 then
            byteCount = 3
        elseif curByte>=240 and curByte<=247 then
            byteCount = 4
        end
        local char = string.sub(inputstr, i, i+byteCount-1)

        i = i + byteCount
        width = width + 1
    end
    return width
end

function LuaOffWorldPassChooseWnd:OnSubmitBtnClick(msg)
  if System.String.IsNullOrEmpty(msg) then
    g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
    return
  end
  local sendtext = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(msg)

  if System.String.IsNullOrEmpty(sendtext) then
    g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
    return
  end

  local length = self:GetStringLength(sendtext)
  if length > 7 then
    g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString('长度超过限制！'))
    return
  elseif length == 0 then
    g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
    return
  end

  Gac2Gas.SubmitSelfEditedOffWorldFashion(self.chooseTable[2],self.chooseTable[1],sendtext)
  --CUIManager.CloseUI(CLuaUIResources.OffWorldPassChooseWnd)
end

function LuaOffWorldPassChooseWnd:UpdateGetSW()
  CUIManager.ShowUI(CLuaUIResources.OffWorldPassChooseGetWnd)
  CUIManager.CloseUI(CLuaUIResources.OffWorldPassChooseWnd)
end

function LuaOffWorldPassChooseWnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.m_ModelName)
end
