local MessageWndManager = import "L10.UI.MessageWndManager"
local Vector3 = import "UnityEngine.Vector3"
local EnumHouseCompetitionStage = import "L10.Game.EnumHouseCompetitionStage"
local EHouseMainWndTab = import "L10.UI.EHouseMainWndTab"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local Constants = import "L10.Game.Constants"
local CHouseUIMgr = import "L10.UI.CHouseUIMgr"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CQiYuanMgr=import "L10.UI.CQiYuanMgr"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"

CLuaHouseMinimap = class()
RegistClassMember(CLuaHouseMinimap,"Anchor")
RegistClassMember(CLuaHouseMinimap,"Grid")
RegistClassMember(CLuaHouseMinimap,"ExpandBtn")
RegistClassMember(CLuaHouseMinimap,"FurnishBtn")
RegistClassMember(CLuaHouseMinimap,"m_FurnishAlert")
RegistClassMember(CLuaHouseMinimap,"HouseBtn")
RegistClassMember(CLuaHouseMinimap,"QifuBtn")
RegistClassMember(CLuaHouseMinimap,"HouseCompetitionBtn")
RegistClassMember(CLuaHouseMinimap,"m_WinterBtn")

CLuaHouseMinimap.s_IsHouseCompetitionGuide=false
CLuaHouseMinimap.sbShowQifuBtn = false

function CLuaHouseMinimap:Awake()
    self.Anchor = self.transform:Find("Anchor").gameObject
    self.Grid = self.transform:Find("Anchor/Tip"):GetComponent(typeof(UIGrid))
    self.ExpandBtn = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    self.FurnishBtn = self.transform:Find("Anchor/Tip/FurnishButton").gameObject
    self.m_FurnishAlert = self.FurnishBtn.transform:Find("Alert").gameObject
    self.m_FurnishAlert:SetActive(false)

    self.HouseBtn = self.transform:Find("Anchor/Tip/HouseButton").gameObject
    self.QifuBtn = self.transform:Find("Anchor/Tip/QifuButton").gameObject
    self.QifuBtn:SetActive(false)
    self.HouseCompetitionBtn = self.transform:Find("Anchor/Tip/HouseCompetitionButton").gameObject
    self.HouseCompetitionBtn:SetActive(false)
    self.m_WinterBtn = self.transform:Find("Anchor/Tip/WinterButton").gameObject
    self.m_WinterBtn:SetActive(false)

    UIEventListener.Get(self.ExpandBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnExpandBtnClicked(go) end)
    UIEventListener.Get(self.FurnishBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnFurnishBtnClicked(go) end)
    UIEventListener.Get(self.HouseBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnHouseBtnClicked(go) end)
    UIEventListener.Get(self.QifuBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnQifuBtnClicked(go) end)
    UIEventListener.Get(self.HouseCompetitionBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnHouseCompetitionBtnClick(go) end)
    UIEventListener.Get(self.m_WinterBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickWinterButton(go) end)

end

function CLuaHouseMinimap:OnClickWinterButton(go)
    local prop = CClientHouseMgr.Inst.mFurnitureProp
    if prop and prop.IsEnableWinterScene>0 then
        --如果已经全部解锁了，则不显示界面
        CUIManager.ShowUI(CLuaUIResources.WinterHouseUnlockListWnd)
    else
        CUIManager.ShowUI(CLuaUIResources.WinterHouseChooseWnd)
    end
end

function CLuaHouseMinimap:OnQifuBtnClicked(go)
    CQiYuanMgr.OpenQiyuanWnd()
end

function CLuaHouseMinimap:OnHouseCompetitionBtnClick(go)
    -- CHouseCompetitionMgr.Inst:OpenRegisterUrl()
    if not CClientMainPlayer.Inst then return end

    local grade = HouseCompetition_Setting.GetData().PlayerGradeNeed
    if CClientMainPlayer.Inst.MaxLevel < grade then
        g_MessageMgr:ShowMessage("House_Competition_Need_Level")
        return
    end
    CLuaWebBrowserMgr.OpenHouseActivityUrl("JiaYuan2022Apply")
end

function CLuaHouseMinimap:OnFurnishBtnClicked(go)
    if CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_House_Furnish, true) then
        return
    end
    CClientFurnitureMgr.Inst:StartFurnish()
    CLuaSceneMgr.OnEnterJiaYuanShiNei( )
end

function CLuaHouseMinimap:Init( )
    self.QifuBtn:SetActive(CLuaHouseMinimap.sbShowQifuBtn)
    if CLuaHouseMinimap.sbShowQifuBtn then
        self.QifuBtn.transform.parent = self.Grid.transform
        self.QifuBtn.transform:SetSiblingIndex(0)
    else
        self.QifuBtn.transform.parent = self.Anchor.transform
    end

    if ((CHouseCompetitionMgr.Inst:GetCurrentStage() == EnumHouseCompetitionStage.eChusai and COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HouseCompetition2020)) 
    or CHouseCompetitionMgr.sb_IsInMirrorServer) and CClientHouseMgr.Inst:IsInOwnHouse() then
        self.HouseCompetitionBtn.transform.parent = self.Grid.transform
        self.HouseCompetitionBtn.transform:SetSiblingIndex(0)
        self.HouseCompetitionBtn:SetActive(true)
    else
        self.HouseCompetitionBtn.transform.parent = self.Anchor.transform
        self.HouseCompetitionBtn:SetActive(false)
    end

    self.m_WinterBtn:SetActive(CClientFurnitureMgr.Inst.m_IsWinter)

    self.Grid:Reposition()

    CLuaGuideMgr.CheckOpenHouseCompetition()
end
function CLuaHouseMinimap:OnEnable( )
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("OnShowQiyuanBtnFirstGoHome", self, "OnShowQiyuanBtnFirstGoHome")
	g_ScriptEvent:AddListener("OnSendHousePrayResult", self, "OnSendHousePrayResult")
    self.m_FurnishAlert:SetActive(CLuaGuideMgr.NeedWanShengJieSkyboxGuide())
    CLuaSceneMgr.OnEnterJiaYuanShiNei( )
end
function CLuaHouseMinimap:OnDisable( )
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("OnShowQiyuanBtnFirstGoHome", self, "OnShowQiyuanBtnFirstGoHome")
	g_ScriptEvent:RemoveListener("OnSendHousePrayResult", self, "OnSendHousePrayResult")
    CLuaSceneMgr.OnEnterJiaYuanShiNei( )
end

function CLuaHouseMinimap:OnSendHousePrayResult()
    --引导的时候不处理
    if not CLuaHouseMinimap.s_IsHouseCompetitionGuide then
        CLuaHouseMinimap.sbShowQifuBtn = false
        self:Init()
    end
end

function CLuaHouseMinimap:OnShowQiyuanBtnFirstGoHome()
    --引导的时候不处理
    if not CLuaHouseMinimap.s_IsHouseCompetitionGuide then
        self:Init()
    end
end

function CLuaHouseMinimap:OnHideTopAndRightTipWnd()
    local rotation = self.ExpandBtn.transform.localEulerAngles
    self.ExpandBtn.transform.localEulerAngles = Vector3(rotation.x, rotation.y, 180)
end


function CLuaHouseMinimap:OnHouseBtnClicked( go) 
    if not CSwitchMgr.EnableHouse then 
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end

    if CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse() then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("正在巫先生家园，无法查看自己的家园信息"))
        return
    end

    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.IsCrossServerPlayer then
            g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
            return
        end
        local houseid = CClientMainPlayer.Inst.ItemProp.HouseId
        if houseid ~= nil and houseid ~= "" then
            CHouseUIMgr.OpenHouseMainWnd(EHouseMainWndTab.EInfoTab)
        else
            local message = g_MessageMgr:FormatMessage("BUY_HOUSE_TIP")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                CTrackMgr.Inst:FindNPC(Constants.JiayuanNpcId, Constants.HangZhouID, 89, 38, nil, nil)
            end), nil, LocalString.GetString("前往"), LocalString.GetString("取消"), false)
        end
    end
end
function CLuaHouseMinimap:OnExpandBtnClicked( go) 
    local rotation = self.ExpandBtn.transform.localEulerAngles
    self.ExpandBtn.transform.localEulerAngles = Vector3(rotation.x, rotation.y, 0)

    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
function CLuaHouseMinimap:GetGuideGo( methodName) 
    if methodName == "GetFurnishButton" then
        return self:GetFurnishButton()
    elseif methodName == "GetHouseCompetitionButton" then
        return self.HouseCompetitionBtn
    end
    return nil
end
function CLuaHouseMinimap:GetFurnishButton( )
    if CGuideMgr.Inst:IsInPhase(45) then
        --是否是主人 可能在别人家 在别人家的时候不中断引导
        if not CClientHouseMgr.Inst:IsCurHouseOwner() then
            return nil
        end
        --在自己家 不满足条件 就中断
        if CClientFurnitureMgr.Inst:IsMiaopu3Placed() or not CClientFurnitureMgr.Inst:IsMiaopu3Exist() then
            CGuideMgr.Inst:EndCurrentPhase()
            return nil
        end
    end

    return self.FurnishBtn
end

function CLuaHouseMinimap:OnDestroy()
    CLuaHouseMinimap.sbShowQifuBtn = false
    CLuaHouseMinimap.s_IsHouseCompetitionGuide = false
end
