local MessageWndManager=import "L10.UI.MessageWndManager"
CLuaMiniDouDiZhuWnd = class()
RegistClassMember(CLuaMiniDouDiZhuWnd,"m_PlayerViews")
RegistClassMember(CLuaMiniDouDiZhuWnd,"m_Highlight")
RegistClassMember(CLuaMiniDouDiZhuWnd,"m_Region")
RegistClassMember(CLuaMiniDouDiZhuWnd,"m_LeftTime")
RegistClassMember(CLuaMiniDouDiZhuWnd,"m_ResultTick")


CLuaMiniDouDiZhuWnd.s_LastPosition = {600,45}

function CLuaMiniDouDiZhuWnd:Start()
    local sprite = self.transform:Find("Node/Sprite"):GetComponent(typeof(UISprite))
    local corners = sprite.worldCorners
    local vv1 = CUIManager.UIMainCamera:WorldToViewportPoint(corners[2])
    local vv2 = CUIManager.UIMainCamera:WorldToViewportPoint(corners[0])
    local offsetX = (vv1.x-vv2.x)/2
    local offsetY = (vv1.y-vv2.y)/2

    local node = self.transform:Find("Node")
    self.m_Region = node.gameObject
    local pos = CLuaMiniDouDiZhuWnd.s_LastPosition
    node.localPosition = Vector3(pos[1],pos[2],0)
    UIEventListener.Get(node.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
        local sv = CUIManager.UIMainCamera:WorldToScreenPoint(node.position)
        sv = Vector3(sv.x+delta.x,sv.y+delta.y,0)

        local vv = CUIManager.UIMainCamera:ScreenToViewportPoint(sv)
        vv = Vector3(math.max(offsetX,math.min(vv.x,1-offsetX)),math.max(offsetY,math.min(vv.y,1-offsetY)),0)

        local wv = CUIManager.UIMainCamera:ViewportToWorldPoint(vv)
        wv = Vector3(wv.x,wv.y,0)

        node.position = wv
        local lv = node.localPosition
        CLuaMiniDouDiZhuWnd.s_LastPosition = {lv.x,lv.y}
    end)
    UIEventListener.Get(node.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.MiniDouDiZhuWnd)
        
        --倒计时
        if CLuaDouDiZhuMgr.m_AutoContinueLeftTime>0 then
            CUIManager.ShowUI(CLuaUIResources.DouDiZhuHouseResultWnd)
        end
        CUIManager.ShowUI(CUIResources.DouDiZhuWnd)
    end)

end
function CLuaMiniDouDiZhuWnd:Awake()
    self.m_Highlight = self.transform:Find("Node/Bg/Highlight").gameObject
    self.m_Highlight:SetActive(false)

    local closeButton=self.transform:Find("Node/Bg/CloseButton").gameObject
    UIEventListener.Get(closeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local msgKey="DouDiZhu_Leave_Game"
        if CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightPre or CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildFightFinal then--家园
            msgKey="DouDiZhu_Fight_Leave_Game"
        end
        if CLuaDouDiZhuMgr.m_IsWatch then
            msgKey="DouDiZhu_Watch_Leave_Game"
        end
        local msg=g_MessageMgr:FormatMessage(msgKey,{})
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            Gac2Gas.RequestLeaveDouDiZhu()
        end), nil, nil, nil, false)
    end)
end
function CLuaMiniDouDiZhuWnd:Init()

    CLuaDouDiZhuMgr.m_CardTemplate=FindChild(self.transform,"CardTemplate").gameObject
    CLuaDouDiZhuMgr.m_CardTemplate:SetActive(false)

    self.m_PlayerViews={}
    for i=1,3 do
        local playerView=CLuaDouDiZhuPlayerView:new()
        local node=FindChild(self.transform,"Player"..tostring(i))
        playerView:Init(node,i)
        table.insert( self.m_PlayerViews, playerView)
    end

    for k,v in pairs(CLuaDouDiZhuMgr.m_Play.m_IndexInfo) do
        local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(k)
        if self.m_PlayerViews[viewIndex] then
            self.m_PlayerViews[viewIndex]:OnSyncDouDiZhuPlayerInfo(v)
            if viewIndex==1 then
                if v.tuoGuan then
                    --托管状态
                    -- self:SetTuoGuanButtonActive(false)
                else
                    -- self:SetTuoGuanButtonActive(true)
                end
            end
        else
            -- print("OnSyncDouDiZhuPlayerInfo error")
            
        end
    end

    if CLuaDouDiZhuMgr.m_ApplyJoinRemoteDouDiZhuPlayerListAlert then
        self:Highlight(true)
    end
end

function CLuaMiniDouDiZhuWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDouDiZhuCardsInfo", self, "OnSyncDouDiZhuCardsInfo")
    g_ScriptEvent:AddListener("SyncDouDiZhuPlayerInfo", self, "OnSyncDouDiZhuPlayerInfo")
    g_ScriptEvent:AddListener("SyncQiangDiZhuProgress", self, "OnSyncQiangDiZhuProgress")
    g_ScriptEvent:AddListener("SyncChuPaiProgress", self, "OnSyncChuPaiProgress")
    g_ScriptEvent:AddListener("PlayerChuPai", self, "OnPlayerChuPai")
    g_ScriptEvent:AddListener("PlayerLeaveDouDiZhu", self, "OnPlayerLeaveDouDiZhu")
    g_ScriptEvent:AddListener("ResetDouDiZhuRound", self, "OnResetDouDiZhuRound")
    g_ScriptEvent:AddListener("DouDiZhuEndRound", self, "OnDouDiZhuEndRound")
    g_ScriptEvent:AddListener("ApplyJoinRemoteDouDiZhuPlayerListAlert", self, "OnApplyJoinRemoteDouDiZhuPlayerListAlert")
    g_ScriptEvent:AddListener("RemoteDouDiZhuShowResult", self, "OnRemoteDouDiZhuShowResult")
end


function CLuaMiniDouDiZhuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDouDiZhuCardsInfo", self, "OnSyncDouDiZhuCardsInfo")
    g_ScriptEvent:RemoveListener("SyncDouDiZhuPlayerInfo", self, "OnSyncDouDiZhuPlayerInfo")
    g_ScriptEvent:RemoveListener("SyncQiangDiZhuProgress", self, "OnSyncQiangDiZhuProgress")
    g_ScriptEvent:RemoveListener("SyncChuPaiProgress", self, "OnSyncChuPaiProgress")
    g_ScriptEvent:RemoveListener("PlayerChuPai", self, "OnPlayerChuPai")
    g_ScriptEvent:RemoveListener("PlayerLeaveDouDiZhu", self, "OnPlayerLeaveDouDiZhu")
    g_ScriptEvent:RemoveListener("ResetDouDiZhuRound", self, "OnResetDouDiZhuRound")
    g_ScriptEvent:RemoveListener("DouDiZhuEndRound", self, "OnDouDiZhuEndRound")
    g_ScriptEvent:RemoveListener("ApplyJoinRemoteDouDiZhuPlayerListAlert", self, "OnApplyJoinRemoteDouDiZhuPlayerListAlert")
    g_ScriptEvent:RemoveListener("RemoteDouDiZhuShowResult", self, "OnRemoteDouDiZhuShowResult")
end
function CLuaMiniDouDiZhuWnd:OnSyncDouDiZhuCardsInfo(index,info,bFaPai)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnSyncDouDiZhuCardsInfo(info,bFaPai)
    end
end

function CLuaMiniDouDiZhuWnd:OnRemoteDouDiZhuShowResult()
    --一局结束 显示结算界面 这个时候倒计时五秒自动进入下一局
    --倒计时
    if self.m_ResultTick then
        UnRegisterTick(self.m_ResultTick)
        self.m_ResultTick=nil
    end
    self.m_LeftTime = CLuaDouDiZhuMgr.m_AutoContinueLeftTime
    self.m_ResultTick = RegisterTickWithDuration(function ()
        self.m_LeftTime = self.m_LeftTime - 1

        if self.m_LeftTime<0 then
            UnRegisterTick(self.m_ResultTick)
            self.m_ResultTick=nil
            CLuaDouDiZhuMgr.CancleAutoContinueCountdown()
            Gac2Gas.RequestDouDiZhuReady(true)
        end
    end,1000,6*1000)
end

function CLuaMiniDouDiZhuWnd:OnSyncQiangDiZhuProgress(index,bIsJiaoDiZhu, leftTime)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    self:Highlight(viewIndex==1)
end

function CLuaMiniDouDiZhuWnd:OnSyncDouDiZhuPlayerInfo(index,info)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    self.m_PlayerViews[viewIndex]:OnSyncDouDiZhuPlayerInfo(info)
end

function CLuaMiniDouDiZhuWnd:OnSyncChuPaiProgress(index,leftTime, maxCardIndex)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnSyncChuPaiProgress(leftTime, maxCardIndex)
        if viewIndex==1 then
            self:Highlight(true)
        else
            self:Highlight(false)
        end
    end
end
function CLuaMiniDouDiZhuWnd:OnPlayerChuPai(index)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    -- print(index,viewIndex)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnPlayerChuPai()
    end
    local info = CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
    if info then
        local cards = info.showCards

        local t = {}
        for i,v in ipairs(cards) do
            local val=math.floor(v/100)
            if val==100 then
                table.insert( t,LocalString.GetString("小王") )
            elseif val==200 then
                table.insert( t,LocalString.GetString("大王") )
            elseif val==11 then
                table.insert( t,"J" )
            elseif val==12 then
                table.insert( t,"Q" )
            elseif val==13 then
                table.insert( t,"K" )
            elseif val==14 then
                table.insert( t,"A" )
            elseif val==20 then
                table.insert( t,"2" )
            else
                table.insert( t,tostring(val) )
            end
        end
        local str = table.concat( t, "-" )
            
        -- g_MessageMgr:ShowMessage("RemoteDouDiZhu_ChuPai_Tip",info.name,str)
    end

end

function CLuaMiniDouDiZhuWnd:Highlight(show)
    self.m_Highlight:SetActive(show)
end

function CLuaMiniDouDiZhuWnd:OnApplyJoinRemoteDouDiZhuPlayerListAlert()
    if CLuaDouDiZhuMgr.m_ApplyJoinRemoteDouDiZhuPlayerListAlert then
        self:Highlight(true)
    end
end


function CLuaMiniDouDiZhuWnd:OnPlayerLeaveDouDiZhu(index)
    local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
    -- print("OnPlayerLeaveDouDiZhu",viewIndex)
    if self.m_PlayerViews[viewIndex] then
        self.m_PlayerViews[viewIndex]:OnPlayerLeaveDouDiZhu()
    end
end


function CLuaMiniDouDiZhuWnd:OnResetDouDiZhuRound()
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnResetDouDiZhuRound()
    end
    --重新显示托管按钮
    -- self:SetTuoGuanButtonActive(true)

    --底牌清掉
    -- self:OnSyncDiZhuReserveCards()
end

function CLuaMiniDouDiZhuWnd:OnDouDiZhuEndRound( winnerIndex )
    --展示底牌
    for i,v in ipairs(self.m_PlayerViews) do
        v:OnDouDiZhuEndRound(winnerIndex)
    end
    self:Highlight(false)
end

function CLuaMiniDouDiZhuWnd:OnDestroy()
    if self.m_ResultTick then
        UnRegisterTick(self.m_ResultTick)
    end
    CLuaGuideMgr.TryEndRemoteDouDiZhuGuide()

    for i,v in ipairs(self.m_PlayerViews) do
        v:OnDestroy()
    end
end

function CLuaMiniDouDiZhuWnd:GetGuideGo(methodName)
    if methodName == "GetRegion" then
        return self.m_Region
    end
    return nil
end