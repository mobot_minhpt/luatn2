local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local ShareInfo = import "NtUniSdk.Unity3d.ShareInfo"
local GameSetting_Share_Wapper = import "L10.Game.GameSetting_Share_Wapper"
local CommonDefs = import "L10.Game.CommonDefs"
local EShareType = import "L10.UI.EShareType"
local HTTPHelper=import "L10.Game.HTTPHelper"
local Main = import "L10.Engine.Main"
local CHTTPForm = import "L10.Game.CHTTPForm"
local WWW = import "UnityEngine.WWW"
local CResourceMgr = import "L10.Engine.CResourceMgr"
LuaShareMoBaiBangMgr = {}
LuaShareMoBaiBangMgr.ServerUrl = "https://god.gameyw.netease.com"
-- "https://god-test.gameyw.netease.com"
--https://god.gameyw.netease.com

LuaShareMoBaiBangMgr.InterfaceUrl = "/v1/app/gameData/getRoleToken"
LuaShareMoBaiBangMgr.token = nil


function LuaShareMoBaiBangMgr.ShareCallback(channel,url)
    if channel == nil or url == nil then
        return
    end

    local shareInfo = ShareInfo()
    shareInfo.shareChannel = channel
    shareInfo.shareType = ShareInfo.TYPE_LINK
    shareInfo.link = GameSetting_Share.GetData().Share_MoBaiBang_Url..WWW.EscapeURL(url)
    shareInfo.title = g_MessageMgr:Format(GameSetting_Share_Wapper.Inst.Share_MoBaiBang_Title, nil)
    shareInfo.Desc = g_MessageMgr:Format(GameSetting_Share_Wapper.Inst.Share_MoBaiBang_Desp, nil)
    if CommonDefs.IsAndroidPlatform() then    
        shareInfo.u3dshareThumb = CResourceMgr.Inst:GetSyncLoadPath("assets/res/syncload/share_thumb/mobaibang.jpg")
    else
        shareInfo.u3dshareThumb = GameSetting_Share_Wapper.Inst.Share_MoBaiBang_Thumb
    end
    shareInfo.image = ""
    shareInfo.u3dShareBitmap = ""
    shareInfo.localImage = ""
    if CommonDefs.IS_KR_CLIENT then
        shareInfo.desc = tostring(EShareType.MoBaiBang)
    end
    SdkU3d.ntShare(shareInfo)
end


function LuaShareMoBaiBangMgr.Share(channel,token)
    if channel == nil or token == nil then
        return
    end
    local url = LuaShareMoBaiBangMgr.ServerUrl..LuaShareMoBaiBangMgr.InterfaceUrl
    local form = CHTTPForm()
    local gmsdkToken = token
    form:AddField("gmsdkToken", gmsdkToken)
    form.m_UseJsonPost = true
    Main.Inst:StartCoroutine(HTTPHelper.DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
            if success then
                local t = luaJson.json2lua(json)           
                local result = t["result"]
                LuaShareMoBaiBangMgr.ShareCallback(channel,result)
            end
        end)))
end

