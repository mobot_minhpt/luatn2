local CUIFx = import "L10.UI.CUIFx"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Profession = import "L10.Game.Profession"
local TweenPosition = import "TweenPosition"
local TweenAlpha = import "TweenAlpha"
local EnumClass = import "L10.Game.EnumClass"

LuaColiseumShowTeamWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaColiseumShowTeamWnd, "BlankBg", "BlankBg", GameObject)
RegistChildComponent(LuaColiseumShowTeamWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaColiseumShowTeamWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaColiseumShowTeamWnd, "LeftView01", "LeftView01", GameObject)
RegistChildComponent(LuaColiseumShowTeamWnd, "RightView01", "RightView01", GameObject)
RegistChildComponent(LuaColiseumShowTeamWnd, "LeftView02", "LeftView02", GameObject)
RegistChildComponent(LuaColiseumShowTeamWnd, "RightView02", "RightView02", GameObject)
RegistChildComponent(LuaColiseumShowTeamWnd, "Fx", "Fx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaColiseumShowTeamWnd,"m_CloseWndTick")
RegistClassMember(LuaColiseumShowTeamWnd,"m_ShowBottomLabelTick")
RegistClassMember(LuaColiseumShowTeamWnd,"m_IsEndShowBottomLabel")
RegistClassMember(LuaColiseumShowTeamWnd,"m_ShowFxTick")
function LuaColiseumShowTeamWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BlankBg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBlankBgClick()
	end)


    --@endregion EventBind end
end

function LuaColiseumShowTeamWnd:OnEnable()
	self:CancelCloseWndTick()
	self:CancelShowBottomLabelTick()
	self.m_CloseWndTick = RegisterTickOnce(function()
		CUIManager.CloseUI(CLuaUIResources.ColiseumShowTeamWnd)
	end,5000)
	self.BottomLabel.gameObject:SetActive(false)
	self.m_ShowBottomLabelTick = RegisterTickOnce(function()
		self.BottomLabel.gameObject:SetActive(true)
		self.m_IsEndShowBottomLabel = true
	end,2000)
end

function LuaColiseumShowTeamWnd:OnDisable()
	self:CancelCloseWndTick()
	self:CancelShowFxTick()
	self:CancelShowBottomLabelTick()
end

function LuaColiseumShowTeamWnd:CancelCloseWndTick()
	if self.m_CloseWndTick then
		UnRegisterTick(self.m_CloseWndTick)
		self.m_CloseWndTick = nil
	end
end

function LuaColiseumShowTeamWnd:CancelShowFxTick()
	if self.m_ShowFxTick then
		UnRegisterTick(self.m_ShowFxTick)
		self.m_ShowFxTick = nil
	end
end

function LuaColiseumShowTeamWnd:CancelShowBottomLabelTick()
	if self.m_ShowBottomLabelTick then
		UnRegisterTick(self.m_ShowBottomLabelTick)
		self.m_ShowBottomLabelTick = nil
	end
end

function LuaColiseumShowTeamWnd:Init()
	self:CancelShowFxTick()
	self.m_ShowFxTick = RegisterTickOnce(function ()
		self.Fx:LoadFx("fx/ui/prefab/UI_Jingji.prefab")
	end, 600)
	self.Template.gameObject:SetActive(false)
	for force = 0,1 do
		local list = LuaColiseumMgr.m_TeamInfo[force]
		if list then
			local viewArr = (#list == 2 or #list == 4) and {self.LeftView02 ,self.RightView02} or {self.LeftView01 ,self.RightView01}
			local view = viewArr[force + 1]
			local arr = {{3},{2,3},{2,3,4},{1,2,3,4},{1,2,3,4,5}}
			for i = 1,#list do
				local root = view.transform:GetChild(arr[#list][i] - 1)
				Extensions.RemoveAllChildren(root.transform)
				local obj = NGUITools.AddChild(root.gameObject, self.Template.gameObject)
				self:InitItem(obj, list[i], i)
			end
		end
	end
end

function LuaColiseumShowTeamWnd:InitItem(obj, data, index)
	local force, playerId, playerName, level, gender, class, hasFeiSheng, isLeader = data.force,data.playerId, data.playerName, data.level, data.gender, data.class, data.hasFeiSheng, data.isLeader
	local tweenDuration = index * 0.1

	local portrait = obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
	local classSprite = obj.transform:Find("Portrait/ClassSprite"):GetComponent(typeof(UISprite))
	local leaderIcon = obj.transform:Find("Portrait/LeaderIcon").gameObject
	local leftView = obj.transform:Find("LeftView").gameObject
	local rightView = obj.transform:Find("RightView").gameObject
	local showView = force == 0 and leftView or rightView
	local nameLabel = showView.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local levelLabel = showView.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local tweenPositon = obj:GetComponent(typeof(TweenPosition))
	local tweenAlpha = obj:GetComponent(typeof(TweenAlpha))

	portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1))
	classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class))
	leaderIcon.gameObject:SetActive(isLeader)
	leftView.gameObject:SetActive(force == 0)
	rightView.gameObject:SetActive(force == 1)
	nameLabel.text = playerName
	nameLabel.color = (CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id) and Color.green or Color.white
	levelLabel.text = SafeStringFormat3("lv.%d", level)
	levelLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	tweenPositon.from = Vector3(force == 0 and - 1024 or 1024,tweenPositon.transform.localPosition.y,0)
	tweenPositon.duration = tweenDuration
	tweenPositon.enabled = true
	tweenAlpha.duration = tweenDuration * 2
	tweenAlpha.enabled = true

	obj.gameObject:SetActive(true)
end

--@region UIEvent

function LuaColiseumShowTeamWnd:OnBlankBgClick()
	if not self.m_IsEndShowBottomLabel then return end
	CUIManager.CloseUI(CLuaUIResources.ColiseumShowTeamWnd)
end

--@endregion UIEvent

