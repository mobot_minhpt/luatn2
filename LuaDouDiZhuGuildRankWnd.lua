local LuaGameObject=import "LuaGameObject"
local CCommonListButtonItem = import "CCommonListButtonItem"
local UIGrid = import "UIGrid"
local CRankData=import "L10.UI.CRankData"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local Profession=import "L10.Game.Profession"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CGuildMgr=import "L10.Game.CGuildMgr"

CLuaDouDiZhuGuildRankWnd=class()

RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_GroupGrid")
RegistClassMember(CLuaDouDiZhuGuildRankWnd, "m_GroupItem")
RegistClassMember(CLuaDouDiZhuGuildRankWnd, "m_Champion_EachSession")
RegistClassMember(CLuaDouDiZhuGuildRankWnd, "m_Champion_Personal")
RegistClassMember(CLuaDouDiZhuGuildRankWnd, "m_Champion_Guild")
RegistClassMember(CLuaDouDiZhuGuildRankWnd, "m_Champion_EachGame")

RegistClassMember(CLuaDouDiZhuGuildRankWnd, "m_ButtonList")
RegistClassMember(CLuaDouDiZhuGuildRankWnd, "m_ShowIndex")

RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableViewDataSource_EachSession")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableView_EachSession")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableViewDataSource_Personal")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableView_Personal")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableViewDataSource_Guild")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableView_Guild")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableViewDataSource_EachGame")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_TableView_EachGame")

RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_MainPlayer_Personal")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_MainPlayer_Guild")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_MainPlayer_EachGame")

RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_RankList_EachSession")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_RankList_Personal")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_RankList_Guild")
RegistClassMember(CLuaDouDiZhuGuildRankWnd,"m_RankList_EachGame")

function CLuaDouDiZhuGuildRankWnd:Init()

	self:InitRankGroups()
	self.m_ShowIndex = -1
    
    self.m_RankList_EachSession = {}
    self.m_RankList_Personal = {}
    self.m_RankList_Guild = {}
    self.m_RankList_EachGame = {}

    local function initItem_EachSession(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem_EachSession(item,index)
    end
    self.m_TableViewDataSource_EachSession=DefaultTableViewDataSource.CreateByCount(#self.m_RankList_EachSession,initItem_EachSession)
    self.m_TableView_EachSession.m_DataSource=self.m_TableViewDataSource_EachSession
    self.m_TableView_EachSession.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow_EachSession(row)
    end)

    local function initItem_Personal(item,index)
    	if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem_Personal(item,index)
    end
    self.m_TableViewDataSource_Personal=DefaultTableViewDataSource.CreateByCount(#self.m_RankList_Personal, initItem_Personal)
    self.m_TableView_Personal.m_DataSource=self.m_TableViewDataSource_Personal
    self.m_TableView_Personal.OnSelectAtRow=DelegateFactory.Action_int(function (row)
    	self:OnSelectAtRow_Personal(row)
    end)

    local function initItem_Guild(item,index)
    	if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem_Guild(item,index)
    end
    self.m_TableViewDataSource_Guild=DefaultTableViewDataSource.CreateByCount(#self.m_RankList_Guild, initItem_Guild)
    self.m_TableView_Guild.m_DataSource=self.m_TableViewDataSource_Guild
    self.m_TableView_Guild.OnSelectAtRow=DelegateFactory.Action_int(function (row)
    	self:OnSelectAtRow_Guild(row)
    end)

    local function initItem_EachGame(item,index)
    	if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem_EachGame(item,index)
    end
    self.m_TableViewDataSource_EachGame=DefaultTableViewDataSource.CreateByCount(#self.m_RankList_EachGame, initItem_EachGame)
    self.m_TableView_EachGame.m_DataSource=self.m_TableViewDataSource_EachGame
    self.m_TableView_EachGame.OnSelectAtRow=DelegateFactory.Action_int(function (row)
    	self:OnSelectAtRow_EachGame(row)
    end)

    if CClientMainPlayer.Inst:IsInGuild() and not CGuildMgr.Inst.m_GuildInfo then
		Gac2Gas.RequestGetGuildInfo(CGuildMgr.RPC_TYPE_MyGuildInfo)
	end

	self:OnRankItemClicked(1)
end

-----------------------------初始化item----------------------------------
-- 初始化历届冠军item
function CLuaDouDiZhuGuildRankWnd:InitItem_EachSession(item,index)
    local tf=item.transform
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"CheckLabel").label
    timeLabel.text=""
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=" "
    local jobSprite =LuaGameObject.GetChildNoGC(tf,"JobIcon").sprite
    jobSprite.spriteName=""
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"NumLabel").label
    rankLabel.text=""
    
    local info = self.m_RankList_EachSession[index+1]
    rankLabel.text=SafeStringFormat3(LocalString.GetString("第%d届"), info.Index)
    nameLabel.text=info.Name
    jobSprite.spriteName=Profession.GetIconByNumber(info.Class)
    timeLabel.text=info.GuildName
end

-- 初始化千王之王item
function CLuaDouDiZhuGuildRankWnd:InitItem_Personal(item,index)
    local tf=item.transform
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"CheckLabel").label
    timeLabel.text=""
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=" "
    local jobSprite =LuaGameObject.GetChildNoGC(tf,"JobIcon").sprite
    jobSprite.spriteName=""
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"NumLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    rankSprite.spriteName=""
    
    local info=self.m_RankList_Personal[index]
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.Name
    jobSprite.spriteName=Profession.GetIcon(info.Job)
    timeLabel.text=info.Value
end

--初始化满贯之帮item
function CLuaDouDiZhuGuildRankWnd:InitItem_Guild(item,index)
    local tf=item.transform
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"CheckLabel").label
    timeLabel.text=""
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=" "
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"NumLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    rankSprite.spriteName=""
    
    local info=self.m_RankList_Guild[index]
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.Guild_Name
    timeLabel.text=info.Value
end

--初始化牌神降世item
function CLuaDouDiZhuGuildRankWnd:InitItem_EachGame(item,index)
    local tf=item.transform
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"CheckLabel").label
    timeLabel.text=""
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=" "
    local jobSprite =LuaGameObject.GetChildNoGC(tf,"JobIcon").sprite
    jobSprite.spriteName=""
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"NumLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    rankSprite.spriteName=""
    
    local info=self.m_RankList_EachGame[index]
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.Name
    jobSprite.spriteName=Profession.GetIcon(info.Job)
    timeLabel.text=info.Value
end
-----------------------------end 初始化item----------------------------------


-----------------------------点击item----------------------------------
function CLuaDouDiZhuGuildRankWnd:OnSelectAtRow_EachSession(row)
    local data=self.m_RankList_EachSession[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end

function CLuaDouDiZhuGuildRankWnd:OnSelectAtRow_Personal(row)
    local data=self.m_RankList_Personal[row]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end

function CLuaDouDiZhuGuildRankWnd:OnSelectAtRow_Guild(row)
    --[[local data=self.m_RankList_Guild[row]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end--]]
end

function CLuaDouDiZhuGuildRankWnd:OnSelectAtRow_EachGame(row)
    local data=self.m_RankList_EachGame[row]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end
-----------------------------end 点击item----------------------------------



function CLuaDouDiZhuGuildRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:AddListener("QueryGuildDouDiZhuChampionRecordResult", self, "OnChampionRecordResult")
end

function CLuaDouDiZhuGuildRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:RemoveListener("QueryGuildDouDiZhuChampionRecordResult", self, "OnChampionRecordResult")
end

-- 处理千王之王、满贯之帮、牌神降世三个排行榜
function  CLuaDouDiZhuGuildRankWnd:OnRankDataReady(args)
    local myInfo=CRankData.Inst.MainPlayerRankInfo
    if myInfo.rankId == 41000157 then
    	-- 千王之王
    	local tf=self.m_MainPlayer_Personal.transform
    	local timeLabel=LuaGameObject.GetChildNoGC(tf,"MyTimeLabel").label
    	timeLabel.text=myInfo.Value
    	local nameLabel=LuaGameObject.GetChildNoGC(tf,"MyNameLabel").label
    	nameLabel.text= myInfo.Name
    	if CClientMainPlayer.Inst then
        	nameLabel.text= CClientMainPlayer.Inst.Name
    	end
    	local jobSprite =LuaGameObject.GetChildNoGC(tf,"JobIcon").sprite
    	jobSprite.spriteName=Profession.GetIcon(CClientMainPlayer.Inst.Class)
    	local rankLabel=LuaGameObject.GetChildNoGC(tf,"MyRankLabel").label
    	rankLabel.text="" 
    	local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    	rankSprite.spriteName=""
    
    	local rank=myInfo.Rank	

    	if rank>0 then
        	rankLabel.text=tostring(rank)
    	else
        	rankLabel.text=LocalString.GetString("未上榜")
    	end

    	self.m_RankList_Personal=CRankData.Inst.RankList
    	self.m_TableViewDataSource_Personal.count=self.m_RankList_Personal.Count
    	self.m_TableView_Personal:ReloadData(true,false)

    elseif myInfo.rankId == 41100017 then
    	-- 满贯之帮
    	local tf=self.m_MainPlayer_Guild.transform
    	local timeLabel=LuaGameObject.GetChildNoGC(tf,"MyTimeLabel").label
    	timeLabel.text=myInfo.Value
    	local nameLabel=LuaGameObject.GetChildNoGC(tf,"MyNameLabel").label
        nameLabel.text= myInfo.Guild_Name
        if CGuildMgr.Inst.m_GuildInfo then
        	nameLabel.text=CGuildMgr.Inst.m_GuildInfo.Name
        end
    	local rankLabel=LuaGameObject.GetChildNoGC(tf,"MyRankLabel").label
    	rankLabel.text=""
    	local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    	rankSprite.spriteName=""
    
    	local rank=myInfo.Rank

    	if rank>0 then
        	rankLabel.text=tostring(rank)
    	else
        	rankLabel.text=LocalString.GetString("未上榜")
    	end

    	self.m_RankList_Guild=CRankData.Inst.RankList
    	self.m_TableViewDataSource_Guild.count=self.m_RankList_Guild.Count
    	self.m_TableView_Guild:ReloadData(true,false)
    elseif myInfo.rankId == 41000158 then
    	-- 牌神降世
    	local tf=self.m_MainPlayer_EachGame.transform
    	local timeLabel=LuaGameObject.GetChildNoGC(tf,"MyTimeLabel").label
    	timeLabel.text=myInfo.Value
    	local nameLabel=LuaGameObject.GetChildNoGC(tf,"MyNameLabel").label
    	nameLabel.text= myInfo.Name
    	if CClientMainPlayer.Inst then
        	nameLabel.text= CClientMainPlayer.Inst.Name
    	end
    	local jobSprite =LuaGameObject.GetChildNoGC(tf,"JobIcon").sprite
    	jobSprite.spriteName=Profession.GetIcon(CClientMainPlayer.Inst.Class)
    	local rankLabel=LuaGameObject.GetChildNoGC(tf,"MyRankLabel").label
    	rankLabel.text=""
    	local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    	rankSprite.spriteName=""
    
    	local rank=myInfo.Rank
    	if rank>0 then
    		rankLabel.text=tostring(rank)
    	else
        	rankLabel.text=LocalString.GetString("未上榜")
    	end
    	self.m_RankList_EachGame=CRankData.Inst.RankList
    	self.m_TableViewDataSource_EachGame.count=self.m_RankList_EachGame.Count
    	self.m_TableView_EachGame:ReloadData(true,false)
    end
end

-- 处理历届冠军排行榜
function CLuaDouDiZhuGuildRankWnd:OnChampionRecordResult(args)
	self.m_RankList_EachSession = CLuaDouDiZhuMgr.m_ChampionRecord
	self.m_TableViewDataSource_EachSession.count=#self.m_RankList_EachSession
	self.m_TableView_EachSession:ReloadData(true,false)
end

-- 初始化左边的四个选项
function CLuaDouDiZhuGuildRankWnd:InitRankGroups()
	self.m_ButtonList = {}
	self.m_GroupItem:SetActive(false)

 	local groupNames = {LocalString.GetString("历届冠军"), LocalString.GetString("千王之王"), LocalString.GetString("满贯之帮"), LocalString.GetString("牌神降世")}
 	for k, v in ipairs(groupNames) do
 		local instance = CUICommonDef.AddChild(self.m_GroupGrid, self.m_GroupItem)
 		instance:SetActive(true)
 		local item = CommonDefs.GetComponent_GameObject_Type(instance.gameObject, typeof(CCommonListButtonItem))
 		item:SetNameLabel(v)
 		item.Index = k
 		item.clickAction = DelegateFactory.Action_int(function (index)
 			self:OnRankItemClicked(index)
 		end)

 		if item ~= nil then
 			table.insert(self.m_ButtonList, item)
 		end
 	end

 	local grid = CommonDefs.GetComponent_GameObject_Type(self.m_GroupGrid, typeof(UIGrid))
 	if grid then
 		grid:Reposition()
 	end
end

function CLuaDouDiZhuGuildRankWnd:OnRankItemClicked(index)
	if self.m_ShowIndex == index then
		return
	end
	self.m_ShowIndex = index

	for k, v in ipairs(self.m_ButtonList) do
		v:SetHighLight(index == v.Index)
	end
	
	if index == 1 then
		self:ShowChampionEachSession()
	elseif index == 2 then
		self:ShowChampionPersonal()
	elseif index == 3 then
		self:ShowChampionGuild()
	elseif index == 4 then
		self:ShowChampionEachGame()
	end

end

-- 历届冠军
function CLuaDouDiZhuGuildRankWnd:ShowChampionEachSession()
	self.m_Champion_EachSession:SetActive(true)
	self.m_Champion_Personal:SetActive(false)
	self.m_Champion_Guild:SetActive(false)
	self.m_Champion_EachGame:SetActive(false)
	Gac2Gas.QueryGuildDouDiZhuChampionRecord()
end

-- 千王之王
function CLuaDouDiZhuGuildRankWnd:ShowChampionPersonal()
	self.m_Champion_EachSession:SetActive(false)
	self.m_Champion_Personal:SetActive(true)
	self.m_Champion_Guild:SetActive(false)
	self.m_Champion_EachGame:SetActive(false)
	Gac2Gas.QueryRank(41000157)
end

-- 满贯之帮
function CLuaDouDiZhuGuildRankWnd:ShowChampionGuild()
	self.m_Champion_EachSession:SetActive(false)
	self.m_Champion_Personal:SetActive(false)
	self.m_Champion_Guild:SetActive(true)
	self.m_Champion_EachGame:SetActive(false)
	Gac2Gas.QueryRank(41100017)
end

-- 牌神降世
function CLuaDouDiZhuGuildRankWnd:ShowChampionEachGame()
	self.m_Champion_EachSession:SetActive(false)
	self.m_Champion_Personal:SetActive(false)
	self.m_Champion_Guild:SetActive(false)
	self.m_Champion_EachGame:SetActive(true)
	Gac2Gas.QueryRank(41000158)
end

return CLuaDouDiZhuGuildRankWnd