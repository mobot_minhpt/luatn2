local CScene = import "L10.Game.CScene"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CMainCamera = import "L10.Engine.CMainCamera"
local GameObject = import "UnityEngine.GameObject"
local Color = import "UnityEngine.Color"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local Setting = import "L10.Engine.Setting"
local PrimitiveType = import "UnityEngine.PrimitiveType"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local BoxCollider = import "UnityEngine.BoxCollider"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local Shader = import "UnityEngine.Shader"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local MeshFilter = import "UnityEngine.MeshFilter"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local MeshCollider = import "UnityEngine.MeshCollider"
local Material = import "UnityEngine.Material"
local CPos = import "L10.Engine.CPos"

LuaZhongQiu2022Mgr = {}
LuaZhongQiu2022Mgr.m_RowNum = 17 --每行多少格
LuaZhongQiu2022Mgr.m_ColNum = 17 --每列多少格
--[[
下标范围1 ~ RowNum or ColNum
0,0 ——————————→x(行坐标)
|
|
|
|
↓
y(列坐标)
--]]
LuaZhongQiu2022Mgr.m_OriginPoint = { x = 46, y = 53 }
LuaZhongQiu2022Mgr.m_XExtend = -1
LuaZhongQiu2022Mgr.m_YExtend = -1

function LuaZhongQiu2022Mgr:GetIndexByGrid(i, j) -- 得到棋盘的压缩网格坐标
    if i < 1 or i > self.m_RowNum or j < 1 or j > self.m_ColNum then
        return -1
    end
    return (i - 1) * self.m_ColNum + j
end

function LuaZhongQiu2022Mgr:GetGridByIndex(index) -- 得到棋盘的网格坐标 
    local i = math.floor(index / self.m_ColNum) + 1
    local j = index % self.m_ColNum
    if j < 1 then
        i = i - 1
        j = self.m_ColNum
    end
    return i, j
end

function LuaZhongQiu2022Mgr:GetPosByGrid(i, j) -- 得到棋盘网格坐标对应的地图实际网格坐标
    local x_shift = math.floor(j / 2) * self.m_XExtend
    local y_shift = math.floor(i / 2) * self.m_YExtend
    return self.m_OriginPoint.x + x_shift, self.m_OriginPoint.y + y_shift
end

function LuaZhongQiu2022Mgr:GetNGUIPosByGrid(i, j) -- 得到棋盘网格坐标对应的NGUI屏幕坐标
    local pos = CMainCamera.Main:WorldToScreenPoint(Utility.GridPos2WorldPos(self:GetPosByGrid(i, j)))
    pos.z = 0
    return UICamera.currentCamera:ScreenToWorldPoint(pos)
end

function LuaZhongQiu2022Mgr:GetGridByNGUIPos(pos)  -- 得到NGUI坐标对应的棋盘网格坐标
    local screenPos = UICamera.currentCamera:WorldToScreenPoint(pos)
    screenPos.z = 1
    local pixelPos = Utility.WorldPos2PixelPos(Utility.ScreenPoint2WorldPos(screenPos, CMainCamera.Main))
    --local offsetX = Utility.GetPixelOffset(pixelPos.x)
    --local offsetY = Utility.GetPixelOffset(pixelPos.y)
    local pos = Utility.PixelPos2GridPos(pixelPos)
    --if offsetX >= 0.5 * Setting.eGridSpan then pos.x = pos.x + 1 end
    --if offsetY >= 0.5 * Setting.eGridSpan then pos.y = pos.y + 1 end
    pos.x = (pos.x - self.m_OriginPoint.x) / self.m_XExtend
    pos.y = (pos.y - self.m_OriginPoint.y) / self.m_YExtend
    return pos.y * 2, pos.x * 2
end

function LuaZhongQiu2022Mgr:HighlightGrid(red, i, j) -- 高亮棋盘网格坐标
    if not self.bIsGameStart or self.bIsGameEnd then return end
    local obj = self.GridObjs[self:GetIndexByGrid(i, j)]
    obj:GetComponent(typeof(MeshRenderer)).enabled = true
    if red then
        local col = NGUIText.ParseColor("ff4a81", 0)
        obj:GetComponent(typeof(MeshRenderer)).sharedMaterial.color = Color(col.r, col.g, col.b, 153 / 255)
    else
        local col = NGUIText.ParseColor("31e470", 0)
        obj:GetComponent(typeof(MeshRenderer)).sharedMaterial.color = Color(col.r, col.g, col.b, 153 / 255)
    end
end

function LuaZhongQiu2022Mgr:Init()

end

function LuaZhongQiu2022Mgr:IsInPlay()
    local gamePlayId = CScene.MainScene and CScene.MainScene.GamePlayDesignId or 0
    local sceneTemplateId = CScene.MainScene and CScene.MainScene.SceneTemplateId or 0
    if gamePlayId == ZhongQiu2022_Setting.GetData().MoonCakeGamePlayId and sceneTemplateId == ZhongQiu2022_Setting.GetData().MoonCakePublicMapId then
        return true
    end
    return false
end

function LuaZhongQiu2022Mgr:ClearGridObjs(destroy)
    for _, obj in pairs(self.GridObjs) do
        if not CommonDefs.IsNull(obj) then
            if destroy then
                obj:Destroy()
            else
                obj:GetComponent(typeof(MeshRenderer)).enabled = false
            end
        end
    end
    if destroy then
        self.GridObjs = {}
    end
end

function LuaZhongQiu2022Mgr:GridClickFunc(go)
    if not self.bIsGameStart or self.bIsGameEnd or CUIManager.IsLoaded(CUIResources.SocialWnd) then return end
    local x, y = self:GetGridByIndex(tonumber(go.name))
    local meshR = go:GetComponent(typeof(MeshRenderer))
    local p1X, p1Y = self:GetGridByIndex(self.PlayerPos[self.MyRole])
    local p2X, p2Y = self:GetGridByIndex(self.PlayerPos[1 - self.MyRole])
    if not meshR.enabled then
        if x == p1X and y == p1Y or x == p2X and y == p2Y then
            g_ScriptEvent:BroadcastInLua("JYDSY_ShowBubbleMsg", 15)
        elseif math.abs(math.floor(x/2) - math.floor(p1X/2)) + math.abs(math.floor(y/2) - math.floor(p1Y/2)) == 1 then
            g_ScriptEvent:BroadcastInLua("JYDSY_ShowBubbleMsg", 11)
        else
            g_ScriptEvent:BroadcastInLua("JYDSY_ShowBubbleMsg", 12)
        end
    else
        Gac2Gas.JYDSY_RequestMoveToGrid(x, y)
    end
end

function LuaZhongQiu2022Mgr:AddTargetFx(fxId, x, y)
    local pos = Utility.GridPos2PixelPos(CPos(self:GetPosByGrid(x, y)))
    local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId,pos,0,0,1,-1,EnumWarnFXType.None,nil,0,0,
        DelegateFactory.Action_GameObject(function(fxGo) 
            --LuaUtils.SetLocalScale(fxGo.transform, 0.3, 0.7, 0.3)
            --LuaUtils.SetLocalPositionY(fxGo.transform:Find("jiantou"), 8.8)
        end))
end

function LuaZhongQiu2022Mgr:OnMainPlayerCreated()
    if self:IsInPlay() then
        local gridsRoot = CRenderScene.Inst.transform:Find("OtherObjects/GridsRoot")
        if not gridsRoot then
            gridsRoot = CreateFromClass(GameObject, "GridsRoot")
            gridsRoot.transform.parent = CRenderScene.Inst.transform:Find("OtherObjects")
        end
        local template = CRenderScene.Inst.transform:Find("OtherObjects/GridsRoot/Template")
        if not template then
            template = GameObject.CreatePrimitive(PrimitiveType.Plane)
            template.name = "Template"
            template.transform.parent = gridsRoot.transform
        end
        local mesh = template:GetComponent(typeof(MeshFilter)).sharedMesh
        self:ClearGridObjs(true)
        for x = 1, self.m_RowNum, 2 do
            for y = 1, self.m_ColNum, 2 do
                local idx = self:GetIndexByGrid(x, y)

                --local ro = CRenderObject.CreateRenderObject(gridsRoot, tostring(idx))
                local obj = CreateFromClass(GameObject, tostring(idx))
                obj.transform.parent = gridsRoot.transform
                local ro = obj:AddComponent(typeof(CRenderObject))
                --ro.Parent = obj
                --ro.m_FormerParent = obj
                ro.IsImportant = false
                ro.HasFeisheng = false
                ro.IsKickOuting = false
                ro.CanBeJob = false
                
                obj.transform.position = Utility.GridPos2WorldPos(self:GetPosByGrid(x, y))
                obj.transform.position = Vector3(obj.transform.position.x + 0.5, 17, obj.transform.position.z + 0.5)
                LuaUtils.SetLocalScale(obj.transform, 0.1, 0.1, 0.1)

                local mf = obj:AddComponent(typeof(MeshFilter))
                local mr = obj:AddComponent(typeof(MeshRenderer))
                --obj:AddComponent(typeof(BoxCollider))
                local mc = obj:AddComponent(typeof(MeshCollider))

                mf.sharedMesh = mesh
                mc.sharedMesh = mesh
                mr.material = CreateFromClass(Material, Shader.Find("Sprites/Default")) -- 仅在初始化时创建一份新的材质
                
                mr.enabled = false

                --mr.sharedMaterial:SetFloat("_Mode", 1)
                --mr.sharedMaterial:SetOverrideTag("RenderType", "Transparent")
                --mr.sharedMaterial:SetInt("_SrcBlend", 5)
                --mr.sharedMaterial:SetInt("_DstBlend", 10)
                --mr.sharedMaterial:SetInt("_ZWrite", 0)
                --mr.sharedMaterial:DisableKeyword("_ALPHATEST_ON")
                --mr.sharedMaterial:EnableKeyword("_ALPHABLEND_ON")
                --mr.sharedMaterial:DisableKeyword("_ALPHAPREMULTIPLY_ON")
                --mr.sharedMaterial.renderQueue = 3000
                --mr.sharedMaterial:SetFloat("_Mode", 2)

                mr.sharedMaterial.color = Color(0, 0, 0, 0)

                self.GridObjs[idx] = ro
            end
        end
        self:ShowNextMove() -- for dx

		CUIManager.ShowUI(CLuaUIResources.JiaoYueDuoSuYiInGameWnd)
    else
        self.bIsGameEnd = false
        self.bIsGameStart = false
        self.bIsOnMove = false
    end
end

function LuaZhongQiu2022Mgr:OnMainPlayerDestroyed()
    self:ClearGridObjs(true)
end

function LuaZhongQiu2022Mgr:ShowNextMove()
    if not self.bIsGameStart or self.bIsGameEnd then return end
    if self.bIsOnMove then
        for i = 1, #self.NextMoves do
            self:HighlightGrid(false, self:GetGridByIndex(self.NextMoves[i]))
        end
    end
end

LuaZhongQiu2022Mgr.bIsOnMove = false
LuaZhongQiu2022Mgr.NextMoves = {}
LuaZhongQiu2022Mgr.BarrierNum = {}
LuaZhongQiu2022Mgr.PlayerPos = {}
LuaZhongQiu2022Mgr.GridObjs = {}
function LuaZhongQiu2022Mgr:JYDSY_StartNextRound(roundTime, barrierAndPosInfo_U, bIsOnMove, nextMoves_U)
    -- barrierAndPosInfo = dict{ playerId: list[ barrierNum, playerPosition ] }
    -- nextMoves = list[ canMovePosition, ... ]
    print("JYDSY_StartNextRound", roundTime, bIsOnMove)

    if bIsOnMove then
        local x = self.MyRole == 0 and 1 or 17
        for y = 1, 17, 2 do
            self:AddTargetFx(88803574, x, y)
        end
    end

    self.bIsGameStart = true
    self.bIsOnMove = bIsOnMove
    self.NextMoves = g_MessagePack.unpack(nextMoves_U)

    local barrierAndPosInfo = g_MessagePack.unpack(barrierAndPosInfo_U)
    
    self.BarrierNum = {}
    self.PlayerPos = {}

    for playerId, info in pairs(barrierAndPosInfo) do
        self.BarrierNum[self.PlayerInfo[playerId].role] = info[1]
        self.PlayerPos[self.PlayerInfo[playerId].role] = info[2]
    end

    g_ScriptEvent:BroadcastInLua("JYDSY_SyncRoundTime", roundTime)
    g_ScriptEvent:BroadcastInLua("JYDSY_UpdateRoundInfo")

    self:ClearGridObjs()
    self:ShowNextMove()
end

LuaZhongQiu2022Mgr.bIsGameEnd = false
LuaZhongQiu2022Mgr.bIsGameStart = false
function LuaZhongQiu2022Mgr:JYDSY_GameEndSyncResult(winnerId)
    print("JYDSY_GameEndSyncResult", winnerId)
    self.bIsGameEnd = true
    self:ClearGridObjs()
    LuaJiaoYueDuoSuYiResultWnd.s_IsWin = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == winnerId
    CUIManager.ShowUI(CLuaUIResources.JiaoYueDuoSuYiResultWnd)
end

function LuaZhongQiu2022Mgr:JYDSY_SyncRoundTime(roundTime)
    --print("JYDSY_SyncRoundTime", roundTime)
    g_ScriptEvent:BroadcastInLua("JYDSY_SyncRoundTime", roundTime)
end

LuaZhongQiu2022Mgr.PlayerInfo = {}
LuaZhongQiu2022Mgr.MyRole = 0 -- 0:right 1:left
function LuaZhongQiu2022Mgr:JYDSY_SyncPlayerInfo(playerInfo_U)
    -- dict{ playerId: list[ name, class, gender, barrierNum, role ] }
    print("JYDSY_SyncPlayerInfo")

    self.PlayerInfo = {}
    local data = g_MessagePack.unpack(playerInfo_U)
    for playerId, info in pairs(data) do
        self.PlayerInfo[playerId] = {
            name = info[1],
            cls = info[2],
            gender = info[3],
            barrierNum = info[4],
            role = info[5],
            lv = info[6],
            server = info[7]
        }
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
            self.MyRole = info[5]
        end
    end

    g_ScriptEvent:BroadcastInLua("JYDSY_SyncPlayerInfo")
end

function LuaZhongQiu2022Mgr:GetPlayerInfo(role)
    for _, info in pairs(self.PlayerInfo) do
        if info.role == role then
            return info
        end
    end
end

LuaZhongQiu2022Mgr.BarrierInfo = {}
function LuaZhongQiu2022Mgr:JYDSY_SyncBarrierInfo(boardBarrierInfo_U)
    -- list[ barrierPosition, ... ]
    print("JYDSY_SyncBarrierInfo")
    local info = g_MessagePack.unpack(boardBarrierInfo_U)
    --g_ScriptEvent:BroadcastInLua("JYDSY_SyncBarrierInfo", g_MessagePack.unpack(boardBarrierInfo_U))

    self.BarrierInfo = {}
    for i = 1, #info do
        local x, y = self:GetGridByIndex(info[i]) 
        if not self.BarrierInfo[x] then
            self.BarrierInfo[x] = {}
        end
        self.BarrierInfo[x][y] = true
    end
end

function LuaZhongQiu2022Mgr:CheckOverlap(isVertical, x, y, showMsg)
    local ok = true
    if  self.BarrierInfo[x] and self.BarrierInfo[x][y] or 
        isVertical and self.BarrierInfo[x] and self.BarrierInfo[x][y + 2] or
        not isVertical and self.BarrierInfo[x + 2] and self.BarrierInfo[x + 2][y]
    then
        ok = false
    elseif isVertical then
        local cnt = 1
        while x - cnt > 0 and self.BarrierInfo[x - cnt] and self.BarrierInfo[x - cnt][y + 1] do
            cnt = cnt + 2
        end
        if (math.floor(cnt / 2) + 1) % 2 == 0 then ok = false end
    elseif not isVertical then
        local cnt = 1
        while y - cnt > 0 and self.BarrierInfo[x + 1] and self.BarrierInfo[x + 1][y - cnt] do
            cnt = cnt + 2
        end
        if (math.floor(cnt / 2) + 1) % 2 == 0 then ok = false end
    end
    if showMsg and not ok then
        g_ScriptEvent:BroadcastInLua("JYDSY_ShowBubbleMsg", 8)
    end
    return ok
end

function LuaZhongQiu2022Mgr:CheckBoundary(isVertical, x, y, showMsg)
    local dir
    if isVertical then 
        dir = { {0, 2}, {1, 0}, {-1, 0}, {1, 2}, {-1, 2} }
    else
        dir = { {0, 0}, {2, -1}, {0, -1}, {2, 1}, {0, 1} }
    end
    local ok = true
    local takeBack = true
    for k = 1, 5 do
        local nx = x + dir[k][1]
        local ny = y + dir[k][2]
        if nx < 1 or nx > self.m_RowNum or ny < 1 or ny > self.m_ColNum then 
            ok = false
        else
            takeBack = false
        end
    end
    if showMsg and not ok and not takeBack then
        g_ScriptEvent:BroadcastInLua("JYDSY_ShowBubbleMsg", 9)
    end
    return ok
end

function LuaZhongQiu2022Mgr:JYDSY_ShowBubbleMsg(msgId)
    print("JYDSY_ShowBubbleMsg", msgId)
    g_ScriptEvent:BroadcastInLua("JYDSY_ShowBubbleMsg", msgId)
end
