local UITable = import "UITable"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"

LuaAppRateWindow = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaAppRateWindow, "GiftTemplate", "GiftTemplate", GameObject)
RegistChildComponent(LuaAppRateWindow, "RateButton", "RateButton", GameObject)
RegistChildComponent(LuaAppRateWindow, "Charge", "Charge", GameObject)
RegistChildComponent(LuaAppRateWindow, "GiftTable", "GiftTable", UITable)

--@endregion RegistChildComponent end

function LuaAppRateWindow:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    
    self.itemList = {}
    local voteAwardData = GameSetting_Client.GetData().SEA_VOTE_AWARD
    local dataSplit = g_LuaUtil:StrSplit(voteAwardData, ";")
    for i = 1, #dataSplit do
        if (dataSplit[i] ~= "") then
            local itemSplit = g_LuaUtil:StrSplit(dataSplit[i], ",")
            table.insert(self.itemList, {itemId = tonumber(itemSplit[1]), itemNum = tonumber(itemSplit[2])})
        end
    end
    
    CommonDefs.AddOnClickListener(self.RateButton, DelegateFactory.Action_GameObject(function (go)
        if CommonDefs.Is_PC_PLATFORM() then
            g_MessageMgr:ShowCustomMsg(LocalString.GetString("请在移动端参加活动"))
        else
            if CommonDefs.IsAndroidPlatform() then
                CommonDefs.OpenURL("https://play.google.com/store/apps/details?id=com.vnggames.ghoststory")
            else
                SdkU3d.ntExtendFunc("{\"methodId\":\"storeRating\"}")
            end
            Gac2Gas.RequestVNClientAward("AV")
        end
    end), false)
    self:CreateRateRewardItem()
end

function LuaAppRateWindow:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaAppRateWindow:CreateRateRewardItem()
    CUICommonDef.ClearTransform(self.GiftTable.transform)

    for i = 1, #self.itemList do
        local itemId = self.itemList[i].itemId
        local count = self.itemList[i].itemNum
        local go = NGUITools.AddChild(self.GiftTable.gameObject, self.GiftTemplate)
        self:InitAwardItem(go, itemId, count)
        go:SetActive(true)
    end
    
    self.GiftTable:Reposition()
end

function LuaAppRateWindow:InitAwardItem(itemObj, itemId, count)
    if not itemObj then return end

    local icon = itemObj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local amount = itemObj.transform:Find("AmountLable"):GetComponent(typeof(UILabel))
    local quality = itemObj.transform:Find("Quality"):GetComponent(typeof(UISprite))

    local item = Item_Item.GetData(itemId)
    if not item then return end

    icon:LoadMaterial(item.Icon)
    amount.text = tostring(count)
    quality.spriteName = CUICommonDef.GetItemCellBorder(item.NameColor)

    CommonDefs.AddOnClickListener(itemObj, DelegateFactory.Action_GameObject(function (gameobject)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end
