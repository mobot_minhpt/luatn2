local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaFSCCardHeadshot = class()

RegistClassMember(LuaFSCCardHeadshot, "m_RoleTex")
RegistClassMember(LuaFSCCardHeadshot, "m_CardBg")
RegistClassMember(LuaFSCCardHeadshot, "m_Empty")

RegistClassMember(LuaFSCCardHeadshot, "m_Id")
RegistClassMember(LuaFSCCardHeadshot, "m_IsRare")
RegistClassMember(LuaFSCCardHeadshot, "m_Status")
RegistClassMember(LuaFSCCardHeadshot, "m_Borrowed")
RegistClassMember(LuaFSCCardHeadshot, "m_Data")

function LuaFSCCardHeadshot:Awake()
    self.m_RoleTex = self.transform:Find("Role"):GetComponent(typeof(CUITexture))
    self.m_CardBg = self.transform:Find("BG")
    self.m_Empty = self.transform:Find("Empty").gameObject

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClick()
    end)
end

function LuaFSCCardHeadshot:SetActive(active)
    for i = 0, self.transform.childCount - 1 do
        self.transform:GetChild(i).gameObject:SetActive(active)
    end
end

function LuaFSCCardHeadshot:SetBg()
    self.m_CardBg.gameObject:SetActive(true)

    local quality = self.m_CardBg:Find("Quality")
    local season = self.m_CardBg:Find("Season")
    local banned = self.m_CardBg:Find("Banned")

    quality.gameObject:SetActive(true)
    quality:Find("Rare").gameObject:SetActive(self.m_IsRare)
    quality:Find("Normal").gameObject:SetActive(not self.m_IsRare)
    season.gameObject:SetActive(true)
    for i = 1, 4 do
        season:Find(LuaFourSeasonCardMgr:GetSeasonName(i)).gameObject:SetActive(self.m_Data.Season == i)
    end
    banned.gameObject:SetActive(self.m_Status == EnumFSC_CardStatus.Banned)

    if self.m_Status == EnumFSC_CardStatus.NotInHand then
        CUICommonDef.SetActive(quality.gameObject, false)
        CUICommonDef.SetActive(self.m_RoleTex.gameObject, false)
    end
end

function LuaFSCCardHeadshot:Init(cardId, isRare, status, borrowed)
    if self.m_Id == cardId and self.m_IsRare == isRare and self.m_Status == status then
        return 
    end
    self:SetActive(false)
    self.m_Id = cardId or 0
    self.m_IsRare = isRare
    self.m_Status = status
    self.m_Borrowed = borrowed
    self.m_Data = LuaFourSeasonCardMgr:GetCardData(cardId) or {}

    if self.m_Status == EnumFSC_CardStatus.Empty then
        self.m_Empty:SetActive(true)
    else
        self.m_RoleTex.gameObject:SetActive(true)
        self.m_RoleTex:LoadMaterial(LuaFourSeasonCardMgr:GetCardData(self.m_Id).SmallPicture)
        self:SetBg()
    end
end

function LuaFSCCardHeadshot:IsInCardPileWnd()
    local obj = CUIManager.instance:GetGameObject(CLuaUIResources.FSCCardPileWnd)
    return obj and NGUITools.IsChild(obj.transform, self.transform)
end

function LuaFSCCardHeadshot:OnClick()
    if self:IsInCardPileWnd() then
        LuaFSCCardDetailWnd.s_CardId = self.m_Id
        LuaFSCCardDetailWnd.s_IsRare = self.m_IsRare
        LuaFSCCardDetailWnd.s_Status = self.m_Status
        LuaFSCCardDetailWnd.s_Borrowed = self.m_Borrowed
        CUIManager.ShowUI(CLuaUIResources.FSCCardDetailWnd)
    end
end
