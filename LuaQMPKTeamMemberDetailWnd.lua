local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
LuaQMPKTeamMemberDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKTeamMemberDetailWnd, "TeamName", "TeamName", UILabel)
RegistChildComponent(LuaQMPKTeamMemberDetailWnd, "TeamSlogan", "TeamSlogan", UILabel)
RegistChildComponent(LuaQMPKTeamMemberDetailWnd, "TeamGroup", "TeamGroup", UILabel)
RegistChildComponent(LuaQMPKTeamMemberDetailWnd, "Table", "Table", GameObject)
RegistChildComponent(LuaQMPKTeamMemberDetailWnd, "PlayerTemplate", "PlayerTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKTeamMemberDetailWnd, "m_Force")
RegistClassMember(LuaQMPKTeamMemberDetailWnd, "m_Data")
RegistClassMember(LuaQMPKTeamMemberDetailWnd, "m_MemberViewList")
RegistClassMember(LuaQMPKTeamMemberDetailWnd, "m_Round")
RegistClassMember(LuaQMPKTeamMemberDetailWnd, "m_QueryTick")
function LuaQMPKTeamMemberDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.m_Data = nil
    self.m_Force = nil
    self.m_Round = 0
    self.m_MemberViewList = nil
    self.m_QueryTick = nil
end

function LuaQMPKTeamMemberDetailWnd:Init()
    self.PlayerTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.TeamName.text = ""
    self.TeamSlogan.text = ""
    self.TeamGroup.text = ""
    self:UpdateViewInfo()
    --self:StartQueryTick()
end

function LuaQMPKTeamMemberDetailWnd:UpdateViewInfo()
    self.m_Data = CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo
    if not self.m_Data then return end
    self:InitHeadAndTitle()
    self:InitPlayerMember()
    self:UpdatePlayerMember()
end

-- function LuaQMPKTeamMemberDetailWnd:StartQueryTick()
--     if self.m_QueryTick then UnRegisterTick(self.m_QueryTick) self.m_QueryTick = nil end
--     self.m_QueryTick  = RegisterTick(function ()
--         if self.m_Force then
--             Gac2Gas.QueryQmpkBiWuZhanDuiInfo(self.m_Force)
--         end
-- 	end, 1000)
-- end

function LuaQMPKTeamMemberDetailWnd:InitHeadAndTitle()
    self.m_Force = self.m_Data.force
    local PlayerName = self.PlayerTemplate.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local titleIcon = self.TeamGroup.transform:Find("Bg_blue"):GetComponent(typeof(CUITexture))
    local bgLight = self.transform:Find("BackGround/Light"):GetComponent(typeof(UITexture))
    if self.m_Force == EnumCommonForce_lua.eDefend then
        self.TeamGroup.text = LocalString.GetString("[ffd5d1]红方[-]")
        self.TeamName.color = NGUIText.ParseColor24("fecec1", 0)
        self.TeamSlogan.color = NGUIText.ParseColor24("b18181", 0)
        PlayerName.color = NGUIText.ParseColor24("c7a9a9", 0)
        titleIcon:LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath("qmpkbattlebeforewnd_xiangqing_hong_zhenying"))
        bgLight.color = NGUIText.ParseColor24("732B31", 0)
    elseif self.m_Force == EnumCommonForce_lua.eAttack then
        self.TeamGroup.text = LocalString.GetString("[8BD8FF]蓝方[-]")
        self.TeamName.color = NGUIText.ParseColor24("c1e7fe", 0)
        self.TeamSlogan.color = NGUIText.ParseColor24("9DAAB9", 0)
        PlayerName.color = NGUIText.ParseColor24("C6D9F3", 0)
        titleIcon:LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath("qmpkbattlebeforewnd_xiangqing_lan_zhenying"))
        bgLight.color = NGUIText.ParseColor24("244974", 0)
    end 
    self.TeamName.text = CUICommonDef.cutDown(self.m_Data.teamName)
    
    self.TeamSlogan.text = self.m_Data.teamSlogan
end

function LuaQMPKTeamMemberDetailWnd:InitPlayerMember()
    local memberInfo = self.m_Data.memberInfoData
    Extensions.RemoveAllChildren(self.Table.transform)
    self.m_MemberViewList = {}
    local member2status = self:GetMemberSelectStatus(self.m_Data.round,memberInfo,self.m_Data.roundMember)
    local cnt = 0
    for k,v in pairs(memberInfo) do
        local member = CUICommonDef.AddChild(self.Table.gameObject,self.PlayerTemplate.gameObject)
        member.gameObject:SetActive(true)
        local headIconBg = member.transform:Find("Head/Bg"):GetComponent(typeof(CUITexture))
        local portrait = member.transform:Find("Head/Portrait"):GetComponent(typeof(CUITexture))
        local leaderIcon = member.transform:Find("Head/LeaderIcon").gameObject
        local classIcon = member.transform:Find("Head/ClassIcon"):GetComponent(typeof(UISprite))
        local NameLabel = member.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local OtherLabel = member.transform:Find("OtherLabel"):GetComponent(typeof(UILabel))
        headIconBg:LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath(self.m_Force == EnumCommonForce_lua.eAttack and "qmpkbattlebeforewnd_lan_touxiangkuang" or "qmpkbattlebeforewnd_hong_touxiangkuang"))
        leaderIcon.gameObject:SetActive(cnt == 0)
        portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(v.Class, v.Gender, -1), false)
        classIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), v.Class))
        NameLabel.text = v.Name
        OtherLabel.text = ""
        self.m_MemberViewList[v.Id] = {
            item = member,
            statusLabel = OtherLabel,
            data = v,
            status = member2status[v.Id],
        }
        cnt = cnt + 1
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
end

function LuaQMPKTeamMemberDetailWnd:UpdatePlayerMember()
    if not self.m_MemberViewList then return end
    local cnt = 0 
    local pickId = 0
    local selectMember = {}
    -- for k,v in pairs(self.m_Data.roundData) do
    --     selectMember[v] = true
    --     if cnt == 0 and self.m_Round == 4 then
    --         pickId = v
    --     end
    --     cnt = cnt + 1
    -- end

    for k,v in pairs(self.m_MemberViewList) do
        if v.status == EnumQmpkSelectBanPickStatus.eRound5Pick or v.status == EnumQmpkSelectBanPickStatus.eRound4Pick then
            self.m_MemberViewList[k].statusLabel.text = LocalString.GetString("[cb9465]本轮强制出战[-]")
        elseif v.status == EnumQmpkSelectBanPickStatus.eCanSelect then
            self.m_MemberViewList[k].statusLabel.text = ""
        else
            self.m_MemberViewList[k].statusLabel.text = LocalString.GetString("[8F8F8F]本轮不可出战[-]")
        end
    end
end


function LuaQMPKTeamMemberDetailWnd:GetMemberSelectStatus(round,allMember,roundMember)
	local memberIdStatus = {}
	for k,v in pairs(allMember) do
		memberIdStatus[v.Id] = EnumQmpkSelectBanPickStatus.eCanSelect
	end
	if round == 2 then 	-- 第二局，只能从第一局出战角色中选
		for k,v in pairs(memberIdStatus) do
			if roundMember[1][k] then
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eCanSelect
			else
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
			end
		end
	elseif round == 3 then -- 第三局，只能从第一局出战角色中选，第二局出战角色不可选
		for k,v in pairs(memberIdStatus) do
			if roundMember[2][k] then 
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound2Ban
			elseif roundMember[1][k] then
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eCanSelect
			else
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
			end
		end
	elseif  round == 4 then
        for k,v in pairs(memberIdStatus) do
            if roundMember[1][k] and not roundMember[2][k] and not roundMember[3][k] then	-- 第一局出战，第二局第三局未出战角色强制上场
                memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound4Pick
            elseif roundMember[2][k] then 
                memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound2Ban
            elseif roundMember[1][k] then 
                memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eCanSelect
            else
                memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
            end
        end
	elseif round == 5 then	-- 第五局只能选第一局出战角色
		for k,v in pairs(memberIdStatus) do
			if roundMember[1][k] then
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound5Pick
			else
				memberIdStatus[k] = EnumQmpkSelectBanPickStatus.eRound1Ban
			end
		end
	end
    return memberIdStatus
end
--@region UIEvent

--@endregion UIEvent

function LuaQMPKTeamMemberDetailWnd:OnEnable()
    --g_ScriptEvent:AddListener("ReplyQmpkQueryBiWuZhanDuiInfo",self,"UpdateViewInfo")
end

function LuaQMPKTeamMemberDetailWnd:OnDisable()
    -- g_ScriptEvent:RemoveListener("ReplyQmpkQueryBiWuZhanDuiInfo",self,"UpdateViewInfo")
    -- if self.m_QueryTick then
    --     UnRegisterTick(self.m_QueryTick)
    --     self.m_QueryTick = nil
    --   end
end