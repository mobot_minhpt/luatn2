local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumGender = import "L10.Game.EnumGender"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaCommonFashionLotteryResultView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaCommonFashionLotteryResultView, "m_CommonFashionLotteryView")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_LotteryEffectView")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_OnceView")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_TenView")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_NormalRewardTemplate")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_FashionRewardTemplate")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_ConfrimBtn")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_AgainBtn_1")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_AgainBtn_10")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_ShareBtn")
RegistClassMember(LuaCommonFashionLotteryResultView, "m_ButtonsRoot")

RegistClassMember(LuaCommonFashionLotteryResultView, "m_Tick")

--@endregion RegistChildComponent end

function LuaCommonFashionLotteryResultView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_CommonFashionLotteryView = self.transform.parent:Find("CommonFashionLotteryView"):GetComponent(typeof(CCommonLuaScript))
    self.m_LotteryEffectView = self.transform:Find("LotteryEffectView").gameObject
    self.m_OnceView = self.transform:Find("OnceView").gameObject
    self.m_NormalReward_Once = self.transform:Find("OnceView/Normal").gameObject
    self.m_NormalReward_gold_Once = self.transform:Find("OnceView/Normal_gold").gameObject
    self.m_FashionReward_Once = self.transform:Find("OnceView/Fashion").gameObject
    self.m_TenView = self.transform:Find("TenView").gameObject
    self.m_NormalReward_Ten = self.transform:Find("TenView/Pool/NormalReward").gameObject
    self.m_NormalReward_gold_Ten = self.transform:Find("TenView/Pool/NormalReward_gold").gameObject
    self.m_FashionReward_Ten = self.transform:Find("TenView/Pool/FashionReward").gameObject
    self.m_ButtonsRoot = self.transform:Find("Buttons").gameObject
    self.m_ConfrimBtn = self.transform:Find("Buttons/ConfrimBtn").gameObject
    self.m_AgainBtn_1 = self.transform:Find("Buttons/AgainBtn_1"):GetComponent(typeof(CButton))
    self.m_AgainBtn_10 = self.transform:Find("Buttons/AgainBtn_10"):GetComponent(typeof(CButton))
    self.m_ShareBtn = self.transform:Find("Buttons/ShareBtn").gameObject
    self.m_LotteryTicketLabel = self.transform:Find("Buttons/Top/LotteryTicket/Label"):GetComponent(typeof(UILabel))
    self.m_LotteryTicketAddBtn = self.transform:Find("Buttons/Top/LotteryTicket/AddBtn").gameObject
    self.m_UniTicketLabel = self.transform:Find("Buttons/Top/UniTicket/Label"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_ConfrimBtn).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnConfrimBtnClick()
    end)
    UIEventListener.Get(self.m_AgainBtn_1.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnAgainBtnClick(1)
    end)
    UIEventListener.Get(self.m_AgainBtn_10.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnAgainBtnClick(10)
    end)
    UIEventListener.Get(self.m_ShareBtn).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnShareBtnClick()
    end)
    UIEventListener.Get(self.m_LotteryTicketAddBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnLotteryTicketAddBtnClick(go)
    end)
    UIEventListener.Get(self.m_OnceView).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnOnceViewClick()
    end)
    
    local lotteryTicketIcon = self.transform:Find("Buttons/Top/LotteryTicket/Icon").gameObject
    local uniTicketIcon = self.transform:Find("Buttons/Top/UniTicket/Icon").gameObject
    UIEventListener.Get(lotteryTicketIcon).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnTicketIconClick(go, LuaFashionLotteryMgr.m_LotteryTicketId)
    end)
    UIEventListener.Get(uniTicketIcon).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnTicketIconClick(go, LuaFashionLotteryMgr.m_UniTicketId)
    end)
    
    self.m_ShareBtn:SetActive(not CommonDefs.IS_VN_CLIENT)
end

function LuaCommonFashionLotteryResultView:Init()
    if LuaFashionLotteryMgr.m_LotteryResult == nil then return end
    if self.m_FashionLotteryResultSound == nil then
        self.m_FashionLotteryResultSound = FashionLottery_Settings.GetData().LotteryResultSound
    end
    self.m_FashionReward_Ten:SetActive(false)
    self.m_NormalReward_Ten:SetActive(false)
    self.m_NormalReward_gold_Ten:SetActive(false)
    self.transform:Find("TenView/Pool").gameObject:SetActive(false)
    self.m_ButtonsRoot:SetActive(false)
    self.m_OnceView:SetActive(false)
    self.m_TenView:SetActive(false)
    self:ShowResult()
end

function LuaCommonFashionLotteryResultView:ShowResult()
    if #LuaFashionLotteryMgr.m_LotteryResult == 1 then
        self:ShowOneResult(1)
        self:ShowButtons(false)
    else
        self:InitTenView()
        self.m_FashionRewardIndex = 0
        self:OnOnceViewClick()
    end
end

function LuaCommonFashionLotteryResultView:ShowOneResult(index)
    local rewardInfo = LuaFashionLotteryMgr.m_LotteryResult[index]
    if rewardInfo.disassembleData then 
        self.m_NormalReward_Once.gameObject:SetActive(false)
        self.m_NormalReward_gold_Once.gameObject:SetActive(false)
        self.m_FashionReward_Once.gameObject:SetActive(true)
        self:InitFashionReward(self.m_FashionReward_Once.transform:Find("Image"), rewardInfo.disassembleData)
    elseif rewardInfo.Precious == 1 then
        self.m_NormalReward_Once.gameObject:SetActive(false)
        self.m_NormalReward_gold_Once.gameObject:SetActive(true)
        self.m_FashionReward_Once.gameObject:SetActive(false)
        self:InitOneRewardInfo(self.m_NormalReward_gold_Once, rewardInfo)
    else
        self.m_NormalReward_Once.gameObject:SetActive(true)
        self.m_NormalReward_gold_Once.gameObject:SetActive(false)
        self.m_FashionReward_Once.gameObject:SetActive(false)
        self:InitOneRewardInfo(self.m_NormalReward_Once, rewardInfo)
    end
    self.m_OnceView:SetActive(false)
    self.m_OnceView:SetActive(true)
end

function LuaCommonFashionLotteryResultView:InitTenView()
    self.m_TenView:SetActive(true)
    local grid = self.m_TenView.transform:Find("RewardGrid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(grid.transform)
    self.m_FashionRewardList = {}
    for i = 1, #LuaFashionLotteryMgr.m_LotteryResult do
        self:InitOneReward(grid, i)
    end
    grid:Reposition()
    self.m_TenView:SetActive(false)
end

function LuaCommonFashionLotteryResultView:InitOneReward(root, index)
    local rewardInfo = LuaFashionLotteryMgr.m_LotteryResult[index]
    local disassembleData = rewardInfo.disassembleData
    local cell = nil
    if disassembleData then 
        cell = NGUITools.AddChild(root.gameObject, self.m_FashionReward_Ten)
        self:InitOneRewardInfo(cell, rewardInfo, disassembleData.LotteryShowTexture)
        self:InitFashionReward(cell.transform:Find("IconTexture"), disassembleData)
    else
        cell = NGUITools.AddChild(root.gameObject, rewardInfo.Precious == 1 and self.m_NormalReward_gold_Ten or self.m_NormalReward_Ten)
        self:InitOneRewardInfo(cell, rewardInfo)
    end
end

function LuaCommonFashionLotteryResultView:InitFashionReward(cell, disassembleData)
    local purple = cell.transform:Find("purple")
    local red = cell.transform:Find("red")
    local item = nil
    -- 紫红
    purple.gameObject:SetActive(disassembleData.Color == 6)
    red.gameObject:SetActive(disassembleData.Color == 5)
    item = disassembleData.Color == 5 and red or purple
    -- 男女
    local male =item.transform:Find("Male")
    local female = item.transform:Find("Female")
    if item and CClientMainPlayer.Inst then
        if male then male.gameObject:SetActive(CClientMainPlayer.Inst.Gender == EnumGender.Male) end
        if female then female.gameObject:SetActive(CClientMainPlayer.Inst.Gender == EnumGender.Female) end
    end
end

function LuaCommonFashionLotteryResultView:InitOneRewardInfo(cell, rewardInfo, showTexture)
    local itemData = CItemMgr.Inst:GetItemTemplate(rewardInfo[1])
    if itemData then
        cell:GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(rewardInfo[1]), rewardInfo[2])
        -- 时装展示图
        if showTexture and CClientMainPlayer.Inst then
            local index = CClientMainPlayer.Inst.Gender == EnumGender.Male and 0 or 1
            cell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(showTexture[index])
        end
        -- 数量
        local numLabel = cell.transform:Find("NumLabel")
        if numLabel then
            numLabel.gameObject:SetActive(rewardInfo[2] > 1)
            numLabel:GetComponent(typeof(UILabel)).text = tostring(rewardInfo[2])
            numLabel.transform:Find("Sprite"):GetComponent(typeof(UISprite)):ResetAndUpdateAnchors()
        end
        -- 名字
        local nameLabel = cell.transform:Find("NameLabel")
        if nameLabel then nameLabel:GetComponent(typeof(UILabel)).text = itemData.Name end
        cell.gameObject:SetActive(true)
    end
end

function LuaCommonFashionLotteryResultView:ShowButtons(onlyShareBtn)
    self:RefreshTicket()
    self.m_AgainBtn_1.gameObject:SetActive(#LuaFashionLotteryMgr.m_LotteryResult == 1 and not onlyShareBtn)
    self.m_AgainBtn_10.gameObject:SetActive(#LuaFashionLotteryMgr.m_LotteryResult == 10 and not onlyShareBtn)
    self.m_ConfrimBtn.gameObject:SetActive(not onlyShareBtn)
    self.m_ButtonsRoot:SetActive(false)
    if self.m_Tick then UnRegisterTick(self.m_Tick) end
    self.m_Tick = RegisterTickOnce(function()
        self.m_ButtonsRoot:SetActive(true)
    end, 500)
end

function LuaCommonFashionLotteryResultView:RefreshTicket()
    local ticketCount = LuaFashionLotteryMgr:GetLotteryTicketCount()
    if CommonDefs.IS_VN_CLIENT then
        if ticketCount < 1 then
            self.m_AgainBtn_1.Text = LocalString.GetString("[c][9f7f8a]1[-][/c]")
            self.m_AgainBtn_10.Text = LocalString.GetString("[c][9f7f8a]10[-][/c]")
        elseif ticketCount < 10 then
            self.m_AgainBtn_1.Text = LocalString.GetString("1")
            self.m_AgainBtn_10.Text = LocalString.GetString("[c][9f7f8a]10[-][/c]")
        else
            self.m_AgainBtn_1.Text = LocalString.GetString("1")
            self.m_AgainBtn_10.Text = LocalString.GetString("10")
        end
    else
        if ticketCount < 1 then
            self.m_AgainBtn_1.Text = LocalString.GetString("再抽1次         [c][9f7f8a]1[-][/c]")
            self.m_AgainBtn_10.Text = LocalString.GetString("再抽10次         [c][9f7f8a]10[-][/c]")
        elseif ticketCount < 10 then
            self.m_AgainBtn_1.Text = LocalString.GetString("再抽1次         1")
            self.m_AgainBtn_10.Text = LocalString.GetString("再抽10次         [c][9f7f8a]10[-][/c]")
        else
            self.m_AgainBtn_1.Text = LocalString.GetString("再抽1次         1")
            self.m_AgainBtn_10.Text = LocalString.GetString("再抽10次         10")
        end
    end
    self.m_LotteryTicketLabel.text = tostring(ticketCount)
    self.m_UniTicketLabel.text = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, LuaFashionLotteryMgr.m_UniTicketId)
end

--@region UIEvent
function LuaCommonFashionLotteryResultView:OnConfrimBtnClick()
    self.gameObject:SetActive(false)
    self.m_CommonFashionLotteryView.m_LuaSelf:Init()
    g_ScriptEvent:BroadcastInLua("PlayFashionLotteryAnimation", "jiuweihufashionlotterywnd_show02", 1)
end

function LuaCommonFashionLotteryResultView:OnAgainBtnClick(drawCount)
    LuaFashionLotteryMgr:FashionLotteryRequestDraw(drawCount)
end

function LuaCommonFashionLotteryResultView:OnShareBtnClick()
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.QingMing2023PVPResultWnd, self.m_ShareBtn.transform.parent.gameObject)
end

function LuaCommonFashionLotteryResultView:OnLotteryTicketAddBtnClick(go)
    CUIManager.ShowUI(LuaFashionLotteryMgr.m_OpenWnd.BuyTicketWnd)
end

function LuaCommonFashionLotteryResultView:OnOnceViewClick()
    if #LuaFashionLotteryMgr.m_LotteryResult > 1 then
        if self.m_FashionRewardIndex < #LuaFashionLotteryMgr.m_FashionRewardList then
            -- 显示下一个抽中的时装奖励
            if self.m_FashionRewardIndex > 0 then
                self:PlayResultSound(0)
            end
            self.m_FashionRewardIndex = self.m_FashionRewardIndex + 1
            local index = LuaFashionLotteryMgr.m_FashionRewardList[self.m_FashionRewardIndex].index
            self:ShowOneResult(index)
            self:ShowButtons(true)
        else
            -- 全部展示完毕，显示十连抽结果
            if self.m_FashionRewardIndex > 0 then
                self:PlayResultSound(1)
            end
            self.m_OnceView:SetActive(false)
            self.m_TenView:SetActive(true)
            self:ShowButtons(false)
        end
    end
end

function LuaCommonFashionLotteryResultView:OnTicketIconClick(go, itemId)
    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Right, go.transform.position.x, go.transform.position.y, 0, 0)
end

function LuaCommonFashionLotteryResultView:PlayResultSound(index)
    local sound = self.m_FashionLotteryResultSound[index + 2]
    if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
    self.m_LotteryResultSound = SoundManager.Inst:PlayOneShot(sound, Vector3.zero, nil, 0)
end

--@endregion UIEvent

function LuaCommonFashionLotteryResultView:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "RefreshTicket")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshTicket")
    self:Init()
end

function LuaCommonFashionLotteryResultView:OnDisable()
    if self.m_Tick then UnRegisterTick(self.m_Tick) end
    if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshTicket")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshTicket")
end
