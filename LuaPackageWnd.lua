local UILabel = import "UILabel"
local MessageWndManager = import "L10.UI.MessageWndManager"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CRepertoryMgr = import "L10.Game.CRepertoryMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CQianKunDaiMgr = import "L10.Game.CQianKunDaiMgr"
local Constants = import "L10.Game.Constants"
local CommonDefs = import "L10.Game.CommonDefs"
--local Charge_RepertoryInfo = import "L10.Game.Charge_RepertoryInfo"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local UITabBar = import "L10.UI.UITabBar"
local CPackageView=import "L10.UI.CPackageView"
local CQianKunDaiInputView=import "L10.UI.CQianKunDaiInputView"
local CQianKunDaiCompoundView=import "L10.UI.CQianKunDaiCompoundView"

CLuaPackageWnd = class()
RegistClassMember(CLuaPackageWnd,"closeBtn")
RegistClassMember(CLuaPackageWnd,"titleLabel")
RegistClassMember(CLuaPackageWnd,"tabBar")
RegistClassMember(CLuaPackageWnd,"packageRoot")
RegistClassMember(CLuaPackageWnd,"warehouseRoot")
RegistClassMember(CLuaPackageWnd,"qiankundaiRoot")
RegistClassMember(CLuaPackageWnd,"packageView")
RegistClassMember(CLuaPackageWnd,"warehousePackageView")
RegistClassMember(CLuaPackageWnd,"warehouseView")
RegistClassMember(CLuaPackageWnd,"qiankundaiInputView")
RegistClassMember(CLuaPackageWnd,"qiankundaiCompoundView")
RegistClassMember(CLuaPackageWnd,"qiankundaiTab")
RegistClassMember(CLuaPackageWnd,"notRequestOpenRepertory")
RegistClassMember(CLuaPackageWnd,"m_WarehouseInited")
RegistClassMember(CLuaPackageWnd,"m_BangCangEnabled")
RegistClassMember(CLuaPackageWnd,"m_YaoGuaiWarehouseBtn")
RegistClassMember(CLuaPackageWnd,"m_YaoGuaiWarehouseBtnAlertSprite")

function CLuaPackageWnd:Awake()
    self.closeBtn = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
    self.titleLabel = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.tabBar = self.gameObject:GetComponent(typeof(UITabBar))
    self.packageRoot = self.transform:Find("Package").gameObject
    self.warehouseRoot = self.transform:Find("Warehouse").gameObject
    self.qiankundaiRoot = self.transform:Find("QianKunDai").gameObject
    self.packageView = self.transform:Find("Package/PackageView"):GetComponent(typeof(CPackageView))
    self.warehousePackageView = self.transform:Find("Warehouse/WarehousePackageView"):GetComponent(typeof(CCommonLuaScript))
    self.warehouseView = self.transform:Find("Warehouse/WarehouseView"):GetComponent(typeof(CCommonLuaScript))
    self.qiankundaiInputView = self.transform:Find("QianKunDai/InputSelectionView"):GetComponent(typeof(CQianKunDaiInputView))
    self.qiankundaiCompoundView = self.transform:Find("QianKunDai/CompoundView"):GetComponent(typeof(CQianKunDaiCompoundView))
    self.qiankundaiTab = self.tabBar:GetTabGoByIndex(3) --QianKunDai
    self.notRequestOpenRepertory = false
    self.m_BangCangEnabled = false
    self.warehouseRoot:SetActive(true)
    self.warehouseView.gameObject:SetActive(true)
    self.warehousePackageView.gameObject:SetActive(true)
    self.m_YaoGuaiWarehouseBtn = self.transform:Find("Package/MoneyInfoView/YaoGuaiWarehouseBtn").gameObject
    self.m_YaoGuaiWarehouseBtn:SetActive(LuaZhuoYaoMgr:IsFeatureOpen())
    self.m_YaoGuaiWarehouseBtnAlertSprite = self.m_YaoGuaiWarehouseBtn.transform:Find("YaoGuaiWarehouseBtnAlertSprite")
    self.m_YaoGuaiWarehouseBtnAlertSprite.gameObject:SetActive(false)
    if LuaZongMenMgr.m_OpenNewSystem then
        Gac2Gas.RequestYaoGuaiRedAlert()
    end
    UIEventListener.Get(self.m_YaoGuaiWarehouseBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.ShowUI(CLuaUIResources.ZhuoYaoMainWnd)
    end)

end

function CLuaPackageWnd:ActiveWarehouse()
    self.warehouseRoot:SetActive(true)

    if(not self.m_WarehouseInited)then
        self.m_WarehouseInited = true
        self.warehousePackageView.m_LuaSelf.OnPackageItemClick = function(packagePos, itemId)
            self:OnWarehousePackageItemClick(packagePos, itemId)
        end
        --AdvanceGridView在初次创建时的OnEnable里调用ReloadData会导致格子的世界空间长宽计算错误，因为此时UI还未设为UIRoot的子物体，而UIRoot的Scale不为1，此时计算的世界空间值与之后的值不同
        self.warehousePackageView.m_LuaSelf:Init()
        self.warehouseView.m_LuaSelf.m_OnMoveItemFromWarehouseToBagAction = function(repoIndex, itemId)
            self:OnMoveItemFromWarehouseToBag(repoIndex, itemId)
        end
        self.warehouseView.m_LuaSelf.m_RefreshPackageView = function(mask_unbinded)
            self.m_BangCangEnabled = mask_unbinded
            self.warehousePackageView.m_LuaSelf:OnRefreshPackageView(mask_unbinded)
        end
        self.warehouseView.m_LuaSelf:InitWarehouse(true)
    end
end

-- Auto Generated!!
function CLuaPackageWnd:Init( )
    self.tabBar.validator = LuaUtils.UITabBar_Validate(
        function(index)
            return self:OnTabChangeValidate(index)
        end
    )
    self.tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTabChange(go, index)
        end
    )

    self.tabBar:ChangeTab(0, false)
    self.qiankundaiTab:SetActive(CQianKunDaiMgr.Inst.QianKunDaiSwitchOpen == 1)

    if CQianKunDaiMgr.Inst.QianKunDaiSwitchOpen == 1 then
        self.qiankundaiTab:SetActive(true)
    else
        self.qiankundaiTab:SetActive(CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.HasFeiSheng)
    end
    self:CheckGuide()
end

function CLuaPackageWnd:OnEnable()
    g_ScriptEvent:AddListener("OnPackageWarehouseOpen", self, "OnPackageWarehouseOpen")
    g_ScriptEvent:AddListener("RefreshQianKunDaiInTransfer", self, "OnRefreshQianKunDaiInTransfer")
    g_ScriptEvent:AddListener("SyncYaoGuaiRedAlert", self, "OnSyncYaoGuaiRedAlert")
    g_ScriptEvent:AddListener("RefreshRpcFromFastOverlap", self, "OnRefreshRpcFromFastOverlap")
end

function CLuaPackageWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnPackageWarehouseOpen", self, "OnPackageWarehouseOpen")
    g_ScriptEvent:RemoveListener("RefreshQianKunDaiInTransfer", self, "OnRefreshQianKunDaiInTransfer")
    g_ScriptEvent:RemoveListener("SyncYaoGuaiRedAlert", self, "OnSyncYaoGuaiRedAlert")
    g_ScriptEvent:RemoveListener("RefreshRpcFromFastOverlap", self, "OnRefreshRpcFromFastOverlap")
end

function CLuaPackageWnd:OnRefreshRpcFromFastOverlap()
    if self.notRequestOpenRepertory then
        Gac2Gas.RequestOpenRepertory()
        self.notRequestOpenRepertory = false
    end
    Gac2Gas.RequestMergeOverlapItemToRepo()
end

function CLuaPackageWnd:OnSyncYaoGuaiRedAlert(bShow)
    if not LuaZongMenMgr.m_OpenNewSystem then
        return
    end
    self.m_YaoGuaiWarehouseBtnAlertSprite.gameObject:SetActive(bShow)
end

function CLuaPackageWnd:OnRefreshQianKunDaiInTransfer()
    self.tabBar:ChangeTab(0, false)
end

function CLuaPackageWnd:OnTabChange( go, index) 
    self.titleLabel.text = string.gsub(CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel)).text, "\n", CommonDefs.IS_VN_CLIENT and " " or "")
    if index == 0 then
        self.packageRoot:SetActive(true)
        self.warehouseRoot:SetActive(false)
        self.qiankundaiRoot:SetActive(false)
        self.packageView:ShowPackage(EnumItemPlace.Bag)
    elseif index == 1 then
        self.packageRoot:SetActive(true)
        self.warehouseRoot:SetActive(false)
        self.qiankundaiRoot:SetActive(false)
        self.packageView:ShowPackage(EnumItemPlace.Task)
    elseif index == 2 then
        if CClientMainPlayer.Inst ~= nil then
            if CRepertoryMgr.Inst.HasCacheItems then
                self.notRequestOpenRepertory = true
                self.packageRoot:SetActive(false)
                self.qiankundaiRoot:SetActive(false)
                --self.warehouseRoot:SetActive(true)
                self:ActiveWarehouse()
            else
                self.notRequestOpenRepertory = false
                Gac2Gas.RequestOpenRepertory()
                --内容先不切换，待rpc回来后再切换
            end
        end
    elseif index == 3 then
        self.packageRoot:SetActive(false)
        self.warehouseRoot:SetActive(false)
        self.qiankundaiRoot:SetActive(true)
        self.qiankundaiInputView:InitQianKunDaiInput(1)
        self.qiankundaiCompoundView:Init(1)
    end
end
function CLuaPackageWnd:OnTabChangeValidate( index) 
    -- 特殊处理一下全民PK
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer and (index == 2 or index == 3) then
        g_MessageMgr:ShowMessage("NOT_ALLOW_IN_QUANMINPK")
        return false
    end
    --只对仓库进行验证
    if index ~= 2 then
        return true
    end
    if CClientMainPlayer.Inst ~= nil then
        local repertoryOpenLevel = GameSetting_Common_Wapper.Inst.RepertoryOpenLevel
        if CClientMainPlayer.Inst.MaxLevel < repertoryOpenLevel then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Repertory_Level_Not_Enough", repertoryOpenLevel), DelegateFactory.Action(function () 
                --DO Nothing
            end), nil, nil, nil, false)
            --tabBar.ChangeTab(0);
            return false
        end

        local minVipLevel = System.UInt32.MaxValue
        Charge_RepertoryInfo.ForeachKey(function (vipLevel) 
            local info = Charge_RepertoryInfo.GetData(vipLevel)
            if info.OpenTimes > 0 and vipLevel < minVipLevel then
                minVipLevel = vipLevel
            end
        end)
        if CClientMainPlayer.Inst.ItemProp.Vip.Level < minVipLevel then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("VIP_UNLOCK_REPERTORY", minVipLevel), DelegateFactory.Action(function () 
                self:FindNPC()
            end), nil, nil, nil, false)
            return false
        elseif CClientMainPlayer.Inst:GetLeftRepertoryOpenTimes() == 0 then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NO_OPEN_REPERTORY_TIMES_LEFT"), DelegateFactory.Action(function () 
                self:FindNPC()
            end), nil, nil, nil, false)
            return false
        end

        return true
    else
        return false
    end
end

function CLuaPackageWnd:FindNPC()
    CTrackMgr.Inst:FindNPC(GameSetting_Common_Wapper.Inst.WarehouseNPCTemplateID, GameSetting_Common_Wapper.Inst.WarehouseNPCSceneTemplateID, 0, 0, nil, nil)
end

function CLuaPackageWnd:OnPackageWarehouseOpen(args)
    if self.tabBar.SelectedIndex == 2 then
        self.packageRoot:SetActive(false)
        self.qiankundaiRoot:SetActive(false)
        --self.warehouseRoot:SetActive(true)
        self:ActiveWarehouse()
    end
end
function CLuaPackageWnd:OnWarehousePackageItemClick( packagePos, itemId) 
    if self.notRequestOpenRepertory then
        Gac2Gas.RequestOpenRepertory()
        self.notRequestOpenRepertory = false
    end
    if(self.m_BangCangEnabled)then
        Gac2Gas.MoveNormalItemToSimpleItems(packagePos, itemId)
    else
        if IsBindRepoDisplayOpen() and CLuaBindRepoMgr.CanPlaceInBindRepo(itemId) then
            Gac2Gas.MoveNormalItemToSimpleItems(packagePos, itemId)
            local commonItem = CItemMgr.Inst:GetById(itemId)
            if commonItem then
                g_MessageMgr:ShowMessage("ANNANG_WARNING",commonItem.Name)
            end
        else
            Gac2Gas.MoveItemToRepertory(packagePos, itemId, self.warehouseView.m_LuaSelf:GetCurRepoPageIdx() * Constants.SingeWarehouseSize + 1)
        end
    end
end
function CLuaPackageWnd:OnMoveItemFromWarehouseToBag( repoIndex, itemId) 
    if self.notRequestOpenRepertory then
        Gac2Gas.RequestOpenRepertory()
        self.notRequestOpenRepertory = false
    end
    Gac2Gas.MoveItemToBag(repoIndex, itemId)
end

function CLuaPackageWnd:YaoGuaiWarehouseBtn()
    return self.m_YaoGuaiWarehouseBtn
end

function CLuaPackageWnd:CheckGuide()
    if CClientMainPlayer.Inst and LuaPlayerPropertyMgr.PropertyGuide then
        if CClientMainPlayer.Inst.Level >= LuaPlayerPropertyMgr:GetPropertyGuideLevel() then
            CLuaGuideMgr.TryTriggerGuide(234)
        end
    end
end