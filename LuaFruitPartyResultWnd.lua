local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local CUIFx = import "L10.UI.CUIFx"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaFruitPartyResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFruitPartyResultWnd, "m_RankBtn", "RankBtn", CButton)
RegistChildComponent(LuaFruitPartyResultWnd, "m_ShareBtn", "ShareBtn", CButton)
RegistChildComponent(LuaFruitPartyResultWnd, "m_PlayerInfoNode", "PlayerInfoNode", GameObject)
RegistChildComponent(LuaFruitPartyResultWnd, "m_ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaFruitPartyResultWnd, "m_Label", "Label", UILabel)
RegistChildComponent(LuaFruitPartyResultWnd, "m_Grid", "Grid", UIGrid)
RegistChildComponent(LuaFruitPartyResultWnd, "m_Templete", "Templete", GameObject)
RegistChildComponent(LuaFruitPartyResultWnd, "m_ItemFx", "ItemFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaFruitPartyResultWnd, "m_Rewards")
RegistClassMember(LuaFruitPartyResultWnd, "m_ClostBtn")
RegistClassMember(LuaFruitPartyResultWnd, "m_HideWhenCapture")
RegistClassMember(LuaFruitPartyResultWnd, "m_Tick")

function LuaFruitPartyResultWnd:Awake()
	self.m_HideWhenCapture = self.transform:Find("Anchor/HideWhenCapture").gameObject
	self.m_ClostBtn = self.transform:Find("Anchor/HideWhenCapture/CloseBtn").gameObject
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.m_RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.m_ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)

    --@endregion EventBind end
end

function LuaFruitPartyResultWnd:Init()
	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end

	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	end

	self.m_ScoreLabel.text = LuaFruitPartyMgr.m_Score
	self.m_Label.text = LuaFruitPartyMgr.m_MaxLianJi

	self:InitRewards()
end

--@region UIEvent

function LuaFruitPartyResultWnd:OnRankBtnClick()
	CUIManager.ShowUI(CLuaUIResources.FruitPartyRankWnd)
end

function LuaFruitPartyResultWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.m_HideWhenCapture,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

--@endregion UIEvent

function LuaFruitPartyResultWnd:InitRewards()
	self.m_Rewards = {}
	for i = 0, ShuiGuoPaiDui_Setting.GetData().RewardInfo.Length - 1 do
		local reward = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_Templete)
		reward:SetActive(true)
		table.insert(self.m_Rewards, reward)
	end
	self.m_Grid:Reposition()

	self:RefreshRewards()
end

function LuaFruitPartyResultWnd:RefreshRewards()
	local info = {}

	for i = 0, ShuiGuoPaiDui_Setting.GetData().RewardInfo.Length - 1 do
		local data = ShuiGuoPaiDui_Setting.GetData().RewardInfo[i]
		table.insert(info, {data[2], data[1]})
	end

	table.sort(info, function(a, b)
		return a[2] < b[2]
	end)

	for i = 1, #self.m_Rewards do
		local reward = self.m_Rewards[i]
		self:InitReward(reward, info[i], i)
	end
end

function LuaFruitPartyResultWnd:InitReward(item, info, i)
	local icon 	= item.transform:Find("Clip/Icon"):GetComponent(typeof(CUITexture))
	local mask 	= item.transform:Find("Clip/Mask").gameObject
	local score = item.transform:Find("Score"):GetComponent(typeof(UILabel))
	local fx 	= item.transform:Find("Fx"):GetComponent(typeof(CUIFx))

	local id	= info[1]
	local itemData = Item_Item.GetData(id)
	icon:LoadMaterial(itemData.Icon)
	score.text = info[2]

	UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)

	fx:DestroyFx()
	if LuaFruitPartyMgr.m_GotRewards[i] == true then
		mask:SetActive(true)
		Extensions.SetLocalPositionZ(item.transform, 0)
	elseif LuaFruitPartyMgr.m_Rewards[i] == true then
		fx:LoadFx("Fx/UI/Prefab/UI_shuiguopaidui_jiesuanlibao.prefab")
		mask:SetActive(false)
		Extensions.SetLocalPositionZ(item.transform, 0)
		self.m_Tick = RegisterTickOnce(function()
			mask:SetActive(true)
		end, 2000)
	else 
		mask:SetActive(false)
		Extensions.SetLocalPositionZ(item.transform, -1)
	end
end

function LuaFruitPartyResultWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end
	LuaFruitPartyMgr.m_MaxLianJi = 0
	LuaFruitPartyMgr.m_Score = 0
	LuaFruitPartyMgr.m_Rewards = {}
	LuaFruitPartyMgr.m_GotRewards = {}
end
