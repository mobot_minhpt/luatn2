local DelegateFactory        = import "DelegateFactory"
local CMessageTipMgr         = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem      = import "L10.UI.CTipParagraphItem"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local Animation              = import "UnityEngine.Animation"

LuaValentine2023LGTMEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023LGTMEnterWnd, "easy")
RegistClassMember(LuaValentine2023LGTMEnterWnd, "hard")
RegistClassMember(LuaValentine2023LGTMEnterWnd, "curTab")
RegistClassMember(LuaValentine2023LGTMEnterWnd, "leftTimes")
RegistClassMember(LuaValentine2023LGTMEnterWnd, "easyLeftTimes")
RegistClassMember(LuaValentine2023LGTMEnterWnd, "hardLeftTimes")

RegistClassMember(LuaValentine2023LGTMEnterWnd, "grid")
RegistClassMember(LuaValentine2023LGTMEnterWnd, "template")
RegistClassMember(LuaValentine2023LGTMEnterWnd, "animation")

function LuaValentine2023LGTMEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.grid = self.transform:Find("Info/Award/Grid"):GetComponent(typeof(UIGrid))
    self.template = self.transform:Find("Info/Award/Template").gameObject
    self.template:SetActive(false)
    self.animation = self.transform:GetComponent(typeof(Animation))

    self.leftTimes = self.transform:Find("Info/LeftTimes"):GetComponent(typeof(UILabel))
    self.leftTimes.gameObject:SetActive(false)

    self:InitTabs()
    self:InitDesc()

    UIEventListener.Get(self.transform:Find("Info/EnterButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)

    UIEventListener.Get(self.transform:Find("Info/TipButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
end

function LuaValentine2023LGTMEnterWnd:InitTabs()
    self.easy = self.transform:Find("Tabs/Easy"):GetComponent(typeof(UILabel))
    self.hard = self.transform:Find("Tabs/Hard"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.easy.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTabClick(1)
	end)

    UIEventListener.Get(self.hard.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTabClick(2)
	end)
    self:OnTabClick(1)
end

-- 更新奖励
function LuaValentine2023LGTMEnterWnd:UpdateAward()
    local childCount = self.grid.transform.childCount

    local setting = Valentine2023_LGTM_Setting.GetData()
    local str = self.curTab == 1 and setting.EasyPassAward or setting.HardPassAward
    local tbl = {}
    for itemId, count in string.gmatch(str, "(%d+),(%d+)") do
        table.insert(tbl, {tonumber(itemId), tonumber(count)})
    end
    if self.curTab == 2 then
        table.insert(tbl, {setting.HardPassExpressionItem, 1})
    end

    if childCount < #tbl then
        for i = childCount + 1, #tbl do
            NGUITools.AddChild(self.grid.gameObject, self.template)
        end
    end

    if childCount > #tbl then
        for i = #tbl + 1, childCount do
            self.grid.transform:GetChild(i - 1).gameObject:SetActive(false)
        end
    end

    for i = 1, #tbl do
        local child = self.grid.transform:GetChild(i - 1)
        child.gameObject:SetActive(true)
        local qnReturnAward = child:GetComponent(typeof(CQnReturnAwardTemplate))
        qnReturnAward:Init(Item_Item.GetData(tbl[i][1]), tbl[i][2])
    end
    self.grid:Reposition()
end

-- 玩法介绍
function LuaValentine2023LGTMEnterWnd:InitDesc()
    local template = self.transform:Find("Info/Desc/ParagraphTemplate").gameObject
    template:SetActive(false)
    local table = self.transform:Find("Info/Desc/Table"):GetComponent(typeof(UITable))

    Extensions.RemoveAllChildren(table.transform)
    local msg =	g_MessageMgr:FormatMessage("VALENTINE2023_LINGGUTANMI_ENTER_TIP")
    if System.String.IsNullOrEmpty(msg) then return end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if not info then return end

    do
		local i = 0
		while i < info.paragraphs.Count do							-- 根据信息创建并显示段落
			local paragraph = CUICommonDef.AddChild(table.gameObject, template) -- 添加段落Go
			paragraph:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraph, typeof(CTipParagraphItem))
			tip:Init(info.paragraphs[i], 4294967295)				-- 初始化段落，color = 4294967295（0xFFFFFFFF）
			i = i + 1
		end
    end
    table:Reposition()
end

function LuaValentine2023LGTMEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("SendValentineLGTMPlayInfo", self, "OnSendValentineLGTMPlayInfo")
    Gac2Gas.RequestValentineLGTMPlayInfo()
end

function LuaValentine2023LGTMEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendValentineLGTMPlayInfo", self, "OnSendValentineLGTMPlayInfo")
end

function LuaValentine2023LGTMEnterWnd:OnSendValentineLGTMPlayInfo(remainEasyModeRewardTimes, remainHardModeRewardTimes)
    self.easyLeftTimes = remainEasyModeRewardTimes
    self.hardLeftTimes = remainHardModeRewardTimes
    self.leftTimes.gameObject:SetActive(true)
    self:UpdateLeftTimes()
end

function LuaValentine2023LGTMEnterWnd:UpdateLeftTimes()
    local totalRewardTimes = Valentine2023_LGTM_Setting.GetData().EasyLingGuTanMiDailyRewardTimes
    self.leftTimes.text = SafeStringFormat3(LocalString.GetString("每日奖励次数 %d/%d  困难模式通关次数 %d/3"), totalRewardTimes - (self.easyLeftTimes or 0), totalRewardTimes, self.hardLeftTimes or 0)
end


function LuaValentine2023LGTMEnterWnd:Init()

end

--@region UIEvent

function LuaValentine2023LGTMEnterWnd:OnTabClick(i)
    if self.curTab and self.curTab == i then return end

    if self.curTab then
        self.animation:Play(i == 1 and "valentine2023lgtmenterwnd_switchtonormal" or "valentine2023lgtmenterwnd_switchtohard")
    end
    self.curTab = i
    self.easy.transform:Find("Highlight").gameObject:SetActive(i == 1)
    self.hard.transform:Find("Highlight").gameObject:SetActive(i == 2)
    self.easy.color = NGUIText.ParseColor24(i == 1 and "815A70" or "FFFFFF", 0)
    self.hard.color = NGUIText.ParseColor24(i == 2 and "815A70" or "FFFFFF", 0)

    self:UpdateAward()
    self:UpdateLeftTimes()
end

function LuaValentine2023LGTMEnterWnd:OnEnterButtonClick()
    Gac2Gas.SignUpValentineLGTMGameplay(self.curTab == 2)
end

function LuaValentine2023LGTMEnterWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("VALENTINE2023_LINGGUTANMI_TIP")
end

--@endregion UIEvent
