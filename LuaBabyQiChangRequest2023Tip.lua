require("common/common_include")

local UIScrollView=import "UIScrollView"
local UIWidget = import "UIWidget"
local Baby_QiChang = import "L10.Game.Baby_QiChang"
local UITable = import "UITable"
local Baby_QiYu = import "L10.Game.Baby_QiYu"
local Baby_ShaoNian = import "L10.Game.Baby_ShaoNian"
local Baby_YouEr = import "L10.Game.Baby_YouEr"
local UIRoot=import "UIRoot"
local Screen=import "UnityEngine.Screen"
local NGUIMath=import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UIGrid = import "UIGrid"
local EnumGender = import "L10.Game.EnumGender"

LuaBabyQiChangRequest2023Tip=class()

RegistChildComponent(LuaBabyQiChangRequest2023Tip, "QiChangNameLabel", UILabel)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "StatusLabel", UILabel)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Background", UIWidget)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Table", UITable)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "ScrollView", UIScrollView)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Header", UIWidget)

RegistChildComponent(LuaBabyQiChangRequest2023Tip, "RequestTemplate", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "RewardTemplate", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "TitleTemplate", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "PreQiChangTemplate", GameObject)

RegistChildComponent(LuaBabyQiChangRequest2023Tip, "TagGrid", UITable)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Current", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "JieBan", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Target", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Locked", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Recormmend", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "DaiJianDing", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "SetTargetRoot", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "SetJieBanRoot", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "JianDingRoot", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "AlreadyTargetRoot", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "AlreadyJieBanRoot", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "SetTargetBtn", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "SetJieBanBtn", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "SetJianDingBtn", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "BtnRecormmendTag", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "QiChangTreeView", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "TreeTable", UITable)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "TreeTemplate", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "CancelBtn", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Bottom", UIWidget)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Line", GameObject)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "HeaderTree", UIWidget)
RegistChildComponent(LuaBabyQiChangRequest2023Tip, "Tree", UIWidget)

RegistClassMember(LuaBabyQiChangRequest2023Tip, "SelectedBaby")
RegistClassMember(LuaBabyQiChangRequest2023Tip, "SelectedQiChang")
RegistClassMember(LuaBabyQiChangRequest2023Tip, "mIsUnlocked")

function LuaBabyQiChangRequest2023Tip:Init()
    self.mIsUnlocked = false
    self.mIsDaiJianDing = false
    self.mIsAlreadyJianDing = false
    if not LuaBabyMgr.m_ChosenBaby then
        CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
        return
    end
    self.SelectedBaby = LuaBabyMgr.m_ChosenBaby

    local qichang =  Baby_QiChang.GetData(LuaBabyMgr.m_SelectedQiChangTipId)
    if not qichang  then
        CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
        return
    end
    self.SelectedQiChang = qichang

    self.RequestTemplate:SetActive(false)
    self.RewardTemplate:SetActive(false)
    self.TitleTemplate:SetActive(false)
    self.TreeTemplate:SetActive(false)
    self.PreQiChangTemplate:SetActive(false)
    self.Line:SetActive(false)

    self.QiChangNameLabel.text = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(self.SelectedQiChang.Quality-1), self:GetQiChangName(self.SelectedQiChang))

    Extensions.RemoveAllChildren(self.Table.transform)
    self:UpdateQiChangRequest()
    self:UpdateQiChangReward()

    
    self.Table:Reposition()

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + (12 * 2) + contentTopPadding + contentBottomPadding + (self.ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 636, virtualScreenHeight - 100)
    self.Background:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.Header:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.Bottom:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.TagGrid:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.ScrollView.panel:ResetAndUpdateAnchors()
    self.ScrollView:ResetPosition()
    self:InitTitle()
    self:InitSetTargetRoot()
    self:InitSetJieBanRoot()
    self:InitJianDingRoot()
    self:InitAlreadyJieBanRoot()
    self:InitAlreadyTargetRoot()
   
    self.QiChangTreeRoot = self.QiChangTreeView.transform.parent.gameObject
    self.QiChangTreeRoot:SetActive(false)
end

function LuaBabyQiChangRequest2023Tip:InitTitle()
    CUICommonDef.ClearTransform(self.TagGrid.transform)

    local isOwnBaby = self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id -- 只有专属宝宝才能结伴气场
    local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
    local isActive =  CommonDefs.DictContains_LuaCall(activedQiChang, self.SelectedQiChang.ID)
    local isUnlocked = self.mIsUnlocked
    local isTarget = LuaBabyMgr.mTargetQiChangId and LuaBabyMgr.mTargetQiChangId == self.SelectedQiChang.ID
	local isRecormmend = LuaBabyMgr.mRecormmendQiChangId and LuaBabyMgr.mRecormmendQiChangId == self.SelectedQiChang.ID
    local isJieBan = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId == self.SelectedQiChang.ID and isOwnBaby
    local isCurrent = self.SelectedBaby.Props.QiChangData.CurrentQiChangId == self.SelectedQiChang.ID

    self.Recormmend:SetActive(false)
    self.Target:SetActive(false)
    self.Current:SetActive(false)
    self.JieBan:SetActive(false)
    self.Locked:SetActive(false)
    self.DaiJianDing:SetActive(false)

	if isRecormmend then
        local go = NGUITools.AddChild(self.TagGrid.gameObject, self.Recormmend)
		go:SetActive(true)
    end
    if isTarget then
        local go = NGUITools.AddChild(self.TagGrid.gameObject, self.Target)
		go:SetActive(true)
    end
    if isCurrent then
        local go = NGUITools.AddChild(self.TagGrid.gameObject, self.Current)
		go:SetActive(true)
    end
    if isJieBan then
        local go = NGUITools.AddChild(self.TagGrid.gameObject, self.JieBan)
		go:SetActive(true)
    end
    if not isUnlocked then
        local go = NGUITools.AddChild(self.TagGrid.gameObject, self.Locked)
		go:SetActive(true)
    elseif not isActive then
        self.mIsDaiJianDing = true
        local go = NGUITools.AddChild(self.TagGrid.gameObject, self.DaiJianDing)
		go:SetActive(true)
    else
        --已鉴定
        self.mIsAlreadyJianDing = true
    end
    self.TagGrid:Reposition()
end

function LuaBabyQiChangRequest2023Tip:InitSetTargetRoot()
    local isTarget = LuaBabyMgr.mTargetQiChangId and LuaBabyMgr.mTargetQiChangId == self.SelectedQiChang.ID
    --local isJieBan = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId == self.SelectedQiChang.ID
    --local isOwnBaby = self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id
    
    local needShow = not self.mIsAlreadyJianDing and not isTarget
    self.SetTargetRoot:SetActive(needShow)
    local isRecormmend = LuaBabyMgr.mRecormmendQiChangId and LuaBabyMgr.mRecormmendQiChangId == self.SelectedQiChang.ID

    self.BtnRecormmendTag:SetActive(isRecormmend)
    UIEventListener.Get(self.SetTargetBtn).onClick = DelegateFactory.VoidDelegate(function()
        self:OnSetTargetBtnClick()
    end)
end

function LuaBabyQiChangRequest2023Tip:InitSetJieBanRoot()
    local isJieBan = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId == self.SelectedQiChang.ID
    local isOwnBaby = self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id
    
    if not isJieBan and self.mIsAlreadyJianDing and isOwnBaby or self.mIsAlreadyJianDing and  not isOwnBaby then
        self.SetJieBanRoot:SetActive(true)
        UIEventListener.Get(self.SetJieBanBtn).onClick = DelegateFactory.VoidDelegate(function()
            local babyId = self.SelectedBaby.Id
            CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
            Gac2Gas.SetJieBanQiChang(babyId, self.SelectedQiChang.ID)
        end)
    else
        self.SetJieBanRoot:SetActive(false)
    end
end

function LuaBabyQiChangRequest2023Tip:InitJianDingRoot()
    self.JianDingRoot:SetActive(false)
    
    -- 和引导有冲突，砍了
    --local isJieBan = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId == self.SelectedQiChang.ID
    --self.JianDingRoot:SetActive(self.mIsDaiJianDing and not isJieBan)
    --UIEventListener.Get(self.SetJianDingBtn).onClick = DelegateFactory.VoidDelegate(function()
    --    CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
    --    CUIManager.CloseUI(CLuaUIResources.BabyQiChangShuWnd)
    --end)
end

function LuaBabyQiChangRequest2023Tip:InitAlreadyTargetRoot()
    local isTarget = LuaBabyMgr.mTargetQiChangId and LuaBabyMgr.mTargetQiChangId == self.SelectedQiChang.ID
    UIEventListener.Get(self.CancelBtn).onClick = DelegateFactory.VoidDelegate(function()
        local babyId = self.SelectedBaby.Id
        Gac2Gas.RequestSetTargetQiChang(babyId, 0)
    end)
    self.AlreadyTargetRoot:SetActive(isTarget and not self.mIsAlreadyJianDing)
end

function LuaBabyQiChangRequest2023Tip:InitAlreadyJieBanRoot()
    local isOwnBaby = self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id
    local isJieBan = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId == self.SelectedQiChang.ID
    self.AlreadyJieBanRoot:SetActive(isJieBan and isOwnBaby and self.mIsAlreadyJianDing)
end

function LuaBabyQiChangRequest2023Tip:OnSetTargetBtnClick()
    local babyId = self.SelectedBaby.Id
    if self:CheckHasWeiJianDingPreQiChang() then
        LuaBabyMgr.OpenSetTargetQiChangWnd(self.SelectedQiChang.ID)
        g_ScriptEvent:BroadcastInLua("FindAndGotoQiChang", 0)
        CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
    else
        Gac2Gas.RequestSetTargetQiChang(babyId, self.SelectedQiChang.ID)
        --CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
    end
end

function LuaBabyQiChangRequest2023Tip:CheckHasWeiJianDingPreQiChang()
    local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
    local data = Baby_QiChang.GetData(self.SelectedQiChang.ID)
    while (data and data.PreQichang>0) do
        --有未鉴定的前置气场
        if not CommonDefs.DictContains_LuaCall(activedQiChang, data.PreQichang) then
            return true
        end
        data = Baby_QiChang.GetData(data.PreQichang)
    end

    return false
end

function LuaBabyQiChangRequest2023Tip:UpdateQiChangRequest()
    local title = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
    local titleLabel = title:GetComponent(typeof(UILabel))
    titleLabel.text = LocalString.GetString("解锁条件")
    title:SetActive(true)

    -- 已经解锁的条件放在前面
    local requestTips = {}

    local preQichangRequest = {}
    local satisfied = {}
    local notSatisfied = {}

    local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
    -- step 1 前置气场
    if self.SelectedQiChang.PreQichang ~= 0 then
        local preQiChang = Baby_QiChang.GetData(self.SelectedQiChang.PreQichang)
        if preQiChang then
            local str = SafeStringFormat3(LocalString.GetString("解锁气场[%s]【%s】[-]"), LuaBabyMgr.GetQiChangNameColor(preQiChang.Quality-1), self:GetQiChangName(preQiChang))
            -- 检查前置气场是否鉴定过
            if CommonDefs.DictContains_LuaCall(activedQiChang, preQiChang.ID) then
                table.insert(requestTips, { tip = str, IsActive = true})
                table.insert(preQichangRequest,{ tip = str, IsActive = true}) 
            else
                table.insert(requestTips, { tip = str, IsActive = false})
                table.insert(preQichangRequest,{ tip = str, IsActive = false}) 
            end
        end
        
    end

    -- Step 2 获得奇遇
    local qiyuDict = self.SelectedBaby.Props.ScheduleData.QiYuData

    if self.SelectedQiChang.QiyuGet ~= 0 then
        local qiyu = Baby_QiYu.GetData(self.SelectedQiChang.QiyuGet)
        if qiyu then
            local str = SafeStringFormat3(LocalString.GetString("解锁奇遇%s"), qiyu.QiYuName)
            if CommonDefs.DictContains_LuaCall(qiyuDict, qiyu.ID) then
                table.insert(requestTips, { tip = str, IsActive = true})
                table.insert(satisfied, str)
            else
                table.insert(requestTips, { tip = str, IsActive = false})
                table.insert(notSatisfied, str)
            end
        end
    end

    -- Step 3 属性要求, 有12个
    for i = 1, 12 do
        if self:GetQiChangRequestPropByIndex(i, self.SelectedQiChang) > 0 then
            local request = self:GetQiChangRequestPropByIndex(i, self.SelectedQiChang)
            local own = 0
            if self.SelectedBaby.Props.YoungProp and CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, i) then
                own = self.SelectedBaby.Props.YoungProp[i]
            end
            local shaoNian = Baby_ShaoNian.GetData(i)
            local youEr = Baby_YouEr.GetData(shaoNian.YouErName)
            if shaoNian and youEr then
                
                local str = SafeStringFormat3(LocalString.GetString("%s-%s达到 ？？？"), youEr.Name, shaoNian.Name) 
                if self.SelectedQiChang.Quality <= 3 then
                    str = SafeStringFormat3(LocalString.GetString("%s-%s达到%d (%d/%d)"), youEr.Name, shaoNian.Name, request, own, request)
                end
                
                if own >= request then
                    table.insert(requestTips, { tip = str, IsActive = true})
                    table.insert(satisfied, str)
                else
                    table.insert(requestTips, { tip = str, IsActive = false})
                    table.insert(notSatisfied, str)
                end
            end
        end
    end

    -- Step 4 属性最高
    if self.SelectedQiChang.TopAttribute ~= 0 and self.SelectedQiChang.Quality >= 5 then
        local youer = Baby_YouEr.GetData(self.SelectedQiChang.TopAttribute)

        if youer and self.SelectedBaby.Props.YoungProp then
            local str = SafeStringFormat3(LocalString.GetString("最高属性%s"), youer.Name)
            local highest = 0

            if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, self.SelectedQiChang.TopAttribute) then
                highest = LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, self.SelectedQiChang.TopAttribute)
            end
            local isHighest = true

            if self.SelectedBaby.Props.YoungProp and self.SelectedBaby.Props.YoungProp.Count >= 12 then
                for i = 1, 6, 1 do
                    if self.SelectedQiChang.TopAttribute ~=i and LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, i) > highest then
                        isHighest = false
                    end
                end
            end
            if isHighest then
                table.insert(requestTips, { tip = str, IsActive = true})
                table.insert(satisfied, str)
            else
                table.insert(requestTips, { tip = str, IsActive = false})
                table.insert(notSatisfied, str)
            end
        end
    end

    -- 创建列表
    for i=1,#preQichangRequest do
        local info = preQichangRequest[i]
        --table.insert(preQichangRequest,{ tip = str, IsActive = true}) 
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.PreQiChangTemplate)
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))

        label.text = info.IsActive and info.tip or SafeStringFormat3("[777777]%s[-]", info.tip)
        go:SetActive(true)

        local SatisfiedTag = go.transform:Find("SatisfiedTag").gameObject
        local UnSatisfiedTag = go.transform:Find("UnSatisfiedTag").gameObject
        local ExpandBtn = go.transform:Find("ExpandBtn").gameObject
        SatisfiedTag:SetActive(info.IsActive)
        UnSatisfiedTag:SetActive(not info.IsActive)
        ExpandBtn:SetActive(true)

        UIEventListener.Get(ExpandBtn).onClick = DelegateFactory.VoidDelegate(function()
            self:OnExpandBtnBtnClick()
        end)
    end

    for i = 1, #satisfied do
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.RequestTemplate)
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = satisfied[i]
        go:SetActive(true)

        local SatisfiedTag = go.transform:Find("SatisfiedTag").gameObject
        local UnSatisfiedTag = go.transform:Find("UnSatisfiedTag").gameObject
        SatisfiedTag:SetActive(true)
        UnSatisfiedTag:SetActive(false)
    end

    for i = 1, #notSatisfied do
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.RequestTemplate)
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = SafeStringFormat3("[777777]%s[-]", notSatisfied[i])
        go:SetActive(true)

        local SatisfiedTag = go.transform:Find("SatisfiedTag").gameObject
        local UnSatisfiedTag = go.transform:Find("UnSatisfiedTag").gameObject
        SatisfiedTag:SetActive(false)
        UnSatisfiedTag:SetActive(true)
    end

    if CommonDefs.DictContains_LuaCall(activedQiChang, self.SelectedQiChang.ID) then
        self.mIsUnlocked = true
    else
        if #notSatisfied > 0 then
            self.mIsUnlocked = false
        else
            self.mIsUnlocked = true
        end
    end
end


function LuaBabyQiChangRequest2023Tip:GetQiChangRequestPropByIndex(index, qichang)
    if index == 1 then
        return qichang.Strategy
    elseif index == 2 then
        return qichang.Taoism
    elseif index == 3 then
        return qichang.Martial
    elseif index == 4 then
        return qichang.Military
    elseif index == 5 then
        return qichang.Poetry
    elseif index == 6 then
        return qichang.Yayi
    elseif index == 7 then
        return qichang.Heaven
    elseif index == 8 then
        return qichang.Process
    elseif index == 9 then
        return qichang.Virtue
    elseif index == 10 then
        return qichang.Ambiguity
    elseif index == 11 then
        return qichang.Tyrannical
    elseif index == 12 then
        return qichang.Evil
    end
    return ""
end

function LuaBabyQiChangRequest2023Tip:UpdateQiChangReward()
	local qiChangReward
	if self.SelectedBaby.Gender == EnumToInt(EnumGender.Male) then
		qiChangReward = self.SelectedQiChang.RewardM
	else
		qiChangReward = self.SelectedQiChang.RewardF
	end
    if not qiChangReward or qiChangReward.Length <= 0 then
        return
    end

    local title = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
    local titleLabel = title:GetComponent(typeof(UILabel))
    titleLabel.text = LocalString.GetString("解锁奖励")
    title:SetActive(true)

    local line = CUICommonDef.AddChild(self.Table.gameObject, self.Line)
    line:SetActive(true)

    local reward = CUICommonDef.AddChild(self.Table.gameObject, self.RewardTemplate)
    reward:SetActive(true)
    local rewardTable = reward.transform:Find("RewardTable"):GetComponent(typeof(UIGrid))
    local rewardTemplate = reward.transform:Find("Reward").gameObject
    rewardTemplate:SetActive(false)

    for i = 0, qiChangReward.Length-1, 2 do
        local go = CUICommonDef.AddChild(rewardTable.gameObject, rewardTemplate)
        self:InitRewardItem(go, qiChangReward[i])
        go:SetActive(true)
    end
    rewardTable:Reposition()

end

function LuaBabyQiChangRequest2023Tip:InitRewardItem(go, itemTId)
    local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    IconTexture:Clear()
    local item = Item_Item.GetData(itemTId)
    if item then
        IconTexture:LoadMaterial(item.Icon)
        CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemTId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        end), false)
    end
end

function LuaBabyQiChangRequest2023Tip:GetQiChangName(qichang)
    local name = qichang.NameM
    if self.SelectedBaby.Gender == 1 then
        name = qichang.NameF
    end
    return name
end

function LuaBabyQiChangRequest2023Tip:Update()
    self:ClickThroughToClose()
end

function LuaBabyQiChangRequest2023Tip:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
            --取消选择气场
            g_ScriptEvent:BroadcastInLua("FindAndGotoQiChang", 0)
        end
    end
end

function LuaBabyQiChangRequest2023Tip:OnExpandBtnBtnClick()
    self.QiChangTreeRoot:SetActive(not self.QiChangTreeRoot.activeSelf)

    if not self.QiChangTreeRoot.activeSelf then
        return
    end
    Extensions.RemoveAllChildren(self.TreeTable.transform)
    local tree = LuaBabyMgr.MakeQiChangTree(self.SelectedQiChang.ID)
    local lastActiveId = tree[1]
    local lastActiveGo = nil

    self.QiChangTreeView.transform:GetComponent(typeof(UISprite)).height = 130 + 80 * #tree

    for i,treeId in ipairs(tree) do
        local go = CUICommonDef.AddChild(self.TreeTable.gameObject, self.TreeTemplate)
        go:SetActive(true)

        local Arrow = go.transform:Find("Arrow").gameObject
        local Lock = go.transform:Find("Lock").gameObject
        local label = go.transform:GetComponent(typeof(UILabel))
        local Sprite = go.transform:Find("Sprite").gameObject
        
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function()
            --CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
            if CUIManager.IsLoaded(CLuaUIResources.SetTargetQiChangWnd) then
                CUIManager.CloseUI(CLuaUIResources.SetTargetQiChangWnd)
            end
            g_ScriptEvent:BroadcastInLua("FindAndGotoQiChang", treeId)
        end)

        local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
        local IsActive = CommonDefs.DictContains_LuaCall(activedQiChang, treeId)
        if not IsActive then
            Lock:SetActive(true)
            Arrow:SetActive(false)
            label.alpha = 0.6
        else
            if treeId > lastActiveId then
                lastActiveId = treeId
                lastActiveGo = go
            end
            label.alpha = 1
            if i>1 then
                Lock:SetActive(false)
                Arrow:SetActive(true)
            else
                Lock:SetActive(false)
                Arrow:SetActive(false)
            end
        end
        
        local data = Baby_QiChang.GetData(treeId)
        label.text = SafeStringFormat3(LocalString.GetString("[%s]【%s】[-]"), LuaBabyMgr.GetQiChangNameColor(data.Quality-1), self:GetQiChangName(data))
        Sprite:SetActive(false)
    end

    if lastActiveGo ~= nil then
        local Sprite = lastActiveGo.transform:Find("Sprite").gameObject
        Sprite:SetActive(true)
    end
    self.TreeTable:Reposition()
    self.HeaderTree:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.Tree:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
end

function LuaBabyQiChangRequest2023Tip:GetGuideGo(methodName)
    if methodName=="SetTargetQiChangButton" then
        return self.SetTargetBtn
    end
    return nil
end
return LuaBabyQiChangRequest2023Tip