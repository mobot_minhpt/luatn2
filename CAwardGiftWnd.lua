-- Auto Generated!!
local CAwardGiftWnd = import "L10.UI.CAwardGiftWnd"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CHongBaoSettings = import "L10.UI.CHongBaoSettings"
local CommonDefs = import "L10.Game.CommonDefs"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DaoJuLiBao_Item = import "L10.Game.DaoJuLiBao_Item"
local DaoJuLiBao_Setting = import "L10.Game.DaoJuLiBao_Setting"
local EnumEventType = import "EnumEventType"
local EnumRequestSource = import "L10.UI.EnumRequestSource"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CAwardGiftWnd.m_Init_CS2LuaHook = function (this) 
    this.m_ChannelType = EnumLibaoType_lua.GuildLibao
    this.m_ChannelTypeGroup:ChangeTo(0, true)
    this.m_GiftType = 0
    this.m_GiftTypeGroup:ChangeTo(this.m_GiftType, true)
    for i = 0, 2 do
        this.m_GiftTypeIcon[i]:LoadMaterial(DaoJuLiBao_Item.GetData(DaoJuLiBao_Setting.GetData().World_LingYu_Option[i]).Icon)
    end

    Gac2Gas.RequestRemainHongBaoSendTimes(EnumToInt(CHongBaoMgr.m_HongbaoType))
    this.m_ContentLabel.text = System.String.Format(LocalString.GetString("请输入礼包留言（最多{0}个字）"), DaoJuLiBao_Setting.GetData().Max_Contents_Length)
end
CAwardGiftWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    this.m_AwardButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_AwardButton.OnClick, MakeDelegateFromCSFunction(this.OnAwardButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_TipButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TipButton.OnClick, MakeDelegateFromCSFunction(this.OnTipButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_WishInput.OnValueChanged = CommonDefs.CombineListner_Action_string(this.m_WishInput.OnValueChanged, MakeDelegateFromCSFunction(this.OnInputChanged, MakeGenericClass(Action1, String), this), true)
    this.m_ChannelTypeGroup.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_ChannelTypeGroup.OnSelect, MakeDelegateFromCSFunction(this.OnChannelTypeSelected, MakeGenericClass(Action2, QnButton, Int32), this), true)
    this.m_GiftTypeGroup.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_GiftTypeGroup.OnSelect, MakeDelegateFromCSFunction(this.OnGiftTypeSelected, MakeGenericClass(Action2, QnButton, Int32), this), true)
    this.m_HongBaoButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_HongBaoButton.onValueChanged, MakeDelegateFromCSFunction(this.OnCountChanged, MakeGenericClass(Action1, UInt32), this), true)

    EventManager.AddListenerInternal(EnumEventType.ReplyRemainHongBaoSendTimes, MakeDelegateFromCSFunction(this.UpdateRemainTimes, MakeGenericClass(Action2, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.ReplySendGiftResult, MakeDelegateFromCSFunction(this.AwardGiftResult, MakeGenericClass(Action2, UInt32, String), this))
end
CAwardGiftWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    this.m_AwardButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_AwardButton.OnClick, MakeDelegateFromCSFunction(this.OnAwardButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_TipButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TipButton.OnClick, MakeDelegateFromCSFunction(this.OnTipButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_WishInput.OnValueChanged = CommonDefs.CombineListner_Action_string(this.m_WishInput.OnValueChanged, MakeDelegateFromCSFunction(this.OnInputChanged, MakeGenericClass(Action1, String), this), false)
    this.m_ChannelTypeGroup.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_ChannelTypeGroup.OnSelect, MakeDelegateFromCSFunction(this.OnChannelTypeSelected, MakeGenericClass(Action2, QnButton, Int32), this), false)
    this.m_GiftTypeGroup.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_GiftTypeGroup.OnSelect, MakeDelegateFromCSFunction(this.OnGiftTypeSelected, MakeGenericClass(Action2, QnButton, Int32), this), false)
    this.m_HongBaoButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_HongBaoButton.onValueChanged, MakeDelegateFromCSFunction(this.OnCountChanged, MakeGenericClass(Action1, UInt32), this), false)

    EventManager.RemoveListenerInternal(EnumEventType.ReplyRemainHongBaoSendTimes, MakeDelegateFromCSFunction(this.UpdateRemainTimes, MakeGenericClass(Action2, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.ReplySendHongBaoResult, MakeDelegateFromCSFunction(this.AwardGiftResult, MakeGenericClass(Action2, UInt32, String), this))
end
CAwardGiftWnd.m_OnChannelTypeSelected_CS2LuaHook = function (this, checkBox, index) 
    this.m_ChannelType = index
    if index == 0 then
        this.m_ChannelType = EnumLibaoType_lua.GuildLibao
    end
    if this.m_ChannelType == EnumLibaoType_lua.WorldLibao then
        this.m_HongBaoButton:SetMinMax(DaoJuLiBao_Setting.GetData().Number_Min_World, DaoJuLiBao_Setting.GetData().Number_Max_World, 1)
        this.m_HongBaoButton:SetValue(DaoJuLiBao_Setting.GetData().Number_Min_World, true)
    else
        this.m_HongBaoButton:SetMinMax(DaoJuLiBao_Setting.GetData().Number_Min_Guild, DaoJuLiBao_Setting.GetData().Number_Max_Guild, 1)
        this.m_HongBaoButton:SetValue(DaoJuLiBao_Setting.GetData().Number_Min_Guild, true)
    end
    Gac2Gas.RequestRemainDaoJuLiBaoSendTimes(this.m_ChannelType)
end
CAwardGiftWnd.m_OnGiftTypeSelected_CS2LuaHook = function (this, checkBox, index) 
    this.m_GiftType = index
    if this.m_ChannelType == EnumLibaoType_lua.WorldLibao then
        this.m_Price = DaoJuLiBao_Setting.GetData().World_LingYu_Option[index]
    else
        this.m_Price = DaoJuLiBao_Setting.GetData().Guild_LingYu_Option[index]
    end
    this.m_TipLabel.text = DaoJuLiBao_Item.GetData(this.m_Price).Description
    this.m_ConsumeJadeLabel.text = tostring((this.m_Price * this.m_HongBaoButton:GetValue()))
end
CAwardGiftWnd.m_OnAwardButtonClick_CS2LuaHook = function (this, button) 
    this.m_WishContents = this.m_WishInput.Text
    if CommonDefs.StringLength(this.m_WishContents) > DaoJuLiBao_Setting.GetData().Max_Contents_Length then
        g_MessageMgr:ShowMessage("Hongbao_Input_Limit", CHongBaoSettings.Max_Contents_Length)
        return
    end

    if CommonDefs.StringLength(this.m_WishContents) > 0 then
        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(this.m_WishContents, true)
        if ret.msg == nil then
            return
        else
            this.m_WishContents = ret.msg
        end
    else
        this.m_WishContents = DaoJuLiBao_Item.GetData(this.m_Price).Message
    end
    local total = this.m_Price * this.m_HongBaoButton:GetValue()
    local text = (LocalString.GetString("是否花费") .. total) .. LocalString.GetString("灵玉发礼包？")
    MessageWndManager.ShowOKCancelMessage(text, MakeDelegateFromCSFunction(this.OnOkButtonClick, Action0, this), nil, nil, nil, false)
end
CAwardGiftWnd.m_AwardGiftResult_CS2LuaHook = function (this, status, reason) 
    if status ~= 0 then
        return
    end
    CHongBaoMgr.m_RequestSource = EnumRequestSource.RefreshRequest
    Gac2Gas.RequestRecentDaoJuLiBaoInfo()
    this:Close()
end
