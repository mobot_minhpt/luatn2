local UIGrid = import "UIGrid"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local Extensions = import "Extensions"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaRunningWildTangYuanSignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "RuleScrollView", "RuleScrollView", UIScrollView)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "ParagraphTemplate", "ParagraphTemplate", CTipParagraphItem)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "OpenTimeLabel", "OpenTimeLabel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "MatchingBottom", "MatchingBottom", GameObject)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "CacelMatchBtn", "CacelMatchBtn", GameObject)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "DailyGift", "DailyGift", GameObject)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "DailyGiftIcon", "DailyGiftIcon", CUITexture)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "SignBtn", "SignBtn", GameObject)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "RewardTimesLabel", "RewardTimesLabel", UILabel)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "RankGiftGrid", "RankGiftGrid", UIGrid)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "RankGift", "RankGift", GameObject)
RegistChildComponent(LuaRunningWildTangYuanSignWnd, "DailyGiftLabel", "DailyGiftLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaRunningWildTangYuanSignWnd,"m_GamePlayId")

function LuaRunningWildTangYuanSignWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)

	UIEventListener.Get(self.CacelMatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCacelMatchBtnClick()
	end)

    --@endregion EventBind end
	UIEventListener.Get(self.SignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignBtnClick()
	end)
	self.m_GamePlayId = YuanXiao_TangYuan2022Setting.GetData().GamePlayId
	self.RankGift:SetActive(false)
end

function LuaRunningWildTangYuanSignWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("WHCB_QueryRewardResult", self, "OnWHCB_QueryRewardResult")
    
end
function LuaRunningWildTangYuanSignWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaRunningWildTangYuanSignWnd:Init()
    if CClientMainPlayer.Inst then
		Gac2Gas.GlobalMatch_RequestCheckSignUp(self.m_GamePlayId,CClientMainPlayer.Inst.Id)
	end

	self.ParagraphTemplate.gameObject:SetActive(false)
	local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(g_MessageMgr:FormatMessage("RunningWild_TangYuan_Rule"))
	Extensions.RemoveAllChildren(self.RuleTable.transform)
	if info ~= nil then
        do
            local i = 0
            while i < info.paragraphs.Count do
                local paragraphGo = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate.gameObject)
                paragraphGo:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
                i = i + 1
            end
        end
        self.RuleTable:Reposition()
    end

	local setting = YuanXiao_TangYuan2022Setting.GetData()
	local openDateString = setting.OpenDateString
	self.OpenTimeLabel.text = openDateString

	--dauly gift
	local maxRewardTimes = setting.DailyPlayRewardTimesLimit
	local rewardedTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eTangYuan2022DailyRewardTimes)
	self.DailyGiftLabel.text = SafeStringFormat3(LocalString.GetString("今日参赛奖(%d/%d)"),rewardedTimes,maxRewardTimes)
	local dailyItemId = setting.DailyJoinedRewardItemIds
	local data1 = Item_Item.GetData(dailyItemId)
	if data1 then
		self.DailyGiftIcon:LoadMaterial(data1.Icon)
	end
	UIEventListener.Get(self.DailyGift.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(dailyItemId,false,nil,AlignType.Default, 0, 0, 0, 0)
	end)

	--rank gifts
	Extensions.RemoveAllChildren(self.RankGiftGrid.transform)
	local rankRewards = setting.TopRankRewardItemIds
	for i=0,rankRewards.Length-2,2 do
		local rank = rankRewards[i]
		local itemId = rankRewards[i+1]
		local award = NGUITools.AddChild(self.RankGiftGrid.gameObject,self.RankGift)
        award:SetActive(true)

		local rankLabel = award.transform:Find("RankGiftLabel"):GetComponent(typeof(UILabel))
		local icon = award.transform:Find("RankGiftIcon"):GetComponent(typeof(CUITexture))
		rankLabel.text = SafeStringFormat3(LocalString.GetString("前%d"),rank)
		local data = Item_Item.GetData(itemId)
		if data then
			icon:LoadMaterial(data.Icon)
		end
		UIEventListener.Get(award).onClick = DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
		end)
	end
end

--@region UIEvent

function LuaRunningWildTangYuanSignWnd:OnSignBtnClick()
	Gac2Gas.GlobalMatch_RequestSignUp(self.m_GamePlayId)
end

function LuaRunningWildTangYuanSignWnd:OnRankBtnClick()
	LuaTangYuan2022Mgr.OpenRankWnd()
end

function LuaRunningWildTangYuanSignWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("RunningWild_TangYuan_Tip")
end

function LuaRunningWildTangYuanSignWnd:OnCacelMatchBtnClick()
	Gac2Gas.GlobalMatch_RequestCancelSignUp(self.m_GamePlayId)
end

--@endregion UIEvent

function LuaRunningWildTangYuanSignWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId==self.m_GamePlayId then
        if isInMatching then
            self.SignBtn:SetActive(false)
            self.MatchingBottom:SetActive(true)
        else
            self.SignBtn:SetActive(true)
            self.MatchingBottom:SetActive(false)
        end
    end
end
function LuaRunningWildTangYuanSignWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId==self.m_GamePlayId and success==true then
        self.SignBtn:SetActive(false)
        self.MatchingBottom:SetActive(true)
    end
end
function LuaRunningWildTangYuanSignWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId==self.m_GamePlayId and success==true then
        self.SignBtn:SetActive(true)
        self.MatchingBottom:SetActive(false)
    end
end
