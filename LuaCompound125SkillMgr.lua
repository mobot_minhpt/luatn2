LuaCompound125SkillMgr = class()
LuaCompound125SkillMgr.m_125SkillItemId = nil
LuaCompound125SkillMgr.m_125EssenceId = nil
LuaCompound125SkillMgr.m_125Resource = nil
LuaCompound125SkillMgr.m_125ResourceType = nil
LuaCompound125SkillMgr.m_125ResourceCount = nil

function LuaCompound125SkillMgr:ShowCompound125SkillItem(itemId)
    self.m_125SkillItemId = itemId
    local data = Skill_Setting.GetData()
    local essenceData = data.Exchange125SkillItem
    local resData = data.Exchange125SkillScoreDes

    local tb = {}
    local i = 1
    CommonDefs.DictIterate(resData, DelegateFactory.Action_object_object(function (key,val) 
        local des = val
        local a, b, count, type = string.find(des, "(%d+)(.+)")
        tb[key] = {des = val,type = i,count = tonumber(count)}
        i = i + 1
    end))

    if CommonDefs.DictContains(essenceData, typeof(UInt32), itemId) and CommonDefs.DictContains(resData, typeof(UInt32), itemId) then
        self.m_125EssenceId = essenceData[itemId]
        self.m_125Resource = tb[itemId].des
        self.m_125ResourceCount = tb[itemId].count
        self.m_125ResourceType = tb[itemId].type
        CUIManager.ShowUI(CLuaUIResources.Compound125SkillItemWnd)
    end
end