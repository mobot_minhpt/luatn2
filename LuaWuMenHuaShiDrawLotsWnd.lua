local Animation       = import "UnityEngine.Animation"
local TweenAlpha      = import "TweenAlpha"

LuaWuMenHuaShiDrawLotsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

--@endregion RegistChildComponent end

RegistClassMember(LuaWuMenHuaShiDrawLotsWnd, "name")
RegistClassMember(LuaWuMenHuaShiDrawLotsWnd, "qianTongAni")
RegistClassMember(LuaWuMenHuaShiDrawLotsWnd, "qianAni")
RegistClassMember(LuaWuMenHuaShiDrawLotsWnd, "bg")

RegistClassMember(LuaWuMenHuaShiDrawLotsWnd, "qianTongClipName")
RegistClassMember(LuaWuMenHuaShiDrawLotsWnd, "qianClipName")
RegistClassMember(LuaWuMenHuaShiDrawLotsWnd, "tick")

function LuaWuMenHuaShiDrawLotsWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
end

function LuaWuMenHuaShiDrawLotsWnd:InitUIComponents()
    self.name = self.transform:Find("Qian/Name"):GetComponent(typeof(UILabel))
    self.qianTongAni = self.transform:Find("QianTong"):GetComponent(typeof(Animation))
    self.qianAni = self.transform:Find("Qian"):GetComponent(typeof(Animation))
    self.bg = self.transform:Find("Bg"):GetComponent(typeof(TweenAlpha))
end


function LuaWuMenHuaShiDrawLotsWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerShakeDevice", self, "OnPlayerShakeDevice")
end

function LuaWuMenHuaShiDrawLotsWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerShakeDevice", self, "OnPlayerShakeDevice")
end

-- 玩家晃动手机
function LuaWuMenHuaShiDrawLotsWnd:OnPlayerShakeDevice()
    self:StartTick()
    self:DoAni()
    self.bg:PlayForward()
end

function LuaWuMenHuaShiDrawLotsWnd:Init()
    self:InitName()
    self:InitAni()
    self.bg:ResetToBeginning()

    g_MessageMgr:ShowMessage("WuMenHuaShi_ChouQian")
end

-- 初始化人名
function LuaWuMenHuaShiDrawLotsWnd:InitName()
    local lotId = LuaWuMenHuaShiMgr.lotId
    if lotId == nil then return end

    if lotId == 1 then
        self.name.text = LocalString.GetString("小\n\n倩")
    elseif lotId == 2 then
        self.name.text = LocalString.GetString("判\n\n官")
    end
end

-- 初始化动画
function LuaWuMenHuaShiDrawLotsWnd:InitAni()
    self.qianTongClipName = "WMHSQiuQian_QianTong"
    self.qianClipName = "WMHSQiuQian_Qian"

    self:SampleAni(self.qianTongAni, self.qianTongClipName, 0)
    self:SampleAni(self.qianAni, self.qianClipName, 0)
end

function LuaWuMenHuaShiDrawLotsWnd:StartTick()
    self.tick = RegisterTickOnce(function()
        local lotId = LuaWuMenHuaShiMgr.lotId
        if lotId == 1 then
            LuaWuMenHuaShiMgr:SendFinishEvent(22111657, "LZChouQian1", true)
        elseif lotId == 2 then
            LuaWuMenHuaShiMgr:SendFinishEvent(22111671, "LZChouQian2", true)
        end

		CUIManager.CloseUI(CLuaUIResources.WuMenHuaShiDrawLotsWnd)
	end, 1000 * 5)
end

-- 清除计时器
function LuaWuMenHuaShiDrawLotsWnd:ClearTick()
	if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

function LuaWuMenHuaShiDrawLotsWnd:SampleAni(anim, clipName, time)
	anim:get_Item(clipName).time = time
    anim:get_Item(clipName).enabled = true
    anim:get_Item(clipName).weight = 1
    anim:Sample()
    anim:get_Item(clipName).enabled = false
end

function LuaWuMenHuaShiDrawLotsWnd:DoAni()
    self.qianTongAni:get_Item(self.qianTongClipName).time = 0
    self.qianAni:get_Item(self.qianClipName).time = 0
    self.qianTongAni:Play()
    self.qianAni:Play()
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.YuanDanYaoQianound, Vector3.zero, nil, 0)
end

function LuaWuMenHuaShiDrawLotsWnd:OnDestroy()
    self:ClearTick()
end

--@region UIEvent

--@endregion UIEvent

