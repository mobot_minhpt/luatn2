local EnumEventType=import "EnumEventType"
local CFightingSpiritLiveWatchWndMgr = import "L10.UI.CFightingSpiritLiveWatchWndMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CGameVideoCharacterMgr = import "L10.Game.CGameVideoCharacterMgr"
local CCppBuff = import "L10.Game.CCppBuff"
local Gas2Gac = import "L10.Game.Gas2Gac"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CGameVideoPlayer = import "L10.Game.CGameVideoPlayer"
local GameVideoLuaRecord = import "L10.Game.GameVideoLuaRecord"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Lua = import "L10.Engine.Lua"
local CClientFakePlayer = import "L10.Game.CClientFakePlayer"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientPet = import "L10.Game.CClientPet"
local CClientLingShou = import "L10.Game.CClientLingShou"
local CGameVideoLingShou = import "L10.Game.CGameVideoLingShou"
local CGameVideoPet = import "L10.Game.CGameVideoPet"
local EnumCommonForce = import "L10.UI.EnumCommonForce"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Constants = import "L10.Game.Constants"
local CMainCamera = import "L10.Engine.CMainCamera"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"

CLuaGameVideoMgr={}

EnumRecordType={
    eSyncYingLingState = 1035,
    eAddFxToObject = 1036,
    eDieKeDoPossessOther = 1037,
    eDieKeDoPossessByOther = 1038,
    eDieKeUnPossessOther = 1039,
    eDieKeUnPossessByOther = 1040,
    eAddWorldPositionFxWithDir = 1041,
    eShowReverseSkillFx = 1042,
	eUpdatePlayerBackPendantAppearance = 1043,

    -- eStartPlayCountDown = 8008,
    -- eShowPlayStartFx = 8010,
    ePlayQmpkUpdateZhanDuiRoundData = 8013,
    eQmpkFightAndSkillDataStat = 8017,
	eMirrorWarWatchMapInfo = 8022,
	eMirrorWarForceInfo = 8023,
	eMirrorWarOccupyInfo = 8024,
	eMirrorWarPlaySummary = 8025,
    eCityOccupyingProgress = 8026,
    eAddBuffAllData = 8027,
    ePlayStarBiwuUpdateZhanDuiRoundData = 8028,
	eStarBiwuFightAndSkillDataStat = 8029,

    eStarBiwuFightKillInfo = 8030,
    eStarBiwuFightWinInfo = 8031,

    eDieKeClearPossessByOther = 8032,
    ePlayerLeaveSaiMaJumpRegion = 8033,
    eStarBiwuFightZhuWeiInfo = 8034,
    eStarBiwuFightChampionInfo = 8035,
    eStarBiwuFightGiftRank = 8036,
    eStarBiwuFightSendGift = 8037,
    eStarBiwuFightZhanduiId = 8038,
    -- eObjectRmBuffFx = 1011,

    eDouHunCrossPlayChampionInfo = 8039,
	eDouHunPlayMatchIndex = 8040,
	eQmpkBanPickData = 8041,
}

CLuaGameVideoMgr.m_CacheKey={
    [EnumRecordType.eSyncYingLingState]=true,
    [EnumRecordType.eAddFxToObject]=true,
    [EnumRecordType.eDieKeDoPossessOther]=true,
    [EnumRecordType.eDieKeDoPossessByOther]=true,
    [EnumRecordType.eDieKeUnPossessOther]=true,
    [EnumRecordType.eDieKeUnPossessByOther]=true,
    [EnumRecordType.eAddWorldPositionFxWithDir]=true,
    [EnumRecordType.eShowReverseSkillFx]=true,
	[EnumRecordType.eUpdatePlayerBackPendantAppearance]=true,

    [EnumRecordType.ePlayQmpkUpdateZhanDuiRoundData]=true,
    [EnumRecordType.eQmpkFightAndSkillDataStat]=true,
	[EnumRecordType.eMirrorWarWatchMapInfo]=true,
	[EnumRecordType.eMirrorWarForceInfo]=true,
	[EnumRecordType.eMirrorWarOccupyInfo]=true,
	[EnumRecordType.eMirrorWarPlaySummary]=true,
	[EnumRecordType.eCityOccupyingProgress]=true,
    [EnumRecordType.ePlayStarBiwuUpdateZhanDuiRoundData]=true,
    [EnumRecordType.eStarBiwuFightAndSkillDataStat]=true,

    [EnumRecordType.eDieKeClearPossessByOther]=true,
    [EnumRecordType.eQmpkBanPickData]=true,

}

CLuaGameVideoMgr.m_CacheFunc={}--对cache的数据进行初始化

CLuaGameVideoMgr.m_RecordFrameDataCached={}
CLuaGameVideoMgr.m_RecordHanderTable={}

function DispatchLuaRecord(mgr,recordType,invoke,frameTime,data)
    if CLuaGameVideoMgr.m_CacheKey[recordType] then
        CLuaGameVideoMgr.m_RecordFrameDataCached[recordType]=data
        if CLuaGameVideoMgr.m_CacheFunc[recordType] then
            CLuaGameVideoMgr.m_CacheFunc[recordType]()
        end
    end
    if invoke then
        if CLuaGameVideoMgr.m_RecordHanderTable[recordType] then
            CLuaGameVideoMgr.m_RecordHanderTable[recordType](data)
        end
    end
end

--=============================================================================================================
--cache func的作用主要是初始化数据

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eSyncYingLingState]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eSyncYingLingState]
    if not cacheData then return end


    local originalEngineId = cacheData[1]
    local engineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(originalEngineId)
    local yingLingState = cacheData[2]
    local expiredTime = cacheData[3]

    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if not obj or not obj.AppearanceProp then return end

    obj.AppearanceProp.YingLingState = yingLingState
    obj:RefreshAppearance()
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eAddFxToObject]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eAddFxToObject]
    if not cacheData then return end


    local originalEngineId = cacheData[1]
    local engineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(originalEngineId)
    local fxId = cacheData[2]

    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if not obj then return end

    Gas2Gac.m_Instance:AddFxToObject(engineId, fxId)
end

-------------------------------------------------------------------------------------------

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eDieKeDoPossessOther]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eDieKeDoPossessOther]
    if not cacheData then return end


    local engineId, targetEngineId, expiredTime = cacheData[1], cacheData[2], cacheData[3]

    local fake_EngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(engineId)
    LuaSkillMgr:DieKeDoPossessOther(fake_EngineId, targetEngineId, expiredTime)
end


CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eDieKeDoPossessByOther]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eDieKeDoPossessByOther]
    if not cacheData then return end


    local engineId, targetEngineId, aggressivePossessCount, positivePossessCount, isEnemy = cacheData[1], cacheData[2], cacheData[3], cacheData[4], cacheData[5]
    
    local fake_TargetEngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(targetEngineId)

    CLuaFightingSpiritMgr:CacheFuncDieKeDoPossessByOther(engineId, fake_TargetEngineId, aggressivePossessCount, positivePossessCount, isEnemy)
    Gas2Gac.m_Instance:DieKeDoPossessByOther(engineId, fake_TargetEngineId, aggressivePossessCount, positivePossessCount, isEnemy)
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eDieKeUnPossessOther]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eDieKeUnPossessOther]
    if not cacheData then return end


    local engineId, targetEngineId = cacheData[1], cacheData[2]

    local fake_EngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(engineId)

    Gas2Gac.m_Instance:DieKeUnPossessOther(fake_EngineId, targetEngineId)
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eDieKeUnPossessByOther]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eDieKeUnPossessByOther]
    if not cacheData then return end


    local engineId, targetEngineId, aggressivePossessCount, positivePossessCount = cacheData[1], cacheData[2], cacheData[3], cacheData[4]
    
    local fake_EngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(engineId)
    local fake_TargetEngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(targetEngineId)

    CLuaFightingSpiritMgr:CacheFuncDieKeUnPossessByOther(fake_EngineId, fake_TargetEngineId, aggressivePossessCount, positivePossessCount)
    Gas2Gac.m_Instance:DieKeUnPossessByOther(fake_EngineId, fake_TargetEngineId, aggressivePossessCount, positivePossessCount)
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eDieKeClearPossessByOther]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eDieKeClearPossessByOther]
    if not cacheData then return end


    local engineId = cacheData[1]
    
    local fake_EngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(engineId)

    Gas2Gac.m_Instance:DieKeClearPossessByOther(fake_EngineId)
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eAddWorldPositionFxWithDir]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eAddWorldPositionFxWithDir]
    if not cacheData then return end


    local selfPosX, selfPosY, fxId, dir = cacheData[1], cacheData[2], cacheData[3], cacheData[4]

    Gas2Gac.m_Instance:AddWorldPositionFxWithDir(selfPosX, selfPosY, 0,fxId, dir)
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eShowReverseSkillFx]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eShowReverseSkillFx]
    if not cacheData then return end


    local aEngineId, dEngineId, aFxId, dFxId, bulletFxId, aFxDelay, bFxDelay, bulletFxDelay = cacheData[1], cacheData[2], cacheData[3], cacheData[4], cacheData[5], cacheData[6], cacheData[7], cacheData[8]
    local fake_aEngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(aEngineId)
    local fake_dEngineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(dEngineId)

    Gas2Gac.m_Instance:ShowReverseSkillFx(fake_aEngineId, fake_dEngineId, aFxId, dFxId, bulletFxId, aFxDelay, bFxDelay, bulletFxDelay)
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eUpdatePlayerBackPendantAppearance]=function()
	local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eUpdatePlayerBackPendantAppearance]
	if not cacheData then return end

	local originalEngineId = cacheData[1]
	local engineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(originalEngineId)
	local appearance = cacheData[2]

	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if not obj then return end

	if TypeIs(obj, typeof(CGameVideoPlayer)) then
		local gameVideoPlayer = TypeAs(obj, typeof(CGameVideoPlayer))
		gameVideoPlayer:UpdatePlayerBackPendantAppearance(appearance)
	end
end

-------------------------------------------------------------------------------------------

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eQmpkFightAndSkillDataStat]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eQmpkFightAndSkillDataStat]
    if not cacheData then return end

    local data=cacheData[1]

    local parse=function(list,out)
        if list.Count < 6 then return end
        for i=2,list.Count,5 do
            local playerId=list[i-1]
            local clazz=list[i]
            local name=list[i+1]
            local fightdata=nil
            local rawFightData=list[i+2]
            --如果有数据
            fightdata={
                [1]=rawFightData[0],
                [2]=rawFightData[1],
                [3]=rawFightData[2],
                [4]=rawFightData[3],
                [5]=rawFightData[4],
                [6]=rawFightData[5],
                [7]=rawFightData[6]
            }
            local skilldata={}
            local rawSkillData=list[i+3]
            for i=1,rawSkillData.Count,2 do
                table.insert( skilldata, rawSkillData[i-1] )
                table.insert( skilldata, rawSkillData[i] )
            end
            table.insert( out,{playerId=playerId,clazz=clazz,name=name,fightdata=fightdata,skilldata=skilldata} )
        end
    end
    CLuaQMPKMgr.m_FightData = {}
    for i=1,data.Count do
        local roundData=data[i-1]
        if roundData.Count==2 then
            local list1=MsgPackImpl.unpack(CommonDefs.DictGetValue_LuaCall(roundData, 0))
            local list2=MsgPackImpl.unpack(CommonDefs.DictGetValue_LuaCall(roundData, 1))
            local data1={}
            local data2={}
            parse(list1,data1)
            parse(list2,data2)
            CLuaQMPKMgr.m_FightData[i]={[1]=data2,[2]=data1,name1=list2[0],name2=list1[0]}
        end
    end
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.ePlayQmpkUpdateZhanDuiRoundData]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.ePlayQmpkUpdateZhanDuiRoundData]
    if not cacheData then return end

    local data=cacheData[1]
    local battle={
        m_AttackName = data[0],
        m_DefendName = data[1],
        m_AttackWinCount = data[2],
        m_DefendWinCount = data[3],
        m_FinishedRound = data[4],
        m_PlayRound = data[5],
        m_SelfForce = data[7],
        m_PlayStage = data[8],
        m_PlayIndex = data[9],
        m_Status = {},
    }
    local round = 0
    local statusData = g_MessagePack.unpack(data[6])
    for i = 1,#statusData,3 do
        round = round + 1
        local attackTotalKill = 0
        local attackTotalDps = 0
        local defendTotalKill = 0
        local defendTotalDps = 0
        local attackMember = statusData[i] or {}
        local defendMember = statusData[i + 1] or {}
        local winForce = statusData[i + 2] or EnumCommonForce_lua.eNeutral
        local attackData = {}
        local defendData = {}
        for j = 1,#attackMember,5 do
            table.insert(attackData,{
                id = attackMember[j] or 0,
                Name = attackMember[j + 1] or "",
                Class = attackMember[j + 2] or 0 ,
                Dps = attackMember[j + 3] or 0,
                Kill = attackMember[j + 4] or 0,
            })
            attackTotalKill = attackTotalKill + (attackMember[j + 4] or 0)
            attackTotalDps = attackTotalDps + (attackMember[j + 3] or 0)
        end
        for j = 1,#defendMember,5 do
            table.insert(defendData,{
                id = defendMember[j]  or 0,
                Name = defendMember[j + 1] or "",
                Class = defendMember[j + 2] or 0,
                Dps = defendMember[j + 3] or 0,
                Kill = defendMember[j + 4] or 0,
            })
            defendTotalKill = defendTotalKill + (defendMember[j + 4] or 0)
            defendTotalDps = defendTotalDps + (defendMember[j + 3] or 0)
        end
        table.insert(battle.m_Status,{
            attackTotalKill = attackTotalKill,
            defendTotalKill = defendTotalKill,
            attackTotalDps = attackTotalDps,
            defendTotalDps = defendTotalDps,
            attackData = attackData,
            defendData = defendData,
            win = winForce,
            m_IsInBattle = battle.m_PlayRound==round,
        })
    end
    CLuaQMPKMgr.m_CurrentBattleStatus=battle

    CLuaFightingSpiritMgr.m_LeftNumOfWinPoints, CLuaFightingSpiritMgr.m_RightNumOfWinPoints = battle.m_AttackWinCount,battle.m_DefendWinCount
    g_ScriptEvent:BroadcastInLua("UpdateWinPoints")
end
CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eQmpkBanPickData]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eQmpkBanPickData]
    if not cacheData then return end
    local data = cacheData[1]
    local zhanDuiData = g_MessagePack.unpack(data[1])
    local attackMember =  g_MessagePack.unpack(data[3])
    local defendMember =  g_MessagePack.unpack(data[4])
    local banMemberData =  g_MessagePack.unpack(data[5])
    if not zhanDuiData or not attackMember or not defendMember or not banMemberData then return end
    
    CLuaQMPKMgr.m_QmpkBanPickInfo = {}
    CLuaQMPKMgr.m_QmpkBanPickInfo.force = EnumCommonForce_lua.eNeutral
    CLuaQMPKMgr.m_QmpkBanPickInfo.round = data[0]
    CLuaQMPKMgr.m_QmpkBanPickInfo.winForce = g_MessagePack.unpack(data[2])     -- 每局胜者
    local attackMemberTbl = {}
    local defendMemberTbl = {}
    local banMemberTbl = {}

    for i = 1,#attackMember, 5 do
        local member = {
            Id = attackMember[i],
            Name = attackMember[i + 1],
            Class = attackMember[i + 2],
            Gender = attackMember[i + 3],
            Status = attackMember[i + 4],
        }
        table.insert(attackMemberTbl,member)
    end

    for i = 1, #defendMember, 5 do
        local member = {
            Id = defendMember[i],
            Name = defendMember[i + 1],
            Class = defendMember[i + 2],
            Gender = defendMember[i + 3],
            Status = defendMember[i + 4],
        }
        table.insert(defendMemberTbl,member)
    end

    if round == 4 then                  --只有第四局双人战才有ban操作
        banMemberTbl.attack = {
            Id = banMemberData[1], 
            Name = banMemberData[2],
            Class = banMemberData[3],
            Gender = banMemberData[4],
        }

        banMemberTbl.defend = {
            Id = banMemberData[5], 
            Name = banMemberData[6],
            Class = banMemberData[7],
            Gender = banMemberData[8],
        }
    end

    CLuaQMPKMgr.m_QmpkBanPickInfo.curTime = zhanDuiData[1]      -- 当前服务器时间
    CLuaQMPKMgr.m_QmpkBanPickInfo.endTime = zhanDuiData[2]      -- 当前阶段倒计时结束时间
    CLuaQMPKMgr.m_QmpkBanPickInfo.attackZhanDui = {             -- 红方战队
        Id = zhanDuiData[3],                                    -- 战队ID
        Name = zhanDuiData[4],                                  -- 战队名
        WinCount = zhanDuiData[7],                              -- 得分
        BPStatus = zhanDuiData[9],                              -- BP状态
        Member = attackMemberTbl,                               -- 成员信息
        Ban = banMemberTbl.attack or nil,                        -- 被Ban成员
    }

    CLuaQMPKMgr.m_QmpkBanPickInfo.defendZhanDui = {             -- 蓝方方战队
        Id = zhanDuiData[5],                                    -- 战队ID
        Name = zhanDuiData[6],                                  -- 战队名
        WinCount = zhanDuiData[8],                              -- 得分
        BPStatus = zhanDuiData[10],                             -- BP状态
        Member = defendMemberTbl,                               -- 成员信息
        Ban = banMemberTbl.defend or nil,                        -- 被Ban成员
    }

    CLuaQMPKMgr.m_QmpkBanPickInfo.m_position = EnumQMPKBPPlayerPosition.eOther         -- 不属于任何一方，观战
    g_ScriptEvent:BroadcastInLua("SyncQmpkBanPickInfo")
    if(not CUIManager.IsLoaded(CLuaUIResources.QMPKBattleBeforeWnd)) then
        CUIManager.ShowUI(CLuaUIResources.QMPKBattleBeforeWnd)
    end
end
---- Star Biwu
CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eStarBiwuFightAndSkillDataStat]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eStarBiwuFightAndSkillDataStat]
    if not cacheData then return end

    local data=cacheData[1]

    local parse=function(list,out)
        for i=2,list.Count,5 do
            local playerId=list[i-1]
            local clazz=list[i]
            local name=list[i+1]
            local fightdata=nil
            local rawFightData=list[i+2]
            --如果有数据
            fightdata={
                [1]=rawFightData[0],
                [2]=rawFightData[1],
                [3]=rawFightData[2],
                [4]=rawFightData[3],
                [5]=rawFightData[4],
                [6]=rawFightData[5],
                [7]=rawFightData[6],
                [8]=(rawFightData.Count >= 10 and rawFightData[9]) and rawFightData[9] or "-"
            }
            local skilldata={}
            local rawSkillData=list[i+3]
            for i=1,rawSkillData.Count,2 do
                table.insert( skilldata, rawSkillData[i-1] )
                table.insert( skilldata, rawSkillData[i] )
            end
            table.insert( out,{playerId=playerId,clazz=clazz,name=name,fightdata=fightdata,skilldata=skilldata} )
        end
    end

    CLuaStarBiwuMgr.m_FightData = {}
    for i=1,data.Count do
        local roundData=data[i-1]
        if roundData.Count==2 then
            local list1=MsgPackImpl.unpack(CommonDefs.DictGetValue_LuaCall(roundData, 0))
            local list2=MsgPackImpl.unpack(CommonDefs.DictGetValue_LuaCall(roundData, 1))
            local data1={}
            local data2={}
            parse(list1,data1)
            parse(list2,data2)
            CLuaStarBiwuMgr.m_FightData[i]={[1]=data2,[2]=data1,name1=list2[0],name2=list1[0]}
        end
    end
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.ePlayStarBiwuUpdateZhanDuiRoundData]=function()
    local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.ePlayStarBiwuUpdateZhanDuiRoundData]
    if not cacheData then return end

    local data=cacheData[1]
    local battle={
        m_AttackName = data[0],
        m_DefendName = data[1],
        m_AttackWinCount = data[2],
        m_DefendWinCount = data[3],
        m_FinishedRound = data[4],
        m_PlayRound = data[5],
        m_SelfForce = data[7],
        m_Stage = data.Count > 9 and data[9] or 0,
        m_Group = data.Count > 10 and data[10] or 0,
        m_MatchIdx = data.Count > 11 and data[11] or 0,
        m_JieShu = data.Count > 12 and data[12] or 0,
        m_Status = {},
    }
    CLuaStarBiwuMgr.m_BattleDataWnd_AliasName = data.Count >= 9 and data[8] or ""
    local statusData=MsgPackImpl.unpack(data[6])
    for i=0,statusData.Count-1,7 do
        local status={
            m_Dps1 = statusData[i],
            m_Kill1 = statusData[i + 2],
            m_Name1 = {},
            m_Id1 = {},
            m_Class1 = {},

            m_Dps2 = statusData[i+3],
            m_Kill2 = statusData[i + 5],
            m_Name2 = {},
            m_Id2 = {},
            m_Class2 = {},

            m_Win = statusData[i+6],
            m_IsInBattle = battle.m_PlayRound==i/5+1,
        }
        local info=statusData[i+1]
        for j=0,info.Count-1,3 do
            table.insert( status.m_Id1,info[j] )
            table.insert( status.m_Name1,info[j+1] )
            table.insert( status.m_Class1,info[j+2] )
        end
        local info=statusData[i+4]
        for j=0,info.Count-1,3 do
            table.insert( status.m_Id2,info[j] )
            table.insert( status.m_Name2,info[j+1] )
            table.insert( status.m_Class2,info[j+2] )
        end

        table.insert( battle.m_Status,status )
    end
    CLuaStarBiwuMgr.m_CurrentBattleStatus=battle

    CLuaFightingSpiritMgr.m_LeftNumOfWinPoints, CLuaFightingSpiritMgr.m_RightNumOfWinPoints = battle.m_AttackWinCount,battle.m_DefendWinCount
    g_ScriptEvent:BroadcastInLua("UpdateWinPoints")
end
---- Star Biwu End

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eMirrorWarForceInfo]=function()
	local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eMirrorWarForceInfo]
	if not cacheData then return end

	local guildForceInfoUD = cacheData[1]
	local warIndex = cacheData[2]

    CLuaCityWarMgr.WarForceInfo = {}
    local list = MsgPackImpl.unpack(guildForceInfoUD)
    if not list then return end
    for i = 0, list.Count - 1, 5 do
        local guildId = list[i]
        local force = list[i + 1]
        local guildName = list[i + 2]
        local mapId = list[i + 3]
        local templateId = list[i + 4]

        table.insert(CLuaCityWarMgr.WarForceInfo, {GuildId = guildId, Force = force, GuildName = guildName, MapId = mapId, TemplateId = templateId})
    end
    CLuaCityWarMgr.WarIndex = warIndex
    --CCityWarMgr.Inst.m_WarIndex = warIndex
--[[    if CCityWarMgr.Inst:() then
        if CMiniMap.Instance then
            CMiniMap.Instance:UpdateMiniMapName(CScene.MainScene.SceneTemplateId, CScene.MainScene.SceneName.."-"..warIndex, 0)
        end
    end--]]
    g_ScriptEvent:BroadcastInLua("UpdateMirrorWarForceInfo")
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eMirrorWarWatchMapInfo]=function()
	local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eMirrorWarWatchMapInfo]
	if not cacheData then return end

	local playIds = MsgPackImpl.unpack(cacheData[1])
	local mirrorMapId = cacheData[2]
	local warIndex = cacheData[3]
  CLuaCityWarMgr.WatchPlayIds = {}
	for i = 0, playIds.Count - 1 do
    table.insert(CLuaCityWarMgr.WatchPlayIds, playIds[i])
	end
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eMirrorWarOccupyInfo]=function()
	local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eMirrorWarOccupyInfo]
	if not cacheData then return end

	local warIndex = cacheData[1]
	local force = cacheData[2]
	local activated = cacheData[3]
	local progress = cacheData[4]
	local occupyForce = cacheData[5]

    CLuaCityWarMgr.WarOccupyInfo[warIndex * 10 + force] = {Activated = activated, Progress = progress, OccupyForce = occupyForce}
    g_ScriptEvent:BroadcastInLua("UpdateMirrorWarOccupyInfo", warIndex, force, activated, progress, occupyForce)
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eMirrorWarPlaySummary]=function()
	local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eMirrorWarPlaySummary]
	if not cacheData then return end

	local finish = cacheData[1]
	local winForce = cacheData[2]
	local playResultUD = cacheData[3]
	local forceInfoUD = cacheData[4]
	local summaryInfoUD = cacheData[5]
end

CLuaGameVideoMgr.m_CacheFunc[EnumRecordType.eCityOccupyingProgress]=function()
	local cacheData=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eCityOccupyingProgress]
	if not cacheData then return end

	local templateId = cacheData[1]
	local npcEngineId = cacheData[2]
	local progress = cacheData[3]
	local guildId = cacheData[4]
	local guildName = cacheData[5]

  g_ScriptEvent:BroadcastInLua("SyncCityOccupyingProgress", npcEngineId, guildName, progress)
end

function CLuaGameVideoMgr.QueryGameVideoQMPKTeamRoundData()
    CLuaQMPKMgr.m_IsGameVideoBattleStatus = true
    CUIManager.ShowUI(CLuaUIResources.QMPKCurrentBattleStatusWnd)
end

-- CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.ePlayQmpkUpdateZhanDuiRoundData]=function()
--     local cache=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.ePlayQmpkUpdateZhanDuiRoundData]
--     if cache then

--     end
-- end

-- CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eQmpkFightAndSkillDataStat]=function()
--     local cache=CLuaGameVideoMgr.m_RecordFrameDataCached[EnumRecordType.eQmpkFightAndSkillDataStat]
--     if cache then

--     end
-- end
-- CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStartPlayCountDown]=function(data)
--     local count = data[1]
--     CCenterCountdownWnd.count = count
--     CCenterCountdownWnd.InitStartTime()
--     CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
-- end

-- CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eShowPlayStartFx]=function()
--     EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_kaishizhandou.prefab"})
-- end

-- engineId, buffId, buffUD
CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eAddBuffAllData]=function(data)

    local originalEngineId = data[1]
    local engineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(originalEngineId);
    local buffId = data[2]
    local buffUD = data[3]

    local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if not obj then return end
	local buffObj = CCppBuff()
	buffObj:LoadFromString(buffUD)
	if not buffObj then return end
	obj.BuffProp.Buffs[buffId] = buffObj;
    EventManager.BroadcastInternalForLua(EnumEventType.OnBuffInfoUpdate,{engineId})
end


CLuaGameVideoMgr.m_HideHpHurtTextPlayerPrefsKey = "HideHpHurtText"
GameVideoLuaRecord.m_hookObjectHpHurt = function()
    Lua.s_IsHook = false
    if PlayerPrefs.GetInt(CLuaGameVideoMgr.m_HideHpHurtTextPlayerPrefsKey, 0) > 0 then
        Lua.s_IsHook = true
        return
    end
end

function CLuaGameVideoMgr.HideHpHurtText(hide)
    PlayerPrefs.SetInt(CLuaGameVideoMgr.m_HideHpHurtTextPlayerPrefsKey, hide and 1 or 0)
end
function CLuaGameVideoMgr.IsHideHpHurtText()
    return PlayerPrefs.GetInt(CLuaGameVideoMgr.m_HideHpHurtTextPlayerPrefsKey, 0) > 0
end

-- 这段代码从csharp代码翻译过来
GameVideoLuaRecord.m_hookObjectRmBuffFx = function(mgr, data)

    local originalEngineId = data[1]
    local buffId = data[2]
    local replaceRm = nil

    -- 请注意,一开始的观战数据中并不包含这个值,这是后来新加的
    -- 所以必须检查这个值是否存在,完全可能是nil
    if data.Count > 3 then
        replaceRm = data[3]
    end

    -- 通过这条日志可以发现,这个函数很容易被调用两次,因为服务端就是会发两次RPC
    -- print("eObjectRmBuffFx enter new lua handler", buffId, replaceRm)

    local engineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(originalEngineId)

    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if not obj then return end

    -- 数据仍然要remove,如果是replace,后面也会add回来
    obj.BuffProp:ClientRmBuff(buffId)
    
    -- 如果是 replaceRm 就不处理特效
    if replaceRm and replaceRm > 0 then
        return
    end
    
    obj.DelayExecuteQueue:AppendCommand(DelegateFactory.Action_uint(function (id)
        obj:RemoveBuffFX(buffId)
    end))

    if not (GameVideoLuaRecord.HideAllYinShenObjectWhenLive and CGameVideoMgr.Inst:IsYinShenBuff(buffId)) then
        return
    end

    if not CGameVideoMgr.Inst:IsLiveMode() then
        return
    end

    if GameVideoLuaRecord.m_CheckYinShenBuffExistOnRmBuff then
        local existYinShenBuff = false

        CommonDefs.DictIterate(obj.BuffProp.Buffs, DelegateFactory.Action_object_object(function (___key, ___value)
            if CGameVideoMgr.Inst:IsYinShenBuff(___key) then
                existYinShenBuff = true
                -- break
            end
        end))

        if existYinShenBuff then
            obj.Visible = false
        else
            local isDiePet = obj.IsDie and (TypeIs(obj, typeof(CClientPet)) or TypeIs(obj, typeof(CClientLingShou)) or TypeIs(obj, typeof(CGameVideoLingShou)) or TypeIs(obj, typeof(CGameVideoPet)) ) 
            if not isDiePet then
                obj.Visible = true
                if TypeIs(obj, typeof(CGameVideoPlayer)) or TypeIs(obj, typeof(CClientFakePlayer)) or TypeIs(obj, typeof(CClientMainPlayer)) or TypeIs(obj, typeof(CClientOtherPlayer)) then
                    if obj.m_NeedRefreshAppearance then
                        obj:RefreshAppearance()
                    end
                end 
            end
        end

        return
    end

    local isDiePet = obj.IsDie and (TypeIs(obj, typeof(CClientPet)) or TypeIs(obj, typeof(CClientLingShou)) or TypeIs(obj, typeof(CGameVideoLingShou)) or TypeIs(obj, typeof(CGameVideoPet)) ) 
    if not isDiePet then
        obj.Visible = true
        if TypeIs(obj, typeof(CGameVideoPlayer)) or TypeIs(obj, typeof(CClientFakePlayer)) or TypeIs(obj, typeof(CClientMainPlayer)) or TypeIs(obj, typeof(CClientOtherPlayer)) then
            if obj.m_NeedRefreshAppearance then
                obj:RefreshAppearance()
            end
        end 
    end
end

GameVideoLuaRecord.m_hookObjectShowSkillName = function(mgr, data)
    local originalEngineId = tonumber(data[1]) 
    local engineId = CGameVideoCharacterMgr.Inst:GetFakeEngineId(originalEngineId)
    local skillId = tonumber(data[2])
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        if TypeIs(obj, typeof(CGameVideoLingShou)) and PlayerSettings.HideSkillEffect then
            return
        end
        obj.DelayExecuteQueue:AppendCommand(DelegateFactory.Action_uint(function (id)
            local obj1 = CClientObjectMgr.Inst:GetObject(engineId)
            if obj1 then
                local skillClass = Skill_AllSkills.GetClass(skillId)
                local ctrlSkillNameScale = StarBiWuShow_Setting.GetData().CtrlSkillNameScale
                local ctrlSkillNameColor = StarBiWuShow_Setting.GetData().CtrlSkillNameColor
                local color = Constants.WaveWords_SkillNameColor
                local fontSize = 0.8
                if CLuaStarBiwuMgr:IsInStarBiwuWatch() and ctrlSkillNameScale and (ctrlSkillNameScale.Length == 5) and (ctrlSkillNameScale[4] > 0) and CMainCamera.Main then
                    local distance = Vector3.Distance(CMainCamera.Main.transform.position , obj1.RO.transform.position)
                    local maxDis, maxSize, minDis, minSize = ctrlSkillNameScale[0], ctrlSkillNameScale[1], ctrlSkillNameScale[2], ctrlSkillNameScale[3]
                    fontSize = maxSize
                    if distance > minDis then
                        fontSize = minSize
                    elseif distance > maxDis then
                        fontSize = math.lerp(maxDis, minSize, (distance - maxDis) / (minDis - maxDis))
                    end
                    if ctrlSkillNameColor and ctrlSkillNameColor.Length == 2 then
                        local leftForce = EnumCommonForce.eAttack
                        local gameVideoPlayer = TypeAs(obj1, typeof(CGameVideoPlayer))
                        if gameVideoPlayer then
                            for i = 0, CFightingSpiritLiveWatchWndMgr.PlayerInfos.Count - 1 do
                                local info = CFightingSpiritLiveWatchWndMgr.PlayerInfos[i]
                                if info.PlayerId == gameVideoPlayer.BasicProp.Id then
                                    color = NGUIText.ParseColor24((info.Force == leftForce) and ctrlSkillNameColor[0] or ctrlSkillNameColor[1],0) 
                                end
                            end
                        end
                    end
                end
                obj1.RO:ShowStatusText(tostring(skillClass), color, "status",fontSize)
            end
        end))
    end
end

-- 明星赛击杀信息
CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStarBiwuFightKillInfo]=function(data)

    local killInfo = data and data[1]
    -- 击杀者
    local playerId1 = killInfo and killInfo[0]
    -- 被杀者
    local playerId2 = killInfo and killInfo[1]

    if playerId1 and playerId2 and playerId1 ~= 0 and playerId2 ~= 0 then
        for i = 0,CFightingSpiritLiveWatchWndMgr.PlayerInfos.Count - 1 do
            local info = CFightingSpiritLiveWatchWndMgr.PlayerInfos[i]
            if info.PlayerId == playerId1 then
                local winnerIsLeft = info.Force == EnumCommonForce.eAttack
                local winerPortrait = CUICommonDef.GetPortraitName(info.Class, info.Gender, -1)
                local winnerName = info.PlayerName
                for j = 0,CFightingSpiritLiveWatchWndMgr.PlayerInfos.Count - 1 do
                    local info2 = CFightingSpiritLiveWatchWndMgr.PlayerInfos[j]
                    if info2.PlayerId == playerId2 then
                        local loserPortrait = CUICommonDef.GetPortraitName(info2.Class, info2.Gender, -1)
                        local loserName = info2.PlayerName
                        g_ScriptEvent:BroadcastInLua("KillInfoUpdate",winnerIsLeft, winerPortrait, loserPortrait,winnerName, loserName)
                        break
                    end
                end
                return
            end
        end
    end
end

-- 明星赛获胜信息
-- 注意：这个后面扩展到了斗魂坛,斗魂坛也会有这个观战。因为明星赛观战录像中已经有了这个名字，就不改变量名了
CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStarBiwuFightWinInfo]=function(data)
    -- 获胜的阵营
    local winForceInfo = data and data[1]
    local winForce = winForceInfo and winForceInfo[0]
    if winForce then
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {EnumCommonForce_lua.eAttack == winForce and
              "Fx/UI/Prefab/UI_lanfanghuosheng.prefab" or "Fx/UI/Prefab/UI_hongfanghuosheng.prefab"})
    end
end

CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.ePlayerLeaveSaiMaJumpRegion]=function(data)
    local engineId = data and CGameVideoCharacterMgr.Inst:GetFakeEngineId(data[1])
    local result = data and data[2]
    local bSkill = data and data[3]
    local forwardSpeed = data and data[4]
    
    LuaNewHorseRaceMgr:PlayerLeaveSaiMaJumpRegion(engineId, result, bSkill, forwardSpeed)
end

CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStarBiwuFightZhuWeiInfo]=function(data)
    local zhuwei = data and data[1]
    
    local f1,v1,f2,v2,f3,v3 = zhuwei[0],zhuwei[1],zhuwei[2],zhuwei[3],zhuwei[4],zhuwei[5]
    g_ScriptEvent:BroadcastInLua("OnStarBiwuFightZhuWeiInfo",f1,v1,f2,v2,f3,v3)
end

CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStarBiwuFightChampionInfo]=function(data)
    local championInfo = data and data[1]
    if not championInfo then return end
    
    CLuaStarBiwuMgr.championZhanduiName = championInfo[1]
    CUIManager.ShowUI(CLuaUIResources.StarBiwuChampionWnd)
end

CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStarBiwuFightGiftRank]=function(data)
    local rankInfo = data and data[1]
    if not rankInfo then return end

    local step = 5
    for i = 0, rankInfo.Count - 1, step do
        local force = rankInfo[i]
        local rankPos = rankInfo[i+1]
        local playerId = rankInfo[i+2]
        local playerName = rankInfo[i+3]
        local rankValue = rankInfo[i+4]
        print("eStarBiwuFightGiftRank", force, rankPos, playerId, playerName, rankValue)
    end
end

CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStarBiwuFightSendGift]=function(data)
    local giftInfo = data and data[1]
    if not giftInfo then return end

    local playerId, playerName, force, giftType = giftInfo[0], giftInfo[1], giftInfo[2], giftInfo[3]
    print("eStarBiwuFightSendGift", playerId, playerName, force, giftType)
    g_ScriptEvent:BroadcastInLua("OnStarBiwuFightSendGift", playerId, playerName, force, giftType)
end

CLuaGameVideoMgr.m_RecordHanderTable[EnumRecordType.eStarBiwuFightZhanduiId]=function(data)
    local zhanduiInfo = data and data[1]
    if not zhanduiInfo then return end
    CLuaStarBiwuMgr.m_FightZhanduiIdInfo = {}
    local step = 2
    for i = 0, zhanduiInfo.Count - 1, step do
        local force = zhanduiInfo[i]
        local zhanduiId = zhanduiInfo[i+1]
        CLuaStarBiwuMgr.m_FightZhanduiIdInfo[force] = zhanduiId
    end
    g_ScriptEvent:BroadcastInLua("OnStarBiwuFightZhanduiId")
end

GameVideoLuaRecord.m_hookDouHunCrossPlayChampionInfo = function(this, data)
    if not data then return end

    LuaFightingSpiritGenerateChampionWnd.s_NthMatch = data[1]
    LuaFightingSpiritGenerateChampionWnd.s_Level = data[2]
    LuaFightingSpiritGenerateChampionWnd.s_TeamName = data[3]
    LuaFightingSpiritGenerateChampionWnd.s_LeaderName = data[4]
    LuaFightingSpiritGenerateChampionWnd.s_WinTeamId = data[5]
    LuaFightingSpiritGenerateChampionWnd.s_Time = data[6]
    CUIManager.ShowUI(CLuaUIResources.FightingSpiritGenerateChampionWnd)
end
