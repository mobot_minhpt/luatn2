local LuaGameObject=import "LuaGameObject"
local UITabBar = import "L10.UI.UITabBar"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"

LuaBingQiPuWnd = class()

RegistClassMember(LuaBingQiPuWnd,"TabBar")
RegistClassMember(LuaBingQiPuWnd,"BingQiPuDailyRoot")
RegistClassMember(LuaBingQiPuWnd,"BingQiPuRankRoot")
RegistClassMember(LuaBingQiPuWnd,"BingQiPuRewardRoot")
RegistClassMember(LuaBingQiPuWnd,"BingQiPuRuleRoot")

function LuaBingQiPuWnd:GetAlert(index)
	return PlayerPrefs.GetInt("BingQiPuMainTab"..index,0)
end

function LuaBingQiPuWnd:SetAlert(index)
	PlayerPrefs.SetInt("BingQiPuMainTab"..index,1)
end

function LuaBingQiPuWnd:Init()
	self.TabBar = self.gameObject:GetComponent(typeof(UITabBar))
	self.BingQiPuDailyRoot = LuaGameObject.GetChildNoGC(self.transform,"BingQiPuDailyTab").gameObject
	self.BingQiPuRankRoot = LuaGameObject.GetChildNoGC(self.transform,"BingQiPuRankTab").gameObject
	self.BingQiPuRewardRoot = LuaGameObject.GetChildNoGC(self.transform,"BingQiPuRewardTab").gameObject
	self.BingQiPuRuleRoot =LuaGameObject.GetChildNoGC(self.transform,"BingQiPuRuleTab").gameObject

	for i=1,4 do
		local go = self.TabBar:GetTabGoByIndex(i-1)
		local alert = LuaGameObject.GetChildNoGC(go.transform,"Alert")
		local clicked = self:GetAlert(i)
		alert.gameObject:SetActive(clicked==0)
	end

	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)

	self.TabBar:ChangeTab(0)
end

function LuaBingQiPuWnd:OnTabChange(gameObject, index)
	self.BingQiPuDailyRoot:SetActive(index == 0)
	self.BingQiPuRankRoot:SetActive(index == 1)
	self.BingQiPuRuleRoot:SetActive(index == 2)
	self.BingQiPuRewardRoot:SetActive(index == 3)

	self:SetAlert(index+1)
	local alert = LuaGameObject.GetChildNoGC(gameObject.transform,"Alert").gameObject
	alert:SetActive(false)

	if index == 0 then--每日
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BingQiPuDailyRoot, typeof(CCommonLuaScript))
		luaScript:Init({})
	elseif index == 1 then--排行
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BingQiPuRankRoot, typeof(CCommonLuaScript))
		luaScript:Init({})
	elseif index == 2 then--规则
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BingQiPuRuleRoot, typeof(CCommonLuaScript))
		luaScript:Init({})
	elseif index == 3 then--奖励
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BingQiPuRewardRoot, typeof(CCommonLuaScript))
		luaScript:Init({})
	end
end