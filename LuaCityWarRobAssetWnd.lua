require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local UISprite = import "UISprite"

CLuaCityWarRobAssetWnd = class()
CLuaCityWarRobAssetWnd.Path = "ui/citywar/LuaCityWarRobAssetWnd"

RegistClassMember(CLuaCityWarRobAssetWnd, "m_ButtonRootObj")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_TipObj")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_AssetRootTransTable")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_AssetLightObjTable")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_AssetWeightTable")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_LastAssetIndex")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_AssetRootSpriteTable")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_Tick")
RegistClassMember(CLuaCityWarRobAssetWnd, "m_Count")

function CLuaCityWarRobAssetWnd:Init( ... )
	self.m_LastAssetIndex = 1
	self.m_TipObj = self.transform:Find("Tip").gameObject
	self.m_TipObj:SetActive(false)
	self.m_ButtonRootObj = self.transform:Find("Buttons").gameObject
	UIEventListener.Get(self.m_ButtonRootObj.transform:Find("ConfirmBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:ConfirmRob()
	end)
	UIEventListener.Get(self.m_ButtonRootObj.transform:Find("ReLookBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:Lookup()
	end)
	local ZhanLongSetting = CityWar_ZhanLong.GetData()
	local countTable = {}
	countTable[1], countTable[2], countTable[3], countTable[4], countTable[5] = string.match(ZhanLongSetting.GrabMaterialCount, "(%d+),(%d+),(%d+),(%d+),(%d+)")
	self.m_AssetWeightTable = {}
	self.m_AssetWeightTable[1] = 0
	self.m_AssetWeightTable[2], self.m_AssetWeightTable[3], self.m_AssetWeightTable[4], self.m_AssetWeightTable[5], self.m_AssetWeightTable[6] = string.match(ZhanLongSetting.GrabMaterialWeight, "(%d+),(%d+),(%d+),(%d+),(%d+)")
	for i = 2, 6 do
		self.m_AssetWeightTable[i] = self.m_AssetWeightTable[i - 1] + self.m_AssetWeightTable[i]
	end

	self.m_AssetRootTransTable = {}
	self.m_AssetLightObjTable = {}
	self.m_AssetRootSpriteTable = {}
	for i = 1, 5 do
		self.m_AssetRootTransTable[i] = self.transform:Find("AssetsRoot/Assets"..i)
		self.m_AssetRootTransTable[i]:Find("Label"):GetComponent(typeof(UILabel)).text = countTable[i]
		self.m_AssetLightObjTable[i] = self.m_AssetRootTransTable[i]:Find("Light").gameObject
		self.m_AssetRootSpriteTable[i] = self.m_AssetRootTransTable[i]:GetComponent(typeof(UISprite))
		self.m_AssetLightObjTable[i]:SetActive(false)
	end

	self:Lookup()
end

function CLuaCityWarRobAssetWnd:ConfirmRob( ... )
	Gac2Gas.RequestGrabMaterialForCityWarZhanLong(self.m_LastAssetIndex, CLuaCityWarMgr.ZhanLongGrabMaterialItemId, CLuaCityWarMgr.ZhanLongGrabMaterialItemPlace, CLuaCityWarMgr.ZhanLongGrabMaterialItemPos)
	CUIManager.CloseUI(CLuaUIResources.CityWarRobAssetWnd)
end

function CLuaCityWarRobAssetWnd:Lookup( ... )
	local ret = math.random(self.m_AssetWeightTable[self.m_LastAssetIndex], self.m_AssetWeightTable[6] - 1)
	for i = 1, 6 do
		if ret >= self.m_AssetWeightTable[i] then
			self.m_LastAssetIndex = i
		else
			break
		end
	end
	self:ShowFx(self.m_LastAssetIndex)
end

function CLuaCityWarRobAssetWnd:ShowFx(index)
	self.m_ButtonRootObj:SetActive(false)
	self.m_TipObj:SetActive(true)
	for i = 1, 5 do
		self.m_AssetRootTransTable[i].localScale = Vector3.one
		self.m_AssetRootSpriteTable[i].alpha = 1
	end
	for _, v in pairs(self.m_AssetRootTransTable) do
		v.localScale = Vector3.one
	end

	self.m_Count = -1
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end
	self.m_Tick = RegisterTickWithDuration(function ( ... )
		self.m_Count = self.m_Count + 1
		if self.m_Count >= 50 then
			UnRegisterTick(self.m_Tick)
			self.m_Tick = nil
			self:EndFx(index)
		end
		local cur = self.m_Count % 5 + 1
		local pre = cur == 1 and 5 or cur - 1
		self.m_AssetLightObjTable[cur]:SetActive(true)
		self.m_AssetLightObjTable[pre]:SetActive(false)
	end, 100, 6000)
	--self:EndFx(index)
end

function CLuaCityWarRobAssetWnd:EndFx(index)
	self.m_ButtonRootObj:SetActive(true)
	self.m_TipObj:SetActive(false)
	for i = 1, 5 do
		self.m_AssetRootTransTable[i].localScale = index == i and Vector3(1.5, 1.5, 1) or Vector3.one
		self.m_AssetRootSpriteTable[i].alpha = index == i and 1.0 or 0.5
	end
end

function CLuaCityWarRobAssetWnd:OnDestroy( ... )
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
