
luaCrazyDyeMgr = {}

luaCrazyDyeMgr.ColorData 	= {}
luaCrazyDyeMgr.BossColor 	= 0    	--用来记录目前boss的状态，1，红，2，蓝
luaCrazyDyeMgr.IsChange = false
luaCrazyDyeMgr.ChangeEndTime = 0

-- 同步副本概况
-- score 自己的分数
-- bossColorUd Boss血量百分比对应的颜色
function Gas2Gac.SyncDyeSceneOverView(bossColorUd, score)
	local list = MsgPackImpl.unpack(bossColorUd)
    luaCrazyDyeMgr.ColorData = {}
	for i = 1,list.Count,2 do
		local t = {}
		if i+1 < list.Count then
			t.threshold = {list[i-1],list[i+1]} -- 血量百分比
		else
			t.threshold = {list[i-1],0}
		end
		t.color = list[i]		-- 颜色 EnumDyePlayColor
		table.insert( luaCrazyDyeMgr.ColorData,t )
	end
	g_ScriptEvent:BroadcastInLua("SyncDyeSceneOverView",score,luaCrazyDyeMgr.ColorData)
end

-- Boss即将变色
-- endTime -> Boss开始变色的时间戳
function Gas2Gac.SyncDyePlayBossChangeColor(endTime)
	luaCrazyDyeMgr.IsChange = true
	luaCrazyDyeMgr.ChangeEndTime = endTime
end

function luaCrazyDyeMgr:ClearData()
    luaCrazyDyeMgr.ColorData = {}
end
