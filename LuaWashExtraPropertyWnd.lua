local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType=import "L10.UI.CTooltip+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaWashExtraPropertyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaWashExtraPropertyWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaWashExtraPropertyWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaWashExtraPropertyWnd, "WashButton", "WashButton", GameObject)
RegistChildComponent(LuaWashExtraPropertyWnd, "QnCheckBox1", "QnCheckBox1", QnCheckBox)
RegistChildComponent(LuaWashExtraPropertyWnd, "QnCheckBox2", "QnCheckBox2", QnCheckBox)
RegistChildComponent(LuaWashExtraPropertyWnd, "QnCostAndOwnExp", "QnCostAndOwnExp", CCurentMoneyCtrl)
RegistChildComponent(LuaWashExtraPropertyWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaWashExtraPropertyWnd, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaWashExtraPropertyWnd, "QualitySprite", "QualitySprite", UISprite)
RegistChildComponent(LuaWashExtraPropertyWnd, "NumLabel", "NumLabel", UILabel)
RegistChildComponent(LuaWashExtraPropertyWnd, "ItemGetLabel", "ItemGetLabel", UILabel)
RegistChildComponent(LuaWashExtraPropertyWnd, "ItemCell", "ItemCell", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaWashExtraPropertyWnd,"m_QnIncreseCorAndDecreaseButtons")
RegistClassMember(LuaWashExtraPropertyWnd,"m_QnRadioBoxIndex")
RegistClassMember(LuaWashExtraPropertyWnd,"m_ValueArray")
RegistClassMember(LuaWashExtraPropertyWnd,"m_SubValue")
RegistClassMember(LuaWashExtraPropertyWnd,"m_TextArray")

function LuaWashExtraPropertyWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.WashButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWashButtonClick()
	end)

	UIEventListener.Get(self.ItemGetLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnItemGetLabelClick()
	end)
    --@endregion EventBind end
end

function LuaWashExtraPropertyWnd:Init()
	self.m_TextArray = {LocalString.GetString("根骨"),LocalString.GetString("精力"),LocalString.GetString("力量"),LocalString.GetString("智力"),LocalString.GetString("敏捷")}
	self.ReadMeLabel.text = g_MessageMgr:FormatMessage("WashExtraPropertyWnd_ReadMe")
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelect(btn, index)
	end)
	self.QnCheckBox1.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self:OnQnCheckBox1Changed(value)
	end)
	self.QnCheckBox2.OnValueChanged = DelegateFactory.Action_bool(function (value)
		self:OnQnCheckBox2Changed(value)
	end)
	self.QnCheckBox1.Selected = LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 1
	self:InitPropertyRadioBox()
	self.QnCostAndOwnExp.updateAction = DelegateFactory.Action(function () 
		if CClientMainPlayer.Inst then
			local exp = CClientMainPlayer.Inst.PlayProp.Exp
			self.QnCostAndOwnExp.own = exp
			self.QnCostAndOwnExp.m_OwnLabel.text = exp
			self.QnCostAndOwnExp:UpdateIcon(self.QnCostAndOwnExp.m_Type)
		end
    end)
	self.QnCostAndOwnExp:SetType(EnumMoneyType.Exp,EnumPlayScoreKey.NONE, true)
	self.QnCostAndOwnMoney:SetType(EnumMoneyType.YinLiang,EnumPlayScoreKey.NONE, true)
	local setting = GameplayItem_Setting.GetData()
	local itemData = L10.Game.Item_Item.GetData(setting.PermanentProp_WashItemId)
	self.IconTexture:LoadMaterial(itemData.Icon)
	self.QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData, nil, false)
	self.NumLabel.text = ""
	self.ItemGetLabel.gameObject:SetActive(false)
	self:InitNumLabel()
end

function LuaWashExtraPropertyWnd:OnEnable()
	g_ScriptEvent:AddListener("OnWashPermanentPropSuccess",self,"OnWashPermanentPropSuccess") 
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaWashExtraPropertyWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnWashPermanentPropSuccess",self,"OnWashPermanentPropSuccess") 
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
	LuaExtraPropertyMgr.m_WashExtraPropertyWndType = 1
end

function LuaWashExtraPropertyWnd:SendItem(args)
	local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)
    if item then
        local templateId = item.TemplateId
		if templateId == GameplayItem_Setting.GetData().PermanentProp_WashItemId then
			self:InitNumLabel()
		end
	end
end

function LuaWashExtraPropertyWnd:OnSetItemAt(args)
	local  place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
	local item = CItemMgr.Inst:GetById( newItemId)
    if item then
        local templateId = item.TemplateId
		if templateId == GameplayItem_Setting.GetData().PermanentProp_WashItemId then
			self:InitNumLabel()
		end
	end
end

function LuaWashExtraPropertyWnd:OnWashPermanentPropSuccess(beforeArray,afterArray,consumeType, index, times)
	self.m_ValueArray = afterArray
	self:OnQnAddSubAndInputButtonValueChanged(0)
	for i = 1, 5 do
		local val = self.m_ValueArray[i]
		local qnAddSubAndInputButton = self.m_QnIncreseCorAndDecreaseButtons[i]
		qnAddSubAndInputButton:SetMinMax(0,val,1)
		qnAddSubAndInputButton:SetValue(val,false)
		local btn = self.QnRadioBox.m_RadioButtons[i - 1]
		btn.transform:Find("ReduceLabel").gameObject:SetActive(false)
		local increaseValueLabel = btn.transform:Find("IncreaseValueLabel"):GetComponent(typeof(UILabel))
		local beforeVal = beforeArray[i]
		increaseValueLabel.gameObject:SetActive(beforeVal < val)
		increaseValueLabel.text = val - beforeVal
	end
	local btn = self.QnRadioBox.m_RadioButtons[index - 1]
	btn.transform:Find("Fx"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_dagongshouce_kekaiqicishu.prefab")
	self.QnCostAndOwnExp:SetType(EnumMoneyType.Exp,EnumPlayScoreKey.NONE, true)
	self.QnCostAndOwnMoney:SetType(EnumMoneyType.YinLiang,EnumPlayScoreKey.NONE, true)
	--g_MessageMgr:ShowMessage("WashPermanentProp_Result",self.m_ValueArray[1],self.m_ValueArray[2],self.m_ValueArray[3],self.m_ValueArray[4],self.m_ValueArray[5])
end

function LuaWashExtraPropertyWnd:InitPropertyRadioBox()
	self.m_QnIncreseCorAndDecreaseButtons = {}
	for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
		local btn = self.QnRadioBox.m_RadioButtons[i]
		btn.transform:Find("IncreaseValueLabel").gameObject:SetActive(false)
		btn.transform:Find("ReduceLabel").gameObject:SetActive(false)
		local qnAddSubAndInputButton = btn.transform:Find("QnIncreseAndDecreaseButton").gameObject:GetComponent(typeof(QnAddSubAndInputButton))
		table.insert(self.m_QnIncreseCorAndDecreaseButtons,qnAddSubAndInputButton)
		qnAddSubAndInputButton:Awake()
		qnAddSubAndInputButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
			self:OnQnAddSubAndInputButtonValueChanged(value)
		end)
	end
	self:ReInitProperty()
end

function LuaWashExtraPropertyWnd:ReInitProperty()
	local c1,c2,c3,c4,c5 = 0,0,0,0,0
	if CClientMainPlayer.Inst then
		local fightProp = CClientMainPlayer.Inst.FightProp
		c1,c2,c3,c4,c5= 
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentCor) + fightProp:GetParam(EPlayerFightProp.RevisePermanentCor)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentSta) + fightProp:GetParam(EPlayerFightProp.RevisePermanentSta)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentStr) + fightProp:GetParam(EPlayerFightProp.RevisePermanentStr)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentInt) + fightProp:GetParam(EPlayerFightProp.RevisePermanentInt)),
			math.floor(fightProp:GetParam(EPlayerFightProp.PermanentAgi) + fightProp:GetParam(EPlayerFightProp.RevisePermanentAgi))
	end
	self.m_ValueArray = {c1,c2,c3,c4,c5}
	for i = 1, 5 do
		local val = self.m_ValueArray[i]
		local qnAddSubAndInputButton = self.m_QnIncreseCorAndDecreaseButtons[i]
		qnAddSubAndInputButton:SetMinMax(0,val,1)
		qnAddSubAndInputButton:SetValue(val,true)
		if not self.m_QnRadioBoxIndex and val > 0 then
			self.m_QnRadioBoxIndex = i - 1
		end
	end
	self.QnRadioBox:ChangeTo(self.m_QnRadioBoxIndex and self.m_QnRadioBoxIndex or 0, false)
end

function LuaWashExtraPropertyWnd:InitNumLabel()
	local setting = GameplayItem_Setting.GetData()
	local sub = self.m_SubValue
	local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, setting.PermanentProp_WashItemId)
	self.NumLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"),count,sub) 
	self.ItemGetLabel.gameObject:SetActive(sub > count)
end
--@region UIEvent

function LuaWashExtraPropertyWnd:OnWashButtonClick()
	if self.m_SubValue == 0 then
		g_MessageMgr:ShowMessage("WashExtraPropertyWnd_None_Select")
		return 
	end
	if LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 1 then
		local setting = GameplayItem_Setting.GetData()
		local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, setting.PermanentProp_WashItemId)
		local msg = g_MessageMgr:FormatMessage("WashExtraPropertyWnd_Cost_Item_Confirm",self.m_SubValue,self.m_TextArray[self.m_QnRadioBoxIndex + 1],self.m_SubValue)
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			Gac2Gas.WashPermanentProp(LuaExtraPropertyMgr.m_WashExtraPropertyWndType, self.m_QnRadioBoxIndex + 1,self.m_SubValue)
		end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
	else
		if not self.QnCostAndOwnExp.moneyEnough or not self.QnCostAndOwnMoney.moneyEnough then
			g_MessageMgr:ShowMessage("WashExtraPropertyWnd_Exp_Money_Not_Enough")
			return 
		end
		local msg = g_MessageMgr:FormatMessage("WashExtraPropertyWnd_Cost_ExpAndSilver_Confirm",self.m_SubValue,self.m_TextArray[self.m_QnRadioBoxIndex + 1],self.QnCostAndOwnExp:GetCost(),self.QnCostAndOwnMoney:GetCost())
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			Gac2Gas.WashPermanentProp(LuaExtraPropertyMgr.m_WashExtraPropertyWndType, self.m_QnRadioBoxIndex + 1,self.m_SubValue)
		end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
	end
end

function LuaWashExtraPropertyWnd:OnItemGetLabelClick()
	local setting = GameplayItem_Setting.GetData()
	CItemAccessListMgr.Inst:ShowItemAccessInfo(setting.PermanentProp_WashItemId, false, self.ItemGetLabel.transform, CTooltipAlignType.Right)
end

function LuaWashExtraPropertyWnd:OnSelect(btn, index)
	if self.m_QnRadioBoxIndex and self.m_QnRadioBoxIndex == index then return end
	self.m_QnRadioBoxIndex = index
	self:ReInitProperty()
end

function LuaWashExtraPropertyWnd:OnQnAddSubAndInputButtonValueChanged(value)
	self.m_SubValue = 0
	for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
		local btn = self.QnRadioBox.m_RadioButtons[i]
		local increaseValueLabel = btn.transform:Find("IncreaseValueLabel"):GetComponent(typeof(UILabel))
		local reduceLabel = btn.transform:Find("ReduceLabel"):GetComponent(typeof(UILabel))
		increaseValueLabel.gameObject:SetActive(false)
		reduceLabel.gameObject:SetActive(self.m_QnRadioBoxIndex == i)

		if self.m_QnRadioBoxIndex == i then
			local index = i + 1
			local val = self.m_ValueArray[index]
			local btnVal = self.m_QnIncreseCorAndDecreaseButtons[index]:GetValue()
			local sub = val - btnVal
			self.m_SubValue = sub
			reduceLabel.text = sub > 0 and SafeStringFormat3(LocalString.GetString("减扣%d"),sub) or ""
			
			local setting = GameplayItem_Setting.GetData()
			self.QnCostAndOwnExp:SetCost(sub * setting.PermanentProp_WashExp)
			self.QnCostAndOwnMoney:SetCost(sub * setting.PermanentProp_WashMoney)

			self:InitNumLabel()
		end
	end
end

function LuaWashExtraPropertyWnd:OnQnCheckBox1Changed(value)
	LuaExtraPropertyMgr.m_WashExtraPropertyWndType = value and 1 or 2
	self.QnCheckBox2:SetSelected(not value, true)
	self.QnCostAndOwnExp.gameObject:SetActive(LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 2)
	self.QnCostAndOwnMoney.gameObject:SetActive(LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 2)
	self.ItemCell.gameObject:SetActive(LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 1)
end

function LuaWashExtraPropertyWnd:OnQnCheckBox2Changed(value)
	LuaExtraPropertyMgr.m_WashExtraPropertyWndType = value and 2 or 1
	self.QnCheckBox1:SetSelected(not value, true)
	self.QnCostAndOwnExp.gameObject:SetActive(LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 2)
	self.QnCostAndOwnMoney.gameObject:SetActive(LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 2)
	self.ItemCell.gameObject:SetActive(LuaExtraPropertyMgr.m_WashExtraPropertyWndType == 1)
end
--@endregion UIEvent

