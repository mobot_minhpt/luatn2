local CWelfareMgr = import "L10.UI.CWelfareMgr"
local Animation = import "UnityEngine.Animation"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaZhouNianQing2023MainWnd = class()

RegistClassMember(LuaZhouNianQing2023MainWnd, "m_TimeLabel")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_LF_Btn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_FSC_Btn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_LFK_Btn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_DoubleBtn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_WelfareBtn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_CouponBtn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_GiftPackBtn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_PassportBtn")
RegistClassMember(LuaZhouNianQing2023MainWnd, "m_Anim")

function LuaZhouNianQing2023MainWnd:Awake()
    self.m_TimeLabel = self.transform:Find("Title/TimeLabel"):GetComponent(typeof(UILabel))
    self.m_TimeLabel.text = LocalString.GetString("2023年")..g_MessageMgr:FormatMessage("ZNQ2023_LookForKid_Wnd_Msg1")

    if CommonDefs.IS_VN_CLIENT then
        self.m_TimeLabel.text = g_MessageMgr:FormatMessage("ZNQ2023_LookForKid_Wnd_Msg1") .. LocalString.GetString("/2024")
    end
    
    self.m_LF_Btn = self.transform:Find("LifeFriend/Btn").gameObject
    self.m_FSC_Btn = self.transform:Find("FourSeasonCard/Btn").gameObject
    self.m_LFK_Btn = self.transform:Find("LookForKid/Btn").gameObject
    self.m_DoubleBtn = self.transform:Find("Bottom/Double").gameObject
    self.m_WelfareBtn = self.transform:Find("Bottom/Welfare").gameObject
    self.m_CouponBtn = self.transform:Find("Bottom/Coupon").gameObject
    self.m_GiftPackBtn = self.transform:Find("Bottom/GiftPack").gameObject
    self.m_PassportBtn = self.transform:Find("Bottom/Passport").gameObject
    self.m_PassportBtn.transform:Find("Level"):GetComponent(typeof(UILabel)).text = 0
    self.m_PassportBtn.transform:Find("Alert").gameObject:SetActive(false)
    self.m_Anim = self.transform:GetComponent(typeof(Animation))

    UIEventListener.Get(self.m_LF_Btn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnLFBtnClick()
    end)
    UIEventListener.Get(self.m_FSC_Btn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnFSCBtnClick()
    end)
    UIEventListener.Get(self.m_LFK_Btn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnLFKBtnClick()
    end)
    UIEventListener.Get(self.m_DoubleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnDoubleBtnClick()
    end)
    UIEventListener.Get(self.m_WelfareBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnWelfareBtnClick()
    end)
    UIEventListener.Get(self.m_CouponBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCouponBtnClick()
    end)
    UIEventListener.Get(self.m_GiftPackBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnGiftPackBtnClick()
    end)
    UIEventListener.Get(self.m_PassportBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnPassportBtnClick()
    end)

    UIEventListener.Get(self.transform:Find("ZhouNianQingFSCDetailWnd/Sprite").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_Anim:Play("zhounianqing2023mainwnd_sishixishow_1")
    end)
end

function LuaZhouNianQing2023MainWnd:OnEnable()
    g_ScriptEvent:AddListener("FSC_QueryFestivalWndDataResult", self, "QueryFestivalWndDataResult")
    g_ScriptEvent:AddListener("ZNQ2023_SyncZhanLingPlayData", self, "SyncZhanLingPlayData")
end

function LuaZhouNianQing2023MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FSC_QueryFestivalWndDataResult", self, "QueryFestivalWndDataResult")
    g_ScriptEvent:RemoveListener("ZNQ2023_SyncZhanLingPlayData", self, "SyncZhanLingPlayData")
end

function LuaZhouNianQing2023MainWnd:QueryFestivalWndDataResult(specialCardNum)
    local progress = self.m_FSC_Btn.transform:Find("Progress")
    for i = 0, 7 do
        progress:GetChild(i):GetChild(0).gameObject:SetActive(i < specialCardNum)
    end
end

function LuaZhouNianQing2023MainWnd:SyncZhanLingPlayData()
    self.m_PassportBtn.transform:Find("Level"):GetComponent(typeof(UILabel)).text = LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
    self.m_PassportBtn.transform:Find("Alert").gameObject:SetActive(LuaZhouNianQing2023Mgr.m_bAlert)
end

function LuaZhouNianQing2023MainWnd:Init()
    Gac2Gas.Anniv2023_QueryFestivalWndData()
    Gac2Gas.QueryLianDanLuAlertInfo()
    Gac2Gas.QueryLianDanLuPlayData()
end

function LuaZhouNianQing2023MainWnd:OnLFBtnClick()
    if CLuaScheduleMgr:IsFestivalTaskOpen(22121496) then
        CUIManager.ShowUI(CLuaUIResources.ZhouNianQingLifeFriendWnd)
    else
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("活动开放时间是")..Task_Schedule.GetData(42030311).ExternTip..LocalString.GetString("，目前暂未开放！"))
    end
end

function LuaZhouNianQing2023MainWnd:OnFSCBtnClick()
    if CLuaScheduleMgr:IsFestivalTaskOpen(22121494) then
        self.m_Anim:Play("zhounianqing2023mainwnd_sishixishowfangan2")
    else
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("活动开放时间是")..Task_Schedule.GetData(42030309).ExternTip..LocalString.GetString("，目前暂未开放！"))
    end
end

function LuaZhouNianQing2023MainWnd:OnLFKBtnClick()
    if CLuaScheduleMgr:IsFestivalTaskOpen(22121495) then
        CUIManager.ShowUI(CLuaUIResources.ZhouNianQingLookForKidMainWnd)
    else
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("活动开放时间是")..Task_Schedule.GetData(42030310).ExternTip..LocalString.GetString("，目前暂未开放！"))
    end
end

function LuaZhouNianQing2023MainWnd:ShowScheduleInfo(schedule)
    CLuaScheduleMgr.selectedScheduleInfo={
        activityId = schedule.ID,
        taskId = schedule.TaskID[0],

        TotalTimes = 0,--不显示次数
        DailyTimes = 0,--不显示活力
        FinishedTimes = 0,
        
        ActivityTime = schedule.ExternTip,
        ShowJoinBtn = false
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end

function LuaZhouNianQing2023MainWnd:OnDoubleBtnClick()
    local schedule = Task_Schedule.GetData(ZhouNianQing2023_Setting.GetData().DoubleTaskId)
    if CLuaScheduleMgr:IsFestivalTaskOpen(schedule.TaskID[0]) then
        local task = Task_Task.GetData(schedule.TaskID[0])
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= task.Level then
            self:ShowScheduleInfo(schedule)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("你不满%s级，无法参与玩法！"), tostring(task.Level), schedule.TaskName))
        end
    else
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("活动开放时间是")..schedule.ExternTip..LocalString.GetString("，目前暂未开放！"))
    end
end

function LuaZhouNianQing2023MainWnd:OnWelfareBtnClick()
    local schedule = Task_Schedule.GetData(ZhouNianQing2023_Setting.GetData().FuliTaskId)
    if CLuaScheduleMgr:IsFestivalTaskOpen(schedule.TaskID[0]) then
        local task = Task_Task.GetData(schedule.TaskID[0])
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= task.Level then
            self:ShowScheduleInfo(schedule)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("你不满%s级，无法参与玩法！"), tostring(task.Level), schedule.TaskName))
        end
    else
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("活动开放时间是")..schedule.ExternTip..LocalString.GetString("，目前暂未开放！"))
    end
end

function LuaZhouNianQing2023MainWnd:OnCouponBtnClick()
    local schedule = Task_Schedule.GetData(ZhouNianQing2023_Setting.GetData().LiquanTaskId)
    if CLuaScheduleMgr:IsFestivalTaskOpen(schedule.TaskID[0]) then
        local task = Task_Task.GetData(schedule.TaskID[0])
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= task.Level then
            self:ShowScheduleInfo(schedule)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("你不满%s级，无法参与玩法！"), tostring(task.Level), schedule.TaskName))
        end
    else
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("活动开放时间是")..schedule.ExternTip..LocalString.GetString("，目前暂未开放！"))
    end
end

function LuaZhouNianQing2023MainWnd:OnGiftPackBtnClick()
    CShopMallMgr.ShowLinyuShoppingMall(ZhouNianQing2023_Setting.GetData().LibaoMallItemId)
end

function LuaZhouNianQing2023MainWnd:OnPassportBtnClick()
    local schedule = Task_Schedule.GetData(42030312)
    if LuaZhouNianQing2023Mgr:IsPassOpen() then
        local task = Task_Task.GetData(schedule.TaskID[0])
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= task.Level then
            LuaZhouNianQing2023Mgr:OpenZhanLingWnd()
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("你不满%s级，无法参与玩法！"), tostring(task.Level), schedule.TaskName))
        end
    else
        g_MessageMgr:ShowCustomMsg(LocalString.GetString("活动开放时间是")..schedule.ExternTip..LocalString.GetString("，目前暂未开放！"))
    end
end