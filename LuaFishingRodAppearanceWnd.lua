local UILabel = import "UILabel"

local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local LayerDefine = import "L10.Engine.LayerDefine"

LuaFishingRodAppearanceWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFishingRodAppearanceWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaFishingRodAppearanceWnd, "Preview", "Preview", CUITexture)
RegistChildComponent(LuaFishingRodAppearanceWnd, "SelectedBtn", "SelectedBtn", GameObject)
RegistChildComponent(LuaFishingRodAppearanceWnd, "Title", "Title", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaFishingRodAppearanceWnd,"m_ModelTextureLoader")
RegistClassMember(LuaFishingRodAppearanceWnd,"m_UnLookedPoleTypeList")
RegistClassMember(LuaFishingRodAppearanceWnd,"m_PoleTypeList")
RegistClassMember(LuaFishingRodAppearanceWnd,"m_TableTextureList")
RegistClassMember(LuaFishingRodAppearanceWnd,"m_LoaderList")
RegistClassMember(LuaFishingRodAppearanceWnd,"m_MainRo")
function LuaFishingRodAppearanceWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.SelectedBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectedBtnClick()
	end)

    --@endregion EventBind end
	self.m_ModelTextureLoader  = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)
	self.m_PoleTypeList = {}
	self.m_UnLookedPoleTypeList = {}
	self.m_TableTextureList = {}
	self.m_LoaderList = {}
end

function LuaFishingRodAppearanceWnd:Init()
	local poleType = LuaSeaFishingMgr.m_SelectedPoleType	
	local poleData = HouseFish_PoleType.GetData(poleType)
	local preview = poleData.Preview
	self.Preview.mainTexture = CUIManager.CreateModelTexture("__poleappearance_main__", self.m_ModelTextureLoader,0,preview[0],preview[1],preview[2],false,true,preview[3],true,true)

	local data = HouseFish_SkillUpgrade.GetData(LuaSeaFishingMgr.m_CurPoleLevel)
	if data then
		local poleTypes = data.PoleLevel
		for i=0,poleTypes.Length-1,1 do 
			table.insert(self.m_UnLookedPoleTypeList,poleTypes[i])
		end
	end
	HouseFish_PoleType.ForeachKey(function (key) 
        table.insert(self.m_PoleTypeList,key)
		table.insert(self.m_TableTextureList,false)
		local loader = LuaDefaultModelTextureLoader.Create(function (ro)
			self:LoadItemModel(ro,key)
		end)
		table.insert(self.m_LoaderList,loader)
    end)

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_PoleTypeList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
	
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		local poleType = self.m_PoleTypeList[row+1]
		local data = HouseFish_PoleType.GetData(poleType)
		if LuaSeaFishingMgr.m_CurPoleLevel < data.UnLockedLevel then
			g_MessageMgr:FormatMessage("UngradeToGetPoleType",data.UnLockedLevel)
		end
		LuaSeaFishingMgr.m_SelectedPoleType = poleType
		self:InitPreviewTexture()
	end)
	self.TableView:ReloadData(false,false)
end

function LuaFishingRodAppearanceWnd:InitItem(item,row)
	local icon = item.transform:GetComponent(typeof(CUITexture))
	local name = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local mask = item.transform:Find("Mask").gameObject
	local texture = self.m_TableTextureList[row + 1]
	if not texture then
		local loader = self.m_LoaderList[row + 1]
		local type = self.m_PoleTypeList[row + 1]
		local preview = HouseFish_PoleType.GetData(type).Preview
		icon.mainTexture = CUIManager.CreateModelTexture("__poleappearance_item__"..row, loader,0,preview[0],preview[1],preview[2],false,true,preview[3],true,true)
		self.m_TableTextureList[row + 1] = true
	end

	local poleType = self.m_PoleTypeList[row + 1]
	local data = HouseFish_PoleType.GetData(poleType)
	if data then
		name.text = data.PoleName
		mask:SetActive(LuaSeaFishingMgr.m_CurPoleLevel < data.UnLockedLevel)
	end
end

function LuaFishingRodAppearanceWnd:LoadItemModel(ro,key)
	local poleType = key
	local poleData = HouseFish_PoleType.GetData(poleType)
	if poleData then
		local path = poleData.MaterialName
		ro.Layer = LayerDefine.ModelForNGUI_3D
        ro:LoadMain(path)
	end
end

function LuaFishingRodAppearanceWnd:LoadModel(ro)
	self.m_MainRo = ro
	local poleType = LuaSeaFishingMgr.m_SelectedPoleType	
	local poleData = HouseFish_PoleType.GetData(poleType)

	if poleData then
		local path = poleData.MaterialName
		ro.Layer = LayerDefine.ModelForNGUI_3D
        ro:LoadMain(path)
		self.Title.text = poleData.PoleName
	end
end

function LuaFishingRodAppearanceWnd:InitPreviewTexture()
	if self.m_MainRo then
		local poleType = LuaSeaFishingMgr.m_SelectedPoleType	
		local poleData = HouseFish_PoleType.GetData(poleType)
		if poleData then
			local path = poleData.MaterialName
			self.m_MainRo.Layer = LayerDefine.ModelForNGUI_3D
			self.m_MainRo:LoadMain(path)
			self.Title.text = poleData.PoleName
		end
	end
end

function LuaFishingRodAppearanceWnd:OnDestroy()
	if self.Preview.mainTexture then
		self.Preview.mainTexture = nil
		CUIManager.DestroyModelTexture("__poleappearance_main__")
	end
	for i,texture in ipairs(self.m_TableTextureList) do
		CUIManager.DestroyModelTexture("__poleappearance_item__"..(i-1))
	end
end
--@region UIEvent

function LuaFishingRodAppearanceWnd:OnSelectedBtnClick()
	if not LuaSeaFishingMgr.m_SelectedPoleType or LuaSeaFishingMgr.m_SelectedPoleType == 0 then
		g_MessageMgr:ShowMessage("ChoosePoleTypeFist")
		return 
	end
	local data = HouseFish_PoleType.GetData(LuaSeaFishingMgr.m_SelectedPoleType)
	if LuaSeaFishingMgr.m_CurPoleLevel >= data.UnLockedLevel then

		Gac2Gas.RequestSetFishPole(LuaSeaFishingMgr.m_SelectedPoleType)
	else
		g_MessageMgr:ShowMessage("UngradeToGetPoleType",data.UnLockedLevel)
	end
end

--@endregion UIEvent

