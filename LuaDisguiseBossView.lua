local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local DelegateFactory  = import "DelegateFactory"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"

LuaDisguiseBossView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDisguiseBossView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaDisguiseBossView, "InviteBtn", "InviteBtn", CButton)
RegistChildComponent(LuaDisguiseBossView, "LeaveBtn", "LeaveBtn", CButton)
RegistChildComponent(LuaDisguiseBossView, "TaskDes1", "TaskDes1", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDisguiseBossView, "m_ShowTextInfo")

function LuaDisguiseBossView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.InviteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInviteBtnClick()
	end)
	
	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick()
	end)


    --@endregion EventBind end
end

function LuaDisguiseBossView:Init()
	LuaShuJia2021Mgr.InitInfo()
	self.m_ShowTextInfo = LocalString.GetString("[c][98F7F7] 挑战者剩余失败次数 [-][c]")
	self.TaskDes1.text = ""
	self.TaskName.text = LocalString.GetString("[c][FBC701][暑假]"..LuaShuJia2021Mgr.bossName.."[-][c]")
end

function LuaDisguiseBossView:UpdateInfo()
	if not CScene.MainScene then return end
	if CScene.MainScene.ShowTimeCountDown then
		self.TaskDes1.text = self.m_ShowTextInfo..SafeStringFormat3( "%d\n",LuaShuJia2021Mgr.leftChallengeTimes )..self:GetRemainTimeText(CScene.MainScene.ShowTime)
 	end
end
--@region UIEvent

function LuaDisguiseBossView:GetRemainTimeText(totalSeconds)
	if not totalSeconds then return SafeStringFormat3( "%02d:%02d",0, 0) end
	if  totalSeconds < 0 then
		totalSeconds = 0
	end

	if totalSeconds >= 3600 then
		return SafeStringFormat3("%02d:%02d:%02d",math.floor(totalSeconds/3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
	else
		return SafeStringFormat3( "%02d:%02d",math.floor( totalSeconds / 60 ), totalSeconds % 60 )
	end
end

function LuaDisguiseBossView:OnInviteBtnClick()
	g_MessageMgr:ShowMessage(ShuJia_TravestyRole.GetData().GameplayConfirm)
end

function LuaDisguiseBossView:OnLeaveBtnClick()
	CGamePlayMgr.Inst:LeavePlay()
end


function LuaDisguiseBossView:OnEnable()
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate",self, "UpdateInfo")
end

function LuaDisguiseBossView:OnDisable()
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate",self, "UpdateInfo")
end
--@endregion UIEvent

