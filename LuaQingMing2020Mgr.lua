local CScene = import "L10.Game.CScene"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CPos = import "L10.Engine.CPos"
local Utility = import "L10.Engine.Utility"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

CLuaQingMing2020Mgr = class()

CLuaQingMing2020Mgr.RankSprites = {
    "Rank_No.1",
    "Rank_No.2png",
    "Rank_No.3png"
}

CLuaQingMing2020Mgr.PlayerStatus = {
    ["Idle"] = 1000,
	["Failed"] = 1100,
	["Success"] = 1101,
	["NotGhost"] = 1200,
	["Ghost"] = 1201
}

CLuaQingMing2020Mgr.PvePlayStatus = {
    ["Idle"] = 0,
	["Start"] = 1,
	["End"] = 2
}

CLuaQingMing2020Mgr.PassName = {
    LocalString.GetString("第一关"),
    LocalString.GetString("第二关"),
    LocalString.GetString("第三关"),
    LocalString.GetString("第四关"),
    LocalString.GetString("第五关")
}

CLuaQingMing2020Mgr.EnumQingMing2020PvePlayDataKey = {
    ePlayTimes = 2,

    ePassRound_1 = 11,
    ePassRound_2 = 12,
    ePassRound_3 = 13,
    ePassRound_4 = 14,
    ePassRound_5 = 15,

    eRewardRound_1 = 21,
    eRewardRound_2 = 22,
    eRewardRound_3 = 23,
    eRewardRound_4 = 24,
    eRewardRound_5 = 25,
}

CLuaQingMing2020Mgr.EnumQingMing2020PveRoundStatus = {
	["Idle"] = 1000,
	["End"] = 1001,

	["Prepare"] = 1101,
	["Guess"] = 1102,
	["Monster"] = 1103,
}

CLuaQingMing2020Mgr.NeedAlert = true

CLuaQingMing2020Mgr.eQingMing2020PvpPlayTimes = 111
CLuaQingMing2020Mgr.eQingMing2020CollectionData = 221
CLuaQingMing2020Mgr.eQingMing2020CollectionRewardTimes = 114

CLuaQingMing2020Mgr.PlayerBattleInfo = {}

CLuaQingMing2020Mgr.UpToRoundIndex = nil
CLuaQingMing2020Mgr.CurPveRoundIndex = 0
CLuaQingMing2020Mgr.RoundEndTime = 0


function CLuaQingMing2020Mgr.IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51101516 then
            return true
        end
    end
    return false
end

function CLuaQingMing2020Mgr:OnEnterPvpPlay()
    CUIManager.CloseUI(CLuaUIResources.QingMingJieZiXiaoChouWnd)
end

--pve 解字消愁
CLuaQingMing2020Mgr.roadTips = {}
CLuaQingMing2020Mgr.posList = {}
CLuaQingMing2020Mgr.targetList = {}

function CLuaQingMing2020Mgr.OnEnterPvePlay()
    g_MessageMgr:ShowMessage("QingMing2020_JZXC_Rule")
    PlayerSettings.HideHpcEnabled = false
    PlayerSettings.HideHeadInfoEnabled = false
    CLuaQingMing2020Mgr.CurPveRoundIndex = 0
    CUIManager.CloseUI(CLuaUIResources.QingMingJieZiXiaoChouWnd)

    --road tip
    CLuaQingMing2020Mgr.posList = {}
    CLuaQingMing2020Mgr.targetList = {}

    for x1,y1,x2,y2 in string.gmatch(QingMing2019_QingMing2020Pve.GetData().ArrowPosition, "(%d+),(%d+),(%d+),(%d+)") do
        x1 = tonumber(x1)
        y1 = tonumber(y1)
        x2 = tonumber(x2)
        y2 = tonumber(y2)
        table.insert(CLuaQingMing2020Mgr.posList,CPos(x1,y1))
        table.insert(CLuaQingMing2020Mgr.targetList,CPos(x2,y2))
    end
    
    for i,pos in ipairs(CLuaQingMing2020Mgr.posList) do
        if CLuaQingMing2020Mgr.roadTips[i] then
            CLuaQingMing2020Mgr.roadTips[i]:Destroy()
        end
        local curPos = CLuaQingMing2020Mgr.posList[i]
        local nextPos = CLuaQingMing2020Mgr.targetList[i]
        local angle = math.atan2(nextPos.y - curPos.y, nextPos.x - curPos.x) * Rad2Deg
        local fxId = QingMing2019_QingMing2020Pve.GetData().ArrowFx
        CLuaQingMing2020Mgr.roadTips[i] = CEffectMgr.Inst:AddWorldPositionFX(fxId, Utility.GridPos2PixelPos(CLuaQingMing2020Mgr.posList[i]),angle,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
    end
end

function CLuaQingMing2020Mgr.OnPlayEnd()
    for k,v in pairs(CLuaQingMing2020Mgr.roadTips) do
        v:Destroy()
    end
	CLuaQingMing2020Mgr.roadTips = {}
end

function CLuaQingMing2020Mgr.IsInPvePlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51101501 then
            return true
        end
    end
    return false
end


--清明风物志
CLuaQingMing2020Mgr.FinishedTaskIds = {}
