-- Auto Generated!!
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local CFirstChargeLevelTemplate = import "L10.UI.CFirstChargeLevelTemplate"
local CommonDefs = import "L10.Game.CommonDefs"
local CSigninMgr = import "L10.Game.CSigninMgr"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
CFirstChargeLevelTemplate.m_Init_CS2LuaHook = function (this, level, index) 
    local list = nil
    repeat
        local default = index
        if default == (0) then
            list = CSigninMgr.Inst.firstChargeLevel1
            break
        elseif default == (1) then
            list = CSigninMgr.Inst.firstChargeLevel2
            break
        elseif default == (2) then
            list = CSigninMgr.Inst.firstChargeLevel3
            break
        else
            break
        end
    until 1

    Extensions.RemoveAllChildren(this.table.transform)
    this.itemCell:SetActive(false)
    if list ~= nil then
        if CommonDefs.IS_HMT_CLIENT then
            this.title.text = System.String.Format(LocalString.GetString("儲值滿\n{0}靈玉送"), level)
        elseif CommonDefs.IS_KR_CLIENT then
            this.title.text = System.String.Format(LocalString.GetString("充值满\n{0}倩女点送"), level)
        elseif CommonDefs.IS_VN_CLIENT then
            this.title.text = System.String.Format(LocalString.GetString("充值满\n{0}灵玉送"), level)
        else    
            this.title.text = System.String.Format(LocalString.GetString("充值满\n{0}元送"), level)
        end
        do
            local i = 0
            while i < list.Count do
                local instance = NGUITools.AddChild(this.table.gameObject, this.itemCell)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CFirstChargeGiftCell)):Init(list[i].templateId, list[i].amount)
                i = i + 1
            end
        end
        this.table:Reposition()
    end
end
