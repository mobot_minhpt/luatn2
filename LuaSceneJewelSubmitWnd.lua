local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector2 = import "UnityEngine.Vector2"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local NGUITools = import "NGUITools"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local NGUIMath = import "NGUIMath"

LuaSceneJewelSubmitWnd = class()
RegistChildComponent(LuaSceneJewelSubmitWnd,"closeBtn", GameObject)
RegistChildComponent(LuaSceneJewelSubmitWnd,"templateNode", GameObject)
RegistChildComponent(LuaSceneJewelSubmitWnd,"scrollView", UIScrollView)
RegistChildComponent(LuaSceneJewelSubmitWnd,"grid", UIGrid)
RegistChildComponent(LuaSceneJewelSubmitWnd,"submitBtn", GameObject)
RegistChildComponent(LuaSceneJewelSubmitWnd,"buyBtn", GameObject)
RegistChildComponent(LuaSceneJewelSubmitWnd,"tipBtn", GameObject)
RegistChildComponent(LuaSceneJewelSubmitWnd,"Background", GameObject)

RegistClassMember(LuaSceneJewelSubmitWnd, "nowChooseNode")
RegistClassMember(LuaSceneJewelSubmitWnd, "nowChooseData")

function LuaSceneJewelSubmitWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SceneJewelSubmitWnd)
end

function LuaSceneJewelSubmitWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "Init")
	g_ScriptEvent:AddListener("SetItemAt", self, "Init")
end

function LuaSceneJewelSubmitWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "Init")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "Init")
end

function LuaSceneJewelSubmitWnd:InitSubmitItem()
  if not CClientMainPlayer.Inst then
    self:Close()
    return
  end

  local needShowItemIdTable = {}
  ScenesItem_ScenesJewelFx.Foreach(function(key,value)
    needShowItemIdTable[value.ItemID] = key
  end)

  local showJewelDataTable = {}
	local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
	do
		local i = 1
		while i <= bagSize do
			local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
			if not System.String.IsNullOrEmpty(id) then
				local itemData = CItemMgr.Inst:GetById(id)
				if itemData ~= nil then
          if needShowItemIdTable[itemData.TemplateId] and needShowItemIdTable[itemData.TemplateId] > 0 then
            table.insert(showJewelDataTable,{data = itemData,pos = i,itemId = id, choiceId = needShowItemIdTable[itemData.TemplateId]})
          end
				end
			end
			i = i + 1
		end
	end

	if self.nowChooseNode then
		self.nowChooseNode:SetActive(false)
		self.nowChooseNode = nil
	end
	Extensions.RemoveAllChildren(self.grid.transform)
  for i,v in ipairs(showJewelDataTable) do
    local node = NGUITools.AddChild(self.grid.gameObject, self.templateNode)
    node:SetActive(true)
    local icon = node.transform:Find('IconTexture'):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(v.data.Icon)
    local num = node.transform:Find('AmountLabel'):GetComponent(typeof(UILabel))
    num.text = v.data.Amount
    local lightNode = node.transform:Find('SelectedSprite').gameObject
    lightNode:SetActive(false)
    --self.bindSprite.spriteName = item.BindOrEquipCornerMark
		local bindNode = node.transform:Find('BindSprite').gameObject
		if v.data.BindOrEquipCornerMark then
			bindNode:SetActive(true)
		else
			bindNode:SetActive(false)
		end

    local onItemClick = function(go)
      if self.nowChooseNode then
        self.nowChooseNode:SetActive(false)
      end
      self.nowChooseNode = lightNode
      self.nowChooseNode:SetActive(true)
      self.nowChooseData = v

			self:ShowItemTip(v.itemId)
    end
    CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onItemClick),false)
  end

  self.grid:Reposition()
  self.scrollView:ResetPosition()
end

function LuaSceneJewelSubmitWnd:ShowItemTip(itemId)
	local b1 = NGUIMath.CalculateRelativeWidgetBounds(self.Background.transform)
	local b2 = NGUIMath.CalculateRelativeWidgetBounds(self.Background.transform, self.closeBtn.transform)
	local height = b1.size.y
	local worldCenterY = self.Background.transform:TransformPoint(b1.center).y
	--b1:Encapsulate(b2)
	local width = b1.size.x
	local worldCenterX = self.Background.transform:TransformPoint(b1.center).x

	CItemInfoMgr.ShowLinkItemInfo(itemId, false, nil, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
end

function LuaSceneJewelSubmitWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  self.templateNode:SetActive(false)

  self:InitSubmitItem()
	local onSubmitClick = function(go)
    if not self.nowChooseData then
      return
    end
		if LuaSceneInteractiveMgr.NowID and LuaSceneInteractiveMgr.NowChoiceId then
			Gac2Gas.ConsumeJewelPlaySceneFx(self.nowChooseData.itemId, EnumItemPlace.Bag, self.nowChooseData.pos, LuaSceneInteractiveMgr.NowID, LuaSceneInteractiveMgr.NowChoiceId)
			LuaSceneInteractiveMgr.NowID = nil
			LuaSceneInteractiveMgr.NowChoiceId = nil
		end
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.submitBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
	local onBuyClick = function(go)
    local itemId = 21000105
    local bound = NGUIMath.CalculateRelativeWidgetBounds(self.buyBtn.transform)
    local targetSize = Vector2(bound.size.x, bound.size.y)
    local targetCenterPos = self.buyBtn.transform:TransformPoint(bound.center)
    CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, true, targetSize, targetCenterPos, CTooltipAlignType.Top)
	end
	CommonDefs.AddOnClickListener(self.buyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)

  local onTipClick = function(go)
	  if CommonDefs.IS_VN_CLIENT then
		  g_MessageMgr:ShowMessage("ThrowJewelsTips_VN")
	  else
		  g_MessageMgr:ShowMessage("ThrowJewelsTips")
	  end
  end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
end

return LuaSceneJewelSubmitWnd
