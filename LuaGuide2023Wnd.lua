local CLevelUpGuideMasterView = import "L10.UI.CLevelUpGuideMasterView"
local CLevelUpGuideDetailView = import "L10.UI.CLevelUpGuideDetailView"

local QnInput = import "L10.UI.QnInput"
local UIInput = import "UIInput"
local UIGrid = import "UIGrid"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local UILabel = import "UILabel"
local MessageMgr = import "L10.Game.MessageMgr"
local GameObject = import "UnityEngine.GameObject"
local Extensions = import "Extensions"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local NGUITools = import "NGUITools"
local ClientAction = import "L10.UI.ClientAction"
local PopupMenuItemData          = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr          = import "L10.UI.CPopupMenuInfoMgr"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local Animation = import "UnityEngine.Animation"

LuaGuide2023Wnd = class()

LuaGuide2023Wnd.m_firstOpenThirdMenuIndex = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuide2023Wnd, "FirstMenu", "FirstMenu", UIGrid)
RegistChildComponent(LuaGuide2023Wnd, "FirstMenuButtonTempalte", "FirstMenuButtonTempalte", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "FirstMenuScrollview", "FirstMenuScrollview", CUIRestrictScrollView)
RegistChildComponent(LuaGuide2023Wnd, "SecondMenuWidget", "SecondMenuWidget", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "SecondMenuScrollview", "SecondMenuScrollview", CUIRestrictScrollView)
RegistChildComponent(LuaGuide2023Wnd, "SecondMenuGrid", "SecondMenuGrid", UIGrid)
RegistChildComponent(LuaGuide2023Wnd, "SectionTempalte", "SectionTempalte", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "RowTemplate", "RowTemplate", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "PicWidget", "PicWidget", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "IndicatorGrid", "IndicatorGrid", UIGrid)
RegistChildComponent(LuaGuide2023Wnd, "Indicator", "Indicator", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "PicBottom", "PicBottom", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "GoGuideBtn", "GoGuideBtn", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "NextArrow", "NextArrow", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "LastArrow", "LastArrow", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "GuideIntroduction", "GuideIntroduction", UILabel)
RegistChildComponent(LuaGuide2023Wnd, "Pics", "Pics", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "PicTemplate", "PicTemplate", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "SearchWidget", "SearchWidget", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "Input", "Input", QnInput)
RegistChildComponent(LuaGuide2023Wnd, "SearchResult", "SearchResult", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "ClearSearchBtn", "ClearSearchBtn", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "SearchButton", "SearchButton", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "SearchButtonsTable", "SearchButtonsTable", UITable)
RegistChildComponent(LuaGuide2023Wnd, "NoSearchResult", "NoSearchResult", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "HistoryBtn", "HistoryBtn", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "HistoryButton", "HistoryButton", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "NoHistoryResult", "NoHistoryResult", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "HistoryButtonsTable", "HistoryButtonsTable", UITable)
RegistChildComponent(LuaGuide2023Wnd, "QuestionHistory", "QuestionHistory", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "LevelUpGuideWidget", "LevelUpGuideWidget", GameObject)
RegistChildComponent(LuaGuide2023Wnd, "MasterView", "MasterView", CLevelUpGuideMasterView)
RegistChildComponent(LuaGuide2023Wnd, "DetailView", "DetailView", CLevelUpGuideDetailView)

--@endregion RegistChildComponent end

function LuaGuide2023Wnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:InitUIEvent()
    self:OnSyncGuideManualData()
end

function LuaGuide2023Wnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaGuide2023Wnd:InitWndData()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end


    self.menuNameDict = {}
    local playerLevel = mainPlayer.MaxLevel
    self.sortedMenuDataList = {}
    self.rawMenuDataList = {}
    Guide_GuideIntroduction2023.Foreach(function(key, data)
        if data.Status == 0 and playerLevel >= data.Level then
            local firstMenuIndex = data.FirstMenu
            local secondMenuIndex = data.SecondMenu
            table.insert(self.sortedMenuDataList, {firstMenuIndex = firstMenuIndex, secondMenuIndex = secondMenuIndex, thirdMenuIndex = key})

            if not self.rawMenuDataList[firstMenuIndex] then
                self.rawMenuDataList[firstMenuIndex] = {}
            end
            if not self.rawMenuDataList[firstMenuIndex][secondMenuIndex] then
                self.rawMenuDataList[firstMenuIndex][secondMenuIndex] = {}
            end
            table.insert(self.rawMenuDataList[firstMenuIndex][secondMenuIndex], key)

        end
    end)
    table.sort(self.sortedMenuDataList, function(a, b)
        if a.firstMenuIndex == b.firstMenuIndex then
            if b.secondMenuIndex == b.secondMenuIndex then
                return a.thirdMenuIndex < b.thirdMenuIndex
            else
                return a.secondMenuIndex < b.secondMenuIndex
            end
        else
            return a.firstMenuIndex < b.firstMenuIndex
        end
    end)

    Guide_GuideIntroduction2023Menu.Foreach(function(key, data)
        self.menuNameDict[key] = data.Name
    end)

    self.firstMenuButtonList = {}

    self.thirdMenuIndex = 0

    if LuaGuide2023Wnd.m_firstOpenThirdMenuIndex == nil then
        --do nothing
        self.firstOpenThirdMenuData = nil
    else
        --想要直接打开thirdMenuIndex这条
        self.firstOpenThirdMenuIndex = self.m_firstOpenThirdMenuIndex
        self.m_firstOpenThirdMenuIndex = nil
    end
    
    self.initLevelWidget = true
end

function LuaGuide2023Wnd:OnEnable()
    g_ScriptEvent:AddListener("SyncGuideManualData", self, "OnSyncGuideManualData")
end

function LuaGuide2023Wnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncGuideManualData", self, "OnSyncGuideManualData")
end

function LuaGuide2023Wnd:OnSyncGuideManualData()
    if CClientMainPlayer.Inst then
        local reddotFlag = CClientMainPlayer.Inst.PlayProp.GuideManualData.ReddotFlag
        self:UpdateAllReddots(reddotFlag)
    end
end

function LuaGuide2023Wnd:UpdateAllReddots(reddotFlag)
    local showFirstMenuObjReddot = {}
    local showSecondMenuObjReddot = {}
    local showThirdMenuObjReddot = {}
    
    for i = 1, #self.sortedMenuDataList do
        local firstMenuIndex = self.sortedMenuDataList[i].firstMenuIndex
        local secondMenuIndex = self.sortedMenuDataList[i].secondMenuIndex
        local thirdMenuIndex = self.sortedMenuDataList[i].thirdMenuIndex
        if reddotFlag:GetBit(thirdMenuIndex) then
            showFirstMenuObjReddot[firstMenuIndex] = true
            showSecondMenuObjReddot[secondMenuIndex] = true
            showThirdMenuObjReddot[thirdMenuIndex] = true
        end
    end

    for k, v in pairs(self.firstMenuButtonList) do
        v.transform:Find("RedDot").gameObject:SetActive(showFirstMenuObjReddot[k] and showFirstMenuObjReddot[k] or false)
    end
    for k, v in pairs(self.secondMenuButtonList) do
        v.transform:Find("RedDot").gameObject:SetActive(showSecondMenuObjReddot[k] and showSecondMenuObjReddot[k] or false)
    end
    for k, v in pairs(self.thirdMenuButtonList) do
        v.transform:Find("RedDot").gameObject:SetActive(showThirdMenuObjReddot[k] and showThirdMenuObjReddot[k] or false)
    end
end

function LuaGuide2023Wnd:RefreshConstUI()
    self:CreateFirstMenuObjects()
    self.SearchResult:SetActive(false)
    self.ClearSearchBtn:SetActive(false)
    self.QuestionHistory:SetActive(false)
end

function LuaGuide2023Wnd:InitUIEvent()
    UIEventListener.Get(self.NextArrow).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnSimulateDrag(self.fourthIndicatorIndex+1)
    end)

    UIEventListener.Get(self.LastArrow).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnSimulateDrag(self.fourthIndicatorIndex-1)
    end)
    
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        local formatStr = MessageMgr.Inst:GetMessageFormat("Guide2023_SHARE_LINK", false)
        local msg = System.String.Format(formatStr, tostring(self.thirdMenuIndex))
        local getShareTextFunc = function()
            return msg
        end

        local tbl = {}
        local shareToWorld = LuaInGameShareMgr.GetShareToWorldMenu(getShareTextFunc, nil)
        table.insert(tbl, shareToWorld)

        local shareToGuild = LuaInGameShareMgr.GetShareToGuildMenu(getShareTextFunc, nil)
        table.insert(tbl, shareToGuild)

        local shareToTeam = LuaInGameShareMgr.GetShareToTeamMenu(getShareTextFunc, nil)
        table.insert(tbl, shareToTeam)

        local shareToFriend = LuaInGameShareMgr.GetShareToFriendMenu(getShareTextFunc, nil)
        table.insert(tbl, shareToFriend)

        local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))

        CPopupMenuInfoMgr.ShowPopupMenu(array, self.ShareBtn.transform, CPopupMenuInfoMgrAlignType.Right, 1, nil, 600, true, 320)
    end)

    self.Input.OnValueChanged = DelegateFactory.Action_string(function (value)
        self:OnSearchGuideText(value)
    end)
    
    UIEventListener.Get(self.ClearSearchBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        self.Input.Text = ""
        self:OnSearchGuideText("")
    end)
    
    UIEventListener.Get(self.HistoryBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnHistoryBtnClick()
    end)
end

function LuaGuide2023Wnd:CreateFirstMenuObjects()
    --create first menu objects
    Extensions.RemoveAllChildren(self.FirstMenu.transform)
    self.FirstMenuButtonTempalte:SetActive(false)
    local isFirstMenuObjectCreated = {}
    local isFirst = true
    for i = 1, #self.sortedMenuDataList do
        local firstMenuIndex = self.sortedMenuDataList[i].firstMenuIndex
        if not isFirstMenuObjectCreated[firstMenuIndex] then
            local obj = NGUITools.AddChild(self.FirstMenu.gameObject, self.FirstMenuButtonTempalte)
            obj:SetActive(true)
            obj.transform:Find("text"):GetComponent("UILabel").text = self.menuNameDict[firstMenuIndex]
            obj.transform:Find("Btn_Highlight").gameObject:SetActive(false)
            UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (_)
                self:ClickFirstMenu(firstMenuIndex)
                for k, v in pairs(self.firstMenuButtonList) do
                    v.transform:Find("Btn_Highlight").gameObject:SetActive(k == firstMenuIndex)
                end
            end)
            isFirstMenuObjectCreated[firstMenuIndex] = true
            self.firstMenuButtonList[firstMenuIndex] = obj
            if self.firstOpenThirdMenuIndex == nil then
                if isFirst then
                    self:ClickFirstMenu(firstMenuIndex)
                    for k, v in pairs(self.firstMenuButtonList) do
                        v.transform:Find("Btn_Highlight").gameObject:SetActive(k == firstMenuIndex)
                    end
                    isFirst = false
                end
            else   
                --想要打开的时候 直接是特定的一条
                local firstOpenThirdMenuData = Guide_GuideIntroduction2023.GetData(self.firstOpenThirdMenuIndex)
                if firstMenuIndex == firstOpenThirdMenuData.FirstMenu and isFirst then
                    self:ClickFirstMenu(firstMenuIndex)
                    for k, v in pairs(self.firstMenuButtonList) do
                        v.transform:Find("Btn_Highlight").gameObject:SetActive(k == firstMenuIndex)
                    end
                    isFirst = false
                end
            end
        end
    end
    --等级指引
    local obj = NGUITools.AddChild(self.FirstMenu.gameObject, self.FirstMenuButtonTempalte)
    self.firstMenuButtonList[0] = obj
    obj:SetActive(true)
    obj.transform:Find("text"):GetComponent("UILabel").text = LocalString.GetString("等级")
    obj.transform:Find("Btn_Highlight").gameObject:SetActive(false)
    UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (_)
        for k, v in pairs(self.firstMenuButtonList) do
            v.transform:Find("Btn_Highlight").gameObject:SetActive(k == 0)
        end
        self:OnClickLevelMenu()
    end)
    
    self.FirstMenu:Reposition()
    self.FirstMenuScrollview:ResetPosition()
end

function LuaGuide2023Wnd:OnClickLevelMenu()
    self.SecondMenuWidget:SetActive(false)
    self.PicWidget:SetActive(false)
    self.LevelUpGuideWidget:SetActive(true)

    if self.initLevelWidget then
        self.MasterView.OnSelected = DelegateFactory.Action_object(function (levelSection)
            self.DetailView:Init(levelSection)
        end)
        self.MasterView:Init()
        self.initLevelWidget = false
        
    end
end

function LuaGuide2023Wnd:ClickFirstMenu(clickFirstMenuIndex)
    self.SecondMenuWidget:SetActive(true)
    self.PicWidget:SetActive(true)
    self.LevelUpGuideWidget:SetActive(false)
    
    self.PicWidget.transform:GetComponent(typeof(Animation)):Play("guide2023wnd_picwidget")
    
    local isFirst = true
    for i = 1, #self.sortedMenuDataList do
        local firstMenuIndex = self.sortedMenuDataList[i].firstMenuIndex
        local secondMenuIndex = self.sortedMenuDataList[i].secondMenuIndex
        if self.firstOpenThirdMenuIndex == nil then
            if firstMenuIndex == clickFirstMenuIndex and isFirst then
                self:CreateOtherMenuObjects(firstMenuIndex, secondMenuIndex)
                isFirst = false
            end
        else
            --想要打开的时候 直接是特定的一条
            local firstOpenThirdMenuData = Guide_GuideIntroduction2023.GetData(self.firstOpenThirdMenuIndex)
            if firstMenuIndex == firstOpenThirdMenuData.FirstMenu and secondMenuIndex == firstOpenThirdMenuData.SecondMenu and isFirst then
                self:CreateOtherMenuObjects(firstMenuIndex, secondMenuIndex)
                isFirst = false
            end
        end
    end
    --self.FirstMenuScrollview:ResetPosition()
end

function LuaGuide2023Wnd:CreateOtherMenuObjects(clickFirstMenuIndex, clickSecondMenuIndex)
    Extensions.RemoveAllChildren(self.SecondMenuGrid.transform)
    self.SectionTempalte:SetActive(false)
    self.RowTemplate:SetActive(false)
    self.secondMenuButtonList = {}
    self.thirdMenuButtonList = {}
    
    local isFirstSecondMenu = true
    local isFirstThirdMenu = true
    local isSecondMenuObjectCreated = {}
    for i = 1, #self.sortedMenuDataList do
        local firstMenuIndex = self.sortedMenuDataList[i].firstMenuIndex
        local secondMenuIndex = self.sortedMenuDataList[i].secondMenuIndex
        local thirdMenuIndex = self.sortedMenuDataList[i].thirdMenuIndex
        
        --create second menu object
        if not isSecondMenuObjectCreated[secondMenuIndex] and firstMenuIndex == clickFirstMenuIndex then
            local obj = NGUITools.AddChild(self.SecondMenuGrid.gameObject, self.SectionTempalte)
            obj:SetActive(true)
            obj.transform:Find("Label"):GetComponent("UILabel").text = self.menuNameDict[secondMenuIndex]
            obj.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_02_normal"
            obj.transform:Find("Label"):GetComponent("UILabel").color = NGUIText.ParseColor24("83C9FF", 0)
            UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (_)
                self:CreateOtherMenuObjects(firstMenuIndex, secondMenuIndex)
            end)
            isSecondMenuObjectCreated[secondMenuIndex] = true
            self.secondMenuButtonList[secondMenuIndex] = obj

            if isFirstSecondMenu and firstMenuIndex == clickFirstMenuIndex and secondMenuIndex == clickSecondMenuIndex then
                isFirstSecondMenu = false
                obj.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_02_highlight"
                obj.transform:Find("Label"):GetComponent("UILabel").color = NGUIText.ParseColor24("1E2B62", 0)
            end
        end

        --create third menu object
        if firstMenuIndex == clickFirstMenuIndex and secondMenuIndex == clickSecondMenuIndex then
            local obj = NGUITools.AddChild(self.SecondMenuGrid.gameObject, self.RowTemplate)
            obj:SetActive(true)
            obj.transform:Find("Label"):GetComponent("UILabel").text = Guide_GuideIntroduction2023.GetData(thirdMenuIndex).Name
            UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (_)
                self:ClickThirdMenu(thirdMenuIndex, true)

                for k, v in pairs(self.thirdMenuButtonList) do
                    v.transform:GetComponent(typeof(UISprite)).spriteName = k == thirdMenuIndex and "common_btn_03_highlight" or "common_btn_03_normal"
                end
            end)
            self.thirdMenuButtonList[thirdMenuIndex] = obj
            
            if self.firstOpenThirdMenuIndex == nil then
                if isFirstThirdMenu then
                    self:ClickThirdMenu(thirdMenuIndex, false)
                    obj.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_03_highlight"
                    isFirstThirdMenu = false
                end
            else
                --想要打开的时候 直接是特定的一条
                local firstOpenThirdMenuData = Guide_GuideIntroduction2023.GetData(self.firstOpenThirdMenuIndex)
                if thirdMenuIndex == firstOpenThirdMenuData.ID and isFirstThirdMenu then
                    self:ClickThirdMenu(thirdMenuIndex, false)
                    obj.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_03_highlight"
                    isFirstThirdMenu = false
                end
            end
        end
        
    end
    self.SecondMenuGrid:Reposition()
    self:OnSyncGuideManualData()
end

function LuaGuide2023Wnd:ClickThirdMenu(thirdMenuIndex, trySyncReddots)
    self.PicTemplate:SetActive(false)
    local thirdMenuData = Guide_GuideIntroduction2023.GetData(thirdMenuIndex)
    --action and show guide button
    local menuAction = thirdMenuData.Action
    local showGuideBtn = not System.String.IsNullOrEmpty(menuAction)
    self.GoGuideBtn:SetActive(showGuideBtn)
    if showGuideBtn then
        UIEventListener.Get(self.GoGuideBtn).onClick = DelegateFactory.VoidDelegate(function (_)
            ClientAction.DoAction(menuAction)
        end)
    end
    
    --description and pic
    local descriptionList = g_LuaUtil:StrSplit(thirdMenuData.Description, ";")
    local picList = g_LuaUtil:StrSplit(thirdMenuData.Pic, ";")
    
    self.thirdMenuIndex = thirdMenuIndex
    self.NextArrow:SetActive(#descriptionList > 1)
    self.LastArrow:SetActive(#descriptionList > 1)
    
    Extensions.RemoveAllChildren(self.Pics.transform)
    self.fourthDataList = {}
    self.fourthUITextures = {}
    self.fourthUITextureBorders = {}
    self.fourthAngles = {}
    self.fourthDeltaDegree = math.floor(360 / #descriptionList)
    
    for i = 1, #descriptionList do
        table.insert(self.fourthDataList, {
            description = descriptionList[i],
            pic = picList[i],
        })
    end
    for i = 1, #self.fourthDataList do
        self.fourthAngles[i] = (i-1) * self.fourthDeltaDegree
    end
    for i = 1, #self.fourthDataList do
        local obj = NGUITools.AddChild(self.Pics, self.PicTemplate)
        obj:SetActive(true)
        obj.transform.localPosition = Vector3(0, -40, 0)
        local texture = obj:GetComponent(typeof(CUITexture))
        texture:LoadMaterial(self.fourthDataList[i].pic)

        local uiTexture = obj:GetComponent(typeof(UITexture))
        table.insert( self.fourthUITextures, uiTexture)
        local border = obj:GetComponent(typeof(UIWidget))
        table.insert( self.fourthUITextureBorders, border)
        
        UIEventListener.Get(obj).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
            self:OnDrag(g,delta)
        end)
        UIEventListener.Get(obj).onDragEnd = DelegateFactory.VoidDelegate(function(g)
            self:OnDragEnd(g)
        end)
        UIEventListener.Get(obj).onDragStart = DelegateFactory.VoidDelegate(function(g)
            self:OnDragStart(g)
        end)
    end
    self:UpdatePosition()
    CUICommonDef.ClearTransform(self.IndicatorGrid.transform)
    self.Indicator:SetActive(false)
    if #self.fourthDataList>1 then
        for i = 1, #self.fourthDataList do
            local igo=NGUITools.AddChild(self.IndicatorGrid.gameObject, self.Indicator)
            igo:SetActive(true)
        end
        self.IndicatorGrid:Reposition()
    end
    self:UpdateIndicator(1)

    if trySyncReddots then
        if CClientMainPlayer.Inst then
            local reddotFlag = CClientMainPlayer.Inst.PlayProp.GuideManualData.ReddotFlag
            if reddotFlag:GetBit(thirdMenuIndex) then
                Gac2Gas.GuideManualClearReddot(thirdMenuIndex)
            end
        end
    end
end

function LuaGuide2023Wnd:UpdateIndicator(index)
    for i = 1, self.IndicatorGrid.transform.childCount do
        local sprite = self.IndicatorGrid.transform:GetChild(i-1):GetComponent(typeof(UISprite))
        sprite.color = i == index and Color.white or Color(0.3,0.5,0.8)
    end
    self.fourthIndicatorIndex = index
    self.GuideIntroduction.text = self.fourthDataList[index].description
end

function LuaGuide2023Wnd:OnSimulateDrag(selectIndex)
    if selectIndex<1 then selectIndex=selectIndex+#self.fourthAngles end
    if selectIndex>#self.fourthAngles then selectIndex=selectIndex-#self.fourthAngles end
    self.fourthIndicatorIndex = selectIndex

    for i,v in ipairs(self.fourthAngles) do
        local degree = (i-selectIndex)*self.fourthDeltaDegree
        degree = degree - math.floor(degree/360)*360

        local moveTo =  math.sin(0.0174533*degree)*118
        local scale = math.cos(0.0174533*degree)*0.16+0.84
        scale=math.max(0,math.min(1,scale))

        local tf = self.Pics.transform:GetChild(i-1)

        LuaTweenUtils.TweenPositionX(tf,moveTo,0.3)
        LuaTweenUtils.TweenScaleTo(tf, Vector3(scale,scale,1),0.3)

        self.fourthUITextures[i].depth = math.floor(scale*100)*2
        self.fourthUITextures[i].alpha=scale*scale>0.95 and 1 or scale*scale*scale*scale
        self.fourthUITextureBorders[i].depth = self.fourthUITextures[i].depth-1

        self.fourthAngles[i]=degree
    end

    self:UpdateIndicator(selectIndex)
end

function LuaGuide2023Wnd:OnDrag(go, delta)
    local moveDeg = 57.2958*math.asin(math.max(-1,math.min(delta.x/400,1)))
    for i,v in ipairs(self.fourthAngles) do
        local angle = v + moveDeg
        self.fourthAngles[i] = angle - math.floor(angle/360)*360
    end
    self:UpdatePosition()
end

function LuaGuide2023Wnd:OnDragStart(go)
    for i,v in ipairs(self.fourthAngles) do
        local tf = self.Pics.transform:GetChild(i-1)
        LuaTweenUtils.DOKill(tf, false)
    end
end

function LuaGuide2023Wnd:OnDragEnd(go)
    local selectIndex=0

    local index=go.transform:GetSiblingIndex()+1
    local x = go.transform.localPosition.x
    local scale = go.transform.localScale.x
    --看划过的距离
    if x>-50 and x<50 and scale>0.7 then
        selectIndex=index
    elseif x<0 then
        selectIndex=index+1
    elseif x>0 then
        selectIndex=index-1
    end

    if selectIndex<1 then selectIndex=selectIndex+#self.fourthAngles end
    if selectIndex>#self.fourthAngles then selectIndex=selectIndex-#self.fourthAngles end


    for i,v in ipairs(self.fourthAngles) do
        local degree = (i-selectIndex)*self.fourthDeltaDegree
        degree = degree - math.floor(degree/360)*360

        local moveTo =  math.sin(0.0174533*degree)*118
        local scale = math.cos(0.0174533*degree)*0.16+0.84
        scale=math.max(0,math.min(1,scale))

        local tf = self.Pics.transform:GetChild(i-1)

        LuaTweenUtils.TweenPositionX(tf,moveTo,0.3)
        LuaTweenUtils.TweenScaleTo(tf, Vector3(scale,scale,1),0.3)

        self.fourthUITextures[i].depth = math.floor(scale*100)*2
        self.fourthUITextures[i].alpha=scale*scale>0.95 and 1 or scale*scale*scale*scale
        self.fourthUITextureBorders[i].depth = self.fourthUITextures[i].depth-1

        self.fourthAngles[i]=degree
    end

    self:UpdateIndicator(selectIndex)
end

function LuaGuide2023Wnd:UpdatePosition()
    for i,v in ipairs(self.fourthDataList) do
        local tf = self.Pics.transform:GetChild(i-1)

        local angle=self.fourthAngles[i]
        angle = angle - math.floor(angle/360)*360

        local offset = math.sin(0.0174533*angle)*118
        LuaUtils.SetLocalPositionX( tf,offset)

        local scale = math.cos(0.0174533*angle)*0.16+0.84
        LuaUtils.SetLocalScale(tf,scale,scale,1)

        self.fourthUITextures[i].depth = math.floor(scale*100)*2
        self.fourthUITextures[i].alpha=scale*scale>0.95 and 1 or scale*scale*scale*scale
        self.fourthUITextureBorders[i].depth = self.fourthUITextures[i].depth-1
    end
end

----Region 搜索及历史记录
function LuaGuide2023Wnd:OnSearchGuideText(inputStr)
    if inputStr == "" then
        --是一个空的输入
        self.SearchResult:SetActive(false)
        self.ClearSearchBtn:SetActive(false)
        return
    else
        self.ClearSearchBtn:SetActive(true)
        self.SearchResult:SetActive(true)
    end

    self.SearchButton:SetActive(false)
    local isFindOnce = false
    Extensions.RemoveAllChildren(self.SearchButtonsTable.transform)
    for i = 1, #self.sortedMenuDataList do
        local thirdIndex = self.sortedMenuDataList[i].thirdMenuIndex
        local cfgData = Guide_GuideIntroduction2023.GetData(thirdIndex)
        if string.find(cfgData.Name, inputStr) then
            isFindOnce = true
            local searchButtonsChild = NGUITools.AddChild(self.SearchButtonsTable.gameObject, self.SearchButton)
            searchButtonsChild:SetActive(true)
            searchButtonsChild.transform:Find("Label"):GetComponent(typeof(UILabel)).text = cfgData.Name
            UIEventListener.Get(searchButtonsChild).onClick = DelegateFactory.VoidDelegate(function (_)
                self:OpenGuideMenu(thirdIndex)
            end)
        end
    end
    self.SearchButtonsTable:Reposition()
    self.NoSearchResult:SetActive(not isFindOnce)
end

function LuaGuide2023Wnd:OpenGuideMenu(thirdIndex)
    Gac2Gas.GuideManualAddSearchHistory(thirdIndex)
    local cfgData = Guide_GuideIntroduction2023.GetData(thirdIndex)
    local firstMenuIndex = cfgData.FirstMenu
    self.firstOpenThirdMenuIndex = thirdIndex
    self:ClickFirstMenu(firstMenuIndex)
    for k, v in pairs(self.firstMenuButtonList) do
        v.transform:Find("Btn_Highlight").gameObject:SetActive(k == firstMenuIndex)
    end
end

function LuaGuide2023Wnd:OnHistoryBtnClick()
    --清空搜索栏
    self.Input.Text = ""
    self:OnSearchGuideText("")

    --打开历史记录
    self.QuestionHistory:SetActive(true)
    local mainPlayer = CClientMainPlayer.Inst
    self.historySearchData = {}
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eGuideManualSearchHistory) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eGuideManualSearchHistory)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.historySearchData = list
    end
    print(LuaYiRenSiMgr:PrintTable(self.historySearchData))

    Extensions.RemoveAllChildren(self.HistoryButtonsTable.transform)
    self.HistoryButton:SetActive(false)
    local createHistoryTbl = {}
    --倒序
    for i = #self.historySearchData, 1, -1  do
        local thirdIndex = self.historySearchData[i]
        if not createHistoryTbl[thirdIndex] then
            local cfgData = Guide_GuideIntroduction2023.GetData(thirdIndex)
            local historyButtonsChild = NGUITools.AddChild(self.HistoryButtonsTable.gameObject, self.HistoryButton)
            historyButtonsChild:SetActive(true)
            historyButtonsChild.transform:Find("Label"):GetComponent(typeof(UILabel)).text = cfgData.Name
            UIEventListener.Get(historyButtonsChild).onClick = DelegateFactory.VoidDelegate(function (_)
                self:OpenGuideMenu(thirdIndex)
            end)
            createHistoryTbl[thirdIndex] = true
        end
    end
    self.HistoryButtonsTable:Reposition()
    self.NoHistoryResult:SetActive(#self.historySearchData == 0)
end
----EndRegion 搜索及历史记录


--Region 等级指引


--End Region 等级指引
