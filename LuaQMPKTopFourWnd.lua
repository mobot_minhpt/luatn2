local QnSelectableButton=import "L10.UI.QnSelectableButton"

local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CButton = import "L10.UI.CButton"
local Extensions = import "Extensions"

LuaQMPKTopFourWnd=class()

RegistClassMember(LuaQMPKTopFourWnd, "TitleLabel") --标题
RegistClassMember(LuaQMPKTopFourWnd, "RankRoot") -- 第几名的root
RegistClassMember(LuaQMPKTopFourWnd, "RankLabel") -- 第几名的数值label

RegistClassMember(LuaQMPKTopFourWnd, "TeamMember1")
RegistClassMember(LuaQMPKTopFourWnd, "TeamMember2")
RegistClassMember(LuaQMPKTopFourWnd, "TeamMember3")
RegistClassMember(LuaQMPKTopFourWnd, "TeamMember4")
RegistClassMember(LuaQMPKTopFourWnd, "TeamMember5")
RegistClassMember(LuaQMPKTopFourWnd, "TeamMember6")
RegistClassMember(LuaQMPKTopFourWnd, "TeamMember7")
RegistClassMember(LuaQMPKTopFourWnd, "TeamMember8")

RegistClassMember(LuaQMPKTopFourWnd, "TeamNameLabel") -- xxx的战队
RegistClassMember(LuaQMPKTopFourWnd, "PreviousButton") -- 前一个队伍
RegistClassMember(LuaQMPKTopFourWnd, "NextButton") -- 后一个队伍


RegistClassMember(LuaQMPKTopFourWnd, "IsFinished") -- 全民争霸是否结束
RegistClassMember(LuaQMPKTopFourWnd, "CurrentIndex") -- 现在选中的index（1-4）
RegistClassMember(LuaQMPKTopFourWnd, "ResultLabel") -- 

RegistClassMember(LuaQMPKTopFourWnd, "Title") -- RPC返回的战队信息
RegistClassMember(LuaQMPKTopFourWnd, "ZhanDuiId") -- RPC返回的战队ID
RegistClassMember(LuaQMPKTopFourWnd, "MemberInfos") -- RPC返回的战队成员信息

function LuaQMPKTopFourWnd:Init()

	self:InitClassMember()

	self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("第%s届全民争霸赛"), Extensions.ToChinese(QuanMinPK_Setting.GetData().JieCi))
	self.TeamMember1.gameObject:SetActive(false)
	self.TeamMember2.gameObject:SetActive(false)
	self.TeamMember3.gameObject:SetActive(false)
	self.TeamMember4.gameObject:SetActive(false)
	self.TeamMember5.gameObject:SetActive(false)
	self.TeamMember6.gameObject:SetActive(false)
	self.TeamMember7.gameObject:SetActive(false)
	self.TeamMember8.gameObject:SetActive(false)

	local onPrevious = function (go)
		self:OnPreviousClicked(go)
	end
	CommonDefs.AddOnClickListener(self.PreviousButton.gameObject,DelegateFactory.Action_GameObject(onPrevious),false)

	local onNext = function (go)
		self:OnNextClicked(go)
	end
	CommonDefs.AddOnClickListener(self.NextButton.gameObject,DelegateFactory.Action_GameObject(onNext),false)
	
	self:CheckPreviousAndNext()
	self:DefaultChoose()
end

-- 初始化本类数据
function LuaQMPKTopFourWnd:InitClassMember()
	
	self.TitleLabel = self.transform:Find("Anchor/Title/TitleLabel"):GetComponent(typeof(UILabel))
	self.RankRoot = self.transform:Find("Anchor/Rank").gameObject
	self.RankLabel = self.transform:Find("Anchor/Rank/RankLabel"):GetComponent(typeof(UILabel))

	self.TeamMember1 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer1"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember2 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer2"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember3 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer3"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember4 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer4"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember5 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer5"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember6 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer6"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember7 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer7"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember8 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer8"):GetComponent(typeof(QnModelPreviewer))

	self.TeamNameLabel = self.transform:Find("Anchor/Selections/TeamNameLabel"):GetComponent(typeof(UILabel))
	
	self.PreviousButton = self.transform:Find("Anchor/Selections/PreviousButton"):GetComponent(typeof(CButton))
	self.NextButton = self.transform:Find("Anchor/Selections/NextButton"):GetComponent(typeof(CButton))
	self.ResultLabel = self.transform:Find("Anchor/Selections/ResultLabel"):GetComponent(typeof(UILabel))

	self.IsFinished = false
	self.CurrentIndex = 1
	self.Title = ""
	self.ZhanDuiId = nil
	self.MemberInfos = {}

end

-- 默认选项
function LuaQMPKTopFourWnd:DefaultChoose()
	self:ChooseTeam(1)
end

-- 选择某一个队伍
function LuaQMPKTopFourWnd:ChooseTeam(index)
	if index >= 1 and index <= 4 then
		Gac2Gas.QueryQmpkZongJueSaiOverView(index)
	end
end

-- 点击前一个队伍
function LuaQMPKTopFourWnd:OnPreviousClicked(go)
	if self.CurrentIndex <= 1 then return end
	self:ChooseTeam(self.CurrentIndex - 1)
end

-- 点击后一个队伍
function LuaQMPKTopFourWnd:OnNextClicked(go)
	if self.CurrentIndex >= 4 then return end
	self:ChooseTeam(self.CurrentIndex + 1)
end

function LuaQMPKTopFourWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateQMPKTopFourInfos", self, "UpdateQMPKTopFourInfos")

end

function LuaQMPKTopFourWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateQMPKTopFourInfos", self, "UpdateQMPKTopFourInfos")
end

-- 更新四强界面的显示
function LuaQMPKTopFourWnd:UpdateQMPKTopFourInfos(index, zhanduiId, title, memberInfos, isFinish)
	self.IsFinished = (isFinish ~= 0)
	self.CurrentIndex = index
	self.Title = title
	self.ZhanDuiId = zhanduiId
	self.MemberInfos = memberInfos

	self:UpdateTeamNameOrRank()
	-- 显示玩家的形象
	for i =1, 8 do
		local previewName = SafeStringFormat3("TeamMember%s", tostring(i))
		self:InitTeamMember(self[previewName], self.MemberInfos[i])
	end
	-- 处理按钮是否可用
	self:CheckPreviousAndNext()
end

-- 更新界面的战队名称或排行
function LuaQMPKTopFourWnd:UpdateTeamNameOrRank()
	if self.IsFinished then
		self.RankRoot:SetActive(true)
		self.RankLabel.text = tostring(self.CurrentIndex)
		self.ResultLabel.gameObject:SetActive(true)
	else
		self.RankRoot:SetActive(false)
	end
	self.TeamNameLabel.text = self.Title
	self.ResultLabel.text = self:GetResultText(self.CurrentIndex)
end

-- 更新玩家信息
function LuaQMPKTopFourWnd:InitTeamMember(previewer, info)
	if not info then
		previewer.gameObject:SetActive(false)
		return
	end
	previewer.gameObject:SetActive(true)

	local tf = previewer.transform
	local playerNameLabel = tf:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local serverNameLabel = tf:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
	playerNameLabel.text = info.memberName
	serverNameLabel.text = info.serverName
	previewer:PreviewFakeAppear(info.fakeAppearance)
end

-- 获得名次的中文显示
function LuaQMPKTopFourWnd:GetResultText(index)
	if self.IsFinished then
		if index == 1 then
			return LocalString.GetString("冠军")
		elseif index == 2 then
			return LocalString.GetString("亚军")
		elseif index == 3 then
			return LocalString.GetString("季军")
		elseif index == 4 then
			return LocalString.GetString("殿军")
		end
	end
	return ""
end

function LuaQMPKTopFourWnd:CheckPreviousAndNext()
	self.PreviousButton.Enabled = self.CurrentIndex > 1
	self.NextButton.Enabled = self.CurrentIndex < 4
end

-- 按下选择框
function LuaQMPKTopFourWnd:OnSelectTeam(go)
	local tf = go.transform
	local sButton = tf:GetComponent(typeof(QnSelectableButton))
	local index = sButton.UserData

	-- 确认选中按钮
	for i = 1, 4 do
		local selectionName = SafeStringFormat3("TeamSelection%s", tostring(i))
		local button = self[selectionName].transform:GetComponent(typeof(QnSelectableButton))
		button:SetSelected(self[selectionName] == go.gameObject)
	end

	Gac2Gas.QueryQmpkZongJueSaiOverView(index)
end

---------------------------------- RPC ----------------------------------


return LuaQMPKTopFourWnd


