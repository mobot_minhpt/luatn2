local QnTableView = import "L10.UI.QnTableView"
local CItemMgr=import "L10.Game.CItemMgr"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Item_Item=import "L10.Game.Item_Item"
local CUITexture=import "L10.UI.CUITexture"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

CLuaExchangeJieBanSkillItemWnd = class()
RegistClassMember(CLuaExchangeJieBanSkillItemWnd, "m_TableView")
RegistClassMember(CLuaExchangeJieBanSkillItemWnd, "m_SkillIds")
RegistClassMember(CLuaExchangeJieBanSkillItemWnd, "m_Button")
-- RegistClassMember(CLuaExchangeJieBanSkillItemWnd, "m_CountLabel")

function CLuaExchangeJieBanSkillItemWnd:Init()
    local countUpdate=FindChild(self.transform,"CountLabel"):GetComponent(typeof(CItemCountUpdate))
    countUpdate:UpdateCount()

    self.m_Button = FindChild(self.transform,"Button").gameObject
    -- CUICommonDef.SetActive(self.m_Button,false,true)

    UIEventListener.Get(self.m_Button).onClick=DelegateFactory.VoidDelegate(function(go)
        local bookItemTempId = self.m_SkillIds[self.m_TableView.currentSelectRow+1] or 0
        if bookItemTempId>0 then
            local msg = g_MessageMgr:FormatMessage("Exchange_LingShouJieBanSkill_Confirm",
                Item_Item.GetData(self.m_ItemTemplateId).Name,
                Item_Item.GetData(bookItemTempId).Name)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                --找青竹竹剪
                local ret, pos ,itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag,self.m_ItemTemplateId)
                if ret then
                    Gac2Gas.RequestExchangePartnerSkillBook(pos, itemId, bookItemTempId)
                else
                    --没有物品
                    g_MessageMgr:ShowMessage("Exchange_LingShouJieBanSkill_NoMat")
                end
            end), nil, nil, nil, false)
        else
            g_MessageMgr:ShowMessage("Exchange_LingShouJieBanSkill_NeedSelect")
        end
    end)

    local setting = LingShouPartner_Setting.GetData()
    self.m_ItemTemplateId = setting.NormalSkillBookBoxId
    local normalSkillBooks = setting.NormalSkillBookSet
    -- local professionalSkillBooks = setting.ProfessionalSkillBookSet

    self.m_SkillIds={}
    
    for i=1,normalSkillBooks.Length do
        table.insert( self.m_SkillIds,normalSkillBooks[i-1] )
    end

    self.m_TableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_SkillIds 
        end,
        function(item,row) 
            self:InitItem(item.transform,self.m_SkillIds[row+1]) 
        end
    )
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        -- CUICommonDef.SetActive(self.m_Button,true,true)
        local bookItemTempId = self.m_SkillIds[row+1] or 0
        CItemInfoMgr.ShowLinkItemTemplateInfo(bookItemTempId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
    self.m_TableView:SetSelectRow(0,true)
    self.m_TableView:ReloadData(true, false)
end

function CLuaExchangeJieBanSkillItemWnd:InitItem(transform,id)
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local data = Item_Item.GetData(id)
    icon:LoadMaterial(data.Icon)
    nameLabel.text=data.Name
end

-- function CLuaExchangeJieBanSkillItemWnd:OnEnable()

-- end

-- function CLuaExchangeJieBanSkillItemWnd:OnDisable()

-- end

-- function CLuaExchangeJieBanSkillItemWnd:UpdateCount()
--     local bindCount,notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(templateId)
--     self.m_CountLabel.text=tostring(bindCount+notBindCount)
-- end