local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaSanxingPipeiWnd = class()
RegistChildComponent(LuaSanxingPipeiWnd,"cancelBtn", GameObject)
RegistChildComponent(LuaSanxingPipeiWnd,"tipBtn", GameObject)
RegistChildComponent(LuaSanxingPipeiWnd,"timeLabel", UILabel)
RegistChildComponent(LuaSanxingPipeiWnd,"eTimeLabel", UILabel)
RegistChildComponent(LuaSanxingPipeiWnd,"pipeiNumLabel", UILabel)

RegistClassMember(LuaSanxingPipeiWnd, "m_Tick")
RegistClassMember(LuaSanxingPipeiWnd, "m_PipeiCount")

function LuaSanxingPipeiWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingPipeiWnd)
end

function LuaSanxingPipeiWnd:OnEnable()
	g_ScriptEvent:AddListener("SanxingBattlePipeiUpdate", self, "CalPipeiNum")
end

function LuaSanxingPipeiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SanxingBattlePipeiUpdate", self, "CalPipeiNum")
end

function LuaSanxingPipeiWnd:Init()
	local onCancelClick = function(go)
    Gac2Gas.RequestCancelSignUpSanXingBattle(100)
		self:Close()
	end
	CommonDefs.AddOnClickListener(self.cancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)

	local onTipClick = function(go)
		CUIManager.ShowUI(CLuaUIResources.SanxingTipWnd)
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

	self.m_Tick = RegisterTickWithDuration(function ()
		self:CalTime()
	end, 1000, 1000 * 90000)

	self:CalTotalTime()

	self:CalTime()

	Gac2Gas.QuerySanXingBattleFreeMatchPlayerNum()
end

function LuaSanxingPipeiWnd:CalTime()
	local restTime = math.ceil(CServerTimeMgr.Inst.timeStamp - LuaSanxingGamePlayMgr.matchTime)
	if restTime < 0 then
		restTime = 0
	end
	local min = math.floor(restTime/60)
	local sec = restTime - min * 60
	if sec < 10 then
		sec = '0' .. sec
	end
	self.timeLabel.text = min..':'..sec

	if not self.m_PipeiCount then
		self.m_PipeiCount = 0
	end
	self.m_PipeiCount = self.m_PipeiCount + 1
	if self.m_PipeiCount > 1 then
		self.m_PipeiCount = 0
		Gac2Gas.QuerySanXingBattleFreeMatchPlayerNum()
	end
end

function LuaSanxingPipeiWnd:CalPipeiNum()
	if LuaSanxingGamePlayMgr.pipeiNum and LuaSanxingGamePlayMgr.pipeiTotalNum then
		self.pipeiNumLabel.text = '[c][FFFE91]' .. LuaSanxingGamePlayMgr.pipeiNum .. '[-][/c]/' .. LuaSanxingGamePlayMgr.pipeiTotalNum
	end
end

function LuaSanxingPipeiWnd:CalTotalTime()
	if LuaSanxingGamePlayMgr.matchTotalTime then
		local min = math.floor(LuaSanxingGamePlayMgr.matchTotalTime/60)
		local sec = LuaSanxingGamePlayMgr.matchTotalTime - min * 60
		if sec < 10 then
			sec = '0' .. sec
		end
		self.eTimeLabel.text = min..':'..sec
	else
		self.eTimeLabel.text = ''
	end
end

function LuaSanxingPipeiWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end

return LuaSanxingPipeiWnd
