-- Auto Generated!!
local CGuildEnemyListItem = import "L10.UI.CGuildEnemyListItem"
local CGuildEnemyListView = import "L10.UI.CGuildEnemyListView"
local CGuildEnemyMgr = import "L10.Game.CGuildEnemyMgr"
local Object = import "System.Object"
CGuildEnemyListView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    if index < 0 or index >= CGuildEnemyMgr.Inst.DisplayList.Count then
        return nil
    end
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.itemTemplate, cellIdentifier)
    end
    CommonDefs.GetComponent_GameObject_Type(cell, typeof(CGuildEnemyListItem)):Init(CGuildEnemyMgr.Inst.DisplayList[index], index)

    return cell
end
CGuildEnemyListView.m_OnRowSelected_CS2LuaHook = function (this, index) 

    if index >= 0 and index < CGuildEnemyMgr.Inst.DisplayList.Count then
        if this.OnSelected ~= nil then
            GenericDelegateInvoke(this.OnSelected, Table2ArrayWithCount({CGuildEnemyMgr.Inst.DisplayList[index].guildId}, 1, MakeArrayClass(Object)))
        end
    end
end
CGuildEnemyListView.m_Init_CS2LuaHook = function (this) 
    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    this.tableView:LoadData(0, false)
end
