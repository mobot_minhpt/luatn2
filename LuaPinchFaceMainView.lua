local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local EnumGender = import "L10.Game.EnumGender"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local Animation = import "UnityEngine.Animation"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"

LuaPinchFaceMainView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceMainView, "TabBar1", "TabBar1", UITabBar)
RegistChildComponent(LuaPinchFaceMainView, "RadioBox2", "RadioBox2", QnRadioBox)
RegistChildComponent(LuaPinchFaceMainView, "RadioBox3", "RadioBox3", QnRadioBox)
RegistChildComponent(LuaPinchFaceMainView, "SubTemplate", "SubTemplate", GameObject)
RegistChildComponent(LuaPinchFaceMainView, "SubSubTemplate1", "SubSubTemplate1", GameObject)
RegistChildComponent(LuaPinchFaceMainView, "SubSubTemplate2", "SubSubTemplate2", GameObject)
RegistChildComponent(LuaPinchFaceMainView, "LibraryBtn", "LibraryBtn", CButton)
RegistChildComponent(LuaPinchFaceMainView, "ImportBtn", "ImportBtn", CButton)
RegistChildComponent(LuaPinchFaceMainView, "PhotoBtn", "PhotoBtn", CButton)
RegistChildComponent(LuaPinchFaceMainView, "RandomButton", "RandomButton", CButton)
RegistChildComponent(LuaPinchFaceMainView, "RedoButton", "RedoButton", QnButton)
RegistChildComponent(LuaPinchFaceMainView, "UndoBtn", "UndoBtn", QnButton)
RegistChildComponent(LuaPinchFaceMainView, "ShuFaButton", "ShuFaButton", CButton)
RegistChildComponent(LuaPinchFaceMainView, "ZhuShiButton", "ZhuShiButton", CButton)
RegistChildComponent(LuaPinchFaceMainView, "PinchFaceSliderControlView", "PinchFaceSliderControlView", CCommonLuaScript)
RegistChildComponent(LuaPinchFaceMainView, "UnLockItem", "UnLockItem", GameObject)
RegistChildComponent(LuaPinchFaceMainView, "SoundSettingBtn", "SoundSettingBtn", CButton)
--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFaceMainView, "m_OnSelectRadioBox3Action")
RegistClassMember(LuaPinchFaceMainView, "m_IsInit")
RegistClassMember(LuaPinchFaceMainView, "m_TabIconDataMap")
RegistClassMember(LuaPinchFaceMainView, "m_TabBar1Index")
RegistClassMember(LuaPinchFaceMainView, "m_TabBar2Index")
RegistClassMember(LuaPinchFaceMainView, "m_TabBar3Index")
RegistClassMember(LuaPinchFaceMainView, "m_TabBar1NameArr")
RegistClassMember(LuaPinchFaceMainView, "m_TabBar2NameArr")
RegistClassMember(LuaPinchFaceMainView, "m_FaceTabBarData")
RegistClassMember(LuaPinchFaceMainView, "m_FacialTabBarData")
RegistClassMember(LuaPinchFaceMainView, "m_HairId")
RegistClassMember(LuaPinchFaceMainView, "m_WaitLoadResourcesFinishedTick")
RegistClassMember(LuaPinchFaceMainView, "m_HasClickedBtn2")

function LuaPinchFaceMainView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LibraryBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLibraryBtnClick()
	end)


	
	UIEventListener.Get(self.ImportBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnImportBtnClick()
	end)


	
	UIEventListener.Get(self.PhotoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPhotoBtnClick()
	end)


	
	UIEventListener.Get(self.RandomButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRandomButtonClick()
	end)


	
	UIEventListener.Get(self.RedoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRedoButtonClick()
	end)


	
	UIEventListener.Get(self.UndoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUndoBtnClick()
	end)


	
	UIEventListener.Get(self.ShuFaButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShuFaButtonClick()
	end)


    --@endregion EventBind end

    self.TabBar1.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabBar1Change(go, index)
	end)

    self.RadioBox2.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectRadioBox2(btn, index)
    end)

    self.m_OnSelectRadioBox3Action = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectRadioBox3(btn, index)
    end)
    self.RadioBox3.OnSelect = self.m_OnSelectRadioBox3Action

    UIEventListener.Get(self.ZhuShiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuShiButtonClick()
	end)

    UIEventListener.Get(self.SoundSettingBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSoundSettingBtnClick()
	end)
end

function LuaPinchFaceMainView:Start()
    if not CFuxiFaceDNAMgr.m_IsOpenFuxiFacialDNA then
        self.PhotoBtn.gameObject:SetActive(false)
    end
    if not CFuxiFaceDNAMgr.m_IsOpenFuxiRandomFacialDNA then
        self.RandomButton.gameObject:SetActive(false)
    end
    if LuaCloudGameFaceMgr:IsCloudGameFace() then
        self.LibraryBtn.gameObject:SetActive(false)
    end
    self.SubTemplate.gameObject:SetActive(false)
    self.SubSubTemplate1.gameObject:SetActive(false)
    self.SubSubTemplate2.gameObject:SetActive(false)
    self.UnLockItem.gameObject:SetActive(false)
    self:InitTabIconMap()
    self:InitFaceTabBarData()
    self.m_IsInit = true
    LuaPinchFaceMgr:InitCommandMode()
    self:ShowView()
end

function LuaPinchFaceMainView:ShowView()
    self:InitFacialTabBarData()
    self.ShuFaButton.Selected = false
    LuaPinchFaceMgr:SetFashion(LuaPinchFaceMgr.m_FashionData, EnumPinchFaceAniState.PreviewStand01, LuaPinchFaceMgr.m_IsShowLuoZhuangHair)
    self:OnSetPinchFaceIKLookAtTo()
    LuaPinchFaceMgr:SetEnableIk(true)
    LuaPinchFaceMgr:RotateRoleModel(180)
    self:ShowUndoAndRedoButton()
    self:CancelWaitTick()
	self.m_WaitLoadResourcesFinishedTick = RegisterTick(function()
		if LuaPinchFaceMgr.m_RO and LuaPinchFaceMgr.m_RO.FaceDnaData and LuaPinchFaceMgr.m_RO.FacialPartData then
            self:CancelWaitTick()
            if self.m_IsInit then
                self.TabBar1:ChangeTab(0, false)
            end
		end
	end,100)

    if not self.m_HasClickedBtn2 then
		local btn = self.TabBar1:GetTabGoByIndex(1)
		local fx = btn.transform:Find("Normal_vfx")
		fx.gameObject:SetActive(false)
		fx.gameObject:SetActive(true)
	end
end

function LuaPinchFaceMainView:CancelWaitTick()
	if self.m_WaitLoadResourcesFinishedTick then
		UnRegisterTick(self.m_WaitLoadResourcesFinishedTick)
		self.m_WaitLoadResourcesFinishedTick = nil
	end
end

function LuaPinchFaceMainView:ShowUndoAndRedoButton()
    self.UndoBtn.gameObject:SetActive(LuaPinchFaceMgr.m_IsEnableCommandMode and LuaCommandMgr:HasUndoCommand())
    self.RedoButton.gameObject:SetActive(LuaPinchFaceMgr.m_IsEnableCommandMode and LuaCommandMgr:HasRedoCommand())
end

function LuaPinchFaceMainView:InitTabIconMap()
    self.m_TabIconDataMap = {}
    PinchFace_TabIcon.ForeachKey(function (k)
        self.m_TabIconDataMap[k] = PinchFace_TabIcon.GetData(k)
    end)
end

function LuaPinchFaceMainView:InitFaceTabBarData()
    local dataList = {}
    PinchFace_FaceTabBarData.ForeachKey(function (k)
        local v = PinchFace_FaceTabBarData.GetData(k)
        table.insert(dataList, v)
    end)
    table.sort(dataList, function (a, b)
        return a.SortIndex < b.SortIndex
    end)
    local tab2List = {}
    self.m_TabBar1NameArr = {}
    for _, v in pairs(dataList) do
        if not tab2List[v.Tab1] then
            tab2List[v.Tab1] = {}
            table.insert(self.m_TabBar1NameArr, v.Tab1)
        end
        table.insert(tab2List[v.Tab1], v)
    end
    self.m_FaceTabBarData = tab2List
end

function LuaPinchFaceMainView:InitFacialTabBarData()
    self.m_FacialTabBarData = {}
    self.m_TabBar2NameArr = {}
    local facialTabBarDataArr = {} 
    PinchFace_FacialTabBarData.ForeachKey(function (k)
        local v = PinchFace_FacialTabBarData.GetData(k)
        table.insert(facialTabBarDataArr, v)
    end)
    table.sort(facialTabBarDataArr,function (a,b)
        if a.SortIndex == b.SortIndex then
            return a.Id < b.Id
        else
            return a.SortIndex < b.SortIndex
        end
    end)
    for index = 1, #facialTabBarDataArr do
        local v = facialTabBarDataArr[index]
        local k = v.Id
        local tabName = self:GetFacialTabName(v)
        if tabName then
            local list = {}
            local hasValidStyleData = false
            if v.Styles then
                for i = 0, v.Styles.Length - 1 do
                    local id = v.Styles[i]
                    local styleData = PinchFace_FacialStyle.GetData(id)
                    if styleData then
                        local canUseStyle = LuaPinchFaceMgr:CheckFacialStyleIsUseful(k,id)
                        if canUseStyle then
                            table.insert(list, styleData)
                            if not hasValidStyleData and not self:CheckIsEmptyStyleData(styleData) then
                                hasValidStyleData = true
                            end
                        end
                    end
                end
            end
            if hasValidStyleData then
                table.insert(self.m_TabBar2NameArr, tabName)
                local path = LuaPinchFaceMgr:GetCurPresetDataStylePath(k)
                local defaultData = {Id = 0, Icon ="UI/Texture/Facial/Material/facial_reset.mat", Path = path, MainTexColor = LuaPinchFaceMgr.m_FacialStylePath2MainTexColor[path]}
                table.insert(list, 1, defaultData)
                self.m_FacialTabBarData[tabName] = {styleDatalist = list, key = k}
            end
        end
    end
    self:InitFacialHairData()
end

function LuaPinchFaceMainView:InitFacialHairData()
    if not LuaPinchFaceMgr.m_CurEnumGender then return end
    table.insert(self.m_TabBar2NameArr, 1, LocalString.GetString("发型"))
    local hairStyleDataList = {}
    local defaultID = LuaPinchFaceMgr:GetDefaultHairId()
    table.insert(hairStyleDataList, 1,{
        ID = defaultID,
        Icon ="UI/Texture/Facial/Material/facial_reset.mat",
    })
    PinchFace_Hair.ForeachKey(function(k)
        local data = PinchFace_Hair.GetData(k)
        if defaultID ~= data.ID and not cs_string.IsNullOrEmpty(data.Icon)  then
            if data.GenderLimit == 0 or (LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male and data.GenderLimit == 1) or (LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Female and data.GenderLimit == 2) then
                table.insert(hairStyleDataList, data)
            end
        end
    end)
    self.m_FacialTabBarData[LocalString.GetString("发型")] = {styleDatalist = hairStyleDataList, key = -1}
end

function LuaPinchFaceMainView:GetFacialTabName(facialTabBarData)
    local tabName = nil
    if facialTabBarData then
        if facialTabBarData.Id == 4 or facialTabBarData.Id == 5 then
            if facialTabBarData.Id == 4 then
                tabName = LocalString.GetString("虹膜")
            end
        else
            tabName = facialTabBarData.SliderName
        end
    end
    return tabName
end

function LuaPinchFaceMainView:InitRadioBox2(tabNameArr)
    Extensions.RemoveAllChildren(self.RadioBox2.m_Table.transform)
    if not tabNameArr then return end
    for _, tabName in pairs(tabNameArr) do
        local data = self.m_TabIconDataMap[tabName]
        local obj = NGUITools.AddChild(self.RadioBox2.m_Table.gameObject, self.SubTemplate.gameObject)
        local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
        local normal = obj.transform:Find("Normal"):GetComponent(typeof(UISprite))
        local highlight = obj.transform:Find("Highlight"):GetComponent(typeof(UISprite))
        label.text = self.m_TabBar1Index == 1 and tabName or ""
        if data then
            normal.spriteName = data.NormalIcon
            highlight.spriteName = data.HighlightIcon
        end
        obj:SetActive(true)
        local btn = obj:GetComponent(typeof(QnSelectableButton))
        btn.OnButtonSelected = DelegateFactory.Action_bool(function (isSelected)
            normal.gameObject:SetActive(not isSelected)
        end)
    end
    self.RadioBox2.m_Table:Reposition()
    self.RadioBox2.gameObject:GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
    self.RadioBox2.Inited = false
    self.RadioBox2.m_RadioButtons = nil
    self.RadioBox2:Awake()
end

function LuaPinchFaceMainView:InitFaceRadioBox3()
    Extensions.RemoveAllChildren(self.RadioBox3.m_Table.transform)
    local tabName = self.m_TabBar1NameArr[self.m_TabBar2Index + 1]
    local list = self.m_FaceTabBarData[tabName]
    if list then
        for i, data in pairs(list) do
            local obj = NGUITools.AddChild(self.RadioBox3.m_Table.gameObject, self.SubSubTemplate1.gameObject)
            local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = data.Tab2
            obj:SetActive(true)
            obj.name = data.Id
        end
    end
    self.RadioBox3.m_Table:Reposition()
    self.RadioBox3.Inited = false
    self.RadioBox3.m_RadioButtons = nil
    self.RadioBox3.OnSelect = self.m_OnSelectRadioBox3Action
    self.RadioBox3:Awake()
end

function LuaPinchFaceMainView:InitFacialRadioBox3()
    Extensions.RemoveAllChildren(self.RadioBox3.m_Table.transform)
    local tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
    local t = self.m_FacialTabBarData[tabName]
    if t then
        for i, data in pairs(t.styleDatalist) do
            local obj = NGUITools.AddChild(self.RadioBox3.m_Table.gameObject, self.SubSubTemplate2.gameObject)
            local tex = obj.transform:Find("Texture"):GetComponent(typeof(CUITexture))
            tex:LoadMaterial(data.Icon)
            obj:SetActive(true)
            obj.name = data.Id
        end
    end
    self.RadioBox3.Inited = false
    self.RadioBox3.m_RadioButtons = nil
    self.RadioBox3.OnSelect = nil
    self.RadioBox3:Awake()
    self.RadioBox3.OnSelect = self.m_OnSelectRadioBox3Action
    self.RadioBox3.gameObject:GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
    self.RadioBox3.m_Table:Reposition()
    self:SetFacialRadioBox3DefaultIndex()
end

function LuaPinchFaceMainView:SetFacialRadioBox3DefaultIndex()
    local styleData, defaultIndex = self:GetCurFacialStyleDataAndIndex()
    local triggerCallback = true
    local tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
    self:InitFacialHairRightView()
    if (LuaPinchFaceMgr.m_HairId == 9 or LuaPinchFaceMgr.m_IsShowLuoZhuangHair) and tabName == LocalString.GetString("发型") then
        triggerCallback = false
        self.m_TabBar3Index = defaultIndex - 1
    end
    self.RadioBox3:ChangeTo(defaultIndex - 1, triggerCallback)
end

function LuaPinchFaceMainView:InitFaceRightView()
    local tab1Name = self.m_TabBar1NameArr[self.m_TabBar2Index+ 1]
    local list = self.m_FaceTabBarData[tab1Name]
    if list then
        local faceTabBarData = list[self.m_TabBar3Index + 1]
        self.PinchFaceSliderControlView:InitFaceSliderView(faceTabBarData)
    end
end

function LuaPinchFaceMainView:InitFacialRightView()
    local tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
    local data = self.m_FacialTabBarData[tabName]
    local key = data.key
    local facialTabBarArr = nil
    local styleData, defaultIndex = self:GetCurFacialStyleDataAndIndex(tabName)
    if self:CheckIsEmptyStyleData(styleData) then
        facialTabBarArr = nil
    elseif styleData and styleData.Bar and styleData.Bar.Length > 0 then
        facialTabBarArr = styleData.Bar
    else
        local tabBarData = PinchFace_FacialTabBarData.GetData(key)
        facialTabBarArr = tabBarData and tabBarData.Bar or nil
    end
    self.UnLockItem.gameObject:SetActive(false)
    if LuaPinchFaceMgr.m_IsInModifyMode and styleData.SN and styleData.SN > 0 then
        self.PinchFaceSliderControlView:InitFacialSliderView(key, nil, styleData)
        self:InitUnLockItem(styleData)
        return
    end
    self.PinchFaceSliderControlView:InitFacialSliderView(key, facialTabBarArr, styleData)
end

function LuaPinchFaceMainView:SelectFacialPartStyle()
    local tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
    local data = self.m_FacialTabBarData[tabName]
    local key = data.key
    local styleData = data.styleDatalist[self.m_TabBar3Index + 1]
    LuaPinchFaceMgr:SelectFacialPartStyle(key, styleData)
end

function LuaPinchFaceMainView:InitFacialHairRightView()
    self.PinchFaceSliderControlView:InitHairSliderView()
end

function LuaPinchFaceMainView:SelectCurHair()
    local tabName = LocalString.GetString("发型")
    local data = self.m_FacialTabBarData[tabName]
    local styleData = data.styleDatalist[self.m_TabBar3Index + 1]
    if LuaPinchFaceMgr.m_HairId == styleData.ID  then
        return
    end
    if LuaPinchFaceMgr.m_HairId == 9 then
        g_MessageMgr:ShowMessage("PinchFace_ShowHairStyle")
    end
    self.ShuFaButton.Selected = false
    LuaPinchFaceMgr:SelectHair(styleData.ID, true, true)
end

function LuaPinchFaceMainView:InitUnLockItem(styleData)
    if styleData.UnlockItem.Length == 2 then
        self.UnLockItem.gameObject:SetActive(true)
        local itemId, num = styleData.UnlockItem[0], styleData.UnlockItem[1]
        local unLockItemTopLabel = self.UnLockItem.transform:Find("UnLockItemTopLabel"):GetComponent(typeof(UILabel))
        local itemCell = self.UnLockItem.transform:Find("ItemCell")
        local iconTexture = self.UnLockItem.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
        local consumeLabel = self.UnLockItem.transform:Find("ItemCell/ConsumeLabel"):GetComponent(typeof(UILabel))
        local button = self.UnLockItem.transform:Find("Button")
        local disableIcon = self.UnLockItem.transform:Find("ItemCell/DisableIcon")
        unLockItemTopLabel.text = SafeStringFormat3(LocalString.GetString("解锁该妆容需%d个点妆膏"), num)
        local itemData = Item_Item.GetData(itemId)
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
        iconTexture:LoadMaterial(itemData.Icon)
        disableIcon.gameObject:SetActive(count < num)
        consumeLabel.text = count < num and SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", count, num) or
        SafeStringFormat3("%d/%d", count, num)
        UIEventListener.Get(itemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            if count < num then
                CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, iconTexture.transform, CTooltipAlignType.Left)
            else
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
            end
        end)
        UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            Gac2Gas:UnlockFacialStyle(styleData.SN)
        end)
    end
end

--TODO 
function LuaPinchFaceMainView:CheckIsEmptyStyleData(styleData)
    return not styleData or (styleData.Path and string.find(styleData.Path, "empty.style.asset"))
end

function LuaPinchFaceMainView:GetCurFacialStyleDataAndIndex(tabName)
    if tabName == nil then
        tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
    end
    local data = self.m_FacialTabBarData[tabName]
    if data then
        local defaultIndex = 0
        local key = data.key
        local styleDatalist = data.styleDatalist
        local styleId = LuaPinchFaceMgr:GetPartStyleId(key)
        if tabName == LocalString.GetString("发型") then
            styleId = LuaPinchFaceMgr.m_HairId == 9 and self.m_HairId or LuaPinchFaceMgr.m_HairId
            -- if not LuaPinchFaceMgr.m_IsShowLuoZhuangHair then
            --     styleId = -1
            -- end
        end
        local presetPath = LuaPinchFaceMgr:GetCurPresetDataStylePath(key)
        for index, styleData in pairs(styleDatalist) do
            if styleData.Id == styleId or styleData.ID == styleId then
                defaultIndex = index
            end
            if presetPath then
                presetPath = string.lower(presetPath)
                if styleData.Path and string.find(presetPath, string.lower(styleData.Path)) then
                    defaultIndex = index
                end
            end
        end
        local styleData = styleDatalist[defaultIndex]
        return styleData, defaultIndex 
    end
end

function LuaPinchFaceMainView:ResetHair()
    if LuaPinchFaceMgr.m_HairId == 9 then
        LuaPinchFaceMgr.m_HairId = self.m_HairId
        if not LuaPinchFaceMgr.m_IsShowLuoZhuangHair then
            self.m_HairId = LuaPinchFaceMgr:GetPreselectionHairId(LuaPinchFaceMgr.m_PreselectionIndex)
            LuaPinchFaceMgr.m_HairId = self.m_HairId
            LuaPinchFaceMgr:SetFashion(LuaPinchFaceMgr.m_FashionData, EnumPinchFaceAniState.PlayStand01, false)
            return
        end
        LuaPinchFaceMgr:SelectHair(self.m_HairId, false)
    end
end
--@region UIEvent
function LuaPinchFaceMainView:OnTabBar1Change(go, index)
    if index == 1 and not self.m_HasClickedBtn2 then
		self.m_HasClickedBtn2 = true
        local btn = self.TabBar1:GetTabGoByIndex(1)
		local fx = btn.transform:Find("Normal_vfx")
		fx.gameObject:SetActive(false)
	end
    self.m_TabBar1Index = index
    self.TabBar1.tabButtons[0].transform:Find("Normal").gameObject:SetActive(index ~= 0)
    self.TabBar1.tabButtons[0].transform:Find("Highlight").gameObject:SetActive(index == 0)
    self.TabBar1.tabButtons[1].transform:Find("Normal").gameObject:SetActive(index ~= 1)
    self.TabBar1.tabButtons[1].transform:Find("Highlight").gameObject:SetActive(index == 1)
    self:InitRadioBox2(index == 0 and self.m_TabBar1NameArr or self.m_TabBar2NameArr)
    if index == 0 then
        LuaPinchFaceMgr:NewShowUIFacialEffectMat()
    else
        LuaPinchFaceMgr:RemoveUIFacialEffectMat()
    end
    local ani = self.RadioBox2.transform.parent:GetComponent(typeof(Animation))
    ani:Stop()
    ani:Play()
end

function LuaPinchFaceMainView:OnSelectRadioBox2(btn, index)
    self.m_TabBar2Index = index
    LuaPinchFaceMgr:SetEnableIk(true)
    if self.m_TabBar1Index == 0 then
        self:InitFaceRadioBox3()
    elseif self.m_TabBar1Index == 1 then
        self:InitFacialRadioBox3()
    end
end

function LuaPinchFaceMainView:OnSelectRadioBox3(btn, index)
    self.m_TabBar3Index = index
    local showCameraIndex = -1
    LuaPinchFaceMgr:SetEnableIk(true)
    if self.m_TabBar1Index == 0 then
        self:InitFaceRightView()
        local tab1Name = self.m_TabBar1NameArr[self.m_TabBar2Index + 1]
        local list = self.m_FaceTabBarData[tab1Name]
        if list then
            local data = list[self.m_TabBar3Index + 1]
            LuaPinchFaceMgr:NewShowUIFacialEffectMat(data)
            LuaPinchFaceMgr:SetEnableIk(data.CloseIK and data.CloseIK <= 0)
            LuaPinchFaceMgr:SetFaceTabDefaultAngle(data)
            showCameraIndex = (data.Tab1 == LocalString.GetString("脸型") or data.Tab1 == LocalString.GetString("耳朵")) and 2 or 3
        end
    elseif self.m_TabBar1Index == 1 then
        local tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
        if tabName == LocalString.GetString("发型") then
            self:SelectCurHair()
            self:InitFacialHairRightView()
            showCameraIndex = 2
        else
            self:SelectFacialPartStyle()
            self:InitFacialRightView()
            showCameraIndex = 3
        end      
    end
    if showCameraIndex > 0 and CPinchFaceCinemachineCtrlMgr.Inst then
        CPinchFaceCinemachineCtrlMgr.Inst:ShowCam(showCameraIndex)
    end
end

function LuaPinchFaceMainView:OnLibraryBtnClick()
    CUIManager.ShowUI(CLuaUIResources.PinchFaceHubWnd)
end

function LuaPinchFaceMainView:OnImportBtnClick()
    LuaPinchFaceMgr:ImportData()
end

function LuaPinchFaceMainView:OnPhotoBtnClick()
    CUIManager.ShowUI(CLuaUIResources.PinchFaceFuxiWnd)
end

function LuaPinchFaceMainView:OnRandomButtonClick()
    local curCommandInfo = LuaPinchFaceMgr.m_IsEnableCommandMode and LuaCommandMgr:GetCurCommandInfo() or nil
    if curCommandInfo and curCommandInfo == "PinchFaceImportData" then
        LuaPinchFaceMgr:RequestAndApplyFuxiRandomData()
        return
    end
    local msg = g_MessageMgr:FormatMessage("Pinchface_Random_MakeSure")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
        LuaPinchFaceMgr:RequestAndApplyFuxiRandomData()
    end), DelegateFactory.Action(function()

    end), nil, nil, false)
end


function LuaPinchFaceMainView:OnRedoButtonClick()
    LuaCommandMgr:Redo()
end

function LuaPinchFaceMainView:OnUndoBtnClick()
    LuaCommandMgr:Undo()
end

function LuaPinchFaceMainView:OnShuFaButtonClick()
    self.ShuFaButton.Selected = not self.ShuFaButton.Selected
    if self.ShuFaButton.Selected then
        self.m_HairId = LuaPinchFaceMgr.m_HairId
    end
    LuaPinchFaceMgr:SelectHair(self.ShuFaButton.Selected and 9 or self.m_HairId, true)
end

function LuaPinchFaceMainView:OnZhuShiButtonClick()
	self.ZhuShiButton.Selected = not self.ZhuShiButton.Selected
    LuaPinchFaceMgr:SetIKLookAtTo(self.ZhuShiButton.Selected)
end

function LuaPinchFaceMainView:OnSoundSettingBtnClick()
    self.SoundSettingBtn.Selected = true
	LuaMiniVolumeSettingsMgr:OpenWnd(self.SoundSettingBtn.gameObject, self.SoundSettingBtn.transform.position, UIWidget.Pivot.Top, function ()
		self.SoundSettingBtn.Selected = false
	end)
end

--@endregion UIEvent

function LuaPinchFaceMainView:OnEnable()
    g_ScriptEvent:AddListener("OnCommandUpdate", self, "OnCommandUpdate")
    g_ScriptEvent:AddListener("OnChangeFacialPartStyle", self, "OnChangeFacialPartStyle")
    g_ScriptEvent:AddListener("OnSelectPinchFaceHair", self, "OnSelectPinchFaceHair")
    g_ScriptEvent:AddListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
    g_ScriptEvent:AddListener("OnCachePresetFacialStylePaths", self, "OnCachePresetFacialStylePaths")
    g_ScriptEvent:AddListener("OnUpdatePinchFaceMainView", self, "OnUpdatePinchFaceMainView")
    self:ShowView()
end

function LuaPinchFaceMainView:OnDisable()
    g_ScriptEvent:RemoveListener("OnCommandUpdate", self, "OnCommandUpdate")
    g_ScriptEvent:RemoveListener("OnChangeFacialPartStyle", self, "OnChangeFacialPartStyle")
    g_ScriptEvent:RemoveListener("OnSelectPinchFaceHair", self, "OnSelectPinchFaceHair")
    g_ScriptEvent:RemoveListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
    g_ScriptEvent:RemoveListener("OnCachePresetFacialStylePaths", self, "OnCachePresetFacialStylePaths")
    g_ScriptEvent:RemoveListener("OnUpdatePinchFaceMainView", self, "OnUpdatePinchFaceMainView")
    self:ResetHair()
    self:CancelWaitTick()
    LuaPinchFaceMgr:SetEnableIk(true)
end

function LuaPinchFaceMainView:OnUpdatePinchFaceMainView()
    if self.m_TabBar1Index == 0 then
        self:InitFaceRightView()
    elseif self.m_TabBar1Index == 1 then
        self:OnSelectRadioBox2(nil, self.m_TabBar2Index)
    end
end

function LuaPinchFaceMainView:OnCachePresetFacialStylePaths()
    self:ShowView()
end

function LuaPinchFaceMainView:OnSetPinchFaceIKLookAtTo()
    self.ZhuShiButton.Selected = LuaPinchFaceMgr.m_IsUseIKLookAt
    self.ZhuShiButton.gameObject:SetActive(LuaPinchFaceMgr.m_IsEnableIK)
end

function LuaPinchFaceMainView:OnChangeFacialPartStyle(partIndex2styleId)
    if self.m_TabBar1Index == 1 then
        local tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
        local data = self.m_FacialTabBarData[tabName]
        if data then
            local styleId = partIndex2styleId[data.key]
            if styleId and data.styleDatalist then
                local defaultIndex = 0
                for index, styleData in pairs(data.styleDatalist) do
                    if styleData.Id == styleId then
                        defaultIndex = index - 1
                        if self.RadioBox3.CurrentSelectIndex ~= defaultIndex then
                            self.RadioBox3:ChangeTo(defaultIndex, true)
                        else
                            self:InitFacialRightView()
                        end
                        break
                    end
                end
            end
        end
    end
end

function LuaPinchFaceMainView:OnSelectPinchFaceHair(ID)
    self.ShuFaButton.Selected = ID == 9
    if self.m_TabBar1Index == 1 then
        local tabName = self.m_TabBar2NameArr[self.m_TabBar2Index + 1]
        if tabName == LocalString.GetString("发型") then
            local data = self.m_FacialTabBarData[tabName]
            local defaultIndex = 0
            for index, hairData in pairs(data.styleDatalist) do
                if hairData.ID == ID then
                    defaultIndex = index - 1
                    self.RadioBox3:ChangeTo(defaultIndex, false)
                    break
                end
            end
        end
    end
end

function LuaPinchFaceMainView:OnCommandUpdate()
    self:ShowUndoAndRedoButton()
end