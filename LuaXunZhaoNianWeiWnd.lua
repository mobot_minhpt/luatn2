local DateTime = import "System.DateTime"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local QnSelectableButton=import "L10.UI.QnSelectableButton"
local UILabel = import "UILabel"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local GameObject = import "UnityEngine.GameObject"


local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaXunZhaoNianWeiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaXunZhaoNianWeiWnd, "StatusLabel", "StatusLabel", UILabel)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "AcceptButton", "AcceptButton", GameObject)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "RewardButton", "RewardButton", GameObject)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "TaskStatusLabel", "TaskStatusLabel", UILabel)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "Rewards", "Rewards", GameObject)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "RewardItem", "RewardItem", GameObject)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "TabGrid", "TabGrid", GameObject)
RegistChildComponent(LuaXunZhaoNianWeiWnd, "TabItem", "TabItem", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaXunZhaoNianWeiWnd,"m_Keys")
RegistClassMember(LuaXunZhaoNianWeiWnd,"m_RewardStatus")
RegistClassMember(LuaXunZhaoNianWeiWnd,"m_TabCmps")
RegistClassMember(LuaXunZhaoNianWeiWnd,"m_SelectedTabIndex")
RegistClassMember(LuaXunZhaoNianWeiWnd,"m_Decos")
RegistClassMember(LuaXunZhaoNianWeiWnd,"m_Background")

LuaXunZhaoNianWeiWnd.s_FinishTaskIdx = 0
function LuaXunZhaoNianWeiWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AcceptButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAcceptButtonClick()
	end)


	
	UIEventListener.Get(self.RewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


    --@endregion EventBind end

    self.m_Background = self.transform:Find("Background"):GetComponent(typeof(UITexture))
    self.AcceptButton:SetActive(false)
    self.RewardButton:SetActive(false)

    Gac2Gas.XZNW_QueryReward()

    -- local openTime = DateTime(2022,1,25+i-1)--
    self.RewardItem:SetActive(false)
    self.TabItem:SetActive(false)
    self.StatusLabel.text = nil
    self.ContentLabel.text = nil

    self.m_SelectedTabIndex = 1

    self.m_Decos = {}
    local tf = self.transform:Find("Decoration")
    for i=1,8 do
        local child = tf:Find(tostring(i)).gameObject
        table.insert( self.m_Decos,child )
    end
    self.m_RewardStatus = {}
end

function LuaXunZhaoNianWeiWnd:Init()
    self.m_Keys = {}
    ChunJie_XunZhaoNianWei2022.ForeachKey(function(key)
        table.insert( self.m_Keys,key )
    end)
    self:InitTabs()
end
function LuaXunZhaoNianWeiWnd:InitTabs()
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local time = DateTime(now.Year,now.Month,now.Day)--今天
    local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or {}


    -- local texture_lock = "UI/Texture/Transparent/Material/xunzhaonianweiwnd_fenye_weijiesuo.mat"
    -- local texture_unlock = "UI/Texture/Transparent/Material/xunzhaonianweiwnd_fenye_yijiesuo.mat"
    -- local texture_highlight = "UI/Texture/Transparent/Material/xunzhaonianweiwnd_fenye_highlight.mat"

    local defaultTabIndex = 1
    if LuaXunZhaoNianWeiWnd.s_FinishTaskIdx>0 then
        defaultTabIndex = LuaXunZhaoNianWeiWnd.s_FinishTaskIdx
    end
    self.m_TabCmps = {}
    for i,key in ipairs(self.m_Keys) do
        local item = NGUITools.AddChild(self.TabGrid,self.TabItem)
        item:SetActive(true)
        local cmp = item:GetComponent(typeof(QnSelectableButton))
        table.insert(self.m_TabCmps,cmp)

        local tf = item.transform
        local bg = tf:GetComponent(typeof(CUITexture))

        local label = tf:Find("Label"):GetComponent(typeof(UILabel))
        local name = ChunJie_XunZhaoNianWei2022.GetData(self.m_Keys[i]).Date
        label.text = name
        local alert = tf:Find("Alert").gameObject
        alert:SetActive(false)

        -- local key = self.m_Keys[row+1]
        local taskId = ChunJie_XunZhaoNianWei2022.GetData(key).Task2
        local finished = taskProp:IsMainPlayerTaskFinished(taskId)
        local openTime =self:GetOpenTime(i)
        if DateTime.Compare(time,openTime)>=0 then
            label.color = NGUIText.ParseColor24("FFF885", 0)
            --看看前一天有没有完成
            local canShow = true
            if i>1 then
                local preTaskId = ChunJie_XunZhaoNianWei2022.GetData(self.m_Keys[i-1]).Task2
                canShow = taskProp:IsMainPlayerTaskFinished(preTaskId)
            end
            if canShow then
                if LuaXunZhaoNianWeiWnd.s_FinishTaskIdx==0 then
                    defaultTabIndex = i
                end
                --时间到了，并且可以选择
                cmp.OnClick = DelegateFactory.Action_QnButton(function(btn)
                    self:OnClickTab(btn)
                end)
            else
                --时间到了，但是前置任务没有完成
                cmp.OnClick = DelegateFactory.Action_QnButton(function(btn)
                    g_MessageMgr:ShowMessage("XunZhaoNianWei_NeedPreTask")
                end)
            end
            -- bg:LoadMaterial(texture_unlock)
            if not finished then
                self.m_Decos[i]:SetActive(false)
            else
                --完成
                self.m_Decos[i]:SetActive(true)
            end
        else
            self.m_Decos[i]:SetActive(false)
            label.color = NGUIText.ParseColor24("808080", 0)

            --时间未到
            cmp.OnClick = DelegateFactory.Action_QnButton(function(btn)
                g_MessageMgr:ShowMessage("XunZhaoNianWei_NoEntry")
            end)
        end
    end
    self.TabGrid:GetComponent(typeof(UIGrid)):Reposition()
    self:OnClickTab(self.m_TabCmps[defaultTabIndex])
    
end
function LuaXunZhaoNianWeiWnd:GetOpenTime(idx)
    local openTime = DateTime(2023,1,15+idx-1)
    return openTime
end


function LuaXunZhaoNianWeiWnd:OnClickTab(btn)
    local texture_lock = "UI/Texture/Transparent/Material/xunzhaonianweiwnd_fenye_weijiesuo.mat"
    local texture_unlock = "UI/Texture/Transparent/Material/xunzhaonianweiwnd_fenye_yijiesuo.mat"
    local texture_highlight = "UI/Texture/Transparent/Material/xunzhaonianweiwnd_fenye_highlight.mat"

    local now = CServerTimeMgr.Inst:GetZone8Time()
    local time = DateTime(now.Year,now.Month,now.Day)--今天
    local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or {}

    local index = 0
    for j,v in ipairs(self.m_TabCmps) do
        local bg = v.transform:GetComponent(typeof(CUITexture))
        local openTime = self:GetOpenTime(j)

        if v==btn then
            index = j
            self.m_SelectedTabIndex = j
            v:SetSelected(true,false)
            bg:LoadMaterial(texture_highlight)
        else
            v:SetSelected(false,false)

            --到时间了
            if DateTime.Compare(time,openTime)>=0 then--
                local canShow = true
                if j>1 then
                    local preTaskId = ChunJie_XunZhaoNianWei2022.GetData(self.m_Keys[j-1]).Task2
                    canShow = taskProp:IsMainPlayerTaskFinished(preTaskId)
                end
                if canShow then
                    bg:LoadMaterial(texture_unlock)--已解锁
                else
                    bg:LoadMaterial(texture_lock)--未解锁
                end
            else
                bg:LoadMaterial(texture_lock)--未解锁
            end

        end
    end

    self:OnSelectAtRow(index)
end
--@region UIEvent

function LuaXunZhaoNianWeiWnd:OnAcceptButtonClick()
    local key = self.m_Keys[self.m_SelectedTabIndex]
    local taskId = ChunJie_XunZhaoNianWei2022.GetData(key).Task1

    Gac2Gas.RequestAutoAcceptTask(taskId)
end

function LuaXunZhaoNianWeiWnd:OnRewardButtonClick()
    local key = self.m_Keys[self.m_SelectedTabIndex]
    Gac2Gas.XZNW_RequestReward(key)
end


function LuaXunZhaoNianWeiWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("XunZhaoNianWei_Tip")
end


--@endregion UIEvent
local tweener = nil
function LuaXunZhaoNianWeiWnd:OnDestroy()
    LuaXunZhaoNianWeiWnd.s_FinishTaskIdx = 0
    LuaTweenUtils.Kill(tweener,false)
end

function LuaXunZhaoNianWeiWnd:OnSelectAtRow(index)
    --将图形显示出来
    if LuaXunZhaoNianWeiWnd.s_FinishTaskIdx==index then
        -- print(self.m_Decos[index])
        -- for i=1,index do
        --     self.m_Decos[i]:SetActive(true)
        -- end
        self.m_Decos[index]:SetActive(true)
        local child = self.m_Decos[index].transform
        LuaTweenUtils.TweenAlpha(child,0,1,0.8)
        LuaXunZhaoNianWeiWnd.s_FinishTaskIdx = 0

        self.m_Background.color = Color(0.5,0.5,0.5,1)
        tweener = LuaTweenUtils.SetDelay(LuaTweenUtils.TweenFloat(0.5,1,0.5,function(val)
            self.m_Background.color = Color(val,val,val,1)
            if val==1 then
                tweener = nil
            end
        end),0.8)
    end

    local key = self.m_Keys[index]

    local data = ChunJie_XunZhaoNianWei2022.GetData(key)
    --看有没有完成
    local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or {}
    local taskId1 = data.Task1
    local taskId2 = data.Task2
    local finished = taskProp:IsMainPlayerTaskFinished(taskId2)
    if finished then
        self.StatusLabel.text = data.Title
        self.ContentLabel.text = data.Text

        self.AcceptButton:SetActive(false)

        if self.m_RewardStatus[key] then
            self.RewardButton:SetActive(false)
            self.TaskStatusLabel.text = LocalString.GetString("奖励已被领取")
            self.TaskStatusLabel.gameObject:SetActive(true)
        else
            self.RewardButton:SetActive(true)
            self.TaskStatusLabel.gameObject:SetActive(false)
        end
        -- self.TaskStatusLabel.gameObject:SetActive(false)

    else
        self.StatusLabel.text = LocalString.GetString("未完成")
        self.ContentLabel.text = g_MessageMgr:FormatMessage("XunZhaoNianWei_NotFinish_Text")

        local got = CommonDefs.DictContains_LuaCall(taskProp.CurrentTasks,taskId1) 
                    or CommonDefs.DictContains_LuaCall(taskProp.CurrentTasks,taskId2)
        if got then
            self.AcceptButton:SetActive(false)
            --任务进行中
            self.TaskStatusLabel.text = LocalString.GetString("任务进行中")
            self.TaskStatusLabel.gameObject:SetActive(true)
        else
            --接取任务
            self.AcceptButton:SetActive(true)
            self.TaskStatusLabel.gameObject:SetActive(false)
        end

        self.RewardButton:SetActive(false)
    end

    Extensions.RemoveAllChildren(self.Rewards.transform)
    for i=1,data.Award.Length,2 do
        local itemId = data.Award[i-1]
        local item = NGUITools.AddChild(self.Rewards,self.RewardItem)
        item:SetActive(true)
        local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        icon:LoadMaterial(Item_Item.GetData(itemId).Icon)
        local get = item.transform:Find("Get").gameObject

        if self.m_RewardStatus[key] then
            get:SetActive(true)
        else
            get:SetActive(false)
        end
        UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(g)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)

        end)
    end
    self.Rewards:GetComponent(typeof(UIGrid)):Reposition()


end

function LuaXunZhaoNianWeiWnd:OnEnable()
    g_ScriptEvent:AddListener("XZNW_QueryRewardResult", self, "OnXZNW_QueryRewardResult")
    g_ScriptEvent:AddListener("AcceptTask", self, "OnAcceptTask")
    g_ScriptEvent:AddListener("XZNW_RewardSuccess", self, "OnXZNW_RewardSuccess")
    
    
end
function LuaXunZhaoNianWeiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("XZNW_QueryRewardResult", self, "OnXZNW_QueryRewardResult")
    g_ScriptEvent:RemoveListener("AcceptTask", self, "OnAcceptTask")
    g_ScriptEvent:RemoveListener("XZNW_RewardSuccess", self, "OnXZNW_RewardSuccess")
end

function LuaXunZhaoNianWeiWnd:OnAcceptTask(args)
    local taskId = tonumber(args[0])
    self:OnSelectAtRow(self.m_SelectedTabIndex)
end

function LuaXunZhaoNianWeiWnd:OnXZNW_QueryRewardResult(rewardData_U)
    local v = MsgPackImpl.unpack(rewardData_U)

    self.m_RewardStatus = {}
    if CommonDefs.IsDic(v) then
        CommonDefs.DictIterate(v,DelegateFactory.Action_object_object(function (___key, ___value) 
            self.m_RewardStatus[tonumber(___key)] = ___value
        end))
    end

    for j,v in ipairs(self.m_TabCmps) do
        self:RefreshTabAlert(j)
    end
    self:OnSelectAtRow(self.m_SelectedTabIndex)
end
function LuaXunZhaoNianWeiWnd:RefreshTabAlert(idx)
    local cmp = self.m_TabCmps[idx]

    local alert = cmp.transform:Find("Alert").gameObject
    alert:SetActive(false)

    local key = self.m_Keys[idx]
    local data = ChunJie_XunZhaoNianWei2022.GetData(key)
    local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or {}
    local finished = taskProp:IsMainPlayerTaskFinished(data.Task2)
    if finished then
        if not self.m_RewardStatus[key] then
            alert:SetActive(true)
        end
    end
end

function LuaXunZhaoNianWeiWnd:OnXZNW_RewardSuccess(key)
    self.m_RewardStatus[key] = true
    self:OnSelectAtRow(self.m_SelectedTabIndex)

    for i,v in ipairs(self.m_Keys) do
        if v==key then
            self:RefreshTabAlert(i)
        end
    end
end
