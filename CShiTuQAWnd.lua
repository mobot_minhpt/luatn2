-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CShiTuQAWnd = import "L10.UI.CShiTuQAWnd"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local QuestionData = import "L10.UI.CShiTuQAWnd+QuestionData"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CShiTuQAWnd.m_ReceiveOtherAnswer_CS2LuaHook = function (this, sessionId, playerId, answer) 
    if answer > 0 and sessionId == this.QuestionSessionId then
        this.otherStatusLabel.text = this.GreenString
    end
end
CShiTuQAWnd.m_GetRandomQuestion_CS2LuaHook = function (this, array) 
    local list = CreateFromClass(MakeGenericClass(List, QuestionData))
    do
        local i = 0
        while i < array.Length do
            local data = CreateFromClass(QuestionData)
            data.index = i + 1
            data.text = array[i]
            CommonDefs.ListAdd(list, typeof(QuestionData), data)
            i = i + 1
        end
    end

    local returnArray = CreateFromClass(MakeArrayClass(QuestionData), array.Length)
    local index = 0
    while list.Count > 0 do
        local randomIndex = UnityEngine_Random(0, list.Count)
        local data = list[randomIndex]
        returnArray[index] = data
        CommonDefs.ListRemoveAt(list, randomIndex)
        index = index + 1
    end

    return returnArray
end
CShiTuQAWnd.m_Init_CS2LuaHook = function (this) 
    this.choiceTmplate:SetActive(false)
    this.choiceSperateNode:SetActive(false)
    if System.String.IsNullOrEmpty(CShiTuMgr.Inst.SaveQuestion.options) then
        this:Close()
        return
    end

    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        Gac2Gas.RequestStopShiTuQingXYLX()
        this:close()
    end)

    this.QuestionSessionId = CShiTuMgr.Inst.SaveQuestion.sessionId

    this.questionLabel.text = CChatLinkMgr.TranslateToNGUIText(CShiTuMgr.Inst.SaveQuestion.title, false)

    this.otherStatusLabel.text = this.RedString
    local questionArray = CommonDefs.StringSplit_ArrayChar(CShiTuMgr.Inst.SaveQuestion.options, ";")
    this.needShowArray = this:GetRandomQuestion(questionArray)
    Extensions.RemoveAllChildren(this.choiceTable.transform)
    do
        local i = 0
        while i < this.needShowArray.Length do
            local node = NGUITools.AddChild(this.choiceTable.gameObject, this.choiceTmplate)
            node:SetActive(true)
            CommonDefs.GetComponentInChildren_GameObject_Type(node, typeof(UILabel)).text = this.needShowArray[i].text
            node.transform:Find("clickbg").gameObject:SetActive(false)
            local data = this.needShowArray[i]
            UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (go) 
                this:QuestionClick(node, data)
            end)
            if CShiTuMgr.Inst.SaveQuestion.selfAnswer > 0 and CShiTuMgr.Inst.SaveQuestion.selfAnswer == data.index then
                node.transform:Find("clickbg").gameObject:SetActive(true)
                this.choiceSperateNode:SetActive(true)
            end
            i = i + 1
        end
    end
    this.choiceTable:Reposition()

    if CShiTuMgr.Inst.SaveQuestion.otherAnswer > 0 then
        this.otherStatusLabel.text = this.GreenString
    end
end
