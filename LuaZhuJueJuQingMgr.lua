require("common/common_include")
local CDanGongWnd = import "L10.UI.CDanGongWnd"

LuaZhuJueJuQingMgr={}

LuaZhuJueJuQingMgr.m_ScreenBrokenTaskId=0
LuaZhuJueJuQingMgr.m_ScreenBrokenMat=nil
LuaZhuJueJuQingMgr.m_ScreenBrokenHint=nil

LuaZhuJueJuQingMgr.m_BottomLines=nil

LuaZhuJueJuQingMgr.m_HanZiTaskId = 0
LuaZhuJueJuQingMgr.m_HanZiIndex = 1

LuaZhuJueJuQingMgr.m_IsTaskReviewOpen = true

function LuaZhuJueJuQingMgr.OpenDrawHanZiWnd(taskId, hanZiId)
	LuaZhuJueJuQingMgr.m_HanZiTaskId = taskId
	LuaZhuJueJuQingMgr.m_HanZiIndex = hanZiId
	CUIManager.ShowUI(CLuaUIResources.DrawHanZiWnd)
end

---------------------------------- 接宝玩法 ----------------------------------
LuaZhuJueJuQingMgr.m_FallingDownTreasureScore = 0
LuaZhuJueJuQingMgr.m_FallingDownTreasureEnd = false
LuaZhuJueJuQingMgr.m_FallingDownTreasureTaskId = 0

function LuaZhuJueJuQingMgr.OpenFallingDownTreasure(taskId)
	LuaZhuJueJuQingMgr.m_FallingDownTreasureScore = 0
	LuaZhuJueJuQingMgr.m_FallingDownTreasureTaskId = taskId	
	CUIManager.ShowUI(CLuaUIResources.FallingDownTreasureWnd)
end

function LuaZhuJueJuQingMgr.AddScore(score)
	if score then
		LuaZhuJueJuQingMgr.m_FallingDownTreasureScore = LuaZhuJueJuQingMgr.m_FallingDownTreasureScore + score
		g_ScriptEvent:BroadcastInLua("UpdateFallingDownTreasureScore")
	end
end

function LuaZhuJueJuQingMgr.AddFrozenStatus()
	g_ScriptEvent:BroadcastInLua("UpdateFallingDownTreasureFrozen")
end


---------------------------------- 伐木玩法 ----------------------------------

LuaZhuJueJuQingMgr.m_LumberingTaskId = 0
function LuaZhuJueJuQingMgr.OpenLumberingWnd(taskId)
	LuaZhuJueJuQingMgr.m_LumberingTaskId = taskId
	CUIManager.ShowUI(CLuaUIResources.LumberingWnd)
end


---------------------------------- 煎药玩法 ----------------------------------
EnumFireStatus = {
	eInit = 0,
	eDyingOut = 1,
	eNormal = 2,
	eScorch = 3,
}

LuaZhuJueJuQingMgr.m_BoilMedicineTaskId = 0
function LuaZhuJueJuQingMgr.OpenBoilMedicineWnd(taskId)
	LuaZhuJueJuQingMgr.m_BoilMedicineTaskId = taskId
	CUIManager.ShowUI(CLuaUIResources.BoilMedicineWnd)
end

---------------------------------- 蒲松龄写字 ----------------------------------

LuaZhuJueJuQingMgr.m_PuSongLingHandWriteTaskId = 0
function LuaZhuJueJuQingMgr.OpenPuSongLingHandWriteEffect(taskId)
	LuaZhuJueJuQingMgr.m_PuSongLingHandWriteTaskId = taskId
	CUIManager.ShowUI(CLuaUIResources.PuSongLingHandWriteEffectWnd)
end

---------------------------------- 彼岸映雪 ---------------------------------------
LuaZhuJueJuQingMgr.BiAnItemPlace = nil
LuaZhuJueJuQingMgr.BiAnItemPos = nil
LuaZhuJueJuQingMgr.BiAnItemId = nil
function LuaZhuJueJuQingMgr.OpenChooseBiAnTailWnd(place, pos, itemId)
	LuaZhuJueJuQingMgr.BiAnItemPlace = place
	LuaZhuJueJuQingMgr.BiAnItemPos = pos
	LuaZhuJueJuQingMgr.BiAnItemId = itemId
	CUIManager.ShowUI(CLuaUIResources.BiAnYingXueChooseWnd)
end

---------------------------------- 打弹弓 ---------------------------------------

LuaZhuJueJuQingMgr.danGongInfo = nil

function LuaZhuJueJuQingMgr:BeginShootBirdsInPlay(threshold, birdLength, birdWidth, flySpeed)
	self.danGongInfo = {}
	self.danGongInfo.threshold = threshold
	self.danGongInfo.birdLength = birdLength
	self.danGongInfo.birdWidth = birdWidth
	self.danGongInfo.flySpeed = flySpeed

	CDanGongWnd.taskID = 0
	CUIManager.ShowUI(CUIResources.DanGongWnd)
end

function LuaZhuJueJuQingMgr:BeginShootBirds(taskId, curCount, needCount)
	self.danGongInfo = nil
	CDanGongWnd.taskID = taskId
	CUIManager.ShowUI(CUIResources.DanGongWnd)
end

---------------------------------- 战狂 ---------------------------------------

LuaZhuJueJuQingMgr.ChangeFaceKey = 1
LuaZhuJueJuQingMgr.OperaPlayKey = 1

function LuaZhuJueJuQingMgr:ShowChangeFaceWnd(key)
    self.ChangeFaceKey = key
    CUIManager.ShowUI(CLuaUIResources.ZhanKuangTaskChangeFaceWnd)
end


function LuaZhuJueJuQingMgr:StartJuQingOperaPlay(key)
	self.OperaPlayKey = key
	if LocalString.isCN then
		CUIManager.ShowUI(CLuaUIResources.ZhanKuangTaskOperaPlayWnd)
	else
		RegisterTickOnce(function()
			Gac2Gas.OnFinishJuQingOperaPlay(self.OperaPlayKey, true)
		end, 500)
	end
end
local TextMeshExDataSource = import "L10.UI.TextMeshExDataSource"
local EnumTextMeshExKey = import "L10.UI.EnumTextMeshExKey"
LuaZhuJueJuQingMgr.m_NvZhanKuangJvQingZiMuDisappearTick = nil
-- 女战狂字幕剧情
function LuaZhuJueJuQingMgr:ShowNvZhanKuangZiMu(key,duration)
	local data = ZhuJueJuQing_ZiMu.GetData(key)
	if data then
		TextMeshExDataSource.Inst:SetDataWithEffect(data.TextKey, data.Text,data.FontSize,data.FontColor,true)
		if self.m_NvZhanKuangJvQingZiMuDisappearTick then
			UnRegisterTick(self.m_NvZhanKuangJvQingZiMuDisappearTick)
			self.m_NvZhanKuangJvQingZiMuDisappearTick = nil
		end
		self.m_NvZhanKuangJvQingZiMuDisappearTick = RegisterTickOnce(function()
			TextMeshExDataSource.Inst:SetData(data.TextKey, "")
		end, duration * 1000)
	end
end
LuaZhuJueJuQingMgr.m_BubblePlayId = nil
function LuaZhuJueJuQingMgr:ShowBubblePlayWnd(playId)
	self.m_BubblePlayId = playId
	CUIManager.ShowUI(CLuaUIResources.ZhanKuangNvBubbleWnd)
end
LuaZhuJueJuQingMgr.m_LearnSkillPlayId = nil
function LuaZhuJueJuQingMgr:ShowLearnSkillPlayWnd(playId)
	self.m_LearnSkillPlayId = playId
	CUIManager.ShowUI(CLuaUIResources.XueWuWnd)
end

LuaZhuJueJuQingMgr.m_FootStepPlayId = nil
function LuaZhuJueJuQingMgr:ShowFootStepPlayWnd(playId)
	self.m_FootStepPlayId = playId
	CUIManager.ShowUI(CLuaUIResources.FootStepPlayWnd)
end

LuaZhuJueJuQingMgr.m_ProgressPlayId = nil
function LuaZhuJueJuQingMgr:ShowShaoKaoPlayWnd(playId)
	self.m_ProgressPlayId = playId
	CUIManager.ShowUI(CLuaUIResources.ShaoKaoPlayWnd)
end

LuaZhuJueJuQingMgr.m_MakeFirePlayId = nil
function LuaZhuJueJuQingMgr:ShowMakeFirePlayWnd(playId)
	self.m_MakeFirePlayId = playId
	CUIManager.ShowUI(CLuaUIResources.ZuanMuQuHuoWnd)
end
EnumHuntRabbitPlayStatus = {
	Idle = 0,
	WaitingForPlayer = 1,
	Throwing = 2,
	Finished = 3,
	Failed = 4,
}
LuaZhuJueJuQingMgr.m_HuntRabbitPlay = nil
function LuaZhuJueJuQingMgr:UpdateHuntRabbitPlayStatus(playStatus, enterStatusTime, leftTime, extraData)
	LuaZhuJueJuQingMgr.m_HuntRabbitPlay = {} 
	-- 当前状态，0.Idle:玩法创建时		1.WaitingForPlayer:等待玩家投掷套圈		2.Throwing:套圈投掷过程		3.Finished:成功		4.Failed:失败
	LuaZhuJueJuQingMgr.m_HuntRabbitPlay.playStatus = playStatus
	-- 设置该状态时的时间戳
	LuaZhuJueJuQingMgr.m_HuntRabbitPlay.enterStatusTime = enterStatusTime
	-- 玩法剩余时间
	LuaZhuJueJuQingMgr.m_HuntRabbitPlay.leftTime = leftTime
	-- 额外数据，目前只有一个投掷次数
	LuaZhuJueJuQingMgr.m_HuntRabbitPlay.extraData = g_MessagePack.unpack(extraData)
	if not CUIManager.IsLoaded(CLuaUIResources.TaoQuan2Wnd) then
		CUIManager.ShowUI(CLuaUIResources.TaoQuan2Wnd)
	else
		g_ScriptEvent:BroadcastInLua("UpdateHuntRabbitPlayStatus")
	end
end
function LuaZhuJueJuQingMgr:HuntRabbitReplyRingPosResult(bSuccess, x, y)
	g_ScriptEvent:BroadcastInLua("HuntRabbitReplyRingPosResult",bSuccess, x, y)
end



---------------------------------- RPC ----------------------------------


function Gas2Gac.OpenScreenBroken(mat, hint)
	LuaZhuJueJuQingMgr.m_ScreenBrokenMat=mat
	LuaZhuJueJuQingMgr.m_ScreenBrokenHint=hint
	CUIManager.ShowUI(CLuaUIResources.ScreenBrokenWnd)
end

function Gas2Gac.RequestOpenScreenBroken(taskId)
	
	LuaZhuJueJuQingMgr.m_ScreenBrokenTaskId = taskId
	LuaZhuJueJuQingMgr.m_HideExit = false
	CUIManager.ShowUI(CLuaUIResources.ScreenBrokenWnd)
end


LuaZhuJueJuQingMgr.m_HideExit = false
LuaZhuJueJuQingMgr.m_HintMsg = nil
LuaZhuJueJuQingMgr.m_HintColor = nil
function Gas2Gac.RequestOpenScreenBrokenWithoutExit(taskId, hintMsg, color)
	LuaZhuJueJuQingMgr.m_ScreenBrokenTaskId = taskId
	LuaZhuJueJuQingMgr.m_HintMsg = hintMsg
	LuaZhuJueJuQingMgr.m_HintColor = color
	LuaZhuJueJuQingMgr.m_HideExit = true
	CUIManager.ShowUI(CLuaUIResources.ScreenBrokenWnd)
end

function Gas2Gac.RequestOpenDrawHanZi(taskId, hanZiId)
	LuaZhuJueJuQingMgr.OpenDrawHanZiWnd(taskId, hanZiId)
end

local CUIFxPaths = import "L10.UI.CUIFxPaths"

function Gas2Gac.FinishHandDraw()
	if CUIManager.IsLoaded(CLuaUIResources.InkDrawWnd) then
		local fxPath = CUIFxPaths.WanChengFxPath
		EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {fxPath})
		CUIManager.CloseUI(CLuaUIResources.InkDrawWnd)
	end
end

function Gas2Gac.ShowUIDangerEffect(enable)
	EventManager.BroadcastInternal(EnumEventType.UpdateDangerWarning, enable)
end
