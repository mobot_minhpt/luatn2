local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"

LuaMyShangShiIngredientWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMyShangShiIngredientWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaMyShangShiIngredientWnd, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaMyShangShiIngredientWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaMyShangShiIngredientWnd, "ItemsScrollView", "ItemsScrollView", CUIRestrictScrollView)

--@endregion RegistChildComponent end
RegistClassMember(LuaMyShangShiIngredientWnd,"m_TabIndex")
RegistClassMember(LuaMyShangShiIngredientWnd,"m_List")
RegistClassMember(LuaMyShangShiIngredientWnd,"m_Row")

function LuaMyShangShiIngredientWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)

	
	UIEventListener.Get(self.ReadMeLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReadMeLabelClick()
	end)


    --@endregion EventBind end
end

function LuaMyShangShiIngredientWnd:OnEnable()
    g_ScriptEvent:AddListener("OnShangShiSendMyFoodIngredient", self, "OnShangShiSendMyFoodIngredient")
end

function LuaMyShangShiIngredientWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnShangShiSendMyFoodIngredient", self, "OnShangShiSendMyFoodIngredient")
end

function LuaMyShangShiIngredientWnd:OnShangShiSendMyFoodIngredient()
    local list = {}
    for k , v in pairs(LuaCookBookMgr.m_MyFoodIngredientData) do
        if (v.data.IsMethod == 1 and self.m_TabIndex == 1) or (v.data.IsMethod == 0 and self.m_TabIndex == 0) then
            table.insert(list,LuaCookBookMgr.m_MyFoodIngredientData[k])
        end
    end
    table.sort(list,function(a,b) 
        if a.isNew and not b.isNew then
            return true
        elseif not a.isNew and b.isNew then 
            return false
        elseif a.count > 0 and b.count == 0 then 
            return true
        elseif a.count == 0 and b.count > 0 then 
            return false
        end
        return a.data.ID < b.data.ID
    end)
    self.m_List = list
    self.QnTableView:ReloadData(true, true)
    self.QnTableView:SetSelectRow(self.m_Row, false)
    self.ItemsScrollView:ResetPosition()
end

function LuaMyShangShiIngredientWnd:Init()
    self.ReadMeLabel.text = g_MessageMgr:FormatMessage("MyShangShiIngredientWnd_ReadMe")
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_List and #self.m_List or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.Tabs:ChangeTab(0,false)
end

function LuaMyShangShiIngredientWnd:ItemAt(item,index)
    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local markSprite = item.transform:Find("MarkSprite").gameObject
    local newMark = item.transform:Find("New").gameObject

    local data = self.m_List[index + 1]

    iconTexture:LoadMaterial(SafeStringFormat3("UI/Texture/Item_Other/Material/%s.mat",data.data.Icon))
    numLabel.text = data.count
    numLabel.gameObject:SetActive(data.count > 0)
    markSprite.gameObject:SetActive(data.count == 0)
    newMark.gameObject:SetActive(data.isNew)
    Extensions.SetLocalPositionZ(iconTexture.transform, data.count == 0 and -1 or 0)
end

--@region UIEvent

function LuaMyShangShiIngredientWnd:OnTabsTabChange(index)
    self.m_TabIndex = index
    self.m_Row = 0
    Gac2Gas.ShangShiMyFoodIngredient()
end


function LuaMyShangShiIngredientWnd:OnReadMeLabelClick()
    local url = self.ReadMeLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end


--@endregion UIEvent

function LuaMyShangShiIngredientWnd:OnSelectAtRow(row)
    self.m_Row = row
    local data = self.m_List[row + 1]
    local templateId = data.data.ItemId
    local names = {}
    local actions = {}
    Gac2Gas.ShangShiRemoveIngredientNewFlag(data.data.ID)
    if data.count > 0 then
        table.insert(names,LocalString.GetString("取至包裹"))
    end

    table.insert(actions,function()
        local data = self.m_List[row + 1]
        if data.count == 1 then
            Gac2Gas.ShangShiRetrieveIngredient(data.data.ID, 1)
            CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        else
            CLuaNumberInputMgr.ShowNumInputBox(1, data.count, 1, function (val)
                Gac2Gas.ShangShiRetrieveIngredient(data.data.ID, val)
                CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
            end, LocalString.GetString("可将重复的食材放入包裹赠送给其他玩家"), -1)
        end
    end)
    local actionSource = DefaultItemActionDataSource.Create(#names,actions,names)
    CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, actionSource, AlignType.Default, 0, 0, 0, 0)
end
