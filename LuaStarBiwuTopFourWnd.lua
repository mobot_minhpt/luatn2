local Screen = import "UnityEngine.Screen"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"

CLuaStarBiwuTopFourWnd=class()
CLuaStarBiwuTopFourWnd.TeamCount = 8

RegistClassMember(CLuaStarBiwuTopFourWnd, "RankRoot") -- 第几名的root
RegistClassMember(CLuaStarBiwuTopFourWnd, "RankLabel") -- 第几名的数值label

RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamMember1")
RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamMember2")
RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamMember3")
RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamMember4")
RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamMember5")
RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamMember6")
RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamMember7")

RegistClassMember(CLuaStarBiwuTopFourWnd, "TeamNameLabel") -- xxx的战队
RegistClassMember(CLuaStarBiwuTopFourWnd, "PreviousButton") -- 前一个队伍
RegistClassMember(CLuaStarBiwuTopFourWnd, "NextButton") -- 后一个队伍


RegistClassMember(CLuaStarBiwuTopFourWnd, "IsFinished") -- 全民争霸是否结束
RegistClassMember(CLuaStarBiwuTopFourWnd, "CurrentIndex") -- 现在选中的index（1-4）
RegistClassMember(CLuaStarBiwuTopFourWnd, "ResultLabel") --

RegistClassMember(CLuaStarBiwuTopFourWnd, "Title") -- RPC返回的战队信息
RegistClassMember(CLuaStarBiwuTopFourWnd, "ZhanDuiId") -- RPC返回的战队ID
RegistClassMember(CLuaStarBiwuTopFourWnd, "MemberInfos") -- RPC返回的战队成员信息

RegistClassMember(CLuaStarBiwuTopFourWnd, "Layout") --排列信息
RegistClassMember(CLuaStarBiwuTopFourWnd, "ModelLayout") --排列信息
RegistClassMember(CLuaStarBiwuTopFourWnd, "Identifier")
RegistClassMember(CLuaStarBiwuTopFourWnd, "mRO")
RegistClassMember(CLuaStarBiwuTopFourWnd, "mChildROList")
RegistChildComponent(CLuaStarBiwuTopFourWnd,"m_TitleLabel","TitleLabel", UILabel)
RegistChildComponent(CLuaStarBiwuTopFourWnd,"m_PreviewerTexture","PreviewerTexture", UITexture)
function CLuaStarBiwuTopFourWnd:Init()
	self:InitTitle()
	self:InitClassMember()

	self.TeamMember1.gameObject:SetActive(false)
	self.TeamMember2.gameObject:SetActive(false)
	self.TeamMember3.gameObject:SetActive(false)
	self.TeamMember4.gameObject:SetActive(false)
	self.TeamMember5.gameObject:SetActive(false)
	self.TeamMember6.gameObject:SetActive(false)
	self.TeamMember7.gameObject:SetActive(false)


	local onPrevious = function (go)
		self:OnPreviousClicked(go)
	end
	CommonDefs.AddOnClickListener(self.PreviousButton.gameObject,DelegateFactory.Action_GameObject(onPrevious),false)

	local onNext = function (go)
		self:OnNextClicked(go)
	end
	CommonDefs.AddOnClickListener(self.NextButton.gameObject,DelegateFactory.Action_GameObject(onNext),false)

	self:CheckPreviousAndNext()
	self:DefaultChoose()
end

function CLuaStarBiwuTopFourWnd:InitTitle()
	local nowTime = CServerTimeMgr.Inst.timeStamp
	local autopatchData = nil
	StarBiWuShow_AutoPatch.Foreach(function(key, data) 
		local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.StartTime)
		if startTime < nowTime then
			local endTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
			if endTime > nowTime then
				autopatchData = StarBiWuShow_AutoPatch.GetData(key)
			end
		end
	end)
	if autopatchData then
		self.m_TitleLabel.text = autopatchData.Desc
	end
end

-- 初始化本类数据
function CLuaStarBiwuTopFourWnd:InitClassMember()

	self.RankRoot = self.transform:Find("Anchor/Rank").gameObject
	self.RankLabel = self.transform:Find("Anchor/Rank/RankLabel"):GetComponent(typeof(UILabel))

	self.TeamMember1 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer1"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember2 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer2"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember3 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer3"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember4 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer4"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember5 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer5"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember6 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer6"):GetComponent(typeof(QnModelPreviewer))
	self.TeamMember7 = self.transform:Find("Anchor/TeamMenbers/QnModelPreviewer7"):GetComponent(typeof(QnModelPreviewer))

	self.TeamNameLabel = self.transform:Find("Anchor/Selections/TeamNameLabel"):GetComponent(typeof(UILabel))

	self.PreviousButton = self.transform:Find("Anchor/Selections/PreviousButton"):GetComponent(typeof(CButton))
	self.NextButton = self.transform:Find("Anchor/Selections/NextButton"):GetComponent(typeof(CButton))
	self.ResultLabel = self.transform:Find("Anchor/Selections/ResultLabel"):GetComponent(typeof(UILabel))

	self.IsFinished = false
	self.CurrentIndex = 1
	self.Title = ""
	self.ZhanDuiId = nil
	self.MemberInfos = {}

end

-- 默认选项
function CLuaStarBiwuTopFourWnd:DefaultChoose()
	self:ChooseTeam(1)
end

-- 选择某一个队伍
function CLuaStarBiwuTopFourWnd:ChooseTeam(index)
	if index >= 1 and index <= CLuaStarBiwuTopFourWnd.TeamCount then
		Gac2Gas.QueryStarBiwuZongJueSaiOverView(index)
	end
end

-- 点击前一个队伍
function CLuaStarBiwuTopFourWnd:OnPreviousClicked(go)
	if self.CurrentIndex <= 1 then return end
	self:ChooseTeam(self.CurrentIndex - 1)
end

-- 点击后一个队伍
function CLuaStarBiwuTopFourWnd:OnNextClicked(go)
	if self.CurrentIndex >= CLuaStarBiwuTopFourWnd.TeamCount then return end
	self:ChooseTeam(self.CurrentIndex + 1)
end

function CLuaStarBiwuTopFourWnd:OnEnable()
	g_ScriptEvent:AddListener("ReplyStarBiwuZongJueSaiOverView", self, "UpdateQMPKTopFourInfos")

end

function CLuaStarBiwuTopFourWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZongJueSaiOverView", self, "UpdateQMPKTopFourInfos")
end

-- 更新四强界面的显示
function CLuaStarBiwuTopFourWnd:UpdateQMPKTopFourInfos(index, zhanduiId, title, memberInfos, isFinish)
	self.IsFinished = (isFinish ~= 0)
	self.CurrentIndex = index
	self.Title = title
	self.ZhanDuiId = zhanduiId
	self.MemberInfos = memberInfos

	self:UpdateTeamNameOrRank()
	-- 显示玩家的形象
	local teamNum = 0
	self.Layout = {}
	self.ModelLayout = {}
	for i =1, 7 do
		teamNum = self.MemberInfos[i] and (teamNum + 1) or teamNum
		local previewName = SafeStringFormat3("TeamMember%s", tostring(i))
		table.insert(self.Layout, self[previewName].transform.localPosition)
	end
	teamNum = teamNum < 5 and 5 or teamNum
	local data = StarBiWuShow_TopFourWndLayout.GetData(teamNum)
	if data then
		for i =1, 7 do
			local posName = SafeStringFormat3("Pos%d", i)
			self.Layout[i] = Vector3(data[posName][0],data[posName][1],0)
			local modelPosName = SafeStringFormat3("ModelPos%d", i)
			self.ModelLayout[i] = Vector3(data[modelPosName][0],data[modelPosName][1],data[modelPosName][2])
		end
	end
	for i =1, 7 do
		local previewName = SafeStringFormat3("TeamMember%s", tostring(i))
		self:InitTeamMember(self[previewName], self.MemberInfos[i], self.Layout[i])
	end
	self:InitPreviewerTexture()
	-- 处理按钮是否可用
	self:CheckPreviousAndNext()
end

function CLuaStarBiwuTopFourWnd:InitPreviewerTexture()
	if self.m_PreviewerTexture.mainTexture and self.mRO then
		self:LoadModels(self.mRO)
		return
	end
	self.Identifier = "StarBiwuTopFourWnd_PreviewerTexture"
	CUIManager.DestroyModelTexture(self.Identifier)
	local modelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
		self.mRO = ro
		self:LoadModels(ro)
    end)
	self.m_PreviewerTexture.mainTexture =  CUIManager.CreateModelTexture(self.Identifier, modelTextureLoader,180,0.1,-1,16.64,false,true,1,true)
	self.m_PreviewerTexture.mainTexture:Release()
	self.m_PreviewerTexture.mainTexture.width = 2048
	self.m_PreviewerTexture.mainTexture.height = 2048
	self.m_PreviewerTexture.mainTexture:Create()
end

function CLuaStarBiwuTopFourWnd:LoadModels(ro)
	--Extensions.RemoveAllChildren(ro.transform)
	if self.mChildROList and #self.mChildROList > 0 then
		for i = 1,#self.mChildROList do
			self.mChildROList[i]:Destroy()
		end
	end
	self.mChildROList = {}
		for i = 1, 7 do
			local info = self.MemberInfos[i]
			if info then
				local childRO = CRenderObject.CreateRenderObject(ro.gameObject)
				childRO.NeedUpdateAABB = true;
				childRO.Layer = LayerDefine.ModelForNGUI_3D
				childRO.name = info.memberName
				local fakeAppearance = info.fakeAppearance
				local layoutPos = self.ModelLayout[i]
				childRO.transform.localPosition = layoutPos
				CClientMainPlayer.LoadResource(childRO, fakeAppearance, true, 1, 0, false, fakeAppearance.ResourceId, false, 0, false, nil, false)
				local previewName = SafeStringFormat3("TeamMember%s", tostring(i))
				UIEventListener.Get(self[previewName].gameObject).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
					local rot = -delta.x / Screen.width * 360
					local rotation = childRO.transform.localEulerAngles.y
					rotation = rotation + rot;
					childRO.transform.localRotation = Quaternion.Euler(0, rotation, 0)
				end)
				table.insert(self.mChildROList, childRO)
			end
		end
end

function CLuaStarBiwuTopFourWnd:OnDestroy()
    CUIManager.DestroyModelTexture(self.Identifier)
end

-- 更新界面的战队名称或排行
function CLuaStarBiwuTopFourWnd:UpdateTeamNameOrRank()
	if self.IsFinished then	
		self.RankLabel.text = tostring(self.CurrentIndex)
		self.ResultLabel.gameObject:SetActive(true)
	end
	self.RankRoot:SetActive(self.IsFinished and self.CurrentIndex <= 4)
	self.TeamNameLabel.text = self.Title
	self.ResultLabel.text = self:GetResultText(self.CurrentIndex)
end

-- 更新玩家信息
function CLuaStarBiwuTopFourWnd:InitTeamMember(previewer, info, pos)
	if not info then
		previewer.gameObject:SetActive(false)
		return
	end
	previewer.gameObject:SetActive(true)

	local tf = previewer.transform
	local playerNameLabel = tf:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local serverNameLabel = tf:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
	playerNameLabel.text = info.memberName
	serverNameLabel.text = info.serverName

	previewer.transform.localPosition = pos
end

-- 获得名次的中文显示
function CLuaStarBiwuTopFourWnd:GetResultText(index)
	if self.IsFinished then
		if index == 1 then
			return LocalString.GetString("冠军")
		elseif index == 2 then
			return LocalString.GetString("亚军")
		elseif index == 3 then
			return LocalString.GetString("季军")
		elseif index == 4 then
			return LocalString.GetString("殿军")
		end
	end
	return LocalString.GetString("八强战队")
end

function CLuaStarBiwuTopFourWnd:CheckPreviousAndNext()
	self.PreviousButton.Enabled = self.CurrentIndex > 1
	self.NextButton.Enabled = self.CurrentIndex < CLuaStarBiwuTopFourWnd.TeamCount
end