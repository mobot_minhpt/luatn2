local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CGuildMgr=import "L10.Game.CGuildMgr"
local CGuildInfo = import "L10.Game.CGuildInfo"
local CGuildDynamicInfoRpc = import "L10.Game.CGuildDynamicInfoRpc"
local CGuildMemberInfoRpc=import "L10.Game.CGuildMemberInfoRpc"
-- local Guild_Job=import "L10.Game.Guild_Job"
local CTianMenShanBattleInfoRpc = import "L10.Game.CTianMenShanBattleInfoRpc"
local CGuildMemberKouDaoInfoRpc=import "L10.Game.CGuildMemberKouDaoInfoRpc"
local CGuildLeagueDataRpc=import "L10.Game.CGuildLeagueDataRpc"
local CGuildMercenaryInfoRpc = import "L10.Game.CGuildMercenaryInfoRpc"
local CCityWarWeekInfoRpc = import "L10.Game.CCityWarWeekInfoRpc"
local CCityWarSeasonInfoRpc = import "L10.Game.CCityWarSeasonInfoRpc"
local CGuildMemberApplyInfoRpc = import "L10.Game.CGuildMemberApplyInfoRpc"
-- local CGuildHistoryInfoRpc=import "L10.Game.CGuildHistoryInfoRpc"
local CGuildMonsterSiegeInfoRpc=import "L10.Game.CGuildMonsterSiegeInfoRpc"
local GuildDefine = import "L10.Game.GuildDefine"
local EGuildJob = import "L10.Game.EGuildJob"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

-- local CGuildRightsInfoRpc=import "L10.Game.CGuildRightsInfoRpc"
CLuaGuildMgr={}

CLuaGuildMgr.m_TianMenShanInfo = nil
CLuaGuildMgr.m_KouDaoInfo=nil
CLuaGuildMgr.m_LeagueInfo1=nil
CLuaGuildMgr.m_LeagueInfo2=nil
CLuaGuildMgr.m_LeagueInfo3=nil
CLuaGuildMgr.m_MercenaryInfo = nil
CLuaGuildMgr.m_CityWarWeekInfo = nil
CLuaGuildMgr.m_CityWarSeasonInfo = nil
CLuaGuildMgr.m_GuildMemberApplyInfo=nil
CLuaGuildMgr.m_GuildHistoryInfo=nil
CLuaGuildMgr.m_GuildMonsterSiegeInfo=nil

CLuaGuildMgr.m_GuildRightsDic=nil

CLuaGuildMgr.m_ExtraNames={}--备注名称

function CLuaGuildMgr.ShowChangeGuild()
    if CClientMainPlayer.Inst==nil then
        return
    end
    if CClientMainPlayer.Inst:IsInGuild() then
        CLuaGuildWnd.fromChangeGuild = true
        CUIManager.ShowUI(CUIResources.GuildWnd)
    else
        g_MessageMgr:ShowMessage("NOT_IN_ANY_GUILD")
    end
end

function CLuaGuildMgr.GetGuildMemberApplyInfo()
    CLuaGuildMgr.m_GuildMemberApplyInfo=nil
    Gac2Gas.RequestGetGuildInfo("GuildMemberApplyInfo")
end
function CLuaGuildMgr.GetGuildHistoryInfo()
    CLuaGuildMgr.m_GuildHistoryInfo=nil
    Gac2Gas.RequestGetGuildInfo("GuildHistoryInfo")
end

function CLuaGuildMgr.GetGuildRightsInfo()
    CLuaGuildMgr.m_GuildRightsDic=nil
    Gac2Gas.RequestGetGuildInfo("GuildRightsInfo")
end

function CLuaGuildMgr.GetMyGuildInfoStaticOnly()
	Gac2Gas.RequestGetGuildInfo("MyGuildInfoStaticOnly")
end

function CLuaGuildMgr.GetMyGuildInfoDynamicOnly()
	Gac2Gas.RequestGetGuildInfo("MyGuildInfoDynamicOnly")
end

function CLuaGuildMgr.SetLeagueCommentLevel(label,val)
    local setting={4,2,1,0.75,0.5,0}--甲 已 丙 丁 戊 己
    local names={
        LocalString.GetString("甲"),
        LocalString.GetString("乙"),
        LocalString.GetString("丙"),
        LocalString.GetString("丁"),
        LocalString.GetString("戊"),
        LocalString.GetString("己")}
    local colors={
        NGUIText.ParseColor24("fc24ff",0),
        NGUIText.ParseColor24("ff4444",0),
        NGUIText.ParseColor24("46fdff",0),
        NGUIText.ParseColor24("66ff22",0),
        NGUIText.ParseColor24("ff7a28",0),
        NGUIText.ParseColor24("ffffff",0),
    }

    if val<0 then
        label.text=nil
    else
        local level=5
        for i,v in ipairs(setting) do
            if val>=v then
                level=i
                break
            end
        end
        -- return sprites[level]
        label.text=names[level]
        label.color=colors[level]
    end
end

function CLuaGuildMgr.SetMonsterSiegeScoreLevel(label,val)
    local setting={0.7,0.5,0.35,0.25,0.1,0}--甲 已 丙 丁 戊 己
    local names={
        LocalString.GetString("甲"),
        LocalString.GetString("乙"),
        LocalString.GetString("丙"),
        LocalString.GetString("丁"),
        LocalString.GetString("戊"),
        LocalString.GetString("己")}
    local colors={
        NGUIText.ParseColor24("fc24ff",0),
        NGUIText.ParseColor24("ff4444",0),
        NGUIText.ParseColor24("46fdff",0),
        NGUIText.ParseColor24("66ff22",0),
        NGUIText.ParseColor24("ff7a28",0),
        NGUIText.ParseColor24("ffffff",0),
    }

    if val<0 then
        label.text=nil
    else
        local level=5
        for i,v in ipairs(setting) do
            if val>=v then
                level=i
                break
            end
        end
        label.text=names[level]
        label.color=colors[level]
    end
end

function CLuaGuildMgr.IsGuildBangZhong(title)
    return GuildDefine.GetGuildJob(title) == EGuildJob.BangZhong
end

function CLuaGuildMgr.IsGuildXueTu(title)
    return GuildDefine.GetGuildJob(title) == EGuildJob.XueTu
end

function CLuaGuildMgr.IsGuildBangZhongOrXueTu(title)
    local job = GuildDefine.GetGuildJob(title)
    return job == EGuildJob.BangZhong or job == EGuildJob.XueTu
end

function Gas2Gac.SendGuildInfo(param1,param2)
    if param2 == "AllGuilds" then
        CGuildMgr.Inst:OnGetAllGuilds(param1)
    elseif param2 == "AllGuildsCreation" then
        CGuildMgr.Inst:OnGetAllGuildsCreation(param1)
    elseif param2 == "MyGuildInfo" then
        CGuildMgr.Inst:OnGetMyGuildInfo(param1)
    elseif param2 == "MyGuildInfoDynamic" then
        CGuildMgr.Inst:OnGetMyGuildInfoDynamic(param1)
    elseif param2 == "GuildMemberInfo" then
        CGuildMgr.Inst:OnGetGuildMemberInfo(param1)
    elseif param2 == "ExportInfo" then
        CGuildMgr.Inst:OnGetGuildExportInfo(param1)
    else
        Gas2Gac_SendGuildInfo(param1,param2)
    end
end

function Gas2Gac_SendGuildInfo(param1,param2)
    if param2 == "TianMenShanBattleZhuLiInfo" then
        local info = CTianMenShanBattleInfoRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_TianMenShanInfo == nil then
            CLuaGuildMgr.m_TianMenShanInfo = {}
        end
        CommonDefs.DictIterate(
            info.Infos,
            DelegateFactory.Action_object_object(
                function(___key, ___value)
                    CLuaGuildMgr.m_TianMenShanInfo[___key] = ___value
                end
            )
        )
    elseif param2=="GuildKouDaoInfo" or param2=="HengYuDangKouInfo" then
        local info=CGuildMemberKouDaoInfoRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_KouDaoInfo==nil then CLuaGuildMgr.m_KouDaoInfo={} end
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_KouDaoInfo[___key]=___value
        end))

    elseif param2=="GuildLeagueFirst" then
        local info=CGuildLeagueDataRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_LeagueInfo1==nil then CLuaGuildMgr.m_LeagueInfo1={} end
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_LeagueInfo1[___key]=___value
        end))
    elseif param2=="GuildLeagueSecond" then
        local info=CGuildLeagueDataRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_LeagueInfo2==nil then CLuaGuildMgr.m_LeagueInfo2={} end
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_LeagueInfo2[___key]=___value
        end))
    elseif param2=="GuildLeagueThird" then
        local info=CGuildLeagueDataRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_LeagueInfo3==nil then CLuaGuildMgr.m_LeagueInfo3={} end
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_LeagueInfo3[___key]=___value
        end))
    elseif param2 == "GuildCityWarAppointInfo" then
        local info = CGuildMercenaryInfoRpc()
        info:LoadFromString(param1)

        CLuaGuildMgr.m_MercenaryInfo = {}
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_MercenaryInfo[___key]=___value
        end))
    elseif param2 == "CityWarWeekInfo" then
        local info = CCityWarWeekInfoRpc()
        info:LoadFromString(param1)

        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_CityWarWeekInfo[___key]=___value
        end))
    elseif param2 == "CityWarSeasonInfo" then
        local info = CCityWarSeasonInfoRpc()
        info:LoadFromString(param1)

        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_CityWarSeasonInfo[___key]=___value
        end))
    elseif param2 == "GuildMemberApplyInfo" then
        local info = CGuildMemberApplyInfoRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_GuildMemberApplyInfo==nil then CLuaGuildMgr.m_GuildMemberApplyInfo={} end
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_GuildMemberApplyInfo[___key]=___value
        end))
    elseif param2 == "GuildHistoryInfo" then
        local info = CGuildHistoryInfoRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_GuildHistoryInfo==nil then CLuaGuildMgr.m_GuildHistoryInfo={} end
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_GuildHistoryInfo[___key]=___value
        end))
    elseif param2 == "MonsterSiegeEliteInfo" then
        local info = CGuildMonsterSiegeInfoRpc()
        info:LoadFromString(param1)

        if CLuaGuildMgr.m_GuildMonsterSiegeInfo==nil then CLuaGuildMgr.m_GuildMonsterSiegeInfo={} end
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CLuaGuildMgr.m_GuildMonsterSiegeInfo[___key]=___value
        end))
    elseif param2 == "GuildRightsInfo" then
        CLuaGuildMgr.OnGetGuildRightsInfo(param1)
	elseif param2 == "MyGuildInfoStaticOnly" then
		CGuildMgr.Inst.m_GuildInfo = CGuildInfo()
		CGuildMgr.Inst.m_GuildInfo:LoadFromString(param1)
	elseif param2 == "MyGuildInfoDynamicOnly" then
		CGuildMgr.Inst.m_GuildDynamicInfo = CGuildDynamicInfoRpc()
		CGuildMgr.Inst.m_GuildDynamicInfo:LoadFromString(param1)
    end
end

function CLuaGuildMgr.OnGetGuildRightsInfo(infoUD)
    local info = CGuildRightsInfoRpc()
    info:LoadFromString(infoUD)
    if CLuaGuildMgr.m_GuildRightsDic==nil then CLuaGuildMgr.m_GuildRightsDic={} end

    local officeInfo = MsgPackImpl.unpack(info.GuildRights.Data)
    if officeInfo then
        CommonDefs.DictIterate(officeInfo, DelegateFactory.Action_object_object(function (___key, ___value)
            local office = tonumber(___key)

            local rights = {}
            local raw = ___value
            if raw and CommonDefs.IsDic(raw) then
                CommonDefs.DictIterate(raw, DelegateFactory.Action_object_object(function (k, v)
                    rights[tonumber(k)] = tonumber(v)
                end))
            elseif raw and CommonDefs.IsList(raw) then--如果服务器的字段是连续的，发过来是list
                for i=1,raw.Count do
                    rights[i] = tonumber(raw[i-1])
                end
            end
            CLuaGuildMgr.m_GuildRightsDic[office] = rights
        end))
    end
end

function CLuaGuildMgr.SetRightInfo(title, rights)
    if not CLuaGuildMgr.m_GuildRightsDic then
        CLuaGuildMgr.m_GuildRightsDic={}
    end
    CLuaGuildMgr.m_GuildRightsDic[title] = rights
end

function CLuaGuildMgr.GetMyRight(key)
    -- if not CLuaGuildMgr.m_GuildRightsDic then return false end
    local keys = {}
    Guild_Job.ForeachKey(function (key)
        table.insert(keys, key)
    end)
    local id = CClientMainPlayer.Inst  and CClientMainPlayer.Inst.BasicProp.Id or 0
    local selfInfo = CommonDefs.DictGetValue_LuaCall(CGuildMgr.Inst.m_GuildMemberInfo.Infos, id)
    if not selfInfo then return false end

    local title = selfInfo.Title--我的职位
    local rights = CLuaGuildMgr.m_GuildRightsDic and CLuaGuildMgr.m_GuildRightsDic[title] or {}

    for i,v in ipairs(keys) do
        local job = Guild_Job.GetData(v)
        if job.VarName == key then
            local config = 0
            if title == 1 then config = job.eBangZhu
            elseif title == 2 then config = job.eFuBangZhu
            elseif title == 3 then config = job.eZhangLao
            elseif math.floor(title/10)==1 then config = job.eTangZhu
            elseif math.floor(title/10)==2 then config = job.eXiangZhu
            elseif math.floor(title/10)==3 then config = job.eJingYing
            elseif title == 100 then config = job.eBangZhong
            elseif title == 4 or title ==  5 then config = job.eHuFa--hufa
            elseif title == 101 then config = job.eXueTu--xuetu
            end
            if config == 0 or config ==1 then
                return config>0
            else
                local ret = false
                if config ==2 then
                    ret = true
                elseif config == 3 then
                    ret = false
                end
                return rights[job.ID] and rights[job.ID]>0 or ret
            end
            break
        end
    end
end

function Gas2Gac.SendGuildInfoEnd(param1)
    if param1 == "AllGuilds" then
        CGuildMgr.Inst:OnGetAllGuildsEnd()
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_AllGuilds")
    elseif param1 == "AllGuildsCreation" then
        CGuildMgr.Inst:OnGetAllGuildsCreationEnd()
    elseif param1 == "MyGuildInfo" then
        CGuildMgr.Inst:OnGetMyGuildInfoEnd()
    elseif param1 == "ExportInfo" then
        CGuildMgr.Inst:OnGetGuildExportInfoEnd()
    ----------------------------------------
    elseif param1 == "TianMenShanBattleZhuLiInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_TianMenShanBattleZhuLiInfo")
    elseif param1 == "GuildKouDaoInfo" or param1 == "HengYuDangKouInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildKouDaoInfo")
    elseif param1 == "GuildLeagueFirst" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildLeagueInfo",1)
    elseif param1 == "GuildLeagueSecond" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildLeagueInfo",2)
    elseif param1 == "GuildLeagueThird" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildLeagueInfo",3)
    elseif param1 == "GuildCityWarAppointInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildMercenaryInfo")
    elseif param1 == "CityWarWeekInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_CityWarWeekInfo")
    elseif param1 == "CityWarSeasonInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_CityWarSeasonInfo")
    elseif param1 == "GuildMemberApplyInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildMemberApplyInfo")
    elseif param1 == "GuildHistoryInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildHistoryInfo")
    elseif param1 == "MonsterSiegeEliteInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_MonsterSiegeEliteInfo")
    elseif param1 == "GuildRightsInfo" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_GuildRightsInfo")
    elseif param1 == "MyGuildInfoStaticOnly" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_MyGuildInfoStaticOnly")
    elseif param1 == "MyGuildInfoDynamicOnly" then
        g_ScriptEvent:BroadcastInLua("SendGuildInfo_MyGuildInfoDynamicOnly")
    else
        --GuildMemberInfo 
        EventManager.BroadcastInternalForLua(EnumEventType.GuildInfoReceived, {param1})
    end
end

function Gas2Gac.SyncPlayerKouDaoEliteRight(rightTag)
    g_ScriptEvent:BroadcastInLua("SyncPlayerKouDaoEliteRight",rightTag)
end


-- 有权限的打开界面后发送原有RPC,没有权限的打开界面后发送新的RPC
function Gas2Gac.OpenEliteKouDaoWnd(hasRight)
    CUIManager.ShowUI(CLuaUIResources.KouDaoJingYingSettingWnd)
end

-- 设置备注名成功
function Gas2Gac.SetMemberExtraNameSuccess(memberId, extraName)
    CLuaGuildMgr.m_ExtraNames[memberId] = extraName
    if extraName and extraName == "" then
        CLuaGuildMgr.m_ExtraNames[memberId] = nil
    end

    g_ScriptEvent:BroadcastInLua("SetMemberExtraNameSuccess",memberId,extraName)
end

function Gas2Gac.GuildNotification(power,value)
    --value 1:shine 0:not shine
    CGuildMgr.Inst.guildNotification = value == 1
    g_ScriptEvent:BroadcastInLua("GuildNotification",value == 1)

    if CUIManager.IsLoaded(CUIResources.GuildMainWnd) then
        CLuaGuildMgr.GetGuildMemberApplyInfo()
    end
end


function Gas2Gac.SendPaiMaiBonus(bonus)
    g_ScriptEvent:BroadcastInLua("SendPaiMaiBonus",bonus)
end

function Gas2Gac.SyncSelfBuildBonus(bonus, isThisWeekTaken, playStatus)
    g_ScriptEvent:BroadcastInLua("SyncSelfBuildBonus",bonus, isThisWeekTaken, playStatus)
end

function Gas2Gac.SyncGuildBuildBonus(bonus)
    -- EventManager.Broadcast(EnumEventType.GuildBuildBonusReceived, bonus);
    g_ScriptEvent:BroadcastInLua("GuildBuildBonusReceived",bonus)
end

function Gas2Gac.RequestSetGuildRightsSuccess()
    g_ScriptEvent:BroadcastInLua("RequestSetGuildRightsSuccess")
end


function Gas2Gac.CheckMyRightsResult(rightName, context, success)
    if rightName == "SetGuildCommander" then
        CGuildMgr.Inst.CanSetCCCommander = success
    end
    if rightName == "ForbidGuildSpeak" then
        CGuildMgr.Inst:UpdateMyRightOfForbiddenToSpeak(success)
    end
    if rightName == "CityBuild" then
        CGuildMgr.Inst.CanCityBuild = success
    end
    if rightName == "RebornPoint" then
        CGuildMgr.Inst.CanCityBuild = success
    end
    g_ScriptEvent:BroadcastInLua("OnQueryGuildRightsFinished",rightName, context, success)
end

function Gas2Gac.SendGuildDiningTableData(id, expireTime, refreshTimesDay, bFull, eatTimes, gridUD, topUD)
	local gridList = MsgPackImpl.unpack(gridUD)
	if gridList then
    local t = {}
    local count = 1
		for i = 0, gridList.Count - 1, 2 do
			local templateId = tonumber(gridList[i])
			local filledNum = tonumber(gridList[i+1])
      table.insert(t,{templateId,filledNum,count})
      count = count + 1
		end
    CLuaGuildMgr.guildBuffGridList = t
	end

	local topList = MsgPackImpl.unpack(topUD)
	if topList then
    local t = {}
		for i = 0, topList.Count - 1, 4 do
			local pos = tonumber(topList[i])
			local playerId = tonumber(topList[i+1])
			local playerName = topList[i+2]
			local score = tonumber(topList[i+3])
      table.insert(t,{pos, playerId, playerName, score})
		end
    CLuaGuildMgr.guildBuffTopList = t
	end

  CLuaGuildMgr.guildBuffExpireTime = expireTime
  CLuaGuildMgr.guildBuffBFull = bFull
  CLuaGuildMgr.guildBuffId = id
  CLuaGuildMgr.guildBuffEatTimes = eatTimes
  if CUIManager.IsLoaded(CLuaUIResources.GuildBuffDeskWnd) then
    g_ScriptEvent:BroadcastInLua("OnRefreshGuildDiningWnd")
  else
    CUIManager.ShowUI(CLuaUIResources.GuildBuffDeskWnd)
  end
end

function Gas2Gac.FillGuildDiningTableGridSuccess(id, gridIdx, templateId, bindedCount, unbindCount, filledCount, bFull, eatTimes)

  CLuaGuildMgr.guildBuffBFull = bFull
  CLuaGuildMgr.guildBuffId = id
  g_ScriptEvent:BroadcastInLua("OnFillGuildDiningTableGridSuccess",{templateId})
end

function Gas2Gac.DiningOnGuildDiningTableSuccess(eatTimes)
  CLuaGuildMgr.guildBuffEatTimes = eatTimes
  g_ScriptEvent:BroadcastInLua("OnDiningOnGuildDiningTableSuccess")
end

-- AppointMember,DismissMember,Resign,NeiWuFuOperation,AcceptApplyAsMember
-- 以上操作会发成员数据
function Gas2Gac.SendGuildMemberInfoAfterOperation(memberId, guildMemberInfoRpcUD, requestType, paramStr)
	-- print("SendGuildMemberInfoAfterOperation", memberId, requestType, paramStr)
    local info = CGuildMemberInfoRpc()
    info:LoadFromString(guildMemberInfoRpcUD)

    local guildMemberInfo = CGuildMgr.Inst.m_GuildMemberInfo
    if guildMemberInfo then
        CommonDefs.DictIterate(info.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            CommonDefs.DictRemove_LuaCall(guildMemberInfo.Infos,___key)
            CommonDefs.DictAdd_LuaCall(guildMemberInfo.Infos,___key,___value)
        end))
        CommonDefs.DictIterate(info.DynamicInfos, DelegateFactory.Action_object_object(function (___key, ___value)
            CommonDefs.DictRemove_LuaCall(guildMemberInfo.DynamicInfos,___key)
            CommonDefs.DictAdd_LuaCall(guildMemberInfo.DynamicInfos,___key,___value)
        end))
    end

    g_ScriptEvent:BroadcastInLua("SendGuildMemberInfoAfterOperation",memberId,info,requestType, paramStr)
end

-- ExpelMember
-- 以上操作会移除成员数据
function Gas2Gac.RemoveGuildMemberInfoAfterOperation(memberId, requestType, paramStr)
	-- print("RemoveGuildMemberInfoAfterOperation", memberId, requestType, paramStr)

    local guildMemberInfo = CGuildMgr.Inst.m_GuildMemberInfo
    if guildMemberInfo then
        CommonDefs.DictRemove_LuaCall(guildMemberInfo.Infos,memberId)
        CommonDefs.DictRemove_LuaCall(guildMemberInfo.DynamicInfos,memberId)
    end
    g_ScriptEvent:BroadcastInLua("RemoveGuildMemberInfoAfterOperation",memberId,requestType, paramStr)
end

-- AcceptApplyAsMember,RejectApplyAsMember,CleanApplyList
-- 以上操作会移除申请列表玩家，其中CleanApplyList的applayPlayerId是0
function Gas2Gac.RemoveGuildMemberApplyInfoAfterOperation(applyPlayerId, requestType, paramStr)
	-- print("RemoveGuildMemberApplyInfoAfterOperation", applyPlayerId, requestType, paramStr)
    if CLuaGuildMgr.m_GuildMemberApplyInfo then
        if applyPlayerId==0 then
            CLuaGuildMgr.m_GuildMemberApplyInfo = {}
        else
            CLuaGuildMgr.m_GuildMemberApplyInfo[applyPlayerId]=nil
        end
    end
    g_ScriptEvent:BroadcastInLua("RemoveGuildMemberApplyInfoAfterOperation",applyPlayerId,requestType,paramStr)
end

function Gas2Gac.SendAllGuildsCreationAlert(alertStatus)
	-- print("SendAllGuildsCreationAlert", tostring(alertStatus))
    g_ScriptEvent:BroadcastInLua("SendAllGuildsCreationAlert",alertStatus)
end

-- 帮会拼图
EnumGuildPinTuPlayStage = {
	eNotOpen = 0, -- 非玩法当天或当天开启倒计时前，暂未开放
	eCountDown = 1, -- 玩法当天提前半小时，开启倒计时
	eOpen = 2, -- 进行中
	eEnd = 3, -- 玩法当天，已结束（客户端显示）
}

-- 拼图进入界面
CLuaGuildMgr.m_PinTuStage = nil
CLuaGuildMgr.m_PinTuDuration = nil
CLuaGuildMgr.m_PinTuDataList = nil
-- 拼图详情数据
CLuaGuildMgr.m_PinTuDetailData = nil
CLuaGuildMgr.m_PinTuNeedOpenId = nil

function CLuaGuildMgr:OpenEnterWnd(stage, duration, pinTuDataList, bQueryFromClient)
    if #pinTuDataList == 0 and stage == EnumGuildPinTuPlayStage.eOpen then
        stage = EnumGuildPinTuPlayStage.eNotOpen
    end

    self.m_PinTuStage = stage
    self.m_PinTuDuration = duration
    self.m_PinTuDataList = pinTuDataList

    -- 打开界面
    if not CUIManager.IsLoaded(CLuaUIResources.GuildPuzzleEnterWnd) and bQueryFromClient then
        -- 判断是否已经拼完
        local finishTime = 0
        for i,v in ipairs(pinTuDataList) do
            if v.finishtime == nil or v.finishtime == 0 then
                finishTime = 0
                break
            elseif v.finishtime and v.finishtime > finishTime then
                finishTime = v.finishtime
            end
        end

        if finishTime > 0 then
            local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(finishTime)

            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Guild_Puzzle_Finshed_Open_Check", tostring(dateTime.Month), tostring(dateTime.Day)), function()
                CUIManager.ShowUI(CLuaUIResources.GuildPuzzleEnterWnd)
            end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        else
            CUIManager.ShowUI(CLuaUIResources.GuildPuzzleEnterWnd)
        end
    else
        g_ScriptEvent:BroadcastInLua("QueryGuildPinTuDataResult", stage, duration, pinTuDataList)
    end
end

function CLuaGuildMgr:OpenDetailWnd(id, getchips, setchips, answered, answer, finishtime, pinTuPlayerCount, chipIdList, answerCountdown)
    self.m_PinTuDetailData = {}
    self.m_PinTuDetailData.id = id
    self.m_PinTuDetailData.getchips = getchips
    self.m_PinTuDetailData.setchips = setchips
    self.m_PinTuDetailData.answered = answered
    self.m_PinTuDetailData.answer = answer
    self.m_PinTuDetailData.finishtime = finishtime
    self.m_PinTuDetailData.pinTuPlayerCount = pinTuPlayerCount
    self.m_PinTuDetailData.chipIdList = chipIdList
    self.m_PinTuDetailData.answerCountdown = answerCountdown

    -- 打开界面
    if not CUIManager.IsLoaded(CLuaUIResources.GuildPuzzleWnd) then
        CUIManager.ShowUI(CLuaUIResources.GuildPuzzleWnd)
    else
        g_ScriptEvent:BroadcastInLua("OpenGuildPinTuDetailWnd", id, getchips, setchips, answered, answer, finishtime, pinTuPlayerCount, chipIdList, answerCountdown)
    end
end