local CUIFx = import "L10.UI.CUIFx"
local UISprite = import "UISprite"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Camera = import "UnityEngine.Camera"
local Plane = import "UnityEngine.Plane"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local CWorldMapInfo = import "L10.UI.CWorldMapInfo"
local DelegateFactory  = import "DelegateFactory"
local CScene = import "L10.Game.CScene"
local CTrackMgr = import "L10.Game.CTrackMgr"
local UICamera = import "UICamera"
local CSharpResourceLoader= import "L10.Game.CSharpResourceLoader"
local CServerTimeMgr= import "L10.Game.CServerTimeMgr"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"

LuaJuDianBattleWorldMapRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "WorldMap", "WorldMap", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "SelfServerBtn", "SelfServerBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "ZhuDianBtn", "ZhuDianBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "CurrentMap", "CurrentMap", UITexture)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "HelpCamera", "HelpCamera", Camera)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "JuDianBattleMapItemRoot", "JuDianBattleMapItemRoot", CCommonLuaScript)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "CompleteBtn", "CompleteBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "DeleteBtn", "DeleteBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "MarkBtn", "MarkBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "MarkItem", "MarkItem", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "BackToBtn", "BackToBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "MarkFx", "MarkFx", CUIFx)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "JuDianBattleSceneOverviewRankRoot", "JuDianBattleSceneOverviewRankRoot", CCommonLuaScript)
RegistChildComponent(LuaJuDianBattleWorldMapRoot, "BottomLabel", "BottomLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_MapDataList")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_MapIdList")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_CurrentMapId")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_CurrentIndex")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_LastClickPos")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_IsTagMode")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_IsLocalMap")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_SwitchBtn")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_CanTag")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_MapId2Item")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_Stage")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_StageEndTime")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_TimeTick")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_TimeStamp")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_LocalMapData")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_SupplyMapId")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_PlayerCountList")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_IsShowPlayerCount")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_InfoLabelList")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_LabelShowTick")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_ShowDefaultInfoTime")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_ShowPlayerCountTime")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_NowShowTime")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_TeleportCdRemain")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_TeleportCdTick")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_LingQuanRefreshTime")
RegistClassMember(LuaJuDianBattleWorldMapRoot, "m_LingQuanTick")

function LuaJuDianBattleWorldMapRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.SelfServerBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelfServerBtnClick()
	end)


	
	UIEventListener.Get(self.ZhuDianBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuDianBtnClick()
	end)


	
	UIEventListener.Get(self.CompleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCompleteBtnClick()
	end)


	
	UIEventListener.Get(self.DeleteBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteBtnClick()
	end)


	
	UIEventListener.Get(self.MarkBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMarkBtnClick()
	end)


	
	UIEventListener.Get(self.BackToBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackToBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleWorldMapRoot:InitMap()

    self.TitleLabel.transform.parent.gameObject:SetActive(false)
    self.m_MapId2Item = {}
    local mapInfoList = CommonDefs.GetComponentsInChildren_Component_Type(self.WorldMap.transform, typeof(CWorldMapInfo))
    for i=0, mapInfoList.Length -1 do
        local mapInfo = mapInfoList[i]
        self.m_MapId2Item[mapInfo.TemplateId] = mapInfo.gameObject
    end

    if self.m_SwitchBtn == nil then
        self.m_SwitchBtn = self.transform.parent:Find("TablePanel/Table/SwitchButton").gameObject
    end

    self:SetShowMode(false, false)
    self.m_InfoLabelList = {} -- 存储当前各场景显示信息的Label，用于信息与人数交替显示
end

-- 设置地图模式
function LuaJuDianBattleWorldMapRoot:SetShowMode(isLocalMap, isTagMode)
    local isDailyMode = self.m_IsDailyMode

    self.MarkFx:DestroyFx()

    self.m_IsLocalMap = isLocalMap
    self.m_IsTagMode = isTagMode

    self.CurrentMap.gameObject:SetActive(isLocalMap)
    self.WorldMap.gameObject:SetActive(not isLocalMap)

    self.MarkBtn:SetActive(isLocalMap and not isTagMode and self.m_CanTag and not isDailyMode)
    self.DeleteBtn:SetActive(isLocalMap and not isTagMode and self.m_CanTag and not isDailyMode)
    self.CompleteBtn:SetActive(isLocalMap and isTagMode and self.m_CanTag and not isDailyMode)

    self.BackToBtn:SetActive(isLocalMap)
    self.m_SwitchBtn:SetActive(not isLocalMap)

    self.TitleLabel.transform.parent.gameObject:SetActive(not (isDailyMode and not self.m_IsLocalMap))

    if isDailyMode then
        self.TitleLabel.text = LocalString.GetString("[FFFE91]点击地图可直接传送")
    end

    if self.m_TimeTick then
        UnRegisterTick(self.m_TimeTick)
    end
    if self.m_TeleportCdTick then
        UnRegisterTick(self.m_TeleportCdTick)
    end
    if self.m_LingQuanTick then
        UnRegisterTick(self.m_LingQuanTick)
    end

    if isLocalMap then
        if isTagMode then
            self.TitleLabel.text = LocalString.GetString("[FFFE91]点击地图添加标记（旗子及据点附近无法标记）")
        -- else
        --     self.TitleLabel.text = LocalString.GetString("[FFFE91]点击地图任意位置可直接传送")
        end
    elseif self.m_StageEndTime then
        self.m_TimeStamp = self.m_StageEndTime - CServerTimeMgr.Inst.timeStamp
        self:UpdataTimeTick()

        self.m_TimeTick = RegisterTick(function()
            self:UpdataTimeTick()
        end, 1000)
    end

    self.JuDianBattleMapItemRoot:SetShowModeItem(isLocalMap, isTagMode)
    self.BottomLabel.gameObject:SetActive(false)
end

function LuaJuDianBattleWorldMapRoot:UpdataTimeTick()
    self.m_TimeStamp = self.m_TimeStamp - 1
    if self.m_TimeStamp < 0 then
        self.m_TimeStamp = 0
    end

    local mins = math.floor(self.m_TimeStamp/60)
    local secs = math.floor((self.m_TimeStamp% 60))

    if self.m_Stage == EnumJuDianPlayStage.ePrepare then
        self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]入场准备中，将于%02d:%02d后开始"), mins, secs)
    elseif self.m_Stage == EnumJuDianPlayStage.eInFight then
        self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("[FFFE91]人间棋局进行中%02d:%02d"), mins, secs)
    else
        self.TitleLabel.text = LocalString.GetString("")
    end

end

function LuaJuDianBattleWorldMapRoot:OnWorldMapData(battleFieldId, currentStage, statusEndTime, mapList)
    self.m_Stage = currentStage
    self.m_StageEndTime = statusEndTime
    self.m_MapDataList = mapList
    self:InitMap()

    for id, item in pairs(self.m_MapId2Item) do
        local data = mapList[id]
        self:InitMapItem(item, data, id)
    end
end

function LuaJuDianBattleWorldMapRoot:InitMapItem(item, mapData, mapid)
    local state = EnumJuDianBattleMapState.NotChosen

    local hasTag = false
    if mapData then
        state = mapData.state
        hasTag = mapData.tag
    end

    -- 测试
    -- state = EnumJuDianBattleMapState.Taken
    -- mapData = {}
    -- mapData.guildId = 123
    -- mapData.guildName = "123"
    -- mapData.supplyEndTime = 123

    local selfGuildId = LuaJuDianBattleMgr:GetSelfGuildId()

    local leaderMark = item.transform:Find("BattleInfo/LeaderMark").gameObject
    local mainplayerTexture = item.transform:Find("BattleInfo/MainplayerTexture"):GetComponent(typeof(CUITexture))

    -- 状态
    local battleInfo = item.transform:Find("BattleInfo").gameObject
    battleInfo:SetActive(state ~= EnumJuDianBattleMapState.NotChosen)

    local battleState = item.transform:Find("BattleInfo/StateRoot/BattleState").gameObject
    local selfOccupyState = item.transform:Find("BattleInfo/StateRoot/SelfOccupyState").gameObject
    local otherOccupyState = item.transform:Find("BattleInfo/StateRoot/OtherOccupyState").gameObject
    local namelessOccupyState = item.transform:Find("BattleInfo/StateRoot/NamelessOccupyState").gameObject
    local monsterState = item.transform:Find("BattleInfo/StateRoot/MonsterState").gameObject
    local lingquanState = item.transform:Find("BattleInfo/StateRoot/LingQuanState").gameObject
    local mapTexture = item.transform:Find("Texture"):GetComponent(typeof(UISprite))

    leaderMark:SetActive(false)
    mainplayerTexture.gameObject:SetActive(false)
    battleState:SetActive(false)
    selfOccupyState:SetActive(false)
    otherOccupyState:SetActive(false)
    namelessOccupyState:SetActive(false)
    monsterState:SetActive(false)
    lingquanState:SetActive(false)

    if hasTag then
        leaderMark:SetActive(true)
    end

    local dailyMap = 0
    local idMap = GuildOccupationWar_DailySetting.GetData().Battle2DailyMapId
    if CommonDefs.DictContains(idMap, typeof(UInt32), mapid) then
        dailyMap = idMap[mapid]
    end

    -- 玩家头像
    if CScene.MainScene and (CScene.MainScene.SceneTemplateId == mapid or CScene.MainScene.SceneTemplateId == dailyMap) then
		mainplayerTexture.gameObject:SetActive(true)
        if CClientMainPlayer.Inst then
            local path = CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1)
            mainplayerTexture:LoadNPCPortrait(path, false)
        end
    end

    local infoLabel = nil
    if state == EnumJuDianBattleMapState.NotTaken then
        battleState:SetActive(true)
        infoLabel = battleState.transform:Find("Label"):GetComponent(typeof(UILabel))
    elseif state == EnumJuDianBattleMapState.Taken then
        -- 本帮占领
        if mapData.guildId  == selfGuildId then
            selfOccupyState:SetActive(true)
            infoLabel = selfOccupyState.transform:Find("Label"):GetComponent(typeof(UILabel))
            infoLabel.text = mapData.guildName
        else
            -- 外帮占领
            otherOccupyState:SetActive(true)
            infoLabel = otherOccupyState.transform:Find("Label"):GetComponent(typeof(UILabel))
            infoLabel.text = mapData.guildName
        end
    elseif state == EnumJuDianBattleMapState.Supply then
        UnRegisterTick(self.m_NpcTimeTick)

        namelessOccupyState:SetActive(true)
        infoLabel = namelessOccupyState.transform:Find("Label"):GetComponent(typeof(UILabel))

        self.m_SupplyMapId = mapid
        self.m_NpcStateLabel = namelessOccupyState.transform:Find("Label"):GetComponent(typeof(UILabel))
        self.m_NpcStateLabel.gameObject:SetActive(true)
        self.m_NpcTimeStamp = mapData.supplyEndTime - CServerTimeMgr.Inst.timeStamp
        self.m_NpcShow = mapData.bShow
        self:UpdataNpcTimeTick()

        self.m_NpcTimeTick = RegisterTick(function()
            self:UpdataNpcTimeTick()
        end, 1000)
    elseif state == EnumJuDianBattleMapState.Monster then
        monsterState:SetActive(true)
        infoLabel = monsterState.transform:Find("Label"):GetComponent(typeof(UILabel))
        infoLabel.text = LocalString.GetString("×") .. mapData.monsterCount
    elseif state == EnumJuDianBattleMapState.LingQuan then -- 新增灵泉
        if mapid ~= self.m_SupplyMapId then
            lingquanState:SetActive(true)
            infoLabel = lingquanState.transform:Find("Label"):GetComponent(typeof(UILabel))
            infoLabel.text = LocalString.GetString("×") .. mapData.lingquanCount
        else
            namelessOccupyState:SetActive(true)
        end
    end
    if infoLabel then
        self.m_InfoLabelList[mapid] = {infoLabel, infoLabel.text}
        if LuaJuDianBattleMgr.SeasonData and LuaJuDianBattleMgr.SeasonData.playStage == EnumJuDianPlayStage.ePrepare then
            infoLabel.transform.parent.gameObject:SetActive(false)
        end
    end

    if state == EnumJuDianBattleMapState.NotChosen then
        CUICommonDef.SetActive(item, false, true)
        mapTexture.color = Color(1,1,1,0.5)
    else
        mapTexture.color = Color(1,1,1,1)
        CUICommonDef.SetActive(item, true, true)
        -- 按钮点击
        UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
            -- 传送按钮
            if CScene.MainScene and CScene.MainScene.SceneTemplateId == mapid then
                g_MessageMgr:ShowMessage("GuildTerritorialWarsMapWnd_TeleportToCurPos")
            else
                self:InitLocalMap(mapid, 1)
            end
        end)
    end
end

-- 外围玩法
function LuaJuDianBattleWorldMapRoot:OnDailyData(data)
    self:InitMap()

    self.m_DailyData = LuaJuDianBattleMgr.SceneMonsterData

    local idMap = GuildOccupationWar_DailySetting.GetData().Battle2DailyMapId

    for id, item in pairs(self.m_MapId2Item) do

        if CommonDefs.DictContains(idMap, typeof(UInt32), id) then
            local dailyId = idMap[id]
            local monsterList = self.m_DailyData[dailyId]
            local data = {}
            print("OnDailyData", id, dailyId, monsterList)
            if monsterList then
                if #monsterList > 0 then
                    data.state = EnumJuDianBattleMapState.Monster
                    data.monsterCount = #monsterList
                end
            else
                data.state = EnumJuDianBattleMapState.NotChosen
            end

            self:InitMapItem(item, data, dailyId)
        end
    end

    self.CompleteBtn.gameObject:SetActive(false)
    self.DeleteBtn.gameObject:SetActive(false)
    self.MarkBtn.gameObject:SetActive(false)
    self.MarkItem.gameObject:SetActive(false)

    self.m_IsDailyMode = true
end


function LuaJuDianBattleWorldMapRoot:UpdataNpcTimeTick()
    if self.m_NpcTimeStamp < 0 then
        self.m_NpcTimeStamp = 0
    end

    local mins = math.floor(self.m_NpcTimeStamp/60)
    local secs = math.floor((self.m_NpcTimeStamp% 60))

    local str = nil
    if self.m_NpcTimeStamp == 0 then
        str = LocalString.GetString("无名神像")
    elseif self.m_NpcShow then
        str = SafeStringFormat3(LocalString.GetString("无名神像[ACF8FF]%02d:%02d[-]后消失"), mins, secs)
    else
        str = SafeStringFormat3(LocalString.GetString("无名神像[ACF8FF]%02d:%02d[-]后出现"), mins, secs)
    end
    if self.m_InfoLabelList[self.m_SupplyMapId] then self.m_InfoLabelList[self.m_SupplyMapId][2] = str end
    if not self.m_IsShowPlayerCount then self.m_NpcStateLabel.text = str end

    self.m_NpcTimeStamp = self.m_NpcTimeStamp - 1
end

-- 加载其他地图
function LuaJuDianBattleWorldMapRoot:LoadOtherLocalMap(scenename, mapid)
    -- 加载资源
    CSharpResourceLoader.Inst:LoadMiniMapData(scenename, DelegateFactory.Action_CMiniMapData(function(mapData)
        self:LoadMapDataCallback(mapData, scenename, mapid)
    end))
end

function LuaJuDianBattleWorldMapRoot:LoadMapDataCallback(mapData, scenename, mapid)
    UIEventListener.Get(self.CurrentMap.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnClickOtherMap()
    end)

    -- 可能存在load完资源之前 rpc就下发的情况
    local data = PublicMap_PublicMap.GetData(self.m_CurrentMapId)
    if data and data.Res == scenename and mapid == self.m_CurrentMapId then
        self.CurrentMap.mainTexture = mapData.m_MapTexture
        self.HelpCamera.transform.position = mapData.m_HelpCameraPos
        self.HelpCamera.transform.eulerAngles = mapData.m_HelpCameraRotation
        self.HelpCamera.orthographicSize = mapData.m_HelpCameraSize
        self.HelpCamera.aspect = 16.0 / 9.0

        if self.m_LocalMapData and self.m_LocalMapData[1] == mapid  then
            g_ScriptEvent:BroadcastInLua("GuildJuDianSyncMapSceneInfo", self.m_LocalMapData[1], self.m_LocalMapData[2], self.m_LocalMapData[3], 
                self.m_LocalMapData[4], self.m_LocalMapData[5], self.m_LocalMapData[6], self.m_LocalMapData[7], nil)
        end
    end

    if self.m_IsDailyMode then
        local data = PublicMap_PublicMap.GetData(self.m_CurrentMapId)
        if data and data.Res == scenename and mapid == self.m_CurrentMapId then
            self.CurrentMap.mainTexture = mapData.m_MapTexture
            self.HelpCamera.transform.position = mapData.m_HelpCameraPos
            self.HelpCamera.transform.eulerAngles = mapData.m_HelpCameraRotation
            self.HelpCamera.orthographicSize = mapData.m_HelpCameraSize
            self.HelpCamera.aspect = 16.0 / 9.0
            local monsterList = self.m_DailyData[self.m_CurrentDailyMapId]
            self.JuDianBattleMapItemRoot:InitDailyMap(self.m_CurrentDailyMapId, monsterList)
            self:SetShowMode(true, false)
        end
    end
end

-- 初始化当前地图显示
function LuaJuDianBattleWorldMapRoot:InitLocalMap(mapid, index)
    self.m_CurrentMapId = mapid
    self.m_CurrentIndex = index

    local idMap = GuildOccupationWar_DailySetting.GetData().Battle2DailyMapId
    local dailyMapId = mapid

    if CommonDefs.DictContains(idMap, typeof(UInt32), mapid) then
        dailyMapId = idMap[mapid]
    end
    self.m_CurrentDailyMapId = dailyMapId

    -- 加载资源
    local data = PublicMap_PublicMap.GetData(mapid)
    if data then
        self:LoadOtherLocalMap(data.Res, mapid)
    end

    if not LuaJuDianBattleMgr:IsInTaskGameplay() then
        if self.m_IsDailyMode then
            local monsterList = self.m_DailyData[dailyMapId]
            self.JuDianBattleMapItemRoot:InitDailyMap(dailyMapId, monsterList)
        else
            -- 请求
            self.JuDianBattleMapItemRoot:InitMap(mapid, index)
        end
    else
        CGuideMgr.Inst:StartGuide(193, 0)
    end
    self:SetShowMode(true, false)
end

-- 设置标记按钮
function LuaJuDianBattleWorldMapRoot:OnLocalMapData(mapId, sceneIdx, sceneId, tag, flagList, juDianList, npcList, teleportCdRemain)
    self.m_LocalMapData = {}
    self.m_LocalMapData[1] = mapId
    self.m_LocalMapData[2] = sceneIdx
    self.m_LocalMapData[3] = sceneId
    self.m_LocalMapData[4] = tag
    self.m_LocalMapData[5] = flagList
    self.m_LocalMapData[6] = juDianList
    self.m_LocalMapData[7] = npcList

	-- 有权利标记
	self.MarkBtn:SetActive(self.m_CanTag)

	-- 删除节点
	self.DeleteBtn:SetActive(tag and self.m_CanTag)

	-- 标记
	if tag then
		self:SetMarkItem(tag, true)
	else
		self.MarkItem:SetActive(false)
	end

    local data = PublicMap_PublicMap.GetData(mapId)

    self.m_CurrentIndex = sceneIdx
    self.m_CurrentMapId = mapId

    local idMap = GuildOccupationWar_DailySetting.GetData().Battle2DailyMapId
    if CommonDefs.DictContains(idMap, typeof(UInt32), mapId) then
        self.m_CurrentDailyMapId = idMap[mapId]
    end

    self:SetShowMode(true, false)
    self:OnTeleportCdRemain(teleportCdRemain)
end


-- 地图点击事件处理
function LuaJuDianBattleWorldMapRoot:GetClickPos()
    local worldPos = UICamera.lastWorldPosition
    local localPos = self.CurrentMap.transform:InverseTransformPoint(worldPos)

    local viewPos = Vector3(localPos.x / self.CurrentMap.width, localPos.y / self.CurrentMap.height, 0)
    viewPos.x = viewPos.x + 0.5
    viewPos.y = viewPos.y + 0.5
    viewPos.z = 0

    local ray = self.HelpCamera:ViewportPointToRay(viewPos)
    local mapHeight = 0
    local heightDict = GuildOccupationWar_Setting.GetData().SceneAverageHeight
    if CommonDefs.DictContains(heightDict, typeof(UInt32), self.m_CurrentMapId) then
        mapHeight = heightDict[self.m_CurrentMapId]
    end
    local plane = CreateFromClass(Plane, Vector3.up, Vector3(0,mapHeight,0))
    local point = CommonDefs.GetRayCastPoint(plane, ray)
    local pixelPos = Utility.WorldPos2PixelPos(point)

    localPos.z = 0
    return pixelPos, localPos
end

function LuaJuDianBattleWorldMapRoot:GetMapPos(pixelPos)
    local worldPos = Utility.GridPos2WorldPos(pixelPos.x, pixelPos.y)

    -- 高度补偿
    local heightDict = GuildOccupationWar_Setting.GetData().SceneAverageHeight
    if CommonDefs.DictContains(heightDict, typeof(UInt32), self.m_CurrentMapId) then
        worldPos.y = heightDict[self.m_CurrentMapId]
    end

    local viewPos = self.HelpCamera:WorldToViewportPoint(worldPos)
    viewPos.x = viewPos.x - 0.5
    viewPos.y = viewPos.y - 0.5
    viewPos.z = 0

    return Vector3(self.CurrentMap.width * viewPos.x, self.CurrentMap.height * viewPos.y, 0);
end

function LuaJuDianBattleWorldMapRoot:OnClickOtherMap()
    local pixelPos, localPos = self:GetClickPos()

    self.MarkFx.transform.localPosition = localPos
    self.MarkFx:DestroyFx()
    self.MarkFx:LoadFx("fx/ui/prefab/UI_arrow_sign.prefab")

    if self.m_IsTagMode then
        self.m_LastClickPos = pixelPos

        self.MarkItem:SetActive(true)
        self.MarkItem.transform.localPosition = localPos
    else
        local x = math.floor(pixelPos.x/64)
        local y = math.floor(pixelPos.y/64)

        if x <0 then
            x = 0
        end
        if y< 0 then
            y =0
        end

        if LuaJuDianBattleMgr:IsInTaskGameplay() then
            Gac2Gas.GuildJuDianTeleportToGuideScene()
        elseif not self.m_IsDailyMode then
            Gac2Gas.GuildJuDianRequestTeleportToPosition(self.m_CurrentMapId, self.m_CurrentIndex, x, y)
        else
            Gac2Gas.GuildJuDianRequestTeleportToDailyScene(self.m_CurrentDailyMapId, x, y)
        end
    end
end

function LuaJuDianBattleWorldMapRoot:SetMarkItem(pos, active)
    self.MarkItem:SetActive(active)
    if active then
        local mapPos = self:GetMapPos(pos)
        self.MarkItem.transform.localPosition = mapPos
    end
end

-- 设置位置
-- 会被子物体调用
function LuaJuDianBattleWorldMapRoot:SetLocation(item, data)
	local mapPos = self:GetMapPos(data)
	item.transform.localPosition = mapPos
end

-- 设置位置
function LuaJuDianBattleWorldMapRoot:OnQualification(canTag, canKick)
	-- 有权利标记
	self.m_CanTag = canTag
	-- 刷新显示
	self:SetShowMode(self.m_IsLocalMap, self.m_IsTagMode)
end

-- 同步灵泉数量
function LuaJuDianBattleWorldMapRoot:OnLingQuanCountData(lingQuanCountList)
    for i = 1, #lingQuanCountList, 2 do
        local id = lingQuanCountList[i]
        local data = {}
        data.state = EnumJuDianBattleMapState.LingQuan
        data.hasTag = false
        data.lingquanCount = lingQuanCountList[i + 1]
        self:InitMapItem(self.m_MapId2Item[id], data, id)
	end
end

-- 灵泉刷新时间
function LuaJuDianBattleWorldMapRoot:OnLingQuanRefreshTime(nextRefreshTime)
    if self.m_LingQuanTick then UnRegisterTick(self.m_LingQuanTick) end
    self.m_LingQuanRefreshTime = nextRefreshTime
    self:UpdataLingQuanTick()
    if self.m_LingQuanRefreshTime > CServerTimeMgr.Inst.timeStamp then
        self.m_LingQuanTick = RegisterTick(function()
            self:UpdataLingQuanTick()
        end, 1000)
    end
end

function LuaJuDianBattleWorldMapRoot:UpdataLingQuanTick()
	local remainTime = self.m_LingQuanRefreshTime - CServerTimeMgr.Inst.timeStamp
	self.BottomLabel.gameObject:SetActive(remainTime > 0)
    if remainTime > 0 then
        local mins = math.floor(remainTime / 60)
        local secs = math.floor((remainTime % 60))
        self.BottomLabel.text = SafeStringFormat3(LocalString.GetString("%d分%d秒后灵泉刷新"), mins, secs)
    end
end

-- 同步场景人数，打开地图时服务器自动推送
function LuaJuDianBattleWorldMapRoot:OnPlayerCountData(playerCountList)
    self.m_PlayerCountList = {}
    for i = 1, #playerCountList, 2 do
        self.m_PlayerCountList[playerCountList[i]] = playerCountList[i + 1]
    end
    -- 据点战地图信息与人数交替显示
    local setData = GuildOccupationWar_Setting.GetData()
    self.m_ShowDefaultInfoTime = setData.MapShowDefaultInfoTime
    self.m_ShowPlayerCountTime = setData.MapShowPlayerCountTime
    self.m_NowShowTime = 0
    if self.m_LabelShowTick then UnRegisterTick(self.m_LabelShowTick) end
    self.m_LabelShowTick = RegisterTick(function()
        self:UpdataLabelShowTick()
    end, 500)
end

-- 交替显示场景信息与人数
function LuaJuDianBattleWorldMapRoot:UpdataLabelShowTick()
    local showInfo = LuaJuDianBattleMgr.SeasonData and LuaJuDianBattleMgr.SeasonData.playStage ~= EnumJuDianPlayStage.ePrepare
    -- 准备阶段不显示信息
    for id, label in pairs(self.m_InfoLabelList) do
        self.m_InfoLabelList[id][1].transform.parent.gameObject:SetActive(showInfo)
    end
    if showInfo then
        self.m_NowShowTime = self.m_NowShowTime + 0.5
        if not self.m_IsShowPlayerCount and self.m_NowShowTime >= self.m_ShowDefaultInfoTime then
            self.m_NowShowTime = 0
            self.m_IsShowPlayerCount = true
            for id, count in pairs(self.m_PlayerCountList) do
                if self.m_InfoLabelList[id] then
                    self.m_InfoLabelList[id][1].text = SafeStringFormat3(LocalString.GetString("%d人"), count)
                end
            end
        elseif self.m_IsShowPlayerCount and self.m_NowShowTime >= self.m_ShowPlayerCountTime then
            self.m_NowShowTime = 0
            self.m_IsShowPlayerCount = false
            for id, count in pairs(self.m_PlayerCountList) do
                if self.m_InfoLabelList[id] then
                    self.m_InfoLabelList[id][1].text = self.m_InfoLabelList[id][2]
                end
            end
        end
    end
end

-- 死亡后传送CD
function LuaJuDianBattleWorldMapRoot:OnTeleportCdRemain(teleportCdRemain)
    if teleportCdRemain then
        if self.m_TeleportCdTick then UnRegisterTick(self.m_TeleportCdTick) end
        self.m_TeleportCdRemain = teleportCdRemain
        self:UpdataTeleportCdTick()
        if teleportCdRemain >= 0 then
            -- 死亡后传送Cd Tick
            self.m_TeleportCdTick = RegisterTick(function()
                self:UpdataTeleportCdTick()
            end, 1000)
        end
    end
end

function LuaJuDianBattleWorldMapRoot:UpdataTeleportCdTick()
    if self.m_TeleportCdRemain > 0 then
        self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("[FF5050]还剩%d秒可以进行传送"), self.m_TeleportCdRemain)
    elseif self.m_TeleportCdRemain == 0 then
        self.TitleLabel.text = LocalString.GetString("[FFFE91]点击地图任意位置可直接传送")
    end
    self.m_TeleportCdRemain = self.m_TeleportCdRemain - 1
end

function LuaJuDianBattleWorldMapRoot:OnEnable()
    self:InitMap()
    self:OnWorldMapData(1,1,1, {})

    if LuaJuDianBattleMgr:IsInTaskGameplay() then
        self.SelfServerBtn.gameObject:SetActive(false)
        self.ZhuDianBtn.gameObject:SetActive(false)
        self.CompleteBtn.gameObject:SetActive(false)
        self.DeleteBtn.gameObject:SetActive(false)
        self.MarkBtn.gameObject:SetActive(false)
        self.MarkItem.gameObject:SetActive(false)
        self.BackToBtn.gameObject:SetActive(false)
        self.JuDianBattleMapItemRoot.gameObject:SetActive(false)

        for id, item in pairs(self.m_MapId2Item) do
            if id == 16102311 then
                local data = {}
                self:InitMapItem(item, data, id)
            end
        end
    else
        -- 请求据点世界地图数据
        Gac2Gas.GuildJuDianQuerySceneOverview()
        Gac2Gas.GuildJuDianQuerySetQualification()
    end

    if not LuaJuDianBattleMgr:IsInGameplay() then
        self.JuDianBattleSceneOverviewRankRoot.gameObject:SetActive(false)
    end

    self.m_Action = DelegateFactory.Action_uint(function(guideId)
        if guideId == 193 then
            Gac2Gas.GuildJuDianTeleportToGuideScene()
        end
    end)
    EventManager.AddListenerInternal(EnumEventType.GuideEnd, self.m_Action)

    self.m_LocalMapData = nil
    g_ScriptEvent:AddListener("JuDianBattle_SyncDailySceneData", self, "OnDailyData")
    g_ScriptEvent:AddListener("GuildJuDianReplySceneOverview", self, "OnWorldMapData")
    g_ScriptEvent:AddListener("GuildJuDianSyncMapSceneInfo", self, "OnLocalMapData")
    g_ScriptEvent:AddListener("GuildJuDianSyncPlayerCountByMapId", self, "OnServerData")
    g_ScriptEvent:AddListener("GuildJuDianSyncSetQualification", self, "OnQualification")
    g_ScriptEvent:AddListener("GuildJuDianSyncMapPlayerCountAll", self, "OnPlayerCountData")
    g_ScriptEvent:AddListener("GuildJuDianSyncMapLingQuanCount", self, "OnLingQuanCountData")
end

function LuaJuDianBattleWorldMapRoot:OnDisable()
    if self.m_TimeTick then
        UnRegisterTick(self.m_TimeTick)
    end
    UnRegisterTick(self.m_NpcTimeTick)
    UnRegisterTick(self.m_LabelShowTick)
    UnRegisterTick(self.m_TeleportCdTick)
    UnRegisterTick(self.m_LingQuanTick)
    self.m_LocalMapData = nil

    EventManager.RemoveListenerInternal(EnumEventType.GuideEnd, self.m_Action)

    g_ScriptEvent:RemoveListener("JuDianBattle_SyncDailySceneData", self, "OnDailyData")
    g_ScriptEvent:RemoveListener("GuildJuDianReplySceneOverview", self, "OnWorldMapData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncMapSceneInfo", self, "OnLocalMapData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncPlayerCountByMapId", self, "OnServerData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncSetQualification", self, "OnQualification")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncMapPlayerCountAll", self, "OnPlayerCountData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncMapLingQuanCount", self, "OnLingQuanCountData")
end

--@region UIEvent

function LuaJuDianBattleWorldMapRoot:OnSelfServerBtnClick()
    LuaJuDianBattleMgr:RequestBackSelfSever()
end

function LuaJuDianBattleWorldMapRoot:OnZhuDianBtnClick()
    -- 回到驻点
    Gac2Gas.GuildJuDianRequestGoToOccupation()
end


function LuaJuDianBattleWorldMapRoot:OnCompleteBtnClick()
    if self.m_LastClickPos then
        print("Gac2Gas.GuildJuDianRequestTagPosition")
        Gac2Gas.GuildJuDianRequestTagPosition(self.m_CurrentMapId, self.m_CurrentIndex, math.floor(self.m_LastClickPos.x/64), math.floor(self.m_LastClickPos.y/64))
    end
    self:SetShowMode(true, false)
end

function LuaJuDianBattleWorldMapRoot:OnDeleteBtnClick()
    -- 删除标记
    self:SetMarkItem(nil, false)
    -- 取消标记点
    Gac2Gas.GuildJuDianRequestTagPosition(self.m_CurrentMapId, self.m_CurrentIndex, 0, 0)
end

function LuaJuDianBattleWorldMapRoot:OnMarkBtnClick()
    self:SetShowMode(true, true)
end


function LuaJuDianBattleWorldMapRoot:OnBackToBtnClick()
    self:SetShowMode(not self.m_IsLocalMap, false)
end


--@endregion UIEvent

