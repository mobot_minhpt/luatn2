local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local CUITexture = import "L10.UI.CUITexture"
local EnumQualityType = import "L10.Game.EnumQualityType"
local NGUITools = import "NGUITools"
local CBaseItemInfoRow = import "L10.UI.CBaseItemInfoRow"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"
local UIRect = import "UIRect"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CChatLinkMgr = import "CChatLinkMgr"

LuaFashionInfoWnd = class()

-- @region RegistChildComponent: Dont Modify Manually!

-- @endregion RegistChildComponent end
RegistClassMember(LuaFashionInfoWnd, "m_Wnd")
RegistClassMember(LuaFashionInfoWnd, "background")
RegistClassMember(LuaFashionInfoWnd, "table")
RegistClassMember(LuaFashionInfoWnd, "iconTexture")
RegistClassMember(LuaFashionInfoWnd, "nameLabel")
RegistClassMember(LuaFashionInfoWnd, "availableLevelLabel")
RegistClassMember(LuaFashionInfoWnd, "categoryLabel")
RegistClassMember(LuaFashionInfoWnd, "qualitySprite")
RegistClassMember(LuaFashionInfoWnd, "bindSprite")
RegistClassMember(LuaFashionInfoWnd, "disableSprite")
RegistClassMember(LuaFashionInfoWnd, "infoBtn")
RegistClassMember(LuaFashionInfoWnd, "labelTemplate")
RegistClassMember(LuaFashionInfoWnd, "contentTable")
RegistClassMember(LuaFashionInfoWnd, "contentScrollView")
RegistClassMember(LuaFashionInfoWnd, "eyeBtn")
RegistClassMember(LuaFashionInfoWnd, "minContentWidth")
RegistClassMember(LuaFashionInfoWnd, "maxContentWidth")
RegistClassMember(LuaFashionInfoWnd, "ValueColor")
RegistClassMember(LuaFashionInfoWnd, "buttonsBg")
RegistClassMember(LuaFashionInfoWnd, "baseFashionInfo")
RegistClassMember(LuaFashionInfoWnd, "m_PaperDurationLimit")

function LuaFashionInfoWnd:Awake()
    -- @region EventBind: Dont Modify Manually!
    -- @endregion EventBind end
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.background = self.transform:Find("Table/Background"):GetComponent(typeof(UISprite))
    self.table = self.transform:Find("Table"):GetComponent(typeof(UITable))
    self.iconTexture = self.transform:Find("Table/Background/Header/ItemCell/IconTexture"):GetComponent(typeof(
        CUITexture))
    self.nameLabel = self.transform:Find("Table/Background/Header/ItemNameLabel"):GetComponent(typeof(UILabel))
    self.availableLevelLabel = self.transform:Find("Table/Background/Header/AvailableLevelLabel"):GetComponent(typeof(
        UILabel))
    self.categoryLabel = self.transform:Find("Table/Background/Header/CategoryLabel"):GetComponent(typeof(UILabel))
    self.qualitySprite = self.transform:Find("Table/Background/Header/ItemCell/QualitySprite"):GetComponent(typeof(
        UISprite))
    self.bindSprite = self.transform:Find("Table/Background/Header/ItemCell/BindSprite"):GetComponent(typeof(UISprite))
    self.disableSprite = self.transform:Find("Table/Background/Header/ItemCell/DisableSprite"):GetComponent(typeof(
        UISprite))
    self.infoBtn = self.transform:Find("Table/Background/Header/InfoButton").gameObject
    self.labelTemplate = self.transform:Find("Table/Background/Templates/LabelTemplate").gameObject
    self.contentTable = self.transform:Find("Table/Background/ContentScrollView/Table"):GetComponent(typeof(UITable))
    self.contentScrollView = self.transform:Find("Table/Background/ContentScrollView")
        :GetComponent(typeof(UIScrollView))
    self.eyeBtn = self.transform:Find("Table/Background/ContentScrollView/EyeButton").gameObject
    self.minContentWidth = 512
    self.maxContentWidth = 600
    self.ValueColor = NGUIText.ParseColor24("E8D0AA", 0)
    self.buttonsBg = self.transform:Find("Table/Background/ButtonsBg"):GetComponent(typeof(UIWidget))
    self.m_PaperDurationLimit = self.transform:Find("Table/Background/Header/ItemCell/PaperDuration"):GetComponent(
        typeof(UILabel))
end

function LuaFashionInfoWnd:Init()
    self.m_PaperDurationLimit.gameObject:SetActive(false)
    self.infoBtn.gameObject:SetActive(false)
    self.eyeBtn:SetActive(false)
    self.buttonsBg.gameObject:SetActive(false)

    self.baseFashionInfo = LuaFashionInfoMgr.m_FashionItemInfo
    self:Init0(self.baseFashionInfo.templateId)
end

-- @region UIEvent

-- @endregion UIEvent

function LuaFashionInfoWnd:Init0(fashionId)
    self.iconTexture:Clear()
    self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    self.bindSprite.spriteName = ""
    self.disableSprite.enabled = false
    local fashionData = Fashion_Fashion.GetData(fashionId)
    if fashionData ~= nil then
        self.iconTexture:LoadMaterial(fashionData.Icon)
        self.nameLabel.text = fashionData.Name
        self.availableLevelLabel.text = nil
        -- 类型
        self.categoryLabel.text = System.String.Format("[c][FFFFFF]{0}[-][/c]", LocalString.GetString("时装"))
        self.qualitySprite.spriteName = LuaAppearancePreviewMgr:GetFashionQualityBorder(fashionData.Quality)
        self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(fashionData.Description), self.ValueColor)
    end

    self:LayoutWnd()
end

function LuaFashionInfoWnd:AddItemToTable(text, color)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.labelTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = text
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).color = color
end

function LuaFashionInfoWnd:LayoutWnd()
    local maxWidth = 0
    do
        local i = 0
        while i < self.contentTable.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.contentTable.transform:GetChild(i),
                typeof(CBaseItemInfoRow))
            if row ~= nil then
                local rowMaxWidth = row:GetMaxContentWidth()
                if maxWidth < rowMaxWidth then
                    maxWidth = rowMaxWidth
                end
            end
            i = i + 1
        end
    end

    local actualContentWidth = math.min(math.max(maxWidth, self.minContentWidth), self.maxContentWidth)

    do
        local i = 0
        while i < self.contentTable.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.contentTable.transform:GetChild(i),
                typeof(CBaseItemInfoRow))
            if row ~= nil then
                row:SetContentWidth(actualContentWidth)
            end
            i = i + 1
        end
    end

    self.contentTable:Reposition()

    self.background.width = math.ceil(actualContentWidth + math.abs(self.contentScrollView.panel.leftAnchor.absolute) +
                                          math.abs(self.contentScrollView.panel.rightAnchor.absolute))

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(self.contentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(self.contentScrollView.panel.bottomAnchor.absolute)

    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding +
                               self.contentScrollView.panel.clipSoftness.y * 2
    local displayWndHeight = math.min(math.max(totalWndHeight, contentTopPadding + contentBottomPadding + 10),
        virtualScreenHeight)

    -- 设置背景高度
    self.background.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.transform, typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors ==
                UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end

    self.contentScrollView:ResetPosition()

    self.background.transform.localPosition = Vector3.zero
    self.table:Reposition()

    if self.baseFashionInfo ~= nil then
        self:AdjustPosition(self.baseFashionInfo.alignType, self.baseFashionInfo.targetCenterPos, self.baseFashionInfo.targetSize)
    end
end

function LuaFashionInfoWnd:TotalHeightOfScrollViewContent()
    return NGUIMath.CalculateRelativeWidgetBounds(self.contentTable.transform).size.y + self.contentTable.padding.y * 2
end

function LuaFashionInfoWnd:AdjustPosition(type, worldPos, targetSize)
    local selfBounds
    repeat
        local default = type
        if default == CItemInfoMgr.AlignType.Default then
            self.table.transform.localPosition = Vector3.zero
            break
        elseif default == CItemInfoMgr.AlignType.Left then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(
                self.table.transform.parent:InverseTransformPoint(worldPos).x - targetSize.x * 0.5 - selfBounds.size.x *
                    0.5, self.table.transform.parent:InverseTransformPoint(worldPos).y + targetSize.y * 0.5 -
                    selfBounds.size.y * 0.5, 0)
            break
        elseif default == CItemInfoMgr.AlignType.Right then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(
                self.table.transform.parent:InverseTransformPoint(worldPos).x + targetSize.x * 0.5 + selfBounds.size.x *
                    0.5, self.table.transform.parent:InverseTransformPoint(worldPos).y + targetSize.y * 0.5 -
                    selfBounds.size.y * 0.5, 0)
            break
        elseif default == CItemInfoMgr.AlignType.Top then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(self.table.transform.parent:InverseTransformPoint(worldPos).x,
                self.table.transform.parent:InverseTransformPoint(worldPos).y + targetSize.y * 0.5 + selfBounds.size.y *
                    0.5, 0)
            break
        elseif default == CItemInfoMgr.AlignType.ScreenLeft then
            do
                local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
                local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
                self.table.transform.localPosition = Vector3(-virtualScreenWidth * 0.25, 0, 0)
            end
            break
        elseif default == CItemInfoMgr.AlignType.ScreenRight then
            do
                local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
                local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
                self.table.transform.localPosition = Vector3(virtualScreenWidth * 0.25, 0, 0)
            end
            break
        end
    until 1

    -- 限制在屏幕范围内
    local selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
    local wp = self.table.transform.localPosition
    self.table.transform.localPosition = CUICommonDef.ConstrainToScreenArea(wp.x, wp.y, selfBounds.size.x,
        selfBounds.size.y)
end

function LuaFashionInfoWnd:Update()
    self.m_Wnd:ClickThroughToClose()
end
