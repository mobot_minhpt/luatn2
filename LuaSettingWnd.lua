local CBaseWnd = import "L10.UI.CBaseWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CScene = import "L10.Game.CScene"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CUITexture = import "L10.UI.CUITexture"
local CPerformanceMgr = import "L10.Engine.CPerformanceMgr"
local LocalString = import "LocalString"
local CSettingInfoMgr = import "L10.UI.CSettingInfoMgr"
local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local ConstProp = import "NtUniSdk.Unity3d.ConstProp"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local UIGrid = import "UIGrid"
local DelegateFactory = import "DelegateFactory"
local C3DTouchMgr = import "L10.UI.C3DTouchMgr"
local EventManager = import "EventManager"
local CUnlockScreenMgr = import "L10.UI.CUnlockScreenMgr"
local String = import "System.String"
local Object = import "System.Object"
local Json = import "L10.Game.Utils.Json"
local UIScrollView = import "UIScrollView"
local NGUITools = import "NGUITools"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local NativeTools = import "L10.Engine.NativeTools"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Application = import "UnityEngine.Application"
local ChannelDefine=import "L10.Game.ChannelDefine"
local Main = import "L10.Engine.Main"
local CButton = import "L10.UI.CButton"

LuaSettingWnd = class()

RegistClassMember(LuaSettingWnd, "m_CloseButton")

--InfoSettings Unity Component
RegistClassMember(LuaSettingWnd, "m_SavePowerButton")
RegistClassMember(LuaSettingWnd, "m_AccountName")
RegistClassMember(LuaSettingWnd, "m_ServerName")
RegistClassMember(LuaSettingWnd, "m_PortraitTexture")
RegistClassMember(LuaSettingWnd, "m_QrCodeBtn")
RegistClassMember(LuaSettingWnd, "m_BindBtn")
RegistClassMember(LuaSettingWnd, "m_QnButtonUserCenter")
RegistClassMember(LuaSettingWnd, "m_ContactButton")
RegistClassMember(LuaSettingWnd, "m_Table")
RegistClassMember(LuaSettingWnd, "m_UserCenterAlert")
RegistClassMember(LuaSettingWnd, "m_ChangeAccountButton")
RegistClassMember(LuaSettingWnd, "m_LockScreenButton")

RegistClassMember(LuaSettingWnd, "m_HideTwoButton")

RegistClassMember(LuaSettingWnd, "m_ButtonTemplate")
RegistClassMember(LuaSettingWnd, "m_ButtonTable")
RegistClassMember(LuaSettingWnd, "m_ButtonScrollView")
RegistClassMember(LuaSettingWnd, "m_WindowScrollView")

RegistClassMember(LuaSettingWnd, "m_ButtonList")
RegistClassMember(LuaSettingWnd, "m_WindowDict")
RegistClassMember(LuaSettingWnd, "m_CurSelectIndex")
RegistClassMember(LuaSettingWnd, "m_WindowsIndexList")

RegistClassMember(LuaSettingWnd, "m_OnScannerMessageDelegate")
RegistClassMember(LuaSettingWnd, "m_CombineCodeScannerOnEventDelegate")
RegistClassMember(LuaSettingWnd, "m_CombineCodeScannerOnDecoderMessageDelegate")
RegistClassMember(LuaSettingWnd, "m_OnNtQRCodeScannerMessageDelegate")
RegistClassMember(LuaSettingWnd, "m_OnUnisdkOnExtendFuncCallDelegate")

function LuaSettingWnd:Awake()
    self.m_CloseButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
    self.m_HideTwoButton = true
    self:InitComponents()
end

function LuaSettingWnd:Start()
    CommonDefs.AddOnClickListener(
            self.m_QrCodeBtn.gameObject, DelegateFactory.Action_GameObject(
                    function(go) self:OnClickQRCodeButton() end),
            true)

    CommonDefs.AddOnClickListener(
        self.m_BindBtn.gameObject, DelegateFactory.Action_GameObject(
                function(go) self:OnClickBindButton() end),
        true)
end

function LuaSettingWnd:Init()
    self.m_AccountName.text = CClientMainPlayer.Inst.Name
    local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
    self.m_ServerName.text = gameServer and gameServer.name or nil
    self.m_PortraitTexture:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName,false)
    self:UpdateSavePowerButtonText()

    self.m_QrCodeBtn.Visible = (CommonDefs.IsAndroidPlatform() and not CommonDefs.IS_LEBIAN_YUN_CLIENT and not NativeTools.IsAllChannelApk()) or CommonDefs.IsIOSPlatform()
    self.m_BindBtn.Visible = CommonDefs.IsIOSPlatform() and CommonDefs.IS_HMT_CLIENT and CLoginMgr.Inst.UniSdk_LoginDone and SdkU3d.getAuthTypeName() == "Apple"

    if CommonDefs.IS_CN_CLIENT then
        self.m_QnButtonUserCenter:SetActive(CLoginMgr.Inst.m_UniSdkLoginInfo.UniSdk_ChannelName == "netease")
        self.m_ContactButton.Visible = CContactAssistantMgr.Inst:ContactAssitantOpenVipCheck()
    elseif CommonDefs.IS_KR_CLIENT then
        self.m_QnButtonUserCenter:SetActive(true)
        self.m_ContactButton.Visible = SdkU3d.getAuthTypeName() == ConstProp.NT_AUTH_NAME_GUEST
    elseif CommonDefs.IS_HMT_CLIENT then
        self.m_QnButtonUserCenter:SetActive(true)
        self.m_UserCenterAlert:SetActive(false);
        self.m_ContactButton.Visible = false
    elseif CommonDefs.IS_VN_CLIENT then
        self.m_ContactButton.Visible = true
        self.m_QrCodeBtn.Visible = false
        self.m_QnButtonUserCenter:SetActive(false)
    elseif CommonDefs.IS_XM_CLIENT then
        self.m_SavePowerButton.Visible = false
    end
    self.m_Table:Reposition()

    self:InitSettingList()

    self.m_ContactButton.OnClick =  DelegateFactory.Action_QnButton(function(btn)
        self:OnContactButtonClick()
    end)
    self.m_ChangeAccountButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnChangeAccountButtonClick()
    end)
    self.m_SavePowerButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSavePowerButtonClick()
    end)
    self.m_LockScreenButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnLockScreenButtonClick()
    end)
    self.m_LockScreenButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnLockScreenButtonClick()
    end)
    CommonDefs.AddOnClickListener(
            self.m_QnButtonUserCenter, DelegateFactory.Action_GameObject(
                    function(go) self:OnUserCenterBtnClick() end),
            false)
end

function LuaSettingWnd:InitSettingList()
    self.m_ButtonList = {}
    self.m_WindowsIndexList = {}
    local buttonsDetail ={
        {title = LocalString.GetString("基础设置"), windowIndex = "BasicSetting"},
        {title = LocalString.GetString("画面设置"), windowIndex = "RenderSetting"},
        {title = LocalString.GetString("语音设置"), windowIndex = "VoiceSetting"},
        {title = LocalString.GetString("其他设置"), windowIndex = "OtherSetting"}
    }

    if CommonDefs.IsPCGameMode() then
        table.insert( buttonsDetail,4, {title = LocalString.GetString("键位设置"), windowIndex = "ShortCutKeySetting"})
    end
    
    self.m_CurSelectIndex = (CSettingInfoMgr.TabIndex >= 0 or CSettingInfoMgr.TabIndex < #buttonsDetail) and CSettingInfoMgr.TabIndex + 1 or 1

    Extensions.RemoveAllChildren(self.m_ButtonTable.transform)


    if(CSwitchMgr.EnableSecondPwd) then
        table.insert(buttonsDetail,{title = LocalString.GetString("二级密码"), windowIndex = "SecondPasswordSetting"})
    end

    if CommonDefs.IS_CN_CLIENT then 
        table.insert(buttonsDetail, {title = LocalString.GetString("个性化服务"), windowIndex = "PersonalizationSetting"})
    end

    if self:DoubleListChannelJudgment() then
        table.insert(buttonsDetail, {title = LocalString.GetString("已收集个人信息清单"), windowIndex = "CollectedPersonalInformationList"})
        table.insert(buttonsDetail, {title = LocalString.GetString("第三方共享个人信息清单"), windowIndex = "ThirdServiceShareList"})
    end

    if self:IsPrivacyCollectInfoButtonEnabled() then
        table.insert(buttonsDetail, {title = self:GetPrivacyCollectionInfoButtonText(), windowIndex = "PrivacySetting"})
    end

    table.insert(buttonsDetail, {title = LocalString.GetString("切换语言"), windowIndex = "ChangeLanguageSetting"})


    for k ,v in pairs(buttonsDetail) do
        self:AddButton(v.title, v.windowIndex)
    end

    self.m_ButtonTable:Reposition()
    self.m_ButtonScrollView:ResetPosition()

    self:OnClick(self.m_ButtonList[self.m_CurSelectIndex])
end

function LuaSettingWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaSettingWnd:OnEnable()

    if not CommonDefs.IS_HMT_CLIENT and not CommonDefs.IS_XM_CLIENT then
        g_ScriptEvent:AddListener("OnDarenUpdated", self, "OnDarenUpdated")
        CLoginMgr.Inst:UniSdkLsDarenUpdated()
        self:UpdateUserCenterAlert(CLoginMgr.Inst.m_bShowDarenUpdated)
    end
    self:AddCodeScan()

	if self.m_OnUnisdkOnExtendFuncCallDelegate == nil then
		self.m_OnUnisdkOnExtendFuncCallDelegate = DelegateFactory.Action_int_Dictionary_string_object(function(code, data)
			if not CommonDefs.DictContains_LuaCall(data, "methodId") or CommonDefs.DictGetValue_LuaCall(data, "methodId") ~= "showPersonalInfoList" then
				return
			end
			if not CommonDefs.DictContains_LuaCall(data, "channel") or CommonDefs.DictGetValue_LuaCall(data, "channel") ~= "personal_info_list" then
				return
			end

			if CommonDefs.DictContains_LuaCall(data, "errorCode") and CommonDefs.DictGetValue_LuaCall(data, "errorCode") == 1 then
				local errorMsg = CommonDefs.DictContains_LuaCall(data, "errorMsg") and CommonDefs.DictGetValue_LuaCall(data, "errorMsg") or ""
				if System.String.IsNullOrEmpty(errorMsg) then
					g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("打开失败"))
				else
					g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("打开失败，") .. errorMsg)
				end
			end
		end)
		EventManager.AddListenerInternal(EnumEventType.UnisdkOnExtendFuncCall, self.m_OnUnisdkOnExtendFuncCallDelegate)
	end
end

function LuaSettingWnd:OnDisable()
    if not CommonDefs.IS_HMT_CLIENT and not CommonDefs.IS_XM_CLIENT then
        g_ScriptEvent:RemoveListener("OnDarenUpdated", self, "OnDarenUpdated")
    end
    self:ClearCodeScan()
	if self.m_OnUnisdkOnExtendFuncCallDelegate then
		EventManager.RemoveListenerInternal(EnumEventType.UnisdkOnExtendFuncCall, self.m_OnUnisdkOnExtendFuncCallDelegate)
		self.m_OnUnisdkOnExtendFuncCallDelegate = nil
	end
end

function LuaSettingWnd:InitComponents()
    self.m_SavePowerButton = self.transform:Find("InfoSettings/Grid/QnButtonSavePower"):GetComponent(typeof(QnButton))
    self.m_AccountName = self.transform:Find("InfoSettings/Account"):GetComponent(typeof(UILabel))
    self.m_ServerName = self.transform:Find("InfoSettings/Server/ServerName"):GetComponent(typeof(UILabel))
    self.m_PortraitTexture = self.transform:Find("InfoSettings/Portait"):GetComponent(typeof(CUITexture))
    self.m_QrCodeBtn = self.transform:Find("InfoSettings/QrCodeBtn"):GetComponent(typeof(QnButton))
    self.m_BindBtn = self.transform:Find("InfoSettings/BindBtn"):GetComponent(typeof(QnButton))
    self.m_QnButtonUserCenter = self.transform:Find("InfoSettings/Grid/QnButtonUserCenter"):GetComponent(typeof(QnButton)).gameObject
    self.m_ContactButton = self.transform:Find("InfoSettings/Grid/ContactButton"):GetComponent(typeof(QnButton))
    self.m_Table = self.transform:Find("InfoSettings/Grid"):GetComponent(typeof(UIGrid))
    self.m_UserCenterAlert = self.m_QnButtonUserCenter.transform:Find("alert").gameObject
    self.m_ChangeAccountButton = self.transform:Find("InfoSettings/Grid/QnButtonChangeAccount"):GetComponent(typeof(QnButton))
    self.m_LockScreenButton = self.transform:Find("InfoSettings/Grid/QnButtonLockScreen"):GetComponent(typeof(QnButton))

    self.m_ButtonTemplate = FindChild(self.transform,"ButtonTemplate").gameObject
    self.m_ButtonTemplate:SetActive(false)
    self.m_ButtonTable = self.transform:Find("TabButtons/ScrollView/Table"):GetComponent(typeof(UIGrid))
    self.m_ButtonScrollView = self.transform:Find("TabButtons/ScrollView"):GetComponent(typeof(UIScrollView))

    local tabWindowsRoot = self.transform:Find("TabWindows/ScorllView")
    local childCount = tabWindowsRoot.childCount
    self.m_WindowDict = {}
    for i = 1, childCount do
        local window = tabWindowsRoot:GetChild(i - 1)
        self.m_WindowDict[window.name] = window
    end
    self.m_WindowDict["ShortCutKeySetting"] = self.transform:Find("TabWindows/ShortCutKeySetting")

    for k,v in pairs(self.m_WindowDict) do
        v.gameObject:SetActive(false)
    end
    self.m_WindowScrollView = tabWindowsRoot:GetComponent(typeof(UIScrollView))
end

function LuaSettingWnd:UpdateSavePowerButtonText()
    if CPerformanceMgr.Inst.EnableSavePowerMode then
        self.m_SavePowerButton.Text = LocalString.GetString("性能模式")
    else
        self.m_SavePowerButton.Text = LocalString.GetString("省电模式")
    end
end

function LuaSettingWnd:OnContactButtonClick()
    if CommonDefs.IS_KR_CLIENT then
        SdkU3d.ntGuestBind()
    elseif CommonDefs.IS_VN_CLIENT then
        if CommonDefs.IsPCGameMode() then
            CWebBrowserMgr.Inst:OpenUrl("https://support.vnggames.com/products/ghost-story-784-dbf7302138e5769a51e96dd9f88864b0/problems")
        else
            if CClientMainPlayer.Inst then
                local content = SafeStringFormat3("Customer Service. roleId:%s, vipLevel:%s", CClientMainPlayer.Inst.Id, CClientMainPlayer.Inst.ItemProp.Vip.Level)
                local jsonStr = SafeStringFormat3("{\"methodId\":\"showCustomerSupport\",\"roleName\":\"%s\",\"level\":\"%s\",\"guild\":\"%s\",\"content\":\"%s\",\"serverId\":\"%s\"}",
                        CClientMainPlayer.Inst.Name, CClientMainPlayer.Inst.MaxLevel, CClientMainPlayer.Inst.BasicProp.GuildId,content, CClientMainPlayer.Inst:GetMyServerId())
                SdkU3d.ntExtendFunc(jsonStr)
            end
        end
    else
        CContactAssistantMgr.Inst:ShowContactAssistantWndByQuickEntrance()
    end
end

function LuaSettingWnd:OnChangeAccountButtonClick()
    if CommonDefs.IS_KR_CLIENT and (CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform()) then
        CLoginMgr.Inst:OnManagerButtonClick()
    else
        CLoginMgr.Inst:OnSwitchAccount()
        self.m_ChangeAccountButton.Enabled = false
        if CommonDefs.IsIOSPlatform() and CommonDefs.IS_CN_CLIENT then
            C3DTouchMgr.Inst:ClearStatus()
            LuaDeepLinkMgr:ClearInGameAction()
        end
    end
end

function LuaSettingWnd:OnSavePowerButtonClick()
    if self.m_SavePowerButton.Text == LocalString.GetString("省电模式") then
        CPerformanceMgr.Inst.EnableSavePowerMode = true
    else
        CPerformanceMgr.Inst.EnableSavePowerMode = false
    end
    EventManager.BroadcastInternalForLua(EnumEventType.ReloadSettings, {})
    self:UpdateSavePowerButtonText()
end

function LuaSettingWnd:OnLockScreenButtonClick()
    CUnlockScreenMgr.Inst:ShowUnlockScreenWnd()
end

function LuaSettingWnd:OnUserCenterBtnClick()
    if CommonDefs.IS_KR_CLIENT then
        CLoginMgr.Inst:UniSdkUploadUserInfo()
        SdkU3d.ntEFunGmView()
    else
        CLoginMgr.Inst:OnManagerButtonClick()
    end
end

function LuaSettingWnd:OnDarenUpdated(args)
    local visible = args[0]
    self:UpdateUserCenterAlert(visible)
end

function LuaSettingWnd:UpdateUserCenterAlert(visible)
    self.m_UserCenterAlert.gameObject:SetActive(visible)
end

function LuaSettingWnd:ClearCodeScan()
	if self.m_OnNtQRCodeScannerMessageDelegate then
		EventManager.RemoveListenerInternal(EnumEventType.OnScannerMessage, self.m_OnNtQRCodeScannerMessageDelegate)
		self.m_OnNtQRCodeScannerMessageDelegate = nil
	end

    if self.m_OnScannerMessageDelegate then
        CommonDefs.CombineCodeScannerOnMessage(self.m_OnScannerMessageDelegate, false)
        self.m_OnScannerMessageDelegate = nil
    end
    if self.m_CombineCodeScannerOnEventDelegate then
        CommonDefs.CombineCodeScannerOnEvent(self.m_CombineCodeScannerOnEventDelegate, false)
        self.m_CombineCodeScannerOnEventDelegate = nil
    end
    if self.m_CombineCodeScannerOnDecoderMessageDelegate then
        CommonDefs.CombineCodeScannerOnDecoderMessage(self.m_CombineCodeScannerOnDecoderMessageDelegate, false)
        self.m_CombineCodeScannerOnDecoderMessageDelegate = nil
    end
end

function LuaSettingWnd:AddCodeScan()
	if self.m_OnNtQRCodeScannerMessageDelegate == nil then
		self.m_OnNtQRCodeScannerMessageDelegate = DelegateFactory.Action_string(function (data)
			self:OnScannerMessage(data)
		end)
		EventManager.AddListenerInternal(EnumEventType.OnScannerMessage, self.m_OnNtQRCodeScannerMessageDelegate)
	end

    if self.m_OnScannerMessageDelegate == nil then
        self.m_OnScannerMessageDelegate = DelegateFactory.Action_string(function (data)
            self:OnScannerMessage(data)
        end)
        CommonDefs.CombineCodeScannerOnMessage(self.m_OnScannerMessageDelegate, true)
    end

    if self.m_CombineCodeScannerOnEventDelegate == nil then
        self.m_CombineCodeScannerOnEventDelegate = DelegateFactory.Action_string(function (data)
            self:OnScannerEvent(data)
        end)
        CommonDefs.CombineCodeScannerOnEvent(self.m_CombineCodeScannerOnEventDelegate,  true)
    end

    if self.m_CombineCodeScannerOnDecoderMessageDelegate == nil then
        self.m_CombineCodeScannerOnDecoderMessageDelegate = DelegateFactory.Action_string(function (data)
            self:OnDecoderMessage(data)
        end)
        CommonDefs.CombineCodeScannerOnDecoderMessage(self.m_CombineCodeScannerOnDecoderMessageDelegate,  true)
    end
end

function LuaSettingWnd:OnScannerMessage(data)
    if System.String.IsNullOrEmpty(data) then
        return
    end
    --如果碰到链接则直接打开
    if StringStartWith(data, "http") then
        CommonDefs.OpenURL(data)
        return
    end
    --否则尝试扫码登录
    local dict = TypeAs(L10.Game.Utils.Json.Deserialize(data), typeof(MakeGenericClass(Dictionary, String, Object)))
    if dict ~= nil then
        local ret = CreateFromClass(MakeGenericClass(Dictionary, String, String))
        CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (___key, ___value)
            local kv = {}
            kv.Key = ___key
            kv.Value = ___value
            CommonDefs.DictAdd(ret, typeof(String), kv.Key, typeof(String), ToStringWrap(kv.Value))
        end))
        CLoginMgr.Inst:OnScanQrcode(ret)
    end
end

function LuaSettingWnd:OnScannerEvent(data)

end

function LuaSettingWnd:OnDecoderMessage(data)
    --如果碰到链接则直接打开
    if StringStartWith(data, "http") then
        CommonDefs.OpenURL(data)
        return
    end
    --否则尝试扫码登录
    local dict = TypeAs(Json.Deserialize(data), typeof(MakeGenericClass(Dictionary, String, Object)))
    local ret = CreateFromClass(MakeGenericClass(Dictionary, String, String))
    CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (___key, ___value)
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        CommonDefs.DictAdd(ret, typeof(String), kv.Key, typeof(String), ToStringWrap(kv.Value))
    end))
    CLoginMgr.Inst:OnScanQrcode(ret)
end

function LuaSettingWnd:OnClickQRCodeButton()
    LuaLoginMgr.OnClickQRCodeButton()
end

function LuaSettingWnd:OnClickBindButton()
    SdkU3d.ntGuestBind()
end

function LuaSettingWnd:AddButton(title, windowIndex)
    local button = NGUITools.AddChild(self.m_ButtonTable.gameObject, self.m_ButtonTemplate)
    button:SetActive(true)
    local qnButton = button:GetComponent(typeof(CButton))
    qnButton.Text = title

    table.insert(self.m_ButtonList, qnButton)
    table.insert(self.m_WindowsIndexList, windowIndex)
    if windowIndex == "PrivacySetting" then
        UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function() self:ShowPrivacyCollectInfo() end)
    elseif windowIndex == "CollectedPersonalInformationList" then
        UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function() self:ShowCollectedPersonalInformationList() end)
    elseif windowIndex == "ThirdServiceShareList" then
        UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function() self:ShowThirdServiceShareList() end)
    elseif windowIndex == "ChangeLanguageSetting" then
        UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function() self:OnChangeLanguageButtonClick() end)    
    else
        UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(btn) self:OnClick(btn) end)
    end
end

function LuaSettingWnd:OnClick(btn)
    for i,v in ipairs(self.m_ButtonList) do
        if v.gameObject == btn then
            self.m_CurSelectIndex = i
        else
            local window = self.m_WindowDict[self.m_WindowsIndexList[i]]
            if window then window.gameObject:SetActive(false) end
            self.m_ButtonList[i].Selected = false
        end
    end

    self.m_WindowDict[self.m_WindowsIndexList[self.m_CurSelectIndex]].gameObject:SetActive(true)
    self.m_ButtonList[self.m_CurSelectIndex].Selected = true
    self.m_WindowScrollView:ResetPosition()
end

function LuaSettingWnd:IsPrivacyCollectInfoButtonEnabled()
    return CommonDefs.IS_CN_CLIENT
end

-- 隐私政策
function LuaSettingWnd:GetPrivacyCollectionInfoButtonText()
    return LocalString.GetString("隐私政策")
end

function LuaSettingWnd:GetPlatformMappingStr()
    local platformList = {}
    platformList[RuntimePlatform.Android] = "a"             -- 安卓
    platformList[RuntimePlatform.IPhonePlayer] = "i"        -- 苹果
    platformList[RuntimePlatform.WindowsPlayer] = "p"       -- 桌面端

    return platformList[Application.platform]
end

function LuaSettingWnd:OnChangeLanguageButtonClick()
    CUIManager.ShowUI("ChooseLanguageWnd")
end

function LuaSettingWnd:ShowPrivacyCollectInfo()
    -- 打开隐私政策
    local platformMappingStr = self:GetPlatformMappingStr()
    if platformMappingStr == nil then return end
    local str = "{\"gameid\":\"l10\",\"app_channel\":\"%s\",\"platform\":\"%s\"}"
    local jsonStr = SafeStringFormat3(str, CLoginMgr.Inst:GetAppChannel(), platformMappingStr)
    local data = CommonDefs.Convert_ToBase64String(CommonDefs.UTF8Encoding_GetBytes(jsonStr))
    data = string.gsub(data, "+", "-")
    data = string.gsub(data, "/", "_")
    data = string.gsub(data, "=", "")
    local url = SafeStringFormat3("https://protocol.unisdk.netease.com/api/template/v90/latest.html?data=%s", data)
    if Main.Inst.EngineVersion > 522390 and (CommonDefs.IsIOSPlatform() or CommonDefs.IsAndroidPlatform()) and not NativeTools.IsAllChannelApk() then
        CWebBrowserMgr.OpenNgWebView(url)
    else
        CWebBrowserMgr.Inst:OpenUrl(url)
    end
end

-- 双清单
function LuaSettingWnd:DoubleListChannelJudgment()
	if not CommonDefs.IsAndroidPlatform() and not CommonDefs.IsIOSPlatform() then
		return
	end
	-- 全渠道包不能开
	if NativeTools.IsAllChannelApk() then
		return
	end
	local engineVersion = Main.Inst.EngineVersion
	if engineVersion > 0 and engineVersion < 521780 then
		return
	end

	if not SdkU3d.hasFeature("MODE_HAS_PERSONALINFOLIST") then
		return
	end

	return true
end

function LuaSettingWnd:ShowCollectedPersonalInformationList()
    -- 打开已收集个人信息清单
	local jsonStr = "{\"methodId\": \"showPersonalInfoList\", \"channel\": \"personal_info_list\"}"
	SdkU3d.ntExtendFunc(jsonStr)
end

function LuaSettingWnd:ShowThirdServiceShareList()
    -- 打开第三方服务共享清单
    local platformMappingStr = self:GetPlatformMappingStr()
    if platformMappingStr == nil then return end
    local str = "{\"gameid\":\"l10\",\"app_channel\":\"%s\",\"platform\":\"%s\"}"
    local jsonStr = SafeStringFormat3(str, CLoginMgr.Inst:GetAppChannel(), platformMappingStr)
    local data = CommonDefs.Convert_ToBase64String(CommonDefs.UTF8Encoding_GetBytes(jsonStr))
    data = string.gsub(data, "+", "-")
    data = string.gsub(data, "/", "_")
    data = string.gsub(data, "=", "")
    local url = SafeStringFormat3("https://unisdk.update.netease.com/tpsl/html/sdk_list.html?data=%s", data)
    if Main.Inst.EngineVersion > 522390 and (CommonDefs.IsIOSPlatform() or CommonDefs.IsAndroidPlatform()) and not NativeTools.IsAllChannelApk() then
        CWebBrowserMgr.OpenNgWebView(url)
    else
        CWebBrowserMgr.Inst:OpenUrl(url)
    end
end

function LuaSettingWnd:GetGuideGo(methodName)
    if methodName=="GetCloseButton" then
        return self.m_CloseButton
    end
end
