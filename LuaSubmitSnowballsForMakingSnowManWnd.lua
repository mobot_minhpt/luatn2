local UISlider = import "UISlider"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CButton = import "L10.UI.CButton"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"

LuaSubmitSnowballsForMakingSnowManWnd = class()

RegistChildComponent(LuaSubmitSnowballsForMakingSnowManWnd,"m_ArrowRootGo","ArrowRoot", GameObject)
RegistChildComponent(LuaSubmitSnowballsForMakingSnowManWnd,"m_Slider","Slider", UISlider)
RegistChildComponent(LuaSubmitSnowballsForMakingSnowManWnd,"m_NumOfSnowBallLabel","NumOfSnowBallLabel", UILabel)
RegistChildComponent(LuaSubmitSnowballsForMakingSnowManWnd,"m_QnIncreseAndDecreaseButton","QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaSubmitSnowballsForMakingSnowManWnd,"m_OwnNumLabel","OwnNumLabel", UILabel)
RegistChildComponent(LuaSubmitSnowballsForMakingSnowManWnd,"m_SubmitButton","SubmitButton", CButton)
RegistChildComponent(LuaSubmitSnowballsForMakingSnowManWnd,"m_BallRootGo","BallRoot", GameObject)

RegistClassMember(LuaSubmitSnowballsForMakingSnowManWnd,"m_CurChooseNum")

 function LuaSubmitSnowballsForMakingSnowManWnd:Init()
     UIEventListener.Get(self.m_SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
         self:Submit()
     end)

     self.m_QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(v)
        self.m_CurChooseNum = v
     end)
     self.m_QnIncreseAndDecreaseButton:SetValue(1)
     self:ReloadData(HanJia2020Mgr.submitSnowBallState)
 end

function LuaSubmitSnowballsForMakingSnowManWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2020_OnSubmitSnowballResult", self, "OnSubmitSnowballResult")
end

function LuaSubmitSnowballsForMakingSnowManWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2020_OnSubmitSnowballResult", self, "OnSubmitSnowballResult")
end

function LuaSubmitSnowballsForMakingSnowManWnd:Submit()
    local num = self.m_QnIncreseAndDecreaseButton:GetValue()
    local maxNum = self:GetNumOfSnowBall()

    if num <= maxNum and num > 0 then
        Gac2Gas.RequestSubmitSnowball(HanJia2020Mgr.unfinishedSnowManID, num)
    end

    if num == 0 then
        g_MessageMgr:ShowMessage("HanJia2020_Submit_ZeroSnowBalls")
    end
end

function LuaSubmitSnowballsForMakingSnowManWnd:ReloadData(state)
    --箭头二阶段才完全亮起
    self.m_ArrowRootGo.transform:GetChild(1):GetComponent(typeof(UITexture)).alpha = (state < 2) and 0.5 or 1 
    --雪人图标二阶段才完全亮起
    self.m_BallRootGo.transform:GetChild(2):GetComponent(typeof(UITexture)).alpha = (state < 2) and 0.5 or 1

    ---更新Slider
    local maxNum = self:GetNumOfSnowBall() --拥有的雪球
    self.m_OwnNumLabel.text = maxNum
    local rest = HanJia2020Mgr.maxSubmitSnowBalllCount - HanJia2020Mgr.curSubmitSnowBallCount
    maxNum = maxNum < rest and maxNum or rest
    self.m_QnIncreseAndDecreaseButton:SetMinMax(1, maxNum, 1)
    self.m_QnIncreseAndDecreaseButton:SetValue(maxNum)


    self.m_Slider.value = HanJia2020Mgr.curSubmitSnowBallCount / HanJia2020Mgr.maxSubmitSnowBalllCount
    self.m_NumOfSnowBallLabel.text = HanJia2020Mgr.curSubmitSnowBallCount .. "/" ..HanJia2020Mgr.maxSubmitSnowBalllCount

    self.m_SubmitButton.Enabled = HanJia2020Mgr.curSubmitSnowBallCount ~= HanJia2020Mgr.maxSubmitSnowBalllCount
end

function LuaSubmitSnowballsForMakingSnowManWnd:OnSubmitSnowballResult(data)
    self:ReloadData(HanJia2020Mgr.submitSnowBallState)
    local maxNum = self:GetNumOfSnowBall()
    local rest = HanJia2020Mgr.maxSubmitSnowBalllCount - HanJia2020Mgr.curSubmitSnowBallCount
    maxNum = maxNum < rest and maxNum or rest
    self.m_QnIncreseAndDecreaseButton:SetValue((self.m_CurChooseNum and self.m_CurChooseNum < maxNum) and self.m_CurChooseNum or 0)
end


function LuaSubmitSnowballsForMakingSnowManWnd:GetNumOfSnowBall()
    return CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, HanJia2020_Setting.GetData().SnowBallItemID)
end
