local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local NGUITools = import "NGUITools"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CButton = import "L10.UI.CButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"


LuaCharacterCardWnd = class()
RegistChildComponent(LuaCharacterCardWnd,"closeBtn", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"tab1", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"tab1Normal", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"tab1Light", GameObject)

RegistChildComponent(LuaCharacterCardWnd,"tab2", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"tab2Normal", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"tab2Light", GameObject)

RegistChildComponent(LuaCharacterCardWnd,"templateNode", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"fNode", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"bonusNode", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"num1", UILabel)
RegistChildComponent(LuaCharacterCardWnd,"num2", UILabel)
RegistChildComponent(LuaCharacterCardWnd,"decomposeBtn", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"compoundBtn", GameObject)
RegistChildComponent(LuaCharacterCardWnd,"totalnum", UILabel)


RegistClassMember(LuaCharacterCardWnd, "pageDataTable")
RegistClassMember(LuaCharacterCardWnd, "nowChooseIndex")
RegistClassMember(LuaCharacterCardWnd, "cardInfo")

function LuaCharacterCardWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CharacterCardWnd)
end

function LuaCharacterCardWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateCharacterCardInfo", self, "Init")
end

function LuaCharacterCardWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateCharacterCardInfo", self, "Init")
end

function LuaCharacterCardWnd:OperateFunc(row,id)
	if row == 0 then
		--local msg = g_MessageMgr:FormatMessage('')
		local data = RoleDrawingCard_CardItem2Idx.GetData(id)
		local item = Item_Item.GetData(data.CardItemId)
		local itemgen = Item_Item.GetData(RoleDrawingCard_Setting.GetData().ComposeMaterialId)
		local count = 1
		local msg = SafeStringFormat3(LocalString.GetString('是否将%s张%s分解为%s个%s'),count,item.Name,RoleDrawingCard_Setting.GetData().DecomposeCount * count,itemgen.Name)

		local cost =  RoleDrawingCard_Setting.GetData().DecomposeCost * count
		QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinLiang, msg, cost, DelegateFactory.Action(function ( ... )
			Gac2Gas.RequestDecomposeOneRdcCard(id)
		end), nil, false, true, EnumPlayScoreKey.NONE, false)
	elseif row == 1 then
		local count = LuaCharacterCardMgr.cardInfo[id].count

		CLuaNumberInputMgr.ShowNumInputBox(1, count - 1, 1, function (val)
			if val > 0 and val < count then
				Gac2Gas.RequestRdxExchangeCard(id,val)
			end
		end, LocalString.GetString("可将重复的卡片放入包裹赠送给其他玩家"), -1)
	elseif row == 2 or row == 3 then
		LuaCharacterCardMgr.ShareBigPicId = id
		CUIManager.ShowUI(CLuaUIResources.CharacterCardDetailWnd)
	end
end

function LuaCharacterCardWnd:OperateFuncSim(row,id)
	if row == 0 or row == 1 then
		LuaCharacterCardMgr.ShareBigPicId = id
		CUIManager.ShowUI(CLuaUIResources.CharacterCardDetailWnd)
	end
end

function LuaCharacterCardWnd:InitData()
	if self.nowChooseIndex then
		self:InitNode(self.nowChooseIndex)
	end
end

function LuaCharacterCardWnd:InitNode(initType)
	self.nowChooseIndex = initType
	local useData = self.pageDataTable[initType]

	Extensions.RemoveAllChildren(self.fNode.transform)
	local sortName = {LocalString.GetString('分解卡片'),LocalString.GetString('放入包裹'),LocalString.GetString('查看大图'),LocalString.GetString('分享')}
	local sortName2 = {LocalString.GetString('查看大图'),LocalString.GetString('分享')}

	for i,v in ipairs(useData) do
		local data = v
		local node = NGUITools.AddChild(self.fNode,self.templateNode)
		node:SetActive(true)
		local width = i % 5
		if width == 0 then
			width = 5
		end
		width = (width -1) * 295
		local height = math.floor(i / 6) * 337
		local bg = node.transform:Find('bg'):GetComponent(typeof(CUITexture))
		local num = node.transform:Find('num'):GetComponent(typeof(UILabel))
		local new = node.transform:Find('new').gameObject
	  bg:LoadMaterial(data.SmallPic)
		local dataIndex = i
		if initType == 2 then
			dataIndex = dataIndex + 10
		end

		local cardData = self.cardInfo[dataIndex]
		if cardData.count <= 0 then
			node.transform.localPosition = Vector3(width,-height,-1)
		else
			node.transform.localPosition = Vector3(width,-height,0)
		end

		if cardData.isNew and cardData.isNew == 0 and cardData.count > 0 then
			node.transform:Find('new').gameObject:SetActive(true)
		else
			node.transform:Find('new').gameObject:SetActive(false)
		end

		node.transform:Find('num'):GetComponent(typeof(UILabel)).text = cardData.count

		local btn = node.transform:Find('bg').gameObject

		if cardData.count and cardData.count > 1 then
			local clickFunction = function(go)
				local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
				for i,v in ipairs(sortName) do
					CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(v, DelegateFactory.Action_int(function (row)
						self:OperateFunc(row,data.Idx)
					end), false, nil))
				end
				CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), btn.transform, CPopupMenuInfoMgr.AlignType.Right,1,
				DelegateFactory.Action(function()
				end)
				,320,true,220)
			end
			CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(clickFunction),false)
		elseif cardData.count and cardData.count == 1 then
			local clickFunction = function(go)
				local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
				for i,v in ipairs(sortName2) do
					CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(v, DelegateFactory.Action_int(function (row)
						self:OperateFuncSim(row,data.Idx)
					end), false, nil))
				end
				CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), btn.transform, CPopupMenuInfoMgr.AlignType.Right,1,
				DelegateFactory.Action(function()
				end)
				,320,true,220)
			end
			CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(clickFunction),false)
		else
			local clickFunction = function(go)
				--g_MessageMgr:ShowMessage('CharacterCardEmptyTip')
			end
			CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(clickFunction),false)
		end
	end
end

function LuaCharacterCardWnd:InitTab()
	local onTab1Click = function(go)
		if self.tab1Normal.activeSelf then
			self.tab1Normal:SetActive(false)
			self.tab1Light:SetActive(true)
			self.tab2Normal:SetActive(true)
			self.tab2Light:SetActive(false)
			self:InitNode(1)
		else

		end
	end
	local onTab2Click = function(go)
		if self.tab2Normal.activeSelf then
			self.tab1Normal:SetActive(true)
			self.tab1Light:SetActive(false)
			self.tab2Normal:SetActive(false)
			self.tab2Light:SetActive(true)
			self:InitNode(2)
		else

		end
	end

	CommonDefs.AddOnClickListener(self.tab1,DelegateFactory.Action_GameObject(onTab1Click),false)
	CommonDefs.AddOnClickListener(self.tab2,DelegateFactory.Action_GameObject(onTab2Click),false)

	if self.nowChooseIndex and self.nowChooseIndex == 2 then
		self.tab1Normal:SetActive(true)
		self.tab1Light:SetActive(false)
		self.tab2Normal:SetActive(false)
		self.tab2Light:SetActive(true)
		self:InitNode(2)
	else
		self.tab1Normal:SetActive(false)
		self.tab1Light:SetActive(true)
		self.tab2Normal:SetActive(true)
		self.tab2Light:SetActive(false)
		self:InitNode(1)
	end
end

function LuaCharacterCardWnd:InitBonusNode()
	local itemIdTable = {RoleDrawingCard_Setting.GetData().RewardItemId1,RoleDrawingCard_Setting.GetData().RewardItemId2,RoleDrawingCard_Setting.GetData().RewardItemId3}
	local itemNodeTable = {self.bonusNode.transform:Find('node1/icon'),self.bonusNode.transform:Find('node2/icon'),self.bonusNode.transform:Find('node3/icon')}
	for i,v in ipairs(itemIdTable) do
		local itemId = v
		local item = Item_Item.GetData(itemId)
		local IconTexture = itemNodeTable[i]:GetComponent(typeof(CUITexture))
		IconTexture:LoadMaterial(item.Icon)
		CommonDefs.AddOnClickListener(itemNodeTable[i].gameObject, DelegateFactory.Action_GameObject(function(go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0,0,0,0)
		end), false)
	end
end

function LuaCharacterCardWnd:Init()
	local onCloseClick = function(go)
		self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.templateNode:SetActive(false)

	self.pageDataTable = {}
	self.pageDataTable[1] = {}
	self.pageDataTable[2] = {}

	for i=1,10 do
		local data = RoleDrawingCard_CardItem2Idx.GetData(i)
		table.insert(self.pageDataTable[1],data)
	end
	for i=11,20 do
		local data = RoleDrawingCard_CardItem2Idx.GetData(i)
		table.insert(self.pageDataTable[2],data)
	end

	--{isNew = isNew,count = count})
	self.cardInfo = LuaCharacterCardMgr.cardInfo

	self:InitTab()

  local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, RoleDrawingCard_Setting.GetData().ComposeMaterialId)

	if count > 0 then
		self.num2.text = count
		local compoundFunction = function()
			CUIManager.ShowUI(CLuaUIResources.CharacterCardHeWnd)
		end
		CommonDefs.AddOnClickListener(self.compoundBtn,DelegateFactory.Action_GameObject(compoundFunction),false)
		self.compoundBtn:GetComponent(typeof(CButton)).Enabled = true
	else
		self.num2.text = '[c][FF0000]'.. count .. '[-]'
		self.compoundBtn:GetComponent(typeof(CButton)).Enabled = false
	end

	local extraCount = 0
	for i,v in pairs(LuaCharacterCardMgr.cardInfo) do
		if v.count > 1 then
			extraCount = extraCount + v.count - 1
		end
	end
	local totalCount = 0
	local haveCount = 0
	for i,v in pairs(LuaCharacterCardMgr.cardInfo) do
		totalCount = totalCount + 1
		if v.count > 0 then
			haveCount = haveCount + 1
		end
	end
	self.totalnum.text = haveCount .. '/'.. totalCount

	if extraCount > 0 then
		local decomposeFunction = function()
			local itemgen = Item_Item.GetData(RoleDrawingCard_Setting.GetData().ComposeMaterialId)
			local count = extraCount
			local msg = SafeStringFormat3(LocalString.GetString('是否将%s张重复卡片分解为%s个%s'),count,RoleDrawingCard_Setting.GetData().DecomposeCount * count,itemgen.Name)

			local cost =  RoleDrawingCard_Setting.GetData().DecomposeCost * count
			QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinLiang, msg, cost, DelegateFactory.Action(function ( ... )
				Gac2Gas.RequestBatchDecomposeRdcCard()
			end), nil, false, true, EnumPlayScoreKey.NONE, false)
		end
		CommonDefs.AddOnClickListener(self.decomposeBtn,DelegateFactory.Action_GameObject(decomposeFunction),false)
		self.decomposeBtn:GetComponent(typeof(CButton)).Enabled = true
		self.num1.text = extraCount
	else
		self.num1.text = '[c][FF0000]'.. extraCount .. '[-]'
		self.decomposeBtn:GetComponent(typeof(CButton)).Enabled = false
	end

	self:InitBonusNode()
end

return LuaCharacterCardWnd
