local CButton                = import "L10.UI.CButton"
local QnAdvanceGridView      = import "L10.UI.QnAdvanceGridView"
local QnButton               = import "L10.UI.QnButton"
local CCurentMoneyCtrl       = import "L10.UI.CCurentMoneyCtrl"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"

LuaArenaPassportBuyLevelWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "topTip", "TopTip", UILabel)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "centerTip", "CenterTip", UILabel)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "moneyCtrl", "MoneyCtrl", CCurentMoneyCtrl)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "slider", "Slider", UISlider)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "decreaseButton", "DecreaseButton", QnButton)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "increaseButton", "IncreaseButton", QnButton)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "title", "TitleLabel", UILabel)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "milestone_ReturnAward", "Award", CQnReturnAwardTemplate)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "milestone_Bind", "Bind", GameObject)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "milestone_Level", "MilestoneLevel", UILabel)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "milestone_AwardName", "Name", UILabel)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "button", "Button", CButton)
RegistChildComponent(LuaArenaPassportBuyLevelWnd, "awardTableView", "TableView", QnAdvanceGridView)

--@endregion RegistChildComponent end

RegistClassMember(LuaArenaPassportBuyLevelWnd, "levelRange")
RegistClassMember(LuaArenaPassportBuyLevelWnd, "selectLevel")
RegistClassMember(LuaArenaPassportBuyLevelWnd, "awardList")

function LuaArenaPassportBuyLevelWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.decreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDecreaseButtonClick()
	end)

	UIEventListener.Get(self.increaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)

	UIEventListener.Get(self.button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)

    --@endregion EventBind end

    self.slider.OnChangeValue = DelegateFactory.Action_float(function(value)
		self:OnSliderChangeValue(value)
    end)
end

function LuaArenaPassportBuyLevelWnd:Init()
	local curLevel = LuaArenaMgr.buyLevelInfo.level
	local maxLevel = Arena_PassportLevelReward.GetDataCount()
	self.levelRange = curLevel + 200 > maxLevel and maxLevel - curLevel or 200

	self.awardList = {}
    self.awardTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.awardList
        end,
        function(item, index)
            self:InitItem(item, index)
        end
    )

	self.selectLevel = 0
	local defaultLevel
	if curLevel < 200 then
		defaultLevel = 50 - curLevel % 50
	else
		defaultLevel = curLevel + 100 > maxLevel and maxLevel - curLevel or 100
	end
	self.slider.value = self.levelRange == 0 and 1 or (defaultLevel - 1) / self.levelRange

	self.title.text = LuaArenaMgr.buyLevelInfo.isGifting and LocalString.GetString("赠送等级") or LocalString.GetString("购买等级")
	self.button.Text = LuaArenaMgr.buyLevelInfo.isGifting and LocalString.GetString("赠  送") or LocalString.GetString("购  买")
end

function LuaArenaPassportBuyLevelWnd:InitItem(item, index)
	local data = self.awardList[index + 1]
	local returnAward = item.transform:GetComponent(typeof(CQnReturnAwardTemplate))
	local itemData = Item_Item.GetData(data.itemId)
	returnAward:Init(itemData, data.count)
	item.transform:Find("Bind").gameObject:SetActive(itemData.Lock == 1)
end

function LuaArenaPassportBuyLevelWnd:UpdateInfo()
	self.decreaseButton.Enabled = self.selectLevel > 1
	self.increaseButton.Enabled = self.selectLevel < self.levelRange

	local curLevel = LuaArenaMgr.buyLevelInfo.level
	local targetLevel = curLevel + self.selectLevel
	if LuaArenaMgr.buyLevelInfo.isGifting then
		self.topTip.text = SafeStringFormat3(LocalString.GetString("%s的战令升至[FFFF00]%d级[-]，可获得以下奖励"), LuaArenaMgr.buyLevelInfo.name, targetLevel)
		local str = ""
		local setting = Arena_PassportSetting.GetData()
		local gotLevelLimit = setting.MaxLevelByGifting - LuaArenaMgr.buyLevelInfo.gotLevel
		if gotLevelLimit > 0 then
			local rewardLevel = math.min(math.floor(self.selectLevel / setting.GiftLevelRequired), gotLevelLimit)
			str = SafeStringFormat3(LocalString.GetString("（你将获得[FFFF00]%d级[-]奖励）"), rewardLevel)
		end
		self.centerTip.text = SafeStringFormat3(LocalString.GetString("赠送[FFFF00]%d级[-]，对方战令升至[FFFF00]%d级[-]%s"), self.selectLevel, targetLevel, str)
	else
		self.topTip.text = SafeStringFormat3(LocalString.GetString("战令升至[FFFF00]%d级[-]，可立即获得以下奖励"), targetLevel)
		self.centerTip.text = SafeStringFormat3(LocalString.GetString("购买[FFFF00]%d[-]等级，升至[FFFF00]%d级[-]"), self.selectLevel, targetLevel)
	end

	self.awardList = LuaArenaMgr:GetPassportItemRewardsInLevelRange(curLevel, targetLevel)
	self.awardTableView:ReloadData(true, true)

	local milestoneLevel = 0
	if targetLevel <= 200 then
		milestoneLevel = math.ceil(targetLevel / 50) * 50
	else
		for level = targetLevel, curLevel + self.levelRange do
			if Arena_PassportLevelReward.GetData(level).IsMilestoneReward == 1 then
				milestoneLevel = level
				break
			end
		end
	end
	if milestoneLevel == 0 then
		for level = targetLevel, curLevel + 1, -1 do
			if Arena_PassportLevelReward.GetData(level).IsMilestoneReward == 1 then
				milestoneLevel = level
				break
			end
		end
	end
	local itemRewards = LuaArenaMgr:GetPassportItemRewards(milestoneLevel)
	local itemData = Item_Item.GetData(itemRewards[1].itemId)
	self.milestone_ReturnAward:Init(itemData, itemRewards[1].count)
	self.milestone_Bind:SetActive(itemData.Lock == 1)
	self.milestone_AwardName.text = itemData.Name
	self.milestone_Level.text = SafeStringFormat3(LocalString.GetString("%d级可获得"), milestoneLevel)

	local totalJade = 0
	for level = curLevel + 1, targetLevel do
		totalJade = totalJade + Arena_PassportLevelReward.GetData(level).BuyJade
	end
	self.moneyCtrl:SetCost(totalJade)
end

function LuaArenaPassportBuyLevelWnd:OnDestroy()
	LuaArenaMgr.buyLevelInfo = {}
end

--@region UIEvent

function LuaArenaPassportBuyLevelWnd:OnButtonClick()
	if not self.moneyCtrl.moneyEnough then
		g_MessageMgr:ShowMessage("ARENA_PASSPORT_BUY_LEVEL_MONEY_NOT_ENOUGH")
		return
	end

	local cost = self.moneyCtrl:GetCost()
	if LuaArenaMgr.buyLevelInfo.isGifting then
		local name = LuaArenaMgr.buyLevelInfo.name
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ARENA_PASSPORT_GIFT_LEVEL_COMFIRM", cost, name, self.selectLevel), function ()
			Gac2Gas.RequestBuyArenaPassportLevel(self.selectLevel)
			CUIManager.CloseUI(CLuaUIResources.ArenaPassportBuyLevelWnd)
		end, nil, nil, nil, false)
	else
		local targetLevel = LuaArenaMgr.buyLevelInfo.level + self.selectLevel
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ARENA_PASSPORT_BUY_LEVEL_COMFIRM", cost, targetLevel), function ()
			Gac2Gas.RequestBuyArenaPassportLevel(self.selectLevel)
			CUIManager.CloseUI(CLuaUIResources.ArenaPassportBuyLevelWnd)
		end, nil, nil, nil, false)
	end
end

function LuaArenaPassportBuyLevelWnd:OnDecreaseButtonClick()
	self.slider.value = self.levelRange == 0 and 1 or (self.selectLevel - 2) / self.levelRange
end

function LuaArenaPassportBuyLevelWnd:OnIncreaseButtonClick()
	self.slider.value = self.levelRange == 0 and 1 or self.selectLevel / self.levelRange
end

function LuaArenaPassportBuyLevelWnd:OnSliderChangeValue(value)
	local level = math.min(math.ceil(self.slider.value * self.levelRange + 0.5), self.levelRange)
	if self.selectLevel ~= level then
		self.selectLevel = level
		self:UpdateInfo()
	end
end


--@endregion UIEvent

