local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local UIWidget = import "UIWidget"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
LuaUpDownWorldEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaUpDownWorldEnterWnd, "TaskItem1", "TaskItem1", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "TaskItem2", "TaskItem2", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "TaskItem3", "TaskItem3", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "RefreshLabel", "RefreshLabel", UILabel)
RegistChildComponent(LuaUpDownWorldEnterWnd, "ScoreSlider", "ScoreSlider", UISlider)
RegistChildComponent(LuaUpDownWorldEnterWnd, "CurScoreLabel", "CurScoreLabel", UILabel)
RegistChildComponent(LuaUpDownWorldEnterWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaUpDownWorldEnterWnd, "ExchangeBtn", "ExchangeBtn", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaUpDownWorldEnterWnd, "ExchangeItem1", "ExchangeItem1", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "ExchangeItem2", "ExchangeItem2", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "ExchangeItem3", "ExchangeItem3", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "ExchangeItem4", "ExchangeItem4", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "ExchangeItem5", "ExchangeItem5", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "EnterBtnTipLabel", "EnterBtnTipLabel", UILabel)
RegistChildComponent(LuaUpDownWorldEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaUpDownWorldEnterWnd, "RuleBtn", "RuleBtn", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaUpDownWorldEnterWnd, "m_exchangeCtrls")
RegistClassMember(LuaUpDownWorldEnterWnd, "m_tick")

function LuaUpDownWorldEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    UIEventListener.Get(self.ExchangeBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnExchangeBtnClick()
        end
    )

    UIEventListener.Get(self.EnterBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnEnterBtnClick()
        end
    )

    UIEventListener.Get(self.RuleBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnRuleBtnClick()
        end
    )

    --@endregion EventBind end

    self.m_exchangeCtrls = {
        self.ExchangeItem1,
        self.ExchangeItem2,
        self.ExchangeItem3,
        self.ExchangeItem4,
        self.ExchangeItem5
    }
end

function LuaUpDownWorldEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncUpDownWorldNmlTaskData", self, "OnSyncUpDownWorldNmlTaskData")
    g_ScriptEvent:AddListener("OnSyncUpDownWorldTaskData", self, "OnSyncUpDownWorldTaskData")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaUpDownWorldEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncUpDownWorldNmlTaskData", self, "OnSyncUpDownWorldNmlTaskData")
    g_ScriptEvent:RemoveListener("OnSyncUpDownWorldTaskData", self, "OnSyncUpDownWorldTaskData")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaUpDownWorldEnterWnd:OnDestroy()
    UnRegisterTick(self.m_tick)
end

function LuaUpDownWorldEnterWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eHanJia2024InvertedWorld_EntrustScore then
        self:UpdateScore()
    end
end

function LuaUpDownWorldEnterWnd:OnSyncUpDownWorldNmlTaskData()
    local data = LuaHanJia2024Mgr.UpDownWorldData
    self:UpdateNmlTasks(data.NmlTasks)
end

function LuaUpDownWorldEnterWnd:OnSyncUpDownWorldTaskData()
    local rcount = 0
    if CClientMainPlayer.Inst then
        local key = EnumPlayTimesKey_lua.eHanJia2024InvertedWorld_ResetTimes
        rcount = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(key)
    end

    local udwsetting = HanJia2024_UpDownWorldSetting.GetData()
    local maxrefesh = udwsetting.ResetEntrustMissionDailyLimit
    self.RefreshLabel.text = rcount .. "/" .. maxrefesh
    local canrefresh = rcount < maxrefesh

    local data = LuaHanJia2024Mgr.UpDownWorldData
    self:UpdateTasks(data.Tasks, canrefresh)
end

--刷新任务
function LuaUpDownWorldEnterWnd:OnTaskRefreshBtnClick(taskdata)
    local cfg = HanJia2024_EntrustMissionInfo.GetData(taskdata.TaskID)
    local setting = HanJia2024_UpDownWorldSetting.GetData()
    local type = cfg.HardType
    local tasksum = cfg.NeedProgress

    local title = ""
    local cost = setting.ResetEntrustMissionNeedSilver
    local moneyType = EnumMoneyType.YinLiang
    local okfunc =
        DelegateFactory.Action(
        function()
            Gac2Gas.HanJia2024InvertedWorld_RequestResetEntrustMission(taskdata.TaskID)
        end
    )

    if taskdata.Count >= tasksum then --可提交
        title = g_MessageMgr:FormatMessage("UPDOWNWORLD_CANSUBMIT_REFRESH")
    else
        title = g_MessageMgr:FormatMessage("UPDOWNWORLD_NORMAL_REFRESH")
    end
    QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(moneyType, title, cost, okfunc, nil, false, true, EnumPlayScoreKey.NONE, false)
end

--完成委托
function LuaUpDownWorldEnterWnd:OnTaskCompleteBtnClick(id)
    Gac2Gas.HanJia2024InvertedWorld_RequestClaimMissionReward(id)
end

function LuaUpDownWorldEnterWnd:GetScore()
    local key = EnumPlayTimesKey_lua.eHanJia2024InvertedWorld_EntrustScore
    local curscore = 0
    if CClientMainPlayer.Inst then
        curscore = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(key)
    end
    return curscore
end

function LuaUpDownWorldEnterWnd:Init()
    local setting = HanJia2024_Setting.GetData()
    self.TimeLabel.text = setting.UpDownWorldWndTime

    self:OnSyncUpDownWorldTaskData()
    self:OnSyncUpDownWorldNmlTaskData()

    self:UpdateScore()

    local data = LuaHanJia2024Mgr.UpDownWorldData
    local canEnter = data.IsStart
    CUICommonDef.SetGrey(self.EnterBtn, not canEnter)
    CUICommonDef.SetColliderActive(self.EnterBtn,canEnter)

    if data.EndTime > 0 then
        local tickfunc = function()
            local delta = data.EndTime - CServerTimeMgr.Inst.timeStamp
            local t = Utility.GetTimeString(delta)
            local txt = ""
            if data.IsStart then
                txt = SafeStringFormat3(LocalString.GetString("将于%s后关闭"), t)
            else
                txt = SafeStringFormat3(LocalString.GetString("将于%s后开启"), t)
            end
            if delta > 0 then
                self.EnterBtnTipLabel.text = txt
            else
                data.EndTime = 0
                if data.IsStart then
                    data.IsStart = false
                    UnRegisterTick(self.m_tick)
                else
                    data.IsStart = true
                end
                CUICommonDef.SetGrey(self.EnterBtn, not data.IsStart) 
                CUICommonDef.SetColliderActive(self.EnterBtn, data.IsStart)
                self.EnterBtnTipLabel.text = ""
                
            end
        end
        UnRegisterTick(self.m_tick)
        self.m_tick = RegisterTick(tickfunc, 1000)
    else
        self.EnterBtnTipLabel.text = ""
    end
end

function LuaUpDownWorldEnterWnd:UpdateTasks(tasks, canrefresh)
    for i = 1, #tasks do
        self:UpdateTaskCtrl(tasks[i], canrefresh)
    end
end

function LuaUpDownWorldEnterWnd:UpdateTaskCtrl(taskdata, canrefresh)
    local taskid = taskdata.TaskID
    local count = taskdata.Count
    local state = taskdata.State

    local ecfg = HanJia2024_EntrustMissionInfo.GetData(taskid)

    local ctrlgo = nil
    local hardname = ""
    if ecfg.HardType == 1 then
        ctrlgo = self.TaskItem1 
        hardname = LocalString.GetString("简单")
    elseif ecfg.HardType == 2 then
        ctrlgo = self.TaskItem2
        hardname = LocalString.GetString("普通")
    elseif ecfg.HardType == 3 then
        ctrlgo = self.TaskItem3
        hardname = LocalString.GetString("困难")
    end     

    local trans = ctrlgo.transform
    local typelabel = FindChildWithType(trans, "TypeName", typeof(UILabel))
    local taskdesclabel = FindChildWithType(trans, "TaskDesc", typeof(UILabel))
    local taskslider = FindChildWithType(trans, "Slider", typeof(UISlider))
    local tasksliderDesc = FindChildWithType(trans, "Slider/Label", typeof(UILabel))
    local rewardlabel1 = FindChildWithType(trans, "Reward/Count1", typeof(UILabel))
    local rewardlabel2 = FindChildWithType(trans, "Reward/Count2", typeof(UILabel))
    local completebtn = FindChildWithType(trans, "CompleteBtn", typeof(GameObject))
    local completedgo = FindChildWithType(trans, "Completed", typeof(GameObject))
    local refreshbtn = FindChildWithType(trans, "RefreshBtn", typeof(GameObject))

    typelabel.text = hardname
    taskdesclabel.text = ecfg.Desc

    local tasksum = ecfg.NeedProgress
    taskslider.value = math.min(1, count / tasksum)
    tasksliderDesc.text = count .. "/" .. tasksum

    rewardlabel1.text = tostring(ecfg.EntrustScore)
    rewardlabel2.text = tostring(ecfg.PassProgress)

    refreshbtn:SetActive(canrefresh)
    UIEventListener.Get(refreshbtn).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnTaskRefreshBtnClick(taskdata)
        end
    )

    if state then --已经完成
        completebtn:SetActive(false)
        completedgo:SetActive(true)
    else
        local canSubmit = count >= ecfg.NeedProgress
        if canSubmit then
            completebtn:SetActive(true)
            completedgo:SetActive(false)
        else
            completebtn:SetActive(false)
            completedgo:SetActive(false)
        end
    end

    UIEventListener.Get(completebtn).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnTaskCompleteBtnClick(taskid)
        end
    )
end

function LuaUpDownWorldEnterWnd:UpdateScore()
    local curscore = self:GetScore()
    local udwsetting = HanJia2024_UpDownWorldSetting.GetData()
    local step = udwsetting.ExchangeNeedEntrustScore

    self.ScoreSlider.value = math.min(1, curscore / step)
    self.CurScoreLabel.text = curscore .. "/" .. step
    self.CountLabel.text = step * math.floor(curscore / step)

    local exchangeAlert = FindChildWithType(self.ExchangeBtn.transform, "Alert", typeof(GameObject))
    exchangeAlert:SetActive(curscore >= step)
end

function LuaUpDownWorldEnterWnd:UpdateNmlTasks(tasks)
    for i = 1, #tasks do
        local ctrlgo = self.m_exchangeCtrls[i]
        local ctrl = ctrlgo.transform
        local widget = ctrlgo:GetComponent(typeof(UIWidget))
        local countlabel = FindChildWithType(ctrl, "Count", typeof(UILabel))
        local icon = FindChildWithType(ctrl, "Item/Mask/Icon", typeof(CUITexture))
        local deslabel1 = FindChildWithType(ctrl, "Desc1", typeof(UILabel))
        local deslabel2 = FindChildWithType(ctrl, "Desc2", typeof(UILabel))

        local taskdata = tasks[i]

        local cfg = HanJia2024_NormalMissionInfo.GetData(taskdata.TaskID)

        widget.alpha = taskdata.Count >= cfg.NeedProgress and 0.5 or 1
        countlabel.text = taskdata.Count .. "/" .. cfg.NeedProgress
        deslabel1.text = cfg.Desc
        deslabel2.text = cfg.Desc2

        local rewarditem = Item_Item.GetData(cfg.RewardItem)
        icon:LoadMaterial(rewarditem.Icon)

        UIEventListener.Get(icon.gameObject).onClick =
            DelegateFactory.VoidDelegate(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(cfg.RewardItem, false, nil, AlignType.Default, 0, 0, 0, 0)
            end
        )
    end
end

--@region UIEvent

--[[
    @desc: 积分兑换
    author:Codee
    time:2023-09-12 15:07:56
    @return:
]]
function LuaUpDownWorldEnterWnd:OnExchangeBtnClick()
    local udwsetting = HanJia2024_UpDownWorldSetting.GetData()
    local step = udwsetting.ExchangeNeedEntrustScore
    local curscore = self:GetScore()
    if curscore >= step then
        Gac2Gas.HanJia2024InvertedWorld_RequestExchangePassProgress()
    else
        g_MessageMgr:ShowMessage("UPDOWNWORLD_CANNOT_EXCHANGE")
    end
end

--[[
    @desc: 进入副本
    author:Codee
    time:2023-09-12 15:08:03
    @return:
]]
function LuaUpDownWorldEnterWnd:OnEnterBtnClick()
    local data = LuaHanJia2024Mgr.UpDownWorldData
    if data.IsStart then
        Gac2Gas.HanJia2024InvertedWorld_RequestEnterPlay()
    end
end

--[[
    @desc: 规则显示
    author:Codee
    time:2023-09-12 15:08:10
    @return:
]]
function LuaUpDownWorldEnterWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("UPDOWNWORLD_RULE")
end

--@endregion UIEvent
