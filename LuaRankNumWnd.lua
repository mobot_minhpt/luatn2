require("common/common_include")

CLuaRankNumWnd=class()
function CLuaRankNumWnd:Init()
    local v=LuaUtils.RankNumWndValue
    FindChild(self.transform,"RankLabel"):GetComponent(typeof(UILabel)).text=tostring(v)

    RegisterTickOnce(function()
        CUIManager.CloseUI(CUIResources.RankNumWnd)
    end,1500)
end

return CLuaRankNumWnd