local UITexture = import "UITexture"
local UIGrid = import "UIGrid"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local GameObject = import "UnityEngine.GameObject"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local DelegateFactory  = import "DelegateFactory"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Constants = import "L10.Game.Constants"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Object = import "System.Object"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local Quaternion = import "UnityEngine.Quaternion"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local EHideKuiLeiStatus = import "L10.Engine.EHideKuiLeiStatus"

LuaShiTuMainWndShiTuView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuMainWndShiTuView, "LevelSprite", "LevelSprite", UISprite)
RegistChildComponent(LuaShiTuMainWndShiTuView, "TreeTexture", "TreeTexture", CUITexture)
RegistChildComponent(LuaShiTuMainWndShiTuView, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaShiTuMainWndShiTuView, "ShiMenSlider", "ShiMenSlider", UISlider)
RegistChildComponent(LuaShiTuMainWndShiTuView, "Bg", "Bg", CUITexture)
RegistChildComponent(LuaShiTuMainWndShiTuView, "ShiMenNameLabel", "ShiMenNameLabel", UILabel)
RegistChildComponent(LuaShiTuMainWndShiTuView, "EditShiMenNameButton", "EditShiMenNameButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "QiYiNumLabel", "QiYiNumLabel", UILabel)
RegistChildComponent(LuaShiTuMainWndShiTuView, "QiYiButton", "QiYiButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "ChatButton", "ChatButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "GiftButton", "GiftButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "ActivityButton", "ActivityButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "LeaveButton", "LeaveButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "QiuSuoButton", "QiuSuoButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "WenDaoButton", "WenDaoButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "DianCeButton", "DianCeButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "CenterViewFirstObject", "CenterViewFirstObject", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "CenterGrid", "CenterGrid", UIGrid)
RegistChildComponent(LuaShiTuMainWndShiTuView, "CenterViewTemplate01", "CenterViewTemplate01", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "CenterViewTemplate02", "CenterViewTemplate02", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "CenterViewTemplate03", "CenterViewTemplate03", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "CenterViewTemplate04", "CenterViewTemplate04", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "CenterViewTemplate05", "CenterViewTemplate05", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "MoreDiZiButton", "MoreDiZiButton", GameObject)
RegistChildComponent(LuaShiTuMainWndShiTuView, "OtherDiZiNumLabel", "OtherDiZiNumLabel", UILabel)
RegistChildComponent(LuaShiTuMainWndShiTuView, "PreviewerTexture", "PreviewerTexture", UITexture)
RegistChildComponent(LuaShiTuMainWndShiTuView, "ExpProgressLabel", "ExpProgressLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuMainWndShiTuView, "m_IsTudiView")
RegistClassMember(LuaShiTuMainWndShiTuView, "m_Identifier")
RegistClassMember(LuaShiTuMainWndShiTuView, "m_RO")
RegistClassMember(LuaShiTuMainWndShiTuView, "m_ChildROList")
RegistClassMember(LuaShiTuMainWndShiTuView, "m_IsCreatingShiTuGroupIm")

function LuaShiTuMainWndShiTuView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.EditShiMenNameButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEditShiMenNameButtonClick()
	end)


	
	UIEventListener.Get(self.QiYiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQiYiButtonClick()
	end)


	
	UIEventListener.Get(self.ChatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)


	
	UIEventListener.Get(self.GiftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiftButtonClick()
	end)


	
	UIEventListener.Get(self.ActivityButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnActivityButtonClick()
	end)


	
	UIEventListener.Get(self.LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.QiuSuoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQiuSuoButtonClick()
	end)


	
	UIEventListener.Get(self.WenDaoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWenDaoButtonClick()
	end)


	
	UIEventListener.Get(self.DianCeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDianCeButtonClick()
	end)


	
	UIEventListener.Get(self.MoreDiZiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMoreDiZiButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuMainWndShiTuView:Init()
    self.m_IsTudiView = LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0 
    self.EditShiMenNameButton.gameObject:SetActive(not self.m_IsTudiView)
    self.LeaveButton.gameObject:SetActive(self.m_IsTudiView)
    self.TitleLabel.color = Color.white
    self.CenterViewTemplate01.gameObject:SetActive(true)
    self.CenterViewTemplate02.gameObject:SetActive(true)
    self.CenterViewTemplate03.gameObject:SetActive(true)
    self.CenterViewTemplate04.gameObject:SetActive(true)
    self.CenterViewTemplate05.gameObject:SetActive(false)
    if CClientMainPlayer.Inst then
        local score = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.QYJF)
        self.QiYiNumLabel.text = score
        self.CenterViewTemplate05.gameObject:SetActive(LuaShiTuMgr.m_EnableWaiMen and CClientMainPlayer.Inst.PlayProp.SealId >= 5)
    end
end

function LuaShiTuMainWndShiTuView:OnEnable()
    g_ScriptEvent:AddListener("OnShiTuRelationshipChanged", self, "OnShiTuRelationshipChanged")
    g_ScriptEvent:AddListener("OnSendShiTuShiMenInfo", self, "OnSendShiTuShiMenInfo")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("MainPlayerJoinGroupIM", self, "OnMainPlayerJoinGroupIM")
    self:Init()
    Gac2Gas.RequestShiTuShiMenInfo(LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0)
end

function LuaShiTuMainWndShiTuView:OnDisable()
    g_ScriptEvent:RemoveListener("OnShiTuRelationshipChanged", self, "OnShiTuRelationshipChanged")
    g_ScriptEvent:RemoveListener("OnSendShiTuShiMenInfo", self, "OnSendShiTuShiMenInfo")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate",self,"OnMainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("MainPlayerJoinGroupIM", self, "OnMainPlayerJoinGroupIM")
    if self.m_ChildROList and #self.m_ChildROList > 0 then
		for i = 1,#self.m_ChildROList do
            if self.m_ChildROList then
                self.m_ChildROList[i]:Destroy()
                self.m_ChildROList[i] = nil
            end
		end
        self.m_ChildROList = {}
	end
end

function LuaShiTuMainWndShiTuView:OnShiTuRelationshipChanged()
    Gac2Gas.RequestShiTuShiMenInfo(LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0)
end

function LuaShiTuMainWndShiTuView:OnSendShiTuShiMenInfo()
    local info = LuaShiTuMgr.m_ShiTuShiMenInfo
    self.m_IsTudiView = info.isTuDi
    LuaShiTuMgr.m_ShiTuMainWndTabIndex = self.m_IsTudiView and 0 or 1
    self.ShiMenNameLabel.text = String.IsNullOrEmpty(info.name) and LocalString.GetString("师门未赋名") or info.name
    self.ShiMenNameLabel.alpha = String.IsNullOrEmpty(info.name) and 0.3 or 1
    self.OtherDiZiNumLabel.text = SafeStringFormat3(LocalString.GetString("+%d位"),#LuaShiTuMgr.m_WaiMenDiZiInfo)
    local level = info.level
    local data = ShiTu_MainLevel.GetData(level)
    if data then
        self.LevelSprite.spriteName = SafeStringFormat3("guildwnd_grade_0%d",level)--string.gsub(data.LevelIcon,".png","")
        self.TreeTexture:LoadMaterial(data.LevelIcon)
        self.TitleLabel.text = data.Title
        self.ExpProgressLabel.text = SafeStringFormat3("%d/%d", info.exp, data.UpgradePionts)
        self.ShiMenSlider.value = info.exp / data.UpgradePionts
        for i = 1, 5 do
            self.transform:Find(SafeStringFormat3("Tree/Tree_Lv0%d",i)).gameObject:SetActive(i == level)
        end
    end
    self.WenDaoButton.transform:Find("Alert").gameObject:SetActive(info.hasWenDaoReward)
    self:InitItemList()
    self:InitPreviewerTexture() 
end

function LuaShiTuMainWndShiTuView:OnMainPlayerPlayPropUpdate()
    local score = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.QYJF)
    self.QiYiNumLabel.text = score
end

function LuaShiTuMainWndShiTuView:InitItem(go, data, isFirst)
    local titleLabel = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local myLabel = go.transform:Find("MyLabel"):GetComponent(typeof(UILabel))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local lvLabel = go.transform:Find("LvLabel"):GetComponent(typeof(UILabel))
    local online = go.transform:Find("Online").gameObject
    local offline = go.transform:Find("Offline").gameObject
    local container = go.transform:Find("Container").gameObject
    myLabel.gameObject:SetActive(data and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.playerId)
    titleLabel.gameObject:SetActive(data and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= data.playerId)
    titleLabel.text = data and data.title or LocalString.GetString("暂无")
    nameLabel.text = data and data.playerName or ""
    lvLabel.text = data and SafeStringFormat3("Lv.%d",data.playerLevel) or ""
    lvLabel.color = (data and data.isFeiSheng) and NGUIText.ParseColor24("fe7900", 0) or Color.white
    online.gameObject:SetActive(data and data.isOnline)
    offline.gameObject:SetActive(data and not data.isOnline)
    UIEventListener.Get(container.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if data then
            local pos = Vector3(540, 0, 0)
            pos = self.transform:TransformPoint(pos)    
            local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
            if not self.m_IsTudiView and data.shituType == 2 then
                CommonDefs.DictSet(dict, typeof(cs_string),  "TuDiId", typeof(Object), data.playerId)
                CommonDefs.DictSet(dict, typeof(cs_string),  "TuDiName", typeof(Object), data.playerName)
            elseif self.m_IsTudiView and data.shituType == 1 then
                CommonDefs.DictSet(dict, typeof(cs_string),  "ShiFuId" , typeof(Object), data.playerId)
            end
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.ShiTu, EChatPanel.Undefined, nil, nil, dict, pos, AlignType1.Default)
        elseif not self.m_IsTudiView then
            CShiTuMgr.Inst.CurrentEnterType = CShiTuMgr.ShiTuChooseType.SHI
            local issuccess = LuaShiTuMgr:GetShiTuResultWndCacheData(CShiTuMgr.ShiTuChooseType.SHI)
            if not issuccess then
                local msg = g_MessageMgr:FormatMessage("ShiTuRecommendWnd_DengJi_ShouTu_Confirm") 
                MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                    CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
                end),nil,LocalString.GetString("登记"),LocalString.GetString("取消"),false)
                return
            end
            Gac2Gas.RequestRecommendTuDi(false)
        end
	end)
    if not isFirst then
        local addBtn = go.transform:Find("AddBtn").gameObject
        local empty = go.transform:Find("Empty").gameObject
        local you = go.transform:Find("You").gameObject
        addBtn.gameObject:SetActive(data == nil and not self.m_IsTudiView)
        empty.gameObject:SetActive(data == nil)
        you.gameObject:SetActive(data and data.isBest)
    else
        titleLabel.gameObject:SetActive(self.m_IsTudiView)
        myLabel.gameObject:SetActive(not self.m_IsTudiView)
    end
end

function LuaShiTuMainWndShiTuView:InitItemList()
    Extensions.RemoveAllChildren(self.CenterGrid.transform)
    local titleArr1 = {LocalString.GetString("吾师"),LocalString.GetString("大师兄"),LocalString.GetString("二师兄"),LocalString.GetString("三师兄"),LocalString.GetString("四师兄")}
    local titleArr2 = {LocalString.GetString("我"),LocalString.GetString("大徒弟"),LocalString.GetString("二徒弟"),LocalString.GetString("三徒弟"),LocalString.GetString("四徒弟")}
    local centerViewTemplateArr = {self.CenterViewTemplate01,self.CenterViewTemplate02,self.CenterViewTemplate03,self.CenterViewTemplate04,self.CenterViewTemplate05}
    local mybeTuDiTime = 0
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
    for i , data in pairs(LuaShiTuMgr.m_ShiTuShiMenPlayerInfo) do
        if myId == data.playerId then
            mybeTuDiTime = data.beTuDiTime
        end
    end
    for i , data in pairs(LuaShiTuMgr.m_WaiMenDiZiInfo) do
        if myId == data.playerId then
            mybeTuDiTime = data.beTuDiTime
        end
    end
    for i = 1, 5 do
        local data = LuaShiTuMgr.m_ShiTuShiMenPlayerInfo[i]
        if data then
            data.title = self.m_IsTudiView and titleArr1[i] or titleArr2[i]
            if self.m_IsTudiView and data.beTuDiTime > mybeTuDiTime then
                data.title = string.gsub(data.title, LocalString.GetString("兄"), LocalString.GetString("弟"))
            end
            if data.gender == 1 then
                data.title = string.gsub(data.title, LocalString.GetString("兄"), LocalString.GetString("姐"))
                data.title = string.gsub(data.title, LocalString.GetString("师弟"), LocalString.GetString("师妹"))
            end
            LuaShiTuMgr.m_ShiTuShiMenPlayerInfo[i] = data
        end
        if i > 1 then
            self:InitItem(centerViewTemplateArr[i - 1], data, false)
        end
    end
    self:InitItem(self.CenterViewFirstObject, LuaShiTuMgr.m_ShiTuShiMenPlayerInfo[1], true)
end

function LuaShiTuMainWndShiTuView:InitPreviewerTexture()
    self.m_Identifier = "_ShiTu_Preview_"
    if self.PreviewerTexture.mainTexture and self.m_RO then
		self:LoadModels(self.m_RO)
		return
	end
	local modelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
		self.m_RO = ro
		self:LoadModels(self.m_RO)
    end)
    CUIManager.DestroyModelTexture(self.m_Identifier)
	self.PreviewerTexture.mainTexture = CUIManager.CreateModelTexture(self.m_Identifier, modelTextureLoader,180,0.1,-1,16.64,false,true,1,true,false)
	self.PreviewerTexture.mainTexture:Release()
	self.PreviewerTexture.mainTexture.width = 2048
	self.PreviewerTexture.mainTexture.height = 2048
	self.PreviewerTexture.mainTexture:Create()
end

function LuaShiTuMainWndShiTuView:LoadModels(ro)
    if self.m_ChildROList and #self.m_ChildROList > 0 then
		for i = 1,#self.m_ChildROList do
            if self.m_ChildROList then
                self.m_ChildROList[i]:Destroy()
                self.m_ChildROList[i] = nil
            end
		end
	end
    self.m_ChildROList = {}
    local modelPosList = ShiTu_Setting.GetData().ShiTuMainWndModelPos
    local modelScaleList = ShiTu_Setting.GetData().ModelScaling
    --self.m_ModelsPosList = {Vector3(1.6,0,7.7),Vector3(0.6,0,7.7),Vector3(-0.2,0,7.7),Vector3(-1,0,7.7),Vector3(-1.8,0,7.7)}
    for i = 1, 5 do
        local childRO = CRenderObject.CreateRenderObject(ro.gameObject,"RenderObject",false)
        childRO.name = i
        childRO.NeedUpdateAABB = true
        childRO.Layer = LayerDefine.ModelForNGUI_3D
        local data = LuaShiTuMgr.m_ShiTuShiMenPlayerInfo[i]
        local fakeAppearance =  CreateFromClass(CPropertyAppearance)
        if data then
            childRO.name = data.playerName
            fakeAppearance.m_Gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), data.gender)
            fakeAppearance.m_Class =  CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class)
            fakeAppearance.FeiShengAppearanceXianFanStatus = data.isFeiSheng and 1 or 0   
            fakeAppearance.Equipment = Table2Array({23030000}, MakeArrayClass(UInt32))
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.playerId then
                fakeAppearance = (CClientMainPlayer.Inst) and CClientMainPlayer.Inst.AppearanceProp:Clone() or fakeAppearance
            end
            CClientMainPlayer.LoadResource(childRO, fakeAppearance, true, 1, 0, false, 0, false, 0, false, nil, false, false)
            childRO:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
            childRO:ForceHideKuiLei(EHideKuiLeiStatus.ForceHideKuiLeiAndClose)
            if i ~= 1 then            
                childRO:DoAni("sit_loop", true, 0, 1.0, 0, true, 1.0)
            end
            local posArr = modelPosList[i - 1]
            childRO.transform.localPosition = Vector3(posArr[0],posArr[1],posArr[2])
            local scale = modelScaleList[i - 1][data.gender]
            childRO.transform.localScale = Vector3(scale,scale,scale)
            childRO.transform.localRotation = Quaternion.Euler(0, posArr[3], 0)
        end
        table.insert(self.m_ChildROList, childRO)
    end
end

function LuaShiTuMainWndShiTuView:OnMainPlayerJoinGroupIM(args)
    local id = args[0]
    local im = CClientMainPlayer.Inst and CClientMainPlayer.Inst.RelationshipProp.JoinedGroupIM[id]
    if im and im.Type == CGroupIMMgr.GroupIMType_ShiTu and self.m_IsCreatingShiTuGroupIm then
        CChatHelper.ShowGroupChat(id)
        self.m_IsCreatingShiTuGroupIm = false
    end
end

function LuaShiTuMainWndShiTuView:OnDestroy()
    CUIManager.DestroyModelTexture(self.m_Identifier)
end
--@region UIEvent

function LuaShiTuMainWndShiTuView:OnEditShiMenNameButtonClick()
    local cost = ShiTu_Setting.GetData().ShiMenModifyName
    CInputBoxMgr.ShowInputBoxWithCost("", DelegateFactory.Action_string(function (str)
        if cs_string.IsNullOrEmpty(str) then
            g_MessageMgr:ShowMessage("Change_ShiMen_Name_Input_Empty")
            return
        elseif CClientMainPlayer.Inst and cost > CClientMainPlayer.Inst.Silver then
            local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function () 
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end), nil, nil, nil, false)
        elseif not CWordFilterMgr.Inst:CheckName(str) then
            g_MessageMgr:ShowMessage("Change_ShiMen_Name_Input_Error")
            return
        end
        Gac2Gas.RequestSetShiTuShiMenName(str)
    end),EnumMoneyType.YinLiang, cost, 16, true, nil, LocalString.GetString("师门名称最多8个字"))
end


function LuaShiTuMainWndShiTuView:OnQiYiButtonClick()
    Shop_PlayScore.ForeachKey(function (key) 
        local playScore = Shop_PlayScore.GetData(key)
        if playScore.Key == ToStringWrap(EnumPlayScoreKey.QYJF) then
            CLuaNPCShopInfoMgr.ShowScoreShopById(key)
            return
        end
    end)
end


function LuaShiTuMainWndShiTuView:OnChatButtonClick()
    local ims = CGroupIMMgr.Inst:GetGroupIMs()
    for i = 0, ims.Count - 1 do
        if ims[i].Type == CGroupIMMgr.GroupIMType_ShiTu then
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= ims[i].OwnerId and self.m_IsTudiView then
                CChatHelper.ShowGroupChat(ims[i].GroupIMID)
                return
            elseif CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == ims[i].OwnerId and not self.m_IsTudiView then
                CChatHelper.ShowGroupChat(ims[i].GroupIMID)
                return 
            end
        end
    end
    if not self.m_IsTudiView then
        local msg = g_MessageMgr:FormatMessage("CreateShiTuGroupIm_Confirm")
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
            self.m_IsCreatingShiTuGroupIm = true
            Gac2Gas.RequestCreateShiTuGroupIm()
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    else
        if not CSwitchMgr.EnableGroupIM then
            g_MessageMgr:ShowMessage("ShiTuMainWndShiTuView_ChatButtonClick_TuDi_GroupIM_Limit")
            return
        else
            Gac2Gas.TryJoinShiMenGroupIM()
        end
    end
end

function LuaShiTuMainWndShiTuView:OnGiftButtonClick()
    if self.m_IsTudiView then
        local shifuPlayerId = CShiTuMgr.Inst:GetCurrentShiFuId()
        LuaShiTuMgr:OpenShiTuGiftGivingWnd(shifuPlayerId, true)
    else
        LuaShiTuTrainingHandbookMgr:ShowTuDiPlayerListWnd(LocalString.GetString("礼赠"), function(playerId)
            LuaShiTuMgr:OpenShiTuGiftGivingWnd(playerId, false)
        end)
    end
end

function LuaShiTuMainWndShiTuView:OnActivityButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ShiTuHistoryWnd)
end

function LuaShiTuMainWndShiTuView:OnLeaveButtonClick()
    local txt = g_MessageMgr:FormatMessage("TuDi_Leave_ShiMen_Confirm")
    MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function () 
        Gac2Gas.BreakShiTuRelationship(true, 0)
        CUIManager.CloseUI(CLuaUIResources.ShiTuMainWnd)
    end), nil, nil, nil, false)
end


function LuaShiTuMainWndShiTuView:OnTipButtonClick()
    g_MessageMgr:ShowMessage("ShiTuMainWndShiTuView_ReadMe")
end

function LuaShiTuMainWndShiTuView:OnQiuSuoButtonClick()
    g_MessageMgr:ShowMessage("ShiTuZhuYeMian_QingYiChunQiu_Button")
end

function LuaShiTuMainWndShiTuView:OnWenDaoButtonClick()
    if self.m_IsTudiView then
        if CClientMainPlayer.Inst then
            local relationshipProp = CClientMainPlayer.Inst.RelationshipProp
            local infoDic = relationshipProp.ShiFu
            local shifuId = CShiTuMgr.Inst:GetCurrentShiFuId()
            local info = CommonDefs.DictGetValue(infoDic, typeof(UInt64), shifuId)
            local isWaiMen = info and info.Extra:GetBit(2) or false
            if isWaiMen then
                g_MessageMgr:ShowMessage("ShiTuView_WaiMenDiZi_Forbid_ShowWenDaoView")
                return
            end
        end
        LuaShiTuTrainingHandbookMgr:ShowWndForStudent()
    else
        LuaShiTuTrainingHandbookMgr:ShowWndForTeacher()
    end
end


function LuaShiTuMainWndShiTuView:OnDianCeButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ShiTuDirectoryWnd)
end


function LuaShiTuMainWndShiTuView:OnMoreDiZiButtonClick()
    CUIManager.ShowUI(CLuaUIResources.WaiMenDiZiWnd)
end


--@endregion UIEvent

