local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

local UILabel = import "UILabel"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIGrid = import "UIGrid"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"
local EnumClass = import "L10.Game.EnumClass"
local Object = import "System.Object"
local CSkillMgr = import "L10.Game.CSkillMgr"

LuaSuggestSkillWnd = class()
LuaSuggestSkillWnd.m_YingLingState = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSuggestSkillWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaSuggestSkillWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaSuggestSkillWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaSuggestSkillWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaSuggestSkillWnd, "Introduction", "Introduction", UILabel)
RegistChildComponent(LuaSuggestSkillWnd, "SkillTemplate", "SkillTemplate", GameObject)
RegistChildComponent(LuaSuggestSkillWnd, "SkillGrid", "SkillGrid", UIGrid)
RegistChildComponent(LuaSuggestSkillWnd, "TalentGrid", "TalentGrid", UIGrid)
RegistChildComponent(LuaSuggestSkillWnd, "TalentTemplate", "TalentTemplate", GameObject)
RegistChildComponent(LuaSuggestSkillWnd, "ApplyBtn", "ApplyBtn", GameObject)
RegistChildComponent(LuaSuggestSkillWnd, "SkillWidget", "SkillWidget", GameObject)
RegistChildComponent(LuaSuggestSkillWnd, "YingLingSwitchRoot", "YingLingSwitchRoot", CCommonLuaScript)

--@endregion RegistChildComponent end

function LuaSuggestSkillWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    if self.suggestCfgIdList == nil or self.suggestCfgIdList == {} or #self.suggestCfgIdList == 0 then
        return
    end
    LuaSkillPreferenceViewYingLingSwitchRoot4SkillSuggestion.m_YingLingState = LuaSuggestSkillWnd.m_YingLingState
    self:RefreshConstUI()
    self:InitUIEvent()
end

function LuaSuggestSkillWnd:Init()
    if self.suggestCfgIdList == nil or self.suggestCfgIdList == {} or #self.suggestCfgIdList == 0 then
        CUIManager.CloseUI("SuggestSkillWnd")
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaSuggestSkillWnd:InitUIEvent()
    UIEventListener.Get(self.ApplyBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        --应用技能与天赋
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Recommended_Skill_Confirm", ""), function()
            self:OnClickApplyBtn()
        end, nil, nil, nil, false)
    end)
end

function LuaSuggestSkillWnd:OnClickApplyBtn()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local skillProperty = mainPlayer.SkillProp
    local mainPlayerXiuWeiGrade = mainPlayer.BasicProp.XiuWeiGrade
    local mainPlayerLevel = mainPlayer.MaxLevel

    local skillTbl = {}
    skillProperty:ForeachOriginalSkillId(DelegateFactory.Action_KeyValuePair_uint_uint(function (kv)
        local skillId = kv.Key
        local skillCls = math.floor(skillId / 100)

        --local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(skillCls, mainPlayerLevel)
        skillTbl[skillCls] = skillId

        --如果是色系其他技能/色系基础技能的话, 把其他技能也存储一下
        if CSkillMgr.Inst:IsColorSystemSkill(skillCls) or CSkillMgr.Inst:IsColorSystemBaseSkill(skillCls) then
            local baseSkillCls = skillCls
            local skillLevel = skillId % 100
            if CSkillMgr.Inst:IsColorSystemSkill(skillCls) then
                baseSkillCls = Skill_ColorSkill.GetData(skillCls).BaseSkill_ID
            end
            Skill_ColorSkill.Foreach(function (key, val)
                if val.BaseSkill_ID == baseSkillCls then
                    if skillLevel >= val.NeedBaseSkillLevel then
                        skillTbl[key] = key * 100 + skillLevel
                        skillTbl[baseSkillCls] = baseSkillCls * 100 + skillLevel
                    end
                end
            end)
        end
    end))

    local list1 = CreateFromClass(MakeGenericClass(List, Object))
    local tianfuSkills = CreateFromClass(MakeGenericClass(List, Object))
    for i = 1, #self.clickSkillIdList do
        if skillTbl[self.clickSkillIdList[i]] then
            CommonDefs.ListAdd(list1, typeof(UInt32), skillTbl[self.clickSkillIdList[i]])
        else
            local commonAttackSkillId = math.floor(self.clickSkillIdList[i] / 1000) * 100000 + 1
            CommonDefs.ListAdd(list1, typeof(UInt32), commonAttackSkillId)
        end
    end

    local totalPoint = mainPlayer.SkillProp.ExtraTianFuPoint + math.floor(mainPlayer.BasicProp.XiuWeiGrade)
    local pointContribution = {0, 0, 0, 0}
    for i = 1, #self.clickTalentIdList do
        local talentId = tonumber(self.clickTalentIdList[i])
        local fakeTalentId = talentId * 100 + 1
        local skillData = Skill_AllSkills.GetData(fakeTalentId)
        if mainPlayerLevel >= skillData.PlayerLevel then
            if mainPlayerXiuWeiGrade >= skillData.NeedXiuweiLevel then
                totalPoint = totalPoint - 1
                pointContribution[i] = 1
            end
        end
    end

    for i = 1, #self.clickTalentIdList do
        local talentId = tonumber(self.clickTalentIdList[i])
        for maxPoint = math.min(totalPoint+pointContribution[i], 40), 1, -1 do
            local fakeTalentId = talentId * 100 + maxPoint
            local skillData = Skill_AllSkills.GetData(fakeTalentId)
            if mainPlayerLevel >= skillData.PlayerLevel then
                if mainPlayerXiuWeiGrade >= skillData.NeedXiuweiLevel then
                    totalPoint = totalPoint - (maxPoint-pointContribution[i])
                    pointContribution[i] = maxPoint
                    break
                end
            end
        end
    end

    local targetTalentIdList = {}
    for i = 1, #self.clickTalentIdList do
        local talentId = tonumber(self.clickTalentIdList[i])
        if pointContribution[i] ~= 0 then
            CommonDefs.ListAdd(tianfuSkills, typeof(UInt32), talentId * 100 + pointContribution[i])
            targetTalentIdList[talentId] = talentId * 100 + pointContribution[i]
        end
    end

    CommonDefs.ListIterate(list1, DelegateFactory.Action_object(function (___value)
        local skillId = tonumber(___value)
        local skillCls = math.floor(skillId / 100)
        if CSkillMgr.Inst:IsColorSystemSkill(skillCls) or CSkillMgr.Inst:IsColorSystemBaseSkill(skillCls) then
            Gac2Gas.ApplyColorSystemSkill(skillCls)
        end
    end))

    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() and self.m_YingLingState ~= EnumToInt(mainPlayer.CurYingLingState) then
        Gac2Gas.RequestSaveYingLingExtraSkillTemplate(skillProperty.CurrentTemplateIdx, self.m_YingLingState, MsgPackImpl.pack(list1), MsgPackImpl.pack(list1), MsgPackImpl.pack(tianfuSkills))
    else
        Gac2Gas.RequestSaveSkill2Template(skillProperty.CurrentTemplateIdx, skillProperty.CurrentSkillTemplateName, MsgPackImpl.pack(list1), MsgPackImpl.pack(tianfuSkills), MsgPackImpl.pack(list1))
    end

    local modification = {}
    --skillTbl
    for k, v in pairs(skillTbl) do
        local cls = k
        local skillId = v
        local skillInfo = Skill_AllSkills.GetData(skillId)
        if skillInfo then
            if skillInfo.Kind == SkillKind_lua.TianFuSkill then
                --原来有的技能
                if targetTalentIdList[cls] then
                    table.insert(modification, cls)
                    table.insert(modification, targetTalentIdList[cls] - skillId)
                else
                    table.insert(modification, cls)
                    table.insert(modification, -(skillId % 100))
                end
            end
        end
    end
    for k, v in pairs(targetTalentIdList) do
        --目标天赋
        local cls = k
        local skillId = v
        if skillTbl[cls] == nil then
            table.insert(modification, cls)
            table.insert(modification, skillId % 100)
        end
    end
    if #modification > 0 then
        Gac2Gas.RequestUpgradeTianFuSkill(MsgPackImpl.pack(Table2List(modification, MakeGenericClass(List, Object))))
    end

    --Gac2Gas.ClientLogCount(EnumClientLogCountKey.RecommendSkills, 1)
end

function LuaSuggestSkillWnd:InitWndData()
    local mainPlayerCls =  CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) or 0
    local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
    self.suggestCfgIdList = {}
    self.yingLingCfgIdList = {[0] = {}, [1] = {}, [2] = {}, [3] = {}}

    Skill_SuggestSkill2023.Foreach(function (key, data)
        if mainPlayerCls == data.Class and playerLv >= data.RequireLevelMin and (data.RequireLevelMax == -1 and true or playerLv <= data.RequireLevelMax) then
            if CClientMainPlayer.Inst.Class  == EnumClass.YingLing then
                --是影灵职业
                local yingLingState = LuaSkillMgr.GetDefaultYingLingState()
                if yingLingState == data.YingLingState then
                    table.insert(self.suggestCfgIdList, key)
                end
                table.insert(self.yingLingCfgIdList[data.YingLingState], key)
            else
                table.insert(self.suggestCfgIdList, key)
            end
        end
    end)

    self.clickTalentIdList = {}
    self.clickSkillIdList = {}
end

function LuaSuggestSkillWnd:RefreshConstUI()
    Extensions.RemoveAllChildren(self.Tabs.transform)
    self.TabTemplate:SetActive(false)
    for i = 1, #self.suggestCfgIdList do
        local suggestId = self.suggestCfgIdList[i]
        local suggestCfgData = Skill_SuggestSkill2023.GetData(suggestId)
        local tag = NGUITools.AddChild(self.Tabs.gameObject, self.TabTemplate)
        tag:SetActive(true)
        tag.transform:Find("Label"):GetComponent(typeof(UILabel)).text = suggestCfgData.Type
    end
    self.Tabs.transform:GetComponent(typeof(UIGrid)):Reposition()
    self.Tabs:ReloadTabButtons()
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:ClickType(self.suggestCfgIdList[index+1])
    end)
    self.Tabs:ChangeTab(0, false)
end

function LuaSuggestSkillWnd:ClickType(suggestId)
    if not CClientMainPlayer.Inst then
        return
    end

    local mainPlayer = CClientMainPlayer.Inst
    local mainPlayerXiuWeiGrade = mainPlayer.BasicProp.XiuWeiGrade
    local mainPlayerLevel = mainPlayer.MaxLevel

    local suggestCfgData = Skill_SuggestSkill2023.GetData(suggestId)
    self.TitleLabel.text = suggestCfgData.Title
    self.Introduction.text = CUICommonDef.TranslateToNGUIText(suggestCfgData.Introduction)
    if suggestCfgData.RequireLevelMax == -1 then
        self.LevelLabel.text = suggestCfgData.RequireLevelMin .. LocalString.GetString("级后")
    else
        self.LevelLabel.text = suggestCfgData.RequireLevelMin .. LocalString.GetString("-") .. suggestCfgData.RequireLevelMax .. LocalString.GetString("级")
    end

    local skillProperty = mainPlayer.SkillProp
    local skillTbl = {}
    skillProperty:ForeachOriginalSkillId(DelegateFactory.Action_KeyValuePair_uint_uint(function (kv)
        local skillId = kv.Key
        local skillCls = math.floor(skillId / 100)

        local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(skillCls, mainPlayerLevel)
        skillTbl[skillCls] = originSkillIdWithDeltaLevel
        --skillTbl[skillCls] = skillId
        --如果是色系其他技能/色系基础技能的话, 把其他技能也存储一下
        if CSkillMgr.Inst:IsColorSystemSkill(skillCls) or CSkillMgr.Inst:IsColorSystemBaseSkill(skillCls) then
            local baseSkillCls = skillCls
            local skillLevel = skillId % 100
            if CSkillMgr.Inst:IsColorSystemSkill(skillCls) then
                baseSkillCls = Skill_ColorSkill.GetData(skillCls).BaseSkill_ID
                --skillTbl[baseSkillCls] = baseSkillCls * 100 + skillLevel
                skillTbl[baseSkillCls] = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(baseSkillCls, mainPlayerLevel)
            end
            Skill_ColorSkill.Foreach(function (key, val)
                if val.BaseSkill_ID == baseSkillCls then
                    if skillLevel >= val.NeedBaseSkillLevel then
                        --skillTbl[key] = key * 100 + skillLevel
                        skillTbl[key] = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(key, mainPlayerLevel)
                    end
                end
            end)
        end
    end))

    --refresh skill grid
    Extensions.RemoveAllChildren(self.SkillGrid.transform)
    self.SkillTemplate:SetActive(false)
    self.clickSkillIdList = {}
    self.clickTalentIdList = {}
    local skillRawData = suggestCfgData.Skills
    local skillRawDataList = g_LuaUtil:StrSplit(skillRawData, ";")
    for i = 1, #skillRawDataList do
        if skillRawDataList[i] == "" then
            break
        end

        local skillId = tonumber(skillRawDataList[i])
        local realSkillId = skillTbl[skillId] and skillTbl[skillId] or skillId * 100 + 1
        local fakeSkillId = skillId * 100 + 1
        table.insert(self.clickSkillIdList, skillId)

        local templateItem = CommonDefs.Object_Instantiate(self.SkillTemplate)
        templateItem:SetActive(true)
        templateItem.transform.parent = self.SkillGrid.transform
        templateItem.transform.localScale = Vector3.one
        if CSkillMgr.Inst:IsColorSystemSkill(skillId) or CSkillMgr.Inst:IsColorSystemBaseSkill(skillId) then
            templateItem.transform:Find("Panel").gameObject:SetActive(true)
            if CSkillMgr.Inst:IsColorSystemBaseSkill(skillId) then
                templateItem.transform:Find("Panel/ColorSystemIcon"):GetComponent(typeof(UISprite)).spriteName = "huahun_bai"
            else
                if CSkillMgr.Inst:IsColorSystemSkill(skillId) then
                    local colorId = Skill_ColorSkill.GetData(skillId).Color_ID
                    local nameList = {"huahun_hong", "huahun_lan", "huahun_huang"}
                    templateItem.transform:Find("Panel/ColorSystemIcon"):GetComponent(typeof(UISprite)).spriteName = nameList[colorId]
                end
            end
        else
            templateItem.transform:Find("Panel").gameObject:SetActive(false)
        end

        local skillData = Skill_AllSkills.GetData(fakeSkillId)
        local skillTexComp = templateItem.transform:GetComponent(typeof(CUITexture))
        skillTexComp:LoadSkillIcon(skillData.SkillIcon)

        if mainPlayerLevel >= skillData.PlayerLevel then
            --等级够
            if skillTbl[skillId] then
                templateItem.transform:Find("Perceptible").gameObject:SetActive(false)
                templateItem.transform:Find("Restricted").gameObject:SetActive(false)
            else
                templateItem.transform:Find("Perceptible").gameObject:SetActive(true)
                templateItem.transform:Find("Restricted").gameObject:SetActive(false)
            end
        else
            --等级不够
            templateItem.transform:Find("Perceptible").gameObject:SetActive(false)
            templateItem.transform:Find("Restricted").gameObject:SetActive(true)
            templateItem.transform:Find("Restricted"):GetComponent(typeof(UILabel)).text = LocalString.GetString("Lv.") .. skillData.PlayerLevel
        end

        UIEventListener.Get(templateItem).onClick = DelegateFactory.VoidDelegate(function (go)
            CSkillInfoMgr.ShowSkillInfoWnd(realSkillId, self.SkillWidget.transform:Find("SkillInfoSpot").position, EnumSkillInfoContext.Default, true, 0, 0, nil)
        end)
    end
    self.SkillGrid:Reposition()

    --refresh talent grid
    Extensions.RemoveAllChildren(self.TalentGrid.transform)
    self.TalentTemplate:SetActive(false)
    local talentRawData = suggestCfgData.Talents
    local talentRawDataList = g_LuaUtil:StrSplit(talentRawData, ";")
    for i = 1, #talentRawDataList do
        if talentRawDataList[i] == "" then
            break
        end

        local talentId = tonumber(talentRawDataList[i])
        local realTalentId = skillTbl[talentId] and skillTbl[talentId] or talentId * 100 + 1

        table.insert(self.clickTalentIdList, talentId)

        local templateItem = CommonDefs.Object_Instantiate(self.TalentTemplate)
        templateItem:SetActive(true)
        templateItem.transform.parent = self.TalentGrid.transform
        templateItem.transform.localScale = Vector3.one

        local skillData = Skill_AllSkills.GetData(realTalentId)
        local skillTexComp = templateItem.transform:GetComponent(typeof(CUITexture))
        skillTexComp:LoadSkillIcon(skillData.SkillIcon)

        if mainPlayerLevel >= skillData.PlayerLevel then
            --等级够
            if mainPlayerXiuWeiGrade >= skillData.NeedXiuweiLevel then
                --修为够
                templateItem.transform:Find("Perceptible").gameObject:SetActive(false)
                templateItem.transform:Find("Restricted").gameObject:SetActive(false)
            else
                --修为不够
                templateItem.transform:Find("Perceptible").gameObject:SetActive(false)
                templateItem.transform:Find("Restricted").gameObject:SetActive(true)
                templateItem.transform:Find("Restricted"):GetComponent(typeof(UILabel)).text = skillData.NeedXiuweiLevel .. LocalString.GetString("修")
            end
        else
            --等级不够, 也只显示修为
            templateItem.transform:Find("Perceptible").gameObject:SetActive(false)
            templateItem.transform:Find("Restricted").gameObject:SetActive(true)
            templateItem.transform:Find("Restricted"):GetComponent(typeof(UILabel)).text = skillData.NeedXiuweiLevel .. LocalString.GetString("修")
        end

        UIEventListener.Get(templateItem).onClick = DelegateFactory.VoidDelegate(function (go)
            CSkillInfoMgr.ShowSkillInfoWnd(realTalentId, self.SkillWidget.transform:Find("SkillInfoSpot").position, EnumSkillInfoContext.MainPlayerMPTZ, true, 0, 0, nil)
        end)
    end
    self.TalentGrid:Reposition()
end

function LuaSuggestSkillWnd:OnEnable()
    g_ScriptEvent:AddListener("SkillPreferenceViewSwitchYingLingState4SkillSuggestion",self,"OnYingLingStateSwitch")
end

function LuaSuggestSkillWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SkillPreferenceViewSwitchYingLingState4SkillSuggestion",self,"OnYingLingStateSwitch")
end

function LuaSuggestSkillWnd:OnYingLingStateSwitch(state)
    if self.suggestCfgIdList == nil or self.suggestCfgIdList == {} or #self.suggestCfgIdList == 0 then
        return
    end
    self.m_YingLingState = state
    self.suggestCfgIdList = self.yingLingCfgIdList[state]
    local selectIndex = self.Tabs.SelectedIndex
    self:ClickType(self.suggestCfgIdList[selectIndex+1])
end