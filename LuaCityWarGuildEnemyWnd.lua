require("common/common_include")

local UILabel = import "UILabel"
local UISprite = import "UISprite"
local NGUITools = import "NGUITools"
local Constants = import "L10.Game.Constants"
local UITable = import "UITable"
local UIEventListener = import "UIEventListener"
local UISearchView = import "L10.UI.UISearchView"
local LuaUtils = import "LuaUtils"
local Color = import "UnityEngine.Color"
local CButton = import "L10.UI.CButton"

CLuaCityWarGuildEnemyWnd = class()
CLuaCityWarGuildEnemyWnd.Path = "ui/citywar/LuaCityWarGuildEnemyWnd"

RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_TemplateObj")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_DataUITable")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_DataTable")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_DataObjTable")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_SetBtnLabel")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_SelectedItemObj")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_ItemObj2IdTable")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_ItemObj2IsOddTable")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_EnemyGuildId")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_CurrentDataTable")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_SetBtn")
RegistClassMember(CLuaCityWarGuildEnemyWnd, "m_NoGuildTipObj")

function CLuaCityWarGuildEnemyWnd:Awake( ... )
	Gac2Gas.QueryCityInfoForCityWarZhanLong()

	self.m_DataTable = {}
	self.m_DataObjTable = {}

	self.m_TemplateObj = self.transform:Find("Anchor/GuildList/ListView/Scrollview/Pool/GuildTemplate").gameObject
	self.m_TemplateObj:SetActive(false)
	self.m_DataUITable = self.transform:Find("Anchor/GuildList/ListView/Scrollview/Table"):GetComponent(typeof(UITable))

	UIEventListener.Get(self.transform:Find("Anchor/BottomView/Tips").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		g_MessageMgr:ShowMessage("CITYWAR_ZHANLONG_TIPS")
	end)
	self.m_SetBtnLabel = self.transform:Find("Anchor/BottomView/SetEnemyButton/Label"):GetComponent(typeof(UILabel))
	self.m_SetBtn = self.transform:Find("Anchor/BottomView/SetEnemyButton"):GetComponent(typeof(CButton))
	UIEventListener.Get(self.m_SetBtn.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		if self.m_SelectedItemObj then
			local guildId = self.m_ItemObj2IdTable[self.m_SelectedItemObj]
			Gac2Gas.RequestSetCityWarZhanLongEnemyGuildId(guildId == self.m_EnemyGuildId and 0 or guildId)
		end
	end)
	self.transform:Find("Anchor/BottomView/SearchView"):GetComponent(typeof(UISearchView)).OnSearch = DelegateFactory.Action_string(function (text)
		self:Search(text)
	end)
	self.m_NoGuildTipObj = self.transform:Find("Anchor/NoGuildTip").gameObject
end

function CLuaCityWarGuildEnemyWnd:Search(text)
	if not text then
		self:InitUITable(self.m_DataTable)
		return
	end
	local tbl = {}
	for k, v in ipairs(self.m_DataTable) do
		if string.find(v[1], text) or string.find(v[2], text) then
			table.insert(tbl, v)
		end
	end
	self:InitUITable(tbl)
end

function CLuaCityWarGuildEnemyWnd:SetCityWarZhanLongEnemyGuildIdSuccess(guildId)
	self.m_EnemyGuildId = guildId
	self:InitUITable(self.m_CurrentDataTable)
end

function CLuaCityWarGuildEnemyWnd:QueryCityInfoForCityWarZhanLongResult(guildId, guildInfo_U)
	self.m_EnemyGuildId = guildId
	local list = MsgPackImpl.unpack(guildInfo_U)
	if not list then return end
	if list.Count > 0 then self.m_NoGuildTipObj:SetActive(false) end
	self.m_DataTable = {}
	for i = 0, list.Count - 1 do
		local tbl = {}
		for j = 0, list[i].Count - 1 do
			table.insert(tbl, list[i][j])
		end
		table.insert(self.m_DataTable, tbl)
	end
	self:InitUITable(self.m_DataTable)
end

function CLuaCityWarGuildEnemyWnd:InitUITable(dataTbl)
	self.m_ItemObj2IdTable = {}
	self.m_ItemObj2IsOddTable = {}
	self.m_CurrentDataTable = dataTbl
	for k, v in ipairs(dataTbl) do
		local obj
		if self.m_DataObjTable[k] then
			obj = self.m_DataObjTable[k]
		else
			obj = NGUITools.AddChild(self.m_DataUITable.gameObject, self.m_TemplateObj)
			self.m_DataObjTable[k] = obj
		end
		obj:SetActive(true)
		self:InitUIItem(obj, v, k % 2 == 0)
		self.m_ItemObj2IdTable[obj] = v[1]
	end
	for i = #dataTbl + 1, #self.m_DataObjTable do
		self.m_DataObjTable[i]:SetActive(false)
	end
	self.m_DataUITable:Reposition()
	self.m_SetBtn.Enabled = false
end

function CLuaCityWarGuildEnemyWnd:InitUIItem(obj, dataItem, isOdd)
	obj:GetComponent(typeof(UISprite)).spriteName = isOdd and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
	self.m_ItemObj2IsOddTable[obj] = isOdd
	local transNameTable = {"Id", "Name", "Region", "Location", "Level"}
	for i = 1, #transNameTable do
		obj.transform:Find(transNameTable[i]):GetComponent(typeof(UILabel)).text = dataItem[i]
	end
	local relationshipLabel = obj.transform:Find("Relationship"):GetComponent(typeof(UILabel))
	relationshipLabel.text = dataItem[1] == self.m_EnemyGuildId and LocalString.GetString("重点敌对") or LocalString.GetString("无")
	relationshipLabel.color = dataItem[1] == self.m_EnemyGuildId and Color.red or NGUIText.ParseColor24("112C4C", 0)
	--if self.m_EnemyGuildId == dataItem[1] then relationshipLabel.color = Color.red end
	UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnItemSelected(obj)
	end)
end

function CLuaCityWarGuildEnemyWnd:OnItemSelected(obj)
	if self.m_SelectedItemObj then 
		self.m_SelectedItemObj:GetComponent(typeof(UISprite)).spriteName = self.m_ItemObj2IsOddTable[self.m_SelectedItemObj] and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
	end
	self.m_SelectedItemObj = obj
	self.m_SelectedItemObj:GetComponent(typeof(UISprite)).spriteName = Constants.ChosenItemBgSpriteName
	self.m_SetBtn.Enabled = true
	self.m_SetBtnLabel.text = self.m_ItemObj2IdTable[self.m_SelectedItemObj] ~= self.m_EnemyGuildId and LocalString.GetString("重点敌对") or LocalString.GetString("取消敌对")
end

function CLuaCityWarGuildEnemyWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("SetCityWarZhanLongEnemyGuildIdSuccess", self, "SetCityWarZhanLongEnemyGuildIdSuccess")
	g_ScriptEvent:AddListener("QueryCityInfoForCityWarZhanLongResult", self, "QueryCityInfoForCityWarZhanLongResult")
end

function CLuaCityWarGuildEnemyWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SetCityWarZhanLongEnemyGuildIdSuccess", self, "SetCityWarZhanLongEnemyGuildIdSuccess")
	g_ScriptEvent:RemoveListener("QueryCityInfoForCityWarZhanLongResult", self, "QueryCityInfoForCityWarZhanLongResult")
end
