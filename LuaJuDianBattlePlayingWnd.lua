local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local EnumEvent = import "L10.Game.EnumEvent"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local CScene = import "L10.Game.CScene"

LuaJuDianBattlePlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattlePlayingWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaJuDianBattlePlayingWnd, "RankRoot", "RankRoot", GameObject)
RegistChildComponent(LuaJuDianBattlePlayingWnd, "SceneInfoRoot", "SceneInfoRoot", GameObject)
RegistChildComponent(LuaJuDianBattlePlayingWnd, "JuDianInfoRoot", "JuDianInfoRoot", GameObject)
RegistChildComponent(LuaJuDianBattlePlayingWnd, "NoneJuDianHintLabel", "NoneJuDianHintLabel", GameObject)
RegistChildComponent(LuaJuDianBattlePlayingWnd, "NoneBangHuiHint", "NoneBangHuiHint", GameObject)

--@endregion RegistChildComponent end

function LuaJuDianBattlePlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattlePlayingWnd:InitWnd()

    -- 初始化当前地图的据点信息
    local mapId = 0
    if CScene.MainScene then
        mapId = CScene.MainScene.SceneTemplateId
    end

    self.PixelPosition = {}
    local mapData = GuildOccupationWar_MapInfo.GetData(mapId)
    if mapData then
        for i=0, mapData.PixelPosition.Length -1, 2 do
            local data = {}
            data.x = mapData.PixelPosition[i]
            data.y = mapData.PixelPosition[i+1]
            table.insert(self.PixelPosition, data)
        end
    end

    local r = GuildOccupationWar_Setting.GetData().StrongholdCaptureRadius
    self.JuDianRadius = r * 64 * r * 64

    if LuaJuDianBattleMgr.PlayingShowMode == EnumJuDianPlayingShowMode.Nothing then
        self:OnNotInBattleSever()
    elseif LuaJuDianBattleMgr.PlayingShowMode == EnumJuDianPlayingShowMode.GuildScore then
        -- 排名
    end

    self:CheckState(true)
end

function LuaJuDianBattlePlayingWnd:ClearShow()
    self.RankRoot:SetActive(false)
    self.SceneInfoRoot:SetActive(false)
    self.JuDianInfoRoot:SetActive(false)
    self.NoneJuDianHintLabel:SetActive(false)
end

function LuaJuDianBattlePlayingWnd:IsInJuDian()
    return LuaJuDianBattleMgr.IsInsideJuDian
end

function LuaJuDianBattlePlayingWnd:CheckState(isInit)
    -- 更新状态
    if LuaJuDianBattleMgr.PlayingShowMode == EnumJuDianPlayingShowMode.JuDianOverview or 
        LuaJuDianBattleMgr.PlayingShowMode == EnumJuDianPlayingShowMode.JuDianDetail then
        local isIn = self:IsInJuDian()

        if isIn then
            -- 据点
            if LuaJuDianBattleMgr.JuDianPeopleData then
                self:OnPlayerRankData(LuaJuDianBattleMgr.JuDianPeopleData)
            end

            if LuaJuDianBattleMgr.JuDianProgressData then
                self:OnJuDianProgressData(LuaJuDianBattleMgr.JuDianProgressData)
            end
        else
            -- 场景
            if LuaJuDianBattleMgr.SceneFlagData and LuaJuDianBattleMgr.SceneJuDianData then
                self:OnJuDianAndFlagData(LuaJuDianBattleMgr.SceneJuDianData, LuaJuDianBattleMgr.SceneFlagData)
            end
        end
    end
end

function LuaJuDianBattlePlayingWnd:OnNotInBattleSever()
    -- 不在分线的提示
    self:ClearShow()
    self.NoneJuDianHintLabel:SetActive(false)
end

function LuaJuDianBattlePlayingWnd:OnRankData(rankData)
    -- 显示排名
    self:ClearShow()
    self.RankRoot:SetActive(true)

    local selfGuildId = 0
    if CClientMainPlayer.Inst then
        selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
    end

    local hasRankShow = false
    for i=1, #rankData do
        local data = rankData[i]
        local rank = data.rankPos
        if rank <= 4 and rank >=1 then
            local item = self.RankRoot.transform:Find("Rank/".. tostring(rank)).gameObject
            self:InitRankItem(item, data)
            hasRankShow = true
        end

        if data.guildId == selfGuildId then
            self:InitSelfRankItem(data)
        end
    end

    self.NoneBangHuiHint:SetActive(not hasRankShow)
    self.RankRoot.transform:Find("Rank").gameObject:SetActive(hasRankShow)
end

function LuaJuDianBattlePlayingWnd:InitRankItem(item, data)
    local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local count = item.transform:Find("Count"):GetComponent(typeof(UILabel))

    name.text = data.guildName
end

function LuaJuDianBattlePlayingWnd:InitSelfRankItem(data)
    local item = self.RankRoot.transform:Find("Self").gameObject
    self:InitRankItem(item, data)

    -- 处理名次
    local rank = data.rankPos

    for i=1,3 do
        local rankTexture = item.transform:Find("RankTexture/r".. tostring(i)).gameObject
        rankTexture:SetActive(i == rank)
    end

    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    if rank <= 3 then
        rankLabel.text = ""
    else
        rankLabel.text = tostring(rank)
    end
    
end

-- data.rankPos = list[i]
-- 		data.guildId = list[i+1]
-- 		data.guildName = list[i+2]
-- 		data.peopleCount = list[i+3]
function LuaJuDianBattlePlayingWnd:OnPlayerRankData(peopleCountData)
    self.m_PeopleCountData = peopleCountData
    if not self:IsInJuDian() then
        -- 场景
        if LuaJuDianBattleMgr.SceneFlagData and LuaJuDianBattleMgr.SceneJuDianData then
            self:OnJuDianAndFlagData(LuaJuDianBattleMgr.SceneJuDianData, LuaJuDianBattleMgr.SceneFlagData)
        end
    else
        -- 显示据点人数和进度
        self:ClearShow()
        self.JuDianInfoRoot:SetActive(true)

        if LuaJuDianBattleMgr.JuDianProgressData then
            self:OnJuDianProgressData(LuaJuDianBattleMgr.JuDianProgressData)
        end
    end
end

-- data.judianIdx = list2[i]
-- data.takenGuildId = list2[i+1]
-- data.progress = list2[i+2]
-- data.x = list2[i+3]
-- data.y = list2[i+4]
-- data.engineId = list2[i+5]
function LuaJuDianBattlePlayingWnd:OnJuDianProgressData(judianData)
    local myGuildId = LuaJuDianBattleMgr:GetSelfGuildId()

    -- 不管怎么样 都要去刷新进度
    for i=1, #judianData do
        -- 找到据点进行显示
        local data = judianData[i]
        local clientObj = CClientObjectMgr.Inst:GetObject(data.engineId)
        if clientObj then
            clientObj.HpFull = 100
            clientObj.Hp = data.progress
            clientObj.showHpBar = true
            EventManager.BroadcastInternalForLua(EnumEventType.HpUpdate,{ clientObj.EngineId })
            if data.takenGuildId == 0 then
                -- 白色
                clientObj:ShowGuanNingFx(true,2)
            elseif data.takenGuildId == myGuildId then
                -- 蓝色
                clientObj:ShowGuanNingFx(true,1)
            else
                -- 红色
                clientObj:ShowGuanNingFx(true,0)
            end
        end
    end

    if not self:IsInJuDian() then
        if LuaJuDianBattleMgr.SceneFlagData and LuaJuDianBattleMgr.SceneJuDianData then
            self:OnJuDianAndFlagData(LuaJuDianBattleMgr.SceneJuDianData, LuaJuDianBattleMgr.SceneFlagData)
        end
        return
    end

    self:ClearShow()
    self.JuDianInfoRoot:SetActive(true)
    
    local progress = self.JuDianInfoRoot.transform:Find("Progress"):GetComponent(typeof(UISlider))
    local foreground = self.JuDianInfoRoot.transform:Find("Progress/Foreground"):GetComponent(typeof(UISprite))
    local icon = progress.gameObject.transform:Find("Icon"):GetComponent(typeof(UITexture))
    local title = progress.gameObject.transform:Find("Title"):GetComponent(typeof(UILabel))

    local juDianGuild = 0
    local engineId = 0
    for i=1, #judianData do
        -- 找到据点进行显示
        local data = judianData[i]
        local clientObj = CClientObjectMgr.Inst:GetObject(data.engineId)
        if clientObj then
            engineId = data.engineId
            if data.takenGuildId == 0 then
                icon.color = JuDianLabelColor.Battle
                title.color = JuDianLabelColor.Battle
                foreground.color = JuDianLabelColor.Battle
            elseif data.takenGuildId == myGuildId then
                icon.color = JuDianLabelColor.Self
                title.color = JuDianLabelColor.Self
                foreground.color = JuDianLabelColor.Self
            else
                icon.color = JuDianLabelColor.Enemy
                title.color = JuDianLabelColor.Enemy
                foreground.color = JuDianLabelColor.Enemy
            end
            juDianGuild = data.takenGuildId
            progress.value = data.progress/100
            title.text = LocalString.GetString("据点") .. tostring(data.judianIdx)
        end
    end

    for i = 1, 3 do
        local item = self.JuDianInfoRoot.transform:Find("JuDian/" .. tostring(i))
        item.gameObject:SetActive(false)
    end

    local itemIndex = 0
    for i=1, #self.m_PeopleCountData do
        local data = self.m_PeopleCountData[i]
        if data.judianEngineId == engineId then
            itemIndex = itemIndex + 1
            local item = self.JuDianInfoRoot.transform:Find("JuDian/" .. tostring(itemIndex))
            local amount = item:Find("Amount"):GetComponent(typeof(UILabel))
            local name = item:Find("Name"):GetComponent(typeof(UILabel))
            local state = item:Find("State"):GetComponent(typeof(UILabel))

            item.gameObject:SetActive(true)
            name.text = data.guildName
            amount.text = SafeStringFormat3(LocalString.GetString("%s人"), tostring(data.peopleCount))

            if data.guildId == judianData then
                state.text = LocalString.GetString("占领中")
            else
                state.text = ""
            end

            -- 设置颜色
            if data.guildId == myGuildId then
                state.color = JuDianLabelColor.Self
                amount.color = JuDianLabelColor.Self
                name.color = JuDianLabelColor.Self
            elseif data.guildId == juDianGuild then
                state.color = JuDianLabelColor.Enemy
                amount.color = JuDianLabelColor.Enemy
                name.color = JuDianLabelColor.Enemy
            else
                state.color = Color(1,1,1)
                amount.color = Color(1,1,1)
                name.color = Color(1,1,1)
            end
        end
    end
end

function LuaJuDianBattlePlayingWnd:OnJuDianAndFlagData(judianData, flagData)
    if self:IsInJuDian() then
        return
    end

    -- 显示据点和旗帜信息
    -- 场景信息
    self:ClearShow()
    self.SceneInfoRoot:SetActive(true)

    local selfId = LuaJuDianBattleMgr:GetSelfGuildId()
    -- 据点信息
    for i=1, 3 do
        local data = judianData[i]
        local item = self.SceneInfoRoot.transform:Find("JuDian/"..tostring(i)).gameObject

        local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
        local icon = item.transform:Find("Icon"):GetComponent(typeof(UITexture))
        local title = item.transform:Find("Title"):GetComponent(typeof(UILabel))

        if data and data.takenGuildId > 0 then
            name.text = data.takenGuildName
            if data.takenGuildId == selfId then
                name.color = JuDianLabelColor.Self
                icon.color = JuDianLabelColor.Self
                title.color = JuDianLabelColor.Self
            else
                name.color = JuDianLabelColor.Enemy
                icon.color = JuDianLabelColor.Enemy
                title.color = JuDianLabelColor.Enemy
            end
        else
            name.text = LocalString.GetString("争夺中...")
            name.color = JuDianLabelColor.Battle
            icon.color = JuDianLabelColor.Battle
            title.color = JuDianLabelColor.Battle
        end
    end

    local flag = self.SceneInfoRoot.transform:Find("Flag").gameObject
    local flagIcon = self.SceneInfoRoot.transform:Find("Flag/Icon"):GetComponent(typeof(UITexture))
    local flagGuildLabel = self.SceneInfoRoot.transform:Find("Flag/Name"):GetComponent(typeof(UILabel))
    local title = self.SceneInfoRoot.transform:Find("Flag/Title"):GetComponent(typeof(UILabel))

    flag:SetActive(true)
    -- 旗帜信息
    if flagData and flagData.takenGuildId > 0 then
        local takenGuildId = flagData.takenGuildId
        local takenGuildName = flagData.takenGuildName

        flagGuildLabel.text = takenGuildName

        if flagData.takenGuildId == selfId then
            flagIcon.color = JuDianLabelColor.Self
            flagGuildLabel.color = JuDianLabelColor.Self
            title.color = JuDianLabelColor.Self
        else
            flagIcon.color = JuDianLabelColor.Enemy
            flagGuildLabel.color = JuDianLabelColor.Enemy
            title.color = JuDianLabelColor.Enemy
        end
    elseif flagData then
        flagGuildLabel.text = LocalString.GetString("争夺中...")
        flagIcon.color = JuDianLabelColor.Battle
        flagGuildLabel.color = JuDianLabelColor.Battle
        title.color = JuDianLabelColor.Battle
    else
        flag:SetActive(false)
    end
end

function LuaJuDianBattlePlayingWnd:OnEnable()
    self:InitWnd()
    g_ScriptEvent:AddListener("GuildJuDianSyncNoneFightSceneIdx", self, "OnNotInBattleSever")
    g_ScriptEvent:AddListener("GuildJuDianSyncCurrentSceneProgress", self, "OnJuDianAndFlagData")
    g_ScriptEvent:AddListener("GuildJuDianSyncCurrentScenePlayerCountRank", self, "OnPlayerRankData")
    g_ScriptEvent:AddListener("GuildJuDianSyncCurrentSceneJuDianProgress", self, "OnJuDianProgressData")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaJuDianBattlePlayingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSyncNoneFightSceneIdx", self, "OnNotInBattleSever")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncCurrentSceneProgress", self, "OnJuDianAndFlagData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncCurrentScenePlayerCountRank", self, "OnPlayerRankData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncCurrentSceneJuDianProgress", self, "OnJuDianProgressData")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end

    UnRegisterTick(self.CheckTick)
end

function LuaJuDianBattlePlayingWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@region UIEvent

function LuaJuDianBattlePlayingWnd:OnExpandButtonClick()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end


--@endregion UIEvent

