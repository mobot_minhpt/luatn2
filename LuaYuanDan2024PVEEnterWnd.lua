local CUITexture = import "L10.UI.CUITexture"

local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CTeamMgr = import "L10.Game.CTeamMgr"
LuaYuanDan2024PVEEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "Time", "Time", UILabel)
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "Level", "Level", UILabel)
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "Reward", "Reward", CUITexture)
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "Obtained", "Obtained", GameObject)
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "TemplateNode", "TemplateNode", GameObject)
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "Team", "Team", UILabel)
RegistChildComponent(LuaYuanDan2024PVEEnterWnd, "Table", "Table", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanDan2024PVEEnterWnd, "m_RewardId")

function LuaYuanDan2024PVEEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Reward.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardClick()
	end)


	
	UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtnClick()
	end)


    --@endregion EventBind end
end
 
function LuaYuanDan2024PVEEnterWnd:Init()
	local TimeLimit = YuanDan2024_Setting.GetData().YuanDanPVETime
	local LevelLimit = YuanDan2024_Setting.GetData().XNCGLevelLimit
	local TeamLimit = YuanDan2024_Setting.GetData().YuanDanPVETeam
	
	self.Time.text = TimeLimit
	self.Level.text = SafeStringFormat3(LocalString.GetString("%s级以上"), LevelLimit)
	self.Team.text = TeamLimit
	self.m_RewardId = tonumber(YuanDan2024_Setting.GetData().YuanDanPVERewardID) or 21000491

	local Hour1, Min1, Hour2, Min2 = string.match(TimeLimit, "(%d+):(%d+)~(%d+):(%d+)")
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local beginTime = CreateFromClass(DateTime, now.Year, now.Month, now.Day, Hour1, Min1, 0)
	local endTine = CreateFromClass(DateTime, now.Year, now.Month, now.Day, Hour2, Min2, 0)
	if DateTime.Compare(now, beginTime) < 0 or DateTime.Compare(now, endTine) > 0 then
		self.Time.color = NGUIText.ParseColor24("ff6565", 0)
	end
	
	local minTeamMember = string.match(TeamLimit, "(%d+)")
	local teamMemberList = CTeamMgr.Inst:GetTeamMembersExceptMe()
	local count = teamMemberList.Count
	if count + 1 < tonumber(minTeamMember) then
		self.Team.color = NGUIText.ParseColor24("ff6565", 0)
	end

	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.Level < tonumber(LevelLimit) then
		self.Level.color = NGUIText.ParseColor24("ff6565", 0)
	else
		for i = 1, count do
			local member = teamMemberList[i - 1]
			if member.m_MemberLevel < tonumber(LevelLimit) then
				self.Level.color = NGUIText.ParseColor24("ff6565", 0)
				break
			end
		end
	end
	
	local rule = YuanDan2024_Setting.GetData().YuanDanPVERule
	local ruleList = g_LuaUtil:StrSplit(rule, "\n")
	for i = 1, #ruleList do
		local go = NGUITools.AddChild(self.Table, self.TemplateNode.gameObject)
		go:SetActive(true)
		local ruleLabel = go.transform:Find("name"):GetComponent("UILabel")
		ruleLabel.text = g_LuaUtil:StrSplit(ruleList[i], ";")[1]
	end
	
	local ItemData = Item_Item.GetData(self.m_RewardId)
	self.Reward:LoadMaterial(ItemData.Icon)
	local isGetReward = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXNCGDailyAwardTimes) or 0
	self.Obtained:SetActive(isGetReward == 1)
end

--@region UIEvent

function LuaYuanDan2024PVEEnterWnd:OnEnterBtnClick()
	Gac2Gas.EnterXinNianChuangGuanPlay()
end


function LuaYuanDan2024PVEEnterWnd:OnRewardClick()
	CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_RewardId, false, nil, AlignType.Right, 0, 0, 0, 0)
end


--@endregion UIEvent

