local CUIManager     = import "L10.UI.CUIManager"
local LocalString    = import "LocalString"
local CPlayerInfoMgr = import "CPlayerInfoMgr"

LuaPlayerReportMgr = class()

LuaPlayerReportMgr.GuildReportInfo = {}
LuaPlayerReportMgr.m_PlayerReportInfo = {}

function LuaPlayerReportMgr:ShowDefaultReportWnd(playerId, playerName)
	LuaPlayerReportMgr:ShowReportWnd(playerId, playerName, nil,
		CPlayerInfoMgr.PopupMenuInfo.context,
		CPlayerInfoMgr.PopupMenuInfo.reportMsg,
		CPlayerInfoMgr.PopupMenuInfo.nearestMsgs, nil,
		CPlayerInfoMgr.PopupMenuInfo.contextStr)
end

function LuaPlayerReportMgr:ShowReportWnd(playerId, playerName, customTitle, playerInfoContext, reportMsg, nearestMsgs, extraInfoTbl, context)
	self.m_PlayerReportInfo = {}
	self.m_PlayerReportInfo.playerId = playerId
	self.m_PlayerReportInfo.playerName = playerName
	self.m_PlayerReportInfo.customTitle = System.String.IsNullOrEmpty(customTitle) and LocalString.GetString("举报") or customTitle
	self.m_PlayerReportInfo.playerInfoContext = playerInfoContext
	self.m_PlayerReportInfo.reportMsg = reportMsg
	self.m_PlayerReportInfo.nearestMsgs = nearestMsgs
	self.m_PlayerReportInfo.extraInfoTbl = extraInfoTbl
	self.m_PlayerReportInfo.context = context and context or ""
	CUIManager.ShowUI(CUIResources.PlayerReportWnd)
end


function LuaPlayerReportMgr:ShowGuildReportWnd(guildId, guildName, type, customTitle, customNameDesc)
	self.GuildReportInfo.guildId = guildId
	self.GuildReportInfo.guildName = guildName
	self.GuildReportInfo.type = System.String.IsNullOrEmpty(type) and "" or type
	self.GuildReportInfo.customTitle = System.String.IsNullOrEmpty(customTitle) and LocalString.GetString("举报") or customTitle
	self.GuildReportInfo.customNameDesc = System.String.IsNullOrEmpty(customNameDesc) and LocalString.GetString("举报对象") or customNameDesc
	CUIManager.ShowUI("GuildReportWnd")
end

function LuaPlayerReportMgr:DoReportGuild(reportType, reason)
	local guildId = self.GuildReportInfo.guildId
	local type = self.GuildReportInfo.type
	if type == "banghui" then
		Gac2Gas.RequestAccuseGuild(guildId or 0, reportType or "", reason or "")
	elseif type == "zongpai" then
		Gac2Gas.RequestAccuseSect(guildId or 0, reportType or "", reason or "")
	end
end
