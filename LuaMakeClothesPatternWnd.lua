local CButton = import "L10.UI.CButton"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local Extensions = import "Extensions"
local UIPanel = import "UIPanel"
local Object = import "System.Object"
local MsgPackImpl = import "MsgPackImpl"

LuaMakeClothesPatternWnd = class()

RegistChildComponent(LuaMakeClothesPatternWnd, "m_ColorGroup","ColorGroup", QnRadioBox)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_PatternGroup","PatternGroup", QnRadioBox)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_TitleLabel","TitleLabel", UILabel)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_SaveButton","SaveButton", CButton)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_ShareButton","ShareButton", CButton)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_ClearButton","ClearButton", CButton)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_OthertView","OthertView", GameObject)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_CustomizeImageEditor","CustomizeImageEditor", CCommonLuaScript)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_TextureTemplate","TextureTemplate", GameObject)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_ShareView","ShareView", UIPanel)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_PatternRoot","PatternRoot", GameObject)
RegistChildComponent(LuaMakeClothesPatternWnd, "m_Bg","Bg", GameObject)

RegistClassMember(LuaMakeClothesPatternWnd,"m_SelectColor")
RegistClassMember(LuaMakeClothesPatternWnd,"m_SelectPattern")
RegistClassMember(LuaMakeClothesPatternWnd,"m_IsEditorImage")
RegistClassMember(LuaMakeClothesPatternWnd,"m_EditingTexture")
RegistClassMember(LuaMakeClothesPatternWnd,"m_Patterns")
RegistClassMember(LuaMakeClothesPatternWnd,"m_ColorsTexs")

function LuaMakeClothesPatternWnd:Init()
    self.m_Patterns = {}
    self.m_TextureTemplate:SetActive(false)
    self.m_SelectColor = 0
    self.m_ColorsTexs = {}
    for i = 0,self.m_ColorGroup.m_RadioButtons.Length - 1 do
        local tex = self.m_ColorGroup.m_RadioButtons[i].transform:Find("Texture"):GetComponent(typeof(CUITexture))
        table.insert(self.m_ColorsTexs,tex)
    end
    self.m_ColorGroup.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectColor(btn, index)
    end)
    self.m_ColorGroup:ChangeTo(self.m_SelectColor, true)
    self.m_SelectPattern = 0
    self.m_PatternGroup.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectPattern(btn, index)
    end)
    self.m_PatternGroup:ChangeTo(self.m_SelectPattern, true)

    UIEventListener.Get(self.m_SaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnSaveButtonClick()
    end)
    UIEventListener.Get(self.m_ClearButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnClearButtonClick()
    end)
    UIEventListener.Get(self.m_ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnShareButtonClick()
    end)
end

function LuaMakeClothesPatternWnd:OnSelectColor(btn, index)
    self.m_SelectColor = index + 1
    if #self.m_Patterns > 3 then
        return
    end
    if (#self.m_Patterns > 3) or (#self.m_Patterns < 1) then
        return
    end
    for i = 0, 2 do
        local array = {"white","red","gold"}
        local s = SafeStringFormat3("UI/Texture/Transparent/Material/makeclotheswnd_gouxian_%s_%s.mat",array[i + 1],i == index and "highlight" or "normal")
        self.m_ColorsTexs[i + 1]:LoadMaterial(s)
    end
    self.m_Patterns[#self.m_Patterns].color = self.m_SelectColor
    if self.m_EditingTexture then
        local mat = LuaMakeClothesMgr:GetPicture(self.m_SelectPattern, self.m_SelectColor)
        self.m_EditingTexture:LoadMaterial(mat)
    end
end

function LuaMakeClothesPatternWnd:OnSelectPattern(btn, index)
    if #self.m_Patterns >= 3 and not self.m_IsEditorImage then
        return
    end
    self.m_SelectPattern = index + 1

    if self.m_IsEditorImage then
        local mat = LuaMakeClothesMgr:GetPicture(self.m_SelectPattern, self.m_SelectColor)
        self.m_EditingTexture:LoadMaterial(mat)
        local len = #self.m_Patterns
        self.m_Patterns[len].pattern = self.m_SelectPattern
        return
    end

    self.m_IsEditorImage = true
    local instance = NGUITools.AddChild(self.m_PatternRoot.gameObject, self.m_TextureTemplate)
    instance:SetActive(true)
    local widget = instance:GetComponent(typeof(UIWidget))
    if self.m_EditingTexture then
        widget.depth = self.m_EditingTexture.uiTexture.depth + 1
    end
    self.m_EditingTexture = instance:GetComponent(typeof(CUITexture))
    local mat = LuaMakeClothesMgr:GetPicture(self.m_SelectPattern, self.m_SelectColor)
    self.m_EditingTexture:LoadMaterial(mat)
    self.m_CustomizeImageEditor:Init(function (go)
        self.m_IsEditorImage = false
    end,function (go)
        self.m_IsEditorImage = false
        self.m_EditingTexture = nil
        table.remove(self.m_Patterns, #self.m_Patterns)
        GameObject.Destroy(instance)
    end, 900, 300, widget, self.m_ShareView, widget.width, widget.height)
    table.insert(self.m_Patterns, {
        tex = self.m_EditingTexture,
        rawWidth = widget.width,
        rawHeight = widget.height,
        color = self.m_SelectColor,
        pattern = self.m_SelectPattern
    })
end

function LuaMakeClothesPatternWnd:OnSaveButtonClick()
    local list = CreateFromClass(MakeGenericClass(List, Object))
    for i = 1,#self.m_Patterns do
        local tex, rawWidth, rawHeight, color, pattern = self.m_Patterns[i].tex, self.m_Patterns[i].rawWidth, self.m_Patterns[i].rawHeight, self.m_Patterns[i].color, self.m_Patterns[i].pattern
        local x,y = tex.transform.localPosition.x,tex.transform.localPosition.y
        local scaleX ,scaleY = self.m_Patterns[i].tex.texture.width / rawWidth, self.m_Patterns[i].tex.texture.height / rawHeight
        local rotationZ = tex.transform.localEulerAngles.z
        CommonDefs.ListAdd(list, typeof(Object), x)
        CommonDefs.ListAdd(list, typeof(Object), y)
        CommonDefs.ListAdd(list, typeof(Object), scaleX)
        CommonDefs.ListAdd(list, typeof(Object), scaleY)
        CommonDefs.ListAdd(list, typeof(Object), rotationZ)
        CommonDefs.ListAdd(list, typeof(Object), color)
        CommonDefs.ListAdd(list, typeof(Object), pattern)
    end
    Gac2Gas.RequestSavePictureInfo(CLuaTaskMgr.m_MakeClothesWnd_TaskId, CLuaTaskMgr.m_MakeClothesWnd_ConditionId, LuaMakeClothesMgr.m_SelectPattern,
            MsgPackImpl.pack(list))
    CUIManager.CloseUI(CLuaUIResources.MakeClothesPatternWnd)
end

function LuaMakeClothesPatternWnd:OnClearButtonClick()
    self.m_IsEditorImage = false
    self.m_EditingTexture = nil
    self.m_Patterns = {}
    self.m_CustomizeImageEditor.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.m_PatternRoot.transform)
end

function LuaMakeClothesPatternWnd:OnShareButtonClick()
    self.m_OthertView:SetActive(false)
    local prePos = self.m_ShareView.transform.localPosition
    local bgOffsetX =  self.m_Bg.transform.localPosition.x - prePos.x
    --self.m_ShareView.transform.localPosition = Vector3.zero
    --self.m_Bg.transform.localPosition = Vector3(bgOffsetX, self.m_Bg.transform.localPosition.y, 0)
    CUICommonDef.CaptureScreen("screenshot", false, true, nil, DelegateFactory.Action_string_byte_Array(function (fullPath, jpgBytes)
        ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
        self.m_OthertView:SetActive(true)
        --self.m_ShareView.transform.localPosition = prePos
        --self.m_Bg.transform.localPosition = Vector3(prePos.x + bgOffsetX, self.m_Bg.transform.localPosition.y, 0)
    end), false)
end

function LuaMakeClothesPatternWnd:GetGuideGo(methodName)
    if methodName == "GetRotateBtn" then
        return self.m_CustomizeImageEditor.transform:Find("RotateBtn").gameObject
    elseif methodName == "GetAddPatternButton" then
        return self.m_PatternGroup.m_RadioButtons[1].gameObject
    elseif methodName == "GetChangeColorButton" then
        return self.m_ColorGroup.m_RadioButtons[1].gameObject
    elseif methodName == "GetSaveButton" then
        return self.m_SaveButton.gameObject
    elseif methodName == "GetCloseButton" then
        return self.m_CloseButton
    elseif methodName == "GetConfirmPatternButton" then
        return self.m_CustomizeImageEditor.transform:Find("ConfirmBtn").gameObject
    end
end
