local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType	= import "L10.UI.CItemInfoMgr+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CUIFx = import "L10.UI.CUIFx"
local Ease = import "DG.Tweening.Ease"

LuaQingMingCGYJMainWnd = class()

RegistChildComponent(LuaQingMingCGYJMainWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "row1", "row1", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "row2", "row2", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "row3", "row3", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "row4", "row4", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "textRow", "textRow", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "textVer", "textVer", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "node1", "node1", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "node2", "node2", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "node3", "node3", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "ResNode", "ResNode", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "nodeBig", "nodeBig", GameObject)
RegistChildComponent(LuaQingMingCGYJMainWnd, "TipBtn", "TipBtn", GameObject)

RegistClassMember(LuaQingMingCGYJMainWnd,"GridTable")
RegistClassMember(LuaQingMingCGYJMainWnd,"totalResNum")
RegistClassMember(LuaQingMingCGYJMainWnd,"BonusData")
RegistClassMember(LuaQingMingCGYJMainWnd,"BonusNodeTable")
RegistClassMember(LuaQingMingCGYJMainWnd,"IndexBonusNode")
RegistClassMember(LuaQingMingCGYJMainWnd,"TotalBonusNode")
RegistClassMember(LuaQingMingCGYJMainWnd,"saveActiveIndex")
RegistClassMember(LuaQingMingCGYJMainWnd,"moveFxTeam")
RegistClassMember(LuaQingMingCGYJMainWnd,"moveFxPositionTeam")
RegistClassMember(LuaQingMingCGYJMainWnd,"moveFxTotalPosition")
RegistClassMember(LuaQingMingCGYJMainWnd,"AwardGetTable")
RegistClassMember(LuaQingMingCGYJMainWnd,"TotalIndexBonusGet")

function LuaQingMingCGYJMainWnd:Awake()

end

function LuaQingMingCGYJMainWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateQingMingCGYJInfo", self, "UpdateInfo")
end

function LuaQingMingCGYJMainWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateQingMingCGYJInfo", self, "UpdateInfo")
end

function LuaQingMingCGYJMainWnd:CheckBonusAvaliable(data)
	local bonusIndex = data[1]
	local bonusTable = data[2]
	local bonusId = data[3]

	local canGetSign = true
	local newOpenRow = false
	for i,v in pairs(bonusTable) do
		if LuaQingMing2021Mgr.CGYJData[1][tonumber(v)] == 0 then
			canGetSign = false
		end
		if self.saveActiveIndex and self.saveActiveIndex == tonumber(v) then
			newOpenRow = true
		end
	end

	if newOpenRow and canGetSign then
		self:PlayTeamFx(bonusId)
	end

	if self.BonusNodeTable[bonusId] then
		if canGetSign then
			self.BonusNodeTable[bonusId].transform:Find('bg').gameObject:SetActive(true)
		else
			self.BonusNodeTable[bonusId].transform:Find('bg').gameObject:SetActive(false)
		end
	end

	if canGetSign and LuaQingMing2021Mgr.CGYJData[2][bonusId] == 1 then
		if not self.TotalIndexBonusGet[bonusIndex] then
			self.TotalIndexBonusGet[bonusIndex] = 0
		end
		self.TotalIndexBonusGet[bonusIndex] = self.TotalIndexBonusGet[bonusIndex] + 1
	end

	if self.IndexBonusNode[bonusIndex] and not self.AwardGetTable[bonusIndex] then
		local node = self.IndexBonusNode[bonusIndex].transform:Find('node').gameObject
		if canGetSign and LuaQingMing2021Mgr.CGYJData[2][bonusId] == 0 then
			self.AwardGetTable[bonusIndex] = true
			--node.transform.localPosition = Vector3(node.transform.localPosition.x,node.transform.localPosition.y,0)
			self.IndexBonusNode[bonusIndex].transform:Find('ani').gameObject:SetActive(true)
			self.IndexBonusNode[bonusIndex].transform:Find('empty').gameObject:SetActive(false)
			self.IndexBonusNode[bonusIndex].transform:Find('full').gameObject:SetActive(false)
			local onClick = function(go)
				Gac2Gas.RequestGetQingMing2021TuJianAward(bonusIndex)
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		else
			--node.transform.localPosition = Vector3(node.transform.localPosition.x,node.transform.localPosition.y,-1)
			self.IndexBonusNode[bonusIndex].transform:Find('ani').gameObject:SetActive(false)
			self.IndexBonusNode[bonusIndex].transform:Find('empty').gameObject:SetActive(true)
			self.IndexBonusNode[bonusIndex].transform:Find('full').gameObject:SetActive(false)

			local awardData = QingMing2021_TuJianAward.GetData(bonusId)
			local onClick = function(go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(awardData.ItemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		end

	elseif bonusIndex == 4 and not self.AwardGetTable[bonusIndex] then
		local node = self.nodeBig.transform:Find('node').gameObject
		if canGetSign and LuaQingMing2021Mgr.CGYJData[2][bonusId] == 0 then
				--node.transform.localPosition = Vector3(node.transform.localPosition.x,node.transform.localPosition.y,0)
				self.AwardGetTable[bonusIndex] = true
				self.nodeBig.transform:Find('ani').gameObject:SetActive(true)
				self.nodeBig.transform:Find('empty').gameObject:SetActive(false)
				self.nodeBig.transform:Find('full').gameObject:SetActive(false)
				local onClick = function(go)
					Gac2Gas.RequestGetQingMing2021TuJianAward(bonusIndex)
				end
				CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		elseif canGetSign and LuaQingMing2021Mgr.CGYJData[2][bonusId] == 1 then
				self.AwardGetTable[bonusIndex] = true
				self.nodeBig.transform:Find('ani').gameObject:SetActive(false)
				self.nodeBig.transform:Find('empty').gameObject:SetActive(false)
				self.nodeBig.transform:Find('full').gameObject:SetActive(true)
				local awardData = QingMing2021_TuJianAward.GetData(bonusId)
				local onClick = function(go)
					CItemInfoMgr.ShowLinkItemTemplateInfo(awardData.ItemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
				end
				CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		else
				--node.transform.localPosition = Vector3(node.transform.localPosition.x,node.transform.localPosition.y,-1)
				self.nodeBig.transform:Find('ani').gameObject:SetActive(false)
				self.nodeBig.transform:Find('empty').gameObject:SetActive(true)
				self.nodeBig.transform:Find('full').gameObject:SetActive(false)
				local awardData = QingMing2021_TuJianAward.GetData(bonusId)
				local onClick = function(go)
					CItemInfoMgr.ShowLinkItemTemplateInfo(awardData.ItemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
				end
				CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
		end
	end

	if self.TotalIndexBonusGet[bonusIndex] == 4 or (bonusIndex == 3 and self.TotalIndexBonusGet[bonusIndex] == 2) then
		self.IndexBonusNode[bonusIndex].transform:Find('ani').gameObject:SetActive(false)
		self.IndexBonusNode[bonusIndex].transform:Find('empty').gameObject:SetActive(false)
		self.IndexBonusNode[bonusIndex].transform:Find('full').gameObject:SetActive(true)
		local awardData = QingMing2021_TuJianAward.GetData(bonusId)
		local onClick = function(go)
			CItemAccessListMgr.Inst:ShowItemAccessInfo(awardData.ItemId, true, go.transform, CTooltipAlignType.Left)
		end
		local node = self.nodeBig.transform:Find('node').gameObject
		CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
	end
end

function LuaQingMingCGYJMainWnd:UpdateBonus(data)
	self.TotalIndexBonusGet = {}

	for i,v in ipairs(data) do
		local index = i
		local bonusData = self.BonusData[index] --QingMing2021_TuJianAward.GetData(index)
		local checkResult = self:CheckBonusAvaliable(bonusData)
	end
end

function LuaQingMingCGYJMainWnd:InitBonusData()
	self.BonusData = {}
	local count = QingMing2021_TuJianAward.GetDataCount()
	for i=1,count do
		local bonusData = QingMing2021_TuJianAward.GetData(i)
		local conditionTable = g_LuaUtil:StrSplit(bonusData.Condition,",")
		table.insert(self.BonusData,{bonusData.Index,conditionTable,i})
	end
end

function LuaQingMingCGYJMainWnd:UpdateIcon(data)
	for i,v in ipairs(data) do
		local index = i
		local iconData = QingMing2021_TuJian.GetData(index)
		local itemId = iconData.ItemId
		local itemData = Item_Item.GetData(itemId)
		local count = iconData.Count
		local node = self.GridTable[index]
		local iconNode = node.transform:Find('Icon').gameObject
		iconNode:GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
		node.transform:Find('NumLabel'):GetComponent(typeof(UILabel)).text = count
		if v == 1 then --
			iconNode.transform.localPosition = Vector3(iconNode.transform.localPosition.x,iconNode.transform.localPosition.y,0)
			node.transform:Find('Texture').gameObject:SetActive(false)
			node.transform:Find('NumLabel').gameObject:SetActive(false)
			local onClick = function(go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end
			CommonDefs.AddOnClickListener(iconNode,DelegateFactory.Action_GameObject(onClick),false)
			node.transform:Find('highSprite').gameObject:SetActive(false)
			if self.saveActiveIndex and self.saveActiveIndex == index then
				node.transform:Find('UnlockFx'):GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_dianlianggezi.prefab')
			end
		else -- can click
			iconNode.transform.localPosition = Vector3(iconNode.transform.localPosition.x,iconNode.transform.localPosition.y,-1)
			node.transform:Find('Texture').gameObject:SetActive(true)
			node.transform:Find('NumLabel').gameObject:SetActive(true)
			local onClick = function(go)
				if self.totalResNum >= count then
					Gac2Gas.RequestEnableQingMing2021TuJianGrid(index)
					self.saveActiveIndex = index
				else
					g_MessageMgr:ShowMessage("QingMingCGYJNotEnough")
				end
			end

			if self.totalResNum >= count then
				node.transform:Find('highSprite').gameObject:SetActive(true)
			else
				node.transform:Find('highSprite').gameObject:SetActive(false)
			end
			CommonDefs.AddOnClickListener(iconNode,DelegateFactory.Action_GameObject(onClick),false)
		end

	end
end

function LuaQingMingCGYJMainWnd:UpdateInfo()
	self:UpdateItemRes()
	self:UpdateIcon(LuaQingMing2021Mgr.CGYJData[1])
	self.AwardGetTable = {}
	self:UpdateBonus(LuaQingMing2021Mgr.CGYJData[2])
end

function LuaQingMingCGYJMainWnd:PlayTeamFx(teamIndex)
	if self.moveFxTeam[teamIndex] and self.moveFxPositionTeam[teamIndex] then
		local moveNode = self.moveFxTeam[teamIndex]
		local fx = moveNode:GetComponent(typeof(CUIFx))
		fx:DestroyFx()
		LuaTweenUtils.DOKill(fx.transform, false)

		fx.transform.position = self.moveFxPositionTeam[teamIndex][1]
		fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
		CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(fx.transform, self.moveFxPositionTeam[teamIndex][2], 2, false), Ease.InOutCubic), DelegateFactory.TweenCallback(function ()
			fx:DestroyFx()
			LuaTweenUtils.DOKill(fx.transform, false)
		end))
	elseif teamIndex == 11 then
--		for i,v in ipairs(self.moveFxTeam) do
--			local moveNode = self.moveFxTeam[i]
--			local fx = moveNode:GetComponent(typeof(CUIFx))
--			fx:DestroyFx()
--			LuaTweenUtils.DOKill(fx.transform, false)
--
--			fx.transform.position = self.moveFxPositionTeam[i][2]
--			fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
--			CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(fx.transform, self.moveFxTotalPosition, 2, false), Ease.InOutCubic), DelegateFactory.TweenCallback(function ()
--				fx:DestroyFx()
--				LuaTweenUtils.DOKill(fx.transform, false)
--			end))
--		end
	end
end

function LuaQingMingCGYJMainWnd:InitUnlockRowFxTeam()
	--
	local fxTeam = {
		self.transform:Find('Anchor/UnlockRowFx1').gameObject,
		self.transform:Find('Anchor/UnlockRowFx2').gameObject,
		self.transform:Find('Anchor/UnlockRowFx3').gameObject,
		self.transform:Find('Anchor/UnlockRowFx4').gameObject,
		self.transform:Find('Anchor/UnlockRowFx5').gameObject,
		self.transform:Find('Anchor/UnlockRowFx6').gameObject,
		self.transform:Find('Anchor/UnlockRowFx7').gameObject,
		self.transform:Find('Anchor/UnlockRowFx8').gameObject,
	}
	local fxPositon = {
		{self.row1.transform:Find('ItemCell4').transform.position,self.textVer.transform:Find('label1').transform.position},
		{self.row2.transform:Find('ItemCell4').transform.position,self.textVer.transform:Find('label2').transform.position},
		{self.row3.transform:Find('ItemCell4').transform.position,self.textVer.transform:Find('label3').transform.position},
		{self.row4.transform:Find('ItemCell4').transform.position,self.textVer.transform:Find('label4').transform.position},
		{self.row4.transform:Find('ItemCell1').transform.position,self.textRow.transform:Find('label1').transform.position},
		{self.row4.transform:Find('ItemCell2').transform.position,self.textRow.transform:Find('label2').transform.position},
		{self.row4.transform:Find('ItemCell3').transform.position,self.textRow.transform:Find('label3').transform.position},
		{self.row4.transform:Find('ItemCell4').transform.position,self.textRow.transform:Find('label4').transform.position},
	}
	local totalFxPosition = self.nodeBig.transform:Find('node').transform.position

	self.moveFxTeam = fxTeam
	self.moveFxPositionTeam = fxPositon
	self.moveFxTotalPosition = totalFxPosition
end

function LuaQingMingCGYJMainWnd:UpdateItemRes()
	local resId = QingMing2021_Setting.GetData().FeiXuItemId
	local bindCount, notbindCount
	bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(resId)
	local total = bindCount + notbindCount
	self.ResNode.transform:Find('NeedExpLb'):GetComponent(typeof(UILabel)).text = total
	self.totalResNum = total
end

function LuaQingMingCGYJMainWnd:Init()
	local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.QingMingCGYJMainWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

	self:InitUnlockRowFxTeam()
	self.GridTable = {}
	local t = {self.row1,self.row2,self.row3,self.row4}
	for i,v in ipairs(t) do
		for j=1,4 do
			local node = v.transform:Find('ItemCell'..j).gameObject
			table.insert(self.GridTable,node)
		end
	end

	self.BonusNodeTable = {
		self.textVer.transform:Find('label1').gameObject,
		self.textVer.transform:Find('label2').gameObject,
		self.textVer.transform:Find('label3').gameObject,
		self.textVer.transform:Find('label4').gameObject,
		self.textRow.transform:Find('label1').gameObject,
		self.textRow.transform:Find('label2').gameObject,
		self.textRow.transform:Find('label3').gameObject,
		self.textRow.transform:Find('label4').gameObject,
	}
	self.IndexBonusNode = {
		self.node1,
		self.node2,
		self.node3,
	}
	self:InitBonusData()

	local resId = QingMing2021_Setting.GetData().FeiXuItemId
	local addNode = self.ResNode.transform:Find('GetMoreBtn').gameObject
	local onAddClick = function(go)
		CItemAccessListMgr.Inst:ShowItemAccessInfo(resId, true, go.transform, CTooltipAlignType.Left)
	end
	CommonDefs.AddOnClickListener(addNode,DelegateFactory.Action_GameObject(onAddClick),false)
	local onTipClick = function(go)
		g_MessageMgr:ShowMessage('QingMingCGYJMainWndTip')
	end
	CommonDefs.AddOnClickListener(self.TipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
	self:UpdateInfo()

end

--@region UIEvent

--@endregion
