local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local UIPanel = import "UIPanel"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local PlayerSettings = import "L10.Game.PlayerSettings"
local ShareMgr = import "ShareMgr"

LuaHaoYiXingCardPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHaoYiXingCardPreviewWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardPreviewWnd, "PlayButton", "PlayButton", UISprite)
RegistChildComponent(LuaHaoYiXingCardPreviewWnd, "Texture", "Texture", UITexture)
RegistChildComponent(LuaHaoYiXingCardPreviewWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHaoYiXingCardPreviewWnd, "m_Data")
RegistClassMember(LuaHaoYiXingCardPreviewWnd, "m_Sound")

function LuaHaoYiXingCardPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

	UIEventListener.Get(self.PlayButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayButtonClick()
	end)

    --@endregion EventBind end
end

function LuaHaoYiXingCardPreviewWnd:Init()
	self.m_Data = SpokesmanTCG_ErHaTCG.GetData(LuaHaoYiXingMgr.m_PreviewWndPreviewCardID)
    local url = ShareMgr.GetWebImagePath(self.m_Data.BigIcon)
	CPersonalSpaceMgr.DownLoadPic(url, self.Texture, 1080, 1920, DelegateFactory.Action(function ()

	end), false, true)
	self.PlayButton.gameObject:SetActive(false)
    -- self.PlayButton.transform:Find("Sprite").gameObject:SetActive(self.m_Data.Type ~= 2)
	-- self.PlayButton.spriteName = self.m_Data.Type == 2 and "common_button_broadcast" or "common_btn_05"
	self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaHaoYiXingCardPreviewWnd:OnDisable()
    if self.m_Sound then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
end
--@region UIEvent

function LuaHaoYiXingCardPreviewWnd:OnShareButtonClick()
	if not CClientMainPlayer.Inst then return end
    self.ShareButton:SetActive(false)
    self.PlayButton.gameObject:SetActive(false)
    self.CloseButton:SetActive(false)
    CUICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            self.m_ShareButton,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        self.ShareButton:SetActive(true)
                        self.PlayButton.gameObject:SetActive(false)
                        self.CloseButton:SetActive(true)
                        if CLuaShareMgr.IsOpenShare() then
                            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        else
                            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
                        end
                    end
            ),
            false
    )
end

function LuaHaoYiXingCardPreviewWnd:OnPlayButtonClick()
	if self.m_Data.Type == 2 then
        if PlayerSettings.VolumeSetting == 0 then
            PlayerSettings.VolumeSetting = 1
        end
        PlayerSettings.SoundEnabled = true
        if self.m_Sound~=nil then
            SoundManager.Inst:StopSound(self.m_Sound)
            self.m_Sound = nil
        end
        self.m_Sound = SoundManager.Inst:PlayOneShot(self.m_Data.Audio, Vector3.zero, nil, 0, -1)
    elseif self.m_Data.Type == 3 then
        AMPlayer.Inst:PlayCG(self.m_Data.Url, DelegateFactory.Action(function () end), 1)
    end
end

--@endregion UIEvent

