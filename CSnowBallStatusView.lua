-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CScene = import "L10.Game.CScene"
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local CSnowBallStatusView = import "L10.UI.CSnowBallStatusView"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CSnowBallStatusView.m_Init_CS2LuaHook = function (this)
    this.remainTimeLabel.enabled = false
    if CClientMainPlayer.Inst ~= nil and CScene.MainScene ~= nil then
        this.remainTimeLabel.enabled = CScene.MainScene.ShowTimeCountDown
        this:SetRemainTimeVal()
    end
    this.titleLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("雪球大作战"), {})
end
CSnowBallStatusView.m_Start_CS2LuaHook = function (this)
    UIEventListener.Get(this.leaveBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnLeaveButtonClick, VoidDelegate, this)

    UIEventListener.Get(this.checkBtn).onClick = DelegateFactory.VoidDelegate(function (p)
      local gameState = CSnowBallMgr.Inst:GetSnowBallState()
      if gameState == 1 then
        Gac2Gas.QueryXueQiuPlayInfo()
      elseif gameState == 2 then
        Gac2Gas.QueryXueQiuDoublePlayInfo()
      end
    end)
end
CSnowBallStatusView.m_OnEnable_CS2LuaHook = function (this)
    this:Init()
    EventManager.AddListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayCreated, Action0, this))
    EventManager.AddListener(EnumEventType.ShowTimeCountDown, MakeDelegateFromCSFunction(this.OnShowTimeCountDown, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
end
CSnowBallStatusView.m_OnGamePlayRemainMonsterNumUpdate_CS2LuaHook = function (this)
    if this ~= nil and CScene.MainScene.ShowMonsterCountDown then
        this:SetRemainTimeVal()
    end
end
CSnowBallStatusView.m_SetRemainTimeVal_CS2LuaHook = function (this)
    if CScene.MainScene ~= nil then
        if CScene.MainScene.ShowTimeCountDown then
            this.remainTimeLabel.text = this:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            this.remainTimeLabel.text = nil
        end
    else
        this.remainTimeLabel.text = nil
    end
end
CSnowBallStatusView.m_GetRemainTimeText_CS2LuaHook = function (this, totalSeconds)
    --damageLabel.text = string.Format(LocalString.GetString("伤害 {0}"), CWeekendGameplayMgr.Inst.totalDamage);
    this.scoreLabel.text = tostring(CSnowBallMgr.Inst.score)
    this.escoreLabel.text = tostring(CSnowBallMgr.Inst.executeScore)
    if CSnowBallMgr.Inst.score >= CSnowBallMgr.Inst.executeScore then
        this.scoreLabel.color = Color.green
    else
        this.scoreLabel.color = Color.red
    end

    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return System.String.Format("[ACF9FF]{0:00}:{1:00}:{2:00}[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return System.String.Format("[ACF9FF]{0:00}:{1:00}[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
