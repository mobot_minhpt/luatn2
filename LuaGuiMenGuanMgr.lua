local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local Utility = import "L10.Engine.Utility"

--require "design/GamePlay/ZhongYuanJie"

CLuaGuiMenGuanMgr = {
    s_FightDuration = 1800,
    s_SceneBossInfoTbl = {},
    s_RemainTime = 0,
    s_ShowStatusView = false,
    s_LiveBossCount = 0,
    s_TotalBossCount = 0,
    p_CountDownTick = nil
}

function Gas2Gac.SyncGuiMenGuanInfo(bShow, remainTime, totalMonsterCount, playInfoSUD)
    CLuaGuiMenGuanMgr.s_ShowStatusView = bShow
    CLuaGuiMenGuanMgr.s_RemainTime = remainTime
    CLuaGuiMenGuanMgr.s_TotalBossCount = totalMonsterCount
    CLuaGuiMenGuanMgr.s_LiveBossCount = 0

    CLuaGuiMenGuanMgr.s_SceneBossInfoTbl = {}
    local list = MsgPackImpl.unpack(playInfoSUD)
    if not list then return end
    for i = 0, list.Count - 1, 3 do
        local sceneId, sceneName, count = list[i], list[i + 1], list[i + 2]
        count = tonumber(count)
        table.insert(CLuaGuiMenGuanMgr.s_SceneBossInfoTbl, {sceneId, sceneName, count})
        CLuaGuiMenGuanMgr.s_LiveBossCount = CLuaGuiMenGuanMgr.s_LiveBossCount + count
    end

    if CLuaGuiMenGuanMgr.p_CountDownTick then
        UnRegisterTick(CLuaGuiMenGuanMgr.p_CountDownTick)
    end
    g_ScriptEvent:BroadcastInLua("RefreshGuiMenGuanVisibleStatus")

    if bShow then
        g_ScriptEvent:BroadcastInLua("SyncGuiMenGuanInfo")

        CLuaGuiMenGuanMgr.p_CountDownTick = RegisterTickWithDuration(function()
            CLuaGuiMenGuanMgr.s_RemainTime = math.max(0, CLuaGuiMenGuanMgr.s_RemainTime - 1)
            g_ScriptEvent:BroadcastInLua("SyncGuiMenGuanInfo")
        end, 1000, CLuaGuiMenGuanMgr.s_FightDuration * 1000)
    end
end

function Gas2Gac.GuiMenGuanTrackToBoss(sceneId, templateId, x, y)
    local pixelPos = Utility.GridPos2PixelPos(CPos(x, y))
    local dist = Utility.Grid2Pixel(2.0);
    CTrackMgr.Inst:Track(sceneId, templateId, pixelPos, dist, nil, nil, nil, nil,  true);
    CUIManager.CloseUI(CUIResources.GuiMenGuanPlayInfoWnd)
end
