local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
LuaDaFuWongZhanBaoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongZhanBaoWnd, "PlayerInfoNode", "PlayerInfoNode", GameObject)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "Pic1", "Pic1", CUITexture)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "Pic2", "Pic2", CUITexture)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "Pic3", "Pic3", CUITexture)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "Pic4", "Pic4", CUITexture)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "FakeModel", "FakeModel", GameObject)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "Shengli", "Shengli", GameObject)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "HideWhenShare", "HideWhenShare", GameObject)
RegistChildComponent(LuaDaFuWongZhanBaoWnd, "ShareBtn", "ShareBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongZhanBaoWnd, "m_ResultInfo")
RegistClassMember(LuaDaFuWongZhanBaoWnd, "m_ModelTexture")
RegistClassMember(LuaDaFuWongZhanBaoWnd, "m_ModelTextureLoader")
RegistClassMember(LuaDaFuWongZhanBaoWnd, "m_RO")
function LuaDaFuWongZhanBaoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end
	self.m_ModelTexture = self.FakeModel.transform:Find("Texture"):GetComponent(typeof(UITexture))
end

function LuaDaFuWongZhanBaoWnd:Init()
	self.m_ResultInfo = LuaDaFuWongMgr.PlayResult
	if not self.m_ResultInfo then return end
	--self:InitPlayerModel()
	self:InitPlayerNode()
	self:InitPlayPerform()
end

function LuaDaFuWongZhanBaoWnd:InitPlayerNode()
	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end
	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.RealName
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	end
end

function LuaDaFuWongZhanBaoWnd:InitPlayerModel()
	self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_RO = ro
		local playerAppearanceProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp
		CClientMainPlayer.LoadResource(ro, playerAppearanceProp, true, 1, 0, false, 0, false, 0, false, nil)
    end)
	self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture("__MainPlayerModelCamera__",self.m_ModelTextureLoader,-160,-0.1,-1.2,2.9,false,true,1,false,false)
	local AniInfo = DaFuWeng_Setting.GetData().ZhanBaoAni
	if self.m_ResultInfo.result then
		self.m_RO:Preview(AniInfo[0][0],tonumber(AniInfo[0][1]))
	else
		self.m_RO:Preview(AniInfo[1][0],tonumber(AniInfo[1][1]))
	end
end

function LuaDaFuWongZhanBaoWnd:InitPlayPerform()
	local picList = {self.Pic1,self.Pic2,self.Pic3,self.Pic4}
	local count = 1
	for m,n in pairs(self.m_ResultInfo.report) do
		local k,v = n[1],n[2]
		local data = DaFuWeng_ZhanBao.GetData(k)
		local pic = picList[count]
		if data then
			pic.transform:Find("Title"):GetComponent(typeof(UILabel)).text = LocalString.CnStrH2V(data.Title,false)
			pic.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.CnStrH2V(SafeStringFormat3(data.Describe,self:GetValueName(k,v)),true)
			pic:LoadMaterial(SafeStringFormat3(data.PicPath,v))
			count = count + 1
		end
	end
	self.Shengli.gameObject:SetActive(self.m_ResultInfo.result)
	local totalMoney = 0
	for k,v in pairs(self.m_ResultInfo.playData) do
		if v[1] == "TotalMoneyMax" then totalMoney = v[2] end
	end
	if self.m_ResultInfo.result then
		self.Desc.text =  SafeStringFormat3(LocalString.GetString("总资产:%d"),totalMoney)
	else
		self.Desc.text =  DaFuWeng_Setting.GetData().ZhanBaoDesc
	end
end

function LuaDaFuWongZhanBaoWnd:GetValueName(key,value)
	local randomEventList = DaFuWeng_Setting.GetData().RandomEventName
	if key == "BuyLandFirst" then
		return DaFuWeng_Land.GetData(value).Name
	elseif key == "Toll" then
		return tostring(value)
	elseif key == "UseCard" then
		return DaFuWeng_Card.GetData(value).Name
	elseif key == "ToHospital" or key == "ToPrison" then
		return ""
	elseif key == "RandomEvent" then
		return randomEventList[value - 1]
	elseif key == "YaoQian" then
		return DaFuWeng_YaoQian.GetData(value).Name
	end
end

--@region UIEvent

function LuaDaFuWongZhanBaoWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.DaFuWongZhanBaoWnd,self.HideWhenShare)   
end

--@endregion UIEvent

function LuaDaFuWongZhanBaoWnd:OnDisable()
	CUIManager.DestroyModelTexture("__MainPlayerModelCamera__")
end