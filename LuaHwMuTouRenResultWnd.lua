local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animator = import "UnityEngine.Animator"
LuaHwMuTouRenResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHwMuTouRenResultWnd, "WinStyle", "WinStyle", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "LossStyle", "LossStyle", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "RankLabel", "RankLabel", UILabel)
--RegistChildComponent(LuaHwMuTouRenResultWnd, "m_PlayerHeadInfoItem", "m_PlayerHeadInfoItem", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "FightBtn", "FightBtn", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "Reward", "Reward", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaHwMuTouRenResultWnd, "AwardGrid", "AwardGrid", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHwMuTouRenResultWnd, "m_ShareTick")
RegistClassMember(LuaHwMuTouRenResultWnd, "m_PlayerHeadInfoItem")
function LuaHwMuTouRenResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	local onShareClick = function(go)
		self:SetOpBtnVisible(false)
		if self.m_ShareTick then
		  UnRegisterTick(self.m_ShareTick)
		  self.m_ShareTick = nil
		end
		self.m_ShareTick = RegisterTickWithDuration(function ()
		  self:SetOpBtnVisible(true)
		end, 1000, 1000)
		CUICommonDef.CaptureScreenAndShare()
	  end
	self.ShareButton:SetActive(false)
	self.FightBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("分享")
	CommonDefs.AddOnClickListener(self.FightBtn.gameObject,DelegateFactory.Action_GameObject(onShareClick),false)
	if CommonDefs.IS_VN_CLIENT then
		self.FightBtn.gameObject:SetActive(false)
	end

	-- UIEventListener.Get(self.FightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	--     self:OnFightBtnClick()
	-- end)
	self.m_PlayerHeadInfoItem = self.transform:Find("PlayerInfoNode").gameObject
	self.m_PlayerHeadInfoItem:SetActive(false)
end

function LuaHwMuTouRenResultWnd:Init()
	self:InitPlayerInfo()
	self:InitResultWnd()
end

function LuaHwMuTouRenResultWnd:InitResultWnd()
	local anim = self.gameObject:GetComponent(typeof(Animator))
	local isWin = LuaHalloween2022Mgr.TrafficLightPlayResult.isWin
	local awardTable = LuaHalloween2022Mgr.TrafficLightPlayResult.GetAwardTbl
	self.RankLabel.text = LuaHalloween2022Mgr.TrafficLightPlayResult.rank
	if isWin == 1 then
		anim:Play("hwmutourenresultwnd_shown")
	else
		anim:Play("hwmutourenresultwnd_shown_1")
	end
	self.WinStyle:SetActive(isWin == 1)
	self.LossStyle:SetActive(isWin == 2 or isWin == 3)
	self.ItemCell:SetActive(false)
	self.Reward:SetActive(false)
	if #awardTable > 0 then
		self.Reward:SetActive(true)
		for i = 1, #awardTable do
			local go = CUICommonDef.AddChild(self.AwardGrid.gameObject, self.ItemCell)
			self:InitOneItem(go,awardTable[i])
		end
	end
end

function LuaHwMuTouRenResultWnd:InitPlayerInfo()
	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end
	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.RealName
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	elseif LuaHalloween2022Mgr.m_Name then
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Name"), typeof(UILabel)).text = LuaHalloween2022Mgr.m_Name
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(LuaHalloween2022Mgr.m_Level)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaHalloween2022Mgr.m_Class, LuaHalloween2022Mgr.m_Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("ID"), typeof(UILabel)).text = tostring(LuaHalloween2022Mgr.m_Id)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerHeadInfoItem.transform:Find("Server"), typeof(UILabel)).text = LuaHalloween2022Mgr.m_ServerName
	end
end

function LuaHwMuTouRenResultWnd:SetOpBtnVisible(sign)
	self.CloseButton:SetActive(sign)
	--self.ShareButton:SetActive(sign)
	self.m_PlayerHeadInfoItem:SetActive(not sign)
	self.FightBtn.gameObject:SetActive(sign)
	if not sign then CUIManager.CloseUI(CLuaUIResources.BatDialogWnd) end
end

function LuaHwMuTouRenResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
	curItem:SetActive(true)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end
--@region UIEvent


function LuaHwMuTouRenResultWnd:OnFightBtnClick()
	CUIManager.CloseUI(CLuaUIResources.HwMuTouRenResultWnd)
	CUIManager.ShowUI(CLuaUIResources.HwMuTouRenEnterWnd)
end

--@endregion UIEvent

function LuaHwMuTouRenResultWnd:OnDisable()
	if self.m_ShareTick~=nil then UnRegisterTick(self.m_ShareTick) end
end
