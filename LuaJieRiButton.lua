local CUIFxPaths = import "L10.UI.CUIFxPaths"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"

LuaJieRiButton = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJieRiButton, "JieRiButtonFx", CUIFx)
RegistChildComponent(LuaJieRiButton, "JieRiButtonRoot", GameObject)
RegistChildComponent(LuaJieRiButton, "Alert", GameObject)
--@endregion RegistChildComponent end

function LuaJieRiButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaJieRiButton:OnDisable()
    self.JieRiButtonRoot.gameObject:SetActive(false)
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot",self,"InitFx")
end

function LuaJieRiButton:OnEnable()
    self:InitIcon()
    self.Alert:SetActive(false)

    self.JieRiButtonRoot.gameObject:SetActive(true)
    self.JieRiButtonRoot.transform.position = self.transform.position
    self.JieRiButtonFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
    self:InitFx(true)
    g_ScriptEvent:AddListener("UpdateActivityRedDot",self,"InitFx")
end

function LuaJieRiButton:InitFx(hasChanged)
    if not hasChanged then return end
    local items = CLuaScheduleMgr:GetTodayFestivals()
    if not items or #items == 0 then 
        self.JieRiButtonFx:DestroyFx()
        LuaTweenUtils.DOKill(self.JieRiButtonFx.transform, true)
        CLuaScheduleMgr.m_IsShowJieRiButtonFx = false
        return
    end
    local showFx = false
    local showReddot = false
    for i = 1,#items do
        local info = items[i]
        showFx = showFx or CLuaScheduleMgr:IsNeedShowJieRiButtonFx(info)
        showReddot = showReddot or LuaActivityRedDotMgr:IsRedDot(info.ID, LuaEnumRedDotType.JieRi)
    end
    self.Alert:SetActive(showReddot)
    if showFx and not CLuaScheduleMgr.m_IsShowJieRiButtonFx then
        CLuaScheduleMgr.m_IsShowJieRiButtonFx = true
        local b = NGUIMath.CalculateRelativeWidgetBounds(self.transform:Find("Icon"))
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
        self.JieRiButtonFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        self.JieRiButtonFx.transform.localPosition = waypoints[0]
        LuaTweenUtils.DOLuaLocalPath(self.JieRiButtonFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    elseif not showFx then
        self.JieRiButtonFx:DestroyFx()
        LuaTweenUtils.DOKill(self.JieRiButtonFx.transform, true)
        CLuaScheduleMgr.m_IsShowJieRiButtonFx = false
    end
end

function LuaJieRiButton:InitIcon()
    local items = CLuaScheduleMgr:GetTodayFestivals()
    if #items == 0 then return end

    local sprite = self.transform:Find("Icon/Sprite")
    local texture = self.transform:Find("Icon/Texture")
    local jieriSprite = self.transform:Find("Icon/JieriSprite")

    if #items == 1 then
        local buttonIcon = items[1].JieRiButtonIcon
        local spriteIcon = items[1].JieRiButtonIconSprite
        if not System.String.IsNullOrEmpty(spriteIcon) then
            jieriSprite.gameObject:SetActive(true)
            sprite.gameObject:SetActive(false)
            texture.gameObject:SetActive(false)
            jieriSprite:GetComponent(typeof(UISprite)).spriteName = spriteIcon
            return
        end
        if not System.String.IsNullOrEmpty(buttonIcon) then
            sprite.gameObject:SetActive(false)
            texture.gameObject:SetActive(true)
            jieriSprite.gameObject:SetActive(false)
            texture:GetComponent(typeof(CUITexture)):LoadMaterial(buttonIcon)
            return
        end
    end

    sprite.gameObject:SetActive(true)
    texture.gameObject:SetActive(false)
    jieriSprite.gameObject:SetActive(false)
end

--@region UIEvent

--@endregion UIEvent

