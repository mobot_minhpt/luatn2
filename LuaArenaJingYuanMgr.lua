local CScene = import "L10.Game.CScene"
local CForcesMgr = import "L10.Game.CForcesMgr"

LuaArenaJingYuanMgr = {}

function LuaArenaJingYuanMgr:OnMainPlayerCreated()
    CUIManager.ShowUI(CLuaUIResources.ArenaJingYuanTopRightWnd)
    self:OnUpdatePlayerForceInfo()
end
function LuaArenaJingYuanMgr:OnMainPlayerDestroyed()
    LuaArenaJingYuanMgr.m_TeamJingYuanInfo = nil
end

-- 队伍信息
function LuaArenaJingYuanMgr:OnUpdatePlayerForceInfo()
    local iconPath = {
        'UI/Texture/Transparent/Material/guanningcommandwnd_icon_zhihui.mat',
        'UI/Texture/Transparent/Material/guanningcommandwnd_icon_zhihui_blue.mat',
        'UI/Texture/Transparent/Material/guanningcommandwnd_icon_zhihui_red.mat',
        'UI/Texture/Transparent/Material/cangbaoge_icon.mat',
    }
    local dict = {}
    if CForcesMgr.Inst.m_PlayForceInfo then
        CommonDefs.DictIterate(CForcesMgr.Inst.m_PlayForceInfo, DelegateFactory.Action_object_object(function (___key, ___value)
            dict[___key] = {texturePath = iconPath[___value]}
        end)) 
    end
    LuaGamePlayMgr:SetPlayerHeadSpeIconInfos(dict, false, true)
end

-- 个人晶元数量
LuaArenaJingYuanMgr.m_PlayerJingYuanInfo = {}
function LuaArenaJingYuanMgr:SyncArenaPlayerJingYuanInfo(playerInfoUd)
    print(tostring(CScene.MainScene.GamePlayDesignId))
    self.m_PlayerJingYuanInfo = playerInfoUd and g_MessagePack.unpack(playerInfoUd) or {}
    for playerId, data in pairs(self.m_PlayerJingYuanInfo) do
        self:OnJingYuanHeadInfoUpdate(data.engineId, data.jingyuanCount, data.isGain)
    end
end

function LuaArenaJingYuanMgr:OnJingYuanHeadInfoUpdate(engineId, jingyuanCount, isGain)
    local args = {}
    args[0] = engineId

    local textureForm = {}
    textureForm.texturePath = 'UI/Texture/Transparent/Material/cangbaoge_icon.mat'
    textureForm.textureSize = {120, 120}
    textureForm.FxPath = isGain and "Fx/UI/Prefab/UI_huanranyixin_cizhui.prefab" or nil
    textureForm.FxTime = 2000
    args[1] = textureForm

    local LabelForm = {}
    LabelForm.content = tostring(jingyuanCount)
    args[2] = LabelForm

    args[3] = {}
    args[4] = jingyuanCount > 0
    args[5] = "ArenaJingYuan"

    g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
end

-- 团队晶元进度
LuaArenaJingYuanMgr.m_TeamJingYuanInfo = nil
function LuaArenaJingYuanMgr:SyncArenaTeamJingYuanInfo(teamInfoUd)
    self.m_TeamJingYuanInfo = teamInfoUd and g_MessagePack.unpack(teamInfoUd) or {}
    g_ScriptEvent:BroadcastInLua("SyncArenaTeamJingYuanInfo")
end
