-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuShiJianWnd = import "L10.UI.CQingQiuShiJianWnd"
local CTickMgr = import "L10.Engine.CTickMgr"
local DelegateFactory = import "DelegateFactory"
local Divination_ShiJian = import "L10.Game.Divination_ShiJian"
local Divination_ShiJianCondition = import "L10.Game.Divination_ShiJianCondition"
local ETickType = import "L10.Engine.ETickType"
local Extensions = import "Extensions"
local NGUIText = import "NGUIText"
local String = import "System.String"
local TweenColor = import "TweenColor"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
CQingQiuShiJianWnd.m_Awake_CS2LuaHook = function (this) 
    this.m_ContentObj:SetActive(false)
    this.m_CloseBtn:SetActive(false)
    this.m_ShijianCount = this.m_ShiJianLabel.Length
    do
        local i = 0
        while i < this.m_ShijianCount do
            Extensions.SetLocalPositionZ(this.m_ShiJianSprite[i].transform, - 1)
            this.m_ShiJianLabel[i].color = Color.gray
            this.m_ShiJianLabel[i].text = Divination_ShiJian.GetData(i + 1).Name
            i = i + 1
        end
    end
    do
        local i = 0 local cnt = this.m_AchievementLabel.Length
        while i < cnt do
            Extensions.SetLocalPositionZ(this.m_AchievementLabel[i].transform, - 1)
            this.m_AchievementLabel[i].color = Color.gray
            this.m_AchievementSprite[i].gameObject:SetActive(false)
            this.m_AchievementLabel[i].text = this:ClearDesignColor(Divination_ShiJianCondition.GetData(i + 1).Description)
            i = i + 1
        end
    end
end
CQingQiuShiJianWnd.m_Init_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil or not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eShiJian) then
        return
    end
    local data = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eShiJian).StringData
    if System.String.IsNullOrEmpty(data) then
        return
    end
    local splitData = CommonDefs.StringSplit_ArrayChar(data, ",")
    local len = splitData.Length
    if len <= 0 then
        return
    end
    do
        local i = 0
        while i < len do
            local continue
            repeat
                local val = math.floor(tonumber(splitData[i] or 0))
                if val > this.m_ShijianCount then
                    continue = true
                    break
                end
                this.m_ShiJianLabel[val - 1].color = NGUIText.ParseColor24(Divination_ShiJian.GetData(val).Color, 0)
                this.m_ShiJianSprite[val - 1].color = this.m_ShiJianLabel[val - 1].color
                Extensions.SetLocalPositionZ(this.m_ShiJianSprite[val - 1].transform, 0)
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    do
        local i = 0 local cnt = this.m_AchievementLabel.Length
        while i < cnt do
            if len >= Divination_ShiJianCondition.GetData(i + 1).Condition then
                --m_AchievementLabel[i].color = Color.green;
                Extensions.SetLocalPositionZ(this.m_AchievementLabel[i].transform, 0)
                this.m_AchievementLabel[i].text = String.Format("[c][621722]{0}[-]", CChatLinkMgr.TranslateToNGUIText(Divination_ShiJianCondition.GetData(i + 1).Description, false))
                this.m_AchievementSprite[i].gameObject:SetActive(true)
            else
                break
            end
            i = i + 1
        end
    end
    this.m_OpenFx:LoadFx(CQingQiuShiJianWnd.s_OpenFxPath)
end
CQingQiuShiJianWnd.m_ShowContent_CS2LuaHook = function (this) 
    this.m_ContentObj:SetActive(true)
    this.m_CloseBtn:SetActive(true)
    if CQingQiuMgr.Inst.m_ShiJianId > 0 and CQingQiuMgr.Inst.m_ShiJianId <= this.m_ShijianCount then
        local tcSprite = CommonDefs.AddComponent_GameObject_Type(this.m_ShiJianSprite[CQingQiuMgr.Inst.m_ShiJianId - 1].gameObject, typeof(TweenColor))
        local tcLabel = CommonDefs.AddComponent_GameObject_Type(this.m_ShiJianLabel[CQingQiuMgr.Inst.m_ShiJianId - 1].gameObject, typeof(TweenColor))
        tcLabel.from = Color.gray tcSprite.from = tcLabel.from
        tcLabel.to = NGUIText.ParseColor24(Divination_ShiJian.GetData(CQingQiuMgr.Inst.m_ShiJianId).Color, 0) tcSprite.to = tcLabel.to
        tcLabel.duration = this.m_AlertDuration tcSprite.duration = tcLabel.duration
        tcSprite:PlayForward()
        tcLabel:PlayForward()
        this.m_AlertFx:DestroyFx()
        this.m_AlertFx.transform.parent = this.m_ShiJianLabel[CQingQiuMgr.Inst.m_ShiJianId - 1].transform
        this.m_AlertFx.transform.localPosition = Vector3.zero
        this.m_AlertFx:LoadFx(CQingQiuShiJianWnd.s_AlertFxPath)
        this.m_AlertTick = CTickMgr.Register(DelegateFactory.Action(function () 
            this.m_AlertFx:DestroyFx()
            this.m_AlertTick = nil
        end), math.floor(this.m_AlertDuration) * 1000, ETickType.Once)
    end
end
CQingQiuShiJianWnd.m_OnDestroy_CS2LuaHook = function (this) 
    if this.m_AlertTick ~= nil then
        invoke(this.m_AlertTick)
        this.m_AlertTick = nil
    end
end
--CQingQiuShiJianWnd.m_ClearDesignColor_CS2LuaHook = function (this, str) 
--    if System.String.IsNullOrEmpty(str) then
--        return str
--    end
--    local sb = NewStringBuilderWraper(StringBuilder)
--    do
--        local i = 0 local len = CommonDefs.StringLength(str)
--        while i < len do
--            if StringAt(str, i + 1) ~= "#" then
--                CommonDefs.StringBuilder_Append_StringBuilder_Char(sb, StringAt(str, i + 1))
--            else
--                while i + 1 < len and ((StringAt(str, i + 1 + 1) >= "a" and StringAt(str, i + 1 + 1) <= "z") or (StringAt(str, i + 1 + 1) >= "A" and StringAt(str, i + 1 + 1) <= "Z") or (StringAt(str, i + 1 + 1) >= "0" and StringAt(str, i + 1 + 1) <= "9")) do
--                    i = i + 1
--                end
--            end
--            i = i + 1
--        end
--    end
--    return ToStringWrap(sb)
--end
