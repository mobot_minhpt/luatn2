local UISlider = import "UISlider"
local UILongPressButton = import "L10.UI.UILongPressButton"
local Object = import "System.Object"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local UIWidget = import "UIWidget"
local CClientNpc = import "L10.Game.CClientNpc"
local NPC_NPC = import "L10.Game.NPC_NPC"

LuaMakeBeautificationWnd = class()

RegistChildComponent(LuaMakeBeautificationWnd , "m_ModelTexture","ModelTexture",UITexture)
RegistChildComponent(LuaMakeBeautificationWnd , "m_TopLabel","TopLabel",UILabel)
RegistChildComponent(LuaMakeBeautificationWnd , "m_Slider","Slider",UISlider)
RegistChildComponent(LuaMakeBeautificationWnd , "m_FaceFx","FaceFx",CUIFx)
RegistChildComponent(LuaMakeBeautificationWnd , "m_Face","Face",UILongPressButton)

RegistClassMember(LuaMakeBeautificationWnd,"m_ModelTextureLoader")
RegistClassMember(LuaMakeBeautificationWnd,"m_ModelName")
RegistClassMember(LuaMakeBeautificationWnd,"m_GrowthProgressTween")
RegistClassMember(LuaMakeBeautificationWnd,"m_RO")
RegistClassMember(LuaMakeBeautificationWnd,"m_IsTaskFinished")

function LuaMakeBeautificationWnd:Init()
    self.m_Slider.value = 0
    self.m_TopLabel.text = LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_TitleText
    self.m_ModelName = "_MakeBeautificationWndModel_"
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro, LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_NPCID)
        self.m_RO = ro
    end)
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,180, 0,-0.71,4.66,false,true,1,true)
    CUIManager.SetModelPosition(self.m_ModelName, LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_ModelPos)
    local longpressArea = self.m_Face
    local tex = longpressArea.gameObject:GetComponent(typeof(UIWidget))
    tex.width = LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_BoundArea[3]
    tex.height = LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_BoundArea[4]
    tex.transform.localPosition = Vector3(LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_BoundArea[1],LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_BoundArea[2],0)
    longpressArea.OnLongPressDelegate = DelegateFactory.Action(function ()
        self:OnPressStart()
    end)
    longpressArea.OnLongPressEndDelegate = DelegateFactory.Action(function ()
        self:OnPressEnd()
    end)
    self.m_FaceFx:LoadFx("Fx/UI/Prefab/UI_heshengzijin_hudiefeiwu.prefab")
end

function LuaMakeBeautificationWnd:OnDisable()
    self:CancelTween()
end

function LuaMakeBeautificationWnd:CancelTween()
    if self.m_GrowthProgressTween then
        LuaTweenUtils.Kill(self.m_GrowthProgressTween, false)
        self.m_GrowthProgressTween = nil
    end
end

function LuaMakeBeautificationWnd:OnPressStart()
    if self.m_IsTaskFinished then return end
    self:CancelTween()
    self.m_GrowthProgressTween = LuaTweenUtils.TweenFloat(0, 1, LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_Delay, function (v)
        self.m_Slider.value = v
        self:OnTaskFinished()
    end)
end

function LuaMakeBeautificationWnd:OnPressEnd()
    if self.m_IsTaskFinished then return end
    if self.m_Slider.value < 1 then
        self:CancelTween()
        self.m_Slider.value = 0
        return
    end
    self:OnTaskFinished()
end

function LuaMakeBeautificationWnd:OnTaskFinished()
    if self.m_Slider.value == 1 then
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        self.m_FaceFx:LoadFx(LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_Fx)
        self.m_TopLabel.gameObject:SetActive(false)
        self:LoadModel(self.m_RO, LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_NewNPCID)
        Gac2Gas.FinishClientTaskEvent(LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_TaskId,LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_EventName,MsgPackImpl.pack(empty))
        RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.MakeBeautificationWnd)
        end,3000)
        self:CancelTween()
        self.m_IsTaskFinished = true
    end
end

function LuaMakeBeautificationWnd:LoadModel(ro, npcID)
    local data = NPC_NPC.GetData(npcID)
    if data then
        ro:LoadMain(CClientNpc.GetNPCPrefabPath(data), nil, false, false, false)
    end
end

LuaMakeBeautificationWndMgr = {}
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_TaskId = 0
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_EventName = "LongPressChangeModel"
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_NPCID = 0
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_NewNPCID = 0
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_Delay = 1
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_ModelPos = Vector3(0,-0.71,4.66)
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_BoundArea = {0,0,500,500}
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_Fx = ""
LuaMakeBeautificationWndMgr.m_MakeBeautificationWnd_TitleText = ""
function LuaMakeBeautificationWndMgr:ShowMakeBeautificationWnd(eventName,taskId)
    if not taskId then return end
    self.m_MakeBeautificationWnd_TaskId = taskId
    self.m_MakeBeautificationWnd_EventName = eventName
    local data = ZhuJueJuQing_LongPressChangeModel.GetData(taskId)
    if data then
        self.m_MakeBeautificationWnd_NPCID = data.NPCID
        self.m_MakeBeautificationWnd_NewNPCID = data.NewNPCID
        self.m_MakeBeautificationWnd_Delay = data.Delay
        self.m_MakeBeautificationWnd_ModelPos = Vector3(data.ModelPos[0],data.ModelPos[1],data.ModelPos[2])
        self.m_MakeBeautificationWnd_BoundArea = {data.BoundArea[0],data.BoundArea[1],data.BoundArea[2],data.BoundArea[3]}
        self.m_MakeBeautificationWnd_Fx = data.Fx
        self.m_MakeBeautificationWnd_TitleText = data.TitleText
    end
    CUIManager.ShowUI(CLuaUIResources.MakeBeautificationWnd)
end
