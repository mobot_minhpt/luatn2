require("common/common_include")
CLuaCommonCharacterDescMgr = class()
CLuaCommonCharacterDescMgr.m_Portrait = nil
CLuaCommonCharacterDescMgr.m_Name = nil
CLuaCommonCharacterDescMgr.m_Desc = nil

function CLuaCommonCharacterDescMgr.ShowWnd(portrait, name, desc)
	CLuaCommonCharacterDescMgr.m_Portrait = portrait
	CLuaCommonCharacterDescMgr.m_Name = name
	CLuaCommonCharacterDescMgr.m_Desc = desc
	CUIManager.ShowUI(CLuaUIResources.CommonCharacterDescWnd)
end


return CLuaCommonCharacterDescMgr