require("common/common_include")

local UIGrid = import "UIGrid"
--local Baby_QiChang = import "L10.Game.Baby_QiChang"
local QnCheckBox = import "L10.UI.QnCheckBox"
local Vector3 = import "UnityEngine.Vector3"
local CButton = import "L10.UI.CButton"

LuaBabyQiChangFxWnd = class()

RegistChildComponent(LuaBabyQiChangFxWnd, "FxGrid", UIGrid)
RegistChildComponent(LuaBabyQiChangFxWnd, "SaveButton", CButton)
RegistChildComponent(LuaBabyQiChangFxWnd, "FxTemplate", GameObject)
RegistChildComponent(LuaBabyQiChangFxWnd, "Anchor", GameObject)

RegistClassMember(LuaBabyQiChangFxWnd, "SelectedBaby")
RegistClassMember(LuaBabyQiChangFxWnd, "PreviewId")
RegistClassMember(LuaBabyQiChangFxWnd, "FxInfos")
RegistClassMember(LuaBabyQiChangFxWnd, "FxItems")

function LuaBabyQiChangFxWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaBabyQiChangFxWnd:InitClassMembers()

end

function LuaBabyQiChangFxWnd:InitValues()
	if not LuaBabyMgr.m_ChosenBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyQiChangFxWnd)
	end
	self.Anchor.transform.localPosition = Vector3(-640, 0, 0)
	self.SelectedBaby = LuaBabyMgr.m_ChosenBaby
	self.FxTemplate:SetActive(false)
	self.PreviewId = self.SelectedBaby.Props.QiChangData.ShowQiChangId

	local onSaveButtonClicked = function (go)
		self:OnSaveButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.SaveButton.gameObject, DelegateFactory.Action_GameObject(onSaveButtonClicked), false)
	self:UpdateQiChangFxs()
end

function LuaBabyQiChangFxWnd:UpdateQiChangFxs()
	CUICommonDef.ClearTransform(self.FxGrid.transform)
	self.FxItems = {}
	self.FxInfos = {}

	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	local currentQiChangId = self.SelectedBaby.Props.QiChangData.CurrentQiChangId
	local showQiChangId = self.SelectedBaby.Props.QiChangData.ShowQiChangId

	-- 增加无特效
	table.insert(self.FxInfos, {
		isActive = true,
        isCurrent = 0 == showQiChangId,
        isPreviewing = self.PreviewId == 0,
        qichangId = 0,
        qichangQuality = 1,
        qichangName = LocalString.GetString("无特效")
	})
	-- 增加其他拥有特效的气场
	Baby_QiChang.ForeachKey(function (key)
        local qichang = Baby_QiChang.GetData(key)
        if qichang and qichang.FxId ~= 0 then
        	local name = qichang.NameM
        	if self.SelectedBaby.Gender == 1 then
        		name = qichang.NameF
        	end
        	table.insert(self.FxInfos, {
				isActive = CommonDefs.DictContains_LuaCall(activedQiChang, key),
        		isCurrent = key == showQiChangId,
        		isPreviewing = self.PreviewId == key,
        		qichangId = key,
        		qichangQuality = qichang.Quality,
        		qichangName = name,
			})
        end
    end)

    -- 排序
    local compare = function (a, b)
    	if a.isActive and not b.isActive then
    		return true
    	elseif not a.isActive and b.isActive then
    			return false
    	else
    		return a.qichangQuality < b.qichangQuality
    	end
    end
    table.sort(self.FxInfos, compare)


    for i = 1, #self.FxInfos do
    	local go = NGUITools.AddChild(self.FxGrid.gameObject, self.FxTemplate)
    	self:InitQiChangFxItem(go, self.FxInfos[i])
    	go:SetActive(true)
    	table.insert(self.FxItems, go)
    end
    self.FxGrid:Reposition()

end

function LuaBabyQiChangFxWnd:InitQiChangFxItem(go, info)
	local QnCheckBox = go.transform:Find("QnCheckbox"):GetComponent(typeof(QnCheckBox))
	local StatusLabel = go.transform:Find("StatusLabel"):GetComponent(typeof(UILabel))
	local BG = go.transform:GetComponent(typeof(UISprite))

	StatusLabel.text = nil
	QnCheckBox:SetSelected(info.isPreviewing, false)
	QnCheckBox.Text = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangShuColor(info.qichangQuality-1), info.qichangName)

	if not info.isActive then
		StatusLabel.text = LocalString.GetString("[B2B2B2]未解锁[-]")
		BG.alpha = 0.5
	elseif info.isCurrent then
		StatusLabel.text = LocalString.GetString("[19A502]生效中[-]")
	end

	local onFxItemClicked = function (gameObject)
		self:OnFxItemClicked(gameObject, info.qichangId, info)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onFxItemClicked), false)
end

function LuaBabyQiChangFxWnd:OnFxItemClicked(go, qichangId, info)

	for i = 1, #self.FxItems do
		self:SetItemSelected(self.FxItems[i], go == self.FxItems[i], self.FxInfos[i])
	end
	self.PreviewId = qichangId
	self.SaveButton.Enabled = info.isActive
	g_ScriptEvent:BroadcastInLua("PreviewBabyQiChangFx", self.PreviewId)
end

function LuaBabyQiChangFxWnd:SetItemSelected(go, isSelected, info)
	local QnCheckBox = go.transform:Find("QnCheckbox"):GetComponent(typeof(QnCheckBox))
	local StatusLabel = go.transform:Find("StatusLabel"):GetComponent(typeof(UILabel))
	StatusLabel.text = nil

	QnCheckBox:SetSelected(isSelected, false)
	if isSelected then
		StatusLabel.text = LocalString.GetString("[FCFC22]预览中[-]")
	else
		if not info.isActive then
			StatusLabel.text = LocalString.GetString("[B2B2B2]未解锁[-]")
		elseif info.isCurrent then
			StatusLabel.text = LocalString.GetString("[19A502]生效中[-]")
		end
	end
end

function LuaBabyQiChangFxWnd:OnSaveButtonClicked(go)
	if self.SelectedBaby then
		Gac2Gas.RequestSetShowQiChangId(self.SelectedBaby.Id, self.PreviewId)
	end
end

function LuaBabyQiChangFxWnd:OnSetShowQiChangIdSuccess(babyId, qichangId)
	if self.SelectedBaby.Id == babyId then
		self.PreviewId = self.SelectedBaby.Props.QiChangData.ShowQiChangId
		self:UpdateQiChangFxs()
	end
end

function LuaBabyQiChangFxWnd:OnEnable()
	g_ScriptEvent:AddListener("SetShowQiChangIdSuccess", self, "OnSetShowQiChangIdSuccess")
end

function LuaBabyQiChangFxWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SetShowQiChangIdSuccess", self, "OnSetShowQiChangIdSuccess")
end

function LuaBabyQiChangFxWnd:OnDestroy()
	g_ScriptEvent:BroadcastInLua("ResetQiChangFx")
end

return LuaBabyQiChangFxWnd