local UISprite = import "UISprite"
local UIPanel = import "UIPanel"
local GameObject  = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"

CLuaXianzhiChooseImageTaskWnd = class()

RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Bg", UISprite)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "AnimPanel", UIPanel)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Items", GameObject)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "LBtn", GameObject)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "RBtn", GameObject)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Point", GameObject)

RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Btns", GameObject)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Btn1", GameObject)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Btn2", GameObject)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Btn3", GameObject)
RegistChildComponent(CLuaXianzhiChooseImageTaskWnd, "Btn4", GameObject)

RegistClassMember(CLuaXianzhiChooseImageTaskWnd, "m_index")
RegistClassMember(CLuaXianzhiChooseImageTaskWnd, "m_dots")
RegistClassMember(CLuaXianzhiChooseImageTaskWnd, "m_tween")

function CLuaXianzhiChooseImageTaskWnd:Init( ... )
    self.m_index = 1
	self.Btns:SetActive(false)
	self.m_dots = {}
	for i = 1,3 do
		self.m_dots[i] = self.Point.transform:Find("Dot"..i):GetComponent(typeof(UISprite))
	end
	self:SetActiveDot(1)

	local leftbg = self.Bg.transform:Find("leftbg")
	local rightbg = self.Bg.transform:Find("rightbg")

	self.m_tween=LuaTweenUtils.TweenFloat(64,1380 , 1, function (val)
		self.Bg.width = val
		local bgpos = leftbg.localPosition;
		bgpos.x = -val / 2
		leftbg.localPosition = bgpos
		bgpos.x = -bgpos.x
		rightbg.localPosition = bgpos

		local v4 = self.AnimPanel.baseClipRegion
		v4.z = val - 50
		self.AnimPanel.baseClipRegion = v4
		if val >= 1380 then
			self.Btns:SetActive(true)
			self.m_tween=nil
		end
	end)

	local turnleft = function(go)
		self:Turn(-1)
	end

	local turnright = function(go)
		self:Turn(1)
	end

	CommonDefs.AddOnClickListener(self.LBtn,DelegateFactory.Action_GameObject(turnleft), false)
	CommonDefs.AddOnClickListener(self.RBtn,DelegateFactory.Action_GameObject(turnright), false)

	local btnclick = function(go)
		self:OnBtnClick(go)
	end


	CommonDefs.AddOnClickListener(self.Btn1,DelegateFactory.Action_GameObject(btnclick), false)
	CommonDefs.AddOnClickListener(self.Btn2,DelegateFactory.Action_GameObject(btnclick), false)
	CommonDefs.AddOnClickListener(self.Btn3,DelegateFactory.Action_GameObject(btnclick), false)
	CommonDefs.AddOnClickListener(self.Btn4,DelegateFactory.Action_GameObject(btnclick), false)
end

function CLuaXianzhiChooseImageTaskWnd:OnDisable()
	if self.m_tween ~= nil then
		LuaTweenUtils.Kill(self.m_tween,false)
	end
end

function CLuaXianzhiChooseImageTaskWnd:SetActiveDot(index)
	for i = 1,3 do
		if i == index then
			self.m_dots[i].color = Color(1,1,1,1)
			self.m_dots[i].width = 40;
			self.m_dots[i].height = 40;
		else
			self.m_dots[i].color = Color(0.5,0.5,0.5,1)
			self.m_dots[i].width = 20;
			self.m_dots[i].height = 20;
		end
	end
end

--左右翻页动画
function CLuaXianzhiChooseImageTaskWnd:Turn(dir)
	--m_index从1开始到3
	local nextindex = self.m_index + dir
	if nextindex <= 0 or nextindex >= 4 then return end
	--图片大小为1012，506是一般宽度，图片之间的间隙为20
	local curpos = -506 * (self.m_index * 2 - 1) + (-20 * (self.m_index-1))
	local tarpos = -506 * (nextindex * 2 - 1) + (-20 * (self.m_index-1))
	self.m_index = nextindex
	self:SetActiveDot(self.m_index)
	--if self.m_tween ~=nil then return end
	self.m_tween = LuaTweenUtils.TweenFloat(curpos, tarpos, 0.5, function (val)
		local pos = self.Items.transform.localPosition
		pos.x = val
		self.Items.transform.localPosition = pos
	end)
end

function CLuaXianzhiChooseImageTaskWnd:OnBtnClick(go)
	local index = 0
	if go == self.Btn1 then
		index = 1
	elseif go == self.Btn2 then
		index = 2
	elseif go == self.Btn3 then
		index = 3
	elseif go == self.Btn4 then
		index = 4
	end
	if index > 0 and index <= 4 then 
		CLuaXianzhiMgr.FinishXianZhiPictureTask(index)
	end
	CUIManager.CloseUI(CLuaUIResources.XianzhiChooseImageTaskWnd)
end

return CLuaXianzhiChooseImageTaskWnd
