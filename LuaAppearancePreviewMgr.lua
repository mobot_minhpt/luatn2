local CTalismanMgr = import "L10.Game.CTalismanMgr"
local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTalismanMgr = import "L10.Game.CTalismanMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CExpressionAppearanceMgr = import "L10.Game.CExpressionAppearanceMgr"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
local Int32 = import "System.Int32"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CFashionMgr = import "L10.Game.CFashionMgr"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CFashionVehicleRenewalMgr = import "L10.UI.CFashionVehicleRenewalMgr"
local RenewalType = import "L10.UI.RenewalType"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"
local EnumYingLingState = import "L10.Game.EnumYingLingState"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CItemConsumeWndMgr = import "L10.UI.CItemConsumeWndMgr"
local CRankData = import "L10.UI.CRankData"
local CLoginMgr = import "L10.Game.CLoginMgr"
LuaAppearancePreviewMgr = class()

LuaAppearancePreviewMgr.MenueType_Take_Off = 0--脫下
LuaAppearancePreviewMgr.MenueType_Delete = 1--丢弃
LuaAppearancePreviewMgr.MenueType_Renew = 2--续费
LuaAppearancePreviewMgr.MenueType_Change_Color = 3--调色
LuaAppearancePreviewMgr.MenueType_Take_On = 4--更换

LuaAppearancePreviewMgr.DressedCanColoed = 0
LuaAppearancePreviewMgr.DressedCannotColoed = 1
LuaAppearancePreviewMgr.NotDressedCanColoed = 2
LuaAppearancePreviewMgr.NotDressedCannotColoed = 3
LuaAppearancePreviewMgr.OutTimeCanRenew = 4
LuaAppearancePreviewMgr.OutTimeCannotRenew = 5

LuaAppearancePreviewMgr.CurSelectedWingId = 0
LuaAppearancePreviewMgr.CurSelectedGemGroupFxId = 0
LuaAppearancePreviewMgr.CurSelectedTalismanId = 0
LuaAppearancePreviewMgr.HideGemFxToOther = 0
LuaAppearancePreviewMgr.HideWingToOther = 0

LuaAppearancePreviewMgr.DefaultHeadIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_toubu.mat"
LuaAppearancePreviewMgr.DefaultBodyIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_shenti.mat"
LuaAppearancePreviewMgr.DefaultWeaponIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_wuqi.mat"
LuaAppearancePreviewMgr.DefaultBeiShiIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_beishi.mat"
LuaAppearancePreviewMgr.DefaultWingIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_chibang.mat"
LuaAppearancePreviewMgr.DefaultLuminousIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_guangxiao.mat"
LuaAppearancePreviewMgr.DefaultTalismanIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_fabao.mat"
LuaAppearancePreviewMgr.DefaulGemGroupIcon = "UI/Texture/Transparent/Material/raw_appearancepreviewwnd_shizhiling.mat"
LuaAppearancePreviewMgr.DefaultSkillAppearIcon = "UI/Texture/Item_Other/Material/other_1475.mat"
LuaAppearancePreviewMgr.GemGroupIcon = "UI/Texture/Item_Other/Material/other_1044.mat"


LuaAppearancePreviewMgr.TalismanIconInfo = {}
LuaAppearancePreviewMgr.JobToIcon = nil
LuaAppearancePreviewMgr.ShowTagIcon = "common_thumb_open"
LuaAppearancePreviewMgr.CloseTagIcon = "common_thumb_close"
LuaAppearancePreviewMgr.AttributeList = {}
LuaAppearancePreviewMgr.IsTransformed = false
LuaAppearancePreviewMgr.ColorRGB2RanFaJiId = nil

function LuaAppearancePreviewMgr:IsAppearanceWndLoaded()
    return CUIManager.IsLoaded(CLuaUIResources.AppearanceWnd)
end

function LuaAppearancePreviewMgr:ShowAppearanceWnd()
    CUIManager.ShowUI(CLuaUIResources.AppearanceWnd)
end

function LuaAppearancePreviewMgr:CloseAppearanceWnd()
    CUIManager.CloseUI(CLuaUIResources.AppearanceWnd)
end

function LuaAppearancePreviewMgr:AddListener( ... )
    g_ScriptEvent:RemoveListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
    g_ScriptEvent:AddListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
end

LuaAppearancePreviewMgr:AddListener()

LuaAppearancePreviewMgr.IsBeiShiSetting = false
LuaAppearancePreviewMgr.IsBeiShiFromOut = false

function LuaAppearancePreviewMgr.ShowBeiShiSetting()
    LuaAppearancePreviewMgr.IsBeiShiSetting = true
    if LuaAppearancePreviewMgr:IsAppearanceWndLoaded() then
        LuaAppearancePreviewMgr.IsBeiShiFromOut = false
        g_ScriptEvent:BroadcastInLua("OpenBeiShiSetting")
    else
        LuaAppearancePreviewMgr.IsBeiShiFromOut = true
        LuaAppearancePreviewMgr:ShowAppearanceWnd()
    end
end

--拷贝自广忠在LuaAppearancePreviewWnd中的处理，用于复用
function LuaAppearancePreviewMgr:GetBeiShiPos(x, y, z)
    if not CClientMainPlayer.Inst then
        return Vector3(0, 0, 0)
    end
    local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Body, EnumBodyPosition_lua.BackPendant)
    local item = CItemMgr.Inst:GetById(id)

    local fid = CClientMainPlayer.Inst.AppearanceProp.BackPendantFashionId
    local data = nil

    if fid > 0 then --有拓本或者时装
        local taben =  EquipmentTemplate_Equip.GetData(fid)
        if taben then
            data = EquipmentTemplate_BackPendantExt.GetData(fid)
        else
            data = Fashion_BackPendantExt.GetData(fid)
        end
    else
        data = EquipmentTemplate_BackPendantExt.GetData(item.TemplateId)
    end

    if data == nil then
        return Vector3(0, 0, 0)
    end

    local job = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Class)
    local gender = CommonDefs.Convert_ToUInt32(CClientMainPlayer.Inst.Gender)

    local offsetx, offsety, offsetz = 0, 0, 0
    local offsets = data.DefaultOffset
    if offsets then
        for i = 0, offsets.Length - 1, 5 do
            if offsets[i] == job and offsets[i + 1] == gender then
                offsetx = offsets[i + 2]
                offsety = offsets[i + 3]
                offsetz = offsets[i + 4]
            end
        end
    end

    local scale = 1

    local edata = ExtraEquip_BackPendantSize.GetData(job)
    local sizes = edata.Size
    if gender < 2 then
        scale = sizes[gender]
    end

    local vx = offsetx + data.XMinMax[0] + (data.XMinMax[1] - data.XMinMax[0]) * x
    local vy = offsety + data.YMinMax[0] + (data.YMinMax[1] - data.YMinMax[0]) * y
    local vz = offsetz + data.ZMinMax[0] + (data.ZMinMax[1] - data.ZMinMax[0]) * z
    return Vector3(scale * vx, scale * vy, scale * vz)
end

function LuaAppearancePreviewMgr.OnTalismanFxListUpdate()
	-- 脱了仙家法宝后，原先身上的仙家法宝特效还保留，仅不让再选
    --if CClientMainPlayer.Inst.TalismanAppearanceList.Count == 0 then
		--Gac2Gas.SelectTalismanAppearance(0, 0, 0)
    --end
end

function LuaAppearancePreviewMgr.InitTalismanData()
    -- 仅加载一次,刷新需要清空LuaAppearancePreviewMgr.JobToIcon
    if LuaAppearancePreviewMgr.JobToIcon then
        return
    end

    local xianJiaIdList = {}
    local i = 1
    Talisman_XianJia.Foreach(function (key, data)
        if i%4 == 1 then
            table.insert(xianJiaIdList,key)
        end
        i = i + 1
    end)

    LuaAppearancePreviewMgr.JobToIcon = {}
    for i,templateId in ipairs(xianJiaIdList) do
        local equipData = EquipmentTemplate_Equip.GetData(templateId)
        local job = equipData.Race[0]
        local icon1 = equipData.Icon
        local grad = 1
        if not LuaAppearancePreviewMgr.JobToIcon[job] then
            local icon2 = EquipmentTemplate_Equip.GetData(templateId+1).Icon
            local icon3 = EquipmentTemplate_Equip.GetData(templateId+2).Icon
            local icon4 = EquipmentTemplate_Equip.GetData(templateId+3).Icon
            LuaAppearancePreviewMgr.JobToIcon[job] = {icon1,icon2,icon3,icon4}
        end
    end
end

function LuaAppearancePreviewMgr.GetTalismanAppearanceIcon(talismanId, talismanFxId)
    local job = CClientMainPlayer.Inst.BasicProp.Class

    return LuaAppearancePreviewMgr.GetTalismanAppearanceIconUseJob(talismanId, talismanFxId, job)
end

function LuaAppearancePreviewMgr.GetTalismanAppearanceIconUseJob(talismanId, talismanFxId, job)
    local icon
    if talismanId > 0 then
      talismanId = talismanId % 100
    end
    local grad = math.floor(talismanId / 10)
    if talismanId == 0 then
        --default icon
    elseif talismanId ~= -1 then
        if grad >= 1 and grad <= 4 then
            icon = LuaAppearancePreviewMgr.JobToIcon[job]
            if icon then
                icon = icon[grad]
            end
        else
            local fxId = CTalismanMgr.Inst:FindTalismanFx(talismanId)
            icon = ItemGet_TalismanFX.GetData(fxId).Icon
        end
    elseif talismanFxId then
        icon = ItemGet_TalismanFX.GetData(talismanFxId).Icon
    end
    return icon
end

function LuaAppearancePreviewMgr.GetCurrentTransformMonsterId()
    local monsterId = 0
    if IsTransformableFashionOpen() and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsTransformOn then
        local headId = CClientMainPlayer.Inst.AppearanceProp.HeadFashionId
        local bodyId = CClientMainPlayer.Inst.AppearanceProp.BodyFashionId
        local head = Fashion_Fashion.GetData(headId)
        local body = Fashion_Fashion.GetData(bodyId)
        local transformable = false
        if body and head and body.Appearance == head.Appearance then
            if CClientMainPlayer.Inst.Gender == EnumGender.Male then
                transformable = body.TransformMonsterMale ~= 0 and head.TransformMonsterMale ~= 0 and body.TransformMonsterMale == head.TransformMonsterMale
                monsterId = transformable and body.TransformMonsterMale or 0
            else
                transformable = body.TransformMonsterFemale ~= 0 and head.TransformMonsterFemale ~= 0 and body.TransformMonsterFemale == head.TransformMonsterFemale
                monsterId = transformable and body.TransformMonsterFemale or 0
            end
        end
	end
    return monsterId
end

function LuaAppearancePreviewMgr.InitRanFaJiData()
        -- 仅加载一次,刷新需要清空LuaAppearancePreviewMgr.ColorRGB2RanFaJiId
    if LuaAppearancePreviewMgr.ColorRGB2RanFaJiId then
        return
    end

    LuaAppearancePreviewMgr.ColorRGB2RanFaJiId = {}
    RanFaJi_RanFaJi.Foreach(function (key, data)
        LuaAppearancePreviewMgr.ColorRGB2RanFaJiId[data.ColorRGB] = key
    end)
end

function LuaAppearancePreviewMgr.GetRanFaJiIdByColorRGB(ColorRGB)
    return LuaAppearancePreviewMgr.ColorRGB2RanFaJiId[ColorRGB]
end

--------------------
-- 2023 迭代内容 本次迭代有很多重复逻辑多次使用，提取到这里减少功能冗余
--------------------
LuaAppearancePreviewMgr.m_EnableNewAppearanceWnd = true

EnumAppearancePreviewLuminousType = {
    eIntensifySuit = 1,
    eGemGroup = 2,
    eSkillAppearance = 3
}

EnumAppearanceFashionPosition = { --对应策划表Wardrobe_Fashion的Position字段
    eUndefined = -1,
    eHead = 0,
    eBody = 1,
    eWeapon = 2,
    eBeiShi = 3,
}

EnumAppearanceFashionUnlockType = {
    eDefault = 0, --已有的时装解锁永久
    eItem = 1,  --通过道具解锁永久时装
}

LuaAppearancePreviewMgr.m_SettingTargetWorldCenter = nil
LuaAppearancePreviewMgr.m_SettingTargetWidth = nil
LuaAppearancePreviewMgr.m_RequestPresentFasionId = nil
LuaAppearancePreviewMgr.m_RequestUnlockFasionId = nil
LuaAppearancePreviewMgr.m_RequestUnlockFasionExtInfo = nil
LuaAppearancePreviewMgr.m_RequestEvaluateFasionSuitId = nil
--试穿ID 用于AppearanceWnd和Subview之间共享，AppearanceSettingWnd里面的头纱开关也引用了这里
LuaAppearancePreviewMgr.m_PreviewHeadFashionId = 0
LuaAppearancePreviewMgr.m_PreviewBodyFashionId = 0
LuaAppearancePreviewMgr.m_PreviewWeaponFashionId = 0
LuaAppearancePreviewMgr.m_PreviewBackPendantFashionId = 0
LuaAppearancePreviewMgr.m_PreviewFashionSuitId = 0
LuaAppearancePreviewMgr.m_PreviewIsEntiretyHead = false
--打开窗口时指定跳转信息
LuaAppearancePreviewMgr.m_DefaultWndTabIndex = -1
LuaAppearancePreviewMgr.m_DefaultWndSection = -1
LuaAppearancePreviewMgr.m_DefaultWndRow = -1


function LuaAppearancePreviewMgr:EnableNewAppearanceWnd()
    self:CloseAppearanceWnd()--关闭可能存在的旧界面，新界面也可以直接关闭
    self.m_EnableNewAppearanceWnd = true
    CClientMainPlayer.s_EnableNewAppearancePreview = true
end

function LuaAppearancePreviewMgr:DisableNewAppearanceWnd()
    self:CloseAppearanceWnd()--关闭可能存在的旧界面，新界面也可以直接关闭
    self.m_EnableNewAppearanceWnd = false
    CClientMainPlayer.s_EnableNewAppearancePreview = false
end

function LuaAppearancePreviewMgr:ShowSettingWnd(targetWorldCenter, targetWidth)
    self.m_SettingTargetWorldCenter = targetWorldCenter
    self.m_SettingTargetWidth = targetWidth
    CUIManager.ShowUI("AppearanceSettingWnd")
end

function LuaAppearancePreviewMgr:RequestChangeXianFanAppearance(changeToXianShen)
    local appearance = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp or nil
    if not appearance then return end
    if changeToXianShen then
        --变成仙身
        local isFashionSupportXianShen = CClientMainPlayer.IsFashionSupportFeiSheng(appearance.HeadFashionId, appearance.BodyFashionId, appearance.HideHeadFashionEffect, appearance.HideBodyFashionEffect)
        if isFashionSupportXianShen then
            Gac2Gas.RequestSwitchFeiShengAppearanceXainFanStatus(1)
        else
            g_MessageMgr:ShowMessage("XianSheng_No_Fashion")
        end
    else
        --变成凡身
        if CClientMainPlayer.Inst.Is150GhostBody or CClientMainPlayer.Inst.Is150GhostHead then
            g_MessageMgr:ShowMessage("Tip_FanShen_Ghost150")
        end
        Gac2Gas.RequestSwitchFeiShengAppearanceXainFanStatus(0)
    end
end

function LuaAppearancePreviewMgr:OpenAppearanceShop()
    CUIManager.ShowUI("AppearanceShopWnd")
end

function LuaAppearancePreviewMgr:ShowFashionabilityRankWnd()
    CRankData.Inst.InitRankId = 41000299
	if IsOpenNewRankWnd() then
	    CUIManager.ShowUI(CLuaUIResources.NewRankWnd)
	else
		CUIManager.ShowUI(CUIResources.RankWnd)
	end
end
------------------我的搭配-------------------
function LuaAppearancePreviewMgr:GetAllOutfitInfo()
    local ret = {}
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return ret end
    CommonDefs.DictIterate(mainplayer.WardrobeProp.FashionDaPeiInfos, DelegateFactory.Action_object_object(function (key, data)
        table.insert(ret, {id=key, name=LocalString.TranslateAndFormatText(data.Name), 
        icons={self:GetFashionIconById(EnumAppearanceFashionPosition.eHead,data.HeadId),
            self:GetFashionIconById(EnumAppearanceFashionPosition.eBody,data.BodyId),
            self:GetFashionIconById(EnumAppearanceFashionPosition.eWeapon,data.WeaponId),
            self:GetFashionIconById(EnumAppearanceFashionPosition.eBeiShi,data.BackPendantId)},
        itemIds={self:GetItemTemplateById(data.HeadId),
            self:GetItemTemplateById(data.BodyId),
            self:GetItemTemplateById(data.WeaponId),
            self:GetItemTemplateById(data.BackPendantId)},
         fashions={data.HeadId, data.BodyId, data.WeaponId, data.BackPendantId}})
    end))
    return ret
end

function LuaAppearancePreviewMgr:RequestModifyFashionDaPeiName_Permanent(index, newName)
    Gac2Gas.RequestModifyFashionDaPeiName_Permanent(index, newName)
end

function LuaAppearancePreviewMgr:RequestSaveFashionDaPeiContent_Permanent(index)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local headId = mainplayer.AppearanceProp.HeadFashionId
    local bodyId = mainplayer.AppearanceProp.BodyFashionId
    local weaponId = mainplayer.AppearanceProp.WeaponFashionId
    local beishiId = mainplayer.AppearanceProp.BackPendantFashionId

    local originDaPei = CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionDaPeiInfos, index)
    if originDaPei.HeadId~=headId or originDaPei.BodyId~=bodyId or originDaPei.WeaponId~=weaponId or originDaPei.BackPendantId~=beishiId then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_Save_Da_Pei_Confirm"), function()
            Gac2Gas.RequestSaveFashionDaPeiContent_Permanent(index)
        end, nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("Appearance_Preview_Save_Same_Da_Pei_Tip")
    end
end

function LuaAppearancePreviewMgr:RequestUseFashionDaPei_Permanent(index)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local headId = mainplayer.AppearanceProp.HeadFashionId
    local bodyId = mainplayer.AppearanceProp.BodyFashionId
    local weaponId = mainplayer.AppearanceProp.WeaponFashionId
    local beishiId = mainplayer.AppearanceProp.BackPendantFashionId

    local originDaPei = CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionDaPeiInfos, index)
    if originDaPei.HeadId==headId and originDaPei.BodyId==bodyId and originDaPei.WeaponId==weaponId and originDaPei.BackPendantId==beishiId then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_Use_Same_Da_Pei_Confirm"), function()
            Gac2Gas.RequestUseFashionDaPei_Permanent(index)
        end, nil, nil, nil, false)
    elseif (originDaPei.HeadId>0 and not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(originDaPei.HeadId)) or
            (originDaPei.BodyId>0 and not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(originDaPei.BodyId)) or
            (originDaPei.WeaponId>0 and not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(originDaPei.WeaponId)) or
            (originDaPei.BackPendantId>0 and not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(originDaPei.BackPendantId)) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_Da_Pei_Has_Unavailable_Fashion"), function()
            Gac2Gas.RequestUseFashionDaPei_Permanent(index)
        end, nil, nil, nil, false)  
    else
        Gac2Gas.RequestUseFashionDaPei_Permanent(index)
    end
end

-------------------时装-------------------
--根据主角的性别和职业获取时装或者套装的大图，Wardrobe_Suit表的字段名称需要和Fashion保持一致
function LuaAppearancePreviewMgr:GetFashionBigIcon(fashionOrSuitData)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        if mainplayer.Gender==EnumGender.Male then
            return fashionOrSuitData.NewWardRobeIconMale
        elseif mainplayer.Class == EnumClass.HuaHun then
            return fashionOrSuitData.NewWardRobeIconLoli
        else
            return fashionOrSuitData.NewWardRobeIconFemale
        end
    end
    return fashionOrSuitData.NewWardRobeIconMale
end
function LuaAppearancePreviewMgr:GetMyFashionInfo(fashionPos, sortIndex)
    local ret = {}
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return ret end
    CommonDefs.DictIterate(mainplayer.WardrobeProp.FashionMainInfos, DelegateFactory.Action_object_object(function (key, info)
        local data = Fashion_Fashion.GetData(key)
        if data==nil then return end --拓本在另外的表里面
        if data.IsNewWardrobe==1 and data.Position == fashionPos then
            local name = self:FilterFashionName(data.Name)
            table.insert(ret,{id=key, name=name, icon=data.Icon, bigIcon=self:GetFashionBigIcon(data), durability=data.Durability,
                canRenewal=data.CanRenewal>0, itemGetId=data.ItemID, quality=data.Quality,order=data.Order,
                specialExpression=data.SpecialExpression, transformFemale=data.TransformMonsterFemale,
                transformMale=data.TransformMonsterMale, video=data.PreviewVideoUrl, multicolor=data.Multicolor})
        end
    end))

    if sortIndex<1 or sortIndex>3 then sortIndex = 1 end
    self:SortFashionInfo(ret, sortIndex)
    return ret
end
--是否存在特定部位的时装（非拓本）
function LuaAppearancePreviewMgr:MyFashionExists(fashionPos)
    local bFound = false
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return bFound end
    CommonDefs.DictIterate(mainplayer.WardrobeProp.FashionMainInfos, DelegateFactory.Action_object_object(function (key, info)
        local data = Fashion_Fashion.GetData(key)
        if data==nil then return end --拓本在另外的表里面
        if data.IsNewWardrobe==1 and data.Position == fashionPos then
            bFound = true
        end
    end))
    return bFound
end

--所有的拓本
function LuaAppearancePreviewMgr:GetMyTabenInfo(sortIndex)
    local ret = {}
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return ret end
    CommonDefs.DictIterate(mainplayer.WardrobeProp.FashionMainInfos, DelegateFactory.Action_object_object(function (key, info)
        local data = EquipmentTemplate_Equip.GetData(key)
        if data==nil then return end --非拓本
        local fashionPos = EnumAppearanceFashionPosition.eUndefined
        local tabenType = CommonDefs.ConvertIntToEnum(typeof(EnumBodyPosition), data.Type)
        if tabenType==EnumBodyPosition.Casque then fashionPos = EnumAppearanceFashionPosition.eHead
        elseif tabenType==EnumBodyPosition.Armour then fashionPos = EnumAppearanceFashionPosition.eBody
        elseif tabenType==EnumBodyPosition.Weapon then fashionPos = EnumAppearanceFashionPosition.eWeapon
        elseif tabenType==EnumBodyPosition.BackPendant then fashionPos = EnumAppearanceFashionPosition.eBeiShi
        else return end
        table.insert(ret,{id=key, name=SafeStringFormat3(LocalString.GetString("%s·拓本"), data.Name), icon=data.Icon,
            position=fashionPos, expiredTime=info.ExpireTime})
    end))
    if sortIndex<1 or sortIndex>2 then sortIndex = 1 end
    table.sort(ret, function (a, b)
        if sortIndex==1 and a.expiredTime~=b.expiredTime then --过期时间从小到大排序
            return a.expiredTime<b.expiredTime
        end
        if sortIndex==2 and a.position~=b.position then --部位从小到大排序
            return a.position<b.position
        end
        return a.id<b.id
    end)
    return ret
end

--新旧时装功能过渡期间，对旧时装名中的有效期信息进行过滤
function LuaAppearancePreviewMgr:FilterFashionName(oldFashionName)
    return CommonDefs.GenericStringFilter(oldFashionName,("(\\(|（)(.+?)(\\)|）)"), 0, "")--去掉带有时间信息的后缀
end

function LuaAppearancePreviewMgr:IsTabenExpired(expiredTime)
    return expiredTime<CServerTimeMgr.Inst.timeStamp 
end

function LuaAppearancePreviewMgr:GetTabenNumLimit()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.WardrobeProp.TabenNumLimit or 0
end

function LuaAppearancePreviewMgr:GetTabenMaxNumLimit()
    return tonumber(Fashion_Setting.GetData("Final_Fashion_Max").Value)
end

function LuaAppearancePreviewMgr:ExpandTabenNumLimit()
    local expandItemId = tonumber(Fashion_Setting.GetData("Fashion_KuoRong_ItemId").Value)
    local item = Item_Item.GetData(expandItemId)
    CItemConsumeWndMgr.ShowItemConsumeWnd(expandItemId, 1,
        SafeStringFormat3(LocalString.GetString("解锁需要消耗%s"), item.Name),
        LocalString.GetString("解锁"),
        DelegateFactory.Action(function ()
            local bFound, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, expandItemId)
            Gac2Gas.RequestUseItem(EnumItemPlace.Bag, pos, itemId, "")
        end),
        DelegateFactory.Action(function ()
            CUIManager.ShowUI(CUIResources.LingShouPurchaseWnd)                                                               

        end),
        DelegateFactory.Action(function ()
            Gac2Gas.FastAddWardRobeNumLimit_Permanent()
        end))
end

-- Taben
function LuaAppearancePreviewMgr:GetTabenExpiredText(expiredTime)
    if expiredTime<CServerTimeMgr.Inst.timeStamp then
        return "[c][FF0000]"..LocalString.GetString("已过期").."[-][/c]"
    else
        local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(expiredTime)
	    return ToStringWrap(dt, LocalString.GetString("有效期至 yy-MM-dd HH:mm"))
    end
end

function LuaAppearancePreviewMgr:GetAllFashionSuitInfoSortNames()
    return {LocalString.GetString("按款式"), LocalString.GetString("按品质")}
end

function LuaAppearancePreviewMgr:GetAllFashionInfoSortNames()
    return {LocalString.GetString("按款式"), LocalString.GetString("按品质"), LocalString.GetString("按收集度")}
end

function LuaAppearancePreviewMgr:GetMyFashionInfoSortNames()
    return {LocalString.GetString("按款式"), LocalString.GetString("按品质"), LocalString.GetString("按收集度")}
end

function LuaAppearancePreviewMgr:GetMyTabenInfoSortNames()
    return {LocalString.GetString("按过期时间"), LocalString.GetString("按部位")}
end

function LuaAppearancePreviewMgr:ExistNewFashionSuitRecommendation()
    local exist = false
    Wardrobe_FashionSuit.Foreach(function (key, data)
        if exist then return end
        local headData = Fashion_Fashion.GetData(data.HeadId)
        local bodyData = Fashion_Fashion.GetData(data.BodyId)
        local bShowHeadNewDisplay = headData.NewDisplayTime~=nil and headData.NewDisplayTime~="" and CServerTimeMgr.Inst:GetTimeStampByStr(headData.NewDisplayTime) > CServerTimeMgr.Inst.timeStamp or false
        local bShowBodyNewDisplay = bodyData.NewDisplayTime~=nil and bodyData.NewDisplayTime~="" and CServerTimeMgr.Inst:GetTimeStampByStr(bodyData.NewDisplayTime) > CServerTimeMgr.Inst.timeStamp or false
        if bShowHeadNewDisplay or bShowBodyNewDisplay then
            exist = true
        end
    end)
    return exist
end

function LuaAppearancePreviewMgr:GetAllFashionInfo(fashionPos, sortIndex)
    local ret = {}
    local multiColorFashionTbl = {}
    Fashion_Fashion.Foreach(function (key, data)
        if data.IsNewWardrobe==1 and data.Position == fashionPos then
            --借助TransferId字段判断需要显示在全部下的时装, DisplayStatus不为0时一定不显示在试衣间里
            if (key==data.TransferId and data.DisplayStatus==0) then
                local name = self:FilterFashionName(data.Name)
                local record = {}
                record.id = key
                record.name = name
                record.icon = data.Icon
                record.bigIcon = self:GetFashionBigIcon(data)
                record.durability = data.Durability
                record.canRenewal = data.CanRenewal>0
                record.itemGetId = data.ItemID
                record.quality = data.Quality
                record.fashionability = data.Fashionability
                record.qualityName = self:GetFashionQualityName(data.Quality)
                record.specialExpression = data.SpecialExpression
                record.transformFemale = data.TransformMonsterFemale
                record.transformMale = data.TransformMonsterMale
                record.video = data.PreviewVideoUrl
                record.order = data.Order
                record.newDisplayTime = data.NewDisplayTime~=nil and data.NewDisplayTime~="" and CServerTimeMgr.Inst:GetTimeStampByStr(data.NewDisplayTime) or 0
                record.bShowNewDisplay = record.newDisplayTime>CServerTimeMgr.Inst.timeStamp
                record.isInUse = self:IsCurrentInUseFashion(fashionPos, key)
                record.isOwned = self:IsMainPlayerFashionAvailable(key)
                record.multicolor = data.Multicolor
                record.fashionTbl = {}

                if record.multicolor>0 then
                    if multiColorFashionTbl[record.multicolor]==nil then
                        multiColorFashionTbl[record.multicolor] = {}
                    end
                    table.insert(multiColorFashionTbl[record.multicolor], record)
                else
                    table.insert(ret, record)
                end
            end
        end 
    end)
    for id, tbl in pairs(multiColorFashionTbl) do
        table.sort(tbl, function (a, b)
            if a.id==id then return true end
            if b.id==id then return false end
            if a.order~=b.order then
                return a.order>b.order
            end
            return a.id<b.id
        end)
        for __,record in pairs(tbl) do
            record.fashionTbl = tbl
        end
        table.insert(ret, tbl[1]) -- 将盲盒款中的第一个放到列表中显示
    end
    if sortIndex<1 or sortIndex>3 then sortIndex = 1 end
    self:SortFashionInfo(ret, sortIndex)
    return ret
end

function LuaAppearancePreviewMgr:SortFashionInfo(tbl, sortIndex)
    if tbl then
        --找到时装（可能是盲盒）中的解锁进度信息
        local getUnlockInfoFunc=function(data)
            if data.fashionTbl==nil or #data.fashionTbl==0 then
                local mainInfo = self:GetFashionMainInfo(data.id)
                if mainInfo~=nil then
                    return true, mainInfo.IsUnLock, mainInfo.Progress
                else
                    return false,0,0
                end
            else
                local hasMainInfo, isUnLock, progress = false,0,0
                for __,val in pairs(data.fashionTbl) do
                    local mainInfo = self:GetFashionMainInfo(val.id)
                    if mainInfo~=nil then
                        hasMainInfo = true
                        if mainInfo.IsUnLock>isUnLock then isUnLock = mainInfo.IsUnLock end
                        if mainInfo.IsUnLock==0 and mainInfo.Progress>progress then progress = mainInfo.Progress end
                    end
                end
                return hasMainInfo, isUnLock, progress
            end
        end

        table.sort(tbl, function (a, b)
            if sortIndex==1 and a.order~=b.order then --款式 order从大到小排序
                return a.order>b.order
            end
            if sortIndex==2 and a.quality~=b.quality then --品质
                return a.quality>b.quality
            end
            if sortIndex==3 then --收集度
                local hasMainInfo1, isUnLock1, progress1 = getUnlockInfoFunc(a)
                local hasMainInfo2, isUnLock2, progress2 = getUnlockInfoFunc(b)
                if hasMainInfo1 or hasMainInfo2 then
                    if not hasMainInfo1 then return false end
                    if not hasMainInfo2 then return true end
                    if isUnLock1~=isUnLock2 then return isUnLock1>isUnLock2 end
                    if progress1~=progress2 then return progress1>progress2 end
                end
            end
            if a.quality~=b.quality then return a.quality>b.quality end
            if a.order~=b.order then return a.order>b.order end
            return a.id<b.id
        end)
    end
end

function LuaAppearancePreviewMgr:GetAllFashionSuitInfo(sortIndex)
    local ret = {}
    local multiColorFashionTbl = {}
    Wardrobe_FashionSuit.Foreach(function (key, data)
        local headData = Fashion_Fashion.GetData(data.HeadId)
        local bodyData = Fashion_Fashion.GetData(data.BodyId)
        if headData.IsNewWardrobe~=1 or bodyData.IsNewWardrobe~=1 then
            return
        end
        --如果头部不可用，则不显示
        if headData.DisplayStatus~=0 then
            return
        end
        --如果身体不可用，则不显示
        if bodyData.DisplayStatus~=0 then
            return
        end
        -- 整体时装头部不填TransferId，这里可能会涉及整体时装， 因此和坤少以及策划同学商量了一下不检查头部
        if bodyData.TransferId~=data.BodyId then
            return
        end
        local headNewDisplayTime = headData.NewDisplayTime~=nil and headData.NewDisplayTime~="" and CServerTimeMgr.Inst:GetTimeStampByStr(headData.NewDisplayTime) or 0
        local bodyNewDisplayTime = bodyData.NewDisplayTime~=nil and bodyData.NewDisplayTime~="" and CServerTimeMgr.Inst:GetTimeStampByStr(bodyData.NewDisplayTime) or 0
        --格式同GetAllFashionInfo接口，以便于统一处理
        local record = {}
        record.id = key
        record.name = data.Name
        record.icon = data.Icon
        record.bigIcon = self:GetFashionBigIcon(data)
        record.durability = 0 --不处理解锁进度
        record.canRenewal = 0 --不处理续费
        record.itemGetId = 0 --不处理道具获取
        record.quality = bodyData.Quality
        record.fashionability = headData.Fashionability + bodyData.Fashionability
        record.qualityName = self:GetFashionQualityName(bodyData.Quality)
        record.specialExpression = bodyData.SpecialExpression
        record.transformFemale = bodyData.TransformMonsterFemale
        record.transformMale = bodyData.TransformMonsterMale
        record.video = bodyData.PreviewVideoUrl
        record.order = data.Order
        record.newDisplayTime = headNewDisplayTime>bodyNewDisplayTime and headNewDisplayTime or bodyNewDisplayTime
        record.bShowNewDisplay = record.newDisplayTime>CServerTimeMgr.Inst.timeStamp
        record.groupId = data.GroupID
        record.headId = data.HeadId
        record.bodyId = data.BodyId
        record.headItemGetId = headData.ItemID
        record.bodyItemGetId = bodyData.ItemID
        record.isInUse = self:IsCurrentInUseFashion(EnumAppearanceFashionPosition.eHead, data.HeadId) and self:IsCurrentInUseFashion(EnumAppearanceFashionPosition.eBody, data.BodyId)
        record.isOwned = self:IsMainPlayerFashionAvailable(data.HeadId) and self:IsMainPlayerFashionAvailable(data.BodyId)
        record.fashionTbl = {}

        if record.groupId>0 then
            if multiColorFashionTbl[record.groupId]==nil then
                multiColorFashionTbl[record.groupId] = {}
            end
            table.insert(multiColorFashionTbl[record.groupId], record)
        else
            table.insert(ret, record)
        end
    end)
    for id, tbl in pairs(multiColorFashionTbl) do
        table.sort(tbl, function (a, b)
            if a.id==id then return true end
            if b.id==id then return false end
            if a.order~=b.order then
                return a.order>b.order
            end
            return a.id<b.id
        end)
        for __,record in pairs(tbl) do
            record.fashionTbl = tbl
        end
        table.insert(ret, tbl[1]) -- 将盲盒款中的第一个放到列表中显示
    end
    if sortIndex<1 or sortIndex>2 then sortIndex = 1 end
    self:SortFashionInfo(ret, sortIndex)
    return ret
end

function LuaAppearancePreviewMgr:GetFashionUnlockProgressInfo(id)
    local mainInfo = self:GetFashionMainInfo(id)
    if mainInfo then 
        return mainInfo.Progress, mainInfo.IsUnLock>0
    end
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and CommonDefs.DictContains_LuaCall(mainplayer.WardrobeProp.FashionBackupProgress, id) then
        return CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionBackupProgress, id), false
    end
    return 0, false
end

function LuaAppearancePreviewMgr:GetFashionMainInfo(id)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and CommonDefs.DictContains_LuaCall(mainplayer.WardrobeProp.FashionMainInfos, id) then
        return CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionMainInfos, id) 
    end
    return nil
end

function LuaAppearancePreviewMgr:GetFashionNameById(id)
    local fashionData = Fashion_Fashion.GetData(id)
    if fashionData then return self:FilterFashionName(fashionData.Name) end
    local tabenData = EquipmentTemplate_Equip.GetData(id)
    if tabenData then return SafeStringFormat3(LocalString.GetString("%s·拓本"), tabenData.Name) end                                               
    return nil
end

function LuaAppearancePreviewMgr:GetFashionIconById(fashionPos, id)
    local fashionData = Fashion_Fashion.GetData(id)
    if fashionData then return fashionData.Icon end
    local tabenData = EquipmentTemplate_Equip.GetData(id)
    if tabenData then return tabenData.Icon end                                               
    return nil
    -- if fashionPos == EnumAppearanceFashionPosition.eHead then
    --     return self.DefaultHeadIcon
    -- elseif fashionPos == EnumAppearanceFashionPosition.eBody then
    --     return self.DefaultBodyIcon
    -- elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
    --     return self.DefaultWeaponIcon
    -- elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
    --     return self.DefaultBeiShiIcon
    -- else
    --     print("GetFashionIconById params error", fashionPos)
    --     return false
    -- end
end

function LuaAppearancePreviewMgr:GetFashionPositionName(fashionPos)
    if fashionPos == EnumAppearanceFashionPosition.eHead then
        return LocalString.GetString("头部")
    elseif fashionPos == EnumAppearanceFashionPosition.eBody then
        return LocalString.GetString("身体")
    elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
        return LocalString.GetString("武器")
    elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
        return LocalString.GetString("背饰")
    else
        return ""
    end
end

function LuaAppearancePreviewMgr:GetItemTemplateById(id)
    local fashionData = Fashion_Fashion.GetData(id)
    if fashionData then 
        return fashionData.ItemID
    elseif EquipmentTemplate_Equip.Exists(id) then
        return id
    else
        return 0
    end
end

function LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(id)
    local mainInfo = self:GetFashionMainInfo(id)
    if not mainInfo then return false end
    if mainInfo.IsUnLock==1 then return true end
    if mainInfo.ExpireTime==0 then return true end
    return mainInfo.ExpireTime>0 and mainInfo.ExpireTime>CServerTimeMgr.Inst.timeStamp
end

function LuaAppearancePreviewMgr:GetFashionExpireText(mainInfo)
    if mainInfo.IsUnLock==1 then
        return LocalString.GetString("永久获得")
    elseif mainInfo.ExpireTime==0 then
        return LocalString.GetString("永久获得")
    elseif mainInfo.ExpireTime<CServerTimeMgr.Inst.timeStamp then
        return "[c][FF0000]"..LocalString.GetString("已过期").."[-][/c]"
    else
        local dt =  CServerTimeMgr.ConvertTimeStampToZone8Time(mainInfo.ExpireTime)
        return SafeStringFormat3(LocalString.GetString("有效期至%s"), ToStringWrap(dt, ("yy-MM-dd HH:mm")))
    end
end

LuaAppearancePreviewMgr.m_Item2FashionTbl = nil
--类似CFashionMgr.Inst:GetFashionByItem，该方法用于新衣橱中获取道具对应的时装列表（绝大部分情况应该只对应一个）
function LuaAppearancePreviewMgr:GetFashionByItem(itemTemplateId)
    if self.m_Item2FashionTbl== nil then
        self.m_Item2FashionTbl = {}
        Fashion_Fashion.Foreach(function(k,v)
            if self.m_Item2FashionTbl[v.ItemID]==nil then
                self.m_Item2FashionTbl[v.ItemID] = {}
            end
            if self.m_Item2FashionTbl[v.ItemID][k]==nil then
                self.m_Item2FashionTbl[v.ItemID][k] = true
            end
            if v.AllOtherItemID then
                for i=0,v.AllOtherItemID.Length-1 do
                    local id = v.AllOtherItemID[i]
                    if self.m_Item2FashionTbl[id]==nil then
                        self.m_Item2FashionTbl[id] = {}
                    end
                    if self.m_Item2FashionTbl[id][k]==nil then
                        self.m_Item2FashionTbl[id][k] = true
                    end
                end
            end
        end)
    end
    return self.m_Item2FashionTbl[itemTemplateId]
end

-- copy from LuaFashionTemplate.lua
function LuaAppearancePreviewMgr:ShowPackageFashionItems(fashionPos)
    local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, System.String)))
    local equipList = CreateFromClass(MakeGenericClass(List, System.String))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize, 1 do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        if (not System.String.IsNullOrEmpty(id)) then
            local equip = CItemMgr.Inst:GetById(id)
            if (equip and equip.IsItem and self:GetFashionByItem(equip.TemplateId)~=nil) then
                 --equipList.Add(equip.Id)
                 CommonDefs.ListAdd_LuaCall(equipList, equip.Id)
            end
        end
    end
    --dic.Add(0, equipList)
    CommonDefs.DictAdd_LuaCall(dic, 0, equipList)
    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("AppearanceFashionWnd", dic, LocalString.GetString("包裹中的时装道具"), 0, false, LocalString.GetString("添加"), LocalString.GetString("获取更多时装"), DelegateFactory.Action(function ()
        CUIManager.CloseUI(CUIResources.FreightEquipSubmitWnd)
        if fashionPos == EnumAppearanceFashionPosition.eBeiShi then
            CShopMallMgr.ShowLingyuBackPendantShop()
        else
            CShopMallMgr.ShowLingyuFashionShop()
        end
    end), LocalString.GetString("包裹中暂无时装道具"))
end

function LuaAppearancePreviewMgr:AddFashionByItem(key, itemId)
    if key ~= "AppearanceFashionWnd" then return end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, pos, itemId, "")
end

function LuaAppearancePreviewMgr:RenewalFashion(id)
    CFashionVehicleRenewalMgr.type = RenewalType.PERMANENT_FASHION
    CFashionVehicleRenewalMgr.renewalList:Clear()
    CFashionVehicleRenewalMgr.renewalList:Add(id)
    CUIManager.ShowUI(CUIResources.FashionRenewalWnd)
end

function LuaAppearancePreviewMgr:MainPlayerHasFashion(id)
    return CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.WardrobeProp.FashionMainInfos, id) or false
end

function LuaAppearancePreviewMgr:IsCurrentInUseFashion(fashionPos, id)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return false end
    if fashionPos == EnumAppearanceFashionPosition.eHead then
        return mainplayer.AppearanceProp.HeadFashionId==id
    elseif fashionPos == EnumAppearanceFashionPosition.eBody then
        return mainplayer.AppearanceProp.BodyFashionId==id
    elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
            return mainplayer.AppearanceProp.WeaponFashionId==id
    elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
        return mainplayer.AppearanceProp.BackPendantFashionId==id
    else
        print("IsCurrentInUseFashion params error", fashionPos)
        return false
    end
end

function LuaAppearancePreviewMgr:GetCurrentInUseFashion(fashionPos)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return 0 end
    if fashionPos == EnumAppearanceFashionPosition.eHead then
        return mainplayer.AppearanceProp.HeadFashionId
    elseif fashionPos == EnumAppearanceFashionPosition.eBody then
        return mainplayer.AppearanceProp.BodyFashionId
    elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
            return mainplayer.AppearanceProp.WeaponFashionId
    elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
        return mainplayer.AppearanceProp.BackPendantFashionId
    else
        print("GetCurrentInUseFashion params error", fashionPos)
        return false
    end
end

function LuaAppearancePreviewMgr:GetCurrentInUseFashionSuit()
    local ret = 0
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return ret end
    Wardrobe_FashionSuit.Foreach(function(key, data)
        if data.HeadId == mainplayer.AppearanceProp.HeadFashionId and 
            data.BodyId == mainplayer.AppearanceProp.BodyFashionId then
            ret = key
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:IsShowFashion(fashionPos, isSubWeapon)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return false end
    if fashionPos == EnumAppearanceFashionPosition.eHead then
        return mainplayer.AppearanceProp.HideHeadFashionEffect == 0
    elseif fashionPos == EnumAppearanceFashionPosition.eBody then
        return mainplayer.AppearanceProp.HideBodyFashionEffect == 0
    elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
        if isSubWeapon then
            return mainplayer.AppearanceProp.HideSubHandWeaponFashionEffect == 0 
        else
            return mainplayer.AppearanceProp.HideWeaponFashionEffect == 0
        end
    elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
        return mainplayer.AppearanceProp.HideBackPendantFashionEffect == 0
    else
        print("IsShowFashion params error", fashionPos)
        return false
    end
end

function LuaAppearancePreviewMgr:MainPlayerHasEntiretyHead()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return false end
    return self:GetEntiretyHead(mainplayer.AppearanceProp.BodyFashionId)>0
end

function LuaAppearancePreviewMgr:GetEntiretyHead(bodyId)
    local body = Fashion_Fashion.GetData(bodyId)
    return body and body.EntiretyHeadId or 0
end

function LuaAppearancePreviewMgr:ChangeHeadAlphaVisibility(bShow)
    g_MessageMgr:ShowMessage(bShow and  "Appearance_Preview_TouSha_Switch_On" or "Appearance_Preview_TouSha_Switch_Off")
    Gac2Gas.SetPropertyAppearanceSettingInfo(EnumPropertyAppearanceSettingBit_lua.HideTouSha, bShow and 0 or 1)
end

function LuaAppearancePreviewMgr:CheckAndShowHideFashionTip(fashionPos)
    --根据最新的需求预览的时候不再检查并提示消息，改为服务器真实穿戴后再提示
    -- if not self:IsShowFashion(fashionPos, false) then
    --     if fashionPos == EnumAppearanceFashionPosition.eHead then
    --         g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Fashion_Head_Tip")
    --     elseif fashionPos == EnumAppearanceFashionPosition.eBody then
    --         g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Fashion_Body_Tip")
    --     elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
    --         g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Fashion_Primary_Weapon_Tip")
    --     elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
    --         g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Fashion_BeiShi_Tip")
    --     end
    -- end
end

function LuaAppearancePreviewMgr:ChangeFashionVisibility(fashionPos, isSubWeapon, bShow)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local hasEntiretyHead = self:MainPlayerHasEntiretyHead()
    if fashionPos == EnumAppearanceFashionPosition.eHead then
        Gac2Gas.RequestSetFashionHide_Permanent(0, bShow and 0 or 1)
        if not hasEntiretyHead then
            g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Fashion_Head_Switch_On" or "Appearance_Preview_Fashion_Head_Switch_Off")
        end
    elseif fashionPos == EnumAppearanceFashionPosition.eBody then
        Gac2Gas.RequestSetFashionHide_Permanent(1, bShow and 0 or 1)
        if not hasEntiretyHead then
            g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Fashion_Body_Switch_On" or "Appearance_Preview_Fashion_Body_Switch_Off")
        end
    elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
        if isSubWeapon then
            Gac2Gas.RequestSetFashionHide_Permanent(4, bShow and 0 or 1)
            if not hasEntiretyHead then
                g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Fashion_Sub_Weapon_Switch_On" or "Appearance_Preview_Fashion_Sub_Weapon_Switch_Off")
            end
        else
            Gac2Gas.RequestSetFashionHide_Permanent(2, bShow and 0 or 1)
            if not hasEntiretyHead then
                g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Fashion_Primary_Weapon_Switch_On" or "Appearance_Preview_Fashion_Primary_Weapon_Switch_Off")
            end
        end
    elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
        Gac2Gas.RequestSetFashionHide_Permanent(3, bShow and 0 or 1)
        if not hasEntiretyHead then
            g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Fashion_BeiShi_Switch_On" or "Appearance_Preview_Fashion_BeiShi_Switch_Off")
        end
    else
        print("ChangeFashionVisibility params error", fashionPos)
        return false
    end
end

function LuaAppearancePreviewMgr:ShowClosetFashionSubview(fashionId)
    local data = Fashion_Fashion.GetData(fashionId)
    local defaultSection = 1
    local defaultRow =0
    if data then
        if data.Position == EnumAppearanceFashionPosition.eHead then
            defaultRow = 0
        elseif data.Position == EnumAppearanceFashionPosition.eBody then
            defaultRow = 1
        elseif data.Position == EnumAppearanceFashionPosition.eBeiShi then
            defaultRow = 2
        end
    elseif EquipmentTemplate_Equip.Exists(fashionId) then
        defaultSection = 5
        defaultRow = 0
    end
    LuaAppearancePreviewMgr.m_DefaultWndTabIndex = 0
    LuaAppearancePreviewMgr.m_DefaultWndSection = defaultSection
    LuaAppearancePreviewMgr.m_DefaultWndRow = defaultRow
    CUIManager.ShowUI("AppearanceWnd")
end

function LuaAppearancePreviewMgr:ShowClosetProfileFrameSubview()
    LuaAppearancePreviewMgr.m_DefaultWndTabIndex = 0
    LuaAppearancePreviewMgr.m_DefaultWndSection = 5
    LuaAppearancePreviewMgr.m_DefaultWndRow = 2
    CUIManager.ShowUI("AppearanceWnd")
end

function LuaAppearancePreviewMgr:ShowClosetTeamLeaderIconSubview()
    LuaAppearancePreviewMgr.m_DefaultWndTabIndex = 0
    LuaAppearancePreviewMgr.m_DefaultWndSection = 5
    LuaAppearancePreviewMgr.m_DefaultWndRow = 4
    CUIManager.ShowUI("AppearanceWnd")
end

function LuaAppearancePreviewMgr:ShowClosetTeamMemberBgSubview()
    LuaAppearancePreviewMgr.m_DefaultWndTabIndex = 0
    LuaAppearancePreviewMgr.m_DefaultWndSection = 5
    LuaAppearancePreviewMgr.m_DefaultWndRow = 5
    CUIManager.ShowUI("AppearanceWnd")
end

function LuaAppearancePreviewMgr:ShowFashionSuitSubview(suitId)
    LuaAppearancePreviewMgr.m_DefaultWndTabIndex = 1
    LuaAppearancePreviewMgr.m_DefaultWndSection = 0
    LuaAppearancePreviewMgr.m_DefaultWndRow = 0
    LuaAppearancePreviewMgr.m_PreviewFashionSuitId = suitId and suitId or 0
    CUIManager.ShowUI("AppearanceWnd")
end

function LuaAppearancePreviewMgr:ShowSkillAppearanceSubview()
    if self.m_EnableNewAppearanceWnd then
        LuaAppearancePreviewMgr.m_DefaultWndTabIndex = 0
        LuaAppearancePreviewMgr.m_DefaultWndSection = 3
        LuaAppearancePreviewMgr.m_DefaultWndRow = 2
        CUIManager.ShowUI("AppearanceWnd")
    else
        self:ShowAppearanceWnd()
    end
end

--功能已废弃
function LuaAppearancePreviewMgr:ShowFashionPresentWnd(id)
    -- local mainplayer = CClientMainPlayer.Inst
    -- if not mainplayer then return end
    -- self.m_RequestPresentFasionId = id
    -- local levelLimit = Fashion_Setting.Exists("ZengsongLevelLimit") and math.floor(tonumber(Fashion_Setting.GetData("ZengsongLevelLimit").Value)) or 0
    -- local xiuweiLimit = Fashion_Setting.Exists("ZengsongXiuweiLimit") and math.floor(tonumber(Fashion_Setting.GetData("ZengsongXiuweiLimit").Value)) or 0
    -- if not LuaAppearancePreviewMgr:HasPresentFashion() then
    --     g_MessageMgr:ShowMessage("Appearance_Fashion_Present_No_UnLock_Fashion")
    -- elseif mainplayer.MaxLevel<levelLimit then
    --     g_MessageMgr:ShowMessage("Appearance_Fashion_Present_Level_Not_Fit", levelLimit)
    -- elseif mainplayer.BasicProp.XiuWeiGrade<xiuweiLimit then
    --     g_MessageMgr:ShowMessage("Appearance_Fashion_Present_Xiuwei_Not_Fit", xiuweiLimit)
    -- else
    --     CUIManager.ShowUI("AppearanceFashionPresentWnd")
    -- end
end

function LuaAppearancePreviewMgr:ShowFashionUnlockWnd(id, type, progress, itemId, place, pos)
    self.m_RequestUnlockFasionId = id
    self.m_RequestUnlockFasionExtInfo = {}
    self.m_RequestUnlockFasionExtInfo.type = type and type or EnumAppearanceFashionUnlockType.eDefault
    self.m_RequestUnlockFasionExtInfo.progress = progress
    self.m_RequestUnlockFasionExtInfo.itemId = itemId
    self.m_RequestUnlockFasionExtInfo.place = place
    self.m_RequestUnlockFasionExtInfo.pos = pos
    --该窗口有两段动画，第二次调用Show方法时为了避免状态异常，先关闭再打开
    if CUIManager.IsLoaded("AppearanceFashionUnlockWnd") then
        CUIManager.CloseUI("AppearanceFashionUnlockWnd")
    end
    CUIManager.ShowUI("AppearanceFashionUnlockWnd")
end

function LuaAppearancePreviewMgr:ShowFashionEvaluationWnd(suitId)
    self.m_RequestEvaluateFasionSuitId = suitId
    CUIManager.ShowUI("AppearanceFashionEvaluationWnd")
end

function LuaAppearancePreviewMgr:GetFashionBuffFxByAppear(appear)
    local buffId = 0
    --根据格格的介绍，影灵怪物形态就走变身表了，特效可以挂在怪物身上，不会读时装的这个特效
    if appear.BodyFashionId > 0 and appear.HideBodyFashionEffect == 0 and
        Fashion_Fashion.Exists(appear.BodyFashionId) and appear.Gender ~= EnumGender.Monster then
            local fashionData = Fashion_Fashion.GetData(appear.BodyFashionId)
            buffId = appear.Gender == EnumGender.Female and fashionData.BuffIdFemale or fashionData.BuffId
    end
    local fxArray = buffId>0 and Buff_Buff.GetData(buffId).FX or nil
    return fxArray
end

function LuaAppearancePreviewMgr:GetFashionUnlockExpression()
    local mainplayer = CClientMainPlayer.Inst
    local expressionArray = Wardrobe_Setting.GetData().UnlockPermanentExpression
    return mainplayer and mainplayer.Gender==EnumGender.Male and expressionArray[0] or expressionArray[1]
end

function LuaAppearancePreviewMgr:IsFashionSuitEvaluated(suitId)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.WardrobeProp.FashionEvaluateSet:GetBit(suitId) or false
end

function LuaAppearancePreviewMgr:GetPreviewFashionAppear(fashionId, fashionPos)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return nil end
    local fakeAppear = mainplayer.AppearanceProp:Clone()
    if fashionPos == EnumAppearanceFashionPosition.eHead then
        fakeAppear.HeadFashionId = fashionId
        fakeAppear.HideHeadFashionEffect = 0
    elseif fashionPos == EnumAppearanceFashionPosition.eBody then
        fakeAppear.BodyFashionId = fashionId
        fakeAppear.HideBodyFashionEffect = 0
    elseif fashionPos == EnumAppearanceFashionPosition.eWeapon then
        fakeAppear.WeaponFashionId = fashionId
        fakeAppear.HideWeaponFashionEffect = 0 
    elseif fashionPos == EnumAppearanceFashionPosition.eBeiShi then
        fakeAppear.BackPendantFashionId = fashionId
        fakeAppear.HideBackPendantFashionEffect = 0
    end
    fakeAppear.YingLingState = EnumYingLingState.eDefault
    return fakeAppear
end

function LuaAppearancePreviewMgr:GetFashionSuitTransformAndExpressionInfo(headFashion, bodyFashion)
    local transformMonsterId = 0
    local transformExpressionId = 0
    local specialExpressionId = 0
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and IsTransformableFashionOpen() then
        if headFashion and bodyFashion and bodyFashion.Appearance == headFashion.Appearance then
            if mainplayer.Gender == EnumGender.Male then
                if  bodyFashion.TransformMonsterMale ~= 0 and bodyFashion.TransformMonsterMale == headFashion.TransformMonsterMale then
                    transformMonsterId = bodyFashion.TransformMonsterMale
                end
            else
                if bodyFashion.TransformMonsterFemale ~= 0 and bodyFashion.TransformMonsterFemale == headFashion.TransformMonsterFemale then
                    transformMonsterId = bodyFashion.TransformMonsterFemale
                end
            end
            if transformMonsterId > 0 and Fashion_TransformSpecialExpression.Exists(transformMonsterId) then
                transformExpressionId = Fashion_TransformSpecialExpression.GetData(transformMonsterId).ExpressionId
            end
        end
    end
    if headFashion and bodyFashion and bodyFashion.SpecialExpression>0 and bodyFashion.SpecialExpression == headFashion.SpecialExpression then
        specialExpressionId = bodyFashion.SpecialExpression
    end
    return transformMonsterId, transformExpressionId, specialExpressionId
end

function LuaAppearancePreviewMgr:GetPreviewFashionInfo(previewHeadFashionId, previewBodyFashionId, previewWeaponFashionId, previewBackPendantFashionId, isTransformOn)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end

    local headFashion = Fashion_Fashion.GetData(previewHeadFashionId)
    local bodyFashion = Fashion_Fashion.GetData(previewBodyFashionId)
    local backPendantFashion = Fashion_Fashion.GetData(previewBackPendantFashionId)
    local taben = EquipmentTemplate_Equip.GetData(previewWeaponFashionId)

    local appearanceData = mainplayer.AppearanceProp:Clone()
    appearanceData.HeadFashionId = headFashion and previewHeadFashionId or appearanceData.HeadFashionId
    appearanceData.BodyFashionId = bodyFashion and previewBodyFashionId or appearanceData.BodyFashionId
    appearanceData.BackPendantFashionId = backPendantFashion and previewBackPendantFashionId or appearanceData.BackPendantFashionId
    appearanceData.WeaponFashionId = taben and previewWeaponFashionId or appearanceData.WeaponFashionId
    local transformMonsterId, transformExpressionId, specialExpressionId = self:GetFashionSuitTransformAndExpressionInfo(headFashion, bodyFashion)
    if isTransformOn and transformMonsterId > 0 then
        appearanceData.ResourceId = transformMonsterId
    end
    appearanceData.HideHeadFashionEffect = headFashion and 0 or appearanceData.HideHeadFashionEffect
    appearanceData.HideBodyFashionEffect = bodyFashion and 0 or appearanceData.HideBodyFashionEffect
    appearanceData.HideBackPendantFashionEffect = backPendantFashion and 0 or appearanceData.HideBackPendantFashionEffect
    appearanceData.HideWeaponFashionEffect = taben and 0 or appearanceData.HideWeaponFashionEffect
    appearanceData.HideHelmetEffect = headFashion and 0 or appearanceData.HideHelmetEffect --预览头部时装时强制显示头盔
    appearanceData.HideBackPendant = backPendantFashion and 0 or appearanceData.HideBackPendant --预览背饰时强制显示背饰

    return appearanceData
end

--获取道具对应的预览套装，如果有预览套装，返回套装id，否则返回0
function LuaAppearancePreviewMgr:GetPreviewFashonSuitByItem(itemTemplateId)
    local dict = Wardrobe_Setting.GetData().SuitBoxNewWardrobe
    if CommonDefs.DictContains_LuaCall(dict, itemTemplateId) then
        return CommonDefs.DictGetValue_LuaCall(dict, itemTemplateId)
    else
        return 0
    end
end

function LuaAppearancePreviewMgr:RequestTakeOnFashion_Permanent(id)
    Gac2Gas.RequestTakeOnFashion_Permanent(id)
end
function LuaAppearancePreviewMgr:RequestTakeOffFashion_Permanent(id)
    Gac2Gas.RequestTakeOffFashion_Permanent(id)
end
function LuaAppearancePreviewMgr:RequestDiscardFashion_Permanent(id)
    Gac2Gas.RequestDiscardFashion_Permanent(id)
end
function LuaAppearancePreviewMgr:RequestUnLockFashion_Permanent(id)
    Gac2Gas.RequestUnLockFashion_Permanent(id)
end
function LuaAppearancePreviewMgr:ConfirmUnLockFashionByItem_Permanent(id, itemId, place, pos)
    Gac2Gas.ConfirmUnLockFashionByItem_Permanent(id, itemId, place, pos)
end
function LuaAppearancePreviewMgr:RequestRenewFashion_Permanent(id, days)
    Gac2Gas.RequestRenewFashion_Permanent(id, days)
end
function LuaAppearancePreviewMgr:RequestTakeOnFashionSuit_Permanent(suitId)
    Gac2Gas.RequestTakeOnFashionSuit_Permanent(suitId)
end
function LuaAppearancePreviewMgr:RequestTakeOffFashionSuit_Permanent(suitId)
    Gac2Gas.RequestTakeOffFashionSuit_Permanent(suitId)
end
function LuaAppearancePreviewMgr:RequestEvaluateFashionSuit_Permanent(suitId, manyidu, banxing, peise, xingjiabi, bianshen, special, others)
    Gac2Gas.RequestEvaluateFashionSuit_Permanent(suitId, manyidu, banxing, peise, xingjiabi, bianshen, special, others)
end
-------------------时装——所有下面的内容-------------------
LuaAppearancePreviewMgr.m_FashionQualityTbl = nil
function LuaAppearancePreviewMgr:InitFahsionQualityTbl()
    if self.m_FashionQualityTbl==nil then
        self.m_FashionQualityTbl = {
            {name=LocalString.GetString("优良"), border="common_itemcell_border", color= NGUIText.ParseColor24("cae5ff", 0)},
            {name=LocalString.GetString("精巧"), border="frame_blue_dark", color=NGUIText.ParseColor24("558dff", 0)},
            {name=LocalString.GetString("佳品"), border="frame_red_dark", color=NGUIText.ParseColor24("da3f4a", 0)},
            {name=LocalString.GetString("传世"), border="frame_purple_light", color=NGUIText.ParseColor24("b700b7", 0)},
            {name=LocalString.GetString("典藏"), border="frame_purple_dark", color=NGUIText.ParseColor24("b700b7", 0)},
        }
    end
end

function LuaAppearancePreviewMgr:GetFashionQualityName(quality)
    self:InitFahsionQualityTbl()
    return self.m_FashionQualityTbl[quality] and self.m_FashionQualityTbl[quality].name or self.m_FashionQualityTbl[1].name
end

function LuaAppearancePreviewMgr:GetFashionQualityBorder(quality)
    self:InitFahsionQualityTbl()
    return self.m_FashionQualityTbl[quality] and self.m_FashionQualityTbl[quality].border or self.m_FashionQualityTbl[1].border
end

function LuaAppearancePreviewMgr:GetFashionQualityColor(quality)
    self:InitFahsionQualityTbl()
    return self.m_FashionQualityTbl[quality] and self.m_FashionQualityTbl[quality].color or self.m_FashionQualityTbl[1].color
end

--copy from LuaShopMallFashionPreviewWnd
function LuaAppearancePreviewMgr:PlayFashionVideo(video)
    if not CCPlayerCtrl.IsSupportted() then
        g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
        return
    end
    local url = CommonDefs.IS_PUB_RELEASE and "http://l10.gph.netease.com/fashionpreviewvideo/" or "http://l10-patch.leihuo.netease.com/fashionpreviewvideo/"
    CWinCGMgr.Inst.m_CGUrl = url .. video
    CUIManager.ShowUI(CUIResources.WinCGWnd)
end

-------------------时装赠送-------------------
function LuaAppearancePreviewMgr:GetPresentFashionInfoSortNames()
    return {LocalString.GetString("按剩余数量"), LocalString.GetString("按部位"), LocalString.GetString("按品质")}
end       

function LuaAppearancePreviewMgr:HasPresentFashion()
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return false end
    local exist = false
    CommonDefs.DictIterate(mainplayer.WardrobeProp.FashionMainInfos, DelegateFactory.Action_object_object(function (key, info)
        if info.IsUnLock==1 then
            exist = true
        end
    end))
    return exist
end

function LuaAppearancePreviewMgr:GetAllPresentFashionInfo(sortIndex)
    local ret = {}
    local countLimit = Wardrobe_Setting.GetData().PresentCountLimit
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return ret end
    CommonDefs.DictIterate(mainplayer.WardrobeProp.FashionMainInfos, DelegateFactory.Action_object_object(function (key, info)
        if info.IsUnLock==1 then
            local data = Fashion_Fashion.GetData(key)
            local presentCount = 0
            if CommonDefs.DictContains_LuaCall(mainplayer.WardrobeProp.FashionPresentInfos, key) then
                presentCount = CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionPresentInfos, key).PlayerInfos.Count
            end
            table.insert(ret, {id=key, name=self:FilterFashionName(data.Name), position=data.Position, icon=self:GetFashionIconById(data.Position,key), quality=data.Quality, remain=math.max(0, countLimit-presentCount)})
        end
    end))

    table.sort(ret, function(data1, data2)
        if sortIndex then
            if sortIndex==1 then
                if data1.remain~=data2.remain then
                    return data1.remain>data2.remain
                end
            elseif sortIndex==2 then
                if data1.position~=data2.position then
                    return data1.position < data2.position
                end
            elseif sortIndex==3 then
                if data1.quality~=data2.quality then
                    return data1.quality>data2.quality
                end
            end
        end
        return data1.id>data2.id
    end)
    return ret
end

function LuaAppearancePreviewMgr:GetPresentFahsionPlayerInfo(id)
    local ret = {}
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return ret end
    if CommonDefs.DictContains_LuaCall(mainplayer.WardrobeProp.FashionPresentInfos, id) then
        local playerInfos = CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionPresentInfos, id).PlayerInfos
        CommonDefs.DictIterate(playerInfos, DelegateFactory.Action_object_object(function (key, info)
            table.insert(ret, {id=key, name=info.Name, icon=CUICommonDef.GetPortraitName(info.Class, info.Gender, -1)})
        end))
    end
    return ret
end

function LuaAppearancePreviewMgr:RequestPresentFashion_Permanent(fashionId, targetId)
    Gac2Gas.RequestPresentFashion_Permanent(fashionId, targetId)
end
-------------------翅膀-------------------
function LuaAppearancePreviewMgr:GetAllWingInfo(excludeNotOwned)
    local ret = {}
    Wing_Wing.Foreach(function (key, data)
        if not excludeNotOwned or self:MainPlayerHasWing(key) then
            table.insert(ret,{id=key, name=data.Name, colorName=data.ColorName, level=data.Level, icon=data.Icon, femaleIcon=data.FemaleIcon, condition=data.Condition})
        end
    end)
    --按解锁难易从低到高排序
    table.sort(ret, function(data1, data2)
        local owned1 = self:MainPlayerHasWing(data1.id)
        local owned2 = self:MainPlayerHasWing(data2.id)
        if owned1~=owned2 then --已解锁放前面
            return owned1
        end
        if data1.level~=data2.level then
            return data1.level<data2.level
        else
            return data1.id<data2.id
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasWing(wingId)
    return CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.ItemProp.Wing, wingId) or false
end

function LuaAppearancePreviewMgr:IsCurrentInUseWing(wingId)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.Wing == wingId or false
end

function LuaAppearancePreviewMgr:GetCurrentInUseWing()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.Wing or 0
end

function LuaAppearancePreviewMgr:IsShowWing()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideWing==0 or false
end

function LuaAppearancePreviewMgr:ChangeWingVisibility(bShow)
    Gac2Gas.SetPropertyAppearanceSettingInfo(EnumPropertyAppearanceSettingBit_lua.HideWing, bShow and 0 or 1)
    g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Wing_Switch_On" or "Appearance_Preview_Wing_Switch_Off")
end

function LuaAppearancePreviewMgr:RequestSetWing(wingId)
    Gac2Gas.PutOnWing(wingId)
    if wingId>0 then
        g_MessageMgr:ShowMessage("Appearance_Preview_Apply_Success")
    else
        g_MessageMgr:ShowMessage("Appearance_Preview_Remove_Success")
    end
end

-------------------光效-强化套装-------------------
function LuaAppearancePreviewMgr:GetAllIntensifySuitInfo(excludeNotOwned)
    local ret = {}
    local get_display_name = function(text, suitFX)
        if suitFX and suitFX.Length>=3 then
            return SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(Color(suitFX[0] / 255, suitFX[1] / 255, suitFX[2] / 255)), text)
        else
            return text
        end
    end
    EquipIntensify_Suit.Foreach(function (key, data)
        if not excludeNotOwned or self:MainPlayerHasIntensitySuit(key) then
            if not (data.ColorName==nil or data.ColorName=="") then
                table.insert(ret,{fxId =key, designId=key, name=get_display_name(data.ColorName, data.SuitFX), icon=data.ColorIcon, condition=self:GetIntensifySuitCondition(data)})
            end
            if not (data.SecondColorName==nil or data.SecondColorName=="") then
                table.insert(ret,{fxId =key+10000, designId=key, name=get_display_name(data.SecondColorName, data.SecondSuitFX), icon=data.SecondColorIcon, condition=self:GetIntensifySuitCondition(data)})
            end
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasIntensitySuit(fxId)
    return CClientMainPlayer.Inst and CommonDefs.ListContains_LuaCall(CClientMainPlayer.Inst.IntensifySuitList, math.floor(fxId%10000)) or false
end

function LuaAppearancePreviewMgr:IsCurrentInUseIntensitySuit(fxId)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId==fxId or false
end

function LuaAppearancePreviewMgr:GetIntensifySuitCondition(equipIntensifySuitDesignData)
    if equipIntensifySuitDesignData == nil then return nil end
    if equipIntensifySuitDesignData.ID < 9 then
        local perfection = equipIntensifySuitDesignData.Perfection
        if perfection % 2 == 0 then
            return SafeStringFormat3(LocalString.GetString("穿戴%d件强化%d级，完美度%d星装备"), equipIntensifySuitDesignData.EquipNum, equipIntensifySuitDesignData.IntensifyLevel, math.floor(perfection / 2))
        else
            return SafeStringFormat3(LocalString.GetString("穿戴%d件强化%d级，完美度%d星半装备"), equipIntensifySuitDesignData.EquipNum, equipIntensifySuitDesignData.IntensifyLevel, math.floor(perfection / 2))
        end
    else
        return SafeStringFormat3(LocalString.GetString("全身均强化20级且基础提升≥%d%%"), equipIntensifySuitDesignData.IntensifyNumber)
    end
end

function LuaAppearancePreviewMgr:IsShowIntensitySuit()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideSuitIdToOtherPlayer==0 or false
end

function LuaAppearancePreviewMgr:GetCurrentInUseIntensitySuit()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId or 0
end

function LuaAppearancePreviewMgr:ChangeIntensitySuitVisibility(bShow)
    Gac2Gas.SetPropertyAppearanceSettingInfo(EnumPropertyAppearanceSettingBit_lua.HideSuitIdToOtherPlayer, bShow and 0 or 1)
    g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Itensify_Suit_Switch_On" or "Appearance_Preview_Itensify_Suit_Switch_Off")
end

function LuaAppearancePreviewMgr:RequestSetIntensitySuit(suitId)
    Gac2Gas.PutOnSuitFx(suitId)
    if suitId>0 then
        g_MessageMgr:ShowMessage("Appearance_Preview_Apply_Success")
    else
        g_MessageMgr:ShowMessage("Appearance_Preview_Remove_Success")
    end
end

-------------------光效-石之灵-------------------
function LuaAppearancePreviewMgr:GetAllGemGroupInfo() --对于主角只会最多有一种
    local ret = {}
    if self:MainPlayerHasGemGroup() then
        --伪造一个fake id用于选中状态处理
        table.insert(ret,{id=1, name=LocalString.GetString("石之灵光效"), icon=self.GemGroupIcon, condition=LocalString.GetString("武器和盾牌镶嵌石之灵达到一定等级")})
    end
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasGemGroup()
    if CClientMainPlayer.Inst then
        local appearance = CClientMainPlayer.Inst.AppearanceProp
        for i=0,appearance.EquipmentGemFx.Length-1 do
            local gemLevel = appearance.EquipmentGemFx[i]
            if gemLevel > 0 then
                local equipId = appearance.Equipment[i]
                local equip = EquipmentTemplate_Equip.GetData(equipId)
                if equip then
                    if equip.GemLow ~= 0 and gemLevel == 1 then
                        return true
                    elseif equip.GemMid ~= 0 and gemLevel == 2 then
                        return true
                    elseif equip.GemHigh ~= 0 and gemLevel == 3 then
                        return true
                    end
                end
            end
        end
    end
    return false
end

function LuaAppearancePreviewMgr:IsShowGemGroup()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer==0 or false
end

function LuaAppearancePreviewMgr:GetCurrentInUseGemGroup()
    return  self:MainPlayerHasGemGroup() and 1 or 0 -- fake id 对应GetAllGemGroupInfo中的信息
end

function LuaAppearancePreviewMgr:ChangeGemGroupVisibility(bShow)
    Gac2Gas.HideGemGroupFx(not bShow)
    g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Gem_Group_Switch_On" or "Appearance_Preview_Gem_Group_Switch_Off")
end

-------------------光效-技能-------------------

function LuaAppearancePreviewMgr:GetAllSkillAppearInfo()
    local info = {}
    local appInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.SkillAppearInfo or nil
    if appInfo then
        CommonDefs.DictIterate(appInfo, DelegateFactory.Action_object_object(function(key,val)
            local id = CommonDefs.Convert_ToUInt32(key)
            local expiredTime = CommonDefs.Convert_ToUInt32(val)
            -- 没过期才加入
            if CServerTimeMgr.Inst.timeStamp < expiredTime then
                local data = {}
                data.id = id
                data.expiredTime = expiredTime
                local designData = SkillAppearance_SkillAppearance.GetData(id)
                data.icon = designData.Icon
                data.name = designData.Name
                data.condition = SafeStringFormat3(LocalString.GetString("%s\n有效期至：%s"), designData.Desc,
                    ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(expiredTime), "yy-MM-dd HH:mm"))
                table.insert(info, data)
            end
        end))
    end
    return info
end

function LuaAppearancePreviewMgr:MainPlayerHasSkillAppear(id)
    return CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.SkillAppearInfo, id) or false
end

function LuaAppearancePreviewMgr:IsCurrentInUseSkillAppear(id)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.SkillAppearanceIdIgnoreSwitch==id or false
end

function LuaAppearancePreviewMgr:GetCurrentSkillAppearId()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.SkillAppearanceIdIgnoreSwitch or 0
end

function LuaAppearancePreviewMgr:IsShowSkillAppear()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideSkillAppearance==0 or false
end

function LuaAppearancePreviewMgr:ChangeSkillAppearVisibility(bShow)
    Gac2Gas.SetPropertyAppearanceSettingInfo(EnumPropertyAppearanceSettingBit_lua.HideSkillAppearance, bShow and 0 or 1)
    g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Skill_Appearance_Switch_On" or "Appearance_Preview_Skill_Appearance_Switch_Off")
end

function LuaAppearancePreviewMgr:RequestSetSkillAppear(skillAppearanceId)
    Gac2Gas.RequestSetSkillAppearance(skillAppearanceId)
    if skillAppearanceId>0 then
        g_MessageMgr:ShowMessage("Appearance_Preview_Apply_Success")
    else
        g_MessageMgr:ShowMessage("Appearance_Preview_Remove_Success")
    end
end

-------------------法宝-------------------
function LuaAppearancePreviewMgr:GetTalismanGrade(id)
    return math.floor((id % 100) / 10)
end
--逻辑搬运自 LuaTalismanView 比较绕，做了一些整理
function LuaAppearancePreviewMgr:GetAllTalismanAppearInfo(excludeNotOwned)
    LuaAppearancePreviewMgr.InitTalismanData()
    local ret = {}
    local list = LuaTalismanMgr:GetMainPlayerTalismanAppearanceList()
    local talismanFx2Id = {}
    local uniqueXianJiaTalismanGradeTbl = {}
    if list then
        CommonDefs.ListIterate(list, DelegateFactory.Action_object(function(___value)
            local talismanId = tonumber(___value) --TalismanID为特殊法宝特效时，对应策划表ItemGet_TalismanFx/Talisman_XianJiaAppearanceLevel中的TalismanID
            local fxId = CTalismanMgr.Inst:FindTalismanFx(talismanId % 100)
            talismanFx2Id[fxId] = talismanId
            local grade = self:GetTalismanGrade(talismanId)
            --1-4阶法宝合并显示
            if grade>=1 and grade<=4 then
                if uniqueXianJiaTalismanGradeTbl[grade] then
                    return
                end
                uniqueXianJiaTalismanGradeTbl[grade] = true
            end
            local fxOrderDesign = Talisman_FXOrder.GetData(fxId)
            local fxOrder = fxOrderDesign and fxOrderDesign.Order or 0
            local fxName = fxOrderDesign and CChatLinkMgr.TranslateToNGUIText(fxOrderDesign.Description,false) or ""
            local fxIcon = LuaAppearancePreviewMgr.GetTalismanAppearanceIcon(talismanId, fxId)
            table.insert(ret, {id=talismanId, fxId=fxId, order=fxOrder, name=fxName, icon=fxIcon, condition=""})
        end))

        --按order从小到大排序
        table.sort(ret, function(data1, data2)
            if data1.order~=data2.order then
                return data1.order<data2.order
            else
                return data1.id<data2.id
            end
        end)
    end
    
    if excludeNotOwned then return ret end

   

    local unlockedTbl = {}
    for i=1,4 do --grade
        local talismanId = i*10+1 --1-4阶只显示1个法宝对应的外观
        local fxId = CTalismanMgr.Inst:FindTalismanFx(talismanId % 100)
        if talismanFx2Id[fxId]==nil then
            talismanFx2Id[fxId] = talismanId
            local fxOrderDesign = Talisman_FXOrder.GetData(fxId)
            local fxOrder = fxOrderDesign and fxOrderDesign.Order or 0
            local fxName = fxOrderDesign and CChatLinkMgr.TranslateToNGUIText(fxOrderDesign.Description,false) or ""
            local fxIcon = LuaAppearancePreviewMgr.GetTalismanAppearanceIcon(talismanId, fxId)
            table.insert(unlockedTbl, {id=talismanId, fxId=fxId, order=fxOrder, name=fxName, icon=fxIcon, condition=""})
        end
    end

     -- hmt和越南不显示特殊法宝特效
     if not CommonDefs.IS_HMT_CLIENT and not CommonDefs.IS_VN_CLIENT then

        ItemGet_TalismanFX.Foreach(function (key, data)
            if data.TalismanID < 100 then
                --当前还未解锁，不考虑法宝阶数全部塞入，这个和原来的外观界面逻辑有点区别
                if not LuaTalismanMgr:CheckContain(data.TalismanID) then
                    local fxId = CTalismanMgr.Inst:FindTalismanFx(data.TalismanID % 100)
                    local fxOrderDesign = Talisman_FXOrder.GetData(key)
                    local fxOrder = fxOrderDesign and fxOrderDesign.Order or 0
                    local fxName = fxOrderDesign and CChatLinkMgr.TranslateToNGUIText(fxOrderDesign.Description,false) or ""
                    local fxIcon = LuaAppearancePreviewMgr.GetTalismanAppearanceIcon(data.TalismanID, fxId)
                    table.insert(unlockedTbl, {id=data.TalismanID, fxId=fxId, order=fxOrder, name=fxName, icon=fxIcon, condition=""})
                end
            end
        end)
    end

    --按order从小到大排序
    table.sort(unlockedTbl, function(data1, data2)
        if data1.order~=data2.order then
            return data1.order<data2.order
        else
            return data1.id<data2.id
        end
    end)

    for __,data in pairs(unlockedTbl) do
        table.insert(ret, data)
    end
    return ret
end

function LuaAppearancePreviewMgr:GetTalismanSpecialAppearFxId(talismanId)
    return  CTalismanMgr.Inst:FindTalismanFx(talismanId)
end

function LuaAppearancePreviewMgr:GetTalismanSpecialAppearInfo(id, fxId)
    local ret = {}
    local data = ItemGet_TalismanFX.GetData(fxId)
    if data==nil then
        --1-4阶的仙家法宝外观
        local grade = self:GetTalismanGrade(id)
        local talismanFx2Id = {}
        for j=1,6 do
            local talismanId = grade*10+j
            local fxId = CTalismanMgr.Inst:FindTalismanFx(talismanId % 100)
            if talismanFx2Id[fxId]==nil then
                talismanFx2Id[fxId] = talismanId
                table.insert(ret, {id=talismanId, fxId=fxId, name=CChatLinkMgr.TranslateToNGUIText(Talisman_FXOrder.GetData(fxId).Description, false)})
            end
        end
    else
        local talismanType = data.TalismanID % 100
        ItemGet_TalismanFX.Foreach(function (key, value)
            if value.TalismanID % 100 == talismanType then
                table.insert(ret, {id=value.TalismanID, fxId=key, name=CChatLinkMgr.TranslateToNGUIText(Talisman_FXOrder.GetData(fxId).Description, false)})
            end
        end)
    end
  
    table.sort(ret,function(a,b)
      return a.id < b.id
    end)
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasTalismanAppear(talismanId)
    return  LuaTalismanMgr:CheckContain(talismanId)
end

function LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(talismanId)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.TalismanAppearance==talismanId or false
end

function LuaAppearancePreviewMgr:ShowGetTalismanInfo(go, talismanId)     
    if self:GetTalismanGrade(talismanId)<=4 then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(Wardrobe_Setting.GetData().XianJiaItemGetID, true, go.transform)
    else
        local fxId = LuaAppearancePreviewMgr:GetTalismanSpecialAppearFxId(talismanId)
        self:ShowTalismanFxExchangeWnd(fxId)
    end
end

function LuaAppearancePreviewMgr:ShowTalismanFxExchangeWnd(talismanFxId)
    if talismanFxId then
        local data = ItemGet_TalismanFX.GetData(talismanFxId)
        if data then
            CLuaTalismanFxExchangeWnd.s_TalismanFxId = talismanFxId --这地方不合理，先按旧代码处理
        end
    end
    CUIManager.ShowUI(CLuaUIResources.TalismanFxExchangeWnd)
end

function LuaAppearancePreviewMgr:IsShowTalismanAppear()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.HideTalismanAppearance==0 or false
end

function LuaAppearancePreviewMgr:GetCurrentInUseTalismanAppear()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.TalismanAppearance or 0
end

function LuaAppearancePreviewMgr:ChangeTalismanAppearVisibility(bShow)
    Gac2Gas.SetPropertyAppearanceSettingInfo(EnumPropertyAppearanceSettingBit_lua.HideTalismanAppearance, bShow and 0 or 1)
    g_MessageMgr:ShowMessage(bShow and "Appearance_Preview_Talisman_Appearance_Switch_On" or "Appearance_Preview_Talisman_Appearance_Switch_Off")
end

function LuaAppearancePreviewMgr:SelectTalismanAppearance(talismanId)
    Gac2Gas.SelectTalismanAppearance(math.ceil(talismanId / 100), math.floor((talismanId % 100) / 10), talismanId % 10)
    if talismanId>0 then
        g_MessageMgr:ShowMessage("Appearance_Preview_Apply_Success")
    else
        g_MessageMgr:ShowMessage("Appearance_Preview_Remove_Success")
    end
end
-------------------染发-------------------
function LuaAppearancePreviewMgr:GetAllRanFaJiInfo(excludeNotOwned)
    local ret = {}
    RanFaJi_RanFaJi.Foreach(function (key, data)
        if (not excludeNotOwned or self:MainPlayerHasRanFaJi(key)) then
            table.insert(ret,{id=key, name=data.ColorName..LocalString.GetString("染发剂"), desc=data.FormulaDescription, icon=data.Icon, colorSystem=data.ColorSystem, itemId=data.RanFaJiItemID, fashionability=data.Fashionability})
        end
    end)
    --优先解锁显示前面，其次order小的显示前面
    table.sort(ret, function(data1, data2)
        local hasData1 = self:MainPlayerHasRanFaJi(data1.id)
        local hasData2 = self:MainPlayerHasRanFaJi(data2.id)
        if hasData1~=hasData2 then return hasData1 end
        local hasRecipe1 = self:MainPlayerHasRanFaJiRecipe(data1.id)
        local hasRecipe2 = self:MainPlayerHasRanFaJiRecipe(data2.id)
        if hasRecipe1~=hasRecipe2 then return hasRecipe1 end
        if data1.colorSystem~=data2.colorSystem then
            return data1.colorSystem<data2.colorSystem
        else
            return data1.id<data2.id
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasRanFaJi(id)
    return CLuaRanFaMgr.m_OwnRanFaJiTable and CLuaRanFaMgr.m_OwnRanFaJiTable[id]~=nil or false
end

function LuaAppearancePreviewMgr:MainPlayerHasRanFaJiRecipe(id)
    return CLuaRanFaMgr.m_RecipeTable and CLuaRanFaMgr.m_RecipeTable[id]~=nil or false
end   

function LuaAppearancePreviewMgr:IsCurrentInUseRanFaJi(id)
    return CLuaRanFaMgr.m_MyRanFaJiId and CLuaRanFaMgr.m_MyRanFaJiId == id or false
end

function LuaAppearancePreviewMgr:GetCurrentInUseRanFaJi()
    return CLuaRanFaMgr.m_MyRanFaJiId or 0
end

function LuaAppearancePreviewMgr:RequestTakeOnRanFaJi(id)
    CLuaRanFaMgr:TakeOnAdvancedHairColor(id)
end

function LuaAppearancePreviewMgr:RequestTakeOffRanFaJi(id)
    CLuaRanFaMgr:TakeOffAdvancedHairColor(id)
end

function LuaAppearancePreviewMgr:RequestOpenRanFaJiRecipe()
    CLuaRanFaMgr:RequestSyncRanFaJiPlayData(EnumSyncRanFaJiType.eOpenMainWndRecipeTab)
end

function LuaAppearancePreviewMgr:RequestOpenRanFaJiLibrary()
    CLuaRanFaMgr:RequestSyncRanFaJiPlayData(EnumSyncRanFaJiType.eOpenMainWndLibraryTab)
end
-------------------伞面-------------------
--伞面的功能主要是从ExpressionEquipSelectWnd搬运修改而来
LuaAppearancePreviewMgr.m_UmbrellaExpressionGroupId = 1
LuaAppearancePreviewMgr.m_DefaultUmbrellaDesignInfo = nil
function LuaAppearancePreviewMgr:GetAllUmbrellaInfo(excludeNotOwned)
    local ret = {}
    local defaultUmbrella = self:GetDefaultUmbrellaDesignInfo()
    Expression_Appearance.Foreach(function (key, data)
        -- ShowStatus==3表示已下架
        if data.Group == self.m_UmbrellaExpressionGroupId and (not excludeNotOwned or self:MainPlayerHasUmbrella(key) or data.id==defaultUmbrella.ID) and data.Status==0 then
            --非下架状态 或者 已下架但仍拥有未过期
            local info = self:GetMainPlayerUmbrellaInfo(key)
            if data.ShowStatus~=3 or (info~=nil and info.IsExpired==0) or data.id==defaultUmbrella.ID then
                table.insert(ret,{id=key, name=data.Name, icon=data.Icon2, femaleIcon=data.Icon, order=data.Order, itemGetId=data.ItemId, expressionId=data.PreviewDefine, availableTime = data.AvailableTime, fashionability = data.Fashionability})
            end
        end
    end)
    --排序优先级：默认伞->永久伞->限时有效伞->已过期伞->未解锁伞，满足同样条件的伞再按order从小到大排序
    table.sort(ret, function(data1, data2)
        local info1 = self:GetMainPlayerUmbrellaInfo(data1.id)
        local info2 = self:GetMainPlayerUmbrellaInfo(data2.id)

        if data1.id==defaultUmbrella.ID then
            return true --默认的放第一个
        elseif data2.id==defaultUmbrella.ID then
            return false
        end
    
        if info1~=nil or info2~=nil then
            if info1~=nil and info2~=nil then
                if info1.ExpireTime==0 or info2.ExpireTime==0 then 
                    if info1.ExpireTime~=info2.ExpireTime then --仅有一个为永久伞
                        return info1.ExpireTime==0
                    end
                elseif info1.IsExpired~=info2.IsExpired then --仅有一个为已过期伞
                    return info1.IsExpired~=1
                end
            else
                return info1~=nil
            end
        end
        --data1 data2满足同样的条件，按order排序，order相同时再比较id
        if data1.order~=data2.order then
            return data1.order<data2.order
        else
            return data1.id<data2.id
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:GetDefaultUmbrellaDesignInfo()
    if self.m_DefaultUmbrellaDesignInfo == nil then
        self.m_DefaultUmbrellaDesignInfo = CExpressionAppearanceMgr.Inst:GetDefaultAppearance(self.m_UmbrellaExpressionGroupId)
    end
    return self.m_DefaultUmbrellaDesignInfo
end

function LuaAppearancePreviewMgr:GetUmbrellaConditionText(id)
    local info = LuaAppearancePreviewMgr:GetMainPlayerUmbrellaInfo(id)
    local defaultUmbrella = self:GetDefaultUmbrellaDesignInfo()
    if id==defaultUmbrella.ID then
        return "[c][FFFFFF]"..LocalString.GetString("永久获得").."[-][/c]"
    elseif info==nil then
        return "[c][FF0000]"..LocalString.GetString("未获得").."[-][/c]"
    elseif info.IsExpired==1 then
        return "[c][FF0000]"..LocalString.GetString("已过期").."[-][/c]"
    elseif Expression_Appearance.GetData(id).AvailableTime==0 then
        return "[c][FFFFFF]"..LocalString.GetString("永久获得").."[-][/c]"
    else
        local dt =  CServerTimeMgr.ConvertTimeStampToZone8Time(info.ExpireTime)
	    return "[c][FFFFFF]"..ToStringWrap(dt, LocalString.GetString("有效期至 yy-MM-dd HH:mm")).."[-][/c]"
    end
end

function LuaAppearancePreviewMgr:MainPlayerHasUmbrella(id)
    local defaultUmbrella = self:GetDefaultUmbrellaDesignInfo()
    if id==defaultUmbrella.ID then
        return true
    end
    local mainplayer = CClientMainPlayer.Inst
    local appearanceData = mainplayer and  mainplayer.PlayProp.ExpressionAppearanceData or nil
    if appearanceData and CommonDefs.DictContains_LuaCall(appearanceData, self.m_UmbrellaExpressionGroupId) then
        local appearanceSet = CommonDefs.DictGetValue_LuaCall(appearanceData, self.m_UmbrellaExpressionGroupId)
        return CommonDefs.DictContains_LuaCall(appearanceSet.Appearances, id)
    end
    return false
end

function LuaAppearancePreviewMgr:IsUmbrellaExpired(id)
    local defaultUmbrella = self:GetDefaultUmbrellaDesignInfo()
    if id==defaultUmbrella.ID then
        return false
    end
    local info = self:GetMainPlayerUmbrellaInfo(id)
    return info and info.IsExpired==1 or false
end

function LuaAppearancePreviewMgr:IsCurrentInUseUmbrella(id)
    return self:GetCurrentInUseUmbrella() == id
end

--仿照CExpressionAppearanceMgr.Inst.GetMainPlayerSelectedAppearance，相关逻辑非常的绕
function LuaAppearancePreviewMgr:GetCurrentInUseUmbrella()
    local defaultUmbrella = self:GetDefaultUmbrellaDesignInfo()
    local mainplayer = CClientMainPlayer.Inst
    local appearanceData = mainplayer and  mainplayer.PlayProp.ExpressionAppearanceData or nil
    if appearanceData and CommonDefs.DictContains_LuaCall(appearanceData, self.m_UmbrellaExpressionGroupId) then
        local appearanceSet = CommonDefs.DictGetValue_LuaCall(appearanceData, self.m_UmbrellaExpressionGroupId)
        return appearanceSet.CurrAppearance==0 and defaultUmbrella.ID or appearanceSet.CurrAppearance
    end
    return defaultUmbrella.ID
end

function LuaAppearancePreviewMgr:GetMainPlayerUmbrellaInfo(id)
    local mainplayer = CClientMainPlayer.Inst
    local appearanceData = mainplayer and  mainplayer.PlayProp.ExpressionAppearanceData or nil
    if appearanceData and CommonDefs.DictContains_LuaCall(appearanceData, self.m_UmbrellaExpressionGroupId) then
        local appearanceSet = CommonDefs.DictGetValue_LuaCall(appearanceData, self.m_UmbrellaExpressionGroupId)
        if CommonDefs.DictContains_LuaCall(appearanceSet.Appearances, id) then
            return CommonDefs.DictGetValue_LuaCall(appearanceSet.Appearances, id)
        end
    end
    return nil
end

function LuaAppearancePreviewMgr:RequestSetUmbrella(id)
    Gac2Gas.SetExpressionAppearance(self.m_UmbrellaExpressionGroupId, id)
end

function LuaAppearancePreviewMgr:RequestDiscardUmbrella(id)
    Gac2Gas.RequestDiscardExpressionAppearance(self.m_UmbrellaExpressionGroupId, id)
end

function LuaAppearancePreviewMgr:ShowUmbrellas(expressionGroupId)
    if self.m_EnableNewAppearanceWnd and expressionGroupId==self.m_UmbrellaExpressionGroupId then
        LuaAppearancePreviewMgr.m_DefaultWndTabIndex = 1
        LuaAppearancePreviewMgr.m_DefaultWndSection = 4
        LuaAppearancePreviewMgr.m_DefaultWndRow = 2
        CUIManager.ShowUI("AppearanceWnd")

    else
        CExpressionAppearanceMgr.AppearanceGroup = expressionGroupId
        CUIManager.ShowUI(CUIResources.ExpressionEquipSelectWnd)
    end
end

-------------------头像框-------------------
function LuaAppearancePreviewMgr:GetAllProfileFrameInfo(excludeNotOwned)
    local ret = {}
    ExpressionHead_ProfileFrame.Foreach(function (key, data)
        if (not excludeNotOwned or self:MainPlayerHasProfileFrame(key)) and data.Status==0 then
            table.insert(ret,{id=key, name=data.Descrition, icon=data.ResName, order=data.StickerOrder, itemGetId=data.ItemGetId, fashionability=data.Fashionability, isPermanent=data.Permanent>0})
        end
    end)
    --排序优先级：永久头像框->限时头像框->已过期头像框->未解锁头像框，满足同样条件的头像框再按order从小到大排序
    table.sort(ret, function(data1, data2)
        local hasData1 = self:MainPlayerHasProfileFrame(data1.id)
        local hasData2 = self:MainPlayerHasProfileFrame(data2.id)
        local expiredTime1 = LuaExpressionMgr:GetProfieFrameExpireTime(data1.id)
        local expiredTime2 = LuaExpressionMgr:GetProfieFrameExpireTime(data2.id)
        local dataExpired1 = self:IsProfileFrameExpired(data1.id)
        local dataExpired2 = self:IsProfileFrameExpired(data2.id)
        if hasData1 or hasData2 then
            if hasData1 and hasData2 then
                if expiredTime1<0 or expiredTime2<0 then
                    if expiredTime1~=expiredTime2 then --仅有一个为永久头像框
                        return expiredTime1<0
                    end
                elseif dataExpired1~=dataExpired2 then --仅有一个为已过期头像框
                    return not dataExpired1
                end
            else
                return hasData1
            end        
        end
        --data1 data2满足同样的条件，按order排序，order相同时再比较id
        if data1.order~=data2.order then
            return data1.order<data2.order
        else
            return data1.id<data2.id
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(frameId)
    return LuaExpressionMgr:GetProfieFrameExpireTime(frameId)~=0
end

function LuaAppearancePreviewMgr:IsProfileFrameExpired(frameId)
    local time = LuaExpressionMgr:GetProfieFrameExpireTime(frameId)
    return time>0 and time<CServerTimeMgr.Inst.timeStamp 
end

function LuaAppearancePreviewMgr:IsCurrentInUseProfileFrame(frameId)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.ProfileInfo.Frame == frameId or false
end

function LuaAppearancePreviewMgr:GetCurrentInUseProfileFrame()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.ProfileInfo.Frame or 0
end

function LuaAppearancePreviewMgr:GetProfileFrameConditionText(id)
    local expiredTime = LuaExpressionMgr:GetProfieFrameExpireTime(id)
    if expiredTime<0 then
        return "[c][FFFFFF]"..LocalString.GetString("有效期至 永久").."[-][/c]"
    elseif expiredTime>0 then --已获得
        local dt =  CServerTimeMgr.ConvertTimeStampToZone8Time(expiredTime)
        if expiredTime<CServerTimeMgr.Inst.timeStamp then
            return "[c][FF0000]"..LocalString.GetString("已过期").."[-][/c]"
        else
	        return "[c][FFFFFF]"..ToStringWrap(dt, LocalString.GetString("有效期至 yy-MM-dd HH:mm")).."[-][/c]"
        end
    else
        return "[c][FF0000]"..LocalString.GetString("未获得").."[-][/c]"
    end
end

function LuaAppearancePreviewMgr:RequestSetProfileFrame(frameId)
    --游戏头像框
    Gac2Gas.SetProfileFrame(frameId)
    --梦岛头像框
    CPersonalSpaceMgr.Inst:SetExpressionFrame(frameId, nil, nil)
end

function LuaAppearancePreviewMgr:RequestDiscardProfileFrame(frameId)
    Gac2Gas.DiscardProfileFrame(frameId)
end

-------------------队伍背景-------------------
function LuaAppearancePreviewMgr:GetAllTeamMemberBgInfo(excludeNotOwned)
    local ret = {}
    Wardrobe_TeamMemberBg.Foreach(function (key, data)
        if (not excludeNotOwned or self:MainPlayerHasTeamMemberBg(key)) and data.Status==0 then
            table.insert(ret,{id=key, name=data.Name, icon=data.ResName, memberBg=data.ResName, 
                                memberBorder=data.BorderName, teamBg=data.SpecialBgName, order=data.Order, itemGetId=data.ItemGetId, fashionability=data.Fashionability})
        end
    end)
    --优先解锁显示前面，其次order大的显示前面
    table.sort(ret, function(data1, data2)
        local hasData1 = self:MainPlayerHasTeamMemberBg(data1.id)
        local hasData2 = self:MainPlayerHasTeamMemberBg(data2.id)
        if hasData1~=hasData2 then
            return hasData1
        else
            return data1.order>data2.order
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasTeamMemberBg(id)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.WardrobeProp.TeamMemberBgUnLockSet:GetBit(id) or false
end

function LuaAppearancePreviewMgr:IsCurrentInUseTeamMemberBg(id)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.TeamMemberBg == id
end

function LuaAppearancePreviewMgr:GetCurrentInUseTeamMemberBg()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.TeamMemberBg or 0
end

function LuaAppearancePreviewMgr:GetCurrentTeamMemberBgName()
    local id = self:GetCurrentInUseTeamMemberBg()
    if id>0 then return Wardrobe_TeamMemberBg.GetData(id).ResName end
    return self:GetDefaultTeamMemberBgName()
end

function LuaAppearancePreviewMgr:GetDefaultTeamMemberBgName()
    return Wardrobe_Setting.GetData().DefaultTeamMemberBg
end

function LuaAppearancePreviewMgr:GetTeamMemberBgConditionText(id)
    if self:MainPlayerHasTeamMemberBg(id) then
        return "[c][FFFFFF]"..LocalString.GetString("永久获得").."[-][/c]"
    else
        return "[c][FF0000]"..LocalString.GetString("未获得").."[-][/c]"
    end
end

function LuaAppearancePreviewMgr:RequestSetTeamMemberBg(bgId)
    Gac2Gas.RequestUseTeamMemberBg(bgId)
end
-------------------队长标识-------------------
function LuaAppearancePreviewMgr:GetAllTeamLeaderIconInfo(excludeNotOwned)
    local ret = {}
    Wardrobe_TeamLeaderIcon.Foreach(function (key, data)
        if (not excludeNotOwned or self:MainPlayerHasTeamLeaderIcon(key)) and data.Status==0 then
            table.insert(ret,{id=key, name=data.Name, icon=data.IconName, sprite=data.ResName, order=data.Order, itemGetId=data.ItemGetId, fashionability=data.Fashionability})
        end
    end)
    --优先解锁显示前面，其次order大的显示前面
    table.sort(ret, function(data1, data2)
        local hasData1 = self:MainPlayerHasTeamLeaderIcon(data1.id)
        local hasData2 = self:MainPlayerHasTeamLeaderIcon(data2.id)
        if hasData1~=hasData2 then
            return hasData1
        else
            return data1.order>data2.order
        end
    end)
    return ret
end

function LuaAppearancePreviewMgr:MainPlayerHasTeamLeaderIcon(id)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.WardrobeProp.TeamLeaderIconUnLockSet:GetBit(id) or false
end

function LuaAppearancePreviewMgr:IsCurrentInUseTeamLeaderIcon(id)
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.TeamLeaderIcon == id
end

function LuaAppearancePreviewMgr:GetCurrentInUseTeamLeaderIcon()
    return CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.TeamLeaderIcon or 0
end

function LuaAppearancePreviewMgr:GetCurrentTeamLeaderIconName()
    local id = self:GetCurrentInUseTeamLeaderIcon()
    if id>0 then return Wardrobe_TeamLeaderIcon.GetData(id).ResName end
    return self:GetDefaultTeamLeaderIconName()
end

function LuaAppearancePreviewMgr:GetDefaultTeamLeaderIconName()
    return Wardrobe_Setting.GetData().DefaultTeamLeaderIcon
end

function LuaAppearancePreviewMgr:GetTeamLeaderIconConditionText(id)
    if self:MainPlayerHasTeamLeaderIcon(id) then
        return "[c][FFFFFF]"..LocalString.GetString("永久获得").."[-][/c]"
    else
        return "[c][FF0000]"..LocalString.GetString("未获得").."[-][/c]"
    end
end

function LuaAppearancePreviewMgr:RequestSetTeamLeaderIcon(id)
    Gac2Gas.RequestUseTeamLeaderIcon(id)
end

-------------------永久时装交易(相关功能目前用于永久时装，之后可能会被通用，如果被其他地方使用，可能相关同学需要调整现有逻辑)-------------------
LuaAppearancePreviewMgr.m_CBGTradeItem2NoneTradeItem = nil
LuaAppearancePreviewMgr.m_CBGNoneTradeItem2TradeItem = nil
LuaAppearancePreviewMgr.m_NeedTransferCBGItemId = 0

function LuaAppearancePreviewMgr:ParseCBGTradeItemInfo()
    if self.m_CBGTradeItem2NoneTradeItem==nil then
        self.m_CBGTradeItem2NoneTradeItem = {}
        self.m_CBGNoneTradeItem2TradeItem = {}
        Cangbaoge_ItemTrade.Foreach(function (key, data)
            self.m_CBGTradeItem2NoneTradeItem[data.ID] = {targetId=data.NoneTradeItem, cost=data.ToNoneTradeItemCost}
            self.m_CBGNoneTradeItem2TradeItem[data.NoneTradeItem] = {targetId=data.ID, cost=data.ToTradeItemCost}
        end)
    end
end

--是否可以上架藏宝阁的道具
function LuaAppearancePreviewMgr:IsCBGTradeItem(templateId)
    self:ParseCBGTradeItemInfo()
    return self.m_CBGTradeItem2NoneTradeItem[templateId]~=nil
end
--是否可以转化成上架藏宝阁的道具
function LuaAppearancePreviewMgr:IsCBGNoneTradeItem(templateId)
    self:ParseCBGTradeItemInfo()
    return self.m_CBGNoneTradeItem2TradeItem[templateId]~=nil
end

function LuaAppearancePreviewMgr:GetCBGTargetInfo(templateId)
    self:ParseCBGTradeItemInfo()
    if self.m_CBGTradeItem2NoneTradeItem[templateId] then 
        return self.m_CBGTradeItem2NoneTradeItem[templateId]
    elseif self.m_CBGNoneTradeItem2TradeItem[templateId] then
        return self.m_CBGNoneTradeItem2TradeItem[templateId]
    else
        return nil
    end
end

function LuaAppearancePreviewMgr:OpenCBG()
    CWebBrowserMgr.Inst:OpenUrl(Cangbaoge_Setting.GetData().CangBaoGeItemTradeURL)
end

function LuaAppearancePreviewMgr:ShowCBGItemTransformWnd(srcItemId)
    if CommonDefs.IS_CN_CLIENT and CLoginMgr.Inst:IsNetEaseOfficialLogin() then --仅官方渠道支持cbg及转换
        self.m_NeedTransferCBGItemId = srcItemId
        CUIManager.ShowUI("AppearanceFashionTransformWnd")
    end
end

function LuaAppearancePreviewMgr:RequestChangeToCbgTradeItem(itemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local place = EnumToInt(EnumItemPlace.Bag)
    if pos>0 then
        Gac2Gas.RequestChangeToCbgTradeItem(place, pos, itemId)
    end
end

function LuaAppearancePreviewMgr:RequestChangeToNoneCbgTradeItem(itemId)
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local place = EnumToInt(EnumItemPlace.Bag)
    if pos>0 then
        Gac2Gas.RequestChangeToNoneCbgTradeItem(place, pos, itemId)
    end
end

------------------------------------------------
-------------------Gas2Gac PRC------------------
------------------------------------------------
function LuaAppearancePreviewMgr:ModifyFashionDaPeiNameSuccess_Permanent(index, name)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end
    local info = CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionDaPeiInfos, index)
    info.Name = name
    g_ScriptEvent:BroadcastInLua("OnFashionDaPeiInfoUpdate")
end

function LuaAppearancePreviewMgr:SaveFashionDaPeiContentSuccess_Permanent(index, headId, bodyId, weaponId, backPendantId)
	local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end
    local info = CommonDefs.DictGetValue_LuaCall(mainplayer.WardrobeProp.FashionDaPeiInfos, index)
    info.HeadId = headId
    info.BodyId = bodyId
    info.WeaponId = weaponId
    info.BackPendantId = backPendantId
    g_ScriptEvent:BroadcastInLua("OnFashionDaPeiInfoUpdate")
end

function LuaAppearancePreviewMgr:UnLockFashionSuccess_Permanent(fashionId)
    g_ScriptEvent:BroadcastInLua("OnUnLockFashionSuccess_Permanent", fashionId)
end

function LuaAppearancePreviewMgr:SetWardRobeTabenNumLimit_Permanent(num)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.WardrobeProp.TabenNumLimit = num
        g_ScriptEvent:BroadcastInLua("OnSetWardRobeTabenNumLimit_Permanent", num)
    end
end

function LuaAppearancePreviewMgr:CompositeTabenSuccess_Permanent()
    g_ScriptEvent:BroadcastInLua("OnCompositeTabenSuccess_Permanent")
end

function LuaAppearancePreviewMgr:SetFashionHideSuccess_Permanent(pos, value)
    --服务器发这个rpc下来时，客户端数据还没更新，这里强制更新一下以便于设置界面上的内容能正常显示
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return false end
    if pos == EnumAppearanceFashionPosition.eHead then
        mainplayer.AppearanceProp.HideHeadFashionEffect = value
    elseif pos == EnumAppearanceFashionPosition.eBody then
        mainplayer.AppearanceProp.HideBodyFashionEffect = value
    elseif pos == EnumAppearanceFashionPosition.eWeapon then
        mainplayer.AppearanceProp.HideWeaponFashionEffect = value 
    elseif pos == EnumAppearanceFashionPosition.eWeapon+2 then -- subweapon
        mainplayer.AppearanceProp.HideSubHandWeaponFashionEffect = value
    elseif pos == EnumAppearanceFashionPosition.eBeiShi then
        mainplayer.AppearanceProp.HideBackPendantFashionEffect = value
    else
        print("SetFashionHideSuccess_Permanent params error", pos)
    end
	g_ScriptEvent:BroadcastInLua("OnSetFashionHideSuccess_Permanent", pos,value)
end

function LuaAppearancePreviewMgr:UnLockPlayerProfileFrameSuccess(id)
    if ExpressionHead_ProfileFrame.Exists(id) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("UNLOCK_NEW_PROFILE_FRAME", ExpressionHead_ProfileFrame.GetData(id).Descrition), function()
            self:ShowClosetProfileFrameSubview()
        end, nil, nil, nil, false)
    end
end

function LuaAppearancePreviewMgr:UnLockTeamMemberBgSuccess(id)
    if Wardrobe_TeamMemberBg.Exists(id) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("UNLOCK_NEW_TEAMMEMBER_BG", Wardrobe_TeamMemberBg.GetData(id).Name), function()
            self:ShowClosetTeamMemberBgSubview()
        end, nil, nil, nil, false)
    end
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.WardrobeProp.TeamMemberBgUnLockSet:SetBit(id, true)
        g_ScriptEvent:BroadcastInLua("OnUnLockTeamMemberBgSuccess", id)
    end
end

function LuaAppearancePreviewMgr:UseTeamMemberBgSuccess(id)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.AppearanceProp.TeamMemberBg = id
        g_ScriptEvent:BroadcastInLua("OnUseTeamMemberBgSuccess", id)
    end
end

function LuaAppearancePreviewMgr:UnLockTeamLeaderIconSuccess(id)
    if Wardrobe_TeamLeaderIcon.Exists(id) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("UNLOCK_NEW_TEAMLEADER_ICON", Wardrobe_TeamLeaderIcon.GetData(id).Name), function()
            self:ShowClosetTeamLeaderIconSubview()
        end, nil, nil, nil, false)
    end
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.WardrobeProp.TeamLeaderIconUnLockSet:SetBit(id, true)
        g_ScriptEvent:BroadcastInLua("OnUnLockTeamLeaderIconSuccess", id)
    end
end

function LuaAppearancePreviewMgr:UseTeamLeaderIconSuccess(id)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.AppearanceProp.TeamLeaderIcon = id
        g_ScriptEvent:BroadcastInLua("OnUseTeamLeaderIconSuccess", id)
    end
end

function LuaAppearancePreviewMgr:SyncWardrobeProperty(wardrobeProp, reason)
	local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end
    mainplayer.WardrobeProp:LoadFromString(wardrobeProp)
    g_ScriptEvent:BroadcastInLua("OnSyncWardrobeProperty", reason)
end

function LuaAppearancePreviewMgr:EvaluateFashionSuitSuccess_Permanent(suitId)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.WardrobeProp.FashionEvaluateSet:SetBit(suitId, true)
        g_ScriptEvent:BroadcastInLua("OnEvaluateFashionSuitSuccess_Permanent", suitId)
    end
end

function LuaAppearancePreviewMgr:SendFashionTransformState(isTransformOn)
    CClientMainPlayer.Inst.IsTransformOn = isTransformOn
    g_ScriptEvent:BroadcastInLua("OnSendFashionTransformState")
end

function LuaAppearancePreviewMgr:SyncFashionability(value)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.WardrobeProp.Fashionability = value
        g_ScriptEvent:BroadcastInLua("OnSyncFashionability", value)
    end
end

function LuaAppearancePreviewMgr:RequestChangeToCbgTradeItemResult(itemId, bSuccess)
    g_ScriptEvent:BroadcastInLua("OnRequestChangeToCbgTradeItemResult", itemId, bSuccess)
end

function LuaAppearancePreviewMgr:RequestChangeToNoneCbgTradeItemResult(itemId, bSuccess)
    g_ScriptEvent:BroadcastInLua("OnRequestChangeToNoneCbgTradeItemResult", itemId, bSuccess)
end

function LuaAppearancePreviewMgr:UnLockFashionByItemConfirm_Permanent(fashionId, progress, itemId, place, pos)
    self:ShowFashionUnlockWnd(fashionId, EnumAppearanceFashionUnlockType.eItem, progress, itemId, place, pos)
end

function LuaAppearancePreviewMgr:TakeOffFashion_Permanent(fashionId)
    g_ScriptEvent:BroadcastInLua("OnTakeOffFashion_Permanent", fashionId)
end

function LuaAppearancePreviewMgr:TakeOffFashionSuit_Permanent(suitId)
    --根据坤少的设计，脱下套装时，发完两个 TakeOffFashion_Permanent 后，再发这个RPC
    g_ScriptEvent:BroadcastInLua("OnTakeOffFashionSuit_Permanent", suitId)
end

function LuaAppearancePreviewMgr:AddFashionSuccess_Permanent(fashionId, isExist)
    if self:IsAppearanceWndLoaded() then
        if isExist == 1 then
            g_MessageMgr:ShowMessage("Appearance_Preview_Add_Exist_Fashion_Success")
        else
            g_MessageMgr:ShowMessage("Appearance_Preview_Add_Fashion_Success")
        end
        self:ShowClosetFashionSubview(fashionId)
    else
        if isExist == 1 then
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_Add_Fashion_Success_And_Open_Wnd_Confirm"), function()
                self:ShowClosetFashionSubview(fashionId)
            end, nil, nil, nil, false)
        else
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GET_NEW_FASHION"), function()
                self:ShowClosetFashionSubview(fashionId)
            end, nil, nil, nil, false)
        end
    end
end

function LuaAppearancePreviewMgr:UnLockFashionAlert_Permanent(fashionId)
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_Fashion_Unlock_Progress_Enough_And_Open_Wnd_Confirm"), function()
        self:ShowClosetFashionSubview(fashionId)
    end, nil, nil, nil, false)
end

function LuaAppearancePreviewMgr:ShowMessageAfterTakeOnFashion_Permanent(position, fashionId, bHide)
    if bHide and self:IsAppearanceWndLoaded() then
        --可能会有多条，会顶替
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_Switch_Off_Change"), function()
            g_ScriptEvent:BroadcastInLua("OnRequestOpenAppearanceSettingWnd")
        end, nil, LocalString.GetString("去设置"), nil, false)
    else
       g_MessageMgr:ShowMessage("Appearance_Preview_Switch_Open_Change")
    end
end

function LuaAppearancePreviewMgr:DiscardExpressionAppearance(group, appearance)
    local appearanceData = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.ExpressionAppearanceData or nil
    if appearanceData and CommonDefs.DictContains_LuaCall(appearanceData, group) then
        local appearanceSet = CommonDefs.DictGetValue_LuaCall(appearanceData, group)
        if CommonDefs.DictContains_LuaCall(appearanceSet.Appearances, appearance) then
            CommonDefs.DictRemove_LuaCall(appearanceSet.Appearances, appearance)
            if appearanceSet.CurrAppearance == appearance then
                appearanceSet.CurrAppearance = 0
            end
            EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerExpressionAppearance, {})
        end
    end
end