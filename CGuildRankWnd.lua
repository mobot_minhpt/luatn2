-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CGuildRankModel = import "L10.UI.Guild.CGuildRankModel"
local CGuildRankWnd = import "L10.UI.Guild.CGuildRankWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildRankModel.m_SetMemberRankLabel_CS2LuaHook = function (guildMemberInfoRpc, playerId, title, zlIdx) 
    local idx = 0
    if not CommonDefs.DictContains(CGuildRankModel.Office2Idx, typeof(Int32), title) then
        return zlIdx
    end
    if title == 3 then
        idx = zlIdx
        zlIdx = zlIdx + 1
    else
        idx = CommonDefs.DictGetValue(CGuildRankModel.Office2Idx, typeof(Int32), title)
    end
    local memberDynamicInfo = CommonDefs.DictGetValue(guildMemberInfoRpc.DynamicInfos, typeof(UInt64), playerId)
    local name = ""
    local id = 0
    if memberDynamicInfo ~= nil then
        name = memberDynamicInfo.Name
        id = memberDynamicInfo.PlayerId
    end
    CGuildRankModel.Names[idx] = name
    CGuildRankModel.ids[idx] = id
    return zlIdx
end
CGuildRankModel.m_setRankLabel_CS2LuaHook = function (guildMemberInfoRpc) 
    CGuildRankModel.Names = Table2ArrayWithCount({
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        "", 
        ""
    }, 15, MakeArrayClass(System.String))
    CGuildRankModel.ids = Table2ArrayWithCount({
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0, 
        0
    }, 15, MakeArrayClass(System.UInt64))
    if guildMemberInfoRpc == nil then
        return
    end
    local zlIdx = CommonDefs.DictGetValue(CGuildRankModel.Office2Idx, typeof(Int32), 3)
    --应该是长老的index

    CommonDefs.DictIterate(guildMemberInfoRpc.Infos, DelegateFactory.Action_object_object(function (___key, ___value) 
        local entry = {}
        entry.Key = ___key
        entry.Value = ___value
        local playerId = entry.Key
        local memberInfo = entry.Value
        local idx = 0
        local title = memberInfo.Title
        zlIdx = CGuildRankModel.SetMemberRankLabel(guildMemberInfoRpc, playerId, title, zlIdx)
        if memberInfo.MiscData.IsNeiWuFuOfficer == 1 then
            zlIdx = CGuildRankModel.SetMemberRankLabel(guildMemberInfoRpc, playerId, - 1, zlIdx)
        end
    end))
end
CGuildRankWnd.m_Init_CS2LuaHook = function (this) 
    --throw new NotImplementedException();
    this:InitNames()
    CGuildMgr.Inst:GetGuildMemberInfo()
end
CGuildRankWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)

    do
        local i = 0
        while i < this.rects.Length do
            UIEventListener.Get(this.rects[i]).onClick = MakeDelegateFromCSFunction(this.OnClickItem, VoidDelegate, this)
            i = i + 1
        end
    end

    UIEventListener.Get(this.powerSettingBtn).onClick = MakeDelegateFromCSFunction(this.OnClickPowerSettingButton, VoidDelegate, this)
    UIEventListener.Get(this.welfareSettingBtn).onClick = MakeDelegateFromCSFunction(this.OnClickWelfareSettingBtn, VoidDelegate, this)
end
CGuildRankWnd.m_OnClickWelfareSettingBtn_CS2LuaHook = function (this, go) 
    if CGuildRankWnd.OPEN_WELFARE_SETTING then
        CUIManager.ShowUI(CUIResources.GuildWelfareWnd)
    else
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
    end
end
CGuildRankWnd.m_OnClickItem_CS2LuaHook = function (this, p) 
    local index = CommonDefs.ArrayFindIndex_GameObject_Array(this.rects, DelegateFactory.Predicate_GameObject(function (p2) 
        return p2 == p
    end))
    if index > - 1 then
        local playerId = CGuildRankModel.ids[index]
        if playerId > 0 then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end
    end
end
CGuildRankWnd.m_OnGuildInfoReceived_CS2LuaHook = function (this, rpcType) 

    if rpcType == CGuildMgr.RPC_TYPE_GuildMemberInfo then
        CGuildRankModel.setRankLabel(CGuildMgr.Inst.m_GuildMemberInfo)
        this:InitNames()
    end
end
CGuildRankWnd.m_InitNames_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.labels.Length do
            local default
            if not System.String.IsNullOrEmpty(CGuildRankModel.Names[i]) then
                default = CGuildRankModel.Names[i]
            else
                default = LocalString.GetString("[c][cccccc]暂无[-][/c]")
            end
            this.labels[i].text = default
            i = i + 1
        end
    end
end
