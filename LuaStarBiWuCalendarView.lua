local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local QnTableItemPool = import "L10.UI.QnTableItemPool"

local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaStarBiWuCalendarView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiWuCalendarView, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaStarBiWuCalendarView, "Title", "Title", UILabel)
RegistChildComponent(LuaStarBiWuCalendarView, "TableBody", "TableBody", CUIRestrictScrollView)
RegistChildComponent(LuaStarBiWuCalendarView, "RowTable", "RowTable", UITable)
RegistChildComponent(LuaStarBiWuCalendarView, "Pool", "Pool", QnTableItemPool)
RegistChildComponent(LuaStarBiWuCalendarView, "split", "split", GameObject)
RegistChildComponent(LuaStarBiWuCalendarView, "Line", "Line", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuCalendarView,"m_ScheduleToView")
RegistClassMember(LuaStarBiWuCalendarView,"m_CalendarDayList")
RegistClassMember(LuaStarBiWuCalendarView,"m_StartTime")
RegistClassMember(LuaStarBiWuCalendarView,"m_BeginTime")
RegistClassMember(LuaStarBiWuCalendarView,"m_EndTime")
RegistClassMember(LuaStarBiWuCalendarView,"m_CurDayIndex")
RegistClassMember(LuaStarBiWuCalendarView,"m_Group2Schedule")
function LuaStarBiWuCalendarView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_ScheduleToView = nil
    self.m_CalendarDayList = nil
    self.m_StartTime = nil
    self.m_BeginTime = nil
    self.m_EndTime = nil
    self.m_Group2Schedule = {}
    self.m_CurDayIndex = 0
    self.split.gameObject:SetActive(false)
    self.Line.gameObject:SetActive(false)
    self:InitView()
end

function LuaStarBiWuCalendarView:InitView()
    local seasonId = StarBiWuShow_Setting.GetData().Season
    local data = StarBiWuShow_AutoPatch.GetDataBySubKey("Season",seasonId)
    self.Title.text = SafeStringFormat3(LocalString.GetString("%s时间"),data.Desc)
    self:InitCalendarView()
    self:ShowCalendar()

    if(self.m_StartTime and self.m_EndTime) then
        self.TimeLabel.text = SafeStringFormat3("%s-%s",ToStringWrap(self.m_StartTime, LocalString.GetString("yyyy年MM月dd日")),ToStringWrap(self.m_EndTime, LocalString.GetString("yyyy年MM月dd日")))
    end
end

function LuaStarBiWuCalendarView:InitCalendarView()
    local startTime = StarBiWuShow_Calendar.GetData(1)
    if not startTime then return end
    local beginTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(startTime.Date)
    if not beginTimestamp then return end
    local beginTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp)
    self.m_StartTime = beginTime
    local weekId = EnumToInt(beginTime.DayOfWeek)
    if weekId ~= 1 then -- 拿到第一个周一的时间
        if(weekId == 0) then
            beginTimestamp = beginTimestamp - 6 * 24 * 60 * 60
        else
            beginTimestamp = beginTimestamp - (weekId - 1) * 24 * 60 * 60
        end
        beginTime = CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp)
        weekId = EnumToInt(beginTime.DayOfWeek)
    end
    self.m_BeginTime = CServerTimeMgr.Inst:GetDayBegin(beginTime)
    self.m_CalendarDayList = {}
    StarBiWuShow_Calendar.Foreach(function(k,v)
        local timeDate = v.Date
        local timeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(timeDate)
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(timeStamp)
        self.m_EndTime = CServerTimeMgr.Inst:GetDayBegin(time)
        self.m_CalendarDayList[ToStringWrap(self.m_EndTime, "yyyy-MM-dd")] = {
            dateTime = time,
            schedule = v.Schedule,
            commonGroupEvent = v.CommonGroupSchedule,
            topGroupSchedule = v.TopGroupSchedule,
        }
    end)
end

function LuaStarBiWuCalendarView:ShowCalendar()
    if not self.m_BeginTime or not self.m_EndTime or not self.m_CalendarDayList then return end
    self.m_ScheduleToView = {}
    local deltaDay = CServerTimeMgr.Inst:DayDiff(self.m_EndTime, self.m_BeginTime)
    local weekNum = math.ceil(deltaDay / 7)

    for i = 0, weekNum * 7 - 1 do
        local curDay = self.m_BeginTime:AddDays(i)
        local curDayStr = ToStringWrap(curDay, "yyyy-MM-dd")
        table.insert(self.m_ScheduleToView,{
            date = curDay,
            curDayStr = curDayStr,
        })
    end
    Extensions.RemoveAllChildren(self.RowTable.transform)
    self.m_Group2Schedule = {}
    for i = 1,#self.m_ScheduleToView / 7 do
        local line = CUICommonDef.AddChild(self.RowTable.gameObject, self.Line.gameObject)
        local lineTable = line.transform:Find("LineTable"):GetComponent(typeof(UITable))
        line.gameObject:SetActive(true)
        Extensions.RemoveAllChildren(lineTable.transform)
        for j = 1,7 do
            local index = (i - 1) * 7 + j
            local item = self:ItemAt(self.Pool,index)
            item.transform:SetParent(lineTable.transform)
            item.transform.localPosition = Vector3.zero
            item.transform.localScale = Vector3.one
            item.gameObject:SetActive(true)
        end 
        lineTable:Reposition()            
        local split = CUICommonDef.AddChild(self.RowTable.gameObject, self.split.gameObject)
        split.gameObject:SetActive(true)
    end
    self.RowTable:Reposition()
    self.TableBody:ResetPosition()

    local curDayItem = self.m_ScheduleToView[self.m_CurDayIndex]
    if curDayItem and curDayItem.item then
        curDayItem.item.transform:Find("Highlight").gameObject:SetActive(true)
    end
    self:UpdateMyGroupSchedule()
end

function LuaStarBiWuCalendarView:ItemAt(view,index)
    local data = self.m_ScheduleToView[index]
    local item = nil
    local weekDay = EnumToInt(data.date.DayOfWeek)
    local month = data.date.Month
    local day = data.date.Day
    if (weekDay == 2 or weekDay == 4) then
        item = view:GetFromPool(0)
    else
        item = view:GetFromPool(1)
        local item1 = item.transform:Find("Content/Item1")
        local item2 = item.transform:Find("Content/Item2")
        item1.gameObject:SetActive(false)
        item2.gameObject:SetActive(false)
        local icon1 = item1.transform:Find("Sprite").gameObject
        local icon2 = item2.transform:Find("Sprite").gameObject
        icon1:SetActive(false)
        icon2:SetActive(false)
        if self.m_CalendarDayList[data.curDayStr] then
            local schedule = self.m_CalendarDayList[data.curDayStr].schedule
            local commonGroupEvent = self.m_CalendarDayList[data.curDayStr].commonGroupEvent
            local topGroupSchedule = self.m_CalendarDayList[data.curDayStr].topGroupSchedule
            if not System.String.IsNullOrEmpty(schedule) then
                item1.gameObject:SetActive(true)
                item1:GetComponent(typeof(UILabel)).text = schedule
                item1:GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("ffffff", 0)
            elseif not System.String.IsNullOrEmpty(commonGroupEvent) then 
                item1.gameObject:SetActive(true)
                item1:GetComponent(typeof(UILabel)).text = commonGroupEvent
                item1:GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("acf8ff", 0)
                table.insert(self.m_Group2Schedule,{icon1,2})
                if not System.String.IsNullOrEmpty(topGroupSchedule) then
                    item2.gameObject:SetActive(true)
                    item2:GetComponent(typeof(UILabel)).text = topGroupSchedule
                    item2:GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("d293e9", 0)
                    table.insert(self.m_Group2Schedule,{icon2,1})
                end
            elseif not System.String.IsNullOrEmpty(topGroupSchedule) then
                item1.gameObject:SetActive(true)
                item1:GetComponent(typeof(UILabel)).text = topGroupSchedule
                item1:GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("d293e9", 0)
                table.insert(self.m_Group2Schedule,{icon1,1})
            end
        end
    end
    self.m_ScheduleToView[index].item = item
    local DayLabel = item.transform:Find("Content/Day"):GetComponent(typeof(UILabel))
    local MonthLabel = item.transform:Find("Content/Month"):GetComponent(typeof(UILabel))
    --local bgSprite = item.gameObject:GetComponent(typeof(UISprite))
    local content = item.transform:Find("Content")
    item.transform:Find("Highlight").gameObject:SetActive(false)
    DayLabel.text = day
    if day == 1 or weekDay == 1 then
        MonthLabel.text = SafeStringFormat3(LocalString.GetString("%d月"),month)
    else
        MonthLabel.text = ""
    end
    --bgSprite.spriteName = index % 2 == 0 and "common_bg_mission_background_n" or "common_bg_yuanbaomarket"
    local serverNow = CServerTimeMgr.Inst:GetZone8Time()
    serverNow = CServerTimeMgr.Inst:GetDayBegin(serverNow)
    local deltaTime = CServerTimeMgr.Inst:DayDiff(data.date, serverNow)
    CUICommonDef.SetActive(content.gameObject, deltaTime >= 0, false)
    content:GetComponent(typeof(UIWidget)).alpha = deltaTime >= 0 and 1 or 0.2
    
    if deltaTime == 0 then
        self.m_CurDayIndex = index
        DayLabel.color =  NGUIText.ParseColor24("ffffff", 0)
        MonthLabel.color =  NGUIText.ParseColor24("ffffff", 0)
    end
    return item
end

function LuaStarBiWuCalendarView:UpdateMyGroupSchedule()
    if self.m_Group2Schedule then
        for i = 1,#self.m_Group2Schedule do
           local data = self.m_Group2Schedule[i]
            data[1].gameObject:SetActive((data[2] == 1 and CLuaStarBiwuMgr.m_MainPlayerGroup == 1) or (data[2] == 2 and CLuaStarBiwuMgr.m_MainPlayerGroup >= 2))
        end
    end
end


function LuaStarBiWuCalendarView:OnEnable()
    Gac2Gas.QueryStarBiwuGroupIdx()
    self:UpdateMyGroupSchedule()
    g_ScriptEvent:BroadcastInLua("OnStarBiWuDetailInfoWndChange",LocalString.GetString("赛程"))
    g_ScriptEvent:AddListener("OnStarBiWuUpdatePlayerGroup",self, "UpdateMyGroupSchedule")
end

function LuaStarBiWuCalendarView:OnDisable()
    g_ScriptEvent:RemoveListener("OnStarBiWuUpdatePlayerGroup",self, "UpdateMyGroupSchedule")
end

--@region UIEvent

--@endregion UIEvent

