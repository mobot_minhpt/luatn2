
LuaChunJie2024Mgr = {}

LuaChunJie2024Mgr.SLQZInfo = {}

LuaChunJie2024Mgr.fuDaiRewardInfo = {}

--#region 送穷神

function LuaChunJie2024Mgr:QuerySongQiongShen2024InfoResult(rewardTimes, maxRewardTimes, firstPassTimes)
    g_ScriptEvent:BroadcastInLua("QuerySongQiongShen2024InfoResult", rewardTimes, maxRewardTimes, firstPassTimes)
end


function LuaChunJie2024Mgr:ShowSQSRewardWnd(data)
    local list = g_MessagePack.unpack(data)

    self.fuDaiRewardInfo = {}
    self.fuDaiRewardInfo.type = "SQS"
    self.fuDaiRewardInfo.playerInfo = list[1]
    self.fuDaiRewardInfo.rewardChooseInfo = list[2]
    CUIManager.ShowUI(CLuaUIResources.ChunJie2024FuDaiWnd)
end

function LuaChunJie2024Mgr:SyncSongQiongShenChooseRewardIndex(rewardChooseInfo_U)
    self.fuDaiRewardInfo.rewardChooseInfo = g_MessagePack.unpack(rewardChooseInfo_U)
    g_ScriptEvent:BroadcastInLua("SyncSongQiongShenChooseRewardIndex")
end

--#endregion 送穷神



--#region 双龙抢珠

EnumShuangLongQiangZhuEvent = {
	ePlayerEnter = 1,	-- 玩家进入
	ePlayerLeave = 2,	-- 玩家离开
	eRefreshLongZhu = 3,-- 刷新龙珠
	eTransform = 4,		-- 转化
	eDestroyed = 5,		-- 销毁
	eScore = 6,			-- 得分
	eDoubleScore = 7,	-- 双倍得分
	eChangeSpeed = 8,	-- 改变速度
	eCastSkill = 9,    -- 放技能
	eCatched = 10,		-- 抓到龙珠
	eGot = 11,			-- 龙珠到手
}

EnumShuangLongQiangZhuRole = {
	eNone = 0,
	eDianJin = 1,		-- 点金
	eBaoPo = 2,			-- 爆破
	eCaiZhu = 3,		-- 采珠
}

function LuaChunJie2024Mgr:SyncShuangLongQiangZhuApplyResult(bMatching, leftTimes, maxTimes)
    g_ScriptEvent:BroadcastInLua("SyncShuangLongQiangZhuApplyResult", bMatching, leftTimes, maxTimes)
end


function LuaChunJie2024Mgr:ShowSLQZSelectWnd(data)
    local list = g_MessagePack.unpack(data)

    self.SLQZInfo = {}
    self.SLQZInfo.stage = "prepare"
    self.SLQZInfo.leftTime = tonumber(list[1])
    self.SLQZInfo.pos2Role = list[2]
    CUIManager.ShowUI(CLuaUIResources.ChunJie2024SLQZPlayWnd)
end

function LuaChunJie2024Mgr:SyncShuangLongQiangZhuPlayerPos(playePos_u)
    self.SLQZInfo.pos2Role = g_MessagePack.unpack(playePos_u)
    g_ScriptEvent:BroadcastInLua("SyncShuangLongQiangZhuPlayerPos")
end

function LuaChunJie2024Mgr:ShowSLQZPlayWnd(data)
    local list = g_MessagePack.unpack(data)

    self.SLQZInfo = {}
    self.SLQZInfo.stage = "play"
    self.SLQZInfo.myForce = list[1]
    self.SLQZInfo.playerPlayInfo = list[2]
    self.SLQZInfo.force2ScoreInfo = list[3]
    self.SLQZInfo.longZhuInfo = list[4]
    CUIManager.ShowUI(CLuaUIResources.ChunJie2024SLQZPlayWnd)
end

function LuaChunJie2024Mgr:ShowSLQZRewardWnd(data)
    local list = g_MessagePack.unpack(data)

    self.fuDaiRewardInfo = {}
    self.fuDaiRewardInfo.type = "SLQZ"
    self.fuDaiRewardInfo.playerInfo = list[1]
    self.fuDaiRewardInfo.rewardChooseInfo = list[2]
    CUIManager.ShowUI(CLuaUIResources.ChunJie2024FuDaiWnd)
end

function LuaChunJie2024Mgr:SyncShuangLongQiangZhuEvent(event, context)
    g_ScriptEvent:BroadcastInLua("SyncShuangLongQiangZhuEvent", event, context)
end

function LuaChunJie2024Mgr:SyncShuangLongQiangZhuChooseRewardIndex(chooseInfo_U)
    self.fuDaiRewardInfo.rewardChooseInfo = g_MessagePack.unpack(chooseInfo_U)
    g_ScriptEvent:BroadcastInLua("SyncShuangLongQiangZhuChooseRewardIndex")
end

--#endregion 双龙抢珠


--#region 帮会福宴

LuaChunJie2024Mgr.fuYanInfo = {}

function LuaChunJie2024Mgr:ShowModelPreviewWnd(type)
    self.fuYanInfo.previewType = type
    CUIManager.ShowUI(CLuaUIResources.ChunJie2024ModelPreviewWnd)
end



--#endregion 帮会福宴


LuaChunJie2024Mgr.gameplayInfo = {}

function LuaChunJie2024Mgr:IsChunJie2024Gameplay(gameplayId)
    return gameplayId == ChunJie2024_SongQiongShen.GetData().EasyGamePlayId or
        gameplayId == ChunJie2024_SongQiongShen.GetData().HardGamePlayId or
        gameplayId == ChunJie2024_DoubleDragonSetting.GetData().GamePlayId
end

function LuaChunJie2024Mgr:ShowGamePlayConfirmWnd(sessionId, playId, timeout, extraInfo)
    local gameplayData = Gameplay_Gameplay.GetData(playId)
    self.gameplayInfo = {
        sessionId = sessionId,
        playId = playId,
        timeout = timeout,
        rewardTimesDict = extraInfo[2],
        gamePlayType = gameplayData and gameplayData.SubGameplay
    }
    CUIManager.ShowUI(CLuaUIResources.ChunJie2024GamePlayConfirmWnd)
end

