local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local UILabel = import "UILabel"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"

CLuaShuangshiyi2020ClearTrolleyEnterWnd=class()
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyEnterWnd,"m_CommitBtn")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyEnterWnd,"m_RewardCount")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyEnterWnd,"m_TextTable")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyEnterWnd,"m_TextTemplate")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyEnterWnd,"m_ScrollView")

function CLuaShuangshiyi2020ClearTrolleyEnterWnd:InitComponents()
    self.m_CommitBtn = self.transform:Find("Anchor/Bottom/CommitBtn").gameObject
    self.m_RewardCount = self.transform:Find("Anchor/Bottom/Count"):GetComponent(typeof(UILabel))
    self.m_TextTemplate = self.transform:Find("Anchor/Content/TextTemplate").gameObject
    self.m_TextTable = self.transform:Find("Anchor/Content/ScrollView/TextTable"):GetComponent(typeof(UITable))
    self.m_ScrollView = self.transform:Find("Anchor/Content/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
end

function CLuaShuangshiyi2020ClearTrolleyEnterWnd:Init()
    self:InitComponents()
    self:LoadRuleCotent()

    UIEventListener.Get(self.m_CommitBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        CLuaShuangshiyi2020Mgr.EnterGamePlay()
    end)

    local rewardTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShuangshiyi2020ClearTrolley)
    self.m_RewardCount.text =  Double11_Setting.GetData().MaxRewardPerDay - rewardTime
end

function CLuaShuangshiyi2020ClearTrolleyEnterWnd:LoadRuleCotent()
    self.m_TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.m_TextTable.transform)
    local msg = g_MessageMgr:FormatMessage("CLEARTROLLEY_README")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.m_TextTable.gameObject, self.m_TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end

    self.m_ScrollView:ResetPosition()
    self.m_TextTable:Reposition()
end
