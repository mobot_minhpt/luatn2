local UIGrid = import "UIGrid"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local Object = import "System.Object"

LuaWaiMenDiZiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWaiMenDiZiWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaWaiMenDiZiWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaWaiMenDiZiWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWaiMenDiZiWnd, "m_IsTudiView")

function LuaWaiMenDiZiWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWaiMenDiZiWnd:Init()
	self.m_IsTudiView = LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0 
	self.Template.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.Grid.transform)
	for i = 1, 6 do
		local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
		self:InitItem(obj, LuaShiTuMgr.m_WaiMenDiZiInfo[i])
	end
	RegisterTickOnce(function ()
		if self.Grid then
			self.Grid.gameObject:SetActive(false)
			self.Grid.gameObject:SetActive(true)
		end
	end,2000)
end

function LuaWaiMenDiZiWnd:InitItem(go, data)
	local portrait = go.transform:Find("Item/Portrait"):GetComponent(typeof(CUITexture))
	local classSprite = go.transform:Find("Item/ClassSprite"):GetComponent(typeof(UISprite))
	local nameLabel = go.transform:Find("Item/NameLabel"):GetComponent(typeof(UILabel))
	local lvLabel = go.transform:Find("Item/LvLabel"):GetComponent(typeof(UILabel))
	local addBtn = go.transform:Find("Empty/AddBtn").gameObject
	local noneLabel = go.transform:Find("Empty").gameObject

	go.gameObject:SetActive(true)
	portrait.gameObject:SetActive(data ~= nil)
	classSprite.gameObject:SetActive(data ~= nil)
	nameLabel.gameObject:SetActive(data ~= nil)
	lvLabel.gameObject:SetActive(data ~= nil)
	addBtn.gameObject:SetActive(data == nil and not self.m_IsTudiView)
	noneLabel.gameObject:SetActive(data == nil)

	if data then
		portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), false)
		classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))
		nameLabel.text = data.playerName
		local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
		if data.playerId == myId then
			nameLabel.color = NGUIText.ParseColor24("379a3c", 0)
		end
		lvLabel.text = SafeStringFormat3("Lv.%d",data.playerLevel)
		lvLabel.color = NGUIText.ParseColor24(data.isFeiSheng and "fe7900" or "6A400F", 0)
		UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
			local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
			CommonDefs.DictSet(dict, typeof(cs_string),  "TuDiId", typeof(Object), data.playerId)
			CommonDefs.DictSet(dict, typeof(cs_string),  "TuDiName", typeof(Object), data.playerName)
			local pos = Vector3(540, 0, 0)
            pos = self.transform:TransformPoint(pos)   
			if self.m_IsTudiView then
				CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
			else
				CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.ShiTu, EChatPanel.Undefined, nil, nil, dict, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
			end 
		end)
		Extensions.SetLocalPositionZ(go.transform, data.isOnline and 1 or -1)
	end
	UIEventListener.Get(addBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		LuaShiTuMgr:ShouTuWaiMenDiZi()
	end)
end

--@region UIEvent

function LuaWaiMenDiZiWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("WaiMenDiZiWnd_ReadMe")
end

--@endregion UIEvent

