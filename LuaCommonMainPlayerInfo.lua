local CLoginMgr = import "L10.Game.CLoginMgr"

LuaCommonMainPlayerInfo = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

function LuaCommonMainPlayerInfo:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    if not CClientMainPlayer.Inst then return end
    self.transform:Find("Player"):GetComponent(typeof(CUITexture)):LoadPortrait(CClientMainPlayer.Inst.PortraitName, false)
    self.transform:Find("Player/Lv"):GetComponent(typeof(UILabel)).text = CUICommonDef.GetMainPlayerColoredLevelString(Color.white)
    self.transform:Find("ID"):GetComponent(typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
    self.transform:Find("Name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name

    local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
    self.transform:Find("Server"):GetComponent(typeof(UILabel)).text = myGameServer and myGameServer.name or ""
end

function LuaCommonMainPlayerInfo:Init()
end

--@region UIEvent

--@endregion UIEvent

