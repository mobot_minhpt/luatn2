-- Auto Generated!!
local AutoDrugWndMgr = import "L10.UI.AutoDrugWndMgr"
local CAutoDrugMidWnd = import "L10.UI.CAutoDrugMidWnd"
local CDrugHMPItemCell = import "L10.UI.CDrugHMPItemCell"
local CGameSettingMgr = import "L10.Game.CGameSettingMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Single = import "System.Single"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local QnCheckBox = import "L10.UI.QnCheckBox"
CAutoDrugMidWnd.m_OnEnable_CS2LuaHook = function (this)
    UIEventListener.Get(this.HPItemButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.HPItemButton).onClick, MakeDelegateFromCSFunction(this.OnHMPButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.MPItemButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.MPItemButton).onClick, MakeDelegateFromCSFunction(this.OnHMPButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.HpSwitchButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.HpSwitchButton).onClick, MakeDelegateFromCSFunction(this.OnHpSwitchButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.MpSwitchButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.MpSwitchButton).onClick, MakeDelegateFromCSFunction(this.OnMpSwitchButtonClick, VoidDelegate, this), true)
end
CAutoDrugMidWnd.m_OnDisable_CS2LuaHook = function (this)
    UIEventListener.Get(this.HPItemButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.HPItemButton).onClick, MakeDelegateFromCSFunction(this.OnHMPButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.MPItemButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.MPItemButton).onClick, MakeDelegateFromCSFunction(this.OnHMPButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.HpSwitchButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.HpSwitchButton).onClick, MakeDelegateFromCSFunction(this.OnHpSwitchButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.MpSwitchButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.MpSwitchButton).onClick, MakeDelegateFromCSFunction(this.OnMpSwitchButtonClick, VoidDelegate, this), false)
end
CAutoDrugMidWnd.m_Start_CS2LuaHook = function (this)
    this:InitButton()
    this.HPSlider:Init(true)
    this.MPSlider:Init(false)
end
CAutoDrugMidWnd.m_InitFx_CS2LuaHook = function (this)
    this:InitAutoDrugFx()
end
CAutoDrugMidWnd.m_InitAutoDrugFx_CS2LuaHook = function (this)
    --hpFxRoot.LoadFx(CUIFxPaths.HpFxPath);
    --mpFxRoot.LoadFx(CUIFxPaths.MpFxPath);
    this.hpFxRoot:SetActive(false)
    this.mpFxRoot:SetActive(false)
    local temp = CGameSettingMgr.Inst:GetAutoHealInfo()
    if temp ~= nil and temp.Length >= 6 then
        if temp[0] == "1" and AutoDrugWndMgr.CurrentHpFood ~= nil then
            local HpFoodAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, AutoDrugWndMgr.CurrentHpFood.ID)
            if HpFoodAmount > 0 then
                this.hpFxRoot:SetActive(true)
            end
        end
        if temp[5] == "1" and AutoDrugWndMgr.CurrentMpFood ~= nil then
            local MpFoodAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, AutoDrugWndMgr.CurrentMpFood.ID)
            if MpFoodAmount > 0 then
                this.mpFxRoot:SetActive(true)
            end
        end
    end
end

CAutoDrugMidWnd.m_UpdateAutoDrugFx_CS2LuaHook = function (this)
    this.hpFxRoot:SetActive(false)
    this.mpFxRoot:SetActive(false)
    if this.isHpAutoOn and AutoDrugWndMgr.CurrentHpFood ~= nil then
        local HpFoodAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, AutoDrugWndMgr.CurrentHpFood.ID)
        if HpFoodAmount > 0 then
            this.hpFxRoot:SetActive(true)
        end
    end
    if this.isMpAutoOn and AutoDrugWndMgr.CurrentMpFood ~= nil then
        local MpFoodAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, AutoDrugWndMgr.CurrentMpFood.ID)
        if MpFoodAmount > 0 then
            this.mpFxRoot:SetActive(true)
        end
    end
end
CAutoDrugMidWnd.m_InitButton_CS2LuaHook = function (this)
    local temp = CGameSettingMgr.Inst:GetAutoHealInfo()
    if temp ~= nil and temp.Length >= 6 then
        local default
        if temp[0] == "0" then
            default = false
        else
            default = true
        end
        this.isHpAutoOn = default
        --SwitchHpLabel.text = isHpAutoOn ? "开启" : "关闭";
        CommonDefs.GetComponent_GameObject_Type(this.HpSwitchButton, typeof(QnSelectableButton)):SetSelected(this.isHpAutoOn, false)
        local extern
        if temp[5] == "0" then
            extern = false
        else
            extern = true
        end
        this.isMpAutoOn = extern
        --SwitchMpLabel.text = isMpAutoOn ? "开启" : "关闭";
        CommonDefs.GetComponent_GameObject_Type(this.MpSwitchButton, typeof(QnSelectableButton)):SetSelected(this.isMpAutoOn, false)

        local autoAddDrugValue = false
        if temp.Length > 6 and temp[6] == '1' then
          autoAddDrugValue = true
        end
        this.isAutoAddDrugOn = autoAddDrugValue

        local qnCheckBox = this.transform:Find('AutoDrug/QnCheckBox'):GetComponent(typeof(QnCheckBox))
        qnCheckBox.Selected = this.isAutoAddDrugOn
        qnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
            this.isAutoAddDrugOn = value
        end)
    end
end
CAutoDrugMidWnd.m_InitFood_CS2LuaHook = function (this)
    local temp = CGameSettingMgr.Inst:GetAutoHealInfo()
    if temp ~= nil and temp.Length >= 6 then
        AutoDrugWndMgr.SetCurrentFood((System.Int32.Parse(temp[2])), (System.Int32.Parse(temp[4])))
        --AutoDrugWndMgr.SetCurrentFood(false, (uint)(int.Parse(temp[4])));
    else
        AutoDrugWndMgr.SetCurrentFood(0, 0)
    end
end
CAutoDrugMidWnd.m_OnHpSwitchButtonClick_CS2LuaHook = function (this, go)
    if this.isHpAutoOn then
        --SwitchHpLabel.text = "关闭";
        CommonDefs.GetComponent_GameObject_Type(go, typeof(QnSelectableButton)):SetSelected(true, false)
        this.isHpAutoOn = false
        --AutoHealInfo[0] = "0";
    else
        --SwitchHpLabel.text = "开启";
        CommonDefs.GetComponent_GameObject_Type(go, typeof(QnSelectableButton)):SetSelected(false, false)
        this.isHpAutoOn = true
        --AutoHealInfo[0] = "1";
    end
    this:UpdateAutoDrugFx()
end
CAutoDrugMidWnd.m_OnMpSwitchButtonClick_CS2LuaHook = function (this, go)
    if this.isMpAutoOn then
        --SwitchMpLabel.text = "关闭";
        CommonDefs.GetComponent_GameObject_Type(go, typeof(QnSelectableButton)):SetSelected(true, false)
        this.isMpAutoOn = false
        --AutoHealInfo[0] = "0";
    else
        --SwitchMpLabel.text = "开启";
        CommonDefs.GetComponent_GameObject_Type(go, typeof(QnSelectableButton)):SetSelected(false, false)
        this.isMpAutoOn = true
        --AutoHealInfo[0] = "1";
    end
    this:UpdateAutoDrugFx()
end
CAutoDrugMidWnd.m_SetFood_CS2LuaHook = function (this, HpFood, MpFood)
    if HpFood ~= nil then
        local HpFoodAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, HpFood.ID)
        CommonDefs.GetComponent_GameObject_Type(this.HPItemButton, typeof(CDrugHMPItemCell)):InitItemCell(HpFood.Icon, HpFoodAmount)
    end
    if MpFood ~= nil then
        local MpFoodAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, MpFood.ID)
        CommonDefs.GetComponent_GameObject_Type(this.MPItemButton, typeof(CDrugHMPItemCell)):InitItemCell(MpFood.Icon, MpFoodAmount)
    end
    this:UpdateAutoDrugFx()
end
