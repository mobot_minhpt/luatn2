local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local UIBasicSprite = import "UIBasicSprite"

LuaKunxiansuoUseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaKunxiansuoUseWnd, "CommitBtn", "CommitBtn", CButton)
RegistChildComponent(LuaKunxiansuoUseWnd, "SelectBtn", "SelectBtn", CButton)
RegistChildComponent(LuaKunxiansuoUseWnd, "Label", "Label", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaKunxiansuoUseWnd,"m_SelectPlayer")
RegistClassMember(LuaKunxiansuoUseWnd,"m_SelectPlayerName")
RegistClassMember(LuaKunxiansuoUseWnd,"m_SelectPlayerId")
RegistClassMember(LuaKunxiansuoUseWnd,"m_SelectListener")

function LuaKunxiansuoUseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)

	UIEventListener.Get(self.SelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectBtnClick()
	end)

    --@endregion EventBind end
end

function LuaKunxiansuoUseWnd:Init()
	CUIManager.ShowUI(CLuaUIResources.KunxiansuoFoeSelectWnd)
	self.m_SelectPlayer = nil
	self.m_SelectPlayerId = nil
	self.m_SelectPlayerName = nil

	local sp = self.SelectBtn.gameObject.transform:GetComponent(typeof(UISprite))
	sp.flip = UIBasicSprite.Flip.Vertically
end

function LuaKunxiansuoUseWnd:OnFoeSelect(playerId, playerName)
	-- 选择一个仇敌
	self.m_SelectPlayerName = playerName
	self.m_SelectPlayerId = playerId
	self.Label.text = playerName
	local sp = self.SelectBtn.gameObject.transform:GetComponent(typeof(UISprite))
	sp.flip = UIBasicSprite.Flip.Horizontally
end

function LuaKunxiansuoUseWnd:OnWndClose()
	local sp = self.SelectBtn.gameObject.transform:GetComponent(typeof(UISprite))
	sp.flip = UIBasicSprite.Flip.Horizontally
end

function LuaKunxiansuoUseWnd:OnEnable()
	self.m_SelectListener = DelegateFactory.Action_ulong_string(function(playerId, playerName)
		self:OnFoeSelect(playerId, playerName)
	end)

	g_ScriptEvent:AddListener("OnKunxiansuoFoeSelect", self, "OnFoeSelect")
	g_ScriptEvent:AddListener("OnKunxiansuoButtonDown", self, "OnWndClose")
	EventManager.AddListenerInternal(EnumEventType.SelectPlayer, self.m_SelectListener)
end

function LuaKunxiansuoUseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnKunxiansuoFoeSelect", self, "OnFoeSelect")
	g_ScriptEvent:RemoveListener("OnKunxiansuoButtonDown", self, "OnWndClose")
	EventManager.RemoveListenerInternal(EnumEventType.SelectPlayer, self.m_SelectListener)
end

--@region UIEvent

function LuaKunxiansuoUseWnd:OnCommitBtnClick()
	local kunxiansuo = LianHua_Setting.GetData().KunXianSuoItemId
	if self.m_SelectPlayerId~=nil then
		Gac2Gas.RequestUseKunXianSuo(self.m_SelectPlayerId, LuaZongMenMgr.m_KunxiansuoItemId, LuaZongMenMgr.m_KunxiansuoPlace, LuaZongMenMgr.m_KunxiansuoPos)
		CUIManager.CloseUI(CLuaUIResources.KunxiansuoUseWnd)
	else
		g_MessageMgr:ShowMessage("Choose_Target")
	end
end

function LuaKunxiansuoUseWnd:OnSelectBtnClick()
	CUIManager.ShowUI(CLuaUIResources.KunxiansuoFoeSelectWnd)
	local sp = self.SelectBtn.gameObject.transform:GetComponent(typeof(UISprite))
	sp.flip = UIBasicSprite.Flip.Vertically
end

--@endregion UIEvent

