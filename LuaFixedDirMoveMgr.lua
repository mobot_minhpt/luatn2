local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

CLuaFixedDirMoveMgr = class()

function CLuaFixedDirMoveMgr:AddListener()
    g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
    g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:AddListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
end
CLuaFixedDirMoveMgr:AddListener()

CLuaFixedDirMoveMgr.m_CachedX = 0
CLuaFixedDirMoveMgr.m_CachedZ = 0
CLuaFixedDirMoveMgr.m_Threshold = 0.0001

function CLuaFixedDirMoveMgr:OnPlayerDragJoyStick(args)
    if not CClientMainPlayer.Inst.IsFixedDirMoving then return end

    local Dir = args[0]
    if  math.abs(self.m_CachedX - Dir.x) > self.m_Threshold or math.abs(self.m_CachedZ - Dir.z) > self.m_Threshold then
        Gac2Gas.ChangeDirInFixedDirMoving(Dir.x, Dir.z)
        self.m_CachedX = Dir.x
        self.m_CachedZ = Dir.z
    end
end

function CLuaFixedDirMoveMgr:OnPlayerDragJoyStickComplete()
    if not CClientMainPlayer.Inst.IsFixedDirMoving then return end
    
    Gac2Gas.ChangeDirInFixedDirMoving(0, 0)
    self.m_CachedX = 0
    self.m_CachedZ = 0
end
