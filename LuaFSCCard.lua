local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local Physics = import "UnityEngine.Physics"
local Debug = import "UnityEngine.Debug"

LuaFSCCard = class()

RegistClassMember(LuaFSCCard, "m_Id")
RegistClassMember(LuaFSCCard, "m_SelectedId")
RegistClassMember(LuaFSCCard, "m_IsRare")
RegistClassMember(LuaFSCCard, "m_Status")
RegistClassMember(LuaFSCCard, "m_Borrowed")
RegistClassMember(LuaFSCCard, "m_Data")
RegistClassMember(LuaFSCCard, "m_InitialScale")
RegistClassMember(LuaFSCCard, "m_IsSelected")
RegistClassMember(LuaFSCCard, "m_IsPopUp")
RegistClassMember(LuaFSCCard, "m_IsTop")
RegistClassMember(LuaFSCCard, "m_OriDepth")
RegistClassMember(LuaFSCCard, "m_OriHLA")
RegistClassMember(LuaFSCCard, "m_CanCollect")

RegistClassMember(LuaFSCCard, "m_Card")
RegistClassMember(LuaFSCCard, "m_Anim")
RegistClassMember(LuaFSCCard, "m_Owner")
RegistClassMember(LuaFSCCard, "m_ParentName")
RegistClassMember(LuaFSCCard, "m_NameLabel")
RegistClassMember(LuaFSCCard, "m_RoleTex")
RegistClassMember(LuaFSCCard, "m_CardBg")
RegistClassMember(LuaFSCCard, "m_EmptyHint")
RegistClassMember(LuaFSCCard, "m_Highlight")
RegistClassMember(LuaFSCCard, "m_Mask")
RegistClassMember(LuaFSCCard, "m_BaseScore")
RegistClassMember(LuaFSCCard, "m_DiscardBtn")

function LuaFSCCard:Awake()
    self:InitComps()
end

function LuaFSCCard:OnEnable()
    g_ScriptEvent:AddListener("FSC_DisableOperation" ,self, "Reset")
    g_ScriptEvent:AddListener("FSC_SelectCard" ,self, "SelectCard")
end

function LuaFSCCard:OnDisable()
    g_ScriptEvent:RemoveListener("FSC_DisableOperation" ,self, "Reset")
    g_ScriptEvent:RemoveListener("FSC_SelectCard" ,self, "SelectCard")
end

function LuaFSCCard:SelectCard(id)
    self.m_SelectedId = id
    if self:IsMy("HandCards") then
        if id > 0 then
            if id == self.m_Id then
                self.m_IsSelected = true
                SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.play, Vector3.zero, nil, 0)
                self.m_Anim:Play("fsccard_in")
                local cardIn = self.m_Anim["fsccard_in"]
                cardIn.time = cardIn.length
                self.m_Card.localScale = self.m_InitialScale
                self:SetTop(false)

                self.m_DiscardBtn:SetActive(not LuaFourSeasonCardMgr:CheckPlayCard())
            else
                self.m_IsSelected = false
                self.m_DiscardBtn:SetActive(false)
                self:PopUp(false)
            end
        elseif self.m_IsSelected then
            self.m_IsSelected = false
            self.m_DiscardBtn:SetActive(false)
            self:PopUp(true)
        end
    elseif self:IsPublic() then
        self.m_CanCollect = false
        if id > 0 then
            local data = LuaFourSeasonCardMgr:GetCardData(self.m_Id)
            local selectData = LuaFourSeasonCardMgr:GetCardData(id)
            if data and selectData and data.Season == selectData.Season then
                self.m_CanCollect = true
                self:Highlight(true)
                self:Mask(false)
            else
                self:Highlight(false)
                self:Mask(true)
            end
        else
            self:Highlight(false)
            self:Mask(false)
        end
    end
end

function LuaFSCCard:Reset()
    if not self:IsInGameWnd() then return end
    if self.m_InitialScale then
        self.m_Card.localScale = self.m_InitialScale
    end
    if self.m_OriDepth then
        self.transform:GetComponent(typeof(UIPanel)).depth = self.m_OriDepth
    end

    self:SetTop(false)
    self:PopUp(false)
    self:Highlight(false)
    self:Mask(false)

    self.m_IsSelected = false
    self.m_CanCollect = false
    self.m_DiscardBtn:SetActive(false)
end

function LuaFSCCard:InitComps()
    self.m_Card = self.transform:Find("Card")
    self.m_Anim = self.m_Card:GetComponent(typeof(Animation))
    self.m_SelectedId = 0

    self.m_InitialScale = self.m_Card.localScale
    self.m_Owner = self.transform.parent and self.transform.parent.parent or nil
    self.m_ParentName = self.transform.parent and self.transform.parent.name or ""

    self.m_NameLabel = self.m_Card:Find("Name"):GetComponent(typeof(UILabel))
    self.m_RoleTex = self.m_Card:Find("Role"):GetComponent(typeof(CUITexture))
    self.m_CardBg = self.m_Card:Find("BG")
    self.m_EmptyHint = self.m_Card:Find("Empty")
    self.m_Highlight = self.m_Card:Find("Highlight"):GetComponent(typeof(Animation))
    self.m_Mask = self.m_Card:Find("Mask").gameObject
    self.m_BaseScore = self.m_Card:Find("BaseScore"):GetComponent(typeof(UILabel))
    self.m_DiscardBtn = self.m_Card:Find("DiscardBtn").gameObject

    self.m_IsTop = false
    self.m_OriHLA = self.m_Highlight:GetComponent(typeof(UITexture)).alpha

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClick()
    end)
    UIEventListener.Get(self.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        if not isPressed and not self:IsPublic() then return end
        self:OnPress(isPressed)
    end)
    UIEventListener.Get(self.gameObject).onHover = DelegateFactory.BoolDelegate(function (go, isOver)
        self:OnHover(isOver)
    end)
    UIEventListener.Get(self.gameObject).onSelect = DelegateFactory.BoolDelegate(function(go, isSelected)
        self:OnSelect(isSelected)
    end)
    UIEventListener.Get(self.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:OnDragStart(g)
    end)
    UIEventListener.Get(self.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self:OnDragEnd(g)
    end)
    UIEventListener.Get(self.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g, delta)
        self:OnDrag(g, delta)
    end)

    UIEventListener.Get(self.m_DiscardBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        if LuaFourSeasonCardMgr.EnableOperation and self.m_Id and self.m_Id > 0 then
            Gac2Gas.Anniv2023FSC_RequestReplaceCard(self.m_Id)
        end
    end)
end

function LuaFSCCard:IsMy(type)
    return self.m_Owner and self.m_Owner.name == "Self" and self.m_ParentName == type
end

function LuaFSCCard:IsOther(type)
    return self.m_Owner and self.m_Owner.name == "Enemy" and self.m_ParentName == type
end

function LuaFSCCard:IsPublic()
    return self.m_Owner and self.m_Owner.name == "Public"
end

function LuaFSCCard:IsInRareCardWnd()
    local obj = CUIManager.instance:GetGameObject(CLuaUIResources.FSCPrepareRareCardWnd)
    return obj and NGUITools.IsChild(obj.transform, self.transform)
end

function LuaFSCCard:IsInCardPileWnd()
    local obj = CUIManager.instance:GetGameObject(CLuaUIResources.FSCCardPileWnd)
    return obj and NGUITools.IsChild(obj.transform, self.transform)
end

function LuaFSCCard:IsInGameWnd()
    local obj = CUIManager.instance:GetGameObject(CLuaUIResources.FSCInGameWnd)
    return obj and NGUITools.IsChild(obj.transform, self.transform)
end

function LuaFSCCard:SetActive(active) -- 可能会关掉动效
    for i = 0, self.m_Card.childCount - 1 do
        self.m_Card:GetChild(i).gameObject:SetActive(active)
    end
end

function LuaFSCCard:SetBg()
    self.m_CardBg.gameObject:SetActive(true)

    local back = self.m_CardBg:Find("Back")
    local quality = self.m_CardBg:Find("Quality")
    local season = self.m_CardBg:Find("Season")
    if self.m_Status == EnumFSC_CardStatus.Back then
        back.gameObject:SetActive(true)
        quality.gameObject:SetActive(false)
        season.gameObject:SetActive(false)
    else
        back.gameObject:SetActive(false)
        quality.gameObject:SetActive(true)
        quality:Find("Rare").gameObject:SetActive(self.m_IsRare)
        quality:Find("Normal").gameObject:SetActive(not self.m_IsRare)
        season.gameObject:SetActive(true)
        for i = 1, 4 do
            season:Find(LuaFourSeasonCardMgr:GetSeasonName(i)).gameObject:SetActive(self.m_Data.Season == i)
        end
    end
end

function LuaFSCCard:SetTop(top)
    local panel = self.transform:GetComponent(typeof(UIPanel))
    if panel then 
        if self.m_IsTop ~= top then
            if top then
                self.m_OriDepth = panel.depth
                panel.depth = LuaFourSeasonCardMgr.MaxCardDepth + 1
            else
                panel.depth = self.m_OriDepth or 0
            end
            self.m_IsTop = top
        end
    end
end

function LuaFSCCard:PopUp(popup)
    if popup then
        if not self.m_IsSelected then
            SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.info, Vector3.zero, nil, 0)
            self.m_Anim:Play("fsccard_scale_in")
            self:SetTop(true)
        end
    else
        if self.m_IsPopUp then
            self.m_Anim:Play("fsccard_out")
            self.m_Card.localScale = self.m_InitialScale
            self:SetTop(false)
        end
    end
    self.m_IsPopUp = popup
    if not self.m_CanCollect then
        self:Highlight(popup, true)
    end
end

function LuaFSCCard:Highlight(highlight, hard)
    self.m_Highlight.gameObject:SetActive(highlight)
    if highlight then
        if hard then
            self.m_Highlight:Stop()
            if self.m_IsSelected then
                self.m_Highlight:GetComponent(typeof(UITexture)).alpha = 1
            else
                self.m_Highlight:GetComponent(typeof(UITexture)).alpha = self.m_OriHLA
            end
        else
            self.m_Highlight:GetComponent(typeof(UITexture)).alpha = self.m_OriHLA
            self.m_Highlight:Play("fsccard_highlight")
        end
    end
end

function LuaFSCCard:Mask(mask)
    self.m_Mask:SetActive(mask)
end

function LuaFSCCard:LevelUp()
    self.m_Card:Find("vfxfankui").gameObject:SetActive(true)
    self.m_Card:Find("vfxfankui/CUIFxka").gameObject:SetActive(true)
    self.m_Card:Find("vfxfankui/CUIFxka"):GetChild(0):GetComponent(typeof(ParticleSystem)):Play()
end

function LuaFSCCard:ShowTopDetail(show)
    if not self.m_IsSelected then
        self:PopUp(show)
    end
    g_ScriptEvent:BroadcastInLua("FSC_ShowCardTopDetail", show, self.m_Id, self.m_IsRare)
end

function LuaFSCCard:Init(cardId, isRare, status, borrowed)
    if self.m_Id == cardId and self.m_IsRare == isRare and self.m_Status == status then
        return 
    end
    self:SetActive(false)
    self.m_Id = cardId or 0
    self.m_IsRare = isRare
    self.m_Status = status
    self.m_Borrowed = borrowed
    self.m_Data = LuaFourSeasonCardMgr:GetCardData(cardId) or {}

    if borrowed then
        self.transform:Find("Card/Temp").gameObject:SetActive(true)
    end

    if self.m_Status == EnumFSC_CardStatus.Empty then
        self:InitEmpty()
    elseif self.m_Status == EnumFSC_CardStatus.Back then
        self:InitBack()
    elseif self.m_Status == EnumFSC_CardStatus.Detail then
        self:InitDetail()
    elseif self.m_Status == EnumFSC_CardStatus.NotInEffect then
        self:InitNotInEffect()
    elseif self.m_Status == EnumFSC_CardStatus.Banned then
        self:InitBanned()
    elseif self.m_Status == EnumFSC_CardStatus.Copy then
        self:InitCopy()
    else
        self:InitCard()
    end
end

function LuaFSCCard:InitCard()
    self:SetBg()
    self.m_NameLabel.gameObject:SetActive(true)
    self.m_RoleTex.gameObject:SetActive(true)

    if CommonDefs.IS_VN_CLIENT then
        self.m_NameLabel.text = (self.m_Data.Title):gsub(' ', '\n')
    else
        self.m_NameLabel.text = self.m_Data.Title
    end

    self.m_NameLabel.text = self.m_Data.Title
    self.m_NameLabel.color = LuaFourSeasonCardMgr:GetSeasonColor(self.m_Data.Season)
    if self.m_Id > 0 then
        self.m_RoleTex:LoadMaterial(LuaFourSeasonCardMgr:GetCardData(self.m_Id).BigPicture)
    end
end

function LuaFSCCard:InitEmpty()     
    self.m_EmptyHint.gameObject:SetActive(true)

    local hintLabel = self.m_EmptyHint:Find("Hint"):GetComponent(typeof(UILabel))
    local hint2 = self.m_EmptyHint:Find("Hint (2)").gameObject
    if LuaFSCPrepareRareCardWnd.s_CountDownEndTime > 0 and self.m_Data.Title then
        hint2:SetActive(false)
        hintLabel.gameObject:SetActive(true)
        hintLabel.text = LocalString.GetString("战胜")..self.m_Data.Title
    else
        hint2:SetActive(true)
        hintLabel.gameObject:SetActive(false)
    end
end

function LuaFSCCard:InitBack()
    self:SetBg()
end

function LuaFSCCard:InitDetail()
    self:InitCard()
    self.m_BaseScore.gameObject:SetActive(true)
    self.m_BaseScore.text = self.m_Data.Value or "0"
end

function LuaFSCCard:InitNotInEffect()
    self:InitCard()
    self:Mask(true)
    self.transform:Find("Card/NotEffect").gameObject:SetActive(true)
end

function LuaFSCCard:InitBanned()
    self:InitCard()
    self:Mask(true)
    self.transform:Find("Card/BottomTex").gameObject:SetActive(true)
    self.transform:Find("Card/Banned").gameObject:SetActive(true)
end

function LuaFSCCard:InitCopy()
    self:InitCard()
    self.transform:Find("Card/BottomTex").gameObject:SetActive(true)
    self.transform:Find("Card/Copy").gameObject:SetActive(true)
end

function LuaFSCCard:InitBorrowed()
    self:InitCard()
end

function LuaFSCCard:CheckClickCard()
    if self:IsMy("HandCards") then
        --if CommonDefs.IsPCGameMode() then
            --self.m_IsSelected = not self.m_IsSelected
            --self:PopUp(self.m_IsSelected)
            g_ScriptEvent:BroadcastInLua("FSC_SelectCard", not self.m_IsSelected and self.m_Id or 0)
        --end
    elseif self:IsOther("HandCards") then
        
    elseif self:IsPublic() and (self.m_SelectedId == 0 or LuaFourSeasonCardMgr:CheckPlayCard()) then
        g_ScriptEvent:BroadcastInLua("FSC_ChooseBoardCard", self.m_Id)
        if LuaFourSeasonCardMgr.WaitForNxtGuide and LuaFourSeasonCardMgr.GuidingStage == 2 or LuaFourSeasonCardMgr.GuidingStage == 4 then
            LuaFourSeasonCardMgr.WaitForNxtGuide = false
            Gac2Gas.Anniv2023FSC_IncGuidingStage()
            LuaFourSeasonCardMgr.GuidingStage = LuaFourSeasonCardMgr.GuidingStage + 1
            if LuaFourSeasonCardMgr.GuidingStage ~= 3 then
                LuaFourSeasonCardMgr:TryTriggerGuide()
            end
        end
    end
end

function LuaFSCCard:OnClick()
    --Debug.LogError("OnClick")
    if LuaFourSeasonCardMgr.EnableOpInGuide == false then return end

    if self:IsInRareCardWnd() then
        LuaFSCRareCardInfoWnd.s_CardId = self.m_Id
        LuaFSCRareCardInfoWnd.s_CardStatus = self.m_Status
        LuaFSCRareCardInfoWnd.s_Borrowed = self.m_Borrowed
        CUIManager.ShowUI(CLuaUIResources.FSCRareCardInfoWnd)
        return
    elseif self:IsInCardPileWnd() then
        LuaFSCCardDetailWnd.s_CardId = self.m_Id
        LuaFSCCardDetailWnd.s_IsRare = self.m_IsRare
        LuaFSCCardDetailWnd.s_Status = self.m_Status
        LuaFSCCardDetailWnd.s_Borrowed = self.m_Borrowed
        CUIManager.ShowUI(CLuaUIResources.FSCCardDetailWnd)
        return
    end

    if not LuaFourSeasonCardMgr.EnableOperation and not LuaFourSeasonCardMgr.EnableOpInGuide then return end

    if self:IsMy("CardPile") then
        SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.pile, Vector3.zero, nil, 0)
        LuaFSCCardPileWnd.s_Side = 1
        CUIManager.ShowUI(CLuaUIResources.FSCCardPileWnd)
        return
    elseif self:IsOther("CardPile") then
        SoundManager.Inst:PlayOneShot(LuaFourSeasonCardMgr.Music.pile, Vector3.zero, nil, 0)
        LuaFSCCardPileWnd.s_Side = 2
        CUIManager.ShowUI(CLuaUIResources.FSCCardPileWnd)
        return
    end

    if not LuaFourSeasonCardMgr.EnableOperation or not LuaFourSeasonCardMgr.bIsOnMove then return end

    self:CheckClickCard()
end

function LuaFSCCard:OnPress(isPressed)
    --Debug.LogError("OnPress:"..(isPressed and 1 or 0))
    if LuaFourSeasonCardMgr.EnableOpInGuide == false then return end

    if not LuaFourSeasonCardMgr.EnableOperation then return end

    if CommonDefs.IsInMobileDevice() then 
        if self:IsMy("HandCards") then
            self:ShowTopDetail(isPressed)
        elseif self:IsPublic() and (self.m_SelectedId == 0 or LuaFourSeasonCardMgr:CheckPlayCard()) then
            self:ShowTopDetail(isPressed)
        end
    end

    if not LuaFourSeasonCardMgr.bIsOnMove then return end
end

function LuaFSCCard:OnHover(isOver)
    if LuaFourSeasonCardMgr.EnableOpInGuide == false then return end

    if not LuaFourSeasonCardMgr.EnableOperation then return end

    if CommonDefs.IsPCGameMode() then 
        if self:IsMy("HandCards") then
            self:ShowTopDetail(isOver)
        elseif self:IsPublic() and (self.m_SelectedId == 0 or LuaFourSeasonCardMgr:CheckPlayCard()) then
            self:ShowTopDetail(isOver)
        end
    end
    
    if not LuaFourSeasonCardMgr.bIsOnMove then return end
end

function LuaFSCCard:OnSelect(isSelected)
    if LuaFourSeasonCardMgr.EnableOpInGuide == false then return end

    if not LuaFourSeasonCardMgr.EnableOperation then return end
    if not LuaFourSeasonCardMgr.bIsOnMove then return end
end

function LuaFSCCard:DragAt()
    local cam = UICamera.currentCamera
    local hits = Physics.RaycastAll(cam:ScreenPointToRay(Input.mousePosition), cam.farClipPlane, cam.cullingMask)
    if hits then
        local cards = {}
        for i = 0, hits.Length - 1 do
            local hitData = hits[i]
            local name = hitData.collider.gameObject.name
            if StringStartWith(name, "FSCCard") or StringStartWith(name, "fsccard")  then 
                local panel = hitData.collider.transform:GetComponent(typeof(UIPanel))
                if panel then
                    table.insert(cards, panel)
                end
            end
        end
        table.sort(cards, function(a, b)
            return a.depth > b.depth
        end)
        if #cards > 0 then
            return cards[1].gameObject
        end
    end
end

LuaFSCCard.s_StartAt = nil
function LuaFSCCard:OnDragStart(go)
    --Debug.LogError("OnDragStart")
    LuaFSCCard.s_StartAt = go
end

function LuaFSCCard:OnDragEnd(go)
    --Debug.LogError("OnDragEnd")
    if LuaFSCCard.s_DragAt then
        local script = LuaFSCCard.s_DragAt:GetComponent(typeof(CCommonLuaScript))  
        if script then
            script:Awake()
            script = script.m_LuaSelf
            --Debug.LogError("OnPress(false)")
            script:OnPress(false)
        end
    end
    local card = self:DragAt()
    if card then
        if card == LuaFSCCard.s_StartAt then
            local script = card:GetComponent(typeof(CCommonLuaScript))  
            if script then
                script:Awake()
                script = script.m_LuaSelf
                script:OnClick()
            end
        end
    end
    LuaFSCCard.s_DragAt = nil
end

LuaFSCCard.s_DragAt = nil
function LuaFSCCard:OnDrag(go, delta)
    local card = self:DragAt()
    if card then
        if card ~= LuaFSCCard.s_DragAt then
            LuaFSCCard.s_StartAt = nil
            local script
            if LuaFSCCard.s_DragAt then
                script = LuaFSCCard.s_DragAt:GetComponent(typeof(CCommonLuaScript))  
                if script then
                    script:Awake()
                    script = script.m_LuaSelf
                    script:OnPress(false)
                end
            end
            script = card:GetComponent(typeof(CCommonLuaScript))  
            if script then
                script:Awake()
                script = script.m_LuaSelf
                --Debug.LogError("OnPress(true)")
                script:OnPress(true)
            end
        end
    else
        LuaFSCCard.s_StartAt = nil
        if LuaFSCCard.s_DragAt then
            local script = LuaFSCCard.s_DragAt:GetComponent(typeof(CCommonLuaScript))  
            if script then
                script:Awake()
                script = script.m_LuaSelf
                script:OnPress(false)
            end
        end
    end
    LuaFSCCard.s_DragAt = card
end
