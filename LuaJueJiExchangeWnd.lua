require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CCommonItemSelectInfoMgr = import "L10.UI.CCommonItemSelectInfoMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaJueJiExchangeWnd = class()
RegistClassMember(LuaJueJiExchangeWnd,"m_TitleLabel")
RegistClassMember(LuaJueJiExchangeWnd,"m_CloseButton")

RegistClassMember(LuaJueJiExchangeWnd,"m_ExchangeWayTemplate")
RegistClassMember(LuaJueJiExchangeWnd,"m_ExchangeWayTable")

RegistClassMember(LuaJueJiExchangeWnd,"m_ExchangeWayButtons")
RegistClassMember(LuaJueJiExchangeWnd,"m_ExchangeIds")
RegistClassMember(LuaJueJiExchangeWnd,"m_CurExchangeId") --当前兑换公式编号

RegistClassMember(LuaJueJiExchangeWnd,"m_LayoutTable")
--遗忘的70绝技道具/70绝技道具
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi70ItemGo")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi70AddIconGo")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi70Icon")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi70NameLabel")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi70LinkGo")
--遗忘的100绝技道具/100绝技道具
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi100ItemGo")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi100AddIconGo")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi100Icon")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi100NameLabel")
RegistClassMember(LuaJueJiExchangeWnd,"m_JueJi100LinkGo")
--九变天书道具
RegistClassMember(LuaJueJiExchangeWnd,"m_JiuBianItemGo")
RegistClassMember(LuaJueJiExchangeWnd,"m_JiuBianDisabledGo")
RegistClassMember(LuaJueJiExchangeWnd,"m_JiuBianIcon")
RegistClassMember(LuaJueJiExchangeWnd,"m_JiuBianOwnNumLabel")
RegistClassMember(LuaJueJiExchangeWnd,"m_JiuBianNeedNumLabel")
--兑换的道具
RegistClassMember(LuaJueJiExchangeWnd,"m_TargetItemGo")
RegistClassMember(LuaJueJiExchangeWnd,"m_TargetIcon")
RegistClassMember(LuaJueJiExchangeWnd,"m_TargetNameLabel")

RegistClassMember(LuaJueJiExchangeWnd,"m_ExchangeButton")

RegistClassMember(LuaJueJiExchangeWnd,"m_YellowColor")
RegistClassMember(LuaJueJiExchangeWnd,"m_TemplateIdOf70ForgetJueJi")
RegistClassMember(LuaJueJiExchangeWnd,"m_TemplateIdOf100ForgetJueJi")
RegistClassMember(LuaJueJiExchangeWnd,"m_TemplateIdOf70JueJi")
RegistClassMember(LuaJueJiExchangeWnd,"m_TemplateIdOf100JueJi")

function LuaJueJiExchangeWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaJueJiExchangeWnd:Init()
	self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_CloseButton = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject

	self.m_ExchangeWayTemplate = self.transform:Find("Anchor/MasterView/ExchangeWayTemplate").gameObject
	self.m_ExchangeWayTable = self.transform:Find("Anchor/MasterView/ExchangeWayTable"):GetComponent(typeof(UITable))

	self.m_ExchangeWayTemplate:SetActive(false)

	local detailViewTableTrans = self.transform:Find("Anchor/DetailView/Table")
	self.m_LayoutTable = detailViewTableTrans:GetComponent(typeof(UITable)) 
	--遗忘的70绝技道具/70绝技道具
	self.m_JueJi70ItemGo = detailViewTableTrans:Find("JueJi70").gameObject
	self.m_JueJi70AddIconGo = detailViewTableTrans:Find("JueJi70/AddIcon").gameObject
	self.m_JueJi70Icon = detailViewTableTrans:Find("JueJi70/Icon"):GetComponent(typeof(CUITexture))
	self.m_JueJi70NameLabel = detailViewTableTrans:Find("JueJi70/Label"):GetComponent(typeof(UILabel))
	self.m_JueJi70LinkGo = detailViewTableTrans:Find("LinkIcon70").gameObject
	--遗忘的100绝技道具/100绝技道具
	self.m_JueJi100ItemGo = detailViewTableTrans:Find("JueJi100").gameObject
	self.m_JueJi100AddIconGo = detailViewTableTrans:Find("JueJi100/AddIcon").gameObject
	self.m_JueJi100Icon = detailViewTableTrans:Find("JueJi100/Icon"):GetComponent(typeof(CUITexture))
	self.m_JueJi100NameLabel = detailViewTableTrans:Find("JueJi100/Label"):GetComponent(typeof(UILabel))
	self.m_JueJi100LinkGo = detailViewTableTrans:Find("LinkIcon100").gameObject
	--九变天书道具
	self.m_JiuBianItemGo = detailViewTableTrans:Find("JiuBian").gameObject
	self.m_JiuBianDisabledGo = detailViewTableTrans:Find("JiuBian/DisableSprite").gameObject
	self.m_JiuBianIcon = detailViewTableTrans:Find("JiuBian/Icon"):GetComponent(typeof(CUITexture))
	self.m_JiuBianOwnNumLabel = detailViewTableTrans:Find("JiuBian/NumLabel"):GetComponent(typeof(UILabel))
	self.m_JiuBianNeedNumLabel = detailViewTableTrans:Find("JiuBian/Label"):GetComponent(typeof(UILabel))
	--兑换的道具
	self.m_TargetItemGo = self.transform:Find("Anchor/DetailView/NewItem").gameObject
	self.m_TargetIcon = self.transform:Find("Anchor/DetailView/NewItem/Icon"):GetComponent(typeof(CUITexture))
	self.m_TargetNameLabel = self.transform:Find("Anchor/DetailView/NewItem/Label"):GetComponent(typeof(UILabel))

	self.m_ExchangeButton = self.transform:Find("Anchor/DetailView/ExchangeButton"):GetComponent(typeof(CButton))

    self.m_YellowColor = "FFE400"
    self.m_TemplateIdOf70ForgetJueJi = 0
    self.m_TemplateIdOf100ForgetJueJi = 0
    self.m_TemplateIdOf70JueJi = 0
    self.m_TemplateIdOf100JueJi = 0

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	CommonDefs.AddOnClickListener(self.m_JueJi70ItemGo, DelegateFactory.Action_GameObject(function(go) self:OnJueJi70ItemClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_JueJi100ItemGo, DelegateFactory.Action_GameObject(function(go) self:OnJueJi100ItemClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_JiuBianItemGo, DelegateFactory.Action_GameObject(function(go) self:OnJiuBianTianShuItemClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_TargetItemGo, DelegateFactory.Action_GameObject(function(go) self:OnTargetItemClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_ExchangeButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnExchangeButtonClick(go) end), false)

    self.m_TitleLabel.text = LuaProfessionTransferMgr.JueJiExchangeTitle
    self:LoadExchangeIds(LuaProfessionTransferMgr.JueJiExchangeType, LuaProfessionTransferMgr.TargetJueJiLevel, LuaProfessionTransferMgr.SourceSkillBookTemplateId)    
end

function LuaJueJiExchangeWnd:LoadExchangeIds(exchangeType, targetJueJiLevel, sourceSkillBookTemplateId)
	self.m_ExchangeIds = {}

    local show70SkillBookExchangeFormula = true
    if sourceSkillBookTemplateId>0 then
        if LuaProfessionTransferMgr.Is70JueJiSkillBook(sourceSkillBookTemplateId) then
            show70SkillBookExchangeFormula = true
        elseif LuaProfessionTransferMgr.Is100JueJiSkillBook(sourceSkillBookTemplateId) then
            show70SkillBookExchangeFormula = false
        end
    end

	ProfessionTransfer_JueJiItemExchange.Foreach(function (key, data)
        if data.Type == exchangeType then
            if exchangeType == 0 then --type为0时结合juejilevel字段来处理兑换逻辑
    	       if data.JueJiLevel == targetJueJiLevel then
                    table.insert(self.m_ExchangeIds, key)
    	       end
        	elseif exchangeType == 1 then
                if show70SkillBookExchangeFormula and data.NumOf70JueJi>0 then
                    table.insert(self.m_ExchangeIds, key)
                elseif not show70SkillBookExchangeFormula and data.NumOf100JueJi>0 then
                    table.insert(self.m_ExchangeIds, key)
                end
        	end
        end
    end)
	self.m_ExchangeWayButtons = {}
	Extensions.RemoveAllChildren(self.m_ExchangeWayTable.transform)
    for id,key in pairs(self.m_ExchangeIds) do
    	local instance = CUICommonDef.AddChild(self.m_ExchangeWayTable.gameObject, self.m_ExchangeWayTemplate)
    	instance:SetActive(true)
    	instance:GetComponent(typeof(CButton)).Text = SafeStringFormat3(LocalString.GetString("兑换方式%s"), CUICommonDef.IntToChinese(id))
    	CommonDefs.AddOnClickListener(instance, DelegateFactory.Action_GameObject(function(go) self:OnExchangeWayButtonClick(go, key) end), false)
    	table.insert(self.m_ExchangeWayButtons, instance)
    end

    if #self.m_ExchangeIds>0 then
    	self:OnExchangeWayButtonClick(self.m_ExchangeWayButtons[1], self.m_ExchangeIds[1])
    end
end

function LuaJueJiExchangeWnd:OnExchangeWayButtonClick(go, exchangeId)
	for __, buttonGo in pairs(self.m_ExchangeWayButtons) do
		buttonGo:GetComponent(typeof(CButton)).Selected = (buttonGo == go)
	end

	self:ShowExchangeContent(exchangeId)
end

function LuaJueJiExchangeWnd:ShowExchangeContent(exchangeId)
	local data = ProfessionTransfer_JueJiItemExchange.GetData(exchangeId)
    if data == nil then
        return
    end
    self.m_CurExchangeId = exchangeId
    --需要注意有限制条件：遗忘的技能书不能和绝技技能书同时出现在公式里面
    self.m_TemplateIdOf70ForgetJueJi = 0
    self.m_TemplateIdOf100ForgetJueJi = 0
    self.m_TemplateIdOf70JueJi = 0
    self.m_TemplateIdOf100JueJi = 0
    local sourceSkillBookTemplateId = LuaProfessionTransferMgr.SourceSkillBookTemplateId
    -- 70绝技与遗忘的70绝技不能同时出现
    if data.NumOf70JueJi > 0 then

        local item = Item_Item.GetData(sourceSkillBookTemplateId)
        self.m_JueJi70ItemGo:SetActive(true)
        self.m_JueJi70AddIconGo:SetActive(false)
        self.m_JueJi70Icon:LoadMaterial(item.Icon)
        self.m_JueJi70LinkGo:SetActive(true)
        self.m_JueJi70NameLabel.text = item.Name
        self.m_TemplateIdOf70JueJi = sourceSkillBookTemplateId --默认记录下来
    elseif data.NumOf70ForgetJueJi > 0 then

    	self.m_JueJi70ItemGo:SetActive(true)
        self.m_JueJi70AddIconGo:SetActive(true)
        self.m_JueJi70Icon:Clear()
        self.m_JueJi70LinkGo:SetActive(true)
        self.m_JueJi70NameLabel.text = System.String.Format(LocalString.GetString("遗忘的[c][{0}]70级[-][/c]绝技"), self.m_YellowColor)
    else
        self.m_JueJi70ItemGo:SetActive(false)
        self.m_JueJi70LinkGo:SetActive(false)
    end

    -- 100绝技与遗忘的100绝技不能同时出现
    if data.NumOf100JueJi > 0 then

        local item = Item_Item.GetData(sourceSkillBookTemplateId)
        self.m_JueJi100ItemGo:SetActive(true)
        self.m_JueJi100AddIconGo:SetActive(false)
        self.m_JueJi100Icon:LoadMaterial(item.Icon)
        self.m_JueJi100LinkGo:SetActive(true)
        self.m_JueJi100NameLabel.text = item.Name
        self.m_TemplateIdOf100JueJi = sourceSkillBookTemplateId --默认记录下来
    elseif data.NumOf100ForgetJueJi > 0 then

    	self.m_JueJi100ItemGo:SetActive(true)
        self.m_JueJi100AddIconGo:SetActive(true)
        self.m_JueJi100Icon:Clear()
        self.m_JueJi100LinkGo:SetActive(true)
        self.m_JueJi100NameLabel.text = System.String.Format(LocalString.GetString("遗忘的[c][{0}]100级[-][/c]绝技"), self.m_YellowColor)
    else
        self.m_JueJi100ItemGo:SetActive(false)
        self.m_JueJi100LinkGo:SetActive(false)
    end


    if data.NumOfJiuBian > 0 then
        self.m_JiuBianItemGo:SetActive(true)
        local jiubianOwnNum = CItemMgr.Inst:GetItemCount(ProfessionTransfer_Setting.GetData().JiuBianTianShuItemTempId)
        local jiubianNeedNum = data.NumOfJiuBian
        self.m_JiuBianDisabledGo:SetActive(jiubianOwnNum < jiubianNeedNum)
        self.m_JiuBianOwnNumLabel.text = System.String.Format("{0}{1}[-]/{2}", jiubianOwnNum < jiubianNeedNum and "[ff0000]" or "[ffffff]", jiubianOwnNum, jiubianNeedNum)
        self.m_JiuBianNeedNumLabel.text = System.String.Format(LocalString.GetString("{0}本九变天书"), jiubianNeedNum)
        local item = Item_Item.GetData(ProfessionTransfer_Setting.GetData().JiuBianTianShuItemTempId)
        if item ~= nil then
            self.m_JiuBianIcon:LoadMaterial(item.Icon)
        end
    else
        self.m_JiuBianItemGo:SetActive(false)
    end

    local targetItem = Item_Item.GetData(data.ItemId)
    self.m_TargetIcon:LoadMaterial(targetItem.Icon)
    self.m_TargetNameLabel.text = System.String.Format(LocalString.GetString("{0}(随机一本[c][{1}]{2}级[-][/c]绝技)"), targetItem.Name, self.m_YellowColor, data.JueJiLevel)

    self.m_LayoutTable:Reposition()
end

function LuaJueJiExchangeWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
	g_ScriptEvent:AddListener("OnExchangeNewJueJiItemSuccess", self, "OnExchangeNewJueJiItemSuccess")
end

function LuaJueJiExchangeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
	g_ScriptEvent:RemoveListener("OnExchangeNewJueJiItemSuccess", self, "OnExchangeNewJueJiItemSuccess")
end

function LuaJueJiExchangeWnd:OnSendItem(args)
	local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)

	if not item then return end

	if item.TemplateId == ProfessionTransfer_Setting.GetData().JiuBianTianShuItemTempId then
		self:RefreshJiuBianTianShu()
	end
end

function LuaJueJiExchangeWnd:OnSetItemAt(args)
	local place = args[0]
	local pos = args[1]
	local oldItemId = args[2]
	local newItemId = args[3]

	if place ~= EnumItemPlace.Bag then return end

	self:RefreshJiuBianTianShu()
end

function LuaJueJiExchangeWnd:OnExchangeNewJueJiItemSuccess(exchangeId, itemTempId70Forget, itemTempId100Forget, itemTempId70, itemTempId100)
    if self.m_CurExchangeId == exchangeId then
        local data = ProfessionTransfer_JueJiItemExchange.GetData(exchangeId)
        if data == nil then
            self:Close()
            return
        end
        if data.NumOf70ForgetJueJi > 0  then
            self:OnSelectForget70Item(0)
        end
        if data.NumOf100ForgetJueJi > 0 then
            self:OnSelectForget100Item(0)
        end
        if data.NumOf70JueJi>0 or data.NumOf100JueJi>0 then
            self:Close()
        end
    end
end

function LuaJueJiExchangeWnd:RefreshJiuBianTianShu()
	local data = ProfessionTransfer_JueJiItemExchange.GetData(self.m_CurExchangeId)
    if data == nil then
        return
    end

    if data.NumOfJiuBian > 0 then
        self.m_JiuBianItemGo:SetActive(true)
        local jiubianOwnNum = CItemMgr.Inst:GetItemCount(ProfessionTransfer_Setting.GetData().JiuBianTianShuItemTempId)
        local jiubianNeedNum = data.NumOfJiuBian
        self.m_JiuBianDisabledGo:SetActive(jiubianOwnNum < jiubianNeedNum)
        self.m_JiuBianOwnNumLabel.text = System.String.Format("{0}{1}[-]/{2}", jiubianOwnNum < jiubianNeedNum and "[ff0000]" or "[ffffff]", jiubianOwnNum, jiubianNeedNum)
        self.m_JiuBianNeedNumLabel.text = System.String.Format(LocalString.GetString("{0}本九变天书"), jiubianNeedNum)
    end
end

function LuaJueJiExchangeWnd:OnJueJi70ItemClick(go)
	local data = ProfessionTransfer_JueJiItemExchange.GetData(self.m_CurExchangeId)
    if data == nil then
        return
    end
    if data.NumOf70JueJi>0 then
    	--Do Nothing 从包裹选择的道具这里不允许切换其他选择
    elseif data.NumOf70ForgetJueJi>0 then

		CCommonItemSelectInfoMgr.Inst:ShowJueJiExchangeItemSelectWnd(LocalString.GetString("选择遗忘的70绝技"), ProfessionTransfer_Setting.GetData()._70ForgetSkillItemId, DelegateFactory.Action_string_uint(function (itemId, templateId) 
	        self:OnSelectForget70Item(templateId)
	    end))
	end
end

function LuaJueJiExchangeWnd:OnJueJi100ItemClick(go)
	local data = ProfessionTransfer_JueJiItemExchange.GetData(self.m_CurExchangeId)
    if data == nil then
        return
    end
    if data.NumOf100JueJi>0 then
    	--Do Nothing 从包裹选择的道具这里不允许切换其他选择
    elseif data.NumOf100ForgetJueJi>0 then

		CCommonItemSelectInfoMgr.Inst:ShowJueJiExchangeItemSelectWnd(LocalString.GetString("选择遗忘的100绝技"), ProfessionTransfer_Setting.GetData()._100ForgetSkillItemId, DelegateFactory.Action_string_uint(function (itemId, templateId) 
	        self:OnSelectForget100Item(templateId)
	    end))
	end
end

function LuaJueJiExchangeWnd:OnSelectForget70Item(templateId)
    self.m_TemplateIdOf70ForgetJueJi = templateId
    local item = Item_Item.GetData(templateId)
    if item ~= nil then
        self.m_JueJi70Icon:LoadMaterial(item.Icon)
        self.m_JueJi70AddIconGo:SetActive(false)
        self.m_JueJi70NameLabel.text = item.Name
    else
        self.m_JueJi70Icon:Clear()
        self.m_JueJi70AddIconGo:SetActive(true)
        self.m_JueJi70NameLabel.text = System.String.Format(LocalString.GetString("遗忘的[c][{0}]70级[-][/c]绝技"), self.m_YellowColor)
    end
end

function LuaJueJiExchangeWnd:OnSelectForget100Item(templateId)
    self.m_TemplateIdOf100ForgetJueJi = templateId
    local item = Item_Item.GetData(templateId)
    if item ~= nil then
        self.m_JueJi100Icon:LoadMaterial(item.Icon)
        self.m_JueJi100AddIconGo:SetActive(false)
        self.m_JueJi100NameLabel.text = item.Name
    else
        self.m_JueJi100Icon:Clear()
        self.m_JueJi100AddIconGo:SetActive(true)
        self.m_JueJi100NameLabel.text = System.String.Format(LocalString.GetString("遗忘的[c][{0}]100级[-][/c]绝技"), self.m_YellowColor)
    end
end

function LuaJueJiExchangeWnd:OnJiuBianTianShuItemClick(go)
	local data = ProfessionTransfer_JueJiItemExchange.GetData(self.m_CurExchangeId)
    if data == nil or CClientMainPlayer.Inst == nil then
        return
    end
    local templateId = ProfessionTransfer_Setting.GetData().JiuBianTianShuItemTempId
    local jiubianOwnNum = CItemMgr.Inst:GetItemCount(templateId)
    local jiubianNeedNum = data.NumOfJiuBian
    CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, jiubianOwnNum >= jiubianNeedNum, go.transform, CTooltipAlignType.Bottom)
end

function LuaJueJiExchangeWnd:OnTargetItemClick(go)
    local data = ProfessionTransfer_JueJiItemExchange.GetData(self.m_CurExchangeId)
    if not data then return end
	CItemInfoMgr.ShowLinkItemTemplateInfo(data.ItemId)
end

function LuaJueJiExchangeWnd:OnExchangeButtonClick(go)
	local data = ProfessionTransfer_JueJiItemExchange.GetData(self.m_CurExchangeId)
    if data == nil then
        return
    end

    if data.NumOf70JueJi > 0 then
        if self.m_TemplateIdOf70JueJi == 0 or CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_TemplateIdOf70JueJi) < data.NumOf70JueJi then
            g_MessageMgr:ShowMessage("Need_70_JueJi_To_Exchange_New_JueJi")
            return
        end
    end

    if data.NumOf100JueJi > 0 then
        if self.m_TemplateIdOf100JueJi == 0 or CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_TemplateIdOf100JueJi) < data.NumOf100JueJi then
            g_MessageMgr:ShowMessage("Need_100_JueJi_ToExchange_New_JueJi")
            return
        end
    end

    if data.NumOf70ForgetJueJi > 0 then
        if self.m_TemplateIdOf70ForgetJueJi == 0 or CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_TemplateIdOf70ForgetJueJi) < data.NumOf70ForgetJueJi then
            g_MessageMgr:ShowMessage("Need_Oblivious_70_JueJi")
            return
        end
    end

    if data.NumOf100ForgetJueJi > 0 then
        if self.m_TemplateIdOf100ForgetJueJi == 0 or CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_TemplateIdOf100ForgetJueJi) < data.NumOf100ForgetJueJi then
            g_MessageMgr:ShowMessage("Need_Oblivious_100_JueJi")
            return
        end
    end


    if data.NumOfJiuBian > 0 then
        local templateId = ProfessionTransfer_Setting.GetData().JiuBianTianShuItemTempId
        local jiubianOwnNum = CItemMgr.Inst:GetItemCount(templateId)
        local jiubianNeedNum = data.NumOfJiuBian
        if jiubianOwnNum < jiubianNeedNum then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, jiubianOwnNum >= jiubianNeedNum, self.m_JiuBianItemGo.transform, CTooltipAlignType.Bottom)
            return
        end
    end

    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Confirm_Exchange_New_JueJi", Item_Item.GetData(data.ItemId).Name, data.JueJiLevel), DelegateFactory.Action(function () 
        LuaProfessionTransferMgr.RequestExchangeNewJueJiItem(self.m_CurExchangeId, self.m_TemplateIdOf70ForgetJueJi, self.m_TemplateIdOf100ForgetJueJi, self.m_TemplateIdOf70JueJi, self.m_TemplateIdOf100JueJi)
    end), nil, nil, nil, false)
end
