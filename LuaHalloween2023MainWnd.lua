local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnTableItem = import "L10.UI.QnTableItem"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
LuaHalloween2023MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHalloween2023MainWnd, "ActBtn1", "ActBtn1", GameObject)
RegistChildComponent(LuaHalloween2023MainWnd, "ActBtn2", "ActBtn2", GameObject)
RegistChildComponent(LuaHalloween2023MainWnd, "ActBtn3", "ActBtn3", GameObject)
RegistChildComponent(LuaHalloween2023MainWnd, "ActBtn4", "ActBtn4", GameObject)
RegistChildComponent(LuaHalloween2023MainWnd, "ActBtn5", "ActBtn5", GameObject)
RegistChildComponent(LuaHalloween2023MainWnd, "SignBtn", "SignBtn", GameObject)
RegistChildComponent(LuaHalloween2023MainWnd, "TimeLb", "TimeLb", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHalloween2023MainWnd, "m_TrafficLightAlert")
RegistClassMember(LuaHalloween2023MainWnd, "m_CandyOrderItemList")
function LuaHalloween2023MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.ActBtn1.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn1Click()
        end
    )

    UIEventListener.Get(self.ActBtn2.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn2Click()
        end
    )

    UIEventListener.Get(self.ActBtn3.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn3Click()
        end
    )

    UIEventListener.Get(self.ActBtn4.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn4Click()
        end
    )

    UIEventListener.Get(self.ActBtn5.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnActBtn5Click()
        end
    )

    UIEventListener.Get(self.SignBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnSignBtnClick()
        end
    )
    --@endregion EventBind end
end


--@region 事件

--@endregion

function LuaHalloween2023MainWnd:Init()
	self.TimeLb.text = Halloween2023_Setting.GetData().OpenTimeString
    self.m_TrafficLightAlert = self.ActBtn3.transform:Find("Alert").gameObject
    self.m_OrderBattleAlert = self.ActBtn4.transform:Find("Alert").gameObject
    self:InitSignBtn() --签到按钮
    self:InitActBtn1() --糖果订单
    self:InitActBtn2() --红包封面
    self:InitActBtn3() --不动如山
    self:InitActBtn4() --抢单大战
    self:InitActBtn5() --日常奖励
end

function LuaHalloween2023MainWnd:InitActBtn5()
    self.ActBtn5.transform:Find("open").gameObject:SetActive(true)
end

function LuaHalloween2023MainWnd:InitActBtn2()
    local alert = FindChild(self.ActBtn2.transform,"Alert")
    alert.gameObject:SetActive(LuaHalloween2022Mgr.HaveAcceptableHongBaoCover())
    self.ActBtn2.transform:Find("open").gameObject:SetActive(true)
    self.ActBtn2.transform:Find("StateLb"):GetComponent(typeof(UILabel)).text = LocalString.GetString("解锁幻灵夜红包封面")
end

function LuaHalloween2023MainWnd:InitActBtn3()
    local timeStr = Halloween2023_Setting.GetData().TrafficPlayTimeString
    self.m_TrafficLightAlert:SetActive(LuaActivityRedDotMgr:IsRedDot(64))
	local statelb = FindChildWithType(self.ActBtn3.transform,"StateLb",typeof(UILabel))
    local timeStrs = g_LuaUtil:StrSplitAdv(timeStr, " ")
	statelb.text = string.match(timeStr,"(%d+:%d+-%d+:%d+)")
    local isOpen = LuaHalloween2023Mgr:IsGameOpen(timeStr)
    self.ActBtn3.transform:Find("open").gameObject:SetActive(isOpen)
end

function LuaHalloween2023MainWnd:InitActBtn4()
	local statelb = FindChildWithType(self.ActBtn4.transform,"StateLb",typeof(UILabel))
    local timeStrs = Halloween2023_Setting.GetData().CandyDeliveryPlayTime
    statelb.text = SafeStringFormat3("%s\n%s", timeStrs[0], timeStrs[1])
    self.m_OrderBattleAlert:SetActive(LuaActivityRedDotMgr:IsRedDot(65))
    local isOpen = false
    for i = 0, timeStrs.Length - 1 do
        isOpen = isOpen or LuaHalloween2023Mgr:IsGameOpen(timeStrs[i])
    end
    self.ActBtn4.transform:Find("Bg/Motorcycle_small/Motorcycle_small (1)").gameObject:SetActive(not isOpen)
    self.ActBtn4.transform:Find("Bg/Motorcycle_small/Motorcycle_small_open").gameObject:SetActive(isOpen)
    self.ActBtn4.transform:Find("open").gameObject:SetActive(isOpen)
end

function LuaHalloween2023MainWnd:InitActBtn1()
    local orderInfos = Halloween2023_Setting.GetData().TaskShowString
    local pool = self.ActBtn1.transform:Find("Pool").gameObject
    self.m_CandyOrderItemList = {}
    self.isMysterOrderOpen = true
    for i = 0, pool.transform.childCount - 1 do
        local orderInfo = orderInfos[i]
        local child = pool.transform:GetChild(i)
        local item = child:GetComponent(typeof(QnTableItem))
        item.transform:Find("Label"):GetComponent(typeof(UILabel)).text = orderInfo[0]
        local iconTemplate = item.transform:Find("Icon"):GetComponent(typeof(CQnReturnAwardTemplate))
        iconTemplate:Init(CItemMgr.Inst:GetItemTemplate(tonumber(orderInfo[1])), 1)
        local isfinish = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(tonumber(orderInfo[2]))
        item.transform:Find("Finish").gameObject:SetActive(isfinish)
        if i < pool.transform.childCount - 1 then 
            self.isMysterOrderOpen = self.isMysterOrderOpen and isfinish
        else
            iconTemplate.gameObject:SetActive(self.isMysterOrderOpen)
            item.transform:Find("Lock").gameObject:SetActive(not self.isMysterOrderOpen)
        end
        item:SetSelected(false)
        table.insert(self.m_CandyOrderItemList, {item = item, mystery = i == pool.transform.childCount - 1, isFinish = isfinish})
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate( function(go)
            self:OnOrderItemClick(i + 1)
        end)
    end
    self.ActBtn1.transform:Find("open").gameObject:SetActive(true)
end

--@endregion

--@region 签到

function LuaHalloween2023MainWnd:InitSignBtn()
    self.SignBtn.gameObject:SetActive(false)
    local nosign = self:IsHolidayAlert()
    local trans = self.SignBtn.transform
    local nosignstyle = trans:Find("NoSignStyle")
    local signstyle = trans:Find("SignStyle")
    nosignstyle.gameObject:SetActive(nosign == 1 or nosign == 2)
    signstyle.gameObject:SetActive(nosign == 3)
end

function LuaHalloween2023MainWnd:IsHolidayAlert()
    local holiday = CSigninMgr.Inst.holidayInfo
    if holiday and holiday.showSignin and not holiday.hasSignGift and holiday.todayInfo and holiday.todayInfo.Open == 1 then    -- 开活动并且未签到
        return 1
    elseif not holiday then -- 没开活动的时候
        return 2
    else return 3   
    end
end

--@endregion

--@region UIEvent
function LuaHalloween2023MainWnd:OnOrderItemClick(index)
    for i, item in pairs(self.m_CandyOrderItemList) do
        item.item:SetSelected(i == index)
    end
    if self.m_CandyOrderItemList[index].mystery then
        if not self.isMysterOrderOpen then
            g_MessageMgr:ShowMessage("Halloween2023_CandyTask_OthersNotFinished")
        elseif not self.m_CandyOrderItemList[index].isFinish then
            g_MessageMgr:ShowMessage("Halloween2023_CandyTask_ItemNotUsed")
        else
            self:OnActBtn1Click()
        end
    else
        self:OnActBtn1Click()
    end
end

function LuaHalloween2023MainWnd:OnActBtn1Click()
    local activityId = Halloween2023_Setting.GetData().CandyTaskActivityID
    CLuaScheduleMgr.ShowJieRiScheduleInfo(activityId, "Halloween2023_CandyTask_NotOpen")
end

function LuaHalloween2023MainWnd:OnActBtn2Click()
    if self:CheckActivityOpenToday(42010133) then
        CUIManager.ShowUI(CLuaUIResources.HalloweenHongBaoCoverWnd)
    else
        g_MessageMgr:ShowMessage("ZHONGYUANJIE2022_GUSHUNINGZHU_NOTOPEN_TEST")
    end
end

function LuaHalloween2023MainWnd:OnActBtn3Click()
    self.m_TrafficLightAlert:SetActive(false)
    local isInTime = LuaHalloween2022Mgr.IsInTrafficLightPlayTime()
    if self:CheckActivityOpenToday(42040032) then
        CUIManager.ShowUI(CLuaUIResources.HwMuTouRenEnterWnd)
    else
        g_MessageMgr:ShowMessage("ZHONGYUANJIE2022_GUSHUNINGZHU_NOTOPEN_TEST")
    end
    if isInTime then
        LuaActivityRedDotMgr:OnRedDotClicked(64)
    end
end

function LuaHalloween2023MainWnd:OnActBtn4Click()
    CUIManager.ShowUI(CLuaUIResources.Halloween2023OrderBattleEnterWnd)
    self.m_OrderBattleAlert:SetActive(false)
    if self.ActBtn4.transform:Find("open").gameObject.activeSelf then
        LuaActivityRedDotMgr:OnRedDotClicked(65)
    end
end

function LuaHalloween2023MainWnd:OnActBtn5Click()
    CLuaScheduleMgr:ShowScheduleInfo(Halloween2023_Setting.GetData().DailyRewardActivityID)
end

function LuaHalloween2023MainWnd:CheckActivityOpenToday(activityId)
    local list = CScheduleMgr.Inst.rawInfoTimeRestriction
    if list == nil then return false end
    for i = 0, list.Count - 1 do
        if list[i].activityId == activityId then return true end
    end
    return false
end

function LuaHalloween2023MainWnd:OnSignBtnClick()
    local signName = Halloween2023_Setting.GetData().QiaoDaoTabName
    local status = self:IsHolidayAlert()
    if status == 2 then
        g_MessageMgr:ShowMessage("Halloween2023_CheckinNotOpened")
    else
        CWelfareMgr.OpenWelfareWnd(signName)
    end
end

function LuaHalloween2023MainWnd:OnEnable()
    g_ScriptEvent:AddListener("OnHolidaySigninInfoUpdate", self, "InitSignBtn")
    g_ScriptEvent:AddListener("Halloween2022SyncAllowToUseHongBaoCover",self,"InitActBtn2")
end

function LuaHalloween2023MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnHolidaySigninInfoUpdate", self, "InitActBtn2")
    g_ScriptEvent:RemoveListener("Halloween2022SyncAllowToUseHongBaoCover",self,"UpdateProgress")
end
--@endregion UIEvent
