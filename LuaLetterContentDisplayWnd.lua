local UIWidget=import "UIWidget"
local CUICenterOnChild=import "L10.UI.CUICenterOnChild"
local CUITexture=import "L10.UI.CUITexture"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UIVerticalLabel = import "UIVerticalLabel"
local UITweener = import "UITweener"
local DelegateFactory = import "DelegateFactory"
local NGUIText = import "NGUIText"
local CItemMgr=import "L10.Game.CItemMgr"
local QnButton=import "L10.UI.QnButton"

CLuaLetterDisplayMgr = {}
CLuaLetterDisplayMgr.LetterWnd = "LetterContentDisplayWnd"
CLuaLetterDisplayMgr.placeForLetterDisplay = 0
CLuaLetterDisplayMgr.ItemIdForLetterDisplay = nil
CLuaLetterDisplayMgr.PosForLetterDisplay = 0
CLuaLetterDisplayMgr.LetterContent = nil
CLuaLetterDisplayMgr.Reply = nil
CLuaLetterDisplayMgr.ReplyContents = nil
CLuaLetterDisplayMgr.ReplyCallback = nil
CLuaLetterDisplayMgr.SendCallback = nil

function CLuaLetterDisplayMgr.ShowItemLetterDisplayWnd(place, itemId, pos)
	local item = CItemMgr.Inst:GetById(itemId)
    if item == nil or not item.IsItem then
        return
    end
	CLuaLetterDisplayMgr.LetterWnd = "LetterContentDisplayWnd"
    CLuaLetterDisplayMgr.placeForLetterDisplay = place
    CLuaLetterDisplayMgr.ItemIdForLetterDisplay = itemId
    CLuaLetterDisplayMgr.PosForLetterDisplay = pos
    CLuaLetterDisplayMgr.LetterContent = Item_Item.GetData(item.TemplateId).Content
	CLuaLetterDisplayMgr.Reply = nil
	CLuaLetterDisplayMgr.ReplyContents = nil
	CLuaLetterDisplayMgr.ReplyCallback = nil
	CLuaLetterDisplayMgr.SendCallback = nil
	CUIManager.ShowUI(CLuaUIResources.EnvelopeDisplayWnd)
end

function CLuaLetterDisplayMgr.ShowLetterDisplayWnd(content,reply,replyContents,callback,instant)
	CLuaLetterDisplayMgr.LetterWnd = "LetterContentDisplayWnd"
	CLuaLetterDisplayMgr.placeForLetterDisplay = nil
    CLuaLetterDisplayMgr.ItemIdForLetterDisplay = nil
    CLuaLetterDisplayMgr.PosForLetterDisplay = nil
    CLuaLetterDisplayMgr.LetterContent = content
	CLuaLetterDisplayMgr.Reply = reply
	CLuaLetterDisplayMgr.ReplyContents = replyContents
	CLuaLetterDisplayMgr.ReplyCallback=callback
	CLuaLetterDisplayMgr.SendCallback = nil
	if instant then
		CUIManager.ShowUI(CUIResources.LetterContentDisplayWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.EnvelopeDisplayWnd)
	end
end

function CLuaLetterDisplayMgr.SendLetter(content,callback)
	CLuaLetterDisplayMgr.LetterWnd = "LetterContentDisplayWnd"
	CLuaLetterDisplayMgr.placeForLetterDisplay = nil
    CLuaLetterDisplayMgr.ItemIdForLetterDisplay = nil
    CLuaLetterDisplayMgr.PosForLetterDisplay = nil
    CLuaLetterDisplayMgr.LetterContent = content
	CLuaLetterDisplayMgr.Reply = nil
	CLuaLetterDisplayMgr.ReplyContents = nil
	CLuaLetterDisplayMgr.ReplyCallback=nil
	CLuaLetterDisplayMgr.SendCallback = callback
	
	CUIManager.ShowUI(CUIResources.LetterContentDisplayWnd)
end

function CLuaLetterDisplayMgr.ShowSpecialLetterWnd(wndName)
    CLuaLetterDisplayMgr.LetterWnd = wndName
	CUIManager.ShowUI(CLuaUIResources.EnvelopeDisplayWnd)
end

LuaLetterContentDisplayWnd = class()

RegistClassMember(LuaLetterContentDisplayWnd,"m_CloseBtn")
-- RegistClassMember(LuaLetterContentDisplayWnd,"m_VerticalLabel")
RegistClassMember(LuaLetterContentDisplayWnd,"m_FadeInTweener")
RegistClassMember(LuaLetterContentDisplayWnd,"m_ChoiceItem")
RegistClassMember(LuaLetterContentDisplayWnd,"m_ChoiceIdx")
RegistClassMember(LuaLetterContentDisplayWnd,"m_Choices")
RegistClassMember(LuaLetterContentDisplayWnd,"m_ContentLabel")
RegistClassMember(LuaLetterContentDisplayWnd,"m_NoCHNode")
RegistClassMember(LuaLetterContentDisplayWnd,"m_Page")
RegistClassMember(LuaLetterContentDisplayWnd,"m_PagesTf")
RegistClassMember(LuaLetterContentDisplayWnd,"m_PageList")
RegistClassMember(LuaLetterContentDisplayWnd,"m_PageLocalPosition")

RegistClassMember(LuaLetterContentDisplayWnd,"m_Indicator")
RegistClassMember(LuaLetterContentDisplayWnd,"m_Indicators")




function LuaLetterContentDisplayWnd:Awake()
	self.m_CloseBtn = self.transform:Find("Anchor/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go)
		self:Close()
	end), false)
	-- self.m_VerticalLabel = self.transform:Find("Anchor/Page/VerticalLabel"):GetComponent(typeof(UIVerticalLabel))
	self.m_Page=self.transform:Find("Anchor/Page").gameObject
	self.m_Page:SetActive(false)

	self.m_PagesTf = self.transform:Find("Anchor/Pages")

	self.m_FadeInTweener = self.transform:Find("Anchor"):GetComponent(typeof(UITweener))
	self.m_NoCHNode=self.transform:Find("Anchor/NoCH").gameObject
	self.m_NoCHNode:SetActive(false)
	self.m_ContentLabel = self.transform:Find("Anchor/NoCH/ContentLabel"):GetComponent(typeof(UILabel))

	self.m_Indicator = self.transform:Find("Anchor/Indicator").gameObject
	self.m_Indicator:SetActive(false)
	self.m_Indicators=self.transform:Find("Anchor/Indicators")

	self.m_ChoiceIdx = 0
	self.m_Choices = {}
	local replyButton = self.transform:Find("Anchor/ReplyButton").gameObject
	local buttonLabel = replyButton.transform:Find("Label"):GetComponent(typeof(UILabel))

	self.m_ChoiceItem = self.transform:Find("Anchor/Choices/ChoiceItem").gameObject
	self.m_ChoiceItem:SetActive(false)

	if CLuaLetterDisplayMgr.Reply then
		self.transform:Find("Anchor/Choices").gameObject:SetActive(false)
		local grid = self.transform:Find("Anchor/Choices/Grid").gameObject

		replyButton:SetActive(true)

		if #CLuaLetterDisplayMgr.Reply==0 then
			CUICommonDef.SetActive(replyButton,false,true)
		end

		UIEventListener.Get(replyButton).onClick = DelegateFactory.VoidDelegate(function(go)
			if buttonLabel.text == LocalString.GetString("回信") then
				self.transform:Find("Anchor/Choices").gameObject:SetActive(true)
				for i,v in ipairs(CLuaLetterDisplayMgr.Reply) do
					local go = NGUITools.AddChild(grid,self.m_ChoiceItem)
					go:SetActive(true)
					local btn = go:GetComponent(typeof(QnButton))
					table.insert( self.m_Choices,btn )
					local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
					label.text = v
					UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
						self:OnClickChoice(g)
					end)
				end
				-- if #self.m_Choices>0 then
				-- 	self:OnClickChoice(self.m_Choices[1].gameObject)
				-- end
		
				grid:GetComponent(typeof(UIGrid)):Reposition()

				local bg = self.transform:Find("Anchor/Choices/bg"):GetComponent(typeof(UISprite))
				local b = NGUIMath.CalculateRelativeWidgetBounds(grid.transform,true)
				bg.height = math.floor(b.size.y) + 40

				buttonLabel.text= LocalString.GetString("寄送")
			elseif buttonLabel.text == LocalString.GetString("寄送") then
				if self.m_ChoiceIdx==0 then--还没选
					g_MessageMgr:ShowMessage("")
				else
					CLuaLetterDisplayMgr.ReplyCallback(self.m_ChoiceIdx)
					self:Close()
				end
			end

		end)
	elseif CLuaLetterDisplayMgr.SendCallback then
		self.transform:Find("Anchor/Choices").gameObject:SetActive(false)
		replyButton:SetActive(true)

		buttonLabel.text= LocalString.GetString("寄送")
		UIEventListener.Get(replyButton).onClick = DelegateFactory.VoidDelegate(function(go)
			CLuaLetterDisplayMgr.SendCallback()
			self:Close()
		end)
	else
		self.transform:Find("Anchor/Choices").gameObject:SetActive(false)
		replyButton:SetActive(false)
	end
		

	if not CommonDefs.IS_CN_CLIENT and not CommonDefs.IS_HMT_CLIENT then
		--海外版本换背景
		local bg = self.transform:Find("Anchor/Background"):GetComponent(typeof(CUITexture))
		-- self.m_ContentLabel.text = CLuaLetterDisplayMgr.LetterContent
		self.m_ContentLabel.color = NGUIText.ParseColor24("612619", 0)
	else
		-- self.m_VerticalLabel.fontColor = NGUIText.ParseColor24("612619", 0)
		-- self.m_VerticalLabel.text = CLuaLetterDisplayMgr.LetterContent
	end

	self:SetLetterContent(CLuaLetterDisplayMgr.LetterContent)
end

function LuaLetterContentDisplayWnd:SetLetterContent(content)
	if not CommonDefs.IS_CN_CLIENT and not CommonDefs.IS_HMT_CLIENT then
		self.m_ContentLabel.text = content
	else
		-- self.m_VerticalLabel.text = content
		local rowNum = 8
		local list = UIVerticalLabel.SplitText(content,15)
		local num = math.ceil( list.Count/rowNum )
		local maxIndex = list.Count
		local lines = {}
		for i=1,num do
			local line = {}
			local k = math.min(rowNum*i,maxIndex)
			for j=rowNum*(i-1)+1,k do
				table.insert( line,list[j-1] )
			end
			table.insert( lines,table.concat( line, "\n" ) )
		end
		local grid = self.m_PagesTf:Find("Grid")
		Extensions.RemoveAllChildren(grid)
		Extensions.RemoveAllChildren(self.m_Indicators)

		self.m_PageList = {}
		for i=#lines,1,-1 do
			local v = lines[i]
			local go =  NGUITools.AddChild(grid.gameObject,self.m_Page)
			go:SetActive(true)
			local label = go.transform:Find("VerticalLabel"):GetComponent(typeof(UIVerticalLabel))
			label.text = v

			table.insert( self.m_PageList,go )

			if #lines>1 then
				local dot=NGUITools.AddChild(self.m_Indicators.gameObject,self.m_Indicator)
				dot:SetActive(true)
			end
		end
		grid:GetComponent(typeof(UIGrid)):Reposition()
		self.m_Indicators:GetComponent(typeof(UIGrid)):Reposition()

		self.m_PageLocalPosition = {}
		for i=1,grid.childCount do
			local x = math.abs(grid:GetChild(i-1).localPosition.x)
			table.insert(self.m_PageLocalPosition, x)
		end

		local panel = self.m_PagesTf:GetComponent(typeof(UIPanel))
		panel.onClipMove = LuaUtils.OnClippingMoved(function(p)
			self:OnClipMove(p)
		end)
		local centerOnChild=grid:GetComponent(typeof(CUICenterOnChild))
		centerOnChild.onCenter=LuaUtils.OnCenterCallback(function(go)
			local index=go.transform:GetSiblingIndex()
			for i=1,self.m_Indicators.childCount do
				local sprite=self.m_Indicators:GetChild(i-1):GetComponent(typeof(UISprite))
				if i==index+1 then
					sprite.color=Color(0.95,0.9,0.74,1)
				else
					sprite.color=Color(0.95,0.9,0.74,0.3)
				end
			end
		end)

		centerOnChild:CenterOnInstant(grid:GetChild(grid.childCount-1))
		self:OnClipMove(panel)
	end
end

function LuaLetterContentDisplayWnd:OnClipMove(panel)
	local corners = panel.worldCorners
    local panelCenter = CommonDefs.op_Multiply_Vector3_Single((CommonDefs.op_Addition_Vector3_Vector3(corners[2], corners[0])), 0.5)
    local tableTf = panel.transform:Find("Grid")
	local localCenterX = tableTf:InverseTransformPoint(panelCenter).x
	
    local localWidth = math.abs(tableTf:InverseTransformPoint(corners[2]).x - tableTf:InverseTransformPoint(corners[0]).x)/2+200
    for i,child in ipairs(self.m_PageList) do
		local coeff = math.abs(self.m_PageLocalPosition[i] + localCenterX) / localWidth
		-- print(i,coeff)
		if coeff<0.3 then
			child:GetComponent(typeof(UIWidget)).alpha=1-coeff*2
			LuaUtils.SetLocalScale(child.transform,1-coeff,1-coeff,1-coeff)
			local widgets = CommonDefs.GetComponentsInChildren_Component_Type(child.transform, typeof(UIWidget))
			for i=1,widgets.Length do
				widgets[i-1].depth = (1-coeff)*100*2
			end
			child:GetComponent(typeof(UIWidget)).depth=(1-coeff)*100*2-1
		else
			LuaUtils.SetLocalScale(child.transform,1-coeff,1-coeff,1-coeff)
			child:GetComponent(typeof(UIWidget)).alpha=0
		end
    end
end

function LuaLetterContentDisplayWnd:OnClickChoice(g)
	for i,v in ipairs(self.m_Choices) do
		if v.gameObject == g then
			v:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/haogandu_xiaozhuan_bg.mat")
			self.m_ChoiceIdx = i
			--更新内容
			self:SetLetterContent(CLuaLetterDisplayMgr.ReplyContents[self.m_ChoiceIdx])
		else
			v:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/haogandu_xiaozhuan_suo.mat")
		end
	end
end

function LuaLetterContentDisplayWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()

	if CLuaLetterDisplayMgr.ItemIdForLetterDisplay then
		Gac2Gas.RequestUseItem(CLuaLetterDisplayMgr.placeForLetterDisplay, 
		CLuaLetterDisplayMgr.PosForLetterDisplay, 
		CLuaLetterDisplayMgr.ItemIdForLetterDisplay, "")
	end
end

function LuaLetterContentDisplayWnd:Init()


	self.m_FadeInTweener:ResetToBeginning()
	self.m_FadeInTweener.enabled = true

end
