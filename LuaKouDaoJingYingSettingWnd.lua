local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
-- local Guild_Setting = import "L10.Game.Guild_Setting"
local CSortButton = import "L10.UI.CSortButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnRadioBox=import "L10.UI.QnRadioBox"
local QnTableView=import "L10.UI.QnTableView"
local Profession = import "L10.Game.Profession"
local Double = import "System.Double"


CLuaKouDaoJingYingSettingWnd = class()
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"sortRadioBox")
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"tableView")
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"refreshBtn")
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"cancleAllBtn")
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"onlineNumLabel")
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"jingyingNumLabel")

RegistClassMember(CLuaKouDaoJingYingSettingWnd,"mGuildMemberInfoList")--除了自己
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"m_MainPlayerGuildInfo")

RegistClassMember(CLuaKouDaoJingYingSettingWnd,"m_SortFlags")
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"RightTag")

RegistClassMember(CLuaKouDaoJingYingSettingWnd,"m_MorePanel")
RegistClassMember(CLuaKouDaoJingYingSettingWnd,"m_FilterOnlineAndJingYing")


function CLuaKouDaoJingYingSettingWnd:Awake()
    self.m_FilterOnlineAndJingYing=true
    self.m_MainPlayerGuildInfo=nil
    self.m_SortFlags={-1,-1,-1,-1,-1,-1,-1,-1,-1}

    self.sortRadioBox = self.transform:Find("Anchor/TableView/Header"):GetComponent(typeof(QnRadioBox))
    self.tableView = self.transform:Find("Anchor/TableView"):GetComponent(typeof(QnTableView))
    self.refreshBtn = self.transform:Find("Anchor/TableView/Buttons/RefreshBtn").gameObject
    self.cancleAllBtn = self.transform:Find("Anchor/TableView/Buttons/CancelAllBtn").gameObject
    self.onlineNumLabel = self.transform:Find("Anchor/TableView/Buttons/Label"):GetComponent(typeof(UILabel))
    self.jingyingNumLabel = self.transform:Find("Anchor/TableView/Buttons/Label (1)"):GetComponent(typeof(UILabel))
    self.mGuildMemberInfoList = {}

    self.m_MorePanel = FindChild(self.transform,"MorePanel").gameObject
    self.m_MorePanel:SetActive(false)
    local configButton = self.m_MorePanel.transform:Find("OptionBtn").gameObject
    UIEventListener.Get(configButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickConfigButton(go)
    end)
    local tipButton = self.m_MorePanel.transform:Find("TipButton").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if LuaHengYuDangKouMgr:IsHengYuDangKouEnable() then -- 横屿荡寇开启
            g_MessageMgr:ShowMessage("HengYuDangKouElite_Tips")
        else
            g_MessageMgr:ShowMessage("KouDaoElite_Tips")
        end
    end)
    local moreButton = FindChild(self.transform,"MoreBtn").gameObject
    UIEventListener.Get(moreButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_MorePanel:SetActive(true)
    end)

    UIEventListener.Get(self.refreshBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickRefreshButton(go)
    end)
    UIEventListener.Get(self.cancleAllBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickCancleAllButton(go)
    end)

    self.sortRadioBox.OnSelect =DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelect(btn,index)
    end)

    local leagueButton=FindChild(self.transform,"LeagueButton").gameObject
    UIEventListener.Get(leagueButton).onClick=DelegateFactory.VoidDelegate(function(go)
        --打开联赛评价界面
        CUIManager.ShowUI(CLuaUIResources.GuildLeagueWeeklyInfoWnd)
    end)

    if LuaHengYuDangKouMgr:IsHengYuDangKouEnable() then -- 横屿荡寇开启
        local titlelabel = self.transform:Find("Wnd_Bg_Primary_Normal/TitleLabel"):GetComponent(typeof(UILabel))
        titlelabel.text = LocalString.GetString("荡寇精英")
        local koudaotimebutton = self.transform:Find("Anchor/TableView/Header/Table/Button6/Label"):GetComponent(typeof(UILabel))
        koudaotimebutton.text = LocalString.GetString("本周/月荡寇")
    end
end
function CLuaKouDaoJingYingSettingWnd:OnRadioBoxSelect( btn, index) 
    local sortButton = TypeAs(btn, typeof(CSortButton))

    self.m_SortFlags[index+1] = - self.m_SortFlags[index+1]
    for i,v in ipairs(self.m_SortFlags) do
        if i~=index+1 then
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[index+1] < 0)
    self:Sort(self.mGuildMemberInfoList, index)
    self.tableView:ReloadData(true, false)
end

function CLuaKouDaoJingYingSettingWnd:OnClickCancleAllButton( go) 
    local msg = g_MessageMgr:FormatMessage("KouDaoElite_CancleAllConfirm", nil)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "EliteKouDaoClear", "", "")
    end), nil, nil, nil, false)
end
function CLuaKouDaoJingYingSettingWnd:OnClickRefreshButton( go) 
    Gac2Gas.CheckSetKouDaoEliteRight()
    -- CGuildMgr.Inst:GetGuildKouDaoInfo()
    CLuaGuildMgr.m_KouDaoInfo = nil
    if LuaHengYuDangKouMgr:IsHengYuDangKouEnable() then -- 横屿荡寇开启
        Gac2Gas.RequestGetGuildInfo("HengYuDangKouInfo")
    else
        Gac2Gas.RequestGetGuildInfo("GuildKouDaoInfo")
    end
end

function CLuaKouDaoJingYingSettingWnd:Init( )
    Gac2Gas.CheckSetKouDaoEliteRight()
    local function initItem(item,row)
        item:SetBackgroundTexture(row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

        if row==0 then
            --自己
            self:InitItem(item.transform,self.m_MainPlayerGuildInfo,row)
        else
            self:InitItem(item.transform,self.mGuildMemberInfoList[row],row)
        end
    end
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return (self.m_MainPlayerGuildInfo and 1 or 0) + #self.mGuildMemberInfoList
        end,
        initItem)
end
function CLuaKouDaoJingYingSettingWnd:OnEnable( )
    g_ScriptEvent:AddListener("SendGuildInfo_GuildKouDaoInfo", self, "OnSendGuildInfo_GuildKouDaoInfo")
    g_ScriptEvent:AddListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
    g_ScriptEvent:AddListener("SyncPlayerKouDaoEliteRight", self, "OnSyncPlayerKouDaoEliteRight")
end
function CLuaKouDaoJingYingSettingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SendGuildInfo_GuildKouDaoInfo", self, "OnSendGuildInfo_GuildKouDaoInfo")
    g_ScriptEvent:RemoveListener("RequestOperationInGuildSucceed", self, "OnRequestOperationInGuildSucceed")
    g_ScriptEvent:RemoveListener("SyncPlayerKouDaoEliteRight", self, "OnSyncPlayerKouDaoEliteRight")
end
--rightTag: 1 有权限设置 0 没有权限设置
function CLuaKouDaoJingYingSettingWnd:OnSyncPlayerKouDaoEliteRight( rightTag) 
    self.RightTag = rightTag
    CUICommonDef.SetActive(self.cancleAllBtn, self.RightTag > 0, true)
    -- CGuildMgr.Inst:GetGuildKouDaoInfo()
    CLuaGuildMgr.m_KouDaoInfo=nil
    if LuaHengYuDangKouMgr:IsHengYuDangKouEnable() then -- 横屿荡寇开启
        Gac2Gas.RequestGetGuildInfo("HengYuDangKouInfo")
    else
        Gac2Gas.RequestGetGuildInfo("GuildKouDaoInfo")
    end
end
function CLuaKouDaoJingYingSettingWnd:OnRequestOperationInGuildSucceed(args) 
    local requestType, paramStr=args[0] ,args[1]

    if "EliteKouDaoSetting" == requestType then
        local splits=g_LuaUtil:StrSplit(paramStr,",")
        if #splits==2 then
            local playerId =tonumber(splits[1])
            local isElite =tonumber(splits[2])
            --设置数据
            if CLuaGuildMgr.m_KouDaoInfo then
                local val=CLuaGuildMgr.m_KouDaoInfo[playerId]
                if val then val.IsKouDaoElite=isElite end
            end

            --找到那个player，然后设置
            local parent=self.tableView.m_Grid.transform
            if self.m_MainPlayerGuildInfo.PlayerId==playerId then
                self:SetItemElite(parent:GetChild(0),0,isElite>0)
            else
                for i,v in ipairs(self.mGuildMemberInfoList) do
                    if v.PlayerId==playerId then
                            self:SetItemElite(parent:GetChild(i),i,isElite>0)
                        break
                    end
                end
            end

        end
    elseif "EliteKouDaoClear" == requestType then
        --设置数据
        if CLuaGuildMgr.m_KouDaoInfo then
            for k,v in pairs(CLuaGuildMgr.m_KouDaoInfo) do
                v.IsKouDaoElite=0
            end
        end
        --取消设置
        local parent=self.tableView.m_Grid.transform
        for i=1,parent.childCount do
            self:SetItemElite(parent:GetChild(i-1),i-1,false)
        end
    end

    if ("EliteKouDaoSetting" == requestType) or ("EliteKouDaoClear" == requestType) then
        self:UpdateKoudaoJingyingNum()
    end
end
function CLuaKouDaoJingYingSettingWnd:OnSendGuildInfo_GuildKouDaoInfo( ) 
    self.mGuildMemberInfoList={}
    local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0

    if CLuaGuildMgr.m_KouDaoInfo then
        for k,v in pairs(CLuaGuildMgr.m_KouDaoInfo) do
            if k==myId then
                self.m_MainPlayerGuildInfo=v
            else
                if self.m_FilterOnlineAndJingYing then
                    if v.IsKouDaoElite>0 or v.IsOnline > 0 then
                        table.insert( self.mGuildMemberInfoList, v )
                    end
                else
                    table.insert( self.mGuildMemberInfoList, v )
                end
            end
        end
    end
    --默认排序
    self.sortRadioBox:ChangeTo(- 1, true)
    self:Sort(self.mGuildMemberInfoList, 8)

    self.tableView:ReloadData(true, false)

    self:UpdateMemberNum()
    self:UpdateKoudaoJingyingNum()
end
function CLuaKouDaoJingYingSettingWnd:UpdateMemberNum( )
    local num = 0
    for i,v in ipairs(self.mGuildMemberInfoList) do
        if v.IsOnline>0 then
            num=num+1
        end
    end
    local me=(self.m_MainPlayerGuildInfo and 1 or 0)
    num = num + me

    self.onlineNumLabel.text = SafeStringFormat3(LocalString.GetString("在线人数：[00ff00]%d[-]/%d"), num, #self.mGuildMemberInfoList+me)
end
function CLuaKouDaoJingYingSettingWnd:UpdateKoudaoJingyingNum( )
    if self.mGuildMemberInfoList ~= nil then
        local num = 0
        local onlineNum = 0

        for i,v in ipairs(self.mGuildMemberInfoList) do
            if v.IsKouDaoElite>0 then
                num=num+1
                if v.IsOnline>0 then
                    onlineNum=onlineNum+1
                end
            end
        end

        if self.m_MainPlayerGuildInfo then
            if self.m_MainPlayerGuildInfo.IsKouDaoElite>0 then
                num=num+1
                if self.m_MainPlayerGuildInfo.IsOnline>0 then
                    onlineNum=onlineNum+1
                end
            end
        end

        local textcontent = LocalString.GetString("寇岛精英：[00ff00]%d[-]/%d/%d")
        if LuaHengYuDangKouMgr:IsHengYuDangKouEnable() then -- 横屿荡寇开启
            textcontent = LocalString.GetString("荡寇精英：[00ff00]%d[-]/%d/%d")
        end
        self.jingyingNumLabel.text = SafeStringFormat3( textcontent, onlineNum, num, Guild_Setting.GetData().KouDaoEliteNum )
    end
end

function CLuaKouDaoJingYingSettingWnd:Sort(t,sortIndex)
    local function defaultSort(a,b)
        if a.IsInKouDaoScene == 3 and b.IsInKouDaoScene ~= 3 then
            return true
        elseif a.IsInKouDaoScene ~= 3 and b.IsInKouDaoScene == 3 then
            return false
        elseif a.IsKouDaoElite > 0 and b.IsKouDaoElite == 0 then
            return true
        elseif a.IsKouDaoElite == 0 and b.IsKouDaoElite > 0 then
            return false
        else
            return a.PlayerId<b.PlayerId
        end
    end
    local flag=self.m_SortFlags[sortIndex+1]
    if sortIndex==0 then--cls name
        table.sort( t, function(a,b)
            local ret=flag
            if a.Class<b.Class then
                ret = -flag
            elseif a.Class>b.Class then
                ret=flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==1 then--level
        table.sort( t, function(a,b)
            local ret=flag
            if a.Level<b.Level then
                ret = flag
            elseif a.Level>b.Level then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )

    elseif sortIndex==2 then--xiuwei
        table.sort( t, function(a,b)
            local ret=flag
            if a.XiuWei<b.XiuWei then
                ret = flag
            elseif a.XiuWei>b.XiuWei then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==3 then--xiulian
        table.sort( t, function(a,b)
            local ret=flag
            if a.XiuLian<b.XiuLian then
                ret = flag
            elseif a.XiuLian>b.XiuLian then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==4 then--equip score
        table.sort( t, function(a,b)
            local ret=flag
            if a.EquipScore<b.EquipScore then
                ret = flag
            elseif a.EquipScore>b.EquipScore then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==5 then--koudao time
        table.sort( t, function(a,b)
            local ret=flag
            if a.WeekKouDaoTimes<b.WeekKouDaoTimes then
                ret = flag
            elseif a.WeekKouDaoTimes>b.WeekKouDaoTimes then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==6 then--评价
        table.sort( t, function(a,b)
            local sum1 = a.CommentLevelR1+a.CommentLevelR2+a.CommentLevelR3
            local sum2 = b.CommentLevelR1+b.CommentLevelR2+b.CommentLevelR3
            local ret=flag
            if sum1<sum2 then
                ret = flag
            elseif sum1>sum2 then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==7 then--is in scene
        table.sort( t, function(a,b)
            local ret=flag
            if a.IsInKouDaoScene==3 and b.IsInKouDaoScene~=3 then
                ret = -flag
            elseif a.IsInKouDaoScene~=3 and b.IsInKouDaoScene==3 then
                ret = flag
            elseif a.IsInKouDaoScene>0 and b.IsInKouDaoScene==0 then
                return -flag
            elseif a.IsInKouDaoScene==0 and b.IsInKouDaoScene>0 then
                return flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==8 then--default
        table.sort( t, function(a,b)
            local ret=flag
            if a.IsKouDaoElite<b.IsKouDaoElite then
                ret = flag
            elseif a.IsKouDaoElite>b.IsKouDaoElite then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    end
end


function CLuaKouDaoJingYingSettingWnd:InitItem(transform,info,row)
    local clsSprite = transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local xiuweiLabel = transform:Find("XiuweiLabel"):GetComponent(typeof(UILabel))
    local xiulianLabel = transform:Find("XiulianLabel"):GetComponent(typeof(UILabel))
    local equipScoreLabel = transform:Find("EquipScoreLabel"):GetComponent(typeof(UILabel))
    local koudaoTimeLabel = transform:Find("KouDaoTimeLabel"):GetComponent(typeof(UILabel))
    -- local leagueTimeLabel = transform:Find("LeagueTimeLabel"):GetComponent(typeof(UILabel))
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))
    -- local btnLabel = transform:Find("Btn/Label"):GetComponent(typeof(UILabel))
    -- local btn = transform:Find("Btn").gameObject
    local inSceneLabel = transform:Find("InGameLabel"):GetComponent(typeof(UILabel))

    local playerId=info.PlayerId

    nameLabel.text = info.Name
    clsSprite.spriteName = Profession.GetIconByNumber(info.Class)

    CUICommonDef.SetActive(clsSprite.gameObject, info.IsOnline > 0, true)

    local default
    if info.XianFanStatus > 0 then
        default=SafeStringFormat3( "[c][ff7900]Lv.%d[-][/c]",info.Level )
        -- default = System.String.Format("[c][ff7900]Lv.%d[-][/c]", Constants.ColorOfFeiSheng, info.Level)
    else
        default=SafeStringFormat3( "Lv.%d",info.Level )
        -- default = System.String.Format("Lv.{0}", info.Level)
    end
    levelLabel.text = default

    xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.XiuWei, 1), typeof(Double), "F1")
    --一位小数显示
    xiulianLabel.text = tostring(info.XiuLian)

    equipScoreLabel.text = tostring(info.EquipScore)

    koudaoTimeLabel.text = SafeStringFormat3("%d/%d", info.WeekKouDaoTimes, info.MonthKouDaoTimes)

    -- leagueTimeLabel.text = tostring(info.JoinLeagueTimes)
    local league=transform:Find("GuildLeague")
    for i=1,3 do
        local label=league:GetChild(i-1):GetComponent(typeof(UILabel))
        if i==1 then
            CLuaGuildMgr.SetLeagueCommentLevel(label,info.CommentLevelR1)
        elseif i==2 then
            CLuaGuildMgr.SetLeagueCommentLevel(label,info.CommentLevelR2)
        elseif i==3 then
            CLuaGuildMgr.SetLeagueCommentLevel(label,info.CommentLevelR3)
        end
    end

    local isElite = info.IsKouDaoElite > 0
    self:SetItemElite(transform,row,isElite)

    if info.IsInKouDaoScene == 0 or info.IsInKouDaoScene == 1 or info.IsInKouDaoScene == 4 then
        inSceneLabel.text = "_"
        inSceneLabel.color =Color(0.066,0.17,0.3)
    elseif info.IsInKouDaoScene == 3 then
        inSceneLabel.text = LocalString.GetString("是")
        inSceneLabel.color = Color(0.066,0.17,0.3)
    elseif info.IsInKouDaoScene == 2 then
        inSceneLabel.text = LocalString.GetString("否")
        inSceneLabel.color = Color.red
    end

    UIEventListener.Get(tickSprite.transform:GetChild(0).gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if tickSprite.enabled then
            --撤销
            local str =SafeStringFormat3( "%s,0",playerId )
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "EliteKouDaoSetting", str, "")
        else
            local str =SafeStringFormat3( "%s,1",playerId )
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "EliteKouDaoSetting", str, "")
        end
    end)
end

function CLuaKouDaoJingYingSettingWnd:SetItemElite(transform,row,isElite)
    local tickSprite = transform:Find("TickSprite"):GetComponent(typeof(UISprite))
    tickSprite.enabled = isElite
end

function CLuaKouDaoJingYingSettingWnd:OnDestroy()
end

function CLuaKouDaoJingYingSettingWnd:OnClickConfigButton(go)
    CLuaOptionMgr.ShowDlg(
        function() return 2 end,
        function(index) 
            if index==1 then
                if LuaHengYuDangKouMgr:IsHengYuDangKouEnable() then -- 横屿荡寇开启
                    return LocalString.GetString("只显示在线成员和荡寇精英") 
                end
                return LocalString.GetString("只显示在线成员和寇岛精英") 
            else
                return LocalString.GetString("显示全部成员") 
            end
        end,
        function(index) 
            if index==1 then 
                return self.m_FilterOnlineAndJingYing
            else
                return not self.m_FilterOnlineAndJingYing
            end
        end,
        function(index,val) 
            if index==1 then
                if val then
                    self.m_FilterOnlineAndJingYing = true
                    self:OnSendGuildInfo_GuildKouDaoInfo()
                end
            else
                if val then
                    self.m_FilterOnlineAndJingYing = false
                    self:OnSendGuildInfo_GuildKouDaoInfo()
                end
            end
        end,
        self.transform:TransformPoint(Vector3(600,-200,0)),
        true
    )
end
