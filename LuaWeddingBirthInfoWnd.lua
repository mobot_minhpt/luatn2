local CommonDefs       = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnNumberInput    = import "L10.UI.QnNumberInput"
local CServerTimeMgr   = import "L10.Game.CServerTimeMgr"
local CTooltip         = import "L10.UI.CTooltip"
local CUIPickerWndMgr  = import "L10.UI.CUIPickerWndMgr"
local CUIDatePickerMgr = import "L10.UI.CUIDatePickerMgr"
local AlignType        = import "L10.UI.CTooltip+AlignType"
local DateTime         = import "System.DateTime"
local EnumGender       = import "L10.Game.EnumGender"
local CButton          = import "L10.UI.CButton"


LuaWeddingBirthInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingBirthInfoWnd, "genderButton")
RegistClassMember(LuaWeddingBirthInfoWnd, "genderLabel")
RegistClassMember(LuaWeddingBirthInfoWnd, "genderArrow")
RegistClassMember(LuaWeddingBirthInfoWnd, "yearInput")
RegistClassMember(LuaWeddingBirthInfoWnd, "yearDefaultLabel")
RegistClassMember(LuaWeddingBirthInfoWnd, "monthDayButton")
RegistClassMember(LuaWeddingBirthInfoWnd, "monthDayTip")
RegistClassMember(LuaWeddingBirthInfoWnd, "monthDayLabel")
RegistClassMember(LuaWeddingBirthInfoWnd, "monthDayArrow")
RegistClassMember(LuaWeddingBirthInfoWnd, "timeButton")
RegistClassMember(LuaWeddingBirthInfoWnd, "timeTip")
RegistClassMember(LuaWeddingBirthInfoWnd, "timeLabel")
RegistClassMember(LuaWeddingBirthInfoWnd, "timeArrow")
RegistClassMember(LuaWeddingBirthInfoWnd, "okButton")
RegistClassMember(LuaWeddingBirthInfoWnd, "closeButton")

RegistClassMember(LuaWeddingBirthInfoWnd, "gender")
RegistClassMember(LuaWeddingBirthInfoWnd, "year")
RegistClassMember(LuaWeddingBirthInfoWnd, "month")
RegistClassMember(LuaWeddingBirthInfoWnd, "day")
RegistClassMember(LuaWeddingBirthInfoWnd, "time")
RegistClassMember(LuaWeddingBirthInfoWnd, "today")
RegistClassMember(LuaWeddingBirthInfoWnd, "maxYear")
RegistClassMember(LuaWeddingBirthInfoWnd, "minYear")
RegistClassMember(LuaWeddingBirthInfoWnd, "selectGender")
RegistClassMember(LuaWeddingBirthInfoWnd, "maleStr")
RegistClassMember(LuaWeddingBirthInfoWnd, "femaleStr")


function LuaWeddingBirthInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaWeddingBirthInfoWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.genderButton = anchor:Find("Gender")
    self.genderLabel = self.genderButton:Find("Value"):GetComponent(typeof(UILabel))
    self.genderArrow = self.genderButton:Find("Arrow")

    self.yearInput = anchor:Find("Year"):GetComponent(typeof(QnNumberInput))
    self.yearDefaultLabel = self.yearInput.transform:Find("Desc"):GetComponent(typeof(UILabel))

    self.monthDayButton = anchor:Find("MonthDay")
    self.monthDayTip = self.monthDayButton:Find("Desc").gameObject
    self.monthDayLabel = self.monthDayButton:Find("Value"):GetComponent(typeof(UILabel))
    self.monthDayArrow = self.monthDayButton:Find("Arrow")

    self.timeButton = anchor:Find("Time")
    self.timeTip = self.timeButton:Find("Desc").gameObject
    self.timeLabel = self.timeButton:Find("Value"):GetComponent(typeof(UILabel))
    self.timeArrow = self.timeButton:Find("Arrow")

    self.okButton = anchor:Find("OKButton")
    self.closeButton = self.transform:Find("Bg"):Find("CloseButton")
end

-- 初始化按键响应
function LuaWeddingBirthInfoWnd:InitEventListener()
    UIEventListener.Get(self.genderButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGenderClick(go)
	end)

    UIEventListener.Get(self.monthDayButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMonthDayClick(go)
	end)

    UIEventListener.Get(self.timeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTimeClick(go)
	end)

    UIEventListener.Get(self.okButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)

    UIEventListener.Get(self.closeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)
end

function LuaWeddingBirthInfoWnd:InitActive()

end


function LuaWeddingBirthInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnPickerItemSelected", self, "OnPickerWndSelect")
    g_ScriptEvent:AddListener("OnDoublePickerItemSelected", self, "OnDoublePickerSelect")
    g_ScriptEvent:AddListener("OnCeBaziWaitPartner", self, "OnWaitPartner")
    g_ScriptEvent:AddListener("OnCeBaziFail", self, "OnFail")
end

function LuaWeddingBirthInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnPickerItemSelected", self, "OnPickerWndSelect")
    g_ScriptEvent:RemoveListener("OnDoublePickerItemSelected", self, "OnDoublePickerSelect")
    g_ScriptEvent:RemoveListener("OnCeBaziWaitPartner", self, "OnWaitPartner")
    g_ScriptEvent:RemoveListener("OnCeBaziFail", self, "OnFail")
end

function LuaWeddingBirthInfoWnd:OnPickerWndSelect(args)
    local index = args[0]
    if not self.selectGender then
        self.time = index
        self.timeTip:SetActive(false)
        self.timeLabel.text = tostring(index)
        Extensions.SetLocalRotationZ(self.timeArrow.transform, 0)
    else
        self.gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), index)
        local default
        if self.gender == EnumGender.Male then
            default = self.maleStr
        else
            default = self.femaleStr
        end
        self.genderLabel.text = default
        Extensions.SetLocalRotationZ(self.genderArrow, 0)
    end
end

function LuaWeddingBirthInfoWnd:OnDoublePickerSelect(args)
    self.month = args[0]
    self.day = args[1]
    self.monthDayTip:SetActive(false)
    self.monthDayLabel.text = System.String.Format("{0}/{1}", self.month, self.day)
    Extensions.SetLocalRotationZ(self.monthDayArrow, 0)
end

function LuaWeddingBirthInfoWnd:OnWaitPartner()
    self.okButton:GetComponent(typeof(CButton)).Enabled = false
end

function LuaWeddingBirthInfoWnd:OnFail()
    g_MessageMgr:ShowMessage("CEZI_FAILED")
    self.okButton:GetComponent(typeof(CButton)).Enabled = true
end


function LuaWeddingBirthInfoWnd:Init()
    self.selectGender = true
    self.maleStr = LocalString.GetString("男")
    self.femaleStr = LocalString.GetString("女")

    self:InitGender()
    self:InitYear()
    self:InitMonthDay()
    self:InitTime()
end

-- 初始化性别
function LuaWeddingBirthInfoWnd:InitGender()
    self.gender = EnumGender.Male
    self.genderLabel.text = self.maleStr
    Extensions.SetLocalRotationZ(self.genderArrow, 0)
end

-- 初始化年
function LuaWeddingBirthInfoWnd:InitYear()
    self.year = 0
    self.today = CServerTimeMgr.Inst:GetZone8Time()
    self.maxYear = self.today.Year
    self.minYear = WeddingIteration_Setting.GetData().BaziMinYear

    self.yearInput:Awake()
    self.yearInput.AlignType = CTooltip.AlignType.Right
    self.yearInput.OnKeyboardClosed = DelegateFactory.Action(function()
        self:OnKeyboardClosed()
    end)

    self.yearInput.OnMaxValueButtonClick = DelegateFactory.Action(function()
        self:OnMaxValueButtonClick()
    end)

    self.yearDefaultLabel.text = System.String.Format(LocalString.GetString("输入{0}-{1}之间的年份"), self.minYear, self.maxYear)
end

-- 初始化月/日
function LuaWeddingBirthInfoWnd:InitMonthDay()
    self.month = 0
    self.day = 0
    self.monthDayLabel.text = ""
    self.monthDayTip:SetActive(true)
    Extensions.SetLocalRotationZ(self.monthDayArrow, 0)
end

-- 初始化时辰
function LuaWeddingBirthInfoWnd:InitTime()
    self.time = -1
    self.timeLabel.text = ""
    self.timeTip:SetActive(true)
    Extensions.SetLocalRotationZ(self.timeArrow, 0)
end

function LuaWeddingBirthInfoWnd:OnKeyboardClosed()
    local default
    default, self.year = System.Int32.TryParse(self.yearInput.Text)
    if default and self.year >= self.minYear and self.year <= self.maxYear then
        self:InitMonthDay()
    else
        g_MessageMgr:ShowMessage("ENTER_RIGHT_YEAR")
        self.yearInput:ForceSetText("", false)
        self.year = 0
    end
end

function LuaWeddingBirthInfoWnd:OnMaxValueButtonClick()
    self.yearInput:ForceSetText(tostring(self.maxYear))
    self:InitMonthDay()
end

--@region UIEvent

function LuaWeddingBirthInfoWnd:OnGenderClick(go)
    self.selectGender = true
    local contents = CreateFromClass(MakeGenericClass(List, String))
    CommonDefs.ListAdd(contents, typeof(String), self.maleStr)
    CommonDefs.ListAdd(contents, typeof(String), self.femaleStr)
    local localPos = go.transform.localPosition
    local anchor = go.transform.parent:TransformPoint(localPos)
    CUIPickerWndMgr.ShowPickerWnd(contents, anchor, EnumToInt(self.gender), CTooltip.AlignType.Right)
    Extensions.SetLocalRotationZ(self.genderArrow, 180)
end

function LuaWeddingBirthInfoWnd:OnMonthDayClick(go)
    if self.year >= self.minYear and self.year <= self.maxYear then
        Extensions.SetLocalRotationZ(self.monthDayArrow, 180)
        CUIDatePickerMgr.ShowDoublePickerWnd(self.year, math.max(self.month, 1), math.max(self.day, 1), go.transform, AlignType.Right)
    else
        g_MessageMgr:ShowMessage("ENTER_RIGHT_BIRTHYEAR")
    end
end

function LuaWeddingBirthInfoWnd:OnTimeClick(go)
    self.selectGender = false
    local contents = CreateFromClass(MakeGenericClass(List, String))
    for i = 0, 23 do
        CommonDefs.ListAdd(contents, typeof(String), tostring(i))
    end
    local localPos = go.transform.localPosition
    local anchor = go.transform.parent:TransformPoint(localPos)
    CUIPickerWndMgr.ShowPickerWnd(contents, anchor, math.max(self.time, 0), CTooltip.AlignType.Right)
    Extensions.SetLocalRotationZ(self.timeArrow, 180)
end

function LuaWeddingBirthInfoWnd:OnOKButtonClick()
    if self.year >= self.minYear and self.year <= self.maxYear and self.month > 0 and self.month <= 12 and self.day > 0 and self.day <= 31 and self.time >= 0 and self.time < 24 then
        local birthTime = CreateFromClass(DateTime, self.year, self.month, self.day, self.time, 0, 0)
        if CommonDefs.op_GreaterThan_DateTime_DateTime(birthTime, self.today) then
            g_MessageMgr:ShowMessage("ENTER_DATA_WRONG")
        else
            Gac2Gas.RequestCeBaZi(EnumToInt(self.gender), self.year, self.month, self.day, self.time)
        end
    else
        g_MessageMgr:ShowMessage("ENTER_COMPLETE_IFO")
    end
end

function LuaWeddingBirthInfoWnd:OnCloseButtonClick()
    Gac2Gas.RequestCancelCeBaZi()
    CUIManager.CloseUI(CLuaUIResources.WeddingBirthInfoWnd)
end

--@endregion UIEvent

