local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local CTextInput = import "L10.UI.CTextInput"
local CItemMgr = import "L10.Game.CItemMgr"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local EChatPanel = import "L10.Game.EChatPanel"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local Utility = import "L10.Engine.Utility"

LuaWishAddWnd = class()
RegistChildComponent(LuaWishAddWnd,"CloseButton", GameObject)
RegistChildComponent(LuaWishAddWnd,"emotionBtn", GameObject)
RegistChildComponent(LuaWishAddWnd,"statusInput", CTextInput)
RegistChildComponent(LuaWishAddWnd,"TipBtn", GameObject)
RegistChildComponent(LuaWishAddWnd,"SendMsgButton", GameObject)
RegistChildComponent(LuaWishAddWnd,"addNode", GameObject)
RegistChildComponent(LuaWishAddWnd,"Label", UILabel)

RegistClassMember(LuaWishAddWnd, "addItemId")
RegistClassMember(LuaWishAddWnd, "addItemNum")
RegistClassMember(LuaWishAddWnd, "m_DefaultChatInputListener")

function LuaWishAddWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.WishAddWnd)
end

function LuaWishAddWnd:OnEnable()
	g_ScriptEvent:AddListener("SelectWishItemUpdate", self, "UpdateWishItemInfo")
end

function LuaWishAddWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SelectWishItemUpdate", self, "UpdateWishItemInfo")
end

function LuaWishAddWnd:UpdateWishItemInfo(templateId,num)
	local itemData = CItemMgr.Inst:GetItemTemplate(templateId)
	self.addNode.transform:Find('bg').gameObject:SetActive(false)
	self.addNode.transform:Find('pic'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
	if num and num > 1 then
		self.addNode.transform:Find('num'):GetComponent(typeof(UILabel)).text = num
	else
		self.addNode.transform:Find('num'):GetComponent(typeof(UILabel)).text = ''
	end
	self.addItemId = templateId
	self.addItemNum = num
end

function LuaWishAddWnd:OnEmotionButtonClick(go)
  CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, go, EChatPanel.Undefined, 0, 0)
end

function LuaWishAddWnd:OnSendWish(msg)
  if System.String.IsNullOrEmpty(msg) then
    --g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
    g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString('添加心愿语和心愿物品后才可许愿哦~#84'))
    return
  end
	if self.addItemId and self.addItemNum then
	else
    g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString('添加心愿语和心愿物品后才可许愿哦~#84'))
    return
	end

	msg = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(msg, true)
  if System.String.IsNullOrEmpty(msg) then
		g_MessageMgr:ShowMessage("Speech_Violation")
    return
  end
	msg = CChatMgr.Inst:FilterYangYangEmotion(msg)

	local addType = 0
	local regionId = EShopMallRegion_lua.ELingyuMall
	local msgArray = Utility.BreakString(msg, 2, 255, nil)

	if self.addItemId and self.addItemNum then
		Gac2Gas.Wish_AddWish(1,self.addItemId,self.addItemNum,regionId,msgArray[0],msgArray[1])
	else
		Gac2Gas.Wish_AddWish(0,0,0,0,msgArray[0],msgArray[1])
	end

  self:Close()
end

function LuaWishAddWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  if not self.m_DefaultChatInputListener then
    self.m_DefaultChatInputListener = LuaPersonalSpaceMgrReal.GetChatListener(self.statusInput)
  end

	local onAddClick = function(go)
    LuaWishMgr:OpenWishShop()
	end
	CommonDefs.AddOnClickListener(self.addNode,DelegateFactory.Action_GameObject(onAddClick),false)

	local onTipClick = function(go)
		g_MessageMgr:ShowMessage("AddWishTip")
	end
	CommonDefs.AddOnClickListener(self.TipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

	self.addNode.transform:Find('num'):GetComponent(typeof(UILabel)).text = ''
	local wishMaxNum = tonumber(Mall_Setting.GetData("Wish_RecentMaxNum").Value)
	local wishUsedNum = wishMaxNum - LuaWishMgr.RestWishNum
	self.Label.text = SafeStringFormat3(LocalString.GetString('30天内已许愿%s次，还可许愿%s次'),wishUsedNum,LuaWishMgr.RestWishNum)

  UIEventListener.Get(self.emotionBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    self:OnEmotionButtonClick()
  end)

  self.statusInput.OnSend = DelegateFactory.Action_string(function (msg)
    self:OnSendWish(msg)
  end)

--  local sendBtn = self.statusInput.transform:Find('SendMsgButton').gameObject
--  UIEventListener.Get(sendBtn).onClick = DelegateFactory.VoidDelegate(function (p)
--    self:OnSendWish()
--  end)
end

return LuaWishAddWnd
