local MessageMgr = import "L10.Game.MessageMgr"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local EnumTeamCCSettingType = import "L10.Game.EnumTeamCCSettingType"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CScene = import "L10.Game.CScene"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaHaiZhanPosSelectionWnd = class()
-- LuaHaiZhanPosSelectionWnd.s_ResultUd = nil
LuaHaiZhanPosSelectionWnd.s_StartTime = 0
LuaHaiZhanPosSelectionWnd.s_Duration = 0

RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_Pos")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_ReadyBtn")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_ReadyHint")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_VoiceBtn")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_VoiceHint")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_CountdownLabel")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_PosTitle")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_PosDesc")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_CurIdx")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_IsGuide")

RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_IsReady")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_Idx2Desc")
RegistClassMember(LuaHaiZhanPosSelectionWnd, "m_CountdownTick")

function LuaHaiZhanPosSelectionWnd:IsGuide()
    return CScene.MainScene and (CScene.MainScene.GamePlayDesignId == NavalWar_Setting.GetData().TrainingPrePlayId or CScene.MainScene.GamePlayDesignId == NavalWar_Setting.GetData().TrainingSeaPlayId)
end

function LuaHaiZhanPosSelectionWnd:Awake()
    self.m_IsGuide = self:IsGuide()
    self.m_Idx2Desc = {
        NavalWar_Setting.GetData().CaptainPosDesc,
        NavalWar_Setting.GetData().GunnerPosDesc,
        NavalWar_Setting.GetData().GunnerPosDesc,
        NavalWar_Setting.GetData().GunnerPosDesc,
        NavalWar_Setting.GetData().CrewPosDesc,
    }
    self.m_Idx2PlayerId = {}

    self.m_Pos = {}
    for i = 1, 5 do 
        self.m_Pos[i] = self.transform:Find("Left/Grid/Pos"..i)
    end

    self.m_ReadyBtn = self.transform:Find("Right/Bg/ReadyButton").gameObject
    UIEventListener.Get(self.m_ReadyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_CurIdx > 0 then
            Gac2Gas.NavalOnEnterSelectSeat(self.m_CurIdx)
        end
        if self.m_IsGuide then
            CUIManager.CloseUI(CLuaUIResources.HaiZhanPosSelectionWnd)
        end
    end)
    self.m_VoiceBtn = self.transform:Find("Left/TeamVoice/VocieBtn").gameObject
    UIEventListener.Get(self.m_VoiceBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnVoiceBtnClick()
    end)

    self.m_CountdownLabel = self.transform:Find("TitleLabel/Countdown"):GetComponent(typeof(UILabel))
    self.m_PosTitle = self.transform:Find("Right/Bg/Title")
    self.m_PosDesc = self.transform:Find("Right/Bg/Desc"):GetComponent(typeof(UILabel))
    self.m_ReadyHint = self.transform:Find("Right/Bg/ReadyLabel").gameObject
    self.m_VoiceHint = self.transform:Find("Left/TeamVoice/Bubble").gameObject
    
    self.m_CurIdx = 0
    for i = 1, 5 do
        self:InitPos(i)
    end
    self:SelectPos(self.m_CurIdx)
end

function LuaHaiZhanPosSelectionWnd:RefreshCCModeSprite()
    local type = CClientMainPlayer.Inst and CClientMainPlayer.Inst.RelationshipProp.TeamSpeakStatus or EnumTeamCCSettingType.eDisableTeamCC
    self.m_VoiceBtn.transform:Find("Normal_m").gameObject:SetActive(type == EnumToInt(EnumTeamCCSettingType.eEnableCapture))
    self.m_VoiceBtn.transform:Find("Disable_m").gameObject:SetActive(type == EnumToInt(EnumTeamCCSettingType.eEnableListen))
    self.m_VoiceBtn.transform:Find("Disable_lis").gameObject:SetActive(type == EnumToInt(EnumTeamCCSettingType.eDisableTeamCC))
end

function LuaHaiZhanPosSelectionWnd:OnVoiceBtnClick()
    if not CCMiniAPIMgr.Inst.IsEngineVersionEnabled then
        MessageMgr.Inst:ShowMessage("NEED_UPDATE_CLIENT")
        return
    end

    if not CCCChatMgr.Inst.EnableTeamCC then
        MessageMgr.Inst:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end
    
    if not CCMiniAPIMgr.Inst:CheckWindowsPlatformCCMiniPath() then
        return
    end
    
    CUIManager.ShowUI(CUIResources.TeamCCSettingWnd)
end

local cnt = 0
function LuaHaiZhanPosSelectionWnd:Update()
    if self.m_IsGuide then
        if not CUIManager.IsLoaded(CLuaUIResources.HaiZhanDriveWnd) then return end
        if cnt > 5 and not CGuideMgr.Inst:IsInPhase(EnumGuideKey.HaiZhan_1) then
            CGuideMgr.Inst:StartGuide(EnumGuideKey.HaiZhan_1, 0)
        end
        cnt = cnt + 1
    end
end

function LuaHaiZhanPosSelectionWnd:Init()
    CUIManager.CloseUI(CLuaUIResources.ShuJia2023MainWnd)
    CUIManager.CloseUI(CLuaUIResources.ShuJiaHaiZhanResultWnd)
    if self.m_IsGuide then
        self.m_CountdownLabel.gameObject:SetActive(false)
    else
        UnRegisterTick(self.m_CountdownTick)
        local tickFunc = function()
            local count = math.floor(LuaHaiZhanPosSelectionWnd.s_StartTime + LuaHaiZhanPosSelectionWnd.s_Duration - CServerTimeMgr.Inst.timeStamp)
            self.m_CountdownLabel.text = math.max(0, count)
            if count < 0 then
                CUIManager.CloseUI(CLuaUIResources.HaiZhanPosSelectionWnd)
            end
        end
        tickFunc()
        self.m_CountdownTick = RegisterTick(tickFunc, 500)
    end

    --初始化每个人的位置
    self:OnSyncNavalWarSeatInfo(LuaHaiZhanMgr.s_SeatInfo)
end

function LuaHaiZhanPosSelectionWnd:OnDestroy()
    UnRegisterTick(self.m_CountdownTick)
    self.m_CountdownTick = nil
    cnt = 0
end

function LuaHaiZhanPosSelectionWnd:OnEnable()
    if CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCCChatMgr.Inst.EnableTeamCC then
        self.m_VoiceBtn.transform.parent.gameObject:SetActive(false)
        self:RefreshCCModeSprite()
    else
        self.m_VoiceBtn.transform.parent.gameObject:SetActive(false)
    end
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("OnTeamCCSettingTypeUpdate", self, "OnMainPlayerTeamCCTypeUpdate")
    g_ScriptEvent:AddListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")
end

function LuaHaiZhanPosSelectionWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("OnTeamCCSettingTypeUpdate", self, "OnMainPlayerTeamCCTypeUpdate")
    g_ScriptEvent:RemoveListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")
end

function LuaHaiZhanPosSelectionWnd:OnSyncNavalWarSeatInfo(seatInfo)
    if seatInfo then
        for i = 1, 5 do
            self:InitPos(i, seatInfo[i])
            if seatInfo[i] and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == seatInfo[i].id then
                self.m_IsReady = true
                self.m_CurIdx = i
            end
        end
        local closeWnd = true
        for i = 6, 10 do
            if seatInfo[i] and seatInfo[i].id then
                closeWnd = false
                break
            end
        end
        if closeWnd then
            CUIManager.CloseUI(CLuaUIResources.HaiZhanPosSelectionWnd)
        end
    end
    self:SelectPos(self.m_CurIdx)
end

function LuaHaiZhanPosSelectionWnd:OnMainPlayerCreated()
    self:RefreshCCModeSprite()
end

function LuaHaiZhanPosSelectionWnd:OnMainPlayerTeamCCTypeUpdate()
	self:RefreshCCModeSprite()
end

function LuaHaiZhanPosSelectionWnd:InitPos(idx, info) 
    local pos = self.m_Pos[idx]
    if pos then
        local leader = pos:Find("Frame/LeaderSprite").gameObject
        local portrait = pos:Find("Frame/Portrait"):GetComponent(typeof(CUITexture))
        local posIcon = pos:Find("Frame/Icon").gameObject
        local nameLabel = pos:Find("NameLabel"):GetComponent(typeof(UILabel))
        local readyHint = pos:Find("ReadyLabel").gameObject
        local selected = pos:Find("Selected").gameObject

        if info then
            leader:SetActive(CTeamMgr.Inst:IsTeamLeader(info.id))
            portrait:LoadPortrait(CUICommonDef.GetPortraitName(info.class, info.gender, -1), false)
            posIcon:SetActive(false)
            nameLabel.text = info.name
            readyHint:SetActive(false)
            selected:SetActive(CClientMainPlayer.Inst and info.id == CClientMainPlayer.Inst.Id)
        else
            leader:SetActive(false)
            portrait:Clear()
            posIcon:SetActive(true)
            nameLabel.text = ""
            readyHint:SetActive(true)
        end
        nameLabel.color = CClientMainPlayer.Inst and info and CClientMainPlayer.Inst.Id == info.id and Color.green or Color.white

        UIEventListener.Get(pos.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:SelectPos(idx)
        end)
    end
end

function LuaHaiZhanPosSelectionWnd:SelectPos(idx)
    if self.m_IsReady and idx ~= self.m_CurIdx then
        return 
    end
    self.m_CurIdx = idx
    self.m_PosTitle:Find("ChuanZhang").gameObject:SetActive(idx == 1)
    self.m_PosTitle:Find("PaoShou").gameObject:SetActive(idx == 2 or idx == 3 or idx == 4)
    self.m_PosTitle:Find("ChuanYuan").gameObject:SetActive(idx == 5)
    self.m_PosDesc.text = CUICommonDef.TranslateToNGUIText(self.m_Idx2Desc[idx] or "")
    if LuaHaiZhanMgr.s_SeatInfo then
        self.m_ReadyBtn:SetActive(idx > 0 and not LuaHaiZhanMgr.s_SeatInfo[idx])
        self.m_ReadyHint:SetActive(idx > 0 and LuaHaiZhanMgr.s_SeatInfo[idx])
    else
        self.m_ReadyBtn:SetActive(true)
        self.m_ReadyHint:SetActive(false)
    end

    for i = 1, 5 do
        self.m_Pos[i]:Find("Selected").gameObject:SetActive(i == idx)
    end
end

-- { { playerId = bConfirm, ... }, { seatId = playerId, ... } }
-- function LuaHaiZhanPosSelectionWnd:OnSyncTeamSelectConfirmInfo(resultUd) -- 这个是通用rpc,要处理一下gameplayId
--     local info = g_MessagePack.unpack(resultUd)
--     self.m_Idx2PlayerId = info[2]
--     for i = 1, 5 do
--         self:InitPos(i, self.m_Idx2PlayerId[i])
--         if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == self.m_Idx2PlayerId[i] then
--             self.m_IsReady = true
--             self.m_CurIdx = i
--         end
--     end
--     self:SelectPos(self.m_CurIdx)
-- end

function LuaHaiZhanPosSelectionWnd:GetGuideGo(methodName)
    if methodName == "GetLeaderBtn" then
        return self.m_Pos[1].gameObject
    elseif methodName == "GetReadyBtn" then
        return self.m_ReadyBtn
    end
    return nil
end