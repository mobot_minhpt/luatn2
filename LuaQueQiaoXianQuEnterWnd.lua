local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local DelegateFactory  = import "DelegateFactory"

LuaQueQiaoXianQuEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "Stage1", "Stage1", GameObject)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "Stage2", "Stage2", GameObject)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "Stage3", "Stage3", GameObject)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "EnterButton", "EnterButton", CButton)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "HistoryTeammateBtn", "HistoryTeammateBtn", CButton)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "RuleTipBtn", "RuleTipBtn", QnButton)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "LeftAwardTimes", "LeftAwardTimes", GameObject)
RegistChildComponent(LuaQueQiaoXianQuEnterWnd, "SignUpLabel", "SignUpLabel", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaQueQiaoXianQuEnterWnd, "m_EnterBtnLabel")
RegistClassMember(LuaQueQiaoXianQuEnterWnd, "m_EnterBtnSprite")

function LuaQueQiaoXianQuEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.EnterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)


	
	UIEventListener.Get(self.HistoryTeammateBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHistoryTeammateBtnClick()
	end)


	
	UIEventListener.Get(self.RuleTipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleTipBtnClick()
	end)


    --@endregion EventBind end
end

function LuaQueQiaoXianQuEnterWnd:Init()
	Gac2Gas.QueryQueQiaoXianQuSignUpInfo()
	self.m_EnterBtnLabel = self.EnterButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.m_EnterBtnSprite = self.EnterButton.gameObject:GetComponent(typeof(UISprite))
	self.m_EnterBtnLabel.text = LocalString.GetString("进入副本")
	self.m_EnterBtnSprite.spriteName = "common_btn_01_yellow"

	self.Title.text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_BackStory")
    self.Stage1.transform:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Guide1")
    self.Stage2.transform:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Guide2")
    self.Stage3.transform:Find("Label"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Guide3")
	self.Stage1.transform:Find("Title"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Title1")
    self.Stage2.transform:Find("Title"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Title2")
    self.Stage3.transform:Find("Title"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("2021QiXi_QueQiaoXianQu_Title3")
	self.SignUpLabel:SetActive(false)
	self.LeftAwardTimes:SetActive(false)
end

function LuaQueQiaoXianQuEnterWnd:UpdateQueQiaoXianQuSignUpInfo()
	if LuaQiXi2021Mgr.isSignUp then
		self.m_EnterBtnLabel.text = LocalString.GetString("取消匹配")
		self.m_EnterBtnSprite.spriteName = "common_btn_01_blue"
		self.LeftAwardTimes:SetActive(false)
		self.SignUpLabel:SetActive(true)
	else
		self.LeftAwardTimes:SetActive(true)
		self.SignUpLabel:SetActive(false)
		self.m_EnterBtnLabel.text = LocalString.GetString("进入副本")
		self.m_EnterBtnSprite.spriteName = "common_btn_01_yellow"
		local leftTimes = self.LeftAwardTimes.transform:Find("Label"):GetComponent(typeof(UILabel))
		if LuaQiXi2021Mgr.todayRemainRewardTimes <= 0 then
			leftTimes.text = LocalString.GetString("[c][FF0000FF]0[-][/c]")
		else
			leftTimes.text = LocalString.GetString(SafeStringFormat3( "[c][FFFFFF]%d[-][/c]",LuaQiXi2021Mgr.todayRemainRewardTimes))
		end
	end
end
--@region UIEvent

function LuaQueQiaoXianQuEnterWnd:OnEnterButtonClick()
	if LuaQiXi2021Mgr.isSignUp then
		Gac2Gas.RequestCancelSignUpQueQiaoXianQu()
	else
		Gac2Gas.TeamRequestEnterQueQiaoXianQu()
	end
end

function LuaQueQiaoXianQuEnterWnd:OnRuleTipBtnClick()
    g_MessageMgr:ShowMessage("2021QiXi_QUEQIAOXIANQU_RULE")
end

function LuaQueQiaoXianQuEnterWnd:OnHistoryTeammateBtnClick()
    Gac2Gas.QueryQueQiaoXianQuPartnerInfo()
end

function LuaQueQiaoXianQuEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncQueQiaoXianQuSignUpInfo",self, "UpdateQueQiaoXianQuSignUpInfo")
end

function LuaQueQiaoXianQuEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncQueQiaoXianQuSignUpInfo",self, "UpdateQueQiaoXianQuSignUpInfo")
end


--@endregion UIEvent

