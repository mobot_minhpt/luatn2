local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIFx = import "L10.UI.CUIFx"
local String = import "System.String"
local Object = import "System.Object"
local Monster_Monster = import "L10.Game.Monster_Monster"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMainPlayerInfoMgr = import "L10.UI.CMainPlayerInfoMgr"
local CGameSettingMgr = import "L10.Game.CGameSettingMgr"
-- 反串bossEnum数据
EnumTravestyRole = {
	eNone = 100,	-- 未知状态
	ePlayer = 101,	-- 报名玩家
	eBoss = 102,	-- 报名boss
	eNotSignUp = 103, -- 未报名状态
}

EnumTravestyRolePlayState = {
	Idle = 1,
	Prepare = 2,
	Start = 3,
	End = 4,
}
LuaShuJia2021Mgr = {}



LuaShuJia2021Mgr.inited = false -- 数据是否初始化
LuaShuJia2021Mgr.bossTipTextTable = {}  -- boss方任务描述
LuaShuJia2021Mgr.challengerTipTextTable = {}    -- 挑战者方任务描述
LuaShuJia2021Mgr.ChallengerTipHpPrecentTable = {}	-- 挑战方血量任务需求百分比
LuaShuJia2021Mgr.lostTimeToReward = {}	-- boss方团灭敌人次数
LuaShuJia2021Mgr.KillPlayerTimeToReward = {} -- boss方击杀敌人次数
LuaShuJia2021Mgr.awardIdTable = {} -- 奖励道具ID
LuaShuJia2021Mgr.maxChallengeTimes = 0 -- 挑战者最多挑战次数
LuaShuJia2021Mgr.needShowFx = {false, false, false}
LuaShuJia2021Mgr.HasShowFx = {false, false, false}
-- boss信息
LuaShuJia2021Mgr.bossId = 0
LuaShuJia2021Mgr.bossIcon = ""
LuaShuJia2021Mgr.bossName = ""
LuaShuJia2021Mgr.bossSkillId = {}
LuaShuJia2021Mgr.bossSkillDict = nil		-- Boss技能映射
--LuaShuJia2021Mgr.bossSkillIntroduce = nil	-- BOSS技能介绍

LuaShuJia2021Mgr.isBoss = false -- 是否为boss
LuaShuJia2021Mgr.bossHpPrecent = 0 -- boss血量降低百分比
LuaShuJia2021Mgr.leftChallengeTimes = 0 -- 挑战者剩余挑战次数
LuaShuJia2021Mgr.killTimes = 0
LuaShuJia2021Mgr.playerInfos = {}	-- 玩家信息
LuaShuJia2021Mgr.awardMailIds = {} -- 奖励邮件id
LuaShuJia2021Mgr.completeTable = {} -- 奖励情况
LuaShuJia2021Mgr.initReward = {}	-- 进入副本时初始获得的奖励
LuaShuJia2021Mgr.GetAwardInPlay = {} -- 副本内获得的奖励
LuaShuJia2021Mgr.AchieveInPlay = {} -- 副本内达成的目标
LuaShuJia2021Mgr.playData = nil
-- 匹配信息
LuaShuJia2021Mgr.isSignUp = 0	-- 报名情况
LuaShuJia2021Mgr.signUpClick = EnumTravestyRole.eNotSignUp
LuaShuJia2021Mgr.queueInfoBoss = 0	-- 排队信息
LuaShuJia2021Mgr.queueInfoPlayer = 0 
LuaShuJia2021Mgr.isBossEasyQueue = false	-- 是否boss方排队人数较少
LuaShuJia2021Mgr.isChallengerEasyQueue = true	-- 是否挑战者方排队人数较少
LuaShuJia2021Mgr.m_OpenPlayerWndKey = -1			-- boss方玩家无法打开玩家个人界面
LuaShuJia2021Mgr.m_AutoHealKey      = -1			-- boss方玩家自动吃药开关
-- 保存Player数据，以防止切换场景时访问不到CClientMainPlayer
LuaShuJia2021Mgr.m_Name = nil
LuaShuJia2021Mgr.m_Id = nil
LuaShuJia2021Mgr.m_Class = nil
LuaShuJia2021Mgr.m_Gender = nil
LuaShuJia2021Mgr.m_Level = nil
LuaShuJia2021Mgr.m_ServerName = nil


function LuaShuJia2021Mgr.IsDaMoWangPlay()
	if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
		if gamePlayId == ShuJia_TravestyRole.GetData().GameplayID then
           return true
        end
    end
	return false
end

function LuaShuJia2021Mgr.InitInfo()
	if LuaShuJia2021Mgr.inited then return end
	LuaShuJia2021Mgr.InitAwardId()
	LuaShuJia2021Mgr.InitTipText()
	LuaShuJia2021Mgr.InitBossInfo()
	LuaShuJia2021Mgr.inited = true
end

function LuaShuJia2021Mgr.InitTipText()
    --读表初始化
    if not LuaShuJia2021Mgr.bossTipTextTable then LuaShuJia2021Mgr.bossTipTextTable = {} end
    if not LuaShuJia2021Mgr.challengerTipTextTable then LuaShuJia2021Mgr.challengerTipTextTable = {} end
	if not LuaShuJia2021Mgr.ChallengerTipHpPrecentTable then LuaShuJia2021Mgr.ChallengerTipHpPrecentTable = {} end
	if not LuaShuJia2021Mgr.awardMailIds then LuaShuJia2021Mgr.awardMailIds = {} end

	LuaShuJia2021Mgr.bossTipTextTable = {
		ShuJia_TravestyRole.GetData().Boss_Target_1,
		ShuJia_TravestyRole.GetData().Boss_Target_2,
		ShuJia_TravestyRole.GetData().Boss_Target_3
	}

	LuaShuJia2021Mgr.challengerTipTextTable = {
		ShuJia_TravestyRole.GetData().Player_Target_1,
		ShuJia_TravestyRole.GetData().Player_Target_2,
		ShuJia_TravestyRole.GetData().Player_Target_3
	}

	local HpDropPrecent = g_LuaUtil:StrSplit(ShuJia_TravestyRole.GetData().HpDropRateToReward,';')
	for i,v in ipairs(HpDropPrecent) do
		if (not System.String.IsNullOrEmpty(v)) then
			local dataInfo = g_LuaUtil:StrSplit(v,",")
			local precentText = tonumber(dataInfo[1])
			local mailId = tonumber(dataInfo[2])
			table.insert(LuaShuJia2021Mgr.ChallengerTipHpPrecentTable, precentText)
			table.insert(LuaShuJia2021Mgr.awardMailIds, mailId)
			LuaShuJia2021Mgr.completeTable[i] = false
		end
	end

	local lostTimes = g_LuaUtil:StrSplit(ShuJia_TravestyRole.GetData().LostTimeToReward,';')
	local killTimes = g_LuaUtil:StrSplit(ShuJia_TravestyRole.GetData().KillPlayerToReward,';')
	LuaShuJia2021Mgr.lostTimeToReward = {-1,-1,-1}
	LuaShuJia2021Mgr.KillPlayerTimeToReward = {-1,-1,-1}
	for i=1,#LuaShuJia2021Mgr.awardMailIds do
		for _,v in ipairs(lostTimes) do
			if (not System.String.IsNullOrEmpty(v)) then
				local dataInfo = g_LuaUtil:StrSplit(v,",")
				if tonumber(dataInfo[2]) == LuaShuJia2021Mgr.awardMailIds[i] then
					LuaShuJia2021Mgr.lostTimeToReward[i] = tonumber(dataInfo[1])
				end
			end
		end
		for _,v in ipairs(killTimes) do
			if (not System.String.IsNullOrEmpty(v)) then
				local dataInfo = g_LuaUtil:StrSplit(v,",")
				if tonumber(dataInfo[2]) == LuaShuJia2021Mgr.awardMailIds[i] then
					LuaShuJia2021Mgr.KillPlayerTimeToReward[i] = tonumber(dataInfo[1])
				end
			end
		end
	end
	LuaShuJia2021Mgr.maxChallengeTimes = ShuJia_TravestyRole.GetData().MaxPlayerLostTime
end

function LuaShuJia2021Mgr.InitAwardId()
    if not LuaShuJia2021Mgr.awardIdTable then LuaShuJia2021Mgr.awardIdTable = {} end
	local awardTable = g_LuaUtil:StrSplit(ShuJia_TravestyRole.GetData().ItemID,';')
	for i,v in ipairs(awardTable) do
		if (not System.String.IsNullOrEmpty(v)) then
			table.insert(LuaShuJia2021Mgr.awardIdTable, tonumber(v))
		end
	end
end

function LuaShuJia2021Mgr.InitBossInfo()
	--[[
	local bossIntroduceDict = CreateFromClass(MakeGenericClass(Dictionary, String, String))
	local Skill = g_LuaUtil:StrSplit(ShuJia_TravestyRole.GetData().SkillIntroduce,';')
	for i,v in ipairs(Skill) do
		if (not System.String.IsNullOrEmpty(v)) then
			local skillInfo = g_LuaUtil:StrSplit(v,",")
			local skillID = skillInfo[1]
			local skillIntroduce = skillInfo[2]
			CommonDefs.DictAdd_LuaCall(bossIntroduceDict, skillID, skillIntroduce)
		end
	end
	LuaShuJia2021Mgr.bossSkillIntroduce = bossIntroduceDict
	]]

	local bossSkillIdDict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
	local bossSkill = g_LuaUtil:StrSplit(ShuJia_TravestyRole.GetData().SkillID,';')
	for i,v in ipairs(bossSkill) do
		if (not System.String.IsNullOrEmpty(v)) then
			local skillInfo = g_LuaUtil:StrSplit(v,",")
			local BossID = skillInfo[1]
			local skillIds = {}
			for j,k in ipairs(skillInfo) do
				if j ~= 1 and not System.String.IsNullOrEmpty(k) then
					table.insert(skillIds, k)
				end
			end
			CommonDefs.DictAdd_LuaCall(bossSkillIdDict, BossID, skillIds)
		end
	end
	LuaShuJia2021Mgr.bossSkillDict = bossSkillIdDict
end

function LuaShuJia2021Mgr.SyncSignUpInfo(isInMatching,role,playerRoleCount, bossRoleCount, BossId, playData)
	if LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.eNone then
		if isInMatching then LuaShuJia2021Mgr.isSignUp = role 
		else  LuaShuJia2021Mgr.isSignUp = EnumTravestyRole.eNotSignUp end
	end
	LuaShuJia2021Mgr.isBossEasyQueue = playerRoleCount > bossRoleCount
	LuaShuJia2021Mgr.isChallengerEasyQueue = playerRoleCount < bossRoleCount
	LuaShuJia2021Mgr.playData = playData
	LuaShuJia2021Mgr.bossId = BossId
	local bossId = BossId
	local boss = Monster_Monster.GetData(bossId)

	if boss ~= nil then
		LuaShuJia2021Mgr.bossName = boss.Name
		LuaShuJia2021Mgr.bossIcon = boss.HeadIcon
	end

	for i=1,#LuaShuJia2021Mgr.awardMailIds do
		if LuaShuJia2021Mgr.playData then
			LuaShuJia2021Mgr.completeTable[i] = CommonDefs.DictGetValue_LuaCall(LuaShuJia2021Mgr.playData,LuaShuJia2021Mgr.awardMailIds[i]) == 1
		else
			LuaShuJia2021Mgr.completeTable[i] = false
		end
	end

	LuaShuJia2021Mgr.queueInfoBoss = bossRoleCount
	LuaShuJia2021Mgr.queueInfoPlayer = playerRoleCount

	g_ScriptEvent:BroadcastInLua("UpdateDaMoWangSignUpInfo")
end

function LuaShuJia2021Mgr.SyncPlayerInfo(state,BossMonsterId, BossHpDropRate, PlayerLossTimes,playerInfos,reason, playerDieTimes)
	LuaShuJia2021Mgr.GetPlayerInfo()
	LuaShuJia2021Mgr.BossId = BossMonsterId
	local bossId = BossMonsterId
	local boss = Monster_Monster.GetData(bossId)
	local getReward = false
	local initReward = false
	if boss ~= nil then 
		LuaShuJia2021Mgr.bossName = boss.Name
		LuaShuJia2021Mgr.bossIcon = boss.HeadIcon
	end
	LuaShuJia2021Mgr.bossHpPrecent = BossHpDropRate
	LuaShuJia2021Mgr.leftChallengeTimes = LuaShuJia2021Mgr.maxChallengeTimes - PlayerLossTimes
	LuaShuJia2021Mgr.killTimes = playerDieTimes
	LuaShuJia2021Mgr.playerInfos = playerInfos
	if CClientMainPlayer.Inst then
		LuaShuJia2021Mgr.isBoss = LuaShuJia2021Mgr.playerInfos[CClientMainPlayer.Inst.Id].Role == EnumTravestyRole.eBoss
		getReward = CommonDefs.DictContains_LuaCall(LuaShuJia2021Mgr.playerInfos[CClientMainPlayer.Inst.Id],"Reward")
		initReward = CommonDefs.DictContains_LuaCall(LuaShuJia2021Mgr.playerInfos[CClientMainPlayer.Inst.Id],"InitReward")
		for i=1,#LuaShuJia2021Mgr.awardMailIds do
			LuaShuJia2021Mgr.GetAwardInPlay[i] = false
			LuaShuJia2021Mgr.initReward[i] = false
			if getReward then
				local Rewardlist = LuaShuJia2021Mgr.playerInfos[CClientMainPlayer.Inst.Id].Reward
				if Rewardlist then
					for j=0,Rewardlist.Count-1 do
						if Rewardlist[j] and Rewardlist[j] == LuaShuJia2021Mgr.awardMailIds[i] then 
							LuaShuJia2021Mgr.GetAwardInPlay[i] = true
						end
					end
				end
			end
			if initReward then
				local initRewardList = LuaShuJia2021Mgr.playerInfos[CClientMainPlayer.Inst.Id].InitReward
				if initRewardList then
					for j=0,initRewardList.Count-1 do
						if initRewardList[j] and initRewardList[j] == LuaShuJia2021Mgr.awardMailIds[i] then 
							LuaShuJia2021Mgr.initReward[i] = true
						end
					end
				end
			end
		end
	end
	for i=1,#LuaShuJia2021Mgr.needShowFx do
		LuaShuJia2021Mgr.needShowFx[i] = (not LuaShuJia2021Mgr.initReward[i]) and (not LuaShuJia2021Mgr.HasShowFx[i]) and LuaShuJia2021Mgr.GetAwardInPlay[i]
		LuaShuJia2021Mgr.HasShowFx[i] = LuaShuJia2021Mgr.initReward[i] or LuaShuJia2021Mgr.needShowFx[i] or LuaShuJia2021Mgr.HasShowFx[i]
	end
	LuaShuJia2021Mgr.GetAchieveInPlay()
	
	LuaShuJia2021Mgr.SetPlayerStateInfo(state,reason)
	g_ScriptEvent:BroadcastInLua("UpdateDaMoWangPlayInfo")
	
end

function LuaShuJia2021Mgr.SetPlayerStateInfo(state,reason)
	-- 玩家扮演boss
	if LuaShuJia2021Mgr.isBoss then
		-- 进入场景或断线重连
		if reason == "EnterScene" or reason == "StateChange" or reason == "Reconnect" or reason == "SyncBossHpTick" then
			-- 关闭显示玩家属性页面开关
			if LuaShuJia2021Mgr.m_OpenPlayerWndKey < 0 then
				LuaShuJia2021Mgr.m_OpenPlayerWndKey = CMainPlayerInfoMgr.Enabled:SetValue(false)
			else
				LuaShuJia2021Mgr.m_OpenPlayerWndKey = CMainPlayerInfoMgr.Enabled:SetValue(false,LuaShuJia2021Mgr.m_OpenPlayerWndKey)
			end
			if LuaShuJia2021Mgr.m_AutoHealKey < 0 then
				LuaShuJia2021Mgr.m_AutoHealKey = CGameSettingMgr.autoHealEnabled:SetValue(false)
			else
				LuaShuJia2021Mgr.m_AutoHealKey = CGameSettingMgr.autoHealEnabled:SetValue(false,LuaShuJia2021Mgr.m_AutoHealKey)
			end
		end
	end
	-- 离开玩法时显示结算界面,如果曾设置为不显示玩家属性面板则恢复
	if state == EnumTravestyRolePlayState.End or reason == "LeaveScene" then
		CUIManager.ShowUI(CLuaUIResources.DaMoWangResultWnd)
		if LuaShuJia2021Mgr.m_OpenPlayerWndKey  >= 0 then
			CMainPlayerInfoMgr.Enabled:SetDic(true,LuaShuJia2021Mgr.m_OpenPlayerWndKey)
		end
		if LuaShuJia2021Mgr.m_AutoHealKey >= 0 then
			CGameSettingMgr.autoHealEnabled:SetDic(true,LuaShuJia2021Mgr.m_AutoHealKey)
		end
	end
end

function LuaShuJia2021Mgr.GetAchieveInPlay()
	if LuaShuJia2021Mgr.isBoss then
		local LossTimes =LuaShuJia2021Mgr.maxChallengeTimes - LuaShuJia2021Mgr.leftChallengeTimes
		local KillTimes = LuaShuJia2021Mgr.killTimes
		for i=1,#LuaShuJia2021Mgr.lostTimeToReward do
			local completeLoss = false
			local completeKill = false
			if tonumber(LuaShuJia2021Mgr.lostTimeToReward[i]) ~= -1 then
				completeLoss = LossTimes >= tonumber(LuaShuJia2021Mgr.lostTimeToReward[i])
			end
			if tonumber(LuaShuJia2021Mgr.KillPlayerTimeToReward[i]) ~= -1 then
				completeKill = KillTimes >= tonumber(LuaShuJia2021Mgr.KillPlayerTimeToReward[i])
			end
			LuaShuJia2021Mgr.AchieveInPlay[i] = (LuaShuJia2021Mgr.initReward[i] or LuaShuJia2021Mgr.GetAwardInPlay[i]) and (completeLoss or completeKill)
		end
	else 
		for i=1,#LuaShuJia2021Mgr.ChallengerTipHpPrecentTable do
			local complete = LuaShuJia2021Mgr.bossHpPrecent >=  LuaShuJia2021Mgr.ChallengerTipHpPrecentTable[i]
			LuaShuJia2021Mgr.AchieveInPlay[i] = (LuaShuJia2021Mgr.initReward[i] or LuaShuJia2021Mgr.GetAwardInPlay[i]) and complete
		end
	end
end


function LuaShuJia2021Mgr.ShowFuBenFx(go)
	if not go then return end
	local fx = go.transform:Find("Fx"):GetComponent(typeof(CUIFx))
	if not fx then return end
	fx:DestroyFx()
	fx:LoadFx("fx/ui/prefab/UI_shuiguopaidui_fubenlibao.prefab")
end

function LuaShuJia2021Mgr.ShowJieSuanFx(go)
	if not go then return end
	local fx = go.transform:Find("Fx"):GetComponent(typeof(CUIFx))
	if not fx then return end
	fx:DestroyFx()
	fx:LoadFx("fx/ui/prefab/UI_shuiguopaidui_jiesuanlibao.prefab")
end

function LuaShuJia2021Mgr.CheckIsSameRole(playerId1, playerId2)
	if LuaShuJia2021Mgr.playerInfos[playerId1] and LuaShuJia2021Mgr.playerInfos[playerId2] then
		return LuaShuJia2021Mgr.playerInfos[playerId1].Role ~= LuaShuJia2021Mgr.playerInfos[playerId2].Role
	end
	return false
end

function LuaShuJia2021Mgr.GetPlayerInfo()
	if  CClientMainPlayer.Inst then
        LuaShuJia2021Mgr.m_Name = CClientMainPlayer.Inst.RealName
        LuaShuJia2021Mgr.m_Id = CClientMainPlayer.Inst.Id
        LuaShuJia2021Mgr.m_Class = CClientMainPlayer.Inst.Class
        LuaShuJia2021Mgr.m_Gender = CClientMainPlayer.Inst.Gender
        LuaShuJia2021Mgr.m_Level = CClientMainPlayer.Inst.Level
        LuaShuJia2021Mgr.m_ServerName = CClientMainPlayer.Inst:GetMyServerName()
    end
end
