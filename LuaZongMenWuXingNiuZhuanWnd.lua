local CMiniMap = import "L10.UI.CMiniMap"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"

LuaZongMenWuXingNiuZhuanWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZongMenWuXingNiuZhuanWnd, "WuXingTex", "WuXingTex", CUITexture)
RegistChildComponent(LuaZongMenWuXingNiuZhuanWnd, "OldWuXingTex", "OldWuXingTex", CUITexture)
RegistChildComponent(LuaZongMenWuXingNiuZhuanWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaZongMenWuXingNiuZhuanWnd, "Fx", "Fx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenWuXingNiuZhuanWnd, "m_Tick")

function LuaZongMenWuXingNiuZhuanWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaZongMenWuXingNiuZhuanWnd:Init()
    if self.Fx then
        self.Fx:LoadFx("fx/ui/prefab/UI_wuxingnizhuan.prefab")
    end
    if self.OldWuXingTex then
        self.OldWuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_WuXingBeforeNiuZhuan))
    end
    self.WuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_NiuZhuanWuXingResult))
    if CClientMainPlayer.Inst and CMiniMap.Instance then
        self.Label.text = g_MessageMgr:FormatMessage("WuXingNiuZhuan_Finished_Text",CMiniMap.Instance.sceneNameButton.Text)
    end
    self.m_Tick = RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.ZongMenWuXingNiuZhuanWnd)
        CUIManager.CloseUI(CLuaUIResources.NewZongMenWuXingNiuZhuanWnd)
    end, LuaZongMenMgr.m_OpenNewSystem and 4000 or 3000)
end

--@region UIEvent

--@endregion UIEvent
function LuaZongMenWuXingNiuZhuanWnd:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end
