-- Auto Generated!!
local CFriendGroupModifyListItem = import "L10.UI.CFriendGroupModifyListItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local Object = import "System.Object"
CFriendGroupModifyListItem.m_SetFriendInfo_CS2LuaHook = function (this, name, groupName, level, friendiness, clazz, gender, expression, isInXianShenStatus, offLineTime) 
    this.nameLabel.text = name
    if offLineTime < 0 then
        this.groupLabel.text = groupName
    else
        if offLineTime > 0 then
            this.groupLabel.text = System.String.Format(LocalString.GetString("[7F7F7F]离线 {0}[-]"), ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(offLineTime), "yyyy-MM-dd"))
        else
            this.groupLabel.text = LocalString.GetString("在线")
        end
    end
    local default
    if isInXianShenStatus then
        default = L10.Game.Constants.ColorOfFeiSheng
    else
        default = NGUIText.EncodeColor24(this.levelLabel.color)
    end
    this.levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, level)
    this.friendInessLabel.text = System.String.Format(LocalString.GetString("友好度:{0}"), friendiness)
    this.Checked = false
    local pname = CUICommonDef.GetPortraitName(clazz, gender, expression)
    this.portraitTexture:LoadNPCPortrait(pname, false)
end
CFriendGroupModifyListItem.m_OnClick_CS2LuaHook = function (this) 
    this.Checked = not this.Checked
    if this.checkCallback ~= nil then
        GenericDelegateInvoke(this.checkCallback, Table2ArrayWithCount({this.ID, this.Checked}, 2, MakeArrayClass(Object)))
    end
end
