local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Object = import "System.Object"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem=import "L10.Game.CItem"

LuaChooseProofWnd = class()
RegistChildComponent(LuaChooseProofWnd,"CloseButton", GameObject)
RegistChildComponent(LuaChooseProofWnd,"itemTemplate", GameObject)
RegistChildComponent(LuaChooseProofWnd,"Scrollview", UIScrollView)
RegistChildComponent(LuaChooseProofWnd,"Table", UITable)
RegistChildComponent(LuaChooseProofWnd,"node1", GameObject)
RegistChildComponent(LuaChooseProofWnd,"node2", GameObject)
RegistChildComponent(LuaChooseProofWnd,"tipLabel", UILabel)
RegistChildComponent(LuaChooseProofWnd,"chooseBtn", GameObject)

RegistClassMember(LuaChooseProofWnd, "chooseData")
RegistClassMember(LuaChooseProofWnd, "highLightNode")

function LuaChooseProofWnd:Close()
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Confirm_CloseWindow"), DelegateFactory.Action(function ()
		CUIManager.CloseUI(CLuaUIResources.ChooseProofWnd)
	end), nil, nil, nil, false)
end

function LuaChooseProofWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaChooseProofWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaChooseProofWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onSubmitClick = function(go)
		self:ItemSubmit()
	end
	CommonDefs.AddOnClickListener(self.chooseBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)

	self.node1:SetActive(true)
	self.node2:SetActive(false)
	self.tipLabel.text = ''

	self:InitItemData()
end

function LuaChooseProofWnd:InitItemData()
	--local data = '21050683,Christmas_RescueTree_Times_Limit,1;21050684,SanXing_FlagBearer_Forbid_Cast_Skill,0;21050685,SANXING_ELO_POINT_STATE_INTERFACE_MSG,0'
	local data = nil
	local dataId = tonumber(LuaZhuJueJuQingMgr.TaskId)
	if dataId then
		local taskData = ZhuJueJuQing_Evidence.GetData(dataId)
		if taskData then
			data = taskData.Item
			self.tipLabel.text = g_MessageMgr:FormatMessage(taskData.Message)
		end
	end
	if not data then
		return
	end

	local itemTable = g_LuaUtil:StrSplit(data, ";")
	for i,v in ipairs(itemTable) do
		local singleItem = g_LuaUtil:StrSplit(v, ",")
		local itemTempId = tonumber(singleItem[1])
		if itemTempId then
			local messageName = singleItem[2]
			local node = NGUITools.AddChild(self.Table.gameObject,self.itemTemplate)
			node:SetActive(true)

			local itemData = CItemMgr.Inst:GetItemTemplate(itemTempId)
			node.transform:Find('IconTexture'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)

			local onItemClick = function(go)
				self:ItemClick(singleItem,itemData,node)
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onItemClick),false)
		end
	end

	self.Table:Reposition()
	self.Scrollview:ResetPosition()
end

function LuaChooseProofWnd:ItemClick(data,itemData,node)
	self.chooseData = data
	self.node1:SetActive(false)
	self.node2:SetActive(true)

	self.node2.transform:Find('name'):GetComponent(typeof(UILabel)).text = itemData.Name
	self.node2.transform:Find('desc'):GetComponent(typeof(UILabel)).text = CUICommonDef.TranslateToNGUIText(CItem.GetItemDescription(itemData.ID,true))

	local highNode = node.transform:Find('highLight').gameObject
	if self.highLightNode then
		self.highLightNode:SetActive(false)
	end
	self.highLightNode = highNode
	self.highLightNode:SetActive(true)
end

function LuaChooseProofWnd:ItemSubmit()
	if self.chooseData then
		local itemTempId = tonumber(self.chooseData[1])
		local messageName = self.chooseData[2]
		local status = tonumber(self.chooseData[3])

		if status == 1 then
			local empty = CreateFromClass(MakeGenericClass(List, Object))
			Gac2Gas.FinishClientTaskEvent(LuaZhuJueJuQingMgr.TaskId,"GongTangBianJie",MsgPackImpl.pack(empty))
			CUIManager.CloseUI(CLuaUIResources.ChooseProofWnd)
		else

		end
		g_MessageMgr:ShowMessage(messageName)
	end
end

return LuaChooseProofWnd
