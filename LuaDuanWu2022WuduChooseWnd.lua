local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"

LuaDuanWu2022WuduChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDuanWu2022WuduChooseWnd, "AdditionLabel", "AdditionLabel", UILabel)
RegistChildComponent(LuaDuanWu2022WuduChooseWnd, "ChooseItem", "ChooseItem", GameObject)
RegistChildComponent(LuaDuanWu2022WuduChooseWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaDuanWu2022WuduChooseWnd, "ResetBtn", "ResetBtn", GameObject)
RegistChildComponent(LuaDuanWu2022WuduChooseWnd, "ResetDescLabel", "ResetDescLabel", UILabel)

--@endregion RegistChildComponent end

function LuaDuanWu2022WuduChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtnClick()
	end)


	
	UIEventListener.Get(self.ResetBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResetBtnClick()
	end)


    --@endregion EventBind end
end

function LuaDuanWu2022WuduChooseWnd:Init()
	-- 设置数据初始化
	local setting = DuanWu_Setting.GetData()
	self.NameList = setting.QuWuDuDifficultyName
	self.DefaultLevel = {
		LuaDuanWu2022Mgr.selfPlayerRankData.Difficulty1, 
		LuaDuanWu2022Mgr.selfPlayerRankData.Difficulty2, 
		LuaDuanWu2022Mgr.selfPlayerRankData.Difficulty3, 
	}
	self.DescFormula = {
		setting.QuWuDuHpDisplayFormulaId,
		setting.QuWuDuAttDisplayFormulaId,
		setting.QuWuDuDebuffDisplayFormulaId,
		setting.QuWuDuDefDisplayFormulaId,
		setting.QuWuDuScoreDisplayFormulaId,
	}

	self.DescLabelList = {}
	self.DescLabelList[5] = self.AdditionLabel

	self.AddBtnList = {}

	self.CurrentLevel = {1,1,1}
	self.MaxLevel = setting.QuWuDuDifficultyMaxLv

	-- 难度显示
	local des = ""
	for i = 1, 3 do
		self:InitItem(i)
		des = des .. SafeStringFormat3(LocalString.GetString("%s%s级 "), self.NameList[i-1], tostring(self.DefaultLevel[i]))
	end
	self.ResetDescLabel.text = des

	-- 设置默认难度
	if LuaDuanWu2022Mgr.selfPlayerRankData.Difficulty1 and LuaDuanWu2022Mgr.selfPlayerRankData.Difficulty2 and 
		LuaDuanWu2022Mgr.selfPlayerRankData.Difficulty3 then
		self:ResetLevel()
	else
		self.ResetBtn.gameObject:SetActive(false)
		self.ResetDescLabel.gameObject:SetActive(false)
		self.transform:Find("Anchor/Label").gameObject:SetActive(false)
	end


	local desStr = {LocalString.GetString("怪物气血约"), LocalString.GetString("怪物攻击约"), LocalString.GetString("怪物技能附带持续效果约")}
	-- 难度显示
	for i = 1, 3 do
		local item = self.ChooseItem.transform:Find(tostring(i))
		item:Find("Desc/Label"):GetComponent(typeof(UILabel)).text = desStr[i]
		if i == 1 then
			item:Find("Desc2/Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("怪物防御约")
		end
	end
end

function LuaDuanWu2022WuduChooseWnd:ResetLevel()
	for i = 1, 3 do
		-- 设置并且调用回调函数
		self.AddBtnList[i]:SetValue(self.DefaultLevel[i], true)
	end
end

function LuaDuanWu2022WuduChooseWnd:InitItem(index)
	local item = self.ChooseItem.transform:Find(tostring(index))
	local label = item.transform:Find("Texture/Label"):GetComponent(typeof(UILabel))
	local addSub = item.transform:Find("AddSubBtn"):GetComponent(typeof(QnAddSubAndInputButton))
	local descLabel = item.transform:Find("Desc"):GetComponent(typeof(UILabel))

	-- 缓存描述文本用于更新
	self.DescLabelList[index] = descLabel
	self.AddBtnList[index] = addSub
	if index == 1 then
		self.DescLabelList[4] = item.transform:Find("Desc2"):GetComponent(typeof(UILabel))
	end
	
	label.text = self.NameList[index-1]
	addSub.onValueChanged = DelegateFactory.Action_uint(function(level)
		self:OnLevelChange(index, level)
	end)
	addSub:SetMinMax(1, self.MaxLevel, 1)
	addSub:SetValue(1, true)
end

-- 更新说明数值
function LuaDuanWu2022WuduChooseWnd:OnLevelChange(index, level)
	self.CurrentLevel[index] = level
	self.DescLabelList[index].text = "+"..GetFormula(self.DescFormula[index])(nil, nil, {level})*100 .. "%"

	if index == 1 then
		self.DescLabelList[4].text = "+"..GetFormula(self.DescFormula[4])(nil, nil, {level})*100 .. "%"
	end

	self.DescLabelList[5].text = "+"..GetFormula(self.DescFormula[5])(nil, nil, {self.CurrentLevel[1],self.CurrentLevel[2],self.CurrentLevel[3]})*100 .. "%"
end

--@region UIEvent

function LuaDuanWu2022WuduChooseWnd:OnEnterBtnClick()
	Gac2Gas.QWD_SelectDifficulty(self.CurrentLevel[1], self.CurrentLevel[2], self.CurrentLevel[3])
end

function LuaDuanWu2022WuduChooseWnd:OnResetBtnClick()
	self:ResetLevel()
end

--@endregion UIEvent

