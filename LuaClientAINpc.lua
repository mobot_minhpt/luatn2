local CClientAINpc = import "L10.Game.CClientAINpc"
local CClientObject = import "L10.Game.CClientObject"
local Setting = import "L10.Engine.Setting"
local CPos = import "L10.Engine.CPos"
local EMoveType = import "L10.Engine.EMoveType"
local PathFindDefs = import "L10.Engine.PathFinding.PathFindDefs"
local EBarrierType = import "L10.Engine.EBarrierType"
local CLogMgr = import "L10.CLogMgr"
local CTickMgr = import "L10.Engine.CTickMgr"

g_ClientAINpcHandlers = {}

LuaClientAINpc = class()
RegistClassMember(LuaClientAINpc, "m_Core")		-- CClientAINpc
RegistClassMember(LuaClientAINpc, "m_EngineId")

function LuaClientAINpc:Ctor(templateId, pos)
	self.m_Core = CClientAINpc.CreateNpc(pos)
	self.m_Core:Init(templateId)

	self.m_EngineId = self.m_Core.EngineId
	g_ClientAINpcHandlers[self.m_EngineId] = self
    self.m_Core:InitLuaSelf(self)
end

function LuaClientAINpc:Destroy()
	self.m_Core:Destroy()
end

function LuaClientAINpc:OnDestroy()
    g_ClientAINpcHandlers[self.m_EngineId] = nil
	-- 停AI
	self:DetachFlowChart()
end

function LuaClientAINpc:AttachFlowChart(flowchartName, chartTblPrefix)
	local id = self.m_EngineId

	local isMapExist = true
	local tbl = (chartTblPrefix and _G["Flowchart_"..chartTblPrefix]) or Flowchart_ClientAINpc
	for name in string.gmatch(flowchartName, "([^%.][^%.]*)") do
		if tbl[name] then
			tbl = tbl[name]
		else
			isMapExist = false
			assert(false)
			return
		end
	end

	local originalName = flowchartName
	flowchartName = (chartTblPrefix and chartTblPrefix.."." or  "ClientAINpc.")..flowchartName
	
	self:DetachFlowChart()
	
	local status = flowchart.init(self, EFlowClass.ClientAINpc)
	if not status then
		return CLogMgr.LogError("LuaClientAINpc:AttachFlowChart init failed, flowchartName = "..flowchartName)
	end

	status = flowchart.load(self, flowchartName)
	if not status then
		return CLogMgr.LogError("LuaClientAINpc:AttachFlowChart attach failed, flowchartName = "..flowchartName)
	end
	status = flowchart.start(self)
	if not status then
		return CLogMgr.LogError("LuaClientAINpc:AttachFlowChart start failed, flowchartName = "..flowchartName)
	end

	g_ClientAIRefObj[id] = true
	if not g_ClientAITick then
		g_ClientAITick = RegisterTick(function() flowchart.step() end, g_ClientAIInterval)
	end
end

function LuaClientAINpc:DetachFlowChart()
	local id = self.m_EngineId
	if not g_ClientAIRefObj[id] then return end

	flowchart.stop(self)
	flowchart.deinit(self)

	g_ClientAIRefObj[id] = nil
	if not next(g_ClientAIRefObj) and g_ClientAITick then
		UnRegisterTick(g_ClientAITick)
		g_ClientAITick = nil
	end
end

-------------------------------------------------------------------------------
-- AI Interface

function LuaClientAINpc:GetName()
	return self.m_Core.Name
end

function LuaClientAINpc:GetEngineId()
	return self.m_EngineId
end

function LuaClientAINpc:AIMoveTo(x, y, speed)
	local targetVec = CreateFromClass(CPos, x * Setting.eGridSpan, y * Setting.eGridSpan)
	local pathType = PathFindDefs.EFindPathType.eFPT_HypoLine
	local barrierType = EBarrierType.eBT_LowBarrier
	local moveType = EMoveType.Normal
	local regionLimit = PathFindDefs.MAX_REGION_LIMIT

    self.m_Core:MoveTo(targetVec, speed * Setting.eGridSpan, 0,0,0,
		pathType, barrierType, false, moveType, regionLimit)
end

function LuaClientAINpc:StopMove()
	self.m_Core:StopMove()
end

function LuaClientAINpc:DoSay(content, duration, soundpath)
	CClientObject.SendSayContent(self.m_EngineId, content, duration, soundpath)
end

-- AI Interface
-------------------------------------------------------------------------------
-- Basic Test Script
function LuaClientAINpcTest()
    local pos = CreateFromClass(CPos, 181 * Setting.eGridSpan, 132 * Setting.eGridSpan)
	local npc = LuaClientAINpc:new(20000056, pos)

	local eid = npc.m_EngineId
	RegisterTickOnce(function()
		local _npc = g_ClientAINpcHandlers[eid]
		if not _npc then return end
        _npc:AttachFlowChart("Test")
    end, 1000)
end

