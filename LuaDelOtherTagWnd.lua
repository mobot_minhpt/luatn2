local CBaseWnd = import "L10.UI.CBaseWnd"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local FamilyTree_Setting = import "L10.Game.FamilyTree_Setting"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaDelOtherTagWnd=class()

RegistClassMember(LuaDelOtherTagWnd,"CancelButton")
RegistClassMember(LuaDelOtherTagWnd,"OKButton")
RegistClassMember(LuaDelOtherTagWnd,"DesLabel")
RegistClassMember(LuaDelOtherTagWnd,"CostAndOwnMoney")

function LuaDelOtherTagWnd:Awake()
    self.CancelButton = self.transform:Find("Anchor/Grid/CancelButton").gameObject
    self.OKButton = self.transform:Find("Anchor/Grid/OKButton").gameObject
    self.DesLabel = self.transform:Find("Anchor/DesLabel"):GetComponent(typeof(UILabel))
    self.DesLabel.text = g_MessageMgr:FormatMessage("Del_Tag_Confirm",luaFamilyMgr.m_TagIndexData[luaFamilyMgr.DelTagId].tagName)
    
    self.CostAndOwnMoney = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.CostAndOwnMoney:SetType(EnumMoneyType.LingYu,EnumPlayScoreKey.NONE,true)
    self.CostAndOwnMoney:SetCost(FamilyTree_Setting.GetData().DelOthersTagCostJade)
    
    UIEventListener.Get(self.CancelButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:Close()
    end)
    UIEventListener.Get(self.OKButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        if not CShopMallMgr.TryCheckEnoughJade(FamilyTree_Setting.GetData().DelOthersTagCostJade, 0) then
            return
        end
        if luaFamilyMgr.DelTagId ~= "" then
            Gac2Gas.TryTagPlayerFamilyTree(CFamilyTreeMgr.Instance.CenterPlayerID,"", luaFamilyMgr.DelTagId, EnumFamilyTreeTagOperationType.eDelOther, false) -- body
        end
        self:Close()
    end)
end

function LuaDelOtherTagWnd:Close()
	self.transform:GetComponent(typeof(CBaseWnd)):Close()
end
