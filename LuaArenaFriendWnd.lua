local QnAdvanceGridView     = import "L10.UI.QnAdvanceGridView"
local CIMMgr                = import "L10.Game.CIMMgr"
local CExpressionMgr        = import "L10.Game.CExpressionMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel            = import "L10.Game.EChatPanel"
local AlignType             = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr        = import "CPlayerInfoMgr"

LuaArenaFriendWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaArenaFriendWnd, "tableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaArenaFriendWnd, "onlineNum", "OnlineNum", UILabel)

--@endregion RegistChildComponent end

function LuaArenaFriendWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaArenaFriendWnd:OnEnable()
    g_ScriptEvent:AddListener("ArenaUpdateFriendInfos", self, "UpdateInfo")
end

function LuaArenaFriendWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ArenaUpdateFriendInfos", self, "UpdateInfo")
end

function LuaArenaFriendWnd:Init()
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaArenaMgr.friendInfos
        end,
        function(item, index)
            self:InitItem(item, index)
        end
    )
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        CPlayerInfoMgr.ShowPlayerPopupMenu(LuaArenaMgr.friendInfos[row + 1].id, EnumPlayerInfoContext.Arena, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
    end)
    self:UpdateInfo()
end

function LuaArenaFriendWnd:InitItem(item, index)
    local info = LuaArenaMgr.friendInfos[index + 1]

    local basicInfo = CIMMgr.Inst:GetBasicInfo(info.id)
    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    portrait:LoadPortrait(CUICommonDef.GetPortraitName(basicInfo.Class, basicInfo.Gender, basicInfo.Expression), true)
    local level = item.transform:Find("Portrait/Level"):GetComponent(typeof(UILabel))
    level.text = CUICommonDef.GetColoredLevelString(basicInfo.XianFanStatus > 0, basicInfo.Level, Color.white)
    item.transform:Find("Name"):GetComponent(typeof(UILabel)).text = basicInfo.Name
    LuaUtils.SetLocalPositionZ(portrait.transform, info.isOnline and 0 or -1)
    item.transform:GetComponent(typeof(UIWidget)).alpha = info.isOnline and 1 or 0.5
    local stateLabel = item.transform:Find("State"):GetComponent(typeof(UILabel))
    stateLabel.gameObject:SetActive(info.state > 0)
    if info.state > 0 then stateLabel.text = LuaArenaMgr:GetStateText(info.state) end

    item.transform:Find("Dan").gameObject:SetActive(info.rankScore > 0)
    if info.rankScore > 0 then
        local largeDan, smallDan = LuaArenaMgr:GetLargeAndSmallDan(info.rankScore)
        item.transform:Find("Dan/Large"):GetComponent(typeof(CUITexture)):LoadMaterial(Arena_DanInfo.GetData(largeDan).Icon)
        item.transform:Find("Dan/Small"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaArenaMgr.smallDanMatPaths[smallDan])
    end
end

function LuaArenaFriendWnd:UpdateInfo()
    self.tableView:ReloadData(true, false)

    local onlineNum = 0
    local infos = LuaArenaMgr.friendInfos
    for i = 1, #infos do
        if infos[i].isOnline then
            onlineNum = onlineNum + 1
        end
    end
    self.onlineNum.text = SafeStringFormat3(LocalString.GetString("在线%d/%d"), onlineNum, #infos)
end

--@region UIEvent
--@endregion UIEvent
