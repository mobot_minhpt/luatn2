local CItemInfoMgr   = import "L10.UI.CItemInfoMgr"
local AlignType      = import "L10.UI.CItemInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CButton        = import "L10.UI.CButton"

LuaDouDiZhuBaoXiangApplyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "desc")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "time")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "portrait")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "rank")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "honor")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "awardTemplate")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "awardGrid")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "countDown")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "applyButton")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "rankButton")

RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "tick")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "endTimeStamp")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "buttonTick")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "buttonTime")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "isSignUp")
RegistClassMember(LuaDouDiZhuBaoXiangApplyWnd, "isApplyButtonClicked")

function LuaDouDiZhuBaoXiangApplyWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaDouDiZhuBaoXiangApplyWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.desc = anchor:Find("Desc/Label"):GetComponent(typeof(UILabel))
    self.time = anchor:Find("Time"):GetComponent(typeof(UILabel))
    self.portrait = anchor:Find("PlayerInfo/Portrait"):GetComponent(typeof(CUITexture))
    self.rank = anchor:Find("PlayerInfo/Rank"):GetComponent(typeof(UILabel))
    self.honor = anchor:Find("PlayerInfo/Honor"):GetComponent(typeof(UILabel))
    self.awardTemplate = anchor:Find("Award/Template").gameObject
    self.awardGrid = anchor:Find("Award/Grid"):GetComponent(typeof(UIGrid))
    self.countDown = anchor:Find("CountDown"):GetComponent(typeof(UILabel))
    self.applyButton = anchor:Find("ApplyButton"):GetComponent(typeof(CButton))
    self.rankButton = anchor:Find("RankButton").gameObject
end

function LuaDouDiZhuBaoXiangApplyWnd:InitEventListener()
    UIEventListener.Get(self.applyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnApplyButtonClick()
	end)

    UIEventListener.Get(self.rankButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)
end

function LuaDouDiZhuBaoXiangApplyWnd:InitActive()
    self.awardTemplate:SetActive(false)
end


function LuaDouDiZhuBaoXiangApplyWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncQiangXiangZiSignUpInfo", self, "OnSyncQiangXiangZiSignUpInfo")
    g_ScriptEvent:AddListener("SyncQiangXiangZiSignUpResult", self, "OnSyncQiangXiangZiSignUpResult")
end

function LuaDouDiZhuBaoXiangApplyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncQiangXiangZiSignUpInfo", self, "OnSyncQiangXiangZiSignUpInfo")
    g_ScriptEvent:RemoveListener("SyncQiangXiangZiSignUpResult", self, "OnSyncQiangXiangZiSignUpResult")
end

function LuaDouDiZhuBaoXiangApplyWnd:OnSyncQiangXiangZiSignUpInfo(isSignUp, awards, myRank, honor, nextRefreshTime)
    self.rank.text = myRank
    self.honor.text = honor

    self:OnSyncQiangXiangZiSignUpResult(isSignUp)
    self:InitGrid(awards)

    self:StartTick(nextRefreshTime)
end

function LuaDouDiZhuBaoXiangApplyWnd:OnSyncQiangXiangZiSignUpResult(isSignUp)
    self.isSignUp = isSignUp
    if self.isApplyButtonClicked then
        self:StartButtonTick()
        self.isApplyButtonClicked = false
    else
        self:ClearButtonTick()
        self:InitApplyButtonLabel()
    end
end

function LuaDouDiZhuBaoXiangApplyWnd:InitGrid(awards)
    Extensions.RemoveAllChildren(self.awardGrid.transform)
    for i = 1, #awards, 2 do
        local k = awards[i]
        local v = awards[i + 1]
        local child = NGUITools.AddChild(self.awardGrid.gameObject, self.awardTemplate)
        child:SetActive(true)

        child.transform:Find("Count"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("X%d", v)
        local item = Item_Item.GetData(k)
        child.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(k, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    self.awardGrid:Reposition()
end


function LuaDouDiZhuBaoXiangApplyWnd:Init()
    self.portrait:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
    self.countDown.text = "00:00"
    self.desc.text = g_MessageMgr:FormatMessage("DOUDIZHU_KAIBAOXIANG_RULE")
    self.time.text = g_MessageMgr:FormatMessage("DOUDIZHU_KAIBAOXIANG_TIME")
    self.rank.text = "0"
    self.honor.text = "0"
    self.isSignUp = nil
    self.isApplyButtonClicked = false

    Gac2Gas.RequestQiangXiangZiSignUpInfo()
end

function LuaDouDiZhuBaoXiangApplyWnd:StartTick(nextRefreshTime)
    self:ClearTick()
    if nextRefreshTime <= CServerTimeMgr.Inst.timeStamp then return end

    self.endTimeStamp = nextRefreshTime
    self:UpdateCountDown()
    self.tick = RegisterTick(function()
        self:UpdateCountDown()
    end, 1000)
end

function LuaDouDiZhuBaoXiangApplyWnd:UpdateCountDown()
    if not self.endTimeStamp then
        self:ClearTick()
        return
    end

    local leftTime = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
    if leftTime < 0 then
        self:ClearTick()
        self.countDown.text = "00:00"
        Gac2Gas.RequestQiangXiangZiSignUpInfo()
        return
    end

    local hour = math.floor(leftTime / 60 / 60)
    local minute = math.floor(leftTime % 3600 / 60)
    local second = math.floor(leftTime % 60)
    self.countDown.text = SafeStringFormat3("%02d:%02d:%02d", hour, minute, second)
end

function LuaDouDiZhuBaoXiangApplyWnd:StartButtonTick()
    self:ClearButtonTick()
    self.applyButton.Enabled = false
    self.buttonTime = LuaDouDiZhuBaoXiangMgr.applyButtonCD

    self:UpdateApplyButtonLabel()
    self.buttonTick = RegisterTick(function ()
        self.buttonTime = self.buttonTime - 1
        self:UpdateApplyButtonLabel()
    end, 1000)
end

-- 更新报名按钮显示
function LuaDouDiZhuBaoXiangApplyWnd:UpdateApplyButtonLabel()
    if self.buttonTime <= 0 then
        self:ClearButtonTick()
        self:InitApplyButtonLabel()
        return
    end

    self.applyButton.label.text = SafeStringFormat3(self.isSignUp and LocalString.GetString("取消报名(%ss)") or LocalString.GetString("报  名(%ss)"), self.buttonTime)
end

-- 报名按钮初始显示
function LuaDouDiZhuBaoXiangApplyWnd:InitApplyButtonLabel()
    self.applyButton.Enabled = true
    self.applyButton.label.text = self.isSignUp and LocalString.GetString("取消报名") or LocalString.GetString("报  名")
end

function LuaDouDiZhuBaoXiangApplyWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
        self.tick = nil
    end
end

function LuaDouDiZhuBaoXiangApplyWnd:ClearButtonTick()
    if self.buttonTick then
        UnRegisterTick(self.buttonTick)
        self.buttonTick = nil
    end
end

function LuaDouDiZhuBaoXiangApplyWnd:OnDestroy()
    self:ClearTick()
    self:ClearButtonTick()
end

--@region UIEvent

function LuaDouDiZhuBaoXiangApplyWnd:OnApplyButtonClick()
    if self.isSignUp == nil then return end

    if not self.isSignUp then
        Gac2Gas.RequestSignUpQiangXiangZi()
    else
        Gac2Gas.RequestCancelSignUpQiangXiangZi()
    end

    self.isApplyButtonClicked = true
end

function LuaDouDiZhuBaoXiangApplyWnd:OnRankButtonClick()
    LuaPlayerRankMgr:ShowPlayerRankWnd(41000262, LocalString.GetString("每日排行榜"), nil, nil, LocalString.GetString("积分"), nil)
end

--@endregion UIEvent
