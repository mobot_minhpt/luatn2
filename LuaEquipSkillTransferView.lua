require("common/common_include")

local UIScrollView = import "UIScrollView"
local UIGrid  = import "UIGrid"
local Extensions = import "Extensions"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CIdentifyEquipCell = import "L10.UI.Equipment.CIdentifyEquipCell"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local Item_Item = import "L10.Game.Item_Item"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

local MessageWndManager = import "L10.UI.MessageWndManager"
local Vector3 = import "UnityEngine.Vector3"
local CUIFx = import "L10.UI.CUIFx"

LuaEquipSkillTransferView = class()
RegistClassMember(LuaEquipSkillTransferView,"inited")
RegistClassMember(LuaEquipSkillTransferView,"gameObject")
RegistClassMember(LuaEquipSkillTransferView,"transform")

RegistClassMember(LuaEquipSkillTransferView,"m_ScrollView")
RegistClassMember(LuaEquipSkillTransferView,"m_Grid")
RegistClassMember(LuaEquipSkillTransferView,"m_EquipCellTemplate")

RegistClassMember(LuaEquipSkillTransferView,"m_SrcSelectButton")
RegistClassMember(LuaEquipSkillTransferView,"m_SrcEquipCell")
RegistClassMember(LuaEquipSkillTransferView,"m_SrcSkillIcon")
RegistClassMember(LuaEquipSkillTransferView,"m_SrcSkillFx")
RegistClassMember(LuaEquipSkillTransferView,"m_SrcSkillLevelLabel")
RegistClassMember(LuaEquipSkillTransferView,"m_SrcSkillNameLabel")

RegistClassMember(LuaEquipSkillTransferView,"m_DstSelectButton")
RegistClassMember(LuaEquipSkillTransferView,"m_DstEquipCell")
RegistClassMember(LuaEquipSkillTransferView,"m_DstSkillIcon")
RegistClassMember(LuaEquipSkillTransferView,"m_DstSkillFx")
RegistClassMember(LuaEquipSkillTransferView,"m_DstSkillLevelLabel")
RegistClassMember(LuaEquipSkillTransferView,"m_DstSkillNameLabel")

RegistClassMember(LuaEquipSkillTransferView,"m_Cells")
RegistClassMember(LuaEquipSkillTransferView,"m_NeedItem1Id")
RegistClassMember(LuaEquipSkillTransferView,"m_NeedItem1Num")
RegistClassMember(LuaEquipSkillTransferView,"m_NeedItem2Id")
RegistClassMember(LuaEquipSkillTransferView,"m_NeedItem2Num")

RegistClassMember(LuaEquipSkillTransferView,"m_CostItem1")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem1IconTexture")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem1DisableSprite")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem1QualitySprite")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem1AmountLabel")

RegistClassMember(LuaEquipSkillTransferView,"m_CostItem2")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem2IconTexture")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem2DisableSprite")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem2QualitySprite")
RegistClassMember(LuaEquipSkillTransferView,"m_CostItem2AmountLabel")

RegistClassMember(LuaEquipSkillTransferView,"m_TransferButton")
RegistClassMember(LuaEquipSkillTransferView,"m_InfoButton")


function LuaEquipSkillTransferView:InitWithGameObject(viewRootGo)
    if self.inited then
    	return
    end
    self.gameObject = viewRootGo
    self.transform = viewRootGo.transform

    self.m_ScrollView = self.transform:Find("EquipList/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Grid = self.transform:Find("EquipList/ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.m_EquipCellTemplate = self.transform:Find("EquipList/ScrollView/EquipCell").gameObject

    self.m_SrcSelectButton = self.transform:Find("SwitchRoot/SrcCell"):GetComponent(typeof(CButton))
    self.m_SrcEquipCell = self.transform:Find("SwitchRoot/SrcCell/EquipCell"):GetComponent(typeof(CIdentifyEquipCell))
    self.m_SrcSkillIcon = self.transform:Find("SwitchRoot/SrcCell/SkillCell/SkillIcon"):GetComponent(typeof(CUITexture))
    self.m_SrcSkillFx = self.transform:Find("SwitchRoot/SrcCell/SkillCell/SkillIcon/SkillFx"):GetComponent(typeof(CUIFx))
    self.m_SrcSkillLevelLabel = self.transform:Find("SwitchRoot/SrcCell/SkillCell/SkillIcon/LevelLabel"):GetComponent(typeof(UILabel))
    self.m_SrcSkillNameLabel = self.transform:Find("SwitchRoot/SrcCell/SkillCell/Label"):GetComponent(typeof(UILabel))
    self.m_DstSelectButton = self.transform:Find("SwitchRoot/DstCell"):GetComponent(typeof(CButton))
    self.m_DstEquipCell = self.transform:Find("SwitchRoot/DstCell/EquipCell"):GetComponent(typeof(CIdentifyEquipCell))
    self.m_DstSkillIcon = self.transform:Find("SwitchRoot/DstCell/SkillCell/SkillIcon"):GetComponent(typeof(CUITexture))
    self.m_DstSkillFx = self.transform:Find("SwitchRoot/DstCell/SkillCell/SkillIcon/SkillFx"):GetComponent(typeof(CUIFx))
    self.m_DstSkillLevelLabel = self.transform:Find("SwitchRoot/DstCell/SkillCell/SkillIcon/LevelLabel"):GetComponent(typeof(UILabel))
    self.m_DstSkillNameLabel = self.transform:Find("SwitchRoot/DstCell/SkillCell/Label"):GetComponent(typeof(UILabel))

    self.m_CostItem1 = self.transform:Find("Cost/ItemCell1").gameObject
    self.m_CostItem1IconTexture = self.transform:Find("Cost/ItemCell1/IconTexture"):GetComponent(typeof(CUITexture))
    self.m_CostItem1DisableSprite = self.transform:Find("Cost/ItemCell1/DisableSprite").gameObject
    self.m_CostItem1QualitySprite = self.transform:Find("Cost/ItemCell1/QualitySprite"):GetComponent(typeof(UISprite))
    self.m_CostItem1AmountLabel = self.transform:Find("Cost/ItemCell1/AmountLabel"):GetComponent(typeof(UILabel))

    self.m_CostItem2 = self.transform:Find("Cost/ItemCell2").gameObject
    self.m_CostItem2IconTexture = self.transform:Find("Cost/ItemCell2/IconTexture"):GetComponent(typeof(CUITexture))
    self.m_CostItem2DisableSprite = self.transform:Find("Cost/ItemCell2/DisableSprite").gameObject
    self.m_CostItem2QualitySprite = self.transform:Find("Cost/ItemCell2/QualitySprite"):GetComponent(typeof(UISprite))
    self.m_CostItem2AmountLabel = self.transform:Find("Cost/ItemCell2/AmountLabel"):GetComponent(typeof(UILabel))

    self.m_TransferButton = self.transform:Find("SwitchButton"):GetComponent(typeof(CButton))
    self.m_InfoButton = self.transform:Find("InfoButton").gameObject

    self.m_EquipCellTemplate:SetActive(false)

    CommonDefs.AddOnClickListener(self.m_SrcSelectButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSrcCellClick() end), false)
    CommonDefs.AddOnClickListener(self.m_DstSelectButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDstCellClick() end), false)

    CommonDefs.AddOnClickListener(self.m_SrcEquipCell.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSrcEquipIconClick() end), false)
    CommonDefs.AddOnClickListener(self.m_DstEquipCell.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDstEquipIconClick() end), false)
    CommonDefs.AddOnClickListener(self.m_SrcSkillIcon.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSrcEquipSkillIconClick() end), false)
    CommonDefs.AddOnClickListener(self.m_DstSkillIcon.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDstEquipSkillIconClick() end), false)

    CommonDefs.AddOnClickListener(self.m_CostItem1, DelegateFactory.Action_GameObject(function(go) self:OnCostItemClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_CostItem2, DelegateFactory.Action_GameObject(function(go) self:OnCostItemClick(go) end), false)

    CommonDefs.AddOnClickListener(self.m_TransferButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnTransferButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)

    self.inited = true
end

function LuaEquipSkillTransferView:Init()

	if not self.inited then return end

    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_Cells = {}
    local items = LuaEquipIdentifySkillMgr.GetCanExchangeEquipments()

    for _, itemId in ipairs(items) do

    	local item = CItemMgr.Inst:GetById(itemId)
    	if item then
    		local go = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_EquipCellTemplate)
    		go:SetActive(true)
            local itemCell = go:GetComponent(typeof(CIdentifyEquipCell))
            itemCell:Init(itemId)
            CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnItemCellClick(go) end), false)
            table.insert(self.m_Cells, itemCell)
    	end

    end
    self.m_Grid:Reposition()
    self.m_ScrollView:ResetPosition()

    self.m_SrcSelectButton.Selected = true
    self.m_SrcEquipCell:Init(nil)
    self.m_SrcSkillIcon:Clear()
    self.m_SrcSkillFx:DestroyFx()
    self.m_SrcSkillLevelLabel.gameObject:SetActive(false)
    self.m_SrcSkillLevelLabel.text = nil
    self.m_SrcSkillNameLabel.text = nil

    self.m_DstSelectButton.Selected = false
    self.m_DstEquipCell:Init(nil)
    self.m_DstSkillIcon:Clear()
    self.m_DstSkillFx:DestroyFx()
    self.m_DstSkillLevelLabel.gameObject:SetActive(false)
    self.m_DstSkillLevelLabel.text = nil
    self.m_DstSkillNameLabel.text = nil

    self:InitCostItems(0,0,0,0)
end

function LuaEquipSkillTransferView:OnSendItem(itemId)
    if not self.inited then return end
    if not itemId then return end

    if self.m_SrcEquipCell.equipId == itemId then
        self:InitSrcCell(itemId)
        self:UpdateItemCellAppearance()
        self:InitCostItems(self.m_NeedItem1Id, self.m_NeedItem1Num, self.m_NeedItem2Id, self.m_NeedItem2Num)
    elseif self.m_DstEquipCell.equipId == itemId then
        self:InitDstCell(itemId)
        self:UpdateItemCellAppearance()
        self:InitCostItems(self.m_NeedItem1Id, self.m_NeedItem1Num, self.m_NeedItem2Id, self.m_NeedItem2Num)
    else
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if not commonItem or not commonItem.IsItem then
            return
        end
        --如果消耗道具数量发生变化，更新一下
        if commonItem.TemplateId == self.m_NeedItem1Id or commonItem.TemplateId == self.m_NeedItem2Id then
             self:InitCostItems(self.m_NeedItem1Id, self.m_NeedItem1Num, self.m_NeedItem2Id, self.m_NeedItem2Num)
        end
    end

end

function LuaEquipSkillTransferView:OnSetItemAt(place, pos, oldItemId, newItemId)
    self:InitCostItems(self.m_NeedItem1Id, self.m_NeedItem1Num, self.m_NeedItem2Id, self.m_NeedItem2Num)
end

function LuaEquipSkillTransferView:InitSrcCell(equipId)
    self.m_SrcEquipCell:Init(equipId)
    local commonItem = equipId and CItemMgr.Inst:GetById(equipId)
    if commonItem and commonItem.IsEquip and commonItem.Equip.Identifiable > 0 and Skill_AllSkills.Exists(commonItem.Equip.FixedIdentifySkillId) then
        local skill = Skill_AllSkills.GetData(commonItem.Equip.FixedIdentifySkillId)
        self.m_SrcSkillIcon:LoadSkillIcon(skill.SkillIcon)
        self.m_SrcSkillLevelLabel.gameObject:SetActive(true)
        self.m_SrcSkillLevelLabel.text = SafeStringFormat3("lv.%d", skill.Level)
        self.m_SrcSkillNameLabel.text = skill.Name
    else
        self.m_SrcSkillIcon:Clear()
        self.m_SrcSkillLevelLabel.gameObject:SetActive(false)
        self.m_SrcSkillLevelLabel.text = nil
        self.m_SrcSkillNameLabel.text = nil
    end
end

function LuaEquipSkillTransferView:InitDstCell(equipId)
    self.m_DstEquipCell:Init(equipId)
    local commonItem = equipId and CItemMgr.Inst:GetById(equipId)
    if commonItem and commonItem.IsEquip and commonItem.Equip.Identifiable > 0 and Skill_AllSkills.Exists(commonItem.Equip.FixedIdentifySkillId) then
        local skill = Skill_AllSkills.GetData(commonItem.Equip.FixedIdentifySkillId)
        self.m_DstSkillIcon:LoadSkillIcon(skill.SkillIcon)
        self.m_DstSkillLevelLabel.gameObject:SetActive(true)
        self.m_DstSkillLevelLabel.text = SafeStringFormat3("lv.%d", skill.Level)
        self.m_DstSkillNameLabel.text = skill.Name
    else
        self.m_DstSkillIcon:Clear()
        self.m_DstSkillLevelLabel.gameObject:SetActive(false)
        self.m_DstSkillLevelLabel.text = nil
        self.m_DstSkillNameLabel.text = nil
    end
end

function LuaEquipSkillTransferView:UpdateCostInfo(equip1Id, equip2Id, needJade, itemTemplateId1, num1, itemTemplateId2, num2)
    local srcEquipId = self.m_SrcEquipCell.equipId
    local dstEquipId = self.m_DstEquipCell.equipId
    --严格检查顺序
    if srcEquipId == equip1Id and dstEquipId == equip2Id then
        self:InitCostItems(itemTemplateId1, num1, itemTemplateId2, num2)
    end
end

function LuaEquipSkillTransferView:InitCostItems(itemTemplateId1, num1, itemTemplateId2, num2)
    self.m_NeedItem1Id = itemTemplateId1 or 0
    self.m_NeedItem1Num = num1 or 0
    self.m_NeedItem2Id = itemTemplateId2 or 0
    self.m_NeedItem2Num = num2 or 0
    local item1 = Item_Item.GetData(itemTemplateId1)
    if not item1 then
        self.m_CostItem1IconTexture:Clear()
         --self.m_CostItem1QualitySprite = nil
        self.m_CostItem1DisableSprite:SetActive(false)
        self.m_CostItem1AmountLabel.text = nil
    else
        self.m_CostItem1IconTexture:LoadMaterial(item1.Icon)
        --self.m_CostItem1QualitySprite = nil
        local ownCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemTemplateId1)
        self.m_CostItem1DisableSprite:SetActive(ownCount < num1)
        if ownCount>=num1 then
            self.m_CostItem1AmountLabel.text = SafeStringFormat3("%d/%d", ownCount, num1)
        else
            self.m_CostItem1AmountLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", ownCount, num1)
        end
    end

    local item2 = Item_Item.GetData(itemTemplateId2)
    if not item2 then
        self.m_CostItem2IconTexture:Clear()
         --self.m_CostItem2QualitySprite = nil
        self.m_CostItem2DisableSprite:SetActive(false)
        self.m_CostItem2AmountLabel.text = nil
    else
        self.m_CostItem2IconTexture:LoadMaterial(item2.Icon)
        --self.m_CostItem2QualitySprite = nil
        local ownCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemTemplateId2)
        self.m_CostItem2DisableSprite:SetActive(ownCount < num2)
        if ownCount>=num2 then
            self.m_CostItem2AmountLabel.text = SafeStringFormat3("%d/%d", ownCount, num2)
        else
            self.m_CostItem2AmountLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", ownCount, num2)
        end
    end
    if item1 and item2 then
        self.m_TransferButton.Enabled = true
    else
        self.m_TransferButton.Enabled = false
    end
end

function LuaEquipSkillTransferView:OnItemCellClick(go)
    local curCell = go:GetComponent(typeof(CIdentifyEquipCell))
    if curCell.equipId == nil then return end
    local srcEquipId = self.m_SrcEquipCell.equipId
    local dstEquipId = self.m_DstEquipCell.equipId
    local srcCommonItem = CItemMgr.Inst:GetById(srcEquipId)
    local dstCommonItem = CItemMgr.Inst:GetById(dstEquipId)
    local curCommonItem = CItemMgr.Inst:GetById(curCell.equipId)
    local srcEquip = srcCommonItem and srcCommonItem.Equip
    local dstEquip = dstCommonItem and dstCommonItem.Equip
    local curEquip = curCommonItem and curCommonItem.Equip

    local changed = false

    if curCell.equipId == srcEquipId then
        self:InitSrcCell(nil)
        self:UpdateItemCellAppearance()
        changed = true
    elseif curCell.equipId == dstEquipId then
        self:InitDstCell(nil)
        self:UpdateItemCellAppearance()
        changed = true
    elseif self.m_SrcSelectButton.Selected then
        --如果当前src被选中，则和dst比较是否可以互换
        if dstEquip==nil or self:CheckEquipCanExchangeIdentifySkill(curEquip, dstEquip) then
            self:InitSrcCell(curCell.equipId)
            if dstEquip == nil then
                --修改默认选中
                self.m_SrcSelectButton.Selected = false
                self.m_DstSelectButton.Selected = true
            end
            self:UpdateItemCellAppearance()
            changed = true
        end
    elseif self.m_DstSelectButton.Selected then
        --如果当前dst被选中，则和src比较是否可以互换
        if srcEquip==nil or self:CheckEquipCanExchangeIdentifySkill(curEquip, srcEquip) then
            self:InitDstCell(curCell.equipId)
            if srcEquip == nil then
                --修改默认选中
                self.m_SrcSelectButton.Selected = true
                self.m_DstSelectButton.Selected = false
            end
            self:UpdateItemCellAppearance()
            changed = true
        end
    end

    --重新计算cost item信息
    if changed then
        self.m_TransferButton.Enabled = false --先禁用
        if self.m_SrcEquipCell.equipId and self.m_DstEquipCell.equipId then
            Gac2Gas.IdentifySkillRequestExchangeSkill(self.m_SrcEquipCell.equipId, self.m_DstEquipCell.equipId, true)
        else
            self:InitCostItems(0, 0, 0, 0)
        end
    end

end

function LuaEquipSkillTransferView:UpdateItemCellAppearance( )

    local srcEquipId = self.m_SrcEquipCell.equipId
    local dstEquipId = self.m_DstEquipCell.equipId

    for _, cell in pairs(self.m_Cells) do
        --如果src或dst中有，则标记为选中状态
        if cell.equipId == srcEquipId or cell.equipId == dstEquipId then
            cell:SetMaskVisible(true)
            cell:SetSelected(true)
        else
            cell:SetSelected(false)
            local srcCommonItem = CItemMgr.Inst:GetById(srcEquipId)
            local dstCommonItem = CItemMgr.Inst:GetById(dstEquipId)
            local curCommonItem = CItemMgr.Inst:GetById(cell.equipId)

            local srcEquip = srcCommonItem and srcCommonItem.Equip
            local dstEquip = dstCommonItem and dstCommonItem.Equip
            local curEquip = curCommonItem and curCommonItem.Equip
            if self.m_SrcSelectButton.Selected then
                --如果当前src被选中，则和dst比较是否可以互换
                if dstEquip==nil or self:CheckEquipCanExchangeIdentifySkill(curEquip, dstEquip) then
                    cell:SetMaskVisible(false)
                else
                    cell:SetMaskVisible(true)
                end
            elseif self.m_DstSelectButton.Selected then
                --如果当前dst被选中，则和src比较是否可以互换
                if srcEquip==nil or self:CheckEquipCanExchangeIdentifySkill(curEquip, srcEquip) then
                    cell:SetMaskVisible(false)
                else
                    cell:SetMaskVisible(true)
                end
            end
        end
    end
end

function LuaEquipSkillTransferView:OnSrcCellClick( )
    if not self.inited then return end
    self.m_SrcSelectButton.Selected = true
    self.m_DstSelectButton.Selected = false
    self:UpdateItemCellAppearance()
end

function LuaEquipSkillTransferView:OnDstCellClick( )
    if not self.inited then return end
    self.m_SrcSelectButton.Selected = false
    self.m_DstSelectButton.Selected = true
    self:UpdateItemCellAppearance()
end

function LuaEquipSkillTransferView:CheckEquipCanExchangeIdentifySkill(equip1, equip2)
    if not equip1 or not equip2 then return false end
    if not equip1.IsIdentifiable and not equip2.IsIdentifiable then return false end
    local designData1 = EquipmentTemplate_Equip.GetData(equip1.TemplateId)
    local designData2 = EquipmentTemplate_Equip.GetData(equip2.TemplateId)
    if not designData1 or not designData2 then return false end
    --检查大部位相同
    if designData1.Type ~= designData2.Type then return false end
    --检查都绑定
    if not equip1.IsBinded or not equip2.IsBinded then return false end
    --原始等级的等级段相同
    local stage_1 = self:GradeToStage(designData1.Grade)
    local stage_2 = self:GradeToStage(designData2.Grade)
    if not stage_1 or stage_1 == 0 or not stage_2 or stage_2 == 0 or stage_1 ~= stage_2 then
        return false
    end
    --至少一方需要有鉴定技能
    local skillId1 = equip1.FixedIdentifySkillId
    local skillId2 = equip2.FixedIdentifySkillId
    if skillId1==0 and skillId2==0 then return false end

    return true
end

-- 装备等级 -> 等级段 从服务器端拷贝过来用
function LuaEquipSkillTransferView:GradeToStage(grade)
    if not grade then return 0 end

    if grade <= 69 then
        return 1
    elseif grade <= 89 then
        return 2
    elseif grade <= 109 then
        return 3
    elseif grade <= 129 then
        return 4
    elseif grade <= 160 then
        return 5
    end
    return 0
end

function LuaEquipSkillTransferView:OnCostItemClick(go)
    if not self.inited then return end

    if go == self.m_CostItem1 then
        if self.m_NeedItem1Id > 0 and self.m_NeedItem1Num > 0 then
            local ownCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_NeedItem1Id)
            if ownCount < self.m_NeedItem1Num then
                CItemAccessTipMgr.Inst:ShowAccessTip(nil, self.m_NeedItem1Id, false, go.transform, CTooltipAlignType.Top)
            end 
        end
    elseif go == self.m_CostItem2 then
        if self.m_NeedItem2Id > 0 and self.m_NeedItem2Num > 0 then
            local ownCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.m_NeedItem2Id)
            if ownCount < self.m_NeedItem2Num then
                CItemAccessTipMgr.Inst:ShowAccessTip(nil, self.m_NeedItem2Id, false, go.transform, CTooltipAlignType.Top)
            end 
        end
    end
end

function LuaEquipSkillTransferView:OnTransferButtonClick()
    if not self.m_SrcEquipCell.equipId or not self.m_DstEquipCell.equipId then
        g_MessageMgr:ShowMessage("Double_EquipWords_Need_Select_Equip")
        return
    end
    --没有数量信息的情况下不允许操作
    if not self.m_NeedItem1Num or not self.m_NeedItem2Num then return end

    local msg = g_MessageMgr:FormatMessage("Equip_ReIdentify_Exchange_Confim", self.m_NeedItem1Num or 0, self.m_NeedItem2Num or 0) --这里有个坑，如果道具顺序变了数量显示就对不上了，目前消息是太初精元、九曲珠这个顺序
    MessageWndManager.ShowOKCancelMessage(msg, 
        DelegateFactory.Action(function()
            Gac2Gas.IdentifySkillRequestExchangeSkill(self.m_SrcEquipCell.equipId, self.m_DstEquipCell.equipId, false)            
         end),
         nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function LuaEquipSkillTransferView:IdentifySkillRequestExchangeSuccess(equipId1, equipId2)
    self:PlayExtendSuccessFx(equipId1, equipId2)
end

function LuaEquipSkillTransferView:PlayExtendSuccessFx(srcEquipId, dstEquipId)
    if self.m_SrcEquipCell.equipId == srcEquipId and self.m_DstEquipCell.equipId == dstEquipId then
        local srcCommonItem = CItemMgr.Inst:GetById(srcEquipId)
        local dstCommonItem = CItemMgr.Inst:GetById(dstEquipId)
        local srcEquip = srcCommonItem and srcCommonItem.Equip
        local dstEquip = dstCommonItem and dstCommonItem.Equip
        if srcEquip and Skill_AllSkills.Exists(srcEquip.FixedIdentifySkillId) then
            self.m_SrcSkillFx:DestroyFx()
            self.m_SrcSkillFx:LoadFx("Fx/UI/Prefab/UI_jinengjianding_01.prefab")
        end
        if dstEquip and Skill_AllSkills.Exists(dstEquip.FixedIdentifySkillId) then
            self.m_DstSkillFx:DestroyFx()
            self.m_DstSkillFx:LoadFx("Fx/UI/Prefab/UI_jinengjianding_01.prefab")
        end
    end
end

function LuaEquipSkillTransferView:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("Equip_ReIdentify_Exchange_ReadMe")
end

function LuaEquipSkillTransferView:OnSrcEquipIconClick()
    if not self.inited then return end
    if self.m_SrcEquipCell.equipId then
         CItemInfoMgr.ShowLinkItemInfo(self.m_SrcEquipCell.equipId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end
end

function LuaEquipSkillTransferView:OnDstEquipIconClick()
    if not self.inited then return end
    if self.m_DstEquipCell.equipId then
         CItemInfoMgr.ShowLinkItemInfo(self.m_DstEquipCell.equipId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end
end

function LuaEquipSkillTransferView:OnSrcEquipSkillIconClick()
    if not self.inited then return end
    local curCommonItem = CItemMgr.Inst:GetById(self.m_SrcEquipCell.equipId)
    local curEquip = curCommonItem and curCommonItem.Equip
    if curEquip then
        local skill = Skill_AllSkills.GetData(curEquip.FixedIdentifySkillId)
        CSkillInfoMgr.ShowSkillInfoWnd(curEquip.FixedIdentifySkillId, curEquip.Id, false, Vector3.zero, EnumSkillInfoContext.MainPlayerSkillIdentify, true, 0, 0, nil)
    end
end

function LuaEquipSkillTransferView:OnDstEquipSkillIconClick()
    if not self.inited then return end
    local curCommonItem = CItemMgr.Inst:GetById(self.m_DstEquipCell.equipId)
    local curEquip = curCommonItem and curCommonItem.Equip
    if curEquip then
        local skill = Skill_AllSkills.GetData(curEquip.FixedIdentifySkillId)
        CSkillInfoMgr.ShowSkillInfoWnd(curEquip.FixedIdentifySkillId, curEquip.Id, false, Vector3.zero, EnumSkillInfoContext.MainPlayerSkillIdentify, true, 0, 0, nil)
    end
end

return LuaEquipSkillTransferView