-- Auto Generated!!
local CHuanLingColor = import "L10.UI.CHuanLingColor"
CHuanLingColor.m_SetLock_CS2LuaHook = function (this, isLocked) 
    if isLocked then
        this.m_Lock:SetActive(true)
        this.m_ColorSprite.alpha = this.m_LockSelectAlpha
        this.Selectable = false
    else
        this.m_Lock:SetActive(false)
        this.m_ColorSprite.alpha = 1
        this.Selectable = true
    end
end
