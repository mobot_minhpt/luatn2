
LuaDouDiZhuBaoXiangMgr = {}

LuaDouDiZhuBaoXiangMgr.resultInfo = {}
LuaDouDiZhuBaoXiangMgr.playerChestCount = {}
LuaDouDiZhuBaoXiangMgr.applyButtonCD = 3 -- 报名界面按钮CD

-- 同步报名信息
function LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiSignUpInfo(isSignUp, remainAwards, honor, myRank, nextRefreshTime)
    local awards = {}
    local list = MsgPackImpl.unpack(remainAwards)
    for i = 0, list.Count - 1, 2 do
        local itemId = list[i]
        local count = list[i + 1]
        table.insert(awards, itemId)
        table.insert(awards, count)
    end
    g_ScriptEvent:BroadcastInLua("SyncQiangXiangZiSignUpInfo", isSignUp, awards, myRank, honor, nextRefreshTime)
end

-- 同步报名结果
function LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiSignUpResult(isSignUp)
    g_ScriptEvent:BroadcastInLua("SyncQiangXiangZiSignUpResult", isSignUp)
end

-- 同步一轮的比赛结果
function LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiRoundResult(isWin, isDiZhu, isPlayOver, roundBoxCount, boxCount, extraData)
    self.resultInfo = {}
    self.resultInfo.isWin = isWin
    self.resultInfo.isDiZhu = isDiZhu
    self.resultInfo.isPlayOver = isPlayOver
    self.resultInfo.boxCount = roundBoxCount
    self.resultInfo.allBoxCount = boxCount
    self:SyncChestCount(MsgPackImpl.unpack(extraData))
    CUIManager.ShowUI(CLuaUIResources.DouDiZhuBaoXiangResultWnd)

    g_ScriptEvent:BroadcastInLua("SyncQiangXiangZiRoundResult", isWin, isPlayOver, boxCount)
end

-- 同步开宝箱结果
function LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiOpenChestResult(remainChestCount, getHonor, awards)
    local t = {}
    local list = MsgPackImpl.unpack(awards)
    for i = 0, list.Count - 1, 2 do
        local itemId = list[i]
        local count = list[i + 1]
        t[itemId] = count
    end
    g_ScriptEvent:BroadcastInLua("SyncQiangXiangZiOpenChestResult", remainChestCount, t)
end

-- 同步箱子数量
function LuaDouDiZhuBaoXiangMgr:SyncChestCount(extra)
    self.playerChestCount = {}
    if not extra or not CommonDefs.IsDic(extra) then return end

	CommonDefs.DictIterate(extra, DelegateFactory.Action_object_object(function(playerId, dict)
        local tbl = {}
        CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key, val)
            tbl[tostring(key)] = tonumber(val)
        end))
        self.playerChestCount[tonumber(playerId)] = tbl
	end))
end

-- 宝箱数显示
function LuaDouDiZhuBaoXiangMgr:GetScoreText(index)
    local info = CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)

    if info then
        for id, data in pairs(self.playerChestCount) do
            if id == info.id then
                return SafeStringFormat3(LocalString.GetString("宝箱数 %d"), data.totalChest)
            end
        end
    end

    return LocalString.GetString("宝箱数 0")
end
