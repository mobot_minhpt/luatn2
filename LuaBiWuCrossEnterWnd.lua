local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaBiWuCrossEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBiWuCrossEnterWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaBiWuCrossEnterWnd, "InfoBtn", "InfoBtn", GameObject)
RegistChildComponent(LuaBiWuCrossEnterWnd, "WatchBtn", "WatchBtn", GameObject)
RegistChildComponent(LuaBiWuCrossEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaBiWuCrossEnterWnd, "TimeLabel", "TimeLabel", UILabel)

--@endregion RegistChildComponent end

function LuaBiWuCrossEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

	UIEventListener.Get(self.InfoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInfoBtnClick()
	end)

	UIEventListener.Get(self.WatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWatchBtnClick()
	end)

	UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtnClick()
	end)

    --@endregion EventBind end

end

function LuaBiWuCrossEnterWnd:Init()
	self.TimeLabel.text = BiWuCross_Setting.GetData().TimeStr
end

--@region UIEvent

function LuaBiWuCrossEnterWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("BIWUCROSS_RULE")
end

function LuaBiWuCrossEnterWnd:OnInfoBtnClick()
	LuaBiWuCrossMgr.ShowInfoWnd()
end

function LuaBiWuCrossEnterWnd:OnWatchBtnClick()
	LuaBiWuCrossMgr.ShowWatchWnd()
end

function LuaBiWuCrossEnterWnd:OnEnterBtnClick()
	LuaBiWuCrossMgr.ReqEnterBattle()
end

--@endregion UIEvent

