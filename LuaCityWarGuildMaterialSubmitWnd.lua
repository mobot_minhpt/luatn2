require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemMgr = import "L10.Game.CItemMgr"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"

CLuaCityWarGuildMaterialSubmitWnd = class()
CLuaCityWarGuildMaterialSubmitWnd.Path = "ui/citywar/LuaCityWarGuildMaterialSubmitWnd"

function CLuaCityWarGuildMaterialSubmitWnd:Init( ... )
	local input = self.transform:Find("Anchor/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
	local materialCount = CItemMgr.Inst:GetItemCount(tonumber(CityWar_Setting.GetData().ZiCaiItemId))
	if materialCount <= 0 then
		if CLuaCityWarMgr.IsFirstOpenSubmitWnd then
			g_MessageMgr:ShowMessage("KFCZ_SUBMIT_NO_ZICAI")
		end
		CUIManager.CloseUI(CLuaUIResources.CityWarGuildMaterialSubmitWnd)
	end
	local maxCount = math.max(math.min(materialCount, CLuaCityWarMgr.GuildMaterialLimit - CLuaCityWarMgr.GuildMaterialAmount), 0)
	input:SetMinMax(0, maxCount, 1)
	input:SetValue(maxCount)
	local transNameTbl = {"Cost", "Own"}
	local valueTbl = {materialCount, CLuaCityWarMgr.GuildMaterialAmount.."/"..CLuaCityWarMgr.GuildMaterialLimit}
	for i = 1, #transNameTbl do
		self.transform:Find("Anchor/QnCostAndOwnMoney/"..transNameTbl[i]):GetComponent(typeof(UILabel)).text = valueTbl[i]
	end
	if maxCount <= 0 then self.transform:Find("Anchor/OkButton"):GetComponent(typeof(CButton)).Enabled = false end
	UIEventListener.Get(self.transform:Find("Anchor/OkButton").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.SubmitCityWarGuildZiCai(input:GetValue())
	end)
	UIEventListener.Get(self.transform:Find("Anchor/CancelButton").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CUIManager.CloseUI(CLuaUIResources.CityWarGuildMaterialSubmitWnd)
	end)
end