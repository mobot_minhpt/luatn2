
LuaBlackScreenMgr = {}
LuaBlackScreenMgr.m_FadeInTime = nil

function LuaBlackScreenMgr:StartFadeIn(time)
    self.m_FadeInTime = time
    if CUIManager.IsLoaded(CLuaUIResources.BlackScreenWnd) then
        g_ScriptEvent:BroadcastInLua("BlackSceenStartFadeIn", time)
    else
        CUIManager.ShowUI(CLuaUIResources.BlackScreenWnd)
    end
end

function LuaBlackScreenMgr:StartFadeOut(time)
    g_ScriptEvent:BroadcastInLua("BlackSceenStartFadeOut", time)
end

function LuaBlackScreenMgr:Reset()
    self.m_FadeInTime = nil
end