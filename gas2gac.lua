

require("common/doudizhu/doudizhu")
local CCommonUIFxWnd=import "L10.UI.CCommonUIFxWnd"
local CFightingSpiritMgr=import "L10.Game.CFightingSpiritMgr"
local MengHuaLu_Skill = import "L10.Game.MengHuaLu_Skill"
local Extensions = import "Extensions"
local StringBuilder = import "System.Text.StringBuilder"
local CPlayerPropertyFight = import "L10.Game.CPlayerPropertyFight"
local CLingShou=import "L10.Game.CLingShou"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CPayMgr = import "L10.Game.CPayMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CIMMgr=import "L10.Game.CIMMgr"
local BITSET=import "L10.Game.Properties.BITSET"
local CHouseCompetitionVictoryHouseInfo = import "L10.Game.CHouseCompetitionVictoryHouseInfo"
local EnumHouseCompetitionStage = import "L10.Game.EnumHouseCompetitionStage"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local Main = import "L10.Engine.Main"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CWordFilterMgr=import "L10.Game.CWordFilterMgr"
local CHongBaoMgr=import "L10.UI.CHongBaoMgr"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CChatMgr = import "L10.Game.CChatMgr"
local CProfileObject = import "L10.Game.CProfileObject"
local CGuanNingMgr=import "L10.Game.CGuanNingMgr"
local MessageMgr=import "L10.Game.MessageMgr"
local QnMoneyCostMesssageMgr=import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType=import "L10.Game.EnumMoneyType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTrackMgr = import "L10.Game.CTrackMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Constants=import "L10.Game.Constants"
local CShopMallMgr=import "L10.UI.CShopMallMgr"
local CChuanjiabaoWordType=import "L10.Game.CChuanjiabaoWordType"
local UInt64 = import "System.UInt64"
local CPos = import "L10.Engine.CPos"
local Utility = import "L10.Engine.Utility"
local CPlayDropItemObject = import "L10.Game.CPlayDropItemObject"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local Gas2Gac2 = import "L10.Game.Gas2Gac"
local MsgPackImpl = import "MsgPackImpl"
local CClientMonster        = import "L10.Game.CClientMonster"
local Object = import "System.Object"
local Vector3 = import "UnityEngine.Vector3"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"

local CItemMgr=import "L10.Game.CItemMgr"
local CCommonItem=import "L10.Game.CCommonItem"

local EventManager = import "EventManager"
local EnumGiftAlertType = import "L10.UI.EnumGiftAlertType"
local EnumEventType = import "EnumEventType"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CScene=import "L10.Game.CScene"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CBiWuDaHuiMgr=import "L10.Game.CBiWuDaHuiMgr"
local CHouseCompetitionMgr=import "L10.Game.CHouseCompetitionMgr"
local CFurnitureLayoutMgr = import "L10.UI.CFurnitureLayoutMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CTeamMgr=import "L10.Game.CTeamMgr"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CTeamFollowMgr = import "L10.Game.CTeamFollowMgr"
local CFightingSpiritLiveWatchWndMgr = import "L10.UI.CFightingSpiritLiveWatchWndMgr"

local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local CUnityHelper = import "L10.Engine.CUnityHelper"
local CLoginMgr = import "L10.Game.CLoginMgr"

local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local CZhuJueJuQingMgr=import "L10.Game.CZhuJueJuQingMgr"
local CActivityMgr = import "L10.Game.CActivityMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CHongBaoMsg = import "L10.Game.CHongBaoMsg"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local EnumDisplayHongBaoType = import "L10.Game.EnumDisplayHongBaoType"
local VARBINARY = import "L10.Game.Properties.VARBINARY"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local ShareMgr = import "ShareMgr"

local CShakeCameraMgr = import "L10.Game.CShakeCameraMgr"
local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local CReliveWnd=import "L10.UI.CReliveWnd"
local CPostEffect = import "L10.Engine.CPostEffect"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local CCameraParam = import "L10.Engine.CameraControl.CCameraParam"
local GameVideo_GameVideo = import "L10.Game.GameVideo_GameVideo"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local SystemInfo = import "UnityEngine.SystemInfo"
local CCooldownMgr = import "L10.Game.CCooldownMgr"
local CTaskAndTeamWnd=import "L10.UI.CTaskAndTeamWnd"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CEquipBaptizeDetailView = import "L10.UI.CEquipBaptizeDetailView"
local CRenderObject = import "L10.Engine.CRenderObject"
local CRightMenuWnd = import "L10.UI.CRightMenuWnd"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"

function Gas2Gac.ShowQiyuanBtnFirstGoHome()
    CLuaHouseMinimap.sbShowQifuBtn = true
    g_ScriptEvent:BroadcastInLua("OnShowQiyuanBtnFirstGoHome")
end

function Gas2Gac.OpenDialog(npcEngineId, dialogId, npcId,choices, customContent, colorIdx)
    local s = MsgPackImpl.unpack(choices)
    local selections = CreateFromClass(MakeGenericClass(List, String))
    CommonDefs.ListIterate(s, DelegateFactory.Action_object(function (___value)
        CommonDefs.ListAdd(selections, typeof(String), LocalString.TranslateAndFormatText(tostring(___value)))
    end))
    CConversationMgr.Inst:OpenDialog(npcEngineId, dialogId, npcId, selections, customContent, colorIdx)
end


function  Gas2Gac.SendQueryBabyInfoResult_NotExist(babyId)

end
function  Gas2Gac.SendQueryBabyInfoResult(babyId, ownerId, score, qichangId, ownerName, birthday, xingGuanId, natureId, showInfoUD, propInfoUD)
    -- print("SendQueryBabyInfoResult",babyId, ownerId, score, ownerName, guildName, showInfoUD)
    local showInfo={}
    local list = MsgPackImpl.unpack(showInfoUD)
    if list then
        showInfo={
            status = tonumber(list[0]),
            gender = tonumber(list[1]),
			name = list[2],
            hairColor = tonumber(list[3]),
            skinColor = tonumber(list[4]),
            bodyWeight = tonumber(list[5]),
            hairstyle = tonumber(list[6]),
            headId = tonumber(list[7]),
            bodyId = tonumber(list[8]),
            colorId = tonumber(list[9]),
            backId = tonumber(list[10]),
            grade = tonumber(list[11]),
            showQiChangId = tonumber(list[12]),
        }

        local propInfo = {}
        local list2 = MsgPackImpl.unpack(propInfoUD)
        if list2 and list2.Count>0 then
            if showInfo.grade >= 11 and showInfo.grade < 31 then--eChild
                for i=1,list2.Count,2 do
                    table.insert( propInfo,{
                        propId = list2[i-1],
                        propNum = list2[i]
                    } )
                end
            elseif showInfo.grade >= 31 then--eYoung
                for i=1,list2.Count,2 do
                    table.insert( propInfo,{
                        propId = list2[i-1],
                        propNum = list2[i]
                    } )
                end
            end
        end
        g_ScriptEvent:BroadcastInLua("SendQueryBabyInfoResult",babyId, ownerId, score,qichangId, ownerName, birthday, xingGuanId, natureId, showInfo,propInfo)
    end
end

function  Gas2Gac.QueryHouseBabyListDone(targetPlayerId, babyGenderInfoUD)
    -- print("QueryHouseBabyListDone")
    local list = MsgPackImpl.unpack(babyGenderInfoUD)
    if list then
        -- print(list.Count)
        local t = {}
        for i=1,list.Count,2 do
            table.insert( t,{
                babyId = tostring(list[i-1]),
                gender = tonumber(list[i])
            } )
        end
        g_ScriptEvent:BroadcastInLua("QueryHouseBabyListDone",targetPlayerId,t)
    end

end

function Gas2Gac.SendAvailableShopActivityIds(bytes)
    LuaNewWelfareWnd.ChargeActivityID = {}
    local list = MsgPackImpl.unpack(bytes)
    for i = 1, list.Count do
        table.insert(LuaNewWelfareWnd.ChargeActivityID, tonumber(list[i-1]) )
    end
    g_ScriptEvent:BroadcastInLua("OnQueryChargeActivityIdReturn")
end


function Gas2Gac.SendHuaBiResult(totalExp, level, timePassed, killPercent, score)
	local spritePath = ""
	local isShowEffect = 0
	local count = Huabi_Evaluation.GetDataCount()
	for i = 1, count do
		local data = Huabi_Evaluation.GetData(i)
		if score >= data.Score then
			spritePath = data.Image
			isShowEffect = data.Effect
			break
		end
	end

	CLuaHuaBiResultWnd.ScoreSpritePath = spritePath
	CLuaHuaBiResultWnd.TotalExp = totalExp
	CLuaHuaBiResultWnd.Level = level
	CLuaHuaBiResultWnd.ShowEffect = isShowEffect

	CLuaHuaBiResultWnd.TimeSpend, CLuaHuaBiResultWnd.KillPercent  = timePassed, killPercent
	CUIManager.ShowUI(CUIResources.HuaBiResultWnd)
end

function Gas2Gac.OpenTaskDialog(taskId, npcEngineId, npcId, message,choices)
    local s = MsgPackImpl.unpack(choices)
    local selections = CreateFromClass(MakeGenericClass(List, String))
    CommonDefs.ListIterate(s, DelegateFactory.Action_object(function (___value)
        CommonDefs.ListAdd(selections, typeof(String), tostring(___value))
    end))

    --下面的逻辑原来写在CTaskDialogWnd中，将移动到此处
    local msgs = CommonDefs.StringSplit_ArrayChar(message, ":")
    local messageType = msgs[0]
    local template = Task_Task.GetData(taskId)
    if template ~= nil then
        if messageType == "b" then
            message = template.BeginChat
            if message == nil then
                message = LocalString.GetString("<popup npc:0>快接任务</popup>")
            end
        elseif messageType == "f" then
            message = template.EndChat
            if message == nil then
                message = LocalString.GetString("<popup npc:0>目标已达成</popup>")
            end
        elseif messageType == "t" then
            message = template.NotEndChat
            if message == nil then
                message = LocalString.GetString("<popup npc:0>目标未达成</popup>")
            end
        elseif messageType == "n" then
            --TODO 以后改到template里去,提供一个接口
            for aa, bb in string.gmatch(template.FindNpc, "([^:]+):([^;]+);?") do
                local NpcId, ItemId
                if string.find(aa, ",") then
                    local cc, dd = string.match(aa, "(%d+),(%d+)")
                    NpcId, ItemId = tonumber(cc), tonumber(dd)
                    if NpcId == nil then
                        --ItemId是负数，表示找到npc的时候扣一个道具
                        local cc, dd = string.match(aa, "(%d+),%-(%d+)")
                        NpcId, ItemId = tonumber(cc), -tonumber(dd)
                    end
                else
                    NpcId = tonumber(aa)
                end
                if NpcId == npcId then
                    message = bb
                    break
                end
            end
            if message == nil then
                message = LocalString.GetString("<popup npc:0>策划配错了</popup>")
            end
        elseif messageType == "i" then
            local TaskId = math.floor(tonumber(msgs[1] or 0))
            CConversationMgr.Inst:OpenTaskItemSubmitDialog(npcEngineId, TaskId, npcId)
            return
        elseif messageType == "s" then
            local TaskId = math.floor(tonumber(msgs[1] or 0))
            message = template.EndChat
            if message == nil then
                message = LocalString.GetString("<popup npc:0>目标已达成</popup>")
            end
        end
    end

    -- print("OpenTaskDialog",npcEngineId, taskId, npcId, message, selections)
    CConversationMgr.Inst:OpenTaskDialog(npcEngineId, taskId, npcId, message, selections)
end


function Gas2Gac.QueryTeamSizeResult(playerId, teamSize)
    g_ScriptEvent:BroadcastInLua("QueryTeamSizeResult",playerId, teamSize)
end



function Gas2Gac.RequestPetDetailsResult(engineId, hp, pAttMin, pAttMax, mAttMin, mAttMax,
    pDef, mDef, leftTime, antiFire, antiThunder, antiIce, antiPoison, antiWind,
    antiLight, antiIllusion, antiWater, pFatal, pFatalDamage, mFatal, mFatalDamage, antiDizzy, antiChaos, antiBind, antiSilence, antiSleep, antiTie, antiPetrify, antiBianHu, pHit, mHit, AntiBlock, Block, IgnoreAntiFire, IgnoreAntiLight, IgnoreAntiIce, IgnoreAntiPoison, pMiss, mMiss, MulpHurt, MulmHurt)
    local info = {}
    info.engineId = engineId
    info.hp = hp
    info.pAttMin = pAttMin
    info.pAttMax = pAttMax
    info.mAttMin = mAttMin
    info.mAttMax = mAttMax
    info.pDef = pDef
    info.mDef = mDef
    info.leftTime = leftTime
    info.antiFire = antiFire
    info.antiThunder = antiThunder
    info.antiIce = antiIce
    info.antiPoison = antiPoison
    info.antiWind = antiWind
    info.antiLight = antiLight
    info.antiIllusion = antiIllusion
    info.antiWater = antiWater

    info.pFatal = pFatal
    info.pFatalDamage = pFatalDamage
    info.mFatal = mFatal
    info.mFatalDamage = mFatalDamage

    info.antiDizzy = antiDizzy
    info.antiChaos = antiChaos
    info.antiBind = antiBind
    info.antiSilence = antiSilence
    info.antiSleep = antiSleep
    info.antiTie = antiTie
    info.antiPetrify = antiPetrify
    info.antiBianHu = antiBianHu

    info.pHit = pHit
    info.mHit = mHit
    info.AntiBlock = AntiBlock
    info.Block = Block
    info.IgnoreAntiFire = IgnoreAntiFire
    info.IgnoreAntiLight = IgnoreAntiLight
    info.IgnoreAntiIce = IgnoreAntiIce
    info.IgnoreAntiPoison = IgnoreAntiPoison
    info.pMiss = pMiss
    info.mMiss = mMiss
    info.MulpHurt = MulpHurt
    info.MulmHurt = MulmHurt

    g_ScriptEvent:BroadcastInLua("OnGetPetDetailInfo", engineId, info)
end




function Gas2Gac.QueryBiWuStageMatchInfoResult(matchInfo_U)
    local rtn = {}
    local matchInfo = MsgPackImpl.unpack(matchInfo_U)
    for i=1,matchInfo.Count do
        local info = matchInfo[i-1]
        if info.Count>=5 then
            local item = {
                team1Name = tostring(info[0]),
                team2Name = tostring(info[1]),
                group = tonumber(info[2]),-- 1 胜者组 2 败者组
                winTeamName = tostring(info[3]),
                index = i,
                invalid = info[4],
            }
            item.teamCount = 0
                if not item.invalid then
                if not (item.team1Name==nil or item.team1Name=="") then
                    item.teamCount = item.teamCount+1
                end
                if not (item.team2Name==nil or item.team2Name=="") then
                    item.teamCount = item.teamCount+1
                end
            end
            item.hasMyTeam = false
            local members = CTeamMgr.Inst.Members
            local lookup = {}
            for i=1,members.Count do
                lookup[members[i-1].m_MemberName] = true
            end
            if lookup[item.team1Name] or lookup[item.team2Name] then
                item.hasMyTeam = true
            end

            table.insert( rtn,item )
        end
    end
    g_ScriptEvent:BroadcastInLua("QueryBiWuStageMatchInfoResult", rtn)
end

function Gas2Gac.CrossXiaLvPkQueryStageMatchInfoResult(matchInfo_U)
    Gas2Gac.XiaLvPkQueryStageMatchInfoResult(matchInfo_U)
end
function Gas2Gac.XiaLvPkQueryStageMatchInfoResult(matchInfo_U)
    local rtn = {}
    local matchInfo = MsgPackImpl.unpack(matchInfo_U)
    for i=1,matchInfo.Count do
        local info = matchInfo[i-1]
        if info.Count>=4 then
            local item = {
                team1Name = tostring(info[0]),
                team2Name = tostring(info[1]),
                group = tonumber(info[2]),-- 1 胜者组 2 败者组
                winTeamName = tostring(info[3]),
                index = i,
            }
            item.teamCount = 0
            if not (item.team1Name==nil or item.team1Name=="") then
                item.teamCount = item.teamCount+1
            end
            if not (item.team2Name==nil or item.team2Name=="") then
                item.teamCount = item.teamCount+1
            end
            item.hasMyTeam = false
            local members = CTeamMgr.Inst.Members
            local lookup = {}
            for i=1,members.Count do
                lookup[members[i-1].m_MemberName] = true
            end
            if lookup[item.team1Name] or lookup[item.team2Name] then
                item.hasMyTeam = true
            end

            table.insert( rtn,item )
        end
    end
    g_ScriptEvent:BroadcastInLua("QueryBiWuStageMatchInfoResult", rtn)
end




function Gas2Gac.PlayerRequestLoadHouseLayoutCheckSuccess(idx)
    local BatchNumber = 5
    if CClientMainPlayer.Inst == nil
        or not CClientHouseMgr.Inst:IsInOwnHouse()
        or CFurnitureLayoutMgr.Layouts == nil
        or CFurnitureLayoutMgr.Layouts.LayoutList.Count <= idx then
        return
    end

    local layout = CFurnitureLayoutMgr.Layouts.LayoutList[idx]

    LuaPoolMgr.RequestLoadPool(layout.Pool,layout.WarmPool)

    local count = 0
    local FurnitureList = CreateFromClass(MakeGenericClass(List, Object))

    do
        local i = 0
        while i < layout.FurnitureItems.Count do
            if count == BatchNumber then
                Gac2Gas.PlayerRequestLoadHouseLayout(MsgPackImpl.pack(FurnitureList), false)
                count = 0
                CommonDefs.ListClear(FurnitureList)
            end

            local item = layout.FurnitureItems[i]
            if item.X < 0 then
                item.IsWinter = true
            end
            --某个版本x<0的时候是雪景状态，后来加了参数
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.TemplateId)
            CommonDefs.ListAdd(FurnitureList, typeof(Single), math.abs(item.X))
            CommonDefs.ListAdd(FurnitureList, typeof(Single), item.Y)
            CommonDefs.ListAdd(FurnitureList, typeof(Single), item.Z)
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.Rot)
            local ui8scale = CClientFurnitureMgr.GetScaleFloatToUI8(item.Scale)
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), ui8scale)
            CommonDefs.ListAdd(FurnitureList, typeof(Boolean), item.IsWinter)

            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.RX)
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.RY)
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.RZ)

            count = count + 1
            i = i + 1
        end
    end
    if count ~= 0 then
        Gac2Gas.PlayerRequestLoadHouseLayout(MsgPackImpl.pack(FurnitureList), false)
        count = 0
        CommonDefs.ListClear(FurnitureList)
    end

    do
        local i = 0
        while i < layout.DecorationItems.Count do
            if count == BatchNumber then
                Gac2Gas.PlayerRequestLoadHouseLayout(MsgPackImpl.pack(FurnitureList), true)
                count = 0
                CommonDefs.ListClear(FurnitureList)
            end

            local item = layout.DecorationItems[i]
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.TemplateId)
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.ParentTemplateId)
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.ParentPos)
            CommonDefs.ListAdd(FurnitureList, typeof(UInt32), item.SlotIdx + 1)

            count = count + 1
            i = i + 1
        end
    end
    if count ~= 0 then
        Gac2Gas.PlayerRequestLoadHouseLayout(MsgPackImpl.pack(FurnitureList), true)
        count = 0
        CommonDefs.ListClear(FurnitureList)
    end

    CFurnitureLayoutMgr.LastUsedLayout = idx
    EventManager.Broadcast(EnumEventType.OnLoadFurnitureLayoutSuccess)

    g_MessageMgr:ShowMessage("HOUSE_LOAD_LAYOUT_SUCCESS")
end



function Gas2Gac.SyncBeginZhaoQinGuanZhan()
    CUIManager.ShowUI(CUIResources.DanmuWnd)
    g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",true)
end

function Gas2Gac.StartWatchLive()
    if not CGameVideoMgr.Inst.IsLiveOpen then
        return
    end
    CGameVideoMgr.Inst:StartWatchLive()

    if CScene.MainScene ~= nil then
        local id = CScene.MainScene.SceneTemplateId
        --切磋的观战不需要显示弹幕
        if id == Constants.DouHunWatchSceneTemplateId
        or id == Constants.BiWuWatchTemplateSceneId
        or id == Constants.QmpkWatchSceneTemplateId then
            CUIManager.ShowUI(CUIResources.DanmuWnd)
            g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",true)
        end
    end
end





function Gas2Gac.UpdatePvpSideDPS(forceDPS_U)
    g_ScriptEvent:BroadcastInLua("FightingSpiritTeamDamageReceive",forceDPS_U)
end

function Gas2Gac.UpdatePvpSideName(forceName_U,bShow)
	local tmp = MsgPackImpl.unpack(forceName_U)

    for i=1,tmp.Count do
        local data = tmp[i-1]
        local index = data[0]
        if CBiWuDaHuiMgr.Inst.inBiWu or LuaColiseumMgr:IsInPlay() then--比武里面 第一个是防守 第二个是进攻
            if index==0 then
                CLuaFightingSpiritDamageWnd.Name2=data[1]
            else
                CLuaFightingSpiritDamageWnd.Name1=data[1]
            end
        else
            if index==0 then
                CLuaFightingSpiritDamageWnd.Name1=data[1]
            else
                CLuaFightingSpiritDamageWnd.Name2=data[1]
            end
        end
    end
    if bShow then
        CUIManager.ShowUI(CUIResources.FightingSpiritDamageWnd)
    else
        CUIManager.CloseUI(CUIResources.FightingSpiritDamageWnd)
    end
end


function Gas2Gac.EmbracePlayerConfirm(playerId, expressionId, tp)
    local messageName = "Embrace_Player_Confirm"
    if tp == "jugaogao" then
        messageName = "Jugaogao_Player_Confirm"
    elseif tp == "kiss" then
        messageName = "Kiss_Player_Confirm"
    elseif tp == "fangfengzheng" then
        messageName = "FangFengZheng_Player_Confirm"
    elseif tp == "shuangrenhua" then
        messageName = "ShuangRenHua_Player_Confirm"
    elseif tp == "xiangyixiangwei" then
        messageName = "XiangYiXiangWei_Player_Confirm"
    end

    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if not player then
        return
    end

    local message = g_MessageMgr:FormatMessage(messageName, player.Name)
    MessageWndManager.ShowConfirmMessage(message, 10, false, DelegateFactory.Action(function ()
        Gac2Gas.RequestEmbracePlayerAnswer(playerId, expressionId, true, tp)
    end), DelegateFactory.Action(function ()
        Gac2Gas.RequestEmbracePlayerAnswer(playerId, expressionId, false, tp)
    end))
end



function Gas2Gac.ReplyQmpkZongJueSaiOverView(index, zhanduiId, title, memberData, isFinish)

	local list = MsgPackImpl.unpack(memberData)
	if not list then return end
	local memberInfos = {}
	for i = 0, list.Count - 1 do
		local info = list[i]
		local memberId = info[0]
		local serverName = info[1]
		local fakeAppearance = CreateFromClass(CPropertyAppearance)
		fakeAppearance:LoadFromString(info[2], EnumClass.Undefined, EnumGender.Undefined)
		local memberName = info[3]
		table.insert(memberInfos, {
			memberId = memberId,
			memberName = memberName,
			serverName = serverName,
			fakeAppearance = fakeAppearance,
			})
	end
	if CUIManager.IsLoaded(CLuaUIResources.QMPKTopFourWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateQMPKTopFourInfos", index, zhanduiId, title, memberInfos, isFinish)
	else
		CUIManager.ShowUI(CLuaUIResources.QMPKTopFourWnd)
	end

end

function Gas2Gac.OpenHandWriteEffectWnd(writeId)
    CLuaHandWriteEffectMgr.ShowHandWriteEffect(writeId,nil)
end


function Gas2Gac.PlayerInputContactInfoByUseItemSuccess()
	g_ScriptEvent:BroadcastInLua("PlayerInputContactInfoByUseItemSuccess")
end


function Gas2Gac.ZhouNianShopAlertStatusQueryResult( bAlert,remainSeconds )
    CLuaActivityAlert.s_GiftAlertType = EnumGiftAlertType.ZNQ
    g_ScriptEvent:BroadcastInLua("ZhouNianShopAlertStatusQueryResult",bAlert,remainSeconds)
end


function Gas2Gac.ReplyWorldCupTodayJingCaiDetail(pid, resultData, value)
    CLuaWorldCupMgr.m_Detail_Pid = pid
    CLuaWorldCupMgr.m_Detail_ResultData = resultData
    CLuaWorldCupMgr.m_Detail_Value = value

    CUIManager.ShowUI(CUIResources.WorldCupJingCaiDetailWnd)
end

--#region 蹴鞠

function Gas2Gac.QueryCuJuCrossSignUpResult(bAttend, attendTimes, rewardTimes)
    g_ScriptEvent:BroadcastInLua("QueryCuJuCrossSignUpResult", bAttend, attendTimes, rewardTimes)
end

function Gas2Gac.UpdateCuJuPlayerPlayInfo(scoreInfo_U, playInfo_U, winForce)
end

function Gas2Gac.UpdateCuJuBallPosAndScore(ballPos_U, forceScore_U)
    LuaCuJuMgr:UpdateCuJuBallPosAndScore(ballPos_U, forceScore_U)
end

function Gas2Gac.QueryCuJuWeekRankResult(playerRank, playerInfo_U, totalRank_U)
    LuaCuJuMgr:QueryCuJuWeekRankResult(playerRank, playerInfo_U, totalRank_U)
end

function Gas2Gac.QueryCuJuTotalRankResult(playerRank, playerInfo_U, totalRank_U)
    LuaCuJuMgr:QueryCuJuTotalRankResult(playerRank, playerInfo_U, totalRank_U)
end

function Gas2Gac.OpenCuJuPlayerPlayInfoWnd(scoreInfo_U, playInfo_U, winForce)
    LuaCuJuMgr:OpenCuJuPlayerPlayInfoWnd(scoreInfo_U, playInfo_U, winForce)
end

-- 旧蹴鞠 废弃RPC
function Gas2Gac.InvitePlayerJoinCuJuPractice(interval)
end

function Gas2Gac.QueryCuJuGameMatchInfoResult(watchInfo_U)
end

function Gas2Gac.RequestUseCuJuLotterySuccess(awardItemId)
end

function Gas2Gac.SendCuJuQieCuoRequest(playerId)
end

function Gas2Gac.UpdateCuJuSignUpInfo(bPracticeSign, bPlaySign, enterTimes, totalTimes, playStage)
end

function Gas2Gac.ShowCuJuSelectPosWnd(position)
end

function Gas2Gac.UpdateSetPositionResult(position)
end

--#endregion 蹴鞠

function Gas2Gac.ReplyServerSubmitInfo(data)
    local list = MsgPackImpl.unpack(data)
    if not list then return end
    local id, submitNum, totalNum = list[0],list[1],list[2]
    g_ScriptEvent:BroadcastInLua("ReplyServerSubmitInfo",id,submitNum,totalNum)
end

--以下的空方法是二设RPC，功能已废弃，因此方法置空
--------------------------------------------------------------------
function Gas2Gac.RequestLoadErSheArPlayDataResult(key, dataStr)
end
function Gas2Gac.ErSheQueryOuyuRoleResult(roleId, data)
end
function Gas2Gac.ErSheShowMessage(messageId, messageData)
end
function Gas2Gac.ErSheRoleProgressChanged(roleId, before, current)
end
function Gas2Gac.ErSheFinishedJuQing(roleId, juqingId, progressDelta)
end
function Gas2Gac.ErSheGetJuQingHistoryResult(juqingId, pathData)
end
function Gas2Gac.ErSheRequestStartJuQingResult(roleId,bTrigger,juqingId)
end
function Gas2Gac.ErSheQueryZhaoHuanRoleListResult(data)
end
function Gas2Gac.ErSheRequestZhaoHuanRoleResult(roleId, roledata)
end
function Gas2Gac.ErSheRequestTouchRoleResult(roleId, bPositive, jiaohuId)
end
function Gas2Gac.ErSheRequestBuyGiftSuccess(roleId, giftId)
end
function Gas2Gac.ErSheQueryFinishedJuQingResult(data)
end
--------------------------------------------------------------------


function Gas2Gac.OpenHuaZhuZiWnd()
    CUIManager.ShowUI(CLuaUIResources.HuaZhuWnd)
end


function Gas2Gac.SendCityWarZiCaiShopInfo(zicai, items)
    g_ScriptEvent:BroadcastInLua("SendCityWarZiCaiShopInfo",zicai, items)
end

function Gas2Gac.SyncQueryStarBiwuOverview(paimaiEndTime, oneShootOpenTime, selfBetIdx, paimaiItemsUd)
    g_ScriptEvent:BroadcastInLua("SyncQueryStarBiwuOverview", paimaiEndTime, oneShootOpenTime, selfBetIdx, paimaiItemsUd)
  end


function Gas2Gac.SculptureEquipSuccess(equipU)
    local item = CCommonItem(false,equipU)
    CItemMgr.Inst:AddItem(item)
    g_ScriptEvent:BroadcastInLua("SculptureEquipSuccess")
    g_MessageMgr:ShowMessage("Equip_Sculpture_Success")
end


function Gas2Gac.SyncNpcFightingHp(npcId, percent)
	CLuaNpcFightingWnd.NpcHp[npcId] = percent
	g_ScriptEvent:BroadcastInLua("SyncNpcFightingHp", npcId, percent)
end

function Gas2Gac.ReplyMecicineBoxInfo(lastTemplateIdStr, itemDataUD, isAutoCollect, lastOneKeyUseTime)
    CLuaBuffItemWnd.Data.lastTemplateIdStr = lastTemplateIdStr
    local dic = MsgPackImpl.unpack(itemDataUD)
    CLuaBuffItemWnd.Data.itemDataUD = dic
    CLuaBuffItemWnd.Data.isAutoCollect = isAutoCollect
    CLuaBuffItemWnd.Data.lastOneKeyUseTime = lastOneKeyUseTime
    g_ScriptEvent:BroadcastInLua("OnBuffItemWndDataChange")

end

function Gas2Gac.StartUITrigger(triggerId)
	if CUIManager.IsLoading(CLuaUIResources.UITriggerWnd) or CUIManager.IsLoaded(CLuaUIResources.UITriggerWnd) then
		CUIManager.CloseUI(CLuaUIResources.UITriggerWnd)
	end

	CLuaUITriggerWnd.s_TriggerId = triggerId
	CUIManager.ShowUI(CLuaUIResources.UITriggerWnd)
end


-- 同步玩家积分发生改变
-- score 新的积分
function Gas2Gac.SyncDyePlayScore(score)
	-- print("SyncDyePlayScore------------------------", score)
	g_ScriptEvent:BroadcastInLua("SyncDyePlayScore",score)
end

-- 染色剂副本显示即将招怪的特效
-- 位置 持续时间 特效类型
function Gas2Gac.SyncDyePreGenMonster(posX, posY, duration, fxType)

    g_ScriptEvent:BroadcastInLua("SyncDyePreGenMonster",posX, posY, duration, fxType)
end


-- bug围场副本,同步副本进度
-- 已捕捉个数 剩余个数
function Gas2Gac.SyncBugHuntProgress(catchedCount, leftCount)
    CLuaBugWeiChangTopRightWnd.Data={GetCount = catchedCount,LeftCount = leftCount}
    g_ScriptEvent:BroadcastInLua("OnSyncBugHuntProgress",catchedCount,leftCount)
end

-- 播放天雷特效
function Gas2Gac.SyncBugHuntThunder(catchedMonsterGidUd)
    -- 被抓住的怪的EngineId的List
    local catchedMonsterGidList = MsgPackImpl.unpack(catchedMonsterGidUd)
    local effectId = 88800219
    for i=0,catchedMonsterGidList.Count - 1 do
        local obj = CClientObjectMgr.Inst:GetObject(catchedMonsterGidList[i])
        if obj and TypeIs(obj, typeof(CClientMonster)) then
            local scale = 1.0       --  小怪物销毁特效  小怪物obj.TemplateId = 18004665
            if obj.TemplateId == 18004778 then  --  如果是大怪物，则播放大怪物销毁特效
                scale = 1.6
            end
            local fx = CEffectMgr.Inst:AddObjectFX(effectId, obj.RO, 0, scale, 1.0, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            obj.RO:AddFX(effectId, fx, -1)
        end
    end
end


function Gas2Gac.SyncHalloweenTaskProgress(infoUd, eachDataLen)
	local list = MsgPackImpl.unpack(infoUd)
	g_ScriptEvent:BroadcastInLua("OnSyncHalloweenTaskProgress",list)
end

-- 某个任务的进度或者状态发生改变
function Gas2Gac.SyncHalloweenCollectProgress(mailId, newCount, status)
	-- print("SyncHalloweenCollectProgress", mailId, newCount, status)
	g_ScriptEvent:BroadcastInLua("SyncHalloweenCollectProgress",mailId)
end


function Gas2Gac.SendRanSeJiBossStatus(hp, hpFull)
	g_ScriptEvent:BroadcastInLua("SendRanSeJiBossStatus",hp,hpFull)
end

-- Boss开始刷新状态条
-- hpThreshold -> boss进入 血量百分比 hpThreshold 以下的状态        boss变身结束
function Gas2Gac.SyncDyePlayBossRefreshStatus(hpThreshold)
    luaCrazyDyeMgr.ChangeEndTime = 0
    luaCrazyDyeMgr.IsChange = false
end



-- 副本结算
function Gas2Gac.SyncDyePlaySummary(rankUd, statUd, rewardUd, extraUd, bGetReward)
	local rank_list = MsgPackImpl.unpack(rankUd)
	local leaderId = rank_list[0] -- 队长id
    LuaCrazyDyeResultWnd.rankData = {}
    -- local rankData = {}
	-- 前三名数据,按照顺序同步,注意pid为0时,数据无意义
    for i = 1,rank_list.Count-1,8 do
        local t = {}
		t.pid = rank_list[i]		    -- 玩家id
		t.score = rank_list[i+1]	    -- 玩家积分
		t.name = rank_list[i+2]		    -- 名字
		t.bXianShen = rank_list[i+3]    -- 是否为仙身
        t.level = rank_list[i+4] 	    -- 当前等级
        t.gender = rank_list[i+5] 	    -- 性别
		t.cls = rank_list[i+6] 	        -- 职业
		t.expression = rank_list[i+7] 	-- 表情
        table.insert(LuaCrazyDyeResultWnd.rankData,t)
        -- print("SyncDyePlaySummary",t.pid,t.score,t.name,t.bXianShen,t.level,t.gender,t.cls,t.expression)
	end

	-- 副本统计数据
    LuaCrazyDyeResultWnd.personalData = {}
	local stat_list = MsgPackImpl.unpack(statUd)
	LuaCrazyDyeResultWnd.personalData.s_pid = stat_list[0]			-- 当前角色id
	LuaCrazyDyeResultWnd.personalData.score = stat_list[1]			-- 积分
	LuaCrazyDyeResultWnd.personalData.rankPos = stat_list[2]		-- 排行榜中位置
	LuaCrazyDyeResultWnd.personalData.rankLen = stat_list[3]		-- 总人数
	LuaCrazyDyeResultWnd.personalData.killCount = stat_list[4]		-- 击杀个数
	LuaCrazyDyeResultWnd.personalData.dps = stat_list[5]			-- 输出

    LuaCrazyDyeResultWnd.rewardData = {}
	local reward_list = MsgPackImpl.unpack(rewardUd)
    for i = 1,reward_list.Count,2 do
        local t = {}
		t.itemId = reward_list[i-1]	-- 奖励物品
        t.count = reward_list[i]		-- 奖励个数
        table.insert(LuaCrazyDyeResultWnd.rewardData,t)
	end

    -- bGetReward 是否领取过奖励
    LuaCrazyDyeResultWnd.bGetReward = bGetReward
    CUIManager.ShowUI("CrazyDyeResultWnd")
end



function Gas2Gac.SyncHalloweenCardOrder(cardOrderValue)
    LuaJigsawWnd.CardOrderValue = cardOrderValue..""
end


function Gas2Gac.ShowYingLingTransformPannel()
    g_ScriptEvent:BroadcastInLua("ShowYingLingSkillRoot")
end


function Gas2Gac.SyncRescueChristmasTreeGrowthValue(value)
    g_ScriptEvent:BroadcastInLua("UpdateChristmasTreeState",value)
end



function Gas2Gac.ShowGuideRegion(eventid,text,duration, params)
    CLuaGuideRegionWnd.s_Text = text
    CLuaGuideRegionWnd.s_EventId=eventid
    CLuaGuideRegionWnd.s_Duration = duration
    local list = MsgPackImpl.unpack(params)
    CLuaGuideRegionWnd.s_Params = {
        clipPos = {list[0],list[1]},
        clipSize = {list[2],list[3]},
        textPos =  {list[4],list[5]},
        textSide = list[6]>0 and "right" or "left"
    }

    CUIManager.ShowUI(CLuaUIResources.GuideRegionWnd)
end

function Gas2Gac.SyncButterflyPickNpcResult(npcId, data)

    if data then
		local argvs = MsgPackImpl.unpack(data)
		if argvs and argvs.Count > 0 then
			CLuaNPCXuanMeiMgr.CandidateNpcIds = {}
            CLuaNPCXuanMeiMgr.CandidateId2Vote = {}
            CLuaNPCXuanMeiMgr.IsPickNpcFinish = argvs[0]
			CLuaNPCXuanMeiMgr.CurMaxVoteTimes = argvs[1]
			for i=2,argvs.Count -2,2 do
				local npcId = argvs[i]
				local vote = argvs[i+1]
				table.insert(CLuaNPCXuanMeiMgr.CandidateNpcIds,argvs[i])
				CLuaNPCXuanMeiMgr.CandidateId2Vote[npcId] = vote
			end
		end
        --刷新界面
        g_ScriptEvent:BroadcastInLua("RefreshNPCXuanMeiWnd",npcId)
	end
end



function Gas2Gac.SavePictureInfoSuccess(taskId, conditionId, pictureIndex)
    Gac2Gas.RequestLoadPictureInfo(taskId, conditionId, pictureIndex)
end

function Gas2Gac.SendPictureInfo(taskId, conditionId, pictureIndex, pictureInfoU)
    local info = MsgPackImpl.unpack(pictureInfoU)
    g_ScriptEvent:BroadcastInLua("MakeClothesWnd_OnPictureLoad",{ pictureIndex, info })
end


function Gas2Gac.DanZhuQuerySignUpStatusResult(bSignUp, gameType, times)
    LuaChildrenDay2020Mgr.gameSignUp = bSignUp
    LuaChildrenDay2020Mgr.gameSignUpType = gameType
    LuaChildrenDay2020Mgr.gameTodayRestNum = times
    if CUIManager.IsLoaded(CLuaUIResources.ChildrenDayPlayShow2020Wnd) then
			g_ScriptEvent:BroadcastInLua('UpdateChildrenDayPlayShow2020Info')
    else
      CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayShow2020Wnd)
    end
end

function Gas2Gac.DanZhuSelectDanZhuType(remainTime)
    LuaChildrenDay2020Mgr.gameSelectRemainTime = remainTime
    if CUIManager.IsLoaded(CLuaUIResources.ChildrenDayPlayChooseBall2020Wnd) then
			g_ScriptEvent:BroadcastInLua('UpdateChildrenDayChooseBall')
    else
      CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayChooseBall2020Wnd)
    end
end

function Gas2Gac.DanZhuVoteEarlyEndConfirm(remainTime, totalTime, voteU)
    LuaChildrenDay2020Mgr.gameVoteRemainTime = remainTime
    LuaChildrenDay2020Mgr.gameVoteTotalTime = totalTime

    LuaChildrenDay2020Mgr.gameVoteTable = {}
    local list = MsgPackImpl.unpack(voteU)
    for i = 0, list.Count - 1, 2 do
      local playerId = tonumber(list[i])
      local vote = tonumber(list[i + 1])
      table.insert(LuaChildrenDay2020Mgr.gameVoteTable,{playerId,vote})
    end

    if CUIManager.IsLoaded(CLuaUIResources.ChildrenDayPlayChoose2020Wnd) then
			g_ScriptEvent:BroadcastInLua('UpdateChildrenDayGameVote')
    else
      CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayChoose2020Wnd)
    end
end

function Gas2Gac.DanZhuSyncPlayInfo(infoU)
    local list = MsgPackImpl.unpack(infoU)

    LuaChildrenDay2020Mgr.gamePlayerInfo  = {}
    for i = 0, list.Count-1, 4 do
      local playerId = tonumber(list[i])
      local playerLive = tonumber(list[i+1])
      local playerName = list[i+2]
      local playerRank = tonumber(list[i+3])
      table.insert(LuaChildrenDay2020Mgr.gamePlayerInfo,{rank = playerRank,playerId=playerId,name=playerName,playerLive=playerLive})
    end

    table.sort(LuaChildrenDay2020Mgr.gamePlayerInfo,function(a,b)
      return a.rank < b.rank
    end)

    g_ScriptEvent:BroadcastInLua('UpdateChildrenDayPlayForceScoreInfo2020')
end

function Gas2Gac.DanZhuPlayResult(gameType, danzhuType, rank, killCount, liveCount, bWashOut)
  LuaChildrenDay2020Mgr.gameResultType = gameType
  LuaChildrenDay2020Mgr.gameResultRank = rank
  LuaChildrenDay2020Mgr.gameResultKillCount = killCount
  LuaChildrenDay2020Mgr.gameResultLiveCount = liveCount
  LuaChildrenDay2020Mgr.gameResultbWashOut = bWashOut
  LuaChildrenDay2020Mgr.gameResultDanzhuType = danzhuType

  if CUIManager.IsLoaded(CLuaUIResources.ChildrenDayPlayResult2020Wnd) then
    g_ScriptEvent:BroadcastInLua('UpdateChildrenDayGameResult')
  else
    CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayResult2020Wnd)
  end
end
function Gas2Gac.SyncPigCoinParams(count,record)
    local addCount = 0
    if CClientMainPlayer.Inst then
        local oldCount = CClientMainPlayer.Inst.PlayProp.PigSavePotCount
        CClientMainPlayer.Inst.PlayProp.PigSavePotCount = count
        addCount = count-oldCount
        CClientMainPlayer.Inst.PlayProp.PigRewardGetRecord = record
    end
	g_ScriptEvent:BroadcastInLua("SyncPigCoinParams",count,addCount,record)
end



function Gas2Gac.CrossSXDDZChallenged(challengerId, challengerName, serverName, duration)
	CLuaCrossSXDDZChampionConfirmWnd.Message = g_MessageMgr:FormatMessage("CROSS_SXDDZ_CHAMPION_CHALLENGE_MSG", serverName, challengerName)
	CLuaCrossSXDDZChampionConfirmWnd.WaitDuration = duration
	CLuaCrossSXDDZChampionConfirmWnd.ActionType = 1
	CUIManager.CloseUI(CLuaUIResources.CrossSXDDZChampionConfirmWnd)
	CUIManager.ShowUI(CLuaUIResources.CrossSXDDZChampionConfirmWnd)
end


function Gas2Gac.CrossSXDDZShowStartCountDown(targetId, targetName, targetServerName, duration)
	CLuaCrossSXDDZChampionConfirmWnd.Message = g_MessageMgr:FormatMessage("CROSS_SXDDZ_ENTER_CONFIRM_MSG")
	CLuaCrossSXDDZChampionConfirmWnd.WaitDuration = duration
	CLuaCrossSXDDZChampionConfirmWnd.ActionType = 2
	CUIManager.CloseUI(CLuaUIResources.CrossSXDDZChampionConfirmWnd)
	CUIManager.ShowUI(CLuaUIResources.CrossSXDDZChampionConfirmWnd)
end

function Gas2Gac.CrossSXDDZQueryCurrentChallengeInfoResult(infoUD)
	g_ScriptEvent:BroadcastInLua("CrossSXDDZQueryCurrentChallengeInfoResult", infoUD)
end


function Gas2Gac.SendRenRenZhongCaiPiaoInfo(itemId, place, pos, code)
    CLuaWorldCupMgr.m_RenRenZhongCaiPiaoCode = code
    CUIManager.ShowUI(CUIResources.WorldCupRenRenZhongCaiPiaoWnd)
end

-- 请求是否能切换词条的返回
-- <param name="equipNewGrade">如果装备在身上,切换后有可能等级发生变化而不能穿戴 需要提示玩家</param>
function Gas2Gac.RequestCanSwitchEquipWordSetReturn( place, pos, targetEquipId, equipNewGrade, result)
	g_ScriptEvent:BroadcastInLua("RequestCanSwitchEquipWordSetReturn",place, pos, targetEquipId, equipNewGrade, result )
end

--[[
    @desc: 粽子玩法结算RPC
    author:CodeGize
    time:2019-04-17 14:06:33
    --@isSuccess: 是否胜利
    @return:
]]
function Gas2Gac.DuanWuZongZiPlayEnd(isSuccess)
    CLuaDwZongziResultWnd.IsWin = isSuccess
    CUIManager.ShowUI("DwZongziResultWnd")
end

--[[
    @desc: 粽子玩法同步怪物血量
    author:CodeGize
    time:2019-04-18 18:45:01
    --@info:
    @return:
]]
function Gas2Gac.UpdateDuanWuZongZiMonsterHp(info)
    g_ScriptEvent:BroadcastInLua("OnProcessMonsterHP",info)
end

--@endregion 事件结束


function Gas2Gac.SyncStarBiwuAudienceBasic(audienceBasicUd, sceneBasicUd, audienceRelationUd)
	LuaStarInviteMgr.ShowAudienceTable = nil
	LuaStarInviteMgr.ShowAudienceObjectTable = nil
	local list = MsgPackImpl.unpack(audienceBasicUd)
  if not list then
    return
  end

	LuaStarInviteMgr.ShowAudienceTable = {}

  for i=0,list.Count-1,5 do
    local id = list[i]
    local atype = list[i+1]
    local name = list[i+2]
    local clazz = list[i+3]
    local gender = list[i+4]
		table.insert(LuaStarInviteMgr.ShowAudienceTable,{id = id,atype = atype,name = name,clazz = clazz,gender = gender})
  end
	-- List
	-- 		audienceId
  --    type
	-- 		name
	-- 		class
	-- 		gender

	local sceneBasicList = MsgPackImpl.unpack(sceneBasicUd)
	if not sceneBasicList then
		return
	end

	local bShowGossip = sceneBasicList[0] -- 是否显示闲话预览
	local bShowVictory = sceneBasicList[1] -- 获胜预览

	LuaStarInviteMgr.ShowGossipSign = bShowGossip
	LuaStarInviteMgr.ShowVictorySign = bShowVictory

	local audienceRelationList = MsgPackImpl.unpack(audienceRelationUd)
	if not audienceRelationList then
		return
	end

	for i = 0, audienceRelationList.Count-1, 2 do
		local audienceId = audienceRelationList[i] 		-- 亲友id
		local npcEngineId = audienceRelationList[i+1]	-- 对应的NPC
		if not LuaStarInviteMgr.ShowAudienceObjectTable then
			LuaStarInviteMgr.ShowAudienceObjectTable = {}
		end
		LuaStarInviteMgr.ShowAudienceObjectTable[audienceId] = npcEngineId
	end

	CUIManager.ShowUI(CLuaUIResources.StarInviteWatchWnd)
end

function Gas2Gac.SyncStarBiwuAudienceAction(audienceActionUd)
	LuaStarInviteMgr.AudienceSettingInfo = nil
	local list = MsgPackImpl.unpack(audienceActionUd)
  if not list or list.Count < 5 then
    return
  end

	LuaStarInviteMgr.AudienceSettingInfo = {}

	local id = list[0]
	local gossip = list[1]
	local gossipFre = list[2]
	local winSelfAction = list[3]
	local winEnemyAction = list[4]

	LuaStarInviteMgr.AudienceSettingInfo = {id = id, gossip = gossip, gossipFre = gossipFre, winSelfAction = winSelfAction, winEnemyAction = winEnemyAction}
	-- List
	-- 		audienceId

	-- 		gossip
	-- 		gossipFrequency，几秒一次
	-- 		winSelfAction

	-- 		winEnemyAction

	if CUIManager.IsLoaded(CLuaUIResources.StarInviteSettingWnd) then

	else
		CUIManager.ShowUI(CLuaUIResources.StarInviteSettingWnd)
	end
end

function Gas2Gac.StarBiwuAudienceReleaseResult(audienceId, engineId, bSuccess)
	if not LuaStarInviteMgr.ShowAudienceObjectTable then
		LuaStarInviteMgr.ShowAudienceObjectTable = {}
	end
	if bSuccess then
		LuaStarInviteMgr.ShowAudienceObjectTable[audienceId] = engineId
		g_ScriptEvent:BroadcastInLua('UpdateStarInviteAudienceRelease',audienceId)
	end
end

function Gas2Gac.StarBiwuAudiencePackResult(audienceId, engineId, bSuccess)
	if bSuccess then
		if LuaStarInviteMgr.ShowAudienceObjectTable and LuaStarInviteMgr.ShowAudienceObjectTable[audienceId] then
			LuaStarInviteMgr.ShowAudienceObjectTable[audienceId] = nil
			g_ScriptEvent:BroadcastInLua('UpdateStarInviteAudiencePack',audienceId)
		end
	end
end

-- 更新NPC的位置、方向信息是否成功
function Gas2Gac.SyncStarBiwuUpdateLocationRes(audienceId, bSuccess)
	if bSuccess then
		g_ScriptEvent:BroadcastInLua('UpdateStarInviteNpcPosSucc',audienceId)
	end
end

-- 更新NPC的动作信息是否成功
function Gas2Gac.SyncStarBiwuUpdateActionRes(audienceId, bSuccess)
end

-- 打开/关闭了取胜预览
function Gas2Gac.SyncStarBiwuViewVictoryRes(bOpen)
end

-- 打开/关闭了闲话预览
function Gas2Gac.SyncStarBiwuViewGossipRes(bOpen)
end

-- NPC说话
function Gas2Gac.StarBiwuShowNpcSayContent(engineId, content)
	if not LuaStarInviteMgr.ShowGossipTable then
		LuaStarInviteMgr.ShowGossipSign = {}
	end
	LuaStarInviteMgr.ShowGossipSign[engineId] = content
	g_ScriptEvent:BroadcastInLua('UpdateStarInviteNpcGossipShow', engineId)
end



--@region Gas2Gac

--@desc 查看一个中邪NPC的返回，对应Gac2Gas.ButterflyPlayerReadNpc，切换选中NPC的时候使用
--感觉与ButterflyUpdateUIStatus功能重复
--@npcIdx: [1, 5]
function Gas2Gac.ButterflyPlayerReadNpcPopWnd(npcIdx)
    Gac2Gas.ButterflyGetUIStatus()
end

--@desc 解锁某段记忆成功，对应Gac2Gas.ButterflyUnlockMemorySegment
function Gas2Gac.ButterflyUnlockMemorySuccess(npcIdx, segmentIdx)
    -- 解锁成功，刷新界面(可以由ButterflyUpdateUIStatus完成)，并且弹出Story
    if not npcIdx or not segmentIdx then return end
    LuaButterflyCrisisMgr.UpdateMemoryUnlockInfo(npcIdx, segmentIdx, true)
end

--@desc: 更新主界面信息
--@npcIdx:上一次查看的NPC的编号/第一个已经解锁但没有read过的NPC
--@rewardTimes:已领奖励bitmap
--@resultTbl: table of 30 true/false :|npc|mem1|mem2|mem3|mem4|solved|
function Gas2Gac.ButterflyUpdateUIStatus(npcIdx, rewardTimes, resultTbl)
    LuaButterflyCrisisMgr.m_NPCOpenInfo = {}
    LuaButterflyCrisisMgr.m_MemoryUnlockInfo = {}
    LuaButterflyCrisisMgr.m_BossSolvedInfo = {}

    LuaButterflyCrisisMgr.ParseRewardTakenInfos(rewardTimes)

    local resultList = MsgPackImpl.unpack(resultTbl)
    for i = 0, resultList.Count-1, 6 do
        -- todo resultList[i] npc是否已读的信息，目前用不到
        local isNPCOpen = resultList[i]
        local mem1Unlock = resultList[i+1]
        local mem2Unlock = resultList[i+2]
        local mem3Unlock = resultList[i+3]
        local mem4Unlock = resultList[i+4]
        local isBossSolved = resultList[i+5]

        table.insert(LuaButterflyCrisisMgr.m_NPCOpenInfo, isNPCOpen)
        table.insert(LuaButterflyCrisisMgr.m_BossSolvedInfo, isBossSolved)
        table.insert(LuaButterflyCrisisMgr.m_MemoryUnlockInfo, {mem1Unlock, mem2Unlock, mem3Unlock, mem4Unlock})
    end
    LuaButterflyCrisisMgr.OpenButterflyCrisisMainWnd(npcIdx)
end

--@desc 领取奖励成功
function Gas2Gac.ButterflyCollectRewardSuccess(rewardIdx, itemCountTbl)
    LuaButterflyCrisisMgr.ParseRewardTakenInfos(rewardIdx)

    local itemList = MsgPackImpl.unpack(itemCountTbl)
    local rewardInfos = {}
    for i = 0, itemList.Count-1, 2 do
        local itemId = itemList[i]
        local count = itemList[i+1]
        table.insert(rewardInfos, { ItemId = itemId, Count = count })
    end
    LuaButterflyCrisisMgr.ShowRewardGetWnd(rewardInfos)
end

-- 登录提示
function Gas2Gac.ButterflyPlayerFirstUnreadNpc(npcIdx)
    LuaButterflyCrisisMgr.ShowButterflyCrisisNotice(npcIdx)
end

-- 完成boss战
function Gas2Gac.ButterflyPlayerCompleteBossFight(npcIdx)
    LuaButterflyCrisisMgr.UpdateBossSolvedInfo(npcIdx, true)
end

--@desc 玩家进入场景
--@isUnderFight:当前场景是否进入战斗，0: 准备阶段不切黑夜，1:战斗阶段切黑夜
function Gas2Gac.ButterflyPlayerEnterScene(isUnderFight)
    if isUnderFight == 1 then
        if CScene.MainScene then
            CScene.MainScene.IsNightScene = true
        end
        if CRenderScene.Inst then
            CRenderScene.Inst:Change2Night()
        end
    end
end
--@endregion



function Gas2Gac.QueryQuanMinHuoYunInfoResult(submitClass, bAssist, day, listUD, rank, rankTotal, submitNum, totalGetNum, totalFillNum, totalSubmitNum, bCanGetFullAward, bFullAwardGot, bAllProgressFull)

	local list = MsgPackImpl.unpack(listUD)
	if not list then return end

    local count = 1
	for i = 0, list.Count - 1, 4 do
		local class = list[i + 0]
		local progress = list[i + 1]
		local assistClass1 = list[i + 2]
		local assistClass2 = list[i + 3]


        local data = {}
        data.class = class
        data.progress = progress
        data.assistClass1 = assistClass1
        data.assistClass2 = assistClass2
        CLuaQMHuoYunMgr.list[count] = data
        count = count + 1
    end

    CLuaQMHuoYunMgr.submitClass = submitClass
    CLuaQMHuoYunMgr.bAssist = bAssist
    CLuaQMHuoYunMgr.day = day
    CLuaQMHuoYunMgr.rank = rank
    CLuaQMHuoYunMgr.rankTotal = rankTotal
    CLuaQMHuoYunMgr.submitNum = submitNum
    CLuaQMHuoYunMgr.totalGetNum = totalGetNum
    CLuaQMHuoYunMgr.totalFillNum = totalFillNum
    CLuaQMHuoYunMgr.totalSubmitNum = totalSubmitNum
    CLuaQMHuoYunMgr.bCanGetFullAward = bCanGetFullAward
    CLuaQMHuoYunMgr.bFullAwardGot = bFullAwardGot
    CLuaQMHuoYunMgr.bAllProgressFull = bAllProgressFull

    if CUIManager.IsLoaded(CLuaUIResources.QMHuoYunSubWnd) then --窗口已经存在则刷新窗口数据
        g_ScriptEvent:BroadcastInLua("QueryQuanMinHuoYunInfoResult")

    else
        CUIManager.ShowUI(CLuaUIResources.QMHuoYunSubWnd)
    end
end

function Gas2Gac.SetQuanMinHuoYunAssistClassSuccess(day, place, class)


    CLuaQMHuoYunMgr.day = day
    CLuaQMHuoYunMgr.submitClass = class
    g_ScriptEvent:BroadcastInLua("SetQuanMinHuoYunAssistClassSuccess")
end

function Gas2Gac.SubmitTongXinJingChipSuccess(day, place, class, count, progress, submitNum, totalSubmitNum)


    CLuaQMHuoYunMgr.day = day
    CLuaQMHuoYunMgr.submitNum = submitNum
    CLuaQMHuoYunMgr.totalSubmitNum = totalSubmitNum
    if place > 0 then --0则代表提交多余同心镜成功，无需设置progress
        CLuaQMHuoYunMgr.list[place].progress = progress
    end
    g_ScriptEvent:BroadcastInLua("SubmitTongXinJingChipSuccess", place, progress)
end

function Gas2Gac.SendQuanMinHuoYunFullAwardSuccess(day)


    g_ScriptEvent:BroadcastInLua("SendQuanMinHuoYunFullAwardSuccess", day)
end

function Gas2Gac.QuanMinHuoYunHeChengYaoFangSuccess(yaoFangItemId)


    g_ScriptEvent:BroadcastInLua("QuanMinHuoYunHeChengYaoFangSuccess", yaoFangItemId)
end

function Gas2Gac.SyncFengHuoLingPlayValue(class, value, maxvalue, canFillNum)

    CLuaQMHuoYunMgr.lastFHLState = {class, value, maxvalue, canFillNum}

    g_ScriptEvent:BroadcastInLua("SyncFengHuoLingPlayValue", class, value, maxvalue, canFillNum)
end

function Gas2Gac.SyncFengHuoLingPlayValueFull()

    local msg = g_MessageMgr:FormatMessage("WuYi2020_QuanMinHuoYun_FHL_EnergyFull")
    MessageWndManager.ShowOKCancelMessage(msg, nil, DelegateFactory.Action(function ()
        CGamePlayMgr.Inst:LeavePlay()
     end), LocalString.GetString("留下"), LocalString.GetString("离开"), false)
end

function Gas2Gac.ShowFengHuoLingReliveWnd(dieNum, costValue)

    local msg =  g_MessageMgr:FormatMessage("WuYi2020_QuanMinHuoYun_FHL_Reborn", dieNum, math.floor(costValue))
    CLuaQMHuoYunMgr.reliveWndId = MessageWndManager.ShowOKMessage(msg, DelegateFactory.Action(function ()
        Gac2Gas.RequestRebornInFengHuoLing();
    end))
end

function Gas2Gac.CloseFengHuoLingReliveWnd()

    if CLuaQMHuoYunMgr.reliveWndId then
        MessageWndManager.CloseMessageBox(CLuaQMHuoYunMgr.reliveWndId)
    end
end

function Gas2Gac.ShowFengHuoLingPlayResult(class, value, maxValue, fillNum)


    CLuaQMHuoYunMgr.resultClass = class
    CLuaQMHuoYunMgr.resultValue = value
    CLuaQMHuoYunMgr.resultMaxValue = maxValue
    CLuaQMHuoYunMgr.resultFillNum = fillNum

    CUIManager.ShowUI(CLuaUIResources.QMHuoYunFHLResultWnd)
end

------------------------------------ Gas2Gac ------------------------------------
-- 报名成功
function Gas2Gac.ApplyChiJiSuccess()
    LuaPUBGMgr.m_IsApplied = true
    g_ScriptEvent:BroadcastInLua("ApplyChiJiSuccess")
end

-- 取消报名成功
function Gas2Gac.CancelApplyChiJiSuccess()
    LuaPUBGMgr.m_IsApplied = false
    g_ScriptEvent:BroadcastInLua("ApplyChiJiSuccess")
end

-- 设置包裹道具 pos 包裹位置 itemid是个double类型 uint32可能放不下Q
function Gas2Gac.SetChiJiBagItemId(pos, itemId)
    -- 用于更新某个格子
    LuaPUBGMgr.SetChiJiBagItemId(pos, itemId)
end

-- 同步整个包裹，返回是一个list<double>，整理包裹或者打开包裹的返回
function Gas2Gac.SendChiJibag(bag_U)
    local list = MsgPackImpl.unpack(bag_U)
    LuaPUBGMgr.UpdateWholePackageInfos(list, true)
end

-- 打开有内容的道具
-- 解释下key key是为了存储场景道具方便而根据坐标计算得到的。 key = posX * 1000 + posY
function Gas2Gac.SendChiJiItemExtraInfoToGac(key, itemId_U, bagItem_U)
    -- 舔包
    local list = MsgPackImpl.unpack(itemId_U)
    local bagList = MsgPackImpl.unpack(bagItem_U)
    LuaPUBGMgr.OpenLickingBag(key, list, bagList)
end

-- 场景所有道具
function Gas2Gac.SyncChiJiSceneItemInfo(itemInfo_U)
    -- table[key] = itemId
    local dict = MsgPackImpl.unpack(itemInfo_U)
    if not dict then return end

    if CommonDefs.IsDic(dict) then
        CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, value)
            local tempKey = tonumber(key)
            local itemId = tonumber(value)
            LuaPUBGMgr.CreateChiJiDropItem(key, itemId)
        end))
    else
    end
end

-- 场景新加道具
function Gas2Gac.ChiJiSceneAddItem(key, itemId)
    LuaPUBGMgr.CreateChiJiDropItem(key, itemId)
end

-- 场景移除道具
function Gas2Gac.ChiJiSceneDelItem(key)
    CPlayDropItemObject.DeleteCPlayDropItemObject(key)
end

-- 缩圈信息
function Gas2Gac.SyncChiJiRoundAreaInfo(centerPosX, centerPosY, halfSideLength, nextCenterPosX, nextCenterPosY, nextHalfSideLength)
    LuaPUBGMgr.UpdateChiJiMiniMap(centerPosX, centerPosY, halfSideLength, nextCenterPosX, nextCenterPosY, nextHalfSideLength)
end

-- 查询报名信息
function Gas2Gac.QueryChiJiApplyInfoResult(bApply)
    LuaPUBGMgr.OpenPUBGStartWnd(bApply)
end

-- 场景使用的缩圈信息
function Gas2Gac.SyncChiJiWarningAreaInfo(centerPosX, centerPosY, halfSideLength, nextCenterPosX, nextCenterPosY, nextHalfSideLength)
    CPUBGMgr.Inst:SyncChiJiArea(centerPosX, centerPosY, halfSideLength, nextCenterPosX, nextCenterPosY, nextHalfSideLength)
end

-- 观战
function Gas2Gac.AtachChiJiWatch(engineId)
    -- 被观战者的engineId
    CPUBGMgr.Inst:AttachChiJiWatcher(engineId)
end

-- 解除观战
function Gas2Gac.DetachChiJiWatch()
    CPUBGMgr.Inst:DetachChiJiWatcher()
end

-- 战斗属性同步
function Gas2Gac.SyncChiJiFightProp(engineId, propId, value)
    LuaPUBGMgr.SyncChiJiFightProp(engineId, propId, value)
end

-- 队伍栏 HP同步
function Gas2Gac.UpdateTeamMemberChiJiHp(playerId, hp, hpFull)
    CTeamMgr.Inst:UpdateTeamMemberHp(playerId, hp, hpFull, hpFull)
end

-- 更新掉落包裹
function Gas2Gac.UpdateChiJiDropBagInfo(key, item_U)
    local list = MsgPackImpl.unpack(item_U)
    LuaPUBGMgr.UpdateLickingBag(key, list)
end

-- 淘汰信息
-- {name, gender, class, score, totalScorem, killNum, dps, liveDuration}
function Gas2Gac.SendChiJiResult(rank, totalTeamCount, teamInfo)
    local list = MsgPackImpl.unpack(teamInfo)
    LuaPUBGMgr.OpenPUBGResultWnd(rank, totalTeamCount, list)
end

-- 设置临时技能成功
function Gas2Gac.SetActiveTempSkillSuccess(index, skillId)
    if not CClientMainPlayer.Inst then return end
    CClientMainPlayer.Inst:SyncActiveTempSkill(index, skillId)
    g_ScriptEvent:BroadcastInLua("SetActiveTempSkillSuccess")
end

-- 同步人数
function Gas2Gac.SyncChiJiLeftTeamCountInfo(playerNum, teamNum)
    LuaPUBGMgr.SyncChiJiLeftTeamCountInfo(playerNum, teamNum)
end

-- takeonId 玩家身上穿戴的发色
-- newPeiFangId 新获得的配方，用于某个配方第一次获得时播放盖章动画
-- openType 1-打开配方库 2-打开发色库
function Gas2Gac.SyncRanFaJiPlayData(peifangData_U, ranfajiData_U, takeonId, newPeiFangId, openType)
	CLuaRanFaMgr.m_OpenWndType = openType
	CLuaRanFaMgr.m_MyRanFaJiId = takeonId
	if newPeiFangId > 0 then
		CLuaRanFaMgr.m_NewRecipeId = newPeiFangId
		CUIManager.ShowUI(CLuaUIResources.RanFaRecipeGotWnd)
	end

	-- list中每一项是配方的id
	local recipeList = MsgPackImpl.unpack(peifangData_U)
	local recipeTbl = {}
	for i = 0, recipeList.Count - 1 do
		recipeTbl[recipeList[i]] = 1
	end
	CLuaRanFaMgr.m_RecipeTable = recipeTbl

	CLuaRanFaMgr.m_OwnRanFaJiTable = {}
	CLuaRanFaMgr.m_RanFaJiTable = {}
	local ranfajiData = MsgPackImpl.unpack(ranfajiData_U)
  	for i=0, ranfajiData.Count-1, 2 do
  		local id = ranfajiData[i]
  		local expiredTime = ranfajiData[i+1]
		CLuaRanFaMgr.m_OwnRanFaJiTable[id] = 1
		-- not discarded
		if expiredTime > 0 then
  			table.insert(CLuaRanFaMgr.m_RanFaJiTable, {id, expiredTime})
		end
  	end

	if openType > EnumSyncRanFaJiType.eDefault then
		CUIManager.ShowUI(CLuaUIResources.RanFaMainWnd)
	else
		g_ScriptEvent:BroadcastInLua("SyncRanFaJiPlayData", recipeTbl, CLuaRanFaMgr.m_RanFaJiTable, takeonId)
    end
    --外观界面依赖了这个rpc同步数据变化，但是上面的判断当在染发剂窗口中增加染发剂时不会发消息，所以这里专门增加一个消息
    g_ScriptEvent:BroadcastInLua("OnSyncRanFaJiPlayData", recipeTbl, CLuaRanFaMgr.m_RanFaJiTable, takeonId)
end


function Gas2Gac.SyncRanFaJiTaskProgress(progress)
	-- 这个进度要用一个全局变量保存，用于任务上的显示
end
function Gas2Gac.OpenRanFaJiColorSystemChooseWnd(itemId, place, pos)
    CLuaRanFaMgr.m_ChooseItemInfo = {itemId, place, pos}
    CUIManager.ShowUI(CLuaUIResources.RanFaRecipeChooseWnd)
end

function Gas2Gac.QingMing2020PvpSignUpPlayResult(success)

    g_ScriptEvent:BroadcastInLua("RefreshQingMing2020MatchSate", success)
end

function Gas2Gac.QingMing2020PvpCheckInMatchingResult(isInMatching)

    g_ScriptEvent:BroadcastInLua("RefreshQingMing2020MatchSate", isInMatching)
end

function Gas2Gac.QingMing2020PvpCancelSignUpResult(success)

    g_ScriptEvent:BroadcastInLua("RefreshQingMing2020MatchSate", not success)
end

function Gas2Gac.QingMing2020PvpQueryRankResult(rankInfo_U, selfInfo_U)

    CLuaQingMing2020Mgr.PlayerRankSelfInfo = {}
    CLuaQingMing2020Mgr.PlayerRankTable = {}
    local rankListData = MsgPackImpl.unpack(rankInfo_U)

    local count = rankListData.Count
    local dtable = {}
    for i=0,count-1 do
        local data = rankListData[i]
        local tt = {}
        tt.PlayerId = tonumber(data.PlayerId)
        tt.Name = data.Name
        tt.Class = tonumber(data.Class)
        if data.UsedTime ~= nil then
            tt.UsedTime = tonumber(data.UsedTime)
        end
        tt.Rank = tonumber(data.Rank)
        table.insert(dtable,tt)
    end

    table.sort(dtable,function(a,b) return a.Rank < b.Rank end)
    CLuaQingMing2020Mgr.PlayerRankTable = dtable

    g_ScriptEvent:BroadcastInLua("QingMing2020PvpQueryRankResult")
end

function Gas2Gac.QingMing2020PvpSyncStatusToPlayersInPlay(playStatus, enterStatusTime, PlayEndTime, extraData)

end

function Gas2Gac.QingMing2020PvpQueryPlayInfoResult(playerInfos_U)

    CLuaQingMing2020Mgr.PlayerBattleInfo = {}
    local playerListData = MsgPackImpl.unpack(playerInfos_U)
    local count = playerListData.Count
    local dtable = {}
    for i=0,count-1 do
        local data = playerListData[i]
        local tt = {}
        tt.PlayerId = tonumber(data.PlayerId)
        tt.Name = data.Name
        tt.Status = data.Status
        table.insert(dtable,tt)
    end

    CLuaQingMing2020Mgr.PlayerBattleInfo = dtable
    CUIManager.ShowUI(CLuaUIResources.QingMingBattleWnd)
end

function Gas2Gac.QingMing2020PveSyncStatusToPlayersInPlay(playStatus, enterStatusTime, roundStatus, roundStatusTime, PlayEndTime, extraData)

    local roundData = MsgPackImpl.unpack(extraData)
    local curRound = roundData[0]
    local roundEndTime = roundData[2]
    CLuaQingMing2020Mgr.RoundEndTime = roundEndTime

    if roundStatus == CLuaQingMing2020Mgr.EnumQingMing2020PveRoundStatus.Monster then
        g_ScriptEvent:BroadcastInLua("ShowQmPveJieZiSucceedFx")
    end

    if curRound and curRound > CLuaQingMing2020Mgr.CurPveRoundIndex then
        if playStatus ~= 2 then
            CLuaQingMing2020Mgr.CurPveRoundIndex = curRound
            CLuaQingMing2020Mgr.UpToRoundIndex = curRound
            g_ScriptEvent:BroadcastInLua("ShowQmPvePassRoundFx", CLuaQingMing2020Mgr.UpToRoundIndex)
        end
    end

    --通关成功
    if curRound == 5 then
        local isSucceed = roundData[1]
        if roundStatus == CLuaQingMing2020Mgr.EnumQingMing2020PveRoundStatus.End and isSucceed then
            CUIManager.ShowUI(CLuaUIResources.QingMingJieZiSucceedWnd)
        elseif roundStatus == CLuaQingMing2020Mgr.EnumQingMing2020PveRoundStatus.End and not isSucceed then
            CLuaQingMing2020Mgr.NeedAlert = false
            CUIManager.ShowUI(CLuaUIResources.QingMingJieZiXiaoChouWnd)
        end
    elseif playStatus == 2 then
        CLuaQingMing2020Mgr.NeedAlert = false
        CUIManager.ShowUI(CLuaUIResources.QingMingJieZiXiaoChouWnd)
    end
end

function Gas2Gac.QueryTempPlayDataInUDResult(key, ud, isNilUD)
	local data = nil
	if not isNilUD then
		data = MsgPackImpl.unpack(ud)
	end
	g_ScriptEvent:BroadcastInLua("QueryTempPlayDataInUDResult", key, ud, data)
end


function Gas2Gac.SyncQingYiXiangTongSignUpStatus(remainTimes,signUpStatus)
    CLuaQYXTMgr.signUpStatus = signUpStatus
    CLuaQYXTMgr.remainTimes = remainTimes
    -- g_ScriptEvent:BroadcastInLua("RefreshQYXTSignUpWnd", signUpStatus, remainTimes)

    if not CUIManager.IsLoaded(CLuaUIResources.QYXTSignUpWnd)  and CLuaQYXTMgr.ForceOpen then
        CLuaQYXTMgr.ForceOpen = false
        CLuaQYXTMgr.SignUpWndNotShow = true
        CUIManager.ShowUI(CLuaUIResources.QYXTSignUpWnd)
    else
        g_ScriptEvent:BroadcastInLua("RefreshQYXTSignUpWnd", signUpStatus, remainTimes)
    end
end

function Gas2Gac.SendQYXTBattleInfo(stage, isEnd, battleInfoTblUD)
    CLuaQYXTMgr.curSatge = stage
    CLuaQYXTMgr.isEnd = isEnd
    CLuaQYXTMgr.battleInfoTblUD = battleInfoTblUD
    if not CUIManager.IsLoaded(CLuaUIResources.QYXTResultWnd) then
        CUIManager.ShowUI(CLuaUIResources.QYXTResultWnd)
        CLuaQYXTMgr.RefreshResultWndNotShow = true
    end
    g_ScriptEvent:BroadcastInLua("RefreshQYXTBattleInfo", stage, isEnd, battleInfoTblUD)
end

function Gas2Gac.SendQYXTProgressInfo(stage, progressInfoUD)
    CLuaQYXTMgr.curSatge = stage
    if stage == 2 and stage ~= CLuaQYXTMgr.lastStage then
        g_ScriptEvent:BroadcastInLua("RefreshQYXTTaskView", stage)
    end
    CLuaQYXTMgr.lastStage = stage
    g_ScriptEvent:BroadcastInLua("RefreshQYXTProgressInfo", stage, progressInfoUD)

    if stage == 2 then
        local list = MsgPackImpl.unpack(progressInfoUD)
        local ghonorList = CreateFromClass(MakeGenericClass(List, UInt64))
        for i=3,list.Count-1,1 do
            CommonDefs.ListAdd(ghonorList, typeof(UInt64), list[i])
        end
        for i=ghonorList.Count,6,1 do
            CommonDefs.ListAdd(ghonorList, typeof(UInt64), 0)
        end
        EventManager.BroadcastInternalForLua(EnumEventType.UpdateQYXTHonorTeams,{ghonorList})
    end

    local list = MsgPackImpl.unpack(progressInfoUD)
    local ghonorList = CreateFromClass(MakeGenericClass(List, UInt64))
    for i=2,list.Count-1,1 do
        if stage == 2 then
            CommonDefs.ListAdd(ghonorList, typeof(UInt64), list[i])
        else
            CommonDefs.ListAdd(ghonorList, typeof(UInt64), 0)
        end
    end
    for i=ghonorList.Count,6,1 do
        CommonDefs.ListAdd(ghonorList, typeof(UInt64), 0)
    end
    EventManager.BroadcastInternalForLua(EnumEventType.UpdateQYXTHonorTeams,{ghonorList})
end

function Gas2Gac.AddQYXTSubmitFlowerFx(srcEngineId, dstEngineId)
    if not srcEngineId or not dstEngineId then
        return
    end
    local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
    local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)
    if not srcObj or not dstObj then
        return
    end
    local srcObjRO = srcObj.RO
    local dstObjRO = dstObj.RO

    if not srcObjRO or not dstObjRO then
        return
    end
    local fx = CEffectMgr.Inst:AddBulletFX(
        Valentine2020_Setting.GetData().SubmitDanDaoFxId,
      	srcObjRO,
        dstObjRO,
        0,1.0,-1,
        DelegateFactory.Action(function ()
            local fx2 = CEffectMgr.Inst:AddObjectFX(Valentine2020_Setting.GetData().SubmitNPCFxId,dstObjRO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        end)
    )
end
function Gas2Gac.SyncFuliWndStatus(bOpen, openIdxTblUd)
    local list = MsgPackImpl.unpack(openIdxTblUd)
    if list then
      local t = {}
      for i = 1,list.Count do
        table.insert(t,tonumber(list[i-1]))
      end
      LuaYunyingfuliMgr.fuliData = t
    end
      g_ScriptEvent:BroadcastInLua("ReceiveYunYingFuLiInfo",bOpen)
  end


function Gas2Gac.RequestChaijieZhushabiDetailsResult(place, pos, itemId, silver,totalCount)
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        if CClientMainPlayer.Inst.ItemProp.Silver < silver then
            local txt = g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy")
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end), nil, nil, nil, false)
        else

            local wordInfo = CreateFromClass(CChuanjiabaoWordType)
            wordInfo:LoadFromString(item.Item.ExtraVarData.Data)
            local washedStar = wordInfo.WashedStar or 0
            local star =washedStar>0 and washedStar or wordInfo.Star

            local msg = g_MessageMgr:FormatMessage("ZhuShaBi_Fenjie_Expensive_Confirm",
                SafeStringFormat3(LocalString.GetString("%s星"),star)..item.Name, silver,totalCount)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                Gac2Gas.RequestChaijieZhushabi(place, pos, itemId)
            end), nil, nil, nil, false)
        end
    end
end


function Gas2Gac.TryOpenQiXiLiuXingYu3D()
	CLuaQiXiMgr.TryOpenQiXiLiuXingYu3D()
end

function Gas2Gac.TryTrackOpenQiXiLiuXingYu3D(sceneId, sceneTemplateId, x, y)
	local pixelPos = Utility.GridPos2PixelPos(CPos(x, y))
	CTrackMgr.Inst:Track(sceneId, sceneTemplateId, pixelPos, 0, DelegateFactory.Action(function()
		CLuaQiXiMgr.TryOpenQiXiLiuXingYu3D()
	end), nil, nil, nil, false)
end

local qixiLiuXingYuFxTbl = {}
function Gas2Gac.StopQiXiLiuXingYu()
	for k, v in pairs(qixiLiuXingYuFxTbl) do
		v:Destroy()
	end
end

function Gas2Gac.StartQiXiLiuXingYu(kind)
	local pos = Vector3(118, 600, 156)
	if kind == 1 then
		if qixiLiuXingYuFxTbl[1] then qixiLiuXingYuFxTbl[1]:Destroy() end
		qixiLiuXingYuFxTbl[1] = CEffectMgr.Inst:AddWorldPositionFX(88801233, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
	elseif kind == 2 then
		if qixiLiuXingYuFxTbl[2] then qixiLiuXingYuFxTbl[2]:Destroy() end
		qixiLiuXingYuFxTbl[2] = CEffectMgr.Inst:AddWorldPositionFX(88801232, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
	elseif kind == 3 then
		if qixiLiuXingYuFxTbl[3] then qixiLiuXingYuFxTbl[3]:Destroy() end
		qixiLiuXingYuFxTbl[3] = CEffectMgr.Inst:AddWorldPositionFX(88801231, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
	end
end

function Gas2Gac.PlayerStartChengZhong(npcEngineId, playerEngineId)
	CLuaChengZhongMgr.m_NpcEngineId = npcEngineId
	CLuaChengZhongMgr.m_PlayerEngineId = playerEngineId
	CLuaChengZhongMgr.BindPlayer()
end

function Gas2Gac.PlayerEndChengZhong()
	CLuaChengZhongMgr.UnBindPlayer()
	CLuaChengZhongMgr.m_NpcEngineId = 0
	CLuaChengZhongMgr.m_PlayerEngineId = 0
end

--显示挑战任务提示
function Gas2Gac.ShowGuanNingChallengeTaskChoiceRemind()
    CLuaGuanNingWuShenMgr.s_ShowGuanNingChallengeTaskChoiceRemind=true
    g_ScriptEvent:BroadcastInLua("ShowGuanNingChallengeTaskChoiceRemind")
end
--更新挑战任务信息
function Gas2Gac.UpdateGuanNingChallengeTaskInfo(taskId,needValue,curValue,bFinish,awardScore)
    CLuaGuanNingWuShenMgr.s_ShowGuanNingChallengeTaskChoiceRemind=false
    CGuanNingMgr.s_UpdateGuanNingChallengeTaskInfo = true

    CLuaGuanNingWuShenMgr.taskId=taskId
    CLuaGuanNingWuShenMgr.needValue=needValue
    CLuaGuanNingWuShenMgr.curValue=curValue
    CLuaGuanNingWuShenMgr.bFinish=bFinish
    CLuaGuanNingWuShenMgr.awardScore=awardScore
    g_ScriptEvent:BroadcastInLua("UpdateGuanNingChallengeTaskInfo")
end

function Gas2Gac.QueryGuanNingChallengeTaskChoiceResult(info_U)
    local list=MsgPackImpl.unpack(info_U)
    if not list then return end
    local info={}
    for i=0,list.Count-1,4 do
        table.insert( info, {taskId=list[i],taskType=list[i+1],needValue=list[i+2],awardScore=list[i+3]})
    end
    local function compare( a, b )
        if a.taskType>b.taskType then
            return false
        elseif a.taskType<b.taskType then
            return true
        else
            return a.taskId>b.taskId
        end
    end
    table.sort( info, compare )

    g_ScriptEvent:BroadcastInLua("QueryGuanNingChallengeTaskChoiceResult",info)
end
--是否继承之前的挑战任务
function Gas2Gac.ShowGuanNingContinueChallengeTaskWnd(info_U)
    local list=MsgPackImpl.unpack(info_U)
    if not list then return end

    local taskId=list[0]
    local designData=GuanNing_ChallengeTask.GetData(taskId)
    if designData then
        local needValue=CLuaGuanNingWuShenMgr.FormatNum(list[1]) or 0
        local costYuanbao=tonumber(list[3]) or 0
        local msg = MessageMgr.Inst:FormatMessage("GuanNing_WuShen_Contine", {SafeStringFormat3("%s[c][ffffff]([c][ff0000]%s[-][/c]/%s)[-][/c]",designData.TaskName, 0, needValue)})
        QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YuanBao, msg, costYuanbao,
            DelegateFactory.Action(function()
                Gac2Gas.RequestContinueGuanNingChallengeTask(true)
            end),
            DelegateFactory.Action(function()
                Gac2Gas.RequestContinueGuanNingChallengeTask(false)
            end))
    end
end


--消耗支机石的数量
function Gas2Gac.RequestUpgradeEquipWordCostReturn(place, pos, equipId, itemCount)
    -- print("RequestUpgradeEquipWordCostReturn",place,pos,equipId,itemCount)
	g_ScriptEvent:BroadcastInLua("RequestUpgradeEquipWordCostReturn",equipId,itemCount)
end

--消耗支机石的数量
function Gas2Gac.UpgradeEquipWordSuccess(equipId, oldWordId, newWordId)
    -- print("UpgradeEquipWordSuccess",equipId,oldWordId,newWordId)
	g_ScriptEvent:BroadcastInLua("UpgradeEquipWordSuccess",equipId,oldWordId,newWordId)
end

--锤炼词条消耗物品的数量
function Gas2Gac.RequestChuiLianEquipWordCostReturn(place, pos, equipId, chuilianItemTemplateId, itemCount)


end

-- 锤炼词条成功:
-- equipId:装备id
-- equipTempPropUD: 装备替换成临时词条后的临时属性值
function Gas2Gac.ChuiLianEquipWordSuccess(equipId, equipTempPropUD)
    -- print("ChuiLianEquipWordSuccess",equipId, equipTempPropUD)
	g_ScriptEvent:BroadcastInLua("ChuiLianEquipWordSuccess",equipId, equipTempPropUD)

end

-- 锤炼词条失败
function Gas2Gac.ChuiLianEquipWordFail(equipId)
	-- print("ChuiLianEquipWordFail",equipId)
	g_ScriptEvent:BroadcastInLua("ChuiLianEquipWordFail",equipId)
end


function Gas2Gac.ReplyChristmasGiftBattlePlayInfo(playData, bFinish)
    LuaGrabChristmasGiftMgr.RankDataTable = {}
    LuaGrabChristmasGiftMgr.MyDataIndex = nil
    LuaGrabChristmasGiftMgr.MyData = nil
    local list = MsgPackImpl.unpack(playData)
    for i=0, list.Count-1, 5 do
        local t = {}
        t.playerId = list[i]
        t.playerName = list[i+1]
        t.choice = list[i+2]
        t.giftNum = list[i+3]
        t.cangGetAward = list[i+4]
        table.insert(LuaGrabChristmasGiftMgr.RankDataTable,t)
    end
    table.sort(LuaGrabChristmasGiftMgr.RankDataTable,
        function(a, b)
            local r
            if a.giftNum == b.giftNum then
                r = a.playerId < b.playerId
            else
                r = a.giftNum > b.giftNum
            end
            return r
        end
    )
    local rank = 1
    local lastScore = LuaGrabChristmasGiftMgr.RankDataTable[1].giftNum
    for i,data in ipairs(LuaGrabChristmasGiftMgr.RankDataTable) do
        if data.giftNum < lastScore then
            rank = rank + 1
        else
            rank = rank
        end
        lastScore = data.giftNum
        data.rank = rank
        if data.playerId and CClientMainPlayer.IsPlayerId(data.playerId) then
            LuaGrabChristmasGiftMgr.MyDataIndex = i
            LuaGrabChristmasGiftMgr.MyData = data
        end
    end

    if not CUIManager.IsLoaded("GrabChristmasGiftRankWnd") then
        LuaGrabChristmasGiftMgr.RefreshRankWndNotShow = true
        CUIManager.ShowUI("GrabChristmasGiftRankWnd")
    else
        g_ScriptEvent:BroadcastInLua("RefreshGiftBattleRankView")
    end
end
function Gas2Gac.OpenChristmasGiftBattleSignUpWnd(bSignUp, restAwardNum, choice)
    local level = CClientMainPlayer.Inst.MaxLevel
    if level < 50 then
        g_MessageMgr:ShowMessage("BAGUALU_GRADELIMIT",50)
        return
    end
    if not CUIManager.IsLoaded("GrabChristmasGiftWnd") then
        LuaGrabChristmasGiftMgr.RefreshSignWndNotShow = true
        LuaGrabChristmasGiftMgr.IsSignUp = bSignUp
        LuaGrabChristmasGiftMgr.RestAwardNum = restAwardNum
        LuaGrabChristmasGiftMgr.Choice = choice
        CUIManager.ShowUI("GrabChristmasGiftWnd")
    else
        g_ScriptEvent:BroadcastInLua("RefreshChristmasGiftBattleSignUpWnd",bSignUp, restAwardNum, choice)
    end
end

function Gas2Gac.SyncChristmasGiftBattleGiftNum(playData)
    local list = MsgPackImpl.unpack(playData)
    LuaGrabChristmasGiftMgr.playerInfoData = {}
    for i=0, list.Count-1, 3 do
        local t = {}
        t.playerId = list[i]
        t.playerName = list[i+1]
        t.giftNum = list[i+2]
        table.insert(LuaGrabChristmasGiftMgr.playerInfoData,t)
    end
    table.sort(LuaGrabChristmasGiftMgr.playerInfoData, function(a, b) return a.giftNum > b.giftNum end)

    g_ScriptEvent:BroadcastInLua("RefreshGiftBattlePlayerInfoView")
end

function Gas2Gac.ChristmasGiftBattleGetGift(num)
    local RO = CClientMainPlayer.Inst.RO
    if RO then
        RO:ShowGiftText(num)
    end
end

function Gas2Gac.RefuseJoinChristmasGiftBattle()
    CUIManager.CloseUI("GrabChristmasGiftWnd")
end


function Gas2Gac.SyncSchoolAchievementStatus(achievementId, status)

    local prop = g_LuaSchoolContributionMgr:GetSchoolContributionProp()
    if not prop then return end

    if status == 0 then
        -- 未达成
        prop.FinishedAchievement:SetBit(achievementId, false)
    elseif status == 1 then
        -- 已达成
        prop.FinishedAchievement:SetBit(achievementId, true)
    elseif status == 2 then
        -- 已领取奖励
        prop.AwardedAchievement:SetBit(achievementId, true)
    end
    g_ScriptEvent:BroadcastInLua("SyncSchoolAchievementStatus",achievementId)
end

function Gas2Gac.SyncSchoolContribution(currentContribution, historyContribution)

    local prop = g_LuaSchoolContributionMgr:GetSchoolContributionProp()
    if not prop then return end

    prop.CurrentContribution = tonumber(currentContribution)
    prop.HistoryContribution = tonumber(historyContribution)

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        CClientMainPlayer.Inst.PlayProp:SetScoreByKey(EnumPlayScoreKey.SchoolContribution, currentContribution);
		EventManager.Broadcast(EnumEventType.MainPlayerPlayPropUpdate)
    end
    g_ScriptEvent:BroadcastInLua("SyncSchoolContribution")
end

function Gas2Gac.SyncSchoolAchievementCounter(counterIndex, counterValue)

    local prop = g_LuaSchoolContributionMgr:GetSchoolContributionProp()
    if not prop then return end

    CommonDefs.DictSet(prop.AchievementCounter, typeof(UInt16), counterIndex, typeof(UInt64), counterValue)
    g_ScriptEvent:BroadcastInLua("SyncSchoolAchievementCounter")
end


function Gas2Gac.ReplyRecentHongBaoInfo(channelType, hongBao_U)
    local hongbaoList = {}
    local data = MsgPackImpl.unpack(hongBao_U)
    for i=0,data.Count-1,11 do
        local hongbao = {
            m_Status = math.floor(tonumber(data[i] or 0)),
            m_Content = tostring(data[i + 1]),
            m_OwnerName = tostring(data[i + 2]),
            m_HongbaoId = tostring(data[i + 3]),
            m_OwnerId = math.floor(tonumber(data[i + 4] or 0)),
            m_EventType = math.floor(tonumber(data[i + 5] or 0)),
            m_HongbaoAmount = math.floor(tonumber(data[i + 6] or 0)),
            m_Class = math.floor(tonumber(data[i + 7] or 0)),
            m_Gender = math.floor(tonumber(data[i + 8] or 0)),
            m_IsVoiceType = CommonDefs.Convert_ToBoolean(data[i + 9]),
            m_HongBaoCoverType = math.floor(tonumber(data[i + 10] or 0)),
        }
        hongbao.m_Content = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(hongbao.m_OwnerId, hongbao.m_Content, nil, false) or "",
        table.insert( hongbaoList,hongbao )
    end
    -- channelType : EnumHongBaoChannel = {eWorld = 1, eGuild = 2, eSystem = 3,}
    if channelType == 2 then
        CLuaHongBaoMgr.m_HongbaoDataTable[EnumHongBaoDisplayType.eGuild] = hongbaoList
    else
        CLuaHongBaoMgr.m_HongbaoDataTable[EnumHongBaoDisplayType.eWorld] = hongbaoList
    end
    g_ScriptEvent:BroadcastInLua("ReplyRecentHongBaoInfo")
end

function Gas2Gac.ReplyRecentDaoJuLiBaoInfo(libao_U)
    local hongbaoList = {}
    local data = MsgPackImpl.unpack(libao_U)
    for i=0,data.Count-1,6 do
        local hongbao = {
            m_Status = math.floor(tonumber(data[i] or 0)),
            m_Content = tostring(data[i + 1]),
            m_OwnerName = tostring(data[i + 2]),
            m_HongbaoId = tostring(data[i + 3]),
            m_OwnerId = math.floor(tonumber(data[i + 4] or 0)),
            m_ChannelType = math.floor(tonumber(data[i + 5] or 0)),
        }
        hongbao.m_Content = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(hongbao.m_OwnerId, hongbao.m_Content, nil, false) or "",
        table.insert( hongbaoList,hongbao )
    end
    CLuaHongBaoMgr.m_HongbaoDataTable[EnumHongBaoDisplayType.eGift] = hongbaoList
    g_ScriptEvent:BroadcastInLua("ReplyRecentHongBaoInfo")
end

function Gas2Gac.ReplyDaoJuLiBaoOpenState(state)
    CHongBaoMgr.Inst:SetLiBaoOpenState(state)
end


function Gas2Gac.LoginZhouBianMallShopSuccess(url)
	local engineVersion = Main.Inst.EngineVersion
	if engineVersion < 0 then
		engineVersion = uint.MaxValue
	end
	CWebBrowserMgr.Inst:OpenUrl(url .. "&version=" .. engineVersion)
end


function Gas2Gac.ConfirmOpenCookingWnd(taskId, uiPath, cookIdListUD, bRandom)
	LuaCookingPlayMgr.m_AllowShake = false
	LuaCookingPlayMgr:InitCookList(cookIdListUD)
	LuaCookingPlayMgr.m_PlayFinish = false
	LuaCookingPlayMgr.m_UIPath = uiPath
	if bRandom then
		LuaCookingPlayMgr.m_Type = EnumCookingPlayType.eRandomCooking
	else
		LuaCookingPlayMgr.m_Type = EnumCookingPlayType.eCooking
	end

	local message = MessageMgr.Inst:FormatMessage("cooking_confirm", {})
	MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
		LuaCookingPlayMgr.m_TaskId = taskId
		CUIManager.ShowUI(CLuaUIResources.CookingPlayWnd)
	end))
end

function Gas2Gac.CookingPlayAddMeterial(cookId, curProgress, totalProgress)
	LuaCookingPlayMgr.m_CurrentCookId = cookId
	LuaCookingPlayMgr.m_CurrentProgress = curProgress
	LuaCookingPlayMgr.m_TotalProgress = totalProgress
	g_ScriptEvent:BroadcastInLua("OnSyncCurrentCookId")
end

function Gas2Gac.CloseCookingWnd(bFinished)
	g_ScriptEvent:BroadcastInLua("OnCookingPlayFinished")
	if bFinished then
		MessageMgr.Inst:ShowMessage("cooking_success", {})
	else
		MessageMgr.Inst:ShowMessage("cooking_failed", {})
	end
	LuaCookingPlayMgr.m_PlayFinish = true
	RegisterTickOnce(function()
		CUIManager.CloseUI(CLuaUIResources.CookingPlayWnd)
	end, 3000)
end

function Gas2Gac.OpenYinMengXiangWnd(taskId, cookIdListUD)
	LuaCookingPlayMgr.m_AllowShake = false
	LuaCookingPlayMgr.m_Type = EnumCookingPlayType.eYinMengXiang
	LuaCookingPlayMgr:InitCookList(cookIdListUD)
	LuaCookingPlayMgr.m_TaskId = taskId
	CUIManager.ShowUI(CLuaUIResources.CookingPlayWnd)
end

function Gas2Gac.YinMengXiangAddMeterial(cookId, count, needCount)
	LuaCookingPlayMgr.m_CurrentCookId = cookId
	LuaCookingPlayMgr.m_CurrentProgress = count
	LuaCookingPlayMgr.m_TotalProgress = needCount
	g_ScriptEvent:BroadcastInLua("OnSyncCurrentCookId")
end

function Gas2Gac.OpenShakeYinMengXiangWnd(taskId)
	LuaCookingPlayMgr.m_PlayFinish = true
	g_ScriptEvent:BroadcastInLua("OnCookingPlayFinished")
	LuaCookingPlayMgr.m_AllowShake = true
	MessageMgr.Inst:ShowMessage("YINMENGXAING_YAOYIYAO_ONE", {})
end

function Gas2Gac.FinishShakeYinMengXiang()
	CUIManager.CloseUI(CLuaUIResources.CookingPlayWnd)
end
--
function Gas2Gac.PlayFakeConversation(conversationStr)
    if conversationStr==nil or conversationStr=="" then return end

    CLuaJuQingDialogMgr.ShowDialog(0,0,0,conversationStr)
end

function Gas2Gac.SendSayDialogId(engineId, specialDialogId)
    CLuaJuQingDialogMgr.Init(engineId)

	local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
	local dialog = Dialog_TaskDialog.GetData(specialDialogId)

	if not clientObject then return end
	if dialog then
		local content = dialog.Dialog
		local npc = TypeAs(clientObject, typeof(CClientNpc))
		local npcId = 0
        if npc then
            npcId = npc.TemplateId
        end
		-- CJuQingDialogMgr.Inst:ShowSerialDialog(specialDialogId, npcId, content)
		CLuaJuQingDialogMgr.ShowDialog(engineId,specialDialogId, npcId, content)
	end
end

function Gas2Gac.NewYearLuckyDrawSuccess(itemId, rewardTemplateId)
    g_ScriptEvent:BroadcastInLua("SyncNewYearPrizeDrawIndex")
  end



---------------------------
---------Gas2Gac-----------
---------------------------

function Gas2Gac.MPTZQueryPlayerTargetResult(speedupPrice, idx, avgRank1, avgRank2, avgRank3, fightScore, lastIdx, lastScore, lastYinPiao, lastExp, targetData)
	local tbl = {}
    local dataList = MsgPackImpl.unpack(targetData)
    if not dataList then return end
    for i=0, dataList.Count-1 do
        local subList = dataList[i]
        local target = {}
        target.targetId = tonumber(subList[0])
        target.rankIdx = tonumber(subList[1])
        target.name = tostring(subList[2])
        target.grade = tonumber(subList[3])
        target.profession = tonumber(subList[4])
        target.gender = tonumber(subList[5])
        target.fightScore =tonumber(subList[6])
        target.expression = tonumber(subList[7])
        target.expressionTxt = tonumber(subList[8])
        target.sticker = tonumber(subList[9])
        target.xianFanStatus = tonumber(subList[10])
        --subList[11] 是 profileInfo_U
        target.fanShenGrade = tonumber(subList[12])
        table.insert(tbl, i+1, target)
    end
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.PlayProp:SetMPTZSpeedupPrice(speedupPrice)
    end
    local avgRankTbl = {}
    table.insert(avgRankTbl, 1, avgRank1)
    table.insert(avgRankTbl, 2, avgRank2)
    table.insert(avgRankTbl, 3, avgRank3)
    LuaMPTZMgr.SetSelfAndTargetPlayerInfo(idx, fightScore, tbl, lastIdx, lastScore, lastYinPiao, lastExp, avgRankTbl)
end


function Gas2Gac.ShowWorldCupJingCaiAwardReddot()
    g_ScriptEvent:BroadcastInLua("ShowWorldCupJingCaiAwardReddot")
end

function Gas2Gac.SendWorldCupTodayJingCaiInfo(pid, totalNum, resultData, myValue, serverValue, myNum)
    g_ScriptEvent:BroadcastInLua("SendWorldCupTodayJingCaiInfo", pid, totalNum, resultData, myValue, serverValue, myNum)
end

function Gas2Gac.ReplyWorldCupTodayJingCaiRecord(data)
    g_ScriptEvent:BroadcastInLua("ReplyWorldCupTodayJingCaiRecord", data)
end

function Gas2Gac.ReplyWorldCupHomeTeamPlayInfo(tid, resultData, bAlert, groupRank, settimes, taotaiStage)
    CLuaWorldCupMgr.m_HomeTeamId = tid
    g_ScriptEvent:BroadcastInLua("ReplyWorldCupHomeTeamPlayInfo", tid, resultData, bAlert, groupRank, settimes, taotaiStage)
end

function Gas2Gac.ReplyWorldCupAgendaInfo(tid, data)
    CLuaWorldCupMgr.m_HomeTeamId = tid
    g_ScriptEvent:BroadcastInLua("ReplyWorldCupAgendaInfo", data)
end

function Gas2Gac.WuCaiShaBingShowDetail(playerGroupId, showGroupId, progressDataUD)
	local progressData = MsgPackImpl.unpack(progressDataUD)
	if not progressData then
		return
	end

	if progressData.Count ~= 8 then
		return
	end

	LuaWuCaiShaBingMgr.m_ProgressData = {}
	for i = 0, progressData.Count - 1 do
		table.insert(LuaWuCaiShaBingMgr.m_ProgressData, progressData[i])
	end

	LuaWuCaiShaBingMgr.m_ShowGroupId = showGroupId
	LuaWuCaiShaBingMgr.m_PlayerGroupId = playerGroupId
	CUIManager.ShowUI("WuCaiShaBingDetailWnd")
end

function Gas2Gac.WuCaiShaBingShowBattleInfo(playerGroupId, groupProgressDataUD)
	local groupProgressData = MsgPackImpl.unpack(groupProgressDataUD)
	if not groupProgressData then
		return
	end

	if groupProgressData.Count ~= 3 then
		return
	end

	LuaWuCaiShaBingMgr.m_GroupProgressData = {}
	for i = 0, groupProgressData.Count - 1 do
		local progressData = groupProgressData[i]
		if progressData.Count ~= 8 then
			return
		end

		local groupId = i + 1
		LuaWuCaiShaBingMgr.m_GroupProgressData[groupId] = {}
		for j = 0, progressData.Count - 1 do
			table.insert(LuaWuCaiShaBingMgr.m_GroupProgressData[groupId], progressData[j])
		end
	end

	LuaWuCaiShaBingMgr.m_PlayerGroupId = playerGroupId
	CUIManager.ShowUI("WuCaiShaBingBattleInfoWnd")
end

function Gas2Gac.WuCaiShaBingShowResult(playerGroupId, winGroupId, groupPlayerInfoUD)
	local groupPlayerInfo = MsgPackImpl.unpack(groupPlayerInfoUD)
	if not groupPlayerInfo then
		return
	end

	if groupPlayerInfo.Count ~= 3 then
		return
	end

	LuaWuCaiShaBingMgr.m_GroupPlayerInfo = {}
	for i = 0, groupPlayerInfo.Count - 1 do
		local groupId = i + 1
		LuaWuCaiShaBingMgr.m_GroupPlayerInfo[groupId] = {}
		local playerInfoTbl = groupPlayerInfo[i]
		for j = 0, playerInfoTbl.Count - 1 do
			table.insert(LuaWuCaiShaBingMgr.m_GroupPlayerInfo[groupId], playerInfoTbl[j])
		end
	end

	LuaWuCaiShaBingMgr.m_PlayerGroupId = playerGroupId
	LuaWuCaiShaBingMgr.m_WinGroupId = winGroupId
	CUIManager.ShowUI("WuCaiShaBingResultWnd")
end

function Gas2Gac.WuCaiShaBingShowState(playerGroupId, progressDataUD)
	local progressData = MsgPackImpl.unpack(progressDataUD)
	if not progressData then
		return
	end

	if progressData.Count ~= 8 then
		return
	end

	local submitData = {}
	for i = 0, progressData.Count - 1 do
		table.insert(submitData, progressData[i])
	end

	LuaWuCaiShaBingMgr.m_StateData.playerGroupId = playerGroupId
	LuaWuCaiShaBingMgr.m_StateData.submitData = submitData
	if CUIManager.IsLoaded("WuCaiShaBingStateWnd") then
		g_ScriptEvent:BroadcastInLua("WuCaiShaBingShowState", playerGroupId, submitData)
	end
end

function Gas2Gac.QueryWuCaiShaBingSignUpStatusDone(playStatus, remainTimes, forceOpen)
	LuaWuCaiShaBingMgr.m_PlayStatus = playStatus
	LuaWuCaiShaBingMgr.m_RemainTimes = remainTimes

	if CUIManager.IsLoaded("WuCaiShaBingEnterWnd") or forceOpen then
		CUIManager.ShowUI("WuCaiShaBingEnterWnd")
	end
end

function Gas2Gac.WuCaiShaBingSubmitMaterialDone(itemTemplateId)
	g_ScriptEvent:BroadcastInLua("WuCaiShaBingSubmitMaterialDone", itemTemplateId)
end

function Gas2Gac.WuCaiShaBingStealMaterialDone(itemTemplateId)
	g_ScriptEvent:BroadcastInLua("WuCaiShaBingStealMaterialDone", itemTemplateId)
end


-- 同步木马乘客信息
function Gas2Gac.SyncCarouselPassengers(mumaEngineId, dataU)
    -- print("SyncCarouselPassengers")
    CLuaXuanZhuanMuMaMgr.m_MuMaEngineId=mumaEngineId
    --先把之前的卸载掉
    local pre = {}
    for k,v in pairs(CLuaXuanZhuanMuMaMgr.m_PlayerInfos) do
        table.insert( pre,k )
    end

    CLuaXuanZhuanMuMaMgr.m_PlayerInfos={}
    local list = MsgPackImpl.unpack(dataU)
    if list then
        for i=1,list.Count,2 do
            local pos = list[i-1]
            local engineId  = list[i]
            CLuaXuanZhuanMuMaMgr.m_PlayerInfos[engineId] = pos
        end
    end
    for i,v in ipairs(pre) do
        if not CLuaXuanZhuanMuMaMgr.m_PlayerInfos[v] then
            CLuaXuanZhuanMuMaMgr.UnBindPlayer(v)
        end
    end

    --绑定player
    CLuaXuanZhuanMuMaMgr.SyncCarouselPassengers()
end

-- 开始转动旋转木马
function Gas2Gac.StartRotateCarousel(mumaEngineId)
    -- print("StartRotateCarousel")
    CLuaXuanZhuanMuMaMgr.m_MuMaEngineId=mumaEngineId
    CLuaXuanZhuanMuMaMgr.StartRotateCarousel()
end

-- 停止转动旋转木马
function Gas2Gac.StopRotateCarousel()
    -- print("StopRotateCarousel")
    CLuaXuanZhuanMuMaMgr.StopRotateCarousel()
end

-- 账号迁移检查条件,每一项都是bool值,代表是否符合条件
-- 对应RPC DetectAccountTransferCondition 的返回
function Gas2Gac.SyncAccountTransferCondition(conditionUd)
    local list = MsgPackImpl.unpack(conditionUd)
    local res = {}
    for i = 0,list.Count - 1 do
        table.insert(res,{status = list[i]})
    end
    g_ScriptEvent:BroadcastInLua("AccountTransferInfoGet", res)
end

-- 账号迁移时，查询账号的绑定手机信息
-- 对应RPC QueryAccoutBindMobleInfo 的返回
function Gas2Gac.SyncAccountBindMobile(areaCode, phoneNumber)
    LuaWelfareMgr:OpenRelatePhoneWnd(areaCode, phoneNumber, false,true, true)
end

-- 账号迁移结束弹出的提示信息
function Gas2Gac.ShowClassTransferSuccessDelayTip()
    LuaAccountTransferMgr:ShowMessageOnServerWnd()
end

function Gas2Gac.BindMobileOpenWnd(areaCode, phoneNumber, bCheckedThisMonth)
	LuaWelfareMgr:OpenRelatePhoneWnd(areaCode, phoneNumber, bCheckedThisMonth, false, false)
end

function Gas2Gac.BindMobileDisbindPhoneSuccess()
    g_ScriptEvent:BroadcastInLua("BindMobileDisbindPhoneSuccess")
end

function Gas2Gac.BindMobileVeryfiCaptchaSuccess(phoneNumber)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.BasicProp.AccountInfo.MobileBindTime = math.floor(CServerTimeMgr.Inst.timeStamp)
    end
    g_ScriptEvent:BroadcastInLua("BindMobileVeryfiCaptchaSuccess",phoneNumber)
end

function Gas2Gac.SyncShiFuScore(oldScore, newScore, oldLevel, newLevel)
end

function Gas2Gac.SendShiFuScoreLevel(score, level, maxLevel, monthRewardedLevel)
  LuaShiTuMgr.ShiLevel = level
  LuaShiTuMgr.ShiTopLevel = maxLevel
  LuaShiTuMgr.ShiRecLevel = monthRewardedLevel
  LuaShiTuMgr.ShiScore = score

  --CUIManager.ShowUI(CLuaUIResources.ShiTuLevelInfoWnd)
end

function Gas2Gac.SendShiTuShiMenHasReward(hasReward,hasReward2)
    LuaShiTuMgr:SendShiTuShiMenHasReward(hasReward,hasReward2)
end

function Gas2Gac.SendShiTuShiMenInfo(isTuDi, name, level, exp, qingyi, hasWenDaoReward, playerInfo)
    LuaShiTuMgr:SendShiTuShiMenInfo(isTuDi, name, level, exp, qingyi, hasWenDaoReward, playerInfo)
end

function Gas2Gac.SendShiTuShiMenNews(isTuDi, news)
    LuaShiTuMgr:SendShiTuShiMenNews(isTuDi, news)
end

function Gas2Gac.SendRecommendShiFu(succ, shifu)
    LuaShiTuMgr:SendRecommendShiFu(succ, shifu)
end

function Gas2Gac.SendRecommendTuDi(succ, tudi)
    LuaShiTuMgr:SendRecommendTuDi(succ, tudi)
end

function Gas2Gac.SendMyShouTuQuestionInfo(question, tagList)
    LuaShiTuMgr:SendMyShouTuQuestionInfo(question, tagList)
end

function Gas2Gac.SendMyBaiShiQuestionInfo(question, tagList)
    LuaShiTuMgr:SendMyBaiShiQuestionInfo(question, tagList)
end

function Gas2Gac.SendShiMenQiuXuePuInfo(isShifu, infoUd)
    LuaShiTuMgr:SendShiMenQiuXuePuInfo(isShifu, infoUd)
end

function Gas2Gac.SendShiMenHanZhangJiInfo(isShifu, infoUd)
    LuaShiTuMgr:SendShiMenHanZhangJiInfo(isShifu, infoUd)
end

function Gas2Gac.SendChuShiGiftInfo(tudiId, tudiName, level, class, gender, jiyuId)
    LuaShiTuMgr:SendChuShiGiftInfo(tudiId, tudiName, level, class, gender, jiyuId)
end

function Gas2Gac.PrepareSendShiTuGiftRet(isShifu, otherPlayerId, targetWinshItemId, targetWishItemCount, targetWishContent, myWinshItemId, myWishItemCount, myWishContent, remainTimes, remainJadeLimit, dynamicOnShelfItems)
    LuaShiTuMgr:PrepareSendShiTuGiftRet(isShifu, otherPlayerId, targetWinshItemId, targetWishItemCount, targetWishContent, myWinshItemId, myWishItemCount, myWishContent, remainTimes, remainJadeLimit, dynamicOnShelfItems)
end

function Gas2Gac.SendShiTuGiftSucc()
    LuaShiTuMgr:SendShiTuGiftSucc()
end

function Gas2Gac.SetShiTuGiftWishSucc(isShifu, otherPlayerId, winshItemId, wishItemCount, wishContent)
    LuaShiTuMgr:SetShiTuGiftWishSucc(isShifu, otherPlayerId, winshItemId, wishItemCount, wishContent)
end

function Gas2Gac.ShowShifuPingJiaWnd()
    LuaShiTuMgr:ShowShifuPingJiaWnd()
end

function Gas2Gac.SendFamilyTreeShiMenInfo(targetPlayerId, isBestTudi, shimenLevel, bestTudiIds)
    LuaShiTuMgr:SendFamilyTreeShiMenInfo(targetPlayerId, isBestTudi, shimenLevel, bestTudiIds)
end

function Gas2Gac.SendShifuAllTudiInfo(shifuId, requestType, allTudiInfo)
    LuaShiTuMgr:SendShifuAllTudiInfo(shifuId, requestType, allTudiInfo)
end

function Gas2Gac.ModifyChuShiJiYuSucc(tudiId, jiyuId)
    LuaShiTuMgr:ModifyChuShiJiYuSucc(tudiId, jiyuId)
end

-- 报名图标是否显示
function Gas2Gac.SyncBaoTuanZuoZhanApplyIcon(bShow)
    CLuaActivityAlert.s_DoubleOneActivityStatus = bShow
    g_ScriptEvent:BroadcastInLua("DoubleOne2019ActivityResult")
  end

  -- 查询报名结果
  function Gas2Gac.QueryBaoTuanZuoZhanApplyInfoResult(bApply, leftAwardTimes)
    LuaDoubleOne2019Mgr.BaotuanApply = bApply
    LuaDoubleOne2019Mgr.BaotuanLeftTime = leftAwardTimes
    CUIManager.ShowUI("DoubleOneBaotuanBeginWnd")
  end

  -- 报名成功
  function Gas2Gac.ApplyBaoTuanZuoZhanSuccess()
    g_ScriptEvent:BroadcastInLua("DoubleOneBaotuanAttendSuccess")
  end

  -- 取消报名成功
  function Gas2Gac.CancelApplyBaoTuanZuoZhanSuccess()
    g_ScriptEvent:BroadcastInLua("DoubleOneBaotuanCancelAttendSuccess")
  end

  -- 玩法结束胜利界面 {{name, class, gender}, {name, class, gender}}  可能是0-2个 包含玩家自己
  function Gas2Gac.ShowBaoTuanZuoZhanEndVideo(playerInfo_U,result)
    if result and result == 0 then
      local playInfo = MsgPackImpl.unpack(playerInfo_U)
      local playerInfoTable = {}
      if playInfo then
        for i = 0, playInfo.Count-1 do
          local data = playInfo[i]
          table.insert(playerInfoTable,{data[0],data[1],data[2]})
        end
        LuaDoubleOne2019Mgr.BoatuanEndInfo = playerInfoTable
      end
    else
      LuaDoubleOne2019Mgr.BoatuanEndInfo = nil
    end
    LuaDoubleOne2019Mgr.BoatuanResult = result

    CUIManager.ShowUI("DoubleOneBaotuanResultWnd")
  end

  -- 进入副本 玩法介绍界面
  function Gas2Gac.ShowBaoTuanZuoZhanInfoWnd()
    CUIManager.ShowUI("DoubleOneBaotuanTipWnd")
  end

  --------------

  function Gas2Gac.SyncSinglesDayRMBPackageStatus(bAlert, remainSeconds, packId)
    CLuaActivityAlert.s_DoubleOneBonusStatus = bAlert
    CLuaActivityAlert.s_DoubleOneBonusTimeTotalRemain = remainSeconds
    CLuaActivityAlert.s_DoubleOneBonusTimeBegin = CServerTimeMgr.Inst.timeStamp
    LuaDoubleOne2019Mgr.RMBPackageId = packId

    -- EventManager.Broadcast(EnumEventType.DoubleOne2019BonusResult)
    g_ScriptEvent:BroadcastInLua("DoubleOne2019BonusResult")
  end


  function Gas2Gac.SendSinglesDayLotteryJoinInfo(selfJoinInfo, totalJoinCount)

    LuaDoubleOne2019Mgr.LotteryTotalJoinCount = totalJoinCount

    local timeTable = {}
      local join_info = MsgPackImpl.unpack(selfJoinInfo)
    if join_info then
      for i = 0, join_info.Count-1 do
        local dayBeginTimestamp = join_info[i]
        table.insert(timeTable,dayBeginTimestamp)
      end
      LuaDoubleOne2019Mgr.LotterySelfTimeInfo = timeTable

      CUIManager.ShowUI("DoubleOneBonusLotteryWnd")
    end
  end

  function Gas2Gac.SendSinglesDayLotteryResult(dayBegin,lotteryResult,dateResult)
      local result = MsgPackImpl.unpack(lotteryResult)
    local resultTable = {}
      for i = 0, result.Count-1, 5 do
          local rank = tonumber(result[i])
          local serverName = result[i+1]
          local playerId = tonumber(result[i+2])
          local playerName = result[i+3]
          local playerClass = tonumber(result[i+4])
          table.insert(resultTable,{rank, serverName, playerId, playerName, playerClass})
      end
    LuaDoubleOne2019Mgr.LotteryRankInfo = resultTable
    LuaDoubleOne2019Mgr.LotteryRankTime = dayBegin

    local timeTable = {}
      local dResult = MsgPackImpl.unpack(dateResult)
      for i = 0, dResult.Count-1, 1 do
          local time = tonumber(dResult[i])
      table.insert(timeTable,time)
      end
    table.sort(timeTable)
    LuaDoubleOne2019Mgr.LotteryTotalRankTime = timeTable


    if CUIManager.IsLoaded("DoubleOneBonusLotteryRankWnd") then
      g_ScriptEvent:BroadcastInLua("DoubleOneBonusRankInfoUpdate")
    else
      CUIManager.ShowUI("DoubleOneBonusLotteryRankWnd")
    end
  end

  --[[
      {"RequestEnterQiLinDongPlay", "", 3000, nil, "lua" },
      {"CostJadeEnterQiLinDongPlay", "", 3000, nil, "lua"},
      {"ReplaceQiLinDongTempSkill", "II", 1000, nil, "lua"},
      {"RequestOpenQiLinDongPlayShop", "", 1000, nil, "lua"},
      {"RequestBuyQiLinDongPlayShopItem", "I", 1000, nil, "lua"},
    --]]
  -- 玩家道具信息
  function Gas2Gac.SyncQiLinDongItemCount(item_U)
    local itemList = MsgPackImpl.unpack(item_U)
    if not itemList then
      return
    end

    local itemTable = {}
    for i = 0, itemList.Count-1, 2 do
      local dataId = tonumber(itemList[i])
      local itemId = QiLinDong_Item.GetData(dataId).ShopItemId
      local itemNum = tonumber(itemList[i+1])
      table.insert(itemTable,{itemId,itemNum})
    end

    LuaDoubleOne2019Mgr.QLDItem = itemTable
    g_ScriptEvent:BroadcastInLua("DoubleOneQLDItemInfoRefresh")
  end

  -- 同步玩家血量
  function Gas2Gac.SyncQiLinDongHp(hp)
    LuaDoubleOne2019Mgr.QLDHp = hp
    g_ScriptEvent:BroadcastInLua("DoubleOneQLDHpRefresh")
  end

  -- 打开商店
  function Gas2Gac.OpenQiLinDongPlayShop(item_U, expiredTime, existItem_U, leftFreeTimes)
    local itemList = MsgPackImpl.unpack(item_U)
    if not itemList then
      return
    end

    local itemTable = {}
    for i = 0, itemList.Count-1, 2 do
      local itemId = tonumber(itemList[i])
      local itemNum = itemList[i+1]
      table.insert(itemTable,itemId)
    end

    LuaDoubleOne2019Mgr.QLDItemTable = itemTable

    local existTable = {}
    local existList = MsgPackImpl.unpack(existItem_U)
    if existList then
      for i = 0, existList.Count-1, 2 do
        local itemId = tonumber(existList[i])
        local itemNum = existList[i+1]
        existTable[itemId] = true
      end
    end

    LuaDoubleOne2019Mgr.QLDExistItemTable = existTable

    LuaDoubleOne2019Mgr.QLDItemExpiredTime = expiredTime
    LuaDoubleOne2019Mgr.QLDLeftFreeTimes = leftFreeTimes

    if CUIManager.IsLoaded("DoubleOneQLDShowWnd") then
      g_ScriptEvent:BroadcastInLua("DoubleOneQLDShowInfoRefresh")
    else
      CUIManager.ShowUI("DoubleOneQLDShowWnd")
    end
  end

  -- 消耗灵玉进入副本
  function Gas2Gac.ShowCostJadeEnterQiLinDongWnd(jadeNum)
    if jadeNum and jadeNum > 0 then
      QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.LingYu, LocalString.GetString('每日前三次进入副本免费,之后再次进入需要支付灵玉哦~'), jadeNum, DelegateFactory.Action(function ()
        Gac2Gas.CostJadeEnterQiLinDongPlay(1)
      end), nil, false, true, EnumPlayScoreKey.NONE, false)
    end
  end

  -- 技能替换界面
  function Gas2Gac.ShowTempSkillReplaceWnd(existSkill_U, skillId)
      local skillList = MsgPackImpl.unpack(existSkill_U)
    local skillTable = {}
    if skillList then
      for i = 0, skillList.Count-1, 1 do
        local skillId = tonumber(skillList[i])
        table.insert(skillTable,skillId)
      end
    end
    LuaDoubleOne2019Mgr.QLDSkillTable = skillTable

    LuaDoubleOne2019Mgr.QLDNewSkillId = skillId

    if CUIManager.IsLoaded("DoubleOneQLDChangeSkillWnd") then
      g_ScriptEvent:BroadcastInLua("DoubleOneQLDChangeSkill")
    else
      CUIManager.ShowUI("DoubleOneQLDChangeSkillWnd")
    end
  end

  -- 副本结果
  function Gas2Gac.ShowQiLinDongResult(result, layer)
    if result == 0 then
      LuaDoubleOne2019Mgr.QLDResult = true
      LuaDoubleOne2019Mgr.QLDResultLayer = layer
      CUIManager.ShowUI("DoubleOneQLDResultWnd")
    elseif result == 1 then
      LuaDoubleOne2019Mgr.QLDResult = false
      LuaDoubleOne2019Mgr.QLDResultLayer = layer + 1
      CUIManager.ShowUI("DoubleOneQLDResultWnd")
    end
  end

  -- 抱团作战当前轮次
  function Gas2Gac.SyncBaoTuanZuoZhanCurRound(curRound, totalRound)
    LuaDoubleOne2019Mgr.BaotuanCurRound = curRound
    LuaDoubleOne2019Mgr.BaotuanTotalRound = totalRound

    g_ScriptEvent:BroadcastInLua("DoubleOneBaotuanRoundRefresh")
  end

  -- boss坐标 地图显示
  function Gas2Gac.SyncQiLinDongBossState(bAlive, posX, posY)
    LuaDoubleOne2019Mgr.QLDBossAlive = bAlive
    LuaDoubleOne2019Mgr.QLDBossPosX = posX
    LuaDoubleOne2019Mgr.QLDBossPosY = posY

    g_ScriptEvent:BroadcastInLua("UpdateQLDBossState")
  end



function Gas2Gac.SendHouseCompetitionHouseList(houseListCache)
    CLuaHouseCompetitionMgr.HouseList = {}
    local info = MsgPackImpl.unpack(houseListCache)
    if info ~= nil then
        for i=1,info.Count,5 do
            local houseInfo = {
                OwnerId = tonumber(info[i-1]),
                Name = tostring(info[i + 1-1]),
                PresentCount = math.floor(tonumber(info[i + 2-1] or 0)),
                GasId = math.floor(tonumber(info[i + 3-1] or 0)),
                VoteCount = math.floor(tonumber(info[i + 4-1] or 0)),
            }
            table.insert( CLuaHouseCompetitionMgr.HouseList,houseInfo )
        end
        -- print(#CLuaHouseCompetitionMgr.HouseList)
    end
    CUIManager.ShowUI(CUIResources.CHouseCompetitionWnd)
end
function Gas2Gac.SendHouseCompetitionVictoryHouseList(houseListCache)
    CommonDefs.ListClear(CHouseCompetitionMgr.Inst.VictoryHouseList)
    local info = TypeAs(MsgPackImpl.unpack(houseListCache), typeof(MakeGenericClass(List, Object)))
    if info ~= nil then
        do
            local i = 0
            while i < info.Count do
                local ownerId = tonumber(info[i])
                local ownerName = tostring(info[i + 1])
                local Class = math.floor(tonumber(info[i + 2] or 0))
                local Gender = math.floor(tonumber(info[i + 3] or 0))
                local expression = math.floor(tonumber(info[i + 4] or 0))
                local rank = math.floor(tonumber(info[i + 5] or 0))

                local houseInfo = CreateFromClass(CHouseCompetitionVictoryHouseInfo)
                houseInfo.OwnerId = ownerId
                houseInfo.OwnerName = ownerName
                houseInfo.Class = CommonDefs.ConvertIntToEnum(typeof(EnumClass), Class)
                houseInfo.Gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), Gender)
                houseInfo.Expression = expression
                houseInfo.Rank = rank

                local idx = - 1
                do
                    local j = 0
                    while j < CHouseCompetitionMgr.Inst.VictoryHouseList.Count do
                        if rank < CHouseCompetitionMgr.Inst.VictoryHouseList[j].Rank then
                            idx = j
                            break
                        end
                        j = j + 1
                    end
                end
                if idx == - 1 then
                    CommonDefs.ListAdd(CHouseCompetitionMgr.Inst.VictoryHouseList, typeof(CHouseCompetitionVictoryHouseInfo), houseInfo)
                else
                    CommonDefs.ListInsert(CHouseCompetitionMgr.Inst.VictoryHouseList, idx, typeof(CHouseCompetitionVictoryHouseInfo), houseInfo)
                end
                i = i + 6
            end
        end
    end

    CUIManager.ShowUI(CUIResources.CHouseCompetitionVictoryWnd)
end
function Gas2Gac.SendHouseCompetitionData(stage, uploadTimes, bRegistered)
    CHouseCompetitionMgr.Inst:SetCurrentStatge(CommonDefs.ConvertIntToEnum(typeof(EnumHouseCompetitionStage), stage))
    CHouseCompetitionMgr.Inst:SetUploadTimes(math.max(0, uploadTimes))
    CHouseCompetitionMgr.Inst.RegisteredChusai = bRegistered

    CLuaHouseCompetitionMgr.CheckMirrorServer()

    g_ScriptEvent:BroadcastInLua("OnSendHouseCompetitionData")
end

function Gas2Gac.SyncHouseCompetitionFinalStatus(bStatus,url)
    g_ScriptEvent:BroadcastInLua("OnSyncHouseCompetitionFinalStatus",bStatus,url)
end

function Gas2Gac.HouseCompetitionTrySelectCardResult(idx, result, addType, templateId)

    g_ScriptEvent:BroadcastInLua("HouseCompetitionTrySelectCardResult", idx, result, addType, templateId)
end

function Gas2Gac.HouseCompetitionTryExtraCardRewardResult(times, result, addType, templateId)

    g_ScriptEvent:BroadcastInLua("HouseCompetitionTryExtraCardRewardResult", times, result, addType, templateId)
end

function Gas2Gac.ZuoQiPlayExtraAnimation(engineId, aniName)

  local obj = CClientObjectMgr.Inst:GetObject(engineId)
  if obj and obj.VehicleRO then
    obj.VehicleRO:DoAni(aniName)
  end

end

function Gas2Gac.SendPengPengCheSignUpInfo(isSignUp, isClientRequest, todayRemainRewardTimes)
		LuaChildrenDay2019Mgr.PengPengCheShowInfo = {isSignUp = isSignUp,isClientRequest= isClientRequest,todayRemainRewardTimes=todayRemainRewardTimes}

		if CUIManager.IsLoaded(CLuaUIResources.ChildrenDayPlayShowWnd) then
			g_ScriptEvent:BroadcastInLua('UpdateChildrenDayPlayShowInfo')
		else
			CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayShowWnd)
		end
end

function Gas2Gac.SyncPengPengCheBattleInfo(dataUD)
		LuaChildrenDay2019Mgr.PengPengCheFightInfo = {}
    local list = MsgPackImpl.unpack(dataUD)
    if list.Count < 5 then return end
    for i = 0, list.Count-1, 5 do
        local rank = tonumber(list[i])
        local playerId = tonumber(list[i+1])
        local force = tonumber(list[i+2])
        local score = tonumber(list[i+3])
        local playerName = list[i+4]
				table.insert(LuaChildrenDay2019Mgr.PengPengCheFightInfo,{rank = rank,playerId=playerId,score=score,force=force,playerName=playerName})
    end

		CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayFightInfoWnd)
end

function Gas2Gac.SyncPengPengCheBattleScore(force1Score, force2Score, selfForce)
	LuaChildrenDay2019Mgr.PengPengCheForceScoreInfo = {force1 = force1Score,force2= force2Score,selfForce = selfForce}

  g_ScriptEvent:BroadcastInLua('UpdateChildrenDayPlayForceScoreInfo')
end

function Gas2Gac.ShowPengPengCheResultWnd(result, playScore, addedLiuYiScore)
  -- result: 1 win 2 lose 3 dogfall
	LuaChildrenDay2019Mgr.PengPengCheResultInfo = {result = result, playScore = playScore,addedLiuYiScore=addedLiuYiScore}
	CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayResultWnd)
end

function Gas2Gac.UpdatePengPengCheSkillItemSlots(data, operation, pos, skillId, engineId, x, y)
	LuaChildrenDay2019Mgr.PengPengCheSkillInfo = {}

	LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_list = {}

	local ids = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
	do
		local i = 0
		while i < ids.Count do
			local id = math.floor(tonumber(ids[i] or 0))
			table.insert(LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_list,id)
			i = i + 1
		end
	end

	LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_operation = operation
	LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_pos = pos
	LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_itemId = skillId
	LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_engineId = engineId
	LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_x = x/64
	LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_y = y/64

  g_ScriptEvent:BroadcastInLua('UpdateChildrenDayPlaySkillInfo')
end

function Gas2Gac.SyncObjectYScale(engineId, yScale)
		local obj = CClientObjectMgr.Inst:GetObject(engineId)
		if obj then
			local sc = obj.Ro.transform.Scale
			obj.RO.transform.localScale = Vector3(sc,yScale,sc)
		end
end

function Gas2Gac.OnPlayerAddPengPengCheScore(score)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst:ShowScoreText(score)
	end
end


function Gas2Gac.SyncNpcHaoGanDuInfo(npcId, haogandu, buyNum, zengsongNum, buyTime, zengsongTime, reason, letterFlagData, miscFlagData)
    -- print(npcId, haogandu, buyNum, zengsongNum, buyTime, zengsongTime, reason, letterFlagData, miscFlagData)
    local upgrade = false --好感度升级
    local addHaoGanDu = false --增加好感度
    local newVoice = false
    local newStory =false

    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    if playProp then
        local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,npcId)
        local oldHaoGanDu = 0
        if not ret then
            data = CNpcHaoGanDuObject()
        else
            oldHaoGanDu = data.HaoGanDu
        end
        local oldLv = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(oldHaoGanDu)
        local newLv = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(haogandu)

        if newLv>oldLv then
            upgrade=true
        end
        if upgrade then
            local oldVoiceCount = 0
            local newVoiceCount = 0
            NpcHaoGanDu_Voice.Foreach(function(k,v)
                if v.NpcId == npcId then
                    if v.Grade>oldLv then
                        oldVoiceCount=oldVoiceCount+1
                    end
                    if v.Grade>newLv then
                        newVoiceCount=newVoiceCount+1
                    end
                end
            end)
            newVoice = oldHaoGanDu~=newVoiceCount

            local oldStoryCount = 0
            local newStoryCount = 0

            NpcHaoGanDu_Xiaozhuan.Foreach(function(k,v)
                if v.NpcId == npcId then
                    if v.HaoGanDu > oldHaoGanDu then
                        oldStoryCount = oldStoryCount+1
                    end
                    if v.HaoGanDu > haogandu then
                        newStoryCount = newStoryCount+1
                    end
                end
            end)
            newStory = oldStoryCount~=newStoryCount
        end

        if oldHaoGanDu<haogandu then
            addHaoGanDu =true
        end

        data.HaoGanDu = haogandu
        data.BuyNum = buyNum
        data.ZengSongNum = zengsongNum
        data.BuyTime = buyTime
        data.ZengSongTime = zengsongTime

        local function getbitset(data)
            local list = MsgPackImpl.unpack(data)
            local bitset = BITSET(32)
            local list2 = {}
            CommonDefs.ListIterate(list,DelegateFactory.Action_object(function(obj)
                table.insert( list2,tonumber(obj) )
            end))
            for i=1,#list2,2 do
                bitset:SetBit(list2[i],tonumber(list2[i+1])>0)
            end
            return bitset
        end

        data.LetterFlag = getbitset(letterFlagData)
        data.MiscFlag = getbitset(miscFlagData)

        if not data.SpecialGiftData then
            data.SpecialGiftData = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Byte))
        end

        CLuaNpcHaoGanDuMgr.ModifyNpcData(data)
        CommonDefs.DictSet_LuaCall(playProp.NpcHaoGanDuData,npcId,data)

        -- local r = bit.band(v, bit.lshift(1, groupIdx - 1))
    end

    g_ScriptEvent:BroadcastInLua("SyncNpcHaoGanDuInfo",npcId,upgrade,reason,addHaoGanDu,newVoice,newStory)

end

function Gas2Gac.SyncNpcHaoGanDuSpecialGiftInfo(npcId, generatedTime, info)
    local oldCount,newCount = 0,0

    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    if playProp then
        local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,npcId)
        if not ret then--
            return
        end

        CommonDefs.DictIterate(data.SpecialGiftData,DelegateFactory.Action_object_object(function (k, v)
            oldCount = oldCount + (v>0 and 1 or 0)
        end))

        data.GeneratedTime = generatedTime
        CommonDefs.DictClear(data.SpecialGiftData)
        local list=MsgPackImpl.unpack(info)
        if list then
            for i=1,list.Count,2 do
                local itemId = list[i-1]
                local sendFlag = list[i]
                newCount = newCount + (sendFlag>0 and 1 or 0)
                CommonDefs.DictSet_LuaCall(data.SpecialGiftData,itemId,sendFlag)
            end
        end
        CommonDefs.DictSet_LuaCall(playProp.NpcHaoGanDuData,npcId,data)
    end

    g_ScriptEvent:BroadcastInLua("SyncNpcHaoGanDuSpecialGiftInfo",npcId,newCount>oldCount)
end


function Gas2Gac.CrossDouDiZhuQueryHaiXuanRankResult(rankU, silverPool, selfRankInfo)
	local t = {}
	local list = MsgPackImpl.unpack(rankU)
	for i=1,list.Count do
		local item = list[i-1]
		if item.Count>=9 then
			table.insert( t,{
				playerId = item[0],
				HaiXuanScore = item[1],
				HaiXuanUsedTime = item[2],
				FirstSignUpTime = item[3],
				Name = item[4],
				Class = item[5],
				Gender = item[6],
				Level = item[7],
				ServerName = item[8],
			} )
		end
	end
	local list = MsgPackImpl.unpack(selfRankInfo)
	g_ScriptEvent:BroadcastInLua("CrossDouDiZhuQueryHaiXuanRankResult",t,silverPool,{
		playerId = list[0],
		HaiXuanScore = list[1],
		HaiXuanUsedTime = list[2],
		FirstSignUpTime = list[3],
		Name = list[4],
		Class = list[5],
		Gender = list[6],
		Level = list[7],
		ServerName = list[8],
	})
end

function Gas2Gac.CrossDouDiZhuQueryJueSaiVoteListResult(voteNum, maxNum, voteU)
	local t = {}
	local list = MsgPackImpl.unpack(voteU)
	for i=1,list.Count,8 do
		table.insert( t,{
			joinId = list[i-1],
			Name = list[i],
			Class = list[i+1],
			Gender = list[i+2],
			Grade = list[i+3],
			ServerName = list[i+4],
			HaiXuanScore = list[i+5],
			IsVoted = list[i+6]>0 and true or false,
		} )
	end

	local myServerName = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerName() or ""
	table.sort( t,function(a,b)
		if a.IsVoted and not b.IsVoted then
			return true
		elseif not a.IsVoted and b.IsVoted then
			return false
		elseif a.ServerName==myServerName and b.ServerName~=myServerName then
			return true
		elseif a.ServerName~=myServerName and b.ServerName==myServerName then
			return false
		elseif a.HaiXuanScore>b.HaiXuanScore then
			return true
		elseif a.HaiXuanScore<b.HaiXuanScore then
			return false
		else
			return a.joinId<b.joinId
		end
	end )
	g_ScriptEvent:BroadcastInLua("CrossDouDiZhuQueryJueSaiVoteListResult",voteNum,maxNum,t)
end

function Gas2Gac.CrossDouDiZhuRequestShowPlayInfoResult(rankU, silverPool, bGM)
	local t = {}
	local list = MsgPackImpl.unpack(rankU)
	for i=0,list.Count-1,11 do
			table.insert( t,{
				playerId = list[i+0],
				Name = list[i+1],
				Class = list[i+2],
				Gender = list[i+3],
				Grade = list[i+4],
				ServerName = list[i+5],
				Score = list[i+6],
				UsedTime = list[i+7],
				FirstSignUpTime = list[i+8],
				WashOut = list[i+9],
				Escape = list[i+10],
			} )
	end
	g_ScriptEvent:BroadcastInLua("CrossDouDiZhuRequestShowPlayInfoResult",t,silverPool,bGM)
end

function Gas2Gac.CrossDouDiZhuOpenSignUpWndResult(infoU)
	local list = MsgPackImpl.unpack(infoU)
	local silver, bSignUp, rank, score, freeJoinCount =  list[0], list[1], list[2], list[3], list[4]
	-- CLuaCrossDouDiZhuMgr.OpenCrossDouDiZhuSignUpWnd(silver, bSignUp, rank, score)
	g_ScriptEvent:BroadcastInLua("CrossDouDiZhuOpenSignUpWndResult",silver, bSignUp, rank, score, freeJoinCount)

end

function Gas2Gac.CrossDouDiZhuWashOutNotify(bWashOut, rank, score)
	CLuaCrossDouDiZhuResultWnd.m_IsWashOut = bWashOut
	CLuaCrossDouDiZhuResultWnd.m_Rank = rank
	CLuaCrossDouDiZhuResultWnd.m_Score = score
	CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuResultWnd)
end

function Gas2Gac.CrossDouDiZhuJieSuanNotify(type, rank, score)
	-- type 1 海选 2 决赛
	-- 注意海选只处理rank<=9的，决赛只处理rank<=3的。
	if type==1 then
		CLuaCrossDouDiZhuRankResultWnd.m_Rank = rank
		CLuaCrossDouDiZhuRankResultWnd.m_Score = score
		CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuRankResultWnd)
	else
		CLuaCrossDouDiZhuShareWnd.m_Rank = rank
		CLuaCrossDouDiZhuShareWnd.m_Score = score
		CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuShareWnd)
	end
end


function Gas2Gac.RequestDouHunPraiseServerListResult(data)
	local list = MsgPackImpl.unpack(data)
	if not list then return end

	CLuaFightingSpiritMgr.m_FavorTeamList = {}
	for i = 0, list.Count - 1 do
        local l = list[i]

		local serverId = l[0]
		local serverName = l[1]
		local groupId = l[2]
		local groupName = l[3]
		local count = l[4]
        local leaderName = l[5]
		local praised = l[6]
		local isTeamMember = l[7]
		
		table.insert(CLuaFightingSpiritMgr.m_FavorTeamList, {
			serverId = serverId or 0,
            serverName = serverName or "",
            groupId = groupId or "",
			groupName =  groupName,
			favorCount = count,
			bFavor = praised and praised > 0 or false,
			bIsTeamMember = isTeamMember,
            leaderName = leaderName,
		})
	end

	if CUIManager.IsLoaded(CLuaUIResources.FightingSpiritFavorWnd) then
		g_ScriptEvent:BroadcastInLua("RequestDouHunPraiseServerListResult")
	else
		CUIManager.ShowUI(CLuaUIResources.FightingSpiritFavorWnd)
	end
end

function Gas2Gac.RequestDouHunPraiseServerInfoResult(groupId, data)
	local list = MsgPackImpl.unpack(data)
	if not list then return end

	CLuaFightingSpiritMgr.m_FavorInfoList = {}
    print("RequestDouHunPraiseServerInfoResult", list.Count)
	for i = 0, list.Count - 1 do
        local l = list[i]

		local playerId = l[0]
		local name = l[1]
		local clazz = l[2]
		local gender = l[3]
		local favorCount = l[4]
		local isLeader = l[5]
		local praised = l[6]

		local info = {
			playerId = playerId or 0,
			name = name or "",
			clazz = clazz or 0,
			gender = gender or 0,
			favorCount = favorCount or 0,
			bFavor = praised and praised > 0 or false,
			bIsLeader = isLeader and isLeader > 0 or false,
		}
		if info.bIsLeader then
			table.insert(CLuaFightingSpiritMgr.m_FavorInfoList, 1, info)
		else
			table.insert(CLuaFightingSpiritMgr.m_FavorInfoList, info)
		end
	end

	g_ScriptEvent:BroadcastInLua("RequestDouHunPraiseServerInfoResult", groupId)
end

function Gas2Gac.RequestDouHunPraiseRankResult(dataU)
	local dataList = g_MessagePack.unpack(dataU)
	if not dataList then return end

    for k, v in pairs(dataList) do
        print(k,v)
        if type(v) == "table" then
            for kk, vv in pairs(v) do
                print(kk, vv)
            end
        end
    end
	CLuaFightingSpiritMgr.m_FavorRankList = {}

    for _, data in ipairs(dataList) do
        local playerId = data[1]
		local name = data[2]
		local clazz = data[3]
		local gender = data[4]
		local serverId = data[5]
		local groupName = data[6]
		local favorCount = data[7]
		local praised = data[8]
		local rank = #CLuaFightingSpiritMgr.m_FavorRankList + 1
		table.insert(CLuaFightingSpiritMgr.m_FavorRankList, {
			playerId = playerId or 0,
			name = name or "",
			clazz = clazz or 0,
			gender = gender or 0,
			serverId = serverId or 0,
			groupName = groupName or "",
			favorCount = favorCount or 0,
			bFavor = praised and praised > 0 or false,
			rank = rank,
		})
    end

	g_ScriptEvent:BroadcastInLua("FightingSpiritFavorRankResult")
end

function Gas2Gac.RequestDouHunPraisePlayerResult(bSuccess, targetPlayerId, targetServerId, remainTimes)
	if bSuccess then
		for i = 1, #CLuaFightingSpiritMgr.m_FavorTeamList do
			local teamInfo = CLuaFightingSpiritMgr.m_FavorTeamList[i]
			if teamInfo.serverId == targetServerId then
				teamInfo.bFavor = true
				teamInfo.favorCount = teamInfo.favorCount + 1
				break
			end
		end
		for i = 1, #CLuaFightingSpiritMgr.m_FavorInfoList do
			local info = CLuaFightingSpiritMgr.m_FavorInfoList[i]
			if info.playerId == targetPlayerId then
				info.bFavor = true
				info.favorCount = info.favorCount + 1
				break
			end
		end
		for i = 1, #CLuaFightingSpiritMgr.m_FavorRankList do
			local info = CLuaFightingSpiritMgr.m_FavorRankList[i]
			if info.playerId == targetPlayerId then
				info.bFavor = true
				info.favorCount = info.favorCount + 1
				break
			end
		end
		g_ScriptEvent:BroadcastInLua("RequestDouHunPraisePlayerSuccess", targetPlayerId, targetServerId)
	end
end

-- 查询指定战队信息
function Gas2Gac.QueryDouHunChlgTeamInfoByIdResult(teamInfo_U)
	local list = MsgPackImpl.unpack(teamInfo_U)
	local teamInfo = CFightingSpiritMgr.Instance:UnpackTeamInfo(list)
	CLuaFightingSpiritMgr.OpenOtherTeam(teamInfo)
end

function Gas2Gac.QueryMergeBattleOverviewResult(beginTime, endTime, bStart, bEnd, winner, serverName1, serverName2, data1, data2, totalScore, force)
	CLuaMergeBattleMgr.m_beginTime=beginTime
	CLuaMergeBattleMgr.m_serverName1=serverName1
	CLuaMergeBattleMgr.m_serverName2=serverName2
	CLuaMergeBattleMgr.m_bStart=bStart
	local list1 = MsgPackImpl.unpack(data1)
	local list2 = MsgPackImpl.unpack(data2)
	local CUIManager = import "L10.UI.CUIManager"
	if list2 and list1 and beginTime and endTime then
		if not CUIManager.IsLoaded(CLuaUIResources.MergeBattleWnd) then
			CUIManager.ShowUI(CLuaUIResources.MergeBattleWnd)
		else
		g_ScriptEvent:BroadcastInLua("QueryMergeBattleOverviewResult", beginTime, endTime, bStart, bEnd, winner, serverName1, serverName2, list1, list2, totalScore, force)
		end
	end
end


function Gas2Gac.QueryMergeBattleInviteHuiLiuInfoResult(bindCode, count, data)
	local list = MsgPackImpl.unpack(data)
	if list then
		g_ScriptEvent:BroadcastInLua("QueryMergeBattleInviteHuiLiuInfoResult", bindCode,count,list)

	end
end

function Gas2Gac.QueryMergeBattleBindCodePlayerNameResult(bindPlayerId, bindPlayerName)
	g_ScriptEvent:BroadcastInLua("QueryMergeBattleBindCodePlayerNameResult", bindPlayerId,bindPlayerName)
end

function Gas2Gac.QueryMergeBattleHuiLiuBindStatusResult(canBind, hasBind, bindPlayerId, bindPlayerName)
	g_ScriptEvent:BroadcastInLua("QueryMergeBattleHuiLiuBindStatusResult", canBind,hasBind,bindPlayerId, bindPlayerName)
end

function Gas2Gac.MergeBattleHuiLiuPlayerBindSuccess(bindPlayerId, bindPlayerName)
	g_ScriptEvent:BroadcastInLua("MergeBattleHuiLiuPlayerBindSuccess", bindPlayerId,bindPlayerName)
end

function Gas2Gac.QueryMergeBattlePersonalEventsResult(finishedEvents)
	local finishedList = MsgPackImpl.unpack(finishedEvents)
	if finishedList then
		g_ScriptEvent:BroadcastInLua("QueryMergeBattlePersonalEventsResult", finishedList)
	end
end

function Gas2Gac.QueryMergeBattlePersonalScoresResult(groupScores)

	local groupScoreList = MsgPackImpl.unpack(groupScores)
	if groupScoreList then
		g_ScriptEvent:BroadcastInLua("QueryMergeBattlePersonalScoresResult", groupScoreList)
	end
end

--查询赛况返回信息
function Gas2Gac.QueryGnjcPlayInfoResult( DefScore, defOccupyScore, DefInfo, AttDef, attOccupyScore, attInfo, bEnd, leftDoubleTimes)
    CLuaGuanNingMgr.m_IsGameEnd=bEnd
    CLuaGuanNingMgr.m_HuxiaoPlayInfoList={}
    CLuaGuanNingMgr.m_LongyinPlayInfoList={}
    CLuaGuanNingMgr.m_HuxiaoScore=DefScore
    CLuaGuanNingMgr.m_LongyinScore=AttDef
    CLuaGuanNingMgr.m_OccupyScores = {defOccupyScore,attOccupyScore}
    CLuaGuanNingMgr.m_LeftDoubleTimes=leftDoubleTimes

    local list1=MsgPackImpl.unpack(DefInfo)
    local list2=MsgPackImpl.unpack(attInfo)

    for i=1,list1.Count,5 do
        local playInfo={
            playerId = tonumber(list1[i-1]),
            name = tostring(list1[i]),
            cls = tonumber(list1[i+1]),
            fightScore = tonumber(list1[i+2]),
            occupyScore = tonumber(list1[i+3]),
        }
        playInfo.score=playInfo.fightScore+playInfo.occupyScore
        table.insert( CLuaGuanNingMgr.m_HuxiaoPlayInfoList,playInfo )
    end
    for i=1,list2.Count,5 do
        local playInfo={
            playerId = tonumber(list2[i-1]),
            name = tostring(list2[i]),
            cls = tonumber(list2[i+1]),
            fightScore = tonumber(list2[i+2]),
            occupyScore = tonumber(list2[i+3]),
        }
        playInfo.score=playInfo.fightScore+playInfo.occupyScore
        table.insert( CLuaGuanNingMgr.m_LongyinPlayInfoList,playInfo )
    end

    local sortFunc=function(a,b)
        if a.fightScore + a.occupyScore>b.fightScore+b.occupyScore then
            return true
        elseif a.fightScore + a.occupyScore<b.fightScore+b.occupyScore then
            return false
        elseif a.fightScore>b.fightScore then
            return true
        elseif a.fightScore<b.fightScore then
            return false
        else
            return a.playerId<b.playerId
        end
    end
    table.sort( CLuaGuanNingMgr.m_HuxiaoPlayInfoList,sortFunc)
    table.sort( CLuaGuanNingMgr.m_LongyinPlayInfoList,sortFunc)

    --QueryGnjcPlayInfo
    g_ScriptEvent:BroadcastInLua("QueryGnjcPlayInfo")
end

--打完时 推送玩家奖励信息
function Gas2Gac.UpdatePlayerGnjcScore( score, expNum, yinPiao)
    -- score, uint expNum, uint yinPiao
    CLuaGuanNingMgr.m_Score=score
    g_ScriptEvent:BroadcastInLua("UpdatePlayerGnjcScore",score, expNum, yinPiao )
end

--领取双倍成功
function Gas2Gac.GetGuanNingDoubleScoreSuccess(leftDoubleTimes)
    --GetGuanNingDoubleScoreSuccess
    CLuaGuanNingMgr.m_LeftDoubleTimes=leftDoubleTimes
    g_ScriptEvent:BroadcastInLua("GetGuanNingDoubleScoreSuccess",leftDoubleTimes)
end

--玩家最高信息
function Gas2Gac.SendGuanNingMaxDataInfo(maxDataInfo_U)
    local dic=MsgPackImpl.unpack(maxDataInfo_U)

    CLuaGuanNingMgr.m_ScoreRecords={}

    local function processOnRecord(d,key)
        local set=CommonDefs.DictGetValue_LuaCall(d,tostring(key))
        if set then
            local value=CommonDefs.DictGetValue_LuaCall(set,"value")
            if value and tonumber(value)>0 then
                local playerIdTbl=CommonDefs.DictGetValue_LuaCall(set,"playerIdTbl")
                if playerIdTbl then
                    CommonDefs.DictIterate(playerIdTbl,DelegateFactory.Action_object_object(function (___key, ___value)
                        local playerId=tonumber(___key)
                        if not CLuaGuanNingMgr.m_ScoreRecords[playerId] then
                            CLuaGuanNingMgr.m_ScoreRecords[playerId]={}
                        end
                        -- print("maxdata",playerId,key)
                        table.insert( CLuaGuanNingMgr.m_ScoreRecords[playerId], key )
                    end))
                end
            end
        end
    end

    local dic1 = CommonDefs.DictGetValue_LuaCall(dic,"0")
    if dic1 then
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxSubmitFlagNumInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxOccupyScoreInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxCtrlNumInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxBaoShiNumInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxDpsInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxHealInForce)
        processOnRecord(dic1,EnumGuanNingWeekDataKey.eMaxUnderDamageInForce)
    end
    local dic2 = CommonDefs.DictGetValue_LuaCall(dic,"1")
    if dic2 then
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxSubmitFlagNumInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxOccupyScoreInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxCtrlNumInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxBaoShiNumInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxDpsInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxHealInForce)
        processOnRecord(dic2,EnumGuanNingWeekDataKey.eMaxUnderDamageInForce)
    end

    -- print(" CLuaGuanNingMgr.m_ScoreRecords",table.getn( CLuaGuanNingMgr.m_ScoreRecords))
    --SendGuanNingMaxDataInfo
    -- g_ScriptEvent:BroadcastInLua("SendGuanNingMaxDataInfo")
end

function Gas2Gac.ShowGnjcResultWnd()
    -- print("ShowGnjcResultWnd")
    CLuaGuanNingMgr.m_IsGameEnd=true
    CUIManager.CloseUI(CUIResources.GuanNingResultWnd)
    CUIManager.ShowUI(CUIResources.GuanNingResultWnd)
end


function Gas2Gac.QueryGuanNingRankDataResult(data_U)
    local datas=MsgPackImpl.unpack(data_U)
    if not datas then return end
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local rank={}
    for i=1,datas.Count do
        local data = datas[i-1]
        if data and data.Count>9 then
            local playerId = tonumber(data[0])
            local name=""
            if myId==playerId then
                name = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
            else
                local info=CIMMgr.Inst:GetBasicInfo(playerId)
                if info then name = info.Name end
            end

            local rankData={
                playerId = playerId,
                name = name,
                clazz = tonumber(data[1]),
                gender = tonumber(data[2]),
                totalScore = tonumber(data[3]),
                joinNum = tonumber(data[4]),
                victoryNum = tonumber(data[5]),
                totalKill = tonumber(data[6]),
                totalHelp = tonumber(data[7]),
                totalRelive = tonumber(data[8]),
                maxLianZhan = tonumber(data[9]),
            }
            table.insert( rank,rankData )
        end
    end
    if #rank>0 then
        g_ScriptEvent:BroadcastInLua("QueryGuanNingRankDataResult",rank)
    end
end

function Gas2Gac.UpdateGuanNingPlayerPersonalData(playId, scoreInfo_U, achieveInfo_U)
    CLuaGuanNingMgr.lastBattleData = nil

    local scoreInfo = MsgPackImpl.unpack(scoreInfo_U)
    local achieveInfo = MsgPackImpl.unpack(achieveInfo_U)

    if scoreInfo and scoreInfo.Count>15 then
        local battleData={
            timeStamp = CServerTimeMgr.Inst.timeStamp,
            result = tonumber(scoreInfo[0]),
            force = tonumber(scoreInfo[1]),
            score = tonumber(scoreInfo[2]),
            lastMaxPlayScore = tonumber(scoreInfo[3]),
            rank = tonumber(scoreInfo[4]),
            isMaxScore = tonumber(scoreInfo[5]),

            killNum = tonumber(scoreInfo[6]),
            lastMaxPlayKillNum = tonumber(scoreInfo[7]),
            dieNum = tonumber(scoreInfo[8]),
            lastMaxPlayDieNum = tonumber(scoreInfo[9]),
            helpNum = tonumber(scoreInfo[10]),
            lastMaxPlayHelpNum = tonumber(scoreInfo[11]),
            reliveNum = tonumber(scoreInfo[12]),
            lastMaxPlayReliveNum = tonumber(scoreInfo[13]),
            maxLianSha = tonumber(scoreInfo[14]),
            isMaxScoreInForce = tonumber(scoreInfo[15]),
            achieveInfo = {}
        }
        CLuaGuanNingMgr:LoadBreakRecords(battleData)
        if achieveInfo then
            for i=1,achieveInfo.Count do
                table.insert( battleData.achieveInfo,tonumber(achieveInfo[i-1]) )
            end
        end
        CLuaGuanNingMgr.lastBattleData = battleData
    end
end

function Gas2Gac.QueryGuanNingHistoryWeekDataResult(monthMax_U,monthTotal_U)
    local monthMax = MsgPackImpl.unpack(monthMax_U)
    local monthTotal = MsgPackImpl.unpack(monthTotal_U)

    local historyMonthData = {}

    if monthMax and monthMax.Count>9 then
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayScore] = tonumber(monthMax[0])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayLianShaNum] = tonumber(monthMax[1])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayKillNum] = tonumber(monthMax[2])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayDieNum] = tonumber(monthMax[3])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayHelpNum] = tonumber(monthMax[4])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayReliveNum] = tonumber(monthMax[5])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayOccupyHighLandNum] = tonumber(monthMax[6])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayOccupyForestNum] = tonumber(monthMax[7])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlayOccupyCaveNum] = tonumber(monthMax[8])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxPlaySubmitFlagNum] = tonumber(monthMax[9])
        historyMonthData[EnumGuanNingWeekDataKey.eMaxGetDianZanNum] = tonumber(monthMax[10])
    end
    CLuaGuanNingMgr:LoadBreakRecordsWeekData(historyMonthData)
    if monthTotal and monthTotal.Count>11 then
        historyMonthData[EnumGuanNingWeekDataKey.eJoinNum] = tonumber(monthTotal[0])
        historyMonthData[EnumGuanNingWeekDataKey.eVictoryNum] = tonumber(monthTotal[1])
        historyMonthData[EnumGuanNingWeekDataKey.eLianShengNum] = tonumber(monthTotal[2])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalKillNum] = tonumber(monthTotal[3])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalDieNum] = tonumber(monthTotal[4])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalHelpNum] = tonumber(monthTotal[5])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalReliveNum] = tonumber(monthTotal[6])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalOccupyHighLandNum] = tonumber(monthTotal[7])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalOccupyForestNum] = tonumber(monthTotal[8])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalOccupyCaveNum] = tonumber(monthTotal[9])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalSubmitFlagNum] = tonumber(monthTotal[10])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalScore] = tonumber(monthTotal[11])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalSendDianZanNum] = tonumber(monthTotal[12])
        historyMonthData[EnumGuanNingWeekDataKey.eTotalGetDianZanNum] = tonumber(monthTotal[13])
    end
    CLuaGuanNingMgr.historyMonthData = historyMonthData
    g_ScriptEvent:BroadcastInLua("QueryGuanNingHistoryWeekDataResult",historyMonthData)
end

function Gas2Gac.UpdateOccupyHpInfo( occupyNpcInfo )
    local datas = MsgPackImpl.unpack(occupyNpcInfo)
    if not datas then return end

    local templateIdList={}
    local hpList={}

    local lookup={}
    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if TypeIs(obj, typeof(CClientNpc)) then
            lookup[obj.TemplateId]=obj
        end
    end))

    for i=1,datas.Count,2 do
        local templateId = tonumber(datas[i-1])
        local hp = tonumber(datas[i])
        table.insert( templateIdList,templateId )
        table.insert( hpList,hp )
        if lookup[templateId] then
            local clientObj = lookup[templateId]
            clientObj.HpFull = 100
            clientObj.Hp = hp
            EventManager.BroadcastInternalForLua(EnumEventType.HpUpdate,{ clientObj.EngineId })

            if hp==0 then
                clientObj:ShowGuanNingFx(true,0)
            elseif hp==100 then
                clientObj:ShowGuanNingFx(true,1)
            else
                clientObj:ShowGuanNingFx(true,2)
            end
        end
    end

    g_ScriptEvent:BroadcastInLua("UpdateOccupyHpInfo",templateIdList,hpList)
end

function Gas2Gac.ShowGuanNingXunLianWin( playIndex )
    CLuaGuanNingMgr.m_XunLianPlayIndex = playIndex
    CUIManager.ShowUI(CLuaUIResources.GuanNingChoiceWnd)
end


function Gas2Gac.QueryGnjcSignUpInfoResult(playIndex, signUpArray, countArray, hour, min)
    CLuaGuanNingMgr.m_CurSignUpIndex=playIndex
    CLuaGuanNingMgr.m_Hour=hour
    CLuaGuanNingMgr.m_Minute=min

    CLuaGuanNingMgr.signUpState={}
    CLuaGuanNingMgr.signUpCount={}

    local list1 = MsgPackImpl.unpack(signUpArray)
    if list1 then
        for i=1,list1.Count do
            table.insert( CLuaGuanNingMgr.signUpState,tonumber(list1[i-1]) )
        end
    -- else
        -- CLuaGuanNingMgr.signUpState={0,0,0}
    end
    local list2 = MsgPackImpl.unpack(countArray)
    if list2 then
        for i=1,list2.Count do
            table.insert( CLuaGuanNingMgr.signUpCount,tonumber(list2[i-1]) )
        end
    -- else
        -- -- CLuaGuanNingMgr.signUpCount={0,0,0,0,0}
    end

    g_ScriptEvent:BroadcastInLua("QueryGnjcSignUpInfoResult")
end

function Gas2Gac.ShowGnjcExchangeWnd()
    --关宁积分兑换元宝功能已经废弃
end

function Gas2Gac.SyncPVPCommand(class, gender, commandId, playerId, msg)
    print("SyncPVPCommand", commandId, msg)
    g_ScriptEvent:BroadcastInLua("SyncPVPCommand",class,gender,commandId,playerId, msg)
end

function Gas2Gac.XYLPTryRewardResult(groupId, bResult)
    ZhongYuanJie2020Mgr:XYLPTryRewardResult(groupId, bResult)
end

function Gas2Gac.XYLPFindThread(threadId)
    ZhongYuanJie2020Mgr:XYLPFindThread(threadId)
end

function Gas2Gac.XYLPQueryThreadsResult(data_U)
    ZhongYuanJie2020Mgr:XYLPQueryThreadsResult(data_U)
end

function Gas2Gac.YNZJQueryRankResult(rankInfo_U, selfInfo_U)
    ZhongYuanJie2020Mgr:YNZJQueryRankResult(rankInfo_U, selfInfo_U)
end

function Gas2Gac.YNZJUpdateMirror(monsterId)
    ZhongYuanJie2020Mgr:YNZJUpdateMirror(monsterId)
end

function Gas2Gac.RJPMQueryRankResult(rankInfo_U, selfInfo_U)
    ZhongYuanJie2020Mgr:RJPMQueryRankResult(rankInfo_U, selfInfo_U)
end

function Gas2Gac.JiuGeShiHunResult(bWin, hpPercent, rewardItemId)
    DuanWu2020Mgr:JiuGeShiHunResult(bWin, hpPercent, rewardItemId)
end

function Gas2Gac.JiuGeShiHunHpPercent(hpPercent)
    DuanWu2020Mgr:JiuGeShiHunHpPercent(hpPercent)
end

function Gas2Gac.JiuGeShiHunMonsterKillCount(monsterKillCount, monsterTotalCount)
    DuanWu2020Mgr:JiuGeShiHunMonsterKillCount(monsterKillCount, monsterTotalCount)
end

----------------------------------------
---Gas2Gac
----------------------------------------
function Gas2Gac.YuanXiaoLanternStart(lanterType)
    -- 加放灯特效
    if LuaYuanXiao2023Mgr.FangDeng then
        LuaYuanXiao2023Mgr:StartFangDeng(lanterType)
    else
        LuaYuanXiao2021Mgr:StartFangDeng(lanterType)
    end
end

function Gas2Gac.YuanXiaoLanternStop()
    -- 取消放灯特效
    if LuaYuanXiao2023Mgr.FangDeng then
        LuaYuanXiao2023Mgr:StopFangDeng()
    else
        LuaYuanXiao2021Mgr:StopFangDeng()
    end
end

function Gas2Gac.YuanXiaoLanternTryOpen3D()
    -- 提示3d模式
    if LuaYuanXiao2023Mgr.FangDeng then
        LuaYuanXiao2023Mgr:TryOpenFangDeng3D()
    else
        LuaYuanXiao2021Mgr:TryOpenFangDeng3D()
    end
end

function Gas2Gac.YuanXiaoLanternTryTrackTo3D(sceneId, sceneTemplateId, x, y)
    -- 寻路并提示3d模式
    if LuaYuanXiao2023Mgr.FangDeng then
        LuaYuanXiao2023Mgr:TryTrackOpenFangDeng3D(sceneId, sceneTemplateId, x, y)
    else
        LuaYuanXiao2021Mgr:TryTrackOpenFangDeng3D(sceneId, sceneTemplateId, x, y)
    end
end

function Gas2Gac.DuiDuiLeSyncPlayInfo(score, infoU)
    LuaYuanXiao2020Mgr:DuiDuiLeSyncPlayInfo(score, infoU)
end

function Gas2Gac.DuiDuiLePlayResult(score, remainNum, totalNum, infoU)
    LuaYuanXiao2020Mgr:DuiDuiLePlayResult(score, remainNum, totalNum, infoU)
end

function Gas2Gac.DuiDuiLeOpenSignUpWndResult(remainTimes, totalTimes)
    LuaYuanXiao2020Mgr:DuiDuiLeOpenSignUpWndResult(remainTimes, totalTimes)
end

-- @param title 标题
-- @param content 内容
-- @param icon_content 内容底层图片
-- @param type 类型 1为直播,2为精灵词条,3为其他链接跳转
-- @param url 链接或者中文,视type决定
-- @param duration 默认-1表示存在20秒,0表示不消失?,其他表示存在duration秒
-- @param choice 0表示内置浏览器
-- @param limitUd 限制条件:
--	{
--     level_limit = { low, high } / nil,			为nil表示不限制,否则表示等级上下限
--     vip_limit = { low, high } / nil,				为nil表示不限制,否则表示vip等级上下限
--     create_time_limit = { low, high } / nil,		为nil表示不限制,否则表示创建时间上下限
--     skip_channels = skip_channels / nil,			为nil表示不限制,否则表示跳过的渠道(具体内容服务器没校验,只是传递给客户端)
--     is_pc = is_pc / nil,							为nil表示不限制,1表示当前pc登录玩家收到,2表示移动端玩家收到
--     is_adult = is_adult / nil,					为nil表示不限制,1表示成年人玩家收到,2表示未成年人玩家收到
--     force_ignore_play = force_ignore_play / nil, 为nil表示不处理,1表示强制忽略玩法限制
--	}
function Gas2Gac.SyncGmPushBubblePopUp(title, content, icon_content, type, url, duration, choice, limitUd)
    LuaGMMgr:ShowBubblePopupWnd(type, title, content, icon_content, url, choice, duration, limitUd)
end

function Gas2Gac.EnablePaiZhaoLvJing(id, ignoreRole, density)
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:LoadLvJingEffect(id, density)
        postEffectCtrl:SetSpliteCharacter(ignoreRole)
    else
        if CScene.MainScene then
            local sceneId = CScene.MainScene.SceneTemplateId
            if sceneId == 16103076 or sceneId == 16103084 then
                ignoreRole = false
            end
        end
        CPostEffectMgr.Instance:LoadEffectFromPrefab(id, density)
        CPostEffectMgr.Instance:SetSplitCharacterFromScene(ignoreRole,id)
    end
end

function Gas2Gac.DisablePaiZhaoLvJing(id)
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:LoadLvJingEffect(0, 0)
        postEffectCtrl:SetSpliteCharacter(false)
    else
        CPostEffectMgr.Instance:DisableAllFilterEffect()
        CPostEffectMgr.Instance:SetSplitCharacterFromScene(false,id)
    end
end


-- 师徒培养
function Gas2Gac.SyncShiTuCultivateInfo(isShiFu, otherPlayerId, isRuShi, cultivateInfo)
    local tbl = MsgPackImpl.unpack(cultivateInfo)
    if not tbl then return end

    if isRuShi then return end

    local data = {}
    for i = 0, (tbl.Count - 1), 7 do
        local t = {}
        t.cultivateId = tonumber(tbl[i])
        t.currentValue = tonumber(tbl[i+1])
        t.jade = tonumber(tbl[i+2])
        t.isItem = tbl[i+3]
        t.itemId =  tbl[i+4]
        if cs_string.IsNullOrEmpty(t.itemId) then
            t.itemId = nil
        end
        t.templateId = tbl[i+5]
        if t.isItem then
            local item = Item_Item.GetData(t.templateId)
            t.itemProp = item
            if item then
                t.icon = item.Icon
            end
        else
            local equip =  EquipmentTemplate_Equip.GetData(t.templateId)
            t.itemProp = equip
            if equip then
                t.icon = equip.Icon
            end
        end
        t.itemAmount = tbl[i+6]
        table.insert(data, t)
    end
    g_ScriptEvent:BroadcastInLua("ShiTuTrainingHandbook_SyncShiTuCultivateInfo", data)
end

function Gas2Gac.SyncShiTuCultivateInfoNew(isShiFu, otherPlayerId, tudiInfo, cultivateInfo)
end

function Gas2Gac.SyncShiTuWeekTaskInfo(isShiFu, otherPlayerId, weekTaskInfo)
    local tbl = MsgPackImpl.unpack(weekTaskInfo)
    if not tbl then return end

    local data = {}
    for i = 0, (tbl.Count - 1), 3 do
        local t = {}
        t.weekTaskId = tonumber(tbl[i])
        t.currentValue = tonumber(tbl[i+1])
        t.isRewarded = tbl[i+2]
        table.insert(data, t)
    end
    g_ScriptEvent:BroadcastInLua("ShiTuTrainingHandbook_SyncShiTuWeekTaskInfo", data)
end

function Gas2Gac.SyncShiTuDailyTaskInfo(isShiFu, otherPlayerId, dailyTaskInfo)

    local tbl = MsgPackImpl.unpack(dailyTaskInfo)
    if not tbl then return end

    local data = {}
    for i = 0, (tbl.Count - 1), 7 do
        local t = {}
        t.dailyTaskId = tonumber(tbl[i])
        t.currentValue = tonumber(tbl[i+1])
        t.isActive = tbl[i+2]--活动是否开启，开启不能领取奖励
        t.canReward = tbl[i+3]
        t.isAccept = tbl[i+4]
        t.acceptPlayerId = tonumber(tbl[i+5])
        t.acceptPlayerName = tbl[i+6]
        table.insert(data, t)
    end
    g_ScriptEvent:BroadcastInLua("ShiTuTrainingHandbook_SyncShiTuDailyTaskInfo", data)
end

function Gas2Gac.SendNewShiTuWenDaoInfo(isShiFu, otherPlayerId, currentStage, tudiInfoUd, wendaoInfoUd)
    LuaShiTuTrainingHandbookMgr:SendNewShiTuWenDaoInfo(isShiFu, otherPlayerId, currentStage, tudiInfoUd, wendaoInfoUd)
end

function Gas2Gac.SendYuanDanGuildVoteInfo(limit,infoUD)
    local list = MsgPackImpl.unpack(infoUD)
    if not list then return end
    luaGuildVoteMgr.VoteData = {}
    luaGuildVoteMgr.Limit = limit
    for i = 0, list.Count-1, 7 do
        local t = {}
		t.questionId = list[i]
		t.questionContent = list[i+1]
		t.title = list[i+2]
		t.playerId = list[i+3]
		t.playerName = list[i+4]
		t.isFinish = list[i+5]
		t.isVoted = list[i+6]

		-- print("SendYuanDanGuildVoteInfo", t.questionId, t.questionContent, t.title, t.playerId, t.playerName, t.isFinish, t.isVoted)
        table.insert( luaGuildVoteMgr.VoteData, t)
	end
    g_ScriptEvent:BroadcastInLua("SendYuanDanGuildVoteInfo")
end

function Gas2Gac.SendYuanDanGuildVoteQuestion(questionId, questionContent, title, isVoted, votePlayerId, isFinish, winPlayerId, playerUD)   --打开投票详情界面
    -- print("SendYuanDanGuildVoteQuestion", questionId, questionContent, title, isVoted, votePlayerId, isFinish, winPlayerId)
    local list = MsgPackImpl.unpack(playerUD)
    if not list then return end

    luaGuildVoteMgr:ClearDetailData()       --  清空旧数据
    luaGuildVoteMgr.QuestionId = questionId
    luaGuildVoteMgr.QuestionContent = questionContent
    luaGuildVoteMgr.IsVoted = isVoted
    luaGuildVoteMgr.VotePlayerId = votePlayerId
    luaGuildVoteMgr.IsFinish = isFinish
    luaGuildVoteMgr.WinPlayerId = winPlayerId
    for i= 0, list.Count-1, 3 do
        local t = {}
		t.playerId = list[i]
		t.playerName = list[i+1]
		t.voteCount = list[i+2]

        -- print("SendYuanDanGuildVoteQuestion PlayerUD", t.playerId, t.playerName, t.voteCount)
        table.insert( luaGuildVoteMgr.PlayerOptionTable, t )
    end

    local sortFunction = function(a, b)
        if a.voteCount > b.voteCount then
            return true
        elseif a.playerId == luaGuildVoteMgr.WinPlayerId then
            return true
        end
    end

    if luaGuildVoteMgr.IsFinish then
        table.sort(luaGuildVoteMgr.PlayerOptionTable, sortFunction)
    end

    CUIManager.ShowUI(CLuaUIResources.GuildVoteDetailWnd)
end

function Gas2Gac.SendGuildMemberIdAndName(playerUD)     --  刷新帮会成员列表
    local list = MsgPackImpl.unpack(playerUD)
    if not list then return end
    luaGuildVoteMgr.GuildMemberTable = {}
    for i = 0, list.Count - 1, 2 do
        local t= {}
        t.memberId = list[i]
        t.memberName = list[i+1]
        -- print("SendGuildMemberIdAndName", t.memberId, t.memberName)
        table.insert(luaGuildVoteMgr.GuildMemberTable,t)
    end
    g_ScriptEvent:BroadcastInLua("SendGuildMemberIdAndName")
end

function Gas2Gac.SendYuanDanGuildVoteCreateResult(questionId, questionContent, title, playerId, playerName, isFinish, isVoted)  --  创建成功一个新的投票
    -- print("SendYuanDanGuildVoteCreateResult", questionId, questionContent, title, playerId, playerName, isFinish, isVoted)
    local t = {}
    t.questionId        = questionId
    t.questionContent   = questionContent
    t.title             = title
    t.playerId          = playerId
    t.playerName        = playerName
    t.isFinish          = isFinish
    t.isVoted           = isVoted
    table.insert( luaGuildVoteMgr.VoteData, t)
    g_ScriptEvent:BroadcastInLua("SendYuanDanGuildVoteCreateResult")
end

function Gas2Gac.SendYuanDanGuildDoVoteSucc(questionId)         --  投票成功
    -- print("SendYuanDanGuildDoVoteSucc"..questionId)
    for i=1,#luaGuildVoteMgr.VoteData do        --  这里刷新VoteData
        if luaGuildVoteMgr.VoteData[i].questionId == questionId then
            luaGuildVoteMgr.VoteData[i].isVoted = true
        end
    end
    if luaGuildVoteMgr.QuestionId == questionId then        --  这里刷新当前detail数据
        luaGuildVoteMgr.IsVoted = true
    end
    g_ScriptEvent:BroadcastInLua("SendYuanDanGuildDoVoteSucc")
end


----------------------------------------
---Gas2Gac
----------------------------------------
-- QueryWorkManualOverview 返回的信息
-- subTaskCount -> 本周子任务总数
-- finishedTaskCount -> 已完成或领取奖励的任务数
-- winePoint -> 酒气
-- leftOpenBottleTimes -> 本周剩余可打开酒壶的次数
function Gas2Gac.ReplyWorkManualOverview(subTask_U, subTaskCount, finishedTaskCount, winePoint, leftOpenBottleTimes, ownManual_U, manualReward_U)
    local subTaskList = MsgPackImpl.unpack(subTask_U)
    local ownManualDataList = MsgPackImpl.unpack(ownManual_U) -- 这个list中的内容就是已经购买的手册,共3项,0表示未拥有,1表示已拥有
    local manualRewardList = MsgPackImpl.unpack(manualReward_U) -- 这个list中的内容是已经领过奖励的层数信息,1表示为领取

    -- 8个子任务的进度和状态,奖励根据公式计算
    local t = {}
    for i = 0, subTaskList.Count - 1, 4 do
        local subTaskId = subTaskList[i] 	-- 子任务id
        local progress 	= subTaskList[i+1]	-- 进度
        local status 	= subTaskList[i+2]	-- 状态,1进行中,2已领取

        local expReward = math.floor(subTaskList[i+3])  --  经验奖励
        table.insert(t,{subTaskId = subTaskId,progress = progress, status = status, expReward = expReward})
    end

    HanJia2020Mgr.OpenBottleTimes = leftOpenBottleTimes
    g_ScriptEvent:BroadcastInLua("HanJia2020_OnReplyWorkManualOverview",{
        t, manualRewardList, leftOpenBottleTimes, winePoint, ownManualDataList
    })
end

-- RequestGetManualReward(获取手册某一层的奖励) 对应的返回,成功一定有返回,失败未必有返回,如果失败也需要返回需要告诉服务端处理一下
-- bSuccess 是否成功
function Gas2Gac.ReplyGetManualRewardResult(bSuccess, level)
    if bSuccess then
        g_ScriptEvent:BroadcastInLua("HanJia2020_GetManualReward",{level})
    end
end

-- RequestBuyHanJiaManual(购买手册) 对应的返回,成功一定有返回,失败未必有返回,如果失败也需要返回需要告诉服务端处理一下
-- bSuccess 是否成功
function Gas2Gac.ReplyBuyHanJiaManualResult(bSuccess, manualId, ownManual_U)
    if bSuccess then
        CUIManager.CloseUI("YanChiXiaWorkWnd")
        local pid = Mall_LingYuMallLimit.GetData(HanJia2020_ManualPrice.GetData(manualId).RMBPackageId).RmbPID
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(pid),0)
        --g_ScriptEvent:BroadcastInLua("HanJia2020_OnBuyManualSuccess",{manualId,  MsgPackImpl.unpack(ownManual_U)})
    end
end

-- RequestOpenHanJiaBottle(打开酒壶) 对应的返回,成功一定有返回,失败未必有返回,如果失败也需要返回需要告诉服务端处理一下
-- bSuccess 是否成功
function Gas2Gac.ReplyOpenBottleResult(bSuccess, leftOpenBottleTimes, reward_U)
    local list = MsgPackImpl.unpack(reward_U)
    local t = {}
    for i = 0, list.Count - 1, 2 do
        local id    = list[i] 	-- 道具id
        local count = list[i+1]	-- 数目
        table.insert(t,{ID = id,Count = count})
    end
    if bSuccess then
        if not HanJia2020Mgr.isOpenFlagonWnd then
            HanJia2020Mgr.FlagonWndData = {leftOpenBottleTimes, t}
            CUIManager.ShowUI(CLuaUIResources.FlagonOpenWnd)
        else
            g_ScriptEvent:BroadcastInLua("HanJia2020_OnBottleOpen",{leftOpenBottleTimes, t})
        end
    end
end

-- RequestGetHanJiaSubTaskReward（领取子任务奖励）对应的返回, 成功一定有返回,失败未必有返回,如果失败也需要返回需要告诉服务端处理一下
-- bSuccess 是否成功
function Gas2Gac.ReplyGetHanjiaSubTaskReward(bSuccess, subTaskId, status, winePoint, rewardedTaskCount, leftOpenBottleTimes)
    if bSuccess then
        g_ScriptEvent:BroadcastInLua("HanJia2020_OnGetTaskReward",{ subTaskId, status, winePoint, rewardedTaskCount, leftOpenBottleTimes})
    end
end

-- ---------------------
-- 本玩法会发OpenWnd RPC
--  对应语义如下:
--  OpenWnd("HanJiaManualWnd")  -> 打开燕赤霞打工手册

--  OpenWnd("SubmitSnowballWnd", dataUd)  -> 打开提交雪球窗口, dataUd  unpack 出来是一个长度为4的list,
--    furnitureId   :雪球装饰物id
--    currentCount  :当前进度
--    maxCount      :需要总进度
--    currentStage  :当前状态(1 -> 大雪球, 2 -> 半成品雪人, 3 -> 雪人)

--  OpenWnd("DecorateSnowmanWnd", dataUd)  -> 打开装饰雪人窗口,dataUd unpack 出来是一个长度为1的list,对应 furnitureId,语义是雪人的装饰物id
-- ---------------------

-- 提交雪球(RequestSubmitSnowball) 请求返回的函数
-- 是否成功, 雪球装饰物id, 当前进度, 需要总进度, 当前状态(1 -> 大雪球, 2 -> 半成品雪人, 3 -> 雪人)
function Gas2Gac.SyncSubmitSnowballResult(bSuccess, furnitureId, currentCount, maxCount, currentStage)
    if bSuccess then
        HanJia2020Mgr:RequestInteractWithUnfinishedSnowMan(furnitureId, currentCount, maxCount, currentStage)
        g_MessageMgr:ShowMessage("HanJia2020_SubmitSnowballResult_Success")
    else
        HanJia2020Mgr.unfinishedSnowManID = furnitureId
        HanJia2020Mgr.curSubmitSnowBallCount = currentCount
        HanJia2020Mgr.maxSubmitSnowBalllCount = maxCount
        HanJia2020Mgr.submitSnowBallState = currentStage
        if HanJia2020Mgr.isGamePlayOpen and currentStage < 3 then
            g_ScriptEvent:BroadcastInLua("HanJia2020_OnSubmitSnowballResult",{})
        end
    end
end

-- 装饰雪人(RequestDecorateSnowman) 请求返回的函数
-- 是否成功, 雪人装饰物id, 雪人的新装饰物模板id(对应装饰物表)
function Gas2Gac.SyncDecorateSnowmanResult(bSuccess, furnitureId, newTemplateId)
    if bSuccess then
        CUIManager.CloseUI(CLuaUIResources.DecorateSnowmanWnd)
    end
end

--雪人做表情动作
function Gas2Gac.SyncUseDecoratedSnowmanAction(furnitureId)
    local cclientFurniture = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if HanJia2020Mgr.snowmanAnimationTickDict[furnitureId] then
        return
    end
    if cclientFurniture then
        local ro = cclientFurniture.RO
        if ro then
            local expressionId = HanJia2020_Setting.GetData().SnowManExpression
            local expression_define = Expression_Define.GetData(expressionId)
            if expression_define then
                ro.AniEndCallback = nil
                ro:DoAni(expression_define.AniName, expression_define.Loop == 1, expression_define.StartTime,1.0,0.15, true, 1.0)
                ro.AniEndCallback = DelegateFactory.Action(function ()
                    if ro then
                        ro.AniEndCallback = nil
                        ro:DoAni("stand01", true, 0, 1.0, 0.15, true, 1.0)
                    end
                end)
            end
        end
    end
end


---------------
--Gas2Gac RPC--
---------------

function Gas2Gac.FinishNewbieSchoolWish()
	g_MessageMgr:ShowMessage("Newbie_School_Task_Submit_Wishing_Success")
end


function Gas2Gac.CreateLuaFlag(engineId, templateId, ownerEngineId, pixelPosX, pixelPosY, pixelPosZ, maxLifeTime)
    LuaSimpleFlagMgr:CreateLuaFlag(engineId, templateId, ownerEngineId, pixelPosX, pixelPosY, pixelPosZ, maxLifeTime)
end

function Gas2Gac.DestroyLuaFlag(engineId)
    LuaSimpleFlagMgr:DestroyLuaFlag(engineId)
end


function Gas2Gac.SendHuiShiAnswer(answerIdx, choiceInfo)
    CKeJuMgr.Inst.rightAnswerIndex = answerIdx
    EventManager.BroadcastInternalForLua(EnumEventType.KeJuQuestionAnswerUpdate, {})
    LuaKeJuMgr:SendHuiShiChoiceInfo(choiceInfo)
end

function Gas2Gac.SendDianShiAnswer(answerIdx, choiceInfo)
    CKeJuMgr.Inst.rightAnswerIndex = answerIdx
    EventManager.BroadcastInternalForLua(EnumEventType.KeJuQuestionAnswerUpdate, {})
    LuaKeJuMgr:SendDianShiChoiceInfo(choiceInfo)
end

-- questionId: 第几道乡试题目
-- choiceInfo: 各选项选择人数
function Gas2Gac.SendXiangShiQuestionChoiceInfo(questionId, choiceInfo)
    LuaKeJuMgr:SendXiangShiChoiceInfo(choiceInfo, questionId)
end

function Gas2Gac.SendXiangShiConfirmMessage(msgName)
    MessageWndManager.ShowOKCancelMessageWithTimelimitAndPriority(
            g_MessageMgr:FormatMessage(msgName), 300, 0,
            DelegateFactory.Action(function()
                Gac2Gas.RequestJoinXiangShi() end),nil,
            nil,nil,false)
end



---------------
--Gas2Gac RPC--
---------------

function Gas2Gac.NationalDayTXZRSignUpPlayResult(bSuccess)
	LuaNationalDayMgr.OnTXZRSignUpPlayResult(bSuccess)
end

function Gas2Gac.NationalDayTXZRCancelSignUpResult(bSuccess)
	LuaNationalDayMgr.OnTXZRCancelSignUpResult(bSuccess)
end

function Gas2Gac.NationalDayTXZRCheckInMatchingResult(bSuccess)
	LuaNationalDayMgr.OnTXZRCheckInMatchingResult(bSuccess)
end

function Gas2Gac.NationalDayTXZRSyncPlayerInfo(playerId, playerInfo_U)
	--暂时用不到
end

function Gas2Gac.NationalDayTXZRSyncStatusToPlayersInPlay(playStatus, enterStatusTime, extraData_U)
	--暂时用不到
end

function Gas2Gac.NationalDayTXZRPlayAoeFx(aoeId)
	LuaNationalDayMgr.TXZRPlayAoeFx(aoeId)
end

function Gas2Gac.NationalDayTXZRPlayEnd(extraData_U)
	LuaNationalDayMgr.OnTXZRPlayEnd(extraData_U)
end

function Gas2Gac.NationalDayJCYWSyncStatusToPlayersInPlay(playStatus, enterStatusTime, extraData_U)
	--暂时用不到
end

function Gas2Gac.NationalDayJCYWPlayEnd(validPlayer_U, achieveGoals_U, score, usedTime)
	-- validPlayer_U:m_CharacterID,m_Name,m_Grade,m_Class,m_Gender,m_XiuWeiGrade,m_XiuLianGrade,m_Expression,m_ExpressionTxt,m_Sticker,m_XianFanStatus,m_HasFeiSheng,m_ProfileInfo_U,m_IsLeader
	LuaNationalDayMgr.OnJCYWPlayEnd(validPlayer_U, achieveGoals_U, score, usedTime)
end

---------------
--Gas2Gac RPC--
---------------

function Gas2Gac.OpenExchangeNewJueJiItemWnd(wndType)
	if wndType == 1 then
		--使用遗忘的绝技兑换70绝技
		LuaProfessionTransferMgr.ShowJueJiExchangeWndByTargetJueJiLevel(70)
	elseif wndType == 2 then
		--使用遗忘的绝技兑换100绝技
		LuaProfessionTransferMgr.ShowJueJiExchangeWndByTargetJueJiLevel(100)
	end
end

function Gas2Gac.ExchangeNewJueJiItemSuccess(groupIdx, itemTempId70Forget, itemTempId100Forget, itemTempId70, itemTempId100)
	g_ScriptEvent:BroadcastInLua("OnExchangeNewJueJiItemSuccess", groupIdx, itemTempId70Forget, itemTempId100Forget, itemTempId70, itemTempId100)
end


---------------
--Gas2Gac RPC--
---------------
function Gas2Gac.SyncLiangHaoInfo(reason, lianghaoId, expiredTime)
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	local profileInfo = mainplayer.BasicProp.ProfileInfo
	profileInfo.LiangHaoId = lianghaoId
	profileInfo.LiangHaoExpiredTime = expiredTime

	CPersonalSpaceMgr.Inst:updateLiangHaoInfo() -- 根据坤少的意见push给梦岛一下，及时刷新

	g_ScriptEvent:BroadcastInLua("OnSyncLiangHaoInfo")
end

function Gas2Gac.ReplyLiangHaoRenewInfo(targetId, lianghaoId, renewPrice, oldExpiredTime, newExpiredTime, addDay)
	LuaLiangHaoMgr.ReplyLiangHaoRenewInfo(targetId, lianghaoId, renewPrice, oldExpiredTime, newExpiredTime, addDay)
end

-- tp目前有三种：gac-客户端主动请求  buy-玩家购买了靓号  jingpai-玩家竞拍靓号
function Gas2Gac.ReplyLiangHaoSellInfo(buyData, jingpaiData, jingpaiEndTime, tp, nextStartTime, dingzhiData)
	local purchaseTbl = {}
	local auctionTbl = {}
	local auctionExpireTime = 0
	local customizeTbl = {}
	local buyList = MsgPackImpl.unpack(buyData)
    for i = 0, buyList.Count-1, 2 do
    	local data = {}
    	data.lianghaoId = buyList[i]
    	data.price = buyList[i+1]
    	table.insert(purchaseTbl, data)
    end

	local jingpaiList = MsgPackImpl.unpack(jingpaiData)
    for i = 0, jingpaiList.Count-1, 3 do
    	local data = {}
    	data.lianghaoId = jingpaiList[i]
    	data.price = jingpaiList[i+1]
    	data.count = jingpaiList[i+2]
    	table.insert(auctionTbl, data)
    end

    local dingzhiList = MsgPackImpl.unpack(dingzhiData)
    for i = 0, dingzhiList.Count-1, 4 do
    	local data = {}



    	local digitNum = tonumber(dingzhiList[i])
    	customizeTbl[digitNum] = {}
    	customizeTbl[digitNum].avaliableNum = tonumber(dingzhiList[i+1])
    	customizeTbl[digitNum].playerNum = tonumber(dingzhiList[i+2])
    	customizeTbl[digitNum].list = {}
    	local tbl = dingzhiList[i+3]
    	for j = 0, tbl.Count-1, 1 do
    		local item = tbl[j]
    		-- playerId, playerName, serverName, price
    		local data = {}
    		data.playerId = tonumber(item[0])
    		data.playerName = tostring(item[1])
    		data.serverName = tostring(item[2])
    		data.price = tonumber(item[3])
    		table.insert(customizeTbl[digitNum].list ,data)
    	end
    	if customizeTbl[digitNum].avaliableNum > customizeTbl[digitNum].playerNum then
    		-- 如果投放数量大于参与人数，补全空缺
    		local n = customizeTbl[digitNum].avaliableNum - customizeTbl[digitNum].playerNum
    		for i=1,n do
    			local data = {}
	    		data.playerId = 0
	    		data.playerName = ""
	    		data.serverName = ""
	    		data.price = 0
	    		table.insert(customizeTbl[digitNum].list ,data)
    		end
    	end
    end

    LuaLiangHaoMgr.ReplyLiangHaoPurchaseInfo(purchaseTbl, auctionTbl, customizeTbl, jingpaiEndTime, nextStartTime, tp)
end

function Gas2Gac.ReplyLiangHaoJingPaiInfo(lianghaoId, roleId, roleName, serverName, currentPrice, myPrice, minPrice, maxPrice, priceStep)
	LuaLiangHaoMgr.ReplyLiangHaoAuctionInfo(lianghaoId, roleId, roleName, serverName, currentPrice, myPrice, minPrice, maxPrice, priceStep)
end

function Gas2Gac.ReplyMyLiangHaoJingPaiInfo(data)
	local tbl = {}
    local list = MsgPackImpl.unpack(data)
    for i = 0, list.Count-1, 6 do
    	local data = {}
    	data.lianghaoId = list[i]
    	data.roleId = list[i+1]
    	data.roleName = tostring(list[i+2])..LocalString.GetString("·")..tostring(list[i+3]) -- roleName·serverName
    	data.currentPrice = list[i+4]
    	data.myPrice = list[i+5]
    	table.insert(tbl, data)
    end
    LuaLiangHaoMgr.ReplyMyLiangHaoAuctionInfo(tbl)
end

function Gas2Gac.SetLiangHaoPrivilegeSuccess(tp, value)
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	if tp == EnumLiangHaoPrivilegeType.HideIcon then
		mainplayer.BasicProp.ProfileInfo.HideLiangHaoIcon = value
	elseif tp == EnumLiangHaoPrivilegeType.ShowDigitIcon then
		mainplayer.BasicProp.ProfileInfo.ShowDigitIcon = value
	elseif tp == EnumLiangHaoPrivilegeType.RefuseAddCrossFriend then
		mainplayer.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.RefuseAddCrossFriend, value > 0)
	elseif tp == EnumLiangHaoPrivilegeType.RefuseCrossStrangerMessage then
		mainplayer.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.RefuseCrossStrangerMessage, value > 0)
	end

	g_ScriptEvent:BroadcastInLua("OnLiangHaoPrivilegeSettingUpdate")
end

function Gas2Gac.ReplyLiangHaoSurplusYuanBaoValue(lianghaoId, context, buyPrice, expiredTime, retYuanBao)
	LuaLiangHaoMgr.ReplyLiangHaoSurplusYuanBaoValue(lianghaoId, context, buyPrice, expiredTime, retYuanBao)
end

-- 定制

function Gas2Gac.OpenLiangHaoDingZhiWnd(itemId, place, pos, digitNum, from, to)
	LuaLiangHaoMgr.OpenLiangHaoDingZhiWnd(itemId, place, pos, digitNum, from, to)
end

function Gas2Gac.ReplyCalcLiangHaoPriceResult(lianghaoId, price, context)
	g_ScriptEvent:BroadcastInLua("ReplyCalcLiangHaoPriceResult", lianghaoId, price, context)
end

function Gas2Gac.ReplyLiangHaoDingZhiInfo(digitNum, toufangNum, playerNum, roleId, roleName, serverName, currentPrice, myPrice, minPrice, maxPrice, priceStep)
	-- 客户端可以根据投放数量和参与玩家数确定当前的玩家信息是第几位
	LuaLiangHaoMgr.ReplyAuctionCustomizeCardInfo(digitNum, toufangNum, playerNum, roleId, roleName, serverName, currentPrice, myPrice, minPrice, maxPrice, priceStep)
end

function Gas2Gac.ReplyMyLiangHaoDingZhiApplyInfo(dataUD)
	local tbl = {}
    local list = MsgPackImpl.unpack(dataUD)
    for i = 0, list.Count-1, 9 do
    	local data = {}
    	data.digitNum = list[i]
    	data.lastBidOrder = list[i+1] --最后一名出价者排名
    	data.roleId = list[i+2]
    	data.roleName = tostring(list[i+3])..LocalString.GetString("·")..tostring(list[i+4]) -- roleName·serverName
    	data.currentPrice = list[i+5]
    	data.myPrice = list[i+6] --我的出价
    	data.myPos = list[i+7] --我的排名
    	data.toufangNum = list[i+8] --投放数目
    	table.insert(tbl, data)
    end

    LuaLiangHaoMgr.ReplyMyAuctionCustomizeCardInfo(tbl)
end

function Gas2Gac.BuyDingZhiLiangHaoSuccess(lianghaoId, price)
    LuaLiangHaoMgr.CloseLiangHaoDingZhiWnd()
end

--Gas2Gac.SendJingLingMsg的同类，类比处理
function Gas2Gac.SendJingLingKeyMsg(keyMessage, duration, canJudge)
	if CServerTimeMgr.Inst.timeStamp > duration then
    	return
    end

    local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
    local val = 0
    if canJudge then val = 1 end
    CommonDefs.DictAdd_LuaCall(dict, "override_evaluate", val)
    CJingLingMgr.Inst:ShowJingLingWnd(keyMessage, "o_push", false, false, dict, false)
    EventManager.Broadcast(EnumEventType.OnJingLingIMReceived)
end


function Gas2Gac.SendJingLingKeyMsgWithCondition(keyMessage, Condition, canJudge)
    if not CClientMainPlayer.Inst then
        return
    end

    local checkCondition = function (Condition)
        if not Condition or Condition == "" then
            return true
        end

        for condName, condArgs in string.gmatch(Condition, "([^:;]+):([^:;]+);?") do
            if condName == "guaji" then
                if not (string.lower(condArgs) == "true" and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GuaJi")) == 1 or
                    string.lower(condArgs) == "false" and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GuaJi")) == 0) then
                    return false
                end
            elseif condName == "ispc" then
                if not (string.lower(condArgs) == "true" and CommonDefs.Is_PC_PLATFORM() or
                    string.lower(condArgs) == "false" and not CommonDefs.Is_PC_PLATFORM()) then
                    return false
                end
            elseif condName == "minlevel" then
                local level = tonumber(condArgs)
                if not level then return false end

                if CClientMainPlayer.Inst.Level < level then
                    return false
                end
            elseif condName == "maxlevel" then
                local level = tonumber(condArgs)
                if not level then return false end

                if CClientMainPlayer.Inst.Level > level then
                    return false
                end
            elseif condName == "minvip" then
                local VIP = tonumber(condArgs)
                if not VIP then return false end

                if CClientMainPlayer.Inst.ItemProp.Vip.Level < VIP then
                    return false
                end
            elseif condName == "maxvip" then
                local VIP = tonumber(condArgs)
                if not VIP then return false end

                if CClientMainPlayer.Inst.ItemProp.Vip.Level > VIP then
                    return false
                end
            elseif condName == "skipchannel" then
                local channelName = CLoginMgr.Inst:GetLoginChannelConverted()
                if not channelName then
                    return false
                end

                for channel in string.gmatch(condArgs, "([^,;]+)") do
                    if channel == channelName then
                        return false
                    end
                end
            end
        end

        return true
    end

    if not checkCondition(Condition) then
        return
    end

    local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
    local val = 0
    if canJudge then val = 1 end
    CommonDefs.DictAdd_LuaCall(dict, "override_evaluate", val)
    CJingLingMgr.Inst:ShowJingLingWnd(keyMessage, "o_push", false, false, dict, false)
    EventManager.Broadcast(EnumEventType.OnJingLingIMReceived)
end

function Gas2Gac.PlayNormalSceneQTE(qteId, duration,usedInMainUI)
    LuaQTEMgr.TriggerNormalSceneQTE(qteId, duration,usedInMainUI)
end

-- 关闭 Normal Scene QTE 窗口
function Gas2Gac.CloseQte(qteId)
    LuaQTEMgr.CloseNormalSceneQTE(qteId)
end


function Gas2Gac.RequestSelectLingShou(place, pos, itemId, lingshouTempId1, lingshouTempId2)
    CLuaLingShouMgr.m_GuideInfo = {
        place = place,
        pos = pos,
        itemId = itemId,
        lingshouTempId1 = lingshouTempId1,
        lingshouTempId2 = lingshouTempId2
    }
    CUIManager.ShowUI(CLuaUIResources.LingShouChosenWnd)
end

function Gas2Gac.RequestOpenGetPiXiangWnd()
    if CClientMainPlayer.Inst.hasLingShou then
        CLingShouMgr.Inst.fromNPC = true
        CUIManager.ShowUI(CLuaUIResources.LingShouGetSkinWnd)
    else
        g_MessageMgr:ShowMessage("LINGSHOU_DONT_HAVE")
    end
end


function Gas2Gac.LingShouDongFangFinish(lingshouId)
    -- EventManager.Broadcast(EnumEventType.LingShouDongFangFinish, lingshouId);
	g_ScriptEvent:BroadcastInLua("LingShouDongFangFinish",lingshouId)
end

function Gas2Gac.LingShouEnterDongFangState(lingshouId)
    -- EventManager.Broadcast(EnumEventType.LingShouEnterDongFangState, lingshouId);
	g_ScriptEvent:BroadcastInLua("LingShouEnterDongFangState",lingshouId)
end

function Gas2Gac.DeleteLingShou(lingshouId)
    CLingShouMgr.Inst:DeleteLingShou(lingshouId)
	g_ScriptEvent:BroadcastInLua("DeleteLingShou",lingshouId)

    if CLingShouMgr.Inst:GetLingShouCount()==0 then
        if CUIManager.IsLoaded(CUIResources.LingShouMainWnd) then
            CUIManager.CloseUI(CUIResources.LingShouMainWnd)
        end
    end
end

function Gas2Gac.AddLingShou(lingshouId, lingshouBuf)
    local lingshou = CLingShou()
    lingshou:LoadFromString(lingshouBuf);

    -- //string lingshouId = lingshou.Id;
    local lingshouName = lingshou.Name;
    local level = lingshou.Level;
    local evolveGrade = lingshou.Props.EvolveGrade;
    local templateId = lingshou.TemplateId;
    local nature = lingshou.Props.Nature;
    local gender = lingshou.Props.MarryInfo.Gender;
    CLingShouMgr.Inst:AddLingShouOverview(lingshouId, lingshouName, level, evolveGrade, templateId,nature, gender)
	g_ScriptEvent:BroadcastInLua("AddLingShou",lingshouId)
end

function Gas2Gac.UpdatePlayerLingShouMaxNumber(maxlingshouNumber,isUsedExtraLingShouSlotItem)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.FightProp.LingShouNumber = maxlingshouNumber
        CClientMainPlayer.Inst.ItemProp.IsUsedExtraLingShouSlotItem = isUsedExtraLingShouSlotItem
    end
	g_ScriptEvent:BroadcastInLua("UpdatePlayerLingShouMaxNumber",maxlingshouNumber,isUsedExtraLingShouSlotItem)
end

function Gas2Gac.ExchangeLingShouPartnerSkillSuccess(lingshouId1,lingshouId2)
	g_ScriptEvent:BroadcastInLua("ExchangeLingShouPartnerSkillSuccess",lingshouId1,lingshouId2)
end


--rpc--
-- roundIndex : 也是 matrixIndex
-- chessDataUD packed by msgpack.unpack (chess:GetAllGrid())
function Gas2Gac.UpdateLianLianKanChessData(roundIndex, chessDataUD)
    if not CLuaLianLianKanMgr.m_Play then
        CLuaLianLianKanMgr.m_Play=CLuaLianLianKanPlay:new()
    end
    CLuaLianLianKanMgr.m_Play:Init(roundIndex)
    local list = MsgPackImpl.unpack(chessDataUD)
    if list then
        local t={}
        for i=1,list.Count do
            table.insert( t,tonumber(list[i-1]) )
        end
        CLuaLianLianKanMgr.m_Play:UpdateLianLianKanChessData(t)
    end
end
-- playerScoreListUD packed by msgpack.unpack ({playerId1,score1,name,playerId2,score2,name})
function Gas2Gac.UpdateAllPlayerScoreToClient(playerScoreListUD)
    -- print("UpdateAllPlayerScoreToClient")
    local list = MsgPackImpl.unpack(playerScoreListUD)
    if list then
        local playerId1=tonumber(list[0])
        local score1=tonumber(list[1])
        local name1=tostring(list[2])
        local playerId2=0
        local score2=0
        local name2=""
        if list.Count>3 then
            playerId2=tonumber(list[3])
            score2=tonumber(list[4])
            name2=tostring(list[5])
        end
        if not CLuaLianLianKanMgr.m_Play then
            CLuaLianLianKanMgr.m_Play=CLuaLianLianKanPlay:new()
        end
        CLuaLianLianKanMgr.m_PlayerInfos={{playerId1,score1,name1},{playerId2,score2,name2}}
        CLuaLianLianKanMgr.m_Play.m_PlayerId1=playerId1
        CLuaLianLianKanMgr.m_Play.m_PlayerId2=playerId2
        g_ScriptEvent:BroadcastInLua("UpdateAllPlayerScoreToClient",playerId1,score1,name1,playerId2,score2,name2)
    end
end

function Gas2Gac.LianLianKanPlayerSelectOneChessman(playerId, playerEngineId, x, y)
    CLuaLianLianKanMgr.m_Play:PlayerSelectOneChessman(playerId, x, y)
end

function Gas2Gac.LianLianKanPlayerCancelSelectOneChessman(playerId, playerEngineId, x, y)
    CLuaLianLianKanMgr.m_Play:PlayerCancelSelectOneChessman(playerId, x, y)
end

function Gas2Gac.LianLianKanPlayerSelectOneChessmanFail(playerId, playerEngineId, x, y)
    CLuaLianLianKanMgr.m_Play:PlayerSelectOneChessmanFail(playerId, x, y)
end

function Gas2Gac.LianLianKanPlayerMatchChessSuccess(playerId, playerEngineId, x1, y1, x2, y2)
    CLuaLianLianKanMgr.m_Play:PlayerMatchChessSuccess(playerId, x1, y1, x2, y2)
end

function Gas2Gac.LianLianKanPlayerMatchChessFail(playerId, playerEngineId, x1, y1, x2, y2)
    CLuaLianLianKanMgr.m_Play:PlayerMatchChessFail(playerId, x1, y1, x2, y2)
end

function Gas2Gac.OpenLianLianKanQuestionWnd(question, answer1, answer2)
    CLuaLianLianKanMgr.m_Question=question
    CLuaLianLianKanMgr.m_Answer1=answer1
    CLuaLianLianKanMgr.m_Answer2=answer2
    CUIManager.ShowUI("LianLianKanQuestionWnd")
end
function Gas2Gac.CloseLianLianKanQuestionWnd()
    CUIManager.CloseUI("LianLianKanQuestionWnd")
end

function Gas2Gac.SendLianLianKanSettlementData(victory, costtime, settlementData)
    local list = MsgPackImpl.unpack(settlementData)
    local resultInfo={}
    for i=1,list.Count do
        local info=list[i-1]
        local playerInfo = {
            playerId = tonumber(info[0]),
            name = tostring(info[1]),
            class = tonumber(info[2]),
            gender = tonumber(info[3]),
            score = tonumber(info[4]),
            maxscore = tonumber(info[5]),
            newRecord = info[6],
        }
        local t={}
        local questionList = info[7]
        for i=1,questionList.Count do
            local questionItem = questionList[i-1]
            local t2={
                questionId = tonumber(questionItem[0]),
                answerIdx = tonumber(questionItem[1]),
                score = tonumber(questionItem[2]),
                addscore = tonumber(questionItem[3]),
                pass = questionItem[4],
            }
            table.insert( t,t2 )
        end
        playerInfo.questions = t
        table.insert( resultInfo,playerInfo )
    end
    CLuaLianLianKanMgr.m_ResultInfo = resultInfo
    CLuaLianLianKanMgr.m_ResultCostTime = costtime
    CUIManager.ShowUI("LianLianKanResultWnd")
end

-- 查询排行榜结果
function Gas2Gac.QueryLianLianKan2018RankReturn(rankDataUD, inRank, selfScore, selfCostTime)
    local list = MsgPackImpl.unpack(rankDataUD)
    if list then
        local t={}
        for i=1,list.Count do
            local info = list[i-1]
            table.insert( t,{
                id = info[0],
                name = info[1],
                score = info[2],
                time = info[3],
            } )
        end
        g_ScriptEvent:BroadcastInLua("QueryLianLianKan2018RankReturn",t,inRank, selfScore, selfCostTime)
    end
end
-- 奖池的总
function Gas2Gac.RequestLianLianKanBonusPoolReturn(bonus)
    g_ScriptEvent:BroadcastInLua("RequestLianLianKanBonusPoolReturn",bonus)
end

-- TODO 查询所有草的数量
function Gas2Gac.QueryAllZhongCaoCountRet(progress,dataUD)
    local t={}
    local dic = MsgPackImpl.unpack(dataUD)
    if dic then
        CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (k, v)
            t[tonumber(k)]=tonumber(v)
        end))
        g_ScriptEvent:BroadcastInLua("QueryAllZhongCaoCountRet",progress,t)
    end
end

function Gas2Gac.PlayerZhongCaoRet(mallItemId, count)
    g_ScriptEvent:BroadcastInLua("PlayerZhongCaoRet",mallItemId,count)
end



function Gas2Gac.QXJTSendData(nUploadTimes, bRegistered, bIsOpen)

    CLuaShareMgr.QixiData={Count = nUploadTimes}
    if not bIsOpen then
        CLuaShareMgr.QixiData.State = -1--服务器未开启
    else
        CLuaShareMgr.QixiData.State = bRegistered and 1 or 0 --1表示报名，0表示未报名
    end
	g_ScriptEvent:BroadcastInLua("OnQixiDataRecived")
end


---------------------------
---------Gas2Gac-----------
---------------------------
-- selfSendNum 自己的送花数
-- 玩家是否上榜通过检查是否存在于rankData中判断
function Gas2Gac.ReplyValentineDaySendFlowerRank(selfSendNum, selfGuildName, rankData)
    local list = MsgPackImpl.unpack(rankData)
    if not list then
        return
    end

    LuaValentine2019Mgr.m_InTheNameOfLoveOthersInfo = {}
    LuaValentine2019Mgr.m_InTheNameOfLoveSelfGuildName = selfGuildName
    LuaValentine2019Mgr.m_InTheNameOfLoveSelfFlowerNum = selfSendNum

    local mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
    LuaValentine2019Mgr.m_InTheNameOfLoveSelfRank = -1
    for i = 0, list.Count-1, 6 do
        local rank = list[i]
        local playerId = list[i+1]
        local playerName = list[i+2]
        local playerClazz = list[i+3]
        local guildName = list[i+4]
        local sendNum = list[i+5]

        local curInfo = {rank = rank, playerId = playerId,playerName = playerName, playerClass = playerClazz, guildName = guildName, flowerNum = sendNum}
        table.insert(LuaValentine2019Mgr.m_InTheNameOfLoveOthersInfo, curInfo)

        if playerId == mainPlayerId then
            LuaValentine2019Mgr.m_InTheNameOfLoveSelfRank = rank
        end
    end

    LuaValentine2019Mgr.ShowInTheNameOfLoveRankWnd()

end

function Gas2Gac.AutoTakePhoto()
    LuaValentine2019Mgr.AutoTakePhoto()
end

-- 情缘手记列表
function Gas2Gac.SyncValentineQYSJList(ud)
    local list = MsgPackImpl.unpack(ud)
    local tbl = {}
    for i = 0, list.Count - 1, 2 do
        local partnerId = tonumber(list[i])
        local partnerName = list[i+1]
        table.insert(tbl,{playerId=partnerId, playerName=partnerName})
    end
    LuaValentine2019Mgr.ShowQingYuanShouJiListWnd(tbl)
end

-- 与某个玩家的情缘手记数据
function Gas2Gac.SyncValentineQYSJInfo(ud)
    local list = MsgPackImpl.unpack(ud)
    if list.Count < 2 then return end
    local partnerId = tonumber(list[0])
    local partnerName = tostring(list[1])
    local taskInfo = {}
    if list.Count > 2 then
        for i = 2, list.Count-1, 4 do
            local data = {}
            data.taskSeqId = tonumber(list[i])
            data.taskId = tonumber(list[i+1])
            data.taskStatus = tonumber(list[i+2])
            data.taskUnlockTime = tonumber(list[i+3])
            table.insert(taskInfo, data)
        end
    end

    LuaValentine2019Mgr.ShowQingYuanShouJiContentWnd(partnerId, partnerName, taskInfo)
end


-- 弹出誓言输入框
function Gas2Gac.ShowValentineQYSSPromiseAndSubmitWnd(partnerId)
    LuaValentine2019Mgr.ShowPromiseWnd(partnerId)
end

-- 前世姻缘列表
function Gas2Gac.SyncValentineQSYYInfo(ud)
    LuaValentine2019Mgr.m_PreExistenceInfo = {}

    local list = MsgPackImpl.unpack(ud)
    for i = 0, LuaValentine2019Mgr.m_TotExistenceGeneration - 1 do
        local curTemplateID;
        if i * 2 < list.Count then
            curTemplateID = list[i*2]
            --TODO: 新物品特效
        else
            curTemplateID = 0
        end
        table.insert(LuaValentine2019Mgr.m_PreExistenceInfo, curTemplateID)
    end

    g_ScriptEvent:BroadcastInLua("SyncValentineQSYYInfoFinish")

end


-- 同步登记/上架的判断条件信息
function Gas2Gac.CBG_SendConditionsInfo(requestType, retInfoUd, extra)

	local dict = MsgPackImpl.unpack(retInfoUd)
	if not dict then return end

	if requestType == "register" then

		LuaTreasureHouseMgr.RegisterInfo = {}

		CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (___key, ___value)
			local ret = ___value.ret
			local extra = ___value.extra
			table.insert(LuaTreasureHouseMgr.RegisterInfo, {
				conditionIdx = tonumber(___key),
				ret = ret,
				extra = extra,
				})
		end))

		local function compare(a, b)
			if a.ret and not b.ret then
				return false
			elseif not a.ret and b.ret then
				return true
			else
				return a.conditionIdx < b.conditionIdx
			end
		end
		table.sort(LuaTreasureHouseMgr.RegisterInfo, compare)

		if CUIManager.IsLoaded(CLuaUIResources.TreasureHouseRegisterWnd) then
			g_ScriptEvent:BroadcastInLua("CBGSendRegisterConditionsInfo", LuaTreasureHouseMgr.RegisterInfo)
		else
			CUIManager.ShowUI(CLuaUIResources.TreasureHouseRegisterWnd)
		end

	elseif requestType == "onshelf" then

		LuaTreasureHouseMgr.ShelfInfo = {}

		CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (___key, ___value)

			local ret = ___value.ret
			local extra = ___value.extra
			table.insert(LuaTreasureHouseMgr.ShelfInfo, {
				conditionIdx = tonumber(___key),
				ret = ret,
				extra = extra,
				})
		end))

		local function compare(a, b)
			if a.ret and not b.ret then
				return false
			elseif not a.ret and b.ret then
				return true
			else
				return a.conditionIdx < b.conditionIdx
			end
		end

		table.sort(LuaTreasureHouseMgr.ShelfInfo, compare)

		if CUIManager.IsLoaded(CLuaUIResources.TreasureHouseShelfWnd) then
			g_ScriptEvent:BroadcastInLua("CBGSendShelfConditionsInfo", LuaTreasureHouseMgr.ShelfInfo)
		else
			CUIManager.ShowUI(CLuaUIResources.TreasureHouseShelfWnd)
		end
	end
end

-- 同步在售列表，开始
function Gas2Gac.CBG_SendAllPlayerInfo_Begin(extra)

	LuaTreasureHouseMgr.SendAllPlayerInfo_Status = 1
	LuaTreasureHouseMgr.AllPlayerInfos = {}
end

-- 同步在售列表，数据
function Gas2Gac.CBG_SendAllPlayerInfo(allPlayerInfoUd, extra)

	local list = MsgPackImpl.unpack(allPlayerInfoUd)
	if not list then return end

	-- 必须在CBG_SendAllPlayerInfo_Begin之后
	if LuaTreasureHouseMgr.SendAllPlayerInfo_Status ~= 1 then return end

	for i = 0, list.Count -1, 1 do

		local basic = list[i]["Basic"]
		local extra = list[i]["Extra"]

		local id = basic.Id
		local name = basic.Name
		local class = basic.Class
		local grade = basic.Grade
		local gender = basic.Gender
		local equipScore = basic.EquipScore
		local xianfan = basic.XianFan
		local price = extra.Price
		local desc = extra.Desc
		local onTime = extra.OnTime
		local offTime = extra.OffTime

		table.insert(LuaTreasureHouseMgr.AllPlayerInfos, {
			id = id,
			name = name,
			class = class,
			grade = grade,
			gender = gender,
			equipScore = equipScore,
			xianfan = xianfan,
			price = price,
			desc = desc,
			onTime = onTime,
			offTime = offTime,
			})
	end
end

-- 同步在售列表，结束
function Gas2Gac.CBG_SendAllPlayerInfo_End(extra)
	LuaTreasureHouseMgr.SendAllPlayerInfo_Status = 2
	-- 更新待售区信息
	g_ScriptEvent:BroadcastInLua("CBGSendAllPlayerInfo", LuaTreasureHouseMgr.AllPlayerInfos)
end


---------------------------
---------Gas2Gac-----------
---------------------------
function Gas2Gac.PlayerStartFloat(engineId)
    local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
    if clientObject and clientObject.RO then
        local savePos = {pos = clientObject.WorldPos}
        clientObject.RO.Position = Vector3(savePos.pos.x,savePos.pos.y + 3,savePos.pos.z)
        LuaQingming2019Mgr.SaveObjectPos[engineId] = savePos
    end
  end

  function Gas2Gac.PlayerFinishFloat(engineId)
    local clientObject = CClientObjectMgr.Inst:GetObject(engineId)
    if clientObject and clientObject.RO and LuaQingming2019Mgr.SaveObjectPos[engineId] then
        clientObject.RO.Position = LuaQingming2019Mgr.SaveObjectPos[engineId].pos
    end
  end




-- 同步整个玩法
function Gas2Gac.SyncMengHuaLuPlay(data_U)

	-- 清除boss延迟掉落的显示
	if LuaMengHuaLuMgr.m_BossDropTick then
		UnRegisterTick(LuaMengHuaLuMgr.m_BossDropTick)
		LuaMengHuaLuMgr.m_BossDropTick = nil
	end
	g_ScriptEvent:BroadcastInLua("MengHuaLuEnterNewLayer")

	local gridInfo = MsgPackImpl.unpack(data_U)
	LuaMengHuaLuMgr.m_MazeInfos = {}

	if gridInfo.Count > 1 then
		local layer = gridInfo[0]
		LuaMengHuaLuMgr.m_Layer = layer

		-- 提示进入层数
		local layMsg = SafeStringFormat3(LocalString.GetString("梦境%s"), Extensions.ToChinese(LuaMengHuaLuMgr.m_Layer))
		MessageMgr.Inst:ShowMessage("MENGHUALU_Enter_Mengjing", {layMsg})

		local mazeList = gridInfo[1]
		for i = 0, mazeList.Count-1 do
			local info = LuaMengHuaLuMgr.LoadGrid(mazeList[i])
			if info then
				table.insert(LuaMengHuaLuMgr.m_MazeInfos, info)
			end
		end
	end
	g_ScriptEvent:BroadcastInLua("UpdateMazeInfo")
end

-- 同步格子数据
function Gas2Gac.SyncMengHuaLuGridInfo(gridInfo_U)
	local gridInfo = MsgPackImpl.unpack(gridInfo_U)
	if gridInfo.Count > 0 then
		for i = 0, gridInfo.Count-1 do
			local info = LuaMengHuaLuMgr.LoadGrid(gridInfo[i])
			g_ScriptEvent:BroadcastInLua("UpdateSingleMazeItem", info)
		end
	end
end

-- 同步boss掉落
function Gas2Gac.SyncMengHuaLuDropItemInfo(item_U)
	local itemInfo = MsgPackImpl.unpack(item_U)
	-- boss掉落需要延迟(todo是否是boss层)
	if LuaMengHuaLuMgr.IsBossLayer() then

		LuaMengHuaLuMgr.m_BossDropTick = RegisterTickOnce(function ()
			if itemInfo.Count > 0 then
				for i = 0, itemInfo.Count-1 do
					local info = LuaMengHuaLuMgr.LoadDropGrid(itemInfo[i])
					g_ScriptEvent:BroadcastInLua("UpdateSingleMazeItem", info)
				end
			end
			LuaMengHuaLuMgr.m_BossDropTick = nil
		end, 3500)
	else
		if itemInfo.Count > 0 then
			for i = 0, itemInfo.Count-1 do
				local info = LuaMengHuaLuMgr.LoadDropGrid(itemInfo[i])
				g_ScriptEvent:BroadcastInLua("UpdateSingleMazeItem", info)
			end
		end
	end
end

-- 玩家二次确认
function Gas2Gac.RequestStartMengHuaLuResult(layer)

	local msg = g_MessageMgr:FormatMessage("SANJIEMENGHUALU_GONO_OR_NOT", layer)
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
		Gac2Gas.RequestStartMengHuaLuWithChoice(false)
	end), DelegateFactory.Action(function()
		local reStartMsg = g_MessageMgr:FormatMessage("MENGHUALU_RESTART_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(reStartMsg, DelegateFactory.Action(function()
			Gac2Gas.RequestStartMengHuaLuWithChoice(true)
		end), DelegateFactory.Action(function ()
			CUIManager.CloseUI(CLuaUIResources.MengHuaLuWnd)
		end), nil, nil, false)
	end), nil, nil, false)
end

----------------------------- buff 气场 -----------------------------

-- 同步buff数据
function Gas2Gac.SynMengHuaLuBuffData(grid, data_U)
	local buffList = MsgPackImpl.unpack(data_U)
	if grid == 0 then
		LuaMengHuaLuMgr.m_BabyInfo.BuffDataList = buffList
		g_ScriptEvent:BroadcastInLua("MengHuaLuUpdateBabyBuffs", buffList)
	else
		if LuaMengHuaLuMgr.m_MazeInfos[grid] and LuaMengHuaLuMgr.m_MazeInfos[grid].MonsterInfo then
			LuaMengHuaLuMgr.m_MazeInfos[grid].MonsterInfo.BuffData = buffList
			g_ScriptEvent:BroadcastInLua("MengHuaLuUpdateMonsterBuffs", grid, buffList)
		end
	end
end

-- 气场查询结果
function Gas2Gac.QueryMengHuaLuQiChangResult(qiChangTbl_U)
	local list = MsgPackImpl.unpack(qiChangTbl_U)
	if list.Count > 1 then
		local allQiChang = list[0]
		local activeQiChang = list[1]

		g_ScriptEvent:BroadcastInLua("QueryMengHuaLuQiChangResult", allQiChang, activeQiChang)
	end
end

-- 设置气场成功
function Gas2Gac.RequestMengHuaLuSetQiChangSuccess(data_U)
	local activedQiChangs = MsgPackImpl.unpack(data_U)
	g_ScriptEvent:BroadcastInLua("RequestMengHuaLuSetQiChangSuccess", activedQiChangs)
end

-- 打开商店信息
function Gas2Gac.SyncMengHuaLuShopInfo(data_U)
	local shopInfo = MsgPackImpl.unpack(data_U)

	if shopInfo.Count >= 2 then

		local itemInfo = shopInfo[0]
		local qichangInfo = shopInfo[1]
		if itemInfo.Count <= 0 then
			MessageMgr.Inst:ShowMessage("MENGHUALU_SHOP_EMPTY", {})
			CUIManager.CloseUI(CLuaUIResources.MengHuaLuShopWnd)
		else
			LuaMengHuaLuMgr.OpenMengHuaLuShop(itemInfo, qichangInfo)
		end
	end
end

-- 显示结算界面
function Gas2Gac.MengHuaLuFailed(rank, toralNum, data_U)
	LuaMengHuaLuMgr.ShowResultWnd(rank, toralNum, data_U)
end


----------------------------- 同步属性 -----------------------------

-- 战斗属性
function Gas2Gac.SyncMengHuaLuCharacterFightProp(grid, propKey, propValue)
	if grid == 0 then
		g_ScriptEvent:BroadcastInLua("SyncMengHuaLuCharacterFightProp", grid, propKey, propValue)
	else
		if LuaMengHuaLuMgr.m_MazeInfos[grid] and LuaMengHuaLuMgr.m_MazeInfos[grid].MonsterInfo then
			if propKey == 1 then
				-- 攻击
				LuaMengHuaLuMgr.m_MazeInfos[grid].MonsterInfo.Attack = propValue
			elseif propKey == 5 then
				-- 生命
				LuaMengHuaLuMgr.m_MazeInfos[grid].MonsterInfo.Hp = propValue
			end
		end
		g_ScriptEvent:BroadcastInLua("SyncMengHuaLuCharacterFightProp", grid, propKey, propValue)
	end

end

-- 血量变化
function Gas2Gac.ShowCharacterHpChange(grid, hpChange)
	-- monster飘字，玩家刷新
	g_ScriptEvent:BroadcastInLua("ShowCharacterHpChange", grid, hpChange)

end

-- 同步金币
function Gas2Gac.SyncMengHuaLuCharacterGold(grid, gold)
	g_ScriptEvent:BroadcastInLua("SyncMengHuaLuCharacterGold", grid, gold)
end


-- 同步新的轮次
function Gas2Gac.EnterMengHuaLuNewRound(round)
	--g_ScriptEvent:BroadcastInLua("EnterMengHuaLuNewRound", round)
end

----------------------------- 技能 -----------------------------

-- 技能查询回调
function Gas2Gac.QueryMengHuaLuSkillResult(skillTbl_U)
	local list = MsgPackImpl.unpack(skillTbl_U)

	local skillList = {}
	for i = 0, list.Count-1 do
		local skillInfo = list[i]
		local skillId = skillInfo[0]
		local count = skillInfo[1]
		local mp = skillInfo[2]

		table.insert(skillList, {
			skillId = skillId,
			num = count,
			comsume = mp,
			})
	end
	g_ScriptEvent:BroadcastInLua("QueryMengHuaLuSkillResult", skillList)
end

-- 技能个数
function Gas2Gac:SyncMengHuaLuSkillNum(count)
	g_ScriptEvent:BroadcastInLua("SyncMengHuaLuSkillNum", count)
end

-- 开始放技能
function Gas2Gac.CastMengHuaLukillBegin(grid, skillId)

end

-- 释放技能结果
function Gas2Gac.CastMengHuaLukillEnd(attackerGrid, skillId, hitGrid_U, missGrid_U)
	local skill = MengHuaLu_Skill.GetData(skillId)
	-- 无视某些技能的处理
	if not skill or skill.IgnoreExpression == 1 then return end

	local hitList = MsgPackImpl.unpack(hitGrid_U)
	local missList = MsgPackImpl.unpack(missGrid_U)

	local hitStr = ""
	for i = 0, hitList.Count-1 do
		hitStr = hitStr..", "..tostring(hitList[i])
	end

	local missStr = ""
	for i = 0, missList.Count-1 do
		missStr = missStr..", "..tostring(missList[i])
	end
	-- 根据技能类型来广播
	local skill = MengHuaLu_Skill.GetData(skillId)
	if not skill then return end

	--  如果attacker是宝宝
	if attackerGrid == 0 then
		-- 宝宝的攻击动作
		local action = {SrcGridId = attackerGrid,
			DestGridId = -1,
			SkillId = skillId,
			Interval = 0.8,
			IsMissed = false,
		}
		g_ScriptEvent:BroadcastInLua("AddBabyAction", action)

		for i = 0, hitList.Count-1 do

			local monsterAction = {
				SrcGridId = -1,
				DestGridId = hitList[i],
				SkillId = skillId,
				Interval = 0.8, -- 间隔时间
				IsMissed = false,
			}
			g_ScriptEvent:BroadcastInLua("AddMonsterAction", monsterAction)
		end

		for i = 0, missList.Count-1 do

			local monsterAction = {
				SrcGridId = -1,
				DestGridId = missList[i],
				SkillId = skillId,
				Interval = 0.8, -- 间隔时间
				IsMissed = true,
			}
			g_ScriptEvent:BroadcastInLua("AddMonsterAction", monsterAction)
		end

	else
		-- attacker是怪物
		local attackAction = {SrcGridId = attackerGrid,
			DestGridId = -1,
			SkillId = skillId,
			Interval = 0.8,
			IsMissed = false,
		}
		g_ScriptEvent:BroadcastInLua("AddMonsterAction", attackAction)

		for i = 0, hitList.Count-1 do
			if hitList[i] == 0 then
				if skillId == 40019 or skillId == 40112 then
					local babyAction = {SrcGridId = -1,
						DestGridId = hitList[i],
						SkillId = skillId,
						Interval = 0.02,
						IsMissed = false,
					}
					g_ScriptEvent:BroadcastInLua("AddBabyAction", babyAction)
				else
					local babyAction = {SrcGridId = -1,
						DestGridId = hitList[i],
						SkillId = skillId,
						Interval = 0.8,
						IsMissed = false,
					}
					g_ScriptEvent:BroadcastInLua("AddBabyAction", babyAction)
				end
			else
				local monsterAction = {
					SrcGridId = -1,
					DestGridId = hitList[i],
					SkillId = skillId,
					Interval = 0.8, -- 间隔时间
					IsMissed = false,
				}
				g_ScriptEvent:BroadcastInLua("AddMonsterAction", monsterAction)
			end
		end

		for i = 0, missList.Count-1 do
			if missList[i] ~= 0 then
				local monsterAction = {
					SrcGridId = -1,
					DestGridId = missList[i],
					SkillId = skillId,
					Interval = 0.8, -- 间隔时间
					IsMissed = true,
				}
				g_ScriptEvent:BroadcastInLua("AddMonsterAction", monsterAction)
			end
			-- 攻击宝宝miss目前没有处理
		end
	end
end

function Gas2Gac.SyncMengHuaLuCharacterDie(grid, skillId)
	if grid ~= 0 then
		-- 怪物不是被技能打死的，直接刷新
		g_ScriptEvent:BroadcastInLua("SyncMengHuaLuCharacterDie", grid)
	end
end

function Gas2Gac.ConfirmTeleportPublicCopy(sceneTemplateId, sceneIds, showRandomButton, scenePlayerCountLimit)
    --格式： sceneIdx1, sceneId1, playerNum1, sceneIdx2, sceneId2, playerNum2 ...
    --showRandomButton: 0-不显示“进入任一副本” 1-显示“进入任一副本”
    --scenePlayerCountLimit: 场景人数上限
    local sceneIdList = MsgPackImpl.unpack(sceneIds)
    local n = sceneIdList.Count
    local map = PublicMap_PublicMap.GetData(sceneTemplateId)
    if not map then return end
    if n % 3 ~= 0 then return end
    local scenes = {}
    for i=0,n-1,3 do
        local sceneInfo = {}
        sceneInfo.index = tonumber(sceneIdList[i]) --序号
        sceneInfo.sceneName = map.Name
        sceneInfo.sceneId = tostring(sceneIdList[i + 1])
        sceneInfo.playerNum = tonumber(sceneIdList[i + 2])
        sceneInfo.maxPlayerNum = tonumber(scenePlayerCountLimit)
        table.insert(scenes, sceneInfo)
    end
    --按序号从小到大排列
    table.sort(scenes, function(a,b)
        return a.index<b.index
    end)

    LuaCopySceneChosenMgr.Show(sceneTemplateId, scenes, showRandomButton ~= 0)
end

local Word_AutoXiLianWord = import "L10.Game.Word_AutoXiLianWord"
function Gas2Gac.QueryAutoBaptizeTargetWordCondListResult(place, pos, equipId, idListUD)

    LuaAutoBaptizeNecessaryConditionMgr.m_canGetWordCondsList = {}
    local idList = MsgPackImpl.unpack(idListUD)
    if not idList then return end
    for i = 0, idList.Count - 1 do
        local id = idList[i]
        LuaAutoBaptizeNecessaryConditionMgr.m_canGetWordCondsList[id] = Word_AutoXiLianWord.GetData(id)
    end

    CUIManager.ShowUI(CLuaUIResources.EquipWordAutoBaptizeTargetWnd)
    CUIManager.CloseUI(CUIResources.EquipWordAutoBaptizeConditionWnd)
end

function Gas2Gac.SHTYSyncStatusToPlayersInPlay(playStatus, enterStateTime, extraData)
    LuaTaoQuanMgr.PlayState = playStatus

    local extraDataDic = MsgPackImpl.unpack(extraData)
    if extraDataDic then
      LuaTaoQuanMgr.Score = extraDataDic.Score
      LuaTaoQuanMgr.ScoreDetails = extraDataDic.ScoreDetails
      LuaTaoQuanMgr.ThrowTimes = LuaTaoQuanMgr.TotalQuanNum - extraDataDic.ThrowTimes
      LuaTaoQuanMgr.RingPos = extraDataDic.RingPos--x,y
    end
      g_ScriptEvent:BroadcastInLua("UpdateTaoQuanInfo")

    if not CUIManager.IsLoaded(CUIResources.TaoQuanWnd) and playStatus > 0 then
      local gamePlayId = CScene.MainScene.GamePlayDesignId
      if gamePlayId == 51100872 then
        LuaTaoQuanMgr.GameType = 1
      elseif gamePlayId == 51101145 then
        LuaTaoQuanMgr.GameType = 2
      end
      CUIManager.ShowUI(CUIResources.TaoQuanWnd)
    end
  end


-- 同步单个玩家的得分信息
function Gas2Gac.DuanWuDZZ_SendPlayerPlayInfoToClient(playerScore, extra)
	-- playerScore: 玩家得分
    -- extra: 额外参数，string(用时)
    local playTime=tonumber(extra)
    CLuaDuanWuDaZuoZhanMgr.m_PlayScore=playerScore
	CLuaDuanWuDaZuoZhanMgr.m_PlayTime=playTime
	g_ScriptEvent:BroadcastInLua("DuanWuDZZ_SendPlayerPlayInfoToClient",playerScore,playTime)
end

-- 同步玩法结算信息
function Gas2Gac.DuanWuDZZ_SendPlayInfoToClient(infosUd, playTimeCount, teamScore, mvpPlayerId, extra)
	-- infosUd: 玩法结算信息
	-- extra: 额外参数，string(用时)

	-- infos = {
	--   {id, name, class, gender, score},
	--   {id, name, class, gender, score},
	--   ...
    -- }
	CLuaDuanWuDaZuoZhanMgr.m_PlayTimeCount=playTimeCount
	CLuaDuanWuDaZuoZhanMgr.m_TeamScore=teamScore
	CLuaDuanWuDaZuoZhanMgr.m_MvpPlayerId=mvpPlayerId

    local playerInfos={}
    local infos = MsgPackImpl.unpack(infosUd)
    for i=1,infos.Count do
        local list=infos[i-1]
        table.insert( playerInfos,{id=list[0],name=list[1],class=list[2],gender=list[3],score=list[4]} )
	end
    CLuaDuanWuDaZuoZhanMgr.m_PlayerInfos=playerInfos

	CUIManager.ShowUI("DuanWuDaZuoZhanResultWnd")
end

-- 同步排行榜数据
function Gas2Gac.DuanWuDZZ_SendOrderedRankListToClient(orderedRankListUd, extra)
	-- orderedRankListUd: 排行榜信息
	-- extra: 额外参数

	-- orderedRankListUd = {
	--   {playerId, name, score, duration, timestamp},
	--   {playerId, name, score, duration, timestamp},
	--   ...
	-- }

	local orderedRankList = MsgPackImpl.unpack(orderedRankListUd)

    local playerInfos={}
	for i=0, orderedRankList.Count-1 do
		local info = orderedRankList[i]
        table.insert( playerInfos,{playerId=info[0],name=info[1],score=info[2],duration=info[3],timestamp=info[4]} )
    end
    CLuaDuanWuDaZuoZhanMgr.m_RankPlayerInfos=playerInfos
	g_ScriptEvent:BroadcastInLua("DuanWuDZZ_SendOrderedRankListToClient",playerInfos)
end



function Gas2Gac.RequestCanInlayStoneOnEquipmentResult(can,equipId,equipNewGrade, setIndex)
    if not can then return end
	g_ScriptEvent:BroadcastInLua("RequestCanInlayStoneOnEquipmentResult",can,equipId,equipNewGrade, setIndex)

end
function Gas2Gac.RequestCanRemoveStoneOnEquipmentResult(can, equipId, removeStoneItemId, costYuanBao, newItemData, equipNewGrade, setIndex)
    if not can then
        return
    end

    if CClientMainPlayer.Inst == nil then
        return
    end

    local playerLevel = CClientMainPlayer.Inst.MaxLevel

    local equipItem = CItemMgr.Inst:GetById(equipId)
    local level = equipItem.Equip.Grade

    local newItemInfo = MsgPackImpl.unpack(newItemData)
    if newItemInfo.Count>0 then
        local newItemId = math.floor(tonumber(newItemInfo[0] or 0))
        local newIitemCount = math.floor(tonumber(newItemInfo[1] or 0))
        local additionalId = 0
        local additionalCount = 0
        if newItemInfo.Count >= 4 then
            additionalId = math.floor(tonumber(newItemInfo[2] or 0))
            additionalCount = math.floor(tonumber(newItemInfo[3] or 0))
        end
        if equipNewGrade > level then
            if equipNewGrade > playerLevel then
                --JEWEL_DEMOUNT_CONFIRM_LEVEL_OVER
                local message = g_MessageMgr:FormatMessage("JEWEL_DEMOUNT_CONFIRM_LEVEL_OVER", equipItem.Equip.ColoredDisplayName, equipNewGrade)
                --
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                    CLuaEquipMgr.CanRemoveStoneOnEquipment(equipId, removeStoneItemId, costYuanBao, newItemId, newIitemCount, additionalId, additionalCount)
                end), nil, nil, nil, false)
            else
                --JEWEL_DEMOUNT_CONFIRM
                local message = g_MessageMgr:FormatMessage("JEWEL_DEMOUNT_CONFIRM", equipItem.Equip.ColoredDisplayName, equipNewGrade)
                --
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
                CLuaEquipMgr.CanRemoveStoneOnEquipment(equipId, removeStoneItemId, costYuanBao, newItemId, newIitemCount, additionalId, additionalCount)
                end), nil, nil, nil, false)
            end
        else
            CLuaEquipMgr.CanRemoveStoneOnEquipment(equipId, removeStoneItemId, costYuanBao, newItemId, newIitemCount, additionalId, additionalCount)
        end
    else
        -- Gac2Gas.RemoveStoneOnEquipment(EnumToInt(info.place), info.pos, info.itemId, holeInfo.holeIndex, CLuaCheckGemGroupWnd.m_HoleSetIndex)
        CLuaEquipMgr.CanRemoveStoneOnEquipment(equipId, removeStoneItemId, costYuanBao, 0, 0, 0, 0)
    end
end

function Gas2Gac.RequestCanUpgradeEquipStoneResult(can, equipId, equipNewGrade, setIndex)
    if not can then return end
    EventManager.BroadcastInternalForLua(EnumEventType.RequestCanUpgradeEquipStoneResult, {can,equipId,equipNewGrade,setIndex})
end

function Gas2Gac.RequestCanReferenceStoneOnEquipmentResult(can, equipId, equipNewGrade)
	g_ScriptEvent:BroadcastInLua("RequestCanReferenceStoneOnEquipmentResult",can, equipId, equipNewGrade )
end

function Gas2Gac.RequestCanSwitchEquipHoleSetReturn(place, pos, equipId, equipNewGrade, canSwitch)
	g_ScriptEvent:BroadcastInLua("RequestCanSwitchEquipHoleSetReturn",place, pos, equipId, equipNewGrade, canSwitch )
end

function Gas2Gac.EquipSuitFxIndexUpdate(fxIndex)
	CClientMainPlayer.Inst.ItemProp.IntesifySuitFxIndex = fxIndex
	EventManager.BroadcastInternalForLua(EnumEventType.EquipmentSuitUpdate, {CClientMainPlayer.Inst})
end


function Gas2Gac.QueryPlayerCapacityResult(targetId,capability,detail)
    if not CClientMainPlayer.Inst then return end

    if targetId == CClientMainPlayer.Inst.Id then
        CClientMainPlayer.Inst.PlayProp.PlayerCapacity = capability
    end

    local detailData = MsgPackImpl.unpack(detail)
    CLuaPlayerCapacityMgr.UpdatePlayerCapacity(targetId,capability, detailData)

	g_ScriptEvent:BroadcastInLua("UpdatePlayerCapacity", targetId,capability,detailData)

end


function Gas2Gac.OpenDouDiZhuWnd(index, isWatch, eType, isRemote, ownerId, extraData_U)
    CLuaDouDiZhuMgr.s_GameData = nil
    CLuaDouDiZhuMgr.m_IsRemote=isRemote
	CLuaDouDiZhuMgr.m_OwnerId = ownerId

	CLuaDouDiZhuMgr.m_PlayerIndex=index
	CLuaDouDiZhuMgr.m_IsWatch=isWatch

	CLuaDouDiZhuMgr.m_Type=eType

	CLuaDouDiZhuMgr.m_Score=0
	CLuaDouDiZhuMgr.m_Power=0

	CLuaDouDiZhuMgr.m_IsJiaoDiZhu=false
	CLuaDouDiZhuMgr.CancleTick()
	CLuaDouDiZhuMgr.UpdateRemoteDouDiZhuApplyAlert(false)

	if CLuaDouDiZhuMgr.m_ResetTick then
		UnRegisterTick(CLuaDouDiZhuMgr.m_ResetTick)
		CLuaDouDiZhuMgr.m_ResetTick=nil
	end
	if CLuaDouDiZhuMgr.m_ShowResultTick then
		UnRegisterTick(CLuaDouDiZhuMgr.m_ShowResultTick)
		CLuaDouDiZhuMgr.m_ShowResultTick=nil
	end

	CLuaDouDiZhuCardMgr.Init()

	local extra = MsgPackImpl.unpack(extraData_U)
    CLuaDouDiZhuMgr.m_Round = 0
    if eType == EnumDouDiZhuType.eRemoteQiangXiangZi then
        LuaDouDiZhuBaoXiangMgr:SyncChestCount(extra)
	elseif eType ~= EnumDouDiZhuType.eCrossPlay then
		if extra and extra.Count > 0 then
			CLuaDouDiZhuMgr.m_Round = extra[0]
		end
	else
		if extra and extra.Count > 0 then
			CLuaDouDiZhuMgr.m_RoundName = extra[0] or ""
		end
	end

	CLuaDouDiZhuMgr.m_Play=CDouDiZhuBoard:new()
	CLuaDouDiZhuMgr.m_CardTemplate=nil

	CLuaDouDiZhuMgr.m_ShowRoundLeftTime=false
	CLuaDouDiZhuMgr.m_RoundLeftTime=0
	CLuaDouDiZhuMgr.m_RoundTime=0

	CUIManager.CloseUI(CLuaUIResources.CrossDouDiZhuRankResultWnd)
	CUIManager.CloseUI(CLuaUIResources.CrossDouDiZhuResultWnd)
	CUIManager.CloseUI(CLuaUIResources.DouDiZhuGuildResultWnd)
	CUIManager.CloseUI(CLuaUIResources.DouDiZhuHouseResultWnd)
	CUIManager.CloseUI(CLuaUIResources.DouDiZhuBaoXiangResultWnd)

	if not CUIManager.IsLoaded(CLuaUIResources.MiniDouDiZhuWnd) then
		CUIManager.CloseUI(CUIResources.PackageWnd)
		CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
		CUIManager.ShowUI(CUIResources.DouDiZhuWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.MiniDouDiZhuWnd)
	end
end

function Gas2Gac.CloseDouDiZhuWnd()
	-- print("closedoudizhuwnd")
	CLuaDouDiZhuMgr.m_Play=nil
	CLuaDouDiZhuMgr.m_CardTemplate=nil

	CLuaDouDiZhuCardMgr.m_SelectedCardList={}
	CLuaDouDiZhuCardMgr.m_AllCards={}--dic
	CLuaDouDiZhuCardMgr.m_CardsLookup={}--dic
	CLuaDouDiZhuCardMgr.m_ShowCardsTf=nil
	CLuaDouDiZhuCardMgr.m_HandCardsTf=nil

	CUIManager.CloseUI(CUIResources.DouDiZhuWnd)
	CUIManager.CloseUI(CLuaUIResources.MiniDouDiZhuWnd)
end

-- bCanSignUp, 当前可否报名, 是否报名阶段
-- bSignUp, 是否已报名, 用于显示报名按钮的状态
-- bFight, 是否显示出战玩家
function Gas2Gac.QuerySignUpOrChoosedGuildDouDiZhuPlayersResult(bCanSignUp, bSignUp, bFight, resultTbl_U)
	-- print("bCanSignUp", bCanSignUp)
	-- print("bSignUp", bSignUp)
	-- print("bFight", bFight)
	CLuaDouDiZhuMgr.m_CanSignUp = bCanSignUp
	CLuaDouDiZhuMgr.m_SignUp = bSignUp
	CLuaDouDiZhuMgr.m_ShowFight = bFight

	CLuaDouDiZhuMgr.m_PraiseNum=0
	CLuaDouDiZhuMgr.m_FightNum=0

	local list = MsgPackImpl.unpack(resultTbl_U)
	if not list then return end

	CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo={}
	for i = 0, list.Count - 1, 10 do
		local bFight = list[i + 8]
		if bFight then
			CLuaDouDiZhuMgr.m_FightNum = CLuaDouDiZhuMgr.m_FightNum + 1
		end

		local bVote = list[i + 9]
		if bVote then
			CLuaDouDiZhuMgr.m_PraiseNum = CLuaDouDiZhuMgr.m_PraiseNum + 1
		end

		table.insert(CLuaDouDiZhuMgr.m_GuildSignupPlayerInfo, {
			id = list[i],
			name = list[i + 1],
			class = list[i + 2],
			gender = list[i + 3],
			grade = list[i + 4],
			voteNum = list[i + 5],
			fightTimes = list[i + 6],
			maxRankRecord = list[i + 7],
			bFight = list[i + 8],
			bVote = list[i + 9],
		})
	end

	CUIManager.ShowUI(CUIResources.DouDiZhuGuildChooseWnd)
end

function Gas2Gac.SyncDouDiZhuPlayerInfo(info_U)
	if not CLuaDouDiZhuMgr.m_Play then return end

	local infoList = MsgPackImpl.unpack(info_U)
	if not infoList then return end

	local index = infoList[0]
	local id = infoList[1]
	local name = infoList[2]
	local class = infoList[3]
	local gender = infoList[4]
	local grade = infoList[5]
	local score = infoList[6]
	local ready = infoList[7]
	local qiangDiZhuStatus = infoList[8]
	local qiangDiZhuTimes = infoList[9]
	local isDiZhu = infoList[10]
	local mingPai = infoList[11]
	local pass = infoList[12]
	local chuPaiTimes = infoList[13]
	local tuoGuan = infoList[14]
	local usedTime = infoList[15]

	CLuaDouDiZhuMgr.m_Play:SetIndexInfo(index, id, name, class, gender, grade,
		score, ready, qiangDiZhuStatus, qiangDiZhuTimes, isDiZhu, mingPai,
		0, {}, {}, pass, chuPaiTimes, tuoGuan, usedTime)

	-- print("SyncDouDiZhuPlayerInfo", index, id, name, class, gender, grade, score)

	g_ScriptEvent:BroadcastInLua("SyncDouDiZhuPlayerInfo",index,CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index))

end

function Gas2Gac.DiZhuAddReserveCards(index, playerId, reserveCards_U, handCardsCount, handCards_U)
	local list1 = MsgPackImpl.unpack(handCards_U)
	local list2 = MsgPackImpl.unpack(reserveCards_U)
	local handCards={}
	local reserveCards={}
	for i = 0, list1.Count - 1, 1 do
		table.insert( handCards,list1[i] )
	end
	for i = 0, list2.Count - 1, 1 do
		table.insert( reserveCards,list2[i] )
	end
	local t=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
	t.handCardsCount=handCardsCount
	t.handCards=handCards
	-- print("DiZhuAddReserveCards", handCardsCount,t)

	--抢地主index置0
	CLuaDouDiZhuMgr.m_Play.m_QiangDiZhuIndex = 0
	g_ScriptEvent:BroadcastInLua("DiZhuAddReserveCards",index,t)

end

function Gas2Gac.SyncDouDiZhuCardsInfo(index, playerId, handCardsCount, handCards_U, showCardsCount, showCards_U, bFaPai)
	local list1 = MsgPackImpl.unpack(handCards_U)
	local list2 = MsgPackImpl.unpack(showCards_U)
	local handCards={}
	local showCards={}
	for i = 0, list1.Count - 1, 1 do
		table.insert( handCards,list1[i] )
	end
	for i = 0, list2.Count - 1, 1 do
		table.insert( showCards,list2[i] )
	end
	local t=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
	t.handCardsCount=handCardsCount
	t.handCards=handCards
	t.showCards=showCards

	-- print("SyncDouDiZhuCardsInfo", index, bFaPai)
	g_ScriptEvent:BroadcastInLua("SyncDouDiZhuCardsInfo",index,CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index),bFaPai)
end

function Gas2Gac.ShowDouDiZhuHandCards(index, playerId, handCardsCount, handCards_U)
	local list = MsgPackImpl.unpack(handCards_U)
	local handCards={}
	for i = 0, list.Count - 1, 1 do
		table.insert( handCards,list[i] )
	end
	if CLuaDouDiZhuMgr.m_Play then
		local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
		if info then
			info.handCards=handCards
		end
	end
	-- print("ShowDouDiZhuHandCards", index)
end

function Gas2Gac.PlayerDouDiZhuReady(index, playerId, ready)
	local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
	if info then
		info.ready=ready
	end

	if ready then
		local viewIndex=CLuaDouDiZhuMgr.SwitchViewChairId(index)
		if viewIndex==1 then
			CLuaDouDiZhuMgr.CancleAutoContinueCountdown()
		end
	end

	g_ScriptEvent:BroadcastInLua("PlayerDouDiZhuReady",index,playerId,ready)
end

function Gas2Gac.PlayerQiangDiZhu(index, playerId, status, bEnd, diZhuIndex, diZhuPlayerId)
	-- print("PlayerQiangDiZhu", index, playerId, status, bEnd)
	if bEnd then
		local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(diZhuIndex)
		if info then
			info.isDiZhu=true
		end
	end
	local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
	if info then
		info.qiangDiZhuStatus=status
	end

	g_ScriptEvent:BroadcastInLua("PlayerQiangDiZhu",index,status, bEnd, diZhuIndex)
end

function Gas2Gac.PlayerMingPai(index, playerId, handCardsCount, handCards_U)
	-- print("PlayerMingPai",index)
	local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
	if info then
		info.mingPai=true
		info.handCardsCount=handCardsCount

		local cards={}
		local list = MsgPackImpl.unpack(handCards_U)
		for i = 0, list.Count - 1,1 do
			table.insert( cards,list[i] )
		end
		info.handCards=cards

		g_ScriptEvent:BroadcastInLua("PlayerMingPai",index)
	end
end

function Gas2Gac.SyncQiangDiZhuProgress(index, playerId, bIsJiaoDiZhu, leftTime)
	-- print("SyncQiangDiZhuProgress", index, playerId, bIsJiaoDiZhu, leftTime)

	CLuaDouDiZhuMgr.m_Play.m_QiangDiZhuIndex=index
	CLuaDouDiZhuMgr.m_IsJiaoDiZhu=bIsJiaoDiZhu

	CLuaDouDiZhuMgr.UpdateTick(leftTime)

	g_ScriptEvent:BroadcastInLua("SyncQiangDiZhuProgress",index,bIsJiaoDiZhu, leftTime)
end

function Gas2Gac.SyncChuPaiProgress(index, playerId, leftTime, maxCardIndex)
	-- print("SyncChuPaiProgress", index, playerId, leftTime, maxCardIndex)
	if not CLuaDouDiZhuMgr.m_Play then
		return
	end

	CLuaDouDiZhuMgr.m_Play.m_ChuPaiIndex=index
	CLuaDouDiZhuMgr.m_Play.m_MaxCardIndex=maxCardIndex
	CLuaDouDiZhuMgr.UpdateTick(leftTime)

	g_ScriptEvent:BroadcastInLua("SyncChuPaiProgress",index,leftTime, maxCardIndex)
end

function Gas2Gac.PlayerChuPai(index, playerId, showCardsCount, showCards_U, usedTime)
	if not CLuaDouDiZhuMgr.m_Play then return end

	local list = MsgPackImpl.unpack(showCards_U)
	if not list then return end
	local cards={}
	for i = 0, list.Count - 1,1 do
		table.insert( cards,list[i] )
	end
	CLuaDouDiZhuMgr.m_Play:DoChuPai(index,cards)
	-- print("PlayerChuPai", index, showCardsCount, usedTime)
	g_ScriptEvent:BroadcastInLua("PlayerChuPai",index)
end

function Gas2Gac.SyncDuZhuScore(score, power)
	-- print("SyncDuZhuScore", score, power)
	CLuaDouDiZhuMgr.m_Score=score
	CLuaDouDiZhuMgr.m_Power=power
	g_ScriptEvent:BroadcastInLua("SyncDuZhuScore",score,power)
end

function Gas2Gac.SyncDiZhuReserveCards(count, cards_U)
	local list = MsgPackImpl.unpack(cards_U)
	if not list then return end
	local cards={}
	for i = 0, list.Count - 1,1 do
		table.insert( cards,list[i] )
	end

	if count~=#cards then
		for i=1,count do
			table.insert( cards,0 )
		end
	end
	CLuaDouDiZhuMgr.m_Play.m_ReserveCards=cards

	g_ScriptEvent:BroadcastInLua("SyncDiZhuReserveCards")

	-- print("SyncDiZhuReserveCards", count)
end

function Gas2Gac.PlayerDouDiZhuTuoGuan(index, playerId, bTuoGuan)
	-- print("PlayerDouDiZhuTuoGuan", index, playerId, bTuoGuan)

	local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
	if info then
		info.tuoGuan=bTuoGuan
	end
	g_ScriptEvent:BroadcastInLua("PlayerDouDiZhuTuoGuan",index,playerId,bTuoGuan)
end

function Gas2Gac.DouDiZhuStartRound()
    -- print("DouDiZhuStartRound")
    CUIManager.CloseUI(CLuaUIResources.DouDiZhuBaoXiangResultWnd)
	g_ScriptEvent:BroadcastInLua("DouDiZhuStartRound")
end

function Gas2Gac.DouDiZhuEndRound(winnerIndex, winnerPlayerId)
	-- print("DouDiZhuEndRound")
	local CSGas2Gac=import "L10.Game.Gas2Gac"
	if winnerIndex==0 then
		--没有赢家
		CLuaDouDiZhuMgr.m_IsWinner = nil
		return
	end
	if not CLuaDouDiZhuMgr.m_Play then return end

	local playerInfo=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(CLuaDouDiZhuMgr.m_PlayerIndex)
	local winnerInfo=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(winnerIndex)
	local isWinner=winnerInfo.isDiZhu==playerInfo.isDiZhu
	CLuaDouDiZhuMgr.m_IsWinner = isWinner

    --抢箱子不显示特效
    if CLuaDouDiZhuMgr.m_Type~=EnumDouDiZhuType.eRemoteQiangXiangZi then
        if isWinner then
            CSGas2Gac.m_Instance:ShowPlayResultFx(0)--胜利
        else
            CSGas2Gac.m_Instance:ShowPlayResultFx(1)--失败
        end
    end

	g_ScriptEvent:BroadcastInLua("DouDiZhuEndRound",winnerIndex, winnerPlayerId)
end

function Gas2Gac.PlayerLeaveDouDiZhu(index, playerId)
	-- print("PlayerLeaveDouDiZhu", index, playerId)
	if CLuaDouDiZhuMgr.m_Play then
		CLuaDouDiZhuMgr.m_Play.m_IndexInfo[index]=nil
	end
	g_ScriptEvent:BroadcastInLua("PlayerLeaveDouDiZhu",index)

end

function Gas2Gac.ResetDouDiZhuRound()
	-- print("ResetDouDiZhuRound")

	CLuaDouDiZhuMgr.m_ResetTick=RegisterTickOnce(function()
		if CLuaDouDiZhuMgr.m_Play then
			CLuaDouDiZhuMgr.m_Play:ResetRoundData()
		end
		g_ScriptEvent:BroadcastInLua("ResetDouDiZhuRound")
		CLuaDouDiZhuMgr.m_ResetTick=nil
	end,CLuaDouDiZhuMgr.m_DelayShowResult)
end

function Gas2Gac.ShowPlayerDouDiZhuShortMessage(index, playerId, id)
	-- print("ShowPlayerDouDiZhuShortMessage", index, playerId, id)
	g_ScriptEvent:BroadcastInLua("ShowPlayerDouDiZhuShortMessage",index,id)
end

function Gas2Gac.ShowDouDiZhuRoundLeftTime(bShow, leftTime, time)
	-- print("ShowDouDiZhuRoundLeftTime", bShow, leftTime, time)
	CLuaDouDiZhuMgr.m_ShowRoundLeftTime=bShow
    CLuaDouDiZhuMgr.m_RoundLeftTime=leftTime
	CLuaDouDiZhuMgr.m_RoundTime=time

	g_ScriptEvent:BroadcastInLua("ShowDouDiZhuRoundLeftTime",bShow, leftTime, time)
end

function Gas2Gac.UpdateDouDiZhuPlayerScore(index, playerId, score)
	-- print("UpdateDouDiZhuPlayerScore", index, playerId, score)
	local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(index)
	if info then
		info.score=score
	end
	g_ScriptEvent:BroadcastInLua("UpdateDouDiZhuPlayerScore",index,score)
end

function Gas2Gac.SyncDouDiZhuBoardResult(resultTbl_U)
	local list = MsgPackImpl.unpack(resultTbl_U)
	if not list then return end

	CLuaDouDiZhuMgr.m_Result={}
	for i = 0, list.Count - 1, 7 do
		local id = list[i]
		local name = list[i + 1]
		local isDiZhu = list[i + 2]
		local duZhuScore = list[i + 3]
		local duZhuPower = list[i + 4]
		local usedTime = list[i + 5]
		local score = list[i + 6]
		table.insert( CLuaDouDiZhuMgr.m_Result, {
			id=id,
			name=name,
			isDiZhu=isDiZhu,
			duZhuScore=duZhuScore,
			duZhuPower=duZhuPower,
			usedTime=usedTime,
			score=score})
	end

	local myScore = 0
	local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
	for i,v in ipairs(CLuaDouDiZhuMgr.m_Result) do
		if v.id == myId then
			myScore=v.score
			break
		end
	end

	-- eUnknown = 0,
	-- eGuildFightPre = 1,
	-- eGuildFightFinal = 2,
	-- eGuildPlay = 3,
    -- eJiaYuanPlay = 4,
	if CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eRemoteQiangXiangZi then

    elseif CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eJiaYuanPlay
		or CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eRemoteJiaYuanPlay
		or CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildPlay then--家园
		CLuaDouDiZhuMgr.m_ShowResultTick=RegisterTickOnce(function()
			if CLuaDouDiZhuMgr.m_Result then
				if CLuaDouDiZhuMgr.m_IsRemote then
					CLuaDouDiZhuMgr.StartContinueCountdown(5)--5秒倒计时
				end

				if not CUIManager.IsLoaded(CLuaUIResources.MiniDouDiZhuWnd) then
					CUIManager.ShowUI(CLuaUIResources.DouDiZhuHouseResultWnd)
				else
					if myScore>0 then
						g_MessageMgr:ShowMessage("RemoteDouDiZhu_Success",myScore)
					elseif myScore<0 then
						g_MessageMgr:ShowMessage("RemoteDouDiZhu_Fail",myScore)
					end
					g_ScriptEvent:BroadcastInLua("RemoteDouDiZhuShowResult")
				end
			end
			CLuaDouDiZhuMgr.m_ShowResultTick=nil
		end,CLuaDouDiZhuMgr.m_DelayShowResult)
	else
		CLuaDouDiZhuMgr.m_ShowResultTick=RegisterTickOnce(function()
			if CLuaDouDiZhuMgr.m_Result then
				CUIManager.ShowUI(CLuaUIResources.DouDiZhuGuildResultWnd)
			end
			CLuaDouDiZhuMgr.m_ShowResultTick=nil
		end,CLuaDouDiZhuMgr.m_DelayShowResult)
	end
end

function Gas2Gac.QueryGuildDouDiZhuFightRankResult(playerId, name, score, rank, resultTbl_U, stage, round, bFinalBroadcast)

	local list = MsgPackImpl.unpack(resultTbl_U)
	if not list then return end

	CLuaDouDiZhuMgr.m_FinalBroadcast = bFinalBroadcast
	CLuaDouDiZhuMgr.m_DouDiZhuStage=stage
	CLuaDouDiZhuMgr.m_DouDiZhuRound=round

	CLuaDouDiZhuMgr.m_MainPlayerRankData = {
		Name = name,
		Value = score,
		Rank = rank,
	}
	CLuaDouDiZhuMgr.m_RankDataList = {}

	for i = 0, list.Count - 1, 4 do
		local id = list[i]
		local name = list[i + 1]
		local score = list[i + 2]
		local guildName = list[i + 3]
		local rank = math.floor(i/4) + 1

		CLuaDouDiZhuMgr.m_RankDataList[rank] = {
			Id = id,
			Name = name,
			Value = score,
			Rank = rank,
			GuildName = guildName,
		}
	end
	-- 修改为gameplayrank2
	CUIManager.ShowUI(CLuaUIResources.DouDiZhuRankWnd)
end

function Gas2Gac.QueryGuildDouDiZhuChampionRecordResult(resultTbl_U)
	-- print("QueryGuildDouDiZhuChampionRecordResult")
	local list = MsgPackImpl.unpack(resultTbl_U)
	if not list then return end

	CLuaDouDiZhuMgr.m_ChampionRecord = {}

	for i = 0, list.Count - 1, 6 do
		local index = list[i]
		local guildId = list[i + 1]
		local guildName = list[i + 2]
		local id = list[i + 3]
		local name = list[i + 4]
		local class = list[i + 5]
		table.insert(CLuaDouDiZhuMgr.m_ChampionRecord, {
			Index = index,
			GuildId = guildId,
			GuildName = guildName,
			PlayerId = id,
			Name = name,
			Class = class
			})
	end
	g_ScriptEvent:BroadcastInLua("QueryGuildDouDiZhuChampionRecordResult")
end

function Gas2Gac.QueryDouDiZhuPlayersForWatchResult(eType, resultTbl_U)
	local list = MsgPackImpl.unpack(resultTbl_U)
	if not list then return end

	CLuaDouDiZhuMgr.m_WatchPlayers={}
	for i = 0, list.Count - 1, 5 do
		local index = list[i]
		local id = list[i + 1]
		local name = list[i + 2]
		local class = list[i + 3]
		local gender = list[i + 4]
		table.insert( CLuaDouDiZhuMgr.m_WatchPlayers,{index, id, name, class, gender} )
	end

	if #CLuaDouDiZhuMgr.m_WatchPlayers>0 then
		CUIManager.ShowUI(CUIResources.DouDiZhuPickPlayerToWatchWnd)
	end
end

function Gas2Gac.SignUpGuildDouDiZhuSuccess()
end

function Gas2Gac.VoteGuildDouDiZhuSuccess(votePlayerId)
	g_ScriptEvent:BroadcastInLua("VoteForGuildDouDiZhuSuccess", votePlayerId)
end

function Gas2Gac.CancelVoteGuildDouDiZhuSuccess(votePlayerId)
	g_ScriptEvent:BroadcastInLua("CancelVoteForGuildDouDiZhuSuccess", votePlayerId)
end

function Gas2Gac.ApplyJoinRemoteDouDiZhuPlayerListAlert()
	CLuaDouDiZhuMgr.UpdateRemoteDouDiZhuApplyAlert(true)
end

function Gas2Gac.QueryApplyJoinRemoteDouDiZhuPlayerListResult(dataUD)
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end
	local t = {}
	for i = 0, list.Count - 1, 6 do
		local playerId = list[i]
		local name = list[i+1]
		local class = list[i+2]
		local gender = list[i+3]
		local expression = list[i+4]
		local applyTime = list[i+5]
		table.insert( t,{
			playerId = playerId,
			name = name,
			class = class,
			gender = gender,
			expression = expression,
			applyTime = applyTime,
		} )
	end
	g_ScriptEvent:BroadcastInLua("QueryApplyJoinRemoteDouDiZhuPlayerListResult", t)

end

function Gas2Gac.ClearApplyJoinRemoteDouDiZhuPlayerListSuccess()
	g_ScriptEvent:BroadcastInLua("ClearApplyJoinRemoteDouDiZhuPlayerListSuccess")
end

function Gas2Gac.ApprovePlayerJoinRemoteDouDiZhuSuccess(playerId)
	g_ScriptEvent:BroadcastInLua("ApprovePlayerJoinRemoteDouDiZhuSuccess",playerId)
end

function Gas2Gac.DisapprovePlayerJoinRemoteDouDiZhuSuccess(playerId)
	g_ScriptEvent:BroadcastInLua("DisapprovePlayerJoinRemoteDouDiZhuSuccess",playerId)
end

function Gas2Gac.InviteRemoteDouDiZhu(containerId, gasId, serverId, inviterId, inviterName, index)
	CLuaRemoteDouDiZhuInviteDlg.m_Info = {
		containerId = containerId,
		gasId = gasId,
		serverId = serverId,
		inviterId = inviterId,
		inviterName = inviterName,
		index = index,
	}
    if LuaCommonSideDialogMgr.m_IsOpen then
        local message = g_MessageMgr:FormatMessage("RemoteDouDiZhu_Invite_Msg",inviterName)
        LuaCommonSideDialogMgr:ShowDialog(message, "InviteRemoteDouDiZhu",
        LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("拒绝"), function()
            Gac2Gas.RefuseJoinRemoteDouDiZhu(containerId, gasId, serverId, inviterId, index)
        end, false, false, 60, true, true), 
        LuaCommonSideDialogMgr:GetBtnInfo(LocalString.GetString("同意"), function()
            Gac2Gas.AcceptJoinRemoteDouDiZhu(containerId, gasId, serverId, inviterId, index)
        end, true, true))
        return
    end
	CUIManager.ShowUI(CLuaUIResources.RemoteDouDiZhuInviteDlg)
end

function Gas2Gac.InviteRemoteDouDiZhuSuccess(containerId, gasId, serverId, inviteeId, index)
	g_ScriptEvent:BroadcastInLua("InviteRemoteDouDiZhuSuccess",inviteeId)
end

function Gas2Gac.QueryInvitedRemoteDouDiZhuPlayerListResult(dataUD)
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end
	local t = {}
	for i = 0, list.Count - 1 do
		t[tonumber(list[i])] = true
	end
	g_ScriptEvent:BroadcastInLua("QueryInvitedRemoteDouDiZhuPlayerListResult",t)
end

function Gas2Gac.UpdateNpcShopLimitItemCount( shopId,  templateId,  leftCount)
	g_ScriptEvent:BroadcastInLua("UpdateNpcShopLimitItemCount",shopId, templateId, leftCount)
end


function Gas2Gac.OpenNpcShop(npcEngineId, shopId,items, equipments, limits)
    CLuaNPCShopInfoMgr.ShowNPCShopWnd(npcEngineId, shopId, items, equipments, limits)
end

function Gas2Gac.OpenSeaFishingShop(npcEngineId, shopId, poolGrade, items, limits, buyedItemList)
    LuaSeaFishingMgr.ShowSeaFishingShopWnd(npcEngineId, poolGrade, shopId, items, limits, buyedItemList)
end

function Gas2Gac.BuyItemFromSeaFishingDecorationShopSuccess(playerId, poolGrade, shopId, itemTemplateId, count)
    if shopId == LuaSeaFishingMgr.Shop[2] then
        g_ScriptEvent:BroadcastInLua("UpdateZswItemState", itemTemplateId)
    end
end

function Gas2Gac.SendSkyBoxKind(SkyBoxKind)
    CLuaHouseMgr.ChangeSkyBox(SkyBoxKind)
end

Gas2Gac.ShowLoveLetterWnd = function (itemInfo_U)
end
Gas2Gac.SendTopLoveLetterInfo = function (letterInfo_U)
end
Gas2Gac.RequestGetPlayerSendLoveLetterResult = function (letterInfo_U)
end
Gas2Gac.RequestGetPlayerReceivedLoveLetterResult = function (receiverId, letterInfo_U)
end
Gas2Gac.BroadcastLoveLetter = function (letterInfo_U)
end
Gas2Gac.QueryLoveLetterReceiverInfoResult = function (playerInfo_U)
    local playerInfo = MsgPackImpl.unpack(playerInfo_U)
    if playerInfo.Count >= 2 then
        local playerId = math.floor(tonumber(playerInfo[0] or 0))
        local name = tostring(playerInfo[1])
        EventManager.BroadcastInternalForLua(EnumEventType.QueryLoveLetterReceiverInfoResult, {playerId, name})
    else
        EventManager.BroadcastInternalForLua(EnumEventType.QueryLoveLetterReceiverInfoResult, {0, ""})
    end
end
Gas2Gac.QueryLoveLetterSenderInfoResult = function (playerInfo_U)
end
Gas2Gac.OpenLoveLetterRankWnd = function (letterIds_U)
end
Gas2Gac.PraiseLoveLetterSuccess = function(letterId)
end
Gas2Gac.RequestSendLoveLetterSuccess = function()
end

function Gas2Gac.SyncSpokesmanTCGData(composeTimes, dataUD)
    CLuaSpokesmanMgr:SyncSpokesmanTCGData(composeTimes, dataUD)
end

function Gas2Gac.SyncSpokesmanTCGComposeResult(dataUD)
    CLuaSpokesmanMgr:SyncSpokesmanTCGComposeResult(dataUD)
end

function Gas2Gac.QuerySpokesmanContractSignResult(signed)
    CLuaSpokesmanMgr:QuerySpokesmanContractSignResult(signed)
end

function Gas2Gac.SignSpokesmanContractSuccess()
    CLuaSpokesmanMgr:SignSpokesmanContractSuccess()
end

function Gas2Gac.UpdateSpokesmanContractProgress(contractId, progress)
    CLuaSpokesmanMgr:UpdateSpokesmanContractProgress(contractId, progress)
end

function Gas2Gac.QuerySpokesmanContractDetailResult(progressListUD, awarded, allAwarded)
    CLuaSpokesmanMgr:QuerySpokesmanContractDetailResult(progressListUD, awarded, allAwarded)
end

function Gas2Gac.CheckSpokesmanContractAwardResult(hasAward, hasAllAward)
    g_ScriptEvent:BroadcastInLua("UpdateScheduleWndAlertStatus", hasAward or hasAllAward)
end

function Gas2Gac.SendFinishSpokesmanContractAwardSuccess(contractId)
    CLuaSpokesmanMgr:SendFinishSpokesmanContractAwardSuccess(contractId)
end

function Gas2Gac.SendFinishAllSpokesmanContractAwardSuccess()
    CLuaSpokesmanMgr:SendFinishAllSpokesmanContractAwardSuccess()
end

function Gas2Gac.ReplaceSkillIcon(oriSkillCls, replaceSkillCls, endTime)
    print(oriSkillCls, replaceSkillCls, endTime)
    LuaSkillMgr:ReplaceSkillIcon(oriSkillCls, replaceSkillCls, endTime)
end

function Gas2Gac.ResetSkillIcon(oriSkillCls)
    LuaSkillMgr:ResetSkillIcon(oriSkillCls)
end

function Gas2Gac.ShowReverseSkillFx(aEngineId, dEngineId, aFxId, dFxId, bulletFxId, aFxDelay, bFxDelay, bulletFxDelay)
    -- aEngineId不为0
    -- dEngineId，aFxId, dFxId, bulletFxId都允许为0，注意判断
    local destObj = CClientObjectMgr.Inst:GetObject(dEngineId)
    local srcObj = CClientObjectMgr.Inst:GetObject(aEngineId)
    if not destObj or not srcObj then return end
    if dFxId > 0 and destObj then
        local destFx = CEffectMgr.Inst:AddObjectFX(dFxId, destObj.RO, bFxDelay, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        destObj.RO:AddFX(dFxId, destFx, -1)
    end
    if bulletFxId > 0 and destObj and srcObj then
        CEffectMgr.Inst:AddBulletFX(bulletFxId, destObj.RO, srcObj.RO, bulletFxDelay, 1, -1, nil)
    end
    if aFxId > 0 and srcObj then
        local srcFx = CEffectMgr.Inst:AddObjectFX(aFxId, srcObj.RO, aFxDelay, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        srcObj.RO:AddFX(aFxId, srcFx, -1)
    end
end

function Gas2Gac.SummonTeammate(wndType)
    if not CTeamMgr.Inst:TeamExists() then return end
    local leaderId = CTeamMgr.Inst:GetLeaderId()
    local m = CTeamMgr.Inst:GetMemberById(leaderId)
    local leaderName
    if m then leaderName = m.m_MemberName end
    local params = {
        g_MessageMgr:FormatMessage("Follow_The_Call", leaderName),
        GameSetting_Common_Wapper.Inst.TeamFollowConfirmCountDown,
        DelegateFactory.Action(function()
            CTeamFollowMgr.Inst:RequestTeamFollowOnNotFollowing()
        end),
        DelegateFactory.Action(function()
            CTeamFollowMgr.Inst:RejectTeamFollow()
        end), LocalString.GetString("同意"), LocalString.GetString("拒绝")}
    if wndType == EnumSummonTeammateWndType.eDieKe then
        MessageWndManager.ShowDefaultConfirm(params[1],params[2],params[3],params[4],params[5],params[6])
        return
    end
    if CTeamFollowMgr.Inst:GetAutoAcceptTeamFllowRequest() then
        CTeamFollowMgr.Inst:RequestTeamFollowOnNotFollowing()
        return
    end
    MessageWndManager.ShowTeamFollowConfirm(params[1],params[2],params[3],params[4],params[5],params[6])
end

function Gas2Gac.ShowAppUpgradeNotify(type, content_U)
	if g_ShowAppUpgradeNotifyCallback[type] then
		g_ShowAppUpgradeNotifyCallback[type](type, content_U)
	end
end

function Gas2Gac.ShowConfirmMessage(sessionId, msg, timeout, countdownType, wndType, extraInfo_U)
    g_MessageMgr:ShowConfirmMessage(sessionId, msg, timeout, countdownType, wndType, extraInfo_U)
end

function Gas2Gac.SyncQiangXiangZiSignUpInfo(isSignUp, remainAwards, honor, myRank, nextRefreshTime)
    LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiSignUpInfo(isSignUp, remainAwards, honor, myRank, nextRefreshTime)
end

function Gas2Gac.SyncQiangXiangZiSignUpResult(isSignUp)
    LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiSignUpResult(isSignUp)
end

function Gas2Gac.SyncQiangXiangZiRoundResult(isWin, isDiZhu, isPlayOver, roundChestCount, chestCount, extraData)
    LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiRoundResult(isWin, isDiZhu, isPlayOver, roundChestCount, chestCount, extraData)
end

function Gas2Gac.SyncQiangXiangZiOpenChestResult(remainChestCount, getHonor, awards)
    LuaDouDiZhuBaoXiangMgr:SyncQiangXiangZiOpenChestResult(remainChestCount, getHonor, awards)
end

--装饰物模板
function Gas2Gac.BindHouseTemplate(templateId)
    local data = HouseCompetition_HouseTemplate.GetData(templateId)
    local houseTemplate = CClientHouseMgr.Inst.mExtraProp.HouseTemplate
    if data then
        local sn = data.SN
        houseTemplate:SetBit(sn,true)
    end
end

function Gas2Gac.EnterHouseCompetitionTemplateShowPlay(templateId)
    CLuaZswTemplateMgr.CurTemplateRoTbl = {}
    CLuaZswTemplateMgr.CurPreviewInHouseId = templateId
end

function Gas2Gac.UpdatePlayPlayerInfoToWatcher(playerInfo_U, serverInfo_U)
    local playerInfo = MsgPackImpl.unpack(playerInfo_U)
    local serverInfo = MsgPackImpl.unpack(serverInfo_U)
    --serverInfo里面加了附体等级 -1 表示本服 无附体, 0表示未激活附体
    --serverInfo增加了新的附体信息：{shenliLv, 已经激活的附体}
    CFightingSpiritLiveWatchWndMgr.OpenWnd(playerInfo, serverInfo)
end

function Gas2Gac.SetAutoPickExtraResult(key, value, success)
    if not CClientMainPlayer.Inst or not success then
        return
    end
    CClientMainPlayer.Inst.ItemProp.AutoPickExtra:SetBit(key,value==1 and true or false)
end


--打开紫冰兑换幸运界面
function Gas2Gac.OpenPurpleIceExchangeMFWnd(leftExchangeMF)
    if CClientMainPlayer.Inst ~= nil then
        CClientMainPlayer.Inst.FightProp.LeftExchangeMF = leftExchangeMF
        if CLivingSkillMgr.Inst:GetSkillLevel(GameSetting_Common_Wapper.Inst.WuGuiYunCaiSkillTypeId) < GameSetting_Common_Wapper.Inst.EquipExchangeMFDependSkillLevel then
            g_MessageMgr:ShowMessage("FIVE_GHOST_GRADE_LIMIT")
        elseif CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.EquipExchangeMF) >= GameSetting_Common_Wapper.Inst.EquipExchangeMFMaxValue then
            g_MessageMgr:ShowMessage("EQUIP_EXCHANGE_MF_MFLIMIT")
        elseif leftExchangeMF < 1E-06 then
            g_MessageMgr:ShowMessage("NO_DAILY_LEFT_EXCHANGE_MF")
        else
            local itemProp = CClientMainPlayer.Inst.ItemProp
            local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
            local n = 0
            do
                local i = 1
                while i <= bagSize do
                    local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                    if not System.String.IsNullOrEmpty(id) then
                        local item = CItemMgr.Inst:GetById(id)
                        if item ~= nil and item.TemplateId == GameSetting_Common_Wapper.Inst.PurpleIceTemplateId then
                            n = n + item.Amount
                        end
                    end
                    i = i + 1
                end
            end
            if n > 0 then
                local total = n
                do
                    local i = 1
                    while i <= n do
                        if (i * GameSetting_Common_Wapper.Inst.PurpleIceExchangeMF) >= leftExchangeMF then
                            total = i
                            break
                        end
                        i = i + 1
                    end
                end

                CLuaNumberInputMgr.ShowNumInputBox(1, total, total, function (val)
                    Gac2Gas.RequestSubmitPurpleIceExchangeMF(val)
                end, System.String.Format(LocalString.GetString("请输入要兑换的紫冰数量，每件紫冰最高可兑换{0}点幸运"), NumberComplexToString(GameSetting_Common_Wapper.Inst.PurpleIceExchangeMF, typeof(Single), "F3")), -1)
            else
                g_MessageMgr:ShowMessage("NO_PURPLE_ICE_EXCHANGE_MF")
            end
        end
    end
end
--打开红冰兑换幸运界面
function Gas2Gac.OpenRedIceExchangeMFWnd(leftExchangeMF)
    if CClientMainPlayer.Inst ~= nil then
        CClientMainPlayer.Inst.FightProp.LeftExchangeMF = leftExchangeMF
        if CLivingSkillMgr.Inst:GetSkillLevel(GameSetting_Common_Wapper.Inst.WuGuiYunCaiSkillTypeId) < GameSetting_Common_Wapper.Inst.EquipExchangeMFDependSkillLevel then
            g_MessageMgr:ShowMessage("FIVE_GHOST_GRADE_LIMIT")
        elseif CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp.EquipExchangeMF) >= GameSetting_Common_Wapper.Inst.EquipExchangeMFMaxValue then
            g_MessageMgr:ShowMessage("EQUIP_EXCHANGE_MF_MFLIMIT")
        elseif leftExchangeMF < 1E-06 then
            g_MessageMgr:ShowMessage("NO_DAILY_LEFT_EXCHANGE_MF")
        else
            local itemProp = CClientMainPlayer.Inst.ItemProp
            local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
            local n = 0
            do
                local i = 1
                while i <= bagSize do
                    local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
                    if id and id~="" then
                        local item = CItemMgr.Inst:GetById(id)
                        if item ~= nil and item.TemplateId == GameSetting_Common_Wapper.Inst.RedIceTemplateId then
                            n = n + item.Amount
                        end
                    end
                    i = i + 1
                end
            end
            if n > 0 then
                local total = n
                do
                    local i = 1
                    while i <= n do
                        if (i * GameSetting_Common_Wapper.Inst.RedIceExchangeMF) >= leftExchangeMF then
                            total = i
                            break
                        end
                        i = i + 1
                    end
                end

                CLuaNumberInputMgr.ShowNumInputBox(1, total, total, function (val)
                    Gac2Gas.RequestSubmitRedIceExchangeMF(val)
                end, SafeStringFormat3(LocalString.GetString("请输入要兑换的红冰数量，每件红冰最高可兑换%s点幸运"), NumberComplexToString(GameSetting_Common_Wapper.Inst.RedIceExchangeMF, typeof(Single), "F3")), -1)
            else
                g_MessageMgr:ShowMessage("NO_RED_ICE_EXCHANGE_MF")
            end
        end
    end
end

function Gas2Gac.ShowSubmitQiFuXiangWnd()
    local total = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, YuanDan_Setting.GetData().Qiuqian_ItemID)
    if total > 0 then
        CLuaNumberInputMgr.ShowNumInputBox(1, total, total, function (amount)
            Gac2Gas.RequestSubmitQiFuXiang(amount)
        end, g_MessageMgr:FormatMessage("Please_Input_The_Number_Of_Xiang"), -1)
    else
        g_MessageMgr:ShowMessage("YuanDan_None_Xiang")
    end
end
function Gas2Gac.GetPlayerShopFundSuccess(shopId, fund, maintainFund, totalFund)
	EventManager.BroadcastInternalForLua(EnumEventType.GetPlayerShopFundSuccess, {shopId, fund, maintainFund, totalFund})
end

function Gas2Gac.CastingForRideFail(reason)
    CTrackMgr.Inst:ClearAutoRideAction()
end

function Gas2Gac.QueryClientInfo(typeName, cipher)
if CommonDefs.Is_PC_PLATFORM() then
    if typeName == "b" then
        local idn = NewStringBuilderWraper(StringBuilder, 200)
        local mac = NewStringBuilderWraper(StringBuilder, 200)
        local dsn = NewStringBuilderWraper(StringBuilder, 200)
        local smb = NewStringBuilderWraper(StringBuilder, 200)
        local mid = NewStringBuilderWraper(StringBuilder, 200)
        CUnityHelper.GetInternalData11(cipher, idn, mac, dsn, smb, mid)
        Gac2Gas.SendClientInfoBegin(typeName, "")
        Gac2Gas.SendClientInfo(typeName, "i", idn:ToString())
        Gac2Gas.SendClientInfo(typeName, "c", mac:ToString())
        Gac2Gas.SendClientInfo(typeName, "d", dsn:ToString())
        Gac2Gas.SendClientInfo(typeName, "s", smb:ToString())
        Gac2Gas.SendClientInfo(typeName, "m", mid:ToString())
        Gac2Gas.SendClientInfoEnd(typeName, cipher)

        -- 根据管超的建议，此调用放到Gac2Gas.SendClientInfoEnd之后，并且在typeName为b时上传额外的PC端信息，这个阶段会记录更多的有用信息
        -- 这部分额外信息运维用不到，主要可以为以后分享玩家设备情况提供一些参考
        CLoginMgr.Inst:RequestLogPCloginExtraInfo()
    else
        local buffer = NewStringBuilderWraper(StringBuilder, 60*1024)
        if typeName == "p" then
            -- GetProcessInfo
            CUnityHelper.GetInternalData2(cipher, buffer)
        elseif typeName == "m" then
            -- GetModuleInfo
            CUnityHelper.GetInternalData3(cipher, buffer)
        elseif typeName == "c" then
            -- GetCaptionInfo
            CUnityHelper.GetInternalData4(cipher, buffer)
        else
            return
        end
        Gac2Gas.SendClientInfoBegin(typeName, "")
        local str = buffer:ToString()
        local maxLength = 200
        local n = math.ceil(#str/maxLength)
        for i = 1, n do
            local subStr = string.sub(str, (i-1)*maxLength+1, i*maxLength)
            Gac2Gas.SendClientInfo(typeName, "", subStr)
        end
        Gac2Gas.SendClientInfoEnd(typeName, cipher)
    end
end
end

function Gas2Gac.YanHuaScreenshotReportSuccess()
    g_MessageMgr:ShowMessage("Report_YanHua_Succeed")
    CUIManager.CloseUI(CLuaUIResources.YanHuaReportWnd)
end

function Gas2Gac.PlayYanHuaLiBao(x, y, str, dir, playerId)

    CLuaYanHuaEditorMgr.PlayYanHuaLiBao(x,y,str,dir,playerId)
end

function Gas2Gac.SyncLastInviteSectTime(infoUd)
    local infoList = MsgPackImpl.unpack(infoUd)
    local step = 2
    for i = 0,infoList.Count,step do
        local playerId          = infoList[i]
        local lastInviteTime    = infoList[i+1]
    end
end

function Gas2Gac.ReplyJianJiaCangCangFlowerInfo(npcEngineId, stage, time1, time2, bOperate)

    if time1==time2 then
        CUIManager.CloseUI(CLuaUIResources.QingRenJie2021PlayStatusWnd)
        return
    end
    CLuaQingRenJie2021PlayStatusWnd.npcEngineId = npcEngineId
    CLuaQingRenJie2021PlayStatusWnd.stage = stage
    CLuaQingRenJie2021PlayStatusWnd.time1 = time1
    CLuaQingRenJie2021PlayStatusWnd.time2 = time2
    CLuaQingRenJie2021PlayStatusWnd.bOperate = bOperate
    CUIManager.ShowUI(CLuaUIResources.QingRenJie2021PlayStatusWnd)
end

function Gas2Gac.ReplyJianJiaCangCangPlayInfo(bReceive)
    -- print("ReplyJianJiaCangCangPlayInfo", bReceive)
    g_ScriptEvent:BroadcastInLua("ReplyJianJiaCangCangPlayInfo", bReceive)
end

function Gas2Gac.AddWorldPositionMultiFxWithDir(x, y, fxList, dir)

    CLuaYanHuaEditorMgr.AddWorldPositionMultiFxWithDir(x, y, fxList, dir)
end

function Gas2Gac.ReplyYiAiZhiMingSendFlowerRank(selfData, rankData)
    CLuaQingRenJie2021Mgr.ReplyYiAiZhiMingSendFlowerRank(selfData, rankData)
end

function Gas2Gac.ShowEngineHeadFxWithDuration(engineId, fxId, duration)
end

function Gas2Gac.AddUIFx(fxId, duration, destroyWithScene)
    CCommonUIFxWnd.AddUIFx(fxId, duration, destroyWithScene, false)
end

function Gas2Gac.AddUIFxWithBG(fxId, duration, destroyWithScene, showBG)
    CCommonUIFxWnd.AddUIFx(fxId, duration, destroyWithScene, showBG)
end

function Gas2Gac.RemoveUIFx(fxId)
	EventManager.BroadcastInternalForLua(EnumEventType.OnDestroyCommonUIFxByID, {fxId})
end

function Gas2Gac.SyncEnemySectOverview(sectId, ovewviewUd)
    local ovewview = MsgPackImpl.unpack(ovewviewUd)
    g_ScriptEvent:BroadcastInLua("SyncEnemySectOverview", ovewview)
end

function Gas2Gac.SyncEnemySectCaptured(enemySectId, Ud)
    local list = MsgPackImpl.unpack(Ud)
    g_ScriptEvent:BroadcastInLua("SyncEnemySectCaptured", list)
end

function Gas2Gac.SyncSectEntranceByMapId(mapInfoUd)
    local mapInfo = MsgPackImpl.unpack(mapInfoUd)
    LuaZongMenMgr:DealSectWithMapData(mapInfo)
end

function Gas2Gac.SyncSectTaskDestSceneId(taskId, sectId, raidMapId, sceneId)
    CTrackMgr.Inst:SyncSectTaskDestSceneId(taskId, sectId, raidMapId, sceneId)
end

function Gas2Gac.SyncDynamicShopShangJiaItem(dataUD)
	local list = MsgPackImpl.unpack(dataUD)
    if not list then return end
    if list.Count == 0 then
        LuaRecommendGiftMgr.OnPlayerLogin()
        g_ScriptEvent:BroadcastInLua("SyncRecommendGiftInfos", false)
    elseif list.Count >=3  then
        LuaRecommendGiftMgr.CurItemId = list[1]
        LuaRecommendGiftMgr.ExpireTime = list[2]
        LuaRecommendGiftMgr.RemainTime = LuaRecommendGiftMgr.ExpireTime - CServerTimeMgr.Inst.timeStamp
        g_ScriptEvent:BroadcastInLua("SyncRecommendGiftInfos", true)
        CLuaActivityAlert.s_RecommendGiftStatus = true
    end
end

-- 所有npc的被邀请情况，Gac2Gas.QueryAllSectNpcData的返回
function Gas2Gac.SendAllSectNpcData(joinedUD, unjoinUD, mySectNpcId,isSectLeader, inCompetition)
    -- 已被邀请走的NPC
    local joinedList = MsgPackImpl.unpack(joinedUD)
    local unjoinList = MsgPackImpl.unpack(unjoinUD)
    LuaInviteNpcMgr.JoinedList = joinedList
    LuaInviteNpcMgr.UnJoinedList = unjoinList
    LuaInviteNpcMgr.MySectNpcId = mySectNpcId
    LuaInviteNpcMgr.IsSectLeader = isSectLeader
    LuaInviteNpcMgr.InCompetition = inCompetition
    CUIManager.ShowUI(CLuaUIResources.InviteNpcJoinZongMenWnd)
end

-- 本人宗门邀请的NPC数据，Gac2Gas.QueryMySectInviteNpcData的返回
function Gas2Gac.SendMySectNpcData(todayTaskTimes, npcId, npcState, isRewarded, dataUD)

    LuaInviteNpcMgr.MyInviteNpcState = npcState
    LuaInviteNpcMgr.MyFavorInfo = {}
    LuaInviteNpcMgr.MyFavorInfo.isRewarded = isRewarded
    LuaInviteNpcMgr.MyFavorInfo.id = npcId
    if npcState == 0 then
        -- 已被本宗门邀请成功
        local list = MsgPackImpl.unpack(dataUD)
        local friendliness = list[0]     -- 青睐度
        local aboutLeaveTime = list[1]   -- 将要离开时间，可能为0则为不会离开
        LuaInviteNpcMgr.MyFavorInfo.taskTimes = todayTaskTimes     
        LuaInviteNpcMgr.MyFavorInfo.friendliness = list[0]
        LuaInviteNpcMgr.MyFavorInfo.aboutLeaveTime = list[1]
        LuaInviteNpcMgr.OpenNpcJoinedView()
    elseif npcState == 1 then
        -- 还未邀请成功，进行中
        local list = MsgPackImpl.unpack(dataUD)
        LuaInviteNpcMgr.MyFavorInfo.taskTimes = todayTaskTimes     
        LuaInviteNpcMgr.MyFavorInfo.inviterCount = list[0]
        LuaInviteNpcMgr.MyFavorInfo.friendliness = list[1]
        LuaInviteNpcMgr.MyFavorInfo.expireTime = list[2]
        LuaInviteNpcMgr.OpenNpcInvitingView()
    elseif npcState == 2 then
        -- NPC被冷落，主干离开宗门
        LuaInviteNpcMgr.MyFavorInfo.leaveReason = LocalString.GetString("因青睐值过低离开")
        LuaInviteNpcMgr.OpenNpcLeaveView()
    elseif npcState == 3 then
        -- NPC被宗主请离宗门
        LuaInviteNpcMgr.MyFavorInfo.leaveReason = LocalString.GetString("已被门主请离")
        LuaInviteNpcMgr.OpenNpcLeaveView()
    elseif npcState == 4 then
        -- NPC被其他宗门邀请走
        LuaInviteNpcMgr.MyFavorInfo.leaveReason = LocalString.GetString("已加入其他宗派")
        LuaInviteNpcMgr.OpenNpcLeaveView()
    end
end

-- 某个NPC正在被邀请的宗门列表，Gac2Gas.QueryMySectInviteNpcData的返回
function Gas2Gac.SendNpcInviterData(mySectDaoYiValue, myDaoYiAddonValue, npcId, inviterCount, isRewarded, inviterUD)

    local inviterList = MsgPackImpl.unpack(inviterUD)
    LuaInviteNpcMgr.InviterList = {}
    for i = 0, inviterList.Count - 1, 3 do
        local t = {}
        t.sectId = inviterList[i]          -- 宗门ID
        t.sectName = inviterList[i+1]      -- 宗门名
        t.daoyiValue = inviterList[i+2]      -- 道义值
        -- t.friendliness = inviterList[i+2]  -- 青睐度
        -- t.expireTime = inviterList[i+3]    -- 过期时间
        -- t.progress = inviterList[i+4]
        table.insert(LuaInviteNpcMgr.InviterList,t)
    end
    table.sort(LuaInviteNpcMgr.InviterList, function(a,b)
        return a.daoyiValue > b.daoyiValue
    end)
    LuaInviteNpcMgr.MyInviterData = { daoyiValue = mySectDaoYiValue, daoYiAddonValue = myDaoYiAddonValue,isRewarded = isRewarded}
    LuaInviteNpcMgr.ShowFreeNpcItemTip()
end

-- 宗门成功邀请到NPC
function Gas2Gac.SendNpcJoinSectSucc(npcId)
    local path = SectInviteNpc_Setting.GetData().InviteSucceedFx
    EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {path})
end

-- 返回自己宗门邀请了的NPCID
function Gas2Gac.SendMySectNpcId(npcId)

    g_ScriptEvent:BroadcastInLua("ReceiveMySectNpcId",npcId)
end

-- 宗派置顶消息,参照 BroadcastGuildMessageTopHighline 实现
function Gas2Gac.BroadcastSectMessageTopHighline(sectId, msg)
end

function Gas2Gac.SyncWuYi2021FindMindData(mindPieceCount, isFullRewarded, dataUD)
    local list = MsgPackImpl.unpack(dataUD)
    g_ScriptEvent:BroadcastInLua("SyncWuYi2021FindMindData",mindPieceCount, isFullRewarded,list)
end

function Gas2Gac.LearnGuanTianXiangSkillSuccess(level)
    g_ScriptEvent:BroadcastInLua("LearnGuanTianXiangSkillSuccess")
end

function Gas2Gac.ReplySectFutureWeather(hours, dataUD)
    local list = MsgPackImpl.unpack(dataUD)

    local weatherInfos = {}
    for i=0, list.Count-1, 3 do
        table.insert(weatherInfos, {weatherId = list[i+2], startTime = list[i], endTime = list[i+1]})
    end

    table.sort(weatherInfos, function(a, b)
        if a.startTime < b.startTime then
            return true
        end

        return false
    end)
    g_ScriptEvent:BroadcastInLua("ReplySectFutureWeather", hours, weatherInfos)
end

function Gas2Gac.SyncWuYi2021JXMXPlayInfo(stage, dataUD, bossEngineId)
    local list = MsgPackImpl.unpack(dataUD)
    LuaWuYi2021Mgr:SyncWuYi2021JXMXPlayInfo(stage, list, bossEngineId)
end

function Gas2Gac.SyncWuYi2021JXMXPlayResult(hasReward, isWin)
    LuaWuYi2021Mgr:ShowJXMXReward(hasReward, isWin)
end


function Gas2Gac.PlayPicture(path,duration,width,heigth)
    CZhuJueJuQingMgr.Instance.PicTexturePath = path
    CZhuJueJuQingMgr.Instance.PicDuduration = duration

    CLuaJuqingPicWnd.s_Width=width
    CLuaJuqingPicWnd.s_Height=heigth
    CUIManager.ShowUI(CUIResources.JuqingPicWnd)
end

function Gas2Gac.TongQingYouLi_QueryInfoResult(info_U)
    local info = MsgPackImpl.unpack(info_U)
    --[[
        info.RewadCount,
        info.RewadPlayers,
        info.TotalReward,
        info.RewardCount_Task,
        info.RewardCount_Charge,
        info.RewardCount_Share,
        info.RewardCount_Appointment)
        ]]
	if not info then return end
    LuaZhouNianQing2021Mgr.UpdateTongQingYouLiInfo(info)
end


function Gas2Gac.TongQingYouLi_RewardResult(rewardTimes, randomIdx, str)
    LuaZhouNianQing2021Mgr.ShowTongQingYouLiChouJiangWnd(rewardTimes, randomIdx - 1, str)
end


function Gas2Gac.ZNQZhiBo_SyncChannelInfo(RemoteChannelInfo)
    LuaZhouNianQing2021Mgr.SyncCCInfo(RemoteChannelInfo)
end

function Gas2Gac.ZNQZhiBo_ShowInvition()
    CUIManager.ShowUI(CLuaUIResources.ZNQZhiBoYaoQingWnd)
end

function Gas2Gac.ZNQZhiBo_ZhiBoEnd()
    LuaZhouNianQing2021Mgr.StopZhiBo()
end

function Gas2Gac.ClubHouse_QueryRoom_Result(room_U)
    LuaClubHouseMgr:ClubHouse_QueryRoom_Result(room_U)
end

function Gas2Gac.ClubHouse_QueryRoomList_Result(rooms_U, Page, context)
    LuaClubHouseMgr:ClubHouse_QueryRoomList_Result(rooms_U, Page, context)
end

function Gas2Gac.ClubHouse_CreateRoom_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_CreateRoom_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_LeaveRoom_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_LeaveRoom_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_EnterRoom_Result(playerId, bSuccess, data_U, Reason)
    LuaClubHouseMgr:ClubHouse_EnterRoom_Result(playerId, bSuccess, data_U, Reason)
end

function Gas2Gac.ClubHouse_SetMic_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_SetMic_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_UnsetMic_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_UnsetMic_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_Kick_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_Kick_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_DestroyRoom_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_DestroyRoom_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_ChangeStatus_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_ChangeStatus_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_ChangeStatus_Reply(DstAccId, Status, bEnable)
    LuaClubHouseMgr:ClubHouse_ChangeStatus_Reply(DstAccId, Status, bEnable)
end

function Gas2Gac.ClubHouse_ClearStatusAll_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_ClearStatusAll_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_QueryMemberByStatus_Result(playerId, bSuccess, data_U, Status, Context)
    LuaClubHouseMgr:ClubHouse_QueryMemberByStatus_Result(playerId, bSuccess, data_U, Status, Context)
end

function Gas2Gac.ClubHouse_ForbidStatusAll_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_ForbidStatusAll_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_QueryAllListenersInRoom_Result_Begin(playerId, RoomId)
    LuaClubHouseMgr:ClubHouse_QueryAllListenersInRoom_Result_Begin(playerId, RoomId)
end

function Gas2Gac.ClubHouse_QueryAllListenersInRoom_Result(playerId, RoomId, data_U)
    LuaClubHouseMgr:ClubHouse_QueryAllListenersInRoom_Result(playerId, RoomId, data_U)
end

function Gas2Gac.ClubHouse_QueryAllListenersInRoom_Result_End(playerId, RoomId)
    LuaClubHouseMgr:ClubHouse_QueryAllListenersInRoom_Result_End(playerId, RoomId)
end

function Gas2Gac.ClubHouse_ChangeOwner_Result(playerId, bSuccess, data_U)
    LuaClubHouseMgr:ClubHouse_ChangeOwner_Result(playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_ChangePassWord_Result(playerId, bSuccess, password)
    g_ScriptEvent:BroadcastInLua("ClubHouse_ChangePassWord_Result", playerId, bSuccess, password)
end

function Gas2Gac.ClubHouse_QueryPassWord_Result(playerId, bSuccess, data_U)
    g_ScriptEvent:BroadcastInLua("ClubHouse_QueryPassWord_Result",  playerId, bSuccess, data_U)
end

function Gas2Gac.ClubHouse_QueryMyRoomId_Result(RoomId, Context)
    LuaClubHouseMgr:ClubHouse_QueryMyRoomId_Result(RoomId, Context)
end

function Gas2Gac.ClubHouse_Notify_DelRoom(RoomId)
    LuaClubHouseMgr:ClubHouse_Notify_DelRoom(RoomId)
end

function Gas2Gac.ClubHouse_Notify_LeaveRoom(RoomId, AccId, MicCount, ListenerCount, Heat)
    LuaClubHouseMgr:ClubHouse_Notify_LeaveRoom(RoomId, AccId, MicCount, ListenerCount, Heat)
end

function Gas2Gac.ClubHouse_Notify_EnterRoom(member_U, MicCount, ListenerCount, Heat)
    LuaClubHouseMgr:ClubHouse_Notify_EnterRoom(member_U, MicCount, ListenerCount, Heat)
end

function Gas2Gac.ClubHouse_Notify_SetMic(member_U, MicCount, ListenerCount, Heat)
    LuaClubHouseMgr:ClubHouse_Notify_SetMic(member_U, MicCount, ListenerCount, Heat)
end

function Gas2Gac.ClubHouse_Notify_UnsetMic(member_U, MicCount, ListenerCount, Heat)
    LuaClubHouseMgr:ClubHouse_Notify_UnsetMic(member_U, MicCount, ListenerCount, Heat)
end

function Gas2Gac.ClubHouse_Notify_MemberChange(member_U)
    LuaClubHouseMgr:ClubHouse_Notify_MemberChange(member_U)
end

function Gas2Gac.ClubHouse_Notify_ClearStatusAll(RoomId, Status)
    LuaClubHouseMgr:ClubHouse_Notify_ClearStatusAll(RoomId, Status)
end

function Gas2Gac.ClubHouse_Notify_ForbidHandsUpChange(RoomId, bForbid)
    LuaClubHouseMgr:ClubHouse_Notify_ForbidHandsUpChange(RoomId, bForbid)
end

function Gas2Gac.ClubHouse_Notify_ChangeOwner(owner_U, prevOwner_U)
    LuaClubHouseMgr:ClubHouse_Notify_ChangeOwner(owner_U, prevOwner_U)
end

function Gas2Gac.ClubHouse_Notify_GivePresent(SrcPlayer_U, PresentId, Count, Combo, Heat, DstAccId, EffectTimes, ToRemove_U)
    LuaClubHouseMgr:ClubHouse_Notify_GivePresent(SrcPlayer_U, PresentId, Count, Combo, Heat, DstAccId, EffectTimes, ToRemove_U)
end

function Gas2Gac.ClubHouse_Notify_Reset()
    LuaClubHouseMgr:ClubHouse_Notify_Reset()
end

function Gas2Gac.ClubHouse_QueryRank_Result(RankType, RankData_U, MyValue)
    LuaClubHouseMgr:ClubHouse_QueryRank_Result(RankType, RankData_U, MyValue)
end

function Gas2Gac.ClubHouse_BroadcastMessageInRoom(RoomId, SendId, AppName, SendName, content, RoomTop, Character)
    LuaClubHouseMgr:ClubHouse_BroadcastMessageInRoom(RoomId, SendId, AppName, SendName, content, RoomTop, Character)
end

function Gas2Gac.ClubHouse_GivePresentFailed()
    LuaClubHouseMgr:ClubHouse_GivePresentFailed()
end

function Gas2Gac.ClubHouse_Notify_RenameRoom(RoomId, Name)
    LuaClubHouseMgr:ClubHouse_Notify_RenameRoom(RoomId, Name)
end

function Gas2Gac.ClubHouse_Notify_UpdateTitle(RoomId, Title)
    LuaClubHouseMgr:ClubHouse_Notify_UpdateTitle(RoomId, Title)
end

function Gas2Gac.ClubHouse_Notify_UpdateHeat(RoomId, Heat)
    LuaClubHouseMgr:ClubHouse_Notify_UpdateHeat(RoomId, Heat)
end

function Gas2Gac.ClubHouse_Notify_ResetRoomPresent()
    LuaClubHouseMgr:ClubHouse_Notify_ResetRoomPresent()
end

function Gas2Gac.SyncGnjcZhanYiProgressInfo(current, total)
    if not CGuanNingMgr.Inst.inGnjc then return end
    g_ScriptEvent:BroadcastInLua("OnSyncGnjcZhanYiProgressInfo",  (total > 0) and (current / total) or 0)
end

function Gas2Gac.SyncGnjcBossHpInfo(attackerHp, attackerFullHp, defenderHp, defenderFullHp)
    g_ScriptEvent:BroadcastInLua("OnSyncGnjcBossHpInfo",  attackerHp, attackerFullHp, defenderHp, defenderFullHp)
end

function Gas2Gac.SendFashionTransformState(isTransformOn)
    LuaAppearancePreviewMgr:SendFashionTransformState(isTransformOn)
end

function Gas2Gac.SyncLongZhouPlaySignUpResult(isSignUp)
    g_ScriptEvent:BroadcastInLua("OnSyncLongZhouPlaySignUpResult",isSignUp)
end

function Gas2Gac.SyncLongZhouPlayPlayInfo(myRank, infoUD)
    LuaDuanWu2021Mgr:SyncLongZhouPlayPlayInfo(myRank, infoUD)
end

function Gas2Gac.ShowLongZhouPlayResultWnd(rank, time, temples, isWin)
    LuaDuanWu2021Mgr:ShowLongZhouPlayResultWnd(rank, time, temples, isWin)
end

function Gas2Gac.SyncStartLongZhouOperate()

    g_ScriptEvent:BroadcastInLua("OnSyncStartLongZhouOperate")
end

function Gas2Gac.SyncLongZhouOperateResult(resultUD, isRoundOver)
    LuaDuanWu2021Mgr:SyncLongZhouOperateResult(resultUD,isRoundOver)
end

function Gas2Gac.OpenLongZhouPlaySignUpWnd(isSignup, remainRewardTimes)

    g_ScriptEvent:BroadcastInLua("OnSyncLongZhouPlaySignUpResult",isSignup, remainRewardTimes)
end

function Gas2Gac.AddLongZhouTempleBuff(buffId)
    g_ScriptEvent:BroadcastInLua("OnAddLongZhouTempleBuff",buffId)
end

function Gas2Gac.SyncLongZhouOnBoatStatus(onBoat)
    LuaDuanWu2021Mgr:SyncLongZhouOnBoatStatus(onBoat)
end

function Gas2Gac.ShowTransferEquipStoneConfirm(cost, sourcePlace, sourcePos, sourceEquipId, targetPlace, targetPos, targetEquipId)
	local msg = g_MessageMgr:FormatMessage("TRANSFER_EQUIP_STONE_CONFIRM", cost)
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
		-- 确认转移则关闭属性对比框，转移后属性会再次发生变更
		CUIManager.CloseUI(CUIResources.PlayerProDifShowWnd)
		Gac2Gas.ConfirmTransferEquipStone(sourcePlace, sourcePos, sourceEquipId, targetPlace, targetPos, targetEquipId)
	end), nil,nil, nil, false)
end

function Gas2Gac.OperateFuXiDanceNeedUnLock()
    LuaFuxiAniMgr.ShowItemConsumeWnd()
end

function Gas2Gac.UnLockFuXiDanceSuccess()
    LuaFuxiAniMgr.UnlockFuxiSuccess()
end

function Gas2Gac.FuXiDanceUploadPreCheckDone()
    LuaFuxiAniMgr.OpenUploadWebPage()
end

function Gas2Gac.OpenFuXiDanceUploadSubJadeWnd()
    LuaFuxiAniMgr.ShowCostWnd()
    
end

function Gas2Gac.SubJadeForFuXiDanceUploadSuccess(index)
    -- LuaFuxiAniMgr.AddEmptyEmotion(index)
    LuaFuxiAniMgr.OpenUploadWebPage()
end

function Gas2Gac.UpdateFuXiDanceData(reason, index, data_U)
    LuaFuxiAniMgr.UpdateFuXiDanceData(reason,index,data_U)
end

function Gas2Gac.ReNameFuXiDanceMotionSuccess(index, motionId, newName)
    LuaFuxiAniMgr.RenameFuXiDanceMotion(index, motionId, newName)
end

function Gas2Gac.DeleteFuXiDanceMotionSuccess(index, motionId)
    LuaFuxiAniMgr.DeleteFuXiDanceMotion(index, motionId)
end

function Gas2Gac.ShowFuXiDanceExpressionActionState(engineId, expressionId, motionId)
    L10.Game.Gas2Gac.m_Instance:ShowFuxiDanceActionState(engineId,motionId)
end

function Gas2Gac.SendQueQiaoXianQuSignUpInfo(isSignUp, forceOpen, todayRemainRewardTimes)
    LuaQiXi2021Mgr.SyncQueQiaoXianQuSignUpInfo(isSignUp,forceOpen,todayRemainRewardTimes)

end

function Gas2Gac.SendQueQiaoXianQuPartnerInfo(resultUD, partnerAwardTimes)

	local list = MsgPackImpl.unpack(resultUD)
	if not list then return end

    local dataList = {}
	for i = 0, list.Count - 1, 5 do
		local time = list[i]
		local playerId = list[i+1]
		local name = list[i+2]
		local class = list[i+3]
		local gender = list[i+4]
        table.insert(dataList,{time = time, playerId = playerId, name = name, class = class, gender = gender})
        table.sort(dataList,function(a,b)
            return a.time > b.time
          end)

	end

    LuaQiXi2021Mgr.SyncQueQiaoXianQuPartnerInfo(dataList,partnerAwardTimes)
end

function Gas2Gac.QueQiaoXianQuQuestion(questionContent, questionId, duration, answerContent)

    LuaQiXi2021Mgr.GetQuestionInfo(questionContent, questionId, duration, answerContent)
end

function Gas2Gac.QueQiaoXianQuQuestionResult(result, timeout)
    g_ScriptEvent:BroadcastInLua("QueQiaoXianQuQuestionResult", result, timeout)

end

function Gas2Gac.SyncQueQiaoXianQuPlayInfo(stage, questionCount, questionFinishCount, daJianQueQiaoFinished, monsterKillDataUD, needHighlightSkill)


	local list = MsgPackImpl.unpack(monsterKillDataUD)
	if not list then return end
    local dataList = {}
	for i = 0, list.Count - 1, 3 do
		local monsterId = list[i]
		local totalCount = list[i+1]
		local killCount = list[i+2]
        table.insert(dataList,{monsterId = monsterId, totalCount = totalCount, killCount = killCount})
        table.sort(dataList,function(a,b)
            return a.monsterId < b.monsterId
          end)

	end
    LuaQiXi2021Mgr.SyncQueQiaoXianQuPlayInfo(stage, questionCount, questionFinishCount, daJianQueQiaoFinished, dataList, needHighlightSkill)
end

function Gas2Gac.ShowQueQiaoXianQuPlayResult(result, partnerPlayerId, partnerName, partnerClass, partnerGrade, partnerGender)
    LuaQiXi2021Mgr.SyncQueQiaoXianQuPlayResultInfo(result, partnerPlayerId, partnerName, partnerClass, partnerGrade, partnerGender)

end

function Gas2Gac.SyncYanHuaPlayInfo(isOwner, visitorNum, maxVisitorNum, effectType, timeStamp, status, chapterIndex, chapterCount)
    CLuaQiXiMgr.SyncYanHuaPlayInfo(isOwner,visitorNum,effectType,timeStamp,maxVisitorNum,status, chapterIndex, chapterCount)
end

function Gas2Gac.TravestyRole_CheckSignUpResult(isInMatching, role, playerRoleCount, bossRoleCount, BossId, PlayData_U)
    local playData = MsgPackImpl.unpack(PlayData_U)

    LuaShuJia2021Mgr.SyncSignUpInfo(isInMatching, role, playerRoleCount, bossRoleCount, BossId , playData)
end

function Gas2Gac.TravestyRole_SignUpPlayResult(bSuccess)
    if bSuccess then
        LuaShuJia2021Mgr.isSignUp = LuaShuJia2021Mgr.signUpClick 
        g_ScriptEvent:BroadcastInLua("UpdateDaMoWangSignUpInfo")
    end

end

function Gas2Gac.TravestyRole_CancelSignUpResult(bSuccess)
    if bSuccess then
        LuaShuJia2021Mgr.isSignUp = EnumTravestyRole.eNotSignUp
        g_ScriptEvent:BroadcastInLua("UpdateDaMoWangSignUpInfo")
    end

end

function Gas2Gac.TravestyRole_SyncPlayState(state, BossMonsterId, BuffMonsterId, BossHpDropRate, PlayerLossTimes, PlayerInfos_U, reason, playerDieTimes)
    local playerInfos = MsgPackImpl.unpack(PlayerInfos_U)
    LuaShuJia2021Mgr.SyncPlayerInfo(state, BossMonsterId, BossHpDropRate, PlayerLossTimes, playerInfos, reason,playerDieTimes)

end

function Gas2Gac.ShowGuanNingEnterBossFightFx()
    EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_BOSSshuaxin.prefab"})
end

function Gas2Gac.SyncOlympicBossHp(bossEngineId, bossHp)
    LuaOlympicGamesMgr:SyncOlympicBossHp(bossEngineId, bossHp)
end

function Gas2Gac.SyncOlympicBossStatus(isPlayStart, isBossAlive)
    LuaOlympicGamesMgr:SyncOlympicBossStatus(isPlayStart, isBossAlive)
end

function Gas2Gac.SyncOlympicBossInfo(currentBossHp, mapNpcCountInfo)
    LuaOlympicGamesMgr:SyncOlympicBossInfo(currentBossHp, mapNpcCountInfo)
end

function Gas2Gac.SendSeaFishingPlayInfo(rewardTimes, lastDifficult, canEnterSpecialPlay)

    LuaSeaFishingMgr.OnSendSeaFishingPlayInfo(rewardTimes, lastDifficult, canEnterSpecialPlay)
end

function Gas2Gac.UpdateSeaFishingPlayStageInfo(stage, params)

    LuaSeaFishingMgr.OnUpdateSeaFishingPlayStageInfo(stage, params)
end

function Gas2Gac.SyncSeaFishingPlayResult(gameplayId, isWin)

    LuaSeaFishingMgr.SyncSeaFishingPlayResult(gameplayId, isWin)
end

function Gas2Gac.ShowIllustrations(id, duration)
    LuaSeaFishingMgr.ShowIllustrations(id,duration)
end

function Gas2Gac.SyncUnity2018UpgradeInfo(canOpenWnd, needUpgrade, upgradeUrl, packageSize, finished, finishTime, awarded)
	LuaWelfareMgr:SyncUnity2018UpgradeInfo(canOpenWnd, needUpgrade, upgradeUrl, packageSize, finished, finishTime, awarded)
end

function Gas2Gac.SendUnity2018UpgradeAwardSuccess()
	LuaWelfareMgr:SendUnity2018UpgradeAwardSuccess()
end

function Gas2Gac.SetTianQiSpecialItemSkipConfirmResult(dataUD)
	local dict = MsgPackImpl.unpack(dataUD)
	if not dict then return end
    if not CClientMainPlayer.Inst then return end
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, value)
		local id = tonumber(key)
		local val = tonumber(value)
		CClientMainPlayer.Inst.PlayProp.TianQiSpecialItemSkipConfirm:SetBit(id, val == 1)
	end))
end

function Gas2Gac.PlayerUseFuXiLaba(Name, Id, Class, Gender, Type, contents_U, combo, extraInfo, atListData_U, expression, expressionTxt, sticker, profileInfo_U, xianFanStatus)

    local contents = MsgPackImpl.unpack(contents_U)
    local atListData = MsgPackImpl.unpack(atListData_U)

    local profileInfo = CreateFromClass(CProfileObject)
    profileInfo:LoadFromString(profileInfo_U)
    -- print(Name, Id, Class, Gender, Type, contents, 
    -- "combo:", combo, " extra:",extraInfo, " atList:", atListData[0], " atList:",atListData[1], 
    -- " ex:", expression, " ext:",expressionTxt, "st:",sticker, "pr", profileInfo, "x:", xianFanStatus)
    
    CChatMgr.Inst:PlayerUseFuXiLaba(Name, Id, Class, Gender, Type, contents, 
        combo, extraInfo, atListData[1], atListData[0], expression, expressionTxt, sticker, profileInfo, xianFanStatus)
end

function Gas2Gac.EnterGuanNingWeekendNewModePlay()
    CLuaGuanNingMgr.m_IsInGuanNingWeekendNewModePlay = true
end

function Gas2Gac.LeaveGuanNingWeekendNewModePlay()
    CLuaGuanNingMgr.m_IsInGuanNingWeekendNewModePlay = false
end

function Gas2Gac.ZuiMengLu_FindThread(ThreadId)
    ZhongYuanJie_ZuiMengLuThread.Foreach(function (tid, data)
        if tid == ThreadId then
            CClientMainPlayer.Inst.PlayProp.ZuiMengLuThreadSN:SetBit(data.SN, true)
        end
    end)
end

function Gas2Gac.ZuiMengLu_ReadThread(ThreadId)
    -- print("ZuiMengLu_ReadThread", ThreadId)
    ZhongYuanJie_ZuiMengLuThread.Foreach(function (tid, data)
        if tid == ThreadId then
            CClientMainPlayer.Inst.PlayProp.ZuiMengLuReadSN:SetBit(data.SN, true)
        end
    end)
end

function Gas2Gac.ZuiMengLu_TryRewardResult(GroupId, bSuccess)
    if bSuccess then
        local SN = ZhongYuanJie_ZuiMengLu.GetData(GroupId).SN
        CClientMainPlayer.Inst.PlayProp.ZuiMengLuRewardSN:SetBit(SN, true)
        g_ScriptEvent:BroadcastInLua("ZuiMengLu_TryRewardResult", GroupId)
    end
end

function Gas2Gac.ZuiMengLu_FinalRewardResult()
    -- print("ZuiMengLu_FinalRewardResult")
end

function Gas2Gac.ZYJ2021HMLZ_CheckSignUpResult(bSuccess)
    -- print("ZYJ2021HMLZ_CheckSignUpResult", bSuccess)
    g_ScriptEvent:BroadcastInLua("ZYJ2021HMLZ_StateUpdate", bSuccess)
end

function Gas2Gac.ZYJ2021HMLZ_SignUpPlayResult(bSuccess)

    if bSuccess then
        g_ScriptEvent:BroadcastInLua("ZYJ2021HMLZ_StateUpdate", true)
    end
end

function Gas2Gac.ZYJ2021HMLZ_CancelSignUpResult(bSuccess)

    if bSuccess then
        g_ScriptEvent:BroadcastInLua("ZYJ2021HMLZ_StateUpdate", false)
    end
end

function Gas2Gac.ZYJ2021HMLZ_SyncPlayState(playState, progressTbl_U)
    -- print("ZYJ2021HMLZ_SyncPlayState", playState, progressTbl_U)
    LuaHMLZMgr:SyncPlayState(playState, progressTbl_U)
end

function Gas2Gac.SendOlympicGambleData(id, myChoice, betCount, totalBet, gambleData)
    local list = MsgPackImpl.unpack(gambleData)
    LuaOlympicGamesMgr:SyncOlympicGambleData(id, myChoice, betCount, totalBet, list)
end

function Gas2Gac.SetAddFriendLvThreashold(lv)

    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        MainPlayer.RelationshipProp.AddFriendLvThreashold = lv
    end
end

function Gas2Gac.SetIgnoreAddFriendRequest(bIgnore)

    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        if bIgnore then
            MainPlayer.RelationshipProp.IgnoreAddFriendRequest = 1
        else
            MainPlayer.RelationshipProp.IgnoreAddFriendRequest = 0
        end
    end
end

function Gas2Gac.ReplyJiaNianHuaTicketSellInfo(dataUD)

    local list = MsgPackImpl.unpack(dataUD)
    local ticketInfo = {}
    for i = 0, list.Count-1, 2 do
        local goodsId = tonumber(list[i])
        local stock = tonumber(list[i+1])

        table.insert(ticketInfo,{GoodsId = goodsId,Stock = stock})
    end
    LuaWelfareMgr.Carnival2021TicketSellInfo(ticketInfo)
end

function Gas2Gac.PlayerUseFuXiLabaSuccess(curCombo)
    g_ScriptEvent:BroadcastInLua("PlayerUseFuXiLabaSuccess", curCombo)
end

function Gas2Gac.SendSeaFishingPlayFishScore(finishTime, playerScoreData)
    local list = MsgPackImpl.unpack(playerScoreData)
    local fishingInfo = {}
    for i = 0, list.Count-1, 3 do
        local t = {}
        t.playerId = tonumber(list[i])
        t.playerName = tonumber(list[i+1])
        t.score = tonumber(list[i+2])
        table.insert(fishingInfo,t)
    end
    --LuaSeaFishingMgr.OnUpdateSeaFishingPlayFishScore(finishTime,fishingInfo)
    g_ScriptEvent:BroadcastInLua("UpdateSeaFishingPlayFishScore", finishTime,fishingInfo)
end

function Gas2Gac.SetWaveStrength(height, strength)
    EventManager.BroadcastInternalForLua(EnumEventType.UpdateFFTOceanParams, {strength, height})
end

function Gas2Gac.YeSheng_OpenThreadWnd(selectableGroupIds_U)
    print("YeSheng_OpenThreadWnd", selectableGroupIds_U)

    local groupIds = MsgPackImpl.unpack(selectableGroupIds_U)
    CommonDefs.ListIterate(groupIds, DelegateFactory.Action_object(function (groupId)
        print(groupId)
    end))
end

function Gas2Gac.SendGetRepairSkillInSeaFishingPlay(skillId)
    LuaSeaFishingMgr.HidePickUpWnd()
end

function Gas2Gac.SendOpenExchangeLiuHeYeJingWnd(currentQingLaiZhi, remainCount)
    print("SendOpenExchangeLiuHeYeJingWnd", currentQingLaiZhi, remainCount)
    LuaInviteNpcMgr.CurrentQingLaiZhi = currentQingLaiZhi
    LuaInviteNpcMgr.RemainExchangeIncreaseDaoyiValueItemCount = remainCount
    CUIManager.ShowUI(CLuaUIResources.BuyIncreaseDaoyiValueItemWnd)
end

function Gas2Gac.PersonalSpaceSetSecretLove(targetId, bIsSuccess, bIsBoth)
    if bIsSuccess then
        LuaPersonalSpaceMgrReal:ComfirmSlientLoveRet(targetId, bIsSuccess, bIsBoth)
    end
end

function Gas2Gac.PersonalSpaceCancelSecretLove(targetId, bIsSuccess)
    if bIsSuccess then
        LuaPersonalSpaceMgrReal:ComfirmSlientLoveRet(targetId, bIsSuccess, false)
    end
end

function Gas2Gac.SyncZhengWuLuItemStatus(zhengWuId, isUnlock, isViewed)
    -- print("SyncZhengWuLuItemStatus", zhengWuId, isUnlock, isViewed)
    CClientMainPlayer.Inst.PlayProp.ZhengWuLuUnlockInfo:SetBit(zhengWuId, isUnlock)
    CClientMainPlayer.Inst.PlayProp.ZhengWuLuViewInfo:SetBit(zhengWuId, isViewed)
    LuaZhengWuLuMgr.UpdateState(zhengWuId, isUnlock, isViewed)
end

function Gas2Gac.SyncRecommendIgnoreBigData(status) 
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.PlayProp then
        mainplayer.PlayProp.RecommendIgnoreBigData = (status and 1 or 0)
    end
end

function Gas2Gac.OpenScrollTaskWnd(openType, subtitleId, pictureKey, layout)

    LuaScrollTaskMgr:OpenScrollTaskWnd(openType, subtitleId, pictureKey, layout)
end

function Gas2Gac.CloseScrollTaskWnd(closeType)

    LuaScrollTaskMgr:CloseScrollTaskWnd(closeType)
end

function Gas2Gac.ShowScrollTaskSubTitle(subtitleId, pictureKey, layout)

    LuaScrollTaskMgr:ShowScrollTaskSubTitle(subtitleId, pictureKey, layout)
end

function Gas2Gac.HideScrollTaskSubTitle(subtitleId, pictureKey)

    LuaScrollTaskMgr:HideScrollTaskSubTitle(subtitleId, pictureKey)
end

function Gas2Gac.SetNextLoadingPicToCurrentScroll()

    LuaScrollTaskMgr:SetNextLoadingPicToCurrentScroll()
end

function Gas2Gac.SetNextLoadingPicToScroll(subtitleId, pictureKey, layout)

    LuaScrollTaskMgr:SetNextLoadingPicToScroll(subtitleId, pictureKey, layout)
end

function Gas2Gac.CancelSetNextLoadingPicToScroll()

    LuaScrollTaskMgr:CancelSetNextLoadingPicToScroll()
end

function Gas2Gac.ScrollTaskCaptureScreen(pictureKey)
    LuaScrollTaskMgr:ScrollTaskCaptureScreen(pictureKey)
end

function Gas2Gac.ScrollTaskChangeLayout(subtitleId, pictureKey, layout)

    LuaScrollTaskMgr:ScrollTaskChangeLayout(subtitleId, pictureKey, layout)
end

function Gas2Gac.WaitForShowScrollTaskSubTitle(subtitleId)

    LuaScrollTaskMgr:WaitForShowScrollTaskSubTitle(subtitleId)
end

function Gas2Gac.PlayScrollTaskQTE(qteId, duration)
    LuaScrollTaskMgr:PlayScrollTaskQTE(qteId, duration)
end

function Gas2Gac.StartScrollTaskGameEvent(taskId, eventName)
    LuaScrollTaskMgr:StartScrollTaskGameEvent(taskId, eventName)
end

function Gas2Gac.StartMusicRun(musicId)
    LuaScrollTaskMgr:StartMusicRun(musicId)
end

function Gas2Gac.StopMusicRun(musicId)
    LuaScrollTaskMgr:StopMusicRun(musicId)
end

function Gas2Gac.OpenTaskWipeScreenTearsWnd(taskId)
    CLuaTaskMgr.m_WipeScreenTearsWnd_TaskId = taskId
    CUIManager.CloseUI(CLuaUIResources.WipeTearsWnd)
	CUIManager.ShowUI(CLuaUIResources.WipeTearsWnd)
end

function Gas2Gac.StartClickScreenLightning(taskId)
    CLuaTaskMgr.YingHuoWeiGuangTaskID = taskId
    CLuaTaskMgr.YingHuoWeiGuangType = 1
	CUIManager.ShowUI(CLuaUIResources.YingHuoWeiGuangWnd)
end

function Gas2Gac.StartClickScreenLightning2(taskId)
    CLuaTaskMgr.YingHuoWeiGuangTaskID = taskId
    CLuaTaskMgr.YingHuoWeiGuangType = 2
	CUIManager.ShowUI(CLuaUIResources.YingHuoWeiGuangWnd)
end

function Gas2Gac.SaveCombDataSuccess(taskId, material, decoration, shape)
    LuaScrollTaskMgr:SaveCombDataSuccess(taskId, material, decoration, shape)
end

function Gas2Gac.PlayCombPicture(taskId, material, decoration, shape, duration, width, height)
    LuaScrollTaskMgr:PlayCombPicture(taskId, material, decoration, shape, duration, width, height)
end

function Gas2Gac.ForceSetBgMusic(eventName, needShowLyric, lyricId)
    if needShowLyric and lyricId then
        --确保关掉音乐开关时也能正确显示歌词
        if not L10.Game.PlayerSettings.MusicEnabled then
            local volum = 0
            SoundManager.Inst:ChangeBgMusicVolume("ShowLyricWithoutMusicEnable", volum, true)
            SoundManager.Inst.m_bgMusic = SoundManager.Inst:PlaySound(eventName, Vector3.zero, volum, nil, 0)
        else
            SoundManager.Inst:ForceSetBgMusic(eventName)
        end        
        LuaJuQingLyricMgr.ShowLyricWnd(eventName,lyricId)
        return
    end
    SoundManager.Inst:ForceSetBgMusic(eventName)
end

-- 竞技场
function Gas2Gac.SendArenaInfo(myMaxRankScore, myArenaPlayScore, weeklyBattleTimes, weeklyWinTimes, weeklyScoreRewardState, matching, matchType, weeklyGetShopScore, weeklyGetShopScoreLimit, BattleRecord)
    LuaColiseumMgr:SendArenaInfo(myMaxRankScore, myArenaPlayScore, weeklyBattleTimes, weeklyWinTimes, weeklyScoreRewardState, matching, matchType, weeklyGetShopScore, weeklyGetShopScoreLimit, BattleRecord)
end

function Gas2Gac.SyncArenaSignupState(matching, matchType)
    LuaArenaMgr:SyncArenaSignupState(matching, matchType)
end

function Gas2Gac.SendArenaClassRecord(classRecord)
    LuaColiseumMgr:SendArenaClassRecord(classRecord)
end

function Gas2Gac.SendArenaScoreShopData(shopScore, nextAutoRefreshTime, goodsLimitData, goodsData)
    LuaColiseumMgr:SendArenaScoreShopData(shopScore, nextAutoRefreshTime, goodsLimitData, goodsData)
end

function Gas2Gac.SendArenaRankInfo(seasonId, rankType, page, totalPage, myScore, myEquipScore, myRank, rankInfo)
    LuaColiseumMgr:SendArenaRankInfo(seasonId, rankType, page, totalPage, myScore, myEquipScore, myRank, rankInfo)
end

function Gas2Gac.SendArenaCrossServerRankInfo(seasonId, levelType, myScore, myEquipScore, myRank, rankInfo)
    LuaColiseumMgr:SendArenaCrossServerRankInfo(seasonId, levelType, myScore, myEquipScore, myRank, rankInfo)
end

function Gas2Gac.SyncWeeklyArenaScoreRewardState(weeklyBattleTimes, weeklyWinTimes, rewardState, currentShopScore)
    LuaColiseumMgr:SyncWeeklyArenaScoreRewardState(weeklyBattleTimes, weeklyWinTimes, rewardState, currentShopScore)
end

function Gas2Gac.SendArenaPlayResult(matchType, winType, changeRankScore, newRankScore, addShopScore, newShopScore, winComboTimes, playerData)
    LuaColiseumMgr:SendArenaPlayResult(matchType, winType, changeRankScore, newRankScore, addShopScore, newShopScore, winComboTimes, playerData)
end

-- 竞技场迭代
function Gas2Gac.SendNewArenaInfo(rankScore, shopScore, winCount, favCount, isPlayOpen, matchType, passportData, taskData, friendData)
    LuaArenaMgr:SendNewArenaInfo(rankScore, shopScore, winCount, favCount, isPlayOpen, matchType, passportData, taskData, friendData)
end
function Gas2Gac.SendPlayerArenaDetails(targetId, name, gender, class, level, isFeiSheng, appearanceProp, arenaDetails, mateData)
    LuaArenaMgr:SendPlayerArenaDetails(targetId, name, gender, class, level, isFeiSheng, appearanceProp, arenaDetails, mateData)
end
function Gas2Gac.SendArenaOpenInfo(matchType, openedTypeData)
    LuaArenaMgr:SendArenaOpenInfo(matchType, openedTypeData)
end
function Gas2Gac.SendNewArenaRankInfo(myScore, myRank, seasonId, levelType, isCross, page, totalPage, rankData)
    LuaArenaMgr:SendNewArenaRankInfo(myScore, myRank, seasonId, levelType, isCross, page, totalPage, rankData)
end
function Gas2Gac.SendArenaTaskInfo(currentWeek, openedWeek, activedWeek, starData, taskData, finishedWeekData)
    LuaArenaMgr:SendArenaTaskInfo(currentWeek, openedWeek, activedWeek, starData, taskData, finishedWeekData)
end
function Gas2Gac.SetArenaActiveTaskWeekResult(activedWeek)
    LuaArenaMgr:SetArenaActiveTaskWeekResult(activedWeek)
end

function Gas2Gac.SendArenaPassportInfo(curExp, curLevel, rewardedLevel, weekExp, weekExpLimit)
    LuaArenaMgr:SendArenaPassportInfo(curExp, curLevel, rewardedLevel, weekExp, weekExpLimit)
end

function Gas2Gac.SendArenaPassportGiftInfo(jadeLimit, gotLevel, friendData)
    LuaArenaMgr:SendArenaPassportGiftInfo(jadeLimit, gotLevel, friendData)
end

function Gas2Gac.SendArenaPassportRankInfo(myRank, rankType, rankData)
    LuaArenaMgr:SendArenaPassportRankInfo(myRank, rankType, rankData)
end

function Gas2Gac.SendNewArenaPlayResult(matchType, winType, changeRankScore, newRankScore, addShopScore, totalShopScore, winComboTimes, playStartTime, weekPassportExp, weekPassportExpLimit, teamInfo, playerData, taskData)
    LuaArenaMgr:SendNewArenaPlayResult(matchType, winType, changeRankScore, newRankScore, addShopScore, totalShopScore, winComboTimes, playStartTime, weekPassportExp, weekPassportExpLimit, teamInfo, playerData, taskData)
end

function Gas2Gac.SyncArenaPlayerJingYuanInfo(playerInfoUd)
    LuaArenaJingYuanMgr:SyncArenaPlayerJingYuanInfo(playerInfoUd)
end

function Gas2Gac.SyncArenaTeamJingYuanInfo(teamInfoUd)
    LuaArenaJingYuanMgr:SyncArenaTeamJingYuanInfo(teamInfoUd)
end

function Gas2Gac.ArenaJYPrepareSpawn(srcEngineId, isCenter)
end

function Gas2Gac.ArenaJYSpawn(srcEngineId, isCenter, spawnInfo)
end

function Gas2Gac.ArenaTalentInfo(score, talentInfo)
    talentInfo = talentInfo and g_MessagePack.unpack(talentInfo)
    for talentId, info in pairs(talentInfo) do
        local level = info.level
        local activedSubTalentId = info.subTalentId
    end
end

function Gas2Gac.SyncArenaTalentLevel(score, telentId, subTalentId, newLevel)
end

function Gas2Gac.SyncGTWZhanShenTaskInfo(acceptedTaskId, progress, isFinish)
    LuaGuildTerritorialWarsMgr:SyncGTWZhanShenTaskInfo(acceptedTaskId, progress, isFinish)
end

function Gas2Gac.SendGTWZhanShenTaskChooseData(acceptedTaskId, taskIds)
    LuaGuildTerritorialWarsMgr:SendGTWZhanShenTaskChooseData(acceptedTaskId, taskIds)
end

function Gas2Gac.YinXianWan_QueryFurnitureInfoResult(furnitureId, userId)
end

function Gas2Gac.UpdateGamePlayStageInfo(gameplayId, specifiedId, title, content, autoSwitch)
    if CommonDefs.IS_VN_CLIENT then
        content = string.gsub(content, "\\n", "\n")
    end
    
    LuaGamePlayMgr.s_TaskStageGamePlayId = gameplayId
    LuaGamePlayMgr.s_TaskSategTitle = title
    LuaGamePlayMgr.s_TaskContent = content
    --wyh 暂时放这，之后改到Mgr
    LuaShenYaoTaskView.m_TaskStageGamePlayId = gameplayId
    LuaShenYaoTaskView.m_TaskSategTitle = title
    LuaShenYaoTaskView.m_TaskContent = content
    --留一条消息给副本用
    g_ScriptEvent:BroadcastInLua("UpdateGamePlayStageInfo",gameplayId,title,content)
    LuaChristmas2021Mgr.m_TaskStageGamePlayId = gameplayId
    LuaChristmas2021Mgr.m_TaskSategTitle = title
    LuaChristmas2021Mgr.m_TaskContent = content

    if autoSwitch and CTaskAndTeamWnd.Instance then
        CTaskAndTeamWnd.Instance:ChangeToTaskTab()
    end
    

    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.PlayProp and mainplayer.PlayProp.PlayId == gameplayId then
        local key =  SafeStringFormat3("%s_%s", gameplayId, specifiedId)
        if content == "" or content == nil then
            CActivityMgr.Inst:DeleteActivity(key) --留一个移除的处理，以防万一之后有移除的需求
        else
            CActivityMgr.Inst:UpdateAcitivity(key, content, gameplayId, nil, 0, title, true)
        end
    end
end

function Gas2Gac.UpdateHuaCheMapPos(huaCheEngineId, posX, posY)
    LuaHalloween2021Mgr:UpdateHuaCheMapPos(huaCheEngineId,posX, posY)
end

function Gas2Gac.SyncHalloweenYouHuaChePlayInfo(stage)
    LuaHalloween2021Mgr:SyncHalloweenYouHuaChePlayInfo(stage)
end

function Gas2Gac.TianJiangBaoXiang_SyncPlayState(isPlayOpen, startTime, totalAddKeysCount, totalKeys_ChiTong, totalKeys_FeiCui, isConsume) 
    LuaTianJiangBaoXiangMgr.totalAddKeysCount = totalAddKeysCount
    LuaTianJiangBaoXiangMgr.totalKeys_ChiTong = totalKeys_ChiTong
    LuaTianJiangBaoXiangMgr.totalKeys_FeiCui = totalKeys_FeiCui
    if not isConsume then
        LuaTianJiangBaoXiangMgr.StartPlayTimeStamp = startTime
        LuaTianJiangBaoXiangMgr.IsPlayOpen = isPlayOpen
        g_ScriptEvent:BroadcastInLua("SyncTianJiangBaoXiangStatus")
    end
end

function Gas2Gac.TianJiangBaoXiang_LevelOne(isRare, keyNumTbl_U, openTimesTbl_U, context) 
    local keyNumInfo = MsgPackImpl.unpack(keyNumTbl_U)
    local openTimesInfo = MsgPackImpl.unpack(openTimesTbl_U)
    --TODO
    LuaTianJiangBaoXiangMgr.ShowLimitInfo = isRare
    LuaTianJiangBaoXiangMgr.GetKeyNumInfo(keyNumInfo)
    LuaTianJiangBaoXiangMgr.GetOpenTimesInfo(openTimesInfo)
    if tonumber(context) == EnumTianJiangBaoXiangDoor.None then 
        LuaTianJiangBaoXiangMgr.ShowTheFirstDoor = EnumTianJiangBaoXiangDoor.FeiCui --默认开翡翠门
    else
        LuaTianJiangBaoXiangMgr.ShowTheFirstDoor = tonumber(context)
    end
    LuaTianJiangBaoXiangMgr.OpenTianJiangBaoXiangWnd(1) 
end

function Gas2Gac.TianJiangBaoXiang_LevelTwo(KeyType, rewardTbl_U, canEnterBossPlay, refreshYuanBao, openTimesTbl_U) 
    local rewardTbl = MsgPackImpl.unpack(rewardTbl_U)
    local openTimesInfo = MsgPackImpl.unpack(openTimesTbl_U)
    --TODO
    LuaTianJiangBaoXiangMgr.PlayerChooseDoor = KeyType
    LuaTianJiangBaoXiangMgr.RewardTbl = rewardTbl
    LuaTianJiangBaoXiangMgr.ShowBossEntrance = canEnterBossPlay
    LuaTianJiangBaoXiangMgr.YuanBaoAmount = refreshYuanBao
    LuaTianJiangBaoXiangMgr.GetOpenTimesInfo(openTimesInfo)
    LuaTianJiangBaoXiangMgr.OpenTianJiangBaoXiangWnd(2) 
end

function Gas2Gac.TianJiangBaoXiang_Reward(itemTemplateId) 
    --TODO
    g_ScriptEvent:BroadcastInLua("TianJiangBaoXiangRewardFinish",itemTemplateId)
end

function Gas2Gac.SyncSkinColor(engineId, skinColor)
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj and (TypeIs(obj, typeof(CClientOtherPlayer)) or TypeIs(obj, typeof(CClientMainPlayer))) then
        obj.AppearanceProp.SkinColor = skinColor
        if obj.AppearanceProp.ResourceId == 0 then
            obj:ProcessSunburnSkinColor(skinColor)
        end
    end
end

function Gas2Gac.OpenJieLiangYuanSignUpWnd(bSignUp, dailyAwardTimes)
    LuaHalloween2021Mgr:OpenJieLiangYuanSignUpWnd(bSignUp, dailyAwardTimes)
end

function Gas2Gac.SyncJieLiangYuanSignUpResult(bSignUp)
    LuaHalloween2021Mgr:SyncJieLiangYuanSignUpResult(bSignUp)
end

function Gas2Gac.JieLianYuanNotifyNeiGui(neiGuiPlayerId)
    LuaHalloween2021Mgr:JieLianYuanNotifyNeiGui(neiGuiPlayerId)
end

function Gas2Gac.JieLiangYuanSyncPlayInfo(stage, isNeiGui, puzzleListUD)
    LuaHalloween2021Mgr:JieLiangYuanSyncPlayInfo(stage, isNeiGui, puzzleListUD)
end

function Gas2Gac.JieLiangYuanSyncVoteNeiGuiInfo(expireTime, voteInfoUD, votePlayerId, voteConfirmed, voteEnd, neiGuiPlayerId, result, canGetAward, awardGot, forceOpen)
	LuaHalloween2021Mgr:JieLiangYuanSyncVoteNeiGuiInfo(expireTime, voteInfoUD, votePlayerId, voteConfirmed, voteEnd, neiGuiPlayerId, result, canGetAward, awardGot,  forceOpen)
end

function Gas2Gac.JieLiangYuanGetAwardSuccess()
    LuaHalloween2021Mgr:JieLiangYuanGetAwardSuccess()
end

function Gas2Gac.Double11DSG_QueryTaskResult(taskTbl_U, leftFreeTimes, acceptTimes)
    LuaShuangshiyi2021Mgr:SyncEntrustTaskInfo(taskTbl_U, leftFreeTimes, acceptTimes)
end

function Gas2Gac.Double11KYD_SyncPlayState(playState, eventTbl_U)
    LuaShuangshiyi2021Mgr:SyncKanYiDaoTaskInfo(playState, eventTbl_U)
end

function Gas2Gac.RequestUsePresentItemMustBeFriend(itemId, recieverId, recieverLevel)
    print("RequestUsePresentItemMustBeFriend", itemId, recieverId, recieverLevel)
end

function Gas2Gac.SyncPrepareSendSectItemRedPackInfo(remainJadeLimit, remainSendTimes, lastSendItems, dynamicOnShelfItems)
    local list = lastSendItems and MsgPackImpl.unpack(lastSendItems)
    local lastItemList = {}
    for i = 0, list.Count-1, 2 do
        local itemId = list[i]
        local itemCount = list[i+1]
        table.insert(lastItemList,{itemId = itemId, num = itemCount})
    end

    local dynamicOnShelfItemsSet = {}
    list = dynamicOnShelfItems and MsgPackImpl.unpack(dynamicOnShelfItems)
    for i = 0, list.Count-1 do
        local itemId = list[i]
        dynamicOnShelfItemsSet[itemId] = true
        print("dynamicOnShelfItems", itemId)
    end
    LuaZongMenMgr.m_HongBaoDynamicOnShelfItemsSet = dynamicOnShelfItemsSet
    g_ScriptEvent:BroadcastInLua("OnSyncPrepareSendSectItemRedPackInfo",remainJadeLimit, remainSendTimes,lastItemList)
end

function Gas2Gac.SendSectItemPackOverview(infoUD)
    --print("SendSectItemPackOverview")
    local list = infoUD and MsgPackImpl.unpack(infoUD)
    local hongbaoList = {}
    for i = 0, list.Count-1, 7 do
        local packId = list[i] -- 红包ID
        local ownerId = list[i+1] -- 发红包玩家ID
        local ownerName = list[i+2] -- 发红包玩家名
        local content = list[i+3] -- 红包内容
        local isSnatch = list[i+4] -- 是否已抢
        local state = list[i+5] -- 红包状态 0 进行中 1 抢光了 2 没抢光且过期了
        local sendTime = list[i+6] -- 红包发放时间

        local hongbao = {
            m_Content = content,
            m_OwnerName = ownerName,
            m_HongbaoId = packId,
            m_OwnerId = ownerId,
            m_Status = isSnatch and 2 or (state == 1 and 3 or (state == 2 and 4 or 1)),
            m_HongbaoAmount = 1,
            m_SendTime = sendTime,
        }
        hongbao.m_Content = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(hongbao.m_OwnerId, hongbao.m_Content, nil, false) or ""
        table.insert( hongbaoList,hongbao )
        --print("SendSectItemPackOverview Pack:", packId, ownerId, ownerName, content, isSnatch, state)
    end
    table.sort(hongbaoList,function(a, b)
        return a.m_SendTime > b.m_SendTime
    end)
    CLuaHongBaoMgr.m_HongbaoDataTable[EnumHongBaoDisplayType.eSect] = hongbaoList
    g_ScriptEvent:BroadcastInLua("ReplyRecentHongBaoInfo")
end

function Gas2Gac.SendSectItemPackDetail(packId, ownerId, ownerName, content, gender, class, jade, state, finishPlayerCount, totalCount, detailUD)
    -- 红包id, 发红包的玩家id, 发红包的玩家名, 红包内容, 发红包的玩家性别, 发红包的玩家职业, 灵玉价值, 红包状态, 已领取人数
    -- 红包状态 0 进行中 1 抢光了 2 没抢光且过期了
    local list = detailUD and MsgPackImpl.unpack(detailUD)
    LuaZongMenMgr.m_SectHongBaoId = packId
    print("SendSectItemPackDetail", packId, ownerId, ownerName, content, gender, class, jade, state, finishPlayerCount,list.Count)
    LuaZongMenMgr.m_SectHongBaoItemPlayerData = {
        packId = packId, ownerId = ownerId, ownerName = ownerName, content = content, totalCount = totalCount,
        gender = gender, class = class, jade = jade, state = state,  finishPlayerCount = finishPlayerCount
    }
    LuaZongMenMgr.m_SectHongBaoItemDetailData = {}
    for i = 0, list.Count-1, 7 do
        local playerId = list[i]
        local playerName = list[i+1]
        local status = list[i+2] -- 0 已抽 1 抽奖中 2 排队中
        local pos = list[i+3] -- 排队时的位置
        local itemId = list[i+4] -- 已抽时获得的道具ID
        local itemCount = list[i+5] -- 已抽时获得的道具数量
        local isBest = list[i+6] -- 是否最佳
        print("SendSectItemPackDetail queue:", playerId, playerName, status, pos, itemId, itemCount, isBest)
        table.insert(LuaZongMenMgr.m_SectHongBaoItemDetailData,{
            playerId = playerId, playerName = playerName,status = status,
            pos = pos, itemId = itemId, itemCount = itemCount,isBest = isBest
        })
    end
    if CUIManager.IsLoaded(CLuaUIResources.SectHongbaoDetailNewWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendSectItemPackDetail")
        return 
    end
    CUIManager.ShowUI(CLuaUIResources.SectHongbaoDetailNewWnd)
end

function Gas2Gac.SendSectItemPackLotteryInfo(currentLotteryPlayerId, currentLotteryPlayerName, myStatus, myQueuePos, myLotteryDeadline, playAnimate, animateFinishTime, animateStopPos, lotteryInfoUD)
    -- 当前抽奖玩家ID 当前抽奖玩家名 我的抽奖状态 我在等待抽奖队列中的位置 我的抽奖结束时间戳 是否播放动画 动画结束时间戳 动画结束位置
    -- 我的抽奖状态 0 未参与抽奖 1 已抽完 2 当前是自己抽且未确认参加抽奖 3 当前是自己抽且未点抽奖按钮 4 当前是自己抽且已点抽奖按钮 5 还在排队等待抽奖
    print("SendSectItemPackLotteryInfo", currentLotteryPlayerId, currentLotteryPlayerName, myStatus, myQueuePos, myLotteryDeadline, playAnimate, animateFinishTime, animateStopPos)
    g_ScriptEvent:BroadcastInLua("OnSendSectItemPackLotteryInfo",currentLotteryPlayerId, currentLotteryPlayerName, myStatus, myQueuePos, myLotteryDeadline,playAnimate, animateFinishTime, animateStopPos, lotteryInfoUD)
end

function Gas2Gac.SendWinSectItemPack(ownerId, ownerName, itemId, itemCount, isBest)
    print("SendWinSectItemPack", ownerId, ownerName, itemId, itemCount, isBest)
    g_ScriptEvent:BroadcastInLua("OnSendWinSectItemPack")
    LuaZongMenMgr.m_GetSectHongBaoResultData = { ownerName = ownerName, itemId = itemId, itemCount = itemCount, isBest = isBest}
    CUIManager.ShowUI(CLuaUIResources.GetSectHongBaoWnd) 
    if CUIManager.IsLoaded(CLuaUIResources.SectHongbaoDetailNewWnd) then
        Gac2Gas.RequestSectItemRedPackDetail(LuaZongMenMgr.m_SectHongBaoId)   
    end
    if CUIManager.IsLoaded(CLuaUIResources.SectHongBaoLotteryWnd) then
        Gac2Gas.RequestWatchSectItemRedPackLottery(LuaZongMenMgr.m_SectHongBaoId)   
    end
end

function Gas2Gac.SendConfirmJoinLottery(sectId, packId, ownerId, ownerName, duration)
    --print("SendConfirmJoinLottery", sectId, packId, ownerId, ownerName, duration)
    LuaZongMenMgr.m_SectHongBaoId = packId
    local msg = g_MessageMgr:FormatMessage("SectHongBao_JoinLottery_Confirm",ownerName)
    MessageWndManager.ShowDefaultConfirmMessage(msg,SectItemRedPack_Setting.GetData().ConfirmTimePerPlayer, false ,DelegateFactory.Action(function()
        Gac2Gas.ConfirmJoinSectItemPackLottery(CClientMainPlayer.Inst.BasicProp.SectId, LuaZongMenMgr.m_SectHongBaoId, true)
        CUIManager.ShowUI(CLuaUIResources.SectHongBaoLotteryWnd)
    end),DelegateFactory.Action(function()
        Gac2Gas.ConfirmJoinSectItemPackLottery(sectId, packId, false)
    end), true,nil,LocalString.GetString("确定"), LocalString.GetString("放弃"))
end

function Gas2Gac.PlayerSendSectItemHongBao(channelId, playerId, playerName, class, gender, expression, expressionTxt, sticker, sectId, packId, contents)
    print("PlayerSendSectItemHongBao", channelId, playerId, playerName, class, gender, expression, expressionTxt, sticker, sectId, packId, contents)
    contents = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(playerId, contents, nil, false)
    if contents == nil then
        contents = ""
    end
    if not CClientMainPlayer.Inst then return end
    local channelType = EnumToInt(EnumHongbaoType.SectHongbao)
    local hongbaoMsg = CHongBaoMsg.Encapsulate(channelId, channelType, contents, packId, EnumDisplayHongBaoType.eHongBao, 1)
    EventManager.BroadcastInternalForLua(EnumEventType.PlayerSendHongBao, {channelType, 1})
    CChatMgr.Inst:AddChatMsg(channelId, playerName, 0, playerId, class, gender, expression, expressionTxt, sticker, hongbaoMsg, "", true, true, nil)
end

function Gas2Gac.SendSectItemRedPackHistory(sendTimes, sendTotalJade, recvTimes, recvTotalJade, historyUD)
    local list = historyUD and MsgPackImpl.unpack(historyUD)
    local datalist = {}
    for i = 0, list.Count-1, 3 do
        local recvTime = list[i]
        local recvItemId = list[i+1]
        local recvItemCount = list[i+2]
        table.insert(datalist,1,{timestamp = recvTime,itemId = recvItemId,itemNum = recvItemCount})
    end
    g_ScriptEvent:BroadcastInLua("SendSectItemRedPackHistory",sendTimes, sendTotalJade, recvTimes, recvTotalJade,datalist)
end

function Gas2Gac.XinShengHuaJi_AddPitureFailed(reason, DetailInfo_U)
    LuaWuMenHuaShiMgr:AddPictureFailed(reason, DetailInfo_U)
end

function Gas2Gac.XinShengHuaJi_AddPitureSuccess(time, data_U)
    LuaWuMenHuaShiMgr:AddPictureSuccess(time, data_U)
end

function Gas2Gac.XinShengHuaJi_DelPitureSuccess(time)
    LuaWuMenHuaShiMgr:DelPictureSuccess(time)
end

function Gas2Gac.XinShengHuaJi_QueryPictures(data_U)
    LuaWuMenHuaShiMgr:SyncPictures(data_U)
end

function Gas2Gac.XinShengHuaJi_AddTaskPic(taskId, data)
    LuaWuMenHuaShiMgr:SyncAddTaskPic(taskId)
end

function Gas2Gac.XinShengHuaJi_ShowTaskPic(data_U, context, taskId, duration)
    LuaWuMenHuaShiMgr:ShowTaskPic(data_U, taskId, duration)
end

function Gas2Gac.XinShengHuaJi_SetHuaJiNum(num)
    LuaWuMenHuaShiMgr:SetHuaJiNum(num)
end

function Gas2Gac.XinShengHuaJi_ReadPictureSuccess(time)
    LuaWuMenHuaShiMgr:ReadPictureSuccess(time)
end

function Gas2Gac.XinShengHuaJi_Unlock(choice, sn)
    LuaWuMenHuaShiMgr:UnlockSuccess(choice, sn)
end

function Gas2Gac.XinShengHuaJi_UnlockFailed(choice, templateId, lackCount, needCount)
    LuaWuMenHuaShiMgr:UnlockFail(choice, templateId, lackCount, needCount)
end

function Gas2Gac.XinShengHuaJi_QueryRankPicResult(playerId, name, pic_U, rankData_U)
    LuaWuMenHuaShiMgr:QueryRankPicResult(playerId, name, pic_U, rankData_U)
end

function Gas2Gac.XinShengHuaJi_VoteResult(playerId, voteNum)
    LuaWuMenHuaShiMgr:VoteResult(playerId, voteNum)
end

function Gas2Gac.XinShengHuaJi_AddRankPicResult(bSuccess)
    LuaWuMenHuaShiMgr:AddRankPicResult(bSuccess)
end

function Gas2Gac.RuMengJi_SetState(npcChoice, state)
    LuaWuMenHuaShiMgr:SyncRuMengJiNpcSetState(npcChoice, state)
end

function Gas2Gac.RuMengJi_QueryNpcStateResult(npcState_U)
    LuaWuMenHuaShiMgr:SyncRuMengJiNpcStateResult(npcState_U)
end

function Gas2Gac.SyncSeaFishingPlayEntranceStatus(isOpen, closeTime)
    g_ScriptEvent:BroadcastInLua("SyncSeaFishingPlayEntranceStatus",isOpen, closeTime)
end

function Gas2Gac.ChangeTaskVisibility(taskId, visibility)
    CLuaTaskMgr.UpdateTempHiddenTasksInTrackPanel(taskId, visibility)
end

function Gas2Gac.Watch_PushPlayData(playId, sceneId, key, data_U)
    print("Watch_PushPlayData", playId, sceneId, key, data_U)
end

function Gas2Gac.WashPermanentPropSuccess(consumeType, index, times, before_U, after_U)
    LuaExtraPropertyMgr:WashPermanentPropSuccess(consumeType, index, times, before_U, after_U)
end

function Gas2Gac.Xingguan_SwitchTemplateSuccess(templateId)
	--print("Xingguan_SwitchTemplateSuccess", templateId)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        CClientMainPlayer.Inst.PlayProp.XingguanProperty.ActiveTemplateId = templateId
    end
    g_ScriptEvent:BroadcastInLua("SwitchXingguanTemplateSuccess", templateId)
end

function Gas2Gac.Xingguan_SyncEffectOverview(activeTemplateId, allPropListUD, statusPropListUD, paramSumListUD, xingguanIdListUD)
	--print("Xingguan_SyncEffectOverview Begin", activeTemplateId)
	local allPropList = MsgPackImpl.unpack(allPropListUD)
    local allPropTable = {}
	for i = 0, allPropList.Count - 1, 2 do
		local propIdx = allPropList[i]
		local val = allPropList[i+1]
        table.insert(allPropTable, {propIdx = propIdx, val = val})
		--print("Xingguan_SyncEffectOverview allProp", propIdx, val)
	end

	local statusPropList = MsgPackImpl.unpack(statusPropListUD)
    local statusPropTable = {}
	for i = 0, statusPropList.Count - 1, 3 do
		local statusIdx = statusPropList[i]
		local propIdx = statusPropList[i+1]
		local val = statusPropList[i+2]
        table.insert(statusPropTable, {statusIdx = statusIdx, propIdx = propIdx, val = val})
		--print("Xingguan_SyncEffectOverview statusProp", statusIdx, propIdx, val)
	end

	local paramSumList = MsgPackImpl.unpack(paramSumListUD)
    local paramSumTable = {}
	for i = 0, paramSumList.Count - 1, 2 do
		local gameEventBonus = paramSumList[i]
		local param = paramSumList[i+1]
        table.insert(paramSumTable, {gameEventBonus = gameEventBonus, param = param})
		--print("Xingguan_SyncEffectOverview paramSum", gameEventBonus, param)
	end

	local xingguanIdList = MsgPackImpl.unpack(xingguanIdListUD)
    local xingguanIdTable = {}
	for i = 0, xingguanIdList.Count - 1 do
        table.insert(xingguanIdTable, xingguanIdList[i])
		--print("Xingguan_SyncEffectOverview xingguanId", xingguanIdList[i])
	end
	--print("Xingguan_SyncEffectOverview End")

    g_ScriptEvent:BroadcastInLua("SyncXingguanEffectOverview", activeTemplateId, allPropTable, statusPropTable, paramSumTable, xingguanIdTable)
end

function Gas2Gac.GlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    -- print("GlobalMatch_CheckInMatchingResult", playerId, playId, isInMatching, resultStr)
    g_ScriptEvent:BroadcastInLua("GlobalMatch_CheckInMatchingResult",playerId, playId, isInMatching, resultStr)
end

function Gas2Gac.GlobalMatch_CheckInMatchingResultWithInfo(playerId, playId, isInMatching, resultStr, info_U)
    print("GlobalMatch_CheckInMatchingResultWithInfo", playerId, playId, isInMatching, resultStr, info_U)

    local info = nil
    if MsgPackImpl.IsValidUserData(info_U) then
        info = g_MessagePack.unpack(info_U)
        for k, v in pairs(info) do
            print(k,v )
        end
    end

    g_ScriptEvent:BroadcastInLua("GlobalMatch_CheckInMatchingResultWithInfo", playerId, playId, isInMatching, resultStr, info)
end

function Gas2Gac.GlobalMatch_SignUpPlayResult(playId, success)
    g_ScriptEvent:BroadcastInLua("GlobalMatch_SignUpPlayResult",playId, success)
end

function Gas2Gac.GlobalMatch_SignUpPlayResultWithInfo(playId, success, info_U)
    print("GlobalMatch_SignUpPlayResultWithInfo", playId, success)

    local info = nil
    if MsgPackImpl.IsValidUserData(info_U) then
        info = g_MessagePack.unpack(info_U)
        for k, v in pairs(info) do
            print(k,v )
        end
    end

    g_ScriptEvent:BroadcastInLua("GlobalMatch_SignUpPlayResultWithInfo", playId, success, info)
end

function Gas2Gac.GlobalMatch_CancelSignUpResult(playId, success)
    g_ScriptEvent:BroadcastInLua("GlobalMatch_CancelSignUpResult", playId, success)
end

function Gas2Gac.GlobalMatch_UpdatePlayStatus(playId, bIsPlayOpen, bIsPlayOpenMatch, ExtraInfo_U)
    --这个是登陆及状态变化时下发的, 先存起来, 用到的时候再取
    LuaGlobalMatchMgr:UpdatePlayStatus(playId, bIsPlayOpen, bIsPlayOpenMatch, ExtraInfo_U)
    g_ScriptEvent:BroadcastInLua("GlobalMatch_UpdatePlayStatus", playId, bIsPlayOpen, bIsPlayOpenMatch, ExtraInfo_U)
end

function Gas2Gac.ShengXiaoCard_SyncPlayState(state, PlayerInfos_U, Round, SubRound, LoserId, ShowInfo_U, GuessInfo_U, PlayerIn2Profile_U, context, PreparePlayers_U, expiredTime, isReportRank)
    -- print("ShengXiaoCard_SyncPlayState", state, PlayerInfos_U, Round, SubRound, LoserId, ShowInfo_U, GuessInfo_U, PlayerIn2Profile_U, context, PreparePlayers_U, expiredTime, isReportRank)
    LuaShengXiaoCardMgr.SyncPlayState(state, PlayerInfos_U, Round, SubRound, LoserId, ShowInfo_U, GuessInfo_U, PlayerIn2Profile_U, context, PreparePlayers_U, expiredTime,isReportRank)
end

function Gas2Gac.ShengXiaoCard_SyncGuaji(playerId, Guaji)
    -- print("ShengXiaoCard_SyncGuaji", playerId, Guaji)
    g_ScriptEvent:BroadcastInLua("ShengXiaoCard_SyncGuaji", playerId, Guaji)
end

function Gas2Gac.ShengXiaoCard_SyncJoin(playerId, playerProfile_U)
    -- print("ShengXiaoCard_SyncJoin", playerId, playerProfile_U)
    g_ScriptEvent:BroadcastInLua("ShengXiaoCard_SyncJoin", playerId,playerProfile_U)
end

function Gas2Gac.ShengXiaoCard_SyncLeave(playerId)
    -- print("ShengXiaoCard_SyncLeave", playerId)
    g_ScriptEvent:BroadcastInLua("ShengXiaoCard_SyncLeave", playerId)
end

function Gas2Gac.ShengXiaoCard_SyncPrepare(playerId, bPrepare)
    -- print("ShengXiaoCard_SyncPrepare", playerId, bPrepare)
    g_ScriptEvent:BroadcastInLua("ShengXiaoCard_SyncPrepare", playerId, bPrepare)
end

function Gas2Gac.ShengXiaoCard_SyncInteract(srcId, targetId, InteractType)
    -- print("ShengXiaoCard_SyncInteract", srcId, targetId, InteractType)
    g_ScriptEvent:BroadcastInLua("ShengXiaoCard_SyncInteract", srcId, targetId, InteractType)
end

function Gas2Gac.SeasonRank_QueryRankResult(rankType, seasonId, selfData, rankData)
    -- print("SeasonRank_QueryRankResult", rankType, seasonId, selfData, rankData)
    g_ScriptEvent:BroadcastInLua("SeasonRank_QueryRankResult", rankType, seasonId, selfData, rankData)
end

function Gas2Gac.ShangShiSendCookbookData(ingredientPackCount, lastCookbookId, cookbookInfoUD)
    LuaCookBookMgr:ShangShiSendCookbookData(ingredientPackCount, lastCookbookId, cookbookInfoUD)
end

function Gas2Gac.ShangShiSendMyFoodIngredient(ingredientInfoUD)
    LuaCookBookMgr:ShangShiSendMyFoodIngredient(ingredientInfoUD)
end

function Gas2Gac.ShangShiSendOpenIngredientPackResult(ingredientInfoUD)
    LuaCookBookMgr:ShangShiSendOpenIngredientPackResult(ingredientInfoUD)
end

function Gas2Gac.ShangShiSendCookingResult(cookbookId, isSucc, star)
    LuaCookBookMgr:ShangShiSendCookingResult(cookbookId, isSucc, star)
end

function Gas2Gac.ShangShiSendRetrieveIngredientSucc(ingredientId, remainCount)
    LuaCookBookMgr:ShangShiSendRetrieveIngredientSucc(ingredientId, remainCount)
end

function Gas2Gac.XinShengHuaJi_NotifyClient(paramStr)
    print("XinShengHuaJi_NotifyClient", paramStr)
end

function Gas2Gac.OpenXueJingKuangHuanSignUpWnd(bSignUp, dailyAwardTimes)
	LuaHanJiaMgr:Gas2Gac_OpenXueJingKuangHuanSignUpWnd(bSignUp, dailyAwardTimes)
end

function Gas2Gac.SyncXueJingKuangHuanSignUpResult(bSignUp)
	LuaHanJiaMgr:Gas2Gac_SyncXueJingKuangHuanSignUpResult(bSignUp)
end

function Gas2Gac.SyncXueJingKuangHuanPlayInfo(infoUD)
	LuaHanJiaMgr:Gas2Gac_SyncXueJingKuangHuanPlayInfo(infoUD)
end

function Gas2Gac.ShowXueJingKuangHuanResultWnd(rank, finalLength, maxLength, historyMaxLength)
    LuaHanJiaMgr:Gas2Gac_ShowXueJingKuangHuanResultWnd(rank, finalLength, maxLength, historyMaxLength)
end

function Gas2Gac.HLPY_SyncRescueProgress(progress, bExistsEnemy)
    -- print("HLPY_SyncRescueProgress", progress)
    LuaHLPYHelpWnd.s_Progress = progress
    if not CUIManager.IsLoaded(CLuaUIResources.HLPYHelpWnd) then
        CUIManager.ShowUI(CLuaUIResources.HLPYHelpWnd)
    end
    g_ScriptEvent:BroadcastInLua("HLPY_SyncRescueProgress", progress,bExistsEnemy)
end


function Gas2Gac.HLPY_EnterRescueRadius(rescueTargetEngineId)
    -- print("HLPY_EnterRescueRadius")
    LuaHLPYHelpWnd.s_InRescueRadius = true
    LuaHLPYHelpWnd.s_RescueTargetEngineId = rescueTargetEngineId

    if not CUIManager.IsLoaded(CLuaUIResources.HLPYHelpWnd) then
        CUIManager.ShowUI(CLuaUIResources.HLPYHelpWnd)
    end
    g_ScriptEvent:BroadcastInLua("HLPY_EnterRescueRadius")
end

function Gas2Gac.HLPY_LeaveRescueRadius()
    -- print("HLPY_LeaveRescueRadius")
    -- CUIManager.CloseUI(CLuaUIResources.HLPYHelpWnd)
    LuaHLPYHelpWnd.s_InRescueRadius = false
    LuaHLPYHelpWnd.s_RescueTargetEngineId = 0
    g_ScriptEvent:BroadcastInLua("HLPY_LeaveRescueRadius")
end

function Gas2Gac.SyncUnLockPaiZhaoLvJing(dataStr)
	if CClientMainPlayer.Inst == nil then
		return
	end

	if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eUnLockPaiZhaoLvJing) then
		CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockPaiZhaoLvJing] = VARBINARY()
	end
	local data = CommonDefs.ASCIIEncoding_GetBytes(dataStr)
	local bufLen = data.Length
	local startIndex = 0
	local default
	default, bufLen, startIndex = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockPaiZhaoLvJing]:LoadFromBytes(data, bufLen, bufLen, startIndex)
    g_ScriptEvent:BroadcastInLua("SyncUnLockPaiZhaoLvJing")
end

function Gas2Gac.XZNW_RewardSuccess(index)
    print("XZNW_RewardSuccess", index)
    g_ScriptEvent:BroadcastInLua("XZNW_RewardSuccess",index)
end

function Gas2Gac.XZNW_QueryRewardResult(rewardData_U)
    print("XZNW_QueryRewardResult", rewardData_U)
    g_ScriptEvent:BroadcastInLua("XZNW_QueryRewardResult",rewardData_U)
end

function Gas2Gac.TianJiangBaoXiang_ReachLimit()
    print("Gas2Gac.TianJiangBaoXiang_ReachLimit")
	CUIManager.CloseUI(CLuaUIResources.TianJiangBaoXiangWnd)
end

function Gas2Gac.WHCB_QueryRewardResult(data_U)
    -- print("Gas2Gac.WHCB_QueryRewardResult", data_U)
    g_ScriptEvent:BroadcastInLua("WHCB_QueryRewardResult",data_U)
end

function Gas2Gac.Watch_UpdateWatchStatus(bIsWatching)
    EventManager.BroadcastInternalForLua(EnumEventType.OnUpdateCommonWatchStatus, {bIsWatching})
end

function Gas2Gac.PlayerEnterGamePlay(designPlayId, sceneTemplateId, extraDataUD)
	LuaGamePlayMgr:PlayerEnterGamePlay(designPlayId, sceneTemplateId, extraDataUD)
end

function Gas2Gac.Curling_SyncPlayState(playState, round, expiredTime)
    LuaOlympicGamesMgr:Curling_SyncPlayState(playState, round, expiredTime)
end

function Gas2Gac.Curling_SyncPlayerInfo(playerInfo_U)
    LuaOlympicGamesMgr:Curling_SyncPlayerInfo(playerInfo_U)
end

function Gas2Gac.Curling_QueryRankResult(rank_U, context)
    LuaOlympicGamesMgr:Curling_QueryRankResult(rank_U, context)
end

function Gas2Gac.UpdateYuanXiaoRiddle2022Question(questionCount, questionIdx, questionId, questionExpireTime, roundExpireTime)
	LuaYuanXiao2022Mgr:UpdateYuanXiaoRiddle2022Question(questionCount, questionIdx, questionId, questionExpireTime, roundExpireTime)
end

function Gas2Gac.UpdateYuanXiaoRiddle2022GuessResult(questionId, guessed, answer)
	LuaYuanXiao2022Mgr:UpdateYuanXiaoRiddle2022GuessResult(questionId, guessed, answer)
end

function Gas2Gac.UpdateYuanXiaoRiddle2022OpenStatus(questionId, opened, itemNum1, itemNum2)
	LuaYuanXiao2022Mgr:UpdateYuanXiaoRiddle2022OpenStatus(questionId, opened, itemNum1, itemNum2)
end

function Gas2Gac.OpenFireworkPartyWnd(totalSubmittedNum, statusListUD, toSubmitNum)
    LuaFireWorkPartyMgr.PartyStatus = {}
	local statusList = MsgPackImpl.unpack(statusListUD)
	if not statusList then return end

	for i = 0, statusList.Count-1, 2 do
        local t = {}
		t.id = statusList[i]
		t.status = statusList[i+1]
        LuaFireWorkPartyMgr.PartyStatus[t.id] = t 
	end
    LuaFireWorkPartyMgr.TotalSubmittedNum = totalSubmittedNum
    LuaFireWorkPartyMgr.ToSubmitNum = toSubmitNum
    CUIManager.ShowUI(CLuaUIResources.FireWorkPartyZhongChouWnd)
end

function Gas2Gac.SubmitItemsToFireworkPartySuccess(submitNum, totalSubmittedNum, toSubmitNum)
    g_ScriptEvent:BroadcastInLua("SubmitItemsToFireworkPartySuccess",submitNum, totalSubmittedNum, toSubmitNum)
end

function Gas2Gac.UpdateFirworkPartyStatus(id, status, totalSubmittedNum)
    g_ScriptEvent:BroadcastInLua("UpdateFirworkPartyStatus",id, status, totalSubmittedNum)

	if id == 1 and status == EnumFireworkPartyStatus.eStart then
		local msg = g_MessageMgr:FormatMessage("FireWorkParty_FirstSchedue_OpenNotice")
		MessageWndManager.ShowConfirmMessage(msg, 10, false, DelegateFactory.Action(function ()
			local sdata = YuanXiao_FireworkPartySchedule.GetData(id)
			local pos = sdata.BestViewPos
			CTrackMgr.Inst:Track(nil, sdata.MapId, CreateFromClass(CPos, pos[0] * 64, pos[1] * 64), 0, nil, nil, nil, nil, true)
		end), nil)
	end
end

function Gas2Gac.NotifyPlayerBuyLiuGuangYiCaiFirework(buyerId, buyerName, serverId, serverName, submitNum, totalSubmittedNum, bShowServerName)
	print("NotifyPlayerBuyLiuGuangYiCaiFirework", buyerId, buyerName, serverId, serverName, submitNum, totalSubmittedNum, bShowServerName)
end

function Gas2Gac.OpenConfessFireworkWnd()
	--print("OpenConfessFireworkWnd")
    LuaFireWorkPartyMgr.OpenSendFireworkToFriendWnd()
end

function Gas2Gac.AddConfessFireworkInScene(id, playerId, playerName, targetId, targetName, x, y, dir, height)
	local scale = 5
    local bloomHeight = height
    LuaFireWorkPartyMgr.LightConfessFireworkInScene(id, playerId, playerName, targetId, targetName, x, y, dir,bloomHeight,scale)
end

function Gas2Gac.SetOffConfessFireworkSuccess(targetId, targetName)
	--print("SetOffConfessFireworkSuccess", targetId, targetName)
    CUIManager.CloseUI(CLuaUIResources.SendShowLoveFireWorkWnd)
end

function Gas2Gac.FireworkPartyZhiBo_SyncChannelInfo(id, RemoteChannelInfo)
	--print("FireworkPartyZhiBo_SyncChannelInfo", id, RemoteChannelInfo)
    LuaFireWorkPartyMgr.SyncCCInfo(id, RemoteChannelInfo)
end

function Gas2Gac.FireworkPartyZhiBo_ZhiBoEnd()
	--print("FireworkPartyZhiBo_ZhiBoEnd")
    LuaFireWorkPartyMgr.StopZhiBo()
end

function Gas2Gac.FireworkPartyStart(id, endTime)
	print("FireworkPartyStart", id, endTime)
    LuaFireWorkPartyMgr.OnFireworkPartyStart(id,endTime)
end

function Gas2Gac.FireworkPartyEnd(id)
	print("FireworkPartyEnd", id)
    LuaFireWorkPartyMgr.OnFireworkPartyEnd(id)
end

function Gas2Gac.SyncTangYuan2022PlayInfo(infoUD)
	-- print("SyncTangYuan2022PlayInfo")
    g_ScriptEvent:BroadcastInLua("SyncTangYuan2022PlayInfo",infoUD)
end

function Gas2Gac.ShowTangYuan2022ResultWnd(finishNum, score, rankInPlay, oldDayMaxScore)
    LuaTangYuan2022Mgr.OpenResultWnd(finishNum, score, rankInPlay, oldDayMaxScore)
end

function Gas2Gac.SyncTangYuan2022RoundScore(playerId, roundScore,enginedId)
    --同步所有其他玩家的当前汤圆分数用来维护汤圆大小
	-- print("SyncTangYuan2022RoundScore", playerId, roundScore,enginedId)
    LuaTangYuan2022Mgr.SyncTangYuan2022RoundScore(playerId, roundScore,enginedId)
end

function Gas2Gac.UpdateTangYuan2022RoundScore(engineId, roundScore, deltaScore)
	-- print("UpdateTangYuan2022RoundScore", engineId, roundScore, deltaScore)
    LuaTangYuan2022Mgr.UpdateTangYuan2022RoundScore(engineId, roundScore, deltaScore)
end

function Gas2Gac.PlayerGrabTangYuan(playerId, engineId)
    if not LuaTangYuan2022Mgr.TangYuanAttachedPlayerDict then
        LuaTangYuan2022Mgr.TangYuanAttachedPlayerDict = {}
    end
    LuaTangYuan2022Mgr.TangYuanAttachedPlayerDict[engineId] = playerId
    LuaTangYuan2022Mgr.PlayerGrabTangYuan(playerId, engineId)
end

function Gas2Gac.PlayerReleaseTangYuan(playerId)
    LuaTangYuan2022Mgr.PlayerReleaseTangYuan(playerId)
end

function Gas2Gac.SnowballFight_SyncPlayState(playState, expiredTime, forceInfos_U)
    LuaOlympicGamesMgr:SnowballFight_SyncPlayState(playState, expiredTime, forceInfos_U)
end

function Gas2Gac.SnowballFight_SyncPlayerInfo(playerInfo_U)
    LuaOlympicGamesMgr:SnowballFight_SyncPlayerInfo(playerInfo_U)
end

function Gas2Gac.BeginBezierCameraMove(x, y, z, rx, ry, rz, startTanx, startTany, startTanz, endTanx, endTany, endTanz, maxDuration, moveType, showMainUI, showSoundSettingBtn)
    local position = Vector3(x,y,z)
    local helpVec = Vector3(rx,ry,rz)
    local startTangent = Vector3(startTanx,startTany,startTanz)
    local endTangent = Vector3(endTanx,endTany,endTanz)
    CameraFollow.Inst:BeginBezierCamera(position,helpVec,startTangent,endTangent,maxDuration,moveType,showMainUI, showSoundSettingBtn)
end

function Gas2Gac.BezierCameraMoveTo(x, y, z, rx, ry, rz, startTanx, startTany, startTanz, endTanx, endTany, endTanz, maxDuration, moveType, showMainUI, showSoundSettingBtn)
    local position = Vector3(x,y,z)
    local helpVec = Vector3(rx,ry,rz)
    local startTangent = Vector3(startTanx,startTany,startTanz)
    local endTangent = Vector3(endTanx,endTany,endTanz)
    CameraFollow.Inst:BezierCameraMoveTo(position,helpVec,startTangent,endTangent,maxDuration,moveType,showMainUI)
end

function Gas2Gac.EndBezierCameraMove()
    CameraFollow.Inst:EndBezierCamera()
end

function Gas2Gac.SnowballFight_RewardResult(score, playResult, rewardItemId)
    LuaOlympicGamesMgr:SnowballFight_RewardResult(score, playResult, rewardItemId)
end

function Gas2Gac.ShowDriftBottleEditWnd(itemTemplateId, itemId, place, pos)
	LuaValentine2022Mgr:ShowDriftBottleEditWnd(itemTemplateId, itemId, place, pos)
end

function Gas2Gac.ShowDriftBottleViewWnd(anonymous, receiverId, receiverName, content, voiceUrl, voiceDuration, senderId, senderName, senderClass, senderGender, senderExpression, senderExpressionTxt, senderSticker)
	LuaValentine2022Mgr:ShowDriftBottleViewWnd(anonymous, receiverId, receiverName, content, voiceUrl, voiceDuration, senderId, senderName, senderClass, senderGender, senderExpression, senderExpressionTxt, senderSticker)
end

function Gas2Gac.GenerateDriftBottleSuccess()
    LuaValentine2022Mgr:GenerateDriftBottleSuccess()
end

function Gas2Gac.PutDriftBottleSuccess()
	LuaValentine2022Mgr:PutDriftBottleSuccess()
end

function Gas2Gac.SyncSetPackagePinnedItemTypeResult(equipFirst, itemType)
    if CClientMainPlayer.Inst then
        local itemProp = CClientMainPlayer.Inst.ItemProp
        itemProp.PackagePinnedItemType = itemType
        itemProp.PackagePinnedEquipment = equipFirst and 1 or 0
        g_ScriptEvent:BroadcastInLua("SyncSetPackagePinnedItemTypeResult")
    end
end

function Gas2Gac.SendArenaBattleTeamInfo(teamInfo)
    LuaColiseumMgr:SendArenaBattleTeamInfo(teamInfo)
end

function Gas2Gac.QueryPlayerPos_JXYSEnemy(posTbl_U)
    local posList =  MsgPackImpl.unpack(posTbl_U)
	local posInfoTbl = {}
	if posList and posList.Count%6==0 then
		for i=0,posList.Count-1,6 do
			table.insert(posInfoTbl, {
				playerId = tonumber(posList[i]),
				engineId = tonumber(posList[i+1]),
				posX = tonumber(posList[i+2]),
				posY = tonumber(posList[i+3]),
                isTeamMember = posList[i+4],
                isTeamLeader = posList[i+5],
			})
		end
	end
    g_ScriptEvent:BroadcastInLua("OnQueryPlayerPos_JXYSEnemy", posInfoTbl)
end

function Gas2Gac.SeasonRank_ReportRankDataSuccess(rankType, seasonId, data_U)
	g_ScriptEvent:BroadcastInLua("SeasonRank_ReportRankDataSuccess", rankType, seasonId, data_U)
end

function Gas2Gac.BeginToricSpaceCamera1(engineId1, engineId2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn, noSway)--showMainUI传过来的参数实际上是ignoreMainUI 
    --print("BeginToricSpaceCamera1", engineId1, engineId2, alpha, theta, phi, maxDuration, noSway)
    CameraFollow.Inst:BeginToricSpaceCamera(engineId1, engineId2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn,noSway)
end

function Gas2Gac.ToricSpaceCameraMoveTo1(engineId1, engineId2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn, noSway)
    --print("ToricSpaceCameraMoveTo1", engineId1, engineId2, alpha, theta, phi, maxDuration, noSway)
    CameraFollow.Inst:ToricSpaceCameraMoveTo(engineId1,engineId2,alpha, theta, phi, maxDuration,noSway)
end

function Gas2Gac.BeginToricSpaceCamera2(x, y, z, x2, y2, z2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn)
    local pos1 = Vector3(x+0.5,y,z+0.5)
    local pos2 = Vector3(x2+0.5,y2,z2+0.5)
    CameraFollow.Inst:BeginToricSpaceCamera_Pos(pos1,pos2,alpha, theta, phi, maxDuration,showMainUI, showSoundSettingBtn)
end

function Gas2Gac.ToricSpaceCameraMoveTo2(x, y, z, x2, y2, z2, alpha, theta, phi, maxDuration, showMainUI, showSoundSettingBtn)
    local pos1 = Vector3(x+0.5,y,z+0.5)
    local pos2 = Vector3(x2+0.5,y2,z2+0.5)
    CameraFollow.Inst:ToricSpaceCameraMoveTo_Pos(pos1,pos2,alpha, theta, phi, maxDuration)
end

function Gas2Gac.EndToricSpaceCamera()
    CameraFollow.Inst:EndToricSpaceCamera()
end

function Gas2Gac.SendTerritoryWarPickBornRegionInfo(pickedRegion, regionGuildCount)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarPickBornRegionInfo(pickedRegion, regionGuildCount)
end

function Gas2Gac.SyncTerritoryWarStatus(seasonId, status)
    LuaGuildTerritorialWarsMgr:SyncTerritoryWarStatus(seasonId, status)
end

function Gas2Gac.SendTerritoryWarMapOverview(myGuildId, observeType, currentWeek, guildList, randGrids)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarMapOverview(myGuildId, observeType, currentWeek, guildList, randGrids)
end

function Gas2Gac.SendTerritoryWarMapDetail(currentStage, myGuildId, battleStartTime, finishTime, curPosGridId, myGuildGridCount, favoriteGrids, grids, hasMonsterGrids)
    -- stage: 3平时外围玩法的阶段, 4正式战斗开始传送阶段,  5正式战斗阶段,
    LuaGuildTerritorialWarsMgr:SendTerritoryWarMapDetail(myGuildId, battleStartTime, finishTime, curPosGridId, myGuildGridCount, favoriteGrids, grids, hasMonsterGrids)
end

function Gas2Gac.StartTerritoryWarTeleportCasting(duration)
    LuaGuildTerritorialWarsMgr:ShowTeleportProgress(duration)
end

function Gas2Gac.SendTerritoryWarPlayInfo(myGuildId, seasonId, weekCount, battleLevel, prepareFinishTime, roundFinishTime, gridId, sceneIdx, isFighting, belongGuildId, belongForce, occupyNpcEngineId)
    -- 赛季id, 第几周, 组别1甲2乙3丙, 当天战斗结束时间, 格子id, 分线id
    LuaGuildTerritorialWarsMgr:SendTerritoryWarPlayInfo(myGuildId,seasonId, weekCount, battleLevel, prepareFinishTime, roundFinishTime, gridId, sceneIdx, isFighting, belongGuildId, belongForce, occupyNpcEngineId)
end

function Gas2Gac.SyncTerritoryWarGuildCurrentRank(rank)
    LuaGuildTerritorialWarsMgr:SyncTerritoryWarGuildCurrentRank(rank)
end

function Gas2Gac.SyncTerritoryWarPlayProgress(progressUd)
    LuaGuildTerritorialWarsMgr:SyncTerritoryWarPlayProgress(progressUd)
end

function Gas2Gac.SendTerritoryWarScoreInfo(myGuildId, currentWeek, currentScore, gridCount, scoreSpeed, toMasterSpeed, fromSlaveSpeed, rankInfo, eventInfo)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarScoreInfo(myGuildId, currentWeek, currentScore, gridCount, scoreSpeed, toMasterSpeed, fromSlaveSpeed, rankInfo, eventInfo)
end

function Gas2Gac.SendTerritoryWarRelationInfo(myGuildId, hasAlliance, relationInfo)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarRelationInfo(myGuildId, hasAlliance, relationInfo)
end

function Gas2Gac.SendTerritoryWarScenePlayerCount(scenePlayerCount)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarScenePlayerCount(scenePlayerCount)
end

function Gas2Gac.SendTerritoryWarRankInfo(playerGuildId, playerBattleLevel, battleLevel, rankInfo)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarRankInfo(playerGuildId, playerBattleLevel, battleLevel, rankInfo)
end

function Gas2Gac.SendTerritoryWarFightDetail(detail)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarFightDetail(detail)
end

function Gas2Gac.RequestGuildTerritoryWarMemberTeleport(leaderId, leaderName, gridId, sceneIdx)
    LuaGuildTerritorialWarsMgr:RequestGuildTerritoryWarMemberTeleport(leaderId, leaderName, gridId, sceneIdx)
end

function Gas2Gac.SendPlayerCanJoinGuildTerritoryWar(canJoin)
    print("SendPlayerCanJoinGuildTerritoryWar", canJoin)
    g_ScriptEvent:BroadcastInLua("SendPlayerCanJoinGuildTerritoryWar", canJoin)
end

function Gas2Gac.SendTerritoryWarAllianceApplyInfo(applyInfo)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarAllianceApplyInfo(applyInfo)
end

function Gas2Gac.SendTerritoryWarCanDrawCommand(requestType, can)
    LuaGuildTerritorialWarsMgr:SendTerritoryWarCanDrawCommand(requestType, can)
end

function Gas2Gac.SendPlayerShareCommandPicResult(isSucc)
    LuaGuildTerritorialWarsMgr:SendPlayerShareCommandPicResult(isSucc)
end

function Gas2Gac.SendGuildTerritoryWarPlayWeeklyResult(seasonId, weekPassed, guildId, guildName, rank, guildScore, gridCount, slaveCount, playerScore, contribution, silver, exp, mingwang, itemUd)
    LuaGuildTerritorialWarsMgr:SendGuildTerritoryWarPlayWeeklyResult(seasonId, weekPassed, guildId, guildName, rank, guildScore, gridCount, slaveCount, playerScore, contribution, silver, exp, mingwang, itemUd)
end

function Gas2Gac.SendGTWRelatedPlayInfo(myGuildId, seasonId, weekCount, battleLevel, gridId, sceneIdx, belongGuildId, belongForce, monsterCount, pickCount)
    LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayInfo(myGuildId, seasonId, weekCount, battleLevel, gridId, sceneIdx, belongGuildId, belongForce, monsterCount, pickCount)
end

function Gas2Gac.SendGTWRelatedPlayMonsterCount(monsterCount)
    LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayMonsterCount(monsterCount)
end

function Gas2Gac.SendGTWRelatedPlayMonsterSceneInfo(sceneInfo)
    LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayMonsterSceneInfo(sceneInfo)
end

function Gas2Gac.SendCanOpenGTWRelatedPlayContributeWnd(canOpen)
    LuaGuildTerritorialWarsMgr:SendCanOpenGTWRelatedPlayContributeWnd(canOpen)
end

function Gas2Gac.SendGTWRelatedPlayContributeInfo(isTotal, contributeInfo)
    LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayContributeInfo(isTotal, contributeInfo)
end

function Gas2Gac.SendGTWRelatedRemainExchangeMonsterHpCount(count)
    print("SendGTWRelatedRemainExchangeMonsterHpCount", count)
    local ownCount = CItemMgr.Inst:GetItemCount(GuildTerritoryWar_RelatedPlaySetting.GetData().ResourceItemId)
	local maxVal =  math.min(math.min(500,ownCount),count)
	if maxVal == 0 then
		g_MessageMgr:ShowMessage("Null_LingQi_Submit")
		return
	end
	CLuaNumberInputMgr.ShowNumInputBox(1, maxVal, 1, function (val) 
		Gac2Gas.ExchangeGTWRelatedPlayCityMonsterHp(val)
	end, LocalString.GetString("请选择提交的灵气数量"), -1)
end

function Gas2Gac.SendGTWSubmitLingQiInfo(currentHaveLingQi, todayExchangeFXYCount, levelInfo)
    LuaGuildTerritorialWarsMgr:SendGTWSubmitLingQiInfo(currentHaveLingQi, todayExchangeFXYCount, levelInfo)
end

function Gas2Gac.SendGTWAltarInfo(altarInfo)
    LuaGuildTerritorialWarsMgr:SendGTWAltarInfo(altarInfo)
end

function Gas2Gac.SendGTWZhanLongTaskInfo(todayTaskCount, taskInfo)
    LuaGuildTerritorialWarsMgr:SendGTWZhanLongTaskInfo(todayTaskCount, taskInfo)
end

function Gas2Gac.SendGTWZhanLongShopInfo(hasScore, hasPermission, shopInfo)
    LuaGuildTerritorialWarsMgr:SendGTWZhanLongShopInfo(hasScore, hasPermission, shopInfo)
end

function Gas2Gac.SendGTWChallengeMemberInfo(onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo)
    LuaGuildTerritorialWarsMgr:SendGTWChallengeMemberInfo(onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo)
end

function Gas2Gac.SendSetGTWChallengeEliteResult(targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
    LuaGuildTerritorialWarsMgr:SendSetGTWChallengeEliteResult(targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
end

function Gas2Gac.SendGTWChallengeBattleFieldInfo(myGuildId, challengeIdx, bossIdx, remainTime, bossInfo, passGuildInfo)
    LuaGuildTerritorialWarsMgr:SendGTWChallengeBattleFieldInfo(myGuildId, challengeIdx, bossIdx, remainTime, bossInfo, passGuildInfo)
end

function Gas2Gac.SendGTWChallengePlayInfo(challengeIdx, currentBossIdx, maxBossIdx, isCurrentBossDie)
    LuaGuildTerritorialWarsMgr:SendGTWChallengePlayInfo(challengeIdx, currentBossIdx, maxBossIdx, isCurrentBossDie)
end

function Gas2Gac.SendGTWChallengeFightDetailInfo(detailType, detail, playerCount, statTotal)
    LuaGuildTerritorialWarsMgr:SendGTWChallengeFightDetailInfo(detailType, detail, playerCount, statTotal)
end

function Gas2Gac.UpdateTeamMemberExtStringForGameplay(gameplayId, memberId, content, extraInfo)
    if CScene.MainScene then
        if LuaGuildTerritorialWarsMgr:IsInPlay() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay()  then
            LuaGuildTerritorialWarsMgr:UpdateTeamMemberExtStringForGameplay(gameplayId, memberId, content, extraInfo)
            return
        end
        local mGamePlayId = CScene.MainScene.GamePlayDesignId
        if mGamePlayId == gameplayId then
            CTeamMgr.Inst:UpdateTeamMemberExtString(memberId, content)
        end
    end
end

function Gas2Gac.SyncAllPlayerTitleInPlay(playerTitleUd)
    local list = playerTitleUd and MsgPackImpl.unpack(playerTitleUd)
    for i=0,list.Count-1, 2 do
        local playerId = list[i]
        local playerTitle = list[i+1]
        CGamePlayMgr.Inst:SyncPlayerTitleInPlay(playerId,playerTitle)
    end
end

function Gas2Gac.SendGuildForeignAidInfo(hasApplication, canApply, maxAidCount, maxEliteAidCount, aidInfoUd, gameplayContext)
    -- canApply 是否开放申请
    LuaGuildExternalAidMgr:SendGuildForeignAidInfo(hasApplication, canApply, maxAidCount, maxEliteAidCount, aidInfoUd, gameplayContext)
end

function Gas2Gac.SendGuildForeignAidApplicationStatus(canApply)
    -- canApply 是否开放申请
    LuaGuildExternalAidMgr:SendGuildForeignAidApplicationStatus(canApply)
end

function Gas2Gac.SendGuildForeignAidApplyInfo(canApply, gameplayContext, applyInfoUd)
    -- canApply 是否开放申请
    LuaGuildExternalAidMgr:SendGuildForeignAidApplyInfo(canApply, gameplayContext,applyInfoUd)
end

function Gas2Gac.SendGuildForeignAidPersonalInfo(guildId, guildName, aidType, canFightTime, hasInvite, personalInfoUd, gameplayContext)
    LuaGuildExternalAidMgr:SendGuildForeignAidPersonalInfo(guildId, guildName, aidType, canFightTime, hasInvite, personalInfoUd, gameplayContext)
end

function Gas2Gac.SendGuildForeignAidInviteInfo(inviteInfoUd, gameplayContext)
    LuaGuildExternalAidMgr:SendGuildForeignAidInviteInfo(inviteInfoUd, gameplayContext)
end

function Gas2Gac.SendGuildForeignAidApplyStatusChange(guildId, isApply, gameplayContext)
    LuaGuildExternalAidMgr:SendGuildForeignAidApplyStatusChange(guildId, isApply)
end

function Gas2Gac.SendGuildForeignAidPlayerBeAid(guildId, gameplayContext)
    LuaGuildExternalAidMgr:SendGuildForeignAidPlayerBeAid(guildId)
end

function Gas2Gac.SharePicToPersonalSpace(pic, topicName, topicId)
    ShareMgr.ShareLocalTexture2Other(pic, topicName, topicId)
end

function Gas2Gac.SetCameraRZY(r, z, y)
	local vec3 = Vector3(r, z, y)
    if CameraFollow.Inst then
        CameraFollow.Inst.targetRZY = vec3
        CameraFollow.Inst.RZY = vec3
        CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
    end
end

function Gas2Gac.ResetCameraRZY()
    if CameraFollow.Inst then
		CameraFollow.Inst:ResetRZY()
	end
end

function Gas2Gac.PlaySoundAtPos(eventName, x, y)
    if CClientMainPlayer.Inst then
        SoundManager.Inst:PlayOneShot(eventName, Vector3(x,CClientMainPlayer.Inst.RO.Position.y,y), nil, 0)
    end
end

--查询对阵结果
function Gas2Gac.QueryGLMatchInfoResult(matchInfo,level)
    local list = MsgPackImpl.unpack(matchInfo)
    local t = {}
    for i = 0, list.Count-1,5 do
        local round = tonumber(list[i])
        local index = tonumber(list[i+1])
        local guildName = list[i+2]
        local otherGuildName = list[i+3]
        local winGuildName = list[i+4]

        if not t[round] then
            t[round] = {}
        end
        t[round][index] = {
            guildName = guildName,
            otherGuildName = otherGuildName,
            winGuildName = winGuildName,
        }
    end
    g_ScriptEvent:BroadcastInLua("QueryGLMatchInfoResult", level,t)
end

function Gas2Gac.ZhuErDan_SyncPlayState(state, expiredTime)
    print("ZhuErDan_SyncPlayState", state, expiredTime)
end

function Gas2Gac.ZhuErDan_PlayDialog(paramTbl_U)
    -- print("ZhuErDan_PlayDialog", paramTbl_U)
    local v = MsgPackImpl.unpack(paramTbl_U)
    local dialogId = 0
    CommonDefs.DictIterate(v,DelegateFactory.Action_object_object(function (___key, ___value) 
        if ___key=="Dialog" then
            dialogId = tonumber(___value)
        end
    end))
    if dialogId>0 then
        CLuaJuQingDialogMgr.ShowClientDialog(dialogId,function()
            Gac2Gas.ZhuErDanParkour_DialogEnd()
        end)
    end
end

function Gas2Gac.ShenYao_SyncPlayState(bIsPlayOpen, bIsPlayStart, playStartTime)
    LuaXinBaiLianDongMgr:ShenYao_SyncPlayState(bIsPlayOpen, bIsPlayStart, playStartTime)
end

function Gas2Gac.ShenYao_QuerySceneCountTblResult(sceneCountTbl, unlockLv)
    LuaXinBaiLianDongMgr:ShenYao_QuerySceneCountTblResult(sceneCountTbl, unlockLv)
end

function Gas2Gac.ShenYao_SignUpSucess(ExpiredTime, NpcId, Level)
    print("ShenYao_SignUpSucess", ExpiredTime, NpcId)
    LuaShenYaoChallengerSelectionWnd.s_ExpiredTime = ExpiredTime
    LuaShenYaoChallengerSelectionWnd.s_NpcId = NpcId
    LuaShenYaoChallengerSelectionWnd.s_Level = Level
    CUIManager.ShowUI(CLuaUIResources.ShenYaoChallengerSelectionWnd)
end

function Gas2Gac.ShenYao_QueryTeamPassResult(Level, NpcId, playerInfos_U)
    LuaXinBaiLianDongMgr:ShenYao_QueryTeamPassResult(Level, NpcId, playerInfos_U)
end

function Gas2Gac.ShenYao_QueryPersonalNpcInfoResult(info_U)
    LuaXinBaiLianDongMgr:ShenYao_QueryPersonalNpcInfoResult(info_U)
end

function Gas2Gac.ShenYao_QueryUnlockLevelResult(lv)
    print("ShenYao_QueryUnlockLevelResult", lv)
    g_ScriptEvent:BroadcastInLua("ShenYao_QueryUnlockLevelResult", lv)
end

function Gas2Gac.ShenYao_QueryNpcEngineIdResult(sceneId, instId, engineId)
    LuaXinBaiLianDongMgr:ShenYao_QueryNpcEngineIdResult(sceneId, instId, engineId)
end

function Gas2Gac.ShenYao_OpenPersonalNpcWnd(consumeItemId, tbl_U)
    LuaXinBaiLianDongMgr:ShenYao_OpenPersonalNpcWnd(consumeItemId, tbl_U)
end

function Gas2Gac.ShenYao_OpenSelectNpcWnd(itemId)
    LuaXinBaiLianDongMgr:ShenYao_OpenSelectNpcWnd(itemId)
end

function Gas2Gac.XinBaiProgress_QueryResult(bOpen, ProgressTbl_U, ShuiManJinShan_U, statusTbl_U)
    LuaXinBaiLianDongMgr:XinBaiProgressQueryResult(bOpen, ProgressTbl_U, ShuiManJinShan_U, statusTbl_U)
end

function Gas2Gac.XinBaiProgress_QueryRewardResult(flagTbl_U)
    LuaXinBaiLianDongMgr:XinBaiProgressQueryRewardResult(flagTbl_U)
end

function Gas2Gac.SetMonitorChannel(tbl_U)
    if CClientMainPlayer.Inst ==nil then return end
    local data = CClientMainPlayer.Inst.PlayProp.PersistPlayData
    local key = EnumPersistPlayDataKey_lua.eMonitorChannel
    local vb = VARBINARY()
    local bufLen = tbl_U.Length
    vb:LoadFromBytes(tbl_U, bufLen, bufLen, 0)
    if not CommonDefs.DictContains_LuaCall(data,key) then
        CommonDefs.DictAdd_LuaCall (data,key,vb)
    else
        CommonDefs.DictSet_LuaCall(data,key,vb)
    end
    LuaChatMgr.LoadFilterWords()
    g_ScriptEvent:BroadcastInLua("OnChatFilterWordsChanged")
end

function Gas2Gac.YaoBan_Unlock(yaobanId, unlockTime)
    LuaXinBaiLianDongMgr:YaoBanUnlock(yaobanId, unlockTime)
end

function Gas2Gac.YaoBan_Enable(yaobanId)
    LuaXinBaiLianDongMgr:YaoBanEnable(yaobanId)
end

function Gas2Gac.YaoBan_Disable()
    LuaXinBaiLianDongMgr:YaoBanDisable()
end

function Gas2Gac.YaoBan_UpdateScore(score)
    LuaXinBaiLianDongMgr:YaoBanUpdateScore(score)
end

function Gas2Gac.GuanYaoJian_Unlock(id)
    LuaXinBaiLianDongMgr:GuanYaoJianUnlock(id)
end

function Gas2Gac.GuanYaoJian_RewardSuccess()
    LuaXinBaiLianDongMgr:GuanYaoJianRewardSuccess()
end

function Gas2Gac.BaiSheBianRen_SyncPlayState(playState, expiredTime, score, maxScore)
    LuaXinBaiLianDongMgr:BaiSheBianRenSyncPlayState(playState, expiredTime, score, maxScore)
end

function Gas2Gac.BaiSheBianRen_UpdateSize(engineId, scale, isScaleAtOnce)
    LuaXinBaiLianDongMgr:ChangeObjectScale(engineId, scale, isScaleAtOnce)
end

function Gas2Gac.ShakeCamera(duration, shakeStrength, fadeInTime, fadeOutTime)
    CShakeCameraMgr.Inst:ShakeCamera(duration, shakeStrength, fadeInTime, fadeOutTime)
end

function Gas2Gac.SyncPlayerSeeSYMZPick(isSaw, pickId, pickEngineId)
    local obj = CClientObjectMgr.Inst:GetObject(pickEngineId)
    if obj then
        local ro = obj.RO
        if ro then
            if isSaw then
                ro:DestroyAllFX()
                ro.gameObject:SetActive(true)
            else
                ro.gameObject:SetActive(false)
            end
        end
    end
end

function Gas2Gac.SendShanYeMiZongInfo(finalRewarded, extraTaskRewarded, xiansuoInfo, chapterInfo, joinedCaiXiang)
    -- 刷新一下外面的红点
	g_ScriptEvent:BroadcastInLua("SendHasShanYeMiZongReward_JieRiButton")
    LuaShanYeMiZongMgr:SendShanYeMiZongInfo(finalRewarded, extraTaskRewarded, xiansuoInfo, chapterInfo, joinedCaiXiang)
end

function Gas2Gac.SendShanYeMiZongCaiXiangInfo(caixiangId, myChoice, rightAnster, caixiangInfo)
    LuaShanYeMiZongMgr:SendShanYeMiZongCaiXiangInfo(caixiangId, myChoice, rightAnster, caixiangInfo)
end

function Gas2Gac.SendShanYeMiZongNewXianSuo(newXianSuoId)
    g_ScriptEvent:BroadcastInLua("SendHasShanYeMiZongReward", true)
    LuaShanYeMiZongMgr:SendShanYeMiZongNewXianSuo(newXianSuoId)
end

-- 还未激活 需要弹个消息提示激活
function Gas2Gac.ShowActiveSYMZBuyHuoLi()
    local str = ""
    local huoli = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.HuoLi or 0

    local huoliStr = tostring(huoli)
    if huoli < 50 then
        huoliStr = "[FF0000]"..huoliStr .."[-]"
    end

	local openTime = ShuJia2022_Setting.GetData().ShanYeMiZongFinalTime
    local _, _, y, m, d, h = string.find(openTime, LocalString.GetString("(%d+)%-(%d+)%-(%d+) (%d+):00"))
	local now = CServerTimeMgr.Inst:GetZone8Time()

	if now.Month > tonumber(m) or (now.Month == tonumber(m) and (now.Day > tonumber(d) or (now.Day == tonumber(d) and now.Hour >= tonumber(h)))) then
		str = g_MessageMgr:FormatMessage("Open_ShanYe_Wnd_Warning02", huoliStr)
	else
		str = g_MessageMgr:FormatMessage("Open_ShanYe_Wnd_Warning", huoliStr)
	end

    g_MessageMgr:ShowOkCancelMessage(str, 
        function()
            -- 请求扣除
            Gac2Gas.RequestActiveSYMZ()
        end, nil, nil, nil, false)
end

function Gas2Gac.ShanYeMiZongGetChapterRewardSucc(chapterId)
    print("ShanYeMiZongGetChapterRewardSucc", chapterId)
end

function Gas2Gac.SendHuanHunShanZhuangShengSiMen(isShow, flagData)
    LuaHuanHunShanZhuangMgr:SendHuanHunShanZhuangShengSiMen(isShow, flagData)
end

function Gas2Gac.SendHuanHunShanZhuangPlayTimes(tiaozhanTimes, zhuzhanTimes, hardModeTimes)
    LuaHuanHunShanZhuangMgr:SendHuanHunShanZhuangPlayTimes(tiaozhanTimes, zhuzhanTimes, hardModeTimes)
end

function Gas2Gac.SendHuanHunShanZhuangPlayResult(isWin, isHardMode, duration, memberInfo, rewardType)
    LuaHuanHunShanZhuangMgr:SendHuanHunShanZhuangPlayResult(isWin, isHardMode, duration, memberInfo, rewardType)
end

function Gas2Gac.SendShanYeMiZongFinalReward(rewarded, finalSucc, allCaiXiangSucc, serverFinalSucc, rewardItems)
    LuaShanYeMiZongMgr:SendShanYeMiZongFinalReward(rewarded, finalSucc, allCaiXiangSucc, serverFinalSucc, rewardItems)
end

function Gas2Gac.ShowShanYeMiZongZhenXiong()
    LuaShanYeMiZongMgr:ShowShanYeMiZongZhenXiong()
end

function Gas2Gac.SendHasShanYeMiZongReward(hasReward)
    g_ScriptEvent:BroadcastInLua("SendHasShanYeMiZongReward", hasReward)
end

function Gas2Gac.SyncHuanHunShanZhuangPlayStageChange(stage, isShowFx)
    LuaHuanHunShanZhuangMgr:SyncHuanHunShanZhuangPlayStageChange(stage, isShowFx) 
end

function Gas2Gac.SyncMiWuLingFogClear(isShowFx, showFog)
    LuaHuanHunShanZhuangMgr:SyncMiWuLingFogClear(isShowFx, showFog)
end

function Gas2Gac.SyncHuanHunShanZHuangYinYang(yinNum, yangNum)
    LuaHuanHunShanZhuangMgr:SyncHuanHunShanZHuangYinYang(yinNum, yangNum)
end

function Gas2Gac.ShuiManJinShan_LeaderRequestSignUp(info_U)
    local info
    if MsgPackImpl.IsValidUserData(info_U) then
        info = g_MessagePack.unpack(info_U)
    end

    LuaShuiManJinShanMgr:ShuiManJinShan_LeaderRequestSignUp(info)
end

function Gas2Gac.ShuiManJinShan_MemberConfirmSignUp(memberId, buffIdx)
    LuaShuiManJinShanMgr:ShuiManJinShan_MemberConfirmSignUp(memberId, buffIdx)
end

function Gas2Gac.ShuiManJinShan_PopUpSuccessWnd(RewardItems_U, unlockIdx, forceId)
    local info = {}
    if MsgPackImpl.IsValidUserData(RewardItems_U) then
        info = RewardItems_U and g_MessagePack.unpack(RewardItems_U)
    end

    LuaShuiManJinShanMgr:ShuiManJinShan_PopUpSuccessWnd(info, unlockIdx, forceId)
end

function Gas2Gac.ShuiManJinShan_SyncPlayState(playState, waterFactor)
    LuaShuiManJinShanMgr:ShuiManJinShan_SyncPlayState(playState, waterFactor)
end

function Gas2Gac.ShuiManJinShan_CreateWave(x, y, duration, speed, height)
    LuaShuiManJinShanMgr:ShuiManJinShan_CreateWave(x, y, duration, speed, height)
end

function Gas2Gac.ShuiManJinShan_SyncProgress(id, progress, maxProgress, subProgress, maxSubProgress, pause)
    LuaShuiManJinShanMgr:ShuiManJinShan_SyncProgress(id, progress, maxProgress, subProgress, maxSubProgress, pause)
end

function Gas2Gac.ShuiManJinShan_SyncNpc(list_U)
    local npcTbl = list_U and g_MessagePack.unpack(list_U) or nil
    LuaShuiManJinShanMgr:ShuiManJinShan_SyncNpc(npcTbl)
end

function Gas2Gac.ShuiManJinShan_CheckPlayOpenMatchResult(bIsOpen, context)
    LuaShuiManJinShanMgr:ShuiManJinShan_CheckPlayOpenMatchResult(bIsOpen, context)
end

function Gas2Gac.WuLiangShenJing_QueryRecentPassResult(playId, data_U)
    print("WuLiangShenJing_QueryRecentPassResult", playId, data_U)
    local data = g_MessagePack.unpack(data_U)
    if data then
        g_ScriptEvent:BroadcastInLua("WuLiangShenJing_QueryRecentPassResult", playId, data)
    end
end
-- championServerId == 0 表示还没出最后的冠军
function Gas2Gac.SendGuildLeagueCrossGambleInfo(openGambleTypeTbl, gambleInfo_U, championServerId, betRecord_U)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleInfo(openGambleTypeTbl, gambleInfo_U, championServerId, betRecord_U)
end

function Gas2Gac.BetGuildLeagueCrossGambleFailed(gambleType, serverId, totalSilver, curDaiBiNum)
    LuaGuildLeagueCrossMgr:BetGuildLeagueCrossGambleFailed(gambleType, serverId, totalSilver, curDaiBiNum)
end

function Gas2Gac.SendGuildLeagueCrossGambleRecord(championServerId, gambleInfo_U, betRecord_U)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleRecord(championServerId, gambleInfo_U, betRecord_U)
end

function Gas2Gac.SendGuildLeagueCrossGambleOpenType(opengambleTypeTbl)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleOpenType(opengambleTypeTbl)
end

function Gas2Gac.SendGuildLeagueCrossGambleResult(rewardSilver)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleResult(rewardSilver)
end

function Gas2Gac.FKPP_SyncTeamInfo(teamInfo_U)
    -- list{ teamId, score,  members dict{playid, {info.name, info.class, info.serverId, info.contribution}} }
    local allTeamInfo = g_MessagePack.unpack(teamInfo_U)
    LuaDuanWu2022Mgr.teamInfo = allTeamInfo
    g_ScriptEvent:BroadcastInLua("FKPP_SyncTeamInfo")
end

function Gas2Gac.FKPP_StartPlay()
    print("FKPP_StartPlay")
end

function Gas2Gac.FKPP_StartChangeRhythm(rhythmId)
    print("FKPP_StartChangeRhythm", rhythmId)
end

function Gas2Gac.FKPP_EndChangeRhythm(rhythmId)
    print("FKPP_EndChangeRhythm", rhythmId)
end

function Gas2Gac.FKPP_PlayerHitBubble(playerId, newContribution)
    print("FKPP_PlayerHitBubble", playerId, newContribution)
    if LuaDuanWu2022Mgr.teamInfo then
        for i = 1, #LuaDuanWu2022Mgr.teamInfo do
            local members = LuaDuanWu2022Mgr.teamInfo[i].members
            local info = members[playerId]
            if info then
                info.contribution = newContribution
            end
        end
        g_ScriptEvent:BroadcastInLua("FKPP_SyncTeamInfo")
    end
end

function Gas2Gac.FKPP_TeamScore(teamId, score)
    print("FKPP_TeamScore", teamId, score)
    if LuaDuanWu2022Mgr.teamInfo then
        for i = 1, #LuaDuanWu2022Mgr.teamInfo do
            local info = LuaDuanWu2022Mgr.teamInfo[i]
            if info.teamId == teamId and info.score ~= score then
                if CClientMainPlayer.Inst and info.score < score then
                    if info.members[CClientMainPlayer.Inst.Id] then
                        CCommonUIFxWnd.AddUIFx(89000141, 0.7, true, false)
                    else
                        CCommonUIFxWnd.AddUIFx(89000142, 0.7, true, false)
                    end
                end
                info.score = score
            end
        end
        g_ScriptEvent:BroadcastInLua("FKPP_SyncTeamInfo")
    end
end

function Gas2Gac.FKPP_SyncResult(win, addPoint, dailyPoint, teamInfo_U)
    local teamInfo = g_MessagePack.unpack(teamInfo_U)
    print("FKPP_SyncResult", win, addPoint, dailyPoint)
    local data = {}
    data.win = (win == 1)
    print(data.win)
    data.addPoint = addPoint
    data.dailyPoint = dailyPoint
    data.teamInfo = teamInfo
    LuaLiuYi2022Mgr:ShowResultWnd(data)
end

function Gas2Gac.SendMsgToGuildLeagueWatcher(time, msg, context_U, msg2)
    LuaGuildLeagueCrossMgr:SendMsgToGuildLeagueWatcher(time, msg, context_U, msg2)
end

function Gas2Gac.SyncPlayerHpToWatcher(hpInfo_U)
    LuaGuildLeagueCrossMgr:SyncPlayerHpToWatcher(hpInfo_U)
end

function Gas2Gac.SyncGuildLeagueKillNumToWatcher(defendKillNum, attackKillNum)
    LuaGuildLeagueCrossMgr:SyncGuildLeagueKillNumToWatcher(defendKillNum, attackKillNum)
end

function Gas2Gac.SendGuildLeagueCrossExchangePosRecord(record_U)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossExchangePosRecord(record_U)
end

function Gas2Gac.SendGuildLeagueCrossHistoryRankInfo(index, rankInfo_U)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossHistoryRankInfo(index, rankInfo_U)
end

function Gas2Gac.StopPlayCountDown()
    print("StopPlayCountDown")
    CCenterCountdownWnd.count = 0
    CUIManager.CloseUI(CUIResources.CenterCountdownWnd)
end

function Gas2Gac.SendGuildLeagueCrossServerMatchRecord(serverId, record_U)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossServerMatchRecord(serverId, record_U)
end

function Gas2Gac.ShowDeathTimerWnd(reliveStatus, leftDeathTime, currentPlaceReliveCost, latency, wndType, sceneName) 
    CReliveWnd.SetReliveInfo(leftDeathTime, currentPlaceReliveCost, wndType, sceneName)
    if CSwitchMgr.EnableCloseDisabledUIsAtDeath then
        CUIManager.CloseDisabledUIsAtDeath()
    end
    --CUIManager.ShowUI(CUIResources.DeathCoverWnd);

    -- 关宁死亡并且没有快速复活途经时特殊处理
    --[[
    if CGuanNingMgr.Inst.inGnjc and not CClientMainPlayer.Inst:HaveReliveSelfBuff() 
        and not CClientMainPlayer.Inst:HaveReliveSelfSkill() and not CClientMainPlayer.Inst:HaveRelivePartnerSkill() then
        CPostEffect.EnableDieMat()
        CUIManager.ShowUI(CLuaUIResources.GuanNingResurrectionWnd)
        return
    end
    --]]

    if latency > 0 then
        if reliveStatus == EnumReliveStatus_lua.eReLogin then
            CPostEffect.EnableDieMat()
        end

        if wndType == EnumSceneReliveType_lua.eNoReborn then
            return
        end
        --简化处理，没有用ActionState实现
        if CReliveWnd.s_Tick ~= nil then
            CReliveWnd.s_Tick:Invoke()
        end
        CReliveWnd.s_Tick = CTickMgr.Register(DelegateFactory.Action(function () 
            if CClientMainPlayer.Inst == nil then
                return
            end

            if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("Death")) == 1 then
                CUIManager.ShowUI(L10.UI.CUIResources.ReliveWnd)
            end
        end), latency, ETickType.Once)
        if CCityWarMgr.Inst:IsInCityWarScene() or CCityWarMgr.Inst:IsInMirrorWarScene() then
            CCenterCountdownWnd.count = math.floor(latency / 1000)
            CCenterCountdownWnd.InitStartTime(true)
            CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
        end
    else
        CUIManager.CloseUI(CUIResources.MiniMapPopWnd)
        --CUIManager.instance.CloseAllPopupViews();
        CPostEffect.EnableDieMat()
        if wndType == EnumSceneReliveType_lua.eNoReborn then
            return
        end
        CUIManager.ShowUI(CUIResources.ReliveWnd)
    end
end


function Gas2Gac.StartTeamConfirm()
    local msg = g_MessageMgr:FormatMessage("Team_Perpare_Confirm_Tip")
    MessageWndManager.ShowConfirmMessage(msg,30,false,
        DelegateFactory.Action(function()
            Gac2Gas.TeamConfirm(true)
        end),   
        DelegateFactory.Action(function()
            Gac2Gas.TeamConfirm(false)
        end))

    EventManager.BroadcastInternalForLua(EnumEventType.FlashPCWindow, {})
end

function Gas2Gac.WuLiangShenJing_SetChuZhan(chuzhan_U)
    print("WuLiangShenJing_SetChuZhan", chuzhan_U)

    local chuzhan = g_MessagePack.unpack(chuzhan_U)
    if LuaWuLiangMgr.enterData then
        LuaWuLiangMgr.enterData.ChuZhan = chuzhan
    end
    
    if LuaWuLiangMgr.huobanData then
        LuaWuLiangMgr.huobanData.ChuZhan = chuzhan
    end
    g_ScriptEvent:BroadcastInLua("WuLiangShenJing_SetChuZhan")
end

-- 同步伙伴血量等信息
function Gas2Gac.WuLiangShenJing_SyncHuoBan(huobanInfo_U)
    local tbl = g_MessagePack.unpack(huobanInfo_U)
    LuaWuLiangMgr.huobanSyncData = tbl
    g_ScriptEvent:BroadcastInLua("WuLiangShenJing_SyncHuoBan")
end

function Gas2Gac.WuLiangShenJing_SyncGates(gates_U)
    local tbl = g_MessagePack.unpack(gates_U)
    LuaWuLiangMgr:SetPrepareScene(tbl)
end

function Gas2Gac.WuLiangShenJing_ApproachGate(engineId, gate_U, huoban_U, chuzhan_U)
    print("WuLiangShenJing_ApproachGate", engineId, gate_U, huoban_U, chuzhan_U)
    local data = {}
    data.Gate = g_MessagePack.unpack(gate_U)
    data.HuoBan = g_MessagePack.unpack(huoban_U)
    data.ChuZhan = g_MessagePack.unpack(chuzhan_U)

    for k, v in pairs(data.Gate) do
        print(k,v)
    end
    LuaWuLiangMgr.enterData = data

    -- 打开进入按钮
    LuaWuLiangMgr:OpenEnterBtn(false)
end

function Gas2Gac.WuLiangShenJing_LeaveGate(engineId)
    print("WuLiangShenJing_LeaveGate", engineId)
    CUIManager.CloseUI(CLuaUIResources.WuLiangEnterButtonWnd)
end

function Gas2Gac.WuLiangShenJing_ApproachElevator(engineId, unlockTbl_U)
    print("WuLiangShenJing_ApproachElevator", engineId, unlockTbl_U)
    local tbl = g_MessagePack.unpack(unlockTbl_U)
    if engineId == 0 then
        LuaWuLiangMgr:OpenLevelWnd(tbl)
    else
        -- 设置数据和按钮界面
        LuaWuLiangMgr.passPlayIdTbl = tbl
        LuaWuLiangMgr:OpenEnterBtn(true)
    end
end

function Gas2Gac.WuLiangShenJing_LeaveElevator(engineId)
    print("WuLiangShenJing_LeaveElevator", engineId)
    CUIManager.CloseUI(CLuaUIResources.WuLiangEnterButtonWnd)
end

function Gas2Gac.WuLiangShenJing_QueryHuoBanDataResult(HuoBan_U, chuzhan_U, SkillScore, InheritScore)
    print("WuLiangShenJing_QueryHuoBanDataResult", HuoBan_U, chuzhan_U, SkillScore, InheritScore)
    local data = {}
    data.HuoBan = g_MessagePack.unpack(HuoBan_U)
    data.ChuZhan = g_MessagePack.unpack(chuzhan_U)
    data.SkillScore = SkillScore
    data.InheritScore = InheritScore
    LuaWuLiangMgr.huobanData = data
    g_ScriptEvent:BroadcastInLua("WuLiangShenJing_QueryHuoBanDataResult", data)
    
end

function Gas2Gac.WuLiangShenJing_UpgradeSkillSuc(huobanId, skillCls, nextLevel)
    print("WuLiangShenJing_UpgradeSkillSuc", huobanId, skillCls, nextLevel)
    if LuaWuLiangMgr.huobanData then
        local info = LuaWuLiangMgr.huobanData.HuoBan[huobanId].SkillLevelTbl
        info[skillCls] = nextLevel
        g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex")
    end

    if LuaWuLiangMgr.enterData and LuaWuLiangMgr.enterData.HuoBan and LuaWuLiangMgr.enterData.HuoBan[huobanId] then
        local info = LuaWuLiangMgr.enterData.HuoBan[huobanId].SkillLevelTbl
        info[skillCls] = nextLevel
        g_ScriptEvent:BroadcastInLua("WuLiangShenJing_Refresh_Challenge")
    end
end

function Gas2Gac.WuLiangShenJing_UpgradeInheritSuc(huobanId, nextLevel)
    print("WuLiangShenJing_UpgradeInheritSuc", huobanId, nextLevel)
    if LuaWuLiangMgr.huobanData then
        local info = LuaWuLiangMgr.huobanData.HuoBan[huobanId]
        info.InheritLevel = nextLevel
        g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex")
    end

    if LuaWuLiangMgr.enterData and LuaWuLiangMgr.enterData.HuoBan and LuaWuLiangMgr.enterData.HuoBan[huobanId] then
        local info = LuaWuLiangMgr.enterData.HuoBan[huobanId]
        info.InheritLevel = nextLevel
        g_ScriptEvent:BroadcastInLua("WuLiangShenJing_Refresh_Challenge")
    end
end

function Gas2Gac.WuLiangShenJing_ResetHuoBanSkillSuc(huobanId, skillCls)
    print("WuLiangShenJing_ResetHuoBanSkillSuc", huobanId, skillCls)
    if LuaWuLiangMgr.huobanData then
        local info = LuaWuLiangMgr.huobanData.HuoBan[huobanId].SkillLevelTbl
        info[skillCls] = 0
        g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex")
    end

    if LuaWuLiangMgr.enterData and LuaWuLiangMgr.enterData.HuoBan and LuaWuLiangMgr.enterData.HuoBan[huobanId] then
        local info = LuaWuLiangMgr.enterData.HuoBan[huobanId].SkillLevelTbl
        info[skillCls] = 0
        g_ScriptEvent:BroadcastInLua("WuLiangShenJing_Refresh_Challenge")
    end
end

function Gas2Gac.WuLiangShenJing_ResetHuoBanInheritSuc(huobanId)
    print("WuLiangShenJing_ResetHuoBanInheritSuc", huobanId)
    if LuaWuLiangMgr.huobanData and LuaWuLiangMgr.huobanData.HuoBan and LuaWuLiangMgr.huobanData.HuoBan[huobanId] then
        local info = LuaWuLiangMgr.huobanData.HuoBan[huobanId]
        info.InheritLevel = 0
        g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex")
    end

    if LuaWuLiangMgr.enterData and LuaWuLiangMgr.enterData.HuoBan and LuaWuLiangMgr.enterData.HuoBan[huobanId] then
        local info = LuaWuLiangMgr.enterData.HuoBan[huobanId]
        info.InheritLevel = 0
        g_ScriptEvent:BroadcastInLua("WuLiangShenJing_Refresh_Challenge")
    end
end

function Gas2Gac.WuLiangShenJing_SetSkillScore(score)
    print("WuLiangShenJing_SetSkillScore", score)
    if LuaWuLiangMgr.huobanData then
        LuaWuLiangMgr.huobanData.SkillScore = score
        g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex")
    end
end

function Gas2Gac.WuLiangShenJing_SetInheritScore(score)
    print("WuLiangShenJing_SetInheritScore", score)
    if LuaWuLiangMgr.huobanData then
        LuaWuLiangMgr.huobanData.InheritScore = score
        g_ScriptEvent:BroadcastInLua("LuaWuLiangCompanionInfoWnd_SelectIndex")
    end
end

function Gas2Gac.WuLiangShenJing_PlaySuc(data_U, bSuccess)
    print("WuLiangShenJing_PlaySuc", data_U, bSuccess)
    local data = g_MessagePack.unpack(data_U)
    data.bSuccess = bSuccess
    LuaWuLiangMgr:OpenResultWnd(data)
end

function Gas2Gac.WuLiangShenJing_QueryHuoBanPropResult(huobanId, data_U)
    print("WuLiangShenJing_QueryHuoBanPropResult", huobanId, data_U)
    local data = g_MessagePack.unpack(data_U)
    g_ScriptEvent:BroadcastInLua("WuLiangShenJing_QueryHuoBanPropResult", huobanId, data)
end

function Gas2Gac.WuLiangShenJing_QueryHuoBanPropResultWithNextLv(huobanId, data_U, dataNextLv_U)
    print("WuLiangShenJing_QueryHuoBanPropResultWithNextLv", huobanId, data_U, dataNextLv_U)
    local data = g_MessagePack.unpack(data_U)
    local dataNextLv = g_MessagePack.unpack(dataNextLv_U)
    g_ScriptEvent:BroadcastInLua("WuLiangShenJing_QueryHuoBanPropResultWithNextLv", huobanId, data, dataNextLv)
end

function Gas2Gac.SetGuildCommanderForAID(commanderPlayerId, commanderPlayerName, bIsReconnect)
    LuaChatMgr:SetGuildCommanderForAID(commanderPlayerId, commanderPlayerName, bIsReconnect)
end

function Gas2Gac.ResetGuildCommanderForAID()
    LuaChatMgr:ResetGuildCommanderForAID()
end

function Gas2Gac.SFEQ_SyncStage(stage)
    LuaQiXi2022Mgr:SFEQ_SyncStage(stage)
end

function Gas2Gac.SFEQ_SyncOptionInfo(optionData_U)
    LuaQiXi2022Mgr:SFEQ_SyncOptionInfo(optionData_U)
end

function Gas2Gac.SFEQ_ChangeOptionText(textId)
    LuaQiXi2022Mgr:SFEQ_ChangeOptionText(textId)
end

function Gas2Gac.SFEQ_OnOptionCountDownTick(countDown)
    LuaQiXi2022Mgr:SFEQ_OnOptionCountDownTick(countDown)
end

function Gas2Gac.SFEQ_PlayerClickOption(playerId,optionId)
    LuaQiXi2022Mgr:SFEQ_PlayerClickOption(playerId,optionId)
end

function Gas2Gac.SFEQ_StartConfirmOption(optionId, countDown, textId)
    LuaQiXi2022Mgr:SFEQ_StartConfirmOption(optionId, countDown, textId)
end

function Gas2Gac.SFEQ_CancelConfirmOption(countDown, textId)
    LuaQiXi2022Mgr:SFEQ_CancelConfirmOption(countDown, textId)
end

function Gas2Gac.SFEQ_EndSelectOption(optionId)
    LuaQiXi2022Mgr:SFEQ_EndSelectOption(optionId)
end

function Gas2Gac.SFEQ_WPFL_PlayerOpenWnd(playerId)
    LuaQiXi2022Mgr:SFEQ_WPFL_PlayerOpenWnd(playerId)
end

function Gas2Gac.SFEQ_WPFL_SyncClickOrReleaseItemResult(posId, bCanClick, playerId)
    LuaQiXi2022Mgr:SFEQ_WPFL_SyncClickOrReleaseItemResult(posId, bCanClick, playerId)
end

function Gas2Gac.SFEQ_WPFL_PlayerCloseWnd(playerId)
    LuaQiXi2022Mgr:SFEQ_WPFL_PlayerCloseWnd(playerId)
end

function Gas2Gac.SFEQ_WPFL_PutItemSuccess(posId)
    LuaQiXi2022Mgr:SFEQ_WPFL_PutItemSuccess(posId)
end

function Gas2Gac.SFEQ_WPFL_EndPlay()
    LuaQiXi2022Mgr:SFEQ_WPFL_EndPlay()
end

function Gas2Gac.SFEQ_XW_UpdateDistance(distance)
    LuaQiXi2022Mgr:SFEQ_XW_UpdateDistance(distance)
end

function Gas2Gac.SFEQ_XW_SyncPickNum(pickNum)
    LuaQiXi2022Mgr:SFEQ_XW_SyncPickNum(pickNum)
end

function Gas2Gac.SFEQ_XW_SyncTargetPos(targetX, targetY)
    LuaQiXi2022Mgr:SFEQ_XW_SyncTargetPos(targetX, targetY)
end

function Gas2Gac.SFEQ_SyncEndingResult(endingId, playerInfo_U)
    LuaQiXi2022Mgr:SFEQ_SyncEndingResult(endingId, playerInfo_U)
end

function Gas2Gac.StopBGMusic()
    SoundManager.Inst:StopBGMusic()
end

function Gas2Gac.BeginShootBirdsInPlay(threshold, birdLength, birdWidth, flySpeed)
    LuaZhuJueJuQingMgr:BeginShootBirdsInPlay(threshold, birdLength, birdWidth, flySpeed)
end

function Gas2Gac.BeginShootBirds(taskId, curCount, needCount)
    LuaZhuJueJuQingMgr:BeginShootBirds(taskId, curCount, needCount)
end

function Gas2Gac.SendWuZiQiInfo(rpcType, info_U)
    LuaShiTuMgr:SendWuZiQiInfo(rpcType, info_U)
end


function Gas2Gac.SFEQ_XW_EndPlay()
    LuaQiXi2022Mgr:SFEQ_XW_EndPlay()
end

function Gas2Gac.YYS_SendInvitation(playerName)
    LuaQiXi2022Mgr:YYS_SendInvitation(playerName)
end

function Gas2Gac.YYS_PlantTreeSuccess(bInvitor, playerName)
    LuaQiXi2022Mgr:YYS_PlantTreeSuccess(bInvitor, playerName)
end

function Gas2Gac.YYS_UpdateMissionProgress(kind, id, value, bFinish, bClaimReward)
    print("YYS_UpdateMissionProgress", kind, id, value, bFinish, bClaimReward)
    LuaQiXi2022Mgr:YYS_UpdateMissionProgress(kind, id, value, bFinish, bClaimReward)
end

function Gas2Gac.YYS_IsFirstOpenTab(bFirstOpen, playData, dailyMissionData)
    LuaQiXi2022Mgr:YYS_IsFirstOpenTab(bFirstOpen, playData, dailyMissionData)
end

function Gas2Gac.YYS_SyncPlayData(playDataU, missionDataU)
    LuaQiXi2022Mgr:YYS_SyncPlayData(playDataU, missionDataU)
end

function Gas2Gac.QueryCompositeExtraEquipWordCostReturn(targetPlace, targetPos, targetEquipId, costYinLiang, costItemId, costItemCount, costJade)
	print("QueryCompositeExtraEquipWordCostReturn", targetPlace, targetPos, targetEquipId, costYinLiang, costItemId, costItemCount, costJade)
end

function Gas2Gac.RequestCanCompositeExtraEquipWordReturn(targetPlace, targetPos, targetEquipId, canComposite)
	print("QueryCompositeExtraEquipWordCostReturn", targetPlace, targetPos, targetEquipId, canComposite)
end

function Gas2Gac.CompositeExtraEquipWordResult(targetEquipId, result)
	print("CompositeExtraEquipWordResult", result)
    -- 合成结果
    -- 弹出新界面
    if result then
        CLuaEquipMgr.m_ResultEquipItemId = targetEquipId
        CUIManager.ShowUI(CLuaUIResources.EquipCompositeResultWnd)
    end
    g_ScriptEvent:BroadcastInLua("CompositeExtraEquipWordResult")
    g_ScriptEvent:BroadcastInLua("SwitchEquipmentTabIndex", {0, 2})
end

function Gas2Gac.TryConfirmCompositeExtraEquipWordResult(equipId, fightPropUD, equipNewGrade)
	print("TryConfirmCompositeExtraEquipWordResult", equipId, equipNewGrade)
    local fightProp = CreateFromClass(CPlayerPropertyFight)
    fightProp:LoadFromString(fightPropUD)

    CEquipmentBaptizeMgr.Inst:CompareWordDif(fightProp, equipId)
end

function Gas2Gac.QueryDisassembleExtraEquipCostAndItemsReturn(place, pos, equipId, getItemInfosUD, cost, bSilverOnly)
	print("QueryDisassembleExtraEquipCostAndItemsReturn", place, pos, equipId, cost, tostring(bSilverOnly))
	local list = MsgPackImpl.unpack(getItemInfosUD)
	if not list then return end
    
	for i = 0, list.Count - 1, 3 do
		local itemId = tonumber(list[i])
		local count = tonumber(list[i+1])
		local binded = tonumber(list[i+2])
		print("DisassembleExtraEquipItems", itemId, count, binded)
	end

    local data = {}
    data.equipId = equipId
    data.place = place
    data.pos = pos
    data.cost = cost
    data.bSilverOnly = bSilverOnly
    data.itemList = g_MessagePack.unpack(getItemInfosUD)
    CLuaEquipMgr.m_DisassembleData = data

    CUIManager.ShowUI(CLuaUIResources.EquipExtraDisassembleWnd)
end

function Gas2Gac.RequestCanForgeExtraEquipResult(canForge, place, pos, equipId, itemInfoUD)
	print("RequestCanForgeExtraEquipResult", tostring(canForge), place, pos, equipId)

	local list = MsgPackImpl.unpack(itemInfoUD)
	if not list then return end
	for i = 0, list.Count - 1, 3 do
		local costItemId = tonumber(list[i])
		local count = tonumber(list[i+1])
		local binded = tonumber(list[i+2])
		print("ForgeExtraEquipItem", costItemId, count, binded)
	end
end

function Gas2Gac.ForgeExtraEquipSuccess(newLevel, newTotalValue)
	print("ForgeExtraEquipSuccess", newLevel, newTotalValue)
    g_ScriptEvent:BroadcastInLua("ForgeExtraEquipSuccess", newLevel, newTotalValue)
end

function Gas2Gac.RequestCanQuickForgeExtraEquipResult(canQuickForge, place, pos, equipId, newLevel, itemInfoUD)
    print(canQuickForge, place, pos, equipId, newLevel, costItemCount)
    local itemInfo = g_MessagePack.unpack(itemInfoUD)
    for k,v in pairs(itemInfo) do
        print(k,v)
    end
end

function Gas2Gac.QuickForgeExtraEquipSuccess(newLevel, newTotalValue)
    print(newLevel, newTotalValue)
    g_ScriptEvent:BroadcastInLua("ForgeExtraEquipSuccess", newLevel, newTotalValue)
end

function Gas2Gac.RequestCanForgeBreakExtraEquipResult(canBreak, place, pos, equipId)
	print("RequestCanForgeBreakExtraEquipResult", tostring(canBreak), place, pos, equipId)
end

function Gas2Gac.ForgeBreakExtraEquipSuccess(newBreakLevel)
	print("ForgeBreakExtraEquipSuccess", newBreakLevel)
    g_ScriptEvent:BroadcastInLua("ForgeBreakExtraEquipSuccess", newBreakLevel)
end

function Gas2Gac.QueryForgeResetExtraEquipCostAndItemsReturn(place, pos, equipId, getItemInfosUD, costYuanBao)
	print("QueryForgeResetExtraEquipCostAndItemsReturn", place, pos, equipId, costYuanBao)

    -- local list = MsgPackImpl.unpack(getItemInfosUD)
	-- if not list then return end

	-- for i = 0, list.Count - 1, 3 do
	-- 	local itemId = tonumber(list[i])
	-- 	local count = tonumber(list[i+1])
	-- 	local binded = tonumber(list[i+2])
	-- 	print("ForgeResetExtraEquipItems", itemId, count, binded)
	-- end

    local data = {}
	data.itemList = g_MessagePack.unpack(getItemInfosUD)
	data.costYuanBao = costYuanBao

    CLuaEquipMgr.m_ForgeResetItemId = equipId
    CLuaEquipMgr.m_ForgeResetCostData = data

    CUIManager.ShowUI(CLuaUIResources.EquipForgeResetWnd)
end

function Gas2Gac.ForgeResetExtraEquipSuccess()
	print("ForgeResetExtraEquipSuccess")
    g_ScriptEvent:BroadcastInLua("ForgeResetExtraEquipSuccess")
end

function Gas2Gac.ShiftBackPendant(xOffset, yOffset, zOffset)
	print("ShiftBackPendant", xOffset, yOffset, zOffset)
end

function Gas2Gac.SyncBackPendantAppearance(engineId, appearance)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj and (TypeIs(obj, typeof(CClientOtherPlayer)) or TypeIs(obj, typeof(CClientMainPlayer))) then
		obj.AppearanceProp.BackPendantAppearance = appearance
        obj:RefreshAppearance()
	end
end

function Gas2Gac.UpdateBackPendantPosTemplate(templateIdx, xOffset, yOffset, zOffset)
	local posTemplate = CBackPendantPosData()
    posTemplate.PosData = CreateFromClass(MakeArrayClass(SByte),4)
	posTemplate.PosData[1] = xOffset
	posTemplate.PosData[2] = yOffset
	posTemplate.PosData[3] = zOffset
	if CClientMainPlayer.Inst then
        local dict = CClientMainPlayer.Inst.PlayProp.BackPendantPosTemplate
        CommonDefs.DictSet_LuaCall(dict, templateIdx, posTemplate)
    end
end

function Gas2Gac.SwitchBackPendantPosTemplateSuccess(templateIdx)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.PlayProp.ActiveBackPendantPosTemplateIdx = templateIdx
	end
end

function Gas2Gac.PlayRainyEffect(delayDuration, duration, strength)
    LuaShuiManJinShanMgr:PlayRainyEffect(delayDuration, duration, strength)
end

function Gas2Gac.XinBaiAchv_PlayerAddAchievement(achievementId)
    print("XinBaiAchv_PlayerAddAchievement", achievementId)
    LuaVoiceAchievementMgr.OnPlayerAddAchievement(achievementId)
end

function Gas2Gac.XinBaiAchv_SyncAchievementInfo(shareData, markData)
    print("XinBaiAchv_SyncAchievementInfo")
    LuaVoiceAchievementMgr.OnSyncAchievementInfo(shareData, markData)
end

function Gas2Gac.SFEQ_XW_SyncEndTime(endTime)
    LuaQiXi2022Mgr:SFEQ_XW_SyncEndTime(endTime)
end

function Gas2Gac.SFEQ_SyncPlayData(playData)
    LuaQiXi2022Mgr:SFEQ_SyncPlayData(playData)
end

function Gas2Gac.SetAtmosphere(skyboxId)
    CRenderScene.Inst:SetAtmosphere(skyboxId)
end

function Gas2Gac.QiXi2022_SyncActivityInfo(bIsOpen, bIsOpenPlay)
    LuaQiXi2022Mgr:QiXi2022_SyncActivityInfo(bIsOpen, bIsOpenPlay)
end

function Gas2Gac.UpdateSkillHintFx(skillId, isHint)
    LuaXinBaiLianDongMgr:UpdateSkillHintFx(skillId, isHint)
end

function Gas2Gac.XinBaiAchv_ShareAchievementFail(id)
    print("XinBaiAchv_ShareAchievementFail", id)
end

function Gas2Gac.AdjustCameraDefaultZY(newDist, newHeight)
    print("AdjustCameraDefaultZY", newDist, newHeight)
    if CameraFollow.Inst == nil then
        return
    end
    local defaultYAngle = CameraFollow.Inst.mDefault3DParam.CurYAngle
    local defaultRadians = defaultYAngle / 180 * PI
    local curYAngle = CameraFollow.Inst.CurCameraParam.CurYAngle
    local curRadians = curYAngle / 180 * PI
    

    CCameraParam.MAX_DIST_TO_PLAYER = (newDist / 15.305) * 28.6104
    CCameraParam.MIN_DIST_TO_PLAYER = (newDist / 15.305) * 1.2

	local defaultVec3 = CameraFollow.Inst.mDefaultRZY
    local targetVec3 = CameraFollow.Inst.targetRZY
    defaultVec3.y = newDist * Mathf.Sin(defaultRadians)
    defaultVec3.z = newDist * Mathf.Cos(defaultRadians)
    targetVec3.y = newDist * Mathf.Sin(curRadians)
    targetVec3.z = newDist * Mathf.Cos(curRadians)
    CameraFollow.Inst.targetRZY = targetVec3
    CameraFollow.Inst.mDefaultRZY = defaultVec3
    CameraFollow.Inst.mDefault3DParam:ApplyRZY(defaultVec3)
    CameraFollow.Inst.CurCameraParam:ApplyRZY(targetVec3)
    CameraFollow.Inst.m_T = ((newDist - CCameraParam.MIN_DIST_TO_PLAYER) / (CCameraParam.MAX_DIST_TO_PLAYER - CCameraParam.MIN_DIST_TO_PLAYER)) * 2 - 1
    CameraFollow.Inst.MaxSpHeight = newHeight
    CameraFollow.Inst:SetCurrentRZY()
end


function Gas2Gac.ShenYao_QueryRemainTimesResult(RemainRewardTimes, RewardTimesToday, RemainJoinTimes, JoinTimesToday)
    print("ShenYao_QueryRemainTimesResult", RemainRewardTimes, RewardTimesToday, RemainJoinTimes, JoinTimesToday)
    g_ScriptEvent:BroadcastInLua("ShenYao_QueryRemainTimesResult", RemainRewardTimes, RewardTimesToday, RemainJoinTimes, JoinTimesToday)
end

function Gas2Gac.ShenYao_QueryRejectLevelResult(level)
    LuaXinBaiLianDongMgr:ShenYao_QueryRejectLevelResult(level)
end

function Gas2Gac.ShenYao_OpenTeamShenYaoInfoWnd(tbl_U)
    LuaXinBaiLianDongMgr:ShenYao_OpenTeamShenYaoInfoWnd(tbl_U)
end

function Gas2Gac.ActivityPannelShowReddot(activityId, bShowReddot)
    print("ActivityPannelShowReddot", activityId, bShowReddot)
    -- CLuaScheduleMgr:ActivityPannelShowReddot(activityId, bShowReddot)
end

function Gas2Gac.YYS_OnPlayerRejectInvitation()
    LuaQiXi2022Mgr:YYS_OnPlayerRejectInvitation()
end

function Gas2Gac.SFEQ_WPFL_ClickItemFail(itemId)
    LuaQiXi2022Mgr:SFEQ_WPFL_ClickItemFail(itemId)
end

function Gas2Gac.QueryDouHunCrossWaiKaRankResult(rankInfo_U)
    local info = g_MessagePack.unpack(rankInfo_U)
    print("QueryDouHunCrossWaiKaRankResult")


    g_ScriptEvent:BroadcastInLua("QueryDouHunCrossWaiKaRankResult", info)
end

function Gas2Gac.JYDSY_StartNextRound(roundTime, barrierAndPosInfo_U, bIsOnMove, nextMoves_U)
    LuaZhongQiu2022Mgr:JYDSY_StartNextRound(roundTime, barrierAndPosInfo_U, bIsOnMove, nextMoves_U)
end

function Gas2Gac.JYDSY_GameEndSyncResult(winnerId)
    LuaZhongQiu2022Mgr:JYDSY_GameEndSyncResult(winnerId)
end

function Gas2Gac.JYDSY_SyncRoundTime(roundTime)
    LuaZhongQiu2022Mgr:JYDSY_SyncRoundTime(roundTime)
end

function Gas2Gac.JYDSY_SyncPlayerInfo(playerInfo_U)
    LuaZhongQiu2022Mgr:JYDSY_SyncPlayerInfo(playerInfo_U)
end

function Gas2Gac.JYDSY_SyncBarrierInfo(boardBarrierInfo_U)
    LuaZhongQiu2022Mgr:JYDSY_SyncBarrierInfo(boardBarrierInfo_U)
end

function Gas2Gac.JYDSY_ShowBubbleMsg(msgId)
    LuaZhongQiu2022Mgr:JYDSY_ShowBubbleMsg(msgId)
end

-- add 可以是负数 total 是+add之前的总数
-- 开始结束通过状态控制 enter or leave
function Gas2Gac.SyncShiTuDuLingTaskInfo(total, add, index)
    LuaShiTuMgr:SyncShiTuDuLingTaskInfo(total, add, index)
end

function Gas2Gac.ShowShiTuJianLaJiMsg(engineId, bSuccess)
    LuaShiTuMgr:ShowShiTuJianLaJiMsg(engineId, bSuccess)
end

function Gas2Gac.SyncShiTuJianLaJiScore(curScore, needScore)
    LuaShiTuMgr:SyncShiTuJianLaJiScore(curScore, needScore)
end

function Gas2Gac.UpdateShiTuQingTaskFinishedTimes(taskId, subTimes)
    LuaGamePlayMgr:UpdateShiTuQingTaskFinishedTimes(taskId, subTimes)
end

function Gas2Gac.OpenShifuInviteTudiJoinShiTuTeamWnd(taskId)
    LuaShiTuMgr:OpenShifuInviteTudiJoinShiTuTeamWnd(taskId)
end

function Gas2Gac.UpdateYiTiaoLongTaskFinishedTimes(taskId, round, subTimes, todayTimes)
    LuaGamePlayMgr:UpdateYiTiaoLongTaskFinishedTimes(taskId, round, subTimes, todayTimes)
end

-- 找秘籍一开始的罗盘界面 参数是怪的位置
function Gas2Gac.ShowShiTuZhaoMiJiStartWnd(posX, posY)
    LuaShiTuMgr:ShowShiTuZhaoMiJiStartWnd(posX, posY)
end

function Gas2Gac.SyncShiTuZhaoMiJiScore(curScore, needScore)
    LuaShiTuMgr:SyncShiTuZhaoMiJiScore(curScore, needScore)
end

-- 更新找秘籍副本怪物位置 
function Gas2Gac.SyncShiTuZhaoMiJiMonsterPos(posX, posY)
    LuaShiTuMgr:SyncShiTuZhaoMiJiMonsterPos(posX, posY)
end

function Gas2Gac.SyncUXDynamicShopShangJiaGiftItems(event, traceId, scm, itemInfos_U, oriTotalJade, totalJade, discount, expireTime)
	LuaLingyuGiftRecommendMgr:SyncUXDynamicShopShangJiaGiftItems(event, traceId, scm, itemInfos_U, oriTotalJade, totalJade, discount, expireTime)
end

function Gas2Gac.BuyUXDynamicGiftItemsSuccess(event, traceId)
	LuaLingyuGiftRecommendMgr:BuyUXDynamicGiftItemsSuccess(event, traceId)
end

function Gas2Gac.SendUXDynamicMallHomeItems(bUseRecommendItems, trace, scm, itemInfos_U, expireTime)
end

function Gas2Gac.Double11MQHX_SyncPlayState(playState, startTime, endTime, RankTbl_U)
    LuaShuangshiyi2022Mgr:Double11MQHX_SyncPlayState(playState, startTime, endTime, RankTbl_U)
end

function Gas2Gac.Double11MQHX_BroadCastAction(playerId, action, data_U)
    LuaShuangshiyi2022Mgr:Double11MQHX_BroadCastAction(playerId, action, data_U)
end

function Gas2Gac.Double11MQHX_ShowRewardWnd(Rewards_U, RankTbl_U)
    LuaShuangshiyi2022Mgr:Double11MQHX_ShowRewardWnd(Rewards_U, RankTbl_U)
end

function Gas2Gac.Double11SZTB_SyncPlayState(playState, EnterStateTime, NextStateTime, round, traps_U)
    LuaShuangshiyi2022Mgr:Double11SZTB_SyncPlayState(playState, EnterStateTime, NextStateTime, round, traps_U)
end

function Gas2Gac.Double11SZTB_ShowRewardWnd(bSuccess, playDuration, bReward, bNewRecord)
    LuaShuangshiyi2022Mgr:Double11SZTB_ShowRewardWnd(bSuccess, playDuration, bReward, bNewRecord)
end

function Gas2Gac.SendDayFestivalInfo(todayFestivalForClient, time)
    CLuaScheduleMgr:BuildDayFestivalInfo(todayFestivalForClient, time)
end

function Gas2Gac.GJZL_SyncStage(stage, countDownTime)
    LuaWorldCup2022Mgr:GJZL_SyncStage(stage, countDownTime)
end

function Gas2Gac.GJZL_SyncProgress(progress, fullProgress)
    LuaWorldCup2022Mgr:GJZL_SyncProgress(progress, fullProgress)
end

function Gas2Gac.InitShuiGuoPaiDuiScore(curScore, maxScore)
    LuaFruitPartyMgr:InitShuiGuoPaiDuiScore(curScore, maxScore)
end

function Gas2Gac.GJZL_SyncSelectPosition(position2PlayerName_U)
    LuaWorldCup2022Mgr:GJZL_SyncSelectPosition(position2PlayerName_U)
end

function Gas2Gac.GJZL_SelectPositionSuccess(position)
    LuaWorldCup2022Mgr:GJZL_SelectPositionSuccess(position)
end

function Gas2Gac.UpdateCuJuPlayerPlace(place2PlayerInfo_U)
    LuaCuJuMgr:UpdateCuJuPlayerPlace(place2PlayerInfo_U)
end

function Gas2Gac.SetCuJuPlayerPlaceSuccess(position)
    LuaCuJuMgr:SetCuJuPlayerPlaceSuccess(position)
end

function Gas2Gac.UpdateReddotDataWithKey(key, times, expiredTime)
    LuaActivityRedDotMgr:UpdateReddotDataWithKey(key, times, expiredTime)
end

function Gas2Gac.SyncTrafficLightSignUpResult(isSignUp)
    LuaHalloween2022Mgr.SyncPlayerTrafficMatchStatus(isSignUp)
end

function Gas2Gac.TrafficLightPlayResult(playerStatus, rank, totalCount, participationAwardTimes, dailyAwardTimes, isGet1stAward, isGetOnceAchieveAward)
    LuaHalloween2022Mgr.ShowMuTouRenResultWnd(playerStatus, rank, totalCount, participationAwardTimes, dailyAwardTimes, isGet1stAward, isGetOnceAchieveAward)
end

function Gas2Gac.SyncTrafficLightPlayerAliveNum(aliveNum)
    LuaHalloween2022Mgr.SyncPlayerTrafficMembers(aliveNum)
end

function Gas2Gac.SyncTrafficLightPlayerFail(playerEngineId)
    LuaHalloween2022Mgr.PlayerBeFail(playerEngineId)
end

function Gas2Gac.SyncStartTrafficLightNpcPerform()
    LuaHalloween2022Mgr.SyncStartTrafficLightNpcPerform()
end

function Gas2Gac.SyncTrafficLightEnd()
    LuaHalloween2022Mgr.SyncTrafficLightNpcEndPerform()
end

function Gas2Gac.SyncEndTrafficLightNpcPerform(isLeft, count)
    LuaHalloween2022Mgr.SyncEndTrafficLightNpcPerform(isLeft, count)
end

function Gas2Gac.QueryPlayerTrafficLightAward_Result(participationAwardTimes, dailyAwardTimes, isGet1stAward, isGetOnceAchieveAward)
    LuaHalloween2022Mgr.SyncPlayerTrafficLightAward(participationAwardTimes, dailyAwardTimes, isGet1stAward, isGetOnceAchieveAward)
end

function Gas2Gac.ProtectCampfirePlayResult(isHero, isWin, heroAward, normalOnceAward, normalDailyAward)
    LuaHalloween2022Mgr.ShowFireDefenceResultWnd(isWin,isHero,heroAward, normalOnceAward, normalDailyAward)
end

function Gas2Gac.ProtectFireMonsterNewWave(count)
    LuaHalloween2022Mgr.SyncFireDefenceMonsterTimes(count)
end


EnumTrafficLightNpcState = {
    Back = 1,               
    TurnFront = 2,          
    Front = 3               
}

function Gas2Gac.SyncTrafficLightNpcState(templateId, x, y, state, totalKeepDuration, leftKeepDuration)
    LuaHalloween2022Mgr.ShowTrafficLightNPC(templateId, x, y, state, totalKeepDuration, leftKeepDuration)
end

function Gas2Gac.QueryPlayerIsMatchingTrafficLight_Result(isMatching)
    LuaHalloween2022Mgr.SyncPlayerTrafficMatchStatus(isMatching)
end

function Gas2Gac.WorldCupJingCai_UpdateAllJingCaiData(allPeriodResultData_U, allRoundResultData_U, odds_U, jingCaiData_U, groupRank, taotaiStage)
    LuaWorldCup2022Mgr:UpdateAllJingCaiData(allPeriodResultData_U, allRoundResultData_U, odds_U, jingCaiData_U, groupRank, taotaiStage)
end

function Gas2Gac.WorldCupJingCai_OnVoteSuccess(pid, value, num, allPeriodResultData_U, allRoundResultData_U, odds_U)
    LuaWorldCup2022Mgr:JingCaiVoteSuccess(pid, value, num, allPeriodResultData_U, allRoundResultData_U, odds_U)
end

function Gas2Gac.WorldCupJingCai_OnReceiveJingCaiAwardSuccess()
    LuaWorldCup2022Mgr:OnReceiveJingCaiAwardSuccess()
end

function Gas2Gac.PlayTeamGameVideoWithCheckNewer(videoName, playerCount, canSkip, isNewPlayer)
    --忽略AllowJump
    local onPlayFinish = function(name) 
        Gac2Gas.PlayGameVideoDone(videoName)
    end

    AMPlayer.Inst:PlayWithCheckNew(videoName, nil, DelegateFactory.Action_string(onPlayFinish), nil,true,isNewPlayer,playerCount,0,not canSkip)
end

function Gas2Gac.SyncTeamGameVideoSkipMemberCount(videoName, playerCount, skipCount)
    if (AMPlayer.Inst.m_CurrentTake and videoName == AMPlayer.Inst.m_CurrentTake.name) or (AMPlayer.Inst.mCurrentTimelineName == videoName) then
        AMPlayer.Inst.m_SkipCount = skipCount
        AMPlayer.Inst.m_MemberCount = playerCount
        if skipCount == playerCount then
            local takename = AMPlayer.Inst:StopCurrentTake(false)
            local udid = SystemInfo.deviceUniqueIdentifier
            local playerId = 0
            if CClientMainPlayer.Inst then
                playerId = CClientMainPlayer.Inst.Id
            end

            Gac2Gas.RequestLogClientCutSceneInfo(udid, playerId, "User Jumped the CutScene:"..takename)
        end
    end
end

function Gas2Gac.WorldCupJingCai_SendSelfJingCaiData(jingCaiData_U)
    LuaWorldCup2022Mgr:SendSelfJingCaiData(jingCaiData_U)
end

function Gas2Gac.WorldCupJingCai_SendPeriodAndRoundData(allPeriodResultData_U, allRoundResultData_U)
    LuaWorldCup2022Mgr:SendPeriodAndRoundData(allPeriodResultData_U, allRoundResultData_U)
end

function Gas2Gac.WorldCupJingCai_SendPeriodRoundStageData(allPeriodResultData_U, allRoundResultData_U, taotaiStage)
    LuaWorldCup2022Mgr:SendPeriodRoundStageData(allPeriodResultData_U, allRoundResultData_U, taotaiStage)
end

function Gas2Gac.WorldCupJingCai_ReplyTaoTaiInfoResult(taoTaiInfo_U)
    LuaWorldCup2022Mgr:JingCaiReplyTaoTaiInfoResult(taoTaiInfo_U)
end

function Gas2Gac.SyncBingTianShiLianData(ret_U)
    LuaHanJia2023Mgr:SyncBingTianShiLianData(ret_U)
end

function Gas2Gac.ShowBingTianShiLianResult(bResult, curSelect, curDifficulty)
    LuaHanJia2023Mgr:ShowBingTianShiLianResult(bResult, curSelect, curDifficulty)
end

function Gas2Gac.SyncXuePoLiXianPrepareInfo(ret_U)
    LuaHanJia2023Mgr:SyncXuePoLiXianPrepareInfo(ret_U)
end

function Gas2Gac.SyncXuePoLiXianData(ret_U)
    LuaHanJia2023Mgr:SyncXuePoLiXianData(ret_U)
end

function Gas2Gac.XuePoLiXianStopPrepare()
    LuaHanJia2023Mgr:XuePoLiXianStopPrepare()
end

function Gas2Gac.ShowXuePoLiXianResult(bResult, curLevelId, costTime)
    LuaHanJia2023Mgr:ShowXuePoLiXianResult(bResult, curLevelId, costTime)
end

function Gas2Gac.ReplyXuePoLiXianBattleInfo(ret_U)
    LuaHanJia2023Mgr:ReplyXuePoLiXianBattleInfo(ret_U)
end

function Gas2Gac.SyncXianKeLaiPlayData(progress, vip1, vip2, resetNum, rewardInfo_U, addRate)
    LuaHanJia2023Mgr:SyncXianKeLaiPlayData(progress, vip1, vip2, resetNum, rewardInfo_U, addRate)
end

function Gas2Gac.SyncXianKeLaiTaskData(ret_U)
    LuaHanJia2023Mgr:SyncXianKeLaiTaskData(ret_U)
end

function Gas2Gac.UnLockXianKeLaiVipSuccess(vip)
    LuaHanJia2023Mgr:UnLockXianKeLaiVipSuccess(vip)
end

function Gas2Gac.ReceiveXianKeLaiRewardSuccess(level, index, resultTbl_U)
    LuaHanJia2023Mgr:ReceiveXianKeLaiRewardSuccess(level, index, resultTbl_U)
end

function Gas2Gac.XianKeLaiSyncPlayState(bIsOpen)
    --只有在isOpen的时候下发
    LuaHanJia2023Mgr.isXKLOpen = true
    g_ScriptEvent:BroadcastInLua("XianKeLaiSyncPlayState")
end

function Gas2Gac.SyncHanHuaMiBaoData(ret_U)
    LuaHanJia2023Mgr:SyncHanHuaMiBaoData(ret_U)
end

function Gas2Gac.QueryBiWuCrossMatchInfoResult(ret_U)
    LuaBiWuCrossMgr.ProcessBiWuCrossMatchInfoResult(ret_U)
end

function Gas2Gac.SetCrtSelectedHongBaoType(coverType)
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        local beforeId = MainPlayer.PlayProp.CrtSelectedHongBaoIdx
        --if beforeId == coverType then return end
        g_ScriptEvent:BroadcastInLua("SetCurHongBaoCover",beforeId,coverType)
        MainPlayer.PlayProp.CrtSelectedHongBaoIdx = coverType
    end
end

function Gas2Gac.QueryBiWuCrossTeamInfoResult(ret_U)
    LuaBiWuCrossMgr.ProcessBiWuCrossTeamInfoResult(ret_U)
end

function Gas2Gac.QueryBiWuCrossGroupTeamInfoResult(ret_U)
    LuaBiWuCrossMgr.ProcessBiWuCrossGroupTeamInfoResult(ret_U)
end

function Gas2Gac.ShowCrossBiWuSetHeroWnd(info_U, startTime, leftTime)
    LuaBiWuCrossMgr.ProcessOpenHeroSelectWnd(info_U, startTime, leftTime)
end

function Gas2Gac.UpdateProtectCampfireHp(engineId, hp, crtHpFull, hpFull, mp, crtMpFull, mpFull)
    LuaHalloween2022Mgr.UpdateProtectCampfireHp(engineId, hp, crtHpFull, hpFull, mp, crtMpFull, mpFull)
end

function Gas2Gac.SyncBiWuFireRegion(region_U, bStart)
	LuaBiWuCrossMgr.ProcessShowEffect(region_U, bStart)
end

function Gas2Gac.SyncShuiGuoPaiDuiXiGuaNum(xiGuaNum, needXiGuaNum, bAddXiGua)
    LuaShiTuMgr:SyncShuiGuoPaiDuiXiGuaNum(xiGuaNum, needXiGuaNum, bAddXiGua)
end

function Gas2Gac.RemoveBiWuFireRegion()
    LuaBiWuCrossMgr.ClearEfts(true)
end

function Gas2Gac.SyncGuessRiddleResult2023(riddleId, isCorrect)
    g_ScriptEvent:BroadcastInLua("SyncGuessRiddleResult2023", riddleId, isCorrect)
end

function Gas2Gac.RequestCCSpeakResult(bOpenOrClose, steamName, extraInfo, bForce)
    LuaCCChatMgr:RequestCCSpeakResult(bOpenOrClose, steamName, extraInfo, bForce)
end

function Gas2Gac.Spring2023TTSY_SyncPlayState(playState, startTime, endTime, RankTbl_U)
    LuaChunJie2023Mgr:Spring2023TTSY_SyncPlayState(playState, startTime, endTime, RankTbl_U)
end

function Gas2Gac.Spring2023TTSY_ShowRewardWnd(rank, score, rewards_U)
    LuaChunJie2023Mgr:Spring2023TTSY_ShowRewardWnd(rank, score, rewards_U)
end

function Gas2Gac.Spring2023TTSY_ShowSitDownBtn(engineId)
    LuaChunJie2023Mgr:Spring2023TTSY_ShowSitDownBtn(engineId)
end

function Gas2Gac.Spring2023TTSY_HideSitDownBtn(engineId)
    LuaChunJie2023Mgr:Spring2023TTSY_HideSitDownBtn(engineId)
end

function Gas2Gac.Spring2023SLTT_ShowRewardWnd(bSuccess, duration, rewards_U)
    LuaChunJie2023Mgr:Spring2023SLTT_ShowRewardWnd(bSuccess, duration, rewards_U)
end

function Gas2Gac.Spring2023SLTT_AddOrUpdateTrap(trapId, x, y, beginRadius, endRadius, beginTime, endTime, playerBuff, monsterBuff, delta)
    LuaChunJie2023Mgr:Spring2023SLTT_AddOrUpdateTrap(trapId, x, y, beginRadius, endRadius, beginTime, endTime, playerBuff, monsterBuff, delta)
end

function Gas2Gac.Spring2023SLTT_RmTrap(trapId)
    LuaChunJie2023Mgr:RemoveTrapByID(trapId)
end

function Gas2Gac.Spring2023ZJNH_QueryTasksResult(TaskInfo_U)
    LuaChunJie2023Mgr:Spring2023ZJNH_QueryTasksResult(TaskInfo_U)
end

function Gas2Gac.SyncXinYuanBiDaStatus(wishId, wishStatus)
    --print("SyncXinYuanBiDaStatus:", wishId, wishStatus)
    LuaYuanDan2023Mgr.UpdateXinYuanState(wishId, wishStatus)
end

function Gas2Gac.SyncXinYuanBiDaStatusAll(WishedInfo_U)
    local info = g_MessagePack.unpack(WishedInfo_U)
    if not info then
        return
    end
    LuaYuanDan2023Mgr.QueryXinYuanBiDaState_Result(info)
    -- for k, v in pairs(info) do
    --     print(k, v)
    -- end

end

function Gas2Gac.SyncYuanDanQiYuanProgress(fuqi, xiqi, caiqi)
    LuaYuanDan2023Mgr.YuanDanQiYuanPlayProgress(fuqi, xiqi, caiqi)
end

function Gas2Gac.SyncYuanDanQiYuanAwardTimes(awardTimes)
    --print("SyncYuanDanQiYuanAwardTimes:", awardTimes)
    LuaYuanDan2023Mgr.YuanDanQiYuanAwardTimesResult(awardTimes)
end

function Gas2Gac.YuanDanQiYuanPlayResult(bWin, award, times)
    --print("YuanDanQiYuanPlayResult=", bWin, award, times)
    LuaYuanDan2023Mgr.YuanDanQiYuanPlayResult(bWin, award, times)
end

function Gas2Gac.UpdateTurkeyMatchSnowmanHp(snowmanTeamId, curHp, curHpFull, maxDamagePlayerName)
    LuaChristmas2022Mgr:UpdateTurkeyMatchSnowmanHp(snowmanTeamId, curHp, curHpFull, maxDamagePlayerName)
end

function Gas2Gac.SyncTurkeyMatchPlayStatus(playStatus)
    LuaChristmas2022Mgr:SyncTurkeyMatchPlayStatus(playStatus)
end

function Gas2Gac.SyncTurkeyMatchSnowballInfo(SnowballInfoData_U)
    -- playerId, snowBallCurPushTime(ms) ...
    local info = g_MessagePack.unpack(SnowballInfoData_U)
    if not info then return end
    
    LuaChristmas2022Mgr:SyncTurkeyMatchSnowballInfo(info)
end

function Gas2Gac.SyncTurkeyMatchSuperLuckyStarInfo(SuperLuckyStarInfoData_U)
    -- starEngineId, pixelPosX, pixelPosY ...
    local info = g_MessagePack.unpack(SuperLuckyStarInfoData_U)
    if not info then return end

    LuaChristmas2022Mgr:SyncTurkeyMatchSuperLuckyStarInfo(info)
end

function Gas2Gac.SyncTurkeyMatchSuperLuckyStarKick(SuperLuckyStarEngineId)
    LuaChristmas2022Mgr:SyncTurkeyMatchSuperLuckyStarKick(SuperLuckyStarEngineId)
end

function Gas2Gac.SyncTurkeyMatchSnowballBroke(playerId1, playerId2)
    LuaChristmas2022Mgr:SyncTurkeyMatchSnowballBroke(playerId1, playerId2)
end

function Gas2Gac.SyncTurkeyMatchResult(isWin, score, rank, selfDamage, teamTotalDamage, isFirstDamager, killSnowmanTime)
    LuaChristmas2022Mgr:SyncTurkeyMatchResult(isWin, score, rank, selfDamage, teamTotalDamage, isFirstDamager, killSnowmanTime)
end

function Gas2Gac.StartPlayCountDownByPlayId(count, time, designPlayId)
    CLuaCenterCountdownWnd.m_CurGamePlay = designPlayId
    CCenterCountdownWnd.count = count
    CCenterCountdownWnd.InitStartTime()
    CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
end

function Gas2Gac.SyncChristmasKindnessPlayStage(playStage, leftTime)
    LuaChristmas2022Mgr:SyncChristmasKindnessPlayStage(playStage, leftTime)
end

function Gas2Gac.SyncChristmasKindnessGiftTimes(firstStageGiftTimes, secondStageGiftTimes, thirdStageGiftTimes)
    LuaChristmas2022Mgr:SyncChristmasKindnessGiftTimes(firstStageGiftTimes, secondStageGiftTimes, thirdStageGiftTimes)
end

function Gas2Gac.SyncChristmasKindnessStartUI()
    LuaChristmas2022Mgr:SyncChristmasKindnessStartUI()
end

function Gas2Gac.QueryCanStartChristmasKindnessGuildPlay_Result(isCanStart)
    LuaChristmas2022Mgr:QueryCanStartChristmasKindnessGuildPlay_Result(isCanStart)
end

function Gas2Gac.QueryPlayerTurkeyMatchInterfaceData_Result(dailyAwardTimes, firstKillAwardTimes, firstTeamInfos_U)
    -- playerId, name, class, gender ...
    local firstTeamInfos = g_MessagePack.unpack(firstTeamInfos_U)
    LuaChristmas2022Mgr:QueryPlayerTurkeyMatchInterfaceData_Result(dailyAwardTimes, firstKillAwardTimes, firstTeamInfos)
end

function Gas2Gac.QueryTurkeyMatchRankData_Result(rankPos, firstStageScore, killSnowmanTime, rankData_U)
    -- rankPos, playerId, firstStageScore, killSnowmanTime, name ...
    local rankData = g_MessagePack.unpack(rankData_U)
    if not rankData then return end
    LuaChristmas2022Mgr:QueryTurkeyMatchRankData_Result(rankPos, firstStageScore, killSnowmanTime, rankData)
end
function Gas2Gac.GaoChangJiDouCrossQueryApplyInfoResult(status)
    LuaGaoChangJiDouMgr:GaoChangJiDouCrossQueryApplyInfoResult(status)
end

function Gas2Gac.GaoChangJiDouCrossInviteToApply()
    LuaGaoChangJiDouMgr:GaoChangJiDouCrossInviteToApply()
end

function Gas2Gac.GaoChangJiDouCrossApplySuccess()
    LuaGaoChangJiDouMgr:GaoChangJiDouCrossApplySuccess()
end

function Gas2Gac.SyncGaoChangJiDouMonsterState(engineId, x, y, isAlive, isBig)
    LuaGaoChangJiDouMgr:SyncGaoChangJiDouMonsterState(engineId, x, y, isAlive, isBig)
end

function Gas2Gac.SyncGaoChangJiDouPickState(engineId, x, y, isAlive, isBig)
    LuaGaoChangJiDouMgr:SyncGaoChangJiDouPickState(engineId, x, y, isAlive, isBig)
end

function Gas2Gac.GaoChangJiDouScoreUpdate(playerId, score)
    LuaGaoChangJiDouMgr:GaoChangJiDouScoreUpdate(playerId, score)
end

function Gas2Gac.GaoChangJiDouStatisticStart()
    --print("GaoChangJiDouStatisticStart")
end

function Gas2Gac.GaoChangJiDouStatisticStop()
    --print("GaoChangJiDouStatisticStop")
end

function Gas2Gac.GaoChangJiDouSelfUpdate(playerId, kill, killed, help, monsterkill, baoxiang, score)
    LuaGaoChangJiDouMgr:GaoChangJiDouSelfUpdate(playerId, kill, killed, help, monsterkill, baoxiang, score)
end

function Gas2Gac.SyncGaoChangJiDouCrossPlayInfo(rankInfo_U, monsterNum, pickNum)
    LuaGaoChangJiDouMgr:SyncGaoChangJiDouCrossPlayInfo(rankInfo_U, monsterNum, pickNum)
end

function Gas2Gac.GaoChangJiDouCrossCancelApplySuccess()
    LuaGaoChangJiDouMgr:GaoChangJiDouCrossCancelApplySuccess()
end

function Gas2Gac.GaoChangCancelApplySuccess()
    LuaGaoChangJiDouMgr:GaoChangCancelApplySuccess()
end

function Gas2Gac.SyncFrameTimeToClient(curFrameTime, lastFrameTime)
    -- print("SyncFrameTimeToClient", curFrameTime, lastFrameTime)
    CCooldownMgr.Inst:SyncFrameTimeToClient(curFrameTime, lastFrameTime)
end

function Gas2Gac.UnlockHongBaoCover(coverType)
    CLuaHongBaoMgr.UnlockHongBaoCover(coverType)
end

function Gas2Gac.SendValentineYWQSInfo(partnerId, partnerIsOnline, loveValue, rank, todayHuoLi, expressionTaskCount, dateTaskCount, shareMengDaoCount, playerAppearanceInfo)
    LuaValentine2023Mgr:SendValentineYWQSInfo(partnerId, partnerIsOnline, loveValue, rank, todayHuoLi, expressionTaskCount, dateTaskCount, shareMengDaoCount, playerAppearanceInfo)
end

function Gas2Gac.SendValentineYWQSRankInfo(partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank, rankInfo)
    LuaValentine2023Mgr:SendValentineYWQSRankInfo(partnerId, partnerName, partnerClass, partnerGender, loveValue, voteCount, myRank, rankInfo)
end

function Gas2Gac.SendValentineYWQSInvitation(partnerPlayerId, partnerPlayerName)
    LuaValentine2023Mgr:SendValentineYWQSInvitation(partnerPlayerId, partnerPlayerName)
end

function Gas2Gac.SyncValentineYWQSTop10(rankInfo)
    LuaValentine2023Mgr:SyncValentineYWQSTop10(rankInfo)
end

function Gas2Gac.SendValentineLGTMPlayInfo(remainRewardTimes, remainHardModeRewardTimes)
    LuaValentine2023Mgr:SendValentineLGTMPlayInfo(remainRewardTimes, remainHardModeRewardTimes)
end

function Gas2Gac.StartLGTMFlyProgress(rangeStart, rangeWidth)
    LuaValentine2023Mgr:StartLGTMFlyProgress(rangeStart, rangeWidth)
end

function Gas2Gac.PlayerDoLGTMFlySkill(doSkillPos, isSucc)
    LuaValentine2023Mgr:PlayerDoLGTMFlySkill(doSkillPos, isSucc)
end

function Gas2Gac.SendLGTMPlayResult(isWin, isHardMode)
    LuaValentine2023Mgr:SendLGTMPlayResult(isWin, isHardMode)
end

function Gas2Gac.PDFY_RequestShuffleBuffResult(shufflePoint, shuffleTimes, shuffleType, buffLevel, tempBuff)
    LuaPengDaoMgr:PDFY_RequestShuffleBuffResult(shufflePoint, shuffleTimes, shuffleType, buffLevel, tempBuff)
end

function Gas2Gac.PDFY_RequestReplaceAllBuffResult(newBuff, shuffleType)
    LuaPengDaoMgr:PDFY_RequestReplaceAllBuffResult(newBuff, shuffleType)
end

function Gas2Gac.PDFY_RequestSkillLevelUpSuccess(id, skillLevel, skillPoint, group, hasGroupPoint)
    LuaPengDaoMgr:PDFY_RequestSkillLevelUpSuccess(id, skillLevel, skillPoint, group, hasGroupPoint)
end

function Gas2Gac.PDFY_RequestEquipSkillSuccess(id)
    LuaPengDaoMgr:PDFY_RequestEquipSkillSuccess(id)
end

function Gas2Gac.PDFY_RequestResetSkillResult(score, skillPoint)
    LuaPengDaoMgr:PDFY_RequestResetSkillResult(score, skillPoint)
end

function Gas2Gac.PDFY_SendMainWndData(lastSeasonId, thisSeasonId, rewardData, rankData, selfData)
    LuaPengDaoMgr:PDFY_SendMainWndData(lastSeasonId, thisSeasonId, rewardData, rankData, selfData)
end

function Gas2Gac.PDFY_ShowNextLevelBlessing(candidateBlessings, selectedBlessings, extraSkills )
    LuaPengDaoMgr:PDFY_ShowNextLevelBlessing(candidateBlessings, selectedBlessings, extraSkills )
end

function Gas2Gac.PDFY_UpdateKilledMonsterNum(updatedMonsterNum)
    LuaPengDaoMgr:PDFY_UpdateKilledMonsterNum(updatedMonsterNum)
end

function Gas2Gac.PDFY_SyncPlayResult(bSuccess, difficulty1, difficulty2, difficulty3, passedLevel, usedTime, score, breakRecord, rewardScore)
    LuaPengDaoMgr:PDFY_SyncPlayResult(bSuccess, difficulty1, difficulty2, difficulty3, passedLevel, usedTime, score, breakRecord, rewardScore)
end

function Gas2Gac.PDFY_GetMonsterRewardSuccess(monsterId, rewardId)
    LuaPengDaoMgr:PDFY_GetMonsterRewardSuccess(monsterId, rewardId)
end

function Gas2Gac.PDFY_UpdataMaxDifficulty(difficulty)
    LuaPengDaoMgr:PDFY_UpdataMaxDifficulty(difficulty)
end

function Gas2Gac.PDFY_QueryTempSkillResult(extraSkill_U)
    LuaPengDaoMgr:PDFY_QueryTempSkillResult(extraSkill_U)
end

function Gas2Gac.PDFY_SyncShufflePoint(value)
    LuaPengDaoMgr:PDFY_SyncShufflePoint(value)
end

function Gas2Gac.PDFY_SyncSkillPoint(value)
    LuaPengDaoMgr:PDFY_SyncSkillPoint(value)
end

function Gas2Gac.YuanDanQiYuanOnMonsterKilled(yuanqiType, deltaProgress)
    --print(string.format("YuanQi%d+%d", yuanqiType, deltaProgress))
    LuaYuanDan2023Mgr.YuanDanQiYuanOnPlayerAddScore(yuanqiType,deltaProgress)
end

function Gas2Gac.NaoYuanXiaoPlayResult(teamScore, todayTotalScore, playerScoreInfo_U, remainNumTbl_U, totalRewardNumTbl_U, rewardItems_U)
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayResult(teamScore, todayTotalScore, playerScoreInfo_U, remainNumTbl_U, totalRewardNumTbl_U, rewardItems_U)
end

function Gas2Gac.SyncNaoYuanXiaoTeamPlayInfo(teamScore, playerScoreInfo_U)
    LuaYuanXiaoDefenseMgr:SyncNaoYuanXiaoTeamPlayInfo(teamScore, playerScoreInfo_U)
end

function Gas2Gac.SyncNaoYuanXiaoGridStatus(gridInfo_U, gridTextureInfo_U)
    LuaYuanXiaoDefenseMgr:SyncNaoYuanXiaoGridStatus(gridInfo_U, gridTextureInfo_U)
end

function Gas2Gac.NaoYuanXiaoGridStatus(gridDropStatus_U)
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoGridStatus(gridDropStatus_U)
end

function Gas2Gac.NaoYuanXiaoPlayerCollisionOthers(playerId, dir)
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayerCollisionOthers(playerId, dir)
end

function Gas2Gac.NaoYuanXiaoPlayerOnBadGrid(playerId)
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayerOnBadGrid(playerId)
end

function Gas2Gac.ReturnNaoYuanXiaoScoreQuery(todayTotalScore)
    LuaYuanXiaoDefenseMgr:ReturnNaoYuanXiaoScoreQuery(todayTotalScore)
end

function Gas2Gac.ReturnNaoYuanXiaoRemainRewardTimes(todayRemainRewardTimes_U)
    LuaYuanXiaoDefenseMgr:ReturnNaoYuanXiaoRemainRewardTimes(todayRemainRewardTimes_U)
end

function Gas2Gac.QuerySpriteGameYWTokenResult(token, expiredTime, context, clientUrl)
    -- 之前是推荐词用的好像，现在热点那也复用这个
    LuaJingLingMgr:QuerySpriteGameYWTokenResult(token, expiredTime, context, clientUrl)
end

function Gas2Gac.QueryGuildLeagueCrossAidEvaluteInfoResult(evaluteInfo_U)
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossAidEvaluteInfoResult(evaluteInfo_U)
end

function Gas2Gac.QHLanternShowStart(partyId, endTime)
    LuaYuanXiao2023Mgr:QHLanternShowStart(partyId, endTime)
end

function Gas2Gac.QHLanternShowEnd(partyId)
    LuaYuanXiao2023Mgr:QHLanternShowEnd(partyId)
end

function Gas2Gac.QHLanternShowZhiBo_SyncChannelInfo(id, zhiboInfo)
    LuaYuanXiao2023Mgr:QHLanternShowZhiBo_SyncChannelInfo(id, zhiboInfo)
end

function Gas2Gac.QHLanternShowZhiBo_ZhiBoEnd()
    LuaYuanXiao2023Mgr:QHLanternShowZhiBo_ZhiBoEnd()
end

function Gas2Gac.NaoYuanXiaoPlayerOnGoodGrid(x, y)
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayerOnGoodGrid(x, y)
end

function Gas2Gac.NaoYuanXiaoPlayersGridPos(playersGrid_U)
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayersGridPos(playersGrid_U)
end

function Gas2Gac.QHLanternShowChangeView(partyId)
    LuaYuanXiao2023Mgr:QHLanternShowChangeView(partyId)
end

function Gas2Gac.NaoYuanXiaoReturnStartPos(playerId)
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoReturnStartPos(playerId)
end

function Gas2Gac.ShowDouHunCrossChampionWnd(nthMatch, level, teamName, leaderName, winTeamId, time)
    LuaFightingSpiritGenerateChampionWnd.s_NthMatch = nthMatch
    LuaFightingSpiritGenerateChampionWnd.s_Level = level
    LuaFightingSpiritGenerateChampionWnd.s_TeamName = teamName
    LuaFightingSpiritGenerateChampionWnd.s_LeaderName = leaderName
    LuaFightingSpiritGenerateChampionWnd.s_WinTeamId = winTeamId
    LuaFightingSpiritGenerateChampionWnd.s_Time = time
    CUIManager.ShowUI(CLuaUIResources.FightingSpiritGenerateChampionWnd)
end

function Gas2Gac.NaoYuanXiaoCommitFx()
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoCommitFx()
end

function Gas2Gac.NaoYuanXiaoShowUI()
    LuaYuanXiaoDefenseMgr:NaoYuanXiaoShowUI()
end

function Gas2Gac.PlayerPropCompositionReturn(propData_U)
    LuaPlayerPropertyMgr:OnPlayerPropCompositionReturn(propData_U)
end

function Gas2Gac.PlayerQueryPropRankDataResult(rankData_U)
    LuaPlayerPropertyMgr:OnPlayerQueryPropRankDataResult(rankData_U)
end

function Gas2Gac.PlayerQueryCrossPropRankDataResult(rankData_U)
    -- local data = g_MessagePack.unpack(rankData_U)
    -- for k,v in pairs(data) do
    --     print(k,v)
    -- end
    LuaPlayerPropertyMgr:OnPlayerQueryPropRankCrossDataResult(rankData_U)
end

function Gas2Gac.PlayerQueryPropShareResult(isShared, fromId)
    print(isShared, fromId)
    LuaPlayerPropertyMgr:OnPlayerQueryPropShareResult(isShared, fromId)
end

function Gas2Gac.PropGuideIsOpenCrossServer(isOpenCross)
    LuaPlayerPropertyMgr:OnPropGuideIsOpenCrossServer(isOpenCross)
end

function Gas2Gac.CompareOnlinePropResult(selfPropData_U, targetPropData_U)
    LuaPlayerPropertyMgr:OnCompareOnlinePropResult(selfPropData_U, targetPropData_U)
end

function Gas2Gac.CompareRankPropResult(selfPropData_U, targetPropData_U)
    LuaPlayerPropertyMgr:OnCompareRankPropResult(selfPropData_U, targetPropData_U)
end

function Gas2Gac.GaoChangJiDouOnPlayerPick(playerId, pickEngineId)
    LuaGaoChangJiDouMgr:GaoChangJiDouOnPlayerPick(playerId, pickEngineId)
end

function Gas2Gac.QueryMonsterPosResultNew(ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    local monsterInfoData = {}
    for i = 1, #unpackData, 6 do
        table.insert(monsterInfoData, {
            templateId = unpackData[i], 
            hp = unpackData[i+1], 
            hpFull = unpackData[i+2],
            posX = unpackData[i+3], 
            posY = unpackData[i+4], 
            force = unpackData[i+5]})
    end
    g_ScriptEvent:BroadcastInLua("QueryMonsterPosResultNew", monsterInfoData)
end

function Gas2Gac.PlayerQueryExtraSkillLvResult(extraSkillLv_U)
    LuaPlayerPropertyMgr:OnQuerySkillDetailResult(extraSkillLv_U)
end

function Gas2Gac.GaoChangJiDouQueryAwardResult(award_s)
    LuaGaoChangJiDouMgr:GaoChangJiDouQueryAwardResult(award_s)
end

function Gas2Gac.QueryOnlinePlayerSharedPropResult(targetId, propType_U)
    LuaPlayerPropertyMgr:OnQueryOnlinePlayerSharedPropResult(targetId, propType_U)
end

function Gas2Gac.RequestCanRecreateExtraEquipReturn(equipPlace, equipPos, equipId, canRecreate)
    print(equipPlace, equipPos, equipId, canRecreate)
end

function Gas2Gac.RecreateExtraEquipResult(equipId, isSuccess, changedTbl)
    local tbl = g_MessagePack.unpack(changedTbl)
    g_ScriptEvent:BroadcastInLua("RecreateExtraEquipResult", equipId, isSuccess, tbl)
end

function Gas2Gac.TryConfirmRecreateExtraEquipResult(equipId, fightpropUD, tempGrade)
    local fightProp = CreateFromClass(CPlayerPropertyFight)
    fightProp:LoadFromString(fightpropUD)

    local item = CItemMgr.Inst:GetById(equipId)
    local equip = item and item.Equip or nil

    if equip then
        equip.TempScore = CLuaEquipMgr.m_RecreateTempScore
        CEquipmentBaptizeMgr.Inst:CompareWordDif(fightProp, equipId)
        equip.TempScore = 0
    end
end

function Gas2Gac.QueryRecreateExtraEquipCostReturn(equipPlace, equipPos, equipId, costYinLiang, costItemId, costItemCount)
    print(equipPlace, equipPos, equipId, costYinLiang, costItemId, costItemCount)
end

function Gas2Gac.RecreateExtraEquipResultConfirmResult()
    print("RecreateExtraEquipResultConfirmResult")
    g_ScriptEvent:BroadcastInLua("RecreateExtraEquipResultConfirmResult")
end

function Gas2Gac.ExtraEquipForgeSuitUpdate(suitId)
    CClientMainPlayer.Inst.ItemProp.ExtraEquipForgeSuitId = suitId
    g_ScriptEvent:BroadcastInLua("ExtraEquipForgeSuitUpdate", CClientMainPlayer.Inst)
end

function Gas2Gac.GmChangeWuLianRecreateStatus(isOpen)
    print("GmChangeWuLianRecreateStatus", isOpen)
    if isOpen then
        CEquipBaptizeDetailView.s_EnableWuLianRecreate = true
    else
        CEquipBaptizeDetailView.s_EnableWuLianRecreate = false
    end
end

function Gas2Gac.UpdateEquipPlayerClass(equipId, equipPlayerClass)
    print("UpdateEquipPlayerClass", equipId, equipPlayerClass)
end

function Gas2Gac.GaoChangJiDouNoTeamMember(engineId)
    LuaGaoChangJiDouMgr:MaskPlayer(engineId, true)
end

function Gas2Gac.GaoChangJiDouMengMianPlayers(otherEngineIds, isFirstEnter)
    LuaGaoChangJiDouMgr:GaoChangJiDouMengMianPlayers(g_MessagePack.unpack(otherEngineIds), isFirstEnter)
end

function Gas2Gac.QingMing2023SyncLevelList(currentLevelIndex, currentLevelId, levelListU, bShowRule)
    LuaQingMing2023Mgr:QingMing2023SyncLevelList(currentLevelIndex, currentLevelId, levelListU, bShowRule)
end

function Gas2Gac.QingMing2023SyncPlayRankInfo(playerRankListU)
    LuaQingMing2023Mgr:QingMing2023SyncPlayRankInfo(playerRankListU)
end

function Gas2Gac.QingMing2023PVPSendFinalResult(rank, score, engageAward, winnerAward, playerDetailsU)
    LuaQingMing2023Mgr:QingMing2023PVPSendFinalResult(rank, score, engageAward, winnerAward, playerDetailsU)
end

function Gas2Gac.QingMing2023SyncSubPlayInfo(subplayId, playerSubPlayInfoU)
    LuaQingMing2023Mgr:QingMing2023SyncSubPlayInfo(subplayId, playerSubPlayInfoU)
end

function Gas2Gac.QingMing2023BurningGroundSyncFireBlocks(isReal, blocksDataU)
end

function Gas2Gac.QingMing2023PlayerArriveLevel(level, arrivalOrder)  -- 已废弃
end

function Gas2Gac.QingMing2023TrapTriggered(x, y)
end

function Gas2Gac.QingMing2023MoveToStartingPoint()
end

function Gas2Gac.QingMing2023SyncCommonData(subplayId, dataType, dataU)
end

function Gas2Gac.SendQueryEquipInfoResult_Online(itemId, itemInfo_U)
    --print("queryequipinfo", itemId, itemInfo_U)
    LuaPlayerPropertyMgr:OnGetPlayerEquipmentInfo(itemId,itemInfo_U)
end

function Gas2Gac.Anniv2023FSC_SyncPlayResult(score, result, bNewRecord, extraData_U)
    LuaFourSeasonCardMgr:SyncPlayResult(score, result, bNewRecord, extraData_U)
end

function Gas2Gac.Anniv2023FSC_QueryRankDataResult(rankData, selfData, npcData)
    LuaFourSeasonCardMgr:QueryRankDataResult(rankData, selfData, npcData)
end

function Gas2Gac.Anniv2023FSC_SyncNearByPlayers(inviteList_U, lastInviteExpiredTime)
    LuaFourSeasonCardMgr:SyncNearByPlayers(inviteList_U, lastInviteExpiredTime)
end

function Gas2Gac.Anniv2023FSC_InviteSuccess(currentInviteExpiredTime)
    LuaFourSeasonCardMgr:InviteSuccess(currentInviteExpiredTime)
end

function Gas2Gac.Anniv2023FSC_SendInvitation(inviterId, inviterName, currentInviteExpiredTime)
    LuaFourSeasonCardMgr:ReceiveInvitation(inviterId, inviterName, currentInviteExpiredTime)
end

function Gas2Gac.Anniv2023FSC_OnInvitationRejected(playerId, playerName, rejectExpiredTime)
    LuaFourSeasonCardMgr:OnInvitationRejected(playerId, playerName, rejectExpiredTime)
end

function Gas2Gac.Anniv2023FSC_SyncPlayerStatus(index, status)
    LuaFourSeasonCardMgr:SyncPlayerStatus(index, status)
end

function Gas2Gac.Anniv2023FSC_OnSelfUseCard(usedCardId, boardCardId, handCards_U, boardCards_U, lastCollectedCard)
    LuaFourSeasonCardMgr:OnSelfUseCard(usedCardId, boardCardId, handCards_U, boardCards_U, lastCollectedCard)
end

function Gas2Gac.Anniv2023FSC_OnOpponentUseCard(usedCardId, boardCardId, opponentCardNum, boardCards_U, lastCollectedCard)
    LuaFourSeasonCardMgr:OnOpponentUseCard(usedCardId, boardCardId, opponentCardNum, boardCards_U, lastCollectedCard)
end

function Gas2Gac.Anniv2023FSC_OnSelfReplaceCard(giveUpCardId, newHandCardId, handCards_U, boardCards_U)
    LuaFourSeasonCardMgr:OnSelfReplaceCard(giveUpCardId, newHandCardId, handCards_U, boardCards_U)
end

function Gas2Gac.Anniv2023FSC_OnOpponentReplaceCard(giveUpCardIdx, opponentCardNum, boardCards_U)
    LuaFourSeasonCardMgr:OnOpponentReplaceCard(giveUpCardIdx, opponentCardNum, boardCards_U)
end

function Gas2Gac.Anniv2023FSC_OnAddBoardCard(cardId, boardCards_U)
    LuaFourSeasonCardMgr:OnAddBoardCard(cardId, boardCards_U)
end

function Gas2Gac.Anniv2023FSC_OnReShuffleBoardCard(boardCards_U)
    LuaFourSeasonCardMgr:OnReShuffleBoardCard(boardCards_U)
end

function Gas2Gac.Anniv2023FSC_StartRound(bIsOnMove, countDownEndTime)
    LuaFourSeasonCardMgr:StartRound(bIsOnMove, countDownEndTime)
end

function Gas2Gac.Anniv2023FSC_OnDealCards(opponentCardNum, handCards_U, boardCards_U)
    LuaFourSeasonCardMgr:OnDealCards(opponentCardNum, handCards_U, boardCards_U)
end

function Gas2Gac.Anniv2023FSC_UpdateScore(selfScore, opponentScore)
    LuaFourSeasonCardMgr:UpdateScore(selfScore, opponentScore)
end

function Gas2Gac.Anniv2023FSC_StartAddNewCombo(cardsUD, comboId, score, countDownEndTime)
    LuaFourSeasonCardMgr:StartAddNewCombo(cardsUD, comboId, score, countDownEndTime)
end

function Gas2Gac.Anniv2023FSC_StartSpCardEffect(cardId, effectId, countDownEndTime)
    LuaFourSeasonCardMgr:StartSpCardEffect(cardId, effectId, countDownEndTime)
end

function Gas2Gac.Anniv2023FSC_SpCardEffectEnd(index, cardId, effectId, extreData_U)
    LuaFourSeasonCardMgr:SpCardEffectEnd(index, cardId, effectId, extreData_U)
end

function Gas2Gac.Anniv2023FSC_OpponentStartSkill(cardId, effectId, countDownEndTime)
    LuaFourSeasonCardMgr:OpponentStartSkill(cardId, effectId, countDownEndTime)
end

function Gas2Gac.Anniv2023FSC_StartReplicate(cardId, effectId, card2Effects_U, countDownEndTime)
    LuaFourSeasonCardMgr:StartReplicate(cardId, effectId, card2Effects_U, countDownEndTime)
end

function Gas2Gac.Anniv2023FSC_StartInterrupt(cardId, effectId, card2Effects_U, countDownEndTime)
    LuaFourSeasonCardMgr:StartInterrupt(cardId, effectId, card2Effects_U, countDownEndTime)
end

function Gas2Gac.Anniv2023FSC_SyncPlayerInfo(selfInfo, opponentInfo)
    LuaFourSeasonCardMgr:SyncPlayerInfo(selfInfo, opponentInfo)
end

function Gas2Gac.Anniv2023FSC_SyncPrepareInfo(specialCards_U, selfStatus, opponentStatus, countDownEndTime)
    LuaFourSeasonCardMgr:SyncPrepareInfo(specialCards_U, selfStatus, opponentStatus, countDownEndTime)
end

function Gas2Gac.Anniv2023FSC_SyncCardInfo(boardCards, handCards, selfSpecialCards, selfLastCard, opponentCardNum, opponentShowedCards, opponentLastCard, selfCollectedCards, opponentCollectedCards)
    LuaFourSeasonCardMgr:SyncCardInfo(boardCards, handCards, selfSpecialCards, selfLastCard, opponentCardNum, opponentShowedCards, opponentLastCard, selfCollectedCards, opponentCollectedCards)
end

function Gas2Gac.Anniv2023FSC_SyncGuidingStage(guidingStage)
    LuaFourSeasonCardMgr:SyncGuidingStage(guidingStage)
end

function Gas2Gac.Anniv2023FSC_CloseGameWnd()
    LuaFourSeasonCardMgr:CloseGameWnd()
end

function Gas2Gac.Anniv2023FSC_SendCollectedCardsData(queryIndex, collectedCards, collectedCombos)
    LuaFourSeasonCardMgr:SendCollectedCardsData(queryIndex, collectedCards, collectedCombos)
end

function Gas2Gac.Anniv2023FSC_QueryPlayWndDataResult(playData, rankData, selfData, npcData)
    LuaFourSeasonCardMgr:QueryPlayWndDataResult(playData, rankData, selfData, npcData)
end

function Gas2Gac.Anniv2023_QueryFestivalWndDataResult(specialCardNum)
    LuaFourSeasonCardMgr:QueryFestivalWndDataResult(specialCardNum)
end

function Gas2Gac.Anniv2023FSC_SendSpecialCardsData(specialCards)
    LuaFourSeasonCardMgr:SendSpecialCardsData(specialCards)
end

function Gas2Gac.SyncHuiCaiShanData(ret_U)
    LuaYiRenSiMgr:SyncHuiCaiShanData(ret_U)
end

function Gas2Gac.SyncYiRenSiFindCluesData(ret_U)
    LuaYiRenSiMgr:SyncYiRenSiFindCluesData(ret_U)
end

function Gas2Gac.YiRenSiFindCluesAndOpenWnd(cluesId, ret_U)
    LuaYiRenSiMgr:OpenYiRenSiClue(cluesId, ret_U)
end

function Gas2Gac.HuiCaiShanShowPic(ret_U, duration)
    LuaYiRenSiMgr:OpenMakeFanWithData(ret_U, duration)
end

function Gas2Gac.OpenDaFuWengSignUpWnd(bSignUp)
    LuaDaFuWongMgr:OnMatchStatusChange(bSignUp)
    --CUIManager.ShowUI(CLuaUIResources.DaFuWongEnterWnd)
end

function Gas2Gac.ReceiveDaFuWengRewardSuccess(level, index, resultTbl)
    LuaDaFuWongMgr:ReceiveDaFuWengRewardSuccess(level, index, resultTbl)
end

function Gas2Gac.UnLockDaFuWengVipSuccess(vip)
    LuaDaFuWongMgr:UnLockDaFuWengVipSuccess(vip)
end

function Gas2Gac.SyncDaFuWengPlayData(ret_U)
    LuaDaFuWongMgr:OnTongXingZhengDataResult(ret_U)
end

function Gas2Gac.SyncDaFuWengSignUpResult(bSignUp)
    LuaDaFuWongMgr:OnMatchStatusChange(bSignUp)
end

function Gas2Gac.DaFuWengSyncGameInfo(ret_U)
    LuaDaFuWongMgr:DaFuWengSyncGameInfo(ret_U)
end

function Gas2Gac.DaFuWengShuYuanAnswerResult(questionId, answer, result, cardRewardInfo_U)
    LuaDaFuWongMgr:DaFuWengShuYuanAnswerResult(questionId, answer, result, cardRewardInfo_U)
end

function Gas2Gac.DaFuWengYaoQianResult()
    LuaDaFuWongMgr:OnPlayerOpreateEvent(2)
end

function Gas2Gac.DaFuWengRandomEventOpenTreasure()
    LuaDaFuWongMgr:OnPlayerOpreateEvent(1)
end

function Gas2Gac.DaFuWengRandomEventBeggarSelect(select, cardRewardInfo_U)
    LuaDaFuWongMgr:OnDaFuWengRandomEventBeggarSelect(select,cardRewardInfo_U)
end

function Gas2Gac.DaFuWengSyncGameResult(result, rank, wonderfulRecord_U, report_U, reward_U)
    LuaDaFuWongMgr:OnDaFuWengGameResult(result, rank, wonderfulRecord_U, report_U, reward_U)
end

function Gas2Gac.DaFuWengSyncMoney(playerId, curMoney, changeValue, totalMoney)
    LuaDaFuWongMgr:ShowWavewards(playerId, curMoney, changeValue, totalMoney)
end

function Gas2Gac.DaFuWengShowMessage(playerId, stage, result, extraInfo_U)
    LuaDaFuWongMgr:needShowEventBroadCast(playerId,stage,result,extraInfo_U)
end

function Gas2Gac.SendGuildJuDianChallengeMemberInfo(onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo)
    LuaJuDianBattleMgr:SendGuildJuDianChallengeMemberInfo(onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo)
end

function Gas2Gac.LiYuZhangDaiFoodCookingResult(cookbookId, isSucc)
    LuaYiRenSiMgr:LiYuZhangDaiFoodCookingResult(cookbookId, isSucc)
end

function Gas2Gac.SendLiYuZhangDaiFoodPositons(positonsData)
    g_ScriptEvent:BroadcastInLua("NanDuFanHua_FindFood_DrawArrowEffectList", positonsData)
end

function Gas2Gac.FindLiYuZhangDaiFoodFar(x, y)
    g_ScriptEvent:BroadcastInLua("NanDuFanHua_FindFood_DrawArrowEffect", x, y)
end

function Gas2Gac.SendQingMing2023PveRank(myRank, finishTime, rankData)
    LuaQingMing2023Mgr:SendQingMing2023PveRank(myRank, finishTime, rankData)
end

function Gas2Gac.QingMing2023PvePlaySuccess(bWin, mode, finishTime, myRank, minTime, award)
    LuaQingMing2023Mgr:QingMing2023PvePlaySuccess(bWin, mode, finishTime, myRank, minTime, award)
end

function Gas2Gac.SendSetGuildJuDianChallengeEliteResult(targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
    LuaJuDianBattleMgr:SendSetGuildJuDianChallengeEliteResult(targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
end

function Gas2Gac.MonthCardInfoChange(reason)
    g_ScriptEvent:BroadcastInLua("MonthCardInfoChange", reason)
end

function Gas2Gac.SendQingMing2023PveInfo(isOpen, isTeamOk, isLevelOk, award, myRank, finishTime)
    g_ScriptEvent:BroadcastInLua("SendQingMing2023PveInfo", isOpen, isTeamOk, isLevelOk, award, myRank, finishTime)
end

function Gas2Gac.Anniv2023ZaiZai_PlaySuccess(playId, bHasReward)
    LuaZhouNianQingLookForKidResultWnd.s_HasReward = bHasReward
    LuaZhouNianQingLookForKidResultWnd.s_GamePlayId = playId
    CUIManager.ShowUI(CLuaUIResources.ZhouNianQingLookForKidResultWnd)
end

function Gas2Gac.Anniv2023ZaiZai_MakeTabenSuccess(count)
    LuaZhouNianQing2023Mgr:MakeTabenSuccess(count)
end

function Gas2Gac.SyncEndlessChallengeResult(bossNumber, isGetBaseDailyReward, isGetAdvancedDailyReward)
    LuaDuanWu2023Mgr:SyncEndlessChallengeResult(bossNumber, isGetBaseDailyReward, isGetAdvancedDailyReward)
end

function Gas2Gac.SyncDuanWu2023PVPVEScore(teamId, teamOneScore, teamTwoScore, scoreInfo_U)
    LuaDuanWu2023Mgr:SyncDuanWu2023PVPVEScore(teamId, teamOneScore, teamTwoScore, scoreInfo_U)
end

function Gas2Gac.SyncDuanWu2023PVPVECompareResult(winnerId, winnerScore, loserId, loserScore, changeScore)
    LuaDuanWu2023Mgr:SyncDuanWu2023PVPVECompareResult(winnerId, winnerScore, loserId, loserScore, changeScore)
end

-- scoreStatus : 0 不变, 1 领先, 2 被领先
function Gas2Gac.SyncDuanWu2023PVPVESelfTeamScoreChange(teamScore, playerId, playerScore, playerContribution, scoreStatus)
    LuaDuanWu2023Mgr:SyncDuanWu2023PVPVESelfTeamScoreChange(teamScore, playerId, playerScore, playerContribution, scoreStatus)
end

function Gas2Gac.SyncDuanWu2023PVPVEOtherTeamScoreChange(teamScore, scoreStatus)
    LuaDuanWu2023Mgr:SyncDuanWu2023PVPVEOtherTeamScoreChange(teamScore, scoreStatus)
end

function Gas2Gac.SyncDuanWu2023PVPVEPlayResult(getScore, dailyScore, winTeamId, teamOneScore, teamTwoScore, teamOneInfo_U, teamTwoInfo_U)
    LuaDuanWu2023Mgr:SyncDuanWu2023PVPVEPlayResult(getScore, dailyScore, winTeamId, teamOneScore, teamTwoScore, teamOneInfo_U, teamTwoInfo_U)
end

function Gas2Gac.SendZhanLingAlertInfo(p1, p2, p3, vip1, vip2, bAlert)
    LuaZhouNianQing2023Mgr:SendZhanLingAlertInfo(p1, p2, p3, vip1, vip2, bAlert)
end

function Gas2Gac.SyncZhanLingPlayData(p1, p2, p3, vip1, vip2, receiveDataUD)
    LuaZhouNianQing2023Mgr:SyncZhanLingPlayData(p1, p2, p3, vip1, vip2, receiveDataUD)
end

function Gas2Gac.UnLockZhanLingVipSuccess(vip)
    LuaZhouNianQing2023Mgr:UnLockZhanLingVipSuccess(vip)
end

function Gas2Gac.SendZhanLingTaskData(taskDataUD)
    LuaZhouNianQing2023Mgr:SendZhanLingTaskData(taskDataUD)
end

function Gas2Gac.SyncStoreRoomItem(pos, itemId)
    g_ScriptEvent:BroadcastInLua("SyncStoreRoomItem", pos, itemId)
end

function Gas2Gac.SyncStoreRoomPosCount(itemUnlockCount, silverUnlockCount, jadeUnlockCount)
    LuaStoreRoomMgr:SyncStoreRoomPosCount(itemUnlockCount, silverUnlockCount, jadeUnlockCount)
end

function Gas2Gac.ReceiveZhanLingRewardSuccess(level, index)
    LuaZhouNianQing2023Mgr:ReceiveZhanLingRewardSuccess(level, index)
end

function Gas2Gac.Anniv2023_SetConfidantId(playerId)
    LuaZhouNianQing2023Mgr:SetConfidantId(playerId)
end

function Gas2Gac.PlayerQueryCompetitionHonorRankReturn(selfRank, viewId, rankData, selfScore)
    print(selfScore)
    LuaCompetitionHonorMgr:PlayerQueryCompetitionHonorRankReturn(selfRank, selfScore, viewId, rankData)
end

function Gas2Gac.QueryCompetitionHonorPlayerInfoResult(score, appearanceProp, honorList, targetId)
    print(targetId)
    g_ScriptEvent:BroadcastInLua("QueryCompetitionHonorPlayerInfoResult", targetId, score, appearanceProp, honorList)
end

function Gas2Gac.QueryCompetitionHonorChapionGroupResult(chapionInfosU)
    g_ScriptEvent:BroadcastInLua("QueryCompetitionHonorChapionGroupResult", chapionInfosU)
end

function Gas2Gac.QueryCompetitionSeasonDataResult(GameSeasonData_U)
    g_ScriptEvent:BroadcastInLua("QueryCompetitionSeasonDataResult", GameSeasonData_U)
end

function Gas2Gac.FenYongZhenXianShowRuleWnd()
    LuaWuYi2023Mgr:FenYongZhenXianShowRuleWnd()
end

function Gas2Gac.FenYongZhenXianSyncGameInfo(selfForce, gameInfoU)
    LuaWuYi2023Mgr:FenYongZhenXianSyncGameInfo(selfForce, gameInfoU)
end

function Gas2Gac.FenYongZhenXianHorseKilled(attackerId, attackerForce, attackerClass, attackerGender)
    LuaWuYi2023Mgr:FenYongZhenXianHorseKilled(attackerId, attackerForce, attackerClass, attackerGender)
end

function Gas2Gac.FenYongZhenXianHorseCountDown(engineId, horseRebornTime, horseForce)
    LuaWuYi2023Mgr:FenYongZhenXianHorseCountDown(engineId, horseRebornTime, horseForce)
end

function Gas2Gac.FenYongZhenXianPlayerKilled(attackerId, attackerForce, attackerClass, attackerGender, defenderId, defenderForce, defenderClass, defenderGender)
    LuaWuYi2023Mgr:FenYongZhenXianPlayerKilled(attackerId, attackerForce, attackerClass, attackerGender, defenderId, defenderForce, defenderClass, defenderGender)
end

function Gas2Gac.FenYongZhenXianReachScoreLimit(score)
    LuaWuYi2023Mgr:FenYongZhenXianReachScoreLimit(score)
end

function Gas2Gac.FenYongZhenXianSendPlayResult(win, winReward, loseReward, extraReward, combo, teamInfoU, playerInfoU)
    LuaWuYi2023Mgr:FenYongZhenXianSendPlayResult(win, winReward, loseReward, extraReward, combo, teamInfoU, playerInfoU)
end

function Gas2Gac.FenYongZhenXianShowSkillChooseWnd(closeTime)
    LuaWuYi2023Mgr:FenYongZhenXianShowSkillChooseWnd(closeTime)
end

function Gas2Gac.QingMing2023PvpSendFriendRank(rankDataU)
    LuaQingMing2023Mgr:QingMing2023PvpRank(rankDataU)
end

function Gas2Gac.QingMing2023PvpSendTopRank(rankDataU)
    LuaQingMing2023Mgr:QingMing2023PvpRank(rankDataU)
end

function Gas2Gac.PlayerQueryTangGuoGuiDataResult(baseRewardTime, winRewardTime, joinTime)
    LuaLiuYi2023Mgr:PlayerQueryTangGuoGuiDataResult(baseRewardTime, winRewardTime, joinTime)
end

function Gas2Gac.SyncTangGuoGuiNpcHp(NpcLeftHp)
    LuaLiuYi2023Mgr:SyncTangGuoGuiNpcHp(NpcLeftHp)
end

function Gas2Gac.TangGuoGuiPlayResult(isSuccess, passNpcHp, useTime, reward_U)
    local info = g_MessagePack.unpack(reward_U)
    local rewards = {}
    for k,v in pairs(info) do
        table.insert(rewards, v)
    end

    LuaLiuYi2023Mgr:TangGuoGuiPlayResult(isSuccess, passNpcHp, useTime, rewards)
end

function Gas2Gac.OpenLiuYi2023DaoDanXiaoGuiRules()
    LuaLiuYi2023Mgr:OpenLiuYi2023DaoDanXiaoGuiRules()
end

function Gas2Gac.PlayerQueryDaoDanXiaoGuiDataResult(todayJoinTime, todayRewardTime, passTime)
    LuaLiuYi2023Mgr:PlayerQueryDaoDanXiaoGuiDataResult(todayJoinTime, todayRewardTime, passTime)
end

function Gas2Gac.SyncDaoDanXiaoGuiPlayInfo(playInfo_U)
    local info = g_MessagePack.unpack(playInfo_U)
    local playerInfos = {}

    for i = 1, #info, 5 do
        table.insert(playerInfos, {playerId = info[i],
        class = info[i + 1],
        playerName = info[i + 2],
        damage = info[i + 3],
        findNum = info[i + 4]})
    end
    LuaLiuYi2023Mgr:SyncDaoDanXiaoGuiPlayInfo(playerInfos)
end

function Gas2Gac.DaoDanXiaoGuiPlayResult(isSuccess, passTime, playerInfo_U, reward_U)
    local info = g_MessagePack.unpack(playerInfo_U)
    local playerInfos = {}
    for i = 1, #info, 5 do
        table.insert(playerInfos, {playerId = info[i],
        class = info[i + 1],
        playerName = info[i + 2],
        damage = info[i + 3],
        findNum = info[i + 4]})
    end

    local rewards = {}
    info = g_MessagePack.unpack(reward_U)
    for k,v in pairs(info) do
        table.insert(rewards, v)
    end

    LuaLiuYi2023Mgr:DaoDanXiaoGuiPlayResult(isSuccess, passTime, playerInfos, rewards)
end

function Gas2Gac.DaoDanXiaoGuiScanResult(isSuccess)
    LuaLiuYi2023Mgr:DaoDanXiaoGuiScanResult(isSuccess)
end

function Gas2Gac.DaoDanXiaoGuiSeeThroughResult(isSuccess)
    LuaLiuYi2023Mgr:DaoDanXiaoGuiSeeThroughResult(isSuccess)
end

function Gas2Gac.LiuYi2023FindDaoDanXiaoGui(playerId, class, gender)
    LuaLiuYi2023Mgr:LiuYi2023FindDaoDanXiaoGui(playerId, class, gender)
end

function Gas2Gac.XinXiangShiChengSendLotteryInfo(haveGold, haveSilver, hasBronze, numberOfDraws, needGold, needSilver, allTakeNeedGold, allTakeNeedSilver, itemsSelected)
    LuaWuYi2023Mgr:XinXiangShiChengSendLotteryInfo(haveGold, haveSilver, hasBronze, numberOfDraws, needGold, needSilver, allTakeNeedGold, allTakeNeedSilver, itemsSelected)
end

function Gas2Gac.SyncTianQiAutoBuyItemConfirmId(idx, itemConfirmId)
	--if CClientMainPlayer.Inst then
	--	CClientMainPlayer.Inst.PlayProp.TianQiAutoBuyItemConfirmId[idx] = itemConfirmId
	--end
    --g_ScriptEvent:BroadcastInLua("SyncTianQiAutoBuyItemConfirmId",idx, itemConfirmId)
end

function Gas2Gac.XinXiangShiChengCommonResult(itemsInfoU)
    LuaWuYi2023Mgr:XinXiangShiChengCommonResult(itemsInfoU)
end

function Gas2Gac.XinXiangShiChengAdvancedResult(silverCanGive, itemsInfoU)
    LuaWuYi2023Mgr:XinXiangShiChengAdvancedResult(silverCanGive, itemsInfoU)
end

function Gas2Gac.XinXiangShiChengSendCoinSuccess(friendId)
    LuaWuYi2023Mgr:XinXiangShiChengSendCoinSuccess(friendId)
end

function Gas2Gac.UnlockPlayerExpressionTxtWithExpiredTime(expressionTxt, expiredTime)
    LuaExpressionMgr:AddLimitExpressionTxt(expressionTxt, expiredTime)
    if expiredTime == -1 then
        EventManager.BroadcastInternalForLua(EnumEventType.UnlockMainPlayerExpressionTxt, {expressionTxt, true})
    else
        if expressionTxt == 18 or expressionTxt == 19 then
            g_MessageMgr:ShowMessage("ANNIV2023_TIEZHI_UNLOCK", ExpressionHead_ExpressionTxt.GetData(expressionTxt).Descrition)
        end
        --限时贴纸要重新刷新界面
        EventManager.BroadcastInternalForLua(EnumEventType.QueryValidExpressionTxtResult, {})
    end
end

function Gas2Gac.QueryValidExpressionTxtResultV2(expressionTxtTblUD)
    local ret = g_MessagePack.unpack(expressionTxtTblUD)
    LuaExpressionMgr.m_ValidLimitExpressionTxtList = {}
    if ret then
        for k, v in pairs(ret) do
            if v ~= -1 then
                LuaExpressionMgr:AddLimitExpressionTxt(k, v)
            else
                CExpressionMgr.Inst:AddExpressionTxt(k)
            end
        end
    end
    LuaExpressionMgr:RegistCheckExpired()
    EventManager.BroadcastInternalForLua(EnumEventType.QueryValidExpressionTxtResult, {})
end

function Gas2Gac.OpenStoreRoomWnd(isValidZuoQi, zuoqiTemplateId)
    LuaStoreRoomMgr:OnOpenStoreRoomWnd(isValidZuoQi, zuoqiTemplateId)
end

function Gas2Gac.XinXiangShiChengSendCoinAmount(gold, bronze)
    g_ScriptEvent:BroadcastInLua("XinXiangShiChengSendCoinAmount", gold, bronze)
end

function Gas2Gac.JZLYSyncStampProgress(stampProgressU, mswsTimesLeft)
    g_ScriptEvent:BroadcastInLua("JZLYSyncStampProgress", stampProgressU, mswsTimesLeft)
end

function Gas2Gac.UpdateCustomFacialData(engineId, key, facialDataUD)
    local CFacialMgr = import "L10.Game.CFacialMgr"
    CFacialMgr.OnUpdateCustomFacialData(engineId, key, facialDataUD)
end

-- 	stageTwoStatus: NotOpen = 0, Open = 1, Boss = 2
function Gas2Gac.ShuJia2023WorldEventsSyncStageTwoStatus(stageTwoStatus, stageTwoStartTime)
    LuaShuJia2023Mgr:ShuJia2023WorldEventsSyncStageTwoStatus(stageTwoStatus, stageTwoStartTime)
end

function Gas2Gac.VoiceMsgRecovery(playerId, channelId, voiceId)
    print("VoiceMsgRecovery", playerId, channelId, voiceId)
    CChatMgr.Inst:VoiceMsgRecovery(playerId, channelId, voiceId)
end

function Gas2Gac.AsianGames2023RequestQuestionsResult(questionCurDay, questionsCache_U, questionPlayerAnswersCache_U)
    -- id, description, optionOne, optionTwo, optionThree, optionFour, ...
    local questionsCache = g_MessagePack.unpack(questionsCache_U)

    -- { num1, num2, num3, num4 }, ...
    local questionPlayerAnswersCache = g_MessagePack.unpack(questionPlayerAnswersCache_U)
    g_ScriptEvent:BroadcastInLua("AsianGames2023RequestQuestionsResult",questionCurDay,questionsCache,questionPlayerAnswersCache)
end

function Gas2Gac.AsianGames2023JueDouRemind()
    local sceneId = AsianGames2023_Setting.GetData().JueDouSceneTemplateIds[0]
    local time = 300
    MessageWndManager.ShowConfirmMessage(g_MessageMgr:FormatMessage("YaYunHui2023_Teleport"), time, false, DelegateFactory.Action(function()
        Gac2Gas.LevelTeleport(sceneId)
    end), nil)
end


function Gas2Gac.AsianGames2023QueryGambleInfo_Result(id, status, choice, choiceOneNum, choiceTwoNum, description)
    local descripionInfo = g_MessagePack.unpack(description)
    g_ScriptEvent:BroadcastInLua("AsianGames2023QueryGambleInfo",id, status, choice, choiceOneNum, choiceTwoNum, descripionInfo)
end

function Gas2Gac.AsianGames2023RequestGambleBet_Result(id, choiceOneNum, choiceTwoNum)
    g_ScriptEvent:BroadcastInLua("AsianGames2023RequestGambleBet",id, choiceOneNum, choiceTwoNum)
end

function Gas2Gac.AsianGames2023QueryGambleStatus_Result(gambleStatusCache_U)
    local gambleStatusCache = g_MessagePack.unpack(gambleStatusCache_U)
    g_ScriptEvent:BroadcastInLua("AsianGames2023QueryGambleStatusResult",gambleStatusCache)
end

function Gas2Gac.PDFY_RequestMonsterAllRewardResult(updatedMonsterReward_U)
    LuaPengDaoMgr:PDFY_RequestMonsterAllRewardResult(updatedMonsterReward_U)
end

function Gas2Gac.QueryCustomFacialDataResult(xianfanStatus, array_U)
    LuaPinchFaceMgr:QueryCustomFacialDataResult(xianfanStatus, array_U)
end

function Gas2Gac.RequestSetCustomFacialDataResult(bSuccess)
    if bSuccess then
        CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
    else
        LuaPinchFaceMgr.m_CanCacheOfflineData = true
        LuaPinchFaceMgr:CacheOfflineData()
        g_MessageMgr:ShowMessage("PinchFace_CacheOfflineData")
        CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
    end
end

function Gas2Gac.CanTransformZKWeaponResult(equipId, needYinPiao, needYinLiang)
    if CClientMainPlayer.Inst then  
        local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, equipId);
        local item = CItemMgr.Inst:GetById(equipId)
        if pos > 0 and item and item.IsEquip then
            local issubhand = item.Equip.SubHand > 0
            local message = ""
            local hasIdentifiableSkill = item.Equip.Identifiable > 0 
            if item.IsBinded then
                local mft = issubhand and "ZKWeapon_zhuanhuan_bang_fushou" or "ZKWeapon_zhuanhuan_bang"
                message = g_MessageMgr:FormatMessage(mft, needYinPiao + needYinLiang, item.ColoredName)
            else
                local mft = issubhand and "ZKWeapon_zhuanhuan_feibang_fushou" or "ZKWeapon_zhuanhuan_feibang"
                message = g_MessageMgr:FormatMessage(mft, needYinPiao + needYinLiang, item.ColoredName)
            end
            if hasIdentifiableSkill and not issubhand then
                message = g_MessageMgr:FormatMessage("ZKWeapon_zhuanhuan_withIdentifiableSkill")..message
            end
            local okfunc = function()
                Gac2Gas.ConfirmTransformZKWeapon(EnumToInt(EnumItemPlace.Bag), pos, equipId)--IIs
            end
            g_MessageMgr:ShowOkCancelMessage(message,okfunc,nil,nil,nil,false)
        end
    end
end

function Gas2Gac.TransformZKWeaponResult(equipId, subHand)
    g_MessageMgr:ShowMessage("ZKWeapon_zhuanhuan_wancheng")
end

function Gas2Gac.SetIsAllowOpenStoreRoomFromRepo(isAllow)
    LuaStoreRoomMgr.IsAllowOpenStoreRoomFromRepo = isAllow
    g_ScriptEvent:BroadcastInLua("SetIsAllowOpenStoreRoomFromRepo", isAllow)
end

function Gas2Gac.SendOpenPassInfos(openPassIds, time)
    LuaCommonPassportMgr:SendOpenPassInfos(openPassIds, time)
end

function Gas2Gac.UpdatePassDataWithId(id, data_U, reason)
    LuaCommonPassportMgr:UpdatePassDataWithId(id, data_U, reason)
end

function Gas2Gac.QueryGuildLeagueSetHeroInfoResult(ret_U)
    LuaGuildLeagueMgr:QueryGuildLeagueSetHeroInfoResult(ret_U)
end

function Gas2Gac.AppointGuildLeagueHeroSuccess(destPlayerId, bHero)
    LuaGuildLeagueMgr:AppointGuildLeagueHeroSuccess(destPlayerId, bHero)
end

function Gas2Gac.RequestGetGuildLeagueCrossTrainLevelRewardSuccess(trainType, rewardIdx)
    LuaGuildLeagueCrossMgr:RequestGetGuildLeagueCrossTrainLevelRewardSuccess(trainType, rewardIdx)
end

function Gas2Gac.SendGuildLeagueCrossTrainRankRewardInfo(ret_U)
	LuaGuildLeagueCrossMgr:SendGuildLeagueCrossTrainRankRewardInfo(ret_U)
end

function Gas2Gac.OpenGuildLeagueHeroInfoWnd(canSetHero, info_U)
    LuaGuildLeagueMgr:OpenGuildLeagueHeroInfoWnd(canSetHero, info_U)
end

function Gas2Gac.PDFY_ShuffleBuffFail()
    print("PDFY_ShuffleBuffFail")
    LuaPengDaoMgr:PDFY_ShuffleBuffFail()
end

function Gas2Gac.SendGuildLeagueCrossMatchBuffInfo(info_U)
    LuaGuildLeagueCrossMgr:SendGuildLeagueCrossMatchBuffInfo(info_U)
end

function Gas2Gac.ShowZhongYuanPuDuResult(bResult, mode, lvIdx, extraInfo_U)
    LuaZhongYuanJie2023Mgr:ShowZhongYuanPuDuResult(bResult, mode,extraInfo_U)
end

function Gas2Gac.QueryGuildLeagueCrossZhanLingRankResult(ret_U, zhanLingLv, zhanLingLingYu)
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingRankResult(ret_U, zhanLingLv, zhanLingLingYu)
end

function Gas2Gac.QueryGuildLeagueCrossRewardPoolInfoResult(context, totalLingYu, totalSilver)
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossRewardPoolInfoResult(context, totalLingYu, totalSilver)
end

function Gas2Gac.QueryGuildLeagueCrossZhanLingExtInfoResult(ownRank, totalLingYu)
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingExtInfoResult(ownRank, totalLingYu)
end

function Gas2Gac.XianHaiTongXingPlayerQueryTeamInfo_Result(member1Id, member2Id, member3Id, member4Id, member5Id, vipInfo, progress, extraInfo_U)
    local extraInfo = g_MessagePack.unpack(extraInfo_U)
    LuaShuJia2023Mgr:XianHaiTongXingPlayerQueryTeamInfo_Result(member1Id, member2Id, member3Id, member4Id, member5Id, vipInfo, progress, extraInfo)
end

function Gas2Gac.XianHaiTongXingPlayerQueryTeamProgress_Result(progress)
    LuaShuJia2023Mgr:XianHaiTongXingPlayerQueryTeamProgress_Result(progress)
end

function Gas2Gac.SendPlayerAppearance(targetPlayerId, context, isSucc, name, gender, class, titleId, titleName, appearProp)
    --print("SendPlayerAppearance", targetPlayerId, context, isSucc, name, gender, class, titleId, titleName, appearProp)
    local fakeAppearance = CreateFromClass(CPropertyAppearance)
    fakeAppearance:LoadFromString(appearProp, CommonDefs.ConvertIntToEnum(typeof(EnumClass), class), CommonDefs.ConvertIntToEnum(typeof(EnumGender), gender))

    g_ScriptEvent:BroadcastInLua("SendPlayerAppearance", targetPlayerId, context, isSucc, name, gender, class, titleId, titleName, fakeAppearance)
end

function Gas2Gac.Carnival2023MonsterResult(killMonsterNum, getAward, getPoint)
    LuaCarnival2023Mgr:Carnival2023MonsterResult(killMonsterNum, getAward, getPoint)
end

function Gas2Gac.Carnival2023RequestPlayDataResult(attackBuffLevel, defBuffLevel, speedBuffLevel, skillPoint, mReminTime, bReminTime, rReminTime, rank)
    LuaCarnival2023Mgr:Carnival2023RequestPlayDataResult(attackBuffLevel, defBuffLevel, speedBuffLevel, skillPoint, mReminTime, bReminTime, rReminTime, rank)
end

function Gas2Gac.Carnival2023BossResult(bWin, finishTime, myRank, minTime, award)
    LuaCarnival2023Mgr:Carnival2023BossResult(bWin, finishTime, myRank, minTime, award)
end

function Gas2Gac.QueryCarnival2023RankResult(myRank, finishTime, RankData_U)
    LuaCarnival2023Mgr:QueryCarnival2023RankResult(myRank, finishTime, RankData_U)
end

function Gas2Gac.Carnival2023BossTrapPlayerCount(npcid, currentPlayerNum, needPlayerNum)
    LuaCarnival2023Mgr:Carnival2023BossTrapPlayerCount(npcid, currentPlayerNum, needPlayerNum)
end

function Gas2Gac.PlayerGetBigTicketCarnivalCollect()
    LuaCarnivalCollectMgr:PlayerGetBigTicketCarnivalCollect()
end

function Gas2Gac.PlayerGetSmallTicketCarnivalCollect()
    LuaCarnivalCollectMgr:PlayerGetSmallTicketCarnivalCollect()
end

function Gas2Gac.SyncJingYuanPlayInfo(teamJingYuanCount, playerJingYuanCount, addJingYuanPlayerId)
    LuaGuoQing2023Mgr:SyncArenaPlayerJingYuanInfo(teamJingYuanCount, playerJingYuanCount, addJingYuanPlayerId)
end

function Gas2Gac.SyncJingYuanPlayResult(win, playTime, teamInfoU, playerInfoU, rewardInfoU)
    LuaGuoQing2023Mgr:SyncJingYuanPlayResult(win, playTime, teamInfoU, playerInfoU, rewardInfoU)
end

function Gas2Gac.JingYuanPlayRemind()
    LuaGuoQing2023Mgr:JingYuanPlayRemind()
end

function Gas2Gac.QueryJingYuanPlayInfoResult(talentLevel, engageTimes, hasPieceCount, maxPieceCount)
    g_ScriptEvent:BroadcastInLua("QueryJingYuanPlayInfoResult", talentLevel, engageTimes, hasPieceCount, maxPieceCount)
end


function Gas2Gac.QueryGuildPinTuDataResult(stage, duration, answerCountdown, listUD, bQueryFromClient)
	print("QueryGuildPinTuDataResult", stage, duration, answerCountdown, bQueryFromClient)
	local list = g_MessagePack.unpack(listUD)
	if not list then return end

    local pinTuDataList = {}
	for i = 1, #list, 8 do
        local pinTuData = {}
		pinTuData.id = list[i]
		pinTuData.getchips = list[i+1]
		pinTuData.setchips = list[i+2]
		pinTuData.answered = list[i+3]
		pinTuData.answer = list[i+4]
		pinTuData.finishtime = list[i+5]
		pinTuData.answerPlayerName = list[i+6]
		pinTuData.pinTuPlayerCount = list[i+7]
		
        print("PinTuData", pinTuData.id, pinTuData.getchips, pinTuData.setchips, pinTuData.answered, pinTuData.answer, pinTuData.finishtime, pinTuData.answerPlayerName, pinTuData.pinTuPlayerCount)
        table.insert(pinTuDataList, pinTuData)
	end

    CLuaGuildMgr:OpenEnterWnd(stage, duration, pinTuDataList, bQueryFromClient)
end

function Gas2Gac.QueryGuildPinTuProgressResult(guildFinished, selfFinished, finishTime)
	print("QueryGuildPinTuProgressResult", tostring(guildFinished), tostring(selfFinished), finishTime)
    if guildFinished or selfFinished then
        g_ScriptEvent:BroadcastInLua("QueryGuildPinTuProgressResult")
    end
end

function Gas2Gac.OpenGuildPinTuDetailWnd(stage, duration, answerCountdown, listUD, chipIdListUD)
	print("OpenGuildPinTuDetailWnd", stage, duration, answerCountdown)

	local list = g_MessagePack.unpack(listUD)
	if not list then return end

	local id = list[1]
	local getchips = list[2]
	local setchips = list[3]
	local answered = list[4]
	local answer = list[5]
	local finishtime = list[6]
	local answerPlayerName = list[7]
	local pinTuPlayerCount = list[8]
	print("PinTuData", id, getchips, setchips, answered, answer, finishtime, answerPlayerName, pinTuPlayerCount)

	local chipIdList = g_MessagePack.unpack(chipIdListUD)
	if not chipIdList then return end

    -- 判断时间
    CLuaGuildMgr.m_PinTuStage = stage
    CLuaGuildMgr.m_PinTuDuration = duration

	print("ChipIdList", table.concat(chipIdList, ","))

    CLuaGuildMgr:OpenDetailWnd(id, getchips, setchips, answered, answer, finishtime, pinTuPlayerCount, chipIdList, answerCountdown)
end

function Gas2Gac.CloseGuildPinTuDetailWnd()
	print("CloseGuildPinTuDetailWnd")
    CUIManager.CloseUI(CLuaUIResources.GuildPuzzleWnd)
end

function Gas2Gac.PlayerEnterGuildPinTu(playerId, pictureId, pinTuPlayerCount)
	print("PlayerEnterGuildPinTu", playerId, pictureId, pinTuPlayerCount)
    g_ScriptEvent:BroadcastInLua("PlayerEnterGuildPinTu", playerId, pictureId, pinTuPlayerCount)
end

function Gas2Gac.PlayerLeaveGuildPinTu(playerId, pictureId, pinTuPlayerCount)
	print("PlayerLeaveGuildPinTu", playerId, pictureId, pinTuPlayerCount)
    g_ScriptEvent:BroadcastInLua("PlayerLeaveGuildPinTu", playerId, pictureId, pinTuPlayerCount)
end

function Gas2Gac.GetGuildPinTuChipSuccess(pictureId, chipId)
	print("GetGuildPinTuChipSuccess", pictureId, chipId)
    g_ScriptEvent:BroadcastInLua("GetGuildPinTuChipSuccess", pictureId, chipId)
end

function Gas2Gac.ReturnGuildPinTuChipSuccess(pictureId, chipId)
	print("ReturnGuildPinTuChipSuccess", pictureId, chipId)
    g_ScriptEvent:BroadcastInLua("ReturnGuildPinTuChipSuccess", pictureId, chipId)
end

function Gas2Gac.UseGuildPinTuChipSuccess(pictureId, chipId, place)
	print("UseGuildPinTuChipSuccess", pictureId, chipId, place)
    g_ScriptEvent:BroadcastInLua("UseGuildPinTuChipSuccess", pictureId, chipId, place)
end

function Gas2Gac.UseGuildPinTuChipFail(pictureId, chipId, place)
	print("UseGuildPinTuChipFail", pictureId, chipId, place)
    g_ScriptEvent:BroadcastInLua("UseGuildPinTuChipFail", pictureId, chipId, place)
end

function Gas2Gac.AnswerGuildPinTuQuestionSuccess(pictureId, answered, answer, placeListUD)
	print("AnswerGuildPinTuQuestionSuccess", pictureId, answered, answer)
	local placeList = g_MessagePack.unpack(placeListUD)
	if not placeList then return end

	print("PinTuPlaces", table.concat(placeList))
    g_ScriptEvent:BroadcastInLua("AnswerGuildPinTuQuestionSuccess", pictureId, answered, answer, placeList)
end

function Gas2Gac.AnswerGuildPinTuQuestionFail(pictureId, answered, answer)
	print("AnswerGuildPinTuQuestionFail", pictureId, answered, answer)
    g_ScriptEvent:BroadcastInLua("AnswerGuildPinTuQuestionFail", pictureId, answered, answer)
end

function Gas2Gac.PlayerGetGuildPinTuChip(playerId, pictureId, chipId)
	print("PlayerGetGuildPinTuChip", playerId, pictureId, chipId)
    g_ScriptEvent:BroadcastInLua("PlayerGetGuildPinTuChip", playerId, pictureId, chipId)
end

function Gas2Gac.PlayerReturnGuildPinTuChip(playerId, pictureId, chipId)
	print("PlayerReturnGuildPinTuChip", playerId, pictureId, chipId)
    g_ScriptEvent:BroadcastInLua("PlayerReturnGuildPinTuChip", playerId, pictureId, chipId)
end

function Gas2Gac.PlayerFillGuildPinTu(playerId, pictureId, place, listUD)
	print("PlayerFillGuildPinTu", playerId, pictureId, place)

	local list = g_MessagePack.unpack(listUD)
	if not list then return end

	local id = list[1]
	local getchips = list[2]
	local setchips = list[3]
	local answered = list[4]
	local answer = list[5]
	local finishtime = list[6]
	local answerPlayerName = list[7]
	print("PinTuData", id, getchips, setchips, answered, answer, finishtime, answerPlayerName)

    g_ScriptEvent:BroadcastInLua("PlayerFillGuildPinTu", playerId, pictureId, place, id, getchips, setchips, answered, answer, finishtime)
end

function Gas2Gac.PlayerAnswerGuildPinTuQuestion(playerId, pictureId, placeListUD, listUD)
	print("PlayerAnswerGuildPinTuQuestion", playerId, pictureId)

	local placeList = g_MessagePack.unpack(placeListUD)
	if not placeList then return end
	print("PinTuPlaces", table.concat(placeList))

	local list = g_MessagePack.unpack(listUD)
	if not list then return end

	local id = list[1]
	local getchips = list[2]
	local setchips = list[3]
	local answered = list[4]
	local answer = list[5]
	local finishtime = list[6]
	local answerPlayerName = list[7]
	print("PinTuData", id, getchips, setchips, answered, answer, finishtime, answerPlayerName)

    g_ScriptEvent:BroadcastInLua("PlayerAnswerGuildPinTuQuestion", playerId, pictureId, placeList, id, getchips, setchips, answered, answer, finishtime)
end

function Gas2Gac.GuildPinTuPlayStart()
	print("GuildPinTuPlayStart")
    g_ScriptEvent:BroadcastInLua("GuildPinTuPlayStart")
end

function Gas2Gac.GuildPinTuPlayEnd()
	print("GuildPinTuPlayEnd")
    g_ScriptEvent:BroadcastInLua("GuildPinTuPlayEnd")
end

function Gas2Gac.QueryGuildPinTuPlayerDataResult(playerDataList_U)
	print("QueryGuildPinTuPlayerDataResult")
	local list = g_MessagePack.unpack(playerDataList_U)
	if not list then return end

    local dataList = {}
	for i = 1, #list, 7 do
        local data = {}
		data.playerId = list[i]
		data.playerName = list[i+1]
		data.isOnline = list[i+2]
		data.playerClass = list[i+3]
		data.playerPinTuTimes = list[i+4]
		data.playerPinTuTime = list[i+5]
		data.playerChipNum = list[i+6]
		
        table.insert(dataList, data)
	end
    g_ScriptEvent:BroadcastInLua("QueryGuildPinTuPlayerDataResult", dataList)
end

function Gas2Gac.ProtectHangZhouSyncSceneNpcCount(sceneNpcCountU, remainTimes)
    g_ScriptEvent:BroadcastInLua("ProtectHangZhouSyncSceneNpcCount", sceneNpcCountU, remainTimes)
end

function Gas2Gac.UpdateFurnitureExpireNotify(isNotify) -- 1 or 0
    local rightMenue = CRightMenuWnd.Instance
    if rightMenue and rightMenue.houseAlert then
        rightMenue.houseAlert:SetActive(isNotify == 1)
    end
    if CClientHouseMgr.Inst.mCurFurnitureProp3 and CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify then
        CClientHouseMgr.Inst.mCurFurnitureProp3.IsFurnitureExpireNotify = isNotify
    end
    g_ScriptEvent:BroadcastInLua("RefreshFurnitureQishuAlert", isNotify == 1)
end

function Gas2Gac.QueryGuoQing2023InfoResult(jyOpen, jyOpenMatch, jyPlayEnd, jyTimes, hzOpen, hzPlayEnd, hzTimes, hzNextTime)
    LuaGuoQing2023Mgr:QueryGuoQing2023InfoResult(jyOpen, jyOpenMatch, jyPlayEnd, jyTimes, hzOpen, hzPlayEnd, hzTimes, hzNextTime)
end

function Gas2Gac.QuanMinKaiShu_SyncPlayData(playDataU)
    LuaProfessionTransferMgr:QuanMinKaiShu_SyncPlayData(playDataU)
end

function Gas2Gac.QuanMinKaiShu_GetTianShuSuccess(count, score)
    LuaProfessionTransferMgr:QuanMinKaiShu_GetTianShuSuccess(count, score)
end

function Gas2Gac.UnlockFacialStyle(id)
    print("UnlockFacialStyle", id)
end

function Gas2Gac.FashionLotteryQueryTicketPriceResult(count, price, originalPrice, discount)
    LuaFashionLotteryMgr:FashionLotteryQueryTicketPriceResult(count, price, originalPrice, discount)
end

function Gas2Gac.FashionLotteryBuyTicketResult(count, price)
    g_ScriptEvent:BroadcastInLua("FashionLotteryBuyTicketResult", count, price)
end

function Gas2Gac.FashionLotteryDrawResult(rewardItemsTblU)
    LuaFashionLotteryMgr:FashionLotteryDrawResult(rewardItemsTblU)
end

function Gas2Gac.CheckFashionLotteryEnsuranceInfoResult(left)
    LuaFashionLotteryMgr:CheckFashionLotteryEnsuranceInfo(left)
end

function Gas2Gac.CheckFashionLotteryDiscountInfoResult(discount)
    g_ScriptEvent:BroadcastInLua("CheckFashionLotteryDiscountInfoResult", discount)
end

function Gas2Gac.EnableScreenEdgeDistortion(range, intensity)
    print("EnableScreenEdgeDistortion", range, intensity)
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:ShowRadialBlurEnable(range, intensity)
    end
end

function Gas2Gac.DisableScreenEdgeDistortion()
    print("DisableScreenEdgeDistortion")
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl.RadialBlurEnable = false
    end
end

function Gas2Gac.StartJuQingOperaPlay(id)
    print("StartJuQingOperaPlay", id)
    LuaZhuJueJuQingMgr:StartJuQingOperaPlay(id)
end

function Gas2Gac.StartJuQingMaskPlay(id)
    print("StartJuQingMaskPlay", id)
    LuaZhuJueJuQingMgr:ShowChangeFaceWnd(id)
end

function Gas2Gac.YaoYeManJuan_SyncPlayState(PlayState, EnterStateTime, NextStateTime, ExtraInfo)
    print("YaoYeManJuan_SyncPlayState", PlayState, EnterStateTime, NextStateTime, ExtraInfo)

    local ExtraInfoTbl = {}
    if MsgPackImpl.IsValidUserData(ExtraInfo) then
        ExtraInfoTbl = g_MessagePack.unpack(ExtraInfo)
        for k, v in pairs(ExtraInfoTbl) do
            print(k,v )
        end
    end
    LuaYaoYeManJuanMgr:YaoYeManJuan_SyncPlayState(PlayState, EnterStateTime, NextStateTime, ExtraInfoTbl)
end

function Gas2Gac.YaoYeManJuan_UpdateIconStatus(bShow)
    print("YaoYeManJuan_UpdateIconStatus", bShow)
    LuaYaoYeManJuanMgr.m_IconStatus = bShow
    g_ScriptEvent:BroadcastInLua("YaoYeManJuan_UpdateIconStatus", bShow)
end

function Gas2Gac.NotifyChargeUseWebpay(msg, url)
	print("NotifyChargeUseWebpay", url)
	-- 国服ios从475730版本开始使用ios7格式收据，更早的版本不再让下单支付，引导到gamepay网页支付(这里是给pc弹引导消息)
	MessageWndManager.ShowOKMessage(msg, DelegateFactory.Action(function ()
		Application.OpenURL(url)
	end))
end

function Gas2Gac.SyncCustomFurnitureTemplateNum(houseId, playerId, num)
    --print("SyncCustomFurnitureTemplateNum",houseId, playerId, num)
    if CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2 and CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo then
		local infoMap = CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo
		if infoMap and CommonDefs.DictContains_LuaCall(infoMap, playerId) then
			local customInfo = CommonDefs.DictGetValue_LuaCall(infoMap, playerId)
            customInfo.Num = num
        elseif infoMap then
            local customInfo = CCustomFurnitureTemplateInfoType()
            customInfo.Num = num
            customInfo.LevelInfo[1] = 1
            customInfo.LevelInfo[2] = 1
            CommonDefs.DictSet_LuaCall(infoMap,playerId,customInfo)
		end
	end
    g_ScriptEvent:BroadcastInLua("SyncCustomFurnitureTemplateNum", houseId, playerId, num)
end

function Gas2Gac.SyncCustomFurnitureTemplateLv(houseId, playerId, slotIdx, lv)
    --print("SyncCustomFurnitureTemplateLv",houseId, playerId, slotIdx, lv)
    if CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2 and CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo then
		local infoMap = CClientHouseMgr.Inst.mCurFurnitureProp2.CustomFurnitureTemplateInfo
		if infoMap and CommonDefs.DictContains_LuaCall(infoMap, playerId) then
			local customInfo = CommonDefs.DictGetValue_LuaCall(infoMap, playerId)
			customInfo.LevelInfo[slotIdx] = lv
        elseif infoMap then
            local customInfo = CCustomFurnitureTemplateInfoType()
            customInfo.Num = 2
            customInfo.LevelInfo[1] = 1
            customInfo.LevelInfo[2] = 1
            customInfo.LevelInfo[slotIdx] = lv
            CommonDefs.DictSet_LuaCall(infoMap,playerId,customInfo)
		end
	end
    g_MessageMgr:ShowMessage("CUSTOMFURNITURE_UPGRADE_SUCCESS")
    g_ScriptEvent:BroadcastInLua("SyncCustomFurnitureTemplateLv", houseId, playerId, slotIdx, lv)
end

function Gas2Gac.SyncGlobalJinglingKey(jinglingKeyTblUD)
	--print("SyncGlobalJinglingKey", jinglingKeyTblUD)
    LuaJingLingMgr.m_WeChatjinglingKey = {}
	local jinglingKeyTbl = g_MessagePack.unpack(jinglingKeyTblUD)
    if jinglingKeyTbl then
        -- index = 1,商城按钮关键词
        -- index = 2,福利界面关键词
        for i = 1, #jinglingKeyTbl,2 do
            LuaJingLingMgr.m_WeChatjinglingKey[jinglingKeyTbl[i]] = jinglingKeyTbl[i + 1]
        end
    end
end

-- rankCache_U: { teamId, score }, ...
-- fightData_U: playerId, name, teamId, killNum, ctrlNum, heal, reliveNum, ...
function Gas2Gac.SyncDouble11MQLDResult(isGetDailyAward, isGetEliteAward, isNewRecord, rankCache_U, fightData_U)
    LuaShuangshiyi2023Mgr:SyncDouble11MQLDResult(isGetDailyAward, isGetEliteAward, isNewRecord, g_MessagePack.unpack(rankCache_U), g_MessagePack.unpack(fightData_U))
end

function Gas2Gac.SyncDouble11MQLDPlayStatus(playerId, status, curTime, endTime, rankCache_U)
    LuaShuangshiyi2023Mgr:Double11MQLD_SyncPlayState(playerId, status, curTime, endTime, g_MessagePack.unpack(rankCache_U))
end

function Gas2Gac.Double11MQLDBroadFlyAction(playerId, x, y, z)
    g_ScriptEvent:BroadcastInLua("Double11MQLDBroadFlyAction", playerId, x, z, y)
end

-- extraInfo_U: playerId, name, level, career, ..
function Gas2Gac.SyncDouble11BYDLYResult(isHero, isWin, isGetDailyAward, isGetHeroAward, isGetEliteAward, usedTime, extraInfo_U)
    LuaShuangshiyi2023Mgr:SyncDouble11BYDLYResult(isHero, isWin, isGetDailyAward, isGetHeroAward, isGetEliteAward, usedTime, extraInfo_U)
end

function Gas2Gac.SyncDouble11BYDLYPlayInfo(isHero, stage, curtime, timelimit, isChangeStage)
    LuaShuangshiyi2023Mgr:SyncDouble11BYDLYPlayInfo(isHero, stage, curtime, timelimit, isChangeStage)
end

-- teamTreasureInfo_U: playerId, name, treasure, ...
function Gas2Gac.SyncDouble11BYDLYFirstStageInfo(totalTreasure, teamTreasureInfo_U)
    LuaShuangshiyi2023Mgr:SyncDouble11BYDLYFirstStageInfo(totalTreasure, g_MessagePack.unpack(teamTreasureInfo_U))
end

function Gas2Gac.SyncDouble11BYDLYHeroReliveTimes(heroReliveTimes)
    print("SyncDouble11BYDLYHeroReliveTimes", heroReliveTimes)
    LuaShuangshiyi2023Mgr:SyncDouble11BYDLYHeroReliveTimes(heroReliveTimes)
end

-- rewards_U: key = itemId, value = count
function Gas2Gac.Double11ShowRechargeLotteryRewards(isBagFull, rewards_U)
    LuaShuangshiyi2023Mgr:Double11ShowRechargeLotteryRewards(isBagFull, rewards_U)
end

-- coupons_U: coupon1, coupon2, ...
function Gas2Gac.Request2023Double11ExteriorDiscountCoupons_Result(coupons_U)
    LuaShuangshiyi2023Mgr:Show2023Double11ExteriorDiscountCoupons_Result(coupons_U)
end

function Gas2Gac.ZhongYuanPuDu_SyncProgress(stageId, param1, param2)
    LuaZhongYuanJie2023Mgr:ZhongYuanPuDu_SyncProgress(stageId, param1, param2)
    g_ScriptEvent:BroadcastInLua("ZhongYuanPuDu_SyncProgress", stageId, param1, param2)
end

function Gas2Gac.DownloadImRecord(url, token, context)
    CLuaIMMgr:DownloadImRecord(url, token, context)
end

function Gas2Gac.UploadImRecord(url, token, context)
    CLuaIMMgr:UploadImRecord(url, token, context)
end

function Gas2Gac.UploadImRecordSuccessCallback(bSuccess, fileId, context)
    print("UploadImRecordSuccessCallback", bSuccess, fileId, context)
end

function Gas2Gac.UploadFacialPic(url, token, context)
    print("UploadFacialPic", url, token, context)
    CFuxiFaceDNAMgr.Inst:ResponseFuxiFpUrlAndToken(url, token, context)
end

function Gas2Gac.ReviewFacialPicBegin(url, pic_id, context, status)
    print("ReviewFacialPicBegin", url, pic_id, context, status)
    g_ScriptEvent:BroadcastInLua("OnReviewFacialPicBeginInLua", url, pic_id, context, status)
end

function Gas2Gac.ReviewFacialPicEnd(url, pic_id, context, status)
    print("ReviewFacialPicEnd", url, pic_id, context, status)
    g_ScriptEvent:BroadcastInLua("OnReviewFacialPicEndInLua", url, pic_id, context, status)
end

function Gas2Gac.BeginCameraShake(shakeType, amplitude, frequency, duration)
    CameraFollow.Inst:BeginCameraShake(shakeType, amplitude, frequency, duration)
end

function Gas2Gac.EndCameraShake()
    CameraFollow.Inst:EndCameraShake()
end

function Gas2Gac.ReplyOpenHengYuDangKouInfo(replyInfo_U)
    local reply = g_MessagePack.unpack(replyInfo_U)
    g_ScriptEvent:BroadcastInLua("ReplyOpenHengYuDangKouInfo", reply)
end

function Gas2Gac.SendHengYuDangKouStat(statType, data_U, playerCount, statTotal)
    -- data_U: playerId, playerName, playerClass, playerStat
    LuaHengYuDangKouMgr:SendHengYuDangKouStat(statType, data_U, playerCount, statTotal)
end

function Gas2Gac.ShowManualEndHengYuDangKouConfirm()
    local msg = g_MessageMgr:FormatMessage("ManualEndHengYuDangKou_Confirm")
    MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function ()
        Gac2Gas.RequestManualEndHengYuDangKou()
    end), nil, 10)
end

function Gas2Gac.SyncHengYuDangKouSceneInfo(sycInfo_U)
    LuaHengYuDangKouMgr:SyncHengYuDangKouSceneInfo(sycInfo_U)
end

function Gas2Gac.SyncHengYuDangKouKillExtraMonster(killCount)
    LuaHengYuDangKouMgr:SyncHengYuDangKouKillExtraMonster(killCount)
end

function Gas2Gac.SyncHengYuDangKouBossHp(curHp, fullHp)
    LuaHengYuDangKouMgr:SyncHengYuDangKouBossHp(curHp, fullHp)
end

function Gas2Gac.QueryHengYuDangKouTeamMemberInfoResult(memberInfo_u, extraInfo_u)
    -- {Capacit, teamId, {memberId, member:GetName(), member:GetClass(), member:GetLevel(), member:GetXianFanStatus()}, team.m_LeaderId}
    -- {LastLeaderApplyTs:value}

    local memberInfo = g_MessagePack.unpack(memberInfo_u)
    local extraInfo = g_MessagePack.unpack(extraInfo_u)
    g_ScriptEvent:BroadcastInLua("QueryHengYuDangKouTeamMemberInfoResult", memberInfo, extraInfo)
end

function Gas2Gac.QueryHengYuDangKouTeamMemberHpInfoResult(memberHpInfo_u)
    -- playerId : hpPercent
    local memberHpInfo = g_MessagePack.unpack(memberHpInfo_u)
    g_ScriptEvent:BroadcastInLua("QueryHengYuDangKouTeamMemberHpInfoResult", memberHpInfo)
end

-- 横屿荡寇排行榜数据
function Gas2Gac.ReplyHengYuDangKouRankList(replyInfo_U)
    LuaHengYuDangKouMgr:ReplyHengYuDangKouRankList(replyInfo_U)
end

-- 横屿荡寇鬼装数据
function Gas2Gac.SendHengYuDangKouItemInfo(itemData)
    LuaHengYuDangKouMgr:SendHengYuDangKouItemInfo(itemData)
end

-- 横屿荡寇结算数据
function Gas2Gac.SyncHengYuDangKouOpenBox(result_U)
    LuaHengYuDangKouMgr:SyncHengYuDangKouOpenBox(result_U)
end

function Gas2Gac.RemoveAllFxFromObject(engineId)
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj ~= nil then
        obj.DelayExecuteQueue:AppendCommand(DelegateFactory.Action_uint(function (skillId)
            obj.RO:DestroyAllFX()
        end), false)
    end
end

function Gas2Gac.SendHengYuDangKouPlayer(data_U, playerCount)
    -- 复用之前的解析与广播方案
    Gas2Gac2.m_Instance:SendKouDaoSceneInfo(data_U, playerCount)
end

function Gas2Gac.ShowHideMonsterName(engineId, bShow)
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj ~= nil and TypeAs(obj, typeof(CClientMonster)) then
        obj:UpdateHeadInfoVisibilityByServer(bShow)
    end
end

function Gas2Gac.SyncZuoQiPassengerModeChanged(driverEngineId, zuoqiId, mode, targetZuoQiTemplateId)
    local obj = CClientObjectMgr.Inst:GetObject(driverEngineId)
    if not obj then return end
    if TypeAs(obj, typeof(CClientMainPlayer)) then
		local myZuoQiId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId or 0
        if myZuoQiId == zuoqiId and CClientMainPlayer.Inst then
            CZuoQiMgr.ProcessZuoQiTransformAni(CClientMainPlayer.Inst.RO, targetZuoQiTemplateId)
        end
	elseif TypeAs(obj, typeof(CClientOtherPlayer)) then
        local co = TypeAs(obj, typeof(CClientOtherPlayer))
		CZuoQiMgr.ProcessZuoQiTransformAni(co.RO, targetZuoQiTemplateId)
    end
    g_ScriptEvent:BroadcastInLua("OnSyncZuoQiPassengerModeChanged", driverEngineId, zuoqiId, mode, targetZuoQiTemplateId)
end

-- 玩法拍卖竞拍
-- Gac2Gas.RequestGamePlayPaiMaiBet
function Gas2Gac.RequestGamePlayPaiMaiBetResult(bOk, itemTemplateId, price, targetType, betCount)
    LuaAuctionMgr:RequestGamePlayPaiMaiBetResult(bOk, itemTemplateId, price, targetType, betCount)
end

-- 查询不同页签上架的数量
-- Gac2Gas.QueryGamePlayPaiMaiOnShelfCount
-- CountResultTbl[itemid]=itemcount
function Gas2Gac.SendGamePlayPaiMaiOnShelfCount(targetType, playId, countResultUd)
    local CountResultTbl = g_MessagePack.unpack(countResultUd)
    g_ScriptEvent:BroadcastInLua("OnSendGamePlayPaiMaiOnShelfCount", targetType, playId, CountResultTbl)
end


-- 请求玩法拍卖上架商品
-- Gac2Gas.QueryGamePlayPaiMaiOnShelfItem
function Gas2Gac.SendGamePlayPaiMaiOnShelfItem(targetType, playId, validTotalCount, infoRpcUd, pageNum, itemTemplateId, onShelfCurCount, onShelfTotalCount)
    LuaAuctionMgr:SendGamePlayPaiMaiOnShelfItem(targetType, playId, validTotalCount, infoRpcUd, pageNum, itemTemplateId, onShelfCurCount, onShelfTotalCount)
end

-- 根据价格查询库存
-- Gac2Gas.QueryGamePlayPaiMaiItemByPrice
function Gas2Gac.SendGamePlayPaiMaiItemInfoByPrice(itemTemplateId, oneShootPrice, currentLowestPrice, takeoffPrice, price, hasCount, extraValue)
    LuaAuctionMgr:SendGamePlayPaiMaiItemInfoByPrice(itemTemplateId, oneShootPrice, currentLowestPrice, takeoffPrice, price, hasCount, extraValue)
end

-- 玩法拍卖查询战利品界面用到的数据
-- Gac2Gas.QueryGamePlayPaiMaiHistory
-- resultTblUd 格式
-- [{itemTemplateId, itemCount, type}, {}, ....]
-- type: 1帮会，2全服，3个人（type为3的时候会多传一个购买者的名字）
-- {itemTemplateId, itemCount, type, buyername}
function Gas2Gac.QueryGamePlayPaiMaiHistoryResult(playId, pageNum, totalCount, resultTblUd)
    local ResultTbl = g_MessagePack.unpack(resultTblUd)
    g_ScriptEvent:BroadcastInLua("OnQueryGamePlayPaiMaiHistoryResult", pageNum, totalCount, ResultTbl)
end

function Gas2Gac.SendQieXianGuideScenePlayerCount(playerCount)
    CLuaGuideMgr.SendQieXianGuideScenePlayerCount(playerCount)
end

function Gas2Gac.StartBlackScreenFadeIn(duration)
    LuaBlackScreenMgr:StartFadeIn(duration)
end

function Gas2Gac.StartBlackScreenFadeOut(duration)
    LuaBlackScreenMgr:StartFadeOut(duration)
end

function Gas2Gac.SectXiuxingUpdateChuansongmenHp(engineId, hp)
    print("SectXiuxingUpdateChuansongmenHp", engineId, hp)
    LuaZongMenMgr:SectXiuxingUpdateChuansongmenHp(engineId, hp)
end

function Gas2Gac.ShowLuoChaBossPlayResult(eResult, loseType, rewardTimes)
    if eResult == EnumPlayResult.eVictory then LuaLuoChaHaiShiResultWnd.s_Type = 0
    else LuaLuoChaHaiShiResultWnd.s_Type = loseType == 1 and 1 or 2 end
    local rewardItemId = LuoCha2023_Setting.GetData().RewardItemId
    if rewardTimes == 1 then LuaLuoChaHaiShiResultWnd.s_Rewards = {rewardItemId[0], rewardItemId[1]}
    else LuaLuoChaHaiShiResultWnd.s_Rewards = {rewardItemId[1]} end
    CUIManager.ShowUI(CLuaUIResources.LuoChaHaiShiResultWnd)
end

function Gas2Gac.QueryLuoChaHaiShi2023InfoResult(curRewardTimes, maxRewardTimes)
    LuaLuoChaHaiShiEnterWnd.s_CurRewardTimes = curRewardTimes
    LuaLuoChaHaiShiEnterWnd.s_MaxRewardTimes = maxRewardTimes
    CUIManager.ShowUI(CLuaUIResources.LuoChaHaiShiEnterWnd)
end

function Gas2Gac.UpdateXiuxingDoorAndTime(totalDoors, destroyedDoors, finishTime)
    print("UpdateXiuxingDoorAndTime", totalDoors, destroyedDoors, finishTime)
    LuaZongMenMgr:UpdateXiuxingDoorAndTime(totalDoors, destroyedDoors, finishTime)
end

function Gas2Gac.GuoQing2023ShowMessage(force, msg)
    g_ScriptEvent:BroadcastInLua("GuoQing2023ShowMessage", force, msg)
end

-- 给自己的礼物获取卡片成功
-- Gac2Gas.Christmas2023CarftCard
function Gas2Gac.Christmas2023GetCardSuccess(cardId, isFirstGet)
    LuaChristmas2023Mgr:Christmas2023GetCardSuccess(cardId, isFirstGet)
end

-- 给自己的礼物获取礼物成功
-- Gac2Gas.Christmas2023GetCardGift
function Gas2Gac.Christmas2023GetCardGiftSuccess(cardId)
    LuaChristmas2023Mgr:Christmas2023GetCardGiftSuccess(cardId)
end

-- 冰雪大世界同步数据
function Gas2Gac.SyncBingXuePlay2023PlayData(stage, bossHp, Score, leftPickTime, bossFullHp)
    LuaChristmas2023Mgr:SyncBingXuePlay2023PlayData(stage, bossHp, Score, leftPickTime, bossFullHp)
end

-- 冰雪大世界开始采集模式
function Gas2Gac.StartBingXuePlay2023PickPart()
    LuaChristmas2023Mgr:StartBingXuePlay2023PickPart()
end

-- 冰雪大世界进入结算环节
function Gas2Gac.Christmas2023BingXuePlayAward(currentScore, isaward)
    LuaChristmas2023Mgr:Christmas2023BingXuePlayAward(currentScore, isaward)
end

-- 冰雪大世界开始选人
function Gas2Gac.Christmas2023BingXuePlayChooseCircle()
    LuaChristmas2023Mgr:Christmas2023BingXuePlayChooseCircle()
end

-- 刮刮卡数据查询结果
-- Gac2Gas.Christmas2023RequestGuaGuaKa
function Gas2Gac.Christmas2023GuaGuaKaResult(upPattern, snowIds_U, rewardTbl_U, playerGetItemIds_U)
    LuaChristmas2023Mgr:Christmas2023GuaGuaKaResult(upPattern, snowIds_U, rewardTbl_U, playerGetItemIds_U)
end

function Gas2Gac.SendPlayerCreditScoreData(targetPlayerId, creditScoreDataUD)
    LuaCreditScoreMgr:SyncSingleCreditScoreData(targetPlayerId, creditScoreDataUD)
end

function Gas2Gac.SyncCandyDeliverySignUpResult(isSignUp)
	g_ScriptEvent:BroadcastInLua("SyncCandyDeliverySignUpResult", isSignUp)
end

function Gas2Gac.QueryPlayerIsMatchingCandyDelivery_Result(isMatching)
	g_ScriptEvent:BroadcastInLua("QueryPlayerIsMatchingCandyDelivery_Result", isMatching)
end

function Gas2Gac.SyncCandyDeliveryCampInfo(isAll, campInfo)
	LuaHalloween2023Mgr:SyncCandyDeliveryCampInfo(isAll, campInfo)
end

function Gas2Gac.SendCandyDeliveryResultInfo(resultInfo)
    LuaHalloween2023Mgr:SendCandyDeliveryResultInfo(resultInfo)
end

function Gas2Gac.SendCandyDeliveryCountDown(countDown)
    LuaHalloween2023Mgr:SendCandyDeliveryCountDown(countDown)
end

function Gas2Gac.ShowCandyDeliverySummonFx(fromEngineId, toEngineId)
    LuaHalloween2023Mgr:ShowCandyDeliverySummonFx(fromEngineId, toEngineId)
end

function Gas2Gac.SyncCandyDeliveryActionType(engineId, actionType)
    LuaHalloween2023Mgr:SyncCandyDeliveryActionType(engineId, actionType)
end

function Gas2Gac.SendCandyDeliveryTeamInfo(driverId, passengerId)
    LuaHalloween2023Mgr:SyncMyTeamRoleInfo(driverId, passengerId)
end

function Gas2Gac.UnlockPermanentProfileFrameDuplicate(place, pos, itemId, frameId)
	print("UnlockPermanentProfileFrameDuplicate", place, pos, itemId, frameId)
end

function Gas2Gac.StartSkillTraining(trainId)
    g_ScriptEvent:BroadcastInLua("StartSkillTraining", trainId)
    LuaSkillBeginnerGuideWnd.startSkillTraining = true
end 

function Gas2Gac.SetSkillTrainingActiveSkills(skillCls1, skillCls2, skillCls3, skillCls4, skillCls5)
    g_ScriptEvent:BroadcastInLua("UpdateSkillBeginnerGuideSkillBoard", skillCls1, skillCls2, skillCls3, skillCls4, skillCls5)
end 

function Gas2Gac.SyncDoubleDragonPlayerBegin(memberInfo)
end

function Gas2Gac.SyncDoubleDragonUseGouzi(playerid, direction)
end

function Gas2Gac.SyncDoubleDragonGameEnd()
end

function Gas2Gac.SyncDoubleDragonExit(playerid)
end

function Gas2Gac.SyncDoubleDragonGetLongzhu(playerid, LongZhuId)
end

function Gas2Gac.SyncDoubleDragonPickPos(playerid, pos)
end

function Gas2Gac.SyncDoubleDragonSyncScore(playerid, score)
end

function Gas2Gac.SyncDoubleDragonLongZhuInfo(u)
end

function Gas2Gac.StartTextBubblePlay(playId)
    --print("StartTextBubblePlay", playId)
    LuaZhuJueJuQingMgr:ShowBubblePlayWnd(playId)
end

function Gas2Gac.StartShowZiMu(id, duration)
    --print("StartShowZiMu", id, duration)
    LuaZhuJueJuQingMgr:ShowNvZhanKuangZiMu(id,duration)
end

-- 送穷神活动信息
function Gas2Gac.QuerySongQiongShen2024InfoResult(rewardTimes, maxRewardTimes, firstPassTimes)
    LuaChunJie2024Mgr:QuerySongQiongShen2024InfoResult(rewardTimes, maxRewardTimes, firstPassTimes)
end

-- 同步送穷神红包选择
function Gas2Gac.SyncSongQiongShenChooseRewardIndex(rewardChooseInfo_U)
    LuaChunJie2024Mgr:SyncSongQiongShenChooseRewardIndex(rewardChooseInfo_U)
end

function Gas2Gac.SendYuanDanJinChuiReward(rewards)
    g_ScriptEvent:BroadcastInLua("SendYuanDanJinChuiReward", rewards)
end

function Gas2Gac.QueryPlayerForWuZiQiResult(playId, result_U, context)
    LuaShiTuMgr:QueryPlayerForWuZiQiResult(playId, result_U, context)
end

function Gas2Gac.ShowActivityAlert(activityId, bShow, dataU)
    print("ShowActivityAlert", activityId, bShow)
    if bShow then
        local tb = g_MessagePack.unpack(dataU)
        for k,v in pairs(tb) do
            print(k,v)
        end
        local data = {}
        data.Time = tb.timeStamp
        data.State = tb.status--1预热/2开启
        print(data.Time,data.State)
        LuaActivityAlertMgr:SetAlertActivity(activityId,bShow,data)
    end
end

function Gas2Gac.HanJia2024InvertedWorld_SyncNpcPos(dataU)
    local data = g_MessagePack.unpack(dataU)
    print("HanJia2024InvertedWorld_SyncNpcPos", dataU,data)
    LuaHanJia2024Mgr:SyncMiniMapPos(data,true)
end

function Gas2Gac.HanJia2024InvertedWorld_SyncMonsterPos(dataU)
    local data = g_MessagePack.unpack(dataU)
    print("HanJia2024InvertedWorld_SyncMonsterPos", dataU,data)
    LuaHanJia2024Mgr:SyncMiniMapPos(data,false)
end

function Gas2Gac.HanJia2024InvertedWorld_SyncPlayStatus(floor, count, total)
    LuaHanJia2024Mgr:SyncUpDownWorldBattleInfo(floor, count, total)
    print("HanJia2024InvertedWorld_SyncPlayStatus", floor, count, total)
    
end

function Gas2Gac.HanJia2024InvertedWorld_SyncYuanQiValue(value)
    print("HanJia2024InvertedWorld_SyncYuanQiValue", value)
    LuaHanJia2024Mgr:SyncUpDownWorldBattleScore(value)
end

function Gas2Gac.HanJia2024InvertedWorld_SyncPlayOpenInfo(bStart, endTime)
    print("HanJia2024InvertedWorld_SyncPlayOpenInfo", bStart, endTime)
    --打开颠倒世界界面
    LuaHanJia2024Mgr:OpenUpDownWorldEnterWndWithData(bStart, endTime)
end

function Gas2Gac.HanJia2024InvertedWorld_SyncEntrustMissionData(missionDataU)
    local data = g_MessagePack.unpack(missionDataU)
    print("HanJia2024InvertedWorld_SyncEntrustMissionData", data)
    LuaHanJia2024Mgr:SyncUpDownWorldTaskData(data)
end

function Gas2Gac.HanJia2024InvertedWorld_SyncNormalMissionData(weeklyMissionU, dailyMissionU)
    print("HanJia2024InvertedWorld_SyncNormalMissionData", weeklyMissionU, dailyMissionU)
    local wm = g_MessagePack.unpack(weeklyMissionU)
    local dm = g_MessagePack.unpack(dailyMissionU)
    LuaHanJia2024Mgr:SyncUpDownWorldNmlTaskData(wm,dm)

end

function Gas2Gac.HanJia2024InvertedWorld_ShowUIFx(UIType)
    print("HanJia2024InvertedWorld_ShowUIFx", UIType)
    LuaHanJia2024Mgr:ShowUnDownWorldFx(UIType)
end

function Gas2Gac.SyncShuangLongQiangZhuApplyResult(bMatching, leftTimes, maxTimes)
    LuaChunJie2024Mgr:SyncShuangLongQiangZhuApplyResult(bMatching, leftTimes, maxTimes)
end

function Gas2Gac.SyncShuangLongQiangZhuEvent(event, context)
    LuaChunJie2024Mgr:SyncShuangLongQiangZhuEvent(event, context)
end

function Gas2Gac.SyncShuangLongQiangZhuChooseRewardIndex(chooseInfo_U)
    LuaChunJie2024Mgr:SyncShuangLongQiangZhuChooseRewardIndex(chooseInfo_U)
end

-- {[pos] = {role, name, class, gender}}
function Gas2Gac.SyncShuangLongQiangZhuPlayerPos(playePos_u)
    LuaChunJie2024Mgr:SyncShuangLongQiangZhuPlayerPos(playePos_u)
end

function Gas2Gac.HanJia2024FurySnowman_SyncPlayResult(isHardMode, isWin, hasRewarded)
    print("HanJia2024FurySnowman_SyncPlayResult", isHardMode, isWin, hasRewarded)
end

-- 女战狂主角剧情开始学武玩法
function Gas2Gac.StartJuQingLearnKungFuPlay(id)
    LuaZhuJueJuQingMgr:ShowLearnSkillPlayWnd(id)
end

function Gas2Gac.SyncYuanDanXNCGAreaInfo(areaInfo)
    g_ScriptEvent:BroadcastInLua("SyncYuanDanXNCGAreaInfo", areaInfo)
end

function Gas2Gac.SyncSpecialConcernCandidatePlayers(playersTblU)
    g_ScriptEvent:BroadcastInLua("SyncSpecialConcernCandidatePlayers", playersTblU)
end

function Gas2Gac.ExtraEquipMarketingGetScoreAwardSuccess()
    print("ExtraEquipMarketingGetScoreAwardSuccess")
end

function Gas2Gac.UpdateVNClientAwardStatus(actionType, status)
    g_ScriptEvent:BroadcastInLua("UpdateVNClientAwardStatus", actionType, status)
    LuaSEASdkMgr:OnSyncOneVNClientAwardResult(actionType, status)
end

function Gas2Gac.QueryVNClientAwardResult(data_U)
    g_ScriptEvent:BroadcastInLua("SyncVNClientAwardResult", data_U)
end

function Gas2Gac.AppsflyerNotify(event)
    LuaSEASdkMgr:AppsflyerNotify(event, SafeStringFormat3("{}"))
end