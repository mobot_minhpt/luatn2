local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"

LuaGuildLeagueCrossServerGuildInfoWnd = class()
RegistClassMember(LuaGuildLeagueCrossServerGuildInfoWnd, "m_TitleLabel")
RegistClassMember(LuaGuildLeagueCrossServerGuildInfoWnd, "m_GuildTemplate")
RegistClassMember(LuaGuildLeagueCrossServerGuildInfoWnd, "m_ScrollView")
RegistClassMember(LuaGuildLeagueCrossServerGuildInfoWnd, "m_Table")

function LuaGuildLeagueCrossServerGuildInfoWnd:Awake()
    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_GuildTemplate = self.transform:Find("Anchor/ScrollView/Item").gameObject
    self.m_ScrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_GuildTemplate:SetActive(false)
end

function LuaGuildLeagueCrossServerGuildInfoWnd:Init()
    self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("%s代表队"), LuaGuildLeagueCrossMgr.m_ServerGuildInfo.serverName)
    local data = LuaGuildLeagueCrossMgr.m_ServerGuildInfo.detailInfo
    Extensions.RemoveAllChildren(self.m_Table.transform)

    for i=1, #data do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_GuildTemplate)
        child:SetActive(true)
        local info = data[i]
        self:InitOneGuildInfo(child.transform, info.guildName, info.ownerName, info.professionIcon, info.memberCount, info.maxMemberCount, info.rankStr, info.score)
        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaGuildLeagueCrossServerGuildInfoWnd:InitOneGuildInfo(transRoot, guildName, ownerName, professionIcon, memberCount, maxMemberCount, rankStr, score)
    transRoot:Find("GuildNameLabel"):GetComponent(typeof(UILabel)).text = guildName
    transRoot:Find("OwnerNameLabel"):GetComponent(typeof(UILabel)).text = ownerName
    transRoot:Find("OwnerNameLabel/ProfessionIcon"):GetComponent(typeof(UISprite)).spriteName = professionIcon
    transRoot:Find("MemberCountLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d", memberCount, maxMemberCount)
    transRoot:Find("RankLabel"):GetComponent(typeof(UILabel)).text = rankStr
    transRoot:Find("EquipScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(score)
end

