local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemAccessBaseInfo = import "L10.UI.CItemAccessBaseInfo"
local CommonDefs = import "L10.Game.CommonDefs"
local CNPCShopInfoMgr = import "L10.UI.CNPCShopInfoMgr"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CCommonItemExchangeMgr = import "L10.UI.CCommonItemExchangeMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaItemAccessListMgr = {}

function LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(templateId, isEnough, targetTrans)
    CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, isEnough, targetTrans, CTooltipAlignType.Left)
end

function LuaItemAccessListMgr:ShowItemAccessInfoAtRight(templateId, isEnough, targetTrans)
    CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, isEnough, targetTrans, CTooltipAlignType.Right)
end

local AddScoreShopItemAccess = function(key,  accessName, btnName, scoreKey)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        local data = ItemGet_item.GetData(CItemAccessListMgr.Inst.ItemTemplateId)
        if data then
            CNPCShopInfoMgr.selectItemTemplateId = data.NpcShopItem
            CLuaNPCShopInfoMgr.ShowScoreShop(scoreKey, CItemAccessListMgr.Inst.ItemTemplateId) --综合了C#下的OnOpenNpcShop以及 CLuaNPCShopInfoMgr.ShowScoreShop对原代码逻辑进行微调
        end
    end)))
end

local AddFurnitureCItemBuyAccess = function(key, accessName, btnName)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        --local data = ItemGet_item.GetData(CItemAccessListMgr.Inst.ItemTemplateId)
        Gac2Gas.OpenRequestBuyWindow(CItemAccessListMgr.Inst.ItemTemplateId)
    end)))
end

local AddSkillItemCompoundAccess = function(key, accessName, btnName)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        LuaCompound125SkillMgr:ShowCompound125SkillItem(CItemAccessListMgr.Inst.ItemTemplateId)
    end)))
end

local AddSkillBookCompoundAccess = function(key, accessName, btnName)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        CUIManager.ShowUI("Exchange125SkillBookWnd")
    end)))
end

local AddSkillLianHuaAccess = function(key, accessName, btnName)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        LuaZongMenMgr:FindLianYaoFaZheng()
        if CUIManager.instance then
            CUIManager.instance:CloseAllPopupViews()
        end
    end)))
end

local AddHousePlantItemAccess = function(key, accessName, btnName)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        CClientHouseMgr.Inst:GoBackHome()
    end)))
end

local AddComposeExchangeAccess = function(key, accessName, btnName, teamplateId, msg)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        local itemCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, teamplateId)
        if itemCount >0 then
            local can, key, wndTitle = g_ComposeMgr.CheckCanCompose(teamplateId)
            CCommonItemExchangeMgr.ShowItemExchangeWnd(teamplateId, key, wndTitle)
        else
            g_MessageMgr:ShowMessage(msg)   
        end
    end)))
end

local AddOpenWndItemAccess = function(key, accessName, btnName, wndName)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        if CLuaUIResources[wndName] then
            CUIManager.ShowUI(CLuaUIResources[wndName])
        end
        CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
    end)))
end

local DoActionItemAccess = function(key, accessName, btnName, action)
    CommonDefs.DictAdd_LuaCall(CItemAccessListMgr.AccessDic, key, CItemAccessBaseInfo(accessName, btnName, DelegateFactory.Action(function()
        action()
    end)))
end
--------
-- 新增获取途径的同学请留意！！！
-- 新增积分商店类的获取途径，可以使用通用的AddScoreShopItemAccess，无需新增自定义的方法
-- 注意顺序编号，不要重复，不要间断，EnumItemAccessType无需再新增内容
--------
--15之前的编号内容位于CItemAccessListWnd.cs
AddHousePlantItemAccess(15, LocalString.GetString("家园种植"), LocalString.GetString("回家"))
--16-21的编号内容位于CItemAccessListWnd.cs
AddScoreShopItemAccess(22, LocalString.GetString("师门贡献度商店"), LocalString.GetString("打开"),"SchoolContribution")
AddScoreShopItemAccess(23, LocalString.GetString("盘谷青阳积分商店"), LocalString.GetString("打开"),"PGQY")
AddScoreShopItemAccess(24, LocalString.GetString("家园图纸积分商店"), LocalString.GetString("打开"),"JiaYuanTuZhi")
AddScoreShopItemAccess(25, LocalString.GetString("家园特殊图纸积分商店"), LocalString.GetString("打开"),"JiaYuanTuZhiSpecialScore")
AddScoreShopItemAccess(26, LocalString.GetString("当铺积分商店"), LocalString.GetString("打开"),"GlobalSpokesmanScore")
AddScoreShopItemAccess(27, LocalString.GetString("海棠花积分商店"), LocalString.GetString("打开"),"SpokesmanHaiTangHua")
AddFurnitureCItemBuyAccess(28, LocalString.GetString("装饰物求购"), LocalString.GetString("打开"))
AddScoreShopItemAccess(29, LocalString.GetString("元宵积分商店"), LocalString.GetString("打开"),"TangYuan2021")
AddSkillItemCompoundAccess(30, LocalString.GetString("蕴灵瓶注灵"), LocalString.GetString("注灵"))
AddSkillBookCompoundAccess(31, LocalString.GetString("技能书合成"), LocalString.GetString("合成"))
AddSkillLianHuaAccess(32, LocalString.GetString("炼化获得"), LocalString.GetString("寻路"))
AddScoreShopItemAccess(33, LocalString.GetString("海钓积分商店"), LocalString.GetString("打开"),"FishingScore")
AddScoreShopItemAccess(34, LocalString.GetString("怨憎洞商店"), LocalString.GetString("打开"),"JXYSScore")
AddComposeExchangeAccess(35, LocalString.GetString("红冰合成"), LocalString.GetString("打开"), 21006331, "ChiJiaoLing_Use_Tip")
--36编号内容位于CItemAccessListWnd.cs 官服自定义消息, 用于做一些官服有,渠道服没有的获得途径
AddScoreShopItemAccess(37, LocalString.GetString("寒假积分商店"), LocalString.GetString("打开"), "HanJiaScore")
DoActionItemAccess(38, LocalString.GetString("使用灵玉兑换"), LocalString.GetString("查看"), function ()
    LuaWuYi2023Mgr.m_IsExchangeGold = true
    CUIManager.ShowUI(CLuaUIResources.WuYi2023XXSCExchangeGoldWnd)
end)
AddOpenWndItemAccess(39, LocalString.GetString("参加君子六艺活动"), LocalString.GetString("查看"), "WuYi2023JZLYWnd")
AddOpenWndItemAccess(40, LocalString.GetString("使用元宝兑换"), LocalString.GetString("查看"), "WuYi2023XXSCExchangeGoldWnd")
AddScoreShopItemAccess(41, LocalString.GetString("外观积分商店"), LocalString.GetString("打开"),"AppearanceScore")