local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUIFx = import "L10.UI.CUIFx"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaDoubleOneBonusLotteryWnd = class()
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"node1", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"node2", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"node3", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"node4", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"text1", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"text2", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"checkBtn", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"goBuyBtn", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"tipBtn", GameObject)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"joinText", UILabel)
RegistChildComponent(LuaDoubleOneBonusLotteryWnd,"fx", CUIFx)

--RegistClassMember(LuaDoubleOneBonusLotteryWnd, "maxChooseNum")

function LuaDoubleOneBonusLotteryWnd:Close()
	CUIManager.CloseUI("DoubleOneBonusLotteryWnd")
end

function LuaDoubleOneBonusLotteryWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaDoubleOneBonusLotteryWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaDoubleOneBonusLotteryWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onCheckClick = function(go)
		Gac2Gas.QuerySinglesDayLotteryResult(0)
	end
	CommonDefs.AddOnClickListener(self.checkBtn,DelegateFactory.Action_GameObject(onCheckClick),false)

	local onBuyClick = function(go)
    CShopMallMgr.ShowLinyuShoppingMall(0)
	end
	CommonDefs.AddOnClickListener(self.goBuyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)

	local onTipClick = function(go)
		g_MessageMgr:ShowMessage('DoubleOneDailyLottery')
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

	--self.joinText.text = LuaDoubleOne2019Mgr.LotteryTotalJoinCount
	self.joinText.text = ''
	if self:CheckTodayBuy() then
		self.text1:SetActive(true)
		self.text2:SetActive(false)
	else
		self.text1:SetActive(false)
		self.text2:SetActive(true)
	end

	self.fx:LoadFx("Fx/UI/Prefab/UI_meirichoujiang_hudie.prefab")

	self:InitBonusInfo()
end

function LuaDoubleOneBonusLotteryWnd:CheckTodayBuy()
	local checkTime = 24*60*60
	local nowTime = CServerTimeMgr.Inst.timeStamp
	if LuaDoubleOne2019Mgr.LotterySelfTimeInfo then
		for i,v in pairs(LuaDoubleOne2019Mgr.LotterySelfTimeInfo) do
			if nowTime - v < checkTime then
				return true
			end
		end
	end
	return false
end

function LuaDoubleOneBonusLotteryWnd:InitBonusInfo()
	local nodeTable = {self.node1,self.node2,self.node3,self.node4}
	local nodeRankCount = {
		SinglesDay2019_LotterSetting.GetData().RankCount1,
		SinglesDay2019_LotterSetting.GetData().RankCount2,
		SinglesDay2019_LotterSetting.GetData().RankCount3,
		SinglesDay2019_LotterSetting.GetData().RankCount4}
	local nodeRankPrize = {
		SinglesDay2019_LotterSetting.GetData().Rank1Prize,
		SinglesDay2019_LotterSetting.GetData().Rank2Prize,
		SinglesDay2019_LotterSetting.GetData().Rank3Prize,
		SinglesDay2019_LotterSetting.GetData().Rank4Prize}

	for i,v in pairs(nodeTable) do
		if i <= 3 then
			v.transform:Find('count'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('全服抽%s名'),nodeRankCount[i])
			local strs = g_LuaUtil:StrSplit(nodeRankPrize[i],",")
			v.transform:Find('num'):GetComponent(typeof(UILabel)).text = strs[2]
			v.transform:Find('fx'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_meirichoujiang_libao.prefab")
		else
			v.transform:Find('count'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('全服抽%s名'),nodeRankCount[i])
			local strs = g_LuaUtil:StrSplit(nodeRankPrize[i],",")
			local itemTemplateId = tonumber(strs[1])
			v.transform:Find('gift'):GetComponent(typeof(CFirstChargeGiftCell)):Init(itemTemplateId,1)
			v.transform:Find('fx'):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_meirichoujiang_liuguang.prefab")
		end
	end
end

return LuaDoubleOneBonusLotteryWnd
