local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UIPanel = import "UIPanel"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaShanYeMiZongRewardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShanYeMiZongRewardWnd, "ItemTable", "ItemTable", UITable)
RegistChildComponent(LuaShanYeMiZongRewardWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaShanYeMiZongRewardWnd, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongRewardWnd, "OkButton", "OkButton", QnButton)
RegistChildComponent(LuaShanYeMiZongRewardWnd, "CollectedHint", "CollectedHint", GameObject)
RegistChildComponent(LuaShanYeMiZongRewardWnd, "ExtraLabel", "ExtraLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaShanYeMiZongRewardWnd, "m_Panel")

function LuaShanYeMiZongRewardWnd:Awake()
    self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    self.ItemTemplate:SetActive(false)
    --self.m_Panel.alpha = 0

    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShanYeMiZongRewardWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaShanYeMiZongRewardWnd:Init()
    self.OkButton.gameObject:SetActive(false)
    self.CollectedHint:SetActive(false)
    self.ContentLabel.gameObject:SetActive(false)
    self.ExtraLabel.gameObject:SetActive(false)
    Gac2Gas.ShanYeMiZongRequestFinalRewardInfo()
end

function LuaShanYeMiZongRewardWnd:OnEnable()
    g_ScriptEvent:AddListener("SendShanYeMiZongInfo", self, "RefreshAll")
    g_ScriptEvent:AddListener("SendShanYeMiZongFinalReward", self, "OnSendShanYeMiZongFinalReward")
end

function LuaShanYeMiZongRewardWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendShanYeMiZongInfo", self, "RefreshAll")
    g_ScriptEvent:RemoveListener("SendShanYeMiZongFinalReward", self, "OnSendShanYeMiZongFinalReward")
end

--@region UIEvent

function LuaShanYeMiZongRewardWnd:OnOkButtonClick()
    Gac2Gas.ShanYeMiZongRequestGetFinalReward()
end


--@endregion UIEvent

-- 领取成功后会刷新主界面
function LuaShanYeMiZongRewardWnd:RefreshAll(finalRewarded)
    if finalRewarded then
        self.OkButton.gameObject:SetActive(false)
        self.CollectedHint:SetActive(true)
    else
        self.OkButton.gameObject:SetActive(true)
        self.CollectedHint:SetActive(false)
    end
end

-- 参数说明：是否领取奖励；自己是否猜对真凶；是否3个猜想全都猜对；全服是否有超过50%的人猜对真凶
function LuaShanYeMiZongRewardWnd:OnSendShanYeMiZongFinalReward(rewarded, rightMurderer, correctGuess, serverRightMurderer, items) 
    if rightMurderer then
        if correctGuess then
            self.ContentLabel.text = CUICommonDef.TranslateToNGUIText(ShuJia2022_Setting.GetData().CaiXiangALLRight)
        else
            self.ContentLabel.text = CUICommonDef.TranslateToNGUIText(ShuJia2022_Setting.GetData().CaiXiangRight)
        end
    else
        self.ContentLabel.text = CUICommonDef.TranslateToNGUIText(ShuJia2022_Setting.GetData().CaiXiangWrong)
    end

    if serverRightMurderer then
        self.ExtraLabel.text = CUICommonDef.TranslateToNGUIText(ShuJia2022_Setting.GetData().QuanfuCaiXiangRight)
    else
        self.ExtraLabel.text = ""
    end
    self.ContentLabel.gameObject:SetActive(true)
    self.ExtraLabel.gameObject:SetActive(true)
    
    Extensions.RemoveAllChildren(self.ItemTable.transform)
    if items then
        for id, cnt in pairs(items) do
            local curItem = CUICommonDef.AddChild(self.ItemTable.gameObject, self.ItemTemplate)
            curItem:SetActive(true)
            self:InitOneItem(curItem, id)
        end
    end
    self.ItemTable:Reposition()

    if rewarded then
        self.OkButton.gameObject:SetActive(false)
        self.CollectedHint:SetActive(true)
    else
        self.OkButton.gameObject:SetActive(true)
        self.CollectedHint:SetActive(false)
    end
    --self.m_Panel.alpha = 1
end
