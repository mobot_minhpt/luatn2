
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaZhiBoScreenLookWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhiBoScreenLookWnd, "Btn", "Btn", GameObject)
RegistChildComponent(LuaZhiBoScreenLookWnd, "CloseBtn", "CloseBtn", GameObject)

--@endregion RegistChildComponent end

function LuaZhiBoScreenLookWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)

	UIEventListener.Get(self.CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)

    --@endregion EventBind end
end

function LuaZhiBoScreenLookWnd:Init()

end

function LuaZhiBoScreenLookWnd:OnEnable()
	self.Btn:SetActive(true)
	self.CloseBtn:SetActive(false)
end

--@region UIEvent

function LuaZhiBoScreenLookWnd:OnBtnClick()
	LuaZhouNianQing2021Mgr.SetUpCamera()
	self.CloseBtn:SetActive(true)
	self.Btn:SetActive(false)
end

function LuaZhiBoScreenLookWnd:OnCloseBtnClick()
	LuaZhouNianQing2021Mgr.ExitCameraLock()
	CUIManager.CloseUI(CLuaUIResources.ZhiBoScreenLookWnd)
end


--@endregion UIEvent

