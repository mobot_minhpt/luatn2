local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UISlider = import "UISlider"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Vector3 = import "UnityEngine.Vector3"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local CServerTimeMgr= import "L10.Game.CServerTimeMgr"
local CGuildSortButton = import "L10.UI.CGuildSortButton"

LuaJuDianBattleSelfGuildInfoRoot = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "Header", "Header", QnRadioBox)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "BuffRoot", "BuffRoot", GameObject)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "OccupyCountLabel", "OccupyCountLabel", UILabel)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "OccupyRankLabel", "OccupyRankLabel", UILabel)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "OccupyPeopleLabel", "OccupyPeopleLabel", UILabel)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "ZhanYiIcon", "ZhanYiIcon", CUITexture)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "ZhanYiLevelLabel", "ZhanYiLevelLabel", UILabel)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "KillCountLevel", "KillCountLevel", UILabel)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "ZhanYiProgress", "ZhanYiProgress", UISlider)
RegistChildComponent(LuaJuDianBattleSelfGuildInfoRoot, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaJuDianBattleSelfGuildInfoRoot, "m_DataList")
RegistClassMember(LuaJuDianBattleSelfGuildInfoRoot, "m_IsLeader")

function LuaJuDianBattleSelfGuildInfoRoot:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleSelfGuildInfoRoot:InitWnd()
	self.m_DataList = {}
	self.m_IsLeader = false
	self.m_PropertyCount = 10

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,

        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
            self:InitItem(item, index, self.m_DataList[index+1])
        end
    )

	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnItemClick(row)
	end)

	self.Header.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        self:OnHeaderSelect(btn, index)
    end)
end

function LuaJuDianBattleSelfGuildInfoRoot:InitBuffItem(item, buffData)
	local skillIcon = item.transform:Find("Mask/SkillIcon"):GetComponent(typeof(CUITexture))

	local data = Buff_Buff.GetData(buffData.BuffId)
    if data ~= nil then
        skillIcon:LoadMaterial(data.Icon)
    end

	if buffData.Enable then
		CUICommonDef.SetActive(skillIcon.gameObject, true, true)
	else
		CUICommonDef.SetActive(skillIcon.gameObject, false, false)
	end
	-- 点击事件
	UIEventListener.Get(skillIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
		-- buff描述
		local extraInfo = {}
		if buffData.Enable then
			local time = buffData.EndTime - CServerTimeMgr.Inst.timeStamp
			local mins = math.floor(time/60)
    		local secs = math.floor(time% 60)
			local desc = SafeStringFormat3(LocalString.GetString("剩余时间：%02d:%02d"), mins, secs)

			table.insert(extraInfo, desc)
		end
		LuaBuffInfoWndMgr:ShowWnd(buffData.BuffId, buffData.Enable, extraInfo)
	end)
end

function LuaJuDianBattleSelfGuildInfoRoot:InitItem(item, index, data)
	local prefix = ""

	if data.PlayerId and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == data.PlayerId then
		item.transform:Find("My").gameObject:SetActive(true)
		prefix = "[00FF60]"
	else
		item.transform:Find("My").gameObject:SetActive(false)
	end
	local name = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local icon = item.transform:Find("Icon"):GetComponent(typeof(UISprite))
	icon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.Class))
	name.text = prefix .. data.Name

	for i = 1, self.m_PropertyCount do
		local label = item.transform:Find(tostring(i)):GetComponent(typeof(UILabel))
		label.text = prefix .. self:ConvertValue(i, data[i])
	end
end

function LuaJuDianBattleSelfGuildInfoRoot:OnItemClick(row)
	print("OnItemClick")
	local data =  self.m_DataList[row+1]
	if data.PlayerId then
		print(data.PlayerId)
		local id = CClientMainPlayer.Inst.Id
		if id == data.PlayerId then
			print("QueryInteractMenuData")
			Gac2Gas2.QueryInteractMenuData(data.PlayerId, 0)	
		else
			CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
		end
	end
end

function LuaJuDianBattleSelfGuildInfoRoot:ConvertValue(i, count)
	if i ~= self.m_PropertyCount and count > 10000 then
		return math.floor(count/10000) .. LocalString.GetString("万")
	end
	return tostring(count)
end

function LuaJuDianBattleSelfGuildInfoRoot:OnHeaderSelect(btn, index)
	local sortButton = TypeAs(btn, typeof(CGuildSortButton))
	sortButton:SetSortTipStatus(false)
	self:SortListByType(index + 1)
end

-- 重新排序
function LuaJuDianBattleSelfGuildInfoRoot:SortListByType(type)
	table.sort(self.m_DataList, function(a, b)
		local selfId = 0

		if CClientMainPlayer.Inst then
			selfId = CClientMainPlayer.Inst.Id
		end

		if a.PlayerId == b.PlayerId then
			return false
		end

		if a.PlayerId == selfId then
			return true
		elseif b.PlayerId == selfId then
			return false
		end

		local av = a[type]
		local bv = b[type]

		if av ~= bv then
			return av>bv
		else
			return a.PlayerId > b.PlayerId
		end
	end)
	-- 刷新数据
	self.TableView:ReloadData(true,false)
end

function LuaJuDianBattleSelfGuildInfoRoot:OnGuildDataEnd(score, rank, totalCount, fightLevel, killCount, playerCount, buffList)
	-- 占领信息
	print(score, rank, totalCount, fightLevel, killCount, playerCount, buffList)

	self.OccupyCountLabel.text = tostring(score)
	self.OccupyRankLabel.text = tostring(rank).."/" .. tostring(totalCount)
	self.OccupyPeopleLabel.text = tostring(playerCount)

	-- 战意信息
	self.ZhanYiLevelLabel.text = "Lv." .. tostring(fightLevel)

	local data = GuildOccupationWar_Setting.GetData().StageTotalKillNum
	local levelUpCount = 0
	if fightLevel < data.Length then
		levelUpCount = data[fightLevel]
	else
		levelUpCount = data[data.Length - 1]
	end
	self.KillCountLevel.text =  tostring(killCount) .."/".. levelUpCount
	self.ZhanYiProgress.value = killCount/levelUpCount

	if fightLevel <= 1 then
		self.ZhanYiIcon:LoadMaterial(JuDianZhanYiTexture.Level1)
	elseif fightLevel == 2 then
		self.ZhanYiIcon:LoadMaterial(JuDianZhanYiTexture.Level2)
	else		
		self.ZhanYiIcon:LoadMaterial(JuDianZhanYiTexture.Level3)
	end

	-- 标志变灰
	CUICommonDef.SetActive(self.ZhanYiIcon.gameObject, fightLevel >0, true)

	-- Buff
	for i=1, 3 do
		local item = self.BuffRoot.transform:Find(tostring(i)).gameObject
		local buffData = buffList[i]
		self:InitBuffItem(item, buffData)
	end

	-- 刷新数据
	self:SortListByType(1)
end

function LuaJuDianBattleSelfGuildInfoRoot:OnQualification(canTag, canKick)
	self.m_IsLeader = canKick
	LuaJuDianBattleMgr.NeedShowPlayerJuDianInfo = canKick
end

function LuaJuDianBattleSelfGuildInfoRoot:OnGuildData(dataList)
	for i=1, #dataList do
		table.insert(self.m_DataList, dataList[i])
	end
end

function LuaJuDianBattleSelfGuildInfoRoot:OnEnable()
    self:InitWnd()
    -- 请求本帮数据
	Gac2Gas.GuildJuDianQueryGuildMemberDetail()
	-- 请求是否有资格
	Gac2Gas.GuildJuDianQuerySetQualification()

	LuaJuDianBattleMgr.NeedShowPlayerJuDianInfo = false
    g_ScriptEvent:AddListener("GuildJuDianSyncGuildMemberDetail", self, "OnGuildData")
    g_ScriptEvent:AddListener("GuildJuDianSyncGuildMemberDetailEnd", self, "OnGuildDataEnd")
    g_ScriptEvent:AddListener("GuildJuDianSyncSetQualification", self, "OnQualification")
end

function LuaJuDianBattleSelfGuildInfoRoot:OnDisable()
	LuaJuDianBattleMgr.NeedShowPlayerJuDianInfo = false
    g_ScriptEvent:RemoveListener("GuildJuDianSyncGuildMemberDetail", self, "OnGuildData")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncGuildMemberDetailEnd", self, "OnGuildDataEnd")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncSetQualification", self, "OnQualification")
end

--@region UIEvent

function LuaJuDianBattleSelfGuildInfoRoot:OnTipBtnClick()
	g_MessageMgr:ShowMessage("JuDian_Self_Guild_Info_Desc")
end

--@endregion UIEvent

