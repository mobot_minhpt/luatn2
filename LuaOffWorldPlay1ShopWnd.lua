local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaOffWorldPlay1ShopWnd = class()

RegistChildComponent(LuaOffWorldPlay1ShopWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaOffWorldPlay1ShopWnd, "lockNode", "lockNode", GameObject)
RegistChildComponent(LuaOffWorldPlay1ShopWnd, "unlockNode", "unlockNode", GameObject)
RegistChildComponent(LuaOffWorldPlay1ShopWnd, "fx", "fx", CUIFx)
RegistChildComponent(LuaOffWorldPlay1ShopWnd, "have", "have", GameObject)
RegistChildComponent(LuaOffWorldPlay1ShopWnd, "yuanbaohave", "yuanbaohave", GameObject)

RegistClassMember(LuaOffWorldPlay1ShopWnd,"TotalResNum")
RegistClassMember(LuaOffWorldPlay1ShopWnd,"UseYuanBao")
RegistClassMember(LuaOffWorldPlay1ShopWnd,"IsLock")
RegistClassMember(LuaOffWorldPlay1ShopWnd,"MyTick")
RegistClassMember(LuaOffWorldPlay1ShopWnd,"MySharkTick")
RegistClassMember(LuaOffWorldPlay1ShopWnd,"itemNodeTable")

function LuaOffWorldPlay1ShopWnd:InitLock()
  self.lockNode:SetActive(true)
  self.unlockNode:SetActive(false)
  local percent = LuaOffWorldPassMgr.NowProgress / OffWorldPass_Setting.GetData().BloodShopUnlockRequire
  self.lockNode.transform:Find('percent'):GetComponent(typeof(UITexture)).fillAmount = percent
  self.lockNode.transform:Find('have/num'):GetComponent(typeof(UILabel)).text = self.TotalResNum
  local lockNum = OffWorldPass_Setting.GetData().BloodShopUnlockRequire - LuaOffWorldPassMgr.NowProgress
  if lockNum < 0 then
    lockNum = 0
  end
  self.lockNode.transform:Find('cost/num'):GetComponent(typeof(UILabel)).text = lockNum
  self.lockNode.transform:Find('num'):GetComponent(typeof(UILabel)).text = tostring(LuaOffWorldPassMgr.NowProgress).."/" .. tostring(OffWorldPass_Setting.GetData().BloodShopUnlockRequire)
  local onClick = function(go)
    Gac2Gas.RequestUnlockOffWorldBloodShop()
  end
  CommonDefs.AddOnClickListener(self.lockNode.transform:Find('btn').gameObject,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaOffWorldPlay1ShopWnd:InitUnlock()
  if LuaOffWorldPassMgr.ShopInfo == nil then
    return
  end
  self.IsLock = false

  for i=1,#self.itemNodeTable do
    self.itemNodeTable[i]:SetActive(false)
  end

  if self.MySharkTick then
    invoke(self.MySharkTick)
    self.MySharkTick = nil
  end

  self.MySharkTick = RegisterTickOnce(function()
    for i=1,#self.itemNodeTable do
      self.itemNodeTable[i]:SetActive(true)
    end
    self.MySharkTick = nil
  end, 27)

  self.lockNode:SetActive(false)
  self.unlockNode:SetActive(true)

  local yuanbaoSign = self.unlockNode.transform:Find('desc/right').gameObject
  yuanbaoSign:SetActive(self.UseYuanBao)
  local onChooseClick = function(go)
    if self.UseYuanBao then
      self.UseYuanBao = false
      self:SetYuanBaoMode()
      yuanbaoSign:SetActive(false)
    else
      self.UseYuanBao = true
      self:SetYuanBaoMode()
      yuanbaoSign:SetActive(true)
    end
  end
  CommonDefs.AddOnClickListener(self.unlockNode.transform:Find('desc/icon').gameObject,DelegateFactory.Action_GameObject(onChooseClick),false)

  UIEventListener.Get(self.unlockNode.transform:Find('btn').gameObject).onClick = DelegateFactory.VoidDelegate(function()
    Gac2Gas.RequestRefreshBloodShopItem(self.UseYuanBao)
  end)

  for i,v in pairs(self.itemNodeTable) do
    local data = LuaOffWorldPassMgr.ShopInfo[i]
    local shopData = OffWorldPass_BloodShop.GetData(data[1])

    local itemData = Item_Item.GetData(data[1])
    v.transform:Find('ItemCell/Icon'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
    v.transform:Find('ItemName'):GetComponent(typeof(UILabel)).text = itemData.Name

    local btn = v.transform:Find('btn').gameObject
    local btnText = v.transform:Find('btn/text'):GetComponent(typeof(UILabel))
    if data[2] ~= 1 then
      btnText.text = LocalString.GetString('购买')
      CUICommonDef.SetActive(btn, true, false)
    else
      btnText.text = LocalString.GetString('已购买')
      CUICommonDef.SetActive(btn, false, true)
    end

    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function()
      if shopData.blood ~= 0 and shopData.blood <= self.TotalResNum then
        Gac2Gas.RequestBuyBloodShopItem(i, data[1], false)
      elseif shopData.blood == 0 or self.UseYuanBao then
        Gac2Gas.RequestBuyBloodShopItem(i, data[1], true)
      else
        g_MessageMgr:ShowMessage("OFFWORLD_BLOODSHOP_BLOOD_NOT_ENOUGHT")
      end
    end)

    UIEventListener.Get(v.transform:Find('ItemCell').gameObject).onClick = DelegateFactory.VoidDelegate(function()
      CItemInfoMgr.ShowLinkItemTemplateInfo(data[1], false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    self:SetYuanBaoMode()
  end
end

function LuaOffWorldPlay1ShopWnd:Init()
  local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPlay1ShopWnd)
  end
  CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  local itemId = OffWorldPass_Setting.GetData().BloodTemplateId
	local bindCount, notbindCount
	bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local total = bindCount + notbindCount
  self.TotalResNum = total
  self.UseYuanBao = false

  self.itemNodeTable = {self.unlockNode.transform:Find('item1').gameObject,self.unlockNode.transform:Find('item2').gameObject,self.unlockNode.transform:Find('item3').gameObject}

  if LuaOffWorldPassMgr.NowProgress >= OffWorldPass_Setting.GetData().BloodShopUnlockRequire then
    self:InitUnlock()
    self.IsLock = false
  else
    self:InitLock()
    self.IsLock = true
  end
end

function LuaOffWorldPlay1ShopWnd:UpdateProgress()
  local itemId = OffWorldPass_Setting.GetData().BloodTemplateId
	local bindCount, notbindCount
	bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local total = bindCount + notbindCount
  self.TotalResNum = total

  if LuaOffWorldPassMgr.NowProgress >= OffWorldPass_Setting.GetData().BloodShopUnlockRequire then
    if self.IsLock then
      self.fx.gameObject:SetActive(true)
      self.lockNode.transform:Find('percent'):GetComponent(typeof(UITexture)).fillAmount = 1
      self.lockNode.transform:Find('num'):GetComponent(typeof(UILabel)).text = tostring(LuaOffWorldPassMgr.NowProgress).."/" .. tostring(OffWorldPass_Setting.GetData().BloodShopUnlockRequire)
      self.MyTick = RegisterTickOnce(function()
        self:InitUnlock()
        self.MyTick = nil
      end, 1500)
    else
      self:InitUnlock()
    end
  else
    self:InitLock()
  end
end

function LuaOffWorldPlay1ShopWnd:SetYuanBaoMode()
  local Blood2Yuanbao = OffWorldPass_Setting.GetData().Blood2Yuanbao

  if self.TotalResNum >= OffWorldPass_Setting.GetData().RefreshBloodShopBlood or not self.UseYuanBao then
    self.unlockNode.transform:Find('cost/icon01').gameObject:SetActive(true)
    self.unlockNode.transform:Find('cost/icon02').gameObject:SetActive(false)
    self.unlockNode.transform:Find('have/num'):GetComponent(typeof(UILabel)).text = self.TotalResNum
    self.unlockNode.transform:Find('cost/num'):GetComponent(typeof(UILabel)).text = OffWorldPass_Setting.GetData().RefreshBloodShopBlood
  else
    self.unlockNode.transform:Find('cost/icon01').gameObject:SetActive(false)
    self.unlockNode.transform:Find('cost/icon02').gameObject:SetActive(true)
    self.unlockNode.transform:Find('have/num'):GetComponent(typeof(UILabel)).text = self.TotalResNum * Blood2Yuanbao
    self.unlockNode.transform:Find('cost/num'):GetComponent(typeof(UILabel)).text = OffWorldPass_Setting.GetData().RefreshBloodShopBlood * Blood2Yuanbao
  end

  self.yuanbaohave:SetActive(self.UseYuanBao)
  self.have:SetActive(not self.UseYuanBao)

  for i,v in pairs(self.itemNodeTable) do
    local data = LuaOffWorldPassMgr.ShopInfo[i]
    local shopData = OffWorldPass_BloodShop.GetData(data[1])
    if shopData.blood > 0 and (not self.UseYuanBao or shopData.blood < self.TotalResNum)  then
      v.transform:Find('icon01').gameObject:SetActive(true)
      v.transform:Find('icon02').gameObject:SetActive(false)
      v.transform:Find('NumLabel'):GetComponent(typeof(UILabel)).text = shopData.blood
    elseif shopData.yuanbao > 0 then
      v.transform:Find('icon01').gameObject:SetActive(false)
      v.transform:Find('icon02').gameObject:SetActive(true)
      v.transform:Find('NumLabel'):GetComponent(typeof(UILabel)).text = shopData.yuanbao
    else
      v.transform:Find('icon01').gameObject:SetActive(false)
      v.transform:Find('icon02').gameObject:SetActive(true)
      v.transform:Find('NumLabel'):GetComponent(typeof(UILabel)).text = shopData.blood * Blood2Yuanbao
    end
  end
end

function LuaOffWorldPlay1ShopWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateOffWorldPlay1ShopWnd", self, "UpdateProgress")
end

function LuaOffWorldPlay1ShopWnd:OnDisable()
  if self.MyTick then
    invoke(self.MyTick)
  end
  if self.MySharkTick then
    invoke(self.MySharkTick)
    self.MySharkTick = nil
  end
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlay1ShopWnd", self, "UpdateProgress")
end

--@region UIEvent

--@endregion
