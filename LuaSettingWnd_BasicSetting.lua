local LocalString = import "LocalString"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local CommonDefs = import "L10.Game.CommonDefs"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CScene = import "L10.Game.CScene"
local PlayerSettings = import "L10.Game.PlayerSettings"
local ClientQuality = import "L10.Game.ClientQuality"
local QnNewSlider = import "L10.UI.QnNewSlider"
local DelegateFactory = import "DelegateFactory"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local CButton = import "L10.UI.CButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CTooltip = import "L10.UI.CTooltip"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow" 
local CCinemachineMgr=import "L10.Game.CCinemachineMgr"

LuaSettingWnd_BasicSetting = class()

--InfoSettings Unity Component
RegistClassMember(LuaSettingWnd_BasicSetting, "m_ResolutionSettingGroup")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_DefinitionSettingGroup")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_VolumeSlider")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_PlayerLimitSlider_UISlider")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_VolumeSlider_UISlider")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_PlayerLimitSlider")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_MusicSelectButton")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_SoundSelectButton")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_ShakeSelectButton")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_3DRadioBox")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_PlayerLimitLabel")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_QnCheckboxTpl")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_3DRadioBox")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_QnCheckboxTpl")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_3DRadioBox")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_QnCheckboxTpl")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_3DRadioBox")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_QuickSettingButtons")
RegistClassMember(LuaSettingWnd_BasicSetting, "MIN_PLAYERLIMIT")
RegistClassMember(LuaSettingWnd_BasicSetting, "MAX_PLAYERLIMIT")
RegistClassMember(LuaSettingWnd_BasicSetting, "m_AmbientSoundSelectButton")

RegistClassMember(LuaSettingWnd_BasicSetting, "m_ClientQualityTable")

function LuaSettingWnd_BasicSetting:Awake()
    self.MIN_PLAYERLIMIT = 3
    self.MAX_PLAYERLIMIT = 100
    self.m_ClientQualityTable = { [0] = ClientQuality.Good, [1] = ClientQuality.Normal, [2] = ClientQuality.Bad}
    self:InitComponents()
    self:Init()
end

function LuaSettingWnd_BasicSetting:InitComponents()
    self.m_QnCheckboxTpl = self.transform:Find("DisplaySettings/QnCheckBoxTel"):GetComponent(typeof(QnCheckBox))
    self.m_ResolutionSettingGroup = self.transform:Find("DisplaySettings/Resolution"):GetComponent(typeof(QnCheckBoxGroup))
    self.m_DefinitionSettingGroup = self.transform:Find("DisplaySettings/Definition"):GetComponent(typeof(QnCheckBoxGroup))
    self.m_VolumeSlider = self.transform:Find("SoundSetting/VolumeSetting/VolumeSlider"):GetComponent(typeof(QnNewSlider))
    self.m_VolumeSlider_UISlider = self.m_VolumeSlider:GetComponent(typeof(UISlider))
    self.m_PlayerLimitSlider = self.transform:Find("DisplaySettings/PlayerLimit/NumberSlider"):GetComponent(typeof(QnNewSlider))
    self.m_PlayerLimitSlider_UISlider = self.m_PlayerLimitSlider:GetComponent(typeof(UISlider))
    self.m_MusicSelectButton =self.transform:Find("SoundSetting/MusicSetting"):GetComponent(typeof(QnSelectableButton))
    self.m_SoundSelectButton =self.transform:Find("SoundSetting/SoundSetting"):GetComponent(typeof(QnSelectableButton))
    self.m_ShakeSelectButton = self.transform:Find("SoundSetting/ShakeSetting"):GetComponent(typeof(QnSelectableButton))
    self.m_AmbientSoundSelectButton = self.transform:Find("SoundSetting/AmbientSoundSetting"):GetComponent(typeof(QnSelectableButton))
    self.m_PlayerLimitLabel = self.transform:Find("DisplaySettings/PlayerLimit/limitLabel"):GetComponent(typeof(UILabel))
    self.m_3DRadioBox = self.transform:Find("DisplaySettings/3DSettings"):GetComponent(typeof(QnRadioBox))
    UIEventListener.Get(self.transform:Find("QuickSetting/Label").gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        CTooltip.ShowAtTop(g_MessageMgr:FormatMessage("Setting_Tip_Game_Quick_Setting", ClientQuality.GetDisplayName(ClientQuality.GetRecommendedQualityByDeviceLevel())), go.transform)
    end)
end

function LuaSettingWnd_BasicSetting:Init()
    local checkboxtitles = Table2ArrayWithCount({LocalString.GetString("高"), LocalString.GetString("中"), LocalString.GetString("低")}, 3, MakeArrayClass(System.String))
    self.m_ResolutionSettingGroup.m_Template =  self.m_QnCheckboxTpl
    self.m_ResolutionSettingGroup:InitWithOptions(checkboxtitles, false, false)
    self.m_DefinitionSettingGroup.m_Template =  self.m_QnCheckboxTpl
    self.m_DefinitionSettingGroup:InitWithOptions(checkboxtitles, false, false)
    self.m_ResolutionSettingGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnSetResolution(level)
    end)
    self.m_DefinitionSettingGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnSetDefinition(level)
    end)
    self.m_VolumeSlider.OnValueChanged = DelegateFactory.Action_float(function(volume)
        self:OnSetVolume(volume)
    end)
    self.m_PlayerLimitSlider.OnValueChanged = DelegateFactory.Action_float(function(percent)
        self:OnSetPlayerLimit(percent)
    end)
    self.m_MusicSelectButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetMusicEnabled()
    end)
    self.m_SoundSelectButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetSoundEnabled()
    end)
    self.m_AmbientSoundSelectButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetAmbientSoundEnabled()
    end)
    self.m_ShakeSelectButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnSetShakeEnabled()
    end)

    --避免打开页面就回调接口
    self.m_3DRadioBox:Awake()
    self.m_3DRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelect3DRadioBox(btn, index)
    end)

    local quickSettingRoot = self.transform:Find("QuickSetting")
    local goodButton = quickSettingRoot:Find("GoodButton"):GetComponent(typeof(CButton))
    local normalButton = quickSettingRoot:Find("NormalButton"):GetComponent(typeof(CButton))
    local badButton = quickSettingRoot:Find("BadButton"):GetComponent(typeof(CButton))
    self.m_QuickSettingButtons = {[2] = badButton,[1] = normalButton, [0] = goodButton}


    for i = 0, 2 do
        local clickFunc = function (go)
            for j = 0, 2 do
                self.m_QuickSettingButtons[j].Selected = (go == self.m_QuickSettingButtons[j].gameObject)
            end

            --低端设备上禁止开启选项
            if PlayerSettings.IsMonitoredProperty("QuickSettingMode") and PlayerSettings.IsLowPerformanceDevice then
                if not badButton.Selected then
                    PlayerSettings.ShowForbiddenSettingMessage()
                end
                return
            end

            PlayerSettings.LoadPerformanceSettings(self.m_ClientQualityTable[i])
        end
        CommonDefs.AddOnClickListener(self.m_QuickSettingButtons[i].gameObject, DelegateFactory.Action_GameObject(function(go) clickFunc(go) end), false)
    end

    self:LoadData()
end

function LuaSettingWnd_BasicSetting:OnSetResolution(level)
    if CommonDefs.IsAndroidPlatform() then
        if CRenderScene.Inst and CScene.MainScene and CRenderScene.Inst:IsSceneEnableDepthTexture() then
            PlayerSettings.Resolution = ClientQuality.Good;
        end
    end

    PlayerSettings.Resolution = self.m_ClientQualityTable[level]
end

function LuaSettingWnd_BasicSetting:OnSetDefinition(level)
    --低端设备上禁止开启选项
    if PlayerSettings.IsMonitoredProperty("QualityAdditiveLevel") and PlayerSettings.IsLowPerformanceDevice then
        if level<2 then
            PlayerSettings.ShowForbiddenSettingMessage()
        end
        return
    end

    local quality = self.m_ClientQualityTable[level]
    CLuaPlayerSettings.SetValueForSettingWnd("ParticleQuality", quality)
    CLuaPlayerSettings.SetValueForSettingWnd("GrassDensity", quality)
    PlayerSettings.SetQualityAdditive(quality)
end

function LuaSettingWnd_BasicSetting:OnSetVolume(volume)
    PlayerSettings.VolumeSetting = volume
end

function LuaSettingWnd_BasicSetting:OnSetPlayerLimit(percent)
    local count
    if not CommonDefs.IS_PUB_RELEASE and CommonDefs.IsPCGameMode() then
        count = percent * self.MAX_PLAYERLIMIT
    else
        count = percent * (self.MAX_PLAYERLIMIT - self.MIN_PLAYERLIMIT) + self.MIN_PLAYERLIMIT
    end
    count = math.floor(count + 0.5)
    self.m_PlayerLimitLabel.text = SafeStringFormat3(LocalString.GetString("最大显示玩家数量: [ffff00]%d[-]"),count)
    CLuaPlayerSettings.SetValueForSettingWnd("VisiblePlayerLimit", count)
end

function LuaSettingWnd_BasicSetting:OnSetMusicEnabled()
    PlayerSettings.MusicEnabled = self.m_MusicSelectButton:isSeleted()
end

function LuaSettingWnd_BasicSetting:OnSetSoundEnabled()
    PlayerSettings.SoundEnabled = self.m_SoundSelectButton:isSeleted()
end

function LuaSettingWnd_BasicSetting:OnSetAmbientSoundEnabled()
    PlayerSettings.AmbientSoundEnabled = self.m_AmbientSoundSelectButton:isSeleted()
end

function LuaSettingWnd_BasicSetting:OnSetShakeEnabled()
    PlayerSettings.ShakeEnabled = self.m_ShakeSelectButton:isSeleted()
end

function LuaSettingWnd_BasicSetting:LoadData()

    local val = PlayerSettings.QuickSettingMode
    for i = 0, 2 do
        self.m_QuickSettingButtons[i].Selected = ((2 - val) == i)
    end

    local resolution = CLuaPlayerSettings.GetValueForSettingWnd("Resolution")
    local quality = PlayerSettings.QualityAdditiveLevel
    --local grassQuality = PlayerSettings.GrassDensity

    for k,v in pairs(self.m_ClientQualityTable) do
        if v == resolution then
            self.m_ResolutionSettingGroup:SetSelect(k, true, true)
        end

        if v == quality then
            self.m_DefinitionSettingGroup:SetSelect(k, true, true)
        end
    end

    self.m_SoundSelectButton:SetSelected(PlayerSettings.SoundEnabled)
    self.m_AmbientSoundSelectButton:SetSelected(PlayerSettings.AmbientSoundEnabled)
    self.m_ShakeSelectButton:SetSelected(PlayerSettings.ShakeEnabled)
    self.m_MusicSelectButton:SetSelected(PlayerSettings.MusicEnabled)

    self.m_VolumeSlider_UISlider.value = PlayerSettings.VolumeSetting;
    local PlayerLimit = CLuaPlayerSettings.GetValueForSettingWnd("VisiblePlayerLimit")
    self.m_PlayerLimitLabel.text = SafeStringFormat3(LocalString.GetString("最大显示玩家数量: [ffff00]%d[-]"),PlayerLimit)

    if not CommonDefs.IS_PUB_RELEASE and CommonDefs.IsPCGameMode() then
        self.m_PlayerLimitSlider_UISlider.value = 1.0 * (PlayerLimit) / (self.MAX_PLAYERLIMIT)
    else
        self.m_PlayerLimitSlider_UISlider.value =  1.0 * (PlayerLimit - self.MIN_PLAYERLIMIT) / (self.MAX_PLAYERLIMIT - self.MIN_PLAYERLIMIT)
    end

    -- 判断是否要显示3D场景中的摄像机
    local camera3DPP = self.m_3DRadioBox.transform:Find("Grid/QnSelectableButton3").gameObject
    local grid = self.m_3DRadioBox.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    local isUse3D = false

    if CScene.MainScene and CScene.MainScene:Is3DScene() then
        camera3DPP:SetActive(true)
        grid.cellWidth = 210
        grid:Reposition()

        if CCinemachineMgr.GetCamera("Cinemachine/scene3d.prefab") then
            isUse3D = true
        end
    else
        camera3DPP:SetActive(false)
        grid.cellWidth = 281.4
        grid:Reposition()
    end

    if CommonDefs.ConvertIntToEnum(typeof(CameraMode), PlayerSettings.Saved3DMode) == CameraMode.E2D
            or CommonDefs.IsSceneEnable3D()
    then
        if isUse3D then
            self.m_3DRadioBox:ChangeTo(3, false);
        elseif CommonDefs.IsSceneDisable2D() then
            self.m_3DRadioBox:ChangeTo(2, false);
        else
            self.m_3DRadioBox:ChangeTo(PlayerSettings.Saved3DMode, false);
        end
    end
end

function LuaSettingWnd_BasicSetting:OnEnable()
    self:LoadData()
    g_ScriptEvent:AddListener("ReloadSettings", self, "LoadData")
end

function LuaSettingWnd_BasicSetting:OnDisable()
    g_ScriptEvent:RemoveListener("ReloadSettings", self, "LoadData")
end

function LuaSettingWnd_BasicSetting:OnSelect3DRadioBox(button,index)
    if index == 3 then
        CCinemachineMgr.OpenCamera("Cinemachine/scene3d.prefab", nil)
        return
    else
        CCinemachineMgr.CloseCamera("Cinemachine/scene3d.prefab")
    end

    local mode = CameraMode.E2D
    if index == 1 then
        mode = CameraMode.E3D
    end
    if index == 2 then
        mode = CameraMode.E3DPlus
    end
    if mode == CameraMode.E2D or CommonDefs.IsSceneEnable3D() then
        if mode ~= CameraMode.E3DPlus and CommonDefs.IsSceneDisable2D() then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("当前场景不允许该设置"))
            self.m_3DRadioBox:ChangeTo(2, false)
        else
            if mode == CameraMode.E2D then
                CTooltip.ShowAtTop(g_MessageMgr:FormatMessage("CAMERA_2D_TIP"), button.gameObject.transform)
            elseif mode == CameraMode.E3D then
                CTooltip.ShowAtTop(g_MessageMgr:FormatMessage("CAMERA_3D_TIP"), button.gameObject.transform)
            else
                CTooltip.ShowAtTop(g_MessageMgr:FormatMessage("CAMERA_3DPLUS_TIP"), button.gameObject.transform)
            end
            PlayerSettings.Saved3DMode = index
            if(CameraFollow.Inst.CurMode ~= mode) then
                CameraFollow.Inst:SetMode(mode, false, true)
            end
        end
    else
        g_MessageMgr:ShowMessage("SCENE_DISABLE_3DCAMERA")
        self.m_3DRadioBox:ChangeTo(0, false)
    end
end
