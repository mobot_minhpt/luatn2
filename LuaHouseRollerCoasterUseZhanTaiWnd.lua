local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local CItemInfoMgr_AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

LuaHouseRollerCoasterUseZhanTaiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseRollerCoasterUseZhanTaiWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaHouseRollerCoasterUseZhanTaiWnd, "Item", "Item", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHouseRollerCoasterUseZhanTaiWnd,"m_Wnd")


function LuaHouseRollerCoasterUseZhanTaiWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    UIEventListener.Get(self.Item).onClick = DelegateFactory.VoidDelegate(function(go)
        local id = LuaHouseRollerCoasterMgr.GetCurParentFurnitureId()
        local parent = CClientFurnitureMgr.Inst:GetFurniture(id)
        if parent.Children and parent.Children.Length>0 then
            local childId = parent.Children[0]
            if childId>0 then
                --取下
                local cnt,actions,names = 1,{
                    function()
                        Gac2Gas.ExchangeFurniture(
                            EnumFurniturePlace_lua.eDecoration,
                            LuaHouseRollerCoasterMgr.GetCurParentFurnitureId(),
                            EnumFurniturePlace_lua.eRepository,
                            1,--pos
                            CClientMainPlayer.Inst.Id,
                            tostring(childId)
                        )
                        CItemInfoMgr.CloseItemInfoWnd()
                    end
                },{ LocalString.GetString("取下") }
                local actionSource=DefaultItemActionDataSource.Create(cnt,actions,names)

                local child = CClientFurnitureMgr.Inst:GetFurniture(childId)


                CItemInfoMgr.ShowRepositoryFurnitureInfo(child.TemplateId,actionSource)
            end
        end
    end)

end

function LuaHouseRollerCoasterUseZhanTaiWnd:Init()
    self.Icon:Clear()
    local id = LuaHouseRollerCoasterMgr.GetCurParentFurnitureId()
    local parent = CClientFurnitureMgr.Inst:GetFurniture(id)
    if parent.Children and parent.Children.Length>0 then
        local childId = parent.Children[0]
        if childId>0 then
            local child = CClientFurnitureMgr.Inst:GetFurniture(childId)
            if child then
                local templateId = child.TemplateId
                local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
                if data then
                    local itemdata = Item_Item.GetData(data.ItemId)
                    self.Icon:LoadMaterial(itemdata.Icon)
                end
            end
        end
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaHouseRollerCoasterUseZhanTaiWnd:Update( )
    self.m_Wnd:ClickThroughToClose()
end
function LuaHouseRollerCoasterUseZhanTaiWnd:OnEnable()
	g_ScriptEvent:AddListener("OnPlacedFurnitureChanged", self, "OnPlacedFurnitureChanged")


    --切换界面
    g_ScriptEvent:BroadcastInLua("OnUseHouseRollerCoasterUseZhanTaiWnd")

end
function LuaHouseRollerCoasterUseZhanTaiWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnPlacedFurnitureChanged", self, "OnPlacedFurnitureChanged")

end
function LuaHouseRollerCoasterUseZhanTaiWnd:OnPlacedFurnitureChanged()
    self:Init()
end