-- Auto Generated!!
local AntiExchange_Setting = import "L10.Game.AntiExchange_Setting"
local CButton = import "L10.UI.CButton"
local CGongXunExchangeWnd = import "L10.UI.CGongXunExchangeWnd"
local CGongXunMgr = import "L10.Game.CGongXunMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local DataObject = import "L10.UI.CGongXunExchangeWnd+DataObject"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local PlayerSettings = import "L10.Game.PlayerSettings"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGongXunExchangeWnd.m_ClosePanel_CS2LuaHook = function (this) 
    local totalDeletePoint = 0
    local deleteDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 then
            CommonDefs.DictSet(deleteDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.deleteLevel)
            totalDeletePoint = totalDeletePoint + dataObject.data.deleteLevel
        end
    end))

    if totalDeletePoint > 0 then
        local returnRatio = System.Int32.Parse(AntiExchange_Setting.GetData("ReturnRatio").Value)

        MessageWndManager.ShowOKCancelMessage(System.String.Format(CGongXunExchangeWnd.DeleteStringTip, totalDeletePoint, returnRatio * totalDeletePoint), DelegateFactory.Action(function () 
            Gac2Gas.RequestSubAntiPointBegin()

            CommonDefs.DictIterate(deleteDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
                local data = {}
                data.Key = ___key
                data.Value = ___value
                local deleteNum = data.Value
                if deleteNum > 0 then
                    Gac2Gas.RequestSubAntiPoint(data.Key, deleteNum)
                end
            end))

            Gac2Gas.RequestSubAntiPointEnd()
            this:Close()
        end), DelegateFactory.Action(function () 
            this:Close()
        end), nil, nil, false)
    else
        this:Close()
    end
end
CGongXunExchangeWnd.m_SaveDeletePoint_CS2LuaHook = function (this) 
    local totalDeletePoint = 0
    local deleteDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 or dataObject.data.addLevel > 0 then
            CommonDefs.DictSet(deleteDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.addLevel - dataObject.data.deleteLevel)
            totalDeletePoint = totalDeletePoint + dataObject.data.deleteLevel
        end
    end))

    if totalDeletePoint > 0 then
        local returnRatio = System.Int32.Parse(AntiExchange_Setting.GetData("ReturnRatio").Value)
        MessageWndManager.ShowOKCancelMessage(System.String.Format(CGongXunExchangeWnd.DeleteStringTip, totalDeletePoint, returnRatio * totalDeletePoint), DelegateFactory.Action(function () 
            Gac2Gas.RequestAddOrSubAntiPointBegin()

            CommonDefs.DictIterate(deleteDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
                local data = {}
                data.Key = ___key
                data.Value = ___value
                local addNum = data.Value
                if addNum ~= 0 then
                    Gac2Gas.RequestAddOrSubAntiPoint(data.Key, addNum)
                end
            end))

            Gac2Gas.RequestAddOrSubAntiPointEnd()
        end), nil, nil, nil, false)
    else
        Gac2Gas.RequestAddOrSubAntiPointBegin()

        CommonDefs.DictIterate(deleteDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
            local data = {}
            data.Key = ___key
            data.Value = ___value
            local addNum = data.Value
            if addNum ~= 0 then
                Gac2Gas.RequestAddOrSubAntiPoint(data.Key, addNum)
            end
        end))

        Gac2Gas.RequestAddOrSubAntiPointEnd()
    end
end
CGongXunExchangeWnd.m_ResetAddPoint_CS2LuaHook = function (this) 
    local addDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.addLevel > 0 then
            CommonDefs.DictSet(addDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.addLevel)
        end
    end))

    CommonDefs.DictIterate(addDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local addNum = data.Value
        if addNum > 0 then
            local _data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).data
            if _data.addLevel > 0 then
                _data.addLevel = 0
                local _dataObject = CreateFromClass(DataObject)
                _dataObject.data = _data
                _dataObject.node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).node
                this:SetNodeFunc(_dataObject)
            end
        end
    end))
end
CGongXunExchangeWnd.m_ResetDeletePoint_CS2LuaHook = function (this, go) 
    CommonDefs.GetComponent_GameObject_Type(this.saveDeletePointBtn, typeof(CButton)).Enabled = false
    CommonDefs.GetComponent_GameObject_Type(this.resetDeletePointBtn, typeof(CButton)).Enabled = false
    UIEventListener.Get(this.saveDeletePointBtn).onClick = nil
    UIEventListener.Get(this.resetDeletePointBtn).onClick = nil

    local deleteDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 or dataObject.data.addLevel > 0 then
            CommonDefs.DictSet(deleteDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.addLevel - dataObject.data.deleteLevel)
        end
    end))

    CommonDefs.DictIterate(deleteDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local deleteNum = data.Value
        if deleteNum > 0 then
            local _data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).data
            if _data.deleteLevel > 0 or _data.addLevel > 0 then
                _data.deleteLevel = 0
                if _data.addLevel > 0 then
                    this.saveUnDistributeAnti2 = this.saveUnDistributeAnti2 + _data.addLevel
                    this:SetUnDistributePoint(this.saveUnDistributeAnti2)
                end
                _data.addLevel = 0
                local _dataObject = CreateFromClass(DataObject)
                _dataObject.data = _data
                _dataObject.node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).node
                this:SetNodeFunc(_dataObject)
            end
        end
    end))
end
CGongXunExchangeWnd.m_SetDeletePointBtn_CS2LuaHook = function (this) 
    CommonDefs.GetComponent_GameObject_Type(this.saveDeletePointBtn, typeof(CButton)).Enabled = false
    CommonDefs.GetComponent_GameObject_Type(this.resetDeletePointBtn, typeof(CButton)).Enabled = false
    UIEventListener.Get(this.saveDeletePointBtn).onClick = nil
    UIEventListener.Get(this.resetDeletePointBtn).onClick = nil

    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 or dataObject.data.addLevel > 0 then
            CommonDefs.GetComponent_GameObject_Type(this.saveDeletePointBtn, typeof(CButton)).Enabled = true
            CommonDefs.GetComponent_GameObject_Type(this.resetDeletePointBtn, typeof(CButton)).Enabled = true
            UIEventListener.Get(this.saveDeletePointBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                this:SaveDeletePoint()
            end)
            UIEventListener.Get(this.resetDeletePointBtn).onClick = MakeDelegateFromCSFunction(this.ResetDeletePoint, VoidDelegate, this)
            return
        end
    end))
end
CGongXunExchangeWnd.m_SubPointClick_CS2LuaHook = function (this, dataObject) 
    local data = dataObject.data
    if data.addLevel > 0 then
        local default = data
        default.addLevel = default.addLevel - 1
        this.saveUnDistributeAnti2 = this.saveUnDistributeAnti2 + 1
        this:SetUnDistributePoint(this.saveUnDistributeAnti2)
    elseif data.deleteLevel < data.nowLevel then
        local extern = data
        extern.deleteLevel = extern.deleteLevel + 1
    else
        return
    end

    local _dataObject = CreateFromClass(DataObject)
    _dataObject.data = data
    _dataObject.node = dataObject.node
    this:SetNodeFunc(_dataObject)
    this:SetDeletePointBtn()
end
CGongXunExchangeWnd.m_AddPointClick_CS2LuaHook = function (this, dataObject) 
    local data = dataObject.data
    if data.deleteLevel > 0 then
        local default = data
        default.deleteLevel = default.deleteLevel - 1
    elseif this.saveUnDistributeAnti2 > 0 then
        this.saveUnDistributeAnti2 = this.saveUnDistributeAnti2 - 1
        this:SetUnDistributePoint(this.saveUnDistributeAnti2)
        local extern = data
        extern.addLevel = extern.addLevel + 1
    else
        return
    end

    local _dataObject = CreateFromClass(DataObject)
    _dataObject.data = data
    _dataObject.node = dataObject.node
    this:SetNodeFunc(_dataObject)
    this:SetDeletePointBtn()
end
CGongXunExchangeWnd.m_SetNodeFunc_CS2LuaHook = function (this, dataObject) 
    local data = dataObject.data
    CommonDefs.DictSet(this.saveDataDic, typeof(UInt32), data.skillCls, typeof(DataObject), dataObject)

    local node = dataObject.node
    local subbtn = node.transform:Find("subbtn").gameObject
    local addbtn = node.transform:Find("addbtn").gameObject
    local nameLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel))
    local numLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel))
    local changeLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("changenum"), typeof(UILabel))
    local fullLabelNode = node.transform:Find("fullstring").gameObject


    local showLevel = data.nowLevel - data.deleteLevel + data.addLevel

    CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel)).text = tostring(showLevel)

    if data.nowLevel == 0 and data.deleteLevel == 0 and data.addLevel == 0 and this.saveUnDistributeAnti2 == 0 then
        subbtn:SetActive(false)
        addbtn:SetActive(false)
        changeLabel.gameObject:SetActive(false)
    else
        subbtn:SetActive(true)
        addbtn:SetActive(true)
        changeLabel.gameObject:SetActive(true)
    end

    if data.deleteLevel > 0 or this.saveUnDistributeAnti2 > 0 and showLevel < data.maxLevel then
        CommonDefs.GetComponent_GameObject_Type(addbtn, typeof(CButton)).Enabled = true
        UIEventListener.Get(addbtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:AddPointClick(dataObject)
        end)
    else
        CommonDefs.GetComponent_GameObject_Type(addbtn, typeof(CButton)).Enabled = false
        UIEventListener.Get(addbtn).onClick = nil
    end

    if showLevel <= 0 then
        CommonDefs.GetComponent_GameObject_Type(subbtn, typeof(CButton)).Enabled = false
        UIEventListener.Get(subbtn).onClick = nil
    else
        CommonDefs.GetComponent_GameObject_Type(subbtn, typeof(CButton)).Enabled = true
        UIEventListener.Get(subbtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:SubPointClick(dataObject)
        end)
    end

    local changeNumText = ""
    if data.addLevel > 0 then
        changeNumText = changeNumText .. (("[c][00FF00](+" .. data.addLevel) .. ")[-][/c]")
    end
    if data.deleteLevel > 0 then
        changeNumText = changeNumText .. (("[c][FF0000](-" .. data.deleteLevel) .. ")[-][/c]")
    end
    if data.addLevel <= 0 and data.deleteLevel <= 0 then
        changeNumText = "0"
    end

    if showLevel >= data.maxLevel then
        fullLabelNode:SetActive(true)
    else
        fullLabelNode:SetActive(false)
    end

    CommonDefs.GetComponent_Component_Type(node.transform:Find("changenum"), typeof(UILabel)).text = changeNumText
end
CGongXunExchangeWnd.m_SetDeletePointBack_CS2LuaHook = function (this, list) 
    do
        local i = 0
        while i < list.Count - 1 do
            local skillCls = math.floor(tonumber(list[i] or 0))
            local deleteLevel = math.floor(tonumber(list[i + 1] or 0))

            local data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).data
            local node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).node
            data.nowLevel = data.nowLevel + deleteLevel
            data.deleteLevel = 0
            data.addLevel = 0

            local dataObject = CreateFromClass(DataObject)
            dataObject.data = data
            dataObject.node = node

            this:SetNodeFunc(dataObject)
            i = i + 2
        end
    end

    this:SetDeletePointBtn()

    this:InitList()
end
CGongXunExchangeWnd.m_SetDistributePointBack_CS2LuaHook = function (this, list, remain) 
    this:SetUnDistributePoint(remain)
    this:ResetAddPoint()

    do
        local i = 0
        while i < list.Count - 1 do
            local skillCls = list[i]
            local addLevel = list[i + 1]
            if CommonDefs.DictContains(this.saveDataDic, typeof(UInt32), skillCls) then
                local data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).data
                local node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).node
                data.nowLevel = data.nowLevel + addLevel
                data.addLevel = addLevel

                local dataObject = CreateFromClass(DataObject)
                dataObject.data = data
                dataObject.node = node

                this:SetNodeFunc(dataObject)
            end
            i = i + 2
        end
    end

    this:SetDeletePointBtn()

    this:InitList()
end
CGongXunExchangeWnd.m_SetSingleNodeData_CS2LuaHook = function (this, node, data) 
    if not node.activeSelf then
        node:SetActive(true)
    end
    local subbtn = node.transform:Find("subbtn").gameObject
    local nameLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel))
    local numLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel))
    local changeLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("changenum"), typeof(UILabel))

    nameLabel.text = data.name

    local dataObject = CreateFromClass(DataObject)
    dataObject.data = data
    dataObject.node = node

    this:SetNodeFunc(dataObject)
end
CGongXunExchangeWnd.m_InitList_CS2LuaHook = function (this) 
    --saveDataDic.Clear();
    Extensions.RemoveAllChildren(this.listTable.transform)
    local count = CGongXunMgr.Inst.showDataList.Count
    local nowType = ""
    local nowNode = nil
    if count > 0 then
        do
            local i = 0
            while i < count do
                local data = CGongXunMgr.Inst.showDataList[i]

                if CommonDefs.DictContains(this.saveDataDic, typeof(UInt32), data.skillCls) then
                    data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.skillCls).data
                end

                if not (nowType == data.type) then
                    local barNode = NGUITools.AddChild(this.listTable.gameObject, this.BarItemTemplate)
                    barNode:SetActive(true)
                    CommonDefs.GetComponent_Component_Type(barNode.transform:Find("Label"), typeof(UILabel)).text = data.type
                    nowType = data.type
                    nowNode = nil
                end

                if nowNode == nil then
                    local node = NGUITools.AddChild(this.listTable.gameObject, this.InfoItemTemplate)
                    node:SetActive(true)
                    node.transform:Find("info_r").gameObject:SetActive(false)
                    this:SetSingleNodeData(node.transform:Find("info_l").gameObject, data)
                    nowNode = node
                else
                    this:SetSingleNodeData(nowNode.transform:Find("info_r").gameObject, data)
                    nowNode = nil
                end
                i = i + 1
            end
        end

        this.listTable:Reposition()
        this.listScrollView:ResetPosition()
    end
end
CGongXunExchangeWnd.m_UsePoint_CS2LuaHook = function (this, go) 
    if PlayerSettings.UseAntiPointAlert <= 0 then
        MessageWndManager.ShowMessageWithCheckBox(CGongXunExchangeWnd.UsePointTipText, CGongXunExchangeWnd.UsePointClickText, false, DelegateFactory.Action_bool(function (sign) 
            if sign then
                PlayerSettings.UseAntiPointAlert = 1
            end

            Gac2Gas.RequestDistributeAntiPoint()
        end), DelegateFactory.Action_bool(function (sign) 
            if sign then
                PlayerSettings.UseAntiPointAlert = 1
            end
        end), nil, nil)
    else
        Gac2Gas.RequestDistributeAntiPoint()
    end
end
CGongXunExchangeWnd.m_SetUnDistributePoint_CS2LuaHook = function (this, point) 
    this.unDistributePointLabel.text = tostring(point)
    if point <= 0 then
        this.unDistributePointLabel.color = Color.red
    else
        this.unDistributePointLabel.color = Color.green
    end
    this.saveUnDistributeAnti2 = point
end
CGongXunExchangeWnd.m_InitExchangePointPanel_CS2LuaHook = function (this) 
    this.exchangePointBtn.transform:Find("RedPoint").gameObject:SetActive(false)
    this.ExchangePointPanel:SetActive(true)

    local cancelBtn = this.ExchangePointPanel.transform:Find("CancelButton").gameObject
    local submitBtn = this.ExchangePointPanel.transform:Find("SubmitButton").gameObject

    local text1 = CommonDefs.GetComponent_Component_Type(this.ExchangePointPanel.transform:Find("text"), typeof(UILabel))
    local text2 = CommonDefs.GetComponent_Component_Type(this.ExchangePointPanel.transform:Find("text1"), typeof(UILabel))
    text1.text = System.String.Format(CGongXunExchangeWnd.ExchangeTip1, CGongXunMgr.Inst.minSinExchangePoint, CGongXunMgr.Inst.maxSinExchangePoint)
    text2.text = System.String.Format(CGongXunExchangeWnd.ExchangeTip2, CGongXunMgr.Inst.restExchangeNum2)
    local numInput = CommonDefs.GetComponent_Component_Type(this.ExchangePointPanel.transform:Find("BuyArea/Label/QnIncreseAndDecreaseButton"), typeof(QnAddSubAndInputButton))
    numInput:SetValue(0, true)
    local maxNum = CGongXunMgr.Inst.restExchangeNum2
    numInput:SetMinMax(0, maxNum, 1)

    UIEventListener.Get(cancelBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this.ExchangePointPanel:SetActive(false)
    end)

    UIEventListener.Get(submitBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        Gac2Gas.RequestExchangeAnti2(numInput:GetValue())
    end)
end
CGongXunExchangeWnd.m_SetExchangePointAdd_CS2LuaHook = function (this) 
    this:SetUnDistributePoint(CGongXunMgr.Inst.unDistributeAnti2)
    if this.ExchangePointPanel.activeSelf then
        this.ExchangePointPanel:SetActive(false)
    end

    this:InitList()
end
CGongXunExchangeWnd.m_Init_CS2LuaHook = function (this) 
    this.BarItemTemplate:SetActive(false)
    this.InfoItemTemplate:SetActive(false)
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:ClosePanel()
    end)

    this.saveUnDistributeAnti2 = CGongXunMgr.Inst.unDistributeAnti2
    this:SetUnDistributePoint(CGongXunMgr.Inst.unDistributeAnti2)
    CommonDefs.DictClear(this.saveDataDic)

    this:InitList()

    local maxNum = CGongXunMgr.Inst.restExchangeNum2
    if maxNum > 0 then
        this.exchangePointBtn.transform:Find("RedPoint").gameObject:SetActive(true)
    else
        this.exchangePointBtn.transform:Find("RedPoint").gameObject:SetActive(false)
    end

    this:SetDeletePointBtn()

    UIEventListener.Get(this.exchangePointBtn).onClick = MakeDelegateFromCSFunction(this.RequestOpenExchangePointPanel, VoidDelegate, this)
    UIEventListener.Get(this.tipBtn).onClick = MakeDelegateFromCSFunction(this.ShowTips, VoidDelegate, this)
end
