-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamQuickJoinDetailItem = import "L10.UI.CTeamQuickJoinDetailItem"
local CTeamQuickJoinDetailView = import "L10.UI.CTeamQuickJoinDetailView"
local CTeamQuickJoinInfoMgr = import "L10.UI.CTeamQuickJoinInfoMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTeamQuickJoinDetailView.m_InitWithActivityId_CS2LuaHook = function (this, activityId) 

    this.activityId = activityId
    this.tableView:Clear()
    this.tableView.dataSource = this

    this:Refresh()
    this:OnTeamMatchStateChanged()
    this.createTeamBtn.Enabled = (not CTeamMgr.Inst:TeamExists())
    this.autoMatchBtn.Enabled = (not CTeamMgr.Inst:TeamExists() and activityId ~= Constants.NearbyActivityId)
end
CTeamQuickJoinDetailView.m_Refresh_CS2LuaHook = function (this) 

    if this.activityId == Constants.NearbyActivityId then
        CTeamMatchMgr.Inst:QueryTeamsInSameScene()
    else
        CTeamMatchMgr.Inst:RequestMatchingTeamInfos(this.activityId, 1, 10, true)
    end
end
CTeamQuickJoinDetailView.m_Start_CS2LuaHook = function (this) 

    this.itemTemplate:SetActive(false)
    UIEventListener.Get(this.refreshBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.refreshBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnRefreshButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.createTeamBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.createTeamBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnCreateTeamButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.autoMatchBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.autoMatchBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnAutoMatchButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.createTeamGroupBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.createTeamGroupBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnCreateTeamGroupClick, VoidDelegate, this), true)
end
CTeamQuickJoinDetailView.m_OnAutoMatchButtonClick_CS2LuaHook = function (this, go) 

    if CTeamMatchMgr.Inst.IsTeamMatching then
        Gac2Gas.AskForLeaveMatching()
    else
        CTeamMatchMgr.Inst:AskForTeamMatchingForMember(this.activityId)
    end
end
CTeamQuickJoinDetailView.m_OnMathingTeamInfosReceived_CS2LuaHook = function (this, activityId) 

    if activityId == this.activityId then
        this.tableView:LoadData(0, true)
    end
end
CTeamQuickJoinDetailView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    local cellIdentifier = "TeamQuickJoinRowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.itemTemplate, cellIdentifier)
    end
    if index >= 0 and index < CTeamQuickJoinInfoMgr.Inst.MatchInfos.Count then
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CTeamQuickJoinDetailItem)):Init(CTeamQuickJoinInfoMgr.Inst.MatchInfos[index], this.activityId)
    end

    return cell
end
