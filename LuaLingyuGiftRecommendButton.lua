local DelegateFactory = import "DelegateFactory"
local Vector4         = import "UnityEngine.Vector4"
local CUIFxPaths      = import "L10.UI.CUIFxPaths"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaLingyuGiftRecommendButton = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaLingyuGiftRecommendButton, "fx")
RegistClassMember(LuaLingyuGiftRecommendButton, "icon")
RegistClassMember(LuaLingyuGiftRecommendButton, "countDown")
RegistClassMember(LuaLingyuGiftRecommendButton, "tick")

function LuaLingyuGiftRecommendButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.countDown = self.transform:Find("CountDown"):GetComponent(typeof(UILabel))
    self.icon = self.transform:GetComponent(typeof(UIWidget))

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClick()
    end)
end

function LuaLingyuGiftRecommendButton:Init(args)
    if args then
        self.fx = args[0]
    end
    if not self.fx then return end

    if LuaLingyuGiftRecommendMgr.needShowButtonFx then
        if not self.fx.FxGo then
            self.fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)

            local gap = 5
            CUIFx.DoAni(Vector4(- self.icon.width * 0.5 + gap, self.icon.height * 0.5 - gap,
                self.icon.width - gap * 2, self.icon.height - gap * 2), true, self.fx)
        end
    else
        self.fx:DestroyFx()
    end

    self:ClearTick()
    self:UpdateTime()
    self.tick = RegisterTick(function()
        self:UpdateTime()
    end, 1000)
end

function LuaLingyuGiftRecommendButton:UpdateTime()
    local expireTime = LuaLingyuGiftRecommendMgr.info.expireTime
    local leftTime = expireTime and math.floor(expireTime - CServerTimeMgr.Inst.timeStamp) or -1

    if leftTime < 0 then
        self.countDown.text = "00:00:00"
        EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
        return
    end

    local h = math.floor(leftTime / 3600)
    local m = math.floor(leftTime % 3600 / 60)
    local s = leftTime % 60
    self.countDown.text = SafeStringFormat3("%02d:%02d:%02d", h, m, s)
end

function LuaLingyuGiftRecommendButton:Update()
    if not self.fx then return end

    self.fx.transform.position = self.transform.position
end

function LuaLingyuGiftRecommendButton:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
        self.tick = nil
    end
end

function LuaLingyuGiftRecommendButton:OnDestroy()
    self:ClearTick()
end

--@region UIEvent

function LuaLingyuGiftRecommendButton:OnClick()
    LuaLingyuGiftRecommendMgr.needShowButtonFx = false
    if self.fx then self.fx:DestroyFx() end

    if LuaLingyuGiftRecommendMgr:HasTriggeredGift() then
        local info = LuaLingyuGiftRecommendMgr.info
        Gac2Gas.ClickUXDynamicGiftIcon(info.event, info.traceId)
        CUIManager.ShowUI(CLuaUIResources.LingyuGiftRecommendWnd)
    end
end

--@endregion UIEvent
