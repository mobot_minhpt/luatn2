-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTradeMgr = import "L10.Game.CTradeMgr"
local CTradePresentedDetailView = import "L10.UI.CTradePresentedDetailView"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTradePresentedDetailView.m_UpdateNeedResInfo_CS2LuaHook = function (this, validGiftLimit, usedWeeklyGiftLimit, weeklyGiftLimit, needGiftLimit) 
    if this.saveResShowNode ~= nil then
        this.saveResShowNode:SetActive(true)
        CommonDefs.GetComponent_Component_Type(this.saveResShowNode.transform:Find("need"), typeof(UILabel)).text = tostring(needGiftLimit)
        CommonDefs.GetComponent_Component_Type(this.saveResShowNode.transform:Find("have"), typeof(UILabel)).text = tostring(validGiftLimit)
        CommonDefs.GetComponent_Component_Type(this.saveResShowNode.transform:Find("thisweek"), typeof(UILabel)).text = (tostring(usedWeeklyGiftLimit) .. "/") .. tostring(weeklyGiftLimit)
    end
end
CTradePresentedDetailView.m_Init_CS2LuaHook = function (this) 

    this.saveResShowNode = nil
    this.iconTexture.material = nil
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.disableSprite.enabled = false
    this.lockSprite.enabled = false
    --amountLabel.text = "";
    this.nameLabel.text = ""
    this.tradeItem = nil
    this.tradeEquip = nil
    local default
    if CTradeMgr.Inst.IsPlayerPresenter then
        default = LocalString.GetString("请填充需要赠送的物品")
    else
        default = LocalString.GetString("请等待玩家选择赠送的物品")
    end
    this.tips.text = default

    this.presentedNode:SetActive(false)
    this.receiveNode:SetActive(false)
    this.plusSprite.enabled = CTradeMgr.Inst.IsPlayerPresenter and not CTradeMgr.Inst.targetTradeEquip and not CTradeMgr.Inst.playerTradeItem
    if CTradeMgr.Inst.IsPlayerPresenter and (CTradeMgr.Inst.playerTradeEquip ~= nil or CTradeMgr.Inst.playerTradeItem ~= nil) then
        if CTradeMgr.Inst.playerTradeEquip ~= nil then
            this.tradeEquip = CTradeMgr.Inst.playerTradeEquip
        else
            this.tradeItem = CTradeMgr.Inst.playerTradeItem
        end

        if this.tradeEquip ~= nil then
            this.equip = EquipmentTemplate_Equip.GetData(this.tradeEquip.TemplateId)
        else
            this.item = CItemMgr.Inst:GetItemTemplate(this.tradeItem.TemplateId)
        end

        this.saveResShowNode = this.presentedNode
    elseif not CTradeMgr.Inst.IsPlayerPresenter and (CTradeMgr.Inst.targetTradeEquip ~= nil or CTradeMgr.Inst.targetTradeItem ~= nil) then
        if CTradeMgr.Inst.targetTradeEquip ~= nil then
            this.tradeEquip = CTradeMgr.Inst.targetTradeEquip
        else
            this.tradeItem = CTradeMgr.Inst.targetTradeItem
        end

        if this.tradeEquip ~= nil then
            this.equip = EquipmentTemplate_Equip.GetData(this.tradeEquip.TemplateId)
        else
            this.item = CItemMgr.Inst:GetItemTemplate(this.tradeItem.TemplateId)
        end

        this.saveResShowNode = this.receiveNode
    else
        return
    end
    this.tips.text = ""
    if this.tradeEquip ~= nil then
        this.iconTexture:LoadMaterial(this.equip.Icon)
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(this.tradeEquip.QualityType)
        this.disableSprite.enabled = not this.tradeEquip.MainPlayerIsFit
        this.nameLabel.text = this.tradeEquip.DisplayName
        this.nameLabel.color = this.tradeEquip.DisplayColor
        this.amountLabel.text = ""
        Gac2Gas.RequestGiftLimitOnShelf()
    elseif this.tradeItem ~= nil then
        this.iconTexture:LoadMaterial(this.tradeItem.Icon)
        this.qualitySprite.spriteName =   CLuaItemMgr:GetItemCellBorder(this.tradeItem.Id)
        this.disableSprite.enabled = not this.tradeItem.MainPlayerIsFit
        this.nameLabel.text = this.tradeItem.DisplayName
        this.nameLabel.color = this.tradeItem.Color
        if this.tradeItem.Count > 1 then
            this.amountLabel.text = tostring(this.tradeItem.Count)
        else
            this.amountLabel.text = ""
        end
        Gac2Gas.RequestGiftLimitOnShelf()
    end
end
CTradePresentedDetailView.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.TargetTradeEquipUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.PlayerTradeEquipUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListener(EnumEventType.TargetTradeItemUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.PlayerTradeItemUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnPlayerSellItemSelect, MakeGenericClass(Action2, String, String), this))

    UIEventListener.Get(this.qualitySprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.qualitySprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
end
CTradePresentedDetailView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.TargetTradeEquipUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerTradeEquipUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListener(EnumEventType.TargetTradeItemUpdate, MakeDelegateFromCSFunction(this.Init, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerTradeItemUpdate, MakeDelegateFromCSFunction(this.TradeItemUpdate, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnFreightSubmitEquipSelected, MakeDelegateFromCSFunction(this.OnPlayerSellItemSelect, MakeGenericClass(Action2, String, String), this))

    UIEventListener.Get(this.qualitySprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.qualitySprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), false)
end
CTradePresentedDetailView.m_OnPlayerSellItemSelect_CS2LuaHook = function (this, itemId, key) 

    if not System.String.IsNullOrEmpty(itemId) then
        if not CTradeMgr.Inst.targetLock then
            local item = CItemMgr.Inst:GetById(itemId)
            if item ~= nil then
                local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)

                this.amountLabel.text = ""
                if pos > 0 then
                    local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, pos)
                    local precious = CItemMgr.Inst:GetById(id)

                    local amount = precious.Amount
                    Gac2Gas.MoveItemToTradeShelf(1, pos, itemId, amount)
                    --if(amount > 1)
                    --    amountLabel.text = amount.ToString();
                end
            end
        else
            g_MessageMgr:ShowMessage("CANNOT_CHANGE_ITEM_AFTER_MONEY_LOCKED")
        end
    end
end
CTradePresentedDetailView.m_OnItemClick_CS2LuaHook = function (this, go) 

    if CTradeMgr.Inst.IsPlayerPresenter then
        local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, String)))
        local preciousList = CreateFromClass(MakeGenericClass(List, String))
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        do
            local i = 1
            while i <= bagSize do
                local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)

                if not System.String.IsNullOrEmpty(id) then
                    local precious = CItemMgr.Inst:GetById(id)
                    if precious ~= nil then
                        if precious.IsEquip and precious.Equip:IsAllowFace2FaceGift() then
                            CommonDefs.ListAdd(preciousList, typeof(String), precious.Id)
                        elseif precious.IsItem and precious.Item:IsAllowFace2FaceGift() then
                            CommonDefs.ListAdd(preciousList, typeof(String), precious.Id)
                        end
                    end
                end
                i = i + 1
            end
        end
        if preciousList.Count > 0 then
            CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, String)), preciousList)
            CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", dic, LocalString.GetString("选择要赠送的物品"), 0, false, LocalString.GetString("选择"), "", nil, "")
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("没有可以赠送的物品"))
        end
    elseif this.tradeItem ~= nil or this.tradeEquip ~= nil then
        if this.tradeItem ~= nil then
            CItemInfoMgr.ShowLinkItemInfo(this.tradeItem.Id, false, nil, AlignType.Default, 0, 0, 0, 0)
        else
            CItemInfoMgr.ShowLinkItemInfo(this.tradeEquip.Id, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    end
end
