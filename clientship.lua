local Vector2=import "UnityEngine.Vector2"
local CPhysicsMgr=import "L10.Engine.CPhysicsMgr"
local CPhysicsWorld=import "L10.Engine.CPhysicsWorld"
local PrimitiveType=import "UnityEngine.PrimitiveType"


local CapsuleCollider2D = import "UnityEngine.CapsuleCollider2D"
local CircleCollider2D = import "UnityEngine.CircleCollider2D"
local BoxCollider2D = import "UnityEngine.BoxCollider2D"
local PhysicsMaterial2D=import "UnityEngine.PhysicsMaterial2D"
local CapsuleDirection2D=import "UnityEngine.CapsuleDirection2D"

require("common/haizhan/PhysicsCommon")
require "common/haizhan/PhysicsShipInc"
require "common/haizhan/PhysicsAIShipInc"


-- g_PhysicsObjects = {}
g_PhysicsObjectHandlers = {}


--物理材质的管理
g_PhysicsMaterials = {}

function SetPhysicsShape(rb,shapeId,isTrigger)
    local designData = nil
    if isTrigger then
        designData = Physics_TriggerShape.GetData(shapeId)
    else
        designData = Physics_ObjectShape.GetData(shapeId)
    end
    if not g_PhysicsMaterials[shapeId] then
        local mat = PhysicsMaterial2D()
        mat.friction = designData.friction
        mat.bounciness = designData.resititution
        g_PhysicsMaterials[shapeId] = mat
    end

    if designData.shapeType==1 then
        local box2dCollder = rb.gameObject:AddComponent(typeof(CapsuleCollider2D))
        box2dCollder.size = Vector2(designData.radius * 2 + designData.height, designData.radius * 2)
        box2dCollder.direction = CapsuleDirection2D.Horizontal

        rb.mass = (PHYSICS_PI * designData.radius * designData.radius + designData.radius * 2 * designData.height) * designData.density
    elseif designData.shapeType==2 then--
        local box2dCollder = rb.gameObject:AddComponent(typeof(CircleCollider2D))
        box2dCollder.radius = designData.radius

        rb.mass = (PHYSICS_PI*designData.radius*designData.radius) * designData.density
    elseif designData.shapeType == 3 then

    end

    rb.sharedMaterial = g_PhysicsMaterials[shapeId]
    rb.drag = designData.linearDamping
    rb.angularDrag = designData.angularDamping
end
