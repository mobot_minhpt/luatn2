-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHideAndSeekHiderResultItem = import "L10.UI.CHideAndSeekHiderResultItem"
local Color = import "UnityEngine.Color"
local Profession = import "L10.Game.Profession"
CHideAndSeekHiderResultItem.m_Init_CS2LuaHook = function (this, info, row) 
    if row % 2 == 0 then
        this.bgSprite.alpha = 0
    else
        this.bgSprite.alpha = 1
    end

    this.nameLabel.text = info.name
    this.clsSprite.spriteName = Profession.GetIcon(info.cls)
    this.scoreLabel.text = tostring(info.pickScore)

    if info.playTime > 0 then
        this.timeLabel.gameObject:SetActive(true)
        this.timeLabel.text = this:Seconds2Minute(info.playTime)
    else
        this.timeLabel.gameObject:SetActive(false)
    end

    if (info.name == CClientMainPlayer.Inst.Name) then
        if info.isCaught then
            this.nameLabel.color = Color.green
            this.nameLabel.alpha = 0.5
            this.scoreLabel.color = Color.green
            this.scoreLabel.alpha = 0.5
            this.timeLabel.color = Color.green
            this.timeLabel.alpha = 0.5
        else
            this.nameLabel.color = Color.green
            this.nameLabel.alpha = 1
            this.scoreLabel.color = Color.green
            this.scoreLabel.alpha = 1
            this.timeLabel.color = Color.green
            this.timeLabel.alpha = 1
        end
    else
        if info.isCaught then
            this.nameLabel.color = Color.grey
            this.nameLabel.alpha = 0.5
            this.scoreLabel.color = Color.grey
            this.scoreLabel.alpha = 0.5
            this.timeLabel.color = Color.grey
            this.timeLabel.alpha = 0.5
        else
            this.nameLabel.color = Color.white
            this.nameLabel.alpha = 1
            this.scoreLabel.color = Color.white
            this.scoreLabel.alpha = 1
            this.timeLabel.color = Color.white
            this.timeLabel.alpha = 1
        end
    end
end
