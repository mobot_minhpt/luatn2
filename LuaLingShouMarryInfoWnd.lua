require("3rdParty/ScriptEvent")
require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CLingShouBaseMgr=import "L10.Game.CLingShouBaseMgr"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"


local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

CLuaLingShouMarryInfoWnd=class()
RegistClassMember(CLuaLingShouMarryInfoWnd,"marryStateLabel")
RegistClassMember(CLuaLingShouMarryInfoWnd,"genderLabel")
RegistClassMember(CLuaLingShouMarryInfoWnd,"bgSprite")
RegistClassMember(CLuaLingShouMarryInfoWnd,"lingShouIcon1")
RegistClassMember(CLuaLingShouMarryInfoWnd,"lingShouIcon2")
RegistClassMember(CLuaLingShouMarryInfoWnd,"lingShouNameLabel1")
RegistClassMember(CLuaLingShouMarryInfoWnd,"lingShouNameLabel2")
RegistClassMember(CLuaLingShouMarryInfoWnd,"radioBox")
-- LuaSetGlobalIfNil("CLuaLingShouMarryInfoWnd",{})
RegistClassMember(CLuaLingShouMarryInfoWnd,"m_FatherTemplateId")
RegistClassMember(CLuaLingShouMarryInfoWnd,"m_MotherTemplateId")
RegistClassMember(CLuaLingShouMarryInfoWnd,"m_PlayerId1")
RegistClassMember(CLuaLingShouMarryInfoWnd,"m_PlayerId2")
RegistClassMember(CLuaLingShouMarryInfoWnd,"m_MarriedNode")
RegistClassMember(CLuaLingShouMarryInfoWnd,"m_ZhengHunButton")



function CLuaLingShouMarryInfoWnd:Awake()
    self.marryStateLabel=LuaGameObject.GetChildNoGC(self.transform,"MarryStateLabel").label
    self.genderLabel=LuaGameObject.GetChildNoGC(self.transform,"GenderLabel").label
    self.bgSprite=LuaGameObject.GetChildNoGC(self.transform,"Node").sprite
    self.lingShouIcon1=LuaGameObject.GetChildNoGC(self.transform,"Icon1").cTexture
    self.lingShouIcon2=LuaGameObject.GetChildNoGC(self.transform,"Icon2").cTexture
    self.lingShouNameLabel1=LuaGameObject.GetChildNoGC(self.transform,"NameLabel1").label
    self.lingShouNameLabel2=LuaGameObject.GetChildNoGC(self.transform,"NameLabel2").label

    self.m_MarriedNode=LuaGameObject.GetChildNoGC(self.transform,"Married").gameObject
    self.radioBox=LuaGameObject.GetChildNoGC(self.transform,"RadioBox").qnRadioBox

    self.m_ZhengHunButton=LuaGameObject.GetChildNoGC(self.transform,"ZhengHunButton").gameObject
    self.m_ZhengHunButton:SetActive(false)

    local function onClickZhengHunButton(go)
        self:OnClickZhengHunButton(go)
    end
    CommonDefs.AddOnClickListener(self.m_ZhengHunButton,DelegateFactory.Action_GameObject(onClickZhengHunButton),false)
end

function CLuaLingShouMarryInfoWnd:OnClickZhengHunButton(go)
    -- local function SelectAction(index)
    --     local id=CClientMainPlayer.Inst.Id
    --     local msg=SafeStringFormat3("<link button=灵兽招亲,LingShouZhaoQin,%s>",id)
    --     if index==0 then
    --         --世界频道
    --         CChatHelper.SendMsg(EChatPanel.World,msg,0)
    --     elseif index==1 then
    --         --帮会频道
    --         CChatHelper.SendMsg(EChatPanel.Guild,msg,0)
    --     elseif index==2 then
    --         --队伍频道
    --         CChatHelper.SendMsg(EChatPanel.Team,msg,0)
    --     end
        
    -- end
    -- local selectShareItem=DelegateFactory.Action_int(SelectAction)

    -- local t={}
    -- local item=PopupMenuItemData(LocalString.GetString("至世界频道"),selectShareItem,false,nil)
    -- table.insert(t, item)
    -- local item=PopupMenuItemData(LocalString.GetString("至帮会频道"),selectShareItem,false,nil)
    -- table.insert(t, item)
    -- local item=PopupMenuItemData(LocalString.GetString("至队伍频道"),selectShareItem,false,nil)
    -- table.insert(t, item)
    -- -- end
    -- local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    -- CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Top,1,nil,600,true,266)
    CUIManager.ShowUI(CUIResources.LingShouMarriageMatchInputWnd)
end

function CLuaLingShouMarryInfoWnd:Init()
    local details=CLuaLingShouMgr.m_TempLingShouInfo
    if details~=nil then
        local marryInfo=details.data.Props.MarryInfo
        if marryInfo==nil then
            self.m_MarriedNode:SetActive(false)
            return
        end
        --父母性别
        if marryInfo.Gender==1 then
            --阳
            self.genderLabel.text=LocalString.GetString("阳")
        elseif marryInfo.Gender==2 then
            --阴
            self.genderLabel.text=LocalString.GetString("阴")
        elseif marryInfo.Gender==3 then
            --阴阳
            self.genderLabel.text=LocalString.GetString("阴阳")
        else
            self.genderLabel.text=LocalString.GetString("未知")
        end

        if marryInfo.IsFather>0 then
            self.m_FatherTemplateId=details.data.TemplateId
            self.m_MotherTemplateId=marryInfo.PartnerTemplateId
            self.m_PlayerId1=self:GetOwnerId()
            self.m_PlayerId2=marryInfo.PartnerOwnerId
        else
            self.m_FatherTemplateId=marryInfo.PartnerTemplateId
            self.m_MotherTemplateId=details.data.TemplateId
            self.m_PlayerId1=marryInfo.PartnerOwnerId
            self.m_PlayerId2=self:GetOwnerId()
        end


        local marryTime=details.data.Props.MarryInfo.MarryTime
        if marryTime>0 then
            self.marryStateLabel.text=LocalString.GetString("已婚")
            self.m_ZhengHunButton:SetActive(false)
            self.bgSprite.height=413
            local templateId=details.data.TemplateId
            self.lingShouIcon1:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(self.m_FatherTemplateId))
            self.lingShouNameLabel1.text=CLingShouBaseMgr.GetLingShouName(self.m_FatherTemplateId)
            self.lingShouIcon2:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(self.m_MotherTemplateId))
            self.lingShouNameLabel2.text=CLingShouBaseMgr.GetLingShouName(self.m_MotherTemplateId)
        else
            self.marryStateLabel.text=LocalString.GetString("未婚")
            --如果是自己的灵兽 并且不是阳 会显示征婚按钮
            if CLuaLingShouMgr.m_IsMyLingShou and marryInfo.Gender~=1 then
                self.m_ZhengHunButton:SetActive(true)
                self.bgSprite.height=270
            else
                self.m_ZhengHunButton:SetActive(false)
                self.bgSprite.height=180
            end
            self.lingShouIcon1:Clear()
            self.lingShouNameLabel1.text=" "
            self.lingShouIcon2:Clear()
            self.lingShouNameLabel2.text=" "

            self.m_MarriedNode:SetActive(false)
        end
        self.bgSprite.transform.localPosition=Vector3(0,self.bgSprite.height/2)

        local function OnSelect(qnButton,index)
            local worldPos = self.transform:TransformPoint(Vector3(520,-60,0))
            if index==0 then
                CPlayerInfoMgr.ShowPlayerPopupMenu(self.m_PlayerId1, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined," ",nil,worldPos,AlignType.Default)
            elseif index==1 then
                CPlayerInfoMgr.ShowPlayerPopupMenu(self.m_PlayerId2, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined," ",nil,worldPos,AlignType.Default)
            end
        end
        self.radioBox.OnSelect=DelegateFactory.Action_QnButton_int(OnSelect)
        self.radioBox:ChangeTo(-1)
    end
end
function CLuaLingShouMarryInfoWnd:GetOwnerId()
    if CLuaLingShouMgr.m_IsMyLingShou then
        if CClientMainPlayer.Inst then
            return CClientMainPlayer.Inst.Id
        end
    else
        return CLuaLingShouOtherMgr.playerId
    end
end
-- function CLuaLingShouMarryInfoWnd:OnEnable()
--     -- g_ScriptEvent:AddListener("GetLingShouOtherDetails", self, "OnGetLingShouOtherDetails")
-- end

-- function CLuaLingShouMarryInfoWnd:OnDisable()
--     -- g_ScriptEvent:RemoveListener("GetLingShouOtherDetails", self, "OnGetLingShouOtherDetails")
-- end

return CLuaLingShouMarryInfoWnd
