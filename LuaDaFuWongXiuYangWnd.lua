local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
LuaDaFuWongXiuYangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongXiuYangWnd, "XiuYang", "XiuYang", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongXiuYangWnd, "m_labelColorList")
RegistClassMember(LuaDaFuWongXiuYangWnd, "m_headIconList")
RegistClassMember(LuaDaFuWongXiuYangWnd, "m_HeadIconBg")
RegistClassMember(LuaDaFuWongXiuYangWnd, "m_HeadIcon")
RegistClassMember(LuaDaFuWongXiuYangWnd, "m_XiuYangLabel")
RegistClassMember(LuaDaFuWongXiuYangWnd, "m_DescLabel")
RegistClassMember(LuaDaFuWongXiuYangWnd, "m_PlayerHead")
function LuaDaFuWongXiuYangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_labelColorList = {"f5c762","e55657","ce77fe","47d58e"}
    self.m_headIconList = {"dafuwongplaylandeopwnd_huang","dafuwongplaylandeopwnd_hong","dafuwongplaylandeopwnd_zi","dafuwongplaylandeopwnd_lv"}
    self.m_HeadIcon = self.XiuYang.transform:Find("Player"):GetComponent(typeof(CUITexture))
    self.m_HeadIconBg = self.XiuYang.transform:Find("Player/bg"):GetComponent(typeof(CUITexture))
    self.m_XiuYangLabel = self.XiuYang.transform:Find("Player/Label"):GetComponent(typeof(UILabel))
    self.m_DescLabel = self.XiuYang.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_PlayerHead = self.transform:Find("XiuYang/PlayerHead").gameObject
end

function LuaDaFuWongXiuYangWnd:Init()
    self:ShowPlayerHead()
    if LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RoundCheckAnimation then
        self:ShowBuffStatus()
        self.m_PlayerHead.gameObject:SetActive(false)
    elseif LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RandomEventDog then
        self:ShowEventAni()
        self.m_PlayerHead.gameObject:SetActive(false)
    elseif LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RoundCardAnimation then
        self:ShowCardAni()
    end
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_YaoPu", Vector3.zero, nil, 0)
end

function LuaDaFuWongXiuYangWnd:ShowBuffStatus()
    if not LuaDaFuWongMgr.CurAniInfo then return end
    local AnimInfo = LuaDaFuWongMgr.CurAniInfo
    local playerId = AnimInfo.playerId
    local playerInfo = LuaDaFuWongMgr.PlayerInfo[playerId]
    self.m_HeadIcon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.class, playerInfo.gender, -1), false)
    self.m_HeadIconBg:LoadMaterial(self:GetTexturePath(playerInfo.round))
    self.m_XiuYangLabel.color = NGUIText.ParseColor24(self.m_labelColorList[playerInfo.round], 0)
    self.m_DescLabel.gameObject:SetActive(false)
    
    local countDown = AnimInfo.time
	local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongXiuYangWnd) end
	local durationFunc = function(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,countDown,durationFunc,endFunc)
end

function LuaDaFuWongXiuYangWnd:ShowEventAni()
    local playerId = LuaDaFuWongMgr.CurRoundPlayer
    local playerInfo = LuaDaFuWongMgr.PlayerInfo[playerId]
    self.m_HeadIcon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.class, playerInfo.gender, -1), false)
    self.m_HeadIconBg:LoadMaterial(self:GetTexturePath(playerInfo.round))
    self.m_XiuYangLabel.color = NGUIText.ParseColor24(self.m_labelColorList[playerInfo.round], 0)
    self.m_DescLabel.gameObject:SetActive(true)

    local countDown = LuaDaFuWongMgr.CurAniInfo.time
	local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongXiuYangWnd) end
	local durationFunc = function(time) self:UpdateDesLabel(playerId,time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,countDown,durationFunc,endFunc)
end

function LuaDaFuWongXiuYangWnd:ShowCardAni()
    if not LuaDaFuWongMgr.CurAniInfo then return end
    local AnimInfo = LuaDaFuWongMgr.CurAniInfo
    local playerId = AnimInfo.selectPlayerId
    local playerInfo = LuaDaFuWongMgr.PlayerInfo[playerId]
    self.m_HeadIcon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.class, playerInfo.gender, -1), false)
    self.m_HeadIconBg:LoadMaterial(self:GetTexturePath(playerInfo.round))
    self.m_XiuYangLabel.color = NGUIText.ParseColor24(self.m_labelColorList[playerInfo.round], 0)
    self.m_DescLabel.gameObject:SetActive(true)
    
    local countDown = AnimInfo.time
	local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongXiuYangWnd) end
	local durationFunc = function(time) self:UpdateDesLabel(playerId,time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,countDown,durationFunc,endFunc)
end

function LuaDaFuWongXiuYangWnd:UpdateDesLabel(playerid,time)
    --local text = LocalString.GetString("[%s]%s[-]在%d回合内无法行动并无法收取过路费。[bcbcbc](%d秒后关闭)[-]")
    local playerInfo = LuaDaFuWongMgr.PlayerInfo[playerid]
    local namecolor = self.m_labelColorList[playerInfo.round]
    local buffRound = playerInfo.BuffInfo[LuaDaFuWongMgr.SpecialId[1].BuffId] or 0
    self.m_DescLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_CannotMove_Desc",namecolor,playerInfo.name,buffRound,time)
    --self.m_DescLabel.text = SafeStringFormat3(text,namecolor,playerInfo.name,buffRound,time)
end
function LuaDaFuWongXiuYangWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
        local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
			Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
            Name.text = playerData.name
        end
    end
end
function LuaDaFuWongXiuYangWnd:GetTexturePath(index)
    local name = self.m_headIconList[index]
    return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",name)
end

function LuaDaFuWongXiuYangWnd:OnStageUpdate(CurStage)
    if CurStage ~= EnumDaFuWengStage.RoundCheckAnimation and CurStage ~= EnumDaFuWengStage.RandomEventDog and CurStage ~= EnumDaFuWengStage.RoundCardAnimation then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongXiuYangWnd)
    end
end
function LuaDaFuWongXiuYangWnd:OnDisable()
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongXiuYangWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end
--@region UIEvent

--@endregion UIEvent

