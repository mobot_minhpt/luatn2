require("3rdParty/ScriptEvent")
require("common/common_include")
local CZuoQiMgr=import "L10.Game.CZuoQiMgr"
local LuaGameObject=import "LuaGameObject"
local EnumMapiGender = import "L10.Game.EnumMapiGender"
local LocalString = import "LocalString"

CLuaMapiFanyuInfoWnd=class()
RegistClassMember(CLuaMapiFanyuInfoWnd,"marryStateLabel")
RegistClassMember(CLuaMapiFanyuInfoWnd,"genderLabel")

function CLuaMapiFanyuInfoWnd:Awake()
    self.marryStateLabel=LuaGameObject.GetChildNoGC(self.transform,"MarryStateLabel").label
    self.genderLabel=LuaGameObject.GetChildNoGC(self.transform,"GenderLabel").label
end

function CLuaMapiFanyuInfoWnd:Init()
	self.marryStateLabel.text = CZuoQiMgr.Lua_MapiFanyuWndFanyued and LocalString.GetString("已繁育") or LocalString.GetString("未繁育")
	self.genderLabel.text = (CZuoQiMgr.Lua_MapiFanyuWndGender == EnumMapiGender.eMale) and LocalString.GetString("阳") or LocalString.GetString("阴")
end

return CLuaMapiFanyuInfoWnd