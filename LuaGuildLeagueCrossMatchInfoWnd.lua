local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UITabBar = import "L10.UI.UITabBar"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumDayName = import "L10.Game.EnumDayName"

LuaGuildLeagueCrossMatchInfoWnd = class()

RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoView")      --小组赛
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_RankMatchInfoView")       --积分赛
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_TabBar")                  --子页面切换TabBar
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_RefreshButton")
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GambleButton")

RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoDetailView")    --小组赛对阵详情
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoDetailRoundTable")
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoDetailRoundItemTemplate")
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoScoreView")     --小组赛对阵积分
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoScoreGrid")
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoScoreItemTemplate")
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoTabBar")        --子页面切换TabBar
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoLastTabIndex")  --上次切过的TabBar Index

RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_RankMatchInfoTabBar")        --子页面切换TabBar
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_RankMatchInfoTable")
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_RankMatchInfoItemTemplate")


RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchInfoTbl") 
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_GroupMatchScoreInfoTbl")
RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_RankMatchInfoTbl") 

RegistClassMember(LuaGuildLeagueCrossMatchInfoWnd, "m_RankMatchInfoRound3TitleTbl") 

function LuaGuildLeagueCrossMatchInfoWnd:Awake()
    self.m_GroupMatchInfoView = self.transform:Find("GroupMatchInfoView").gameObject
    self.m_RankMatchInfoView = self.transform:Find("RankMatchInfoView").gameObject
    self.m_TabBar = self.transform:GetComponent(typeof(UITabBar))
    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTabChange(go, index)
    end), true)
    self.m_RefreshButton = self.transform:Find("RefreshButton").gameObject
    self.m_GambleButton = self.transform:Find("GambleButton").gameObject

    UIEventListener.Get(self.m_RefreshButton).onClick = DelegateFactory.VoidDelegate(function() self:OnRefreshButtonClick() end)
    UIEventListener.Get(self.m_GambleButton).onClick = DelegateFactory.VoidDelegate(function() self:OnGambleButtonClick() end)
    -- Group Match
    self.m_GroupMatchInfoDetailView = self.transform:Find("GroupMatchInfoView/DetailView").gameObject
    self.m_GroupMatchInfoDetailRoundTable = self.m_GroupMatchInfoDetailView.transform:Find("Table"):GetComponent(typeof(UITable))
    self.m_GroupMatchInfoDetailRoundItemTemplate = self.m_GroupMatchInfoDetailView.transform:Find("RoundItem").gameObject
    self.m_GroupMatchInfoScoreView = self.transform:Find("GroupMatchInfoView/ScoreView").gameObject
    self.m_GroupMatchInfoScoreGrid = self.m_GroupMatchInfoScoreView.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.m_GroupMatchInfoScoreItemTemplate = self.m_GroupMatchInfoScoreView.transform:Find("GroupItem").gameObject
    self.m_GroupMatchInfoTabBar = self.m_GroupMatchInfoView.transform:Find("TabBar"):GetComponent(typeof(UITabBar))
    self.m_GroupMatchInfoTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnGroupMatchInfoTabChange(go, index)
    end)

    
    self.m_GroupMatchInfoDetailRoundItemTemplate:SetActive(false)
    self.m_GroupMatchInfoScoreItemTemplate:SetActive(false)

    -- Rank Match
    self.m_RankMatchInfoTable = self.m_RankMatchInfoView.transform:Find("Table"):GetComponent(typeof(UITable))
    self.m_RankMatchInfoItemTemplate = self.m_RankMatchInfoView.transform:Find("Item").gameObject
    self.m_RankMatchInfoTabBar = self.m_RankMatchInfoView.transform:Find("TabBar"):GetComponent(typeof(UITabBar))
    self.m_RankMatchInfoTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnRankMatchInfoTabChange(go, index)
    end)
    self.m_RankMatchInfoItemTemplate:SetActive(false)

    self.m_RankMatchInfoRound3TitleTbl = LuaGuildLeagueCrossMgr:GetRankMatchRound3TitleInfo()
end

function LuaGuildLeagueCrossMatchInfoWnd:Init()
    --根据开赛时间选择默认的Tab
    self.m_TabBar:ChangeTab(LuaGuildLeagueCrossMgr:IsGroupMatchOver() and 1 or 0, false)
end

function LuaGuildLeagueCrossMatchInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryGLCGroupMatchInfoResult", self, "QueryGLCGroupMatchInfoResult")
    g_ScriptEvent:AddListener("QueryGLCGroupScoreInfoResultSuccess", self, "QueryGLCGroupScoreInfoResultSuccess")
    g_ScriptEvent:AddListener("QueryGLCRankMatchInfoResultSuccess", self, "QueryGLCRankMatchInfoResultSuccess")
end

function LuaGuildLeagueCrossMatchInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryGLCGroupMatchInfoResult", self, "QueryGLCGroupMatchInfoResult")
    g_ScriptEvent:RemoveListener("QueryGLCGroupScoreInfoResultSuccess", self, "QueryGLCGroupScoreInfoResultSuccess")
    g_ScriptEvent:RemoveListener("QueryGLCRankMatchInfoResultSuccess", self, "QueryGLCRankMatchInfoResultSuccess")
end

function LuaGuildLeagueCrossMatchInfoWnd:OnTabChange(go, index)
    if index == 0 then
        self.m_GroupMatchInfoView:SetActive(true)
        self.m_RankMatchInfoView:SetActive(false)
        self:InitGroupMatchInfoView()
    else
        self.m_GroupMatchInfoView:SetActive(false)
        self.m_RankMatchInfoView:SetActive(true)
        if self.m_RankMatchInfoTabBar.SelectedIndex == -1 then
            self.m_RankMatchInfoTabBar:ChangeTab(0, false)
        end
    end
end

function LuaGuildLeagueCrossMatchInfoWnd:OnGroupMatchInfoTabChange(go, index)
    self.m_GroupMatchInfoLastTabIndex = index
    if index>=0 and index<=3 then
        self.m_GroupMatchInfoDetailView:SetActive(true)
        self.m_GroupMatchInfoScoreView:SetActive(false)
        self:InitGroupMatchInfoDetailView(index+1)
    else -- 小组赛积分
        self.m_GroupMatchInfoDetailView:SetActive(false)
        self.m_GroupMatchInfoScoreView:SetActive(true)
        self:InitGroupMatchInfoScoreView()
    end
end

function LuaGuildLeagueCrossMatchInfoWnd:OnRankMatchInfoTabChange(go, index)
    self:InitRankMatchInfoView(index)
end

function LuaGuildLeagueCrossMatchInfoWnd:OnRefreshButtonClick()
    if self.m_GroupMatchInfoView.activeSelf then
        if self.m_GroupMatchInfoDetailView.activeSelf then
            LuaGuildLeagueCrossMgr:QueryGLCMatchInfo(true)
        else
            LuaGuildLeagueCrossMgr:QueryGLCGroupScoreInfo()
        end
    else
        LuaGuildLeagueCrossMgr:QueryGLCMatchInfo(false)

    end
end

function LuaGuildLeagueCrossMatchInfoWnd:OnGambleButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossGambleInfo()
end
------------------------
--BEGIN Group Match Info View
------------------------
-- GroupMatchView
function LuaGuildLeagueCrossMatchInfoWnd:InitGroupMatchInfoView()
    if self.m_GroupMatchInfoLastTabIndex == nil then --首次打开页面时，查询一次信息
        LuaGuildLeagueCrossMgr:QueryGLCMatchInfo(true)
    else
        self.m_GroupMatchInfoTabBar:ChangeTab(self.m_GroupMatchInfoLastTabIndex, false)
    end
end

function LuaGuildLeagueCrossMatchInfoWnd:QueryGLCGroupMatchInfoResult(belongGroup, groupMatchTbl)
    self.m_GroupMatchInfoTbl = groupMatchTbl
    if self.m_GroupMatchInfoLastTabIndex == nil then --默认选中自己服务器所在分组
        self.m_GroupMatchInfoLastTabIndex = belongGroup-1
        if self.m_GroupMatchInfoView.activeSelf then
            self.m_GroupMatchInfoTabBar:ChangeTab(self.m_GroupMatchInfoLastTabIndex, false)
        end
    elseif self.m_GroupMatchInfoDetailView.activeSelf and self.m_GroupMatchInfoLastTabIndex<LuaGuildLeagueCrossMgr.m_MAX_GROUP_COUNT then
        self:InitGroupMatchInfoDetailView(self.m_GroupMatchInfoLastTabIndex+1)
    end
end

function LuaGuildLeagueCrossMatchInfoWnd:InitGroupMatchInfoDetailView(groupIndex)
    local roundInfoTbl = self.m_GroupMatchInfoTbl and self.m_GroupMatchInfoTbl[groupIndex] or {}
    local n = self.m_GroupMatchInfoDetailRoundTable.transform.childCount
    for i=0,n-1 do
        local child = self.m_GroupMatchInfoDetailRoundTable.transform:GetChild(i)
        child.gameObject:SetActive(false)
    end

    local index = 0
    for i=1, #roundInfoTbl do
        local go = nil
		if index<n then
			go = self.m_GroupMatchInfoDetailRoundTable.transform:GetChild(index).gameObject
		else
			go = CUICommonDef.AddChild(self.m_GroupMatchInfoDetailRoundTable.gameObject, self.m_GroupMatchInfoDetailRoundItemTemplate)
		end
		index = index+1
        go:SetActive(true)
        local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local timeLabel = go.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
        local matchinfo1 = go.transform:Find("MatchInfoItem1").gameObject
        local matchinfo2 = go.transform:Find("MatchInfoItem2").gameObject
        nameLabel.text = SafeStringFormat3(LocalString.GetString("小组赛第%d轮"), i)
        GuildLeagueCross_Agenda.Foreach(function(key, data)
            if data.Type == 1 and data.Round == i then
                local beginTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.BeginTime)
                local beginTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp)
                timeLabel.text = ToStringWrap(beginTime, LocalString.GetString("MM月dd日 HH:mm ")) .. LocalString.GetString("开战")  
            end
        end)
        self:InitOneGroupMatchInfo(matchinfo1, roundInfoTbl[i][1])
        self:InitOneGroupMatchInfo(matchinfo2, roundInfoTbl[i][2])
    end
    self.m_GroupMatchInfoDetailRoundTable:Reposition()
end

function LuaGuildLeagueCrossMatchInfoWnd:InitOneGroupMatchInfo(go, data)
    local battleIcon = go.transform:Find("BattleIcon").gameObject
    local vsIcon = go.transform:Find("VSIcon").gameObject
    local infoButton = go.transform:Find("InfoButton").gameObject

    local isInBattle = data.hasInfo and data.winServerId<=0
    battleIcon:SetActive(isInBattle)
    vsIcon:SetActive(not isInBattle)
    infoButton:SetActive(data.hasInfo)
    local server1 = go.transform:Find("Server1")
    local server2 = go.transform:Find("Server2")
    self:InitOneServerInGroupMatch(server1, data.serverId1, data.serverName1, data.score1, data.hasInfo, data.winServerId)
    self:InitOneServerInGroupMatch(server2, data.serverId2, data.serverName2, data.score2, data.hasInfo, data.winServerId)

    UIEventListener.Get(server1.gameObject).onClick = DelegateFactory.VoidDelegate(function() LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(data.serverId1, data.serverName1) end)
    UIEventListener.Get(server2.gameObject).onClick = DelegateFactory.VoidDelegate(function() LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(data.serverId2, data.serverName2) end)
    if infoButton.activeSelf then
        UIEventListener.Get(infoButton).onClick = DelegateFactory.VoidDelegate(function() LuaGuildLeagueCrossMgr:ShowServerMatchDetailInfoWnd(data.matchIndex) end)

    end
end

function LuaGuildLeagueCrossMatchInfoWnd:InitOneServerInGroupMatch(transRoot, serverId, serverName, score, hasBattleInfo, winServerId)
    local serverResultIcon = transRoot:Find("ResultIcon"):GetComponent(typeof(UISprite))
    local serverNameLabel = transRoot:Find("NameLabel"):GetComponent(typeof(UILabel))
    local serverScoreLabel = transRoot:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local isMyServer = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() == serverId or false
    serverNameLabel.text = SafeStringFormat3("%s%s", isMyServer and "[79ff7c]" or "", serverName)

    if hasBattleInfo and winServerId>0 then --比赛已结束
        serverResultIcon.gameObject:SetActive(true)
        serverResultIcon.spriteName = (winServerId == serverId) and g_sprites.CommonWinSpriteName or g_sprites.CommonLoseSpriteName
        serverScoreLabel.gameObject:SetActive(true)
        serverScoreLabel.text = tostring(score)
    else --比赛进行中或尚未开始
        serverResultIcon.gameObject:SetActive(false)
        serverScoreLabel.gameObject:SetActive(false)
    end
end

-- ScoreView
function LuaGuildLeagueCrossMatchInfoWnd:InitGroupMatchInfoScoreView()
    if self.m_GroupMatchScoreInfoTbl == nil then
        LuaGuildLeagueCrossMgr:QueryGLCGroupScoreInfo()
    end
    self:InitAllGroupScoreInfo()
end

function LuaGuildLeagueCrossMatchInfoWnd:QueryGLCGroupScoreInfoResultSuccess(scoreInfoTbl)
    self.m_GroupMatchScoreInfoTbl = scoreInfoTbl
    self:InitAllGroupScoreInfo()
end

function LuaGuildLeagueCrossMatchInfoWnd:InitAllGroupScoreInfo()
    local n = self.m_GroupMatchInfoScoreGrid.transform.childCount
    for i=0,n-1 do
        local child = self.m_GroupMatchInfoScoreGrid.transform:GetChild(i)
        child.gameObject:SetActive(false)
    end
    local scoreInfoTbl = self.m_GroupMatchScoreInfoTbl
    if scoreInfoTbl == nil then return end
    local tbl = {LocalString.GetString("麒麟"), LocalString.GetString("凤鸟"), LocalString.GetString("元龟"), LocalString.GetString("龙马")}
    local index = 0
    for i=1, math.min(#tbl, #scoreInfoTbl) do
        local go = nil
		if index<n then
			go = self.m_GroupMatchInfoScoreGrid.transform:GetChild(index).gameObject
		else
			go = CUICommonDef.AddChild(self.m_GroupMatchInfoScoreGrid.gameObject, self.m_GroupMatchInfoScoreItemTemplate)
		end
		index = index+1
        go:SetActive(true)
        self:InitOneGroupScoreInfo(go.transform, tbl[i], scoreInfoTbl[i])
    end
    self.m_GroupMatchInfoScoreGrid:Reposition()
end

function LuaGuildLeagueCrossMatchInfoWnd:InitOneGroupScoreInfo(transRoot, groupName, scoreInfo)
    transRoot:Find("Header/IndexLabel"):GetComponent(typeof(UILabel)).text = groupName
    local template = transRoot:Find("Item").gameObject
    local table = transRoot:Find("Table"):GetComponent(typeof(UITable))
    template:SetActive(false)
    Extensions.RemoveAllChildren(table.transform)
    for i=1, #scoreInfo do
        local child = CUICommonDef.AddChild(table.gameObject, template)
        child:SetActive(true)
        local info = scoreInfo[i]
        local indexLabel = child.transform:Find("IndexLabel"):GetComponent(typeof(UILabel))
        local serverNameLabel = child.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
        local winCountLabel = child.transform:Find("WinCountLabel"):GetComponent(typeof(UILabel))
        local scoreLabel = child.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
        indexLabel.text = tostring(info.index)
        local isMyServer = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() == info.serverId or false
        serverNameLabel.text = SafeStringFormat3("%s%s", isMyServer and "[79ff7c]" or "", info.serverName)
        winCountLabel.text = tostring(info.winCount)
        scoreLabel.text = tostring(info.score)
        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function() LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(info.serverId, info.serverName) end)
    end
    table:Reposition()
end


------------------------
--END Group Match Info View
------------------------
------------------------
--BEGIN Rank Match Info View
------------------------
function LuaGuildLeagueCrossMatchInfoWnd:InitRankMatchInfoView(index)
    if self.m_RankMatchInfoTbl == nil then
        self.m_RankMatchInfoTbl = LuaGuildLeagueCrossMgr:GetDefultRankMatchInfo()
        LuaGuildLeagueCrossMgr:QueryGLCMatchInfo(false)
    end
    local agendaData = {}
    for i=EnumGuildLeagueCrossRound.eRankRound1, EnumGuildLeagueCrossRound.eRankRound3 do
        local data = GuildLeagueCross_Agenda.GetData(i)
        local tbl = {}
        local beginTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.BeginTime)
        local beginTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp)
        local weekdayStr = EnumDayName.GetDay(beginTime.DayOfWeek)
        tbl.isToday = (CServerTimeMgr.Inst:DayDiff(beginTime, CServerTimeMgr.Inst:GetZone8Time()) == 0)
        if data.ID == EnumGuildLeagueCrossRound.eRankRound3 then
            tbl.timeStr = ToStringWrap(beginTime, LocalString.GetString("MM月dd日 ")) .. weekdayStr
        else 
            tbl.timeStr = ToStringWrap(beginTime, LocalString.GetString("MM月dd日 ")) .. weekdayStr .. ToStringWrap(beginTime, " HH:mm")
        end
        table.insert(agendaData, tbl)
    end

    self:InitReusableTable(self.m_RankMatchInfoTable, self.m_RankMatchInfoItemTemplate, #agendaData, function(childIdx, transRoot)
        self:InitOneRankMatchInfo(transRoot, childIdx+1, childIdx*4 + 1, agendaData[childIdx+1].isToday, agendaData[childIdx+1].timeStr)
    end) 
end

function LuaGuildLeagueCrossMatchInfoWnd:QueryGLCRankMatchInfoResultSuccess(infoTbl)
    self.m_RankMatchInfoTbl = infoTbl
    self:InitRankMatchInfoView(self.m_RankMatchInfoTabBar.SelectedIndex)
end


function LuaGuildLeagueCrossMatchInfoWnd:InitOneRankMatchInfo(transRoot, roundOffset, startOrder, isToday, timeStr)
    local timeLabel = transRoot:Find("Label"):GetComponent(typeof(UILabel))
    local normalTimeBg = transRoot:Find("Label/NormalBg").gameObject
    local highlightTimeBg = transRoot:Find("Label/HighlightBg").gameObject
    timeLabel.text = timeStr
    timeLabel.alpha = isToday and 1 or 0.3
    normalTimeBg:SetActive(not isToday)
    highlightTimeBg:SetActive(isToday)

    local battleItem = transRoot:Find("BattleItem").gameObject
    local battleTable = transRoot:Find("BattleTable"):GetComponent(typeof(UITable))
    battleItem:SetActive(false)
    local childCount = 4
    self:InitReusableTable(battleTable, battleItem, childCount, function(childIdx, childTrans)
        local orderLabel = childTrans:Find("OrderLabel"):GetComponent(typeof(UILabel))
        local battleIcon = childTrans:Find("BattleIcon").gameObject
        local infoButton = childTrans:Find("InfoButton").gameObject
        local descLabel = childTrans:Find("DescLabel"):GetComponent(typeof(UILabel))
        orderLabel.text = tostring(startOrder + childIdx)
        local data = self.m_RankMatchInfoTbl[self.m_RankMatchInfoTabBar.SelectedIndex+1][roundOffset][childIdx+1]
        battleIcon:SetActive(data.onGoing and data.winServerId==0 or false)
        infoButton:SetActive(data.matchIndex~=nil)

        if roundOffset==3 then
            descLabel.alpha = 1
            local titleInfo = self.m_RankMatchInfoRound3TitleTbl[self.m_RankMatchInfoTabBar.SelectedIndex+1][childIdx+1]
            descLabel.text = titleInfo.title
            descLabel.color = titleInfo.color
            descLabel.transform:Find("FinalBg").gameObject:SetActive(titleInfo.showBg)
        else
            descLabel.alpha = 0
        end
        
        local server1 = childTrans:Find("ServerItem1")
        local server2 = childTrans:Find("ServerItem2")
        self:InitOneServerInRankMatch(server1, data.matchIndex, data.serverId1, data.serverName1, data.score1, data.winServerId)
        self:InitOneServerInRankMatch(server2, data.matchIndex, data.serverId2, data.serverName2, data.score2, data.winServerId)
        if infoButton.activeSelf then
            UIEventListener.Get(infoButton).onClick = DelegateFactory.VoidDelegate(function() LuaGuildLeagueCrossMgr:ShowServerMatchDetailInfoWnd(data.matchIndex) end)
        end
    end)   
end

function LuaGuildLeagueCrossMatchInfoWnd:InitOneServerInRankMatch(transRoot, matchIndex, serverId, serverName, score, winServerId)
    local item1Root = transRoot:Find("Item1")
    local item2Root = transRoot:Find("Item2")
    if matchIndex then
        item1Root.gameObject:SetActive(true)
        item2Root.gameObject:SetActive(false)

        local serverResultIcon = item1Root:Find("ResultIcon"):GetComponent(typeof(UISprite))
        local serverNameLabel = item1Root:Find("NameLabel"):GetComponent(typeof(UILabel))
        local serverScoreLabel = item1Root:Find("ScoreLabel"):GetComponent(typeof(UILabel))
        local isMyServer = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() == serverId or false
        serverNameLabel.text = SafeStringFormat3("%s%s", isMyServer and "[79ff7c]" or "", serverName)
        if winServerId>0 then --比赛已结束
            serverResultIcon.gameObject:SetActive(true)
            serverResultIcon.spriteName = (winServerId==serverId) and g_sprites.CommonWinSpriteName or g_sprites.CommonLoseSpriteName
            serverScoreLabel.gameObject:SetActive(true)
            serverScoreLabel.text = tostring(score)
        else --比赛进行中或尚未开始
            serverResultIcon.gameObject:SetActive(false)
            serverScoreLabel.gameObject:SetActive(false)
        end
        UIEventListener.Get(item1Root.gameObject).onClick = DelegateFactory.VoidDelegate(function() LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(serverId, serverName) end)
    else
        item1Root.gameObject:SetActive(false)
        item2Root.gameObject:SetActive(true)
        item2Root:Find("NameLabel"):GetComponent(typeof(UILabel)).text = serverName
    end
end

------------------------
--END Rank Match Info View
------------------------
--通用的复用table节点机制
function LuaGuildLeagueCrossMatchInfoWnd:InitReusableTable(table, template, maxItemCount, initFunc)
    local n = table.transform.childCount
    for i=0,n-1 do
        local child = table.transform:GetChild(i)
        child.gameObject:SetActive(false)
    end
    local idx = 0
    for i=0, maxItemCount-1 do
        local go = idx<n and table.transform:GetChild(idx).gameObject or CUICommonDef.AddChild(table.gameObject, template)
		idx = idx+1
        go:SetActive(true)
        initFunc(i, go.transform)
    end
    table:Reposition()
end