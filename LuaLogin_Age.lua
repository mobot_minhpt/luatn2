local UIScrollView = import "UIScrollView"
local CLoginMgr=import "L10.Game.CLoginMgr"

local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaLogin_Age = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLogin_Age, "OKButton", "OKButton", GameObject)
RegistChildComponent(LuaLogin_Age, "Content", "Content", GameObject)
RegistChildComponent(LuaLogin_Age, "Table", "Table", GameObject)
RegistChildComponent(LuaLogin_Age, "ScrollView", "ScrollView", UIScrollView)

--@endregion RegistChildComponent end

function LuaLogin_Age:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OKButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)


    --@endregion EventBind end
end

function LuaLogin_Age:Init()
    local msg = nil
    if CLoginMgr.Inst:IsNetEaseOfficialLogin() then
        msg = g_MessageMgr:FormatMessage("Announcement_Age_Tip")
    else
        msg = g_MessageMgr:FormatMessage("Announcement_Age_Tip_QUDAO")
    end
    -- local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    -- if info == nil then
    --     return
    -- end

    -- do
    --     local i = 0
    --     while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.Content)
            paragraphGo:SetActive(true)
            paragraphGo:GetComponent(typeof(UILabel)).text = msg
    --         i = i + 1
    --     end
    -- end

    self.Table:GetComponent(typeof(UITable)):Reposition()
    self.ScrollView:ResetPosition()

end

--@region UIEvent

function LuaLogin_Age:OnOKButtonClick()
    CUIManager.CloseUI(CLuaUIResources.Login_Age)
end


--@endregion UIEvent

