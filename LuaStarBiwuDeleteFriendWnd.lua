local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Constants = import "L10.Game.Constants"
local UISprite = import "UISprite"
local Profession = import "L10.Game.Profession"
local NGUIText = import "NGUIText"

CLuaStarBiwuDeleteFriendWnd = class()
CLuaStarBiwuDeleteFriendWnd.Path = "ui/starbiwu/LuaStarBiwuDeleteFriendWnd"

RegistClassMember(CLuaStarBiwuDeleteFriendWnd, "m_GridView")
RegistClassMember(CLuaStarBiwuDeleteFriendWnd, "m_SelectSprite")
RegistClassMember(CLuaStarBiwuDeleteFriendWnd, "m_SelectIndex")

function CLuaStarBiwuDeleteFriendWnd:Init()
  local bNoFriend = not CLuaStarBiwuMgr.m_FriendList or #CLuaStarBiwuMgr.m_FriendList <= 0
  self.transform:Find("Anchor/NoFriendTip").gameObject:SetActive(bNoFriend)
  self.m_GridView = self.transform:Find("Anchor/AdvView"):GetComponent(typeof(QnAdvanceGridView))
  self.m_GridView.gameObject:SetActive(not bNoFriend)
  if bNoFriend then return end

  self.m_GridView.m_DataSource = DefaultTableViewDataSource.Create(function()
			return #CLuaStarBiwuMgr.m_FriendList
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)
  self.m_GridView:ReloadData(true, false)
end

function CLuaStarBiwuDeleteFriendWnd:InitItem(obj, index)
  local bgSprite = obj.transform:GetComponent(typeof(UISprite))
  bgSprite.spriteName = index % 2 == 0 and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName
  UIEventListener.Get(bgSprite.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
  	if self.m_SelectSprite then self.m_SelectSprite.spriteName = self.m_SelectIndex % 2 == 0 and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName end
  	self.m_SelectIndex = index
  	self.m_SelectSprite = bgSprite
  	bgSprite.spriteName = Constants.ChosenItemBgSpriteName
  end)

  local memberData = CLuaStarBiwuMgr.m_HistoryDataTable[CLuaStarBiwuMgr.m_HistoryTeamIndex]["TeamTable"][index]
  obj.transform:Find("LeaderFlag").gameObject:SetActive(memberData.Id == CLuaStarBiwuMgr.m_HistoryDataTable[CLuaStarBiwuMgr.m_HistoryTeamIndex]["LeaderId"])
  obj.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel)).text = memberData.ServerName
  obj.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = memberData.Name
  local levelLabel = obj.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
  levelLabel.text = "lv."..(memberData.FeiShengGrade > 0 and memberData.FeiShengGrade or memberData.Grade)
  levelLabel.color = memberData.FeiShengGrade > 0 and NGUIText.ParseColor24(Constants.ColorOfFeiSheng, 0) or Color.white
  obj.transform:Find("ClassSprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(memberData.Class)
end
