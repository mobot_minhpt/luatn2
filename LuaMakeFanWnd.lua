local GameObject = import "UIPanel"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local Flip = import "UIBasicSprite+Flip"
local Quaternion = import "UnityEngine.Quaternion"
local CMainCamera = import "L10.Engine.CMainCamera"
local Input = import "UnityEngine.Input"
local Physics = import "UnityEngine.Physics"
local LayerDefine = import "L10.Engine.LayerDefine"
local Animation = import "UnityEngine.Animation"

LuaMakeFanWnd = class()
LuaMakeFanWnd.fanData = nil
LuaMakeFanWnd.duration = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMakeFanWnd, "SelectPanel", "SelectPanel", GameObject)
RegistChildComponent(LuaMakeFanWnd, "FanPanel", "FanPanel", GameObject)
RegistChildComponent(LuaMakeFanWnd, "FanRemindLabel", "FanRemindLabel", UILabel)
RegistChildComponent(LuaMakeFanWnd, "BottomBtns", "BottomBtns", GameObject)
RegistChildComponent(LuaMakeFanWnd, "CloseV2Btn", "CloseV2Btn", GameObject)
RegistChildComponent(LuaMakeFanWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaMakeFanWnd, "AlertRemindLabel", "AlertRemindLabel", UILabel)
RegistChildComponent(LuaMakeFanWnd, "CustomizeImageEditor", "CustomizeImageEditor", CCommonLuaScript)
RegistChildComponent(LuaMakeFanWnd, "FanPatternTemplate", "FanPatternTemplate", GameObject)
RegistChildComponent(LuaMakeFanWnd, "Fan", "Fan", GameObject)
RegistChildComponent(LuaMakeFanWnd, "FanSurface", "FanSurface", UIPanel)

--@endregion RegistChildComponent end

function LuaMakeFanWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaMakeFanWnd:Init()

end

--@region UIEvent

--@endregion UIEvent


function LuaMakeFanWnd:InitWndData()
    self.prefixPath = "UI/Texture/Transparent/Material/"
    self:InitSelectPanel()
    
    self.CustomizeImageEditor.gameObject:SetActive(false)
end

function LuaMakeFanWnd:InitSelectPanel()
    self.selectPanelCfg = {}
    self.curSelectData = {{}, {}, {}, {}}
    self.curSelectPositionData = {{}, {}, {}, {}}
    self.selectPanelCfg[1] = {element = {}, title = LocalString.GetString("铺色"), RepeatMaxNum = 1, UniqueRange = {1, 1}, InitRedAlert = true}
    self.selectPanelCfg[2] = {element = {}, title = LocalString.GetString("图案"), RepeatMaxNum = NanDuFanHua_HuiCaiShanSetting.GetData().PatternRepeat, UniqueRange = {1, 1}, InitRedAlert = true}
    local patternNum = NanDuFanHua_HuiCaiShanSetting.GetData().PatternNum
    local patternNumArr = g_LuaUtil:StrSplit(patternNum,";")
    for i = 1, #patternNumArr do
        self.selectPanelCfg[2].UniqueRange[i] = tonumber(patternNumArr[i])
    end
    self.selectPanelCfg[3] = {element = {}, title = LocalString.GetString("题字"), RepeatMaxNum = 1, UniqueRange = {1, 1}, InitRedAlert = true}
    local inscriptionNum = NanDuFanHua_HuiCaiShanSetting.GetData().InscriptionNum
    local inscriptionNumArr = g_LuaUtil:StrSplit(inscriptionNum,";")
    for i = 1, #inscriptionNumArr do
        self.selectPanelCfg[3].UniqueRange[i] = tonumber(inscriptionNumArr[i])
    end
    self.selectPanelCfg[4] = {element = {}, title = LocalString.GetString("印章"), RepeatMaxNum = 1, UniqueRange = {1, 1}, InitRedAlert = true}
    local sealNum = NanDuFanHua_HuiCaiShanSetting.GetData().SealNum
    local sealNumArr = g_LuaUtil:StrSplit(sealNum,";")
    for i = 1, #sealNumArr do
        self.selectPanelCfg[4].UniqueRange[i] = tonumber(sealNumArr[i])
    end
    NanDuFanHua_HuiCaiShanElement.Foreach(function (k, v)
        table.insert(self.selectPanelCfg[v.ElementType].element, {IconResource = v.IconResource, IconRGB = v.IconRGB, PicResource = v.PicResource, PicRGB = v.PicRGB})
    end)

    self.selectPanelScript = LuaSelectPanel:new()
    self.selectPanelScript:Init(self.SelectPanel, self.selectPanelCfg, 1)
    
    self.patternDepth = 5
    self.recordPatternDepthData = {}
    
    self.minPatternScale = NanDuFanHua_HuiCaiShanSetting.GetData().MinPatternScale
    self.maxPatternScale = NanDuFanHua_HuiCaiShanSetting.GetData().MaxPatternScale
    self.patterMaxNum = NanDuFanHua_HuiCaiShanSetting.GetData().PatterMaxNum
end

function LuaMakeFanWnd:RefreshConstUI()
    self.FanPatternTemplate:SetActive(false)
end

function LuaMakeFanWnd:OnDestroy()
    --if self.autoHideWndTicker then
    --    UnRegisterTick(self.autoHideWndTicker)
    --end
end

function LuaMakeFanWnd:RefreshVariableUI()
    if LuaMakeFanWnd.fanData then
        --if self.autoHideWndTicker then
        --    UnRegisterTick(self.autoHideWndTicker)
        --end
        --self.autoHideWndTicker = RegisterTickOnce(function()
        --    CUIManager.CloseUI(CLuaUIResources.MakeFanWnd)
        --end, LuaMakeFanWnd.duration)
        self.transform:GetComponent(typeof(Animation)):Play("makefanwnd_show")

        self.SelectPanel:SetActive(false)
        self.AlertRemindLabel.gameObject:SetActive(false)
        self.FanRemindLabel.gameObject:SetActive(false)
        self.BottomBtns:SetActive(false)
        for i = 1, #LuaMakeFanWnd.fanData do
            if i == 1 then
                local elementData = LuaMakeFanWnd.fanData[i][1]
                local subIndex = elementData[1]
                self.Fan.transform:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(self.selectPanelCfg[i].element[subIndex].PicRGB, 0)
            elseif i == 2 then
                for k, v in pairs(LuaMakeFanWnd.fanData[i]) do
                    local elementListData = LuaMakeFanWnd.fanData[i][k]
                    for elementIdx = 1, #elementListData do
                        local elementData = elementListData[elementIdx]
                        local instance = NGUITools.AddChild(self.FanSurface.gameObject, self.FanPatternTemplate)
                        local subIndex = elementData[1]
                        local tex = instance.transform:GetComponent(typeof(UITexture))
                        instance.transform.localPosition = Vector3(elementData[2], elementData[3], 0)
                        instance.transform.localScale = Vector3.one
                        instance.transform.localEulerAngles = Vector3(0, 0, elementData[4] / 100)
                        instance.transform:GetComponent(typeof(CUITexture)):LoadMaterial(self.prefixPath .. self.selectPanelCfg[i].element[subIndex].PicResource .. ".mat")
                        tex.color = NGUIText.ParseColor24(self.selectPanelCfg[i].element[subIndex].PicRGB, 0)
                        tex.width = elementData[5]
                        tex.height = elementData[6]
                        if elementData[7] == 1 then
                            tex.flip = Flip.Horizontally
                        else
                            tex.flip = Flip.Nothing
                        end
                        tex.depth = elementData[8]
                        instance:SetActive(true)
                    end
                end
            else
                local instance = NGUITools.AddChild(self.FanSurface.gameObject, self.FanPatternTemplate)
                local elementData = LuaMakeFanWnd.fanData[i][1]
                local subIndex = elementData[1]
                local tex = instance.transform:GetComponent(typeof(UITexture))
                instance.transform.localPosition = Vector3(elementData[2], elementData[3], 0)
                instance.transform.localScale = Vector3.one
                instance.transform.localEulerAngles = Vector3(0, 0, elementData[4] / 100)

                instance.transform:GetComponent(typeof(CUITexture)):LoadMaterial(self.prefixPath .. self.selectPanelCfg[i].element[subIndex].PicResource .. ".mat")
                tex.color = NGUIText.ParseColor24(self.selectPanelCfg[i].element[subIndex].PicRGB, 0)
                tex.width = elementData[5]
                tex.height = elementData[6]
                if elementData[7] == 1 then
                    tex.flip = Flip.Horizontally
                else
                    tex.flip = Flip.Nothing
                end
                tex.depth = elementData[8]
                instance:SetActive(true)
            end
        end
    else
        self.transform:GetComponent(typeof(Animation)):Play("makefanwnd_open")

        self.SelectPanel:SetActive(true)
        self.AlertRemindLabel.gameObject:SetActive(true)
        self.FanRemindLabel.gameObject:SetActive(true)
        self.BottomBtns:SetActive(false)
        
        self:OnChangeRemindLabel(1)
    end
end

function LuaMakeFanWnd:OnShowFan()
    self.SelectPanel:SetActive(false)
    self.AlertRemindLabel.gameObject:SetActive(false)
    self.FanRemindLabel.gameObject:SetActive(false)
    self.BottomBtns:SetActive(true)
    --if self.autoHideWndTicker then
    --    UnRegisterTick(self.autoHideWndTicker)
    --end
    --self.autoHideWndTicker = RegisterTickOnce(function()
    --    CUIManager.CloseUI(CLuaUIResources.MakeFanWnd)
    --end, 5000)
    self.transform:GetComponent(typeof(Animation)):Play("makefanwnd_show")
end

function LuaMakeFanWnd:InitUIEvent()
    UIEventListener.Get(self.CloseV2Btn).onClick = DelegateFactory.VoidDelegate(function ()
        CUIManager.CloseUI(CLuaUIResources.MakeFanWnd)
    end)

    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function ()
        self.CloseV2Btn:SetActive(false)
        self.ShareBtn:SetActive(false)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            self.CloseV2Btn:SetActive(true)
            self.ShareBtn:SetActive(true)
        end))
    end)
end

function LuaMakeFanWnd:OnEnable()
    g_ScriptEvent:AddListener("SelectPanel_TryAddSubItem", self, "OnTryAddSubItem")
    g_ScriptEvent:AddListener("SelectPanel_TryCommit", self, "OnTryCommit")
    g_ScriptEvent:AddListener("SelectPanel_ClickTabButton", self, "OnChangeRemindLabel")
    g_ScriptEvent:AddListener("ShowFan", self, "OnShowFan")
end

function LuaMakeFanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SelectPanel_TryAddSubItem", self, "OnTryAddSubItem")
    g_ScriptEvent:RemoveListener("SelectPanel_TryCommit", self, "OnTryCommit")
    g_ScriptEvent:RemoveListener("SelectPanel_ClickTabButton", self, "OnChangeRemindLabel")
    g_ScriptEvent:RemoveListener("ShowFan", self, "OnShowFan")
end 

function LuaMakeFanWnd:OnTryAddSubItem(tabIndex, subIndex)
    --判断能否新增,如果能新增的话, 发消息通知红点状态
    local repeatTime, uniqueNumber = self:CheckIllegal(tabIndex, subIndex)
    local cfg = self.selectPanelCfg[tabIndex]

    if tabIndex == 1 then
        --铺色仅有一种
        self.curSelectData[tabIndex] = {}
        g_ScriptEvent:BroadcastInLua("SelectPanel_AddSubItemAndClearOtherSuccess", tabIndex, subIndex, 1)
    elseif tabIndex == 2 then
        if repeatTime == 0 then
            --新图案, 检查UniqueRange
            if uniqueNumber >= cfg.UniqueRange[2] then
                g_MessageMgr:ShowMessage("MakeFan_Exceed_Unique_Upper_Range")
                return
            end
        else
            --老图案, 检查RepeatTime
            if repeatTime >= cfg.RepeatMaxNum then
                --这个图案选过, 且重复次数>=允许重复的上限
                g_MessageMgr:ShowMessage("MakeFan_Exceed_RepeatMaxNum")
                return
            end
        end

        if #self.curSelectPositionData[2] >= self.patterMaxNum then
            g_MessageMgr:ShowMessage("MakeFan_Exceed_TotalPatternMaxNum")
            return
        end
        
    elseif tabIndex == 3 then
        if self.CustomizeImageEditor.gameObject.activeInHierarchy then
            self:RecoverPatternOrDelete(self.CustomizeImageEditor.m_TargetTexture, tabIndex, subIndex)
            self.CustomizeImageEditor.gameObject:SetActive(false)
        end
        repeatTime, uniqueNumber = self:CheckIllegal(tabIndex, subIndex)

        if repeatTime == 0 then
            --新题字, 检查UniqueRange
            if uniqueNumber >= cfg.UniqueRange[2] then
                self:GetChoseItem(tabIndex, subIndex)
                return
            end
        else
            --老题字, 检查RepeatTime
            if repeatTime >= cfg.RepeatMaxNum then
                --这个题字选过, 且重复次数>=允许重复的上限
                self:GetChoseItem(tabIndex, subIndex)
                return
            end
        end
    elseif tabIndex == 4 then
        if self.CustomizeImageEditor.gameObject.activeInHierarchy then
            self:RecoverPatternOrDelete(self.CustomizeImageEditor.m_TargetTexture, tabIndex, subIndex)
            self.CustomizeImageEditor.gameObject:SetActive(false)
        end
        repeatTime, uniqueNumber = self:CheckIllegal(tabIndex, subIndex)
        
        if repeatTime == 0 then
            --新印章, 检查UniqueRange
            if uniqueNumber >= cfg.UniqueRange[2] then
                self:GetChoseItem(tabIndex, subIndex)
                return
            end
        else
            --老印章, 检查RepeatTime
            if repeatTime >= cfg.RepeatMaxNum then
                --这个印章选过, 且重复次数>=允许重复的上限
                self:GetChoseItem(tabIndex, subIndex)
                return
            end
        end
    end
    
    --成功添加
    self:AddSubItem2Fan(tabIndex, subIndex)
end

function LuaMakeFanWnd:GetChoseItem(tabIndex, subIndex)
    g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectBg", tabIndex, subIndex, false)

    --直接选中老的
    --有一个很强的假设, 只有选中tabIndex=3/4的物品, 且tabIndex 3,4最多只能选一项
    --这个tabIndex是准确的, subIndex要修正
    --获得instance, texDepth, subIndex
    local realTabInedx = tabIndex
    local realSubIndex = self.curSelectData[tabIndex][1]
    local realDepth = self.curSelectPositionData[tabIndex][1][7]
    local realInstance = self.FanSurface.transform:Find(realDepth)
    if realInstance then
        self:OnTouchPattern(realInstance.gameObject, realTabInedx, realSubIndex, realDepth)
    end
end


function LuaMakeFanWnd:CheckRedAlert(tabIndex)
    local _, uniqueNumber = self:CheckIllegal(tabIndex, -1)
    local cfg = self.selectPanelCfg[tabIndex]
    if uniqueNumber >= cfg.UniqueRange[1] and uniqueNumber <= cfg.UniqueRange[2] then
        return true
    else
        return false
    end
end

function LuaMakeFanWnd:CheckIllegal(tabIndex, subIndex)
    local repeatTime = 0
    local uniqueNumber = 0
    local uniqueTbl = {}
    for i = 1, #self.curSelectData[tabIndex] do
        if subIndex == self.curSelectData[tabIndex][i] then
            repeatTime = repeatTime + 1
        end
        if uniqueTbl[self.curSelectData[tabIndex][i]] then
            --统计过这个数据了
        else
            uniqueTbl[self.curSelectData[tabIndex][i]] = true
            uniqueNumber = uniqueNumber + 1
        end
    end
    return repeatTime, uniqueNumber
end

function LuaMakeFanWnd:OnTryRemoveSubItem(tabIndex, subIndex)
    --判断能否删除,如果能删除的话,发消息通知红点状态
    local repeatTime, _ = self:CheckIllegal(tabIndex, subIndex)
    g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeAlertState", tabIndex, not self:CheckRedAlert(tabIndex))
    g_ScriptEvent:BroadcastInLua("SelectPanel_RemoveSubItemSuccess", tabIndex, subIndex, repeatTime)
end

function LuaMakeFanWnd:OnTryCommit()
    local canCommit = true
    for i = 1, #self.selectPanelCfg do
        canCommit = canCommit and self:CheckRedAlert(i)
    end

    if canCommit then
        for i = 2, #self.selectPanelCfg do
            if #self.curSelectData[i] ~= #self.curSelectPositionData[i] then
                g_MessageMgr:ShowMessage("MakeFan_Select_Postion_Length_NOT_Equal")
                return
            end
        end
        
        local sendData = {{}, {}, {}, {}}
        table.insert(sendData[1], {self.curSelectData[1][1]})
        for i = 3, 4 do
            local tbl = {self.curSelectData[i][1]}
            for posIdx = 1, 7 do
                table.insert(tbl, self.curSelectPositionData[i][1][posIdx])
            end
            table.insert(sendData[i], tbl)    
        end
        
        --reCalculate PatternData
        local patternUniqueIdx = 0
        local patternHashTbl = {}
        for i = 1, #self.curSelectData[2] do
            local selectPatternIdx = self.curSelectData[2][i]
            local positionData = self.curSelectPositionData[2][i]
            local tbl = {}
            table.insert(tbl, selectPatternIdx)
            for posIdx = 1, 7 do
                table.insert(tbl, positionData[posIdx])
            end
            if patternHashTbl[selectPatternIdx] then
                --patternIdx 已经存储过了
                table.insert(sendData[2][patternHashTbl[selectPatternIdx]], tbl)
            else    
                table.insert(sendData[2], {tbl})
                patternUniqueIdx = patternUniqueIdx + 1
                patternHashTbl[selectPatternIdx] = patternUniqueIdx
            end
        end
        if self.CustomizeImageEditor.gameObject.activeInHierarchy then
            local unsaveTex = self.CustomizeImageEditor.m_TargetTexture
            if not CommonDefs.IsNull(unsaveTex.gameObject) then
                GameObject.Destroy(unsaveTex.gameObject)
            end
            self.CustomizeImageEditor.gameObject:SetActive(false)
        end

        --Gac2Gas.SetHuiCaiShanResult(g_MessagePack.pack( sendData))
        self:OnShowFan()
    else
        g_MessageMgr:ShowMessage("MakeFan_CANNOT_Commit")
    end
end 

function LuaMakeFanWnd:OnChangeRemindLabel(idx)
    local strKeyList = {"PuSe", "TuAn", "TiZi", "YinZhang"}
    self.FanRemindLabel.text = g_MessageMgr:FormatMessage("MakeFan_Reminder_" .. strKeyList[idx])
    self.AlertRemindLabel.text = g_MessageMgr:FormatMessage("MakeFan_AlertTips_" .. strKeyList[idx])
    self.AlertRemindLabel.gameObject:SetActive(not self:CheckRedAlert(idx))
end 

function LuaMakeFanWnd:AddSubItem2Fan(tabIndex, subIndex)
    if tabIndex == 1 then
        -- 这个是扇面信息, 只需要染色+存一下idx
        table.insert(self.curSelectData[tabIndex], subIndex)
        g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeAlertState", tabIndex, not self:CheckRedAlert(tabIndex))
        self:OnChangeRemindLabel(tabIndex)
        self.Fan.transform:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(self.selectPanelCfg[tabIndex].element[subIndex].PicRGB, 0)
    else
        --右侧选择添加出来的, 但发现之前已有一个正在编辑中的元素
        if self.CustomizeImageEditor.gameObject.activeInHierarchy then
            --self.CustomizeImageEditor:OnCancleBtnClick()
            --local instance = self.CustomizeImageEditor.m_TargetTexture.gameObject
            --g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectExceptBg", tabIndex, subIndex, false)
            --if not CommonDefs.IsNull(instance) then
            --    GameObject.Destroy(instance)
            --end
            self:RecoverPatternOrDelete(self.CustomizeImageEditor.m_TargetTexture, tabIndex, subIndex)
        end
        
        ---- 其他物料数据, 需要存idx.位置等信息
        ----还要存一份位置信息, 修改和提交的时候使用
        local instance = NGUITools.AddChild(self.FanSurface.gameObject, self.FanPatternTemplate)
        instance.name = self.patternDepth
        instance.transform.localPosition = Vector3.zero
        instance.transform.localScale = Vector3.one
        instance.transform:GetComponent(typeof(CUITexture)):LoadMaterial(self.prefixPath .. self.selectPanelCfg[tabIndex].element[subIndex].PicResource .. ".mat")
        local tex = instance.transform:GetComponent(typeof(UITexture))
        tex.depth = self.patternDepth
        self.patternDepth = self.patternDepth + 1
        tex.color = NGUIText.ParseColor24(self.selectPanelCfg[tabIndex].element[subIndex].PicRGB, 0)
        instance:SetActive(true)
        instance.transform:GetComponent(typeof(Animation)):Play("customizeimageeditor_in")

        tex:MakePixelPerfect()
        local texWidth = tex.width
        local texHeight = tex.height
        if self.ImageEditorFadeOutTicker then
            UnRegisterTick(self.ImageEditorFadeOutTicker)
        end
        self.CustomizeImageEditor:Init(function (go)
            --save data
            local posData = {}
            table.insert(posData, math.floor(instance.transform.localPosition.x))
            table.insert(posData, math.floor(instance.transform.localPosition.y))
            table.insert(posData, math.floor(instance.transform.localEulerAngles.z * 100))

            table.insert(posData, tex.width)
            table.insert(posData, tex.height)
            if tex.flip == Flip.Nothing then
                table.insert(posData, 0)
            else
                table.insert(posData, 1)
            end
            table.insert(posData, tex.depth)
            table.insert(self.curSelectPositionData[tabIndex], posData)
            table.insert(self.curSelectData[tabIndex], subIndex)
            local repeatTime, _ = self:CheckIllegal(tabIndex, subIndex)
            self.recordPatternDepthData[tex.depth] = {tabIndex = tabIndex, subIndex = subIndex, posData = posData}
            g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeAlertState", tabIndex, not self:CheckRedAlert(tabIndex))
            g_ScriptEvent:BroadcastInLua("SelectPanel_AddSubItemSuccess", tabIndex, subIndex, repeatTime)
            g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectBg", tabIndex, subIndex, false)
            self:OnChangeRemindLabel(tabIndex)
            
            UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function ()
                self:OnTouchPattern(instance, tabIndex, subIndex, tex.depth)
            end)
        end,function (go)
        end, math.max(texWidth, tex.height) * self.maxPatternScale, math.max(texWidth, tex.height) * self.minPatternScale, instance.transform:GetComponent(typeof(UITexture)), self.FanSurface, texWidth, texHeight)
        self.CustomizeImageEditor.transform:GetComponent(typeof(Animation)):Play("customizeimageeditor_in")
        UIEventListener.Get(self.CustomizeImageEditor.transform:Find("CancleBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectBg", tabIndex, subIndex, false)
            self.CustomizeImageEditor.transform:GetComponent(typeof(Animation)):Play("customizeimageeditor_out")
            instance.transform:GetComponent(typeof(Animation)):Play("customizeimageeditor_out")
            if self.ImageEditorFadeOutTicker then
                UnRegisterTick(self.ImageEditorFadeOutTicker)
            end
            self.ImageEditorFadeOutTicker = RegisterTickOnce(function()
                if not CommonDefs.IsNull(instance)then
                    GameObject.Destroy(instance)
                end
                self.CustomizeImageEditor.gameObject:SetActive(false)
            end, 100)
        end)
        self.CustomizeImageEditor:ChangeSymmetryBtn(tabIndex == 2)
    end
end

function LuaMakeFanWnd:RecoverPatternOrDelete(instance, newTabIndex, newSubIndex)
    local texDepth = instance.depth
    if self.recordPatternDepthData[texDepth] then
        --储存过数据的, 还原         self.recordPatternDepthData[tex.depth] = {tabIndex = tabIndex, subIndex = subIndex, posData = posData}
        local recordTabIndex = self.recordPatternDepthData[texDepth].tabIndex
        local recordSubIndex = self.recordPatternDepthData[texDepth].subIndex
        local posData = self.recordPatternDepthData[texDepth].posData

        instance.transform.localPosition = Vector3(posData[1], posData[2], 0)
        instance.transform.localScale = Vector3.one
        instance.transform.localEulerAngles = Vector3(0, 0, posData[3] / 100)

        instance.width = posData[4]
        instance.height = posData[5]
        if posData[7] == 1 then
            instance.flip = Flip.Horizontally
        else
            instance.flip = Flip.Nothing
        end
        table.insert(self.curSelectPositionData[recordTabIndex], posData)
        table.insert(self.curSelectData[recordTabIndex], recordSubIndex)
        
        local repeatTime, _ = self:CheckIllegal(recordTabIndex, recordSubIndex)
        g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeAlertState", recordTabIndex, not self:CheckRedAlert(recordTabIndex))
        g_ScriptEvent:BroadcastInLua("SelectPanel_AddSubItemSuccess", recordTabIndex, recordSubIndex, repeatTime)
        g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectBg", recordTabIndex, recordSubIndex, false)
        self:OnChangeRemindLabel(recordTabIndex)
    else    
        --没存过数据的, 删除
        g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectExceptBg", newTabIndex, newSubIndex, false)
        if not CommonDefs.IsNull(instance.gameObject) then
            GameObject.Destroy(instance.gameObject)
        end
    end
end

function LuaMakeFanWnd:OnTouchPattern(instance, tabIndex, subIndex, texDepth)
    if self.CustomizeImageEditor.gameObject.activeInHierarchy then
        --扇面拾取了一个元素, 但发现之前已有一个正在编辑中的元素
        --self.CustomizeImageEditor:OnCancleBtnClick()
        --local subInstance = self.CustomizeImageEditor.m_TargetTexture.gameObject
        --g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectExceptBg", tabIndex, subIndex, false)
        --if not CommonDefs.IsNull(subInstance) then
        --    GameObject.Destroy(subInstance)
        --end
        self:RecoverPatternOrDelete(self.CustomizeImageEditor.m_TargetTexture, tabIndex, subIndex)
    end
    
    local tex = instance.transform:GetComponent(typeof(UITexture))
    --先把已存储的数据删除, 再点勾的时候, 再存储数据
    --find index
    local isFind = false
    for i = 2, 4 do
        if not isFind then
            for k, v in pairs(self.curSelectPositionData[i]) do
                if v[7] and v[7] == texDepth then
                    table.remove(self.curSelectPositionData[i], k)
                    table.remove(self.curSelectData[i], k)
                    isFind = true
                    break
                end
            end
        end
    end
    self:OnTryRemoveSubItem(tabIndex, subIndex)
    g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectBg", tabIndex, subIndex, true)
    if self.ImageEditorFadeOutTicker then
        UnRegisterTick(self.ImageEditorFadeOutTicker)
    end

    tex:MakePixelPerfect()
    local texWidth = tex.width
    local texHeight = tex.height
    self.CustomizeImageEditor:Init(function (go)
        --save data
        local posData = {}
        table.insert(posData, math.floor(instance.transform.localPosition.x))
        table.insert(posData, math.floor(instance.transform.localPosition.y))
        table.insert(posData, math.floor(instance.transform.localEulerAngles.z * 100))

        table.insert(posData, tex.width)
        table.insert(posData, tex.height)
        if tex.flip == Flip.Nothing then
            table.insert(posData, 0)
        else
            table.insert(posData, 1)
        end
        table.insert(posData, tex.depth)
        table.insert(self.curSelectPositionData[tabIndex], posData)
        table.insert(self.curSelectData[tabIndex], subIndex)
        self.recordPatternDepthData[tex.depth] = {tabIndex = tabIndex, subIndex = subIndex, posData = posData}
        local repeatTime, _ = self:CheckIllegal(tabIndex, subIndex)
        g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeAlertState", tabIndex, not self:CheckRedAlert(tabIndex))
        g_ScriptEvent:BroadcastInLua("SelectPanel_AddSubItemSuccess", tabIndex, subIndex, repeatTime)
        g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectBg", tabIndex, subIndex, false)
        self:OnChangeRemindLabel(tabIndex)
    end,function (go)
    end, math.max(texWidth, tex.height) * self.maxPatternScale, math.max(texWidth, tex.height) * self.minPatternScale, tex, self.FanSurface, texWidth, texHeight)
    self.CustomizeImageEditor.transform:GetComponent(typeof(Animation)):Play("customizeimageeditor_in")
    UIEventListener.Get(self.CustomizeImageEditor.transform:Find("CancleBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_ScriptEvent:BroadcastInLua("SelectPanel_ChangeSelectBg", tabIndex, subIndex, false)
        self.CustomizeImageEditor.transform:GetComponent(typeof(Animation)):Play("customizeimageeditor_out")
        instance.transform:GetComponent(typeof(Animation)):Play("customizeimageeditor_out")
        if self.ImageEditorFadeOutTicker then
            UnRegisterTick(self.ImageEditorFadeOutTicker)
        end
        self.ImageEditorFadeOutTicker = RegisterTickOnce(function()
            if not CommonDefs.IsNull(instance) then
                GameObject.Destroy(instance)
            end
            self.CustomizeImageEditor.gameObject:SetActive(false)
        end, 100)
    end)
    self.CustomizeImageEditor:ChangeSymmetryBtn(tabIndex == 2)
end 