local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CRankData = import "L10.UI.CRankData"
local Constants = import "L10.Game.Constants"
local Profession = import "L10.Game.Profession"

LuaChildrenDayPlayFightInfo2020Wnd = class()
RegistChildComponent(LuaChildrenDayPlayFightInfo2020Wnd,"CloseButton", GameObject)
RegistChildComponent(LuaChildrenDayPlayFightInfo2020Wnd,"selfNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayFightInfo2020Wnd,"templateNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayFightInfo2020Wnd,"table", UITable)
RegistChildComponent(LuaChildrenDayPlayFightInfo2020Wnd,"scrollView", UIScrollView)
RegistChildComponent(LuaChildrenDayPlayFightInfo2020Wnd,"timeTip", UILabel)

RegistClassMember(LuaChildrenDayPlayFightInfo2020Wnd, "team1NodeTable")
RegistClassMember(LuaChildrenDayPlayFightInfo2020Wnd, "team2NodeTable")

function LuaChildrenDayPlayFightInfo2020Wnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayFightInfo2020Wnd)
end

function LuaChildrenDayPlayFightInfo2020Wnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaChildrenDayPlayFightInfo2020Wnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaChildrenDayPlayFightInfo2020Wnd:InitRankNodeData(node,data)
	node:SetActive(true)
	node.transform:Find('Name/text'):GetComponent(typeof(UILabel)).text = data.name
	if data.rank <= 0 then
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未上榜')
	else
		if data.rank <= 3 then
			local rankSprite = {Constants.RankFirstSpriteName,Constants.RankSecondSpriteName,Constants.RankThirdSpriteName}
			node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = ''
			node.transform:Find('rank/spe').gameObject:SetActive(true)
			node.transform:Find('rank/spe'):GetComponent(typeof(UISprite)).spriteName = rankSprite[data.rank]
		else
			node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = data.rank
		end

	end
	node.transform:Find('score'):GetComponent(typeof(UILabel)).text = data.score

	node.transform:Find("job"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(data.job)
end

function LuaChildrenDayPlayFightInfo2020Wnd:OnRankDataReady()
	local PlayerRankInfo = CRankData.Inst.MainPlayerRankInfo
	if PlayerRankInfo then
		local selfData = {name = CClientMainPlayer.Inst.Name,rank = PlayerRankInfo.Rank,score = PlayerRankInfo.Value,job = CClientMainPlayer.Inst.Class}
		self:InitRankNodeData(self.selfNode,selfData)
	end

	Extensions.RemoveAllChildren(self.table.transform)

	if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
		do
			local i = 0
			while i < CRankData.Inst.RankList.Count do
				local info = CRankData.Inst.RankList[i]
				local go = NGUITools.AddChild(self.table.gameObject, self.templateNode)
				local data = {name = info.Name,rank = info.Rank,score = info.Value,job = info.Job}
				self:InitRankNodeData(go,data)
				i = i + 1
			end
		end

		self.table:Reposition()
		self.scrollView:ResetPosition()
	end
end

function LuaChildrenDayPlayFightInfo2020Wnd:InitNode(node,info)
	node.transform:Find('Name/text'):GetComponent(typeof(UILabel)).text = info.playerName
	node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = info.rank
	node.transform:Find('score'):GetComponent(typeof(UILabel)).text = info.score
	if info.force == 1 then
		node.transform:Find('b').gameObject:SetActive(true)
		node.transform:Find('r').gameObject:SetActive(false)
	else
		node.transform:Find('b').gameObject:SetActive(false)
		node.transform:Find('r').gameObject:SetActive(true)
	end

	if info.playerId == CClientMainPlayer.Inst.Id then
		node.transform:Find('Name/text'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('score'):GetComponent(typeof(UILabel)).color = Color.green
	end
end

function LuaChildrenDayPlayFightInfo2020Wnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.timeTip.text = g_MessageMgr:FormatMessage('ChildrenDay2020TimeTip')
	self.templateNode:SetActive(false)
	self.selfNode:SetActive(false)
	Extensions.RemoveAllChildren(self.table.transform)

	Gac2Gas.QueryRank(LiuYi2020_Setting.GetData().RankId)

	--(LuaChildrenDay2019Mgr.PengPengCheFightInfo,{rank = rank,playerId=playerId,score=score,force=force,playerName=playerName})
--		if playerInfo.rank > 0 then
--			local node = NGUITools.AddChild(self.table.gameObject, self.templateNode)
--			node:SetActive(true)
--			self:InitNode(node,playerInfo)
--		end
--
end

return LuaChildrenDayPlayFightInfo2020Wnd
