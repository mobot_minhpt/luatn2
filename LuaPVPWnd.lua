--local Shop_PlayScore = import "L10.Game.Shop_PlayScore"
local NGUITools = import "NGUITools"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MenPaiTiaoZhan_Setting = import "L10.Game.MenPaiTiaoZhan_Setting"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local ETickType = import "L10.Engine.ETickType"
local DelegateFactory = import "DelegateFactory"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CTickMgr = import "L10.Engine.CTickMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScene = import "L10.Game.CScene"
local Constants = import "L10.Game.Constants"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CUIManager = import "L10.UI.CUIManager"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Vector3 = import "UnityEngine.Vector3"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CUITexture = import "L10.UI.CUITexture"
local Time = import "UnityEngine.Time"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CButton = import "L10.UI.CButton"


CLuaPVPWnd = class()
RegistClassMember(CLuaPVPWnd,"closeBtn")
--Header
RegistClassMember(CLuaPVPWnd,"selfRankLabel")
RegistClassMember(CLuaPVPWnd,"selfFightScoreLabel")
RegistClassMember(CLuaPVPWnd,"ruleDescBtn")
RegistClassMember(CLuaPVPWnd,"rankBtn")
RegistClassMember(CLuaPVPWnd,"fightRecordBtn")
RegistClassMember(CLuaPVPWnd,"receiveAwardBtn")
RegistClassMember(CLuaPVPWnd,"skillArrangeBtn")
RegistClassMember(CLuaPVPWnd,"helperButton")
RegistClassMember(CLuaPVPWnd,"stageRankNameLabels") --array
RegistClassMember(CLuaPVPWnd,"stageRankValueLabels") -- array
--Challenge Info
RegistClassMember(CLuaPVPWnd,"remainTimesLabel")
RegistClassMember(CLuaPVPWnd,"countDownLabel")
RegistClassMember(CLuaPVPWnd,"moneyCtrl")
RegistClassMember(CLuaPVPWnd,"resetBtn")
RegistClassMember(CLuaPVPWnd,"randomBtn")
--Player List
RegistClassMember(CLuaPVPWnd,"playerTemplate")
RegistClassMember(CLuaPVPWnd,"playerListTable")
RegistClassMember(CLuaPVPWnd,"firstRankGameObject")

RegistClassMember(CLuaPVPWnd,"totalTimesPerDay")
RegistClassMember(CLuaPVPWnd,"cooldown")
RegistClassMember(CLuaPVPWnd,"lastUpdateTime")
RegistClassMember(CLuaPVPWnd,"cancelRandomBtnDisableTick")
RegistClassMember(CLuaPVPWnd,"CountDownColor1")
RegistClassMember(CLuaPVPWnd,"CountDownColor2")
RegistClassMember(CLuaPVPWnd,"timeoutInterval")
RegistClassMember(CLuaPVPWnd,"updateInterval")
RegistClassMember(CLuaPVPWnd,"m_WaitForQueryResponse")
function CLuaPVPWnd:Awake()
    self.closeBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    UIEventListener.Get(self.closeBtn).onClick = DelegateFactory.VoidDelegate( function(go) self:OnCloseButtonClick(go) end)
	--Header
    self.selfRankLabel = self.transform:Find("Header/SelfRank/ValueLabel"):GetComponent(typeof(UILabel))
    self.selfFightScoreLabel = self.transform:Find("Header/SelfFightScore/ValueLabel"):GetComponent(typeof(UILabel))
    self.ruleDescBtn = self.transform:Find("Header/Buttons/RuleDescBtn").gameObject
    self.rankBtn = self.transform:Find("Header/Buttons/RankBtn").gameObject
    self.fightRecordBtn = self.transform:Find("Header/Buttons/FightRecordBtn").gameObject
    self.receiveAwardBtn = self.transform:Find("Header/Buttons/ReceiveAwardBtn").gameObject
    self.skillArrangeBtn = self.transform:Find("Header/Buttons/SkillBtn").gameObject
    self.helperButton = self.transform:Find("Header/Buttons/HelperButton").gameObject
    self.stageRankNameLabels = {}
    table.insert(self.stageRankNameLabels, 1, self.transform:Find("Header/StageRank/1"):GetComponent(typeof(UILabel)))
    table.insert(self.stageRankNameLabels, 2, self.transform:Find("Header/StageRank/2"):GetComponent(typeof(UILabel)))
    table.insert(self.stageRankNameLabels, 3, self.transform:Find("Header/StageRank/3"):GetComponent(typeof(UILabel)))
    self.stageRankValueLabels = {}
    table.insert(self.stageRankValueLabels, 1, self.transform:Find("Header/StageRank/1/ValueLabel"):GetComponent(typeof(UILabel)))
    table.insert(self.stageRankValueLabels, 2, self.transform:Find("Header/StageRank/2/ValueLabel"):GetComponent(typeof(UILabel)))
    table.insert(self.stageRankValueLabels, 3, self.transform:Find("Header/StageRank/3/ValueLabel"):GetComponent(typeof(UILabel)))
    self.remainTimesLabel = self.transform:Find("ChallengeInfo/RemainTimes/ValueLabel"):GetComponent(typeof(UILabel))
    self.countDownLabel = self.transform:Find("ChallengeInfo/CountDownLabel"):GetComponent(typeof(UILabel))
    self.moneyCtrl = self.transform:Find("ChallengeInfo/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.resetBtn = self.transform:Find("ChallengeInfo/ResetBtn"):GetComponent(typeof(CButton))
    self.randomBtn = self.transform:Find("ChallengeInfo/RandomBtn"):GetComponent(typeof(CButton))
    self.playerTemplate = self.transform:Find("PlayerList/PlayerItem").gameObject
    self.playerListTable = self.transform:Find("PlayerList/Table"):GetComponent(typeof(UITable))
    self.firstRankGameObject = self.transform:Find("PlayerList/FirstRankTexture").gameObject
    self.totalTimesPerDay = 0
    self.cooldown = 60*60
    self.lastUpdateTime = 0
    self.cancelRandomBtnDisableTick = nil
    self.CountDownColor1 = "FF0000"
    self.CountDownColor2 = "E0F8FF"
    self.timeoutInterval = 200
    self.updateInterval = 1

    if self.playerTemplate.activeSelf then
        self.playerTemplate:SetActive(false)
    end
    self.helperButton:SetActive(false)
end

function CLuaPVPWnd:Start()
    UIEventListener.Get(self.ruleDescBtn).onClick = DelegateFactory.VoidDelegate( function(go) self:OnRuleDescButtonClick(go) end)
    UIEventListener.Get(self.rankBtn).onClick = DelegateFactory.VoidDelegate( function(go) self:OnRankButtonClick(go) end)
    UIEventListener.Get(self.fightRecordBtn).onClick = DelegateFactory.VoidDelegate( function(go) self:OnFightRecordButtonClick(go) end)
    UIEventListener.Get(self.receiveAwardBtn).onClick = DelegateFactory.VoidDelegate( function(go) self:OnReceiveAwardButtonClick(go) end)
    UIEventListener.Get(self.resetBtn.gameObject).onClick = DelegateFactory.VoidDelegate( function(go) self:OnResetButtonClick(go) end)
    UIEventListener.Get(self.randomBtn.gameObject).onClick = DelegateFactory.VoidDelegate( function(go) self:OnRandomButtonClick(go) end)
    UIEventListener.Get(self.skillArrangeBtn).onClick = DelegateFactory.VoidDelegate( function(go) self:OnSkillArrangeButtonClick(go) end)
    UIEventListener.Get(self.helperButton).onClick = DelegateFactory.VoidDelegate( function(go) self:OnMPTZHelperButtonClick(go) end)

    if LuaPVPMgr.IsRobotOpen() then
        if CClientMainPlayer.Inst.SkillProp.IsMPTZRobotSkillInited == 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("门派挑战技能配置已被清空，请前往调整技能界面重新设置"))
        end
    end
end

function CLuaPVPWnd:OnEnable( )
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "MainPlayerPlayPropUpdate")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "MainPlayerCreated")

    g_ScriptEvent:AddListener("OnMPTZQueryPlayerTargetFinish", self, "OnMPTZQueryPlayerTargetFinish")
end

function CLuaPVPWnd:OnDisable( )
    self:CancelRandomBtnDisableTick()
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "MainPlayerPlayPropUpdate")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "MainPlayerCreated")

    g_ScriptEvent:RemoveListener("OnMPTZQueryPlayerTargetFinish", self, "OnMPTZQueryPlayerTargetFinish")
end

function CLuaPVPWnd:Update()
    if Time.realtimeSinceStartup - self.lastUpdateTime >= self.updateInterval then
        self.lastUpdateTime = Time.realtimeSinceStartup
        self:UpdateSpeedUpPriceAndRemainTimes()
    end
end

function CLuaPVPWnd:Init( )

    self.selfRankLabel.text = nil
    self.selfFightScoreLabel.text = nil
    local setting = MenPaiTiaoZhan_Setting.GetData()
    local timeArr = setting.ClientCronTime -- C# array
    do
        local i = 1
        while i <= #self.stageRankNameLabels do
            if timeArr.Length >= i then
                if i == 1 then
                    self.stageRankNameLabels[i].text = timeArr[i-1] .. LocalString.GetString(" 整点排名")
                else
                    self.stageRankNameLabels[i].text = timeArr[i-1] .. LocalString.GetString(" 平均排名")
                end
            else
                self.stageRankNameLabels[i].text = LocalString.GetString("00:00 排名")
            end
            i = i + 1
        end
    end
    do
        local i = 1
        while i <= #self.stageRankValueLabels do
            self.stageRankValueLabels[i].text = LocalString.GetString("暂无")
            i = i + 1
        end
    end

    self.resetBtn.Enabled = false
    self.firstRankGameObject:SetActive(false)
    self.totalTimesPerDay = setting.TimesPerDay
    self.cooldown = setting.CooldownTime * 60
    self:UpdateSpeedUpPriceAndRemainTimes()
    Gac2Gas.MPTZQueryPlayerTarget()
    self:CancelRandomBtnDisableTick()
end
function CLuaPVPWnd:OnChallenge( playerId, playerName) 

    local closeTime = MenPaiTiaoZhan_Setting.GetData().CloseTime
    if closeTime ~= nil and closeTime.Length == 4 then
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local beginTime = closeTime[0] * 60 + closeTime[1]
        local endTime = closeTime[2] * 60 + closeTime[3]
        local nowTime = now.Hour * 60 + now.Minute
        if nowTime >= beginTime and nowTime <= endTime then
            g_MessageMgr:ShowMessage("MPTZ_CLOSE_TIME")
            return
        end
    end

    local message = g_MessageMgr:FormatMessage("PVP_Confirm_Challenge", playerName)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        Gac2Gas.RequestChallengeMPTZ(playerId, false)
    end), nil, nil, nil, false)
end
function CLuaPVPWnd:OnMPTZQueryPlayerTargetFinish( )

    self.selfRankLabel.text = tostring(LuaMPTZMgr.SelfRankId)
    self.selfFightScoreLabel.text = tostring(LuaMPTZMgr.SelfFightScore)
    do
        local i = 1
        while i <= #self.stageRankValueLabels do
            if #LuaMPTZMgr.StageRanks >= i then
                self.stageRankValueLabels[i].text = LuaMPTZMgr.StageRanks[i] > 0 and tostring(LuaMPTZMgr.StageRanks[i]) or LocalString.GetString("暂无")
            else
                self.stageRankValueLabels[i].text = LocalString.GetString("暂无")
            end
            i = i + 1
        end
    end
    Extensions.RemoveAllChildren(self.playerListTable.transform)
    self.firstRankGameObject:SetActive(LuaMPTZMgr.SelfRankId == 1)
    do
        local i = 1
        while i <= #LuaMPTZMgr.TargetInfoTbl do
            local instance = NGUITools.AddChild(self.playerListTable.gameObject, self.playerTemplate)
            instance:SetActive(true)
            local info = LuaMPTZMgr.TargetInfoTbl[i]
            self:InitPVPCandidateItem(instance, info)
            i = i + 1
        end
    end
    self.playerListTable:Reposition()

    if LuaMPTZMgr.SelfRankId<=4 and self.m_WaitForQueryResponse then --服务器端根据MatchDistribution进行目标随机，写死了小于等于4时目标为固定的
        self.m_WaitForQueryResponse = false
        g_MessageMgr:ShowMessage("MPTZ_NO_TARGET")
    end
end

function CLuaPVPWnd:InitPVPCandidateItem(go, info)
    local portrait = go.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local levelLabel = go.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local rankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local fightScoreLabel = go.transform:Find("FightScoreLabel"):GetComponent(typeof(UILabel))
    local challengeBtn = go.transform:Find("ChallengeBtn").gameObject

    local playerId = math.floor(info.targetId)
    local playerName = info.name
    portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.profession, info.gender, info.expression), false)

    if info.xianFanStatus > 0 then
        levelLabel.text = SafeStringFormat3("[c][%s][%s]%d[-]/%d[-][/c]", NGUIText.EncodeColor24(levelLabel.color), L10.Game.Constants.ColorOfFeiSheng, info.grade, info.fanShenGrade)
    else
        levelLabel.text = SafeStringFormat3("[c][%s]%d[-][/c]", NGUIText.EncodeColor24(levelLabel.color), info.grade)
    end

    nameLabel.text = info.name
    rankLabel.text = System.String.Format(LocalString.GetString("排名 {0}"), info.rankIdx)
    fightScoreLabel.text = System.String.Format(LocalString.GetString("战斗力 {0}"), info.fightScore)

    UIEventListener.Get(challengeBtn).onClick = DelegateFactory.VoidDelegate( function(go) self:OnChallengeButtonClick(go, playerId, playerName) end)
    UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate( function(go) self:OnPortraitClick(go, playerId, playerName) end)
end

function CLuaPVPWnd:OnChallengeButtonClick(go, playerId, playerName)
    self:OnChallenge(playerId, playerName)
end

function CLuaPVPWnd:OnPortraitClick(go, playerId, playerName)
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.MPTZ, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
end

function CLuaPVPWnd:MainPlayerCreated(args)
    if LuaMPTZMgr.IsInMPTZ() then
        self:Close()
    end
end

function CLuaPVPWnd:MainPlayerPlayPropUpdate(args)
    self:UpdateSpeedUpPriceAndRemainTimes()
end

function CLuaPVPWnd:UpdateSpeedUpPriceAndRemainTimes( )
    --TODO 为0时需要显示null
    if CClientMainPlayer.Inst ~= nil then
        local price = CClientMainPlayer.Inst.PlayProp:GetMPTZSpeedUpPrice()
        self.moneyCtrl:SetCost(price > 0 and price or 0)
    else
        self.moneyCtrl:SetCost(0)
    end
    self:UpdateRemainTimes()
end
function CLuaPVPWnd:UpdateRemainTimes( )

    if CClientMainPlayer.Inst ~= nil then
        local n = CClientMainPlayer.Inst.PlayProp:GetMPTZTimes()
        self.remainTimesLabel.text = System.String.Format("{0}/{1}", n, self.totalTimesPerDay)
        local t = CServerTimeMgr.ConvertTimeStampToZone8Time(CClientMainPlayer.Inst.PlayProp:GetMPTZLastPlayTime())
        local ts = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), t)
        if ts.TotalSeconds <= 0 or n == 0 then
            self.countDownLabel.text = nil
        else
            local subtract = self.cooldown - ts.TotalSeconds
            local remain = 0
            if subtract > 0 then
                remain = math.floor(subtract)
                self.countDownLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("[{0}]{1}:{2:00}后[-][{3}]可再次挑战[-]"), {self.CountDownColor1, math.floor(remain / 60), remain % 60, self.CountDownColor2})
            else
                self.countDownLabel.text = nil
            end
        end

        --次数满的时候重置按钮置灰
        if n >= self.totalTimesPerDay or (CScene.MainScene ~= nil and CScene.MainScene.SceneTemplateId == Constants.MPTZMapTemplateId) then
            if self.resetBtn.Enabled then
                self.resetBtn.Enabled = false
            end
        else
            if not self.resetBtn.Enabled then
                self.resetBtn.Enabled = true
            end
        end
    else
        self.remainTimesLabel.text = nil
        self.countDownLabel.text = nil
    end

    local default
    if not System.String.IsNullOrEmpty(self.countDownLabel.text) then
        default = LocalString.GetString("快速开始")
    else
        default = LocalString.GetString("重置次数")
    end
    self.resetBtn.Text = (default)
    --有倒计时时显示“快速开始”，否则显示“重置次数”
end

function CLuaPVPWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaPVPWnd:OnCloseButtonClick(go)
    self:Close()
end

function CLuaPVPWnd:OnRuleDescButtonClick(go)
    CUIManager.ShowUI("PVPRuleWnd")
end

function CLuaPVPWnd:OnRankButtonClick(go)
    CUIManager.ShowUI("MPTZRankWnd")
end

function CLuaPVPWnd:OnFightRecordButtonClick(go)
     CUIManager.ShowUI("PVPRecordWnd")
end

function CLuaPVPWnd:OnReceiveAwardButtonClick( go) 
    Shop_PlayScore.ForeachKey(function (key) 
        local playScore = Shop_PlayScore.GetData(key)
        if playScore.Key == "MPTZ" then
            CLuaNPCShopInfoMgr.ShowScoreShopById(key)
            return
        end
    end)
end

function CLuaPVPWnd:OnResetButtonClick( go) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    local price = CClientMainPlayer.Inst.PlayProp:GetMPTZSpeedUpPrice()
    local message = g_MessageMgr:FormatMessage("PVP_Confirm_Reset", price)
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        Gac2Gas.RequestSpeedupMPTZCooldown()
    end), nil, nil, nil, false)
end

function CLuaPVPWnd:OnRandomButtonClick( go) 
    self.randomBtn.Enabled = false
    self:CancelRandomBtnDisableTick()
    self.cancelRandomBtnDisableTick = CTickMgr.Register(DelegateFactory.Action(function () 
        self.randomBtn.Enabled = true
    end), self.timeoutInterval, ETickType.Once)
    self.m_WaitForQueryResponse = true
    Gac2Gas.MPTZQueryPlayerTarget()
end

function CLuaPVPWnd:OnSkillArrangeButtonClick(go)
     CUIManager.ShowUI("PVPSkillWnd")
end

function CLuaPVPWnd:OnMPTZHelperButtonClick(go)
    if not CClientMainPlayer.Inst then return end
    local helperUrlPrefix = "https://www.16163.com/zt/qnm/mptzcs/geren.html?roleid=%s"
    CWebBrowserMgr.Inst:OpenUrl(SafeStringFormat3(helperUrlPrefix, CClientMainPlayer.Inst.Id))
end

function CLuaPVPWnd:CancelRandomBtnDisableTick( )
    if self.cancelRandomBtnDisableTick ~= nil then
        invoke(self.cancelRandomBtnDisableTick)
        self.cancelRandomBtnDisableTick = nil
    end
end

