-- Auto Generated!!
local CDaDiZiBuffIcon = import "L10.UI.CDaDiZiBuffIcon"
local CDaDiZiWatchStateItem = import "L10.UI.CDaDiZiWatchStateItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Vector3 = import "UnityEngine.Vector3"
CDaDiZiWatchStateItem.m_Awake_CS2LuaHook = function (this) 
    this.portrait:Clear()
    this.nameLabel.text = ""
    this.levelLabel.text = ""
    this.hpBar:UpdateValue(0, 0, 0)
    this.mpBar:UpdateValue(0, 0, 0)

    this.dpsLabel.text = ""
end
CDaDiZiWatchStateItem.m_Init_CS2LuaHook = function (this, info) 
    local PortraitName = CUICommonDef.GetPortraitName(info.cls, info.gender, -1)

    this.portrait:LoadNPCPortrait(PortraitName, false)

    local displayName = info.playerName
    if CUICommonDef.GetStrByteLength(displayName) > CDaDiZiWatchStateItem.DisplayLen then
        while CUICommonDef.GetStrByteLength(displayName) > CDaDiZiWatchStateItem.DisplayLen do
            displayName = CommonDefs.StringSubstring2(displayName, 0, CommonDefs.StringLength(displayName) - 1)
        end
        displayName = displayName .. "..."
    end
    this.nameLabel.text = displayName
    --

    this.levelLabel.text = tostring(info.level)

    this.hpBar:UpdateValue(info.hp, info.curHpFull, info.hpFull)
    this.mpBar:UpdateValue(info.mp, info.curMpFull, info.mpFull)

    this.dpsLabel.text = tostring(info.DPS)

    this:UpdateBuff(info.buffs, info.debuffs)
end
CDaDiZiWatchStateItem.m_UpdateBuff_CS2LuaHook = function (this, buffs, debuffs) 
    for i = this.buffTable.transform.childCount, 1, - 1 do
        this.pool:Recycle(this.buffTable.transform:GetChild(i - 1).gameObject)
    end
    for i = this.debuffTable.transform.childCount, 1, - 1 do
        this.pool:Recycle(this.debuffTable.transform:GetChild(i - 1).gameObject)
    end

    if buffs ~= nil then
        do
            local i = 0
            while i < buffs.Count do
                local buffItem = this.pool:GetFromPool(0)
                buffItem.transform.parent = this.buffTable.transform
                buffItem.transform.localScale = Vector3.one
                buffItem.gameObject:SetActive(true)
                local buffIcon = CommonDefs.GetComponent_GameObject_Type(buffItem, typeof(CDaDiZiBuffIcon))
                buffIcon:Init(buffs[i].buffId)
                i = i + 1
            end
        end
    end
    if debuffs ~= nil then
        local parent = this.debuffTable.transform
        if buffs == nil or (buffs ~= nil and buffs.Count == 0) then
            parent = this.buffTable.transform
        end
        do
            local i = 0
            while i < debuffs.Count do
                local buffItem = this.pool:GetFromPool(0)
                buffItem.transform.parent = parent
                buffItem.transform.localScale = Vector3.one
                buffItem.gameObject:SetActive(true)
                local buffIcon = CommonDefs.GetComponent_GameObject_Type(buffItem, typeof(CDaDiZiBuffIcon))
                buffIcon:Init(debuffs[i].buffId)
                i = i + 1
            end
        end
    end

    this.buffTable:Reposition()
    this.debuffTable:Reposition()
end
