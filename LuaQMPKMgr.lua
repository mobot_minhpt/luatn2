local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
local Main = import "L10.Engine.Main"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Utility = import "L10.Engine.Utility"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumQMPKPlayStage=import "L10.Game.EnumQMPKPlayStage"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CEquipment = import "L10.Game.CEquipment"
local CLingShou = import "L10.Game.CLingShou"
local CAntiProfessionSkillObject = import "L10.Game.CAntiProfessionSkillObject"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CMailMgr = import "L10.Game.CMailMgr"
local CScene = import "L10.Game.CScene"
local CSharpResourceLoader= import "L10.Game.CSharpResourceLoader"

CLuaQMPKMgr={}
CLuaQMPKMgr.m_OtherPersonalInfo=nil
CLuaQMPKMgr.m_ZhanduiFightData=nil

CLuaQMPKMgr.m_JingCaiListInfo={}
CLuaQMPKMgr.m_JingCaiRecord=nil
CLuaQMPKMgr.m_PlayIndex=0
CLuaQMPKMgr.m_JingCaiIndex=0
CLuaQMPKMgr.m_SelectIndex=0
CLuaQMPKMgr.m_Peilv=0--赔率
-- CLuaQMPKMgr.m_PlayRound=0
CLuaQMPKMgr.m_DaiBi=0
CLuaQMPKMgr.m_JingCaiRefreshDelta=10000--10秒钟


CLuaQMPKMgr.m_FightData={}

CLuaQMPKMgr.m_ServerList=nil
--实力之星
CLuaQMPKMgr.m_ShiLiZhiXingInfo={}
CLuaQMPKMgr.m_ShiLiZhiXingTotalPage=0

CLuaQMPKMgr.m_JingCaiPlayStage=0
CLuaQMPKMgr.m_StatueFx=0


CLuaQMPKMgr.m_StatueAniId=0
CLuaQMPKMgr.m_StatuePreviewTime=0

CLuaQMPKMgr.m_CurrentBattleStatus=nil
CLuaQMPKMgr.m_IsGameVideoBattleStatus=false--录像

--淘汰赛数据
CLuaQMPKMgr.m_TaoTaiSaiRecordList={}
CLuaQMPKMgr.m_MatchRecordList={}

--特殊数据分享
CLuaQMPKMgr.m_ExcellentData={}

CLuaQMPKMgr.m_PlayStage=0

--吉祥物解锁触发分享
CLuaQMPKMgr.m_ShareTemplateId=0
CLuaQMPKMgr.m_ShareEvolveGradeForSkin=0
CLuaQMPKMgr.m_ShareStage=0
CLuaQMPKMgr.m_Type=1

CLuaQMPKMgr.m_QMPKSeverWndUseWhiteList = false    -- 全民pk服务器界面显示是否使用白名单
CLuaQMPKMgr.m_WhiteWndList = nil                   -- 白名单列表

-- CLuaQMPKMgr.m_ShareUrl="http://file.mg.163.com/public/qnm/zbs2018/"
-- "http://192.168.131.157:8081/public/qnm/incubation2019/"
CLuaQMPKMgr.m_ShareUrl="https://ssl.hi.163.com/file_mg/public/qnm/incubation2020/"

--手册是否可领奖
CLuaQMPKMgr.m_QMPKPageClickedKey = "IsQMPKPageClicked"
CLuaQMPKMgr.m_IsHandBookCanRecive = false
CLuaQMPKMgr.m_HandBookRedDot = false
CLuaQMPKMgr.m_ProgressRewardCanRecive = false
--快速进入服务器信息
CLuaQMPKMgr.m_RoleInfos = nil
CLuaQMPKMgr.m_ServerInfos = nil

CLuaQMPKMgr.s_NewJiXiangWu = true  -- 剑心琉璃迭代

CLuaQMPKMgr.m_PreLoadedPrefab = nil  

CLuaQMPKMgr.ShowQmpkCertificationInfoAlert = false

function CLuaQMPKMgr.OnEnterPlay()
    CLuaQMPKMgr.m_FightData={}
    CLuaQMPKMgr.m_TaoTaiSaiRecordList={}
    CLuaQMPKMgr.m_MatchRecordList={}
    CSharpResourceLoader.Inst:LoadGameObject("UI/Prefab/QuanMinPK/QMPKBattleBeforeWnd.prefab", DelegateFactory.Action_GameObject(function(obj)
		CLuaQMPKMgr.m_PreLoadedPrefab = obj  
	end))
end
-- 观战时的阶段信息是在玩家进副本之前发的，这里不能将阶段信息清空
function CLuaQMPKMgr.OnLeavePlay()
    --CLuaQMPKMgr.m_PlayStage=0
    CLuaQMPKMgr.m_PreLoadedPrefab = nil
end

function CLuaQMPKMgr.OnPlayerLogin()
    CLuaQMPKMgr.ShowQmpkCertificationInfoAlert = false
end

function CLuaQMPKMgr.GetPlayName(playStage)
    -- eQieCuoSai  = 1,
    -- ePiPeiSai   = 2,
    -- eJiFenSai   = 3,
    -- eTaoTaiSai  = 4,
    -- eZongJueSai = 5,
    -- eReShenSai  = 6,
    -- eMeiRiYiZhanSai = 7,
    if playStage==EnumQmpkPlayStage.eQieCuoSai then
        return LocalString.GetString("切磋")
    elseif playStage==EnumQmpkPlayStage.ePiPeiSai then
        return LocalString.GetString("匹配")
    elseif playStage==EnumQmpkPlayStage.eJiFenSai then
        return LocalString.GetString("积分赛")
    elseif playStage==EnumQmpkPlayStage.eTaoTaiSai then
        return LocalString.GetString("淘汰赛")
    elseif playStage==EnumQmpkPlayStage.eZongJueSai then
        return LocalString.GetString("总决赛")
    elseif playStage==EnumQmpkPlayStage.eReShenSai then
        return LocalString.GetString("热身赛")
    elseif playStage==EnumQmpkPlayStage.eMeiRiYiZhanSai then
        return LocalString.GetString("每日一战")
    end
end

-- 501-506表示的是决赛的6场比赛，分别是1-2半决赛，3是季军赛，4-6是冠军赛
-- 淘汰赛如下:
-- local index = 100
-- local stage, rank, round = self.m_TaoTaiSaiStage, self.m_TaoTaiSaiRank, self.m_TaoTaiSaiRound
-- if stage == EnumQMPKTaoTaiSaiStage.e64To32 then
--     index = index + (rank - 1) * 3 + round
-- elseif stage == EnumQMPKTaoTaiSaiStage.e32To16 then
--     index = index + 32 * 3 + (rank - 1) * 3 + round
-- elseif stage == EnumQMPKTaoTaiSaiStage.e16To8 then
--     index = index + 48 * 3 + (rank - 1) * 3 + round
-- elseif stage == EnumQMPKTaoTaiSaiStage.e8To4 then
--     index = index + 56 * 3 + (rank - 1) * 3 + round
-- end
function CLuaQMPKMgr.GetMatchName(playIndex)
    -- print(playIndex)
    local id=math.floor(playIndex/100)
    local index=math.floor(playIndex%100)

    if id==5 then
        if index==1 or index==2 then
            local changci=SafeStringFormat3( LocalString.GetString("第%d场"),index )
            return LocalString.GetString("半决赛")..changci
        elseif index==3 then
            return LocalString.GetString("季军赛")
        else
            local changci=SafeStringFormat3( LocalString.GetString("第%d场"),index-3 )
            return LocalString.GetString("总决赛")..changci
        end
    else 
        local basicIndex = playIndex - 100
        local stageName, roundName, round

        if basicIndex > 56 * 3 then
            stageName = LocalString.GetString("8进4")
            round = (basicIndex - 56 * 3 - 1) % 3 + 1
        elseif basicIndex > 48 * 3 then
            stageName = LocalString.GetString("16进8")
            round = (basicIndex - 48 * 3 - 1) % 3 + 1
        elseif basicIndex > 32 * 3 then
            stageName = LocalString.GetString("32进16")
            round = (basicIndex - 32 * 3 - 1) % 3 + 1
        else
            stageName = LocalString.GetString("64进32")
            round = (basicIndex - 1) % 3 + 1
        end

        if round == 1 then
            roundName = LocalString.GetString("第一场")
        elseif round == 2 then
            roundName = LocalString.GetString("第二场")
        elseif round == 3 then
            roundName = LocalString.GetString("第三场")
        end

        return stageName .. roundName
    end
end

function CLuaQMPKMgr.DownloadQMPKPlayList()
    CommonDefs.ListClear(CQuanMinPKMgr.Inst.m_VideoRecordList)
    local url = Utility.TransformUrl(CLuaQMPKMgr.GetPlayListUrl(), "qmpk_video_url")
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, text) 
        if success then
            CQuanMinPKMgr.Inst:ParseQMPKPlayInfo(text)
            EventManager.BroadcastInternalForLua(EnumEventType.GameVideoListDownloaded, {true, "qmpk"})
        else
            EventManager.BroadcastInternalForLua(EnumEventType.GameVideoListDownloaded, {false, "qmpk"})
        end
    end)))
end

function CLuaQMPKMgr.ClearFastEnterServerInfo()
    CLuaQMPKMgr.m_RoleInfos = nil
    CLuaQMPKMgr.m_ServerInfos = nil
end

function CLuaQMPKMgr.GetPlayListUrl()
    return CGameVideoMgr.GameVideoListBaseUrl .. "/qmpk/2023_list.txt"
end

function CLuaQMPKMgr.ShowPlayerConfigWnd(playerId, playerName)
    if CScene.MainScene and CScene.MainScene.GamePlayDesignId == 51100444 then
        g_MessageMgr:ShowMessage("QueryQmpkPlayerInfoResult_FightingDisable")
        return
    end
    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id == playerId then
        return
    end
    CLuaQMPKMgr.s_PlayerId = playerId
    CLuaQMPKMgr.s_PlayerName = playerName
    CPlayerInfoMgr.PlayerId = playerId
    Gac2Gas.QueryQmpkPlayerInfo(playerId)
end

function CLuaQMPKMgr.GetQMPKPageClicked()
    return PlayerPrefs.GetInt(CLuaQMPKMgr.m_QMPKPageClickedKey, 0) > 0
end

function CLuaQMPKMgr.SetQMPKPageClicked()
    PlayerPrefs.SetInt(CLuaQMPKMgr.m_QMPKPageClickedKey, 1)
end

function CLuaQMPKMgr.InitWhiteWndList()
    local list = QuanMinPK_Setting.GetData().EnabledWindowAtQMPK
    CLuaQMPKMgr.m_WhiteWndList = {}
    for i = 0, list.Length - 1 do
        CLuaQMPKMgr.m_WhiteWndList[list[i]] = true
    end
end

function CLuaQMPKMgr.AddWndToWhiteList(wndName)
    if not CLuaQMPKMgr.m_WhiteWndList then CLuaQMPKMgr.InitWhiteWndList() end
    CLuaQMPKMgr.m_WhiteWndList[wndName] = true
end

function CLuaQMPKMgr.DeleteWndFromWhiteList(wndName)
    if not CLuaQMPKMgr.m_WhiteWndList then CLuaQMPKMgr.InitWhiteWndList() end
    CLuaQMPKMgr.m_WhiteWndList[wndName] = false
end
-- 每日一战是否开启
function CLuaQMPKMgr.IsMeiRiYiZhanOpen()
    -- 7月27号-7月31号，8月1号，8月3号，8月7号，8月12号-8月19号
    -- 下个版本看能不能读表做优化，不然每次策划改时间这里都要改，容易遗漏
    local time=CServerTimeMgr.Inst:GetZone8Time()
    local month=time.Month
    local day=time.Day
    if month==8 and ((day>=12 and day<=19) or (day == 1) or (day == 3) or (day == 7)) then
        return true
    end
    if month==7 and (day>=27 and day<=31) then
        return true
    end 
    return false
end
CQuanMinPKMgr.m_hookIsDisableWindowsAtQuanMinPKServer = function(this,wndName)
    if(not CLuaQMPKMgr.m_QMPKSeverWndUseWhiteList) then
        if (CQuanMinPKMgr.Inst.m_DisableWindowsAtQuanMinPKServer ~= nil and 
            CommonDefs.HashSetContains(CQuanMinPKMgr.Inst.m_DisableWindowsAtQuanMinPKServer, typeof(cs_string), wndName)) then
            return true
        end
        return false
    else
        if not CLuaQMPKMgr.m_WhiteWndList then CLuaQMPKMgr.InitWhiteWndList() end
        return not CLuaQMPKMgr.m_WhiteWndList[wndName]
    end
end

function Gas2Gac.OpenQmpkSchedule()
    CUIManager.ShowUI(CLuaUIResources.QMPKAgendaWnd)
end

function Gas2Gac.QmpkBiWuPlayEnd()
    CLuaQMPKMgr.m_IsGameVideoBattleStatus = false
    CUIManager.ShowUI(CLuaUIResources.QMPKCurrentBattleStatusWnd)
    -- L10.CLogMgr.Log("QmpkBiWuPlayEnd");
    g_ScriptEvent:BroadcastInLua("QmpkBiWuPlayEnd")

    --比赛结束，请求所有的战斗数据
    for i=1,5 do
        if not CLuaQMPKMgr.m_FightData[i] then
            Gac2Gas.QueryQmpkDetailFightData(i)
        end
    end
end

function Gas2Gac.ReplyQmpkAllMemberFightData(playStage, playIndex, fightData, isWin)
    local list=MsgPackImpl.unpack(fightData)
    if not list then return end

    CLuaQMPKMgr.m_ZhanduiFightData={}
    for i=0,list.Count-1,20 do
        local fight={
            m_PlayerId = list[i],
            m_PlayerName = list[i+1],
            m_PlayerClass = list[i+2],
            m_PlayerGender = list[i+3],

            m_Dps = list[i+4],
            m_BestDps = list[i+5],
            m_Heal = list[i+6],
            m_BestHeal = list[i+7],
            m_UnderDamage = list[i+8],
            m_BestUnderDamage = list[i+9],
            m_KillNum = list[i+10],
            m_BestKillNum = list[i+11],
            m_Ctrl = list[i+12],
            m_BestCtrl = list[i+13],
            m_RmCtrl = list[i+14],
            m_BestRmCtrl = list[i+15],
            m_Die = list[i+16],
            m_BestDie = list[i+17],
            m_Relive = list[i+18],
            m_BestRelive = list[i+19],
        }
        table.insert( CLuaQMPKMgr.m_ZhanduiFightData, fight )
    end

    CQuanMinPKMgr.Inst:SetCurrentFightName(playStage, playIndex)

	g_ScriptEvent:BroadcastInLua("QMPKReplyFightDataResult",playStage,playIndex)
end


function Gas2Gac.CreateQmpkZhanDuiSuccess()
	g_ScriptEvent:BroadcastInLua("CreateQmpkZhanDuiSuccess")
end
function Gas2Gac.JoinQmpkFreeMatch()
    CUIManager.ShowUI(CLuaUIResources.QMPKMatchingWnd)
    -- EventManager.Broadcast(EnumEventType.QMPKUpdateFreeMatchingState);
	g_ScriptEvent:BroadcastInLua("QMPKUpdateFreeMatchingState")
end

function Gas2Gac.QuitQmpkFreeMatch()
    CUIManager.CloseUI(CLuaUIResources.QMPKMatchingWnd)
    -- EventManager.Broadcast(EnumEventType.QMPKUpdateFreeMatchingState);
	g_ScriptEvent:BroadcastInLua("QMPKUpdateFreeMatchingState")
end

--每日一战
function Gas2Gac.ReplyQmpkMeiRiYiZhanInfo(lianshengCount, rankData, dynamicData, isMatching)
    -- body

    local rlist=MsgPackImpl.unpack(rankData)
    local rankInfo={}
    for i=1,rlist.Count do
        local item=rlist[i-1]
        local playerId=item[0]
        local class=item[1]
        local name=item[2]
        local count=item[3]
        table.insert(rankInfo, {playerId=playerId,class=class,name=name,count=count})
    end

    local dlist = MsgPackImpl.unpack(dynamicData)
    local dynamicInfo={}
    for i=1,dlist.Count do
        local now=dlist[i-1][0]
        local msg=dlist[i-1][1]
        table.insert(dynamicInfo, {now=now,msg=msg})
    end

	g_ScriptEvent:BroadcastInLua("ReplyQmpkMeiRiYiZhanInfo",lianshengCount,rankInfo,dynamicInfo,isMatching)
end

function Gas2Gac.ReplyQmpkPlayerDetailsQueryResult(targetId, fromCharacterData, currentCharacterData)
    local fromCharacterTbl = MsgPackImpl.unpack(fromCharacterData)
    local currentCharacterTbl = MsgPackImpl.unpack(currentCharacterData)

    CLuaQMPKMgr.m_OtherPersonalInfo={
        m_Id = targetId,

        m_FromCharacterId = fromCharacterTbl[0],
        m_FromServerName = fromCharacterTbl[1],
        m_FromCharacterName = fromCharacterTbl[2],
        m_FromGrade = fromCharacterTbl[3],
        m_FromEquipScore = fromCharacterTbl[4],
        m_FromGuildName = fromCharacterTbl[5],
        m_FromGender = fromCharacterTbl[6],
        m_FromClass = fromCharacterTbl[7],

        m_Name = currentCharacterTbl[0],
        m_Clazz = currentCharacterTbl[1],
        m_Gender = currentCharacterTbl[2],
        m_Grade = currentCharacterTbl[3],
        m_GoodAtClass = currentCharacterTbl[4],
        m_LocationId = currentCharacterTbl[5],
        m_OnlineTimeId = currentCharacterTbl[6],
        m_CanCommand = currentCharacterTbl[7],
        m_ZhanDuiId = currentCharacterTbl[8],
        m_Remark = currentCharacterTbl[9],
    }
    CUIManager.ShowUI(CLuaUIResources.QMPKPlayerInfoWnd)

end

function Gas2Gac.JoinQmpkMeiRiYiZhan()
    -- body
	g_ScriptEvent:BroadcastInLua("JoinQmpkMeiRiYiZhan")
end

function Gas2Gac.QuitQmpkMeiRiYiZhan()
    -- body
	g_ScriptEvent:BroadcastInLua("QuitQmpkMeiRiYiZhan")
end

-- function Gas2Gac.SyncJiXiangWuTrainingData(infoBytes,colorBytes,weaponColorBytes)
--     print("SyncJiXiangWuTrainingData")
-- 	g_ScriptEvent:BroadcastInLua("SyncJiXiangWuTrainingData")
-- end

function Gas2Gac.ReplyQmpkJingCaiList(playStage, listData)
    local t={}
    CLuaQMPKMgr.m_JingCaiListInfo[playStage]=t

    -- body
    local list=MsgPackImpl.unpack(listData)
    if not list then return end

    for i=1,list.Count do
        local item=list[i-1]

        local playIndex=item[0]
        local zhanduiId1=item[1]
        local zhanduiName1=item[2]
        local zhanduiId2=item[3]
        local zhanduiName2=item[4]

        table.insert( t,{playIndex=playIndex,zhanduiId1=zhanduiId1,zhanduiName1=zhanduiName1,zhanduiId2=zhanduiId2,zhanduiName2=zhanduiName2} )
    end

    table.sort( t,function(a,b)
        return a.playIndex<b.playIndex
    end )
    g_ScriptEvent:BroadcastInLua("ReplyQmpkJingCaiList",playStage,t)
end

function Gas2Gac.ReplyQmpkJingCaiRecord(recordData)
    local t={}
    CLuaQMPKMgr.m_JingCaiRecord=t
    -- body
    local list=MsgPackImpl.unpack(recordData)
    if not list then return end


     -- 1-比赛编号、2-竞猜编号、3-选项、4-投注数量、5-赔率、6-是否处理、7-获得积分
    for i=1,list.Count do
        local item=list[i-1]
        if item.Count==12 then
            table.insert( t,{
                playIndex=item[0],
                jingcaiIndex=item[1],
                selectIndex=item[2],
                touzhuNum=item[3],
                peilv=item[4],
                hasResult=item[5],
                score=item[6],
                lunkong=item[7],--轮空

                zhanduiId1=item[8],
                zhanduiId2=item[9],
                zhanduiName1=item[10],
                zhanduiName2=item[11],
                })
        end
    end
    g_ScriptEvent:BroadcastInLua("ReplyQmpkJingCaiRecord",CLuaQMPKMgr.m_JingCaiRecord)
end

function Gas2Gac.ReplyQmpkJingCaiData(playIndex, playRound, peilvData, zhanduiName1, zhanduiName2, daibi)
    CLuaQMPKMgr.m_DaiBi=daibi
    local list=MsgPackImpl.unpack(peilvData)
    if not list then return end

    local data={}
    for i=1,list.Count do
        local item=list[i-1]
        local peilv1=item[0]
        local peilv2=item[1]
        local result=-1
        if item.Count==3 then
            result=item[2]
        end
        table.insert( data,{peilv1,peilv2,result} )
    end
    g_ScriptEvent:BroadcastInLua("ReplyQmpkJingCaiData",playIndex,playRound,data,zhanduiName1, zhanduiName2, daibi)
end

function Gas2Gac.ReplyQmpkDetailFightData(round, attackData, defendData, playStage, attackName, defendName)

    -- playerId,class,name,{fightdata},{skilldata}
    --fightdata:{kill, die, ctrl, rmCtrl, relive, dps, heal}

    local data1={}
    local data2={}
    local list1=MsgPackImpl.unpack(attackData)
    local list2=MsgPackImpl.unpack(defendData)

    local parse=function(list,out)
        if list.Count < 6 then return end
        for i=2,list.Count,5 do
            local playerId=list[i-1]
            local clazz=list[i]
            local name=list[i+1]
            local fightdata=nil
            local rawFightData=list[i+2]
            --如果有数据
            fightdata={
                [1]=rawFightData[0],
                [2]=rawFightData[1],
                [3]=rawFightData[2],
                [4]=rawFightData[3],
                [5]=rawFightData[4],
                [6]=rawFightData[5],
                [7]=rawFightData[6]
            }
            local skilldata={}
            local rawSkillData=list[i+3]
            for i=1,rawSkillData.Count,2 do
                table.insert( skilldata, rawSkillData[i-1] )
                table.insert( skilldata, rawSkillData[i] )
            end
            table.insert( out,{playerId=playerId,clazz=clazz,name=name,fightdata=fightdata,skilldata=skilldata} )
        end
    end
    parse(list1,data1)
    parse(list2,data2)

    CLuaQMPKMgr.m_FightData[round]={[1]=data1,[2]=data2,name1=attackName,name2=defendName}


    g_ScriptEvent:BroadcastInLua("ReplyQmpkDetailFightData",round, attackData, defendData)
end


function Gas2Gac.OpenQmpkJingCaiWnd(playStage)
    --根据playstage显示不同
    CLuaQMPKMgr.m_JingCaiPlayStage=playStage
    CUIManager.ShowUI(CLuaUIResources.QMPKJingCaiWnd)
end

function Gas2Gac.OpenQmpkBiWuOverView(playStage)
    if playStage==4 then
        CQuanMinPKMgr.Inst.m_WatchListWndType=EnumQMPKPlayStage.eTaoTaiSai
        CUIManager.ShowUI(CLuaUIResources.QMPKTaoTaiSaiMatchRecordWnd)
    elseif playStage==5 then
        --4强界面
        CUIManager.ShowUI(CLuaUIResources.QMPKTopFourWnd)
    end
end

function Gas2Gac.OpenQmpkAllServerListWnd(serverList)
    local data=MsgPackImpl.unpack(serverList)
    if not data then return end
    CLuaQMPKMgr.m_ServerList={}

    for i=1,data.Count,2 do
        local server={}
        server.m_Id=data[i-1]
        server.m_OnlinePlayerNum=data[i]
        table.insert( CLuaQMPKMgr.m_ServerList,server )
    end
    table.sort( CLuaQMPKMgr.m_ServerList,function(a,b)
        if a.m_Id<b.m_Id then
            return true
        else
            return false
        end
    end )

    CUIManager.ShowUI(CLuaUIResources.QMPKServerChosenWnd)
end

function Gas2Gac.OpenQmpkRegisterWnd()
    CUIManager.ShowUI(CLuaUIResources.QMPKRegisterWnd)
end

function Gas2Gac.ReplyQmpkShiLiZhiXingInfo(page, totalPage, data)

    local list=MsgPackImpl.unpack(data)
    local playerInfo={}
    for i=1,list.Count,9 do
        local fromCharacterId=list[i-1]
        local serverName=list[i]
        local fromProfession=list[i+1]
        local fromGender=list[i+2]
        local fromCharacterName=list[i+3]
        local pid=list[i+4]
        local class=list[i+5]
        local gender=list[i+6]
        local name=list[i+7]
        table.insert( playerInfo,{
            fromCharacterId=fromCharacterId,
            serverName=serverName,
            fromProfession=fromProfession,
            fromGender=fromGender,
            fromCharacterName=fromCharacterName,
            pid=pid,
            class=class,
            gender=gender,
            name=name,
        } )
    end
    CLuaQMPKMgr.m_ShiLiZhiXingInfo[page]=playerInfo
    CLuaQMPKMgr.m_ShiLiZhiXingTotalPage=totalPage
    g_ScriptEvent:BroadcastInLua("ReplyQmpkShiLiZhiXingInfo",page, totalPage, playerInfo)

end

function Gas2Gac.QmpkExcellentDataShare(playStage, tp, value)
	-- LianZhan = 1,
	-- Kill = 2,
	-- Heal = 3,
	-- Relive = 4,
	-- Ctrl = 5,
	-- KillLingShou = 6,
    -- DPS = 7,
    -- local t={5,12,10000000,5,30,10,10000000}
    -- if value >= t[tp] then
        --达到条件 触发
        CLuaQMPKMgr.m_ExcellentData={tp,value,CLuaQMPKMgr.GetPlayName(playStage)}
        CUIManager.ShowUI(CLuaUIResources.QMPKShareWnd)
    -- end
end
function Gas2Gac.ReplyQmpkBiWuZhanKuang(attackName, defendName, attackWinCount, defendWinCount, finishRound, playRound, dataUD, selfForce, playStage, playIndex)
    local battle={
        m_AttackName = attackName,
        m_DefendName = defendName,
        m_AttackWinCount = attackWinCount,
        m_DefendWinCount = defendWinCount,
        m_FinishedRound = finishRound,
        m_PlayRound = playRound,
        m_SelfForce = selfForce,
        m_PlayStage = playStage,
        m_PlayIndex = playIndex,
        m_Status = {},
    }
    local round = 0
    -- local statusData=MsgPackImpl.unpack(data[6])
    local statusData = g_MessagePack.unpack(dataUD)
    for i = 1,#statusData,3 do
        round = round + 1
        local attackTotalKill = 0
        local attackTotalDps = 0
        local defendTotalKill = 0
        local defendTotalDps = 0
        local attackMember = statusData[i] or {}
        local defendMember = statusData[i + 1] or {}
        local winForce = statusData[i + 2] or EnumCommonForce_lua.eNeutral
        local attackData = {}
        local defendData = {}
        for j = 1,#attackMember,5 do
            table.insert(attackData,{
                id = attackMember[j] or 0,
                Name = attackMember[j + 1] or "",
                Class = attackMember[j + 2] or 0 ,
                Dps = attackMember[j + 3] or 0,
                Kill = attackMember[j + 4] or 0,
            })
            attackTotalKill = attackTotalKill + (attackMember[j + 4] or 0)
            attackTotalDps = attackTotalDps + (attackMember[j + 3] or 0)
        end
        for j = 1,#defendMember,5 do
            table.insert(defendData,{
                id = defendMember[j]  or 0,
                Name = defendMember[j + 1] or "",
                Class = defendMember[j + 2] or 0,
                Dps = defendMember[j + 3] or 0,
                Kill = defendMember[j + 4] or 0,
            })
            defendTotalKill = defendTotalKill + (defendMember[j + 4] or 0)
            defendTotalDps = defendTotalDps + (defendMember[j + 3] or 0)
        end
        table.insert(battle.m_Status,{
            attackTotalKill = attackTotalKill,
            defendTotalKill = defendTotalKill,
            attackTotalDps = attackTotalDps,
            defendTotalDps = defendTotalDps,
            attackData = attackData,
            defendData = defendData,
            win = winForce,
            m_IsInBattle = battle.m_PlayRound==round,
        })
    end
    -- for i=0,statusData.Count-1,7 do
    --     local status={
    --         m_Dps1 = statusData[i],
    --         m_Name1 = {},
    --         m_Id1 = {},
    --         m_Class1 = {},

    --         m_Dps2 = statusData[i+2],
    --         m_Name2 = {},
    --         m_Id2 = {},
    --         m_Class2 = {},

    --         m_Win = statusData[i+4],
    --         m_IsInBattle = battle.m_PlayRound==i/5+1,
    --         m_Kill1 = statusData[i+5],
    --         m_Kill2 = statusData[i+6]
    --     }
    --     local info=statusData[i+1]
    --     for j=0,info.Count-1,3 do
    --         table.insert( status.m_Id1,info[j] )
    --         table.insert( status.m_Name1,info[j+1] )
    --         table.insert( status.m_Class1,info[j+2] )
    --     end
    --     local info=statusData[i+3]
    --     for j=0,info.Count-1,3 do
    --         table.insert( status.m_Id2,info[j] )
    --         table.insert( status.m_Name2,info[j+1] )
    --         table.insert( status.m_Class2,info[j+2] )
    --     end

    --     table.insert( battle.m_Status,status )
    -- end
    CLuaQMPKMgr.m_CurrentBattleStatus=battle
    CLuaQMPKMgr.m_IsGameVideoBattleStatus = false
    g_ScriptEvent:BroadcastInLua("QMPKQueryBattleStatusResult")
end

function Gas2Gac.OpenQmpkHistoryVideoWnd()
    CUIManager.ShowUI(CLuaUIResources.QMPKGameVideoWnd)
end

function Gas2Gac.ReplyQmpkTaoTaiSaiWatchList(stage, dataUD)
    local list
    if MsgPackImpl.IsValidUserData(dataUD) then
        list = MsgPackImpl.unpack(dataUD)
    end
    CLuaQMPKMgr.m_TaoTaiSaiRecordList={}
    if list then
        for i=0,list.Count-1 do
            local data = list[i]

            if tonumber(data[0]) then
                local record={
                    m_Rank = tonumber(data[0]),
                    m_Zhandui1 = tonumber(data[1]) ,
                    m_TeamName1 = tostring(data[2]),
                    m_Score1 = tonumber(data[3]),
                    m_Zhandui2 = tonumber(data[4]),
                    m_TeamName2 = tostring(data[5]),
                    m_Score2 = tonumber(data[6]),
                    m_Win = tonumber(data[7]),
                    m_IsMatching = tostring(data[8]) and tostring(data[8]) ~= "0" or false,
                }
                table.insert(CLuaQMPKMgr.m_TaoTaiSaiRecordList,record)
            end
        end
    end

    g_ScriptEvent:BroadcastInLua("ReplyQmpkTaoTaiSaiWatchList", stage, true)
end

function Gas2Gac.ReplyQmpkZongJueSaiWatchList(playIndex, zhanduiId1, zhanduiTitle1, zhanduiId2, zhanduiTitle2, winZhanDuiId, start)
    CLuaQMPKMgr.m_TaoTaiSaiRecordList={}
    if zhanduiId1>0 then
        local record={
            m_Zhandui1 = zhanduiId1,
            m_TeamName1 = zhanduiTitle1,
            m_Zhandui2 = zhanduiId2,
            m_TeamName2 = zhanduiTitle2,
            m_Win = winZhanDuiId,
            m_IsMatching = start>0
        }
        table.insert( CLuaQMPKMgr.m_TaoTaiSaiRecordList,record )
    end
    g_ScriptEvent:BroadcastInLua("ReplyQmpkTaoTaiSaiWatchList", playIndex, false)
end

function Gas2Gac.ReplyQmpkJiFenSaiWatchList(dataUD)
    local list=MsgPackImpl.unpack(dataUD)
    if not list then return end
    CLuaQMPKMgr.m_MatchRecordList={}

    for i=1,list.Count,6 do
        local record={
            m_Zhandui1=tonumber(list[i-1]),
            m_TeamName1=tostring(list[i]),
            m_Zhandui2=tonumber(list[i+1]),
            m_TeamName2=tostring(list[i+2]),
            m_Win=tonumber(list[i+3]),
            m_IsMatching=tonumber(list[i+4])>0
        }
        table.insert( CLuaQMPKMgr.m_MatchRecordList,record )
    end
    -- g_ScriptEvent:BroadcastInLua("ReplyQmpkJiFenSaiWatchList")
    CUIManager.ShowUI(CLuaUIResources.QMPKMatchRecordWnd)
end


function Gas2Gac.ReplyQmpkQieCuoTeamList(selfQieCuoId, page, totalPage, data)
    local list=MsgPackImpl.unpack(data)
    if not list then return end

    local teamList={}
    for i=1,list.Count,4 do
        local rawMembers=list[i+2]
        local members={}
        for j=1,rawMembers.Count do
            table.insert( members,tonumber(rawMembers[j-1]) )
        end
        local team={
            m_Id = tonumber(list[i-1]),
            m_LeaderId = tonumber(list[i]),
            m_LeaderName = tostring(list[i+1]),
            m_Member = members
        }
        table.insert( teamList,team )
    end
    g_ScriptEvent:BroadcastInLua("ReplyQmpkQieCuoTeamList", selfQieCuoId, page, totalPage,teamList)
end

function Gas2Gac.ReplyQmpkQieCuoTeam(selfQieCuoId,memberId,dataUD)
    Gas2Gac.ReplyQmpkQieCuoTeamList(selfQieCuoId,1,1,dataUD)
end

function Gas2Gac.SignUpQmpkQieCuoSuccess(qiecuoId)
    g_ScriptEvent:BroadcastInLua("SignUpQmpkQieCuoSuccess")
end

function Gas2Gac.QmpkRegisterPersonalInfoSuccess()
    g_ScriptEvent:BroadcastInLua("QmpkRegisterPersonalInfoSuccess")
end

function Gas2Gac.ReplyQmpkZhanDuiApplyList(totalPage,dataUD)
    local list=MsgPackImpl.unpack(dataUD)
    if not list then return end

    local requestPlayers={}
    for i=1,list.Count,7 do
        local player={
            m_Id = tonumber(list[i-1]),
            m_Name = tostring(list[i]),
            m_Clazz = tonumber(list[i+1]),
            m_GoodAtClass = tonumber(list[i+2]),
            m_OnlineTimeId = tonumber(list[i+3]),
            m_CanCommand = tonumber(list[i+4]),
            m_Remark=tostring(list[i+5]),
        }
        table.insert( requestPlayers,player )
    end
    g_ScriptEvent:BroadcastInLua("ReplyQmpkZhanDuiApplyList",requestPlayers)
end

function Gas2Gac.ReplyQmpkPlayerListQueryResult(dataUD)
    local list=MsgPackImpl.unpack(dataUD)
    if not list then return end

    local players={}
    for i=1,list.Count,8 do
        local player={
            m_Id = tonumber(list[i-1]),
            m_Name = tostring(list[i]),
            m_Grade = tonumber(list[i+1]),
            m_Clazz = tonumber(list[i+2]),
            m_Gender = tonumber(list[i+3]),
            m_GoodAtClass = tonumber(list[i+4]),
            -- m_OnlineTimeId = tonumber(list[i+3]),
            m_CanCommand = tonumber(list[i+5]),
            m_Remark=tostring(list[i+6]),
        }
        table.insert( players,player )
    end
    g_ScriptEvent:BroadcastInLua("ReplyQmpkPlayerListQueryResult",players)
end

function Gas2Gac.PlayerRequestJoinQmpkZhanDui()
    g_ScriptEvent:BroadcastInLua("PlayerRequestJoinQmpkZhanDui")
end

-- function Gas2Gac.RefuseApplyPlayerJoinQmpkZhanDuiResult(result)
--     -- //0-对方不在申请列表
--     -- //1-成功拒绝
-- end

function Gas2Gac.UpdateQmpkPersonalInfoSuccess()
    g_MessageMgr:ShowMessage("QMPK_SELFINFO_SET_SUCCESS");
    Gac2Gas.RequestSelfQmpkPersonalInfo()
end

function Gas2Gac.QmpkSetChuZhanInfoSuccess()
    Gac2Gas.RequestSelfQmpkZhanDuiInfo()
end

-- function Gas2Gac.AcceptApplyPlayerJoinQmpkZhanDuiFail(reason)

-- end

function Gas2Gac.SyncQmpkPlayData(playStage)
    -- print("SyncQmpkPlayData ", playStage)
    CLuaQMPKMgr.m_PlayStage=playStage
end

function Gas2Gac.ReplyQmpkHandBookInfo(score, recordDataUD, awardDataUD, isUnLock)
    if(not CLuaQMPKHandBookWnd.m_Opened) then
        CUIManager.ShowUI(CLuaUIResources.QMPKHandBookWnd)
    end
    local award_dict = {}
    do
        local list = MsgPackImpl.unpack(awardDataUD)
        for i = 1, list.Count, 1 do
            award_dict[list[i - 1]] = true
        end
    end
    local record_list = {}
    do
        local list = MsgPackImpl.unpack(recordDataUD)
        for i=1,list.Count,2 do
            table.insert(record_list, {date = list[i - 1], id = list[i]})
        end
    end
    CLuaQMPKMgr.CheckHandBookAward(score, award_dict, isUnLock)
    if(isUnLock == 1 and CUIManager.IsLoaded(CLuaUIResources.QMPHKUnlockZhanLingWnd))  then
        CUIManager.CloseUI(CLuaUIResources.QMPHKUnlockZhanLingWnd)
    end 
    g_ScriptEvent:BroadcastInLua("ReplyQmpkHandBookInfo", score, record_list, award_dict, isUnLock)
end

function CLuaQMPKMgr.CheckHandBookAward(score, award_dict, isUnLock)
    local rewardData = {}
    QuanMinPK_HandBook.ForeachKey(function (key)
        local data = QuanMinPK_HandBook.GetData(key)
        if score >= data.Score then
            table.insert(rewardData, {itemKey = key, passItemKey = key + CLuaQMPKHandBookWnd.m_Reward2IdOffset})
        end
    end)

    for i = 1, #rewardData do
        local data = rewardData[i] 
        if not award_dict[data.itemKey] or (isUnLock == 1 and not award_dict[data.passItemKey]) then
            CLuaQMPKMgr.m_IsHandBookCanRecive = true
            CLuaQMPKMgr:ProcessHandBookRedDot()
            return 
        end
    end
    CLuaQMPKMgr.m_IsHandBookCanRecive = false
    CLuaQMPKMgr:ProcessHandBookRedDot()
end


function CLuaQMPKMgr.IsPlayerSignUp()
    if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eQmpkSignUpData) then
		local tempPlayData = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eQmpkSignUpData)
        local charAscii = tempPlayData.Data.Data and g_MessagePack.unpack(tempPlayData.Data.Data)
        return tonumber(string.char(charAscii)) == 1
	end
    return false
end

function CLuaQMPKMgr.GetQmpkTexturePath(name)
    return SafeStringFormat3("Assets/Res/UI/Texture/Transparent/Material/%s.mat",name)
end

function Gas2Gac.ReplyQmpkPlayerPrayWordGroup(engineId, diGeId, tianGeId, renGeId, fishGroupTblData, fishWordTblData)
    local fishGroupTbl = {}
    local list = MsgPackImpl.unpack(fishGroupTblData)
    for i = 0, list.Count - 1 do
        table.insert(fishGroupTbl, list[i])
    end

    local fishWordTbl = {}
    local list = MsgPackImpl.unpack(fishWordTblData)
    for i = 0, list.Count - 1 do
        table.insert(fishWordTbl, list[i])
    end

    g_ScriptEvent:BroadcastInLua("QMPKPrayBuffWordResult", engineId, diGeId, tianGeId, renGeId, fishGroupTbl, fishWordTbl)
end
function Gas2Gac.ReplyQmpkLingShouLingFuIndex(engineId, generalLingFu, specialLingFu, suitLingFu)
    g_ScriptEvent:BroadcastInLua("ReplyQmpkLingShouLingFuIndex", engineId, generalLingFu, specialLingFu, suitLingFu)
end

function Gas2Gac.SyncQmpkDpsInfo(attackDps, defendDps, attackNme, defendName)
    CLuaQMPKDamageWnd.Name1 = attackNme
    CLuaQMPKDamageWnd.Name2 = defendName
    g_ScriptEvent:BroadcastInLua("SyncQmpkDpsInfo", attackDps, defendDps)
end

function Gas2Gac.SetQmpkAntiProfessionDataSuccess(str)
    --print("SetQmpkAntiProfessionDataSuccess: ", str)

    local curLevels = g_LuaUtil:StrSplit(str, ',')
    for i = 1, #str do
        local ret, data = CommonDefs.DictTryGet_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i)
        if ret then
            data.CurLevel = curLevels[i]
        else
            local newSkill = CreateFromClass(CAntiProfessionSkillObject)
            newSkill.CurLevel = curLevels[i]
            newSkill.MaxLevel = QuanMinPK_Setting.GetData().MaxAntiProfessionLevel
            newSkill.Profession = i
            newSkill.UpgradeFailTimes = 0
            CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i, newSkill)
        end
    end
    g_ScriptEvent:BroadcastInLua("SetQmpkAntiProfessionDataSuccess")
end

function Gas2Gac.SendQmpkCreateRoleInfo(dataUD, allServerDataUD)
    local roleTbl = dataUD and g_MessagePack.unpack(dataUD)
    local tab = {}
    if roleTbl then
        for k, v in pairs(roleTbl) do
            table.insert(tab, v)
        end
    end

    local serverTbl = allServerDataUD and g_MessagePack.unpack(allServerDataUD)
    local tab2 = {}
    if serverTbl then
        for i = 1, #serverTbl, 2 do
            table.insert(tab2, {id = serverTbl[i], onlineNum = serverTbl[i + 1]})
        end
    end

    table.sort(tab,function(a,b)
        if a.JoinZhanDui == b.JoinZhanDui then
            return a.PlayerId < b.PlayerId
        else
            return a.JoinZhanDui > b.JoinZhanDui
        end
    end)

    table.sort(tab2,function(a,b)
        if a.id < b.id then
            return true
        else
            return false
        end
    end)

    CLuaQMPKMgr.m_RoleInfos = tab
    CLuaQMPKMgr.m_ServerInfos = tab2
    if #tab > 0 then
        CUIManager.ShowUI(CLuaUIResources.QMPKEnterServerWnd)
    else
        CUIManager.ShowUI(CLuaUIResources.QMPKSelectServerWnd)
    end
end

function Gas2Gac.ReplyQmpkZongJueSaiOverView2(isFinish, dataUD)
    --print("ReplyQmpkZongJueSaiOverView2", isFinish)
	local list = MsgPackImpl.unpack(dataUD)
	if not list then return end

    local tab = {}
	for i = 0, list.Count - 1, 2 do
        local zhanduiId = list[i]
        local zhanduiTitle = list[i+1]
        table.insert(tab, {zhanduiId = zhanduiId, zhanduiTitle = zhanduiTitle})
	end
    g_ScriptEvent:BroadcastInLua("ReplyQmpkZongJueSaiOverView2", isFinish, tab)
end

function Gas2Gac.SubmitQmpkCertificationInfoSuccess(name, id)
    --print("==SubmitQmpkCertificationInfoSuccess==", name, id)

    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("已完成身份认证"))
    g_ScriptEvent:BroadcastInLua("SubmitQmpkCertificationInfoSuccess", name, id)
end

function Gas2Gac.ShowQmpkCertificationInfoAlert()
    CLuaQMPKMgr.ShowQmpkCertificationInfoAlert = true
    g_ScriptEvent:BroadcastInLua("OnChangeQmpkCertificationStatus", true)
end

CLuaQMPKMgr.s_IsOtherPlayer = false
CLuaQMPKMgr.s_PlayerId = nil
CLuaQMPKMgr.s_PlayerName = nil
CLuaQMPKMgr.s_PlayerCls = nil
CLuaQMPKMgr.s_PlayerInfo = nil

function Gas2Gac.SendQueryQmpkPlayerInfoResult_NotExist(targetPlayerId)
    --print("SendQueryQmpkPlayerInfoResult_NotExist", targetPlayerId)
    if CLuaQMPKMgr.s_PlayerId == targetPlayerId then
        g_MessageMgr:ShowMessage("QmpkPlayerInfoResult_NotExist")
    end
end

function Gas2Gac.SendQueryQmpkPlayerInfoResult_Offline(targetPlayerId)
    --print("SendQueryQmpkPlayerInfoResult_Offline", targetPlayerId)
    if CLuaQMPKMgr.s_PlayerId == targetPlayerId then
        g_MessageMgr:ShowMessage("QueryQmpkPlayerInfoResult_Offline")
    end
end

function Gas2Gac.SendQueryQmpkPlayerInfoResult_Online_Begin(targetPlayerId, class, qifuWordInfo, fishWordInfo, effectFishWordInfo)
    --print("SendQueryQmpkPlayerInfoResult_Online_Begin", targetPlayerId, class)
    if CLuaQMPKMgr.s_PlayerId == targetPlayerId then
        CLuaQMPKMgr.s_PlayerInfo = {}
        CLuaQMPKMgr.s_PlayerCls = class

        if MsgPackImpl.IsValidUserData(qifuWordInfo) then
            local QifuWordInfo = g_MessagePack.unpack(qifuWordInfo)
            -- for k, v in pairs(QifuWordInfo) do
            --     print(k,v )
            -- end
            -- [1]diGeId, [2]tianGeId, [3]renGeId
            CLuaQMPKMgr.s_PlayerInfo.QifuWordInfo = QifuWordInfo
        end

        if MsgPackImpl.IsValidUserData(fishWordInfo) then
            local FishWordInfo = g_MessagePack.unpack(fishWordInfo)
            -- for k, v in pairs(FishWordInfo) do
            --     print(k,v )
            -- end
            -- fishTbl
            CLuaQMPKMgr.s_PlayerInfo.FishWordInfo = FishWordInfo
        end

        if MsgPackImpl.IsValidUserData(effectFishWordInfo) then
            local EffectFishWordInfo = g_MessagePack.unpack(effectFishWordInfo)
            -- for k, v in pairs(EffectFishWordInfo) do
            --     print(k,v )
            -- end
            -- ?
            CLuaQMPKMgr.s_PlayerInfo.EffectFishWordInfo = EffectFishWordInfo
        end

        CLuaQMPKMgr.s_PlayerInfo.Pos2Equip = {}
        CLuaQMPKMgr.s_PlayerInfo.ItemId2Equip = {}
        CLuaQMPKMgr.s_PlayerInfo.ItemId2Pos = {}
        CLuaQMPKMgr.s_PlayerInfo.LingShou = {}
    end
end

function Gas2Gac.SendQueryQmpkPlayerInfoResult_Online_Equipment(targetPlayerId, pos, itemId, itemInfo)
    --print("SendQueryQmpkPlayerInfoResult_Online_Equipment", targetPlayerId, pos, itemId, itemInfo)
    if CLuaQMPKMgr.s_PlayerId == targetPlayerId then
        local equip = CreateFromClass(CEquipment)
        equip:LoadFromString(itemInfo)
        local playerId, pos = targetPlayerId,pos

        CLuaQMPKMgr.s_PlayerInfo.Pos2Equip[pos] = equip
        CLuaQMPKMgr.s_PlayerInfo.ItemId2Equip[equip.Id] = equip
        CLuaQMPKMgr.s_PlayerInfo.ItemId2Pos[equip.Id] = pos
    end
end

function Gas2Gac.SendQueryQmpkPlayerInfoResult_Online_LingShou(targetPlayerId, lingshouId, lingshouInfo)
    --print("SendQueryQmpkPlayerInfoResult_Online_LingShou", targetPlayerId)
    if CLuaQMPKMgr.s_PlayerId == targetPlayerId then
        local lingshou = CreateFromClass(CLingShou)
        lingshou:LoadFromString(lingshouInfo)
        
        table.insert(CLuaQMPKMgr.s_PlayerInfo.LingShou, lingshou)
    end
end

function Gas2Gac.SendQueryQmpkPlayerInfoResult_Online_End(targetPlayerId, wuxing, antiprofessionInfo, otherInfo)
    --print("SendQueryQmpkPlayerInfoResult_Online_End", targetPlayerId)

    if CLuaQMPKMgr.s_PlayerId == targetPlayerId then
        CLuaQMPKMgr.s_PlayerInfo.MingGe = wuxing

        if MsgPackImpl.IsValidUserData(antiprofessionInfo) then
            local AntiprofessionInfo = g_MessagePack.unpack(antiprofessionInfo)
            -- for k, v in pairs(AntiprofessionInfo) do
            --     print(k,v )
            -- end
            CLuaQMPKMgr.s_PlayerInfo.AntiprofessionInfo = AntiprofessionInfo
        end

        if MsgPackImpl.IsValidUserData(otherInfo) then
            local OtherInfo = g_MessagePack.unpack(otherInfo)
            CLuaQMPKMgr.s_PlayerInfo.OtherInfo = OtherInfo
        end
    end
    CLuaQMPKMgr.s_IsOtherPlayer = true
    CUIManager.ShowUI(CLuaUIResources.QMPKConfigMainWnd)
end

function Gas2Gac.SendQmpkHandbookReceiveAwardInfo(canReceive)
    -- CLuaQMPKMgr.m_IsHandBookCanRecive = canReceive
    -- CLuaQMPKMgr.ProcessHandBookRedDot()
end

function CLuaQMPKMgr.ProcessHandBookRedDot()
    CLuaQMPKMgr.m_HandBookRedDot = CLuaQMPKMgr.m_IsHandBookCanRecive or CLuaQMPKMgr.FindNextMail() ~= nil or CLuaQMPKMgr.m_ProgressRewardCanRecive
    g_ScriptEvent:BroadcastInLua("ProcessHandBookRedDot")
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)
end

function CLuaQMPKMgr.FindNextMail()
    local setting = QuanMinPK_Setting.GetData()
    local mail = nil
    do
        local mails = CMailMgr.Inst:GetMails()
        local i = 0
        while (i < mails.Count) do

            if(tonumber(mails[i].TemplateId) == 11110729)then
                mail = mails[i].MailId
                break
            end
            i = i + 1
        end
    end
    return mail
end

EnumQMPKBPPlayerPosition = {
    eTeamLeader = 1,     -- 队长
    eTeamMember = 2,     -- 队员
    eOther = 3,          -- 观战
}
EnumQMPKWndBPStatus = {     -- bp操作界面状态
    eNone = 0,
    eBPStatus = 1,      -- BP阶段
    eShowStatus = 2,    -- BP完成后5s展示阶段
}
EnumQmpkBanPickStatus = {
    eNone = 0,
    eBanBegin = 1,  -- ban操作开始
    eBanFinish = 2, -- ban操作结束
    ePickBegin = 3, -- pick操作开始
    ePickFinish = 4, -- pick操作结束
    eAllFinish = 5  -- 两方当前局操作全部结束
}

EnumQmpkSelectBanPickStatus = {
    eRound1Ban = 0, -- 第一局未出战角色不可选
    eRound2Ban = 1, -- 第二局出战角色不可选
    eRound3Ban = 2, -- 第三局未出场角色不可禁用
    eRound4Ban = 3, -- 第四局规则限制不可选
    eBeBan = 4, -- 被对手禁用不可选
    eCanSelect = 5, -- 可以选
    eRound5Pick = 6, -- 第五局规则限制强制选择
    eRound4Pick = 7, -- 第四局规则限制强制选择
}
CLuaQMPKMgr.m_QmpkBanPickInfo=nil

function Gas2Gac.SyncQmpkBanPickInfo(force, round, basicData, roundWinForceData, attackChuZhanMemberData, defendChuZhanMemberData, banMemberData)
    -- round为4时才需要解析 banMemberData
    -- roundWinForceData 是一个List,按顺序记录每局获胜的force
    local zhanDuiData = g_MessagePack.unpack(basicData)
    local attackMember =  g_MessagePack.unpack(attackChuZhanMemberData)
    local defendMember =  g_MessagePack.unpack(defendChuZhanMemberData)
    local banMemberData =  g_MessagePack.unpack(banMemberData)
    if not zhanDuiData or not attackMember or not defendMember or not banMemberData then return end
    
    CLuaQMPKMgr.m_QmpkBanPickInfo = {}
    CLuaQMPKMgr.m_QmpkBanPickInfo.force = force
    CLuaQMPKMgr.m_QmpkBanPickInfo.round = round
    CLuaQMPKMgr.m_QmpkBanPickInfo.winForce = roundWinForceData     -- 每局胜者
    local attackMemberTbl = {}
    local defendMemberTbl = {}
    local banMemberTbl = {}

    for i = 1, #attackMember, 5 do
        local member = {
            Id = attackMember[i],
            Name = attackMember[i + 1],
            Class = attackMember[i + 2],
            Gender = attackMember[i + 3],
            Status = attackMember[i + 4],
        }
        table.insert(attackMemberTbl,member)
    end

    for i = 1, #defendMember, 5 do
        local member = {
            Id = defendMember[i],
            Name = defendMember[i + 1],
            Class = defendMember[i + 2],
            Gender = defendMember[i + 3],
            Status = defendMember[i + 4],
        }
        table.insert(defendMemberTbl,member)
    end

    if round == 4 then                  --只有第四局双人战才有ban操作
        banMemberTbl.attack = {
            Id = banMemberData[1], 
            Name = banMemberData[2],
            Class = banMemberData[3],
            Gender = banMemberData[4],
        }

        banMemberTbl.defend = {
            Id = banMemberData[5], 
            Name = banMemberData[6],
            Class = banMemberData[7],
            Gender = banMemberData[8],
        }
    end

    CLuaQMPKMgr.m_QmpkBanPickInfo.curTime = zhanDuiData[1]      -- 当前服务器时间
    CLuaQMPKMgr.m_QmpkBanPickInfo.endTime = zhanDuiData[2]      -- 当前阶段倒计时结束时间
    CLuaQMPKMgr.m_QmpkBanPickInfo.attackZhanDui = {             -- 红方战队
        Id = zhanDuiData[3],                                    -- 战队ID
        Name = zhanDuiData[4],                                  -- 战队名
        WinCount = zhanDuiData[7],                              -- 得分
        BPStatus = zhanDuiData[9],                              -- BP状态
        Member = attackMemberTbl,                               -- 成员信息
        Ban = banMemberTbl.attack or nil,                        -- 被Ban成员
    }

    CLuaQMPKMgr.m_QmpkBanPickInfo.defendZhanDui = {             -- 蓝方方战队
        Id = zhanDuiData[5],                                    -- 战队ID
        Name = zhanDuiData[6],                                  -- 战队名
        WinCount = zhanDuiData[8],                              -- 得分
        BPStatus = zhanDuiData[10],                             -- BP状态
        Member = defendMemberTbl,                               -- 成员信息
        Ban = banMemberTbl.defend or nil,                        -- 被Ban成员
    }

    if CClientMainPlayer.Inst then
        local id = CClientMainPlayer.Inst.Id 
        if CLuaQMPKMgr.m_QmpkBanPickInfo.force == EnumCommonForce_lua.eAttack then
            if id == CLuaQMPKMgr.m_QmpkBanPickInfo.attackZhanDui.Id then        -- 战队id就是队长ID
                CLuaQMPKMgr.m_QmpkBanPickInfo.m_position = EnumQMPKBPPlayerPosition.eTeamLeader
            else
                CLuaQMPKMgr.m_QmpkBanPickInfo.m_position = EnumQMPKBPPlayerPosition.eTeamMember
            end
        elseif CLuaQMPKMgr.m_QmpkBanPickInfo.force == EnumCommonForce_lua.eDefend then
            if id == CLuaQMPKMgr.m_QmpkBanPickInfo.defendZhanDui.Id then        -- 战队id就是队长ID
                CLuaQMPKMgr.m_QmpkBanPickInfo.m_position = EnumQMPKBPPlayerPosition.eTeamLeader
            else
                CLuaQMPKMgr.m_QmpkBanPickInfo.m_position = EnumQMPKBPPlayerPosition.eTeamMember
            end
        else
            CLuaQMPKMgr.m_QmpkBanPickInfo.m_position = EnumQMPKBPPlayerPosition.eOther         -- 不属于任何一方，观战
        end
    end
    g_ScriptEvent:BroadcastInLua("SyncQmpkBanPickInfo")
    -- 已经加载好或者正在加载中都不去尝试再打开界面
    if(not CUIManager.IsLoaded(CLuaUIResources.QMPKBattleBeforeWnd) and not CUIManager.IsLoading(CLuaUIResources.QMPKBattleBeforeWnd)) then
        CUIManager.ShowUI(CLuaUIResources.QMPKBattleBeforeWnd)
    end
end
CLuaQMPKMgr.m_QmpkSelectBanPickMemberInfo = nil
function Gas2Gac.ReplyQmpkQueryZhanDuiMemberForBanPick(memberInfoData, roundData1, roundData2, roundData3, roundData4, roundData5)

    local memberData = g_MessagePack.unpack(memberInfoData)
    local data1 =  g_MessagePack.unpack(roundData1)
    local data2 =  g_MessagePack.unpack(roundData2)
    local data3 =  g_MessagePack.unpack(roundData3)
    local data4 =  g_MessagePack.unpack(roundData4)
    local data5 =  g_MessagePack.unpack(roundData5)
    if not memberData or not data1 or not data2 or not data3 or not data4 or not data5 then return end
    CLuaQMPKMgr.m_QmpkSelectBanPickMemberInfo = {}
    local memberTbl = {}
    for i = 1, #memberData, 3 do
        local member = {
            Id = memberData[i],
            Name = memberData[i + 1],
            Class = memberData[i + 2],
        }
        table.insert(memberTbl,member)
    end
    CLuaQMPKMgr.m_QmpkSelectBanPickMemberInfo.memberData = memberTbl
    local round1member = {}
    local round2member = {}
    local round3member = {}
    local round4member = {}
    local round5member = {}

    for k,v in pairs(data1) do round1member[v] = true end
    for k,v in pairs(data2) do round2member[v] = true end
    for k,v in pairs(data3) do round3member[v] = true end
    for k,v in pairs(data4) do round4member[v] = true end
    for k,v in pairs(data5) do round5member[v] = true end
    CLuaQMPKMgr.m_QmpkSelectBanPickMemberInfo.roundMember = {round1member,round2member,round3member,round4member,round5member}
    g_ScriptEvent:BroadcastInLua("ReplyQmpkQueryZhanDuiMemberForBanPick")

end

CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo = nil
function Gas2Gac.ReplyQmpkQueryBiWuZhanDuiInfo(force, title, kouhao, round, memberInfoData, roundData1, roundData2, roundData3, roundData4, roundData5)
    --print("ReplyQmpkQueryBiWuZhanDuiInfo",force, title, kouhao, round, banMemberId, memberInfoData, roundData)
    local allmemberData = g_MessagePack.unpack(memberInfoData)
    local data1 =  g_MessagePack.unpack(roundData1)
    local data2 =  g_MessagePack.unpack(roundData2)
    local data3 =  g_MessagePack.unpack(roundData3)
    local data4 =  g_MessagePack.unpack(roundData4)
    local data5 =  g_MessagePack.unpack(roundData5)

    if not allmemberData or not data1 or not data2 or not data3 or not data4 or not data5 then return end
    local membertbl = {}
    for i = 1, #allmemberData, 4 do
        local member = {
            Id = allmemberData[i],
            Name = allmemberData[i + 1],
            Class = allmemberData[i + 2],
            Gender = allmemberData[i + 3],
        }
        table.insert(membertbl,member)
    end
    CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo = {}
    CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo.force = force
    CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo.round = round
    CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo.teamName = title
    CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo.teamSlogan = kouhao
    CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo.memberInfoData = membertbl
    local round1member = {}
    local round2member = {}
    local round3member = {}
    local round4member = {}
    local round5member = {}

    for k,v in pairs(data1) do round1member[v] = true end
    for k,v in pairs(data2) do round2member[v] = true end
    for k,v in pairs(data3) do round3member[v] = true end
    for k,v in pairs(data4) do round4member[v] = true end
    for k,v in pairs(data5) do round5member[v] = true end
    CLuaQMPKMgr.m_QmpkZhanDuiMemberInfo.roundMember = {round1member,round2member,round3member,round4member,round5member}
    if(not CUIManager.IsLoaded(CLuaUIResources.QMPKTeamMemberDetailWnd)) then
        CUIManager.ShowUI(CLuaUIResources.QMPKTeamMemberDetailWnd)
    end
end
CLuaQMPKMgr.m_QmpkActivityPageInfo = nil
-- 是否有可领取的奖励，活力值，总报名人数，已经领取的奖励
function Gas2Gac.ReplyQmpkActivityPageMeedInfo(canReceive, huoli, totalSignUpNum, receiveData)
    --print("ReplyQmpkActivityPageMeedInfo", canReceive, huoli, totalSignUpNum)
    -- receiveData 是一个list，里面存的是领取过的num，比如1000, 2000
    CLuaQMPKMgr.m_QmpkActivityPageInfo = {}
    CLuaQMPKMgr.m_QmpkActivityPageInfo.huoLi = huoli
    CLuaQMPKMgr.m_QmpkActivityPageInfo.totalSignUpNum = totalSignUpNum
    local receiveList = g_MessagePack.unpack(receiveData)
    local num2HasReceiveList = {}
    local totalReceiveItem = QuanMinPK_Setting.GetData().SignUpNum2ItemId
    local canReceiveItem = false
    local maxNum = 0

    for i = 1,#receiveList do
        num2HasReceiveList[receiveList[i]] = true
    end
    for i = 0,totalReceiveItem.Length - 1 do
        local num = totalReceiveItem[i][0]
        if totalSignUpNum >= num and not num2HasReceiveList[num] then canReceiveItem = true break end
    end
    CLuaQMPKMgr.m_QmpkActivityPageInfo.receiveList = num2HasReceiveList

    CLuaQMPKMgr.m_IsHandBookCanRecive = canReceive
    CLuaQMPKMgr.m_ProgressRewardCanRecive = canReceiveItem
    CLuaQMPKMgr.ProcessHandBookRedDot()
    g_ScriptEvent:BroadcastInLua("ReplyQmpkActivityPageMeedInfo")
end
