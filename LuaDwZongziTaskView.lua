local GameObject		    = import "UnityEngine.GameObject"
local UILabel				= import "UILabel"
local CommonDefs			= import "L10.Game.CommonDefs"
local DelegateFactory		= import "DelegateFactory"
local CUIManager            = import "L10.UI.CUIManager"
local CTaskMgr              = import "L10.Game.CTaskMgr"

CLuaDwZongziTaskView = class()

--注册View组件
RegistChildComponent(CLuaDwZongziTaskView, "TaskName",		UILabel)
RegistChildComponent(CLuaDwZongziTaskView, "TaskInfo",		UILabel)
RegistChildComponent(CLuaDwZongziTaskView, "LeaveBtn",		GameObject)
RegistChildComponent(CLuaDwZongziTaskView, "RuleBtn",		GameObject)

--注册变量
--RegistClassMember(CLuaDwWusesiHeChengWnd,"m_enable")

--[[
    @desc: 初始化
    author:CodeGize
    time:2019-04-16 11:22:58
    @return:
]]
function CLuaDwZongziTaskView:Init()
    local dwsetting = DuanWu_Setting.GetData()
    if dwsetting == nil then return end

    local taskid =dwsetting.ZongZi2019TaskId
	local inprogress = CTaskMgr.Inst:IsInProgress(taskid)
	if not inprogress then return end

    local des = CTaskMgr.Inst:GetTaskDescriptionSimple(taskid)
    self.TaskInfo.text = des
end

--[[
    @desc: 离开副本
    author:CodeGize
    time:2019-04-16 14:56:35
    @return:
]]
function CLuaDwZongziTaskView:OnLeaveBtnClick( )
    Gac2Gas.RequestLeavePlay()
end

--[[
    @desc: 显示规则界面
    author:CodeGize
    time:2019-04-16 14:56:46
    @return:
]]
function CLuaDwZongziTaskView:OnRuleBtnClick( )
    CUIManager.ShowUI("DwZongziRuleTipWnd")
end

--[[
    @desc: OnEnable中注册按钮事件
    author:CodeGize
    time:2019-04-16 14:57:01
    @return:
]]
function CLuaDwZongziTaskView:OnEnable( )
    local leaveClick = function ( go )
		self:OnLeaveBtnClick()
    end
    
    local ruleClick = function( go )
        self:OnRuleBtnClick()
    end

	CommonDefs.AddOnClickListener(self.LeaveBtn, DelegateFactory.Action_GameObject(leaveClick), false)
	CommonDefs.AddOnClickListener(self.RuleBtn, DelegateFactory.Action_GameObject(ruleClick), false)
end