local LuaGameObject=import "LuaGameObject"

local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local Color = import "UnityEngine.Color"


CLuaZhuojiResultInfoWnd = class()
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_RankLabel")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_ScoreLabel")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_ForceLabel")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_QueryResultBtn")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_ShareBtn")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_WildmillSprite")

RegistClassMember(CLuaZhuojiResultInfoWnd, "m_EyeSprite1")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_EyeSprite2")

RegistClassMember(CLuaZhuojiResultInfoWnd, "m_SelfNameLabel")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_SelfServerLabel")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_SelfIDLabel")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_SelfIconTexture")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_SelfLvlLabel")

RegistClassMember(CLuaZhuojiResultInfoWnd, "m_Tick")
RegistClassMember(CLuaZhuojiResultInfoWnd, "m_LastChangeSpriteTime")

function CLuaZhuojiResultInfoWnd:Init()
	self.m_ShareBtn = LuaGameObject.GetChildNoGC(self.transform, "shareBtn").gameObject
	UIEventListener.Get(self.m_ShareBtn).onClick = DelegateFactory.VoidDelegate(function(go)
		self:OnShareBtnClicked(go)
	end)

	self.m_QueryResultBtn = LuaGameObject.GetChildNoGC(self.transform, "checkBtn").gameObject
	UIEventListener.Get(self.m_QueryResultBtn).onClick = DelegateFactory.VoidDelegate(function(go)
		self:OnQueryBtnClicked(go)
	end)

	local infoNode = LuaGameObject.GetChildNoGC(self.transform, "info").transform
	self.m_ScoreLabel = LuaGameObject.GetChildNoGC(infoNode, "Score").label
	self.m_ForceLabel = LuaGameObject.GetChildNoGC(infoNode, "Force").label
	self.m_RankLabel = LuaGameObject.GetChildNoGC(infoNode, "rank").label

	local force2name = {[1] = LocalString.GetString("红队"), [2] = LocalString.GetString("黄队"), [3] = LocalString.GetString("蓝队"), [4] = LocalString.GetString("绿队"), }
	local force2color = {[1] = Color.red, [2] = Color.yellow, [3] = Color.blue, [4] = Color.green, }
	
	if CClientMainPlayer.Inst then
		if CLuaLiuYiMgr.FinalResult and CLuaLiuYiMgr.FinalResult.score and CLuaLiuYiMgr.FinalResult.force and CLuaLiuYiMgr.FinalResult.rank then
			self.m_ScoreLabel.text = CLuaLiuYiMgr.FinalResult.score .. ""
			self.m_ForceLabel.text = force2name[CLuaLiuYiMgr.FinalResult.force] or ""
			self.m_RankLabel.text = CLuaLiuYiMgr.FinalResult.rank
			self.m_ForceLabel.color = force2color[CLuaLiuYiMgr.FinalResult.force] or Color.white
		end
	end

	self.m_WildmillSprite = LuaGameObject.GetChildNoGC(infoNode, "wildmill").sprite
	self.m_EyeSprite1 = LuaGameObject.GetChildNoGC(infoNode, "lefteye").sprite
	self.m_EyeSprite2 = LuaGameObject.GetChildNoGC(infoNode, "righteye").sprite
	self.m_Tick = RegisterTick(function() self:OnTick() end, 33) 

	local playerNode = LuaGameObject.GetChildNoGC(self.transform, "playerInfoNode").transform
	self.m_SelfNameLabel = LuaGameObject.GetChildNoGC(playerNode, "name").label
	self.m_SelfServerLabel = LuaGameObject.GetChildNoGC(playerNode, "server").label
	self.m_SelfLvlLabel = LuaGameObject.GetChildNoGC(playerNode, "lv").label
	self.m_SelfIDLabel = LuaGameObject.GetChildNoGC(playerNode, "ID").label
	self.m_SelfIconTexture = LuaGameObject.GetChildNoGC(playerNode, "icon").cTexture
	if CClientMainPlayer.Inst then
		self.m_SelfNameLabel.text = CClientMainPlayer.Inst.Name
		local myserver = CLoginMgr.Inst:GetSelectedGameServer()
		self.m_SelfServerLabel.text = myserver.name
		self.m_SelfLvlLabel.text = CClientMainPlayer.Inst.Level .. ""
		self.m_SelfIDLabel.text = CClientMainPlayer.Inst.Id .. ""
		local portraitName = CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender)
		self.m_SelfIconTexture:LoadNPCPortrait(portraitName)
	end
end

function CLuaZhuojiResultInfoWnd:OnTick()
	local origZ = self.m_WildmillSprite.transform.localEulerAngles.z
	self.m_WildmillSprite.transform.localEulerAngles = Vector3(0, 0, (origZ + 5) % 360)

	self.m_LastChangeSpriteTime = (self.m_LastChangeSpriteTime or 0) + 1
	if self.m_LastChangeSpriteTime >= 90 then
		self.m_LastChangeSpriteTime = 0
		if self.m_EyeSprite1.spriteName == "zhuoji_openeye" then
			self.m_EyeSprite2.transform.localEulerAngles = Vector3(0, 0, 180)
		else
			self.m_EyeSprite2.transform.localEulerAngles = Vector3(0, 0, 0)
		end
		self.m_EyeSprite1.spriteName = (self.m_EyeSprite1.spriteName == "zhuoji_openeye") and "zhuoji_closeeye" or "zhuoji_openeye"
		self.m_EyeSprite2.spriteName = (self.m_EyeSprite2.spriteName == "zhuoji_openeye") and "zhuoji_closeeye" or "zhuoji_openeye"
	end
end

function CLuaZhuojiResultInfoWnd:OnShareBtnClicked(go)
	CUICommonDef.CaptureScreenAndShare()
end

function CLuaZhuojiResultInfoWnd:OnQueryBtnClicked(go)
	Gac2Gas.QueryZhuoJiPlayInfo()
end

function CLuaZhuojiResultInfoWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
return CLuaZhuojiResultInfoWnd