require("3rdParty/ScriptEvent")
require("common/common_include")

local CButton = import "L10.UI.CButton"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Item_Item = import "L10.Game.Item_Item"
local ChiJi_Item = import "L10.Game.ChiJi_Item"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CGac2Gas = import "L10.Game.Gac2Gas"
local CPUBGMgr = import "L10.Game.CPUBGMgr"

LuaPUBGLickingBagWnd = class()

-- 自身包裹
RegistChildComponent(LuaPUBGLickingBagWnd, "MyPackageView", CCommonLuaScript)

-- 他人包裹
RegistChildComponent(LuaPUBGLickingBagWnd, "OtherPlayerTitleLabel", UILabel)
RegistChildComponent(LuaPUBGLickingBagWnd, "BatchBtn", CButton)
RegistChildComponent(LuaPUBGLickingBagWnd, "PickBtn", CButton)
RegistChildComponent(LuaPUBGLickingBagWnd, "CancelBtn", CButton)

RegistChildComponent(LuaPUBGLickingBagWnd, "OthersItemCell", GameObject)
RegistChildComponent(LuaPUBGLickingBagWnd, "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPUBGLickingBagWnd, "Grid", UIGrid)

RegistChildComponent(LuaPUBGLickingBagWnd, "NormalBtnRoot", GameObject)
RegistChildComponent(LuaPUBGLickingBagWnd, "BatchBtnRoot", GameObject)

RegistClassMember(LuaPUBGLickingBagWnd, "OthersPackageItemList")
RegistClassMember(LuaPUBGLickingBagWnd, "OthersPackageInfos")
RegistClassMember(LuaPUBGLickingBagWnd, "BAGSIZE")
RegistClassMember(LuaPUBGLickingBagWnd, "IsBatchStatus")
RegistClassMember(LuaPUBGLickingBagWnd, "IsBatchingList")


function LuaPUBGLickingBagWnd:Awake()
    self.BAGSIZE = 25
    self.OthersPackageItemList = {}
    self.OthersPackageInfos = {}
    self.IsBatchStatus = false
    self.IsBatchingList = {}
    self.OthersItemCell:SetActive(false)
    
end

function LuaPUBGLickingBagWnd:Init()
    self.MyPackageView:Init({})

    self:InitOthersPUBGPackage()

    CommonDefs.AddOnClickListener(self.BatchBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnBatchBtnClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.PickBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnPickBtnClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.CancelBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnCancelBtnClicked(go)
    end), false)
end

-- 将packageInfo补全为25个
function LuaPUBGLickingBagWnd:ProcessOthersPackageInfos()
    self.OthersPackageInfos = {}

    if not LuaPUBGMgr.m_OthersPackageInfos then LuaPUBGMgr.m_OthersPackageInfos = {} end
    for i = 1, self.BAGSIZE do
        if i <= #LuaPUBGMgr.m_OthersPackageInfos then
            table.insert(self.OthersPackageInfos, LuaPUBGMgr.m_OthersPackageInfos[i])
        else
            table.insert(self.OthersPackageInfos, {itemId = 0, count = 0, enable = 0,id = 0})
        end
    end
end

function LuaPUBGLickingBagWnd:InitOthersPUBGPackage()

    self:ShowNormalBtnRoot()

    self:ProcessOthersPackageInfos()

    Extensions.RemoveAllChildren(self.Grid.transform)
    self.OthersPackageItemList = {}
    
    for i = 1, self.BAGSIZE do
        local instance = NGUITools.AddChild(self.Grid.gameObject, self.OthersItemCell)
        self:InitOthersPackageItem(instance, self.OthersPackageInfos[i], i)
        instance:SetActive(true)
        table.insert(self.OthersPackageItemList, instance)
    end

    self.Grid:Reposition()
    self.ScrollView:ResetPosition()

    
end

function LuaPUBGLickingBagWnd:InitOthersPackageItem(item, info, index)
    if not item then return end

    local IconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    IconTexture:Clear()

    local LockedSprite = item.transform:Find("LockedSprite").gameObject
    LockedSprite:SetActive(false)

    local CooldownSprite = item.transform:Find("CooldownSprite"):GetComponent(typeof(UISprite))
    CooldownSprite.gameObject:SetActive(false)

    local CannotDiscardSprite = item.transform:Find("CannotDiscardSprite").gameObject
    CannotDiscardSprite:SetActive(false)

    local DisableSprite = item.transform:Find("DisableSprite").gameObject
    DisableSprite:SetActive(false)

    local QualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    
    local ClearupCheckbox = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
    ClearupCheckbox.gameObject:SetActive(false)

    local AmountLabel = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    AmountLabel.text = ""

    local TextLabel = item.transform:Find("TextLabel"):GetComponent(typeof(UILabel))
    TextLabel.text = ""

    self:SetOthersPackageItemSelected(item, false)

    if not info then return end

    local chijiItem = ChiJi_Item.GetData(info.itemId)
    if not chijiItem then return end

    

    if LuaPUBGMgr.IsEquip(info.itemId) then
        local equip = EquipmentTemplate_Equip.GetData(chijiItem.ItemId)
        if equip then
            IconTexture:LoadMaterial(equip.Icon)
            AmountLabel.text = tostring(info.count)
            QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
        end
    else
        local item = Item_Item.GetData(chijiItem.ItemId)
        if item then
            IconTexture:LoadMaterial(item.Icon)
            AmountLabel.text = tostring(info.count)
            QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item, nil, false);
        end
    end

    local actsource = nil

    local pickAction = function ()
        -- todo 拾取 key, bagItemId, itemId
        local playDropObj = CPUBGMgr.Inst:GetObject(LuaPUBGMgr.m_SelectedDropKey)
        if playDropObj then
            CGac2Gas.RequestPickPlayerChiJiBagItem(LuaPUBGMgr.m_SelectedDropKey, playDropObj.ItemId, info.id)
        end
        
    end

    local t_action = {}
    t_action[1] = pickAction
    local t_name = {}
    t_name[1] = LocalString.GetString("拾取")
    actsource = DefaultItemActionDataSource.Create(1, t_action,t_name)
    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function (go) 
        self:OnOthersPackageItemSelected(go, info, index)
        if not self.IsBatchStatus then
            CItemInfoMgr.ShowLinkItemTemplateInfo(chijiItem.ItemId, false, actsource, AlignType.ScreenLeft, 0, 0, 0, 0) 
        end
    end),false)
end

function LuaPUBGLickingBagWnd:OnOthersPackageItemSelected(item, info, index)
    if not item then return end
    if self.IsBatchStatus then
        local ClearupCheckbox = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
        ClearupCheckbox.Selected = not ClearupCheckbox.Selected

        if ClearupCheckbox.Selected and not self:IsAlreadyWaitToBatch(index) then
            table.insert(self.IsBatchingList, index)
        elseif not ClearupCheckbox.Selected and self:IsAlreadyWaitToBatch(index) then
            self:RemoveFromBatchingList(index)
        end
    else
        self.SelectedItemIndex = index
        for i = 1, #self.OthersPackageItemList do
            self:SetOthersPackageItemSelected(self.OthersPackageItemList[i], (self.OthersPackageItemList[i] == item))
        end
    end
end

-- 是否已经被选中捡起
function LuaPUBGLickingBagWnd:IsAlreadyWaitToBatch(index)
    if not self.IsBatchingList then return false end

    for i = 1, #self.IsBatchingList do
        if self.IsBatchingList[i] == index then
            return true
        end
    end
    return false
end

function LuaPUBGLickingBagWnd:RemoveFromBatchingList(index)
    if not self.IsBatchingList then return end
    for i = #self.IsBatchingList, 1, -1 do
        if self.IsBatchingList[i] == index then
            table.remove(self.IsBatchingList, i)
        end
    end
end

-- 整体选中状态
function LuaPUBGLickingBagWnd:SetOthersPackageItemSelected(item, isSelected)
    if not item then return end

    local SelectedSprite = item.transform:Find("SelectedSprite").gameObject
    SelectedSprite:SetActive(isSelected)
end

-- checkBox选中状态
function LuaPUBGLickingBagWnd:UpdateBatchState(item, isBatch, index)
    if not item then return end

    if isBatch then
        if not self.OthersPackageInfos[index] or self.OthersPackageInfos[index].itemId == 0 then return end
    end

    local ClearupCheckbox = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
    ClearupCheckbox.gameObject:SetActive(isBatch)
    ClearupCheckbox.Selected = false
end

function LuaPUBGLickingBagWnd:OnBatchBtnClicked(go)
    self:ShowBatchBtnRoot()
end

function LuaPUBGLickingBagWnd:OnCancelBtnClicked(go)
    self:ShowNormalBtnRoot()
end

function LuaPUBGLickingBagWnd:OnPickBtnClicked(go)
    -- todo 捡多个物品
    if not self.IsBatchingList or #self.IsBatchingList <= 0 then return end
    local playDropObj = CPUBGMgr.Inst:GetObject(LuaPUBGMgr.m_SelectedDropKey)
    if not playDropObj then return end
    
    for i = 1, #self.IsBatchingList do
        local info = self.OthersPackageInfos[self.IsBatchingList[i]]
        if info then
            CGac2Gas.RequestPickPlayerChiJiBagItem(LuaPUBGMgr.m_SelectedDropKey, playDropObj.ItemId, info.id)
        end
    end
    self:ClearSelectionUnderBatch()
end

function LuaPUBGLickingBagWnd:ShowNormalBtnRoot()
    self.IsBatchStatus = false
    self.NormalBtnRoot:SetActive(true)
    self.BatchBtnRoot:SetActive(false)
    if not self.OthersPackageItemList then return end
    self:ClearSelectionUnderBatch()
    for i = 1, #self.OthersPackageItemList do
        self:UpdateBatchState(self.OthersPackageItemList[i], false, i)
    end
end

function LuaPUBGLickingBagWnd:ShowBatchBtnRoot()
    self.IsBatchStatus = true
    self.NormalBtnRoot:SetActive(false)
    self.BatchBtnRoot:SetActive(true)
    if not self.OthersPackageItemList then return end
    self:ClearSelectinUnderNonBatch()
    for i = 1, #self.OthersPackageItemList do
        self:UpdateBatchState(self.OthersPackageItemList[i], true, i)
    end
end

function LuaPUBGLickingBagWnd:ClearSelectionUnderBatch()
    self.IsBatchingList = {}
end

function LuaPUBGLickingBagWnd:ClearSelectinUnderNonBatch()
    if self.SelectedItemIndex ~= -1 then
        self.SelectedItemIndex = -1
        for i = 1, #self.OthersPackageItemList do
            self:SetOthersPackageItemSelected(self.OthersPackageItemList[i], false, i)
        end
    end
end

function LuaPUBGLickingBagWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateLickingBag", self, "InitOthersPUBGPackage")
end

function LuaPUBGLickingBagWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateLickingBag", self, "InitOthersPUBGPackage")
end

return LuaPUBGLickingBagWnd