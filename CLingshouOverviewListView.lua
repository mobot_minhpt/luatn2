-- Auto Generated!!
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingshouOverviewInfo = import "L10.UI.CLingshouOverviewListView+CLingshouOverviewInfo"
local CLingshouOverviewItemCell = import "L10.UI.CLingshouOverviewItemCell"
local CLingshouOverviewListView = import "L10.UI.CLingshouOverviewListView"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local DelegateFactory = import "DelegateFactory"
local EnumOverviewType = import "L10.UI.CLingshouOverviewListView+EnumOverviewType"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
local LingShouOverview = import "L10.Game.CLingShouBaseMgr+LingShouOverview"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLingshouOverviewListView.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.lingshouList)
    CommonDefs.ListClear(this.idList)
    Extensions.RemoveAllChildren(this.table.transform)
    this.lingshouTemplate:SetActive(false)
    LingShou_LingShou.Foreach(MakeDelegateFromCSFunction(this.FilterLingshou, MakeGenericClass(Action2, Object, Object), this))
    this:Sort()
    this:InitLingshou()
    this.table:Reposition()
    this.scrollView:ResetPosition()
    if this.lingshouList.Count > 0 then
        this:OnLingshouClicked(this.lingshouList[0])
    end
end
CLingshouOverviewListView.m_FilterLingshou_CS2LuaHook = function (this, key, data)
    if data.Status == 0 then
        return
    elseif data.Status == 2 and not CLingShouBaseMgr.ShowLingshouByPlayConfig() then
        return
    end
    local info = CreateFromClass(CLingshouOverviewInfo)
    info.id = key
    info.hasOwn = true
    repeat
        local default = this.type
        if default == EnumOverviewType.all then
            CommonDefs.ListAdd(this.idList, typeof(CLingshouOverviewInfo), info)
            break
        elseif default == EnumOverviewType.normal then
            if data.ShenShou == 0 then
                CommonDefs.ListAdd(this.idList, typeof(CLingshouOverviewInfo), info)
            end
            break
        elseif default == EnumOverviewType.shenshou then
            if data.ShenShou ~= 0 then
                CommonDefs.ListAdd(this.idList, typeof(CLingshouOverviewInfo), info)
            end
            break
        else
            break
        end
    until 1
end
CLingshouOverviewListView.m_Sort_CS2LuaHook = function (this) 
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    do
        local i = 0
        while i < this.idList.Count do
            this.idList[i].hasOwn = CommonDefs.ListFind(list, typeof(LingShouOverview), DelegateFactory.Predicate_LingShouOverview(function (p) 
                return p.templateId == this.idList[i].id
            end)) ~= nil
            i = i + 1
        end
    end
    CommonDefs.ListSort1(this.idList, typeof(CLingshouOverviewInfo), DelegateFactory.Comparison_CLingshouOverviewInfo(function (data1, data2) 
        local lingshou1 = LingShou_LingShou.GetData(data1.id)
        local lingshou2 = LingShou_LingShou.GetData(data2.id)
        return NumberCompareTo(lingshou1.Index, lingshou2.Index)
    end))
end
CLingshouOverviewListView.m_InitLingshou_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.idList.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.lingshouTemplate)
            instance:SetActive(true)
            local itemcell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CLingshouOverviewItemCell))
            if itemcell ~= nil then
                itemcell:Init(this.idList[i].id, this.idList[i].hasOwn)
                CommonDefs.ListAdd(this.lingshouList, typeof(GameObject), instance)
                UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnLingshouClicked, VoidDelegate, this), true)
            end
            i = i + 1
        end
    end
end
CLingshouOverviewListView.m_OnLingshouClicked_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.lingshouList.Count do
            local itemcell = CommonDefs.GetComponent_GameObject_Type(this.lingshouList[i], typeof(CLingshouOverviewItemCell))
            if itemcell ~= nil then
                itemcell:SetSelect(go == this.lingshouList[i])
                if go == this.lingshouList[i] and this.OnLingshouSelect ~= nil then
                    GenericDelegateInvoke(this.OnLingshouSelect, Table2ArrayWithCount({itemcell.lingshouId}, 1, MakeArrayClass(Object)))
                end
            end
            i = i + 1
        end
    end
end
CLingshouOverviewListView.m_OnFilterButtonClick_CS2LuaHook = function (this, go) 
    local keyPairList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    local allSelect = PopupMenuItemData(this.allTitle, MakeDelegateFromCSFunction(this.AllSelect, MakeGenericClass(Action1, Int32), this), false, nil)
    local normalSelect = PopupMenuItemData(this.normalTitle, MakeDelegateFromCSFunction(this.NormalSelect, MakeGenericClass(Action1, Int32), this), false, nil)
    local shenshouSelect = PopupMenuItemData(this.shenshouTitle, MakeDelegateFromCSFunction(this.ShenshouSelect, MakeGenericClass(Action1, Int32), this), false, nil)
    CommonDefs.ListAdd(keyPairList, typeof(PopupMenuItemData), allSelect)
    CommonDefs.ListAdd(keyPairList, typeof(PopupMenuItemData), normalSelect)
    CommonDefs.ListAdd(keyPairList, typeof(PopupMenuItemData), shenshouSelect)
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(keyPairList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 600, true, this.buttonWidth)
end
