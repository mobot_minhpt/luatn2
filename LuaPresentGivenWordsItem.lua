require("common/common_include")

local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"

LuaPresentGivenWordsItem = class()
RegistClassMember(LuaPresentGivenWordsItem,"gameObject")
RegistClassMember(LuaPresentGivenWordsItem,"transform")
RegistClassMember(LuaPresentGivenWordsItem,"textLabel")
RegistClassMember(LuaPresentGivenWordsItem,"button")

RegistClassMember(LuaPresentGivenWordsItem,"Index")
RegistClassMember(LuaPresentGivenWordsItem,"Text")

function LuaPresentGivenWordsItem:Init(gameObject, index, text)
	self.gameObject = gameObject
	self.transform = gameObject.transform
    self.textLabel = self.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.button = self.transform:GetComponent(typeof(CButton))
	self.Index = index
	self.Text = text
	self.textLabel.text = text
	self.button.Selected = false
end

function LuaPresentGivenWordsItem:SetSelected(selected)
	if not self.button then return end
	self.button.Selected = selected
end

return LuaPresentGivenWordsItem