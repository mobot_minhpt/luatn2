local UITable = import "UITable"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local QnTabButton = import "L10.UI.QnTabButton"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QnTableView = import "L10.UI.QnTableView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaCommonTextImageRuleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonTextImageRuleWnd, "TextTab", "TextTab", QnTabButton)
RegistChildComponent(LuaCommonTextImageRuleWnd, "ImageTab", "ImageTab", QnTabButton)
RegistChildComponent(LuaCommonTextImageRuleWnd, "TextRoot", "TextRoot", GameObject)
RegistChildComponent(LuaCommonTextImageRuleWnd, "ImageRoot", "ImageRoot", GameObject)
RegistChildComponent(LuaCommonTextImageRuleWnd, "ImageDescLabel", "ImageDescLabel", UILabel)
RegistChildComponent(LuaCommonTextImageRuleWnd, "ImageTexture", "ImageTexture", CUITexture)
RegistChildComponent(LuaCommonTextImageRuleWnd, "AddSubBtn", "AddSubBtn", QnAddSubAndInputButton)
RegistChildComponent(LuaCommonTextImageRuleWnd, "TitleView", "TitleView", QnTableView)
RegistChildComponent(LuaCommonTextImageRuleWnd, "PicCountLabel", "PicCountLabel", UILabel)
RegistChildComponent(LuaCommonTextImageRuleWnd, "TextScrollView", "TextScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaCommonTextImageRuleWnd, "TextTable", "TextTable", UITable)
RegistChildComponent(LuaCommonTextImageRuleWnd, "TextTemplate", "TextTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaCommonTextImageRuleWnd, "m_TextData")
RegistClassMember(LuaCommonTextImageRuleWnd, "m_ImageData")

function LuaCommonTextImageRuleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaCommonTextImageRuleWnd:Init()
    self.TextTab.onClick = DelegateFactory.Action_QnTabButton(function(btn)
        self:ShowText()
    end)

    self.ImageTab.onClick = DelegateFactory.Action_QnTabButton(function(btn)
        self:ShowImage()
    end)

    self.m_TextData = LuaCommonTextImageRuleMgr.m_TextRuleInfo
    self.m_ImageData = LuaCommonTextImageRuleMgr.m_ImageRuleInfo

    self:ShowText()
end

function LuaCommonTextImageRuleWnd:SetMode(isText)
    self.ImageRoot:SetActive(not isText)
    self.TextRoot:SetActive(isText)
    self.TextTab.Selected = isText
    self.ImageTab.Selected = not isText
end

function LuaCommonTextImageRuleWnd:ShowText()
    self:SetMode(true)

    self.TitleView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_TextData
    end, function(item, index)
        self:InitTitle(item, index)
    end)

    -- 设置选中状态
    self.TitleView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 显示文字
        self:InitTextTable(self.m_TextData[row+1].Msg)
    end)

    -- 刷新数据
	self.TitleView:ReloadData(true,false)

	self.TitleView:SetSelectRow(0, true)
    self:InitTextTable(self.m_TextData[1].Msg)
end

function LuaCommonTextImageRuleWnd:InitTextTable(msg)
    self.TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.TextTable.transform)

    local msg = g_MessageMgr:FormatMessage(msg)
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end

    self.TextScrollView:ResetPosition()
    self.TextTable:Reposition()
end

function LuaCommonTextImageRuleWnd:InitTitle(item, index)
    local data = self.m_TextData[index+1]
    local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
    label.text = data.Title
end

function LuaCommonTextImageRuleWnd:ShowImage()
    self:SetMode(false)

    -- 设置
    self.AddSubBtn:SetMinMax(1, #self.m_ImageData, 1)
    self.AddSubBtn.onValueChanged = DelegateFactory.Action_uint(function(index)
        self:SetPicIndex(index)
    end)

    self:SetPicIndex(1)
end

-- index从1开始
function LuaCommonTextImageRuleWnd:SetPicIndex(index)
    self.PicCountLabel.text = index .. "/" .. #self.m_ImageData
    local data = self.m_ImageData[index]

    self.ImageTexture:LoadMaterial(data.ImagePath)
    self.ImageDescLabel.text = g_MessageMgr:FormatMessage(data.Msg)
end

--@region UIEvent

--@endregion UIEvent

