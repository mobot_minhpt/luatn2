local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animation = import "UnityEngine.Animation"

LuaBaoYuDuoYu2023SignUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Difficulty", "Difficulty", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "EasyModeButton", "EasyModeButton", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "HeroModeButton", "HeroModeButton", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Need", "Need", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "RequireLevelInfo", "RequireLevelInfo", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Type", "Type", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "TypeInfo", "TypeInfo", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Time", "Time", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "TimeInfo", "TimeInfo", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "TaskDescribeInfo", "TaskDescribeInfo", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Reward", "Reward", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "RewardItem1", "RewardItem1", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "RewardItem2", "RewardItem2", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "PlayButton", "PlayButton", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "BriefTexture", "BriefTexture", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "Brief", "Brief", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "BriefTitle", "BriefTitle", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SignUpWnd, "BriefHint", "BriefHint", UILabel)

--@endregion RegistChildComponent end

function LuaBaoYuDuoYu2023SignUpWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:InitUIEvent()
    
    self:SelectDifficulty(1)
end

function LuaBaoYuDuoYu2023SignUpWnd:Init()

end

--@region UIEvent

--@endregion UIEvent
function LuaBaoYuDuoYu2023SignUpWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDouble11BYDLYPlayInfo", self, "CloseUI")
end

function LuaBaoYuDuoYu2023SignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDouble11BYDLYPlayInfo", self, "CloseUI")
end

function LuaBaoYuDuoYu2023SignUpWnd:CloseUI()
    CUIManager.CloseUI(CLuaUIResources.BaoYuDuoYu2023SignUpWnd)
    CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2023MainWnd)
end

function LuaBaoYuDuoYu2023SignUpWnd:InitWndData()
    self.wndConfigData = Double11_BYDLY.GetData()

    
    self.heroWeekJoinTimes = Double11_BYDLY.GetData().HeroWeekJoinTimes
    self.dailyAwardLimitTimes = tonumber(g_LuaUtil:StrSplit(Double11_BYDLY.GetData().DailyAwardInfo, ",")[1])
    self.heroAwardLimitTimes = tonumber(g_LuaUtil:StrSplit(Double11_BYDLY.GetData().HeroAwardInfo, ",")[1])
    self.eliteAwardLimitTimes = tonumber(g_LuaUtil:StrSplit(Double11_BYDLY.GetData().EliteAwardInfo, ",")[1])
    self.eliteSeconds = tonumber(g_LuaUtil:StrSplit(Double11_BYDLY.GetData().EliteAwardInfo, ",")[2])

    self.selectDifficultyIndex = 0
end

function LuaBaoYuDuoYu2023SignUpWnd:RefreshConstUI()
    self.RequireLevelInfo.text = self.wndConfigData.MainWndLevelLimit
    self.TypeInfo.text = self.wndConfigData.MainWndType
    self.TimeInfo.text = self.wndConfigData.MainWndTime
end 

function LuaBaoYuDuoYu2023SignUpWnd:InitUIEvent()
    UIEventListener.Get(self.EasyModeButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:SelectDifficulty(1)
        self.transform:GetComponent(typeof(Animation)):Play("baoyuduoyu2023signupwnd_switch02")
    end)

    UIEventListener.Get(self.HeroModeButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:SelectDifficulty(2)
        self.transform:GetComponent(typeof(Animation)):Play("baoyuduoyu2023signupwnd_switch01")
    end)

    UIEventListener.Get(self.PlayButton).onClick = DelegateFactory.VoidDelegate(function (_)
        Gac2Gas.EnterDouble11BYDLYPlay(self.selectDifficultyIndex)
    end)

    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("BaoYuDuoYu_MainWnd_Rule")
    end)
end

function LuaBaoYuDuoYu2023SignUpWnd:SelectDifficulty(difficulty)
    local modeButtonList = {self.EasyModeButton, self.HeroModeButton}
    for i = 1, #modeButtonList do
        local modeButton = modeButtonList[i]
        local isSelected = i == difficulty
        modeButton.transform:Find("IsSelected").gameObject:SetActive(isSelected)
    end
    
    self:RefreshBrief(difficulty)
    self:RefreshReward(difficulty)
end 

function LuaBaoYuDuoYu2023SignUpWnd:RefreshBrief(difficulty)
    self.BriefHint.text = CUICommonDef.TranslateToNGUIText(self.wndConfigData.MainWndDesc)
end 

function LuaBaoYuDuoYu2023SignUpWnd:RefreshReward(difficulty)
    if difficulty == 1 then
        self.selectDifficultyIndex = 0
        self.TaskDescribeInfo.text = LocalString.GetString("无挑战次数限制")
        self.RewardItem1:SetActive(true)
        self.RewardItem2:SetActive(false)
        self.PlayButton.transform:Find("EnterButtonLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("挑战普通难度")
        self.PlayButton.transform:Find("EnterButtonLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("351B01", 0)
        self.PlayButton.transform:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023resultwnd_anniu.mat")
        
        local getAwardTime = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11BYDLYDailyAwardTimes) or 0
        local awardLimit = self.dailyAwardLimitTimes
        local templateId = Double11_BYDLY.GetData().MainWndRewardItems[0]
        self:InitRewardItem(self.RewardItem1, templateId, getAwardTime >= awardLimit, 
                LocalString.GetString("每日通关奖励"),  SafeStringFormat3(LocalString.GetString("剩%d次"), awardLimit-getAwardTime))
        self.BriefTexture.transform:Find("HeroMode").gameObject:SetActive(false)
    else
        self.selectDifficultyIndex = 1
        local heroChallengeTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11BYDLYHeroWeekJoinTimes) or 0
        local getAwardTime = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11BYDLYHeroAwardTimes) or 0
        local eliteAwardTime = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11BYDLYEliteAwardTimes) or 0
        self.PlayButton.transform:Find("EnterButtonLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("挑战英雄难度")
        self.PlayButton.transform:Find("EnterButtonLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("FDFFF3", 0)
        self.PlayButton.transform:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023signupwnd_anniu_02.mat")

        self.TaskDescribeInfo.text = SafeStringFormat3(LocalString.GetString("挑战限次 本周剩%d次机会"), self.heroWeekJoinTimes-heroChallengeTimes)
        self.RewardItem1:SetActive(true)
        self:InitRewardItem(self.RewardItem1, Double11_BYDLY.GetData().MainWndRewardItems[1], getAwardTime >= self.heroAwardLimitTimes,
                LocalString.GetString("每周通关奖励"), SafeStringFormat3(LocalString.GetString("剩%d次"), self.heroAwardLimitTimes-getAwardTime))
        self.RewardItem2:SetActive(true)
        self:InitRewardItem(self.RewardItem2, Double11_BYDLY.GetData().MainWndRewardItems[2], eliteAwardTime >= self.eliteAwardLimitTimes,
        LocalString.GetString("每周精英奖励"),
        self.eliteSeconds/60 .. LocalString.GetString("分内通过可再得（每周[c][fffc19]限1次[-][/c]）"))
        self.BriefTexture.transform:Find("HeroMode").gameObject:SetActive(true)
    end
end 

function LuaBaoYuDuoYu2023SignUpWnd:InitRewardItem(rewardItemObj, templateId, isGotAll, name, description)
    local texture = rewardItemObj.transform:Find("Item/Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(templateId)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(rewardItemObj.transform:Find("Item").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
    rewardItemObj.transform:Find("Item/Check").gameObject:SetActive(isGotAll)
    rewardItemObj.transform:Find("Title"):GetComponent(typeof(UILabel)).text = name
    if isGotAll then
        rewardItemObj.transform:Find("RewardText"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已获得")
    else
        rewardItemObj.transform:Find("RewardText"):GetComponent(typeof(UILabel)).text = description
    end
    
end 