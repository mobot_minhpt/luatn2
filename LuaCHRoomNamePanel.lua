local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"

LuaCHRoomNamePanel = class()

RegistClassMember(LuaCHRoomNamePanel, "Panel") --房间类型选择
RegistClassMember(LuaCHRoomNamePanel, "RoomNameLabel1") --房间类型选择
RegistClassMember(LuaCHRoomNamePanel, "RoomNameLabel2") --搜索按钮

RegistClassMember(LuaCHRoomNamePanel, "m_RoomId") 
RegistClassMember(LuaCHRoomNamePanel, "m_IsSpecialRoom") 

function LuaCHRoomNamePanel:Awake()
    self.Panel = self.transform:GetComponent(typeof(UIPanel))
    self.RoomNameLabel1 = self.transform:Find("RoomNameLabel1"):GetComponent(typeof(UILabel))
    self.RoomNameLabel2 = self.transform:Find("RoomNameLabel2"):GetComponent(typeof(UILabel))

    self.m_TwoNameLabelOffset = 60
    self.m_NamePanelClipRegion = self.Panel.baseClipRegion
    self.m_RoomNamePanelWidth = self.m_NamePanelClipRegion.z
    self.m_ClipRegionCenterOffsetX = self.m_NamePanelClipRegion.z / 2 - self.m_NamePanelClipRegion.x
    self.m_RoomNameLabelInitPos = self.RoomNameLabel1.transform.localPosition
    self.m_RoomNameLabelRollSpeed = GameplayItem_Setting.GetData().ClubHouse_RoomNameLabelRollSpeed
end

function LuaCHRoomNamePanel:Init(roomId, roomName, isSpecialRoom)
    self.m_RoomId = roomId
    self.m_IsSpecialRoom = isSpecialRoom
    self:SetRoomName(roomName)
end

function LuaCHRoomNamePanel:SetNamePanelWidth(width)
    -- 重新调整panel宽度
    self.m_RoomNamePanelWidth = width
    self.m_NamePanelClipRegion.z = width
    self.m_NamePanelClipRegion.x = width / 2 - self.m_ClipRegionCenterOffsetX
    self.Panel.baseClipRegion = self.m_NamePanelClipRegion
    -- 调整后重新滚动
    self:SetRoomNameRoll()
end

function LuaCHRoomNamePanel:SetRoomName(name)
    self.RoomNameLabel1.text = name
    self.RoomNameLabel2.text = name
    self.RoomNameLabel1.color = self.m_IsSpecialRoom and NGUIText.ParseColor24("e6eada", 0) or NGUIText.ParseColor24("9DC1F2", 0)
    self.RoomNameLabel2.color = self.m_IsSpecialRoom and NGUIText.ParseColor24("e6eada", 0) or NGUIText.ParseColor24("9DC1F2", 0)
    self.m_isShow = true
    self:SetRoomNameRoll()
end

function LuaCHRoomNamePanel:SetRoomNameRoll()
    LuaTweenUtils.DOKill(self.RoomNameLabel1.transform, false)
    LuaTweenUtils.DOKill(self.RoomNameLabel2.transform, false)
    local roomNameLabelWidth = self.RoomNameLabel1.localSize.x
    LuaUtils.SetLocalPositionX(self.RoomNameLabel1.transform, self.m_RoomNameLabelInitPos.x)
    LuaUtils.SetLocalPositionX(self.RoomNameLabel2.transform, self.m_RoomNameLabelInitPos.x + roomNameLabelWidth + self.m_TwoNameLabelOffset)
    if roomNameLabelWidth> self.m_RoomNamePanelWidth then
        self.RoomNameLabel2.gameObject:SetActive(true)
        local beginPosX = self.RoomNameLabel1.localSize.x + 2 * self.m_TwoNameLabelOffset
        local reDuration = (2 * self.RoomNameLabel1.localSize.x + 2 * self.m_TwoNameLabelOffset) / self.m_RoomNameLabelRollSpeed
        self:DoRollTween(self.RoomNameLabel1.transform, roomNameLabelWidth / self.m_RoomNameLabelRollSpeed, beginPosX, -roomNameLabelWidth, reDuration)
        self:DoRollTween(self.RoomNameLabel2.transform, (2 * roomNameLabelWidth + self.m_TwoNameLabelOffset) / self.m_RoomNameLabelRollSpeed, beginPosX, -roomNameLabelWidth, reDuration)
    else
        self.RoomNameLabel2.gameObject:SetActive(false)
    end
end

function LuaCHRoomNamePanel:DoRollTween(trans, firstDuration, beginPosX, endPosX, reDuration)
    -- 第一次
    local tween = LuaTweenUtils.TweenPositionX(trans, endPosX, firstDuration)
    LuaTweenUtils.SetEase(tween, Ease.Linear)
    -- 反复
    LuaTweenUtils.OnComplete(tween, function()
        self:DoReRollTween(trans, beginPosX, endPosX, reDuration)
    end)
end

function LuaCHRoomNamePanel:DoReRollTween(labelTrans, beginPosX, endPosX, duration)
    LuaUtils.SetLocalPositionX(labelTrans, beginPosX)
    local tween = LuaTweenUtils.TweenPositionX(labelTrans, endPosX, duration)
    LuaTweenUtils.SetEase(tween, Ease.Linear)
    LuaTweenUtils.OnComplete(tween, function()
        self:DoReRollTween(labelTrans, beginPosX, endPosX, duration)
    end)
end

function LuaCHRoomNamePanel:OnClubHouseNotifyRenameRoom(RoomId, Name)
    if RoomId == self.m_RoomId then
        self:SetRoomName(Name)
    end
end

function LuaCHRoomNamePanel:OnClubHouseNotifyUpdateTitle(RoomId, Title)
    if RoomId == self.m_RoomId then
        local titleInfo = GameplayItem_ClubHouseTitle.GetData(Title)
        self.m_IsSpecialRoom = titleInfo ~= nil
        self.RoomNameLabel1.color = self.m_IsSpecialRoom and NGUIText.ParseColor24("e6eada", 0) or NGUIText.ParseColor24("9DC1F2", 0)
        self.RoomNameLabel2.color = self.m_IsSpecialRoom and NGUIText.ParseColor24("e6eada", 0) or NGUIText.ParseColor24("9DC1F2", 0)
    end
end

function LuaCHRoomNamePanel:OnEnable()
	g_ScriptEvent:AddListener("ClubHouse_Notify_RenameRoom", self, "OnClubHouseNotifyRenameRoom")
    g_ScriptEvent:AddListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle")
end

function LuaCHRoomNamePanel:OnDisable()
    LuaTweenUtils.DOKill(self.RoomNameLabel1.transform, false)
    LuaTweenUtils.DOKill(self.RoomNameLabel2.transform, false)
	g_ScriptEvent:RemoveListener("ClubHouse_Notify_RenameRoom", self, "OnClubHouseNotifyRenameRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle") 
end