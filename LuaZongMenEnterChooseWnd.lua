local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local BoxCollider = import "UnityEngine.BoxCollider"
local UIDragScrollView = import "UIDragScrollView"
local UICamera = import "UICamera"

LuaZongMenEnterChooseWnd = class()
RegistClassMember(LuaZongMenEnterChooseWnd,"m_TableViewDataSource")
RegistClassMember(LuaZongMenEnterChooseWnd,"m_TableView")
RegistClassMember(LuaZongMenEnterChooseWnd,"m_Info")
RegistClassMember(LuaZongMenEnterChooseWnd,"m_Bg")

function LuaZongMenEnterChooseWnd:Init()
    if LuaZongMenMgr.m_ShowMapId == nil or LuaZongMenMgr.m_ShowMapInfo == nil then
        CUIManager.CloseUI(CLuaUIResources.ZongMenEnterChooseWnd)
    end

    self.m_Info = LuaZongMenMgr.m_ShowMapInfo
    self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    self.m_Bg = self.transform:Find("Bg"):GetComponent(typeof(UIWidget))

    -- 更新背景高度
    local count = self.m_Info.Count/4
    self.m_Bg.height = count>8 and 800 or (97*count + 10)
    local box = self.m_Bg.gameObject.transform:GetComponent(typeof(BoxCollider))
    box.enabled = count>8

    local currentPos = UICamera.lastWorldPosition
    currentPos.z = 1
    currentPos.y = self.m_Bg.height / 5000

    -- 左边的在右边 右边的在左边
    if currentPos.x >0 then
        currentPos.x = currentPos.x - 0.7
    else
        currentPos.x = currentPos.x + 0.7
    end

    self.transform.position = currentPos

    local function initItem(item,index)
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount( self.m_Info.Count/4,initItem)
    self.m_TableView.m_DataSource = self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
    end)
    self.m_TableView:ReloadData(true, false)
end

function LuaZongMenEnterChooseWnd:InitItem(item,index)
    -- 节点
    local tf = item.transform
    local nameLabel = FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local selfIcon = FindChild(tf,"SelfIcon").gameObject
    local enemyIcon = FindChild(tf,"EnemyIcon").gameObject
    local drag = tf:GetComponent(typeof(UIDragScrollView))
    drag.enabled = self.m_Info.Count/4 > 8
    
    -- 数据
    local sectStatus = self.m_Info[index*4]
    local sectName  = self.m_Info[index*4+1]
    local sectId    = self.m_Info[index*4+2]
    local mapId     = self.m_Info[index*4+3]

    nameLabel.text = sectName
    if CClientMainPlayer.Inst and sectId == CClientMainPlayer.Inst.BasicProp.SectId then
        selfIcon:SetActive(true)
        enemyIcon:SetActive(false)
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            Gac2Gas.RequestTrackToSectEntrance()
            CUIManager.CloseUI(CLuaUIResources.ZongMenEnterChooseWnd)
            CUIManager.CloseUI(CUIResources.MiniMapPopWnd)
        end)
    else
        selfIcon:SetActive(false)
        enemyIcon:SetActive(true)
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            Gac2Gas.RequestTrackEnemySect(sectId)
            CUIManager.CloseUI(CLuaUIResources.ZongMenEnterChooseWnd)
            CUIManager.CloseUI(CUIResources.MiniMapPopWnd)
        end)
    end
end
