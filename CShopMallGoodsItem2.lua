-- Auto Generated!!
local AlignType           = import "L10.UI.CItemInfoMgr+AlignType"
local CEquipment          = import "L10.Game.CEquipment"
local CItem               = import "L10.Game.CItem"
local CItemInfoMgr        = import "L10.UI.CItemInfoMgr"
local CItemMgr            = import "L10.Game.CItemMgr"
local CShopMallGoodsItem2 = import "L10.UI.CShopMallGoodsItem2"
local DelegateFactory     = import "DelegateFactory"
local EnumEventType       = import "EnumEventType"
local EventManager        = import "EventManager"
local IdPartition         = import "L10.Game.IdPartition"
local LocalString         = import "LocalString"
local UIEventListener     = import "UIEventListener"

CShopMallGoodsItem2.m_UpdateData_CS2LuaHook = function (this, template, showItemInfo)
    this.templateId = template.ItemId
    this.showItemInfo = showItemInfo
    
    local item = CItemMgr.Inst:GetItemTemplate(this.templateId)
    this.m_Texture:LoadMaterial(item.Icon)
    this.m_NameLabel.text = item.Name

    --是否是人民币礼包
    if System.String.IsNullOrEmpty(template.RmbPID) then
        this.m_CurrencySprite.gameObject:SetActive(true)
        this.m_PriceLabel.text = tostring(template.Price)
    else
        this.m_CurrencySprite.gameObject:SetActive(false)
        local cfgData = Mall_LingYuMallLimit.GetData(template.ItemId)
        if CommonDefs.Is_PC_PLATFORM() or CommonDefs.Is_UNITY_STANDALONE_WIN() then
            local originalShowPrice = cfgData.ShowPrice
            local priceList = g_LuaUtil:StrSplit(originalShowPrice, ";")
            LuaSEASdkMgr:AddProductPrice(template.ItemId, priceList[LocalString.languageId+1])
        else
            local pidStr = ""
            pidStr = pidStr .. "\"" .. template.ItemId .. "\""
            SdkU3d.ntExtendFunc("{\"methodId\":\"vngProductInfo\", \"items\":["..pidStr.."]}")
        end
        local showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[template.ItemId] and LuaSEASdkMgr.cachePriceData[template.ItemId]) or ""
        this.m_PriceLabel.text = showPrice
        if showPrice == "" then
            RegisterTickOnce(function ()
                showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[template.ItemId] and LuaSEASdkMgr.cachePriceData[template.ItemId]) or cfgData.ShowPrice
                if not CommonDefs.IsNull(this.m_PriceLabel) then
                    this.m_PriceLabel.text = showPrice
                end
            end, 300)
        end
    end
    this.m_SoldoutSprite.gameObject:SetActive(false)

    --限购
    if template.AvalibleCount >= 0 then
        this.m_WeekAvalibleCountLabel.fontSize = 28
        --lsy
        if template.IsGlobalLimitRmbPackage == 1 then
            this.m_LifeAvalibleCountLabel.gameObject:SetActive(false)
            this.m_WeekAvalibleCountLabel.gameObject:SetActive(false)
            this.m_QuanFuXianGou_CountLabel.gameObject:SetActive(true)
            this.m_QuanFuXianGou_CountLabel.text = LocalString.GetString("全服限购")
        elseif template.IsAllLifeLimit then
            this.m_LifeAvalibleCountLabel.gameObject:SetActive(true)
            this.m_WeekAvalibleCountLabel.gameObject:SetActive(false)
            this.m_QuanFuXianGou_CountLabel.gameObject:SetActive(false)
            local str = System.String.Format(LocalString.GetString("限购{0}次"), template.AvalibleCount)

            if CommonDefs.IS_CN_CLIENT then
                this.m_LifeAvalibleCountLabel.text = LocalString.StrH2V(str, true)
            else
                this.m_LifeAvalibleCountLabel.text = string.gsub(str, " ", "\n")
            end
        else
            this.m_LifeAvalibleCountLabel.gameObject:SetActive(false)
            this.m_WeekAvalibleCountLabel.gameObject:SetActive(true)
            this.m_QuanFuXianGou_CountLabel.gameObject:SetActive(false)

            local str = ""
            if template.IsMonthLimit then
                str = SafeStringFormat3(LocalString.GetString("本月限购%s次"), tostring(template.AvalibleCount))
                -- 处理一下分格
                if CommonDefs.IS_HMT_CLIENT and template.AvalibleCount == 100 then
                    str = SafeStringFormat3(LocalString.GetString("本月限购%s次"), "1\n0\n0")
                    this.m_WeekAvalibleCountLabel.fontSize = 24
                end
            else
                str = SafeStringFormat3(LocalString.GetString("本周限购%s次"), tostring(template.AvalibleCount))
                if CommonDefs.IS_HMT_CLIENT and template.AvalibleCount == 100 then
                    str = SafeStringFormat3(LocalString.GetString("本周限购%s次"), "1\n0\n0")
                    this.m_WeekAvalibleCountLabel.fontSize = 24
                end
            end
            if CommonDefs.IS_CN_CLIENT then
                this.m_WeekAvalibleCountLabel.text = string.gsub(str, " ", "\n")
            else
                this.m_WeekAvalibleCountLabel.text = LocalString.StrH2V(str, true)
            end
            this.m_WeekAvalibleCountLabel.transform:Find('num').gameObject:SetActive(false)
        end
        if template.AvalibleCount == 0 then
            this:MarkedAsSoldout()
        end
    else
        this.m_LifeAvalibleCountLabel.gameObject:SetActive(false)
        this.m_WeekAvalibleCountLabel.gameObject:SetActive(false)
        this.m_QuanFuXianGou_CountLabel.gameObject:SetActive(false)
    end

    this.disableSprite.enabled = false
    this:UpdateDisableStatus()
end
CShopMallGoodsItem2.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListener(EnumEventType.MainPlayerLevelChange, MakeDelegateFromCSFunction(this.UpdateDisableStatus, Action0, this))
    UIEventListener.Get(this.m_Texture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        local item = CItemMgr.Inst:GetItemTemplate(this.templateId)
        if this.showItemInfo then
            CItemInfoMgr.ShowLinkItemTemplateInfo(this.templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
        if this.OnClickItemTexture ~= nil then
            invoke(this.OnClickItemTexture)
        end
    end)
end
CShopMallGoodsItem2.m_UpdateDisableStatus_CS2LuaHook = function (this)
    if IdPartition.IdIsItem(this.templateId) then
        this.disableSprite.enabled = not CItem.GetMainPlayerIsFit(this.templateId, true)
    elseif IdPartition.IdIsEquip(this.templateId) then
        this.disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(this.templateId, true)
    end
end
