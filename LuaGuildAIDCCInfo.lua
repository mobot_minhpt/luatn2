local DelegateFactory = import "DelegateFactory"
local Constants = import "L10.Game.Constants"
local CButton = import "L10.UI.CButton"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"

LuaGuildAIDCCInfo = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildAIDCCInfo, "m_CommanderNameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaGuildAIDCCInfo, "m_ListenButton", "ListenToBtn", CButton)
RegistChildComponent(LuaGuildAIDCCInfo, "m_ListenIcon", "PlayStatusIcon", UISprite)

--@endregion RegistChildComponent end


function LuaGuildAIDCCInfo:Awake()
    UIEventListener.Get(self.m_ListenButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnListenButtonClick()
	end)
end

function LuaGuildAIDCCInfo:UpdateGuildAIDCCInfo()
    local existCommander = LuaChatMgr:GuildAIDCommanderExists()
    local commanderName = LuaChatMgr.m_GuildAidCommanderInfo.commanderName
    self.m_CommanderNameLabel.text = cs_string.IsNullOrEmpty(commanderName) and LocalString.GetString("暂无") or commanderName
    self.m_ListenButton.gameObject:SetActive(existCommander)
    self:RefreshListenIcon()
end

function LuaGuildAIDCCInfo:OnEnable()
    self:UpdateGuildAIDCCInfo()
    g_ScriptEvent:AddListener("OnJoinCCStream", self, "OnJoinCCStream")
    g_ScriptEvent:AddListener("OnQuitCCStream", self, "OnQuitCCStream")
    g_ScriptEvent:AddListener("OnGuildAIDCommanderUpdate", self, "OnGuildAIDCommanderUpdate")
end

function LuaGuildAIDCCInfo:OnDisable()
    g_ScriptEvent:RemoveListener("OnJoinCCStream", self, "OnJoinCCStream")
    g_ScriptEvent:RemoveListener("OnQuitCCStream", self, "OnQuitCCStream")
    g_ScriptEvent:RemoveListener("OnGuildAIDCommanderUpdate", self, "OnGuildAIDCommanderUpdate")
end

function LuaGuildAIDCCInfo:OnJoinCCStream(args)
    self:RefreshListenIcon()
end

function LuaGuildAIDCCInfo:OnQuitCCStream(args)
    self:RefreshListenIcon()
end

function LuaGuildAIDCCInfo:OnGuildAIDCommanderUpdate()
    self:UpdateGuildAIDCCInfo()
end

function LuaGuildAIDCCInfo:RefreshListenIcon()
    local inGuildAidCC = CCMiniAPIMgr.Inst:IsInGuildAidStream()
    self.m_ListenButton.Text = inGuildAidCC and LocalString.GetString("禁听") or LocalString.GetString("收听")
    self.m_ListenIcon.spriteName = inGuildAidCC and Constants.Common_Pause_Icon or Constants.Common_Play_Icon
end

function LuaGuildAIDCCInfo:OnListenButtonClick()
    LuaChatMgr:SetGuildAIDCCListen()
end


