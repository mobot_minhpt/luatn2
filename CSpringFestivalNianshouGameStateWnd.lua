-- Auto Generated!!
local CScene = import "L10.Game.CScene"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local CSpringFestivalNianshouGameStateWnd = import "L10.UI.CSpringFestivalNianshouGameStateWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CSpringFestivalNianshouGameStateWnd.m_Init_CS2LuaHook = function (this) 
    if CSpringFestivalMgr.Instance.yongShiCount ~= 0 and CSpringFestivalMgr.Instance.yongShiMaxCount ~= 0 and CSpringFestivalMgr.Instance.nianShouCount ~= 0 and CSpringFestivalMgr.Instance.nianShouCount ~= 0 then
        this.hiderNumber.text = (tostring(CSpringFestivalMgr.Instance.yongShiCount) .. "/") .. tostring(CSpringFestivalMgr.Instance.yongShiMaxCount)
        this.seekerNumber.text = (tostring(CSpringFestivalMgr.Instance.nianShouCount) .. "/") .. tostring(CSpringFestivalMgr.Instance.nianShouCount)
    else
        this.hiderNumber.text = ""
        this.seekerNumber.text = ""
    end
    Gac2Gas.RequestNSDZZTeamInfo()
end
CSpringFestivalNianshouGameStateWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.expandButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        this.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    UIEventListener.Get(this.leaveButton).onClick = MakeDelegateFromCSFunction(this.OnLeaveButtonClick, VoidDelegate, this)
end
CSpringFestivalNianshouGameStateWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.UpdatePlayerNumLeft, MakeDelegateFromCSFunction(this.UpdatePlayerNum, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end
CSpringFestivalNianshouGameStateWnd.m_OnRemainTimeUpdate_CS2LuaHook = function (this, leftTime) 
    if CScene.MainScene ~= nil then
        this.remainingTime.text = this:GetRemainTimeText(CScene.MainScene.ShowTime)
    else
        this.remainingTime.text = ""
    end
end
CSpringFestivalNianshouGameStateWnd.m_GetRemainTimeText_CS2LuaHook = function (this, totalSeconds) 
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return System.String.Format("[ACF9FF]{0:00}:{1:00}:{2:00}[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return System.String.Format("[ACF9FF]{0:00}:{1:00}[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
