local GameObject        = import "UnityEngine.GameObject"
local CButton           = import "L10.UI.CButton"
local DelegateFactory   = import "DelegateFactory"
local UILabel           = import "UILabel"
local UIGrid            = import "UIGrid"
local UITable           = import "UITable"
local UIWidget          = import "UIWidget"
local UITexture         = import "UITexture"
local UIBasicSprite     = import "UIBasicSprite"
local UIEventListener   = import "UIEventListener"
local NGUIText          = import "NGUIText"
local CUIFx             = import "L10.UI.CUIFx"
local Vector3           = import "UnityEngine.Vector3"
local UIRoot            = import "UIRoot"
local TweenAlpha        = import "TweenAlpha"
local TweenPosition     = import "TweenPosition"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUICommonDef      = import "L10.UI.CUICommonDef"
local CPos              = import "L10.Engine.CPos"
local CUIWidgetHSV      = import "L10.UI.CUIWidgetHSV"
local UISprite          = import "UISprite"
local CConversationMgr  = import "L10.Game.CConversationMgr"
local Clipboard         = import "L10.Engine.Clipboard"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Input             = import "UnityEngine.Input"
local CTaskMgr          = import "L10.Game.CTaskMgr"
local Constants         = import "L10.Game.Constants"
local Screen            = import "UnityEngine.Screen"
local Vector2           = import "UnityEngine.Vector2"
local CGuideMgr         = import "L10.Game.Guide.CGuideMgr"

LuaXinShengHuaJiDrawWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "Portrait", "Portrait", GameObject)
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "ShareButton", "ShareButton", CButton)
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "SubmitButton", "SubmitButton", CButton)
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "ReturnButton", "ReturnButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "RandomButton", "RandomButton", GameObject)
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "FinishButton", "FinishButton", CButton)
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "Part", "Part", GameObject)
RegistChildComponent(LuaXinShengHuaJiDrawWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaXinShengHuaJiDrawWnd, "brush")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "appearFx")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "dragRegion")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "rightDown")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "left")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "leftTbl")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "tipLabel")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "wnds")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "templates")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "grids")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "flipButton")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "avatarButton")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "themeName")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "maleButton")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "femaleButton")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "scrollView34")

RegistClassMember(LuaXinShengHuaJiDrawWnd, "portraitTweenAlpha")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "leftTweenPosition")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "leftTweenAlpha")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "rightTweenPosition")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "rightTweenAlpha")

RegistClassMember(LuaXinShengHuaJiDrawWnd, "sexId")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "selectedUITbl")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "selectedUIId")-- 当前选择的UI Id
RegistClassMember(LuaXinShengHuaJiDrawWnd, "classfiedItemTbl")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "typeTransTbl")-- 颜色type对应部件的transform
RegistClassMember(LuaXinShengHuaJiDrawWnd, "curChoiceId")-- 当前选择的Id
RegistClassMember(LuaXinShengHuaJiDrawWnd, "curChoiceInfo")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "parts")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "isModified")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "dragDistance")
RegistClassMember(LuaXinShengHuaJiDrawWnd, "flip")

RegistClassMember(LuaXinShengHuaJiDrawWnd, "needLoadedType") -- 完成加载部分数据后再显示图像
RegistClassMember(LuaXinShengHuaJiDrawWnd, "curLoadedType") -- 当前已经加载的数据

-- 衍生剧情需要
RegistClassMember(LuaXinShengHuaJiDrawWnd, "fixedChoice")-- 固定选择
RegistClassMember(LuaXinShengHuaJiDrawWnd, "tick")-- 显示计时
RegistClassMember(LuaXinShengHuaJiDrawWnd, "isDrawingFinished")

function LuaXinShengHuaJiDrawWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShareButtonClick()
    end)

    UIEventListener.Get(self.SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSubmitButtonClick()
    end)

    UIEventListener.Get(self.ReturnButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnReturnButtonClick()
    end)

    UIEventListener.Get(self.RandomButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRandomButtonClick()
    end)

    UIEventListener.Get(self.FinishButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnFinishButtonClick()
    end)

    UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCloseButtonClick()
    end)

    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
    self:InitDerivativePlotActive()
end

-- 初始化UI组件
function LuaXinShengHuaJiDrawWnd:InitUIComponents()
    self.wnds = {}
    local anchor = self.transform:Find("Anchor")
    self.appearFx = anchor:Find("AppearFx"):GetComponent(typeof(CUIFx))
    self.portraitTweenAlpha = self.Portrait.transform:GetComponent(typeof(TweenAlpha))

    self.wnds[1] = anchor:Find("BrushWnd").gameObject
    self.brush = self.wnds[1].transform:Find("Brush")
    self.dragRegion = self.wnds[1].transform:Find("Container").gameObject

    self.wnds[2] = anchor:Find("DrawWnd").gameObject
    self.rightDown = self.wnds[2].transform:Find("RightDown").gameObject
    self.left = self.wnds[2].transform:Find("Left")
    self.leftTbl = self.left:Find("Table")
    self.leftTweenPosition = self.left:GetComponent(typeof(TweenPosition))
    self.leftTweenAlpha = self.left:GetComponent(typeof(TweenAlpha))

    self.templates = {}
    self.grids = {}

    local right = self.wnds[2].transform:Find("Right")
    self.rightTweenPosition = right:GetComponent(typeof(TweenPosition))
    self.rightTweenAlpha = right:GetComponent(typeof(TweenAlpha))
    self.templates[2] = right:Find("Level2/Level2Template").gameObject
    self.templates[2]:SetActive(false)
    self.grids[2] = right:Find("Level2/ScrollView/Grid"):GetComponent(typeof(UIGrid))

    self.templates[3] = right:Find("Level34/Level3Template").gameObject
    self.templates[3]:SetActive(false)
    self.templates[4] = right:Find("Level34/Level4Template").gameObject
    self.templates[4]:SetActive(false)
    self.grids[3] = right:Find("Level34/ScrollView/Table"):GetComponent(typeof(UITable))
    self.grids[4] = self.grids[3].transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.scrollView34 = right:Find("Level34/ScrollView"):GetComponent(typeof(UIScrollView))

    self.wnds[3] = anchor:Find("FinishWnd").gameObject
    self.tipLabel = self.wnds[3].transform:Find("Tip"):GetComponent(typeof(UILabel))
    self.avatarButton = self.wnds[3].transform:Find("AvatarButton").gameObject
    self.avatarButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    self.flipButton = self.rightDown.transform:Find("FlipButton").gameObject
    self.themeName = self.wnds[2].transform:Find("Top/ThemeName"):GetComponent(typeof(UILabel))
    self.maleButton = self.left:Find("MaleButton")
    self.femaleButton = self.left:Find("FemaleButton")
end

function LuaXinShengHuaJiDrawWnd:InitEventListener()
    UIEventListener.Get(self.flipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnFlipButtonClick()
    end)

    UIEventListener.Get(self.avatarButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnAvatarButtonClick()
    end)

    UIEventListener.Get(self.maleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnGenderClick(0)
    end)

    UIEventListener.Get(self.femaleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnGenderClick(1)
    end)
end

function LuaXinShengHuaJiDrawWnd:InitActive()
    self.Part:SetActive(false)
end

function LuaXinShengHuaJiDrawWnd:OnEnable()
    g_ScriptEvent:AddListener("XinShengHuaJiAddPictureSuccess", self, "OnAddPicSuccess")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("XinShengHuaJiAddTaskPic", self, "OnAddTaskPic")
    g_ScriptEvent:AddListener("XinShengHuaJiUnlockSuccess", self, "OnUnlockSuccess")

    g_ScriptEvent:AddListener("XinShengHuaJiCopyPictureData", self, "OnCopyPictureData")
end

function LuaXinShengHuaJiDrawWnd:OnDisable()
    g_ScriptEvent:RemoveListener("XinShengHuaJiAddPictureSuccess", self, "OnAddPicSuccess")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("XinShengHuaJiAddTaskPic", self, "OnAddTaskPic")
    g_ScriptEvent:RemoveListener("XinShengHuaJiUnlockSuccess", self, "OnUnlockSuccess")

    g_ScriptEvent:RemoveListener("XinShengHuaJiCopyPictureData", self, "OnCopyPictureData")
end

function LuaXinShengHuaJiDrawWnd:OnUpdateTempPlayTimesWithKey(args)
    if LuaWuMenHuaShiMgr.isDerivativePlot then return end

    local key = args[0]
    if key == EnumPlayTimesKey_lua.eXinShengHuaJiAdjustLimit or
        key == EnumPlayTimesKey_lua.eXinShengHuaJiTimes then
        LuaItemInfoMgr:CloseItemConsumeWnd()
        self:UpdateTip()
    end
end

function LuaXinShengHuaJiDrawWnd:OnAddPicSuccess(time, data_U)
    if LuaWuMenHuaShiMgr.isDerivativePlot then return end

    self.isModified = false
end

function LuaXinShengHuaJiDrawWnd:OnAddTaskPic(taskId)
    if taskId == LuaWuMenHuaShiMgr.taskId then
        self.isDrawingFinished = true
        self:PlayConfirmTaskDialog()
    end
end

function LuaXinShengHuaJiDrawWnd:OnUnlockSuccess(choice)
    local id = 0
    for i = 1, #self.curChoiceInfo do
        if self.curChoiceInfo[i].choice == choice then
            self.curChoiceInfo[i].unlock = 1
            id = i
            break
        end
    end
    if id == 0 then return end

    local item = self.grids[4].transform:GetChild(id - 1)
    item:Find("Locked").gameObject:SetActive(self.curChoiceInfo[id].unlock <= 0)
    item:Find("UnlockFx"):GetComponent(typeof(CUIFx)):LoadFx(g_UIFxPaths.XinShengHuaJiUnlockFxPath)
end


function LuaXinShengHuaJiDrawWnd:Init()
    self:InitItemTbl()
    self:InitPortrait()
    self:InitSpecial()
end

-- 初始化策划表数据
function LuaXinShengHuaJiDrawWnd:InitItemTbl()
    LuaWuMenHuaShiMgr:InitItemTbl()
    self:InitClassfiedItemTbl()
end

-- 初始化画像
function LuaXinShengHuaJiDrawWnd:InitPortrait()
    if LuaWuMenHuaShiMgr.isPictureShow or LuaWuMenHuaShiMgr.isHYGSDraw then
        self.Portrait:SetActive(false)
        self.needLoadedType = {200, 202, 211, 230}
        self.curLoadedType = {}
    end

    self:InitChoiceId()
    self:InitEachPart()
    self:InitBody()
end

-- 初始化curChoiceId
function LuaXinShengHuaJiDrawWnd:InitChoiceId()
    local dataTbl = self.classfiedItemTbl
    local tbl = {}

    for i1 = 1, #dataTbl do
        tbl[i1] = {}
        for i2 = 1, #dataTbl[i1] do
            tbl[i1][i2] = {}
            if dataTbl[i1][i2][0] ~= nil then
                tbl[i1][i2][0] = -1
            else
                for i3 = 1, #dataTbl[i1][i2] do
                    tbl[i1][i2][i3] = -1
                end
            end
        end
    end

    self.curChoiceId = tbl
end

-- 初始化各个部件同时初始化typeTransTbl
function LuaXinShengHuaJiDrawWnd:InitEachPart()
    self.parts = {}
    self.typeTransTbl = {}
    local dataTbl = self.classfiedItemTbl

    for i1 = 1, #dataTbl do
        self.parts[i1] = {}
        for i2 = 1, #dataTbl[i1] do
            self.parts[i1][i2] = {}
            local subTbl = dataTbl[i1][i2]
            if subTbl[0] ~= nil then
                if subTbl[0].isColor == false then
                    self.parts[i1][i2][0] = self:GenerateOnePart(subTbl[0])
                end
            else
                for i3 = 1, #subTbl do
                    if subTbl[i3].isColor == false then
                        self.parts[i1][i2][i3] = self:GenerateOnePart(subTbl[i3])
                    end
                end
            end
        end
    end
end

-- 初始化一个部件，设置其深度
function LuaXinShengHuaJiDrawWnd:GenerateOnePart(item)
    local depth = item.depth
    local affectedByColorType = item.affectedByColorType

    local tbl = {}
    for i = 1, #depth do
        local child = CUICommonDef.AddChild(self.Portrait, self.Part).transform
        local tex = child:Find("Texture"):GetComponent(typeof(UITexture))
        local sprite = child:Find("Sprite"):GetComponent(typeof(UISprite))

        local depthFinal = depth[i] + LuaWuMenHuaShiMgr.depthPlus
        tex.depth = depthFinal
        sprite.depth = depthFinal

        -- 初始化HSV为0,0,0
        for j = 1, child.childCount do
            local cuiWidgetHSV = child:GetChild(j - 1):GetComponent(typeof(CUIWidgetHSV))
            if cuiWidgetHSV then
                cuiWidgetHSV:SetHSVOffset(0, 0, 0)
            end
        end

        table.insert(tbl, child)

        -- 更新颜色对应关系表
        if affectedByColorType ~= nil and affectedByColorType[i] ~= nil then
            local sexType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType
            if affectedByColorType[i] >= sexType then
                local colorType = affectedByColorType[i]
                if self.typeTransTbl[colorType] == nil then
                    self.typeTransTbl[colorType] = {}
                end
                table.insert(self.typeTransTbl[colorType], child)
            end
        end
    end
    return tbl
end

-- 随机选择部件
function LuaXinShengHuaJiDrawWnd:RandomChoice()
    local dataTbl = self.classfiedItemTbl

    for i1 = 1, #dataTbl do
        for i2 = 1, #dataTbl[i1] do
            if dataTbl[i1][i2][0] ~= nil then
                local choiceId = self:RandomOneChoice(i1, i2, 0)
                self:ChangePart(i1, i2, 0, choiceId)
            else
                for i3 = 1, #dataTbl[i1][i2] do
                    local choiceId = self:RandomOneChoice(i1, i2, i3)
                    self:ChangePart(i1, i2, i3, choiceId)
                end
            end
        end
    end
    self:UpdateFlip(LuaWuMenHuaShiMgr:RandomNum(0, 1))

    self.isModified = false
end

-- 随机一个选项
function LuaXinShengHuaJiDrawWnd:RandomOneChoice(i1, i2, i3)
    local choiceInfo = self:GetChoiceInfo(i1, i2, i3)
    if #choiceInfo == 0 then return -1 end

    local unlockNum = 0
    for i = 1, #choiceInfo do
        if choiceInfo[i].unlock == 1 then
            unlockNum = unlockNum + 1
        else
            break
        end
    end

    if unlockNum == 0 then return -1 end
    local id = LuaWuMenHuaShiMgr:RandomNum(1, unlockNum)
    return choiceInfo[id].choice
end

-- 获取可选项
function LuaXinShengHuaJiDrawWnd:GetChoiceInfo(i1, i2, i3)
    local result = {}

    local subTbl = self.classfiedItemTbl[i1][i2][i3]

    local tmpChoice
    if i3 == 2 then
        local choiceId = self.curChoiceId[i1][i2][1]
        if choiceId < 0 then return result end

        local colors = WuMenHuaShi_XinShengHuaJiDrawItem.GetData(choiceId).ChangeColor
        tmpChoice = LuaWuMenHuaShiMgr:Array2Tbl(colors)
    else
        tmpChoice = subTbl.choice
    end

    if not tmpChoice then return result end
    for i = 1, #tmpChoice do
        if tmpChoice[i] <= 0 or subTbl.isAvailable then
            table.insert(result, {choice = tmpChoice[i], sn = 0, exchangeNum = 0, unlock = 1})
        else
            local choiceData
            if subTbl.isColor then
                choiceData = WuMenHuaShi_ItemColor.GetData(tmpChoice[i])
            else
                choiceData = WuMenHuaShi_XinShengHuaJiDrawItem.GetData(tmpChoice[i])
            end
            local sn = choiceData.SN
            local exchangeNum = choiceData.ExchangeNum

            local unlock = 1
            if sn > 0 then
                unlock = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.XinShengHuaJiUnlockInfo:GetBit(sn)) and 1 or 0
            end

            if sn <= 0 or not LuaWuMenHuaShiMgr.isDerivativePlot then
                table.insert(result, {choice = tmpChoice[i], sn = sn, exchangeNum = exchangeNum, unlock = unlock})
            end
        end
    end

    -- 排序 未解锁的放后面
    table.sort(result, function(a, b)
        if a.unlock ~= b.unlock then return a.unlock > b.unlock end

        return a.choice < b.choice
    end)
    return result
end

-- 改变部件
function LuaXinShengHuaJiDrawWnd:ChangePart(i1, i2, i3, choiceId)
    local subTbl = self.classfiedItemTbl[i1][i2][i3]
    self.curChoiceId[i1][i2][i3] = choiceId

    if subTbl.isColor then
        local type = subTbl.type
        self:ChangeColor(choiceId, type)
    else
        local trans = self.parts[i1][i2][i3]
        self:ChangeStyle(choiceId, trans, subTbl.type)
    end
end

-- 改变样式
function LuaXinShengHuaJiDrawWnd:ChangeStyle(choiceId, trans, type)
    if choiceId < 0 then
        for i = 1, #trans do
            trans[i].gameObject:SetActive(false)
        end
    else
        -- 获取图像、位置和尺寸信息
        local drawItem = WuMenHuaShi_XinShengHuaJiDrawItem.GetData(choiceId)
        local pic = LuaWuMenHuaShiMgr:Array2Tbl(drawItem.Picture)
        local pos = LuaWuMenHuaShiMgr:ParseXY(drawItem.Position)
        local size = LuaWuMenHuaShiMgr:ParseXY(drawItem.Size)
        local texOrSprite = drawItem.TextureOrSprite

        for i = 1, #trans do
            LuaWuMenHuaShiMgr:SetMatPosAndSize(trans[i], texOrSprite, pic[i], pos[i], size[i], LuaWuMenHuaShiMgr.sexId,
                DelegateFactory.Action_bool(function(isFinish)
                    if isFinish then
                        if not self.needLoadedType or self.Portrait.active then return end

                        for k, v in pairs(self.needLoadedType) do
                            if v == type then
                                self.curLoadedType[v] = true
                                break
                            end
                        end

                        for k, v in pairs(self.needLoadedType) do
                            if not self.curLoadedType[v] then
                                return
                            end
                        end

                        self.Portrait:SetActive(true)
                    end
                end))
            trans[i].gameObject:SetActive(true)
        end
    end
end

-- 改变颜色
function LuaXinShengHuaJiDrawWnd:ChangeColor(choiceId, type)
    if choiceId <= 0 then return end

    local data = WuMenHuaShi_ItemColor.GetData(choiceId)

    local trans = self.typeTransTbl[type]
    if trans == nil then return end
    for i = 1, #trans do
        for j = 1, trans[i].childCount do
            local cuiWidgetHSV = trans[i]:GetChild(j - 1):GetComponent(typeof(CUIWidgetHSV))
            if cuiWidgetHSV then
                cuiWidgetHSV:SetHSVOffset(data.Hue, data.Saturation, data.Brightness)
            end
        end
    end
end

-- 初始化身体
function LuaXinShengHuaJiDrawWnd:InitBody()
    local sexType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType
    local sexTypeData = WuMenHuaShi_XinShengHuaJi.GetData(sexType)
    local item = {}
    item.affectedByColorType = LuaWuMenHuaShiMgr:Array2Tbl(sexTypeData.AffectedByColorType)
    item.depth = LuaWuMenHuaShiMgr:Array2Tbl(sexTypeData.Depth)

    local tbl = self:GenerateOnePart(item)
    self:ChangeStyle(LuaWuMenHuaShiMgr.sexId, tbl, sexType)
end

-- 更新翻转
function LuaXinShengHuaJiDrawWnd:UpdateFlip(flip)
    if LuaWuMenHuaShiMgr.isDerivativePlot or (self.flip and self.flip == flip) then return end

    self.flip = flip
    self.flipButton.transform:Find("Selected").gameObject:SetActive(flip == 1)
    Extensions.SetLocalRotationY(self.Portrait.transform, flip * 180)
    Extensions.SetLocalPositionX(self.Portrait.transform, -440 + flip * 780)
    Extensions.SetPositionZ(self.Portrait.transform, 0.1)
end

-- 初始化粉刷界面
function LuaXinShengHuaJiDrawWnd:InitBrushWnd()
    self:SetWndActive(1)
    self.portraitTweenAlpha:ResetToBeginning()

    UIEventListener.Get(self.dragRegion).onPress = DelegateFactory.BoolDelegate(function(go, isPressed)
        self:UpdateBrushPosition()
    end)

    UIEventListener.Get(self.dragRegion).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
        self:OnDrag(go, delta)
    end)

    local cuiFx = self.brush:Find("Fx"):GetComponent(typeof(CUIFx))
    UIEventListener.Get(self.dragRegion).onDragStart = DelegateFactory.VoidDelegate(function(go)
        cuiFx:LoadFx(g_UIFxPaths.XinShengHuaJiBrushFx)
    end)

    UIEventListener.Get(self.dragRegion).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        cuiFx:DestroyFx()
    end)
end

-- 进入绘图界面
function LuaXinShengHuaJiDrawWnd:EnterDrawWnd(init)
    self:SetWndActive(2)
    self.rightDown:SetActive(true)
    self:PlayTween(true)
    if init == false then return end
    self:InitSelectedUI()

    local dataTbl = self.classfiedItemTbl
    for i = 1, self.leftTbl.childCount do
        local item = self.leftTbl:GetChild(i - 1)
        item:Find("Label"):GetComponent(typeof(UILabel)).text = dataTbl[i].name
        self:AddSelectedUI(item, 1)
        if #dataTbl[i] == 0 then
            item:Find("Normal"):GetComponent(typeof(UIWidget)).color = NGUIText.ParseColor24("6F6F6F", 0)
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                g_MessageMgr:ShowMessage("XINSHENGHUAJI_MODIFICATION_NOT_SUPPORTED")
            end)
        else
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnLevelClick(1, i)
            end)
        end
    end

    for i = 1, #dataTbl do
        if #dataTbl[i] > 0 then
            self:SetSelectedUIActive(1, i, true)
            break
        end
    end
    self:UpdateLevel(2)
end

-- 切换完成界面
function LuaXinShengHuaJiDrawWnd:EnterFinishWnd()
    self:PlayTween(false)
    self.rightDown:SetActive(false)
end

-- 更新完成界面的tip文本
function LuaXinShengHuaJiDrawWnd:UpdateTip()
    if not CClientMainPlayer.Inst then return end

    local freeTimes = WuMenHuaShi_XinShengHuaJiSetting.GetData().UploadDefaultLimit
    local maxAdjustTimes = WuMenHuaShi_XinShengHuaJiSetting.GetData().UploadMaxAdjustLimit
    local uploadAdjustLimit = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXinShengHuaJiAdjustLimit)
    local uploadTimes = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXinShengHuaJiTimes)

    local ownTimes = freeTimes + uploadAdjustLimit - uploadTimes
    local leftTimes = maxAdjustTimes - uploadAdjustLimit
    if ownTimes < 0 then ownTimes = 0 end
    if leftTimes < 0 then leftTimes = 0 end
    self.tipLabel.text = g_MessageMgr:FormatMessage("XINSHENGHUAJI_UPLOADTIMES_TIP", ownTimes, leftTimes)
end

-- 播放动效
function LuaXinShengHuaJiDrawWnd:PlayTween(forward)
    if forward then
        self:SetFromTo(0, 1, -240, 0, 240, 0)
        self.rightTweenPosition:SetOnFinished(DelegateFactory.Callback(function()
            end))
    else
        self:SetFromTo(1, 0, 0, -240, 0, 240)
        self.rightTweenPosition:SetOnFinished(DelegateFactory.Callback(function()
            self:UpdateTip()
            self:SetWndActive(3)
        end))
    end

    self.leftTweenAlpha:ResetToBeginning()
    self.leftTweenPosition:ResetToBeginning()
    self.leftTweenAlpha:PlayForward()
    self.leftTweenPosition:PlayForward()

    self.rightTweenAlpha:ResetToBeginning()
    self.rightTweenPosition:ResetToBeginning()
    self.rightTweenAlpha:PlayForward()
    self.rightTweenPosition:PlayForward()
end

-- 设置from和to的值
function LuaXinShengHuaJiDrawWnd:SetFromTo(alphaFrom, apphaTo, lPositionFrom, lPositionTo, rPositionFrom, rPositionTo)
    self.leftTweenAlpha.from = alphaFrom
    self.leftTweenAlpha.to = apphaTo
    self.rightTweenAlpha.from = alphaFrom
    self.rightTweenAlpha.to = apphaTo

    self.leftTweenPosition.from = Vector3(lPositionFrom, self.leftTweenPosition.from.y, self.leftTweenPosition.from.z)
    self.leftTweenPosition.to = Vector3(lPositionTo, self.leftTweenPosition.to.y, self.leftTweenPosition.to.z)
    self.rightTweenPosition.from = Vector3(rPositionFrom, self.rightTweenPosition.from.y, self.rightTweenPosition.from.z)
    self.rightTweenPosition.to = Vector3(rPositionTo, self.rightTweenPosition.to.y, self.rightTweenPosition.to.z)
end

-- 初始化选择的UI
function LuaXinShengHuaJiDrawWnd:InitSelectedUI()
    self.selectedUIId = {1, 1, 0, 1}
    self.selectedUITbl = {}
    for i = 1, 4 do
        self.selectedUITbl[i] = {}
    end
end

-- 设置当前显示哪一个界面
function LuaXinShengHuaJiDrawWnd:SetWndActive(id)
    for i = 1, #self.wnds do
        self.wnds[i]:SetActive(false)
    end
    if self.wnds[id] ~= nil then
        self.wnds[id]:SetActive(true)
    end
end

-- 添加选择改变项
function LuaXinShengHuaJiDrawWnd:AddSelectedUI(item, level)
    local go = item:Find("Selected").gameObject
    go:SetActive(false)
    table.insert(self.selectedUITbl[level], go)
end

-- 设置选择状态
function LuaXinShengHuaJiDrawWnd:SetSelectedUIActive(level, id, isActive)
    if #self.selectedUITbl[level] < id then
        return
    end
    if isActive then
        self.selectedUIId[level] = id
    else
        self.selectedUIId[level] = 0
    end

    local item = self.selectedUITbl[level][id]
    if item == nil then return end
    item:SetActive(isActive)

    if level == 3 then
        local arrow = item.transform.parent:Find("Sprite"):GetComponent(typeof(UISprite))
        if isActive then
            arrow.flip = UIBasicSprite.Flip.Horizontally
        else
            arrow.flip = UIBasicSprite.Flip.Vertically
        end
    end
end

-- 更新各个分类
function LuaXinShengHuaJiDrawWnd:UpdateLevel(level)
    if level == nil or level > 4 then return end
    self:SetSelectedUIActive(level, self.selectedUIId[level], false)

    local subItemTbl = self:GetSubItemTbl(level)
    local itemNum
    if level < 4 then
        itemNum = #subItemTbl
    else
        self.curChoiceInfo = self:GetChoiceInfo(self.selectedUIId[1], self.selectedUIId[2], self.selectedUIId[3])
        itemNum = #self.curChoiceInfo
    end
    self:UpdateGrid(level, itemNum)

    if itemNum > 0 then
        local id = 1
        if level == 4 then
            local curChoice = self.curChoiceId[self.selectedUIId[1]][self.selectedUIId[2]][self.selectedUIId[3]]
            for i = 1, #self.curChoiceInfo do
                if self.curChoiceInfo[i].choice == curChoice then
                    id = i
                    break
                end
            end
        end
        self:SetSelectedUIActive(level, id, true)
    end

    if level < 4 then
        self:UpdateLevel(level + 1)
    else
        self:RepositionLevel34(true)
    end
end

function LuaXinShengHuaJiDrawWnd:UpdateGrid(level, itemNum)
    local template = self.templates[level]
    local grid = self.grids[level]
    local gridGo = grid.gameObject
    local gridTrans = grid.transform

    local childList = {}
    for i = 1, gridTrans.childCount do
        local child = gridTrans:GetChild(i - 1)
        if child:GetComponent(typeof(UIGrid)) == nil then
            table.insert(childList, child)
        end
    end

    local childCount = #childList
    for i = 1, itemNum do
        local item = nil
        if i <= childCount then
            item = childList[i]
        else
            item = CUICommonDef.AddChild(gridGo, template).transform
            self:AddSelectedUI(item, level)
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnLevelClick(level, i)
            end)
        end
        self:UpdateItem(level, i, item)
    end

    -- 不用的子项设置为false
    for i = itemNum + 1, childCount do
        childList[i].gameObject:SetActive(false)
    end

    grid:Reposition()
end

-- 更新一个item
function LuaXinShengHuaJiDrawWnd:UpdateItem(level, id, item)
    local subItemTbl = self:GetSubItemTbl(level)
    if level == 4 then
        local choiceId = self.curChoiceInfo[id].choice

        local icons = item:Find("Icons")
        local none = item:Find("None")
        icons.gameObject:SetActive(false)
        none.gameObject:SetActive(false)
        item:Find("Locked").gameObject:SetActive(self.curChoiceInfo[id].unlock <= 0)
        item:Find("UnlockFx"):GetComponent(typeof(CUIFx)):DestroyFx()

        if subItemTbl.isColor then
            self:SetColorIcon(icons, subItemTbl, choiceId, id)
            icons.gameObject:SetActive(true)
        else
            if choiceId < 0 then
                none.gameObject:SetActive(true)
            else
                self:SetIcons(icons, choiceId, subItemTbl)
                icons.gameObject:SetActive(true)
            end
        end
    else
        local label = item:Find("Label")
        local name = subItemTbl[id].name
        label:GetComponent(typeof(UILabel)).text = name
    end
    item.gameObject:SetActive(true)
end

-- 设置颜色图标
function LuaXinShengHuaJiDrawWnd:SetColorIcon(icons, subItemTbl, choiceId, id)
    local typeColorTbl = self:ParseTypeColor(WuMenHuaShi_XinShengHuaJiSetting.GetData().DrawColorIcons)
    local type = subItemTbl.type
    local colorData = WuMenHuaShi_ItemColor.GetData(choiceId)
    Extensions.RemoveAllChildren(icons)

    if typeColorTbl[type] then
        local child = CUICommonDef.AddChild(icons.gameObject, self.Part)
        child:SetActive(true)
        local pos = Vector3.zero
        local size = Vector3(96, 96, 1)
        local basicSprite = LuaWuMenHuaShiMgr:SetMatPosAndSize(child.transform, 0, typeColorTbl[type], pos, size)

        local cuiWidgetHSV = basicSprite.transform:GetComponent(typeof(CUIWidgetHSV))
        cuiWidgetHSV:SetHSVOffset(colorData.Hue, colorData.Saturation, colorData.Brightness)
        return
    end

    if self.selectedUIId[3] ~= 2 then return end
    local partChoiceId = self.curChoiceId[self.selectedUIId[1]][self.selectedUIId[2]][1]
    local partSubTbl = self.classfiedItemTbl[self.selectedUIId[1]][self.selectedUIId[2]][1]
    local basicSprites = self:SetIcons(icons, partChoiceId, partSubTbl)

    for i, depth in pairs(partSubTbl.depth) do
        basicSprites[i].depth = depth + LuaWuMenHuaShiMgr.depthPlus

        local colorType = partSubTbl.affectedByColorType[i]
        if colorType and colorType == type then
            local cuiWidgetHSV = basicSprites[i].transform:GetComponent(typeof(CUIWidgetHSV))
            cuiWidgetHSV:SetHSVOffset(colorData.Hue, colorData.Saturation, colorData.Brightness)
        end
    end
end

-- 解析类型颜色表
function LuaXinShengHuaJiDrawWnd:ParseTypeColor(str)
    if str == nil or str == "" then return nil end
    local tbl = {}

    for type, color in string.gmatch(str, "(%d+),([%w_]+)") do
        tbl[tonumber(type)] = tostring(color)
    end
    return tbl
end

-- 设置图标
function LuaXinShengHuaJiDrawWnd:SetIcons(icons, choiceId, subTbl)
    Extensions.RemoveAllChildren(icons)

    -- 获取图像、位置和尺寸信息
    local drawItem = WuMenHuaShi_XinShengHuaJiDrawItem.GetData(choiceId)
    local pic = LuaWuMenHuaShiMgr:Array2Tbl(drawItem.Picture)
    local pos = LuaWuMenHuaShiMgr:ParseXY(drawItem.Position)
    local size = LuaWuMenHuaShiMgr:ParseXY(drawItem.Size)
    local texOrSprite = drawItem.TextureOrSprite

    -- 计算最小位置和最大位置
    local minPos = CPos(10000, 10000)
    local maxPos = CPos(0, 0)
    for i = 1, #subTbl.depth do
        if pos[i].x < minPos.x then minPos.x = pos[i].x end
        if pos[i].y < minPos.y then minPos.y = pos[i].y end

        if pos[i].x + size[i].x > maxPos.x then maxPos.x = pos[i].x + size[i].x end
        if pos[i].y + size[i].y > maxPos.y then maxPos.y = pos[i].y + size[i].y end
    end

    -- 计算缩放尺度
    local xFrom = maxPos.x - minPos.x
    local yFrom = maxPos.y - minPos.y
    local scale = (xFrom > 0 and yFrom > 0) and math.min(96 / xFrom, 96 / yFrom) or 0
    if scale > 1 then scale = 1 end

    local newMaxPos = CPos((maxPos.x - minPos.x) * scale, (maxPos.y - minPos.y) * scale)
    local offset = CPos((96 - newMaxPos.x) / 2, (96 - newMaxPos.y) / 2)

    -- 重新计算位置和尺寸
    local newPos = {}
    local newSize = {}
    for i = 1, #subTbl.depth do
        newPos[i] = CPos((pos[i].x - minPos.x) * scale + offset.x, (pos[i].y - minPos.y) * scale + offset.y)
        newSize[i] = CPos(size[i].x * scale, size[i].y * scale)
    end

    local basicSprites = {}
    for i = 1, #subTbl.depth do
        local child = CUICommonDef.AddChild(icons.gameObject, self.Part)
        local basicSprite = LuaWuMenHuaShiMgr:SetMatPosAndSize(child.transform, texOrSprite, pic[i], newPos[i], newSize[i])
        basicSprite.transform:GetComponent(typeof(CUIWidgetHSV)):SetHSVOffset(0, 0, 0)
        basicSprite.depth = subTbl.depth[i] + LuaWuMenHuaShiMgr.depthPlus
        child:SetActive(true)
        table.insert(basicSprites, basicSprite)
    end

    return basicSprites
end

-- 根据层级不同返回不同数据
function LuaXinShengHuaJiDrawWnd:GetSubItemTbl(level)
    if level == 1 then
        return self.classfiedItemTbl
    elseif level == 2 then
        return self.classfiedItemTbl[self.selectedUIId[1]]
    elseif level == 3 then
        return self.classfiedItemTbl[self.selectedUIId[1]][self.selectedUIId[2]]
    elseif level == 4 then
        return self.classfiedItemTbl[self.selectedUIId[1]][self.selectedUIId[2]][self.selectedUIId[3]]
    end
end

-- 更改位置
function LuaXinShengHuaJiDrawWnd:RepositionLevel34(active)
    if active then
        self.grids[4].transform:SetSiblingIndex(self.selectedUIId[3])
    end
    self.grids[4].gameObject:SetActive(active)
    self.grids[3]:Reposition()
    self.scrollView34:ResetPosition()
end

-- 改变选择的状态
function LuaXinShengHuaJiDrawWnd:ChangeSelectedUIState(level, id)
    self:SetSelectedUIActive(level, self.selectedUIId[level], false)
    self:SetSelectedUIActive(level, id, true)
end

-- 限制移动区域
function LuaXinShengHuaJiDrawWnd:LimitMoveRegion(pos, xMin, xMax, yMin, yMax)
    if pos.x < xMin then pos.x = xMin end
    if pos.x > xMax then pos.x = xMax end
    if pos.y < yMin then pos.y = yMin end
    if pos.y > yMax then pos.y = yMax end
end

-- 人像显现
function LuaXinShengHuaJiDrawWnd:PortraitAppear(needRandom, func)
    self.appearFx:DestroyFx()
    self.portraitTweenAlpha:ResetToBeginning()
    if needRandom then
        self:RandomChoice()
        self:UpdateLevel(4)
    end

    self.appearFx:LoadFx(g_UIFxPaths.XinShengHuaJiMoYunFx)
    self.portraitTweenAlpha:PlayForward()
    self.portraitTweenAlpha:SetOnFinished(func)
end

-- 解锁确认窗口
function LuaXinShengHuaJiDrawWnd:UnlockConfirm(id)
    local choice = self.curChoiceInfo[id].choice
    local num = self.curChoiceInfo[id].exchangeNum

    local itemId = WuMenHuaShi_XinShengHuaJiSetting.GetData().UnlockItemId
    local msg = g_MessageMgr:FormatMessage("XinShengHuaJi_UnLock_Confirm", num)
    LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(itemId, num, msg, false, false,
        LocalString.GetString("解锁"), function(enough)
            if enough then
                Gac2Gas.XinShengHuaJi_RequestUnlock(choice)
                LuaItemInfoMgr:CloseItemConsumeWnd()
            end
        end, nil, true)
end

-- 根据鼠标的位置更新画笔的位置
function LuaXinShengHuaJiDrawWnd:UpdateBrushPosition()
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
    local localPos = self.brush.transform.parent:InverseTransformPoint(worldPos)

    local widget = self.dragRegion.transform:GetComponent(typeof(UIWidget))
    local widgetPos = widget.transform.localPosition

    local xMin = -widget.width / 2 + widgetPos.x
    local xMax = widget.width / 2 + widgetPos.x
    local yMin = -widget.height / 2 + widgetPos.y
    local yMax = widget.height / 2 + widgetPos.y

    self:LimitMoveRegion(localPos, xMin, xMax, yMin, yMax)
    self.brush.localPosition = localPos
end

-- 局部坐标转为屏幕坐标
function LuaXinShengHuaJiDrawWnd:Local2Screen(localPos, scale)
	local worldPos = self.transform:TransformPoint(localPos)
	local screenPos = UICamera.currentCamera:WorldToScreenPoint(worldPos)
	return Vector2(screenPos.x * scale, screenPos.y * scale)
end

--@region UIEvent
-- 不同类别下的点击响应
function LuaXinShengHuaJiDrawWnd:OnLevelClick(level, id)
    if id == self.selectedUIId[level] then
        if level == 3 then
            self:SetSelectedUIActive(3, self.selectedUIId[3], false)
            self:RepositionLevel34(false)
        end
    else
        if level < 4 then
            self:ChangeSelectedUIState(level, id)
            self:UpdateLevel(level + 1)
        else
            if self.curChoiceInfo[id].unlock == 0 then
                self:UnlockConfirm(id)
                return
            end
            self:ChangeSelectedUIState(level, id)

            self:ChangePart(self.selectedUIId[1], self.selectedUIId[2], self.selectedUIId[3], self.curChoiceInfo[id].choice)

            if self.selectedUIId[3] == 1 then
                local randomColorChoiceId = self:RandomOneChoice(self.selectedUIId[1], self.selectedUIId[2], 2)
                self:ChangePart(self.selectedUIId[1], self.selectedUIId[2], 2, randomColorChoiceId)
            end
            self.isModified = true
        end
    end
end

function LuaXinShengHuaJiDrawWnd:OnDrag(go, delta)
    self:UpdateBrushPosition()
    local scale = UIRoot.GetPixelSizeAdjustment(go)

    local threshold = 2000
    if self.dragDistance > threshold then return end
    self.dragDistance = self.dragDistance + math.sqrt(delta.x ^ 2 + delta.y ^ 2) * scale
    if self.dragDistance > threshold then
        self:PortraitAppear(false, DelegateFactory.Callback(function()
            self:EnterDrawWnd(true)

            if CTaskMgr.Inst:IsInProgress(WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiGuideTaskId) then
                g_MessageMgr:ShowMessage("WUMENHUASHI_CREATE_NOTICE")
            end
            if LuaWuMenHuaShiMgr.isDerivativePlot and LuaWuMenHuaShiMgr.taskId == 22111647 then
                CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.XinShengHuaJiDrawGuide)
            end
        end))
    end
end

function LuaXinShengHuaJiDrawWnd:OnRandomButtonClick()
    if self.isModified then
        local message = g_MessageMgr:FormatMessage("XINSHENGHUAJI_RANDOM_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
            self:PortraitAppear(true, nil)
        end), nil, nil, nil, false)
    else
        self:PortraitAppear(true, nil)
    end
end

function LuaXinShengHuaJiDrawWnd:OnFinishButtonClick()
    if LuaWuMenHuaShiMgr.isDerivativePlot then
        if LuaWuMenHuaShiMgr.taskId == WuMenHuaShi_XinShengHuaJiSetting.GetData().NeedSavePictureTaskId then
            LuaWuMenHuaShiMgr:UploadPic(self.classfiedItemTbl, self.curChoiceId)
        else
            self.isDrawingFinished = true
            self:PlayConfirmTaskDialog()
        end
    elseif LuaWuMenHuaShiMgr.isHYGSDraw then
        LuaWuMenHuaShiMgr.newHYGSPicData = LuaWuMenHuaShiMgr:GetDrawnPicData(self.classfiedItemTbl, self.curChoiceId, self.flip)
        CUIManager.ShowUI(CLuaUIResources.HuaYiGongShangConfirmWnd)
    else
        self:EnterFinishWnd()
    end
end

function LuaXinShengHuaJiDrawWnd:OnShareButtonClick()
    self.wnds[3]:SetActive(false)
    self.CloseButton.gameObject:SetActive(false)
    self.appearFx:DestroyFx()

    CUICommonDef.CaptureScreen("screenshot", false, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            self.wnds[3]:SetActive(true)
            self.CloseButton.gameObject:SetActive(true)
            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
        end), false)
end

function LuaXinShengHuaJiDrawWnd:OnSubmitButtonClick()
    LuaWuMenHuaShiMgr:UploadPic(self.classfiedItemTbl, self.curChoiceId, self.flip)
end

function LuaXinShengHuaJiDrawWnd:OnReturnButtonClick()
    self:EnterDrawWnd(false)
end

function LuaXinShengHuaJiDrawWnd:OnCloseButtonClick()
    if LuaWuMenHuaShiMgr.isDerivativePlot and self.isDrawingFinished then
        LuaWuMenHuaShiMgr:CloseDerivativePlotWnd()
        return
    end

    if self.isModified then
        local message = g_MessageMgr:FormatMessage("XINSHENGHUAJI_CLOSE_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
            LuaWuMenHuaShiMgr:CloseXinShengHuaJiDrawWnd()
        end), nil, nil, nil, false)
    else
        LuaWuMenHuaShiMgr:CloseXinShengHuaJiDrawWnd()
    end
end

function LuaXinShengHuaJiDrawWnd:OnFlipButtonClick()
    self:UpdateFlip(1 - self.flip)
end

function LuaXinShengHuaJiDrawWnd:OnAvatarButtonClick()
    self.appearFx:DestroyFx()

    local scale = CUICommonDef.m_CaptureWidth / Screen.width
    local bIsWinSocialWndOpened = false
    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
    end
    if bIsWinSocialWndOpened then
        scale = scale / (1 - Constants.WinSocialWndRatio)
    end

    -- 计算需要截图的左下角和右上角的屏幕坐标
    local leftDownPos = self:Local2Screen(Vector3(-440, -390, 0), scale)
    local rightTopPos = self:Local2Screen(Vector3(340, 390, 0), scale)

    CUICommonDef.CapturePartOfScreen("screenshot", true, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            LuaWuMenHuaShiMgr.avatarFilePath = fullPath
            CUIManager.ShowUI(CLuaUIResources.AvatarReplaceWnd)
        end), leftDownPos, rightTopPos.x - leftDownPos.x, rightTopPos.y - leftDownPos.y, false)
end

function LuaXinShengHuaJiDrawWnd:OnGenderClick(newSex)
    if LuaWuMenHuaShiMgr.sexId == newSex then return end

    if self.isModified then
        local message = g_MessageMgr:FormatMessage("XINSHENGHUAJI_CHANGE_SEX_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
            self:ChangeSex(newSex)
        end), nil, nil, nil, false)
    else
        self:ChangeSex(newSex)
    end
end

--@endregion UIEvent


------------------ 衍生剧情 -------------------------

-- 衍生剧情界面的active初始化
function LuaXinShengHuaJiDrawWnd:InitDerivativePlotActive()
    self.ShareButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    if not LuaWuMenHuaShiMgr.isDerivativePlot then return end

    self.SubmitButton.gameObject:SetActive(false)
    self.ReturnButton.gameObject:SetActive(false)
    self.tipLabel.gameObject:SetActive(false)
    self.ShareButton.gameObject:SetActive(false)
    self.flipButton:SetActive(false)
    self.avatarButton:SetActive(false)

    local pos = self.ShareButton.transform.localPosition
    pos.x = pos.x + 200
    self.ShareButton.transform.localPosition = pos
end

-- 解析成choice
function LuaXinShengHuaJiDrawWnd:Parse2Choice(tbl)
    local choiceTbl = {}
    if not tbl then return choiceTbl end
    for i = 1, #tbl do
        local type = tbl[i].x
        local choice = tbl[i].y

        if choiceTbl[type] == nil then
            choiceTbl[type] = {}
        end
        table.insert(choiceTbl[type], choice)
    end
    return choiceTbl
end

-- 衍生剧情需要复制并修改表
function LuaXinShengHuaJiDrawWnd:InitClassfiedItemTbl()
    local itemTbl = LuaWuMenHuaShiMgr.classfiedItemTbl[LuaWuMenHuaShiMgr.sexId]
    if not LuaWuMenHuaShiMgr.isDerivativePlot then
        self.classfiedItemTbl = itemTbl
        return
    end

    local taskId = LuaWuMenHuaShiMgr.taskId
    local data = WuMenHuaShi_DerivativePlot.GetData(taskId)
    local availableChoiceTbl = self:Parse2Choice(LuaWuMenHuaShiMgr:ParseXY(data.AvailableChoice))
    local unSelectableChoiceTbl = self:Parse2Choice(LuaWuMenHuaShiMgr:ParseXY(data.UnSelectableChoice))
    local unSelectableChoiceDict = {}
    for type, choiceTbl in pairs(unSelectableChoiceTbl) do
        unSelectableChoiceDict[type] = {}
        for _, choiceId in pairs(choiceTbl) do
            unSelectableChoiceDict[type][choiceId] = true
        end
    end
    if LuaWuMenHuaShiMgr.isTaskPicDataShow and LuaWuMenHuaShiMgr.taskPicData then
        local sexType = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiSexType
        for type, choice in pairs(LuaWuMenHuaShiMgr.taskPicData) do
            if type > sexType then
                availableChoiceTbl[type] = {choice}
            end
        end
    end

    self.fixedChoice = {}
    local newItemTbl = {}
    for i1 = 1, #itemTbl do
        newItemTbl[i1] = {}
        for i2 = 1, #itemTbl[i1] do
            local itemSubTbl = itemTbl[i1][i2]
            local newItemSubTbl = {}

            local fixed = true
            if itemSubTbl[0] then
                newItemSubTbl[0] = self:GetNewItemSubTblInDP(itemSubTbl[0], availableChoiceTbl, unSelectableChoiceDict)
                local choiceTbl = newItemSubTbl[0].choice
                fixed = #choiceTbl <= 1
                if #choiceTbl == 1 and choiceTbl[1] > 0 then
                    table.insert(self.fixedChoice, {choiceId = choiceTbl[1], subTbl = itemSubTbl[0]})
                end
            else
                for i3 = 1, #itemSubTbl do
                    newItemSubTbl[i3] = self:GetNewItemSubTblInDP(itemSubTbl[i3], availableChoiceTbl, unSelectableChoiceDict)
                    fixed = fixed and (#newItemSubTbl[i3].choice <= 1)
                end
                if fixed then
                    for i3 = 1, #itemSubTbl do
                        local choiceTbl = newItemSubTbl[i3].choice
                        if #choiceTbl == 1 and choiceTbl[1] > 0 then
                            table.insert(self.fixedChoice, {choiceId = choiceTbl[1], subTbl = itemSubTbl[i3]})
                        end
                    end
                end
            end
            if not fixed then
                newItemSubTbl.name = itemSubTbl.name
                table.insert(newItemTbl[i1], newItemSubTbl)
            end
        end
        newItemTbl[i1].name = itemTbl[i1].name
    end

    self.classfiedItemTbl = newItemTbl
end

function LuaXinShengHuaJiDrawWnd:GetNewItemSubTblInDP(itemSubTbl, availableChoiceTbl, unSelectableChoiceDict)
    local type = itemSubTbl.type
    if not availableChoiceTbl[type] and not unSelectableChoiceDict[type] then
        return itemSubTbl
    end

    local newItemSubTbl = {}
    newItemSubTbl.type = type
    newItemSubTbl.name = itemSubTbl.name
    newItemSubTbl.depth = itemSubTbl.depth
    newItemSubTbl.isColor = itemSubTbl.isColor
    newItemSubTbl.affectedByColorType = itemSubTbl.affectedByColorType

    if availableChoiceTbl[type] then
        newItemSubTbl.choice = availableChoiceTbl[type]
        newItemSubTbl.isAvailable = true
    elseif unSelectableChoiceDict[type] then
        newItemSubTbl.choice = {}
        for _, choiceId in pairs(itemSubTbl.choice) do
            if not unSelectableChoiceDict[type][choiceId] then
                table.insert(newItemSubTbl.choice, choiceId)
            end
        end
    end
    return newItemSubTbl
end

-- 初始化固定的部件
function LuaXinShengHuaJiDrawWnd:InitFixedPart()
    for _, data in pairs(self.fixedChoice) do
        if data.choiceId > 0 then
            local subTbl = data.subTbl
            if not subTbl.isColor then
                local trans = self:GenerateOnePart(data.subTbl)
                self:ChangeStyle(data.choiceId, trans, subTbl.type)
            end
        end
    end

    for _, data in pairs(self.fixedChoice) do
        if data.choiceId > 0 then
            local subTbl = data.subTbl
            if subTbl.isColor then
                self:ChangeColor(data.choiceId, subTbl.type)
            end
        end
    end
end

-- 点击确认播放dialog
function LuaXinShengHuaJiDrawWnd:PlayConfirmTaskDialog()
    self:EnterFinishWnd()
    local taskId = LuaWuMenHuaShiMgr.taskId
    local taskDialogId = WuMenHuaShi_DerivativePlot.GetData(taskId).ConfrimTaskDialogId
    if taskDialogId == 0 then
        self.ShareButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
        return
    end
    local dialogMsg = Dialog_TaskDialog.GetData(taskDialogId).Dialog
    CConversationMgr.Inst:OpenStoryDialog(dialogMsg, DelegateFactory.Action(function()
        self.CloseButton.gameObject:SetActive(true)
        self.ShareButton.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    end))
end

-- 开始倒计时
function LuaXinShengHuaJiDrawWnd:StartPictureShowTick()
    self:ClearTick()
    self.tick = RegisterTickOnce(function()
        LuaWuMenHuaShiMgr:CloseDerivativePlotWnd()
    end, 1000 * LuaWuMenHuaShiMgr.showTime)
end

-- 清除计时器
function LuaXinShengHuaJiDrawWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
        self.tick = nil
    end
end

-- 关闭界面清除计时器和新手引导
function LuaXinShengHuaJiDrawWnd:OnDestroy()
    self:ClearTick()

    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.XinShengHuaJiDrawGuide) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
end

-- 新手引导
function LuaXinShengHuaJiDrawWnd:GetGuideGo(methodName)
    local child = nil
    if methodName == "GetClothesButton" then
        child = self.leftTbl:GetChild(2)
    elseif methodName == "GetCoatButton" then
        child = self.grids[2].transform:GetChild(1)
    elseif methodName == "GetStyleButton" then
        child = self.grids[4].transform:GetChild(1)
    elseif methodName == "GetColorButton" then
        child = self.grids[3].transform:GetChild(2)
    end
    return child and child.gameObject or nil
end

-- 用于策划导出捏脸数据
function LuaXinShengHuaJiDrawWnd:OnCopyPictureData()
    local choiceIds = self.curChoiceId
    local tbl = {}

    for i1 = 1, #choiceIds do
        for i2 = 1, #choiceIds[i1] do
            local subChoice = choiceIds[i1][i2]
            local subTbl = self.classfiedItemTbl[i1][i2]
            if subTbl[0] ~= nil then
                if subChoice[0] >= 0 then
                    local type = subTbl[0].type
                    table.insert(tbl, SafeStringFormat3("%d,%d;", type, subChoice[0]))
                end
            else
                for i3 = 1, #subChoice do
                    if subChoice[i3] and subChoice[i3] >= 0 then
                        local type = subTbl[i3].type
                        table.insert(tbl, SafeStringFormat3("%d,%d;", type, subChoice[i3]))
                    end
                end
            end
        end
    end

    local str = table.concat(tbl)
    Clipboard.SetClipboradText(str)
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("捏脸数据已复制到剪贴板"))
end


---------------------- 画意共赏 -------------------
-- 一些特殊处理
function LuaXinShengHuaJiDrawWnd:InitSpecial()
    if LuaWuMenHuaShiMgr.isDerivativePlot then
        self:InitFixedPart()
    end

    if LuaWuMenHuaShiMgr.isHYGSDraw and LuaWuMenHuaShiMgr.oldHYGSPicData then
        self:InitHYGSPortrait()
    elseif not LuaWuMenHuaShiMgr.isPictureShow then
        self:RandomChoice()
    end

    self.isDrawingFinished = LuaWuMenHuaShiMgr.isPictureShow

    if LuaWuMenHuaShiMgr.isPictureShow then
        self:SetWndActive(0)
        self:StartPictureShowTick()
    elseif LuaWuMenHuaShiMgr.isHYGSDraw then
        self:EnterDrawWnd(true)
    else
        self.dragDistance = 0
        self:InitBrushWnd()
    end

    self.maleButton.gameObject:SetActive(not LuaWuMenHuaShiMgr.isDerivativePlot)
    self.femaleButton.gameObject:SetActive(not LuaWuMenHuaShiMgr.isDerivativePlot)
    self.themeName.gameObject:SetActive(LuaWuMenHuaShiMgr.isHYGSDraw)
    if LuaWuMenHuaShiMgr.isHYGSDraw then
        self.themeName.text = LuaWuMenHuaShiMgr:GetThemeName()
    end
    self:SetGenderButtonState()
end

-- 初始化画意共享画像
function LuaXinShengHuaJiDrawWnd:InitHYGSPortrait()
    local dataTbl = self.classfiedItemTbl
    local setting = WuMenHuaShi_XinShengHuaJiSetting.GetData()

    for i1 = 1, #dataTbl do
        for i2 = 1, #dataTbl[i1] do
            if dataTbl[i1][i2][0] ~= nil then
                local choiceId = self:GetHYGSChoice(i1, i2, 0)
                self:ChangePart(i1, i2, 0, choiceId)
            else
                for i3 = 1, #dataTbl[i1][i2] do
                    local choiceId = self:GetHYGSChoice(i1, i2, i3)
                    self:ChangePart(i1, i2, i3, choiceId)
                end
            end
        end
    end

    self:UpdateFlip(LuaWuMenHuaShiMgr.oldHYGSPicData[setting.XinShengHuaJiFlipType])
    self.isModified = false
end

-- 获取画意共享选项
function LuaXinShengHuaJiDrawWnd:GetHYGSChoice(i1, i2, i3)
    local type = self.classfiedItemTbl[i1][i2][i3].type
    if not LuaWuMenHuaShiMgr.oldHYGSPicData[type] then return -1 end

    return LuaWuMenHuaShiMgr.oldHYGSPicData[type]
end

-- 设置性别按钮的选中状态
function LuaXinShengHuaJiDrawWnd:SetGenderButtonState()
    self.maleButton:Find("Selected").gameObject:SetActive(LuaWuMenHuaShiMgr.sexId == 0)
    self.femaleButton:Find("Selected").gameObject:SetActive(LuaWuMenHuaShiMgr.sexId == 1)
end

-- 切换性别
function LuaXinShengHuaJiDrawWnd:ChangeSex(newSex)
    LuaWuMenHuaShiMgr.sexId = newSex
    self:SetGenderButtonState()

    self:InitClassfiedItemTbl()

    Extensions.RemoveAllChildren(self.Portrait.transform)
    self:InitPortrait()
    self:RandomChoice()
    self:UpdateLevel(2)
    self:PortraitAppear(false, nil)
end

