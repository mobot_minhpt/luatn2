local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaCuJuSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCuJuSelectWnd, "table")
RegistClassMember(LuaCuJuSelectWnd, "okButton")
RegistClassMember(LuaCuJuSelectWnd, "closeTip")
RegistClassMember(LuaCuJuSelectWnd, "bottomTip")

RegistClassMember(LuaCuJuSelectWnd, "tick")
RegistClassMember(LuaCuJuSelectWnd, "curSelect")

function LuaCuJuSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self.okButton:SetActive(true)
    self.bottomTip.text = ""

    UIEventListener.Get(self.okButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOKButtonClick()
    end)

    UIEventListener.Get(self.transform:Find("WorldCup2022SelectCommon/CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCloseButtonClick()
    end)
end

function LuaCuJuSelectWnd:InitUIComponents()
    self.table = self.transform:Find("WorldCup2022SelectCommon/Table")
    self.okButton = self.transform:Find("OKButton").gameObject
    self.closeTip = self.transform:Find("CloseTip"):GetComponent(typeof(UILabel))
    self.bottomTip = self.transform:Find("BottomTip"):GetComponent(typeof(UILabel))
end

function LuaCuJuSelectWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateCuJuPlayerPlace", self, "OnUpdatePlayerPlace")
    g_ScriptEvent:AddListener("SetCuJuPlayerPlaceSuccess", self, "OnSetPlayerPlaceSuccess")
end

function LuaCuJuSelectWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateCuJuPlayerPlace", self, "OnUpdatePlayerPlace")
    g_ScriptEvent:RemoveListener("SetCuJuPlayerPlaceSuccess", self, "OnSetPlayerPlaceSuccess")
end

function LuaCuJuSelectWnd:OnUpdatePlayerPlace()
    self:UpdatePos()
end

function LuaCuJuSelectWnd:OnSetPlayerPlaceSuccess()
    self:UpdatePos()
    self:UpdateTime()
end


function LuaCuJuSelectWnd:Init()
    self:InitTable()
    self:UpdatePos()

    self:ClearTick()
    self:UpdateTime()
    self.tick = RegisterTick(function ()
        self:UpdateTime()
    end, 1000)
end

function LuaCuJuSelectWnd:InitTable()
    for i = 1, 5 do
        local child = self.table:GetChild(i - 1)

        child:Find("Place"):GetComponent(typeof(UILabel)).text = CuJu_Place.GetData(i).Name
        child:Find("Highlight").gameObject:SetActive(false)
        child:Find("Select").gameObject:SetActive(false)

        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnPlaceClick(i)
        end)
    end
end

-- 更新位置信息
function LuaCuJuSelectWnd:UpdatePos()
    local posInfo = LuaCuJuMgr.Info.posInfo or {}
    local myPos = LuaCuJuMgr.Info.myPos

    for i = 1, 5 do
        local child = self.table:GetChild(i - 1)
        local name = child:Find("Name"):GetComponent(typeof(UILabel))
        local outline = child:Find("Outline").gameObject
        local portrait = child:GetComponent(typeof(CUITexture))
        local select = child:Find("Select").gameObject

        name.color = NGUIText.ParseColor24("FFFFFF", 0)
        if posInfo[i] then
            name.text = posInfo[i][1]
            name.alpha = 1
            if myPos and myPos == i then
                name.color = NGUIText.ParseColor24("00FF60", 0)
                select:SetActive(true)
            else
                select:SetActive(false)
            end
            outline.gameObject:SetActive(false)
            portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(posInfo[i][2], posInfo[i][3], -1), false)
        else
            select:SetActive(false)
            name.text = LocalString.GetString("待就位")
            name.alpha = 0.6
            outline.gameObject:SetActive(true)
            portrait:LoadMaterial("")
        end
    end
    self:UpdateBottom()
end

-- 更新底部
function LuaCuJuSelectWnd:UpdateBottom()
    local myPos = LuaCuJuMgr.Info.myPos
    if myPos then
        self.okButton:SetActive(false)
        self.bottomTip.text = LocalString.GetString("已就位，等待其他玩家就位")
        self.bottomTip.color = NGUIText.ParseColor24("00FF60", 0)
        return
    end

    local posInfo = LuaCuJuMgr.Info.posInfo or {}
    if self.curSelect and posInfo[self.curSelect] then
        self.okButton:SetActive(false)
        self.bottomTip.text = LocalString.GetString("该位置已有其他玩家")
        self.bottomTip.color = NGUIText.ParseColor24("FF5050", 0)
    else
        self.okButton:SetActive(true)
        self.bottomTip.text = ""
    end
end

function LuaCuJuSelectWnd:UpdateTime()
    local leftTime = math.floor(LuaCuJuMgr.Info.selectEndTimeStamp - CServerTimeMgr.Inst.timeStamp)

    if leftTime <= 0 then
        CUIManager.CloseUI(CLuaUIResources.CuJuSelectWnd)
        return
    end

    if LuaCuJuMgr.Info.myPos then
        self.closeTip.text = SafeStringFormat3(LocalString.GetString("%d秒后关闭"), leftTime)
    else
        self.closeTip.text = SafeStringFormat3(LocalString.GetString("%d秒后自动选择"), leftTime)
    end
end

function LuaCuJuSelectWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
    end
end

function LuaCuJuSelectWnd:OnDestroy()
    self:ClearTick()
end

--@region UIEvent

function LuaCuJuSelectWnd:OnPlaceClick(i)
    if self.curSelect and self.curSelect == i then return end

    if self.curSelect then
        local before = self.table:GetChild(self.curSelect - 1)
        before:Find("Highlight").gameObject:SetActive(false)
    end

    self.curSelect = i
    local now = self.table:GetChild(self.curSelect - 1)
    now:Find("Highlight").gameObject:SetActive(true)
    self:UpdateBottom()
end

function LuaCuJuSelectWnd:OnOKButtonClick()
    if self.curSelect then
        Gac2Gas.RequestSetCuJuPlace(self.curSelect)
    else
        g_MessageMgr:ShowMessage("CUJU_NEED_SELECT_PLACE")
    end
end

function LuaCuJuSelectWnd:OnCloseButtonClick()
    if LuaCuJuMgr.Info.myPos then
        CUIManager.CloseUI(CLuaUIResources.CuJuSelectWnd)
        return
    end

    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CUJU_CLOSE_SELECT_WND_CONFIRM"), function()
        CUIManager.CloseUI(CLuaUIResources.CuJuSelectWnd)
    end, nil, nil, nil, false)
end

--@endregion UIEvent
