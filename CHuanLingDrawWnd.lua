-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDrawLineMgr = import "L10.Game.CDrawLineMgr"
local CHuanLingDrawWnd = import "L10.UI.CHuanLingDrawWnd"
local CHuanLingMgr = import "L10.Game.CHuanLingMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMapiLingfuPos = import "L10.Game.EnumMapiLingfuPos"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local Object1 = import "UnityEngine.Object"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_LingfuZhizuoUpgrade = import "L10.Game.ZuoQi_LingfuZhizuoUpgrade"
CHuanLingDrawWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    if CommonDefs.IS_HMT_CLIENT then
        UIEventListener.Get(this.m_ShareButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnShareToMengDaoClicked, VoidDelegate, this)
    else
        UIEventListener.Get(this.m_ShareButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnShareButtonClicked, VoidDelegate, this)
    end
    UIEventListener.Get(this.m_FinishButtion).onClick = MakeDelegateFromCSFunction(this.OnFinishButtonClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_ShareToMengDao).onClick = MakeDelegateFromCSFunction(this.OnShareToMengDaoClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_ShareToPlatform).onClick = MakeDelegateFromCSFunction(this.OnShareToPlatformClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_DrawAnotherButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnDrawAnotherButtonClicked, VoidDelegate, this)
    this.m_colorRadioBox.OnSelect = MakeDelegateFromCSFunction(this.OnSelect, MakeGenericClass(Action2, QnButton, Int32), this)
end
CHuanLingDrawWnd.m_Init_CS2LuaHook = function (this) 

    this:UpdateHuanLingExp()
    this.m_FuZhouFX:LoadFx(this.m_HuanLingFx)
    this.m_DiZuoFX:LoadFx(this.m_HuanLingDiZuoFx)
    this.m_colorRadioBox:ChangeTo(0, true)
    this.m_ShareButton.gameObject:SetActive(false)
    this.m_ShareView:SetActive(false)
    this.m_Result.gameObject:SetActive(false)

    do
        local i = 0
        while i < this.m_FiveResults.Length do
            this.m_FiveResults[i].gameObject:SetActive(false)
            i = i + 1
        end
    end

    do
        local i = 0
        while i < this.m_HuanLingColor.Length do
            local isLocked = this:IsColorLocked(i)
            this.m_HuanLingColor[i]:Init(isLocked)
            i = i + 1
        end
    end
    CDrawLineMgr.Inst.LineColor = NGUIText.ParseColor24("FFA330", 0)
    CDrawLineMgr.Inst.LineStartWidth = CHuanLingDrawWnd.LINE_START_WIDTH
    CDrawLineMgr.Inst.LineEndWidth = CHuanLingDrawWnd.LINE_END_WIDTH
    this.m_LineDrawer:Init()
    CDrawLineMgr.Inst:AllowDraw()
end
CHuanLingDrawWnd.m_OnGenerateLingguSuccess_CS2LuaHook = function (this, lingfuId, quality, pos) 

    -- Step 0 : 隐藏模型
    this.m_FuZhou:HideFuZhouModel()

    this.m_BaoKaiFx.OnLoadFxFinish = DelegateFactory.Action(function () 
        -- Step 1 : 符纸慢慢变淡
        this.m_FuZhou:ShowFuZhouTexture()
        -- Step 2 : 显示获得的东西 和 特效
        -- Step 3 : 移动位置到右下角
        this.m_Result.gameObject:SetActive(true)
        this.m_Result:Init(lingfuId, quality, pos)
        -- Step 4 : 分享按钮可用
        if CSwitchMgr.EnableHuanLingDrawShare then
            this.m_ShareButton.gameObject:SetActive(true)
            this.m_ShareButton.Enabled = true
        else
            this.m_ShareButton.gameObject:SetActive(false)
            this.m_ShareButton.Enabled = false
        end

        this.m_DrawAnotherButton.gameObject:SetActive(true)
        if CHuanLingMgr.IS_HUANLING_FIVE then
            if CHuanLingMgr.Inst:GetHuanLingNum(CHuanLingMgr.Inst.SelectedLingFuId) == CHuanLingMgr.FIVE_HUANLING then
                this.m_DrawAnotherButton.Text = LocalString.GetString("再画五张")
            else
                this.m_DrawAnotherButton.Text = LocalString.GetString("再画十张")
            end
        else
            this.m_DrawAnotherButton.Text = LocalString.GetString("再画一张")
        end
        this.m_DrawAnotherButton.Enabled = this:CanDrawAnother()
        this.m_CloseButton:SetActive(true)
    end)
    this.m_Pos = pos
    this.m_Quality = quality
    this:LoadBaoKaiFx(quality)
    this:UpdateHuanLingExp()
end
CHuanLingDrawWnd.m_OnGenerateLingguFastSuccess_CS2LuaHook = function (this, generateLingfus) 

    this.m_FuZhou:HideFuZhouModel()

    this.m_BaoKaiFx.OnLoadFxFinish = DelegateFactory.Action(function () 
        this.m_FuZhou:ShowFuZhouTexture()
        do
            local i = 0
            while i < generateLingfus.Count do
                local info = generateLingfus[i]
                this.m_FiveResults[i].gameObject:SetActive(true)
                this.m_FiveResults[i]:Init(info.itemid, info.quality, info.pos)
                i = i + 1
            end
        end
        -- Step 4 : 分享按钮可用
        if CSwitchMgr.EnableHuanLingDrawShare then
            this.m_ShareButton.gameObject:SetActive(true)
            this.m_ShareButton.Enabled = true
        else
            this.m_ShareButton.gameObject:SetActive(false)
            this.m_ShareButton.Enabled = false
        end

        this.m_DrawAnotherButton.gameObject:SetActive(true)
        if CHuanLingMgr.IS_HUANLING_FIVE then
            if CHuanLingMgr.Inst:GetHuanLingNum(CHuanLingMgr.Inst.SelectedLingFuId) == CHuanLingMgr.FIVE_HUANLING then
                this.m_DrawAnotherButton.Text = LocalString.GetString("再画五张")
            else
                this.m_DrawAnotherButton.Text = LocalString.GetString("再画十张")
            end
        else
            this.m_DrawAnotherButton.Text = LocalString.GetString("再画一张")
        end
        this.m_DrawAnotherButton.Enabled = this:CanDrawAnother()
        this.m_CloseButton:SetActive(true)
    end)

    local bestLingfu = generateLingfus[0]
    do
        local i = 0
        while i < generateLingfus.Count do
            if bestLingfu.quality < generateLingfus[i].quality then
                bestLingfu = generateLingfus[i]
            end
            i = i + 1
        end
    end
    this.m_Pos = bestLingfu.pos
    this.m_Quality = bestLingfu.quality
    this:LoadBaoKaiFx(bestLingfu.quality)
    this:UpdateHuanLingExp()
end
CHuanLingDrawWnd.m_LoadBaoKaiFx_CS2LuaHook = function (this, quality) 
    if quality < 1 or quality > 4 then
        return
    end

    local fxName = System.String.Format(this.m_HuanLingBaokaiFx, this.baokaiLevel[quality - 1])
    this.m_BaoKaiFx:LoadFx(fxName)
end
CHuanLingDrawWnd.m_UpdateHuanLingExp_CS2LuaHook = function (this) 
    local level = 1
    local slideValue = 0
    if CClientMainPlayer.Inst ~= nil then
        local info = CClientMainPlayer.Inst.PlayProp.YumaPlayData
        level = math.max(1, info.LingfuLevel)
        local exp = info.LingfuExp

        local zhiZuoUpgrade = ZuoQi_LingfuZhizuoUpgrade.GetData(level)
        if zhiZuoUpgrade ~= nil then
            slideValue = (exp * 1) / zhiZuoUpgrade.Exp
            this.m_LvExpLabel.text = System.String.Format("{0}/{1}", exp, zhiZuoUpgrade.Exp)
        end
    end
    this.m_LvLabel.text = System.String.Format("Lv. {0}", level)
    this.m_LvSlider.value = slideValue
end
CHuanLingDrawWnd.m_OnFinishButtonClicked_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < 1 then
        g_MessageMgr:ShowMessage("BAG_SPACE_NOT_ENOUGH")
    else
        CDrawLineMgr.Inst:ForbidDraw()
        --CanDraw = false;
        local lineTexture = this.m_LineDrawer:GetLineTexture()
        this.m_FuZhou:SetDrawToFuZhouTexture(lineTexture)
        this.m_FuZhou:HideFuZhouTexture()
        this.m_FuZhou:LoadFuZhou()
        this.m_LinePanel:SetActive(false)
        this.m_FinishButtion:SetActive(false)
        this.m_colorRadioBox.gameObject:SetActive(false)
        this.m_CloseButton:SetActive(false)
        CTickMgr.Register(DelegateFactory.Action(function () 
            if not CHuanLingMgr.IS_HUANLING_FIVE then
                local itemId = nil
                local bagPos = 0
                local default
                default, bagPos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, CHuanLingMgr.Inst.SelectedLingFuId)
                if default then
                    if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < 1 then
                        g_MessageMgr:ShowMessage("BAG_SPACE_NOT_ENOUGH")
                        this:Close()
                    else
                        Gac2Gas.RequestGenerateLingfuItem(itemId, CHuanLingMgr.Inst:GetLingFuQuality(CHuanLingMgr.Inst.SelectedLingFuId), bagPos)
                    end
                end
            else
                local itemId = nil
                local bagPos = 0
                local extern
                extern, bagPos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, CHuanLingMgr.Inst.SelectedLingFuId)
                if extern then
                    if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < CHuanLingMgr.Inst:GetHuanLingNum(CHuanLingMgr.Inst.SelectedLingFuId) then
                        g_MessageMgr:ShowMessage("BAG_SPACE_NOT_ENOUGH")
                        this:Close()
                    else
                        Gac2Gas.RequestGenerateLingfuItemFast(CHuanLingMgr.Inst:GetLingFuQuality(CHuanLingMgr.Inst.SelectedLingFuId), CHuanLingMgr.Inst:GetHuanLingNum(CHuanLingMgr.Inst.SelectedLingFuId))
                    end
                end
            end
        end), CHuanLingDrawWnd.s_Duration, ETickType.Once)
    end
end
CHuanLingDrawWnd.m_OnDrawAnotherButtonClicked_CS2LuaHook = function (this, go) 
    if not CHuanLingMgr.IS_HUANLING_FIVE then
        local hasFuZhi = false
        do
            local i = 0
            while i < this.fuzhiIDs.Length do
                local itemId = nil
                local bagPos = 0
                local default
                default, bagPos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, this.fuzhiIDs[i])
                if default then
                    hasFuZhi = true
                    EventManager.BroadcastInternalForLua(EnumEventType.OnSelectedFuZhi, {this.fuzhiIDs[i]})
                    CHuanLingMgr.Inst.SelectedLingFuId = this.fuzhiIDs[i]
                    break
                end
                i = i + 1
            end
        end
        if not hasFuZhi then
            return
        end

        local item = Item_Item.GetData(CHuanLingMgr.Inst.SelectedLingFuId)
        if item ~= nil then
            g_MessageMgr:ShowMessage("Draw_Another_One", item.Name)
            this:Close()
            CHuanLingMgr.IS_HUANLING_FIVE = false
            CDrawLineMgr.Inst.PenMaterialPath = nil
            CUIManager.ShowUI(CUIResources.HuanLingDrawWnd)
        end
    else
        local hasFuZhi = false
        do
            local i = 0
            while i < this.fuzhiIDs.Length do
                local bindCount, notbindCount
                local lingFuID = this.fuzhiIDs[i]
                bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(lingFuID)
                if bindCount + notbindCount >= CHuanLingMgr.Inst:GetHuanLingNum(lingFuID) then
                    hasFuZhi = true
                    EventManager.BroadcastInternalForLua(EnumEventType.OnSelectedFuZhi, {this.fuzhiIDs[i]})
                    CHuanLingMgr.Inst.SelectedLingFuId = this.fuzhiIDs[i]
                    break
                end
                i = i + 1
            end
        end
        if not hasFuZhi then
            return
        end

        local item = Item_Item.GetData(CHuanLingMgr.Inst.SelectedLingFuId)
        if item ~= nil then
            g_MessageMgr:ShowMessage("Draw_Another_One", item.Name)
            this:Close()
            CHuanLingMgr.IS_HUANLING_FIVE = true
            CDrawLineMgr.Inst.PenMaterialPath = nil
            CUIManager.ShowUI(CUIResources.HuanLingDrawWnd)
        end
    end
end
CHuanLingDrawWnd.m_CanDrawAnother_CS2LuaHook = function (this) 
    -- 包裹内有三种符纸之一 且 包裹内有多余的位置
    if not CHuanLingMgr.IS_HUANLING_FIVE then
        if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < 1 then
            return false
        end
        do
            local i = 0
            while i < this.fuzhiIDs.Length do
                local itemId = nil
                local bagPos = 0
                local default
                default, bagPos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, this.fuzhiIDs[i])
                if default then
                    return true
                end
                i = i + 1
            end
        end
        return false
    else
        if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < CHuanLingMgr.Inst:GetHuanLingNum(CHuanLingMgr.Inst.SelectedLingFuId) then
            return false
        end
        do
            local i = 0
            while i < this.fuzhiIDs.Length do
                local bindCount, notbindCount
                local lingFuID = this.fuzhiIDs[i]
                bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(lingFuID)
                if bindCount + notbindCount >= CHuanLingMgr.Inst:GetHuanLingNum(lingFuID) then
                    return true
                end
                i = i + 1
            end
        end
        return false
    end
end
CHuanLingDrawWnd.m_OnSelect_CS2LuaHook = function (this, btn, index) 
    if this:IsColorLocked(index) then
        g_MessageMgr:ShowMessage("Color_Locked")
        return
    else
        if index <= this.lineColors.Count then
            for i = 0, this.m_colorRadioBox.m_RadioButtons.Length - 1 do
                local bg = this.m_colorRadioBox.m_RadioButtons[i].m_BackGround
                bg.spriteName = index == i and "common_btn_09" or "common_btn_05"
                bg.width = index == i and  134 or 110
                bg.height = bg.width
            end
            CDrawLineMgr.Inst.LineColor = this.lineColors[index]
        end
    end
end
CHuanLingDrawWnd.m_IsColorLocked_CS2LuaHook = function (this, index) 
    -- quality 0 : 0, 1
    -- quality 1 : 0, 1, 3
    -- quality 2 : 0, 1, 2, 3
    local fuzhiQuality = CHuanLingMgr.Inst:GetFuZhiQuality(CHuanLingMgr.Inst.SelectedLingFuId)
    if fuzhiQuality == 0 and index > 1 then
        return true
    elseif fuzhiQuality == 1 and index > 2 then
        return true
    else
        if index <= this.lineColors.Count then
            return false
        end
    end
    return true
end
CHuanLingDrawWnd.m_OnShareToMengDaoClicked_CS2LuaHook = function (this, go) 
    CommonDefs.GetComponent_GameObject_Type(this.m_ShareToMengDao, typeof(QnButton)).Enabled = false
    LoadPicMgr.UploadPic(CPersonalSpaceMgr.Upload_MomentPhotoType, CommonDefs.EncodeToPNG(this.m_FuZhou:GetShapedTexture()), DelegateFactory.Action_string(function (url) 
        CommonDefs.GetComponent_GameObject_Type(this.m_ShareToMengDao, typeof(QnButton)).Enabled = true
        CHuanLingMgr.Inst:ShareLingFu(url, CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), this.m_Pos))
    end))

    CUIManager.ShowUI(CUIResources.HuanLingShareWnd)
end
CHuanLingDrawWnd.m_OnShareToPlatformClicked_CS2LuaHook = function (this, go) 

    --除官网渠道之外其他渠道屏蔽分享
	if not CLoginMgr.Inst:IsNetEaseOfficialLogin() then
        g_MessageMgr:ShowMessage("PC_CANNOT_SHARE")
        return
    end
    CommonDefs.GetComponent_GameObject_Type(this.m_ShareToPlatform, typeof(QnButton)).Enabled = false
    LoadPicMgr.UploadPic(CPersonalSpaceMgr.Upload_MomentPhotoType, CommonDefs.EncodeToPNG(this.m_FuZhou:GetShapedTexture()), DelegateFactory.Action_string(function (url) 
        CHuanLingMgr.Inst:ShareLingFu(url, CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), this.m_Pos))
    end))
end
CHuanLingDrawWnd.m_OnShareButtonClicked_CS2LuaHook = function (this, go) 
    if CHuanLingDrawWnd.ShowShareArea then
        CHuanLingDrawWnd.ShowShareArea = false
        this.m_ShareView:SetActive(false)
    else
        CHuanLingDrawWnd.ShowShareArea = true
        this.m_ShareView:SetActive(true)
    end
end
CHuanLingDrawWnd.m_ClearTexture_CS2LuaHook = function (this) 
    if this.m_CachedTexture ~= nil then
        Object1.Destroy(this.m_CachedTexture)
    end
end
