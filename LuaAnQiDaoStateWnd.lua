local TweenAlpha = import "TweenAlpha"
local Object = import "UnityEngine.Object"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture = import "L10.UI.CUITexture"
local Initialization_Init = import "L10.Game.Initialization_Init"
local Ease = import "DG.Tweening.Ease"
local CScene = import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CUIFx = import "L10.UI.CUIFx"
local CMainCamera = import "L10.Engine.CMainCamera"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local SoundManager = import "SoundManager"
local PlayerSettings = import "L10.Game.PlayerSettings"

CLuaAnQiDaoStateWnd = class()

RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangSlider")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuSlider")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangFakeSlider")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuFakeSlider")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangFakeFore")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuFakeFore")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangFore")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuFore")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_ContentNode")

RegistClassMember(CLuaAnQiDaoStateWnd, "m_ExpandButton")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_Alert")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_AlertNode")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_TipButton")

RegistClassMember(CLuaAnQiDaoStateWnd, "m_TimeLab")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangNumLab")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuNumLab")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangBullets")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuBullets")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangFxs")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuFxs")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_SliderColors")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_SelfNode")

RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangFlash")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuFlash")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangFlashTick")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuFlashTick")

RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangCannons")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuCannons")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangFakeCannons")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuFakeCannons")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_TimeToRefresh")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_RefreshTick")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_HaiCangCannonsNum")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_DaFuCannonsNum")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_FxPool")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_Fx")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_IsInFx")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_FxsPrepareToPlay")

RegistClassMember(CLuaAnQiDaoStateWnd, "m_DieTipAlert")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_FireAlert")

RegistClassMember(CLuaAnQiDaoStateWnd, "m_Sound1")
RegistClassMember(CLuaAnQiDaoStateWnd, "m_Sound2")

function CLuaAnQiDaoStateWnd:Awake()
    self.m_SliderColors = {"ff0000", "fe790a", "fcb824", "fbfd3e", "79ff1f"}
    self:InitComponents()

    self.m_HaiCangSlider.value = 1
    self.m_DaFuSlider.value = 1
    
    self.m_Alert:SetActive(false)
    self.m_SelfNode[0]:SetActive(false)
    self.m_SelfNode[1]:SetActive(false)
    self.m_DieTipAlert:SetActive(false)
    self.m_FireAlert:SetActive(false)

    if CLuaAnQiDaoMgr.haicangInfoList == nil then
        CLuaAnQiDaoMgr.haicangInfoList = {5, 3, 0}
    end
    if CLuaAnQiDaoMgr.dafuInfoList == nil then
        CLuaAnQiDaoMgr.dafuInfoList = {5, 3, 0}
    end

    self:RefreshState()
end

function CLuaAnQiDaoStateWnd:InitComponents()
    self.m_HaiCangSlider= self.transform:Find("Anchor/Left/Slider"):GetComponent(typeof(UISlider))
    self.m_DaFuSlider   = self.transform:Find("Anchor/Right/Slider"):GetComponent(typeof(UISlider))
    self.m_HaiCangFore  = self.transform:Find("Anchor/Left/Slider/Foreground"):GetComponent(typeof(UITexture))
    self.m_DaFuFore     = self.transform:Find("Anchor/Right/Slider/Foreground"):GetComponent(typeof(UITexture))
    self.m_ContentNode  = self.transform:Find("Anchor").gameObject

    self.m_ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    self.m_Alert        = self.transform:Find("Center/Alert").gameObject
    self.m_AlertNode    = self.transform:Find("Center/AlertNode").gameObject
    self.m_TipButton    = self.transform:Find("Anchor/TipButton").gameObject

    self.m_TimeLab      = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
    self.m_HaiCangNumLab= self.transform:Find("Anchor/Left/Cannons/Label"):GetComponent(typeof(UILabel))
    self.m_DaFuNumLab   = self.transform:Find("Anchor/Right/Cannons/Label"):GetComponent(typeof(UILabel))
    self.m_HaiCangBullets = {}
    self.m_DaFuBullets = {}
    self.m_HaiCangFxs = {}
    self.m_DaFuFxs = {}
    for i=1,3 do
        self.m_HaiCangBullets[i]= self.transform:Find("Anchor/Left/Cannons/Bullets/Bullet" .. i).gameObject
        self.m_DaFuBullets[i]   = self.transform:Find("Anchor/Right/Cannons/Bullets/Bullet" .. i).gameObject
        self.m_HaiCangFxs[i]    = self.transform:Find("Anchor/Left/Fxs/Fx".. i):GetComponent(typeof(CUIFx))
        self.m_DaFuFxs[i]       = self.transform:Find("Anchor/Right/Fxs/Fx".. i):GetComponent(typeof(CUIFx))
    end
    self.m_SelfNode     = {}
    self.m_SelfNode[0]  = self.transform:Find("Anchor/Left/Static/SelfNode").gameObject
    self.m_SelfNode[1]  = self.transform:Find("Anchor/Right/Static/SelfNode").gameObject

    self.m_HaiCangFlash = self.transform:Find("Anchor/Left/FakeCannons/Bullets"):GetComponent(typeof(TweenAlpha))
    self.m_DaFuFlash    = self.transform:Find("Anchor/Right/FakeCannons/Bullets"):GetComponent(typeof(TweenAlpha))

    self.m_HaiCangCannons       = self.transform:Find("Anchor/Left/Cannons").gameObject
    self.m_DaFuCannons          = self.transform:Find("Anchor/Right/Cannons").gameObject
    self.m_HaiCangFakeCannons   = self.transform:Find("Anchor/Left/FakeCannons").gameObject
    self.m_DaFuFakeCannons      = self.transform:Find("Anchor/Right/FakeCannons").gameObject

    self.m_HaiCangFakeSlider    = self.transform:Find("Anchor/Left/FakeSlider"):GetComponent(typeof(UISlider))
    self.m_DaFuFakeSlider       = self.transform:Find("Anchor/Right/FakeSlider"):GetComponent(typeof(UISlider))
    self.m_HaiCangFakeFore      = self.transform:Find("Anchor/Left/FakeSlider/Foreground"):GetComponent(typeof(UITexture))
    self.m_DaFuFakeFore         = self.transform:Find("Anchor/Right/FakeSlider/Foreground"):GetComponent(typeof(UITexture))

    self.m_DieTipAlert          = self.transform:Find("Center/BossAlert").gameObject
    self.m_FireAlert            = self.transform:Find("Center/FireAlert").gameObject

    self.m_HaiCangCannonsNum = 0
    self.m_DaFuCannonsNum = 0
    self.m_Fx = self.transform:Find("Anchor/Fx").gameObject
    self.m_FxPool = {}
    self.m_IsInFx = 0
    self.m_FxsPrepareToPlay = {}
end

function CLuaAnQiDaoStateWnd:Init()
    local summonCannonBallsTime = AnQiIsland_Setting.GetData().SummonCannonBallsTime
    self.m_TimeToRefresh = {}
    for i = 0, summonCannonBallsTime.Length - 1 do
        table.insert(self.m_TimeToRefresh, summonCannonBallsTime[i])
    end

    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaGuanNingMgr.m_ClickSignalWorldPos=go.transform.position
        CUIManager.ShowUI(CLuaUIResources.AnQiDaoSignalWnd)
    end)

    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    local gamePlayId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
    end

    if gamePlayId==51100208 then --关宁训练场
        self.m_TipButton:SetActive(false)
    else
        self.m_TipButton:SetActive(true)
    end

    if self.m_RefreshTick ~= nil then
        UnRegisterTick(self.m_RefreshTick)
    end
    self.m_RefreshTick = RegisterTickWithDuration(function ()
        self:OnTick()
    end, 1000, 1000 * 9999)
    self:OnTick()

    self:OnUpdatePlayerForceInfo()
end

function CLuaAnQiDaoStateWnd:OnTick()
    local remain = nil
    if CLuaAnQiDaoMgr.m_FightTime and self.m_TimeToRefresh ~= nil then
        local delta = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), CLuaAnQiDaoMgr.m_FightTime)
        local deltaSecond = math.floor(delta.TotalSeconds)
        for i=1,#self.m_TimeToRefresh do
            local nextTime = self.m_TimeToRefresh[i]
            if deltaSecond <= nextTime then
                remain = nextTime - deltaSecond
                break
            end
        end
    end

    if remain and remain >= 0 then
        self.m_TimeLab.text = SafeStringFormat3(LocalString.GetString("弹药点刷新还有%s秒"), SafeStringFormat3("[c][83ff46]%02d[-]", remain))
    else
        self.m_TimeLab.text = ""
    end
end

function CLuaAnQiDaoStateWnd:OnEnable()
    g_ScriptEvent:AddListener("AnQiDaoSyncState", self, "RefreshState")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncPVPCommand", self, "OnSyncPVPCommand")
    g_ScriptEvent:AddListener("AnQiIslandFireCannon", self, "OnAnQiIslandFireCannon")
    g_ScriptEvent:AddListener("UpdatePlayerForceInfo", self, "OnUpdatePlayerForceInfo")
    g_ScriptEvent:AddListener("AnQiIslandPickCannonBall", self, "OnAnQiIslandPickCannonBall")
    g_ScriptEvent:AddListener("AnQiIslandBossKilled", self, "OnAnQiIslandBossKilled")
end

function CLuaAnQiDaoStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AnQiDaoSyncState", self, "RefreshState")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncPVPCommand", self, "OnSyncPVPCommand")
    g_ScriptEvent:RemoveListener("AnQiIslandFireCannon", self, "OnAnQiIslandFireCannon")
    g_ScriptEvent:RemoveListener("UpdatePlayerForceInfo", self, "OnUpdatePlayerForceInfo")
    g_ScriptEvent:RemoveListener("AnQiIslandPickCannonBall", self, "OnAnQiIslandPickCannonBall")
    g_ScriptEvent:RemoveListener("AnQiIslandBossKilled", self, "OnAnQiIslandBossKilled")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function CLuaAnQiDaoStateWnd:OnUpdatePlayerForceInfo()
    local MainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if MainPlayerId ~= 0 then
        --CForcesMgr.Inst:GetPlayerForce在未分配阵营时会返回0,0代表defend
        local bFind, force = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, MainPlayerId)
        if bFind == true then
            CLuaAnQiDaoMgr.m_MainPlayerForce = force
            self.m_SelfNode[0]:SetActive(CLuaAnQiDaoMgr.m_MainPlayerForce == 0)
            self.m_SelfNode[1]:SetActive(CLuaAnQiDaoMgr.m_MainPlayerForce ~= 0)
        else
            self.m_SelfNode[0]:SetActive(false)
            self.m_SelfNode[1]:SetActive(false)
        end
    end
end

function CLuaAnQiDaoStateWnd:OnSyncPVPCommand(class,gender,msgId)
    local count=self.m_AlertNode.transform.childCount
    if count>0 then
        for i=1,count do
            local item=self.m_AlertNode.transform:GetChild(i-1)
            LuaUtils.SetLocalPositionY(item,item.localPosition.y+64)
        end
    end

    local go=NGUITools.AddChild(self.m_AlertNode,self.m_Alert)
    go:SetActive(true)
    local tf=go.transform
    local label = tf:Find("Node/Label"):GetComponent(typeof(UILabel))
    local icon = tf:Find("Node/Icon"):GetComponent(typeof(CUITexture))

    local designData = AnQiIsland_QuickCommand.GetData(msgId)
    label.text=designData.Content

    local initData=Initialization_Init.GetData(class*100+gender)
    if initData then
        icon:LoadPortrait(initData.ResName,false)
    end

    local texture= tf:Find("Node/Texture"):GetComponent(typeof(CUITexture))
    if designData.Icon then
        texture:LoadMaterial(designData.Icon)
        LuaUtils.SetLocalPositionX(label.transform,62)
    else
        texture:Clear()
        LuaUtils.SetLocalPositionX(label.transform,0)
    end

    Object.Destroy(go,2)

    local node=tf:Find("Node")

    --总时长1.5s长1.5s
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(go.transform, Vector3(0.5,0.5,1),Vector3(1,1,1),0.3), Ease.OutBack)
    LuaTweenUtils.TweenAlpha(node, 0,1,0.3)

    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(label.transform, 0,1,0.1),0.1)
    local bg=tf:Find("Bg")
    LuaTweenUtils.TweenAlpha(bg, 0,0.75,0.3)
    local sprite=tf:Find("Sprite")
    LuaTweenUtils.TweenAlpha(sprite, 0,0.3,0.3)

    LuaTweenUtils.TweenScale(bg, Vector3(0,0,1),Vector3(1,1,1),0.1)

    LuaTweenUtils.SetDelay(
            LuaTweenUtils.TweenAlpha(node,1,0,0.3),1.2)
            
    LuaTweenUtils.SetDelay(
        LuaTweenUtils.TweenAlpha(bg,0.75,0,0.3),1.2)
        
    LuaTweenUtils.SetDelay(
        LuaTweenUtils.TweenAlpha(sprite,0.3,0,0.3),1.2)
            
    LuaTweenUtils.SetDelay(
        LuaTweenUtils.SetEase(
            LuaTweenUtils.TweenPositionY(go.transform,100,0.3),Ease.InExpo),1.2)
end

function CLuaAnQiDaoStateWnd:RefreshState() 
    local haicangInfoList = CLuaAnQiDaoMgr.haicangInfoList
    local dafuInfoList = CLuaAnQiDaoMgr.dafuInfoList

    local haicangHp             = haicangInfoList[1]
    local haicangTotalBullet    = haicangInfoList[2]
    local haicangCurrentBullet  = haicangInfoList[3]
    local dafuHp                = dafuInfoList[1]
    local dafuTotalBullet       = dafuInfoList[2]
    local dafuCurrentBullet     = dafuInfoList[3]

    self.m_HaiCangFakeSlider.value = self.m_HaiCangSlider.value
    self.m_DaFuFakeSlider.value = self.m_DaFuSlider.value
    self.m_HaiCangFakeFore.color = self.m_HaiCangFore.color
    self.m_DaFuFakeFore.color = self.m_DaFuFore.color

    self.m_HaiCangSlider.value = haicangHp / 5
    self.m_DaFuSlider.value = dafuHp / 5
    self.m_HaiCangFore.color = NGUIText.ParseColor24(self.m_SliderColors[haicangHp > 0 and haicangHp or 1], 0)
    self.m_DaFuFore.color = NGUIText.ParseColor24(self.m_SliderColors[dafuHp > 0 and dafuHp or 1], 0)
    self.m_HaiCangCannonsNum = haicangCurrentBullet
    self.m_DaFuCannonsNum = dafuCurrentBullet
    if self.m_IsInFx == 0 then
        self:RefreshBulletsState(haicangCurrentBullet, dafuCurrentBullet)
    end
end

function CLuaAnQiDaoStateWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function CLuaAnQiDaoStateWnd:RefreshBulletsState(haiCangBulletNum, daFuBulletNum)
    for i=1, 3 do
        if not self.m_HaiCangBullets[i].gameObject.activeSelf and haiCangBulletNum >= i then
            self.m_HaiCangFxs[i]:DestroyFx()
            self.m_HaiCangFxs[i]:LoadFx(CUIFxPaths.PlayerLevelUpBoomFxPath)
        end
        self.m_HaiCangBullets[i].gameObject:SetActive(haiCangBulletNum >= i)
        if not self.m_DaFuBullets[i].gameObject.activeSelf and daFuBulletNum >= i then
            self.m_DaFuFxs[i]:DestroyFx()
            self.m_DaFuFxs[i]:LoadFx(CUIFxPaths.PlayerLevelUpBoomFxPath)
        end
        self.m_DaFuBullets[i].gameObject:SetActive(daFuBulletNum >= i)
    end

    self.m_HaiCangNumLab.text = haiCangBulletNum .. "/3"
    self.m_DaFuNumLab.text = daFuBulletNum .. "/3"
end

function CLuaAnQiDaoStateWnd:PreparePlayBulletsFx(haiCangBulletNum, daFuBulletNum)
    for i=1, 3 do
        if not self.m_HaiCangBullets[i].gameObject.activeSelf and haiCangBulletNum >= i then
            table.insert(self.m_FxsPrepareToPlay, self.m_HaiCangFxs[i])
        end

        if not self.m_DaFuBullets[i].gameObject.activeSelf and daFuBulletNum >= i then
            table.insert(self.m_FxsPrepareToPlay, self.m_DaFuFxs[i])
        end
    end
end

function CLuaAnQiDaoStateWnd:PlayBulletsFx()
    for i=1, #self.m_FxsPrepareToPlay do
        local fx = self.m_FxsPrepareToPlay[i]
        fx:DestroyFx()
        fx:LoadFx(CUIFxPaths.PlayerLevelUpBoomFxPath)
    end

    self.m_FxsPrepareToPlay = {}
end

function CLuaAnQiDaoStateWnd:OnAnQiIslandFireCannon(force, cnt)
    --用满状态的FakeCannons来闪烁
    if force == 0 then
        if self.m_Sound1 then
            SoundManager.Inst:StopSound(self.m_Sound1)
        end
        self.m_Sound1 = SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.AnQiDaoFire, Vector3.zero, nil, 0)
        self.m_HaiCangCannons:SetActive(false)
        self.m_HaiCangFakeCannons:SetActive(true)
        
        self.m_DaFuFakeSlider.gameObject:SetActive(true)

        self.m_HaiCangFlash.enabled = true
        self:FireAlert(force)

        if self.m_HaiCangFlashTick then
            UnRegisterTick(self.m_HaiCangFlashTick)
        end
        self.m_HaiCangFlashTick = RegisterTickOnce(function () 
            SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.AnQiDaoHit, Vector3.zero, nil, 0, 0.5 *  PlayerSettings.VolumeSetting)
            self.m_HaiCangCannons:SetActive(true)
            self.m_HaiCangFakeCannons:SetActive(false)
            
            self.m_DaFuFakeSlider.gameObject:SetActive(false)

            self.m_HaiCangFlash.enabled = false
            self.m_HaiCangFlash.gameObject:GetComponent(typeof(UIWidget)).alpha = 1
            self.m_HaiCangFlashTick = nil
        end, 3 * 1000)
    else
        if self.m_Sound2 then
            SoundManager.Inst:StopSound(self.m_Sound2)
        end
        self.m_Sound2 = SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.AnQiDaoFire, Vector3.zero, nil, 0)
        self.m_DaFuCannons:SetActive(false)
        self.m_DaFuFakeCannons:SetActive(true)
        
        self.m_HaiCangFakeSlider.gameObject:SetActive(true)

        self.m_DaFuFlash.enabled = true
        self:FireAlert(force)

        if self.m_DaFuFlashTick then
            UnRegisterTick(self.m_DaFuFlashTick)
        end
        self.m_DaFuFlashTick = RegisterTickOnce(function () 
            SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.AnQiDaoHit, Vector3.zero, nil, 0, 0.5 *  PlayerSettings.VolumeSetting)
            self.m_DaFuCannons:SetActive(true)
            self.m_DaFuFakeCannons:SetActive(false)
            
            self.m_HaiCangFakeSlider.gameObject:SetActive(false)

            self.m_DaFuFlash.enabled = false
            self.m_DaFuFlash.gameObject:GetComponent(typeof(UIWidget)).alpha = 1
            self.m_DaFuFlashTick = nil
        end, 3 * 1000)
    end
end

function CLuaAnQiDaoStateWnd:OnAnQiIslandPickCannonBall(ballPos)
    local screenPos = CMainCamera.Main:WorldToScreenPoint(ballPos)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)

    local target = self:GetNextEmptyBullet(CLuaAnQiDaoMgr.m_MainPlayerForce)
    
    if target ~= nil then
        self:PlayLineAni(nguiWorldPos, target.transform.position, 1)
    end
end

function CLuaAnQiDaoStateWnd:PlayLineAni(from, to, duration)
    self.m_IsInFx = self.m_IsInFx + 1
    local lineFx = nil
    if #self.m_FxPool == 0 then
        lineFx = CommonDefs.Object_Instantiate(self.m_Fx):GetComponent(typeof(CUIFx))
        lineFx.gameObject.transform.parent = self.transform
    else
        lineFx = self.m_FxPool[#self.m_FxPool]
        table.remove(self.m_FxPool, #self.m_FxPool)
    end
    lineFx.gameObject:SetActive(true)

    self:PreparePlayBulletsFx(self.m_HaiCangCannonsNum, self.m_DaFuCannonsNum)
    lineFx.OnLoadFxFinish = DelegateFactory.Action(function()
        lineFx.gameObject.transform.position = from
        local tweener = CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(lineFx.transform, to, duration, false), Ease.Linear)
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(
            function()
                lineFx:DestroyFx()
                lineFx.gameObject:SetActive(false)
                table.insert(self.m_FxPool, lineFx)

                self:RefreshBulletsState(self.m_HaiCangCannonsNum, self.m_DaFuCannonsNum)
                self:PlayBulletsFx()
                self.m_IsInFx = self.m_IsInFx - 1
            end))
    end)
    lineFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
end

function CLuaAnQiDaoStateWnd:GetNextEmptyBullet(force)
    if force == 0 then
        self.m_HaiCangCannonsNum = self.m_HaiCangCannonsNum + 1
        if self.m_HaiCangCannonsNum > 3 or self.m_HaiCangFlashTick ~= nil then
            return nil
        end
        return self.m_HaiCangBullets[self.m_HaiCangCannonsNum].gameObject
    else
        self.m_DaFuCannonsNum = self.m_DaFuCannonsNum + 1
        if self.m_DaFuCannonsNum > 3 or self.m_DaFuFlashTick ~= nil then
            return nil
        end
        return self.m_DaFuBullets[self.m_DaFuCannonsNum].gameObject
    end
end

function CLuaAnQiDaoStateWnd:OnAnQiIslandBossKilled(force)
    -- 居中通知
    local count=self.m_AlertNode.transform.childCount
    if count>0 then
        for i=1,count do
            local item=self.m_AlertNode.transform:GetChild(i-1)
            LuaUtils.SetLocalPositionY(item,item.localPosition.y+128)
        end
    end

    local go=NGUITools.AddChild(self.m_AlertNode,self.m_DieTipAlert)
    go:SetActive(true)
    local tf=go.transform
    Object.Destroy(go,3)

    local icon = tf:Find("Icon"):GetComponent(typeof(UITexture))
    local dieFx = tf:Find("Icon/Fx"):GetComponent(typeof(CUIFx))
    local dieTipFx = tf:Find("Fx"):GetComponent(typeof(CUIFx))

    dieFx:DestroyFx()
    dieFx:LoadFx(CUIFxPaths.ShenShouSiWang1)

    if self.m_DieTipFxDelayTick then
        UnRegisterTick(self.m_DieTipFxDelayTick)
    end
    self.m_DieTipFxDelayTick = RegisterTickOnce(function ()
        dieTipFx:DestroyFx()
        dieTipFx:LoadFx(CUIFxPaths.ShenShouSiWang2)
        self.m_DieTipFxDelayTick = nil
    end, 500)

    
    LuaTweenUtils.SetDelay(
        LuaTweenUtils.TweenAlpha(icon,1,0,0.5),2.4)

    -- UI特效
    local target = self:GetNextEmptyBullet(force)
    local target1 = self:GetNextEmptyBullet(force)
    if target ~= nil then
        self:PlayLineAni(icon.transform.position, target.transform.position, 1)
    end
    if target1 ~= nil then
        self:PlayLineAni(icon.transform.position, target1.transform.position, 1)
    end
end

function CLuaAnQiDaoStateWnd:FireAlert(force)
    local count=self.m_AlertNode.transform.childCount
    if count>0 then
        for i=1,count do
            local item=self.m_AlertNode.transform:GetChild(i-1)
            LuaUtils.SetLocalPositionY(item,item.localPosition.y+128)
        end
    end

    local go=NGUITools.AddChild(self.m_AlertNode,self.m_FireAlert)
    go:SetActive(true)
    Object.Destroy(go,3.5)

    local tf = go.transform
    local sprite = nil
    if  force == 0  then
        sprite = tf:Find("haicangFire")
    else
        sprite = tf:Find("dafuFire")
    end
    sprite.gameObject:SetActive(true)

    LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(go.transform, Vector3(0.5,0.5,1),Vector3(1,1,1),0.3), Ease.OutBack)
    LuaTweenUtils.TweenAlpha(sprite, 0,1,0.3)
    LuaTweenUtils.SetDelay(
            LuaTweenUtils.TweenAlpha(sprite,1,0,0.3),3)
    LuaTweenUtils.SetDelay(
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionY(go.transform,100,0.3),Ease.InExpo),3)
end

function CLuaAnQiDaoStateWnd:OnDestroy()
    if self.m_RefreshTick then
        UnRegisterTick(self.m_RefreshTick)
    end
    if self.m_HaiCangFlashTick then
        UnRegisterTick(self.m_HaiCangFlashTick)
    end
    if self.m_DaFuFlashTick then
        UnRegisterTick(self.m_DaFuFlashTick)
    end
    if self.m_DieTipFxDelayTick then
        UnRegisterTick(self.m_DieTipFxDelayTick)
    end
    CLuaAnQiDaoMgr.m_MainPlayerForce = nil
    CLuaAnQiDaoMgr.m_FightTime = nil
    CLuaAnQiDaoMgr.haicangInfoList = nil
    CLuaAnQiDaoMgr.dafuInfoList = nil
end
