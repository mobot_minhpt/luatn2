local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local QnTabButton = import "L10.UI.QnTabButton"
local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"
local UILabel = import "UILabel"
local LuaTweenUtils = import "LuaTweenUtils"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaHengYuDangKouOpenWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouOpenWnd, "ModeTabBar", "ModeTabBar", GameObject)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "Common", "Common", GameObject)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "Eilte", "Eilte", GameObject)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "DifficultyTabBar", "DifficultyTabBar", UITabBar)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "CommonDescLabel", "CommonDescLabel", UILabel)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "EilteDescLabel", "EilteDescLabel", UILabel)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "OpenBtn", "OpenBtn", GameObject)
RegistChildComponent(LuaHengYuDangKouOpenWnd, "OpenMark", "OpenMark", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouOpenWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.OpenBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenBtnClick()
	end)


    --@endregion EventBind end
end

function LuaHengYuDangKouOpenWnd:Init()
	Gac2Gas.RequestOpenHengYuDangKouInfo()

	self.CommonDescLabel.text = g_MessageMgr:FormatMessage("HengYuDangKou_OpenWnd_CommonDesc")
	self.EilteDescLabel.text = g_MessageMgr:FormatMessage("HengYuDangKou_OpenWnd_EilteDesc")

	self.DifficultyTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:SetDiff(index+1)
	end)

	for i = 1, 3 do
		local btn = self.DifficultyTabBar:GetTabGoByIndex(i-1):GetComponent(typeof(BoxCollider))
		btn.enabled = false
	end
	self.OpenBtn:SetActive(false)
	self.OpenMark:SetActive(false)
end

function LuaHengYuDangKouOpenWnd:SetState(hasOpen, diff)
	self.m_HasOpen = hasOpen
	self.OpenBtn:SetActive(not hasOpen)
	self.OpenMark:SetActive(hasOpen)

	if self.m_Info.isFinish then
		self.OpenMark.transform:Find("Common"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已结束")
		self.OpenMark.transform:Find("Eilte"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已结束")
	else
		self.OpenMark.transform:Find("Common"):GetComponent(typeof(UILabel)).text = LocalString.GetString("普通副本开启中")
		self.OpenMark.transform:Find("Eilte"):GetComponent(typeof(UILabel)).text = LocalString.GetString("精英副本开启中")
	end

	self.Eilte.transform:Find("On").gameObject:SetActive(hasOpen)
	self.Eilte.transform:Find("Off").gameObject:SetActive(not hasOpen)

	if self.m_Tween then
		LuaTweenUtils.Kill(self.m_Tween, false)
	end

	if hasOpen then
		self.ModeTabBar.gameObject:GetComponent(typeof(Animation)):Play("hengyudangkouopenwnd_open")
	end

	-- 如果开启之后 就禁止改变难度
	for i = 1, 3 do
		local btn = self.DifficultyTabBar:GetTabGoByIndex(i-1):GetComponent(typeof(BoxCollider))
		btn.enabled = not hasOpen
	end
	self:SetDiff(diff)
end

function LuaHengYuDangKouOpenWnd:SetDiff(diff)
	self.m_Diff = diff
	for i = 1, 3 do
		local btn = self.DifficultyTabBar:GetTabGoByIndex(i-1):GetComponent(typeof(QnTabButton))
		btn.Selected = i == diff
	end

	for i = 1, 3 do
		self.Eilte.transform:Find(SafeStringFormat3("On/Difficulty/Level_%d", i)).gameObject:SetActive(diff == i)
	end
end

--  replyInfo = {
-- 	difficulty=difficulty,
-- 	onlineMemberCount=onlineMemberCount,
-- 	totalMemberCount=totalMemberCount,
-- 	maxMemberCount=maxMemberCount,
-- 	scale=scale,
-- }
function LuaHengYuDangKouOpenWnd:OnReplyOpenHengYuDangKouInfo(info)
	self.CountLabel.text = SafeStringFormat3(LocalString.GetString("在线人数：%d/%d"), info.onlineMemberCount, info.totalMemberCount)
	self.m_Info = info

	print(info.isOpen, info.isFinsih, info.difficulty)
	local difficulty = info.difficulty
	if difficulty == 0 then
		difficulty = 1
	end
	
	self:SetState(info.isOpen, difficulty)
end

function LuaHengYuDangKouOpenWnd:OnEnable()
	g_ScriptEvent:AddListener("ReplyOpenHengYuDangKouInfo", self, "OnReplyOpenHengYuDangKouInfo")
end

function LuaHengYuDangKouOpenWnd:OnDisable()
	if self.m_Tween then
		LuaTweenUtils.Kill(self.m_Tween, false)
	end
	g_ScriptEvent:RemoveListener("ReplyOpenHengYuDangKouInfo", self, "OnReplyOpenHengYuDangKouInfo")
end

--@region UIEvent

function LuaHengYuDangKouOpenWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("HengYuDangKou_OpenWnd_Tip")
end

function LuaHengYuDangKouOpenWnd:OnOpenBtnClick()
	Gac2Gas.OpenHengYuDangKou(1, self.m_Diff)
	CUIManager.CloseUI(CLuaUIResources.HengYuDangKouOpenWnd)
end


--@endregion UIEvent

