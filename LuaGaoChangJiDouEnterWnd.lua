local UITabBar = import "L10.UI.UITabBar"
local CGaoChangMgr = import "L10.Game.CGaoChangMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"

LuaGaoChangJiDouEnterWnd = class()

--RegistClassMember(LuaGaoChangJiDouEnterWnd, "TabBar")
RegistClassMember(LuaGaoChangJiDouEnterWnd, "TipBtn")
RegistClassMember(LuaGaoChangJiDouEnterWnd, "NormalSignUpBtn")
RegistClassMember(LuaGaoChangJiDouEnterWnd, "JiDouSignUpBtn")
RegistClassMember(LuaGaoChangJiDouEnterWnd, "SignUpLabel")
RegistClassMember(LuaGaoChangJiDouEnterWnd, "RuleLabel")
RegistClassMember(LuaGaoChangJiDouEnterWnd, "EnterButton")
RegistClassMember(LuaGaoChangJiDouEnterWnd, "CancelButton")


function LuaGaoChangJiDouEnterWnd:Awake()
    --self.TabBar = self.transform:Find("Anchor/Header/TabBar"):GetComponent(typeof(UITabBar))
    self.TipBtn = self.transform:Find("Anchor/Header/TipButton").gameObject
    self.NormalSignUpBtn = self.transform:Find("Anchor/SignUpButton"):GetComponent(typeof(QnButton))
    self.JiDouSignUpBtn = self.transform:Find("Anchor/SignUpButton (1)"):GetComponent(typeof(QnButton))
    self.SignUpLabel = self.transform:Find("Anchor/SignUpLabel"):GetComponent(typeof(UILabel))
    self.RuleLabel = self.transform:Find("Anchor/Background/Label"):GetComponent(typeof(UILabel))
    self.EnterButton = self.transform:Find("Anchor/EnterButton"):GetComponent(typeof(QnButton))
    self.CancelButton = self.transform:Find("Anchor/CancelButton"):GetComponent(typeof(QnButton))
    
    self.CancelButton.gameObject:SetActive(false)

    self.RuleLabel.text = CUICommonDef.TranslateToNGUIText(GaoChangJiDou_Setting.GetData().JiDouDescription)

    UIEventListener.Get(self.TipBtn).onClick = LuaUtils.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("GAOCHANG_JIDOU_INTRODUCTION_TIP")
    end)

    UIEventListener.Get(self.EnterButton.gameObject).onClick = LuaUtils.VoidDelegate(function (p)
        if not CGaoChangMgr.Inst.canApply then
            Gac2Gas.GaoChangEnterStage1()
            CUIManager.CloseUI(CLuaUIResources.YuanXiaoGaoChangEnterWnd)
        elseif LuaGaoChangJiDouMgr.hasApplied then
            Gac2Gas.GaoChangJiDouCrossEnterStage1()
            CUIManager.CloseUI(CLuaUIResources.YuanXiaoGaoChangEnterWnd)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先报名高昌"))
        end
    end)

    UIEventListener.Get(self.NormalSignUpBtn.gameObject).onClick = LuaUtils.VoidDelegate(function (p)
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GAOCHANG_APPLY_INTERFACE_TIP"), function()
            Gac2Gas.GaoChangApply()
        end, nil, nil, nil, false)
    end)

    UIEventListener.Get(self.JiDouSignUpBtn.gameObject).onClick = LuaUtils.VoidDelegate(function (p)
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GAOCHANG_JIDOU_APPLY_INTERFACE_TIP"), function()
            Gac2Gas.GaoChangJiDouCrossApply()
        end, nil, nil, nil, false)
    end)

    UIEventListener.Get(self.CancelButton.gameObject).onClick = LuaUtils.VoidDelegate(function(p)
        if not CGaoChangMgr.Inst.canApply then
            Gac2Gas.GaoChangCancelApply()
        elseif LuaGaoChangJiDouMgr.hasApplied then
            Gac2Gas.GaoChangJiDouCrossCancelApply()
        end
    end)

    --[[
    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        if index == 0 then
            self.SignUpBtn.m_Label.text = LocalString.GetString("报名普通模式") 
            self.RuleLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("GAOCHANG_INTERFACE_TIP"))
        elseif index == 1 then
            self.SignUpBtn.m_Label.text = LocalString.GetString("报名激斗模式")
            self.RuleLabel.text = CUICommonDef.TranslateToNGUIText(GaoChangJiDou_Setting.GetData().JiDouDescription)
        end
    end)
    --]]
end

function LuaGaoChangJiDouEnterWnd:Init()
    if not LuaGaoChangJiDouMgr.hasApplied then
        --self.TabBar:ChangeTab(0)
        self.TryTriggerGuide()
    end
end

function LuaGaoChangJiDouEnterWnd:OnDestroy()
    
    CGuideMgr.Inst:EndCurrentPhase()
end

function LuaGaoChangJiDouEnterWnd:OnEnable()
    self:RefreshGaoChangJiDouEnterWnd()
    g_ScriptEvent:AddListener("RefreshGaoChangJiDouEnterWnd", self, "RefreshGaoChangJiDouEnterWnd")
end

function LuaGaoChangJiDouEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshGaoChangJiDouEnterWnd", self, "RefreshGaoChangJiDouEnterWnd")
end

function LuaGaoChangJiDouEnterWnd:RefreshGaoChangJiDouEnterWnd()
    if not CGaoChangMgr.Inst.canApply or LuaGaoChangJiDouMgr.hasApplied then
        self.SignUpLabel.gameObject:SetActive(true)
        --LuaUtils.SetLocalPositionX(self.SignUpLabel.transform, 512)
        self.NormalSignUpBtn.gameObject:SetActive(false)
        self.JiDouSignUpBtn.gameObject:SetActive(false)
        self.CancelButton.gameObject:SetActive(true)
        if LuaGaoChangJiDouMgr.hasApplied then
            self.EnterButton.m_Label.text = LocalString.GetString("进入激斗模式") 
            self.SignUpLabel.text = LocalString.GetString("已报名高昌激斗模式") 
        else
            self.EnterButton.m_Label.text = LocalString.GetString("进入普通模式") 
            self.SignUpLabel.text = LocalString.GetString("已报名高昌普通模式") 
        end
    else
        self.SignUpLabel.gameObject:SetActive(false)
        self.NormalSignUpBtn.gameObject:SetActive(true)
        self.JiDouSignUpBtn.gameObject:SetActive(true)
        self.CancelButton.gameObject:SetActive(false)
        self.EnterButton.m_Label.text = LocalString.GetString("进入高昌") 
    end

    --if LuaGaoChangJiDouMgr.hasApplied then
    --    self.TabBar:ChangeTab(1)
    --end
end

function LuaGaoChangJiDouEnterWnd:GetGuideGo(methodName)
    if methodName == "GetJiDouModeBtn" then
        --return self.TabBar.transform:GetChild(1).gameObject
        return self.JiDouSignUpBtn.gameObject
	elseif methodName == "GetTipBtn" then
		return self.TipBtn
	end
	return nil
end

function LuaGaoChangJiDouEnterWnd.TryTriggerGuide()
    if not CLuaGuideMgr.EnableGuide() then return end
    if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.GaoChangJiDouIntro) then
        return
    end
    local triggered = CGuideMgr.Inst:StartGuide(EnumGuideKey.GaoChangJiDouIntro, 0)
    if triggered then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.GaoChangJiDouIntro)
    end
end
