local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local EnumTeamCCSettingType = import "L10.Game.EnumTeamCCSettingType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local GameSetting_Common = import "L10.Game.GameSetting_Common"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"

LuaTeamCCSettingWnd = class()

RegistChildComponent(LuaTeamCCSettingWnd, "m_TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaTeamCCSettingWnd, "m_DisableTeamCCCheckbox", "DisableCheckbox", QnCheckBox)
RegistChildComponent(LuaTeamCCSettingWnd, "m_EnableListenCheckbox", "EnableListenCheckbox", QnCheckBox)
RegistChildComponent(LuaTeamCCSettingWnd, "m_EnableCaptureCheckbox", "EnableCaptureCheckbox", QnCheckBox)
RegistChildComponent(LuaTeamCCSettingWnd, "m_OKButton", "OKButton", GameObject)
RegistChildComponent(LuaTeamCCSettingWnd, "m_CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaTeamCCSettingWnd, "m_CloseButton", "CloseButton", GameObject)

function LuaTeamCCSettingWnd:Init()

	CommonDefs.AddOnClickListener(self.m_OKButton, DelegateFactory.Action_GameObject(function(go) self:OnOKButtonClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_CancelButton, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseButtonClick(go) end), false)


	self.m_DisableTeamCCCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
		if selected then
			self:OnCheckBoxSelected(self.m_DisableTeamCCCheckbox)
		end
	end)
	self.m_EnableListenCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
		if selected then
			self:OnCheckBoxSelected(self.m_EnableListenCheckbox)
		end
	end)
	self.m_EnableCaptureCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
		if selected then
			self:OnCheckBoxSelected(self.m_EnableCaptureCheckbox)
		end
	end)
	
	self:OnMainPlayerTeamCCTypeUpdate()
end

function LuaTeamCCSettingWnd:OnEnable()
	g_ScriptEvent:AddListener("OnTeamCCSettingTypeUpdate", self, "OnMainPlayerTeamCCTypeUpdate")
end

function LuaTeamCCSettingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnTeamCCSettingTypeUpdate", self, "OnMainPlayerTeamCCTypeUpdate")
end

function LuaTeamCCSettingWnd:OnMainPlayerTeamCCTypeUpdate(args)

	local type = EnumToInt(EnumTeamCCSettingType.eDisableTeamCC)

	if CClientMainPlayer.Inst then
		type = CClientMainPlayer.Inst.RelationshipProp.TeamSpeakStatus
	end
	self.m_DisableTeamCCCheckbox:SetSelected(type == EnumToInt(EnumTeamCCSettingType.eDisableTeamCC), true)
	self.m_EnableListenCheckbox:SetSelected(type == EnumToInt(EnumTeamCCSettingType.eEnableListen), true);
	self.m_EnableCaptureCheckbox:SetSelected(type == EnumToInt(EnumTeamCCSettingType.eEnableCapture), true);
end

function LuaTeamCCSettingWnd:OnOKButtonClick(go)

	if CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_Team_CC_Capture_Setting, true) then
        return
    end

	local type = EnumTeamCCSettingType.eDisableTeamCC
	if self.m_DisableTeamCCCheckbox.Selected then
		type =  EnumTeamCCSettingType.eDisableTeamCC
	elseif self.m_EnableListenCheckbox.Selected then
		type =  EnumTeamCCSettingType.eEnableListen
	elseif self.m_EnableCaptureCheckbox.Selected then
		type =  EnumTeamCCSettingType.eEnableCapture
	end

	--根据浩神的意见先按外网patch方式进行等级筛选，以后正式的改法需要服务器进行统一等级控制
	local lv = tonumber(GameSetting_Common.GetData().TEAM_FREE_VOICE_LIMIT_LEVEL)
	if type == EnumTeamCCSettingType.eEnableCapture and CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel < lv then
		g_MessageMgr:ShowMessage("TEAM_FREE_VOICE_LEVEL_LIMIT", lv)
		return
	end

	if type == EnumTeamCCSettingType.eEnableCapture and CChatMgr.Inst:IsInChatRoom("teamccsettingwnd") then
		g_MessageMgr:ShowMessage("Voice_Mute_Can_Not_CC_Capture_When_In_Club_House")
		return
	end

	CCCChatMgr.Inst:InitTeamCC(type)
	Gac2Gas.SetTeamSpeakStatus(EnumToInt(type))
	self:Close()
end

function LuaTeamCCSettingWnd:OnCancelButtonClick(go)
	self:Close()
end

function LuaTeamCCSettingWnd:OnCloseButtonClick(go)
	self:Close()
end

function LuaTeamCCSettingWnd:OnCheckBoxSelected(btn)
	self.m_DisableTeamCCCheckbox:SetSelected(btn == self.m_DisableTeamCCCheckbox, true)
	self.m_EnableListenCheckbox:SetSelected(btn == self.m_EnableListenCheckbox, true)
	self.m_EnableCaptureCheckbox:SetSelected(btn == self.m_EnableCaptureCheckbox, true)
end

function LuaTeamCCSettingWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
