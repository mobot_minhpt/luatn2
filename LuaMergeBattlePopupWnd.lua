local LuaGameObject=import "LuaGameObject"

local Vector3 = import "UnityEngine.Vector3"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local CUIManager = import "L10.UI.CUIManager"
local UICamera = import "UICamera"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"


CLuaMergeBattlePopupWnd=class()--还用了这个类来完成tooltip类似的点击其他区域 关闭自己的功能-copyBtn


function CLuaMergeBattlePopupWnd:Awake()

end

function CLuaMergeBattlePopupWnd:Init()
	

	
end
function CLuaMergeBattlePopupWnd:Update()

	self:ClickThroughToClose()
end

function CLuaMergeBattlePopupWnd:OnEnable()
	local anchor =LuaGameObject.GetChildNoGC(self.transform,"Anchor").gameObject
	if anchor~=nil then
		local infoBtn=CommonDefs.GetComponent_Component_Type(anchor.transform:Find("InfoBtn"), typeof(CButton))
		local pointsBtn=CommonDefs.GetComponent_Component_Type(anchor.transform:Find("PointsBtn"), typeof(CButton))

		if CLuaPointsDetailWnd.m_targetData and CLuaPointsDetailWnd.m_targetId and infoBtn and pointsBtn then
		    CommonDefs.AddOnClickListener(infoBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
				CPlayerInfoMgr.ShowPlayerPopupMenu(CLuaPointsDetailWnd.m_targetId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
		        --self:ShowSelf(false)
			end), false)

		    CommonDefs.AddOnClickListener(pointsBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
				CUIManager.ShowUI(CLuaUIResources.PointsComparedWnd)
		        --self:ShowSelf(false)
			end), false)
		end
	end
end

function CLuaMergeBattlePopupWnd:OnDisable()
	
end
function CLuaMergeBattlePopupWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self:ShowSelf(false)
        end
    end
end

function CLuaMergeBattlePopupWnd:ShowSelf(show)
		self.gameObject:SetActive(show)
    
end

function CLuaMergeBattlePopupWnd:OnDestroy()
    
end

