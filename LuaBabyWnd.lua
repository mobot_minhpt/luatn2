local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local UITabBar = import "L10.UI.UITabBar"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local BabyMgr = import "L10.Game.BabyMgr"

LuaBabyWnd = class()

RegistClassMember(LuaBabyWnd,"TabBar")
RegistClassMember(LuaBabyWnd, "BabyInfoView")
RegistClassMember(LuaBabyWnd, "SchedulePlanView")


function LuaBabyWnd:Init()
	
	if not BabyMgr.Inst.BabyDictionary or BabyMgr.Inst.BabyDictionary.Count == 0 then
		CUIManager.CloseUI(CLuaUIResources.BabyWnd)
		return
	end
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyWnd:InitClassMembers()
	self.TabBar = self.gameObject:GetComponent(typeof(UITabBar))

	self.BabyInfoView = self.transform:Find("BabyInfoView").gameObject
	self.BabyInfoView:SetActive(false)
	self.SchedulePlanView = self.transform:Find("SchedulePlanView").gameObject
	self.SchedulePlanView:SetActive(false)
end

function LuaBabyWnd:InitValues()
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)

	self.TabBar:ChangeTab(LuaBabyMgr.m_BabyWndTab)
end

function LuaBabyWnd:OnTabChange(go, index)
	if index == 0 then
		self.BabyInfoView:SetActive(true)
		self.SchedulePlanView:SetActive(false)
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.BabyInfoView, typeof(CCommonLuaScript))
		luaScript:Init({})
	elseif index == 1 then
		self.BabyInfoView:SetActive(false)
		self.SchedulePlanView:SetActive(true)
		local luaScript = CommonDefs.GetComponent_GameObject_Type(self.SchedulePlanView, typeof(CCommonLuaScript))
		luaScript:Init({})
	end
end

function LuaBabyWnd:OnBabyInfoDelete()
	CUIManager.CloseUI(CLuaUIResources.BabyWnd)
end

function LuaBabyWnd:OnEnable()
	g_ScriptEvent:AddListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

function LuaBabyWnd:OnDisable()
	LuaBabyMgr.m_BabyWndTab = 0
	g_ScriptEvent:RemoveListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

-- return LuaBabyWnd

function LuaBabyWnd:GetGuideGo(methodName)
	if methodName=="Get2rdTab" then
		local tf = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs")
		return tf and tf:GetChild(1).gameObject
	elseif methodName=="GetAddPlanButton" then
		if self.SchedulePlanView.activeSelf then
			local tf = self.SchedulePlanView.transform:Find("ScheduleRoot/ScheduleGrid")
			for i=1,tf.childCount do
				if i>4 then--
					local sprite = tf:GetChild(i-1):Find("AddPlanSprite").gameObject
					if sprite.activeSelf then
						return sprite
					end
				end
			end
		end
		CGuideMgr.Inst:EndCurrentPhase()
		return nil
	elseif methodName=="QiChangButton" then
		local tf = self.transform:Find("BabyInfoView/QiChangRoot/QiChangTag/QiChangBG")
		if not tf.gameObject.activeInHierarchy then
			tf = self.transform:Find("BabyInfoView/QiChangRoot/NotOpen")
		end
		return tf and tf.gameObject
	end
	return nil
end