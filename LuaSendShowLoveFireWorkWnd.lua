local QnButton = import "L10.UI.QnButton"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local LayerDefine = import "L10.Engine.LayerDefine"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local Camera = import "UnityEngine.Camera"
local Color = import "UnityEngine.Color"
local Quaternion = import "UnityEngine.Quaternion"
local CCustomParticleEffect = import "L10.Game.CCustomParticleEffect"
local CCustomParticleMultyEffect = import "L10.Game.CCustomParticleMultyEffect"

LuaSendShowLoveFireWorkWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "FriendNode", "FriendNode", GameObject)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "FriendIcon", "FriendIcon", CUITexture)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "AddSprite", "AddSprite", GameObject)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "FireworkIcon", "FireworkIcon", CUITexture)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "FireworkCountLabel", "FireworkCountLabel", UILabel)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "LightFireBtn", "LightFireBtn", GameObject)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "FriendshipLabel", "FriendshipLabel", UILabel)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "LeftNameLabel", "LeftNameLabel", UILabel)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "RightNameLabel", "RightNameLabel", UILabel)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "Heart", "Heart", UILabel)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "PreviewTexture", "PreviewTexture", CUITexture)
RegistChildComponent(LuaSendShowLoveFireWorkWnd, "GetMask", "GetMask", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaSendShowLoveFireWorkWnd,"m_SelectPlayerId")
RegistClassMember(LuaSendShowLoveFireWorkWnd,"m_SelectPlayerName")
RegistClassMember(LuaSendShowLoveFireWorkWnd,"m_ItemId")
RegistClassMember(LuaSendShowLoveFireWorkWnd,"m_TextTexture")
function LuaSendShowLoveFireWorkWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.FriendNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFriendNodeClick()
	end)


	
	UIEventListener.Get(self.LightFireBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLightFireBtnClick()
	end)


	
	UIEventListener.Get(self.GetMask.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetMaskClick()
	end)


    --@endregion EventBind end
	self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)
end

function LuaSendShowLoveFireWorkWnd:Init()
	local player = CClientMainPlayer.Inst
	if not player then
		return
	end
	self.LeftNameLabel.text = player.RealName
	self.RightNameLabel.text = player.RealName
	self.Heart.text = LocalString.GetString(" ♥ ")
	--init item
	local setting = YuanXiao_FireworkPartySetting.GetData()
	local id = setting.ConfessFireworkItemID
	self.m_ItemId = id
	self.FireworkIcon:LoadMaterial(Item_Item.GetData(id).Icon)
	local needCount = setting.ConfessFireworkComposeNum

	local ownCount = 0
	local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(id)
	ownCount = bindCount + notBindCount
	local colorStr = ownCount >= needCount and "[00ff00]" or "[ff0000]"
	self.FireworkCountLabel.text = SafeStringFormat3("%s%d[-]/%d",colorStr,ownCount,needCount)

	local confessFireworkAddFriendliness = setting.ConfessFireworkAddFriendliness
	self.FriendshipLabel.text = SafeStringFormat3(LocalString.GetString("可增加%d友好度"),confessFireworkAddFriendliness)
	self.GetMask.gameObject:SetActive(ownCount<=0)

	--local text = SafeStringFormat3(LocalString.GetString("%s ♥      %s"),player.RealName,player.RealName)
	--self.m_TextTexture = self:CreateTextTexture(text,Color.red)

	self.PreviewTexture.mainTexture = CUIManager.CreateModelTexture("__LoveFire_Preview__", self.m_ModelTextureLoader,0,0.05,-5,10,false,true,1)
	self:PreviewCustomFx()
	self.cancelTick = CTickMgr.Register(DelegateFactory.Action(function () 
        self:PreviewCustomFx()
    end), 3000, ETickType.Loop)--3000

end

function LuaSendShowLoveFireWorkWnd:OnEnable()
    g_ScriptEvent:AddListener("SelectFriendToSendLoveFirework",self,"OnSelectFriendToSendLoveFirework")
	g_ScriptEvent:AddListener("SendItem",self,"Init")
end

function LuaSendShowLoveFireWorkWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SelectFriendToSendLoveFirework",self,"OnSelectFriendToSendLoveFirework")
	g_ScriptEvent:RemoveListener("SendItem",self,"Init")
	if self.cancelTick ~= nil then
		UnRegisterTick(self.cancelTick)
	end

	self.PreviewTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__LoveFire_Preview__")
end

function LuaSendShowLoveFireWorkWnd:LoadModel(ro) 
	self.m_Camera = ro.transform.parent.parent:GetComponent(typeof(Camera))
    self.m_Camera.farClipPlane = 30
	self.m_Camera.transform.rotation = Quaternion.Euler(0,0,0)

    self.m_Pos = ro.transform.position
    self.m_Pos2 = Vector3(self.m_Pos.x,self.m_Pos.y+10,self.m_Pos.z)
    self.m_RO = ro

    self:CancelTick()
end

function LuaSendShowLoveFireWorkWnd:CancelTick()
    if self.cancelTick ~= nil then
        invoke(self.cancelTick)
        self.cancelTick = nil
    end
end

function LuaSendShowLoveFireWorkWnd:PreviewCustomFx()
	local pos = self.m_Pos
	local onLoadFinish = DelegateFactory.Action_GameObject(function(go)
		local multyEffects = CommonDefs.GetComponentsInChildren_Component_Type(go.transform, typeof(CCustomParticleMultyEffect))
		if multyEffects then
			local multyEffect = multyEffects[0]
            if multyEffect and multyEffect.customEffect then
				multyEffect.customEffect.sampleTexture = nil
			end
			
		end
	end)

	local yuanhua = CEffectMgr.Inst:AddWorldPositionFX(88802934, Vector3(pos.x,pos.y+5,pos.z), 90, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, onLoadFinish)
end

function LuaSendShowLoveFireWorkWnd:OnSelectFriendToSendLoveFirework(playerId,playerName,portraitName)
	self.m_SelectPlayerId = playerId
	self.m_SelectPlayerName = playerName
	self.FriendIcon:LoadNPCPortrait(portraitName)
	self.NameLabel.text = playerName
	self.AddSprite.gameObject:SetActive(false)
	self.RightNameLabel.text = playerName
end
--@region UIEvent

function LuaSendShowLoveFireWorkWnd:OnFriendNodeClick()
	LuaFireWorkPartyMgr.SelectFriendOpenForFP = true
	CUIManager.ShowUI("ZhongQiuSelectFriendWnd")
end

function LuaSendShowLoveFireWorkWnd:OnLightFireBtnClick()
	if not self.m_SelectPlayerId or not self.m_SelectPlayerName then
		g_MessageMgr:ShowMessage("FireworkParty_PleaseSelectFriend_First")
	else
		Gac2Gas.RequestSetOffConfessFirework(self.m_SelectPlayerId,self.m_SelectPlayerName)
	end
end

function LuaSendShowLoveFireWorkWnd:OnGetMaskClick()
	--	@策划配表
	CItemAccessListMgr.Inst:ShowItemAccessInfo(self.m_ItemId, true, nil, AlignType1.Right)
end

function LuaSendShowLoveFireWorkWnd:CreateTextTexture(text,textColor)
	local target = CUIManager.s_GlobalFont
    local drawOffsetX,drawOffsetY,textGap,spaceGap,rowHeight=0,0,2,2,50 
    local texture = CCustomParticleEffect.TextToTexture(target,text,textColor,0,0,0,0,2,2,50)
    return texture
end
--@endregion UIEvent

