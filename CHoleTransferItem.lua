-- Auto Generated!!
local CHoleTransferItem = import "L10.UI.CHoleTransferItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
local Extensions = import "Extensions"
CHoleTransferItem.m_Init_CS2LuaHook = function (this, itemId, selected) 

    this.highlightBgSprite:SetActive(selected)
    this.highlightItemCellSprite:SetActive(selected)
    this.itemIcon:Clear()
    this.itemBindSprite.enabled = false
    this.itemCellBorder.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.holeCountLabel.text = "0"
    Extensions.RemoveAllChildren(this.holeTable.transform)
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.IsEquip then
        this.itemIcon:LoadMaterial(item.Icon)
        this.itemBindSprite.enabled = item.IsBinded
        this.itemCellBorder.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
        this.holeCountLabel.text = tostring(item.Equip.Hole)
        do
            local i = 1
            while i <= item.Equip.Hole do
                if i <= item.Equip.HoleItems.Length then
                    local hole = CUICommonDef.AddChild(this.holeTable.gameObject, this.holeTpl)
                    hole:SetActive(true)
                else
                    break
                end
                i = i + 1
            end
        end
        this.holeTable:Reposition()
    end

    this.addSprite:SetActive(item == nil and selected)
end
