local GameObject = import "UnityEngine.GameObject"
local CTooltip = import "L10.UI.CTooltip"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local TouchPhase = import "UnityEngine.TouchPhase"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaQiXi2022ReturnGoodsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "Portrait1", "Portrait1", CUITexture)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "NameLabel1", "NameLabel1", UILabel)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "Portrait2", "Portrait2", CUITexture)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "NameLabel2", "NameLabel2", UILabel)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "GoodsView", "GoodsView", GameObject)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "NPC1", "NPC1", GameObject)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "NPC2", "NPC2", GameObject)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "NPC3", "NPC3", GameObject)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "GoodsTemplate", "GoodsTemplate", GameObject)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaQiXi2022ReturnGoodsWnd, "GuangFx", "GuangFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_IsDragging")
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_LastDragPos")
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_FingerIndex")
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_SelectItemId")
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_SelectObj")
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_NpcDataArr")
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_ItemDict")
RegistClassMember(LuaQiXi2022ReturnGoodsWnd, "m_UpdateTimeLabelTick")

function LuaQiXi2022ReturnGoodsWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaQiXi2022ReturnGoodsWnd:Init()
    self.GuangFx:LoadFx("fx/ui/prefab/UI_mgd_suifeng_guanglizi.prefab")
    self.TimeLabel.text = ""
    self:InitTopLabel()
    self.GoodsTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.GoodsView.transform)
    local npcSet = {}
    self.m_ItemDict = {}
    if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo then
        for itemId, itemInfo in pairs(LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo) do
            local obj = NGUITools.AddChild(self.GoodsView.gameObject, self.GoodsTemplate.gameObject)
            obj.gameObject:SetActive(true)
            local val = QiXi2022_Classification.GetData(itemId)
            self:InitItem(obj, itemId, val)
            local npcid = val.Npcid
            npcSet[npcid] = true
            self.m_ItemDict[itemId] = obj
        end
    end
    local arr = {self.NPC1,self.NPC2,self.NPC3}
    local npcIndex = 1
    self.m_NpcDataArr = {}
    for npcid, _ in pairs(npcSet) do
        local npcData = NPC_NPC.GetData(npcid)
        if npcData then
            local npcRoot = arr[npcIndex]
            local tex = npcRoot:GetComponent(typeof(CUITexture))
            local nameLabel = npcRoot.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            tex:LoadNPCPortrait(npcData.Portrait)
            nameLabel.text = npcData.Name
            npcIndex = npcIndex + 1
            table.insert(self.m_NpcDataArr, npcData) 
        end
    end
    self:InitPortrait()
end

function LuaQiXi2022ReturnGoodsWnd:InitTopLabel()
    local itemCount = 0
    local finishedCount = 0
    if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo then
        for itemId, itemInfo in pairs(LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo) do
            if itemInfo == 2 then
                finishedCount = finishedCount + 1
            end
            itemCount = itemCount + 1
        end
    end
    self.TopLabel.text = g_MessageMgr:FormatMessage("QiXi2022ReturnGoodsWnd_TopLabel", finishedCount,itemCount)
    self:CancelTimeLabelTick()
    self.m_UpdateTimeLabelTick = RegisterTick(function ()
        if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.endTime then
            local t = LuaQiXi2022Mgr.m_ReturnGoodsWndData.endTime - CServerTimeMgr.Inst.timeStamp
            self.TimeLabel.text = SafeStringFormat3("%02d:%02d", math.floor(t % 3600 / 60),t % 60)
        end
    end,500)
end

function LuaQiXi2022ReturnGoodsWnd:CancelTimeLabelTick()
    if self.m_UpdateTimeLabelTick then
        UnRegisterTick(self.m_UpdateTimeLabelTick)
        self.m_UpdateTimeLabelTick = nil
    end
end

function LuaQiXi2022ReturnGoodsWnd:InitPortrait()
    Extensions.SetLocalPositionZ(self.Portrait1.transform, 0)
    Extensions.SetLocalPositionZ(self.Portrait2.transform, 0)
    self.Portrait2.alpha = 0.5
    if CClientMainPlayer.Inst then
        self.NameLabel1.text = CClientMainPlayer.Inst.Name
        self.Portrait1:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
    end
    if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.playerInfo then
        for playerId, playerInfo in pairs(LuaQiXi2022Mgr.m_ReturnGoodsWndData.playerInfo) do
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= playerId then
                local open, name, class, sex = playerInfo[1], playerInfo[2], tonumber(playerInfo[3]), tonumber(playerInfo[4])
                self.NameLabel2.text = name
                self.Portrait2:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, sex, -1))
                Extensions.SetLocalPositionZ(self.Portrait2.transform, open and 0 or -1)
                self.Portrait2.alpha = open and 1 or 0.5
            end
        end
    end
end

function LuaQiXi2022ReturnGoodsWnd:InitItem(obj, key, val)
    local npcid = val.Npcid
    local itemId = key
    local npcData = NPC_NPC.GetData(npcid)
    local itemData = Item_Item.GetData(itemId)
    obj.transform.localPosition = Vector3(val.Pos[0], val.Pos[1], 0)
    UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CTooltip.ShowAtTop(itemData.Name, obj.transform)
    end)
    UIEventListener.Get(obj.gameObject).onDragStart = DelegateFactory.VoidDelegate(function (go)
        if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo then
            local itemInfo = LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo[itemId]
            if itemInfo == 0 then
                self.m_IsDragging = true
                self.m_LastDragPos = Input.mousePosition
                self.m_SelectItemId = itemId
                self.m_SelectObj = obj
                if CommonDefs.IsInMobileDevice() then
                    self.m_FingerIndex = Input.GetTouch(0).fingerId
                    local pos = Input.GetTouch(0).position
                    self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                end
                Gac2Gas.SFEQ_WPFL_ClickOrReleaseItem(self.m_SelectItemId)
            else
                g_MessageMgr:ShowMessage("QiXi2022ReturnGoodsWnd_Drag_Failed",self.NameLabel2.text,itemData.Name)
            end
        end
    end)
    if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo then
        local itemInfo = LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo[itemId]
        local tex = obj:GetComponent(typeof(CUITexture))
        tex.alpha = itemInfo == 0 and 1 or 0.2
        obj.gameObject:SetActive(itemInfo ~= 2)
        tex:LoadMaterial(itemData.Icon)
    end
end

function LuaQiXi2022ReturnGoodsWnd:Update()
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end

    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        if self.m_SelectObj then
                            self.m_SelectObj.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                        end
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            if self.m_SelectObj then
                self.m_SelectObj.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            end
            self.m_LastDragPos = Input.mousePosition

            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self:ReturnGoods()

    self.m_IsDragging = false
    self.m_FingerIndex = -1
end

function LuaQiXi2022ReturnGoodsWnd:ReturnGoods()
    if not self.m_SelectObj then
        return
    end
    if not self.m_NpcDataArr then
        return
    end
    local pos1 = self.m_SelectObj.transform.localPosition
    local arr = {self.NPC1, self.NPC2, self.NPC3}
    local isRight = false
    local npcId = 0
    for i, npc in pairs(arr) do
        local pos2 = npc.transform.localPosition
        local dist = Vector3.Distance(pos1, pos2)
        local npcData = self.m_NpcDataArr[i]
        local itemId = self.m_SelectItemId
        local classification = QiXi2022_Classification.GetData(itemId) 
        if npcData and dist < 120 then
            isRight = true
            npcId = npcData.ID
            if classification.Npcid ~= npcData.ID then
                if self.m_SelectObj then
                    self:InitItem(self.m_SelectObj, self.m_SelectItemId, QiXi2022_Classification.GetData(self.m_SelectItemId))
                end
            end
        end
    end
    if isRight then
        Gac2Gas.SFEQ_WPFL_PutItemOnNPC(self.m_SelectItemId,npcId)
    else
        Gac2Gas.SFEQ_WPFL_ClickOrReleaseItem(self.m_SelectItemId)
        if self.m_SelectObj then
            self:InitItem(self.m_SelectObj, self.m_SelectItemId, QiXi2022_Classification.GetData(self.m_SelectItemId))
        end
    end
end
--@region UIEvent

--@endregion UIEvent

function LuaQiXi2022ReturnGoodsWnd:OnEnable()
    g_ScriptEvent:AddListener("SFEQ_WPFL_PlayerOpenWnd", self, "OnPlayerOpenWnd")
    g_ScriptEvent:AddListener("SFEQ_WPFL_PlayerCloseWnd", self, "OnPlayerCloseWnd")
    g_ScriptEvent:AddListener("SFEQ_WPFL_SyncClickOrReleaseItemResult", self, "OnSyncClickOrReleaseItemResult")
    g_ScriptEvent:AddListener("SFEQ_WPFL_PutItemSuccess", self, "OnPutItemSuccess")
    g_ScriptEvent:AddListener("SFEQ_WPFL_ClickItemFail", self, "OnClickItemFail")
end

function LuaQiXi2022ReturnGoodsWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SFEQ_WPFL_PlayerOpenWnd", self, "OnPlayerOpenWnd")
    g_ScriptEvent:RemoveListener("SFEQ_WPFL_PlayerCloseWnd", self, "OnPlayerCloseWnd")
    g_ScriptEvent:RemoveListener("SFEQ_WPFL_SyncClickOrReleaseItemResult", self, "OnSyncClickOrReleaseItemResult")
    g_ScriptEvent:RemoveListener("SFEQ_WPFL_PutItemSuccess", self, "OnPutItemSuccess")
    g_ScriptEvent:RemoveListener("SFEQ_WPFL_ClickItemFail", self, "OnClickItemFail")
    self:CancelTimeLabelTick()
    Gac2Gas.SFEQ_WPFL_RequestCloseWnd()
end

function LuaQiXi2022ReturnGoodsWnd:OnPlayerOpenWnd(playerId)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= playerId then
        Extensions.SetLocalPositionZ(self.Portrait2.transform, 0)
        self.Portrait2.alpha = 1
    end
end

function LuaQiXi2022ReturnGoodsWnd:OnPlayerCloseWnd(playerId)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= playerId then
        Extensions.SetLocalPositionZ(self.Portrait2.transform, -1)
        self.Portrait2.alpha = 0.5
    end
end

function LuaQiXi2022ReturnGoodsWnd:OnSyncClickOrReleaseItemResult(itemId, bCanClick, playerId)
    if  CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id then
        return
    end
    if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo then
        local itemInfo = LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo[itemId]
        if itemInfo and itemInfo ~= 2 then
            LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo[itemId] = (bCanClick or self.m_SelectItemId == itemId) and 0 or 1
        end
    end
    local obj = self.m_ItemDict[itemId]
    if obj  then
        self:InitItem(obj , itemId, QiXi2022_Classification.GetData(itemId))
    end
end

function LuaQiXi2022ReturnGoodsWnd:OnPutItemSuccess(itemId)
    if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo then
        LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo[itemId] = 2
    end
    local obj = self.m_ItemDict[itemId]
    if obj  then
        local data = QiXi2022_Classification.GetData(itemId)
        self:InitItem(obj , itemId, data)
        local arr = {self.NPC1, self.NPC2, self.NPC3}
        for i, npc in pairs(arr) do
            local npcData = self.m_NpcDataArr[i]
            if npcData.ID == data.Npcid then
                local fx = npc.transform:Find("Fx"):GetComponent(typeof(CUIFx))
                fx.gameObject:SetActive(false)
                fx:LoadFx("fx/ui/prefab/UI_mgd_guihuan_zhengque.prefab")
                fx.gameObject:SetActive(true)
                break
            end
        end
    end
    self:InitTopLabel()
end

function LuaQiXi2022ReturnGoodsWnd:OnClickItemFail(itemId)
    local itemId = self.m_SelectItemId
    if LuaQiXi2022Mgr.m_ReturnGoodsWndData and LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo then
        LuaQiXi2022Mgr.m_ReturnGoodsWndData.itemInfo[itemId] = 1
    end
    local obj = self.m_ItemDict[itemId]
    if obj  then
        self:InitItem(obj , itemId, QiXi2022_Classification.GetData(itemId))
    end
    if self.m_SelectItemId == itemId then
        self.m_IsDragging = false
        self.m_SelectItemId = 0
        self.m_SelectObj = nil
    end
end