local CLoginMgr = import "L10.Game.CLoginMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local DelegateFactory = import "DelegateFactory"
local DRPFMgr = import "L10.Game.DRPFMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local WWW = import "UnityEngine.WWW"
local Main = import "L10.Engine.Main"

g_ShowAppUpgradeNotifyCallback = {}
g_LoginShowAppUpgradeNotifyCallback = {}

CLuaUpgradeNotifyMgr = {}
function CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify(response)
	Gac2Gas.ResponseAppUpgradeNotify(response, tostring(Main.Inst.EngineVersion), tostring(CommonDefs.GetUnityVersion()))
end

g_ShowAppUpgradeNotifyCallback["appver_notify"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 3 then
		local message = list[0]
		local url = list[1]
		local isPcLogin = list[2]
		local isDownload = url and url ~= ""
		if isPcLogin then
			message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("ANTIADDICTION_PCLOGIN_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			MessageWndManager.ShowOKMessage(message, nil)
		else
			message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("ANTIADDICTION_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("appver_notify_ok")
					CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
				end
			end),
			DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("appver_notify_cancel")
				end
			end), isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		end
	end
end

g_ShowAppUpgradeNotifyCallback["appver_notify_force"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 3 then
		local message = list[0]
		local url = list[1]
		local isPcLogin = list[2]
		local isDownload = url and url ~= "" or false
		if isPcLogin then
			message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("ANTIADDICTION_PCLOGIN_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			MessageWndManager.ShowOKMessage(message, nil)
		else
			message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("ANTIADDICTION_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			MessageWndManager.ShowOKMessage(message, isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("appver_notify_force_ok")
					CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
				end
			end))
		end
	end
end

g_ShowAppUpgradeNotifyCallback["unity2018_notify"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 5 then
		local message = list[0]
		local url = list[1]
		local size = list[2]
		local isPcLogin = list[3]
		local isPcUpgrade = list[4]
		local isDownload = url and url ~= ""
		if isPcLogin and not isPcUpgrade then
			message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_PCLOGIN_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			MessageWndManager.ShowOKMessage(message, nil)
		else
			if isPcLogin and isPcUpgrade then
				message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_PC_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			else
				message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			end
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("unity2018_notify_ok")
					CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
				end
			end),
			DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("unity2018_notify_cancel")
				end
			end), isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		end
	end
end

g_ShowAppUpgradeNotifyCallback["unity2018_notify_force"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 5 then
		local message = list[0]
		local url = list[1]
		local size = list[2]
		local isPcLogin = list[3]
		local isPcUpgrade = list[4]
		local isDownload = url and url ~= "" or false
		if isPcLogin and not isPcUpgrade then
			message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_PCLOGIN_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			MessageWndManager.ShowOKMessage(message, nil)
		else
			if isPcLogin and isPcUpgrade then
				message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_PC_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			else
				message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			end
			MessageWndManager.ShowOKMessage(message, isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("unity2018_notify_force_ok")
					CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
				end
			end))
		end
	end
end

g_ShowAppUpgradeNotifyCallback["unity2018_notify_v2"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 6 then
		local message = list[0]
		local url = list[1]
		local size = list[2]
		local isPcLogin = list[3]
		local isPcUpgrade = list[4]
		local surveyUrl = list[5]
		local isDownload = url and url ~= ""
		if isPcLogin and not isPcUpgrade then
			message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_PCLOGIN_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			MessageWndManager.ShowOKMessage(message, nil)
		else
			if isPcLogin and isPcUpgrade then
				message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_PC_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			else
				message = cs_string.IsNullOrEmpty(message) and g_MessageMgr:FormatMessage("UNITY2018_UPGRADE_NOTIFY_AFTER_LOGIN") or message
			end
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("unity2018_notify_v2_ok")
					CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
				end
			end),
			DelegateFactory.Action(function ()
				if isDownload then
					CLuaUpgradeNotifyMgr.ResponseAppUpgradeNotify("unity2018_notify_v2_cancel")
					if surveyUrl and surveyUrl ~= "" then
						if CWebBrowserMgr.s_EnableAutoLogin and (StringStartWith(surveyUrl, "https://qnm.163.com") or StringStartWith(surveyUrl, "https://test.nie.163.com")) and CClientMainPlayer.Inst and not cs_string.IsNullOrEmpty(CPersonalSpaceMgr.Inst.Token) then
							local urlFormat = "%sqnm/activity/login?skey=%s&roleid=%s&redirect_url=%s&login_channel=%s"
							surveyUrl = string.format(urlFormat, CWebBrowserMgr.s_ApiUrl, CPersonalSpaceMgr.Inst.Token, CClientMainPlayer.Inst.Id, WWW.EscapeURL(surveyUrl), CLoginMgr.Inst:GetLoginChannelConverted())
						end
						CommonDefs.OpenURL(surveyUrl)
					end
				end
			end), isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		end
	end
end

g_LoginShowAppUpgradeNotifyCallback["appver_notify"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 3 then
		local message = list[0]
		local url = list[1]
		local isPcLogin = list[2]
		local isDownload = url and url ~= ""
        if isPcLogin and cs_string.IsNullOrEmpty(message) then
            message = g_MessageMgr:FormatMessage("ANTIADDICTION_PCLOGIN_UPGRADE_NOTIFY_LOGIN")
		end
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
			if isDownload then
				CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
			end
		end), nil, isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	end
end

g_LoginShowAppUpgradeNotifyCallback["appver_notify_force"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 3 then
		local message = list[0]
		local url = list[1]
		local isPcLogin = list[2]
		local isDownload = url and url ~= ""
        if isPcLogin and cs_string.IsNullOrEmpty(message) then
			message = g_MessageMgr:FormatMessage("ANTIADDICTION_PCLOGIN_UPGRADE_NOTIFY_LOGIN")
		end
		MessageWndManager.ShowOKMessage(message, isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), DelegateFactory.Action(function ()
			if isDownload then
				CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
			end
		end))
	end
end

g_LoginShowAppUpgradeNotifyCallback["appver_force_upgrade"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 4 then
		local message = list[0]
		local url = list[1]
		local isPcLogin = list[2]
		local stage = list[3]
		local isDownload = url and url ~= ""
		if isPcLogin then
			if stage == 1 then
				if cs_string.IsNullOrEmpty(message) then
					message = g_MessageMgr:FormatMessage("ANTIADDICTION_PCLOGIN_FORCE_UPGRADE_ROLEENTER")
				end
				MessageWndManager.ShowOKMessage(message, nil)
			else
				if cs_string.IsNullOrEmpty(message) then
					message = g_MessageMgr:FormatMessage("ANTIADDICTION_PCLOGIN_FORCE_UPGRADE_LOGIN")
				end
				MessageWndManager.ShowOKMessage(message, isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), DelegateFactory.Action(function ()
					if isDownload then
						DRPFMgr.Inst:RecordForceDownload(type, url, "ok")
						CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
					end
				end))
			end
		else 
			if cs_string.IsNullOrEmpty(message) then
				message = g_MessageMgr:FormatMessage("ANTIADDICTION_FORCE_UPGRADE_ROLEENTER")
			end
			MessageWndManager.ShowOKMessage(message, isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), DelegateFactory.Action(function ()
				if isDownload then
					DRPFMgr.Inst:RecordForceDownload(type, url, "ok")
					CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
				end
			end))
		end
	end
end

g_LoginShowAppUpgradeNotifyCallback["unity2018_force_upgrade"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 6 then
		local message = list[0]
		local url = list[1]
		local isPcLogin = list[2]
		local surveyUrl = list[3]
		local inAppUpgrade = list[4]
		local inAppUpgradeUrl = list[5]
		local isDownload = url and url ~= ""
		local isInAppUpgrade = inAppUpgrade == 1 and inAppUpgradeUrl and inAppUpgradeUrl ~= ""
		if isPcLogin then
			if cs_string.IsNullOrEmpty(message) then
				message = g_MessageMgr:FormatMessage("UNITY2018_PCLOGIN_FORCE_UPGRADE_LOGIN")
			end
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
				if isDownload then
					if isInAppUpgrade and CommonDefs.IsAndroidPlatform() and Main.Inst.EngineVersion >= 427168 then
						DRPFMgr.Inst:RecordForceDownload(type, inAppUpgradeUrl, "inapp_ok")
						LuaLoginMgr.TryDownloadNewVersion(type, inAppUpgradeUrl)
					else
						DRPFMgr.Inst:RecordForceDownload(type, url, "ok")
						CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
					end
				end
			end), 
			DelegateFactory.Action(function ()
				if isDownload then
					DRPFMgr.Inst:RecordForceDownload(type, url, "cancel")
					if surveyUrl and surveyUrl ~= "" then
						CommonDefs.OpenURL(surveyUrl)
					end
				end
			end), isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		else 
			if cs_string.IsNullOrEmpty(message) then
				message = g_MessageMgr:FormatMessage("UNITY2018_FORCE_UPGRADE_LOGIN")
			end
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
				if isDownload then
					if isInAppUpgrade and CommonDefs.IsAndroidPlatform() and Main.Inst.EngineVersion >= 427168 then
						DRPFMgr.Inst:RecordForceDownload(type, inAppUpgradeUrl, "inapp_ok")
						LuaLoginMgr.TryDownloadNewVersion(type, inAppUpgradeUrl)
					else
						DRPFMgr.Inst:RecordForceDownload(type, url, "ok")
						CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
					end
				end
			end), 
			DelegateFactory.Action(function ()
				if isDownload then
					DRPFMgr.Inst:RecordForceDownload(type, url, "cancel")
					if surveyUrl and surveyUrl ~= "" then
						CommonDefs.OpenURL(surveyUrl)
					end
				end
			end), isDownload and LocalString.GetString("下载") or LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		end
	end
end

g_LoginShowAppUpgradeNotifyCallback["unity2018_notify"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 4 then
		local message = list[0]
		local url = list[1]
		local size = list[2]
		local isPcLogin = list[3]
		if cs_string.IsNullOrEmpty(url) then
			if cs_string.IsNullOrEmpty(message) then
				g_MessageMgr:ShowMessage("UNITY2018_PCLOGIN_UPGRADE_NOTIFY_LOGIN_NO_URL")
			else
				g_MessageMgr:ShowMessage("CUSTOM_STRING", message)
			end
		else
			if isPcLogin and cs_string.IsNullOrEmpty(message) then
				message = g_MessageMgr:FormatMessage("UNITY2018_PCLOGIN_UPGRADE_NOTIFY_LOGIN")
			end
			MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
				CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
			end), nil, LocalString.GetString("下载"), LocalString.GetString("取消"), false)
		end
	end
end

g_LoginShowAppUpgradeNotifyCallback["unity2018_notify_force"] = function(type, content_U)
	local list = MsgPackImpl.unpack(content_U)
	if list and list.Count >= 4 then
		local message = list[0]
		local url = list[1]
		local size = list[2]
		local isPcLogin = list[3]
		if cs_string.IsNullOrEmpty(url) then
			if cs_string.IsNullOrEmpty(message) then
				g_MessageMgr:ShowMessage("UNITY2018_PCLOGIN_UPGRADE_NOTIFY_LOGIN_NO_URL")
			else
				g_MessageMgr:ShowMessage("CUSTOM_STRING", message)
			end
		else
			if isPcLogin and cs_string.IsNullOrEmpty(message) then
				message = g_MessageMgr:FormatMessage("UNITY2018_PCLOGIN_UPGRADE_NOTIFY_LOGIN")
			end
			MessageWndManager.ShowOKMessage(message, LocalString.GetString("下载"), DelegateFactory.Action(function ()
				CommonDefs.OpenURL(CLoginMgr.CdnUrlEncrypt(url))
			end))
		end
	end
end
