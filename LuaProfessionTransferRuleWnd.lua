require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CProfessionTransferMgr = import "L10.Game.CProfessionTransferMgr"

LuaProfessionTransferRuleWnd = class()
RegistClassMember(LuaProfessionTransferRuleWnd,"closeBtn")
RegistClassMember(LuaProfessionTransferRuleWnd,"scrollView")
RegistClassMember(LuaProfessionTransferRuleWnd,"ruleLabel")
RegistClassMember(LuaProfessionTransferRuleWnd,"nextStepBtn")
RegistClassMember(LuaProfessionTransferRuleWnd,"agreeCheckbox")

function LuaProfessionTransferRuleWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaProfessionTransferRuleWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
	self.scrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
	self.ruleLabel = self.transform:Find("Anchor/ScrollView/TextLabel"):GetComponent(typeof(UILabel))
	self.nextStepBtn = self.transform:Find("Anchor/NextStepBtn"):GetComponent(typeof(CButton))
	self.agreeCheckbox = self.transform:Find("Anchor/AgreeCheckbox"):GetComponent(typeof(QnCheckBox))
	
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.nextStepBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnNextStepButtonClick(go) end), false)
	self.agreeCheckbox.OnValueChanged = DelegateFactory.Action_bool(function(value) self:OnAgreeCheckboxValueChanged(value) end)
	
	self.agreeCheckbox:SetSelected(false, true)
	self.nextStepBtn.Enabled = false
	self.ruleLabel.text = g_MessageMgr:FormatMessage("ProfessionTransfer_Description1")
end

function LuaProfessionTransferRuleWnd:OnNextStepButtonClick(go)
	CUIManager.ShowUI("ProfessionTransferCharacterWnd")
	self:Close()
end

function LuaProfessionTransferRuleWnd:OnAgreeCheckboxValueChanged(value)
	 self.nextStepBtn.Enabled = value
end
return LuaProfessionTransferRuleWnd