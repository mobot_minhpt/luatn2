-- Auto Generated!!
local CGuildTrainItemCell = import "L10.UI.CGuildTrainItemCell"
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local CGuildTrainWindows = import "L10.UI.CGuildTrainWindows"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GuildTrainItemBaseInfo = import "L10.Game.GuildTrainItemBaseInfo"
local NGUITools = import "NGUITools"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UInt32 = import "System.UInt32"
CGuildTrainWindows.m_Init_CS2LuaHook = function (this, categoryIndex) 
    this.categoryIndex = categoryIndex
    CommonDefs.ListClear(this.itemList)
    Extensions.RemoveAllChildren(this.table.transform)
    this.trainItemCell:SetActive(false)
    local baseInfoList = CGuildTrainMgr.Inst.trainCategoryList[categoryIndex].trainItemList
    do
        local i = 0
        while i < baseInfoList.Count do
            local trainItem = NGUITools.AddChild(this.table.gameObject, this.trainItemCell)
            trainItem:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(trainItem, typeof(CGuildTrainItemCell)).OnTrainButtonClicked = CommonDefs.CombineListner_Action_uint_GuildTrainItemBaseInfo_GameObject(CommonDefs.GetComponent_GameObject_Type(trainItem, typeof(CGuildTrainItemCell)).OnTrainButtonClicked, MakeDelegateFromCSFunction(this.OnTrainButtonClicked, MakeGenericClass(Action3, UInt32, GuildTrainItemBaseInfo, GameObject), this), true)
            CommonDefs.GetComponent_GameObject_Type(trainItem, typeof(CGuildTrainItemCell)):Init(categoryIndex, i)
            local button = CommonDefs.GetComponent_GameObject_Type(trainItem, typeof(QnSelectableButton))
            CommonDefs.ListAdd(this.itemList, typeof(QnSelectableButton), button)
            button.OnClick = CommonDefs.CombineListner_Action_QnButton(button.OnClick, MakeDelegateFromCSFunction(this.OnTrainItemClicked, MakeGenericClass(Action1, QnButton), this), true)
            button:SetSelected(false, false)
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollview:ResetPosition()
    if this.itemList.Count > 0 then
        local itemCell = CommonDefs.GetComponent_Component_Type(this.itemList[0], typeof(CGuildTrainItemCell))
        if itemCell ~= nil then
            this:OnTrainButtonClicked(itemCell.maxLevel, itemCell.baseInfo, itemCell.gameObject)
            this:OnTrainItemClicked(this.itemList[0])
        end
    end
end
CGuildTrainWindows.m_OnTrainButtonClicked_CS2LuaHook = function (this, maxLevel, baseInfo, go) 
    do
        local i = 0
        while i < this.itemList.Count do
            --itemList[i].SetSelected(false);
            if go == this.itemList[i].gameObject then
                this.detailView:Init(maxLevel, baseInfo)
                --itemList[i].SetSelected(true);
            end
            i = i + 1
        end
    end
end
CGuildTrainWindows.m_OnTrainItemClicked_CS2LuaHook = function (this, button) 
    do
        local i = 0
        while i < this.itemList.Count do
            this.itemList[i]:SetSelected(button == this.itemList[i], false)
            --if(button == itemList[i])
            --{
            --CSkillInfoMgr.ShowPracticeSkillInfoWnd(
            --    itemList[i].GetComponent<CGuildTrainItemCell>().PracticeSkillId,
            --    itemList[i].transform.position);
            --}
            i = i + 1
        end
    end
end
