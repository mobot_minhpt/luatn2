-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouWashCost = import "L10.UI.CLingShouWashCost"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventDelegate = import "EventDelegate"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local UIEventListener = import "UIEventListener"
CLingShouWashCost.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.getNode).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemAccessListMgr.Inst:ShowItemAccessInfo(Constants.BiLiuLuId, false, p.transform, AlignType.Right)
    end)
    --addBtn.SetActive(false);
    --UIEventListener.Get(addBtn).onClick = p =>
    --{
    --    CShopMallMgr.ShowChargeWnd();
    --};

    this.countUpdate.templateId = Constants.BiLiuLuId
    this.countUpdate.excludeExpireTime = true
    --碧柳露名字
    this.TemplateId = Constants.BiLiuLuId
    local template = CItemMgr.Inst:GetItemTemplate(Constants.BiLiuLuId)
    if template ~= nil then
        this.icon:LoadMaterial(template.Icon)
        this.nameLabel.text = template.Name
    else
        this.nameLabel.text = ""
        this.icon.material = nil
    end

    this.countUpdate.format = "{0}/1"
    this.countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        if val == 0 then
            this.countUpdate.format = "[ff0000]{0}[-]/1"
            this.getNode:SetActive(true)
        else
            this.countUpdate.format = "{0}/1"
            this.getNode:SetActive(false)
        end
        this:UpdateDisplay()
    end)
    this.countUpdate:UpdateCount()

    --碧柳露价格
    do
        local data = Mall_LingYuMall.GetData(Constants.BiLiuLuId)
        this.jadeCost = data.Jade
        this.moneyCtrl:SetType(data.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
        this.moneyCtrl:SetCost(this.jadeCost)
        --costJadeLabel.text = data.Jade.ToString();// string.Format("{0}灵玉自动购买", data.Jade);
    end


    this.autoBuyToggle.value = CLingShouWashCost.autoBuy
    CommonDefs.ListAdd(this.autoBuyToggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        CLingShouWashCost.autoBuy = this.autoBuyToggle.value

        this:UpdateDisplay()
    end)))
end
CLingShouWashCost.m_UpdateDisplay_CS2LuaHook = function (this) 
    if CLingShouWashCost.autoBuy then
        if this.countUpdate.count > 0 then
            this.jadeNode.gameObject:SetActive(false)
        else
            this.jadeNode.gameObject:SetActive(true)
        end
    else
        this.jadeNode.gameObject:SetActive(false)
    end
end
