-- Auto Generated!!
local Application = import "UnityEngine.Application"
local Byte = import "System.Byte"
local Camera = import "UnityEngine.Camera"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCoroutineMgr = import "L10.Engine.CCoroutineMgr"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CGuideWnd = import "L10.UI.CGuideWnd"
local CLogMgr = import "L10.CLogMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CStarItem = import "L10.UI.CStarItem"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUIConstellation = import "L10.UI.CUIConstellation"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local CXingGuanWnd = import "L10.UI.CXingGuanWnd"
local CXingGuanWndMgr = import "L10.UI.CXingGuanWndMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local File = import "System.IO.File"
local GameObject = import "UnityEngine.GameObject"
local GenericGesture = import "L10.Engine.GenericGesture"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local Input = import "UnityEngine.Input"
local L10 = import "L10"
local LineRenderer = import "UnityEngine.LineRenderer"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local NGUITools = import "NGUITools"
local Object = import "UnityEngine.Object"
local Physics = import "UnityEngine.Physics"
local PlayerSettings = import "L10.Game.PlayerSettings"
local SoundManager = import "SoundManager"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local Time = import "UnityEngine.Time"
local TweenAlpha = import "TweenAlpha"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UIWidget = import "UIWidget"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Xingguan_StarGroup = import "L10.Game.Xingguan_StarGroup"
local Constants = import "L10.Game.Constants"

CXingGuanWnd.m_Awake_CS2LuaHook = function (this)
    CXingGuanWnd.Inst = this
    this.m_ConstellationWnd.gameObject:SetActive(false)
    this.m_InsideUIRoot:SetActive(false)
    this.m_OverviewUIRoot:SetActive(true)
    this.m_PoleTex:SetActive(false)
    this.m_StarObj:SetActive(false)
    this.m_ConstellationNameObj:SetActive(false)
    this.m_ConstellationWnd.gameObject:SetActive(false)
    this.m_ConstellationObj:SetActive(false)
    this.m_LineObj:SetActive(false)
    this.m_BgStarTex.gameObject:SetActive(false)

    this.m_SphereRoot = CreateFromClass(GameObject, "__XingGuan__")
    this.m_SphereRoot.transform.parent = CUIManager.instance.transform
    Extensions.SetLocalPositionX(this.m_SphereRoot.transform, - 20)
    local sphereObj = CommonDefs.Object_Instantiate(this.m_SphereObj)
    sphereObj:SetActive(true)
    this.m_SphereTrans = sphereObj.transform
    this.m_SphereTrans.parent = this.m_SphereRoot.transform
    this.m_SphereTrans.localPosition = Vector3.zero
    this.m_SphereTrans.localScale = Vector3.one
    this.m_Camera = CommonDefs.GetComponentInChildren_GameObject_Type(sphereObj, typeof(Camera))
    this.m_CameraTrans = this.m_Camera.transform
    this.m_WaterObj = sphereObj.transform:Find("Water").gameObject
    local mr = CommonDefs.GetComponent_GameObject_Type(this.m_WaterObj, typeof(MeshRenderer))
    this.m_WaterMaterial = mr.material
    this.m_WaterColor = this.m_WaterMaterial:GetColor("_WaterColor")
    this.m_WaterAlpha = this.m_WaterColor.a
    this.m_WaterObj:SetActive(false)
    this.m_XingMangTemplate.gameObject:SetActive(false)

    if PlayerSettings.MusicEnabled then
        SoundManager.Inst:StopBGMusic()
        this.m_BgMusic = SoundManager.Inst:PlaySound(SoundManager.s_XingGuanMusic, Vector3.zero, 1, nil, 0)
    end

    CCoroutineMgr.Inst:StartCoroutine(this:LoadFx(CXingGuanWnd.SwipeFxPath, DelegateFactory.Action_GameObject(function (fx)
        this.m_SwipeFxObj = fx
        this.m_SwipeFxObj:SetActive(not this.m_InsideUIRoot.activeSelf)
    end)))
    
    CLuaXingGuanWnd:Init(this)
end
CXingGuanWnd.m_EnterConstellation_CS2LuaHook = function (this)
    if CXingGuanWndMgr.Inst.m_ConstellationId > 0 then
        this.m_OverviewUIRoot:SetActive(false)
        this.m_BeforeEnterCameraPos = this.m_CameraTrans.localPosition
        this:EnterInsideInternal()
        CLuaXingGuanWnd:OnCloseOverview()
        this:ShowConstellationById(CXingGuanWndMgr.Inst.m_ConstellationId)
        CXingGuanWndMgr.Inst.m_ConstellationId = 0
    end
end
CXingGuanWnd.m_FastEnterInside_CS2LuaHook = function (this, go)
    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(53) then
        L10.Game.Guide.CGuideMgr.Inst:TriggerGuide(5)
        CGuideWnd.Instance:HidePinchGuide()
    end

    this.m_OverviewUIRoot:SetActive(false)
    this.m_BeforeEnterCameraPos = this.m_CameraTrans.localPosition
    this:EnterInsideInternal()
    CLuaXingGuanWnd:OnCloseOverview()
end
CXingGuanWnd.m_IsStarLighted_CStar_CS2LuaHook = function (this, star)
    if star.m_ConstellationId <= 0 or nil == CClientMainPlayer.Inst then
        return false
    end
    if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), star.m_ConstellationId) then
        return false
    end
    if not CommonDefs.DictContains(this.m_Constellation2StarDict, typeof(UInt32), star.m_ConstellationId) then
        return false
    end
    local staredCount = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), star.m_ConstellationId).StaredCount
    if staredCount <= 0 then
        return false
    end
    local starList = CommonDefs.DictGetValue(this.m_Constellation2StarDict, typeof(UInt32), star.m_ConstellationId)
    do
        local i = 0 local cnt = starList.Count
        while i < cnt and i < staredCount do
            if starList[i].m_Id == star.m_Id then
                return true
            end
            i = i + 1
        end
    end
    return false
end
CXingGuanWnd.m_DrawAllLine_CS2LuaHook = function (this)
    Extensions.RemoveAllChildren(this.m_LineRoot)
    CommonDefs.DictIterate(this.m_StarKey2Index, DelegateFactory.Action_object_object(function (___key, ___value)
        local val = {}
        val.Key = ___key
        val.Value = ___value
        local continue
        repeat
            local startStar = this.m_StarList[val.Value]
            if startStar.m_LineTo == nil or startStar.m_LineTo.Length <= 0 then
                continue = true
                break
            end
            do
                local i = 0 local cnt = startStar.m_LineTo.Length
                while i < cnt do
                    local continue
                    repeat
                        if not CommonDefs.DictContains(this.m_StarKey2Index, typeof(UInt32), startStar.m_LineTo[i]) then
                            CLogMgr.Log((("Line Error : " .. val.Key) .. " to ") .. startStar.m_LineTo[i])
                            continue = true
                            break
                        end
                        local endStar = this.m_StarList[CommonDefs.DictGetValue(this.m_StarKey2Index, typeof(UInt32), startStar.m_LineTo[i])]
                        if not this:IsStarLighted(startStar) or not this:IsStarLighted(endStar) then
                            continue = true
                            break
                        end
                        this:DrawLine(this.m_SphereTrans:TransformPoint(startStar.m_LocalPos), this.m_SphereTrans:TransformPoint(endStar.m_LocalPos))
                        continue = true
                    until 1
                    if not continue then
                        break
                    end
                    i = i + 1
                end
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end))
    this.m_LineRoot.gameObject:SetActive(not this.m_IsInOverview)
    this.m_HasLine = true
end
CXingGuanWnd.m_OnStarClick_CS2LuaHook = function (this, go)
    if this.m_IsInOverview then
        return
    end
    if CommonDefs.DictContains(this.m_Star2Index, typeof(GameObject), go) and this.m_StarList[CommonDefs.DictGetValue(this.m_Star2Index, typeof(GameObject), go)].m_ConstellationId > 0 then
        local conKey = this.m_StarList[CommonDefs.DictGetValue(this.m_Star2Index, typeof(GameObject), go)].m_ConstellationId
        local isVisiable = this:IsConstellationVisiable(conKey)
        if isVisiable then
            return
        end
        local data = Xingguan_StarGroup.GetData(conKey)
        if nil == data then
            return
        end
        if data.Pos ~= nil and data.Pos.Length == 4 then
            this:GenerateConstellation(Vector2(data.Pos[0], data.Pos[1]), Vector2(data.Pos[2], data.Pos[3]), System.String.Format(CXingGuanWnd.IMAGE_PATTERN, data.ImagePath), conKey, false)
        end
        --m_ConstellationWnd.gameObject.SetActive(true);
        --m_ConstellationWnd.Init(m_Star2Index[go]);
        --m_CanOperate = false;
    end
end
CXingGuanWnd.m_UpdateUIConstellation_CS2LuaHook = function (this, con, isVisiable)
    local centerViewPos = this.m_Camera:WorldToViewportPoint(this.m_SphereTrans:TransformPoint(con.m_CenterPos))
    if centerViewPos.x < 0 or centerViewPos.y < 0 or centerViewPos.x > 1 or centerViewPos.y > 1 or centerViewPos.z < 0 then
        con.m_Tex.gameObject:SetActive(false)
        return false
    end
    -- visiable constellation
    if isVisiable then
        if centerViewPos.x < 0.35 then
            con.m_Tex.texture.alpha = (centerViewPos.x - 0.1) * 4
        elseif centerViewPos.x > 0.65 then
            con.m_Tex.texture.alpha = (1 - centerViewPos.x - 0.1) * 4
        else
            con.m_Tex.texture.alpha = 1
        end
    end

    local rightViewPos = this.m_Camera:WorldToViewportPoint(this.m_SphereTrans:TransformPoint(con.m_TRPos))
    con.m_Tex.gameObject:SetActive(true)
    con.m_Tex.transform.localPosition = Vector3(centerViewPos.x * this.m_LastSphereTexWidth - math.floor(this.m_LastSphereTexWidth / 2), centerViewPos.y * this.m_LastSphereTexHeight - math.floor(this.m_LastSphereTexHeight / 2), 0)
    --m_ConstellationTexList[i].texture.width = (int)(Mathf.Abs(rightViewPos.x - centerViewPos.x) * m_SphereTexWidth) * 2;
    --m_ConstellationTexList[i].texture.height = (int)(Mathf.Abs(rightViewPos.y - centerViewPos.y) * m_SphereTexHeight) * 2;
    local centerPos = Vector2(centerViewPos.x * this.m_SphereTexWidth, centerViewPos.y * this.m_SphereTexHeight)
    local rightPos = Vector2(rightViewPos.x * this.m_SphereTexWidth, rightViewPos.y * this.m_SphereTexHeight)
    local diff = CommonDefs.op_Subtraction_Vector2_Vector2(rightPos, centerPos)
    local texWidth = math.sqrt(diff.sqrMagnitude / 2) * 2
    con.m_Tex.texture.width = math.floor(texWidth)
    con.m_Tex.texture.height = math.floor(texWidth)
    local angle = Vector2.Angle(diff, Vector2(1, 0))
    if diff.y < 0 then
        angle = 360 - angle
    end
    con.m_Tex.transform.localEulerAngles = Vector3(0, 0, angle - 45)
    return true
end
CXingGuanWnd.m_UpdatePoles_CS2LuaHook = function (this)
    if nil == this.m_NorthPole or nil == this.m_SouthPole then
        return
    end
    local centerViewPos = this.m_Camera:WorldToViewportPoint(this.m_SphereTrans:TransformPoint(Vector3(0, this.m_SphereRadius, 0)))
    if centerViewPos.x <= 0 or centerViewPos.y <= 0 or centerViewPos.x >= 1 or centerViewPos.y >= 1 or centerViewPos.z <= 0 then
        this.m_NorthPole.gameObject:SetActive(false)
    else
        this.m_NorthPole.gameObject:SetActive(true)
        this.m_NorthPole.localPosition = Vector3(centerViewPos.x * this.m_LastSphereTexWidth - math.floor(this.m_LastSphereTexWidth / 2), centerViewPos.y * this.m_LastSphereTexHeight - math.floor(this.m_LastSphereTexHeight / 2), 0)
        Extensions.SetLocalRotationZ(this.m_NorthPole, - this.m_CameraTrans.localEulerAngles.y)
    end
    centerViewPos = this.m_Camera:WorldToViewportPoint(this.m_SphereTrans:TransformPoint(Vector3(0, - this.m_SphereRadius, 0)))
    if centerViewPos.x <= 0 or centerViewPos.y <= 0 or centerViewPos.x >= 1 or centerViewPos.y >= 1 or centerViewPos.z <= 0 then
        this.m_SouthPole.gameObject:SetActive(false)
    else
        this.m_SouthPole.gameObject:SetActive(true)
        this.m_SouthPole.localPosition = Vector3(centerViewPos.x * this.m_LastSphereTexWidth - math.floor(this.m_LastSphereTexWidth / 2), centerViewPos.y * this.m_LastSphereTexHeight - math.floor(this.m_LastSphereTexHeight / 2), 0)
        Extensions.SetLocalRotationZ(this.m_SouthPole, this.m_CameraTrans.localEulerAngles.y)
    end
end
CXingGuanWnd.m_OnEnable_CS2LuaHook = function (this)
    GestureRecognizer.Inst:SetEnableAutoSelect(false)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, MakeDelegateFromCSFunction(this.OnPinchIn, MakeGenericClass(Action1, GenericGesture), this))
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, MakeDelegateFromCSFunction(this.OnPinchOut, MakeGenericClass(Action1, GenericGesture), this))
    GestureRecognizer.AddGestureListener(GestureType.Swipe, MakeDelegateFromCSFunction(this.Swipe, MakeGenericClass(Action1, GenericGesture), this))
    UIEventListener.Get(this.m_CloseBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_BackOverviewBtn).onClick = MakeDelegateFromCSFunction(this.GoBackOverview, VoidDelegate, this)
    UIEventListener.Get(this.m_SearchBtn).onClick = MakeDelegateFromCSFunction(this.OnClickSearchButton, VoidDelegate, this)
    UIEventListener.Get(this.m_XingMangAddBtn).onClick = MakeDelegateFromCSFunction(this.OnXingMangAddBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.m_XingMangTemplateBtn).onClick = MakeDelegateFromCSFunction(this.OnXingMangTemplateBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.m_SphereTex.gameObject).onDoubleClick = MakeDelegateFromCSFunction(this.OnSphereDoubleClick, VoidDelegate, this)
    UIEventListener.Get(this.m_FastEntranceBtn).onClick = MakeDelegateFromCSFunction(this.FastEnterInside, VoidDelegate, this)
    --UIEventListener.Get(m_SphereTex.gameObject).onDrag = this.OnDrag;
    CMainCamera.Inst:SetCameraEnableStatus(false, "xingguanwnd", false)

    EventManager.AddListener(EnumEventType.MouseScrollWheel, MakeDelegateFromCSFunction(this.OnMouseScrollWheel, Action0, this))
    EventManager.AddListener(EnumEventType.SyncXingguanProp, MakeDelegateFromCSFunction(this.UpdateXingGuanProp, Action0, this))
    g_ScriptEvent:AddListener("XingguanConstellationWndClose", CLuaXingGuanWnd, "OnXingguanConstellationWndClose")
end
CXingGuanWnd.m_OnDisable_CS2LuaHook = function (this)
    GestureRecognizer.Inst:SetEnableAutoSelect(true)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, MakeDelegateFromCSFunction(this.OnPinchIn, MakeGenericClass(Action1, GenericGesture), this))
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, MakeDelegateFromCSFunction(this.OnPinchOut, MakeGenericClass(Action1, GenericGesture), this))
    GestureRecognizer.RemoveGestureListener(GestureType.Swipe, MakeDelegateFromCSFunction(this.Swipe, MakeGenericClass(Action1, GenericGesture), this))
    CMainCamera.Inst:SetCameraEnableStatus(true, "xingguanwnd", false)

    EventManager.RemoveListener(EnumEventType.MouseScrollWheel, MakeDelegateFromCSFunction(this.OnMouseScrollWheel, Action0, this))
    EventManager.RemoveListener(EnumEventType.SyncXingguanProp, MakeDelegateFromCSFunction(this.UpdateXingGuanProp, Action0, this))
    g_ScriptEvent:RemoveListener("XingguanConstellationWndClose", CLuaXingGuanWnd, "OnXingguanConstellationWndClose")
end
CXingGuanWnd.m_OnDestroy_CS2LuaHook = function (this)
    if this.m_DoubleClickFxObj ~= nil then
        CResourceMgr.Inst:Destroy(this.m_DoubleClickFxObj)
    end
    if this.m_SwipeFxObj ~= nil then
        CResourceMgr.Inst:Destroy(this.m_SwipeFxObj)
    end
    if this.m_DoubleClickFxDetroyTick ~= nil then
        invoke(this.m_DoubleClickFxDetroyTick)
    end
    if this.m_SwipeFxDetroyTick ~= nil then
        invoke(this.m_SwipeFxDetroyTick)
    end
    CXingGuanWnd.Inst = nil
    Object.Destroy(this.m_SphereRoot)

    if PlayerSettings.MusicEnabled then
        SoundManager.Inst:StopSound(this.m_BgMusic)
        SoundManager.Inst:StartBGMusic()
    end

    CLuaConstellationWnd:OnXingGuanWndDestroy()
end
CXingGuanWnd.m_OnMouseScrollWheel_CS2LuaHook = function (this)
    this:StartAutoRotatingTick()
    if not this.m_CanOperate then
        return
    end
    local delta = Input.GetAxis("Mouse ScrollWheel")
    if this.m_IsInOverview then
        if delta > 0 and this.m_Camera.fieldOfView >= this.m_MaxOverviewFov then
            return
        end
        if delta < 0 and this.m_Camera.fieldOfView <= this.m_MinOverviewFov then
            this:EnterInside()
            return
        end
    else
        if delta > 0 and this.m_Camera.fieldOfView >= this.m_MaxInsideFov then
            return
        end
        if delta < 0 and this.m_Camera.fieldOfView <= this.m_MinInsideFov then
            return
        end
    end
    this.m_Camera.fieldOfView = this.m_Camera.fieldOfView + (delta * 10) this.m_Camera.fieldOfView = this.m_Camera.fieldOfView
    this:AdjustBgTex()
end
CXingGuanWnd.m_OnPinchIn_CS2LuaHook = function (this, gesture)
    this:StartAutoRotatingTick()
    if not this.m_CanOperate then
        return
    end
    if this.m_IsInOverview and this.m_Camera.fieldOfView >= this.m_MaxOverviewFov then
        return
    end
    if not this.m_IsInOverview and this.m_Camera.fieldOfView >= this.m_MaxInsideFov then
        return
    end
    this.m_Camera.fieldOfView = this.m_Camera.fieldOfView + (gesture.deltaPinch / this.m_PinchSpeed) this.m_Camera.fieldOfView = this.m_Camera.fieldOfView
    if this.m_IsInOverview and this.m_Camera.fieldOfView > this.m_MaxOverviewFov then
        this.m_Camera.fieldOfView = this.m_MaxOverviewFov
    end
    if not this.m_IsInOverview and this.m_Camera.fieldOfView > this.m_MaxInsideFov then
        this.m_Camera.fieldOfView = this.m_MaxInsideFov
    end
    this:AdjustBgTex()
end
CXingGuanWnd.m_OnPinchOut_CS2LuaHook = function (this, gesture)
    this:StartAutoRotatingTick()
    if not this.m_CanOperate then
        return
    end
    if this.m_IsInOverview and this.m_Camera.fieldOfView <= this.m_MinOverviewFov then
        this:EnterInside()
        return
    end
    if not this.m_IsInOverview and this.m_Camera.fieldOfView <= this.m_MinInsideFov then
        return
    end
    this.m_Camera.fieldOfView = this.m_Camera.fieldOfView - (gesture.deltaPinch / this.m_PinchSpeed) this.m_Camera.fieldOfView = this.m_Camera.fieldOfView
    if this.m_IsInOverview and this.m_Camera.fieldOfView < this.m_MinOverviewFov then
        this.m_Camera.fieldOfView = this.m_MinOverviewFov
    end
    if not this.m_IsInOverview and this.m_Camera.fieldOfView < this.m_MinInsideFov then
        this.m_Camera.fieldOfView = this.m_MinInsideFov
    end
    this:AdjustBgTex()
end
CXingGuanWnd.m_UpdateCamera_CS2LuaHook = function (this)
    if this.m_IsInOverview then
        if not this:FloatEqualToZero(this.m_NextAngleX - this.m_AngleX) or not this:FloatEqualToZero(this.m_NextAngleY - this.m_AngleY) then
            this.m_SwipeTime = this.m_SwipeTime + Time.deltaTime
            this.m_AngleX = math.lerp(this.m_AngleX, this.m_NextAngleX, this.m_SwipeTime / CXingGuanWnd.SwipeDeltaTime)
            this.m_AngleY = math.lerp(this.m_AngleY, this.m_NextAngleY, this.m_SwipeTime / CXingGuanWnd.SwipeDeltaTime)
            this.m_CameraTrans.localPosition = CommonDefs.op_Multiply_Single_Vector3(this.m_CameraRadius, Vector3(math.sin(this.m_AngleY) * math.cos(this.m_AngleX), math.cos(this.m_AngleY), math.sin(this.m_AngleY) * math.sin(this.m_AngleX)))
            this.m_CameraTrans:LookAt(this.m_SphereTrans.position)
        end
    else
        if not this:FloatEqualToZero((CommonDefs.op_Subtraction_Vector3_Vector3(this.m_NextInsideCameraDir, this.m_InsideCameraDir)).magnitude) then
            this.m_SwipeTime = this.m_SwipeTime + Time.deltaTime
            this.m_InsideCameraDir = Vector3.Lerp(this.m_InsideCameraDir, this.m_NextInsideCameraDir, this.m_SwipeTime / CXingGuanWnd.SwipeDeltaTime)
            this.m_CameraTrans.localEulerAngles = this.m_InsideCameraDir
        end
    end
end
CXingGuanWnd.m_StartAutoRotatingTick_CS2LuaHook = function (this)
    this.m_IsAutoRotating = false
    if this.m_AutoRotatingTick ~= nil then
        invoke(this.m_AutoRotatingTick)
        this.m_AutoRotatingTick = nil
    end
    if this.m_IsInOverview then
        CTickMgr.Register(DelegateFactory.Action(function ()
            this.m_IsAutoRotating = true
            this.m_AutoRotatingTick = nil
        end), 500, ETickType.Once)
    end
end
CXingGuanWnd.m_AutoRotating_CS2LuaHook = function (this)
    if this.m_IsInOverview then
        this.m_AngleX = this.m_AngleX + 0.005
        this.m_NextAngleX = this.m_AngleX
        this.m_CameraTrans.localPosition = CommonDefs.op_Multiply_Single_Vector3(this.m_CameraRadius, Vector3(math.sin(this.m_AngleY) * math.cos(this.m_AngleX), math.cos(this.m_AngleY), math.sin(this.m_AngleY) * math.sin(this.m_AngleX)))
        this.m_CameraTrans:LookAt(this.m_SphereTrans.position)
    end
end
CXingGuanWnd.m_AdjustBgTex_CS2LuaHook = function (this)
    if not this.m_IsInOverview then
        return
    end
    local ratio = (this.m_Camera.fieldOfView - this.m_OverviewShowStarBgFovThreshold) / (this.m_MaxOverviewFov - this.m_OverviewShowStarBgFovThreshold)
    if ratio < 0 then
        this.m_BgStarTex.gameObject:SetActive(false)
        return
    end
    if ratio > 1 then
        ratio = 1
    end
    this.m_BgStarTex.gameObject:SetActive(true)
    this.m_BgStarTex.alpha = ratio
    local width = math.floor((this.m_MinBgWidthAnchorValue + this.m_MaxBgWidthAnchorValue * (1 - ratio)))
    local height = math.floor((this.m_MinBgHeightAnchorValue + this.m_MaxBgHeightAnchorValue * (1 - ratio)))
    this:SetBgAnchor(width, height)
end
CXingGuanWnd.m_SetBgAnchor_CS2LuaHook = function (this, width, height)
    this.m_BgTex.leftAnchor.absolute = - width
    this.m_BgTex.rightAnchor.absolute = width
    this.m_BgTex.topAnchor.absolute = height
    this.m_BgTex.bottomAnchor.absolute = - height
end
CXingGuanWnd.m_OnSphereDoubleClick_CS2LuaHook = function (this, go)
    if not this.m_IsInOverview then
        return
    end
    local screenHeight = CommonDefs.GameScreenHeight local screenWidth = CommonDefs.GameScreenWidth
    local sphereTexHeight = CommonDefs.GameScreenHeight
    local sphereTexWidth = math.floor((CommonDefs.GameScreenHeight * 1920 / 1080))
    local diffWidth = (screenWidth - sphereTexWidth) / 2
    local viewport = Vector3((Input.mousePosition.x - diffWidth) / sphereTexWidth, Input.mousePosition.y / sphereTexHeight, 0)
    local ray = this.m_Camera:ViewportPointToRay(viewport)
    local hits = Physics.RaycastAll(ray, this.m_Camera.farClipPlane, this.m_Camera.cullingMask)
    if nil == hits or hits.Length == 0 then
        return
    end

    if this.m_DoubleClickFxObj ~= nil then
        CResourceMgr.Inst:Destroy(this.m_DoubleClickFxObj)
    end
    if this.m_DoubleClickFxDetroyTick ~= nil then
        invoke(this.m_DoubleClickFxDetroyTick)
    end
    CCoroutineMgr.Inst:StartCoroutine(this:LoadFx(CXingGuanWnd.DoubleClickFxPath, DelegateFactory.Action_GameObject(function (fx)
        fx.transform:LookAt(hits[0].point)
        fx.transform.localEulerAngles = CommonDefs.op_Addition_Vector3_Vector3(fx.transform.localEulerAngles, Vector3(90, 0, 0))
        this.m_DoubleClickFxObj = fx
        this.m_DoubleClickFxDetroyTick = CTickMgr.Register(DelegateFactory.Action(function ()
            if this.m_DoubleClickFxObj ~= nil then
                CResourceMgr.Inst:Destroy(this.m_DoubleClickFxObj)
            end
            if this.m_DoubleClickFxDetroyTick ~= nil then
                invoke(this.m_DoubleClickFxDetroyTick)
                this.m_DoubleClickFxDetroyTick = nil
            end
        end), CXingGuanWnd.DoubleClickFxDuration, ETickType.Once)
    end)))
end
CXingGuanWnd.m_GetConstellationStarCount_CS2LuaHook = function (this, key, lightedStarCount, starCount)
    starCount = 0 lightedStarCount = starCount
    if not CommonDefs.DictContains(this.m_Constellation2StarDict, typeof(UInt32), key) then
        return lightedStarCount, starCount
    end
    local list = CommonDefs.DictGetValue(this.m_Constellation2StarDict, typeof(UInt32), key)
    starCount = list.Count
    do
        local i = 0
        while i < starCount do
            if this:IsStarLighted(list[i]) then
                lightedStarCount = lightedStarCount + 1
            end
            i = i + 1
        end
    end
    return lightedStarCount, starCount
end
CXingGuanWnd.m_GoBackOverview_CS2LuaHook = function (this, go)
    this.m_IsAutoRotating = true
    this.m_IsInOverview = true
    this.m_InsideUIRoot:SetActive(false)
    this.m_OverviewUIRoot:SetActive(true)
    this.m_CameraTrans.localPosition = CommonDefs.op_Multiply_Single_Vector3(this.m_CameraRadius, Vector3(math.sin(this.m_AngleY) * math.cos(this.m_AngleX), math.cos(this.m_AngleY), math.sin(this.m_AngleY) * math.sin(this.m_AngleX)))
    this.m_CameraTrans:LookAt(this.m_SphereTrans.position)
    this.m_Camera.fieldOfView = this.m_DefaultOverviewFov
    this.m_SphereTex.keepAspectRatio = UIWidget.AspectRatioSource.BasedOnHeight
    this.m_BgTex.color = Color.white
    this.m_WaterObj:SetActive(false)
    if this.m_SwipeFxObj~=nil then
        this.m_SwipeFxObj:SetActive(true)
    end
    do
        local i = 0 local cnt = this.m_UIConstellationList.Count
        while i < cnt do
            this.m_UIConstellationList[i].m_Tex.gameObject:SetActive(false)
            i = i + 1
        end
    end

    do
        local i = 0 local cnt = this.m_ConstellationNameTransList.Count
        while i < cnt do
            this.m_ConstellationNameTransList[i].gameObject:SetActive(false)
            i = i + 1
        end
    end

    this.m_NorthPole.gameObject:SetActive(false)
    this.m_SouthPole.gameObject:SetActive(false)

    this.m_LineRoot.gameObject:SetActive(false)

    CLuaXingGuanWnd:ResetOverviewSphereTexPos()
end
CXingGuanWnd.m_GenerateConstellation_CS2LuaHook = function (this, center, topright, path, key, isVisiable)
    if CommonDefs.HashSetContains(this.m_ConstellationKeySet, typeof(UInt32), key) then
        return
    end
    local centerPos = CommonDefs.op_Multiply_Single_Vector3(this.m_SphereRadius, Vector3(math.sin(center.y / 180 * CXingGuanWnd.PI) * math.cos(center.x / 180 * CXingGuanWnd.PI), math.cos(center.y / 180 * CXingGuanWnd.PI), math.sin(center.y / 180 * CXingGuanWnd.PI) * math.sin(center.x / 180 * CXingGuanWnd.PI)))
    local toprightPos = CommonDefs.op_Multiply_Single_Vector3(this.m_SphereRadius, Vector3(math.sin(topright.y / 180 * CXingGuanWnd.PI) * math.cos(topright.x / 180 * CXingGuanWnd.PI), math.cos(topright.y / 180 * CXingGuanWnd.PI), math.sin(topright.y / 180 * CXingGuanWnd.PI) * math.sin(topright.x / 180 * CXingGuanWnd.PI)))

    local go = NGUITools.AddChild(this.m_SphereTex.gameObject, this.m_ConstellationObj)
    local tex = CommonDefs.GetComponent_GameObject_Type(go, typeof(CUITexture))
    if tex ~= nil then
        tex:LoadMaterial(path)
        if key > 0 and isVisiable then
            CommonDefs.HashSetAdd(this.m_ConstellationKeySet, typeof(UInt32), key)
        end
        local con = CreateFromClass(CUIConstellation)
        con.m_Id = key
        con.m_Tex = tex
        con.m_CenterPos = centerPos
        con.m_TRPos = toprightPos
        if isVisiable then
            CommonDefs.ListAdd(this.m_UIConstellationList, typeof(CUIConstellation), con)
        else
            go:SetActive(true)
            CommonDefs.ListAdd(this.m_InvisiableUIConstellationList, typeof(CUIConstellation), con)
            local alpha = CommonDefs.GetComponent_GameObject_Type(go, typeof(TweenAlpha))
            if alpha ~= nil then
                alpha.enabled = true
            end
        end
    end
end
CXingGuanWnd.m_OnConstellationNameClick_CS2LuaHook = function (this, go)
    if CommonDefs.DictContains(this.m_ConstellationName2KeyDict, typeof(GameObject), go) then
        this.m_ConstellationWnd.gameObject:SetActive(true)
        this.m_ConstellationWnd:Init(CommonDefs.DictGetValue(this.m_ConstellationName2KeyDict, typeof(GameObject), go))
        this.m_CanOperate = false
    end
end
CXingGuanWnd.m_DrawLine_CS2LuaHook = function (this, startPos, endPos)
    local go = CommonDefs.Object_Instantiate(this.m_LineObj)
    go:SetActive(true)
    go.transform.parent = this.m_LineRoot
    local lr = CommonDefs.GetComponent_GameObject_Type(go, typeof(LineRenderer))
    if lr ~= nil then
        lr.useWorldSpace = true
        lr:SetPosition(0, startPos)
        lr:SetPosition(1, endPos)
    end
end
CXingGuanWnd.m_UpdateConstellation_CS2LuaHook = function (this, constellationId)
    if nil == CClientMainPlayer.Inst or nil == CClientMainPlayer.Inst.PlayProp or not CommonDefs.DictContains(this.m_Constellation2StarDict, typeof(UInt32), constellationId) then
        return
    end
    local staredCount = CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), constellationId) and CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.XingguanProperty.XingguanData, typeof(Byte), constellationId).StaredCount or 0
    local starList = CommonDefs.DictGetValue(this.m_Constellation2StarDict, typeof(UInt32), constellationId)
    do
        local i = 0 local cnt = starList.Count
        while i < cnt do
            local item = CommonDefs.GetComponent_Component_Type(starList[i].m_Tranform, typeof(CStarItem))
            if item ~= nil then
                item:LightUpOrDown(i < staredCount)
            end
            i = i + 1
        end
    end
    --DrawAllLine();
    -- Update Texture
    if staredCount >= starList.Count then
        if not CommonDefs.HashSetContains(this.m_ConstellationKeySet, typeof(UInt32), constellationId) then
            local data = Xingguan_StarGroup.GetData(constellationId)
            if data ~= nil and data.Pos ~= nil and data.Pos.Length == 4 then
                this:GenerateConstellation(Vector2(data.Pos[0], data.Pos[1]), Vector2(data.Pos[2], data.Pos[3]), System.String.Format(CXingGuanWnd.IMAGE_PATTERN, data.ImagePath), constellationId, true)
            end
        end
    else
        this:RemoveOneConstellation(constellationId)
    end
end
CXingGuanWnd.m_RemoveOneConstellation_CS2LuaHook = function (this, constellationId)
    if not CommonDefs.HashSetContains(this.m_ConstellationKeySet, typeof(UInt32), constellationId) then
        return
    end
    CommonDefs.HashSetRemove(this.m_ConstellationKeySet, typeof(UInt32), constellationId)
    do
        local i = 0 local cnt = this.m_UIConstellationList.Count
        while i < cnt do
            if this.m_UIConstellationList[i].m_Id == constellationId then
                Object.Destroy(this.m_UIConstellationList[i].m_Tex.gameObject)
                CommonDefs.ListRemoveAt(this.m_UIConstellationList, i)
                break
            end
            i = i + 1
        end
    end
end
CXingGuanWnd.m_UpdateXingGuanProp_CS2LuaHook = function (this)
    this.m_XingMangNumLabel.text = (CFeiShengMgr.Inst.m_TotalXingmangCount - CFeiShengMgr.Inst.m_UsedXingmangCount .. "/") .. CFeiShengMgr.Inst.m_TotalXingmangCount
    Xingguan_StarGroup.ForeachKey(DelegateFactory.Action_object(function (key)
        this:UpdateConstellation(key)
    end))
    this:DrawAllLine()
end

CXingGuanWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName)
    if methodName == "GetSearchButton" then
        return this.m_SearchBtn
    elseif methodName == "GetXingGuanItem" then
        return this.m_ConstellationWnd:GetXingGuanItem()
    elseif methodName == "GetLightUpButton" then
        return this.m_ConstellationWnd:GetLightUpButton()
    elseif methodName == "GetXingGuanEditButton" then 
        return this.m_OverviewEditBtn
    end

    return nil
end

CLuaXingGuanWnd = {}
CLuaXingGuanWnd.m_Wnd = nil
CLuaXingGuanWnd.m_OverviewWnd = nil
CLuaXingGuanWnd.m_OverviewShowBtn = nil
CLuaXingGuanWnd.m_OverviewShowBtnArrow = nil

function CLuaXingGuanWnd:Init(this)
    self.m_Wnd = this

    self.m_OverviewWnd = self.m_Wnd.transform:Find("XingGuanOverview").gameObject
    self.m_OverviewShowBtn = self.m_Wnd.transform:Find("XingGuanOverviewShowButton"):GetComponent(typeof(QnButton))
    self.m_OverviewShowBtnArrow = self.m_Wnd.transform:Find("XingGuanOverviewShowButton/sprite").gameObject

    self.m_OverviewShowBtn.OnClick = DelegateFactory.Action_QnButton(function (q)
        self.m_OverviewWnd.gameObject:SetActive(true)
        self.m_OverviewShowBtn.gameObject:SetActive(false)
        self:OnOpenOverview()
    end)

    self.m_OverviewWnd.gameObject:SetActive(true)
    self:ResetOverviewSphereTexPos()
end

function CLuaXingGuanWnd:OnOpenOverview()
    if self.m_Wnd == nil then
        return 
    end

    self.m_Wnd.m_FastEntranceBtn:SetActive(true)
    if self.m_Wnd.m_OverviewUIRoot.activeSelf then
        LuaTweenUtils.DOLocalMoveX(self.m_Wnd.m_SphereTex.transform, self.m_Wnd.m_FastEntranceBtn.transform.localPosition.x, Constants.GeneralAnimationDuration, false)
    end
end

function CLuaXingGuanWnd:OnCloseOverview()
    if self.m_Wnd == nil then
        return 
    end

    self.m_Wnd.m_FastEntranceBtn:SetActive(false)
    LuaTweenUtils.DOLocalMoveX(self.m_Wnd.m_SphereTex.transform, 0, Constants.GeneralAnimationDuration, false)
end

function CLuaXingGuanWnd:ResetOverviewSphereTexPos()
    if self.m_Wnd == nil then
        return 
    end

    self.m_Wnd.m_FastEntranceBtn:SetActive(self.m_OverviewWnd.gameObject.activeSelf)
    LuaTweenUtils.DOLocalMoveX(self.m_Wnd.m_SphereTex.transform, (self.m_OverviewWnd.gameObject.activeSelf and not self.m_Wnd.m_InsideUIRoot.activeSelf) and self.m_Wnd.m_FastEntranceBtn.transform.localPosition.x or 0, Constants.GeneralAnimationDuration, false)
end

function CLuaXingGuanWnd:OnXingguanConstellationWndClose()
    self:ResetOverviewSphereTexPos()
end
