local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local UIInput = import "UIInput"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local Vector2 = import "UnityEngine.Vector2"

CLuaPKProtectSetWnd = class()

RegistChildComponent(CLuaPKProtectSetWnd,"m_ZongMenButton","ZongMenButton", QnCheckBox)
RegistChildComponent(CLuaPKProtectSetWnd,"m_Table","Table", UITable)

RegistClassMember(CLuaPKProtectSetWnd,"m_TeamButton")
RegistClassMember(CLuaPKProtectSetWnd,"m_GuildButton")
RegistClassMember(CLuaPKProtectSetWnd,"m_TeamGroupButton")
RegistClassMember(CLuaPKProtectSetWnd,"m_GreenNameButton")
RegistClassMember(CLuaPKProtectSetWnd,"m_OtherServerButton")
RegistClassMember(CLuaPKProtectSetWnd,"m_GradeInput")
RegistClassMember(CLuaPKProtectSetWnd,"m_GradeInputRoot")
RegistClassMember(CLuaPKProtectSetWnd,"m_CityWarTipRoot")
RegistClassMember(CLuaPKProtectSetWnd,"m_OKButton")

function CLuaPKProtectSetWnd:Init()
    --self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
    self.m_TeamButton = self.transform:Find("Offset/Table/TeamButton"):GetComponent(typeof(QnCheckBox))
    self.m_GuildButton = self.transform:Find("Offset/Table/GuildButton"):GetComponent(typeof(QnCheckBox))
    self.m_TeamGroupButton = self.transform:Find("Offset/Table/TeamGroupButton"):GetComponent(typeof(QnCheckBox))
    self.m_GreenNameButton = self.transform:Find("Offset/Table/GreenNameButton"):GetComponent(typeof(QnCheckBox))
    self.m_OtherServerButton = self.transform:Find("Offset/Table/OtherServerButton"):GetComponent(typeof(QnCheckBox))
    self.m_GradeInput = self.transform:Find("Offset/Input/InputField"):GetComponent(typeof(UIInput))
    self.m_GradeInputRoot = self.transform:Find("Offset/Input").gameObject
    self.m_CityWarTipRoot = self.transform:Find("Offset/CityWarTip").gameObject
    self.m_OKButton = self.transform:Find("Offset/OKButton").gameObject

    local pkProtect = CClientMainPlayer.Inst and CClientMainPlayer.Inst.FightProp.PkProtect or nil

    self.m_TeamButton.Selected = (pkProtect and pkProtect[PkProtect_lua.Team] == 1)

    self.m_GuildButton.Selected = (pkProtect and pkProtect[PkProtect_lua.Guild] == 1)

    self.m_TeamGroupButton.Selected = (pkProtect and pkProtect[PkProtect_lua.Group] == 1)

    self.m_GreenNameButton.Selected = (pkProtect and pkProtect[PkProtect_lua.GreenName] == 1)

    self.m_OtherServerButton.Selected = (pkProtect and pkProtect[PkProtect_lua.OtherServer] == 1)

    self.m_ZongMenButton.Selected = (pkProtect and pkProtect[PkProtect_lua.Sect] == 1)

    local zongMenCreated = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0
    self.m_ZongMenButton.gameObject:SetActive(zongMenCreated)
    self.m_Table.padding = Vector2(0, zongMenCreated and 5 or 10)
    self.m_Table:Reposition ()

    local grade = pkProtect and pkProtect[PkProtect_lua.Grade] or 0
    if grade >= 29 and grade <= 150 then
        self.m_GradeInput.value = tostring(grade)
    else
        self.m_GradeInput.value = "29"
    end

    local bInCityWarScene = CCityWarMgr.Inst:IsInCityWarScene() or CCityWarMgr.Inst:IsInCityWarCopyScene() or false
    self.m_GradeInputRoot:SetActive(not bInCityWarScene)
    self.m_CityWarTipRoot:SetActive(bInCityWarScene)

end

function CLuaPKProtectSetWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaPKProtectSetWnd:OnValueChange()
    if self.m_GradeInput.value ~= "" then
        local grade = tonumber(self.m_GradeInput.value) or -1
        if grade<0 or grade>150 then
            self.m_GradeInput.value = "150"
        end
    end
end

function CLuaPKProtectSetWnd:Start()
    if not self.m_GradeInput then return end

    CommonDefs.AddEventDelegate(self.m_GradeInput.onChange, DelegateFactory.Action(function ( ... )
        self:OnValueChange()
    end))


    UIEventListener.Get(self.m_OKButton).onClick = LuaUtils.VoidDelegate(function ( ... )
        self:OnOkButtonClick()
    end)

    -- UIEventListener.Get(self.m_CloseBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
    --     self:Close()
    -- end)
end

function CLuaPKProtectSetWnd:OnOkButtonClick()

    if CClientMainPlayer.Inst==nil then
        self:Close()
        return
    end

    local grade = tonumber(self.m_GradeInput.value) or -1
    if grade < 29 or grade > 150 then
        g_MessageMgr:ShowMessage("PKPROTECT_GRADE_INPUT_ILLEGAL")
        return
    end

    Gac2Gas.RequestSetPKProtect(PkProtect_lua.Team, self.m_TeamButton.Selected and 1 or 0)
    Gac2Gas.RequestSetPKProtect(PkProtect_lua.Guild, self.m_GuildButton.Selected and 1 or 0)
    Gac2Gas.RequestSetPKProtect(PkProtect_lua.Group, self.m_TeamGroupButton.Selected and 1 or 0)
    Gac2Gas.RequestSetPKProtect(PkProtect_lua.GreenName, self.m_GreenNameButton.Selected and 1 or 0)
    Gac2Gas.RequestSetPKProtect(PkProtect_lua.OtherServer, self.m_OtherServerButton.Selected and 1 or 0)
    Gac2Gas.RequestSetPKProtect(PkProtect_lua.Sect, self.m_ZongMenButton.Selected and 1 or 0)
    Gac2Gas.RequestSetPKProtect(PkProtect_lua.Grade, grade)

    self:Close()
end
