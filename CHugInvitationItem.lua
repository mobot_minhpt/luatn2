-- Auto Generated!!
local CHugInvitationItem = import "L10.UI.CHugInvitationItem"
local LocalString = import "LocalString"
local StringBuilder = import "System.Text.StringBuilder"
CHugInvitationItem.m_Init_CS2LuaHook = function (this, playerId, playerName, portraitName, level, buttonText) 
    this.PlayerId = playerId
    this.nameLabel.text = playerName
    this.portrait:LoadNPCPortrait(portraitName, false)
    this.levelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(level))
    this.opButton.Text = buttonText
end
