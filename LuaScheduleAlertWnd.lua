local UITabBar = import "L10.UI.UITabBar"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaScheduleAlertWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaScheduleAlertWnd, "Calendar", "Calendar", GameObject)
RegistChildComponent(LuaScheduleAlertWnd, "Setting", "Setting", GameObject)
RegistChildComponent(LuaScheduleAlertWnd, "Tabs", "Tabs", UITabBar)

--@endregion RegistChildComponent end

function LuaScheduleAlertWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index)
    end)
    --@endregion EventBind end
end

function LuaScheduleAlertWnd:Init()
    self.Tabs:ChangeTab(0, false)
end

function LuaScheduleAlertWnd:OnTabChange(index)
    self.Calendar.gameObject:SetActive(index == 0)
    self.Setting.gameObject:SetActive(index == 1)
end
--@region UIEvent

--@endregion UIEvent

