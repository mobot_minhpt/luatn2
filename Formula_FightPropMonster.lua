local max = math.max
local min = math.min
local abs = math.abs
local ceil = math.ceil
local floor = math.floor
local log = math.log
local power = function(a, b) return a^b end

local EnumMonsterFightProp = {
	PermanentCor = 1,
	PermanentSta = 2,
	PermanentStr = 3,
	PermanentInt = 4,
	PermanentAgi = 5,
	PermanentHpFull = 6,
	Hp = 7,
	Mp = 8,
	CurrentHpFull = 9,
	CurrentMpFull = 10,
	EquipExchangeMF = 11,
	RevisePermanentCor = 12,
	RevisePermanentSta = 13,
	RevisePermanentStr = 14,
	RevisePermanentInt = 15,
	RevisePermanentAgi = 16,
	Class = 2001,
	Grade = 2002,
	Cor = 2003,
	AdjCor = 2004,
	MulCor = 2005,
	Sta = 2006,
	AdjSta = 2007,
	MulSta = 2008,
	Str = 2009,
	AdjStr = 2010,
	MulStr = 2011,
	Int = 2012,
	AdjInt = 2013,
	MulInt = 2014,
	Agi = 2015,
	AdjAgi = 2016,
	MulAgi = 2017,
	HpFull = 2018,
	AdjHpFull = 2019,
	MulHpFull = 2020,
	HpRecover = 2021,
	AdjHpRecover = 2022,
	MulHpRecover = 2023,
	MpFull = 2024,
	AdjMpFull = 2025,
	MulMpFull = 2026,
	MpRecover = 2027,
	AdjMpRecover = 2028,
	MulMpRecover = 2029,
	pAttMin = 2030,
	AdjpAttMin = 2031,
	MulpAttMin = 2032,
	pAttMax = 2033,
	AdjpAttMax = 2034,
	MulpAttMax = 2035,
	pHit = 2036,
	AdjpHit = 2037,
	MulpHit = 2038,
	pMiss = 2039,
	AdjpMiss = 2040,
	MulpMiss = 2041,
	pDef = 2042,
	AdjpDef = 2043,
	MulpDef = 2044,
	AdjpHurt = 2045,
	MulpHurt = 2046,
	pSpeed = 2047,
	OripSpeed = 2048,
	MulpSpeed = 2049,
	pFatal = 2050,
	AdjpFatal = 2051,
	AntipFatal = 2052,
	AdjAntipFatal = 2053,
	pFatalDamage = 2054,
	AdjpFatalDamage = 2055,
	AntipFatalDamage = 2056,
	AdjAntipFatalDamage = 2057,
	Block = 2058,
	AdjBlock = 2059,
	MulBlock = 2060,
	BlockDamage = 2061,
	AdjBlockDamage = 2062,
	AntiBlock = 2063,
	AdjAntiBlock = 2064,
	AntiBlockDamage = 2065,
	AdjAntiBlockDamage = 2066,
	mAttMin = 2067,
	AdjmAttMin = 2068,
	MulmAttMin = 2069,
	mAttMax = 2070,
	AdjmAttMax = 2071,
	MulmAttMax = 2072,
	mHit = 2073,
	AdjmHit = 2074,
	MulmHit = 2075,
	mMiss = 2076,
	AdjmMiss = 2077,
	MulmMiss = 2078,
	mDef = 2079,
	AdjmDef = 2080,
	MulmDef = 2081,
	AdjmHurt = 2082,
	MulmHurt = 2083,
	mSpeed = 2084,
	OrimSpeed = 2085,
	MulmSpeed = 2086,
	mFatal = 2087,
	AdjmFatal = 2088,
	AntimFatal = 2089,
	AdjAntimFatal = 2090,
	mFatalDamage = 2091,
	AdjmFatalDamage = 2092,
	AntimFatalDamage = 2093,
	AdjAntimFatalDamage = 2094,
	EnhanceFire = 2095,
	AdjEnhanceFire = 2096,
	MulEnhanceFire = 2097,
	EnhanceThunder = 2098,
	AdjEnhanceThunder = 2099,
	MulEnhanceThunder = 2100,
	EnhanceIce = 2101,
	AdjEnhanceIce = 2102,
	MulEnhanceIce = 2103,
	EnhancePoison = 2104,
	AdjEnhancePoison = 2105,
	MulEnhancePoison = 2106,
	EnhanceWind = 2107,
	AdjEnhanceWind = 2108,
	MulEnhanceWind = 2109,
	EnhanceLight = 2110,
	AdjEnhanceLight = 2111,
	MulEnhanceLight = 2112,
	EnhanceIllusion = 2113,
	AdjEnhanceIllusion = 2114,
	MulEnhanceIllusion = 2115,
	AntiFire = 2116,
	AdjAntiFire = 2117,
	MulAntiFire = 2118,
	AntiThunder = 2119,
	AdjAntiThunder = 2120,
	MulAntiThunder = 2121,
	AntiIce = 2122,
	AdjAntiIce = 2123,
	MulAntiIce = 2124,
	AntiPoison = 2125,
	AdjAntiPoison = 2126,
	MulAntiPoison = 2127,
	AntiWind = 2128,
	AdjAntiWind = 2129,
	MulAntiWind = 2130,
	AntiLight = 2131,
	AdjAntiLight = 2132,
	MulAntiLight = 2133,
	AntiIllusion = 2134,
	AdjAntiIllusion = 2135,
	MulAntiIllusion = 2136,
	IgnoreAntiFire = 2137,
	IgnoreAntiThunder = 2138,
	IgnoreAntiIce = 2139,
	IgnoreAntiPoison = 2140,
	IgnoreAntiWind = 2141,
	IgnoreAntiLight = 2142,
	IgnoreAntiIllusion = 2143,
	EnhanceDizzy = 2144,
	AdjEnhanceDizzy = 2145,
	MulEnhanceDizzy = 2146,
	EnhanceSleep = 2147,
	AdjEnhanceSleep = 2148,
	MulEnhanceSleep = 2149,
	EnhanceChaos = 2150,
	AdjEnhanceChaos = 2151,
	MulEnhanceChaos = 2152,
	EnhanceBind = 2153,
	AdjEnhanceBind = 2154,
	MulEnhanceBind = 2155,
	EnhanceSilence = 2156,
	AdjEnhanceSilence = 2157,
	MulEnhanceSilence = 2158,
	AntiDizzy = 2159,
	AdjAntiDizzy = 2160,
	MulAntiDizzy = 2161,
	AntiSleep = 2162,
	AdjAntiSleep = 2163,
	MulAntiSleep = 2164,
	AntiChaos = 2165,
	AdjAntiChaos = 2166,
	MulAntiChaos = 2167,
	AntiBind = 2168,
	AdjAntiBind = 2169,
	MulAntiBind = 2170,
	AntiSilence = 2171,
	AdjAntiSilence = 2172,
	MulAntiSilence = 2173,
	DizzyTimeChange = 2174,
	SleepTimeChange = 2175,
	ChaosTimeChange = 2176,
	BindTimeChange = 2177,
	SilenceTimeChange = 2178,
	BianhuTimeChange = 2179,
	FreezeTimeChange = 2180,
	PetrifyTimeChange = 2181,
	EnhanceHeal = 2182,
	AdjEnhanceHeal = 2183,
	MulEnhanceHeal = 2184,
	Speed = 2185,
	OriSpeed = 2186,
	AdjSpeed = 2187,
	MulSpeed = 2188,
	MF = 2189,
	AdjMF = 2190,
	Range = 2191,
	AdjRange = 2192,
	HatePlus = 2193,
	AdjHatePlus = 2194,
	HateDecrease = 2195,
	Invisible = 2196,
	AdjInvisible = 2197,
	TrueSight = 2198,
	OriTrueSight = 2199,
	AdjTrueSight = 2200,
	EyeSight = 2201,
	OriEyeSight = 2202,
	AdjEyeSight = 2203,
	EnhanceTian = 2204,
	EnhanceTianMul = 2205,
	EnhanceEGui = 2206,
	EnhanceEGuiMul = 2207,
	EnhanceXiuLuo = 2208,
	EnhanceXiuLuoMul = 2209,
	EnhanceDiYu = 2210,
	EnhanceDiYuMul = 2211,
	EnhanceRen = 2212,
	EnhanceRenMul = 2213,
	EnhanceChuSheng = 2214,
	EnhanceChuShengMul = 2215,
	EnhanceBuilding = 2216,
	EnhanceBuildingMul = 2217,
	EnhanceBoss = 2218,
	EnhanceBossMul = 2219,
	EnhanceSheShou = 2220,
	EnhanceSheShouMul = 2221,
	EnhanceJiaShi = 2222,
	EnhanceJiaShiMul = 2223,
	EnhanceFangShi = 2224,
	EnhanceFangShiMul = 2225,
	EnhanceYiShi = 2226,
	EnhanceYiShiMul = 2227,
	EnhanceMeiZhe = 2228,
	EnhanceMeiZheMul = 2229,
	EnhanceYiRen = 2230,
	EnhanceYiRenMul = 2231,
	IgnorepDef = 2232,
	IgnoremDef = 2233,
	EnhanceZhaoHuan = 2234,
	EnhanceZhaoHuanMul = 2235,
	DizzyProbability = 2236,
	SleepProbability = 2237,
	ChaosProbability = 2238,
	BindProbability = 2239,
	SilenceProbability = 2240,
	HpFullPow2 = 2241,
	HpFullPow1 = 2242,
	HpFullInit = 2243,
	MpFullPow1 = 2244,
	MpFullInit = 2245,
	MpRecoverPow1 = 2246,
	MpRecoverInit = 2247,
	PAttMinPow2 = 2248,
	PAttMinPow1 = 2249,
	PAttMinInit = 2250,
	PAttMaxPow2 = 2251,
	PAttMaxPow1 = 2252,
	PAttMaxInit = 2253,
	PhitPow1 = 2254,
	PHitInit = 2255,
	PMissPow1 = 2256,
	PMissInit = 2257,
	PSpeedPow1 = 2258,
	PSpeedInit = 2259,
	PDefPow1 = 2260,
	PDefInit = 2261,
	PfatalPow1 = 2262,
	PFatalInit = 2263,
	MAttMinPow2 = 2264,
	MAttMinPow1 = 2265,
	MAttMinInit = 2266,
	MAttMaxPow2 = 2267,
	MAttMaxPow1 = 2268,
	MAttMaxInit = 2269,
	MSpeedPow1 = 2270,
	MSpeedInit = 2271,
	MDefPow1 = 2272,
	MDefInit = 2273,
	MHitPow1 = 2274,
	MHitInit = 2275,
	MMissPow1 = 2276,
	MMissInit = 2277,
	MFatalPow1 = 2278,
	MFatalInit = 2279,
	BlockDamagePow1 = 2280,
	BlockDamageInit = 2281,
	RunSpeed = 2282,
	HpRecoverPow1 = 2283,
	HpRecoverInit = 2284,
	AntiBlockDamagePow1 = 2285,
	AntiBlockDamageInit = 2286,
	BBMax = 2287,
	AdjBBMax = 2288,
	AntiBreak = 2289,
	AdjAntiBreak = 2290,
	AntiPushPull = 2291,
	AdjAntiPushPull = 2292,
	AntiPetrify = 2293,
	AdjAntiPetrify = 2294,
	MulAntiPetrify = 2295,
	AntiTie = 2296,
	AdjAntiTie = 2297,
	MulAntiTie = 2298,
	EnhancePetrify = 2299,
	AdjEnhancePetrify = 2300,
	MulEnhancePetrify = 2301,
	EnhanceTie = 2302,
	AdjEnhanceTie = 2303,
	MulEnhanceTie = 2304,
	TieProbability = 2305,
	StrZizhi = 2306,
	CorZizhi = 2307,
	StaZizhi = 2308,
	AgiZizhi = 2309,
	IntZizhi = 2310,
	InitStrZizhi = 2311,
	InitCorZizhi = 2312,
	InitStaZizhi = 2313,
	InitAgiZizhi = 2314,
	InitIntZizhi = 2315,
	Wuxing = 2316,
	Xiuwei = 2317,
	SkillNum = 2318,
	PType = 2319,
	MType = 2320,
	PHType = 2321,
	GrowFactor = 2322,
	WuxingImprove = 2323,
	XiuweiImprove = 2324,
	EnhanceBeHeal = 2325,
	AdjEnhanceBeHeal = 2326,
	MulEnhanceBeHeal = 2327,
	LookUpCor = 2328,
	LookUpSta = 2329,
	LookUpStr = 2330,
	LookUpInt = 2331,
	LookUpAgi = 2332,
	LookUpHpFull = 2333,
	LookUpMpFull = 2334,
	LookUppAttMin = 2335,
	LookUppAttMax = 2336,
	LookUppHit = 2337,
	LookUppMiss = 2338,
	LookUppDef = 2339,
	LookUpmAttMin = 2340,
	LookUpmAttMax = 2341,
	LookUpmHit = 2342,
	LookUpmMiss = 2343,
	LookUpmDef = 2344,
	AdjHpFull2 = 2345,
	AdjpAttMin2 = 2346,
	AdjpAttMax2 = 2347,
	AdjmAttMin2 = 2348,
	AdjmAttMax2 = 2349,
	LingShouType = 2350,
	MHType = 2351,
	EnhanceLingShou = 2352,
	EnhanceLingShouMul = 2353,
	AntiAoe = 2354,
	AdjAntiAoe = 2355,
	AntiBlind = 2356,
	AdjAntiBlind = 2357,
	MulAntiBlind = 2358,
	AntiDecelerate = 2359,
	AdjAntiDecelerate = 2360,
	MulAntiDecelerate = 2361,
	MinGrade = 2362,
	CharacterCor = 2363,
	CharacterSta = 2364,
	CharacterStr = 2365,
	CharacterInt = 2366,
	CharacterAgi = 2367,
	EnhanceDaoKe = 2368,
	EnhanceDaoKeMul = 2369,
	ZuoQiSpeed = 2370,
	EnhanceBianHu = 2371,
	AdjEnhanceBianHu = 2372,
	MulEnhanceBianHu = 2373,
	AntiBianHu = 2374,
	AdjAntiBianHu = 2375,
	MulAntiBianHu = 2376,
	EnhanceXiaKe = 2377,
	EnhanceXiaKeMul = 2378,
	TieTimeChange = 2379,
	EnhanceYanShi = 2380,
	EnhanceYanShiMul = 2381,
	PAType = 2382,
	MAType = 2383,
	lifetimeNoReduceRate = 2384,
	AddLSHpDurgImprove = 2385,
	AddHpDurgImprove = 2386,
	AddMpDurgImprove = 2387,
	FireHurtReduce = 2388,
	ThunderHurtReduce = 2389,
	IceHurtReduce = 2390,
	PoisonHurtReduce = 2391,
	WindHurtReduce = 2392,
	LightHurtReduce = 2393,
	IllusionHurtReduce = 2394,
	FakepDef = 2395,
	FakemDef = 2396,
	EnhanceWater = 2397,
	AdjEnhanceWater = 2398,
	MulEnhanceWater = 2399,
	AntiWater = 2400,
	AdjAntiWater = 2401,
	MulAntiWater = 2402,
	WaterHurtReduce = 2403,
	IgnoreAntiWater = 2404,
	AntiFreeze = 2405,
	AdjAntiFreeze = 2406,
	MulAntiFreeze = 2407,
	EnhanceFreeze = 2408,
	AdjEnhanceFreeze = 2409,
	MulEnhanceFreeze = 2410,
	EnhanceHuaHun = 2411,
	EnhanceHuaHunMul = 2412,
	TrueSightProb = 2413,
	LSBBGrade = 2414,
	LSBBQuality = 2415,
	LSBBAdjHp = 2416,
	AdjIgnoreAntiFire = 2417,
	AdjIgnoreAntiThunder = 2418,
	AdjIgnoreAntiIce = 2419,
	AdjIgnoreAntiPoison = 2420,
	AdjIgnoreAntiWind = 2421,
	AdjIgnoreAntiLight = 2422,
	AdjIgnoreAntiIllusion = 2423,
	IgnoreAntiFireLSMul = 2424,
	IgnoreAntiThunderLSMul = 2425,
	IgnoreAntiIceLSMul = 2426,
	IgnoreAntiPoisonLSMul = 2427,
	IgnoreAntiWindLSMul = 2428,
	IgnoreAntiLightLSMul = 2429,
	IgnoreAntiIllusionLSMul = 2430,
	AdjIgnoreAntiWater = 2431,
	IgnoreAntiWaterLSMul = 2432,
	MulSpeed2 = 2433,
	MulConsumeMp = 2434,
	AdjAgi2 = 2435,
	AdjAntiBianHu2 = 2436,
	AdjAntiBind2 = 2437,
	AdjAntiBlind2 = 2438,
	AdjAntiBlock2 = 2439,
	AdjAntiBlockDamage2 = 2440,
	AdjAntiChaos2 = 2441,
	AdjAntiDecelerate2 = 2442,
	AdjAntiDizzy2 = 2443,
	AdjAntiFire2 = 2444,
	AdjAntiFreeze2 = 2445,
	AdjAntiIce2 = 2446,
	AdjAntiIllusion2 = 2447,
	AdjAntiLight2 = 2448,
	AdjAntimFatal2 = 2449,
	AdjAntimFatalDamage2 = 2450,
	AdjAntiPetrify2 = 2451,
	AdjAntipFatal2 = 2452,
	AdjAntipFatalDamage2 = 2453,
	AdjAntiPoison2 = 2454,
	AdjAntiSilence2 = 2455,
	AdjAntiSleep2 = 2456,
	AdjAntiThunder2 = 2457,
	AdjAntiTie2 = 2458,
	AdjAntiWater2 = 2459,
	AdjAntiWind2 = 2460,
	AdjBlock2 = 2461,
	AdjBlockDamage2 = 2462,
	AdjCor2 = 2463,
	AdjEnhanceBianHu2 = 2464,
	AdjEnhanceBind2 = 2465,
	AdjEnhanceChaos2 = 2466,
	AdjEnhanceDizzy2 = 2467,
	AdjEnhanceFire2 = 2468,
	AdjEnhanceFreeze2 = 2469,
	AdjEnhanceIce2 = 2470,
	AdjEnhanceIllusion2 = 2471,
	AdjEnhanceLight2 = 2472,
	AdjEnhancePetrify2 = 2473,
	AdjEnhancePoison2 = 2474,
	AdjEnhanceSilence2 = 2475,
	AdjEnhanceSleep2 = 2476,
	AdjEnhanceThunder2 = 2477,
	AdjEnhanceTie2 = 2478,
	AdjEnhanceWater2 = 2479,
	AdjEnhanceWind2 = 2480,
	AdjIgnoreAntiFire2 = 2481,
	AdjIgnoreAntiIce2 = 2482,
	AdjIgnoreAntiIllusion2 = 2483,
	AdjIgnoreAntiLight2 = 2484,
	AdjIgnoreAntiPoison2 = 2485,
	AdjIgnoreAntiThunder2 = 2486,
	AdjIgnoreAntiWater2 = 2487,
	AdjIgnoreAntiWind2 = 2488,
	AdjInt2 = 2489,
	AdjmDef2 = 2490,
	AdjmFatal2 = 2491,
	AdjmFatalDamage2 = 2492,
	AdjmHit2 = 2493,
	AdjmHurt2 = 2494,
	AdjmMiss2 = 2495,
	AdjmSpeed2 = 2496,
	AdjpDef2 = 2497,
	AdjpFatal2 = 2498,
	AdjpFatalDamage2 = 2499,
	AdjpHit2 = 2500,
	AdjpHurt2 = 2501,
	AdjpMiss2 = 2502,
	AdjpSpeed2 = 2503,
	AdjStr2 = 2504,
	LSpAttMin = 2505,
	LSpAttMax = 2506,
	LSmAttMin = 2507,
	LSmAttMax = 2508,
	LSpFatal = 2509,
	LSpFatalDamage = 2510,
	LSmFatal = 2511,
	LSmFatalDamage = 2512,
	LSAntiBlock = 2513,
	LSAntiBlockDamage = 2514,
	LSIgnoreAntiFire = 2515,
	LSIgnoreAntiThunder = 2516,
	LSIgnoreAntiIce = 2517,
	LSIgnoreAntiPoison = 2518,
	LSIgnoreAntiWind = 2519,
	LSIgnoreAntiLight = 2520,
	LSIgnoreAntiIllusion = 2521,
	LSIgnoreAntiWater = 2522,
	LSpAttMin_adj = 2523,
	LSpAttMax_adj = 2524,
	LSmAttMin_adj = 2525,
	LSmAttMax_adj = 2526,
	LSpFatal_adj = 2527,
	LSpFatalDamage_adj = 2528,
	LSmFatal_adj = 2529,
	LSmFatalDamage_adj = 2530,
	LSAntiBlock_adj = 2531,
	LSAntiBlockDamage_adj = 2532,
	LSIgnoreAntiFire_adj = 2533,
	LSIgnoreAntiThunder_adj = 2534,
	LSIgnoreAntiIce_adj = 2535,
	LSIgnoreAntiPoison_adj = 2536,
	LSIgnoreAntiWind_adj = 2537,
	LSIgnoreAntiLight_adj = 2538,
	LSIgnoreAntiIllusion_adj = 2539,
	LSIgnoreAntiWater_adj = 2540,
	LSpAttMin_jc = 2541,
	LSpAttMax_jc = 2542,
	LSmAttMin_jc = 2543,
	LSmAttMax_jc = 2544,
	LSpFatal_jc = 2545,
	LSpFatalDamage_jc = 2546,
	LSmFatal_jc = 2547,
	LSmFatalDamage_jc = 2548,
	LSAntiBlock_jc = 2549,
	LSAntiBlockDamage_jc = 2550,
	LSIgnoreAntiFire_jc = 2551,
	LSIgnoreAntiThunder_jc = 2552,
	LSIgnoreAntiIce_jc = 2553,
	LSIgnoreAntiPoison_jc = 2554,
	LSIgnoreAntiWind_jc = 2555,
	LSIgnoreAntiLight_jc = 2556,
	LSIgnoreAntiIllusion_jc = 2557,
	LSIgnoreAntiWater_jc = 2558,
	LSIgnoreAntiFire_mul = 2559,
	LSIgnoreAntiThunder_mul = 2560,
	LSIgnoreAntiIce_mul = 2561,
	LSIgnoreAntiPoison_mul = 2562,
	LSIgnoreAntiWind_mul = 2563,
	LSIgnoreAntiLight_mul = 2564,
	LSIgnoreAntiIllusion_mul = 2565,
	LSIgnoreAntiWater_mul = 2566,
	JieBan_Child_QiChangColor = 2567,
	JieBan_LingShou_Ratio = 2568,
	JieBan_LingShou_QinMi = 2569,
	JieBan_LingShou_CorZizhi = 2570,
	JieBan_LingShou_StaZizhi = 2571,
	JieBan_LingShou_StrZizhi = 2572,
	JieBan_LingShou_IntZizhi = 2573,
	JieBan_LingShou_AgiZizhi = 2574,
	Buff_DisplayValue1 = 2575,
	Buff_DisplayValue2 = 2576,
	Buff_DisplayValue3 = 2577,
	Buff_DisplayValue4 = 2578,
	Buff_DisplayValue5 = 2579,
	LSNature = 2580,
	PrayMulti = 2581,
	EnhanceYingLing = 2582,
	EnhanceYingLingMul = 2583,
	EnhanceFlag = 2584,
	EnhanceFlagMul = 2585,
	ObjectType = 2586,
	MonsterType = 2587,
	FightPropType = 2588,
	PlayHpFull = 2589,
	PlayHp = 2590,
	PlayAtt = 2591,
	PlayDef = 2592,
	PlayHit = 2593,
	PlayMiss = 2594,
	EnhanceDieKe = 2595,
	EnhanceDieKeMul = 2596,
	DotRemain = 2597,
	IsConfused = 2598,
	EnhanceSheShouKefu = 2599,
	EnhanceJiaShiKefu = 2600,
	EnhanceDaoKeKefu = 2601,
	EnhanceXiaKeKefu = 2602,
	EnhanceFangShiKefu = 2603,
	EnhanceYiShiKefu = 2604,
	EnhanceMeiZheKefu = 2605,
	EnhanceYiRenKefu = 2606,
	EnhanceYanShiKefu = 2607,
	EnhanceHuaHunKefu = 2608,
	EnhanceYingLingKefu = 2609,
	EnhanceDieKeKefu = 2610,
	AdjustPermanentCor = 2611,
	AdjustPermanentSta = 2612,
	AdjustPermanentStr = 2613,
	AdjustPermanentInt = 2614,
	AdjustPermanentAgi = 2615,
	SoulCoreLevel = 2616,
	AdjustPermanentMidCor = 2617,
	AdjustPermanentMidSta = 2618,
	AdjustPermanentMidStr = 2619,
	AdjustPermanentMidInt = 2620,
	AdjustPermanentMidAgi = 2621,
	SumPermanent = 2622,
	AntiTian = 2623,
	AntiEGui = 2624,
	AntiXiuLuo = 2625,
	AntiDiYu = 2626,
	AntiRen = 2627,
	AntiChuSheng = 2628,
	AntiBuilding = 2629,
	AntiZhaoHuan = 2630,
	AntiLingShou = 2631,
	AntiBoss = 2632,
	AntiSheShou = 2633,
	AntiJiaShi = 2634,
	AntiDaoKe = 2635,
	AntiXiaKe = 2636,
	AntiFangShi = 2637,
	AntiYiShi = 2638,
	AntiMeiZhe = 2639,
	AntiYiRen = 2640,
	AntiYanShi = 2641,
	AntiHuaHun = 2642,
	AntiYingLing = 2643,
	AntiDieKe = 2644,
	AntiFlag = 2645,
	EnhanceZhanKuang = 2646,
	EnhanceZhanKuangMul = 2647,
	EnhanceZhanKuangKefu = 2648,
	AntiZhanKuang = 2649,
	JumpSpeed = 2650,
	OriJumpSpeed = 2651,
	AdjJumpSpeed = 2652,
	GravityAcceleration = 2653,
	OriGravityAcceleration = 2654,
	AdjGravityAcceleration = 2655,
	HorizontalAcceleration = 2656,
	OriHorizontalAcceleration = 2657,
	AdjHorizontalAcceleration = 2658,
	SwimSpeed = 2659,
	OriSwimSpeed = 2660,
	AdjSwimSpeed = 2661,
	StrengthPoint = 2662,
	StrengthPointMax = 2663,
	OriStrengthPointMax = 2664,
	AdjStrengthPointMax = 2665,
	AntiMulEnhanceIllusion = 2666,
	GlideHorizontalSpeedWithUmbrella = 2667,
	GlideVerticalSpeedWithUmbrella = 2668,
	AdjpSpeed = 3001,
	AdjmSpeed = 3002,
	
}


CLuaMonsterPropertyFight = {}

CLuaMonsterPropertyFight.Param = nil
CLuaMonsterPropertyFight.TmpParam = nil
local function SetParam_At(id, v)
	CLuaMonsterPropertyFight.Param[id] = v
end
local function GetParam_At(id)
	return CLuaMonsterPropertyFight.Param[id] or 0
end
local function SetTmpParam_At(id, v)
	CLuaMonsterPropertyFight.TmpParam[id] = v
end
local function GetTmpParam_At(id)
	return CLuaMonsterPropertyFight.TmpParam[id] or 0
end

local function LookUpGrow1(clazz, target, keys, defaultValue)
	local key = 0
	for i,v in ipairs(keys) do
		if i==1 then
			key = v*1000
		else
			key = key + v
		end
	end
	local data = Grow_Grow1.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local function LookUpGrow2(clazz, target, keys, defaultValue)
	local key = ""
	for i,v in ipairs(keys) do
		if i==1 then
			key = tostring(v)
		else
			key = key.."_"..v
		end
	end
	local data = Grow_Grow2.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local CMonsterPropertyFight = import "L10.Game.CMonsterPropertyFight"
local SetParamFunctions = {}
CMonsterPropertyFight.m_hookSetParam = function(this,id,newv)
	CLuaMonsterPropertyFight.Param = this.Param
	CLuaMonsterPropertyFight.TmpParam = this.TmpParam
	if SetParamFunctions[id] then
		SetParamFunctions[id](newv)
	end
	CLuaMonsterPropertyFight.Param = nil
	CLuaMonsterPropertyFight.TmpParam = nil
end
CLuaMonsterPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaMonsterPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaMonsterPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaMonsterPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaMonsterPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaMonsterPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)
end
CLuaMonsterPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)
end
CLuaMonsterPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()

	CLuaMonsterPropertyFight.RefreshParampDef()

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampHit()

	CLuaMonsterPropertyFight.RefreshParammFatal()

	CLuaMonsterPropertyFight.RefreshParampFatal()

	CLuaMonsterPropertyFight.RefreshParammHit()

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParammSpeed()

	CLuaMonsterPropertyFight.RefreshParampAttMax()

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParampMiss()

	CLuaMonsterPropertyFight.RefreshParammDef()

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()

	CLuaMonsterPropertyFight.RefreshParamMpFull()

	CLuaMonsterPropertyFight.RefreshParammAttMax()

	CLuaMonsterPropertyFight.RefreshParampSpeed()

	CLuaMonsterPropertyFight.RefreshParamMpRecover()

	CLuaMonsterPropertyFight.RefreshParammMiss()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamCor = function(newv)
	SetTmpParam_At(3, newv)
end
CLuaMonsterPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)
end
CLuaMonsterPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)
end
CLuaMonsterPropertyFight.SetParamSta = function(newv)
	SetTmpParam_At(6, newv)
end
CLuaMonsterPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)
end
CLuaMonsterPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)
end
CLuaMonsterPropertyFight.SetParamStr = function(newv)
	SetTmpParam_At(9, newv)
end
CLuaMonsterPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)
end
CLuaMonsterPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)
end
CLuaMonsterPropertyFight.SetParamInt = function(newv)
	SetTmpParam_At(12, newv)
end
CLuaMonsterPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)
end
CLuaMonsterPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)
end
CLuaMonsterPropertyFight.SetParamAgi = function(newv)
	SetTmpParam_At(15, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)
end
CLuaMonsterPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)
end
CLuaMonsterPropertyFight.RefreshParamHpFull = function()
	local AdjHpFull = GetTmpParam_At(19)
	local MulHpFull = GetTmpParam_At(20)
	local HpFullInit = GetTmpParam_At(243)
	local HpFullPow1 = GetTmpParam_At(242)
	local HpFullPow2 = GetTmpParam_At(241)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( HpFullPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ HpFullPow1* max(Grade,MinGrade)+ HpFullInit +AdjHpFull )*(1+MulHpFull))
	SetTmpParam_At(18, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.RefreshParamHpRecover = function()
	local AdjHpRecover = GetTmpParam_At(22)
	local MulHpRecover = GetTmpParam_At(23)
	local HpRecoverPow1 = GetTmpParam_At(283)
	local HpRecoverInit = GetTmpParam_At(284)
	local HpFull = GetTmpParam_At(18)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( HpRecoverPow1* max(Grade,MinGrade)+ HpRecoverInit*HpFull + AdjHpRecover)*(1+ MulHpRecover)
	SetTmpParam_At(21, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.RefreshParamMpFull = function()
	local AdjMpFull = GetTmpParam_At(25)
	local MulMpFull = GetTmpParam_At(26)
	local MpFullInit = GetTmpParam_At(245)
	local MpFullPow1 = GetTmpParam_At(244)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( MpFullPow1* max(Grade,MinGrade)+ MpFullInit + AdjMpFull)*(1+MulMpFull))
	SetTmpParam_At(24, newv)
end
CLuaMonsterPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.RefreshParamMpRecover = function()
	local AdjMpRecover = GetTmpParam_At(28)
	local MulMpRecover = GetTmpParam_At(29)
	local MpRecoverInit = GetTmpParam_At(247)
	local MpRecoverPow1 = GetTmpParam_At(246)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MpRecoverPow1* max(Grade,MinGrade)+ MpRecoverInit + AdjMpRecover )*(1+ MulMpRecover)
	SetTmpParam_At(27, newv)
end
CLuaMonsterPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.RefreshParampAttMin = function()
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local AdjpAttMin = GetTmpParam_At(31)
	local MulpAttMin = GetTmpParam_At(32)
	local PAttMaxInit = GetTmpParam_At(253)
	local PAttMinInit = GetTmpParam_At(250)
	local PAttMaxPow1 = GetTmpParam_At(252)
	local PAttMinPow1 = GetTmpParam_At(249)
	local PAttMaxPow2 = GetTmpParam_At(251)
	local PAttMinPow2 = GetTmpParam_At(248)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,min(( PAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ PAttMaxPow1* max(Grade,MinGrade)+ PAttMaxInit + AdjpAttMax )*(1+ MulpAttMax),( PAttMinPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ PAttMinPow1* max(Grade,MinGrade)+ PAttMinInit + AdjpAttMin )*(1+ MulpAttMin)))
	SetTmpParam_At(30, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.RefreshParampAttMax = function()
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local PAttMaxInit = GetTmpParam_At(253)
	local PAttMaxPow1 = GetTmpParam_At(252)
	local PAttMaxPow2 = GetTmpParam_At(251)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( PAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ PAttMaxPow1* max(Grade,MinGrade)+ PAttMaxInit + AdjpAttMax )*(1+ MulpAttMax))
	SetTmpParam_At(33, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.RefreshParampHit = function()
	local AdjpHit = GetTmpParam_At(37)
	local MulpHit = GetTmpParam_At(38)
	local PHitInit = GetTmpParam_At(255)
	local PhitPow1 = GetTmpParam_At(254)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PhitPow1* max(Grade,MinGrade)+ PHitInit + AdjpHit )*(1+ MulpHit)
	SetTmpParam_At(36, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.RefreshParampMiss = function()
	local AdjpMiss = GetTmpParam_At(40)
	local MulpMiss = GetTmpParam_At(41)
	local PMissInit = GetTmpParam_At(257)
	local PMissPow1 = GetTmpParam_At(256)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PMissPow1* max(Grade,MinGrade)+ PMissInit + AdjpMiss )*(1+ MulpMiss)
	SetTmpParam_At(39, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.RefreshParampDef = function()
	local AdjpDef = GetTmpParam_At(43)
	local MulpDef = GetTmpParam_At(44)
	local PDefInit = GetTmpParam_At(261)
	local PDefPow1 = GetTmpParam_At(260)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( PDefPow1* max(Grade,MinGrade)+ PDefInit + AdjpDef )*(1+ MulpDef))
	SetTmpParam_At(42, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaMonsterPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaMonsterPropertyFight.RefreshParampSpeed = function()
	local AdjpSpeed = GetTmpParam_At(1001)
	local MulpSpeed = GetTmpParam_At(49)
	local OripSpeed = GetTmpParam_At(48)
	local PSpeedPow1 = GetTmpParam_At(258)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PSpeedPow1* max(Grade,MinGrade)+ OripSpeed+AdjpSpeed) * (1+ MulpSpeed)
	SetTmpParam_At(47, newv)
end
CLuaMonsterPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.RefreshParampFatal = function()
	local AdjpFatal = GetTmpParam_At(51)
	local PFatalInit = GetTmpParam_At(263)
	local PfatalPow1 = GetTmpParam_At(262)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PfatalPow1* max(Grade,MinGrade)+ PFatalInit + AdjpFatal )
	SetTmpParam_At(50, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)

	CLuaMonsterPropertyFight.RefreshParampFatal()
end
CLuaMonsterPropertyFight.RefreshParamAntipFatal = function()
	local AdjAntipFatal = GetTmpParam_At(53)

	local newv = AdjAntipFatal
	SetTmpParam_At(52, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)

	CLuaMonsterPropertyFight.RefreshParamAntipFatal()
end
CLuaMonsterPropertyFight.RefreshParampFatalDamage = function()
	local AdjpFatalDamage = GetTmpParam_At(55)

	local newv = (AdjpFatalDamage + 0.5)
	SetTmpParam_At(54, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)

	CLuaMonsterPropertyFight.RefreshParampFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamAntipFatalDamage = function()
	local AdjAntipFatalDamage = GetTmpParam_At(57)

	local newv = AdjAntipFatalDamage
	SetTmpParam_At(56, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)

	CLuaMonsterPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamBlock = function()
	local AdjBlock = GetTmpParam_At(59)
	local MulBlock = GetTmpParam_At(60)

	local newv = AdjBlock * (1+MulBlock)
	SetTmpParam_At(58, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)

	CLuaMonsterPropertyFight.RefreshParamBlock()
end
CLuaMonsterPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)

	CLuaMonsterPropertyFight.RefreshParamBlock()
end
CLuaMonsterPropertyFight.RefreshParamBlockDamage = function()
	local AdjBlockDamage = GetTmpParam_At(62)
	local BlockDamageInit = GetTmpParam_At(281)
	local BlockDamagePow1 = GetTmpParam_At(280)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(0,( BlockDamagePow1* max(Grade,MinGrade)+ BlockDamageInit + AdjBlockDamage))
	SetTmpParam_At(61, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()
end
CLuaMonsterPropertyFight.RefreshParamAntiBlock = function()
	local AdjAntiBlock = GetTmpParam_At(64)

	local newv = AdjAntiBlock
	SetTmpParam_At(63, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlock()
end
CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage = function()
	local AntiBlockDamageInit = GetTmpParam_At(286)
	local AdjAntiBlockDamage = GetTmpParam_At(66)
	local AntiBlockDamagePow1 = GetTmpParam_At(285)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( AdjAntiBlockDamage + AntiBlockDamagePow1*max(Grade,MinGrade) + AntiBlockDamageInit )
	SetTmpParam_At(65, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaMonsterPropertyFight.RefreshParammAttMin = function()
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local AdjmAttMin = GetTmpParam_At(68)
	local MulmAttMin = GetTmpParam_At(69)
	local MAttMaxInit = GetTmpParam_At(269)
	local MAttMinInit = GetTmpParam_At(266)
	local MAttMaxPow1 = GetTmpParam_At(268)
	local MAttMinPow1 = GetTmpParam_At(265)
	local MAttMaxPow2 = GetTmpParam_At(267)
	local MAttMinPow2 = GetTmpParam_At(264)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,min(( MAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ MAttMaxPow1* max(Grade,MinGrade)+ MAttMaxInit + AdjmAttMax)*(1+ MulmAttMax),( MAttMinPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ MAttMinPow1* max(Grade,MinGrade)+ MAttMinInit + AdjmAttMin)*(1+ MulmAttMin)))
	SetTmpParam_At(67, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.RefreshParammAttMax = function()
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local MAttMaxInit = GetTmpParam_At(269)
	local MAttMaxPow1 = GetTmpParam_At(268)
	local MAttMaxPow2 = GetTmpParam_At(267)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( MAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ MAttMaxPow1* max(Grade,MinGrade)+ MAttMaxInit + AdjmAttMax)*(1+ MulmAttMax))
	SetTmpParam_At(70, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.RefreshParammHit = function()
	local AdjmHit = GetTmpParam_At(74)
	local MulmHit = GetTmpParam_At(75)
	local MHitInit = GetTmpParam_At(275)
	local MHitPow1 = GetTmpParam_At(274)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MHitPow1* max(Grade,MinGrade)+ MHitInit + AdjmHit)*(1+ MulmHit)
	SetTmpParam_At(73, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.RefreshParammMiss = function()
	local AdjmMiss = GetTmpParam_At(77)
	local MulmMiss = GetTmpParam_At(78)
	local MMissInit = GetTmpParam_At(277)
	local MMissPow1 = GetTmpParam_At(276)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MMissPow1* max(Grade,MinGrade)+ MMissInit + AdjmMiss)*(1+ MulmMiss)
	SetTmpParam_At(76, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.RefreshParammDef = function()
	local AdjmDef = GetTmpParam_At(80)
	local MulmDef = GetTmpParam_At(81)
	local MDefInit = GetTmpParam_At(273)
	local MDefPow1 = GetTmpParam_At(272)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( MDefPow1* max(Grade,MinGrade)+ MDefInit + AdjmDef )*(1+ MulmDef))
	SetTmpParam_At(79, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaMonsterPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaMonsterPropertyFight.RefreshParammSpeed = function()
	local AdjmSpeed = GetTmpParam_At(1002)
	local MulpSpeed = GetTmpParam_At(49)
	local OrimSpeed = GetTmpParam_At(85)
	local MSpeedPow1 = GetTmpParam_At(270)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MSpeedPow1* max(Grade,MinGrade)+ OrimSpeed+AdjmSpeed)*(1+ MulpSpeed)
	SetTmpParam_At(84, newv)
end
CLuaMonsterPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()
end
CLuaMonsterPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)
end
CLuaMonsterPropertyFight.RefreshParammFatal = function()
	local AdjmFatal = GetTmpParam_At(88)
	local MFatalInit = GetTmpParam_At(279)
	local MFatalPow1 = GetTmpParam_At(278)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MFatalPow1* max(Grade,MinGrade)+ MFatalInit + AdjmFatal )
	SetTmpParam_At(87, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)

	CLuaMonsterPropertyFight.RefreshParammFatal()
end
CLuaMonsterPropertyFight.RefreshParamAntimFatal = function()
	local AdjAntimFatal = GetTmpParam_At(90)

	local newv = AdjAntimFatal
	SetTmpParam_At(89, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)

	CLuaMonsterPropertyFight.RefreshParamAntimFatal()
end
CLuaMonsterPropertyFight.RefreshParammFatalDamage = function()
	local AdjmFatalDamage = GetTmpParam_At(92)

	local newv = (AdjmFatalDamage+0.5)
	SetTmpParam_At(91, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)

	CLuaMonsterPropertyFight.RefreshParammFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamAntimFatalDamage = function()
	local AdjAntimFatalDamage = GetTmpParam_At(94)

	local newv = AdjAntimFatalDamage
	SetTmpParam_At(93, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)

	CLuaMonsterPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceFire = function()
	local AdjEnhanceFire = GetTmpParam_At(96)

	local newv = AdjEnhanceFire
	SetTmpParam_At(95, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceFire()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceThunder = function()
	local AdjEnhanceThunder = GetTmpParam_At(99)

	local newv = AdjEnhanceThunder
	SetTmpParam_At(98, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceThunder()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceIce = function()
	local AdjEnhanceIce = GetTmpParam_At(102)

	local newv = AdjEnhanceIce
	SetTmpParam_At(101, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceIce()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhancePoison = function()
	local AdjEnhancePoison = GetTmpParam_At(105)

	local newv = AdjEnhancePoison
	SetTmpParam_At(104, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhancePoison()
end
CLuaMonsterPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceWind = function()
	local AdjEnhanceWind = GetTmpParam_At(108)

	local newv = AdjEnhanceWind
	SetTmpParam_At(107, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceWind()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceLight = function()
	local AdjEnhanceLight = GetTmpParam_At(111)

	local newv = AdjEnhanceLight
	SetTmpParam_At(110, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceLight()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceIllusion = function()
	local AdjEnhanceIllusion = GetTmpParam_At(114)

	local newv = AdjEnhanceIllusion
	SetTmpParam_At(113, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiFire = function()
	local AdjAntiFire = GetTmpParam_At(117)
	local MulAntiFire = GetTmpParam_At(118)

	local newv = AdjAntiFire*(1+MulAntiFire)
	SetTmpParam_At(116, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFire()
end
CLuaMonsterPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFire()
end
CLuaMonsterPropertyFight.RefreshParamAntiThunder = function()
	local AdjAntiThunder = GetTmpParam_At(120)
	local MulAntiThunder = GetTmpParam_At(121)

	local newv = AdjAntiThunder*(1+MulAntiThunder)
	SetTmpParam_At(119, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiThunder()
end
CLuaMonsterPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiThunder()
end
CLuaMonsterPropertyFight.RefreshParamAntiIce = function()
	local AdjAntiIce = GetTmpParam_At(123)
	local MulAntiIce = GetTmpParam_At(124)

	local newv = AdjAntiIce*(1+MulAntiIce)
	SetTmpParam_At(122, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIce()
end
CLuaMonsterPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIce()
end
CLuaMonsterPropertyFight.RefreshParamAntiPoison = function()
	local AdjAntiPoison = GetTmpParam_At(126)
	local MulAntiPoison = GetTmpParam_At(127)

	local newv = AdjAntiPoison*(1+MulAntiPoison)
	SetTmpParam_At(125, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPoison()
end
CLuaMonsterPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPoison()
end
CLuaMonsterPropertyFight.RefreshParamAntiWind = function()
	local AdjAntiWind = GetTmpParam_At(129)
	local MulAntiWind = GetTmpParam_At(130)

	local newv = AdjAntiWind*(1+MulAntiWind)
	SetTmpParam_At(128, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWind()
end
CLuaMonsterPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWind()
end
CLuaMonsterPropertyFight.RefreshParamAntiLight = function()
	local AdjAntiLight = GetTmpParam_At(132)
	local MulAntiLight = GetTmpParam_At(133)

	local newv = AdjAntiLight*(1+MulAntiLight)
	SetTmpParam_At(131, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiLight()
end
CLuaMonsterPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiLight()
end
CLuaMonsterPropertyFight.RefreshParamAntiIllusion = function()
	local AdjAntiIllusion = GetTmpParam_At(135)
	local MulAntiIllusion = GetTmpParam_At(136)

	local newv = AdjAntiIllusion*(1+MulAntiIllusion)
	SetTmpParam_At(134, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIllusion()
end
CLuaMonsterPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIllusion()
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiFire = function()
	local AdjIgnoreAntiFire = GetTmpParam_At(417)

	local newv = AdjIgnoreAntiFire
	SetTmpParam_At(137, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiThunder = function()
	local AdjIgnoreAntiThunder = GetTmpParam_At(418)

	local newv = AdjIgnoreAntiThunder
	SetTmpParam_At(138, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIce = function()
	local AdjIgnoreAntiIce = GetTmpParam_At(419)

	local newv = AdjIgnoreAntiIce
	SetTmpParam_At(139, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiPoison = function()
	local AdjIgnoreAntiPoison = GetTmpParam_At(420)

	local newv = AdjIgnoreAntiPoison
	SetTmpParam_At(140, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWind = function()
	local AdjIgnoreAntiWind = GetTmpParam_At(421)

	local newv = AdjIgnoreAntiWind
	SetTmpParam_At(141, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiLight = function()
	local AdjIgnoreAntiLight = GetTmpParam_At(422)

	local newv = AdjIgnoreAntiLight
	SetTmpParam_At(142, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIllusion = function()
	local AdjIgnoreAntiIllusion = GetTmpParam_At(423)

	local newv = AdjIgnoreAntiIllusion
	SetTmpParam_At(143, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceDizzy = function()
	local AdjEnhanceDizzy = GetTmpParam_At(145)
	local MulEnhanceDizzy = GetTmpParam_At(146)

	local newv = AdjEnhanceDizzy*(1+MulEnhanceDizzy)
	SetTmpParam_At(144, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceSleep = function()
	local AdjEnhanceSleep = GetTmpParam_At(148)
	local MulEnhanceSleep = GetTmpParam_At(149)

	local newv = AdjEnhanceSleep*(1+MulEnhanceSleep)
	SetTmpParam_At(147, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSleep()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSleep()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceChaos = function()
	local AdjEnhanceChaos = GetTmpParam_At(151)
	local MulEnhanceChaos = GetTmpParam_At(152)

	local newv = AdjEnhanceChaos*(1+MulEnhanceChaos)
	SetTmpParam_At(150, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceChaos()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceChaos()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceBind = function()
	local AdjEnhanceBind = GetTmpParam_At(154)
	local MulEnhanceBind = GetTmpParam_At(155)

	local newv = AdjEnhanceBind*(1+MulEnhanceBind)
	SetTmpParam_At(153, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBind()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBind()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceSilence = function()
	local AdjEnhanceSilence = GetTmpParam_At(157)
	local MulEnhanceSilence = GetTmpParam_At(158)

	local newv = AdjEnhanceSilence*(1+MulEnhanceSilence)
	SetTmpParam_At(156, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSilence()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSilence()
end
CLuaMonsterPropertyFight.RefreshParamAntiDizzy = function()
	local AdjAntiDizzy = GetTmpParam_At(160)
	local MulAntiDizzy = GetTmpParam_At(161)

	local newv = AdjAntiDizzy*(1+MulAntiDizzy)
	SetTmpParam_At(159, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDizzy()
end
CLuaMonsterPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDizzy()
end
CLuaMonsterPropertyFight.RefreshParamAntiSleep = function()
	local AdjAntiSleep = GetTmpParam_At(163)
	local MulAntiSleep = GetTmpParam_At(164)

	local newv = AdjAntiSleep*(1+MulAntiSleep)
	SetTmpParam_At(162, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSleep()
end
CLuaMonsterPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSleep()
end
CLuaMonsterPropertyFight.RefreshParamAntiChaos = function()
	local AdjAntiChaos = GetTmpParam_At(166)
	local MulAntiChaos = GetTmpParam_At(167)

	local newv = AdjAntiChaos*(1+MulAntiChaos)
	SetTmpParam_At(165, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiChaos()
end
CLuaMonsterPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiChaos()
end
CLuaMonsterPropertyFight.RefreshParamAntiBind = function()
	local AdjAntiBind = GetTmpParam_At(169)
	local MulAntiBind = GetTmpParam_At(170)

	local newv = AdjAntiBind*(1+MulAntiBind)
	SetTmpParam_At(168, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBind()
end
CLuaMonsterPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBind()
end
CLuaMonsterPropertyFight.RefreshParamAntiSilence = function()
	local AdjAntiSilence = GetTmpParam_At(172)
	local MulAntiSilence = GetTmpParam_At(173)

	local newv = AdjAntiSilence*(1+MulAntiSilence)
	SetTmpParam_At(171, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSilence()
end
CLuaMonsterPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSilence()
end
CLuaMonsterPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaMonsterPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaMonsterPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaMonsterPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaMonsterPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaMonsterPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaMonsterPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaMonsterPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceHeal = function()
	local AdjEnhanceHeal = GetTmpParam_At(183)

	local newv = AdjEnhanceHeal
	SetTmpParam_At(182, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceHeal()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaMonsterPropertyFight.RefreshParamSpeed = function()
	local OriSpeed = GetTmpParam_At(186)
	local AdjSpeed = GetTmpParam_At(187)
	local MulSpeed2 = GetTmpParam_At(433)
	local MulSpeed = GetTmpParam_At(188)

	local newv = (OriSpeed + AdjSpeed)*max(0.3,1+MulSpeed+MulSpeed2)
	SetTmpParam_At(185, newv)
end
CLuaMonsterPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamMF = function(newv)
	SetTmpParam_At(189, newv)
end
CLuaMonsterPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)
end
CLuaMonsterPropertyFight.SetParamRange = function(newv)
	SetTmpParam_At(191, newv)
end
CLuaMonsterPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)
end
CLuaMonsterPropertyFight.SetParamHatePlus = function(newv)
	SetTmpParam_At(193, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)
end
CLuaMonsterPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaMonsterPropertyFight.RefreshParamInvisible = function()
	local AdjInvisible = GetTmpParam_At(197)

	local newv = AdjInvisible
	SetTmpParam_At(196, newv)
end
CLuaMonsterPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)

	CLuaMonsterPropertyFight.RefreshParamInvisible()
end
CLuaMonsterPropertyFight.RefreshParamTrueSight = function()
	local OriTrueSight = GetTmpParam_At(199)
	local AdjTrueSight = GetTmpParam_At(200)

	local newv = OriTrueSight + AdjTrueSight
	SetTmpParam_At(198, newv)
end
CLuaMonsterPropertyFight.SetParamOriTrueSight = function(newv)
	SetTmpParam_At(199, newv)

	CLuaMonsterPropertyFight.RefreshParamTrueSight()
end
CLuaMonsterPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)

	CLuaMonsterPropertyFight.RefreshParamTrueSight()
end
CLuaMonsterPropertyFight.RefreshParamEyeSight = function()
	local OriEyeSight = GetTmpParam_At(202)
	local AdjEyeSight = GetTmpParam_At(203)

	local newv = OriEyeSight + AdjEyeSight
	SetTmpParam_At(201, newv)
end
CLuaMonsterPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)

	CLuaMonsterPropertyFight.RefreshParamEyeSight()
end
CLuaMonsterPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)

	CLuaMonsterPropertyFight.RefreshParamEyeSight()
end
CLuaMonsterPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaMonsterPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaMonsterPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaMonsterPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaMonsterPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaMonsterPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaMonsterPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaMonsterPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaMonsterPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)

	CLuaMonsterPropertyFight.RefreshParampFatal()
end
CLuaMonsterPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)

	CLuaMonsterPropertyFight.RefreshParampFatal()
end
CLuaMonsterPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()
end
CLuaMonsterPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaMonsterPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)

	CLuaMonsterPropertyFight.RefreshParammFatal()
end
CLuaMonsterPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)

	CLuaMonsterPropertyFight.RefreshParammFatal()
end
CLuaMonsterPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()
end
CLuaMonsterPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()
end
CLuaMonsterPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaMonsterPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaMonsterPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaMonsterPropertyFight.SetParamBBMax = function(newv)
	SetTmpParam_At(287, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiBreak = function()
	local AdjAntiBreak = GetTmpParam_At(290)

	local newv = AdjAntiBreak
	SetTmpParam_At(289, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBreak()
end
CLuaMonsterPropertyFight.RefreshParamAntiPushPull = function()
	local AdjAntiPushPull = GetTmpParam_At(292)

	local newv = AdjAntiPushPull
	SetTmpParam_At(291, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPushPull()
end
CLuaMonsterPropertyFight.RefreshParamAntiPetrify = function()
	local AdjAntiPetrify = GetTmpParam_At(294)
	local MulAntiPetrify = GetTmpParam_At(295)

	local newv = AdjAntiPetrify*(1+MulAntiPetrify)
	SetTmpParam_At(293, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPetrify()
end
CLuaMonsterPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPetrify()
end
CLuaMonsterPropertyFight.RefreshParamAntiTie = function()
	local AdjAntiTie = GetTmpParam_At(297)
	local MulAntiTie = GetTmpParam_At(298)

	local newv = AdjAntiTie*(1+MulAntiTie)
	SetTmpParam_At(296, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiTie()
end
CLuaMonsterPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiTie()
end
CLuaMonsterPropertyFight.RefreshParamEnhancePetrify = function()
	local AdjEnhancePetrify = GetTmpParam_At(300)
	local MulEnhancePetrify = GetTmpParam_At(301)

	local newv = AdjEnhancePetrify*(1+MulEnhancePetrify)
	SetTmpParam_At(299, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhancePetrify()
end
CLuaMonsterPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhancePetrify()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceTie = function()
	local AdjEnhanceTie = GetTmpParam_At(303)
	local MulEnhanceTie = GetTmpParam_At(304)

	local newv = AdjEnhanceTie*(1+MulEnhanceTie)
	SetTmpParam_At(302, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceTie()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceTie()
end
CLuaMonsterPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaMonsterPropertyFight.SetParamStrZizhi = function(newv)
	SetTmpParam_At(306, newv)
end
CLuaMonsterPropertyFight.SetParamCorZizhi = function(newv)
	SetTmpParam_At(307, newv)
end
CLuaMonsterPropertyFight.SetParamStaZizhi = function(newv)
	SetTmpParam_At(308, newv)
end
CLuaMonsterPropertyFight.SetParamAgiZizhi = function(newv)
	SetTmpParam_At(309, newv)
end
CLuaMonsterPropertyFight.SetParamIntZizhi = function(newv)
	SetTmpParam_At(310, newv)
end
CLuaMonsterPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)
end
CLuaMonsterPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)
end
CLuaMonsterPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)
end
CLuaMonsterPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)
end
CLuaMonsterPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)
end
CLuaMonsterPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)
end
CLuaMonsterPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)
end
CLuaMonsterPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)
end
CLuaMonsterPropertyFight.SetParamPType = function(newv)
	SetTmpParam_At(319, newv)
end
CLuaMonsterPropertyFight.SetParamMType = function(newv)
	SetTmpParam_At(320, newv)
end
CLuaMonsterPropertyFight.SetParamPHType = function(newv)
	SetTmpParam_At(321, newv)
end
CLuaMonsterPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)
end
CLuaMonsterPropertyFight.SetParamWuxingImprove = function(newv)
	SetTmpParam_At(323, newv)
end
CLuaMonsterPropertyFight.SetParamXiuweiImprove = function(newv)
	SetTmpParam_At(324, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceBeHeal = function()
	local AdjEnhanceBeHeal = GetTmpParam_At(326)

	local newv = AdjEnhanceBeHeal
	SetTmpParam_At(325, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBeHeal()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpCor = function(newv)
	SetTmpParam_At(328, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpSta = function(newv)
	SetTmpParam_At(329, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpStr = function(newv)
	SetTmpParam_At(330, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpInt = function(newv)
	SetTmpParam_At(331, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpAgi = function(newv)
	SetTmpParam_At(332, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpHpFull = function(newv)
	SetTmpParam_At(333, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpMpFull = function(newv)
	SetTmpParam_At(334, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppAttMin = function(newv)
	SetTmpParam_At(335, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppAttMax = function(newv)
	SetTmpParam_At(336, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppHit = function(newv)
	SetTmpParam_At(337, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppMiss = function(newv)
	SetTmpParam_At(338, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppDef = function(newv)
	SetTmpParam_At(339, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmAttMin = function(newv)
	SetTmpParam_At(340, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmAttMax = function(newv)
	SetTmpParam_At(341, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmHit = function(newv)
	SetTmpParam_At(342, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmMiss = function(newv)
	SetTmpParam_At(343, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmDef = function(newv)
	SetTmpParam_At(344, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)
end
CLuaMonsterPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)
end
CLuaMonsterPropertyFight.SetParamMHType = function(newv)
	SetTmpParam_At(351, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiAoe()
end
CLuaMonsterPropertyFight.RefreshParamAntiBlind = function()
	local AdjAntiBlind = GetTmpParam_At(357)
	local MulAntiBlind = GetTmpParam_At(358)

	local newv = AdjAntiBlind*(1+MulAntiBlind)
	SetTmpParam_At(356, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlind()
end
CLuaMonsterPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlind()
end
CLuaMonsterPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)
	SetTmpParam_At(359, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDecelerate()
end
CLuaMonsterPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDecelerate()
end
CLuaMonsterPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()

	CLuaMonsterPropertyFight.RefreshParampDef()

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampHit()

	CLuaMonsterPropertyFight.RefreshParammFatal()

	CLuaMonsterPropertyFight.RefreshParampFatal()

	CLuaMonsterPropertyFight.RefreshParammHit()

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParammSpeed()

	CLuaMonsterPropertyFight.RefreshParampAttMax()

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParampMiss()

	CLuaMonsterPropertyFight.RefreshParammDef()

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()

	CLuaMonsterPropertyFight.RefreshParamMpFull()

	CLuaMonsterPropertyFight.RefreshParammAttMax()

	CLuaMonsterPropertyFight.RefreshParampSpeed()

	CLuaMonsterPropertyFight.RefreshParamMpRecover()

	CLuaMonsterPropertyFight.RefreshParammMiss()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamCharacterCor = function(newv)
	SetTmpParam_At(363, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterSta = function(newv)
	SetTmpParam_At(364, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterStr = function(newv)
	SetTmpParam_At(365, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterInt = function(newv)
	SetTmpParam_At(366, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterAgi = function(newv)
	SetTmpParam_At(367, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaMonsterPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceBianHu = function()
	local AdjEnhanceBianHu = GetTmpParam_At(372)
	local MulEnhanceBianHu = GetTmpParam_At(373)

	local newv = AdjEnhanceBianHu*(1+MulEnhanceBianHu)
	SetTmpParam_At(371, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaMonsterPropertyFight.RefreshParamAntiBianHu = function()
	local AdjAntiBianHu = GetTmpParam_At(375)
	local MulAntiBianHu = GetTmpParam_At(376)

	local newv = AdjAntiBianHu*(1+MulAntiBianHu)
	SetTmpParam_At(374, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBianHu()
end
CLuaMonsterPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBianHu()
end
CLuaMonsterPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaMonsterPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaMonsterPropertyFight.SetParamPAType = function(newv)
	SetTmpParam_At(382, newv)
end
CLuaMonsterPropertyFight.SetParamMAType = function(newv)
	SetTmpParam_At(383, newv)
end
CLuaMonsterPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaMonsterPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaMonsterPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaMonsterPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaMonsterPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaMonsterPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaMonsterPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaMonsterPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaMonsterPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaMonsterPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaMonsterPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaMonsterPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaMonsterPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceWater = function()
	local AdjEnhanceWater = GetTmpParam_At(398)

	local newv = AdjEnhanceWater
	SetTmpParam_At(397, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceWater()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiWater = function()
	local AdjAntiWater = GetTmpParam_At(401)
	local MulAntiWater = GetTmpParam_At(402)

	local newv = AdjAntiWater*(1+MulAntiWater)
	SetTmpParam_At(400, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWater()
end
CLuaMonsterPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWater()
end
CLuaMonsterPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWater = function()
	local AdjIgnoreAntiWater = GetTmpParam_At(431)

	local newv = AdjIgnoreAntiWater
	SetTmpParam_At(404, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiFreeze = function()
	local AdjAntiFreeze = GetTmpParam_At(406)
	local MulAntiFreeze = GetTmpParam_At(407)

	local newv = AdjAntiFreeze*(1+MulAntiFreeze)
	SetTmpParam_At(405, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFreeze()
end
CLuaMonsterPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFreeze()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceFreeze = function()
	local AdjEnhanceFreeze = GetTmpParam_At(409)
	local MulEnhanceFreeze = GetTmpParam_At(410)

	local newv = AdjEnhanceFreeze*(1+MulEnhanceFreeze)
	SetTmpParam_At(408, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaMonsterPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaMonsterPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaMonsterPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)
end
CLuaMonsterPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)
end
CLuaMonsterPropertyFight.SetParamLSBBAdjHp = function(newv)
	SetTmpParam_At(416, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)
end
CLuaMonsterPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)
end
CLuaMonsterPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)
end
CLuaMonsterPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaMonsterPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMin = function(newv)
	SetTmpParam_At(505, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMax = function(newv)
	SetTmpParam_At(506, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMin = function(newv)
	SetTmpParam_At(507, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMax = function(newv)
	SetTmpParam_At(508, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatal = function(newv)
	SetTmpParam_At(509, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatalDamage = function(newv)
	SetTmpParam_At(510, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatal = function(newv)
	SetTmpParam_At(511, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatalDamage = function(newv)
	SetTmpParam_At(512, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlock = function(newv)
	SetTmpParam_At(513, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage = function(newv)
	SetTmpParam_At(514, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire = function(newv)
	SetTmpParam_At(515, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder = function(newv)
	SetTmpParam_At(516, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce = function(newv)
	SetTmpParam_At(517, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison = function(newv)
	SetTmpParam_At(518, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind = function(newv)
	SetTmpParam_At(519, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight = function(newv)
	SetTmpParam_At(520, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(521, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater = function(newv)
	SetTmpParam_At(522, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaMonsterPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaMonsterPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaMonsterPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaMonsterPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaMonsterPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaMonsterPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaMonsterPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaMonsterPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaMonsterPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaMonsterPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaMonsterPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaMonsterPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaMonsterPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentCor = function(newv)
	SetTmpParam_At(611, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentSta = function(newv)
	SetTmpParam_At(612, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentStr = function(newv)
	SetTmpParam_At(613, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentInt = function(newv)
	SetTmpParam_At(614, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentAgi = function(newv)
	SetTmpParam_At(615, newv)
end
CLuaMonsterPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidCor = function(newv)
	SetTmpParam_At(617, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidSta = function(newv)
	SetTmpParam_At(618, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidStr = function(newv)
	SetTmpParam_At(619, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidInt = function(newv)
	SetTmpParam_At(620, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidAgi = function(newv)
	SetTmpParam_At(621, newv)
end
CLuaMonsterPropertyFight.SetParamSumPermanent = function(newv)
	SetTmpParam_At(622, newv)
end
CLuaMonsterPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaMonsterPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaMonsterPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaMonsterPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaMonsterPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaMonsterPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaMonsterPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaMonsterPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaMonsterPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaMonsterPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaMonsterPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaMonsterPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaMonsterPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaMonsterPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaMonsterPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaMonsterPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaMonsterPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaMonsterPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaMonsterPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaMonsterPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaMonsterPropertyFight.SetParamJumpSpeed = function(newv)
	SetTmpParam_At(650, newv)
end
CLuaMonsterPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)
end
CLuaMonsterPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)
end
CLuaMonsterPropertyFight.SetParamGravityAcceleration = function(newv)
	SetTmpParam_At(653, newv)
end
CLuaMonsterPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)
end
CLuaMonsterPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)
end
CLuaMonsterPropertyFight.SetParamHorizontalAcceleration = function(newv)
	SetTmpParam_At(656, newv)
end
CLuaMonsterPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)
end
CLuaMonsterPropertyFight.SetParamSwimSpeed = function(newv)
	SetTmpParam_At(659, newv)
end
CLuaMonsterPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)
end
CLuaMonsterPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)
end
CLuaMonsterPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaMonsterPropertyFight.SetParamStrengthPointMax = function(newv)
	SetTmpParam_At(663, newv)
end
CLuaMonsterPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)
end
CLuaMonsterPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)
end
CLuaMonsterPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaMonsterPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaMonsterPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()
end
CLuaMonsterPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)
end
CLuaMonsterPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaMonsterPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaMonsterPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaMonsterPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaMonsterPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaMonsterPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)
end
CLuaMonsterPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)
end
CLuaMonsterPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)
end
CLuaMonsterPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()

	CLuaMonsterPropertyFight.RefreshParampDef()

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampHit()

	CLuaMonsterPropertyFight.RefreshParammFatal()

	CLuaMonsterPropertyFight.RefreshParampFatal()

	CLuaMonsterPropertyFight.RefreshParammHit()

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParammSpeed()

	CLuaMonsterPropertyFight.RefreshParampAttMax()

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParampMiss()

	CLuaMonsterPropertyFight.RefreshParammDef()

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()

	CLuaMonsterPropertyFight.RefreshParamMpFull()

	CLuaMonsterPropertyFight.RefreshParammAttMax()

	CLuaMonsterPropertyFight.RefreshParampSpeed()

	CLuaMonsterPropertyFight.RefreshParamMpRecover()

	CLuaMonsterPropertyFight.RefreshParammMiss()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamCor = function(newv)
	SetTmpParam_At(3, newv)
end
CLuaMonsterPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)
end
CLuaMonsterPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)
end
CLuaMonsterPropertyFight.SetParamSta = function(newv)
	SetTmpParam_At(6, newv)
end
CLuaMonsterPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)
end
CLuaMonsterPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)
end
CLuaMonsterPropertyFight.SetParamStr = function(newv)
	SetTmpParam_At(9, newv)
end
CLuaMonsterPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)
end
CLuaMonsterPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)
end
CLuaMonsterPropertyFight.SetParamInt = function(newv)
	SetTmpParam_At(12, newv)
end
CLuaMonsterPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)
end
CLuaMonsterPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)
end
CLuaMonsterPropertyFight.SetParamAgi = function(newv)
	SetTmpParam_At(15, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)
end
CLuaMonsterPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)
end
CLuaMonsterPropertyFight.RefreshParamHpFull = function()
	local AdjHpFull = GetTmpParam_At(19)
	local MulHpFull = GetTmpParam_At(20)
	local HpFullInit = GetTmpParam_At(243)
	local HpFullPow1 = GetTmpParam_At(242)
	local HpFullPow2 = GetTmpParam_At(241)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( HpFullPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ HpFullPow1* max(Grade,MinGrade)+ HpFullInit +AdjHpFull )*(1+MulHpFull))
	SetTmpParam_At(18, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.RefreshParamHpRecover = function()
	local AdjHpRecover = GetTmpParam_At(22)
	local MulHpRecover = GetTmpParam_At(23)
	local HpRecoverPow1 = GetTmpParam_At(283)
	local HpRecoverInit = GetTmpParam_At(284)
	local HpFull = GetTmpParam_At(18)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( HpRecoverPow1* max(Grade,MinGrade)+ HpRecoverInit*HpFull + AdjHpRecover)*(1+ MulHpRecover)
	SetTmpParam_At(21, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.RefreshParamMpFull = function()
	local AdjMpFull = GetTmpParam_At(25)
	local MulMpFull = GetTmpParam_At(26)
	local MpFullInit = GetTmpParam_At(245)
	local MpFullPow1 = GetTmpParam_At(244)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( MpFullPow1* max(Grade,MinGrade)+ MpFullInit + AdjMpFull)*(1+MulMpFull))
	SetTmpParam_At(24, newv)
end
CLuaMonsterPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.RefreshParamMpRecover = function()
	local AdjMpRecover = GetTmpParam_At(28)
	local MulMpRecover = GetTmpParam_At(29)
	local MpRecoverInit = GetTmpParam_At(247)
	local MpRecoverPow1 = GetTmpParam_At(246)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MpRecoverPow1* max(Grade,MinGrade)+ MpRecoverInit + AdjMpRecover )*(1+ MulMpRecover)
	SetTmpParam_At(27, newv)
end
CLuaMonsterPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.RefreshParampAttMin = function()
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local AdjpAttMin = GetTmpParam_At(31)
	local MulpAttMin = GetTmpParam_At(32)
	local PAttMaxInit = GetTmpParam_At(253)
	local PAttMinInit = GetTmpParam_At(250)
	local PAttMaxPow1 = GetTmpParam_At(252)
	local PAttMinPow1 = GetTmpParam_At(249)
	local PAttMaxPow2 = GetTmpParam_At(251)
	local PAttMinPow2 = GetTmpParam_At(248)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,min(( PAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ PAttMaxPow1* max(Grade,MinGrade)+ PAttMaxInit + AdjpAttMax )*(1+ MulpAttMax),( PAttMinPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ PAttMinPow1* max(Grade,MinGrade)+ PAttMinInit + AdjpAttMin )*(1+ MulpAttMin)))
	SetTmpParam_At(30, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.RefreshParampAttMax = function()
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local PAttMaxInit = GetTmpParam_At(253)
	local PAttMaxPow1 = GetTmpParam_At(252)
	local PAttMaxPow2 = GetTmpParam_At(251)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( PAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ PAttMaxPow1* max(Grade,MinGrade)+ PAttMaxInit + AdjpAttMax )*(1+ MulpAttMax))
	SetTmpParam_At(33, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.RefreshParampHit = function()
	local AdjpHit = GetTmpParam_At(37)
	local MulpHit = GetTmpParam_At(38)
	local PHitInit = GetTmpParam_At(255)
	local PhitPow1 = GetTmpParam_At(254)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PhitPow1* max(Grade,MinGrade)+ PHitInit + AdjpHit )*(1+ MulpHit)
	SetTmpParam_At(36, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.RefreshParampMiss = function()
	local AdjpMiss = GetTmpParam_At(40)
	local MulpMiss = GetTmpParam_At(41)
	local PMissInit = GetTmpParam_At(257)
	local PMissPow1 = GetTmpParam_At(256)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PMissPow1* max(Grade,MinGrade)+ PMissInit + AdjpMiss )*(1+ MulpMiss)
	SetTmpParam_At(39, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.RefreshParampDef = function()
	local AdjpDef = GetTmpParam_At(43)
	local MulpDef = GetTmpParam_At(44)
	local PDefInit = GetTmpParam_At(261)
	local PDefPow1 = GetTmpParam_At(260)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( PDefPow1* max(Grade,MinGrade)+ PDefInit + AdjpDef )*(1+ MulpDef))
	SetTmpParam_At(42, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaMonsterPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaMonsterPropertyFight.RefreshParampSpeed = function()
	local AdjpSpeed = GetTmpParam_At(1001)
	local MulpSpeed = GetTmpParam_At(49)
	local OripSpeed = GetTmpParam_At(48)
	local PSpeedPow1 = GetTmpParam_At(258)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PSpeedPow1* max(Grade,MinGrade)+ OripSpeed+AdjpSpeed) * (1+ MulpSpeed)
	SetTmpParam_At(47, newv)
end
CLuaMonsterPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.RefreshParampFatal = function()
	local AdjpFatal = GetTmpParam_At(51)
	local PFatalInit = GetTmpParam_At(263)
	local PfatalPow1 = GetTmpParam_At(262)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( PfatalPow1* max(Grade,MinGrade)+ PFatalInit + AdjpFatal )
	SetTmpParam_At(50, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)

	CLuaMonsterPropertyFight.RefreshParampFatal()
end
CLuaMonsterPropertyFight.RefreshParamAntipFatal = function()
	local AdjAntipFatal = GetTmpParam_At(53)

	local newv = AdjAntipFatal
	SetTmpParam_At(52, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)

	CLuaMonsterPropertyFight.RefreshParamAntipFatal()
end
CLuaMonsterPropertyFight.RefreshParampFatalDamage = function()
	local AdjpFatalDamage = GetTmpParam_At(55)

	local newv = (AdjpFatalDamage + 0.5)
	SetTmpParam_At(54, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)

	CLuaMonsterPropertyFight.RefreshParampFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamAntipFatalDamage = function()
	local AdjAntipFatalDamage = GetTmpParam_At(57)

	local newv = AdjAntipFatalDamage
	SetTmpParam_At(56, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)

	CLuaMonsterPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamBlock = function()
	local AdjBlock = GetTmpParam_At(59)
	local MulBlock = GetTmpParam_At(60)

	local newv = AdjBlock * (1+MulBlock)
	SetTmpParam_At(58, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)

	CLuaMonsterPropertyFight.RefreshParamBlock()
end
CLuaMonsterPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)

	CLuaMonsterPropertyFight.RefreshParamBlock()
end
CLuaMonsterPropertyFight.RefreshParamBlockDamage = function()
	local AdjBlockDamage = GetTmpParam_At(62)
	local BlockDamageInit = GetTmpParam_At(281)
	local BlockDamagePow1 = GetTmpParam_At(280)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(0,( BlockDamagePow1* max(Grade,MinGrade)+ BlockDamageInit + AdjBlockDamage))
	SetTmpParam_At(61, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()
end
CLuaMonsterPropertyFight.RefreshParamAntiBlock = function()
	local AdjAntiBlock = GetTmpParam_At(64)

	local newv = AdjAntiBlock
	SetTmpParam_At(63, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlock()
end
CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage = function()
	local AntiBlockDamageInit = GetTmpParam_At(286)
	local AdjAntiBlockDamage = GetTmpParam_At(66)
	local AntiBlockDamagePow1 = GetTmpParam_At(285)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( AdjAntiBlockDamage + AntiBlockDamagePow1*max(Grade,MinGrade) + AntiBlockDamageInit )
	SetTmpParam_At(65, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaMonsterPropertyFight.RefreshParammAttMin = function()
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local AdjmAttMin = GetTmpParam_At(68)
	local MulmAttMin = GetTmpParam_At(69)
	local MAttMaxInit = GetTmpParam_At(269)
	local MAttMinInit = GetTmpParam_At(266)
	local MAttMaxPow1 = GetTmpParam_At(268)
	local MAttMinPow1 = GetTmpParam_At(265)
	local MAttMaxPow2 = GetTmpParam_At(267)
	local MAttMinPow2 = GetTmpParam_At(264)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,min(( MAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ MAttMaxPow1* max(Grade,MinGrade)+ MAttMaxInit + AdjmAttMax)*(1+ MulmAttMax),( MAttMinPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ MAttMinPow1* max(Grade,MinGrade)+ MAttMinInit + AdjmAttMin)*(1+ MulmAttMin)))
	SetTmpParam_At(67, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.RefreshParammAttMax = function()
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local MAttMaxInit = GetTmpParam_At(269)
	local MAttMaxPow1 = GetTmpParam_At(268)
	local MAttMaxPow2 = GetTmpParam_At(267)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( MAttMaxPow2* max(Grade,MinGrade)* max(Grade,MinGrade)+ MAttMaxPow1* max(Grade,MinGrade)+ MAttMaxInit + AdjmAttMax)*(1+ MulmAttMax))
	SetTmpParam_At(70, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.RefreshParammHit = function()
	local AdjmHit = GetTmpParam_At(74)
	local MulmHit = GetTmpParam_At(75)
	local MHitInit = GetTmpParam_At(275)
	local MHitPow1 = GetTmpParam_At(274)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MHitPow1* max(Grade,MinGrade)+ MHitInit + AdjmHit)*(1+ MulmHit)
	SetTmpParam_At(73, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.RefreshParammMiss = function()
	local AdjmMiss = GetTmpParam_At(77)
	local MulmMiss = GetTmpParam_At(78)
	local MMissInit = GetTmpParam_At(277)
	local MMissPow1 = GetTmpParam_At(276)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MMissPow1* max(Grade,MinGrade)+ MMissInit + AdjmMiss)*(1+ MulmMiss)
	SetTmpParam_At(76, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.RefreshParammDef = function()
	local AdjmDef = GetTmpParam_At(80)
	local MulmDef = GetTmpParam_At(81)
	local MDefInit = GetTmpParam_At(273)
	local MDefPow1 = GetTmpParam_At(272)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = max(1,( MDefPow1* max(Grade,MinGrade)+ MDefInit + AdjmDef )*(1+ MulmDef))
	SetTmpParam_At(79, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaMonsterPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaMonsterPropertyFight.RefreshParammSpeed = function()
	local AdjmSpeed = GetTmpParam_At(1002)
	local MulpSpeed = GetTmpParam_At(49)
	local OrimSpeed = GetTmpParam_At(85)
	local MSpeedPow1 = GetTmpParam_At(270)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MSpeedPow1* max(Grade,MinGrade)+ OrimSpeed+AdjmSpeed)*(1+ MulpSpeed)
	SetTmpParam_At(84, newv)
end
CLuaMonsterPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()
end
CLuaMonsterPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)
end
CLuaMonsterPropertyFight.RefreshParammFatal = function()
	local AdjmFatal = GetTmpParam_At(88)
	local MFatalInit = GetTmpParam_At(279)
	local MFatalPow1 = GetTmpParam_At(278)
	local Grade = GetTmpParam_At(2)
	local MinGrade = GetTmpParam_At(362)

	local newv = ( MFatalPow1* max(Grade,MinGrade)+ MFatalInit + AdjmFatal )
	SetTmpParam_At(87, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)

	CLuaMonsterPropertyFight.RefreshParammFatal()
end
CLuaMonsterPropertyFight.RefreshParamAntimFatal = function()
	local AdjAntimFatal = GetTmpParam_At(90)

	local newv = AdjAntimFatal
	SetTmpParam_At(89, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)

	CLuaMonsterPropertyFight.RefreshParamAntimFatal()
end
CLuaMonsterPropertyFight.RefreshParammFatalDamage = function()
	local AdjmFatalDamage = GetTmpParam_At(92)

	local newv = (AdjmFatalDamage+0.5)
	SetTmpParam_At(91, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)

	CLuaMonsterPropertyFight.RefreshParammFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamAntimFatalDamage = function()
	local AdjAntimFatalDamage = GetTmpParam_At(94)

	local newv = AdjAntimFatalDamage
	SetTmpParam_At(93, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)

	CLuaMonsterPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceFire = function()
	local AdjEnhanceFire = GetTmpParam_At(96)

	local newv = AdjEnhanceFire
	SetTmpParam_At(95, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceFire()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceThunder = function()
	local AdjEnhanceThunder = GetTmpParam_At(99)

	local newv = AdjEnhanceThunder
	SetTmpParam_At(98, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceThunder()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceIce = function()
	local AdjEnhanceIce = GetTmpParam_At(102)

	local newv = AdjEnhanceIce
	SetTmpParam_At(101, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceIce()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhancePoison = function()
	local AdjEnhancePoison = GetTmpParam_At(105)

	local newv = AdjEnhancePoison
	SetTmpParam_At(104, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhancePoison()
end
CLuaMonsterPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceWind = function()
	local AdjEnhanceWind = GetTmpParam_At(108)

	local newv = AdjEnhanceWind
	SetTmpParam_At(107, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceWind()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceLight = function()
	local AdjEnhanceLight = GetTmpParam_At(111)

	local newv = AdjEnhanceLight
	SetTmpParam_At(110, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceLight()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceIllusion = function()
	local AdjEnhanceIllusion = GetTmpParam_At(114)

	local newv = AdjEnhanceIllusion
	SetTmpParam_At(113, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiFire = function()
	local AdjAntiFire = GetTmpParam_At(117)
	local MulAntiFire = GetTmpParam_At(118)

	local newv = AdjAntiFire*(1+MulAntiFire)
	SetTmpParam_At(116, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFire()
end
CLuaMonsterPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFire()
end
CLuaMonsterPropertyFight.RefreshParamAntiThunder = function()
	local AdjAntiThunder = GetTmpParam_At(120)
	local MulAntiThunder = GetTmpParam_At(121)

	local newv = AdjAntiThunder*(1+MulAntiThunder)
	SetTmpParam_At(119, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiThunder()
end
CLuaMonsterPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiThunder()
end
CLuaMonsterPropertyFight.RefreshParamAntiIce = function()
	local AdjAntiIce = GetTmpParam_At(123)
	local MulAntiIce = GetTmpParam_At(124)

	local newv = AdjAntiIce*(1+MulAntiIce)
	SetTmpParam_At(122, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIce()
end
CLuaMonsterPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIce()
end
CLuaMonsterPropertyFight.RefreshParamAntiPoison = function()
	local AdjAntiPoison = GetTmpParam_At(126)
	local MulAntiPoison = GetTmpParam_At(127)

	local newv = AdjAntiPoison*(1+MulAntiPoison)
	SetTmpParam_At(125, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPoison()
end
CLuaMonsterPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPoison()
end
CLuaMonsterPropertyFight.RefreshParamAntiWind = function()
	local AdjAntiWind = GetTmpParam_At(129)
	local MulAntiWind = GetTmpParam_At(130)

	local newv = AdjAntiWind*(1+MulAntiWind)
	SetTmpParam_At(128, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWind()
end
CLuaMonsterPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWind()
end
CLuaMonsterPropertyFight.RefreshParamAntiLight = function()
	local AdjAntiLight = GetTmpParam_At(132)
	local MulAntiLight = GetTmpParam_At(133)

	local newv = AdjAntiLight*(1+MulAntiLight)
	SetTmpParam_At(131, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiLight()
end
CLuaMonsterPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiLight()
end
CLuaMonsterPropertyFight.RefreshParamAntiIllusion = function()
	local AdjAntiIllusion = GetTmpParam_At(135)
	local MulAntiIllusion = GetTmpParam_At(136)

	local newv = AdjAntiIllusion*(1+MulAntiIllusion)
	SetTmpParam_At(134, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIllusion()
end
CLuaMonsterPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiIllusion()
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiFire = function()
	local AdjIgnoreAntiFire = GetTmpParam_At(417)

	local newv = AdjIgnoreAntiFire
	SetTmpParam_At(137, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiThunder = function()
	local AdjIgnoreAntiThunder = GetTmpParam_At(418)

	local newv = AdjIgnoreAntiThunder
	SetTmpParam_At(138, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIce = function()
	local AdjIgnoreAntiIce = GetTmpParam_At(419)

	local newv = AdjIgnoreAntiIce
	SetTmpParam_At(139, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiPoison = function()
	local AdjIgnoreAntiPoison = GetTmpParam_At(420)

	local newv = AdjIgnoreAntiPoison
	SetTmpParam_At(140, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWind = function()
	local AdjIgnoreAntiWind = GetTmpParam_At(421)

	local newv = AdjIgnoreAntiWind
	SetTmpParam_At(141, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiLight = function()
	local AdjIgnoreAntiLight = GetTmpParam_At(422)

	local newv = AdjIgnoreAntiLight
	SetTmpParam_At(142, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIllusion = function()
	local AdjIgnoreAntiIllusion = GetTmpParam_At(423)

	local newv = AdjIgnoreAntiIllusion
	SetTmpParam_At(143, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceDizzy = function()
	local AdjEnhanceDizzy = GetTmpParam_At(145)
	local MulEnhanceDizzy = GetTmpParam_At(146)

	local newv = AdjEnhanceDizzy*(1+MulEnhanceDizzy)
	SetTmpParam_At(144, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceSleep = function()
	local AdjEnhanceSleep = GetTmpParam_At(148)
	local MulEnhanceSleep = GetTmpParam_At(149)

	local newv = AdjEnhanceSleep*(1+MulEnhanceSleep)
	SetTmpParam_At(147, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSleep()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSleep()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceChaos = function()
	local AdjEnhanceChaos = GetTmpParam_At(151)
	local MulEnhanceChaos = GetTmpParam_At(152)

	local newv = AdjEnhanceChaos*(1+MulEnhanceChaos)
	SetTmpParam_At(150, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceChaos()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceChaos()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceBind = function()
	local AdjEnhanceBind = GetTmpParam_At(154)
	local MulEnhanceBind = GetTmpParam_At(155)

	local newv = AdjEnhanceBind*(1+MulEnhanceBind)
	SetTmpParam_At(153, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBind()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBind()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceSilence = function()
	local AdjEnhanceSilence = GetTmpParam_At(157)
	local MulEnhanceSilence = GetTmpParam_At(158)

	local newv = AdjEnhanceSilence*(1+MulEnhanceSilence)
	SetTmpParam_At(156, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSilence()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceSilence()
end
CLuaMonsterPropertyFight.RefreshParamAntiDizzy = function()
	local AdjAntiDizzy = GetTmpParam_At(160)
	local MulAntiDizzy = GetTmpParam_At(161)

	local newv = AdjAntiDizzy*(1+MulAntiDizzy)
	SetTmpParam_At(159, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDizzy()
end
CLuaMonsterPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDizzy()
end
CLuaMonsterPropertyFight.RefreshParamAntiSleep = function()
	local AdjAntiSleep = GetTmpParam_At(163)
	local MulAntiSleep = GetTmpParam_At(164)

	local newv = AdjAntiSleep*(1+MulAntiSleep)
	SetTmpParam_At(162, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSleep()
end
CLuaMonsterPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSleep()
end
CLuaMonsterPropertyFight.RefreshParamAntiChaos = function()
	local AdjAntiChaos = GetTmpParam_At(166)
	local MulAntiChaos = GetTmpParam_At(167)

	local newv = AdjAntiChaos*(1+MulAntiChaos)
	SetTmpParam_At(165, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiChaos()
end
CLuaMonsterPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiChaos()
end
CLuaMonsterPropertyFight.RefreshParamAntiBind = function()
	local AdjAntiBind = GetTmpParam_At(169)
	local MulAntiBind = GetTmpParam_At(170)

	local newv = AdjAntiBind*(1+MulAntiBind)
	SetTmpParam_At(168, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBind()
end
CLuaMonsterPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBind()
end
CLuaMonsterPropertyFight.RefreshParamAntiSilence = function()
	local AdjAntiSilence = GetTmpParam_At(172)
	local MulAntiSilence = GetTmpParam_At(173)

	local newv = AdjAntiSilence*(1+MulAntiSilence)
	SetTmpParam_At(171, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSilence()
end
CLuaMonsterPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiSilence()
end
CLuaMonsterPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaMonsterPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaMonsterPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaMonsterPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaMonsterPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaMonsterPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaMonsterPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaMonsterPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceHeal = function()
	local AdjEnhanceHeal = GetTmpParam_At(183)

	local newv = AdjEnhanceHeal
	SetTmpParam_At(182, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceHeal()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaMonsterPropertyFight.RefreshParamSpeed = function()
	local OriSpeed = GetTmpParam_At(186)
	local AdjSpeed = GetTmpParam_At(187)
	local MulSpeed2 = GetTmpParam_At(433)
	local MulSpeed = GetTmpParam_At(188)

	local newv = (OriSpeed + AdjSpeed)*max(0.3,1+MulSpeed+MulSpeed2)
	SetTmpParam_At(185, newv)
end
CLuaMonsterPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamMF = function(newv)
	SetTmpParam_At(189, newv)
end
CLuaMonsterPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)
end
CLuaMonsterPropertyFight.SetParamRange = function(newv)
	SetTmpParam_At(191, newv)
end
CLuaMonsterPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)
end
CLuaMonsterPropertyFight.SetParamHatePlus = function(newv)
	SetTmpParam_At(193, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)
end
CLuaMonsterPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaMonsterPropertyFight.RefreshParamInvisible = function()
	local AdjInvisible = GetTmpParam_At(197)

	local newv = AdjInvisible
	SetTmpParam_At(196, newv)
end
CLuaMonsterPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)

	CLuaMonsterPropertyFight.RefreshParamInvisible()
end
CLuaMonsterPropertyFight.RefreshParamTrueSight = function()
	local OriTrueSight = GetTmpParam_At(199)
	local AdjTrueSight = GetTmpParam_At(200)

	local newv = OriTrueSight + AdjTrueSight
	SetTmpParam_At(198, newv)
end
CLuaMonsterPropertyFight.SetParamOriTrueSight = function(newv)
	SetTmpParam_At(199, newv)

	CLuaMonsterPropertyFight.RefreshParamTrueSight()
end
CLuaMonsterPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)

	CLuaMonsterPropertyFight.RefreshParamTrueSight()
end
CLuaMonsterPropertyFight.RefreshParamEyeSight = function()
	local OriEyeSight = GetTmpParam_At(202)
	local AdjEyeSight = GetTmpParam_At(203)

	local newv = OriEyeSight + AdjEyeSight
	SetTmpParam_At(201, newv)
end
CLuaMonsterPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)

	CLuaMonsterPropertyFight.RefreshParamEyeSight()
end
CLuaMonsterPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)

	CLuaMonsterPropertyFight.RefreshParamEyeSight()
end
CLuaMonsterPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaMonsterPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaMonsterPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaMonsterPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaMonsterPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaMonsterPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaMonsterPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaMonsterPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)

	CLuaMonsterPropertyFight.RefreshParamMpFull()
end
CLuaMonsterPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)

	CLuaMonsterPropertyFight.RefreshParamMpRecover()
end
CLuaMonsterPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()
end
CLuaMonsterPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampAttMax()
end
CLuaMonsterPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)

	CLuaMonsterPropertyFight.RefreshParampHit()
end
CLuaMonsterPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)

	CLuaMonsterPropertyFight.RefreshParampMiss()
end
CLuaMonsterPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaMonsterPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)

	CLuaMonsterPropertyFight.RefreshParampDef()
end
CLuaMonsterPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)

	CLuaMonsterPropertyFight.RefreshParampFatal()
end
CLuaMonsterPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)

	CLuaMonsterPropertyFight.RefreshParampFatal()
end
CLuaMonsterPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()
end
CLuaMonsterPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParammAttMax()
end
CLuaMonsterPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()
end
CLuaMonsterPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaMonsterPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)

	CLuaMonsterPropertyFight.RefreshParammDef()
end
CLuaMonsterPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)

	CLuaMonsterPropertyFight.RefreshParammHit()
end
CLuaMonsterPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)

	CLuaMonsterPropertyFight.RefreshParammMiss()
end
CLuaMonsterPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)

	CLuaMonsterPropertyFight.RefreshParammFatal()
end
CLuaMonsterPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)

	CLuaMonsterPropertyFight.RefreshParammFatal()
end
CLuaMonsterPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()
end
CLuaMonsterPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()
end
CLuaMonsterPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaMonsterPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaMonsterPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaMonsterPropertyFight.SetParamBBMax = function(newv)
	SetTmpParam_At(287, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiBreak = function()
	local AdjAntiBreak = GetTmpParam_At(290)

	local newv = AdjAntiBreak
	SetTmpParam_At(289, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBreak()
end
CLuaMonsterPropertyFight.RefreshParamAntiPushPull = function()
	local AdjAntiPushPull = GetTmpParam_At(292)

	local newv = AdjAntiPushPull
	SetTmpParam_At(291, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPushPull()
end
CLuaMonsterPropertyFight.RefreshParamAntiPetrify = function()
	local AdjAntiPetrify = GetTmpParam_At(294)
	local MulAntiPetrify = GetTmpParam_At(295)

	local newv = AdjAntiPetrify*(1+MulAntiPetrify)
	SetTmpParam_At(293, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPetrify()
end
CLuaMonsterPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiPetrify()
end
CLuaMonsterPropertyFight.RefreshParamAntiTie = function()
	local AdjAntiTie = GetTmpParam_At(297)
	local MulAntiTie = GetTmpParam_At(298)

	local newv = AdjAntiTie*(1+MulAntiTie)
	SetTmpParam_At(296, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiTie()
end
CLuaMonsterPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiTie()
end
CLuaMonsterPropertyFight.RefreshParamEnhancePetrify = function()
	local AdjEnhancePetrify = GetTmpParam_At(300)
	local MulEnhancePetrify = GetTmpParam_At(301)

	local newv = AdjEnhancePetrify*(1+MulEnhancePetrify)
	SetTmpParam_At(299, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhancePetrify()
end
CLuaMonsterPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhancePetrify()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceTie = function()
	local AdjEnhanceTie = GetTmpParam_At(303)
	local MulEnhanceTie = GetTmpParam_At(304)

	local newv = AdjEnhanceTie*(1+MulEnhanceTie)
	SetTmpParam_At(302, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceTie()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceTie()
end
CLuaMonsterPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaMonsterPropertyFight.SetParamStrZizhi = function(newv)
	SetTmpParam_At(306, newv)
end
CLuaMonsterPropertyFight.SetParamCorZizhi = function(newv)
	SetTmpParam_At(307, newv)
end
CLuaMonsterPropertyFight.SetParamStaZizhi = function(newv)
	SetTmpParam_At(308, newv)
end
CLuaMonsterPropertyFight.SetParamAgiZizhi = function(newv)
	SetTmpParam_At(309, newv)
end
CLuaMonsterPropertyFight.SetParamIntZizhi = function(newv)
	SetTmpParam_At(310, newv)
end
CLuaMonsterPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)
end
CLuaMonsterPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)
end
CLuaMonsterPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)
end
CLuaMonsterPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)
end
CLuaMonsterPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)
end
CLuaMonsterPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)
end
CLuaMonsterPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)
end
CLuaMonsterPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)
end
CLuaMonsterPropertyFight.SetParamPType = function(newv)
	SetTmpParam_At(319, newv)
end
CLuaMonsterPropertyFight.SetParamMType = function(newv)
	SetTmpParam_At(320, newv)
end
CLuaMonsterPropertyFight.SetParamPHType = function(newv)
	SetTmpParam_At(321, newv)
end
CLuaMonsterPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)
end
CLuaMonsterPropertyFight.SetParamWuxingImprove = function(newv)
	SetTmpParam_At(323, newv)
end
CLuaMonsterPropertyFight.SetParamXiuweiImprove = function(newv)
	SetTmpParam_At(324, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceBeHeal = function()
	local AdjEnhanceBeHeal = GetTmpParam_At(326)

	local newv = AdjEnhanceBeHeal
	SetTmpParam_At(325, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBeHeal()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpCor = function(newv)
	SetTmpParam_At(328, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpSta = function(newv)
	SetTmpParam_At(329, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpStr = function(newv)
	SetTmpParam_At(330, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpInt = function(newv)
	SetTmpParam_At(331, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpAgi = function(newv)
	SetTmpParam_At(332, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpHpFull = function(newv)
	SetTmpParam_At(333, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpMpFull = function(newv)
	SetTmpParam_At(334, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppAttMin = function(newv)
	SetTmpParam_At(335, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppAttMax = function(newv)
	SetTmpParam_At(336, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppHit = function(newv)
	SetTmpParam_At(337, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppMiss = function(newv)
	SetTmpParam_At(338, newv)
end
CLuaMonsterPropertyFight.SetParamLookUppDef = function(newv)
	SetTmpParam_At(339, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmAttMin = function(newv)
	SetTmpParam_At(340, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmAttMax = function(newv)
	SetTmpParam_At(341, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmHit = function(newv)
	SetTmpParam_At(342, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmMiss = function(newv)
	SetTmpParam_At(343, newv)
end
CLuaMonsterPropertyFight.SetParamLookUpmDef = function(newv)
	SetTmpParam_At(344, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)
end
CLuaMonsterPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)
end
CLuaMonsterPropertyFight.SetParamMHType = function(newv)
	SetTmpParam_At(351, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiAoe()
end
CLuaMonsterPropertyFight.RefreshParamAntiBlind = function()
	local AdjAntiBlind = GetTmpParam_At(357)
	local MulAntiBlind = GetTmpParam_At(358)

	local newv = AdjAntiBlind*(1+MulAntiBlind)
	SetTmpParam_At(356, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlind()
end
CLuaMonsterPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBlind()
end
CLuaMonsterPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)
	SetTmpParam_At(359, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDecelerate()
end
CLuaMonsterPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiDecelerate()
end
CLuaMonsterPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)

	CLuaMonsterPropertyFight.RefreshParamBlockDamage()

	CLuaMonsterPropertyFight.RefreshParampDef()

	CLuaMonsterPropertyFight.RefreshParampAttMin()

	CLuaMonsterPropertyFight.RefreshParampHit()

	CLuaMonsterPropertyFight.RefreshParammFatal()

	CLuaMonsterPropertyFight.RefreshParampFatal()

	CLuaMonsterPropertyFight.RefreshParammHit()

	CLuaMonsterPropertyFight.RefreshParamHpFull()

	CLuaMonsterPropertyFight.RefreshParammSpeed()

	CLuaMonsterPropertyFight.RefreshParampAttMax()

	CLuaMonsterPropertyFight.RefreshParammAttMin()

	CLuaMonsterPropertyFight.RefreshParampMiss()

	CLuaMonsterPropertyFight.RefreshParammDef()

	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()

	CLuaMonsterPropertyFight.RefreshParamMpFull()

	CLuaMonsterPropertyFight.RefreshParammAttMax()

	CLuaMonsterPropertyFight.RefreshParampSpeed()

	CLuaMonsterPropertyFight.RefreshParamMpRecover()

	CLuaMonsterPropertyFight.RefreshParammMiss()

	CLuaMonsterPropertyFight.RefreshParamHpRecover()
end
CLuaMonsterPropertyFight.SetParamCharacterCor = function(newv)
	SetTmpParam_At(363, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterSta = function(newv)
	SetTmpParam_At(364, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterStr = function(newv)
	SetTmpParam_At(365, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterInt = function(newv)
	SetTmpParam_At(366, newv)
end
CLuaMonsterPropertyFight.SetParamCharacterAgi = function(newv)
	SetTmpParam_At(367, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaMonsterPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceBianHu = function()
	local AdjEnhanceBianHu = GetTmpParam_At(372)
	local MulEnhanceBianHu = GetTmpParam_At(373)

	local newv = AdjEnhanceBianHu*(1+MulEnhanceBianHu)
	SetTmpParam_At(371, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaMonsterPropertyFight.RefreshParamAntiBianHu = function()
	local AdjAntiBianHu = GetTmpParam_At(375)
	local MulAntiBianHu = GetTmpParam_At(376)

	local newv = AdjAntiBianHu*(1+MulAntiBianHu)
	SetTmpParam_At(374, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBianHu()
end
CLuaMonsterPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiBianHu()
end
CLuaMonsterPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaMonsterPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaMonsterPropertyFight.SetParamPAType = function(newv)
	SetTmpParam_At(382, newv)
end
CLuaMonsterPropertyFight.SetParamMAType = function(newv)
	SetTmpParam_At(383, newv)
end
CLuaMonsterPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaMonsterPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaMonsterPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaMonsterPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaMonsterPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaMonsterPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaMonsterPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaMonsterPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaMonsterPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaMonsterPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaMonsterPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaMonsterPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaMonsterPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaMonsterPropertyFight.RefreshParamEnhanceWater = function()
	local AdjEnhanceWater = GetTmpParam_At(398)

	local newv = AdjEnhanceWater
	SetTmpParam_At(397, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceWater()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiWater = function()
	local AdjAntiWater = GetTmpParam_At(401)
	local MulAntiWater = GetTmpParam_At(402)

	local newv = AdjAntiWater*(1+MulAntiWater)
	SetTmpParam_At(400, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWater()
end
CLuaMonsterPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiWater()
end
CLuaMonsterPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWater = function()
	local AdjIgnoreAntiWater = GetTmpParam_At(431)

	local newv = AdjIgnoreAntiWater
	SetTmpParam_At(404, newv)
end
CLuaMonsterPropertyFight.RefreshParamAntiFreeze = function()
	local AdjAntiFreeze = GetTmpParam_At(406)
	local MulAntiFreeze = GetTmpParam_At(407)

	local newv = AdjAntiFreeze*(1+MulAntiFreeze)
	SetTmpParam_At(405, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFreeze()
end
CLuaMonsterPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)

	CLuaMonsterPropertyFight.RefreshParamAntiFreeze()
end
CLuaMonsterPropertyFight.RefreshParamEnhanceFreeze = function()
	local AdjEnhanceFreeze = GetTmpParam_At(409)
	local MulEnhanceFreeze = GetTmpParam_At(410)

	local newv = AdjEnhanceFreeze*(1+MulEnhanceFreeze)
	SetTmpParam_At(408, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaMonsterPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)

	CLuaMonsterPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaMonsterPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaMonsterPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaMonsterPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)
end
CLuaMonsterPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)
end
CLuaMonsterPropertyFight.SetParamLSBBAdjHp = function(newv)
	SetTmpParam_At(416, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)

	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaMonsterPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)
end
CLuaMonsterPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)

	CLuaMonsterPropertyFight.RefreshParamSpeed()
end
CLuaMonsterPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)
end
CLuaMonsterPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)
end
CLuaMonsterPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)
end
CLuaMonsterPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)
end
CLuaMonsterPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)
end
CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)
end
CLuaMonsterPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)
end
CLuaMonsterPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaMonsterPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMin = function(newv)
	SetTmpParam_At(505, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMax = function(newv)
	SetTmpParam_At(506, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMin = function(newv)
	SetTmpParam_At(507, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMax = function(newv)
	SetTmpParam_At(508, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatal = function(newv)
	SetTmpParam_At(509, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatalDamage = function(newv)
	SetTmpParam_At(510, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatal = function(newv)
	SetTmpParam_At(511, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatalDamage = function(newv)
	SetTmpParam_At(512, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlock = function(newv)
	SetTmpParam_At(513, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage = function(newv)
	SetTmpParam_At(514, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire = function(newv)
	SetTmpParam_At(515, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder = function(newv)
	SetTmpParam_At(516, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce = function(newv)
	SetTmpParam_At(517, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison = function(newv)
	SetTmpParam_At(518, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind = function(newv)
	SetTmpParam_At(519, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight = function(newv)
	SetTmpParam_At(520, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(521, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater = function(newv)
	SetTmpParam_At(522, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)
end
CLuaMonsterPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)
end
CLuaMonsterPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)
end
CLuaMonsterPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)
end
CLuaMonsterPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)
end
CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)
end
CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaMonsterPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaMonsterPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaMonsterPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaMonsterPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaMonsterPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaMonsterPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaMonsterPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaMonsterPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaMonsterPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaMonsterPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaMonsterPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaMonsterPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaMonsterPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaMonsterPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaMonsterPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentCor = function(newv)
	SetTmpParam_At(611, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentSta = function(newv)
	SetTmpParam_At(612, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentStr = function(newv)
	SetTmpParam_At(613, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentInt = function(newv)
	SetTmpParam_At(614, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentAgi = function(newv)
	SetTmpParam_At(615, newv)
end
CLuaMonsterPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidCor = function(newv)
	SetTmpParam_At(617, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidSta = function(newv)
	SetTmpParam_At(618, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidStr = function(newv)
	SetTmpParam_At(619, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidInt = function(newv)
	SetTmpParam_At(620, newv)
end
CLuaMonsterPropertyFight.SetParamAdjustPermanentMidAgi = function(newv)
	SetTmpParam_At(621, newv)
end
CLuaMonsterPropertyFight.SetParamSumPermanent = function(newv)
	SetTmpParam_At(622, newv)
end
CLuaMonsterPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaMonsterPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaMonsterPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaMonsterPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaMonsterPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaMonsterPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaMonsterPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaMonsterPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaMonsterPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaMonsterPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaMonsterPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaMonsterPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaMonsterPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaMonsterPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaMonsterPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaMonsterPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaMonsterPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaMonsterPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaMonsterPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaMonsterPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaMonsterPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaMonsterPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaMonsterPropertyFight.SetParamJumpSpeed = function(newv)
	SetTmpParam_At(650, newv)
end
CLuaMonsterPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)
end
CLuaMonsterPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)
end
CLuaMonsterPropertyFight.SetParamGravityAcceleration = function(newv)
	SetTmpParam_At(653, newv)
end
CLuaMonsterPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)
end
CLuaMonsterPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)
end
CLuaMonsterPropertyFight.SetParamHorizontalAcceleration = function(newv)
	SetTmpParam_At(656, newv)
end
CLuaMonsterPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)
end
CLuaMonsterPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)
end
CLuaMonsterPropertyFight.SetParamSwimSpeed = function(newv)
	SetTmpParam_At(659, newv)
end
CLuaMonsterPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)
end
CLuaMonsterPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)
end
CLuaMonsterPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaMonsterPropertyFight.SetParamStrengthPointMax = function(newv)
	SetTmpParam_At(663, newv)
end
CLuaMonsterPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)
end
CLuaMonsterPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)
end
CLuaMonsterPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaMonsterPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaMonsterPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaMonsterPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)

	CLuaMonsterPropertyFight.RefreshParampSpeed()
end
CLuaMonsterPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)

	CLuaMonsterPropertyFight.RefreshParammSpeed()
end
CMonsterPropertyFight.m_hookRefreshAll = function(this)
	CLuaMonsterPropertyFight.Param = this.Param
	CLuaMonsterPropertyFight.TmpParam = this.TmpParam

	CLuaMonsterPropertyFight.RefreshParamAntiPushPull()
	CLuaMonsterPropertyFight.RefreshParamBlockDamage()
	CLuaMonsterPropertyFight.RefreshParamAntipFatalDamage()
	CLuaMonsterPropertyFight.RefreshParampDef()
	CLuaMonsterPropertyFight.RefreshParamInvisible()
	CLuaMonsterPropertyFight.RefreshParamEnhancePoison()
	CLuaMonsterPropertyFight.RefreshParampAttMin()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiLight()
	CLuaMonsterPropertyFight.RefreshParamAntiBreak()
	CLuaMonsterPropertyFight.RefreshParamAntiSilence()
	CLuaMonsterPropertyFight.RefreshParamEnhanceChaos()
	CLuaMonsterPropertyFight.RefreshParamAntiBlock()
	CLuaMonsterPropertyFight.RefreshParamAntiWater()
	CLuaMonsterPropertyFight.RefreshParampHit()
	CLuaMonsterPropertyFight.RefreshParamEnhanceFire()
	CLuaMonsterPropertyFight.RefreshParamAntimFatal()
	CLuaMonsterPropertyFight.RefreshParammFatalDamage()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiThunder()
	CLuaMonsterPropertyFight.RefreshParamAntipFatal()
	CLuaMonsterPropertyFight.RefreshParammFatal()
	CLuaMonsterPropertyFight.RefreshParamAntiWind()
	CLuaMonsterPropertyFight.RefreshParamEnhancePetrify()
	CLuaMonsterPropertyFight.RefreshParamAntiChaos()
	CLuaMonsterPropertyFight.RefreshParamEnhanceIce()
	CLuaMonsterPropertyFight.RefreshParampFatal()
	CLuaMonsterPropertyFight.RefreshParammHit()
	CLuaMonsterPropertyFight.RefreshParamEnhanceWind()
	CLuaMonsterPropertyFight.RefreshParamEyeSight()
	CLuaMonsterPropertyFight.RefreshParamAntiThunder()
	CLuaMonsterPropertyFight.RefreshParamBlock()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiFire()
	CLuaMonsterPropertyFight.RefreshParamSpeed()
	CLuaMonsterPropertyFight.RefreshParamEnhanceBind()
	CLuaMonsterPropertyFight.RefreshParamAntiFreeze()
	CLuaMonsterPropertyFight.RefreshParamAntiSleep()
	CLuaMonsterPropertyFight.RefreshParamEnhanceHeal()
	CLuaMonsterPropertyFight.RefreshParamHpFull()
	CLuaMonsterPropertyFight.RefreshParamAntiDizzy()
	CLuaMonsterPropertyFight.RefreshParamAntiBind()
	CLuaMonsterPropertyFight.RefreshParamAntiPoison()
	CLuaMonsterPropertyFight.RefreshParamEnhanceSleep()
	CLuaMonsterPropertyFight.RefreshParamEnhanceFreeze()
	CLuaMonsterPropertyFight.RefreshParamEnhanceThunder()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWater()
	CLuaMonsterPropertyFight.RefreshParamTrueSight()
	CLuaMonsterPropertyFight.RefreshParamEnhanceWater()
	CLuaMonsterPropertyFight.RefreshParamEnhanceSilence()
	CLuaMonsterPropertyFight.RefreshParammSpeed()
	CLuaMonsterPropertyFight.RefreshParamAntiDecelerate()
	CLuaMonsterPropertyFight.RefreshParamAntiAoe()
	CLuaMonsterPropertyFight.RefreshParamAntiPetrify()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiWind()
	CLuaMonsterPropertyFight.RefreshParamAntiBianHu()
	CLuaMonsterPropertyFight.RefreshParamAntiBlind()
	CLuaMonsterPropertyFight.RefreshParamAntiLight()
	CLuaMonsterPropertyFight.RefreshParamEnhanceBeHeal()
	CLuaMonsterPropertyFight.RefreshParamEnhanceTie()
	CLuaMonsterPropertyFight.RefreshParamAntiTie()
	CLuaMonsterPropertyFight.RefreshParampAttMax()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiPoison()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIce()
	CLuaMonsterPropertyFight.RefreshParampFatalDamage()
	CLuaMonsterPropertyFight.RefreshParammAttMin()
	CLuaMonsterPropertyFight.RefreshParampMiss()
	CLuaMonsterPropertyFight.RefreshParamEnhanceBianHu()
	CLuaMonsterPropertyFight.RefreshParamIgnoreAntiIllusion()
	CLuaMonsterPropertyFight.RefreshParammDef()
	CLuaMonsterPropertyFight.RefreshParamAntiIllusion()
	CLuaMonsterPropertyFight.RefreshParamAntiIce()
	CLuaMonsterPropertyFight.RefreshParamAntiBlockDamage()
	CLuaMonsterPropertyFight.RefreshParamMpFull()
	CLuaMonsterPropertyFight.RefreshParamEnhanceLight()
	CLuaMonsterPropertyFight.RefreshParamAntiFire()
	CLuaMonsterPropertyFight.RefreshParammAttMax()
	CLuaMonsterPropertyFight.RefreshParampSpeed()
	CLuaMonsterPropertyFight.RefreshParamMpRecover()
	CLuaMonsterPropertyFight.RefreshParammMiss()
	CLuaMonsterPropertyFight.RefreshParamAntimFatalDamage()
	CLuaMonsterPropertyFight.RefreshParamEnhanceIllusion()
	CLuaMonsterPropertyFight.RefreshParamEnhanceDizzy()
	CLuaMonsterPropertyFight.RefreshParamHpRecover()
	
	CLuaMonsterPropertyFight.Param = nil
	CLuaMonsterPropertyFight.TmpParam = nil
end
SetParamFunctions[EnumMonsterFightProp.PermanentCor] = CLuaMonsterPropertyFight.SetParamPermanentCor
SetParamFunctions[EnumMonsterFightProp.PermanentSta] = CLuaMonsterPropertyFight.SetParamPermanentSta
SetParamFunctions[EnumMonsterFightProp.PermanentStr] = CLuaMonsterPropertyFight.SetParamPermanentStr
SetParamFunctions[EnumMonsterFightProp.PermanentInt] = CLuaMonsterPropertyFight.SetParamPermanentInt
SetParamFunctions[EnumMonsterFightProp.PermanentAgi] = CLuaMonsterPropertyFight.SetParamPermanentAgi
SetParamFunctions[EnumMonsterFightProp.PermanentHpFull] = CLuaMonsterPropertyFight.SetParamPermanentHpFull
SetParamFunctions[EnumMonsterFightProp.Hp] = CLuaMonsterPropertyFight.SetParamHp
SetParamFunctions[EnumMonsterFightProp.Mp] = CLuaMonsterPropertyFight.SetParamMp
SetParamFunctions[EnumMonsterFightProp.CurrentHpFull] = CLuaMonsterPropertyFight.SetParamCurrentHpFull
SetParamFunctions[EnumMonsterFightProp.CurrentMpFull] = CLuaMonsterPropertyFight.SetParamCurrentMpFull
SetParamFunctions[EnumMonsterFightProp.EquipExchangeMF] = CLuaMonsterPropertyFight.SetParamEquipExchangeMF
SetParamFunctions[EnumMonsterFightProp.RevisePermanentCor] = CLuaMonsterPropertyFight.SetParamRevisePermanentCor
SetParamFunctions[EnumMonsterFightProp.RevisePermanentSta] = CLuaMonsterPropertyFight.SetParamRevisePermanentSta
SetParamFunctions[EnumMonsterFightProp.RevisePermanentStr] = CLuaMonsterPropertyFight.SetParamRevisePermanentStr
SetParamFunctions[EnumMonsterFightProp.RevisePermanentInt] = CLuaMonsterPropertyFight.SetParamRevisePermanentInt
SetParamFunctions[EnumMonsterFightProp.RevisePermanentAgi] = CLuaMonsterPropertyFight.SetParamRevisePermanentAgi
SetParamFunctions[EnumMonsterFightProp.Class] = CLuaMonsterPropertyFight.SetParamClass
SetParamFunctions[EnumMonsterFightProp.Grade] = CLuaMonsterPropertyFight.SetParamGrade
SetParamFunctions[EnumMonsterFightProp.Cor] = CLuaMonsterPropertyFight.SetParamCor
SetParamFunctions[EnumMonsterFightProp.AdjCor] = CLuaMonsterPropertyFight.SetParamAdjCor
SetParamFunctions[EnumMonsterFightProp.MulCor] = CLuaMonsterPropertyFight.SetParamMulCor
SetParamFunctions[EnumMonsterFightProp.Sta] = CLuaMonsterPropertyFight.SetParamSta
SetParamFunctions[EnumMonsterFightProp.AdjSta] = CLuaMonsterPropertyFight.SetParamAdjSta
SetParamFunctions[EnumMonsterFightProp.MulSta] = CLuaMonsterPropertyFight.SetParamMulSta
SetParamFunctions[EnumMonsterFightProp.Str] = CLuaMonsterPropertyFight.SetParamStr
SetParamFunctions[EnumMonsterFightProp.AdjStr] = CLuaMonsterPropertyFight.SetParamAdjStr
SetParamFunctions[EnumMonsterFightProp.MulStr] = CLuaMonsterPropertyFight.SetParamMulStr
SetParamFunctions[EnumMonsterFightProp.Int] = CLuaMonsterPropertyFight.SetParamInt
SetParamFunctions[EnumMonsterFightProp.AdjInt] = CLuaMonsterPropertyFight.SetParamAdjInt
SetParamFunctions[EnumMonsterFightProp.MulInt] = CLuaMonsterPropertyFight.SetParamMulInt
SetParamFunctions[EnumMonsterFightProp.Agi] = CLuaMonsterPropertyFight.SetParamAgi
SetParamFunctions[EnumMonsterFightProp.AdjAgi] = CLuaMonsterPropertyFight.SetParamAdjAgi
SetParamFunctions[EnumMonsterFightProp.MulAgi] = CLuaMonsterPropertyFight.SetParamMulAgi
SetParamFunctions[EnumMonsterFightProp.AdjHpFull] = CLuaMonsterPropertyFight.SetParamAdjHpFull
SetParamFunctions[EnumMonsterFightProp.MulHpFull] = CLuaMonsterPropertyFight.SetParamMulHpFull
SetParamFunctions[EnumMonsterFightProp.AdjHpRecover] = CLuaMonsterPropertyFight.SetParamAdjHpRecover
SetParamFunctions[EnumMonsterFightProp.MulHpRecover] = CLuaMonsterPropertyFight.SetParamMulHpRecover
SetParamFunctions[EnumMonsterFightProp.AdjMpFull] = CLuaMonsterPropertyFight.SetParamAdjMpFull
SetParamFunctions[EnumMonsterFightProp.MulMpFull] = CLuaMonsterPropertyFight.SetParamMulMpFull
SetParamFunctions[EnumMonsterFightProp.AdjMpRecover] = CLuaMonsterPropertyFight.SetParamAdjMpRecover
SetParamFunctions[EnumMonsterFightProp.MulMpRecover] = CLuaMonsterPropertyFight.SetParamMulMpRecover
SetParamFunctions[EnumMonsterFightProp.AdjpAttMin] = CLuaMonsterPropertyFight.SetParamAdjpAttMin
SetParamFunctions[EnumMonsterFightProp.MulpAttMin] = CLuaMonsterPropertyFight.SetParamMulpAttMin
SetParamFunctions[EnumMonsterFightProp.AdjpAttMax] = CLuaMonsterPropertyFight.SetParamAdjpAttMax
SetParamFunctions[EnumMonsterFightProp.MulpAttMax] = CLuaMonsterPropertyFight.SetParamMulpAttMax
SetParamFunctions[EnumMonsterFightProp.AdjpHit] = CLuaMonsterPropertyFight.SetParamAdjpHit
SetParamFunctions[EnumMonsterFightProp.MulpHit] = CLuaMonsterPropertyFight.SetParamMulpHit
SetParamFunctions[EnumMonsterFightProp.AdjpMiss] = CLuaMonsterPropertyFight.SetParamAdjpMiss
SetParamFunctions[EnumMonsterFightProp.MulpMiss] = CLuaMonsterPropertyFight.SetParamMulpMiss
SetParamFunctions[EnumMonsterFightProp.AdjpDef] = CLuaMonsterPropertyFight.SetParamAdjpDef
SetParamFunctions[EnumMonsterFightProp.MulpDef] = CLuaMonsterPropertyFight.SetParamMulpDef
SetParamFunctions[EnumMonsterFightProp.AdjpHurt] = CLuaMonsterPropertyFight.SetParamAdjpHurt
SetParamFunctions[EnumMonsterFightProp.MulpHurt] = CLuaMonsterPropertyFight.SetParamMulpHurt
SetParamFunctions[EnumMonsterFightProp.OripSpeed] = CLuaMonsterPropertyFight.SetParamOripSpeed
SetParamFunctions[EnumMonsterFightProp.MulpSpeed] = CLuaMonsterPropertyFight.SetParamMulpSpeed
SetParamFunctions[EnumMonsterFightProp.AdjpFatal] = CLuaMonsterPropertyFight.SetParamAdjpFatal
SetParamFunctions[EnumMonsterFightProp.AdjAntipFatal] = CLuaMonsterPropertyFight.SetParamAdjAntipFatal
SetParamFunctions[EnumMonsterFightProp.AdjpFatalDamage] = CLuaMonsterPropertyFight.SetParamAdjpFatalDamage
SetParamFunctions[EnumMonsterFightProp.AdjAntipFatalDamage] = CLuaMonsterPropertyFight.SetParamAdjAntipFatalDamage
SetParamFunctions[EnumMonsterFightProp.AdjBlock] = CLuaMonsterPropertyFight.SetParamAdjBlock
SetParamFunctions[EnumMonsterFightProp.MulBlock] = CLuaMonsterPropertyFight.SetParamMulBlock
SetParamFunctions[EnumMonsterFightProp.AdjBlockDamage] = CLuaMonsterPropertyFight.SetParamAdjBlockDamage
SetParamFunctions[EnumMonsterFightProp.AdjAntiBlock] = CLuaMonsterPropertyFight.SetParamAdjAntiBlock
SetParamFunctions[EnumMonsterFightProp.AdjAntiBlockDamage] = CLuaMonsterPropertyFight.SetParamAdjAntiBlockDamage
SetParamFunctions[EnumMonsterFightProp.AdjmAttMin] = CLuaMonsterPropertyFight.SetParamAdjmAttMin
SetParamFunctions[EnumMonsterFightProp.MulmAttMin] = CLuaMonsterPropertyFight.SetParamMulmAttMin
SetParamFunctions[EnumMonsterFightProp.AdjmAttMax] = CLuaMonsterPropertyFight.SetParamAdjmAttMax
SetParamFunctions[EnumMonsterFightProp.MulmAttMax] = CLuaMonsterPropertyFight.SetParamMulmAttMax
SetParamFunctions[EnumMonsterFightProp.AdjmHit] = CLuaMonsterPropertyFight.SetParamAdjmHit
SetParamFunctions[EnumMonsterFightProp.MulmHit] = CLuaMonsterPropertyFight.SetParamMulmHit
SetParamFunctions[EnumMonsterFightProp.AdjmMiss] = CLuaMonsterPropertyFight.SetParamAdjmMiss
SetParamFunctions[EnumMonsterFightProp.MulmMiss] = CLuaMonsterPropertyFight.SetParamMulmMiss
SetParamFunctions[EnumMonsterFightProp.AdjmDef] = CLuaMonsterPropertyFight.SetParamAdjmDef
SetParamFunctions[EnumMonsterFightProp.MulmDef] = CLuaMonsterPropertyFight.SetParamMulmDef
SetParamFunctions[EnumMonsterFightProp.AdjmHurt] = CLuaMonsterPropertyFight.SetParamAdjmHurt
SetParamFunctions[EnumMonsterFightProp.MulmHurt] = CLuaMonsterPropertyFight.SetParamMulmHurt
SetParamFunctions[EnumMonsterFightProp.OrimSpeed] = CLuaMonsterPropertyFight.SetParamOrimSpeed
SetParamFunctions[EnumMonsterFightProp.MulmSpeed] = CLuaMonsterPropertyFight.SetParamMulmSpeed
SetParamFunctions[EnumMonsterFightProp.AdjmFatal] = CLuaMonsterPropertyFight.SetParamAdjmFatal
SetParamFunctions[EnumMonsterFightProp.AdjAntimFatal] = CLuaMonsterPropertyFight.SetParamAdjAntimFatal
SetParamFunctions[EnumMonsterFightProp.AdjmFatalDamage] = CLuaMonsterPropertyFight.SetParamAdjmFatalDamage
SetParamFunctions[EnumMonsterFightProp.AdjAntimFatalDamage] = CLuaMonsterPropertyFight.SetParamAdjAntimFatalDamage
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceFire] = CLuaMonsterPropertyFight.SetParamAdjEnhanceFire
SetParamFunctions[EnumMonsterFightProp.MulEnhanceFire] = CLuaMonsterPropertyFight.SetParamMulEnhanceFire
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceThunder] = CLuaMonsterPropertyFight.SetParamAdjEnhanceThunder
SetParamFunctions[EnumMonsterFightProp.MulEnhanceThunder] = CLuaMonsterPropertyFight.SetParamMulEnhanceThunder
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceIce] = CLuaMonsterPropertyFight.SetParamAdjEnhanceIce
SetParamFunctions[EnumMonsterFightProp.MulEnhanceIce] = CLuaMonsterPropertyFight.SetParamMulEnhanceIce
SetParamFunctions[EnumMonsterFightProp.AdjEnhancePoison] = CLuaMonsterPropertyFight.SetParamAdjEnhancePoison
SetParamFunctions[EnumMonsterFightProp.MulEnhancePoison] = CLuaMonsterPropertyFight.SetParamMulEnhancePoison
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceWind] = CLuaMonsterPropertyFight.SetParamAdjEnhanceWind
SetParamFunctions[EnumMonsterFightProp.MulEnhanceWind] = CLuaMonsterPropertyFight.SetParamMulEnhanceWind
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceLight] = CLuaMonsterPropertyFight.SetParamAdjEnhanceLight
SetParamFunctions[EnumMonsterFightProp.MulEnhanceLight] = CLuaMonsterPropertyFight.SetParamMulEnhanceLight
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceIllusion] = CLuaMonsterPropertyFight.SetParamAdjEnhanceIllusion
SetParamFunctions[EnumMonsterFightProp.MulEnhanceIllusion] = CLuaMonsterPropertyFight.SetParamMulEnhanceIllusion
SetParamFunctions[EnumMonsterFightProp.AdjAntiFire] = CLuaMonsterPropertyFight.SetParamAdjAntiFire
SetParamFunctions[EnumMonsterFightProp.MulAntiFire] = CLuaMonsterPropertyFight.SetParamMulAntiFire
SetParamFunctions[EnumMonsterFightProp.AdjAntiThunder] = CLuaMonsterPropertyFight.SetParamAdjAntiThunder
SetParamFunctions[EnumMonsterFightProp.MulAntiThunder] = CLuaMonsterPropertyFight.SetParamMulAntiThunder
SetParamFunctions[EnumMonsterFightProp.AdjAntiIce] = CLuaMonsterPropertyFight.SetParamAdjAntiIce
SetParamFunctions[EnumMonsterFightProp.MulAntiIce] = CLuaMonsterPropertyFight.SetParamMulAntiIce
SetParamFunctions[EnumMonsterFightProp.AdjAntiPoison] = CLuaMonsterPropertyFight.SetParamAdjAntiPoison
SetParamFunctions[EnumMonsterFightProp.MulAntiPoison] = CLuaMonsterPropertyFight.SetParamMulAntiPoison
SetParamFunctions[EnumMonsterFightProp.AdjAntiWind] = CLuaMonsterPropertyFight.SetParamAdjAntiWind
SetParamFunctions[EnumMonsterFightProp.MulAntiWind] = CLuaMonsterPropertyFight.SetParamMulAntiWind
SetParamFunctions[EnumMonsterFightProp.AdjAntiLight] = CLuaMonsterPropertyFight.SetParamAdjAntiLight
SetParamFunctions[EnumMonsterFightProp.MulAntiLight] = CLuaMonsterPropertyFight.SetParamMulAntiLight
SetParamFunctions[EnumMonsterFightProp.AdjAntiIllusion] = CLuaMonsterPropertyFight.SetParamAdjAntiIllusion
SetParamFunctions[EnumMonsterFightProp.MulAntiIllusion] = CLuaMonsterPropertyFight.SetParamMulAntiIllusion
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceDizzy] = CLuaMonsterPropertyFight.SetParamAdjEnhanceDizzy
SetParamFunctions[EnumMonsterFightProp.MulEnhanceDizzy] = CLuaMonsterPropertyFight.SetParamMulEnhanceDizzy
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceSleep] = CLuaMonsterPropertyFight.SetParamAdjEnhanceSleep
SetParamFunctions[EnumMonsterFightProp.MulEnhanceSleep] = CLuaMonsterPropertyFight.SetParamMulEnhanceSleep
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceChaos] = CLuaMonsterPropertyFight.SetParamAdjEnhanceChaos
SetParamFunctions[EnumMonsterFightProp.MulEnhanceChaos] = CLuaMonsterPropertyFight.SetParamMulEnhanceChaos
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceBind] = CLuaMonsterPropertyFight.SetParamAdjEnhanceBind
SetParamFunctions[EnumMonsterFightProp.MulEnhanceBind] = CLuaMonsterPropertyFight.SetParamMulEnhanceBind
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceSilence] = CLuaMonsterPropertyFight.SetParamAdjEnhanceSilence
SetParamFunctions[EnumMonsterFightProp.MulEnhanceSilence] = CLuaMonsterPropertyFight.SetParamMulEnhanceSilence
SetParamFunctions[EnumMonsterFightProp.AdjAntiDizzy] = CLuaMonsterPropertyFight.SetParamAdjAntiDizzy
SetParamFunctions[EnumMonsterFightProp.MulAntiDizzy] = CLuaMonsterPropertyFight.SetParamMulAntiDizzy
SetParamFunctions[EnumMonsterFightProp.AdjAntiSleep] = CLuaMonsterPropertyFight.SetParamAdjAntiSleep
SetParamFunctions[EnumMonsterFightProp.MulAntiSleep] = CLuaMonsterPropertyFight.SetParamMulAntiSleep
SetParamFunctions[EnumMonsterFightProp.AdjAntiChaos] = CLuaMonsterPropertyFight.SetParamAdjAntiChaos
SetParamFunctions[EnumMonsterFightProp.MulAntiChaos] = CLuaMonsterPropertyFight.SetParamMulAntiChaos
SetParamFunctions[EnumMonsterFightProp.AdjAntiBind] = CLuaMonsterPropertyFight.SetParamAdjAntiBind
SetParamFunctions[EnumMonsterFightProp.MulAntiBind] = CLuaMonsterPropertyFight.SetParamMulAntiBind
SetParamFunctions[EnumMonsterFightProp.AdjAntiSilence] = CLuaMonsterPropertyFight.SetParamAdjAntiSilence
SetParamFunctions[EnumMonsterFightProp.MulAntiSilence] = CLuaMonsterPropertyFight.SetParamMulAntiSilence
SetParamFunctions[EnumMonsterFightProp.DizzyTimeChange] = CLuaMonsterPropertyFight.SetParamDizzyTimeChange
SetParamFunctions[EnumMonsterFightProp.SleepTimeChange] = CLuaMonsterPropertyFight.SetParamSleepTimeChange
SetParamFunctions[EnumMonsterFightProp.ChaosTimeChange] = CLuaMonsterPropertyFight.SetParamChaosTimeChange
SetParamFunctions[EnumMonsterFightProp.BindTimeChange] = CLuaMonsterPropertyFight.SetParamBindTimeChange
SetParamFunctions[EnumMonsterFightProp.SilenceTimeChange] = CLuaMonsterPropertyFight.SetParamSilenceTimeChange
SetParamFunctions[EnumMonsterFightProp.BianhuTimeChange] = CLuaMonsterPropertyFight.SetParamBianhuTimeChange
SetParamFunctions[EnumMonsterFightProp.FreezeTimeChange] = CLuaMonsterPropertyFight.SetParamFreezeTimeChange
SetParamFunctions[EnumMonsterFightProp.PetrifyTimeChange] = CLuaMonsterPropertyFight.SetParamPetrifyTimeChange
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceHeal] = CLuaMonsterPropertyFight.SetParamAdjEnhanceHeal
SetParamFunctions[EnumMonsterFightProp.MulEnhanceHeal] = CLuaMonsterPropertyFight.SetParamMulEnhanceHeal
SetParamFunctions[EnumMonsterFightProp.OriSpeed] = CLuaMonsterPropertyFight.SetParamOriSpeed
SetParamFunctions[EnumMonsterFightProp.AdjSpeed] = CLuaMonsterPropertyFight.SetParamAdjSpeed
SetParamFunctions[EnumMonsterFightProp.MulSpeed] = CLuaMonsterPropertyFight.SetParamMulSpeed
SetParamFunctions[EnumMonsterFightProp.MF] = CLuaMonsterPropertyFight.SetParamMF
SetParamFunctions[EnumMonsterFightProp.AdjMF] = CLuaMonsterPropertyFight.SetParamAdjMF
SetParamFunctions[EnumMonsterFightProp.Range] = CLuaMonsterPropertyFight.SetParamRange
SetParamFunctions[EnumMonsterFightProp.AdjRange] = CLuaMonsterPropertyFight.SetParamAdjRange
SetParamFunctions[EnumMonsterFightProp.HatePlus] = CLuaMonsterPropertyFight.SetParamHatePlus
SetParamFunctions[EnumMonsterFightProp.AdjHatePlus] = CLuaMonsterPropertyFight.SetParamAdjHatePlus
SetParamFunctions[EnumMonsterFightProp.HateDecrease] = CLuaMonsterPropertyFight.SetParamHateDecrease
SetParamFunctions[EnumMonsterFightProp.AdjInvisible] = CLuaMonsterPropertyFight.SetParamAdjInvisible
SetParamFunctions[EnumMonsterFightProp.OriTrueSight] = CLuaMonsterPropertyFight.SetParamOriTrueSight
SetParamFunctions[EnumMonsterFightProp.AdjTrueSight] = CLuaMonsterPropertyFight.SetParamAdjTrueSight
SetParamFunctions[EnumMonsterFightProp.OriEyeSight] = CLuaMonsterPropertyFight.SetParamOriEyeSight
SetParamFunctions[EnumMonsterFightProp.AdjEyeSight] = CLuaMonsterPropertyFight.SetParamAdjEyeSight
SetParamFunctions[EnumMonsterFightProp.EnhanceTian] = CLuaMonsterPropertyFight.SetParamEnhanceTian
SetParamFunctions[EnumMonsterFightProp.EnhanceTianMul] = CLuaMonsterPropertyFight.SetParamEnhanceTianMul
SetParamFunctions[EnumMonsterFightProp.EnhanceEGui] = CLuaMonsterPropertyFight.SetParamEnhanceEGui
SetParamFunctions[EnumMonsterFightProp.EnhanceEGuiMul] = CLuaMonsterPropertyFight.SetParamEnhanceEGuiMul
SetParamFunctions[EnumMonsterFightProp.EnhanceXiuLuo] = CLuaMonsterPropertyFight.SetParamEnhanceXiuLuo
SetParamFunctions[EnumMonsterFightProp.EnhanceXiuLuoMul] = CLuaMonsterPropertyFight.SetParamEnhanceXiuLuoMul
SetParamFunctions[EnumMonsterFightProp.EnhanceDiYu] = CLuaMonsterPropertyFight.SetParamEnhanceDiYu
SetParamFunctions[EnumMonsterFightProp.EnhanceDiYuMul] = CLuaMonsterPropertyFight.SetParamEnhanceDiYuMul
SetParamFunctions[EnumMonsterFightProp.EnhanceRen] = CLuaMonsterPropertyFight.SetParamEnhanceRen
SetParamFunctions[EnumMonsterFightProp.EnhanceRenMul] = CLuaMonsterPropertyFight.SetParamEnhanceRenMul
SetParamFunctions[EnumMonsterFightProp.EnhanceChuSheng] = CLuaMonsterPropertyFight.SetParamEnhanceChuSheng
SetParamFunctions[EnumMonsterFightProp.EnhanceChuShengMul] = CLuaMonsterPropertyFight.SetParamEnhanceChuShengMul
SetParamFunctions[EnumMonsterFightProp.EnhanceBuilding] = CLuaMonsterPropertyFight.SetParamEnhanceBuilding
SetParamFunctions[EnumMonsterFightProp.EnhanceBuildingMul] = CLuaMonsterPropertyFight.SetParamEnhanceBuildingMul
SetParamFunctions[EnumMonsterFightProp.EnhanceBoss] = CLuaMonsterPropertyFight.SetParamEnhanceBoss
SetParamFunctions[EnumMonsterFightProp.EnhanceBossMul] = CLuaMonsterPropertyFight.SetParamEnhanceBossMul
SetParamFunctions[EnumMonsterFightProp.EnhanceSheShou] = CLuaMonsterPropertyFight.SetParamEnhanceSheShou
SetParamFunctions[EnumMonsterFightProp.EnhanceSheShouMul] = CLuaMonsterPropertyFight.SetParamEnhanceSheShouMul
SetParamFunctions[EnumMonsterFightProp.EnhanceJiaShi] = CLuaMonsterPropertyFight.SetParamEnhanceJiaShi
SetParamFunctions[EnumMonsterFightProp.EnhanceJiaShiMul] = CLuaMonsterPropertyFight.SetParamEnhanceJiaShiMul
SetParamFunctions[EnumMonsterFightProp.EnhanceFangShi] = CLuaMonsterPropertyFight.SetParamEnhanceFangShi
SetParamFunctions[EnumMonsterFightProp.EnhanceFangShiMul] = CLuaMonsterPropertyFight.SetParamEnhanceFangShiMul
SetParamFunctions[EnumMonsterFightProp.EnhanceYiShi] = CLuaMonsterPropertyFight.SetParamEnhanceYiShi
SetParamFunctions[EnumMonsterFightProp.EnhanceYiShiMul] = CLuaMonsterPropertyFight.SetParamEnhanceYiShiMul
SetParamFunctions[EnumMonsterFightProp.EnhanceMeiZhe] = CLuaMonsterPropertyFight.SetParamEnhanceMeiZhe
SetParamFunctions[EnumMonsterFightProp.EnhanceMeiZheMul] = CLuaMonsterPropertyFight.SetParamEnhanceMeiZheMul
SetParamFunctions[EnumMonsterFightProp.EnhanceYiRen] = CLuaMonsterPropertyFight.SetParamEnhanceYiRen
SetParamFunctions[EnumMonsterFightProp.EnhanceYiRenMul] = CLuaMonsterPropertyFight.SetParamEnhanceYiRenMul
SetParamFunctions[EnumMonsterFightProp.IgnorepDef] = CLuaMonsterPropertyFight.SetParamIgnorepDef
SetParamFunctions[EnumMonsterFightProp.IgnoremDef] = CLuaMonsterPropertyFight.SetParamIgnoremDef
SetParamFunctions[EnumMonsterFightProp.EnhanceZhaoHuan] = CLuaMonsterPropertyFight.SetParamEnhanceZhaoHuan
SetParamFunctions[EnumMonsterFightProp.EnhanceZhaoHuanMul] = CLuaMonsterPropertyFight.SetParamEnhanceZhaoHuanMul
SetParamFunctions[EnumMonsterFightProp.DizzyProbability] = CLuaMonsterPropertyFight.SetParamDizzyProbability
SetParamFunctions[EnumMonsterFightProp.SleepProbability] = CLuaMonsterPropertyFight.SetParamSleepProbability
SetParamFunctions[EnumMonsterFightProp.ChaosProbability] = CLuaMonsterPropertyFight.SetParamChaosProbability
SetParamFunctions[EnumMonsterFightProp.BindProbability] = CLuaMonsterPropertyFight.SetParamBindProbability
SetParamFunctions[EnumMonsterFightProp.SilenceProbability] = CLuaMonsterPropertyFight.SetParamSilenceProbability
SetParamFunctions[EnumMonsterFightProp.HpFullPow2] = CLuaMonsterPropertyFight.SetParamHpFullPow2
SetParamFunctions[EnumMonsterFightProp.HpFullPow1] = CLuaMonsterPropertyFight.SetParamHpFullPow1
SetParamFunctions[EnumMonsterFightProp.HpFullInit] = CLuaMonsterPropertyFight.SetParamHpFullInit
SetParamFunctions[EnumMonsterFightProp.MpFullPow1] = CLuaMonsterPropertyFight.SetParamMpFullPow1
SetParamFunctions[EnumMonsterFightProp.MpFullInit] = CLuaMonsterPropertyFight.SetParamMpFullInit
SetParamFunctions[EnumMonsterFightProp.MpRecoverPow1] = CLuaMonsterPropertyFight.SetParamMpRecoverPow1
SetParamFunctions[EnumMonsterFightProp.MpRecoverInit] = CLuaMonsterPropertyFight.SetParamMpRecoverInit
SetParamFunctions[EnumMonsterFightProp.PAttMinPow2] = CLuaMonsterPropertyFight.SetParamPAttMinPow2
SetParamFunctions[EnumMonsterFightProp.PAttMinPow1] = CLuaMonsterPropertyFight.SetParamPAttMinPow1
SetParamFunctions[EnumMonsterFightProp.PAttMinInit] = CLuaMonsterPropertyFight.SetParamPAttMinInit
SetParamFunctions[EnumMonsterFightProp.PAttMaxPow2] = CLuaMonsterPropertyFight.SetParamPAttMaxPow2
SetParamFunctions[EnumMonsterFightProp.PAttMaxPow1] = CLuaMonsterPropertyFight.SetParamPAttMaxPow1
SetParamFunctions[EnumMonsterFightProp.PAttMaxInit] = CLuaMonsterPropertyFight.SetParamPAttMaxInit
SetParamFunctions[EnumMonsterFightProp.PhitPow1] = CLuaMonsterPropertyFight.SetParamPhitPow1
SetParamFunctions[EnumMonsterFightProp.PHitInit] = CLuaMonsterPropertyFight.SetParamPHitInit
SetParamFunctions[EnumMonsterFightProp.PMissPow1] = CLuaMonsterPropertyFight.SetParamPMissPow1
SetParamFunctions[EnumMonsterFightProp.PMissInit] = CLuaMonsterPropertyFight.SetParamPMissInit
SetParamFunctions[EnumMonsterFightProp.PSpeedPow1] = CLuaMonsterPropertyFight.SetParamPSpeedPow1
SetParamFunctions[EnumMonsterFightProp.PSpeedInit] = CLuaMonsterPropertyFight.SetParamPSpeedInit
SetParamFunctions[EnumMonsterFightProp.PDefPow1] = CLuaMonsterPropertyFight.SetParamPDefPow1
SetParamFunctions[EnumMonsterFightProp.PDefInit] = CLuaMonsterPropertyFight.SetParamPDefInit
SetParamFunctions[EnumMonsterFightProp.PfatalPow1] = CLuaMonsterPropertyFight.SetParamPfatalPow1
SetParamFunctions[EnumMonsterFightProp.PFatalInit] = CLuaMonsterPropertyFight.SetParamPFatalInit
SetParamFunctions[EnumMonsterFightProp.MAttMinPow2] = CLuaMonsterPropertyFight.SetParamMAttMinPow2
SetParamFunctions[EnumMonsterFightProp.MAttMinPow1] = CLuaMonsterPropertyFight.SetParamMAttMinPow1
SetParamFunctions[EnumMonsterFightProp.MAttMinInit] = CLuaMonsterPropertyFight.SetParamMAttMinInit
SetParamFunctions[EnumMonsterFightProp.MAttMaxPow2] = CLuaMonsterPropertyFight.SetParamMAttMaxPow2
SetParamFunctions[EnumMonsterFightProp.MAttMaxPow1] = CLuaMonsterPropertyFight.SetParamMAttMaxPow1
SetParamFunctions[EnumMonsterFightProp.MAttMaxInit] = CLuaMonsterPropertyFight.SetParamMAttMaxInit
SetParamFunctions[EnumMonsterFightProp.MSpeedPow1] = CLuaMonsterPropertyFight.SetParamMSpeedPow1
SetParamFunctions[EnumMonsterFightProp.MSpeedInit] = CLuaMonsterPropertyFight.SetParamMSpeedInit
SetParamFunctions[EnumMonsterFightProp.MDefPow1] = CLuaMonsterPropertyFight.SetParamMDefPow1
SetParamFunctions[EnumMonsterFightProp.MDefInit] = CLuaMonsterPropertyFight.SetParamMDefInit
SetParamFunctions[EnumMonsterFightProp.MHitPow1] = CLuaMonsterPropertyFight.SetParamMHitPow1
SetParamFunctions[EnumMonsterFightProp.MHitInit] = CLuaMonsterPropertyFight.SetParamMHitInit
SetParamFunctions[EnumMonsterFightProp.MMissPow1] = CLuaMonsterPropertyFight.SetParamMMissPow1
SetParamFunctions[EnumMonsterFightProp.MMissInit] = CLuaMonsterPropertyFight.SetParamMMissInit
SetParamFunctions[EnumMonsterFightProp.MFatalPow1] = CLuaMonsterPropertyFight.SetParamMFatalPow1
SetParamFunctions[EnumMonsterFightProp.MFatalInit] = CLuaMonsterPropertyFight.SetParamMFatalInit
SetParamFunctions[EnumMonsterFightProp.BlockDamagePow1] = CLuaMonsterPropertyFight.SetParamBlockDamagePow1
SetParamFunctions[EnumMonsterFightProp.BlockDamageInit] = CLuaMonsterPropertyFight.SetParamBlockDamageInit
SetParamFunctions[EnumMonsterFightProp.RunSpeed] = CLuaMonsterPropertyFight.SetParamRunSpeed
SetParamFunctions[EnumMonsterFightProp.HpRecoverPow1] = CLuaMonsterPropertyFight.SetParamHpRecoverPow1
SetParamFunctions[EnumMonsterFightProp.HpRecoverInit] = CLuaMonsterPropertyFight.SetParamHpRecoverInit
SetParamFunctions[EnumMonsterFightProp.AntiBlockDamagePow1] = CLuaMonsterPropertyFight.SetParamAntiBlockDamagePow1
SetParamFunctions[EnumMonsterFightProp.AntiBlockDamageInit] = CLuaMonsterPropertyFight.SetParamAntiBlockDamageInit
SetParamFunctions[EnumMonsterFightProp.BBMax] = CLuaMonsterPropertyFight.SetParamBBMax
SetParamFunctions[EnumMonsterFightProp.AdjBBMax] = CLuaMonsterPropertyFight.SetParamAdjBBMax
SetParamFunctions[EnumMonsterFightProp.AdjAntiBreak] = CLuaMonsterPropertyFight.SetParamAdjAntiBreak
SetParamFunctions[EnumMonsterFightProp.AdjAntiPushPull] = CLuaMonsterPropertyFight.SetParamAdjAntiPushPull
SetParamFunctions[EnumMonsterFightProp.AdjAntiPetrify] = CLuaMonsterPropertyFight.SetParamAdjAntiPetrify
SetParamFunctions[EnumMonsterFightProp.MulAntiPetrify] = CLuaMonsterPropertyFight.SetParamMulAntiPetrify
SetParamFunctions[EnumMonsterFightProp.AdjAntiTie] = CLuaMonsterPropertyFight.SetParamAdjAntiTie
SetParamFunctions[EnumMonsterFightProp.MulAntiTie] = CLuaMonsterPropertyFight.SetParamMulAntiTie
SetParamFunctions[EnumMonsterFightProp.AdjEnhancePetrify] = CLuaMonsterPropertyFight.SetParamAdjEnhancePetrify
SetParamFunctions[EnumMonsterFightProp.MulEnhancePetrify] = CLuaMonsterPropertyFight.SetParamMulEnhancePetrify
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceTie] = CLuaMonsterPropertyFight.SetParamAdjEnhanceTie
SetParamFunctions[EnumMonsterFightProp.MulEnhanceTie] = CLuaMonsterPropertyFight.SetParamMulEnhanceTie
SetParamFunctions[EnumMonsterFightProp.TieProbability] = CLuaMonsterPropertyFight.SetParamTieProbability
SetParamFunctions[EnumMonsterFightProp.StrZizhi] = CLuaMonsterPropertyFight.SetParamStrZizhi
SetParamFunctions[EnumMonsterFightProp.CorZizhi] = CLuaMonsterPropertyFight.SetParamCorZizhi
SetParamFunctions[EnumMonsterFightProp.StaZizhi] = CLuaMonsterPropertyFight.SetParamStaZizhi
SetParamFunctions[EnumMonsterFightProp.AgiZizhi] = CLuaMonsterPropertyFight.SetParamAgiZizhi
SetParamFunctions[EnumMonsterFightProp.IntZizhi] = CLuaMonsterPropertyFight.SetParamIntZizhi
SetParamFunctions[EnumMonsterFightProp.InitStrZizhi] = CLuaMonsterPropertyFight.SetParamInitStrZizhi
SetParamFunctions[EnumMonsterFightProp.InitCorZizhi] = CLuaMonsterPropertyFight.SetParamInitCorZizhi
SetParamFunctions[EnumMonsterFightProp.InitStaZizhi] = CLuaMonsterPropertyFight.SetParamInitStaZizhi
SetParamFunctions[EnumMonsterFightProp.InitAgiZizhi] = CLuaMonsterPropertyFight.SetParamInitAgiZizhi
SetParamFunctions[EnumMonsterFightProp.InitIntZizhi] = CLuaMonsterPropertyFight.SetParamInitIntZizhi
SetParamFunctions[EnumMonsterFightProp.Wuxing] = CLuaMonsterPropertyFight.SetParamWuxing
SetParamFunctions[EnumMonsterFightProp.Xiuwei] = CLuaMonsterPropertyFight.SetParamXiuwei
SetParamFunctions[EnumMonsterFightProp.SkillNum] = CLuaMonsterPropertyFight.SetParamSkillNum
SetParamFunctions[EnumMonsterFightProp.PType] = CLuaMonsterPropertyFight.SetParamPType
SetParamFunctions[EnumMonsterFightProp.MType] = CLuaMonsterPropertyFight.SetParamMType
SetParamFunctions[EnumMonsterFightProp.PHType] = CLuaMonsterPropertyFight.SetParamPHType
SetParamFunctions[EnumMonsterFightProp.GrowFactor] = CLuaMonsterPropertyFight.SetParamGrowFactor
SetParamFunctions[EnumMonsterFightProp.WuxingImprove] = CLuaMonsterPropertyFight.SetParamWuxingImprove
SetParamFunctions[EnumMonsterFightProp.XiuweiImprove] = CLuaMonsterPropertyFight.SetParamXiuweiImprove
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceBeHeal] = CLuaMonsterPropertyFight.SetParamAdjEnhanceBeHeal
SetParamFunctions[EnumMonsterFightProp.MulEnhanceBeHeal] = CLuaMonsterPropertyFight.SetParamMulEnhanceBeHeal
SetParamFunctions[EnumMonsterFightProp.LookUpCor] = CLuaMonsterPropertyFight.SetParamLookUpCor
SetParamFunctions[EnumMonsterFightProp.LookUpSta] = CLuaMonsterPropertyFight.SetParamLookUpSta
SetParamFunctions[EnumMonsterFightProp.LookUpStr] = CLuaMonsterPropertyFight.SetParamLookUpStr
SetParamFunctions[EnumMonsterFightProp.LookUpInt] = CLuaMonsterPropertyFight.SetParamLookUpInt
SetParamFunctions[EnumMonsterFightProp.LookUpAgi] = CLuaMonsterPropertyFight.SetParamLookUpAgi
SetParamFunctions[EnumMonsterFightProp.LookUpHpFull] = CLuaMonsterPropertyFight.SetParamLookUpHpFull
SetParamFunctions[EnumMonsterFightProp.LookUpMpFull] = CLuaMonsterPropertyFight.SetParamLookUpMpFull
SetParamFunctions[EnumMonsterFightProp.LookUppAttMin] = CLuaMonsterPropertyFight.SetParamLookUppAttMin
SetParamFunctions[EnumMonsterFightProp.LookUppAttMax] = CLuaMonsterPropertyFight.SetParamLookUppAttMax
SetParamFunctions[EnumMonsterFightProp.LookUppHit] = CLuaMonsterPropertyFight.SetParamLookUppHit
SetParamFunctions[EnumMonsterFightProp.LookUppMiss] = CLuaMonsterPropertyFight.SetParamLookUppMiss
SetParamFunctions[EnumMonsterFightProp.LookUppDef] = CLuaMonsterPropertyFight.SetParamLookUppDef
SetParamFunctions[EnumMonsterFightProp.LookUpmAttMin] = CLuaMonsterPropertyFight.SetParamLookUpmAttMin
SetParamFunctions[EnumMonsterFightProp.LookUpmAttMax] = CLuaMonsterPropertyFight.SetParamLookUpmAttMax
SetParamFunctions[EnumMonsterFightProp.LookUpmHit] = CLuaMonsterPropertyFight.SetParamLookUpmHit
SetParamFunctions[EnumMonsterFightProp.LookUpmMiss] = CLuaMonsterPropertyFight.SetParamLookUpmMiss
SetParamFunctions[EnumMonsterFightProp.LookUpmDef] = CLuaMonsterPropertyFight.SetParamLookUpmDef
SetParamFunctions[EnumMonsterFightProp.AdjHpFull2] = CLuaMonsterPropertyFight.SetParamAdjHpFull2
SetParamFunctions[EnumMonsterFightProp.AdjpAttMin2] = CLuaMonsterPropertyFight.SetParamAdjpAttMin2
SetParamFunctions[EnumMonsterFightProp.AdjpAttMax2] = CLuaMonsterPropertyFight.SetParamAdjpAttMax2
SetParamFunctions[EnumMonsterFightProp.AdjmAttMin2] = CLuaMonsterPropertyFight.SetParamAdjmAttMin2
SetParamFunctions[EnumMonsterFightProp.AdjmAttMax2] = CLuaMonsterPropertyFight.SetParamAdjmAttMax2
SetParamFunctions[EnumMonsterFightProp.LingShouType] = CLuaMonsterPropertyFight.SetParamLingShouType
SetParamFunctions[EnumMonsterFightProp.MHType] = CLuaMonsterPropertyFight.SetParamMHType
SetParamFunctions[EnumMonsterFightProp.EnhanceLingShou] = CLuaMonsterPropertyFight.SetParamEnhanceLingShou
SetParamFunctions[EnumMonsterFightProp.EnhanceLingShouMul] = CLuaMonsterPropertyFight.SetParamEnhanceLingShouMul
SetParamFunctions[EnumMonsterFightProp.AdjAntiAoe] = CLuaMonsterPropertyFight.SetParamAdjAntiAoe
SetParamFunctions[EnumMonsterFightProp.AdjAntiBlind] = CLuaMonsterPropertyFight.SetParamAdjAntiBlind
SetParamFunctions[EnumMonsterFightProp.MulAntiBlind] = CLuaMonsterPropertyFight.SetParamMulAntiBlind
SetParamFunctions[EnumMonsterFightProp.AdjAntiDecelerate] = CLuaMonsterPropertyFight.SetParamAdjAntiDecelerate
SetParamFunctions[EnumMonsterFightProp.MulAntiDecelerate] = CLuaMonsterPropertyFight.SetParamMulAntiDecelerate
SetParamFunctions[EnumMonsterFightProp.MinGrade] = CLuaMonsterPropertyFight.SetParamMinGrade
SetParamFunctions[EnumMonsterFightProp.CharacterCor] = CLuaMonsterPropertyFight.SetParamCharacterCor
SetParamFunctions[EnumMonsterFightProp.CharacterSta] = CLuaMonsterPropertyFight.SetParamCharacterSta
SetParamFunctions[EnumMonsterFightProp.CharacterStr] = CLuaMonsterPropertyFight.SetParamCharacterStr
SetParamFunctions[EnumMonsterFightProp.CharacterInt] = CLuaMonsterPropertyFight.SetParamCharacterInt
SetParamFunctions[EnumMonsterFightProp.CharacterAgi] = CLuaMonsterPropertyFight.SetParamCharacterAgi
SetParamFunctions[EnumMonsterFightProp.EnhanceDaoKe] = CLuaMonsterPropertyFight.SetParamEnhanceDaoKe
SetParamFunctions[EnumMonsterFightProp.EnhanceDaoKeMul] = CLuaMonsterPropertyFight.SetParamEnhanceDaoKeMul
SetParamFunctions[EnumMonsterFightProp.ZuoQiSpeed] = CLuaMonsterPropertyFight.SetParamZuoQiSpeed
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceBianHu] = CLuaMonsterPropertyFight.SetParamAdjEnhanceBianHu
SetParamFunctions[EnumMonsterFightProp.MulEnhanceBianHu] = CLuaMonsterPropertyFight.SetParamMulEnhanceBianHu
SetParamFunctions[EnumMonsterFightProp.AdjAntiBianHu] = CLuaMonsterPropertyFight.SetParamAdjAntiBianHu
SetParamFunctions[EnumMonsterFightProp.MulAntiBianHu] = CLuaMonsterPropertyFight.SetParamMulAntiBianHu
SetParamFunctions[EnumMonsterFightProp.EnhanceXiaKe] = CLuaMonsterPropertyFight.SetParamEnhanceXiaKe
SetParamFunctions[EnumMonsterFightProp.EnhanceXiaKeMul] = CLuaMonsterPropertyFight.SetParamEnhanceXiaKeMul
SetParamFunctions[EnumMonsterFightProp.TieTimeChange] = CLuaMonsterPropertyFight.SetParamTieTimeChange
SetParamFunctions[EnumMonsterFightProp.EnhanceYanShi] = CLuaMonsterPropertyFight.SetParamEnhanceYanShi
SetParamFunctions[EnumMonsterFightProp.EnhanceYanShiMul] = CLuaMonsterPropertyFight.SetParamEnhanceYanShiMul
SetParamFunctions[EnumMonsterFightProp.PAType] = CLuaMonsterPropertyFight.SetParamPAType
SetParamFunctions[EnumMonsterFightProp.MAType] = CLuaMonsterPropertyFight.SetParamMAType
SetParamFunctions[EnumMonsterFightProp.lifetimeNoReduceRate] = CLuaMonsterPropertyFight.SetParamlifetimeNoReduceRate
SetParamFunctions[EnumMonsterFightProp.AddLSHpDurgImprove] = CLuaMonsterPropertyFight.SetParamAddLSHpDurgImprove
SetParamFunctions[EnumMonsterFightProp.AddHpDurgImprove] = CLuaMonsterPropertyFight.SetParamAddHpDurgImprove
SetParamFunctions[EnumMonsterFightProp.AddMpDurgImprove] = CLuaMonsterPropertyFight.SetParamAddMpDurgImprove
SetParamFunctions[EnumMonsterFightProp.FireHurtReduce] = CLuaMonsterPropertyFight.SetParamFireHurtReduce
SetParamFunctions[EnumMonsterFightProp.ThunderHurtReduce] = CLuaMonsterPropertyFight.SetParamThunderHurtReduce
SetParamFunctions[EnumMonsterFightProp.IceHurtReduce] = CLuaMonsterPropertyFight.SetParamIceHurtReduce
SetParamFunctions[EnumMonsterFightProp.PoisonHurtReduce] = CLuaMonsterPropertyFight.SetParamPoisonHurtReduce
SetParamFunctions[EnumMonsterFightProp.WindHurtReduce] = CLuaMonsterPropertyFight.SetParamWindHurtReduce
SetParamFunctions[EnumMonsterFightProp.LightHurtReduce] = CLuaMonsterPropertyFight.SetParamLightHurtReduce
SetParamFunctions[EnumMonsterFightProp.IllusionHurtReduce] = CLuaMonsterPropertyFight.SetParamIllusionHurtReduce
SetParamFunctions[EnumMonsterFightProp.FakepDef] = CLuaMonsterPropertyFight.SetParamFakepDef
SetParamFunctions[EnumMonsterFightProp.FakemDef] = CLuaMonsterPropertyFight.SetParamFakemDef
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceWater] = CLuaMonsterPropertyFight.SetParamAdjEnhanceWater
SetParamFunctions[EnumMonsterFightProp.MulEnhanceWater] = CLuaMonsterPropertyFight.SetParamMulEnhanceWater
SetParamFunctions[EnumMonsterFightProp.AdjAntiWater] = CLuaMonsterPropertyFight.SetParamAdjAntiWater
SetParamFunctions[EnumMonsterFightProp.MulAntiWater] = CLuaMonsterPropertyFight.SetParamMulAntiWater
SetParamFunctions[EnumMonsterFightProp.WaterHurtReduce] = CLuaMonsterPropertyFight.SetParamWaterHurtReduce
SetParamFunctions[EnumMonsterFightProp.AdjAntiFreeze] = CLuaMonsterPropertyFight.SetParamAdjAntiFreeze
SetParamFunctions[EnumMonsterFightProp.MulAntiFreeze] = CLuaMonsterPropertyFight.SetParamMulAntiFreeze
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceFreeze] = CLuaMonsterPropertyFight.SetParamAdjEnhanceFreeze
SetParamFunctions[EnumMonsterFightProp.MulEnhanceFreeze] = CLuaMonsterPropertyFight.SetParamMulEnhanceFreeze
SetParamFunctions[EnumMonsterFightProp.EnhanceHuaHun] = CLuaMonsterPropertyFight.SetParamEnhanceHuaHun
SetParamFunctions[EnumMonsterFightProp.EnhanceHuaHunMul] = CLuaMonsterPropertyFight.SetParamEnhanceHuaHunMul
SetParamFunctions[EnumMonsterFightProp.TrueSightProb] = CLuaMonsterPropertyFight.SetParamTrueSightProb
SetParamFunctions[EnumMonsterFightProp.LSBBGrade] = CLuaMonsterPropertyFight.SetParamLSBBGrade
SetParamFunctions[EnumMonsterFightProp.LSBBQuality] = CLuaMonsterPropertyFight.SetParamLSBBQuality
SetParamFunctions[EnumMonsterFightProp.LSBBAdjHp] = CLuaMonsterPropertyFight.SetParamLSBBAdjHp
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiFire] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiFire
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiThunder] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiThunder
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiIce] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIce
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiPoison] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiPoison
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiWind] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWind
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiLight] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiLight
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiIllusion] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIllusion
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiFireLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiFireLSMul
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiThunderLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiThunderLSMul
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiIceLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiIceLSMul
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiPoisonLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiPoisonLSMul
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiWindLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiWindLSMul
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiLightLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiLightLSMul
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiIllusionLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiIllusionLSMul
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiWater] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWater
SetParamFunctions[EnumMonsterFightProp.IgnoreAntiWaterLSMul] = CLuaMonsterPropertyFight.SetParamIgnoreAntiWaterLSMul
SetParamFunctions[EnumMonsterFightProp.MulSpeed2] = CLuaMonsterPropertyFight.SetParamMulSpeed2
SetParamFunctions[EnumMonsterFightProp.MulConsumeMp] = CLuaMonsterPropertyFight.SetParamMulConsumeMp
SetParamFunctions[EnumMonsterFightProp.AdjAgi2] = CLuaMonsterPropertyFight.SetParamAdjAgi2
SetParamFunctions[EnumMonsterFightProp.AdjAntiBianHu2] = CLuaMonsterPropertyFight.SetParamAdjAntiBianHu2
SetParamFunctions[EnumMonsterFightProp.AdjAntiBind2] = CLuaMonsterPropertyFight.SetParamAdjAntiBind2
SetParamFunctions[EnumMonsterFightProp.AdjAntiBlind2] = CLuaMonsterPropertyFight.SetParamAdjAntiBlind2
SetParamFunctions[EnumMonsterFightProp.AdjAntiBlock2] = CLuaMonsterPropertyFight.SetParamAdjAntiBlock2
SetParamFunctions[EnumMonsterFightProp.AdjAntiBlockDamage2] = CLuaMonsterPropertyFight.SetParamAdjAntiBlockDamage2
SetParamFunctions[EnumMonsterFightProp.AdjAntiChaos2] = CLuaMonsterPropertyFight.SetParamAdjAntiChaos2
SetParamFunctions[EnumMonsterFightProp.AdjAntiDecelerate2] = CLuaMonsterPropertyFight.SetParamAdjAntiDecelerate2
SetParamFunctions[EnumMonsterFightProp.AdjAntiDizzy2] = CLuaMonsterPropertyFight.SetParamAdjAntiDizzy2
SetParamFunctions[EnumMonsterFightProp.AdjAntiFire2] = CLuaMonsterPropertyFight.SetParamAdjAntiFire2
SetParamFunctions[EnumMonsterFightProp.AdjAntiFreeze2] = CLuaMonsterPropertyFight.SetParamAdjAntiFreeze2
SetParamFunctions[EnumMonsterFightProp.AdjAntiIce2] = CLuaMonsterPropertyFight.SetParamAdjAntiIce2
SetParamFunctions[EnumMonsterFightProp.AdjAntiIllusion2] = CLuaMonsterPropertyFight.SetParamAdjAntiIllusion2
SetParamFunctions[EnumMonsterFightProp.AdjAntiLight2] = CLuaMonsterPropertyFight.SetParamAdjAntiLight2
SetParamFunctions[EnumMonsterFightProp.AdjAntimFatal2] = CLuaMonsterPropertyFight.SetParamAdjAntimFatal2
SetParamFunctions[EnumMonsterFightProp.AdjAntimFatalDamage2] = CLuaMonsterPropertyFight.SetParamAdjAntimFatalDamage2
SetParamFunctions[EnumMonsterFightProp.AdjAntiPetrify2] = CLuaMonsterPropertyFight.SetParamAdjAntiPetrify2
SetParamFunctions[EnumMonsterFightProp.AdjAntipFatal2] = CLuaMonsterPropertyFight.SetParamAdjAntipFatal2
SetParamFunctions[EnumMonsterFightProp.AdjAntipFatalDamage2] = CLuaMonsterPropertyFight.SetParamAdjAntipFatalDamage2
SetParamFunctions[EnumMonsterFightProp.AdjAntiPoison2] = CLuaMonsterPropertyFight.SetParamAdjAntiPoison2
SetParamFunctions[EnumMonsterFightProp.AdjAntiSilence2] = CLuaMonsterPropertyFight.SetParamAdjAntiSilence2
SetParamFunctions[EnumMonsterFightProp.AdjAntiSleep2] = CLuaMonsterPropertyFight.SetParamAdjAntiSleep2
SetParamFunctions[EnumMonsterFightProp.AdjAntiThunder2] = CLuaMonsterPropertyFight.SetParamAdjAntiThunder2
SetParamFunctions[EnumMonsterFightProp.AdjAntiTie2] = CLuaMonsterPropertyFight.SetParamAdjAntiTie2
SetParamFunctions[EnumMonsterFightProp.AdjAntiWater2] = CLuaMonsterPropertyFight.SetParamAdjAntiWater2
SetParamFunctions[EnumMonsterFightProp.AdjAntiWind2] = CLuaMonsterPropertyFight.SetParamAdjAntiWind2
SetParamFunctions[EnumMonsterFightProp.AdjBlock2] = CLuaMonsterPropertyFight.SetParamAdjBlock2
SetParamFunctions[EnumMonsterFightProp.AdjBlockDamage2] = CLuaMonsterPropertyFight.SetParamAdjBlockDamage2
SetParamFunctions[EnumMonsterFightProp.AdjCor2] = CLuaMonsterPropertyFight.SetParamAdjCor2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceBianHu2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceBianHu2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceBind2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceBind2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceChaos2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceChaos2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceDizzy2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceDizzy2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceFire2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceFire2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceFreeze2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceFreeze2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceIce2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceIce2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceIllusion2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceIllusion2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceLight2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceLight2
SetParamFunctions[EnumMonsterFightProp.AdjEnhancePetrify2] = CLuaMonsterPropertyFight.SetParamAdjEnhancePetrify2
SetParamFunctions[EnumMonsterFightProp.AdjEnhancePoison2] = CLuaMonsterPropertyFight.SetParamAdjEnhancePoison2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceSilence2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceSilence2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceSleep2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceSleep2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceThunder2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceThunder2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceTie2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceTie2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceWater2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceWater2
SetParamFunctions[EnumMonsterFightProp.AdjEnhanceWind2] = CLuaMonsterPropertyFight.SetParamAdjEnhanceWind2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiFire2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiFire2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiIce2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIce2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiIllusion2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiIllusion2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiLight2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiLight2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiPoison2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiPoison2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiThunder2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiThunder2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiWater2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWater2
SetParamFunctions[EnumMonsterFightProp.AdjIgnoreAntiWind2] = CLuaMonsterPropertyFight.SetParamAdjIgnoreAntiWind2
SetParamFunctions[EnumMonsterFightProp.AdjInt2] = CLuaMonsterPropertyFight.SetParamAdjInt2
SetParamFunctions[EnumMonsterFightProp.AdjmDef2] = CLuaMonsterPropertyFight.SetParamAdjmDef2
SetParamFunctions[EnumMonsterFightProp.AdjmFatal2] = CLuaMonsterPropertyFight.SetParamAdjmFatal2
SetParamFunctions[EnumMonsterFightProp.AdjmFatalDamage2] = CLuaMonsterPropertyFight.SetParamAdjmFatalDamage2
SetParamFunctions[EnumMonsterFightProp.AdjmHit2] = CLuaMonsterPropertyFight.SetParamAdjmHit2
SetParamFunctions[EnumMonsterFightProp.AdjmHurt2] = CLuaMonsterPropertyFight.SetParamAdjmHurt2
SetParamFunctions[EnumMonsterFightProp.AdjmMiss2] = CLuaMonsterPropertyFight.SetParamAdjmMiss2
SetParamFunctions[EnumMonsterFightProp.AdjmSpeed2] = CLuaMonsterPropertyFight.SetParamAdjmSpeed2
SetParamFunctions[EnumMonsterFightProp.AdjpDef2] = CLuaMonsterPropertyFight.SetParamAdjpDef2
SetParamFunctions[EnumMonsterFightProp.AdjpFatal2] = CLuaMonsterPropertyFight.SetParamAdjpFatal2
SetParamFunctions[EnumMonsterFightProp.AdjpFatalDamage2] = CLuaMonsterPropertyFight.SetParamAdjpFatalDamage2
SetParamFunctions[EnumMonsterFightProp.AdjpHit2] = CLuaMonsterPropertyFight.SetParamAdjpHit2
SetParamFunctions[EnumMonsterFightProp.AdjpHurt2] = CLuaMonsterPropertyFight.SetParamAdjpHurt2
SetParamFunctions[EnumMonsterFightProp.AdjpMiss2] = CLuaMonsterPropertyFight.SetParamAdjpMiss2
SetParamFunctions[EnumMonsterFightProp.AdjpSpeed2] = CLuaMonsterPropertyFight.SetParamAdjpSpeed2
SetParamFunctions[EnumMonsterFightProp.AdjStr2] = CLuaMonsterPropertyFight.SetParamAdjStr2
SetParamFunctions[EnumMonsterFightProp.LSpAttMin] = CLuaMonsterPropertyFight.SetParamLSpAttMin
SetParamFunctions[EnumMonsterFightProp.LSpAttMax] = CLuaMonsterPropertyFight.SetParamLSpAttMax
SetParamFunctions[EnumMonsterFightProp.LSmAttMin] = CLuaMonsterPropertyFight.SetParamLSmAttMin
SetParamFunctions[EnumMonsterFightProp.LSmAttMax] = CLuaMonsterPropertyFight.SetParamLSmAttMax
SetParamFunctions[EnumMonsterFightProp.LSpFatal] = CLuaMonsterPropertyFight.SetParamLSpFatal
SetParamFunctions[EnumMonsterFightProp.LSpFatalDamage] = CLuaMonsterPropertyFight.SetParamLSpFatalDamage
SetParamFunctions[EnumMonsterFightProp.LSmFatal] = CLuaMonsterPropertyFight.SetParamLSmFatal
SetParamFunctions[EnumMonsterFightProp.LSmFatalDamage] = CLuaMonsterPropertyFight.SetParamLSmFatalDamage
SetParamFunctions[EnumMonsterFightProp.LSAntiBlock] = CLuaMonsterPropertyFight.SetParamLSAntiBlock
SetParamFunctions[EnumMonsterFightProp.LSAntiBlockDamage] = CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiFire] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiThunder] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIce] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiPoison] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWind] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiLight] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIllusion] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWater] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater
SetParamFunctions[EnumMonsterFightProp.LSpAttMin_adj] = CLuaMonsterPropertyFight.SetParamLSpAttMin_adj
SetParamFunctions[EnumMonsterFightProp.LSpAttMax_adj] = CLuaMonsterPropertyFight.SetParamLSpAttMax_adj
SetParamFunctions[EnumMonsterFightProp.LSmAttMin_adj] = CLuaMonsterPropertyFight.SetParamLSmAttMin_adj
SetParamFunctions[EnumMonsterFightProp.LSmAttMax_adj] = CLuaMonsterPropertyFight.SetParamLSmAttMax_adj
SetParamFunctions[EnumMonsterFightProp.LSpFatal_adj] = CLuaMonsterPropertyFight.SetParamLSpFatal_adj
SetParamFunctions[EnumMonsterFightProp.LSpFatalDamage_adj] = CLuaMonsterPropertyFight.SetParamLSpFatalDamage_adj
SetParamFunctions[EnumMonsterFightProp.LSmFatal_adj] = CLuaMonsterPropertyFight.SetParamLSmFatal_adj
SetParamFunctions[EnumMonsterFightProp.LSmFatalDamage_adj] = CLuaMonsterPropertyFight.SetParamLSmFatalDamage_adj
SetParamFunctions[EnumMonsterFightProp.LSAntiBlock_adj] = CLuaMonsterPropertyFight.SetParamLSAntiBlock_adj
SetParamFunctions[EnumMonsterFightProp.LSAntiBlockDamage_adj] = CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiFire_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiThunder_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIce_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiPoison_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWind_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiLight_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIllusion_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_adj
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWater_adj] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_adj
SetParamFunctions[EnumMonsterFightProp.LSpAttMin_jc] = CLuaMonsterPropertyFight.SetParamLSpAttMin_jc
SetParamFunctions[EnumMonsterFightProp.LSpAttMax_jc] = CLuaMonsterPropertyFight.SetParamLSpAttMax_jc
SetParamFunctions[EnumMonsterFightProp.LSmAttMin_jc] = CLuaMonsterPropertyFight.SetParamLSmAttMin_jc
SetParamFunctions[EnumMonsterFightProp.LSmAttMax_jc] = CLuaMonsterPropertyFight.SetParamLSmAttMax_jc
SetParamFunctions[EnumMonsterFightProp.LSpFatal_jc] = CLuaMonsterPropertyFight.SetParamLSpFatal_jc
SetParamFunctions[EnumMonsterFightProp.LSpFatalDamage_jc] = CLuaMonsterPropertyFight.SetParamLSpFatalDamage_jc
SetParamFunctions[EnumMonsterFightProp.LSmFatal_jc] = CLuaMonsterPropertyFight.SetParamLSmFatal_jc
SetParamFunctions[EnumMonsterFightProp.LSmFatalDamage_jc] = CLuaMonsterPropertyFight.SetParamLSmFatalDamage_jc
SetParamFunctions[EnumMonsterFightProp.LSAntiBlock_jc] = CLuaMonsterPropertyFight.SetParamLSAntiBlock_jc
SetParamFunctions[EnumMonsterFightProp.LSAntiBlockDamage_jc] = CLuaMonsterPropertyFight.SetParamLSAntiBlockDamage_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiFire_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiThunder_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIce_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiPoison_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWind_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiLight_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIllusion_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWater_jc] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_jc
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiFire_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiFire_mul
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiThunder_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiThunder_mul
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIce_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIce_mul
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiPoison_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiPoison_mul
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWind_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWind_mul
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiLight_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiLight_mul
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiIllusion_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiIllusion_mul
SetParamFunctions[EnumMonsterFightProp.LSIgnoreAntiWater_mul] = CLuaMonsterPropertyFight.SetParamLSIgnoreAntiWater_mul
SetParamFunctions[EnumMonsterFightProp.JieBan_Child_QiChangColor] = CLuaMonsterPropertyFight.SetParamJieBan_Child_QiChangColor
SetParamFunctions[EnumMonsterFightProp.JieBan_LingShou_Ratio] = CLuaMonsterPropertyFight.SetParamJieBan_LingShou_Ratio
SetParamFunctions[EnumMonsterFightProp.JieBan_LingShou_QinMi] = CLuaMonsterPropertyFight.SetParamJieBan_LingShou_QinMi
SetParamFunctions[EnumMonsterFightProp.JieBan_LingShou_CorZizhi] = CLuaMonsterPropertyFight.SetParamJieBan_LingShou_CorZizhi
SetParamFunctions[EnumMonsterFightProp.JieBan_LingShou_StaZizhi] = CLuaMonsterPropertyFight.SetParamJieBan_LingShou_StaZizhi
SetParamFunctions[EnumMonsterFightProp.JieBan_LingShou_StrZizhi] = CLuaMonsterPropertyFight.SetParamJieBan_LingShou_StrZizhi
SetParamFunctions[EnumMonsterFightProp.JieBan_LingShou_IntZizhi] = CLuaMonsterPropertyFight.SetParamJieBan_LingShou_IntZizhi
SetParamFunctions[EnumMonsterFightProp.JieBan_LingShou_AgiZizhi] = CLuaMonsterPropertyFight.SetParamJieBan_LingShou_AgiZizhi
SetParamFunctions[EnumMonsterFightProp.Buff_DisplayValue1] = CLuaMonsterPropertyFight.SetParamBuff_DisplayValue1
SetParamFunctions[EnumMonsterFightProp.Buff_DisplayValue2] = CLuaMonsterPropertyFight.SetParamBuff_DisplayValue2
SetParamFunctions[EnumMonsterFightProp.Buff_DisplayValue3] = CLuaMonsterPropertyFight.SetParamBuff_DisplayValue3
SetParamFunctions[EnumMonsterFightProp.Buff_DisplayValue4] = CLuaMonsterPropertyFight.SetParamBuff_DisplayValue4
SetParamFunctions[EnumMonsterFightProp.Buff_DisplayValue5] = CLuaMonsterPropertyFight.SetParamBuff_DisplayValue5
SetParamFunctions[EnumMonsterFightProp.LSNature] = CLuaMonsterPropertyFight.SetParamLSNature
SetParamFunctions[EnumMonsterFightProp.PrayMulti] = CLuaMonsterPropertyFight.SetParamPrayMulti
SetParamFunctions[EnumMonsterFightProp.EnhanceYingLing] = CLuaMonsterPropertyFight.SetParamEnhanceYingLing
SetParamFunctions[EnumMonsterFightProp.EnhanceYingLingMul] = CLuaMonsterPropertyFight.SetParamEnhanceYingLingMul
SetParamFunctions[EnumMonsterFightProp.EnhanceFlag] = CLuaMonsterPropertyFight.SetParamEnhanceFlag
SetParamFunctions[EnumMonsterFightProp.EnhanceFlagMul] = CLuaMonsterPropertyFight.SetParamEnhanceFlagMul
SetParamFunctions[EnumMonsterFightProp.ObjectType] = CLuaMonsterPropertyFight.SetParamObjectType
SetParamFunctions[EnumMonsterFightProp.MonsterType] = CLuaMonsterPropertyFight.SetParamMonsterType
SetParamFunctions[EnumMonsterFightProp.FightPropType] = CLuaMonsterPropertyFight.SetParamFightPropType
SetParamFunctions[EnumMonsterFightProp.PlayHpFull] = CLuaMonsterPropertyFight.SetParamPlayHpFull
SetParamFunctions[EnumMonsterFightProp.PlayHp] = CLuaMonsterPropertyFight.SetParamPlayHp
SetParamFunctions[EnumMonsterFightProp.PlayAtt] = CLuaMonsterPropertyFight.SetParamPlayAtt
SetParamFunctions[EnumMonsterFightProp.PlayDef] = CLuaMonsterPropertyFight.SetParamPlayDef
SetParamFunctions[EnumMonsterFightProp.PlayHit] = CLuaMonsterPropertyFight.SetParamPlayHit
SetParamFunctions[EnumMonsterFightProp.PlayMiss] = CLuaMonsterPropertyFight.SetParamPlayMiss
SetParamFunctions[EnumMonsterFightProp.EnhanceDieKe] = CLuaMonsterPropertyFight.SetParamEnhanceDieKe
SetParamFunctions[EnumMonsterFightProp.EnhanceDieKeMul] = CLuaMonsterPropertyFight.SetParamEnhanceDieKeMul
SetParamFunctions[EnumMonsterFightProp.DotRemain] = CLuaMonsterPropertyFight.SetParamDotRemain
SetParamFunctions[EnumMonsterFightProp.IsConfused] = CLuaMonsterPropertyFight.SetParamIsConfused
SetParamFunctions[EnumMonsterFightProp.EnhanceSheShouKefu] = CLuaMonsterPropertyFight.SetParamEnhanceSheShouKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceJiaShiKefu] = CLuaMonsterPropertyFight.SetParamEnhanceJiaShiKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceDaoKeKefu] = CLuaMonsterPropertyFight.SetParamEnhanceDaoKeKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceXiaKeKefu] = CLuaMonsterPropertyFight.SetParamEnhanceXiaKeKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceFangShiKefu] = CLuaMonsterPropertyFight.SetParamEnhanceFangShiKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceYiShiKefu] = CLuaMonsterPropertyFight.SetParamEnhanceYiShiKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceMeiZheKefu] = CLuaMonsterPropertyFight.SetParamEnhanceMeiZheKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceYiRenKefu] = CLuaMonsterPropertyFight.SetParamEnhanceYiRenKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceYanShiKefu] = CLuaMonsterPropertyFight.SetParamEnhanceYanShiKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceHuaHunKefu] = CLuaMonsterPropertyFight.SetParamEnhanceHuaHunKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceYingLingKefu] = CLuaMonsterPropertyFight.SetParamEnhanceYingLingKefu
SetParamFunctions[EnumMonsterFightProp.EnhanceDieKeKefu] = CLuaMonsterPropertyFight.SetParamEnhanceDieKeKefu
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentCor] = CLuaMonsterPropertyFight.SetParamAdjustPermanentCor
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentSta] = CLuaMonsterPropertyFight.SetParamAdjustPermanentSta
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentStr] = CLuaMonsterPropertyFight.SetParamAdjustPermanentStr
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentInt] = CLuaMonsterPropertyFight.SetParamAdjustPermanentInt
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentAgi] = CLuaMonsterPropertyFight.SetParamAdjustPermanentAgi
SetParamFunctions[EnumMonsterFightProp.SoulCoreLevel] = CLuaMonsterPropertyFight.SetParamSoulCoreLevel
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentMidCor] = CLuaMonsterPropertyFight.SetParamAdjustPermanentMidCor
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentMidSta] = CLuaMonsterPropertyFight.SetParamAdjustPermanentMidSta
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentMidStr] = CLuaMonsterPropertyFight.SetParamAdjustPermanentMidStr
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentMidInt] = CLuaMonsterPropertyFight.SetParamAdjustPermanentMidInt
SetParamFunctions[EnumMonsterFightProp.AdjustPermanentMidAgi] = CLuaMonsterPropertyFight.SetParamAdjustPermanentMidAgi
SetParamFunctions[EnumMonsterFightProp.SumPermanent] = CLuaMonsterPropertyFight.SetParamSumPermanent
SetParamFunctions[EnumMonsterFightProp.AntiTian] = CLuaMonsterPropertyFight.SetParamAntiTian
SetParamFunctions[EnumMonsterFightProp.AntiEGui] = CLuaMonsterPropertyFight.SetParamAntiEGui
SetParamFunctions[EnumMonsterFightProp.AntiXiuLuo] = CLuaMonsterPropertyFight.SetParamAntiXiuLuo
SetParamFunctions[EnumMonsterFightProp.AntiDiYu] = CLuaMonsterPropertyFight.SetParamAntiDiYu
SetParamFunctions[EnumMonsterFightProp.AntiRen] = CLuaMonsterPropertyFight.SetParamAntiRen
SetParamFunctions[EnumMonsterFightProp.AntiChuSheng] = CLuaMonsterPropertyFight.SetParamAntiChuSheng
SetParamFunctions[EnumMonsterFightProp.AntiBuilding] = CLuaMonsterPropertyFight.SetParamAntiBuilding
SetParamFunctions[EnumMonsterFightProp.AntiZhaoHuan] = CLuaMonsterPropertyFight.SetParamAntiZhaoHuan
SetParamFunctions[EnumMonsterFightProp.AntiLingShou] = CLuaMonsterPropertyFight.SetParamAntiLingShou
SetParamFunctions[EnumMonsterFightProp.AntiBoss] = CLuaMonsterPropertyFight.SetParamAntiBoss
SetParamFunctions[EnumMonsterFightProp.AntiSheShou] = CLuaMonsterPropertyFight.SetParamAntiSheShou
SetParamFunctions[EnumMonsterFightProp.AntiJiaShi] = CLuaMonsterPropertyFight.SetParamAntiJiaShi
SetParamFunctions[EnumMonsterFightProp.AntiDaoKe] = CLuaMonsterPropertyFight.SetParamAntiDaoKe
SetParamFunctions[EnumMonsterFightProp.AntiXiaKe] = CLuaMonsterPropertyFight.SetParamAntiXiaKe
SetParamFunctions[EnumMonsterFightProp.AntiFangShi] = CLuaMonsterPropertyFight.SetParamAntiFangShi
SetParamFunctions[EnumMonsterFightProp.AntiYiShi] = CLuaMonsterPropertyFight.SetParamAntiYiShi
SetParamFunctions[EnumMonsterFightProp.AntiMeiZhe] = CLuaMonsterPropertyFight.SetParamAntiMeiZhe
SetParamFunctions[EnumMonsterFightProp.AntiYiRen] = CLuaMonsterPropertyFight.SetParamAntiYiRen
SetParamFunctions[EnumMonsterFightProp.AntiYanShi] = CLuaMonsterPropertyFight.SetParamAntiYanShi
SetParamFunctions[EnumMonsterFightProp.AntiHuaHun] = CLuaMonsterPropertyFight.SetParamAntiHuaHun
SetParamFunctions[EnumMonsterFightProp.AntiYingLing] = CLuaMonsterPropertyFight.SetParamAntiYingLing
SetParamFunctions[EnumMonsterFightProp.AntiDieKe] = CLuaMonsterPropertyFight.SetParamAntiDieKe
SetParamFunctions[EnumMonsterFightProp.AntiFlag] = CLuaMonsterPropertyFight.SetParamAntiFlag
SetParamFunctions[EnumMonsterFightProp.EnhanceZhanKuang] = CLuaMonsterPropertyFight.SetParamEnhanceZhanKuang
SetParamFunctions[EnumMonsterFightProp.EnhanceZhanKuangMul] = CLuaMonsterPropertyFight.SetParamEnhanceZhanKuangMul
SetParamFunctions[EnumMonsterFightProp.EnhanceZhanKuangKefu] = CLuaMonsterPropertyFight.SetParamEnhanceZhanKuangKefu
SetParamFunctions[EnumMonsterFightProp.AntiZhanKuang] = CLuaMonsterPropertyFight.SetParamAntiZhanKuang
SetParamFunctions[EnumMonsterFightProp.JumpSpeed] = CLuaMonsterPropertyFight.SetParamJumpSpeed
SetParamFunctions[EnumMonsterFightProp.OriJumpSpeed] = CLuaMonsterPropertyFight.SetParamOriJumpSpeed
SetParamFunctions[EnumMonsterFightProp.AdjJumpSpeed] = CLuaMonsterPropertyFight.SetParamAdjJumpSpeed
SetParamFunctions[EnumMonsterFightProp.GravityAcceleration] = CLuaMonsterPropertyFight.SetParamGravityAcceleration
SetParamFunctions[EnumMonsterFightProp.OriGravityAcceleration] = CLuaMonsterPropertyFight.SetParamOriGravityAcceleration
SetParamFunctions[EnumMonsterFightProp.AdjGravityAcceleration] = CLuaMonsterPropertyFight.SetParamAdjGravityAcceleration
SetParamFunctions[EnumMonsterFightProp.HorizontalAcceleration] = CLuaMonsterPropertyFight.SetParamHorizontalAcceleration
SetParamFunctions[EnumMonsterFightProp.OriHorizontalAcceleration] = CLuaMonsterPropertyFight.SetParamOriHorizontalAcceleration
SetParamFunctions[EnumMonsterFightProp.AdjHorizontalAcceleration] = CLuaMonsterPropertyFight.SetParamAdjHorizontalAcceleration
SetParamFunctions[EnumMonsterFightProp.SwimSpeed] = CLuaMonsterPropertyFight.SetParamSwimSpeed
SetParamFunctions[EnumMonsterFightProp.OriSwimSpeed] = CLuaMonsterPropertyFight.SetParamOriSwimSpeed
SetParamFunctions[EnumMonsterFightProp.AdjSwimSpeed] = CLuaMonsterPropertyFight.SetParamAdjSwimSpeed
SetParamFunctions[EnumMonsterFightProp.StrengthPoint] = CLuaMonsterPropertyFight.SetParamStrengthPoint
SetParamFunctions[EnumMonsterFightProp.StrengthPointMax] = CLuaMonsterPropertyFight.SetParamStrengthPointMax
SetParamFunctions[EnumMonsterFightProp.OriStrengthPointMax] = CLuaMonsterPropertyFight.SetParamOriStrengthPointMax
SetParamFunctions[EnumMonsterFightProp.AdjStrengthPointMax] = CLuaMonsterPropertyFight.SetParamAdjStrengthPointMax
SetParamFunctions[EnumMonsterFightProp.AntiMulEnhanceIllusion] = CLuaMonsterPropertyFight.SetParamAntiMulEnhanceIllusion
SetParamFunctions[EnumMonsterFightProp.GlideHorizontalSpeedWithUmbrella] = CLuaMonsterPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella
SetParamFunctions[EnumMonsterFightProp.GlideVerticalSpeedWithUmbrella] = CLuaMonsterPropertyFight.SetParamGlideVerticalSpeedWithUmbrella
SetParamFunctions[EnumMonsterFightProp.AdjpSpeed] = CLuaMonsterPropertyFight.SetParamAdjpSpeed
SetParamFunctions[EnumMonsterFightProp.AdjmSpeed] = CLuaMonsterPropertyFight.SetParamAdjmSpeed

