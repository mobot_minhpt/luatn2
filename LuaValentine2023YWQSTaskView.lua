local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaValentine2023YWQSTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

function LuaValentine2023YWQSTaskView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.transform:Find("Desc"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("VALENTINE2023_YIWANGQINGSHEN_TASK_DESC")
    self.transform:Find("Bg"):GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()

    UIEventListener.Get(self.transform:Find("RankButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

    local leaveButton = self.transform:Find("LeaveButton"):GetComponent(typeof(UIWidget))
    leaveButton:ResetAndUpdateAnchors()
    UIEventListener.Get(leaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)
end

function LuaValentine2023YWQSTaskView:Init()

end

--@region UIEvent

function LuaValentine2023YWQSTaskView:OnRankButtonClick()
    local endTimeStamp = CServerTimeMgr.Inst:GetTimeStampByStr("2025-02-20 00:00")
    if CServerTimeMgr.Inst.timeStamp > endTimeStamp then
        g_MessageMgr:ShowMessage("YIWANGQINGSHEN_END")
        return
    end

    CUIManager.ShowUI(CLuaUIResources.Valentine2023YWQSRankWnd)
end

function LuaValentine2023YWQSTaskView:OnLeaveButtonClick()
    Gac2Gas.LevelTeleport(16000003)
end

--@endregion UIEvent
