local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local CUIPickerWndMgr = import "L10.UI.CUIPickerWndMgr"
local CTooltip = import "L10.UI.CTooltip"

CLuaPickerWnd = class()
RegistClassMember(CLuaPickerWnd,"anchorNode")
RegistClassMember(CLuaPickerWnd,"scrollview")
RegistClassMember(CLuaPickerWnd,"table")
RegistClassMember(CLuaPickerWnd,"itemCellTpl")
RegistClassMember(CLuaPickerWnd,"center")
RegistClassMember(CLuaPickerWnd,"itemList")
RegistClassMember(CLuaPickerWnd,"panel")
RegistClassMember(CLuaPickerWnd,"background")
RegistClassMember(CLuaPickerWnd,"originLocalPosition")

function CLuaPickerWnd:Awake()
    self.anchorNode = self.transform:Find("Anchor")
    self.scrollview = self.transform:Find("Anchor/Offset/SelectList/Scrollview"):GetComponent(typeof(UIScrollView))
    self.table = self.transform:Find("Anchor/Offset/SelectList/Scrollview/Table"):GetComponent(typeof(UITable))
    self.itemCellTpl = self.transform:Find("Anchor/Offset/SelectList/Scrollview/ItemCell").gameObject
    self.center = self.transform:Find("Anchor/Offset/SelectList/Scrollview/Table"):GetComponent(typeof(L10.UI.CUICenterOnChild))
    self.itemList = {}
    self.panel = self.transform:Find("Anchor/Offset/SelectList/Scrollview"):GetComponent(typeof(UIPanel))
    self.background = self.transform:Find("Anchor/Offset/Bg"):GetComponent(typeof(UISprite))
    self.originLocalPosition = {}

end

function CLuaPickerWnd:Init( )
    self.itemList = {}
    Extensions.RemoveAllChildren(self.table.transform)
    self.itemCellTpl:SetActive(false)

    local localPos = self.anchorNode.parent:InverseTransformPoint(CUIPickerWndMgr.topAnchorPosition)
    repeat
        local default = CUIPickerWndMgr.alignType
        if default == CTooltip.AlignType.Bottom then
            self.anchorNode.localPosition = localPos
            break
        elseif default == CTooltip.AlignType.Right then
            self.anchorNode.localPosition = CreateFromClass(Vector3, localPos.x + self.background.width, localPos.y + self.background.height * 0.5, 1)
            break
        else
            self.anchorNode.localPosition = localPos
            break
        end
    until 1


    if CUIPickerWndMgr.contents == nil or CUIPickerWndMgr.contents.Count == 0 then
        return
    end

    do
        local i = 0
        while i < CUIPickerWndMgr.contents.Count do
            local itemcell = NGUITools.AddChild(self.table.gameObject, self.itemCellTpl)
            table.insert( self.itemList,itemcell )
            itemcell:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(itemcell, typeof(UILabel)).text = CUIPickerWndMgr.contents[i]
            UIEventListener.Get(itemcell).onClick = DelegateFactory.VoidDelegate(function(go)
                self:ClickCallbck(go)
            end)
            i = i + 1
        end
    end
    self.table:Reposition()
    self:OnReposition()
end

function CLuaPickerWnd:ClickCallbck( go) 
    for i=1,self.table.transform.childCount do
        local t = self.table.transform:GetChild(i-1).gameObject
        if go==t then
            -- 点击选中的就关闭
            if i -1 == CUIPickerWndMgr.selectedIndex then
                CUIManager.CloseUI(CUIResources.PickerWnd)
            end
            local panelTrans = self.scrollview.panel.cachedTransform
            local op = self.originLocalPosition[i]
            local cp =panelTrans:InverseTransformPoint(self.table.transform:TransformPoint(op))
            self.center:CenterOn(go.transform, cp)
            break
        end
    end
end
function CLuaPickerWnd:Start( )
    if #self.itemList > 0 then
        self.center.onCenter = LuaUtils.OnCenterCallback(function(go)
            self:OnCenter(go)
        end)
        CUIPickerWndMgr.selectedIndex = math.min(math.max(0, CUIPickerWndMgr.selectedIndex), #self.itemList - 1)
        self.center:CenterOnInstant(self.itemList[CUIPickerWndMgr.selectedIndex+1].transform)
        self.panel.onClipMove = LuaUtils.OnClippingMoved(function(panel)
            self:OnMove(panel)
        end)
        self:OnMove(self.panel)
    end
end
function CLuaPickerWnd:OnReposition( )
    self.originLocalPosition = {}
    for i=1,self.table.transform.childCount do
        local t = self.table.transform:GetChild(i-1)
        table.insert( self.originLocalPosition, t.localPosition )
    end
end
function CLuaPickerWnd:OnMove( panel) 
    local corners = panel.worldCorners
    local c2=corners[2]
    local c0=corners[0]
    local panelCenter = Vector3((c2.x+c0.x)/2,(c2.y+c0.y)/2,0)
    local localCenter = self.table.transform:InverseTransformPoint(panelCenter)
    local localHeight = math.abs(self.table.transform:InverseTransformPoint(corners[2]).y - self.table.transform:InverseTransformPoint(corners[0]).y)
    for i=1,self.table.transform.childCount do
        local t = self.table.transform:GetChild(i-1)
        local coeff = math.abs(self.originLocalPosition[i].y - localCenter.y) / localHeight
        local scale = math.max(1 - coeff * coeff, 0.6)
        t.localScale = Vector3(scale, scale, 1)

        t:GetComponent(typeof(UILabel)).alpha = 1 - coeff
        local v = 1 - coeff * 0.5
        v = math.max(v,0.6)
        t.localPosition =  Vector3(0,localCenter.y + (self.originLocalPosition[i].y - localCenter.y) * v,0)
    end
end
function CLuaPickerWnd:OnCenter( go) 
    for i,v in ipairs(self.itemList) do
        if go==v then
            CUIPickerWndMgr.selectedIndex = i-1
            v:GetComponent(typeof(UILabel)).fontSize = 40
        else
            v:GetComponent(typeof(UILabel)).fontSize = 34
        end
    end
end

function CLuaPickerWnd:OnDestroy()
    EventManager.BroadcastInternalForLua(EnumEventType.OnPickerItemSelected, {CUIPickerWndMgr.selectedIndex})
end

