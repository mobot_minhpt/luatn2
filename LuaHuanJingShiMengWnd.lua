local DelegateFactory = import "DelegateFactory"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaHuanJingShiMengWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaHuanJingShiMengWnd, "grid")
RegistClassMember(LuaHuanJingShiMengWnd, "topLabel")
RegistClassMember(LuaHuanJingShiMengWnd, "template")

RegistClassMember(LuaHuanJingShiMengWnd, "tick")
RegistClassMember(LuaHuanJingShiMengWnd, "endTimeStamp")

function LuaHuanJingShiMengWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitActive()
end

-- 初始化UI组件
function LuaHuanJingShiMengWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.topLabel = anchor:Find("TopLabel"):GetComponent(typeof(UILabel))
    self.grid = anchor:Find("Grid"):GetComponent(typeof(UIGrid))
    self.template = anchor:Find("Template").gameObject
end

function LuaHuanJingShiMengWnd:InitActive()
    self.template:SetActive(false)
end


function LuaHuanJingShiMengWnd:Init()
    self:InitGrid()
    self:StartTick()
end

-- 初始化各个增益
function LuaHuanJingShiMengWnd:InitGrid()
    for k, id in pairs(LuaQingMing2022Mgr.HJSMGainTbl) do
        local child = CUICommonDef.AddChild(self.grid.gameObject, self.template)
        child:SetActive(true)

        local buffType = QingMing2022_SingleBuff.GetData(id).BuffType
        local buffId = QingMing2022_SingleBuff.GetData(id).BuffId
        local desc = QingMing2022_SingleBuff.GetData(id).Desc

        local up = child.transform:Find("Up")
        local name = up:Find("Name"):GetComponent(typeof(UILabel))
        up:Find("Passive").gameObject:SetActive(buffType == 1)
        up:Find("Active").gameObject:SetActive(buffType == 2)

        local icon = child.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
        child.transform:Find("Detail"):GetComponent(typeof(UILabel)).text = desc

        if buffType == 1 then
            local buff = Buff_Buff.GetData(buffId)
            icon:LoadMaterial(buff.Icon)
            name.text = buff.Name
        elseif buffType == 2 then
            local skill = Skill_AllSkills.GetData(buffId)
            icon:LoadSkillIcon(skill.SkillIcon)
            name.text = skill.Name
        end

        self:UpCentered(up)

        local selectButton = child.transform:Find("SelectButton").gameObject
        UIEventListener.Get(selectButton).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnSelectButtonClick(id)
        end)
    end

    self.grid:Reposition()
end

-- 主被动标识与技能名文本居中对齐
function LuaHuanJingShiMengWnd:UpCentered(up)
    local minPosX, maxPosX = 1000, -1000
    for i = 1, up.childCount do
        if up:GetChild(i - 1).gameObject.active then
            local widget = up:GetChild(i - 1):GetComponent(typeof(UIWidget))
            if widget then
                local left = widget.transform.localPosition.x
                local right = widget.transform.localPosition.x + widget.width
                minPosX = left < minPosX and left or minPosX
                maxPosX = right > maxPosX and right or maxPosX
            end
        end
    end

    Extensions.SetLocalPositionX(up, -1 * (minPosX + maxPosX) / 2)
end

function LuaHuanJingShiMengWnd:StartTick()
    local timeOut = LuaQingMing2022Mgr.HJSMTimeOut
    local countDownDuration = QingMing2022_SingleSetting.GetData().CountDownDuration

    if timeOut > countDownDuration then
        self:StartCountDownAppearTick(timeOut - countDownDuration)
        self.topLabel.text = LocalString.GetString("请从以下增益中任选其一")
    else
        self:StartCountDownTick(timeOut)
    end
end

-- 等待一段时间出现倒计时
function LuaHuanJingShiMengWnd:StartCountDownAppearTick(duration)
    self.tick = RegisterTickOnce(function()
        self:StartCountDownTick(QingMing2022_SingleSetting.GetData().CountDownDuration)
    end, duration * 1000)
end

-- 开始倒计时
function LuaHuanJingShiMengWnd:StartCountDownTick(duration)
    self:ClearTick()

    self.endTimeStamp = CServerTimeMgr.Inst.timeStamp + duration
    self:UpdateTime()
    self.tick = RegisterTick(function()
        self:UpdateTime()
    end, 1000)
end

function LuaHuanJingShiMengWnd:UpdateTime()
    local leftTime = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
    if leftTime <= 0 then
        self:ClearTick()
        local random = math.random(#LuaQingMing2022Mgr.HJSMGainTbl)
        self:OnSelectButtonClick(LuaQingMing2022Mgr.HJSMGainTbl[random])
        return
    end

    self.topLabel.text = SafeStringFormat3(LocalString.GetString("请从以下增益中任选其一(%d)"), leftTime)
end

function LuaHuanJingShiMengWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
    end
end

function LuaHuanJingShiMengWnd:OnDestroy()
    self:ClearTick()
end

--@region UIEvent

function LuaHuanJingShiMengWnd:OnSelectButtonClick(id)
    Gac2Gas.QingMing2022SingleSelectBuff(id)
    CUIManager.CloseUI(CLuaUIResources.HuanJingShiMengWnd)
end

--@endregion UIEvent

