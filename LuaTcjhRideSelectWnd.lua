--@region import
local Object                    = import "System.Object"
local GameObject                = import "UnityEngine.GameObject"
local CItemMgr                  = import "L10.Game.CItemMgr"
local CommonDefs                = import "L10.Game.CommonDefs"
local CUIManager                = import "L10.UI.CUIManager"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local QnTableView               = import "L10.UI.QnTableView"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LuaGameObject             = import "LuaGameObject"
local MsgPackImpl               = import "MsgPackImpl"
--@endregion

--说明
CLuaTcjhRideSelectWnd = class()

--@region Regist

--RegistChildComponent
RegistChildComponent(CLuaTcjhRideSelectWnd, "OKBtn",		GameObject)
RegistChildComponent(CLuaTcjhRideSelectWnd, "NoBtn",		GameObject)
RegistChildComponent(CLuaTcjhRideSelectWnd, "QnTableView1",	QnTableView)
RegistChildComponent(CLuaTcjhRideSelectWnd, "ItemLabel",	UILabel)

--RegistClassMember
RegistClassMember(CLuaTcjhRideSelectWnd,"SelectItemCfg")
RegistClassMember(CLuaTcjhRideSelectWnd, "SelectedItem")

--@endregion
CLuaTcjhRideSelectWnd.ItemID = nil
function CLuaTcjhRideSelectWnd:Awake()
    UIEventListener.Get(self.OKBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOKBtnClick()
    end)
    UIEventListener.Get(self.NoBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.TcjhRideSelectWnd)
    end)
end

function CLuaTcjhRideSelectWnd:Init()
    
    self.SelectItemCfg = nil
    self.SelectedItem = nil;

    local cfgs = self:GetGiftItems()
    if cfgs == nil then return end
    
    local len = #cfgs
    local initfunc = function (item,index)
        local giftcfg = cfgs[index+1]
        self:FillItemCell(item,giftcfg)
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView1:ReloadData(true, true)

    self.ItemLabel.transform.parent.gameObject:SetActive(false)
end

function CLuaTcjhRideSelectWnd:GetGiftItems()
    if CLuaTcjhRideSelectWnd.ItemID == nil then return nil end
    local item = CItemMgr.Inst:GetById(CLuaTcjhRideSelectWnd.ItemID)
    if item == nil then return nil end
    local cfgid = item.TemplateId
    local cfg = Item_Item.GetData(cfgid);
    if cfg == nil then return nil end
    local extradata = cfg.ExtraAttribute
    if System.String.IsNullOrEmpty(extradata) then return nil end
    local splits = Table2ArrayWithCount({";",",",":"}, 3, MakeArrayClass(System.String))
    local op = System.StringSplitOptions.RemoveEmptyEntries
    local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(extradata, splits, op)
    local count = tonumber(strs[1])
    if count ~= 1 then return nil end
    local res = {}
    for i=2,strs.Length-1 do
        local giftcfgid = tonumber(strs[i])
        local giftcfg = Item_Item.GetData(giftcfgid)
        if giftcfg then 
            res[#res+1]=giftcfg
        end
    end
    return res
end


function CLuaTcjhRideSelectWnd:FillItemCell(item,cfg)
    if item == nil then return end

    local cell = item.transform
    local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture

    local itemid = cfg.ID
    if cfg then
        icon:LoadMaterial(cfg.Icon)
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.ScreenLeft, 0, 0, 0, 0)
        self.ItemLabel.transform.parent.gameObject:SetActive(true)
        self.ItemLabel.text = cfg.Name
        self.SelectItemCfg = cfg
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end 
    end)

    item.gameObject:SetActive(true)
end

function CLuaTcjhRideSelectWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function CLuaTcjhRideSelectWnd:OnOKBtnClick()
    if self.SelectItemCfg == nil then
        g_MessageMgr:DisplayMessage("TCJH_RIDE_SELECT_EMPTY")
        return --玩家没有选择
    end
    local msg = g_MessageMgr:FormatMessage("TCJH_RIDE_SELECT_COMFIRE",self.SelectItemCfg.Name)
    local selectid = self.SelectItemCfg.ID
    local okfunc = function()
        local itemid = CLuaTcjhRideSelectWnd.ItemID
        local iteminfo = CItemMgr.Inst:GetItemInfo(itemid)

        local infos = CreateFromClass(MakeGenericClass(List, Object))
        CommonDefs.ListAdd(infos, typeof(Int32), self.SelectItemCfg.ID)

        Gac2Gas.SelectRewardsByUseItem(MsgPackImpl.pack(infos),iteminfo.place,iteminfo.pos,itemid)
        CUIManager.CloseUI(CLuaUIResources.TcjhRideSelectWnd)
    end
    g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
end

function CLuaTcjhRideSelectWnd.OpenUI(itemid)
    CLuaTcjhRideSelectWnd.ItemID = itemid
    CUIManager.ShowUI(CLuaUIResources.TcjhRideSelectWnd)
end

