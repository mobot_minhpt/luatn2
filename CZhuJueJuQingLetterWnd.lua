-- Auto Generated!!
local CCoroutineMgr = import "L10.Engine.CCoroutineMgr"
local CLetterMgr = import "L10.Game.CLetterMgr"
local CZhuJueJuQingLetterWnd = import "L10.UI.CZhuJueJuQingLetterWnd"
local L10 = import "L10"
CZhuJueJuQingLetterWnd.m_Init_CS2LuaHook = function (this) 
    if this.m_Coroutine ~= nil then
        this.uiTexture.material = nil
        CCoroutineMgr.Inst:CancelCoroutine(this.m_Coroutine)
        this.m_Coroutine = nil
    end
    this.m_Coroutine = CCoroutineMgr.Inst:StartCoroutine(this:LoadMaterialAsync(CLetterMgr.Inst.m_TextMat))
end
CZhuJueJuQingLetterWnd.m_OnDestory_CS2LuaHook = function (this) 
    if this.m_Coroutine ~= nil then
        CCoroutineMgr.Inst:CancelCoroutine(this.m_Coroutine)
        this.m_Coroutine = nil
    end
end
CLetterMgr.m_ShowLetter_CS2LuaHook = function (this, textMat, width, height, delayPlayTime, finishedWaitTime, canSkip, autoClose) 
    this.m_TextMat = LocalString.isCN and textMat or ("assets/res_" .. LocalString.language .. "/" .. textMat)
    this.m_FinishedWaitTime = finishedWaitTime
    this.m_CanSkip = canSkip
    this.m_AutoClose = autoClose
    this.m_Width = width
    this.m_Height = height
    this.m_DelayPlayTime = delayPlayTime
    L10.UI.CUIManager.ShowUI(L10.UI.CUIResources.ZhuJueJuQingLetterWnd)
end

