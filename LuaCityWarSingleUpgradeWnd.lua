require("common/common_include")
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local CQMPKTitleTemplate = import "L10.UI.CQMPKTitleTemplate"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CCityWarMgr = import "L10.Game.CCityWarMgr"

CLuaCityWarSingleUpgradeWnd = class()
CLuaCityWarSingleUpgradeWnd.Path = "ui/citywar/LuaCityWarSingleUpgradeWnd"
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_CurrentLevel")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_CurrentName")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_MoneyCtrl")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_ButtonLabel")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_TextLabel")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_CostLabel")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_CostValue")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_TargetLevel")
RegistClassMember(CLuaCityWarSingleUpgradeWnd, "m_GradeSelector")

function CLuaCityWarSingleUpgradeWnd:Init( ... )
	local unitDData = CityWar_Unit.GetData(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId)
	local nextLevelDData = CityWar_Unit.GetData(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId + 1)
	self.m_CurrentLevel = CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId % 100
	self.m_CurrentName = unitDData.Name
	self.m_MoneyCtrl = self.transform:Find("Anchor/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_MoneyCtrl:SetType(15, 0, true)
	self.m_ButtonLabel = self.transform:Find("Anchor/OkButton/Label"):GetComponent(typeof(UILabel))
	self.m_TextLabel = self.transform:Find("Anchor/TextLabel"):GetComponent(typeof(UILabel))
	self.m_CostLabel = self.transform:Find("Anchor/QnCostAndOwnMoney/Cost/Label"):GetComponent(typeof(UILabel))
	self.m_GradeSelector = self.transform:Find("Anchor/Grade"):GetComponent(typeof(CQMPKTitleTemplate))

	local popupMenuItemTable = {}
	local gradeTable = {}
	for i = 1, 5 do
		if i ~= self.m_CurrentLevel then
			table.insert(gradeTable, i)
			table.insert(popupMenuItemTable, PopupMenuItemData(i..LocalString.GetString("级"), DelegateFactory.Action_int(function ( index )
				self:InitLevel(gradeTable[index + 1])
			end), false, nil, EnumPopupMenuItemStyle.Default))
		end
	end
	local popupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	UIEventListener.Get(self.m_GradeSelector.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self.m_GradeSelector:Expand(true)
		CPopupMenuInfoMgr.ShowPopupMenu(popupMenuItemArray, go.transform, AlignType.Right, 1, DelegateFactory.Action(function ( ... )
				self.m_GradeSelector:Expand(false)
			end), 600, true, 296)
	end)

	if nextLevelDData then
		self:InitLevel(self.m_CurrentLevel + 1)
	else
		self:InitLevel(self.m_CurrentLevel - 1)
	end

	UIEventListener.Get(self.transform:Find("Anchor/OkButton").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		if self.m_CostValue > 0 and CLuaCityWarMgr.CurrentMaterial < self.m_CostValue then
			g_MessageMgr:ShowMessage("UPGRADE_CITY_UNIT_MATERIAL_NOT_ENOUGH")
			return
		else
			CCityWarMgr.Inst.CurUnit = nil
			CLuaCityWarMgr.CurrentUpgradeUnit.Moving = false
			Gac2Gas.UpgradeCityUnit(CLuaCityWarMgr.CurrentCityId, CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId, CLuaCityWarMgr.CurrentUpgradeUnit.ID, self.m_TargetLevel)
		end
		CUIManager.CloseUI(CLuaUIResources.CityWarSingleUpgradeWnd)
	end)
end

function CLuaCityWarSingleUpgradeWnd:InitLevel(level)
	self.m_TargetLevel = level
	self.m_GradeSelector:Init(SafeStringFormat3(LocalString.GetString("%s级"), tostring(level)))
	local opTxt = level > self.m_CurrentLevel and LocalString.GetString("升级") or LocalString.GetString("降级")
	self.m_TextLabel.text = SafeStringFormat3(LocalString.GetString("%s此%s至"), opTxt, self.m_CurrentName)
	self.m_ButtonLabel.text = opTxt
	self.m_CostLabel.text = level > self.m_CurrentLevel and LocalString.GetString("消耗") or LocalString.GetString("返还")

	local targetTemplateId = math.floor(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId / 100) * 100 + level
	local targetDData = CityWar_Unit.GetData(targetTemplateId)
	if not targetDData then return end
	self.m_CostValue = targetDData.Material - CityWar_Unit.GetData(CLuaCityWarMgr.CurrentUpgradeUnit.TemplateId).Material
	local cost = math.abs(self.m_CostValue)
	self.m_MoneyCtrl:SetCost(cost)
end