local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local Constants = import "L10.Game.Constants"
local UISprite = import "UISprite"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UITable = import "UITable"
local Profession = import "L10.Game.Profession"

CLuaStarBiwuChampionHistoryWnd = class()
CLuaStarBiwuChampionHistoryWnd.Path = "ui/starbiwu/LuaStarBiwuChampionHistoryWnd"

RegistClassMember(CLuaStarBiwuChampionHistoryWnd, "m_GridView")
RegistClassMember(CLuaStarBiwuChampionHistoryWnd, "m_SelectSprite")
RegistClassMember(CLuaStarBiwuChampionHistoryWnd, "m_SelectIndex")

function CLuaStarBiwuChampionHistoryWnd:Init()
  CLuaStarBiwuMgr.m_HistoryDataTable = {}
  Gac2Gas.QuerySarBiwuChampion(0)

  self.m_GridView = self.transform:Find("Anchor/AdvView"):GetComponent(typeof(QnAdvanceGridView))
  self.m_GridView.m_DataSource = DefaultTableViewDataSource.Create(function()
			return #CLuaStarBiwuMgr.m_HistoryDataTable
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)
end

function CLuaStarBiwuChampionHistoryWnd:InitItem(obj, index)
  local transNameTbl = {"IndexLabel", "TeamNameLabel", "LeaderNameLabel"}
  local indexStr = ""
  local idx = CLuaStarBiwuMgr.m_HistoryDataTable[index]["Index"]
  if idx <= 10 then
    indexStr = EnumChineseDigits.GetDigit(idx)
  else
    indexStr = (math.floor(idx/10) ~= 1 and EnumChineseDigits.GetDigit(idx/10) or "")..LocalString.GetString("十")..(idx%10 ~= 0 and EnumChineseDigits.GetDigit(idx%10) or "")
  end
  indexStr = LocalString.GetString("第")..indexStr..LocalString.GetString("届")
  local valueTbl = {indexStr, CLuaStarBiwuMgr.m_HistoryDataTable[index]["TeamName"], CLuaStarBiwuMgr.m_HistoryDataTable[index]["LeaderName"]}
  for i = 1, #transNameTbl do
    obj.transform:Find(transNameTbl[i]):GetComponent(typeof(UILabel)).text = valueTbl[i]
  end
  local bgSprite = obj.transform:GetComponent(typeof(UISprite))
  bgSprite.spriteName = index % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
  UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function ( ... )
    if self.m_SelectSprite then self.m_SelectSprite.spriteName = self.m_SelectIndex % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite end
    self.m_SelectIndex = index
    self.m_SelectSprite = bgSprite
    bgSprite.spriteName = Constants.ChosenItemBgSpriteName
    CLuaStarBiwuMgr.m_HistoryTeamIndex = index
    CUIManager.ShowUI(CLuaUIResources.StarBiwuChampionTeamWnd)
    end)

  -- Init member class list
  local teamTrans = obj.transform:Find("TeamRoot")
  Extensions.RemoveAllChildren(teamTrans)
  local classObj = obj.transform:Find("SpriteClass").gameObject
  for k, v in ipairs(CLuaStarBiwuMgr.m_HistoryDataTable[index]["TeamTable"]) do
    local memberObj = NGUITools.AddChild(teamTrans.gameObject, classObj)
    memberObj:SetActive(true)
    memberObj:GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(v["Class"])
  end
  teamTrans:GetComponent(typeof(UITable)):Reposition()
end

function CLuaStarBiwuChampionHistoryWnd:SyncStarBiwuChampionEnd()
  table.sort(CLuaStarBiwuMgr.m_HistoryDataTable, function(a, b)
    return a["Index"] > b["Index"]
  end)
  self.m_GridView:ReloadData(true, false)
end

function CLuaStarBiwuChampionHistoryWnd:OnDestroy()
  CLuaStarBiwuMgr.m_HistoryDataTable = {}
end

function CLuaStarBiwuChampionHistoryWnd:OnEnable()
  g_ScriptEvent:AddListener("SyncStarBiwuChampionEnd", self, "SyncStarBiwuChampionEnd")
end

function CLuaStarBiwuChampionHistoryWnd:OnDisable()
  g_ScriptEvent:RemoveListener("SyncStarBiwuChampionEnd", self, "SyncStarBiwuChampionEnd")
end
