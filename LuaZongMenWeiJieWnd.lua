local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local Object = import "System.Object"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local CIMMgr = import "L10.Game.CIMMgr"

LuaZongMenWeiJieWnd = class()

RegistChildComponent(LuaZongMenWeiJieWnd,"m_LeaderPortrait","LeaderPortrait", CUITexture)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_Manager1Portrait","Manager1Portrait", CUITexture)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_Manager2Portrait","Manager2Portrait", CUITexture)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_LeaderNameLabel","LeaderNameLabel", UILabel)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_Manager1NameLabel","Manager1NameLabel", UILabel)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_Manager2NameLabel","Manager2NameLabel", UILabel)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_EditorLeaderButton","EditorLeaderButton", GameObject)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_EditorManager1Button","EditorManager1Button", GameObject)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_EditorManager2Button","EditorManager2Button", GameObject)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_FixButton","FixButton", GameObject)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_QuanXianButton","QuanXianButton", GameObject)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_PopupMenuCenter","Center", GameObject)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_SaveButton","SaveButton", GameObject)
RegistChildComponent(LuaZongMenWeiJieWnd,"m_HightlightFixButton","HightlightFixButton", GameObject)

RegistClassMember(LuaZongMenWeiJieWnd, "m_ChangedList")
RegistClassMember(LuaZongMenWeiJieWnd, "m_ChooseZhangLaoIndex")
RegistClassMember(LuaZongMenWeiJieWnd, "m_ManagerList")
RegistClassMember(LuaZongMenWeiJieWnd, "m_CurShowMemberList")
function LuaZongMenWeiJieWnd:Init()
    self.m_ChangedList = {}
    self.m_CurShowMemberList = {}
    self.m_EditorLeaderButton:SetActive(false)
    self.m_EditorManager1Button:SetActive(false)
    self.m_EditorManager2Button:SetActive(false)
    self.m_FixButton:SetActive(false)
    self.m_SaveButton:SetActive(false)
    self.m_HightlightFixButton:SetActive(false)
    UIEventListener.Get(self.m_FixButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnFixButtonClicked()
    end)
    UIEventListener.Get(self.m_SaveButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnFixButtonClicked()
    end)
    UIEventListener.Get(self.m_QuanXianButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.ZongMenJurisdictionSettingWnd)
    end)
    UIEventListener.Get(self.m_EditorLeaderButton).onClick = DelegateFactory.VoidDelegate(function (p)
        if CClientMainPlayer.Inst then
            Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "candidate",EnumSectOffice.eZhangMen)
        end
    end)
    UIEventListener.Get(self.m_EditorManager1Button).onClick = DelegateFactory.VoidDelegate(function (p)
        if CClientMainPlayer.Inst then
            self.m_ChooseZhangLaoIndex = 1
            Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "candidate",EnumSectOffice.eZhangLao)
        end
    end)
    UIEventListener.Get(self.m_EditorManager2Button).onClick = DelegateFactory.VoidDelegate(function (p)
        if CClientMainPlayer.Inst then
            self.m_ChooseZhangLaoIndex = 2
            Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId, "candidate",EnumSectOffice.eZhangLao)
        end
    end)
    UIEventListener.Get(self.m_LeaderPortrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local pos = Vector3(540, 0)
        pos = self.m_LeaderPortrait.transform:TransformPoint(pos)
        self:ShowPlayerPopupMenu(1, pos)
    end)
    UIEventListener.Get(self.m_Manager1Portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local pos = Vector3(540, 0)
        pos = self.m_Manager1Portrait.transform:TransformPoint(pos)
        self:ShowPlayerPopupMenu(2, pos)
    end)
    UIEventListener.Get(self.m_Manager2Portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local pos = Vector3(540, 0)
        pos = self.m_Manager2Portrait.transform:TransformPoint(pos)
        self:ShowPlayerPopupMenu(3, pos)
    end)
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId,"office",0)
    end
end

function LuaZongMenWeiJieWnd:OnFixButtonClicked()
    if not self.m_ManagerList then return end
    if not self.m_ChangedList then self.m_ChangedList = {} end
    local isChanged = false
    for index, playerId in pairs(self.m_ChangedList) do
        if playerId ~= self.m_ManagerList[index].PlayerId then
            isChanged = true
        end
    end
    if isChanged then
        local msg1, msg2, rmTbl, addTbl,isZhangMenChanged = self:GetRenMianMsg()
        LuaZongMenMgr:ConfirmRenMian(msg1, msg2, rmTbl, addTbl,isZhangMenChanged)
    end
    self.m_ChangedList = {}
    self.m_EditorLeaderButton:SetActive(not self.m_EditorLeaderButton.gameObject.activeSelf)
    self.m_EditorManager1Button:SetActive(not self.m_EditorManager1Button.gameObject.activeSelf)
    self.m_EditorManager2Button:SetActive(not self.m_EditorManager2Button.gameObject.activeSelf)
    self.m_SaveButton:SetActive(not self.m_SaveButton.gameObject.activeSelf)
    self.m_QuanXianButton:SetActive(not self.m_QuanXianButton.gameObject.activeSelf)
    self.m_HightlightFixButton:SetActive(not self.m_HightlightFixButton.gameObject.activeSelf)
end

function LuaZongMenWeiJieWnd:GetRenMianMsg()
    local msg1, msg2 = "", ""
    local rmTbl,addTbl = nil, nil
    local isZhangMenChanged = false
    local rmlist,addlist = CreateFromClass(MakeGenericClass(Dictionary, String, Object)),CreateFromClass(MakeGenericClass(Dictionary, String, Object))
    for index, data in pairs(self.m_ChangedList) do
        local weijie =  index == 1 and LocalString.GetString("门主") or LocalString.GetString("长老")
        local playerId = data.PlayerId
        if playerId ~= self.m_ManagerList[index].PlayerId then
            if msg1 ~=  "" then 
                msg1 = msg1 .. "\n"
            end
            if self.m_ManagerList[index].Name then
                if index == 1 then isZhangMenChanged  = true end
                msg1 = msg1 .. SafeStringFormat3(LocalString.GetString("%s[fffe91]%s[ffffff]位阶;"), self.m_ManagerList[index].Name, weijie)
                CommonDefs.DictAdd_LuaCall(rmlist, tostring(self.m_ManagerList[index].PlayerId), index == 1 and EnumSectOffice.eZhangMen or EnumSectOffice.eZhangLao)
            end
            if playerId ~= 0 then
                if msg2 ~=  "" then 
                    msg2 = msg2 .. "\n"
                end
            if index == 1 then isZhangMenChanged  = true end
                msg2 = msg2 .. SafeStringFormat3(LocalString.GetString("%s为[fffe91]%s[ffffff];"),data.Name, weijie)
                CommonDefs.DictAdd_LuaCall(addlist, tostring(playerId), index == 1 and EnumSectOffice.eZhangMen or EnumSectOffice.eZhangLao)
            end
        end
    end
    rmTbl = MsgPackImpl.pack(rmlist)
    addTbl = MsgPackImpl.pack(addlist)
    return msg1, msg2, rmTbl, addTbl,isZhangMenChanged
end

function LuaZongMenWeiJieWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSectOfficeInfoResult", self, "OnSectOfficeInfoResult")
    g_ScriptEvent:AddListener("OnSectOfficeCandidataInfoResult", self, "OnSectOfficeCandidataInfoResult")
end

function LuaZongMenWeiJieWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSectOfficeInfoResult", self, "OnSectOfficeInfoResult")
    g_ScriptEvent:RemoveListener("OnSectOfficeCandidataInfoResult", self, "OnSectOfficeCandidataInfoResult")
end

function LuaZongMenWeiJieWnd:OnSectOfficeInfoResult(list, isShowFixBtn)
    self.m_FixButton:SetActive(isShowFixBtn == 1)
    local zhanglaoindex = 1
    self.m_ManagerList = {{PlayerId = 0},{PlayerId = 0},{PlayerId = 0}}
    for _, info in pairs(list) do
        if info.Office == EnumSectOffice.eZhangMen then
            self.m_LeaderNameLabel.text = info.Name
            self.m_LeaderPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Class, info.Gender, -1),false)
            self.m_ManagerList[1] = {PlayerId = info.PlayerId, Name = info.Name,Class = info.Class, Gender = info.Gender}   
            self.m_CurShowMemberList[1] = self.m_ManagerList[1]
        elseif zhanglaoindex == 1 and info.Office == EnumSectOffice.eZhangLao then
            self.m_Manager1NameLabel.text = info.Name
            self.m_Manager1Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Class, info.Gender, -1),false)
            self.m_ManagerList[2] = {PlayerId = info.PlayerId, Name = info.Name,Class = info.Class, Gender = info.Gender}
            self.m_CurShowMemberList[2] = self.m_ManagerList[2]
            zhanglaoindex = 2
        elseif info.Office == EnumSectOffice.eZhangLao then
            self.m_Manager2NameLabel.text = info.Name
            self.m_Manager2Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Class, info.Gender, -1),false)
            self.m_ManagerList[3] ={PlayerId = info.PlayerId, Name = info.Name,Class = info.Class, Gender = info.Gender}
            self.m_CurShowMemberList[3] = self.m_ManagerList[3]
        end
    end
end


function LuaZongMenWeiJieWnd:OnSectOfficeCandidataInfoResult(data, office)
    local items = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    if office ~= EnumSectOffice.eZhangMen then
        local item = PopupMenuItemData(LocalString.GetString("无"),  DelegateFactory.Action_int(function (index) 
            if office == EnumSectOffice.eZhangMen then
                self.m_LeaderNameLabel.text = ""
                self.m_LeaderPortrait.material = nil
                self.m_ChangedList[1] = {PlayerId = 0, Name = ""}
                self.m_CurShowMemberList[1] = self.m_ChangedList[1]
            elseif office == EnumSectOffice.eZhangLao and self.m_ChooseZhangLaoIndex == 1 then
                self.m_Manager1NameLabel.text = ""
                self.m_Manager1Portrait.material = nil
                self.m_ChangedList[2] = {PlayerId = 0, Name = ""}
                self.m_CurShowMemberList[2] = self.m_ChangedList[2]
            elseif office == EnumSectOffice.eZhangLao and self.m_ChooseZhangLaoIndex == 2 then
                self.m_Manager2NameLabel.text = ""
                self.m_Manager2Portrait.material = nil
                self.m_ChangedList[3] = {PlayerId = 0, Name = ""}
                self.m_CurShowMemberList[3] = self.m_ChangedList[3]
            end
        end), false, nil)
        CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
    end
    for i, t1 in pairs(self.m_ManagerList) do
        if self.m_ChangedList[i] and self.m_ChangedList[i].PlayerId ~= t1.PlayerId and t1.PlayerId ~= 0 then
            local tempt = t1
            table.insert(data, tempt)
        end       
    end
    for _,info in pairs(data) do
        local name = info.Name
        local portraitName = CUICommonDef.GetPortraitName(info.Class, info.Gender, -1)
        local playerId = info.PlayerId
        local canChoose = true
        for i, t in pairs(self.m_ChangedList) do
            if t.PlayerId == playerId then
                canChoose = false
                break
            end
        end
        if canChoose then
            local item = PopupMenuItemData(name,  DelegateFactory.Action_int(function (index) 
                if office == EnumSectOffice.eZhangMen then
                    self.m_LeaderNameLabel.text = name
                    self.m_LeaderPortrait:LoadNPCPortrait(portraitName ,false)
                    self.m_ChangedList[1] = {PlayerId = playerId, Name = name}
                    self.m_CurShowMemberList[1] = self.m_ChangedList[1]
                elseif office == EnumSectOffice.eZhangLao and self.m_ChooseZhangLaoIndex == 1 then
                    self.m_Manager1NameLabel.text = name
                    self.m_Manager1Portrait:LoadNPCPortrait(portraitName ,false)
                    self.m_ChangedList[2] = {PlayerId = playerId, Name = name}
                    self.m_CurShowMemberList[2] = self.m_ChangedList[2]
                elseif office == EnumSectOffice.eZhangLao and self.m_ChooseZhangLaoIndex == 2 then
                    self.m_Manager2NameLabel.text = name
                    self.m_Manager2Portrait:LoadNPCPortrait(portraitName ,false)
                    self.m_ChangedList[3] = {PlayerId = playerId, Name = name}
                    self.m_CurShowMemberList[3] = self.m_ChangedList[3]
                end
            end), false, nil)
            CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
        end 
    end
    --CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(items), self.m_PopupMenuCenter.transform, CPopupMenuInfoMgr.AlignType.Bottom)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(items), self.m_PopupMenuCenter.transform, AlignType.Bottom, 1, nil, nil, nil, 600,420)
end


function LuaZongMenWeiJieWnd:ShowPlayerPopupMenu(index, pos)
    local id = self.m_CurShowMemberList[index] and self.m_CurShowMemberList[index].PlayerId or 0
    CPlayerInfoMgr.ShowPlayerPopupMenu(id, CIMMgr.Inst:IsMyFriend(id) and EnumPlayerInfoContext.FriendInFriendWnd or EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
end