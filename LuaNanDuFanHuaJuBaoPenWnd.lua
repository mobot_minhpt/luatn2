local QnRadioBox = import "L10.UI.QnRadioBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIGrid = import "UIGrid"
local UITexture = import "UITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
local PathMode = import "DG.Tweening.PathMode"
LuaNanDuFanHuaJuBaoPenWnd = class()
LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId = 1

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "FameQnRadioBox", "FameQnRadioBox", QnRadioBox)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "Tab1", "Tab1", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "Tab2", "Tab2", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "Tab3", "Tab3", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "Tab4", "Tab4", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "Tab5", "Tab5", QnSelectableButton)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "CiTiaoGrid", "CiTiaoGrid", GameObject)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "CiTiaoTemplate", "CiTiaoTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "ProgressBar", "ProgressBar", UITexture)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "ProgressBarBg", "ProgressBarBg", UITexture)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "FameLabel", "FameLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "Item", "Item", GameObject)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "CheckIcon", "CheckIcon", GameObject)
RegistChildComponent(LuaNanDuFanHuaJuBaoPenWnd, "RewardItem", "RewardItem", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaJuBaoPenWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.topTabList = {self.Tab1, self.Tab2, self.Tab3, self.Tab4, self.Tab5}    
    self.fameName = {"WuYiXiangFameIndex", "ShiLiuLouFameIndex", "ShiLiQingHuaiFameIndex", "ShangShiJieKouFameIndex", "SanFaSiFameIndex"}
    self.wordCfgName = {"WuYiXiangFameIndex", "ShiLiuLouFameIndex", "ShiLiQingHuaiFameIndex", "ShangShiJieKouFameIndex", "SanFaSiFameIndex"}

    self:InitUIEvent()

    g_ScriptEvent:AddListener("UnlockNanDuJuBaoPenItem", self, "OnUnlockNanDuJuBaoPenItem")
    g_ScriptEvent:AddListener("GetNanDuJuBaoPenRewardSuccess", self, "OnGetNanDuJuBaoPenRewardSuccess")

end

function LuaNanDuFanHuaJuBaoPenWnd:Init()

end

function LuaNanDuFanHuaJuBaoPenWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("UnlockNanDuJuBaoPenItem", self, "OnUnlockNanDuJuBaoPenItem")
    g_ScriptEvent:RemoveListener("GetNanDuJuBaoPenRewardSuccess", self, "OnGetNanDuJuBaoPenRewardSuccess")
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaJuBaoPenWnd:InitUIEvent()
    self.FameQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId = index+1
        self:RefreshCiTiaoWidget(index+1)
        self:RefreshUI()
    end)
end

function LuaNanDuFanHuaJuBaoPenWnd:OnEnable()
    self.ciTiaoData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.ciTiaoData = data[4]
        end
    end
    self.FameQnRadioBox:ChangeTo(LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId-1, true)
    self:RefreshUI(LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId)
end

function LuaNanDuFanHuaJuBaoPenWnd:OnDisable()

end

function LuaNanDuFanHuaJuBaoPenWnd:OnUnlockNanDuJuBaoPenItem()
    self.ciTiaoData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.ciTiaoData = data[4]
        end
    end
    self:RefreshUI(LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId)
end

function LuaNanDuFanHuaJuBaoPenWnd:OnGetNanDuJuBaoPenRewardSuccess()
    --统一更新UI就好
    self:OnUnlockNanDuJuBaoPenItem()
end

function LuaNanDuFanHuaJuBaoPenWnd:RefreshUI()
    for i = 1, #self.topTabList do
        local unlock, all = LuaYiRenSiMgr:CalculateLordProsperity(self.fameName[i])
        self.topTabList[i].transform:Find("FameLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("繁荣度 ") .. math.floor(unlock/all*100) .. "%"
        self.topTabList[i].transform:Find("NormalLabel"):GetComponent(typeof(UILabel)).text = LuaYiRenSiMgr.LordFameName[i]

        self.topTabList[i].transform:Find("FameLabel"):GetComponent(typeof(UILabel)).color = i == LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId and NGUIText.ParseColor24("FAE9BA", 0) or NGUIText.ParseColor24("8F7B5C", 0)
        self.topTabList[i].transform:Find("NormalLabel"):GetComponent(typeof(UILabel)).color = i == LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId and NGUIText.ParseColor24("FAE9BA", 0) or NGUIText.ParseColor24("454545", 0)

    end
    
    --refresh bottom
    local unlock, all = LuaYiRenSiMgr:CalculateLordProsperity(self.fameName[LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId])
    self.FameLabel.text =  LocalString.GetString("繁荣度 ") .. math.floor(unlock/all*100) .. "%"
    self.ProgressBar.transform:GetComponent(typeof(UISlider)).value = unlock / all
    local overallFameName = self.fameName[LuaNanDuFanHuaJuBaoPenWnd.ActiveFameId]
    local translateIndex = nil
    for i = 1, #self.wordCfgName do
        if self.wordCfgName[i] == overallFameName then
            translateIndex = i
            break
        end
    end
    if translateIndex then
        local rewardCfg = NanDuFanHua_JuBaoPenReward.GetData(translateIndex)
        --local curCiTiaoGetReward = bit.band(self.ciTiaoData[6] and self.ciTiaoData[6] or 0, bit.lshift(1, translateIndex - 1)) > 0
        --self.CheckIcon:SetActive(curCiTiaoGetReward)
        self:UpdateRewardItemState(translateIndex)
        local rewardString = rewardCfg.Reward
        local itemInfoList = g_LuaUtil:StrSplit(rewardString,";")
        local firstItemInfoList = g_LuaUtil:StrSplit(itemInfoList[1],",")
        local itemId = tonumber(firstItemInfoList[1])

        local texture = self.Item.transform:GetComponent(typeof(CUITexture))
        local ItemData = Item_Item.GetData(itemId)
        if ItemData then texture:LoadMaterial(ItemData.Icon) end
        UIEventListener.Get(self.RewardItem).onClick = DelegateFactory.VoidDelegate(function (_)
            if self:CheckCanGetReward(translateIndex) then
                Gac2Gas.ReuqestNanDuJuBaoPenReward(translateIndex)
            else
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        end)
    end
end

function LuaNanDuFanHuaJuBaoPenWnd:UpdateRewardItemState(fameIndex)
    --0: 进度条未完成
    --1: 进度条已完成, 未领取
    --2: 进度条已完成, 已领取
    local curCiTiaoGetReward = bit.band(self.ciTiaoData[6] and self.ciTiaoData[6] or 0, bit.lshift(1, fameIndex - 1)) > 0
    local fxComp = self.RewardItem.transform:Find("blueLineFx"):GetComponent(typeof(CUIFx))

    if curCiTiaoGetReward then
        --状态2
        Extensions.SetLocalPositionZ(self.RewardItem.transform:Find("di"), -1)
        Extensions.SetLocalPositionZ(self.RewardItem.transform:Find("Item"), -1)
        self.CheckIcon:SetActive(true)
        fxComp:DestroyFx()
        LuaTweenUtils.DOKill(fxComp.transform, true)
    else
        local canGetReward = self:CheckCanGetReward(fameIndex)
        if canGetReward then
            --状态1
            Extensions.SetLocalPositionZ(self.RewardItem.transform:Find("di"), 0)
            Extensions.SetLocalPositionZ(self.RewardItem.transform:Find("Item"), 0)
            self.CheckIcon:SetActive(false)
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.RewardItem.transform)
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
            fxComp:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
            fxComp.transform.localPosition = waypoints[0]
            LuaTweenUtils.DOLuaLocalPath(fxComp.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        else
            --状态0
            Extensions.SetLocalPositionZ(self.RewardItem.transform:Find("di"), 0)
            Extensions.SetLocalPositionZ(self.RewardItem.transform:Find("Item"), 0)
            self.CheckIcon:SetActive(false)
            fxComp:DestroyFx()
            LuaTweenUtils.DOKill(fxComp.transform, true)
        end
    end
end

function LuaNanDuFanHuaJuBaoPenWnd:CheckCanGetReward(fameIndex)
    local curCiTiaoGetReward = bit.band(self.ciTiaoData[6] and self.ciTiaoData[6] or 0, bit.lshift(1, fameIndex - 1)) > 0
    if curCiTiaoGetReward then
        --已领取
        return false
    end

    local unlockCiTiao = self.ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[self.wordCfgName[fameIndex]]-1] and self.ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[self.wordCfgName[fameIndex]]-1] or 0
    local checkAllUnlock = true
    NanDuFanHua_JuBaoPen.Foreach(function(key, data)
        --为了兼顾线上的数据, 已解锁的成就忽略status的限制, 未解锁的成就依然被status限制
        if data.Fame == fameIndex then
            local cfg = NanDuFanHua_JuBaoPen.GetData(key)
            local curCiTiaoUnlock = bit.band(unlockCiTiao, bit.lshift(1, cfg.Idx - 1)) > 0
            if curCiTiaoUnlock then
                --ignore
            else
                if data.Status ~= 3 then
                    checkAllUnlock = false
                end
            end
        end
    end)
    return checkAllUnlock
end

function LuaNanDuFanHuaJuBaoPenWnd:RefreshCiTiaoWidget(fameIndex)
    Extensions.RemoveAllChildren(self.CiTiaoGrid.transform)
    local overallFameName = self.fameName[fameIndex]
    local translateIndex = nil
    for i = 1, #self.wordCfgName do
        if self.wordCfgName[i] == overallFameName then
            translateIndex = i
            break
        end
    end

    self.ciTiaoList = {}
    if translateIndex then
        NanDuFanHua_JuBaoPen.Foreach(function(key, data)
            --为了兼顾线上的数据, 已解锁的成就忽略status的限制, 未解锁的成就依然被status限制
            if data.Fame == translateIndex then
                local cfg = NanDuFanHua_JuBaoPen.GetData(key)
                local unlockCiTiao = self.ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[self.wordCfgName[translateIndex]]-1] and self.ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[self.wordCfgName[translateIndex]]-1] or 0
                local curCiTiaoUnlock = bit.band(unlockCiTiao, bit.lshift(1, cfg.Idx - 1)) > 0
                if curCiTiaoUnlock then
                    local go = NGUITools.AddChild(self.CiTiaoGrid, self.CiTiaoTemplate)
                    go:SetActive(true)
                    self:InitCiTiao(go, key, translateIndex)
                    self.ciTiaoList[NanDuFanHua_JuBaoPen.GetData(key).Idx] = go
                else
                    if data.Status ~= 3 then
                        local go = NGUITools.AddChild(self.CiTiaoGrid, self.CiTiaoTemplate)
                        go:SetActive(true)
                        self:InitCiTiao(go, key, translateIndex)
                        self.ciTiaoList[NanDuFanHua_JuBaoPen.GetData(key).Idx] = go
                    end
                end
            end
        end)
        
        self.CiTiaoGrid.transform:GetComponent(typeof(UIGrid)):Reposition()
    end
end

function LuaNanDuFanHuaJuBaoPenWnd:InitCiTiao(obj, key, translateIndex)
    local cfg = NanDuFanHua_JuBaoPen.GetData(key)
    local unlockCiTiao = self.ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[self.wordCfgName[translateIndex]]-1] and self.ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[self.wordCfgName[translateIndex]]-1] or 0
    local curCiTiaoUnlock = bit.band(unlockCiTiao, bit.lshift(1, cfg.Idx - 1)) > 0
    
    obj.transform:Find("LockedPart").gameObject:SetActive(not curCiTiaoUnlock)
    obj.transform:Find("UnlockedPart").gameObject:SetActive(curCiTiaoUnlock)
    if curCiTiaoUnlock then
        obj.transform:Find("UnlockedPart/Label"):GetComponent(typeof(UILabel)).text = cfg.Name
        UIEventListener.Get(obj.transform:Find("UnlockedPart").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            for k, v in pairs(self.ciTiaoList) do
                v.transform:Find("UnlockedPart/SelectedBg").gameObject:SetActive(k == cfg.Idx)
            end
            LuaNanDuFanHuaLordCiTiaoDetailWnd.citiaoIndex = key
            CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordCiTiaoDetailWnd)
        end)
    end
end 
