local CMainCamera = import "L10.Engine.CMainCamera"
local Vector3 = import "UnityEngine.Vector3"
local CPos = import "L10.Engine.CPos"

LuaRefinePeopleButtonWnd = class()
RegistClassMember(LuaRefinePeopleButtonWnd, "m_RefineBtn")
RegistClassMember(LuaRefinePeopleButtonWnd, "m_KaiZhenBtn")
RegistClassMember(LuaRefinePeopleButtonWnd, "m_TopAnchor")
RegistClassMember(LuaRefinePeopleButtonWnd, "m_CachedPos")
RegistClassMember(LuaRefinePeopleButtonWnd, "m_ViewPos")
RegistClassMember(LuaRefinePeopleButtonWnd, "m_ScreenPos")
RegistClassMember(LuaRefinePeopleButtonWnd, "m_TopWorldPos")

function LuaRefinePeopleButtonWnd:Init()
    self.m_RefineBtn = self.transform:Find("Anchor/RefineBtn/LianHua/Texture").gameObject
    self.m_TopAnchor = nil
    self.m_CachedPos = self.transform.position

    -- 处理炼人法阵
    local lianrenData = LianHua_Setting.GetData().FaZhenFakeNpcInfo
    local index = LuaZongMenMgr.m_LianRenBtnPos
    local x = math.floor(lianrenData[index*2 -1])
    local y = math.floor(lianrenData[index*2])

    self.m_TopAnchor = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
    self.m_TopAnchor.y = self.m_TopAnchor.y + LianHua_Setting.GetData().LianHuaGuaiBtnHight

    UIEventListener.Get(self.m_RefineBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if self:CheckCondition() then
            Gac2Gas.RequestChangeSectWuXing(index)
        else
            g_MessageMgr:ShowMessage("LianHua_Change_WuXing_TimeLimit")
        end
    end)

    self:Update()
end

function LuaRefinePeopleButtonWnd:Update()
    -- 更新位置 位于法阵正上方
    self.m_ViewPos = CMainCamera.Main:WorldToViewportPoint(self.m_TopAnchor)
    
    if self.m_ViewPos .z > 0 and self.m_ViewPos .x > 0 and self.m_ViewPos .x < 1 and self.m_ViewPos .y > 0 and self.m_ViewPos .y < 1 then
        self.m_ScreenPos = CMainCamera.Main:WorldToScreenPoint(self.m_TopAnchor)
    else  
        self.m_ScreenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    self.m_ScreenPos.z = 0
    self.m_TopWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_ScreenPos)

    if self.m_CachedPos.x ~= self.m_TopWorldPos.x or self.m_CachedPos.x ~= self.m_TopWorldPos.x then
        self.transform.position = self.m_TopWorldPos
        self.m_CachedPos = self.m_TopWorldPos
    end
end

function LuaRefinePeopleButtonWnd:GetWeekBegin(t)
	local d = os.date("*t", t)
	local day = os.time({year = d.year, month = d.month, day = d.day, hour = 0, min = 0, sec = 0, isdst = false})
	return day - ((d.wday + 5) % 7) * 86400
end

function LuaRefinePeopleButtonWnd:WeekDiff(t1, t2)
	local week1 = LuaRefinePeopleButtonWnd.GetWeekBegin(t1)
	local week2 = LuaRefinePeopleButtonWnd.GetWeekBegin(t2)
	return (week1 - week2) / 604800
end

function LuaRefinePeopleButtonWnd:IsSameWeek(t1, t2)
	return LuaRefinePeopleButtonWnd.WeekDiff(t1, t2) == 0
end

function LuaRefinePeopleButtonWnd:CheckCondition()
    return true
end
