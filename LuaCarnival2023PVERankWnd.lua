local QnAdvanceGridView      = import "L10.UI.QnAdvanceGridView"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Profession             = import "L10.Game.Profession"
local CPlayerInfoMgr         = import "CPlayerInfoMgr"
local EnumPlayerInfoContext  = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel             = import "L10.Game.EChatPanel"
local AlignType              = import "CPlayerInfoMgr+AlignType"
local Item_Item              = import "L10.Game.Item_Item"

LuaCarnival2023PVERankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCarnival2023PVERankWnd, "myRank", "Rank", UILabel)
RegistChildComponent(LuaCarnival2023PVERankWnd, "myName", "Name", UILabel)
RegistChildComponent(LuaCarnival2023PVERankWnd, "myTime", "Time", UILabel)
RegistChildComponent(LuaCarnival2023PVERankWnd, "myClass", "Class", UISprite)
RegistChildComponent(LuaCarnival2023PVERankWnd, "tableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaCarnival2023PVERankWnd, "topAward", "TopAward", CQnReturnAwardTemplate)
RegistChildComponent(LuaCarnival2023PVERankWnd, "awardLabel", "AwardLabel", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaCarnival2023PVERankWnd, "rankList")
RegistClassMember(LuaCarnival2023PVERankWnd, "rankSpriteTbl")

function LuaCarnival2023PVERankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaCarnival2023PVERankWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryCarnival2023RankResult", self, "OnQueryCarnival2023RankResult")
end

function LuaCarnival2023PVERankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryCarnival2023RankResult", self, "OnQueryCarnival2023RankResult")
end

function LuaCarnival2023PVERankWnd:OnQueryCarnival2023RankResult(myRank, finishTime, RankData_U)
    self.myRank.text = myRank > 0 and myRank or LocalString.GetString("未上榜")
    self.myTime.text = self:GetFinishTimeStr(finishTime)

    self.rankList = {}
    local rankData = g_MessagePack.unpack(RankData_U)
    for i = 1, #rankData, 5 do
        table.insert(self.rankList, {
            rank = rankData[i],
            playerId = rankData[i + 1],
            class = rankData[i + 2],
            name = rankData[i + 3],
            finishTime = rankData[i + 4],
        })
    end
    self.tableView:ReloadData(true, false)
end


function LuaCarnival2023PVERankWnd:Init()
    self.myRank.text = ""
    local mainPlayer = CClientMainPlayer.Inst
    self.myClass.spriteName = mainPlayer and Profession.GetIcon(mainPlayer.Class) or ""
    self.myName.text = mainPlayer and mainPlayer.Name or ""
    self.myTime.text = ""
    self:InitTableView()
    self:InitBottom()
    Gac2Gas.QueryCarnival2023Rank()
end

function LuaCarnival2023PVERankWnd:InitTableView()
    self.rankList = {}
    self.rankSpriteTbl = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankList
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item, index)
        end
    )
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.rankList[row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)
end

function LuaCarnival2023PVERankWnd:InitItem(item, index)
    local tf = item.transform

    local name = tf:Find("Name"):GetComponent(typeof(UILabel))
    local rankNum = tf:Find("Rank"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("Rank/Sprite"):GetComponent(typeof(UISprite))
    local time = tf:Find("Time"):GetComponent(typeof(UILabel))
    local class = tf:Find("Class"):GetComponent(typeof(UISprite))

    local info = self.rankList[index + 1]
    local rank = info.rank

    rankNum.text = ""
    rankSprite.spriteName = ""
    if rank >= 1 and rank <= 3 then
        rankSprite.spriteName = self.rankSpriteTbl[rank]
    else
        rankNum.text = rank
    end

    name.text = info.name
    time.text = self:GetFinishTimeStr(info.finishTime)
    class.spriteName = Profession.GetIconByNumber(info.class)
end

function LuaCarnival2023PVERankWnd:GetFinishTimeStr(finishTime)
    local second = finishTime % 60
    local minute = math.floor(finishTime / 60)
    return SafeStringFormat3("%02d:%02d", minute, second)
end

function LuaCarnival2023PVERankWnd:InitBottom()
    local getAwardNeedRank = Carnival2023_Boss.GetData().GetAwardNeedRank
    self.topAward:Init(Item_Item.GetData(LuaCarnival2023Mgr:GetPVERankAwardItemId()), 0)
    self.topAward.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("前%d"), 50)
    self.awardLabel.text = SafeStringFormat3(LocalString.GetString("榜单数据每日清零\n23:59结算时，前%d名可得奖励"), getAwardNeedRank)
end

--@region UIEvent

--@endregion UIEvent
