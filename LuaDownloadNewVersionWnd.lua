local UILabel = import "UILabel"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local Main=import "L10.Engine.Main"
local DRPFMgr = import "L10.Game.DRPFMgr"

local UISlider = import "UISlider"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local DownloadApk=import "L10.Engine.DownloadApk"
local EnumDownloadNewVersionStatus = import "L10.Engine.EnumDownloadNewVersionStatus"
local NativeUtility=import "NativeUtility"
local EnumNetworkType=import "EnumNetworkType"

LuaDownloadNewVersionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDownloadNewVersionWnd, "SpeedLabel", "SpeedLabel", UILabel)
RegistChildComponent(LuaDownloadNewVersionWnd, "PauseButton", "PauseButton", GameObject)
RegistChildComponent(LuaDownloadNewVersionWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaDownloadNewVersionWnd, "PauseLabel", "PauseLabel", UILabel)
RegistChildComponent(LuaDownloadNewVersionWnd, "TipLabel", "TipLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDownloadNewVersionWnd, "m_DownloadApk")
RegistClassMember(LuaDownloadNewVersionWnd, "m_CalcSpeedTick")
RegistClassMember(LuaDownloadNewVersionWnd, "m_Speed")
RegistClassMember(LuaDownloadNewVersionWnd, "m_WaitConfirm")
RegistClassMember(LuaDownloadNewVersionWnd, "m_Tips")
RegistClassMember(LuaDownloadNewVersionWnd, "m_TipTick")

LuaDownloadNewVersionWnd.s_Type = nil
LuaDownloadNewVersionWnd.s_Url = nil
function LuaDownloadNewVersionWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.PauseButton).onClick = DelegateFactory.VoidDelegate(function()
        if self.m_DownloadApk then
            self:SetPause()
        else
            self:TryDownload()
            self.PauseLabel.text = LocalString.GetString("暂停")
        end
    end)
    self.PauseButton:SetActive(false)
    self.ProgressBar.value = 0
    self.SpeedLabel.text = nil
    self.m_WaitConfirm = false
    self:TryDownload()
end

function LuaDownloadNewVersionWnd:Init()
    self.m_Tips = {
        LocalString.GetString("暂停下载不会丢失已下载进度，小倩建议您在WIFI环境下进行更新哦！"),
        LocalString.GetString("切换后台可能导致下载暂停，下载过程中请尽量不要频繁切换后台。"),
        LocalString.GetString("下载安装完成后，不要忘记手动清理安装包哦！"),
    }
    local idx = 1
    self.m_TipTick = RegisterTick(function()
        self.TipLabel.text = self.m_Tips[idx]
        idx = idx+1
        if idx > #self.m_Tips then
            idx = 1
        end
    end,3000)
end

--@region UIEvent

--@endregion UIEvent
function LuaDownloadNewVersionWnd:Update()
    if not self.m_DownloadApk then return end

    if self.m_DownloadApk.Status == EnumDownloadNewVersionStatus.ConfirmDownload then
        if not self.m_WaitConfirm then--进入等待确认状态
            if NativeUtility.GetNetworkInfo() == EnumNetworkType.Wifi then
                self.m_WaitConfirm = false
                self.m_DownloadApk.m_DownloadConfirmed = true
                self.m_DownloadApk.Status = EnumDownloadNewVersionStatus.Downloading
            else
                self.m_WaitConfirm = true
                local msg = SafeStringFormat3(LocalString.GetString("是否继续使用移动数据下载？本次更新大小为%s，建议您在WIFI环境下更新，暂停下载不会丢失已下载进度。"),
                    self:GetDownloadSizeString(self.m_DownloadApk.NeedDownloadSize))

                g_MessageMgr:ShowOkCancelMessage(msg,
                    function()
                        --暂停
                        self:SetPause()
                    end,
                    function()
                        -- self.m_WaitConfirm = true
                        self.m_DownloadApk.m_DownloadConfirmed = true
                        self.m_DownloadApk.Status = EnumDownloadNewVersionStatus.Downloading
                    end, LocalString.GetString("暂停"), LocalString.GetString("下载"), false)
            end
        end
        return
    end

    if self.m_DownloadApk.Status == EnumDownloadNewVersionStatus.Fail then
		DRPFMgr.Inst:RecordForceDownload(LuaDownloadNewVersionWnd.s_Type or "Unknown", LuaDownloadNewVersionWnd.s_Url or "", "inapp_fail")
        if self.m_DownloadApk then
            self.m_DownloadApk:StopDownload()
            self.m_DownloadApk = nil
        end
        self:OnFail()
        return
    end

    if self.m_DownloadApk.Status == EnumDownloadNewVersionStatus.Done then
		DRPFMgr.Inst:RecordForceDownload(LuaDownloadNewVersionWnd.s_Type or "Unknown", LuaDownloadNewVersionWnd.s_Url or "", "inapp_done")
        if self.m_DownloadApk then
            self.m_DownloadApk:StopDownload()
            self.m_DownloadApk = nil
        end
        self.ProgressBar.value = 1
        self.PauseButton:SetActive(false)
        --等待安装
        CUIManager.CloseUI(CLuaUIResources.DownloadNewVersionWnd)
        return
    end
    if self.m_DownloadApk.Status == EnumDownloadNewVersionStatus.Downloading then
        self.PauseButton:SetActive(true)
        self:UpdateView()
    end
end

function LuaDownloadNewVersionWnd:SetPause()
    if self.m_DownloadApk then
        self.ProgressBar.value = self.m_DownloadApk.Progress
        self.SpeedLabel.text = SafeStringFormat3(LocalString.GetString("已暂停，已完成%s/%s"),
            self:GetSizeString(self.m_DownloadApk.DownloadedSize),
            self:GetSizeString(self.m_DownloadApk.RemoteFileSize))

        self.m_DownloadApk:StopDownload()
        self.m_DownloadApk = nil
        self.PauseButton:SetActive(true)
        self.PauseLabel.text = LocalString.GetString("继续")
    end
end

function LuaDownloadNewVersionWnd:UpdateView()
    if self.m_DownloadApk then
        self.ProgressBar.value = self.m_DownloadApk.Progress

        self.SpeedLabel.text = SafeStringFormat3(LocalString.GetString("正在下载中，已完成%s/%s,下载速度%s/s,剩余时间约%s"),
            self:GetSizeString(self.m_DownloadApk.DownloadedSize),
            self:GetSizeString(self.m_DownloadApk.RemoteFileSize),
            self:GetSizeString(self.m_Speed),
            self:GetLeftTimeString()
        )
    end
end

function LuaDownloadNewVersionWnd:GetDownloadSizeString(size)
    if size<1024*1024 then
        return SafeStringFormat3("%.1fKB",size/1024)
    elseif size<1024*1024*1024 then
        return SafeStringFormat3("%.1fMB",size/1024/1024)
    else
        return SafeStringFormat3("%.1fGB",size/1024/1024/1024)
    end
end
function LuaDownloadNewVersionWnd:GetSizeString(size)
    if size<1024*1024 then
        return SafeStringFormat3("%.1fKB",size/1024)
    else
        return SafeStringFormat3("%.1fMB",size/1024/1024)
    end
end
function LuaDownloadNewVersionWnd:GetLeftTimeString()
    if not self.m_DownloadApk then return "?" end
    if self.m_Speed==0 then return "?" end

    local leftSecond = (self.m_DownloadApk.RemoteFileSize -self.m_DownloadApk.DownloadedSize) / self.m_Speed
    if leftSecond<60 then
        return SafeStringFormat3(LocalString.GetString("%d秒"),leftSecond)
    elseif leftSecond<60*60 then
        return SafeStringFormat3(LocalString.GetString("%d分钟%d秒"),leftSecond/60,leftSecond%60)
    else
        return SafeStringFormat3(LocalString.GetString("%d小时%d分钟"),leftSecond/60/60,leftSecond/60%60)
    end
end


function LuaDownloadNewVersionWnd:OnFail()
    local msg = LocalString.GetString("下载中断，是否重试？")
    g_MessageMgr:ShowOkCancelMessage(msg,function()
        self:TryDownload()
    end,function()
        CUIManager.CloseUI(CLuaUIResources.DownloadNewVersionWnd)
    end, nil, nil, false)
end
function LuaDownloadNewVersionWnd:OnDestroy()
    if self.m_DownloadApk then
        self.m_DownloadApk:StopDownload()
        self.m_DownloadApk = nil
    end
    LuaDownloadNewVersionWnd.s_Url = nil
    if self.m_CalcSpeedTick then
        UnRegisterTick(self.m_CalcSpeedTick)
        self.m_CalcSpeedTick = nil
    end
    if self.m_TipTick then
        UnRegisterTick(self.m_TipTick)
        self.m_TipTick = nil
    end
end
local lastSize = nil
function LuaDownloadNewVersionWnd:TryDownload()
    if LuaDownloadNewVersionWnd.s_Url then
        self.m_WaitConfirm = false
        if self.m_DownloadApk then
            self.m_DownloadApk:StopDownload()
            self.m_DownloadApk = nil
        end
        self.m_DownloadApk = DownloadApk()
        self.m_DownloadApk:StartDownloadCoroutine(LuaDownloadNewVersionWnd.s_Url)

        if self.m_CalcSpeedTick then
            UnRegisterTick(self.m_CalcSpeedTick)
            self.m_CalcSpeedTick = nil
        end
        
        self.m_Speed = 0
        lastSize = nil
        self.m_CalcSpeedTick = RegisterTick(function()
            if not self.m_DownloadApk then return end
            if not lastSize then
                lastSize = self.m_DownloadApk.DownloadedSize
            end
            self.m_Speed = (self.m_DownloadApk.DownloadedSize - lastSize)*2
            lastSize = self.m_DownloadApk.DownloadedSize
        end,500)
    end
end