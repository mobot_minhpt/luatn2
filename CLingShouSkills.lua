-- Auto Generated!!
local CLingShouSkills = import "L10.UI.CLingShouSkills"
local CLingShouSkillSlot = import "L10.UI.CLingShouSkillSlot"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Vector3 = import "UnityEngine.Vector3"
CLingShouSkills.m_Init_CS2LuaHook = function (this) 
    this.inited = true
    this.slots = CreateFromClass(MakeGenericClass(List, CLingShouSkillSlot))
    CommonDefs.ListAdd(this.slots, typeof(CLingShouSkillSlot), this.first)
    for i = 0, 6 do
        local go = TypeAs(CommonDefs.Object_Instantiate(this.first.gameObject), typeof(GameObject))
        go.transform.parent = this.grid.transform
        go.transform.localScale = Vector3.one
        local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CLingShouSkillSlot))
        CommonDefs.ListAdd(this.slots, typeof(CLingShouSkillSlot), cmp)
    end
    this.grid:Reposition()
    this:InitNull()
end
CLingShouSkills.m_InitSlots_CS2LuaHook = function (this, skillList) 
    if not this.inited then
        this:Init()
    end
    do
        local i = 1
        while i < skillList.Length do
            if i - 1 < this.slots.Count then
                this.slots[i - 1]:Init(skillList[i])
            end
            i = i + 1
        end
    end
end
CLingShouSkills.m_InitSkills_CS2LuaHook = function (this, prop) 
    if prop.Baby ~= nil and prop.Baby.BornTime > 0 then
        if this.babySkillSlot1 ~= nil and this.babySkillSlot2 ~= nil then
            this.babySkillSlot1.gameObject:SetActive(true)
            this.babySkillSlot1:Init(prop.Baby.SkillList[1])

            this.babySkillSlot2.gameObject:SetActive(true)
            this.babySkillSlot2:Init(prop.Baby.SkillList[2])

            this.grid.transform.localPosition = Vector3(0, 0)
        end
    else
        if this.babySkillSlot1 ~= nil and this.babySkillSlot2 ~= nil then
            this.babySkillSlot1.gameObject:SetActive(false)
            this.babySkillSlot2.gameObject:SetActive(false)
            this.grid.transform.localPosition = Vector3(60, 0)
        end
    end
    this:InitSlots(prop.SkillListForSave)
end
CLingShouSkills.m_InitNull_CS2LuaHook = function (this) 
    if not this.inited then
        this:Init()
    end
    CommonDefs.ListIterate(this.slots, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        item:Init(0)
    end))
end
