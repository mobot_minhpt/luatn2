local CUIManager = import "L10.UI.CUIManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

--同服务器保持一致的枚举
EnumNewbieSchoolShiLianType = {
    eChuJieShiLian = 1, -- 初阶
    eJinJieShiLian = 2, -- 进阶
    eChuShiKaoHe = 3,   -- 出师
}

EnumNewbieSchoolCourseType = {
    eTravel = 1, -- 游历任务
    eFight = 2,  -- 战斗任务
    eMisc = 3,   -- 杂物任务
}

LuaSchoolTaskMgr = class()

LuaSchoolTaskMgr.m_CurrentStage = 0
LuaSchoolTaskMgr.m_OptionInfo = nil
LuaSchoolTaskMgr.m_TranscriptInfo = nil

--根据stage获取阶段名称
function LuaSchoolTaskMgr:GetStageName(stage)
    if stage == EnumNewbieSchoolShiLianType.eChuJieShiLian then 
        return LocalString.GetString("初阶课业")
    elseif stage == EnumNewbieSchoolShiLianType.eJinJieShiLian then
        return LocalString.GetString("进阶课业")
    elseif stage == EnumNewbieSchoolShiLianType.eChuShiKaoHe then
        return LocalString.GetString("出师考验")
    else
        return ""
    end
end
--根据stage获取阶段对应的颜色
function LuaSchoolTaskMgr:GetStageColor(stage)
    if stage == EnumNewbieSchoolShiLianType.eChuJieShiLian then 
        return "fffe91"
    elseif stage == EnumNewbieSchoolShiLianType.eJinJieShiLian then
        return "ff5050"
    elseif stage == EnumNewbieSchoolShiLianType.eChuShiKaoHe then
        return "ff88ff"
    else
        return ""
    end
end

--获取课业任务名称
function LuaSchoolTaskMgr:GetCourseNames()
    local cls =  CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) or nil
    if not cls then return {"", "", ""} end
    local inited = false
    local ret = {}
    NewbieSchoolTask_Dispatch.Foreach(function (id, data)
        if not inited and data.Class == cls then --只有门派区别
            inited = true
            for i=0,data.TasksName.Length-1 do
                table.insert(ret, data.TasksName[i])
            end
        end
    end)

    return ret
end

function LuaSchoolTaskMgr:GetCourseDescs()
    local cls =  CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) or nil
    if not cls then return {"", "", ""} end
    local inited = false
    local ret = {}
    NewbieSchoolTask_Dispatch.Foreach(function (id, data)
        if not inited and data.Class == cls then --只有门派区别
            inited = true
            for i=0,data.TasksNameTips.Length-1 do
                table.insert(ret, data.TasksNameTips[i])
            end
        end
    end)

    return ret
end

--当前阶段课业的完整名
function LuaSchoolTaskMgr:GetCurrentStageName()

    local schoolName = CClientMainPlayer.Inst and CClientMainPlayer.Inst.SchoolName or ""
    local colorStr = self:GetStageColor(self.m_CurrentStage)
    local stageName = self:GetStageName(self.m_CurrentStage)
    
    return SafeStringFormat3("[c][%s]%s %s %s[-][/c]", colorStr, schoolName, LocalString.GetString("师门"), stageName)
end
--当前阶段课业选择提示文本
function LuaSchoolTaskMgr:GetCurrentStageTaskChosenTip()
    if self.m_CurrentStage == 1 then 
        return LocalString.GetString("点击选择今天想要修习的课业")
    elseif self.m_CurrentStage == 2 then
        return LocalString.GetString("点击选择今天想要修习的课业")
    elseif self.m_CurrentStage == 3 then
        return LocalString.GetString("点击选择其中一门课业进行考验")
    else
        return ""
    end
end

--当前课业的得分统计信息
function LuaSchoolTaskMgr:GetCurrentStageStatInfo()
    --今日剩余可获得学分
    local str = ""
    if self.m_CurrentStage == EnumNewbieSchoolShiLianType.eChuShiKaoHe then
        str = g_MessageMgr:FormatMessage("Newbie_School_Task_Excam_Huoli_Notice",
            NewbieSchoolTask_Setting.GetData().HuoLiAward)
    else
        str = g_MessageMgr:FormatMessage("Newbie_School_Task_Huoli_Notice", self.m_OptionInfo.remainCredit,
            NewbieSchoolTask_Setting.GetData().HuoLiAward)
    end

    local colorStr = self:GetStageColor(self.m_CurrentStage + 1)
    local stageName = self:GetStageName(self.m_CurrentStage + 1)
    if self.m_CurrentStage == EnumNewbieSchoolShiLianType.eChuShiKaoHe then
        str = str..""
    else
        str = str..g_MessageMgr:Format(LocalString.GetString("累计获得#G%d#n学分，距离[%s]%s[-]还需#G%d#n学分。"), 
            self.m_OptionInfo.totalObtainedCredit, colorStr, stageName, self.m_OptionInfo.needCredit)
    end
    return str
end
--请求选择课业任务
function LuaSchoolTaskMgr:RequestChooseCourse(index)
    Gac2Gas.RequestSelectNewbieSchoolCourse(self.m_OptionInfo.taskId, index or 0)
end


--打开师门许愿窗口
function LuaSchoolTaskMgr:ShowWishingWnd()
    CUIManager.ShowUI("SchoolTaskWishingWnd")
end

--打开课业选择窗口
function LuaSchoolTaskMgr:ShowOptionWnd(taskId, stage, remainCredit, needCredit, travelCredit, fightCredit, miscCredit)
    if stage<EnumNewbieSchoolShiLianType.eChuJieShiLian or stage>EnumNewbieSchoolShiLianType.eChuShiKaoHe then
        return
    end
    self.m_CurrentStage = stage
    self.m_OptionInfo = {}
    self.m_OptionInfo.taskId = taskId or 0
    self.m_OptionInfo.remainCredit = remainCredit
    self.m_OptionInfo.needCredit = needCredit
    self.m_OptionInfo.detailCredit = {travelCredit, fightCredit, miscCredit}
    self.m_OptionInfo.totalObtainedCredit = travelCredit + fightCredit + miscCredit
    self.m_OptionInfo.courseNames = self:GetCourseNames()
    self.m_OptionInfo.courseDescs = self:GetCourseDescs()
    CUIManager.ShowUI("SchoolTaskOptionWnd")
end

--打开课业成绩窗口
function LuaSchoolTaskMgr:ShowTranscriptWnd(daydiff, travelCredit, fightCredit, miscCredit, wishContent)
    self.m_TranscriptInfo = {}
    self.m_TranscriptInfo.daydiff = daydiff
    self.m_TranscriptInfo.detailCredit = {travelCredit, fightCredit, miscCredit}
    self.m_TranscriptInfo.courseNames = self:GetCourseNames()
    self.m_TranscriptInfo.wishContent = wishContent
    CUIManager.ShowUI("SchoolTaskTranscriptWnd")
end

--提交许愿内容
function LuaSchoolTaskMgr:SubmitWishingWords(wishContent)
    Gac2Gas.RequestNewbieSchoolWish(wishContent or "")
end

