local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Color = import "UnityEngine.Color"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientNpc = import "L10.Game.CClientNpc"
local CameraFollow=import "L10.Engine.CameraControl.CameraFollow"
local EKickOutType = import "L10.Engine.EKickOutType"
local CRenderObject=import "L10.Engine.CRenderObject"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CMainCamera = import "L10.Engine.CMainCamera"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CForcesMgr = import "L10.Game.CForcesMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
LuaMengQuan2023TopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMengQuan2023TopRightWnd, "ExplandBtn", "ExplandBtn", GameObject)
RegistChildComponent(LuaMengQuan2023TopRightWnd, "tipPanel", "tipPanel", GameObject)
RegistChildComponent(LuaMengQuan2023TopRightWnd, "TopNode1", "TopNode1", GameObject)
RegistChildComponent(LuaMengQuan2023TopRightWnd, "TopNode2", "TopNode2", GameObject)
RegistChildComponent(LuaMengQuan2023TopRightWnd, "TopNode3", "TopNode3", GameObject)

--@endregion RegistChildComponent end

function LuaMengQuan2023TopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaMengQuan2023TopRightWnd:Init()
    g_MessageMgr:ShowMessage("Double11_MENGQUAN2023_TEMPSKILL_TIPS")
end

--@region UIEvent

--@endregion UIEvent

function LuaMengQuan2023TopRightWnd:InitWndData()
    self.myPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if self.myPlayerId ~= 0 then
        local bFind, force = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, self.myPlayerId)
        if bFind == true then
            self.teamId = force
        else
            --这个地方不应该跑到, 做个保底
            self.teamId = 0
        end
    end

    local MQLDConfigData = Double11_MQLD.GetData()
    self.dogNpcIds = {}
    for i = 1, MQLDConfigData.DogNpcInfos.Length do
        local NpcId = MQLDConfigData.DogNpcInfos[i-1][0]
        self.dogNpcIds[NpcId] = true
    end

    self.colliderDistance = MQLDConfigData.ColliderCheckDistance
    self.movementDistance = MQLDConfigData.ColliderMovementDistance
    self.movementRatio = MQLDConfigData.MovementCombinationRatio / (MQLDConfigData.MovementCombinationRatio + 1.0)
    self.flyGravity = MQLDConfigData.flyGravity

    self.teamNameList = {LocalString.GetString("红队"),
                          LocalString.GetString("黄队"),
                          LocalString.GetString("蓝队")}
    self.teamBgColorList = {NGUIText.ParseColor("974C42", 0),
                             NGUIText.ParseColor("D6AE3D", 0),
                             NGUIText.ParseColor("5373B2", 0)}
    self.teamNameColorList = {NGUIText.ParseColor("ff5050", 0),
                              NGUIText.ParseColor("ffff00", 0),
                              NGUIText.ParseColor("00BAFF", 0)}

    self.teamFxIdList = {}
    local fxRawData = Double11_MQLD.GetData().TeamFxId
    local rawDataList = g_LuaUtil:StrSplit(fxRawData, ",")
    for i = 1, #rawDataList do
        if rawDataList[i] ~= "" then
            table.insert(self.teamFxIdList, tonumber(rawDataList[i]))
        end
    end
end 

function LuaMengQuan2023TopRightWnd:RefreshVariableUI()
    if LuaShuangshiyi2023Mgr.MQLDPlayInfo then
        self:OnSyncMengQuanRankInfo(LuaShuangshiyi2023Mgr.MQLDPlayInfo.playState, LuaShuangshiyi2023Mgr.MQLDPlayInfo.RankTbl)
    end
end

function LuaMengQuan2023TopRightWnd:InitUIEvent()
    UIEventListener.Get(self.ExplandBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnExplandBtnClick()
    end)
end

function LuaMengQuan2023TopRightWnd:OnExplandBtnClick()
    self.tipPanel:SetActive(false)
    self.ExplandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaMengQuan2023TopRightWnd:OnHideTopAndRightTipWnd()
    self.tipPanel:SetActive(true)
    self.ExplandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaMengQuan2023TopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncMengQuanHengXingRankInfo", self, "OnSyncMengQuanRankInfo")
    g_ScriptEvent:AddListener("Double11MQLDBroadFlyAction", self, "OnReceiveMQLDBroadCastAction")

end

function LuaMengQuan2023TopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncMengQuanHengXingRankInfo", self, "OnSyncMengQuanRankInfo")
    g_ScriptEvent:RemoveListener("Double11MQLDBroadFlyAction", self, "OnReceiveMQLDBroadCastAction")

end


function LuaMengQuan2023TopRightWnd:OnSyncMengQuanRankInfo(playState, rankData, playerId)
    print("rankData", LuaYiRenSiMgr:PrintTable(rankData))
    self.leadForce = 0
    for i = 1, #rankData do
        self:RefreshTopNode(playState, i, rankData[i], playerId)
        if i == 1 then
            self.leadForce = rankData[i][1]
        end
    end
    for i = #rankData+1, 3 do
        self:RefreshTopNode(playState, i, nil, playerId)
    end
    
    local dict = {}
    
    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if TypeIs(obj, typeof(CClientMainPlayer)) then
            local bFind, force = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, self.myPlayerId)
            if bFind then
                local renderObject = obj.RO
                local fx = CEffectMgr.Inst:AddObjectFX(self.teamFxIdList[force], renderObject,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
                renderObject:RemoveFX("MengQuanForceFx")
                renderObject:AddFX("MengQuanForceFx", fx)

                local info = {}
                info.displayName = self.teamNameList[force]
                info.fontColor = self.teamNameColorList[force] and self.teamNameColorList[force] or NGUIText.ParseColor24("ffffff", 0)
                LuaGamePlayMgr:SetTitleBoardInfo(obj.EngineId, info, true)
            end
        elseif TypeIs(obj, typeof(CClientOtherPlayer)) then
            local bFind, force = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, obj.PlayerId)
            if bFind then
                local renderObject = obj.RO
                local fx = CEffectMgr.Inst:AddObjectFX(self.teamFxIdList[force], renderObject,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
                renderObject:RemoveFX("MengQuanForceFx")
                renderObject:AddFX("MengQuanForceFx", fx)

                local info = {}
                info.displayName = self.teamNameList[force]
                info.fontColor = self.teamNameColorList[force] and self.teamNameColorList[force] or NGUIText.ParseColor24("ffffff", 0)
                LuaGamePlayMgr:SetTitleBoardInfo(obj.EngineId, info, true)
            end
        end
    end))
    LuaGamePlayMgr:SetPlayerHeadSpeIconInfos(dict, true, true)
end

function LuaMengQuan2023TopRightWnd:RefreshTopNode(playState, rank, info, playerId)
    local node = self["TopNode" .. rank]
    if info then
        local teamId = info[1]
        local score = info[2]
        local nameLabel = node.transform:Find("teamName"):GetComponent(typeof(UILabel))
        local scoreLabel = node.transform:Find("score"):GetComponent(typeof(UILabel))
        local isMeBg = node.transform:Find("isMeBg")
        local teamBg = node.transform:Find("teamBg"):GetComponent(typeof(UITexture))

        node.transform:Find("rankTex").gameObject:SetActive(playState ~= 1)
        node.transform:Find("defaultRankTex").gameObject:SetActive(playState == 1)

        local isMe = teamId == self.teamId
        nameLabel.text = self.teamNameList[teamId] and self.teamNameList[teamId] or ""
        scoreLabel.text = score
        isMeBg.gameObject:SetActive(isMe)
        teamBg.color = self.teamBgColorList[teamId] and self.teamBgColorList[teamId] or NGUIText.ParseColor("ffffff", 0)
        if isMe then
            --判断一下是不是自己得分了, 得分的话播一下粒子特效
            if self.myLastScroe == nil then
                --初始化分数
                self.myLastScroe = score
            else
                if score > self.myLastScroe then
                    --得分啦
                    --self:PlayScoreEffect(node.transform)
                    if playerId ~= 0 then
                        if playerId == self.myPlayerId then
                            --是主角自己
                            local screenPos = CMainCamera.Main:WorldToScreenPoint(CClientMainPlayer.Inst.RO.Position)
                            screenPos.z = 0
                            local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
                            self:PlayScoreEffect(nguiWorldPos, node.transform)
                        else
                            --是队友
                            node.transform:Find("CUIFx").gameObject:SetActive(false)
                            node.transform:Find("CUIFx").gameObject:SetActive(true)
                        end
                    end
                    self.myLastScroe = score
                end
            end
        end
        node.gameObject:SetActive(true)
    else
        node.gameObject:SetActive(false)
    end
end

function LuaMengQuan2023TopRightWnd:PlayScoreEffect(fromPos, nodeTransform)
    local moveTime = 1
    CCommonUIFxWnd.Instance:PlayLineAni(fromPos, nodeTransform.position, moveTime, DelegateFactory.Action(function (_)
        nodeTransform:Find("CUIFx").gameObject:SetActive(false)
        nodeTransform:Find("CUIFx").gameObject:SetActive(true)
    end))
end

function LuaMengQuan2023TopRightWnd:Update()
    local mainPlayerObj = CClientMainPlayer.Inst
    if mainPlayerObj and mainPlayerObj.IsDie then
        self.skipCheckColliderUntilDeath = false
    end
    if mainPlayerObj and self.skipCheckColliderUntilDeath then
        return
    end
    if mainPlayerObj and (not mainPlayerObj.IsDie) then
        local mainPlayerPosition = mainPlayerObj and mainPlayerObj.RO.Position
        local mainPlayerForwardDir = mainPlayerObj and CommonDefs.op_Multiply_Vector3_Single(mainPlayerObj.RO.transform.forward, 1-self.movementRatio)
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if TypeIs(obj, typeof(CClientNpc)) then
                if obj and self.dogNpcIds[obj.TemplateId] then
                    --check Collider
                    local npcWorldPosition = obj.RO.transform.position
                    local npcForwardDir = CommonDefs.op_Multiply_Vector3_Single(obj.RO.transform.forward, self.movementRatio)
                    local distance = Vector3.Distance(mainPlayerPosition, npcWorldPosition)
                    if distance <= self.colliderDistance then
                        print("check collider", distance, self.colliderDistance)

                        --碰撞发生啦
                        --CameraFollow.Inst:StopFollow()
                        local movementDirection = CommonDefs.op_Addition_Vector3_Vector3(mainPlayerForwardDir, npcForwardDir)
                        local moveOffset = Vector3.zero
                        --检查飞行过程中的碰撞
                        for curCheckDistance = 1, self.movementDistance do
                            local checkMoveOffset = CommonDefs.op_Multiply_Vector3_Single(movementDirection, curCheckDistance)
                            local dx = mainPlayerPosition.x + checkMoveOffset.x
                            local dy = mainPlayerPosition.z + checkMoveOffset.z
                            local barrier = CoreScene.MainCoreScene:GetBarrierv(dx, dy, true)
                            if BarrierType2Int[barrier] == 0 then
                                moveOffset = checkMoveOffset
                            else
                                break
                            end
                        end
                        mainPlayerObj.RO:DoAni("liedown01", false, 0,1, 0.15, true, 0.8)

                        --obj.RO:SetKickOutStatus(true, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -50)
                        --obj.RO.Position = Vector3(x/64.0, y/64.0, z/64.0)
                        --obj.RO:SetKickOutStatus(false, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -20)
                        
                        self.skipCheckColliderUntilDeath = true
                        Gac2Gas.Double11MQLDBroadFlyAction(math.floor(64*(mainPlayerPosition.x+moveOffset.x)), math.floor(64*(mainPlayerPosition.z+moveOffset.z)), math.floor(64*(mainPlayerPosition.y)))
                        return
                    end
                end
            end
        end))
    end
end

function LuaMengQuan2023TopRightWnd:OnReceiveMQLDBroadCastAction(playerId, x, y, z)
    --把PlayerId的玩家，做一个飞起的表现，坐标为服务器传下来中的位置
    local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if obj and TypeIs(obj, typeof(CClientOtherPlayer)) then
        obj.RO:DoAni("liedown01", false, 0,1, 0.15, true, 0.8)
        --obj.RO:SetKickOutStatus(true, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -50)
        --obj.RO.Position = Vector3(x/64.0, y/64.0, z/64.0)
        --obj.RO:SetKickOutStatus(false, EKickOutType.ParabolaWithoutRotate, 0.5, -1, -20)
    end
end