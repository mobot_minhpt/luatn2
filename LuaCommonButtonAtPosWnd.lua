
local CMainCamera = import "L10.Engine.CMainCamera"

LuaCommonButtonAtPosWnd = class()

LuaCommonButtonAtPosWndMgr = {}
LuaCommonButtonAtPosWndMgr.m_PosX = 0
LuaCommonButtonAtPosWndMgr.m_PosY = 0
LuaCommonButtonAtPosWndMgr.m_ButtonName = ""
LuaCommonButtonAtPosWndMgr.m_Callback = {}
LuaCommonButtonAtPosWndMgr.m_ButtonHeight = 1
LuaCommonButtonAtPosWndMgr.m_CallbackName = ""

function LuaCommonButtonAtPosWndMgr:ShowButton(name,x, y, height, str)
	self.m_PosX, self.m_PosY, self.m_ButtonHeight, self.m_CallbackName, self.m_ButtonName = x,y,height,str,name
	CUIManager.ShowUI(CLuaUIResources.CommonButtonAtPosWnd)
end

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonButtonAtPosWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaCommonButtonAtPosWnd, "Btn", "Btn", GameObject)
RegistChildComponent(LuaCommonButtonAtPosWnd, "TwoWordLabel", "TwoWordLabel", UILabel)
RegistClassMember(LuaCommonButtonAtPosWnd, "m_ButtonWorldPos")
--@endregion RegistChildComponent end

function LuaCommonButtonAtPosWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)
    --@endregion EventBind end
end

function LuaCommonButtonAtPosWnd:Init()
	self.TwoWordLabel.text = LuaCommonButtonAtPosWndMgr.m_ButtonName
	local fazhenWorldPos = Utility.GridPos2WorldPos(LuaCommonButtonAtPosWndMgr.m_PosX, LuaCommonButtonAtPosWndMgr.m_PosY)
	self.m_ButtonWorldPos = Vector3(fazhenWorldPos.x, fazhenWorldPos.y + LuaCommonButtonAtPosWndMgr.m_ButtonHeight, fazhenWorldPos.z)
end

function LuaCommonButtonAtPosWnd:Update()
	local viewPos = CMainCamera.Main:WorldToViewportPoint(self.m_ButtonWorldPos)
	local isInView = viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
	local screenPos = isInView and CMainCamera.Main:WorldToScreenPoint(self.m_ButtonWorldPos) or Vector3(0,3000,0)
	screenPos.z = 0
	local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
	self.Anchor.transform.position = worldPos
end

--@region UIEvent

function LuaCommonButtonAtPosWnd:OnBtnClick()
	local callback = LuaCommonButtonAtPosWndMgr.m_Callback[LuaCommonButtonAtPosWndMgr.m_CallbackName] 
	if callback then
		callback()
	end
end

--@endregion UIEvent

LuaCommonButtonAtPosWndMgr.m_Callback["baoqi"] = function()
	Gac2Gas.ErHaPlayerEmbraceNpcLimitDist(20023227, 47000084)
end

LuaCommonButtonAtPosWndMgr.m_Callback["beiqi"] = function()
	Gac2Gas.ErHaPlayerEmbraceNpcLimitDist(20022858, 47000655)
end

LuaCommonButtonAtPosWndMgr.m_Callback["wuyijitan"] = function()
	Gac2Gas.RequestReliveDaGongHunPo(LuaWuYi2022Mgr.m_CurButtonEngineId)
end