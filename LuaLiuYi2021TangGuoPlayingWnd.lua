
local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"

LuaLiuYi2021TangGuoPlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "Rank", "Rank", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "Self", "Self", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "RankTexture", "RankTexture", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "r1", "r1", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "r2", "r2", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoPlayingWnd, "r3", "r3", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2021TangGuoPlayingWnd,  "m_Names")
RegistClassMember(LuaLiuYi2021TangGuoPlayingWnd,  "m_Counts")
RegistClassMember(LuaLiuYi2021TangGuoPlayingWnd,  "m_MyRanks")

function LuaLiuYi2021TangGuoPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

    --@endregion EventBind end
end

function LuaLiuYi2021TangGuoPlayingWnd:Init()
    self.m_Names = {}
    self.m_Counts = {}
    self.m_MyRanks = {self.r1, self.r2, self.r3}
    for i=1,5 do
        if i== 5 then
            self.m_Names[i] = self.Self.transform:Find("Name"):GetComponent(typeof(UILabel))
            self.m_Counts[i] = self.Self.transform:Find("Count"):GetComponent(typeof(UILabel))
        else
            self.m_Names[i] = self.Rank.transform:Find(i.."/Name"):GetComponent(typeof(UILabel))
            self.m_Counts[i] = self.Rank.transform:Find(i.."/Count"):GetComponent(typeof(UILabel))
        end
    end
end

function LuaLiuYi2021TangGuoPlayingWnd:UpdateInfo(info)
    local updateData = {}
    for i=0, info.Count-1, 4 do
        local id = info[i]
        local score = info[i+1]
        local name = info[i+2]
        local rank = info[i+3]

        if CClientMainPlayer.Inst and id == CClientMainPlayer.Inst.Id then
            updateData[5] = {}
            updateData[5].score = score
            updateData[5].name = CClientMainPlayer.Inst.Name
            updateData[5].rank = rank
        end

        if rank>=1 and rank<=4 then
            updateData[rank] = {}
            updateData[rank].name = name
            updateData[rank].score = score
            updateData[rank].rank = rank
        end
    end

    for i=1, 5 do
        local name = LocalString.GetString("暂无")
        local rank = "-"
        local score = "-"
        if updateData[i] then
            name = updateData[i].name
            score = updateData[i].score
            rank = updateData[i].rank
        end
        self.m_Names[i].text = name
        self.m_Counts[i].text = tostring(score)

        if i==5 and rank~="-" then
            self.RankLabel.text = tostring(rank)
            for j=1,3 do
                self.m_MyRanks[j]:SetActive(j==rank)
            end
            if rank<=3 then
                self.RankLabel.gameObject:SetActive(false)
            else
                self.RankLabel.gameObject:SetActive(true)
            end
        end
    end
end

function LuaLiuYi2021TangGuoPlayingWnd:OnEnable( )
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("CandyPlaySyncPlayInfo", self, "UpdateInfo")
end

function LuaLiuYi2021TangGuoPlayingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("CandyPlaySyncPlayInfo", self, "UpdateInfo")
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

function LuaLiuYi2021TangGuoPlayingWnd:OnHideTopAndRightTipWnd( )
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@region UIEvent

function LuaLiuYi2021TangGuoPlayingWnd:OnExpandButtonClick()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end


--@endregion UIEvent

