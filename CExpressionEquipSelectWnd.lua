-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local Byte = import "System.Byte"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionAppearanceInfo = import "L10.Game.CExpressionAppearanceInfo"
local CExpressionAppearanceMgr = import "L10.Game.CExpressionAppearanceMgr"
local CExpressionEquipSelectItem = import "L10.UI.CExpressionEquipSelectItem"
local CExpressionEquipSelectWnd = import "L10.UI.CExpressionEquipSelectWnd"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Expression_Appearance = import "L10.Game.Expression_Appearance"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CExpressionEquipSelectWnd.m_Init_CS2LuaHook = function (this) 


    this.appearanceGroup = CExpressionAppearanceMgr.AppearanceGroup
    local appearanceId = CExpressionAppearanceMgr.Inst:GetMainPlayerSelectedAppearance(this.appearanceGroup)
    local selectedAppearance = Expression_Appearance.GetData(appearanceId)

    if selectedAppearance ~= nil then
        CExpressionAppearanceMgr.AppearancePreviewID = selectedAppearance.PreviewDefine
    end

    this.textureLoader:Init()
    Gac2Gas.RequestCheckExpressionAppearance()
    this:OnUpdatePlayerExpressionAppearance()
    --打开过就不会再触发引导了
    if not COpenEntryMgr.Inst:GetGuideRecord(L10.Game.Guide.GuideDefine.Phase_FirstUseUmbrella) then
        COpenEntryMgr.Inst:SetGuideRecord(L10.Game.Guide.GuideDefine.Phase_FirstUseUmbrella)
    end
end
CExpressionEquipSelectWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this), true)
    UIEventListener.Get(this.applyButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.applyButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnApply, VoidDelegate, this), true)
    this.table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), true)

    EventManager.AddListenerInternal(EnumEventType.SyncValidExpressionAppearance, MakeDelegateFromCSFunction(this.OnSyncValidExpressionAppearance, MakeGenericClass(Action2, Int32, MakeGenericClass(List, UInt32)), this))
    EventManager.AddListener(EnumEventType.UpdatePlayerExpressionAppearance, MakeDelegateFromCSFunction(this.OnUpdatePlayerExpressionAppearance, Action0, this))
end
CExpressionEquipSelectWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this), false)
    UIEventListener.Get(this.applyButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.applyButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnApply, VoidDelegate, this), false)
    this.table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), false)

    EventManager.RemoveListenerInternal(EnumEventType.SyncValidExpressionAppearance, MakeDelegateFromCSFunction(this.OnSyncValidExpressionAppearance, MakeGenericClass(Action2, Int32, MakeGenericClass(List, UInt32)), this))
    EventManager.RemoveListener(EnumEventType.UpdatePlayerExpressionAppearance, MakeDelegateFromCSFunction(this.OnUpdatePlayerExpressionAppearance, Action0, this))
end
CExpressionEquipSelectWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.dataSource.Count then
        local item = TypeAs(view:GetFromPool(0), typeof(CExpressionEquipSelectItem))

        local appearance = this.dataSource[row]
        local isDiable = false
        local isCurrent = false
        local isExpired = false

        if not CommonDefs.ListContains(this.validExpression, typeof(UInt32), appearance.ID) then
            isDiable = true
        end

        local selectedAppearance = CExpressionAppearanceMgr.Inst:GetMainPlayerSelectedAppearance(this.appearanceGroup)
        if appearance.ID == selectedAppearance then
            isCurrent = true
        end

        if CommonDefs.ListContains(this.expireExpression, typeof(UInt32), appearance.ID) then
            isExpired = true
        end

        item:Init(this.dataSource[row], isDiable, isCurrent, isExpired)
        return item
    end
    return nil
end
CExpressionEquipSelectWnd.m_OnItemClick_CS2LuaHook = function (this, row) 

    local item = this.dataSource[row]
    CExpressionAppearanceMgr.AppearancePreviewID = item.PreviewDefine
    this.previewAppearance = item.ID
    this.textureLoader:Init()

    if not CommonDefs.ListContains(this.validExpression, typeof(UInt32), item.ID) then
        local data = Expression_Appearance.GetData(this.previewAppearance)
        this.applyButton.Text = (CommonDefs.ListContains(this.expireExpression, typeof(UInt32), item.ID)  and data.CanRenewal > 0) and LocalString.GetString("续费") or LocalString.GetString("获取")
    else
        this.applyButton.Text = LocalString.GetString("确定")
    end
end
CExpressionEquipSelectWnd.m_OnSyncValidExpressionAppearance_CS2LuaHook = function (this, group, validList) 
    if this.appearanceGroup == group then
        this.validExpression = validList
        this.table:ReloadData(false, false)
    end
end
CExpressionEquipSelectWnd.m_hookOnUpdatePlayerExpressionAppearance = function (this)
    -- 初始化validList
    local appearanceData = CClientMainPlayer.Inst.PlayProp.ExpressionAppearanceData
    local appearances = CreateFromClass(MakeGenericClass(Dictionary, UInt32, CExpressionAppearanceInfo))
    if CommonDefs.DictContains(appearanceData, typeof(Byte), this.appearanceGroup) then
        appearances = CommonDefs.DictGetValue(appearanceData, typeof(Byte), this.appearanceGroup).Appearances
    end

    local defaultAppearance = CExpressionAppearanceMgr.Inst:GetDefaultAppearance(this.appearanceGroup)
    CommonDefs.ListAdd(this.validExpression, typeof(UInt32), defaultAppearance.ID)
    CommonDefs.EnumerableIterate(appearances.Keys, DelegateFactory.Action_object(function (___value) 
        local key = ___value
        local info = CommonDefs.DictGetValue(appearances, typeof(UInt32), key)
        if info.IsExpired == 1 then
            CommonDefs.ListAdd(this.expireExpression, typeof(UInt32), key)
        else
            CommonDefs.ListAdd(this.validExpression, typeof(UInt32), key)
        end
    end))

    CommonDefs.ListSort1(this.dataSource, typeof(Expression_Appearance), DelegateFactory.Comparison_Expression_Appearance(function (e1, e2) 
        -- 排序
        -- 有效的：永久有效的排在又有效期的之前
        -- 无效的：过期的排在未获得的之前
        if CommonDefs.ListContains(this.validExpression, typeof(UInt32), e1.ID) and CommonDefs.ListContains(this.validExpression, typeof(UInt32), e2.ID) then
            if (e1.AvailableTime == 0 and e2.AvailableTime == 0) or (e1.AvailableTime ~= 0 and e2.AvailableTime ~= 0) then
                return NumberCompareTo(e1.Order, e2.Order)
            elseif e1.AvailableTime == 0 and e2.AvailableTime ~= 0 then
                return - 1
            else
                return 1
            end
        elseif not CommonDefs.ListContains(this.validExpression, typeof(UInt32), e1.ID) and not CommonDefs.ListContains(this.validExpression, typeof(UInt32), e2.ID) then
            if (CommonDefs.ListContains(this.expireExpression, typeof(UInt32), e1.ID) and CommonDefs.ListContains(this.expireExpression, typeof(UInt32), e2.ID)) or (not CommonDefs.ListContains(this.expireExpression, typeof(UInt32), e1.ID) and not CommonDefs.ListContains(this.expireExpression, typeof(UInt32), e2.ID)) then
                return NumberCompareTo(e1.Order, e2.Order)
            elseif CommonDefs.ListContains(this.expireExpression, typeof(UInt32), e1.ID) and not CommonDefs.ListContains(this.expireExpression, typeof(UInt32), e2.ID) then
                return - 1
            else
                return 1
            end
        elseif CommonDefs.ListContains(this.validExpression, typeof(UInt32), e1.ID) and not CommonDefs.ListContains(this.validExpression, typeof(UInt32), e2.ID) then
            return - 1
        else
            return 1
        end
    end))

    this.table.m_DataSource = this
    this.table:ReloadData(true, false)
end
CExpressionEquipSelectWnd.m_OnApply_CS2LuaHook = function (this, go) 
    if CommonDefs.ListContains(this.validExpression, typeof(UInt32), this.previewAppearance) then
        Gac2Gas.SetExpressionAppearance(this.appearanceGroup, this.previewAppearance)
        this:Close()
    elseif this.applyButton.Text == LocalString.GetString("续费") then
        LuaExpressionMgr.m_RenewalEquipApperanceId = this.previewAppearance
        CUIManager.ShowUI(CLuaUIResources.ExpressionEquipRenewalWnd)
    else
        local appearance = Expression_Appearance.GetData(this.previewAppearance)
        if appearance ~= nil then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(appearance.ItemId, true, this.applyButton.transform, AlignType.Right)
        end
    end
end
