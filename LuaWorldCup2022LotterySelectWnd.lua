local DelegateFactory        = import "DelegateFactory"
local CCurrentMoneyCtrl      = import "L10.UI.CCurentMoneyCtrl"
local CButton                = import "L10.UI.CButton"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local EnumMoneyType          = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey       = import "L10.Game.EnumPlayScoreKey"
local CServerTimeMgr         = import "L10.Game.CServerTimeMgr"

LuaWorldCup2022LotterySelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022LotterySelectWnd, "topLabel")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "moneyCtrl")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "okButton")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "baseRoot")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "lotteryRoot")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "lottery_Table")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "lottery_AddSubButton")

RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeamRoot")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_Icon")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_HomeTeam")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_Name")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_Group")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_Times")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_BestScore")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_LastScore")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_SelectTip")
RegistClassMember(LuaWorldCup2022LotterySelectWnd, "homeTeam_CenterTip")

RegistClassMember(LuaWorldCup2022LotterySelectWnd, "lottery_CurSelectId")

function LuaWorldCup2022LotterySelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

function LuaWorldCup2022LotterySelectWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.topLabel = anchor:Find("Top"):GetComponent(typeof(UILabel))
    self.baseRoot = anchor:Find("Base").gameObject
    self.moneyCtrl = anchor:Find("Base/CurrentMoneyCtrl"):GetComponent(typeof(CCurrentMoneyCtrl))
    self.okButton = anchor:Find("Base/OKButton"):GetComponent(typeof(CButton))
    self.lotteryRoot = anchor:Find("Lottery")
    self.homeTeamRoot = anchor:Find("HomeTeam")

    local type = LuaWorldCup2022Mgr.lotterySelectInfo.type
    if type == "lottery" then
        self.lottery_Table = self.lotteryRoot:Find("Table")
        self.lottery_AddSubButton = self.lotteryRoot:Find("AddSubButton"):GetComponent(typeof(QnAddSubAndInputButton))
    else
        self.homeTeam_Icon = self.homeTeamRoot:Find("Icon"):GetComponent(typeof(CUITexture))
        self.homeTeam_HomeTeam = self.homeTeamRoot:Find("Icon/HomeTeam").gameObject
        self.homeTeam_Name = self.homeTeamRoot:Find("Name"):GetComponent(typeof(UILabel))
        self.homeTeam_Group = self.homeTeamRoot:Find("Group"):GetComponent(typeof(UILabel))
        self.homeTeam_Times = self.homeTeamRoot:Find("Times"):GetComponent(typeof(UILabel))
        self.homeTeam_BestScore = self.homeTeamRoot:Find("BestScore"):GetComponent(typeof(UILabel))
        self.homeTeam_LastScore = self.homeTeamRoot:Find("LastScore"):GetComponent(typeof(UILabel))
        self.homeTeam_SelectTip = self.homeTeamRoot:Find("SelectTip"):GetComponent(typeof(UILabel))
        self.homeTeam_CenterTip = self.homeTeamRoot:Find("CenterTip"):GetComponent(typeof(UILabel))
    end
end

function LuaWorldCup2022LotterySelectWnd:InitEventListener()
    local tipButton = self.transform:Find("Anchor/Base/TipButton").gameObject

    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTipButtonClick()
    end)

    UIEventListener.Get(self.okButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOKButtonClick()
    end)
end

function LuaWorldCup2022LotterySelectWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:AddListener("WorldCupJingCai_ReplyTaoTaiInfoResult", self, "ReplyTaoTaiInfoResult")
    g_ScriptEvent:AddListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
end

function LuaWorldCup2022LotterySelectWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
    g_ScriptEvent:RemoveListener("WorldCupJingCai_ReplyTaoTaiInfoResult", self, "ReplyTaoTaiInfoResult")
    g_ScriptEvent:RemoveListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
end

function LuaWorldCup2022LotterySelectWnd:OnMainPlayerUpdateMoney()
    if LuaWorldCup2022Mgr.lotterySelectInfo.type == "homeTeam" then
        self:UpdateHomeTeamSelect()
    end
end

function LuaWorldCup2022LotterySelectWnd:ReplyTaoTaiInfoResult()
    if LuaWorldCup2022Mgr.lotterySelectInfo.type == "homeTeam" then
        self:UpdateHomeTeamSelect()
    end
end

function LuaWorldCup2022LotterySelectWnd:UpdateAllJingCaiData()
    if LuaWorldCup2022Mgr.lotterySelectInfo.type == "homeTeam" then
        self:UpdateHomeTeamSelect()
    end
end


function LuaWorldCup2022LotterySelectWnd:Init()
    local info = LuaWorldCup2022Mgr.lotterySelectInfo
    local type = info.type

    if type == "lottery" then
        self.lotteryRoot.gameObject:SetActive(true)
        self.homeTeamRoot.gameObject:SetActive(false)
        self:InitLotterySelect(info)
    else
        self.lotteryRoot.gameObject:SetActive(false)
        self.homeTeamRoot.gameObject:SetActive(true)
        self:InitHomeTeamSelect(info)
        self:UpdateHomeTeamSelect()
    end
end

function LuaWorldCup2022LotterySelectWnd:InitLotterySelect(info)
    local playId = info.playId
    local roundInfo = LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData[playId]

    local odd = LuaWorldCup2022Mgr.lotteryInfo.odds[playId]
    for i = 0, 2 do
        local child = self.lottery_Table:GetChild(i)
        local normal = child:Find("Normal").gameObject
        local highlight = child:Find("Highlight").gameObject

        normal:SetActive(true)
        highlight:SetActive(false)
        UIEventListener.Get(normal).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnLotteryClick(i)
        end)

        if i ~= 2 then
            local icon = child:Find("Icon"):GetComponent(typeof(CUITexture))
            local bet = child:Find("Bet"):GetComponent(typeof(UILabel))
            local teamId = i == 0 and roundInfo[1] or roundInfo[2]

            local teamData = WorldCup_TeamInfo.GetData(teamId)
            icon:LoadMaterial(teamData.Icon)
            bet.text = SafeStringFormat3(LocalString.GetString("%s 胜"), teamData.Name)
        end

        local option = nil
        if i == 0 then
            option = EnumWorldCupJingCaiVoteOption.HostTeamWin
        elseif i == 1 then
            option = EnumWorldCupJingCaiVoteOption.HostTeamLose
        else
            option = EnumWorldCupJingCaiVoteOption.Draw
        end

        local times = child:Find("Times"):GetComponent(typeof(UILabel))
        times.text = SafeStringFormat3("%.2f", odd and odd[2][option] / 100 or 1)
    end

    self.topLabel.text = LocalString.GetString("请作出你的竞猜选择")
    self.okButton.Text = LocalString.GetString("竞  猜")

    self.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, false)

    local setting = WorldCup_Setting.GetData()
    self.lottery_AddSubButton:SetMinMax(1, setting.MaxTouZhuNum, 1)
    self.lottery_AddSubButton.onValueChanged = DelegateFactory.Action_uint(function (value)
        self.moneyCtrl:SetCost(value * setting.JingCaiYinLiang)
    end)
    self.lottery_AddSubButton:SetValue(1, true)
end

function LuaWorldCup2022LotterySelectWnd:InitHomeTeamSelect(info)
    local teamData = WorldCup_TeamInfo.GetData(info.teamId)

    self.homeTeam_Icon:LoadMaterial(teamData.Icon)
    self.homeTeam_Name.text = teamData.Name
    self.homeTeam_Group.text = SafeStringFormat3(LocalString.GetString("%s组"), teamData.Group)
    self.homeTeam_Times.text = SafeStringFormat3(LocalString.GetString("%d次"), teamData.ParticipateTimes)
    self.homeTeam_BestScore.text = teamData.BestScore
    self.homeTeam_LastScore.text = teamData.LastScore

    self.topLabel.text = LocalString.GetString("根据主队战绩将获得相应奖励")
    self.okButton.Text = LocalString.GetString("设为主队")
end

function LuaWorldCup2022LotterySelectWnd:UpdateHomeTeamSelect()
    local setHomeTeamEndTime = CServerTimeMgr.Inst:GetTimeStampByStr(WorldCup_Setting.GetData().SetHomeTeamDeadLine)
    local isSetHomeTeamEnd = CServerTimeMgr.Inst.timeStamp >= setHomeTeamEndTime

    local jingCaiData = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData
    local setTimes = jingCaiData.m_SetTimes
    local teamId = LuaWorldCup2022Mgr.lotterySelectInfo.teamId
    local isHomeTeam = jingCaiData.m_HomeTeamId == teamId
    local taoTaiInfo = LuaWorldCup2022Mgr.lotteryInfo.taoTaiInfo
    local isTaoTai = taoTaiInfo and taoTaiInfo[teamId] and taoTaiInfo[teamId] > 0
    LuaUtils.SetLocalPositionZ(self.homeTeam_Icon.transform, isTaoTai and -1 or 0)

    self.homeTeam_HomeTeam:SetActive(isHomeTeam)
    if isHomeTeam or isSetHomeTeamEnd or setTimes >= 2 or isTaoTai then
        self.baseRoot:SetActive(false)
        self.homeTeam_SelectTip.gameObject:SetActive(false)
        self.homeTeam_CenterTip.gameObject:SetActive(true)

        if isHomeTeam then
            self.homeTeam_CenterTip.text = SafeStringFormat3(LocalString.GetString("%s是你的主队"), WorldCup_TeamInfo.GetData(teamId).Name)
        elseif isSetHomeTeamEnd then
            self.homeTeam_CenterTip.text = LocalString.GetString("小组赛已结束\n无法再更换主队")
        elseif setTimes >= 2 then
            self.homeTeam_CenterTip.text = LocalString.GetString("无更换主队次数")
        else
            self.homeTeam_CenterTip.text = LocalString.GetString("该队伍在小组赛被淘汰")
        end
    else
        self.baseRoot:SetActive(true)
        self.homeTeam_SelectTip.gameObject:SetActive(true)
        self.homeTeam_CenterTip.gameObject:SetActive(false)

        self.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, false)
        if setTimes == 0 then
            self.homeTeam_SelectTip.text = LocalString.GetString("仅初次选择主队免费")
            self.moneyCtrl:SetCost(0)
        else
            self.homeTeam_SelectTip.text = LocalString.GetString("小组赛结束前可变更主队\n(仅限一次)")
            self.moneyCtrl:SetCost(WorldCup_Setting.GetData().SetHomeTeamNeedSilver)
        end
    end
end

--@region UIEvent

function LuaWorldCup2022LotterySelectWnd:OnOKButtonClick()
    local info = LuaWorldCup2022Mgr.lotterySelectInfo
    local type = info.type

    if type == "lottery" then
        if not self.lottery_CurSelectId then
            g_MessageMgr:ShowMessage("WORLDCUP2022_LOTTERY_NEED_SELECT_FIRST")
            return
        end

        if not self.moneyCtrl.moneyEnough then
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
            return
        end

        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("WORLDCUP2022_LOTTERY_TOUZHU_CONFIRM", self.moneyCtrl:GetCost()), function()
            local value = nil
            if self.lottery_CurSelectId == 0 then
                value = EnumWorldCupJingCaiVoteOption.HostTeamWin
            elseif self.lottery_CurSelectId == 1 then
                value = EnumWorldCupJingCaiVoteOption.HostTeamLose
            elseif self.lottery_CurSelectId == 2 then
                value = EnumWorldCupJingCaiVoteOption.Draw
            end
            Gac2Gas.RequestWorldCupTodayJingCaiVote(info.playId, value, self.lottery_AddSubButton:GetValue())
        end, nil, nil, nil, false)
    else
        if not self.moneyCtrl.moneyEnough then
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
            return
        end

        local jingCaiData = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData
        local setTimes = jingCaiData.m_SetTimes
        local teamId = LuaWorldCup2022Mgr.lotterySelectInfo.teamId
        local name = WorldCup_TeamInfo.GetData(teamId).Name

        local str = setTimes == 0 and "WORLDCUP2022_HOMETEAM_SET_CONFIRM" or "WORLDCUP2022_HOMETEAM_CHANGE_CONFIRM"
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage(str, name), function()
            Gac2Gas.RequestSetWorldCupHomeTeam(teamId)
        end, nil, nil, nil, false)
    end
end

function LuaWorldCup2022LotterySelectWnd:OnTipButtonClick()
    local type = LuaWorldCup2022Mgr.lotterySelectInfo.type

    if type == "lottery" then
        g_MessageMgr:ShowMessage("WORLDCUP2022_LOTTERY_SELECT_TIP")
    else
        g_MessageMgr:ShowMessage("WORLDCUP2022_HOMETEAM_SELECT_TIP")
    end
end

function LuaWorldCup2022LotterySelectWnd:OnLotteryClick(i)
    if self.lottery_CurSelectId then
        local before = self.lottery_Table:GetChild(self.lottery_CurSelectId)
        before:Find("Normal").gameObject:SetActive(true)
        before:Find("Highlight").gameObject:SetActive(false)
    end

    self.lottery_CurSelectId = i
    local child = self.lottery_Table:GetChild(self.lottery_CurSelectId)
    child:Find("Normal").gameObject:SetActive(false)
    child:Find("Highlight").gameObject:SetActive(true)
end

--@endregion UIEvent
