local CTaskQuizWnd = import "L10.UI.CTaskQuizWnd"
local CQuizMgr = import "L10.Game.CQuizMgr"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Random = import "UnityEngine.Random"

local questionWidthPic = 686
local questionWidthNoPic = 800
local answerWidthPic = 255
local answerWidthNoPic = 352

CTaskQuizWnd.m_hookInit = function(this)
    local questionTex = this.questionLabel.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    this.choiceTmplate.gameObject:SetActive(false)

    this.queryJingLingBtn:SetActive(GameSetting_Common_Wapper.Inst.JingLingEnabled and EnumToInt(CQuizMgr.Inst.TaskQuestionInfo.type) == 0)
    this.queryJingLingBtn.transform.parent:Find("TipLabel"):GetComponent(typeof(UILabel)).text = ""--g_MessageMgr:FormatMessage("BAOLINGDAN_TIP")
    Extensions.RemoveAllChildren(this.choiceTable.transform)
    
    local question = Question_Task.GetData(CQuizMgr.Inst.TaskQuestionInfo.questionId)
    local msg = question.Question--.."<pic=UI/Texture/Transparent/Material/guanningcommandwnd_zengyifazhen.mat>"
    local pre, pic, post = string.match(msg, "(.*)<pic=(.*)>(.*)")
    pre = pre or ""
    post = post or ""
    msg = pic and pre .. post or msg

    this.questionLabel.text = CUICommonDef.TranslateToNGUIText(msg) 
    this.questionText = msg
    if pic then
        this.questionLabel.width = questionWidthPic
        questionTex.gameObject:SetActive(true)
        questionTex:LoadMaterial(pic)

        this.questionLabel:UpdateNGUIText()
        NGUIText.regionWidth = 1000000
        local size = NGUIText.CalculatePrintedSize(this.questionLabel.text)
        LuaUtils.SetLocalPositionX(questionTex.transform, math.min(math.ceil(size.x), questionWidthPic + 2))
    else
        this.questionLabel.width = questionWidthNoPic
        questionTex.gameObject:SetActive(false)
    end

    local task = Task_Task.GetData(CQuizMgr.Inst.TaskQuestionInfo.taskId)
    local solveStr = function(replaceString)
        local result = ""
        for i = 1, #replaceString do
            local currentChar = replaceString:sub(i, i)
            if currentChar == LocalString.GetString("（") then
                result = result .. "("
            elseif currentChar == LocalString.GetString("）") then
                result = result .. ")"
            else
                result = result .. currentChar
            end
        end
        return result
    end
    
    this.titleLabel.text = solveStr(task.Display)
    local choices = {}
    table.insert(choices, question.Right)
    table.insert(choices, question.Wrong1)
    table.insert(choices, question.Wrong2)
    table.insert(choices, question.Wrong3)
    local idx = 1
    local op = { "A", "B", "C", "D" }
    while #choices > 0 do
        local i = math.floor(Random.Range(0, #choices)) + 1
        local instance = NGUITools.AddChild(this.choiceTable.gameObject, this.choiceTmplate)
        instance:SetActive(true)
        local label = instance.transform:Find("Label"):GetComponent(typeof(UILabel))
        local tex = instance.transform:Find("Texture"):GetComponent(typeof(CUITexture))
        instance.transform:Find("Mark/Label"):GetComponent(typeof(UILabel)).text = op[idx]
        idx = idx + 1

        msg = choices[i]--.."<pic=UI/Texture/Transparent/Material/guanningcommandwnd_zengyifazhen.mat>"
        pre, pic, post = string.match(msg, "(.*)<pic=(.*)>(.*)")
        pre = pre or ""
        post = post or ""
        msg = pic and pre .. post or msg
        
        label.text = CUICommonDef.TranslateToNGUIText(msg)
        
        if pic then
            label.width = answerWidthPic
            tex.gameObject:SetActive(true)
            tex:LoadMaterial(pic)
        else
            local offset = (answerWidthNoPic - answerWidthPic)-- / 2
            label.width = answerWidthNoPic
            LuaUtils.SetLocalPositionX(label.transform, label.transform.localPosition.x - offset)
            tex.gameObject:SetActive(false)
        end

        if question.Right == choices[i] then
            this.rightAnswer = instance
            UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(
                UIEventListener.Get(instance).onClick,
                DelegateFactory.VoidDelegate(
                    function(go)
                        this:OnRightAnswerClick(go)
                    end
                ),
                true
            )
        else
            UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(
                UIEventListener.Get(instance).onClick,
                DelegateFactory.VoidDelegate(
                    function(go)
                        this:OnWrongAnswerClick(go)
                    end
                ),
                true
            )
        end
        table.remove(choices, i)
    end
    this.choiceTable:Reposition()
    --if CQuizMgr.Inst.TaskQuestionInfo.type == CQuizMgr.EnumQuizType.MultiQuiz or CQuizMgr.Inst.TaskQuestionInfo.type == CQuizMgr.EnumQuizType.ContinuousQuiz then
    --    this.showAreaWidget.height = this.otherHeight
    --    local rects = CommonDefs.GetComponentsInChildren_Component_Type(this.showAreaWidget, typeof(UIWidget))
    --    for i = 0, rects.Length - 1 do
    --        rects[i]:ResetAndUpdateAnchors()
    --    end
    --end
end
