local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnTableView = import "L10.UI.QnTableView"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local UIGrid = import "UIGrid"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItem = import "L10.Game.CItem"
local CChatLinkMgr = import "CChatLinkMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local Money = import "L10.Game.Money"

LuaColiseumShopWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaColiseumShopWnd, "ItemIcon", "ItemIcon", CUITexture)
RegistChildComponent(LuaColiseumShopWnd, "ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaColiseumShopWnd, "ItemDescLabel", "ItemDescLabel", UILabel)
RegistChildComponent(LuaColiseumShopWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaColiseumShopWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaColiseumShopWnd, "BuyBtn", "BuyBtn", CButton)
RegistChildComponent(LuaColiseumShopWnd, "GoodsTableView", "GoodsTableView", QnTableView)
RegistChildComponent(LuaColiseumShopWnd, "RefreshCountDownLabel", "RefreshCountDownLabel", UILabel)
RegistChildComponent(LuaColiseumShopWnd, "ScoreCostLabel", "ScoreCostLabel", UILabel)
RegistChildComponent(LuaColiseumShopWnd, "OwnScoreLabel", "OwnScoreLabel", UILabel)
RegistChildComponent(LuaColiseumShopWnd, "RefreshButton", "RefreshButton", GameObject)
RegistChildComponent(LuaColiseumShopWnd, "LeftView", "LeftView", UITexture)
RegistChildComponent(LuaColiseumShopWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaColiseumShopWnd, "ResetCountDownLabel", "ResetCountDownLabel", UILabel)
RegistChildComponent(LuaColiseumShopWnd, "LeftViewShopGoodsItemTemplate", "LeftViewShopGoodsItemTemplate", GameObject)
RegistChildComponent(LuaColiseumShopWnd, "LeftViewGrid", "LeftViewGrid", UIGrid)
RegistChildComponent(LuaColiseumShopWnd, "ItemZhenpinSprite", "ItemZhenpinSprite", GameObject)
RegistChildComponent(LuaColiseumShopWnd, "ItemBorder", "ItemBorder", UISprite)
RegistChildComponent(LuaColiseumShopWnd, "DetailView", "DetailView", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaColiseumShopWnd,"m_ShopGoodsData")
RegistClassMember(LuaColiseumShopWnd,"m_RandomZhenpinGoodsData")
RegistClassMember(LuaColiseumShopWnd,"m_LeftViewInitialHeight")
RegistClassMember(LuaColiseumShopWnd,"m_PlayShopScoreData")
RegistClassMember(LuaColiseumShopWnd,"m_ZhenpinItemIds")
RegistClassMember(LuaColiseumShopWnd,"m_AutoResetTime")
RegistClassMember(LuaColiseumShopWnd,"m_AutoRefreshTime")
RegistClassMember(LuaColiseumShopWnd,"m_AllShopGoodItemIds")
RegistClassMember(LuaColiseumShopWnd,"m_PriceType2EnumMoneyType")
RegistClassMember(LuaColiseumShopWnd,"m_SelectRow")
RegistClassMember(LuaColiseumShopWnd,"m_UpdateTimeTick")
RegistClassMember(LuaColiseumShopWnd,"m_ShopScore")
RegistClassMember(LuaColiseumShopWnd,"m_CostIsEnough")
RegistClassMember(LuaColiseumShopWnd,"m_HasZhenpin")
function LuaColiseumShopWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick()
	end)


	
	UIEventListener.Get(self.RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	UIEventListener.Get(self.OwnScoreLabel.transform:Find("AddBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddScoreMoneyClick()
	end)
    --@endregion EventBind end
end

function LuaColiseumShopWnd:Init()
	self.m_LeftViewInitialHeight = self.LeftView.height
	self.ScoreCostLabel.text = Arena_Shop_Setting.GetData().ManualRefreshPrice
	self.OwnScoreLabel.text = 0
	self.QnCostAndOwnMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	self.m_ShopGoodsData = {}
	self.m_ZhenpinItemIds = {}
	self.DetailView.gameObject:SetActive(false)
	self.GoodsTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self:NumOfRow()
        end,
        function(item,index) self:InitItem(item,index) end
    )
	self.GoodsTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
	self.LeftViewShopGoodsItemTemplate.gameObject:SetActive(false)

	Gac2Gas.RequestOpenArenaScoreShop()
end

function LuaColiseumShopWnd:GetResetCountDownTime()
	local DateFields = {}
    local DateStringFields = {}
	for DateFieldString in string.gmatch(Arena_Shop_Setting.GetData().RareGoodsLimitRefreshCron, "([%d*,/-]+)") do
		table.insert(DateStringFields, DateFieldString)
	end
	local hour = tonumber(DateStringFields[2])
	local dayWeek = tonumber(DateStringFields[5])
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local resetTime = DateTime(now.Year,now.Month,now.Day,hour,0,0)
	while EnumToInt(resetTime.DayOfWeek) ~= dayWeek or resetTime:Subtract(now).TotalSeconds <= 0 do
		resetTime = resetTime:AddDays(1)
	end
	self.m_AutoResetTime = resetTime
	return (resetTime:Subtract(now).TotalSeconds)
    -- DateFields[1] = self:PreParseDateField(DateStringFields[1], 0, 59)
	-- DateFields[2] = self:PreParseDateField(DateStringFields[2], 0, 23)
	-- DateFields[3] = self:PreParseDateField(DateStringFields[3], 1, 31)
	-- DateFields[4] = self:PreParseDateField(DateStringFields[4], 1, 12)
	-- DateFields[5] = self:PreParseDateField(DateStringFields[5], 0, 6)
    -- DateFields[6] = self:PreParseDateField(DateStringFields[6] and DateStringFields[6] or now.Year, 1970, 3000)
end

-- function LuaColiseumShopWnd:PreParseDateField(DateFieldString, RestrictLowerBound, RestrictUpperBound)
-- 	local Fields = {}
-- 	for s in string.gmatch(DateFieldString, "([%d*/-]+),*") do
-- 		for	ss1, ss2, ss3 in string.gmatch(s, "([%d*]+)-*(%d*)/*(%d*)") do			
-- 			local LowerBound, UpperBound, Interval
-- 			if ss1 == "*" then
-- 				LowerBound, UpperBound = RestrictLowerBound, RestrictUpperBound
-- 			else
-- 				LowerBound = tonumber(ss1) or RestrictLowerBound
-- 				UpperBound = tonumber(ss2) or tonumber(ss1) or RestrictUpperBound
-- 			end
-- 			Interval = tonumber(ss3) or 1
-- 			table.insert(Fields, {
-- 				LowerBound = LowerBound,
-- 				UpperBound = UpperBound,
-- 				Interval = Interval,
-- 			})
-- 			print(LowerBound, UpperBound, Interval)
-- 		end
-- 	end
-- 	return Fields
-- end

function LuaColiseumShopWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendArenaScoreShopData", self, "OnSendArenaScoreShopData")
end

function LuaColiseumShopWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendArenaScoreShopData", self, "OnSendArenaScoreShopData")
	self:CancelUpdateTimeTick()
end

function LuaColiseumShopWnd:OnSendArenaScoreShopData(shopScore, nextAutoRefreshTime, limitGoodsArray, goodsArray)
	self.OwnScoreLabel.text = shopScore
	self.m_ShopScore = shopScore
	self.DetailView.gameObject:SetActive(true)
	self:GetResetCountDownTime()
	self.m_AutoRefreshTime = nextAutoRefreshTime
	self:UpdateCountDownTime(self:GetResetCountDownTime(), nextAutoRefreshTime - CServerTimeMgr.Inst.timeStamp)
	self:CancelUpdateTimeTick()
	self.m_UpdateTimeTick = RegisterTick(function ()
		self:UpdateTimeLabel()
	end, 500)
	self.m_ShopGoodsData = goodsArray
	self.m_AllShopGoodItemIds = {}
	for k, data in pairs(self.m_ShopGoodsData) do
		self.m_AllShopGoodItemIds[data.shopData.ItemId] = true
	end
	self.m_RandomZhenpinGoodsData = limitGoodsArray
	self:LoadRandomZhenpin()
	self:LoadShopGoods()
end

function LuaColiseumShopWnd:CancelUpdateTimeTick()
	if self.m_UpdateTimeTick then
		UnRegisterTick(self.m_UpdateTimeTick)
		self.m_UpdateTimeTick = nil
	end
end

function LuaColiseumShopWnd:NumOfRow()
	return #self.m_ShopGoodsData
end

function LuaColiseumShopWnd:InitItem(item,index)
	local data = self.m_ShopGoodsData[index + 1]
	local goodsId = data.goodsId
	local shopData = data.shopData
	local itemId = shopData.ItemId
	local itemData = Item_Item.GetData(itemId)
	local isSellOut = data.state ~= 0
	local moneyType = self.m_PriceType2EnumMoneyType[data.shopData.PriceType]
	local moneySpriteName = Money.GetIconName(CommonDefs.ConvertIntToEnum(typeof(EnumMoneyType), moneyType), EnumPlayScoreKey.NONE)

	local iconTexture = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local borderSprite = item.transform:Find("Icon/Border"):GetComponent(typeof(UISprite))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local moneySprite = item.transform:Find("MoneySprite"):GetComponent(typeof(UISprite))
	local moneySprite2 = item.transform:Find("MoneySprite2")
	local priceLabel = item.transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
	local sellOutTag = item.transform:Find("GlobalSellOutTag").gameObject
	local zhenpinTag = item.transform:Find("ZhenpinSprite").gameObject
	local fx = item.transform:Find("Icon/Fx"):GetComponent(typeof(CUIFx))

	iconTexture:LoadMaterial(itemData.Icon)
	borderSprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemId))
	nameLabel.text = itemData.Name
	priceLabel.text = data.shopData.Price
	moneySprite.spriteName = moneySpriteName 
	moneySprite.gameObject:SetActive(moneySpriteName ~= nil)
	moneySprite2.gameObject:SetActive(moneySpriteName == nil)
	sellOutTag.gameObject:SetActive(isSellOut)
	zhenpinTag.gameObject:SetActive(self.m_ZhenpinItemIds[itemId])
	if self.m_ZhenpinItemIds[itemId] and not isSellOut then
		self.m_HasZhenpin = true
	end
	fx:LoadFx("Fx/UI/Prefab/UI_meirichoujiang_liuguang.prefab")
	fx.gameObject:SetActive(self.m_ZhenpinItemIds[itemId])
	UIEventListener.Get(iconTexture.gameObject).onClick = DelegateFactory.VoidDelegate(
        function (p) 
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
        end
    )
	Extensions.SetLocalPositionZ(iconTexture.transform,isSellOut and -1 or 0)
	Extensions.SetLocalPositionZ(sellOutTag.transform,isSellOut and -1 or 0)
end

function LuaColiseumShopWnd:OnSelectAtRow(row)
	self.m_SelectRow = row
	local data = self.m_ShopGoodsData[row + 1]
	local itemId = data.shopData.ItemId
	local itemData = Item_Item.GetData(itemId)
	local moneyType = self.m_PriceType2EnumMoneyType[data.shopData.PriceType]
	local isSellOut = data.state ~= 0

	self.ItemIcon:LoadMaterial(itemData.Icon)
	self.ItemNameLabel.text = itemData.Name
	self.ItemBorder.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemId))
	self.ItemDescLabel.text = CChatLinkMgr.TranslateToNGUIText(CItem.GetItemDescription(itemId,true))
	self.ItemZhenpinSprite.gameObject:SetActive(self.m_ZhenpinItemIds[itemId])
	self.QnCostAndOwnMoney.gameObject:SetActive(true)
	self.QnCostAndOwnMoney:SetCost(data.shopData.Price)
	self.QnCostAndOwnMoney.enabled = data.shopData.PriceType ~= 1
	self.BuyBtn.Enabled = not isSellOut
	self.QnCostAndOwnMoney.m_OwnIcon.gameObject:SetActive(data.shopData.PriceType ~= 1)
	self.QnCostAndOwnMoney.m_CostIcon.gameObject:SetActive(data.shopData.PriceType ~= 1)
	self.QnCostAndOwnMoney.transform:Find("Cost/Sprite2").gameObject:SetActive(data.shopData.PriceType == 1)
	self.QnCostAndOwnMoney.transform:Find("Own/Sprite2").gameObject:SetActive(data.shopData.PriceType == 1)
	if data.shopData.PriceType == 1 then
		self.QnCostAndOwnMoney.m_OwnLabel.text = self.OwnScoreLabel.text
		local costIsEnough = data.shopData.Price <= self.m_ShopScore
		self.QnCostAndOwnMoney.m_OwnLabel.color = costIsEnough and Color.white or NGUIText.ParseColor24("ff5050", 0)
		self.m_CostIsEnough = costIsEnough
		self.BuyBtn.Enabled = not isSellOut and costIsEnough
		self.QnCostAndOwnMoney.m_AddMoneyButton.OnClick = DelegateFactory.Action_QnButton(function (go)
			self:OnAddScoreMoneyClick()
		end)
	else
		self.QnCostAndOwnMoney:SetType(moneyType, 0, true)
	end
end

function LuaColiseumShopWnd:LoadShopGoods()
	table.sort(self.m_ShopGoodsData,function (a,b)
		if self.m_ZhenpinItemIds[a.shopData.ItemId] and not self.m_ZhenpinItemIds[b.shopData.ItemId] then
			return true
		elseif self.m_ZhenpinItemIds[b.shopData.ItemId] and not self.m_ZhenpinItemIds[a.shopData.ItemId] then
			return false
		end
		return a.goodsId < b.goodsId
	end)
	self.m_PriceType2EnumMoneyType = {
		[1] = 5,[2] = 4,[3] = 3,[4] = 1
	}
	self.m_HasZhenpin = false
	self.GoodsTableView:ReloadData(true, true)
	self.GoodsTableView:SetSelectRow(0,true)
end

function LuaColiseumShopWnd:LoadRandomZhenpin()
	self.m_ZhenpinItemIds = {}
	self.LeftViewShopGoodsItemTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.LeftViewGrid.transform)
	for k,data in pairs(self.m_RandomZhenpinGoodsData) do
		local item = NGUITools.AddChild(self.LeftViewGrid.gameObject,self.LeftViewShopGoodsItemTemplate.gameObject)
		item:SetActive(true)

		local iconTexture = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
		local borderSprite = item.transform:Find("Icon/Border"):GetComponent(typeof(UISprite))
		local zhenpinTag = item.transform:Find("Icon/ZhenpinSprite").gameObject
		local leaveLabel = item.transform:Find("LeaveLabel"):GetComponent(typeof(UILabel))
		local ariseMark = item.transform:Find("Icon/AriseMark").gameObject
		local sellOutTag = item.transform:Find("Icon/GlobalSellOutTag").gameObject

		local goodsId = data.goodsId
		local shopData = data.shopData
		local itemId = shopData.ItemId
		local itemData = Item_Item.GetData(itemId)
		local remainCount = data.remainCount
		self.m_ZhenpinItemIds[itemId] = true
		Extensions.SetLocalPositionZ(iconTexture.transform,(remainCount == 0) and -1 or 0)
		iconTexture:LoadMaterial(itemData.Icon)
		borderSprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemId))
		leaveLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"),remainCount) 
		ariseMark.gameObject:SetActive(self.m_AllShopGoodItemIds[itemId])
		sellOutTag.gameObject:SetActive(data.remainCount == 0)
		UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(
			function (p) 
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
			end
		)
		zhenpinTag.gameObject:SetActive(true)
	end
	self.LeftViewGrid:Reposition()
	self.LeftView.height = self.m_LeftViewInitialHeight + (#self.m_RandomZhenpinGoodsData - 4) * self.LeftViewGrid.cellHeight
end

function LuaColiseumShopWnd:UpdateCountDownTime(resetCountDownTime, refreshCountDownTime)
	if resetCountDownTime <= 0 or refreshCountDownTime <= 0 then
		Gac2Gas.RequestOpenArenaScoreShop()
		return
	end
	self.ResetCountDownLabel.text = (resetCountDownTime > 86400) and SafeStringFormat3(LocalString.GetString("%d天"),math.floor(resetCountDownTime / 86400)) or
		SafeStringFormat3("%d:%d:%d",math.floor(resetCountDownTime / 3600),math.floor(resetCountDownTime % 3600 / 60),resetCountDownTime % 60)
	self.RefreshCountDownLabel.text = CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3(LocalString.GetString("[00FF00]%02d:%02d:%02d[FFFFFF]后"),
		math.floor(refreshCountDownTime / 3600), math.floor(refreshCountDownTime % 3600 / 60),refreshCountDownTime % 60))
end

function LuaColiseumShopWnd:UpdateTimeLabel()
	local now = CServerTimeMgr.Inst:GetZone8Time()
	self:UpdateCountDownTime(self.m_AutoResetTime:Subtract(now).TotalSeconds, self.m_AutoRefreshTime - CServerTimeMgr.Inst.timeStamp)
end


--@region UIEvent

function LuaColiseumShopWnd:OnRefreshButtonClick()
	if self.m_HasZhenpin then
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ColiseumShopWnd_RefresConfirm"),DelegateFactory.Action(function()
			Gac2Gas.RequestRefreshArenaShopManually()
		end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
		return
	end
	Gac2Gas.RequestRefreshArenaShopManually()
end

function LuaColiseumShopWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ColiseumShopWnd_ReadMe")
end

function LuaColiseumShopWnd:OnBuyBtnClick()
	local data = self.m_ShopGoodsData[self.m_SelectRow + 1]
	Gac2Gas.RequestBuyFromArenaScoreShop(data.goodsId)
end

function LuaColiseumShopWnd:OnAddScoreMoneyClick()
	local data = self.m_ShopGoodsData[self.m_SelectRow + 1]
	if data.shopData.PriceType ~= 1 then return end
	g_MessageMgr:ShowMessage("ColiseumShopScore_ItemGet_ReadMe")
end
--@endregion UIEvent

