local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local UIGrid = import "UIGrid"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaPengDaoDialogWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaPengDaoDialogWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaPengDaoDialogWnd, "StartGameButton", "StartGameButton", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "DevelopButton", "DevelopButton", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "TuJianButton", "TuJianButton", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "BgTexture", "BgTexture", CUITexture)
--RegistChildComponent(LuaPengDaoDialogWnd, "ThemeLabel", "ThemeLabel", UILabel)
RegistChildComponent(LuaPengDaoDialogWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaPengDaoDialogWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaPengDaoDialogWnd, "ShopButton", "ShopButton", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaPengDaoDialogWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "DetailButton", "DetailButton", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "RewardItem", "RewardItem", GameObject)
RegistChildComponent(LuaPengDaoDialogWnd, "Top3RewardViewGrid", "Top3RewardViewGrid", UIGrid)
RegistChildComponent(LuaPengDaoDialogWnd, "TuJianButtonRedDot", "TuJianButtonRedDot", GameObject)
--@endregion RegistChildComponent end

function LuaPengDaoDialogWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.StartGameButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartGameButtonClick()
	end)

	UIEventListener.Get(self.DevelopButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDevelopButtonClick()
	end)

	UIEventListener.Get(self.TuJianButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTuJianButtonClick()
	end)

	UIEventListener.Get(self.ShopButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShopButtonClick()
	end)

	UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

	UIEventListener.Get(self.DetailButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailButtonClick()
	end)

    --@endregion EventBind end
end

function LuaPengDaoDialogWnd:Init()
	self.ReadMeLabel.text = g_MessageMgr:FormatMessage("PengDaoDialogWnd_ReadMe")
	--self.ThemeLabel.text = PengDaoFuYao_Setting.GetData().PengDaoFuYaoSubtitle
	self.BgTexture:LoadMaterial(PengDaoFuYao_Setting.GetData().PengDaoFuYaoBackgroundPicture)
	local selfData = LuaPengDaoMgr.m_MainWndData.selfData
	self.ScoreLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.PengDaoFuYao] or 0
	local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	local rank = LuaPengDaoMgr.m_MainWndData.playerId2RankMap[myId]
	self.RankLabel.text = rank and rank or LocalString.GetString("未上榜")
	self:InitRewardView()
	self:UpdateCountDownLabel()
	if selfData and selfData.Score > 0 then
		CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.PengDaoDevelopWnd)
	else
		CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.PengDaoSelectDifficultyWnd)
	end
	self:InitTuJianButtonRedDot()

	if CommonDefs.IS_VN_CLIENT then
		self.RankLabel.transform.parent:GetComponent(typeof(UILabel)).fontSize = 26
	end
end

function LuaPengDaoDialogWnd:InitTuJianButtonRedDot()
	local isShow = false
	PengDaoFuYao_MonsterList.ForeachKey(function (k)
		if isShow == false then
			isShow = isShow or LuaPengDaoMgr:GetMonsterRewarded(k)
		end
	end)
	self.TuJianButtonRedDot.gameObject:SetActive(isShow)
end

function LuaPengDaoDialogWnd:InitRewardView()
	self.RewardItem.gameObject:SetActive(false)
	local awardData = LuaPengDaoMgr:GetAwardItemData()
	local rewardData = LuaPengDaoMgr.m_MainWndData.rewardData
	local arr = {}
	if rewardData then
		for i = 1,#awardData do
			local id = awardData[i].Id
			local status = rewardData[id]
			if status ~= 1 and status ~= 2 then
				table.insert(arr, awardData[i])
			end
		end
		Extensions.RemoveAllChildren(self.Top3RewardViewGrid.transform)
		for i = 1, math.min(3,#arr) do
			local obj = NGUITools.AddChild(self.Top3RewardViewGrid.gameObject,self.RewardItem.gameObject)
			obj.gameObject:SetActive(true)
			local icon = obj.transform:Find("Icon"):GetComponent(typeof(CUITexture))
			local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))

			local data = arr[i]
			local itemData = Item_Item.GetData(data.ItemId)
			icon:LoadMaterial(itemData.Icon)
			if data.Difficulty > 3 then
				label.text = SafeStringFormat3(LocalString.GetString("通过第%d层\n且难度之和≥%d"), data.Stage, data.Difficulty)
			else
				label.text = SafeStringFormat3(LocalString.GetString("通过第%d层"), data.Stage)
			end
			UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(data.ItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end)
		end
		self.Top3RewardViewGrid:Reposition()
	end
end

function LuaPengDaoDialogWnd:OnEnable()
	g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate",self,"InitScoreLabel")
	g_ScriptEvent:AddListener("PDFY_RequestResetSkillResult",self,"InitScoreLabel")
	g_ScriptEvent:AddListener("PDFY_GetMonsterRewardSuccess",self,"InitTuJianButtonRedDot")
	g_ScriptEvent:AddListener("PDFY_RequestMonsterAllRewardResult",self,"InitTuJianButtonRedDot")
end

function LuaPengDaoDialogWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate",self,"InitScoreLabel")
	g_ScriptEvent:RemoveListener("PDFY_RequestResetSkillResult",self,"InitScoreLabel")
	g_ScriptEvent:RemoveListener("PDFY_GetMonsterRewardSuccess",self,"InitTuJianButtonRedDot")
	g_ScriptEvent:RemoveListener("PDFY_RequestMonsterAllRewardResult",self,"InitTuJianButtonRedDot")
end

function LuaPengDaoDialogWnd:InitScoreLabel()
	if not CClientMainPlayer.Inst then return end
	self.ScoreLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.PengDaoFuYao] or 0
end

--@region UIEvent

function LuaPengDaoDialogWnd:UpdateCountDownLabel()
	local seasonData = PengDaoFuYao_Season.GetData(LuaPengDaoMgr.m_SeasonId)
	self.CountDownLabel.text =  ""
	if seasonData then
		local refreshTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst:GetTimeStampByStr(seasonData.EndTime))
		local now = CServerTimeMgr.Inst:GetZone8Time()
		local dayDiff =  CServerTimeMgr.Inst:DayDiff(refreshTime,now)
		self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d天后刷新"),dayDiff)
	end
end
--@region UIEvent

function LuaPengDaoDialogWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("PengDaoDialogWnd_Tip")
end

function LuaPengDaoDialogWnd:OnStartGameButtonClick()
	CUIManager.ShowUI(CLuaUIResources.PengDaoSelectDifficultyWnd)
end

function LuaPengDaoDialogWnd:OnDevelopButtonClick()
	CUIManager.ShowUI(CLuaUIResources.PengDaoDevelopWnd)
end

function LuaPengDaoDialogWnd:OnTuJianButtonClick()
	CUIManager.ShowUI(CLuaUIResources.PengDaoTuJianWnd)
end

function LuaPengDaoDialogWnd:OnShopButtonClick()
	CLuaNPCShopInfoMgr.ShowScoreShopById(PengDaoFuYao_Setting.GetData().PengDaoFuYaoShopId)
end

function LuaPengDaoDialogWnd:OnRankButtonClick()
	CUIManager.ShowUI(CLuaUIResources.PengDaoRankWnd)
end

function LuaPengDaoDialogWnd:OnDetailButtonClick()
	CUIManager.ShowUI(CLuaUIResources.PengDaoRewardsListWnd)
end

--@endregion UIEvent

function LuaPengDaoDialogWnd:GetGuideGo( methodName )
	if methodName == "GetStartGameButton" then
		return self.StartGameButton.gameObject
	elseif methodName == "GetDevelopButton" then
		return self.DevelopButton.gameObject
	end
	return nil
end