-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGCSituationPlayerTemplate = import "L10.UI.CGCSituationPlayerTemplate"
local Color = import "UnityEngine.Color"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
CGCSituationPlayerTemplate.m_UpdateData_CS2LuaHook = function (this, info, row, blue) 
    this.levelLabel.text = System.String.Format(LocalString.GetString("{0}级"), info.playerLevel)
    this.nameLabel.text = info.playerName
    this.killNumLabel.text = tostring(info.killNum)
    this.killedNumLabel.text = tostring(info.killedNum)
    this.killPointLabel.text = tostring(info.killPoint)

    local default
    if row % 2 == 1 then
        default = this.grayBackground
    else
        default = this.transparentBackground
    end
    this.background.spriteName = default

    local color = Color.white
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Name == info.playerName then
        color = Color.green
    elseif blue then
        color = row % 2 == 0 and NGUIText.ParseColor(this.blueColor, 0) or NGUIText.ParseColor(this.darkBlueColor, 0)
    else
        color = row % 2 == 0 and NGUIText.ParseColor(this.redColor, 0) or NGUIText.ParseColor(this.darkRedColor, 0)
    end
    this:InitColor(color)
end
CGCSituationPlayerTemplate.m_InitColor_CS2LuaHook = function (this, color) 
    this.levelLabel.color = color
    this.nameLabel.color = color
    this.killedNumLabel.color = color
    this.killNumLabel.color = color
    this.killPointLabel.color = color
end
