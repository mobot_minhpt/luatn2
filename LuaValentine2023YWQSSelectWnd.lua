local DelegateFactory = import "DelegateFactory"
local CButton         = import "L10.UI.CButton"

LuaValentine2023YWQSSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023YWQSSelectWnd, "curId")
RegistClassMember(LuaValentine2023YWQSSelectWnd, "grid")
RegistClassMember(LuaValentine2023YWQSSelectWnd, "taskList")

RegistClassMember(LuaValentine2023YWQSSelectWnd, "taskId")

function LuaValentine2023YWQSSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local content = self.transform:Find("Bg/Content")
    local top = content:Find("Top"):GetComponent(typeof(UILabel))

    local isExpression = LuaValentine2023Mgr.YWQSInfo.selectType == "expression"
    top.text = isExpression and LocalString.GetString("选择表情后前往公共场景互动") or LocalString.GetString("请选择一个约会的场景")

    local leftTimes = content:Find("LeftTimes"):GetComponent(typeof(UILabel))
    if LuaValentine2023Mgr.YWQSInfo.awardCount > 0 then
        leftTimes.text = SafeStringFormat3(LocalString.GetString("[00FF60]剩余奖励次数 %d[-]"), LuaValentine2023Mgr.YWQSInfo.awardCount)
    else
        leftTimes.text = LocalString.GetString("[FF5050]今日奖励次数已用完[-]")
    end

    self.grid = content:Find("Grid"):GetComponent(typeof(UIGrid))
    local template = content:Find("Template").gameObject
    template:SetActive(false)

    self.taskList = {}
    local setting = Valentine2023_YWQS_Setting.GetData()
    local data = isExpression and setting.ExpressionTask or setting.DateSceneTask
    for id, taskId in string.gmatch(data, "(%d+),(%d+)") do
        table.insert(self.taskList, {tonumber(id), tonumber(taskId)})
    end

    local sceneSpriteNames = {}
    if not isExpression then
        for id, spriteName in string.gmatch(setting.DateSceneSpriteNames, "(%d+),([%d%a_]+)") do
            sceneSpriteNames[tonumber(id)] = spriteName
        end
    end

    Extensions.RemoveAllChildren(self.grid.transform)
    for i = 1, #self.taskList do
        local child = NGUITools.AddChild(self.grid.gameObject, template)
        child:SetActive(true)
        child.transform:Find("Highlight").gameObject:SetActive(false)

        local expression = child.transform:Find("Expression"):GetComponent(typeof(CUITexture))
        local scene = child.transform:Find("Scene"):GetComponent(typeof(UISprite))
        local name = child.transform:Find("Name"):GetComponent(typeof(UILabel))
        expression.gameObject:SetActive(isExpression)
        scene.gameObject:SetActive(not isExpression)
        local id = self.taskList[i][1]
        if isExpression then
            expression:LoadMaterial(Expression_Show.GetData(id).Icon)
            name.text = Expression_Show.GetData(id).ExpName
        else
            scene.spriteName = sceneSpriteNames[id]
            name.text = PublicMap_PublicMap.GetData(id).Name
        end

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnItemClick(i)
        end)
    end
    self.grid:Reposition()
end

function LuaValentine2023YWQSSelectWnd:OnEnable()
    g_ScriptEvent:AddListener("AcceptTask", self, "OnAcceptTask")
end

function LuaValentine2023YWQSSelectWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AcceptTask", self, "OnAcceptTask")
end

function LuaValentine2023YWQSSelectWnd:OnAcceptTask(args)
    local taskId = tonumber(args[0])
    if self.taskId and self.taskId == taskId then
        CUIManager.CloseUI(CLuaUIResources.Valentine2023YWQSSelectWnd)
    end
end

function LuaValentine2023YWQSSelectWnd:Init()
    self:UpdateBg()
end

function LuaValentine2023YWQSSelectWnd:UpdateBg()
    local content = self.transform:Find("Bg/Content")
    local bounds = NGUIMath.CalculateRelativeWidgetBounds(content)
    local bg = self.transform:Find("Bg"):GetComponent(typeof(UIWidget))
    bg.width = bounds.size.x + 20

    bounds = NGUIMath.CalculateAbsoluteWidgetBounds(LuaValentine2023Mgr.YWQSInfo.selectGo.transform)
    local worldPos = Vector3(bounds.min.x, bounds.center.y, 0)
    local localPos = bg.transform.parent:InverseTransformPoint(worldPos)
    LuaUtils.SetLocalPosition(bg.transform, localPos.x - bg.width / 2 - 20, localPos.y, 0)
end

--@region UIEvent

function LuaValentine2023YWQSSelectWnd:OnItemClick(i)
    if self.curId then
        if self.curId == i then return end
        self.grid.transform:GetChild(self.curId - 1):Find("Highlight").gameObject:SetActive(false)
    end

    self.grid.transform:GetChild(i - 1):Find("Highlight").gameObject:SetActive(true)
    self.curId = i
    local isExpression = LuaValentine2023Mgr.YWQSInfo.selectType == "expression"
    self.taskId = self.taskList[i][2]
    if isExpression then
        Gac2Gas.AcceptValentineYWQSExpressionTask(self.taskId)
    else
        Gac2Gas.AcceptValentineYWQSDateTask(self.taskId)
    end
end

--@endregion UIEvent
