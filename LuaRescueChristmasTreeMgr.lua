local CPos = import "L10.Engine.CPos"
local Utility = import "L10.Engine.Utility"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

CLuaRescueChristmasTreeMgr={}

CLuaRescueChristmasTreeMgr.roadTips = {}
CLuaRescueChristmasTreeMgr.posList = {}
CLuaRescueChristmasTreeMgr.closeTick = nil
CLuaRescueChristmasTreeMgr.delayCloseTime = 10
CLuaRescueChristmasTreeMgr.shengDanIcons = {
    "UI/Texture/Transparent/Material/christmas_shengdanshu_01.mat",
    "UI/Texture/Transparent/Material/christmas_shengdanshu_01.mat"
}
CLuaRescueChristmasTreeMgr.yuanDanIcons = {
    "UI/Texture/Transparent/Material/yuandan_dan_01.mat",
    "UI/Texture/Transparent/Material/yuandan_dan_02.mat"
}
CLuaRescueChristmasTreeMgr.curState = 1 -- 1圣诞 2元旦
function CLuaRescueChristmasTreeMgr.OnEnterPlay()
    --rad tip
    CLuaRescueChristmasTreeMgr.posList = {}
    CLuaRescueChristmasTreeMgr.closeTick = nil
    for _,x,y in string.gmatch(ShengDan_RescueTree.GetData().MonsterInfo, "(%d+),(%d+),(%d+)") do
        x = tonumber(x)
        y = tonumber(y)
        table.insert(CLuaRescueChristmasTreeMgr.posList,CPos(x,y))
    end
    local npcInfo = ShengDan_RescueTree.GetData().NpcInfo
    local nextPos = CPos(npcInfo[0],npcInfo[1])
    for i=1,4,1 do
        if CLuaRescueChristmasTreeMgr.roadTips[i] then
            CLuaRescueChristmasTreeMgr.roadTips[i]:Destroy()
        end
        local curPos = CLuaRescueChristmasTreeMgr.posList[i]
        local angle = math.atan2(nextPos.y - curPos.y, nextPos.x - curPos.x) * Rad2Deg
        CLuaRescueChristmasTreeMgr.roadTips[i] = CEffectMgr.Inst:AddWorldPositionFX(88600020, Utility.GridPos2PixelPos(CLuaRescueChristmasTreeMgr.posList[i]),angle,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
    end

    CLuaRescueChristmasTreeMgr.WaitDestroyTips()
end

function CLuaRescueChristmasTreeMgr.OnPlayEnd()
    CLuaRescueChristmasTreeMgr.CancelCloseTick()
    for k,v in pairs(CLuaRescueChristmasTreeMgr.roadTips) do
        v:Destroy()
    end
	CLuaRescueChristmasTreeMgr.roadTips = {}
end

function CLuaRescueChristmasTreeMgr.CancelCloseTick()
    if CLuaRescueChristmasTreeMgr.closeTick then
        UnRegisterTick(CLuaRescueChristmasTreeMgr.closeTick)
        CLuaRescueChristmasTreeMgr.closeTick = nil
    end
end

function CLuaRescueChristmasTreeMgr.WaitDestroyTips()
    CLuaRescueChristmasTreeMgr.CancelCloseTick()
    CLuaRescueChristmasTreeMgr.closeTick = RegisterTickOnce(function ( ... )
		CLuaRescueChristmasTreeMgr.closeTick = nil
        for k,v in ipairs(CLuaRescueChristmasTreeMgr.roadTips) do
            v:Destroy()
        end
		end, 1000 * CLuaRescueChristmasTreeMgr.delayCloseTime)
end

