--local ButterflyCrisis_Reward = import "L10.Game.ButterflyCrisis_Reward"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"

LuaButterflyCrisisMgr = {}

LuaButterflyCrisisMgr.m_Story = nil
--@desc 白蝶剧情tip
function LuaButterflyCrisisMgr.ShowStory(str)
    LuaButterflyCrisisMgr.m_Story = str
    CUIManager.ShowUI(CLuaUIResources.ButterflyCrisisStoryWnd)
end

LuaButterflyCrisisMgr.m_StoryWorldPos = nil
function LuaButterflyCrisisMgr.ShowStoryWithPos(str, worldPosition)
    LuaButterflyCrisisMgr.m_Story = str
    LuaButterflyCrisisMgr.m_StoryWorldPos = worldPosition
    CUIManager.ShowUI(CLuaUIResources.ButterflyCrisisStoryWnd)
end


--@desc 白蝶剧情tip
--@playId: 白蝶玩法的id
--@memId: 展示记忆的id
function LuaButterflyCrisisMgr.ShowStoryByID(playId, memId)
    local butterflyPlay = ButterflyCrisis_Play.GetData(playId)
    if not butterflyPlay then return end

    local strName = SafeStringFormat3("Story%s", tostring(memId))
    local str = butterflyPlay[strName]
    LuaButterflyCrisisMgr.ShowStory(str)
end

LuaButterflyCrisisMgr.m_NoticeNPCIdx = 0
--@desc 登录提示
function LuaButterflyCrisisMgr.ShowButterflyCrisisNotice(npcIdx)
    LuaButterflyCrisisMgr.m_NoticeNPCIdx = npcIdx
    CUIManager.ShowUI(CLuaUIResources.ButterflyCrisisNoticeWnd)
end


LuaButterflyCrisisMgr.m_SelectedNPCIdx = 0

--@desc list<index(npcIdx), bool(isOpen)>
LuaButterflyCrisisMgr.m_NPCOpenInfo = {}
--@desc list<index(npcIdx), {}>
LuaButterflyCrisisMgr.m_MemoryUnlockInfo = {}
--@desc list<index(npcIdx), bool(isSolved)>
LuaButterflyCrisisMgr.m_BossSolvedInfo = {}

--@desc 打开白蝶风波主界面
--@npcIdx: [1, 5]
function LuaButterflyCrisisMgr.OpenButterflyCrisisMainWnd(npcIdx)
    LuaButterflyCrisisMgr.m_SelectedNPCIdx = npcIdx

    if CUIManager.IsLoaded(CLuaUIResources.ButterflyCrisisMainWnd) then
        g_ScriptEvent:BroadcastInLua("UpdateButterflyCrisisMainWnd")
    else
        CUIManager.ShowUI(CLuaUIResources.ButterflyCrisisMainWnd)
    end
end

--@desc 更新白蝶记忆的解锁情况
--@npcIdx: NPCPlay的idx
--@memId: 记忆id
--@isUnlock: 是否解锁
function LuaButterflyCrisisMgr.UpdateMemoryUnlockInfo(npcIdx, memId, isUnlock)
    if not LuaButterflyCrisisMgr.m_MemoryUnlockInfo or not LuaButterflyCrisisMgr.m_MemoryUnlockInfo[npcIdx] then return end

    LuaButterflyCrisisMgr.m_MemoryUnlockInfo[npcIdx][memId] = isUnlock

    g_ScriptEvent:BroadcastInLua("UpdateMemoryUnlockInfo", npcIdx, memId)
end


--@desc 更新白蝶BOSS打败情况
--@npcIdx: NPCPlay的idx
--@isUnlock: 是否被打开
function LuaButterflyCrisisMgr.UpdateBossSolvedInfo(npcIdx, isUnlock)

    if not LuaButterflyCrisisMgr.m_BossSolvedInfo then return end

    LuaButterflyCrisisMgr.m_BossSolvedInfo[npcIdx] = isUnlock

    g_ScriptEvent:BroadcastInLua("UpdateBossSolvedInfo", npcIdx)
end


--@desc list<index(rewardId), bool>
LuaButterflyCrisisMgr.m_RewardTakenInfos = {}
--@desc 处理奖励是否领取的bitmap
function LuaButterflyCrisisMgr.ParseRewardTakenInfos(rewardBitmap)
    LuaButterflyCrisisMgr.m_RewardTakenInfos = {}

    ButterflyCrisis_Reward.ForeachKey(function (key)
        local temp = bit.band(rewardBitmap, bit.lshift(1, key-1))
        LuaButterflyCrisisMgr.m_RewardTakenInfos[key] = temp ~= 0
    end)
end

--@desc list {ItemId, Count}
LuaButterflyCrisisMgr.m_RewardList = {}
--@desc 显示已领取的物品
function LuaButterflyCrisisMgr.ShowRewardGetWnd(rewardInfos)
    LuaButterflyCrisisMgr.m_RewardList = rewardInfos
    CUIManager.ShowUI(CLuaUIResources.ButterflyCrisisRewardWnd)
    g_ScriptEvent:BroadcastInLua("ButterflyCollectRewardSuccess")
end

--@desc 是否boss通关后首次打开界面
--@npcIdx: boss index
function LuaButterflyCrisisMgr.IsFirstTimeBossSolved(npcIdx)
    local key = tostring(CClientMainPlayer.Inst.Id).."Butterfly_Crisis_Boss_Info"
    local bossIdx = PlayerPrefs.GetInt(key, 0)
    return npcIdx > bossIdx
end

--@desc 更新本地存储的打败的最新的boss的index
function LuaButterflyCrisisMgr.UpdateSolvedBossInfo(npcIdx)
    local key = tostring(CClientMainPlayer.Inst.Id).."Butterfly_Crisis_Boss_Info"
    local bossIdx = PlayerPrefs.GetInt(key)
    if npcIdx > bossIdx then
        PlayerPrefs.SetInt(key, npcIdx)
    end
end



