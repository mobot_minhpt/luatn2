local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local IdPartition = import "L10.Game.IdPartition"

LuaTianShu2023ShareWnd = class()
LuaTianShu2023ShareWnd.m_ShowTemplateId = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTianShu2023ShareWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaTianShu2023ShareWnd, "BackToLastButton", "BackToLastButton", GameObject)
RegistChildComponent(LuaTianShu2023ShareWnd, "ItemIcon", "ItemIcon", CUITexture)
RegistChildComponent(LuaTianShu2023ShareWnd, "ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaTianShu2023ShareWnd, "Logo", "Logo", GameObject)
RegistChildComponent(LuaTianShu2023ShareWnd, "PlayerInfo", "PlayerInfo", GameObject)
RegistChildComponent(LuaTianShu2023ShareWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaTianShu2023ShareWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:RefreshVariableUI()
    self:InitUIEvent()

    if CommonDefs.IS_VN_CLIENT then
        self.ShareButton:SetActive(false)
    end
end

function LuaTianShu2023ShareWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaTianShu2023ShareWnd:RefreshVariableUI()
    self.tianshuTemplateId2Icon = {}
    Item_TianShuWeight.Foreach(function(_, v)
        self.tianshuTemplateId2Icon[v.Id] = v.Icon
        if v.FeiShengReplaceItem ~= 0 then
            self.tianshuTemplateId2Icon[v.FeiShengReplaceItem] = v.FeiShengReplaceIcon
        end
    end)
    
    self.Logo:SetActive(false)
    self.PlayerInfo:SetActive(false)
    if (IdPartition.IdIsItem(LuaTianShu2023ShareWnd.m_ShowTemplateId)) then
        local itemCfg = Item_Item.GetData(LuaTianShu2023ShareWnd.m_ShowTemplateId)
        --local tianshuCfg = Item_TianShuWeight.GetData(LuaTianShu2023ShareWnd.m_ShowTemplateId)
        self.ItemIcon:LoadMaterial(self.tianshuTemplateId2Icon[LuaTianShu2023ShareWnd.m_ShowTemplateId])
        self.ItemNameLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor(GameSetting_Common_Wapper.Inst:GetColor(itemCfg.NameColor)),
                string.gsub(itemCfg.Name, LocalString.GetString("%(秘籍%)"), ""))

        local showJueJiEffect = 0
        if itemCfg and itemCfg.Type == EnumItemType_lua.Jueji then
            if itemCfg.GradeCheck == 100 then
                showJueJiEffect = 2
            elseif itemCfg.GradeCheck == 70 then
                showJueJiEffect = 1
            end
        end
        self.ItemIcon.transform:Find("vfx_juejiruo").gameObject:SetActive(showJueJiEffect == 1)
        self.ItemIcon.transform:Find("vfx_juejiqiang").gameObject:SetActive(showJueJiEffect == 2)
    end
end

function LuaTianShu2023ShareWnd:InitUIEvent()
    UIEventListener.Get(self.ShareButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self.ShareButton:SetActive(false)
        self.BackToLastButton:SetActive(false)
        self.CloseButton:SetActive(false)
        self.Logo:SetActive(true)
        self.PlayerInfo:SetActive(true)
        CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            if not CommonDefs.IS_VN_CLIENT then
                self.ShareButton.gameObject:SetActive(true)
            end
            self.BackToLastButton.gameObject:SetActive(true)
            self.CloseButton:SetActive(true)
            self.Logo:SetActive(false)
            self.PlayerInfo:SetActive(false)
        end))
    end)

    UIEventListener.Get(self.BackToLastButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI("TianShu2023ShareWnd")
    end)

    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI("TianShu2023ShareWnd")
        CUIManager.CloseUI("TianShu2023OpenWnd")
    end)
end
