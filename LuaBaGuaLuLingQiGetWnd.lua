require("common/common_include")

local CQianKunDaiInputView = import "L10.UI.CQianKunDaiInputView"
local CQianKunDaiCompoundView = import "L10.UI.CQianKunDaiCompoundView"
local QianKunDai_Setting = import "L10.Game.QianKunDai_Setting"

LuaBaGuaLuLingQiGetWnd = class()

RegistClassMember(LuaBaGuaLuLingQiGetWnd, "InputSelectionView")
RegistClassMember(LuaBaGuaLuLingQiGetWnd, "CompoundView")

function LuaBaGuaLuLingQiGetWnd:Init()
	if not CClientMainPlayer.Inst then
		CUIManager.CloseUI(CLuaUIResources.BaGuaLuLingQiGetWnd)
		return
	end
	self:InitClassMembers()
	self:InitValues()
end

function LuaBaGuaLuLingQiGetWnd:InitClassMembers()
	self.InputSelectionView = self.transform:Find("InputSelectionView"):GetComponent(typeof(CQianKunDaiInputView))
	self.CompoundView = self.transform:Find("CompoundView"):GetComponent(typeof(CQianKunDaiCompoundView))
end

function LuaBaGuaLuLingQiGetWnd:InitValues()
	local setting = QianKunDai_Setting.GetData()
	if not setting then
		CUIManager.CloseUI(CLuaUIResources.BaGuaLuLingQiGetWnd)
		return
	end

	self.InputSelectionView:InitQianKunDaiInput(setting.ScoreFormulaId)
	self.CompoundView:Init(setting.ScoreFormulaId)

end


return LuaBaGuaLuLingQiGetWnd
