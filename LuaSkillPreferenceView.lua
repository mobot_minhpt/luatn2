
LuaSkillPreferenceView = class()
RegistChildComponent(LuaSkillPreferenceView,"m_TopView","TopView", CCommonLuaScript)
RegistChildComponent(LuaSkillPreferenceView,"m_BottomView","BottomView", CCommonLuaScript)

function LuaSkillPreferenceView:OnDragComplete(skillId, destGo)
    self.m_BottomView:ReplaceSkill(skillId, destGo, 0)
end

function LuaSkillPreferenceView:Get6thSkillTab()
    return self.m_TopView:Get6thSkillTab()
end
function LuaSkillPreferenceView:Get6thSkillItem()
    return self.m_TopView:Get6thSkillItem()
end
function LuaSkillPreferenceView:GetTianFuAdjustButton()
    return self.m_BottomView:GetTianFuAdjustButton()
end
function LuaSkillPreferenceView:GetSwitchSkill()
    return self.m_BottomView:GetSwitchSkill()
end
function LuaSkillPreferenceView:GetSwitchButton()
    return self.m_BottomView:GetSwitchButton()
end
function LuaSkillPreferenceView:GetHuaHunColorSkillItem()
    return self.m_TopView:GetHuaHunColorSkillItem()
end
function LuaSkillPreferenceView:GetHuaHunColorSkillTab()
    return self.m_TopView:GetHuaHunColorSkillTab()
end

function LuaSkillPreferenceView:GetYingLingSwitchSkillStateBtn()
    return self.m_BottomView:GetYingLingSwitchSkillStateBtn()
end