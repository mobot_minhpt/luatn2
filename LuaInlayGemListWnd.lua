local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumItemType = import "L10.Game.EnumItemType"
local CItemMgr = import "L10.Game.CItemMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

local UIDefaultTableView=import "L10.UI.UIDefaultTableView"
local CellIndexType=import "L10.UI.UITableView+CellIndexType"
local CellIndex=import "L10.UI.UITableView+CellIndex"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CUITexture=import "L10.UI.CUITexture"

CLuaInlayGemListWnd = class()
RegistClassMember(CLuaInlayGemListWnd,"okBtn")
RegistClassMember(CLuaInlayGemListWnd,"gemList")
RegistClassMember(CLuaInlayGemListWnd,"background")
RegistClassMember(CLuaInlayGemListWnd,"offset")
RegistClassMember(CLuaInlayGemListWnd,"sections")
RegistClassMember(CLuaInlayGemListWnd,"m_SrcHoleIndex")
RegistClassMember(CLuaInlayGemListWnd,"m_SrcHoleItemId")
RegistClassMember(CLuaInlayGemListWnd,"m_IsReference")
RegistClassMember(CLuaInlayGemListWnd,"selectJewelInfo")
RegistClassMember(CLuaInlayGemListWnd,"isRequestInlayHere")

CLuaInlayGemListWnd.m_HoleSetIndex=0

function CLuaInlayGemListWnd:Awake()
    self.isRequestInlayHere = false
    self.selectJewelInfo = nil
    self.okBtn = self.transform:Find("Anchor/OkButton").gameObject
    self.gemList = self.transform:Find("Anchor/GemList"):GetComponent(typeof(UIDefaultTableView))

    self.gemList:Init(function()
            return #self.sections--numberOfSectionFunc
        end,
        function(section)--numberOfRowInSectionFunc
            return 1
        end,
        function(cell,index)--cell
            local gem = self.sections[index.section+1]
            if (index.type == CellIndexType.Section) then
                local nameLabel = cell.transform:Find("Name"):GetComponent(typeof(UILabel))
                local iconTexture = cell.transform:Find("Icon"):GetComponent(typeof(CUITexture))
                local countLabel = cell.transform:Find("Count"):GetComponent(typeof(UILabel))
                
                local item = CItemMgr.Inst:GetItemTemplate(gem.templateId)
                if gem.name then
                    nameLabel.text = gem.name
                else
                    nameLabel.text = item.Name
                    if gem.isBind then
                        nameLabel.text = nameLabel.text .. SafeStringFormat3(LocalString.GetString("[c][%s](绑)[-]"), NGUIText.EncodeColor24(Color.red))
                    end
                end
                iconTexture:LoadMaterial(item.Icon)
                countLabel.text = tostring(gem.count)
            else
                local descLabel = cell.transform:Find("Label"):GetComponent(typeof(UILabel))
                local background=cell:GetComponent(typeof(UISprite))
                descLabel.text = gem.desc
                background.height = 16*2 + descLabel.height
            end
        end,
        function(index,expanded)--sectionClick
            if index.type == CellIndexType.Section then
            end
        end,
        function(index)--rowselect
            local section = self.sections[index.section+1]
            if section.isReference then
                self.m_SrcHoleIndex = section.srcHoleIndex
                self.m_SrcHoleItemId = section.srcHoleItemId
                self.selectJewelInfo=nil
                self.m_IsReference=true
            else
                self.selectJewelInfo = section.itemInfo
                self.m_IsReference=false
            end
        end
    )

    self.background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))
    self.offset = self.transform:Find("Anchor")

    UIEventListener.Get(self.okBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOkBtnClicked(go)
    end)
end
function CLuaInlayGemListWnd:GetJewelWordContext(stoneItemId, equip) 
    local jewel = Jewel_Jewel.GetData(stoneItemId)
    local wordId = 0
    if equip.Equip.IsWeapon then
        wordId = jewel.onWeapon
    elseif equip.Equip.IsArmor then
        wordId = jewel.onArmor
    elseif equip.Equip.IsJewelry then
        wordId = jewel.onJewelry
    elseif equip.Equip.IsFaBao then
        wordId = jewel.onFaBao
    end
    if wordId > 0 then
        local word = Word_Word.GetData(wordId)
        return word.Description
    end
    return nil
end

function CLuaInlayGemListWnd:InitList(isFaBao)
    self.sections = {}

    local itemInfoList = CItemMgr.Inst:GetPlayerItemAtPlaceByType(EnumItemPlace.Bag, isFaBao and EnumItemType.WenShi or EnumItemType.Gem)
    local bindTemplateIdList = {}
    local notBindTemplateIdList = {}

    --如果是宝石，则判断是否是第二套宝石
    local equip = CLuaEquipMgr.m_GemInlaySelectedEquipment.Equip
    if CLuaInlayGemListWnd.m_HoleSetIndex==2 then
        local refLookup = {}
        for i=1,equip.Hole do
            local jewelId = equip.SecondaryHoleItems[i]
            if jewelId>0 and jewelId<100 then
                refLookup[equip.HoleItems[jewelId]] = i--这个宝石已经被引用过了
            end
        end
        --显示公用宝石
        for i=1,equip.Hole do
            local jewelId = equip.HoleItems[i]
            if jewelId>0 and not refLookup[jewelId] then
                local jewelData = Jewel_Jewel.GetData(jewelId)
                local gem = {
                    name = jewelData.JewelName .. SafeStringFormat3("(lv.%d)",jewelData.JewelLevel),
                    templateId = jewelId,
                    isBind = false,
                    count = 1,
                    desc = self:GetJewelWordContext(jewelId,CLuaEquipMgr.m_GemInlaySelectedEquipment).."\n"..LocalString.GetString("[c][00ff00]已镶嵌于第一套，可共用[-][/c]"),
                    itemInfo = nil,
                    isReference = true,
                    srcHoleIndex = i,
                    srcHoleItemId = jewelId,
                }
                table.insert( self.sections,gem )
            end
        end
    else
        --如果是第一套，要显示第二套上面非引用的宝石
        for i=1,equip.Hole do
            local jewelId = equip.SecondaryHoleItems[i]
            if jewelId>100 then
                local jewelData = Jewel_Jewel.GetData(jewelId)
                if jewelData then
                    local gem = {
                        name = jewelData.JewelName .. SafeStringFormat3("(lv.%d)",jewelData.JewelLevel),
                        templateId = jewelId,
                        isBind = false,
                        count = 1,
                        desc = self:GetJewelWordContext(jewelId,CLuaEquipMgr.m_GemInlaySelectedEquipment).."\n"..LocalString.GetString("[c][00ff00]已镶嵌于第二套，可共用[-][/c]"),
                        itemInfo = nil,
                        isReference = true,
                        srcHoleIndex = i,
                        srcHoleItemId = jewelId,
                    }
                    table.insert( self.sections,gem )
                end
            end
        end
    end

    for i=1,itemInfoList.Count do
        local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemInfoList[i-1].templateId)
        local desc = self:GetJewelWordContext(itemInfoList[i-1].templateId, CLuaEquipMgr.m_GemInlaySelectedEquipment)
        local item = CItemMgr.Inst:GetById(itemInfoList[i-1].itemId)
        if bindTemplateIdList[itemInfoList[i-1].templateId] and item.IsBinded then
        elseif notBindTemplateIdList[itemInfoList[i-1].templateId] and not item.IsBinded then
        else
            if item.IsBinded then
                bindTemplateIdList[item.TemplateId] = true
            else
                notBindTemplateIdList[item.TemplateId] = true
            end

            local gem = nil
            if item.IsBinded and bindItemCountInBag > 0 then
                gem = {
                    templateId = itemInfoList[i-1].templateId,
                    isBind = true,
                    count = bindItemCountInBag,
                    desc = desc,
                    itemInfo = itemInfoList[i-1]
                }
                table.insert( self.sections,gem )
            elseif not item.IsBinded and notBindItemCountInBag > 0 then
                gem = {
                    templateId = itemInfoList[i-1].templateId,
                    isBind = false,
                    count = notBindItemCountInBag,
                    desc = desc,
                    itemInfo = itemInfoList[i-1]
                }
                table.insert( self.sections,gem )
            end
        end
    end
    self.gemList:LoadData(CellIndex(0, 0, CellIndexType.Row), true)
end

function CLuaInlayGemListWnd:Init( )
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    local item = CItemMgr.Inst:GetById(equipInfo.itemId)
    if item.IsEquip and item.Equip.IsFaBao then
        self:InitList(true)
    else
        self:InitList(false)
    end
end

function CLuaInlayGemListWnd:CanInlayStone(equip, stoneId) 
    local item = CItemMgr.Inst:GetById(stoneId)
    if item == nil then
        return false
    end
    local putInJewel = Jewel_Jewel.GetData(item.TemplateId)
    if putInJewel == nil then
        return false
    end
    local putInJewelKind = putInJewel.JewelKind
    --检查宝石类型
    local is2ndset = CLuaInlayGemListWnd.m_HoleSetIndex==2
    for i=1,equip.Hole do
        local jewelId = is2ndset and equip.SecondaryHoleItems[i] or equip.HoleItems[i]
        if is2ndset and jewelId>0 and jewelId<100 then
            jewelId = equip.HoleItems[jewelId]
        end
        if jewelId > 0 then
            local jewel = Jewel_Jewel.GetData(jewelId)
            if jewel ~= nil then
                if jewel.JewelKind == putInJewelKind then
                    if item.IsItem and item.Item.Type == EnumItemType_lua.WenShi then
                        g_MessageMgr:ShowMessage("WenShi_XiangQian_Error_SameType")
                    else
                        g_MessageMgr:ShowMessage("INLAY_STONE_KIND_NOT_OK")
                    end
                    return false
                end
            end
        end
    end
    for i=1,equip.Hole do
        local otherJewelId = is2ndset and equip.HoleItems[i] or equip.SecondaryHoleItems[i]
        if not is2ndset and otherJewelId>0 and otherJewelId<100 then
            otherJewelId = equip.HoleItems[otherJewelId]
        end
        if otherJewelId > 0 then
            local otherJewel = Jewel_Jewel.GetData(otherJewelId)
            if otherJewel ~= nil then
                if otherJewel.JewelKind == putInJewelKind then
                    if not (item.IsItem and item.Item.Type == EnumItemType_lua.WenShi) then
                        g_MessageMgr:ShowMessage("Double_EquipStones_Error_HasSameKind")
                    end
                    return false
                end
            end
        end
    end
    return true
end

function CLuaInlayGemListWnd:OnOkBtnClicked( go) 
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if not self.m_IsReference then
        if self.selectJewelInfo ~= nil then
            local equip = CItemMgr.Inst:GetById(equipInfo.itemId)
            if equip == nil then
                return
            end
            if self:CanInlayStone(equip.Equip, self.selectJewelInfo.itemId) then
                local stone = CItemMgr.Inst:GetById(self.selectJewelInfo.itemId)
                if stone ~= nil then
                    if not stone.IsBinded and equip.IsBinded then
                        local message = g_MessageMgr:FormatMessage("INLAY_STONE_BIND_CONFIRM")
                        if stone.IsItem and stone.Item.Type == EnumItemType_lua.WenShi then
                            message = g_MessageMgr:FormatMessage("INLAY_FABAO_BIND_CONFIRM")
                        end
                        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                                self:InlayStone()
                            end), nil, nil, nil, false)
                        return
                    else
                        self:InlayStone()
                    end
                end
            end
        else
            g_MessageMgr:ShowMessage("JEWEL_INLAY_NEED_SELECT_JEWEL")
        end
    else
        self:InlayStone()
    end
end
function CLuaInlayGemListWnd:InlayStone( )
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if equipInfo == nil then
        return
    end

    self.isRequestInlayHere = true
    if self.selectJewelInfo then
        Gac2Gas.RequestCanInlayStoneOnEquipment(
            EnumToInt(equipInfo.place), 
            equipInfo.pos, 
            equipInfo.itemId, 
            EnumToInt(self.selectJewelInfo.place), 
            self.selectJewelInfo.pos, 
            self.selectJewelInfo.itemId, 
            CLuaInlayGemListWnd.m_HoleSetIndex)
    else--引用第一套
        Gac2Gas.RequestCanReferenceStoneOnEquipment(
            EnumToInt(equipInfo.place), 
            equipInfo.pos, 
            equipInfo.itemId, 
            self.m_SrcHoleIndex,
            self.m_SrcHoleItemId,
			CLuaInlayGemListWnd.m_HoleSetIndex==2 and 1 or 2)--这里指的是引用哪套宝石
    end
end

function CLuaInlayGemListWnd:OnRequestCanInlayStoneOnEquipmentResult( can, equipId, equipNewGrade, setIndex) 
    if not self.isRequestInlayHere then return end
    self.isRequestInlayHere = false 

    if not can then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end

    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if self.selectJewelInfo == nil or equipInfo == nil then
        return
    end

    local equipItem = CItemMgr.Inst:GetById(equipInfo.itemId)
    if equipItem == nil or not equipItem.IsEquip then
        return
    end

    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(equipInfo) then
        return
    end
    if not equipItem.Equip.IsBinded then
        local message = g_MessageMgr:FormatMessage("Equipment_Inlay_Stone_Confirm")
        local v1,v2,v3=EnumToInt(self.selectJewelInfo.place), self.selectJewelInfo.pos, self.selectJewelInfo.itemId
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
            Gac2Gas.InlayStoneOnEquipment(EnumToInt(equipInfo.place), equipInfo.pos, equipInfo.itemId, v1, v2, v3, setIndex)
        end), nil, nil, nil, false)
    else
        Gac2Gas.InlayStoneOnEquipment(EnumToInt(equipInfo.place), equipInfo.pos, equipInfo.itemId, EnumToInt(self.selectJewelInfo.place), self.selectJewelInfo.pos, self.selectJewelInfo.itemId, setIndex)
    end
    CUIManager.CloseUI(CUIResources.InlayGemListWnd)
end

function CLuaInlayGemListWnd:OnEnable()
    g_ScriptEvent:AddListener("RequestCanInlayStoneOnEquipmentResult",self,"OnRequestCanInlayStoneOnEquipmentResult")
    g_ScriptEvent:AddListener("RequestCanReferenceStoneOnEquipmentResult",self,"OnRequestCanReferenceStoneOnEquipmentResult")
    
end
function CLuaInlayGemListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RequestCanInlayStoneOnEquipmentResult",self,"OnRequestCanInlayStoneOnEquipmentResult")
    g_ScriptEvent:RemoveListener("RequestCanReferenceStoneOnEquipmentResult",self,"OnRequestCanReferenceStoneOnEquipmentResult")
end

function CLuaInlayGemListWnd:OnDestroy()
    CLuaInlayGemListWnd.m_HoleSetIndex=0
end

function CLuaInlayGemListWnd:OnRequestCanReferenceStoneOnEquipmentResult(can, equipId, equipNewGrade)
    if not self.isRequestInlayHere then return end
    self.isRequestInlayHere = false 
    if not can then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end
    
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if equipInfo == nil then
        return
    end

    Gac2Gas.ReferenceStoneOnEquipment(
        EnumToInt(equipInfo.place), 
        equipInfo.pos, 
        equipInfo.itemId, 
        self.m_SrcHoleIndex,
        self.m_SrcHoleItemId,
		CLuaInlayGemListWnd.m_HoleSetIndex==2 and 1 or 2)
    CUIManager.CloseUI(CUIResources.InlayGemListWnd)
end
