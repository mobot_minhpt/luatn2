local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local BoxCollider = import "UnityEngine.BoxCollider"
local QnTableView = import "L10.UI.QnTableView"
local QnTableItem = import "L10.UI.QnTableItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Object = import "System.Object"

LuaLiuRuShiDaTiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuRuShiDaTiWnd, "NumberLabel", "NumberLabel", UILabel)
RegistChildComponent(LuaLiuRuShiDaTiWnd, "TopicLabel", "TopicLabel", UILabel)
RegistChildComponent(LuaLiuRuShiDaTiWnd, "Option", "Option", UITable)
RegistChildComponent(LuaLiuRuShiDaTiWnd, "OptionTemplate", "OptionTemplate", GameObject)
RegistChildComponent(LuaLiuRuShiDaTiWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaLiuRuShiDaTiWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaLiuRuShiDaTiWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaLiuRuShiDaTiWnd, "ScoreView", "ScoreView", QnTableView)

RegistClassMember(LuaLiuRuShiDaTiWnd, "m_QuestionId")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_AllQuestionCount")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_SelectIndex")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_OptionList")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_RankData")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_Time")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_QuestionInterval")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_AnswerInterval")
RegistClassMember(LuaLiuRuShiDaTiWnd, "m_CountDownTick")
--@endregion RegistChildComponent end

function LuaLiuRuShiDaTiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaLiuRuShiDaTiWnd:Init()
    self.OptionTemplate.gameObject:SetActive(false)
    -- 分数和排行榜
    self:InitRank()
    -- 开始答题
    self.m_QuestionId = 0
    self.m_AllQuestionCount = NanDuFanHua_LiuRuShiDaTiTiKu.GetDataCount()
    local setData = NanDuFanHua_LiuRuShiSetting.GetData()
    self.m_AnswerInterval = setData.AnswerInterval
    self.m_QuestionInterval = setData.AnswerInterval - setData.QuestionInterval
    self:StartAnswer()
end

function LuaLiuRuShiDaTiWnd:InitRank()
    self.m_RankData = {}
    local myData = {}
    myData.Id = 0
    myData.Name = CClientMainPlayer.Inst.BasicProp.Name
    myData.Score = 0
    table.insert(self.m_RankData, myData)
    for i = 1, NanDuFanHua_LiuRuShiDaTiNpc.GetDataCount() do
        local npcData = NanDuFanHua_LiuRuShiDaTiNpc.GetData(i)
        npcData.Score = 0
        table.insert(self.m_RankData, npcData)
    end
    self.ScoreView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_RankData
    end, function(item, index)
        self:InitItem(item, index + 1)
    end)
    self.ScoreView:ReloadData(false, false)
    self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("得分  %d"), myData.Score)
end

function LuaLiuRuShiDaTiWnd:InitItem(item, index)
    local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local score = item.transform:Find("Score"):GetComponent(typeof(UILabel))
    local bg = item.transform:GetComponent(typeof(QnTableItem))
    if index % 2 == 0 then
        bg.m_NormalSpirte = "common_bg_mission_background_s"
    else
        bg.m_NormalSpirte = "common_bg_mission_background_n"
    end
    if self.m_RankData[index].Id == 0 then
        name.color = Color.green
        score.color = Color.green
    else
        name.color = Color.white
        score.color = Color.white
    end
    name.text = self.m_RankData[index].Name
    score.text = self.m_RankData[index].Score
end

function LuaLiuRuShiDaTiWnd:InitQuestion()
    local data = NanDuFanHua_LiuRuShiDaTiTiKu.GetData(self.m_QuestionId)
    self.TopicLabel.text = data.Question

    local answerList = {}
    table.insert(answerList, {text = data.Right, isRight = true})
    table.insert(answerList, {text = data.Wrong1, isRight = false})
    table.insert(answerList, {text = data.Wrong2, isRight = false})
    table.insert(answerList, {text = data.Wrong3, isRight = false})
    g_LuaUtil:RandomShuffleArray(answerList)

    self.m_OptionList = {}
    self.m_SelectIndex = nil
    local prefix = {"A.", "B.", "C.", "D."}
    Extensions.RemoveAllChildren(self.Option.transform)
    for i = 1, 4 do
        local go = NGUITools.AddChild(self.Option.gameObject, self.OptionTemplate)
        local option = go:GetComponent(typeof(QnSelectableButton))
        self:InitOption(option, prefix[i] .. answerList[i].text, i)
        table.insert(self.m_OptionList, {btn = option, isRight = answerList[i].isRight})
    end
    self.Option:Reposition()
end

function LuaLiuRuShiDaTiWnd:InitOption(optionBtn, answerText, index)
    optionBtn.Text = answerText
    optionBtn.gameObject:SetActive(true)
    optionBtn.transform:Find("RightTip").gameObject:SetActive(false)
    optionBtn.transform:Find("WrongTip").gameObject:SetActive(false)
    optionBtn.transform:Find("Slider").gameObject:SetActive(false)
    UIEventListener.Get(optionBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOptionClick(index)
    end)
end

function LuaLiuRuShiDaTiWnd:StartAnswer()
    -- 所有题目回答完毕
    if self.m_QuestionId == self.m_AllQuestionCount then
        self:FinishTask()
        return
    end

    -- 倒计时
    self.m_Time = self.m_AnswerInterval
    self:UpdateCountDown()
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
    end
    self.m_CountDownTick = RegisterTick(function()
        self:UpdateCountDown()
    end, 1000)

    -- 题数
    self.m_QuestionId = self.m_QuestionId + 1
    self.NumberLabel.text = SafeStringFormat3(LocalString.GetString("第%d/%d题"), self.m_QuestionId, self.m_AllQuestionCount)
    self:InitQuestion()
end

function LuaLiuRuShiDaTiWnd:EndAnswer()
    -- 显示答案正确与否
    for i, option in pairs(self.m_OptionList) do
        if option.isRight or i == self.m_SelectIndex then
            option.btn.transform:Find("RightTip").gameObject:SetActive(option.isRight)
            option.btn.transform:Find("WrongTip").gameObject:SetActive(not option.isRight)
        end
    end
    -- 更新分数
    local isRight = self.m_SelectIndex and self.m_OptionList[self.m_SelectIndex].isRight or false
    for i, data in pairs(self.m_RankData) do
        if data.Id == 0 and isRight then
            data.Score = data.Score + 10
            self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("得分  %d"), data.Score)
        end
        if data.Id ~= 0 and data.ScoreList[self.m_QuestionId - 1] then
            data.Score = data.Score + data.ScoreList[self.m_QuestionId - 1]
        end
    end
    -- 更新排行榜
    table.sort(self.m_RankData, function(a,b)
        if a.Score == b.Score then return a.Id < b.Id
        else return a.Score > b.Score end
	end)
    self.ScoreView:ReloadData(false, false)
end

function LuaLiuRuShiDaTiWnd:UpdateCountDown()
    if self.m_Time == 0 then self:EndAnswer() end        -- 倒计时结束显示答案正确与否
    if self.m_Time == self.m_QuestionInterval then     -- 答案显示两秒后开始下一题
        self:StartAnswer()
        return
    end
    local time = self.m_Time < 0 and 0 or self.m_Time
    self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒后下一题"), time)
    self.ProgressBar.value = time / self.m_AnswerInterval
    self.m_Time = self.m_Time - 1
end

function LuaLiuRuShiDaTiWnd:FinishTask()
    if self.m_RankData[1].Id == 0 and self.m_RankData[1].Score >= self.m_RankData[2].Score then
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        Gac2Gas.FinishClientTaskEvent(LuaLiuRuShiMgr.m_TaskId, "LiuRuShi_Examination", MsgPackImpl.pack(empty))
    else
        LuaLiuRuShiMgr:ShowLiuRuShiTaskFailWnd(LuaLiuRuShiMgr.m_TaskId, NanDuFanHua_LiuRuShiSetting.GetData().DaTiTaskFailText)
    end
    CUIManager.CloseUI(CLuaUIResources.LiuRuShiDaTiWnd)
end

--@region UIEvent
function LuaLiuRuShiDaTiWnd:OnOptionClick(index)
    self.m_SelectIndex = index
    for i, option in pairs(self.m_OptionList) do
        option.btn:GetComponent(typeof(BoxCollider)).enabled = false
    end
    self.m_OptionList[index].btn:SetSelected(true)
end

--@endregion UIEvent

function LuaLiuRuShiDaTiWnd:OnDisable()
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
    end
end