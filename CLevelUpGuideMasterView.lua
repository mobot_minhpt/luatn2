-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuidelineMasterListItem = import "L10.UI.CGuidelineMasterListItem"
local CLevelUpGuideMasterView = import "L10.UI.CLevelUpGuideMasterView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Guide_LevelUpGuide = import "L10.Game.Guide_LevelUpGuide"
local LevelSection = import "L10.UI.LevelSection"
local Object = import "System.Object"
local Task_Task = import "L10.Game.Task_Task"
CLevelUpGuideMasterView.m_Init_CS2LuaHook = function (this) 

    this.tableView.dataSource = this
    this.tableView.eventDelegate = this

    local dataList = CreateFromClass(MakeGenericClass(List, Guide_LevelUpGuide))
    Guide_LevelUpGuide.ForeachKey(DelegateFactory.Action_object(function (key) 
        local guideData = Guide_LevelUpGuide.GetData(key)

        local canAdd = true
        if guideData.ID == CLevelUpGuideMasterView.ZhuXianTaskGuideId then
            canAdd = this:ZhuXianReceivedTaskExists()
        elseif guideData.ID == CLevelUpGuideMasterView.ZhiXianTaskGuideId then
            canAdd = this:ZhiXianAcceptableTaskExists()
        end
        if canAdd then
            CommonDefs.ListAdd(dataList, typeof(Guide_LevelUpGuide), guideData)
        end
    end))
    CommonDefs.ListClear(this.sections)
    for i = 0, 14 do
        local minLevel = i * 10 < 1 and 1 or i * 10
        local maxLevel = (i + 1) * 10 >= 150 and 150 or (i + 1) * 10 - 1
        local bFound = false
        do
            local j = 0
            while j < dataList.Count do
                if not (dataList[j].MinPlayerLevel > maxLevel or dataList[j].MaxPlayerLevel < minLevel) then
                    bFound = true
                    break
                end
                j = j + 1
            end
        end
        if bFound then
            CommonDefs.ListAdd(this.sections, typeof(LevelSection), CreateFromClass(LevelSection, minLevel, maxLevel))
        end
    end

    local selectedIndex = 0
    if CClientMainPlayer.Inst ~= nil then
        do
            local i = 0
            while i < this.sections.Count do
                if this.sections[i].minLevel <= CClientMainPlayer.Inst.Level and this.sections[i].maxLevel >= CClientMainPlayer.Inst.Level then
                    selectedIndex = i
                    break
                end
                i = i + 1
            end
        end
    end
    this.tableView:LoadData(0, false)
    this.tableView.SelectedIndex = selectedIndex
    this:OnRowSelected(selectedIndex)
end
CLevelUpGuideMasterView.m_ZhuXianReceivedTaskExists_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        local tasks = CClientMainPlayer.Inst.TaskProp.CurrentTaskList
        do
            local i = 0
            while i < tasks.Count do
                local template = Task_Task.GetData(tasks[i])
                if template ~= nil and template.DisplayNode == 1 then
                    return true
                end
                i = i + 1
            end
        end
    end
    return false
end
CLevelUpGuideMasterView.m_ZhiXianAcceptableTaskExists_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        local tasks = CClientMainPlayer.Inst.TaskProp.AcceptableTaskList
        do
            local i = 0
            while i < tasks.Count do
                local template = Task_Task.GetData(tasks[i])
                if template ~= nil and template.DisplayNode == 3 then
                    return true
                end
                i = i + 1
            end
        end
    end
    return false
end
CLevelUpGuideMasterView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    if index < 0 and index >= this.sections.Count then
        return nil
    end
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.itemTemplate, cellIdentifier)
    end
    CommonDefs.GetComponent_GameObject_Type(cell, typeof(CGuidelineMasterListItem)).Text = this.sections[index].text

    return cell
end
CLevelUpGuideMasterView.m_OnRowSelected_CS2LuaHook = function (this, index) 

    if index >= 0 and index < this.sections.Count then
        if this.OnSelected ~= nil then
            GenericDelegateInvoke(this.OnSelected, Table2ArrayWithCount({this.sections[index]}, 1, MakeArrayClass(Object)))
        end
    end
end
