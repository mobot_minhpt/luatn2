require("common/common_include")
local Vector3 = import "UnityEngine.Vector3"
local UIWidget = import "UIWidget"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local NGUIMath = import "NGUIMath"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local CPetInfoMgr = import "L10.UI.CPetInfoMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CBaseWnd = import "L10.UI.CBaseWnd"
local EnumObjectType = import "L10.Game.EnumObjectType"

LuaPetInfoWnd = class()

RegistClassMember(LuaPetInfoWnd, "m_ScrollView")
RegistClassMember(LuaPetInfoWnd, "m_Table")
RegistClassMember(LuaPetInfoWnd, "m_Template")
RegistClassMember(LuaPetInfoWnd, "m_Background")
RegistClassMember(LuaPetInfoWnd, "m_BaseWnd")

function LuaPetInfoWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaPetInfoWnd:Init( )
    local obj = CClientObjectMgr.Inst:GetObject(CPetInfoMgr.EngineId)
    if not obj or obj.ObjectType ~= EnumObjectType.Pet then
        self:Close()
        return
    end

    self.m_ScrollView = self.transform:Find("Anchor/Body"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("Anchor/Body/Table"):GetComponent(typeof(UITable))
    self.m_Template = self.transform:Find("Anchor/Body/Template").gameObject
    self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UIWidget))
    self.m_BaseWnd =  self.gameObject:GetComponent(typeof(CBaseWnd))

    Extensions.RemoveAllChildren(self.m_Table.transform)
    self:LayoutWnd()
    Gac2Gas.RequestPetDetails(CPetInfoMgr.EngineId)
end

function LuaPetInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnGetPetDetailInfo", self, "UpdatePetDetailInfo")
end

function LuaPetInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnGetPetDetailInfo", self, "UpdatePetDetailInfo")
end

function LuaPetInfoWnd:Update()
    if not self.m_BaseWnd then
        self.m_BaseWnd =  self.gameObject:GetComponent(typeof(CBaseWnd))
    end
    self.m_BaseWnd:ClickThroughToClose()
end

function LuaPetInfoWnd:UpdatePetDetailInfo(engineId, info)
    if CPetInfoMgr.EngineId ~= engineId then
        return
    end
    local obj = CClientObjectMgr.Inst:GetObject(CPetInfoMgr.EngineId)
    if not obj or obj.ObjectType ~= EnumObjectType.Pet then
        return
    end
    Extensions.RemoveAllChildren(self.m_Table.transform)
    local namecolor = "FFED5F"
    local valuecolor = "E8D0AA"

    local name = obj.Name
    --分身跟随主角的名字
    local ownerInfo = LuaGamePlayMgr:GetNameBoardInfo(obj.m_OwnerEngineId)
    if obj.IsShadow and ownerInfo and ownerInfo.displayName then
        name = ownerInfo.displayName
    end
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", namecolor, name))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d[-]", namecolor, LocalString.GetString("最大气血:"), valuecolor, math.floor(info.hp)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d-%d[-]", namecolor, LocalString.GetString("物理攻击:"), valuecolor, math.floor(info.pAttMin), math.floor(info.pAttMax)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d-%d[-]", namecolor, LocalString.GetString("法术攻击:"), valuecolor, math.floor(info.mAttMin), math.floor(info.mAttMax)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d[-]", namecolor, LocalString.GetString("防御(物/法):"), valuecolor, math.floor(info.pDef), math.floor(info.mDef)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d[-]", namecolor, LocalString.GetString("命中(物/法):"), valuecolor, math.floor(info.pHit), math.floor(info.mHit)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d[-]", namecolor, LocalString.GetString("躲避(物/法):"), valuecolor, math.floor(info.pMiss), math.floor(info.mMiss)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d[-]", namecolor, LocalString.GetString("致命(物/法):"), valuecolor, math.floor(info.pFatal),math.floor(info.mFatal)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s%%/%s%%[-]", namecolor, LocalString.GetString("致命加成(物/法):"), valuecolor, CUICommonDef.TruncateToString(info.pFatalDamage * 100, 1),CUICommonDef.TruncateToString(info.mFatalDamage * 100, 1)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s%%/%s%%[-]", namecolor, LocalString.GetString("减伤比例(物/法):"), valuecolor, CUICommonDef.TruncateToString(-info.MulpHurt * 100, 1),CUICommonDef.TruncateToString(-info.MulmHurt * 100, 1)))
        self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d[-]", namecolor, LocalString.GetString("破盾/格挡:"), valuecolor, math.floor(info.AntiBlock), math.floor(info.Block)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d/%d/%d[-]", namecolor, LocalString.GetString("忽视火光冰毒:"), valuecolor,
        math.floor(info.IgnoreAntiFire), math.floor(info.IgnoreAntiLight), math.floor(info.IgnoreAntiIce), math.floor(info.IgnoreAntiPoison)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d/%d/%d[-]", namecolor, LocalString.GetString("火毒电冰抗:"), valuecolor,
        math.floor(info.antiFire), math.floor(info.antiPoison), math.floor(info.antiThunder), math.floor(info.antiIce)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d/%d/%d[-]", namecolor, LocalString.GetString("风光幻水抗:"), valuecolor,
        math.floor(info.antiWind), math.floor(info.antiLight), math.floor(info.antiIllusion), math.floor(info.antiWater)))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d/%d/%d[-]", namecolor, LocalString.GetString("晕混定默抗:"), valuecolor,
        math.min(999,math.floor(info.antiDizzy)), math.min(999,math.floor(info.antiChaos)), math.min(999,math.floor(info.antiBind)), math.min(999,math.floor(info.antiSilence))))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%d/%d/%d/%d[-]", namecolor, LocalString.GetString("睡束狐石抗:"), valuecolor,
        math.min(999,math.floor(info.antiSleep)), math.min(999,math.floor(info.antiTie)), math.min(999,math.floor(info.antiBianHu)), math.min(999,math.floor(info.antiPetrify))))
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", namecolor, LocalString.GetString("存活时间:"), valuecolor, self:FormatLeftTime(info.leftTime)))
    self.m_Table:Reposition()
    self:LayoutWnd()
end

function LuaPetInfoWnd:FormatLeftTime(leftTime)
    leftTime = math.floor(leftTime)
    if leftTime < 60 then
        return SafeStringFormat3(LocalString.GetString("%d秒"), leftTime)
    elseif leftTime <3600 then
        return SafeStringFormat3(LocalString.GetString("%d分%d秒"), math.floor(leftTime/60), leftTime%60)
    else
        return SafeStringFormat3(LocalString.GetString("%d小时%d分%d秒"), math.floor(leftTime/3600), math.floor(leftTime%3600/60), leftTime%60)
    end
end

function LuaPetInfoWnd:AddItemToTable(text)
    local instance =  CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
    instance:SetActive(true)
    local label = instance:GetComponent(typeof(UILabel))
    label.text = SafeStringFormat3("[c]%s[/c]", text)
end

function LuaPetInfoWnd:LayoutWnd( )
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale
    local virtualScreenHeight = Screen.height * scale

    local verticalGap = 30

    local minHeightOfBody = 0
    local maxheightOfBody = virtualScreenHeight - verticalGap

    local contentHeight = self:TotalHeightOfScrollViewContent()
    local heightOfBody = math.min(math.max(contentHeight + self.m_ScrollView.panel.clipSoftness.y * 2, minHeightOfBody), maxheightOfBody)

    --设置背景高度
    self.m_Background.height = math.floor((heightOfBody + verticalGap))

    --设置Body位置
    self.m_ScrollView.panel:ResetAndUpdateAnchors()
    self.m_ScrollView:ResetPosition()
    Extensions.SetLocalPositionX(self.m_Table.transform, self.m_ScrollView.panel.baseClipRegion.x - NGUIMath.CalculateRelativeWidgetBounds(self.m_Table.transform).size.x * 0.5)

    --
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_Background.transform)
    local localPos = self.m_Background.transform.parent:InverseTransformPoint(CPetInfoMgr.NGUIWorldPos)
    local centerX = localPos.x
    --窗口顶部与点击位置对齐
    local centerY = localPos.y - b.size.y * 0.5 - 100
    --窗口顶部与点击位置对齐
    --左右不超出屏幕范围
    if centerX - b.size.x * 0.5 < - virtualScreenWidth * 0.5 then
        centerX = - virtualScreenWidth * 0.5 + b.size.x * 0.5
    end
    if centerX + b.size.x * 0.5 > (virtualScreenWidth * 0.5) then
        centerX = virtualScreenWidth * 0.5 - b.size.x * 0.5
    end
    --上下不超出屏幕范围
    if centerY + b.size.y * 0.5 > virtualScreenHeight * 0.5 then
        centerY = virtualScreenHeight * 0.5 - b.size.y * 0.5
    elseif centerY - b.size.y * 0.5 < - virtualScreenHeight * 0.5 then
        centerY = - virtualScreenHeight * 0.5 + b.size.y * 0.5
    end
    self.m_Background.transform.localPosition = Vector3(centerX, centerY, 0)
    self.m_ScrollView.panel:ResetAndUpdateAnchors()
    self.m_ScrollView:ResetPosition()
end

function LuaPetInfoWnd:TotalHeightOfScrollViewContent()
    local n = self.m_Table.transform.childCount
    local totalHeight = 0
    do
        local i = 0
        while i < n do
            totalHeight = totalHeight + self.m_Table.transform:GetChild(i):GetComponent(typeof(UIWidget)).localSize.y
            i = i + 1
        end
    end
    return totalHeight
end









