require("ui/lingshou/LuaLingShouBabySkillSlot")
local LuaGameObject=import "LuaGameObject"
local CLingShouBaseMgr=import "L10.Game.CLingShouBaseMgr"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local Gac2Gas=import "L10.Game.Gac2Gas"
local MessageMgr=import "L10.Game.MessageMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"

CLuaLingShouBabyPropertyDetailView =class()
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_FatherLabel")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_MotherLabel")
-- RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_BrotherLabel")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_SkillSlot1")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_SkillSlot2")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_ExpSlider")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_ExpLabel")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_AddExpButton")

RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_BabyInfo")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_XiuxiLabel")
RegistClassMember(CLuaLingShouBabyPropertyDetailView,"m_AdjHpLabel")


function CLuaLingShouBabyPropertyDetailView:ctor()
end

function CLuaLingShouBabyPropertyDetailView:Init( tf )

    self.m_SkillSlot1=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot1:Init(LuaGameObject.GetChildNoGC(tf,"Skill1").transform)
    self.m_SkillSlot2=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot2:Init(LuaGameObject.GetChildNoGC(tf,"Skill2").transform)
    
    self.m_FatherLabel=LuaGameObject.GetChildNoGC(tf,"FatherLabel").label
    self.m_FatherLabel.text=" "
    self.m_MotherLabel=LuaGameObject.GetChildNoGC(tf,"MotherLabel").label
    self.m_MotherLabel.text=" "
    -- self.m_BrotherLabel=LuaGameObject.GetChildNoGC(tf,"BrotherLabel").label
    -- self.m_BrotherLabel.text=" "

    self.m_ExpSlider=LuaGameObject.GetChildNoGC(tf,"ExpSlider").slider
    self.m_ExpSlider.value=0
    self.m_ExpLabel=LuaGameObject.GetChildNoGC(tf,"ExpLabel").label
    self.m_ExpLabel.text="0/0"

    self.m_AdjHpLabel=LuaGameObject.GetChildNoGC(tf,"AdjHpLabel").label
    self.m_AdjHpLabel.text="+0"
    local g = LuaGameObject.GetChildNoGC(tf,"AddExpBtn").gameObject
    UIEventListener.Get(g).onClick =  DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.LingShouBabyUseWnd)
    end)
    --放生
    local g = LuaGameObject.GetChildNoGC(tf,"FangshengBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        local content = MessageMgr.Inst:FormatMessage("LingShouBaby_FangSheng_Confirm",{})
        MessageWndManager.ShowDelayOKCancelMessage(content,
            DelegateFactory.Action(function ()
                Gac2Gas.RequestFangShengLingShouBaby(CLingShouMgr.Inst.selectedLingShou)
            end), nil, 10)
    end)
    --休息或出战
    self.m_XiuxiLabel=LuaGameObject.GetChildNoGC(tf,"XiuxiLabel").label
    local g = LuaGameObject.GetChildNoGC(tf,"XiuxiBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_BabyInfo then
            if self:IsShowBaby() then--休息
                Gac2Gas.RequestPackLingShouBaby(CLingShouMgr.Inst.selectedLingShou)
            else--召唤
                Gac2Gas.RequestSummonLingShouBaby(CLingShouMgr.Inst.selectedLingShou)
            end
        end
    end)
end
function CLuaLingShouBabyPropertyDetailView:IsShowBaby()
    if self.m_BabyInfo then
        return self.m_BabyInfo.IsShowBaby>0 and self.m_BabyInfo.IsNotAutoShowBaby==0
    else
        return false
    end
end
function CLuaLingShouBabyPropertyDetailView:InitData(fatherTemplateId,motherTemplateId,isFollowFather,babyInfo,lingShouPropertyFight)
    self.m_BabyInfo=babyInfo

    if self.m_BabyInfo then
        if self:IsShowBaby() then
            self.m_XiuxiLabel.text=LocalString.GetString("隐藏")
        else
            self.m_XiuxiLabel.text=LocalString.GetString("显示")
        end
    end

    if isFollowFather then
        self.m_FatherLabel.text=SafeStringFormat3(LocalString.GetString("%s [ffd739](跟随方)[-]"),CLingShouBaseMgr.GetLingShouName(fatherTemplateId))
        self.m_MotherLabel.text=CLingShouBaseMgr.GetLingShouName(motherTemplateId) or " "
    else
        self.m_FatherLabel.text=CLingShouBaseMgr.GetLingShouName(fatherTemplateId) or " "
        self.m_MotherLabel.text=SafeStringFormat3(LocalString.GetString("%s [ffd739](跟随方)[-]"),CLingShouBaseMgr.GetLingShouName(motherTemplateId))
    end

    self.m_AdjHpLabel.text="+"..tostring(CLuaLingShouMgr.GetLSBBAdjHp(babyInfo.Level,babyInfo.Quality))

    local skillId1=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[1])
    local skillId2=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[2])

    self.m_SkillSlot1:InitData(skillId1)
    self.m_SkillSlot2:InitData(skillId2)

    local needExp=LingShouBaby_BabyExp.Exists(babyInfo.Level) and LingShouBaby_BabyExp.GetData(babyInfo.Level).ExpFull or 0
    self.m_ExpSlider.value=babyInfo.Exp/needExp
    self.m_ExpLabel.text=SafeStringFormat3("%s/%s",babyInfo.Exp,needExp)
end
return CLuaLingShouBabyPropertyDetailView
