local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CMainCamera = import "L10.Engine.CMainCamera"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaZYPDGetSpiritWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZYPDGetSpiritWnd, "GetSpiritBtn", "GetSpiritBtn", GameObject)
RegistChildComponent(LuaZYPDGetSpiritWnd, "ButtonLabel", "ButtonLabel", UILabel)

--@endregion RegistChildComponent end

function LuaZYPDGetSpiritWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.GetSpiritBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetSpiritBtnClick()
	end)

    --@endregion EventBind end
    
    self:Update()
end

function LuaZYPDGetSpiritWnd:Init()

end

function LuaZYPDGetSpiritWnd:Update()
    if LuaZhongYuanJie2023Mgr.s_PickEngineId == 0 then return end
    local obj = CClientObjectMgr.Inst:GetObject(LuaZhongYuanJie2023Mgr.s_PickEngineId)
    if not obj then return end
    if obj.TemplateId == 37000483 then --灵石
        self.ButtonLabel.text = LocalString.GetString("拾取")
    elseif obj.TemplateId == 37000484 then --破碎书信
        self.ButtonLabel.text = LocalString.GetString("查看")
    elseif obj.TemplateId == 37000485 then --灵符堆
        self.ButtonLabel.text = LocalString.GetString("画符")
    elseif obj.TemplateId == 37000486 then --魂灯
        self.ButtonLabel.text = LocalString.GetString("查看")
    elseif obj.TemplateId == 37000487 then --杂物堆
        self.ButtonLabel.text = LocalString.GetString("翻找")
    end

    if not obj.RO.m_TopAnchorTransform then return end
    
    local pos = obj.RO.m_TopAnchorTransform.position
    -- local newpos = Vector3(pos.x,pos.y+1,pos.z)
    local viewPos = CMainCamera.Main:WorldToViewportPoint(pos)
    
    local screenPos
    if viewPos and viewPos.z and viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1 then
        screenPos = CMainCamera.Main:WorldToScreenPoint(pos)
    else  
        screenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end
    screenPos.z = 0

    local topWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)

    self.transform.position = topWorldPos
end

--@region UIEvent

function LuaZYPDGetSpiritWnd:OnGetSpiritBtnClick()
    if LuaZhongYuanJie2023Mgr.s_PickEngineId==0 then return end
    local obj = CClientObjectMgr.Inst:GetObject(LuaZhongYuanJie2023Mgr.s_PickEngineId)
    if not obj then return end
    if obj.TemplateId == 37000483 or obj.TemplateId == 37000484 or  obj.TemplateId == 37000485 or obj.TemplateId == 37000486 or obj.TemplateId == 37000487 then
        Gac2Gas.RequestPick(LuaZhongYuanJie2023Mgr.s_PickEngineId)
        CUIManager.CloseUI(CLuaUIResources.ZYPDGetSpiritWnd)
    end
end


--@endregion UIEvent

