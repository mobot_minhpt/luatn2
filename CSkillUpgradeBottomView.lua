-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuideMessager = import "L10.Game.Guide.CGuideMessager"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CSkillUpgradeBottomView = import "L10.UI.CSkillUpgradeBottomView"
local UpgradeButtonAction = import "L10.UI.CSkillUpgradeBottomView+UpgradeButtonAction"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CTooltip = import "L10.UI.CTooltip"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Item_Item = import "L10.Game.Item_Item"
local L10 = import "L10"
local LocalString = import "LocalString"
local Money = import "L10.Game.Money"
local Object = import "System.Object"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Skill_ColorSkill = import "L10.Game.Skill_ColorSkill"
local Skill_Setting = import "L10.Game.Skill_Setting"
local SoundManager = import "SoundManager"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local MsgPackImpl = import "MsgPackImpl"

CSkillUpgradeBottomView.m_Init_CS2LuaHook = function (this, skillClassId) 

    this:Clear()
    if CClientMainPlayer.Inst == nil then
        return
    end
    local skillClsChanged = this.curSkillClsId ~= skillClassId
    this.curSkillClsId = skillClassId
    this:InitColorSystemSkill(skillClassId)
    local checkBoxStatusChanged = (this.checkbox.Selected ~= not PlayerSettings.SkillSummaryDescEnabled)
    this.checkbox.Selected = not PlayerSettings.SkillSummaryDescEnabled
    this.xiuweiLabel.text = tostring(NumberTruncate(CClientMainPlayer.Inst.BasicProp.XiuWeiGrade, 1))

    local curLevel = 0
    local curLevelWithDelta = 0
    -- 获得当前等级
    if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(skillClassId) then
        curLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(CClientMainPlayer.Inst.SkillProp:GetOriginalSkillIdByCls(skillClassId))
        curLevelWithDelta = CLuaDesignMgr.Skill_AllSkills_GetLevel(CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(skillClassId, CClientMainPlayer.Inst.MaxLevel))
    end
    
    -- thi.curSkillId包含了当前等级
    this.curSkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClassId, curLevel)
    local curSkill = nil
    local nextSkill = nil
    if CSkillMgr.Inst:IsInSameColorSystem(this.lastColorSkillCls, skillClassId) then
        local colorSystemSkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(this.lastColorSkillCls, curLevel)
        curSkill = Skill_AllSkills.GetData(colorSystemSkillId)
        nextSkill = Skill_AllSkills.GetData(Skill_AllSkills.GetNextLevelSkillId(colorSystemSkillId))
    else
        -- 包含等级数据
        curSkill = Skill_AllSkills.GetData(this.curSkillId)
        nextSkill = Skill_AllSkills.GetData(Skill_AllSkills.GetNextLevelSkillId(this.curSkillId))
    end

    this:InitSkillNameAndTagsDisplay(curSkill, curLevel, curLevelWithDelta, nextSkill)

    if curSkill ~= nil then
        this:AddDescItem(curSkill, curLevelWithDelta - curLevel, true)
    end

    local is125Skill = false
    -- 判断是否为125技能
    local oneLevelSkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClassId, 1)
    local oneLevelData = Skill_AllSkills.GetData(oneLevelSkillId)
    if oneLevelData.PlayerLevel == 125 then
        is125Skill = true
    end

    local isAvailable = curLevel > 0

    -- 需要升级材料
    local needUpItem = false
    local extraItemId = 0
    if is125Skill and is125Skill then
        Skill_Skill125Upgrade.Foreach(
            function(key, data)
                if curLevel == data.Level and skillClassId == data.SkillCls then
                    extraItemId = data.ExtraItem[0]
                    needUpItem = true
                end
            end
        )
    end

    -- 是否已经学习
    if not isAvailable and not is125Skill then
        local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
        if nextSkill ~= nil and CommonDefs.DictContains(skillBooks, typeof(UInt32), nextSkill.SkillClass) and LuaSkillMgr.PreLevelConditionIsFit(nextSkill) and LuaSkillMgr.PreSkillConditionIsFit(nextSkill) then
            -- 显示110突破任务
            if oneLevelData.PlayerLevel == 110 then
                local taskFinished = this:playerFinishTupoTask()
                local taskReceived = this:playerRecevieTupoTask()
                this.learnTaskId = Skill_Setting.GetData().New110SkillRequireTaskId
                this.guideTaskId = Skill_Setting.GetData().New110SkillYinDaoTaskId
                if taskFinished then
                    this:ShowSkillBook()
                else
                    local str = LocalString.GetString("完成境界突破任务")
                    this:ShowTask(taskReceived, str)
                end
            else
                this:ShowSkillBook()
            end
        else
            this.hintLabel.text = LocalString.GetString("此技能尚未解锁")
            this.skillBook:SetActive(false)
            this.upgradeBtn.Text = LocalString.GetString("领悟技能")
            this.upgradeBtn.Enabled = false
        end
    elseif not isAvailable and is125Skill then
        if nextSkill ~= nil and LuaSkillMgr.PreLevelConditionIsFit(nextSkill) and LuaSkillMgr.PreSkillConditionIsFit(nextSkill) then
            -- 显示125任务
            this.learnTaskId = Skill_Setting.GetData().Skill125LearnTaskId
            this.guideTaskId = Skill_Setting.GetData().Skill125YinDaoTaskId
            local taskFinished = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(this.learnTaskId)
            local taskReceived = CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), this.learnTaskId) or 
                CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), this.guideTaskId) or 
                CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(this.guideTaskId)
            if taskFinished then
                this:ShowSkillBook()
            else
                local str = LocalString.GetString("当前等级达到125级且完成突破任务")
                this:ShowTask(taskReceived, str)
            end
        else
            this.hintLabel.text = LocalString.GetString("此技能尚未解锁")
            this.skillBook:SetActive(false)
            this.upgradeBtn.Text = LocalString.GetString("领悟技能")
            this.upgradeBtn.Enabled = false
        end
    elseif needUpItem then
        this:ShowItem(extraItemId)
    end

    if nextSkill ~= nil then
        if not PlayerSettings.SkillSummaryDescEnabled or curSkill == nil then
            this:AddDescItem(nextSkill, curLevelWithDelta - curLevel, false)
        end

        this.needExpLabel.text = tostring(nextSkill.AdjustedExperience)
        this.needMoneyLabel.text = tostring(nextSkill.AdjustedMoney)

        local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
        local exp = CClientMainPlayer.Inst.PlayProp.Exp

        local freeSilver = CClientMainPlayer.Inst.FreeSilver
        local silver = CClientMainPlayer.Inst.Silver
        local skillPrivateSilver = CClientMainPlayer.Inst.SkillPrivateSilver

        local qianshiExp = CClientMainPlayer.Inst.QianShiExp
        --前世经验

        this.expEnough = (skillPrivateExp + exp + qianshiExp) >= nextSkill.AdjustedExperience
        this.moneyEnough = (skillPrivateSilver + freeSilver + silver >= 0) and ((skillPrivateSilver + freeSilver + silver) >= nextSkill.AdjustedMoney)

        this.costHintLabel.text = this:GetTip(nextSkill)

        local reachMaxLevel = (nextSkill.Level > Constants.MaxSkillUpgradeLevel)
        if reachMaxLevel then
            this.hintLabel.text = LocalString.GetString("此技能已满级")
            this.skillBook:SetActive(false)
        end
        --优先消耗专有经验
        if CClientMainPlayer.Inst.SkillProp.SkillPrivateExp > 0 then
            this.ownExpIcon.spriteName = Money.GetIconName(EnumMoneyType.PrivateExp, EnumPlayScoreKey.NONE)
            this.needExpIcon.spriteName = this.ownExpIcon.spriteName
            this.ownExpLabel.text = tostring(skillPrivateExp)
        elseif CClientMainPlayer.Inst.QianShiExp > 0 then
            this.ownExpIcon.spriteName = Money.GetIconName(EnumMoneyType.QianShiExp, EnumPlayScoreKey.NONE)
            this.needExpIcon.spriteName = this.ownExpIcon.spriteName
            this.ownExpLabel.text = tostring(qianshiExp)
        else
            this.ownExpIcon.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
            this.needExpIcon.spriteName = this.ownExpIcon.spriteName
            this.ownExpLabel.text = tostring(exp)
        end
        --优先消耗银票
        if skillPrivateSilver > 0 then
            this.ownMoneyIcon.spriteName = Money.GetIconName(EnumMoneyType.SkillPrivateSilver, EnumPlayScoreKey.NONE)
            this.needMoneyIcon.spriteName = this.ownMoneyIcon.spriteName
            this.ownMoneyLabel.text = tostring(skillPrivateSilver)
        elseif freeSilver > 0 then
            this.ownMoneyIcon.spriteName = Money.GetIconName(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE)
            this.needMoneyIcon.spriteName = this.ownMoneyIcon.spriteName
            this.ownMoneyLabel.text = tostring(freeSilver)
        else
            this.ownMoneyIcon.spriteName = Money.GetIconName(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE)
            this.needMoneyIcon.spriteName = this.ownMoneyIcon.spriteName
            this.ownMoneyLabel.text = tostring(silver)
        end
        this.costRoot:SetActive(isAvailable and not reachMaxLevel and not needUpItem)
        if isAvailable and not needUpItem then
            this.upgradeBtn.Enabled = (this.expEnough and this.moneyEnough and not reachMaxLevel and isAvailable)
            this.upgradeBtn.Text = LocalString.GetString("升级")
            this.btnAction = UpgradeButtonAction.LevelUp
        end
    end
    this.descTable:Reposition()
    if skillClsChanged or checkBoxStatusChanged then
        this.descScrollView:ResetPosition()
    end
    if PlayerSettings.SkillSummaryDescEnabled then
        local contentHeight = NGUIMath.CalculateRelativeWidgetBounds(this.descTable.transform).size.y
        local scrollviewHeight = this.descScrollView.panel.baseClipRegion.w
        if contentHeight>=scrollviewHeight then return end
        local localCorners = this.descScrollView.panel.localCorners
        Extensions.SetLocalPositionY(this.descTable.transform, (localCorners[0].y + localCorners[1].y) * 0.5 + contentHeight * 0.5)
    end
end
CSkillUpgradeBottomView.m_GetTip_CS2LuaHook = function (this, nextSkill) 
    local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
    local exp = CClientMainPlayer.Inst.PlayProp.Exp

    local freeSilver = CClientMainPlayer.Inst.FreeSilver
    local silver = CClientMainPlayer.Inst.Silver
    local skillPrivateSilver = CClientMainPlayer.Inst.SkillPrivateSilver

    local qianshiExp = CClientMainPlayer.Inst.QianShiExp
    --前世经验

    local expEnough = (skillPrivateExp + exp + qianshiExp) >= nextSkill.AdjustedExperience
    local moneyEnough = (skillPrivateSilver + freeSilver + silver >= 0) and ((skillPrivateSilver + freeSilver + silver) >= nextSkill.AdjustedMoney)

    if expEnough and moneyEnough then
        local icons = ""
        --专有经验，前世经验、个人经验
        local privateExpNotEnough = (skillPrivateExp > 0 and skillPrivateExp < nextSkill.AdjustedExperience)
        --有前世经验 但是不够
        if skillPrivateExp == 0 then
            if qianshiExp == 0 then
                --显示个人经验
            else
                if qianshiExp < nextSkill.AdjustedExperience then
                    icons = icons .. "#290"
                    --个人经验图标
                end
            end
        else
            --显示专有经验图标
            if qianshiExp == 0 then
                if skillPrivateExp >= nextSkill.AdjustedExperience then
                else
                    icons = icons .. "#290"
                    --个人经验图标
                end
            else
                if (skillPrivateExp + qianshiExp) < nextSkill.AdjustedExperience then
                    icons = icons .. "#292"
                    --前世经验图标
                    icons = icons .. "#290"
                    --个人经验图标
                elseif (skillPrivateExp + qianshiExp) >= nextSkill.AdjustedExperience then
                    icons = icons .. "#292"
                    --前世经验图标
                end
            end
        end

        if skillPrivateSilver > 0 then
            if skillPrivateSilver < nextSkill.AdjustedMoney then
                if freeSilver + skillPrivateSilver >= nextSkill.AdjustedMoney then
                    icons = icons .. "#288"
                elseif freeSilver <= 0 then
                    icons = icons .. "#287"
                else
                    icons = icons .. "#288#287"
                end
            end
        elseif freeSilver > 0 then
            if freeSilver < nextSkill.AdjustedMoney then
                icons = icons .. "#287"
            end
        end

        if not System.String.IsNullOrEmpty(icons) then
            return System.String.Format(LocalString.GetString("不足的部分将从{0}中补足"), icons)
        end
    else
        return LocalString.GetString("[c][ff0000]不足的部分无法从#290或#287中补足[-][/c]")
    end

    return ""
end
CSkillUpgradeBottomView.m_InitSkillNameAndTagsDisplay_CS2LuaHook = function (this, curSkill, curLevel, curLevelWithDelta, nextSkill) 
    if this.tagSprites == nil then
        local childCount = this.tagsTable.transform.childCount
        this.tagSprites = CreateFromClass(MakeArrayClass(UISprite), childCount)
        do
            local i = 0
            while i < childCount do
                this.tagSprites[i] = CommonDefs.GetComponent_Component_Type(this.tagsTable.transform:GetChild(i), typeof(UISprite))
                UIEventListener.Get(this.tagSprites[i].gameObject).onClick = MakeDelegateFromCSFunction(this.OnTagSpriteClick, VoidDelegate, this)
                this.tagSprites[i].gameObject:SetActive(false)
                i = i + 1
            end
        end
    end
    do
        local i = 0
        while i < this.tagSprites.Length do
            this.tagSprites[i].gameObject:SetActive(false)
            i = i + 1
        end
    end

    CommonDefs.ListClear(this.tagSpriteNames)

    if curSkill ~= nil then
        if CSkillInfoMgr.s_ShowSkillDeltaLevelInSkillUpgradeView and curLevelWithDelta > curLevel then
            this.skillNameLabel.text = System.String.Format(LocalString.GetString("{0} {1}(+{2})级"), curSkill.Name, curSkill.Level, curLevelWithDelta - curLevel)
        else
            this.skillNameLabel.text = System.String.Format(LocalString.GetString("{0} {1}级"), curSkill.Name, curSkill.Level)
        end
        
        if this.skillNameBg then
            this.skillNameBg:ResetAndUpdateAnchors()
        end
        if curSkill.AttMode == "p" then
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttackMode_Wu)
        elseif curSkill.AttMode == "m" then
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttackMode_Fa)
        else
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttackMode_Fu)
        end

        if curSkill.AttExtMode ~= nil and curSkill.AttExtMode.Length > 0 then
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttExtMode_Dot)
            do
                local i = 0
                while i < curSkill.AttExtMode.Length do
                    CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), curSkill.AttExtMode[i])
                    i = i + 1
                end
            end
        end
    elseif nextSkill ~= nil then
        this.skillNameLabel.text = nextSkill.Name
        if this.skillNameBg then
            this.skillNameBg:ResetAndUpdateAnchors()
        end
        if nextSkill.AttMode == "p" then
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttackMode_Wu)
        elseif nextSkill.AttMode == "m" then
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttackMode_Fa)
        else
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttackMode_Fu)
        end

        if nextSkill.AttExtMode ~= nil and nextSkill.AttExtMode.Length > 0 then
            CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), CSkillUpgradeBottomView.Tag_AttExtMode_Dot)
            do
                local i = 0
                while i < nextSkill.AttExtMode.Length do
                    CommonDefs.ListAdd(this.tagSpriteNames, typeof(String), nextSkill.AttExtMode[i])
                    i = i + 1
                end
            end
        end
    end
    do
        local i = 0
        while i < this.tagSprites.Length do
            if i < this.tagSpriteNames.Count then
                this.tagSprites[i].gameObject:SetActive(true)
                this.tagSprites[i].spriteName = this.tagSpriteNames[i]
                this.tagSprites[i]:MakePixelPerfect()
            else
                this.tagSprites[i].gameObject:SetActive(false)
            end
            i = i + 1
        end
    end
    this.tagsTable:Reposition()
    LuaSkillUpgradeBottomView:UpdateSkillDetailBtnAnchor()
end

CSkillUpgradeBottomView.m_ShowTask_CS2LuaHook = function (this, taskReceived, taskLabel)
    this.skillTaskRoot:SetActive(true)
    this.skillBook:SetActive(false)
    this.skillItem:SetActive(false)
    this.btnAction = UpgradeButtonAction.GetTask
    this.skillTaskLabel.text = taskLabel

    if not taskReceived then
        this.upgradeBtn.Text = LocalString.GetString("领取任务")
        this.upgradeBtn.Enabled = true
    else
        this.upgradeBtn.Text = LocalString.GetString("已领取")
        this.upgradeBtn.Enabled = false
    end
end

CSkillUpgradeBottomView.m_ShowItem_CS2LuaHook = function (this, itemId)
    local nextSkill = Skill_AllSkills.GetData(Skill_AllSkills.GetNextLevelSkillId(this.curSkillId))
    this.skillTaskRoot:SetActive(false)
    this.skillBook:SetActive(false)
    this.btnAction = UpgradeButtonAction.ShowLevelUp
    this.upgradeBtn.Text = LocalString.GetString("下一步")
    local item = Item_Item.GetData(itemId)
    local itemCount = 1
    if item ~= nil then
        this.hintLabel.text = ""
        this.skillItem:SetActive(true)
        this.skillItemIcon:LoadMaterial(item.Icon)
        this.skillItemName.text = item.Name
        this.skillItemName.color = GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)
        local bindCount, notBindCount
        bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(item.ID)
        -- 判断是否足够
        if bindCount + notBindCount >= itemCount then
            this.skillItemCount.text = System.String.Format("{0}/{1}", bindCount + notBindCount, itemCount)
            this.skillItemMask:SetActive(false)
            this.upgradeBtn.Enabled = true
            CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
        else
            this.skillItemCount.text = System.String.Format("[c][FF0000]{0}[-]/{1}", bindCount + notBindCount, itemCount)
            this.skillItemMask:SetActive(true)
            UIEventListener.Get(this.skillItemMask).onClick = DelegateFactory.VoidDelegate(function (go) 
                CItemAccessListMgr.Inst:ShowItemAccessInfo(item.ID, false, go.transform, CTooltip.AlignType.Top)
            end)
            this.upgradeBtn.Enabled = false
        end
    else
        this.hintLabel.text = LocalString.GetString("此技能尚未解锁")
        this.skillItem:SetActive(false)
        this.upgradeBtn.Text = LocalString.GetString("领悟技能")
        this.upgradeBtn.Enabled = false
    end
end

CSkillUpgradeBottomView.m_ShowSkillBook_CS2LuaHook = function (this) 
    local nextSkill = Skill_AllSkills.GetData(Skill_AllSkills.GetNextLevelSkillId(this.curSkillId))
    local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
    this.skillItem:SetActive(false)
    this.skillTaskRoot:SetActive(false)
    this.btnAction = UpgradeButtonAction.LearnSkill
    -- 技能书相关信息 id:数量
    local pair = CommonDefs.DictGetValue(skillBooks, typeof(UInt32), nextSkill.SkillClass)
    local item = Item_Item.GetData(pair.Key)
    if item ~= nil then
        --hintLabel.text = MessageMgr.Inst.FormatMessage("Skill_not_get_need_book", new object[] { item.Name });
        this.hintLabel.text = ""
        this.skillBook:SetActive(true)
        this.skillBookIcon:LoadMaterial(item.Icon)
        this.skillBookName.text = item.Name
        this.skillBookName.color = GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)
        local bindCount, notBindCount
        bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(item.ID)
        if bindCount + notBindCount >= pair.Value then
            this.skillBookCount.text = System.String.Format("{0}/{1}", bindCount + notBindCount, pair.Value)
            this.skillBookMask:SetActive(false)
            this.upgradeBtn.Text = LocalString.GetString("领悟技能")
            this.upgradeBtn.Enabled = true
            CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
        else
            this.skillBookCount.text = System.String.Format("[c][FF0000]{0}[-]/{1}", bindCount + notBindCount, pair.Value)
            this.skillBookMask:SetActive(true)
            UIEventListener.Get(this.skillBookMask).onClick = DelegateFactory.VoidDelegate(function (go) 
                CItemAccessListMgr.Inst:ShowItemAccessInfo(item.ID, false, go.transform, CTooltip.AlignType.Top)
            end)
            this.upgradeBtn.Text = LocalString.GetString("领悟技能")
            this.upgradeBtn.Enabled = false
        end
    else
        this.hintLabel.text = LocalString.GetString("此技能尚未解锁")
        this.skillBook:SetActive(false)
        this.upgradeBtn.Text = LocalString.GetString("领悟技能")
        this.upgradeBtn.Enabled = false
    end
end
CSkillUpgradeBottomView.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.upgradeBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.upgradeBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnUpgradeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.upgradeInfoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.upgradeInfoBtn).onClick, MakeDelegateFromCSFunction(this.OnUpgradeInfoButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.checkboxArea).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.checkboxArea).onClick, MakeDelegateFromCSFunction(this.OnCheckboxValueChanged, VoidDelegate, this), true)

    do
        local i = 0
        while i < this.colorBtns.Length do
            UIEventListener.Get(this.colorBtns[i].gameObject).onClick = MakeDelegateFromCSFunction(this.OnColorButtonClick, VoidDelegate, this)
            i = i + 1
        end
    end
    --#region GUIDE
    --主动发事件
    UIEventListener.Get(this.scrollViewRegion).onClick = DelegateFactory.VoidDelegate(function (p) 
        local cmp = CommonDefs.GetComponent_GameObject_Type(this.skillInfoRegion, typeof(CGuideMessager))
        if cmp ~= nil then
            cmp:Click()
        end
    end)
    --#endregion
end
CSkillUpgradeBottomView.m_OnEnable_CS2LuaHook = function (this) 

    EventManager.AddListener(EnumEventType.MainPlayerPlayPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerPropUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerBasicPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerBasicPropUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnMainPlayerMoneyUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.AddListenerInternal(EnumEventType.SyncQianShiExp, MakeDelegateFromCSFunction(this.OnSyncQianShiExp, MakeGenericClass(Action1, UInt64), this))
    LuaSkillUpgradeBottomView:OnEnable(this)
end
CSkillUpgradeBottomView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.MainPlayerPlayPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerPropUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerBasicPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerBasicPropUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnMainPlayerMoneyUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SyncQianShiExp, MakeDelegateFromCSFunction(this.OnSyncQianShiExp, MakeGenericClass(Action1, UInt64), this))
    LuaSkillUpgradeBottomView:OnDisable()
end

CSkillUpgradeBottomView.m_OnUpgradeButtonClick_CS2LuaHook = function (this, go)
    if this.btnAction == UpgradeButtonAction.ShowLevelUp then
        this.skillItem:SetActive(false)
        this.skillTaskRoot:SetActive(false)
        this.upgradeBtn.Text = LocalString.GetString("升级")
        this.btnAction = UpgradeButtonAction.LevelUp
        this.upgradeBtn.Enabled = (this.expEnough and this.moneyEnough)
        this.costRoot:SetActive(true)
    elseif this.btnAction == UpgradeButtonAction.ShowSkillBook then
        this:ShowSkillBook()
    elseif this.btnAction == UpgradeButtonAction.LearnSkill then
        local nextSkill = Skill_AllSkills.GetData(Skill_AllSkills.GetNextLevelSkillId(this.curSkillId))
        local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
        local item = Item_Item.GetData(CommonDefs.DictGetValue(skillBooks, typeof(UInt32), nextSkill.SkillClass).Key)
        local pos
        local itemId
        local default
        local oneLevelSkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(this.curSkillClsId, 1)
        local oneLevelData = Skill_AllSkills.GetData(oneLevelSkillId)
        default, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, item.ID)
        if default then
            if oneLevelData.PlayerLevel == 125 then

                Gac2Gas.RequestLearnNew125Skill(nextSkill.SkillClass)
            elseif oneLevelData.PlayerLevel == 110 then
                -- body nextSkill.PlayerLevel >= Skill_Setting.GetData().New110SkillRequireGrade
                Gac2Gas.RequestLearnNew110Skill(nextSkill.SkillClass)
            else
                Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, pos, itemId, "")
            end
        end
    elseif this.btnAction == UpgradeButtonAction.LevelUp then
        local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))

        local cls = CLuaDesignMgr.Skill_AllSkills_GetClass(this.curSkillId)
        local level = CLuaDesignMgr.Skill_AllSkills_GetLevel(this.curSkillId)
        if CSkillMgr.Inst:IsColorSystemSkill(cls) and Skill_ColorSkill.Exists(cls) then
            local baseCls = Skill_ColorSkill.GetData(cls).BaseSkill_ID
            Gac2Gas.RequestUpgradeNormalSkill(CLuaDesignMgr.Skill_AllSkills_GetSkillId(baseCls, level), MsgPackImpl.pack(dict))
        else
            Gac2Gas.RequestUpgradeNormalSkill(this.curSkillId, MsgPackImpl.pack(dict))
        end
    elseif this.btnAction == UpgradeButtonAction.GetTask then
        if CClientMainPlayer.Inst == nil then
            return
        end
        local templateId = 0
        if CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(this.guideTaskId) then
            templateId = this.learnTaskId
        else
            templateId = this.guideTaskId
        end
        if CTaskMgr.Inst:IsTrackingToDoTask(templateId) then
            return
        end
        if CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.AcceptableTasks, typeof(UInt32), templateId) then
            CTaskMgr.Inst:TrackToDoTask(templateId)
        elseif CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), templateId) then
            this:Init(this.curSkillClsId)
        else
            g_MessageMgr:ShowMessage("TASK_IS_NOT_TRADED_LEVEL_LOW")
        end
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Update, Vector3.zero, nil, 0)
end
CSkillUpgradeBottomView.m_OnTagSpriteClick_CS2LuaHook = function (this, go) 

    if this.tagSprites == nil then
        return
    end
    do
        local i = 0
        while i < this.tagSprites.Length do
            if this.tagSprites[i].gameObject == go then
                if this.tagSprites[i].spriteName ~= CSkillUpgradeBottomView.Tag_AttExtMode_Dot then
                    g_MessageMgr:ShowMessage(this.tagSprites[i].spriteName)
                end
            end
            i = i + 1
        end
    end
end
CSkillUpgradeBottomView.m_PreConditionSkillsInfo_CS2LuaHook = function (this, pairs) 

    local sb = NewStringBuilderWraper(StringBuilder)
    if pairs == nil or pairs.Length == 0 then
        return nil
    end
    sb:Append(LocalString.GetString("前置技能"))
    sb:Append(" ")
    do
        local i = 0
        while i < pairs.Length do
            local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(pairs[i][0], pairs[i][1])
            local skillLevel = pairs[i][1]

            local levelReached = false
            local playerSkillId = 0
            local neededExp = 0
            local neededMoney = 0
            local default
            default, playerSkillId = CClientMainPlayer.Inst.SkillProp:TryGetOriginalSkillIdByCls(pairs[i][0])
            if default then
                if CLuaDesignMgr.Skill_AllSkills_GetLevel(playerSkillId) >= skillLevel then
                    levelReached = true
                else
                    neededExp = CSkillMgr.GetUpgradeNeededExp(playerSkillId, skillLevel)
                    neededMoney = CSkillMgr.GetUpgradeNeededMoney(playerSkillId, skillLevel)
                end
            else
                neededExp = CSkillMgr.GetUpgradeNeededExp(CLuaDesignMgr.Skill_AllSkills_GetSkillId(pairs[i][0], 1), skillLevel)
                --第一级无需经验处理
                neededMoney = CSkillMgr.GetUpgradeNeededMoney(CLuaDesignMgr.Skill_AllSkills_GetSkillId(pairs[i][0], 1), skillLevel)
                --第一级无需经验处理
            end
            sb:Append("[c]")
            sb:Append(levelReached and CSkillUpgradeBottomView.SKILL_UNLOCKED_COLOR or CSkillUpgradeBottomView.SKILL_LOCKED_COLOR)
            sb:Append(Skill_AllSkills.GetData(skillId).Name)
            if CommonDefs.IS_VN_CLIENT then
                sb:Append(" ")
            end
            sb:Append(System.String.Format(LocalString.GetString("{0}级"), skillLevel))
            sb:Append("[-]")
            if i < pairs.Length - 1 then
                sb:Append(LocalString.GetString("，"))
            end
            i = i + 1
        end
    end
    sb:Append("[/c]")
    return ToStringWrap(sb)
end
CSkillUpgradeBottomView.m_PreConditionLevelInfo_CS2LuaHook = function (this, skill) 

    if CClientMainPlayer.Inst == nil or skill == nil then
        return nil
    end
    local sb = NewStringBuilderWraper(StringBuilder)
    --检查玩家等级
    local needPlayerLevel = skill.PlayerLevel
    local levelReach = false
    if CClientMainPlayer.Inst.MaxLevel >= needPlayerLevel then
        levelReach = true
    end
    sb:Append(LocalString.GetString("需求等级 "))
    sb:Append("[c]")
    sb:Append(levelReach and CSkillUpgradeBottomView.SKILL_UNLOCKED_COLOR or CSkillUpgradeBottomView.SKILL_LOCKED_COLOR)
    CommonDefs.StringBuilder_Append_StringBuilder_UInt32(sb, skill.PlayerLevel)
    sb:Append("[-]")
    sb:Append("[/c]")
    return ToStringWrap(sb)
end
CSkillUpgradeBottomView.m_AddDescItem_CS2LuaHook = function (this, skill, skillDeltaLevel, curSkillPlace) 

    if skill == nil or CClientMainPlayer.Inst == nil then
        return
    end
    local builder = NewStringBuilderWraper(StringBuilder)

    if CSkillInfoMgr.s_ShowSkillDeltaLevelInSkillUpgradeView then
        local skillWithDelta = Skill_AllSkills.GetData(skill.ID + skillDeltaLevel)
        if skillWithDelta == nil then
            return
        end
        local default
        if (PlayerSettings.SkillSummaryDescEnabled and not System.String.IsNullOrEmpty(skillWithDelta.Display_jian)) then
            default = skillWithDelta.Display_jian
        else
            default = skillWithDelta:Get_Display_Internal(true, true, CClientMainPlayer.Inst:CalRealCD(skillWithDelta.CD))
        end
        builder:Append(CChatLinkMgr.TranslateToNGUIText(default, false))

        if curSkillPlace then
            this.curSkillItem.gameObject:SetActive(true)
            this.curSkillItem:Init_V2(0, ToStringWrap(builder), nil, nil)
        else
            this.nextSkillItem.gameObject:SetActive(true)
            local preConditionSkillsInfo = this:PreConditionSkillsInfo(skill.AdjustedPreSkills)
            --按老技能
            local preConditionLevelInfo = this:PreConditionLevelInfo(skill)
            --按老技能
            this.nextSkillItem:Init_V2(skillWithDelta.Level, ToStringWrap(builder), preConditionSkillsInfo, preConditionLevelInfo)
        end
        return
    end

    local extern
    if (PlayerSettings.SkillSummaryDescEnabled and not System.String.IsNullOrEmpty(skill.Display_jian)) then
        extern = skill.Display_jian
    else
        extern = skill:Get_Display_Internal(true, true, CClientMainPlayer.Inst:CalRealCD(skill.CD))
    end
    builder:Append(CChatLinkMgr.TranslateToNGUIText(extern, false))

    if curSkillPlace then
        this.curSkillItem.gameObject:SetActive(true)
        this.curSkillItem:Init(ToStringWrap(builder), nil, nil)
    else
        this.nextSkillItem.gameObject:SetActive(true)
        local preConditionSkillsInfo = this:PreConditionSkillsInfo(skill.AdjustedPreSkills)
        local preConditionLevelInfo = this:PreConditionLevelInfo(skill)
        this.nextSkillItem:Init(ToStringWrap(builder), preConditionSkillsInfo, preConditionLevelInfo)
    end
end
CSkillUpgradeBottomView.m_Clear_CS2LuaHook = function (this) 
    this.costRoot:SetActive(false)
    this.skillItem:SetActive(false)
    this.skillNameLabel.text = nil
    this.xiuweiLabel.text = nil
    this.curSkillItem.gameObject:SetActive(false)
    this.nextSkillItem.gameObject:SetActive(false)
    this.needExpLabel.text = nil
    this.ownExpLabel.text = nil
    this.needMoneyLabel.text = nil
    this.ownMoneyLabel.text = nil
    this.hintLabel.text = nil
    this.skillBook:SetActive(false)
    this.upgradeBtn.Enabled = false
    this.skillTaskRoot:SetActive(false)

    this.costHintLabel.text = nil
end
CSkillUpgradeBottomView.m_InitColorSystemSkill_CS2LuaHook = function (this, skillCls) 
    local idx = - 1
    if CSkillMgr.Inst:IsColorSystemBaseSkill(skillCls) then
        idx = 0
    elseif CSkillMgr.Inst:IsColorSystemSkill(skillCls) then
        idx = Skill_ColorSkill.GetData(skillCls).Color_ID
    end

    if idx >= 0 then
        if not CSkillMgr.Inst:IsInSameColorSystem(this.lastColorSkillCls, skillCls) then
            this.lastColorSkillCls = skillCls
        end
        this.colorSystemRoot:SetActive(true)
        Extensions.SetLocalPositionX(this.skillDescRoot.transform, this.colorSkillDescOffset)

        local curIdx = CSkillMgr.Inst:IsColorSystemBaseSkill(this.lastColorSkillCls) and 0 or Skill_ColorSkill.GetData(this.lastColorSkillCls).Color_ID

        do
            local i = 0
            while i < this.colorBtns.Length do
                this.colorBtns[i].Selected = (curIdx == i)
                i = i + 1
            end
        end
    else
        this.lastColorSkillCls = 0
        this.colorSystemRoot:SetActive(false)
        Extensions.SetLocalPositionX(this.skillDescRoot.transform, this.normalSkillDescOffset)
    end
end
CSkillUpgradeBottomView.m_OnColorButtonClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.colorBtns.Length do
            if this.colorBtns[i].gameObject == go then
                this.lastColorSkillCls = CSkillMgr.Inst:GetReplaceColorSystemSkillCls(this.lastColorSkillCls, i)
                this:Init(CLuaDesignMgr.Skill_AllSkills_GetClass(this.curSkillId))
                break
            end
            i = i + 1
        end
    end
end
CSkillUpgradeBottomView.m_GetUpgradeButton_CS2LuaHook = function (this) 

    if not this.upgradeBtn.Enabled then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        return nil
    end
    return this.upgradeBtn.gameObject
end
LuaSkillUpgradeBottomView = {}
LuaSkillUpgradeBottomView.view = nil
LuaSkillUpgradeBottomView.detailBtn = nil

function LuaSkillUpgradeBottomView:OnEnable(view)
    self.view = view
    self.detailBtn = self.view.transform:Find("DetailButton"):GetComponent(typeof(UIWidget))
end

function LuaSkillUpgradeBottomView:OnDisable()
    self.view = nil
    self.detailBtn = nil
end

function LuaSkillUpgradeBottomView:UpdateSkillDetailBtnAnchor()
    if not self.detailBtn or not LuaPlayerPropertyMgr.PropertyGuide then return end
    self.detailBtn:ResetAndUpdateAnchors()
end