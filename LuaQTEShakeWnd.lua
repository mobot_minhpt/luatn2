local UITexture = import "UITexture"
local UnityEngine = import "UnityEngine"

LuaQTEShakeWnd = class()
RegistClassMember(LuaQTEShakeWnd, "CountDownBar")
RegistClassMember(LuaQTEShakeWnd, "curTime")
RegistClassMember(LuaQTEShakeWnd, "totTime")

function LuaQTEShakeWnd:Awake()
    self.CountDownBar = self.transform:Find("CountDownBar"):GetComponent(typeof(UITexture))

    self.curTime, self.totTime = 0, 0
    self.gameObject:SetActive(false)
end

function LuaQTEShakeWnd:Init()
    if LuaQTEMgr.CurrentQTEType ~= EnumQTEType.Shake then
        return
    end

    self.curTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteTimeOffset)
    self.totTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteDuration)

    self.transform.localPosition =
        LuaQTEMgr.ParsePosition(self.gameObject, LuaQTEMgr.CurrentQTEInfo.xPos, LuaQTEMgr.CurrentQTEInfo.yPos)

    self.gameObject:SetActive(true)
end

function LuaQTEShakeWnd:Update()
    local deltaTime = UnityEngine.Time.deltaTime
    self.curTime = self.curTime + deltaTime
    local timeNow = math.min(self.totTime, math.max(self.curTime, 0.0))

    local ratio = 0.0
    if self.totTime ~= 0 then
        ratio = 1 - timeNow / self.totTime
    end
    self.CountDownBar.fillAmount = ratio

    if ratio <=0 then
        self:QTEFail()
    end
end

function LuaQTEShakeWnd:OnEnable()
    g_ScriptEvent:AddListener("PlayerShakeDevice", self, "OnShaking")
end

function LuaQTEShakeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("PlayerShakeDevice", self, "OnShaking")
end

function LuaQTEShakeWnd:OnShaking()
    self:QTESuccess()
end

function LuaQTEShakeWnd:QTESuccess()
    LuaQTEMgr.TriggerQTEFinishFx(true, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(true)
    self:Reset()
end

function LuaQTEShakeWnd:QTEFail()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(false)
    self:Reset()
end

function LuaQTEShakeWnd:OnTimeLimitExceeded()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    self:Reset()
end

function LuaQTEShakeWnd:Reset()
    self.curTime, self.totTime = 0, 0
    self.gameObject:SetActive(false)
    g_ScriptEvent:BroadcastInLua("OnQTESubWndClose")
end
