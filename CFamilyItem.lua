-- Auto Generated!!
local BoxCollider = import "UnityEngine.BoxCollider"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFamilyItem = import "L10.UI.CFamilyItem"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local FamilyRelationShip = import "L10.Game.FamilyRelationShip"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local UITexture = import "UITexture"

CFamilyItem.guestColor = NGUIText.ParseColor24("a9bebb", 0)
CFamilyItem.shifuColor = NGUIText.ParseColor24("c19dd1", 0)
CFamilyItem.shifuwifeColor = NGUIText.ParseColor24("c19dd1", 0)
CFamilyItem.brotherColor = NGUIText.ParseColor24("9493d6", 0)
CFamilyItem.apprenticeColor = NGUIText.ParseColor24("8ea4df", 0)
CFamilyItem.laseapprenticeColor = NGUIText.ParseColor24("9da4b4", 0)
local showZhiJiInfo = function(this)
    if this.member.PlayerID ~= 0 then
        this.NameLabel.text = this.member.Name
        this.TitleLabel.text = LocalString.GetString("知己")
    else
        this.NameLabel.text = LocalString.GetString("缔结知己")
    end
end

CFamilyItem.m_SetMemberInfo_CS2LuaHook = function (this, memberInfo, elder, apprenticeIndex, apprenticeCnt)
    this.member = memberInfo
    this.elder = elder
    local goodTuDiGo = this.transform:Find("GoodTuDi")
    if goodTuDiGo then
        local isGoodTudi = LuaShiTuMgr.m_FamilyTreeShiMenInfo and LuaShiTuMgr.m_FamilyTreeShiMenInfo.bestTudiIdSet and LuaShiTuMgr.m_FamilyTreeShiMenInfo.bestTudiIdSet[this.member.PlayerID]
        goodTuDiGo.gameObject:SetActive(isGoodTudi)
    end
    local collider = CommonDefs.GetComponent_Component_Type(this, typeof(BoxCollider))
    if this.member.PlayerID == 0 then
        this.portraitTexture.gameObject:SetActive(false)
        this.TitleLabel.gameObject:SetActive(false)
        if CFamilyTreeMgr.Instance.CenterPlayerID == CClientMainPlayer.Inst.Id then
            this.GuideSprite.gameObject:SetActive(true)
            this.EmptyLabel.gameObject:SetActive(false)
            this.NameLabel.gameObject:SetActive(true)
            collider.enabled = true
        else
            this.GuideSprite.gameObject:SetActive(false)
            this.EmptyLabel.gameObject:SetActive(true)
            this.NameLabel.gameObject:SetActive(false)
            collider.enabled = false
        end
    else
        if collider then
            collider.enabled = true
        end
        if this.EmptyLabel then
            this.EmptyLabel.gameObject:SetActive(false)
        end
        this.portraitTexture.gameObject:SetActive(false)
        this.GuideSprite.gameObject:SetActive(false)
        this.portraitTexture.gameObject:SetActive(true)
        this.TitleLabel.gameObject:SetActive(true)
        local pname = CUICommonDef.GetPortraitName(this.member.Clazz, this.member.Gender, this.member.Expression)
        this.portraitTexture:LoadNPCPortrait(pname, false)
        this.NameLabel.gameObject:SetActive(true)
        this.NameLabel.text = this.member.Name
    end
    local uitex = CommonDefs.GetComponent_Component_Type(this, typeof(UITexture))
    this:showTextAndBGInfo(apprenticeIndex, apprenticeCnt)
    this:showBGColor()
    this.LocationNode:SetActive(false)
    --if (memberInfo.shareIPLocation)
    --{
    if CommonDefs.DictContains(CFamilyTreeMgr.Instance.playerLocationDic, typeof(UInt64), this.member.PlayerID) then
        if not System.String.IsNullOrEmpty(CommonDefs.DictGetValue(CFamilyTreeMgr.Instance.playerLocationDic, typeof(UInt64), this.member.PlayerID)) then
            this.LocationNode:SetActive(true)
            CommonDefs.GetComponent_Component_Type(this.LocationNode.transform:Find("text"), typeof(UILabel)).text = CommonDefs.DictGetValue(CFamilyTreeMgr.Instance.playerLocationDic, typeof(UInt64), this.member.PlayerID)
        end
    end
end
CFamilyItem.m_showTextAndBGInfo_CS2LuaHook = function (this, apprenticeIndex, apprenticeCnt)
    repeat
        local default = this.member.Relationship
        if default == FamilyRelationShip.Self then
            this:showSelfInfo()
            break
        elseif default == FamilyRelationShip.Wife then
            this:showWifeInfo()
            break
        elseif default == FamilyRelationShip.Engaged then
            this:showEngagedInfo()
            break
        elseif default == FamilyRelationShip.Shifu then
            this:showShifuInfo()
            break
        elseif default == FamilyRelationShip.Shifu_Wife then
            this:showShifuWifeInfo()
            break
        elseif default == FamilyRelationShip.ClassMate then
            this:showClassMateInfo()
            break
        elseif default == FamilyRelationShip.Brother then
            this:showBrotherInfo()
            break
        elseif default == FamilyRelationShip.Apprentice or default == FamilyRelationShip.LastApprentice then
            this:showApprenticeInfo(apprenticeIndex, apprenticeCnt)
            break
        elseif default == FamilyRelationShip.Guest then
            this:showGuestInfo()
            break
        elseif default == FamilyRelationShip.HouseMaster then
            this:showHouseMaster()
            break
        elseif default == FamilyRelationShip.ZhiJi then
            showZhiJiInfo(this)
            break
        end
    until 1
end
CFamilyItem.m_showBGColor_CS2LuaHook = function (this)
    local uitex = CommonDefs.GetComponent_Component_Type(this, typeof(UITexture))
    repeat
        local default = this.member.Relationship
        if default == FamilyRelationShip.Wife then
            uitex.color = CFamilyItem.wifeColor
            break
        elseif default == FamilyRelationShip.Engaged then
            uitex.color = CFamilyItem.wifeColor
            break
        elseif default == FamilyRelationShip.Shifu or default == FamilyRelationShip.LastMaster then
            uitex.color = CFamilyItem.shifuColor
            break
        elseif default == FamilyRelationShip.Shifu_Wife then
            uitex.color = CFamilyItem.shifuwifeColor
            break
        elseif default == FamilyRelationShip.ClassMate then
            uitex.color = CFamilyItem.classmateColor
            break
        elseif default == FamilyRelationShip.Brother then
            uitex.color = CFamilyItem.brotherColor
            break
        elseif default == FamilyRelationShip.Apprentice then
            uitex.color = CFamilyItem.apprenticeColor
            break
        elseif default == FamilyRelationShip.LastApprentice then
            uitex.color = CFamilyItem.laseapprenticeColor
            break
        elseif default == FamilyRelationShip.Guest then
            uitex.color = CFamilyItem.guestColor
            break
        elseif default == FamilyRelationShip.HouseMaster then
            uitex.color = CFamilyItem.guestColor
            break
        end
    until 1
end
CFamilyItem.m_showWifeInfo_CS2LuaHook = function (this)
    if this.member.PlayerID ~= 0 then
        --   GetComponent<UITexture>().color = wifeColor;
        this.NameLabel.text = this.member.Name
        if this.member.Gender == 0 then
            this.TitleLabel.text = LocalString.GetString("夫君")
        else
            this.TitleLabel.text = LocalString.GetString("娘子")
        end
    else
        this.NameLabel.text = LocalString.GetString("点击结婚")
    end
end
CFamilyItem.m_showEngagedInfo_CS2LuaHook = function (this)
    if this.member.PlayerID ~= 0 then
        --     GetComponent<UITexture>().color = wifeColor;
        if this.member.Gender == 0 then
            this.TitleLabel.text = LocalString.GetString("未婚夫")
        else
            this.TitleLabel.text = LocalString.GetString("未婚妻")
        end
    else
        this.NameLabel.text = LocalString.GetString("点击结婚")
    end
end
CFamilyItem.m_showShifuInfo_CS2LuaHook = function (this)
    local uitex = CommonDefs.GetComponent_Component_Type(this, typeof(UITexture))
    if this.member.PlayerID ~= 0 then
        --       GetComponent<UITexture>().color = shifuColor;
        this.NameLabel.text = this.member.Name
        this.TitleLabel.text = LocalString.GetString("师父")
        uitex.width = 130
        uitex.height = 130
    else
        uitex.width = 116
        uitex.height = 116
        this.NameLabel.text = LocalString.GetString("点击拜师")
    end
end
CFamilyItem.m_showShifuWifeInfo_CS2LuaHook = function (this)
    if this.member.PlayerID ~= 0 then
        --      GetComponent<UITexture>().color = shifuwifeColor;
        this.NameLabel.text = this.member.Name
        if this.member.PlayerID ~= CClientMainPlayer.Inst.Id then
          if this.member.Gender == 0 then
            this.TitleLabel.text = LocalString.GetString("师公")
          else
            this.TitleLabel.text = LocalString.GetString("师娘")
          end
        else
          this.TitleLabel.text = ""
        end
    end
end
CFamilyItem.m_showClassMateInfo_CS2LuaHook = function (this)
    if this.member.PlayerID ~= 0 then
        --  GetComponent<UITexture>().color = classmateColor;
        this.NameLabel.text = this.member.Name
        if this.elder then
            if this.member.Gender == 0 then
                this.TitleLabel.text = LocalString.GetString("师兄")
            else
                this.TitleLabel.text = LocalString.GetString("师姐")
            end
        else
            if this.member.Gender == 0 then
                this.TitleLabel.text = LocalString.GetString("师弟")
            else
                this.TitleLabel.text = LocalString.GetString("师妹")
            end
        end
    end
end
CFamilyItem.m_showBrotherInfo_CS2LuaHook = function (this)
    if this.member.PlayerID ~= 0 then
        --      GetComponent<UITexture>().color = brotherColor;
        this.NameLabel.text = this.member.Name
        -- TitleLabel.text = CJieBaiMgr.Inst.GetExistJieBaiRelationByID(member.PlayerID, false);
        this.TitleLabel.text = CJieBaiMgr.Inst:GetJieBaiTitle(this.member.Index, not this.elder, this.member.Gender, false)
    else
        this.NameLabel.text = LocalString.GetString("点击结拜")
    end
end
CFamilyItem.m_showApprenticeInfo_CS2LuaHook = function (this, apprenticeIndex, apprenticeCnt)
    if this.member.PlayerID ~= 0 then
        this.NameLabel.text = this.member.Name
        if this.member.Relationship == FamilyRelationShip.LastApprentice then
            CommonDefs.GetComponent_Component_Type(this, typeof(UITexture)).color = CFamilyItem.laseapprenticeColor
            this.TitleLabel.text = LocalString.GetString("已出师")
        else
            CommonDefs.GetComponent_Component_Type(this, typeof(UITexture)).color = CFamilyItem.apprenticeColor
            if CFamilyTreeMgr.Instance:CheckRuShiPlayer(this.member.PlayerID) then
              this.TitleLabel.text = LocalString.GetString("入室弟子")
            else
              this.TitleLabel.text = CFamilyItem.s_ApprenticeTitle[apprenticeCnt - 1][apprenticeIndex] .. LocalString.GetString("徒弟")
            end
        end
      else
        this.NameLabel.text = LocalString.GetString("点击收徒")
    end
end
CFamilyItem.m_showGuestInfo_CS2LuaHook = function (this)
    if this.member.PlayerID ~= 0 then
        --      GetComponent<UITexture>().color = guestColor;
        this.NameLabel.text = this.member.Name
        this.TitleLabel.text = LocalString.GetString("房客")
    end
end
CFamilyItem.m_showHouseMaster_CS2LuaHook = function (this)
    if this.member.PlayerID ~= 0 then
        --      GetComponent<UITexture>().color = guestColor;
        this.NameLabel.text = this.member.Name
        this.TitleLabel.text = LocalString.GetString("房主")
    end
end
CFamilyItem.m_OnClick_CS2LuaHook = function (this)
    if this.member.PlayerID > 0 then
        if this.member.Relationship ~= FamilyRelationShip.Self then
            if this.member.PlayerID ~= CClientMainPlayer.Inst.Id then
                --  Transform parent = transform.parent;
                --     transform.parent = UICamera.currentCamera.transform;
                local v = this.transform.position
                CPlayerInfoMgr.ShowPlayerPopupMenu(this.member.PlayerID, EnumPlayerInfoContext.FamilyTree, EChatPanel.Undefined, nil, nil, v, CPlayerInfoMgr.AlignType.Bottom)
                --      transform.parent = parent;
            else
                CFamilyTreeMgr.Instance:SendFamilyTreeRequest(this.member.PlayerID)
            end
        end
    else
        local message = ""
        repeat
            local default = this.member.Relationship
            if default == FamilyRelationShip.Wife or default == FamilyRelationShip.Engaged then
                message = g_MessageMgr:FormatMessage("LEAD_TO_JIEHUN")
                break
            elseif default == FamilyRelationShip.Shifu then
                message = g_MessageMgr:FormatMessage("LEAD_TO_BAISHI")
                break
            elseif default == FamilyRelationShip.Apprentice then
                message = g_MessageMgr:FormatMessage("LEAD_TO_SHOUTU")
                break
            elseif default == FamilyRelationShip.Brother then
                message = g_MessageMgr:FormatMessage("LEAD_TO_JIEBAI")
                break
            elseif default == FamilyRelationShip.ZhiJi then
                message = g_MessageMgr:FormatMessage("PSZJ_NPC_GO")
                break
            end
        until 1
        MessageWndManager.ShowOKCancelMessage(message, MakeDelegateFromCSFunction(this.onOkButtonClick, Action0, this), nil, nil, nil, false)
    end
    return
end
CFamilyItem.m_onOkButtonClick_CS2LuaHook = function (this)
    repeat
        local default = this.member.Relationship
        if default == FamilyRelationShip.Wife or default == FamilyRelationShip.Engaged then
            CTrackMgr.Inst:FindNPC(Constants.SanshengshiID, Constants.HangZhouID, 0, 0, nil, nil)
            break
        elseif default == FamilyRelationShip.Shifu then
            CTrackMgr.Inst:FindNPC(Constants.LaofuziID, Constants.JinLing, 0, 0, nil, nil)
            break
        elseif default == FamilyRelationShip.Apprentice then
            CTrackMgr.Inst:FindNPC(Constants.LaofuziID, Constants.JinLing, 0, 0, nil, nil)
            break
        elseif default == FamilyRelationShip.Brother then
            CTrackMgr.Inst:FindNPC(Constants.XianganID, Constants.PuJiaCun, 0, 0, nil, nil)
            break
        elseif default == FamilyRelationShip.ZhiJi then
            local npc = ZhouNianQing2023_PSZJSetting.GetData().PSZJ_NPC
            CTrackMgr.Inst:FindNPC(npc[0], npc[1], npc[2], npc[3], nil, nil)
            break
        end
    until 1
    CUIManager.CloseUI(CUIResources.FamilyTreeWnd)
end
