local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Vector3 = import "Vector3"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTextInput = import "L10.UI.CTextInput"
local CItemMgr = import "L10.Game.CItemMgr"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local EChatPanel = import "L10.Game.EChatPanel"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local Utility = import "L10.Engine.Utility"
local CUICommonDef = import "L10.UI.CUICommonDef"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DateTime = import "System.DateTime"
local CultureInfo = import "System.Globalization.CultureInfo"

LuaWishItemWnd = class()
RegistChildComponent(LuaWishItemWnd,"CloseButton", GameObject)
RegistChildComponent(LuaWishItemWnd,"emotionBtn", GameObject)
RegistChildComponent(LuaWishItemWnd,"statusInput", CTextInput)
RegistChildComponent(LuaWishItemWnd,"addNode", GameObject)
RegistChildComponent(LuaWishItemWnd,"QnIncAndDec", QnAddSubAndInputButton)
RegistChildComponent(LuaWishItemWnd,"QnCost", CCurentMoneyCtrl)
RegistChildComponent(LuaWishItemWnd,"restedu", UILabel)
RegistChildComponent(LuaWishItemWnd,"tipBtn", GameObject)
RegistChildComponent(LuaWishItemWnd,"usedu", UILabel)
RegistChildComponent(LuaWishItemWnd,"textLabel", UILabel)

RegistClassMember(LuaWishItemWnd, "m_DefaultChatInputListener")
RegistClassMember(LuaWishItemWnd, "TimeTick")
RegistClassMember(LuaWishItemWnd, "InfoNode")
RegistClassMember(LuaWishItemWnd, "InfoData")

function LuaWishItemWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.WishItemWnd)
end

function LuaWishItemWnd:ParseTime(leftTime)
	if leftTime <= 0 then
		return LocalString.GetString("已过期")
	end

	if leftTime < 24 * 3600 then
		local hour = math.floor(leftTime / 3600)
		local minute = math.floor(leftTime % 3600 / 60)
		local sec = leftTime % 60
		return SafeStringFormat3("%d:%d:%d", hour, minute, sec)
	else
		--local days = math.floor(leftTime / (24 * 3600))
		--return SafeStringFormat3(LocalString.GetString("%d天"), days)
		local days = math.floor(leftTime / (24 * 3600))
		local hour = math.floor((leftTime % (24 * 3600)) / 3600)
		local minute = math.floor(leftTime % 3600 / 60)
		local sec = leftTime % 60
		return SafeStringFormat3(LocalString.GetString("%d天 %d:%d:%d"), days,hour,minute,sec)
	end
end

function LuaWishItemWnd:TimeCountDown()
  local node = self.InfoNode
  local data = self.InfoData
  if node and data then
		if data.currentProgress >= data.totalProgress then
			node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已实现')
		else
			local leftTime = data.endTime/1000 - CServerTimeMgr.Inst.timeStamp
			local restDay = math.floor(leftTime/3600/24)
			if restDay > 0 then
				--node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('剩余实现时间 [c][E8E918]%s天[-]'),restDay)
				node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('剩余实现时间 [c][E8E918]%s[-]'),self:ParseTime(leftTime))
			else
				node.transform:Find('restTime'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('剩余实现时间 [c][FF0000]%s[-]'),self:ParseTime(leftTime))
			end
		end
  end
end

function LuaWishItemWnd:OnEnable()
  if self.TimeTick then
    UnRegisterTick(self.TimeTick)
    self.TimeTick = nil
  end
  self.TimeTick = RegisterTick(function ()
    self:TimeCountDown()
  end, 1000)
end

function LuaWishItemWnd:OnDisable()
  if self.TimeTick then
    UnRegisterTick(self.TimeTick)
    self.TimeTick = nil
  end
end

function LuaWishItemWnd:OnEmotionButtonClick(go)
  CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, go, EChatPanel.Undefined, 0, 0)
end

function LuaWishItemWnd:OnSendZhuliCost(msg)
  if msg then
    msg = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(msg, true)
    if System.String.IsNullOrEmpty(msg) then
      g_MessageMgr:ShowMessage("Speech_Violation")
      return
    end
    msg = CChatMgr.Inst:FilterYangYangEmotion(msg)
  else
    msg = ''
  end

  if self.QnCost.moneyEnough then
    local data = LuaWishMgr.CurrentWishData
    local msgArray = Utility.BreakString(msg, 2, 255, nil)
    local regionId = EShopMallRegion_lua.ELingyuMall
    local cost = self.QnCost:GetCost()
    Gac2Gas.Wish_AddHelp(data.roleId,data.roleName or data.roleId,data.wishId,regionId,data.templateId,cost,0,msgArray[0],msgArray[1])
    --CUIManager.CloseUI(CLuaUIResources.WishSelfFillWnd)
		self:Close()
  else
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function ()
			CShopMallMgr.ShowChargeWnd()
    end), nil, nil, nil, false)
  end
end

function LuaWishItemWnd:OnSendZhuli(msg)
  msg = CUICommonDef.Trim(msg)
  --  LuaPersonalSpaceMgrReal.AddWishComment(LuaWishMgr.CurrentWishData.wishId,msg,function(data)
  --    self:AddWordBack(data)
  --  end)
  if System.String.IsNullOrEmpty(msg) then
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("你还没有留下你的助力留言，确定要直接助力吗？"), DelegateFactory.Action(function ()
      self:OnSendZhuliCost()
    end), nil, nil, nil, false)
  else
    self:OnSendZhuliCost(msg)
  end

end

function LuaWishItemWnd:SendBtnClick()
	local cost = self.QnCost:GetCost()
	if cost > LuaWishMgr.MonthSendRest then
		g_MessageMgr:ShowMessage('WISH_ADDHELP_FAILED_PRESENT_LIMIT',LuaWishMgr.MonthSendRest)
	else
		local msg = self.statusInput.transform:Find('StatusText'):GetComponent(typeof(UIInput)).value
		self:OnSendZhuli(msg)
	end
end

function LuaWishItemWnd:InitAddNode()
  local data = LuaWishMgr.CurrentWishData
  local node = self.addNode
  local itemData = CItemMgr.Inst:GetItemTemplate(data.templateId)
  if not itemData then
    node:SetActive(false)
    return
  end
  local itemNum = data.num
	if itemNum and itemNum > 1 then
		node.transform:Find('num'):GetComponent(typeof(UILabel)).text = itemNum
	else
		node.transform:Find('num'):GetComponent(typeof(UILabel)).text = ''
	end
  node.transform:Find('pic'):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
  node.transform:Find('name'):GetComponent(typeof(UILabel)).text = data.roleName
  node.transform:Find('desc'):GetComponent(typeof(UILabel)).text = itemData.Name

  self.InfoNode = node
  self.InfoData = data
  self:TimeCountDown()
--  self.TimeTick = RegisterTick(function ()
--    self:TimeCountDown()
--  end, 1000)
end

function LuaWishItemWnd:SetZhuliNum(value,oneStep)
  local data = LuaWishMgr.CurrentWishData
  local node = self.addNode
  local cost = value * oneStep
  local totalrest = data.totalProgress - data.currentProgress
  if cost > totalrest then
    cost = totalrest
  end

  self.QnCost:SetCost(cost)

  local oldpercent = math.floor(data.currentProgress * 1000 / data.totalProgress) / 10

  local percent = math.floor((data.currentProgress + cost) * 1000 / data.totalProgress) / 10
  if percent > 100 then
    percent = 100
  end
  local maxLength = 717
  local nowLength = maxLength * (data.currentProgress + cost) / data.totalProgress
  if nowLength > maxLength then
    nowLength = maxLength
  end

  node.transform:Find('sliderBg/sliderNew'):GetComponent(typeof(UISprite)).width = nowLength
  node.transform:Find('desc'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('进度 [c][E8E918]%s%%→%s%%[-]'),oldpercent,percent)
end

function LuaWishItemWnd:InitQnIncAndDecBtn()
  local data = LuaWishMgr.CurrentWishData
  self.QnCost:SetCost(data.totalProgress)

--  local oneStep = math.ceil(data.totalProgress/10)
--  local maxNum = math.ceil((data.totalProgress - data.currentProgress)/oneStep)
--  if maxNum < 1 then
--    self:Close()
--    return
--  end
--
--  self.QnIncAndDec.onValueChanged = DelegateFactory.Action_uint(function(v)
--    self:SetZhuliNum(v,oneStep)
--  end)
--
--  self.QnIncAndDec:SetMinMax(1,maxNum,1)
--  self.QnIncAndDec:SetValue(1,true)
end

function LuaWishItemWnd:GetText(text)
  local showText = CChatMgr.Inst:FilterYangYangEmotion(text)
  showText = CChatLinkMgr.TranslateToNGUIText(showText, false)
	return showText
end

function LuaWishItemWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)


	local onTipClick = function(go)
    g_MessageMgr:ShowMessage('WishZhuliTip')
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

  if not LuaWishMgr.CurrentWishData then
    self:Close()
    return
  end

  if not self.m_DefaultChatInputListener then
    self.m_DefaultChatInputListener = LuaPersonalSpaceMgrReal.GetChatListener(self.statusInput)
  end

  local data = LuaWishMgr.CurrentWishData
	local statusNode = self.textLabel
  statusNode.text = self:GetText(LocalString.GetString('[c][FFFE91]心愿语：[-] ')..data.text)
	UIEventListener.Get(statusNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
		local index = statusNode:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
		if index == 0 then
			index = - 1
		end

		local url = statusNode:GetUrlAtCharacterIndex(index)
		if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then

			return
		end
	end)

  self:InitAddNode()

  UIEventListener.Get(self.emotionBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    self:OnEmotionButtonClick()
  end)

  self.statusInput.OnSend = DelegateFactory.Action_string(function (msg)
    self:OnSendZhuli(msg)
  end)

  self.restedu.text = LuaWishMgr.MonthSendRest

	self:CalUseDu()

  self.QnCost:SetCost(0)
  self:InitQnIncAndDecBtn()

	self:UpdateTextRegion()
end

function LuaWishItemWnd:UpdateTextRegion()
	local textHeight = self.textLabel.height
	self.statusInput.transform:Find('StatusText'):GetComponent(typeof(UISprite)).height = 290 - textHeight
	local labelTrans = self.statusInput.transform:Find('StatusText/Label').transform
	labelTrans.localPosition = Vector3(labelTrans.localPosition.x,280-textHeight,labelTrans.localPosition.z)
end

function LuaWishItemWnd:CalUseDu()
  local calTime = function(beginTime,endTime)
    local beginOfTheBuyDay = DateTime.ParseExact(beginTime, "yyyy-MM-dd-HH-mm", CultureInfo.CurrentCulture)
    local endOfTheBuyDay = DateTime.ParseExact(endTime, "yyyy-MM-dd-HH-mm", CultureInfo.CurrentCulture)

    local now = CServerTimeMgr.Inst:GetZone8Time()
    local seconds = beginOfTheBuyDay:Subtract(now).TotalSeconds
    local endSeconds = endOfTheBuyDay:Subtract(now).TotalSeconds
    if seconds < 0 and endSeconds > 0 then
      return true
    end

    return false
  end

  local data = LuaWishMgr.CurrentWishData
	if data and data.templateId > 0 then
		local mallData = Mall_LingYuMall.GetData(data.templateId)
		local price = mallData.Jade
		local num = data.num
		local discount = 1
		if mallData then
			local discountItem = Mall_ZengsongDiscountItem.GetData(mallData.ID)
			if discountItem and discountItem.Status == 0 then
				if calTime(discountItem.DiscountBeginTime,discountItem.DiscountEndTime) then
					 discount = discountItem.DiscountNum / 10
				end
			else
				local discountCategory = Mall_ZengsongDiscountCategory.GetData(mallData.SubCategory)
				if discountCategory and discountCategory.Status == 0 then
					if calTime(discountCategory.DiscountBeginTime,discountCategory.DiscountEndTime) then
						discount = discountCategory.DiscountNum / 10
					end
				end
			end
			self.usedu.text = discount * price * num
		else
			self.usedu.text = '0'
		end
	else
		self.usedu.text = '0'
	end
end

return LuaWishItemWnd
