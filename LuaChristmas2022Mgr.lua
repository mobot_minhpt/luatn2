local CScene = import "L10.Game.CScene"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CMoveScalingEffect = import "L10.Game.CMoveScalingEffect"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CActivityMgr = import "L10.Game.CActivityMgr"
local Utility = import "L10.Engine.Utility"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CMiniMapMarkTemplate = import "L10.UI.CMiniMapMarkTemplate"
local EMinimapCategory = import "L10.UI.EMinimapCategory"
local EMinimapMarkType = import "L10.UI.EMinimapMarkType"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local PlayerSettings = import "L10.Game.PlayerSettings"
local SoundManager = import "SoundManager"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local Quaternion = import "UnityEngine.Quaternion"

LuaChristmas2022Mgr = {}
LuaChristmas2022Mgr.m_PlayStatus = nil
LuaChristmas2022Mgr.m_IsWin = false
LuaChristmas2022Mgr.m_Score = 0
LuaChristmas2022Mgr.m_Rank = 0
LuaChristmas2022Mgr.m_SelfDamage = 0
LuaChristmas2022Mgr.m_TeamTotalDamage = 0
LuaChristmas2022Mgr.m_KillTurkeyTime = 0
LuaChristmas2022Mgr.m_IsFirstDamager = false
LuaChristmas2022Mgr.m_AddHeadInfoEnginedID = false
LuaChristmas2022Mgr.m_SnowManInfos = {}
LuaChristmas2022Mgr.m_SnowBallFxId = 88804278
LuaChristmas2022Mgr.m_PlayStage = nil
LuaChristmas2022Mgr.m_EndTime = nil
LuaChristmas2022Mgr.m_CurGiftTimes = {}
LuaChristmas2022Mgr.m_MusicName = "event:/L10/L10_Cine/L10_Cine_jingle_bell2022"
LuaChristmas2022Mgr.m_Sound = nil
LuaChristmas2022Mgr.m_AnimStartTick = nil
LuaChristmas2022Mgr.m_MusciFinishTick = nil
LuaChristmas2022Mgr.m_MainWndOpened = false
LuaChristmas2022Mgr.IconRes = "UI/Texture/FestivalActivity/Festival_Christmas/Christmas2022/Material/turkeyluckystartemplate_xingyunxing.mat"
LuaChristmas2022Mgr.m_EngineId2BrokeInfo = {}

function LuaChristmas2022Mgr:UpdateTurkeyMatchSnowmanHp(snowmanTeamId, curHp, curHpFull, maxDamagePlayerName)
    self.m_SnowManInfos[snowmanTeamId] = {curHp, curHpFull, maxDamagePlayerName}
    g_ScriptEvent:BroadcastInLua("UpdateTurkeyMatchSnowmanHp", snowmanTeamId, curHp, curHpFull, maxDamagePlayerName)
end

function LuaChristmas2022Mgr:SyncTurkeyMatchPlayStatus(playStatus)
    self.m_PlayStatus = playStatus
    if self:IsTurkeyMatchPlaySecondStage() then
        CUIManager.CloseUI(CLuaUIResources.TurkeyMatchSkillWnd)
        CUIManager.CloseUI(CLuaUIResources.TurkeyMatchStateWnd)
        self:ClearHeadInfo()
    end
    g_ScriptEvent:BroadcastInLua("UpdateGamePlayStageInfo", Christmas2022_Setting.GetData().GamePlayId, self:GetTurkeyMatchTaskTitle(), self:GetTurkeyMatchTaskContent())
end

function LuaChristmas2022Mgr:IsTurkeyMatchPlaySecondStage()
    return self.m_PlayStatus and self.m_PlayStatus >= EnumTurkeyMatchPlayState.eInterval
end


function LuaChristmas2022Mgr:SyncTurkeyMatchSnowballInfo(SnowballInfo)
    -- SnowballInfo:playerId, snowBallCurPushTime(ms) 
    for i = 1, #SnowballInfo, 2 do
        local playerId = SnowballInfo[i]
        local snowBallCurPushTime = SnowballInfo[i+1]
        local clientObj = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if clientObj and clientObj.RO then
            if CStatusMgr.Inst:GetStatus(clientObj, EPropStatus.GetId("TurkeyMatchPushSnowball")) == 1 and not clientObj.RO:ContainFx("TurkeySnowball") then
                local snowBallFxId = 88804278
                local fx = CEffectMgr.Inst:AddObjectFX(snowBallFxId, clientObj.RO, 0, clientObj.RO.Scale, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                clientObj.RO:AddFX("TurkeySnowball", fx)
            end
            local fx = (clientObj.RO:GetFxById(self.m_SnowBallFxId) or {}).m_FX
            if fx then
                local effect = CommonDefs.GetComponentInChildren_GameObject_Type(fx.gameObject, typeof(CMoveScalingEffect))
                if effect then
                    effect.m_Time = snowBallCurPushTime * 0.001
                end
            end
        end
    end
end

function LuaChristmas2022Mgr:SyncTurkeyMatchSuperLuckyStarInfo(SuperLuckyStarInfo)
    for i = 1, #SuperLuckyStarInfo, 3 do
        LuaGamePlayMgr:SetMiniMapMarkInfo(SuperLuckyStarInfo[i],  {
            res = self.IconRes, 
            x = SuperLuckyStarInfo[i+1], 
            y = SuperLuckyStarInfo[i+2], 
            size = {
                {width = 32, height = 32},
                {width = 75, height = 75},
            },
        } , true)
    end
end

function LuaChristmas2022Mgr:SyncTurkeyMatchSuperLuckyStarKick(SuperLuckyStarEngineId)
    LuaGamePlayMgr:SetMiniMapMarkInfo(SuperLuckyStarEngineId, nil, true)
end

function LuaChristmas2022Mgr:SyncTurkeyMatchResult(isWin, score, rank, selfDamage, teamTotalDamage, isFirstDamager, killTurkeyTime)
    self.m_IsWin = isWin
    self.m_Score = score
    self.m_Rank = rank
    self.m_SelfDamage = selfDamage
    self.m_TeamTotalDamage = teamTotalDamage
    self.m_IsFirstDamager = isFirstDamager
    self.m_KillTurkeyTime = killTurkeyTime
    
    CUIManager.ShowUI(CLuaUIResources.TurkeyMatchResultWnd)
end

function LuaChristmas2022Mgr:SyncTurkeyMatchSnowballBroke(playerId1, playerId2)
    local func = function (playerId)
        local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if obj and obj.RO then
            local offset = Vector3(0, 0, 1)
            local quaternion = Quaternion.Euler(0, obj.RO.Direction, 0)
            local pos = CommonDefs.op_Addition_Vector3_Vector3(obj.RO.Position , CommonDefs.op_Multiply_Quaternion_Vector3(quaternion, offset))
            local fx = CEffectMgr.Inst:AddWorldPositionFX(88804333, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 1, 1, nil)
        end
    end
    if playerId1 == playerId2 then
        func(playerId1)
    else
        func(playerId1)
        func(playerId2)
    end
end

function LuaChristmas2022Mgr:QueryPlayerTurkeyMatchInterfaceData_Result(dailyAwardTimes, firstKillAwardTimes, firstTeamInfos)
    local infos = {}
    if firstTeamInfos then
        for i = 1, #firstTeamInfos, 4 do
            table.insert(infos, {playerId = firstTeamInfos[i], name = firstTeamInfos[i + 1], class = firstTeamInfos[i + 2], gender = firstTeamInfos[i + 3]})
        end
    end

    g_ScriptEvent:BroadcastInLua("QueryPlayerTurkeyMatchInterfaceData_Result", dailyAwardTimes, firstKillAwardTimes, infos)
end

function LuaChristmas2022Mgr:QueryTurkeyMatchRankData_Result(rankPos, firstStageScore, secondStageScore, rankData)
    -- rankData:rankPos, playerId, firstStageScore, secondStageScore, name ...
    local infos = {}
    for i = 1, #rankData, 5 do
        local info = {}
        info.rankPos = rankData[i]
        info.playerId = rankData[i + 1]
        info.firstStageScore = rankData[i + 2]
        info.secondStageScore = rankData[i + 3]
        info.name = rankData[i + 4]

        table.insert(infos, info)
    end

    g_ScriptEvent:BroadcastInLua("QueryTurkeyMatchRankData_Result", rankPos, firstStageScore, secondStageScore, infos)
end

function LuaChristmas2022Mgr:OnAddBuffFX(args)
    local enginedId = args[0]
    local buffId = args[1]
    local endTime = args[2]
    local clientObj = CClientObjectMgr.Inst:GetObject(enginedId)
    if clientObj and (buffId == 67021101 or buffId == 67021201) then
        local args = {}
        args[0] = enginedId
        args[1] = {}
        args[1].textureSize = {133, 133}
        args[4] = true
        args[5] = "TurKeyMatch"
        local format = "UI/Texture/FestivalActivity/Festival_Christmas/Christmas2022/Material/christmas2022mainwnd_fuben_xuhua0%d.mat"
        if buffId == 67021101 then
            args[1].texturePath = SafeStringFormat3(format, 2)
            self.m_AddHeadInfoEnginedID = true
            g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
        elseif buffId == 67021201 then
            args[1].texturePath = SafeStringFormat3(format, 3)
            args[2] = {}
            args[2].LabelSize = 40 
            args[2].LabelLocalPosition = Vector3(0, 4, 0)
            args[3] = {}
            args[3].content = endTime + CServerTimeMgr.Inst.timeStamp
            args[3].autoTick = true
            args[3].autoRemove = true
            self.m_AddHeadInfoEnginedID = true
            g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
        end
    end
end

function LuaChristmas2022Mgr:ClearHeadInfo()
    if not LuaChristmas2022Mgr.m_AddHeadInfoEnginedID then return end
    local args = {}
    args[4] = false
    args[5] = "TurKeyMatch"
    g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
    LuaChristmas2022Mgr.m_AddHeadInfoEnginedID = false
end

function LuaChristmas2022Mgr:InitTurkeyMatchSkillWnd()
    if self:IsTurkeyMatchPlaySecondStage() then
        CUIManager.CloseUI(CLuaUIResources.TurkeyMatchSkillWnd)
    else
        if not CUIManager.IsLoaded(CLuaUIResources.TurkeyMatchSkillWnd) then
            CUIManager.ShowUI(CLuaUIResources.TurkeyMatchSkillWnd)
        end
    end
end

function LuaChristmas2022Mgr:OnMainPlayerCreated()
    self:InitTurkeyMatchSkillWnd()
    EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
    g_ScriptEvent:AddListener("OnAddBuffFX", self, "OnAddBuffFX")
end

function LuaChristmas2022Mgr:OnMainPlayerDestroyed()
    self.m_SnowManInfos = {}
    self.m_CurGiftTimes = {}
    self.m_EngineId2BrokeInfo = {}
    g_ScriptEvent:RemoveListener("OnAddBuffFX", self, "OnAddBuffFX")
end

function LuaChristmas2022Mgr:SyncChristmasKindnessPlayStage(playStage, leftTime)
    self.m_PlayStage = playStage
    self.m_EndTime = leftTime + CServerTimeMgr.Inst.timeStamp
    if playStage == 0 then
        CActivityMgr.Inst:DeleteActivity("eChristmasKindness")
        return
    end

    self:UpdateAcitivity()
end

function LuaChristmas2022Mgr:SyncChristmasKindnessGiftTimes(firstStageGiftTimes, secondStageGiftTimes, thirdStageGiftTimes)
    self.m_CurGiftTimes = {firstStageGiftTimes, secondStageGiftTimes, thirdStageGiftTimes}

    self:UpdateAcitivity()
end

function LuaChristmas2022Mgr:UpdateAcitivity()
    local content = g_MessageMgr:FormatMessage("Christmas2022_ChristmasKindness_ActivityInfo", self.m_PlayStage or 1, 3)
    CActivityMgr.Inst:UpdateAcitivity("eChristmasKindness", content, 51103060, nil, self.m_EndTime, LocalString.GetString("[圣诞]温暖平安夜"), false)
end

function LuaChristmas2022Mgr:StartChristmasKindnessGuildPlay()
    Gac2Gas.QueryCanStartChristmasKindnessGuildPlay()
end

function LuaChristmas2022Mgr:QueryCanStartChristmasKindnessGuildPlay_Result(isCanStart)
    if isCanStart then
        local message = g_MessageMgr:FormatMessage("Christmas2022_ChristmasKindness_Open_MakeSure")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            Gac2Gas.StartChristmasKindnessGuildPlay()
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end
end

function LuaChristmas2022Mgr:SyncChristmasKindnessStartUI()
    if self.m_MusicName then
        SoundManager.Inst:ChangeBgMusicVolume("ChristmasKindnessStart", 0, false)
        local volum = PlayerSettings.MusicEnabled and PlayerSettings.VolumeSetting or 0
        self.m_Sound = SoundManager.Inst:PlaySound(self.m_MusicName, Vector3.zero, volum, nil, 0)
        local slen = SoundManager.Inst:GetEventLength(self.m_Sound)
        local animationDelay = 3000
        UnRegisterTick(self.m_AnimStartTick)
        self.m_AnimStartTick = RegisterTickOnce(function()
            CUIManager.ShowUI(CLuaUIResources.WarmChristmasEveWnd)
        end, animationDelay)

        UnRegisterTick(self.m_MusciFinishTick)
        self.m_MusciFinishTick = RegisterTickOnce(function()
            SoundManager.Inst:ChangeBgMusicVolume("ChristmasKindnessStart", 1, true)
            SoundManager.Inst:StopSound(self.m_Sound)
        end, slen)
    end
end


function LuaChristmas2022Mgr:GetTurkeyMatchPlayId()
    return Christmas2022_Setting.GetData().GamePlayId
end

function LuaChristmas2022Mgr:GetTurkeyMatchTaskTitle()
    if not self:IsTurkeyMatchPlaySecondStage() then
        return LocalString.GetString("[融雪逐麋鹿]激战")
    else
        return LocalString.GetString("[融雪逐麋鹿]神鹿临")
    end
end

function LuaChristmas2022Mgr:GetTurkeyMatchTaskContent()
    if not self:IsTurkeyMatchPlaySecondStage() then
        local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
        return g_MessageMgr:FormatMessage(myForce == 1 and "Christmas2022_TurkeyMatchTip_Team1" or "Christmas2022_TurkeyMatchTip_Team2")
    else
        if self.m_PlayStatus == 8 then 
            return CChatLinkMgr.TranslateToNGUIText(LocalString.GetString("击败圣诞神鹿，获取礼物！#r进度：1/1"), false)
        else
            return CChatLinkMgr.TranslateToNGUIText(LocalString.GetString("击败圣诞神鹿，获取礼物！#r进度：0/1"), false)
        end
    end
end

function LuaChristmas2022Mgr:OnClickTurkeyMatchTask()
    g_MessageMgr:ShowMessage("Christmas2022_TurkeyMatch_Rules")
end

function LuaChristmas2022Mgr:IsInTurkeyMatch()
    if CScene.MainScene then
        local GameplayId = Christmas2022_Setting.GetData().GamePlayId
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == GameplayId then
           return true
        end
    end
    return false
end