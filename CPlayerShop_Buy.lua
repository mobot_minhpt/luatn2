-- Auto Generated!!
local Boolean = import "System.Boolean"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_Buy = import "L10.UI.CPlayerShop_Buy"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopTemplateItem = import "L10.UI.CPlayerShopTemplateItem"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemType = import "L10.Game.EnumItemType"
local EventManager = import "EventManager"
local IdPartition = import "L10.Game.IdPartition"
local Int32 = import "System.Int32"
local ItemTemplateInfo = import "L10.UI.ItemTemplateInfo"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local UInt32 = import "System.UInt32"
local QnButtonState = import "L10.UI.QnButton+QnButtonState"
CPlayerShop_Buy.m_Init_CS2LuaHook = function (this)
    this.m_BuySectionData = CPlayerShopMgr.Inst:GetItemGroupNames()
    CommonDefs.DictClear(this.m_BuySubSectionData)
    do
        local i = 0
        while i < this.m_BuySectionData.Count do
            local subSectionNames = CPlayerShopMgr.Inst:GetItemSubTypeNames(this.m_BuySectionData[i])
            CommonDefs.DictAdd(this.m_BuySubSectionData, typeof(String), this.m_BuySectionData[i], typeof(MakeGenericClass(List, String)), subSectionNames)
            i = i + 1
        end
    end
    this.m_BuySectionTableView:Init(this.m_BuySectionData, this.m_BuySubSectionData, false)
    if CYuanbaoMarketMgr.SearchTemplateId ~= 0 then
		local templateId = CYuanbaoMarketMgr.SearchTemplateId
		local section = 0
	    local row = 0
        local realTID = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
	    if IdPartition.IdIsEquip(realTID) then
	        if IdPartition.IdIsTalisman(realTID) then
	            section = 1
	            row = 2
            else
                if CYuanbaoMarketMgr.SearchEquipValueable == SearchOption.Valueable then
                    section = 1
                    row = 1
                else
                    section = 1
	                row = 0
                end
	        end
	    elseif IdPartition.IdIsItem(realTID) then
	        local data = CItemMgr.Inst:GetItemTemplate(realTID)
	        if data ~= nil then
	            local groupName = CPlayerShopMgr.Inst:GetItemGroupName(data.Type)
	            local typeName = CPlayerShopMgr.Inst:GetItemTypeName(data.Type)
	            section = CommonDefs.ListIndexOf(this.m_BuySectionData, groupName)
	            if section > 0 then
	                row = CommonDefs.ListIndexOf(CommonDefs.DictGetValue(this.m_BuySubSectionData, typeof(String), groupName), typeName)
	                if row < 0 then
	                    row = 0
	                end
	            else
	                section = 0
	            end
	        end
	    end
        this.m_BuySectionTableView:SelectRowIndex(section, row)
        this:GotoShopItemInfo(CYuanbaoMarketMgr.SearchTemplateId, SearchOption.All)
    elseif CYuanbaoMarketMgr.PlayerShopBuySectionIdx ~= 0 or CYuanbaoMarketMgr.PlayerShopBuyRowIdx ~= 0 then
        this.m_BuySectionTableView:SelectRowIndex(CYuanbaoMarketMgr.PlayerShopBuySectionIdx, CYuanbaoMarketMgr.PlayerShopBuyRowIdx)
        if this.m_BuySectionTableView.OnBuySectionSelect == nil then
            this:OnBuySectionSelect(CYuanbaoMarketMgr.PlayerShopBuySectionIdx, CYuanbaoMarketMgr.PlayerShopBuyRowIdx)
        end
    end
    local billButton = this.transform:Find("BillButton")
    if billButton then
        UIEventListener.Get(billButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CPlayerShopMgr.ShowPlayerShopBillWnd()
        end)
    end
end
CPlayerShop_Buy.m_OnDisable_CS2LuaHook = function (this) 
    this.m_BuySectionTableView.OnBuySectionSelect = CommonDefs.CombineListner_Action_int_int(this.m_BuySectionTableView.OnBuySectionSelect, MakeDelegateFromCSFunction(this.OnBuySectionSelect, MakeGenericClass(Action2, Int32, Int32), this), false)
    this.m_ShopItemTemplateTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_ShopItemTemplateTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectTemplateItem, MakeGenericClass(Action1, Int32), this), false)
    this.m_SearchButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SearchButton.OnClick, MakeDelegateFromCSFunction(this.OnSearch, MakeGenericClass(Action1, QnButton), this), false)
    CYuanbaoMarketMgr.ClearPlayerShopData()

    this.m_EquipSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_EquipSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_EquipSearch.OnSearchWordsCallback = CommonDefs.CombineListner_Action_string_uint_uint_uint_SearchOption(this.m_EquipSearch.OnSearchWordsCallback, MakeDelegateFromCSFunction(this.OnSearchWordsCallBack, MakeGenericClass(Action5, String, UInt32, UInt32, UInt32, SearchOption), this), false)
    this.m_NormalSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_NormalSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_TalismanSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_TalismanSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_FurnitureSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_FurnitureSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_FurnitureSearch.OnSearchColorCallback = CommonDefs.CombineListner_Action_int_int_int(this.m_FurnitureSearch.OnSearchColorCallback, MakeDelegateFromCSFunction(this.OnSearchColorCallBack, MakeGenericClass(Action3, Int32, Int32, Int32), this), false)
    this.m_ZhushabiSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_ZhushabiSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_FocusButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_FocusButton.OnClick, MakeDelegateFromCSFunction(this.OnFocusButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
end
CPlayerShop_Buy.m_OnFocusButtonClick_CS2LuaHook = function (this, button) 
    if this.m_IsEditMode then
        button.Text = LocalString.GetString("关注一类")
        this.m_IsEditMode = false
        this.m_SelectTemplateIndex = - 1
        --保存关注数据
        CPlayerShopMgr.Inst:SetAllFocusTemplate(this.m_CurFocusData)
        this.m_ShopItemTemplateTable:ReloadData(true, true)
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注信息同步成功"))
    else
        this.m_IsEditMode = true
        button.Text = LocalString.GetString("确认关注")
        this.m_ShopItemTemplateTable:ReloadData(true, true)
    end
    if this.m_FocusTip ~= nil then
        this.m_FocusTip:SetActive(this.m_IsEditMode)
    end
end
CPlayerShop_Buy.m_OnSearch_CS2LuaHook = function (this, button)
    this.m_SearchButton:SetSelected(true, false)
    this.m_BuySectionTableView:CancleSelection()
    this:GotoNormalSearch()
end
CPlayerShop_Buy.m_OnBuySectionSelect_CS2LuaHook = function (this, section, subsection)    this.m_SearchButton:SetSelected(false, false)
    if section == 0 then
        this:GotoPlayerShopFocus()
    elseif section == 1 then
        if subsection == 0 then
            this:GotoEquipSearch(SearchOption.Normal)
        elseif subsection == 1 then
            this:GotoEquipSearch(SearchOption.Valueable)
        elseif subsection == 2 then
            this:GotoTalismanSearch()
        else
            this:GotoZhushabiSearch()
        end
    elseif section < this.m_BuySectionData.Count then
        local typeName = this.m_BuySectionData[section]
        if CommonDefs.DictContains(this.m_BuySubSectionData, typeof(String), typeName) and subsection < CommonDefs.DictGetValue(this.m_BuySubSectionData, typeof(String), typeName).Count then
            local subTypeName = CommonDefs.DictGetValue(this.m_BuySubSectionData, typeof(String), typeName)[subsection]

            if typeName == CPlayerShop_Buy.FurnitureTypeName and subTypeName == CPlayerShop_Buy.FurnitureSubtypeName then
                this:GotoFurnitureSearch()
                return
            end

            if typeName == CPlayerShop_Buy.ZhushabiTypeName and subTypeName == CPlayerShop_Buy.ZhushabiSubtypeName then
                this:GotoZhushabiSearch()
                return
            end

            this:GotoTemplateTable(typeName, subTypeName)
        end
    end
end
CPlayerShop_Buy.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local cell = TypeAs(view:GetFromPool(0), typeof(CPlayerShopTemplateItem))
    local info = this.m_CurrentTemplateIds[row]
    cell:UpdateData(info.TemplateId, info.SellCount, info.IsFocus, this.m_IsEditMode, false)
    cell.OnFocusValueChanged = (DelegateFactory.Action_bool(function (check) 
        if check then
            if this.m_CurFocusData.Count < CPlayerShopMgr.s_FocusTemplateLimit then
                info.IsFocus = true
                CommonDefs.HashSetAdd(this.m_CurFocusData, typeof(UInt32), info.TemplateId)
            else
                info.IsFocus = false
                cell:UpdateData(info.TemplateId, info.SellCount, info.IsFocus, this.m_IsEditMode, false)
                g_MessageMgr:ShowMessage("PLAYER_SHOP_MAX_COLLECTIONS")
            end
        else
            info.IsFocus = false
            CommonDefs.HashSetRemove(this.m_CurFocusData, typeof(UInt32), info.TemplateId)
        end
    end))
    return cell
end
CPlayerShop_Buy.m_OnSelectTemplateItem_CS2LuaHook = function (this, row) 
    if not this.m_IsEditMode then
        this.m_SelectTemplateIndex = row
        if this.m_SelectTemplateIndex >= 0 and this.m_SelectTemplateIndex < this.m_CurrentTemplateIds.Count then
            this:GotoShopItemInfo(this.m_CurrentTemplateIds[this.m_SelectTemplateIndex].TemplateId, SearchOption.All)
        end
    end
end
CPlayerShop_Buy.m_OnSearchCallBack_CS2LuaHook = function (this, templateId, option, jumpToZhuShaBi) 
    CPlayerShopMgr.Inst:AddHistory(templateId)
    local itemName = CPlayerShopMgr.Inst:GetName(templateId)
    if jumpToZhuShaBi and (string.find(itemName, LocalString.GetString("朱砂笔"), 1, true) ~= nil) then
        local level = this:GetZhuShaBiLevel(itemName)
        local type = this:GetZhuShaBiType(itemName)
        this:GotoZhushabiSearch(level, type)
        return
    end
    this:GotoShopItemInfo(templateId, option)
end
CPlayerShop_Buy.m_OnSearchWordsCallBack_CS2LuaHook = function (this, wordIdStr, gradeRange, type, subtype, option) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(true)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo:Init2(wordIdStr, gradeRange, type, subtype, option, false, false)
end
CPlayerShop_Buy.m_OnSearchColorCallBack_CS2LuaHook = function (this, grade, type, color) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(true)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo:Init3(grade, type, color, false)
end

--CPlayerShop_Buy.m_GetSectionByItemId_CS2LuaHook = function (this, templateId) 
--    local section = 0
--    local row = 0
--    if IdPartition.IdIsEquip(templateId) then
--        if IdPartition.IdIsTalisman(templateId) then
--            section = 1
--            row = 2
--        else
--            section = 1
--            row = 0
--        end
--    elseif IdPartition.IdIsItem(templateId) then
--        local data = CItemMgr.Inst:GetItemTemplate(templateId)
--        if data ~= nil then
--            local groupName = CPlayerShopMgr.Inst:GetItemGroupName(data.Type)
--            local typeName = CPlayerShopMgr.Inst:GetItemTypeName(data.Type)
--            section = CommonDefs.ListIndexOf(this.m_BuySectionData, groupName)
--            if section > 0 then
--                row = CommonDefs.ListIndexOf(CommonDefs.DictGetValue(this.m_BuySubSectionData, typeof(String), groupName), typeName)
--                if row < 0 then
--                    row = 0
--                end
--            else
--                section = 0
--            end
--        end
--    end
--    return CreateFromClass(MakeGenericClass(KeyValuePair, Int32, Int32), section, row)
--end
CPlayerShop_Buy.m_GotoShopItemInfo_CS2LuaHook = function (this, templateId, option) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(true)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo:Init(templateId, option, false, false)
end
CPlayerShop_Buy.m_GotoTemplateTable_CS2LuaHook = function (this, typeName, subTypeName)    local hasOtherGuide = false
    if subTypeName == CPlayerShopMgr.Inst:GetItemTypeName(EnumItemType_lua.WushanStone) then
        this.m_OtherBuyGuide:Init(EnumItemType.WushanStone, 0)
        hasOtherGuide = true
    elseif subTypeName == CPlayerShopMgr.Inst:GetItemTypeName(EnumItemType_lua.Gem) then
        this.m_OtherBuyGuide:Init(EnumItemType.Gem, 0)
        hasOtherGuide = true
    else
        this.m_OtherBuyGuide:Init(EnumItemType.Invalid, 0)
        hasOtherGuide = false
    end

    this.m_IsEditMode = false
    this.m_FocusButton.Text = LocalString.GetString("关注一类")
    this.m_FocusButton:SetState(QnButtonState.Normal)
    this.m_FocusButton.m_IsSelected = false
    if this.m_FocusTip ~= nil then
        this.m_FocusTip:SetActive(this.m_IsEditMode)
    end
    --当前关注数据
    this.m_CurFocusData = CPlayerShopMgr.Inst:GetAllFocusTemplate()

    this.m_ShopItemTemplateTable.gameObject:SetActive(true)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    local Ids = CPlayerShopMgr.Inst:GetItemTemplatesInGroup(typeName, subTypeName)
    local packdata = CreateFromClass(MakeGenericClass(List, Object))
    CommonDefs.ListClear(this.m_CurrentTemplateIds)
    local data = CreateFromClass(MakeGenericClass(List, Object))
    local data2 = CreateFromClass(MakeGenericClass(List, Object))
    local data3 = CreateFromClass(MakeGenericClass(List, Object))
    do
        local i = 0
        while i < Ids.Count do
            local info = CreateFromClass(ItemTemplateInfo, Ids[i], CommonDefs.HashSetContains(this.m_CurFocusData, typeof(UInt32), Ids[i]), false)
            CommonDefs.ListAdd(this.m_CurrentTemplateIds, typeof(ItemTemplateInfo), info)
            if i < 50 then
                CommonDefs.ListAdd(data, typeof(UInt32), Ids[i])
            elseif i < 100 then
                CommonDefs.ListAdd(data2, typeof(UInt32), Ids[i])
			else
                CommonDefs.ListAdd(data3, typeof(UInt32), Ids[i])
            end
            i = i + 1
        end
    end
    Gac2Gas.QueryPlayerShopOnShelfNum(MsgPackImpl.pack(data), MsgPackImpl.pack(data2), MsgPackImpl.pack(data3), false, SearchOption_lua.All, false)
    this.m_ShopItemTemplateTable:ReloadData(true, not hasOtherGuide)
end
CPlayerShop_Buy.m_GotoNormalSearch_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_NormalSearch.gameObject:SetActive(true)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_NormalSearch:Init()
end
CPlayerShop_Buy.m_GotoEquipSearch_CS2LuaHook = function (this, option) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(true)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_EquipSearch:Init(false, option)
end
CPlayerShop_Buy.m_GotoPlayerShopFocus_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(true)
    this.m_PlayerShopFocus:Init(false)
end
CPlayerShop_Buy.m_GotoTalismanSearch_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(true)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_TalismanSearch:Init(false)
end
CPlayerShop_Buy.m_GotoFurnitureSearch_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(true)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_FurnitureSearch:Init(false)
end
CPlayerShop_Buy.m_GotoZhushabiSearch_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_NormalSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_FurnitureSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(true)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_ZhushabiSearch:Init(false, SearchOption.All)
end
CPlayerShop_Buy.m_OnQueryPlayerShopOnShelfNumResult_CS2LuaHook = function (this, selledCounts, publicity)
    if publicity then
        return
    end
    do
        local i = 0
        while i < selledCounts.Count do
            local id = selledCounts[i]
            local count = selledCounts[i + 1]
            do
                local j = 0
                while j < this.m_CurrentTemplateIds.Count do
                    if this.m_CurrentTemplateIds[j].TemplateId == id then
                        this.m_CurrentTemplateIds[j].SellCount = count
                        this.m_CurrentTemplateIds[j].IsFocus = CommonDefs.HashSetContains(this.m_CurFocusData, typeof(UInt32), id)
                        break
                    end
                    j = j + 1
                end
            end
            i = i + 2
        end
    end

    --            m_ShopItemTemplateTable.ReloadData(false, true);
end
CPlayerShop_Buy.m_GetZhuShaBiLevel_CS2LuaHook = function (this, text) 
    if (string.find(text, LocalString.GetString("1级"), 1, true) ~= nil) then
        return 0
    end
    if (string.find(text, LocalString.GetString("2级"), 1, true) ~= nil) then
        return 1
    end
    return 2
end
CPlayerShop_Buy.m_GetZhuShaBiType_CS2LuaHook = function (this, text) 
    if (string.find(text, LocalString.GetString("天格"), 1, true) ~= nil) then
        return 0
    end
    if (string.find(text, LocalString.GetString("人格"), 1, true) ~= nil) then
        return 1
    end
    return 2
end
