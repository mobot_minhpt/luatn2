local CommonDefs = import "L10.Game.CommonDefs"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local NGUITools = import "NGUITools"
local CTianFuTabItem = import "L10.UI.CTianFuTabItem"
local DelegateFactory = import "DelegateFactory"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UILabel = import "UILabel"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip = import "L10.UI.CTooltip"
local Constants = import "L10.Game.Constants"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumTianFuSkillType = import "L10.Game.EnumTianFuSkillType"
local CButton = import "L10.UI.CButton"
local UIEventListener = import "UIEventListener"
local MessageWndManager = import "L10.UI.MessageWndManager"
local MsgPackImpl = import "MsgPackImpl"
local LocalString = import "LocalString"
local SkillKind = import "L10.Game.SkillKind"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Object = import "System.Object"
local CTianFuListItem = import "L10.UI.CTianFuListItem"
local Extensions = import "Extensions"
local StringBuilder = import "System.Text.StringBuilder"
--local __Skill_AllSkills_Template = import "L10.Game.__Skill_AllSkills_Template"

LuaTianFuSkillWnd = class()
RegistClassMember(LuaTianFuSkillWnd,"m_CurGroupIndex")
RegistClassMember(LuaTianFuSkillWnd,"m_SelectedIndecies")
RegistClassMember(LuaTianFuSkillWnd,"m_TianfuSkills")
RegistClassMember(LuaTianFuSkillWnd,"m_TotalAvaliablePoints")
RegistClassMember(LuaTianFuSkillWnd,"m_TianfuTabItems")
RegistClassMember(LuaTianFuSkillWnd,"m_CurType")
RegistClassMember(LuaTianFuSkillWnd,"TIAN_FU_GROUP_NUM")
RegistClassMember(LuaTianFuSkillWnd,"TianFuType")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupItemTemplate")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupIsInit")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupItems")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupSKILL_NUM")
RegistClassMember(LuaTianFuSkillWnd,"m_SkillRegion")
--Component
RegistClassMember(LuaTianFuSkillWnd,"m_AvaliablePointsLabel")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupUnlockLabel")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupItemTable")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupGroupChosenHintLabel")
RegistClassMember(LuaTianFuSkillWnd,"m_TianFuGroupSkillChosenHintLabel")
RegistChildComponent(LuaTianFuSkillWnd,"TianFuTabItem", GameObject)
RegistChildComponent(LuaTianFuSkillWnd,"TabsTable", UITable)
RegistChildComponent(LuaTianFuSkillWnd,"ResetButton", CButton)
RegistChildComponent(LuaTianFuSkillWnd,"SaveButton", CButton)
RegistChildComponent(LuaTianFuSkillWnd,"CloseButton", GameObject)
RegistChildComponent(LuaTianFuSkillWnd,"InfoButton", GameObject)

function LuaTianFuSkillWnd:Awake()
    self.m_CurGroupIndex = 1
    self.m_TotalAvaliablePoints = 0
    self.m_SelectedIndecies ={}
    self.m_TianfuSkills = {}
    self.m_TianfuTabItems = {}
    self.m_CurType = EnumTianFuSkillType.Normal
    self.TIAN_FU_GROUP_NUM = 4
    self.m_TianFuGroupSKILL_NUM = 3
    self.TianFuType ={ "Xiuwei40", "Xiuwei60", "Xiuwei80", "Xiuwei100" }
    self.m_TianFuGroupIsInit = false
    self:InitComponents()
end

function LuaTianFuSkillWnd:InitComponents()
    self.m_AvaliablePointsLabel = self.transform:Find("Anchor/AvaliablePoints/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_SkillRegion = self.transform:Find("Anchor/TianFuGroup/Bg").gameObject
    self.m_TianFuGroupUnlockLabel = self.transform:Find("Anchor/TianFuGroup/UnlockLabel"):GetComponent(typeof(UILabel))
    self.m_TianFuGroupItemTable = self.transform:Find("Anchor/TianFuGroup/Items"):GetComponent(typeof(UITable))
    self.m_TianFuGroupItemTemplate = self.transform:Find("Anchor/TianFuGroup/TianFuSkillItem").gameObject
    self.m_TianFuGroupItemTemplate:SetActive(false)
    self.m_TianFuGroupGroupChosenHintLabel = self.transform:Find("Anchor/TianFuGroup/GroupChosenHintLabel"):GetComponent(typeof(UILabel))
    self.m_TianFuGroupSkillChosenHintLabel = self.transform:Find("Anchor/TianFuGroup/SkillChosenHintLabel"):GetComponent(typeof(UILabel))
end

function LuaTianFuSkillWnd:Init()
    Extensions.RemoveAllChildren(self.TabsTable.transform)
    for i = 0, 3 do
        local go = NGUITools.AddChild(self.TabsTable.gameObject, self.TianFuTabItem)
        go:SetActive(true)
        local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTianFuTabItem))
        item.OnIncPointBtnClick = DelegateFactory.Action(function ()
            self:IncreasePoint()
        end)
        item.OnDecPointBtnClick = DelegateFactory.Action(function ()
            self:DecreasePoint()
        end)
        item.OnShowNumberKeyboard = DelegateFactory.Action_GameObject(function (go)
            self:ShowNumberKeyboard(go)
        end)
        item.OnItemClick = DelegateFactory.Action_GameObject(function (go)
            self:OnTabItemClick(go)
        end)
        table.insert(self.m_TianfuTabItems,item)
    end
    self.TabsTable:Reposition()


    self.m_AvaliablePointsLabel.text = nil
    self.m_CurType = CSkillInfoMgr.TianFuSkillType

    local pointUsed = 0

    local mainplayer = CClientMainPlayer.Inst
    if self.m_CurType == EnumTianFuSkillType.MPTZ then
        if mainplayer ~= nil then
            local skillIds = mainplayer.SkillProp.TianFuSkillForAI
            for i = 0,Constants.NumOfAvailableMPTZTianFuSkills-1,1 do
                local skillLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()])
                if Skill_AllSkills.Exists(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()]) then
                    pointUsed = pointUsed + skillLevel
                end
            end
        end
    else
        pointUsed = (mainplayer == nil) and 0 or mainplayer.SkillProp.TianFuPointUsed
    end
    local totalPoint = (mainplayer == nil) and 0 or mainplayer.SkillProp.ExtraTianFuPoint + math.floor(mainplayer.BasicProp.XiuWeiGrade)
    --设置界面数据
    self.m_TotalAvaliablePoints = totalPoint - pointUsed
    self.m_AvaliablePointsLabel.text = tostring(self.m_TotalAvaliablePoints)
    --未分配点数
    self:InitPlayerTianFuSkills()
    self:LoadData()

    self.ResetButton.Enabled = true
    self.SaveButton.Enabled = true
    if mainplayer ~= nil then
        local xiuweiGrade = math.floor(mainplayer.BasicProp.XiuWeiGrade)
        if xiuweiGrade < Constants.TianFuSkillUnlockLevel then
            self.ResetButton.Enabled = false
            self.SaveButton.Enabled = false
        end
    end
    local index = CSkillInfoMgr.DefaultTianFuTabIndex
    CSkillInfoMgr.DefaultTianFuTabIndex = - 1
    if index >= 1 and index <= #self.m_TianfuTabItems then
        self:OnTabItemClick(self.m_TianfuTabItems[index].gameObject)
    end
end

function LuaTianFuSkillWnd:Start()
    self.TianFuTabItem:SetActive(false)
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnCloseButtonClick()
    end)
    UIEventListener.Get(self.SaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnSaveButtonClick()
    end)
    UIEventListener.Get(self.ResetButton.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnResetButtonClick()
    end)
    UIEventListener.Get(self.InfoButton).onClick = DelegateFactory.VoidDelegate(function ()
        self:OnInfoButtonClick()
    end)
end

function LuaTianFuSkillWnd:IncreasePoint()
    if self.m_CurGroupIndex < 1 or self.m_CurGroupIndex > #self.m_SelectedIndecies then
        return
    end
    local selectedIndex = self.m_SelectedIndecies[self.m_CurGroupIndex]
    if selectedIndex < 1 or selectedIndex > #self.m_TianfuSkills[self.m_CurGroupIndex] then
        return
    end
    --分配点数
    if self.m_TotalAvaliablePoints == 0 then
        g_MessageMgr:ShowMessage("Skill_TianFu_No_Available_Points")
        return
    elseif self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex].skillLevel >= self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex].maxLevel then
        g_MessageMgr:ShowMessage("Skill_TianFu_Max_Level_Reach")
        return
    else
        local skill = self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex]
        local nextSkill = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(skill.skillClass, skill.skillLevel + 1))
        if nextSkill ~= nil and CClientMainPlayer.Inst ~= nil and nextSkill.PlayerLevel > CClientMainPlayer.Inst.MaxLevel then
            g_MessageMgr:ShowMessage("Skill_TianFu_Player_Level_Not_Fit", nextSkill.Name, skill.skillLevel)
            return
        end
        local default = skill
        default.skillLevel = default.skillLevel + 1
        self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex] = skill
        self.m_TotalAvaliablePoints = self.m_TotalAvaliablePoints - 1
        self.m_AvaliablePointsLabel.text = tostring(self.m_TotalAvaliablePoints)
        self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
    end
end

function LuaTianFuSkillWnd:DecreasePoint()
    if self.m_CurGroupIndex < 1 or self.m_CurGroupIndex > #self.m_SelectedIndecies then
        return
    end
    local selectedIndex = self.m_SelectedIndecies[self.m_CurGroupIndex]
    if selectedIndex < 1 or selectedIndex > #self.m_TianfuSkills[self.m_CurGroupIndex] then
        return
    end
    if self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex].skillLevel == 0 then
        g_MessageMgr:ShowMessage("Skill_TianFu_Min_Level_Reach")
        return
    else
        local skill = self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex]
        local default = skill
        default.skillLevel = default.skillLevel - 1
        self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex] = skill
        self.m_TotalAvaliablePoints =  self.m_TotalAvaliablePoints + 1
        self.m_AvaliablePointsLabel.text = tostring(self.m_TotalAvaliablePoints)
        self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
    end
end

function LuaTianFuSkillWnd:ShowNumberKeyboard(go)
    local item = go:GetComponent(typeof(CTianFuTabItem))
    if self.m_CurGroupIndex < 1 or self.m_CurGroupIndex > #self.m_SelectedIndecies then
        return
    end
    local selectedIndex = self.m_SelectedIndecies[self.m_CurGroupIndex]
    if selectedIndex < 1 or selectedIndex > #self.m_TianfuSkills[self.m_CurGroupIndex] then
        return
    end
    local skill = self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex]
    local min = 0
    local max = math.floor((math.min(skill.skillLevel + self.m_TotalAvaliablePoints, skill.maxLevel)))
    local defaultVal = skill.skillLevel
    --找到能升到的最大等级
    while max > 0 do
        local nextSkill = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(skill.skillClass, max))
        if nextSkill ~= nil and CClientMainPlayer.Inst ~= nil and nextSkill.PlayerLevel <= CClientMainPlayer.Inst.MaxLevel then
            break
        end
        max = max - 1
    end

    if min >= max then
        return
    end
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(min, max, defaultVal, 2, DelegateFactory.Action_int(function (val)
        item:UpdatePoints(val, skill.maxLevel)
    end), DelegateFactory.Action_int(function (val)
        self:ChangePoint(item, val)
    end), item.PointsLabel, CTooltip.AlignType.Top, true)
end

function LuaTianFuSkillWnd:OnTabItemClick(go)
    local item = go:GetComponent(typeof(CTianFuTabItem))
    for i = 1,#self.m_TianfuTabItems do
        if item:Equals(self.m_TianfuTabItems[i]) then
            self.m_CurGroupIndex = i
            local needXiuWeiLevel = self:GetNeedXiuweiGrade(i)
            local locked = CClientMainPlayer.Inst.BasicProp.XiuWeiGrade < needXiuWeiLevel
            local classId = 0
            local level = 0
            local maxLevel = Constants.MaxSkillUpgradeLevel
            if self.m_SelectedIndecies[i] ~= 0 then
                classId = self.m_TianfuSkills[i][self.m_SelectedIndecies[i]].skillClass
                level = self.m_TianfuSkills[i][self.m_SelectedIndecies[i]].skillLevel
                maxLevel = self.m_TianfuSkills[i][self.m_SelectedIndecies[i]].maxLevel
            end
            local mainplayer = CClientMainPlayer.Inst
            if mainplayer ~= nil and mainplayer.IsInXianShenStatus then
                local feishengLevel = level > 0 and mainplayer.SkillProp:GetFeiShengModifyLevel(classId * 100 + level, SkillKind_lua.TianFuSkill, mainplayer.Level) or 0
                self.m_TianfuTabItems[i]:InitFeiSheng(true, feishengLevel)
            else
                self.m_TianfuTabItems[i]:InitFeiSheng(false, 0)
            end

            self.m_TianfuTabItems[i]:Init(classId, level, maxLevel, needXiuWeiLevel, locked)
            self:TianFuGroupItem_Init(needXiuWeiLevel, self.m_SelectedIndecies[i], self.m_TianfuSkills[i])
            self.m_TianfuTabItems[i].Selected = true
        else
            self.m_TianfuTabItems[i].Selected = false
        end
    end
end

function LuaTianFuSkillWnd:OnSkillItemSelected (index, isClick)
    if CClientMainPlayer.Inst == nil then
        return
    end
    if  self.m_CurGroupIndex >= 1 and  self.m_CurGroupIndex <= #self.m_SelectedIndecies then
        local selectedIndex = self.m_SelectedIndecies[ self.m_CurGroupIndex]
        if selectedIndex ~= index then
            --check
            if selectedIndex ~= 0 and self.m_TianfuSkills[ self.m_CurGroupIndex][selectedIndex].skillLevel > 0 then
                --减去之前的点数
                local skill = self.m_TianfuSkills[ self.m_CurGroupIndex][selectedIndex]
                local n = skill.skillLevel
                self.m_TotalAvaliablePoints = self.m_TotalAvaliablePoints + skill.skillLevel
                skill.skillLevel = 0
                self.m_TianfuSkills[ self.m_CurGroupIndex][selectedIndex] = skill
                --尝试加到新的上面
                if index >= 1 and index <= #self.m_TianfuSkills[ self.m_CurGroupIndex]then
                    local newSkill = self.m_TianfuSkills[ self.m_CurGroupIndex][index]
                    local count = 0
                    do
                        local i = 1
                        while i <= n do
                            local nextSkill = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(newSkill.skillClass, newSkill.skillLevel + i))
                            if nextSkill ~= nil and nextSkill.PlayerLevel <= CClientMainPlayer.Inst.MaxLevel and nextSkill.Level <= newSkill.maxLevel then
                                count = count + 1
                            end
                            i = i + 1
                        end
                    end
                    self.m_TotalAvaliablePoints = self.m_TotalAvaliablePoints - count
                    newSkill.skillLevel = newSkill.skillLevel + count
                    self.m_TianfuSkills[ self.m_CurGroupIndex][index] = newSkill
                end
            end
            self.m_SelectedIndecies[self.m_CurGroupIndex] = index
            self:OnTabItemClick(self.m_TianfuTabItems[ self.m_CurGroupIndex].gameObject)
        end
    end

    if isClick and CGuideMgr.Inst:IsInPhase(21) then
        CGuideMgr.Inst:TriggerGuide(8)
    end
end

function LuaTianFuSkillWnd:ChangePoint(item, newPoint)
    if self.m_CurGroupIndex < 1 or self.m_CurGroupIndex > #self.m_SelectedIndecies then
        return
    end
    local selectedIndex = self.m_SelectedIndecies[self.m_CurGroupIndex]
    if selectedIndex < 1 or selectedIndex > #self.m_TianfuSkills[self.m_CurGroupIndex] then
        return
    end
    local skill = self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex]

    if newPoint > skill.skillLevel then
        --加点
        local delta = math.floor(math.min(self.m_TotalAvaliablePoints, newPoint - skill.skillLevel))
        if self.m_TotalAvaliablePoints == 0 then
            g_MessageMgr:ShowMessage("Skill_TianFu_No_Available_Points")
            self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
            --复原
            return
        elseif self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex].skillLevel >= self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex].maxLevel then
            g_MessageMgr:ShowMessage("Skill_TianFu_Max_Level_Reach")
            self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
            --复原
            return
        else
            local nextSkill = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(skill.skillClass, skill.skillLevel + delta))
            if nextSkill ~= nil and CClientMainPlayer.Inst ~= nil and nextSkill.PlayerLevel > CClientMainPlayer.Inst.MaxLevel then
                g_MessageMgr:ShowMessage("Skill_TianFu_Player_Level_Not_Fit", nextSkill.Name, skill.skillLevel)
                self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
                --复原
                return
            end
            skill.skillLevel = skill.skillLevel + delta
            self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex] = skill
            self.m_TotalAvaliablePoints = self.m_TotalAvaliablePoints - delta
            self.m_AvaliablePointsLabel.text = tostring(self.m_TotalAvaliablePoints)
            self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
        end
    elseif newPoint < skill.skillLevel then
        --减点
        local delta = math.floor(math.max(0, skill.skillLevel - newPoint))
        if skill.skillLevel == 0 then
            g_MessageMgr:ShowMessage("Skill_TianFu_Min_Level_Reach")
            self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
            --复原
            return
        else
            skill.skillLevel = skill.skillLevel - delta
            self.m_TianfuSkills[self.m_CurGroupIndex][selectedIndex] = skill
            self.m_TotalAvaliablePoints = self.m_TotalAvaliablePoints + delta
            self.m_AvaliablePointsLabel.text = tostring(self.m_TotalAvaliablePoints)
            self:OnTabItemClick(self.m_TianfuTabItems[self.m_CurGroupIndex].gameObject)
        end
    end
end

function LuaTianFuSkillWnd:GetNeedXiuweiGrade(index)
    local needXiuweiGrade = 0
    repeat
        local default = self.TianFuType[index]
        if default == "Xiuwei40" then
            needXiuweiGrade = 40
            break
        elseif default == "Xiuwei60" then
            needXiuweiGrade = 60
            break
        elseif default == "Xiuwei80" then
            needXiuweiGrade = 80
            break
        elseif default == "Xiuwei100" then
            needXiuweiGrade = 100
            break
        end
    until 1
    return needXiuweiGrade
end

function LuaTianFuSkillWnd:InitPlayerTianFuSkills()
    self.m_TianfuSkills = {}
    self.m_SelectedIndecies = {}
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    local mptzTianFuSkillMap = {}
    if self.m_CurType == EnumTianFuSkillType.MPTZ then
        local skillIds = mainplayer.SkillProp.TianFuSkillForAI
        for i = 0,Constants.NumOfAvailableMPTZTianFuSkills-1,1 do
            local skillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()])
            if Skill_AllSkills.Exists(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()]) then
                if mptzTianFuSkillMap[skillCls] == nil then
                    mptzTianFuSkillMap[skillCls] = skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()]
                end
            end
        end
    end

    for i = 1, 4 do
        local needXiuweiGrade = self:GetNeedXiuweiGrade(i)
        local data = {}

        --获取mainPlayer的特定修为等级下的天赋技能
        local tianfuSkillIds = {}
        local prefix = 900 + EnumToInt(mainplayer.Class)
        __Skill_AllSkills_Template.ForeachKey(function (cls)
            if math.floor(cls / 1000) == prefix then
                --以(900+ClassId)开头，以01结尾
                local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1)
                local skill = Skill_AllSkills.GetData(skillId)
                if skill.EKind == SkillKind.TianFuSkill and skill.NeedXiuweiLevel == needXiuweiGrade then
                    if self.m_CurType == EnumTianFuSkillType.MPTZ then
                        if mptzTianFuSkillMap[cls] then
                            table.insert(tianfuSkillIds,{Key = mptzTianFuSkillMap[cls], Value = skill.Weight})
                        else
                            table.insert(tianfuSkillIds,{Key = skillId, Value = skill.Weight})
                        end
                    else
                        if mainplayer.SkillProp:IsOriginalSkillClsExist(cls) then
                            table.insert(tianfuSkillIds,{Key = mainplayer.SkillProp:GetOriginalSkillIdByCls(cls), Value = skill.Weight})
                        else
                            table.insert(tianfuSkillIds,{Key = skillId, Value = skill.Weight})
                        end
                    end
                end
            end
        end)
        --按照技能权重、ID进行排序
        table.sort(tianfuSkillIds,function (skill1, skill2)
            if skill1.Value ==  skill2.Value then
                return skill1.Key <  skill2.Key
            end
            return skill1.Value <  skill2.Value
        end)
        local selectedIndex = 0
        do
            for j = 1,#tianfuSkillIds do
                local id = tianfuSkillIds[j].Key
                local _level = CLuaDesignMgr.Skill_AllSkills_GetLevel(id)
                local _class = CLuaDesignMgr.Skill_AllSkills_GetClass(id)
                local skill = Skill_AllSkills.GetData(id)
                if self.m_CurType == EnumTianFuSkillType.MPTZ then
                    if mptzTianFuSkillMap[_class] then
                        selectedIndex = j
                        _level = CLuaDesignMgr.Skill_AllSkills_GetLevel(mptzTianFuSkillMap[_class])
                    else
                        _level = 0
                    end
                else
                    if mainplayer.SkillProp:IsOriginalSkillClsExist(_class) then
                        selectedIndex = j
                        _level = CLuaDesignMgr.Skill_AllSkills_GetLevel(mainplayer.SkillProp:GetOriginalSkillIdByCls(_class))
                    else
                        _level = 0
                    end
                end

                local skillData = {skillClass = _class, skillLevel = _level, name = skill.Name, iconName = skill.SkillIcon, maxLevel = Constants.MaxSkillUpgradeLevel}
                --CreateFromClass(SkillData, _class, _level, skill.Name, skill.SkillIcon, Constants.MaxSkillUpgradeLevel)
                table.insert(data, skillData)
            end
        end
        table.insert(self.m_TianfuSkills, data)
        table.insert(self.m_SelectedIndecies, selectedIndex)
    end
end

function LuaTianFuSkillWnd:LoadData()
    if #self.m_TianfuSkills ~= self.TIAN_FU_GROUP_NUM then
        return
    end
    local mainPlayer = CClientMainPlayer.Inst

    for i = 1, #self.m_TianfuSkills do
        local needXiuWeiLevel = self:GetNeedXiuweiGrade(i)
        local locked = mainPlayer.BasicProp.XiuWeiGrade < needXiuWeiLevel
        local classId = 0
        local level = 0
        local maxLevel = Constants.MaxSkillUpgradeLevel
        if self.m_SelectedIndecies[i] ~= 0 then
            local skillData = self.m_TianfuSkills[i][self.m_SelectedIndecies[i]]
            classId = skillData.skillClass
            level = skillData.skillLevel
            maxLevel = skillData.maxLevel

            if mainPlayer.IsInXianShenStatus then
                local feishengLevel = level > 0 and mainPlayer.SkillProp:GetFeiShengModifyLevel(classId * 100 + level, SkillKind_lua.TianFuSkill, mainPlayer.Level) or 0
                self.m_TianfuTabItems[i]:InitFeiSheng(true, feishengLevel)
            else
                self.m_TianfuTabItems[i]:InitFeiSheng(false, 0)
            end
        end
        self.m_TianfuTabItems[i]:Init(classId, level, maxLevel, needXiuWeiLevel, locked)
        self.m_TianfuTabItems[i].Selected = false
    end

    self.m_CurGroupIndex = 0
    self:TianFuGroupItem_ClearDisplay()
end

function LuaTianFuSkillWnd:OnCloseButtonClick()
    local modification = self:GetModification()
    local hasModification = false
    do
        local i = 2
        while i <= #modification do
            if math.floor(tonumber(modification[i] or 0)) ~= 0 then
                hasModification = true
                break
            end
            i = i + 2
        end
    end
    if hasModification then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Skill_TianFu_Confirm_Before_Close"), DelegateFactory.Action(function ()
            self:OnSaveButtonClick(self.SaveButton.gameObject)
            self:Close()
        end), DelegateFactory.Action(function ()
            self:Close()
        end), nil, nil, false)
    else
        self:Close()
    end
end

function LuaTianFuSkillWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaTianFuSkillWnd:GetModification()
    local modification = {}

    if CClientMainPlayer.Inst == nil then
        return modification
    end

    local newTianFuSkillStatus = {}

    for i = 1 , #self.m_TianfuSkills do
        local idx = self.m_SelectedIndecies[i]
        if idx <= #self.m_TianfuSkills[i] and idx >= 1 then
            local skillCls = self.m_TianfuSkills[i][idx].skillClass
            local level = self.m_TianfuSkills[i][idx].skillLevel
            newTianFuSkillStatus[skillCls] = level
            --table.insert(newTianFuSkillStatus,{Key = skillCls, Value = level})
        end
    end

    --1. 对比当前找出所有降低（点了-）或者删除了的天赋技能
    local skillProp = CClientMainPlayer.Inst.SkillProp

    local skillCls2IdDict = {}
    if self.m_CurType == EnumTianFuSkillType.MPTZ then
        local skillIds = skillProp.TianFuSkillForAI
        
        for i = 0,Constants.NumOfAvailableMPTZTianFuSkills-1,1 do
            local skillCls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()])
            if Skill_AllSkills.Exists(skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()]) then
                if skillCls2IdDict[skillCls] == nil then
                    skillCls2IdDict[skillCls] = skillIds[i+LuaPVPMgr.GetTianFuBaseIndex()]
                    --table.insert(skillCls2IdDict,{Key = skillCls,Value = skillIds[i]})
                end
            end
        end
    else
        skillProp:ForeachOriginalSkillCls2Id(DelegateFactory.Action_KeyValuePair_uint_uint(function (data)
            skillCls2IdDict[data.Key] = data.Value
            --table.insert(skillCls2IdDict,{Key = data.Key,Value = data.Value})
        end))
    end

    for k, v in pairs(skillCls2IdDict) do
        local data = {}
        data.Key = k
        data.Value = v
        local skillInfo = Skill_AllSkills.GetData(data.Value)
        if skillInfo.Kind == SkillKind_lua.TianFuSkill then
            if newTianFuSkillStatus[data.Key] == nil then
                table.insert(modification, data.Key)
                table.insert(modification,  - CLuaDesignMgr.Skill_AllSkills_GetLevel(data.Value))
                --技能等级归0
            else
                table.insert(modification, data.Key)
                table.insert(modification, newTianFuSkillStatus[data.Key] - CLuaDesignMgr.Skill_AllSkills_GetLevel(data.Value))
                --增加或减少若干级
            end
        end
    end

    --2. 对比找出所有新增的天赋技能
    for k, v in pairs(newTianFuSkillStatus) do
        local data = {}
        data.Key = k
        data.Value = v
        local continue
        repeat
            local default
            if self.m_CurType == EnumTianFuSkillType.MPTZ then
                default = skillCls2IdDict[data.Key] ~= nil
            else
                default = skillProp:IsOriginalSkillClsExist(data.Key)
            end
            local existSkill = (default)
            if not existSkill then
                if data.Value == 0 then
                    continue = true
                    break
                end
                table.insert(modification, data.Key)
                table.insert(modification, CLuaDesignMgr.Skill_AllSkills_GetLevel(data.Value))
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end

    return modification
end

function LuaTianFuSkillWnd:OnSaveButtonClick(go)
    local modification = self:GetModification()
    local hasModification = false

    for i = 2, #modification, 2 do
        if math.floor(tonumber(modification[i] or 0)) ~= 0 then
            hasModification = true
            break
        end
    end
    if not hasModification then
        return
    end
    if self.m_CurType == EnumTianFuSkillType.MPTZ then
        local normalSkills = {}

        local curAISkillType = LuaPVPMgr.GetAISkillType()
        local startIndex = (curAISkillType-1)*5
        for j = startIndex + 1,Constants.NumOfAvaliableMPTZActiveSkills + startIndex do
            table.insert(normalSkills,CClientMainPlayer.Inst.SkillProp.NewActiveSkillForAI[j])
        end
        
        local tianfuSkillList = {}

        for i = 1, #self.m_TianfuSkills do
            local continue
            repeat
                local idx = self.m_SelectedIndecies[i]
                if idx <= #self.m_TianfuSkills[i] and idx >= 1 then
                    local skillCls = self.m_TianfuSkills[i][idx].skillClass
                    local level = self.m_TianfuSkills[i][idx].skillLevel
                    if level == 0 then
                        continue = true
                        break
                    end
                    table.insert(tianfuSkillList, CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillCls, level))
                end
                continue = true
            until 1
            if not continue then
                break
            end
        end

        -- 根据是否打开AI来保存
        if LuaPVPMgr.IsRobotOpen() then
            local takeFirstJueJi = CClientMainPlayer.Inst.SkillProp.IsMPTZCast79Skill > 0
            local takeSecondJueJi = CClientMainPlayer.Inst.SkillProp.IsMPTZCast109Skill > 0
            Gac2Gas.SetActiveSkillForRobot(LuaPVPMgr.GetAISkillType(),
                    MsgPackImpl.pack(Table2List(normalSkills, MakeGenericClass(List, Object))),
                    MsgPackImpl.pack(Table2List(tianfuSkillList,MakeGenericClass(List, Object))),
                            takeFirstJueJi, takeSecondJueJi, 0, LuaPVPMgr.m_CurrentYingLingState or 0)
        else
            Gac2Gas.SetActiveSkillForAI(AISkillType_lua.MPTZ,
                    MsgPackImpl.pack(Table2List(normalSkills, MakeGenericClass(List, Object))),
                    MsgPackImpl.pack(Table2List(tianfuSkillList, MakeGenericClass(List, Object))), 0)
        end
    else
        Gac2Gas.RequestUpgradeTianFuSkill(MsgPackImpl.pack(Table2List(modification, MakeGenericClass(List, Object))))
    end
end

function LuaTianFuSkillWnd:OnResetButtonClick()

    for i = 1,#self.m_TianfuSkills do
        local list = self.m_TianfuSkills[i]
        do
            local j = 1
            while j <= #list do
                local data = list[j]
                data.skillLevel = 0
                list[j] = data
                j = j + 1
            end
        end
        self.m_SelectedIndecies[i] = 0
    end
    self:LoadData()
    local mainPlayer = CClientMainPlayer.Inst
    self.m_TotalAvaliablePoints = (mainPlayer == nil) and 0 or mainPlayer.SkillProp.ExtraTianFuPoint + math.floor(mainPlayer.BasicProp.XiuWeiGrade)
    self.m_AvaliablePointsLabel.text = tostring(self.m_TotalAvaliablePoints)
end

function LuaTianFuSkillWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("Skill_Tips_Tianfu_Readme")
end

function LuaTianFuSkillWnd:GetGuideGo(methodName)
    if methodName == "Get1stTabItem" then
        return self:Get1stTabItem()
    elseif methodName == "GetSkillRegion" then
        return self:GetSkillRegion()
    elseif methodName == "GetIncreaseButton" then
        return self:GetIncreaseButton()
    elseif methodName == "GetSaveButton" then
        return self:GetSaveButton()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end

function LuaTianFuSkillWnd:Get1stTabItem()
    return self.TabsTable.transform:GetChild(0).gameObject
end

function LuaTianFuSkillWnd:GetSkillRegion()
    return self.m_SkillRegion
end

function LuaTianFuSkillWnd:GetIncreaseButton()
    local go = self:Get1stTabItem()
    local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTianFuTabItem))
    return item:GetIncButton()
end

function LuaTianFuSkillWnd:GetSaveButton()
    return self.SaveButton.gameObject
end

function LuaTianFuSkillWnd:TianFuGroupItem_OnSkillClick(go)
    local item = go:GetComponent(typeof(CTianFuListItem))
    for i = 1,#self.m_TianFuGroupItems do
        local tex = CommonDefs.GetComponent_GameObject_Type(self.m_TianFuGroupItems[i].transform:Find("Texture").gameObject, typeof(CUITexture))
        if item:Equals(self.m_TianFuGroupItems[i]) then
            self:OnSkillItemSelected(i, true)
            tex:LoadMaterial("UI/Texture/Transparent/Material/raw_common_btn_01_highlight.mat")
        else
            tex:LoadMaterial("UI/Texture/Transparent/Material/raw_common_btn_01_normal.mat")
        end
    end
end

function LuaTianFuSkillWnd:TianFuGroupItem_ClearDisplay()
    self.m_TianFuGroupUnlockLabel.text = nil
    self.m_TianFuGroupGroupChosenHintLabel.enabled = true
    self.m_TianFuGroupSkillChosenHintLabel.enabled = false
    self.m_TianFuGroupItems = {}
    Extensions.RemoveAllChildren(self.m_TianFuGroupItemTable.transform)
    self.m_TianFuGroupIsInit = false
end

function LuaTianFuSkillWnd:TianFuGroupItem_OnSkillInitSelection(item)
    do
        local i = 1
        while i <= #self.m_TianFuGroupItems do
            if item:Equals(self.m_TianFuGroupItems[i]) then
                self:OnSkillItemSelected(i, false)
                break
            end
            i = i + 1
        end
    end
end

function LuaTianFuSkillWnd:TianFuGroupItem_Init(needXiuweiGrade, selectedIndex, array)
    if array == nil or #array ~= self.m_TianFuGroupSKILL_NUM then
        return
    end
    if not self.m_TianFuGroupIsInit then
        self.m_TianFuGroupIsInit= true
        self.m_TianFuGroupGroupChosenHintLabel.enabled = false
        self.m_TianFuGroupSkillChosenHintLabel.enabled = true
        self.m_TianFuGroupItems = {}
        for i = 1, 3 do
            local go = NGUITools.AddChild(self.m_TianFuGroupItemTable.gameObject, self.m_TianFuGroupItemTemplate)
            go:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CTianFuListItem))
            item.OnItemClickDelegate = DelegateFactory.Action_GameObject(function (go)
                self:TianFuGroupItem_OnSkillClick(go)
            end)
            local t =  item.transform:Find("Texture")
            CommonDefs.AddComponent_GameObject_Type(t.gameObject, typeof(CUITexture))
            table.insert(self.m_TianFuGroupItems,item)
        end
        self.m_TianFuGroupItemTable :Reposition()
    end

    local sb = NewStringBuilderWraper(StringBuilder)
    CommonDefs.StringBuilder_Append_StringBuilder_Int32(sb, needXiuweiGrade)
    sb:Append(LocalString.GetString("\n修\n天\n赋"))
    self.m_TianFuGroupUnlockLabel.text = ToStringWrap(sb)

    --同一个系别下的天赋技能，显示同样的等级信息
    local displayLevel = 0
    if selectedIndex >= 1 and selectedIndex <= #array then
        displayLevel = array[selectedIndex].skillLevel
    end
    local mainPlayer = CClientMainPlayer.Inst

    do
        local i = 1
        while i <= #array do
            if mainPlayer ~= nil and mainPlayer.IsInXianShenStatus then
                local feishengLevel = mainPlayer.SkillProp:GetFeiShengModifyLevel(displayLevel + array[i].skillClass * 100, SkillKind_lua.TianFuSkill, mainPlayer.Level)
                self.m_TianFuGroupItems[i]:InitFeiSheng(true, feishengLevel)
            else
                self.m_TianFuGroupItems[i]:InitFeiSheng(false, 0)
            end
            self.m_TianFuGroupItems[i]:Init(displayLevel, array[i].skillClass)
            i = i + 1
        end
    end

    if selectedIndex >= 1 and selectedIndex <= #array then
        self:TianFuGroupItem_OnSkillInitSelection(self.m_TianFuGroupItems[selectedIndex])
    end

    do
        local i = 1
        while i <= #self.m_TianFuGroupItems do
            local tex = CommonDefs.GetComponent_GameObject_Type(self.m_TianFuGroupItems[i].transform:Find("Texture").gameObject, typeof(CUITexture))
            if i == selectedIndex then
                self.m_TianFuGroupItems[i].Selected = true
                tex:LoadMaterial("UI/Texture/Transparent/Material/raw_common_btn_01_highlight.mat")
            else
                self.m_TianFuGroupItems[i].Selected = false
                tex:LoadMaterial("UI/Texture/Transparent/Material/raw_common_btn_01_normal.mat")
            end
            i = i + 1
        end
    end
end