local Vector3 = import "UnityEngine.Vector3"
local CUITexture=import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Extensions = import "Extensions"
local DelegateFactory = import "DelegateFactory"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CIndirectUIResources=import "L10.UI.CIndirectUIResources"


CLuaQMPKZhanDuiSettingWnd = class()
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"ScrollView")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"Grid")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"ItemPrefab")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"CloseButton")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"HintButton")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"CountLabels")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_SaveBtn")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_MemberSettingItemList")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_ChuZhanInfo")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_IsDirty")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_MaxPlayerInBattle")

RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_IsTeamLeader")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_MainPlayerId")
RegistClassMember(CLuaQMPKZhanDuiSettingWnd,"m_TipLab")
-- Auto Generated!!
function CLuaQMPKZhanDuiSettingWnd:Init( )
    if CClientMainPlayer.Inst then
        self.m_MainPlayerId=CClientMainPlayer.Inst.Id
    end
    self.ScrollView = self.transform:Find("ShowArea/Scroll View"):GetComponent(typeof(UIScrollView))
    self.Grid = self.transform:Find("ShowArea/Scroll View/Grid"):GetComponent(typeof(UIGrid))
    self.ItemPrefab = self.transform:Find("ShowArea/Member").gameObject
    self.ItemPrefab:SetActive(false)
    self.CloseButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.HintButton = self.transform:Find("ShowArea/BottomArea/Sprite").gameObject
    self.CountLabels = {}
    local tf=FindChild(self.transform,"CountLabels")
    for i=1,5 do
        self.CountLabels[i]=tf:GetChild(i-1):GetComponent(typeof(UILabel))
    end

    self.m_SaveBtn = self.transform:Find("ShowArea/BottomArea/SaveButton").gameObject
    self.m_TipLab = self.transform:Find("ShowArea/BottomArea/TipLab"):GetComponent(typeof(UILabel))
    self.m_MemberSettingItemList = {}
    self.m_ChuZhanInfo = nil
    self.m_IsDirty = false

    self.m_MaxPlayerInBattle={5,1,3,5,1}

    self.m_SaveBtn:SetActive(false)
    self.m_IsTeamLeader=false
    if CClientMainPlayer.Inst then
        --是否是队长
        if CQuanMinPKMgr.Inst.m_MemberInfoList then
            local leaderInfo=CQuanMinPKMgr.Inst.m_MemberInfoList[0]
            if leaderInfo.m_Id==CClientMainPlayer.Inst.Id then
                self.m_SaveBtn:SetActive(true)
                self.m_IsTeamLeader=true
            end
        end
    end

    UIEventListener.Get(self.HintButton).onClick = DelegateFactory.VoidDelegate(function(go) self:onHintClick(go) end)
    UIEventListener.Get(self.m_SaveBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSaveBtnClicked(go) end)
    self.m_TipLab.text = g_MessageMgr:FormatMessage("QMPK_ZhanDui_Setting_Tip")
    self:showMemberList()
end
function CLuaQMPKZhanDuiSettingWnd:OnEnable( )
    -- EventManager.AddListener(EnumEventType.QMPKQueryZhanDuiInfoResult, MakeDelegateFromCSFunction(self.showMemberList, Action0, self))
    g_ScriptEvent:AddListener("QMPKQueryZhanDuiInfoResult", self, "showMemberList")
    
end
function CLuaQMPKZhanDuiSettingWnd:OnDisable( )
    -- EventManager.RemoveListener(EnumEventType.QMPKQueryZhanDuiInfoResult, MakeDelegateFromCSFunction(self.showMemberList, Action0, self))
    g_ScriptEvent:RemoveListener("QMPKQueryZhanDuiInfoResult", self, "showMemberList")
end
function CLuaQMPKZhanDuiSettingWnd:showMemberList( )
    -- CommonDefs.ListClear(self.m_MemberSettingItemList)
    self.m_MemberSettingItemList={}

    self.m_ChuZhanInfo = CQuanMinPKMgr.Inst.m_ChuZhanInfoList
    Extensions.RemoveAllChildren(self.Grid.transform)
    do
        local i = 0 local cnt = CQuanMinPKMgr.Inst.m_MemberInfoList.Count
        while i < cnt do
            local go = CommonDefs.Object_Instantiate(self.ItemPrefab)
            go:SetActive(true)
            local trans = go.transform
            self.Grid:AddChild(trans)
            trans.localScale = Vector3.one
            trans.localPosition = Vector3.zero
            -- local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CQMPKChuZhanSettingItem))
            -- item:SetMemberInfo(CQuanMinPKMgr.Inst.m_MemberInfoList[i], i)
            self:InitItem(go.transform,CQuanMinPKMgr.Inst.m_MemberInfoList[i], i)
            -- CommonDefs.ListAdd(self.m_MemberSettingItemList, typeof(CQMPKChuZhanSettingItem), item)
            table.insert( self.m_MemberSettingItemList,go.transform )
            do
                local j = 0 local cnt2 = self.m_ChuZhanInfo.Count
                while j < cnt2 do
                    if (bit.band(self.m_ChuZhanInfo[j], (bit.lshift(1, i)))) ~= 0 then
                        -- item:SetCheckBox(j, true)
                        self:SetCheckBox(go.transform,j,true)
                    else
                        -- item:SetCheckBox(j, false)
                        self:SetCheckBox(go.transform,j,false)
                    end
                    j = j + 1
                end
            end
            -- item.m_CheckBoxChanged =DelegateFactory.Action_int_int(function(p1,p2) self:OnCheckBoxChanged(p1,p2) end)
            i = i + 1
        end
    end
    self:RefreshLabels()

    self.Grid:Reposition()
    self.ScrollView:InvalidateBounds()
    self.ScrollView:SetDragAmount(0, 0, false)
    self.m_IsDirty = false
end
function CLuaQMPKZhanDuiSettingWnd:OnCheckBoxChanged( memberIndex, checkBoxIndex) 
    if (bit.band(self.m_ChuZhanInfo[checkBoxIndex], (bit.lshift(1, memberIndex)))) ~= 0 then--取消选择
        if checkBoxIndex==0 or checkBoxIndex==3 then--团队赛
            self.m_ChuZhanInfo[0] = bit.band(self.m_ChuZhanInfo[0], (bit.bnot((bit.lshift(1, memberIndex)))))
            self.m_ChuZhanInfo[3] = bit.band(self.m_ChuZhanInfo[3], (bit.bnot((bit.lshift(1, memberIndex)))))
            self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],0,false)
            self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],3,false)
        else
            self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],checkBoxIndex,false)
            self.m_ChuZhanInfo[checkBoxIndex] = bit.band(self.m_ChuZhanInfo[checkBoxIndex], (bit.bnot((bit.lshift(1, memberIndex)))))
        end
        -- self.m_ChuZhanInfo[checkBoxIndex] = bit.band(self.m_ChuZhanInfo[checkBoxIndex], (bit.bnot((bit.lshift(1, memberIndex)))))
        self.m_IsDirty = true
        self:RefreshLabels()
        return
    end

    local hasOtherBattle = false
    local battleCnt = 0
    do
        local i = 0 local cnt = #self.m_MemberSettingItemList--.Count
        while i < cnt do
            for j = 0, 4 do
                if (bit.band(self.m_ChuZhanInfo[j], (bit.lshift(1, i)))) ~= 0 then--团队赛
                    if i == memberIndex then
                        hasOtherBattle = true
                    end
                    battleCnt = battleCnt + 1
                    break
                end
            end
            if hasOtherBattle then
                break
            end
            i = i + 1
        end
    end
    

    if not hasOtherBattle and battleCnt >= 5 then
        g_MessageMgr:ShowMessage("Qmpk_ChuZhan_Set_Special_Rule2")--出战总人数不得超过5人
        return
    end

    --出战人数验证
    if self:GetOneCount(self.m_ChuZhanInfo[checkBoxIndex]) >= self.m_MaxPlayerInBattle[checkBoxIndex+1] then
        g_MessageMgr:ShowMessage("QMPK_Reach_Max_Player_In_Battle")--该场次出战人数已达上限
        return
    end



    -- if checkBoxIndex == 2 then--3v3
        -- if (bit.band(self.m_ChuZhanInfo[1], (bit.lshift(1, memberIndex)))) ~= 0 
        -- or (bit.band(self.m_ChuZhanInfo[4], (bit.lshift(1, memberIndex)))) ~= 0 then
        --     g_MessageMgr:ShowMessage("Qmpk_ChuZhan_Set_Special_Rule")--单人赛和3v3出战成员不能重复
        --     return
        -- end
    -- end
    if checkBoxIndex == 2 then--3v3
        local curPlayerClass = CQuanMinPKMgr.Inst.m_MemberInfoList[memberIndex].m_Class
        local i = 0 local cnt = CQuanMinPKMgr.Inst.m_MemberInfoList.Count
        while i < cnt do
            -- 不是当前玩家 and 参加了3v3 and 与当前玩家职业相同
            if i ~= memberIndex and (bit.band(self.m_ChuZhanInfo[2], (bit.lshift(1, i)))) ~= 0 and CQuanMinPKMgr.Inst.m_MemberInfoList[i].m_Class == curPlayerClass then
                g_MessageMgr:ShowMessage("Qmpk_ChuZhan_Set_3v3_Special_Rule")--3v3出战成员不能出现相同职业
                return
            end
            i = i + 1
        end
    end
    if checkBoxIndex==1 then
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],2,false)
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],4,false)
        self.m_ChuZhanInfo[2] = bit.band(self.m_ChuZhanInfo[2], (bit.bnot((bit.lshift(1, memberIndex)))))
        self.m_ChuZhanInfo[4] = bit.band(self.m_ChuZhanInfo[4], (bit.bnot((bit.lshift(1, memberIndex)))))
    elseif checkBoxIndex==2 then
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],1,false)
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],4,false)
        self.m_ChuZhanInfo[1] = bit.band(self.m_ChuZhanInfo[1], (bit.bnot((bit.lshift(1, memberIndex)))))
        self.m_ChuZhanInfo[4] = bit.band(self.m_ChuZhanInfo[4], (bit.bnot((bit.lshift(1, memberIndex)))))
    elseif checkBoxIndex==4 then
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],1,false)
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],2,false)
        self.m_ChuZhanInfo[1] = bit.band(self.m_ChuZhanInfo[1], (bit.bnot((bit.lshift(1, memberIndex)))))
        self.m_ChuZhanInfo[2] = bit.band(self.m_ChuZhanInfo[2], (bit.bnot((bit.lshift(1, memberIndex)))))
    end
    -- if checkBoxIndex == 1 or checkBoxIndex == 4 then--单人赛
    --     if (bit.band(self.m_ChuZhanInfo[2], (bit.lshift(1, memberIndex)))) ~= 0 
    --     or (bit.band(self.m_ChuZhanInfo[1], (bit.lshift(1, memberIndex)))) ~= 0 
    --     or (bit.band(self.m_ChuZhanInfo[4], (bit.lshift(1, memberIndex)))) ~= 0 then
    --         g_MessageMgr:ShowMessage("Qmpk_ChuZhan_Set_Special_Rule")--单人赛和3v3出战成员不能重复
    --         return
    --     end
    -- end
    if checkBoxIndex==0 or checkBoxIndex==3 then
        self.m_ChuZhanInfo[0] = bit.bor(self.m_ChuZhanInfo[0], (bit.lshift(1, memberIndex)))
        self.m_ChuZhanInfo[3] = bit.bor(self.m_ChuZhanInfo[3], (bit.lshift(1, memberIndex)))
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],0,true)
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],3,true)
    else
        self.m_ChuZhanInfo[checkBoxIndex] = bit.bor(self.m_ChuZhanInfo[checkBoxIndex], (bit.lshift(1, memberIndex)))
        self:SetCheckBox(self.m_MemberSettingItemList[memberIndex+1],checkBoxIndex,true)
    end

    self.m_IsDirty = true
    self:RefreshLabels()
end
function CLuaQMPKZhanDuiSettingWnd:GetOneCount( value) 
    local cnt = 0
    while value > 0 do
        cnt = cnt + 1
        value = bit.band(value, (value - 1))
    end
    return cnt
end

function CLuaQMPKZhanDuiSettingWnd:OnCloseBtnClicked( go) 
    if self.m_IsDirty then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_ZhanDui_Setting_Save_Tip"), DelegateFactory.Action(function () 
            self:OnSaveBtnClicked(nil)
            -- self:Close()
            CUIManager.CloseUI(CIndirectUIResources.QMPKZhanDuiSettingWnd)
        end), DelegateFactory.Action(function () 
            -- self:Close()
            CUIManager.CloseUI(CIndirectUIResources.QMPKZhanDuiSettingWnd)
        end), nil, nil, false)
    else
        -- self:Close()
        CUIManager.CloseUI(CIndirectUIResources.QMPKZhanDuiSettingWnd)
    end
end
function CLuaQMPKZhanDuiSettingWnd:OnSaveBtnClicked( go) 
    if CQuanMinPKMgr.Inst.m_IsBiWuChuZhanSetting > 0 then
        Gac2Gas.AdjustQmpkBiWuChuZhanInfo(self.m_ChuZhanInfo[0], self.m_ChuZhanInfo[1], self.m_ChuZhanInfo[2], self.m_ChuZhanInfo[3], self.m_ChuZhanInfo[4])
    else
        Gac2Gas.RequestSetQmpkChuZhanInfo(self.m_ChuZhanInfo[0], self.m_ChuZhanInfo[1], self.m_ChuZhanInfo[2], self.m_ChuZhanInfo[3], self.m_ChuZhanInfo[4])
    end
    self.m_IsDirty = false
end
function CLuaQMPKZhanDuiSettingWnd:RefreshLabels( )
    -- do
    --     local i = 0 local cnt = self.CountLabels.Length
    --     while i < cnt do
    --         local count = self:GetOneCount(self.m_ChuZhanInfo[i])
    --         if count == self.m_MaxPlayerInBattle[i+1] then
    --             self.CountLabels[i].text = SafeStringFormat3("(%d/%d)", count, self.m_MaxPlayerInBattle[i+1])
    --         else
    --             self.CountLabels[i].text = SafeStringFormat3("([ff0000]%d[-]/%d)", count, self.m_MaxPlayerInBattle[i+1])
    --         end
    --         i = i + 1
    --     end
    -- end
    for i,v in ipairs(self.CountLabels) do
        -- print(i,v)
        local count=self:GetOneCount(self.m_ChuZhanInfo[i-1])
        if count==self.m_MaxPlayerInBattle[i] then
            v.text=SafeStringFormat3("(%d/%d)", count, self.m_MaxPlayerInBattle[i])
        else
            v.text = SafeStringFormat3("([ff0000]%d[-]/%d)", count, self.m_MaxPlayerInBattle[i])
        end
    end

end
function CLuaQMPKZhanDuiSettingWnd:onHintClick( go) 
    g_MessageMgr:ShowMessage("QMPK_CHUZHAN_RULE")
end

function CLuaQMPKZhanDuiSettingWnd:InitItem(transform,member, index) 
    local IconTexture = transform:Find("PortraitCelll_Sprite"):GetComponent(typeof(CUITexture))
    local LevelLabel = transform:Find("PortraitCelll_Sprite/Level_Label"):GetComponent(typeof(UILabel))
    local NameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local m_LeaderFlagObj = transform:Find("Leader").gameObject
    local m_MemberIndex = index


    local parent=transform:Find("CheckBox")
    for i=1,5 do
        local go=parent:GetChild(i-1).gameObject
        UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
            -- if self.m_IsTeamLeader then
            --     self:OnCheckBoxChanged(m_MemberIndex,i-1)
            -- else
            --     g_MessageMgr:ShowMessage("Qmpk_No_ZhanDui_Operate_Power")
            -- end
            self:OnClickCheckBox(m_MemberIndex,i-1)
        end)
    end

    local pname = CUICommonDef.GetPortraitName(member.m_Class, member.m_Gender, -1)
    IconTexture:LoadNPCPortrait(pname, false)
    LevelLabel.text = tostring(member.m_Grade)
    if self.m_MainPlayerId==member.m_Id then
        NameLabel.text = SafeStringFormat3("[c][00ff00]%s[-][/c]",member.m_Name)
    else
        NameLabel.text = member.m_Name
    end

    m_LeaderFlagObj:SetActive(index == 0)

end
function CLuaQMPKZhanDuiSettingWnd:SetCheckBox(transform,index,selected)
    local parent=transform:Find("CheckBox")
    parent:GetChild(index):Find("Checkmark").gameObject:SetActive(selected)
end
function CLuaQMPKZhanDuiSettingWnd:OnClickCheckBox(memberIndex,index)
    if self.m_IsTeamLeader then
        self:OnCheckBoxChanged(memberIndex,index)
    else
        -- eEnd        = 0,
        -- eQieCuoSai  = 1,
        -- ePiPeiSai   = 2,
        -- eJiFenSai   = 3,
        -- eTaoTaiSai  = 4,
        -- eZongJueSai = 5,
        -- eReShenSai  = 6,
        -- eMeiRiYiZhanSai = 7,
        local msgKey="Qmpk_No_ZhanDui_Operate_Power"
        if CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eQieCuoSai
            or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.ePiPeiSai
            or CLuaQMPKMgr.m_PlayStage == EnumQmpkPlayStage.eMeiRiYiZhanSai then
            msgKey= "Qmpk_No_Team_Operate_Power"
        end

        g_MessageMgr:ShowMessage(msgKey)
    end
end

return CLuaQMPKZhanDuiSettingWnd
