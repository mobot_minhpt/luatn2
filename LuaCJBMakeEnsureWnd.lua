local AlignType2 = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCJBMgr = import "L10.Game.CCJBMgr"
local CCJBMakeEnsureWnd = import "L10.UI.CCJBMakeEnsureWnd"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"


CLuaCJBMakeEnsureWnd = class()
RegistClassMember(CLuaCJBMakeEnsureWnd,"infoText")
RegistClassMember(CLuaCJBMakeEnsureWnd,"itemTexture")
RegistClassMember(CLuaCJBMakeEnsureWnd,"itemNumInput")
RegistClassMember(CLuaCJBMakeEnsureWnd,"itemNumLabel")
RegistClassMember(CLuaCJBMakeEnsureWnd,"itemEmptyNode")
RegistClassMember(CLuaCJBMakeEnsureWnd,"chooseSignBtn")
RegistClassMember(CLuaCJBMakeEnsureWnd,"chooseSignNode")
RegistClassMember(CLuaCJBMakeEnsureWnd,"choosedTip")
RegistClassMember(CLuaCJBMakeEnsureWnd,"unchoosedTip")
RegistClassMember(CLuaCJBMakeEnsureWnd,"useItemTipText")
RegistClassMember(CLuaCJBMakeEnsureWnd,"tipText")
RegistClassMember(CLuaCJBMakeEnsureWnd,"backBtn")
RegistClassMember(CLuaCJBMakeEnsureWnd,"submitBtn")
RegistClassMember(CLuaCJBMakeEnsureWnd,"cancelBtn")
RegistClassMember(CLuaCJBMakeEnsureWnd,"nowLingqiLabel")
RegistClassMember(CLuaCJBMakeEnsureWnd,"totalLingqiLabel")
RegistClassMember(CLuaCJBMakeEnsureWnd,"costText")
RegistClassMember(CLuaCJBMakeEnsureWnd,"nowChooseNum")
RegistClassMember(CLuaCJBMakeEnsureWnd,"itemTemplateId")

function CLuaCJBMakeEnsureWnd:Awake()
    self.infoText = self.transform:Find("Info/InfoText"):GetComponent(typeof(UILabel))
    self.itemTexture = self.transform:Find("Info/InfoItem/icon"):GetComponent(typeof(CUITexture))
    self.itemNumInput = self.transform:Find("Info/BuyArea (1)/Label/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.itemNumLabel = self.transform:Find("Info/InfoItem/icon/num"):GetComponent(typeof(UILabel))
    self.itemEmptyNode = self.transform:Find("Info/InfoItem/emptyNode").gameObject
    self.chooseSignBtn = self.transform:Find("Info/AutoCostJadeToggle/Toggle").gameObject
    self.chooseSignNode = self.transform:Find("Info/AutoCostJadeToggle/Toggle/Sprite").gameObject
    self.choosedTip = self.transform:Find("Info/AutoCostJadeToggle/choosedTip").gameObject
    self.unchoosedTip = self.transform:Find("Info/AutoCostJadeToggle/unChoosedTip").gameObject
    self.useItemTipText = self.transform:Find("Info/ConsumeText"):GetComponent(typeof(UILabel))
    self.tipText = self.transform:Find("Info/TipText"):GetComponent(typeof(UILabel))
    self.submitBtn = self.transform:Find("Info/Ensure").gameObject
    self.cancelBtn = self.transform:Find("Info/BackBtn").gameObject
    self.nowLingqiLabel = self.transform:Find("Info/Lingqi1/bg/num"):GetComponent(typeof(UILabel))
    self.totalLingqiLabel = self.transform:Find("Info/Lingqi2/bg/num"):GetComponent(typeof(UILabel))
    -- self.costText = LocalString.GetString("额外消耗%d点灵气")
    self.nowChooseNum = 0
    self.itemTemplateId = 0
end

function CLuaCJBMakeEnsureWnd:Init( )
    if CClientMainPlayer.Inst == nil then
        CUIManager.CloseUI(CUIResources.CJBMakerEnsureWnd)
        return
    end

    UIEventListener.Get(self.cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CUIManager.CloseUI(CUIResources.CJBMakerEnsureWnd)
    end)

    self.itemTemplateId = ChuanjiabaoModel_Setting.GetData().ExtraItemTempId

    self.tipText.text = g_MessageMgr:FormatMessage("CHUANJIABAO_ADD_ITEM_NOTICE")
    if not System.String.IsNullOrEmpty(CCJBMgr.Inst.enterShowString) then
        self.infoText.text = CCJBMgr.Inst.enterShowString
    else
        self.infoText.text = g_MessageMgr:FormatMessage("CHUANJIABAO_NEED_LINGQI")
    end

    self.nowLingqiLabel.text = tostring(CCJBMakeEnsureWnd.normalLingqiCost)

    self.totalLingqiLabel.text = ""

    UIEventListener.Get(self.submitBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:SubmitInfo(go)
    end)
    UIEventListener.Get(self.itemEmptyNode).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.itemTemplateId, false, go.transform, AlignType2.Right)
    end)

    local iteminfo = CItemMgr.Inst:GetItemTemplate(self.itemTemplateId)
    if iteminfo ~= nil then
        self.itemTexture:LoadMaterial(iteminfo.Icon)
        UIEventListener.Get(self.itemTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.itemTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end

    self.itemNumInput.onValueChanged = DelegateFactory.Action_uint(function(v)
        self:OnValueChange(v)
    end)

    self.chooseSignNode:SetActive(false)
    self.choosedTip:SetActive(false)
    self.unchoosedTip:SetActive(true)
    self:SetExtraItemChoose()
    UIEventListener.Get(self.chooseSignBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if CCJBMgr.EnableEnhanceCJBStar then
            if self.chooseSignNode.activeSelf then
                self.chooseSignNode:SetActive(false)
                self.choosedTip:SetActive(false)
                self.unchoosedTip:SetActive(true)
            else
                self.chooseSignNode:SetActive(true)
                self.choosedTip:SetActive(true)
                self.unchoosedTip:SetActive(false)
            end
            self:SetExtraItemChoose()
        else
            g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        end
    end)

    Gac2Gas.QueryMyHouseLingqi()
end
function CLuaCJBMakeEnsureWnd:OnQueryHouseLingqiResult( args) 
    local  houseId, lingqi=args[0],args[1]
    if CClientMainPlayer.Inst == nil then
        return
    end
    if houseId == CClientMainPlayer.Inst.ItemProp.HouseId then
        self.totalLingqiLabel.text = tostring((math.floor(lingqi)))
    end
end
function CLuaCJBMakeEnsureWnd:OnValueChange( value) 
    if value == 0 then
        value = 1
    end

    self.nowChooseNum = value
    local costNum = 0

    local formulaId = ChuanjiabaoModel_Setting.GetData().LingqiImproveFormulaId
    local func = AllFormulas.Action_Formula[formulaId].Formula

    local rtn = func ~= nil and func(nil,nil,{value}) or 0
    costNum = rtn
    self.useItemTipText.text = SafeStringFormat3(LocalString.GetString("额外消耗%d点灵气"), costNum)
    local amout = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.itemTemplateId)
    if self.chooseSignNode.activeSelf and amout >= value then
        self.nowLingqiLabel.text = tostring((CCJBMakeEnsureWnd.normalLingqiCost + costNum))
    else
        self.nowLingqiLabel.text = tostring(CCJBMakeEnsureWnd.normalLingqiCost)
    end

    if amout >= self.nowChooseNum then
        self.itemNumLabel.text = tostring(amout) .. "/" .. tostring(self.nowChooseNum)
    else
        self.itemNumLabel.text = "[c][FF0000]" .. tostring(amout) .. "[-][/c]/" .. tostring(self.nowChooseNum)
    end
end
function CLuaCJBMakeEnsureWnd:SubmitInfo( go) 

    local chooseValue = self.itemNumInput:GetValue()
    local amout = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.itemTemplateId)
    if chooseValue <= amout then
        CUIManager.CloseUI(CUIResources.CJBMakerEnsureWnd)
        CUIManager.CloseUI(CUIResources.CJBMakeWnd)
        Gac2Gas.RequestMakeNewModel(chooseValue)
    else
        g_MessageMgr:ShowMessage("NOT_ENOUGH_CHANGE_ITEM")
    end
end
function CLuaCJBMakeEnsureWnd:SetExtraItemChoose( )
    if self.chooseSignNode.activeSelf then
        local amout = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.itemTemplateId)

        if amout > 0 then
            self.itemNumInput:SetMinMax(1, amout, 1)
            self.itemNumInput:SetValue(1, true)
        else
            self.itemNumInput:SetMinMax(1, 1, 1)
            self.itemNumInput:SetValue(1, true)
        end
    else
        self.itemNumInput:SetMinMax(0, 0, 1)
        self.itemNumInput:SetValue(0, true)
    end
    self:UpdateItemNum()
end

function CLuaCJBMakeEnsureWnd:UpdateItemNum( )
    local amout = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.itemTemplateId)
    if amout >= self.nowChooseNum then
        self.itemNumLabel.text = tostring(amout) .. "/" .. tostring(self.nowChooseNum)
    else
        self.itemNumLabel.text = "[c][FF0000]" .. tostring(amout) .. "[-][/c]/" .. tostring(self.nowChooseNum)
    end

    if amout > 0 then
        self.itemEmptyNode:SetActive(false)
    else
        self.itemEmptyNode:SetActive(true)
    end
end
function CLuaCJBMakeEnsureWnd:OnSetItemAt( args ) 
local  place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    if oldItemId == nil and newItemId == nil then
        return
    end
    local itemId = oldItemId ~= nil and oldItemId or newItemId
    -- self:OnSendItem(itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.TemplateId == self.itemTemplateId then
        if self.chooseSignNode.activeSelf and CCJBMgr.EnableEnhanceCJBStar then
            self:SetExtraItemChoose()
        end
        self:UpdateItemNum()
    end
end

function CLuaCJBMakeEnsureWnd:OnSendItem( args) 
    local itemId=args[0]
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and item.TemplateId == self.itemTemplateId then
        if self.chooseSignNode.activeSelf and CCJBMgr.EnableEnhanceCJBStar then
            self:SetExtraItemChoose()
        end
        self:UpdateItemNum()
    end
end

function CLuaCJBMakeEnsureWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:AddListener("QueryHouseLingqiResult",self,"OnQueryHouseLingqiResult")
end

function CLuaCJBMakeEnsureWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:RemoveListener("QueryHouseLingqiResult",self,"OnQueryHouseLingqiResult")
end