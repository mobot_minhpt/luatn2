local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIVerticalLabel = import "UIVerticalLabel"

LuaLiuRuShiEndWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuRuShiEndWnd, "PageLabel", "PageLabel", UIVerticalLabel)
RegistChildComponent(LuaLiuRuShiEndWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaLiuRuShiEndWnd, "DescLabel", "DescLabel", UIVerticalLabel)
RegistChildComponent(LuaLiuRuShiEndWnd, "ShareBtn", "ShareBtn", GameObject)

--@endregion RegistChildComponent end

function LuaLiuRuShiEndWnd:Awake()
    UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)
end

function LuaLiuRuShiEndWnd:Init()
    local data = NanDuFanHua_LiuRuShiEnd.GetData(LuaLiuRuShiMgr.m_EndId)
    self.TitleLabel.text = data.Title
    self.DescLabel.fontColor = Color.white
    self.DescLabel.text = data.Description
    self.PageLabel.fontColor = NGUIText.ParseColor24("321a08", 0)
    self.PageLabel.text = data.LetterText
end

--@region UIEvent

function LuaLiuRuShiEndWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.LiuRuShiEndWnd, self.ShareBtn)
end
--@endregion UIEvent

