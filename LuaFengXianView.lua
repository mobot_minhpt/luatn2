require("common/common_include")

local CButton = import "L10.UI.CButton"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

LuaFengXianView = class()

RegistChildComponent(LuaFengXianView, "TaskName", UILabel)
RegistChildComponent(LuaFengXianView, "TaskInfo", UILabel)
RegistChildComponent(LuaFengXianView, "InviteBtn", CButton)
RegistChildComponent(LuaFengXianView, "LeaveBtn", CButton)

function LuaFengXianView:Awake()
	self.TaskName.text = nil
	self.TaskInfo.text = nil
	self.InviteBtn.gameObject:SetActive(false)
end

function LuaFengXianView:Init()

	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)		
		CGamePlayMgr.Inst:LeavePlay()
    end)

	self:UpdateFengXianStatus()
end


function LuaFengXianView:OnSceneRemainTimeUpdate(args)
	if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.TaskInfo.text = SafeStringFormat3(LocalString.GetString("计时 [00FF00]%s[-]"), self:GetRemainTimeText(CScene.MainScene.ShowTime))
        else
            self.TaskInfo.text = nil
        end
    else
        self.TaskInfo.text = nil
    end
end


function LuaFengXianView:GetRemainTimeText(totalSeconds)

	if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GodCoronationWait"))  == 1 or 
		CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("SeeGodCoronationWait")) == 1 then
		totalSeconds = 600 - (3600 - totalSeconds)
	end
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaFengXianView:OnCanLeaveGamePlay()
	if CScene.MainScene then
        self.LeaveBtn.gameObject:SetActive(CScene.MainScene.ShowLeavePlayButton)
    end
end

-- 根据封仙的状态来更新界面
function LuaFengXianView:UpdateFengXianStatus()

	if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GodCoronationWait")) == 1 then
		-- 封仙准备
		self.TaskName.text = LocalString.GetString("封仙大会-准备场")
		self.InviteBtn.gameObject:SetActive(true)
		self.InviteBtn.Text = LocalString.GetString("邀请")
		UIEventListener.Get(self.InviteBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
        	CLuaXianzhiMgr.OpenFengXianInviteWnd()
    	end)

	elseif CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GodCoronationStart")) == 1 then
		-- 封仙进行
		self.TaskName.text = LocalString.GetString("封仙大会")
		self.InviteBtn.gameObject:SetActive(false)

	elseif CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("SeeGodCoronationWait")) == 1 then
		-- 封仙观众准备
		self.TaskName.text = LocalString.GetString("封仙大会-准备场")
		self.InviteBtn.gameObject:SetActive(false)

	elseif CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("SeeGodCoronationStart")) == 1 then
		-- 封仙观众封仙
		self.TaskName.text = LocalString.GetString("封仙大会")
		self.InviteBtn.gameObject:SetActive(false)
	end
end

function LuaFengXianView:OnEnable()
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:AddListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
	g_ScriptEvent:AddListener("FengXianStatusChanged", self, "UpdateFengXianStatus")
end

function LuaFengXianView:OnDisable()
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:RemoveListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
	g_ScriptEvent:RemoveListener("FengXianStatusChanged", self, "UpdateFengXianStatus")
end

return LuaFengXianView
