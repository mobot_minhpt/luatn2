local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UICamera = import "UICamera"
local Extensions = import "Extensions"
local Vector2 = import "UnityEngine.Vector2"

LuaScrollTaskCrosswordGameTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaScrollTaskCrosswordGameTaskView, "Template", "Template", GameObject)
RegistChildComponent(LuaScrollTaskCrosswordGameTaskView, "DescTitle", "DescTitle", UILabel)
RegistChildComponent(LuaScrollTaskCrosswordGameTaskView, "Words", "Words", UIGrid)
RegistChildComponent(LuaScrollTaskCrosswordGameTaskView, "TargetLab", "TargetLab", UILabel)
RegistChildComponent(LuaScrollTaskCrosswordGameTaskView, "DestroyFx", "DestroyFx", CUIFx)
RegistChildComponent(LuaScrollTaskCrosswordGameTaskView, "FinishFx", "FinishFx", CUIFx)
RegistChildComponent(LuaScrollTaskCrosswordGameTaskView, "BackLab", "BackLab", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_Tick")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_Words")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_WordsGo")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_DragOffset")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_Fields")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_SuccessFields")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_Bgs")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_TargetText")
RegistClassMember(LuaScrollTaskCrosswordGameTaskView, "m_IsPlaying")

function LuaScrollTaskCrosswordGameTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Bgs = {}
    for i = 1, 6 do 
        local name = "Bg0" .. i
        table.insert(self.m_Bgs, self.transform:Find("TopRight/"..name):GetComponent(typeof(UITexture)))
    end
end

function LuaScrollTaskCrosswordGameTaskView:Start()
end

function LuaScrollTaskCrosswordGameTaskView:InitGame(gameId, taskId, callback)
    UnRegisterTick(self.m_Tick)
    self:LoadDesignData(gameId)
    
    self.FinishFx:DestroyFx()
    self:InitCrosswordFields()
    self:InitWords()

    self.m_IsPlaying = true
    self.m_Callback = callback
end

function LuaScrollTaskCrosswordGameTaskView:LoadDesignData(gameId)
    local data = ScrollTask_CrosswordGame.GetData(gameId)
    if data == nil then
        return
    end

-- string[] PicturePaths [parser:toStringList]
-- string Crossword
-- string Words
    self.m_TargetText = data.Crossword
    --self.m_Words = data.m_Words
    self.m_Words = {}
    for i = 0, data.Words.Length - 1 do
        table.insert(self.m_Words, data.Words[i])
    end
end
--@region UIEvent

--@endregion UIEvent

function LuaScrollTaskCrosswordGameTaskView:InitCrosswordFields()
    self.m_Fields = {}
    self.m_SuccessFields = {}
    

    -- 处理前景，替换其他字符
    local foregroundText = string.gsub(self.m_TargetText, "[^%$%d%s]*", function (s)
        return s ~= "" and SafeStringFormat3("[c][394258ff]%s[-][/c]", s) or s
    end)

    -- 处理前景，替换[]
    foregroundText = string.gsub(foregroundText, "%$%d+", function (s)
        local index = tonumber(string.match(s, "%$(%d+)"))
        local wordStr = self.m_Words[index]
        table.insert(self.m_Fields, wordStr)
        local replaceStr = SafeStringFormat3("[c][394258ff][url=%s]%s[/url][-][/c]", #self.m_Fields, string.rep("_", string.len(wordStr)))
        table.insert(self.m_SuccessFields, false)
        return replaceStr
    end)

    self.TargetLab.text = foregroundText
    self.BackLab.text = ""
end

function LuaScrollTaskCrosswordGameTaskView:RefreshCrosswordFields()
    local foregroundText = self.TargetLab.text 
    for i = 1, #self.m_SuccessFields do
        if self.m_SuccessFields[i] == true then
            local s = self.m_Fields[i]
            local replaceStr = "%[c%]%[394258ff%]%[url="..i.."%]"..string.rep("_", string.len(s)).."%[%/url%]%[%-%]%[%/c%]"
            foregroundText = string.gsub(foregroundText, replaceStr, s)
        end
    end

    self.TargetLab.text = foregroundText
end

function LuaScrollTaskCrosswordGameTaskView:IsGameFinish()
    for i = 1, #self.m_SuccessFields do
        if self.m_SuccessFields[i] == false then
            return false
        end
    end

    return true
end

function LuaScrollTaskCrosswordGameTaskView:InitWords()
    self.Words.gameObject:SetActive(true)

    if self.m_WordsGo then
        for i, v in ipairs(self.m_WordsGo) do
            GameObject.Destroy(v)
        end
    end

    self.m_WordsGo = {}
    for i = 1, #self.m_Words do
        local wordGo = NGUITools.AddChild(self.Words.gameObject, self.Template)
        self:InitWord(wordGo, self.m_Words[i])
        wordGo:SetActive(true)
        table.insert(self.m_WordsGo, wordGo)
    end

    self.Words:Reposition()
end

function LuaScrollTaskCrosswordGameTaskView:InitWord(go, word)
    local lab = go:GetComponent(typeof(UILabel))
    local bg = go.transform:Find("bg"):GetComponent(typeof(UITexture))
    local randomBg = self.m_Bgs[math.random(1, #self.m_Bgs)]
    lab.text = word
    bg.material = randomBg.material
    bg.height = randomBg.height
    bg.width = randomBg.width
    
    Extensions.SetLocalRotationZ(go.transform, math.random(-15, 15))

    CommonDefs.AddOnDragListener(go, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragStart(go)
    end)

    UIEventListener.Get(go).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragEnd(go, word)
    end)
end

function LuaScrollTaskCrosswordGameTaskView:OnScreenDrag(go, delta)
    local currentPos = UICamera.currentTouch.pos
    go.transform.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))
end

function LuaScrollTaskCrosswordGameTaskView:OnDragStart(go)
    local currentPos = UICamera.currentTouch.pos
    --self.m_DragOffset = CommonDefs.op_Subtraction_Vector3_Vector3(go.transform.position, UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0)))
end

function LuaScrollTaskCrosswordGameTaskView:OnDragEnd(go, word)
    local localPos = self.TargetLab.transform:InverseTransformPoint(UICamera.lastWorldPosition)
    local delta = 25
    for i = -4, 4 do 
        for j = -4, 4 do 
            local localPosV2 = Vector2(localPos.x + i * delta, localPos.y + j * delta)
            local url = self.TargetLab:GetUrlAtPosition(localPosV2)
            if not System.String.IsNullOrEmpty(url) and self.m_Fields[tonumber(url)] == word then
                self:TryCrossword(go, url, word) 
                self.Words:Reposition()
                return
            end
        end
    end
    self.Words:Reposition()
end

function LuaScrollTaskCrosswordGameTaskView:TryCrossword(go, url, word)
    local index = tonumber(url)
    if self.m_Fields[index] == word then
        go:SetActive(false)
        self.m_SuccessFields[index] = true
        self:RefreshCrosswordFields()
        if self:IsGameFinish() and self.m_IsPlaying then
            self.m_IsPlaying = false
            self:OnSuccess()
        end
    end
end

function LuaScrollTaskCrosswordGameTaskView:OnSuccess()
    --self.Words.gameObject:SetActive(false)
    --self.DestroyFx:LoadFx("fx/ui/prefab/UI_juanzhou_zhitiaoxiaoshi.prefab")
    self.FinishFx:LoadFx("fx/ui/prefab/UI_juanzhou_gushipinjie_wancheng.prefab")

    if self.m_WordsGo then
        for i, v in ipairs(self.m_WordsGo) do
            if v.activeSelf then
                local lab = v:GetComponent(typeof(UILabel))
                local destroyFx = v.transform:Find("DestroyFx"):GetComponent(typeof(CUIFx))
                local bg = v.transform:Find("bg"):GetComponent(typeof(UITexture))
                lab.text = ""
                bg.alpha = 0
                destroyFx:LoadFx("fx/ui/prefab/UI_juanzhou_zhitiaoxiaoshi.prefab")
            end
        end
    end

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:OnGameFinish()
    end,self.m_FinishDelay or 2000)
end

function LuaScrollTaskCrosswordGameTaskView:OnGameFinish()
    if self.m_Callback then
        self.m_Callback()
    end

    if self.m_WordsGo then
        for i, v in ipairs(self.m_WordsGo) do
            GameObject.Destroy(v)
        end
    end
end

function LuaScrollTaskCrosswordGameTaskView:OnDestroy()
    UnRegisterTick(self.m_Tick)
end