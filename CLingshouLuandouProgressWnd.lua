-- Auto Generated!!
local CLingshouLuandouMgr = import "L10.Game.CLingshouLuandouMgr"
local CLingshouLuandouProgressWnd = import "L10.UI.CLingshouLuandouProgressWnd"
local Hanjiahuodong_Setting = import "L10.Game.Hanjiahuodong_Setting"
local Time = import "UnityEngine.Time"
local UIBasicSprite = import "UIBasicSprite"
local UISprite = import "UISprite"
CLingshouLuandouProgressWnd.m_StartCountDown_CS2LuaHook = function (this) 
    this.startTime = Time.realtimeSinceStartup
    this.passTime = CLingshouLuandouMgr.Instance.PassedTime
    this.totalTime = Hanjiahuodong_Setting.GetData().BattleTime
    this.Canrefresh = true
end
CLingshouLuandouProgressWnd.m_switchShow_CS2LuaHook = function (this, go) 
    if this.InfoRoot.activeSelf then
        this.InfoRoot:SetActive(false)
        CommonDefs.GetComponent_GameObject_Type(this.HideButton, typeof(UISprite)).flip = UIBasicSprite.Flip.Horizontally
    else
        this.InfoRoot:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.HideButton, typeof(UISprite)).flip = UIBasicSprite.Flip.Nothing
    end
end
