local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local EnumPreviewType = import "L10.UI.EnumPreviewType"
local CIMMgr = import "L10.Game.CIMMgr"


LuaZhouNianQingLifeFriendWnd = class()

RegistClassMember(LuaZhouNianQingLifeFriendWnd, "m_Bracelet1")
RegistClassMember(LuaZhouNianQingLifeFriendWnd, "m_Bracelet2")

RegistClassMember(LuaZhouNianQingLifeFriendWnd, "m_LastPreviewType")

function LuaZhouNianQingLifeFriendWnd:Awake()
    self.m_LastPreviewType = CFashionPreviewMgr.curPreviewType
    local tabenId = ZhouNianQing2023_PSZJSetting.GetData().TabenItemId
    for i = 1, tabenId.Length do
        self["m_Bracelet"..i] = self.transform:Find("Anchor/Taben/Bracelet"..i).gameObject
        self["m_Bracelet"..i].transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(Item_Item.GetData(tabenId[i-1]).Icon)
        UIEventListener.Get(self["m_Bracelet"..i]).onClick = DelegateFactory.VoidDelegate(function(go)
            CFashionPreviewMgr.curPreviewType = EnumPreviewType.PreviewBraceletTaben
            LuaFashionPreviewTextureLoaderMgr.ModelNum = tabenId.Length
            LuaFashionPreviewTextureLoaderMgr.CurModelIdx = i
            CUIManager.ShowUI(CUIResources.FashionPreviewWnd)
        end)
    end
    
    local view = self.transform:Find("Anchor/Content/ScrollView")
    local table = view:Find("Table"):GetComponent(typeof(UITable))
    local template = view:Find("Template").gameObject
    template:SetActive(false)
    local rules = ZhouNianQing2023_PSZJSetting.GetData().PSZJ_RULE
    for p in string.gmatch(rules, "<p>(.-)</p>") do
        local pObj = NGUITools.AddChild(table.gameObject, template)
        pObj:SetActive(true)
        pObj:GetComponent(typeof(UILabel)).text = p
    end
    table:Reposition()
end

function LuaZhouNianQingLifeFriendWnd:Init()
    if CClientMainPlayer.Inst then
        self.transform:Find("Anchor/Content/Self/Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
        local nameLabel = self.transform:Find("Anchor/Content/Self/Name"):GetComponent(typeof(UILabel))
        nameLabel.text = CClientMainPlayer.Inst.Name
        nameLabel.color = NGUIText.ParseColor24("157000", 0)

        self:InitZhiJi()
    end
end

function LuaZhouNianQingLifeFriendWnd:OnDestroy()
    CFashionPreviewMgr.curPreviewType = self.m_LastPreviewType
end

function LuaZhouNianQingLifeFriendWnd:OnEnable()
    g_ScriptEvent:AddListener("ZNQ2023_SetConfidantId", self, "Init")
end

function LuaZhouNianQingLifeFriendWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ZNQ2023_SetConfidantId", self, "Init")
end

function LuaZhouNianQingLifeFriendWnd:InitZhiJi()
    local tr = FindChild(self.transform, "ZhiJi")
    local portrait = tr:Find("Portrait"):GetComponent(typeof(CUITexture))
    local btn = tr:Find("Btn").gameObject
    local name = tr:Find("Name"):GetComponent(typeof(UILabel))

    local ret, info = CommonDefs.DictTryGet_LuaCall(CClientMainPlayer.Inst.RelationshipProp.TempRelationData, 1)
	if ret and info.ExpiredTime > CServerTimeMgr.Inst.timeStamp and tonumber(info.Data.StringData) > 0 then
		local playerId = tonumber(info.Data.StringData)
        local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
        portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(basicInfo.Class, basicInfo.Gender, basicInfo.Expression), false)
        UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
        end)

        btn:SetActive(false)
        name.gameObject:SetActive(true)
        name.text = basicInfo.Name
    else
        portrait.material = nil
        local npc = ZhouNianQing2023_PSZJSetting.GetData().PSZJ_NPC
        UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
            --g_MessageMgr:ShowMessage("PSZJ_NPC_GO")
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("PSZJ_NPC_GO"),
                DelegateFactory.Action(function() 
                    CUIManager.CloseUI(CLuaUIResources.ZhouNianQingLifeFriendWnd)
                    CUIManager.CloseUI(CLuaUIResources.ZhouNianQing2023MainWnd)
                    CTrackMgr.Inst:FindNPC(npc[0], npc[1], npc[2], npc[3], nil, nil)
                end), nil, nil, nil, false)
        end)
        btn:SetActive(true)
        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go) 
            --g_MessageMgr:ShowMessage("PSZJ_NPC_GO")
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("PSZJ_NPC_GO"),
                DelegateFactory.Action(function() 
                    CUIManager.CloseUI(CLuaUIResources.ZhouNianQingLifeFriendWnd)
                    CUIManager.CloseUI(CLuaUIResources.ZhouNianQing2023MainWnd)
                    CTrackMgr.Inst:FindNPC(npc[0], npc[1], npc[2], npc[3], nil, nil)
                end), nil, nil, nil, false)
        end)
        name.gameObject:SetActive(false)
	end
end
