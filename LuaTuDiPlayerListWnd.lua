local Extensions = import "Extensions"
local EnumClass = import "L10.Game.EnumClass"
local CIMMgr = import "L10.Game.CIMMgr"
local Profession = import "L10.Game.Profession"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"

LuaTuDiPlayerListWnd = class()

RegistChildComponent(LuaTuDiPlayerListWnd, "m_Table","Table", UITable)
RegistChildComponent(LuaTuDiPlayerListWnd, "m_ItemTemplate","ItemTemplate", GameObject)

function LuaTuDiPlayerListWnd:Init()
    local mainPlayer = CClientMainPlayer.Inst
    self.m_ItemTemplate:SetActive(false)
    if mainPlayer then
        local tudiDict = mainPlayer.RelationshipProp.TuDi
        local t = {}
        local waiMenList = {}
        CommonDefs.DictIterate(tudiDict, DelegateFactory.Action_object_object(function(key,val)
            if val.Finished == 0 then
                local isWaiMen = val and val.Extra:GetBit(2) or false
                if isWaiMen then
                    table.insert(waiMenList,val)
                else
                    table.insert(t,val)
                end
            end
        end))
        table.sort( t,function(a,b)
            if CFamilyTreeMgr.Instance:CheckRuShiPlayer(a.PlayerId) then
                return true
            end
            if CFamilyTreeMgr.Instance:CheckRuShiPlayer(b.PlayerId) then
                return false
            end
            return a.Time < b.Time
        end )
        for _,val in pairs(waiMenList) do
            table.insert(t,val)
        end
        Extensions.RemoveAllChildren(self.m_Table.transform)
        if #t == 0 then return end
        for i = 1, #t do
            local obj = NGUITools.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
            local indexText = ShiTu_Setting.GetData().MemberSerialNumber[i - 1]
            local hasRuShi = CFamilyTreeMgr.Instance:CheckRuShiPlayer(t[i].PlayerId)
            local isWaiMen = t[i] and (t[i].Extra:GetBit(2)) or false
            local text = hasRuShi and LocalString.GetString("入室弟子") or (isWaiMen and LocalString.GetString("外门弟子") or SafeStringFormat3(LocalString.GetString("%s徒弟"),indexText))
            self:InitItem(obj, t[i], text)
        end
        self.m_Table:Reposition()
    end
end

function LuaTuDiPlayerListWnd:InitItem(go, shituinfo, tudiRankTxt)
    go:SetActive(true)
    local info = CIMMgr.Inst:GetBasicInfo(shituinfo.PlayerId)
    local professionIcon = go.transform:Find("Icon"):GetComponent(typeof(UISprite))
    professionIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.Class))
    local portrait = go.transform:Find("Border/Portrait"):GetComponent(typeof(CUITexture))
    portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Class, info.Gender, info.Expression))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = info.Name
    local rankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text = tudiRankTxt
    local btn = go.transform:Find("Button").gameObject
    local playerId = shituinfo.PlayerId
    btn:GetComponent(typeof(CButton)).Text = LuaShiTuTrainingHandbookMgr.m_TuDiPlayerListWndBtnText
    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
        if LuaShiTuTrainingHandbookMgr.m_TuDiPlayerListWndBtnAction then
            LuaShiTuTrainingHandbookMgr.m_TuDiPlayerListWndBtnAction(playerId)
        end
    end)
    UIEventListener.Get(go.transform:Find("Border").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
    end)
end