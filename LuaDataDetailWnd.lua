require("common/common_include")
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local Vector3 = import "UnityEngine.Vector3"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
--local Gac2Gas = import "L10.Game.Gac2Gas"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local UIGrid = import "UIGrid"
local Object = import "UnityEngine.Object"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Color = import "UnityEngine.Color"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LocalString = import "LocalString"


CLuaDataDetailWnd=class()

RegistChildComponent(CLuaDataDetailWnd, "Pool", CUIGameObjectPool)
RegistChildComponent(CLuaDataDetailWnd, "Table", UIGrid)
RegistChildComponent(CLuaDataDetailWnd, "m_AllBtn", QnSelectableButton)
RegistChildComponent(CLuaDataDetailWnd, "m_YitiaolongBtn", QnSelectableButton)
RegistChildComponent(CLuaDataDetailWnd, "m_GuanningBtn", QnSelectableButton)
RegistChildComponent(CLuaDataDetailWnd, "m_ShouxiBtn", QnSelectableButton)
RegistChildComponent(CLuaDataDetailWnd, "m_GaochangBtn", QnSelectableButton)
RegistChildComponent(CLuaDataDetailWnd, "m_HaoyouBtn", QnSelectableButton)
RegistChildComponent(CLuaDataDetailWnd, "TableView", CUIRestrictScrollView)


RegistClassMember(CLuaDataDetailWnd,"m_finishedEvents")


function CLuaDataDetailWnd:Awake()
	--self.m_finishedEvents={}
    self.m_AllBtn:SetSelected(true,false)
	self.m_YitiaolongBtn:SetSelected(true,false)
	self.m_GuanningBtn:SetSelected(true,false)
	self.m_ShouxiBtn:SetSelected(true,false)
	self.m_GaochangBtn:SetSelected(true,false)
	self.m_HaoyouBtn:SetSelected(true,false)
	
    CommonDefs.AddOnClickListener(self.m_AllBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:ClearList()
		self:SetOthersNotSelected()
		self.m_AllBtn:SetSelected(true,false)
		self:InitWndData(0)
	end), false)
	
	CommonDefs.AddOnClickListener(self.m_YitiaolongBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:ClearList()
		self:SetOthersNotSelected()
		self.m_YitiaolongBtn:SetSelected(true,false)
		self:InitWndData(1)
		
	end), false)
	CommonDefs.AddOnClickListener(self.m_GuanningBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:ClearList()
		self:SetOthersNotSelected()
		self.m_GuanningBtn:SetSelected(true,false)
		self:InitWndData(2)
	end), false)
	CommonDefs.AddOnClickListener(self.m_ShouxiBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:ClearList()
		self:SetOthersNotSelected()
		self.m_ShouxiBtn:SetSelected(true,false)
		self:InitWndData(3)
	end), false)
	CommonDefs.AddOnClickListener(self.m_GaochangBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:ClearList()
		self:SetOthersNotSelected()
		self.m_GaochangBtn:SetSelected(true,false)
		self:InitWndData(4)
	end), false)
	CommonDefs.AddOnClickListener(self.m_HaoyouBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:ClearList()
		self:SetOthersNotSelected()
		self.m_HaoyouBtn:SetSelected(true,false)
		self:InitWndData(5)
	end), false)
	self:InitData()--初始化界面
end

function CLuaDataDetailWnd:Init()
	
	
end



function CLuaDataDetailWnd:ClearList()

	local childList = self.Table:GetChildList()
	--os.date()
    do
        local i = 0
        while i < childList.Count do
            local trans = childList[i]
            trans.parent = nil
            Object.Destroy(trans.gameObject)
            i = i + 1
        end
    end
end

function CLuaDataDetailWnd:SetOthersNotSelected()
	self.m_AllBtn:SetSelected(false,false)
	self.m_HaoyouBtn:SetSelected(false,false)
	self.m_GaochangBtn:SetSelected(false,false)
	self.m_ShouxiBtn:SetSelected(false,false)
	self.m_GuanningBtn:SetSelected(false,false)
	self.m_YitiaolongBtn:SetSelected(false,false)
end

function CLuaDataDetailWnd:InitData(finishedList)
	self:ClearList()
	self:SetOthersNotSelected()
	self.m_AllBtn:SetSelected(true,false)
	self.m_finishedEvents=finishedList

	if not self.gameObject.activeSelf then 
		return 
	end

	self:ClearList()
	local begintime = CLuaMergeBattleMgr.m_beginTime--unix时间戳
	local todayTime = CServerTimeMgr.Inst:GetZone8Time()
	local index =0
	if begintime then
		index = todayTime.DayOfYear-CServerTimeMgr.ConvertTimeStampToZone8Time(begintime).DayOfYear+1
	end
	
	if self.m_finishedEvents==nil then --如果没有RPC，初始化无数据的界面
		for i=1,5 do  
			for j=1,30 do
				if MergeBattle_Event.Exists(i*10000+j) then
					self:AddItemToTable(i*10000+j,false,true,-1)
			    end
			end
	    end
	    self.Table:Reposition()
		self.TableView:SetDragAmount(0, 0, false)
	    return
	end

	if index<=0 and CLuaMergeBattleMgr.m_bStart==false then --活动未开始
		for i=1,5 do  
			for j=1,30 do
				if MergeBattle_Event.Exists(i*10000+j) then
					self:AddItemToTable(i*10000+j,false,true,-2)
			    end
			end
	    end
	    self.Table:Reposition()
		self.TableView:SetDragAmount(0, 0, false)
	    return
	elseif index>6 then --活动过期 在展示结果阶段
		for i=1,5 do  
			for j=1,30 do
				if MergeBattle_Event.Exists(i*10000+j) then
					local ddata = MergeBattle_Event.GetData(i*10000+j)
					if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,true,ddata.Day)
		            else
		             	self:AddItemToTable(i*10000+j,false,true,ddata.Day)
		            end
			    end
			end
	    end
	    self.Table:Reposition()
		self.TableView:SetDragAmount(0, 0, false)
	    return
	end

    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day==index then--今日
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,false,ddata.Day)
		             else
		             	self:AddItemToTable(i*10000+j,false,false,ddata.Day)
		             end
		        end
		    end
		end
    end


    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day==0 then--本周任务
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,false,0)
		             else
		             	self:AddItemToTable(i*10000+j,false,false,0)
		             end
		        end
		    end
		end
    end

    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day>index then--之后的任务
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,false,ddata.Day)
		             else
		             	self:AddItemToTable(i*10000+j,false,false,ddata.Day)
		             end
		        end
		    end
		end
    end

    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day<index and ddata.Day~=0 then--过期的任务
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,true,ddata.Day)
		             else
		             	self:AddItemToTable(i*10000+j,false,true,ddata.Day)
		             end
		        end
		    end
		end
    end

    self.Table:Reposition()
    self.TableView:SetDragAmount(0, 0, false)
end

function CLuaDataDetailWnd:InitWndData(indexwnd)--index代表玩法窗口，0-5个窗口，0为全部（第一个窗口）
	self:ClearList()
	local childList = self.Table:GetChildList()
	if indexwnd==0 then 
		self:InitData(self.m_finishedEvents)
		return
	end

	local begintime = CLuaMergeBattleMgr.m_beginTime--unix时间戳
	local todayTime = CServerTimeMgr.Inst:GetZone8Time()
	local index =0
	if begintime then
		index = todayTime.DayOfYear-CServerTimeMgr.ConvertTimeStampToZone8Time(begintime).DayOfYear+1
	end

	if self.m_finishedEvents==nil then 
		for i=1,5 do  
			for j=1,30 do
				if MergeBattle_Event.Exists(i*10000+j) then
					local ddata = MergeBattle_Event.GetData(i*10000+j)
					if ddata.PlayGroup==indexwnd then 
	            		self:AddItemToTable(i*10000+j,false,true,-1)
	            	end
			    end
			end
	    end
	    self.Table:Reposition()
		self.TableView:SetDragAmount(0, 0, false)
	    return
	end

	if index<=0 and CLuaMergeBattleMgr.m_bStart==false then --活动未开始
		for i=1,5 do  
			for j=1,30 do
				if MergeBattle_Event.Exists(i*10000+j) then
					local ddata = MergeBattle_Event.GetData(i*10000+j)
					if ddata.PlayGroup==indexwnd then
						self:AddItemToTable(i*10000+j,false,true,-2)
					end
			    end
			end
	    end
	    self.Table:Reposition()
		self.TableView:SetDragAmount(0, 0, false)
	    return
	elseif index>6 then --活动过期 在展示结果阶段
		for i=1,5 do  
			for j=1,30 do
				if MergeBattle_Event.Exists(i*10000+j) then
					local ddata = MergeBattle_Event.GetData(i*10000+j)
					if ddata.PlayGroup==indexwnd then
						if self:CheckDataFinished(i*10000+j) then
							self:AddItemToTable(i*10000+j,true,true,ddata.Day)
						else
							self:AddItemToTable(i*10000+j,false,true,ddata.Day)
						end
					end
			    end
			end
	    end
	    self.Table:Reposition()
		self.TableView:SetDragAmount(0, 0, false)
	    return
	end

    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day==index and ddata.PlayGroup==indexwnd then
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,false,ddata.Day)
		             else
		             	self:AddItemToTable(i*10000+j,false,false,ddata.Day)
		             end
		        end
		    end
		end
    end


    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day==0 and ddata.PlayGroup==indexwnd then
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,false,0)
		             else
		             	self:AddItemToTable(i*10000+j,false,false,0)
		             end
		        end
		    end
		end
    end

    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day>index and ddata.PlayGroup==indexwnd then
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,false,ddata.Day)
		             else
		             	self:AddItemToTable(i*10000+j,false,false,ddata.Day)
		             end
		        end
		    end
		end
    end

    for i=1,5 do  --为了保证顺序，不能用pairs （Hash表的索引顺序）
		for j=1,30 do
			if MergeBattle_Event.Exists(i*10000+j) then
				local ddata = MergeBattle_Event.GetData(i*10000+j)
	            if ddata.Day<index and ddata.PlayGroup==indexwnd and ddata.Day~=0 then
	             	if self:CheckDataFinished(i*10000+j) then
		             	self:AddItemToTable(i*10000+j,true,true,ddata.Day)
		             else
		             	self:AddItemToTable(i*10000+j,false,true,ddata.Day)
		             end
		        end
		    end
		end
    end


    self.Table:Reposition()
	self.TableView:SetDragAmount(0, 0, false)
end

function CLuaDataDetailWnd:CheckDataFinished(index)
	if self.m_finishedEvents then
		for i = 0, self.m_finishedEvents.Count - 1 do
			if index==self.m_finishedEvents[i] then
			return true
			end
		end
		return false
	end
	return false
end

function CLuaDataDetailWnd:AddItemToTable(finishedId,finished,expired,day)
	local item = self.Pool:GetFromPool(0)
	if not item then 
		return
	end
	item.transform.parent = self.Table.transform
	item.transform.localScale = Vector3.one
	item.gameObject:SetActive(true)
	local eventLabel = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("eventLabel"), typeof(UILabel))
	local pointsLabel = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("pointsLabel"), typeof(UILabel))--积分
	local guoqiLabel = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("guoqiLabel"), typeof(UILabel))--过期
	local timeLabel = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("timeLabel"), typeof(UILabel))--时间
	local toggle = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("Toggle"), typeof(UISprite))--完成框
	local finishedSprite = CommonDefs.GetComponent_Component_Type(toggle.gameObject.transform:Find("Sprite"), typeof(UISprite))--完成的对勾
	local bgSprite = CommonDefs.GetComponent_GameObject_Type(item, typeof(UISprite))

	local finishedDData = MergeBattle_Event.GetData(finishedId)
	eventLabel.text=finishedDData.Description
	pointsLabel.text=finishedDData.Score
	timeLabel.text=finishedDData.Day
	if finished then
		toggle.gameObject:SetActive(true)
		finishedSprite.gameObject:SetActive(true)
		guoqiLabel.gameObject:SetActive(false)
		--pointsLabel.color=Color.white
		CUICommonDef.SetActive(pointsLabel.gameObject, true, true)
	else
		toggle.gameObject:SetActive(true)
		finishedSprite.gameObject:SetActive(false)
		guoqiLabel.gameObject:SetActive(false)
		--pointsLabel.color=Color.black
		CUICommonDef.SetActive(pointsLabel.gameObject, false, true)
	end

	local begintime = CLuaMergeBattleMgr.m_beginTime--unix时间戳
	local todayTime = CServerTimeMgr.Inst:GetZone8Time()
	if begintime then 

		local zone8day=CServerTimeMgr.ConvertTimeStampToZone8Time(begintime+86400*(day-1)).Day--     Time.GetNextDay0AMTimeStampInZone8()
		local zone8month=CServerTimeMgr.ConvertTimeStampToZone8Time(begintime+86400*(day-1)).Month

		timeLabel.text=System.String.Format(LocalString.GetString("{0}月{1}日"), zone8month,zone8day)
		bgSprite.color=Color(28/255,61/255,106/255,1)
		toggle.gameObject:SetActive(true)
		guoqiLabel.gameObject:SetActive(false)
		if todayTime.Day==zone8day then
			timeLabel.text=LocalString.GetString("今日")
			timeLabel.color=Color.green
			bgSprite.color=Color(60/255,97/255,150/255,1)
		end
	end

	if day==0 then
		timeLabel.text=LocalString.GetString("本周")
		--bgSprite.color=Color(28/255,61/255,106/255,1)
	end
	
	if expired then
		bgSprite.color=Color(60/255,73/255,93/255,1)
		if not finished then
			toggle.gameObject:SetActive(false)
			guoqiLabel.gameObject:SetActive(true)
		else
			toggle.gameObject:SetActive(true)
			guoqiLabel.gameObject:SetActive(false)
		end
	end
	if day==-1 then --过期，初始化界面
		timeLabel.text=""
		timeLabel.color=Color.white
		bgSprite.color=Color(60/255,73/255,93/255,1)
		toggle.gameObject:SetActive(false)
		guoqiLabel.gameObject:SetActive(true)
	end

	if day==-2 then --未开始的界面
		timeLabel.text=LocalString.GetString("下周")
		timeLabel.color=Color.white
		bgSprite.color=Color(28/255,61/255,106/255,1)
		toggle.gameObject:SetActive(true)
		guoqiLabel.gameObject:SetActive(false)
	end

	if self:isEnd()==true and day==0 then --结果展示时，本周任务要处理一下
		timeLabel.text=""
	end
end


function CLuaDataDetailWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryMergeBattlePersonalEventsResult", self, "InitData")

	Gac2Gas.QueryMergeBattlePersonalEvents()
end

function CLuaDataDetailWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryMergeBattlePersonalEventsResult", self, "InitData")
end

function CLuaDataDetailWnd:isEnd()
	local begintime = CLuaMergeBattleMgr.m_beginTime--unix时间戳
	local todayTime = CServerTimeMgr.Inst:GetZone8Time()
	local index =0
	if begintime then
		index = todayTime.DayOfYear-CServerTimeMgr.ConvertTimeStampToZone8Time(begintime).DayOfYear+1
	end
	if index>6 then 
		return true
	end
	return false
end

