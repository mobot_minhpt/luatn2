-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CQMPKScoreItem = import "L10.UI.CQMPKScoreItem"
local EnumClass = import "L10.Game.EnumClass"
local Extensions = import "Extensions"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UISprite = import "UISprite"

CQMPKScoreItem.m_Init_CS2LuaHook = function (this, score, index, isMyZhandui)
    if not isMyZhandui then
        if index % 2 ~= 0 then
            this:SetBackgroundTexture("common_textbg_02_light")
        else
            this:SetBackgroundTexture("common_textbg_02_dark")
        end
    end
    if not isMyZhandui then
        this.m_NameLabel.text = score.m_Name
    end

    this.m_TimeLabel.text = SafeStringFormat3("%02d:%02d:%02d", math.floor(score.m_Time / 3600), math.floor((score.m_Time % 3600) / 60), score.m_Time % 3600 % 60)
    --this.m_TimeLabel.text = System.String.Format("{0:D2}:{1:D2}:{2:D2}", math.floor(score.m_Time / 3600), math.floor((score.m_Time % 3600) / 60), score.m_Time % 3600 % 60)

    this.m_ScoreLabel.text = tostring(score.m_Score)
    if isMyZhandui then
        this.m_RankLabel.text = score.m_Rank > 0 and tostring(score.m_Rank) or LocalString.GetString("未上榜")
    else
        this.m_RankLabel.text = tostring(score.m_Rank)
    end
    Extensions.RemoveAllChildren(this.m_MemberGrid.transform)
    do
        local i = 0 local len = score.m_Member.Length
        while i < len do
            local go = NGUITools.AddChild(this.m_MemberGrid.gameObject, this.m_MemberObj)
            go:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(go, typeof(UISprite)).spriteName = L10.Game.Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), score.m_Member[i]))
            i = i + 1
        end
    end
    this.m_MemberGrid:Reposition()
end
