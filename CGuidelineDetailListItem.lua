-- Auto Generated!!
local CGuidelineDetailListItem = import "L10.UI.CGuidelineDetailListItem"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local EnumActionType = import "L10.UI.EnumActionType"
local LocalString = import "LocalString"
CGuidelineDetailListItem.m_Init_CS2LuaHook = function (this, data) 
    this.iconTexture:LoadMaterial(data.iconPath)
    this.nameLabel.text = data.name
    this.descLabel.text = data.desc
    this.gotoBtn.Text = data.btnText

    local showJoinBtn = function(activityIds)
        local canJoin = false
        local isJoin = false
		for _, id in ipairs(activityIds) do
			if CScheduleMgr.Inst:IsCanJoinSchedule(id, true) then
                canJoin = true
				if CScheduleMgr.Inst:IsInProgress(id) then
					this.gotoBtn.Text = LocalString.GetString("已接")
					isJoin = true
				elseif CScheduleMgr.Inst:IsRegardAsFinished(id) then
					this.gotoBtn.Text = LocalString.GetString("完成")
					isJoin = true
				end
			end
		end
        this.gotoBtn.Enabled = canJoin and not isJoin
    end

    if data.action == nil then
        this.gotoBtn.Enabled = false
    elseif data.action ~= nil and data.action.actionType == EnumActionType.Undefined then
        this.gotoBtn.Enabled = false
    elseif data.action ~= nil and data.action.actionType == EnumActionType.DoSchedule then
        showJoinBtn({data.action.activityId})
    elseif data.action ~= nil and data.action.actionName == "JoinGaoChang" then
        showJoinBtn({42000018, GaoChangJiDou_Setting.GetData().GaoChangJiDouScheduleId})
    else
        this.gotoBtn.Enabled = true
    end
    this.guideData = data
end