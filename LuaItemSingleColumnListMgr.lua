require("common/common_include")

local CUIManager = import "L10.UI.CUIManager"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"

LuaItemSingleColumnListMgr = {}

LuaItemSingleColumnListMgr.m_Title = nil
LuaItemSingleColumnListMgr.m_AllData = nil
LuaItemSingleColumnListMgr.targetWorldPos = Vector3.zero
LuaItemSingleColumnListMgr.targetWidth = 0
LuaItemSingleColumnListMgr.targetHeight = 0
LuaItemSingleColumnListMgr.alignType = nil
LuaItemSingleColumnListMgr.callback = nil


LuaItemSingleColumnListMgr.ShowWnd = function(title, dataList, targetWorldPos, targetWidth, targetHeight, alignType, callback)
	LuaItemSingleColumnListMgr.m_Title = title
	LuaItemSingleColumnListMgr.m_AllData = dataList
	LuaItemSingleColumnListMgr.targetWorldPos = targetWorldPos
	LuaItemSingleColumnListMgr.targetWidth = targetWidth
	LuaItemSingleColumnListMgr.targetHeight = targetHeight
	LuaItemSingleColumnListMgr.alignType = alignType
	LuaItemSingleColumnListMgr.callType = type
	LuaItemSingleColumnListMgr.callback = callback
	CUIManager.ShowUI(CIndirectUIResources.ItemSingleColumnListWnd)
end

LuaItemSingleColumnListMgr.OnSelect = function(index)
	if LuaItemSingleColumnListMgr.callback then
		LuaItemSingleColumnListMgr.callback(index)
		LuaItemSingleColumnListMgr.callback = nil
	end
end
