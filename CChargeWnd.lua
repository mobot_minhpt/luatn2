-- Auto Generated!!
local CChargeItem = import "L10.UI.CChargeItem"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local ChannelDefine = import "L10.Game.ChannelDefine"
local Charge_Charge = import "L10.Game.Charge_Charge"
local CChargeMgr = import "L10.Game.CChargeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPayMgr = import "L10.Game.CPayMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local NtUniSdk = import "NtUniSdk"
local UInt16 = import "System.UInt16"
local UInt32 = import "System.UInt32"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"

CChargeWnd.m_HookAwake = function(this)
    this.m_ChargeItemIds = CreateFromClass(MakeGenericClass(List, UInt32))
    this.m_VipIds = CreateFromClass(MakeGenericClass(List, UInt32))

    local enableBigPay = false
    if(CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eAllowBigAmountPay))then
      local bigPayStatus = tonumber(CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eAllowBigAmountPay).StringData)

      local vipLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0
      local needVipLevel = tonumber(Charge_ChargeSetting.GetData("BigAmountPayVipLevel").Value)
      if ((not bigPayStatus or bigPayStatus == 0) and vipLevel >= needVipLevel) or bigPayStatus == 1 then --do big pay
        enableBigPay = true
      end
    else
      local vipLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0
      local needVipLevel = tonumber(Charge_ChargeSetting.GetData("BigAmountPayVipLevel").Value)
      if vipLevel >= needVipLevel then --do big pay
        enableBigPay = true
      end
    end
    do
        local i = 0
        while i < CPayMgr.ChargeItems.Count do
            local continue
            repeat
                local id = CPayMgr.ChargeItems[i]
                local data = Charge_Charge.GetData(id)
                if data == nil then
                    continue = true
                    break
                end
                --fix #69693 【海外渠道】google play充值屏蔽一元档
                if data.IsRmbPackage > 0 then
                    continue = true
                    break
                end

                if data.IsShowCharge <= 0 then
                    continue = true
                    break
                end

                if CommonDefs.IS_VN_CLIENT then
                  if data.ID > 6 then
                    continue = true
                  end
                else
                  if data.Price <= 1.5 and NtUniSdk.Unity3d.SdkU3d.getPayChannel() == ChannelDefine.GOOGLE then
                    continue = true
                    break
                  end
                end

                if id == LuaWelfareMgr.BigMonthCardId and not LuaWelfareMgr.IsOpenBigMonthCard then
                    continue = true
                    break
                end

                if enableBigPay then
                  if math.floor((data.IsShowCharge%100)/10) == 1 then
                    CommonDefs.ListAdd(this.m_ChargeItemIds, typeof(UInt32), id)
                  end
                else
                  if data.IsShowCharge%10 == 1 then
                    CommonDefs.ListAdd(this.m_ChargeItemIds, typeof(UInt32), id)
                  end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    Charge_VipInfo.Foreach(function (id, data)
        if data.Status > 0 then
            return
        end
        CommonDefs.ListAdd(this.m_VipIds, typeof(UInt32), id)
    end)
    CChargeWnd.MAX_VIP_LEVEL = this.m_VipIds.Count
    CommonDefs.ListSort1(this.m_ChargeItemIds, typeof(UInt32), nil)
    CommonDefs.ListSort1(this.m_VipIds, typeof(UInt32), nil)

    if CommonDefs.IS_VN_CLIENT then
        if CommonDefs.Is_PC_PLATFORM() or CommonDefs.Is_UNITY_STANDALONE_WIN() then
            for i = 0, this.m_ChargeItemIds.Count - 1 do
                local data = Charge_Charge.GetData(this.m_ChargeItemIds[i])
                local originalShowPrice = data.ShowPrice
                local priceList = g_LuaUtil:StrSplit(originalShowPrice, ";")
                LuaSEASdkMgr:AddProductPrice(data.PID, priceList[LocalString.languageId+1])
            end
        else
            local pidStr = ""
            for i = 0, this.m_ChargeItemIds.Count - 1 do
                local data = Charge_Charge.GetData(this.m_ChargeItemIds[i])
                if i ~= 0 then
                    pidStr = pidStr .. ","
                end
                pidStr = pidStr .. "\"" .. data.PID .. "\""
            end
            SdkU3d.ntExtendFunc("{\"methodId\":\"vngProductInfo\", \"items\":["..pidStr.."]}")
        end
    end
    
    this.m_ChargeTable.m_DataSource = this
    this.m_SpecialLabel.gameObject:SetActive(CommonDefs.IS_CN_CLIENT)

end
CChargeWnd.m_OnDisable_CS2LuaHook = function (this)
    this.m_ChargeTable.OnSelectAtRow = nil
    this.m_ChargeGiftButton.OnClick = nil
    this.m_VipPrivilegeButton.OnClick = nil
    this.m_ChargeGuideButton.OnClick = nil
    this.m_ExChangeQnPointButton.OnClick = nil
    CChargeMgr.EnableBuyMonthCardForFriend = true
    EventManager.RemoveListener(EnumEventType.ItemPropUpdated, MakeDelegateFromCSFunction(this.OnChargePropUpdated, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnUpdateMoney, Action0, this))
end
CChargeWnd.m_Init_CS2LuaHook = function (this)
    CUIManager.CloseUI(CUIResources.ShopMallFashionPreviewWnd)
    if LuaWelfareMgr.IsOpenBigMonthCard then
        this.m_ChargeTable.gameObject:SetActive(false)
        this.transform:Find("NewChargeInfo").gameObject:SetActive(true)
    end
    if CClientMainPlayer.Inst ~= nil then
        this.m_ExChangeQnPointButton.Visible = (CClientMainPlayer.Inst.QnPoint > 0)
        local bindLingYu = CClientMainPlayer.Inst.BindJade
        this.m_BindLingYuCtrl.gameObject:SetActive(bindLingYu > 0)
    else
        this.m_BindLingYuCtrl.gameObject:SetActive(false)
    end
    this.m_BindLingYuTable:Reposition()
    Gac2Gas.RequestUpdateServerTime()
    this:OnChargePropUpdated()
    CLuaChargeWnd:InitGMWeChatBtn(this)
end
CChargeWnd.m_ItemAt_CS2LuaHook = function (this, view, row)
    local data = Charge_Charge.GetData(this.m_ChargeItemIds[row])
    if data ~= nil and data.IsMonthCard > 0 then
        local item = TypeAs(view:GetFromPool(data.ID == LuaWelfareMgr.BigMonthCardId and 2 or 0), typeof(CChargeItem))
        item:UpdateView(this.m_ChargeItemIds[row])
        return item
    else
        local item = TypeAs(view:GetFromPool(1), typeof(CChargeItem))
        item:UpdateView(this.m_ChargeItemIds[row])
        return item
    end
end
CChargeWnd.m_OnChargePropUpdated_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil then
        this.m_ExChangeQnPointButton.Visible = (CClientMainPlayer.Inst.QnPoint > 0)
    end
    this:UpdateVipInfo()
    if LuaWelfareMgr.IsOpenBigMonthCard then
        local tableView = this.transform:Find("NewChargeInfo"):GetComponent(typeof(QnTableView))
        local grid01 = this.transform:Find("NewChargeInfo/ScrollView/Grid01"):GetComponent(typeof(UIGrid))
        local grid02 = this.transform:Find("NewChargeInfo/ScrollView/Grid02"):GetComponent(typeof(UIGrid))
        Extensions.RemoveAllChildren(grid01.transform)
        Extensions.RemoveAllChildren(grid02.transform)
        local bigMonthCardItem 
        for i = 0, this.m_ChargeItemIds.Count - 1 do 
            local item = this:ItemAt(tableView, i)
            if this.m_ChargeItemIds[i] == LuaWelfareMgr.BigMonthCardId then
                bigMonthCardItem = item
            end
            if i < 4 then 
                item.transform.parent = grid01.transform
            else
                item.transform.parent = grid02.transform
            end
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                this:OnSelectChargeItem(i)
            end)
        end
        if bigMonthCardItem then -- 大月卡的位置需要特殊处理
            bigMonthCardItem.transform:SetAsFirstSibling()
        end
        grid01:Reposition()
        grid02:Reposition()
    else
        this.m_ChargeTable:ReloadData(true, false)
    end
    this.m_BindLingYuTable:Reposition()
    --更新限购信息
    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.ELingyuMallLimit)
    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.EYuanBaoMall)
end
CChargeWnd.m_HasVipGiftAfterLevel_CS2LuaHook = function (level)
    if CClientMainPlayer.Inst ~= nil then
        local vipInfo = CChargeWnd.VipInfo
        if vipInfo ~= nil then
            do
                local i = level + 1
                while i <= vipInfo.Level do
                    if not CommonDefs.DictContains(vipInfo.Level2GiftReceivedTime, typeof(UInt16), i) then
                        return true
                    end
                    i = i + 1
                end
            end
        end
    end
    return false
end
CChargeWnd.m_MonthCardLeftDay_CS2LuaHook = function (info)

    local buytime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.BuyTime)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local beginOfTheBuyDay = CreateFromClass(DateTime, buytime.Year, buytime.Month, buytime.Day, 0, 0, 0)
    local nowOfTheDay = CreateFromClass(DateTime, now.Year, now.Month, now.Day, 0, 0, 0)
    return beginOfTheBuyDay:AddDays(info.TotalCount):Subtract(nowOfTheDay).Days
end
CChargeWnd.m_HuahunMonthCardLeftDay_CS2LuaHook = function (cardInfo)
    local beginTime = CServerTimeMgr.ConvertTimeStampToZone8Time(cardInfo.ZhuanZhiTime)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local beginOfTheBuyDay = CreateFromClass(DateTime, beginTime.Year, beginTime.Month, beginTime.Day, 0, 0, 0)
    local nowOfTheDay = CreateFromClass(DateTime, now.Year, now.Month, now.Day, 0, 0, 0)
    return beginOfTheBuyDay:AddDays(cardInfo.TotalCount):Subtract(nowOfTheDay).Days
end

CLuaChargeWnd = {}
function CLuaChargeWnd:InitGMWeChatBtn(view)
    local wechatBtn = view.transform:Find("RefreshArea/GMWeChatBtn")
    wechatBtn.gameObject:SetActive(false)

    if CommonDefs.IS_VN_CLIENT then
        --VN服没有这个按钮
        return
    end
    
    local vipLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or -1
    wechatBtn.gameObject:SetActive(LuaJingLingMgr:CanOpenWeChatWelFare() and vipLv <= 14 and vipLv >= 1)
    UIEventListener.Get(wechatBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaChargeWnd:OnClickGMWeChatBtn()
    end) 
end

function CLuaChargeWnd:OnClickGMWeChatBtn()
    if LuaJingLingMgr.m_WeChatjinglingKey and LuaJingLingMgr.m_WeChatjinglingKey[1] then
        CJingLingMgr.Inst:ShowJingLingWnd(LuaJingLingMgr.m_WeChatjinglingKey[1], "o_push", true, false, nil, false)
    else
        local key = Welfare_Setting.GetData().ShoppingMallWeChatJingLingDefaultKey
        if key then
            CJingLingMgr.Inst:ShowJingLingWnd(key, "o_push", true, false, nil, false)
        end
    end
end