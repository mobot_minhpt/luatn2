local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaCakeCuttingMgr = {}
LuaCakeCuttingMgr.PlayerInfo = {}
LuaCakeCuttingMgr.MapHeight = 0
LuaCakeCuttingMgr.MapWidth = 0
LuaCakeCuttingMgr.MapInitData = nil
LuaCakeCuttingMgr.PlayerEndTime = nil
LuaCakeCuttingMgr.SkillNextTime = nil
LuaCakeCuttingMgr.PlayerRankTable = nil
LuaCakeCuttingMgr.MapDiffTable = {}
LuaCakeCuttingMgr.PlayerRankTable = {}

function LuaCakeCuttingMgr.InitGame()
  if not CUIManager.IsLoaded(CLuaUIResources.CakeCuttingWnd) then
    CUIManager.ShowUI(CLuaUIResources.CakeCuttingWnd)
  end
end

function Gas2Gac.CakeCuttingSyncStatusToPlayersInPlay(playStatus, enterStatusTime, extraData)
end

function Gas2Gac.CakeCuttingClearPath(playerId) -- clear path
  g_ScriptEvent:BroadcastInLua('CakeCuttingClearPath',playerId)
end

function Gas2Gac.CakeCuttingSyncDiff(diff_U) -- set color
  local diffData = MsgPackImpl.unpack(diff_U)
  if not LuaCakeCuttingMgr.MapDiffTable then
    LuaCakeCuttingMgr.MapDiffTable = {}
  end
  table.insert(LuaCakeCuttingMgr.MapDiffTable, diffData)
  g_ScriptEvent:BroadcastInLua('CakeCuttingSyncDiff')
end

function Gas2Gac.CakeCuttingPlayClearColor(colorIdx) --clear color
  g_ScriptEvent:BroadcastInLua('CakeCuttingClearColor',colorIdx)
end

function Gas2Gac.CakeCuttingPlaySyncMap(map_U, playEndTime) --map init
  local data = CCakeCuttingPlayMap()
  data:LoadFromString(map_U)
  LuaCakeCuttingMgr.MapHeight = data.Height
  LuaCakeCuttingMgr.MapWidth = data.Width
  LuaCakeCuttingMgr.MapInitData = data.Grid
  LuaCakeCuttingMgr.PlayerEndTime = playEndTime
  LuaCakeCuttingMgr.InitGame()
  LuaCakeCuttingMgr.MapDiffTable = {}
end

function Gas2Gac.CakeCuttingPathAddNode(playerId, x, y) -- add path node
  g_ScriptEvent:BroadcastInLua('CakeCuttingAddPathNode',{playerId = playerId,x = x,y= y})
end

function Gas2Gac.CakeCuttingSyncPlayerInfo(playerId, playerInfo_U)
  local playerInfo = MsgPackImpl.unpack(playerInfo_U)
  local playerInfoTable = {}
  playerInfoTable.PlayerId = tonumber(playerInfo.PlayerId)
  playerInfoTable.Name = playerInfo.Name
  playerInfoTable.Class = tonumber(playerInfo.Class)
  playerInfoTable.Gender = tonumber(playerInfo.Gender)
  playerInfoTable.ColorIdx = tonumber(playerInfo.ColorIdx)

  if CommonDefs.DictContains(playerInfo, typeof(String), 'Path') then
    playerInfoTable.Path = playerInfo.Path
  end
  playerInfoTable.KillNum = tonumber(playerInfo.KillNum)
  LuaCakeCuttingMgr.PlayerInfo[playerId] = playerInfoTable
  g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowInfo')
end

function Gas2Gac.CakeCuttingCheckInMatchingResult(isInMatching)
  if isInMatching then
    g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowBtn',false)
  else
    g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowBtn',true)
  end
end

function Gas2Gac.CakeCuttingSignUpPlayResult(success)
  if success then
    g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowBtn',false)
  else
    g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowBtn',true)
  end
end

function Gas2Gac.CakeCuttingCancelSignUpResult(success)
  if not success then
    g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowBtn',false)
  else
    g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowBtn',true)
  end
end

function Gas2Gac.CakeCuttingPlayKilledByPlayer(playerId, killedByPlayerId, continueKillNum)
    if playerId == CClientMainPlayer.Inst.Id then
      --g_ScriptEvent:BroadcastInLua('CakeCuttingUpdateShowInfo')
      CUIManager.ShowUI(CLuaUIResources.CakeCuttingDieWnd)
    end
    if playerId ~= killedByPlayerId then
      if LuaCakeCuttingMgr.PlayerInfo[killedByPlayerId] then
        LuaCakeCuttingMgr.PlayerInfo[killedByPlayerId].KillNum = LuaCakeCuttingMgr.PlayerInfo[killedByPlayerId].KillNum + 1
      end
      if killedByPlayerId == CClientMainPlayer.Inst.Id then
        if continueKillNum >= 2 then
          LuaCakeCuttingMgr.SelfComboNum = continueKillNum
          CUIManager.ShowUI(CLuaUIResources.CakeCuttingComboWnd)
        end
      end
    end
    if not LuaCakeCuttingMgr.KillInfoTable then
      LuaCakeCuttingMgr.KillInfoTable = {}
    end
    table.insert(LuaCakeCuttingMgr.KillInfoTable,{playerId = playerId,killedByPlayerId = killedByPlayerId,continueKillNum = continueKillNum})

end

function Gas2Gac.CakeCuttingPlayRestartMoving()
  CUIManager.CloseUI(CLuaUIResources.CakeCuttingDieWnd)
end

function Gas2Gac.CakeCuttingPlaySpeedUpResult(CDTime)
  LuaCakeCuttingMgr.SkillNextTime = CDTime
  --g_ScriptEvent:BroadcastInLua('CakeCuttingSkillCDTime',CDTime)
end

------------------caipiao
function Gas2Gac.ZNQLotteryAddTicketResult(success, lotteryId)
  g_ScriptEvent:BroadcastInLua('LotteryAddSucc')
end

function Gas2Gac.ZNQLotterySetLotteryDataResult(success, lotteryId, lotteryData_U)

end

function Gas2Gac.ZNQLotteryQueryLotteryResult(success, lotteryInfos_U, lotteryResult_U, sumOfReward, sumOfCanReward)
  g_ScriptEvent:BroadcastInLua('LotteryHisInfoBack',{his = MsgPackImpl.unpack(lotteryInfos_U), result = MsgPackImpl.unpack(lotteryResult_U), reward = sumOfReward})
end

function Gas2Gac.ZNQLotteryQueryPrizePoolResult(prizePool_U)
	g_ScriptEvent:BroadcastInLua("LotteryTotalRewardInfoBack", MsgPackImpl.unpack(prizePool_U))
end

---------------yuyue
LuaTeamAppointMgr = {}
LuaTeamAppointMgr.teamPlayerTable = nil
LuaTeamAppointMgr.rewardTable = nil

function Gas2Gac.TeamAppointmentPlayerQueryRewardResult(retPlayerList_U, retRewardList_U)
  local playerList = MsgPackImpl.unpack(retPlayerList_U)
  local rewardList = MsgPackImpl.unpack(retRewardList_U)
  LuaTeamAppointMgr.teamPlayerTable = {}
  for i=0,playerList.Count - 1 do
    local playerData = playerList[i]
    local _table = {Id = tonumber(playerData.Id),
    Name = playerData.Name,
    Level = tonumber(playerData.Level),
    Gender = tonumber(playerData.Gender),
    Class = tonumber(playerData.Class)}
    table.insert(LuaTeamAppointMgr.teamPlayerTable, _table)
  end
  LuaTeamAppointMgr.rewardTable = {}
  for i=0,rewardList.Count - 1 do
    local rewardData = rewardList[i]
    local _table = {rewardId = tonumber(rewardData[0]),canReward = tonumber(rewardData[1])} -- canReward: 1 cannotReward, 2 canReward, 3 rewarded
    table.insert(LuaTeamAppointMgr.rewardTable,_table)
  end
  table.sort(LuaTeamAppointMgr.rewardTable,function(a,b) return a.rewardId < b.rewardId end)

  if CUIManager.IsLoaded(CLuaUIResources.NewSeverInviteWnd) then
    g_ScriptEvent:BroadcastInLua("TeamAppointmentPlayerQueryRewardResult")
  else
    CUIManager.ShowUI(CLuaUIResources.NewSeverInviteWnd)
  end
end

function Gas2Gac.TeamAppointmentPlayerGetRewardResult(rewardId, success)
	--g_ScriptEvent:BroadcastInLua("TeamAppointmentPlayerGetRewardResult", {rewardId = rewardId,success = success})
  Gac2Gas.TeamAppointmentPlayerQueryReward()
end

function Gas2Gac.TeamAppointmentCheckHasRewardResult(hasReward)
	g_ScriptEvent:BroadcastInLua("TeamAppointmentCheckHasRewardResult", hasReward)
end

function Gas2Gac.CakeCuttingPlayEnd(playerInfos_U)
  local playerListData = MsgPackImpl.unpack(playerInfos_U)
  local count = playerListData.Count
  local dtable = {}
  for i=0,count-1 do
    local data = playerListData[i]
    local tt = {}
    tt.PlayerId = tonumber(data.PlayerId)
    tt.Name = data.Name
    tt.Class = tonumber(data.Class)
    tt.Gender = tonumber(data.Gender)
    tt.ColorIdx = tonumber(data.ColorIdx)
    tt.KillNum = tonumber(data.KillNum)
    tt.AreaRate = tonumber(data.AreaRate)
    tt.Area = tonumber(data.Area)
    tt.Rank = tonumber(data.Rank)
    table.insert(dtable,tt)
  end

  table.sort(dtable,function(a,b) return a.Rank < b.Rank end)
  LuaCakeCuttingMgr.PlayerFightInfoTable = dtable
	g_ScriptEvent:BroadcastInLua("CakeCuttingEnd")
  CUIManager.ShowUI(CLuaUIResources.CakeCuttingFightInfoWnd)
end

function Gas2Gac.CakeCuttingQueryRankResult(rankInfo_U,selfInfo_U)
  local rankListData = MsgPackImpl.unpack(rankInfo_U)

  local count = rankListData.Count
  local dtable = {}
  for i=0,count-1 do
    local data = rankListData[i]
    local tt = {}
    tt.PlayerId = tonumber(data.PlayerId)
    tt.Name = data.Name
    tt.Class = tonumber(data.Class)
    tt.Gender = tonumber(data.Gender)
    tt.ColorIdx = tonumber(data.ColorIdx)
    tt.KillNum = tonumber(data.KillNum)
    tt.AreaRate = tonumber(data.AreaRate)
    tt.Area = tonumber(data.Area)
    tt.Rank = tonumber(data.Rank)
    table.insert(dtable,tt)
  end

  table.sort(dtable,function(a,b) return a.Rank < b.Rank end)
  LuaCakeCuttingMgr.PlayerServerRankTable = dtable

  local selfInfo = MsgPackImpl.unpack(selfInfo_U)
  local tt = {}
  if CommonDefs.DictContains(selfInfo, typeof(String), 'IsEmpty') then
    tt.PlayerId = CClientMainPlayer.Inst.Id
    tt.Name = CClientMainPlayer.Inst.Name
    tt.Class = EnumToInt(CClientMainPlayer.Inst.Class)
    tt.Gender = EnumToInt(CClientMainPlayer.Inst.Gender)
    tt.AreaRate = 0
    tt.Area = 0
    tt.Rank = 0
  else
    tt.PlayerId = tonumber(selfInfo.PlayerId)
    tt.Name = selfInfo.Name
    tt.Class = tonumber(selfInfo.Class)
    tt.Gender = tonumber(selfInfo.Gender)
    tt.ColorIdx = tonumber(selfInfo.ColorIdx)
    tt.KillNum = tonumber(selfInfo.KillNum)
    tt.AreaRate = tonumber(selfInfo.AreaRate)
    tt.Area = tonumber(selfInfo.Area)
    tt.Rank = tonumber(selfInfo.Rank)
  end
  LuaCakeCuttingMgr.PlayerServerRankSelfInfo = tt

  CUIManager.ShowUI(CLuaUIResources.CakeCuttingRankWnd)
end
