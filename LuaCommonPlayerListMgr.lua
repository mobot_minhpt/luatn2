local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local CIMMgr = import "L10.Game.CIMMgr"

LuaCommonPlayerListMgr={}

LuaCommonPlayerListMgr.Title = ""
LuaCommonPlayerListMgr.BtnData = nil

--[[
    @desc: 显示可以带好友，宗派，帮会的邀请界面
    author:Codee
    time:2021-05-31 14:31:19
    --@title:
	--@btndata:btndata={
        Pbtn = {Name,Func},
        Fbtn = {Name,Func},
        Zbtn = {Name,Func},
        Bbtn = {Name,Func},
    }
    Pbtn数据是必要数据
    Fbtn/Zbtn/Bbtn需要保证至少一个不为nil，为nil时，对应的tab将会隐藏。如果Func为nil，则按钮隐藏
]]
function LuaCommonPlayerListMgr.ShowWnd2(title,btndata)
    LuaCommonPlayerListMgr.Title = title
    LuaCommonPlayerListMgr.BtnData = btndata
    CUIManager.ShowUI(CLuaUIResources.CommonPlayerListWnd2)
end

function LuaCommonPlayerListMgr:ShowPlayersForShiTuWuZiQi(onlinePlayerTbl, isShiTuTask, forWatcher)
    --{Id, masterPlayer.m_Name, masterPlayer.m_Class, masterPlayer.m_Gender, masterPlayer.m_Grade}
    local inst = CCommonPlayerListMgr.Inst
    inst.WndTitle = forWatcher and LocalString.GetString("邀请观众") or LocalString.GetString("邀请对手")
    inst.ButtonText = LocalString.GetString("邀请")
    inst.UpdateType = EnumCommonPlayerListUpdateType.Default
    CommonDefs.ListClear(inst.allData)

    for __, playerInfo in pairs(onlinePlayerTbl) do
        local data = CreateFromClass(CCommonPlayerDisplayData, playerInfo.id, playerInfo.name, playerInfo.grade, playerInfo.class, playerInfo.gender, -1, true)
		CommonDefs.ListAdd(inst.allData, typeof(CCommonPlayerDisplayData), data)
    end


	CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(function (playerId)
        if forWatcher then
            LuaShiTuMgr:InvitePlayerWatchWuZiQi(playerId)
        else
            local message = isShiTuTask and "ShiTu_WuZiQi_Invite_Player_Confirm" or "ShiTu_WuZiQi_Invite_Normal_Player_Confirm"
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage(message), function ()
                LuaShiTuMgr:InvitePlayerJoinWuZiQi(playerId)
            end, nil, nil, nil, false)
        end
    end)
	CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function LuaCommonPlayerListMgr:ShowOnlineFriends(title, buttonText, onSelectFunc)
    CCommonPlayerListMgr.Inst:ShowOnlineFriends(title, buttonText, DelegateFactory.Action_ulong(function(id)
        if onSelectFunc then onSelectFunc(id) end
    end))                                                                                                 
end

--双向好友
function LuaCommonPlayerListMgr:ShowOnlineFriendsWithCheck(title, buttonText, onSelectFunc, noPlayerMsgName, checkValidFunc)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end

    local inst = CCommonPlayerListMgr.Inst
    inst.WndTitle = title
    inst.ButtonText = buttonText
    inst.UpdateType = EnumCommonPlayerListUpdateType.Default
    CommonDefs.ListClear(inst.allData)
    CommonDefs.DictIterate(mainplayer.RelationshipProp.Friends, DelegateFactory.Action_object_object(function (playerId, data)
        local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
        if basicInfo and CIMMgr.Inst:IsOnline(playerId) then
            if checkValidFunc==nil or checkValidFunc(playerId) then
                local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
                CommonDefs.ListAdd(inst.allData, typeof(CCommonPlayerDisplayData), data)
            end
        end
    end))

    if (inst.allData.Count == 0) then
        g_MessageMgr:ShowMessage(noPlayerMsgName)
        return
    end
    inst.OnPlayerSelected = DelegateFactory.Action_ulong(function (playerId)
        if onSelectFunc then onSelectFunc(playerId) end
        CUIManager.CloseUI(CIndirectUIResources.CommonPlayerListWnd)
    end)

    CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd);                                                                                              
end