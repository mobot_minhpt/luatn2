require("common/common_include")

local CPortraitSocialWndMgr = import "L10.UI.CPortraitSocialWndMgr"
local QianKunDai_Divine = import "L10.Game.QianKunDai_Divine"
local UITexture = import "UITexture"
local Color = import "UnityEngine.Color"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CUITexture = import "L10.UI.CUITexture"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QianKunDai_Setting = import "L10.Game.QianKunDai_Setting"
local MessageMgr = import "L10.Game.MessageMgr"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Random = import "UnityEngine.Random"
local NGUIText = import "NGUIText"
local LuaTweenUtils = import "LuaTweenUtils"
local Vector3 = import "UnityEngine.Vector3"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local NativeTools = import "L10.Engine.NativeTools"

LuaBaGuaLuWnd = class()

RegistClassMember(LuaBaGuaLuWnd, "BaGuaArea")
RegistClassMember(LuaBaGuaLuWnd, "CountDownLabel")
RegistClassMember(LuaBaGuaLuWnd, "MoreInfo")
RegistClassMember(LuaBaGuaLuWnd, "ShowSocialButton")
RegistClassMember(LuaBaGuaLuWnd, "HintLabel")

RegistClassMember(LuaBaGuaLuWnd, "OwnLingQi")
RegistClassMember(LuaBaGuaLuWnd, "AddLingQiBtn")

--金币
RegistClassMember(LuaBaGuaLuWnd, "JinBi1")
RegistClassMember(LuaBaGuaLuWnd, "JinBi2")
RegistClassMember(LuaBaGuaLuWnd, "JinBi3")
RegistClassMember(LuaBaGuaLuWnd, "JinBi1FX")
RegistClassMember(LuaBaGuaLuWnd, "JinBi2FX")
RegistClassMember(LuaBaGuaLuWnd, "JinBi3FX")
RegistClassMember(LuaBaGuaLuWnd, "JinBi1OriginPos")
RegistClassMember(LuaBaGuaLuWnd, "JinBi2OriginPos")
RegistClassMember(LuaBaGuaLuWnd, "JinBi3OriginPos")

RegistChildComponent(LuaBaGuaLuWnd, "ShangGuaResult", GameObject)
RegistChildComponent(LuaBaGuaLuWnd, "XiaGuaResult", GameObject)

RegistClassMember(LuaBaGuaLuWnd, "SelectDivineIndex")

RegistClassMember(LuaBaGuaLuWnd, "DivineItems")
RegistClassMember(LuaBaGuaLuWnd, "CurrentTime")
RegistClassMember(LuaBaGuaLuWnd, "TotalTime")
RegistClassMember(LuaBaGuaLuWnd, "ShowWindows")
RegistClassMember(LuaBaGuaLuWnd, "IsShowing")

RegistClassMember(LuaBaGuaLuWnd, "CountDownTick")
RegistClassMember(LuaBaGuaLuWnd, "ChouJiangTick")

RegistClassMember(LuaBaGuaLuWnd, "ShowResultTicks")

function LuaBaGuaLuWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBaGuaLuWnd:InitClassMembers()

	self.BaGuaArea = self.transform:Find("BaGuaArea").gameObject
	self.CountDownLabel = self.transform:Find("BaGuaArea/CountDownLabel"):GetComponent(typeof(UILabel))
	self.MoreInfo = self.transform:Find("BG/MoreInfo").gameObject
	self.HintLabel = self.transform:Find("BaGuaArea/HintLabel"):GetComponent(typeof(UILabel))
	self.ShowSocialButton = self.transform:Find("SocialArea/ShowSocialButton").gameObject

	self.OwnLingQi = self.transform:Find("BaGuaArea/MyLingQi/OwnLingQi"):GetComponent(typeof(UILabel))
	self.AddLingQiBtn = self.transform:Find("BaGuaArea/MyLingQi/OwnLingQi/AddLingQiBtn").gameObject

	self.JinBi1 = self.transform:Find("BaGuaArea/JinBis/JinBi1"):GetComponent(typeof(CUITexture))
	self.JinBi2 = self.transform:Find("BaGuaArea/JinBis/JinBi2"):GetComponent(typeof(CUITexture))
	self.JinBi3 = self.transform:Find("BaGuaArea/JinBis/JinBi3"):GetComponent(typeof(CUITexture))

	self.JinBi1FX = self.transform:Find("BaGuaArea/JinBis/JinBi1FX"):GetComponent(typeof(CUIFx))
	self.JinBi1FX:DestroyFx()
	self.JinBi2FX = self.transform:Find("BaGuaArea/JinBis/JinBi2FX"):GetComponent(typeof(CUIFx))
	self.JinBi2FX:DestroyFx()
	self.JinBi3FX = self.transform:Find("BaGuaArea/JinBis/JinBi3FX"):GetComponent(typeof(CUIFx))
	self.JinBi3FX:DestroyFx()

	self.JinBi1OriginPos = self.JinBi1.transform.localPosition
	self.JinBi2OriginPos = self.JinBi2.transform.localPosition
	self.JinBi3OriginPos = self.JinBi3.transform.localPosition

	self.DivineItems = {}
	self.ShowResultTicks = {}
	self.ShowWindows = CreateFromClass(MakeGenericClass(List, cs_string))
	self.IsShowing = true
	self:HideShangXiaResult()

	for i = 1, 8 do
		local go = self.transform:Find(SafeStringFormat3("BaGuaArea/Divines/%d", i)).gameObject
		table.insert(self.DivineItems, go)
		self:InitDivineItem(go, i)
	end

	-- 打开聊天界面
	if CPortraitSocialWndMgr.Inst:IsPortraitSocialWndOpened() then
		CPortraitSocialWndMgr.Inst:ClosePortraitSocialWnd()
	end

	-- pc版本
	local bIsWinSocialWndOpened = false
	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
	end

	if not bIsWinSocialWndOpened then
		CUIManager.ShowUI(CUIResources.SocialWnd)
	else
		-- pc版本打开了外置聊天
		self.ShowSocialButton:SetActive(false)
		if self.BaGuaArea then
			LuaUtils.SetLocalPositionX(self.BaGuaArea.transform, -534)
		end
	end
	
end

function LuaBaGuaLuWnd:InitValues()
	
	self.SelectDivineIndex = 0
	self.TotalTime = 0
	self.CurrentTime = 0

	self.JinBi1.gameObject:SetActive(false)
	self.JinBi2.gameObject:SetActive(false)
	self.JinBi3.gameObject:SetActive(false)

	if self.CountDownTick ~= nil then
		UnRegisterTick(self.CountDownTick)
      	self.CountDownTick = nil
	end

	local onAddLingQiBtnClicked = function (go)
		self:OnAddLingQiBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.AddLingQiBtn, DelegateFactory.Action_GameObject(onAddLingQiBtnClicked), false)

	local onMoreInfoClicked = function (go)
		self:OnMoreInfoClicked()
	end
	CommonDefs.AddOnClickListener(self.MoreInfo, DelegateFactory.Action_GameObject(onMoreInfoClicked), false)

	local onShowSocialButtonClicked = function (go)
		self:OnShowSocialButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ShowSocialButton, DelegateFactory.Action_GameObject(onShowSocialButtonClicked), false)

	self:UpdateLingQi()
	self:UpdateBaGuaLuPlayStatus()
end


function LuaBaGuaLuWnd:InitDivineItem(go, index)
	local BG = go.transform:Find("BG").gameObject
	local SelectedBG = go.transform:Find("BG/Selected").gameObject
	SelectedBG:SetActive(false)
end

function LuaBaGuaLuWnd:OnDivineItemClicked(go, index)
	self.SelectDivineIndex = index

	for i = 1, #self.DivineItems do
		self:SetDivineItemSelected(self.DivineItems[i], self.DivineItems[i] == go)
	end
	LuaBaGuaLuMgr.ZhuRuSelectedDivine(index)
end

function LuaBaGuaLuWnd:EnableDivine(go, index)
	local BG = go.transform:Find("BG").gameObject
	local onDivineItemClicked = function (gameObject)
		self:OnDivineItemClicked(go, index)
	end
	CommonDefs.AddOnClickListener(BG, DelegateFactory.Action_GameObject(onDivineItemClicked), false)
end

function LuaBaGuaLuWnd:UnableDivine(go, index)
	local BG = go.transform:Find("BG").gameObject
	local onDivineItemClicked = function (gameObject)
		
	end
	CommonDefs.AddOnClickListener(BG, DelegateFactory.Action_GameObject(onDivineItemClicked), false)
end

function LuaBaGuaLuWnd:UpdateLingQi()
	local myLingQi = 0

	if CClientMainPlayer.Inst then
		myLingQi = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.QianKunDai)
	end
	self.OwnLingQi.text = tostring(myLingQi)

end

-- 更新玩家注入后的主客卦信息
function LuaBaGuaLuWnd:UpdateZhuRuDivineTexture()
	for i = 1, #self.DivineItems do
		local isZhuGuaZhuRu = self:GetIsZhuGuaZhuRu(i)
		local isKeGuaZhuRu = self:GetIsKeGuaZhuRu(i)
		self:UpdateDivineTexture(self.DivineItems[i], isZhuGuaZhuRu, isKeGuaZhuRu)
	end
	self:UpdateLingQi()
end

function LuaBaGuaLuWnd:UpdateDivineTexture(go, isZhuGuaZhuRu, isKeGuaZhuRu)
	local left = go.transform:Find("BG/Left"):GetComponent(typeof(UITexture))
	local right = go.transform:Find("BG/Right"):GetComponent(typeof(UITexture))

	local redColor = NGUIText.ParseColor24("FF1212", 0)
	local blueColor = NGUIText.ParseColor24("00B5FF", 0)

	if isZhuGuaZhuRu and not isKeGuaZhuRu then
		left.color = redColor
		left.alpha = 1
		right.color = redColor
		right.alpha = 1
	elseif isZhuGuaZhuRu and isKeGuaZhuRu then
		left.color = redColor
		left.alpha = 1
		right.color = blueColor
		right.alpha = 1
	elseif not isZhuGuaZhuRu and isKeGuaZhuRu then
		left.color = blueColor
		left.alpha = 1
		right.color = blueColor
		right.alpha = 1
	elseif not isZhuGuaZhuRu and not isKeGuaZhuRu then
		left.color = Color.white
		left.alpha = 0.7
		right.color = Color.white
		right.alpha = 0.7
	end
end

function LuaBaGuaLuWnd:GetIsZhuGuaZhuRu(index)
	local isZhuGuaZhuRu = false
	if LuaBaGuaLuMgr.m_ZhuGuaPosList then
		for i = 1, #LuaBaGuaLuMgr.m_ZhuGuaPosList do
			if LuaBaGuaLuMgr.m_ZhuGuaPosList[i] == index then
				isZhuGuaZhuRu = true
			end
		end
	end
	return isZhuGuaZhuRu
end

function LuaBaGuaLuWnd:GetIsKeGuaZhuRu(index)
	local isKeGuaZhuRu = false
	if LuaBaGuaLuMgr.m_KeGuaPosList then
		for i = 1, #LuaBaGuaLuMgr.m_KeGuaPosList do
			if LuaBaGuaLuMgr.m_KeGuaPosList[i] == index then
				isKeGuaZhuRu = true
			end
		end
	end
	return isKeGuaZhuRu
end

function LuaBaGuaLuWnd:SetDivineItemSelected(go, selected)
	local SelectedBG = go.transform:Find("BG/Selected").gameObject
	SelectedBG:SetActive(selected)
end

function LuaBaGuaLuWnd:OnAddLingQiBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.BaGuaLuLingQiGetWnd)
end

function LuaBaGuaLuWnd:OnMoreInfoClicked(go)
	MessageMgr.Inst:ShowMessage("BAGUALU_MORE_INFO", {})
end

function LuaBaGuaLuWnd:OnShowSocialButtonClicked(go)
	if NativeTools.IsMuMuDevice() then
		local CWinSocialWndMgr=import "L10.UI.CWinSocialWndMgr"
		if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
			CWinSocialWndMgr.Inst:OpenOrCloseWinSocialWnd()
		end
	end
	CUIManager.ShowUI(CUIResources.SocialWnd)
end

function LuaBaGuaLuWnd:UpdateSelfLingQi(itemId, score)
	self:UpdateLingQi()
end

function LuaBaGuaLuWnd:UpdateBaGuaLuPlayStatus()
	if LuaBaGuaLuMgr.m_BaGuaLuStatus == EnumBaGuaLuStatus.eZhuRu then

		self:UpdateBaGuaLuZhuRuStatus()

	elseif LuaBaGuaLuMgr.m_BaGuaLuStatus == EnumBaGuaLuStatus.eChouJiang then

		self:UpdateBaGuaLuChouJiangStatus()

	elseif LuaBaGuaLuMgr.m_BaGuaLuStatus == EnumBaGuaLuStatus.eFinish then

		self:UpdateBaGuaLuFinishStatus()

	elseif LuaBaGuaLuMgr.m_BaGuaLuStatus == EnumBaGuaLuStatus.eEnd then

		self:UpdateBaGuaLuEndStatus()
	end
end

-- 八卦炉进入注入状态
-- 清除所有tick，enable注入按钮（设置为白色），注册注入倒数tick
function LuaBaGuaLuWnd:UpdateBaGuaLuZhuRuStatus()
	self.HintLabel.text = g_MessageMgr:FormatMessage("BAGULU_ZHURU_HINT")

	local time = math.abs(CServerTimeMgr.Inst.timeStamp - LuaBaGuaLuMgr.m_BaGuaLuStartTime)
	local setting = QianKunDai_Setting.GetData()
	self.CurrentTime = math.ceil(setting.ZhuRuTime - time)

	self:DestroyCountDownTick()
	self:DestroyChouJiangTick()
	self:DestroyShowResultTick()

	self.JinBi1.gameObject:SetActive(false)
	self.JinBi2.gameObject:SetActive(false)
	self.JinBi3.gameObject:SetActive(false)

	self.JinBi1FX:DestroyFx()
	self.JinBi2FX:DestroyFx()
	self.JinBi3FX:DestroyFx()

	self:HideShangXiaResult()

	for i = 1, #self.DivineItems do
		self:EnableDivine(self.DivineItems[i], i)
		self:UpdateDivineTexture(self.DivineItems[i], false, false)
		self:ClearRodsColor(i)
	end

	local time = math.abs(CServerTimeMgr.Inst.timeStamp - LuaBaGuaLuMgr.m_BaGuaLuStartTime)
	local setting = QianKunDai_Setting.GetData()
	self.CurrentTime = math.ceil(setting.ZhuRuTime - time)
	self.CountDownLabel.text = tostring(self.CurrentTime)

	self.CountDownTick = RegisterTick(function ()
		local time = math.abs(CServerTimeMgr.Inst.timeStamp - LuaBaGuaLuMgr.m_BaGuaLuStartTime)
		local setting = QianKunDai_Setting.GetData()
		self.CurrentTime = math.ceil(setting.ZhuRuTime - time)

		if self.CurrentTime >= 0 then
			self.CountDownLabel.text = tostring(self.CurrentTime)
		else
			self.CountDownLabel.text = ""
			self:DestroyCountDownTick()
		end
	end, 200)
end

-- 八卦炉进入抽奖状态
-- 不显示hint，清除所有tick
function LuaBaGuaLuWnd:UpdateBaGuaLuChouJiangStatus()
	self.HintLabel.text = ""
	self.CountDownLabel.text = ""

	local time = math.abs(CServerTimeMgr.Inst.timeStamp - LuaBaGuaLuMgr.m_BaGuaLuStartTime)
	local setting = QianKunDai_Setting.GetData()
	self.CurrentTime = math.ceil(setting.ChouJiangTime - time)

	self:DestroyCountDownTick()
	self:DestroyChouJiangTick()
	self:HideShangXiaResult()


	-- unable 注入
	for i = 1, #self.DivineItems do
		self:UnableDivine(self.DivineItems[i], i)
	end

end

-- 八卦炉进入一次抽奖结束阶段阶段
-- enable注入供玩家查看，清除tick，注册抽奖倒数tick
function LuaBaGuaLuWnd:UpdateBaGuaLuFinishStatus()
	self.HintLabel.text = ""

	self:DestroyCountDownTick()
	self:DestroyChouJiangTick()
	self:DestroyShowResultTick()

	self.JinBi1FX:DestroyFx()
	self.JinBi2FX:DestroyFx()
	self.JinBi3FX:DestroyFx()


	for i = 1, #self.DivineItems do
		self:EnableDivine(self.DivineItems[i], i)
	end

	local time = math.abs(CServerTimeMgr.Inst.timeStamp - LuaBaGuaLuMgr.m_BaGuaLuStartTime)
	local setting = QianKunDai_Setting.GetData()
	self.CurrentTime = math.ceil(setting.IntervalTime - time)
	self.CountDownLabel.text = tostring(self.CurrentTime)

	self.ChouJiangTick = RegisterTick(function ()
		local time = math.abs(CServerTimeMgr.Inst.timeStamp - LuaBaGuaLuMgr.m_BaGuaLuStartTime)
		local setting = QianKunDai_Setting.GetData()
		self.CurrentTime = math.ceil(setting.IntervalTime - time)

		if self.CurrentTime >= 0 then
			self.CountDownLabel.text = tostring(self.CurrentTime)
		else
			self.CountDownLabel.text = ""
			self:DestroyChouJiangTick()
		end
	end, 200)

end

-- 八卦炉进入结束阶段
-- unable注入，清楚tick，提示
function LuaBaGuaLuWnd:UpdateBaGuaLuEndStatus()
	self.HintLabel.text = LocalString.GetString("六爻八卦已结束")
	self.CountDownLabel.text = ""

	self:DestroyCountDownTick()
	self:DestroyChouJiangTick()
	self:DestroyShowResultTick()

	self.JinBi1FX:DestroyFx()
	self.JinBi2FX:DestroyFx()
	self.JinBi3FX:DestroyFx()
	self:HideShangXiaResult()

	-- unable 注入
	for i = 1, #self.DivineItems do
		self:UnableDivine(self.DivineItems[i], i)
	end

end

function LuaBaGuaLuWnd:DestroyCountDownTick()
	if self.CountDownTick ~= nil then
      	UnRegisterTick(self.CountDownTick)
      	self.CountDownTick = nil
    end
end

function LuaBaGuaLuWnd:DestroyChouJiangTick()
	if self.ChouJiangTick ~= nil then
      	UnRegisterTick(self.ChouJiangTick)
      	self.ChouJiangTick = nil
    end
end

function LuaBaGuaLuWnd:DestroyShowResultTick()
	if self.ShowResultTicks then
		for i = 1, #self.ShowResultTicks do
			UnRegisterTick(self.ShowResultTicks[i])
		end
	end
	self.ShowResultTicks = {}
end

function LuaBaGuaLuWnd:BaGuaLuFinishChouJiang(zhuGuaPos, keGuaPos)

	local zhuGua = QianKunDai_Divine.GetData(zhuGuaPos)
	local keGua = QianKunDai_Divine.GetData(keGuaPos)

	local zhuGuaDivineIds = zhuGua.DivineId
	local keGuaDivineIds = keGua.DivineId

	local zhuKeSame = zhuGuaPos == keGuaPos

	self.JinBi1.gameObject:SetActive(false)
	self.JinBi2.gameObject:SetActive(false)
	self.JinBi3.gameObject:SetActive(false)

	-- 开始抽奖 关闭注入界面
	CUIManager.CloseUI(CLuaUIResources.BaGuaLuLingQiZhuRuWnd)
	CUIManager.CloseUI(CLuaUIResources.BaGuaLuJieGuaWnd)

	-- 揭晓客卦
	local interval = 1500

	self:ShowJinBiResult(keGuaDivineIds[2] == 0)
	local resultTick1 = RegisterTickOnce(function ()
		self:ShowResult(3, zhuGuaPos, keGuaPos, true)
	end, 0 + interval)
	table.insert(self.ShowResultTicks, resultTick1)


	local jinbiTick2 = RegisterTickOnce(function ()
		self:ShowJinBiResult(keGuaDivineIds[1] == 0)
	end, 2000)
	table.insert(self.ShowResultTicks, jinbiTick2)
	local resultTick2 = RegisterTickOnce(function ()
		self:ShowResult(2, zhuGuaPos, keGuaPos, true)
	end, 2000 + interval)
	table.insert(self.ShowResultTicks, resultTick2)


	local jinbiTick3 = RegisterTickOnce(function ()
		self:ShowJinBiResult(keGuaDivineIds[0] == 0)
	end, 4000)
	table.insert(self.ShowResultTicks, jinbiTick3)
	local resultTick3 = RegisterTickOnce(function ()
		self:ShowResult(1, zhuGuaPos, keGuaPos, true)
		self:ShowXiaGuaResult(keGuaPos)
	end, 4000 + interval)
	table.insert(self.ShowResultTicks, resultTick3)

	-- 揭晓主卦
	local jinbiTick4 = RegisterTickOnce(function ()
		for i = 1, #self.DivineItems do
			self:ClearRodsColor(i)
		end
		self:ShowJinBiResult(zhuGuaDivineIds[2] == 0)
	end, 6000)
	table.insert(self.ShowResultTicks, jinbiTick4)
	local resultTick4 = RegisterTickOnce(function ()
		self:ShowResult(3, zhuGuaPos, keGuaPos, false)
	end, 6000 + interval)
	table.insert(self.ShowResultTicks, resultTick4)


	local jinbiTick5 = RegisterTickOnce(function ()
		self:ShowJinBiResult(zhuGuaDivineIds[1] == 0)
	end, 8000)
	table.insert(self.ShowResultTicks, jinbiTick5)
	local resultTick5 = RegisterTickOnce(function ()
		self:ShowResult(2, zhuGuaPos, keGuaPos, false)
	end, 8000 + interval)
	table.insert(self.ShowResultTicks, resultTick5)

	local jinbiTick6 = RegisterTickOnce(function ()
		self:ShowJinBiResult(zhuGuaDivineIds[0] == 0)
	end, 10000)
	table.insert(self.ShowResultTicks, jinbiTick6)
	local resultTick6 = RegisterTickOnce(function ()
		self:ShowResult(1, zhuGuaPos, keGuaPos, false)
		self:ShowShangGuaResult(zhuGuaPos)
	end, 10000 + interval)
	table.insert(self.ShowResultTicks, resultTick6)
	
end

-- index 1-3
-- result 0 or 1
-- 确定颜色
-- 展示客/主卦的index个杠
function LuaBaGuaLuWnd:ShowResult(index, zhuGuaPos, keGuaPos, isKeGua)
	local zhuGuaDivine = QianKunDai_Divine.GetData(zhuGuaPos)
	local keGuaDivine = QianKunDai_Divine.GetData(keGuaPos)

	local redColor = NGUIText.ParseColor24("FF1212", 0)
	local blueColor = NGUIText.ParseColor24("00B5FF", 0)

	for i = 1, #self.DivineItems do --遍历8个卦象

		local divineItem = QianKunDai_Divine.GetData(i)
		local fit = true
		
		if isKeGua then
			for j = 2, index - 1, -1 do
				if divineItem.DivineId[j] ~= keGuaDivine.DivineId[j] then
					fit = false
				end
			end
			if fit then
				local rod = self.DivineItems[i].transform:Find(SafeStringFormat3("DivinePic/%d-%d", index, keGuaDivine.DivineId[index-1]))
				if rod then
					local left = rod:Find("Left"):GetComponent(typeof(UITexture))
					local right = rod:Find("Right"):GetComponent(typeof(UITexture))
					left.color =  blueColor
					left.alpha = 1
					right.color = blueColor
					right.alpha = 1
				else
					self:ClearRodsColor(i)
				end
			else
				self:ClearRodsColor(i)
			end
		else
			for j = 2, index - 1, -1 do
				if divineItem.DivineId[j] ~= zhuGuaDivine.DivineId[j] then
					fit = false
				end
			end
			if fit then
				local rod = self.DivineItems[i].transform:Find(SafeStringFormat3("DivinePic/%d-%d", index, zhuGuaDivine.DivineId[index-1]))
				if rod then
					local zhuKeSameAtIndex = zhuGuaDivine.DivineId[index-1] == keGuaDivine.DivineId[index-1]

					local left = rod:Find("Left"):GetComponent(typeof(UITexture))
					local right = rod:Find("Right"):GetComponent(typeof(UITexture))
					left.color =  redColor
					left.alpha = 1
					right.color = redColor
					right.alpha = 1
				else
					self:ClearRodsColor(i, i == keGuaPos)
				end
			else
				self:ClearRodsColor(i, i == keGuaPos)
			end
		end
	end
end


function LuaBaGuaLuWnd:ClearRodsColor(divinePos, isKeGuaPos)
	local divine = QianKunDai_Divine.GetData(divinePos)
	local divineItem = self.DivineItems[divinePos]

	local redColor = NGUIText.ParseColor24("FF1212", 0)
	local blueColor = NGUIText.ParseColor24("00B5FF", 0)

	--[[if isKeGuaPos then
		for i = 0, divine.DivineId.Length-1 do
			local rod = divineItem.transform:Find(SafeStringFormat3("DivinePic/%d-%d", i+1 , divine.DivineId[i]))
			if rod then
				local left = rod:Find("Left"):GetComponent(typeof(UITexture))
				local right = rod:Find("Right"):GetComponent(typeof(UITexture))
				left.color = blueColor
				left.alpha = 1
				right.color = blueColor
				right.alpha = 1
			end
		end
	else--]]
		for i = 0, divine.DivineId.Length-1 do
			local rod = divineItem.transform:Find(SafeStringFormat3("DivinePic/%d-%d", i+1 , divine.DivineId[i]))
			if rod then
				local left = rod:Find("Left"):GetComponent(typeof(UITexture))
				local right = rod:Find("Right"):GetComponent(typeof(UITexture))
				left.color = Color.white
				left.alpha = 0.7
				right.color = Color.white
				right.alpha = 0.7
			end
		end
	--end
	
end

function LuaBaGuaLuWnd:ShowJinBiResult(isRod)
	local interval = 300
	if not isRod then
		
		local time1 = Random.Range(0, interval)
		local jinbiDelay1 = RegisterTickOnce(function ()
			self.JinBi1FX:DestroyFx()
			if self.IsShowing then
				self:RandomPosition(self.JinBi1FX.gameObject, self.JinBi1.transform)
				self.JinBi1FX:LoadFx(CUIFxPaths.BaGuaLuZhengMian)
			end
		end, time1)
		table.insert(self.ShowResultTicks, jinbiDelay1)

		
		local time2 = Random.Range(0, interval)
		local jinbiDelay2 = RegisterTickOnce(function ()
			self.JinBi2FX:DestroyFx()
			if self.IsShowing then
				self:RandomPosition(self.JinBi2FX.gameObject, self.JinBi2.transform)
				self.JinBi2FX:LoadFx(CUIFxPaths.BaGuaLuZhengMian)
			end
		end, time2)
		table.insert(self.ShowResultTicks, jinbiDelay2)

		
		local time3 = Random.Range(0, interval)
		local jinbiDelay3 = RegisterTickOnce(function ()
			self.JinBi3FX:DestroyFx()
			if self.IsShowing then
				self:RandomPosition(self.JinBi3FX.gameObject, self.JinBi3.transform)
				self.JinBi3FX:LoadFx(CUIFxPaths.BaGuaLuZhengMian)
			end
		end, time3)
		table.insert(self.ShowResultTicks, jinbiDelay3)
		
	else

		
		local time1 = Random.Range(0, interval)
		local jinbiDelay1 = RegisterTickOnce(function ()
			self.JinBi1FX:DestroyFx()
			if self.IsShowing then
				self:RandomPosition(self.JinBi1FX.gameObject, self.JinBi1.transform)
				self.JinBi1FX:LoadFx(CUIFxPaths.BaGuaLuFanMian)
			end
		end, time1)
		table.insert(self.ShowResultTicks, jinbiDelay1)

		
		local time2 = Random.Range(0, interval)
		local jinbiDelay2 = RegisterTickOnce(function ()
			self.JinBi2FX:DestroyFx()
			if self.IsShowing then
				self:RandomPosition(self.JinBi2FX.gameObject, self.JinBi2.transform)
				self.JinBi2FX:LoadFx(CUIFxPaths.BaGuaLuFanMian)
			end
		end, time2)
		table.insert(self.ShowResultTicks, jinbiDelay2)

		
		local time3 = Random.Range(0, interval)
		local jinbiDelay3 = RegisterTickOnce(function ()
			self.JinBi3FX:DestroyFx()
			if self.IsShowing then
				self:RandomPosition(self.JinBi3FX.gameObject, self.JinBi3.transform)
				self.JinBi3FX:LoadFx(CUIFxPaths.BaGuaLuFanMian)
			end
		end, time3)
		table.insert(self.ShowResultTicks, jinbiDelay3)

	end
end

function LuaBaGuaLuWnd:RandomPosition(go, tf)
	local anglesRandomZ = Random.Range(-95, -80)
	local anglesRandomX = Random.Range(0, 180)
	local anglesRandomY = Random.Range(88, 92)
	go.transform.localEulerAngles = Vector3(anglesRandomX, anglesRandomY, anglesRandomZ)
	local randomX = Random.Range(-15, 15)
	local randomY = Random.Range(-15, 15)
	local targetPos = Vector3(tf.localPosition.x + randomX, tf.localPosition.y + randomY, 0)
	LuaTweenUtils.TweenPosition(go.transform, tf.localPosition.x + randomX,  tf.localPosition.y + randomY, 0, 1)
end

function LuaBaGuaLuWnd:HideShangXiaResult()
	self.ShangGuaResult:SetActive(false)
	self.XiaGuaResult:SetActive(false)
end

function LuaBaGuaLuWnd:ShowXiaGuaResult(result)
	local divine = QianKunDai_Divine.GetData(result)
	if not divine then return end
	local xiaGuaTexture = self.XiaGuaResult.transform:GetComponent(typeof(CUITexture))
	xiaGuaTexture:LoadMaterial(divine.DivinePic)
	self.XiaGuaResult:SetActive(true)
	LuaTweenUtils.TweenScale(self.XiaGuaResult.transform, Vector3(2, 2, 2), Vector3(1, 1, 1), 1)
end

function LuaBaGuaLuWnd:ShowShangGuaResult(result)
	local divine = QianKunDai_Divine.GetData(result)
	if not divine then return end
	local shangGuaTexture = self.ShangGuaResult.transform:GetComponent(typeof(CUITexture))
	shangGuaTexture:LoadMaterial(divine.DivinePic)
	self.ShangGuaResult:SetActive(true)
	LuaTweenUtils.TweenScale(self.ShangGuaResult.transform, Vector3(2, 2, 2), Vector3(1, 1, 1), 1)
end

function LuaBaGuaLuWnd:OnSocialWndOpenOrClose(args)
	local show = args[0]
	local moduleName = args[1]

	if moduleName == "SocialWnd" then
		if show then
			if self.BaGuaArea then
				LuaUtils.SetLocalPositionX(self.BaGuaArea.transform, -76)
			end
		else
			if self.BaGuaArea then
				LuaUtils.SetLocalPositionX(self.BaGuaArea.transform, -534)
			end
		end
	end
	-- pc版本
	if self.ShowSocialButton then
		local bIsWinSocialWndOpened = false
		if CommonDefs.DEVICE_PLATFORM == "pc" then
			local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
			bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
		end

		if not bIsWinSocialWndOpened then
			self.ShowSocialButton:SetActive(true)
		else
			-- pc版本打开了外置聊天
			self.ShowSocialButton:SetActive(false)
		end
	end
end

function LuaBaGuaLuWnd:OnShowUIPreProcess(args)
	local show = args[0]
	local moduleName = args[1]

	self:OnSocialWndOpenOrClose(args)

	if not self:NeedPreProcess(moduleName) then
		return
	end

	if show and not self.ShowWindows:Contains(moduleName) then
		self:HideJinBiFx()
		self.ShowWindows:Add(moduleName)
	elseif not show and self.ShowWindows:Contains(moduleName) then
		self.ShowWindows:Remove(moduleName)
		if self.ShowWindows.Count < 1 then
			self:ShowJinBiFx()
		end
	end
end

function LuaBaGuaLuWnd:NeedPreProcess(name)
	if name == CLuaUIResources.BaGuaLuWnd.Name or name == CIndirectUIResources.ItemAvailableWnd.Name or name == CUIResources.SocialWnd.Name then 
		return false
	end
	return true
end

function LuaBaGuaLuWnd:HideJinBiFx()
	self.JinBi1FX.gameObject:SetActive(false)
	self.JinBi2FX.gameObject:SetActive(false)
	self.JinBi3FX.gameObject:SetActive(false)
	self.IsShowing = false
end

function LuaBaGuaLuWnd:ShowJinBiFx()
	self.JinBi1FX.gameObject:SetActive(true)
	self.JinBi2FX.gameObject:SetActive(true)
	self.JinBi3FX.gameObject:SetActive(true)
	self.IsShowing = true
end

function LuaBaGuaLuWnd:OnEnable()
	g_ScriptEvent:AddListener("QianKunDaiHeChengSuccess", self, "UpdateSelfLingQi")
	g_ScriptEvent:AddListener("SyncPlayerTotalZhuRuInfo", self, "UpdateZhuRuDivineTexture")
	g_ScriptEvent:AddListener("UpdateBaGuaLuPlayStatus", self, "UpdateBaGuaLuPlayStatus")
	g_ScriptEvent:AddListener("BaGuaLuFinishChouJiang", self, "BaGuaLuFinishChouJiang")
	g_ScriptEvent:AddListener("ShowUIPreDraw", self, "OnShowUIPreProcess")
end

function LuaBaGuaLuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QianKunDaiHeChengSuccess", self, "UpdateSelfLingQi")
	g_ScriptEvent:RemoveListener("SyncPlayerTotalZhuRuInfo", self, "UpdateZhuRuDivineTexture")
	g_ScriptEvent:RemoveListener("UpdateBaGuaLuPlayStatus", self, "UpdateBaGuaLuPlayStatus")
	g_ScriptEvent:RemoveListener("BaGuaLuFinishChouJiang", self, "BaGuaLuFinishChouJiang")
	g_ScriptEvent:RemoveListener("ShowUIPreDraw", self, "OnShowUIPreProcess")

	self:DestroyCountDownTick()
	self:DestroyChouJiangTick()
	self:DestroyShowResultTick()

end

return LuaBaGuaLuWnd
