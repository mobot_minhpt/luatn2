require("ui/schedule/LuaScheduleMgr")
require("ui/schedule/LuaScheduleHuoliView")
local CScheduleAlertCtl=import "L10.UI.CScheduleAlertCtl"
local CKeJuMgr=import "L10.Game.CKeJuMgr"
local CUIFx=import "L10.UI.CUIFx"
local CUIFxPaths=import "L10.UI.CUIFxPaths"
local CJingLingMgr=import "L10.Game.CJingLingMgr"
local UIPanel = import "UIPanel"
local GameSetting_Common_Wapper=import "L10.Game.GameSetting_Common_Wapper"
local CScheduleMgr=import "L10.Game.CScheduleMgr"
local UIScrollView=import "UIScrollView"
local CUITexture=import "L10.UI.CUITexture"
local CUICenterOnChild=import "L10.UI.CUICenterOnChild"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local QnRadioBox=import "L10.UI.QnRadioBox"
local EnumEventType=import "EnumEventType"
local QnSelectableButton=import "L10.UI.QnSelectableButton"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local Item_Item=import "L10.Game.Item_Item"
local Task_Task=import "L10.Game.Task_Task"
local CTaskMgr=import "L10.Game.CTaskMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CGuideMessager = import "L10.Game.Guide.CGuideMessager"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayUpdateAlertMgr=import "L10.Game.CPlayUpdateAlertMgr"
local Vector4 = import "UnityEngine.Vector4"
local CItemMgr = import "L10.Game.CItemMgr"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local DateTime = import "System.DateTime"

CLuaScheduleWnd=class()
RegistClassMember(CLuaScheduleWnd,"m_OpenJingLingButton")
RegistClassMember(CLuaScheduleWnd,"m_OpenCalendarButton")
RegistClassMember(CLuaScheduleWnd,"m_OpenKejuReplayButton")
RegistClassMember(CLuaScheduleWnd,"m_GuideButton")
RegistClassMember(CLuaScheduleWnd,"m_AlertButton")
RegistClassMember(CLuaScheduleWnd,"m_JieRiButton")
RegistClassMember(CLuaScheduleWnd,"m_SettingButton")


RegistClassMember(CLuaScheduleWnd,"m_KejuReplayFx")
RegistClassMember(CLuaScheduleWnd,"m_HuoliView")

RegistClassMember(CLuaScheduleWnd,"m_TableView")
RegistClassMember(CLuaScheduleWnd,"m_DataList")

RegistClassMember(CLuaScheduleWnd,"m_RouteButtonTemplate")
RegistClassMember(CLuaScheduleWnd,"m_RouteButtonTemplate2")
RegistClassMember(CLuaScheduleWnd,"m_ContentPanel")
RegistClassMember(CLuaScheduleWnd,"m_ContentRegionSprite")

RegistClassMember(CLuaScheduleWnd,"m_RouteButtonsGrid")
RegistClassMember(CLuaScheduleWnd,"m_SidebarIndexs")
RegistClassMember(CLuaScheduleWnd,"m_StyleRoot")

RegistClassMember(CLuaScheduleWnd,"m_Type")
RegistClassMember(CLuaScheduleWnd,"m_RegionSize1")
RegistClassMember(CLuaScheduleWnd,"m_RegionSize2")

RegistClassMember(CLuaScheduleWnd,"m_SidebarCmps")

--guide
RegistClassMember(CLuaScheduleWnd,"m_JuQingItem")
RegistClassMember(CLuaScheduleWnd,"m_JuQingInfo")

RegistClassMember(CLuaScheduleWnd,"m_ShiMenItem")
RegistClassMember(CLuaScheduleWnd,"m_YiTiaoLongItem")

RegistClassMember(CLuaScheduleWnd,"m_MainPlayerLevel")
RegistClassMember(CLuaScheduleWnd,"m_HasFeiSheng")
RegistClassMember(CLuaScheduleWnd,"m_XianshenLevel")

RegistClassMember(CLuaScheduleWnd,"m_StarBiWuNode")

RegistClassMember(CLuaScheduleWnd,"m_SaiShiNode")
RegistClassMember(CLuaScheduleWnd,"m_SaiShiButton")
RegistClassMember(CLuaScheduleWnd,"m_SaiShiDescLabel")
RegistClassMember(CLuaScheduleWnd,"m_SaiShiTitleLabel")
RegistClassMember(CLuaScheduleWnd,"m_SaiShiItem1")
RegistClassMember(CLuaScheduleWnd,"m_SaiShiItem2")
RegistClassMember(CLuaScheduleWnd,"m_SaiShiItems")
RegistClassMember(CLuaScheduleWnd,"m_SaiShiDescScrollView")


RegistClassMember(CLuaScheduleWnd,"m_ChouJiangPage")
RegistClassMember(CLuaScheduleWnd,"m_GuildLeagueCrossPage")
RegistClassMember(CLuaScheduleWnd,"m_RollBannerTick")
RegistClassMember(CLuaScheduleWnd,"m_CurRollBannerIndex")

RegistClassMember(CLuaScheduleWnd,"m_HaoYiXingTabButton")
RegistClassMember(CLuaScheduleWnd,"m_HaoYiXingButton")
RegistClassMember(CLuaScheduleWnd,"m_SideView")
RegistClassMember(CLuaScheduleWnd, "m_JieRiPage")
RegistClassMember(CLuaScheduleWnd, "m_XinBaiPage")
RegistClassMember(CLuaScheduleWnd, "m_CheckPlotDiscussionIsEnableTick")
RegistClassMember(CLuaScheduleWnd, "m_GroupButtonsHeight")

RegistClassMember(CLuaScheduleWnd, "m_ShenYaoTimesReceived")

RegistClassMember(CLuaScheduleWnd, "m_QMPKNode")

RegistClassMember(CLuaScheduleWnd, "m_ActivityId2Item")

function CLuaScheduleWnd:Init()
    local id = StarBiWuShow_Setting.GetData().MatchServerSchedule
    CLuaScheduleMgr.m_CrossServerActivityIds[id] = true

    self.m_MainPlayerLevel = 0
    self.m_HasFeiSheng = false
    self.m_XianshenLevel = 0
    if CClientMainPlayer.Inst then
        self.m_MainPlayerLevel=CClientMainPlayer.Inst.Level
        self.m_HasFeiSheng=CClientMainPlayer.Inst.HasFeiSheng
        self.m_XianshenLevel=CClientMainPlayer.Inst.XianShenLevel
    end

    self.transform:Find("Anchor/Style1/SaiShi").gameObject:SetActive(false)
    local pool=self.transform:Find("Anchor/Pool")
    self.m_SaiShiItem1 = pool:Find("SaiShiItem1").gameObject--:SetActive(false)
    self.m_SaiShiItem1:SetActive(false)
    self.m_SaiShiItem2 = pool:Find("SaiShiItem2").gameObject--:SetActive(false)
    self.m_SaiShiItem2:SetActive(false)
    self.m_SideView = self.transform:Find("Anchor/Style1/SideView"):GetComponent(typeof(UIPanel))


    self.m_SidebarIndexs={}

    FindChild(self.transform,"Style1").gameObject:SetActive(false)
    FindChild(self.transform,"Style2").gameObject:SetActive(false)

    self.m_RouteButtonTemplate=FindChild(self.transform,"RouteButton").gameObject
    self.m_RouteButtonTemplate:SetActive(false)

    self.m_RouteButtonTemplate2=FindChild(self.transform,"RouteButton2").gameObject
    self.m_RouteButtonTemplate2:SetActive(false)

    self.m_GuideButton=FindChild(self.transform,"GuideButton").gameObject
    self.m_AlertButton=FindChild(self.transform,"AlertButton").gameObject
    self.m_JieRiButton=FindChild(self.transform,"JieRiButton").gameObject
    self.m_SettingButton=FindChild(self.transform,"SettingButton").gameObject
    UIEventListener.Get(self.m_GuideButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.GuidelineWnd)
    end)
    UIEventListener.Get(self.m_AlertButton).onClick=DelegateFactory.VoidDelegate(function(go)
        local dataList = CPlayUpdateAlertMgr.Inst:GetValidPlayUpdateAlertTemplates()
        if dataList.Count>0 then
            CUIManager.ShowUI(CLuaUIResources.NewActivityWnd)
        else
            g_MessageMgr:ShowMessage("NEW_GAMEPLAY_UPDATA")
        end
    end)

    local todayFestivals = CLuaScheduleMgr:GetTodayFestivals()
    self.m_JieRiButton:SetActive(todayFestivals and #todayFestivals > 0 and CClientMainPlayer.Inst and not CClientMainPlayer.Inst.IsCrossServerPlayer and CClientMainPlayer.Inst.Level >= FestivalHelper_Setting.GetData().FestivalBtnOpenLevel)
    UIEventListener.Get(self.m_JieRiButton).onClick=DelegateFactory.VoidDelegate(function(go)
        local fes = CLuaScheduleMgr:GetTodayFestivals()
        if not fes then return end
        if #fes > 1 then
            LuaJieRiGroupScheduleWnd.s_OpenFromScheduleWnd = true
            CUIManager.ShowUI(CLuaUIResources.JieRiGroupScheduleWnd)
            return
        elseif #fes > 0 then
            CLuaScheduleMgr.OnJieRiGroupScheduleClick(fes[1])
        end
    end)

    self.m_SettingButton.gameObject:SetActive(false)
    -- UIEventListener.Get(self.m_SettingButton).onClick=DelegateFactory.VoidDelegate(function(go)
    --     CUIManager.ShowUI(CUIResources.ScheduleSettingWnd)
    -- end)

    self.m_HuoliView=CLuaScheduleHuoliView:new()
    self.m_HuoliView:Init(FindChild(self.transform,"HuoLi"))

    self.m_OpenJingLingButton=FindChild(self.transform,"OpenJingLingButton").gameObject
    self.m_OpenCalendarButton=FindChild(self.transform,"OpenCalendarButton").gameObject
    self.m_OpenKejuReplayButton=FindChild(self.transform,"OpenKejuReplayButton").gameObject
    self.m_KejuReplayFx=FindChild(self.m_OpenKejuReplayButton.transform,"Fx"):GetComponent(typeof(CUIFx))

    UIEventListener.Get(self.m_OpenKejuReplayButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
        Gac2Gas.RequestStartKeJuReview()
    end)
    UIEventListener.Get(self.m_OpenJingLingButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CJingLingMgr.Inst:ShowJingLingWnd()
    end)
    UIEventListener.Get(self.m_OpenCalendarButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.ScheduleAlertWnd)
    end)

    self.m_OpenJingLingButton:SetActive(GameSetting_Common_Wapper.Inst.JingLingEnabled)
    self.m_HuoliView:UpdateHuoli()
    
    self.m_OpenKejuReplayButton:SetActive(false)

    self:InitWorldEvent()

    CScheduleMgr.Inst:RequestTaskTimeCheck()
    Gac2Gas.QueryBiWuSignUpInfo()--请求有没有报名比武
    Gac2Gas.QueryWuHuoQiQinCount()--请求五火七禽扇的使用数量
    Gac2Gas.QueryKeJuReviewInfo()--查询是否有科举回顾
    Gac2Gas.QueryTaskHuoLiTimes()
    Gac2Gas.QuerySchoolTaskOpenStatus()
    Gac2Gas.TongQingYouLi_QueryInfo() --请求同庆有礼活动相关信息
    Gac2Gas.QueryLianDanLuAlertInfo()--请求2022周年庆丹炉战令相关信息
    Gac2Gas.XinBaiAchv_QueryAchievementInfo()--请求新白成就语音相关信息
    Gac2Gas.ShenYao_QueryRemainTimes()--请求蜃妖奖励和挑战次数信息
    CScheduleMgr.Inst:RequestActivity()
    CLuaScheduleMgr.RequestAlertStatus()
    if LuaHaoYiXingMgr.m_IsPlayOpen then
        self:CancelCheckPlotDiscussionIsEnableTick()
        self.m_CheckPlotDiscussionIsEnableTick = RegisterTickOnce(function()
            LuaPlotDiscussionMgr:CheckIsEnable()
        end, math.random(1000,60000))
    end
end

function CLuaScheduleWnd:CancelCheckPlotDiscussionIsEnableTick()
    if self.m_CheckPlotDiscussionIsEnableTick then
        UnRegisterTick(self.m_CheckPlotDiscussionIsEnableTick)
        self.m_CheckPlotDiscussionIsEnableTick = nil
    end
end

function CLuaScheduleWnd:OnDouHunStageResult(stage)
    if stage == 6 or stage == 7 then
        -- 主赛事
        Gac2Gas.QueryDouHunCrossGambleInfo(6)
    end
end

function CLuaScheduleWnd:OnDouHunGambleResult(gambleType, bOpenDoubleBet, gambleData, betDataList)
    local hasGamble = betDataList[gambleType].hasGamble
    local bSetResult = gambleData.bSetResult
    local hasGotReward = betDataList[gambleType].hasGotReward

    -- 没猜没结果 有结果已猜
    if (hasGamble and bSetResult and not hasGotReward)  then
        -- 设置红点
        CScheduleMgr.Inst:SetAlertState(42000429, CScheduleMgr.EnumAlertState.Show, false)
    elseif (not hasGamble and not bSetResult)  then
        CScheduleMgr.Inst:SetAlertState(42000429, CScheduleMgr.EnumAlertState.Show, false)
    end
end

function CLuaScheduleWnd:OnEnable()
    self.m_ActivityId2Item = {}
    g_ScriptEvent:AddListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
    g_ScriptEvent:AddListener("KeJuReplayInfoReturn", self, "OnKeJuReplayInfoReturn")
    g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "OnMainPlayerLevelChange")
    g_ScriptEvent:AddListener("UpdateScheduleTime", self, "OnUpdateScheduleTime")
    g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:AddListener("QueryWuHuoQiQinCount", self, "OnQueryWuHuoQiQinCount")
    g_ScriptEvent:AddListener("SyncCheckStarBiwuInJingCaiResult", self, "OnSyncCheckStarBiwuInJingCaiResult")
    g_ScriptEvent:AddListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:AddListener("UpdateScheduleWndAlertStatus", self, "OnUpdateScheduleWndAlertStatus")
    g_ScriptEvent:AddListener("ReceiveMySectNpcId", self, "OnReceiveMySectNpcId")
    g_ScriptEvent:AddListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    g_ScriptEvent:AddListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:AddListener("UpdateTask", self, "OnUpdateTask")
    g_ScriptEvent:AddListener("QueryDouHunStageResult", self, "OnDouHunStageResult")
    g_ScriptEvent:AddListener("QueryDouHunCrossGambleInfoResult", self, "OnDouHunGambleResult")
    g_ScriptEvent:AddListener("SendLianDanLuAlertInfo", self, "OnSendLianDanLuAlertInfo")
    g_ScriptEvent:AddListener("SyncVoiceAchievementAlertInfo", self, "OnSyncVoiceAchievementAlertInfo")
    g_ScriptEvent:AddListener("ShenYao_QueryRemainTimesResult", self, "ShenYao_QueryRemainTimesResult")
    g_ScriptEvent:AddListener("QueryGuildPinTuProgressResult", self, "QueryGuildPinTuProgressResult")
end

function CLuaScheduleWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
    g_ScriptEvent:RemoveListener("KeJuReplayInfoReturn", self, "OnKeJuReplayInfoReturn")
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "OnMainPlayerLevelChange")
    g_ScriptEvent:RemoveListener("UpdateScheduleTime", self, "OnUpdateScheduleTime")
    g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:RemoveListener("QueryWuHuoQiQinCount", self, "OnQueryWuHuoQiQinCount")
    g_ScriptEvent:RemoveListener("SyncCheckStarBiwuInJingCaiResult", self, "OnSyncCheckStarBiwuInJingCaiResult")
    g_ScriptEvent:RemoveListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:RemoveListener("UpdateScheduleWndAlertStatus", self, "OnUpdateScheduleWndAlertStatus")
    g_ScriptEvent:RemoveListener("ReceiveMySectNpcId", self, "OnReceiveMySectNpcId")
    g_ScriptEvent:RemoveListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    g_ScriptEvent:RemoveListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:RemoveListener("UpdateTask", self, "OnUpdateTask")
    g_ScriptEvent:RemoveListener("QueryDouHunStageResult", self, "OnDouHunStageResult")
    g_ScriptEvent:RemoveListener("QueryDouHunCrossGambleInfoResult", self, "OnDouHunGambleResult")
    g_ScriptEvent:RemoveListener("SendLianDanLuAlertInfo", self, "OnSendLianDanLuAlertInfo")
    g_ScriptEvent:RemoveListener("SyncVoiceAchievementAlertInfo", self, "OnSyncVoiceAchievementAlertInfo")
    g_ScriptEvent:RemoveListener("ShenYao_QueryRemainTimesResult", self, "ShenYao_QueryRemainTimesResult")
    g_ScriptEvent:RemoveListener("QueryGuildPinTuProgressResult", self, "QueryGuildPinTuProgressResult")
    self:CancelCheckPlotDiscussionIsEnableTick()
    CLuaStarBiwuMgr.m_IsInitStarBiwuPage = false
end

function CLuaScheduleWnd:OnUpdateActivityRedDot(changeList)
    if changeList then
        local tabTypeInfo = Task_TabType.GetData(CLuaScheduleMgr.taskTabType)
        if tabTypeInfo and System.String.IsNullOrEmpty(tabTypeInfo.SpecialPage) then 
            -- 更新ScheduleItem的红点
            for i = 1, #changeList do
                local redDotData = LuaActivityRedDotMgr.m_CachedRedDot[changeList[i]]
                if redDotData and redDotData.Activity then
                    for i = 0, redDotData.Activity.Length - 1 do
                        local item = self.m_ActivityId2Item[redDotData.Activity[i]]
                        if item then
                            self:InitItem(item[1], item[2])
                        end
                    end
                end
            end
        else
            -- 特殊页面暂时只能全刷一遍了
            self:OnUpdateActivity()
        end
        -- 更新RouteButton的红点
        EventManager.Broadcast(EnumEventType.UpdateScheduleAlert) 
    end
end

function CLuaScheduleWnd:QueryGuildPinTuProgressResult()
    self.m_PinTuFininsh = true
    local item = self.m_ActivityId2Item[42000500]
    if item then
        self:InitItem(item[1], item[2])
    end
end

--剩余奖励次数、今日已获得奖励次数，剩余参与次数、今日已参与次数
function CLuaScheduleWnd:ShenYao_QueryRemainTimesResult(RemainRewardTimes, RewardTimesToday, RemainJoinTimes, JoinTimesToday)
    LuaXinBaiLianDongMgr.m_ShenYaoRewardTimes = RewardTimesToday
    LuaXinBaiLianDongMgr.m_ShenYaoMaxRewardTimes = RewardTimesToday + RemainRewardTimes
    LuaXinBaiLianDongMgr.m_ShenYaoJoinTimes = JoinTimesToday
    LuaXinBaiLianDongMgr.m_ShenYaoMaxJoinTimes = JoinTimesToday + RemainJoinTimes
    self.m_ShenYaoTimesReceived = true
    if self.m_Type and not CUIManager.IsLoaded(CLuaUIResources.ScheduleInfoWnd) then
        local scheduleData = Task_Schedule.GetData(42000434) --ShenYao
        for i = 0, scheduleData.TaskTabType.Length - 1 do 
            if scheduleData.TaskTabType[i] == self.m_Type then
                local item = self.m_ActivityId2Item[42000434]
                if item then
                    self:InitItem(item[1], item[2])
                end
                break
            end
        end
    end
end

function CLuaScheduleWnd:OnPlotDiscussionEnable()
    g_ScriptEvent:RemoveListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    local isEnable = LuaPlotDiscussionMgr:IsOpen()
    if isEnable then
        LuaPlotDiscussionMgr:RequestRedDotInfo()
    end
end

function CLuaScheduleWnd:OnPlotCommentShowRedAlert(haoYiXingCardId, yaoGuiHuCardId, isClear)
    if isClear then
        CScheduleMgr.Inst:SetAlertState(42000402, CScheduleMgr.EnumAlertState.Hide, true)
    end
    if haoYiXingCardId ~= 0 then
        CScheduleMgr.Inst:SetAlertState(42000402, CScheduleMgr.EnumAlertState.Show, true)
    end
end

function CLuaScheduleWnd:OnUpdateScheduleWndAlertStatus(alert)
    CScheduleMgr.Inst:SetAlertState(42010040, alert and CScheduleMgr.EnumAlertState.Show or CScheduleMgr.EnumAlertState.Hide, false)
end

function CLuaScheduleWnd:OnSyncCheckStarBiwuInJingCaiResult(bInJingCai)
    CLuaScheduleStarBiwuPage.s_InJingCai = bInJingCai
    --跨服明星赛
    CLuaScheduleMgr.UpdateAlertStatus(StarBiWuShow_Setting.GetData().MatchServerSchedule,bInJingCai and LuaEnumAlertState.Show or LuaEnumAlertState.Hide)
end

function CLuaScheduleWnd:OnReceiveMySectNpcId(npcId)
    if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.InviteNpcAlert) and npcId ~= 0 then
        CScheduleMgr.Inst:SetAlertState(42000401, CScheduleMgr.EnumAlertState.Show, false)
    end
end

function CLuaScheduleWnd:OnQueryTempPlayDataInUDResult(key, ud, data)
    if key ~= EnumTempPlayDataKey_lua.eHouseCompetitionFanPaiZiData then
        return
    end

    local scheduleId = HouseCompetition_Setting.GetData().ScheduleId
    if not CScheduleMgr.Inst:IsCanJoinSchedule(scheduleId, true)  then
        return
    end
    
    local chancesItemId = HouseCompetition_Setting.GetData().FanPaiZi_ItemID
    local m_ProgressMax  = HouseCompetition_Setting.GetData().FanPaiZi_Progress
    local rewardsData = HouseCompetition_Setting.GetData().FanPaiZi_Reward

    local currentProgress = 0
    local canGetReward = false
    if data then
        for i = 0, m_ProgressMax-1 do
            if data.CardToReward[i] ~= nil and data.CardToReward[i] ~= 0 then
                currentProgress = currentProgress + 1
            end
        end

        for i = 0, rewardsData.Length - 1 do
            local progress = rewardsData[i][0]
            if currentProgress >= progress and data.ExtraReward[progress-1] == 0 then --是否可以领奖
                canGetReward = true
                break
            end
        end
    end
    local canDraw = CItemMgr.Inst:GetItemCount(chancesItemId) > 0 and currentProgress<m_ProgressMax

    CScheduleMgr.Inst:SetAlertState(scheduleId, (canDraw or canGetReward) and CScheduleMgr.EnumAlertState.Show or CScheduleMgr.EnumAlertState.Clicked, true)
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)
    self:OnUpdateActivity() --更新一下当前界面
end

function CLuaScheduleWnd:OnClickBanner(go)
    local index=go.transform:GetSiblingIndex()
    local info=CLuaScheduleMgr.m_PicList[index+1]
    if info then
        if info.activityId then
            local id=info.activityId
            local scheduleData=Task_Schedule.GetData(id)
            if scheduleData and scheduleData.PicFull and scheduleData.PicFull~="" then
                CLuaScheduleMgr.m_PicScheduleInfo=info
                CLuaScheduleMgr.m_PicScheduleIndex = index + 1
                CUIManager.ShowUI(CLuaUIResources.SchedulePicWnd)
            end
        elseif info.alertId then
            local id= info.alertId
            local alertData=Gameplay_PropagandaAlert.GetData(id)
            if alertData and alertData.ActivityPicturePath and alertData.ActivityPicturePath~="" then
                CLuaScheduleMgr.m_PicScheduleInfo=info
                CLuaScheduleMgr.m_PicScheduleIndex = index + 1
                CUIManager.ShowUI(CLuaUIResources.SchedulePicWnd)
            end
        end
    end
end

function CLuaScheduleWnd:OnSendLianDanLuAlertInfo()
    local worldEventRoot = FindChild(self.transform,"WorldEventRoot")
    local liandanlu = worldEventRoot:Find("liandanlu").gameObject
    if not IsHuLuWaLianDanLuBtnShow() then
        return
    end
    if liandanlu then
        liandanlu:SetActive(true)
        local levelLabel = liandanlu.transform:Find("Label"):GetComponent(typeof(UILabel))
        local level,_ = LuaHuLuWa2022Mgr.GetCurZhanLingLevel()
        levelLabel.text = level
        local fx = liandanlu.transform:Find("AlertFx"):GetComponent(typeof(CUIFx))
        if LuaHuLuWa2022Mgr.m_SchedulePageAlert then
            fx:LoadFx("fx/ui/prefab/UI_zhounianhuijuan_huo.prefab")
            liandanlu.transform:Find("DanLuTexture").gameObject:SetActive(false)
        else
            fx:DestroyFx()
            liandanlu.transform:Find("DanLuTexture").gameObject:SetActive(true)
        end
    end
end

function CLuaScheduleWnd:OnSyncVoiceAchievementAlertInfo(needAlert)
    if not needAlert then
        CScheduleMgr.Inst:SetAlertState(42010123, CScheduleMgr.EnumAlertState.Hide, true)
    else
        CScheduleMgr.Inst:SetAlertState(42010123, CScheduleMgr.EnumAlertState.Show, true)
    end
end

function CLuaScheduleWnd:InitBanner()
    local tf=FindChild(self.transform,"Banner")

    local item=FindChild(tf,"Item").gameObject
    local indicator=FindChild(tf,"Indicator").gameObject

    item:SetActive(false)
    indicator:SetActive(false)
    --table.sort(CLuaScheduleMgr.m_PicList,function (a,b)
    --    local priorityList = {0, 0}
    --    local infoList = {a, b}
    --    for i = 1, 2 do
    --        if infoList[i].activityId then
    --            local scheduleData = Task_Schedule.GetData(infoList[i].activityId)
    --            if scheduleData then
    --                priorityList[i] = scheduleData.PicThubPriority
    --            end
    --        elseif infoList[i].alertId then
    --            local alertData = Gameplay_PropagandaAlert.GetData(infoList[i].alertId)
    --            if alertData then
    --                priorityList[i] = alertData.BannerPriority
    --            end
    --        end
    --    end
    --    return priorityList[1] > priorityList[2]
    --end)
    local table=FindChild(tf,"Grid")
    local indicators=FindChild(tf,"Indicators")
    Extensions.RemoveAllChildren(table)
    Extensions.RemoveAllChildren(indicators)

    for i,v in ipairs(CLuaScheduleMgr.m_PicList) do
        local go=NGUITools.AddChild(table.gameObject,item)
        go:SetActive(true)
        if v.activityId then
            local scheduleData=Task_Schedule.GetData(v.activityId)
            if scheduleData then
                go:GetComponent(typeof(CUITexture)):LoadMaterial(scheduleData.PicThub)
            end
        elseif v.alertId then
            local alertData =Gameplay_PropagandaAlert.GetData(v.alertId)
            if alertData then
                go:GetComponent(typeof(CUITexture)):LoadMaterial(alertData.BannerPicturePath)
            end
        end
        UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(g)
            self:OnClickBanner(g)
        end)

        if #CLuaScheduleMgr.m_PicList>1 then
            local igo=NGUITools.AddChild(indicators.gameObject,indicator)
            igo:SetActive(true)
        end
    end
    table:GetComponent(typeof(UIGrid)):Reposition()
    indicators:GetComponent(typeof(UIGrid)):Reposition()

    local centerOnChild=table:GetComponent(typeof(CUICenterOnChild))

    centerOnChild.onCenter=LuaUtils.OnCenterCallback(function(go)
        if not go then return end
        local index=go.transform:GetSiblingIndex()
        self.m_CurRollBannerIndex = index
        for i=1,indicators.childCount do
            local sprite=indicators:GetChild(i-1):GetComponent(typeof(UISprite))
            if i==index+1 then
                -- sprite.spriteName="loginwnd_button_hair_color_n"--"common_page_icon_highlight"
                sprite.color=Color.white
            else
                -- sprite.spriteName="loginwnd_button_hair_color_n"--"common_page_icon_normal"
                sprite.color=Color(0.3,0.5,0.8)
            end
        end
    end)

    

    centerOnChild:CenterOnInstant(table:GetChild(0))
    self:AutoRollBanner(table,centerOnChild)
end

function CLuaScheduleWnd:AutoRollBanner(table,centerOnChild)
    if self.m_RollBannerTick then
        UnRegisterTick(self.m_RollBannerTick)
        self.m_RollBannerTick = nil
    end
    self.m_RollBannerTick = RegisterTick(function ()
        local idx = (self.m_CurRollBannerIndex + 1) %(#CLuaScheduleMgr.m_PicList)
        if table.childCount>idx then
            centerOnChild:CenterOnInstant(table:GetChild(idx))
        end
    end,10000)
end

function CLuaScheduleWnd:OnUpdateActivity()
    if not CScheduleMgr.Inst.rawInfoTimeRestriction then
	   return 
    end
    --筛选item
    CLuaScheduleMgr.BuildInfos()
    --推荐
    --日常
    --休闲
    local isBannerEnable = CLuaScheduleMgr.GetBannarEnable()
    if #CLuaScheduleMgr.m_PicList==0 or not isBannerEnable then
        local parent=FindChild(self.transform,"Style2")
        self.m_StyleRoot=parent
        FindChild(self.transform,"Style1").gameObject:SetActive(false)
        FindChild(self.transform,"Style2").gameObject:SetActive(true)

        -- self.m_RouteButtonsRectSprite=FindChild(parent,"RouteButtons"):GetComponent(typeof(UIWidget))
        self.m_ContentPanel=FindChild(parent,"ContentPanel"):GetComponent(typeof(UIPanel))
        self.m_RouteButtonsGrid=FindChild(parent,"RouteButtons")
        self.m_ContentRegionSprite=FindChild(parent,"Region"):GetComponent(typeof(UIWidget))

        self.m_TableView = self.m_ContentPanel.gameObject:GetComponent(typeof(QnTableView))
        self.m_GroupButtonsHeight = 475
    else
        local parent=FindChild(self.transform,"Style1")
        self.m_StyleRoot=parent
        FindChild(self.transform,"Style1").gameObject:SetActive(true)
        FindChild(self.transform,"Style2").gameObject:SetActive(false)

        self:InitBanner()

        -- self.m_RouteButtonsRectSprite=FindChild(parent,"RouteButtons"):GetComponent(typeof(UIWidget))
        self.m_ContentPanel=FindChild(parent,"ContentPanel"):GetComponent(typeof(UIPanel))
        self.m_RouteButtonsGrid=FindChild(parent,"RouteButtons")
        self.m_ContentRegionSprite=FindChild(parent,"Region"):GetComponent(typeof(UIWidget))

        self.m_TableView = self.m_ContentPanel.gameObject:GetComponent(typeof(QnTableView))
        self.m_GroupButtonsHeight = 324
    end
    self.m_StarBiWuNode = self.m_StyleRoot:Find("CrossStar")

    self.m_SaiShiNode = self.m_StyleRoot:Find("SaiShi")
    self.m_SaiShiButton = self.m_SaiShiNode:Find("Overlay/Button").gameObject
    self.m_SaiShiDescLabel = self.m_SaiShiNode:Find("Overlay/Text/DescLabel"):GetComponent(typeof(UILabel))
    self.m_SaiShiTitleLabel = self.m_SaiShiNode:Find("Overlay/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_SaiShiDescScrollView=self.m_SaiShiNode:Find("Overlay/Text"):GetComponent(typeof(UIScrollView))

    local todayFestivals = CLuaScheduleMgr:GetTodayFestivals()
    self.m_JieRiButton:SetActive(todayFestivals and #todayFestivals > 0 and (not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.IsCrossServerPlayer))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,
        function(item,index)
            -- self:InitItem(item,index,self.m_RankList[index+1])
            self:InitItem(item.transform,self.m_DataList[index+1])
        end)

    local root=FindChild(self.m_ContentPanel.transform,"Grid")
    self:ProcessLeft()
    
    self:ShowTab()
end

function CLuaScheduleWnd:ProcessLeft()
    local leftRoot=self.m_RouteButtonsGrid
    if not leftRoot then return end
    CUICommonDef.ClearTransform(leftRoot)

    local addFunc=function(taskTabType)
        local barItem = nil
        local taskTabTypeInfo = Task_TabType.GetData(taskTabType)

        local btnStyle = taskTabTypeInfo.ButtonStyle
        if btnStyle == 2 then
            barItem = NGUITools.AddChild(leftRoot.gameObject,self.m_RouteButtonTemplate2)
        else
            barItem = NGUITools.AddChild(leftRoot.gameObject,self.m_RouteButtonTemplate)
        end
        barItem:SetActive(true)
        local alertCtrl=barItem:GetComponent(typeof(CScheduleAlertCtl))
        local label=barItem.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text=taskTabTypeInfo.TabType
        alertCtrl.tabType = taskTabType + 2
        alertCtrl.designTabTypeID = taskTabType
        --alertCtrl.scheduleId = taskTabTypeInfo.SchduleId
        if taskTabTypeInfo.NotShowRedDot == 1 then alertCtrl.tabType = 0 end
        if taskTabType == 11 then self.m_HaoYiXingTabButton = barItem end -- 皓衣行活动引导按钮的特殊处理
        if taskTabType == 7 then alertCtrl.scheduleId = StarBiWuShow_Setting.GetData().MatchServerSchedule end  -- 跨服明星赛ScheduleId处理

        if taskTabType == 16 then
            -- 设置每日红点
            if CLuaFightingSpiritMgr:HasWenDaOpen() and not CLuaFightingSpiritMgr:HasGambleFinish() and CClientMainPlayer.Inst then
                local now = CServerTimeMgr.Inst:GetZone8Time()
                local cur = now.Day + now.Month*100

                local alertStrGamble = "douhun_gamble_alert".. tostring(CClientMainPlayer.Inst.Id)
                local recordGamble = PlayerPrefs.GetInt(alertStrGamble, 0)

                local alertStrWenda = "douhun_wenda_alert".. tostring(CClientMainPlayer.Inst.Id)
                local recordWenda = PlayerPrefs.GetInt(alertStrWenda, 0)

                if recordWenda ~= cur or (CLuaFightingSpiritMgr:HasGambleOpen() and recordGamble ~= cur) then
                    CScheduleMgr.Inst:SetAlertState(42000429, CScheduleMgr.EnumAlertState.Show, true)
                end
            end

            local date = DouHunTan_Cron.GetData("SignUp").Value
            local _, _, mm, hh, dd, MM = string.find(date, "(%d+) (%d+) (%d+) (%d+) *")

            local now = CServerTimeMgr.Inst:GetZone8Time()
            local startTime = CreateFromClass(DateTime, now.Year, MM, dd, hh, mm, 0)
            -- 查询斗魂进度
            -- 为了竞猜结果
            if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, startTime) then
                Gac2Gas.QueryDouHunStage()
            end
        end

        -- 全民争霸赛手册奖励
        if taskTabType == 22 then
            --Gac2Gas.QueryQmpkHandbookReceiveAwardInfo()
            Gac2Gas.QueryQmpkActivityPageMeedInfo()
        end

        -- local cmp=barItem:GetComponent(typeof(QnSelectableButton))
        table.insert(self.m_SidebarCmps,barItem)

        UIEventListener.Get(barItem).onClick=DelegateFactory.VoidDelegate(function(go)
            --切换活动
            self:OnClickSidebar(go)
        end)
    end

    self.m_SidebarIndexs={}
    self.m_SidebarCmps={}

    for index,items in pairs(CLuaScheduleMgr.m_TableTypeList) do
        if #items > 0 and index ~= 2 then
            table.insert(self.m_SidebarIndexs,index )
        end
    end
    table.sort(self.m_SidebarIndexs,function(a,b)
        local p1 = Task_TabType.GetData(a).PriorityLevel
        local p2 = Task_TabType.GetData(b).PriorityLevel
        if p1 ~= p2 then
            return p1 < p2
        else
            return a < b
        end
    end)
    for i=1,#self.m_SidebarIndexs do
        addFunc(self.m_SidebarIndexs[i])
    end

    leftRoot:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaScheduleWnd:ShowTab()
    local leftRoot = self.m_RouteButtonsGrid
    if not leftRoot then return end
    local index = 0
    local defaultType = CLuaScheduleMgr.taskTabType
    if defaultType then
        index = self:GetSideBarGoIndex(defaultType)
        if index < 0 then
            index = 0
        end
    end
    self:OnClickSidebar(leftRoot:GetChild(index).gameObject)
end

function CLuaScheduleWnd:GetSideBarGoIndex(tasktabtype)
    for i=1,#self.m_SidebarIndexs do
        if self.m_SidebarIndexs[i] == tasktabtype then 
            return i - 1
        end
    end
    return -1
end

function CLuaScheduleWnd:OnClickSidebar(go)
    --根据类型显示，只有日常才需要显示
    local index=go.transform:GetSiblingIndex()+1
    local type=self.m_SidebarIndexs[index]
    CLuaScheduleMgr.taskTabType = type
    local groupButtons=FindChild(self.m_StyleRoot,"GroupButtons").gameObject
    if type==0 then--推荐
        groupButtons:SetActive(false)

    elseif type==1 then--日常
        --显示
        groupButtons:SetActive(true)
        local radioBox=groupButtons:GetComponent(typeof(QnRadioBox))
        radioBox.OnSelect=DelegateFactory.Action_QnButton_int(function(btn,index)
            self:OnSelectRadioBox(btn,index)
        end)

    else--节日 休闲 线下
        groupButtons:SetActive(false) 

    end
    Extensions.SetLocalPositionY(groupButtons.transform,type== 1 and self.m_GroupButtonsHeight or (self.m_GroupButtonsHeight + 100))
    local page = self.m_StyleRoot.transform:Find("DouHunPage")
    if page then
        page.gameObject:SetActive(type == 16)
    end

    local huluwa = self.m_StyleRoot.transform:Find("HuLuWaPage")
    if huluwa then
        huluwa.gameObject:SetActive(type == 18 and LuaHuLuWa2022Mgr.s_HuLuWaSwitch)
    end


    -- local cmp=go:GetComponent(typeof(QnSelectableButton))
    for i,v in ipairs(self.m_SidebarCmps) do
        if v==go then
            local cmp=v:GetComponent(typeof(QnSelectableButton))
            if cmp then
                cmp:SetSelected(true,false)
            else
                local texture = v.transform:Find("Texture"):GetComponent(typeof(CUITexture))
                texture:LoadMaterial("UI/Texture/Transparent/Material/schedulewnd_button_highlight.mat")
            end
        else
            local cmp=v:GetComponent(typeof(QnSelectableButton))
            if cmp then
                cmp:SetSelected(false,false)
            else
                local texture = v.transform:Find("Texture"):GetComponent(typeof(CUITexture))
                texture:LoadMaterial("UI/Texture/Transparent/Material/schedulewnd_button_normal.mat")
            end
        end
    end
    


    self.m_Type=type
    self:InitList(type,0)
end
function CLuaScheduleWnd:OnSelectRadioBox(btn,index)
    --过滤
    if index==0 then--所有活动
        self:InitList(self.m_Type,0)
    elseif index==1 then--我要装备
        self:InitList(self.m_Type,1)
    elseif index==2 then--我要经验
        self:InitList(self.m_Type,2)
    elseif index==3 then--我要帮贡
        self:InitList(self.m_Type,3)
    end


    --刷新引导
    CGuideMgr.Inst:ClearGuideBubbleTarget()
    -- EventManager.Broadcast(EnumEventType.Guide_ChangeView, "ScheduleWnd")
end
function CLuaScheduleWnd:InitList(type,filterType)
    self.m_JuQingItem=nil
    self.m_YiTiaoLongItem=nil
    self.m_ShiMenItem=nil

    local list={}
    -- if type==0 then
    --     list=CLuaScheduleMgr.m_RecommendList
    -- else
    list = CLuaScheduleMgr.m_TableTypeList[type]

    --[[
    if type==1 then--
        list=CLuaScheduleMgr.m_DailyList
    elseif type==2 then
        list=CLuaScheduleMgr.m_JieRiList
    elseif type==3 then
        list=CLuaScheduleMgr.m_CasualList
    elseif type==4 then
        list=CLuaScheduleMgr.m_XianXiaList
    elseif type==5 then
        list=CLuaScheduleMgr.m_ChengZhanList
    elseif type==6 then
        list=CLuaScheduleMgr.m_SaiShiList
    elseif type==7 then
        list=CLuaScheduleMgr.m_StarBiWuList
    elseif type==8 then
        list=CLuaScheduleMgr.m_SpokesmanList
    elseif type == LuaEnumTaskTabType.ChouJiang then
        list = CLuaScheduleMgr.m_ChouJiangList
    elseif type == LuaEnumTaskTabType.HaoYiXing then
        list = CLuaScheduleMgr.m_HaoYiXingList
    end
]]
    self.m_ContentPanel:ResetAndUpdateAnchors()
    local tempList={}
    for i,v in ipairs(list) do
        local pass=true
        if filterType>0 and not CLuaScheduleMgr.m_FilterLookup[filterType][v.activityId] then
            pass=false
        end
        
        if pass then
            table.insert( tempList,v )
        end
    end

    -- if type==6 then
    --     self.m_TableView:Clear()
    --     self.m_TableView.gameObject:SetActive(false)
    --     self.m_SaiShiNode.gameObject:SetActive(true)
    --     if self.m_StarBiWuNode then
    --         self.m_StarBiWuNode.gameObject:SetActive(false)
    --     end
    --     self:InitSaiShiList(tempList)
    -- else

    self.m_JieRiPage = self.m_StyleRoot:Find("JieRiPage")
    self.m_ChouJiangPage = self.m_StyleRoot:Find("ChouJiangPage")
    self.m_GuildLeagueCrossPage = self.m_StyleRoot:Find("GuildLeagueCrossPage")
    self.m_XinBaiPage = self.m_StyleRoot:Find("XinBaiPage")
    self.m_QMPKNode = self.m_StyleRoot:Find("QMPKPage")
    if self.m_ChouJiangPage then self.m_ChouJiangPage.gameObject:SetActive(false) end
    if self.m_JieRiPage then self.m_JieRiPage.gameObject:SetActive(false) end
    if self.m_SaiShiNode then self.m_SaiShiNode.gameObject:SetActive(false) end
    if self.m_StarBiWuNode then  self.m_StarBiWuNode.gameObject:SetActive(false) end
    if self.m_GuildLeagueCrossPage then self.m_GuildLeagueCrossPage.gameObject:SetActive(false) end
    if self.m_XinBaiPage then self.m_XinBaiPage.gameObject:SetActive(false) end
    if self.m_QMPKNode then self.m_QMPKNode.gameObject:SetActive(false) end

    local tabTypeInfo = Task_TabType.GetData(type)
    if System.String.IsNullOrEmpty(tabTypeInfo.SpecialPage) or tabTypeInfo.SpecialPage == "SaiShi" then
        self.m_TableView.gameObject:SetActive(true)

        self.m_DataList = tempList
        self.m_ActivityId2Item = {}
        self.m_TableView:ReloadData(true,false)
    
        self.m_ContentPanel.gameObject:GetComponent(typeof(UIScrollView)):ResetPosition()

    else
        local Page = self.m_StyleRoot:Find(tabTypeInfo.SpecialPage)
        if not Page then return end
        self.m_TableView:Clear()
        self.m_TableView.gameObject:SetActive(false)
        Page.gameObject:SetActive(true)
        --if tabTypeInfo.SpecialPage == "SaiShi" then
        --    self:InitSaiShiList(tempList)
        --    return
        --end
        Page:GetComponent(typeof(CCommonLuaScript)):Init({})
    end
    --[[
    self.m_ChouJiangPage = self.m_StyleRoot:Find("ChouJiangPage")
    if self.m_ChouJiangPage then
        self.m_ChouJiangPage.gameObject:SetActive(tabTypeInfo.SpecialPage == LocalString.GetString("ChouJiangPage"))
    end
    -- 新活动界面
    self.m_JieRiPage = self.m_StyleRoot:Find("JieRiPage")
    if self.m_JieRiPage then
        self.m_JieRiPage.gameObject:SetActive(tabTypeInfo.SpecialPage == LocalString.GetString("JieRiPage"))
    end
    
    if tabTypeInfo.SpecialPage == LocalString.GetString("CrossStar") then
        self.m_TableView:Clear()
        self.m_TableView.gameObject:SetActive(false)
        self.m_SaiShiNode.gameObject:SetActive(false)
        if self.m_StarBiWuNode then
            self.m_StarBiWuNode.gameObject:SetActive(true)
            local script = self.m_StarBiWuNode:GetComponent(typeof(CCommonLuaScript))
            script:Init({})
        end
    elseif tabTypeInfo.SpecialPage == LocalString.GetString("ChouJiangPage") then
        self.m_TableView:Clear()
        self.m_TableView.gameObject:SetActive(false)
        self.m_SaiShiNode.gameObject:SetActive(false)

        if self.m_ChouJiangPage then
            local script = self.m_ChouJiangPage:GetComponent(typeof(CCommonLuaScript))
            script:Init({})
        end
    elseif tabTypeInfo.SpecialPage == LocalString.GetString("JieRiPage") then
        self.m_TableView.gameObject:SetActive(false)
        self.m_SaiShiNode.gameObject:SetActive(false)

        if self.m_JieRiPage then
            self.m_JieRiPage:GetComponent(typeof(CCommonLuaScript)):Init({})
        end
    else
        self.m_TableView.gameObject:SetActive(true)
        self.m_SaiShiNode.gameObject:SetActive(false)
        if self.m_StarBiWuNode then
            self.m_StarBiWuNode.gameObject:SetActive(false)
        end

        self.m_DataList = tempList
        self.m_ActivityId2Item = {}
        self.m_TableView:ReloadData(true,false)
    
        self.m_ContentPanel.gameObject:GetComponent(typeof(UIScrollView)):ResetPosition()
    end]]

end

function CLuaScheduleWnd:GetSaiShiItemPos(i)
    local index=i-1
    local saishiPage = 783
    
    if #CLuaScheduleMgr.m_PicList==0 then
        if index%3==0 then
            return Vector3(27,-112 - math.floor(index/3)*saishiPage,0)
        elseif index%3==1 then
            return Vector3(88,-326- math.floor(index/3)*saishiPage,0)
        elseif index%3==2 then
            return Vector3(64,-667- math.floor(index/3)*saishiPage,0)
        end
    else
        if index%3==0 then
            return Vector3(27,-120 - math.floor(index/3)*saishiPage,0)
        elseif index%3==1 then
            return Vector3(88,-333- math.floor(index/3)*saishiPage,0)
        elseif index%3==2 then
            return Vector3(64,-673- math.floor(index/3)*saishiPage,0)
        end
    end
end


function CLuaScheduleWnd:InitSaiShiList(list)
    self.m_SaiShiItems = {}

    local wzss = {}
    local ids = GameSetting_Client.GetData().WaiZhiSaiShiIds
    if ids then
        for i=1,ids.Length do
            wzss[ids[i-1]] = true
        end
    end

    self.m_SaiShiButton:SetActive(false)
    self.m_SaiShiDescLabel.text = nil

    local parent = self.m_SaiShiNode:Find("SaiShi/Items").gameObject
    CUICommonDef.ClearTransform(parent.transform)

    local bg = self.m_SaiShiNode:Find("SaiShi/Texture"):GetComponent(typeof(UITexture))
    bg.height = math.floor(#list/3)*511 + (math.floor(#list%3)>0 and 511 or 0)

    local setFirst=false
    for i,info in ipairs(list) do
        local schedule = Task_Schedule.GetData(info.activityId)
        if schedule then 
            local go = NGUITools.AddChild(parent,wzss[info.activityId] and self.m_SaiShiItem2 or self.m_SaiShiItem1)
            go:SetActive(true)
            table.insert( self.m_SaiShiItems,go )

            go.transform.localPosition = self:GetSaiShiItemPos(i)
            local transform = go.transform
    
            local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
            local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    
            nameLabel.text = schedule.TaskName
            icon.material = nil
            --图标
            if schedule.Reward ~= nil and schedule.Reward.Length > 0 then
                local id = schedule.Reward[0]
                local template = Item_Item.GetData(id)
                if template ~= nil then
                    icon:LoadMaterial(template.Icon)
                else
                    local equipTemplate = EquipmentTemplate_Equip.GetData(id)
                    if equipTemplate ~= nil then
                        icon:LoadMaterial(equipTemplate.Icon)
                    end
                end
            end

            --
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
                self:OnClickSaiShiItem(g,info)
            end)
            --默认选中第一个
            if not setFirst then
                setFirst=true
                self:OnClickSaiShiItem(go,info)
            end
        end
    end
end
function CLuaScheduleWnd:OnClickSaiShiItem(go,info)
    self.m_SaiShiButton:SetActive(true)
    UIEventListener.Get(self.m_SaiShiButton).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnClickCanJiaButton(nil,info)--不传入transform
    end)
    local template = Task_Task.GetData(info.taskId)
    self.m_SaiShiDescLabel.text = CUICommonDef.TranslateToNGUIText(template.TaskDescription)
    self.m_SaiShiDescScrollView:ResetPosition()

    local schedule = Task_Schedule.GetData(info.activityId)
    if schedule then
        self.m_SaiShiTitleLabel.text = schedule.TaskName
    else
        self.m_SaiShiTitleLabel.text = nil
    end

    for i,v in ipairs(self.m_SaiShiItems) do
        local tf = v.transform
        local highlight = tf:Find("Highlight").gameObject
        local normal = tf:Find("Normal").gameObject

        if v==go then
            highlight:SetActive(true)
            normal:SetActive(false)
        else
            highlight:SetActive(false)
            normal:SetActive(true)
        end
    end
end


function CLuaScheduleWnd:OnKeJuReplayInfoReturn()
    self.m_OpenKejuReplayButton:SetActive(CKeJuMgr.Inst.kejuReplayOpen)
    self.m_SideView.baseClipRegion = CKeJuMgr.Inst.kejuReplayOpen and Vector4(-6,-220,288,608) or Vector4(-6,-277,288,718)
    local indicator = CommonDefs.GetComponentInChildren_Component_Type(self.m_SideView.transform.parent, typeof(UIScrollViewIndicator))
    if indicator ~= nil then
        indicator:Layout()
    end
    if CKeJuMgr.Inst.kejuReplayOpen and CKeJuMgr.Inst.replayLastIndex==-1 then
        self.m_KejuReplayFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        local background=self.m_OpenKejuReplayButton:GetComponent(typeof(UISprite))
        --doani
    else
        self.m_KejuReplayFx:DestroyFx()
    end
end


function CLuaScheduleWnd:OnMainPlayerLevelChange()
    self.m_HuoliView:UpdateHuoli()
end
function CLuaScheduleWnd:OnUpdateScheduleTime()
    self.m_HuoliView:UpdateHuoli()
    self:OnUpdateActivity()
end

function CLuaScheduleWnd:GetGuideGo(methodName)
    if not self.m_ContentPanel then
        return nil
    end
    local scrollView=self.m_ContentPanel:GetComponent(typeof(UIScrollView))
    scrollView.mCalculatedBounds=false
    if methodName=="GetJuqingButton" then
        --剧情
        if self.m_JuQingItem then
            local btn=FindChild(self.m_JuQingItem.transform,"Btn").gameObject

            CUICommonDef.SetFullyVisible(self.m_JuQingItem, scrollView)
            return btn
        end
    elseif methodName=="GetShimenButton" then
        --师门
        if self.m_ShiMenItem then
            local btn=FindChild(self.m_ShiMenItem.transform,"Btn").gameObject

            if btn.activeInHierarchy then--如果参加按钮不显示
                CUICommonDef.SetFullyVisible(self.m_ShiMenItem, scrollView)
                return btn
            else
                CGuideMgr.Inst:EndCurrentPhase()
                return nil
            end
        end
    elseif methodName=="GetYitiaolongButton" then
        --一条龙
        if self.m_YiTiaoLongItem then
            local btn=FindChild(self.m_YiTiaoLongItem.transform,"Btn").gameObject
            if btn.activeInHierarchy then
                CUICommonDef.SetFullyVisible(self.m_YiTiaoLongItem, scrollView)
                return btn
            else
                CGuideMgr.Inst:EndCurrentPhase()
            end
        end
    elseif methodName=="GetDailyTab" then
        return nil
    elseif methodName=="GetWorldEventButton" then
        local go= self.transform:Find("Anchor/Center/WorldEventRoot/fenghualu").gameObject
        if go.activeSelf then
            return go
        else
            CGuideMgr.Inst:EndCurrentPhase()
            return nil
        end
    elseif methodName=="GetHaoYiXingTabButton" then
        return self.m_HaoYiXingTabButton
    elseif methodName=="GetHaoYiXingButton" then
        if not self.m_HaoYiXingButton then return nil end
        local btn=FindChild(self.m_HaoYiXingButton.transform,"Btn").gameObject
        if btn.activeInHierarchy then
            CUICommonDef.SetFullyVisible(self.m_HaoYiXingButton, scrollView)
            return btn
        else
            CGuideMgr.Inst:EndCurrentPhase()
        end
    elseif methodName == "GetXinBaiFirstTask" then
        local grid = self.m_XinBaiPage:Find("TaskProgress/ScrollView/Grid")
        if grid.childCount > 0 then
            return grid:GetChild(0):Find("Tween/Normal").gameObject
        end
    end
    return nil
end


function CLuaScheduleWnd:IsLevelEnough(taskData)
    if self.m_HasFeiSheng then
        if taskData.GradeCheckFS1 and taskData.GradeCheckFS1.Length>0 then
            local gradeCheck=taskData.GradeCheckFS1[0]
            if self.m_XianshenLevel>=gradeCheck then
                return true
            end
        end
    else
        return self.m_MainPlayerLevel>=CLuaScheduleMgr:GetTaskCheckLevel(taskData)
    end
    return false
end

function CLuaScheduleWnd:InitItem(transform,info)
    self.m_ActivityId2Item[info.activityId] = {transform, info}

    local schedule = Task_Schedule.GetData(info.activityId)
    if not schedule then return end

    if schedule then
        if not self.m_JuQingItem and schedule.Type=="JuQing" then
            self.m_JuQingItem=transform.gameObject
            self.m_JuQingInfo=info
        elseif not self.m_YiTiaoLongItem and schedule.TaskName==LocalString.GetString("一条龙") then
            self.m_YiTiaoLongItem=transform.gameObject
        elseif not self.m_ShiMenItem and (schedule.TaskName==LocalString.GetString("师门任务") or schedule.TaskName == LocalString.GetString("师门课业")) then
            self.m_ShiMenItem=transform.gameObject
        elseif not self.m_HaoYiXingButton and string.match(schedule.TaskName, LocalString.GetString("红尘册页")) then
            self.m_HaoYiXingButton=transform.gameObject
        end
    end
    UIEventListener.Get(transform:Find("Sprite").gameObject).onClick=DelegateFactory.VoidDelegate(function(p)
        CLuaScheduleMgr.selectedScheduleInfo=info
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end)

    local countLabel = transform:Find("Content/CountLabel"):GetComponent(typeof(UILabel))
    local huoliLabel = transform:Find("Content/HuoliLabel"):GetComponent(typeof(UILabel))
    local nameLabel = transform:Find("Content/NameLabel"):GetComponent(typeof(UILabel))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local btn = transform:Find("Content/Btn").gameObject
    local alertSprite = transform:Find("Content/Alert"):GetComponent(typeof(UISprite))
    local btnLabel = transform:Find("Content/Btn/Label"):GetComponent(typeof(UILabel))
    local levelRequireLabel = transform:Find("Content/LevelRequireLabel"):GetComponent(typeof(UILabel))
    local finishSprite = transform:Find("Content/FinishSprite"):GetComponent(typeof(UISprite))
    finishSprite.gameObject:SetActive(false)
    local contentNode = transform:Find("Content").gameObject

    local finishedTimes=info.FinishedTimes--每日完成次数
    local totalTimes=CLuaScheduleMgr.GetDailyTimes(schedule)--每日次数上限
    local restrictTimes=CScheduleMgr.GetRestrictTimes(info.activityId, info.taskId)--最大次数

    nameLabel.text = schedule.TaskName
    icon.material = nil
    --图标
    if schedule.Reward ~= nil and schedule.Reward.Length > 0 then
        local id = schedule.Reward[0]
        local template = Item_Item.GetData(id)
        if template ~= nil then
            icon:LoadMaterial(template.Icon)
        else
            local equipTemplate = EquipmentTemplate_Equip.GetData(id)
            if equipTemplate ~= nil then
                icon:LoadMaterial(equipTemplate.Icon)
            end
        end
    end

    if schedule.ButtonType == 0 then
        btnLabel.text = LocalString.GetString("参加")
    elseif schedule.ButtonType == 1 then
        btnLabel.text = LocalString.GetString("领取")
    elseif schedule.ButtonType == 2 then
        btnLabel.text = LocalString.GetString("查看")
    end

    local settingBtn = transform:Find("SettingButton").gameObject
    settingBtn:SetActive(false)

    if totalTimes == 0 or schedule.Type == "NewBieSchoolTask" then--没有次数
        countLabel.enabled = false
        huoliLabel.text = ""
    else
        countLabel.enabled = true

        if schedule.Type == "ShenYao" then -- 蜃妖特殊处理
            if self.m_ShenYaoTimesReceived then
                settingBtn:SetActive(true)
                UIEventListener.Get(settingBtn).onClick = DelegateFactory.VoidDelegate(function (go)
                    Gac2Gas.ShenYao_QueryRejectLevel()
                end)
                countLabel.text = SafeStringFormat3(LocalString.GetString("奖励 %d"), LuaXinBaiLianDongMgr.m_ShenYaoMaxRewardTimes - LuaXinBaiLianDongMgr.m_ShenYaoRewardTimes)
            end
        elseif schedule.Type=="JuQing" then
            countLabel.text = SafeStringFormat3(LocalString.GetString("次数 %d/%d"),finishedTimes, totalTimes+CLuaScheduleMgr.m_WuHuoQiQinCount)
        else
            countLabel.text =SafeStringFormat3(LocalString.GetString("次数 %d/%d"),finishedTimes, totalTimes)
        end
        --活力显示
        if schedule.Type == "ShenYao" then -- 蜃妖特殊处理
            if self.m_ShenYaoTimesReceived then
                huoliLabel.text = SafeStringFormat3(LocalString.GetString("次数 %d"), LuaXinBaiLianDongMgr.m_ShenYaoMaxJoinTimes - LuaXinBaiLianDongMgr.m_ShenYaoJoinTimes)
            end
        elseif not CLuaScheduleMgr.NeedShowHuoli(schedule) then
            huoliLabel.text =nil-- LocalString.GetString("活力 -/-")
        else
            local curHuoli =CLuaScheduleMgr.GetHuoli(schedule.HuoLi)
            local huoli = math.min(totalTimes, finishedTimes) * curHuoli
            huoliLabel.text = SafeStringFormat3(LocalString.GetString("活力 %d/%d"), huoli, CLuaScheduleMgr.GetDailyTimes(schedule) * curHuoli)
        end
    end
    local specialActivityIds = {[42000401] = true,[42000402] = true}
    --红点提示
    alertSprite.enabled = false
    if specialActivityIds[info.activityId] then
        alertSprite.enabled = finishedTimes == 0 and LuaActivityRedDotMgr:IsRedDot(info.activityId, LuaEnumRedDotType.Schedule)
    else
        --info.lastingTime>0
        alertSprite.enabled = LuaActivityRedDotMgr:IsRedDot(info.activityId, LuaEnumRedDotType.Schedule)
    end

    levelRequireLabel.gameObject:SetActive(false)
    btn:SetActive(true)

    CUICommonDef.SetActive(contentNode, true, true)

    if schedule.Type == "ShenYao" and self.m_ShenYaoTimesReceived and LuaXinBaiLianDongMgr:IsShenYaoFinished() then
        finishSprite.gameObject:SetActive(true)--完成
        CUICommonDef.SetActive(contentNode, false, true)
        btn:SetActive(false)
    elseif schedule.Type ~= "ShenYao" and totalTimes>0 and finishedTimes >= restrictTimes then
        -- ret=false--已完成次数
        finishSprite.gameObject:SetActive(true)--完成
        CUICommonDef.SetActive(contentNode, false, true)
        btn:SetActive(false)
    elseif schedule.ID == 42000500 then
        -- 拼图玩法特殊处理
        if not self.m_PinTuRequest then
            self.m_PinTuRequest = true
            Gac2Gas.QueryGuildPinTuProgress()
        elseif self.m_PinTuFininsh then
            finishSprite.gameObject:SetActive(true)--完成
            CUICommonDef.SetActive(contentNode, false, true)
            btn:SetActive(false)
            alertSprite.enabled = false
        end
    else
        local taskTemplate=Task_Task.GetData(info.taskId)
        
        if self:IsLevelEnough(taskTemplate) then
            if CLuaScheduleMgr.IsInProgress(info.taskId) then
                levelRequireLabel.text = LocalString.GetString("已接")--进行中 不能参加
                levelRequireLabel.gameObject:SetActive(true)
                btn:SetActive(false)
            else
                if schedule.NoJoinButton>0 then
                    -- ret=false
                    levelRequireLabel.gameObject:SetActive(false)
                    btn:SetActive(false)
                else
                    if info.lastingTime>0 then--限时
                        local now=CServerTimeMgr.Inst:GetZone8Time()
                        local nowTime=(now.Hour*60+now.Minute)*60+now.Second
                        local startTime=(info.hour * 60 + info.minute) * 60 + CScheduleMgr.DelayTriggerSecond
                        local endTime=(info.hour * 60 + info.minute) * 60 + info.lastingTime*60
                        if nowTime> startTime and nowTime<=endTime then
                            --
                            levelRequireLabel.gameObject:SetActive(false)
                        elseif nowTime<startTime then--没开始
                            btn:SetActive(false)
                            levelRequireLabel.gameObject:SetActive(true)
                            levelRequireLabel.text = SafeStringFormat3("%02d:%02d", info.hour, info.minute)
                        elseif nowTime>endTime then--已结束
                            btn:SetActive(false)
                            levelRequireLabel.gameObject:SetActive(true)
                            levelRequireLabel.text = LocalString.GetString("已结束")
                            CUICommonDef.SetActive(contentNode, false, true)
                        end
                    end
                end
            end
        else
            --等级不够
            levelRequireLabel.text = SafeStringFormat3(LocalString.GetString("%d级开启"), CLuaScheduleMgr:GetTaskCheckLevel(taskTemplate))
            levelRequireLabel.gameObject:SetActive(true)
            btn:SetActive(false)
        end
    end

    UIEventListener.Get(btn).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnClickCanJiaButton(transform,info)
    end)
end

function CLuaScheduleWnd:OnClickCanJiaButton(transform, info)
    local activityId = info.activityId
    --跨服
    if (not CLuaScheduleMgr.m_CrossServerActivityIds[activityId] and not LuaGuildTerritorialWarsMgr:IsPeripheryPlay()) and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer then
        g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
        return
    end
    local schedule = Task_Schedule.GetData(activityId)
    if not schedule then return end

    local isXianShi=schedule.TabType==2

    --限时活动要检查 
    if isXianShi and CLuaScheduleMgr.m_TaskCheck and not CTaskMgr.Inst:CheckTaskTime(info.taskId) then
        g_MessageMgr:ShowMessage("GAMEPLAY_NOT_OPEN")
        return
    end

    CLuaScheduleMgr.TrackSchedule(info)

    if transform then
        --红点逻辑
        local alertSprite = transform:Find("Content/Alert"):GetComponent(typeof(UISprite))
        alertSprite.enabled = false
        if activityId == 42000401 then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.InviteNpcAlert)
        end
    end


    CScheduleMgr.Inst:SetAlertState(activityId, CScheduleMgr.EnumAlertState.Clicked, false)
    LuaActivityRedDotMgr:OnRedDotClicked(activityId, LuaEnumRedDotType.Schedule)
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)

    --引导
    if transform then
        local messager = transform:GetComponent(typeof(CGuideMessager)) -- CommonDefs.GetComponent_GameObject_Type(self.gameObject, typeof(CGuideMessager))
        if messager ~= nil then
            messager:Click()
        end
    end

    CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
end

function CLuaScheduleWnd:OnQueryWuHuoQiQinCount(wuHuoQiQinCount)
    if self.m_JuQingItem then
        local countLabel = self.m_JuQingItem.transform:Find("Content/CountLabel"):GetComponent(typeof(UILabel))
        countLabel.text = SafeStringFormat3(LocalString.GetString("次数 %d/%d"),
                                        self.m_JuQingInfo.FinishedTimes, 
                                        self.m_JuQingInfo.TotalTimes+wuHuoQiQinCount)
    end
end

function CLuaScheduleWnd:InitWorldEvent()
    local worldEventRoot = FindChild(self.transform,"WorldEventRoot")--:GetComponent(typeof(UIWidget))

    local fenghualu = worldEventRoot:Find("fenghualu").gameObject
    local shimen = worldEventRoot:Find("shimen").gameObject
    local liandanlu = worldEventRoot:Find("liandanlu").gameObject
    if IsShiMenMapChanged() then
        shimen:SetActive(true)
        fenghualu:SetActive(false)
        UIEventListener.Get(shimen).onClick = DelegateFactory.VoidDelegate(function(go)
            CUIManager.ShowUI(CLuaUIResources.SanJieFengHuaLuEntryWnd)
        end)
        return 
    end
    --2022周年庆 葫芦娃丹炉战令入口
    if IsHuLuWaLianDanLuBtnShow() then
        shimen:SetActive(false)
        fenghualu:SetActive(false)
        liandanlu:SetActive(false)
        local fx = liandanlu.transform:Find("NormalFx"):GetComponent(typeof(CUIFx))
        fx:LoadFx("fx/ui/prefab/UI_zhounianhuijuan_yan.prefab")
        UIEventListener.Get(liandanlu).onClick = DelegateFactory.VoidDelegate(function(go)
            LuaHuLuWa2022Mgr.OpenLianDanLuWnd()
        end)
        return 
    end
    --#169174 【三界风华录】关闭三界风华录入口 【2022.4.28投放】4.14Build版本
    if false then
        shimen:SetActive(false)
        fenghualu:SetActive(true)
        UIEventListener.Get(fenghualu).onClick = DelegateFactory.VoidDelegate(function(go)
            LuaWorldEventMgr2021.ShowFengHuaLu()
        end)
    end
end

function CLuaScheduleWnd:OnDestroy()
    CLuaScheduleMgr.m_WuHuoQiQinCount=0
    CLuaScheduleMgr.taskTabType = 0
    if self.m_WorldEventTick then
        UnRegisterTick(self.m_WorldEventTick)
    end
    if self.m_RollBannerTick then
        UnRegisterTick(self.m_RollBannerTick)
        self.m_RollBannerTick = nil
    end
    CUIManager.CloseUI(CLuaUIResources.ShenYaoSettingWnd)
end
-- return CLuaScheduleWnd

function CLuaScheduleWnd:CheckTaskState(taskId)
    return CommonDefs.ListContains_LuaCall(CClientMainPlayer.Inst.TaskProp.AcceptableTaskList, taskId)
end

function CLuaScheduleWnd:OnUpdateTask()
    CLuaScheduleMgr.BuildInfos()
    self:ProcessLeft()
    self:ShowTab()
end