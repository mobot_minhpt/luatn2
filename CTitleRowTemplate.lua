-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTitleMgr = import "L10.Game.CTitleMgr"
local CTitleRowTemplate = import "L10.UI.CTitleRowTemplate"
local Title_Title = import "L10.Game.Title_Title"
local UInt32 = import "System.UInt32"
CTitleRowTemplate.m_Init_CS2LuaHook = function (this, Id, name) 

    this.title = Title_Title.GetData(Id)
    if this.title == nil then
        return
    end
    this.nameLabel.text = name
    this.TitleName = name
    this.displaySprite.enabled = CClientMainPlayer.Inst.PlayProp.ShowTitleId == Id
    this.equipSprite.enabled = CClientMainPlayer.Inst.PlayProp.AttrTitleId == Id
    if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.Titles, typeof(UInt32), Id) then
        this.nameLabel.color = Color.gray
    else
        this.nameLabel.color = CTitleMgr.Inst:GetTitleColor(this.title)
    end

    local originWidth = 376
    if not System.String.IsNullOrEmpty(this.title.SmallIcon) then
        this.nameLabel.width = originWidth-70
        this.nameLabel.text = "    " .. name

        this.titleIcon.gameObject:SetActive(true)
        this.titleIcon.gameObject.transform.localPosition = Vector3(15- this.nameLabel.printedSize.x/2, 0, 0)
        this.titleIcon.spriteName = this.title.SmallIcon
    else
        this.nameLabel.width = originWidth
        this.titleIcon.gameObject:SetActive(false)
    end
end
