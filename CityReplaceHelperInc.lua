CCityReplaceHelper = class()
RegistClassMember(CCityReplaceHelper, "m_ServerType")
RegistClassMember(CCityReplaceHelper, "m_ReplaceFunctionTbl")
RegistClassMember(CCityReplaceHelper, "m_PostActionFunctionTbl")
RegistClassMember(CCityReplaceHelper, "m_BackupStateTbl")
RegistClassMember(CCityReplaceHelper, "m_AllReplaces")
RegistClassMember(CCityReplaceHelper, "m_FieldReplaceValueFormatter")

if GetServerType and GetServerType() ~= "gac" then
require "base_common/cityreplace/CityReplaceHelperDefine"
require "base_common/cityreplace/CityReplaceHelper"
else
require "base_common/cityreplace/CityReplaceHelperDefine"
require "base_common/cityreplace/CityReplaceHelper"
end
