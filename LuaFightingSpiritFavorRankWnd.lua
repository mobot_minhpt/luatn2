local LuaGameObject=import "LuaGameObject"
local Vector3 = import "UnityEngine.Vector3"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableItem=import "L10.UI.QnTableItem"

CLuaFightingSpiritFavorRankWnd = class()

RegistClassMember(CLuaFightingSpiritFavorRankWnd, "m_TableView")
RegistClassMember(CLuaFightingSpiritFavorRankWnd, "m_TableViewDataSource")

function CLuaFightingSpiritFavorRankWnd:Init()
	self.m_TableView = LuaGameObject.GetChildNoGC(self.transform, "TableView").tableView
	local function initItem(item, index)
		if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
		self:InitItem(item, index)
	end
	self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(0, initItem)
	self.m_TableView.m_DataSource = self.m_TableViewDataSource
	self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
	end)
	self.m_TableView:ReloadData(true, false)

	Gac2Gas.RequestDouHunPraiseRank()
end

function CLuaFightingSpiritFavorRankWnd:InitItem(item, index)
	local tf = item.transform
	local rankLabel = LuaGameObject.GetChildNoGC(tf, "NumLabel").label
	local rankSprite = LuaGameObject.GetChildNoGC(tf, "RankImage").sprite
	local serverNameLabel = LuaGameObject.GetChildNoGC(tf, "ServerLabel").label
	local nameLabel = LuaGameObject.GetChildNoGC(tf, "NameLabel").label
	local countLabel = LuaGameObject.GetChildNoGC(tf, "CountLabel").label
	local favorSprite = LuaGameObject.GetChildNoGC(tf, "FavorSprite").sprite
	local favorButton = LuaGameObject.GetChildNoGC(tf, "FavorButton").qnButton
	local buttonSprite = LuaGameObject.GetLuaGameObjectNoGC(favorButton.transform).sprite
	local fxGO = LuaGameObject.GetChildNoGC(favorButton.transform, "UI_aixin").gameObject

	local info = CLuaFightingSpiritMgr.m_FavorRankList[index + 1]
	local rank = info.rank

	local bg = item.transform:GetComponent(typeof(QnTableItem))
    if index %2 == 0 then
        bg.m_NormalSpirte = "common_bg_mission_background_n"
    else
        bg.m_NormalSpirte = "common_bg_mission_background_s"
    end

	rankLabel.text = ""
	rankSprite.spriteName = ""
	if rank==1 then
        rankSprite.spriteName="Rank_No.1"
        rankSprite.width=103
        rankSprite.height=70
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
        rankSprite.width=102
        rankSprite.height=56
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
        rankSprite.width=80
        rankSprite.height=45
    else
        rankLabel.text=tostring(rank)
    end

	if CClientMainPlayer.Inst and info.playerId == CClientMainPlayer.Inst.Id then
		nameLabel.color = Color.green
	else
		nameLabel.color = Color.white
	end
	if CClientMainPlayer.Inst and info.serverId == CClientMainPlayer.Inst:GetMyServerId() then
		serverNameLabel.color = Color.green
	else
		serverNameLabel.color = Color.white
	end

	--local serverInfo = CFightingSpiritMgr.Instance:GetServerInfoFromID(info.serverId)
	serverNameLabel.text = info.serverId
	nameLabel.text = info.name
	countLabel.text = tostring(info.favorCount)
	if info.bFavor then
		favorSprite.spriteName = "personalspacewnd_heart_2"
		buttonSprite.spriteName = "personalspacewnd_heart_2"
	else
		favorSprite.spriteName = "personalspacewnd_heart_1"
		buttonSprite.spriteName = "personalspacewnd_heart_1"
	end

	fxGO:SetActive(false)
	
	favorButton.OnClick = DelegateFactory.Action_QnButton(function(button)
		Gac2Gas.RequestDouHunPraisePlayer(info.playerId)
	end)
end

function CLuaFightingSpiritFavorRankWnd:OnSelectAtRow(row)
	local data = CLuaFightingSpiritMgr.m_FavorRankList[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
    end
end

function CLuaFightingSpiritFavorRankWnd:OnEnable()
	g_ScriptEvent:AddListener("FightingSpiritFavorRankResult", self, "OnFavorRankResult")
	g_ScriptEvent:AddListener("RequestDouHunPraisePlayerSuccess", self, "OnFavorPlayerSuccess")
end

function CLuaFightingSpiritFavorRankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("FightingSpiritFavorRankResult", self, "OnFavorRankResult")
	g_ScriptEvent:RemoveListener("RequestDouHunPraisePlayerSuccess", self, "OnFavorPlayerSuccess")
end

function CLuaFightingSpiritFavorRankWnd:OnFavorRankResult()
	self.m_TableViewDataSource.count = #CLuaFightingSpiritMgr.m_FavorRankList
	self.m_TableView:ReloadData(true, false)
end

function CLuaFightingSpiritFavorRankWnd:OnFavorPlayerSuccess(targetPlayerId, targetServerId)
	for i = 1, #CLuaFightingSpiritMgr.m_FavorRankList do
		local info = CLuaFightingSpiritMgr.m_FavorRankList[i]
		if info.playerId == targetPlayerId then
			local item = self.m_TableView:GetItemAtRow(i-1)
			if item then
				local tf = item.transform
				local countLabel = LuaGameObject.GetChildNoGC(tf, "CountLabel").label
				local favorSprite = LuaGameObject.GetChildNoGC(tf, "FavorSprite").sprite
				local favorButton = LuaGameObject.GetChildNoGC(tf, "FavorButton").qnButton
				local buttonSprite = LuaGameObject.GetLuaGameObjectNoGC(favorButton.transform).sprite
				local fxGO = LuaGameObject.GetChildNoGC(favorButton.transform, "UI_aixin").gameObject
				countLabel.text = tostring(info.favorCount)
				favorSprite.spriteName = "personalspacewnd_heart_2"
				buttonSprite.spriteName = "personalspacewnd_heart_2"
				fxGO:SetActive(true)
			end
		end
	end
end

return CLuaFightingSpiritFavorRankWnd
