-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CSelfAuctionShopItem = import "L10.UI.CSelfAuctionShopItem"
local LocalString = import "LocalString"
CSelfAuctionShopItem.m_SetData_CS2LuaHook = function (this, itemInfo) 
    this.itemInfo = itemInfo
    this.m_CurrentPriceGO:SetActive(false)
    if itemInfo == nil then
        this.EmptyRoot:SetActive(true)
        this.NormalRoot:SetActive(false)
        this.IconContainer:Clear()
        if this.m_IdentifyIcon ~= nil then
            this.m_IdentifyIcon.enabled = false
        end
    else
        this.EmptyRoot:SetActive(false)
        this.NormalRoot:SetActive(true)
        this.item = CAuctionMgr.Inst:GetItem(itemInfo.InstanceID)
        if this.item == nil then
            return
        end

        local FixPriceBGSprite = this.transform:Find("Normal/FixPriceBGSprite").gameObject
        -- 不同情况的layout
        if LuaAuctionMgr.NeedShowStartPrice(this.item.TemplateId) then
            this.m_CurrentPriceGO:SetActive(true)
            this.m_CurrentPriceLabel.text = tostring(itemInfo.Price)
            -- 显示竞拍价
            LuaUtils.SetLocalPosition(this.m_CurrentPriceGO.transform, 0, -44, 0)
            LuaUtils.SetLocalPosition(FixPriceBGSprite.transform, 60, 0, 0)
            --LuaUtils.SetLocalPosition(this.BuyPrice.transform, -118, 2, 0)
        else
            this.m_CurrentPriceGO:SetActive(false)
            -- 不显示竞拍价
            LuaUtils.SetLocalPosition(FixPriceBGSprite.transform, 60, -44, 0)
        end

        this.IconContainer:LoadMaterial(this.item.Icon)
        this.NameLabel.text = this.item.Name
        if this.item.IsItem then
            this.NameLabel.color = this.item.Item.Color
        elseif this.item.IsEquip then
            this.NameLabel.color = this.item.Equip.DisplayColor
        end
        this.BuyPrice.text = tostring(itemInfo.FixPrice)

        if itemInfo.isPublicity == 1 then
            this.statusTagSprite.spriteName = this.redTag
            this.statusTagLabel.text = LocalString.GetString("公示期")
        else
            this.statusTagSprite.spriteName = this.greenTag
            this.statusTagLabel.text = LocalString.GetString("已上架")
        end
        if this.m_IdentifyIcon ~= nil then
            this.m_IdentifyIcon.enabled = (CPlayerShopMgr.s_EnableShowEquipIdentifyInfo and this.item.IsEquip and this.item.Equip.IsIdentifiable)
        end
    end
end
CSelfAuctionShopItem.m_onIconClick_CS2LuaHook = function (this, go) 
    if this.item == nil then
        return
    end

    CItemInfoMgr.ShowLinkItemInfo(this.item, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
end
