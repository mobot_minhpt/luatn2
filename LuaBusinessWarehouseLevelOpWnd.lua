local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"

LuaBusinessWarehouseLevelOpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "LevelOp", "LevelOp", GameObject)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "UnenoughOp", "UnenoughOp", GameObject)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "OpTip", "OpTip", UILabel)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "CurLevelLab", "CurLevelLab", UILabel)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "StateLab", "StateLab", UILabel)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "CostLab", "CostLab", UILabel)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "UnenoughOpTip", "UnenoughOpTip", UILabel)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "OKBtn", "OKBtn", CButton)
RegistChildComponent(LuaBusinessWarehouseLevelOpWnd, "CancelBtn", "CancelBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessWarehouseLevelOpWnd, "m_IsBuyingItem")

function LuaBusinessWarehouseLevelOpWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOKBtnClick()
	end)

	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancelBtnClick()
	end)
end

function LuaBusinessWarehouseLevelOpWnd:Init()
    self.m_IsBuyingItem = false
    if LuaBusinessMgr.m_BusinessIslevelUp == 1 then
        self:InitLevelUp()
    else
        self:InitLevelDown()
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessWarehouseLevelOpWnd:InitLevelUp()
    self.m_IsBuyingItem = false
    local targetLevel = LuaBusinessMgr.m_CarriageLevel + 1
    local horseData = Business_Horse.GetData(targetLevel)
    self.LevelOp:SetActive(true)
    self.UnenoughOp:SetActive(false)

    self.CurLevelLab.text = horseData.HorseName
    self.StateLab.text = horseData.Warehouse
    self.CostLab.text = horseData.NeedMoney

    local lvUpItemInfo = horseData.RiseLvNeedGoods
    local lvUpGoodsId = lvUpItemInfo[0]
    local lvUpGoodsNum = lvUpItemInfo[1]
    local lvUpGoodsData = Business_Goods.GetData(lvUpGoodsId)
    self.OpTip.text = SafeStringFormat3(LocalString.GetString("花费%d个%s升级为%s?"), lvUpGoodsNum, lvUpGoodsData.Name, horseData.HorseName)
end

function LuaBusinessWarehouseLevelOpWnd:InitLevelDown()
    self.m_IsBuyingItem = false
    local targetLevel = LuaBusinessMgr.m_CarriageLevel - 1
    local horseData = Business_Horse.GetData(targetLevel)
    self.LevelOp:SetActive(true)
    self.UnenoughOp:SetActive(false)

    self.CurLevelLab.text = horseData.HorseName
    self.StateLab.text = horseData.Warehouse
    self.CostLab.text = horseData.NeedMoney

    self.OpTip.text = SafeStringFormat3(LocalString.GetString("是否降级为%s?"), horseData.HorseName)
end

function LuaBusinessWarehouseLevelOpWnd:InitBuyItem()
    self.m_IsBuyingItem = true
    self.LevelOp:SetActive(false)
    self.UnenoughOp:SetActive(true)

    local targetLevel = LuaBusinessMgr.m_CarriageLevel + 1
    local horseData = Business_Horse.GetData(targetLevel)
    local lvUpItemInfo = horseData.RiseLvNeedGoods
    local lvUpGoodsId = lvUpItemInfo[0]
    local lvUpGoodsNum = lvUpItemInfo[1]
    local lvUpGoodsData = Business_Goods.GetData(lvUpGoodsId)

    local ownGoodsNum = LuaBusinessMgr:GetOwnGoodsNum(lvUpGoodsId)
    local needBuyNum = lvUpGoodsNum - ownGoodsNum
    local needMoney = needBuyNum * LuaBusinessMgr.m_CurCityGoodsType2Price[lvUpGoodsId]
    self.UnenoughOpTip.text = g_MessageMgr:FormatMessage("BUSINESS_INTLEVELUP_ITEM_UNENOUGH",needMoney, needBuyNum, lvUpGoodsData.Name)
end

function LuaBusinessWarehouseLevelOpWnd:OnCancelBtnClick()
    CUIManager.CloseUI(CLuaUIResources.BusinessWarehouseLevelOpWnd)
end

function LuaBusinessWarehouseLevelOpWnd:OnOKBtnClick()
    if LuaBusinessMgr.m_BusinessIslevelUp == 1 then
        local targetLevel = LuaBusinessMgr.m_CarriageLevel + 1
        local horseData = Business_Horse.GetData(targetLevel)
        local lvUpItemInfo = horseData.RiseLvNeedGoods
        local lvUpGoodsId = lvUpItemInfo[0]
        local lvUpGoodsNum = lvUpItemInfo[1]
        local lvUpGoodsData = Business_Goods.GetData(lvUpGoodsId)
        local ownGoodsNum = LuaBusinessMgr:GetOwnGoodsNum(lvUpGoodsId)
        local needBuyNum = lvUpGoodsNum - ownGoodsNum

        if self.m_IsBuyingItem == true then
            -- 买道具
            Gac2Gas.TSRepoLevelChange(LuaBusinessMgr.m_CarriageLevel + 1)
            CUIManager.CloseUI(CLuaUIResources.BusinessWarehouseLevelOpWnd)
        else
            if needBuyNum > 0 then
                self:InitBuyItem()
            else
                -- 升级
                Gac2Gas.TSRepoLevelChange(LuaBusinessMgr.m_CarriageLevel + 1)
                CUIManager.CloseUI(CLuaUIResources.BusinessWarehouseLevelOpWnd)
            end
        end
    else
        Gac2Gas.TSRepoLevelChange(LuaBusinessMgr.m_CarriageLevel - 1)
        CUIManager.CloseUI(CLuaUIResources.BusinessWarehouseLevelOpWnd)
    end
end