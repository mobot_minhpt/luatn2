local DelegateFactory            = import "DelegateFactory"
local CPlayerInfoMgr             = import "CPlayerInfoMgr"
local EnumPlayerInfoContext      = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel                 = import "L10.Game.EChatPanel"
local AlignType                  = import "CPlayerInfoMgr+AlignType"
local MessageMgr                 = import "L10.Game.MessageMgr"
local PopupMenuItemData          = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr          = import "L10.UI.CPopupMenuInfoMgr"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local UILabelOverflow = import "UILabel+Overflow"

LuaHuaYiGongShangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaHuaYiGongShangWnd, "tableView")
RegistClassMember(LuaHuaYiGongShangWnd, "mainPlayerInfo")
RegistClassMember(LuaHuaYiGongShangWnd, "mainPlayerRankNum")
RegistClassMember(LuaHuaYiGongShangWnd, "mainPlayerWorkName")
RegistClassMember(LuaHuaYiGongShangWnd, "mainPlayerName")
RegistClassMember(LuaHuaYiGongShangWnd, "mainPlayerPopularity")
RegistClassMember(LuaHuaYiGongShangWnd, "mainPlayerMale")
RegistClassMember(LuaHuaYiGongShangWnd, "mainPlayerFemale")
RegistClassMember(LuaHuaYiGongShangWnd, "leftTime")
RegistClassMember(LuaHuaYiGongShangWnd, "drawButton")
RegistClassMember(LuaHuaYiGongShangWnd, "drawButtonLabel")
RegistClassMember(LuaHuaYiGongShangWnd, "part")
RegistClassMember(LuaHuaYiGongShangWnd, "portrait")
RegistClassMember(LuaHuaYiGongShangWnd, "playerName")
RegistClassMember(LuaHuaYiGongShangWnd, "playerWorkName")
RegistClassMember(LuaHuaYiGongShangWnd, "playerPopularity")
RegistClassMember(LuaHuaYiGongShangWnd, "noPicture")
RegistClassMember(LuaHuaYiGongShangWnd, "playerInfo")
RegistClassMember(LuaHuaYiGongShangWnd, "playerRank")
RegistClassMember(LuaHuaYiGongShangWnd, "playerRankNum")
RegistClassMember(LuaHuaYiGongShangWnd, "playerRankSprite")
RegistClassMember(LuaHuaYiGongShangWnd, "playerRankNotOnList")
RegistClassMember(LuaHuaYiGongShangWnd, "voteButton")
RegistClassMember(LuaHuaYiGongShangWnd, "voteButtonLabel")
RegistClassMember(LuaHuaYiGongShangWnd, "shareButton")
RegistClassMember(LuaHuaYiGongShangWnd, "themeName")
RegistClassMember(LuaHuaYiGongShangWnd, "themeMonth")
RegistClassMember(LuaHuaYiGongShangWnd, "tipButton")
RegistClassMember(LuaHuaYiGongShangWnd, "fx")

RegistClassMember(LuaHuaYiGongShangWnd, "rankList")
RegistClassMember(LuaHuaYiGongShangWnd, "myRankInfo")
RegistClassMember(LuaHuaYiGongShangWnd, "rankSpriteTbl")
RegistClassMember(LuaHuaYiGongShangWnd, "curSelectInfo")
RegistClassMember(LuaHuaYiGongShangWnd, "isMainPlayerSelected")
RegistClassMember(LuaHuaYiGongShangWnd, "tick")
RegistClassMember(LuaHuaYiGongShangWnd, "selfPicData")
RegistClassMember(LuaHuaYiGongShangWnd, "isDrawQuery")
RegistClassMember(LuaHuaYiGongShangWnd, "normalX")
RegistClassMember(LuaHuaYiGongShangWnd, "needShowPopupMenu")

RegistClassMember(LuaHuaYiGongShangWnd, "needLoadedType") -- 完成加载部分数据后再显示图像
RegistClassMember(LuaHuaYiGongShangWnd, "curLoadedType") -- 当前已经加载的数据

function LuaHuaYiGongShangWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
    self:InitBefore()
end

function LuaHuaYiGongShangWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.tableView = anchor:Find("Rank/TableView"):GetComponent(typeof(QnTableView))
    self.mainPlayerInfo = anchor:Find("Rank/MainPlayerInfo")
    self.mainPlayerRankNum = self.mainPlayerInfo:Find("RankNum"):GetComponent(typeof(UILabel))
    self.mainPlayerWorkName = self.mainPlayerInfo:Find("WorkName"):GetComponent(typeof(UILabel))
    self.mainPlayerName = self.mainPlayerInfo:Find("Name"):GetComponent(typeof(UILabel))
    self.mainPlayerPopularity = self.mainPlayerInfo:Find("Popularity"):GetComponent(typeof(UILabel))
    self.mainPlayerMale = self.mainPlayerInfo:Find("Bg/Male").gameObject
    self.mainPlayerFemale = self.mainPlayerInfo:Find("Bg/Female").gameObject
    self.drawButton = anchor:Find("Rank/DrawButton").gameObject
    self.drawButtonLabel = self.drawButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.leftTime = anchor:Find("Rank/LeftTime"):GetComponent(typeof(UILabel))

    self.part = anchor:Find("Present/Part").gameObject
    self.portrait = anchor:Find("Present/Portrait").gameObject
    self.playerInfo = anchor:Find("Present/PlayerInfo")
    self.playerWorkName = self.playerInfo:Find("WorkName"):GetComponent(typeof(UILabel))
    self.playerName = self.playerInfo:Find("Name"):GetComponent(typeof(UILabel))
    self.playerPopularity = self.playerInfo:Find("Popularity"):GetComponent(typeof(UILabel))
    self.noPicture = self.playerInfo:Find("NoPicture").gameObject
    self.voteButton = anchor:Find("Present/VoteButton").gameObject
    self.voteButtonLabel = self.voteButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.shareButton = anchor:Find("Present/ShareButton").gameObject
    self.playerRank = anchor:Find("Present/Rank")
    self.playerRankNum = self.playerRank:Find("Num"):GetComponent(typeof(UILabel))
    self.playerRankSprite = self.playerRank:Find("Sprite"):GetComponent(typeof(UISprite))
    self.playerRankNotOnList = self.playerRank:Find("NotOnList").gameObject

    self.themeName = anchor:Find("Theme/Name"):GetComponent(typeof(UILabel))
    self.themeMonth = anchor:Find("Theme/Month"):GetComponent(typeof(UILabel))
    self.tipButton = anchor:Find("Theme/TipButton").gameObject

    self.fx = self.transform:Find("Bg/Fx"):GetComponent(typeof(CUIFx))
end

function LuaHuaYiGongShangWnd:InitEventListener()
    UIEventListener.Get(self.drawButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDrawButtonClick()
	end)

    UIEventListener.Get(self.voteButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnVoteButtonClick()
	end)

    UIEventListener.Get(self.shareButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    UIEventListener.Get(self.tipButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    UIEventListener.Get(self.mainPlayerInfo.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMainPlayerInfoClick()
	end)
end

function LuaHuaYiGongShangWnd:InitActive()
    self.mainPlayerInfo.gameObject:SetActive(false)

    self.shareButton:SetActive(false)
    self.voteButton:SetActive(false)
    self.playerInfo.gameObject:SetActive(false)
    self.playerRank.gameObject:SetActive(false)
    self.drawButton:SetActive(false)
end

function LuaHuaYiGongShangWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
    g_ScriptEvent:AddListener("XinShengHuaJiQueryRankPicResult", self, "OnQueryRankPicResult")
    g_ScriptEvent:AddListener("XinShengHuaJiVoteResult", self, "OnVoteResult")
    g_ScriptEvent:AddListener("XinShengHuaJiAddRankPicSuccess", self, "OnAddRankPicSuccess")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaHuaYiGongShangWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
    g_ScriptEvent:RemoveListener("XinShengHuaJiQueryRankPicResult", self, "OnQueryRankPicResult")
    g_ScriptEvent:RemoveListener("XinShengHuaJiVoteResult", self, "OnVoteResult")
    g_ScriptEvent:RemoveListener("XinShengHuaJiAddRankPicSuccess", self, "OnAddRankPicSuccess")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaHuaYiGongShangWnd:OnSeasonRank_QueryRankResult(rankType, seasonId, selfData, rankData)
    if rankType ~= EnumSeasonRankType.eXinShengHuaJi or seasonId ~= LuaWuMenHuaShiMgr.seasonInfo.id then return end

    local rawData = MsgPackImpl.unpack(selfData)
    if CommonDefs.IsDic(rawData) then
        CommonDefs.DictIterate(rawData,DelegateFactory.Action_object_object(function (key, value)
            self.myRankInfo[tostring(key)] = value
        end))
    end

    local myId = CClientMainPlayer.Inst.Id
    self.rankList = {}
    local rawData2 = MsgPackImpl.unpack(rankData)
    if rawData2.Count > 0 then
        for i = 1, rawData2.Count do
            local rawData3 = rawData2[i - 1]
            local dict = {}
            if CommonDefs.IsDic(rawData3) then
                CommonDefs.DictIterate(rawData3, DelegateFactory.Action_object_object(function (key, value)
                    dict[tostring(key)] = value
                end))
            end
            dict.rank = i

            if dict.Score and dict.Score > 0 then
                if dict.playerId and dict.playerId == myId then
                    self.myRankInfo.rank = i
                end
                table.insert(self.rankList, dict)
            end
        end
    end

    self.tableView:ReloadData(true, false)
    self:UpdateMainPlayerRank()
    self:UpdateSelectInfo()
    self:SetDrawButton()
end

function LuaHuaYiGongShangWnd:OnQueryRankPicResult(playerId, name, picData, score, title)
    if playerId == CClientMainPlayer.Inst.Id then
        self.selfPicData = picData
        if self.isDrawQuery then
            self.isDrawQuery = false
            LuaWuMenHuaShiMgr:OpenHuaYiGongShangDrawWnd(self.selfPicData)
            return
        end
    end
    self.isDrawQuery = false
    if playerId ~= self.curSelectInfo.playerId then return end

    self.portrait:SetActive(false)
    self.curLoadedType = {}
    LuaWuMenHuaShiMgr:GeneratePortraitWithFlip(self.portrait, self.part, picData, self.normalX, function (type)
        if not self.needLoadedType then return end

        for k, v in pairs(self.needLoadedType) do
            if v == type then
                self.curLoadedType[v] = true
                break
            end
        end

        for k, v in pairs(self.needLoadedType) do
            if not self.curLoadedType[v] then
                return
            end
        end

        self.portrait:SetActive(true)
    end)

    self.shareButton:SetActive(true)
    self.voteButton:SetActive(true)
    self.playerInfo.gameObject:SetActive(true)
    self:SetPlayerInfoActive(true, true, true, false)

    self.playerName.text = name
    self.playerWorkName.text = title
    self.playerName.transform:Find("Texture"):GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self:UpdatePlayerPopularity(score)
    self:UpdatePlayerRank()
end

function LuaHuaYiGongShangWnd:OnVoteResult(playerId, voteNum)
    if self.curSelectInfo.playerId ~= playerId then return end

    g_MessageMgr:ShowMessage("HUAYIGONGSHANG_VOTE_SUCCESS")
    self:UpdatePlayerPopularity(voteNum)
    self:UpdateVoteButton()
end

function LuaHuaYiGongShangWnd:OnAddRankPicSuccess()
    self.curSelectInfo = {playerId = CClientMainPlayer.Inst.Id, rank = 0}
    Gac2Gas.SeasonRank_QueryRank(EnumSeasonRankType.eXinShengHuaJi, LuaWuMenHuaShiMgr.seasonInfo.id)
end

function LuaHuaYiGongShangWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eXinShengHuaJi_VoteNum then
        self:UpdateVoteButton()
    end
end


function LuaHuaYiGongShangWnd:InitBefore()
    self:InitTheme()
    self:InitTableView()
    self.curSelectInfo = {}
    self:StartTick()
    self:UpdateVoteButton()
    self.normalX = self.portrait.transform.localPosition.x
    self.isMainPlayerSelected = false
    self.fx:LoadFx(g_UIFxPaths.HuaYiGongShangFxPath)

    self.needLoadedType = {200, 202, 211, 230}
end

function LuaHuaYiGongShangWnd:Init()
    Gac2Gas.SeasonRank_QueryRank(EnumSeasonRankType.eXinShengHuaJi, LuaWuMenHuaShiMgr.seasonInfo.id)

    local sharedPlayerId = LuaWuMenHuaShiMgr.sharedPlayerId
    local curPlayerId = self.curSelectInfo.playerId
    if sharedPlayerId and (not curPlayerId or sharedPlayerId ~= curPlayerId) then
        self.curSelectInfo = {playerId = sharedPlayerId, rank = 0}
    end

    self.isDrawQuery = false
    self.needShowPopupMenu = true
end

-- 初始化主题显示
function LuaHuaYiGongShangWnd:InitTheme()
    if CommonDefs.IS_VN_CLIENT then
        self.themeName.transform.localPosition = Vector3(-819, 184, 0)
        self.themeName.overflowMethod = UILabelOverflow.ResizeHeight
        self.themeName.width = 95

        self.themeMonth.transform.localPosition = Vector3(-740, 160, 0)
        self.themeMonth.overflowMethod = UILabelOverflow.ResizeHeight
        self.themeMonth.width = 56
    end
    
    self.themeName.text = LuaWuMenHuaShiMgr:GetThemeName()

    local Number2Chinese = {
        LocalString.GetString("一"),
        LocalString.GetString("二"),
        LocalString.GetString("三"),
        LocalString.GetString("四"),
        LocalString.GetString("五"),
        LocalString.GetString("六"),
        LocalString.GetString("七"),
        LocalString.GetString("八"),
        LocalString.GetString("九"),
        LocalString.GetString("十"),
        LocalString.GetString("十一"),
        LocalString.GetString("十二")
    }

    local month = Number2Chinese[math.floor(LuaWuMenHuaShiMgr.seasonInfo.id / 100) % 1000]
    self.themeMonth.text = SafeStringFormat3(LocalString.GetString("%s月主题"), month)
end

function LuaHuaYiGongShangWnd:InitTableView()
    self.myRankInfo = {}
    self.rankList = {}
    self.rankSpriteTbl = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankList
        end,
        function(item, index)
            self:InitItem(item, self.rankList[index + 1])
        end
    )

    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.rankList[row + 1]
        self.curSelectInfo = data
        self:SetMainPlayerState(false)
        self:QueryRankPic()
        if data and self.needShowPopupMenu then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3(1, 0, 0), AlignType.Right)
        end
        self.needShowPopupMenu = true
    end)
end

function LuaHuaYiGongShangWnd:InitItem(item, info)
    local tf = item.transform
    local rank = info.rank

    local rankNum = tf:Find("RankNum"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("RankSprite"):GetComponent(typeof(UISprite))
    local workName = tf:Find("WorkName"):GetComponent(typeof(UILabel))
    local name = tf:Find("Name"):GetComponent(typeof(UILabel))
    local popularity = tf:Find("Popularity"):GetComponent(typeof(UILabel))
    local male = tf:Find("Bg/Male").gameObject
    local female = tf:Find("Bg/Female").gameObject

    if rank >= 1 and rank <= 3 then
        rankSprite.spriteName = self.rankSpriteTbl[rank]
        rankSprite.gameObject:SetActive(true)
        rankNum.gameObject:SetActive(false)
    else
        rankNum.text = rank
        rankSprite.gameObject:SetActive(false)
        rankNum.gameObject:SetActive(true)
    end

    name.text = info.name
    local emptyName = LuaWuMenHuaShiMgr.allowCustomTitle and LocalString.GetString("无题") or LocalString.GetString("—")
    workName.text = System.String.IsNullOrEmpty(info.Title) and emptyName or info.Title
    popularity.text = info.Score
    male:SetActive(info.Gender == EnumGender_lua.Male)
    female:SetActive(info.Gender == EnumGender_lua.Female)
end

function LuaHuaYiGongShangWnd:UpdateMainPlayerRank()
    self.mainPlayerInfo.gameObject:SetActive(true)
    local rank = self.myRankInfo.rank and self.myRankInfo.rank or 0
    local score = self.myRankInfo.Score and self.myRankInfo.Score or -1
    local gender = self.myRankInfo.Gender and self.myRankInfo.Gender or EnumGender_lua.Undefined

    if self.myRankInfo.playerId then
        self.mainPlayerRankNum.text = (rank > 0 and score > 0) and rank or LocalString.GetString("未上榜")
        local emptyName = LuaWuMenHuaShiMgr.allowCustomTitle and LocalString.GetString("无题") or LocalString.GetString("—")
        self.mainPlayerWorkName.text = System.String.IsNullOrEmpty(self.myRankInfo.Title) and emptyName or self.myRankInfo.Title
    else
        self.mainPlayerRankNum.text = LocalString.GetString("尚未参与")
        self.mainPlayerWorkName.text = LocalString.GetString("—")
    end

    self.mainPlayerName.text = CClientMainPlayer.Inst.RealName
    self.mainPlayerPopularity.text = score >= 0 and score or LocalString.GetString("—")
    self.mainPlayerMale:SetActive(gender == EnumGender_lua.Male)
    self.mainPlayerFemale:SetActive(gender == EnumGender_lua.Female)
end

-- 更新选择信息
function LuaHuaYiGongShangWnd:UpdateSelectInfo()
    local preSelectId = self.curSelectInfo.playerId

    if preSelectId then
        if preSelectId == self.myRankInfo.playerId then
            self.curSelectInfo = self.myRankInfo
            self:SetMainPlayerState(true)
        else
            for id, data in pairs(self.rankList) do
                if data.playerId == preSelectId then
                    self.curSelectInfo = data
                    self.needShowPopupMenu = false
                    self.tableView:ScrollToRow(id - 1)
                    self.tableView:SetSelectRow(id - 1, true)
                    return
                end
            end
            self.tableView:SetSelectRow(-1, true)
            self:QueryRankPic()
        end
    else
        if self.myRankInfo.playerId then
            self:SetMainPlayerState(true)
        elseif #self.rankList > 0 then
            self.needShowPopupMenu = false
            self.tableView:SetSelectRow(0, true)
        else
            self:ShowNoPicture()
        end
    end
end

-- 查询图片
function LuaHuaYiGongShangWnd:QueryRankPic()
    local playerId = self.curSelectInfo.playerId
    if not playerId then return end

    Gac2Gas.XinShengHuaJi_QueryRankPic(playerId)
end

-- 设置玩家信息是否显示
function LuaHuaYiGongShangWnd:SetPlayerInfoActive(bName, bPopularity, bWorkName, bNoPicture)
    self.playerName.gameObject:SetActive(bName)
    self.playerWorkName.gameObject:SetActive(LuaWuMenHuaShiMgr.allowCustomTitle and bWorkName or false)
    self.playerPopularity.gameObject:SetActive(bPopularity)
    self.noPicture:SetActive(bNoPicture)
end

-- 设置玩家排名信息激活状态
function LuaHuaYiGongShangWnd:SetPlayerRankActive(bNum, bSprite, bNotOnList)
    self.playerRankNum.gameObject:SetActive(bNum)
    self.playerRankSprite.gameObject:SetActive(bSprite)
    self.playerRankNotOnList:SetActive(bNotOnList)
end

-- 显示小像剪影
function LuaHuaYiGongShangWnd:ShowNoPicture()
    self.playerInfo.gameObject:SetActive(true)
    self.playerRank.gameObject:SetActive(false)
    self:SetPlayerInfoActive(false, false, false, true)
end

-- 设置玩家自己信息的显示状态
function LuaHuaYiGongShangWnd:SetMainPlayerState(selected)
    if not selected and self.isMainPlayerSelected == selected then return end

    self.mainPlayerInfo:GetComponent(typeof(UISprite)).spriteName = selected and g_sprites.HighlightBgSprite or g_sprites.TransparentSprite

    self.isMainPlayerSelected = selected
    if selected then
        self.curSelectInfo = self.myRankInfo
        self.tableView:SetSelectRow(-1, true)
        self:QueryRankPic()
    end
end

-- 设置绘制按钮显示
function LuaHuaYiGongShangWnd:SetDrawButton()
    local drawSprite = self.drawButton.transform:GetComponent(typeof(UISprite))
    local voteSprite = self.voteButton.transform:GetComponent(typeof(UISprite))

    self.drawButton:SetActive(true)
    if self.myRankInfo.playerId then
        drawSprite.spriteName = g_sprites.GreenHandGiftWnd_BlueButtonName
        self.drawButtonLabel.text = LocalString.GetString("重新绘制小像")
        self.drawButtonLabel.color = NGUIText.ParseColor24("0E3254", 0)

        voteSprite.spriteName = g_sprites.GreenHandGiftWnd_OrangeButtonName
        self.voteButtonLabel.color = NGUIText.ParseColor24("351B01", 0)
    else
        drawSprite.spriteName = g_sprites.GreenHandGiftWnd_OrangeButtonName
        self.drawButtonLabel.text = LocalString.GetString("前往绘制小像")
        self.drawButtonLabel.color = NGUIText.ParseColor24("351B01", 0)

        voteSprite.spriteName = g_sprites.GreenHandGiftWnd_BlueButtonName
        self.voteButtonLabel.color = NGUIText.ParseColor24("0E3254", 0)
    end
end

-- 获取剩余投票数
function LuaHuaYiGongShangWnd:GetLeftVoteNum()
    if not CClientMainPlayer.Inst then return 0 end
    return CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXinShengHuaJi_VoteNum)
end

-- 更新投票按钮显示
function LuaHuaYiGongShangWnd:UpdateVoteButton()
    self.voteButtonLabel.text = SafeStringFormat3(LocalString.GetString("投票(剩%d)"), self:GetLeftVoteNum())
end

-- 更新排名显示
function LuaHuaYiGongShangWnd:UpdatePlayerRank()
    self.playerRank.gameObject:SetActive(true)
    local rank = self.curSelectInfo.rank
    if not rank or rank <= 0 or self.curSelectInfo.Score == 0 then
        self:SetPlayerRankActive(false, false, true)
        return
    end

    if rank >= 1 and rank <= 3 then
        self.playerRankSprite.spriteName = self.rankSpriteTbl[rank]
        self:SetPlayerRankActive(false, true, false)
    else
        self.playerRankNum.text = rank
        self:SetPlayerRankActive(true, false, false)
    end
end

-- 更新人气值
function LuaHuaYiGongShangWnd:UpdatePlayerPopularity(value)
    self.playerPopularity.text = value
    self.playerPopularity.transform:Find("Texture"):GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
end

function LuaHuaYiGongShangWnd:StartTick()
    self:ClearTick()
    self:UpdateLeftTime()
    self.tick = RegisterTick(function()
        self:UpdateLeftTime()
    end, 1000)
end

-- 剩余时间
function LuaHuaYiGongShangWnd:UpdateLeftTime()
    local leftTime = math.floor(LuaWuMenHuaShiMgr.seasonInfo.endTime - LuaWuMenHuaShiMgr:GetCurrentTime())
    if leftTime < 0 then
        self:ClearTick()
        return
    end

    local hour = leftTime / 3600
    if hour > 24 then
        self.leftTime.text = SafeStringFormat3(LocalString.GetString("%s天"), math.ceil(hour / 24))
    else
        self.leftTime.text = SafeStringFormat3(LocalString.GetString("%s小时"), math.ceil(hour))
    end
end

function LuaHuaYiGongShangWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
    end
end

function LuaHuaYiGongShangWnd:OnDestroy()
    self:ClearTick()
end

-- 分享
function LuaHuaYiGongShangWnd:DoShare(getShareTextFunc, parent, alignType, playerId)
    if not getShareTextFunc then return end
    if not CClientMainPlayer.Inst then return end

    local tbl = {}
    local shareToWorld = self:GetShareToWorldMenu(playerId)
    table.insert(tbl, shareToWorld)

    local shareToGuild = LuaInGameShareMgr.GetShareToGuildMenu(getShareTextFunc, nil)
    table.insert(tbl, shareToGuild)

    local shareToTeam = LuaInGameShareMgr.GetShareToTeamMenu(getShareTextFunc, nil)
    table.insert(tbl, shareToTeam)

    local shareToFriend = LuaInGameShareMgr.GetShareToFriendMenu(getShareTextFunc, nil)
    table.insert(tbl, shareToFriend)

    local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))

    CPopupMenuInfoMgr.ShowPopupMenu(array, parent, alignType, 1, nil, 600, true, 320)
end

-- 分享到世界频道，限制每天的分享次数
function LuaHuaYiGongShangWnd:GetShareToWorldMenu(playerId)
    local timesLimit = WuMenHuaShi_XinShengHuaJiSetting.GetData().RankPicShareTimesLimit
    local shareTimes = math.max(timesLimit - CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXinShengHuaJi_RankPicShareTimes), 0)
    local str = SafeStringFormat3(LocalString.GetString("至世界频道(%d次)"), shareTimes)

    return PopupMenuItemData(str, DelegateFactory.Action_int(function (index)
        if not LuaInGameShareMgr.CanShare() then
            return
        end
        Gac2Gas.XinShengHuaJi_RequestShareRankPic(playerId)
    end), false, nil)
end

--@region UIEvent

function LuaHuaYiGongShangWnd:OnDrawButtonClick()
    if not CClientMainPlayer.Inst then return end

    local taskProperty = CClientMainPlayer.Inst.TaskProp
    local taskId = WuMenHuaShi_XinShengHuaJiSetting.GetData().XinShengHuaJiTaskId
    if not taskProperty:IsMainPlayerTaskFinished(taskId) then
        g_MessageMgr:ShowMessage("XinShengHuaJi_AddPicture_Need_Task")
        return
    end

    local joinFlag = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXinShengHuaJi_JoinFlag)
    if joinFlag == 1 then
        if self.selfPicData then
            LuaWuMenHuaShiMgr:OpenHuaYiGongShangDrawWnd(self.selfPicData)
        else
            self.isDrawQuery = true
            Gac2Gas.XinShengHuaJi_QueryRankPic(CClientMainPlayer.Inst.Id)
        end
    else
        LuaWuMenHuaShiMgr:OpenHuaYiGongShangDrawWnd()
    end
end

function LuaHuaYiGongShangWnd:OnVoteButtonClick()
    if not self.curSelectInfo.playerId then return end

    self:UpdateVoteButton()
    local leftTimes = self:GetLeftVoteNum()
    if leftTimes == 0 then
        g_MessageMgr:ShowMessage("XinShengHuaJi_VoteNum_Not_Enough")
        return
    end

    local playerId = self.curSelectInfo.playerId
    CLuaNumberInputMgr.ShowNumInputBox(1, leftTimes, 1, function(count)
        Gac2Gas.XinShengHuaJi_Vote(playerId, count)
    end, LocalString.GetString("请输入投出的票数"), -1)
end

function LuaHuaYiGongShangWnd:OnShareButtonClick()
    if not self.curSelectInfo.playerId then return end

    local formatStr = MessageMgr.Inst:GetMessageFormat("HUAYIGONGSHANG_SHARE_LINK", false)
    local msg = System.String.Format(formatStr, tostring(self.curSelectInfo.playerId))
    self:DoShare(function (channel)
        return msg
    end, self.shareButton.transform, CPopupMenuInfoMgrAlignType.Right, tonumber(self.curSelectInfo.playerId))
end

function LuaHuaYiGongShangWnd:OnTipButtonClick()
    if CommonDefs.IS_VN_CLIENT then
        g_MessageMgr:ShowMessage("HUAYIGONGSHANG_TIP_VN")
    else
        g_MessageMgr:ShowMessage("HUAYIGONGSHANG_TIP")
    end
end

function LuaHuaYiGongShangWnd:OnMainPlayerInfoClick()
    if not self.myRankInfo.playerId then
        g_MessageMgr:ShowMessage("HUAYIGONGSHANG_HAVE_NOT_DRAWN")
        return
    end

    if self.isMainPlayerSelected then return end
    self:SetMainPlayerState(true)
end

--@endregion UIEvent


