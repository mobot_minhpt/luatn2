local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local UITexture = import "UITexture"

CLuaRanFaRecipeChooseWnd = class()
CLuaRanFaRecipeChooseWnd.Path = "ui/ranfa/LuaRanFaRecipeChooseWnd"

function CLuaRanFaRecipeChooseWnd:Init()
    self.transform:Find("Offset/TopLabel"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("RanFaJi_ColorSystem_Choose")
    local indexTbl = {0, 0, 0, 0, 0, 0}
    RanFaJi_RanFaJi.Foreach(function(k, v)
        local type = v.ColorSystem
        indexTbl[type] = indexTbl[type] + 1
        if indexTbl[type] == 1 then
            self.transform:Find("Offset/RadioBox/"..type.."/Label"):GetComponent(typeof(UILabel)).text = v.ColorSystemName
        end
        self.transform:Find("Offset/RadioBox/"..type.."/"..indexTbl[type]):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(v.ColorRGB, 0)
    end)
    UIEventListener.Get(self.transform:Find("Offset/GetBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ()
        local index = self.transform:Find("Offset/RadioBox"):GetComponent(typeof(QnRadioBox)).CurrentIndex
        Gac2Gas.RanFaJiColorSystemChooseComplete(CLuaRanFaMgr.m_ChooseItemInfo[1], CLuaRanFaMgr.m_ChooseItemInfo[2], CLuaRanFaMgr.m_ChooseItemInfo[3], index + 1)
        CUIManager.CloseUI(CLuaUIResources.RanFaRecipeChooseWnd)
    end)
    UIEventListener.Get(self.transform:Find("Offset/PreviewBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ()
        --CUIManager.ShowUI(CLuaUIResources.RanFaMainWnd)
        CLuaRanFaMgr:RequestSyncRanFaJiPlayData(EnumSyncRanFaJiType.eOpenMainWndRecipeTab)
        CUIManager.CloseUI(CLuaUIResources.RanFaRecipeChooseWnd)
    end)
end
