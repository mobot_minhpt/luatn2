require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local CButton = import "L10.UI.CButton"
local MsgPackImpl = import "MsgPackImpl"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local UISprite = import "UISprite"
local CCommonSelector = import "L10.UI.CCommonSelector"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CUITexture = import "L10.UI.CUITexture"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaWorldCupJingCaiView=class()

RegistClassMember(CLuaWorldCupJingCaiView, "m_ScorllView")
RegistClassMember(CLuaWorldCupJingCaiView, "m_Table")
RegistClassMember(CLuaWorldCupJingCaiView, "m_ItemTemplate")

RegistClassMember(CLuaWorldCupJingCaiView, "m_TotalSilver")
RegistClassMember(CLuaWorldCupJingCaiView, "m_JingCaiQiShu")
RegistClassMember(CLuaWorldCupJingCaiView, "m_JingCaiTime")

RegistClassMember(CLuaWorldCupJingCaiView, "m_TouZhuView1")
RegistClassMember(CLuaWorldCupJingCaiView, "m_TouZhuView2")
RegistClassMember(CLuaWorldCupJingCaiView, "m_TouZhuResultLabel")

RegistClassMember(CLuaWorldCupJingCaiView, "m_TouZhuButton")
RegistClassMember(CLuaWorldCupJingCaiView, "m_TouZhuLabel")
RegistClassMember(CLuaWorldCupJingCaiView, "m_TipsButton")
RegistClassMember(CLuaWorldCupJingCaiView, "m_AddSubInputButton")
RegistClassMember(CLuaWorldCupJingCaiView, "m_CostMoneyLabel")
RegistClassMember(CLuaWorldCupJingCaiView, "m_OwnMoneyLabel")

RegistClassMember(CLuaWorldCupJingCaiView, "m_SelectDataCSharpList")
RegistClassMember(CLuaWorldCupJingCaiView, "m_SelectDataCSharpList2")

RegistClassMember(CLuaWorldCupJingCaiView, "m_Pid")
RegistClassMember(CLuaWorldCupJingCaiView, "m_MyValue")
RegistClassMember(CLuaWorldCupJingCaiView, "m_TouZhuNum")

RegistClassMember(CLuaWorldCupJingCaiView, "m_SelectResultTbl")

function CLuaWorldCupJingCaiView:Init(tf)
    self.m_ScorllView = tf.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = tf.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_ItemTemplate = tf.transform:Find("ScrollView/PlayInfoItem").gameObject
    self.m_ItemTemplate:SetActive(false)

    self.m_TotalSilver = tf.transform:Find("TotalSilver"):GetComponent(typeof(UILabel))
    self.m_JingCaiQiShu = tf.transform:Find("JingCaiQiShu"):GetComponent(typeof(UILabel))
    self.m_JingCaiTime = tf.transform:Find("JingCaiTime"):GetComponent(typeof(UILabel))

    self.m_TouZhuView1 = tf.transform:Find("TouZhuArea/TouZhuView1").gameObject
    self.m_TouZhuView2 = tf.transform:Find("TouZhuArea/TouZhuView2").gameObject
    self.m_TouZhuResultLabel = tf.transform:Find("TouZhuArea/TouZhuView2/Label"):GetComponent(typeof(UILabel))

    self.m_TouZhuButton = tf.transform:Find("TouZhuArea/TouZhuView1/TouZhuButton"):GetComponent(typeof(CButton))
    self.m_TouZhuLabel = tf.transform:Find("TouZhuArea/TouZhuView1/TouZhuButton/Label"):GetComponent(typeof(UILabel))
    self.m_TipsButton = tf.transform:Find("TouZhuArea/TipsButton"):GetComponent(typeof(CButton))
    self.m_AddSubInputButton = tf.transform:Find("TouZhuArea/TouZhuView1/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_CostMoneyLabel = tf.transform:Find("TouZhuArea/TouZhuView1/QnCostAndOwnMoney/Cost"):GetComponent(typeof(UILabel))
    self.m_OwnMoneyLabel = tf.transform:Find("TouZhuArea/TouZhuView1/QnCostAndOwnMoney/Own"):GetComponent(typeof(UILabel))

    self.m_TouZhuNum = 1

    self.m_SelectResultTbl = {}
    local List_String = MakeGenericClass(List,cs_string)
    self.m_SelectDataCSharpList = Table2List(CLuaWorldCupMgr.m_SelectDataTbl, List_String)
    self.m_SelectDataCSharpList2 = Table2List(CLuaWorldCupMgr.m_SelectDataTbl2, List_String)

    -- 竞彩奖励小红点
    Gac2Gas.CheckWorldCupJingCaiAwardReddot()

    -- 回调函数初始化的时候就绑定
    CommonDefs.AddOnClickListener(self.m_TouZhuButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnPlayerClickTouZhuButton()
    end), false)

    CommonDefs.AddOnClickListener(self.m_TipsButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        g_MessageMgr:ShowMessage("WorldCup_JingCai_Tips")
    end), false)

    self.m_AddSubInputButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
        self.m_CostMoneyLabel.text = tostring(CLuaWorldCupMgr.m_JingCaiYinLiang * value)
    end)
    self.m_CostMoneyLabel.text = tostring(CLuaWorldCupMgr.m_JingCaiYinLiang)

    self.m_AddSubInputButton.m_MaxValue = CLuaWorldCupMgr.m_MaxTouZhuNum
end

function CLuaWorldCupJingCaiView:Refresh(pid, totalNum, resultData, myValue, serverValue, myNum)
    self.m_Pid = pid
    self.m_MyValue = myValue

    -- 顶部
    self.m_TotalSilver.text = SafeStringFormat(LocalString.GetString("%s"), totalNum * CLuaWorldCupMgr.m_JingCaiYinLiang)
    self.m_JingCaiQiShu.text = SafeStringFormat(LocalString.GetString("第%s期竞彩"), pid)
    local JingCaiInfo = CLuaWorldCupMgr.GetJingCaiInfoById(pid)
    local BeginTime = string.sub(JingCaiInfo.BeginTime, 6)
    local EndTime = string.sub(JingCaiInfo.EndTime, 6)
    self.m_JingCaiTime.text = SafeStringFormat(LocalString.GetString("%s - %s"), BeginTime, EndTime)
    local t1 = CLuaWorldCupMgr.GetTimeStampForPlay(JingCaiInfo.BeginTime)
    local t2 = CLuaWorldCupMgr.GetTimeStampForPlay(JingCaiInfo.EndTime)
    local serverTime = CLuaWorldCupMgr.m_GMTime or CServerTimeMgr.Inst.timeStamp

    if serverTime >= t1 and serverTime <= t2 then
        self.m_JingCaiTime.color = Color.green
    else
        self.m_JingCaiTime.color = Color.red
    end

    local bFinish = true
    if serverTime <= t2 then
        bFinish = false
    end

    -- 清空内容
    Extensions.RemoveAllChildren(self.m_Table.transform)
    self.m_SelectResultTbl = {}

    local list = MsgPackImpl.unpack(resultData)
    local index = 1
    for i=0, list.Count-1, 6 do
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
        go:SetActive(true)

        local round = list[i]
        local tid1 = list[i+1]
        local tid2 = list[i+2]
        local score1 = list[i+3]
        local score2 = list[i+4]
        local dianqiu = list[i+5]

        local PlayInfo = CLuaWorldCupMgr.GetPlayInfoById(round)
        local PlayTime = go.transform:Find("PlayTime"):GetComponent(typeof(UILabel))
        PlayTime.text = LocalString.GetString(PlayInfo.Time)

        local PlayRound = go.transform:Find("PlayRound"):GetComponent(typeof(UILabel))
        PlayRound.text = LocalString.GetString(PlayInfo.RoundInfo)

        local TeamInfo1 = CLuaWorldCupMgr.GetTeamInfoById(tid1)
        local Team1Name = go.transform:Find("Team1Name"):GetComponent(typeof(UILabel))
        Team1Name.text = LocalString.GetString(TeamInfo1.Name)

        local TeamInfo2 = CLuaWorldCupMgr.GetTeamInfoById(tid2)
        local Team2Name = go.transform:Find("Team2Name"):GetComponent(typeof(UILabel))
        Team2Name.text = LocalString.GetString(TeamInfo2.Name)

        local Texture1 = go.transform:Find("Texture1"):GetComponent(typeof(CUITexture))
        Texture1:LoadMaterial(TeamInfo1.Icon)

        local Texture2 = go.transform:Find("Texture2"):GetComponent(typeof(CUITexture))
        Texture2:LoadMaterial(TeamInfo2.Icon)

        local DianQiu = go.transform:Find("DianQiu"):GetComponent(typeof(UISprite))
        if dianqiu == 1 then
            DianQiu.gameObject:SetActive(true)
        else
            DianQiu.gameObject:SetActive(false)
        end

        local vs1 = go.transform:Find("vs1"):GetComponent(typeof(UISprite))
        local vs2 = go.transform:Find("vs2"):GetComponent(typeof(UILabel))
        if score1 == -1 then
            vs1.gameObject:SetActive(true)
            vs2.gameObject:SetActive(false)
        else
            vs1.gameObject:SetActive(false)
            vs2.gameObject:SetActive(true)
            vs2.text = SafeStringFormat(LocalString.GetString("%s:%s"), score1, score2)
        end

        local CommonSelector = go.transform:Find("CommonSelector"):GetComponent(typeof(CCommonSelector))
        local MyJingCai = go.transform:Find("MyJingCai"):GetComponent(typeof(UILabel))
        self:HandleCommonSelector(CommonSelector, MyJingCai, myValue, index, pid, bFinish, score1, score2)

        index = index + 1
    end

    self.m_Table:Reposition()
    self.m_ScorllView:ResetPosition()

    self:HandleTouZhuView(myValue, myNum, bFinish, serverValue)
end

function CLuaWorldCupJingCaiView:HandleCommonSelector(CommonSelector, MyJingCai, myValue, index, pid, bFinish, score1, score2)
    CommonSelector.gameObject:SetActive(false)
    MyJingCai.gameObject:SetActive(false)

    if myValue and myValue ~= "" then
        MyJingCai.gameObject:SetActive(true)
        local myVote = string.sub(myValue, index, index)
        myVote = tonumber(myVote)
        MyJingCai.text = CLuaWorldCupMgr.m_SelectDataTbl[myVote] or LocalString.GetString("未知")
        CLuaWorldCupMgr.HandleMyJingCaiTextColor(MyJingCai, score1, score2, myVote)
        self.m_SelectResultTbl[index] = myVote
    else
        -- 玩家没有竞彩
        if bFinish then
            MyJingCai.gameObject:SetActive(true)
            MyJingCai.text = LocalString.GetString("未选择")
            MyJingCai.color = Color.yellow
        else
            local Label = CommonSelector.transform:Find("Label"):GetComponent(typeof(UILabel))
            -- 区分淘汰赛和非淘汰赛
            if pid < CLuaWorldCupMgr.m_TaoTaiSaiJingCaiBeginId then
                CommonSelector:Init(self.m_SelectDataCSharpList, DelegateFactory.Action_int(function (idx)
                    idx = idx + 1
                    CommonSelector:SetName(CLuaWorldCupMgr.m_SelectDataTbl[idx])
                    self.m_SelectResultTbl[index] = idx
                    Label.color = Color.yellow
                end))
            else
                CommonSelector:Init(self.m_SelectDataCSharpList2, DelegateFactory.Action_int(function (idx)
                    idx = idx + 1
                    CommonSelector:SetName(CLuaWorldCupMgr.m_SelectDataTbl2[idx])
                    self.m_SelectResultTbl[index] = idx
                    Label.color = Color.yellow
                end))
            end

            CommonSelector:SetName(LocalString.GetString("未选择"))
            CommonSelector.gameObject:SetActive(true)
            Label.color = Color.red
            self.m_SelectResultTbl[index] = 0
        end
    end
end

function CLuaWorldCupJingCaiView:HandleTouZhuView(myValue, myNum, bFinish, serverValue)
    if myValue and myValue ~= "" then
        self.m_TouZhuView1:SetActive(false)
        self.m_TouZhuView2:SetActive(true)
        if serverValue and serverValue ~= "" then
            if myValue == serverValue then
                self.m_TouZhuResultLabel.text = SafeStringFormat(LocalString.GetString("竞彩成功, 本轮投入%s份"), myNum)
                self.m_TouZhuResultLabel.color = Color.green     
            else
                self.m_TouZhuResultLabel.text = SafeStringFormat(LocalString.GetString("竞彩失败, 本轮投入%s份"), myNum)
                self.m_TouZhuResultLabel.color = Color.red
            end
        else
            self.m_TouZhuResultLabel.text = SafeStringFormat(LocalString.GetString("本轮投入%s份, 耐心等待竞彩结果吧"), myNum)
            self.m_TouZhuResultLabel.color = Color.yellow
        end
    else
        if bFinish then
            -- 超过当期的竞彩时间后，隐藏投注选项
            self.m_TouZhuView1:SetActive(false)
            self.m_TouZhuView2:SetActive(true)
            self.m_TouZhuResultLabel.text = LocalString.GetString("本轮竞彩未投注")
            self.m_TouZhuResultLabel.color = Color.yellow
        else
            self.m_TouZhuView1:SetActive(true)
            self.m_TouZhuView2:SetActive(false)
        end
    end
end

function CLuaWorldCupJingCaiView:OnSelected()
    Gac2Gas.QueryWorldCupTodayJingCaiInfo()
end

function CLuaWorldCupJingCaiView:OnPlayerClickTouZhuButton()
    if self.m_MyValue and self.m_MyValue ~= "" then
        return
    end

    if not self.m_Pid then
        return
    end

    -- 获取投注票数，以及投注值
    local TouZhuNum = self.m_AddSubInputButton:GetValue()
    if TouZhuNum <= 0 then
        return
    end

    -- 检查选项
    if #self.m_SelectResultTbl == 0 then
        return
    end

    for _, v in ipairs(self.m_SelectResultTbl) do
        if not (v>=1 and v<=3) then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("需选择本轮竞彩的所有比赛结果才能投注哦"))
            return
        end
    end

    -- 金钱检查
    local need = TouZhuNum * CLuaWorldCupMgr.m_JingCaiYinLiang
    local total = System.UInt64.Parse(self.m_OwnMoneyLabel.text)
    if need > total then
        g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        return
    end

    local msg = SafeStringFormat(LocalString.GetString("你确认要消耗 %s 银两进行投注吗?"), need)
    local okFunc = function () 
            local value = table.concat(self.m_SelectResultTbl)
            Gac2Gas.RequestWorldCupTodayJingCaiVote(self.m_Pid, value, TouZhuNum) 
        end

    g_MessageMgr:ShowOkCancelMessage(msg, okFunc, nil, LocalString.GetString("确认"), LocalString.GetString("取消"), true)
end

return CLuaWorldCupJingCaiView

