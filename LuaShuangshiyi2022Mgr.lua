LuaShuangshiyi2022Mgr = {}
LuaShuangshiyi2022Mgr.currentPhase1Round = 0
LuaShuangshiyi2022Mgr.hideShenZhaiTanBaoRule = false

LuaShuangshiyi2022Mgr.MQXHEnterPhase2Check = false
LuaShuangshiyi2022Mgr.MQHXEnterPhaseCrazyCheck = false
local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory = import "DelegateFactory"

LuaShuangshiyi2022Mgr.IsInMQHXPlay = function()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == Double11_MQHX.GetData().PlayDesignId then
            return true
        end
    end
    return false
end

function LuaShuangshiyi2022Mgr:Double11MQHX_SyncPlayState(playState, startTime, endTime, RankTbl_U)
    LuaShuangshiyi2022Mgr.MQHXPlayInfo = {
        playState = playState,
        startTime = startTime,
        endTime = endTime,
        RankTbl_U = RankTbl_U
    }
    if playState == EnumDouble11MQHXPlayState.Idle then
        local curPhaseRemainTime = endTime - CServerTimeMgr.Inst.timeStamp
        local standardPlayDelay = Double11_MQHX.GetData().PlayStartDelay
        if curPhaseRemainTime >= standardPlayDelay then
            if not LuaShuangshiyi2022Mgr.m_IdleStateLeftTimeTick then
                LuaShuangshiyi2022Mgr.m_IdleStateLeftTimeTick = CTickMgr.Register(DelegateFactory.Action(function ()
                    if LuaShuangshiyi2022Mgr.IsInMQHXPlay() then
                        CCenterCountdownWnd.count = standardPlayDelay
                        CCenterCountdownWnd.InitStartTime()
                        CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
                    end
                end), curPhaseRemainTime-standardPlayDelay, ETickType.Once)
            end
        else
            CCenterCountdownWnd.count = curPhaseRemainTime
            CCenterCountdownWnd.InitStartTime()
            CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
        end
        LuaShuangshiyi2022Mgr.MQXHEnterPhase2Check = true
    elseif playState == EnumDouble11MQHXPlayState.Normal then
        if LuaShuangshiyi2022Mgr.MQXHEnterPhase2Check then
            if not CUIManager.IsLoaded(CLuaUIResources.MengQuanHengXingWarningWnd) then
                LuaMengQuanHengXingWarningWnd.playState = playState
                CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingWarningWnd)
            else
                g_ScriptEvent:BroadcastInLua("UpdateMengQuanHengXingWarning", playState)
            end
            LuaShuangshiyi2022Mgr.MQXHEnterPhase2Check = false
        end
        g_ScriptEvent:BroadcastInLua("SyncMengQuanHengXingEndTime", endTime + Double11_MQHX.GetData().PlayCrazyDuration, true)
    elseif playState == EnumDouble11MQHXPlayState.Warning then
        if not CUIManager.IsLoaded(CLuaUIResources.MengQuanHengXingWarningWnd) then
            LuaMengQuanHengXingWarningWnd.playState = playState
            CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingWarningWnd)
        else
            g_ScriptEvent:BroadcastInLua("UpdateMengQuanHengXingWarning", playState)
        end
        g_ScriptEvent:BroadcastInLua("SyncMengQuanHengXingEndTime", Double11_MQHX.GetData().PlayCrazyDuration, false)
        LuaShuangshiyi2022Mgr.MQHXEnterPhaseCrazyCheck = true
    elseif playState == EnumDouble11MQHXPlayState.Crazy then
        if LuaShuangshiyi2022Mgr.MQHXEnterPhaseCrazyCheck then
            if not CUIManager.IsLoaded(CLuaUIResources.MengQuanHengXingWarningWnd) then
                LuaMengQuanHengXingWarningWnd.playState = playState
                CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingWarningWnd)
            else
                g_ScriptEvent:BroadcastInLua("UpdateMengQuanHengXingWarning", playState)
            end
            LuaShuangshiyi2022Mgr.MQHXEnterPhaseCrazyCheck = false
        end
        g_ScriptEvent:BroadcastInLua("SyncMengQuanHengXingEndTime", endTime, true)
    elseif playState == EnumDouble11MQHXPlayState.Reward then
        --这个会在Double11MQHX_ShowRewardWnd中处理
        if not CUIManager.IsLoaded(CLuaUIResources.MengQuanHengXingWarningWnd) then
            LuaMengQuanHengXingWarningWnd.playState = playState
            CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingWarningWnd)
        else
            g_ScriptEvent:BroadcastInLua("UpdateMengQuanHengXingWarning", playState)
        end
    elseif playState == EnumDouble11MQHXPlayState.GameFinish then
        --g_ScriptEvent:BroadcastInLua("HideMengQuanHengXingWarning")
    end
    g_ScriptEvent:BroadcastInLua("SyncMengQuanHengXingRankInfo", RankTbl_U)
end

function LuaShuangshiyi2022Mgr:Double11MQHX_BroadCastAction(playerId, action, data_U)
    g_ScriptEvent:BroadcastInLua("Double11MQHX_BroadCastAction", playerId, action, data_U)
end

function LuaShuangshiyi2022Mgr:Double11MQHX_ShowRewardWnd(Rewards_U, RankTbl_U)
    LuaMengQuanHengXingResultWnd.S_RewardsData = Rewards_U
    LuaMengQuanHengXingResultWnd.S_RankTbl = RankTbl_U

    if LuaShuangshiyi2022Mgr.m_ShowMQHXRewardDelayTick then
        LuaShuangshiyi2022Mgr.m_ShowMQHXRewardDelayTick:Invoke()
        LuaShuangshiyi2022Mgr.m_ShowMQHXRewardDelayTick = nil
    end
    LuaShuangshiyi2022Mgr.m_ShowMQHXRewardDelayTick = CTickMgr.Register(DelegateFactory.Action(function ()
        CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingResultWnd)
    end), 1500, ETickType.Once)
end

function LuaShuangshiyi2022Mgr:Double11SZTB_SyncPlayState(playState, EnterStateTime, NextStateTime, round, traps_U)
    LuaShuangshiyi2022Mgr.SZTBPlayState = playState
    LuaShuangshiyi2022Mgr.Round = round
    g_ScriptEvent:BroadcastInLua("SyncSZTBPlayState", playState, round, traps_U)
    if playState == EnumDouble11SZTBPlayState.Idle then
        LuaShuangshiyi2022Mgr.hideShenZhaiTanBaoRule = true
        g_MessageMgr:ShowMessage("Double11_ShenZhaiTanBao_Tips")
    elseif playState == EnumDouble11SZTBPlayState.Stage_1_Scenario then
        if LuaShuangshiyi2022Mgr.hideShenZhaiTanBaoRule and CUIManager.IsLoaded(CUIResources.MessageTipWnd) then
            LuaShuangshiyi2022Mgr.hideShenZhaiTanBaoRule = false
            CUIManager.CloseUI(CUIResources.MessageTipWnd)
        end
    elseif playState == EnumDouble11SZTBPlayState.Stage_1_Prepare then
        CCenterCountdownWnd.count = Double11_SZTB.GetData().Stage_1_Prepare_Duration
        CCenterCountdownWnd.InitStartTime()
        CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
    elseif playState == EnumDouble11SZTBPlayState.Stage_1_Playing then
        LuaShuangshiyi2022Mgr.currentPhase1Round = round
    elseif playState == EnumDouble11SZTBPlayState.Stage_1_Finish then
        g_ScriptEvent:BroadcastInLua("Double11SZTB_Stage1_Finish")
    elseif playState == EnumDouble11SZTBPlayState.Stage_2_Scenario then
    elseif playState == EnumDouble11SZTBPlayState.Stage_2 then
        CUIManager.ShowUI(CLuaUIResources.ShenZhaiTanBaoKickSkillWnd)
    elseif playState == EnumDouble11SZTBPlayState.Stage_3 then
        g_ScriptEvent:BroadcastInLua("Double11SZTB_Stage2_Finish")
        if CUIManager.IsLoaded(CLuaUIResources.ShenZhaiTanBaoKickSkillWnd) then
            CUIManager.CloseUI(CLuaUIResources.ShenZhaiTanBaoKickSkillWnd)
        end
    elseif playState == EnumDouble11SZTBPlayState.Reward then
    elseif playState == EnumDouble11SZTBPlayState.GameFinish then
    end
end 

function LuaShuangshiyi2022Mgr:Double11SZTB_ShowRewardWnd(bSuccess, playDuration, bReward, bNewRecord)
    LuaShenZhaiTanBaoResultWnd.S_bSuccess = bSuccess
    LuaShenZhaiTanBaoResultWnd.S_playDuration = playDuration
    LuaShenZhaiTanBaoResultWnd.S_bReward = bReward
    LuaShenZhaiTanBaoResultWnd.S_bNewRecord = bNewRecord
    CUIManager.ShowUI(CLuaUIResources.ShenZhaiTanBaoResultWnd)
end 

function LuaShuangshiyi2022Mgr:DoubleOne2022SZTBCheck()
    local rewardTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11SZTBRewardTimes) or 0
    local rewardTimeLimit = Double11_SZTB.GetData().RewardTimesLimit
    local remainRewards = rewardTimeLimit > rewardTimes
    return remainRewards
end 