local MessageWndManager = import "L10.UI.MessageWndManager"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local Quaternion = import "UnityEngine.Quaternion"
local CMiniMapCommonItemNodeResource = import "L10.UI.CMiniMapPopWnd+CMiniMapCommonItemNodeResource"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
local CScene = import "L10.Game.CScene"
local CTeamMgr = import "L10.Game.CTeamMgr"
local EnumObjectType = import "L10.Game.EnumObjectType"
local CGaoChangMgr = import "L10.Game.CGaoChangMgr"

LuaGaoChangJiDouMgr = {}

if rawget(_G, "LuaGaoChangJiDouMgr") then
    g_ScriptEvent:RemoveListener("TeamInfoChange",LuaGaoChangJiDouMgr,"OnTeamInfoChange")
    g_ScriptEvent:RemoveListener("ClientObjCreate", LuaGaoChangJiDouMgr, "OnClientObjCreate")
end

g_ScriptEvent:AddListener("TeamInfoChange",LuaGaoChangJiDouMgr,"OnTeamInfoChange")
g_ScriptEvent:AddListener("ClientObjCreate", LuaGaoChangJiDouMgr, "OnClientObjCreate")

LuaGaoChangJiDouMgr.HeadInfoColor = {}
LuaGaoChangJiDouMgr.hasApplied = false

LuaGaoChangJiDouMgr.IconRes = {
    "UI/Texture/FestivalActivity/Festival_YuanXiao/YuanXiao2023/Material/yuanxiaogaochangresultwnd_icon_guaiwu.mat", -- Monster
    "UI/Texture/FestivalActivity/Festival_YuanXiao/YuanXiao2023/Material/yuanxiaogaochangresultwnd_icon_baoxiang.mat" -- Pick
}

LuaGaoChangJiDouMgr.MyInfo = {}
LuaGaoChangJiDouMgr.TeamMembers = {}
LuaGaoChangJiDouMgr.PlayerDict = {}
LuaGaoChangJiDouMgr.MonsterNum = 0
LuaGaoChangJiDouMgr.PickNum = 0
LuaGaoChangJiDouMgr.PlayerNum = 0

LuaGaoChangJiDouMgr.MainPlayerCreated = false
LuaGaoChangJiDouMgr.MaskedPlayerName = nil
LuaGaoChangJiDouMgr.MaskedLingShouName = nil

--#region Called by GamePlayMgr

function LuaGaoChangJiDouMgr:OnMainPlayerCreated()
    CUIManager.ShowUI(CLuaUIResources.YuanXiaoGaoChangTopRightWnd)
    --if CScene.MainScene and CScene.MainScene.SceneTemplateId == 16102323 then -- 只在第一层弹
    --    LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(6)
    --end

    self:OnTeamInfoChange()
    
    self.MainPlayerCreated = true
end

function LuaGaoChangJiDouMgr:OnMainPlayerDestroyed()
    self.MyInfo = {}
    self.HeadInfoColor = {}
    self.PlayerDict = {}
    self.MainPlayerCreated = false
    self.MonsterNum = 0
    self.PickNum = 0
    self.PlayerNum = 0
    CUIManager.CloseUI(CLuaUIResources.YuanXiaoGaoChangTopRightWnd)
end

--#endregion

function LuaGaoChangJiDouMgr:OnClientObjCreate(args)
    if not LuaGaoChangJiDouMgr:IsInJiDou() then
        return 
    end
    local engineId = args[0]
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        if obj.ObjectType == EnumObjectType.Player then
            local pInfo = self.PlayerDict[obj.BasicProp.Id] or {}
            if pInfo.masked then self:MaskPlayer(engineId, true) 
            elseif pInfo.masked == false then self:UnMaskPlayer(engineId, true) end

            local info = LuaGamePlayMgr:GetTitleBoardInfo(engineId)
            info = info or {}
            info.displayName = pInfo.displayName
            info.fontColor = pInfo.fontColor
            LuaGamePlayMgr:SetTitleBoardInfo(engineId, info, true)
        end
    end
end

function LuaGaoChangJiDouMgr:OnTeamInfoChange()
    if not LuaGaoChangJiDouMgr:IsInJiDou() then
        return 
    end

    if self.TeamMembers then
        for _, id in ipairs(self.TeamMembers) do
            local player = CClientPlayerMgr.Inst:GetPlayer(id)
            if player then
                self:MaskPlayer(player.EngineId, true)
            end
            if not self.PlayerDict[id] then self.PlayerDict[id] = {} end
            self.PlayerDict[id].masked = true
        end
    end

    self.TeamMembers = {}
    local teamLs = CTeamMgr.Inst:GetTeamMembersExceptMe()
    for i = 0, teamLs.Count - 1 do
        table.insert(self.TeamMembers, teamLs[i].m_MemberId)
    end

    for _, id in ipairs(self.TeamMembers) do
        local player = CClientPlayerMgr.Inst:GetPlayer(id)
        if player then
            self:UnMaskPlayer(player.EngineId, true)
        end
        if not self.PlayerDict[id] then self.PlayerDict[id] = {} end
        self.PlayerDict[id].masked = false
    end
end

function LuaGaoChangJiDouMgr:GaoChangJiDouOnPlayerPick(playerId, pickEngineId)
    --local nodeTransform = CHeadInfoWnd.Instance.headInfoDict[CClientMainPlayer.Inst.EngineId] 
    local obj = CClientObjectMgr.Inst:GetObject(pickEngineId)
    if obj then
        g_ScriptEvent:BroadcastInLua("GaoChangJiDouOnPlayerPick", obj.RO.Position)
    end
end

function LuaGaoChangJiDouMgr:SetMiniMapMarkInfo(type, engineId, x, y, isAlive, isBig) --isBig:区分大小怪/宝箱
    -- 前景
    LuaGamePlayMgr:SetMiniMapMarkInfo(engineId, isAlive and {
        res = self.IconRes[type], 
        x = x, 
        y = y, 
        size = {
            isBig and {width = 32, height = 32} or {width = 24, height = 24}, -- 小地图
            isBig and {width = 36, height = 36} or {width = 28, height = 28}, -- 大地图
        },
    } or nil, true)
    -- 背景
    LuaGamePlayMgr:SetMiniMapMarkInfo(engineId + 1000000, isAlive and {
        res = "UI/Texture/Transparent/Material/huluwazhanlingwnd_white.mat", 
        x = x, 
        y = y, 
        size = {
            isBig and {width = 48, height = 48} or {width = 40, height = 40}, 
            isBig and {width = 54, height = 54} or {width = 48, height = 48}, 
        },
        color = isBig and NGUIText.ParseColor24("ff0000", 0) or NGUIText.ParseColor24("ff800a", 0),
        depth = 4,
    } or nil, true)
    LuaGamePlayMgr:SetMiniMapMarkInfo(engineId + 2000000, isAlive and {
        res = "UI/Texture/Transparent/Material/ZYJYaoZhengWnd_bg_white.mat", 
        x = x, 
        y = y, 
        offset = {
            isBig and {y = 22} or {y = 16},
            isBig and {y = 26} or {y = 20},
        },
        size = {
            isBig and {width = 24, height = 32} or {width = 18, height = 24}, 
            isBig and {width = 28, height = 40} or {width = 22, height = 30}, 
        },
        color = isBig and NGUIText.ParseColor24("ff0000", 0) or NGUIText.ParseColor24("ff800a", 0),
        alpha = 0.65,
        depth = 3,
    } or nil, true)
end

function LuaGaoChangJiDouMgr:SyncGaoChangJiDouMonsterState(engineId, x, y, isAlive, isBig) 
    --print(engineId, x, y, isAlive)
    self:SetMiniMapMarkInfo(1, engineId, x, y, isAlive, isBig)
end

function LuaGaoChangJiDouMgr:SyncGaoChangJiDouPickState(engineId, x, y, isAlive, isBig)
    --print(engineId, x, y, isAlive)
    self:SetMiniMapMarkInfo(2, engineId, x, y, isAlive, isBig)
end

function LuaGaoChangJiDouMgr:GaoChangJiDouMengMianPlayers(players, isFirstEnter)
    if isFirstEnter then
        LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(6)
    end
    for k, v in pairs(players) do 
        self:MaskPlayer(v, true)
    end 
end

function LuaGaoChangJiDouMgr:MaskPlayer(engineId, doUpdate)
    if not self.MaskedPlayerName or not self.MaskedLingShouName then 
        self.MaskedPlayerName, self.MaskedLingShouName = string.match(GaoChangJiDou_Setting.GetData().ShowPlayerName, "(.+),%s*(.+)")
    end
    LuaGamePlayMgr:SetNameBoardInfo(engineId, {
        displayName = self.MaskedPlayerName,
        onClickTargetWnd = function()
            g_MessageMgr:ShowMessage("GaoChangJiDou_MaskedPerson_Hint")
        end,
        lingShouInfo = {
            displayName = self.MaskedLingShouName
        },
        hideShadowPetName = true,
    }, doUpdate)
    local player = CClientObjectMgr.Inst:GetObject(engineId)
    local playerId = player and player.BasicProp.Id or 0
    local pInfo = self.PlayerDict[playerId] or {}
    LuaGamePlayMgr:SetTitleBoardInfo(engineId, {
        displayName = pInfo.displayName,
        fontColor = pInfo.fontColor,
        petInfo = {
            displayName = LocalString.GetString("【")..self.MaskedPlayerName..LocalString.GetString("】")
        }
    }, doUpdate)
end

function LuaGaoChangJiDouMgr:UnMaskPlayer(engineId, doUpdate)
    LuaGamePlayMgr:SetNameBoardInfo(engineId, nil, doUpdate)
    local info = LuaGamePlayMgr:GetTitleBoardInfo(engineId)
    if info then info.petInfo = nil end
    LuaGamePlayMgr:SetTitleBoardInfo(engineId, info, doUpdate)
end

function LuaGaoChangJiDouMgr:LoadDesignTable()
    if #self.HeadInfoColor == 0 then
        local color = YuanXiao2023_Setting.GetData().PointColor
        for score2hex in string.gmatch(color, "[^;]+") do
            local score, hex = string.match(score2hex, "(%d+):(.+)")
            table.insert(self.HeadInfoColor,{tonumber(score), string.sub(hex, 2)})
        end
    end
end

function LuaGaoChangJiDouMgr:IsInJiDou() 
    return CScene.MainScene and CScene.MainScene.GamePlayDesignId == 51103057
end

function LuaGaoChangJiDouMgr:IsOpen() 
    local id = GaoChangJiDou_Setting.GetData().GaoChangJiDouScheduleId
    return CLuaScheduleMgr:GetScheduleInfo(id) or LuaActivityRedDotMgr.m_ScheduleDict[id]
end

function LuaGaoChangJiDouMgr:Filter(weekInfos) -- 高昌激斗开启时屏蔽普通高昌
    local jiDouId = GaoChangJiDou_Setting.GetData().GaoChangJiDouScheduleId -- 只在每周四开启, 所以周历获取不到
    local weekInfoTaskId = GaoChangJiDou_Setting.GetData().ZhouLiTask -- 用来显示高昌激斗周历信息的任务id
    if weekInfos then
        local lookup = {}
        for i = 1, 7 do
            lookup[i] = {}
        end
        for i = 1, #weekInfos do
            local info = weekInfos[i]
            lookup[info.day + 1][info.activityId] = i
            lookup[info.day + 1][info.taskId] = i
        end
        local removeIdx = {}
        for i = 1, 7 do
            if lookup[i][weekInfoTaskId] then
                if lookup[i][42000018] then removeIdx[lookup[i][42000018]] = true end
                if lookup[i][jiDouId] then removeIdx[lookup[i][weekInfoTaskId]] = true end --周四当天筛掉周历任务
            end
        end
        for i = #weekInfos, 1, -1 do
            if removeIdx[i] then
                table.remove(weekInfos, i)
            end
        end
    elseif self:IsOpen() and CLuaScheduleMgr.m_TableTypeList[1] then --客户端小概率可能在服务器同步数据之前就BuildInfo, 此时数据是空的
        local list = CLuaScheduleMgr.m_TableTypeList[1]
        for i = #list, 1, -1 do
            if list[i].activityId == 42000018 or list[i].taskId == weekInfoTaskId then
                table.remove(list, i)
            end
        end
    end
end

function LuaGaoChangJiDouMgr:GaoChangJiDouCrossQueryApplyInfoResult(status)
    self.hasApplied = status
    CUIManager.ShowUI(CLuaUIResources.YuanXiaoGaoChangEnterWnd)
end

function LuaGaoChangJiDouMgr:GaoChangJiDouCrossInviteToApply()
    CScheduleMgr.Inst:RequestActivity()
    local msg = g_MessageMgr:FormatMessage("GAOCHANG_JIDOU_OPEN_INFORM")
    MessageWndManager.ShowOKCancelMessageWithTimelimitAndPriority(msg, 300, 0, DelegateFactory.Action(function()
        Gac2Gas.GaoChangQueryApplyInfo()
        Gac2Gas.GaoChangJiDouCrossQueryApplyInfo()
    end), nil, nil, nil, false)
end

function LuaGaoChangJiDouMgr:GaoChangJiDouCrossApplySuccess()
    self.hasApplied = true
    g_ScriptEvent:BroadcastInLua("RefreshGaoChangJiDouEnterWnd")
end

-- PlayerEnterScene时会发, 比OnMainPlayerCreated早
function LuaGaoChangJiDouMgr:GaoChangJiDouScoreUpdate(playerId, score)
    self:LoadDesignTable()
    
    local color, topHexColor
    local fnd = false
    for i = 1, #self.HeadInfoColor do
        local pair = self.HeadInfoColor[i]
        topHexColor = pair[2]
        if score <= pair[1] then
            color = pair[2]
            fnd = true
            break
        end
    end
    if not fnd then color = topHexColor end

    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    local info = player and LuaGamePlayMgr:GetTitleBoardInfo(player.EngineId)
    info = info or {}
    info.displayName = score..LocalString.GetString("分")
    info.fontColor = NGUIText.ParseColor24(color or "ffffff", 0)
    if player then
        LuaGamePlayMgr:SetTitleBoardInfo(player.EngineId, info, true)
    end
    
    if not self.PlayerDict[playerId] then self.PlayerDict[playerId] = {} end
    self.PlayerDict[playerId].displayName = info.displayName
    self.PlayerDict[playerId].fontColor = info.fontColor
end

function LuaGaoChangJiDouMgr:GaoChangJiDouSelfUpdate(playerId, kill, killed, help, monsterkill, baoxiang, score)
    self.MyInfo.playerId = playerId
    self.MyInfo.kill = kill
    self.MyInfo.killed = killed
    self.MyInfo.help = help
    self.MyInfo.monsterkill = monsterkill
    self.MyInfo.baoxiang = baoxiang
    self.MyInfo.score = score
    g_ScriptEvent:BroadcastInLua("SyncGaoChangJiDouCrossPlayInfo")
end

function LuaGaoChangJiDouMgr:SyncGaoChangJiDouCrossPlayInfo(rankInfo_U, monsterNum, pickNum)
    self.MonsterNum = monsterNum
    self.PickNum = pickNum
    self.PlayerNum = 0
    
    local rankInfo = g_MessagePack.unpack(rankInfo_U)
    local id = self.MyInfo.playerId or CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
    
    for i = 1, #rankInfo, 2 do 
        if id == rankInfo[i + 1] then
            self.MyInfo.rank = rankInfo[i]
        end
        self.PlayerNum = self.PlayerNum + 1
    end
    g_ScriptEvent:BroadcastInLua("SyncGaoChangJiDouCrossPlayInfo")
end

function LuaGaoChangJiDouMgr:GaoChangJiDouQueryAwardResult(award_s)
    LuaGaoChangJiDouAwardListWnd.s_Reward = award_s
    CUIManager.CloseUI(CLuaUIResources.YuanXiaoGaoChangResultWnd)
    CUIManager.ShowUI(CLuaUIResources.YuanXiaoGaoChangAwardListWnd)
end

function LuaGaoChangJiDouMgr:GaoChangJiDouCrossCancelApplySuccess()
    self.hasApplied = false
    g_ScriptEvent:BroadcastInLua("RefreshGaoChangJiDouEnterWnd")
end

function LuaGaoChangJiDouMgr:GaoChangCancelApplySuccess()
    CGaoChangMgr.Inst.canApply = true
end

CGaoChangMgr.m_hookSetCanApply = function(this, canApply)
    g_ScriptEvent:BroadcastInLua("RefreshGaoChangJiDouEnterWnd")
end