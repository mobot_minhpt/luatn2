local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CMessageTipMgr=import "L10.UI.CMessageTipMgr"
local CTipTitleItem= import "CTipTitleItem"
local CTipParagraphItem=import "L10.UI.CTipParagraphItem"
local UITable=import "UITable"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaLianLianKanApplyWnd=class()
RegistClassMember(CLuaLianLianKanApplyWnd,"m_ApplyButton")
RegistClassMember(CLuaLianLianKanApplyWnd,"m_RankBonusRatioTbl")
RegistClassMember(CLuaLianLianKanApplyWnd,"m_ContentTable")
RegistClassMember(CLuaLianLianKanApplyWnd,"m_TitleTemplate")
RegistClassMember(CLuaLianLianKanApplyWnd,"m_ParagraphTemplate")

function CLuaLianLianKanApplyWnd:Init()
    self.m_ContentTable = FindChild(self.transform,"ContentTable").gameObject
    self.m_TitleTemplate = FindChild(self.transform,"TitleTemplate").gameObject
    self.m_ParagraphTemplate = FindChild(self.transform,"ParagraphTemplate").gameObject
    self.m_TitleTemplate:SetActive(false)
    self.m_ParagraphTemplate:SetActive(false)
    local tipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(g_MessageMgr:FormatMessage("LianLianKan2018_Rule"))
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    if tipInfo.titleVisible then
        local go = NGUITools.AddChild(self.m_ContentTable,self.m_TitleTemplate)
        go:SetActive(true)
        go:GetComponent(typeof(CTipTitleItem)):Init(tipInfo.title)
    end
    for i=1,tipInfo.paragraphs.Count do
        local go = NGUITools.AddChild(self.m_ContentTable,self.m_ParagraphTemplate)
        go:SetActive(true)
        go:GetComponent(typeof(CTipParagraphItem)):Init(tipInfo.paragraphs[i-1],CMessageTipMgr.Inst.ContentColor)
    end
    self.m_ContentTable:GetComponent(typeof(UITable)):Reposition()



    local applyButton=FindChild(self.transform,"ApplyButton").gameObject
    UIEventListener.Get(applyButton).onClick=DelegateFactory.VoidDelegate(function(go)
        local text = g_MessageMgr:FormatMessage("LianLianKanApplyConfirm")
        MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
            Gac2Gas.RequestEnterLianLianKan2018Play()
            CUIManager.CloseUI("LianLianKanApplyWnd")
        end), nil, nil, nil, false)
    end)

    local moneyCtrl = FindChild(self.transform,"QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    moneyCtrl:SetCost(tonumber(LianLianKan2018_Setting.GetData().PlayCost))

    self:LoadRankBonusRatio()
    Gac2Gas.RequestLianLianKanBonusPool()
end

function CLuaLianLianKanApplyWnd:LoadRankBonusRatio()
    self.m_RankBonusRatioTbl = {}

    local index = 0
    local rankBonus = LianLianKan2018_Setting.GetData().RankBonus
    for i=1,rankBonus.Length do
        table.insert( self.m_RankBonusRatioTbl, rankBonus[i-1] )
    end
    -- for str in string.gmatch(Lua_LianLianKan2018_Setting["RankBonus"].Value, "(%d+),?") do
    --     local ratio = tonumber(str)
    --     table.insert( self.m_RankBonusRatioTbl, ratio )
	-- end
end

function CLuaLianLianKanApplyWnd:OnEnable()
    g_ScriptEvent:AddListener("RequestLianLianKanBonusPoolReturn", self, "OnRequestLianLianKanBonusPoolReturn")

end

function CLuaLianLianKanApplyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RequestLianLianKanBonusPoolReturn", self, "OnRequestLianLianKanBonusPoolReturn")

end

function CLuaLianLianKanApplyWnd:OnRequestLianLianKanBonusPoolReturn(bouns)
    local rewardLabel=FindChild(self.transform,"RewardLabel"):GetComponent(typeof(UILabel))
    rewardLabel.text=tostring(bouns)
    local tf=FindChild(self.transform,"Rewards")
    for i=1,5 do
        local label = tf:GetChild(i-1):GetComponent(typeof(UILabel))
        label.text=tostring(math.floor(self.m_RankBonusRatioTbl[i]*bouns/100))
    end
end