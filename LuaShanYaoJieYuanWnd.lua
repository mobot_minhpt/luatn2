local DelegateFactory              = import "DelegateFactory"
local UITabBar                     = import "L10.UI.UITabBar"
local CButton                      = import "L10.UI.CButton"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local TweenAlpha                   = import "TweenAlpha"
local Screen                       = import "UnityEngine.Screen"
local EnumWarnFXType               = import "L10.Game.EnumWarnFXType"
local CEffectMgr                   = import "L10.Game.CEffectMgr"
local CServerTimeMgr               = import "L10.Game.CServerTimeMgr"
local CPopupMenuInfoMgrAlignType   = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local MessageMgr                   = import "L10.Game.MessageMgr"

LuaShanYaoJieYuanWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaShanYaoJieYuanWnd, "buddyTab")
RegistClassMember(LuaShanYaoJieYuanWnd, "score")
RegistClassMember(LuaShanYaoJieYuanWnd, "buddyName")
RegistClassMember(LuaShanYaoJieYuanWnd, "model")
RegistClassMember(LuaShanYaoJieYuanWnd, "word")
RegistClassMember(LuaShanYaoJieYuanWnd, "bubble")
RegistClassMember(LuaShanYaoJieYuanWnd, "storyButton")
RegistClassMember(LuaShanYaoJieYuanWnd, "unlockButton")
RegistClassMember(LuaShanYaoJieYuanWnd, "followButton")
RegistClassMember(LuaShanYaoJieYuanWnd, "unlockCondition")
RegistClassMember(LuaShanYaoJieYuanWnd, "shareButton")
RegistClassMember(LuaShanYaoJieYuanWnd, "outline")
RegistClassMember(LuaShanYaoJieYuanWnd, "unlockFx")
RegistClassMember(LuaShanYaoJieYuanWnd, "juJiFx")
RegistClassMember(LuaShanYaoJieYuanWnd, "tipButton")

RegistClassMember(LuaShanYaoJieYuanWnd, "tabId2BuddyId")
RegistClassMember(LuaShanYaoJieYuanWnd, "buddyId2TabId")

RegistClassMember(LuaShanYaoJieYuanWnd, "modelIdentifier")
RegistClassMember(LuaShanYaoJieYuanWnd, "modelLoader")
RegistClassMember(LuaShanYaoJieYuanWnd, "bubbleTick")
RegistClassMember(LuaShanYaoJieYuanWnd, "rotation")
RegistClassMember(LuaShanYaoJieYuanWnd, "ro")

function LuaShanYaoJieYuanWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

function LuaShanYaoJieYuanWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.buddyTab = anchor:Find("BuddyTab"):GetComponent(typeof(UITabBar))
    self.score = anchor:Find("Score"):GetComponent(typeof(UILabel))
    self.buddyName = anchor:Find("Model/Name"):GetComponent(typeof(UILabel))
    self.model = anchor:Find("Model/Texture"):GetComponent(typeof(CUITexture))
    self.word = anchor:Find("Model/Word"):GetComponent(typeof(UILabel))
    self.bubble = anchor:Find("Model/Word/Bubble"):GetComponent(typeof(UIWidget))
    self.outline = anchor:Find("Model/Outline"):GetComponent(typeof(CUITexture))
    self.juJiFx = anchor:Find("Model/JuJiFx"):GetComponent(typeof(CUIFx))
    self.storyButton = anchor:Find("StoryButton"):GetComponent(typeof(CButton))
    self.unlockButton = anchor:Find("UnlockButton"):GetComponent(typeof(CButton))
    self.unlockFx = anchor:Find("UnlockButton/Fx"):GetComponent(typeof(CUIFx))
    self.followButton = anchor:Find("FollowButton"):GetComponent(typeof(CButton))
    self.shareButton = anchor:Find("ShareButton"):GetComponent(typeof(CButton))
    self.unlockCondition = anchor:Find("UnlockCondition"):GetComponent(typeof(UILabel))
    self.tipButton = anchor:Find("TipButton").gameObject
end

function LuaShanYaoJieYuanWnd:InitEventListener()
    UIEventListener.Get(self.storyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStoryButtonClick()
	end)

    UIEventListener.Get(self.unlockButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUnlockButtonClick()
	end)

    UIEventListener.Get(self.followButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFollowButtonClick()
	end)

    UIEventListener.Get(self.tipButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    UIEventListener.Get(self.shareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    UIEventListener.Get(self.model.gameObject).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
        self:OnModelDrag(go, delta)
    end)
end

function LuaShanYaoJieYuanWnd:OnEnable()
    g_ScriptEvent:AddListener("ShanYaoJieYuanYaoBanUnlock", self, "OnYaoBanUnlock")
    g_ScriptEvent:AddListener("ShanYaoJieYuanYaoBanEnableOrDisable", self, "OnYaoBanEnableOrDisable")
    g_ScriptEvent:AddListener("ShanYaoJieYuanYaoBanUpdateScore", self, "OnYaoBanUpdateScore")
end

function LuaShanYaoJieYuanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShanYaoJieYuanYaoBanUnlock", self, "OnYaoBanUnlock")
    g_ScriptEvent:RemoveListener("ShanYaoJieYuanYaoBanEnableOrDisable", self, "OnYaoBanEnableOrDisable")
    g_ScriptEvent:RemoveListener("ShanYaoJieYuanYaoBanUpdateScore", self, "OnYaoBanUpdateScore")
end

function LuaShanYaoJieYuanWnd:OnYaoBanUnlock(yaobanId)
    self:UpdateTabId2BuddyId()
    self:UpdateThirdTabActive()
    self:UpdateUnlock()
    self:UpdateIcon()
    self:OnTabChange()

    -- 播放特效
    local tabId = self.buddyTab.SelectedIndex + 1
    local buddyId = self.tabId2BuddyId[tabId]
    if buddyId and buddyId == yaobanId and tabId > 1 then
        self.juJiFx:LoadFx(g_UIFxPaths.ShanYanJieYuanJuJiFxPath)
    end
end

function LuaShanYaoJieYuanWnd:OnYaoBanEnableOrDisable()
    self:UpdateUnlockButton()
    self:UpdateFollow()
end

function LuaShanYaoJieYuanWnd:OnYaoBanUpdateScore()
    self:UpdateScore()
end


function LuaShanYaoJieYuanWnd:Init()
    self:InitModelLoader()
    self.rotation = 180

    self:UpdateTabId2BuddyId()
    self:UpdateThirdTabActive()
    self:InitBuddyTab()

    self:UpdateScore()
end

-- 建立tabId(1-3) --> buddyId(1001-1003)的映射关系, 未解锁情况下buddyId为nil
function LuaShanYaoJieYuanWnd:UpdateTabId2BuddyId()
    self.tabId2BuddyId = {}
    self.buddyId2TabId = {}

    local unlock = LuaXinBaiLianDongMgr.SYJYInfo.unlock
    if not unlock then return end

    if unlock[1001] then
        self.tabId2BuddyId[1] = 1001
        self.buddyId2TabId[1001] = 1
    end

    local buddyId2 = nil
    local buddyId3 = nil

    if unlock[1002] and not unlock[1003] then buddyId2 = 1002 end
    if unlock[1003] and not unlock[1002] then buddyId2 = 1003 end
    if unlock[1002] and unlock[1003] then
        if unlock[1002].Time > unlock[1003].Time then
            buddyId2 = 1003
            buddyId3 = 1002
        else
            buddyId2 = 1002
            buddyId3 = 1003
        end
    end

    if buddyId2 then
        self.tabId2BuddyId[2] = buddyId2
        self.buddyId2TabId[buddyId2] = 2
    end

    if buddyId3 then
        self.tabId2BuddyId[3] = buddyId3
        self.buddyId2TabId[buddyId3] = 3
    end
end

-- 初始化妖伴选择列表
function LuaShanYaoJieYuanWnd:InitBuddyTab()
    self:UpdateIcon()

    self.buddyTab.validator = LuaUtils.UITabBar_Validate(
        function(index)
            return self:OnTabChangeValidate(index)
        end
    )

    self.buddyTab.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)
    self:InitTabSelect()
    self:InitRedDot()

    self:UpdateFollow()

    for i = 1, 3 do
        self:SetUnlockActive(i, false)
    end
    self:UpdateUnlock()
end

-- 第二个图标未解锁时，第三个图标不显示
function LuaShanYaoJieYuanWnd:UpdateThirdTabActive()
    self.buddyTab:GetTabGoByIndex(2):SetActive(self.tabId2BuddyId[2] and true or false)
end

-- 初始选择优先级 服务器通知 > 当前跟随的 > 已解锁的
function LuaShanYaoJieYuanWnd:InitTabSelect()
    local defaultId = LuaXinBaiLianDongMgr.SYJYInfo.defaultSelectId
    local followId = LuaXinBaiLianDongMgr.SYJYInfo.followId
    if not defaultId and followId then
        defaultId = followId
    end

    if defaultId and self.buddyId2TabId[defaultId] then
        self.buddyTab:ChangeTab(self.buddyId2TabId[defaultId] - 1, false)
        return
    end

    local selectIndex = 1

    for i = 1, 3 do
        if self.tabId2BuddyId[i] then
            selectIndex = i
            break
        end
    end
    self.buddyTab:ChangeTab(selectIndex - 1, false)
end

-- 初始化红点显示
function LuaShanYaoJieYuanWnd:InitRedDot()
    for i = 1, 3 do
        local redDot = self.buddyTab:GetTabGoByIndex(i - 1).transform:Find("RedDot").gameObject
        redDot:SetActive(false)

        if self.buddyTab.SelectedIndex + 1 ~= i and self.buddyTab:GetTabGoByIndex(i - 1).active and not self.tabId2BuddyId[i] then
            local data = XinBaiLianDong_YaoBan.GetData(1000 + i)
            local score = LuaXinBaiLianDongMgr.SYJYInfo.score
            local jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade

            if not ((data.NeedScore > 0 and score < data.NeedScore) or (data.NeedLingYu > 0 and jade < data.NeedLingYu) or
                (data.NeedTask > 0 and not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(data.NeedTask))) then
                if i == 1 or self:IsMysteriousBuddyOpen() then
                    redDot:SetActive(true)
                end
            end
        end
    end
end

-- 更新图标显示
function LuaShanYaoJieYuanWnd:UpdateIcon()
    for id = 1, 3 do
        local buddy = self.buddyTab:GetTabGoByIndex(id - 1)
        local tex = buddy.transform:GetComponent(typeof(CUITexture))

        if id > 1 then
            local outline = buddy.transform:Find("Outline").gameObject
            if self.tabId2BuddyId[id] then
                tex:LoadMaterial(XinBaiLianDong_YaoBan.GetData(self.tabId2BuddyId[id]).Icon)
                outline:SetActive(false)
            else
                tex:LoadMaterial("")
                outline:SetActive(true)
            end
        else
            local data = XinBaiLianDong_YaoBan.GetData(1000 + id)
            tex:LoadMaterial(self.tabId2BuddyId[id] and data.Icon or data.LockedIcon)
        end
    end
end

-- 更新跟随的显示
function LuaShanYaoJieYuanWnd:UpdateFollow()
    local followId = LuaXinBaiLianDongMgr.SYJYInfo.followId
    for i = 1, 3 do
        self:SetFollowActive(i, false)
    end

    if followId and self.buddyId2TabId[followId] then
        self:SetFollowActive(self.buddyId2TabId[followId], true)
    end
end

-- 设置是否显示跟随
function LuaShanYaoJieYuanWnd:SetFollowActive(id, active)
    self.buddyTab:GetTabGoByIndex(id - 1).transform:Find("Follow").gameObject:SetActive(active)
end

-- 更新解锁的显示
function LuaShanYaoJieYuanWnd:UpdateUnlock()
    if not self.tabId2BuddyId then return end

    for tabId, buddyId in pairs(self.tabId2BuddyId) do
        self:SetUnlockActive(tabId, true)
    end
end

-- 设置是否显示跟随
function LuaShanYaoJieYuanWnd:SetUnlockActive(id, active)
    self.buddyTab:GetTabGoByIndex(id - 1).transform:Find("Unlock").gameObject:SetActive(active)
end

-- 更新积分
function LuaShanYaoJieYuanWnd:UpdateScore()
    self.score.text = LuaXinBaiLianDongMgr.SYJYInfo.score
end


function LuaShanYaoJieYuanWnd:InitModelLoader()
    self.modelLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        ro.Scale = 1
        self.ro = ro
        self:LoadModel(ro)
    end)
    self.modelIdentifier = SafeStringFormat3("__%s__", tostring(self.model.gameObject:GetInstanceID()))
end

-- 更新显示的模型
function LuaShanYaoJieYuanWnd:UpdateModel()
    local tabId = self.buddyTab.SelectedIndex + 1
    local buddyId = self.tabId2BuddyId[tabId]
    if buddyId then
        self.model.gameObject:SetActive(true)
        self.outline.gameObject:SetActive(false)
        self.model.mainTexture = CUIManager.CreateModelTexture(self.modelIdentifier, self.modelLoader, 180, 0.05, -1, 4.66, false, true, 2, true, false)
        self:SetRotation(180)

        -- 添加特效
        local fxId = XinBaiLianDong_YaoBan.GetData(buddyId).Fx
        local fx = CEffectMgr.Inst:AddObjectFX(fxId, self.ro, 0, 2, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        self.ro:AddFX("SelfFx", fx)
    else
        self.model.gameObject:SetActive(false)
        self.outline.gameObject:SetActive(true)
        self.outline:LoadMaterial(XinBaiLianDong_YaoBan.GetData(tabId + 1000).Outline)
    end
end

-- 载入模型
function LuaShanYaoJieYuanWnd:LoadModel(ro)
    local modelPath = XinBaiLianDong_YaoBan.GetData(self.tabId2BuddyId[self.buddyTab.SelectedIndex + 1]).Model
    ro:LoadMain(modelPath, nil, false, false, false)
end

-- 更新名字
function LuaShanYaoJieYuanWnd:UpdateName()
    local buddyId = self.tabId2BuddyId[self.buddyTab.SelectedIndex + 1]

    if buddyId then
        self.buddyName.text = XinBaiLianDong_YaoBan.GetData(buddyId).UnlockName
    else
        self.buddyName.text = XinBaiLianDong_YaoBan.GetData(self.buddyTab.SelectedIndex + 1001).Name
    end
end

-- 更新气泡显示
function LuaShanYaoJieYuanWnd:UpdateBubble()
    local buddyId = self.tabId2BuddyId[self.buddyTab.SelectedIndex + 1]

    if buddyId then
        self.word.text = XinBaiLianDong_YaoBan.GetData(buddyId).UnlockDialog
        self.word.gameObject:SetActive(true)
    else
        local lockDialog = XinBaiLianDong_YaoBan.GetData(self.buddyTab.SelectedIndex + 1001).LockDialog
        if System.String.IsNullOrEmpty(lockDialog) then
            self.word.gameObject:SetActive(false)
        else
            self.word.text = lockDialog
            self.word.gameObject:SetActive(true)
        end
    end

    self.word:UpdateNGUIText()
    NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(self.word.text)
    self.word.width = size.x > 453 and 453 or math.ceil(size.x)
    self.bubble:ResetAndUpdateAnchors()

    local tweenAlpha = self.word.transform:GetComponent(typeof(TweenAlpha))
    tweenAlpha.tweenFactor = 0
    tweenAlpha:PlayForward()

    self:ClearTick()
    self.bubbleTick = RegisterTickOnce(function()
        tweenAlpha.tweenFactor = 1
        tweenAlpha:PlayReverse()
    end, 10000)
end

-- 更新解锁与跟随按钮的显示 切换解锁妖伴--跟随--取消跟随
function LuaShanYaoJieYuanWnd:UpdateUnlockButton()
    local buddyId = self.tabId2BuddyId[self.buddyTab.SelectedIndex + 1]
    local followId = LuaXinBaiLianDongMgr.SYJYInfo.followId

    self.unlockFx:DestroyFx()
    if buddyId then
        self.unlockButton.gameObject:SetActive(false)
        self.followButton.gameObject:SetActive(true)
        if followId and followId == buddyId then
            self.followButton.label.text = LocalString.GetString("取消跟随")
        else
            self.followButton.label.text = LocalString.GetString("召唤跟随")
        end
    else
        self.unlockButton.gameObject:SetActive(true)
        self.followButton.gameObject:SetActive(false)

        -- 解锁按键扫光特效
        local data = XinBaiLianDong_YaoBan.GetData(self.buddyTab.SelectedIndex + 1001)
        local score = LuaXinBaiLianDongMgr.SYJYInfo.score
        local jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade

        if not ((data.NeedScore > 0 and score < data.NeedScore) or (data.NeedLingYu > 0 and jade < data.NeedLingYu) or
            (data.NeedTask > 0 and not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(data.NeedTask))) then
            self.unlockFx:LoadFx(g_UIFxPaths.ShanYanJieYuanSaoGuangFxPath)
        end
    end
end

-- 更新解锁条件
function LuaShanYaoJieYuanWnd:UpdateUnlockCondition()
    local buddyId = self.tabId2BuddyId[self.buddyTab.SelectedIndex + 1]
    if buddyId then
        self.unlockCondition.gameObject:SetActive(false)
    else
        self.unlockCondition.gameObject:SetActive(true)

        local data = XinBaiLianDong_YaoBan.GetData(self.buddyTab.SelectedIndex + 1001)
        local score = LuaXinBaiLianDongMgr.SYJYInfo.score
        local needScore = SafeStringFormat3("[%s]%s[-]", score >= data.NeedScore and "00FF00" or "FF0000", data.NeedScore)
        local jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
        local needLingYu = SafeStringFormat3("[%s]%s[-]", jade >= data.NeedLingYu and "00FF00" or "FF0000", data.NeedLingYu)

        local str = ""
        if data.NeedScore > 0 and data.NeedLingYu > 0 then
            str = SafeStringFormat3(LocalString.GetString("%s妖灵积分 + %s灵玉"), needScore, needLingYu)
        elseif data.NeedScore > 0 then
            str = SafeStringFormat3(LocalString.GetString("%s妖灵积分"), needScore)
        elseif data.NeedLingYu > 0 then
            str = SafeStringFormat3(LocalString.GetString("%s灵玉"), needLingYu)
        end

        if data.NeedTask > 0 then
            local taskName = Task_Task.GetData(data.NeedTask).Display
            local isTaskFinish = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(data.NeedTask)
            local needTask = SafeStringFormat3(LocalString.GetString("[%s]%s[-]"), isTaskFinish and "00FF00" or "FF0000", taskName)
            self.unlockCondition.text = SafeStringFormat3(LocalString.GetString("%s\n完成任务%s"), str, needTask)
        else
            self.unlockCondition.text = str
        end
    end
end

-- 更新查看故事按钮显示
function LuaShanYaoJieYuanWnd:UpdateStoryButton()
    local tabId = self.buddyTab.SelectedIndex + 1
    if tabId > 1 and not self.tabId2BuddyId[tabId] then
        self.storyButton.gameObject:SetActive(false)
    else
        self.storyButton.gameObject:SetActive(true)
    end
end

-- 更新分享按钮显示
function LuaShanYaoJieYuanWnd:UpdateShareButton()
    local buddyId = self.tabId2BuddyId[self.buddyTab.SelectedIndex + 1]
    self.shareButton.gameObject:SetActive(buddyId and true or false)
end

function LuaShanYaoJieYuanWnd:ClearTick()
    if self.bubbleTick then
        UnRegisterTick(self.bubbleTick)
        self.bubbleTick = nil
    end
end

function LuaShanYaoJieYuanWnd:OnDestroy()
    self:ClearTick()
    CUIManager.DestroyModelTexture(self.modelIdentifier)
end

--@region UIEvent

function LuaShanYaoJieYuanWnd:OnStoryButtonClick()
    local tabId = self.buddyTab.SelectedIndex + 1
    if tabId == 1 then
        local msg = XinBaiLianDong_YaoBan.GetData(1000 + tabId).StoryMsg
        LuaXinBaiLianDongMgr:ShowShanYaoJieYuanStoryWnd(msg)
    else
        if self.tabId2BuddyId[tabId] then
            local msg = XinBaiLianDong_YaoBan.GetData(self.tabId2BuddyId[tabId]).StoryMsg
            LuaXinBaiLianDongMgr:ShowShanYaoJieYuanStoryWnd(msg)
        end
    end
end

function LuaShanYaoJieYuanWnd:OnUnlockButtonClick()
    local tabId = self.buddyTab.SelectedIndex + 1
    local buddyId = self.tabId2BuddyId[tabId]
    if buddyId then return end

    local str = ""
    local data = XinBaiLianDong_YaoBan.GetData(tabId + 1000)
    if data.NeedScore > 0 and data.NeedLingYu > 0 then
        str = SafeStringFormat3(LocalString.GetString("%d妖灵积分和%d灵玉"), data.NeedScore, data.NeedLingYu)
    elseif data.NeedScore > 0 then
        str = SafeStringFormat3(LocalString.GetString("%d妖灵积分"), data.NeedScore)
    elseif data.NeedLingYu > 0 then
        str = SafeStringFormat3(LocalString.GetString("%d灵玉"), data.NeedLingYu)
    end

    local date = CServerTimeMgr.Inst:GetTimeStampByStr(XinBaiLianDong_Setting.GetData().YaoBan_SpecialMessageDate)
    local needSpecialMsg = (CServerTimeMgr.Inst.timeStamp > date) and (data.NeedLingYu > 0)
    if tabId == 1 then
        local msgName = needSpecialMsg and "SHANYAOJIEYUAN_UNLOCK_CONFIRM2" or "SHANYAOJIEYUAN_UNLOCK_CONFIRM"
        local msg = g_MessageMgr:FormatMessage(msgName, str)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
            Gac2Gas.YaoBan_Unlock(tabId + 1000)
        end), nil, nil, nil, false)
    else
        local msgName = needSpecialMsg and "SHANYAOJIEYUAN_UNLOCK_RANDOM_CONFIRM2" or "SHANYAOJIEYUAN_UNLOCK_RANDOM_CONFIRM"
        local msg = g_MessageMgr:FormatMessage(msgName, str)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
            Gac2Gas.YaoBan_RandomUnlock()
        end), nil, nil, nil, false)
    end
end

function LuaShanYaoJieYuanWnd:OnFollowButtonClick()
    local tabId = self.buddyTab.SelectedIndex + 1
    local buddyId = self.tabId2BuddyId[tabId]
    if not buddyId then return end

    local followId = LuaXinBaiLianDongMgr.SYJYInfo.followId
    if followId and followId == buddyId then
        Gac2Gas.YaoBan_Disable(buddyId)
    else
        Gac2Gas.YaoBan_Enable(buddyId)
    end
end

function LuaShanYaoJieYuanWnd:OnTabChangeValidate(index)
    if index > 0 and not self:IsMysteriousBuddyOpen() then
        g_MessageMgr:ShowMessage("SHANYAOJIEYUAN_CLICK_INVALID_YAOBAN")
        return false
    end
    return true
end

function LuaShanYaoJieYuanWnd:OnTabChange(go, index)
    self:UpdateModel()
    self:UpdateName()
    self:UpdateUnlockCondition()
    self:UpdateUnlockButton()
    self:UpdateBubble()
    self:UpdateStoryButton()
    self:UpdateShareButton()

    self.buddyTab:GetTabGoByIndex(index).transform:Find("RedDot").gameObject:SetActive(false)
end

function LuaShanYaoJieYuanWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("SHANYAOJIEYUAN_RULE_TIPS")
end

function LuaShanYaoJieYuanWnd:OnModelDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
    self:SetRotation(self.rotation + rot)
end

function LuaShanYaoJieYuanWnd:OnShareButtonClick()
    local tabId = self.buddyTab.SelectedIndex + 1
    local buddyId = self.tabId2BuddyId[tabId]
    if not buddyId then return end

    local formatStr = MessageMgr.Inst:GetMessageFormat("SHANYAOJIEYUAN_SHARE_LINK", false)
    local msg = System.String.Format(formatStr, tostring(buddyId))
    LuaInGameShareMgr.Share(bit.bor(EnumInGameShareChannelDefine.eWorld, EnumInGameShareChannelDefine.eGuild, EnumInGameShareChannelDefine.eTeam, EnumInGameShareChannelDefine.eCurrent), function(channel)
        return msg
    end, self.shareButton.transform, CPopupMenuInfoMgrAlignType.Right, nil)
end

--@endregion UIEvent

-- 神秘妖伴是否开启
function LuaShanYaoJieYuanWnd:IsMysteriousBuddyOpen()
    return XinBaiLianDong_YaoBan.GetData(1002).Status == 0 and XinBaiLianDong_YaoBan.GetData(1003).Status == 0
end

-- 设置模型旋转
function LuaShanYaoJieYuanWnd:SetRotation(val)
    self.rotation = val
    CUIManager.SetModelRotation(self.modelIdentifier, val)
end

