local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"

LuaGuildTerritorialWarsBeiZhanWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanWnd, "GuildTerritorialWarsBeiZhanView", "GuildTerritorialWarsBeiZhanView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanWnd, "GuildTerritorialWarsGongFengView", "GuildTerritorialWarsGongFengView", GameObject)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsBeiZhanWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsBeiZhanWnd:Init()
    self.Tabs:ChangeTab(0,false)
end

--@region UIEvent

function LuaGuildTerritorialWarsBeiZhanWnd:OnTabsTabChange(index)
    self.GuildTerritorialWarsBeiZhanView.gameObject:SetActive(index == 0)
    self.GuildTerritorialWarsGongFengView.gameObject:SetActive(index == 1)
end


--@endregion UIEvent

