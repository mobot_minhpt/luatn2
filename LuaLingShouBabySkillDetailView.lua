require("common/common_include")
require("ui/lingshou/LuaLingShouBabySkillSlot")
require("ui/lingshou/LuaLingShouBabyCostMatItem")


local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CGetMoneyMgr=import "L10.UI.CGetMoneyMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
-- local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local MessageMgr=import "L10.Game.MessageMgr"
local LingShouBaby_BabySkillOfQuality=import "L10.Game.LingShouBaby_BabySkillOfQuality"

CLuaLingShouBabySkillDetailView =class()
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_UpgradeButton")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_SkillSlot1")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_SkillSlot2")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_Skill1Template")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_Skill2Template")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_SelectedSkillId")

RegistClassMember(CLuaLingShouBabySkillDetailView,"m_MoneyCtrl")

RegistClassMember(CLuaLingShouBabySkillDetailView,"m_Icon")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_Mask")
-- RegistClassMember(CLuaLingShouBabySkillDetailView,"m_ItemCountUpdate")
-- RegistClassMember(CLuaLingShouBabySkillDetailView,"m_CostTemplateId")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_NeedCount")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_Cost")

RegistClassMember(CLuaLingShouBabySkillDetailView,"m_TopLevelNode")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_TopLevelLevelLabel")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_NormalNode")

RegistClassMember(CLuaLingShouBabySkillDetailView,"m_BabyInfo")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_CostItems")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_CostGrid")
RegistClassMember(CLuaLingShouBabySkillDetailView,"m_CostTemplate")
-- function CLuaLingShouBabySkillDetailView:ctor()
-- end

function CLuaLingShouBabySkillDetailView:IsEnough()
    for i,v in ipairs(self.m_CostItems) do
        if not v:IsEnough() then
            return false
        end
    end
    return true
end

function CLuaLingShouBabySkillDetailView:Init( tf )
    self.m_CostItems={}
    self.m_SelectedSkillId=0
    self.m_SkillSlot1=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot1:Init(LuaGameObject.GetChildNoGC(tf,"Skill1").transform)
    self.m_SkillSlot2=CLuaLingShouBabySkillSlot:new()
    self.m_SkillSlot2:Init(LuaGameObject.GetChildNoGC(tf,"Skill2").transform)

    self.m_CostGrid=LuaGameObject.GetChildNoGC(tf,"CostGrid").grid
    self.m_CostTemplate=LuaGameObject.GetChildNoGC(tf,"CostTemplate").gameObject
    self.m_CostTemplate:SetActive(false)

    self.m_NormalNode=LuaGameObject.GetChildNoGC(tf,"Normal").gameObject
    self.m_TopLevelNode=LuaGameObject.GetChildNoGC(tf,"TopLevel").gameObject
    self.m_TopLevelLevelLabel=LuaGameObject.GetChildNoGC(self.m_TopLevelNode.transform,"Label").label
    self.m_UpgradeButton=LuaGameObject.GetChildNoGC(tf,"UpgradeBtn").gameObject

    self.m_Icon=LuaGameObject.GetChildNoGC(tf,"MatIcon").cTexture
    self.m_Mask=LuaGameObject.GetChildNoGC(tf,"Mask").gameObject

    self.m_MoneyCtrl=LuaGameObject.GetChildNoGC(tf,"QnCostAndOwnMoney").moneyCtrl
    self.m_MoneyCtrl:SetCost(0)
    local g = LuaGameObject.GetChildNoGC(tf,"UpgradeBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_SelectedSkillId <= 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择要升级的技能"))
            --g_MessageMgr:ShowMessage("LINGSHOU_BABY_SKILL_SELECT_SKILL")
            return
        end
        --升级
        for i,v in ipairs(self.m_CostItems) do
            if not v:IsEnough() then
                CItemAccessListMgr.Inst:ShowItemAccessInfo(v.m_CostTemplateId, false, go.transform)
                return
            end
        end

        if not self.m_MoneyCtrl.moneyEnough then
            CGetMoneyMgr.Inst:GetYinLiang()
            return
        end
        if self.m_SelectedSkillId>0 then
            Gac2Gas.RequestUpgradeBabyLingShouSkill(CLingShouMgr.Inst.selectedLingShou,self.m_SelectedSkillId,false)
        end
    end)

    local function OnClick(go)
        self:OnClickSkillSlot(go)
    end
    CommonDefs.AddOnClickListener(self.m_SkillSlot1.m_GameObject,DelegateFactory.Action_GameObject(OnClick),true)
    CommonDefs.AddOnClickListener(self.m_SkillSlot2.m_GameObject,DelegateFactory.Action_GameObject(OnClick),true)
end
function CLuaLingShouBabySkillDetailView:OnClickSkillSlot(go)
    --初始设置
    self.m_TopLevelLevelLabel.text=LocalString.GetString("技能已满级！")
    
    if go==self.m_SkillSlot1.m_GameObject then
        self.m_SelectedSkillId=self.m_Skill1Template
        self.m_SkillSlot1:SetSelected(true)
        self.m_SkillSlot2:SetSelected(false)
        self:UpdateSelectedSkillId(self.m_Skill1Template,1)
    elseif go==self.m_SkillSlot2.m_GameObject then
        self.m_SelectedSkillId=self.m_Skill2Template
        self.m_SkillSlot1:SetSelected(false)
        self.m_SkillSlot2:SetSelected(true)
        self:UpdateSelectedSkillId(self.m_Skill2Template,2)
    end
end

function CLuaLingShouBabySkillDetailView:InitData( babyInfo )
    -----self.m_SelectedSkillId = 0
    self.m_BabyInfo=babyInfo

    local skillId1=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[1])
    local skillId2=CLuaLingShouMgr.ProcessSkillId(babyInfo.Quality,babyInfo.SkillList[2])
    self.m_SkillSlot1:InitData(skillId1)
    self.m_SkillSlot2:InitData(skillId2)
    
    self.m_Skill1Template=skillId1--babyInfo.SkillList[1]
    self.m_Skill2Template=skillId2--babyInfo.SkillList[2]
    

    self.m_SkillSlot1:SetSelected(false)
    self.m_SkillSlot2:SetSelected(false)
    local skillCls=math.floor(self.m_SelectedSkillId/100)

    if skillCls==0 and self.m_Skill1Template>0 then
        self:OnClickSkillSlot(self.m_SkillSlot1.m_GameObject)
    elseif skillCls==0 and self.m_Skill2Template>0 then
        self:OnClickSkillSlot(self.m_SkillSlot2.m_GameObject)
    elseif skillCls>0 and math.floor(self.m_Skill1Template/100)==skillCls then
        self:OnClickSkillSlot(self.m_SkillSlot1.m_GameObject)
    elseif skillCls>0 and math.floor(self.m_Skill2Template/100)==skillCls then
        self:OnClickSkillSlot(self.m_SkillSlot2.m_GameObject)
    else
        self.m_NormalNode:SetActive(false)
        self.m_TopLevelNode:SetActive(false)
        --CUICommonDef.SetActive(self.m_UpgradeButton,false,true)
    end
end
function CLuaLingShouBabySkillDetailView:UpdateSelectedSkillId( skillId ,slotIndex)
    CUICommonDef.ClearTransform(self.m_CostGrid.transform)
    self.m_CostItems={}


    self.m_SelectedSkillId=skillId
    local skillTemplate = Skill_AllSkills.GetData(skillId)
    if skillTemplate then
        local nextSkillTemplate = Skill_AllSkills.GetData(skillId + 1)
        -- local items=skillTemplate.Item
        if nextSkillTemplate then
            local items=nextSkillTemplate.Item
            if items then
                for i=1,items.Length/2 do
                    local count=items[(i-1)*2+1]
                    if count>0 then
                        local go=NGUITools.AddChild(self.m_CostGrid.gameObject,self.m_CostTemplate)
                        go:SetActive(true)
                        local cmp=CLuaLingShouBabyCostMatItem:new()
                        cmp:Init(go.transform,items[(i-1)*2],count)
                        table.insert( self.m_CostItems,cmp )
                    end
                end
                self.m_CostGrid:Reposition()
            end

            self.m_Cost=0
            -- if nextSkillTemplate then
            self.m_NormalNode:SetActive(true)
            self.m_TopLevelNode:SetActive(false)
            
            self.m_Cost=nextSkillTemplate.Money
            CUICommonDef.SetActive(self.m_UpgradeButton,true,true)

            self.m_MoneyCtrl:SetCost(self.m_Cost)

            -- if slotIndex==1 then
                --乙级 并且等级>=20,<30 提示文字
                --如果等级==30 提示文字
                local level=skillId%100
                local needAlert=false
                local quality=self.m_BabyInfo.Quality
                if quality<=5 then
                    local maxUpgradeLv=LingShouBaby_BabySkillOfQuality.GetData(quality).MaxSkillLv
                    if level>=maxUpgradeLv and level<50 then
                        needAlert=true
                    end
                end
                
                if needAlert then
                    self.m_TopLevelLevelLabel.text=MessageMgr.Inst:FormatMessage("LSBB_SKILL_UPGRADE_TIP",{})
                    
                    self.m_NormalNode:SetActive(false)
                    self.m_TopLevelNode:SetActive(true)

                    CUICommonDef.SetActive(self.m_UpgradeButton,false,true)
                end
            -- end
        else
            --满级
            self.m_NormalNode:SetActive(false)
            self.m_TopLevelNode:SetActive(true)
            CUICommonDef.SetActive(self.m_UpgradeButton,false,true)
            self.m_TopLevelLevelLabel.text=LocalString.GetString("技能已满级！")
        end
    else
        self.m_NormalNode:SetActive(false)
        self.m_TopLevelNode:SetActive(false)
        CUICommonDef.SetActive(self.m_UpgradeButton,false,true)
    end
end

return CLuaLingShouBabySkillDetailView
