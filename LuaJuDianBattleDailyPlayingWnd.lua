local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaJuDianBattleDailyPlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleDailyPlayingWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaJuDianBattleDailyPlayingWnd, "JuDianBattleDailyBtn", "JuDianBattleDailyBtn", GameObject)
RegistChildComponent(LuaJuDianBattleDailyPlayingWnd, "JuDianBattleWorldMapBtn", "JuDianBattleWorldMapBtn", GameObject)
RegistChildComponent(LuaJuDianBattleDailyPlayingWnd, "BuffBtn", "BuffBtn", GameObject)

--@endregion RegistChildComponent end

function LuaJuDianBattleDailyPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BuffBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuffBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleDailyPlayingWnd:Init()
	Extensions.RemoveAllChildren(self.Grid.transform)

    self.JuDianBattleDailyBtn:SetActive(false)
    self.JuDianBattleWorldMapBtn:SetActive(false)

	
    if LuaJuDianBattleMgr:IsInDailyGameplay() then
        local go = NGUITools.AddChild(self.Grid.gameObject, self.JuDianBattleDailyBtn)
        go:SetActive(true)
    end

    if LuaJuDianBattleMgr:IsInGameplay() or LuaJuDianBattleMgr:IsInDailyGameplay() 
        or LuaJuDianBattleMgr:IsInJuDian() then
        local go = NGUITools.AddChild(self.Grid.gameObject, self.JuDianBattleWorldMapBtn)
        
        go:SetActive(true)
    end
    if CClientMainPlayer.Inst then
        self:OnBuffChange(CClientMainPlayer.Inst.EngineId)
    end
    self.Grid:Reposition()
end

function LuaJuDianBattleDailyPlayingWnd:OnBuffChange(engineId)
    if CClientMainPlayer.Inst and engineId == CClientMainPlayer.Inst.EngineId then
        local obj = CClientMainPlayer.Inst
        local buffProp = obj.BuffProp
        if self.m_BuffId == nil then
            self.m_BuffId = GuildOccupationWar_Setting.GetData().FlagOwnerBuffId
        end
        if buffProp and buffProp.Buffs then
            if CommonDefs.DictContains(buffProp.Buffs, typeof(UInt32), self.m_BuffId) then
                self.BuffBtn:SetActive(true)
            else
                self.BuffBtn:SetActive(false)
            end
        end
    end
end

function LuaJuDianBattleDailyPlayingWnd:OnEnable()
	self:Init()
    if self.m_BuffAction == nil then
        self.m_BuffAction = DelegateFactory.Action_uint(function (engineId)
            self:OnBuffChange(engineId)
        end)
    end
    EventManager.AddListenerInternal(EnumEventType.OnBuffInfoUpdate, self.m_BuffAction)
end

function LuaJuDianBattleDailyPlayingWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.OnBuffInfoUpdate, self.m_BuffAction)
end

--@region UIEvent


function LuaJuDianBattleDailyPlayingWnd:OnBuffBtnClick()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GUILD_JUDIAN_THROW_FLAG_CONFIRM"), function()
        Gac2Gas.GuildJuDianRequestDropFlag()
    end, nil, nil, nil, false)
end


--@endregion UIEvent

