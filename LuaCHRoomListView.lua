local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCommonSelector = import "L10.UI.CCommonSelector"
local UIInput = import "UIInput"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local NGUIText = import "NGUIText"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltipAlignType= import "L10.UI.CTooltip+AlignType"
local Vector3 = import "UnityEngine.Vector3"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaCHRoomListView = class()

RegistClassMember(LuaCHRoomListView, "m_RoomTypeSelector") --房间类型选择
RegistClassMember(LuaCHRoomListView, "m_SearchButton") --搜索按钮
RegistClassMember(LuaCHRoomListView, "m_PublicRoomButton") --公共房间按钮
RegistClassMember(LuaCHRoomListView, "m_PrivateRoomButton") --私密房间按钮
RegistClassMember(LuaCHRoomListView, "m_HeatSortButton") --热度排序按钮
RegistClassMember(LuaCHRoomListView, "m_TimeSortButton") --时间排序按钮
RegistClassMember(LuaCHRoomListView, "m_RankButton") --时间排序按钮

RegistClassMember(LuaCHRoomListView, "m_SimpleTableView")
RegistClassMember(LuaCHRoomListView, "m_DefaultSimpleTableViewDataSource")
RegistClassMember(LuaCHRoomListView, "m_RoomTemplate")
RegistClassMember(LuaCHRoomListView, "m_TipLabel")

RegistClassMember(LuaCHRoomListView, "m_BottomRootTable")
RegistClassMember(LuaCHRoomListView, "m_CreateRoomRoot")
RegistClassMember(LuaCHRoomListView, "m_NextPageButton")
RegistClassMember(LuaCHRoomListView, "m_PrevPageButton")
RegistClassMember(LuaCHRoomListView, "m_CurPageLabel")
RegistClassMember(LuaCHRoomListView, "m_CreateRoomButton") --创建房间
RegistClassMember(LuaCHRoomListView, "m_CurRoomRoot")
RegistClassMember(LuaCHRoomListView, "m_ButtonTable") --房间内操作列表
RegistClassMember(LuaCHRoomListView, "m_LeaveRoomButton") --离开房间
RegistClassMember(LuaCHRoomListView, "m_DealWithApplyButton") --处理上麦请求
RegistClassMember(LuaCHRoomListView, "m_LeaveMicButton") -- 下麦
RegistClassMember(LuaCHRoomListView, "m_MuteMicButton") -- 开/闭 麦
RegistClassMember(LuaCHRoomListView, "m_ApplyMicButton") --申请/取消申请 上麦
RegistClassMember(LuaCHRoomListView, "m_SearchRoomRoot") --返回查询
RegistClassMember(LuaCHRoomListView, "m_SearchReturnButton") --返回查询按钮

RegistClassMember(LuaCHRoomListView, "m_AllData")
RegistClassMember(LuaCHRoomListView, "m_RoomTypeNameList")
RegistClassMember(LuaCHRoomListView, "m_RoomTypeIndex")
RegistClassMember(LuaCHRoomListView, "m_CurPageIndex")
RegistClassMember(LuaCHRoomListView, "m_IsInSearch")

function LuaCHRoomListView:Awake()
    self.m_RoomTypeSelector = self.transform:Find("Header/RoomTypeSelector"):GetComponent(typeof(CCommonSelector))
    self.m_SearchButton = self.transform:Find("Header/SearchBar/SearchButton").gameObject
    self.m_RoomTemplate = self.transform:Find("Content/ScrollView/Pool/RoomTemplate").gameObject
    self.m_TipLabel = self.transform:Find("Content/ContentBg/TipLabel").gameObject
    self.m_SimpleTableView = self.transform:Find("Content/ScrollView"):GetComponent(typeof(UISimpleTableView))
    self.m_PublicRoomButton = self.transform:Find("Header/ClassType/PublicRoomButton").gameObject
    self.m_PrivateRoomButton = self.transform:Find("Header/ClassType/PrivateRoomButton").gameObject
    self.m_HeatSortButton = self.transform:Find("Header/SortType/HeatSortButton").gameObject
    self.m_TimeSortButton = self.transform:Find("Header/SortType/TimeSortButton").gameObject
    self.m_RankButton = self.transform:Find("Header/RankButton").gameObject

    self.m_BottomRootTable = self.transform:Find("Bottom/Panel/Table"):GetComponent(typeof(UITable))
    self.m_CreateRoomRoot = self.transform:Find("Bottom/Panel/Table/CreateRoom").gameObject
    self.m_CreateRoomButton = self.m_CreateRoomRoot.transform:Find("CreateRoomButton").gameObject
    self.m_NextPageButton = self.m_CreateRoomRoot.transform:Find("Page/NextPageButton").gameObject
    self.m_PrevPageButton = self.m_CreateRoomRoot.transform:Find("Page/PrevPageButton").gameObject
    self.m_CurPageLabel = self.m_CreateRoomRoot.transform:Find("Page/Label"):GetComponent(typeof(UILabel))
    self.m_CurRoomRoot = self.transform:Find("Bottom/Panel/Table/CurRoom").gameObject
    self.m_CurRoomSpecialBg = self.transform:Find("Bottom/Panel/Table/CurRoom/SpecialBg").gameObject
    self.m_ButtonTable = self.transform:Find("Bottom/Panel/Table/CurRoom/Table"):GetComponent(typeof(UITable))
    self.m_RoomNamePanel = self.transform:Find("Bottom/Panel/Table/CurRoom/RoomNamePanel"):GetComponent(typeof(CCommonLuaScript))
    self.m_LeaveRoomButton = self.m_ButtonTable.transform:Find("LeaveRoomButton").gameObject
    self.m_DealWithApplyButton = self.m_ButtonTable.transform:Find("DealWithApplyButton").gameObject
    self.m_LeaveMicButton = self.m_ButtonTable.transform:Find("LeaveMicButton").gameObject
    self.m_MuteMicButton = self.m_ButtonTable.transform:Find("MuteMicButton").gameObject
    self.m_ApplyMicButton = self.m_ButtonTable.transform:Find("ApplyMicButton").gameObject
    self.m_SearchRoomRoot = self.transform:Find("Bottom/Panel/Table/SearchRoom").gameObject
    self.m_SearchReturnButton = self.m_SearchRoomRoot.transform:Find("ReturnButton").gameObject

    self.m_RoomNamePanelMaxWidth = self.m_RoomNamePanel:GetComponent(typeof(UIPanel)).width
    self.m_RoomTypeNameList = LuaClubHouseMgr:GetRoomTypeNameList()
    self.m_RoomTypeIndex = 0
    self.m_RoomTypeSelector:Init(self.m_RoomTypeNameList, DelegateFactory.Action_int(function ( index )
        self.m_RoomTypeIndex = index
        local roomTypeName = self.m_RoomTypeNameList[index]
        self.m_RoomTypeSelector:SetName(roomTypeName)
        LuaClubHouseMgr:SetSelectedRoomTypeName(roomTypeName)
        self:QueryRoomList("ontypechanged", 1)
    end))

    if LuaClubHouseMgr:IsInWinSocialWnd() then
        self.m_RoomTypeSelector:SetShowInCustomWorldPos(true, Vector3(1,self.m_RoomTypeSelector.transform.position.y,0))
    end
    local roomTypeName = self.m_RoomTypeNameList[self.m_RoomTypeIndex]
    self.m_RoomTypeSelector:SetName(roomTypeName)
    LuaClubHouseMgr:SetSelectedRoomTypeName(roomTypeName)
    self:InitTableView()
    self:RefreshClearSearchButtonVisibility()
    
    CommonDefs.AddOnClickListener(self.m_SearchButton, DelegateFactory.Action_GameObject(function(go) self:OnSearchButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_CreateRoomButton, DelegateFactory.Action_GameObject(function(go) self:OnCreateRoomButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_NextPageButton, DelegateFactory.Action_GameObject(function(go) self:OnNextPageButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_PrevPageButton, DelegateFactory.Action_GameObject(function(go) self:OnPrevPageButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_CurPageLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnCurPageLabelClick() end), false)
    CommonDefs.AddOnClickListener(self.m_LeaveRoomButton, DelegateFactory.Action_GameObject(function(go) self:OnLeaveRoomButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_DealWithApplyButton, DelegateFactory.Action_GameObject(function(go) self:OnDealWithApplyButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_LeaveMicButton, DelegateFactory.Action_GameObject(function(go) self:OnLeaveMicButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_MuteMicButton, DelegateFactory.Action_GameObject(function(go) self:OnMuteMicButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_ApplyMicButton, DelegateFactory.Action_GameObject(function(go) self:OnApplyMicButtonClick() end), false)

    CommonDefs.AddOnClickListener(self.m_CurRoomRoot, DelegateFactory.Action_GameObject(function(go) self:OnCurRoomRootClick() end), false)
    CommonDefs.AddOnClickListener(self.m_SearchReturnButton, DelegateFactory.Action_GameObject(function(go) self:OnSearchReturnButtonClick() end), false)

    CommonDefs.AddOnClickListener(self.m_PublicRoomButton, DelegateFactory.Action_GameObject(function(go) self:OnSortButtonClick(EnumCHRoomType.apponly, LuaClubHouseMgr.m_RoomSortType) end), false)
    CommonDefs.AddOnClickListener(self.m_PrivateRoomButton, DelegateFactory.Action_GameObject(function(go) self:OnSortButtonClick(EnumCHRoomType.private, LuaClubHouseMgr.m_RoomSortType) end), false)
    CommonDefs.AddOnClickListener(self.m_HeatSortButton, DelegateFactory.Action_GameObject(function(go) self:OnSortButtonClick(LuaClubHouseMgr.m_RoomQueryType, EnumCHSortType.heat) end), false)
    CommonDefs.AddOnClickListener(self.m_TimeSortButton, DelegateFactory.Action_GameObject(function(go) self:OnSortButtonClick(LuaClubHouseMgr.m_RoomQueryType, EnumCHSortType.time) end), false)
    CommonDefs.AddOnClickListener(self.m_RankButton, DelegateFactory.Action_GameObject(function(go) self:OnRankButtonClick() end), false)

    self.m_CurPageIndex = 1
end

function LuaCHRoomListView:Init()
    -- self:QueryRoomList("init")
    self.m_RoomTemplate.gameObject:SetActive(false)
    self:OnSortButtonClick(LuaClubHouseMgr.m_RoomQueryType, LuaClubHouseMgr.m_RoomSortType)
    self:InitBottomDisplay()
end

function LuaCHRoomListView:UpdatePageIndex(pageIndex)
    self.m_CurPageIndex = pageIndex>0 and pageIndex or 1
    self.m_PrevPageButton:GetComponent(typeof(CButton)).Enabled = (self.m_CurPageIndex>1)
    self.m_CurPageLabel.text = tostring(self.m_CurPageIndex)
end
---------------------------------------------------------
-- UI表现
---------------------------------------------------------
function LuaCHRoomListView:RefreshClearSearchButtonVisibility()
end
---------------------------------------------------------
-- 列表初始化
---------------------------------------------------------
function LuaCHRoomListView:InitTableView()
    self.m_AllData = {}
    if not self.m_DefaultSimpleTableViewDataSource then

		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return self:NumberOfRows()
		end,
		function ( index )
			return self:CellForRowAtIndex(index)
		end)
    end
    self.m_TipLabel:SetActive(true)
    self.m_SimpleTableView:Clear()
    self.m_SimpleTableView.dataSource = self.m_DefaultSimpleTableViewDataSource
	self.m_SimpleTableView:LoadData(0, false)
end

function LuaCHRoomListView:Refresh()
    self.m_TipLabel:SetActive(#self.m_AllData==0)
    self.m_SimpleTableView:Clear()
	self.m_SimpleTableView:LoadData(0, false)
    local pos = Extensions.GetTopPos(self.m_SimpleTableView.scrollView)
    pos.y = pos.y + 5
    self.m_SimpleTableView.table.transform.localPosition = pos
end

function LuaCHRoomListView:NumberOfRows()
	return #self.m_AllData
end

function LuaCHRoomListView:CellForRowAtIndex(index)
	if index < 0 and index >= #self.m_AllData then
        return nil
    end

    local data = self.m_AllData[index + 1]
    local cellIdentifier = "RoomTemplate"
    local template = self.m_RoomTemplate

    local cell = self.m_SimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.m_SimpleTableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end
    CommonDefs.AddOnClickListener(cell, DelegateFactory.Action_GameObject(function(go) self:OnRoomClick(data) end), false)
    self:InitRoom(cell, data)
    return cell
end

function LuaCHRoomListView:OnRoomClick(data)
    if LuaClubHouseMgr:IsInRoom() and LuaClubHouseMgr:GetRoomId() == data.RoomId then
        self:OnCurRoomRootClick()
    elseif data.RoomType == EnumCHRoomType.private then
        LuaClubHouseMgr:EnterRoom(data.RoomId, data.RoomName, nil)
    else
        LuaClubHouseMgr:EnterRoom(data.RoomId, data.RoomName, "")
    end
end

function LuaCHRoomListView:InitRoom(roomItem, roomInfo)
    roomItem:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitRoomInfo(roomInfo)
end

---------------------------------------------------------
-- 事件
---------------------------------------------------------

function LuaCHRoomListView:OnEnable()
    g_ScriptEvent:AddListener("ClubHouse_QueryRoomList_Result", self, "OnClubHouseQueryRoomListResult")
    --leave room or destroy room
    g_ScriptEvent:AddListener("ClubHouse_Myself_Leave_Room", self, "OnClubHouseMyselfLeaveRoom")
    g_ScriptEvent:AddListener("ClubHouse_Notify_DelRoom", self, "OnClubHouseNotifyDelRoom")
    --member status change
    g_ScriptEvent:AddListener("ClubHouse_Member_Update", self, "OnClubHouseMemberUpdate")
    g_ScriptEvent:AddListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")

    g_ScriptEvent:AddListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
    g_ScriptEvent:AddListener("ClubHouse_Notify_ForbidHandsUpChange", self, "OnClubHouseNotifyForbidHandsUpChange")

    g_ScriptEvent:AddListener("ClubHouse_Notify_ChangeOwner", self, "OnClubHouseChangeOwner")
    g_ScriptEvent:AddListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle") 
end

function LuaCHRoomListView:OnDisable()
    g_ScriptEvent:RemoveListener("ClubHouse_QueryRoomList_Result", self, "OnClubHouseQueryRoomListResult")
    g_ScriptEvent:RemoveListener("ClubHouse_Myself_Leave_Room", self, "OnClubHouseMyselfLeaveRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_DelRoom", self, "OnClubHouseNotifyDelRoom")

    g_ScriptEvent:RemoveListener("ClubHouse_Broadcaster_Leave_Room", self, "OnClubHouseBroadcasterLeaveRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Member_Update", self, "OnClubHouseMemberUpdate")
    g_ScriptEvent:RemoveListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")

    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ForbidHandsUpChange", self, "OnClubHouseNotifyForbidHandsUpChange")

    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ChangeOwner", self, "OnClubHouseChangeOwner")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle") 
end

function LuaCHRoomListView:OnClubHouseQueryRoomListResult(rooms, page, context)
    self.m_AllData = rooms
    self.m_IsInSearch = context == "roomsearch"
    if self.m_IsInSearch and #self.m_AllData > 0 then
        local type = self.m_AllData[1].RoomType
        LuaClubHouseMgr.m_RoomQueryType = type
        self.m_PublicRoomButton.transform:Find("SelectedTexture").gameObject:SetActive(type == EnumCHRoomType.apponly)
        self.m_PrivateRoomButton.transform:Find("SelectedTexture").gameObject:SetActive(type == EnumCHRoomType.private)
    end
    self:UpdatePageIndex(page)
    self:Refresh()
    self:InitBottomDisplay()
end

function LuaCHRoomListView:OnClubHouseMyselfLeaveRoom()
    self:InitBottomDisplay()
end

function LuaCHRoomListView:OnClubHouseNotifyDelRoom(RoomId)
    self:InitBottomDisplay()
end

function LuaCHRoomListView:OnClubHouseMemberUpdate(member)
    if member.IsMe then
        self:InitOperationButtons()
    end
end

function LuaCHRoomListView:OnClubHouseMemberCharacterChanged(member)
    if member.IsMe then
        self:InitOperationButtons()
    end
end

function LuaCHRoomListView:OnClubHouseNotifyClearStatusAll(status)
    self:InitOperationButtons()
end

function LuaCHRoomListView:OnClubHouseNotifyForbidHandsUpChange(bForbid)
    if LuaClubHouseMgr:IsAudience(LuaClubHouseMgr:GetMyselfInfo()) then
        self:InitOperationButtons() --观众更新举手状态
    end
end

function LuaCHRoomListView:OnClubHouseChangeOwner()
    self:InitOperationButtons()
end

function LuaCHRoomListView:QueryRoomList(reason, page)
    LuaClubHouseMgr:QueryRoomList(0, LuaClubHouseMgr.m_RoomQueryType, LuaClubHouseMgr.m_RoomSortType, page > 0 and page or 1, reason)
end

function LuaCHRoomListView:OnClubHouseNotifyUpdateTitle()
    local roomTitleInfo = GameplayItem_ClubHouseTitle.GetData(LuaClubHouseMgr:GetRoomTitle() or 0)
    self.m_IsSpecialRoom = roomTitleInfo ~= nil

    self.m_CurRoomSpecialBg:SetActive(self.m_IsSpecialRoom)
end

function LuaCHRoomListView:InitBottomDisplay()
    local roomTitleInfo = GameplayItem_ClubHouseTitle.GetData(LuaClubHouseMgr:GetRoomTitle() or 0)
    self.m_IsSpecialRoom = roomTitleInfo ~= nil

    if LuaClubHouseMgr:IsInRoom() then
        self.m_CurRoomRoot:SetActive(true)
        self.m_CurRoomSpecialBg:SetActive(self.m_IsSpecialRoom)
        self:InitOperationButtons()
    else
        self.m_CurRoomRoot:SetActive(false)
    end
    self.m_SearchRoomRoot:SetActive(self.m_IsInSearch)
    self.m_CreateRoomRoot:SetActive(not self.m_IsInSearch)
    self.m_BottomRootTable:Reposition()

    -- 设置房间名和房间类型
    self.m_RoomNamePanel.m_LuaSelf:Init(LuaClubHouseMgr:GetRoomId() ,LuaClubHouseMgr:GetRoomName() or "", self.m_IsSpecialRoom)
end

function LuaCHRoomListView:InitOperationButtons()
    local myself = LuaClubHouseMgr:GetMyselfInfo()
    self.m_DealWithApplyButton:SetActive(myself.Character == EnumCHCharacterType.Owner)
    self.m_LeaveMicButton:SetActive(myself.Character == EnumCHCharacterType.Speaker)
    self.m_MuteMicButton:SetActive(myself.Character == EnumCHCharacterType.Owner or myself.Character == EnumCHCharacterType.Speaker)
    self.m_ApplyMicButton:SetActive(myself.Character == EnumCHCharacterType.Listener)
    self.m_MuteMicButton.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = LuaClubHouseMgr:GetSpeakingSprite(myself)
    if self.m_ApplyMicButton.activeSelf then
        self.m_ApplyMicButton:GetComponent(typeof(CButton)).Enabled = not LuaClubHouseMgr:IsRoomForbiddenHandsUp()
        self.m_ApplyMicButton.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = LuaClubHouseMgr:GetInverseHandsUpSprite(myself)
    end
    self.m_ButtonTable:Reposition()

    -- 调整名字面板宽度
    local btnCount = 0
    if myself.Character == EnumCHCharacterType.Owner then btnCount = 3
    elseif myself.Character == EnumCHCharacterType.Speaker then btnCount = 3
    elseif myself.Character == EnumCHCharacterType.Listener then btnCount = 2
    end
    self.m_RoomNamePanel.m_LuaSelf:SetNamePanelWidth(self.m_RoomNamePanelMaxWidth - btnCount * 100)
end

function LuaCHRoomListView:OnSearchButtonClick()
    CUIManager.ShowUI(CLuaUIResources.CHRoomIdSearchWnd)
end

function LuaCHRoomListView:OnCreateRoomButtonClick()
    if LuaClubHouseMgr:CheckIfInCCMiniCapturingForCreateRoom() then
        return
    end
    LuaClubHouseMgr:CheckAndRequestRecordPermission(function()
        LuaClubHouseMgr:QueryMyRoom()
    end)
end

function LuaCHRoomListView:OnNextPageButtonClick()
    self:QueryRoomList("onextpagebuttonclick", self.m_CurPageIndex + 1)
end
function LuaCHRoomListView:OnPrevPageButtonClick()
    self:QueryRoomList("onprevpagebuttonclick", self.m_CurPageIndex - 1)
end
function LuaCHRoomListView:OnCurPageLabelClick()

    local anchorLabel = not LuaClubHouseMgr:IsInWinSocialWnd() and self.m_CurPageLabel or nil
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(1, 999, self.m_CurPageIndex, 3, DelegateFactory.Action_int(function (val)
        self:UpdatePageIndex(val)
    end), DelegateFactory.Action_int(function (val)
        self:UpdatePageIndex(val)
        self:QueryRoomList("numberkeyboardinput", val)
    end), anchorLabel, CTooltipAlignType.Top, true)
end

function LuaCHRoomListView:OnLeaveRoomButtonClick()
    LuaClubHouseMgr:LeaveRoom()
end

function LuaCHRoomListView:OnDealWithApplyButtonClick()
    local myself = LuaClubHouseMgr:GetMyselfInfo()
    if myself and myself.Character == EnumCHCharacterType.Owner then
        CUIManager.ShowUI("CHApplySpeakListWnd")
    end
end

function LuaCHRoomListView:OnLeaveMicButtonClick()
    LuaClubHouseMgr:LeaveMic()
end

function LuaCHRoomListView:OnMuteMicButtonClick()
    LuaClubHouseMgr:ChangeSpeakingStatus(not LuaClubHouseMgr:IsMyselfSpeaking())
end

function LuaCHRoomListView:OnApplyMicButtonClick()
    LuaClubHouseMgr:ChangeHandsUpStatus(not LuaClubHouseMgr:IsMyselfHandsUp())
end

function LuaCHRoomListView:OnCurRoomRootClick()
    g_ScriptEvent:BroadcastInLua("RoomListView_OnRoomButtonClick")
end

function LuaCHRoomListView:OnSortButtonClick(queryType, sortType)
    LuaClubHouseMgr.m_RoomQueryType = queryType or EnumCHRoomType.apponly
    self.m_PublicRoomButton.transform:Find("SelectedTexture").gameObject:SetActive(queryType == EnumCHRoomType.apponly)
    self.m_PrivateRoomButton.transform:Find("SelectedTexture").gameObject:SetActive(queryType == EnumCHRoomType.private)

    LuaClubHouseMgr.m_RoomSortType = sortType or EnumCHSortType.heat
    self.m_HeatSortButton.transform:Find("SelectedTexture").gameObject:SetActive(sortType == EnumCHSortType.heat)
    self.m_TimeSortButton.transform:Find("SelectedTexture").gameObject:SetActive(sortType == EnumCHSortType.time)
    self:QueryRoomList("onsortbuttonclick", 1)
end

function LuaCHRoomListView:OnRankButtonClick()
    CUIManager.ShowUI("CHMonthRankWnd")
end

function LuaCHRoomListView:OnSearchReturnButtonClick()
    self.m_IsInSearch = false
    self:InitBottomDisplay()
    self:QueryRoomList("onsearchreturnbuttonclick", 1)
end