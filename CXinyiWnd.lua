-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpace_AddEvent_Ret = import "L10.Game.CPersonalSpace_AddEvent_Ret"
local CPersonalSpace_JingliList_Ret = import "L10.Game.CPersonalSpace_JingliList_Ret"
local CXinyiJingliItem = import "L10.UI.CXinyiJingliItem"
local CXinyiWnd = import "L10.UI.CXinyiWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local HTTPHelper = import "L10.Game.HTTPHelper"
local L10 = import "L10"
local LocalString = import "LocalString"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Texture = import "UnityEngine.Texture"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
--lsy 3
CXinyiWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), true)

    UIEventListener.Get(this.starQuestionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.starQuestionBtn).onClick, MakeDelegateFromCSFunction(this.OnStarQuestionBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtnStar).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtnStar).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.openListBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.openListBtn).onClick, MakeDelegateFromCSFunction(this.OnListBtnClicked, VoidDelegate, this), true)

    EventManager.AddListenerInternal(EnumEventType.SendFlowerBack, MakeDelegateFromCSFunction(this.SendFlowerBack, MakeGenericClass(Action1, CPersonalSpace_AddEvent_Ret), this))
end
CXinyiWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), false)

    UIEventListener.Get(this.starQuestionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.starQuestionBtn).onClick, MakeDelegateFromCSFunction(this.OnStarQuestionBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.closeBtnStar).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtnStar).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.openListBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.openListBtn).onClick, MakeDelegateFromCSFunction(this.OnListBtnClicked, VoidDelegate, this), false)

    EventManager.RemoveListenerInternal(EnumEventType.SendFlowerBack, MakeDelegateFromCSFunction(this.SendFlowerBack, MakeGenericClass(Action1, CPersonalSpace_AddEvent_Ret), this))
end
CXinyiWnd.m_OnSuccessStar_CS2LuaHook = function (this, ret) 
    if ret == nil or ret.data == nil then
        this:Process_NoStarData()
        return
    end
    if PlayerSettings.XinyiJingli_LastMonthViewed ~= ret.data.month then
        PlayerSettings.XinyiJingli_LastMonthViewed = ret.data.month
        this:InitStar(ret)
    else
        this:InitList()
    end
end
CXinyiWnd.m_InitList_CS2LuaHook = function (this) 
    this.rootList:SetActive(true)
    this.rootStar:SetActive(false)
    this.listTitle.text = LocalString.GetString("心易经理投票")
    this.listContent.text = LocalString.GetString("点击照片查看心易经理名片，可以为你喜欢的心易经理送花投票哦！")
    this.m_Table.m_DataSource = this
    this:GetList(MakeDelegateFromCSFunction(this.OnSuccess, MakeGenericClass(Action1, CPersonalSpace_JingliList_Ret), this), MakeDelegateFromCSFunction(this.OnFail, Action0, this))
end
CXinyiWnd.m_OnSuccess_CS2LuaHook = function (this, list) 
    if list.data == nil or list.data.Length == 0 then
        this:Process_NoListData()
    end


    this.jingli_list = list
    this.m_Table:ReloadData(true, false)
end
CXinyiWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row > this.jingli_list.data.Length or row < 0 then
        return nil
    end
    local data = this.jingli_list.data[row]
    local item = TypeAs(view:GetFromPool(0), typeof(CXinyiJingliItem))
    if item ~= nil then
        item:Init(data)
        item.actionOnSendFlower = MakeDelegateFromCSFunction(this.OnSendFlower, MakeGenericClass(Action1, CXinyiJingliItem), this)
        return item
    end
    return nil
end
CXinyiWnd.m_InitStar_CS2LuaHook = function (this, ret) 
    this.rootList:SetActive(false)
    this.rootStar:SetActive(true)
    this.starTitle.text = LocalString.GetString("上月最受欢迎")
    this.starBtnText.text = LocalString.GetString("本月候选经理")

    local year = CommonDefs.StringSplit_ArrayChar(ret.data.month, "-")[1]
    local fotmat = LocalString.GetString("{0}月最受欢迎心易经理")
    local txt = System.String.Format(fotmat, year)
    this.starContent.text = txt
    this:DownLoadPic(ret.data.avatar, this.StarPhoto)
end
CXinyiWnd.m_DownLoadPic_CS2LuaHook = function (this, path, uiTexture) 
    L10.Engine.Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(path, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
        if not this then
            return
        end
        local _texture = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGBA32, false)
        CommonDefs.LoadImage(_texture, bytes)
        local texture = TypeAs(_texture, typeof(Texture))
        uiTexture.mainTexture = texture
    end)))
end
