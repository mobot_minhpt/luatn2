local QnButton = import "L10.UI.QnButton"
local UISlider = import "UISlider"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CButton = import "L10.UI.CButton"
local QnTableView = import "L10.UI.QnTableView"
local UIProgressBar = import "UIProgressBar"
local QnRadioBox = import "L10.UI.QnRadioBox"
local Buff_Buff = import "L10.Game.Buff_Buff"
local Animation = import "UnityEngine.Animation"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"

EnumPengDaoXiDianRequestState = {
    eNone = 0,
    eNormal = 1,
    eWaitResult = 2,
    eResult = 3,
    eWaitDelta = 4,
}
LuaPengDaoDevelopWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoDevelopWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaPengDaoDevelopWnd, "LeftSkillTemplate", "LeftSkillTemplate", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillRoot", "SkillRoot", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkilPointer", "SkilPointer", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillPointsNumLabel", "SkillPointsNumLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "GetSkillPointsButton", "GetSkillPointsButton", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "ResetSkillPointsButton", "ResetSkillPointsButton", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillView", "SkillView", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "CiFuView", "CiFuView", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "RightSkillIcon", "RightSkillIcon", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillNameLabel", "SkillNameLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillTypeLabel", "SkillTypeLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillLevelLabel", "SkillLevelLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "ShowSkillDetailBtn", "ShowSkillDetailBtn", QnSelectableButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillDesLabel", "SkillDesLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillCostLabel", "SkillCostLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillCostLabel2", "SkillCostLabel2", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "LevelUpSkillButton", "LevelUpSkillButton", CButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "EnableSkillButton", "EnableSkillButton", CButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "BottomCenterButton", "BottomCenterButton", CButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "SkillLockIcon", "SkillLockIcon", UITexture)
RegistChildComponent(LuaPengDaoDevelopWnd, "CurCiFuQnTableView", "CurCiFuQnTableView", QnTableView)
RegistChildComponent(LuaPengDaoDevelopWnd, "NewCiFuQnTableView", "NewCiFuQnTableView", QnTableView)
RegistChildComponent(LuaPengDaoDevelopWnd, "ReplaceCiFuButton", "ReplaceCiFuButton", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "XiDianCiFuProgressLabel", "XiDianCiFuProgressLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "XiDianCiFuLevelLabel", "XiDianCiFuLevelLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "XiDianCiFuBaoDiProgressBar", "XiDianCiFuBaoDiProgressBar", UIProgressBar)
RegistChildComponent(LuaPengDaoDevelopWnd, "RadioBox", "RadioBox", QnRadioBox)
RegistChildComponent(LuaPengDaoDevelopWnd, "XiDianCiFUPointsNumLabel", "XiDianCiFUPointsNumLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "GetXiDianPointsButton", "GetXiDianPointsButton", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "XiDianButton", "XiDianButton", CButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "NoneCiFuLabel", "NoneCiFuLabel", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "NoneSelectCiFuLabel", "NoneSelectCiFuLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "XiDianCostLabel", "XiDianCostLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "LuoPanWidget", "LuoPanWidget", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "TabBtn2Widget", "TabBtn2Widget", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "CiFuLeftWidget", "CiFuLeftWidget", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "CiFuRightTopWidget", "CiFuRightTopWidget", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "CiFuRightBottomWidget", "CiFuRightBottomWidget", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "CenterView", "CenterView", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "BottomView", "BottomView", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "AutoCenterView", "AutoCenterView", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "AutoBottomView", "AutoBottomView", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "AutoXiDianTip", "AutoXiDianTip", GameObject)
RegistChildComponent(LuaPengDaoDevelopWnd, "TimesDecreaseButton", "TimesDecreaseButton", QnButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "TimesIncreaseButton", "TimesIncreaseButton", QnButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "TimesInputLab", "TimesInputLab", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "AutoSpeedRadioBox", "AutoSpeedRadioBox", QnRadioBox)
RegistChildComponent(LuaPengDaoDevelopWnd, "AutoXiDianSlider", "AutoXiDianSlider", UISlider)
RegistChildComponent(LuaPengDaoDevelopWnd, "AutoProgressLabel", "AutoProgressLabel", UILabel)
RegistChildComponent(LuaPengDaoDevelopWnd, "StopXiDianButton", "StopXiDianButton", CButton)
RegistChildComponent(LuaPengDaoDevelopWnd, "TimesIncAndDec", "TimesIncAndDec", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoDevelopWnd, "m_SkillRadioBox")
RegistClassMember(LuaPengDaoDevelopWnd, "m_SelectedSkillIndex")
RegistClassMember(LuaPengDaoDevelopWnd, "m_SkillDataList")
RegistClassMember(LuaPengDaoDevelopWnd, "m_IsShowSkillDetail")
RegistClassMember(LuaPengDaoDevelopWnd, "m_SkillId2ItemMap")
RegistClassMember(LuaPengDaoDevelopWnd, "m_CiFuIndex")
RegistClassMember(LuaPengDaoDevelopWnd, "m_AllBuff")
RegistClassMember(LuaPengDaoDevelopWnd, "m_TempBuff")
RegistClassMember(LuaPengDaoDevelopWnd, "m_Ani")
RegistClassMember(LuaPengDaoDevelopWnd, "m_Ani2")
RegistClassMember(LuaPengDaoDevelopWnd, "m_Ani3")
RegistClassMember(LuaPengDaoDevelopWnd, "m_TempCiFuLevel")
RegistClassMember(LuaPengDaoDevelopWnd, "m_UpdateGuideObjectsTick")

RegistClassMember(LuaPengDaoDevelopWnd, "m_AutoSpeedIndex")
RegistClassMember(LuaPengDaoDevelopWnd, "m_InitRate")
RegistClassMember(LuaPengDaoDevelopWnd, "m_RequestRate")
RegistClassMember(LuaPengDaoDevelopWnd, "m_RequestCount")
RegistClassMember(LuaPengDaoDevelopWnd, "m_AutoTimes")
RegistClassMember(LuaPengDaoDevelopWnd, "m_StartWaitTime")
RegistClassMember(LuaPengDaoDevelopWnd, "m_RequestState")
RegistClassMember(LuaPengDaoDevelopWnd, "m_LastTabIndex")
function LuaPengDaoDevelopWnd:Awake()
	self:InitAutoXiDian()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabBarTabChange(index)
	end)

	UIEventListener.Get(self.GetSkillPointsButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetSkillPointsButtonClick()
	end)

	UIEventListener.Get(self.ResetSkillPointsButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResetSkillPointsButtonClick()
	end)

	self.ShowSkillDetailBtn.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnShowSkillDetailBtnSelected(selected)
	end)

	
	UIEventListener.Get(self.LevelUpSkillButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLevelUpSkillButtonClick()
	end)

	UIEventListener.Get(self.EnableSkillButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnableSkillButtonClick()
	end)

	UIEventListener.Get(self.BottomCenterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBottomCenterButtonClick()
	end)

	UIEventListener.Get(self.ReplaceCiFuButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReplaceCiFuButtonClick()
	end)

	UIEventListener.Get(self.XiDianButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnXiDianButtonClick()
	end)

	self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
	    self:OnRadioBoxSelected(btn, index)
	end)

	UIEventListener.Get(self.GetXiDianPointsButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetXiDianPointsButtonClick()
	end)

	-- 自动洗炼
	UIEventListener.Get(self.TimesDecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTimesDecreaseButtonClick()
	end)

	UIEventListener.Get(self.TimesIncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTimesIncreaseButtonClick()
	end)

	UIEventListener.Get(self.TimesInputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTimesInputLabClick()
	end)

	self.AutoSpeedRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
	    self:OnAutoSpeedRadioBoxSelected(btn, index)
	end)

	UIEventListener.Get(self.StopXiDianButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStopXiDianButtonClick()
	end)
    --@endregion EventBind end

	self.TimesIncAndDec:SetActive(true)
	self.XiDianCostLabel.gameObject:SetActive(false)
	self.XiDianButton.label.text = LocalString.GetString("开始洗炼")

	if CommonDefs.IS_VN_CLIENT then
		self.BottomCenterButton.transform:Find("Label"):GetComponent(typeof(UILabel)).spacingX = 0
		self.LevelUpSkillButton.transform:Find("Label"):GetComponent(typeof(UILabel)).spacingX = 0
	end
end

function LuaPengDaoDevelopWnd:Init()
	self.LeftSkillTemplate.gameObject:SetActive(false)
	self.m_IsShowSkillDetail = false
	self:InitSkillRoot()
	self:InitCiFuQnTableView()
	self:InitCiFuView(true,true)
	self.TabBar:ChangeTab(0)
	self.ShowSkillDetailBtn:SetSelected(self:IsShowDetailBtn(), false)
	self.m_Ani = self.transform:GetComponent(typeof(Animation))
	self.m_Ani2 = self.SkillView.transform:Find("RightView"):GetComponent(typeof(Animation))
	self.m_Ani3 = self.CiFuView.transform:Find("RightView/RightView"):GetComponent(typeof(Animation))
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd) then
		CGuideMgr.Inst:StartGuide(EnumGuideKey.PengDaoDevelopWnd, 1)
		self:CancelUpdateGuideObjectsTick()
		self.m_UpdateGuideObjectsTick = RegisterTick(function ()
			self:InitGuideObjects()
			if not CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd) then
				self:CancelUpdateGuideObjectsTick()
			end
		end,1000)
	end
	self:InitGuideObjects()
end

function LuaPengDaoDevelopWnd:CancelUpdateGuideObjectsTick()
	if self.m_UpdateGuideObjectsTick then
		UnRegisterTick(self.m_UpdateGuideObjectsTick)
		self.m_UpdateGuideObjectsTick = nil
	end
end

function LuaPengDaoDevelopWnd:InitGuideObjects()
	self.LuoPanWidget.gameObject:SetActive(CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd))
	self.TabBtn2Widget.gameObject:SetActive(CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd))
	self.CiFuLeftWidget.gameObject:SetActive(CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd))
	self.CiFuRightTopWidget.gameObject:SetActive(CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd))
	self.CiFuRightBottomWidget.gameObject:SetActive(CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd))
end

function LuaPengDaoDevelopWnd:IsShowDetailBtn()
	local isShow = PlayerPrefs.GetInt("PengDaoDevelopWnd_IsShowDetailBtn")
	return isShow == 1
end

function LuaPengDaoDevelopWnd:InitSkillRoot()
	self.SkilPointer.gameObject:SetActive(false)
	if not self.m_SkillRadioBox then
		self.m_SkillRadioBox = self.SkillRoot.gameObject:AddComponent(typeof(QnRadioBox))
		self.m_SkillRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), PengDaoFuYao_OutsiderSkill.GetDataCount())
		self.m_SkillId2ItemMap = {}
		local index = 0
		self.m_SkillDataList = {}
		Extensions.RemoveAllChildren(self.SkillRoot.transform)
			PengDaoFuYao_OutsiderSkill.ForeachKey(function (k)
			table.insert(self.m_SkillDataList, PengDaoFuYao_OutsiderSkill.GetData(k))
			local obj = NGUITools.AddChild(self.SkillRoot.gameObject, self.LeftSkillTemplate.gameObject)
			obj.gameObject:SetActive(true)
			self:InitSkillItem(obj,k)
			self.m_SkillId2ItemMap[k] = obj
			self.m_SkillRadioBox.m_RadioButtons[index] = obj:GetComponent(typeof(QnSelectableButton))
			self.m_SkillRadioBox.m_RadioButtons[index].OnClick = MakeDelegateFromCSFunction(self.m_SkillRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_SkillRadioBox)
			index = index + 1
		end)
		self.m_SkillRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
			self:OnSkillRadioBoxSelected(btn,index)
		end)
	else
		for id, obj in pairs(self.m_SkillId2ItemMap) do
			self:InitSkillItem(obj,id)
		end
	end
	local equipedSkill = self:GetEquipedSkill()
	local obj = self.m_SkillId2ItemMap[equipedSkill]
	if obj then
		self.SkilPointer.gameObject:SetActive(true)
		local data =  PengDaoFuYao_OutsiderSkill.GetData(equipedSkill)
		Extensions.SetLocalRotationZ(self.SkilPointer.transform, data.CentralAngle)
	end
	self.m_SkillRadioBox:ChangeTo(self.m_SelectedSkillIndex and self.m_SelectedSkillIndex or 0, true)
	self.SkillPointsNumLabel.text = self:GetSkillPoint()
end

function LuaPengDaoDevelopWnd:InitSkillItem(obj,id)
	local data =  PengDaoFuYao_OutsiderSkill.GetData(id)
	local skillid = data.SkillId
	local skillData = __Skill_AllSkills_Template.GetData(skillid)
	local level = self:GetSkillLevel(data.Id)
	local isFullLevel = level == 70
	local locked = level == 0
	local equipedSkill = self:GetEquipedSkill()
	local cost = self:GetSkillCost(data.Id,level)
	local pointsNum = self:GetSkillPoint()
	local isActiveSkill = not locked and data.Category == 1
	local groupPoint = self:GetSkillGroupPoint(data.SkillGroup)

	local lock = obj.transform:Find("Lock"):GetComponent(typeof(UITexture)) --解锁前
	local canUnlockAni = lock.transform:GetComponent(typeof(Animation))
	local tex = obj.transform:Find("Texture"):GetComponent(typeof(CUITexture))
	local zhu = obj.transform:Find("Zhu"):GetComponent(typeof(UITexture))
	local levelLabel = obj.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local highlight = obj.transform:Find("Highlight") --激活状态
	--local select = obj.transform:Find("Select") --选中
	local mark = obj.transform:Find("Mark") --未启用
	local kuang = obj.transform:Find("Kuang"):GetComponent(typeof(UITexture))
	local waifaguang = obj.transform:Find("Waifaguang"):GetComponent(typeof(UITexture))
	canUnlockAni:Stop()
	if not isActiveSkill then
		if not locked and data.Category == 0 then
			if not isFullLevel then
				if pointsNum >= cost then
					canUnlockAni:Play()
				end
			end
		elseif groupPoint >= data.UnlockNeedPoint then
			canUnlockAni:Play()
		end
	end
	local color = NGUIText.ParseColor24(data.SkillGroup == 1 and "dd8b6a" or (data.SkillGroup == 2 and "b3e6d8" or "b697e0" ),0) 
	lock.color = color
	zhu.color = color
	kuang.color = color
	waifaguang.color = color
	lock.gameObject:SetActive(locked)
	tex:LoadSkillIcon(skillData.SkillIcon)
	zhu.gameObject:SetActive(data.Category == 1)
	levelLabel.text = locked and "" or level
	highlight.gameObject:SetActive(not locked and data.Category == 1 and equipedSkill == skillid)
	mark.gameObject:SetActive((data.Category ~= 1 and locked) or (data.Category == 1 and equipedSkill ~= id))
	obj.transform.localPosition = Vector3(data.IconLocation[0], data.IconLocation[1], 0)

end

function LuaPengDaoDevelopWnd:GetSkillLevel(id)
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		local map = info.SkillLevel
		local default, level = CommonDefs.DictTryGet(map, typeof(Byte), id, typeof(Byte))
		return default and level or 0
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetEquipedSkill()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		return info.EquipedSkill
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetSkillPoint()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		return info.SkillPoint
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetSkillCost(id,level)
	local data = PengDaoFuYao_OutsiderSkill.GetData(id)
	local formulaId = data.NeedSkillPoint
	local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula or nil
	if formula then
		local v = formula(nil, nil, {level})
		return v
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetSkillGroupPoint(group)
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		local map = info.SkillGroupPoint
		local default, point = CommonDefs.DictTryGet(map, typeof(Byte), group, typeof(UInt32))
		return default and point or 0
	end
	return 0
end

function LuaPengDaoDevelopWnd:InitCiFuView(updateCurCiFuQnTableView, updateNewCiFuQnTableView)
	local level = self:GetCiFuBuffLevel()
	if self.m_TempCiFuLevel and level > self.m_TempCiFuLevel then
		if (self.m_AllBuff and #self.m_AllBuff or 0) > 0 then
			self.m_Ani:Play("pengdaodevelopwnd_shuaxinfankui")
		else
			local fx = self.CiFuView.transform:Find("RightView/fankui")
			fx.gameObject:SetActive(false)
			fx.gameObject:SetActive(true)
		end
	end
	self.XiDianCiFuLevelLabel.text = level
	self.XiDianCiFUPointsNumLabel.text = self:GetCiFuPointsNum()
	self.RadioBox:ChangeTo(self.m_CiFuIndex and self.m_CiFuIndex or 0, true)
	self:GetCiFuAllBuff()
	if updateCurCiFuQnTableView or (self.m_TempCiFuLevel and level > self.m_TempCiFuLevel ) then
		self.CurCiFuQnTableView:ReloadData(true, false)
		self.CurCiFuQnTableView:ReloadData(true, false)
	end
	if updateNewCiFuQnTableView or (self.m_TempCiFuLevel and level > self.m_TempCiFuLevel ) then
		self.NewCiFuQnTableView:ReloadData(true, false)
		self.NewCiFuQnTableView:ReloadData(true, false)
	end
	self.NoneCiFuLabel.gameObject:SetActive(#self.m_AllBuff == 0)
	self.NoneSelectCiFuLabel.gameObject:SetActive(#self.m_TempBuff == 0)
	local guaranteeNumber = self:GetGuaranteeNumber(level)
	local times = self:GetCiFuShuffleTimes()
	local showProgress = guaranteeNumber > 0
	self.XiDianCiFuProgressLabel.text = not showProgress and "" or SafeStringFormat3(LocalString.GetString("保底进度%d/%d"),times , guaranteeNumber)
	self.XiDianCiFuBaoDiProgressBar.value = showProgress and (times / guaranteeNumber) or 0
	self.XiDianCostLabel.text = SafeStringFormat3(LocalString.GetString("每次洗炼消耗%d点"), PengDaoFuYao_Setting.GetData().PengDaoFuYaoWordConsumption)
	self.m_TempCiFuLevel = level
end

function LuaPengDaoDevelopWnd:GetGuaranteeNumber(level)
	local lvData = PengDaoFuYao_OutsiderWordLv.GetData(level)
	if lvData then
		local res = lvData.GuaranteeNumber
		return res
	end
	return 0 
end

function LuaPengDaoDevelopWnd:InitCiFuQnTableView()
	self.CurCiFuQnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_AllBuff and #self.m_AllBuff or 0
        end,
        function(item, index)
			local buffData =  self.m_AllBuff and self.m_AllBuff[index + 1] or nil
            self:InitWordItem(item, index, buffData, self:GetCiFuCurrShuffleType())
        end)
	self.CurCiFuQnTableView.m_Table.direction = UITable.Direction.Down
	self.NewCiFuQnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_TempBuff and #self.m_TempBuff or 0
        end,
        function(item, index)
			local buffData =  self.m_TempBuff and self.m_TempBuff[index + 1] or nil
            self:InitWordItem(item, index, buffData, self:GetCiFuShuffleType())
        end)
	self.NewCiFuQnTableView.m_Table.direction = UITable.Direction.Down
end

function LuaPengDaoDevelopWnd:InitWordItem(item, index, buffData,type)
	item.gameObject:SetActive(buffData ~= nil)
	if buffData == nil then
		return
	end
	local desLabel = item.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local numTex = numLabel.transform:Find("Texture"):GetComponent(typeof(UITexture))
	numLabel.text = index + 1
	numTex.color = NGUIText.ParseColor24(type == 1 and "ffcd99" or (type == 2 and "99ffae" or "C284ED"),0) 
	desLabel.text = buffData.Display
	desLabel:ResizeCollider()
end

function LuaPengDaoDevelopWnd:GetCiFuShuffleType()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		return info.ShuffleType
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetCiFuCurrShuffleType()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		return info.CurrShuffleType
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetCiFuBuffLevel()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		return info.BuffLevel
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetCiFuPointsNum()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		return info.ShufflePoint
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetCiFuShuffleTimes()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		return info.ShuffleTimes
	end
	return 0
end

function LuaPengDaoDevelopWnd:GetCiFuAllBuff()
	self.m_AllBuff = {}
	self.m_TempBuff = {}
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		for i = 0, info.AllBuff.Length - 1 do
			local id = info.AllBuff[i]
			if id ~= 0 then
				local wordData = PengDaoFuYao_OutsiderWordList.GetData(id)
				local buffid = wordData.BuffId * 100 + self:GetCiFuBuffLevel()
				local buffData = Buff_Buff.GetData(buffid)
				if buffData then
					table.insert(self.m_AllBuff, buffData)
				end
			end
		end
		for i = 0, info.TempBuff.Length - 1 do
			local id = info.TempBuff[i]
			if id ~= 0 then
				local wordData = PengDaoFuYao_OutsiderWordList.GetData(id)
				local buffid = wordData.BuffId * 100 + self:GetCiFuBuffLevel()
				local buffData = Buff_Buff.GetData(buffid)
				if buffData then
					table.insert(self.m_TempBuff, buffData)
				end
			end
		end
	end
end

function LuaPengDaoDevelopWnd:OnEnable()
	g_ScriptEvent:AddListener("PDFY_RequestSkillLevelUpSuccess",self,"OnSkillLevelUpSuccess")
	g_ScriptEvent:AddListener("PDFY_RequestEquipSkillSuccess",self,"OnEquipSkillSuccess")
	g_ScriptEvent:AddListener("PDFY_RequestResetSkillResult",self,"OnResetSkillResult")
	g_ScriptEvent:AddListener("PDFY_RequestShuffleBuffResult",self,"OnShuffleBuffResult")
	g_ScriptEvent:AddListener("PDFY_RequestReplaceAllBuffResult",self,"OnReplaceAllBuffResult")
	g_ScriptEvent:AddListener("PDFY_SyncShufflePoint",self,"OnSyncShufflePoint")
	g_ScriptEvent:AddListener("PDFY_SyncSkillPoint",self,"OnSyncSkillPoint")
	g_ScriptEvent:AddListener("PDFY_ShuffleBuffFail",self,"OnShuffleBuffFail")
end

function LuaPengDaoDevelopWnd:OnDisable()
	g_ScriptEvent:RemoveListener("PDFY_RequestSkillLevelUpSuccess",self,"OnSkillLevelUpSuccess")
	g_ScriptEvent:RemoveListener("PDFY_RequestEquipSkillSuccess",self,"OnEquipSkillSuccess")
	g_ScriptEvent:RemoveListener("PDFY_RequestResetSkillResult",self,"OnResetSkillResult")
	g_ScriptEvent:RemoveListener("PDFY_RequestShuffleBuffResult",self,"OnShuffleBuffResult")
	g_ScriptEvent:RemoveListener("PDFY_RequestReplaceAllBuffResult",self,"OnReplaceAllBuffResult")
	g_ScriptEvent:RemoveListener("PDFY_SyncShufflePoint",self,"OnSyncShufflePoint")
	g_ScriptEvent:RemoveListener("PDFY_SyncSkillPoint",self,"OnSyncSkillPoint")
	g_ScriptEvent:RemoveListener("PDFY_ShuffleBuffFail",self,"OnShuffleBuffFail")
	self:CancelUpdateGuideObjectsTick()
end

function LuaPengDaoDevelopWnd:OnSkillLevelUpSuccess(id, skillLevel, skillPoint, group, hasGroupPoint)
	self:InitSkillRoot()
	if skillLevel == 1 then
		self.m_Ani2:Play()
	else
		local fx = self.SkillView.transform:Find("RightView/CUIFxjiesuo")
		fx.gameObject:SetActive(false)
		fx.gameObject:SetActive(true)
	end
end

function LuaPengDaoDevelopWnd:OnEquipSkillSuccess(equipedSkill)
	self:InitSkillRoot()
end

function LuaPengDaoDevelopWnd:OnResetSkillResult(score, skillPoint)
	self:InitSkillRoot()
end

function LuaPengDaoDevelopWnd:OnShuffleBuffResult(shufflePoint, shuffleTimes, shuffleType, buffLevel, tempBuff)
	if not CClientMainPlayer.Inst then return end

	if self.m_RequestState ~= EnumPengDaoXiDianRequestState.eNone then
		self.m_RequestCount = self.m_RequestCount + 1
		self.AutoXiDianSlider.value = self.m_RequestCount / self.m_AutoTimes
		self.AutoProgressLabel.text = SafeStringFormat3("%d/%d", self.m_RequestCount, self.m_AutoTimes)
		if self.m_AutoTimes <= self.m_RequestCount or self:GetCiFuPointsNum() <= 0 then
			self.m_RequestState = EnumPengDaoXiDianRequestState.eNone
			self:EndAutoRequest()
		end
		if self.m_RequestState == EnumPengDaoXiDianRequestState.eWaitResult then
			self.m_RequestState = EnumPengDaoXiDianRequestState.eResult
		end
	end

	self:InitCiFuView(false, true)
end

function LuaPengDaoDevelopWnd:OnReplaceAllBuffResult(newBuff,shuffleType)
	self:InitCiFuView(true, true)
end

function LuaPengDaoDevelopWnd:OnSyncShufflePoint(val)
	if self.m_RequestState == EnumPengDaoXiDianRequestState.eNone then
		self:SetAutoTimes(self.m_AutoTimes)
	end
	self:InitCiFuView(false, false)
end

function LuaPengDaoDevelopWnd:OnSyncSkillPoint(val)
	self:InitSkillRoot()
end

function LuaPengDaoDevelopWnd:OnShuffleBuffFail()
	if self.m_RequestState ~= EnumPengDaoXiDianRequestState.eNone then
		self.m_RequestState = EnumPengDaoXiDianRequestState.eNone
		self:EndAutoRequest()
	end
end
--@region UIEvent

function LuaPengDaoDevelopWnd:OnTabBarTabChange(index)
	self.SkillView.gameObject:SetActive(index == 0)
	self.CiFuView.gameObject:SetActive(index == 1)
	if index == 1 then
		self:InitCiFuView(true,true)
		if self.m_Ani then
			self.m_Ani:Play("pengdaodevelopwnd_qiehuan")
		end
	elseif index == 0 then
		self:InitSkillRoot()
		if self.m_Ani then
			self.m_Ani:Play("pengdaodevelopwnd_qiehuanhui")
		end
		if self.m_RequestState ~= EnumPengDaoXiDianRequestState.eNone then
			self.m_RequestState = EnumPengDaoXiDianRequestState.eNone
        	self:EndAutoRequest()
		end
	end
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd) then
		CGuideMgr.Inst:StartGuide(EnumGuideKey.PengDaoDevelopWnd, index == 0 and 1 or 3)
	end
	local tab1 = self.TabBar:GetTabGoByIndex(0)
	local tex1 = tab1:GetComponent(typeof(CUITexture))
	tex1:LoadMaterial(index == 1 and "UI/Texture/Transparent/Material/pengdaodevelopwnd_tab_jineng_n.mat" or "UI/Texture/Transparent/Material/pengdaodevelopwnd_tab_jineng_s.mat")
	local tab2 = self.TabBar:GetTabGoByIndex(1)
	local tex2 = tab2:GetComponent(typeof(CUITexture))
	tex2:LoadMaterial(index == 0 and "UI/Texture/Transparent/Material/pengdaodevelopwnd_tab_cifu_n.mat" or "UI/Texture/Transparent/Material/pengdaodevelopwnd_tab_cifu_s.mat")
end

function LuaPengDaoDevelopWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("PengDaoDevelopWnd_ReadMe")
end

function LuaPengDaoDevelopWnd:OnGetSkillPointsButtonClick()
	CLuaNPCShopInfoMgr.ShowScoreShopById(PengDaoFuYao_Setting.GetData().PengDaoFuYaoShopId)
end

function LuaPengDaoDevelopWnd:OnResetSkillPointsButtonClick()
	local cost = PengDaoFuYao_Setting.GetData().PengDaoFuYaoOutsiderSkillResetNeedScore
	local pointsNum = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.PengDaoFuYao]
	local color = cost <= pointsNum and "[00FF00]" or "[FF0000]"
	local msg = g_MessageMgr:FormatMessage("PengDaoDevelopWnd_ResetSkillPoints_Confirm",color ..cost .."[FFFFFF]", pointsNum) 
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.PDFY_RequestResetSkill()
	end),nil,LocalString.GetString("重置"),LocalString.GetString("取消"),false)
end

function LuaPengDaoDevelopWnd:OnShowSkillDetailBtnSelected(selected)
	self.m_IsShowSkillDetail = selected
	if not self.m_SelectedSkillIndex then
		return
	end
	local data = self.m_SkillDataList[self.m_SelectedSkillIndex + 1]
	local skillid = data.SkillId
	local level = self:GetSkillLevel(data.Id)
	self.SkillDesLabel.text = CUICommonDef.TranslateToNGUIText(LuaPengDaoMgr:GetSkillDes(skillid, level == 0 and 1 or level, self.m_IsShowSkillDetail))
	PlayerPrefs.SetInt("PengDaoDevelopWnd_IsShowDetailBtn",selected and 1 or 0)
end

function LuaPengDaoDevelopWnd:OnLevelUpSkillButtonClick()
	local data = self.m_SkillDataList[self.m_SelectedSkillIndex + 1]
	Gac2Gas.PDFY_RequestLevelUpSkill(data.Id)
end

function LuaPengDaoDevelopWnd:OnEnableSkillButtonClick()
	local data = self.m_SkillDataList[self.m_SelectedSkillIndex + 1]
	Gac2Gas.PDFY_RequestEquipSkill(data.Id)
end

function LuaPengDaoDevelopWnd:OnBottomCenterButtonClick()
	local data = self.m_SkillDataList[self.m_SelectedSkillIndex + 1]
	local level = self:GetSkillLevel(data.Id)
	local locked = level == 0 
	if not locked and data.Category == 1 then
		Gac2Gas.PDFY_RequestEquipSkill(data.Id)
	else
		Gac2Gas.PDFY_RequestLevelUpSkill(data.Id)
	end
end

function LuaPengDaoDevelopWnd:OnSkillRadioBoxSelected(btn, index)
	self.m_SelectedSkillIndex = index
	local data = self.m_SkillDataList[index + 1]
	local skillid = data.SkillId
	local skillData = __Skill_AllSkills_Template.GetData(skillid)
	local level = self:GetSkillLevel(data.Id)
	local isFullLevel = level == 70
	local groupPoint = self:GetSkillGroupPoint(data.SkillGroup)
	local locked = level == 0 
	local equipedSkillId = self:GetEquipedSkill()
	local isEquipedSkill = equipedSkillId == skillid
	local isActiveSkill = not locked and data.Category == 1
	local cost = self:GetSkillCost(data.Id,level)
	local pointsNum = self:GetSkillPoint()
	local color = pointsNum >= cost  and "[99ffae]" or "[ff5050]"
	local skilCostStr = SafeStringFormat3(LocalString.GetString("消耗技能点%s%d[ffffff]/%d"),color, pointsNum, cost)
	local color2 =  groupPoint >= data.UnlockNeedPoint and "[99ffae]" or "[ff5050]"
	local skilCostStr2 = SafeStringFormat3(LocalString.GetString("本系技能点数总投入达到%s%d[ffffff]/%d"),color2, groupPoint, data.UnlockNeedPoint)

	local lock = self.RightSkillIcon.transform:Find("Lock")
	local zhu = self.RightSkillIcon.transform:Find("Zhu")
	local tex = self.RightSkillIcon.transform:Find("Texture"):GetComponent(typeof(CUITexture))
	local skillNameLabelBg = self.SkillNameLabel.transform:Find("NameBg"):GetComponent(typeof(UITexture))
	lock.gameObject:SetActive(locked)
	self.ShowSkillDetailBtn.gameObject:SetActive(not locked)
	tex:LoadSkillIcon(skillData.SkillIcon)
	zhu.gameObject:SetActive(data.Category == 1)
	self.SkillNameLabel.text = data.Name
	skillNameLabelBg.color = NGUIText.ParseColor24(data.SkillGroup == 1 and "645251" or (data.SkillGroup == 2 and "55687c" or "595a7a"),0) 
	self.SkillTypeLabel.text =  (data.Category == 0 and LocalString.GetString("被动") or LocalString.GetString("主动"))
	self.SkillDesLabel.text = CUICommonDef.TranslateToNGUIText(LuaPengDaoMgr:GetSkillDes(skillid, level == 0 and 1 or level, self.m_IsShowSkillDetail))
	self.SkillDesLabel.alpha = locked and 0.3 or 1
	self.SkillLevelLabel.text = locked and LocalString.GetString("未解锁") or SafeStringFormat3(LocalString.GetString("%d级"), level)
	self.LevelUpSkillButton.gameObject:SetActive(isActiveSkill)
	self.EnableSkillButton.gameObject:SetActive(isActiveSkill)
	if isActiveSkill then
		self.LevelUpSkillButton.Enabled = pointsNum >= cost
		self.LevelUpSkillButton.Text = isFullLevel and LocalString.GetString("已满级") or LocalString.GetString("升级")
		self.EnableSkillButton.Enabled = not isEquipedSkill
		self.EnableSkillButton.Text = isEquipedSkill and LocalString.GetString("启用中") or LocalString.GetString("启用")
		if isFullLevel then
			self.LevelUpSkillButton.Enabled = false
			skilCostStr = ""
		end
	else
		if not locked and data.Category == 0 then
			if isFullLevel then
				self.BottomCenterButton.Enabled = false
				skilCostStr2 = ""
			else
				self.BottomCenterButton.Enabled = pointsNum >= cost
				skilCostStr2 = skilCostStr
			end
		else
			self.BottomCenterButton.Enabled = groupPoint >= data.UnlockNeedPoint
		end
		self.BottomCenterButton.Text = (not locked and data.Category == 0) and (isFullLevel and LocalString.GetString("已满级") or LocalString.GetString("升级")) or LocalString.GetString("解锁")
	end
	self.BottomCenterButton.gameObject:SetActive(not isActiveSkill)
	self.SkillCostLabel.gameObject:SetActive(isActiveSkill)
	self.SkillCostLabel.text = skilCostStr
	self.SkillCostLabel2.gameObject:SetActive(not isActiveSkill)
	self.SkillCostLabel2.text = skilCostStr2
	self.SkillLockIcon.gameObject:SetActive(locked)
	self.SkillLockIcon.alpha = locked and 0.8 or 1
	self.SkillLockIcon.color = NGUIText.ParseColor24(data.SkillGroup == 1 and "e38965" or (data.SkillGroup == 2 and "b6e7da" or "b698e1"),0) 
end

function LuaPengDaoDevelopWnd:OnReplaceCiFuButtonClick()
	Gac2Gas.PDFY_RequestReplaceAllBuff()
end

function LuaPengDaoDevelopWnd:OnRadioBoxSelected(btn, index)
	self.m_CiFuIndex = index
end

function LuaPengDaoDevelopWnd:OnGetXiDianPointsButtonClick()
	CLuaNPCShopInfoMgr.ShowScoreShopById(PengDaoFuYao_Setting.GetData().PengDaoFuYaoShopId)
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.PengDaoDevelopWnd) then
		CGuideMgr.Inst:EndCurrentPhase()
	end
	self:InitGuideObjects()
	self:CancelUpdateGuideObjectsTick()
end

function LuaPengDaoDevelopWnd:OnRadioBoxSelected(btn, index)
	self.m_CiFuIndex = index
end

-- 自动洗炼
function LuaPengDaoDevelopWnd:OnXiDianButtonClick()
	if self.m_AutoTimes <= 1 then
		Gac2Gas.PDFY_RequestShuffleBuff(self.m_CiFuIndex + 1)
	else
		self:BeginAutoRequest()
	end
end

function LuaPengDaoDevelopWnd:OnTimesDecreaseButtonClick()
    self:SetAutoTimes(self.m_AutoTimes - 1)
end

function LuaPengDaoDevelopWnd:OnTimesIncreaseButtonClick()
    self:SetAutoTimes(self.m_AutoTimes + 1)
end

function LuaPengDaoDevelopWnd:OnTimesInputLabClick()
	local autoMinTimes = 1
	local autoMaxTimes = self.GetCiFuPointsNum()
	CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(autoMinTimes, autoMaxTimes, self.m_AutoTimes, 100, DelegateFactory.Action_int(function (val) 
		self.TimesInputLab.text = SafeStringFormat3(LocalString.GetString("%d次"), val)
    end), DelegateFactory.Action_int(function (val) 
        self:SetAutoTimes(val)
    end), self.TimesInputLab, AlignType.Left, true)
end

function LuaPengDaoDevelopWnd:OnAutoSpeedRadioBoxSelected(btn, index)
	self.m_AutoSpeedIndex = index

	if self.m_AutoSpeedIndex == 0 then
		self.m_RequestRate = self.m_InitRate
	elseif self.m_AutoSpeedIndex == 1 then
		self.m_RequestRate = self.m_InitRate * 0.5
	elseif self.m_AutoSpeedIndex == 2 then
		self.m_RequestRate = self.m_InitRate * 0.25
	end

	-- 重置计时
	self.m_StartWaitTime = Time.time
end

function LuaPengDaoDevelopWnd:OnStopXiDianButtonClick()
	self.m_RequestState = EnumPengDaoXiDianRequestState.eNone
	self:EndAutoRequest()
end
--@endregion UIEvent

function LuaPengDaoDevelopWnd:SetAutoTimes(num)
	if num < 1 then
		num = 1
	end

	local autoMaxTimes = self.GetCiFuPointsNum()
    if num > autoMaxTimes then
		num = autoMaxTimes
	end

	self.m_AutoTimes = num

	self.TimesIncreaseButton.Enabled = autoMaxTimes - num > 0
	self.TimesDecreaseButton.Enabled = num > 1

	self.TimesInputLab.text = SafeStringFormat3(LocalString.GetString("%d次"), num)
end

function LuaPengDaoDevelopWnd:GetGuideGo( methodName )
	if methodName == "GetXiDianPointsButton" then
		return self.GetXiDianPointsButton.gameObject
	elseif methodName == "GetLuoPanWidget" and self.SkillView.gameObject.activeSelf then
		return self.LuoPanWidget.gameObject
	elseif methodName == "GetTabBtn2Widget" and self.SkillView.gameObject.activeSelf then
		return self.TabBtn2Widget.gameObject
	elseif methodName == "GetCiFuLeftWidget" and self.CiFuView.gameObject.activeSelf then
		return self.CiFuLeftWidget.gameObject
	elseif methodName == "GetCiFuRightTopWidget" and self.CiFuView.gameObject.activeSelf then
		return self.CiFuRightTopWidget.gameObject
	elseif methodName == "GetCiFuRightBottomWidget" and self.CiFuView.gameObject.activeSelf then
		return self.CiFuRightBottomWidget.gameObject
	end
	return nil
end

-- 自动洗炼
function LuaPengDaoDevelopWnd:Update()
	if CClientMainPlayer.Inst == nil then
        self.m_RequestState = EnumPengDaoXiDianRequestState.eNone
        self:EndAutoRequest()
        return
    end

	if self.m_RequestState == EnumPengDaoXiDianRequestState.eNormal then
		if self.m_AutoTimes > self.m_RequestCount and self:GetCiFuPointsNum() > 0 then
			Gac2Gas.PDFY_RequestShuffleBuff(self.m_CiFuIndex + 1)
			self.m_RequestState = EnumPengDaoXiDianRequestState.eResult
		else
			self.m_RequestState = EnumPengDaoXiDianRequestState.eNone
			self:EndAutoRequest()
			return
		end
	elseif self.m_RequestState == EnumPengDaoXiDianRequestState.eResult then
		self.m_RequestState = EnumPengDaoXiDianRequestState.eWaitDelta
        self.m_StartWaitTime = Time.time
	elseif self.m_RequestState == EnumPengDaoXiDianRequestState.eWaitDelta then
		if Time.time > self.m_StartWaitTime + self.m_RequestRate then
            self.m_RequestState = EnumPengDaoXiDianRequestState.eNormal
        end
    end
end

function LuaPengDaoDevelopWnd:InitAutoXiDian()
	self.m_RequestState = EnumPengDaoXiDianRequestState.eNone
	self.m_AutoTimes = 1
	self.m_AutoSpeedIndex = 0
	self.m_InitRate = 2
	self.m_RequestRate = 2

	self:SetAutoTimes(self.m_AutoTimes)
end

function LuaPengDaoDevelopWnd:BeginAutoRequest()
    g_MessageMgr:ShowMessage("Start_Refinery")
    self.m_RequestCount = 0
	self.AutoXiDianSlider.value = self.m_RequestCount / self.m_AutoTimes
	self.AutoProgressLabel.text = SafeStringFormat3("%d/%d", self.m_RequestCount, self.m_AutoTimes)
	self:SwitchAuto(true)
	self.m_RequestState = EnumPengDaoXiDianRequestState.eNormal
end
function LuaPengDaoDevelopWnd:EndAutoRequest()
	g_MessageMgr:ShowMessage("Stop_Refinery")

	self:SwitchAuto(false)
	self.AutoSpeedRadioBox:ChangeTo(0)
	-- 刷新洗炼次数
	self:SetAutoTimes(self.m_AutoTimes)
end

function LuaPengDaoDevelopWnd:SwitchAuto(isAuto)
	self.ReplaceCiFuButton.gameObject:SetActive(not isAuto)
	self.CenterView:SetActive(not isAuto)
	self.BottomView:SetActive(not isAuto)
	self.AutoCenterView:SetActive(isAuto)
	self.AutoBottomView:SetActive(isAuto)
	self.AutoXiDianTip:SetActive(isAuto)
	self.m_Ani3:Play(isAuto and "pengdaodevelopwnd_xilianqie1" or "pengdaodevelopwnd_xilianqie2")
end