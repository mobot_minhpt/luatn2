local CIMMgr = import "L10.Game.CIMMgr"
local Profession=import "L10.Game.Profession"

CLuaRemoteDouDiZhuInviteWnd = class()



RegistClassMember(CLuaRemoteDouDiZhuInviteWnd,"m_TableView")
RegistClassMember(CLuaRemoteDouDiZhuInviteWnd,"m_Friends")

CLuaRemoteDouDiZhuInviteWnd.s_Index=0

function CLuaRemoteDouDiZhuInviteWnd:Init()
    Gac2Gas.QueryInvitedRemoteDouDiZhuPlayerList()

    self.m_TableView = self.transform:Find("Anchor"):GetComponent(typeof(QnTableView))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
            return #self.m_Friends
        end,
        function(item,row)
            self:InitItem(item,row)
        end)


    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    self.m_Friends = {}
    local friends = CIMMgr.Inst.Friends
    CommonDefs.EnumerableIterate(friends, DelegateFactory.Action_object(function (___value)
        local playerId = ___value
        local online = CIMMgr.Inst:IsOnline(playerId)
        if online then
            local info = CIMMgr.Inst:GetBasicInfo(playerId)
            table.insert( self.m_Friends,{
                playerId = info.ID,
                -- info = info,
                friendliness = CIMMgr.Inst:GetFriendliness(playerId),
                class = info.Class,
                gender = info.Gender,
                expression = info.Expression,
                name = info.Name,
                level = info.Level,
                invited = false,
                }
            )
        end
    end))
    table.sort( self.m_Friends,function(a,b)
        local fa = a.friendliness--CIMMgr.Inst:GetFriendliness(a.ID)
        local fb = b.friendliness--CIMMgr.Inst:GetFriendliness(b.ID)
        if fa>fb then
            return true
        elseif fa<fb then
            return false
        else
            return a.playerId<b.playerId
        end
    end )

    self.m_TableView:ReloadData(false, false)
end

function CLuaRemoteDouDiZhuInviteWnd:InitItem(item,row)
    local tf = item.transform
    local friendInfo = self.m_Friends[row+1]
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))

    local portraitName = CUICommonDef.GetPortraitName(friendInfo.class, friendInfo.gender, friendInfo.expression)
    icon:LoadNPCPortrait(portraitName,false)

    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))
    clsSprite.spriteName = Profession.GetIconByNumber(friendInfo.class)
    
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = friendInfo.name

    local levelLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
    levelLabel.text = SafeStringFormat3("lv.%d",friendInfo.level)


    local inviteButton = item.transform:Find("InviteButton").gameObject
    UIEventListener.Get(inviteButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local id = friendInfo.playerId
        -- print(CLuaRemoteDouDiZhuInviteWnd.s_Index)
        Gac2Gas.InvitePlayerRemoteDouDiZhu(id,CLuaRemoteDouDiZhuInviteWnd.s_Index)
    end)

    CUICommonDef.SetActive(inviteButton,not friendInfo.invited,true)
end

function CLuaRemoteDouDiZhuInviteWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryInvitedRemoteDouDiZhuPlayerListResult", self, "OnQueryInvitedRemoteDouDiZhuPlayerListResult")
    g_ScriptEvent:AddListener("InviteRemoteDouDiZhuSuccess", self, "OnInviteRemoteDouDiZhuSuccess")
    
end

function CLuaRemoteDouDiZhuInviteWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryInvitedRemoteDouDiZhuPlayerListResult", self, "OnQueryInvitedRemoteDouDiZhuPlayerListResult")
    g_ScriptEvent:RemoveListener("InviteRemoteDouDiZhuSuccess", self, "OnInviteRemoteDouDiZhuSuccess")
end

function CLuaRemoteDouDiZhuInviteWnd:OnInviteRemoteDouDiZhuSuccess(invitedId)
    for i,v in ipairs(self.m_Friends) do
        if invitedId == v.playerId then
            v.invited = true
        end
    end
    for i=1,#self.m_Friends do
        local item = self.m_TableView:GetItemAtRow(i-1)
        if item then
            if self.m_Friends[i].playerId == invitedId then
                local inviteButton = item.transform:Find("InviteButton").gameObject
                CUICommonDef.SetActive(inviteButton,false,true)
                break
            end
        end
    end
end

function CLuaRemoteDouDiZhuInviteWnd:OnQueryInvitedRemoteDouDiZhuPlayerListResult(t)
    for i,v in ipairs(self.m_Friends) do
        if t[v.playerId] then
            v.invited = true
        end
    end
    -- self.m_TableView:ReloadData(false, false)
    for i=1,#self.m_Friends do
        local item = self.m_TableView:GetItemAtRow(i-1)
        if item then
            if t[self.m_Friends[i].playerId] then
                local inviteButton = item.transform:Find("InviteButton").gameObject
                CUICommonDef.SetActive(inviteButton,false,true)
            end
        end
    end
end