require("3rdParty/ScriptEvent")
require("common/common_include")

local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIRes = import "L10.UI.CUIResources"
local Color = import "UnityEngine.Color"
local Vector2 = import "UnityEngine.Vector2"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local UIPanel = import "UIPanel"
local Extensions = import "Extensions"
local CUIManager = import "L10.UI.CUIManager"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local Profession = import "L10.Game.Profession"
local Constants = import "L10.Game.Constants"
local NGUIText = import "NGUIText"
local LocalString = import "LocalString"

LuaWeekendFightInfoWnd=class()
RegistClassMember(LuaWeekendFightInfoWnd,"CloseBtn")
RegistClassMember(LuaWeekendFightInfoWnd,"TemplateNode")
RegistClassMember(LuaWeekendFightInfoWnd,"ScrollViewNode")
RegistClassMember(LuaWeekendFightInfoWnd,"TableNode")
RegistClassMember(LuaWeekendFightInfoWnd,"PlayerTemplateNode")

local NowOpenState = false
local NowOpenIndex = nil

function LuaWeekendFightInfoWnd:InitNewTable(index, dataTable)
	Extensions.RemoveAllChildren(self.TableNode.transform)
	local forceList = CWeekendFightMgr.Inst.ForceList
	local selfForceInfo = CWeekendFightMgr.Inst:GetSelfForceInfo()
	for i = 0, forceList.Count - 1, 1 do
		local data = forceList[i]
		local node = NGUITools.AddChild(self.TableNode,self.TemplateNode)
		node:SetActive(true)
		CWeekendFightMgr.Inst:SetRankLabel(node.transform:Find("rank").gameObject,data.rank)
		CWeekendFightMgr.Inst:SetForceIcon(node.transform:Find("force").gameObject,data.force)
		node.transform:Find("1"):GetComponent(typeof(UILabel)).text = data.num1
		node.transform:Find("2"):GetComponent(typeof(UILabel)).text = data.num2
		node.transform:Find("3"):GetComponent(typeof(UILabel)).text = data.num3
		node.transform:Find("monster"):GetComponent(typeof(UILabel)).text = data.killnum
		node.transform:Find("score"):GetComponent(typeof(UILabel)).text = data.score
		if selfForceInfo and selfForceInfo.force == data.force then
			node.transform:Find("rank"):GetComponent(typeof(UILabel)).color = Color.green
			node.transform:Find("1"):GetComponent(typeof(UILabel)).color = Color.green
			node.transform:Find("2"):GetComponent(typeof(UILabel)).color = Color.green
			node.transform:Find("3"):GetComponent(typeof(UILabel)).color = Color.green
			node.transform:Find("monster"):GetComponent(typeof(UILabel)).color = Color.green
			node.transform:Find("score"):GetComponent(typeof(UILabel)).color = Color.green
		end
		local checkSign = false
		if not NowOpenState then
			checkSign = true
		else
			if NowOpenIndex then
				if NowOpenIndex ~= i then
					checkSign = true
				else
					NowOpenState = false
					NowOpenIndex = nil
				end
			end
		end

		if checkSign then
			if index and index == i and dataTable then
				for i,v in ipairs(dataTable) do
					local playerNode = NGUITools.AddChild(self.TableNode,self.PlayerTemplateNode)
					playerNode:SetActive(true)
					playerNode.transform:Find("job1"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(v.clazz)
					playerNode.transform:Find("name"):GetComponent(typeof(UILabel)).text = v.name

					local levelLabel = playerNode.transform:Find("lv"):GetComponent(typeof(UILabel))
	        local default
	        if v.xianshen then
	            default = Constants.ColorOfFeiSheng
	        else
	            default = NGUIText.EncodeColor24(levelLabel.color)
	        end
	        levelLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]%s[-][/c]"), default, v.level)
					playerNode.transform:Find("zhanli"):GetComponent(typeof(UILabel)).text = v.zhanli
				end
				NowOpenState = true
				NowOpenIndex = i
			end
		end

		local playerInfoTable = {}

		CommonDefs.DictIterate(CWeekendFightMgr.Inst.PlayerInfoDic, DelegateFactory.Action_object_object(function (___key, ___value)
				if ___value.force == data.force then
					table.insert(playerInfoTable,___value)
				end
		end))

		if #playerInfoTable == 1 then
			node.transform:Find("job1"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(playerInfoTable[1].clazz)
		elseif #playerInfoTable == 2 then
			node.transform:Find("job1"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(playerInfoTable[1].clazz)
			node.transform:Find("job2"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(playerInfoTable[2].clazz)
		end

		local onItemClick = function(go)
			self:InitNewTable(i,playerInfoTable)
		end
		CommonDefs.AddOnClickListener(node.transform:Find("clicksign").gameObject,DelegateFactory.Action_GameObject(onItemClick),false)

		if i % 2 == 0 then
			node.transform:Find("Bg"):GetComponent(typeof(UISprite)).spriteName = "common_textbg_02_dark"
		else
			node.transform:Find("Bg"):GetComponent(typeof(UISprite)).spriteName = "common_textbg_02_light"
		end
	end

	if not index and not dataTable then
		self.TableNode:GetComponent(typeof(UITable)):Reposition()
		self.ScrollViewNode:GetComponent(typeof(UIScrollView)):ResetPosition()
	else
		local panel = self.ScrollViewNode:GetComponent(typeof(UIPanel))
		local savePos = self.ScrollViewNode.transform.localPosition
		local saveOffsetY = panel.clipOffset.y
		self.TableNode:GetComponent(typeof(UITable)):Reposition()
		self.ScrollViewNode:GetComponent(typeof(UIScrollView)):ResetPosition()
		self.ScrollViewNode.transform.localPosition = savePos
		panel.clipOffset = Vector2(panel.clipOffset.x,saveOffsetY)
	end
end

function LuaWeekendFightInfoWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.WeekendFightInfoWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	NowOpenState = false
	NowOpenIndex = nil
	self.TemplateNode:SetActive(false)
	self.PlayerTemplateNode:SetActive(false)
	self:InitNewTable()
end

return LuaWeekendFightInfoWnd
