-- Auto Generated!!
local CTickMgr = import "L10.Engine.CTickMgr"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local CYuanDanYaoQianWnd = import "L10.UI.CYuanDanYaoQianWnd"
local DelegateFactory = import "DelegateFactory"
local ETickType = import "L10.Engine.ETickType"
local SoundManager = import "SoundManager"
local Vector3 = import "UnityEngine.Vector3"
CYuanDanYaoQianWnd.m_GetQianYu_CS2LuaHook = function (this, data) 
    local val = UnityEngine_Random(0, 4)
    if val == 0 then
        return data.Word
    elseif val == 1 then
        return data.Word2
    elseif val == 2 then
        return data.Word3
    else
        return data.Word4
    end
end
CYuanDanYaoQianWnd.m_SamleAni_CS2LuaHook = function (this, anim, clipName, time) 

    anim:get_Item(clipName).time = time
    anim:get_Item(clipName).enabled = true
    anim:get_Item(clipName).weight = 1
    anim:Sample()
    anim:get_Item(clipName).enabled = false
end
CYuanDanYaoQianWnd.m_OnPlayerShakeDevice_CS2LuaHook = function (this) 

    if this.isShaked then
        return
    end
    this.isShaked = true

    this:CancelTick()
    this.m_Tick = CTickMgr.Register(DelegateFactory.Action(function () 
        CYuanDanMgr.Inst:RequestCompleteYaoQian()
    end), this.TimeDelayToSendRequest, ETickType.Once)
    this:StartShake()
end
CYuanDanYaoQianWnd.m_StartShake_CS2LuaHook = function (this) 
    this.qianyuAni:get_Item(this.QianYuClipName).time = 0
    this.qiantongAni:get_Item(this.QianTongClipName).time = 0
    this.qianAni:get_Item(this.QianClipName).time = 0
    this.qiantongAni:Play()
    this.qianyuAni:Play()
    this.qianAni:Play()
    if not CYuanDanYaoQianWnd.s_EnableAnimCallback then
        SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.YuanDanYaoQianound, Vector3.zero, nil, 0)
    end
end
CYuanDanYaoQianWnd.m_CancelTick_CS2LuaHook = function (this) 

    if this.m_Tick ~= nil then
        invoke(this.m_Tick)
        this.m_Tick = nil
    end
end
