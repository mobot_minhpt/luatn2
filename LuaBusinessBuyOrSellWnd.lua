local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local UILongPressButton = import "L10.UI.UILongPressButton"
local Color = import "UnityEngine.Color"
local NGUIText = import "NGUIText"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaBusinessBuyOrSellWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessBuyOrSellWnd, "GoodsView", "GoodsView", QnTableView)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "GoodIcon", "GoodIcon", CUITexture)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "GoodName", "GoodName", UILabel)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "TotallPriceLab", "TotallPriceLab", UILabel)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "OwnAmountLab", "OwnAmountLab", UILabel)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "PriceLab", "PriceLab", UILabel)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "OwnMoneyLab", "OwnMoneyLab", UILabel)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaBusinessBuyOrSellWnd, "YellowButton", "YellowButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_TitleLab")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_DecreaseButton")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_IncreaseButton")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_InputLab")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_InputNum")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_InputNumMax")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_MaxReason")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_CurGoodsPrice")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_CurGoodsType")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_CurPriceLab")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_Good")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_Inventory")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_Buy")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_Own")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_Sprite")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_CurBuyGoodsRow")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_CurSellGoodsRow")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_GrayColor")
RegistClassMember(LuaBusinessBuyOrSellWnd, "m_CloseBtn")

function LuaBusinessBuyOrSellWnd:Awake()
    self.m_TitleLab         = self.transform:Find("Anchor/Right/Buy/BuyAmount/TitleLab"):GetComponent(typeof(UILabel))
    self.m_DecreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_IncreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InputLab         = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))
    self.m_CurPriceLab      = self.transform:Find("Anchor/Right/Good/CurPrice/CurPriceLab"):GetComponent(typeof(UILabel))
    self.m_Good             = self.transform:Find("Anchor/Right/Good").gameObject
    self.m_Inventory        = self.transform:Find("Anchor/Right/Inventory").gameObject
    self.m_Buy              = self.transform:Find("Anchor/Right/Buy").gameObject
    self.m_Own              = self.transform:Find("Anchor/Right/Own").gameObject
    self.m_Sprite           = self.transform:Find("Anchor/Right/Sprite").gameObject
    self.m_GrayColor        = NGUIText.ParseColor24("b8b7b7", 0)
    self.m_CloseBtn         = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton"):GetComponent(typeof(CButton))

    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDecreaseButtonClick()
	end)

	UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)

	UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInputLabClick()
	end)

    UIEventListener.Get(self.YellowButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOKBtnClick(go)
    end)

    self.GoodsView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return LuaBusinessMgr.m_BusinessIsBuy == 1 and #LuaBusinessMgr.m_CurCityGoods or #LuaBusinessMgr.m_Inventory
        end,
        function(item, index)
            self:InitItem(item, LuaBusinessMgr.m_BusinessIsBuy == 1 and LuaBusinessMgr.m_CurCityGoods[index + 1] or LuaBusinessMgr.m_Inventory[index + 1])
        end
    )

    self.GoodsView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        if LuaBusinessMgr.m_BusinessIsBuy == 1 then
            self.m_CurBuyGoodsRow = row
        else
            self.m_CurSellGoodsRow = row
        end
        self:RefreshState()
    end)

    self.m_CurBuyGoodsRow = LuaBusinessMgr.m_CurBuyGoodsRow
    self.m_CurSellGoodsRow = LuaBusinessMgr.m_CurSellGoodsRow
end

function LuaBusinessBuyOrSellWnd:Start()
    self.GoodsView:ReloadData(true, false)

    local isBuying = LuaBusinessMgr.m_BusinessIsBuy == 1
    self.m_TitleLab.text = isBuying and LocalString.GetString("买入") or LocalString.GetString("卖出")
    self.TitleLabel.text = isBuying and  LocalString.GetString("买进") or LocalString.GetString("卖出")
    self.YellowButton.Text = isBuying and LocalString.GetString("买  入") or LocalString.GetString("卖  出")
    self.m_Own:SetActive(isBuying)
    LuaUtils.SetLocalPosition(self.m_Good.transform, -10.5, isBuying and 256 or 224, 0)
    LuaUtils.SetLocalPosition(self.m_Inventory.transform, -5.1, isBuying and 30 or 0, 0)
    LuaUtils.SetLocalPosition(self.m_Buy.transform, -3.3, isBuying and -193 or -236, 0)
    LuaUtils.SetLocalPosition(self.m_Sprite.transform, -19, isBuying and 135 or 102, 0)

    if self:IsNoGoods() then
        self:NoGoods()
    else
        self:RefreshSelect()
    end
end

function LuaBusinessBuyOrSellWnd:RefreshState()
    local ownGoodsInfo = nil
    local price = 0
    if LuaBusinessMgr.m_BusinessIsBuy == 1 then
        local info = LuaBusinessMgr.m_CurCityGoods[self.m_CurBuyGoodsRow + 1]
        if info == nil then
            self:NoGoods()
            return
        end
        local resetVolume = LuaBusinessMgr.m_InventorySize - LuaBusinessMgr:GetCurInventoryAmount()
        local moneyCouldBuy = math.floor(LuaBusinessMgr.m_OwnMoney / info.price)
        self.m_InputNum = 1
        self.m_InputNumMax = math.min(resetVolume, moneyCouldBuy)
        -- 1 容量不足 2 通宝不足
        self.m_MaxReason = self.m_InputNumMax == resetVolume and 1 or 2
        self.m_InputNumMax = math.max(0, self.m_InputNumMax)
        price = LuaBusinessMgr.m_CurCityGoodsType2Price[info.type]
        for i = 1, #LuaBusinessMgr.m_Inventory do 
            local t = LuaBusinessMgr.m_Inventory[i]
            if t.type == info.type then
                ownGoodsInfo = t
                break
            end
        end

        local data = Business_Goods.GetData(info.type)
        self.GoodIcon:LoadMaterial(data.Icon)
        self.GoodName.text = data.Name
        self.m_CurGoodsType = info.type

        local color
        if not info.onSale then
            color = self.m_GrayColor
        elseif LuaBusinessMgr:GetCityOfSpecialGoods(info.type) then
            color = Color.yellow
        else
            color = Color.white
        end
        self.GoodName.color = color

        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.BusinessGuide) then
            self.m_InputNum = Business_Setting.GetData().GuideAutoBuyGoodsNum
        end
    else
        ownGoodsInfo = LuaBusinessMgr.m_Inventory[self.m_CurSellGoodsRow + 1]
        if ownGoodsInfo == nil then
            self:NoGoods()
            return
        end
        price = LuaBusinessMgr.m_CurCityGoodsType2Price[ownGoodsInfo.type]
        self.m_InputNum = ownGoodsInfo.num
        self.m_InputNumMax = ownGoodsInfo.num

        local data = Business_Goods.GetData(ownGoodsInfo.type)
        self.GoodIcon:LoadMaterial(data.Icon)
        self.GoodName.text = data.Name
        self.m_CurGoodsType = ownGoodsInfo.type

        local color
        if LuaBusinessMgr:GetCityOfSpecialGoods(ownGoodsInfo.type) then
            color = Color.yellow
        else
            color = Color.white
        end
        self.GoodName.color = color
    end

    self.OwnAmountLab.text = ownGoodsInfo and ownGoodsInfo.num or 0
    self.PriceLab.text = ownGoodsInfo and ownGoodsInfo.price or LocalString.GetString("暂无")
    self.OwnMoneyLab.text = SafeStringFormat3("%.f", LuaBusinessMgr.m_OwnMoney)
    self.m_CurGoodsPrice = price
    self:SetInputNum(self.m_InputNum, true)
    self.TotallPriceLab.text = self.m_InputNum * price
    self.m_CurPriceLab.text = price
end

function LuaBusinessBuyOrSellWnd:IsNoGoods()
    local info
    if LuaBusinessMgr.m_BusinessIsBuy == 1 then
        info = LuaBusinessMgr.m_CurCityGoods[self.m_CurBuyGoodsRow + 1]
    else
        info = LuaBusinessMgr.m_Inventory[self.m_CurSellGoodsRow + 1]
    end

    return info == nil
end

function LuaBusinessBuyOrSellWnd:RefreshSelect()
    if LuaBusinessMgr.m_BusinessIsBuy == 1 then
        self.GoodsView:SetSelectRow(self.m_CurBuyGoodsRow, true)
    else
        self.GoodsView:SetSelectRow(self.m_CurSellGoodsRow, true)
    end
end

--@region UIEvent

--@endregion UIEvent
function LuaBusinessBuyOrSellWnd:OnEnable()
    g_ScriptEvent:AddListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessBuyOrSellWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnBusinessDataUpdate", self, "OnBusinessDataUpdate")
end

function LuaBusinessBuyOrSellWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - 1)
end

function LuaBusinessBuyOrSellWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + 1)
end

function LuaBusinessBuyOrSellWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val) 
		self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val) 
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaBusinessBuyOrSellWnd:SetInputNum(num, noMsg)
	if num < 0 then
		num = 0
	end

	if num > self.m_InputNumMax then
		num = self.m_InputNumMax

        if not noMsg and LuaBusinessMgr.m_BusinessIsBuy == 1 then
             -- 1 容量不足 2 通宝不足
            if self.m_MaxReason == 1 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("马车仓库容量不足"))
            elseif self.m_MaxReason == 2 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("当前通宝不足，请取出投资或卖掉其他商品"))
            end
        end
	end

	self.m_InputNum = num
	self.m_InputLab.text = num

	if self.m_InputNumMax - num < 0 then
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end

	if num < 0 then
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end
    self.TotallPriceLab.text = self.m_InputNum * self.m_CurGoodsPrice
end

function LuaBusinessBuyOrSellWnd:OnOKBtnClick(go)
    if self.m_InputNum == 0 then
        if LuaBusinessMgr.m_BusinessIsBuy == 1 and self.m_InputNumMax == 0 then
            if self.m_MaxReason == 1 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("马车仓库容量不足"))
            elseif self.m_MaxReason == 2 then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("当前通宝不足，请取出投资或卖掉其他商品"))
            end
        end
        return
    end
    if LuaBusinessMgr.m_BusinessIsBuy == 1 then
        if LuaBusinessMgr.m_CurCityGoodsSet[self.m_CurGoodsType] then
            Gac2Gas.TSBuyItem(self.m_CurGoodsType, self.m_InputNum)
        else
            g_MessageMgr:ShowMessage("BUSINESS_GOODS_NOTONSALE")
        end
    else
        if self.m_CurGoodsType then
            Gac2Gas.TSSellItem(self.m_CurGoodsType, self.m_InputNum)
        end
    end
end

function LuaBusinessBuyOrSellWnd:InitItem(item, info)
    local icon      = item.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local nameLab   = item.transform:Find("NameLab"):GetComponent(typeof(UILabel))
    local priceLab  = item.transform:Find("Price/PriceLab"):GetComponent(typeof(UILabel))
    local redMark   = item.transform:Find("RedMark").gameObject
    local greenMark = item.transform:Find("GreenMark").gameObject

    local data = Business_Goods.GetData(info.type)
    local sellPrice = LuaBusinessMgr.m_CurCityGoodsType2Price[info.type]
    local buyPrice = info.price -- 出售时
    icon:LoadMaterial(data.Icon)
    nameLab.text = data.Name
    priceLab.text = sellPrice
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaBusinessMgr.m_BusinessCurGoods = info.type
		CUIManager.ShowUI(CLuaUIResources.BusinessItemInfoWnd)	
	end)

    local color
    local isBuying = LuaBusinessMgr.m_BusinessIsBuy == 1
    
    if isBuying and not info.onSale then
        color = self.m_GrayColor
    elseif LuaBusinessMgr:GetCityOfSpecialGoods(info.type) then
        color = Color.yellow
    else
        color = Color.white
    end

    nameLab.color = color

    greenMark:SetActive(not isBuying and buyPrice > sellPrice)
    redMark:SetActive(not isBuying and sellPrice > buyPrice)
end

function LuaBusinessBuyOrSellWnd:OnBusinessDataUpdate()
    if LuaBusinessMgr.m_BusinessIsBuy ~= 1 then
        self.GoodsView:ReloadData(false, true)
    end
    local isEmpty = false
    if LuaBusinessMgr.m_BusinessIsBuy == 1 then
        isEmpty = #LuaBusinessMgr.m_CurCityGoods == 0
    else
        isEmpty = #LuaBusinessMgr.m_Inventory == 0
    end

    if isEmpty then
        self:NoGoods()
    elseif self:IsNoGoods() then
        if LuaBusinessMgr.m_BusinessIsBuy == 1 then
            self.self.m_CurBuyGoodsRow = #LuaBusinessMgr.m_CurCityGoods - 1
        else
            self.m_CurSellGoodsRow = #LuaBusinessMgr.m_Inventory - 1
        end
        self:RefreshSelect()
    else
        self:RefreshSelect()
    end
end

function LuaBusinessBuyOrSellWnd:NoGoods()
    self.m_InputNum = 0
    self.m_InputNumMax = 0
    self.m_CurGoodsPrice = 0
    self.GoodIcon:LoadMaterial("")
    self.GoodName.text = ""
    self.m_CurPriceLab.text = 0
	self.m_InputLab.text = 0
	self.m_IncreaseButton.Enabled = false
	self.m_DecreaseButton.Enabled = false

    self.OwnAmountLab.text = 0
    self.PriceLab.text = LocalString.GetString("暂无")
    self.TotallPriceLab.text = LocalString.GetString("暂无")
    self.PriceLab.color = Color.white
    self.TotallPriceLab.color = Color.white
    self.OwnMoneyLab.text = SafeStringFormat3("%.f", LuaBusinessMgr.m_OwnMoney)
end

function LuaBusinessBuyOrSellWnd:GetGuideGo(methodName)
    if LuaBusinessMgr.m_BusinessIsBuy == 1 then
        if methodName == "GetBuyBtn" then
            return self.YellowButton.gameObject
        elseif methodName == "GetCloseBtn" then
            return self.m_CloseBtn.gameObject
        end
    end
	return nil
end
