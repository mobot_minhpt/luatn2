local LuaGameObject=import "LuaGameObject"

local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Constants = import "L10.Game.Constants"
local MessageMgr = import "L10.Game.MessageMgr"

CLuaZNQDiscountView=class()
RegistClassMember(CLuaZNQDiscountView,"m_Template")
RegistClassMember(CLuaZNQDiscountView,"m_PreviewItemTemplate")

RegistClassMember(CLuaZNQDiscountView,"m_Ids")
RegistClassMember(CLuaZNQDiscountView,"m_TimeRemain")
RegistClassMember(CLuaZNQDiscountView,"m_CountDownTick")

function CLuaZNQDiscountView:Init()
    self.m_Ids={}
    self.m_TimeRemain=0
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick=nil
    end

    self.m_Template=LuaGameObject.GetChildNoGC(self.transform,"Template").gameObject
    self.m_Template:SetActive(false)
    self.m_PreviewItemTemplate=LuaGameObject.GetChildNoGC(self.transform,"PreviewItemTemplate").gameObject
    self.m_PreviewItemTemplate:SetActive(false)
    self:HideTime()
    Gac2Gas.ZhouNianShopQueryPackage()
    
end

function CLuaZNQDiscountView:OnEnable()
    g_ScriptEvent:AddListener("ZhouNianShopQueryPackageResult", self, "OnUpdateMallLimit")
end

function CLuaZNQDiscountView:OnDisable()
    g_ScriptEvent:RemoveListener("ZhouNianShopQueryPackageResult", self, "OnUpdateMallLimit")
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick=nil
    end
end

function CLuaZNQDiscountView:OnDestroy()
   if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick=nil
    end
end

function CLuaZNQDiscountView:InitItems()
    local function OnBuyButtonClicked(go)
        self:OnClickBuyButton(go)
    end
    local function OnPreviewItemClicked(go)
        self:OnClickPreviewItem(go)
    end

    local grid=LuaGameObject.GetChildNoGC(self.transform,"Grid").grid
    CUICommonDef.ClearTransform(grid.transform)

    for i,v in ipairs(CLuaZhouNianQingMgr.m_ShopInfo) do
        local go=NGUITools.AddChild(grid.gameObject,self.m_Template)
        go:SetActive(true)
        local designData = ZhouNianQing_LimitedShop.GetData(tonumber(v.packageId))

        LuaGameObject.GetChildNoGC(go.transform,"NameLabel").label.text=designData.Name
        LuaGameObject.GetChildNoGC(go.transform,"DiscountLabel").label.text=SafeStringFormat3(LocalString.GetString("%d折"),designData.Discount * 10)
        
        LuaGameObject.GetChildNoGC(go.transform,"OriginalPriceLabel").label.text=math.floor(tostring(designData.Price/designData.Discount))
        if designData.PriceType == 1 then
            LuaGameObject.GetChildNoGC(go.transform,"OriginalPriceSprite").sprite.spriteName=Money.GetIconName(EnumMoneyType.YinLiang)
        else
            LuaGameObject.GetChildNoGC(go.transform,"OriginalPriceSprite").sprite.spriteName=Money.GetIconName(EnumMoneyType.LingYu)
        end
        
        LuaGameObject.GetChildNoGC(go.transform,"NowPriceLabel").label.text=tostring(designData.Price)
        if designData.PriceType == 1 then
            LuaGameObject.GetChildNoGC(go.transform,"NowPriceSprite").sprite.spriteName=Money.GetIconName(EnumMoneyType.YinLiang)
        else
            LuaGameObject.GetChildNoGC(go.transform,"NowPriceSprite").sprite.spriteName=Money.GetIconName(EnumMoneyType.LingYu)
        end

        local bg = LuaGameObject.GetChildNoGC(go.transform,"BGTexture").cTexture
        if i % 3 == 0 then
            bg:LoadMaterial("UI/Texture/Transparent/Material/qmpk_guanjun.mat")
        elseif i % 3 == 1 then
            bg:LoadMaterial("UI/Texture/Transparent/Material/qmpk_jijun.mat")
        else
            bg:LoadMaterial("UI/Texture/Transparent/Material/qmpk_dianjun.mat")
        end
        
        local button=LuaGameObject.GetChildNoGC(go.transform,"BuyButton").qnButton
        if v.remainBuyTimes>0 then
            CUICommonDef.SetActive(button.gameObject,true,true)
            button.Text=LocalString.GetString("购买") 
        else
            CUICommonDef.SetActive(button.gameObject,false,true)
            button.Text=LocalString.GetString("售完") 
        end

        CommonDefs.AddOnClickListener(button.gameObject,DelegateFactory.Action_GameObject(OnBuyButtonClicked),false)

        local itemsTmp=g_LuaUtil:StrSplit(designData.Items, ";") 
        local items = {}
        for i =1, #itemsTmp do
            if itemsTmp[i] and itemsTmp[i] ~= "" then
                table.insert(items, itemsTmp[i])
            end 
        end

        local itemParent=LuaGameObject.GetChildNoGC(go.transform,"Grid").gameObject
        local len=#items

        for i=0,len-1 do
            local itemInfo = g_LuaUtil:StrSplit(items[i+1], ",") 
            if itemInfo and #itemInfo >= 2 then
                local previewItem=NGUITools.AddChild(itemParent,self.m_PreviewItemTemplate)
                previewItem:SetActive(true)
                self:InitPreviewItem(previewItem, itemInfo[1], itemInfo[2], false)
                local x,y,z=self:GetPosition(i,len)
                LuaUtils.SetLocalPosition(previewItem.transform,x,y,z)
                CommonDefs.AddOnClickListener(previewItem,DelegateFactory.Action_GameObject(OnPreviewItemClicked),false)
            end
        end
    end
    grid:Reposition()
end


function CLuaZNQDiscountView:HideTime()
    LuaGameObject.GetChildNoGC(self.transform,"TimeTitle").gameObject:SetActive(false)
end

function CLuaZNQDiscountView:ShowTime()
    if not self.transform then return end
    local timeTitle = LuaGameObject.GetChildNoGC(self.transform,"TimeTitle").gameObject
    timeTitle:SetActive(true)
    local countDownLabel = LuaGameObject.GetChildNoGC(timeTitle.transform, "CountDownLabel").label
    local hour = math.floor(self.m_TimeRemain / 3600)
    local minute = math.floor(self.m_TimeRemain % 3600 / 60)
    local second = math.floor(self.m_TimeRemain % 60)
    if hour > 0 then
        countDownLabel.text = SafeStringFormat3("%02d:%02d:%02d", hour, minute, second)
    else
        countDownLabel.text = SafeStringFormat3("%02d:%02d", minute, second)
    end
end

function CLuaZNQDiscountView:OnUpdateMallLimit()
    self:InitItems()

    self.m_TimeRemain = CLuaZhouNianQingMgr.m_ShopRemainTime

    if self.m_TimeRemain >= 0 then
        self:ShowTime()
    end

    local timeCountDown = function ()
        self.m_TimeRemain = self.m_TimeRemain - 1
        if self.m_TimeRemain < 0 then
            if self.m_CountDownTick then
                UnRegisterTick(self.m_CountDownTick)
                self.m_CountDownTick=nil
            end
        else 
            self:ShowTime()
        end
    end
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick=nil
    end
    self.m_CountDownTick = RegisterTick(timeCountDown,1000)
end

function  CLuaZNQDiscountView:InitPreviewItem(go, id, num, more)
    if more then
        LuaGameObject.GetChildNoGC(go.transform,"Normal").gameObject:SetActive(false)
        LuaGameObject.GetChildNoGC(go.transform,"MoreContent").gameObject:SetActive(true)
    else
        LuaGameObject.GetChildNoGC(go.transform,"Normal").gameObject:SetActive(true)
        LuaGameObject.GetChildNoGC(go.transform,"MoreContent").gameObject:SetActive(false)

        LuaGameObject.GetChildNoGC(go.transform,"NumLabel").label.text=tostring(num)
        local itemData=Item_Item.GetData(id)
        LuaGameObject.GetChildNoGC(go.transform,"IconTexture").cTexture:LoadMaterial(itemData.Icon,false)
    end
end

-- 处理item的位置
function CLuaZNQDiscountView:GetPosition(index,num)
    if num==1 then
        return 0,0,0
    elseif num==2 then
        if index==0 then
            return -70,0,0
        else
            return 70,0,0
        end
    elseif num==3 then
        if index==0 then
            return 0,70,0
        elseif index==1 then
            return -70,-70,0
        elseif index==2 then
            return 70,-70,0
        end
    elseif num==4 then
        if index==0 then
            return -70,70,0
        elseif index==1 then
            return 70,-70,0
        elseif index==2 then
            return -70,-70,0
        elseif index==3 then
            return 70,-70,0
        end
    end
end

-- 处理点击购买
function CLuaZNQDiscountView:OnClickBuyButton(go)
    local index=go.transform.parent:GetSiblingIndex()
    local id=CLuaZhouNianQingMgr.m_ShopInfo[index+1].packageId

    local designData = ZhouNianQing_LimitedShop.GetData(id)
    if designData.PriceType == 1 then
        local silver = CClientMainPlayer.Inst.Silver
        if silver >= designData.Price then
            Gac2Gas.ZhouNianShopBuyPackage(id)
        else
            local msg = MessageMgr.Inst:FormatMessage("NotEnough_Silver_QuickBuy", {})
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
                function()
                    CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
                end
            ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end
    else
        local jade = CClientMainPlayer.Inst.Jade
        if jade >= designData.Price then
            Gac2Gas.ZhouNianShopBuyPackage(id)
        else
            MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(
                function()
                    CShopMallMgr.ShowChargeWnd()
                end
            ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        end
    end
end

-- 点击查看物品详细信息
function CLuaZNQDiscountView:OnClickPreviewItem(go)
    local index=go.transform.parent.parent:GetSiblingIndex()
    local id=CLuaZhouNianQingMgr.m_ShopInfo[index+1].packageId
    local items={}

    local shopData = ZhouNianQing_LimitedShop.GetData(id)
    local itemsTmp=g_LuaUtil:StrSplit(shopData.Items, ";") 
    local items = {}
    for i =1, #itemsTmp do
        if itemsTmp[i] and itemsTmp[i] ~= "" then
            table.insert(items, itemsTmp[i])
        end 
    end

    local index2=go.transform:GetSiblingIndex()
    local itemInfo = g_LuaUtil:StrSplit(items[index2+1], ",") 
    if itemInfo and #itemInfo >= 2 then
        local itemId=itemInfo[1]
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default,0,0,0,0)
    end
    
end

return CLuaZNQDiscountView