-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CPetPKSettingWnd = import "L10.UI.CPetPKSettingWnd"
local EnumClass = import "L10.Game.EnumClass"
local EnumPetCtrlType = import "L10.Game.EnumPetCtrlType"
local GameSetting_Client_Wapper = import "L10.Game.GameSetting_Client_Wapper"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPetPKSettingWnd.m_Init_CS2LuaHook = function (this) 

    local cls = EnumClass.Undefined
    if CClientMainPlayer.Inst ~= nil then
        cls = CClientMainPlayer.Inst.Class
    end
    this.followModeCheckbox.gameObject:SetActive(true)
    repeat
        local default = cls
        if default == EnumClass.YiRen then
            this.titleLabel.text = LocalString.GetString("鬼灵战斗设置")
            break
        elseif default == EnumClass.SheShou then
            this.titleLabel.text = LocalString.GetString("箭塔战斗设置")
            this.followModeCheckbox.gameObject:SetActive(false)
            break
        elseif default == EnumClass.DaoKe then
            this.titleLabel.text = LocalString.GetString("分身战斗设置")
            break
        elseif default == EnumClass.YanShi then
            this.titleLabel.text = LocalString.GetString("机关战斗设置")
            break
        elseif default == EnumClass.HuaHun then
            this.titleLabel.text = LocalString.GetString("分身战斗设置")
            break
        else
            this.titleLabel.text = LocalString.GetString("战斗设置")
            break
        end
    until 1

    this.passiveModeCheckbox.Text = GameSetting_Client_Wapper.Inst.Pet_AttMode_String_BeiDong
    this.positiveModeCheckbox.Text = GameSetting_Client_Wapper.Inst.Pet_AttMode_String_ZhuDong
    this.followModeCheckbox.Text = GameSetting_Client_Wapper.Inst.Pet_AttMode_String_GenSui

    this:OnMainPlayerPetCtrlTypeUpdate()
end
CPetPKSettingWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.okBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okBtn).onClick, MakeDelegateFromCSFunction(this.OnOKButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    this.passiveModeCheckbox.OnClick = CommonDefs.CombineListner_Action_QnButton(this.passiveModeCheckbox.OnClick, MakeDelegateFromCSFunction(this.OnCheckBoxClick, MakeGenericClass(Action1, QnButton), this), true)
    this.positiveModeCheckbox.OnClick = CommonDefs.CombineListner_Action_QnButton(this.positiveModeCheckbox.OnClick, MakeDelegateFromCSFunction(this.OnCheckBoxClick, MakeGenericClass(Action1, QnButton), this), true)
    this.followModeCheckbox.OnClick = CommonDefs.CombineListner_Action_QnButton(this.followModeCheckbox.OnClick, MakeDelegateFromCSFunction(this.OnCheckBoxClick, MakeGenericClass(Action1, QnButton), this), true)
end
CPetPKSettingWnd.m_OnOKButtonClick_CS2LuaHook = function (this, go) 

    local type = EnumPetCtrlType.ePassive
    if this.passiveModeCheckbox.Selected then
        type = EnumPetCtrlType.ePassive
    elseif this.positiveModeCheckbox.Selected then
        type = EnumPetCtrlType.eActive
    elseif this.followModeCheckbox.Selected then
        type = EnumPetCtrlType.eFollow
    end
    Gac2Gas.SetPetBehaviorType(EnumToInt(type))
    this:Close()
end
CPetPKSettingWnd.m_OnMainPlayerPetCtrlTypeUpdate_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        this.passiveModeCheckbox:SetSelected(false, true)
        this.positiveModeCheckbox:SetSelected(false, true)
        this.followModeCheckbox:SetSelected(false, true)
    else
        local type = CommonDefs.ConvertIntToEnum(typeof(EnumPetCtrlType), CClientMainPlayer.Inst.FightProp.PetCtrlType)
        this.passiveModeCheckbox:SetSelected(type == EnumPetCtrlType.ePassive, true)
        this.positiveModeCheckbox:SetSelected(type == EnumPetCtrlType.eActive, true)
        this.followModeCheckbox:SetSelected(type == EnumPetCtrlType.eFollow, true)
    end
end
