require("common/common_include")

local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UISlider = import "UISlider"
local UITexture = import "UITexture"
local CUIFx = import "L10.UI.CUIFx"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CUIManager = import "L10.UI.CUIManager"
local CButton = import "L10.UI.CButton"
local LuaTweenUtils = import "LuaTweenUtils"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local NGUIMath = import "NGUIMath"
local Vector4 = import "UnityEngine.Vector4"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local Ease = import "DG.Tweening.Ease"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaCityWarGongFengView = class()

RegistClassMember(LuaCityWarGongFengView, "m_Inited")

RegistClassMember(LuaCityWarGongFengView, "m_ShenTanFireFx")
RegistClassMember(LuaCityWarGongFengView, "m_ShenTanWoodFx")
RegistClassMember(LuaCityWarGongFengView, "m_ShenTanIceFx")
RegistClassMember(LuaCityWarGongFengView, "m_ShenTanWindFx")
RegistClassMember(LuaCityWarGongFengView, "m_ShenTanFireTexture")
RegistClassMember(LuaCityWarGongFengView, "m_ShenTanWoodTexture")
RegistClassMember(LuaCityWarGongFengView, "m_ShenTanIceTexture")
RegistClassMember(LuaCityWarGongFengView, "m_ShenTanWindTexture")

RegistClassMember(LuaCityWarGongFengView, "m_ShenTanActivateValueLabel")

RegistClassMember(LuaCityWarGongFengView, "m_FireProgressRoot")
RegistClassMember(LuaCityWarGongFengView, "m_WoodProgressRoot")
RegistClassMember(LuaCityWarGongFengView, "m_IceProgressRoot")
RegistClassMember(LuaCityWarGongFengView, "m_WindProgressRoot")

--镖车相关
RegistClassMember(LuaCityWarGongFengView, "m_FirstBiaoCheNameLabel")
RegistClassMember(LuaCityWarGongFengView, "m_FirstBiaoCheHint")
RegistClassMember(LuaCityWarGongFengView, "m_FirstBiaoCheContentRoot")
RegistClassMember(LuaCityWarGongFengView, "m_FirstBiaoCheInfoItem")
RegistClassMember(LuaCityWarGongFengView, "m_FirstBiaoCheMsgLabel")
RegistClassMember(LuaCityWarGongFengView, "m_FirstBiaoCheBtnFx")
RegistClassMember(LuaCityWarGongFengView, "m_FirstBiaoCheGoToBtn")

RegistClassMember(LuaCityWarGongFengView, "m_SecondBiaoCheNameLabel")
RegistClassMember(LuaCityWarGongFengView, "m_SecondBiaoCheHint")
RegistClassMember(LuaCityWarGongFengView, "m_SecondBiaoCheContentRoot")
RegistClassMember(LuaCityWarGongFengView, "m_SecondBiaoCheInfoItem")
RegistClassMember(LuaCityWarGongFengView, "m_SecondBiaoCheMsgLabel")
RegistClassMember(LuaCityWarGongFengView, "m_SecondBiaoCheBtnFx")
RegistClassMember(LuaCityWarGongFengView, "m_SecondBiaoCheGoToBtn")

RegistClassMember(LuaCityWarGongFengView, "m_SettingBtn")
RegistClassMember(LuaCityWarGongFengView, "m_InfoBtn")

RegistClassMember(LuaCityWarGongFengView, "m_BiaoCheInfo")

RegistClassMember(LuaCityWarGongFengView, "m_ShowSubmitNotFullFx")

function LuaCityWarGongFengView:Awake()
	self.m_ShenTanFireFx = self.transform:Find("ShengTan/Fire/Fx"):GetComponent(typeof(CUIFx))
	self.m_ShenTanWoodFx = self.transform:Find("ShengTan/Wood/Fx"):GetComponent(typeof(CUIFx))
	self.m_ShenTanIceFx = self.transform:Find("ShengTan/Ice/Fx"):GetComponent(typeof(CUIFx))
	self.m_ShenTanWindFx = self.transform:Find("ShengTan/Wind/Fx"):GetComponent(typeof(CUIFx))
	self.m_ShenTanFireTexture = self.transform:Find("ShengTan/Fire/Texture"):GetComponent(typeof(UITexture))
	self.m_ShenTanWoodTexture = self.transform:Find("ShengTan/Wood/Texture"):GetComponent(typeof(UITexture))
	self.m_ShenTanIceTexture = self.transform:Find("ShengTan/Ice/Texture"):GetComponent(typeof(UITexture))
	self.m_ShenTanWindTexture = self.transform:Find("ShengTan/Wind/Texture"):GetComponent(typeof(UITexture))
	self.m_ShenTanActivateValueLabel = self.transform:Find("ShengTan/ActivateLabel/ValueLabel"):GetComponent(typeof(UILabel))

	self.m_FireProgressRoot = self.transform:Find("ShengTan/Progress/Fire")
	self.m_WoodProgressRoot = self.transform:Find("ShengTan/Progress/Wood")
	self.m_IceProgressRoot = self.transform:Find("ShengTan/Progress/Ice")
	self.m_WindProgressRoot = self.transform:Find("ShengTan/Progress/Wind")

	local firstBiaoCheRoot = self.transform:Find("BiaoChe/First")
	self.m_FirstBiaoCheNameLabel = firstBiaoCheRoot:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.m_FirstBiaoCheHint = firstBiaoCheRoot:Find("HintLabel").gameObject
	self.m_FirstBiaoCheContentRoot = firstBiaoCheRoot:Find("Content").gameObject
	self.m_FirstBiaoCheInfoItem = firstBiaoCheRoot:Find("Content/Info"):GetComponent(typeof(CCommonLuaScript))
	self.m_FirstBiaoCheMsgLabel = firstBiaoCheRoot:Find("Content/MsgLabel"):GetComponent(typeof(UILabel))
	self.m_FirstBiaoCheBtnFx = firstBiaoCheRoot:Find("Content/ButtonFx"):GetComponent(typeof(CUIFx))
	self.m_FirstBiaoCheGoToBtn = firstBiaoCheRoot:Find("Content/GoToButton").gameObject

	local secondBiaoCheRoot = self.transform:Find("BiaoChe/Second")
	self.m_SecondBiaoCheNameLabel = secondBiaoCheRoot:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.m_SecondBiaoCheHint = secondBiaoCheRoot:Find("HintLabel").gameObject
	self.m_SecondBiaoCheContentRoot = secondBiaoCheRoot:Find("Content").gameObject
	self.m_SecondBiaoCheInfoItem = secondBiaoCheRoot:Find("Content/Info"):GetComponent(typeof(CCommonLuaScript))
	self.m_SecondBiaoCheMsgLabel = secondBiaoCheRoot:Find("Content/MsgLabel"):GetComponent(typeof(UILabel))
	self.m_SecondBiaoCheBtnFx = secondBiaoCheRoot:Find("Content/ButtonFx"):GetComponent(typeof(CUIFx))
	self.m_SecondBiaoCheGoToBtn = secondBiaoCheRoot:Find("Content/GoToButton").gameObject

	self.m_SettingBtn = self.transform:Find("BiaoChe/SettingButton").gameObject
	self.m_InfoBtn = self.transform:Find("BiaoChe/InfoButton").gameObject

	self.m_BiaoCheInfo = {}

	CommonDefs.AddOnClickListener(self.m_FirstBiaoCheGoToBtn, DelegateFactory.Action_GameObject(function(go) self:OnFirstBiaoCheGoToButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SecondBiaoCheGoToBtn, DelegateFactory.Action_GameObject(function(go) self:OnSecondBiaoCheGoToButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SettingBtn, DelegateFactory.Action_GameObject(function(go) self:OnBiaoCheSettingButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_InfoBtn, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
end

function LuaCityWarGongFengView:ShowProgress(zone, progressVal)
	local progressRoot = nil
	if zone == 1 then
		progressRoot = self.m_FireProgressRoot
	elseif zone == 2 then
		progressRoot = self.m_WoodProgressRoot
	elseif zone == 3 then
		progressRoot = self.m_IceProgressRoot
	elseif zone ==4 then
		progressRoot = self.m_WindProgressRoot
	end
	if not progressRoot then return end

	local threshold = YaYun_Setting.GetData().AltarActiveThreshold
	local val = (progressVal or 0)/threshold
	progressRoot:Find("Icon"):GetComponent(typeof(UISprite)).spriteName = CityWarMapZone2ResIcon[zone]
	progressRoot:Find("ValueLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d/%d", progressVal or 0, threshold)
	progressRoot:Find("ProgressBar"):GetComponent(typeof(UISlider)).value = val
end

function LuaCityWarGongFengView:ShowShenTan(zone, show)
	local shenTanFx = nil
	local texture = nil
	local fxPath = nil
	if zone == 1 then
		shenTanFx = self.m_ShenTanFireFx
		texture = self.m_ShenTanFireTexture
		fxPath = "fx/ui/prefab/UI_shentan_huo.prefab"
	elseif zone == 2 then
		shenTanFx = self.m_ShenTanWoodFx
		texture = self.m_ShenTanWoodTexture
		fxPath = "fx/ui/prefab/UI_shentan_mu.prefab"
	elseif zone == 3 then
		shenTanFx = self.m_ShenTanIceFx
		texture = self.m_ShenTanIceTexture
		fxPath = "fx/ui/prefab/UI_shentan_bing.prefab"
	elseif zone ==4 then
		shenTanFx = self.m_ShenTanWindFx
		texture = self.m_ShenTanWindTexture
		fxPath = "fx/ui/prefab/UI_shentan_feng.prefab"
	end
	if not shenTanFx then return end

	if show then
		shenTanFx:LoadFx(fxPath)
		texture.alpha = 1
		self:SetGrey(texture.transform, false)
	else
		shenTanFx:DestroyFx()
		texture.alpha = 0.8
		self:SetGrey(texture.transform, true)
	end
end

function LuaCityWarGongFengView:SetGrey(trans, showGrey)
	local pos = trans.localPosition
	if showGrey then
		pos.z = -1
	else
		pos.z = 0
	end
	trans.localPosition = pos
end

function LuaCityWarGongFengView:PlayGoToButtonFx(fx, targetTransform)
	LuaTweenUtils.DOKill(fx.FxRoot, false)
    fx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
    local b = NGUIMath.CalculateRelativeWidgetBounds(targetTransform)
    local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
    fx.FxRoot.localPosition = waypoints[0]
    LuaTweenUtils.DOLuaLocalPath(fx.FxRoot, waypoints, 1.5, PathType.Linear, PathMode.Ignore, 10, -1, Ease.Linear)
end

function LuaCityWarGongFengView:ShowBiaoChe(biaoCheInfo, showSubmitNotFullFx)
	self.m_BiaoCheInfo = biaoCheInfo or {}
	local mapId = self.m_BiaoCheInfo.biaoCheFMapId or 0
	local progress = self.m_BiaoCheInfo.biaoCheFProgress or 0
	local status = self.m_BiaoCheInfo.biaoCheFStatus or 0
	local closeTime = self.m_BiaoCheInfo.biaoCheCloseTime or 0
	local map = CityWar_Map.GetData(mapId)
	self.m_FirstBiaoCheBtnFx:DestroyFx()
	LuaTweenUtils.DOKill(self.m_FirstBiaoCheBtnFx.FxRoot, false)
	if map then
		self.m_FirstBiaoCheNameLabel.text = SafeStringFormat3(LocalString.GetString("镖车·%s"), map.Name)
		self.m_FirstBiaoCheHint:SetActive(false)
		self.m_FirstBiaoCheContentRoot:SetActive(true)
		self.m_FirstBiaoCheInfoItem.m_LuaSelf:Init(mapId, progress, status, closeTime)
		self.m_FirstBiaoCheMsgLabel.text = CLuaCityWarMgr.GetMsgByStatusAndProgress(status, progress)
		self.m_FirstBiaoCheGoToBtn:GetComponent(typeof(CButton)).Enabled = (status==EnumBiaoCheStatus.eSubmitRes or status==EnumBiaoCheStatus.eYaYun)
		if showSubmitNotFullFx and status~=EnumBiaoCheStatus.eYaYunEnd then
			self:PlayGoToButtonFx(self.m_FirstBiaoCheBtnFx, self.m_FirstBiaoCheGoToBtn.transform)
		end
	else
		self.m_FirstBiaoCheNameLabel.text = LocalString.GetString("镖车·未设置")
		self.m_FirstBiaoCheHint:SetActive(true)
		self.m_FirstBiaoCheContentRoot:SetActive(false)
	end

	mapId = self.m_BiaoCheInfo.biaoCheSMapId or 0
	progress = self.m_BiaoCheInfo.biaoCheSProgress or 0
	status = self.m_BiaoCheInfo.biaoCheSStatus or 0

	map = CityWar_Map.GetData(mapId)
	self.m_SecondBiaoCheBtnFx:DestroyFx()
	LuaTweenUtils.DOKill(self.m_SecondBiaoCheBtnFx.FxRoot, false)
	if map then
		self.m_SecondBiaoCheNameLabel.text = SafeStringFormat3(LocalString.GetString("镖车·%s"), map.Name)
		self.m_SecondBiaoCheHint:SetActive(false)
		self.m_SecondBiaoCheContentRoot:SetActive(true)
		self.m_SecondBiaoCheInfoItem.m_LuaSelf:Init(mapId, progress, status, closeTime)
		self.m_SecondBiaoCheMsgLabel.text = CLuaCityWarMgr.GetMsgByStatusAndProgress(status, progress)
		self.m_SecondBiaoCheGoToBtn:GetComponent(typeof(CButton)).Enabled = (status==EnumBiaoCheStatus.eSubmitRes or status==EnumBiaoCheStatus.eYaYun)
		if showSubmitNotFullFx and status~=EnumBiaoCheStatus.eYaYunEnd then
			self:PlayGoToButtonFx(self.m_SecondBiaoCheBtnFx, self.m_SecondBiaoCheGoToBtn.transform)
		end
	else
		self.m_SecondBiaoCheNameLabel.text = LocalString.GetString("镖车·未设置")
		self.m_SecondBiaoCheHint:SetActive(true)
		self.m_SecondBiaoCheContentRoot:SetActive(false)
	end

end

function LuaCityWarGongFengView:OnFirstBiaoCheGoToButtonClick()
	if self.m_BiaoCheInfo and self.m_BiaoCheInfo.biaoCheFMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheFMapId) then
		CLuaCityWarMgr.RequestTrackToBiaoChe(self.m_BiaoCheInfo.biaoCheFMapId, self.m_BiaoCheInfo.biaoCheFStatus)
	end
end

function LuaCityWarGongFengView:OnSecondBiaoCheGoToButtonClick()
	if self.m_BiaoCheInfo and self.m_BiaoCheInfo.biaoCheSMapId and CityWar_Map.Exists(self.m_BiaoCheInfo.biaoCheSMapId) then
		CLuaCityWarMgr.RequestTrackToBiaoChe(self.m_BiaoCheInfo.biaoCheSMapId, self.m_BiaoCheInfo.biaoCheSStatus)
	end
end

function LuaCityWarGongFengView:OnBiaoCheSettingButtonClick()
	CLuaCityWarMgr:OpenPrimaryMap(3)
	CUIManager.CloseUI("CityMainWnd")
end

function LuaCityWarGongFengView:OnInfoButtonClick()
	g_MessageMgr:ShowMessage("YAYUN_GONG_FENG_TIPS")
end

function LuaCityWarGongFengView:OnEnable( ... )
	g_ScriptEvent:AddListener("SyncQueryGuildAltarResult", self, "SyncQueryGuildAltarResult")
	self:InitInfo()
	self.m_ShowSubmitNotFullFx = CLuaCityWarMgr.MyCityWndJumpToGongFeng or false
	CLuaCityWarMgr.MyCityWndJumpToGongFeng = false
	Gac2Gas.QueryGuildAltarInfo()
end

function LuaCityWarGongFengView:OnDisable( ... )
	g_ScriptEvent:RemoveListener("SyncQueryGuildAltarResult", self, "SyncQueryGuildAltarResult")
end

function LuaCityWarGongFengView:InitInfo()
	for zone=1,4 do
		self:ShowProgress(zone, 0)
		self:ShowShenTan(zone, false)
	end
	self.m_ShenTanActivateValueLabel.text = "0/4"
	self:ShowBiaoChe(nil, false)
end

function LuaCityWarGongFengView:SyncQueryGuildAltarResult(zone2ProgressTbl, activatedTbl, biaoCheInfoTbl)
	for zone,progress in pairs(zone2ProgressTbl) do
		self:ShowProgress(zone, progress)
	end

	local t = {}
	for _,zone in pairs(activatedTbl) do
		t[zone] = 1
	end
	local cnt = 0
	for zone=1,4 do
		local show = t[zone] or false
		self:ShowShenTan(zone, show)
		if show then cnt=cnt+1 end
	end

	self.m_ShenTanActivateValueLabel.text = SafeStringFormat3("%d/4", cnt)

	self:ShowBiaoChe(biaoCheInfoTbl, self.m_ShowSubmitNotFullFx)
	self.m_ShowSubmitNotFullFx = false --仅在第一次显示
end

return LuaCityWarGongFengView
