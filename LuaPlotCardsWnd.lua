local QnTableView = import "L10.UI.QnTableView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local QnTabButton = import "L10.UI.QnTabButton"

LuaPlotCardsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPlotCardsWnd, "LeftScrollView", "LeftScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPlotCardsWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaPlotCardsWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaPlotCardsWnd, "RightScrollView", "RightScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPlotCardsWnd, "RightTable", "RightTable", UITable)
RegistChildComponent(LuaPlotCardsWnd, "Tab1Table", "Tab1Table", UITable)
RegistChildComponent(LuaPlotCardsWnd, "Tab2Table", "Tab2Table", UITable)
RegistChildComponent(LuaPlotCardsWnd, "Tab1RedDot", "Tab1RedDot", GameObject)
RegistChildComponent(LuaPlotCardsWnd, "Tab2RedDot", "Tab2RedDot", GameObject)
RegistChildComponent(LuaPlotCardsWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaPlotCardsWnd, "RightView", "RightView", QnTableView)
RegistChildComponent(LuaPlotCardsWnd, "TipButton", "TipButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaPlotCardsWnd,"m_TabBar1")
RegistClassMember(LuaPlotCardsWnd,"m_TabBar2")
RegistClassMember(LuaPlotCardsWnd,"m_TabsTable")
RegistClassMember(LuaPlotCardsWnd,"m_ChildTabs")
RegistClassMember(LuaPlotCardsWnd,"m_TabIndex")
RegistClassMember(LuaPlotCardsWnd,"m_ChildTabIndex")
RegistClassMember(LuaPlotCardsWnd,"m_HaoYiXingCardsData") --服务器同步过来的皓衣行界面卡牌数据
RegistClassMember(LuaPlotCardsWnd,"m_CurHaoYiXingCardIdsList") --当前要显示的皓衣行卡牌
RegistClassMember(LuaPlotCardsWnd,"m_YaoGuiHuCardsData") --妖鬼狐数据
RegistClassMember(LuaPlotCardsWnd,"m_CurYaoGuiHuCardsData") --当前要显示的妖鬼狐数据
RegistClassMember(LuaPlotCardsWnd,"m_AllCardList")

function LuaPlotCardsWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)

    UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
    --@endregion EventBind end
end

function LuaPlotCardsWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("PlotCardsWnd_ReadMe")
end

function LuaPlotCardsWnd:OnEnable()
    g_ScriptEvent:AddListener("OnPlotRequestCardsLikeInfo",self,"OnPlotRequestCardsLikeInfo") 
    g_ScriptEvent:AddListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:AddListener("OnPlotDiscussionCardFavorited", self, "OnPlotDiscussionCardFavorited")
    g_ScriptEvent:AddListener("SyncErHaTCGData",self,"OnSyncErHaTCGData") 
    LuaPlotDiscussionMgr:RequestRedDotInfo()
end

function LuaPlotCardsWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnPlotRequestCardsLikeInfo",self,"OnPlotRequestCardsLikeInfo")
    g_ScriptEvent:RemoveListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:RemoveListener("OnPlotDiscussionCardFavorited", self, "OnPlotDiscussionCardFavorited")
    g_ScriptEvent:RemoveListener("SyncErHaTCGData",self,"OnSyncErHaTCGData")
end

function LuaPlotCardsWnd:OnPlotRequestCardsLikeInfo()
    self:OnChildTabChange(self.m_ChildTabIndex)
end

function LuaPlotCardsWnd:OnPlotCommentShowRedAlert(haoYiXingCardId, yaoGuiHuCardId, isClear)
    --print(haoYiXingCardId, yaoGuiHuCardId)
    if self.m_TabIndex == 0 and self.m_CurHaoYiXingCardIdsList then
        for index, id in pairs(self.m_CurHaoYiXingCardIdsList) do
            local item = self.RightView:GetItemAtRow(index - 1)
            if item then
                local alertSprite = item.transform:Find("AlertSprite").gameObject
                if isClear then
                    alertSprite:SetActive(false)
                end
                if id == haoYiXingCardId then
                    alertSprite:SetActive(true)
                end
            end
        end
    end
    if self.m_TabIndex == 1 and self.m_CurYaoGuiHuCardsData then
        for index, data in pairs(self.m_CurYaoGuiHuCardsData) do
            local item = self.RightView:GetItemAtRow(index - 1)
            if item then
                local alertSprite = item.transform:Find("AlertSprite").gameObject
                if isClear then
                    alertSprite:SetActive(false)
                end
                if data.GroupID == yaoGuiHuCardId then
                    alertSprite:SetActive(true)
                end
            end
        end
    end
    self:UpdateTabsAlert()
end

function LuaPlotCardsWnd:OnPlotDiscussionCardFavorited(cardId, isMyFavor)
    if cardId ~= 0 then
        if self.m_TabIndex == 0 and self.m_CurHaoYiXingCardIdsList then
            for index, id in pairs(self.m_CurHaoYiXingCardIdsList) do
                if id == cardId then
                    local item = self.RightView:GetItemAtRow(index - 1)
                    if item then
                        self:InitHaoYiXingTemplate(item,index - 1)
                    end
                    break
                end
            end
        elseif self.m_TabIndex == 1 and self.m_CurYaoGuiHuCardsData then
            for index, data in pairs(self.m_CurYaoGuiHuCardsData) do
                if data.GroupID == cardId then
                    self:InitYaoGuiHuView(self.m_ChildTabIndex)
                end
            end
        end
    end
    LuaPlotDiscussionMgr:RequestCardsLikeInfo(self.m_AllCardList)
end

function LuaPlotCardsWnd:OnSyncErHaTCGData(data)
    self.m_HaoYiXingCardsData = {}
    local list = data.list
    for i = 1,#list do

        local d = list[i]
        local id = d.id
        table.insert(self.m_AllCardList,id)
        local isnew = d.isNew
        local num = d.num
        local pieceCount = d.pieceCount
        local taskStatus = d.taskStatus

        local t = {}
        t.tcgdata = SpokesmanTCG_ErHaTCG.GetData(id)
        t.num = num
        t.isnew = isnew
        t.id = id
        t.pieceCount = pieceCount
        t.owner = d.owner
        t.isCollected = d.isCollected
        t.taskStatus = taskStatus

        self.m_HaoYiXingCardsData[id] = t
    end
    self.Tabs:ChangeTab(LuaPlotDiscussionMgr.m_ShowCardWndType - 1, false)
    self:UpdateTabsAlert()
    LuaPlotDiscussionMgr:RequestCardsLikeInfo(self.m_AllCardList)
end

function LuaPlotCardsWnd:InitYaoGuiHuData()
    self.m_YaoGuiHuCardsData = {}
    Achievement_NewMainStory.ForeachKey(function (key)
        table.insert(self.m_YaoGuiHuCardsData,Achievement_NewMainStory.GetData(key))
        table.insert(self.m_AllCardList, key)
    end)
    LuaPlotDiscussionMgr:RequestCardsLikeInfo(self.m_AllCardList)
end

function LuaPlotCardsWnd:Init()
    self.m_AllCardList = {}
    self.TabTemplate.gameObject:SetActive(false)
    self.Template.gameObject:SetActive(false)
    self:InitYaoGuiHuData()
    self:InitTabs()
end

function LuaPlotCardsWnd:InitTabs()
    local tab1 = self.Tabs.transform:GetChild(0) 
    local tab2 = self.Tabs.transform:GetChild(1) 
    tab1.gameObject:SetActive(LuaHaoYiXingMgr.m_IsPlayOpen)
    self.m_TabsTable = self.Tabs:GetComponent(typeof(UITable))
    self.m_ChildTabs = {}

    local tab1Texts = {LocalString.GetString("红尘逸客"),LocalString.GetString("主线剧情"),LocalString.GetString("支线剧情")}
    self.m_TabBar1 = self:InitChildTabBar(tab1Texts, self.Tab1Table, 0)
    self.Tab1RedDot.gameObject:SetActive(false)

    local tab2Texts = {LocalString.GetString("奇谈"),LocalString.GetString("志异")}
    self.m_TabBar2 = self:InitChildTabBar(tab2Texts, self.Tab2Table, 1)
    self.Tab2RedDot.gameObject:SetActive(false)
    self.m_TabsTable:Reposition()
    if not LuaHaoYiXingMgr.m_IsPlayOpen then
        self.Tabs:ChangeTab(1, false)
    else
        Gac2Gas.RequestErHaTCGData()
    end
    self:UpdateTabsAlert()
end

function LuaPlotCardsWnd:InitChildTabBar(tabTexts, tabTable, tabIndex)
    self.m_ChildTabs[tabIndex] = {}
    Extensions.RemoveAllChildren(tabTable.transform)
    for index, text in pairs(tabTexts) do
        local obj = NGUITools.AddChild(tabTable.gameObject, self.TabTemplate.gameObject)
        obj:SetActive(true)
        obj.transform:Find("RedDot").gameObject:SetActive(false)
        local btn = obj:GetComponent(typeof(QnTabButton))
        btn.Text = tabTexts[index]
        table.insert(self.m_ChildTabs[tabIndex],btn)
    end
    tabTable:Reposition()
    local tabBar = tabTable.gameObject:AddComponent(typeof(UITabBar))
    tabBar:Init()
    tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnChildTabChange(index)
    end)
    tabBar:ChangeTab(0, true)
    return tabBar
end

--@region UIEvent

function LuaPlotCardsWnd:OnTabsTabChange(index)
    self.m_TabIndex = index
    for tabIndex, list in pairs(self.m_ChildTabs) do
        for _,tab in pairs(list) do
            tab.gameObject:SetActive(index == tabIndex)
        end
    end
    self.Tab1Table:Reposition()
    self.Tab2Table:Reposition()
    if index == 0 then
        self.m_TabBar1:ChangeTab(0, false)
    elseif index == 1 then
        self.m_TabBar2:ChangeTab(0, false)
    end
    self.m_TabsTable:Reposition()
    self.LeftScrollView:ResetPosition()
end

--@endregion UIEvent
function LuaPlotCardsWnd:OnChildTabChange(childIndex)
    self.m_ChildTabIndex = childIndex
    if self.m_TabIndex == 0 then
        self:InitHaoYiXingView(childIndex)
    elseif self.m_TabIndex == 1 then
        self:InitYaoGuiHuView(childIndex)
    end
end

function LuaPlotCardsWnd:InitHaoYiXingView(childIndex)
    self.m_CurHaoYiXingCardIdsList = {}
    if not self.m_HaoYiXingCardsData then return end
    for id, t in pairs(self.m_HaoYiXingCardsData) do
        if t.tcgdata.Type == (childIndex + 2) then
            table.insert(self.m_CurHaoYiXingCardIdsList, id)
        end
    end
    table.sort(self.m_CurHaoYiXingCardIdsList, function(a,b) 
        local data1 = self.m_HaoYiXingCardsData[a]
        local data2 = self.m_HaoYiXingCardsData[b]
        if data1.owner and not data2.owner then
            return true
        elseif not data1.owner and data2.owner then
            return false
        elseif data1.owner and data2.owner then
            return a < b
        end
        if (data1.pieceCount > 0 or data2.pieceCount > 0) and data1.pieceCount ~= data2.pieceCount then
            return data1.pieceCount > data2.pieceCount
        end
        return a < b
    end)
    self.RightView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_CurHaoYiXingCardIdsList
        end,
        function(item,index) self:InitHaoYiXingTemplate(item,index) end
    )
	self.RightView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectHaoYiXingTemplateAtRow(row)
    end)
    self.RightView:ReloadData(true,true)
    self.RightScrollView:ResetPosition()
end

function LuaPlotCardsWnd:IsAchievementFinished(groupId)
    local finished = false
    local achievement = Achievement_Achievement.GetDataBySubKey("Group", groupId)
    if CClientMainPlayer.Inst and achievement then
        if CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(achievement.ID) then
            finished = true
        else
            local progress = 0
            if CClientMainPlayer.Inst.PlayProp.Achievement.ProgressInfo then
                if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.Achievement.ProgressInfo, typeof(UInt32), achievement.ID) then
                    progress = tonumber(CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.Achievement.ProgressInfo, typeof(UInt32), achievement.ID).Progress[1])
                end
            end
            local totalProgress = 0
            if not System.String.IsNullOrEmpty(achievement.NeedProgress) then
                local strs = CommonDefs.StringSplit_ArrayChar(CommonDefs.StringSplit_ArrayChar(achievement.NeedProgress, ";")[0], ",")
                totalProgress = System.Int32.Parse(strs[strs.Length - 1])
            end
            if(progress ~= 0 and totalProgress ~= 0 and progress == totalProgress)then
                finished = true
            end
        end
    end
    return (groupId == 205) or finished
end

function LuaPlotCardsWnd:InitYaoGuiHuView(childIndex)
    self.m_CurYaoGuiHuCardsData = {}
    for index, data in pairs(self.m_YaoGuiHuCardsData) do
        if data.TabType == childIndex then
            table.insert(self.m_CurYaoGuiHuCardsData, data)
        end
    end
    table.sort(self.m_CurYaoGuiHuCardsData, function(a,b) 
        if CClientMainPlayer.Inst then
            local finished1 = self:IsAchievementFinished(a.GroupID)
            local finished2 = self:IsAchievementFinished(b.GroupID)
            if finished1 ~= finished2 then
                return finished1 == true
            end
        end
        return a.GroupID < b.GroupID
    end)
    self.RightView.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            return #self.m_CurYaoGuiHuCardsData
        end,
        function(item, index)
            return self:InitYaoGuiHuTemplate(nil,index) 
        end)
	self.RightView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectYaoGuiHuTemplateAtRow(row)
    end)
    self.RightView:ReloadData(true,true)
    self.RightScrollView:ResetPosition()
end

function LuaPlotCardsWnd:OnSelectHaoYiXingTemplateAtRow(row)
    local id = self.m_CurHaoYiXingCardIdsList[row + 1]
    local t = self.m_HaoYiXingCardsData[id]
    local likeNum = LuaPlotDiscussionMgr.m_CardId2LikeNum[id] and LuaPlotDiscussionMgr.m_CardId2LikeNum[id] or 0
    local isLike = LuaPlotDiscussionMgr.m_CardId2Like[id]
    local item = self.RightView:GetItemAtRow(row)
    local alertSprite = item.transform:Find("AlertSprite").gameObject
    alertSprite:SetActive(false)
    self:UpdateTabsAlert()
    local showOwnerTexture = t.owner or (t.pieceCount ~= 0 and t.pieceCount < t.tcgdata.PieceCount and t.num < 1)
    local showSuiPianTexture = t.pieceCount ~= 0 and t.pieceCount < t.tcgdata.PieceCount and t.num < 1 and not t.owner
    LuaPlotDiscussionMgr:ShowHaoYiXingPlotDiscussion(t,isLike,likeNum,showOwnerTexture,showSuiPianTexture)
end

function LuaPlotCardsWnd:OnSelectYaoGuiHuTemplateAtRow(row)
    local design = self.m_CurYaoGuiHuCardsData[row + 1]
    local groupId = design.GroupID
    local likeNum = LuaPlotDiscussionMgr.m_CardId2LikeNum[groupId] and LuaPlotDiscussionMgr.m_CardId2LikeNum[groupId] or 0
    local isLike = LuaPlotDiscussionMgr.m_CardId2Like[groupId]
    local item = self.RightView:GetItemAtRow(row)
    local alertSprite = item.transform:Find("AlertSprite").gameObject
    alertSprite:SetActive(false)
    self:UpdateTabsAlert()
    local z = (self:IsAchievementFinished(groupId)) and 0 or - 1
    LuaPlotDiscussionMgr:ShowYaoGuiHuPlotDiscussion(design,isLike,likeNum,z)
end

function LuaPlotCardsWnd:UpdateTabsAlert()
    local tab2Alert = {}
    if self.m_HaoYiXingCardsData then
        for id, t in pairs(self.m_HaoYiXingCardsData) do
            local showAlert = LuaPlotDiscussionMgr.m_ShowAlertIds[id]
            tab2Alert[t.tcgdata.Type - 2] = showAlert or tab2Alert[t.tcgdata.Type - 2]
        end
    end
    local tab1 = self.Tabs.transform:GetChild(0)
    local tab1Table = tab1.transform:Find("Tab1Table")
    local tab1ShowAlert = false
    for i = 0, tab1Table.childCount - 1 do
        local child = tab1Table:GetChild(i)
        local showAlert = tab2Alert[i]
        tab1ShowAlert = tab1ShowAlert or showAlert
        child.transform:Find("RedDot").gameObject:SetActive(showAlert)
    end
    tab1.transform:Find("Tab1RedDot").gameObject:SetActive(tab1ShowAlert)

    tab2Alert = {}
    for index, data in pairs(self.m_YaoGuiHuCardsData) do
        local showAlert = LuaPlotDiscussionMgr.m_ShowAlertIds[data.GroupID]
        tab2Alert[data.TabType] = showAlert or tab2Alert[data.TabType]
    end
    local tab2 = self.Tabs.transform:GetChild(1)
    local tab2Table = tab2.transform:Find("Tab2Table")
    local tab2ShowAlert = false
    for i = 0, tab2Table.childCount - 1 do
        local child = tab2Table:GetChild(i)
        local showAlert = tab2Alert[i]
        tab2ShowAlert = tab2ShowAlert or showAlert
        child.transform:Find("RedDot").gameObject:SetActive(showAlert)
    end
    tab2.transform:Find("Tab2RedDot").gameObject:SetActive(tab2ShowAlert)
end

function LuaPlotCardsWnd:InitHaoYiXingTemplate(obj,index)
    local id = self.m_CurHaoYiXingCardIdsList[index + 1]
    local t = self.m_HaoYiXingCardsData[id]
    local pieceNum = t.pieceCount
    local likeNum = LuaPlotDiscussionMgr.m_CardId2LikeNum[id] and LuaPlotDiscussionMgr.m_CardId2LikeNum[id] or 0

    local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local purpleTexture = obj.transform:Find("PurpleTexture").gameObject
    local redTexture = obj.transform:Find("RedTexture").gameObject
    local blueTexture = obj.transform:Find("BlueTexture").gameObject
    local sketchTexture = obj.transform:Find("SketchTexture"):GetComponent(typeof(CUITexture))
    local suiPianTexture = obj.transform:Find("SuiPianTexture").gameObject
    local alertSprite = obj.transform:Find("AlertSprite").gameObject
    local collectionSprite = obj.transform:Find("CollectionSprite"):GetComponent(typeof(UISprite))
    local collectionLabel = obj.transform:Find("CollectionLabel"):GetComponent(typeof(UILabel))
    local ownerTexture = obj.transform:Find("OwnerTexture"):GetComponent(typeof(CUITexture))

    nameLabel.text = t.tcgdata.Name
    purpleTexture.gameObject:SetActive(t.tcgdata.Type == 3 or t.tcgdata.Type == 4)
    redTexture.gameObject:SetActive(t.tcgdata.Type == 2)
    sketchTexture:LoadMaterial(t.tcgdata.Silhouette)
    suiPianTexture.gameObject:SetActive(pieceNum ~= 0 and pieceNum < t.tcgdata.PieceCount and t.num < 1 and not t.owner)
    alertSprite.gameObject:SetActive(LuaPlotDiscussionMgr.m_ShowAlertIds[id])
    collectionSprite.spriteName = LuaPlotDiscussionMgr.m_CardId2Like[id] and "personalspacewnd_heart_2" or "personalspacewnd_heart_1"
    collectionLabel.text = likeNum
    ownerTexture:LoadMaterial(t.tcgdata.Res)
    ownerTexture.gameObject:SetActive(t.owner or (pieceNum ~= 0 and pieceNum < t.tcgdata.PieceCount and t.num < 1))
end

function LuaPlotCardsWnd:InitYaoGuiHuTemplate(obj,index)
    obj = self.RightView:GetFromPool(1)
    
    local design = self.m_CurYaoGuiHuCardsData[index + 1]
    local groupId = design.GroupID
    local likeNum = LuaPlotDiscussionMgr.m_CardId2LikeNum[groupId] and LuaPlotDiscussionMgr.m_CardId2LikeNum[groupId] or 0

    local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local purpleTexture = obj.transform:Find("PurpleTexture").gameObject
    local redTexture = obj.transform:Find("RedTexture").gameObject
    local blueTexture = obj.transform:Find("BlueTexture").gameObject
    local sketchTexture = obj.transform:Find("SketchTexture"):GetComponent(typeof(CUITexture))
    local suiPianTexture = obj.transform:Find("SuiPianTexture").gameObject
    local alertSprite = obj.transform:Find("AlertSprite").gameObject
    local collectionSprite = obj.transform:Find("CollectionSprite"):GetComponent(typeof(UISprite))
    local collectionLabel = obj.transform:Find("CollectionLabel"):GetComponent(typeof(UILabel))
    local ownerTexture = obj.transform:Find("OwnerTexture"):GetComponent(typeof(CUITexture))

    nameLabel.text = design.Name
    purpleTexture.gameObject:SetActive(true)
    redTexture.gameObject:SetActive(false)
    blueTexture.gameObject:SetActive(false)
    sketchTexture.gameObject:SetActive(false)
    suiPianTexture.gameObject:SetActive(false)
    alertSprite.gameObject:SetActive(LuaPlotDiscussionMgr.m_ShowAlertIds[groupId])
    collectionSprite.spriteName = LuaPlotDiscussionMgr.m_CardId2Like[groupId] and "personalspacewnd_heart_2" or "personalspacewnd_heart_1"
    collectionLabel.text = likeNum
    ownerTexture:LoadMaterial(design.Picture)
    if CClientMainPlayer.Inst then
        Extensions.SetLocalPositionZ(ownerTexture.transform, (self:IsAchievementFinished(groupId)) and 0 or - 1)
    end

    return obj
end
