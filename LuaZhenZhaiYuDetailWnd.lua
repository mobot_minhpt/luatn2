local UIGrid = import "UIGrid"

local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"

LuaZhenZhaiYuDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "Border", "Border", UISprite)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "TagLabel", "TagLabel", UILabel)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "TableBg", "TableBg", UISprite)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "AttrLabel", "AttrLabel", UILabel)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "LingQiLabel", "LingQiLabel", UILabel)
RegistChildComponent(LuaZhenZhaiYuDetailWnd, "Background", "Background", UISprite)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhenZhaiYuDetailWnd, "m_ZswId")
RegistClassMember(LuaZhenZhaiYuDetailWnd, "m_TemplateId")
function LuaZhenZhaiYuDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaZhenZhaiYuDetailWnd:Init()
    self.m_ZswId = LuaSeaFishingMgr.SelectdZhenZhaiFishZswId
	local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(self.m_ZswId)
	local itemId = zswdata.ItemId
	local itemdata = Item_Item.GetData(itemId)
	local fishdata = HouseFish_AllFishes.GetData(itemId)
	self.Icon:LoadMaterial(itemdata.Icon)
	self.Border.spriteName = CUICommonDef.GetItemCellBorder(itemdata.NameColor)
	self.NameLabel.text = itemdata.Name
	--self.TagLabel
	local preHeight = self.DescLabel.height
	local desc = System.String.Format(itemdata.Description, fishdata.Score)
	self.DescLabel.text = CChatLinkMgr.TranslateToNGUIText(desc,false)
	local addHeight = self.DescLabel.height - preHeight
	self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),fishdata.Level)
	self.m_TemplateId = itemId
	local duration = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.duration
	local wordId = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.wordId
	if duration and duration ~= 0 then
		self.LingQiLabel.text = duration
	else
		self.LingQiLabel.text = 0
	end
	if wordId and wordId ~= 0 and Word_Word.GetData(wordId) then
		self.AttrLabel.text = Word_Word.GetData(wordId).Description
	else
		self.AttrLabel.text = SafeStringFormat3(LocalString.GetString("暂无"))
	end
	self.Background.height = addHeight + self.Background.height
	self:InitButtons()
end

function LuaZhenZhaiYuDetailWnd:InitButtons()
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaZhenZhaiYuMgr.m_SelectedFishButtons
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		local name = LuaZhenZhaiYuMgr.m_SelectedFishButtons[row+1]
		if name ==  LocalString.GetString("培养") then
			LuaZhenZhaiYuMgr.OpenBringUpWnd(self.m_TemplateId)
		elseif name ==  LocalString.GetString("喂食") then
			LuaZhenZhaiYuMgr.OpenFeedWnd(self.m_TemplateId)
		elseif name ==  LocalString.GetString("取消关联") then
			self:CancelAttachFabao()
		elseif name ==  LocalString.GetString("收至包裹") then
			self:PutBackToBag()
		end
	end)
	self.TableView:ReloadData(false,false)
	--tableBg
	local gridHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Grid.transform).size.y
	local height = 25 + gridHeight
	self.TableBg.height = height
end


function LuaZhenZhaiYuDetailWnd:InitItem(item,row)
	local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
	label.text = LuaZhenZhaiYuMgr.m_SelectedFishButtons[row+1]

	if label.text == LocalString.GetString("取消关联") then
		if not LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.attachedFaBaoId or LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.attachedFaBaoId == 0 then
			item.gameObject:SetActive(false)
		else
			item.gameObject:SetActive(true)
		end
	end
end

--@region UIEvent

function LuaZhenZhaiYuDetailWnd:OnPickUpBtnClick()
	local fishmapId = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.fishmapId
	Gac2Gas.GetbackZhengzhaiFish(fishmapId)
end

--@endregion UIEvent

function LuaZhenZhaiYuDetailWnd:OnDestroy()
	g_ScriptEvent:BroadcastInLua("UnSelectZhenZhaiYuInWnd")
end

function LuaZhenZhaiYuDetailWnd:CancelAttachFabao()
	local fishmapId = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.fishmapId
	Gac2Gas.AttachZhengzhaiFishToFabao(fishmapId,0)--fishId,fabaoId
end

function LuaZhenZhaiYuDetailWnd:PutBackToBag()
	local fishmapId = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.fishmapId
	Gac2Gas.GetbackZhengzhaiFish(fishmapId)--fishTempId
end
