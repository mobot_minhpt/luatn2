local GameObject = import "UnityEngine.GameObject"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local UISlider = import "UISlider"

LuaOffWorldPlay2TopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPlay2TopRightWnd, "rightBtn", "rightBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay2TopRightWnd, "totalNum", "totalNum", UILabel)
RegistChildComponent(LuaOffWorldPlay2TopRightWnd, "HPItem", "HPItem", UISprite)
RegistChildComponent(LuaOffWorldPlay2TopRightWnd, "text", "text", UILabel)
RegistChildComponent(LuaOffWorldPlay2TopRightWnd, "num", "num", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaOffWorldPlay2TopRightWnd, "percentMaxLength")
RegistClassMember(LuaOffWorldPlay2TopRightWnd, "sliderSc")

function LuaOffWorldPlay2TopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateOffWorldPlay2Data", self, "UpdateTop")
	g_ScriptEvent:AddListener("UpdateOffWorldPlay2StoneData", self, "UpdateStone")
end

function LuaOffWorldPlay2TopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlay2Data", self, "UpdateTop")
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlay2StoneData", self, "UpdateStone")
end

function LuaOffWorldPlay2TopRightWnd:UpdateStone()
  self.totalNum.text = LuaOffWorldPassMgr.Play2Stome or '0'
end

function LuaOffWorldPlay2TopRightWnd:UpdateTop()
  if LuaOffWorldPassMgr.Play2StageData then
    if LuaOffWorldPassMgr.Play2StageData[1] == 1 then
      self.text.text = LocalString.GetString('破坏冥婚仪式')
      self.HPItem.spriteName = 'common_hpandmp_max'
    elseif LuaOffWorldPassMgr.Play2StageData[1] == 2 then
      self.text.text = LocalString.GetString('围剿鬼司仪')
      self.HPItem.spriteName = 'common_hpandmp_hp'
      self.num.gameObject:SetActive(false)
    end

    --self.num.text = LuaOffWorldPassMgr.Play2StageData[2] .. '%'

    local hp = LuaOffWorldPassMgr.Play2StageData[2]
    if hp then
      local percent = hp/100
      --self.HPItem.width = self.percentMaxLength * percent
			self.sliderSc.value = percent
      self.num.text = hp..'%'
    end
  end
end

function LuaOffWorldPlay2TopRightWnd:Init()
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

  self.percentMaxLength = 338 --self.HPItem.width
	self.sliderSc = self.HPItem.transform.parent:GetComponent(typeof(UISlider))

  self.num.text = '100%'
  self.text.text = LocalString.GetString('破坏冥婚仪式')
  self:UpdateStone()
	self:UpdateTop()
end

--@region UIEvent

--@endregion UIEvent
