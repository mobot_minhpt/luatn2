local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTaskMgr = import "L10.Game.CTaskMgr"

LuaNanDuFanHuaLordWayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "FameWidget", "FameWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "ShangShiJieKouFame", "ShangShiJieKouFame", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "ShiLiuLouFame", "ShiLiuLouFame", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "SanFaSiFame", "SanFaSiFame", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "ShiLiChangJieFame", "ShiLiChangJieFame", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "WuYiXiangFame", "WuYiXiangFame", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "PropertyWidget", "PropertyWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "DaoDePropertyTemplate", "DaoDePropertyTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "ZhiHuiPropertyTemplate", "ZhiHuiPropertyTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "XinQingPropertyTemplate", "XinQingPropertyTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "TiZhiPropertyTemplate", "TiZhiPropertyTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "ShouYiPropertyTemplate", "ShouYiPropertyTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "YiZhangPropertyTemplate", "YiZhangPropertyTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "JoinDaFuWenButton", "JoinDaFuWenButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "Money", "Money", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "MoneyIcon", "MoneyIcon", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "MoneyLabel", "MoneyLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "MoneyAddBtn", "MoneyAddBtn", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "ScheduleWidget", "ScheduleWidget", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "ChenJian", "ChenJian", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "Wuke", "Wuke", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordWayWnd, "Mushi", "Mushi", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaLordWayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.fameObjList = {self.WuYiXiangFame, self.ShiLiuLouFame, self.ShiLiChangJieFame, self.ShangShiJieKouFame, self.SanFaSiFame}
    self.fameNameList = {"WuYiXiangFameIndex", "ShiLiuLouFameIndex", "ShiLiQingHuaiFameIndex", "ShangShiJieKouFameIndex", "SanFaSiFameIndex"}
    for i = 1, #self.fameObjList do 
        self.fameObjList[i].transform:Find("Label"):GetComponent(typeof(UILabel)).text = LuaYiRenSiMgr.LordFameName[i]
    end
    
    self.propertyList = {self.DaoDePropertyTemplate, self.ZhiHuiPropertyTemplate, self.TiZhiPropertyTemplate, self.XinQingPropertyTemplate, self.ShouYiPropertyTemplate, self.YiZhangPropertyTemplate}
    self.propertyNameList = {"DaoDeIndex", "ZhiHuiIndex", "TiZhiIndex", "XinQingIndex", "ShouYiIndex", "YiZhangIndex"}
    self:InitUIEvent()

    g_ScriptEvent:AddListener("NanDuFanHua_Lord_AddSchedule", self, "OnUpdateLordSchedule")
    g_ScriptEvent:AddListener("NanDuLordSelectScheduleChoiceSuccess", self, "OnNanDuLordSelectScheduleChoiceSuccess")
    g_ScriptEvent:AddListener("UpdateNanDuFanHuaLordProperty", self, "OnUpdateNanDuFanHuaLordProperty")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
end

function LuaNanDuFanHuaLordWayWnd:Init()

end

function LuaNanDuFanHuaLordWayWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("NanDuFanHua_Lord_AddSchedule", self, "OnUpdateLordSchedule")
    g_ScriptEvent:RemoveListener("NanDuLordSelectScheduleChoiceSuccess", self, "OnNanDuLordSelectScheduleChoiceSuccess")
    g_ScriptEvent:RemoveListener("UpdateNanDuFanHuaLordProperty", self, "OnUpdateNanDuFanHuaLordProperty")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaLordWayWnd:InitUIEvent()
    local scheduleObjList = {self.ChenJian, self.Wuke, self.Mushi}
    for i = 1, 3 do
        local scheduleObj = scheduleObjList[i]
        UIEventListener.Get(scheduleObj.transform:Find("Border/AddScheduleButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            --need finish previous schedule
            self.scheduleData = {}
            if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey) then
                local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey)
                if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                    local data = g_MessagePack.unpack(playData.Data.Data)
                    self.scheduleData = data
                end
            end
            local passCheck = true
            for t = 1, i-1 do
                if self.scheduleData[t] then
                else
                    passCheck = false
                end
            end
            if passCheck then
                LuaNanDuFanHuaLordPlanScheduleWnd.PeriodId = i
                CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordPlanScheduleWnd)
            else
                g_MessageMgr:ShowMessage("NANDULORD_NEED_FINISH_PREVIOUS_SCHEDULE")
            end
        end)
    end

    UIEventListener.Get(self.JoinDaFuWenButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.DaFuWongEnterWnd)
    end)

    UIEventListener.Get(self.MoneyAddBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaExchangeMoneyWnd)
    end)

    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("NANDUFANHUA_LORD_SCHEDULE_TIPS")
    end)
    
    UIEventListener.Get(self.transform:Find("OweMoney").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("NANDUFANHUA_LORD_DEBT")
    end)
end

function LuaNanDuFanHuaLordWayWnd:OnEnable()
    self:RefreshUIAndTempPlayData()
end

function LuaNanDuFanHuaLordWayWnd:OnDisable()
end

function LuaNanDuFanHuaLordWayWnd:OnUpdateNanDuFanHuaLordProperty()
    self:RefreshUIAndTempPlayData()
end

function LuaNanDuFanHuaLordWayWnd:OnUpdatePlayProp()
    self:RefreshUIAndTempPlayData()
end

function LuaNanDuFanHuaLordWayWnd:OnNanDuLordSelectScheduleChoiceSuccess()
    self:RefreshUIAndTempPlayData()
end

function LuaNanDuFanHuaLordWayWnd:RefreshUIAndTempPlayData()
    self.lordData = {}
    self.scheduleData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.lordData = data[1]
        end
    end
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.scheduleData = data
        end
    end
    
    self.MoneyLabel.text = self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
    local debt = self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] or 0
    self.transform:Find("OweMoney/MoneyLabel"):GetComponent(typeof(UILabel)).text = debt
    self.transform:Find("OweMoney/MoneyLabel"):GetComponent(typeof(UILabel)).color = debt == 0 and NGUIText.ParseColor24("FFFFFF", 0) or NGUIText.ParseColor24("FF0200", 0)
    self:RefreshUI()
end

function LuaNanDuFanHuaLordWayWnd:OnUpdateLordSchedule()
    self.scheduleData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.scheduleData = data
        end
    end
    local scheduleObjList = {self.ChenJian, self.Wuke, self.Mushi}
    for i = 1, 3 do
        local scheduleInfo = self.scheduleData[i]
        local scheduleObj = scheduleObjList[i]
        if scheduleInfo then
            --有数据
            scheduleObj.transform:Find("Border/AddScheduleButton").gameObject:SetActive(false)
            local scheduleInfoObj = scheduleObj.transform:Find("Border/LordScheduleInfo")

            scheduleInfoObj.gameObject:SetActive(true)
            local scheduleId = scheduleInfo[1]
            local lordScheduleCfgData = NanDuFanHua_LordTask.GetData(scheduleId)
            scheduleInfoObj:Find("InfoTitle/Label"):GetComponent(typeof(UILabel)).text = lordScheduleCfgData.Schedule
            if scheduleInfo[2] == 1 then
                scheduleInfoObj:Find("InfoTitle/Info1").gameObject:SetActive(false)
                scheduleInfoObj:Find("InfoTitle/Info2").gameObject:SetActive(false)

                scheduleInfoObj:Find("FinishButton").gameObject:SetActive(false)
                scheduleInfoObj:Find("GivenUpButton").gameObject:SetActive(false)
                scheduleInfoObj:Find("GoToButton").gameObject:SetActive(scheduleInfo[3] == 2)
                scheduleInfoObj:Find("InScheduleButton").gameObject:SetActive(scheduleInfo[3] == 1)

                UIEventListener.Get(scheduleInfoObj:Find("GoToButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                    self:ShowChoice(i, scheduleId, scheduleInfo[5])
                end)
                UIEventListener.Get(scheduleInfoObj:Find("InScheduleButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                    if lordScheduleCfgData.DuringSpecial == "ShowTradingWnd" then
                        LuaYiRenSiMgr:NanDuLordOpenWnd(2)
                    else
                        local taskId = scheduleInfo[6] and scheduleInfo[6] or 0
                        if taskId ~= 0 then
                            local taskName = Task_Task.GetData(taskId).Display
                            g_MessageMgr:ShowMessage("NANDUFANHUA_LORD_GETTASK", taskName)
                        end
                    end
                end)

                if scheduleInfo[3] == 1 then
                    local taskId = scheduleInfo[6]
                    if taskId then
                        local taskInfo = CTaskMgr.Inst:GetCurrentTask(taskId)
                        if taskInfo then
                            --do nothing
                        else
                            Gac2Gas.NanDuLordGiveUpSchedule(scheduleId)
                        end
                    end
                end
            elseif scheduleInfo[2] == 2 then
                scheduleInfoObj:Find("FinishButton").gameObject:SetActive(true)
                scheduleInfoObj:Find("GoToButton").gameObject:SetActive(false)
                scheduleInfoObj:Find("InScheduleButton").gameObject:SetActive(false)
                scheduleInfoObj:Find("InfoTitle/Info1").gameObject:SetActive(false)
                scheduleInfoObj:Find("InfoTitle/Info2").gameObject:SetActive(false)
                scheduleInfoObj:Find("GivenUpButton").gameObject:SetActive(false)

                local fameInfoObjList = {scheduleInfoObj:Find("InfoTitle/Info1").gameObject, scheduleInfoObj:Find("InfoTitle/Info2").gameObject}
                local fameInfo = scheduleInfo[4]
                if fameInfo then
                    for subId = 1, #fameInfoObjList do
                        local fameInfoObj = fameInfoObjList[subId]
                        if fameInfo[2*subId-1] then
                            local fameId = fameInfo[2*subId-1]
                            local fameValue = fameInfo[2*subId]
                            fameInfoObj:SetActive(true)
                            fameInfoObj.transform:Find("FamePlace"):GetComponent(typeof(UILabel)).text = LuaYiRenSiMgr.LordFameName[fameId]
                            if subId == 1 then
                                fameInfoObj.transform:Find("FameValue"):GetComponent(typeof(UILabel)).text = (fameValue > 0 and "+" or "") .. fameValue
                            else
                                fameInfoObj.transform:Find("FameValue"):GetComponent(typeof(UILabel)).text = "-" .. fameValue
                            end
                        else
                            fameInfoObj:SetActive(false)
                        end
                    end
                end
            elseif scheduleInfo[2] == 3 then
                scheduleInfoObj:Find("GivenUpButton").gameObject:SetActive(true)
                scheduleInfoObj:Find("FinishButton").gameObject:SetActive(false)
                scheduleInfoObj:Find("GoToButton").gameObject:SetActive(false)
                scheduleInfoObj:Find("InScheduleButton").gameObject:SetActive(false)
                scheduleInfoObj:Find("InfoTitle/Info1").gameObject:SetActive(false)
                scheduleInfoObj:Find("InfoTitle/Info2").gameObject:SetActive(false)
            end
        else
            --空数据
            scheduleObj.transform:Find("Border/AddScheduleButton").gameObject:SetActive(true)
            scheduleObj.transform:Find("Border/LordScheduleInfo").gameObject:SetActive(false)
        end
    end
end

function LuaNanDuFanHuaLordWayWnd:RefreshUI()
    for i = 1, #self.fameObjList do
        local fameValue = self.lordData[LuaYiRenSiMgr.LordPropertyKey[self.fameNameList[i]]] and self.lordData[LuaYiRenSiMgr.LordPropertyKey[self.fameNameList[i]]] or 0
        self.fameObjList[i].transform:Find("Label/ValueLabel"):GetComponent(typeof(UILabel)).text = fameValue
    end

    local fashionData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            fashionData = data[5]
        end
    end
    local fashion1Id = fashionData[1] and fashionData[1] or 0
    local fashionAddValue = {}
    if fashion1Id ~= 0 then
        self:ParseFashionAttrChange(fashionAddValue, NanDuFanHua_LordFashion.GetData(fashion1Id).AttrChange)
    end

    local fashion2Id = fashionData[2] and fashionData[2] or 0
    if fashion2Id ~= 0 then
        self:ParseFashionAttrChange(fashionAddValue, NanDuFanHua_LordFashion.GetData(fashion2Id).AttrChange)
    end

    local zuoqiId = fashionData[3] and fashionData[3] or 0
    if zuoqiId ~= 0 then
        self:ParseFashionAttrChange(fashionAddValue, NanDuFanHua_LordZuoQi.GetData(zuoqiId).AttrChange)
    end

    for i = 1, #self.propertyList do
        local propertyValue = self.lordData[LuaYiRenSiMgr.LordPropertyKey[self.propertyNameList[i]]] and self.lordData[LuaYiRenSiMgr.LordPropertyKey[self.propertyNameList[i]]] or 0
        self.propertyList[i].transform:Find("Label/ValueLabel"):GetComponent(typeof(UILabel)).text = propertyValue + (fashionAddValue[i] and fashionAddValue[i] or 0)
    end

    self:OnUpdateLordSchedule()
end 

function LuaNanDuFanHuaLordWayWnd:ParseFashionAttrChange(tbl, parseText)
    local attrList = g_LuaUtil:StrSplit(parseText, ";")
    for i = 1, #attrList-1 do
        local subKeyValuePair = g_LuaUtil:StrSplit(attrList[i], ",")
        tbl[tonumber(subKeyValuePair[1])] = (tbl[tonumber(subKeyValuePair[1])] and tbl[tonumber(subKeyValuePair[1])] or 0) + tonumber(subKeyValuePair[2])
    end
end

function LuaNanDuFanHuaLordWayWnd:ShowChoice(periodId, scheduleId, lordQuestionId)
    LuaNanDuFanHuaDaTiWnd.m_QuestionId = lordQuestionId
    LuaNanDuFanHuaDaTiWnd.m_PeriodId = periodId
    LuaNanDuFanHuaDaTiWnd.m_ScheduleId = scheduleId

    CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaDaTiWnd)
end 