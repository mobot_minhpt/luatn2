local GameObject = import "UnityEngine.GameObject"
local CCJBRenderObject=import "L10.Game.CCJBRenderObject"
-- local Object = import "UnityEngine.Object"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CCJBMgr = import "L10.Game.CCJBMgr"
local TweenPosition=import "TweenPosition"
local TweenScale=import "TweenScale"


CLuaChuanjiabaoShelf = class()
RegistClassMember(CLuaChuanjiabaoShelf,"model")
RegistClassMember(CLuaChuanjiabaoShelf,"addButton")
RegistClassMember(CLuaChuanjiabaoShelf,"type")
RegistClassMember(CLuaChuanjiabaoShelf,"tPos")
RegistClassMember(CLuaChuanjiabaoShelf,"tScale")
RegistClassMember(CLuaChuanjiabaoShelf,"height")
RegistClassMember(CLuaChuanjiabaoShelf,"OnCheckChuanjiabaoInfo")
RegistClassMember(CLuaChuanjiabaoShelf,"_showNode")
RegistClassMember(CLuaChuanjiabaoShelf,"chuanjiabaoId")

function CLuaChuanjiabaoShelf:Awake()
    self.model = self.transform:Find("Model"):GetComponent(typeof(UITexture))
    self.addButton = self.transform:Find("AddButton").gameObject
    self.type = nil
    self.tPos = self.transform:GetComponent(typeof(TweenPosition))
    self.tScale = self.transform:GetComponent(typeof(TweenScale))
    self.height = 360
    self.OnCheckChuanjiabaoInfo = nil
end

function CLuaChuanjiabaoShelf:GetShowData()
    if self._showNode then
        return self._showNode:GetComponent(typeof(CCJBRenderObject)).showData
    end
    return nil
end

function CLuaChuanjiabaoShelf:Init(id)
    self.chuanjiabaoId = id
    self.addButton:SetActive(id==nil or id=="")
    self:InitModel()
end

function CLuaChuanjiabaoShelf:OnDestroy()
    if self._showNode then
        GameObject.Destroy(self._showNode)
        self._showNode=nil
    end
end
function CLuaChuanjiabaoShelf:AddListener()
    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnAddButtonClick(go)
    end)
end
function CLuaChuanjiabaoShelf:RemoveListener()
    UIEventListener.Get(self.gameObject).onClick = nil
end

function CLuaChuanjiabaoShelf:CleanShowNode( )
    self.chuanjiabaoId = nil
    self.addButton:SetActive(true)
    if self._showNode ~= nil then
        GameObject.Destroy(self._showNode)
        self._showNode=nil
    end
end
function CLuaChuanjiabaoShelf:InitModel( )
    if self._showNode ~= nil then
        GameObject.Destroy(self._showNode)
        self._showNode=nil
    end

    if self.chuanjiabaoId==nil or self.chuanjiabaoId=="" then
        return
    end

    self._showNode = CCJBMgr.Inst:LoadCJBRenderModel(self.model.gameObject, self.chuanjiabaoId, self.height, self.height, nil, true)
end
function CLuaChuanjiabaoShelf:OnAddButtonClick( go) 
    if self.chuanjiabaoId==nil or self.chuanjiabaoId=="" then
        CClientChuanJiaBaoMgr.Inst.currentSelectShelfIndex = self.type
        CUIManager.ShowUI(CUIResources.ChuanjiabaoSelectWnd)
    else
        if self.OnCheckChuanjiabaoInfo ~= nil then
            self.OnCheckChuanjiabaoInfo(self.type)
        end
    end
end

