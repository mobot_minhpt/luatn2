require("3rdParty/ScriptEvent")

local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local Vector3 = import "UnityEngine.Vector3"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CUIFx = import "L10.UI.CUIFx"
local UIRoot = import "UIRoot"
--local ZhuJueJuQing_Pintu = import "L10.Game.ZhuJueJuQing_Pintu"
local CUITexture = import "L10.UI.CUITexture"

LuaJuQingPinTuWnd=class()
RegistClassMember(LuaJuQingPinTuWnd,"Shard1")
RegistClassMember(LuaJuQingPinTuWnd,"Shard2")
RegistClassMember(LuaJuQingPinTuWnd,"Shard3")
RegistClassMember(LuaJuQingPinTuWnd,"Shard4")
RegistClassMember(LuaJuQingPinTuWnd,"Shard5")
RegistClassMember(LuaJuQingPinTuWnd,"Shard6")
RegistClassMember(LuaJuQingPinTuWnd,"TargetParent")
RegistClassMember(LuaJuQingPinTuWnd,"HintLabel")
RegistClassMember(LuaJuQingPinTuWnd,"MarkLabel")
RegistClassMember(LuaJuQingPinTuWnd,"FXRoot")
RegistClassMember(LuaJuQingPinTuWnd,"CloseButton")
RegistClassMember(LuaJuQingPinTuWnd,"BGArea")
RegistClassMember(LuaJuQingPinTuWnd,"BGShadow")

RegistClassMember(LuaJuQingPinTuWnd,"MarkColor")
RegistClassMember(LuaJuQingPinTuWnd,"Offset1")
RegistClassMember(LuaJuQingPinTuWnd,"Offset2")
RegistClassMember(LuaJuQingPinTuWnd,"Offset3")
RegistClassMember(LuaJuQingPinTuWnd,"Offset4")
RegistClassMember(LuaJuQingPinTuWnd,"Offset5")
RegistClassMember(LuaJuQingPinTuWnd,"Offset6")

RegistClassMember(LuaJuQingPinTuWnd,"Finished")
RegistClassMember(LuaJuQingPinTuWnd,"ori")
RegistClassMember(LuaJuQingPinTuWnd,"oriScale")
RegistClassMember(LuaJuQingPinTuWnd,"offset")


function LuaJuQingPinTuWnd:Awake()
	self.Finished = false
	self.ori={}
	self.oriScale={}
	self.offset={}
	self:UpdatePinTuPic()
end

function LuaJuQingPinTuWnd:UpdatePinTuPic()
	local taskId = CZhuJueJuQingMgr.Instance.MihanTaskID
	local pintuInfo = nil
	ZhuJueJuQing_Pintu.ForeachKey(function (key) 
        local pintu = ZhuJueJuQing_Pintu.GetData(key)
        if pintu.TaskID == taskId then
        	pintuInfo = pintu
        end
    end)

	if not pintuInfo then
		CUIManager.CloseUI(CLuaUIResources.JuQingPinTuWnd)
		return
	end
    self.Shard1.transform:GetComponent(typeof(CUITexture)):LoadMaterial(pintuInfo.Pic1)
    self.Shard2.transform:GetComponent(typeof(CUITexture)):LoadMaterial(pintuInfo.Pic2)
    self.Shard3.transform:GetComponent(typeof(CUITexture)):LoadMaterial(pintuInfo.Pic3)
    self.Shard4.transform:GetComponent(typeof(CUITexture)):LoadMaterial(pintuInfo.Pic4)
    self.Shard5.transform:GetComponent(typeof(CUITexture)):LoadMaterial(pintuInfo.Pic5)
    self.Shard6.transform:GetComponent(typeof(CUITexture)):LoadMaterial(pintuInfo.Pic6)
    self.MarkLabel.transform:GetComponent(typeof(UILabel)).text = pintuInfo.Msg
end

function LuaJuQingPinTuWnd:SetShardName(shard, picName)
	if string.match(picName, "1") then
		shard.transform.name = "1"
	elseif string.match(picName, "2") then
		shard.transform.name = "2"
	elseif string.match(picName, "3") then
		shard.transform.name = "3"
	elseif string.match(picName, "4") then
		shard.transform.name = "4"
	elseif string.match(picName, "5") then
		shard.transform.name = "5"
	elseif string.match(picName, "6") then
		shard.transform.name = "6"
	end
end

function LuaJuQingPinTuWnd:OnEnable()
	self.ori={}
	self.oriScale={}
	self.offset={}
	self.ori[self.Shard1] = self.Shard1.transform.parent
	self.ori[self.Shard2] = self.Shard2.transform.parent
	self.ori[self.Shard3] = self.Shard3.transform.parent
	self.ori[self.Shard4] = self.Shard4.transform.parent
	self.ori[self.Shard5] = self.Shard5.transform.parent
	self.ori[self.Shard6] = self.Shard6.transform.parent
	
	self.offset[self.Shard1] = self.Offset1
	self.offset[self.Shard2] = self.Offset2
	self.offset[self.Shard3] = self.Offset3
	self.offset[self.Shard4] = self.Offset4
	self.offset[self.Shard5] = self.Offset5
	self.offset[self.Shard6] = self.Offset6
	
	self.oriScale[self.Shard1] = self.Shard1.transform.localScale
	self.oriScale[self.Shard2] = self.Shard2.transform.localScale
	self.oriScale[self.Shard3] = self.Shard3.transform.localScale
	self.oriScale[self.Shard4] = self.Shard4.transform.localScale
	self.oriScale[self.Shard5] = self.Shard5.transform.localScale
	self.oriScale[self.Shard6] = self.Shard6.transform.localScale
	local onDrag = function(go,delta)
		if self.Finished then 
			return 
		end
		local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject);
		local trans = go.transform
		local pos = trans.localPosition
		pos.x = pos.x + delta.x*scale
		pos.y = pos.y + delta.y*scale
		trans.localPosition = pos
	end
	local onPress = function(go,flag)
		if self.Finished then 
			return 
		end
		if not flag then
			local currentPos = UICamera.currentTouch.pos
			local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
			local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
			for i =0,hits.Length -1 do
				if hits[i].collider.gameObject.transform.parent == self.TargetParent then
					go.transform.parent = hits[i].collider.gameObject.transform
					local p = Vector3.zero
					local z = self.offset[go]
					p.x = p.x + z.x
					p.y = p.y + z.y
					go.transform.localPosition = p
					self:judgeFinish()
					self.HintLabel:SetActive(false)
					return
				end
			end
			go.transform.parent = self.ori[go]
			go.transform.localPosition = Vector3.zero
			go.transform.localScale = self.oriScale[go]
		else
			go.transform.localScale = Vector3.one
		end
	end
	local onCloseClick = function(go)
		CUIManager.CloseUI(CLuaUIResources.JuQingPinTuWnd)
	end
	CommonDefs.AddOnClickListener(self.BGShadow,DelegateFactory.Action_GameObject(onCloseClick),false)

	CommonDefs.AddOnDragListener(self.Shard1,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard2,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard3,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard4,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard5,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard6,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnPressListener(self.Shard1,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard2,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard3,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard4,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard5,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard6,DelegateFactory.Action_GameObject_bool(onPress),false)
	
end
function LuaJuQingPinTuWnd:OnDisable()
	
	CUICommonDef.KillTween(self.gameObject:GetHashCode())
end
function LuaJuQingPinTuWnd:judgeFinish()
	for i = 0,self.TargetParent.childCount-1 do
		local trans = self.TargetParent:GetChild(i)
		if trans.childCount == 0 then
			return 
		end
		if trans:GetChild(0).name ~= trans.name then
			return 
		end
	end
	self.Finished=true
	self:showMarkAndAnimation()
	local fx = self.FXRoot:GetComponent(typeof(CUIFx))
	fx:LoadFx("fx/ui/prefab/ui_pintu.prefab")
	Gac2Gas.FinishTaskPintuPuzzle(CZhuJueJuQingMgr.Instance.MihanTaskID)
end
function LuaJuQingPinTuWnd:showMarkAndAnimation()
	CUICommonDef.KillTween(self.gameObject:GetHashCode())
	local targetTrans = self.TargetParent.transform
	local targetP = targetTrans.localPosition
	local onUpdate = function(p)
		local color = self.MarkColor
		color.a = p
		self.MarkLabel.color = color
		local z =targetP
		z.x = (1-p)*targetP.x
		z.y = (1-p)*targetP.y
		targetTrans.localPosition = z 
		local s =Vector3.one
		s.x = 1 + 0.2 * p
		s.y = 1 + 0.2 * p
		targetTrans.localScale = s
	end
	local onComplete = function()
		self.BGArea:SetActive(false)
		self.BGShadow:SetActive(true)
		CUICommonDef.Tween(1,DelegateFactory.Action_float(onUpdate),nil,1,self.gameObject:GetHashCode())
	end
	local pintuUpdate = function(p)
		for i = 0,self.TargetParent.childCount-1 do
			local go = self.TargetParent:GetChild(i):GetChild(0).gameObject
			local trans = go.transform
			local z = self.offset[go]
			z.x = z.x*(1-p)
			z.y = z.y*(1-p)
			trans.localPosition = z
		end
	end
	
	CUICommonDef.Tween(3,DelegateFactory.Action_float(pintuUpdate),DelegateFactory.Action(onComplete),1,self.gameObject:GetHashCode())
end

return LuaJuQingPinTuWnd
