local QnTableView = import "L10.UI.QnTableView"
local UICamera = import "UICamera"
local CChatLinkMgr = import "CChatLinkMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaZongMenHistoryWnd = class()

RegistChildComponent(LuaZongMenHistoryWnd,"m_QnTableView","QnTableView", QnTableView)

RegistClassMember(LuaZongMenHistoryWnd,"m_TextList")

function LuaZongMenHistoryWnd:Init( )
    self.m_TextList = {}
    self.m_QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_TextList
        end,
        function(item, index)
            self:InitItem(item,index)
        end)
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId,"history",0)
    end
end

function LuaZongMenHistoryWnd:InitItem(item,row)
    item.m_Label.text = self.m_TextList[row+1]
    UIEventListener.Get(item.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local url = item.m_Label:GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)
end


function LuaZongMenHistoryWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSectHistoryInfoResult", self, "OnSectHistoryInfoResult")
end

function LuaZongMenHistoryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSectHistoryInfoResult", self, "OnSectHistoryInfoResult")
end


function LuaZongMenHistoryWnd:OnSectHistoryInfoResult(data)
    local count = #data
    table.sort(data,function(a, b)

        if a.GenerateTime > b.GenerateTime then
            return true
        end

        if a.GenerateTime < b.GenerateTime then
            return false
        end

        if a.GenerateTime == b.GenerateTime  then
            
            if a.Age > b.Age then
                return true
            end

            if a.Age < b.Age then
                return false
            end

            -- 相同的值一定返回false
            if a.Age == b.Age then
                return false
            end
        end
    end)

    for i = 1, count do
        local info = data[i]

        local data = Menpai_History.GetData(info.DesignId)
        local event = data and data.Content or ""

        
        local isSupport = data ~= nil

        local argList = info.MessageParam.Data and MsgPackImpl.unpack(info.MessageParam.Data) or {}
        local msg

        if isSupport then
            if not argList.Count or argList.Count == 0 then
                msg = event
            else
                local argTblLua = {}
                for i= 0, argList.Count-1 do
                    table.insert(argTblLua, argList[i])
                end
                msg = SafeStringFormat3(event, unpack(argTblLua))
            end
        else
            msg = event
        end

        local date = CServerTimeMgr.ConvertTimeStampToZone8Time(info.GenerateTime)
        local content = ToStringWrap(date, "yyyy-MM-dd") .. "       " .. msg
        table.insert( self.m_TextList, CChatLinkMgr.TranslateToNGUIText(content, false) )
    end
    self.m_QnTableView:ReloadData(false, false)
end