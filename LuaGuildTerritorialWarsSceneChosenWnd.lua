local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISlider = import "UISlider"

LuaGuildTerritorialWarsSceneChosenWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsSceneChosenWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsSceneChosenWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsSceneChosenWnd, "BottomLabel", "BottomLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsSceneChosenWnd, "m_ObjList")

function LuaGuildTerritorialWarsSceneChosenWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsSceneChosenWnd:Init()
    self.ItemTemplate.gameObject:SetActive(false)
    self.m_ObjList = {}
    for i = 1, 3 do
        local obj = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate.gameObject)
        obj.gameObject:SetActive(true)
        self:InitItem(obj,i)
        table.insert(self.m_ObjList, obj)
    end
    self.BottomLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsSceneChosenWnd_BottomText")
    Gac2Gas.RequestTerritoryWarScenePlayerCount()
end

function LuaGuildTerritorialWarsSceneChosenWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncTerritoryWarPlayProgress", self, "OnSyncTerritoryWarPlayProgress")
end

function LuaGuildTerritorialWarsSceneChosenWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncTerritoryWarPlayProgress", self, "OnSyncTerritoryWarPlayProgress")
end

function LuaGuildTerritorialWarsSceneChosenWnd:OnSyncTerritoryWarPlayProgress()
    for i = 1, 3 do
        local obj = self.m_ObjList[i]
        self:InitItem(obj,i)
    end
end

function LuaGuildTerritorialWarsSceneChosenWnd:InitItem(obj,index)
    local label1 = obj.transform:Find("Label1"):GetComponent(typeof(UILabel))
    local slider = obj.transform:Find("Slider"):GetComponent(typeof(UISlider))
    local occupyProgressLabel = obj.transform:Find("OccupyProgressLabel"):GetComponent(typeof(UILabel))
    local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local curObj = obj.transform:Find("Cur").gameObject
    local button = obj.transform:Find("Button").gameObject

    local progressList = LuaGuildTerritorialWarsMgr.m_ProgressList
    local progress = progressList[index] and progressList[index] or progressList[0]
    if index == 1 then
        if progressList[0] == 100 then
            progress = 100
        end
    end
    local num = LuaGuildTerritorialWarsMgr.m_PlayerNumList[index] and LuaGuildTerritorialWarsMgr.m_PlayerNumList[index] or 0

    label1.text = SafeStringFormat3(LocalString.GetString("分线%d"), index)
    slider.value = progress / 100
    occupyProgressLabel.text = math.floor(progress) .."%"
    numLabel.text = SafeStringFormat3(LocalString.GetString("%d人"), num)
    curObj.gameObject:SetActive(LuaGuildTerritorialWarsMgr.m_SceneIdx == index)

    UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick(index)
	end)

    obj.gameObject:SetActive(progressList[index] or index == 1 or num > 0)
end

function LuaGuildTerritorialWarsSceneChosenWnd:OnButtonClick(index)
    Gac2Gas.RequestTerritoryWarTeleport(LuaGuildTerritorialWarsMgr.m_CurGridId, index)
end
