LuaCopySceneChosenMgr = class()

LuaCopySceneChosenMgr.SceneTemplateId = 0
LuaCopySceneChosenMgr.CandidateScenes = nil
LuaCopySceneChosenMgr.ShowRandomButton = false
LuaCopySceneChosenMgr.OpenWnd = nil
function LuaCopySceneChosenMgr.Show(sceneTemplateId, candidateScenes, showRandomButton)
    LuaCopySceneChosenMgr.SceneTemplateId = sceneTemplateId or 0
    LuaCopySceneChosenMgr.CandidateScenes = candidateScenes or {}
    LuaCopySceneChosenMgr.ShowRandomButton = showRandomButton or false
    CUIManager.ShowUI(LuaCopySceneChosenMgr.OpenWnd or "CopySceneChosenWnd")
    LuaCopySceneChosenMgr.OpenWnd = nil
end

function LuaCopySceneChosenMgr.OnSceneSelected(sceneId)
    Gac2Gas.ConfirmTeleportPublicCopy(LuaCopySceneChosenMgr.SceneTemplateId or 0, sceneId or "")
end

