local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local DelegateFactory = import "DelegateFactory"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"


LuaZhuoYaoMainWnd = class()

RegistChildComponent(LuaZhuoYaoMainWnd, "m_Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaZhuoYaoMainWnd, "m_ZhuoYaoWarehouseView", "ZhuoYaoWarehouseView", GameObject)
RegistChildComponent(LuaZhuoYaoMainWnd, "m_ZhuoYaoTuJianView", "ZhuoYaoTuJianView", GameObject)
RegistChildComponent(LuaZhuoYaoMainWnd, "m_ZhuoYaoCaptureView", "ZhuoYaoCaptureView",  CCommonLuaScript)
RegistChildComponent(LuaZhuoYaoMainWnd, "m_LevelLabel", "LevelLabel", GameObject)
RegistChildComponent(LuaZhuoYaoMainWnd, "m_Tab3AlertSprite", "Tab3AlertSprite", GameObject)

RegistClassMember(LuaZhuoYaoMainWnd, "m_Action")
RegistClassMember(LuaZhuoYaoMainWnd, "m_TabIndex")

function LuaZhuoYaoMainWnd:Awake()
    self.m_Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabsClick(index)
    end)
    self.m_Tabs:ChangeTab(LuaZhuoYaoMgr.m_ZhuoYaoMainWndTab, false)
    Gac2Gas.RequestYaoGuaiTuJianData()
end

function LuaZhuoYaoMainWnd:Init()
    local tab = self.m_Tabs:GetTabGoByIndex(1)
    tab.gameObject:SetActive(LuaZongMenMgr.m_OpenNewSystem)
    local uitable = self.m_Tabs.transform:GetComponent(typeof(UITable))
    uitable:Reposition()
end

function LuaZhuoYaoMainWnd:OnSyncYaoGuaiTuJian(list)
    self.m_Tab3AlertSprite:SetActive(false)
    for _,t in pairs(list) do
        local yaoguaiTempId = t.yaoguaiTempId
        local tuJianId = yaoguaiTempId
        local tujianData = ZhuoYao_TuJian.GetData(tuJianId)
        if not t.isRewarded and tujianData.Status == 0 then
            self.m_Tab3AlertSprite:SetActive(true)
        end
        local isFullQualit = t.maxQuality >= tujianData.QualityRange[1]
        local canGetFullQualitGift = isFullQualit and not t.isFullQualityRewarded
        if canGetFullQualitGift and tujianData.Status == 0 then
            self.m_Tab3AlertSprite:SetActive(true)
        end
    end
end

--@region UIEvent

function LuaZhuoYaoMainWnd:OnTabsClick(index)
    self.m_TabIndex = index
    self.m_ZhuoYaoWarehouseView:SetActive(index == 0)
    self.m_ZhuoYaoCaptureView.gameObject:SetActive(index == 1)
    self.m_ZhuoYaoTuJianView:SetActive(index == 2)

    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) and self.m_TabIndex ~= 1 then
		CGuideMgr.Inst:StartGuide(EnumGuideKey.NewZhuoYaoCaptureView, 2)
	end
end

--@endregion

function LuaZhuoYaoMainWnd:GetGuideGo(methodname)
    --print(methodname)
    if methodname == "GetLevelGo" then
        if self:GetTabIndex() == 1 then
            return self.m_ZhuoYaoCaptureView.transform:Find("LeftView/DefaultBottomView/LevelLabel").gameObject
        end
        return self.m_LevelLabel.gameObject
    elseif methodname == "GetCaptureTab" then
        local tab = self.m_Tabs:GetTabGoByIndex(1)
        return tab.gameObject
    elseif methodname == "GetCaptureBtn" then
        return self.m_ZhuoYaoCaptureView.transform:Find("RightBottomView/CatchButton").gameObject
    elseif methodname == "GetGuiJiangItem" then
        if self:GetTabIndex() == 1 then
            return self.m_ZhuoYaoCaptureView:GetGuiJiangItem()
        end
        return nil
    elseif methodname == "GetCatchItem1" then
        return self.m_ZhuoYaoCaptureView:GetCatchItem1()
    elseif methodname == "GetTuJianTab" then
        local tab = self.m_Tabs:GetTabGoByIndex(2)
        return tab.gameObject
    end
    return nil
end

function LuaZhuoYaoMainWnd:GetTabIndex()
    return self.m_Tabs.SelectedIndex
end

function LuaZhuoYaoMainWnd:OnEnable()
    self.m_Action = DelegateFactory.Action_uint(function(guideId)
        if guideId == 124 then
            --发个rpc通知任务完成
            Gac2Gas.FinishChuShiLianHuaGuideTask()
        end
    end)
    EventManager.AddListenerInternal(EnumEventType.GuideEnd, self.m_Action)
    g_ScriptEvent:AddListener("OnSyncYaoGuaiTuJian", self, "OnSyncYaoGuaiTuJian")
end

function LuaZhuoYaoMainWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.GuideEnd, self.m_Action)
    g_ScriptEvent:RemoveListener("OnSyncYaoGuaiTuJian", self, "OnSyncYaoGuaiTuJian")
    LuaZhuoYaoMgr.m_ZhuoYaoMainWndTab = 0
    if LuaZongMenMgr.m_OpenNewSystem then
        Gac2Gas.RequestYaoGuaiRedAlert()
    end
end
