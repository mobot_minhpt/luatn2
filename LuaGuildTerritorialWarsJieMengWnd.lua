local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local CChatHelper = import "L10.UI.CChatHelper"
local CButton = import "L10.UI.CButton"
local UITabBar = import "L10.UI.UITabBar"

LuaGuildTerritorialWarsJieMengWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "HeadLabel1", "HeadLabel1", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "JieMengButton", "JieMengButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "WaitLabel", "WaitLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "JieMengView", "JieMengView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "InvitationView", "InvitationView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "TableView2", "TableView2", QnTableView)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "Alert2Sprite", "Alert2Sprite", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "ClearButton", "ClearButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "RefuseButton", "RefuseButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "AcceptButton", "AcceptButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "NoneInvitationLabel", "NoneInvitationLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsJieMengWnd, "DiDuiButton", "DiDuiButton", CButton)
--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsJieMengWnd, "m_List")
RegistClassMember(LuaGuildTerritorialWarsJieMengWnd, "m_List2")
RegistClassMember(LuaGuildTerritorialWarsJieMengWnd, "m_SelectRow")
RegistClassMember(LuaGuildTerritorialWarsJieMengWnd, "m_SelectRow2")
RegistClassMember(LuaGuildTerritorialWarsJieMengWnd, "m_MyGuildId")
RegistClassMember(LuaGuildTerritorialWarsJieMengWnd, "m_MyFriendAccountNum")
RegistClassMember(LuaGuildTerritorialWarsJieMengWnd, "m_TabIndex")

function LuaGuildTerritorialWarsJieMengWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.JieMengButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJieMengButtonClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)

	UIEventListener.Get(self.ClearButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearButtonClick()
	end)

	UIEventListener.Get(self.RefuseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefuseButtonClick()
	end)

	UIEventListener.Get(self.AcceptButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAcceptButtonClick()
	end)

	UIEventListener.Get(self.DiDuiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDiDuiButtonClick()
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsJieMengWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendTerritoryWarRelationInfo", self, "OnSendTerritoryWarRelationInfo")
	g_ScriptEvent:AddListener("SendTerritoryWarAllianceApplyInfo", self, "OnAllianceApplyInfo")
end

function LuaGuildTerritorialWarsJieMengWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendTerritoryWarRelationInfo", self, "OnSendTerritoryWarRelationInfo")
	g_ScriptEvent:RemoveListener("SendTerritoryWarAllianceApplyInfo", self, "OnAllianceApplyInfo")
end

function LuaGuildTerritorialWarsJieMengWnd:OnSendTerritoryWarRelationInfo(myGuildId, hasAlliance, relationInfoList)
	self.m_List = relationInfoList
	self.m_MyGuildId = myGuildId
	self.TableView:ReloadData(true,false)
	for index, data in pairs(self.m_List) do
		if data.isMyAlliance then
			self.TableView:SetSelectRow(index - 1, true)
			break
		end
	end
	Gac2Gas.RequestTerritoryWarAllianceApplyInfo()
end

function LuaGuildTerritorialWarsJieMengWnd:OnAllianceApplyInfo()
	self.m_List2 = LuaGuildTerritorialWarsMgr.m_AllianceApplyInfo
	self.Alert2Sprite.gameObject:SetActive(#self.m_List2 > 0)
	self.ClearButton.Enabled = #self.m_List2 > 0
	self.NoneInvitationLabel.gameObject:SetActive(#self.m_List2 == 0)
	self.TableView2:ReloadData(true,false)
end

function LuaGuildTerritorialWarsJieMengWnd:Init()
	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTabChange(go, index)
        end
    )
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_List and #self.m_List or 0
        end,
        function(item,row)
            self:IniItem(item,row)
        end)
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(index)
        self:OnSelectAtRow(index)
    end)
	self.TableView2.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_List2 and #self.m_List2 or 0
        end,
        function(item,row)
            self:IniItem2(item,row)
        end)
	self.TableView2.OnSelectAtRow = DelegateFactory.Action_int(function(index)
        self:OnSelectAtRow2(index)
    end)
	self.Alert2Sprite.gameObject:SetActive(false)
	self.JieMengButton.gameObject:SetActive(true)
	self.CancelButton.gameObject:SetActive(false)
	self.BottomLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsJieMengWnd_BottomLabel")
	Gac2Gas.RequestTerritoryWarRelationData()
	--Gac2Gas.RequestTerritoryWarAllianceApplyInfo()
	self.Tabs:ChangeTab(0, false)
end

function LuaGuildTerritorialWarsJieMengWnd:IniItem(item,row)
	local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
	local serverNameLabel = item.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
	local leaderNameLabel = item.transform:Find("LeaderNameLabel"):GetComponent(typeof(UILabel))
	local powerLabel = item.transform:Find("PowerLabel"):GetComponent(typeof(UILabel))
	local occupyNumLabel = item.transform:Find("OccupyNumLabel"):GetComponent(typeof(UILabel))
	local mengYouNumLabel = item.transform:Find("MengYouNumLabel"):GetComponent(typeof(UILabel))
	local relationLabel = item.transform:Find("RelationLabel"):GetComponent(typeof(UILabel))
	local talkButton = item.transform:Find("LeaderNameLabel/TalkButton"):GetComponent(typeof(QnButton))
	local deletedObj = item.transform:Find("Deleted")

	UIEventListener.Get(talkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTalkButtonClick(row)
	end)

	local myGuildId = self.m_MyGuildId
	local data = self.m_List[row + 1]
	local guildName = data.guildName
	local serverName = data.serverName
	local guildLeaderName = data.leaderName
	local guild = data.guildId
	local power = data.equipScore
	local occupyNum = data.ownGridCount
	local friendAccountNum = data.allianceCount
	local isFriend = data.isMyAlliance
	local isWait = data.isWait
	local isSlave = data.isSlave
	local isMaster = data.isMaster
	local isDiDui = data.isDiDui

	local isMyGuild = myGuildId == guild
	if isMyGuild then
		self.m_MyFriendAccountNum = friendAccountNum
	end
	local labelColor = NGUIText.ParseColor24(isMyGuild and "00ff60" or "ffffff", 0)
	guildNameLabel.color = labelColor
	serverNameLabel.color = labelColor
	leaderNameLabel.color = labelColor
	powerLabel.color = labelColor
	occupyNumLabel.color = labelColor
	mengYouNumLabel.color = labelColor
	talkButton.Enabled = not isMyGuild
	guildNameLabel.text = guildName
	deletedObj.gameObject:SetActive(data.guildDeleted)
	serverNameLabel.text = serverName
	leaderNameLabel.text = guildLeaderName
	powerLabel.text = power
	occupyNumLabel.text = occupyNum
	mengYouNumLabel.text = friendAccountNum
	local relationText = ""
	if isFriend then
		relationText = LocalString.GetString("[00ff60]盟友")
	elseif isWait then
		relationText = LocalString.GetString("[fffe91]等待回应")
	elseif isMaster then
		relationText = LocalString.GetString("[ff5050]俘虏我帮")
	elseif isSlave then
		relationText = LocalString.GetString("[ff5050]我帮俘虏")
	elseif isDiDui then
		relationText = LocalString.GetString("[ff5050]敌对")
	end
	relationLabel.color = Color.white
	relationLabel.text = relationText
	relationLabel.gameObject:SetActive(not isMyGuild)
	item:GetComponent(typeof(UISprite)).spriteName = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
end

function LuaGuildTerritorialWarsJieMengWnd:IniItem2(item,row)
	local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
	local serverNameLabel = item.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
	local leaderNameLabel = item.transform:Find("LeaderNameLabel"):GetComponent(typeof(UILabel))
	local talkButton = item.transform:Find("LeaderNameLabel/TalkButton"):GetComponent(typeof(QnButton))
	local powerLabel = item.transform:Find("PowerLabel"):GetComponent(typeof(UILabel))
	local occupyNumLabel = item.transform:Find("OccupyNumLabel"):GetComponent(typeof(UILabel))
	local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
	local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))

	UIEventListener.Get(talkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTalkButtonClick(row)
	end)

	local data = self.m_List2[row + 1]
	local guildName = data.guildName
	local serverName = data.serverName
	local guildLeaderName = data.leaderName
	local power = data.equipScore
	local occupyNum = data.ownGridCount
	local applyTime = data.applyTime
	local time = CServerTimeMgr.ConvertTimeStampToZone8Time(applyTime)
    local timeString = ToStringWrap(time, "MM-dd HH:mm")
	local score = data.score

	guildNameLabel.text = guildName
	serverNameLabel.text = serverName
	leaderNameLabel.text = guildLeaderName
	powerLabel.text = power
	occupyNumLabel.text = occupyNum
	timeLabel.text = timeString
	scoreLabel.text = score

	item:GetComponent(typeof(UISprite)).spriteName = row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
end

function LuaGuildTerritorialWarsJieMengWnd:OnSelectAtRow(index)
	self.m_SelectRow = index
	local data = self.m_List[self.m_SelectRow + 1]
	local myGuildId = self.m_MyGuildId
	local guild = data.guildId
	local isMyGuild = myGuildId == guild
	local isFriend = data.isMyAlliance
	local isWait = data.isWait
	local isDiDui = data.isDiDui
	self.JieMengButton.gameObject:SetActive(not isFriend and not isWait)
	self.CancelButton.gameObject:SetActive((isFriend or isWait) and not isMyGuild)
	self.JieMengButton.Enabled = not isMyGuild
	self.DiDuiButton.Text = isDiDui and LocalString.GetString("取消敌对") or LocalString.GetString("设置敌对") 
end

function LuaGuildTerritorialWarsJieMengWnd:OnSelectAtRow2(index)
	self.m_SelectRow2 = index
end

--@region UIEvent
function LuaGuildTerritorialWarsJieMengWnd:OnTabChange(go, index)
	self.m_TabIndex = index
	self.JieMengView.gameObject:SetActive(index == 0)
	self.InvitationView.gameObject:SetActive(index == 1)
	if index == 1 then
		self.Alert2Sprite.gameObject:SetActive(false)
		self.TableView2:ReloadData(true,false)
	else
		self.TableView:ReloadData(true,false)
	end
end

function LuaGuildTerritorialWarsJieMengWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_ReadMe")
end

function LuaGuildTerritorialWarsJieMengWnd:OnJieMengButtonClick()
	if not self.m_SelectRow then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_Unselect_Confirm")
		return
	end
	local data = self.m_List[self.m_SelectRow + 1]
	local isSlave = data.isSlave
	local isMaster = data.isMaster
	if isSlave then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_IsSlave")
		return
	elseif isMaster then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_IsMaster")
		return
	elseif data.guildDeleted then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_GuildDeleted")
		return
	elseif data.isWait then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_IsWait")
		return
	elseif data.allianceCount > 0 then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_HasAlliance")
		return
	elseif self.m_MyFriendAccountNum and self.m_MyFriendAccountNum > 0 then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_HasFriend")
		return
	end
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsJieMengWnd_MakeAlliance_Confirm", data.guildName)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarMakeAlliance(data.guildId)
	end),nil,LocalString.GetString("邀请"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsJieMengWnd:OnCancelButtonClick()
	if not self.m_SelectRow then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_Unselect_Confirm")
		return
	end
	local data = self.m_List[self.m_SelectRow + 1]
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsJieMengWnd_BreakAlliance_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarBreakAlliance(data.guildId)
	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsJieMengWnd:OnTalkButtonClick(row)
	local data = self.m_TabIndex == 0 and self.m_List[row + 1] or self.m_List2[row + 1]
	CChatHelper.ShowFriendWnd(data.leaderId, data.leaderName)
end

function LuaGuildTerritorialWarsJieMengWnd:OnClearButtonClick()
	local msg = g_MessageMgr:FormatMessage("TerritoryWarRefuseAllAllianceApplication_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.TerritoryWarRefuseAllAllianceApplication()
	end),nil,LocalString.GetString("清空"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsJieMengWnd:OnRefuseButtonClick()
	if not self.m_SelectRow2 then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_Unselect_Confirm")
		return
	end
	local data = self.m_List2[self.m_SelectRow2 + 1]
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsJieMengWnd_Refuse_Alliance_Confirm",data.guildName)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarBreakAlliance(data.guildId)
	end),nil,LocalString.GetString("拒绝"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsJieMengWnd:OnAcceptButtonClick()
	if not self.m_SelectRow2 then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_Unselect_Confirm")
		return
	end
	if self.m_MyFriendAccountNum and self.m_MyFriendAccountNum > 0 then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_HasFriend")
		return
	end
	local data = self.m_List2[self.m_SelectRow2 + 1]
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsJieMengWnd_Accept_Alliance_Confirm",data.guildName)
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestTerritoryWarMakeAlliance(data.guildId)
	end),nil,LocalString.GetString("接受"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsJieMengWnd:OnDiDuiButtonClick()
	if not self.m_SelectRow then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsJieMengWnd_Unselect_Confirm")
		return
	end
	local data = self.m_List[self.m_SelectRow + 1]
	local isDiDui = false
	if data.isDiDui then
		isDiDui = true
	end
	local msg = g_MessageMgr:FormatMessage(isDiDui and "GuildTerritorialWarsJieMengWnd_NotSetGTWGuildEnemy_Confirm" or "GuildTerritorialWarsJieMengWnd_SetGTWGuildEnemy_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.SetGTWGuildEnemy(data.guildId,isDiDui)
	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end
--@endregion UIEvent
