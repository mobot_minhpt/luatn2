-- Auto Generated!!
local CMiniMapGuildLeagueFlagTemplate = import "L10.UI.CMiniMapGuildLeagueFlagTemplate"
local EnumGuildLeagueFlagType = import "L10.Game.EnumGuildLeagueFlagType"
local Time = import "UnityEngine.Time"
CMiniMapGuildLeagueFlagTemplate.m_Init_CS2LuaHook = function (this, flagType) 
    this.startTime = Time.realtimeSinceStartup
    repeat
        local default = flagType
        if default == EnumGuildLeagueFlagType.eAttack then
            this.sprite.spriteName = CMiniMapGuildLeagueFlagTemplate.Flag_Attack
            this.sprite:MakePixelPerfect()
            break
        elseif default == EnumGuildLeagueFlagType.eDefend then
            this.sprite.spriteName = CMiniMapGuildLeagueFlagTemplate.Flag_Defend
            this.sprite:MakePixelPerfect()
            this.sprite.width = this.sprite.width * 2
            this.sprite.height = this.sprite.height * 2
            break
        elseif default == EnumGuildLeagueFlagType.eEnemyGather then
            this.sprite.spriteName = CMiniMapGuildLeagueFlagTemplate.Flag_EnemyGather
            this.sprite:MakePixelPerfect()
            break
        elseif default == EnumGuildLeagueFlagType.eGather then
            this.sprite.spriteName = CMiniMapGuildLeagueFlagTemplate.Flag_Gather
            this.sprite:MakePixelPerfect()
            this.sprite.width = this.sprite.width * 2
            this.sprite.height = this.sprite.height * 2
            break
        elseif default == EnumGuildLeagueFlagType.eMine then
            this.sprite.spriteName = CMiniMapGuildLeagueFlagTemplate.Flag_Mine
            this.sprite:MakePixelPerfect()
            this.sprite.width = this.sprite.width * 2
            this.sprite.height = this.sprite.height * 2
            break
        elseif default == EnumGuildLeagueFlagType.eTwinkle then
            this.sprite.spriteName = CMiniMapGuildLeagueFlagTemplate.Flag_Twinkle
            this.sprite:MakePixelPerfect()
            this.sprite.width = this.sprite.width * 2
            this.sprite.height = this.sprite.height * 2
            break
        end
    until 1
end
