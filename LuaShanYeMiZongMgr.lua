LuaShanYeMiZongMgr = {}
--LuaShanYeMiZongMgr.s_ExtraTaskList = nil

--[[
function LuaShanYeMiZongMgr:Init()
    --g_ScriptEvent:AddListener("FinishTask", self, "OnFinishTask")
    LuaShanYeMiZongMgr.s_ExtraTaskList = ShuJia2022_Setting.GetData().FanWaiTaskList
end
LuaShanYeMiZongMgr:Init()

function LuaShanYeMiZongMgr:OnFinishTask(taskId)
    if taskId >= LuaShanYeMiZongMgr.s_ExtraTaskList[0] and taskId <= LuaShanYeMiZongMgr.s_ExtraTaskList[4] then
        LuaShanYeMiZongMainWnd.s_UnlockNewExtraTask = true
        CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongMainWnd)
    end
end
--]]

function LuaShanYeMiZongMgr:SendShanYeMiZongInfo(finalRewarded, extraTaskRewarded, xiansuoInfo, chapterInfo, joinedCaiXiang)
    local clue = {}
    local chapter = {}
    local guess = {}

    print("SendShanYeMiZongInfo", finalRewarded, extraTaskRewarded)
    local list = xiansuoInfo and g_MessagePack.unpack(xiansuoInfo)
    for i = 1, #list, 2 do
        local xiansuoId = list[i]
        local rewarded = list[i + 1]
        clue[xiansuoId] = rewarded
        --print("SendShanYeMiZongInfo xiansuo", xiansuoId, rewarded)
    end

    list = chapterInfo and g_MessagePack.unpack(chapterInfo)
    for i = 1, #list, 3 do
        local chapterId = list[i]
        local rewarded = list[i + 1]
        local xiansuoCount = list[i + 2]
        chapter[chapterId] = {rewarded = rewarded, clueCount = xiansuoCount}
        --print("SendShanYeMiZongInfo chapterInfo", chapterId, rewarded, xiansuoCount)
    end

    list = joinedCaiXiang and g_MessagePack.unpack(joinedCaiXiang)
    for i = 1, #list do
        local joinedCaiXiangId = list[i]
        guess[joinedCaiXiangId] = true
        --print("SendShanYeMiZongInfo joinedCaiXiangId", joinedCaiXiangId)
    end

    if CUIManager.IsLoaded(CLuaUIResources.ShanYeMiZongMainWnd) then
        g_ScriptEvent:BroadcastInLua("SendShanYeMiZongInfo", finalRewarded, extraTaskRewarded, clue, chapter, guess)
    else
        LuaShanYeMiZongMainWnd.s_OpenWndWithInfo = {finalRewarded, extraTaskRewarded, clue, chapter, guess}
        CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongMainWnd)
    end

    -- 打开山野迷踪 关闭活动主界面
    CUIManager.CloseUI(CLuaUIResources.ShuJia2022MainWnd)
end

function LuaShanYeMiZongMgr:SendShanYeMiZongCaiXiangInfo(caixiangId, myChoice, rightAnswer, caixiangInfo)
    local choice = {}

    --print("SendShanYeMiZongCaiXiangInfo", caixiangId, myChoice, rightAnswer)
    local list = caixiangInfo and g_MessagePack.unpack(caixiangInfo)
    local tot = 0
    for i = 1, #list, 2 do
        tot = tot + list[i + 1]
    end
    for i = 1, #list, 2 do
        local answerId = list[i]
        local percent = tot > 0 and list[i + 1] / tot or 0
        choice[answerId] = percent
        --print("SendShanYeMiZongCaiXiangInfo choice", answerId, percent)
    end

    g_ScriptEvent:BroadcastInLua("SendShanYeMiZongCaiXiangInfo", choice, myChoice, rightAnswer)
end

function LuaShanYeMiZongMgr:SendShanYeMiZongNewXianSuo(newXianSuoId)
    print("SendShanYeMiZongNewXianSuo", newXianSuoId)
    if newXianSuoId and newXianSuoId ~= 0 then
        LuaShanYeMiZongClueAvailableWnd.s_ClueId = newXianSuoId
        CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongClueAvailableWnd)
        return
    end
end

function LuaShanYeMiZongMgr:SendShanYeMiZongFinalReward(rewarded, finalSucc, allCaiXiangSucc, serverFinalSucc, rewardItems)
    local items = {}

    --print("SendShanYeMiZongFinalReward", rewarded, finalSucc, allCaiXiangSucc, serverFinalSucc)
    local list = rewardItems and g_MessagePack.unpack(rewardItems)
    for i = 1, #list, 2 do
        local rewardItemId = list[i]
        local rewardItemCount = list[i + 1]
        items[rewardItemId] = rewardItemCount
        --print("SendShanYeMiZongFinalReward reward", rewardItemId, rewardItemCount)
    end

    g_ScriptEvent:BroadcastInLua("SendShanYeMiZongFinalReward", rewarded, finalSucc, allCaiXiangSucc, serverFinalSucc, items)
end

function LuaShanYeMiZongMgr:ShowShanYeMiZongZhenXiong() -- 只可能在QueryShanYeMiZongInfo之后发
    --print("ShowShanYeMiZongZhenXiong")
    LuaShanYeMiZongRevealMurdererWnd.s_bIsRevealMurderer = true
    LuaShanYeMiZongRevealMurdererWnd.s_ShowRevealFx = true
    CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongRevealMurdererWnd)
end

function LuaShanYeMiZongMgr.timediff(long_time, short_time)
	local n_short_time,n_long_time,carry,diff = os.date('*t',short_time),os.date('*t',long_time),false,{}
	local colMax = {60,60,24,os.date('*t',os.time{year=n_short_time.year,month=n_short_time.month+1,day=0}).day,12,0}
	n_long_time.hour = n_long_time.hour - (n_long_time.isdst and 1 or 0) + (n_short_time.isdst and 1 or 0) -- handle dst
	for i,v in ipairs({'sec','min','hour','day','month','year'}) do
		diff[v] = n_long_time[v] - n_short_time[v] + (carry and -1 or 0)
		carry = diff[v] < 0
		if carry then
			diff[v] = diff[v] + colMax[i]
		end
	end
	return diff
end
