-- Auto Generated!!
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CGuildQueenHeroInfo = import "L10.Game.CGuildQueenHeroInfo"
local CGuildQueenMgr = import "L10.Game.CGuildQueenMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local EnumEventType = import "EnumEventType"
local EnumGender = import "L10.Game.EnumGender"
local EnumStatType = import "L10.Game.EnumStatType"
local ETrackType = import "L10.Game.CutScene.ETrackType"
local EventManager = import "EventManager"
local KouDaoPlayerBasicInfo = import "L10.Game.KouDaoPlayerBasicInfo"
local KouDaoPlayerStatInfo = import "L10.Game.KouDaoPlayerStatInfo"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"

Gas2Gac.QueryBangHuaFightDataResult = function (type, data_U, dataLength, totalNum) 
    local data = TypeAs(MsgPackImpl.unpack(data_U), typeof(MakeGenericClass(List, Object)))

    CGuildQueenMgr.Inst.StatInfo.statType = CommonDefs.ConvertIntToEnum(typeof(EnumStatType), type)
    CGuildQueenMgr.Inst.StatInfo.playerCount = data.Count
    CGuildQueenMgr.Inst.StatInfo.statTotal = totalNum
    CommonDefs.ListClear(CGuildQueenMgr.Inst.StatInfo.players)

    do
        local i = 0
        while i < data.Count do
            local dataList = TypeAs(data[i], typeof(MakeGenericClass(List, Object)))
            local playerId = math.floor(tonumber(dataList[0] or 0))
            local playerName = ToStringWrap(dataList[1])
            local playerClass = math.floor(tonumber(dataList[2] or 0))
            local num = math.floor(tonumber(dataList[3] or 0))

            local info = CreateFromClass(KouDaoPlayerStatInfo, playerId, playerName, playerClass, num)
            CommonDefs.ListAdd(CGuildQueenMgr.Inst.StatInfo.players, typeof(KouDaoPlayerStatInfo), info)
            i = i + 1
        end
    end
    CommonDefs.ListSort1(CGuildQueenMgr.Inst.StatInfo.players, typeof(KouDaoPlayerStatInfo), DelegateFactory.Comparison_KouDaoPlayerStatInfo(function (info1, info2) 
        return NumberCompareTo(info2.playerStat, info1.playerStat)
    end))
    EventManager.BroadcastInternalForLua(EnumEventType.OnRequestBangHuaStatInfoFinished, {type})
end
Gas2Gac.QueryBangHuaScenePlayerInfoResult = function (count, data_U) 
    local data = TypeAs(MsgPackImpl.unpack(data_U), typeof(MakeGenericClass(List, Object)))
    CommonDefs.ListClear(CGuildQueenMgr.Inst.PlayersInCopy)
    do
        local i = 0
        while i < data.Count do
            local dataList = TypeAs(data[i], typeof(MakeGenericClass(List, Object)))
            local playerId = math.floor(tonumber(dataList[0] or 0))
            local playerName = ToStringWrap(dataList[1])
            local playerClass = math.floor(tonumber(dataList[2] or 0))
            local playerlevel = math.floor(tonumber(dataList[3] or 0))
            local inTeam = CommonDefs.Convert_ToBoolean(dataList[4])

            local info = CreateFromClass(KouDaoPlayerBasicInfo, playerId, playerName, playerClass, playerlevel, inTeam)
            CommonDefs.ListAdd(CGuildQueenMgr.Inst.PlayersInCopy, typeof(KouDaoPlayerBasicInfo), info)
            i = i + 1
        end
    end
    CommonDefs.ListSort1(CGuildQueenMgr.Inst.PlayersInCopy, typeof(KouDaoPlayerBasicInfo), DelegateFactory.Comparison_KouDaoPlayerBasicInfo(function (info1, info2) 
        return NumberCompareTo(info2.playerGrade, info1.playerGrade)
    end))
    EventManager.Broadcast(EnumEventType.OnQueryBangHuaScenePlayerInfoResult)
end
Gas2Gac.PlayBangHuaThirdStageVideo = function (videoName, bExist, role, gender, banghuaappearance, heroClass, heroGender, heroAppearance) 
    local dict = CreateFromClass(MakeGenericClass(Dictionary, ETrackType, CPropertyAppearance))
    if bExist then
        local banghua = CreateFromClass(CPropertyAppearance)
        banghua:LoadFromString(banghuaappearance, CommonDefs.ConvertIntToEnum(typeof(EnumClass), role), CommonDefs.ConvertIntToEnum(typeof(EnumGender), gender))
        CommonDefs.DictAdd(dict, typeof(ETrackType), ETrackType.GuildQueen, typeof(CPropertyAppearance), banghua)
    end
    local hero = CreateFromClass(CPropertyAppearance)
    hero:LoadFromString(heroAppearance, CommonDefs.ConvertIntToEnum(typeof(EnumClass), heroClass), CommonDefs.ConvertIntToEnum(typeof(EnumGender), heroGender))
    CommonDefs.DictAdd(dict, typeof(ETrackType), ETrackType.GuildQueenHero, typeof(CPropertyAppearance), hero)
    AMPlayer.Inst:Play(videoName, nil, nil, dict)
end
Gas2Gac.QueryBangHuaSecondSingleInfoResult = function (result_U) 
    local result = TypeAs(MsgPackImpl.unpack(result_U), typeof(MakeGenericClass(List, Object)))
    if result ~= nil then
        CommonDefs.ListClear(CGuildQueenMgr.Inst.HeroInfo)
        do
            local i = 0
            while i < result.Count do
                local info = TypeAs(result[i], typeof(MakeGenericClass(List, Object)))
                if info ~= nil then
                    local heroInfo = CreateFromClass(CGuildQueenHeroInfo)
                    heroInfo.PlayerId = math.floor(tonumber(info[0] or 0))
                    heroInfo.PlayerName = ToStringWrap(info[1])
                    heroInfo.Cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), math.floor(tonumber(info[2] or 0)))
                    heroInfo.Stage = math.floor(tonumber(info[3] or 0))
                    heroInfo.Percent = 0
                    CommonDefs.ListAdd(CGuildQueenMgr.Inst.HeroInfo, typeof(CGuildQueenHeroInfo), heroInfo)
                end
                i = i + 1
            end
        end
        EventManager.BroadcastInternalForLua(EnumEventType.OnRequestBangHuaStatInfoFinished, {EnumStatType_lua.eGuildHero})
    end
end
