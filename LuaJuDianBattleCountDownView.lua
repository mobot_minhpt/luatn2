local UISprite = import "UISprite"
local UITable = import "UITable"
local NGUIMath = import "NGUIMath"
local UILabel = import "UILabel"
local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Extensions = import "Extensions"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTaskMgr = import "L10.Game.CTaskMgr"
local NGUIText = import "NGUIText"
local CChatLinkMgr = import "CChatLinkMgr"

LuaJuDianBattleCountDownView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleCountDownView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaJuDianBattleCountDownView, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaJuDianBattleCountDownView, "RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaJuDianBattleCountDownView, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaJuDianBattleCountDownView, "TransportBtn", "TransportBtn", GameObject)
RegistChildComponent(LuaJuDianBattleCountDownView, "Table", "Table", UITable)
RegistChildComponent(LuaJuDianBattleCountDownView, "LeftBtn", "LeftBtn", CButton)
RegistChildComponent(LuaJuDianBattleCountDownView, "RightBtn", "RightBtn", CButton)
RegistChildComponent(LuaJuDianBattleCountDownView, "Rank", "Rank", GameObject)
RegistChildComponent(LuaJuDianBattleCountDownView, "Time", "Time", GameObject)
RegistChildComponent(LuaJuDianBattleCountDownView, "Bg", "Bg", UISprite)
RegistChildComponent(LuaJuDianBattleCountDownView, "TaskItem", "TaskItem", GameObject)

RegistClassMember(LuaJuDianBattleCountDownView, "m_TaskItem")
RegistClassMember(LuaJuDianBattleCountDownView, "m_isDaily")
RegistClassMember(LuaJuDianBattleCountDownView, "m_ZhanLongTaskIDList")
RegistClassMember(LuaJuDianBattleCountDownView, "m_CurrentZhanLongTaskID")

--@endregion RegistChildComponent end

function LuaJuDianBattleCountDownView:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.TransportBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTransportBtnClick()
	end)

	UIEventListener.Get(self.TaskItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTaskItemClick()
	end)

    --@endregion EventBind end
end

function LuaJuDianBattleCountDownView:Init()
	self.m_ZhanLongTaskIDList = LuaJuDianBattleMgr:GetZhanLongTaskIDList()
	if LuaJuDianBattleMgr.SeasonData then
		self:OnData()
	else
		-- 没有数据就显示日常模式
		self:SetMode(true)
	end
end

-- 更新倒计时
function LuaJuDianBattleCountDownView:OnSceneRemainTimeUpdate(args)
	local totalSeconds = 0
    if CScene.MainScene then
		totalSeconds = CScene.MainScene.ShowTime
    end
	self.TimeLabel.text = SafeStringFormat3("[ACF9FF]%s", LuaGamePlayMgr:GetRemainTimeText(totalSeconds))
end

function LuaJuDianBattleCountDownView:SetMode(isDaily)
	self.m_isDaily = isDaily
	self.Rank:SetActive(false)
	local jumpBtn = self.TransportBtn.transform:GetComponent(typeof(CButton))
	if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
		jumpBtn.Text = LocalString.GetString("召唤队员跟随场景")
	else
		jumpBtn.Text = LocalString.GetString("前往队长所在场景")
	end

	if isDaily or LuaJuDianBattleMgr:IsInDailyGameplay() then
		self.DescLabel.text = g_MessageMgr:FormatMessage("JuDian_Guide_Task_Wnd_View_Desc")
		self.RankLabel.gameObject:SetActive(false)
		self.TransportBtn.gameObject:SetActive(true)

		self.LeftBtn.Text = LocalString.GetString("回本服")
		UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			LuaJuDianBattleMgr:RequestBackSelfSever()
		end)

		self.RightBtn.Text = LocalString.GetString("回据点")
		UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			-- 回到驻点
			Gac2Gas.GuildJuDianRequestGoToOccupation()
		end)
	else
		local seasonData = LuaJuDianBattleMgr.SeasonData
		-- 战斗中
		local nameList = {LocalString.GetString("甲"), LocalString.GetString("乙"), LocalString.GetString("丙"),LocalString.GetString("丁"),
		LocalString.GetString("戊"),LocalString.GetString("己"),LocalString.GetString("庚")}
		self.DescLabel.text = SafeStringFormat3(LocalString.GetString("第%s赛季-第%s周-%s组"), seasonData.season, seasonData.week, nameList[seasonData.battleFieldId])
		self.RankLabel.text = seasonData.rankPos
		self:OnSceneRemainTimeUpdate()

		self.RankLabel.gameObject:SetActive(true)
		self.TransportBtn.gameObject:SetActive(true)
		self.Rank:SetActive(true)

		self.LeftBtn.Text = LocalString.GetString("规则")
		UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			LuaCommonTextImageRuleMgr:ShowWnd(2)
		end)

		self.RightBtn.Text = LocalString.GetString("战况")
		UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			-- 打开战况
			if LuaJuDianBattleMgr.SeasonData then
				if LuaJuDianBattleMgr.SeasonData.playStage == EnumJuDianPlayStage.eInFight or 
					LuaJuDianBattleMgr.SeasonData.playStage == EnumJuDianPlayStage.ePrepare or 
						seasonData.playStage == EnumJuDianPlayStage.eSummary   then
					LuaJuDianBattleMgr:OpenInfoWnd(0)
					return
				end
			end 
			g_MessageMgr:ShowMessage("Can_not_Open_JuDian_Battle_Info_Wnd")
		end)
	end
	self:InitZhanLongTaskView()

	self.Table:Reposition()

	local h = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform)
	self.Bg.height = h.size.y
end

function LuaJuDianBattleCountDownView:OnData()
    local seasonData = LuaJuDianBattleMgr.SeasonData
	print("seasonData.playStage ", seasonData.playStage )
	if seasonData.playStage == EnumJuDianPlayStage.eInFight or 
		seasonData.playStage == EnumJuDianPlayStage.ePrepare or seasonData.playStage == EnumJuDianPlayStage.eSummary then
		self:SetMode(false)
	else
		self:SetMode(true)
	end
end

function LuaJuDianBattleCountDownView:ShowZhanLongTaskView(showTask)
	self.Rank.gameObject:SetActive(not self.m_isDaily)
	self.Time:SetActive(LuaJuDianBattleMgr:IsInGameplay())
	self.TaskName.gameObject:SetActive(not showTask)
	self.DescLabel.gameObject:SetActive(not showTask)
	self.TaskItem.gameObject:SetActive(showTask)
end

function LuaJuDianBattleCountDownView:InitZhanLongTaskView()
	local showTask = false
    local mainplayer = CClientMainPlayer.Inst
	if mainplayer and self.m_isDaily then
		local taskProperty = mainplayer.TaskProp
		for i = 1, #self.m_ZhanLongTaskIDList do
			local taskId = self.m_ZhanLongTaskIDList[i]
			if CommonDefs.DictContains(taskProperty.CurrentTasks, typeof(UInt32), taskId) then
				showTask = true
				self.m_CurrentZhanLongTaskID = taskId
				local task = CommonDefs.DictGetValue(taskProperty.CurrentTasks, typeof(UInt32), taskId)
				self:InitTaskItem(taskId, task)
				break
			end
		end
	end
	self:ShowZhanLongTaskView(showTask)
	self.Table:Reposition()
	local h = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform)
	self.Bg.height = h.size.y
end

function LuaJuDianBattleCountDownView:InitTaskItem(taskId, task)
	local background = self.TaskItem:GetComponent(typeof(UISprite))
	local taskNameLabel = self.TaskItem.transform:Find("TaskName"):GetComponent(typeof(UILabel))
	local taskInfoLabel = self.TaskItem.transform:Find("TaskInfo"):GetComponent(typeof(UILabel))
	local taskTemplate = Task_Task.GetData(taskId)
	-- 任务标题
	local taskname = taskTemplate.Display
	if task.IsFailed then
		taskname = taskname .. LocalString.GetString("  [ff0000](失败)[-]")
	elseif task.CanSubmit == 1 then
		taskname = taskname .. LocalString.GetString("  [14ff0e]完成[-]")
	end
	taskNameLabel.text = taskname
	-- 任务信息
	local local_ = CTaskMgr.Inst:GetTaskTrackPos(task)
	local taskinfo = CTaskMgr.Inst:GetTaskTrackDescription(task)
	taskinfo = local_ and local_.trackInfo .. taskinfo or taskinfo
	taskInfoLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(taskInfoLabel.color), CChatLinkMgr.TranslateToNGUIText(taskinfo))

    local nameHeight = taskNameLabel.localSize.y
    local verticalGap = 14
    local infoHeight = taskInfoLabel.localSize.y
	local toalHeight = nameHeight + infoHeight + verticalGap * 3
    background.height = math.floor(toalHeight + 0.5)
end

function LuaJuDianBattleCountDownView:OnEnable()
	Gac2Gas.GuildJuDianQueryGlobalTopGuildRank()
	self:Init()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("GuildJuDianSyncGlobalTopRank", self, "OnData")
    g_ScriptEvent:AddListener("UpdateCurrentTask", self, "InitZhanLongTaskView")
end

function LuaJuDianBattleCountDownView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("GuildJuDianSyncGlobalTopRank", self, "OnData")
    g_ScriptEvent:RemoveListener("UpdateCurrentTask", self, "InitZhanLongTaskView")
end

--@region UIEvent

function LuaJuDianBattleCountDownView:OnTransportBtnClick()
	Gac2Gas.GuildJuDianTeleportToLeaderScene()
end

function LuaJuDianBattleCountDownView:OnTaskItemClick()
	local taskProperty = CClientMainPlayer.Inst.TaskProp
	local task = CommonDefs.DictGetValue(taskProperty.CurrentTasks, typeof(UInt32), self.m_CurrentZhanLongTaskID)
	local location = CTaskMgr.Inst:GetTaskTrackPos(task)
	local taskTemplate = Task_Task.GetData(self.m_CurrentZhanLongTaskID)
	if task.IsFailed then
		CTaskInfoMgr.ShowTaskWnd(self.m_CurrentZhanLongTaskID, 0)
		g_MessageMgr:ShowMessage("TASK_FAILURE_NEED_GIVE_UP")
		return 
	elseif location.sceneTemplateId > 0 and CScene.MainScene and CScene.MainScene.SceneTemplateId ~= location.sceneTemplateId then
		-- 涉及到场景切换
		if location.sceneTemplateId == 16102358 then
			-- 前往驻地
			Gac2Gas.GuildJuDianRequestGoToOccupation()
			return
		elseif taskTemplate.GamePlay == "GuildJuDianZhanLong" then
			Gac2Gas.GuildJuDianRequestTeleportToDailyScene(location.sceneTemplateId, location.targetX, location.targetY)
			return
		end
	end
	CTaskMgr.Inst:TrackToTaskLocation(location)
end

--@endregion UIEvent

