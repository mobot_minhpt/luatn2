local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"

local DelegateFactory  = import "DelegateFactory"

LuaPersonalSpaceSlientLoveConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPersonalSpaceSlientLoveConfirmWnd, "NoBtn", "NoBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceSlientLoveConfirmWnd, "YesBtn", "YesBtn", GameObject)
RegistChildComponent(LuaPersonalSpaceSlientLoveConfirmWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaPersonalSpaceSlientLoveConfirmWnd, "Name", "Name", UILabel)
RegistChildComponent(LuaPersonalSpaceSlientLoveConfirmWnd, "Icon", "Icon", CUITexture)

--@endregion RegistChildComponent end

function LuaPersonalSpaceSlientLoveConfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.NoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNoBtnClick()
	end)

	UIEventListener.Get(self.YesBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYesBtnClick()
	end)

    --@endregion EventBind end
end

function LuaPersonalSpaceSlientLoveConfirmWnd:Init()
	if LuaPersonalSpaceMgrReal.SlientLoveCls and LuaPersonalSpaceMgrReal.SlientLoveGender and LuaPersonalSpaceMgrReal.SlientLoveRoleId then
		self.Icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaPersonalSpaceMgrReal.SlientLoveCls, LuaPersonalSpaceMgrReal.SlientLoveGender, -1), false)
		self.Name.text = LuaPersonalSpaceMgrReal.SlientLoveName
		self.Desc.text = g_MessageMgr:FormatMessage("MengDao_AnLian_Confirm")
	else
		CUIManager.CloseUI(CLuaUIResources.PersonalSpaceSlientLoveConfirmWnd)
	end
end

--@region UIEvent

function LuaPersonalSpaceSlientLoveConfirmWnd:OnNoBtnClick()
	CUIManager.CloseUI(CLuaUIResources.PersonalSpaceSlientLoveConfirmWnd)
end

function LuaPersonalSpaceSlientLoveConfirmWnd:OnYesBtnClick()
	if LuaPersonalSpaceMgrReal.SlientLoveRoleId and LuaPersonalSpaceMgrReal.SlientLoveName then
		Gac2Gas.SetSecretLove(LuaPersonalSpaceMgrReal.SlientLoveRoleId, LuaPersonalSpaceMgrReal.SlientLoveName)
	end
	CUIManager.CloseUI(CLuaUIResources.PersonalSpaceSlientLoveConfirmWnd)
end


--@endregion UIEvent

