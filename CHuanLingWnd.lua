-- Auto Generated!!
local Byte = import "System.Byte"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDrawLineMgr = import "L10.Game.CDrawLineMgr"
local CFileTools = import "CFileTools"
local CHuanLingMgr = import "L10.Game.CHuanLingMgr"
local CHuanLingWnd = import "L10.UI.CHuanLingWnd"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EShareType = import "L10.UI.EShareType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Item_Item = import "L10.Game.Item_Item"
local Json = import "L10.Game.Utils.Json"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local Object = import "System.Object"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local System = import "System"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_LingfuZhizuoUpgrade = import "L10.Game.ZuoQi_LingfuZhizuoUpgrade"
local MD5 =  import "System.Security.Cryptography.MD5"
CHuanLingWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.m_ApplyButton).onClick = MakeDelegateFromCSFunction(this.OnApplyButtonClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_ApplyFiveButton.gameObject).onClick = MakeDelegateFromCSFunction(this.OnApplyFiveButtonClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_UpgradeButton).onClick = MakeDelegateFromCSFunction(this.OnUpgradeButtonClicked, VoidDelegate, this)
    UIEventListener.Get(this.m_MoreInfo).onClick = MakeDelegateFromCSFunction(this.OnMoreInfoClicked, VoidDelegate, this)
end
CHuanLingWnd.m_OnSelectLingFu_CS2LuaHook = function (this, lingFuId) 
    if lingFuId == this.fuzhiIDs[0] then
        this.m_ApplyFiveButton.Text = LocalString.GetString("召唤十次")
    else
        this.m_ApplyFiveButton.Text = LocalString.GetString("召唤五次")
    end
end
CHuanLingWnd.m_Init_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.m_Consumptions.Length do
            this.m_Consumptions[i]:Init(this.fuzhiIDs[i])
            i = i + 1
        end
    end

    this:UpdateHuanLingExp()
    this.m_RadioBox:ChangeTo(0, true)
end
CHuanLingWnd.m_OnApplyButtonClicked_CS2LuaHook = function (this, go) 
    if this.m_RadioBox.CurrentSelectIndex < this.fuzhiIDs.Length then
        local bindCount, notbindCount
        local lingFuID = this.fuzhiIDs[this.m_RadioBox.CurrentSelectIndex]
        bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(CHuanLingMgr.Inst.SelectedLingFuId)
        if bindCount + notbindCount > 0 then
            if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < 1 then
                g_MessageMgr:ShowMessage("BAG_SPACE_NOT_ENOUGH")
            else
                CHuanLingMgr.IS_HUANLING_FIVE = false
                CDrawLineMgr.Inst.PenMaterialPath = nil
                CUIManager.ShowUI(CUIResources.HuanLingDrawWnd)
            end
        else
            local item = Item_Item.GetData(CHuanLingMgr.Inst.SelectedLingFuId)
            if item ~= nil then
                g_MessageMgr:ShowMessage("LINGFU_IS_NOT_ENOUGH", item.Name)
            end
        end
    end
end
CHuanLingWnd.m_OnApplyFiveButtonClicked_CS2LuaHook = function (this, go) 
    if not CHuanLingMgr.OPEN_FIVE_HUANLING then
        g_MessageMgr:ShowMessage("FIVE_HUANLING_NOT_OPEN")
        return
    end

    local bindCount, notbindCount
    if this.m_RadioBox.CurrentSelectIndex < this.fuzhiIDs.Length then
        local lingFuID = this.fuzhiIDs[this.m_RadioBox.CurrentSelectIndex]
        bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(CHuanLingMgr.Inst.SelectedLingFuId)
        if bindCount + notbindCount >= CHuanLingMgr.Inst:GetHuanLingNum(CHuanLingMgr.Inst.SelectedLingFuId) then
            if CClientMainPlayer.Inst.ItemProp:GetEmptyPosCount(EnumItemPlace.Bag) < CHuanLingMgr.Inst:GetHuanLingNum(CHuanLingMgr.Inst.SelectedLingFuId) then
                g_MessageMgr:ShowMessage("BAG_SPACE_NOT_ENOUGH")
            else
                CHuanLingMgr.IS_HUANLING_FIVE = true
                CDrawLineMgr.Inst.PenMaterialPath = nil
                CUIManager.ShowUI(CUIResources.HuanLingDrawWnd)
            end
        else
            local item = Item_Item.GetData(CHuanLingMgr.Inst.SelectedLingFuId)
            if item ~= nil then
                g_MessageMgr:ShowMessage("LINGFU_IS_NOT_ENOUGH", item.Name)
            end
        end
    end
end
CHuanLingWnd.m_OnUpgradeButtonClicked_CS2LuaHook = function (this, go) 

    if CClientMainPlayer.Inst ~= nil then
        local info = CClientMainPlayer.Inst.PlayProp.YumaPlayData
        local level = math.max(1, info.LingfuLevel)
        local exp = info.LingfuExp
        -- Step 1 检查经验是否足够
        local zhiZuoExpUpgrade = ZuoQi_LingfuZhizuoUpgrade.GetData(level)
        if zhiZuoExpUpgrade ~= nil and exp < zhiZuoExpUpgrade.Exp then
            g_MessageMgr:ShowMessage("Mapi_Lingfu_Upgrade_Exp_Not_Enough")
            return
        end
        -- Step 2 检查银票（确认框）
        --ZuoQi_LingfuZhizuoUpgrade zhiZuoCostUpgrade = ZuoQi_LingfuZhizuoUpgrade.GetData(level + 1);
        if zhiZuoExpUpgrade ~= nil and level < CHuanLingWnd.MAX_HUANLING_LEVEL then
            local cost = zhiZuoExpUpgrade.Cost
            local message = g_MessageMgr:FormatMessage("Huanling_Lv_Up_Cost_Confirm", cost)
            -- 优先银票
            QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinPiao, message, cost, DelegateFactory.Action(function () 
                Gac2Gas.RequestUpgradeLingfuLevel()
            end), nil, false, true, EnumPlayScoreKey.NONE, false)
        elseif level >= CHuanLingWnd.MAX_HUANLING_LEVEL then
            g_MessageMgr:ShowMessage("Update_Fail_Max_Grade")
        end
    end
end
CHuanLingWnd.m_OnGenerateLingguSuccess_CS2LuaHook = function (this, lingfuId, quality, pos) 
    -- 更新数量
    do
        local i = 0
        while i < this.m_Consumptions.Length do
            this.m_Consumptions[i]:Init(this.fuzhiIDs[i])
            i = i + 1
        end
    end
    -- 更新经验
    this:UpdateHuanLingExp()
end
CHuanLingWnd.m_UpdateHuanLingExp_CS2LuaHook = function (this) 

    local level = 1
    local slideValue = 0
    if CClientMainPlayer.Inst ~= nil then
        local info = CClientMainPlayer.Inst.PlayProp.YumaPlayData

        level = math.max(1, info.LingfuLevel)
        local exp = info.LingfuExp

        local zhiZuoUpgrade = ZuoQi_LingfuZhizuoUpgrade.GetData(level)
        if zhiZuoUpgrade ~= nil then
            slideValue = (exp * 1) / zhiZuoUpgrade.Exp
            this.m_ExpLabel.text = System.String.Format("{0}/{1}", exp, zhiZuoUpgrade.Exp)
            this.m_UpgradeAlert:SetActive(exp >= zhiZuoUpgrade.Exp and level < CHuanLingWnd.MAX_HUANLING_LEVEL)
        else
            this.m_UpgradeAlert:SetActive(false)
        end
    end
    this.m_LvLabel.text = System.String.Format("Lv. {0}", level)
    this.m_LvSlider.value = slideValue
end
CHuanLingMgr.m_GetLingFuQuality_CS2LuaHook = function (this, templateId) 
    do
        local i = 0
        while i < this.fuzhiIDs.Length do
            if this.fuzhiIDs[i] == templateId then
                return i + 1
            end
            i = i + 1
        end
    end
    return 0
end
CHuanLingMgr.m_GetFuZhiQuality_CS2LuaHook = function (this, fuzhiId) 
    do
        local i = 0
        while i < this.fuzhiIDs.Length do
            if fuzhiId == this.fuzhiIDs[i] then
                return i
            end
            i = i + 1
        end
    end
    return - 1
end
CHuanLingMgr.m_ShareLingFu_CS2LuaHook = function (this, imgurl, pos) 

    if this.m_UrlCoroutine ~= nil then
        Main.Inst:StopCoroutine(this.m_UrlCoroutine)
    end

    if CClientMainPlayer.Inst == nil then
        return
    end

    local roleid = CClientMainPlayer.Inst.Id
    local nickname = CClientMainPlayer.Inst.Name
    local server = CClientMainPlayer.Inst:GetMyServerName()
    local gender = EnumToInt(CClientMainPlayer.Inst.Gender)
    local job = EnumToInt(CClientMainPlayer.Inst.Class)

    local md5 = MD5.Create()
    local token = ((CHuanLingMgr.s_CipherCode .. tostring(CClientMainPlayer.Inst.Id)) .. EnumToInt(CClientMainPlayer.Inst.Gender)) .. EnumToInt(CClientMainPlayer.Inst.Class)
    local inputBytes = CommonDefs.ASCIIEncoding_GetBytes(token)
    local hash = md5:ComputeHash(inputBytes)

    local sb = NewStringBuilderWraper(StringBuilder)
    sb:Append((NumberComplexToString(hash[0], typeof(Byte), "x2") .. NumberComplexToString(hash[1], typeof(Byte), "x2")) .. NumberComplexToString(hash[2], typeof(Byte), "x2"))

    local url = CommonDefs.String_Format_String_ArrayObject(CHuanLingMgr.GetShareUrl(), {
        roleid, 
        nickname, 
        server, 
        gender, 
        job, 
        imgurl, 
        EnumToInt(pos), 
        ToStringWrap(sb)
    })

    this.m_UrlCoroutine = Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, text) 
        if success then
            local dict = TypeAs(Json.Deserialize(text), typeof(MakeGenericClass(Dictionary, String, Object)))
            if nil == dict then
                return
            end
            if not CommonDefs.DictContains(dict, typeof(String), "code") or math.floor(tonumber(CommonDefs.DictGetValue(dict, typeof(String), "code") or 0)) <= 0 or not CommonDefs.DictContains(dict, typeof(String), "imgurl") then
                if CommonDefs.DictContains(dict, typeof(String), "msg") then
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2", CommonDefs.DictGetValue(dict, typeof(String), "msg"))
                end
                return
            end
            this:ShareLingFuImage(ToStringWrap(CommonDefs.DictGetValue(dict, typeof(String), "imgurl")))
        else
            g_MessageMgr:ShowMessage("SHARE_LINGFU_IMAGE_FAIL")
        end
    end)))
end
CHuanLingMgr.m_ShareLingFuImage_CS2LuaHook = function (this, imageUrl) 
    if this.m_PicCoroutine ~= nil then
        Main.Inst:StopCoroutine(this.m_PicCoroutine)
        this.m_PicCoroutine = nil
    end

    this.m_PicCoroutine = Main.Inst:StartCoroutine(HTTPHelper.DownloadImage(imageUrl, DelegateFactory.Action_bool_byte_Array(function (picResult, picData) 
        local filename = Utility.persistentDataPath .. "/LingFuShare.jpg"
        System.IO.File.WriteAllBytes(filename, picData)
        CFileTools.SetFileNoBackup(filename)

        if CUIManager.IsLoaded(CUIResources.HuanLingShareWnd) then
            EventManager.BroadcastInternalForLua(EnumEventType.OnGetLingFuShareImage, {imageUrl})
        else
            CTickMgr.Register(DelegateFactory.Action(function () 
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, {filename})
            end), 1000, ETickType.Once)
        end
    end)))
end
CHuanLingMgr.m_GetHuanLingNum_CS2LuaHook = function (this, lingFuID) 
    if lingFuID == 21102063 then
        return CHuanLingMgr.TEN_HUANLING
    end
    return CHuanLingMgr.FIVE_HUANLING
end

