local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local CCurrentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CUITexture = import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CJingLingMgr = import "L10.Game.CJingLingMgr"

LuaStoreRoomPosUnlockWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStoreRoomPosUnlockWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaStoreRoomPosUnlockWnd, "DaoJuTemplate", "DaoJuTemplate", GameObject)
RegistChildComponent(LuaStoreRoomPosUnlockWnd, "YinLiangTemplate", "YinLiangTemplate", GameObject)
RegistChildComponent(LuaStoreRoomPosUnlockWnd, "LingYuTemplate", "LingYuTemplate", GameObject)
RegistChildComponent(LuaStoreRoomPosUnlockWnd, "FeiShengTemplate", "FeiShengTemplate", GameObject)
RegistChildComponent(LuaStoreRoomPosUnlockWnd, "YueKaTemplate", "YueKaTemplate", GameObject)
RegistChildComponent(LuaStoreRoomPosUnlockWnd, "WatrForMoreTemplate", "WatrForMoreTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStoreRoomPosUnlockWnd,"mTemplateTable")

function LuaStoreRoomPosUnlockWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.DaoJuTemplate:SetActive(false)
    self.YinLiangTemplate:SetActive(false)
    self.LingYuTemplate:SetActive(false)
    self.FeiShengTemplate:SetActive(false)
    self.YueKaTemplate:SetActive(false)
    self.WatrForMoreTemplate:SetActive(false)
end

function LuaStoreRoomPosUnlockWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncStoreRoomPosCount", self, "OnSyncStoreRoomPosCount")
end

function LuaStoreRoomPosUnlockWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncStoreRoomPosCount", self, "OnSyncStoreRoomPosCount")
end

function LuaStoreRoomPosUnlockWnd:Init()
    self.mTemplateTable = {}
    local repoProp = CClientMainPlayer.Inst.RepertoryProp

    local itemUnlockCount =  repoProp.ItemUnlockedRoomPosCount
    local silverUnlockCount = repoProp.SilverUnlockedRoomPosCount
    local jadeUnlockCount = repoProp.JadeUnlockedRoomPosCount

    local setting = StoreRoom_Setting.GetData()
    local itemUnlockLimit = setting.ItemUnlockLimit
    local silverUnlockLimit = setting.SilverUnlockLimit
    local jadeUnlockLimit = setting.JadeUnlockLimit

    local t = {}
    t.template = self.DaoJuTemplate
    t.isOverLimit = itemUnlockCount >= itemUnlockLimit
    t.priority = 1
    t.times = SafeStringFormat3("%d/%d",itemUnlockCount,itemUnlockLimit)
    t.type = "Item"
    table.insert(self.mTemplateTable,t)

    local t2 = {}
    t2.template = self.YinLiangTemplate
    t2.isOverLimit = silverUnlockCount >= silverUnlockLimit
    t2.priority = 2
    t2.times = SafeStringFormat3("%d/%d",silverUnlockCount,silverUnlockLimit)
    t2.type = "Silver"
    table.insert(self.mTemplateTable,t2)

    local t3 = {}
    t3.template = self.LingYuTemplate
    t3.isOverLimit = jadeUnlockCount >= jadeUnlockLimit
    t3.priority = 3
    t3.times = SafeStringFormat3("%d/%d",jadeUnlockCount,jadeUnlockLimit)
    t3.type = "Jade"
    table.insert(self.mTemplateTable,t3)

    local t4 = {}
    t4.template = self.FeiShengTemplate
    t4.isOverLimit = CClientMainPlayer.Inst.HasFeiSheng
    t4.priority = 4
    t4.type = "FeiSheng"
    table.insert(self.mTemplateTable,t4)

    --是否开启了大月卡
    if LuaWelfareMgr.IsOpenBigMonthCard then
        local info = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo or nil
        local bigLeftDay = LuaWelfareMgr:MonthCardLeftDay(info)
        local t5 = {}
        t5.template = self.YueKaTemplate
        t5.isOverLimit = info and bigLeftDay > 0 
        t5.priority = 5
        t5.type = "YueKa"
        table.insert(self.mTemplateTable,t5)
    end

    table.sort(self.mTemplateTable,function(a,b)
        if a.isOverLimit and not b.isOverLimit then
            return false
        elseif not a.isOverLimit and b.isOverLimit then
            return true
        else
            return a.priority < b.priority
        end
    end)

    self:InitGrid()

end

function LuaStoreRoomPosUnlockWnd:InitGrid()
    Extensions.RemoveAllChildren(self.Grid.transform)
    for i,t in ipairs(self.mTemplateTable) do
        local go = CUICommonDef.AddChild(self.Grid.gameObject, t.template)
        go:SetActive(true)
        self:InitTemplate(t,go)
    end
    --是否开启了大月卡
    if not LuaWelfareMgr.IsOpenBigMonthCard then
        --敬请期待
        local go = CUICommonDef.AddChild(self.Grid.gameObject, self.WatrForMoreTemplate)
        go:SetActive(true)
    end

    self.Grid:Reposition()
end

function LuaStoreRoomPosUnlockWnd:InitTemplate(t,go)
    if t.type == "Item" then
        self:InitDaoJuTemplate(t,go)
    elseif t.type == "Silver" then
        self:InitYinLiangTemplate(t,go)
    elseif t.type == "Jade" then
        self:InitLingYuTemplate(t,go)
    elseif t.type == "FeiSheng" then
        self:InitFeiShengTemplate(t,go)
    elseif t.type == "YueKa" then
        self:InitYueKaTemplate(t,go)
    else

    end

end
--使用道具解锁
function LuaStoreRoomPosUnlockWnd:InitDaoJuTemplate(t,go)
    local UseTimesLabel = go.transform:Find("UseTimesLabel"):GetComponent(typeof(UILabel))
    local ItemCell = go.transform:Find("Content/ItemCell")
    local IconTexture = ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local AmountLabel = ItemCell.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local GetMask = ItemCell.transform:Find("GetMask")
    local Button = go.transform:Find("Button")
    local LimitLabel = go.transform:Find("LimitLabel")
    local content = go.transform:Find("Content")

    --item
    local setting = StoreRoom_Setting.GetData()
    local templateId = setting.UnlockItemTempId
    local cost = self:GetCostByType("Item")
    local bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(templateId)
    local own = bindCount + notBindCount
    local colorstr = own < cost and "FF0000" or "00FF00"
    AmountLabel.text = SafeStringFormat3("[%s]%d[-][c]/%d",colorstr,own,cost)
    local data = Item_Item.GetData(templateId)
    if data then
        IconTexture:LoadMaterial(data.Icon)
    end
    UseTimesLabel.text = t.times

    Button.gameObject:SetActive(not t.isOverLimit)
    LimitLabel.gameObject:SetActive(t.isOverLimit)
    GetMask.gameObject:SetActive(own < cost)

    UIEventListener.Get(ItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    if own < cost then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, true, nil, AlignType1.Right)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId,false,nil,AlignType.Default, 0, 0, 0, 0)
        end
	end)

    local costStr = SafeStringFormat3(LocalString.GetString("%d个%s"),cost,data.Name)
    UIEventListener.Get(Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)    
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("UNLOCK_STOREROOM_CONFIRM",costStr), DelegateFactory.Action(function ()            
            Gac2Gas.RequestUnlockStoreRoomByItem()
        end), nil, nil, nil, false)
	end)

    content.gameObject:SetActive(not t.isOverLimit)
    self.m_UseItemGo = Button.gameObject
end

--使用银两解锁
function LuaStoreRoomPosUnlockWnd:InitYinLiangTemplate(t,go)
    local UseTimesLabel = go.transform:Find("UseTimesLabel"):GetComponent(typeof(UILabel))
    local Button = go.transform:Find("Button")
    local LimitLabel = go.transform:Find("LimitLabel")
    local MoneyCtrl = go.transform:Find("MoneyCtrl"):GetComponent(typeof(CCurrentMoneyCtrl))

    Button.gameObject:SetActive(not t.isOverLimit)
    LimitLabel.gameObject:SetActive(t.isOverLimit)

    UseTimesLabel.text = t.times
    local cost = self:GetCostByType("Silver")
    MoneyCtrl:SetCost(cost)
    MoneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)

    local costStr = SafeStringFormat3(LocalString.GetString("%d银两"),cost)
    UIEventListener.Get(Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("UNLOCK_STOREROOM_CONFIRM",costStr), DelegateFactory.Action(function ()            
            Gac2Gas.RequestUnlockStoreRoomBySilver()
        end), nil, nil, nil, false)
	end)
    MoneyCtrl.gameObject:SetActive(not t.isOverLimit)
end

--使用灵玉解锁
function LuaStoreRoomPosUnlockWnd:InitLingYuTemplate(t,go)
    local UseTimesLabel = go.transform:Find("UseTimesLabel"):GetComponent(typeof(UILabel))
    local Button = go.transform:Find("Button")
    local LimitLabel = go.transform:Find("LimitLabel")
    local MoneyCtrl = go.transform:Find("MoneyCtrl"):GetComponent(typeof(CCurrentMoneyCtrl))

    Button.gameObject:SetActive(not t.isOverLimit)
    LimitLabel.gameObject:SetActive(t.isOverLimit)

    UseTimesLabel.text = t.times
    local cost = self:GetCostByType("Jade")
    MoneyCtrl:SetCost(cost)
    MoneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)

    local costStr = SafeStringFormat3(LocalString.GetString("%d灵玉"),cost)
    UIEventListener.Get(Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("UNLOCK_STOREROOM_CONFIRM",costStr), DelegateFactory.Action(function ()            
            Gac2Gas.RequestUnlockStoreRoomByJade()
        end), nil, nil, nil, false)
	end)
    MoneyCtrl.gameObject:SetActive(not t.isOverLimit)
end

--飞升解锁
function LuaStoreRoomPosUnlockWnd:InitFeiShengTemplate(t,go)
    local Button = go.transform:Find("Button")
    local LimitLabel = go.transform:Find("LimitLabel")

    Button.gameObject:SetActive(not t.isOverLimit)
    LimitLabel.gameObject:SetActive(t.isOverLimit)

    UIEventListener.Get(Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CJingLingMgr.Inst:ShowJingLingWnd(LocalString.GetString("飞升"), "o_push", true, false, nil, false)
    end)
end

--月卡解锁
function LuaStoreRoomPosUnlockWnd:InitYueKaTemplate(t,go)
    local Button = go.transform:Find("Button")
    local LimitLabel = go.transform:Find("LimitLabel")

    Button.gameObject:SetActive(not t.isOverLimit)
    LimitLabel.gameObject:SetActive(t.isOverLimit)

    UIEventListener.Get(Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        --跳转福利界面
        CUIManager.ShowUI("WelfareWnd")
    end)
end

function LuaStoreRoomPosUnlockWnd:GetCostByType(cType)
    local cost = 0
    local unlockedCount = 0
    local repoProp
    if CClientMainPlayer.Inst then
        repoProp = CClientMainPlayer.Inst.RepertoryProp
    end
    local data 
    if cType == "Item" then
        unlockedCount = repoProp and repoProp.ItemUnlockedRoomPosCount or 0
        data = StoreRoom_BuyPrice.GetData(unlockedCount+1)
        if data then
            cost = data.ItemCost
        end
    elseif cType == "Silver" then
        unlockedCount = repoProp and repoProp.SilverUnlockedRoomPosCount or 0
        data = StoreRoom_BuyPrice.GetData(unlockedCount+1)
        if data then
            cost = data.SilverCost
        end
    elseif cType == "Jade" then
        unlockedCount = repoProp and repoProp.JadeUnlockedRoomPosCount or 0
        data = StoreRoom_BuyPrice.GetData(unlockedCount+1)
        if data then
            cost = data.JadeCost
        end
    end
    return cost
end

function LuaStoreRoomPosUnlockWnd:OnSyncStoreRoomPosCount(itemUnlockCount, silverUnlockCount, jadeUnlockCount)
    self:Init()
end
--@region UIEvent

--@endregion UIEvent
--引导
function LuaStoreRoomPosUnlockWnd:GetGuideGo(methodName)
    if methodName == "GetUseItemBtn" then
        return self.m_UseItemGo
    end
    return nil
end
