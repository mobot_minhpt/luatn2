local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
LuaCreditScoreMgr = {}
LuaCreditScoreMgr.m_otherCreditTimeData = {}

function LuaCreditScoreMgr:RequestPlayerCreditScoreData(pid)
    Gac2Gas.RequestPlayerCreditScoreData(pid)
end

function LuaCreditScoreMgr:SyncSingleCreditScoreData(pid, creditScoreDataUD)
    local creditScoreInfo = CCreditScoreData()
    creditScoreInfo:LoadFromString(creditScoreDataUD)
    g_ScriptEvent:BroadcastInLua("GetPlayerCreditScore", pid, creditScoreInfo.CreditScore)
    EventManager.BroadcastInternalForLua(EnumEventType.GetPlayerCreditScore, {pid, creditScoreInfo.CreditScore});
end

function LuaCreditScoreMgr:OpenFriendTalkReminder()
    return false
end 

function LuaCreditScoreMgr:OpenFace2FaceTradeReminder()
    return false
end

function LuaCreditScoreMgr:RecordCheckCreditScoreTime(pid, timeStamp)
    LuaCreditScoreMgr.m_otherCreditTimeData[pid] = timeStamp
    local json = luaJson.table2json(LuaCreditScoreMgr.m_otherCreditTimeData)
    CPlayerDataMgr.Inst:SavePlayerData("CheckCreditScoreData", json)
end

function LuaCreditScoreMgr:PrepareLocalData()
    if next(LuaCreditScoreMgr.m_otherCreditTimeData) == nil then
        local list = {}
        local json = CPlayerDataMgr.Inst:LoadPlayerData("CheckCreditScoreData")
        if json~=nil and json~="" then
            list = luaJson.json2lua(json)
        end
        if list then
            for key,v in pairs(list) do
                LuaCreditScoreMgr.m_otherCreditTimeData[key] = v
            end
        end 
    end
end

function LuaCreditScoreMgr:OnPlayerLogin()
    LuaCreditScoreMgr.m_otherCreditTimeData = {}
end