local UISimpleTableView = import "L10.UI.UISimpleTableView"
local CButton = import "L10.UI.CButton"
local UISlider = import "UISlider"
local QnTableView = import "L10.UI.QnTableView"
local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local Item_Item = import "L10.Game.Item_Item"
local CUIFx = import "L10.UI.CUIFx"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongTongXingZhengWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "ExpSlider", "ExpSlider", UISlider)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "LvLable", "LvLable", UILabel)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "ExpLabel", "ExpLabel", UILabel)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "BackArrow", "BackArrow", QnButton)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "NextArrow", "NextArrow", QnButton)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "PageLabel", "PageLabel", UILabel)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "FocusBtn", "FocusBtn", QnButton)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "TimeLb", "TimeLb", UILabel)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "HelpBtn", "HelpBtn", QnButton)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "BuyLevelBtn", "BuyLevelBtn", QnButton)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "FinalReward", "FinalReward", GameObject)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "UnLockButton", "UnLockButton", CButton)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "ReceiveButton", "ReceiveButton", CButton)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "Alert", "Alert", GameObject)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "Special", "Special", UILabel)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "FirstPage", "FirstPage", GameObject)
RegistChildComponent(LuaDaFuWongTongXingZhengWnd, "SecondPage", "SecondPage", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_CurLv")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_MaxLv")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_Exp")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_RewardList")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_AllData")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_HasUnlock")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_OnePageNum")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_FirstLineFill")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_SecondLineFill")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_CurPage")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_IsInAni")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_ShowPageTick")
RegistClassMember(LuaDaFuWongTongXingZhengWnd, "m_Anim")
function LuaDaFuWongTongXingZhengWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BackArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackArrowClick()
	end)


	
	UIEventListener.Get(self.NextArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextArrowClick()
	end)


	
	UIEventListener.Get(self.FocusBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFocusBtnClick()
	end)


	
	UIEventListener.Get(self.HelpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHelpBtnClick()
	end)


	
	UIEventListener.Get(self.BuyLevelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyLevelBtnClick()
	end)


	
	UIEventListener.Get(self.UnLockButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUnLockButtonClick()
	end)


	
	UIEventListener.Get(self.ReceiveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReceiveButtonClick()
	end)


	
	UIEventListener.Get(self.Special.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSpecialClick()
	end)


    --@endregion EventBind end
    self.m_CurPage = 1
    self.m_OnePageNum = 5
    self.m_FirstLineFill = {0,0.13,0.3,0.5,0.7,0.85,1}
    self.m_SecondLineFill = {0,0.18,0.35,0.5,0.65,0.83,1}
    self.m_IsInAni = false
    self.m_ShowPageTick = nil
    self.m_Anim = self.gameObject:GetComponent(typeof(Animation))
    local DragArea = self.transform:Find("RewardTabPanel/DragArea").gameObject
    UIEventListener.Get(DragArea.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
        self:OnDragPage(go,delta)
    end)
end

function LuaDaFuWongTongXingZhengWnd:Init()
    if not LuaDaFuWongMgr.TongXingZhengInfo then
        Gac2Gas.RequestDaFuWengPlayData()
    elseif not LuaDaFuWongMgr:TongXingZhengReward() then
        local data = LuaDaFuWongMgr.TongXingZhengInfo
        self.m_CurPage = math.floor((data.lv - 0.5) / self.m_OnePageNum) + 1
    else
        local data = LuaDaFuWongMgr.TongXingZhengInfo
        for k,v in pairs(data.hasReward) do
            if data.vip == 1 and v ~= EnumRewardType.All then
                self.m_CurPage = math.floor((k - 0.5) / self.m_OnePageNum) + 1
                break
            elseif data.vip ~= 1 and v ~= EnumRewardType.Normal then
                self.m_CurPage = math.floor((k - 0.5) / self.m_OnePageNum) + 1
                break
            end
        end
    end
    self.m_CurLv = 1
    self.m_MaxLv = 0
    self.m_Exp = 0
    self.TimeLb.text = g_MessageMgr:FormatMessage("DaFuWeng_TongXingZheng_Time")
    self.DesLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_TongXingZheng_Des")
    self:InitDesignData()
    self:UpdateTongXingZhengInfo()
    UIEventListener.Get(self.FinalReward.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(DaFuWeng_Setting.GetData().BeiShiItemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
	end)
end
-- 读策划表数据
function LuaDaFuWongTongXingZhengWnd:InitDesignData()
    self.m_RewardList = {}
    DaFuWeng_TongXingZheng.Foreach(function (key, data)
        self.m_MaxLv = self.m_MaxLv + 1
        self.m_RewardList[key] = {}
        self.m_RewardList[key].needExp = data.NeedExp
        self.m_RewardList[key].ItemList = {}
        self.m_RewardList[key].ItemList1 = {}
        self.m_RewardList[key].ItemList2 = {}
        self:AddItemData(self.m_RewardList[key].ItemList,data.ItemRewards1)
        self:AddItemData(self.m_RewardList[key].ItemList,data.ItemRewards2)
        self:AddItemData(self.m_RewardList[key].ItemList1,data.ItemRewards1)
        self:AddItemData(self.m_RewardList[key].ItemList2,data.ItemRewards2)
    end)
end

function LuaDaFuWongTongXingZhengWnd:AddItemData(list,itemdata)
    if not itemdata or not list then return end
    for i = 0 ,itemdata.Length - 1 do
        local data = {}
        data.ItemID = itemdata[i][0]
        data.Count = itemdata[i][1]
        data.IsBind = itemdata[i][2]
        data.weight = itemdata[i][3]
        table.insert(list,data)
    end
end
-- 初始化经验等级
function LuaDaFuWongTongXingZhengWnd:InitLevel()
    local curMaxExp = self.m_RewardList[math.min(math.max(self.m_CurLv + 1,2),self.m_MaxLv)].needExp
    self.LvLable.text = self.m_CurLv
    self.ExpLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_TongXingZheng_LeftExp", curMaxExp - self.m_Exp)
    self.ExpSlider.value = self.m_Exp / curMaxExp
    if self.m_CurLv >= self.m_MaxLv then
        self.ExpLabel.text = LocalString.GetString("已满级")
        self.ExpLabel.transform:Find("ExpIcon").gameObject:SetActive(false)
        self.ExpSlider.value = 1
    end
    
end
-- 奖励信息展示
function LuaDaFuWongTongXingZhengWnd:InitTable()
    self:InitAllData()
    self:ShowPage()
    self:UpdatePageLabel()
end

function LuaDaFuWongTongXingZhengWnd:InitAllData()
    self.m_AllData = {}
    local RewardInfo = LuaDaFuWongMgr.TongXingZhengInfo and LuaDaFuWongMgr.TongXingZhengInfo.hasReward
    for k,v in pairs(self.m_RewardList) do
        self.m_AllData[k] = {}
        self.m_AllData[k].item1 = self.m_RewardList[k].ItemList1[1]
        self.m_AllData[k].item2 = self.m_RewardList[k].ItemList2[1]
        self.m_AllData[k].item1Lock = self.m_CurLv < k
        self.m_AllData[k].item2Lock = not self.m_HasUnlock or self.m_CurLv < k
        self.m_AllData[k].item1HasReceive = RewardInfo and RewardInfo[k] and (RewardInfo[k] == EnumRewardType.All or RewardInfo[k] == EnumRewardType.Normal)
        self.m_AllData[k].item2HasReceive = RewardInfo and RewardInfo[k] and (RewardInfo[k] == EnumRewardType.All or RewardInfo[k] == EnumRewardType.Vip)
        self.m_AllData[k].isSpecial = k % self.m_OnePageNum == 0
    end

end


function LuaDaFuWongTongXingZhengWnd:ShowPage()
    local page = nil
    local fillAmountList = nil
    if self.m_CurPage % 2 == 0 then
        page = self.FirstPage
        self.FirstPage.gameObject:SetActive(true)
        self.SecondPage.gameObject:SetActive(false)
        fillAmountList = self.m_FirstLineFill
    else
        page = self.SecondPage
        self.FirstPage.gameObject:SetActive(false)
        self.SecondPage.gameObject:SetActive(true)
        fillAmountList = self.m_SecondLineFill
    end
    local line = page.transform:Find("BG/line"):GetComponent(typeof(UITexture))
    local startIndex = (self.m_CurPage - 1) * self.m_OnePageNum
    local lineIndex = math.max(0,math.min((self.m_CurLv - startIndex),6)) + 1
    local itemList = page.transform:Find("Item")
    line.fillAmount = fillAmountList[lineIndex]
    for i = 1,self.m_OnePageNum do
        local go = itemList.transform:GetChild(i - 1)
        local data = self.m_AllData[startIndex + i]
        local Item1 = go.transform:Find("ItemCell1").gameObject
        local Item2 = go.transform:Find("ItemCell2").gameObject
        local Node = go.transform:Find("Node")
        Node.transform:Find("lv"):GetComponent(typeof(UILabel)).text = startIndex + i
        local bgTitle = Node.transform:Find("bgtitle"):GetComponent(typeof(UITexture))
        local texture = bgTitle.transform:Find("Texture"):GetComponent(typeof(UITexture))
        local Heightlight = bgTitle.transform:Find("Higlight")
        if self.m_CurLv < startIndex + i then
            bgTitle.color = NGUIText.ParseColor("707070", 0)
            texture.color = NGUIText.ParseColor("707070", 0)
            texture.gameObject:SetActive(true)
            Heightlight.gameObject:SetActive(false)
        elseif self.m_CurLv == startIndex + i then
            Heightlight.gameObject:SetActive(true)
        else
            bgTitle.color = NGUIText.ParseColor("C18D81", 0)
            texture.color = NGUIText.ParseColor("C18D81", 0)
            texture.gameObject:SetActive(true)
            Heightlight.gameObject:SetActive(false)
        end
        self:InitItemCell(Item1,data.item1,data.item1Lock,data.item1HasReceive,startIndex + i,1)
        self:InitItemCell(Item2,data.item2,data.item2Lock,data.item2HasReceive,startIndex + i,2)
    end
end

function LuaDaFuWongTongXingZhengWnd:InitItemCell(go,itemdata,isLock,isGet,level,index)
    if not go or not itemdata then return end
    local texture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemdata.ItemID)
	go.transform:Find("Checkmark").gameObject:SetActive(isGet)
    go.transform:Find("LockedSprite").gameObject:SetActive(isLock)
    local cell = go.transform:Find("BGSprite")
    local fx = go.transform:Find("CUIFx"):GetComponent(typeof(CUIFx))
    fx:DestroyFx()
    if not isLock and not isGet then
        fx.gameObject:SetActive(true)
        LuaTweenUtils.DOKill(fx.transform, false)
        fx.transform.localPosition = Vector3(0, 0, 0)
        local b = NGUIMath.CalculateRelativeWidgetBounds(cell)
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
        fx.transform.localPosition = waypoints[0]
        fx:LoadFx("Fx/UI/Prefab/UI_kuang_blue02.prefab")
	    LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, -1, Ease.Linear)
        go.transform:Find("SpecialBg/Highlight").gameObject:SetActive(true)
    else
        fx.gameObject:SetActive(false)
        go.transform:Find("SpecialBg/Highlight").gameObject:SetActive(false)
    end
    
    go.transform:Find("BindSprite").gameObject:SetActive(itemdata.IsBind == 1)
    local amountLabel = go.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.gameObject:SetActive(itemdata.Count > 1)
    amountLabel.text = itemdata.Count
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if not isLock and not isGet then
            -- 领取奖励
            Gac2Gas.RequestGetDaFuWengReward(level,index)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemdata.ItemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        end
	end)
end
function LuaDaFuWongTongXingZhengWnd:UpdateTongXingZhengInfo()
    if LuaDaFuWongMgr.TongXingZhengInfo then 
        local dataInfo = LuaDaFuWongMgr.TongXingZhengInfo
        self.m_HasUnlock = dataInfo.vip == 1       -- 是否已经解锁
        self.m_CurLv = dataInfo.lv
        self.m_Exp = dataInfo.curExp
        self.Special.transform:Find("Unlock").gameObject:SetActive(dataInfo.vip ~= 1)
        self.Special.transform:Find("BG").gameObject:SetActive(dataInfo.vip == 1)
    end
    self:InitLevel()
    self:InitTable()
    self.Alert:SetActive(LuaDaFuWongMgr:TongXingZhengReward())
    self.UnLockButton.gameObject:SetActive(not self.m_HasUnlock)
    local startStamp = CServerTimeMgr.Inst:GetTimeStampByStr(DaFuWeng_Setting.GetData().StartBuyLevelTime)
    if startStamp <= CServerTimeMgr.Inst.timeStamp and self.m_CurLv < self.m_MaxLv then
        self.BuyLevelBtn.gameObject:SetActive(true)
    else
        self.BuyLevelBtn.gameObject:SetActive(false)
    end
end

function LuaDaFuWongTongXingZhengWnd:UpdatePageLabel()
    local maxPage = math.floor(self.m_MaxLv / self.m_OnePageNum)
    self.PageLabel.text = SafeStringFormat3("%d/%d",self.m_CurPage,maxPage)
    self.BackArrow.Enabled = self.m_CurPage > 1
    self.NextArrow.Enabled = self.m_CurPage < maxPage
end
--@region UIEvent

function LuaDaFuWongTongXingZhengWnd:OnBackArrowClick()
    local targetPage = math.max(1,self.m_CurPage - 1)
    self:DoPageAni(targetPage)
end

function LuaDaFuWongTongXingZhengWnd:OnNextArrowClick()
    local maxPage = math.floor(self.m_MaxLv / self.m_OnePageNum)
    local targetPage = math.min(maxPage,self.m_CurPage + 1)
    self:DoPageAni(targetPage)
end

function LuaDaFuWongTongXingZhengWnd:OnFocusBtnClick()
    local targetPage = math.floor((self.m_CurLv - 0.5) / self.m_OnePageNum) + 1
    self:DoPageAni(targetPage)
end

function LuaDaFuWongTongXingZhengWnd:DoPageAni(targetPage)
    if self.m_IsInAni then return end
    if targetPage > self.m_CurPage then
        self.m_Anim["dafuwongtongxingzhengwnd_zuofanyou"].time = 0
        self.m_Anim:Play("dafuwongtongxingzhengwnd_zuofanyou")
    elseif targetPage < self.m_CurPage then
        self.m_Anim["dafuwongtongxingzhengwnd_youfanzuo"].time = 0
        self.m_Anim:Play("dafuwongtongxingzhengwnd_youfanzuo")
    end
    self.m_IsInAni = true
    if self.m_ShowPageTick then UnRegisterTick(self.m_ShowPageTick) self.m_ShowPageTick = nil end
    self.m_ShowPageTick = RegisterTickOnce(function ()
        self.m_CurPage = targetPage
        self:ShowPage()
        self:UpdatePageLabel()
        self.m_IsInAni = false
    end,200)
end


function LuaDaFuWongTongXingZhengWnd:OnHelpBtnClick()
    g_MessageMgr:ShowMessage("DaFuWeng_TongXingZheng_RuleTip")
end

function LuaDaFuWongTongXingZhengWnd:OnBuyLevelBtnClick()
    local iconPath = "UI/Texture/Transparent/Material/businessmainwnd_jinbi.mat"
    local rewardList = {}
    for k,v in pairs(self.m_RewardList) do
        rewardList[k] = {}
        rewardList[k].needExp = v.needExp
        if self.m_HasUnlock then
            rewardList[k].ItemList = v.ItemList
        else
            rewardList[k].ItemList = v.ItemList1
        end
    end
    local oneJadeAddValue = DaFuWeng_Setting.GetData().OneJadeAddValue
    local callBack = function(cost,exp,level) 
        CUIManager.CloseUI(CLuaUIResources.CommonTongXingZhengBuyLevelWnd)
        if CClientMainPlayer.Inst then
            if CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade < cost then
                MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function ()
                    CShopMallMgr.ShowChargeWnd()
                end), nil, nil, nil, false)
            else
                Gac2Gas.RequestBuyDaFuWengLevel(level)
            end
        end
    end

    LuaCommonTongXingZhengBuyLevelMgr:ShowBuyLevelWnd(self.m_CurLv,self.m_MaxLv,self.m_Exp,rewardList,oneJadeAddValue,iconPath,callBack)
end
function LuaDaFuWongTongXingZhengWnd:OnDragPage(go,delta)

    local moveDeg = 57.2958*math.asin(math.max(-1,math.min(delta.x/400,1)))
    if moveDeg > 5 then
        self:OnBackArrowClick()
    elseif moveDeg < -5 then
        self:OnNextArrowClick()
    end
end

function LuaDaFuWongTongXingZhengWnd:OnUnLockButtonClick()
    CUIManager.ShowUI(CLuaUIResources.DaFuWongTongXingZhengUnLockWnd)
end

function LuaDaFuWongTongXingZhengWnd:OnReceiveButtonClick()
    if LuaDaFuWongMgr:TongXingZhengReward() then
        Gac2Gas.RequestGetDaFuWengReward(0,0)
    else
        g_MessageMgr:ShowMessage("DaFuWeng_TongXingZheng_NoReward")
    end
    
end


function LuaDaFuWongTongXingZhengWnd:OnSpecialClick()
    if self.m_HasUnlock then return end
    CUIManager.ShowUI(CLuaUIResources.DaFuWongTongXingZhengUnLockWnd)
end


--@endregion UIEvent

function LuaDaFuWongTongXingZhengWnd:OnEnable()
    g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate",self,"UpdateTongXingZhengInfo")
end

function LuaDaFuWongTongXingZhengWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnTongXingZhengDataUpdate",self,"UpdateTongXingZhengInfo")
    if self.m_ShowPageTick then UnRegisterTick(self.m_ShowPageTick) self.m_ShowPageTick = nil end
end
