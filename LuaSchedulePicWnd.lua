local CScheduleMgr=import "L10.Game.CScheduleMgr"
local ClientAction = import "L10.UI.ClientAction"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local CUICenterOnChild=import "L10.UI.CUICenterOnChild"

CLuaSchedulePicWnd=class()

RegistClassMember(CLuaSchedulePicWnd, "m_SchedulePic")
RegistClassMember(CLuaSchedulePicWnd, "m_SchedulePicRoot")
RegistClassMember(CLuaSchedulePicWnd, "m_Indicator")
RegistClassMember(CLuaSchedulePicWnd, "m_IndicatorGrid")
RegistClassMember(CLuaSchedulePicWnd, "m_NextArrow")
RegistClassMember(CLuaSchedulePicWnd, "m_LastArrow")
RegistClassMember(CLuaSchedulePicWnd, "m_OkButton")

RegistClassMember(CLuaSchedulePicWnd, "m_CurRollBannerIndex")
RegistClassMember(CLuaSchedulePicWnd, "m_SelectActivityId")
RegistClassMember(CLuaSchedulePicWnd, "m_SelectActivityAction")
RegistClassMember(CLuaSchedulePicWnd, "m_SchedulePicList")

RegistClassMember(CLuaSchedulePicWnd, "m_Angles")
RegistClassMember(CLuaSchedulePicWnd, "m_DeltaDegree")

function CLuaSchedulePicWnd:Awake()
    self.m_SchedulePic = self.transform:Find("Anchor/Pic").gameObject
    self.m_SchedulePicRoot = self.transform:Find("Anchor/Pics").gameObject
    self.m_Indicator = self.transform:Find("Anchor/Indicator").gameObject
    self.m_IndicatorGrid = self.transform:Find("Anchor/IndicatorGrid"):GetComponent(typeof(UIGrid))
    self.m_NextArrow = self.transform:Find("Anchor/Buttons/NextArrow").gameObject
    self.m_LastArrow = self.transform:Find("Anchor/Buttons/LastArrow").gameObject
    self.m_OkButton = self.transform:Find("Anchor/Buttons/OkButton"):GetComponent(typeof(CButton))

    UIEventListener.Get(self.m_NextArrow).onClick=DelegateFactory.VoidDelegate(function(go)
        self:UpdatePage(1)
    end)

    UIEventListener.Get(self.m_LastArrow).onClick=DelegateFactory.VoidDelegate(function(go)
        self:UpdatePage(-1)
    end)

    UIEventListener.Get(self.m_OkButton.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnOkButtonClick()
    end)
end

function CLuaSchedulePicWnd:Init()
    self.m_SchedulePic:SetActive(false)
    self.m_Indicator:SetActive(false)
 
    self.m_SchedulePicList = {}
    for i = 1, #CLuaScheduleMgr.m_PicList do
        local schedulePic = NGUITools.AddChild(self.m_SchedulePicRoot.gameObject, self.m_SchedulePic)
        local indicator = NGUITools.AddChild(self.m_IndicatorGrid.gameObject, self.m_Indicator)
        schedulePic:SetActive(true)
        indicator:SetActive(true)
        self:InitSchedulePic(schedulePic:GetComponent(typeof(CUITexture)), CLuaScheduleMgr.m_PicList[i])
    end
    self.m_IndicatorGrid:Reposition()

    self.m_Angles = {}
    self.m_DeltaDegree = math.floor(360 / #self.m_SchedulePicList)
    self.m_CurRollBannerIndex = CLuaScheduleMgr.m_PicScheduleIndex
    self:UpdatePage(0)
end

function CLuaSchedulePicWnd:InitSchedulePic(schedulePic, info)
    self.m_SelectActivityId = info.activityId
    if self.m_SelectActivityId then
        local scheduleData = Task_Schedule.GetData(self.m_SelectActivityId)
        if scheduleData then
            schedulePic:LoadMaterial(scheduleData.PicFull)
        end
    else
        --propaganda类型
        local alertData = Gameplay_PropagandaAlert.GetData(info.alertId)
        if alertData then
            schedulePic:LoadMaterial(alertData.ActivityPicturePath)
        end
    end
    UIEventListener.Get(schedulePic.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
        self:OnDrag(go, delta)
    end)
    UIEventListener.Get(schedulePic.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(go)
        self:OnDragEnd(go)
    end)
    UIEventListener.Get(schedulePic.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragStart(go)
    end)
    
    local pic = schedulePic:GetComponent(typeof(UITexture))
    local border = schedulePic.transform:Find("bg1"):GetComponent(typeof(UITexture))
    table.insert(self.m_SchedulePicList, {pic = pic, border = border})
end

function CLuaSchedulePicWnd:OnDrag(go, delta)
    local moveDeg = 57.2958 * math.asin(math.max(-1, math.min(delta.x / 400, 1)))
    for i, v in ipairs(self.m_Angles) do
        local angle = v + moveDeg
        self.m_Angles[i] = angle - math.floor(angle/360) * 360
    end
    self:UpdatePosition()
end

function CLuaSchedulePicWnd:OnDragStart(go)
    for i, v in ipairs(self.m_SchedulePicList) do
        LuaTweenUtils.DOKill(v.pic.transform, false)
    end
end

function CLuaSchedulePicWnd:OnDragEnd(go)
    local selectIndex = 0

    local index = go.transform:GetSiblingIndex() + 1
    local x = go.transform.localPosition.x
    local scale = go.transform.localScale.x

    --看划过的距离
    if x > -50 and x < 50 and scale > 0.7 then
        selectIndex = index
    elseif x < 0 then
        selectIndex = index + 1 > #self.m_Angles and index + 1 - #self.m_Angles or index + 1
    elseif x > 0 then
        selectIndex = index - 1 < 1 and index - 1 + #self.m_Angles or index - 1
    end

    self.m_CurRollBannerIndex = selectIndex
    self:UpdatePage(0)
end

function CLuaSchedulePicWnd:UpdatePosition(isTween)
    for i, v in ipairs(self.m_SchedulePicList) do
        local angle = self.m_Angles[i]
        angle = angle - math.floor(angle / 360) * 360
        local offset = math.sin(0.0174533 * angle) * 200
        local moveTo =  math.sin(0.0174533 * angle) * 200
        local scale = math.cos(0.0174533 * angle) * 0.15 + 0.85

        if isTween then
            LuaTweenUtils.TweenPositionX(v.pic.transform, moveTo, 0.3)
            LuaTweenUtils.TweenScaleTo(v.pic.transform, Vector3(scale, scale, 1), 0.3)
        else
            LuaUtils.SetLocalPositionX(v.pic.transform, offset)
            LuaUtils.SetLocalScale(v.pic.transform, scale, scale, 1)
        end

        v.pic.depth = math.floor(scale * 100) * 2
        -- v.pic.alpha = scale * scale > 0.95 and 1 or scale * scale * scale * scale
        v.pic.alpha = scale * scale > 0.95 and 1 or 1 - (1-scale) * (1-90/255) / (1-0.85)
        v.border.depth = v.pic.depth - 1
    end
end


function CLuaSchedulePicWnd:UpdateIndicator()
    for i = 1, self.m_IndicatorGrid.transform.childCount do
        local sprite = self.m_IndicatorGrid.transform:GetChild(i-1):GetComponent(typeof(UISprite))
        sprite.color = i == self.m_CurRollBannerIndex and Color.white or Color(0.3,0.5,0.8)
    end
end

function CLuaSchedulePicWnd:UpdateOkBtn()
    local info = CLuaScheduleMgr.m_PicList[self.m_CurRollBannerIndex]
    self.m_SelectActivityId = info.activityId
    if self.m_SelectActivityId then
        -- 设置按钮是否显示
        if not CLuaScheduleMgr.CanShowButton(info) then
            self.m_OkButton.gameObject:SetActive(false)
        elseif info.activityId == 42000287 then --城战帮会押镖
            self.m_OkButton.gameObject:SetActive(false)
        elseif info.activityId == 42000286 then --洞天守护灵
            self.m_OkButton.gameObject:SetActive(false)
        else
            self.m_OkButton.gameObject:SetActive(true)
        end
        local scheduleData = Task_Schedule.GetData(self.m_SelectActivityId)
        if scheduleData then
            -- 设置按钮文本
            if scheduleData.ButtonType == 0 then
                self.m_OkButton.Text = LocalString.GetString("参加")
            elseif scheduleData.ButtonType == 1 then
                self.m_OkButton.Text = LocalString.GetString("领取")
            elseif scheduleData.ButtonType == 2 then
                self.m_OkButton.Text = LocalString.GetString("查看")
            end
        end
    else
        --propaganda类型
        local alertData = Gameplay_PropagandaAlert.GetData(info.alertId)
        self.m_OkButton.gameObject:SetActive(alertData.ActivityButtonText and alertData.ActivityButtonText ~= "" or false)
        if alertData then
            self.m_OkButton.Text = alertData.ActivityButtonText
            self.m_SelectActivityAction = alertData.ActivityAction
        end
    end
end

function CLuaSchedulePicWnd:OnOkButtonClick()
    CUIManager.CloseUI(CLuaUIResources.SchedulePicWnd)
    CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
    if self.m_SelectActivityId then
        if not CLuaScheduleMgr.m_CrossServerActivityIds[self.m_SelectActivityId] and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer then
            g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
            return
        end
        CScheduleMgr.Inst:DoScheduleTask(self.m_SelectActivityId)
    else
        ClientAction.DoAction(self.m_SelectActivityAction)
    end
end

function CLuaSchedulePicWnd:UpdatePage(delta)
    self.m_CurRollBannerIndex = self.m_CurRollBannerIndex + delta
    if self.m_CurRollBannerIndex < 1 then
        self.m_CurRollBannerIndex = #CLuaScheduleMgr.m_PicList
    elseif self.m_CurRollBannerIndex > #CLuaScheduleMgr.m_PicList then
        self.m_CurRollBannerIndex = 1
    end

    for i, v in ipairs(self.m_SchedulePicList) do
        local degree = (i - self.m_CurRollBannerIndex) * self.m_DeltaDegree
        self.m_Angles[i] = degree - math.floor(degree / 360) * 360
    end

    self:UpdatePosition(true)
    self:UpdateOkBtn()
    self:UpdateIndicator()
end
