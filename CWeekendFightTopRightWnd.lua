-- Auto Generated!!
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local CWeekendFightTopRightWnd = import "L10.UI.CWeekendFightTopRightWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CWeekendFightTopRightWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.expandBtn).onClick = MakeDelegateFromCSFunction(this.OnExpandBtnClicked, VoidDelegate, this)
    this.template:SetActive(false)
    this:InitData()
    EventManager.BroadcastInternalForLua(EnumEventType.UpdateWeekendFightStatus, {true})
end
CWeekendFightTopRightWnd.m_OnExpandBtnClicked_CS2LuaHook = function (this, go) 
    local rotation = this.expandBtn.transform.localEulerAngles
    this.expandBtn.transform.localEulerAngles = Vector3(rotation.x, rotation.y, 180)

    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
CWeekendFightTopRightWnd.m_GetItemPos_CS2LuaHook = function (this, index) 
    if CWeekendFightMgr.Inst.ForceList.Count <= 4 then
        if index <= 2 then
            return Vector3(- this.width * (2 - index), 0, 0)
        else
            return Vector3(- this.width * (4 - index), - this.height, 0)
        end
    elseif CWeekendFightMgr.Inst.ForceList.Count <= 6 then
        if index <= 3 then
            return Vector3(- this.width * (3 - index), 0, 0)
        else
            return Vector3(- this.width * (6 - index), - this.height, 0)
        end
    else
        if index <= 4 then
            return Vector3(- this.width * (4 - index), 0, 0)
        else
            return Vector3(- this.width * (8 - index), - this.height, 0)
        end
    end
end
CWeekendFightTopRightWnd.m_InitData_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.fatherNode.transform)
    local selfForceInfo = CWeekendFightMgr.Inst:GetSelfForceInfo()
    do
        local i = 0
        while i < CWeekendFightMgr.Inst.ForceList.Count do
            local info = CWeekendFightMgr.Inst.ForceList[i]
            local node = NGUITools.AddChild(this.fatherNode, this.template)
            node:SetActive(true)
            node.transform.localPosition = this:GetItemPos(i + 1)
            CommonDefs.GetComponent_Component_Type(node.transform:Find("score"), typeof(UILabel)).text = tostring(info.score)
            if selfForceInfo ~= nil and selfForceInfo.force == info.force then
                CommonDefs.GetComponent_Component_Type(node.transform:Find("score"), typeof(UILabel)).color = Color.green
                node.transform:Find("selfbg").gameObject:SetActive(true)
            else
                node.transform:Find("selfbg").gameObject:SetActive(false)
            end
            CWeekendFightMgr.Inst:SetForceIcon(node.transform:Find("force").gameObject, info.force)

            local count = 0
            node.transform:Find("player1/Icon").gameObject:SetActive(false)
            node.transform:Find("player2/Icon").gameObject:SetActive(false)

            CommonDefs.DictIterate(CWeekendFightMgr.Inst.PlayerInfoDic, DelegateFactory.Action_object_object(function (___key, ___value) 
                local pair = {}
                pair.Key = ___key
                pair.Value = ___value
                if pair.Value.force == info.force then
                    count = count + 1
                    if count == 1 then
                        node.transform:Find("player1/Icon").gameObject:SetActive(true)
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("player1/Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CommonDefs.DictGetValue(CWeekendFightMgr.Inst.PlayerInfoDic, typeof(UInt64), pair.Key).clazz, CommonDefs.DictGetValue(CWeekendFightMgr.Inst.PlayerInfoDic, typeof(UInt64), pair.Key).gender, -1), false)
                    elseif count == 2 then
                        node.transform:Find("player2/Icon").gameObject:SetActive(true)
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("player2/Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CommonDefs.DictGetValue(CWeekendFightMgr.Inst.PlayerInfoDic, typeof(UInt64), pair.Key).clazz, CommonDefs.DictGetValue(CWeekendFightMgr.Inst.PlayerInfoDic, typeof(UInt64), pair.Key).gender, -1), false)
                    else
                        return
                    end
                end
            end))
            i = i + 1
        end
    end
end
