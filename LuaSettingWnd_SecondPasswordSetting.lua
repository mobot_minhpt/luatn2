local UIEventListener = import "UIEventListener"
local CUIResources = import "L10.UI.CUIResources"
local CUIManager = import "L10.UI.CUIManager"
local CSecondPwdConfirmWnd = import "L10.UI.CSecondPwdConfirmWnd"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumSecondaryPasswordOperationType = import "L10.Game.EnumSecondaryPasswordOperationType"

LuaSettingWnd_SecondPasswordSetting = class()

RegistClassMember(LuaSettingWnd_SecondPasswordSetting, "m_NotOpenGo")
RegistClassMember(LuaSettingWnd_SecondPasswordSetting, "m_OpenGo")
RegistClassMember(LuaSettingWnd_SecondPasswordSetting, "m_OpenPwdBtn")
RegistClassMember(LuaSettingWnd_SecondPasswordSetting, "m_ContentBtn")
RegistClassMember(LuaSettingWnd_SecondPasswordSetting, "m_ChangePwdBtn")
RegistClassMember(LuaSettingWnd_SecondPasswordSetting, "m_ClosePwdBtn")

function LuaSettingWnd_SecondPasswordSetting:Awake()
    self:InitComponents()
    self:Init()
    self:LoadData()
end

function LuaSettingWnd_SecondPasswordSetting:OnEnable()
    self:LoadData()
    g_ScriptEvent:AddListener("SyncSecondaryPasswordInfo", self, "OnSyncSecondaryPasswordInfo")
end

function LuaSettingWnd_SecondPasswordSetting:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSecondaryPasswordInfo", self, "OnSyncSecondaryPasswordInfo")
end

function LuaSettingWnd_SecondPasswordSetting:InitComponents()
    self.m_NotOpenGo = self.transform:Find("Settings/NotOpen").gameObject
    self.m_OpenGo = self.transform:Find("Settings/Open").gameObject
    self.m_OpenPwdBtn = self.m_NotOpenGo.transform:Find("OkButton").gameObject
    self.m_ContentBtn = self.m_OpenGo.transform:Find("ContentButton").gameObject
    self.m_ChangePwdBtn = self.m_OpenGo.transform:Find("ChangePwdButton").gameObject
    self.m_ClosePwdBtn = self.m_OpenGo.transform:Find("ClosePwdButton").gameObject
end

function LuaSettingWnd_SecondPasswordSetting:Init()
    UIEventListener.Get(self.m_OpenPwdBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClickOpenPwdButton()
    end)
    UIEventListener.Get(self.m_ContentBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClickContentButton()
    end)
    UIEventListener.Get(self.m_ChangePwdBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClickChangePwdButton()
    end)
    UIEventListener.Get(self.m_ClosePwdBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClickClosePwdButton()
    end)
    self:LoadData()
end

function LuaSettingWnd_SecondPasswordSetting:OnClickOpenPwdButton()
    Gac2Gas.RequestOpenSecondaryPassword()
end

function LuaSettingWnd_SecondPasswordSetting:OnClickContentButton()
    CUIManager.ShowUI(CUIResources.SecondPwdContentWnd)
end

function LuaSettingWnd_SecondPasswordSetting:OnClickChangePwdButton()
    CUIManager.ShowUI(CUIResources.SecondPwdChangeWnd)
end

function LuaSettingWnd_SecondPasswordSetting:OnClickClosePwdButton()
    CSecondPwdConfirmWnd.RequestCloseSecondPwd = true
    CUIManager.ShowUI(CUIResources.SecondPwdConfirmWnd)
end

function LuaSettingWnd_SecondPasswordSetting:LoadData()
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsSecondPwdOpened() then
        self.m_NotOpenGo:SetActive(false)
        self.m_OpenGo:SetActive(true)
    else
        self.m_NotOpenGo:SetActive(true)
        self.m_OpenGo:SetActive(false)
    end
end

function LuaSettingWnd_SecondPasswordSetting:OnSyncSecondaryPasswordInfo(tp)
    if tp[0] == EnumToInt(EnumSecondaryPasswordOperationType.eSet) or
            tp[0] == EnumToInt(EnumSecondaryPasswordOperationType.eClose) then
        self:LoadData()
    end
end
