-- Auto Generated!!
local CGuildChallengeMgr = import "L10.Game.CGuildChallengeMgr"
local CGuildChallengeWnd = import "L10.UI.CGuildChallengeWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumGuildChallengeStatus = import "L10.Game.EnumGuildChallengeStatus"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildChallengeWnd.m_Init_CS2LuaHook = function (this) 
    this.idArrow.enabled = false
    this.numArrow.enabled = false
    this.levelArrow.enabled = false

    this:OnNoGuildSelect()

    this.listView.onGuildSelect = MakeDelegateFromCSFunction(this.OnGuildSelect, MakeGenericClass(Action1, Int32), this)

    this.listView:Init()
end
CGuildChallengeWnd.m_OnNoGuildSelect_CS2LuaHook = function (this) 
    this.declareButton.Enabled = CGuildChallengeMgr.Inst.hasRightForChallenge
    this.acceptButton.Enabled = CGuildChallengeMgr.Inst.hasRightForChallenge
    this.rejectButton.Enabled = CGuildChallengeMgr.Inst.hasRightForChallenge
    this.acceptButton.gameObject:SetActive(false)
    this.rejectButton.gameObject:SetActive(false)
    this.declareButton.gameObject:SetActive(true)
end
CGuildChallengeWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.idButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.idButton).onClick, MakeDelegateFromCSFunction(this.OnIdButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.numButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.numButton).onClick, MakeDelegateFromCSFunction(this.OnNumButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.levelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.levelButton).onClick, MakeDelegateFromCSFunction(this.OnLevelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.searchButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.searchButton).onClick, MakeDelegateFromCSFunction(this.OnSearchButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.declareButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.declareButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnDeclareButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tipsButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tipsButton).onClick, MakeDelegateFromCSFunction(this.OnTipsButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.acceptButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acceptButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnAcceptButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.rejectButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rejectButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnRejectButtonClick, VoidDelegate, this), true)
end
CGuildChallengeWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.idButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.idButton).onClick, MakeDelegateFromCSFunction(this.OnIdButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.numButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.numButton).onClick, MakeDelegateFromCSFunction(this.OnNumButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.levelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.levelButton).onClick, MakeDelegateFromCSFunction(this.OnLevelButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.searchButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.searchButton).onClick, MakeDelegateFromCSFunction(this.OnSearchButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.declareButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.declareButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnDeclareButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.tipsButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tipsButton).onClick, MakeDelegateFromCSFunction(this.OnTipsButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.acceptButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acceptButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnAcceptButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.rejectButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rejectButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnRejectButtonClick, VoidDelegate, this), false)
end
CGuildChallengeWnd.m_OnGuildSelect_CS2LuaHook = function (this, index) 
    --Debug.Log("index " + index);
    local status = EnumGuildChallengeStatus.eNone
    if this.listView.searchGuild and index < CGuildChallengeMgr.Inst.searchGuildList.Count then
        status = CGuildChallengeMgr.Inst.searchGuildList[index].status
    elseif not this.listView.searchGuild and index < CGuildChallengeMgr.Inst.guildList.Count then
        status = CGuildChallengeMgr.Inst.guildList[index].status
    end
    this.declareButton.gameObject:SetActive(status ~= EnumGuildChallengeStatus.eInviting)
    this.acceptButton.gameObject:SetActive(status == EnumGuildChallengeStatus.eInviting)
    this.rejectButton.gameObject:SetActive(status == EnumGuildChallengeStatus.eInviting)
end
CGuildChallengeWnd.m_OnIdButtonClick_CS2LuaHook = function (this, go) 
    if this.listView.searchGuild then
        return
    end
    if not this.idArrow.enabled then
        this.idArrow.enabled = true
    elseif this.idArrow.transform.localRotation.z == 0 then
        Extensions.SetLocalRotationZ(this.idArrow.transform, 180)
    else
        Extensions.SetLocalRotationZ(this.idArrow.transform, 0)
    end
    this.numArrow.enabled = false
    this.levelArrow.enabled = false
    CGuildChallengeMgr.Inst:SortGuildListById(this.idArrow.transform.localRotation.z == 0)
    this.listView:Init()
    this:OnNoGuildSelect()
end
CGuildChallengeWnd.m_OnNumButtonClick_CS2LuaHook = function (this, go) 
    if this.listView.searchGuild then
        return
    end
    if not this.numArrow.enabled then
        this.numArrow.enabled = true
    elseif this.numArrow.transform.localRotation.z == 0 then
        Extensions.SetLocalRotationZ(this.numArrow.transform, 180)
    else
        Extensions.SetLocalRotationZ(this.numArrow.transform, 0)
    end
    this.idArrow.enabled = false
    this.levelArrow.enabled = false
    CGuildChallengeMgr.Inst:SortGuildListByNum(this.numArrow.transform.localRotation.z == 0)
    this.listView:Init()
    this:OnNoGuildSelect()
end
CGuildChallengeWnd.m_OnLevelButtonClick_CS2LuaHook = function (this, go) 
    if this.listView.searchGuild then
        return
    end
    if not this.levelArrow.enabled then
        this.levelArrow.enabled = true
    elseif this.levelArrow.transform.localRotation.z == 0 then
        Extensions.SetLocalRotationZ(this.levelArrow.transform, 180)
    else
        Extensions.SetLocalRotationZ(this.levelArrow.transform, 0)
    end
    this.idArrow.enabled = false
    this.numArrow.enabled = false
    CGuildChallengeMgr.Inst:SortGuildListByLevel(this.levelArrow.transform.localRotation.z == 0)
    this.listView:Init()
    this:OnNoGuildSelect()
end
CGuildChallengeWnd.m_OnSearchButtonClick_CS2LuaHook = function (this, go) 
    if System.String.IsNullOrEmpty(this.searchInput.value) then
        this.listView:Init()
        this:OnNoGuildSelect()
    else
        CGuildChallengeMgr.Inst:GetSearchGuild(this.searchInput.value)
        this.listView:InitSearchGuild()
        this:OnNoGuildSelect()
    end
end
CGuildChallengeWnd.m_OnDeclareButtonClick_CS2LuaHook = function (this, go) 
    local info = this:GetGuildInfo()
    if info ~= nil then
        if info.status ~= EnumGuildChallengeStatus.eNone then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("无法对该帮会进行宣战。"))
        else
            CGuildChallengeMgr.Inst.selectGuildId = info.guildId
            Gac2Gas.GC_RequestOpenChallengeGuildWnd(info.guildId)
        end
    end
end
CGuildChallengeWnd.m_GetGuildInfo_CS2LuaHook = function (this) 
    if this.listView.selectIndex < 0 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个帮会！"))
        return nil
    end
    local info = nil
    if this.listView.searchGuild and this.listView.selectIndex < CGuildChallengeMgr.Inst.searchGuildList.Count then
        info = CGuildChallengeMgr.Inst.searchGuildList[this.listView.selectIndex]
    elseif not this.listView.searchGuild and this.listView.selectIndex < CGuildChallengeMgr.Inst.guildList.Count then
        info = CGuildChallengeMgr.Inst.guildList[this.listView.selectIndex]
    end
    return info
end
CGuildChallengeWnd.m_OnAcceptButtonClick_CS2LuaHook = function (this, go) 
    local info = this:GetGuildInfo()
    if info ~= nil and info.status == EnumGuildChallengeStatus.eInviting then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GC_ACCEPT_REQUEST_CONFIRM", info.guildName), DelegateFactory.Action(function () 
            Gac2Gas.GC_AcceptChallenge(info.guildId)
        end), nil, nil, nil, false)
    end
end
CGuildChallengeWnd.m_OnRejectButtonClick_CS2LuaHook = function (this, go) 
    local info = this:GetGuildInfo()
    if info ~= nil and info.status == EnumGuildChallengeStatus.eInviting then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GC_REJECT_REQUEST_CONFIRM", info.guildName), DelegateFactory.Action(function () 
            Gac2Gas.GC_RejectChallenge(info.guildId)
        end), nil, nil, nil, false)
    end
end
