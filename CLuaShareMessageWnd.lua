local ShareBoxMgr=import "L10.UI.ShareBoxMgr"
local EShareType=import "L10.UI.EShareType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

CLuaShareMessageWnd=class()
RegistClassMember(CLuaShareMessageWnd,"m_ShareButton")

function CLuaShareMessageWnd:Init()
    self.m_ShareButton=self.transform:Find("Anchor/Button").gameObject
    local function OnClick(go)
        self:Share()
    end
    CommonDefs.AddOnClickListener(self.m_ShareButton,DelegateFactory.Action_GameObject(OnClick),false)
end

function CLuaShareMessageWnd:Share()
    ShareBoxMgr.OpenShareBox(EShareType.Trade,0,{});
end

return CLuaShareMessageWnd