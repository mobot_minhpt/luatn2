BeginSample("CLuaUIResources")

local UIParentType=import "L10.UI.UIParentType"
local CUIModule=import "L10.UI.CUIModule"
CLuaUIResources={}
local __LuaUIResources={
	--世界事件
	WorldEventShareWnd = {"UI/Prefab/WorldEvent/WorldEventShareWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	WorldEventHorShowWnd = {"UI/Prefab/WorldEvent/WorldEventHorShowWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	WorldEventVerShowWnd = {"UI/Prefab/WorldEvent/WorldEventVerShowWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	--世界事件 奇异树
	WorldQiyishuWnd	= {"UI/Prefab/WorldEvent/WorldQiyishuWnd.prefab",UIParentType.POP_UI_ROOT,false,false},

	ShenbingDuliniangWnd = {"UI/Prefab/Shenbing/ShenbingDuliniangWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	ShenbingEnterWnd = {"UI/Prefab/Shenbing/ShenbingEnterWnd.prefab",UIParentType.POP_UI_ROOT,false,true},

	--DuanWuDaZuoZhanResultWnd = {"UI/Prefab/Festival_DuanWu/DuanWu/DuanWuDaZuoZhanResultWnd.prefab",UIParentType.POP_UI_ROOT,false,false},

	-- 兵器谱
    BQPCommitEquipWnd = { "UI/Prefab/BingQiPu/BQPCommitEquipWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
    BQPConfirmCommitWnd = { "UI/Prefab/BingQiPu/BQPConfirmCommitWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
    BQPEquipInfoWnd = { "UI/Prefab/BingQiPu/BQPEquipInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BQPSponsorListWnd = {"UI/Prefab/BingQiPu/BQPSponsorListWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	BQPMySponsorWnd = {"UI/Prefab/BingQiPu/BQPMySponsorWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	BQPPromotionWnd = {"UI/Prefab/BingQiPu/BQPPromotionWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	BQPResultWnd = {"UI/Prefab/BingQiPu/BQPResultWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	BQPDonateWnd = {"UI/Prefab/BingQiPu/BQPDonateWnd.prefab",UIParentType.POP_UI_ROOT,false,false},

	--ZhuojiResultInfoWnd = { "UI/Prefab/Festival_LiuYi/LiuYi/ZhuojiResultInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--ZhuojiSingUpWnd = { "UI/Prefab/Festival_LiuYi/LiuYi/ZhuojiSingUpWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--ZhuojiInfoWnd = { "UI/Prefab/Festival_LiuYi/LiuYi/ZhuojiInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--ZhuojiTopRightWnd = { "UI/Prefab/Festival_LiuYi/LiuYi/ZhuojiTopRightWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--装备鉴定技能
	EquipIdentifySkillWnd = { "UI/Prefab/Equip/EquipIdentifySkillWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	EquipIdentifySkillItemChooseWnd = { "UI/Prefab/Equip/EquipIdentifySkillItemChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	Exchange125SkillBookWnd = { "UI/Prefab/Skill/Exchange125SkillBookWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	ScheduleWnd={ "UI/Prefab/Schedule/ScheduleWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SchedulePicWnd={ "UI/Prefab/Schedule/SchedulePicWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ScheduleInfoWnd = { "UI/Prefab/Schedule/ScheduleInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	JieRiGroupScheduleWnd = { "UI/Prefab/Schedule/JieRiGroupScheduleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ScheduleAlertWnd = { "UI/Prefab/Schedule/ScheduleAlertWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	QMPKConfigMainWnd = { "UI/Prefab/QuanMinPK/QMPKConfigMainWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	QMPKInfoWnd = { "UI/Prefab/QuanMinPK/QMPKInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	-- QMPKInfoWnd2 = { "UI/Prefab/QuanMinPK_1/QMPKInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKMeiRiYiZhanWnd = { "UI/Prefab/QuanMinPK/QMPKMeiRiYiZhanWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKPlayerInfoWnd = { "UI/Prefab/QuanMinPK/QMPKPlayerInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKSelfZhanDuiWnd = { "UI/Prefab/QuanMinPK/QMPKSelfZhanDuiWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	ShenBingZhuZaoWnd = { "UI/Prefab/Shenbing/ShenBingZhuZaoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
    ShenBingPeiYangWnd = { "UI/Prefab/Shenbing/ShenBingPeiYangWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShenBingSubmitWnd = { "UI/Prefab/Shenbing/ShenBingSubmitWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShenBingExchangeQyWnd = { "UI/Prefab/Shenbing/ShenBingExchangeQyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShenBingExchangeQyBuyWnd = { "UI/Prefab/Shenbing/ShenBingExchangeQyBuyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenBingColorationWnd = { "UI/Prefab/Shenbing/ShenBingColorationWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShenBingColorComposeWnd = { "UI/Prefab/Shenbing/ShenBingColorComposeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShenBingColorChooseWnd = { "UI/Prefab/Shenbing/ShenBingColorChooseWnd.Prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenBingRestoreWnd = { "UI/Prefab/Shenbing/ShenBingRestoreWnd.Prefab", UIParentType.POP_UI_ROOT, false, true},

	-- QMPKPlayerInfoWnd= { "UI/Prefab/QuanMinPK/QMPKPlayerInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKCreateZhanDuiWnd={ "UI/Prefab/QuanMinPK/QMPKCreateZhanDuiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKMatchingWnd={ "UI/Prefab/QuanMinPK/QMPKMatchingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QMPKAgendaWnd = { "UI/Prefab/QuanMinPK/QMPKAgendaWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	QMPKCurrentBattleStatusWnd={ "UI/Prefab/QuanMinPK/QMPKCurrentBattleStatusWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKJiXiangWuWnd={ "UI/Prefab/QuanMinPK/QMPKJiXiangWuWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKJiXiangWuFeedWnd={ "UI/Prefab/QuanMinPK/QMPKJiXiangWuFeedWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	QMPKJingCaiWnd={ "UI/Prefab/QuanMinPK/QMPKJingCaiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKJingCaiContentWnd={ "UI/Prefab/QuanMinPK/QMPKJingCaiContentWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKJingCaiConfirmWnd={ "UI/Prefab/QuanMinPK/QMPKJingCaiConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKSearchRecruitWnd = { "UI/Prefab/QuanMinPK/QMPKSearchRecruitWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKZhanDuiSearchWnd = { "UI/Prefab/QuanMinPK/QMPKZhanDuiSearchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKBattleDataWnd = { "UI/Prefab/QuanMinPK/QMPKBattleDataWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMPKServerChosenWnd = { "UI/Prefab/QuanMinPK/QMPKServerChosenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QMPKRegisterWnd = { "UI/Prefab/QuanMinPK/QMPKRegisterwnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKBattleWnd = { "UI/Prefab/QuanMinPK/QMPKBattleWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKStarWnd = { "UI/Prefab/QuanMinPK/QMPKStarWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKZhanDuiSettingWnd = { "UI/Prefab/QuanMinPK/QMPKZhanDuiSettingWnd.prefab", UIParentType.POP_UI_ROOT, false,true},
	QMPKModifySloganWnd = { "UI/Prefab/QuanMinPK/QMPKModifySloganWnd.prefab", UIParentType.POP_UI_ROOT, false,false},
	QMPKTopFourWnd = { "UI/Prefab/QuanMinPK/QMPKTopFourWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKStatueBuildWnd = { "UI/Prefab/QuanMinPK/QMPKStatueBuildWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKGameVideoWnd = { "UI/Prefab/QuanMinPK/QMPKGameVideoWnd.prefab", UIParentType.POP_UI_ROOT, false,true},
	QMPKMatchRecordWnd = { "UI/Prefab/QuanMinPK/QMPKMatchRecordWnd.prefab", UIParentType.POP_UI_ROOT, false,true},
	QMPKRecruitWnd = { "UI/Prefab/QuanMinPK/QMPKRecruitWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKGetItemWnd = { "UI/Prefab/QuanMinPK/QMPKGetItemWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKQieCuoWnd = { "UI/Prefab/QuanMinPK/QMPKQieCuoWnd.prefab", UIParentType.POP_UI_ROOT, false,true},
	QMPKTaoTaiSaiMatchRecordWnd = { "UI/Prefab/QuanMinPK/QMPKTaoTaiSaiMatchRecordWnd.prefab", UIParentType.POP_UI_ROOT, false,true},
	QMPKSearchPlayerResultWnd = { "UI/Prefab/QuanMinPK/QMPKSearchPlayerResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKShareWnd = { "UI/Prefab/QuanMinPK/QMPKShareWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKJiXiangWuShareWnd = { "UI/Prefab/QuanMinPK/QMPKJiXiangWuShareWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPKDamageWnd = { "UI/Prefab/QuanMinPK/QMPKDamageWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	QMPKAntiprofessionAdjustWnd = { "UI/Prefab/QuanMinPK/QMPKAntiprofessionAdjustWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKWuXingAdjustWnd = { "UI/Prefab/QuanMinPK/QMPKWuXingAdjustWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKSelectServerWnd = { "UI/Prefab/QuanMinPK/QMPKSelectServerWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKEnterServerWnd = { "UI/Prefab/QuanMinPK/QMPKEnterServerWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKRewardWnd = { "UI/Prefab/QuanMinPK/QMPKRewardWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKCertificationWnd = { "UI/Prefab/QuanMinPK/QMPKCertificationWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKCertificationConfirmWnd = { "UI/Prefab/QuanMinPK/QMPKCertificationConfirmWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKLingShouLearnWnd = { "UI/Prefab/QuanMinPK/QMPKLingShouLearnWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKBattleBeforeWnd = { "UI/Prefab/QuanMinPK/QMPKBattleBeforeWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QMPKTeamMemberDetailWnd = { "UI/Prefab/QuanMinPK/QMPKTeamMemberDetailWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	CommonShare2PersonalSpaceWnd = { "UI/Prefab/Share/CommonShare2PersonalSpaceWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	MajiuWnd = { "UI/Prefab/House/MajiuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--WuCaiShaBingEnterWnd = { "UI/Prefab/Festival_ShuJia/WuCaiShaBing/WuCaiShaBingEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--WuCaiShaBingDetailWnd = { "UI/Prefab/Festival_ShuJia/WuCaiShaBing/WuCaiShaBingDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--WuCaiShaBingBattleInfoWnd = { "UI/Prefab/Festival_ShuJia/WuCaiShaBing/WuCaiShaBingBattleInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--WuCaiShaBingResultWnd = { "UI/Prefab/Festival_ShuJia/WuCaiShaBing/WuCaiShaBingResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--WuCaiShaBingStateWnd = { "UI/Prefab/Festival_ShuJia/WuCaiShaBing/WuCaiShaBingStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	FightingSpiritFavorWnd = { "UI/Prefab/FightingSpirit/FightingSpiritFavorWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	FightingSpiritFavorTeamWnd = { "UI/Prefab/FightingSpirit/FightingSpiritFavorTeamWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	FightingSpiritFavorRankWnd = { "UI/Prefab/FightingSpirit/FightingSpiritFavorRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	FightingSpiritOtherTeamWnd = { "UI/Prefab/FightingSpirit/FightingSpiritOtherTeamWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FightingSpiritCheerWnd = { "UI/Prefab/FightingSpirit/FightingSpiritCheerWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	MergeBattleWnd={ "UI/Prefab/MergeBattle/MergeBattleWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CrossSXDDZChampionConfirmWnd = { "UI/Prefab/CrossSXDDZChampionConfirm.prefab", UIParentType.POP_UI_ROOT, false, false},
	PopupRecallWnd={ "UI/Prefab/MergeBattle/PopupRecallWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	PointsComparedWnd={ "UI/Prefab/MergeBattle/PointsComparedWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	FriendStatusWnd={ "UI/Prefab/Friend/FriendStatusWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	KouDaoJingYingSettingWnd = { "UI/Prefab/KouDao/KouDaoJingYingSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},--寇岛战况窗口
	GuildLeagueWeeklyInfoWnd = { "UI/Prefab/Guild/GuildLeagueWeeklyInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TianMenShanZhuLiSettingWnd = { "UI/Prefab/TianMenShan/TianMenShanZhuLiSettingWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	BangHuiJingYingSettingWnd = { "UI/Prefab/BangHuiJingYingSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	CCDanMuWnd = { "UI/Prefab/CCLive/CCDanmuWnd.prefab", UIParentType.POP_UI_ROOT, false, false},--CC直播弹幕
	CCLiveWnd = { "UI/Prefab/CCLive/CCLiveWnd.prefab", UIParentType.POP_UI_ROOT, false, false}, --CC直播窗口
	CCLiveListWnd = { "UI/Prefab/CCLive/CCLiveListWnd.prefab", UIParentType.POP_UI_ROOT, false, false}, --CC直播列表窗口
	CCLiveFollowingListWnd = { "UI/Prefab/CCLive/CCLiveFollowingListWnd.prefab", UIParentType.POP_UI_ROOT, false, false}, --CC主播关注列表
	CrossSXDDZWnd = { "UI/Prefab/DaDiZi/CrossSXDDZWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CrossSXDDZChallengerListWnd = { "UI/Prefab/DaDiZi/CrossSXDDZChallengerListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--藏宝阁
	TreasureHouseWnd = { "UI/Prefab/TreasureHouse/TreasureHouseWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TreasureHouseRegisterWnd = { "UI/Prefab/TreasureHouse/TreasureHouseRegisterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TreasureHousePutToShowWnd = { "UI/Prefab/TreasureHouse/TreasureHousePutToShowWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TreasureHouseShelfWnd = { "UI/Prefab/TreasureHouse/TreasureHouseShelfWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TreasureHousePutToSellWnd = { "UI/Prefab/TreasureHouse/TreasureHousePutToSellWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ExchangeYuanBaoWithBaoShiWnd = { "UI/Prefab/ExChange/ExchangeYuanBaoWithBaoShiWnd.prefab", UIParentType.POP_UI_ROOT, false, true}, --宝石兑换元宝

	GuanNingChoiceWnd = { "UI/Prefab/GuanNing/GuanNingChoiceWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuanNingSignalWnd = { "UI/Prefab/GuanNing/GuanNingSignalWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuanNingGroupWnd = { "UI/Prefab/GuanNing/GuanNingGroupWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuanNingGroupInfoTip = { "UI/Prefab/GuanNing/GuanNingGroupInfoTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuanNingCommandWnd = {"UI/Prefab/GuanNing/GuanNingCommandWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuanNingResurrectionWnd = {"UI/Prefab/GuanNing/GuanNingResurrectionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--BaiGuiTuJianWnd = { "UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie/BaiGuiTuJianWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	LingShouGetSkinWnd = { "UI/Prefab/LingShou/LingShouGetSkinWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	LingShouChosenWnd = { "UI/Prefab/LingShou/LingShouChosenWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	LingShouLevelLimitWnd = { "UI/Prefab/LingShou/LingShouLevelLimitWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	CommonCharacterDescWnd = { "UI/Prefab/Common/CommonCharacterDescWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	ScreenBrokenWnd = { "UI/Prefab/ZhuJueJuQing/ScreenBrokenWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FallingDownWnd = { "UI/Prefab/ZhuJueJuQing/FallingDownWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	DrawHanZiWnd = { "UI/Prefab/ZhuJueJuQing/DrawHanZiWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FallingDownTreasureWnd = { "UI/Prefab/ZhuJueJuQing/FallingDownTreasureWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	LumberingWnd = { "UI/Prefab/ZhuJueJuQing/LumberingWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	BoilMedicineWnd = { "UI/Prefab/ZhuJueJuQing/BoilMedicineWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	PuSongLingHandWriteEffectWnd = { "UI/Prefab/ZhuJueJuQing/PuSongLingHandWriteEffectWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	ServerSubmitWnd = { "UI/Prefab/ServerSubmitWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	NewActivityWnd = { "UI/Prefab/Schedule/NewActivityWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	-- 城战
	PlaceCityUnitWnd = { "UI/Prefab/CityWar/PlaceCityUnitWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	CityConstructionRankWnd = { "UI/Prefab/CityWar/CityConstructionRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarSecondaryMapWnd = { "UI/Prefab/CityWar/CityWarSecondaryMapWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarPrimaryMapWnd = { "UI/Prefab/CityWar/CityWarPrimaryMapWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarOccupyConfirmWnd = { "UI/Prefab/CityWar/CityWarOccupyConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarSoldierWnd = { "UI/Prefab/CityWar/CityWarSoldierWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityMapInfoTip = { "UI/Prefab/CityWar/CityMapInfoTip.prefab", UIParentType.POP_UI_ROOT, false, false},
	MajorCityMapInfoTip = { "UI/Prefab/CityWar/MajorCityMapInfoTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityMainWnd = { "UI/Prefab/CityWar/CityMainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarCityInfoWnd = { "UI/Prefab/CityWar/CityWarCityInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarScheduleWnd = { "UI/Prefab/CityWar/CityWarScheduleWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarAwardWnd = { "UI/Prefab/CityWar/CityWarAwardWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarRuleTip = { "UI/Prefab/CityWar/CityWarRuleTip.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarRobAssetWnd = { "UI/Prefab/CityWar/CityWarRobAssetWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarChallengeWnd = { "UI/Prefab/CityWar/CityWarChallengeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CiryWarStateWnd = { "UI/Prefab/CityWar/CiryWarStateWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarResultWnd = { "UI/Prefab/CityWar/CityWarResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarGuildEnemyWnd = { "UI/Prefab/CityWar/CityWarGuildEnemyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarBiaoCheMenu = { "UI/Prefab/CityWar/CityWarBiaoCheMenu.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarPlaceBiaoCheConfirmWnd = { "UI/Prefab/CityWar/CityWarPlaceBiaoCheConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarContributionWnd = { "UI/Prefab/CityWar/CityWarContributionWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarBiaoCheResSubmitWnd = { "UI/Prefab/CityWar/CityWarBiaoCheResSubmitWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarBiaoCheConfirmWnd = { "UI/Prefab/CityWar/CityWarBiaoCheConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarCopySecondaryMapWnd = { "UI/Prefab/CityWar/CityWarCopySecondaryMapWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CommonAnnounceWnd = { "UI/Prefab/CommonAnnounceWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarHistoryWnd = { "UI/Prefab/CityWar/CityWarHistoryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarGuildMaterialSubmitWnd = { "UI/Prefab/CityWar/CityWarGuildMaterialSubmitWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarMonsterSiegeWnd = { "UI/Prefab/CityWar/CityWarMonsterSiegeWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	CityWarSingleUpgradeWnd = { "UI/Prefab/CityWar/CityWarSingleUpgradeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarBatchUpgradeWnd = { "UI/Prefab/CityWar/CityWarBatchUpgradeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarMonsterSiegeSettingWnd = { "UI/Prefab/CityWar/CityWarMonsterSiegeSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CityWarWatchWnd = { "UI/Prefab/CityWar/CityWarWatchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarExchangeWnd = { "UI/Prefab/CityWar/CityWarExchangeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarGuildSearchWnd = { "UI/Prefab/CityWar/CityWarGuildSearchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityGuildRankWnd = { "UI/Prefab/CityWar/CityGuildRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityOccupyRankWnd = { "UI/Prefab/CityWar/CityOccupyRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 国庆校场
	--GQJCStateWnd = { "UI/Prefab/Festival_GuoQing/GQJC/GQJCStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	--GQJCChooseRewardWnd = { "UI/Prefab/Festival_GuoQing/GQJC/GQJCChooseRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--GQJCSituationWnd = { "UI/Prefab/Festival_GuoQing/GQJC/GQJCSituationWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--GQJCRankWnd = { "UI/Prefab/Festival_GuoQing/GQJC/GQJCRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	GuildLeagueInviteWnd = { "UI/Prefab/GuildLeague/GuildLeagueInviteWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueAltarBuffHistoryWnd = { "UI/Prefab/GuildLeague/GuildLeagueAltarBuffHistoryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueAppointHeroWnd = { "UI/Prefab/GuildLeague/GuildLeagueAppointHeroWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueAppointHeroDetailWnd = { "UI/Prefab/GuildLeague/GuildLeagueAppointHeroDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--跨服帮赛
	GuildLeagueCrossAgendaWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossAgendaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossMatchInfoWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossMatchInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossSelfServerGuildListWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossSelfServerGuildListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossResultWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossMatchDetailInfoWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossMatchDetailInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossServerGuildInfoWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossServerGuildInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossPosExchangeWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossPosExchangeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossPosExchangeApplyWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossPosExchangeApplyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossAwardOverviewWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossAwardOverviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossTrainWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossTrainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossTrainAdditionWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossTrainAdditionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossWatchWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossWatchWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	GuildLeagueCrossGambleWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossGambleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossGambleBetWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossGambleBetWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossGambleRewardWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossGambleRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossGambleServerInfoWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossGambleServerInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossAidEvaluationWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossAidEvaluationWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossZhanLingWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossZhanLingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossZhanLingRankWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossZhanLingRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildLeagueCrossZhanLingAwardPoolWnd = { "UI/Prefab/GuildLeague/GuildLeagueCrossZhanLingAwardPoolWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	HSVTestWnd = { "UI/NoneReleasePrefab/HSVTestWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 养育界面
	BirthPermissionWnd = { "UI/Prefab/Baby/BirthPermissionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PrenatalMessageWnd = { "UI/Prefab/Baby/PrenatalMessageWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ProductionInspectionWnd = { "UI/Prefab/Baby/ProductionInspectionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyNamingWnd = { "UI/Prefab/Baby/BabyNamingWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BabyGrowDiaryWnd = { "UI/Prefab/Baby/BabyGrowDiaryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyQiYuShareWnd = { "UI/Prefab/Baby/BabyQiYuShareWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabySchedulePlanList = { "UI/Prefab/Baby2023/BabySchedulePlanList.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyWnd = { "UI/Prefab/Baby2023/BabyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BabyScheduleGainWnd = { "UI/Prefab/Baby/BabyScheduleGainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyProDifShowWnd = { "UI/Prefab/Baby/BabyProDifShowWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyTaiMengWnd = { "UI/Prefab/Baby/BabyTaiMengWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabySchedulePlanDetailWnd = { "UI/Prefab/Baby/BabySchedulePlanDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyFeedWnd = { "UI/Prefab/Baby/BabyFeedWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyChatWnd = { "UI/Prefab/Baby/BabyChatWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyChatHistoryWnd = { "UI/Prefab/Baby/BabyChatHistoryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyExpressionActionWnd = { "UI/Prefab/Baby/BabyExpressionActionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyExpressionUnlockWnd = { "UI/Prefab/Baby/BabyExpressionUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyGetBackWnd = { "UI/Prefab/Baby/BabyGetBackWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyQiChangJianDingWnd = { "UI/Prefab/Baby2023/BabyQiChangJianDingWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BabyQiChangShuWnd = { "UI/Prefab/Baby2023/BabyQiChangShuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyChooseWnd = { "UI/Prefab/Baby/BabyChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyQiChangRequestTip = { "UI/Prefab/Baby2023/BabyQiChangRequestTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyPropXiLianWnd = { "UI/Prefab/Baby2023/BabyPropXiLianWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BabyPoemWnd = {"UI/Prefab/Baby/BabyPoemWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	BabyQiChangFxWnd = { "UI/Prefab/Baby/BabyQiChangFxWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabySkillTip = { "UI/Prefab/Baby/BabySkillTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabySendHongBaoWnd = { "UI/Prefab/Baby/BabySendHongBaoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyHongBaoHistoryWnd = { "UI/Prefab/Baby/BabyHongBaoHistoryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyTiLiGetTip = { "UI/Prefab/Baby/BabyTiLiGetTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyDivinationWnd = { "UI/Prefab/Baby/BabyDivinationWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BabyFashionPreviewWnd = { "UI/Prefab/Baby/BabyFashionPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShopMallBabyFashionPreviewWnd = { "UI/Prefab/Baby/ShopMallBabyFashionPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BabyFashionChooseWnd = { "UI/Prefab/Baby/BabyFashionChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	BlindBoxPreviewWnd = { "UI/Prefab/BlindBoxPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	CommonRuleTipWnd = { "UI/Prefab/CommonRuleTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CheckboxPopupMenu = { "UI/Prefab/Common/CheckboxPopupMenu.prefab", UIParentType.POP_UI_ROOT, false, true},
	CommonImageRuleWnd = { "UI/Prefab/Common/CommonImageRuleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 养育玩法
	ParentExamWnd = { "UI/Prefab/Baby/ParentExamWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ParentExamResultWnd = { "UI/Prefab/Baby/ParentExamResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AutoTakingPhotoWnd = { "UI/Prefab/Baby/AutoTakingPhotoWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	--ChristmasSendGiftWnd = { "UI/Prefab/Festival_Christmas/Christmas/ChristmasSendGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--炼神
	LianShenWnd = { "UI/Prefab/Guild/LianShenWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	EnvelopeDisplayWnd = { "UI/Prefab/EnvelopeDisplayWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--LianLianKanApplyWnd = { "UI/Prefab/Festival_Double11/LianLianKan2018/LianLianKanApplyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--LianLianKanResultWnd = { "UI/Prefab/Festival_Double11/LianLianKan2018/LianLianKanResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--LianLianKanQuestionWnd = { "UI/Prefab/Festival_Double11/LianLianKan2018/LianLianKanQuestionWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--LianLianKanRankWnd = { "UI/Prefab/Festival_Double11/LianLianKan2018/LianLianKanRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--LianLianKanTopRightWnd = { "UI/Prefab/Festival_Double11/LianLianKan2018/LianLianKanTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--ZhongCaoFashionWnd = { "UI/Prefab/Festival_Double11/LianLianKan2018/ZhongCaoFashionWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	CityWarZiCaiShopWnd = { "UI/Prefab/CityWar/CityWarZiCaiShopWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	OfflineItemWnd = { "UI/Prefab/OfflineItem/OfflineItemWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	NPCShopWnd2 = { "UI/Prefab/NPCShopWnd2.prefab", UIParentType.POP_UI_ROOT,false,false},

	--保卫南瓜玩法
	--BaoWeiNanGuaTopRightWnd = { "UI/Prefab/Festival_Halloween/BaoWeiNanGua/BaoWeiNanGuaTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	--BaoWeiNanGuaResultWnd = { "UI/Prefab/Festival_Halloween/BaoWeiNanGua/BaoWeiNanGuaResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	HuaZhuWnd = { "UI/Prefab/Baby/HuaZhuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ManYueJiuChooseBabyWnd = { "UI/Prefab/Baby/ManYueJiuChooseBabyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OtherBabyInfoWnd = { "UI/Prefab/Baby/OtherBabyInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 圣诞节
	--ChristmasTreeBreedWnd = { "UI/Prefab/Festival_Christmas/Christmas/ChristmasTreeBreedWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--ChristmasTreeAddItemWnd = {"UI/Prefab/Festival_Christmas/Christmas/ChristmasTreeAddItemWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 八卦炉
	BaGuaLuLingQiGetWnd = { "UI/Prefab/QianKunDai/BaGuaLuLingQiGetWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BaGuaLuWnd = { "UI/Prefab/QianKunDai/BaGuaLuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BaGuaLuLingQiZhuRuWnd = { "UI/Prefab/QianKunDai/BaGuaLuLingQiZhuRuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BaGuaLuJieGuaWnd = { "UI/Prefab/QianKunDai/BaGuaLuJieGuaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ExchangeJieBanSkillItemWnd = { "UI/Prefab/LingShou/ExchangeJieBanSkillItemWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ExchangeJieBanSkillItemWnd2 = { "UI/Prefab/LingShou/ExchangeJieBanSkillItemWnd2.prefab", UIParentType.POP_UI_ROOT, false, true},
	SelectJieBanLingShouWnd = { "UI/Prefab/LingShou/SelectJieBanLingShouWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--2019元旦活动
	--NewYear2019DrawWnd = { "UI/Prefab/Festival_YuanDan/YuanDan2019/NewYear2019DrawWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--NewYear2019TaskBookWnd = { "UI/Prefab/Festival_YuanDan/YuanDan2019/NewYear2019TaskBookWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--NewYear2019TaskTipWnd = { "UI/Prefab/Festival_YuanDan/YuanDan2019/NewYear2019TaskTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--辩论玩法
	DebateWithNpcWnd = { "UI/Prefab/DebateWithNpc/DebateWithNpcWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--七巧板玩法
	QiqiaobanWnd = { "UI/Prefab/Qiqiaoban/QiqiaobanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--雪球大战
	SnowBallHisWnd = { "UI/Prefab/Festival_HanJia/SnowBallFight/SnowBallHisWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--SnowBallRankWnd = { "UI/Prefab/Festival_HanJia/SnowBallFight/SnowBallRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--寒假2020
	YanChiXiaWorkWnd = { "UI/Prefab/Festival_HanJia/HanJia2020/YanChiXiaWorkWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FlagonOpenWnd = { "UI/Prefab/Festival_HanJia/HanJia2020/FlagonOpenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	--寒假2024
	SubmitSnowballsForMakingSnowManWnd = { "UI/Prefab/Festival_HanJia/HanJia2024/SubmitSnowballsForMakingSnowManWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DecorateSnowmanWnd = { "UI/Prefab/Festival_HanJia/HanJia2024/DecorateSnowmanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--元宵2020
	DuiDuiLeStartWnd =  { "UI/Prefab/Festival_YuanXiao/YuanXiao2020/DuiDuiLeStartWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuiDuiLeSettlementWnd =  { "UI/Prefab/Festival_YuanXiao/YuanXiao2020/DuiDuiLeSettlementWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DuiDuiLeGamePlayWnd =  { "UI/Prefab/Festival_YuanXiao/YuanXiao2020/DuiDuiLeGamePlayWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	YuanXiao2020QiYuanWnd =  { "UI/Prefab/Festival_YuanXiao/YuanXiao2020/YuanXiao2020QiYuanWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--元宵节玩法
	--YuanxiaoTopRightWnd = { "UI/Prefab/Festival_YuanXiao/YuanxiaoFestival/YuanxiaoTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	--YuanxiaoChoosePuzzleWnd = { "UI/Prefab/Festival_YuanXiao/YuanxiaoFestival/YuanxiaoChoosePuzzleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--YuanxiaoResultWnd = { "UI/Prefab/Festival_YuanXiao/YuanxiaoFestival/YuanxiaoResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	CityWarMonsterSiegeSituationWnd = { "UI/Prefab/CityWar/CityWarMonsterSiegeSituationWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WinterHouseChooseWnd = { "UI/Prefab/House/WinterHouseChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WinterHouseUnlockListWnd = { "UI/Prefab/House/WinterHouseUnlockListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WinterHouseUnlockWnd = { "UI/Prefab/House/WinterHouseUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WinterHouseZswLookupWnd = { "UI/Prefab/House/WinterHouseZswLookupWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--分线
	CopySceneChosenWnd = { "UI/Prefab/CopySceneChosenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--2019情人节
	--QingYuanShouJiListWnd = { "UI/Prefab/Festival_Valentine/Valentine2019/QingYuanShouJiListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--QingYuanShouJiContentWnd = { "UI/Prefab/Festival_Valentine/Valentine2019/QingYuanShouJiContentWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QingYuanShouJiMemoirsWnd = { "UI/Prefab/Festival_Valentine/Valentine2019/QingYuanShouJiMemoirsWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--QingYuanShouJiPromiseWnd = { "UI/Prefab/Festival_Valentine/Valentine2019/QingYuanShouJiPromiseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--PreExistenceMarridgeAlbumWnd = { "UI/Prefab/Festival_Valentine/Valentine2019/PreExistenceMarridgeAlbumWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--PreExistenceMarridgeDetailWnd = { "UI/Prefab/Festival_Valentine/Valentine2019/PreExistenceMarridgeDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--InTheNameOfLoveRankWnd = {"UI/Prefab/Festival_Valentine/Valentine2019/InTheNameOfLoveRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--2019国庆
	--NationalDayTXZREnterWnd = { "UI/Prefab/Festival_GuoQing/NationalDayWnd/NationalDayTXZREnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--NationalDayTXZRResultWnd = { "UI/Prefab/Festival_GuoQing/NationalDayWnd/NationalDayTXZRResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--NationalDayJCYWResultWnd = { "UI/Prefab/Festival_GuoQing/NationalDayWnd/NationalDayJCYWResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 梦华录
	MengHuaLuWnd = { "UI/Prefab/MengHuaLu/MengHuaLuWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MengHuaLuResultWnd = { "UI/Prefab/MengHuaLu/MengHuaLuResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengHuaLuQiChangWnd = { "UI/Prefab/MengHuaLu/MengHuaLuQiChangWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengHuaLuBuffListWnd = { "UI/Prefab/MengHuaLu/MengHuaLuBuffListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengHuaLuMonsterWnd = { "UI/Prefab/MengHuaLu/MengHuaLuMonsterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengHuaLuStartWnd = { "UI/Prefab/MengHuaLu/MengHuaLuStartWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengHualuRankWnd = { "UI/Prefab/MengHuaLu/MengHualuRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengHuaLuShopWnd = { "UI/Prefab/MengHuaLu/MengHuaLuShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengHuaLuSkillTip = { "UI/Prefab/MengHuaLu/MengHuaLuSkillTip.prefab", UIParentType.POP_UI_ROOT, false, true},

	JuQingPinTuWnd = { "UI/Prefab/ZhuJueJuQing/JuQingPinTuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	BigJuqingPicWnd = { "UI/Prefab/ZhuJueJuQing/BigJuqingPicWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--NewYear2019CunQianGuanWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2019/NewYear2019CunQianGuanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--NewYear2019NianFireworkConfirmWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2019/NewYear2019NianFireworkConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--NewYear2019NianRewardExchangeWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2019/NewYear2019NianRewardExchangeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PreciousItemAlertWnd = { "UI/Prefab/Common/PreciousItemAlertWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LingShouZhandouliWnd = { "UI/Prefab/LingShou/LingShouZhandouliWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--梦岛热点
	PersonalSpaceHotTalkWnd = { "UI/Prefab/PersonalSpace/PersonalSpaceHotTalkWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PersonalSpaceDetailPicWnd = { "UI/Prefab/PersonalSpace/PersonalSpaceDetailPicWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LingShouSwitchJieBanSkillWnd = { "UI/Prefab/LingShou/LingShouSwitchJieBanSkillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PersonalSpaceForwardLuaWnd = { "UI/Prefab/PersonalSpace/PersonalSpaceForwardLuaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	DetailMovieShowWnd = { "UI/Prefab/PersonalSpace/DetailMovieShowWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	EquipKeZiWnd = { "UI/Prefab/Equip/EquipKeZiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	StarBiwuStatueBuildWnd = { "UI/Prefab/StarBiwu/StarBiwuStatueBuildWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuChampionTeamWnd = { "UI/Prefab/StarBiwu/StarBiwuChampionTeamWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuChampionHistoryWnd = { "UI/Prefab/StarBiwu/StarBiwuChampionHistoryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuDeleteFriendWnd = { "UI/Prefab/StarBiwu/StarBiwuDeleteFriendWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuAgendaWnd = { "UI/Prefab/StarBiwu/StarBiwuAgendaWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuQieCuoWnd = { "UI/Prefab/StarBiwu/StarBiwuQieCuoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuCreateZhanDuiWnd = { "UI/Prefab/StarBiwu/StarBiwuCreateZhanDuiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuZhanDuiSearchWnd = { "UI/Prefab/StarBiwu/StarBiwuZhanDuiSearchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuSelfZhanDuiWnd = { "UI/Prefab/StarBiwu/StarBiwuSelfZhanDuiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuZhanDuiSettingWnd = { "UI/Prefab/StarBiwu/StarBiwuZhanDuiSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuModifySloganWnd = { "UI/Prefab/StarBiwu/StarBiwuModifySloganWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuCurrentBattleStatusWnd = { "UI/Prefab/StarBiwu/StarBiwuCurrentBattleStatusWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuBattleDataWnd = { "UI/Prefab/StarBiwu/StarBiwuBattleDataWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuZhanDuiMemberListWnd = { "UI/Prefab/StarBiwu/StarBiwuZhanDuiMemberListWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuMatchRecordWnd = { "UI/Prefab/StarBiwu/StarBiwuMatchRecordWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuScoreWnd = { "UI/Prefab/StarBiwu/StarBiwuScoreWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuGroupMatchWnd = { "UI/Prefab/StarBiwu/StarBiwuGroupMatchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuTopFourWnd = { "UI/Prefab/StarBiwu/StarBiwuTopFourWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuFinalMatchRecordWnd = { "UI/Prefab/StarBiwu/StarBiwuFinalMatchRecordWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	StarBiwuRewardWnd = { "UI/Prefab/StarBiwu/StarBiwuRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuFinalWatchWnd = { "UI/Prefab/StarBiwu/StarBiwuFinalWatchWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuFinalJingCaiWnd = { "UI/Prefab/StarBiwu/StarBiwuFinalJingCaiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuStopVoteWnd = { "UI/Prefab/StarBiwu/StarBiwuStopVoteWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuEventReviewWnd = { "UI/Prefab/StarBiwu/StarBiwuEventReviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuChampionWnd = { "UI/Prefab/StarBiwu/StarBiwuChampionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiWuGiftRankWnd = { "UI/Prefab/StarBiwu/StarBiWuGiftRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiWuGiftContributionRankWnd = { "UI/Prefab/StarBiwu/StarBiWuGiftContributionRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuBuyLeiGuTimesWnd = { "UI/Prefab/StarBiwu/StarBiwuBuyLeiGuTimesWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuReShenBattleStatusWnd = { "UI/Prefab/StarBiwu/StarBiwuReShenBattleStatusWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuQuDaoBattleStatusWnd = { "UI/Prefab/StarBiwu/StarBiwuQuDaoBattleStatusWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuDetailInfoWnd = { "UI/Prefab/StarBiwu/StarBiwuDetailInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiwuZhanDuiPlatformWnd = { "UI/Prefab/StarBiwu/StarBiwuZhanDuiPlatformWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiWuZhanDuiInvitedWnd = { "UI/Prefab/StarBiwu/StarBiWuZhanDuiInvitedWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiWuZhanDuiRequestWnd = { "UI/Prefab/StarBiwu/StarBiWuZhanDuiRequestWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StarBiWuFreePlayerWnd = { "UI/Prefab/StarBiwu/StarBiWuFreePlayerWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 通用战况
	BattleSituationWnd = {"UI/Prefab/Guild/BattleSituationWnd.prefab",UIParentType.POP_UI_ROOT, false, true},

	--明星邀请赛观战
	StarInviteWatchWnd = {"UI/Prefab/StarInviteGame/StarInviteWatchWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	StarInviteSettingWnd = {"UI/Prefab/StarInviteGame/StarInviteSettingWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	DaShenWelfareWnd = {"UI/Prefab/Welfare/DashenWelfareWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	SignInWnd = {"UI/Prefab/Welfare/SignInWnd.prefab",UIParentType.POP_UI_ROOT, false, true},

	CommonKillInfoTopWnd = {"UI/Prefab/Common/CommonKillInfoTopWnd.prefab",UIParentType.BASE_UI_ROOT, false, true},

	--清明节活动 2019
	--QingmingMakeWineWnd = {"UI/Prefab/Festival_QingMing/QingMing2019/QingmingMakeWineWnd.prefab",UIParentType.POP_UI_ROOT, false, true},

	TianMenShanBattleStateWnd = { "UI/Prefab/CityWar/TianMenShanBattleStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	--涂色功能通用界面
	PaintPicWnd = {"UI/Prefab/PaintPic/PaintPicWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	PaintPicWndType2 = {"UI/Prefab/PaintPic/PaintPicWndType2.prefab",UIParentType.POP_UI_ROOT,false,false},
	PaintPicWndType3 = {"UI/Prefab/PaintPic/PaintPicWndType3.prefab",UIParentType.POP_UI_ROOT,false,false},

	--音乐盒
	MusicBoxMainWnd = { "UI/Prefab/MusicBox/MusicBoxMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MusicBoxRecordWnd = { "UI/Prefab/MusicBox/MusicBoxRecordWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--轻鸿玉佩飞行
	QingHongFlyWnd = { "UI/Prefab/ZhuSiDong/QingHongFlyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 2019周年庆玩法
	CakeCuttingWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CakeCuttingWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	CakeCuttingFightInfoWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CakeCuttingFightInfoWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	CakeCuttingRankWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CakeCuttingRankWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	CakeCuttingShowWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CakeCuttingShowWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	CakeCuttingDieWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CakeCuttingDieWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	CakeCuttingChatWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CakeCuttingChatWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	CakeCuttingComboWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CakeCuttingComboWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	CaiPiao2019Wnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/CaiPiao2019Wnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	NewSeverInviteWnd = {"UI/Prefab/Festival_ZhouNianQing/CakeCutting/NewSeverInviteWnd.prefab",UIParentType.POP_UI_ROOT, false, true},

	NpcFightingWnd = { "UI/Prefab/NpcFightingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 仙职
	XianZhiPuWnd = { "UI/Prefab/XianZhi/XianZhiPuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XianZhiManageWnd = { "UI/Prefab/XianZhi/XianZhiManageWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XianZhiShenZhiWnd = { "UI/Prefab/XianZhi/XianZhiShenZhiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XianZhiWnd = { "UI/Prefab/XianZhi/XianZhiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XianZhiSkillUpgradeWnd = { "UI/Prefab/XianZhi/XianZhiSkillUpgradeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XianZhiDetailDescWnd = { "UI/Prefab/XianZhi/XianZhiDetailDescWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XianZhiTypeDescWnd = { "UI/Prefab/XianZhi/XianZhiTypeDescWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XianZhiPromotionWnd = { "UI/Prefab/XianZhi/XianZhiPromotionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FengXianSignUpWnd = { "UI/Prefab/XianZhi/FengXianSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FengXianInviteWnd = { "UI/Prefab/XianZhi/FengXianInviteWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FengXianInvitationCardWnd = { "UI/Prefab/XianZhi/FengXianInvitationCardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FengXianTingFengWnd = { "UI/Prefab/XianZhi/FengXianTingFengWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	XianzhiTaskCompletedWnd = {"UI/Prefab/Xianzhi/XianzhiTaskCompletedWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	XianzhiDescribeWnd = {"UI/Prefab/Xianzhi/XianzhiDescribeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	XianzhiChooseImageTaskWnd = {"UI/Prefab/Xianzhi/XianzhiChooseImageTaskWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	PropertySwitchWnd = {"UI/Prefab/PropertySwitchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	PropertySwitchPopupMenu = {"UI/Prefab/PropertySwitchPopupMenu.prefab", UIParentType.POP_UI_ROOT, false, false},

	NpcHaoGanDuWnd= {"UI/Prefab/NpcHaoGanDuWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	NpcHaoGanDuBuyGiftCountWnd= {"UI/Prefab/NpcHaoGanDuBuyGiftCountWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 五一挖矿
	--WaKuangStartWnd = {"UI/Prefab/Festival_WuYi/WuYi/WaKuangStartWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--WaKuangStateWnd = {"UI/Prefab/Festival_WuYi/WuYi/WaKuangStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	--WaKuangUpgradeWnd = {"UI/Prefab/Festival_WuYi/WuYi/WaKuangUpgradeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--WaKuangResultWnd = {"UI/Prefab/Festival_WuYi/WuYi/WaKuangResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--儿童节活动
	ChildrenDay2019TaskBookWnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2019/ChildrenDay2019TaskBookWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChildrenDayPlayFightInfoWnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2019/ChildrenDayPlayFightInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChildrenDayPlayOperateWnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2019/ChildrenDayPlayOperateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ChildrenDayPlayResultWnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2019/ChildrenDayPlayResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChildrenDayPlayShowWnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2019/ChildrenDayPlayShowWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChildrenDayPlayTopRightWnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2019/ChildrenDayPlayTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	--
	CharacterCardWnd = { "UI/Prefab/CharacterCard/CharacterCardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CharacterCardHeWnd = { "UI/Prefab/CharacterCard/CharacterCardHeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CharacterCardDetailWnd = { "UI/Prefab/CharacterCard/CharacterCardDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--2019端午节
	--DwWusesiHeChengWnd	= {	"UI/Prefab/Festival_DuanWu/DuanWu2019/DwWusesiHeChengWnd.prefab", 	UIParentType.POP_UI_ROOT, false, true},
	--DwWusesiTabenWnd	= {		"UI/Prefab/Festival_DuanWu/DuanWu2019/DwWusesiTabenWnd.prefab",		UIParentType.POP_UI_ROOT, false, true},
	--DwZongziRuleWnd		= {		"UI/Prefab/Festival_DuanWu/DuanWu2019/DwZongziRuleWnd.prefab",		UIParentType.POP_UI_ROOT, false, true},
	--DwZongziRuleTipWnd	= {	"UI/Prefab/Festival_DuanWu/DuanWu2019/DwZongziRuleTipWnd.prefab",	UIParentType.POP_UI_ROOT, false, true},
	--DwZongziResultWnd	= {	"UI/Prefab/Festival_DuanWu/DuanWu2019/DwZongziResultWnd.prefab",	UIParentType.POP_UI_ROOT, false, true},
	DwZongziTaskView	= {		"UI/Prefab/Festival_DuanWu/DuanWu2019/DwZongziTaskView.prefab",		UIParentType.POP_UI_ROOT, false, true},
	--DwZongziTopRightMenu= {	"UI/Prefab/Festival_DuanWu/DuanWu2019/DwZongziTopRightMenu.prefab",	UIParentType.POP_UI_ROOT, false, true},

	--靓号
	PlayerAppearanceSettingWnd = { "UI/Prefab/LiangHao/PlayerAppearanceSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoSettingWnd = { "UI/Prefab/LiangHao/LiangHaoSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoRenewWnd = { "UI/Prefab/LiangHao/LiangHaoRenewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoFriendListWnd = { "UI/Prefab/LiangHao/LiangHaoFriendListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoPurchaseWnd = { "UI/Prefab/LiangHao/LiangHaoPurchaseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoAuctionWnd = { "UI/Prefab/LiangHao/LiangHaoAuctionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoMyAuctionWnd = { "UI/Prefab/LiangHao/LiangHaoMyAuctionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoCustomizeWnd = { "UI/Prefab/LiangHao/LiangHaoCustomizeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoMyReqCustomizeCardWnd = { "UI/Prefab/LiangHao/LiangHaoMyReqCustomizeCardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiangHaoReqCustomizeCardWnd = { "UI/Prefab/LiangHao/LiangHaoReqCustomizeCardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--精灵
	JingLingQuickPurchaseWnd = { "UI/Prefab/JingLing/JingLingQuickPurchaseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	MessageTipWithButtonWnd = { "UI/Prefab/Common/MessageTipWithButtonWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AchievementDetailWnd = { "UI/Prefab/Achievement/AchievementDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AchievementAwardsTipWnd = { "UI/Prefab/Achievement/AchievementAwardsTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CommonHorizontalPopupMenu = { "UI/Prefab/Common/CommonHorizontalPopupMenu.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildReportWnd = { "UI/Prefab/Report/GuildReportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	CrossServerFriendPopupMenu = { "UI/Prefab/CrossServerFriendPopupMenu.prefab", UIParentType.POP_UI_ROOT, false, true},
	FlowerWithBloodEffectWnd = { "UI/Prefab/JuQing/FlowerWithBloodEffectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DriveAwayGhostWnd = { "UI/Prefab/JuQing/DriveAwayGhostWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FlowerWitheredEffectWnd = { "UI/Prefab/JuQing/FlowerWitheredEffectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CageEffectWnd = { "UI/Prefab/JuQing/CageEffectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LeafWithMemoroyWnd = {"UI/Prefab/JuQing/LeafWithMemoroyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--125-150级主线剧情
	PanZhuangWnd = {"UI/Prefab/JuQing/PanZhuangWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	PourWineWnd = {"UI/Prefab/JuQing/PourWineWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	EyeCatchTheDogWnd = {"UI/Prefab/JuQing/EyeCatchTheDogWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CouncilWnd = {"UI/Prefab/JuQing/CouncilWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	ObservingSoulWnd = {"UI/Prefab/JuQing/ObservingSoulWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ObservingSoulDetailTipWnd = {"UI/Prefab/JuQing/ObservingSoulDetailTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CocoonBreakWnd = {"UI/Prefab/JuQing/CocoonBreakWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MakeBeautificationWnd = {"UI/Prefab/JuQing/MakeBeautificationWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	CookingPlayWnd = { "UI/Prefab/CookingPlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	BoLangGuWnd = { "UI/Prefab/DiGong/BoLangGuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JianBiHuaWnd = { "UI/Prefab/DiGong/JianBiHuaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShowPictureWnd = { "UI/Prefab/Common/ShowPictureWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	BatDialogWnd = { "UI/Prefab/BatDialogWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	BackBonusWnd = { "UI/Prefab/BackBonus/BackBonusWnd.prefab", UIParentType.POP_UI_ROOT, false, true},


	WuJianDiYuHeChengWnd = { "UI/Prefab/WuJianDiYu/WuJianDiYuHeChengWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	WuJianDiYuEnterWnd = { "UI/Prefab/WuJianDiYu/WuJianDiYuEnterWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	WuJianDiYuTopRightWnd = { "UI/Prefab/WuJianDiYu/WuJianDiYuTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},

	TalismanFxExchangeWnd = { "UI/Prefab/TalismanFxExchangeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TalismanFxReturnWnd = { "UI/Prefab/Appearance2019/TalismanFxReturnWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DrawLineWnd = { "UI/Prefab/WorldEvent/DrawLineWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DrawDripWnd = { "UI/Prefab/WorldEvent/DrawDripWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	BuffItemWnd = { "UI/Prefab/BuffItemWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	FeiShengRuleConfirmWnd = { "UI/Prefab/Common/FeiShengRuleConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	SceneJewelSubmitWnd = { "UI/Prefab/SceneInteractive/SceneJewelSubmitWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhuXianAchievementWnd = { "UI/Prefab/Achievement/ZhuXianAchievementWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	UITriggerWnd = { "UI/Prefab/JuQing/UITriggerWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QMPKHandBookWnd = { "UI/Prefab/QuanMinPK/QMPKHandBookWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QMPHKUnlockZhanLingWnd = { "UI/Prefab/QuanMinPK/QMPHKUnlockZhanLingWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	NpcHaoGanDuZhanBuWnd = { "UI/Prefab/NpcHaoGanDuZhanBuWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	NpcHaoGanDuGiftWnd = { "UI/Prefab/NpcHaoGanDuGiftWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	CarnivalCaiPiao2019Wnd = {"UI/Prefab/Carnival/CarnivalCaiPiao2019Wnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	DanMuViewWnd = { "UI/Prefab/DanMuViewWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	--QiXiLiuXingQiYuanWnd = { "UI/Prefab/Festival_QiXi/QiXi2019/QiXiLiuXingQiYuanWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--中秋2019
	--ZQYueShenDianDescWnd 	= {	"UI/Prefab/Festival_ZhongQiu/ZhongQiu2019/YueShenDianDescWnd.prefab",	UIParentType.POP_UI_ROOT,false,false},
	--ZQYaoBanShangYueWnd		= {	"UI/Prefab/Festival_ZhongQiu/ZhongQiu2019/YaoBanShangYueWnd.prefab",	UIParentType.POP_UI_ROOT,false,false},
	--ZQSelectionToolTip		= {	"UI/Prefab/Festival_ZhongQiu/ZhongQiu2019/ZQSelectionToolTip.prefab",	UIParentType.POP_UI_ROOT,false,false},
	--ZQYueLingShiHeChengWnd	= {	"UI/Prefab/Festival_ZhongQiu/ZhongQiu2019/ZQYueLingShiHeChengWnd.prefab",		UIParentType.POP_UI_ROOT,false,false},
	--ZQYueShenDianTopRightWnd= {	"UI/Prefab/Festival_ZhongQiu/ZhongQiu2019/ZQYueShenDianTopRightWnd.prefab",	UIParentType.BASE_UI_ROOT,false,true},

	ZhuShaBiProcessWnd = { "UI/Prefab/ZhuShaBiProcessWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	ZhuShaBiXiLianWnd = { "UI/Prefab/ZhuShaBiXiLianWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	ZhuShaBiAutoXiLianWnd = { "UI/Prefab/ZhuShaBiAutoXiLianWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	ZhuShaBiStarWnd = { "UI/Prefab/ZhuShaBiStarWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	ZhuShaBiXiLianConditionWnd = { "UI/Prefab/ZhuShaBiXiLianConditionWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	--山鬼剧情
	BookshelfSearchWnd 		= {	"UI/Prefab/ShanGui/BookshelfSearchWnd.prefab",	UIParentType.POP_UI_ROOT,false,false},
	GuqinWnd				= {				"UI/Prefab/ShanGui/GuqinWnd.prefab",			UIParentType.POP_UI_ROOT,false,false},

	--地狱剧情
	WuZiQiWnd				= {			"UI/Prefab/DiYu/WuZiQiWnd.prefab",				UIParentType.POP_UI_ROOT,false,false},
	KaoChengHuangWnd		= {		"UI/Prefab/DiYu/KaoChengHuangWnd.prefab",		UIParentType.POP_UI_ROOT,false,false},

	GuildBuffDeskWnd = { "UI/Prefab/Guild/GuildBuffDeskWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	HouseFurnitureRotationWnd = { "UI/Prefab/House/HouseFurnitureRotationWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	QYCodeInputBoxWnd = { "UI/Prefab/QYCodeInputBox.prefab", UIParentType.POP_UI_ROOT, false, false},

	StarBiwuJingCaiWnd = { "UI/Prefab/StarBiwu/StarBiwuJingCaiWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	StarBiwuJingCaiDetailWnd = { "UI/Prefab/StarBiwu/StarBiwuJingCaiDetailWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	--万圣节2019
	--HalloweenRescueWnd 		= {	"UI/Prefab/Festival_Halloween/Halloween2019/HalloweenRescueWnd.prefab",	UIParentType.POP_UI_ROOT,false,false},
	--HalloweenLetterWnd 		= {	"UI/Prefab/Festival_Halloween/Halloween2019/HalloweenLetterWnd.prefab",	UIParentType.POP_UI_ROOT,false,false},
	--BugWeiChangTopRightWnd	= {	"UI/Prefab/Festival_Halloween/Halloween2019/BugWeiChangTopRightWnd.prefab",	UIParentType.BASE_UI_ROOT,false,true},
	--CrazyDyeResultWnd		= {	"UI/Prefab/Festival_Halloween/Halloween2019/CrazyDyeResultWnd.prefab",	UIParentType.POP_UI_ROOT,false,false},
	--CrazyDyeTopRightWnd		= {	"UI/Prefab/Festival_Halloween/Halloween2019/CrazyDyeTopRightWnd.prefab",	UIParentType.BASE_UI_ROOT,false,true},
	--CrazyDyeBossStateWnd 	= {	"UI/Prefab/Festival_Halloween/Halloween2019/CrazyDyeBossStateWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	--JigsawWnd 				= {	"UI/Prefab/Festival_Halloween/Halloween2019/JigsawWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	--CleanLingYuWnd 			= {	"UI/Prefab/Festival_Halloween/Halloween2019/CleanLingYuWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},

	--家谱界面标签
	LuaAddTagWnd = { "UI/Prefab/AddTagWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	LuaDelTagWnd = { "UI/Prefab/DelTagWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	LuaDelOtherTagWnd = { "UI/Prefab/DelOtherTagWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	FamilyTagDetailWnd = { "UI/Prefab/FamilyTagDetailWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	--自动洗练
	EquipWordAutoBaptizeTargetWnd = { "UI/Prefab/Equip/EquipWordAutoBaptizeTargetWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	EquipWordBaptizeChooseWordWnd = { "UI/Prefab/Equip/EquipWordBaptizeChooseWordWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	--2019双11
	--DoubleOneExamWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneExamWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneExamResultWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneExamResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneBonusLotteryWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneBonusLotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneBonusLotteryRankWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneBonusLotteryRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneBaotuanBeginWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneBaotuanBeginWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneBaotuanTipWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneBaotuanTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneBaotuanResultWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneBaotuanResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--DoubleOneQLDShowWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneQLDShowWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneQLDChangeSkillWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneQLDChangeSkillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--DoubleOneQLDResultWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneQLDResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--DoubleOneQLDTopRightWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneQLDTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	--DoubleOneBaotuanTopRightWnd = { "UI/Prefab/Festival_Double11/DoubleOne2019/DoubleOneBaotuanTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	SanxingPipeiWnd = { "UI/Prefab/SanxingGamePlay/SanxingPipeiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SanxingResultWnd = { "UI/Prefab/SanxingGamePlay/SanxingResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SanxingShowWnd = { "UI/Prefab/SanxingGamePlay/SanxingShowWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SanxingTipWnd = { "UI/Prefab/SanxingGamePlay/SanxingTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SanxingTopRightWnd = { "UI/Prefab/SanxingGamePlay/SanxingTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	SanxingGuideWnd = { "UI/Prefab/SanxingGamePlay/SanxingGuideWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SanxingRebornWnd = { "UI/Prefab/SanxingGamePlay/SanxingRebornWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SanxingEndWnd = { "UI/Prefab/SanxingGamePlay/SanxingEndWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SanxingChangeSkillWnd = { "UI/Prefab/SanxingGamePlay/SanxingChangeSkillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	HuiLiuFriendWnd = { "UI/Prefab/HuiLiu/HuiLiuFriendWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuiLiuMainWnd = { "UI/Prefab/HuiLiu/HuiLiuMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuiLiuTaskWnd = { "UI/Prefab/HuiLiu/HuiLiuTaskWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuiLiuGiftWnd = { "UI/Prefab/HuiLiu/HuiLiuGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuiLiuNewMainWnd = { "UI/Prefab/HuiLiu/HuiLiuNewMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	WishShopWnd = { "UI/Prefab/PersonalSpace/WishShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WishAddWnd = { "UI/Prefab/PersonalSpace/WishAddWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WishItemWnd = { "UI/Prefab/PersonalSpace/WishItemWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WishWordWnd = { "UI/Prefab/PersonalSpace/WishWordWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WishSelfFillWnd = { "UI/Prefab/PersonalSpace/WishSelfFillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChoosePersonalSpaceNewBgWnd = { "UI/Prefab/PersonalSpace/ChoosePersonalSpaceNewBgWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	CleanScreenWnd 		= {	"UI/Prefab/CleanScreenWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	DiZiWnd 			= {	"UI/Prefab/DiZiWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	PlantTreeWnd 		= {	"UI/Prefab/PlantTreeWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	DayToNightWnd 		= {	"UI/Prefab/DayToNightWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	FireCageWnd 		= {	"UI/Prefab/FireCageWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},

	--ShiTuLevelInfoWnd = { "UI/Prefab/ShiTu/ShiTuLevelInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--圣诞2019
	--RescueChristmasTreeStateWnd = { "UI/Prefab/Festival_Christmas/Christmas2019/RescueChristmasTreeStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	--ChristmasDisplayWnd = { "UI/Prefab/Festival_Christmas/Christmas2019/ChristmasDisplayWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	--GrabChristmasGiftWnd = { "UI/Prefab/Festival_Christmas/Christmas2019/GrabChristmasGiftWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--GrabChristmasGiftRankWnd = { "UI/Prefab/Festival_Christmas/Christmas2019/GrabChristmasGiftRankWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	--GrabGiftTopRightWnd = { "UI/Prefab/Festival_Christmas/Christmas2019/GrabGiftTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},

	CommonConfirmWithInfoWnd = { "UI/Prefab/common/CommonConfirmWithInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CityWarDeclareRankWnd = { "UI/Prefab/citywar/CityWarDeclareRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--商城
	FashionPreviewTryToBuyWnd =  { "UI/Prefab/FashionPreviewTryToBuyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--	元旦帮会大票选
	GuildVoteWnd 			= {	"UI/Prefab/GuildVoteWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	GuildVoteEditWnd 		= {	"UI/Prefab/GuildVoteEditWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	GuildVoteDetailWnd 		= {	"UI/Prefab/GuildVoteDetailWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	GuildVoteSelectWnd 		= {	"UI/Prefab/GuildVoteSelectWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	GuildVoteThanksWnd 		= {	"UI/Prefab/GuildVoteThanksWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},

	LeafPicWnd 				= {	"UI/Prefab/LeafPicWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},

	--外观
	PlayerFashionAttributeWnd = { "UI/Prefab/Appearance2019/PlayerFashionAttributeWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	AppearanceWnd    = { "UI/Prefab/Appearance/AppearanceWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	AppearanceSettingWnd = { "UI/Prefab/Appearance/AppearanceSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AppearanceFashionUnlockWnd = { "UI/Prefab/Appearance/AppearanceFashionUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AppearanceFashionEvaluationWnd = { "UI/Prefab/Appearance/AppearanceFashionEvaluationWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AppearanceFashionTransformWnd = { "UI/Prefab/Appearance/AppearanceFashionTransformWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AppearanceFashionPresentWnd = { "UI/Prefab/Appearance/AppearanceFashionPresentWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AppearanceFashionTabenWnd = { "UI/Prefab/Appearance/AppearanceFashionTabenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AppearanceShopWnd = { "UI/Prefab/Appearance/AppearanceShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	SchoolContributionWnd 	= {	"UI/Prefab/SchoolContribution/SchoolContributionWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},

	GuideRegionWnd = { "UI/Prefab/Guide/GuideRegionWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	HouseChristmasGiftWnd = {	"UI/Prefab/House/HouseChristmasGiftWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},

	ChunJieSeriesTaskWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2020/ChunJieSeriesTaskWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ChunJieSeriesTaskDescWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2020/ChunJieSeriesTaskDescWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ChunJieSeriesTaskRewardWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2020/ChunJieSeriesTaskRewardWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	TieChunLianTopRightWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2020/TieChunLianTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	TieChunLianRankWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2020/TieChunLianRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	TieChunLianResultWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2020/TieChunLianResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	FurnitureColorWnd = { "UI/Prefab/House/FurnitureColorWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	CrossDouDiZhuRankWnd = { "UI/Prefab/DouDiZhu/CrossDouDiZhuRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	CrossDouDiZhuApplyWnd = { "UI/Prefab/DouDiZhu/CrossDouDiZhuApplyWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	CrossDouDiZhuVoteWnd = { "UI/Prefab/DouDiZhu/CrossDouDiZhuVoteWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	CrossDouDiZhuResultWnd = { "UI/Prefab/DouDiZhu/CrossDouDiZhuResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	CrossDouDiZhuShareWnd = { "UI/Prefab/DouDiZhu/CrossDouDiZhuShareWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	CrossDouDiZhuRankResultWnd = { "UI/Prefab/DouDiZhu/CrossDouDiZhuRankResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	DouDiZhuGuildResultWnd = { "UI/Prefab/DouDiZhu/DouDiZhuGuildResultWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	DouDiZhuHouseResultWnd = { "UI/Prefab/DouDiZhu/DouDiZhuHouseResultWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	MiniDouDiZhuWnd = { "UI/Prefab/DouDiZhu/MiniDouDiZhuWnd.prefab", UIParentType.BASE_UI_ROOT,false,false},
	RemoteDouDiZhuApplyListWnd = { "UI/Prefab/DouDiZhu/RemoteDouDiZhuApplyListWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	RemoteDouDiZhuInviteWnd = { "UI/Prefab/DouDiZhu/RemoteDouDiZhuInviteWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	RemoteDouDiZhuInviteDlg = { "UI/Prefab/DouDiZhu/RemoteDouDiZhuInviteDlg.prefab", UIParentType.POP_UI_ROOT,false,false},

	DouDiZhu2020ApplyWnd = { "UI/Prefab/DouDiZhu/DouDiZhu2020ApplyWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	DouDiZhu2020RankWnd = { "UI/Prefab/DouDiZhu/DouDiZhu2020RankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	DouDiZhu2020ResultWnd = { "UI/Prefab/DouDiZhu/DouDiZhu2020ResultWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	ShengXiaoCardWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ShengXiaoCardWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ShengXiaoCardEntryWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ShengXiaoCardEntryWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ShengXiaoCardConfirmWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ShengXiaoCardConfirmWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ShengXiaoCardChooseWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ShengXiaoCardChooseWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ShengXiaoCardResultWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ShengXiaoCardResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	ShengXiaoCardPopupMenu = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ShengXiaoCardPopupMenu.prefab", UIParentType.POP_UI_ROOT,false,true},
	ShengXiaoCardRankWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ShengXiaoCardRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	HLPYHelpWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/HLPYHelpWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	ChunJie2022HeKaWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/ChunJie2022HeKaWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	XunZhaoNianWeiWnd = { "UI/Prefab/Festival_ChunJie/ChunJie2022/XunZhaoNianWeiWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	HouseWoodPileStatusWnd = { "UI/Prefab/House/HouseWoodPileStatusWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	HouseWoodPileSwitchLevelWnd = { "UI/Prefab/House/HouseWoodPileSwitchLevelWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	HouseWoodPileDataHistoryWnd = { "UI/Prefab/House/HouseWoodPileDataHistoryWnd.prefab", UIParentType.POP_UI_ROOT,false,true},


	SchoolTaskWishingWnd 	= {	"UI/Prefab/SchoolTask/SchoolTaskWishingWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	SchoolTaskOptionWnd 	= {	"UI/Prefab/SchoolTask/SchoolTaskOptionWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	SchoolTaskTranscriptWnd = {	"UI/Prefab/SchoolTask/SchoolTaskTranscriptWnd.prefab",	UIParentType.POP_UI_ROOT,false,true},
	--情人节
	QYXTSignUpWnd = { "UI/Prefab/Festival_Valentine/ValenTine2020/QYXTSignUpWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QYXTResultWnd = { "UI/Prefab/Festival_Valentine/ValenTine2020/QYXTResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QYXTTopRightWnd = { "UI/Prefab/Festival_Valentine/ValenTine2020/QYXTTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	QYXTReliveWnd = { "UI/Prefab/Festival_Valentine/ValenTine2020/QYXTReliveWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	ChooseProofWnd = { "UI/Prefab/ZhuJueJuQing/ChooseProofWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	RabbitJumpWnd = { "UI/Prefab/ZhuJueJuQing/RabbitJumpWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	RabbitMakeDrugWnd = { "UI/Prefab/ZhuJueJuQing/RabbitMakeDrugWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	WinterHouseUnlockPreviewWnd = { "UI/Prefab/House/WinterHouseUnlockPreviewWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	--清明节2020
	QingMingSignRankWnd = { "UI/Prefab/Festival_QingMing/QingMing2020/QingMingSignRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QingMingBattleWnd = { "UI/Prefab/Festival_QingMing/QingMing2020/QingMingBattleWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QingMingJieZiXiaoChouWnd = { "UI/Prefab/Festival_QingMing/QingMing2020/QingMingJieZiXiaoChouWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QingMingJieZiSucceedWnd = { "UI/Prefab/Festival_QingMing/QingMing2020/QingMingJieZiSucceedWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QingMingFengWuZhiWnd = { "UI/Prefab/Festival_QingMing/QingMing2020/QingMingFengWuZhiWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	QingMingJieZiTipWnd = { "UI/Prefab/Festival_QingMing/QingMing2020/QingMingJieZiTipWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	RanFaJiMakeWnd = { "UI/Prefab/RanFa/RanFaJiMakeWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	RanFaRecipeGotWnd = { "UI/Prefab/RanFa/RanFaRecipeGotWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	RanFaJiBookWnd = { "UI/Prefab/RanFa/RanFaJiBookWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	RanFaMainWnd = { "UI/Prefab/RanFa/RanFaMainWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	RanFaRecipeChooseWnd = { "UI/Prefab/RanFa/RanFaRecipeChooseWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	MessageBoxTimeLimitWnd = { "UI/Prefab/MessageBoxTimeLimitWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},

	DebitNoteWnd = {"UI/Prefab/JuQing/DebitNoteWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ScreenBloodFogWnd = {"UI/Prefab/JuQing/ScreenBloodFogWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	PetCatWnd = {"UI/Prefab/JuQing/PetCatWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--制衣玩法
	MakeClothesWnd = {"UI/Prefab/JuQing/MakeClothes/MakeClothesWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MakeClothesPatternWnd = {"UI/Prefab/JuQing/MakeClothes/MakeClothesPatternWnd.prefab", UIParentType.POP_UI_ROOT, false,true},
	MakeClothesResultWnd = {"UI/Prefab/JuQing/MakeClothes/MakeClothesResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MakeJiaYiWnd = {"UI/Prefab/JuQing/MakeClothes/MakeJiaYiWnd.prefab", UIParentType.POP_UI_ROOT, false,true},

	HeShengZiJinWnd = {"UI/Prefab/JuQing/HeShengZiJinWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--2020儿童节活动
	ChildrenDayPlayChoose2020Wnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2020/ChildrenDayPlayChoose2020Wnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ChildrenDayPlayChooseBall2020Wnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2020/ChildrenDayPlayChooseBall2020Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChildrenDayPlayFightInfo2020Wnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2020/ChildrenDayPlayFightInfo2020Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChildrenDayPlayResult2020Wnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2020/ChildrenDayPlayResult2020Wnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChildrenDayPlayShow2020Wnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2020/ChildrenDayPlayShow2020Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChildrenDayPlayTopRight2020Wnd = { "UI/Prefab/Festival_LiuYi/ChildrenDay2020/ChildrenDayPlayTopRight2020Wnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	-- 周年庆吃鸡
	PUBGStartWnd = { "UI/Prefab/Festival_ZhouNianQing/PUBG/PUBGStartWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PUBGSelfPackageWnd = { "UI/Prefab/Festival_ZhouNianQing/PUBG/PUBGSelfPackageWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PUBGSkillSetWnd = { "UI/Prefab/Festival_ZhouNianQing/PUBG/PUBGSkillSetWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PUBGLickingBagWnd = { "UI/Prefab/Festival_ZhouNianQing/PUBG/PUBGLickingBagWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PUBGResultWnd = { "UI/Prefab/Festival_ZhouNianQing/PUBG/PUBGResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	ShiTuTrainingHandbookWnd = {"UI/Prefab/ShiTu/ShiTuTrainingHandbookWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TuDiPlayerListWnd = {"UI/Prefab/ShiTu/TuDiPlayerListWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShiTuNewTrainingHandbookWnd = {"UI/Prefab/ShiTu/ShiTuNewTrainingHandbookWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShiTuWenDaoRewardWnd = {"UI/Prefab/ShiTu/ShiTuWenDaoRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	ItemWithDetailMessageChooseWnd = {"UI/Prefab/Common/ItemWithDetailMessageChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--停服维护
	ServerMaintenanceWnd = {"UI/Prefab/ServerMaintenanceWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	--2020五一地狱誓空
	DiYuShiKongWnd = { "UI/Prefab/Festival_WuYi/WuYi2020/DiYuShiKongWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DiYuShiKongStateWnd = { "UI/Prefab/Festival_WuYi/WuYi2020/DiYuShiKongStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},

	--2020五一全民货运
	QMHuoYunSubWnd = { "UI/Prefab/Festival_WuYi/WuYi2020/QMHuoYunSubWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QMHuoYunFHLStateWnd = { "UI/Prefab/Festival_WuYi/WuYi2020/QMHuoYunFHLStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	QMHuoYunFHLResultWnd = { "UI/Prefab/Festival_WuYi/WuYi2020/QMHuoYunFHLResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--端午节2020
	DwWusesiHeCheng2020Wnd	= {	"UI/Prefab/Festival_DuanWu/DuanWu2020/DwWusesiHeCheng2020Wnd.prefab", 	UIParentType.POP_UI_ROOT, false, true},
	DwWusesiTaben2020Wnd	= {		"UI/Prefab/Festival_DuanWu/DuanWu2020/DwWusesiTaben2020Wnd.prefab",		UIParentType.POP_UI_ROOT, false, true},
	JiuGeShiHunTopAndRightWnd =  { "UI/Prefab/Festival_DuanWu/DuanWu2020/JiuGeShiHunTopAndRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	JiuGeShiHunResultWnd	= {		"UI/Prefab/Festival_DuanWu/DuanWu2020/JiuGeShiHunResultWnd.prefab",		UIParentType.POP_UI_ROOT, false, false},

	--端午节2021
	DragonBoatGamePlayWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2021/DragonBoatGamePlayWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	DuanWu2021TopRightWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2021/DuanWu2021TopRightWnd.prefab",UIParentType.BASE_UI_ROOT, false, true},
	DragonBoatControllerWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2021/DragonBoatControllerWnd.prefab",UIParentType.BASE_UI_ROOT, false, true},
	DragonBoatResultWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2021/DragonBoatResultWnd.prefab",UIParentType.POP_UI_ROOT, false, false},
	--第三届家园大赛翻牌子
	HouseCompetitionFanPaiZiWnd = { "UI/Prefab/House/HouseCompetitionFanPaiZiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	HouseCompetitionFanPaiZiRewardWnd = { "UI/Prefab/House/HouseCompetitionFanPaiZiRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HouseCompetitionFanPaiZiAddChancesWnd = { "UI/Prefab/House/HouseCompetitionFanPaiZiAddChancesWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--白蝶风波
	ButterflyCrisisNoticeWnd = { "UI/Prefab/ButterflyCrisis/ButterflyCrisisNoticeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ButterflyCrisisMainWnd = { "UI/Prefab/ButterflyCrisis/ButterflyCrisisMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ButterflyCrisisStoryWnd = { "UI/Prefab/ButterflyCrisis/ButterflyCrisisStoryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ButterflyCrisisRMBWnd = { "UI/Prefab/ButterflyCrisis/ButterflyCrisisRMBWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ButterflyCrisisRewardWnd = { "UI/Prefab/ButterflyCrisis/ButterflyCrisisRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--调香玩法
	TiaoXiangPlayWnd = { "UI/Prefab/JuQing/TiaoXiangPlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--角色迁移
	AccountTransferWnd = { "UI/Prefab/AccountTransfer/AccountTransferWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	NPCXuanMeiWnd = { "UI/Prefab/NPCXuanMei/NPCXuanMeiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	HouseUnlockSkyboxWnd = { "UI/Prefab/House/HouseUnlockSkyboxWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	LingShouConsumeBabyExpItemWnd = { "UI/Prefab/LingShou/LingShouConsumeBabyExpItemWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	LingShouBabyInjectExpWnd = { "UI/Prefab/LingShou/LingShouBabyInjectExpWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	LingShouBabyHandWashWnd = { "UI/Prefab/LingShou/LingShouBabyHandWashWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	LingShouBabyAutoWashWnd = { "UI/Prefab/LingShou/LingShouBabyAutoWashWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	LingShouMarriageMatchSelectWnd = { "UI/Prefab/LingShou/LingShouMarriageMatchSelectWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	--DuanWuDaZuoZhanTopRightWnd={"UI/Prefab/Festival_DuanWu/DuanWu/DuanWuDaZuoZhanTopRightWnd.prefab",UIParentType.BASE_UI_ROOT,false,true},
	--DuanWuDaZuoZhanRankWnd={"UI/Prefab/Festival_DuanWu/DuanWu/DuanWuDaZuoZhanRankWnd.prefab",UIParentType.BASE_UI_ROOT,false,false},


	DouDiZhuRankWnd = { "UI/Prefab/DouDiZhu/DouDiZhuRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--WaKuangRankWnd = { "UI/Prefab/Festival_WuYi/WuYi/WaKuangRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	CommonItemListWnd = { "UI/Prefab/Common/CommonItemListWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	ItemExchange4To1Wnd = { "UI/Prefab/ItemExchange4To1Wnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	--2020中元节
	XYLPWnd = { "UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2020/XYLPWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	XYLPTipWnd = { "UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2020/XYLPTipWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	DesireMirrorWnd = { "UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2020/DesireMirrorWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	CommonRankWnd = { "UI/Prefab/Common/CommonRankWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	--家园截图举报
	HouseScreenshotReportWnd = { "UI/Prefab/House/HouseScreenshotReportWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	--代言人
	SpokesmanHireContractWnd = { "UI/Prefab/Spokesman/SpokesmanHireContractWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	SpokesmanCardsWnd = { "UI/Prefab/Spokesman/SpokesmanCardsWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	SpokesmanDialogWnd = { "UI/Prefab/Spokesman/SpokesmanDialogWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	SpokesmanPreviewWnd = { "UI/Prefab/Spokesman/SpokesmanPreviewWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	SpokesmanCardsComposeWnd = { "UI/Prefab/Spokesman/SpokesmanCardsComposeWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	SpokesmanCardsComposeResultWnd = { "UI/Prefab/Spokesman/SpokesmanCardsComposeResultWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	SpokesmanExpressionWnd = { "UI/Prefab/House/Spokesman/SpokesmanExpressionWnd.prefab",UIParentType.POP_UI_ROOT,false,true},
	SpokesmanPawnshopWnd = { "UI/Prefab/House/Spokesman/SpokesmanPawnshopWnd.prefab",UIParentType.POP_UI_ROOT,false,true},
	SpokesmanPawnCertificateWnd = { "UI/Prefab/House/Spokesman/SpokesmanPawnCertificateWnd.prefab",UIParentType.POP_UI_ROOT,false,true},

	SpokesmanHouseZhongChouWnd = { "UI/Prefab/House/Spokesman/SpokesmanHouseZhongChouWnd.prefab",UIParentType.POP_UI_ROOT,false,true},

	--二哈
	CommonButtonAtPosWnd = { "UI/Prefab/Common/CommonButtonAtPosWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	HaoYiXingCardsWnd = { "UI/Prefab/HaoYiXing/HaoYiXingCardsWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	HaoYiXingCardPreviewWnd = { "UI/Prefab/HaoYiXing/HaoYiXingCardPreviewWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	HaoYiXingCardsComposeWnd = { "UI/Prefab/HaoYiXing/HaoYiXingCardsComposeWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	HaoYiXingCardsComposeResultWnd = { "UI/Prefab/HaoYiXing/HaoYiXingCardsComposeResultWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	HaoYiXingCardStoryWnd = { "UI/Prefab/HaoYiXing/HaoYiXingCardStoryWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	GetNewPersonalSpaceBgWnd = { "UI/Prefab/HaoYiXing/GetNewPersonalSpaceBgWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	GetHaoYiXingHaiTangWnd = { "UI/Prefab/HaoYiXing/GetHaoYiXingHaiTangWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	HaoYiXingExperienceTasksWnd = { "UI/Prefab/HaoYiXing/HaoYiXingExperienceTasksWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	--家园装饰物模板
	ZswTemplateEditWnd = { "UI/Prefab/JyZhuangShiWuTemplate/ZswTemplateEditWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZswTemplateFillingComfirmWnd = { "UI/Prefab/JyZhuangShiWuTemplate/ZswTemplateFillingComfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZswTemplateTopRightWnd = { "UI/Prefab/JyZhuangShiWuTemplate/ZswTemplateTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZswTemplatePreviewWnd = { "UI/Prefab/JyZhuangShiWuTemplate/ZswTemplatePreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZswTemplateComponentListWnd = { "UI/Prefab/JyZhuangShiWuTemplate/ZswTemplateComponentListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--
	Carnival2020RealNameWnd = { "UI/Prefab/Welfare/Carnival2020RealNameWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Carnival2020VipTicketWnd = { "UI/Prefab/Welfare/Carnival2020VipTicketWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--
	QiugouWnd = { "UI/Prefab/Qiugou/QiugouWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QiugouBuyWnd = { "UI/Prefab/Qiugou/QiugouBuyWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QiugouSellWnd = { "UI/Prefab/Qiugou/QiugouSellWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	ChushouDetailWnd = { "UI/Prefab/Qiugou/ChushouDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	ZhuangshiwuPreviewWnd = { "UI/Prefab/House/ZhuangshiwuPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 双十一活动
	Shuangshiyi2020FashionPreviewWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020FashionPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2020VoucherWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020VoucherWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2020ZhongCaoWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020ZhongCaoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--2020万圣节
	MengGuiJieSignWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/MengGuiJieSignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengGuiJiePiPeiWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/MengGuiJiePiPeiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengGuiJieTipWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/MengGuiJieTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengGuiJieResultWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/MengGuiJieResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DaoDanTipEnterWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/DaoDanTipEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TerrifyItemUseWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/TerrifyItemUseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MengGuiJieSkillTipWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/MengGuiJieSkillTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TerrifyItemUseEntryWnd = { "UI/Prefab/Festival_Halloween/Halloween2020/TerrifyItemUseEntryWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	Shuangshiyi2020ClearTrolleyResultWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020ClearTrolleyResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2020ClearTrolleyEnterWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020ClearTrolleyEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2020ClearTrolleyPlayingWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020ClearTrolleyPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	Shuangshiyi2020LotteryWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020LotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2020LotteryRankWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020LotteryRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2020LotteryRewardWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2020/Shuangshiyi2020LotteryRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--安期岛之战
	AnQiDaoResultWnd = { "UI/Prefab/AnQiDao/AnQiDaoResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	AnQiDaoSignalWnd = { "UI/Prefab/AnQiDao/AnQiDaoSignalWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	AnQiDaoStateWnd = { "UI/Prefab/AnQiDao/AnQiDaoStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	AnQiDaoTipWnd = { "UI/Prefab/AnQiDao/AnQiDaoTipWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 仙职窗口迭代
	XianZhiExtendWnd = { "UI/Prefab/XianZhi/XianZhiExtendWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 手绘界面迭代
	InkDrawWnd = { "UI/Prefab/ZhuJueJuQing/InkDrawWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 2021元宵节
	YuanXiao2021QiYuanWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2021/YuanXiao2021QiYuanWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	YuanXiao2021TangYuanEnterWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2021/YuanXiao2021TangYuanEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2021TangYuanSettlementWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2021/YuanXiao2021TangYuanSettlementWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--烟花编辑器
	YanHuaDesignWnd = { "UI/Prefab/YanHuaEditor/YanHuaDesignWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	YanHuaLiBaoEditorWnd = { "UI/Prefab/YanHuaEditor/YanHuaLiBaoEditorWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	YanHuaViewWnd = { "UI/Prefab/YanHuaEditor/YanHuaViewWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	YanHuaReportWnd = { "UI/Prefab/YanHuaEditor/YanHuaScreenshotReportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YanHuaSinglePreviewWnd = { "UI/Prefab/YanHuaEditor/YanHuaSinglePreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 寒假2021 天成酒壶
	TcjhMainWnd 		= {		"UI/Prefab/Festival_HanJia/HanJia2021/TcjhMainWnd.prefab", 			UIParentType.POP_UI_ROOT, false, false},
	TcjhRewardWnd 		= {	"UI/Prefab/Festival_HanJia/HanJia2021/TcjhRewardWnd.prefab", 		UIParentType.POP_UI_ROOT, false, false},
	TcjhBuyAdvPassWnd 	= {"UI/Prefab/Festival_HanJia/HanJia2021/TcjhBuyAdvPassWnd.prefab", 	UIParentType.POP_UI_ROOT, false, false},
	TcjhBuyLevelWnd 	= {	"UI/Prefab/Festival_HanJia/HanJia2021/TcjhBuyLevelWnd.prefab", 		UIParentType.POP_UI_ROOT, false, false},
	TcjhRideSelectWnd	= {"UI/Prefab/Festival_HanJia/HanJia2021/TcjhRideSelectWnd.prefab",	UIParentType.POP_UI_ROOT, false, false},

	XueJingKuangHuanStartWnd 	= {"UI/Prefab/Festival_HanJia/HanJia2022/XueJingKuangHuanStartWnd.prefab", 	UIParentType.POP_UI_ROOT, false, true},
	XueJingKuangHuanResultWnd 	= {"UI/Prefab/Festival_HanJia/HanJia2022/XueJingKuangHuanResultWnd.prefab", 	UIParentType.POP_UI_ROOT, false, false}, --离开副本才弹，需要避免切场景被关闭
	XueJingKuangHuanStateWnd 	= {"UI/Prefab/Festival_HanJia/HanJia2022/XueJingKuangHuanStateWnd.prefab", 	UIParentType.BASE_UI_ROOT, false, true},
	
	-- 寒假2023
	SnowManPVEPlayWnd = {"UI/Prefab/Festival_HanJia/HanJia2023/SnowManPVEPlayWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowManPVEResultWnd = {"UI/Prefab/Festival_HanJia/HanJia2023/SnowManPVEResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowManPVERankWnd = {"UI/Prefab/Festival_HanJia/HanJia2023/SnowManPVERankWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	SnowAdventureSingleStageMainWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureSingleStageMainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventureStageDetailWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureStageDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventureSingleStageRankWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureSingleStageRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventureMultipleStageMainWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureMultipleStageMainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventureMultipleTeamWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureMultipleTeamWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventureSnowManWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureSnowManWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventureResultWnd = {"UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventurePropertyDetailWnd = {"UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventurePropertyDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SnowAdventureStatwnd = {"UI/Prefab/Festival_HanJia/HanJia2023/SnowAdventureStatwnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	HanJia2023LotteryWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/HanJia2023LotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	HanJia2023PassDetailWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/HanJia2023PassDetailWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	HanJia2023PassMainWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/HanJia2023PassMainWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	HanJia2023MainWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/HanJia2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CommonGetRewardWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/CommonGetRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SpringCouponsWnd = { "UI/Prefab/Festival_HanJia/HanJia2023/SpringCouponsWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	SanJieFengHuaLuWnd  = {	"UI/Prefab/WorldEvent2021/SanJieFengHuaLuWnd.prefab",	UIParentType.POP_UI_ROOT, false, false},
	SanJieFengHuaLuWishWnd 	= {	"UI/Prefab/WorldEvent2021/SanJieFengHuaLuWishWnd.prefab",	UIParentType.POP_UI_ROOT, false, false},
	SanJieFengHuaLuEntryWnd = { "UI/Prefab/WorldEvent2021/SanJieFengHuaLuEntryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	YuanDanShareWnd = { "UI/Prefab/Festival_YuanDan/YuanDan/YuanDanShareWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	PetAdventureStartWnd = { "UI/Prefab/Festival_HanJia/HanJia2021/PetAdventureStartWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PetAdventureStateWnd = { "UI/Prefab/Festival_HanJia/HanJia2021/PetAdventureStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	PetAdventureResultWnd = { "UI/Prefab/Festival_HanJia/HanJia2021/PetAdventureResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChristmasCreateSnowmanStateWnd = { "UI/Prefab/Festival_Christmas/Christmas/ChristmasCreateSnowmanStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	ChristmasCreateSnowmanSignWnd = { "UI/Prefab/Festival_Christmas/Christmas/ChristmasCreateSnowmanSignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	QingRenJie2021RankWnd = { "UI/Prefab/Festival_Valentine/QingRenJie2021/QingRenJie2021RankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QingRenJie2021EntryWnd = { "UI/Prefab/Festival_Valentine/QingRenJie2021/QingRenJie2021EntryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QingRenJie2021PlayStatusWnd = { "UI/Prefab/Festival_Valentine/QingRenJie2021/QingRenJie2021PlayStatusWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	ExpressionEquipRenewalWnd = { "UI/Prefab/ExpressionEquipRenewalWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 125技能物品合成
	Compound125SkillItemWnd = { "UI/Prefab/Skill/Compound125SkillItemWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QieCuoPopupMenuWnd = { "UI/Prefab/QieCuoPopupMenuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QieCuoMessageBox = { "UI/Prefab/QieCuoMessageBox.prefab", UIParentType.POP_UI_ROOT, false, true},
	VehicleRenewalWnd = { "UI/Prefab/ZuoQi/VehicleRenewalWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	VehicleBatchRenewalFeesWnd = { "UI/Prefab/ZuoQi/VehicleBatchRenewalFeesWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TianQiMysticalShopSettingWnd = { "UI/Prefab/ZuoQi/TianQiMysticalShopSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--自立师门相关
	SoulCoreWnd = { "UI/Prefab/ZongMen/SoulCoreWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	OtherSoulCoreWnd = { "UI/Prefab/ZongMen/OtherSoulCoreWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SoulCoreCondenseWnd = { "UI/Prefab/ZongMen/SoulCoreCondenseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenCreateIntroductionWnd = { "UI/Prefab/ZongMen/ZongMenCreateIntroductionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenCreationWnd = { "UI/Prefab/ZongMen/ZongMenCreationWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenTuDiJoiningWnd = { "UI/Prefab/ZongMen/ZongMenTuDiJoiningWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MingGeJianDingWnd = { "UI/Prefab/ZongMen/MingGeJianDingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NewMingGeJianDingWnd = { "UI/Prefab/ZongMen/NewMingGeJianDingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuXingMingGeWnd = { "UI/Prefab/ZongMen/WuXingMingGeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenSearchWnd = { "UI/Prefab/ZongMen/ZongMenSearchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenGuiZeWnd = { "UI/Prefab/ZongMen/ZongMenGuiZeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenJoiningWnd = { "UI/Prefab/ZongMen/ZongMenJoiningWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	InviteToJoinZongMenWnd = { "UI/Prefab/ZongMen/InviteToJoinZongMenWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenTopRightWnd = { "UI/Prefab/ZongMen/ZongMenTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ZongMenMainWnd = { "UI/Prefab/ZongMen/ZongMenMainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenChengWeiWnd = { "UI/Prefab/ZongMen/ZongMenChengWeiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenWeiJieWnd = { "UI/Prefab/ZongMen/ZongMenWeiJieWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenRenMianMessageBoxWnd = { "UI/Prefab/ZongMen/ZongMenRenMianMessageBoxWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenJurisdictionSettingWnd = { "UI/Prefab/ZongMen/ZongMenJurisdictionSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenHistoryWnd = { "UI/Prefab/ZongMen/ZongMenHistoryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenChangeWeiJieWnd = { "UI/Prefab/ZongMen/ZongMenChangeWeiJieWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZhuoYaoMainWnd = { "UI/Prefab/ZongMen/ZhuoYaoMainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	YaoGuaiInfoWnd = { "UI/Prefab/ZongMen/YaoGuaiInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	WuXingAvoidSettingWnd = { "UI/Prefab/ZongMen/WuXingAvoidSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShiMenRuinedWnd = { "UI/Prefab/ZongMen/ShiMenRuinedWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	GuardZongMenWnd = { "UI/Prefab/ZongMen/GuardZongMenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuardShiMenRankWnd = { "UI/Prefab/ZongMen/GuardShiMenRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenJusticeContributeWnd = { "UI/Prefab/ZongMen/ZongMenJusticeContributeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenJusticeContributeChooseWnd = { "UI/Prefab/ZongMen/ZongMenJusticeContributeChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenChangeNameWnd = { "UI/Prefab/ZongMen/ZongMenChangeNameWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenChangeSkyboxWnd = { "UI/Prefab/ZongMen/ZongMenChangeSkyboxWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChangeRuMenBiaoZhunWnd = { "UI/Prefab/ZongMen/ChangeRuMenBiaoZhunWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenWuXingNiuZhuanWnd = { "UI/Prefab/ZongMen/ZongMenWuXingNiuZhuanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NewZongMenWuXingNiuZhuanWnd = { "UI/Prefab/ZongMen/NewZongMenWuXingNiuZhuanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 自立师门炼化相关
	RefineMonsterButtonWnd = { "UI/Prefab/ZongMen/RefineMonsterButtonWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	RefineMonsterEvilBuffConfirmWnd = { "UI/Prefab/ZongMen/RefineMonsterEvilBuffConfirmWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	RefinePeopleButtonWnd = { "UI/Prefab/ZongMen/RefinePeopleButtonWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	RefineMonsterCircleWnd = { "UI/Prefab/ZongMen/RefineMonsterCircleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	RefineMonsterItemWnd = { "UI/Prefab/ZongMen/RefineMonsterItemWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	RefineMonsterListWnd = { "UI/Prefab/ZongMen/RefineMonsterListWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	RefineMonsterRecordWnd = { "UI/Prefab/ZongMen/RefineMonsterRecordWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	RefineMonsterLingliDescWnd = { "UI/Prefab/ZongMen/RefineMonsterLingliDescWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	RefineMonsterChooseWnd = { "UI/Prefab/ZongMen/RefineMonsterChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	RefineMonsterItemChooseWnd = { "UI/Prefab/ZongMen/RefineMonsterItemChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	RefineMonsterItemAccessWnd = { "UI/Prefab/ZongMen/RefineMonsterItemAccessWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenXuanMeiButtonWnd = { "UI/Prefab/ZongMen/ZongMenXuanMeiButtonWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	SectPrisonListWnd = { "UI/Prefab/ZongMen/SectPrisonListWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 克符相关
	AntiProfessionUnlockWnd = { "UI/Prefab/ZongMen/AntiProfessionUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	AntiProfessionLevelUpWnd = { "UI/Prefab/ZongMen/AntiProfessionLevelUpWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	AntiProfessionMakeWnd = { "UI/Prefab/ZongMen/AntiProfessionMakeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	AntiProfessionSwapWnd = { "UI/Prefab/ZongMen/AntiProfessionSwapWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	AntiProfessionConvertWnd = { "UI/Prefab/ZongMen/AntiProfessionConvertWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	AntiProfessionConvertChooseWnd = { "UI/Prefab/ZongMen/AntiProfessionConvertChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	AntiProfessionConvertResultWnd = { "UI/Prefab/ZongMen/AntiProfessionConvertResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 宗门迭代相关相关
	ZongMenNewSearchWnd = { "UI/Prefab/ZongMen/ZongMenNewSearchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZhuoYaoHuaFuWnd = { "UI/Prefab/ZongMen/ZhuoYaoHuaFuWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZongMenJoinStandardSearchResultWnd = { "UI/Prefab/ZongMen/ZongMenJoinStandardSearchResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 宗派修行
	ZongMenPracticeRuleAndRankWnd = { "UI/Prefab/ZongMen/ZongMenPracticeRuleAndRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenPracticeReliveWnd = { "UI/Prefab/ZongMen/ZongMenPracticeReliveWnd.prefab", UIParentType.POP_UI_ROOT,false, true},
	ZongMenPracticeStateWnd = { "UI/Prefab/ZongMen/ZongMenPracticeStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	CommonItemChooseWnd = { "UI/Prefab/Common/CommonItemChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CommonItemSelectWnd = { "UI/Prefab/Common/CommonItemSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	ZhuoRenTipWnd = { "UI/Prefab/ZongMen/ZhuoRenTipWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	EnemyZongPaiArrestListWnd = { "UI/Prefab/ZongMen/EnemyZongPaiArrestListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	EnemyZongPaiWnd = { "UI/Prefab/ZongMen/EnemyZongPaiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZongMenEnterChooseWnd = { "UI/Prefab/ZongMen/ZongMenEnterChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	MusicBoxPopControlWnd = { "UI/Prefab/MusicBox/MusicBoxPopControlWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	MusicListWnd = { "UI/Prefab/MusicBox/MusicListWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--BaoZhuPlayEndWnd = { "UI/Prefab/Festival_ChunJie/BaoZhuPlay/BaoZhuPlayEndWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--BaoZhuPlayShowWnd = { "UI/Prefab/Festival_ChunJie/BaoZhuPlay/BaoZhuPlayShowWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--BaoZhuPlayTopRightWnd = { "UI/Prefab/Festival_ChunJie/BaoZhuPlay/BaoZhuPlayTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	ErHaOpenJinNangWnd = { "UI/Prefab/ErHaOpenJinNangWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	OffWorldPassMainWnd	= {"UI/Prefab/OffWorldPass/OffWorldPassMainWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	OffWorldPassReward1Wnd	= {"UI/Prefab/OffWorldPass/OffWorldPassReward1Wnd.prefab",UIParentType.POP_UI_ROOT, false, false},
	OffWorldPassReward2Wnd	= {"UI/Prefab/OffWorldPass/OffWorldPassReward2Wnd.prefab",UIParentType.POP_UI_ROOT, false, false},

	MiniMapWeatherTipWnd = {"UI/Prefab/MiniMapWeatherTipWnd.prefab",UIParentType.POP_UI_ROOT, false, true},

	HouseUnlockWallpaperWnd = { "UI/Prefab/House/HouseUnlockWallpaperWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HouseUnlockJianZhuWnd = { "UI/Prefab/House/HouseUnlockJianZhuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	InviteNpcJoinZongMenWnd = { "UI/Prefab/NpcJoinZongMen/InviteNpcJoinZongMenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NpcInvitedInfoWnd = { "UI/Prefab/NpcJoinZongMen/NpcInvitedInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NpcInvitedEnchantAbilityInfoWnd = { "UI/Prefab/NpcJoinZongMen/NpcInvitedEnchantAbilityInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BuyIncreaseDaoyiValueItemWnd = { "UI/Prefab/NpcJoinZongMen/BuyIncreaseDaoyiValueItemWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TrainNpcFavorWnd = { "UI/Prefab/NpcJoinZongMen/TrainNpcFavorWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	InsectTaskFindLingShouWnd = { "UI/Prefab/NpcJoinZongMen/InsectTaskFindLingShouWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SystemRecommendGiftWnd = { "UI/Prefab/SystemRecommendGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	QingMingCGYJMainWnd = { "UI/Prefab/Festival_QingMing/QingMing2021/QingMingCGYJMainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QingMingNHTQResultWnd = { "UI/Prefab/Festival_QingMing/QingMing2021/QingMingNHTQResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QingMingNHTQTopRightWnd = { "UI/Prefab/Festival_QingMing/QingMing2021/QingMingNHTQTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	OffWorldPlay1ShopWnd = { "UI/Prefab/OffWorldPass/OffWorldPlay1ShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OffWorldPlay1MiddleResultWnd = { "UI/Prefab/OffWorldPass/OffWorldPlay1MiddleResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	OffWorldPlay1ResultWnd = { "UI/Prefab/OffWorldPass/OffWorldPlay1ResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	OffWorldPlay1TopRightWnd = { "UI/Prefab/OffWorldPass/OffWorldPlay1TopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	OffWorldPlay2ResultWnd = { "UI/Prefab/OffWorldPass/OffWorldPlay2ResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	OffWorldPlay2TopRightWnd = { "UI/Prefab/OffWorldPass/OffWorldPlay2TopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	--家园水池
	PoolEditWnd = { "UI/Prefab/House/Pool/PoolEditWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BuyPoolComfirmWnd = { "UI/Prefab/House/Pool/BuyPoolComfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	AddPoolMaxCountWnd = { "UI/Prefab/House/Pool/AddPoolMaxCountWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	HouseTerrainEditWnd = { "UI/Prefab/House/HouseTerrainEditWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HouseExchangeGrassWnd = { "UI/Prefab/House/HouseExchangeGrassWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HouseExchangeTerrainWnd = { "UI/Prefab/House/HouseExchangeTerrainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	OffWorldPlay2Game1Wnd = { "UI/Prefab/OffWorldPass/OffWorldPlay2Game1Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OffWorldPlay2Game2Wnd = { "UI/Prefab/OffWorldPass/OffWorldPlay2Game2Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OffWorldPha1Wnd = { "UI/Prefab/OffWorldPass/OffWorldPha1Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OffWorldPha1PicWnd = { "UI/Prefab/OffWorldPass/OffWorldPha1PicWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OffWorldPassChooseWnd = { "UI/Prefab/OffWorldPass/OffWorldPassChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OffWorldPassChooseGetWnd = { "UI/Prefab/OffWorldPass/OffWorldPassChooseGetWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OffWorldPassDetailWnd = { "UI/Prefab/OffWorldPass/OffWorldPassDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	

	KunxiansuoFoeSelectWnd = { "UI/Prefab/ZongMen/KunxiansuoFoeSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	KunxiansuoUseWnd = { "UI/Prefab/ZongMen/KunxiansuoUseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CommonSearchPlayerWnd = { "UI/Prefab/Common/CommonSearchPlayerWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	HaMaoTaskMessageBox = { "UI/Prefab/JuQing/HaMaoTaskMessageBox.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 2021五一
	WuYi2021JianXinWnd = { "UI/Prefab/Festival_WuYi/WuYi2021/WuYi2021JianXinWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2021MingXingPlayingWnd = { "UI/Prefab/Festival_WuYi/WuYi2021/WuYi2021MingXingPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	WuYi2021MingXingResultWnd = { "UI/Prefab/Festival_WuYi/WuYi2021/WuYi2021MingXingResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	WuYi2022AcceptTaskWnd = { "UI/Prefab/Festival_WuYi/WuYi2022/WuYi2022AcceptTaskWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2022TopRightWnd = { "UI/Prefab/Festival_WuYi/WuYi2022/WuYi2022TopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	
	WuYi2023FYZXGamePlayWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023FYZXGamePlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023FYZXChooseTempSkillWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023FYZXChooseTempSkillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023FYZXTopRightWnd =  { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023FYZXTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	WuYi2023FYZXResultWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023FYZXResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	WuYi2023XXSCExchangeGoldWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023XXSCExchangeGoldWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023XXSCGiveSilverWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023XXSCGiveSilverWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023XXSCMainWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023XXSCMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023XXSCRewardSelectWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023XXSCRewardSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023CommonGetRewardWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023CommonGetRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023JZLYWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023JZLYWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuYi2023MainWnd = { "UI/Prefab/Festival_WuYi/WuYi2023/WuYi2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 六一
	LiuYi2021TangGuoEnterWnd = { "UI/Prefab/LiuYi2021/LiuYi2021TangGuoEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiuYi2021TangGuoResultWnd = { "UI/Prefab/LiuYi2021/LiuYi2021TangGuoResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuYi2021TangGuoPlayingWnd = { "UI/Prefab/LiuYi2021/LiuYi2021TangGuoPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	LiuYi2021TangGuoChooseWnd = { "UI/Prefab/LiuYi2021/LiuYi2021TangGuoChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 罗云熙宗门
	LuoSpecialZongMenWnd = { "UI/Prefab/ZongMen/LuoSpecialZongMenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LuoSpecialZongMenRankWnd = { "UI/Prefab/ZongMen/LuoSpecialZongMenRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--Clubhouse
	CHCreateChatRoomWnd = { "UI/Prefab/Chat/CHCreateChatRoomWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CHApplySpeakListWnd = { "UI/Prefab/Chat/CHApplySpeakListWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CHAudienceListWnd = { "UI/Prefab/Chat/CHAudienceListWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CHDayRankWnd = { "UI/Prefab/Chat/CHDayRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CHMonthRankWnd = { "UI/Prefab/Chat/CHMonthRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CHChooseGiftWnd = { "UI/Prefab/Chat/CHChooseGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CHPrivatePasswordCheckWnd = { "UI/Prefab/Chat/CHPrivatePasswordCheckWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CHRoomIdSearchWnd = { "UI/Prefab/Chat/CHRoomIdSearchWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 2021周年庆
	ZhiBoScreenLookWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2021/ZhiBoScreenLookWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShengChenGangWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2021/ShengChenGangWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZNQZhiBoYaoQingWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2021/ZNQZhiBoYaoQingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TongQingYouLiWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2021/TongQingYouLiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TongQingYouLiChouJiangWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2021/TongQingYouLiChouJiangWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	NewHorseRaceBaseWnd = {"UI/Prefab/HorseRace/NewHorseRaceBaseWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	RepairTongTianTaSubmitSilverWnd = {"UI/Prefab/ZongMen/RepairTongTianTaSubmitSilverWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	CommonExpressionSelectionWnd = { "UI/Prefab/Expression/CommonExpressionSelectionWnd.prefab",UIParentType.POP_UI_ROOT,false,true},

	GiftSelectWnd = {"UI/Prefab/Common/GiftSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FuxiAniPreviewWnd = {"UI/Prefab/FuxiAni/FuxiAniPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	Login_Age = {"UI/Prefab/Login_Age.prefab", UIParentType.POP_UI_ROOT, false, true},
	Unity2018UpgradeWnd = { "UI/Prefab/Welfare/Unity2018UpgradeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--海钓
	HouseFishingWnd = { "UI/Prefab/SeaFishing/HouseFishingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FishBaitWnd = { "UI/Prefab/SeaFishing/FishBaitWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FishGetAndRecordWnd = { "UI/Prefab/SeaFishing/FishGetAndRecordWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FishingRodAppearanceWnd = { "UI/Prefab/SeaFishing/FishingRodAppearanceWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SeaFishingPackageWnd = { "UI/Prefab/SeaFishing/SeaFishingPackageWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SeaFishingTuJianWnd = { "UI/Prefab/SeaFishing/SeaFishingTuJianWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SeaFishingShopWnd = { "UI/Prefab/SeaFishing/SeaFishingShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SeaFishingServerRecordWnd = { "UI/Prefab/SeaFishing/SeaFishingServerRecordWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--镇宅鱼
	ZhenZhaiYuWnd = { "UI/Prefab/House/Pool/ZhenZhaiYuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhenZhaiYuDetailWnd = { "UI/Prefab/House/Pool/ZhenZhaiYuDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FishCreelWnd = { "UI/Prefab/House/Pool/FishCreelWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FeedZhenZhaiYuWnd = { "UI/Prefab/House/Pool/FeedZhenZhaiYuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChooseFabao2GongFengWnd = { "UI/Prefab/House/Pool/ChooseFabao2GongFengWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BringUpZhenZhaiYuWnd = { "UI/Prefab/House/Pool/BringUpZhenZhaiYuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ClearZhenZhaiYuWordWnd = { "UI/Prefab/House/Pool/ClearZhenZhaiYuWordWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	InheritZhenZhaiYuWordWnd = { "UI/Prefab/House/Pool/InheritZhenZhaiYuWordWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--海钓副本
	SeaFishingPlayWnd = { "UI/Prefab/SeaFishing/SeaFishingPlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SeaFishingSucceedWnd = { "UI/Prefab/SeaFishing/SeaFishingSucceedWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ComicPPTWnd = { "UI/Prefab/SeaFishing/ComicPPTWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SpecialHaiDiaoPlayPickUpWnd = { "UI/Prefab/SeaFishing/SpecialHaiDiaoPlayPickUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SeaFishingShopWnd = { "UI/Prefab/SeaFishing/SeaFishingShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--2021暑假反串boss
	DaMoWangEnterWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2021/DaMoWangEnterWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	DaMoWangResultWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2021/DaMoWangResultWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	DaMoWangPlayingWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2021/DaMoWangPlayingWnd.prefab",UIParentType.BASE_UI_ROOT, false, true},
	--水果派对
	FruitPartySignWnd = { "UI/Prefab/Festival_ShuJia/FruitParty/FruitPartySignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FruitPartyRankWnd = { "UI/Prefab/Festival_ShuJia/FruitParty/FruitPartyRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FruitPartyStateWnd = { "UI/Prefab/Festival_ShuJia/FruitParty/FruitPartyStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	FruitPartyResultWnd = { "UI/Prefab/Festival_ShuJia/FruitParty/FruitPartyResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuFruitPartySignWnd = { "UI/Prefab/ShiTu/ShiTuFruitPartySignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuFruitPartyNormalStateWnd = { "UI/Prefab/ShiTu/ShiTuFruitPartyNormalStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ShiTuFruitPartyTaskStateWnd = { "UI/Prefab/ShiTu/ShiTuFruitPartyTaskStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ShiTuFruitPartyResultWnd = { "UI/Prefab/ShiTu/ShiTuFruitPartyResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--跨服竞技场
	CrossServerColiseumMainWnd = { "UI/Prefab/Coliseum/CrossServerColiseumMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ColiseumRecordsTipWnd = { "UI/Prefab/Coliseum/ColiseumRecordsTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ColiseumRankWnd = { "UI/Prefab/Coliseum/ColiseumRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ColiseumWeeklyRewardInfoWnd = { "UI/Prefab/Coliseum/ColiseumWeeklyRewardInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ColiseumSeasonRewardWnd = { "UI/Prefab/Coliseum/ColiseumSeasonRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ColiseumShopWnd = { "UI/Prefab/Coliseum/ColiseumShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ColiseumEndGameWnd = { "UI/Prefab/Coliseum/ColiseumEndGameWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ColiseumShareWnd = { "UI/Prefab/Coliseum/ColiseumShareWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ColiseumShowTeamWnd = { "UI/Prefab/Coliseum/ColiseumShowTeamWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--剧情讨论
	PlotCardsWnd = { "UI/Prefab/PlotDiscussion/PlotCardsWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PlotDiscussionWnd = { "UI/Prefab/PlotDiscussion/PlotDiscussionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SendPlotDiscussionWnd = { "UI/Prefab/PlotDiscussion/SendPlotDiscussionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--万圣节2021
	Halloween2021GamePlayWnd = { "UI/Prefab/Festival_Halloween/Halloween2021/Halloween2021GamePlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Halloween2021SpyTipWnd = { "UI/Prefab/Festival_Halloween/Halloween2021/Halloween2021SpyTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Halloween2021JieYuanVoteWnd = { "UI/Prefab/Festival_Halloween/Halloween2021/Halloween2021JieYuanVoteWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--奥运
	OlympicGamePlayWnd =  { "UI/Prefab/OlympicGames/OlympicGamePlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OlympicGamesGambleWnd =  { "UI/Prefab/OlympicGames/OlympicGamesGambleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--冬奥会
	CurlingOlympicWinterGamesPlayWnd = { "UI/Prefab/OlympicGames/CurlingOlympicWinterGamesPlayWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	CurlingOlympicWinterGamesRankWnd = { "UI/Prefab/OlympicGames/CurlingOlympicWinterGamesRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CurlingOlympicWinterGamesPlayResultWnd = { "UI/Prefab/OlympicGames/CurlingOlympicWinterGamesPlayResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	OlympicWinterGamesSnowBattleEnterWnd = { "UI/Prefab/OlympicGames/OlympicWinterGamesSnowBattleEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	OlympicWinterGamesSnowBattleResultWnd = { "UI/Prefab/OlympicGames/OlympicWinterGamesSnowBattleResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--2021七夕活动
	QiXiFireworkEntryWnd = { "UI/Prefab/Festival_QiXi/QiXi2021/QiXiFireworkEntryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiXi2021RightBottom = { "UI/Prefab/Festival_QiXi/QiXi2021/QiXi2021RightBottom.prefab", UIParentType.BASE_UI_ROOT, false, true},
	QiXi2021ConfirmWnd = { "UI/Prefab/Festival_QiXi/QiXi2021/QiXi2021ConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CommonPlayerListWnd2 = { "UI/Prefab/Common/CommonPlayerListWnd2.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 2021七夕pve
	QiXiDaTiWnd = {"UI/Prefab/Festival_QiXi/QiXi2017/QiXiDaTiWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	QueQiaoXianQuEnterWnd = {"UI/Prefab/Festival_QiXi/QiXi2021/QueQiaoXianQuEnterWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	QueQiaoXianQuResultWnd = {"UI/Prefab/Festival_QiXi/QiXi2021/QueQiaoXianQuResultWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	QueQiaoXianQuHistoryTeammateWnd = {"UI/Prefab/Festival_QiXi/QiXi2021/QueQiaoXianQuHistoryTeammateWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	-- 2023七夕
	QiXi2023QueQiaoXianQuEnterWnd = {"UI/Prefab/Festival_QiXi/QiXi2023/QiXi2023QueQiaoXianQuEnterWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	QiXi2023QueQiaoXianQuResultWnd = {"UI/Prefab/Festival_QiXi/QiXi2023/QiXi2023QueQiaoXianQuResultWnd.prefab",UIParentType.POP_UI_ROOT,false,false},
	QiXi2023QueQiaoXianQuHistoryTeammateWnd = {"UI/Prefab/Festival_QiXi/QiXi2023/QiXi2023QueQiaoXianQuHistoryTeammateWnd.prefab",UIParentType.POP_UI_ROOT, false, true},

	ScreenTransitionWnd = { "UI/Prefab/JuQing/ScreenTransitionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	CommonShareBox = {"UI/Prefab/Common/CommonShareBox.prefab", UIParentType.POP_UI_ROOT, false, false},
	GMBubblePopupWnd = {"UI/Prefab/GM/GMBubblePopupWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 趣味喇叭
	QuweiLabaChooseWnd = { "UI/Prefab/Friend/QuweiLabaChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QuweiLabaWnd = { "UI/Prefab/Friend/QuweiLabaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QuweiLabaTipWnd = { "UI/Prefab/Friend/QuweiLabaTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FriendChatSettingWnd = { "UI/Prefab/Friend/FriendChatSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 2021中元节
	ZuiMengLuWnd = { "UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2021/ZuiMengLuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HMLZEnterWnd = { "UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2021/HMLZEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HMLZPlayingWnd = { "UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2021/HMLZPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	YeShengTuiLiWnd = { "UI/Prefab/JuQing/YeShengTuiLiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ZhengWuLuWnd = { "UI/Prefab/ZhengWuLu/ZhengWuLuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhengWuLuTipWnd = { "UI/Prefab/ZhengWuLu/ZhengWuLuTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 梦岛暗恋
	PersonalSpaceSlientLoveConfirmWnd = { "UI/Prefab/PersonalSpace/PersonalSpaceSlientLoveConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ItemExchange1To1Wnd = { "UI/Prefab/ItemExchange1To1Wnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	-- 罗刹海市
	LuoCha2021EnterWnd = { "UI/Prefab/Festival_GuoQing/LuoCha2021/LuoCha2021EnterWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	LuoCha2021ResultWnd = { "UI/Prefab/Festival_GuoQing/LuoCha2021/LuoCha2021ResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	LuoCha2021PlayingWnd = { "UI/Prefab/Festival_GuoQing/LuoCha2021/LuoCha2021PlayingWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	LuoCha2021LearnSkillWnd = { "UI/Prefab/Festival_GuoQing/LuoCha2021/LuoCha2021LearnSkillWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	LuoCha2021GameplayBtnWnd = { "UI/Prefab/Festival_GuoQing/LuoCha2021/LuoCha2021GameplayBtnWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},

	-- 摘星
	ZhaiXingEnterWnd = { "UI/Prefab/ZhaiXing/ZhaiXingEnterWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ZhaiXingStateWnd = { "UI/Prefab/ZhaiXing/ZhaiXingStateWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	ZhaiXingResultWnd = { "UI/Prefab/ZhaiXing/ZhaiXingResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	ZhaiXingRankWnd = { "UI/Prefab/ZhaiXing/ZhaiXingRankWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	-- 砍一刀
	KanYiDaoEnterWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2021/KanYiDaoEnterWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	KanYiDaoPlayTopRightWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2021/KanYiDaoPlayTopRightWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	-- 委托
	EntrustWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2021/EntrustWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	-- 吴门画士
	XinShengHuaJiDrawWnd = { "UI/Prefab/WuMenHuaShi/XinShengHuaJiDrawWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	XinShengHuaJiPaintSetWnd = { "UI/Prefab/WuMenHuaShi/XinShengHuaJiPaintSetWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	XinShengHuaJiBigPicWnd = { "UI/Prefab/WuMenHuaShi/XinShengHuaJiBigPicWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	WuMenHuaShiDrawLotsWnd = { "UI/Prefab/WuMenHuaShi/WuMenHuaShiDrawLotsWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	RuMengJiWnd = { "UI/Prefab/WuMenHuaShi/RuMengJiWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	FanKanHuaCeWnd = { "UI/Prefab/WuMenHuaShi/FanKanHuaCeWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	QuShuiLiuShangWnd = { "UI/Prefab/WuMenHuaShi/QuShuiLiuShangWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	HuaYiGongShangWnd = { "UI/Prefab/WuMenHuaShi/HuaYiGongShangWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	AvatarReplaceWnd = { "UI/Prefab/WuMenHuaShi/AvatarReplaceWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	HuaYiGongShangConfirmWnd = { "UI/Prefab/WuMenHuaShi/HuaYiGongShangConfirmWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	ScrollTaskWnd = { "UI/Prefab/Common/ScrollTaskWnd.prefab", UIParentType.BG_UI_ROOT,false,false},

	-- 灵兽认亲
	LingShouRenQinWnd = { "UI/Prefab/JuQing/LingShouRenQinWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	
	--抹眼泪
	WipeTearsWnd = { "UI/Prefab/Juqing/WipeTearsWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	
	-- 天降宝箱
	TianJiangBaoXiangWnd = { "UI/Prefab/TianJiangBaoXiangWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	--宗派福利
	ZongMenHongBaoSetWnd = { "UI/Prefab/ZongMen/ZongMenHongBaoSetWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	SectHongBaoItemSelectWnd = { "UI/Prefab/ZongMen/SectHongBaoItemSelectWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	ChangeZongMenHongBaoItemWnd = { "UI/Prefab/ZongMen/ChangeZongMenHongBaoItemWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	SectHongbaoDetailNewWnd = { "UI/Prefab/ZongMen/SectHongbaoDetailNewWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	SectHongBaoLotteryWnd = { "UI/Prefab/ZongMen/SectHongBaoLotteryWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	GetSectHongBaoWnd = { "UI/Prefab/ZongMen/GetSectHongBaoWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	
	--额外属性点
	ExtraPropertyWnd = { "UI/Prefab/ExtraProperty/ExtraPropertyWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	WashExtraPropertyWnd = { "UI/Prefab/ExtraProperty/WashExtraPropertyWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	-- NPC聊天礼物界面
	NPCGiftWnd = { "UI/Prefab/Friend/NPCGiftWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	-- 婚礼迭代
	WeddingSceneSelectWnd = { "UI/Prefab/Wedding/WeddingSceneSelectWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingSetInvitationCardWnd = { "UI/Prefab/Wedding/WeddingSetInvitationCardWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingInvitationCardWnd = { "UI/Prefab/Wedding/WeddingInvitationCardWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingEnterSceneWnd = { "UI/Prefab/Wedding/WeddingEnterSceneWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingTopWnd = { "UI/Prefab/Wedding/WeddingTopWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	WeddingFakeBrideWnd = { "UI/Prefab/Wedding/WeddingFakeBrideWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingPromiseWnd = { "UI/Prefab/Wedding/WeddingPromiseWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingCertificationWnd = { "UI/Prefab/Wedding/WeddingCertificationWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingAnniversaryGiftWnd = { "UI/Prefab/Wedding/WeddingAnniversaryGiftWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingSelectWnd = { "UI/Prefab/Wedding/WeddingSelectWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingFeastWnd = { "UI/Prefab/Wedding/WeddingFeastWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingFlowerBallWnd = { "UI/Prefab/Wedding/WeddingFlowerBallWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingBuyWnd = { "UI/Prefab/Wedding/WeddingBuyWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingSprinkleCandyWnd = { "UI/Prefab/Wedding/WeddingSprinkleCandyWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	WeddingProposalWnd = { "UI/Prefab/Wedding/WeddingProposalWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingBirthInfoWnd = { "UI/Prefab/Wedding/WeddingBirthInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingBaZiWnd = { "UI/Prefab/Wedding/WeddingBaZiWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingBaZiExtraWnd = { "UI/Prefab/Wedding/WeddingBaZiExtraWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingOpenGameplayWnd = { "UI/Prefab/Wedding/WeddingOpenGameplayWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WeddingBottomWnd = { "UI/Prefab/Wedding/WeddingBottomWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	WeddingDayPromiseWnd = { "UI/Prefab/Wedding/WeddingDayPromiseWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	BiAnYingXueChooseWnd = { "UI/Prefab/JuQing/BiAnYingXueChooseWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	--尚食坊 食谱
	CookBookWnd = { "UI/Prefab/CookBook/CookBookWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	CookBookItemSelectWnd = { "UI/Prefab/CookBook/CookBookItemSelectWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	OpenIngredientPackResultWnd = { "UI/Prefab/CookBook/OpenIngredientPackResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	CookingResultWnd = { "UI/Prefab/CookBook/CookingResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	MyShangShiIngredientWnd = { "UI/Prefab/CookBook/MyShangShiIngredientWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	--师门重建
	SchoolRebuildingWnd = { "UI/Prefab/SchoolRebuilding/SchoolRebuildingWnd.prefab", UIParentType.POP_UI_ROOT,false,false},

	--2022元旦
	YuanDan2022XuYuanWnd = { "UI/Prefab/Festival_YuanDan/YuanDan2022/YuanDan2022XuYuanWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	YuanDan2022FuDanGamePlayWnd = { "UI/Prefab/Festival_YuanDan/YuanDan2022/YuanDan2022FuDanGamePlayWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	YuanDan2022FuDanReadMeWnd = { "UI/Prefab/Festival_YuanDan/YuanDan2022/YuanDan2022FuDanReadMeWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	YuanDan2022FuDanResultWnd = { "UI/Prefab/Festival_YuanDan/YuanDan2022/YuanDan2022FuDanResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	YuanDan2022FuDanTopRightWnd =  { "UI/Prefab/Festival_YuanDan/YuanDan2022/YuanDan2022FuDanTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	YingHuoWeiGuangWnd = { "UI/Prefab/JuQing/YingHuoWeiGuangWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	TsjzBuyAdvPassWnd = { "UI/Prefab/Festival_HanJia/HanJia2022/TsjzBuyAdvPassWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	TsjzMainWnd = { "UI/Prefab/Festival_HanJia/HanJia2022/TsjzMainWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	
	Valentine2022DriftBottleWnd = { "UI/Prefab/Festival_Valentine/Valentine2022/Valentine2022DriftBottleWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	YuanXiao2022DengMiWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2022/YuanXiao2022DengMiWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	--帮会领土战
	GuildTerritorialWarsEnterWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsEnterWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsOccupyWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsOccupyWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsMapWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsMapWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsSituationWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsSituationWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsMapTipWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsMapTipWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsMapIconWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsMapIconWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsTopRightWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	GuildTerritorialWarsJieMengWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsJieMengWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsRankWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsSceneChosenWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsSceneChosenWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsScoreSpeedDetailWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsScoreSpeedDetailWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsPeripheryAlertWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsPeripheryAlertWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	GuildTerritorialWarsPeripheryMonsterWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsPeripheryMonsterWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsContributionWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsContributionWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsDrawColorWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsDrawColorWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsResultWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	GuildTerritorialWarsBeiZhanWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsBeiZhanWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsZhanLongWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsZhanLongWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsServerGroupWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsServerGroupWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsZhanLongShopWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsZhanLongShopWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsJingYingSettingWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsJingYingSettingWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsZhanShenTaskWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsZhanShenTaskWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildTerritorialWarsChallengeTopRightWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsChallengeTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,false},
	--GuildTerritorialWarsChallengeSituatuionWnd = { "UI/Prefab/Guild/TerritorialWars/GuildTerritorialWarsChallengeSituatuionWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	--帮会外援
	GuildExternalAidWnd = { "UI/Prefab/Guild/ExternalAid/GuildExternalAidWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildExternalAidCardWnd = { "UI/Prefab/Guild/ExternalAid/GuildExternalAidCardWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	GuildExternalAidInvitationWnd = { "UI/Prefab/Guild/ExternalAid/GuildExternalAidInvitationWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--圣诞2021
	WNXYGiveWishWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/WNXYGiveWishWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WNXYAddGiftWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/WNXYAddGiftWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	WNXYGuildWishesWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/WNXYGuildWishesWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	SDXYSignAndRankWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/SDXYSignAndRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--BezierCameraEditWnd = { "UI/Prefab/BezierCameraEditWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	SDXYGuanXingWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/SDXYGuanXingWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	SDXYSucceedWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/SDXYSucceedWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	MJSYPlayWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/MJSYPlayWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	SDXYTopRightWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/SDXYTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	QuestionAndAnswerWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/QuestionAndAnswerWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	MJSYInfoWnd = { "UI/Prefab/Festival_Christmas/Christmas2021/MJSYInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	
	-- 斗魂坛迭代
	FightingSpiritRuleWnd = { "UI/Prefab/FightingSpirit/FightingSpiritRuleWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FightingSpiritMatchInfoWnd = { "UI/Prefab/FightingSpirit/FightingSpiritMatchInfoWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FightingSpiritDouHunWnd = { "UI/Prefab/FightingSpirit/FightingSpiritDouHunWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FightingSpiritDouHunAppendageQiLiWnd = { "UI/Prefab/FightingSpirit/FightingSpiritDouHunAppendageQiLiWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FightingSpiritDouHunAppendageShenLiWnd = { "UI/Prefab/FightingSpirit/FightingSpiritDouHunAppendageShenLiWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FightingSpiritGambleWnd = { "UI/Prefab/FightingSpirit/FightingSpiritGambleWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FightingSpiritGambleResultWnd = { "UI/Prefab/FightingSpirit/FightingSpiritGambleResultWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	FightingSpiritRewardShowWnd = { "UI/Prefab/FightingSpirit/FightingSpiritRewardShowWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	CommonProgressRankWnd = { "UI/Prefab/Common/CommonProgressRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	--烟花大会2022
	FireWorkPartyZhongChouWnd = { "UI/Prefab/FireWorkParty/FireWorkPartyZhongChouWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	SendShowLoveFireWorkWnd = { "UI/Prefab/FireWorkParty/SendShowLoveFireWorkWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	--狂奔的汤圆
	RunningWildTangYuanRankWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2022/RunningWildTangYuanRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	RunningWildTangYuanResultWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2022/RunningWildTangYuanResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	RunningWildTangYuanSignWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2022/RunningWildTangYuanSignWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	RunningWildTangYuanTopRightWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2022/RunningWildTangYuanTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	--魅香楼
	QingJiuLaBaWnd = { "UI/Prefab/MeiXiangLou/QingJiuLaBaWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	MusicPlaySignWnd = { "UI/Prefab/MeiXiangLou/MusicPlaySignWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	MusicPlayRankWnd = { "UI/Prefab/MeiXiangLou/MusicPlayRankWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	MusicPlayHitingWnd = { "UI/Prefab/MeiXiangLou/MusicPlayHitingWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	MusicPlayHitResultWnd = { "UI/Prefab/MeiXiangLou/MusicPlayHitResultWnd.prefab", UIParentType.POP_UI_ROOT,false,false},
	-- 清明2022
	YinHunPoZhiEnterWnd = { "UI/Prefab/Festival_QingMing/QingMing2022/YinHunPoZhiEnterWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	YinHunPoZhiTopRightWnd = { "UI/Prefab/Festival_QingMing/QingMing2022/YinHunPoZhiTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	YinHunPoZhiQuestionWnd = { "UI/Prefab/Festival_QingMing/QingMing2022/YinHunPoZhiQuestionWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	YinHunPoZhiResultWnd = { "UI/Prefab/Festival_QingMing/QingMing2022/YinHunPoZhiResultWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	HuanJingShiMengWnd = { "UI/Prefab/Festival_QingMing/QingMing2022/HuanJingShiMengWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	-- 高昌
	GaoChangTopRightWnd = { "UI/Prefab/GaoChang/GaoChangTopRightWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	GaoChangSignalWnd = { "UI/Prefab/GaoChang/GaoChangSignalWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	ZhuErDanJieDianXinWnd = { "UI/Prefab/ZhuErDan/ZhuErDanJieDianXinWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhuErDanChuoPaoPaoWnd = { "UI/Prefab/ZhuErDan/ZhuErDanChuoPaoPaoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhuErDanDaDiShuWnd = { "UI/Prefab/ZhuErDan/ZhuErDanDaDiShuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhuErDanGuanChaXinZangWnd = { "UI/Prefab/ZhuErDan/ZhuErDanGuanChaXinZangWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	GuildLeagueResultLookUpWnd = { "UI/Prefab/GuildLeague/GuildLeagueResultLookUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	OverheadButtonWnd = { "UI/Prefab/OverheadButtonWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	BusinessMainWnd = { "UI/Prefab/Business/BusinessMainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessBuyOrSellWnd = { "UI/Prefab/Business/BusinessBuyOrSellWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessNewsWnd = { "UI/Prefab/Business/BusinessNewsWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessInvestmentWnd = { "UI/Prefab/Business/BusinessInvestmentWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessRepaymentWnd = { "UI/Prefab/Business/BusinessRepaymentWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessWarehouseLevelOpWnd = { "UI/Prefab/Business/BusinessWarehouseLevelOpWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessHealthRecoverWnd = { "UI/Prefab/Business/BusinessHealthRecoverWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessItemInfoWnd = { "UI/Prefab/Business/BusinessItemInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BusinessResultWnd = { "UI/Prefab/Business/BusinessResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	EmployCatAgreementWnd = { "UI/Prefab/Business/EmployCatAgreementWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BusinessLotteryWnd = { "UI/Prefab/Business/BusinessLotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	-- 据点战
	JuDianBattleSceneChooseWnd = { "UI/Prefab/JuDianBattle/JuDianBattleSceneChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JuDianBattlePlayingWnd = { "UI/Prefab/JuDianBattle/JuDianBattlePlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	JuDianBattleLevelUpTipWnd = { "UI/Prefab/JuDianBattle/JuDianBattleLevelUpTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JuDianBattleInfoWnd = { "UI/Prefab/JuDianBattle/JuDianBattleInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JuDianBattleOccupationDetailWnd = { "UI/Prefab/JuDianBattle/JuDianBattleOccupationDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 通用图文规则界面
	CommonTextImageRuleWnd = { "UI/Prefab/Common/CommonTextImageRuleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 通用buff界面
	BuffInfoWnd = { "UI/Prefab/Common/BuffInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 据点战&领土战入口
	GuildWarsEnterWnd = { "UI/Prefab/JuDianBattle/GuildWarsEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--周年庆2022 葫芦娃
	HuLuWaTravelWorldWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/HuLuWaTravelWorldWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuLuWaTravelEventPopWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/HuLuWaTravelEventPopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	RuYiDongSignWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/RuYiDongSignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	RuYiDongTopRightWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/RuYiDongTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},
	RuYiDongResultWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/RuYiDongResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	HuLuWaBianShenPreviewWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/HuLuWaBianShenPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	RuYiVoicePasswordWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/RuYiVoicePasswordWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiaoDuoRuYiSignWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/QiaoDuoRuYiSignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuLuWaUnLockWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/HuLuWaUnLockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiaoDuoRuYiRankWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/QiaoDuoRuYiRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuLuWaZhanLingWnd = {"UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/ZhanLing/HuLuWaZhanLingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuLuWaUnLockDanHuWnd = {"UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/ZhanLing/HuLuWaUnLockDanHuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuLuWaLianDanShouCeWnd = {"UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/ZhanLing/HuLuWaLianDanShouCeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	RuYiAdditionSkillWnd = { "UI/Prefab/Festival_ZhouNianQing/HuLuWa2022/RuYiAdditionSkillWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	-- 五一斗地主
	DouDiZhuBaoXiangApplyWnd = { "UI/Prefab/DouDiZhu/BaoXiang/DouDiZhuBaoXiangApplyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DouDiZhuBaoXiangResultWnd = { "UI/Prefab/DouDiZhu/BaoXiang/DouDiZhuBaoXiangResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 玩家排行榜
	PlayerRankWnd = { "UI/Prefab/PlayerRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	HaiZhanDriveWnd = { "UI/Prefab/HaiZhan/HaiZhanDriveWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HaiZhanStateWnd = { "UI/Prefab/HaiZhan/HaiZhanStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HaiZhanChangePosWnd = { "UI/Prefab/HaiZhan/HaiZhanChangePosWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HaiZhanPickUpWnd = { "UI/Prefab/HaiZhan/HaiZhanPickUpWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HaiZhanPickWnd = { "UI/Prefab/HaiZhan/HaiZhanPickWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HaiZhanPickResultWnd = { "UI/Prefab/HaiZhan/HaiZhanPickResultWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	
	--聊天过滤词编辑界面
	ChatFilterWordEditWnd = { "UI/Prefab/Chat/ChatFilterWordEditWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--新白联动
	ShenYaoLeftTimeWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoLeftTimeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoChallengerSelectionWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoChallengerSelectionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoPassingRecordWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoPassingRecordWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoExclusiveWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoExclusiveWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoSettingWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoTeamInfoWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoTeamInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoSelectOneOfTwoWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoSelectOneOfTwoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoSelectOneOfThreeWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoSelectOneOfThreeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShenYaoSelectOneOfFourWnd = {"UI/Prefab/XinBaiLianDong/ShenYaoSelectOneOfFourWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYaoJieYuanWnd = {"UI/Prefab/XinBaiLianDong/ShanYaoJieYuanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYaoJieYuanShareWnd = {"UI/Prefab/XinBaiLianDong/ShanYaoJieYuanShareWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShanYaoJieYuanStoryWnd = {"UI/Prefab/XinBaiLianDong/ShanYaoJieYuanStoryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuanYaoJianWnd = {"UI/Prefab/XinBaiLianDong/GuanYaoJianWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BaiSheBianRenTopRightWnd = {"UI/Prefab/XinBaiLianDong/BaiSheBianRenTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 倩影初闻录
	QianYingChuWenWnd = {"UI/Prefab/QianYingChuWen/QianYingChuWenWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 端午活动
	DuanWu2022WuduChooseWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2022/DuanWu2022WuduChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2022WuduBuffWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2022/DuanWu2022WuduBuffWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2022WuduEnterWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2022/DuanWu2022WuduEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2022WuduResultWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2022/DuanWu2022WuduResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DuanWu2022WuduRankWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2022/DuanWu2022WuduRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DuanWu2022MainWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2022/DuanWu2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 六一2022
	LiuYi2022PopoEnterWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2022/LiuYi2022PopoEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiuYi2022PopoResultWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2022/LiuYi2022PopoResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuYi2022PopoPlayingWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2022/LiuYi2022PopoPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},

	-- 暑假2022
	ShanYeMiZongBigPicWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShanYeMiZongBigPicWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYeMiZongGuessWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShanYeMiZongGuessWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYeMiZongMainWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShanYeMiZongMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYeMiZongRetroWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShanYeMiZongRetroWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYeMiZongRevealMurdererWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShanYeMiZongRevealMurdererWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYeMiZongRewardWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShanYeMiZongRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShanYeMiZongClueAvailableWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShanYeMiZongClueAvailableWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuanHunShanZhuangTopRightWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/HuanHunShanZhuangTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HuanHunShanZhuangResultWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/HuanHunShanZhuangResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	HuanHunShanZhuangSignUpWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/HuanHunShanZhuangSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuJia2022MainWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2022/ShuJia2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	--师徒系統优化
	ShiTuMainWnd = {"UI/Prefab/ShiTu/ShiTuMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuRecommendWnd = {"UI/Prefab/ShiTu/ShiTuRecommendWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuHistoryWnd = {"UI/Prefab/ShiTu/ShiTuHistoryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WaiMenDiZiWnd = {"UI/Prefab/ShiTu/WaiMenDiZiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuDirectoryWnd = {"UI/Prefab/ShiTu/ShiTuDirectoryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuChooseJiYuWnd = {"UI/Prefab/ShiTu/ShiTuChooseJiYuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChuShiGiftWnd = {"UI/Prefab/ShiTu/ChuShiGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuRatingTipWnd = {"UI/Prefab/ShiTu/ShiTuRatingTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuGiftGivingWnd = {"UI/Prefab/ShiTu/ShiTuGiftGivingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShiTuWishGiftWnd = {"UI/Prefab/ShiTu/ShiTuWishGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShifuPingJiaWnd = {"UI/Prefab/ShiTu/ShifuPingJiaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BaiShiPosterWnd = {"UI/Prefab/ShiTu/BaiShiPosterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChuShiPosterWnd = {"UI/Prefab/ShiTu/ChuShiPosterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ShopMallItemSelectWnd = {"UI/Prefab/ShopMallItemSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	
	ShiTuWuZiQiWnd = {"UI/Prefab/ShiTu/ShiTuWuZiQiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShiTuDuLingTaskWnd = {"UI/Prefab/ShiTu/ShiTuDuLingTaskWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShiTuCompassWnd = {"UI/Prefab/ShiTu/ShiTuCompassWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	QiXi2022PlotWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022PlotWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiXi2022EnterWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022EnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiXi2022ChoosePlotWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022ChoosePlotWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiXi2022ReturnGoodsWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022ReturnGoodsWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiXi2022FindItemSkillWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022FindItemSkillWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	QiXi2022PlotResultWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022PlotResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QiXi2022PlantTreesWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022PlantTreesWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QiXi2022ScheduleWnd = {"UI/Prefab/Festival_QiXi/QiXi2022/QiXi2022ScheduleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	QiXi2023MainWnd = {"UI/Prefab/Festival_QiXi/QiXi2023/QiXi2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	DownloadNewVersionWnd = {"UI/Prefab/DownloadNewVersionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HouseRollerCoasterWnd = { "UI/Prefab/House/HouseRollerCoasterWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HouseRollerCoasterPlayWnd = { "UI/Prefab/House/HouseRollerCoasterPlayWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HouseRollerCoasterUseZhanTaiWnd = { "UI/Prefab/House/HouseRollerCoasterUseZhanTaiWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HouseRollerCoasterExchangeWnd = { "UI/Prefab/House/HouseRollerCoasterExchangeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	-- HMT绑定奖励
	HmtBindWnd = { "UI/Prefab/Common/HmtBindWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--水漫金山
	ShuiManJinShanConfirmWnd = {"UI/Prefab/ShuiManJinShan/ShuiManJinShanConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuiManJinShanResultWnd = {"UI/Prefab/ShuiManJinShan/ShuiManJinShanResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuiManJinShanStateWnd = {"UI/Prefab/ShuiManJinShan/ShuiManJinShanStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	--中元节2022
	ZYJ2022MainWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2022/ZYJ2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuYuanMengJiWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2022/YuYuanMengJiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GSNZEnterWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2022/GSNZEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GSNZPlayWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2022/GSNZPlayWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	GSNZResultWnd= {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2022/GSNZResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZYJYaoZhengWnd= {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2022/ZYJYaoZhengWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZYJYaoZhengPuWnd= {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2022/ZYJYaoZhengPuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--主角剧情歌词
	ZhuJueJuQingLyricWnd = {"UI/Prefab/ZhuJueJuQing/ZhuJueJuQingLyricWnd.prefab", UIParentType.BASE_UI_ROOT, false, false},

	-- 无量蜃境
	WuLiangChallengeWnd = {"UI/Prefab/WuLiang/WuLiangChallengeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuLiangCompanionBtnWnd = {"UI/Prefab/WuLiang/WuLiangCompanionBtnWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	WuLiangCompanionInfoWnd = {"UI/Prefab/WuLiang/WuLiangCompanionInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuLiangCompanionTipWnd = {"UI/Prefab/WuLiang/WuLiangCompanionTipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuLiangEnterButtonWnd = {"UI/Prefab/WuLiang/WuLiangEnterButtonWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	WuLiangLevelWnd = {"UI/Prefab/WuLiang/WuLiangLevelWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuLiangPassedTeamInfoWnd = {"UI/Prefab/WuLiang/WuLiangPassedTeamInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WuLiangResultWnd = {"UI/Prefab/WuLiang/WuLiangResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	WuLiangRankWnd = {"UI/Prefab/WuLiang/WuLiangRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--新白 语音成就
	XinBaiVoiceAchivementWnd = {"UI/Prefab/Achievement/XinBaiVoiceAchivementWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	UnLockVoiceAchievementWnd = {"UI/Prefab/Achievement/UnLockVoiceAchievementWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	VoiceAchievemenAwardPreviewWnd = {"UI/Prefab/Achievement/VoiceAchievemenAwardPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--中秋2022
	ZhongQiu2022MainWnd = {"UI/Prefab/Festival_ZhongQiu/ZhongQiu2022/ZhongQiu2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JiaoYueDuoSuYiSignUpWnd = {"UI/Prefab/Festival_ZhongQiu/ZhongQiu2022/JiaoYueDuoSuYiSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JiaoYueDuoSuYiInGameWnd = {"UI/Prefab/Festival_ZhongQiu/ZhongQiu2022/JiaoYueDuoSuYiInGameWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JiaoYueDuoSuYiResultWnd = {"UI/Prefab/Festival_ZhongQiu/ZhongQiu2022/JiaoYueDuoSuYiResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--新装备相关
	EquipCompositeWnd = {"UI/Prefab/Equip/EquipCompositeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	EquipCompositeResultWnd = {"UI/Prefab/Equip/EquipCompositeResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	EquipExtraDisassembleWnd = {"UI/Prefab/Equip/EquipExtraDisassembleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	EquipForgeResetWnd = {"UI/Prefab/Equip/EquipForgeResetWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	EquipAdvDisassembleWnd = {"UI/Prefab/Equip/EquipAdvDisassembleWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	GhostEquipDisassembleWnd = {"UI/Prefab/Equip/GhostEquipDisassembleWnd.prefab",UIParentType.POP_UI_ROOT, false, true},

	-- 据点站二期
	JuDianBattleResultWnd = { "UI/Prefab/JuDianBattle/JuDianBattleResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	JuDianBattleRankWnd = { "UI/Prefab/JuDianBattle/JuDianBattleRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JuDianBattleContributionWnd = { "UI/Prefab/JuDianBattle/JuDianBattleContributionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JuDianBattleDailySceneViewWnd = { "UI/Prefab/JuDianBattle/JuDianBattleDailySceneViewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JuDianBattleDailyPlayingWnd = { "UI/Prefab/JuDianBattle/JuDianBattleDailyPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	-- 斗魂二十届
	FightingSpiritLocalMatchInfoWnd = { "UI/Prefab/FightingSpirit/FightingSpiritLocalMatchInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FightingSpiritChampionPreviewWnd = { "UI/Prefab/FightingSpirit/FightingSpiritChampionPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 世界杯
	WorldCup2022GJZLEnterWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/WorldCup2022GJZLEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WorldCup2022GJZLSelectWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/WorldCup2022GJZLSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WorldCup2022LotteryWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/WorldCup2022LotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	WorldCup2022LotterySelectWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/WorldCup2022LotterySelectWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	WorldCup2022MainWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/WorldCup2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	WorldCup2022GJZLWinWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/WorldCup2022GJZLWinWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 蹴鞠
	CuJuEnterWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/CuJuEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CuJuSelectWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/CuJuSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CuJuMinimap = { "UI/Prefab/Festival_WorldCup/WorldCup2022/CuJuMinimap.prefab", UIParentType.BASE_UI_ROOT, false, true},
	CuJuRankWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/CuJuRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CuJuResultWnd = { "UI/Prefab/Festival_WorldCup/WorldCup2022/CuJuResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 灵玉礼包推荐
	LingyuGiftRecommendWnd = { "UI/Prefab/Welfare/LingyuGiftRecommendWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--2022国庆活动
	GuoQingPvEWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvEWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuoQingPvERankWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvERankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuoqingPvEResultWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoqingPvEResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	GuoQingPvPWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvPWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuoQingPvPSkillSelectWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvPSkillSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuoQingPvPSkillInfoWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvPSkillInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuoQingPvPResultWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvPResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	GuoQingPvPStartWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvPStartWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuoQingMainWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuoQingPvPPlayingWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2022/GuoQingPvPPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	-- 跨服比武
	BiWuCrossEnterWnd =  { "UI/Prefab/BiWuCross/BiWuCrossEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BiWuCrossInfoWnd =  { "UI/Prefab/BiWuCross/BiWuCrossInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BiWuCrossWatchWnd =  { "UI/Prefab/BiWuCross/BiWuCrossWatchWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BiWuCrossTeamWnd =  { "UI/Prefab/BiWuCross/BiWuCrossTeamWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	--万圣节
	Halloween2022MainWnd = { "UI/Prefab/Festival_Halloween/Halloween2022/Halloween2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HwMuTouRenEnterWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/HwMuTouRenEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HwMuTouRenResultWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/HwMuTouRenResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	HwMuTouRenTopRightWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/HwMuTouRenTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	HwFireDefenceEnterWnd = { "UI/Prefab/Festival_Halloween/Halloween2022/HwFireDefenceEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HwFireDefenceResultWnd = { "UI/Prefab/Festival_Halloween/Halloween2022/HwFireDefenceResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	HalloweenHongBaoCoverWnd = {"UI/Prefab/HongBaoWnd/HalloweenHongBaoCoverWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 双十一 2022
	MengQuanHengXingSignUpWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/MengQuanHengXingSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MengQuanHengXingTopRightWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/MengQuanHengXingTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	MengQuanHengXingResultWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/MengQuanHengXingResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MengQuanHengXingWarningWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/MengQuanHengXingWarningWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShenZhaiTanBaoSignUpWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/ShenZhaiTanBaoSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ShenZhaiTanBaoTopRightWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/ShenZhaiTanBaoTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ShenZhaiTanBaoKickSkillWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/ShenZhaiTanBaoKickSkillWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ShenZhaiTanBaoResultWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/ShenZhaiTanBaoResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2022MainWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/Shuangshiyi2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	FuLiQuan2022MainWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/FuLiQuan2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	FuLiQuan2022ExchangeWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2022/FuLiQuan2022ExchangeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	-- 双十一 2023
	MengQuan2023SignUpWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/MengQuan2023SignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MengQuan2023TopRightWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/MengQuan2023TopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	MengQuan2023ResultWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/MengQuan2023ResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MengQuan2023StatWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/MengQuan2023StatWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MengQuan2023RankWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/MengQuan2023RankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MengQuan2023WarningWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/MengQuan2023WarningWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	
	BaoYuDuoYu2023SignUpWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/BaoYuDuoYu2023SignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	BaoYuDuoYu2023SubmitWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/BaoYuDuoYu2023SubmitWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BaoYuDuoYu2023ChangePhaseWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/BaoYuDuoYu2023ChangePhaseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	BaoYuDuoYu2023TopRightWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/BaoYuDuoYu2023TopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	BaoYuDuoYu2023ResultWnd = {"UI/Prefab/Festival_Double11/Shuangshiyi2023/BaoYuDuoYu2023ResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023LotteryWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023LotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Shuangshiyi2023ChargeRewardWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023ChargeRewardWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023LotteryOverviewWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023LotteryOverviewWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023LotteryReplacementWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023LotteryReplacementWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023LotteryMustGetWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023LotteryMustGetWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023MainWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023DiscountWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023DiscountWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023DiscountPackageWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023DiscountPackageWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023DiscountCompositeWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023DiscountCompositeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Shuangshiyi2023DiscountBuyFashionWnd = { "UI/Prefab/Festival_Double11/Shuangshiyi2023/Shuangshiyi2023DiscountBuyFashionWnd.prefab", UIParentType.POP_UI_ROOT, false, false},


	--帮会自定义建设 前置任务
	GuildCustomBuildPreTaskWnd = {"UI/Prefab/GuildCustomBuild/GuildCustomBuildPreTaskWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildPreTaskBreakSchemeTopRightWnd = { "UI/Prefab/GuildCustomBuild/GuildPreTaskBreakSchemeTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	
	MiniVolumeSettingsWnd = { "UI/Prefab/MiniVolumeSettingsWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SoundSettingButtonWnd = { "UI/Prefab/SoundSettingButtonWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	-- 红包封面
	HongBaoCoverWnd = { "UI/Prefab/HongBaoWnd/HongBaoCoverWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HongBaoCoverInfoWnd = { "UI/Prefab/HongBaoWnd/HongBaoCoverInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HongBaoCoverPopupMenu = { "UI/Prefab/HongBaoWnd/HongBaoCoverPopupMenu.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 宗派迭代
	ZongPaiLingFuRankWnd = { "UI/Prefab/ZongMen/ZongPaiLingFuRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	RefineMonsterCircleNewWnd = { "UI/Prefab/ZongMen/RefineMonsterCircleNewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 元宵2023
	YuanXiao2023MainWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiao2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2023AddRiddleWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiao2023AddRiddleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2023GuessRiddleWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiao2023GuessRiddleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2023QiYuanWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiao2023QiYuanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2023RiddleRankWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiao2023RiddleRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoGaoChangEnterWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoGaoChangEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoGaoChangTopRightWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoGaoChangTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoDefenseTopRightWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoDefenseTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoGaoChangResultWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoGaoChangResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoGaoChangAwardListWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoGaoChangAwardListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoDefenseSignUpWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoDefenseSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoDefenseResultWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoDefenseResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	YuanXiaoDefenseRankWnd = { "UI/Prefab/Festival_YuanXiao/YuanXiao2023/YuanXiaoDefenseRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 元旦2023
	YuanDan2023MainWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2023/YuanDan2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanDan2023XinYuanBiDaWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2023/YuanDan2023XinYuanBiDaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanDan2023YuanDanQiYuanEnterWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2023/YuanDan2023YuanDanQiYuanEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanDan2023YuanDanQiYuanResultWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2023/YuanDan2023YuanDanQiYuanResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	YuanDan2023YuanDanQiYuanTopRightWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2023/YuanDan2023YuanDanQiYuanTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	
	-- 春节2023
	ChunJie2023MainWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2023HongBaoCoverWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023HongBaoCoverWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2023NianHuoWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023NianHuoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2023ShengYanEnterWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023ShengYanEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2023ShengYanRankWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023ShengYanRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChunJie2023ShengYanResultWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023ShengYanResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChunJie2023ShengYanTopRightWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023ShengYanTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ChunJie2023ShouLieEnterWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023ShouLieEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2023ShouLieResultWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023ShouLieResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChunJie2023ShengYanSitDownButtonWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2023/ChunJie2023ShengYanSitDownButtonWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 蓬岛伏妖
	PengDaoDialogWnd = {"UI/Prefab/PengDao/PengDaoDialogWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoRewardsListWnd = {"UI/Prefab/PengDao/PengDaoRewardsListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoDevelopWnd = {"UI/Prefab/PengDao/PengDaoDevelopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoRankWnd = {"UI/Prefab/PengDao/PengDaoRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoTuJianWnd = {"UI/Prefab/PengDao/PengDaoTuJianWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoShopWnd = {"UI/Prefab/PengDao/PengDaoShopWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoSelectDifficultyWnd = {"UI/Prefab/PengDao/PengDaoSelectDifficultyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoPlayWnd = {"UI/Prefab/PengDao/PengDaoPlayWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	PengDaoSelectBlessingWnd = {"UI/Prefab/PengDao/PengDaoSelectBlessingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PengDaoResultWnd = {"UI/Prefab/PengDao/PengDaoResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	PengDaoReplaceSkillWnd = {"UI/Prefab/PengDao/PengDaoReplaceSkillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 圣诞2022
	TurkeyMatchRankWnd = {"UI/Prefab/Festival_Christmas/Christmas2022/TurkeyMatchRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TurkeyMatchSkillWnd = {"UI/Prefab/Festival_Christmas/Christmas2022/TurkeyMatchSkillWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	TurkeyMatchStateWnd = {"UI/Prefab/Festival_Christmas/Christmas2022/TurkeyMatchStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	-- TurkeyMatchResultWnd = {"UI/Prefab/Festival_Christmas/Christmas2022/TurkeyMatchResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Christmas2022MainWnd = {"UI/Prefab/Festival_Christmas/Christmas2022/Christmas2022MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- WarmChristmasEveWnd = {"UI/Prefab/Festival_Christmas/Christmas2022/WarmChristmasEveWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 情人节2023
	Valentine2023Wnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Valentine2023YWQSSelectWnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023YWQSSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Valentine2023YWQSVoteWnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023YWQSVoteWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Valentine2023YWQSRankWnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023YWQSRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Valentine2023YWQSExpressionWnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023YWQSExpressionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Valentine2023LGTMEnterWnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023LGTMEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Valentine2023LGTMResultWnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023LGTMResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Valentine2023LGTMProgressWnd = {"UI/Prefab/Festival_Valentine/Valentine2023/Valentine2023LGTMProgressWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	SummerLBSSearchItemAddWnd = {"UI/Prefab/SummerLBS/SummerLBSSearchItemAddWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChristmasCardWnd = {"UI/Prefab/Festival_Christmas/Christmas/ChristmasCardWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	FurniturePlaceHint = {"UI/Prefab/Festival_DuanWu/TowerDefense/FurniturePlaceHint.prefab", UIParentType.POP_UI_ROOT, false, true},
	TowerDefensePopupMenu = {"UI/Prefab/Festival_DuanWu/TowerDefense/TowerDefensePopupMenu.prefab", UIParentType.POP_UI_ROOT, false, true},
	TowerDefenseStateWnd = {"UI/Prefab/Festival_DuanWu/TowerDefense/TowerDefenseStateWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TowerDefenseEditWnd = {"UI/Prefab/Festival_DuanWu/TowerDefense/TowerDefenseEditWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	--家园布置迭代
	MultipleFurnitureEditWnd = {"UI/Prefab/House/Advance/MultipleFurnitureEditWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MultipleChooseListWnd = {"UI/Prefab/House/Advance/MultipleChooseListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	MultipleFurnishPopupMenu = {"UI/Prefab/House/Advance/MultipleFurnishPopupMenu.prefab", UIParentType.POP_UI_ROOT, false, true},
	FurnitureLianPuWnd = {"UI/Prefab/House/Advance/FurnitureLianPuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 据点战（人间棋局）三期
	JuDianBattlePrepareWnd = { "UI/Prefab/JuDianBattle/JuDianBattlePrepareWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	JuDianBattleZhanLongWnd = { "UI/Prefab/JuDianBattle/JuDianBattleZhanLongWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	JuDianBattleZhanLongShopWnd = { "UI/Prefab/JuDianBattle/JuDianBattleZhanLongShopWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	JuDianBattleJingYingSettingWnd = { "UI/Prefab/JuDianBattle/JuDianBattleJingYingSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	-- 通用通行证购买等级界面
	CommonTongXingZhengBuyLevelWnd = {"UI/Prefab/Common/CommonTongXingZhengBuyLevelWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 大富翁
	DaFuWongMainPlayWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongMainPlayWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	DaFuWongBuyLandWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongBuyLandWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongDaTiWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongDaTiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongEnterWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongEventDialogWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongEventDialogWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongJiaoyiHangWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongJiaoyiHangWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongLandTip = {"UI/Prefab/Festival_DaFuWong/DaFuWongLandTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongPlayerTip = {"UI/Prefab/Festival_DaFuWong/DaFuWongPlayerTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongPlayLandTopWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongPlayLandTopWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	DaFuWongQiTaoWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongQiTaoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongResultWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DaFuWongRuleWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongRuleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongShuiLaoWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongShuiLaoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongTaskWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongTaskWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongTongXingZhengWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongTongXingZhengWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongWorldEventWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongWorldEventWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongXiuYangWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongXiuYangWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongYaoQianWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongYaoQianWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongZhiYaWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongZhiYaWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongZhanBaoWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongZhanBaoWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DaFuWongYiZhanWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongYiZhanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongSanCaiWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongSanCaiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongBaoXiangWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongBaoXiangWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongItemUseTip = {"UI/Prefab/Festival_DaFuWong/DaFuWongItemUseTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongItemUseWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongItemUseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongTouziUseWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongTouziUseWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongJunWangLingWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongJunWangLingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongTongXingZhengUnLockWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongTongXingZhengUnLockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongVoicePackageWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongVoicePackageWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongBuyVoicePackageWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongBuyVoicePackageWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongGodEventWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongGodEventWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongPopMapWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongPopMapWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongLandLvWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongLandLvWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DaFuWongGuaJiWnd = {"UI/Prefab/Festival_DaFuWong/DaFuWongGuaJiWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	ToolBtnWnd = {"UI/Prefab/ToolBtnWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- 属性引导
	PropertyGuideDetailWnd = {"UI/Prefab/Property/PropertyGuideDetailWnd.prefab",UIParentType.POP_UI_ROOT, false, false},
	PlayerPropertyCompareWnd = {"UI/Prefab/Property/PlayerPropertyCompareWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	SkillLevelDetailInfoWnd = {"UI/Prefab/Skill/SkillLevelDetailInfoWnd.prefab",UIParentType.POP_UI_ROOT, false, true},
	PropertyRankWnd = {"UI/Prefab/Property/PropertyRankWnd.prefab",UIParentType.POP_UI_ROOT, false, false},
	MapGridEditWnd = { "UI/NoneReleasePrefab/MapGridEditWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	FightingSpiritGenerateChampionWnd = {"UI/Prefab/FightingSpirit/FightingSpiritGenerateChampionWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	CommonSideDialogWnd = {"UI/Prefab/Common/CommonSideDialogWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 四时戏
	FSCCardPileWnd = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCCardPileWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FSCInviteDlg = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCInviteDlg.prefab", UIParentType.POP_UI_ROOT, false, false},
	FSCInviteWnd = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCInviteWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FSCPrepareRareCardWnd = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCPrepareRareCardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FSCInGameWnd = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCInGameWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FSCResultWnd = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FSCCardDetailWnd = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCCardDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FSCRareCardInfoWnd = {"UI/Prefab/Festival_ZhouNianQing/FourSeasonCard/FSCRareCardInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 周年庆2023
	ZhouNianQingLookForKidMainWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingLookForKidMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhouNianQingLookForKidResultWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingLookForKidResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZhouNianQingLookForKidComposeWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingLookForKidComposeWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZhouNianQingPassportWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingPassportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhouNianQingPassportBuyLevelWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingPassportBuyLevelWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhouNianQingPassportUnlockWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingPassportUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhouNianQing2023MainWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQing2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhouNianQingCarveTabenWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingCarveTabenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhouNianQingLifeFriendWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingLifeFriendWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhouNianQingFSCRankWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2023/ZhouNianQingFSCRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	-- 装备再造
	RecreateExtraEquipWnd = {"UI/Prefab/Equip/RecreateExtraEquipWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 队伍招募
	TeamRecruitWnd = {"UI/Prefab/TeamRecruit/TeamRecruitWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TeamRecruitPostWnd = {"UI/Prefab/TeamRecruit/TeamRecruitPostWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TeamRecruitDetailWnd = {"UI/Prefab/TeamRecruit/TeamRecruitDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 月卡福利礼包
	WelfareGiftWnd = {"UI/Prefab/Welfare/WelfareGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 清明2023
	QingMing2023MainWnd = {"UI/Prefab/Festival_QingMing/QingMing2023/QingMing2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QingMing2023PVEWnd = {"UI/Prefab/Festival_QingMing/QingMing2023/QingMing2023PVEWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QingMing2023PVEResultWnd = {"UI/Prefab/Festival_QingMing/QingMing2023/QingMing2023PVEResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QingMing2023PVPWnd = {"UI/Prefab/Festival_QingMing/QingMing2023/QingMing2023PVPWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	QingMing2023PVPResultWnd = {"UI/Prefab/Festival_QingMing/QingMing2023/QingMing2023PVPResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	QingMing2023PVPTopRightWnd = {"UI/Prefab/Festival_QingMing/QingMing2023/QingMing2023PVPTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	
	-- 2023世界事件
	MakeFanWnd = {"UI/Prefab/Festival_YiRenSi/MakeFanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SelectPanel = {"UI/Prefab/Festival_YiRenSi/SelectPanel.prefab", UIParentType.POP_UI_ROOT, false, false},
	CollectCluesWnd = {"UI/Prefab/Festival_YiRenSi/CollectCluesWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CluesDetailWnd = {"UI/Prefab/Festival_YiRenSi/CluesDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuPaperWnd = {"UI/Prefab/Festival_YiRenSi/NanDuPaperWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuPaperGlassWnd = {"UI/Prefab/Festival_YiRenSi/NanDuPaperGlassWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuScrollWnd = {"UI/Prefab/Festival_YiRenSi/NanDuScrollWnd.prefab", UIParentType.BG_UI_ROOT, false, false},
	FreeFishWnd = {"UI/Prefab/Festival_YiRenSi/FreeFishWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaCookBookWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaCookBookWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	NanDuFanHuaCookBookItemSelectWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaCookBookItemSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	NanDuFanHuaLordMainWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaLordMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaLordVertifyWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaLordVertifyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaLordPlanScheduleWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaLordPlanScheduleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaLordCiTiaoDetailWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaLordCiTiaoDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaBargainWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaBargainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaFashionPreviewTryToBuyWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaFashionPreviewTryToBuyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaFashionPreviewWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaFashionPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaLordChangePropertyWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaLordChangePropertyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaWorldEventsMainWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaWorldEventsMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaMuKeWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaMuKeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaDaTiWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaDaTiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaRepaymentWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaRepaymentWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NanDuFanHuaExchangeMoneyWnd = {"UI/Prefab/Festival_YiRenSi/NanDuFanHuaExchangeMoneyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	TearCalendarWnd = {"UI/Prefab/ZhuJueJuQing/TearCalendarWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 竞技场
	ArenaMainWnd = {"UI/Prefab/Arena/ArenaMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaFriendWnd = {"UI/Prefab/Arena/ArenaFriendWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaPlayerInfoWnd = {"UI/Prefab/Arena/ArenaPlayerInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaRankWnd = {"UI/Prefab/Arena/ArenaRankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ArenaPassportWnd = {"UI/Prefab/Arena/ArenaPassportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaPassportPreviewWnd = {"UI/Prefab/Arena/ArenaPassportPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaPassportOpenWnd = {"UI/Prefab/Arena/ArenaPassportOpenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaPassportBuyLevelWnd = {"UI/Prefab/Arena/ArenaPassportBuyLevelWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaPassportGiftingWnd = {"UI/Prefab/Arena/ArenaPassportGiftingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaSeasonTaskWnd = {"UI/Prefab/Arena/ArenaSeasonTaskWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ArenaResultWnd = {"UI/Prefab/Arena/ArenaResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ArenaRuleAwardWnd = {"UI/Prefab/Arena/ArenaRuleAwardWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	-- 柳如是剧情
	LiuRuShiTaskFailWnd = {"UI/Prefab/LiuRuShi/LiuRuShiTaskFailWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuRuShiDaTiWnd = {"UI/Prefab/LiuRuShi/LiuRuShiDaTiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuRuShiHuanZhuangWnd = {"UI/Prefab/LiuRuShi/LiuRuShiHuanZhuangWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuRuShiJianShuWnd = {"UI/Prefab/LiuRuShi/LiuRuShiJianShuWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuRuShiGiftWnd = {"UI/Prefab/LiuRuShi/LiuRuShiGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuRuShiEndWnd = {"UI/Prefab/LiuRuShi/LiuRuShiEndWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	
	-- 赛事荣耀
	CompetitionHonorFameHallWnd = {"UI/Prefab/CompetitionHonor/CompetitionHonorFameHallWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CompetitionHonorWnd = {"UI/Prefab/CompetitionHonor/CompetitionHonorWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 端午2023
	ArenaJingYuanTopRightWnd = {"UI/Prefab/Arena/ArenaJingYuan/ArenaJingYuanTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	-- 端午2023
	DuanWu2023PVEEnterWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVEEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2023PVERankWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVERankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2023PVEResultWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVEResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	DuanWu2023MainWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2023PVPVEEnterWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVPVEEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2023PVPVESkillSelectWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVPVESkillSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2023PVPVETopRightWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVPVETopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	DuanWu2023PVPVECompareResultWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVPVECompareResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	DuanWu2023PVPVEResultWnd = {"UI/Prefab/Festival_DuanWu/DuanWu2023/DuanWu2023PVPVEResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	NewAuctionBidWnd = {"UI/Prefab/Auction/NewAuctionBidWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	MyAuctionWnd = {"UI/Prefab/Auction/MyAuctionWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	PinchFaceShareWnd = {"UI/Prefab/PinchFace/PinchFaceShareWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PinchFaceWnd = {"UI/Prefab/PinchFace/PinchFaceWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	PinchFaceModifyWnd = {"UI/Prefab/PinchFace/PinchFaceModifyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PinchFaceHubWnd = {"UI/Prefab/PinchFace/PinchFaceHubWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PinchFaceHubUploadWnd = {"UI/Prefab/PinchFace/PinchFaceHubUploadWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PinchFaceHubReportWnd = {"UI/Prefab/PinchFace/PinchFaceHubReportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	PinchFaceFuxiWnd = {"UI/Prefab/PinchFace/PinchFaceFuxiWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CameraWnd = {"UI/Prefab/Common/CameraWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	TopNoticeCenter = {"UI/Prefab/TopNoticeCenter.prefab", UIParentType.BASE_UI_ROOT, false, false},
	
	--养育迭代2023
	SetTargetQiChangWnd = {"UI/Prefab/Baby2023/SetTargetQiChangWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 六一2023
	LiuYi2023MainWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2023/LiuYi2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiuYi2023BossSignWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2023/LiuYi2023BossSignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiuYi2023BossResultWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2023/LiuYi2023BossResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuYi2023DaoDanXiaoGuiSignWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2023/LiuYi2023DaoDanXiaoGuiSignWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiuYi2023DaoDanXiaoGuiResultWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2023/LiuYi2023DaoDanXiaoGuiResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	LiuYi2023DaoDanXiaoGuiRankWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2023/LiuYi2023DaoDanXiaoGuiRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	LiuYi2023DaoDanXiaoGuiStateWnd = {"UI/Prefab/Festival_LiuYi/LiuYi2023/LiuYi2023DaoDanXiaoGuiStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	OwnMoneyListWnd = {"UI/Prefab/Main/OwnMoneyListWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--府库
	StoreRoomWnd = {"UI/Prefab/StoreRoom2023/StoreRoomWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StoreRoomPosUnlockWnd = {"UI/Prefab/StoreRoom2023/StoreRoomPosUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	StoreRoomConstructWnd = {"UI/Prefab/StoreRoom2023/StoreRoomConstructWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 暑假2023
	ShuJia2023MainWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJia2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuJia2023WorldEventWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJia2023WorldEventWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuJiaHaiZhanResultWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJiaHaiZhanResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--ShuJiaHaiZhanSignUpWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJiaHaiZhanSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TreasureSeaEventWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/TreasureSeaEventWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TreasureSeaHintWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/TreasureSeaHintWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	TreasureSeaMiniMapWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/TreasureSeaMiniMapWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	-- TreasureSeaMiniMapTip = {"UI/Prefab/Festival_ShuJia/ShuJia2023/TreasureSeaMiniMapTip.prefab", UIParentType.POP_UI_ROOT, false, true},
	TreasureSeaResultWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/TreasureSeaResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	TreasureSeaSignUpWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/TreasureSeaSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TreasureSeaRankRewardDetailWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/TreasureSeaRankRewardDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuJia2023XHTXWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJia2023XHTXWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuJia2023XHTXPresentGivenWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJia2023XHTXPresentGivenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuJia2023PassportWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJia2023PassportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShuJia2023PassportVIPUnlockWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/ShuJia2023PassportVIPUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 海战
	HaiZhanPosSelectionWnd = {"UI/Prefab/Festival_ShuJia/ShuJia2023/HaiZhanPosSelectionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 福利、大月卡迭代
	BigMonthCardSelectItemWnd = {"UI/Prefab/Welfare/BigMonthCardSelectItemWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ReSignInConfirmWnd = {"UI/Prefab/Welfare/ReSignInConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ReceiveGiftPackSettingWnd = {"UI/Prefab/Welfare/ReceiveGiftPackSettingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 亚运会
	YaYunHui2023MainWnd = {"UI/Prefab/Festival_YaYunHui/YaYunHui2023/YaYunHui2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YaYunHui2023QuestionWnd = {"UI/Prefab/Festival_YaYunHui/YaYunHui2023/YaYunHui2023QuestionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YaYunHui2023LotteryWnd = {"UI/Prefab/Festival_YaYunHui/YaYunHui2023/YaYunHui2023LotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YaYunHui2023LotteryRankWnd = {"UI/Prefab/Festival_YaYunHui/YaYunHui2023/YaYunHui2023LotteryRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YaYunHui2023BiWuRankWnd = {"UI/Prefab/Festival_YaYunHui/YaYunHui2023/YaYunHui2023BiWuRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HangZhouScrollWnd = {"UI/Prefab/Festival_YaYunHui/YaYunHui2023/HangZhouScrollWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	--中元节2023
	ZhongYuanJie2023MainWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2023/ZhongYuanJie2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZYPDSignUpWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2023/ZYPDSignUpWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZYPDResultWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2023/ZYPDResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ZYPDGetSpiritWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2023/ZYPDGetSpiritWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZYPDHuaFuWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2023/ZYPDHuaFuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZYPDStateWnd = {"UI/Prefab/Festival_ZhongYuanJie/ZhongYuanJie2023/ZYPDStateWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	CommonPassportWnd = {"UI/NoneReleasePrefab/CommonPassportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CommonPassportVIPUnlockWnd = {"UI/NoneReleasePrefab/CommonPassportVIPUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NewPlayerAppearanceInfoWnd = {"UI/Prefab/NewPlayerAppearanceInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	NewRankWnd = {"UI/Prefab/NewRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	-- 2023国庆
	GuoQing2023MainWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2023/GuoQing2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	GuoQing2023JingYuanResultWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2023/GuoQing2023JingYuanResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	GuoQing2023JingYuanTopRightWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2023/GuoQing2023JingYuanTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	GuoQing2023ProtectHZWnd = {"UI/Prefab/Festival_GuoQing/GuoQing2023/GuoQing2023ProtectHZWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- NPC商城模型预览
	NPCShopModelPreviewWnd = {"UI/Prefab/Common/NPCShopModelPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 帮会拼图
	GuildPuzzleEnterWnd = {"UI/Prefab/Guild/GuildPuzzleEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildPuzzleWnd = {"UI/Prefab/Guild/GuildPuzzleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	GuildPuzzleDetailWnd = {"UI/Prefab/Guild/GuildPuzzleDetailWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	--2023转职界面
	ProfessionTransferMainWnd = {"UI/Prefab/Festival_ProfessionTransfer2023/ProfessionTransferMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ProfessionTransferZhanKuangIntroductionWnd = {"UI/Prefab/Festival_ProfessionTransfer2023/ProfessionTransferZhanKuangIntroductionWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ProfessionTransferCharacterWnd = {"UI/Prefab/ProfessionTransfer/ProfessionTransferCharacterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ProfessionTransferSkillbookItemSelectWnd = {"UI/Prefab/ProfessionTransfer/ProfessionTransferSkillbookItemSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TianShu2023OpenWnd = {"UI/Prefab/TianShu2023OpenWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TianShu2023ShareWnd = {"UI/Prefab/Share/TianShu2023ShareWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	
	-- 嘉年华2023
	Carnival2023PVEEnterWnd = {"UI/Prefab/Festival_Carnival/Carnival2023/Carnival2023PVEEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Carnival2023PVESkillAddPointWnd = {"UI/Prefab/Festival_Carnival/Carnival2023/Carnival2023PVESkillAddPointWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Carnival2023PVERankWnd = {"UI/Prefab/Festival_Carnival/Carnival2023/Carnival2023PVERankWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Carnival2023PVEResultWnd = {"UI/Prefab/Festival_Carnival/Carnival2023/Carnival2023PVEResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Carnival2023PVEPlayerCountWnd = {"UI/Prefab/Festival_Carnival/Carnival2023/Carnival2023PVEPlayerCountWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	Carnival2023MainWnd = {"UI/Prefab/Festival_Carnival/Carnival2023/Carnival2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CarnivalCollectWnd = { "UI/Prefab/Festival_Carnival/Carnival2023/CarnivalCollectWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CarnivalDrawWnd = { "UI/Prefab/Festival_Carnival/Carnival2023/CarnivalDrawWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	CarnivalCollectSubWnd = { "UI/Prefab/Festival_Carnival/Carnival2023/CarnivalCollectSubWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	--CharacterCreation2023Wnd = { "UI/Prefab/ChuangJue/CharacterCreation2023Wnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	--BeforeEnterDengZhongShiJieWnd = { "UI/Prefab/ChuangJue/BeforeEnterDengZhongShiJieWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	-- 永久时装
	FashionLotterySplitWnd = {"UI/Prefab/Common/FashionLotterySplitWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JiuWeiHuFashionLotteryBuyTicketWnd = {"UI/Prefab/Festival_FashionLottery/JiuWeiHu/JiuWeiHuFashionLotteryBuyTicketWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JiuWeiHuFashionLotteryExchangeWnd = {"UI/Prefab/Festival_FashionLottery/JiuWeiHu/JiuWeiHuFashionLotteryExchangeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JiuWeiHuFashionLotteryPrizePoolWnd = {"UI/Prefab/Festival_FashionLottery/JiuWeiHu/JiuWeiHuFashionLotteryPrizePoolWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JiuWeiHuFashionLotteryWnd = {"UI/Prefab/Festival_FashionLottery/JiuWeiHu/JiuWeiHuFashionLotteryWnd.prefab", UIParentType.POP_UI_ROOT, false, false},

	-- 战狂任务
	ZhanKuangTaskChangeFaceWnd = { "UI/Prefab/ZhuJueJuQing/ZhanKuangTaskChangeFaceWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	ZhanKuangTaskOperaPlayWnd = { "UI/Prefab/ZhuJueJuQing/ZhanKuangTaskOperaPlayWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
	BlackScreenWnd = { "UI/Prefab/Story/BlackScreenWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},

	-- 妖叶漫卷
	YaoYeManJuanStateWnd = { "UI/Prefab/Festival_YaoYeManJuan/YaoYeManJuanStateWnd.prefab", UIParentType.BASE_UI_ROOT,false,true},
	YaoYeManJuanCompositeWnd = { "UI/Prefab/Festival_YaoYeManJuan/YaoYeManJuanCompositeWnd.prefab", UIParentType.POP_UI_ROOT,false,true},

	--自定义装饰物模板
	CustomZswTemplateTopRightWnd = { "UI/Prefab/JyZhuangShiWuTemplate/CustomZswTemplateTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	UnlockCustomZswTemplateWnd = { "UI/Prefab/JyZhuangShiWuTemplate/UnlockCustomZswTemplateWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CustomZswTemplateEditWnd = { "UI/Prefab/JyZhuangShiWuTemplate/CustomZswTemplateEditWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CustomZswTemplateUpgradeWnd = { "UI/Prefab/JyZhuangShiWuTemplate/CustomZswTemplateUpgradeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	CustomZswTemplateFillingWnd = { "UI/Prefab/JyZhuangShiWuTemplate/CustomZswTemplateFillingWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ZhongQiu2023MainWnd = { "UI/Prefab/Festival_ZhongQiu/ZhongQiu2023/ZhongQiu2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 横屿荡寇
	HengYuDangKouZhanLiPinWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouZhanLiPinWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	HengYuDangKouMonsterInfoWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouMonsterInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	HengYuDangKouMonsterOpenWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouMonsterOpenWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	HengYuDangKouPlayingWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true },
	HengYuDangKouTeamSelectWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouTeamSelectWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	HengYuDangKouOpenWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouOpenWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	HengYuDangKouSelectDifficultyBox = { "UI/Prefab/HengYuDangKou/HengYuDangKouSelectDifficultyBox.prefab", UIParentType.POP_UI_ROOT, false, true },
	HengYuDangKouResultWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	HengYuDangKouRankingListWnd = { "UI/Prefab/HengYuDangKou/HengYuDangKouRankingListWnd.prefab", UIParentType.POP_UI_ROOT, false, true },

	GuideChooseWnd = { "UI/Prefab/Guide/GuideChooseWnd.prefab", UIParentType.BASE_UI_ROOT, false, true },
	FailGuideWnd = { "UI/Prefab/Guide/FailGuideWnd.prefab", UIParentType.POP_UI_ROOT, false, true },

	-- 罗刹海市
	LuoChaHaiShiEnterWnd = { "UI/Prefab/LuoChaHaiShi/LuoChaHaiShiEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	LuoChaHaiShiResultWnd = { "UI/Prefab/LuoChaHaiShi/LuoChaHaiShiResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false },

	-- 2023万圣节
	Halloween2023MainWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/Halloween2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Halloween2023OrderBattleEnterWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/Halloween2023OrderBattleEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Halloween2023OrderBattleResultWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/Halloween2023OrderBattleResultWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	Halloween2023OrderBattleRuleWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/Halloween2023OrderBattleRuleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	Halloween2023OrderBattleTopRightWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/Halloween2023OrderBattleTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	Halloween2023DiaoKeWnd = { "UI/Prefab/Festival_Halloween/Halloween2023/Halloween2023DiaoKeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 圣诞节2023
	Christmas2023MainWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023GuaGuaCardWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023GuaGuaCardWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023XueHuaXuYuSendGiftWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023XueHuaXuYuSendGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023XueHuaXuYuSelectGiftWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023XueHuaXuYuSelectGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023XueHuaXuYuGetGiftWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023XueHuaXuYuGetGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023BingXuePlayEnterWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023BingXuePlayEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023BingXuePlayPlayingWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023BingXuePlayPlayingWnd.prefab", UIParentType.BASE_UI_ROOT, false, true },
	Christmas2023BingXuePlayChooseWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023BingXuePlayChooseWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023BingXuePlayResultWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023BingXuePlayResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023GiftForSelfEnterWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023GiftForSelfEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023GiftForSelfMakeGiftWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023GiftForSelfMakeGiftWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
	Christmas2023GiftForSelfResultWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/Christmas2023GiftForSelfResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
    WarmChristmasEveWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/WarmChristmasEveWnd.prefab", UIParentType.POP_UI_ROOT, false, true },
    TurkeyMatchResultWnd = { "UI/Prefab/Festival_Christmas/Christmas2023/TurkeyMatchResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true },

	-- 元旦节2024
	YuanDan2024BreakEggWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2024/YuanDan2024BreakEggWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanDan2024MainWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2024/YuanDan2024MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanDan2024PVEEnterWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2024/YuanDan2024PVEEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanDan2024PVEGridWnd = {"UI/Prefab/Festival_YuanDan/YuanDan2024/YuanDan2024PVEGridWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	-- 技能新手引导
	SkillBeginnerGuideWnd = {"UI/Prefab/SkillBeginnerGuide/SkillBeginnerGuideWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},

	-- 寒假2024
	HanJia2024MainWnd = {"UI/Prefab/Festival_HanJia/HanJia2024/HanJia2024MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SnowManEnterWnd = {"UI/Prefab/Festival_HanJia/HanJia2024/SnowManEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SnowManResultWnd = {"UI/Prefab/Festival_HanJia/HanJia2024/SnowManResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HanJia2024PassportWnd = {"UI/Prefab/Festival_HanJia/HanJia2024/HanJia2024PassportWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	HanJia2024PassportVIPUnlockWnd = {"UI/Prefab/Festival_HanJia/HanJia2024/HanJia2024PassportVIPUnlockWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	UpDownWorldEnterWnd = {"UI/Prefab/Festival_HanJia/HanJia2024/UpDownWorldEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	UpDownWorldTopRightWnd = {"UI/Prefab/Festival_HanJia/HanJia2024/UpDownWorldTopRightWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	JieRiMainWnd = {"UI/Prefab/Common/JieRiMainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	SuggestSkillWnd = {"UI/Prefab/Skill/SuggestSkillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZhanKuangNvBubbleWnd = {"UI/Prefab/ZhuJueJuQing/ZhanKuangNvBubbleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	XueWuWnd = {"UI/Prefab/ZhuJueJuQing/XueWuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	FootStepPlayWnd = {"UI/Prefab/ZhuJueJuQing/FootStepPlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ShaoKaoPlayWnd = {"UI/Prefab/ZhuJueJuQing/ShaoKaoPlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZuanMuQuHuoWnd = {"UI/Prefab/ZhuJueJuQing/ZuanMuQuHuoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	TaoQuan2Wnd = {"UI/Prefab/ZhuJueJuQing/TaoQuan2Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 春节2024
	ChunJie2024SLQZPlayWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024SLQZPlayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024SLQZEnterWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024SLQZEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024SQSEnterWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024SQSEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuDaiWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuDaiWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	ChunJie2024BaiNianWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024BaiNianWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024JLSCWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024JLSCWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuYanWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuYanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuYanBuyWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuYanBuyWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuYanBuyRankWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuYanBuyRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuYanUpgradeWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuYanUpgradeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024ModelPreviewWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024ModelPreviewWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuYanExtraMealWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuYanExtraMealWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuYanMenuWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuYanMenuWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024XingYunQianWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024XingYunQianWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FuYanGuildRankWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FuYanGuildRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024GamePlayConfirmWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024GamePlayConfirmWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024FLCXWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024FLCXWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ChunJie2024LGBKWnd = {"UI/Prefab/Festival_ChunJie/ChunJie2024/ChunJie2024LGBKWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 快捷聊天
	QuickChatWnd = { "UI/Prefab/Chat/QuickChatWnd.prefab", UIParentType.POP_UI_ROOT, false, false},
	-- 聊天弹幕
	ChatDanMuWnd = { "UI/Prefab/Chat/ChatDanMuWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ChatDanMuSetWnd = { "UI/Prefab/Chat/ChatDanMuSetWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	-- 新强化&锻造详情界面
	EquipmentNewSuitWnd = {"UI/Prefab/Equip/EquipmentNewSuitWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	Guide2023Wnd = {"UI/Prefab/Guide2023/Guide2023Wnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	--元宵2024
	YuanXiao2024MainWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiao2024MainWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2024AddRiddleWnd= {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiao2024AddRiddleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2024GuessRiddleWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiao2024GuessRiddleWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoYanHua2024TopRightWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiaoYanHua2024TopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	YuanXiao2024QiYuanWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiao2024QiYuanWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiao2024RiddleRankWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiao2024RiddleRankWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoDaLuanDouResultWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiaoDaLuanDouResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoDaLuanDouSkillWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiaoDaLuanDouSkillWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoDaLuanDouTopRightWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiaoDaLuanDouTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	YuanXiaoYanHua2024BottomWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiaoYanHua2024BottomWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	YuanXiaoYanHua2024TopRightWnd = {"UI/Prefab/Festival_YuanXiao/YuanXiao2024/YuanXiaoYanHua2024TopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	 -- 周年庆2024
	ZNQ2024NLDBEnterWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2024/ZNQ2024NLDBEnterWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZNQ2024NLDBEscapeWnd= {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2024/ZNQ2024NLDBEscapeWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZNQ2024NLDBPayWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2024/ZNQ2024NLDBPayWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZNQ2024NLDBResultWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2024/ZNQ2024NLDBResultWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZNQ2024NLDBStartWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2024/ZNQ2024NLDBStartWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	ZNQ2024NLDBTopRightWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2024/ZNQ2024NLDBTopRightWnd.prefab", UIParentType.BASE_UI_ROOT, false, true},
	ZNQ2024NLDBWarDataWnd = {"UI/Prefab/Festival_ZhouNianQing/ZhouNianQing2024/ZNQ2024NLDBWarDataWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	FashionInfoWnd = {"UI/Prefab/FashionInfoWnd.prefab", UIParentType.POP_UI_ROOT, false, true},

	ChooseLanguageWnd = {"UI/Prefab/MultiLanguage/ChooseLanguageWnd.prefab", UIParentType.POP_UI_ROOT, false, true},
	SEASelectServerRegionWnd = { "UI/Prefab/SEASelectServerRegionWnd.prefab", UIParentType.POP_UI_ROOT,false,true},
}

local __modules = {}
setmetatable(CLuaUIResources, {
	__newindex = function(t,key,value)
		__modules[key] = value
    end,
    __index = function(t, key)
        if not __modules[key] then
			local config = __LuaUIResources[key]
			if config then
				__modules[key] = CUIModule(key,config[1],config[2],config[3] or false,config[4] or false)
				__LuaUIResources[key] = nil
			else
				return nil
			end
        end
        return __modules[key]
    end})

if CommonDefs.IS_VN_CLIENT then
    CLuaUIResources.DebitNoteWnd = CUIModule("DebitNoteWnd","UI/Prefab/JuQing/DebitNoteWnd_H.prefab", UIParentType.POP_UI_ROOT, false, true)
end

EndSample()
