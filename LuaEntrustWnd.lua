local CButton         = import "L10.UI.CButton"
local CUITexture      = import "L10.UI.CUITexture"
local UIGrid          = import "UIGrid"
local GameObject      = import "UnityEngine.GameObject"
local CommonDefs      = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Extensions      = import "Extensions"
local CUICommonDef    = import "L10.UI.CUICommonDef"
local UILabel         = import "UILabel"
local UISprite        = import "UISprite"
local NGUIText        = import "NGUIText"
local Vector3         = import "UnityEngine.Vector3"
local Ease            = import "DG.Tweening.Ease"

LuaEntrustWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEntrustWnd, "EntrustItemTemplate", "EntrustItemTemplate", GameObject)
RegistChildComponent(LuaEntrustWnd, "EntrustGrid", "EntrustGrid", UIGrid)
RegistChildComponent(LuaEntrustWnd, "RefreshButton", "RefreshButton", CButton)
RegistChildComponent(LuaEntrustWnd, "ReceiveButton", "ReceiveButton", CButton)
RegistChildComponent(LuaEntrustWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaEntrustWnd, "AcceptTimesLabel", "AcceptTimesLabel", UILabel)
RegistChildComponent(LuaEntrustWnd, "MoneyLabel", "MoneyLabel", UILabel)
RegistChildComponent(LuaEntrustWnd, "FreeLabel", "FreeLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaEntrustWnd, "currentTaskInfo")
RegistClassMember(LuaEntrustWnd, "acceptTaskInfo")
RegistClassMember(LuaEntrustWnd, "preSelectedFx")
RegistClassMember(LuaEntrustWnd, "currentAcceptTimes")
RegistClassMember(LuaEntrustWnd, "colorDict")

function LuaEntrustWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)

	UIEventListener.Get(self.ReceiveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReceiveButtonClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    self.EntrustItemTemplate:SetActive(false)
    --@endregion EventBind end
end

function LuaEntrustWnd:Init()
    self:InitClassMember()
    self:InitColorDict()
    self:UpdateRefreshTimes(0)
    self:UpdateAcceptTimes(0)
    Gac2Gas.Double11DSG_QueryTask(false)
end

function LuaEntrustWnd:InitClassMember()
    self.currentTaskInfo = nil
    self.preSelectedFx = nil
    self.acceptTaskInfo = nil
end

function LuaEntrustWnd:InitColorDict()
    self.colorDict={
        [1] = LocalString.GetString("白"),
        [2] = LocalString.GetString("黄"),
        [3] = LocalString.GetString("橙"),
        [4] = LocalString.GetString("蓝"),
        [5] = LocalString.GetString("红"),
        [6] = LocalString.GetString("紫"),
        [7] = LocalString.GetString("鬼")
    }
end

function LuaEntrustWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncEntrustTaskInfo", self, "OnRefreshTask")
    g_ScriptEvent:AddListener("AcceptTask", self, "OnAcceptTask")
end

function LuaEntrustWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncEntrustTaskInfo", self, "OnRefreshTask")
    g_ScriptEvent:RemoveListener("AcceptTask", self, "OnAcceptTask")
end

function LuaEntrustWnd:OnRefreshTask(taskTable, leftFreeTimes, acceptTimes)
    self:UpdateEntrustTasks(taskTable)
    self:UpdateRefreshTimes(leftFreeTimes)
    self:UpdateAcceptTimes(acceptTimes)
end

function LuaEntrustWnd:OnAcceptTask(args)
    local taskId = tonumber(args[0])
    if self.acceptTaskInfo ~= nil and self.acceptTaskInfo.taskId == taskId then
        self:UpdateFinishStatusLabel(self.acceptTaskInfo.entrustItemTF, 1, true)
        self:UpdateAcceptTimes(self.currentAcceptTimes + 1)
    end
end

-- 更新委托任务
function LuaEntrustWnd:UpdateEntrustTasks(taskTable)
    Extensions.RemoveAllChildren(self.EntrustGrid.transform)
    self:InitClassMember()

    for id, data in pairs(taskTable) do
        local entrustItem = CUICommonDef.AddChild(self.EntrustGrid.gameObject, self.EntrustItemTemplate)
        local entrustItemTF = entrustItem.transform
        local selectedFx = entrustItemTF:Find("SelectedFx").gameObject
        -- 初始化关闭选中特效
        selectedFx:SetActive(false)
        -- 添加点击事件
        UIEventListener.Get(entrustItem).onClick = DelegateFactory.VoidDelegate(function (go)
            self.currentTaskInfo = {taskId = data.taskId, entrustItemTF = entrustItemTF}
            self:OnEntrustTaskClick(selectedFx)
        end)
        self:UpdateTaskDescriptionAndIcon(data.taskId, entrustItemTF)
        -- 修改姓名标签
        local nameLabel = CommonDefs.GetComponent_Component_Type(entrustItemTF:Find("NameLabel"), typeof(UILabel))
        nameLabel.text = Double11_DanShenGouNpc.GetData(data.taskInfo.npcId).Name
        self:UpdateFinishStatusLabel(entrustItemTF, data.taskInfo.taskStatus, false)
        entrustItem:SetActive(true)
    end

    self.EntrustGrid:Reposition()

    -- 给下面一排的任务图标一个偏移
    local childCount = self.EntrustGrid.transform.childCount
    for i=6, childCount-1 do
        local child = self.EntrustGrid.transform:GetChild(i)
        local pos = child.localPosition
        pos.x = pos.x + 40
        child.localPosition = pos
    end
end

-- 更新显示进行中以及已完成标签
function LuaEntrustWnd:UpdateFinishStatusLabel(entrustItemTF, taskStatus, hasEffect)
    local stateLabelTF = entrustItemTF:Find("StateLabel")
    local stateLabel = CommonDefs.GetComponent_Component_Type(stateLabelTF, typeof(UILabel))
    local stateSprite = CommonDefs.GetComponent_Component_Type(stateLabelTF:Find("Sprite"), typeof(UISprite))
    if taskStatus == 0 then -- Idle
        stateLabel.gameObject:SetActive(false)
    elseif taskStatus == 1 then -- Accept
        stateLabel.text = LocalString.GetString("进行中")
        stateSprite.color = NGUIText.ParseColor24("7D632C", 0)
        stateLabel.gameObject:SetActive(true)
    elseif taskStatus == 2 then -- Submit
        stateLabel.text = LocalString.GetString("已完成")
        stateSprite.color = NGUIText.ParseColor24("12BB16", 0)
        stateLabel.gameObject:SetActive(true)
    end
    if hasEffect then
        LuaTweenUtils.TweenAlpha(stateLabel, 0, 1, 0.3)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(stateLabel.transform, Vector3(2, 2, 1), Vector3(1, 1, 1), 0.3), Ease.OutExpo)
    end
end

-- 更新奖励图标、任务描述文字以及颜色
function LuaEntrustWnd:UpdateTaskDescriptionAndIcon(taskId, entrustItemTF)
    -- 修改图标
    local quality = Double11_DanShenGouTask.GetData(taskId).Quality
    local award = Double11_DanShenGouQuality.GetData(quality).Award
    local iconTexture = CommonDefs.GetComponent_Component_Type(entrustItemTF:Find("Icon"), typeof(CUITexture))
    iconTexture:LoadMaterial(Item_Item.GetData(award).Icon)
    -- 修改任务描述
    local taskInfo = Task_Task.GetData(taskId)
    local taskLabel = CommonDefs.GetComponent_Component_Type(entrustItemTF:Find("TaskLabel"), typeof(UILabel))
    if taskInfo.KillMonster ~= nil and taskInfo.KillMonster ~= "" then
        -- 砍怪
        local monsterId, number = self:GetFirstKeyValueFromDict(taskInfo.NeedKillMonster)
        if monsterId and number then
            local monsterName = Monster_Monster.GetData(monsterId).Name
            taskLabel.text = SafeStringFormat3(LocalString.GetString("除掉%d个\n%s"), number, monsterName)
        end
    elseif taskInfo.FindItem ~= nil and taskInfo.FindItem ~= "" then
        -- 找物
        local itemId, number = self:GetFirstKeyValueFromDict(taskInfo.NeedFindItem)
        if itemId and number then
            local itemName = Item_Item.GetData(itemId).Name
            taskLabel.text = SafeStringFormat3(LocalString.GetString("%d个\n%s"), number, itemName)
        end
    elseif taskInfo.FindNpc ~= nil and taskInfo.FindNpc ~= "" then
        -- 找NPC
        local npcId, text = self:GetFirstKeyValueFromDict(taskInfo.NeedFindNPC)
        if npcId then
            local npcName = NPC_NPC.GetData(npcId).Name
            taskLabel.text = SafeStringFormat3(LocalString.GetString("找\n%s"), npcName)
        end
    elseif taskInfo.SubmitItem ~= nil and taskInfo.SubmitItem ~= "" then
        -- 提交装备
        local submitItemInfo = taskInfo.SubmitItemInfo
        if submitItemInfo then
            local colorName = self.colorDict[submitItemInfo.coloId]
            local equipType = submitItemInfo.equipType
            local equipName = EquipmentTemplate_Type.GetData(equipType).Name
            if equipName then
                if equipType >= 17 and equipType <= 24 then
                    taskLabel.text = SafeStringFormat3(LocalString.GetString("需要\n%s位法宝"), equipName)
                else
                    taskLabel.text = SafeStringFormat3(LocalString.GetString("需要\n%s%s"), colorName, equipName)
                end
            end
        end
    end
    -- 修改任务描述颜色
    local qualityColor = Double11_DanShenGouQuality.GetData(quality).Color
    local color = NGUIText.ParseColor32(GameSetting_Common.GetData().COLOR_VALUE[qualityColor], 0)
    taskLabel.color = color
end

-- 更新完成次数
function LuaEntrustWnd:UpdateAcceptTimes(acceptTimes)
    local totalTimes = Double11_Setting.GetData().DanShenGou_MaxAcceptTimesOneDay
    if acceptTimes <= totalTimes then
        self.currentAcceptTimes = acceptTimes
        self.AcceptTimesLabel.text = SafeStringFormat3(LocalString.GetString("今日接受委托 (%d/%d)"), acceptTimes, totalTimes)
    end
end

-- 更新免费刷新次数
function LuaEntrustWnd:UpdateRefreshTimes(leftFreeTimes)
    if leftFreeTimes > 0 then
        self.MoneyLabel.gameObject:SetActive(false)
        self.FreeLabel.text = SafeStringFormat3(LocalString.GetString("免费刷新(%d次)"), leftFreeTimes)
        self.FreeLabel.gameObject:SetActive(true)
    elseif leftFreeTimes <= 0 then
        self.FreeLabel.gameObject:SetActive(false)
        local refreshMoney = Double11_Setting.GetData().DanShenGou_RefreshMoney
        self.MoneyLabel.text = SafeStringFormat3(LocalString.GetString("%d 刷新"), refreshMoney)
        self.MoneyLabel.gameObject:SetActive(true)
    end
end

--@region UIEvent
-- 按下刷新按钮
function LuaEntrustWnd:OnRefreshButtonClick()
    Gac2Gas.Double11DSG_QueryTask(true)
end

-- 按下领取委托按钮
function LuaEntrustWnd:OnReceiveButtonClick()
    if self.currentTaskInfo == nil then
        g_MessageMgr:ShowMessage("Double11_No_Entrust_Selected")
    else
        self.acceptTaskInfo = self.currentTaskInfo
        Gac2Gas.Double11DSG_AcceptTask(self.currentTaskInfo.taskId)
    end
end

-- 按下提示按钮
function LuaEntrustWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("Double11_Entrust_Rule")
end

-- 按下委托任务
function LuaEntrustWnd:OnEntrustTaskClick(selectedFx)
    if self.preSelectedFx ~= nil then
        self.preSelectedFx:SetActive(false)
    end
    selectedFx:SetActive(true)
    self.preSelectedFx = selectedFx
end

-- 从一个字典中提取键值对
function LuaEntrustWnd:GetFirstKeyValueFromDict(dict)
    local key
    local val
    CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (___key, ___value)
        key = ___key
        val = ___value
    end))
    return key, val
end

--@endregion UIEvent

