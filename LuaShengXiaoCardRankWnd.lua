local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local GameObject = import "UnityEngine.GameObject"

local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaShengXiaoCardRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengXiaoCardRankWnd, "RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardRankWnd, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaShengXiaoCardRankWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaShengXiaoCardRankWnd, "TopLable", "TopLable", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaShengXiaoCardRankWnd,"m_Data")
RegistClassMember(LuaShengXiaoCardRankWnd,"m_MyData")

function LuaShengXiaoCardRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.RewardLabel.text = g_MessageMgr:FormatMessage("ShengXiaoCard_Winner_Reward")
    self.TopLable.text = g_MessageMgr:FormatMessage("ShengXiaoCard_Rank_Tip")
end

function LuaShengXiaoCardRankWnd:Init()
    Gac2Gas.SeasonRank_QueryRank(EnumSeasonRankType.eShengXiaoCard,1001)

    self:InitMainInfo(0,0,0)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Data
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
            self:InitItem(item,index+1,self.m_Data[index+1])
        end)

    self.TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        local data=self.m_Data[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

end

--@region UIEvent

--@endregion UIEvent

function LuaShengXiaoCardRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
    -- 
end
function LuaShengXiaoCardRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")

end

function LuaShengXiaoCardRankWnd:OnSeasonRank_QueryRankResult(rankType, seasonId, selfData, rankData)
    self.m_Data = {}
    self.m_MyData = {}
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local myrank = 0
    if rankType==EnumSeasonRankType.eShengXiaoCard and seasonId==1001 then
        local rawData = MsgPackImpl.unpack(selfData)
        if CommonDefs.IsDic(rawData) then
            CommonDefs.DictIterate(rawData,DelegateFactory.Action_object_object(function (___key, ___value) 
                --Score Escape Win Lose playerId name updateTime overdueTime
                -- print(___key,___value)
                self.m_MyData[tostring(___key)]=___value
            end))
        end


        local rawData2 = MsgPackImpl.unpack(rankData)
        if rawData2.Count>0 then
            for i=1,rawData2.Count do
                local rawData3 = rawData2[i-1]
                local t = {}
                if CommonDefs.IsDic(rawData3) then
                    CommonDefs.DictIterate(rawData3,DelegateFactory.Action_object_object(function (___key, ___value) 
                        -- print(___key,___value)
                        t[tostring(___key)] = ___value
                    end))
                end
                if t.playerId and t.playerId==myId then
                    myrank = i
                end
                table.insert( self.m_Data,t )
            end
        end
        local myrate = 0
        if self.m_MyData.Win and self.m_MyData.Win>0 then
            myrate = self.m_MyData.Win/(self.m_MyData.Win+(self.m_MyData.Lose or 0)+(self.m_MyData.Escape or 0))
        end
        self:InitMainInfo(myrank,self.m_MyData.Score or 0,myrate)

        self.TableView:ReloadData(true,false)
    end
end

function LuaShengXiaoCardRankWnd:InitMainInfo(rank,score,winrate)
    local tf = self.transform:Find("MainPlayerInfo")
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local winRateLabel = tf:Find("WinRateLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))

    rankLabel.text = rank>0 and tostring(rank) or LocalString.GetString("未上榜")
    scoreLabel.text = score and tostring(score) or "—"
    winRateLabel.text = winrate and SafeStringFormat3("%.2f%%",winrate*100) or "—"

    local name = CClientMainPlayer.Inst.Name
    nameLabel.text = name and name or "—"
    -- local cls = EnumToInt(CClientMainPlayer.Inst.Class)
    -- clsSprite.spriteName = Profession.GetIconByNumber(cls)
    clsSprite.spriteName = nil
end

function LuaShengXiaoCardRankWnd:InitItem( item,rank,info )
    local tf = item.transform
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local winRateLabel = tf:Find("WinRateLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))

    local rate = 0
    if info.Win>0 then
        rate = info.Win/(info.Win+(info.Lose or 0)+(info.Escape or 0))
    end

    rankLabel.text = rank>0 and tostring(rank) or LocalString.GetString("未上榜")
    scoreLabel.text = tostring(info.Score)
    winRateLabel.text = SafeStringFormat3("%.2f%%",rate*100)

    nameLabel.text = info.name
    -- clsSprite.spriteName = Profession.GetIconByNumber(info.Class or 0)
    clsSprite.spriteName = nil
end
