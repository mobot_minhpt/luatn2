local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CButton = import "L10.UI.CButton"

LuaDaFuWongZhiYaWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongZhiYaWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaDaFuWongZhiYaWnd, "QnCheckBox1", "QnCheckBox1", QnCheckBox)
RegistChildComponent(LuaDaFuWongZhiYaWnd, "QnCheckBox2", "QnCheckBox2", QnCheckBox)
RegistChildComponent(LuaDaFuWongZhiYaWnd, "QnCheckBox3", "QnCheckBox3", QnCheckBox)
RegistChildComponent(LuaDaFuWongZhiYaWnd, "TotalLabel", "TotalLabel", UILabel)
RegistChildComponent(LuaDaFuWongZhiYaWnd, "BuyBtn", "BuyBtn", CButton)
RegistChildComponent(LuaDaFuWongZhiYaWnd, "LoseButton", "LoseButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongZhiYaWnd, "m_CheckBoxList")
RegistClassMember(LuaDaFuWongZhiYaWnd, "m_ZhiYaStatus")
RegistClassMember(LuaDaFuWongZhiYaWnd, "m_ZhiYaData")
RegistClassMember(LuaDaFuWongZhiYaWnd, "m_CloseBtn")

function LuaDaFuWongZhiYaWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick()
	end)


	
	UIEventListener.Get(self.LoseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLoseButtonClick()
	end)


    --@endregion EventBind end
	self.m_ZhiYaStatus = {false,false,false}
	self.m_CheckBoxList = {self.QnCheckBox1,self.QnCheckBox2,self.QnCheckBox3}
	self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton")

	UIEventListener.Get(self.m_CloseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseBtnClick()
	end)
end

function LuaDaFuWongZhiYaWnd:Init()
	self.Title.text = g_MessageMgr:FormatMessage("DaFuWeng_ZhiYaTitleMsg",LuaDaFuWongMgr.CurRoundArgs)
	self:InitData()
	self:InitCheckBox()
	local CountDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundPawn).Time
    local endFunc = function() self:OnCloseBtnClick() end
	local durationFunc = function(time) self:OnTimeUpdate(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.ZhiYa,CountDown,durationFunc,endFunc)
end
-- 质押数值初始化
function LuaDaFuWongZhiYaWnd:InitData()
	self.m_ZhiYaData = {0,0,0,0}
	local landValue,goodsValue,cardValue = 0,0,0
	if CClientMainPlayer.Inst then
		local id = CClientMainPlayer.Inst.Id
		local playerData = LuaDaFuWongMgr.PlayerInfo[id]
		if playerData then
			-- for k,v in pairs(playerData.LandsInfo) do
			-- 	if v then
			-- 		landValue = landValue + LuaDaFuWongMgr.LandInfo[k].pawnPrice
			-- 		if LuaDaFuWongMgr.LandInfo[k].lv > 1 then
			-- 			for i = 1,LuaDaFuWongMgr.LandInfo[k].price.Length - 1 do
			-- 				if LuaDaFuWongMgr.LandInfo[k].lv > i then
			-- 					landValue = landValue + LuaDaFuWongMgr.LandInfo[k].price[i]
			-- 				end
			-- 			end
			-- 		end
			-- 	end
			-- end
			for k,v in pairs(playerData.GoodsInfo) do
				goodsValue = goodsValue + DaFuWeng_Goods.GetData(v.goodsId).PawnPrice
			end
			for k,v in pairs(playerData.CardInfo) do
				if v > 0 then
					cardValue = cardValue + DaFuWeng_Card.GetData(k).PawnPrice * v
				end
			end

		end
	end

	self.m_ZhiYaData[1] = landValue
	self.m_ZhiYaData[2] = goodsValue
	self.m_ZhiYaData[3] = cardValue
	self.m_CheckBoxList[1].transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("地产价值 %d"),self.m_ZhiYaData[1])
	self.m_CheckBoxList[2].transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("货物价值 %d"),self.m_ZhiYaData[2])
	self.m_CheckBoxList[3].transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("道具价值 %d"),self.m_ZhiYaData[3])
	self.TotalLabel.text = SafeStringFormat3(LocalString.GetString("质押总值 %d"),0)
end
-- 选择框初始化
function LuaDaFuWongZhiYaWnd:InitCheckBox()
	for i = 1,3 do
		self.m_CheckBoxList[i].OnValueChanged = DelegateFactory.Action_bool(function (value)
			self:OnQnCheckBoxChanged(i,value)
		end)
		self.m_CheckBoxList[i]:SetSelected(false, false)
	end
end

function LuaDaFuWongZhiYaWnd:OnTimeUpdate(time)
	self.LoseButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("认输(%d)"),time)
end

function LuaDaFuWongZhiYaWnd:OnQnCheckBoxChanged(type,value)
	self.m_ZhiYaStatus[type] = value
	local total = 0
	for k,v in pairs(self.m_ZhiYaStatus) do
		if v then total = total + self.m_ZhiYaData[k] end
	end
	self.TotalLabel.text = SafeStringFormat3(LocalString.GetString("质押总值 %d"),total)
	if total <= LuaDaFuWongMgr.CurRoundArgs then
		self.TotalLabel.color = NGUIText.ParseColor24("b92826",0)
		self.BuyBtn.Enabled = false
	else
		self.TotalLabel.color = NGUIText.ParseColor24("C54F25",0)
		self.BuyBtn.Enabled = true
	end
end

function LuaDaFuWongZhiYaWnd:OnCloseBtnClick()
	Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundPawn)
    CUIManager.CloseUI(CLuaUIResources.DaFuWongZhiYaWnd)
end

--@region UIEvent

function LuaDaFuWongZhiYaWnd:OnBuyBtnClick()
	Gac2Gas.DaFuWengPawn(self.m_ZhiYaStatus[1],self.m_ZhiYaStatus[2],self.m_ZhiYaStatus[3])
end

function LuaDaFuWongZhiYaWnd:OnLoseButtonClick()
	Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundPawn)
    CUIManager.CloseUI(CLuaUIResources.DaFuWongZhiYaWnd)
end

function LuaDaFuWongZhiYaWnd:OnStageUpdate(CurStage)
	if CurStage ~= EnumDaFuWengStage.RoundPawn then CUIManager.CloseUI(CLuaUIResources.DaFuWongZhiYaWnd) end
end
--@endregion UIEvent
function LuaDaFuWongZhiYaWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.ZhiYa)
end
function LuaDaFuWongZhiYaWnd:OnEnable()
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end