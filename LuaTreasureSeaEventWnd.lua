local CTeamMgr = import "L10.Game.CTeamMgr"
local Animation = import "UnityEngine.Animation"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaTreasureSeaEventWnd = class()
LuaTreasureSeaEventWnd.s_Type = 0 --0:宝箱 1:追猎 2:幽灵 3:接舷
LuaTreasureSeaEventWnd.s_Info = {}
LuaTreasureSeaEventWnd.s_PosIndex = nil
LuaTreasureSeaEventWnd.s_NpcEngineId = nil
LuaTreasureSeaEventWnd.s_EventId = nil
LuaTreasureSeaEventWnd.s_OnDestroyCallback = nil

RegistClassMember(LuaTreasureSeaEventWnd, "Title")
RegistClassMember(LuaTreasureSeaEventWnd, "ContentLabel")
RegistClassMember(LuaTreasureSeaEventWnd, "Rewards")
RegistClassMember(LuaTreasureSeaEventWnd, "MoneyLabel")
RegistClassMember(LuaTreasureSeaEventWnd, "ExpLabel")
RegistClassMember(LuaTreasureSeaEventWnd, "Skills")
RegistClassMember(LuaTreasureSeaEventWnd, "SkillTemplate")
RegistClassMember(LuaTreasureSeaEventWnd, "SkillTable")
RegistClassMember(LuaTreasureSeaEventWnd, "Chest")
RegistClassMember(LuaTreasureSeaEventWnd, "Fight")
RegistClassMember(LuaTreasureSeaEventWnd, "Open")
RegistClassMember(LuaTreasureSeaEventWnd, "Bottom")
RegistClassMember(LuaTreasureSeaEventWnd, "BottomHint")
RegistClassMember(LuaTreasureSeaEventWnd, "OkBtn")
RegistClassMember(LuaTreasureSeaEventWnd, "OkBtnLb")
RegistClassMember(LuaTreasureSeaEventWnd, "CancelBtn")
RegistClassMember(LuaTreasureSeaEventWnd, "Spiral")
RegistClassMember(LuaTreasureSeaEventWnd, "WndBg")

RegistClassMember(LuaTreasureSeaEventWnd, "ChestTick")
RegistClassMember(LuaTreasureSeaEventWnd, "CloseTick")
RegistClassMember(LuaTreasureSeaEventWnd, "Tick")
RegistClassMember(LuaTreasureSeaEventWnd, "m_IsCaptain")
RegistClassMember(LuaTreasureSeaEventWnd, "m_CaptainName")
RegistClassMember(LuaTreasureSeaEventWnd, "m_Done")

function LuaTreasureSeaEventWnd:Awake()
    self.Title = self.transform:Find("TitleLabel")
    self.ContentLabel = self.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
    self.Rewards = self.transform:Find("Rewards").gameObject
    self.MoneyLabel = self.transform:Find("Rewards/Money"):GetComponent(typeof(UILabel))
    self.ExpLabel = self.transform:Find("Rewards/Exp"):GetComponent(typeof(UILabel))
    self.Skills = self.transform:Find("Skills").gameObject
    self.SkillTemplate = self.transform:Find("Skills/SkillTemplate").gameObject
    self.SkillTable = self.transform:Find("Skills/Table"):GetComponent(typeof(UITable))
    self.Chest = self.transform:Find("Chest"):GetComponent(typeof(Animation))
    self.Fight = self.transform:Find("Fight").gameObject
    self.Open = self.transform:Find("Open"):GetComponent(typeof(Animation))
    self.Bottom = self.transform:Find("Bottom").gameObject
    self.BottomHint = self.transform:Find("Bottom/Label").gameObject
    self.Spiral = self.transform:Find("vfx_xuanwo").gameObject
    self.WndBg = self.transform:Find("Wnd_Bg").gameObject
    self.OkBtn = self.transform:Find("Bottom/OkButton").gameObject
    self.OkBtnLb = self.OkBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.CancelBtn = self.transform:Find("Bottom/CancelButton").gameObject
    UIEventListener.Get(self.OkBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOkBtnClicked()
    end)
    UIEventListener.Get(self.CancelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCancelBtnClick()
    end)
end

function LuaTreasureSeaEventWnd:OnEnable()
    self:OnSyncNavalWarSeatInfo(LuaHaiZhanMgr.s_SeatInfo)
    g_ScriptEvent:AddListener("NavalPvpAcceptSpecialEvent", self, "OnNavalPvpAcceptSpecialEvent")
    g_ScriptEvent:AddListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")
end

function LuaTreasureSeaEventWnd:OnDisable()
    g_ScriptEvent:RemoveListener("NavalPvpAcceptSpecialEvent", self, "OnNavalPvpAcceptSpecialEvent")
    g_ScriptEvent:RemoveListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")
end

function LuaTreasureSeaEventWnd:OnSyncNavalWarSeatInfo(seatInfo)
    self.m_IsCaptain = CClientMainPlayer.Inst and seatInfo and seatInfo[1] and seatInfo[1].id == CClientMainPlayer.Inst.Id
    self.m_CaptainName = seatInfo and seatInfo[1] and seatInfo[1].name or ""
    self:InitBottom()
end

function LuaTreasureSeaEventWnd:OnNavalPvpAcceptSpecialEvent(reasonId, bAccept, type, result)
    if not bAccept then 
        self.m_Done = true
        CUIManager.CloseUI(CLuaUIResources.TreasureSeaEventWnd)
        return
    end
    if type == 0 then -- 宝箱(只可能开出一个奖励)
        result = result and result[1] or {}
        if self.ChestTick then return end
        self.Chest:Play("treasureseaeventwnd_chest")
        self.ChestTick = RegisterTickOnce(function()
            for i = 0, self.transform.childCount - 1 do
                self.transform:GetChild(i).gameObject:SetActive(false)
            end
            if result.type == 4 then
                self.Spiral:SetActive(true)
            else
                self.Open.gameObject:SetActive(true)
                self.Open.transform:Find("Title"):GetChild(0).gameObject:SetActive(result.type == 2)
                self.Open.transform:Find("Title"):GetChild(1).gameObject:SetActive(result.type == 3)
                self.Open.transform:Find("Title"):GetChild(2).gameObject:SetActive(result.type == 1)
                self.Open.transform:Find("Gain/Exp").gameObject:SetActive(result.type == 2)
                self.Open.transform:Find("Gain/Skill").gameObject:SetActive(result.type == 3)
                self.Open.transform:Find("Gain/Coin").gameObject:SetActive(result.type == 1)
                if result.type == 3 then
                    local skill = Skill_AllSkills.GetData(result.val)
                    if skill then
                        self.Open.transform:Find("Gain/Skill/Icon"):GetComponent(typeof(CUITexture)):LoadSkillIconWithClip(skill.SkillIcon)
                        self.Open.transform:Find("Label"):GetComponent(typeof(UILabel)).text = skill.Name
                    end
                else
                    self.Open.transform:Find("Label"):GetComponent(typeof(UILabel)).text = "+"..result.val
                end
            end
        end, self.Chest["treasureseaeventwnd_chest"].length * 1000)
        self.CloseTick = RegisterTickOnce(function()
            self.m_Done = true
            CUIManager.CloseUI(CLuaUIResources.TreasureSeaEventWnd)
        end, 2500)
    elseif type == 1 then -- hunting
        self.m_Done = true
        CUIManager.CloseUI(CLuaUIResources.TreasureSeaEventWnd)
    end
end

function LuaTreasureSeaEventWnd:CloseChest()
    self.Chest.transform:GetChild(1):GetChild(0):GetComponent(typeof(UITexture)).alpha = 0
    self.Chest.transform:GetChild(3):GetChild(0):GetComponent(typeof(UITexture)).alpha = 1
    self.Chest.transform:Find("CUIFx/andi (2)").gameObject:SetActive(false)
end

function LuaTreasureSeaEventWnd:Init()
    CUIManager.CloseUI(CLuaUIResources.TreasureSeaHintWnd)
    UnRegisterTick(self.ChestTick)
    self.ChestTick = nil
    UnRegisterTick(self.CloseTick)
    self.CloseTick = nil
    for i = 0, self.transform.childCount - 1 do
        self.transform:GetChild(i).gameObject:SetActive(false)
    end
    self.Title.gameObject:SetActive(true)
    for i = 0, 3 do
        self.Title:GetChild(i).gameObject:SetActive(LuaTreasureSeaEventWnd.s_Type == i)
    end
    local info = LuaTreasureSeaEventWnd.s_Info
    self.ContentLabel.gameObject:SetActive(true)
    self.WndBg:SetActive(true)
    if LuaTreasureSeaEventWnd.s_Type == 0 then -- 宝箱
        self.ContentLabel.text = NavalWar_Setting.GetData().MysteryChestDesc
        self.Chest.gameObject:SetActive(true)
        self:CloseChest()
    elseif LuaTreasureSeaEventWnd.s_Type == 1 then -- 追猎
        self.Rewards:SetActive(true)
        self.ContentLabel.text = NavalWar_Setting.GetData().HuntingMissionDesc
        self.MoneyLabel.text = info.gold
        --self.ExpLabel.text = info.exp
        self.ExpLabel.gameObject:SetActive(false)
        LuaUtils.SetLocalPositionX(self.MoneyLabel.transform, -16)
    elseif LuaTreasureSeaEventWnd.s_Type == 2 then -- 幽灵
    elseif LuaTreasureSeaEventWnd.s_Type == 3 then -- 接舷
    end
    self:InitBottom()

    UIEventListener.Get(self.transform:Find("Wnd_Bg/CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsCaptain then
            Gac2Gas.NavalPvpAcceptSpecialEvent(LuaTreasureSeaEventWnd.s_PosIndex, LuaTreasureSeaEventWnd.s_NpcEngineId, LuaTreasureSeaEventWnd.s_EventId, false)
        else
            CUIManager.CloseUI(CLuaUIResources.TreasureSeaEventWnd)
        end
    end)
end

function LuaTreasureSeaEventWnd:InitBottom()
    UnRegisterTick(self.Tick)
    self.Tick = nil

    local okTxt = LuaTreasureSeaEventWnd.s_Type == 0 and LocalString.GetString("打开") or LocalString.GetString("接受")
    local cd = NavalWar_Setting.GetData().SpEventDuration
    self.OkBtnLb.text = okTxt.."("..cd..")"
    self.Tick = RegisterTick(function()
        cd = cd - 1
        self.OkBtnLb.text = okTxt.."("..cd..")"
        if cd == 0 and self.m_IsCaptain then
            self:OnOkBtnClicked()
        end
        if cd == -2 then 
            self.m_Done = true
            CUIManager.CloseUI(CLuaUIResources.TreasureSeaEventWnd)
        end
    end, 1000)

    self.Bottom:SetActive(true)
    self.OkBtn:SetActive(self.m_IsCaptain)
    self.CancelBtn:SetActive(self.m_IsCaptain)
    self.BottomHint:SetActive(not self.m_IsCaptain)
    if not self.m_IsCaptain then
        self.BottomHint:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("等待舵手%s选择"), self.m_CaptainName)
    end
end

function LuaTreasureSeaEventWnd:OnDestroy()
    UnRegisterTick(self.ChestTick)
    self.ChestTick = nil
    UnRegisterTick(self.CloseTick)
    self.CloseTick = nil
    UnRegisterTick(self.Tick)
    self.Tick = nil
    if LuaTreasureSeaEventWnd.s_OnDestroyCallback then
        LuaTreasureSeaEventWnd.s_OnDestroyCallback()
        LuaTreasureSeaEventWnd.s_OnDestroyCallback = nil
    end
    if self.m_IsCaptain and not self.m_Done then
        Gac2Gas.NavalPvpAcceptSpecialEvent(LuaTreasureSeaEventWnd.s_PosIndex, LuaTreasureSeaEventWnd.s_NpcEngineId, LuaTreasureSeaEventWnd.s_EventId, false)
    end
end

function LuaTreasureSeaEventWnd:CheckStatus(showMsg) 
    if self.m_IsCaptain and CClientMainPlayer.Inst then
        local myShipId = LuaHaiZhanMgr.s_EngineId2ShipId[math.abs(CClientMainPlayer.Inst.AppearanceProp.DriverId)]
        if myShipId then
            local statusInfo = LuaHaiZhanMgr.s_Status[myShipId]
            if statusInfo then
                local timeStamp = CServerTimeMgr.Inst.timeStamp
                local status,start,duration = statusInfo[1],statusInfo[2],statusInfo[3]
                if status == EnumNavalPlayShipStatus.Escape and timeStamp >= start and timeStamp <= duration+start then
                    if showMsg then
                        g_MessageMgr:ShowMessage("NAVALPVP_ACCEPT_SPECIAL_EVENT_FAIL_ESCAPE")
                    end
                    return false
                end
            end
        end
        return true
    end
    if showMsg then
        g_MessageMgr:ShowMessage("NAVALPVP_ACCEPT_SPECIAL_EVENT_FAIL_DRIVER")
    end
end

function LuaTreasureSeaEventWnd:OnOkBtnClicked()
    if not self:CheckStatus(true) then 
        self.m_Done = true
        CUIManager.CloseUI("TreasureSeaEventWnd")
        return
    end
    --UnRegisterTick(self.Tick)
    --self.Tick = nil
    Gac2Gas.NavalPvpAcceptSpecialEvent(LuaTreasureSeaEventWnd.s_PosIndex, LuaTreasureSeaEventWnd.s_NpcEngineId, LuaTreasureSeaEventWnd.s_EventId, true)
    if LuaTreasureSeaEventWnd.s_Type == 0 then
        --改成由服务器回调触发
        --[[
        if self.ChestTick then return end
        self.Chest:Play("treasureseaeventwnd_chest")
        self.ChestTick = RegisterTickOnce(function()
            for i = 0, self.transform.childCount - 1 do
                self.transform:GetChild(i).gameObject:SetActive(false)
            end
            
            if LuaTreasureSeaEventWnd.s_Info.isTrap then
                self.Spiral:SetActive(true)
            else
                self.Open.gameObject:SetActive(true)
                self.Open.transform:Find("Title"):GetChild(0).gameObject:SetActive(false)
                self.Open.transform:Find("Title"):GetChild(1).gameObject:SetActive(true)
                self.Open.transform:Find("Title"):GetChild(2).gameObject:SetActive(false)
            end
        end, self.Chest["treasureseaeventwnd_chest"].length * 1000)
        --]]
    end
end

function LuaTreasureSeaEventWnd:OnCancelBtnClick()
    if not self:CheckStatus() then 
        self.m_Done = true
        CUIManager.CloseUI("TreasureSeaEventWnd")
        return
    end
    Gac2Gas.NavalPvpAcceptSpecialEvent(LuaTreasureSeaEventWnd.s_PosIndex, LuaTreasureSeaEventWnd.s_NpcEngineId, LuaTreasureSeaEventWnd.s_EventId, false)
end