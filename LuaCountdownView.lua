local CScene=import "L10.Game.CScene"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CEnumSituationType=import "L10.UI.EnumSituationType"
local CSituationWndMgr=import "L10.UI.CSituationWndMgr"
local CButton = import "L10.UI.CButton"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local CDaDiZiMgr = import "L10.Game.CDaDiZiMgr"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CQMJJMgr = import "L10.Game.CQMJJMgr"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CLingshouLuandouMgr = import "L10.Game.CLingshouLuandouMgr"
local CDuoMaoMaoMgr = import "L10.Game.CDuoMaoMaoMgr"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local CZhongQiuMgr = import "L10.Game.CZhongQiuMgr"
local CLianLianKanMgr = import "L10.Game.CLianLianKanMgr"
local CLiaoLuoWanMgr = import "L10.Game.CLiaoLuoWanMgr"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local CValentineMgr = import "L10.UI.CValentineMgr"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local CPGQYMgr = import "L10.Game.CPGQYMgr"
local CTowerDefenseMgr = import "L10.Game.CTowerDefenseMgr"
local CHorseRaceMgr = import "L10.Game.CHorseRaceMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChristmasHongLvMgr = import "L10.Game.CChristmasHongLvMgr"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local KitePlayMgr = import "L10.Game.KitePlayMgr"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local CGaoChangMgr = import "L10.Game.CGaoChangMgr"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CGuildQueenMgr = import "L10.Game.CGuildQueenMgr"
local Gameplay_Gameplay = import "L10.Game.Gameplay_Gameplay"
local UILabelOverflow = import "UILabel+Overflow"

LuaCountDownView = class()

g_CountDownViewForGamePlayFunc = {}
g_CountDownViewGamePlayName = nil

RegistClassMember(LuaCountDownView, "m_GameplayNameLabel")
RegistClassMember(LuaCountDownView, "m_RemainTimeLabel")
RegistClassMember(LuaCountDownView, "m_LeaveBtn")
RegistClassMember(LuaCountDownView, "m_SituationBtn")


function LuaCountDownView:Awake()
    self.m_GameplayNameLabel = self.transform:Find("TaskName"):GetComponent(typeof(UILabel))
    self.m_RemainTimeLabel = self.transform:Find("TaskInfo"):GetComponent(typeof(UILabel))
    self.m_LeaveBtn = self.transform:Find("LeaveBtn"):GetComponent(typeof(CButton))
    self.m_SituationBtn = self.transform:Find("SituationBtn"):GetComponent(typeof(CButton))
end

function LuaCountDownView:InitContent()

    self.m_LeaveBtn.Text = LocalString.GetString("离开")
    self.m_SituationBtn.Text = LocalString.GetString("战况")
    --添加离开按钮的默认行为,战况按钮由玩法自己实现
    CommonDefs.AddOnClickListener(self.m_LeaveBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnLeaveButtonClick(go) end), false)

    local mainScene = CScene.MainScene
    if mainScene then
        self.m_GameplayNameLabel.text = g_CountDownViewGamePlayName or mainScene.SceneName
        self.m_RemainTimeLabel.enabled = mainScene.ShowTimeCountDown or mainScene.ShowMonsterCountDown
        self.m_LeaveBtn.gameObject:SetActive(mainScene.ShowLeavePlayButton)
        self:SetRemainTimeVal()
    else
        self.m_GameplayNameLabel.text = ""
        self.m_RemainTimeLabel.enabled = false
        self.m_LeaveBtn.gameObject:SetActive(false)
    end

    self:InitButtons()
end

function LuaCountDownView:OnEnable()
    self:InitContent()
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayCreated")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:AddListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
    g_ScriptEvent:AddListener("GamePlayRemainMonsterNumUpdate", self, "OnGamePlayRemainMonsterNumUpdate")
    g_ScriptEvent:AddListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
    g_ScriptEvent:AddListener("LLWSyncSeatIndex", self, "SetLiaoLuoWan")
    g_ScriptEvent:AddListener("YuanDanQiangDanRoundInfoUpdate", self, "SetQuanMinQiangDan")
    g_ScriptEvent:AddListener("UpdateCountDownWndGamePlayName", self, "OnUpdateCountDownWndGamePlayName")
end


function LuaCountDownView:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayCreated")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
    g_ScriptEvent:RemoveListener("GamePlayRemainMonsterNumUpdate", self, "OnGamePlayRemainMonsterNumUpdate")
    g_ScriptEvent:RemoveListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
    g_ScriptEvent:RemoveListener("LLWSyncSeatIndex", self, "SetLiaoLuoWan")
    g_ScriptEvent:RemoveListener("YuanDanQiangDanRoundInfoUpdate", self, "SetQuanMinQiangDan")
    g_ScriptEvent:RemoveListener("UpdateCountDownWndGamePlayName", self, "OnUpdateCountDownWndGamePlayName")
end

function LuaCountDownView:OnLeaveButtonClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaCountDownView:OnMainPlayCreated(args)
    self:InitContent()
end

function LuaCountDownView:OnRemainTimeUpdate(args)
    self:SetRemainTimeVal()
end

function LuaCountDownView:OnUpdateCountDownWndGamePlayName()
      self.m_GameplayNameLabel.text = g_CountDownViewGamePlayName or (CScene.MainScene and CScene.MainScene.SceneName  or "")
end

function LuaCountDownView:OnCanLeaveGamePlay(args)
    --MEMO 这里其实有点问题，很多玩法里面已经开始自定义离开按钮的内容了，目前是C#转lua先保持原来的写法，以后遇到问题再一并解决
    local mainScene = CScene.MainScene
    if mainScene then
        self.m_LeaveBtn.gameObject:SetActive(mainScene.ShowLeavePlayButton)
    end
end

function LuaCountDownView:OnGamePlayRemainMonsterNumUpdate(args)
    local mainScene = CScene.MainScene
    if mainScene and mainScene.ShowMonsterCountDown then
        self:SetRemainTimeVal()
    end
end

function LuaCountDownView:OnShowTimeCountDown(args)
    local mainScene = CScene.MainScene
    if mainScene then
        self.m_RemainTimeLabel.enabled =  mainScene.ShowTimeCountDown or mainScene.ShowMonsterCountDown
    end
end

function LuaCountDownView:SetLiaoLuoWan()
    if CLiaoLuoWanMgr.Inst.m_MySeatIndex >= 0 then
        self:ShowTwoCustomButton(LocalString.GetString("离开"), LocalString.GetString("下船"), function(go)
            CGamePlayMgr.Inst:LeavePlay()
        end, function(go)

            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LLW_Request_Land_Confirm"), function ()
                if CClientMainPlayer.Inst then
                    Gac2Gas.LLW_RequestLand(CClientMainPlayer.Inst.Id, "")
                end
            end, nil, nil, nil, false)
            
        end)
    else
        self:ShowCustomButton(LocalString.GetString("离开"), function(go)
            CGamePlayMgr.Inst:LeavePlay()
        end, true)
    end
end

function LuaCountDownView:SetQuanMinQiangDan()
    local mainScene = CScene.MainScene
    if mainScene then
        self.m_GameplayNameLabel.text = SafeStringFormat3(LocalString.GetString("%s-第%d场"), mainScene.SceneName, CYuanDanMgr.Inst.CurQiangDanRound)
    end
end

function LuaCountDownView:SetRemainTimeVal()
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.m_RemainTimeLabel.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.m_RemainTimeLabel.text = ""
        end
    else
        self.m_RemainTimeLabel.text = ""
    end
end

function LuaCountDownView:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),  
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60), 
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

--只显示离开按钮，离开按钮的行为是调用CGamePlayMgr.Inst:LeavePlay()
function LuaCountDownView:ShowLeaveButton()
    self:ShowButtonsInternal(true, false, LocalString.GetString("离开"), function(go)
        CGamePlayMgr.Inst:LeavePlay()
    end, nil, nil)
end

--显示Custom功能按钮，离开按钮可以强制隐藏或采用CScene.MainScene.ShowLeavePlayButton的默认配置（如果隐藏离开按钮，内部实际上是利用了离开按钮来做Custom按钮，保证位置靠左侧）
function LuaCountDownView:ShowCustomButton(customButtonText, onCustomButtonClick, needForceHideLeaveButton)
    local hideLeaveButton = needForceHideLeaveButton or(CScene.MainScene == nil or not CScene.MainScene.ShowLeavePlayButton)
    if hideLeaveButton then
        self:ShowButtonsInternal(true, false, customButtonText, "", onCustomButtonClick, nil)
    else
        self:ShowLeaveAndCustomButton(customButtonText, onCustomButtonClick)
    end
end

--显示离开和规则按钮，离开按钮的行为是调用CGamePlayMgr.Inst:LeavePlay()，规则按钮的行为是显示一条Message
function LuaCountDownView:ShowLeaveAndRuleButtonWithRuleMessage(messageName)
    if CScene.MainScene and CScene.MainScene.GamePlayDesignId == Christmas2022_Setting.GetData().GamePlayId then
        return
    end
    self:ShowButtonsInternal(true, true, LocalString.GetString("离开"), LocalString.GetString("规则"), function(go)
        CGamePlayMgr.Inst:LeavePlay()
    end, function(go)
        g_MessageMgr:ShowMessage(messageName)
    end)
end

--显示离开和自定义按钮，离开按钮的行为是调用CGamePlayMgr.Inst:LeavePlay()，自定义按钮的文本和行为以传入为准
function LuaCountDownView:ShowLeaveAndCustomButton(customButtonText, onCustomButtonClick)
    self:ShowButtonsInternal(true, true, LocalString.GetString("离开"), customButtonText, function(go)
        CGamePlayMgr.Inst:LeavePlay()
    end, onCustomButtonClick)
end

--显示两个自定义按钮，自定义按钮的文本和行为以传入为准
function LuaCountDownView:ShowTwoCustomButton(button1Text, button2Text, onButton1Click, onButton2Click)
    self:ShowButtonsInternal(true, true, button1Text, button2Text, onButton1Click, onButton2Click)
end

--一般情况下不需要调用这里，请使用上述方法中的一种来满足需求
function LuaCountDownView:ShowButtonsInternal(button1Visible, button2Visible, button1Text, button2Text, onButton1Click, onButton2Click)
    self.m_LeaveBtn.gameObject:SetActive(button1Visible and button1Visible or false)
    self.m_SituationBtn.gameObject:SetActive(button2Visible and button2Visible or false)
    self.m_LeaveBtn.Text = button1Text~=nil and button1Text or  LocalString.GetString("离开")
    self.m_SituationBtn.Text = button2Text~=nil and button2Text or LocalString.GetString("战况")

    if onButton1Click~=nil then
        UIEventListener.Get(self.m_LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(onButton1Click)
    end
    if onButton1Click~=nil then
        UIEventListener.Get(self.m_SituationBtn.gameObject).onClick = DelegateFactory.VoidDelegate(onButton2Click)
    end
end

--InitButtons0来自原CCountDownView.cs的Init内容，转Lua后单独放这里，万一有问题patch范围可以小一点
function LuaCountDownView:InitButtons() 

    if CGuildLeagueMgr.Inst.inGuildLeagueBattle then    -- r255347 r325276 帮会联赛
        self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
            LuaGuildLeagueMgr:ShowGuildLeagueSituationWnd(false)
        end)
    elseif CDaDiZiMgr.Inst:IsInDaDiZiBattleScene() then -- r255347 首席大弟子
        self:ShowLeaveAndCustomButton(LocalString.GetString("查看对阵"), function(go)
            CUIManager.ShowUI(CUIResources.DaDiZiBattleMatchWnd)
        end)
    elseif CDaDiZiMgr.Inst:IsInDaDiZiPrepareScene() then -- #78276 首席大弟子准备场景
        self:ShowCustomButton(LocalString.GetString("查看对阵"), function(go)
            CUIManager.ShowUI(CUIResources.DaDiZiBattleMatchWnd)
        end, true)
    elseif CBiWuDaHuiMgr.Inst.inBiWu or CQMJJMgr.Inst.inQMJJ then -- #64987 #79473 比武和2017清明2017清明祭酒（复用比武大会）
        self:ShowTwoCustomButton(LocalString.GetString("参赛调整"), LocalString.GetString("战况"), function(go)
            if CBiWuDaHuiMgr.Inst.inBiWu then
                Gac2Gas.RequestOpenBiWuSelectWnd()
            elseif CQMJJMgr.Inst.inQMJJ then
                Gac2Gas.RequestOpenQMJJSelectWnd()
            end
        end, function(go)
            CUIManager.ShowUI(CUIResources.BWDHResultWnd)
        end)
    elseif LuaBiWuDaHuiMgr.IsInBiWuPrepare() then
        self:ShowCustomButton(LocalString.GetString("对阵信息"), function(go)
            CUIManager.ShowUI(CLuaUIResources.BiWuCrossInfoWnd)
        end, false)
    elseif CBiWuDaHuiMgr.Inst.inBiWuPrepare or CQMJJMgr.Inst.inQMJJPrepare then -- #70440 #79473 比武和2017清明祭酒（复用比武大会） 
        self:ShowCustomButton(LocalString.GetString("对阵信息"), function(go)
            CUIManager.ShowUI(CUIResources.BWDHMatchWnd)
        end, false)
    elseif CFightingSpiritMgr.Instance:IsDouhunLocalPrepare() then -- #76718 斗魂
        self:ShowTwoCustomButton(LocalString.GetString("队伍调整"), LocalString.GetString("对阵"), function(go)
            Gac2Gas.QueryPlayerDouHunChlgTeamInfo()
        end, function(go)
            CUIManager.ShowUI(CLuaUIResources.FightingSpiritLocalMatchInfoWnd)
        end)
    elseif CFightingSpiritMgr.Instance:IsDouhunCrossPrepare() then -- #76718 斗魂
        self:ShowTwoCustomButton(LocalString.GetString("队伍调整"), LocalString.GetString("战况"), function(go)
            Gac2Gas.QueryPlayerDouHunChlgTeamInfo()
        end, function(go)
            Gac2Gas.OpenCrossDouHunWatchWnd()
        end)
    elseif CFightingSpiritMgr.Instance:IsDouhunFighting() then -- #76348 斗魂
        self:ShowTwoCustomButton(LocalString.GetString("我的战队"), LocalString.GetString("战况"), function(go)
            Gac2Gas.QueryPlayerDouHunChlgTeamInfo()
        end, function(go)
            if CGameVideoMgr.Inst:IsInGameVideoScene() then
                CGameVideoMgr.Inst:QueryDouHunPlayInfo()
            else
                Gac2Gas.QueryDouHunPlayInfo()
            end
        end)
    elseif CLingshouLuandouMgr.Instance:IsLuandou() then -- r241692 寒假活动在左侧任务栏添加了战况按钮
        self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
            Gac2Gas.RequestLingShouBattleRank()
        end)
    elseif CDuoMaoMaoMgr.Inst.InHideAndSeek then -- #81547 躲猫猫
        self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
            Gac2Gas.QueryDuoMaoMaoPlayInfo()
        end)
    elseif CXialvPKMgr.Inst.InXiaLvPK then -- #86089 【侠侣PK赛】
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            local setting = XiaLvPK_Setting.GetData()
            -- 跨服战斗 - 观战
            if CXialvPKMgr.Inst.InXiaLvPkCross or CXialvPKMgr.Inst.InXiaLvPkGuanZhan then
                Gac2Gas.CrossXiaLvPkQueryForceRoundData()
            else -- 本服战斗
                Gac2Gas.XiaLvPkQueryForceRoundData()
            end
        end, false)
    elseif CXialvPKMgr.Inst.InXialvPKPrepare then -- #86089 【侠侣PK赛】
        self:ShowCustomButton(LocalString.GetString("对阵信息"), function(go)
            CUIManager.ShowUI(CUIResources.BWDHMatchWnd)
        end, false)
    elseif CGamePlayMgr.Inst.IsInJuQingPlay then  -- #89735 英雄剧情战况按钮
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CUIManager.ShowUI(CUIResources.TeamDamageStatWnd)
        end, false)
    elseif CZhongQiuMgr.Inst.InYBZD then --  #89102 2017中秋月饼争夺赛
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            Gac2Gas.QueryYBZDPlayInfo()
        end, false)
    elseif CLianLianKanMgr.Instance:IsInLianLianKanPrepare() then -- #91468 连连看决赛准备场
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CUIManager.ShowUI("LianLianKanBattleMatchWnd")
        end, true)
    elseif CLianLianKanMgr.Instance:IsInLianLianKan() then --  #91470 连连看
        self:ShowLeaveButton()
    elseif CLiaoLuoWanMgr.Inst.IsInLLWPlay then -- #90588 料罗湾之战剧情任务海战训练小游戏
        self:SetLiaoLuoWan()
    elseif CYuanDanMgr.Inst.IsInQMQDPlay then -- #92664 【元旦】全民抢蛋
        self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
            CYuanDanMgr.Inst.RequestQiangDanPlayInfo()
        end)
        self:SetQuanMinQiangDan()
    elseif CValentineMgr.Inst.InValentineVoiceLove then --  #94771 爱意四海宣
        self:ShowButtonsInternal(false, false, nil, nil, nil, nil) --按钮全部隐藏
    elseif CWeekendFightMgr.Inst.inPrepare then -- #97459 新周末活动
        self:ShowLeaveButton()
    elseif CWeekendFightMgr.Inst.inBaoWeiNanGua then -- #106314 万圣节活动保卫南瓜
        self:ShowLeaveButton()
    elseif CChristmasHongLvMgr.Inst:IsInHongLvFuBen() then -- #94587 [圣诞2017]红绿大作战
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CChristmasHongLvMgr.Inst:ShowZhanKuanMessage()
        end, false)
    elseif CGaoChangMgr.Inst.initSign then
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CUIManager.ShowUI(LuaGaoChangJiDouMgr:IsInJiDou() and CLuaUIResources.YuanXiaoGaoChangResultWnd or CUIResources.GaoChangResultWnd)
        end, false)
    elseif LuaGaoChangCrossMgr:IsInScene() then
        self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
            CUIManager.ShowUI(LuaGaoChangJiDouMgr:IsInJiDou() and CLuaUIResources.YuanXiaoGaoChangResultWnd or CUIResources.GaoChangResultWnd)
        end)
    elseif CGuildQueenMgr.Inst.InBangHuaFuBen then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CSituationWndMgr.OpenSituationWnd(CEnumSituationType.BangHua)
        end, false)
    elseif CPGQYMgr.Inst.IsInPGQY then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CSituationWndMgr.OpenSituationWnd(CEnumSituationType.PGQY)
        end, false)
    elseif CTowerDefenseMgr.Inst:IsInTowerDefenseScene() then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CUIManager.ShowUI("TowerDefenseStatusWnd")
        end, false)
    elseif CHorseRaceMgr.Inst.InHorseRace then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            Gac2Gas.MPTYSRequestRankInfo()
        end, false)
    elseif CGamePlayMgr.Inst.IsInHuluBrothersPlay then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CUIManager.ShowUI(CUIResources.HuluBrothersRankWnd)
        end, false)
    elseif CSpringFestivalMgr.Instance:IsInNianshouGame() then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            Gac2Gas.RequestNSDZZBattleInfo()
        end, false)
    elseif KitePlayMgr.Inst:IsInYuanyuPlay() then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            Gac2Gas.RequestYuanYuRankData()
        end, false)
    elseif CCityWarMgr.Inst:IsInCityWarCopyScene() then -- 
        self:ShowCustomButton(LocalString.GetString("战况"), function(go)
            CUIManager.ShowUI(CUIResources.CityWarResultWnd)
        end, false)
    else
        self:InitButtons2() --原Lua下的内容
    end

end

function LuaCountDownView:InitButtons2()
    if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        local gameplayData = Gameplay_Gameplay.GetData(gamePlayId)
        if gameplayData and gameplayData.OnlyShowLeaveButton == 1 then
            self:ShowLeaveButton()
        elseif gamePlayId==51100795 then--端午大作战
            self:ShowLeaveButton()
        elseif gamePlayId == 51100803 or 51100804 == gamePlayId then

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                Gac2Gas.QueryZhuoJiPlayInfo()
            end)

        elseif gamePlayId==51100170 then--寇岛

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                CSituationWndMgr.OpenSituationWnd(CEnumSituationType.Koudao)
            end)

        elseif gamePlayId == 51100816 then

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                Gac2Gas.RequestQueryWCSBBattleInfo()
            end)

        elseif gamePlayId == 51100444 then--全民pk
            self:ShowCustomButton(LocalString.GetString("战况"), function(go)
                CLuaQMPKMgr.m_IsGameVideoBattleStatus = false
                CUIManager.ShowUI(CLuaUIResources.QMPKCurrentBattleStatusWnd)
            end)
            -- self:ShowTwoCustomButton(LocalString.GetString("参赛调整"), LocalString.GetString("战况"), function(go)
            --     Gac2Gas.RequestQmpkBiWuChuZhanInfo()
            -- end, function(go)
            --     CLuaQMPKMgr.m_IsGameVideoBattleStatus = false
            --     CUIManager.ShowUI(CLuaUIResources.QMPKCurrentBattleStatusWnd)
            -- end)

        elseif gamePlayId == 51101092 then -- 明星邀请赛

            self:ShowTwoCustomButton(LocalString.GetString("参赛调整"), LocalString.GetString("战况"), function(go)
                Gac2Gas.RequestStarBiWuChuZhanInfo()
            end, function(go)
                CLuaStarBiwuMgr.m_IsGameVideoBattleStatus = false
                CUIManager.ShowUI(CLuaUIResources.StarBiwuCurrentBattleStatusWnd)
            end)

        elseif gamePlayId == 51100864 then --国庆校场

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                Gac2Gas.RequestPlayInfoGQJC()
            end)

        elseif gamePlayId == 51100871 then --连连看2018

            self:ShowLeaveButton()

        elseif gamePlayId == MonsterSiege_Setting.GetData().GamePlayId then --怪物攻城隐藏战况

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                CUIManager.ShowUI(CLuaUIResources.CityWarMonsterSiegeSituationWnd)
            end)

        elseif gamePlayId == tonumber(TianMenShanBattle_Setting.GetData().GamePlayId) then -- 天门山副本

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                LuaBattleSituationMgr.OpenBattleSituationWnd(EnumSituationType.eTianMenShan)
            end)

        elseif gamePlayId == 51101100 then

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                Gac2Gas.RequestPengPengCheBattleInfo()
            end)

        elseif gamePlayId == LuaWuJianDiYuMgr.gamePlayId then--无间地狱

            self:ShowLeaveAndCustomButton(LocalString.GetString("规则"), function(go)
                LuaWuJianDiYuMgr:ShowRuleWnd()
            end)

        elseif LuaNationalDayMgr.IsInTXZRGameplay(gamePlayId) then--天选之人
            self:ShowLeaveAndRuleButtonWithRuleMessage("NationalDay_TXZR_Rule")
        elseif LuaNationalDayMgr.IsInJCYWGameplay(gamePlayId) then--校场演武
            self:ShowLeaveAndRuleButtonWithRuleMessage("NationalDay_JCYW_Rule")
        elseif gamePlayId == 51101253 then--染色剂大暴走
            self:ShowLeaveAndRuleButtonWithRuleMessage("CrazyDye_Rule")
        elseif gamePlayId == 51101137 then --中秋2019
            self:ShowLeaveButton()
        elseif gamePlayId == 51101250 then --bug围场
            self:ShowLeaveAndRuleButtonWithRuleMessage("Halloween2019_Rule")
        elseif gamePlayId == 51101263 then --拯救圣诞树
            self:ShowLeaveAndRuleButtonWithRuleMessage("SaveChristmasTree_Rule")
        elseif gamePlayId == 51101260 then --礼物大作战

            self:ShowLeaveAndCustomButton(LocalString.GetString("战况"), function(go)
                Gac2Gas.QueryChristmasGiftBattlePlayInfo()
            end)

        elseif gamePlayId == 51101373 or gamePlayId == 51101374 then

            self:ShowLeaveAndCustomButton(LocalString.GetString("积分排行"), function(go)
                CLuaCrossDouDiZhuRankWnd.m_IsAllRank = false
                CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuRankWnd)
            end)

        elseif LuaYuanXiao2020Mgr:CheckInDuiDuiLeGamePlay() then
            self:ShowLeaveButton()
        elseif gamePlayId == 51101501 then--解字消愁
            self:ShowLeaveButton()
        elseif LuaChildrenDay2020Mgr.InGamePlay() then
            self:ShowLeaveAndRuleButtonWithRuleMessage("ChildrenDayGame2020Tip")
        elseif gamePlayId == 51101530 then --地狱誓空
            self:ShowLeaveAndRuleButtonWithRuleMessage("WuYi2020_DYSK_Tips")
        elseif gamePlayId == 51101532 then --货运烽火令
            self:ShowLeaveAndRuleButtonWithRuleMessage("WuYi2020_QuanMinHuoYun_FHL_Tips")
        elseif gamePlayId == 51101560 or gamePlayId == 51101561 or gamePlayId == 51101562 or gamePlayId == 51101563 or gamePlayId == 51101564 then
            -- 白蝶风波有5个副本
            self:ShowLeaveButton()
        elseif gamePlayId == Double11_Setting.GetData().ClearTrolleyGamePlayId then
            self:ShowLeaveAndRuleButtonWithRuleMessage("CLEARTROLLEY_README")
        elseif gamePlayId == WuYi2021_Setting.GetData().GameplayId then
            self:ShowLeaveAndRuleButtonWithRuleMessage("WuYi2021_MXJX_FHL_Tips")
        elseif gamePlayId == LiuYi2021_Setting.GetData().GameplayId  then
            self:ShowLeaveAndRuleButtonWithRuleMessage("LiuYi_2021_TangGuo_Rule_Desc")
        elseif ZhongYuanJie2020Mgr:IsRJPMGamePlay(gamePlayId) then
            --中元节2020的处理从ZhongYuanJie2020Mgr搬过来
            self:ShowLeaveAndCustomButton(LocalString.GetString("排行"), function(go)
                Gac2Gas.RJPMQueryRank()
            end)
        elseif gamePlayId == OffWorldPass_Setting.GetData().JiaoshanGameplayId then
            self:ShowLeaveButton()
        elseif gamePlayId == Double11_Setting.GetData().KanYiDao_GamePlayId then
            self:ShowLeaveAndRuleButtonWithRuleMessage("KanYiDao_Rule")
        elseif gamePlayId == WeddingIteration_Setting.GetData().WeddingNDFPlayId then
            self:ShowTwoCustomButton(LocalString.GetString("离开"), LocalString.GetString("规则"), function(go)
                LuaWeddingIterationMgr:LeaveDongFang()
            end, function(go)
                g_MessageMgr:ShowMessage("WEDDING_NAODONGFANG_RULE")
            end)
        elseif gamePlayId == QingMing2022_PVESetting.GetData().GameplayId then
            self:ShowTwoCustomButton(LocalString.GetString("离开"), LocalString.GetString("规则"), function(go)
                LuaQingMing2022Mgr:LeaveYHPZScene()
            end, function(go)
                LuaQingMing2022Mgr:ShowYHPZImageRuleWnd()
            end)
        elseif LuaDuanWu2022Mgr:IsInWuduPlay() then -- 端午五毒
            self:ShowLeaveAndRuleButtonWithRuleMessage("DuanWu_2022_WuDu_Introduction")

        elseif LuaJuDianBattleMgr:IsInDailyGameplay() then
            self:ShowCustomButton(LocalString.GetString("回本服"), function(go)
                LuaJuDianBattleMgr:RequestBackSelfSever()
            end, true)
        elseif gamePlayId == CuJu_Setting.GetData().GamePlayId then
            self:ShowLeaveAndRuleButtonWithRuleMessage("CUJU_RULE")
        --！！！ 如果不是和常数（gameplay id）比较，则请在上方添加新的elseif分支，否则请移步至文件末尾追加新的成员到g_CountDownViewForGamePlayFunc
        elseif g_CountDownViewForGamePlayFunc[gamePlayId] then
            g_CountDownViewForGamePlayFunc[gamePlayId](self, gamePlayId)
        end
    end
end

--------------------------------------
-- 根据gameplay id确定显示按钮及行为
--------------------------------------

g_CountDownViewForGamePlayFunc[51101849] = function(wnd, gamePlayId) -- #141821 安期岛之战
    wnd:ShowTwoCustomButton(LocalString.GetString("规则"), LocalString.GetString("战况"), function(go)
        CUIManager.ShowUI(CLuaUIResources.AnQiDaoTipWnd)
    end, function(go)
        CLuaGuanNingMgr.ShowLastBattleData = true
        CUIManager.ShowUI(CLuaUIResources.AnQiDaoResultWnd)
    end)
end

g_CountDownViewForGamePlayFunc[51101850] = function(wnd, gamePlayId) -- #143304 2020圣诞-pve堆雪人
    wnd:ShowLeaveAndRuleButtonWithRuleMessage("ShengDan2020_CreateSnowman") 
end

g_CountDownViewForGamePlayFunc[51101252] = function(wnd, gamePlayId) -- #144971 【策划工具】技能、怪物编辑工具
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("地图编辑"), function(go)
        if not CLuaMapEditMgr then return end
        CLuaMapEditMgr:OnClickEdit()
    end)
end

g_CountDownViewForGamePlayFunc[51101934] = function(wnd, gamePlayId) -- #145980 萌宠大冒险
    wnd:ShowLeaveAndRuleButtonWithRuleMessage("Pet_Adventure_RuleMesssage")
end

g_CountDownViewForGamePlayFunc[51102144] = function(wnd, gamePlayId) -- #155986 奥运2021
    wnd:ShowLeaveButton()
end

g_CountDownViewForGamePlayFunc[51102271] = function(wnd, gamePlayId) -- #157746 罗刹海市
    wnd:ShowButtonsInternal(true, false, LocalString.GetString("规则"), nil, function(go)
        g_MessageMgr:ShowMessage("LuoChaHaiShi_Rule_Introduction")
    end, nil)
end

g_CountDownViewForGamePlayFunc[51102350] = function(wnd, gamePlayId) -- #161137 【寒假活动】雪镜狂欢 XueJingKuangHuanSetting GamePlayId
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("规则"), function(go)
        LuaHanJiaMgr:ShowXueJingKuangHuanImageRuleWnd()
    end, nil)
end

g_CountDownViewForGamePlayFunc[51102729] = function(wnd, gamePlayId) -- 六一疯狂泡泡活动
    wnd:ShowLeaveAndRuleButtonWithRuleMessage("LiuYi_2022_Bubble_Introduction")
end

g_CountDownViewForGamePlayFunc[51102971] = function(wnd, gamePlayId) -- #国庆2022PvP
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("技能"), function(go)
        Gac2Gas.QueryJinLuHunYuanZhanSkillInfo()
    end)
end
g_CountDownViewForGamePlayFunc[51102972] = function(wnd, gamePlayId) -- #国庆2022PvE
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("规则"), function(go)
        g_MessageMgr:ShowMessage("XiangYaoYuQiuSi_TaskBar_Rule")
    end)
end
g_CountDownViewForGamePlayFunc[51103193] = function(wnd, gamePlayId) -- #端午2023PVPVE
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("规则"), function(go)
        LuaDuanWu2023Mgr:ShowPVPVERuleWnd()
    end)
end
g_CountDownViewForGamePlayFunc[51103194] = function(wnd, gamePlayId) -- #周年庆2023一崽
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("规则"), function(go)
        g_MessageMgr:ShowMessage("ANNIV2023_ZAIZAI_TIPS1")
    end)
end
g_CountDownViewForGamePlayFunc[51103195] = function(wnd, gamePlayId) -- #周年庆2023二崽
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("规则"), function(go)
        g_MessageMgr:ShowMessage("ANNIV2023_ZAIZAI_TIPS2")
    end)
end
g_CountDownViewForGamePlayFunc[51103196] = function(wnd, gamePlayId) -- #周年庆2023三崽
    wnd:ShowLeaveAndCustomButton(LocalString.GetString("规则"), function(go)
        g_MessageMgr:ShowMessage("ANNIV2023_ZAIZAI_TIPS3")
    end)
end

g_CountDownViewForGamePlayFunc[51103304] = function(wnd, gamePlayId) -- 猛犬2023
    wnd:ShowLeaveAndRuleButtonWithRuleMessage("Double11_MengQuanHengXing_Tips")
end

g_CountDownViewForGamePlayFunc[51103327] = function(wnd, gamePlayId) -- #204772 圣诞2023冰雪大世界
    wnd:ShowLeaveAndRuleButtonWithRuleMessage("Christmas2023_BingXue_TIPS")
end

------ 

function Gas2Gac.UpateCountDownWndGamePlayName(name)
    if name == "" then
        g_CountDownViewGamePlayName = nil
    else
        g_CountDownViewGamePlayName = name
    end
    g_ScriptEvent:BroadcastInLua("UpdateCountDownWndGamePlayName")
end