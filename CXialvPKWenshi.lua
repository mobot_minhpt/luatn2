-- Auto Generated!!
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CKeJuQuestionTemplate = import "L10.UI.CKeJuQuestionTemplate"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CXialvPKWenshi = import "L10.UI.CXialvPKWenshi"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
CXialvPKWenshi.m_Init_CS2LuaHook = function (this, questionTemplate)
    questionTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.questionAnchor)
    local question = NGUITools.AddChild(this.questionAnchor.gameObject, questionTemplate)
    question:SetActive(true)
    question.transform.localPosition = Vector3.zero
    this.questionBoard = CommonDefs.GetComponent_GameObject_Type(question, typeof(CKeJuQuestionTemplate))
    this.questionBoard:Init(false)

    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.countDown = CKeJuMgr.Inst.questionCountDown

    if this.countDown <= 1 then
        this.countdownLabel.text = LocalString.GetString("0秒后下一题")
        this.progressBar.value = 0
    else
        this.countdownLabel.text = System.String.Format(LocalString.GetString("{0}秒后下一题"), this.countDown)
        this.progressBar.value = this.countDown / CKeJuMgr.Inst.questionCountDown
    end
    this.cancel = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function () 
        if this.countDown <= 1 then
            this.countdownLabel.text = LocalString.GetString("0秒后下一题")
            this.progressBar.value = 0
            invoke(this.cancel)
        else
            this.countDown = this.countDown - 1
            this.countdownLabel.text = System.String.Format(LocalString.GetString("{0}秒后下一题"), this.countDown)
            this.progressBar.value = this.countDown / CKeJuMgr.Inst.questionCountDown
        end
    end), 1000, ETickType.Loop)

    this.scoreLabel1.text = "0"
    this.scoreLabel2.text = "0"
    this.resultSprite1.gameObject:SetActive(false)
    this.resultSprite2.gameObject:SetActive(false)
    this:UpdateWenshiPlayerInfo()
end
CXialvPKWenshi.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.XiaLvPkSendWenShiScore, MakeDelegateFromCSFunction(this.UpdateWenshiScores, Action0, this))
    EventManager.AddListener(EnumEventType.UpdateWenshiAnswers, MakeDelegateFromCSFunction(this.UpdateWenshiAnswers, Action0, this))
    EventManager.AddListener(EnumEventType.ClearWenshiAnswers, MakeDelegateFromCSFunction(this.ClearAllPlayerAnswers, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerDestroyed, MakeDelegateFromCSFunction(this.OnMainPlayerDestroyed, Action0, this))
    EventManager.AddListener(EnumEventType.GasDisconnect, MakeDelegateFromCSFunction(this.OnMainPlayerDestroyed, Action0, this))
end
CXialvPKWenshi.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.XiaLvPkSendWenShiScore, MakeDelegateFromCSFunction(this.UpdateWenshiScores, Action0, this))
    EventManager.RemoveListener(EnumEventType.UpdateWenshiAnswers, MakeDelegateFromCSFunction(this.UpdateWenshiAnswers, Action0, this))
    EventManager.RemoveListener(EnumEventType.ClearWenshiAnswers, MakeDelegateFromCSFunction(this.ClearAllPlayerAnswers, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerDestroyed, MakeDelegateFromCSFunction(this.OnMainPlayerDestroyed, Action0, this))
    EventManager.RemoveListener(EnumEventType.GasDisconnect, MakeDelegateFromCSFunction(this.OnMainPlayerDestroyed, Action0, this))
end
CXialvPKWenshi.m_OnDestroy_CS2LuaHook = function (this) 
    if this.cancel ~= nil then
        invoke(this.cancel)
        this.cancel = nil
    end
end
CXialvPKWenshi.m_UpdateWenshiPlayerInfo_CS2LuaHook = function (this)
    if CXialvPKWenshi.PlayerIds.Count < CXialvPKWenshi.MAX_WENSHI_PLAYER then
        return
    end

    if CXialvPKWenshi.PlayerNames.Count < CXialvPKWenshi.MAX_WENSHI_PLAYER then
        return
    end

    local team1Score = 0
    local team2Score = 0

    do
        local i = 0
        while i < CXialvPKWenshi.MAX_WENSHI_PLAYER do
            local playerId = CXialvPKWenshi.PlayerIds[i]

            local answerIdx = 0
            if CommonDefs.DictContains(CXialvPKWenshi.WenshiAnswers, typeof(UInt64), playerId) then
                answerIdx = CommonDefs.DictGetValue(CXialvPKWenshi.WenshiAnswers, typeof(UInt64), playerId)
            end

            local score = 0
            if CommonDefs.DictContains(CXialvPKWenshi.WenshiScores, typeof(UInt64), playerId) then
                score = CommonDefs.DictGetValue(CXialvPKWenshi.WenshiScores, typeof(UInt64), playerId)
            end

            this.players[i]:Init(playerId, CXialvPKWenshi.PlayerNames[i], score, answerIdx)

            if i < 2 then
                team1Score = team1Score + score
            else
                team2Score = team2Score + score
            end
            i = i + 1
        end
    end

    this.scoreLabel1.text = tostring(team1Score)
    this.scoreLabel2.text = tostring(team2Score)

    -- 显示结果
    if CXialvPKWenshi.ShowResult then
        this.resultSprite1.gameObject:SetActive(true)
        this.resultSprite2.gameObject:SetActive(true)

        if team1Score > team2Score then
            this.resultSprite1.spriteName = Constants.WinSprite
            this.resultSprite2.spriteName = Constants.LoseSprite
        else
            this.resultSprite1.spriteName = Constants.LoseSprite
            this.resultSprite2.spriteName = Constants.WinSprite
        end
    end
end
