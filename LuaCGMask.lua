require("common/common_include")

local Time = import "UnityEngine.Time"
local Mathf = import "UnityEngine.Mathf"
local Color = import "UnityEngine.Color"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local LoginMgr = import "L10.Game.CLoginMgr"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CMainCamera = import "L10.Engine.CMainCamera"
local SoundManager = import "SoundManager"

LuaCGMask = class()

RegistClassMember(LuaCGMask, "m_Texture")
RegistClassMember(LuaCGMask, "m_TargetColor")
RegistClassMember(LuaCGMask, "m_Fading")
RegistClassMember(LuaCGMask, "m_AutoClose")

function LuaCGMask:Awake()
	self.m_Texture = self.transform:Find("Camera/Panel/Texture"):GetComponent(typeof(UITexture))
	self.m_Texture.color = Color(1, 1, 1, 0)
	self.m_TargetColor = Color(1, 1, 1, 0)
	self.m_Fading = false
	self.m_AutoClose = true
end

function LuaCGMask:OnEnable()
    g_ScriptEvent:AddListener("CharacterCreationCGStop", self, "OnCharacterCreationCGStop")
end

function LuaCGMask:OnDisable()
    g_ScriptEvent:RemoveListener("CharacterCreationCGStop", self, "OnCharacterCreationCGStop")
end

function LuaCGMask:OnCharacterCreationCGStop()
	self.m_Texture.color = Color(1, 1, 1, 1)
	self.m_TargetColor = Color(1, 1, 1, 0)
	self.m_Fading = true
	self.m_AutoClose = true

	if IsEnableChuangJue2023() and AMPlayer.s_CGName == LoginMgr.s_NewPlayerLoginVideoName then
		self.m_Texture.color = Color(0, 0, 0, 1)
		self.m_TargetColor = Color(0, 0, 0, 0)
	end
end

function LuaCGMask:Update()
	if self.m_Fading then
		local curColor = self.m_Texture.color
		local delta = math.abs(self.m_TargetColor.a - curColor.a)
		if delta > 0.001 then
			local a = math.max(math.min(Mathf.MoveTowards(curColor.a, self.m_TargetColor.a, 0.5 * Time.deltaTime), 1), 0)
			self.m_Texture.color = Color(curColor.r, curColor.g, curColor.b, a) 
		else
			self.m_Fading = false
			if self.m_AutoClose then
				CResourceMgr.Inst:Destroy(self.gameObject)
			end
		end
	end
end
