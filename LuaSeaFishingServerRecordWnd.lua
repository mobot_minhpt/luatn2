local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local TweenPosition = import "TweenPosition"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaSeaFishingServerRecordWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSeaFishingServerRecordWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaSeaFishingServerRecordWnd, "ZhanKai", "ZhanKai", GameObject)
RegistChildComponent(LuaSeaFishingServerRecordWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaSeaFishingServerRecordWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaSeaFishingServerRecordWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaSeaFishingServerRecordWnd, "NextPageBtn", "NextPageBtn", GameObject)
RegistChildComponent(LuaSeaFishingServerRecordWnd, "LastPageBtn", "LastPageBtn", GameObject)
RegistChildComponent(LuaSeaFishingServerRecordWnd, "PageNumber", "PageNumber", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaSeaFishingServerRecordWnd,"m_SelectTabIndex")
RegistClassMember(LuaSeaFishingServerRecordWnd,"m_SelectPage")
RegistClassMember(LuaSeaFishingServerRecordWnd,"m_SpecialFishList")
RegistClassMember(LuaSeaFishingServerRecordWnd,"m_NormalFishList")
RegistClassMember(LuaSeaFishingServerRecordWnd,"m_CountPerPage")
RegistClassMember(LuaSeaFishingServerRecordWnd,"m_PageCount")

function LuaSeaFishingServerRecordWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.NextPageBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextPageBtnClick()
	end)


	
	UIEventListener.Get(self.LastPageBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLastPageBtnClick()
	end)


    --@endregion EventBind end
	self.m_CountPerPage = 8
	self.m_SelectPage = 1
end

function LuaSeaFishingServerRecordWnd:Init()
	self.m_SpecialFishList = {}
	self.m_NormalFishList = {}

	LuaZhenZhaiYuMgr.m_FishId2ServerRecordList = {}
	__HouseFish_AllFishes_SubIndex_FishIdx.Foreach(function(key,data)
		local t = {}
		t.Idx = tonumber(key)
		t.TemplateId = data.KeyValue
		local fishdata = HouseFish_AllFishes.GetData(t.TemplateId)
		t.IsNormal = fishdata.IsZhenZhai == 0 and fishdata.IsGuanShang == 0
		t.Level = fishdata.Level
		local itemdata = Item_Item.GetData(t.TemplateId)
		t.FishName = itemdata.Name
		t.FishIcon = itemdata.Icon
		t.RecordData = LuaSeaFishingMgr.m_ServerRecordList[t.Idx]
		table.insert(LuaZhenZhaiYuMgr.m_FishId2ServerRecordList,t)
		if t.IsNormal then
			table.insert(self.m_NormalFishList,t)
		else
			table.insert(self.m_SpecialFishList,t)
		end
	end)

	local sort_func = function (a,b)
		return a.Level > b.Level
	end

	table.sort(self.m_NormalFishList,sort_func)
	table.sort(self.m_SpecialFishList,sort_func)

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
			if self.m_SelectTabIndex == 0 then
				return #self.m_SpecialFishList
			else
				return #self.m_NormalFishList
			end
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectTableView(row + 1)
    end)

	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self.m_SelectTabIndex = index
		self.m_SelectPage = 1
		if index == 0 then
			self.m_PageCount = #self.m_SpecialFishList / self.m_CountPerPage
		else
			self.m_PageCount = #self.m_NormalFishList / self.m_CountPerPage
		end
		self.m_PageCount = 	math.ceil(self.m_PageCount)	
		self.TableView:ReloadData(false,false)
		self:InitPageLabel()
    end)
	self.TabBar:ChangeTab(0, false)
end

function LuaSeaFishingServerRecordWnd:InitItem(item,row)
	local info
	if self.m_SelectTabIndex == 0 then
		info = self.m_SpecialFishList[row+1]
	else
		info = self.m_NormalFishList[row+1]
	end
	local active 
	local lindex = (self.m_SelectPage-1) * self.m_CountPerPage + 1
	local rindex = self.m_SelectPage * self.m_CountPerPage 
	if row+1 >= lindex and row+1 <= rindex then
		active = true
	else
		active = false
	end
	item.gameObject:SetActive(active)
	if not active then return end

	local fishIcon = item.transform:Find("Pao/FishIcon"):GetComponent(typeof(CUITexture))
	local fishName = item.transform:Find("FishName"):GetComponent(typeof(UILabel))
	local weight = item.transform:Find("Pao/Weight"):GetComponent(typeof(UILabel))
	local playerName = item.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
	local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
	local tween = item.transform:Find("Pao"):GetComponent(typeof(TweenPosition))
	tween.delay = (row%8)*0.02

	fishIcon:LoadMaterial(info.FishIcon)
	fishName.text = info.FishName

	local pos = fishIcon.transform.localPosition
	if info.RecordData then
		pos.z = 0
		tween.enabled = true
		weight.text = SafeStringFormat3(LocalString.GetString("%.2f两"),info.RecordData.Weight)
		tween.duration = 2.5 * ((info.RecordData.Weight - 15)/15*0.3 + 1)
		playerName.text = info.RecordData.Name
		local d = CServerTimeMgr.ConvertTimeStampToZone8Time(info.RecordData.Timestamp)
        local str = ToStringWrap(d, "yyyy-MM-dd")
		timeLabel.text = str
	else
		pos.z = -1
		playerName.text = LocalString.GetString("暂未钓起")
		timeLabel.text = nil
		weight.text = nil
		tween.enabled = false
	end
	fishIcon.transform.localPosition = pos
end

function LuaSeaFishingServerRecordWnd:OnSelectTableView(row)
end

function LuaSeaFishingServerRecordWnd:InitPageLabel()
	self.PageNumber.text = SafeStringFormat3("%d/%d",self.m_SelectPage,self.m_PageCount)
end
--@region UIEvent

function LuaSeaFishingServerRecordWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("SesFishing_ServerRecord_Tip")
end

function LuaSeaFishingServerRecordWnd:OnNextPageBtnClick()
	if self.m_SelectPage + 1 <= self.m_PageCount then
		self.m_SelectPage = self.m_SelectPage + 1
		self.TableView:ReloadData(false,false)
		self:InitPageLabel()
	end
end

function LuaSeaFishingServerRecordWnd:OnLastPageBtnClick()
	if self.m_SelectPage - 1 >= 1 then
		self.m_SelectPage = self.m_SelectPage - 1
		self.TableView:ReloadData(false,false)
		self:InitPageLabel()
	end
end


--@endregion UIEvent

