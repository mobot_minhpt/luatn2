local CScene = import "L10.Game.CScene"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CCommonItem = import "L10.Game.CCommonItem"

LuaHengYuDangKouMgr = {}
LuaHengYuDangKouMgr.m_StatInfo = {}
LuaHengYuDangKouMgr.m_SceneInfo = nil
LuaHengYuDangKouMgr.m_ClickId = 1 -- 玩家点击查看的怪物id
LuaHengYuDangKouMgr.m_KillCount = nil -- 击败小怪数
LuaHengYuDangKouMgr.m_MonsterInfo = nil -- 怪物信息
LuaHengYuDangKouMgr.m_IsMonsterInfoShow = false -- 怪物信息是否显示过了
LuaHengYuDangKouMgr.m_curHp = 0 -- boss当前血量
LuaHengYuDangKouMgr.m_fullHp = 0 -- boss总血量
LuaHengYuDangKouMgr.m_RankList = nil -- 排行榜信息
LuaHengYuDangKouMgr.m_ResultTbl = nil -- 结算信息
LuaHengYuDangKouMgr.m_GhostDict = {} -- 鬼装信息
LuaHengYuDangKouMgr.m_ShowCurBattle = true

-- 当前服务器是否开启横屿荡寇 帮会活动，拍卖的总开关 用于patch
function LuaHengYuDangKouMgr:IsHengYuDangKouEnable()
    return false
end

function LuaHengYuDangKouMgr:GetGamePlayId()
    return HengYuDangKou_Setting.GetData().GamePlayId
end

function LuaHengYuDangKouMgr:IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51103268 then
           return true
        end
    end
    return false
end

function LuaHengYuDangKouMgr:IsInEiltePlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == 51103268 and self.m_SceneInfo and self.m_SceneInfo.curDifficulty > 0 then
           return true
        end
    end
    return false
end

function LuaHengYuDangKouMgr:InitMonsterInfo()
    local MonsterInfo = {}
    HengYuDangKou_BossReward.ForeachKey(function(key)
        local info = HengYuDangKou_BossReward.GetData(key)
        local difficulty = info.Difficulty
        local progress = info.Progress
        if not MonsterInfo[difficulty] then
            MonsterInfo[difficulty] = {}
        end
        MonsterInfo[difficulty][progress] = {
            MonsterName = info.MonsterName,
            MonsterSIcon = info.MonsterSIcon,
            MonsterLIcon = info.MonsterLIcon,
            MonsterIntro = info.MonsterIntro,
            StageInfo = info.StageInfo,
            StageInfo2 = info.StageInfo2,
        }
    end)
    return MonsterInfo
end

--  curDifficulty 当前难度
-- 	lastResetDifficultyTs 上次修改难度的时间戳
-- 	stage 副本类型
-- 	progress 当前进度
-- 	difficulty={ 已经通通关的boss选择的难度
-- 		1: 1,
-- 		2: 3,
-- 		3: 2,
-- 	}
--  bossCountdownEndTs boss模式倒计时结束时间戳
--  bossCountdownDuration boss模式倒计时总时间
--  leftTime 限时小怪剩余时间
--  extraMonsterEndTime 限时小怪结束时间戳

--  leader 有权限的人的id
--  leaderName 有权限的玩家名称
--  leaderStage 阶段

--  curSelectTeamId 当前出战队伍
--  boss2Score boss2阶段分数
--  boss2UseShiQiCount boss2士气已使用次数
--  boss5Stage boss5所在阶段（1小怪 2boss）

--  applyForFightTeamIdList 申请列表
--  openSelectTeamWnd 是否打开界面

function LuaHengYuDangKouMgr:SyncHengYuDangKouSceneInfo(sycInfo_U)
    self.m_SceneInfo = g_MessagePack.unpack(sycInfo_U)

    if self.m_SceneInfo.lastResetDifficultyTs == nil then
        self.m_SceneInfo.lastResetDifficultyTs = 0
    end

    if self.m_SceneInfo.curSelectTeamId == nil then
        self.m_SceneInfo.curSelectTeamId = 0
    end

    -- 处理技能只能释放到相同区域的人
    CommonDefs.DictClear(CSkillMgr.s_EngineId2PartScene)
    if self.m_SceneInfo.mirrorEngineId then
        for _, id in ipairs(self.m_SceneInfo.mirrorEngineId) do
            CommonDefs.DictAdd(CSkillMgr.s_EngineId2PartScene, typeof(UInt32), id, typeof(UInt32), 1)
        end
    end 

    if self.m_SceneInfo.openSelectTeamWnd and self:IsInPlay() and not CUIManager.IsLoaded(CLuaUIResources.HengYuDangKouTeamSelectWnd) then
        self:OpenTeamSelectWnd()
    elseif self.m_SceneInfo.openSelectTeamWnd ~= nil and self.m_SceneInfo.openSelectTeamWnd == false then
        CUIManager.CloseUI(CLuaUIResources.HengYuDangKouTeamSelectWnd)
    end

    if self:IsInPlay() then
        LuaCommonGamePlayTaskViewMgr:OnUpdateTitleAndContent()
    end
    g_ScriptEvent:BroadcastInLua("SyncHengYuDangKouSceneInfo", self.m_SceneInfo)
end

-- 左侧任务栏标题
function LuaHengYuDangKouMgr:GetTaskViewTitle()
    if CScene.MainScene then return CScene.MainScene.SceneName end
    return nil
end

-- 左侧任务栏内容
function LuaHengYuDangKouMgr:GetTaskViewContent()
    self.m_MonsterInfo = self.m_MonsterInfo and self.m_MonsterInfo or self:InitMonsterInfo()
    if not self.m_SceneInfo or not self.m_MonsterInfo then return nil end
    local progress = self.m_SceneInfo.progress
    if not self.m_MonsterInfo[self.m_SceneInfo.stage][progress] then return nil end
    local infoindex = (progress == 5 and self.m_SceneInfo.boss5Stage) and self.m_SceneInfo.boss5Stage or 1
    local content = ""
    if infoindex == 1 then
        content = LocalString.GetString(self.m_MonsterInfo[self.m_SceneInfo.stage][progress].StageInfo)
    else
        content = LocalString.GetString(self.m_MonsterInfo[self.m_SceneInfo.stage][progress].StageInfo2)
    end
    if progress == 2 then
        local ShiQiNeed = HengYuDangKou_Setting.GetData().Boss2ShiQiNeed
        local score = self.m_SceneInfo.boss2Score and self.m_SceneInfo.boss2Score or 0
        local count = math.floor(score / ShiQiNeed)
        local usecount = self.m_SceneInfo.boss2UseShiQiCount and self.m_SceneInfo.boss2UseShiQiCount or 0
        return g_MessageMgr:Format(content, score, count, usecount)
    end
    return g_MessageMgr:Format(content)
end

-- 左侧任务栏按钮（战况）
function LuaHengYuDangKouMgr:OnStatusButtonClick()
    LuaBattleSituationMgr.OpenBattleSituationWnd(EnumSituationType.eHengYuDangKou)
end

-- 同步击败小怪数
-- Gas2Gac.SyncHengYuDangKouKillExtraMonster
function LuaHengYuDangKouMgr:SyncHengYuDangKouKillExtraMonster(killCount)
    self.m_KillCount = killCount
end

-- 同步boss血量
-- Gas2Gac.SyncHengYuDangKouBossHp
function LuaHengYuDangKouMgr:SyncHengYuDangKouBossHp(curHp, fullHp)
    self.m_curHp = curHp
    self.m_fullHp = fullHp
    g_ScriptEvent:BroadcastInLua("SyncHengYuDangKouBossHp", curHp, fullHp)
end

function LuaHengYuDangKouMgr:OpenTeamSelectWnd()
    if CTeamMgr.Inst:TeamExists() then
        CUIManager.ShowUI(CLuaUIResources.HengYuDangKouTeamSelectWnd)
    else
        g_MessageMgr:ShowMessage("HengYuDangKou_TeamSelect_NoTeam")
    end
end

function LuaHengYuDangKouMgr:SendHengYuDangKouStat(detailType, detail, playerCount, statTotal)
    print("SendHengYuDangKouStat", detailType, playerCount, statTotal)

    detail = g_MessagePack.unpack(detail)
    local detailInfo = {}
    for i= 1 , #detail, 4 do
        local playerId = detail[i]
        local playerName = detail[i+1]
        local playerClass = detail[i+2]
        local playerStat = detail[i+3]
        table.insert(detailInfo, {playerId = playerId, playerName = playerName, playerClass = playerClass, playerStat = playerStat})
        print("SendHengYuDangKouStat detail", playerId, playerName, playerClass, playerStat)
    end

    table.sort(detailInfo, function(a,b)
        if a.playerStat == b.playerStat then
            return a.playerId < b.playerId
        end
        return a.playerStat > b.playerStat
    end)
    local players = {}
    for i,v in ipairs(detailInfo) do
        players[i-1] = v
    end
    players.Count = #players
    self.m_StatInfo = {statType = detailType, playerCount = playerCount, statTotal = statTotal,players = players}
    g_ScriptEvent:BroadcastInLua("SendHengYuDangKouStat",detailType, detailInfo , playerCount, statTotal)
end

-- 排行榜数据
-- Gas2Gac.ReplyHengYuDangKouRankList
-- {guildId, guildName, monsterGrade, killCount, rankPercent}
function LuaHengYuDangKouMgr:ReplyHengYuDangKouRankList(replyInfo_U)
    self.m_RankList = g_MessagePack.unpack(replyInfo_U)
    if not self.m_RankList or #self.m_RankList < 1 then
        g_MessageMgr:ShowMessage("HengYuDangKou_QueryRankAddNoRank")
        return
    end
    if CUIManager.IsLoaded(CLuaUIResources.HengYuDangKouRankingListWnd) then
        g_ScriptEvent:BroadcastInLua("OnReplyHengYuDangKouRankList")
    else
        CUIManager.ShowUI(CLuaUIResources.HengYuDangKouRankingListWnd)
    end
end

-- 横屿荡寇结算数据
-- Gas2Gac.SyncHengYuDangKouOpenBox
-- {totalScore, keyNum, jGZNum, boxItemId2Count, ghostId2Count}
function LuaHengYuDangKouMgr:SyncHengYuDangKouOpenBox(result_U)
    self.m_ResultTbl = g_MessagePack.unpack(result_U)
    if not self.m_ResultTbl then return end
    CUIManager.ShowUI(CLuaUIResources.HengYuDangKouResultWnd)
end

-- 横屿荡寇鬼装数据
-- Gas2Gac.SendHengYuDangKouItemInfo
function LuaHengYuDangKouMgr:SendHengYuDangKouItemInfo(itemData)
    local item = CreateFromClass(CCommonItem, false, itemData)
    if item and item.TemplateId then
        self.m_GhostDict[item.TemplateId] = item
    end
end