-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CYuanDanConcertWnd = import "L10.UI.CYuanDanConcertWnd"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local Ease = import "DG.Tweening.Ease"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"
local EnumEventType = import "EnumEventType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EventManager = import "EventManager"
local PlayerSettings = import "L10.Game.PlayerSettings"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CYuanDanConcertWnd.m_Init_CS2LuaHook = function (this) 
    this.expandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
    this:UpdateDisplay()
    this:OnMainPlayerCreated()
    this:OnRightMenuChanged()
end
CYuanDanConcertWnd.m_UpdateSingerInfo_CS2LuaHook = function (this) 
    local state = CYuanDanMgr.Inst.CurrentConcertState
    if state.singerId > 0 then
        this.playerNameLabel.text = state.singerName
        this.portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(state.singerClass, state.singerGender, -1), false)
        this.flowerNumLabel.text = tostring(CYuanDanMgr.Inst.SingerFlowerNum)
    else
        this.playerNameLabel.text = nil
        this.portrait:Clear()
        this.flowerNumLabel.text = tostring(CYuanDanMgr.Inst.SingerFlowerNum)
    end
end
CYuanDanConcertWnd.m_UpdateDisplay_CS2LuaHook = function (this) 
    local state = CYuanDanMgr.Inst.CurrentConcertState
    if state.singerExsit then
        this.singerRoot:SetActive(true)
        if state.mainplayerIsSinger then
            --我是歌手
            this.audienceRoot:SetActive(false)
            this.speakerRoot:SetActive(true)
            this.snatchRoot:SetActive(false)
        else
            --我是听众
            this.audienceRoot:SetActive(true)
            this.speakerRoot:SetActive(false)
            this.snatchRoot:SetActive(false)
        end
    elseif state.sceneState == CYuanDanMgr.ENewYearConcertSceneState.QiangMai then
        --准备抢麦
        this.singerRoot:SetActive(false)
        this.audienceRoot:SetActive(false)
        this.speakerRoot:SetActive(false)
        this.snatchRoot:SetActive(true)
        local enableQiangMai = (state.qiangmaiState == CYuanDanMgr.ENewYearConcertQiangMaiState.QiangMai)
        this.snatchBtn.Enabled = enableQiangMai
        this.snatchBtnSpeakerIcon:SetActive(not enableQiangMai)
        this.snatchBeginIcon:SetActive(enableQiangMai)
        if enableQiangMai then
            this.snatchBeginIcon.transform.localScale = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, 1.5)
            CommonDefs.SetEase_Tweener(LuaTweenUtils.DOScale(this.snatchBeginIcon.transform, Vector3.one, 0.5), Ease.InCubic)
            this.snatchBeginFx:LoadFx(CUIFxPaths.YuanDanQiangMaiBeginFx)
            this.snatchBeginFx.gameObject:SetActive(not PlayerSettings.RightMenuExpanded)
        else
            this.snatchBeginFx:DestroyFx()
        end
    else
        this.singerRoot:SetActive(false)
        this.audienceRoot:SetActive(false)
        this.speakerRoot:SetActive(false)
        this.snatchRoot:SetActive(false)
        this.snatchBeginFx:DestroyFx()
    end
    this:UpdateSingerInfo()
    this:UpdateListenOrCaptureInfo()

    this:UpdateDanMuWnd()
end
CYuanDanConcertWnd.m_UpdateDanMuWnd_CS2LuaHook = function (this) 
    local state = CYuanDanMgr.Inst.CurrentConcertState

    if state.sceneState ~= CYuanDanMgr.ENewYearConcertSceneState.Idle and state.sceneState ~= CYuanDanMgr.ENewYearConcertSceneState.Undefined then
        CUIManager.ShowUI(CUIResources.DanmuWnd)
        g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",true)
    else
        if CUIManager.IsLoaded(CUIResources.DanmuWnd) or CUIManager.IsLoading(CUIResources.DanmuWnd) then
            CUIManager.CloseUI(CUIResources.DanmuWnd)
        end
    end
end
CYuanDanConcertWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.portrait.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.portrait.gameObject).onClick, MakeDelegateFromCSFunction(this.OnPortraitClick, VoidDelegate, this), true)
    UIEventListener.Get(this.expandBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.expandBtn).onClick, MakeDelegateFromCSFunction(this.OnExpandButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.listenBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.listenBtn).onClick, MakeDelegateFromCSFunction(this.OnListenButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.sendFlowerBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.sendFlowerBtn).onClick, MakeDelegateFromCSFunction(this.OnSendFlowerButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.captureBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.captureBtn).onClick, MakeDelegateFromCSFunction(this.OnCaptureButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.stopCaptureBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.stopCaptureBtn).onClick, MakeDelegateFromCSFunction(this.OnStopCaptureButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.snatchBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.snatchBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnSnatchButtonClick, VoidDelegate, this), true)
end
CYuanDanConcertWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
    EventManager.AddListener(EnumEventType.YuanDanConcertSceneStateUpdate, MakeDelegateFromCSFunction(this.OnYuanDanConcertSceneStateUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.YuanDanSingerFlowerInfoUpdate, MakeDelegateFromCSFunction(this.OnYuanDanSingerFlowerInfoUpdate, Action0, this))

    EventManager.AddListenerInternal(EnumEventType.OnJoinCCStream, MakeDelegateFromCSFunction(this.OnJoinCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.AddListenerInternal(EnumEventType.OnQuitCCStream, MakeDelegateFromCSFunction(this.OnQuitCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.AddListener(EnumEventType.OnCCCaptureStatusUpdate, MakeDelegateFromCSFunction(this.OnCCCaptureStatusUpdate, Action0, this))

    EventManager.AddListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
    EventManager.AddListener(EnumEventType.OnRightMenuWndExpandedStatusChanged, MakeDelegateFromCSFunction(this.OnRightMenuChanged, Action0, this))
end
CYuanDanConcertWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
    EventManager.RemoveListener(EnumEventType.YuanDanConcertSceneStateUpdate, MakeDelegateFromCSFunction(this.OnYuanDanConcertSceneStateUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.YuanDanSingerFlowerInfoUpdate, MakeDelegateFromCSFunction(this.OnYuanDanSingerFlowerInfoUpdate, Action0, this))

    EventManager.RemoveListenerInternal(EnumEventType.OnJoinCCStream, MakeDelegateFromCSFunction(this.OnJoinCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnQuitCCStream, MakeDelegateFromCSFunction(this.OnQuitCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.RemoveListener(EnumEventType.OnCCCaptureStatusUpdate, MakeDelegateFromCSFunction(this.OnCCCaptureStatusUpdate, Action0, this))

    EventManager.RemoveListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayerCreated, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnRightMenuWndExpandedStatusChanged, MakeDelegateFromCSFunction(this.OnRightMenuChanged, Action0, this))
end
CYuanDanConcertWnd.m_OnPortraitClick_CS2LuaHook = function (this, go) 
    local state = CYuanDanMgr.Inst.CurrentConcertState
    if state.singerExsit then
        CPlayerInfoMgr.ShowPlayerPopupMenu(state.singerId, EnumPlayerInfoContext.CurrentTarget, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end
end
CYuanDanConcertWnd.m_UpdateListenOrCaptureInfo_CS2LuaHook = function (this) 
    this:UpdateListenIcon()
    this:UpdateSpeakerIcon()
    this.captureBtn:SetActive(not CCMiniAPIMgr.Inst:IsInNewYearConcertStream() or not CCMiniAPIMgr.Inst:IsCapturing())
    this.stopCaptureBtn:SetActive(not this.captureBtn.activeSelf)
end
CYuanDanConcertWnd.m_OnMainPlayerCreated_CS2LuaHook = function (this) 
    -- if (!CUIManager.IsLoaded(CUIResources.DanmuWnd))
    --     CUIManager.ShowUI(CUIResources.DanmuWnd);

    this:UpdateDanMuWnd()
end
CYuanDanConcertWnd.m_UpdateSpeakerIcon_CS2LuaHook = function (this) 
    local isListening = CCMiniAPIMgr.Inst:IsInNewYearConcertStream()
    if CYuanDanMgr.Inst.CurrentConcertState.singerExsit and CClientMainPlayer.Inst ~= nil and CYuanDanMgr.Inst.CurrentConcertState.singerId == CClientMainPlayer.Inst.Id then
        this.speakerIcon:SetActive(isListening and CCMiniAPIMgr.Inst:IsCapturing())
    else
        this.speakerIcon:SetActive(isListening)
    end
end
