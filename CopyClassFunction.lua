--
-- $Id: CopyClassFunction.lua 121053 2013-04-08 07:09:59Z cjfan $
--

local function GetClassesTable()
	return __AllClasses
end

local function CopyBaseClassesFunctionToTable(Class, ClassTable)
	for i = #Class.__base_list, 1, -1 do
		local Base = Class.__base_list[i]
		CopyBaseClassesFunctionToTable(Base, ClassTable)
	end

	for k, v in pairs(Class) do
		if k ~= "Ctor" then		--Ctor不拷
			rawset(ClassTable, k, v)
		end
	end
end

local function CopyBaseClassesFunction(Class)
	local TempClass = {}
	CopyBaseClassesFunctionToTable(Class, TempClass)

	for k, v in pairs(TempClass) do
		if not rawget(Class, k) then
			rawset(Class, k, v)
		end
	end
end

local function CopyClassesFunction(ClassesTable)
	for k, v in pairs(ClassesTable) do
		CopyBaseClassesFunction(v)
		local mt = getmetatable(v)
		if mt then
			setmetatable(v, nil)
		end
	end
end

local function CopyBaseClassesDeclaredToTable(Class, ClassDeclardTable)
	for i = #Class.__base_list, 1, -1 do
		local Base = Class.__base_list[i]
		CopyBaseClassesDeclaredToTable(Base, ClassDeclardTable)
	end

	for k, v in pairs(Class.Declared) do
		rawset(ClassDeclardTable, k, v)
	end
end

local function CopyBaseClassesDeclared(Class)
	local TempClassDeclard = {}
	CopyBaseClassesDeclaredToTable(Class, TempClassDeclard)

	for k, v in pairs(TempClassDeclard) do
		Class.Declared[k] = v
	end
end

local function CopyClassesDeclared(ClassesTable)
	for k, v in pairs(ClassesTable) do
		CopyBaseClassesDeclared(v)
	end
end

function GetClassName(Class)
	for k, v in pairs(_G) do
		if v == Class then
			return k
		end
	end
	return "LocalClass"
end

local function PrintClassDeclard(Class)
	local ClassName = GetClassName(Class)
	local TempObj = {}


	setmetatable(TempObj, { __newindex  = function(tbl, key, value)
			rawset(tbl, key, value)
			if key == ClassName .. "_hObject" then
				return
			end
			for i = 1, #Class.__base_list do
				local Base = Class.__base_list[i]
				if key == GetClassName(Base) .. "_hObject" then
					return
				end
			end
			if Class.Declared[key] then
				return
			end
			if type(key) == "number" then
				return
			end
		end }
	)
	if type(Class.Ctor) ~= "function" then

		return
	end

	rawset(TempObj, "CreateFromRes", function() end)

	if ClassName == "CSerializableArray" then
	--RegistClassMember( , "", "")
	else
		Class.Ctor(TempObj)
	end
end

local function PrintClassesDeclard(ClassesTable)
	for k, v in pairs(ClassesTable) do
		PrintClassDeclard(v)
	end
	os.execute("pause")
end



function DeepCopyAllClasses()
	local ClassesTable = GetClassesTable()
	for k, v in pairs(_G) do
		if type(k) == "string" and type(v) == "table" and rawget(v,"__base_list") then
			rawset(v,"Name",k)
		elseif type(k)=="string" and type(v)=="userdata" and (not string.find(k, "Class%d+")) then
			local mt = getmetatable(v)
			local f = mt and mt.__index
			if f and type(f)=="function" then
				--todo userdata检查成员是否存在 看下应该怎么判断
				if rawget(getmetatable(v),"Name") then
					v.Name = k
				end
			end
		end
	end
	CopyClassesDeclared(ClassesTable)

	-- 统计所有类declared的大小
	for k,v in pairs(ClassesTable) do
		v.DeclaredCount = 0
		for kk,vv in pairs(v.Declared) do
			v.DeclaredCount = v.DeclaredCount + 1
		end
	end
	
	--PrintClassesDeclard(ClassesTable)
	
	CopyClassesFunction(ClassesTable)
end
