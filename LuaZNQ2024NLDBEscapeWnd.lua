local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaZNQ2024NLDBEscapeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZNQ2024NLDBEscapeWnd, "PickUpBtn", "PickUpBtn", GameObject)
RegistChildComponent(LuaZNQ2024NLDBEscapeWnd, "ButtonLabel", "ButtonLabel", GameObject)

--@endregion RegistChildComponent end

function LuaZNQ2024NLDBEscapeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaZNQ2024NLDBEscapeWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

