require("common/common_include")

local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local BabyMgr = import "L10.Game.BabyMgr"
local CChatListBaseItem = import "L10.UI.CChatListBaseItem"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"


LuaBabyChatHistoryWnd = class()

RegistClassMember(LuaBabyChatHistoryWnd, "m_CloseButton")
RegistClassMember(LuaBabyChatHistoryWnd, "m_TitleLabel")
RegistClassMember(LuaBabyChatHistoryWnd, "m_SimpleTableView")
RegistClassMember(LuaBabyChatHistoryWnd, "m_DefaultSimpleTableViewDataSource")

RegistClassMember(LuaBabyChatHistoryWnd, "m_OppositeTemplate")
RegistClassMember(LuaBabyChatHistoryWnd, "m_SelfTempalte")
RegistClassMember(LuaBabyChatHistoryWnd, "m_TimesplitTemplate")

RegistClassMember(LuaBabyChatHistoryWnd, "m_AllData")

function LuaBabyChatHistoryWnd:Init()

	self.m_CloseButton = self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
	self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_3/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_SimpleTableView = self.transform:Find("Chat"):GetComponent(typeof(UISimpleTableView))
	self.m_OppositeTemplate = self.transform:Find("Chat/Templates/OppsiteItem").gameObject
	self.m_SelfTempalte = self.transform:Find("Chat/Templates/SelfItem").gameObject
	self.m_TimesplitTemplate = self.transform:Find("Chat/Templates/TimeSplitItem").gameObject


	if not self.m_DefaultSimpleTableViewDataSource then

		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return self:NumberOfRows()
		end,
		function ( index )
			return self:CellForRowAtIndex(index)
		end)
	end

	self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("%s 的聊天记录"), self:GetBabyName())

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_AllData = BabyMgr.Inst:GetMessageDataList(LuaBabyMgr.CurrentChatBabyId or "")
	self.m_SimpleTableView:Clear()
    self.m_SimpleTableView.dataSource = self.m_DefaultSimpleTableViewDataSource
	self.m_SimpleTableView:LoadDataFromTail()
end

function LuaBabyChatHistoryWnd:GetBabyName()
	local name = ""
	if LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromBabyWnd then
		local baby = BabyMgr.Inst:GetBabyById(LuaBabyMgr.CurrentChatBabyId or "")
		name = baby and baby.Name or ""
	elseif LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromNPC then
		local npc = CClientObjectMgr.Inst:GetObject(LuaBabyMgr.CurrentChatBabyEngineId or 0)
		name = npc and npc.Name or ""
	end
	return name
end

function LuaBabyChatHistoryWnd:NumberOfRows()
	return self.m_AllData.Count
end

function LuaBabyChatHistoryWnd:CellForRowAtIndex(index)
	if index < 0 and index >= self.m_AllData.Count then
        return nil
    end
    local cellIdentifier = nil
    local template = nil
    local data = self.m_AllData[index]
    if data.usedForDisplayTime then
        cellIdentifier = "TimeSplitCell"
        template = self.m_TimesplitTemplate
    elseif data.isOpposite then
        cellIdentifier = "OppositeTemplate"
        template = self.m_OppositeTemplate
    else
        cellIdentifier = "SelfTemplate"
        template = self.m_SelfTempalte
    end

    local cell = self.m_SimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.m_SimpleTableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end

    local chatListItem = cell:GetComponent(typeof(CChatListBaseItem))
    chatListItem:Init(data)
    chatListItem.onReLayoutDelegate = nil

    return cell
end

function LuaBabyChatHistoryWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaBabyChatHistoryWnd:OnEnable()
	
end

function LuaBabyChatHistoryWnd:OnDisable()

end
