local Time = import "UnityEngine.Time"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CUIPickerWndMgr = import "L10.UI.CUIPickerWndMgr"
local Color = import "UnityEngine.Color"
local CItemMgr = import "L10.Game.CItemMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local BoxCollider = import "UnityEngine.BoxCollider"
local CJewelTemplate=import "L10.UI.CJewelTemplate"


CLuaGemUpdateDetailView = class()
RegistClassMember(CLuaGemUpdateDetailView,"curLevelName")
RegistClassMember(CLuaGemUpdateDetailView,"curLevelDesc")
RegistClassMember(CLuaGemUpdateDetailView,"nextLevelName")
RegistClassMember(CLuaGemUpdateDetailView,"nextLevelDesc")
RegistClassMember(CLuaGemUpdateDetailView,"needPlayerLevel")
RegistClassMember(CLuaGemUpdateDetailView,"updateBtn")
RegistClassMember(CLuaGemUpdateDetailView,"gemButton")
RegistClassMember(CLuaGemUpdateDetailView,"gemAdditionalButton")
RegistClassMember(CLuaGemUpdateDetailView,"m_GemButtonTable")
RegistClassMember(CLuaGemUpdateDetailView,"m_MaxLevelTip")
RegistClassMember(CLuaGemUpdateDetailView,"jewel")
RegistClassMember(CLuaGemUpdateDetailView,"baseJewelId")
RegistClassMember(CLuaGemUpdateDetailView,"nextLevelJewel")
RegistClassMember(CLuaGemUpdateDetailView,"holeInfo")
RegistClassMember(CLuaGemUpdateDetailView,"maxLevel")
RegistClassMember(CLuaGemUpdateDetailView,"btnClickTime")

RegistClassMember(CLuaGemUpdateDetailView,"m_DesignCache")


function CLuaGemUpdateDetailView:Awake()
    self.curLevelName = self.transform:Find("CurrentLevel/Label"):GetComponent(typeof(UILabel))
    self.curLevelDesc = self.transform:Find("CurrentLevel/Description/Label"):GetComponent(typeof(UILabel))
    self.nextLevelName = self.transform:Find("NextLevel/Title/Aim"):GetComponent(typeof(UILabel))
    self.nextLevelDesc = self.transform:Find("NextLevel/Description/Label"):GetComponent(typeof(UILabel))
    self.needPlayerLevel = self.transform:Find("NextLevel/Title/GradeCheck"):GetComponent(typeof(UILabel))
    self.updateBtn = self.transform:Find("UpdateBtn").gameObject
    self.gemButton = self.transform:Find("Table/stoneTemplate"):GetComponent(typeof(CJewelTemplate))
    self.gemAdditionalButton = self.transform:Find("Table/stoneAddtionalTemplate"):GetComponent(typeof(CJewelTemplate))
    self.m_GemButtonTable = self.transform:Find("Table"):GetComponent(typeof(UITable))
    self.m_MaxLevelTip = self.transform:Find("maxLevelTip"):GetComponent(typeof(UILabel))
    self.jewel = nil
    self.baseJewelId = 0
    self.nextLevelJewel = nil
    self.holeInfo = nil
    self.maxLevel = 0
    self.btnClickTime = 0
    UIEventListener.Get(self.updateBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnUpdateBtnClicked(go)
    end)

    self.m_DesignCache = {}
    Jewel_Jewel.Foreach(function (Id, data) 
        local jewelKind = data.JewelKind
        if not self.m_DesignCache[jewelKind] then
            self.m_DesignCache[jewelKind] = {}
        end
        table.insert( self.m_DesignCache[jewelKind],Id )
    end)
end

function CLuaGemUpdateDetailView:Init( stoneTemplate) --CEquipmentHoleInfo

    self.holeInfo = stoneTemplate

    local stoneItemId = stoneTemplate.stoneItemId
    if stoneItemId>0 and stoneItemId<100 then
        local commonItem = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
        stoneItemId = commonItem.Equip.HoleItems[stoneItemId]
    end
    self.jewel = Jewel_Jewel.GetData(stoneItemId)


    self.curLevelName.text = self.jewel.JewelName
    self.curLevelDesc.text = self:GetJewelWordContext(stoneItemId, CLuaEquipMgr.m_GemUpdateSelectedEquipment)
    self.updateBtn:GetComponent(typeof(CButton)).Enabled = true

    self.baseJewelId = 0
    local nextLevelJewelId = 0

    local jewelKind = self.jewel.JewelKind
    
    for i,v in ipairs(self.m_DesignCache[jewelKind]) do
        local data = Jewel_Jewel.GetData(v)
        if data.JewelKind == self.jewel.JewelKind then
            self.maxLevel = math.max(data.JewelLevel, self.maxLevel)

            if self.baseJewelId == 0 and data.JewelLevel == 1 then
                self.baseJewelId = data.JewelID
            end
            if nextLevelJewelId == 0 and data.JewelLevel == self.jewel.JewelLevel + 1 then
                nextLevelJewelId = data.JewelID
            end
        end
    end

    if self.jewel.JewelLevel >= self.maxLevel then
        self.updateBtn:GetComponent(typeof(CButton)).Enabled = false
        self.nextLevelName.text = LocalString.GetString("无")
        self.needPlayerLevel.gameObject:SetActive(false)
        self.nextLevelDesc.text = LocalString.GetString("")
        self.nextLevelName:GetComponent(typeof(BoxCollider)).enabled = false

        self.gemButton:Init(self.baseJewelId, - 1)
        self.gemAdditionalButton.gameObject:SetActive(false)
        self.updateBtn.gameObject:SetActive(false)
        if self.m_MaxLevelTip ~= nil then
            self.m_MaxLevelTip.gameObject:SetActive(true)
            self.m_MaxLevelTip.text = LocalString.GetString("已达到最高等级")
        end
    else
        if self.m_MaxLevelTip ~= nil then
            self.m_MaxLevelTip.gameObject:SetActive(false)
        end
        self.updateBtn.gameObject:SetActive(true)
        CommonDefs.GetComponent_Component_Type(self.nextLevelName, typeof(BoxCollider)).enabled = true
        local nextStone = CItemMgr.Inst:GetItemTemplate(nextLevelJewelId)
        local nextJewel = Jewel_Jewel.GetData(nextLevelJewelId)
        self.nextLevelName.text = nextJewel.JewelName

        UIEventListener.Get(self.nextLevelName.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnAimLevelClick(go)
        end)

        self.needPlayerLevel.gameObject:SetActive(true)
        local adjustedGradeCheck = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(nextStone)
        self.needPlayerLevel.text = SafeStringFormat3(LocalString.GetString("%d级"), adjustedGradeCheck)
        self.needPlayerLevel.color = CClientMainPlayer.Inst.Level >= adjustedGradeCheck and Color.white or Color.red

        self.nextLevelJewel = Jewel_Jewel.GetData(nextLevelJewelId)
        self.nextLevelDesc.text = self:GetJewelWordContext(nextLevelJewelId, CLuaEquipMgr.m_GemUpdateSelectedEquipment)
        self.gemButton:Init(self.baseJewelId, self.nextLevelJewel.UpgradeNeedStone)
        if self.nextLevelJewel.UpgradeNeedOtherStone ~= nil and self.nextLevelJewel.UpgradeNeedOtherStone.Length >= 2 then
            local additionalJewelId = self.nextLevelJewel.UpgradeNeedOtherStone[0]
            local count = self.nextLevelJewel.UpgradeNeedOtherStone[1]
            self.gemAdditionalButton.gameObject:SetActive(true)
            self.gemAdditionalButton:Init(additionalJewelId, count)
        else
            self.gemAdditionalButton.gameObject:SetActive(false)
        end
    end
    self.m_GemButtonTable:Reposition()
end

function CLuaGemUpdateDetailView:UpdateNextJewel( )
    local nextStone = CItemMgr.Inst:GetItemTemplate(self.nextLevelJewel.JewelID)
    self.nextLevelName.text = self.nextLevelJewel.JewelName
    self.needPlayerLevel.gameObject:SetActive(true)
    local adjustedGradeCheck = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(nextStone)
    self.needPlayerLevel.text = SafeStringFormat3(LocalString.GetString("%d级"), adjustedGradeCheck)
    self.needPlayerLevel.color = CClientMainPlayer.Inst.Level >= adjustedGradeCheck and Color.white or Color.red

    local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(self.baseJewelId)
    local itemCountInBag = bindItemCountInBag + notBindItemCountInBag
    self.nextLevelJewel = Jewel_Jewel.GetData(self.nextLevelJewel.JewelID)
    self.nextLevelDesc.text = self:GetJewelWordContext(self.nextLevelJewel.JewelID, CLuaEquipMgr.m_GemUpdateSelectedEquipment)
    local needJewelCount = 0
    --是否需要额外的宝石
    local hasAdditional = false
    local needAddtionalJewelCount = 0
    local additionalJewelId = 0

    for i=self.jewel.JewelID + 1,self.nextLevelJewel.JewelID do
        local j = Jewel_Jewel.GetData(i)
        if j ~= nil then
            needJewelCount = needJewelCount + j.UpgradeNeedStone
            if j.UpgradeNeedOtherStone ~= nil and j.UpgradeNeedOtherStone.Length >= 2 then
                hasAdditional = true
                additionalJewelId = j.UpgradeNeedOtherStone[0]
                needAddtionalJewelCount = needAddtionalJewelCount + j.UpgradeNeedOtherStone[1]
            end
        end
    end


    self.gemButton:Init(self.baseJewelId, needJewelCount)
    if hasAdditional then
        self.gemAdditionalButton.gameObject:SetActive(true)
        self.gemAdditionalButton:Init(additionalJewelId, needAddtionalJewelCount)
    else
        self.gemAdditionalButton.gameObject:SetActive(false)
    end
    self.m_GemButtonTable:Reposition()
end
function CLuaGemUpdateDetailView:GetJewelWordContext( stoneItemId, equip) 

    local jewel = Jewel_Jewel.GetData(stoneItemId)
    local wordId = 0
    if equip.Equip.IsWeapon then
        wordId = jewel.onWeapon
    elseif equip.Equip.IsArmor then
        wordId = jewel.onArmor
    elseif equip.Equip.IsJewelry then
        wordId = jewel.onJewelry
    elseif equip.Equip.IsFaBao then
        wordId = jewel.onFaBao
    end

    if wordId > 0 then
        local word = Word_Word.GetData(wordId)
        return word.Description
    end

    return nil
end
function CLuaGemUpdateDetailView:OnEnable( )
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:AddListener("OnPickerItemSelected",self,"OnPickerWndClosed")
    g_ScriptEvent:AddListener("RequestCanUpgradeEquipStoneResult",self,"OnRequestCanUpgradeEquipStoneResult")
end
function CLuaGemUpdateDetailView:OnDisable( )
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:RemoveListener("OnPickerItemSelected",self,"OnPickerWndClosed")
    g_ScriptEvent:RemoveListener("RequestCanUpgradeEquipStoneResult",self,"OnRequestCanUpgradeEquipStoneResult")
end
function CLuaGemUpdateDetailView:OnUpdateBtnClicked( go) 
    local holeInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
    local info = CEquipmentProcessMgr.Inst.SelectEquipment
    if info == nil or holeInfo == nil then
        return
    end
    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(info) then
        return
    end
    if Time.realtimeSinceStartup - self.btnClickTime > 0.5 then
        self:TryUpgradeGem()
        self.btnClickTime = Time.realtimeSinceStartup
    end
end
function CLuaGemUpdateDetailView:OnRequestCanUpgradeEquipStoneResult(args) 
    local can, equipId, equipNewGrade, setIndex =args[0],args[1],args[2],args[3]

    if not can then
        return
    end

    if CClientMainPlayer.Inst == nil then
        return
    end

    local playerLevel = CClientMainPlayer.Inst.FinalMaxLevelForEquip
    local equipItem = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
    local level = equipItem.Equip.Grade

    if equipNewGrade > level then
        if equipNewGrade > playerLevel then
            local message = g_MessageMgr:FormatMessage("JEWEL_UPGRADE_CONFIRM_LEVEL_OVER", equipItem.Equip.ColoredDisplayName, equipNewGrade)
            --
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function() self:SendUpgradeGem() end), nil, nil, nil, false)
        else
            local message = g_MessageMgr:FormatMessage("JEWEL_UPGRADE_CONFIRM", equipItem.Equip.ColoredDisplayName, equipNewGrade)
            --
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function() self:SendUpgradeGem() end), nil, nil, nil, false)
        end
    else
        self:SendUpgradeGem()
    end
end
function CLuaGemUpdateDetailView:SendUpgradeGem( )

    local holeInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
    local info = CEquipmentProcessMgr.Inst.SelectEquipment
    --最后一个参数 指定升级到N级 传0代表升级到下一级
    Gac2Gas.RequestUpgradeEquipStone(
        EnumToInt(info.place), 
        info.pos, 
        info.itemId, 
        holeInfo.holeIndex, 
        self.nextLevelJewel.JewelLevel, 
        CLuaGemUpgradeWnd.m_HoleSetIndex)
end
function CLuaGemUpdateDetailView:TryUpgradeGem( )
    local holeInfo = CLuaEquipMgr.SelectedEquipmentHoleInfo
    local info = CEquipmentProcessMgr.Inst.SelectEquipment
    if info == nil or holeInfo == nil then
        return
    end
    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(info) then
        return
    end
    --最后一个参数 指定升级到N级 传0代表升级到下一级
    Gac2Gas.RequestCanUpgradeEquipStone(
        EnumToInt(info.place), 
        info.pos, 
        info.itemId, 
        holeInfo.holeIndex, 
        self.nextLevelJewel.JewelLevel, 
        CLuaGemUpgradeWnd.m_HoleSetIndex)
end


function CLuaGemUpdateDetailView:OnAimLevelClick(go)
    local contents = {}
    for i=1,10 do
        local nextJewel = Jewel_Jewel.GetData(self.jewel.JewelID + i)
        if nextJewel and nextJewel.JewelKind == self.jewel.JewelKind then
            table.insert( contents, nextJewel.JewelName )
        end
    end

    local index = self.nextLevelJewel.JewelID - self.jewel.JewelID - 1
    local localPos = go.transform.localPosition
    localPos.y =localPos.y- go:GetComponent(typeof(UILabel)).height * 0.5
    local anchor = go.transform.parent:TransformPoint(localPos)

    CUIPickerWndMgr.ShowPickerWnd(Table2Class(contents,MakeGenericClass(List,cs_string)), anchor, index)
end

function CLuaGemUpdateDetailView:OnPickerWndClosed(args)
    local index = args[0]
    self.nextLevelJewel = Jewel_Jewel.GetData(self.jewel.JewelID + index + 1)
    self:UpdateNextJewel()
end

function CLuaGemUpdateDetailView:OnSendItem(args)
    local itemId = args[0]
    self:Init(self.holeInfo)
end

function CLuaGemUpdateDetailView:OnSetItemAt(args)
    CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
    self:Init(self.holeInfo)
end
