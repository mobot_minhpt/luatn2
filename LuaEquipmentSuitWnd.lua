local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipmentIntensifyMgr = import "L10.Game.CEquipmentIntensifyMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumItemPlaceSize = import "L10.Game.EnumItemPlaceSize"
local LocalString = import "LocalString"
local NGUIMath = import "NGUIMath"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UITable = import "UITable"
local Vector3 = import "UnityEngine.Vector3"

CLuaEquipmentSuitWnd = class()
RegistClassMember(CLuaEquipmentSuitWnd,"parent")
RegistClassMember(CLuaEquipmentSuitWnd,"table")
RegistClassMember(CLuaEquipmentSuitWnd,"curTitleLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"curAdditionLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"curDescLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"curScoreLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"nextScoreLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"seperateSprite")
RegistClassMember(CLuaEquipmentSuitWnd,"nextTitleLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"nextAdditionLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"nextDescLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"backgroundSprite")
RegistClassMember(CLuaEquipmentSuitWnd,"perfectionIconArray1")
RegistClassMember(CLuaEquipmentSuitWnd,"perfectionIconArray2")
RegistClassMember(CLuaEquipmentSuitWnd,"advanceLabel")
RegistClassMember(CLuaEquipmentSuitWnd,"nextAdvanceLabel")

RegistClassMember(CLuaEquipmentSuitWnd,"find")
RegistClassMember(CLuaEquipmentSuitWnd,"nextFind")

function CLuaEquipmentSuitWnd:Awake()
	self.parent = self.transform:Find("Parent")
	self.table = self.parent:GetComponent(typeof(UITable))
	self.curTitleLabel = self.parent:Find("CurTitleLabel"):GetComponent(typeof(UILabel))
	self.curAdditionLabel = self.parent:Find("CurAdditionLabel"):GetComponent(typeof(UILabel))
	self.curDescLabel = self.parent:Find("CurDescLabel"):GetComponent(typeof(UILabel))
	self.curScoreLabel = self.parent:Find("CurScoreLabel"):GetComponent(typeof(UILabel))
	self.nextScoreLabel = self.parent:Find("NextScoreLabel"):GetComponent(typeof(UILabel))
	self.seperateSprite = self.parent:Find("SeperateSprite"):GetComponent(typeof(UISprite))
	self.nextTitleLabel = self.parent:Find("NextTitleLabel"):GetComponent(typeof(UILabel))
	self.nextAdditionLabel = self.parent:Find("NextAdditionLabel"):GetComponent(typeof(UILabel))
	self.nextDescLabel = self.parent:Find("NextDescLabel"):GetComponent(typeof(UILabel))

	self.backgroundSprite = self.transform:Find("BackgroundSprite"):GetComponent(typeof(UISprite))

	self.advanceLabel = self.parent:Find("AdvanceLabel"):GetComponent(typeof(UILabel))
	self.nextAdvanceLabel = self.parent:Find("NextAdvanceLabel"):GetComponent(typeof(UILabel))

	self.perfectionIconArray1 = CreateFromClass(MakeArrayClass(UISprite), 5)
	for i = 0, self.perfectionIconArray1.Length - 1 do
		self.perfectionIconArray1[i] = self.curDescLabel.transform:Find(tostring(i+1)):GetComponent(typeof(UISprite))
	end
	self.perfectionIconArray2 = CreateFromClass(MakeArrayClass(UISprite), 5)
	for i = 0, self.perfectionIconArray2.Length - 1 do
		self.perfectionIconArray2[i] = self.nextDescLabel.transform:Find(tostring(i+1)):GetComponent(typeof(UISprite))
	end
end

function CLuaEquipmentSuitWnd:Init()
	if CLuaEquipmentSuitInfoMgr.IsShowMainPlayerSuit then
		if CClientMainPlayer.Inst ~= nil then
			self:OnUpdateData(CClientMainPlayer.Inst)
		else
			self:UpdateData(0, 0)
		end
	else
        self:UpdateData(CLuaEquipmentSuitInfoMgr.IntesifySuitId, CLuaEquipmentSuitInfoMgr.IntesifySuitFxIndex)
	end
end

function CLuaEquipmentSuitWnd:OnEnable()
	g_ScriptEvent:AddListener("EquipmentSuitUpdate", self, "OnUpdateData")
end

function CLuaEquipmentSuitWnd:OnDisable()
	g_ScriptEvent:RemoveListener("EquipmentSuitUpdate", self, "OnUpdateData")
end

function CLuaEquipmentSuitWnd:OnUpdateData(obj)
	if not CLuaEquipmentSuitInfoMgr.IsShowMainPlayerSuit then
		return
	end

    if obj == nil or not (TypeIs(obj, typeof(CClientMainPlayer))) then
		return
	end

	local intesifySuitId = 0
	local intesifySuitFxIndex = 0

	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp ~= nil then
        intesifySuitId = CClientMainPlayer.Inst.ItemProp.IntesifySuitId
		intesifySuitFxIndex = CClientMainPlayer.Inst.ItemProp.IntesifySuitFxIndex
    end
    self:UpdateData(intesifySuitId, intesifySuitFxIndex)
end

function CLuaEquipmentSuitWnd:GetAdvanceDesc(num)
	return SafeStringFormat3(LocalString.GetString("全身均强化20级且基础提升≥%d%%"), num)
end

function CLuaEquipmentSuitWnd:GetEquipNum(suitDat)
	if suitDat.ID < 9 then
        return suitDat.NoWeaponEquipNum
    else
        if CClientMainPlayer.Inst == nil then
            return suitDat.NoWeaponEquipNum
        end
        if CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class) then --战狂按照单手算
            return suitDat.EquipNum
        end
        local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)
        local itemProp = CClientMainPlayer.Inst.ItemProp
		for i = 1, count do
			local itemId = itemProp:GetItemAt(EnumItemPlace.Body, i)
			local commonItem = CItemMgr.Inst:GetById(itemId)
			if commonItem ~= nil and commonItem.IsEquip and commonItem.Equip.IsWeapon then
				local equip = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)
				if equip ~= nil and equip.BothHand == 1 then
					return suitDat.BothHandEquip
				else
					return suitDat.EquipNum
				end
			end
        end
        return suitDat.NoWeaponEquipNum
    end
end

function CLuaEquipmentSuitWnd:UpdateData(intesifySuitId, intesifySuitFxIndex)
	self.find = EquipIntensify_Suit.GetData(intesifySuitId)
    self.nextFind = EquipIntensify_Suit.GetData(intesifySuitId + 1)

    self.seperateSprite.gameObject:SetActive(true)
    self.nextDescLabel.gameObject:SetActive(true)
    self.nextAdditionLabel.gameObject:SetActive(true)
    self.curTitleLabel.gameObject:SetActive(true)
    self.curDescLabel.gameObject:SetActive(true)
    self.curAdditionLabel.gameObject:SetActive(true)
    self.nextTitleLabel.gameObject:SetActive(true)
    self.curScoreLabel.gameObject:SetActive(true)
    self.nextScoreLabel.gameObject:SetActive(true)

    if intesifySuitId < 9 then
        self.curDescLabel.gameObject:SetActive(true)
        self.advanceLabel.gameObject:SetActive(false)
    else
        self.curDescLabel.gameObject:SetActive(false)
        self.advanceLabel.gameObject:SetActive(true)
        if self.find ~= nil then
			if intesifySuitFxIndex > 0 and not (self.find.SecondColorName==nil or self.find.SecondColorName=="") then
				self.advanceLabel.text = SafeStringFormat3(LocalString.GetString("获得%s"), self.find.SecondColorName)
			else
				self.advanceLabel.text = SafeStringFormat3(LocalString.GetString("获得%s"), self.find.ColorName)
			end
			local descLabel = self.advanceLabel.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
			descLabel.text = self:GetAdvanceDesc(self.find.IntensifyNumber)
        end
    end
    if intesifySuitId + 1 < 9 then
        self.nextDescLabel.gameObject:SetActive(true)
        self.nextAdvanceLabel.gameObject:SetActive(false)
    else
        self.nextDescLabel.gameObject:SetActive(false)
        self.nextAdvanceLabel.gameObject:SetActive(true)
        if self.nextFind ~= nil then
            self.nextAdvanceLabel.text = SafeStringFormat3(LocalString.GetString("获得%s"), self.nextFind.ColorName)
			local descLabel = self.nextAdvanceLabel.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
			descLabel.text = self:GetAdvanceDesc(self.nextFind.IntensifyNumber)
        end
    end

    if self.find ~= nil then
		if intesifySuitFxIndex > 0 and not (self.find.SecondDesc==nil or self.find.SecondDesc=="") then
			self.curTitleLabel.text = self.find.SecondDesc
		else
			self.curTitleLabel.text = self.find.Desc
		end

        self.curAdditionLabel.text = CEquipmentIntensifyMgr.GetSuitPropertyAddText(self.find.AddProperty)

        self.curScoreLabel.text = (LocalString.GetString("装备评分") .. "        +") .. self.find.Score

        self.curDescLabel.text = SafeStringFormat3(LocalString.GetString("穿戴%d件强化%d级"), self:GetEquipNum(self.find), self.find.IntensifyLevel)
        CUICommonDef.UpdatePerFectionIcons(self.perfectionIconArray1, self.find.Perfection, self.find.IntensifyLevel)
    else
        self.curTitleLabel.text = ""
        self.curAdditionLabel.text = ""
        self.curDescLabel.text = ""

        self.curTitleLabel.gameObject:SetActive(false)
        self.curAdditionLabel.gameObject:SetActive(false)
        self.seperateSprite.gameObject:SetActive(false)
        self.curDescLabel.gameObject:SetActive(false)
        self.curScoreLabel.gameObject:SetActive(false)
        self.advanceLabel.gameObject:SetActive(false)
    end

    if self.nextFind == nil or not CLuaEquipmentSuitInfoMgr.IsShowMainPlayerSuit then
        self.seperateSprite.gameObject:SetActive(false)
        self.nextDescLabel.gameObject:SetActive(false)
        self.nextAdditionLabel.gameObject:SetActive(false)
        self.nextTitleLabel.gameObject:SetActive(false)
        self.nextScoreLabel.gameObject:SetActive(false)

        self.nextAdvanceLabel.gameObject:SetActive(false)
    else
        if self.find ~= nil then
            self.seperateSprite.gameObject:SetActive(true)
        end

        local equips = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
        local count = 0
        CommonDefs.ListIterate(equips, DelegateFactory.Action_object(function (___value) 
            local equip = ___value
            local item = CItemMgr.Inst:GetById(equip.itemId)
            if CEquipmentIntensifyMgr.Inst:CheckEquipTypeCanIntensify(equip.itemId) and item.Equip.IntensifyLevel >= self.nextFind.IntensifyLevel and item.Equip.Duration > 0 and item.Equip.IsLostSoul == 0 then
                local addvalue = item.Equip:GetIntensifyTotalValue()
                local nextperfection = CEquipmentIntensifyMgr.Inst:CalcEquipIntensifyPerfectionByLevel(item.Equip, self.nextFind.IntensifyLevel)
                local perfection = math.max(item.Equip.Perfection, nextperfection)
                if perfection >= self.nextFind.Perfection and addvalue >= self.nextFind.IntensifyNumber then
                    count = count + 1
                end
            end
        end))

        self.nextTitleLabel.text = ((((LocalString.GetString("下一级：") .. self.nextFind.Desc) .. ":") .. count) .. "/") .. self:GetEquipNum(self.nextFind)

        self.nextAdditionLabel.text = CEquipmentIntensifyMgr.GetSuitPropertyAddText(self.nextFind.AddProperty)

        self.nextScoreLabel.text = (LocalString.GetString("装备评分") .. "        +") .. self.nextFind.Score


        self.nextDescLabel.text = SafeStringFormat3(LocalString.GetString("穿戴%d件强化%d级"), self:GetEquipNum(self.nextFind), self.nextFind.IntensifyLevel)
        CUICommonDef.UpdatePerFectionIcons(self.perfectionIconArray2, self.nextFind.Perfection, self.nextFind.IntensifyLevel)
    end
    self.table:Reposition()
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.parent.transform)
    self.parent.transform.localPosition = Vector3(0, b.size.y / 2)

    self.backgroundSprite.height = math.floor((b.size.y + 50))
end
