local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"

LuaSanxingResultWnd = class()
RegistChildComponent(LuaSanxingResultWnd,"closeBtn", GameObject)
RegistChildComponent(LuaSanxingResultWnd,"templateItem", GameObject)
RegistChildComponent(LuaSanxingResultWnd,"scoreLabel", UILabel)
RegistChildComponent(LuaSanxingResultWnd,"leftScrollView", UIScrollView)
RegistChildComponent(LuaSanxingResultWnd,"leftTable", UITable)
RegistChildComponent(LuaSanxingResultWnd,"rightScrollView", UIScrollView)
RegistChildComponent(LuaSanxingResultWnd,"rightTable", UITable)
RegistChildComponent(LuaSanxingResultWnd,"leftWinNode", GameObject)
RegistChildComponent(LuaSanxingResultWnd,"rightWinNode", GameObject)
RegistChildComponent(LuaSanxingResultWnd,"inGameNode", GameObject)
RegistChildComponent(LuaSanxingResultWnd,"leftSelfNode", GameObject)
RegistChildComponent(LuaSanxingResultWnd,"rightSelfNode", GameObject)
RegistChildComponent(LuaSanxingResultWnd,"scoreLabel1", UILabel)
RegistChildComponent(LuaSanxingResultWnd,"scoreSlider", GameObject)

RegistClassMember(LuaSanxingResultWnd, "maxChooseNum")

function LuaSanxingResultWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingResultWnd)
end

function LuaSanxingResultWnd:OnEnable()
	g_ScriptEvent:AddListener("SanxingResultUpdate", self, "InitData")
end

function LuaSanxingResultWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SanxingResultUpdate", self, "InitData")
end

function LuaSanxingResultWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.templateItem:SetActive(false)

	self:InitData()
end

function LuaSanxingResultWnd:InitData()
	self:InitScrollData()
	self:InitUpData()
	self:InitScoreData()
end

function LuaSanxingResultWnd:InitScoreData()
	local maxSliderWidth = 340
	if LuaSanxingGamePlayMgr.currentScore and LuaSanxingGamePlayMgr.totalScore then
		local percent = LuaSanxingGamePlayMgr.currentScore / LuaSanxingGamePlayMgr.totalScore
		if percent > 1 then
			percent = 1
		end
		self.scoreLabel1.text = LuaSanxingGamePlayMgr.currentScore .. '/' .. LuaSanxingGamePlayMgr.totalScore
		self.scoreSlider:GetComponent(typeof(UISprite)).width = percent * maxSliderWidth
	end
end

function LuaSanxingResultWnd:InitUpData()
	if LuaSanxingGamePlayMgr.winForce == 0 then
		self.leftWinNode:SetActive(true)
		self.rightWinNode:SetActive(false)
		--self.inGameNode:SetActive(false)
	elseif LuaSanxingGamePlayMgr.winForce == 1 then
		self.leftWinNode:SetActive(false)
		self.rightWinNode:SetActive(true)
		--self.inGameNode:SetActive(false)
	else
		self.leftWinNode:SetActive(false)
		self.rightWinNode:SetActive(false)
		--self.inGameNode:SetActive(true)
	end

	self.scoreLabel.text = LuaSanxingGamePlayMgr.jinshaScore
end

function LuaSanxingResultWnd:InitScrollData()
	CUICommonDef.ClearTransform(self.rightTable.transform)
	CUICommonDef.ClearTransform(self.leftTable.transform)

	table.sort(LuaSanxingGamePlayMgr.attackDataTable,function(a,b)
		return a[4]+a[5] > b[4]+b[5]
	end)
	table.sort(LuaSanxingGamePlayMgr.defendDataTable,function(a,b)
		return a[4]+a[5] > b[4]+b[5]
	end)
	local selfPos = 0
	for i,v in ipairs(LuaSanxingGamePlayMgr.attackDataTable) do
		local node = NGUITools.AddChild(self.rightTable.gameObject,self.templateItem)
		node:SetActive(true)
		if self:InitItemNodeInfo(v,node) then
			selfPos = 1
		end
	end
	self.rightTable:Reposition()
	self.rightScrollView:ResetPosition()

	for i,v in ipairs(LuaSanxingGamePlayMgr.defendDataTable) do
		local node = NGUITools.AddChild(self.leftTable.gameObject,self.templateItem)
		node:SetActive(true)
		if self:InitItemNodeInfo(v,node) then
			selfPos = 2
		end
	end
	self.leftTable:Reposition()
	self.leftScrollView:ResetPosition()

	if selfPos == 1 then
		self.rightSelfNode:SetActive(true)
		self.leftSelfNode:SetActive(false)
	elseif selfPos == 2 then
		self.rightSelfNode:SetActive(false)
		self.leftSelfNode:SetActive(true)
	else
		self.rightSelfNode:SetActive(false)
		self.leftSelfNode:SetActive(false)
	end
end

function LuaSanxingResultWnd:InitItemNodeInfo(data,node)
	node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = data[2]
	node.transform:Find('Sprite'):GetComponent(typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data[3]))
	node.transform:Find('ServerNameLabel'):GetComponent(typeof(UILabel)).text = data[6]
	node.transform:Find('FightScoreLabel'):GetComponent(typeof(UILabel)).text = data[4]
	node.transform:Find('OtherScoreLabel'):GetComponent(typeof(UILabel)).text = data[5]
	node.transform:Find('ScoreLabel'):GetComponent(typeof(UILabel)).text = data[4] + data[5]
	if data[1] == CClientMainPlayer.Inst.Id then
		node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('ServerNameLabel'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('FightScoreLabel'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('OtherScoreLabel'):GetComponent(typeof(UILabel)).color = Color.green
		node.transform:Find('ScoreLabel'):GetComponent(typeof(UILabel)).color = Color.green
		return true
	end

	local m_PlayerId = data[1]
	UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function(go)
		if m_PlayerId <= 10000000 then
			--fakeplayer
			--g_MessageMgr:ShowMessage("GuanNing_FakePlayer_NoInfo")
		else
			CPlayerInfoMgr.ShowPlayerPopupMenu(m_PlayerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
		end
	end)

	return false
end

function LuaSanxingResultWnd:OnDestroy()
--	if LuaSanxingGamePlayMgr.winForce == 0 or LuaSanxingGamePlayMgr.winForce == 1 then
--		LuaSanxingGamePlayMgr.ClearData()
--	end
	if LuaSanxingGamePlayMgr.EndScore then
		CUIManager.ShowUI(CLuaUIResources.SanxingEndWnd)
	end
end

return LuaSanxingResultWnd
