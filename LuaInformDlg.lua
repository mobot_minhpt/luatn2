local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UIEventListener = import "UIEventListener"
local CBaseWnd = import "L10.UI.CBaseWnd"

LuaInformDlg = class()

RegistClassMember(LuaInformDlg, "m_RefuseButton")
RegistClassMember(LuaInformDlg, "m_AcceptButton")
RegistClassMember(LuaInformDlg, "m_CloseButton")
RegistClassMember(LuaInformDlg, "m_InfoLabel")

RegistClassMember(LuaInformDlg, "m_CurInfo")
RegistClassMember(LuaInformDlg, "m_ExpiredTimeCheckTick")

function LuaInformDlg:Awake()
    self.m_RefuseButton = self.transform:Find("Anchor/RefuseButton"):GetComponent(typeof(CButton))
    self.m_AcceptButton = self.transform:Find("Anchor/AcceptButton"):GetComponent(typeof(CButton))
    self.m_CloseButton = self.transform:Find("Anchor/CloseButton").gameObject
    self.m_InfoLabel = self.transform:Find("Anchor/InfoLabel"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_RefuseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) self:OnRefuseButtonClick(go) end)
    UIEventListener.Get(self.m_AcceptButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) self:OnAcceptButtonClick(go) end)
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function (go) self:OnCloseButtonClick(go) end)
end

function LuaInformDlg:Init()
    self:Refresh()
end

function LuaInformDlg:OnEnable()
    self:StartExpiredTimeCheckTick()
end

function LuaInformDlg:OnDisable()
    self:StopExpiredTimeCheckTick()
end

function LuaInformDlg:StartExpiredTimeCheckTick()
    self:StopExpiredTimeCheckTick(type)
    self.m_ExpiredTimeCheckTick = RegisterTick(function()
        if LuaInformDlgMgr:CheckIfExpired() then
            self:Refresh()
        end
    end, 1000)
end

function LuaInformDlg:StopExpiredTimeCheckTick(type)
    if self.m_ExpiredTimeCheckTick then
        UnRegisterTick(self.m_ExpiredTimeCheckTick)
        self.m_ExpiredTimeCheckTick = nil
    end
end

function LuaInformDlg:Refresh()
    self.m_CurInfo = LuaInformDlgMgr:GetLatestInformInfo()
    if self.m_CurInfo then
        self.m_InfoLabel.text = self.m_CurInfo.message
        self.m_RefuseButton.Text = self.m_CurInfo.refuseText and self.m_CurInfo.refuseText or LocalString.GetString("拒绝")
        self.m_AcceptButton.Text = self.m_CurInfo.acceptText and self.m_CurInfo.acceptText or LocalString.GetString("同意")
    else
        self:Close()
    end
end

function LuaInformDlg:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaInformDlg:OnRefuseButtonClick(go)
    LuaInformDlgMgr:OnRefuse(self.m_CurInfo)
    self:Refresh()
end

function LuaInformDlg:OnAcceptButtonClick(go)
    LuaInformDlgMgr:OnAccept(self.m_CurInfo)
    self:Refresh()
end

function LuaInformDlg:OnCloseButtonClick(go)
    LuaInformDlgMgr:OnClose(self.m_CurInfo)
    self:Refresh()
end




