local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUIFx = import "L10.UI.CUIFx"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local QnTableItem = import "L10.UI.QnTableItem"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CUICommonDef = import "L10.UI.CUICommonDef"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CCommonItem = import "L10.Game.CCommonItem"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local StringBuilder = import "System.Text.StringBuilder"
local Object = import "System.Object"
local String = import "System.String"
local UInt32 = import "System.UInt32"
local UILabel = import "UILabel"
local UISprite = import "UISprite"

CLuaSpokesmanPawnshopWnd = class()

RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_TableView")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_DataList")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_NumLab")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_Fx1")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_Fx2")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_FxReplace")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_ScoreShopBtn")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_PawnBtn")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_TipBtn")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_GlobalClick")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_LimitLab")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_FxTick")

RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_TotalValue")
--RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_Tick")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_IsReadyPawn")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_PawnMaxTimePerDay")
RegistClassMember(CLuaSpokesmanPawnshopWnd, "m_PawnTime")

function CLuaSpokesmanPawnshopWnd:Awake()
    self:InitComponents()
end

function CLuaSpokesmanPawnshopWnd:InitComponents()
    self.m_TableView    = self.transform:Find("Anchor/Right/Table"):GetComponent(typeof(QnTableView))
    self.m_NumLab       = self.transform:Find("Anchor/Left/Title/Num"):GetComponent(typeof(UILabel))
    self.m_Fx1          = self.transform:Find("Anchor/Left/Fx1"):GetComponent(typeof(CUIFx))
    self.m_Fx2          = self.transform:Find("Anchor/Left/Fx2"):GetComponent(typeof(CUIFx))
    self.m_FxReplace    = self.transform:Find("Anchor/Left/Texture").gameObject
    self.m_ScoreShopBtn = self.transform:Find("Anchor/Left/ScoreBtn"):GetComponent(typeof(QnButton))
    self.m_PawnBtn      = self.transform:Find("Anchor/Right/Bottom/PawnBtn"):GetComponent(typeof(QnButton))
    self.m_TipBtn       = self.transform:Find("Anchor/Right/Bottom/Tip"):GetComponent(typeof(QnButton))
    self.m_GlobalClick  = self.transform:Find("GlobalClick").gameObject
    self.m_LimitLab     = self.transform:Find("Anchor/Right/DayLimit/Num"):GetComponent(typeof(UILabel))
end

function CLuaSpokesmanPawnshopWnd:Init()
    --初始化table
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,
        function(item, index)
            self:InitItem(item, self.m_DataList[index + 1])
        end
    )

    --初始化其它成员
    self.m_NumLab.text = LocalString.GetString("0当票")
    self.m_ScoreShopBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        CLuaNPCShopInfoMgr.ShowScoreShop("GlobalSpokesmanScore")
    end)
    self.m_PawnBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        if self.m_IsReadyPawn == true then
            self:ReallyToPawn()
            return
        end

        local hasSelect = false
        for k, v in pairs(self.m_DataList) do
            local info = v
            if info.isSelected == true then
                hasSelect = true
                break
            end
        end
        if hasSelect == true then
            self:MakeSurePawn()
        else
            g_MessageMgr:ShowMessage("SPOKESMAN_PAWNSHOP_PAWN_NOSELECT")
        end
    end)

    self.m_TipBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        g_MessageMgr:ShowMessage("SPOKESMAN_PAWNSHOP_TIP")
    end)

    UIEventListener.Get(self.m_GlobalClick).onClick = DelegateFactory.VoidDelegate(function ()
        if self.m_IsReadyPawn == true then
            self:ReallyToPawn()
            return
        end
    end)

    self.m_TotalValue = 0
    self.m_PawnMaxTimePerDay = Spokesman_Setting.GetData().PawnMaxTimePerDay
    self.m_PawnTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(CLuaSpokesmanMgr.eSpokesmanPawnTime)
    self.m_LimitLab.text = self.m_PawnTime .. "/" .. self.m_PawnMaxTimePerDay
    self:RequestData()
end

function CLuaSpokesmanPawnshopWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "RequestData")
    g_ScriptEvent:AddListener("SetItemAt", self, "RequestData")
    g_ScriptEvent:AddListener("GlobalSpokesmanSendAvailablePawn", self, "RefreshDataList")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function CLuaSpokesmanPawnshopWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "RequestData")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "RequestData")
    g_ScriptEvent:RemoveListener("GlobalSpokesmanSendAvailablePawn", self, "RefreshDataList")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function CLuaSpokesmanPawnshopWnd:OnDestroy()
    if self.m_IsReadyPawn == true then
        self:PawnSelectedItems()
        return
    end

    if self.m_FxTick then
        UnRegisterTick(self.m_FxTick)
    end
end

function CLuaSpokesmanPawnshopWnd:MakeSurePawn()
    local importantItems = CreateFromClass(MakeGenericClass(List, CCommonItem))
    for k, v in pairs(self.m_DataList) do
        local info = v
        if info.isSelected == true then
            local item = info.item
            if item.IsEquip and item.Equip.NeedConfirmationWhenDiscard then
                CommonDefs.ListAdd(importantItems, typeof(CCommonItem), item)
            elseif item.IsItem and item.Item.NeedConfirmationWhenDiscard then
                CommonDefs.ListAdd(importantItems, typeof(CCommonItem), item)
            end
        end
    end

    if importantItems.Count > 0 then
        local builder = StringBuilder()
        if importantItems.Count <= 4 then
            do
                local i = 0
                while i < importantItems.Count do
                    local item = importantItems[i]
                    builder:Append(item.ColoredName)
                    if i < importantItems.Count - 1 then
                        builder:Append(LocalString.GetString("、"))
                    end
                    i = i + 1
                end
            end
        else
            for i = 0, 3 do
                local item = importantItems[i]
                builder:Append(item.ColoredName)
                if i < 3 then
                    builder:Append(LocalString.GetString("、"))
                else
                    builder:Append(LocalString.GetString("等"))
                end
            end
        end
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GLOBALSPOKESMAN_REQUEST_PAWN_PRECIOUS", builder:ToString(), self.m_TotalValue), DelegateFactory.Action(function ()
            self:ReadyToPawn()
        end), nil, nil, nil, false)
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("SPOKESMAN_PAWNSHOP_PAWN_CONFIRM", self.m_TotalValue), DelegateFactory.Action(function()
            self:ReadyToPawn()
        end), nil, nil, nil, false)
    end
end

function CLuaSpokesmanPawnshopWnd:ReadyToPawn()
    self.m_IsReadyPawn = true
    self.m_FxReplace:SetActive(false)
    self.m_Fx1:LoadFx("Fx/UI/Prefab/UI_dangpudiandang_dongxiao.prefab")
    self.m_FxTick = RegisterTickOnce(function ()
        if self.m_Fx2 then
            self.m_Fx2:LoadFx("Fx/UI/Prefab/UI_dangpudiandang_didangpiaofaguang.prefab")
        end
    end, 3000)
end

function CLuaSpokesmanPawnshopWnd:ReallyToPawn()
    self.m_IsReadyPawn = false
    self.m_FxReplace:SetActive(true)
    self.m_Fx1:DestroyFx()
    self.m_Fx2:DestroyFx()
    if self.m_FxTick then
        UnRegisterTick(self.m_FxTick)
    end
    self:PawnSelectedItems()
end

function CLuaSpokesmanPawnshopWnd:RequestData()
    Gac2Gas.GlobalSpokesmanGetAvailablePawn()
end

function CLuaSpokesmanPawnshopWnd:RefreshDataList(datas)
    self.m_DataList = {}
    for i = 1, #datas do
        local data = datas[i]
        local itemId = data.itemId
        local pos = data.pos

        local item = CItemMgr.Inst:GetById(itemId)
        if item ~= nil then
                local amount = item.Amount
                local info = {}
                info.isFake = false
                info.pos = pos
                info.amount = amount
                info.item = item
                table.insert(self.m_DataList, info)
        end
    end

    --填充至16个
    for i = #self.m_DataList + 1, 16 do
        local info = {}
        info.isFake = true
        --info.index = i
        table.insert(self.m_DataList, info)
    end
    self:RefreshTotalValue()
    self.m_TableView:ReloadData(true, false)
end

function CLuaSpokesmanPawnshopWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == CLuaSpokesmanMgr.eSpokesmanPawnTime then
        self.m_PawnTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(CLuaSpokesmanMgr.eSpokesmanPawnTime)
        self.m_LimitLab.text = self.m_PawnTime .. "/" .. self.m_PawnMaxTimePerDay
    end
end

function CLuaSpokesmanPawnshopWnd:PawnSelectedItems()
    local rpcDatas = CreateFromClass(MakeGenericClass(List, Object))
    for k, v in pairs(self.m_DataList) do
        local info = v
        if info.isSelected == true then
            CommonDefs.ListAdd(rpcDatas, typeof(UInt32), info.pos)
            CommonDefs.ListAdd(rpcDatas, typeof(String), info.item.Id)
            CommonDefs.ListAdd(rpcDatas, typeof(UInt32), info.selectAmount)
        end
    end
    Gac2Gas.GlobalSpokesmanRequestPawn(MsgPackImpl.pack(rpcDatas))
    self:RequestData()
end

function CLuaSpokesmanPawnshopWnd:RefreshTotalValue()
    local totalValue = 0
    for k, v in pairs(self.m_DataList) do
        local info = v
        if info.isSelected == true then
            totalValue = totalValue + info.selectAmount
        end
    end
    self.m_TotalValue  = totalValue
    self.m_NumLab.text = SafeStringFormat3(LocalString.GetString("%d当票"), totalValue)
end

function CLuaSpokesmanPawnshopWnd:InitItem(item, info)
    local tableItem     = item.transform:GetComponent(typeof(QnTableItem))
    local longPressBtn  = item.transform:GetComponent(typeof(UILongPressButton))
    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local checkBox      = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
    local icon          = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local BindSprite    = item.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
    local AmountLabel   = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local TextLabel     = item.transform:Find("TextLabel"):GetComponent(typeof(UILabel))

    if info.isFake == true then
        qualitySprite.spriteName = "common_itemcell_01_border"
        icon.gameObject:SetActive(false)
        checkBox.gameObject:SetActive(false)
        BindSprite.gameObject:SetActive(false)
        AmountLabel.gameObject:SetActive(false)
        TextLabel.gameObject:SetActive(false)
        longPressBtn.OnClickDelegate = DelegateFactory.Action(function ()
            if self.m_IsReadyPawn == true then
                self:ReallyToPawn()
                return
            end
        end)
        longPressBtn.OnLongPressDelegate = nil
        return
    end

    if info.item.IsEquip then
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(info.item.Equip.QualityType)
    else
        local itemData = Item_Item.GetData(info.item.TemplateId)
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    end
    icon.gameObject:SetActive(true)
    icon:LoadMaterial(info.item.Icon)
    checkBox.gameObject:SetActive(true)
    checkBox.Selected = info.isSelected
    BindSprite.gameObject:SetActive(info.item.IsBinded)
    AmountLabel.gameObject:SetActive(info.item.IsItem)                      --装备不可以堆叠
    if info.isSelected == true then
        AmountLabel.text = info.selectAmount .. "/" .. info.amount
    else
        AmountLabel.text = info.amount
    end
    TextLabel.gameObject:SetActive(false)

    longPressBtn.OnClickDelegate = DelegateFactory.Action(function ()
        if self.m_IsReadyPawn == true then
            self:ReallyToPawn()
            return
        end

        if info.selectAmount == nil then
            info.selectAmount = 0
        end

        local allSelect = 0
        for k, v in pairs(self.m_DataList) do
            local info = v
            if info.isSelected == true then
                allSelect = allSelect + info.selectAmount
            end
        end

        local inputNumMax = math.min(info.amount, self.m_PawnMaxTimePerDay - self.m_PawnTime - allSelect)

        if inputNumMax <= 0 and not info.isSelected then                    --已经满了不让勾选了
            g_MessageMgr:ShowMessage("GLOBALSPOKESMAN_REQUEST_PAWN_MAXTIME", self.m_PawnMaxTimePerDay)
            return
        end

        if info.item.IsItem then
            if info.amount > 1 and not info.isSelected then                 --有堆叠且未勾选，让玩家选择数量
                CLuaNumberInputMgr.ShowNumInputBox(1, inputNumMax, info.selectAmount, function (num)
                    info.selectAmount = num
                    info.isSelected = true
                    AmountLabel.text = info.selectAmount .. "/" .. info.amount

                    checkBox.Selected = info.isSelected
                    self:RefreshTotalValue()
                end, g_MessageMgr:FormatMessage("GLOBALSPOKESMAN_REQUEST_PAWN_NUMBER", info.item.Name))
            else                                                            --没堆叠或者有堆叠且勾选，就直接点击控制
                info.isSelected = not info.isSelected
                info.selectAmount = info.isSelected and 1 or 0
                if info.isSelected then
                    AmountLabel.text = info.selectAmount .. "/" .. info.amount
                else
                    AmountLabel.text = info.amount
                end

                checkBox.Selected = info.isSelected
                self:RefreshTotalValue()
            end
        else
            info.isSelected = not info.isSelected
            info.selectAmount = info.isSelected and 1 or 0

            checkBox.Selected = info.isSelected
            self:RefreshTotalValue()
        end
    end)

    longPressBtn.OnLongPressDelegate = DelegateFactory.Action(function ()
        CItemInfoMgr.ShowLinkItemInfo(info.item, false, nil, AlignType.ScreenLeft, 0, 0, 0, 0)
    end)
end
