local UIGrid=import "UIGrid"
local Object=import "UnityEngine.Object"
local UISoundEvents=import "SoundManager+UISoundEvents"
local Vector3 = import "UnityEngine.Vector3"

CLuaDouDiZhuCard=class()

RegistClassMember(CLuaDouDiZhuCard,"m_Selected")
RegistClassMember(CLuaDouDiZhuCard,"m_Node")
RegistClassMember(CLuaDouDiZhuCard,"m_BgSprite")
RegistClassMember(CLuaDouDiZhuCard,"m_Card")
RegistClassMember(CLuaDouDiZhuCard,"m_Index")
RegistClassMember(CLuaDouDiZhuCard,"m_StartDrag")
RegistClassMember(CLuaDouDiZhuCard,"m_CardNum")
RegistClassMember(CLuaDouDiZhuCard,"m_Transform")
RegistClassMember(CLuaDouDiZhuCard,"m_Tweener")

RegistClassMember(CLuaDouDiZhuCard,"m_Lookup")

function CLuaDouDiZhuCard:Init(tf,card,index,cardNum,showback)
    self.m_Transform=tf
    self.m_Index=index
    self.m_StartDrag=false
    self.m_CardNum=cardNum
    tf.name=tostring(index)

    self.m_Selected=false
    self.m_Card=card
    self.m_Node=FindChild(tf,"Node").gameObject
    self.m_BgSprite=self.m_Node:GetComponent(typeof(UISprite))


    CLuaDouDiZhuMgr.SetCard(tf,card,index,showback)

    if not CLuaDouDiZhuMgr.m_IsWatch then
        UIEventListener.Get(self.m_Node).onClick=LuaUtils.VoidDelegate(function(go)
            self:OnClick()
        end)

        UIEventListener.Get(self.m_Node).onDrag=LuaUtils.VectorDelegate(function(go,vec)
            self:OnDrag(vec)
        end)
        UIEventListener.Get(self.m_Node).onDragStart=LuaUtils.VoidDelegate(function(go)
            self.m_StartDrag=true
        end)
        UIEventListener.Get(self.m_Node).onDragEnd=LuaUtils.VoidDelegate(function(go)
            self:OnDragEnd()
        end)

        UIEventListener.Get(self.m_Node).onPress=LuaUtils.BoolDelegate(function(go,isPressed)
            self:OnPress(isPressed)
        end)
    end

    self:SetCardLocalPosition(self.m_Index)
end

function CLuaDouDiZhuCard:OnPress(isPressed)
    if isPressed then
        CLuaDouDiZhuCardMgr.m_SelectedCardList={}
        self:SetGreyColor()
        CLuaDouDiZhuCardMgr.m_SelectedCardList[self]=true

        self.m_Lookup={}
        for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
            if v then
                self.m_Lookup[k.m_Index]=k
            end
        end
    else
        self.m_Lookup={}
        --所有选中的牌弹出
        self:SetNormalColor()
    end
end
function CLuaDouDiZhuCard:OnDragEnd()
    self.m_StartDrag=false
    for k,v in pairs(CLuaDouDiZhuCardMgr.m_SelectedCardList) do
        if v then
            k:SetNormalColor()
            if k.m_Selected then
                --复位
                LuaUtils.SetLocalPositionY(k.m_Node.transform,0)
            else
                --弹出
                LuaUtils.SetLocalPositionY(k.m_Node.transform,68)
            end
            k.m_Selected=not k.m_Selected
        end
    end
    SoundManager.Inst:PlayOneShot(UISoundEvents.ButtonClick, Vector3(0,0,0),nil,0)
end
function CLuaDouDiZhuCard:OnDrag(vec)
    if self.m_StartDrag then
        local dragonitem=CUICommonDef.SelectedUIWithRacast
        if dragonitem and dragonitem.name=="Node" then
            if CLuaDouDiZhuCardMgr.m_CardsLookup[dragonitem] then
                local card=CLuaDouDiZhuCardMgr.m_CardsLookup[dragonitem]
                -- print(self.m_Index,card.m_Index)
                if card.m_Index>self.m_Index then
                    for k,v in pairs(self.m_Lookup) do
                        if k>self.m_Index and k<=card.m_Index then
                            v:SetGreyColor()
                            CLuaDouDiZhuCardMgr.m_SelectedCardList[v]=true
                        end
                    end
                else
                    for k,v in pairs(self.m_Lookup) do
                        if k<self.m_Index and k>=card.m_Index then
                            v:SetGreyColor()
                            CLuaDouDiZhuCardMgr.m_SelectedCardList[v]=true
                        end
                    end
                end
            end
        end
    end
end
function CLuaDouDiZhuCard:OnClick(  )
    if self.m_Selected then
        --复位
        LuaUtils.SetLocalPositionY(self.m_Node.transform,0)
    else
        --弹出
        LuaUtils.SetLocalPositionY(self.m_Node.transform,68)
    end
    self.m_Selected=not self.m_Selected

    SoundManager.Inst:PlayOneShot(UISoundEvents.ButtonClick, Vector3(0,0,0),nil,0)
end


function CLuaDouDiZhuCard:SetCardLocalPosition(index)
    LuaUtils.SetLocalPositionX(self.m_Transform,self:GetCardLocalPositionX(index))
end



function CLuaDouDiZhuCard:GetCardLocalPositionX(index)
    local grid=self.m_Transform.parent:GetComponent(typeof(UIGrid))

    local maxPerLine=grid.maxPerLine
    local posX=grid.cellWidth*(index-1)

    local fx=grid.cellWidth*(self.m_CardNum-1)/2

    posX=posX-fx
    return posX
end

function CLuaDouDiZhuCard:Register()
    CLuaDouDiZhuCardMgr.m_CardsLookup[self.m_Node]=self
    CLuaDouDiZhuCardMgr.m_AllCards[self]=true
end
function CLuaDouDiZhuCard:Unregister()
    CLuaDouDiZhuCardMgr.m_CardsLookup[self.m_Node]=nil
    CLuaDouDiZhuCardMgr.m_AllCards[self]=nil
end

function CLuaDouDiZhuCard:SetGreyColor()
    self.m_BgSprite.color=Color(0.8,0.9,1,1)
end
function CLuaDouDiZhuCard:SetNormalColor()
    self.m_BgSprite.color=Color(1,1,1,1)
end
function CLuaDouDiZhuCard:Pop()
    LuaUtils.SetLocalPositionY(self.m_Node.transform,68)
    self.m_Selected=true
end
function CLuaDouDiZhuCard:TakeBack()
    LuaUtils.SetLocalPositionY(self.m_Node.transform,0)
    self.m_Selected=false
end
function CLuaDouDiZhuCard:MoveToNewPos()
    local posX=self:GetCardLocalPositionX(self.m_Index)
    LuaTweenUtils.DOKill(self.m_Tweener,false)
    LuaTweenUtils.TweenPositionX(self.m_Transform,posX,0.5)
end


function CLuaDouDiZhuCard:Disappear()
    LuaTweenUtils.DOKill(self.m_Transform,false)
    LuaTweenUtils.TweenPositionY(self.m_Transform,80,0.5)
    LuaTweenUtils.TweenAlpha(self.m_Transform,1,0,0.5)

    Object.Destroy(self.m_Transform.gameObject,0.5)
end

function CLuaDouDiZhuCard:PlayGetDiPaiEffect()
    LuaTweenUtils.DOKill(self.m_Transform,false)
    LuaUtils.SetLocalPositionY(self.m_Transform,180)
    LuaTweenUtils.TweenAlpha(self.m_Transform,0,1,0.8)
    LuaTweenUtils.TweenPositionY(self.m_Transform,0,0.8)
end

function CLuaDouDiZhuCard:PlayFaPaiEffect()
    LuaUtils.SetLocalPositionX(self.m_Transform,0)
    self:MoveToNewPos()
end
