local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaInheritZhenZhaiYuWordWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaInheritZhenZhaiYuWordWnd, "Fish1", "Fish1", GameObject)
RegistChildComponent(LuaInheritZhenZhaiYuWordWnd, "Fish2", "Fish2", GameObject)
RegistChildComponent(LuaInheritZhenZhaiYuWordWnd, "InheritBtn", "InheritBtn", GameObject)
RegistChildComponent(LuaInheritZhenZhaiYuWordWnd, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "m_ShuXingFish")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "m_EmptyFish")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "AddSprite")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "FishIcon")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "NameLabel")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "LevelLabel")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "FishBorder")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "WordLabel")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "AddSprite2")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "FishIcon2")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "NameLabel2")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "LevelLabel2")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "FishBorder2")
RegistClassMember(LuaInheritZhenZhaiYuWordWnd, "WordLabel2")
function LuaInheritZhenZhaiYuWordWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.AddSprite = self.Fish1.transform:Find("AddSprite").gameObject
    self.FishIcon = self.Fish1.transform:Find("FishIcon"):GetComponent(typeof(CUITexture))
    self.NameLabel = self.FishIcon.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.LevelLabel = self.FishIcon.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	self.FishBorder = self.FishIcon.transform:Find("FishBorder"):GetComponent(typeof(UISprite))
	self.WordLabel = self.FishIcon.transform:Find("WordLabel"):GetComponent(typeof(UILabel))

    self.AddSprite2 = self.Fish2.transform:Find("AddSprite").gameObject
    self.FishIcon2 = self.Fish2.transform:Find("FishIcon"):GetComponent(typeof(CUITexture))
    self.NameLabel2 = self.FishIcon2.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.LevelLabel2 = self.FishIcon2.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	self.FishBorder2 = self.FishIcon2.transform:Find("FishBorder"):GetComponent(typeof(UISprite))
	self.WordLabel2 = self.FishIcon2.transform:Find("WordLabel"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.Fish1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShuXingFishClick()
	end)
    UIEventListener.Get(self.Fish2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEmptyFishClick()
	end)
    UIEventListener.Get(self.InheritBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInheritBtnClick()
	end)
    UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)
end
function LuaInheritZhenZhaiYuWordWnd:OnEnable()
	g_ScriptEvent:AddListener("SelectFishForInheriting", self, "OnSelectFishForInheriting")
    g_ScriptEvent:AddListener("SelectFishForClearing", self, "OnSelectFishForClearing")
end

function LuaInheritZhenZhaiYuWordWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SelectFishForInheriting", self, "OnSelectFishForInheriting")
    g_ScriptEvent:RemoveListener("SelectFishForClearing", self, "OnSelectFishForClearing")
end

function LuaInheritZhenZhaiYuWordWnd:Init()
    self.FishIcon.gameObject:SetActive(self.m_ShuXingFish)
	self.AddSprite:SetActive(not self.m_ShuXingFish)

    self.FishIcon2.gameObject:SetActive(self.m_EmptyFish)
	self.AddSprite2:SetActive(not self.m_EmptyFish)
end

function LuaInheritZhenZhaiYuWordWnd:OnShuXingFishClick()
    LuaZhenZhaiYuMgr.OpenFishCreelForClearing()
end

function LuaInheritZhenZhaiYuWordWnd:OnEmptyFishClick()
    LuaZhenZhaiYuMgr.OpenFishCreelForInheriting()
end

function LuaInheritZhenZhaiYuWordWnd:OnSelectFishForClearing(fish)
	self.m_ShuXingFish = fish
	self.FishIcon.gameObject:SetActive(self.m_ShuXingFish)
	self.AddSprite:SetActive(not self.m_ShuXingFish)

	if fish then
		local itemId = fish.itemId
		local data = Item_Item.GetData(itemId)
		if  data then
			self.FishIcon:LoadMaterial(data.Icon)
			self.NameLabel.text = data.Name
			self.FishBorder.spriteName = CUICommonDef.GetItemCellBorder(data.NameColor)
		end
		local wordId = fish.wordId
		if wordId and wordId ~= 0 then
			self.WordLabel.text = Word_Word.GetData(wordId).Description
		else
			self.WordLabel.text = nil
		end

		local fishData = HouseFish_AllFishes.GetData(itemId)
		if fishData then
			self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),fishData.Level)
		end
	end
end

function LuaInheritZhenZhaiYuWordWnd:OnSelectFishForInheriting(fish)
	self.m_EmptyFish = fish
	self.FishIcon2.gameObject:SetActive(self.m_EmptyFish)
	self.AddSprite2:SetActive(not self.m_EmptyFish)

	if fish then
		local itemId = fish.itemId
		local data = Item_Item.GetData(itemId)
		if  data then
			self.FishIcon2:LoadMaterial(data.Icon)
			self.NameLabel2.text = data.Name
			self.FishBorder2.spriteName = CUICommonDef.GetItemCellBorder(data.NameColor)
		end
		local wordId = fish.wordId
		if wordId and wordId ~= 0 then
			self.WordLabel2.text = Word_Word.GetData(wordId).Description
		else
			self.WordLabel2.text = nil
		end

		local fishData = HouseFish_AllFishes.GetData(itemId)
		if fishData then
			self.LevelLabel2.text = SafeStringFormat3(LocalString.GetString("%d级"),fishData.Level)
		end
	end

end

function LuaInheritZhenZhaiYuWordWnd:OnInheritBtnClick()
	if not self.m_ShuXingFish then
		return
	end
	if not self.m_EmptyFish then
		return
	end
	
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Inherit_Zhenzhaiyu_Word_Comfirm"), 
        DelegateFactory.Action(function ()
			local consumeItemPos = LuaZhenZhaiYuMgr.m_InheritConsumeItemPos
    		local consumeItemId = LuaZhenZhaiYuMgr.m_InheritConsumeItemId
			local fromType = self.m_ShuXingFish.fishmapId and 2 or 1
			local fromFishPos=0
			local fromFishItemId = ""
			local fromFishId=0
			local toType = self.m_EmptyFish.fishmapId and 2 or 1
			local toFishPos=0
			local toFishItemId=""
			local toFishId=0
			if fromType == 1 then--从包裹
				fromFishPos = self.m_ShuXingFish.bagPos
				fromFishItemId = self.m_ShuXingFish.stringId
			else--从家园
				fromFishId = self.m_ShuXingFish.fishmapId
			end
			if toType == 1 then--从包裹
				toFishPos = self.m_EmptyFish.bagPos
				toFishItemId = self.m_EmptyFish.stringId
			else--从家园
				toFishId = self.m_EmptyFish.fishmapId
			end
			--IsI IsIIIsI
			Gac2Gas.RequestExchangeZhangzhaiFishEffect(consumeItemPos, consumeItemId, fromType, fromFishPos, fromFishItemId, fromFishId, toType, toFishPos, toFishItemId, toFishId)
			CUIManager.CloseUI(CLuaUIResources.InheritZhenZhaiYuWordWnd)
		end),
    nil,
    LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function LuaInheritZhenZhaiYuWordWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("Inherit_Zhenzhaiyu_Word_Tip")
end

--@region UIEvent

--@endregion UIEvent

