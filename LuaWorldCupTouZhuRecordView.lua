require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local CButton = import "L10.UI.CButton"
local MsgPackImpl = import "MsgPackImpl"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType2 = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CUIFx = import "L10.UI.CUIFx"

CLuaWorldCupTouZhuRecordView = class()

RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_ScorllView")
RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_Table")
RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_ItemTemplate")
RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_ReceiveAwardButton")
RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_CanReceiveSilver")
RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_ReceiveSilverLabel")
RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_ReceiveAwardReddot")
RegistClassMember(CLuaWorldCupTouZhuRecordView, "m_LastChooseItemBgGameObject")

function CLuaWorldCupTouZhuRecordView:Init(tf)
    self.m_ScorllView = tf.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = tf.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_ItemTemplate = tf.transform:Find("ScrollView/PlayInfoItem").gameObject
    self.m_ItemTemplate:SetActive(false)

    self.m_ReceiveAwardButton = tf.transform:Find("BottomArea/ReceiveAwardButton"):GetComponent(typeof(CButton))
    self.m_ReceiveSilverLabel = tf.transform:Find("BottomArea/CanReceiveSilver/Label"):GetComponent(typeof(UILabel))
    self.m_CanReceiveSilver = tf.transform:Find("BottomArea/CanReceiveSilver"):GetComponent(typeof(UILabel))
    self.m_ReceiveAwardReddot = tf.transform:Find("BottomArea/ReceiveAwardButton/Reddot"):GetComponent(typeof(UISprite))
end

function CLuaWorldCupTouZhuRecordView:Refresh(data)
    self.m_LastChooseItemBgGameObject = nil
    Extensions.RemoveAllChildren(self.m_Table.transform)

    local totalAward = 0

    -- 服务器的msgpack是C++实现的
    local index = 1
    local list = MsgPackImpl.unpack(data)
    for i = 0, list.Count-1, 6 do
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
        go:SetActive(true)

        local pid = list[i]

        local JingCaiQiShu = go.transform:Find("JingCaiQiShu"):GetComponent(typeof(UILabel))
        JingCaiQiShu.text = SafeStringFormat(LocalString.GetString("第%s期"), pid)

        local d = os.date("*t", list[i+1])
        local JingCaiTime = go.transform:Find("JingCaiTime"):GetComponent(typeof(UILabel))
        JingCaiTime.text = SafeStringFormat(LocalString.GetString("%d-%02d-%02d"), d.year, d.month, d.day)

        local JiangChi = go.transform:Find("JiangChi"):GetComponent(typeof(UILabel))
        JiangChi.text = SafeStringFormat(LocalString.GetString("%s"), list[i+2] * CLuaWorldCupMgr.m_JingCaiYinLiang)

        local TouZhuNum = go.transform:Find("TouZhuNum"):GetComponent(typeof(UILabel))
        TouZhuNum.text = SafeStringFormat(LocalString.GetString("%s"), list[i+3])

        local JingCaiResult1 = go.transform:Find("JingCaiResult1"):GetComponent(typeof(UILabel))
        local JingCaiResult2 = go.transform:Find("JingCaiResult2"):GetComponent(typeof(UILabel))
        JingCaiResult1.gameObject:SetActive(false)
        JingCaiResult2.gameObject:SetActive(false)

        local result = list[i+4]

        local FxNode = go.transform:Find("FxNode"):GetComponent(typeof(CUIFx))
        FxNode.gameObject:SetActive(false)

        local IsReceive = list[i+5]
        local ReceiveFlag = go.transform:Find("ReceiveFlag"):GetComponent(typeof(UISprite))
        ReceiveFlag.gameObject:SetActive(false)

        if result == -1 then
            JingCaiResult1.text = SafeStringFormat(LocalString.GetString("未知"))
            JingCaiResult1.gameObject:SetActive(true)
        elseif result == 0 then
            JingCaiResult1.text = LocalString.GetString("失败")
            JingCaiResult1.gameObject:SetActive(true)
        else
            JingCaiResult2.text = SafeStringFormat(LocalString.GetString("%s"), result)
            JingCaiResult2.gameObject:SetActive(true)
            if IsReceive == 0 then
                totalAward = totalAward + result
                FxNode.gameObject:SetActive(true)
            else
                -- 已经领取了打个勾
                ReceiveFlag.gameObject:SetActive(true)
            end
        end

        local UISprite = go:GetComponent(typeof(UISprite))
        if index % 2 == 0 then
            UISprite.spriteName = "common_textbg_02_light"
        else
            UISprite.spriteName = "common_textbg_02_dark"
        end

        local function OnClickPlayItem(go)
            self:OnPlayerClickPlayItem(go, pid)
        end
        CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(OnClickPlayItem), false)

        index = index + 1
    end

    self.m_Table:Reposition()
    self.m_ScorllView:ResetPosition()

    self.m_ReceiveSilverLabel.text = tostring(totalAward)

    local bAlert = false
    self.m_ReceiveAwardReddot.gameObject:SetActive(false)
    if totalAward == 0 then
        self.m_ReceiveAwardButton.Enabled = false
    else
        bAlert = true
        self.m_ReceiveAwardReddot.gameObject:SetActive(true)
        self.m_ReceiveAwardButton.Enabled = true
    end

    return bAlert
end

function CLuaWorldCupTouZhuRecordView:OnSelected()
    Gac2Gas.QueryWorldCupTodayJingCaiRecord()

    CommonDefs.AddOnClickListener(self.m_ReceiveAwardButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        Gac2Gas.RequestReceiveWorldCupJingCaiAward()
    end), false)
end

function CLuaWorldCupTouZhuRecordView:OnPlayerClickPlayItem(go, pid)
    if self.m_LastChooseItemBgGameObject then
        self.m_LastChooseItemBgGameObject.gameObject:SetActive(false)
    end
    local ItemBg = go.transform:Find("ItemBg"):GetComponent(typeof(UISprite))
    ItemBg.gameObject:SetActive(true)
    self.m_LastChooseItemBgGameObject = ItemBg

    local function SelectAction(index)
        Gac2Gas.QueryWorldCupTodayJingCaiDetail(pid)
    end

    local selectShareItem = DelegateFactory.Action_int(SelectAction)

    local tbl = {}
    local item = PopupMenuItemData(LocalString.GetString("查看详情"), selectShareItem, false,nil)
    table.insert(tbl, item)
    local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform:Find("FxNode").transform.position, AlignType2.Right, 1)
end

return CLuaWorldCupTouZhuRecordView
