local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local GameObject = import "UnityEngine.GameObject"
local BoxCollider = import "UnityEngine.BoxCollider"
local UIGrid = import "UIGrid"
local UITabBar = import "L10.UI.UITabBar"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UIProgressBar = import "UIProgressBar"
local UILabel = import "UILabel"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local DelegateFactory  = import "DelegateFactory"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local Profession = import "L10.Game.Profession"
local CTeamMgr = import "L10.Game.CTeamMgr"

LuaHengYuDangKouTeamSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "PlayerItem", "PlayerItem", GameObject)
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "TeamItem", "TeamItem", GameObject)
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "TeamGrid", "TeamGrid", UIGrid)
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "LeaderRoot", "LeaderRoot", GameObject)
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "MemberRoot", "MemberRoot", GameObject)
RegistChildComponent(LuaHengYuDangKouTeamSelectWnd, "SpectatorRoot", "SpectatorRoot", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouTeamSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaHengYuDangKouTeamSelectWnd:Init()

	self:ClearData()
	self.PlayerItem.gameObject:SetActive(false)
	self.TeamItem.gameObject:SetActive(false)

	self.DescLabel.text = g_MessageMgr:FormatMessage("HengYuDangKou_TeamSelect_Level_Desc")

	Gac2Gas.QueryHengYuDangKouTeamMemberInfo()
	Gac2Gas.QueryHengYuDangKouTeamMemberHpInfo()

	-- 请求队伍信息
	UnRegisterTick(self.m_InfoTick)
	self.m_InfoTick = RegisterTick(function()
		Gac2Gas.QueryHengYuDangKouTeamMemberInfo()
	end, 1000)

	-- 请求场景信息
	UnRegisterTick(self.m_HpTick)
	self.m_HpTick = RegisterTick(function()
		Gac2Gas.QueryHengYuDangKouTeamMemberHpInfo()
	end, 1000)

	-- 领队选择的队伍
	self.m_LeaderSelectTeam = nil
	self:SyncHengYuDangKouSceneInfo(LuaHengYuDangKouMgr.m_SceneInfo)
end

function LuaHengYuDangKouTeamSelectWnd:ClearData()
	self.m_TeamItemList = {}
	self.m_CheckBoxList = {}
	Extensions.RemoveAllChildren(self.TeamGrid.transform)
end

function LuaHengYuDangKouTeamSelectWnd:ReloadData()
	local voidLabel = self.transform:Find("Content/VoidLabel").gameObject
	voidLabel:SetActive(false)

	if self.m_MemberInfo == nil or #self.m_MemberInfo == 0 then
		self:ClearData()
		voidLabel:SetActive(true)
		return
	end

	local countChange = #self.m_MemberInfo ~= #self.m_TeamItemList
	if countChange then
		self:ClearData()
	end

	for i = 1, #self.m_MemberInfo do
		local g = self.m_TeamItemList[i]
		if g == nil then
			g = NGUITools.AddChild(self.TeamGrid.gameObject, self.TeamItem)
			self.m_TeamItemList[i] = g
		end
		g.gameObject:SetActive(true)

		local checkBox = g.transform:Find("CheckBox"):GetComponent(typeof(QnCheckBox))
		local indexLabel = g.transform:Find("TitleSprite/IndexLabel"):GetComponent(typeof(UILabel))
		local battleState = g.transform:Find("TitleSprite/BattleState").gameObject
		local inBattleState = g.transform:Find("TitleSprite/InBattleState").gameObject
		local memberGrid = g.transform:Find("MemberGrid"):GetComponent(typeof(UIGrid))

		indexLabel.text = SafeStringFormat3(LocalString.GetString("%d"), i)
		checkBox.gameObject:SetActive(not self.m_IsBattle)

		self.m_CheckBoxList[i] = checkBox

		--	{Capacit, teamId, memberList, team.m_LeaderId}
		local teamInfo = self.m_MemberInfo[i]

		local capacit = teamInfo[1]
		local teamId = teamInfo[2]
		local leaderId = teamInfo[4]

		-- 设置是否准备出战
		local isApplyBattle = false
		if self.m_SceneInfo and self.m_SceneInfo.applyForFightTeamIdList then
			for _, id in ipairs(self.m_SceneInfo.applyForFightTeamIdList) do
				if id == teamId then
					isApplyBattle = true
				end
			end
		end
		battleState:SetActive(isApplyBattle and not self.m_IsBattle)
		inBattleState:SetActive(self.m_IsBattle and self.m_SceneInfo.curSelectTeamId == teamId)

		-- 设置当前选中
		local selected = self.m_CurTeamId == teamId
		-- 如果是领对依赖领对选择的字段
		if self.m_IsLeader and self.m_LeaderSelectTeam then
			selected = self.m_LeaderSelectTeam == teamId
		end

		-- 检查选中是否变动
		if selected and self.m_CacheTeamData and self.m_IsLeader then
			local hasChange = false
			
			for index, data in ipairs(self.m_CacheTeamData) do
				if index == 3 then
					-- 检查队员
					if #data ~= #teamInfo[index] then
						hasChange = true
						break
					end

					for j = 1, #data do
						local member = data[j]
						local cacheMember = teamInfo[index][j]

						if member[1] ~= cacheMember[1] then
							hasChange = true
							break
						end
					end
				else
					-- 检查队伍数据
					if data ~= teamInfo[index] then
						hasChange = true
						break
					end
				end
			end

			self.m_CacheTeamData = teamInfo
			if hasChange then
				g_MessageMgr:ShowMessage("HengYuDangKou_Selected_Team_Change")
			end
		end

		checkBox:SetSelected(selected, true)
		checkBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
			self:SetSelectTeamId(teamId)
		end)
		checkBox.gameObject:GetComponent(typeof(BoxCollider)).enabled = self.m_IsLeader

		local changeMember = false
		if teamInfo ~= nil then
			local memberList = teamInfo[3]
			-- memberId, member:GetName(), member:GetClass(), member:GetLevel(), member:GetXianFanStatus()
			for j = 1, 5 do
				local item = nil
				if memberGrid.transform.childCount >= j then
					item = memberGrid.transform:GetChild(j - 1).gameObject
				else
					item = NGUITools.AddChild(memberGrid.gameObject, self.PlayerItem)
					changeMember = true
				end
				item.gameObject:SetActive(true)

				local memberInfo = memberList[j]
				local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
				local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
				local classIcon = item.transform:Find("ClazzIconSprite"):GetComponent(typeof(UISprite))
				local healthProgressBar = item.transform:Find("HealthProgressBar"):GetComponent(typeof(UIProgressBar))
				local voidLabel = item.transform:Find("VoidLabel")
				local nameBgSprite = item.transform:Find("NameBgSprite")

				local isVoid = memberInfo == nil
				voidLabel.gameObject:SetActive(isVoid)
				nameLabel.gameObject:SetActive(not isVoid)
				levelLabel.gameObject:SetActive(not isVoid)
				classIcon.gameObject:SetActive(not isVoid)
				nameBgSprite.gameObject:SetActive(not isVoid)

				healthProgressBar.gameObject:SetActive(true)
				healthProgressBar.value = 0

				-- 数据
				if memberInfo then
					local playerId = memberInfo[1]
					local name = memberInfo[2]
					local clss = memberInfo[3]
					local lv = memberInfo[4]
					local xianfanState = memberInfo[5]

					-- 血量
					local hpPercent = 0
					if self.m_HpInfo and self.m_HpInfo[playerId] then
						hpPercent = self.m_HpInfo[playerId]
					end
					healthProgressBar.value = hpPercent / 100

					-- 姓名
					if playerId == CClientMainPlayer.Inst.Id then
						nameLabel.color = Color.green
					else
						nameLabel.color = Color.white
					end
					nameLabel.text = name

					-- 职业
					classIcon.spriteName = Profession.GetIconByNumber(clss)

					-- 等级
					local colorString = NGUIText.EncodeColor24(levelLabel.color)
					if xianfanState then
						colorString = Constants.ColorOfFeiSheng
					end
					levelLabel.text = cs_string.Format("[c][{0}]lv.{1}[-][/c]", colorString, lv)

					UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
						CPlayerInfoMgr.ShowPlayerInfoWnd(playerId, name)
					end)
				end
			end
		end

		if changeMember then
			memberGrid:Reposition()
		end
	end
	
	if countChange then
		self.ScrollView:ResetPosition()
		self.TeamGrid:Reposition()
	end
end

function LuaHengYuDangKouTeamSelectWnd:SetSelectTeamId(teamId, notTrigger)
	if teamId == nil then
		self.m_CacheTeamData = nil
	elseif self.m_CurTeamId ~= teamId and self.m_IsLeader and not notTrigger then
		-- 不等于的时候才会上报
		Gac2Gas.HengYuDangKouSelectTeamForBoss2(teamId)
	end

	self.m_CurTeamId = teamId

	if teamId == nil then
		self.m_LeaderSelectTeam = 0
	else
		self.m_LeaderSelectTeam = teamId
	end

	-- 判断当前是几队
	local teamIndex = 0
	if self.m_MemberInfo then
		for index, teamInfo in ipairs(self.m_MemberInfo) do
			if teamInfo[2] == teamId then
				teamIndex = index
				self.m_CacheTeamData = teamInfo
				break
			end
		end
	end

	if self.m_CheckBoxList then
		for i, v in ipairs(self.m_CheckBoxList) do
			local select = teamIndex == i
			-- 设置当前选中
			v:SetSelected(select, true)
		end
	end
end

--  curDifficulty 当前难度
-- 	lastResetDifficultyTs 上次修改难度的时间戳
-- 	stage 副本类型
-- 	progress 当前进度
-- 	difficulty={ 已经通通关的boss选择的难度
-- 		1: 1,
-- 		2: 3,
-- 		3: 2,
-- 	}
--  leftTime 限时小怪剩余时间

--  leader 有权限的人的id
--  leaderName 有权限的玩家名称
--  leaderStage 阶段

--  curSelectTeamId 当前出战队伍
--  boss2Score boss2阶段分数

--  applyForFightTeamIdList 申请列表
--  openSelectTeamWnd 是否打开界面
function LuaHengYuDangKouTeamSelectWnd:SetMode()
	print("test")
	UnRegisterTick(self.m_BtnLabelTick)

	if self.m_SceneInfo == nil then
		return
	end

	self.m_IsBattle = self.m_SceneInfo.leaderStage == EnumHengYuDangKouLeaderStage.eFight
	self.m_IsLeader = self.m_SceneInfo.leader == CClientMainPlayer.Inst.Id and not self.m_IsBattle
	self.m_IsSpectator = not self.m_IsLeader and not self.m_IsBattle
	
	self.LeaderRoot.gameObject:SetActive(self.m_IsLeader and not self.m_IsBattle)
	self.SpectatorRoot.gameObject:SetActive(self.m_IsBattle)
	self.MemberRoot.gameObject:SetActive(not self.m_IsLeader and not self.m_IsBattle)

	local selectRoot = self.m_IsLeader and self.LeaderRoot or self.MemberRoot
	if self.m_IsBattle then
		selectRoot = self.SpectatorRoot
	end

	-- 根据推送刷新当前选择队伍
	if self.m_LeaderSelectTeam == nil or not self.m_IsLeader then
		self:SetSelectTeamId(self.m_SceneInfo.curSelectTeamId, true)
	end	

	-- 设置通用的按钮
	local guildBtn = selectRoot.transform:Find("GuildBtn").gameObject
	UIEventListener.Get(guildBtn).onClick = DelegateFactory.VoidDelegate(function (go)
		CSocialWndMgr.ShowChatWnd(EChatPanel.Guild)
	end)

	local ruleBtn = selectRoot.transform:Find("RuleBtn").gameObject
	UIEventListener.Get(ruleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
		g_MessageMgr:ShowMessage("HengYuDangKou_TeamSelect_Tip")
	end)

	-- 设置当前选择的队伍名称
	local teamSelectDesc = LocalString.GetString("无")
	local curSelectTeamId = self.m_SceneInfo.curSelectTeamId
	if self.m_MemberInfo then
		for index, teamInfo in ipairs(self.m_MemberInfo) do
			if teamInfo[2] == curSelectTeamId then
				local leaderId = teamInfo[4]
				for _, member in ipairs(teamInfo[3]) do
					if member[1] == leaderId then
						teamSelectDesc = SafeStringFormat3(LocalString.GetString("%s队（%s的队伍）"), tostring(index) ,member[2])
						break
					end
				end
				break
			end
		end
	end

	-- 设置通用的显示
	if not self.m_IsBattle then
		local leaderDesc = selectRoot.transform:Find("InfoTable/LeaderDesc/LeaderDescLabel"):GetComponent(typeof(UILabel))
		leaderDesc.text = self.m_SceneInfo.leaderName
	end
	
	local selectDesc = selectRoot.transform:Find("InfoTable/SelectDesc/SelectDescLabel"):GetComponent(typeof(UILabel))
	-- 设置当前选择的队伍名称
	selectDesc.text = teamSelectDesc

	if self.m_IsBattle then
		self.DescLabel.text = g_MessageMgr:FormatMessage("HengYuDangKou_TeamSelect_Level_Desc2")
	elseif self.m_IsLeader then
		self.DescLabel.text = g_MessageMgr:FormatMessage("HengYuDangKou_TeamSelect_Level_Desc")

		-- 操作人界面
		-- 确定开启战斗
		local applyBtn = self.LeaderRoot.transform:Find("ApplyBtn").gameObject
		UIEventListener.Get(applyBtn).onClick = DelegateFactory.VoidDelegate(function (go)
			if self.m_CurTeamId == nil then
				g_MessageMgr:ShowMessage("HengYuDangKou_Need_Select_Team")
				return
			end
			Gac2Gas.HengYuDangKouBoss2MemberConfirm()
		end)
	else
		self.DescLabel.text = g_MessageMgr:FormatMessage("HengYuDangKou_TeamSelect_Level_Desc1")
		-- 普通成员玩家界面
		-- 申请出战
		local requestBattleBtn = self.MemberRoot.transform:Find("RequestBattleBtn").gameObject
		local requestBattleLabel = self.MemberRoot.transform:Find("RequestBattleBtn/Label"):GetComponent(typeof(UILabel))

		local selfTeamId = CTeamMgr.Inst.TeamId

		-- 判断是否已经申请
		local hasRequestBattle = false
		local applyForFightTeamIdList = self.m_SceneInfo.applyForFightTeamIdList
		
		if applyForFightTeamIdList then
			for _, id in ipairs(applyForFightTeamIdList) do
				if selfTeamId == id then
					hasRequestBattle = true
				end
			end
		end
		
		-- 判断当前是几队
		local teamIndex = 0
		if self.m_MemberInfo then
			for index, teamInfo in ipairs(self.m_MemberInfo) do
				if teamInfo[2] == selfTeamId then
					teamIndex = index
					break
				end
			end
		end

		local teamStr = LocalString.GetString("队伍")
		if teamIndex > 0 then
			teamStr = SafeStringFormat3(LocalString.GetString("%s队"), tostring(teamIndex))
		end

		if hasRequestBattle then
			requestBattleLabel.text = SafeStringFormat3(LocalString.GetString("%s已申请出战"), teamStr)
			CUICommonDef.SetActive(requestBattleBtn, false, true)
		else
			CUICommonDef.SetActive(requestBattleBtn, true, true)
			requestBattleLabel.text = SafeStringFormat3(LocalString.GetString("%s申请出战"), teamStr)

			UIEventListener.Get(requestBattleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
				if CTeamMgr.Inst:GetLeaderId() == CClientMainPlayer.Inst.Id then
					-- 申请出战
					Gac2Gas.HengYuDangKouApplyForFight()
				else
					g_MessageMgr:ShowMessage("HengYuDangKou_Boss2CantApplyForFightNotLeader")
				end
			end)
		end
		
		-- 申请成为领队
		local requestLeaderBtn = self.MemberRoot.transform:Find("RequestLeaderBtn").gameObject
		local requestLeaderLabel = self.MemberRoot.transform:Find("RequestLeaderBtn/Label"):GetComponent(typeof(UILabel))

		UIEventListener.Get(requestLeaderBtn).onClick = DelegateFactory.VoidDelegate(function (go)
			Gac2Gas.HengYuDangKouApplyBoss2Leader()
		end)

		local lastTc = 0
		if self.m_ExtraInfo and self.m_ExtraInfo.LastLeaderApplyTs then
			lastTc = self.m_ExtraInfo.LastLeaderApplyTs
		end

		local diff = math.floor(10 - math.max(CServerTimeMgr.Inst.timeStamp - lastTc, 0))

		-- 超过了这个时间
		if diff <= 0 then
			requestLeaderLabel.text = LocalString.GetString("申请操作权")
			CUICommonDef.SetActive(requestLeaderBtn, true, true)
		else
			requestLeaderLabel.text = SafeStringFormat3(LocalString.GetString("申请操作权(%s)"), tostring(diff))
			CUICommonDef.SetActive(requestLeaderBtn, false, true)

			-- 更新时间
			self.m_BtnLabelTick = RegisterTick(function()
				if self.m_ExtraInfo and self.m_ExtraInfo.LastLeaderApplyTs then
					local diffBtn = math.floor(10 - math.max(CServerTimeMgr.Inst.timeStamp - self.m_ExtraInfo.LastLeaderApplyTs, 0))
					if diffBtn <= 0 then
						requestLeaderLabel.text = LocalString.GetString("申请操作权")
						CUICommonDef.SetActive(requestLeaderBtn, true, true)
					else
						requestLeaderLabel.text = SafeStringFormat3(LocalString.GetString("申请操作权(%s)"), tostring(diffBtn))
						CUICommonDef.SetActive(requestLeaderBtn, false, true)
					end
				end
			end, 100)
		end
	end

	-- 降低透明度
	self.TeamGrid.gameObject:GetComponent(typeof(UIWidget)).alpha = self.m_IsSpectator and 0.6 or 1
	selectRoot.transform:Find("InfoTable"):GetComponent(typeof(UITable)):Reposition()
end

function LuaHengYuDangKouTeamSelectWnd:SyncHengYuDangKouSceneInfo(sceneInfo)
	self.m_SceneInfo = sceneInfo
	self:ReloadData()
	self:SetMode()
end

function LuaHengYuDangKouTeamSelectWnd:OnHpInfo(hpInfo)
	self.m_HpInfo = hpInfo
	self:ReloadData()
	self:SetMode()
end

function LuaHengYuDangKouTeamSelectWnd:OnMemberInfo(memberInfo, extraInfo)
	self.m_MemberInfo = memberInfo
	self.m_ExtraInfo = extraInfo
	self:ReloadData()
	self:SetMode()
end

function LuaHengYuDangKouTeamSelectWnd:MainPlayerLeaveTeam()
	CUIManager.CloseUI(CLuaUIResources.HengYuDangKouTeamSelectWnd)
end

function LuaHengYuDangKouTeamSelectWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
	g_ScriptEvent:AddListener("QueryHengYuDangKouTeamMemberInfoResult", self, "OnMemberInfo")
	g_ScriptEvent:AddListener("QueryHengYuDangKouTeamMemberHpInfoResult", self, "OnHpInfo")
	g_ScriptEvent:AddListener("MainPlayerLeaveTeam", self, "MainPlayerLeaveTeam")
end

function LuaHengYuDangKouTeamSelectWnd:OnDisable()
	UnRegisterTick(self.m_InfoTick)
	UnRegisterTick(self.m_HpTick)
	UnRegisterTick(self.m_BtnLabelTick)

	g_ScriptEvent:RemoveListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
	g_ScriptEvent:RemoveListener("QueryHengYuDangKouTeamMemberInfoResult", self, "OnMemberInfo")
	g_ScriptEvent:RemoveListener("QueryHengYuDangKouTeamMemberHpInfoResult", self, "OnHpInfo")
	g_ScriptEvent:RemoveListener("MainPlayerLeaveTeam", self, "MainPlayerLeaveTeam")
end


--@region UIEvent

function LuaHengYuDangKouTeamSelectWnd:OnOpenGroupBtnClick()
	CUIManager.ShowUI(CUIResources.TeamGroupWnd)
end

function LuaHengYuDangKouTeamSelectWnd:OnGuildBtnClick()
	CSocialWndMgr.ShowChatWnd(EChatPanel.Guild)
end

function LuaHengYuDangKouTeamSelectWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("HengYuDangKou_TeamSelect_Tip")
end

function LuaHengYuDangKouTeamSelectWnd:OnCommitBtnClick()
	if self.m_CurTeamId == nil then
		g_MessageMgr:ShowMessage("HengYuDangKou_Need_Select_Team")
		return
	end
	Gac2Gas.HengYuDangKouBoss2MemberConfirm()
end

function LuaHengYuDangKouTeamSelectWnd:OnApplyBtnClick()
	Gac2Gas.HengYuDangKouApplyBoss2Leader()
end


--@endregion UIEvent

