local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local GameSetting_Client=import "L10.Game.GameSetting_Client"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local MessageMgr=import "L10.Game.MessageMgr"
local CGetMoneyMgr=import "L10.UI.CGetMoneyMgr"

CLuaLingShouBabyAutoWashWnd=class(CLuaLingShouBabyBaseWashWnd)


RegistClassMember(CLuaLingShouBabyAutoWashWnd,"m_AccelerateButton")
RegistClassMember(CLuaLingShouBabyAutoWashWnd,"m_InitRate")--初始请求速度
RegistClassMember(CLuaLingShouBabyAutoWashWnd,"m_RequestRate")--请求速度
RegistClassMember(CLuaLingShouBabyAutoWashWnd,"m_SpeedLabel")--请求速度
RegistClassMember(CLuaLingShouBabyAutoWashWnd,"m_RequestState")

function CLuaLingShouBabyAutoWashWnd:Init()
    CLuaLingShouBabyBaseWashWnd.Init(self)
    --按钮置灰
    self:InitView()
end

function CLuaLingShouBabyAutoWashWnd:InitView()
    LuaGameObject.GetChildNoGC(self.transform,"TitleLabel").label.text=LocalString.GetString("灵兽宝宝自动洗炼")
    self.m_AccelerateButton=LuaGameObject.GetChildNoGC(self.transform,"AccelerateButton").gameObject
    self.m_AccelerateButton:SetActive(true)

    self.m_InitRate=GameSetting_Client.GetData().EQUIP_AUTO_XILIAN_LIMIT_TIME
    self.m_RequestRate=self.m_InitRate
    self.m_SpeedLabel=LuaGameObject.GetChildNoGC(self.transform,"SpeedLabel").label

    --加速按钮
    local g = LuaGameObject.GetChildNoGC(self.transform,"AccelerateButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_SpeedLabel.text == LocalString.GetString("速度x1") then
            self.m_SpeedLabel.text = LocalString.GetString("速度x2")
            self.m_RequestRate = self.m_InitRate / 2
        elseif self.m_SpeedLabel.text == LocalString.GetString("速度x2") then
            self.m_SpeedLabel.text = LocalString.GetString("速度x4")
            self.m_RequestRate = self.m_InitRate / 4
        elseif self.m_SpeedLabel.text == LocalString.GetString("速度x4") then
            self.m_SpeedLabel.text = LocalString.GetString("速度x1")
            self.m_RequestRate = self.m_InitRate
        end
    end)
    self.m_RequestState=EnumBabyWashRequestState.eNone

end
--开始洗炼之前设置界面状态
function CLuaLingShouBabyAutoWashWnd:SetUIBeforeRequest()
    self.m_RequestLabel.text=LocalString.GetString("停止洗炼")
    CUICommonDef.SetActive(self.m_AcceptButton, false,true)
    self.m_Pause=false
    local g = LuaGameObject.GetChildNoGC(self.transform,"StartButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        self:StopAutoWash()
    end)
end
--结束洗炼之后设置界面状态
function CLuaLingShouBabyAutoWashWnd:SetUIAfterRequest()
    self.m_RequestLabel.text=LocalString.GetString("开始洗炼")
    CUICommonDef.SetActive(self.m_AcceptButton, true,true)
    self.m_Pause=false
    local g = LuaGameObject.GetChildNoGC(self.transform,"StartButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        self:StartWash()
    end)
end

function CLuaLingShouBabyAutoWashWnd:RequestWash()
    self:SetUIBeforeRequest()
    self.m_RequestState=EnumBabyWashRequestState.eNormal
end

function CLuaLingShouBabyAutoWashWnd:StopAutoWash()
    self:SetUIAfterRequest()
    self.m_RequestState=EnumBabyWashRequestState.eNone
end

function CLuaLingShouBabyAutoWashWnd:Update()
    if self.m_Pause then
        return
    end
    if CClientMainPlayer.Inst==nil then
        self.m_RequestState=EnumBabyWashRequestState.eNone
        self:SetUIAfterRequest()
        return
    end
    if self.m_RequestState==EnumBabyWashRequestState.eNormal then
        local rst,pos,itemId=CItemMgr.Inst:FindItemByTemplateIdWithNotExpired(EnumItemPlace.Bag, CLuaLingShouMgr.BiLiuLuId)
        if rst then
            Gac2Gas.RequestWashLingShouBaby(self.m_LingShouId, 1, pos, itemId, false);
            self.m_RequestState=EnumBabyWashRequestState.eWaitResult
            return
        else
            --没有材料了
            --尝试用灵玉购买
            if self:AutoCostLingyu() then
                if self.m_MoneyCtrl.moneyEnough then
                    Gac2Gas.RequestWashLingShouBaby(self.m_LingShouId, LuaEnumItemPlace.Bag, 0, "",true)
                    self.m_RequestState=EnumBabyWashRequestState.eWaitResult
                    return
                else
                    CGetMoneyMgr.Inst:GetLingYu(self.m_JadeCost,CLuaLingShouMgr.BiLiuLuId)
                    self:StopAutoWash()
                    return
                end
            else
                MessageMgr.Inst:ShowMessage("LINGSHOU_NOT_HAVE_WASH_ITEM",{})
                self.m_RequestState=EnumBabyWashRequestState.eNone
                self:SetUIAfterRequest()
                return
            end
            return
        end
    elseif self.m_RequestState==EnumBabyWashRequestState.eResult then
        --看看结果如何
        --继续洗 等待一点时间

        self.m_RequestState=EnumBabyWashRequestState.eWaitDelta
        self.m_StartWaitTime=Time.time
    -- elseif self.m_RequestState==EnumBabyWashRequestState.eWaitResult then

    elseif self.m_RequestState==EnumBabyWashRequestState.eWaitDelta then
        if Time.time>self.m_StartWaitTime+self.m_RequestRate then
            self.m_RequestState=EnumBabyWashRequestState.eNormal
        end
    end
end

function CLuaLingShouBabyAutoWashWnd:OnUpdateLingShouBabyLastWashResult( args )
    local lingShouId=args[0]
    local babyQuality=args[1]
    local babySkillCls1=args[2]
    local babySkillCls2=args[3]
    --洗炼结束
    if self.m_LingShouId==lingShouId then
        self.m_View2:InitResult(self.m_BabyInfo,babyQuality,babySkillCls1,babySkillCls2)
        -- CUICommonDef.SetActive(self.m_AcceptButton,true,true)


        if babyQuality>self.m_BabyInfo.Quality or babyQuality>=5 then--甲乙的时候都停一下
            if babyQuality>self.m_BabyInfo.Quality then
                self.m_RecommendSprite.gameObject:SetActive(true)
            end
            self.m_RequestState=EnumBabyWashRequestState.eNone
            self:SetUIAfterRequest()
        else
            self.m_RecommendSprite.gameObject:SetActive(false)
            self.m_RequestState=EnumBabyWashRequestState.eResult
        end
        local details=CLingShouMgr.Inst:GetLingShouDetails(self.m_LingShouId)
        if details~=nil then
            self.m_BabyInfo=details.data.Props.Baby
        end
    end
end

return CLuaLingShouBabyAutoWashWnd
