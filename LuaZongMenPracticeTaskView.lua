local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaZongMenPracticeTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongMenPracticeTaskView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaZongMenPracticeTaskView, "DescLab", "DescLab", UILabel)
RegistChildComponent(LuaZongMenPracticeTaskView, "Bg", "Bg", UISprite)
RegistChildComponent(LuaZongMenPracticeTaskView, "CountDownLabel", "CountDownLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenPracticeTaskView, "m_Stage")
RegistClassMember(LuaZongMenPracticeTaskView, "m_TotalDoors")
RegistClassMember(LuaZongMenPracticeTaskView, "m_DestroyedDoors")
RegistClassMember(LuaZongMenPracticeTaskView, "m_Tick")

function LuaZongMenPracticeTaskView:Awake()
    self.TaskName.text = LocalString.GetString("[破幻伏魔]")
    self.DescLab.text = ""
end

function LuaZongMenPracticeTaskView:Init()
    self.m_Stage = LuaZongMenMgr.m_SectXiuxingPlayStage
    self.m_TotalDoors = LuaZongMenMgr.m_SectXiuxingTotalDoors or 0
    self.m_DestroyedDoors = LuaZongMenMgr.m_SectXiuxingDestroyedDoors or 0
    self:SetFinishTime(LuaZongMenMgr.m_SectXiuxingFinishTime)

    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
    self:RefreshDesc()
end

--@region UIEvent

--@endregion UIEvent
function LuaZongMenPracticeTaskView:OnEnable()
    g_ScriptEvent:AddListener("UpdateXiuxingDoorAndTime", self, "OnUpdateXiuxingDoorAndTime")
    g_ScriptEvent:AddListener("SyncSectXiuxingPlayStatus_Ite", self, "OnSyncSectXiuxingPlayStatus_Ite")
end

function LuaZongMenPracticeTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateXiuxingDoorAndTime", self, "OnUpdateXiuxingDoorAndTime")
    g_ScriptEvent:RemoveListener("SyncSectXiuxingPlayStatus_Ite", self, "OnSyncSectXiuxingPlayStatus_Ite")
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaZongMenPracticeTaskView:OnUpdateXiuxingDoorAndTime(totalDoors, destroyedDoors, finishTime)
    self.m_TotalDoors = totalDoors
    self.m_DestroyedDoors = destroyedDoors
    self:SetFinishTime(finishTime)
    self:RefreshDesc()
end

function LuaZongMenPracticeTaskView:OnSyncSectXiuxingPlayStatus_Ite(sectId, playStage, bJoined)
    self.m_Stage = playStage
    self:SetFinishTime(self.m_FinishTime)
    self:RefreshDesc()
end

function LuaZongMenPracticeTaskView:SetFinishTime(finishTime)
    if EnumSectXiuxingPlayStage.eBoss == self.m_Stage then
        self.m_FinishTime = CServerTimeMgr.Inst:GetTodayTimeStampByTime(19, 45, 0)
    else
        self.m_FinishTime = finishTime
    end
end

function LuaZongMenPracticeTaskView:RefreshDesc()
    if self.m_Stage == EnumSectXiuxingPlayStage.eBoss then
        self.DescLab.text  = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("ZongMen_Practice_Stage2TaskDesc"))
        self.Bg.height = self.DescLab.height + 103
        self.CountDownLabel.alpha = 1
    elseif self.m_Stage == EnumSectXiuxingPlayStage.eRunning then
        self.DescLab.text  = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("ZongMen_Practice_Stage1TaskDesc", self.m_DestroyedDoors, self.m_TotalDoors))
        self.Bg.height = self.DescLab.height + 103
        self.CountDownLabel.alpha = 1
    elseif self.m_Stage == EnumSectXiuxingPlayStage.eSignup then
        self.DescLab.text  = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("ZongMen_Practice_PreStage1TaskDesc"))
        self.Bg.height = self.DescLab.height + 63
        self.CountDownLabel.alpha = 0
    end
end

function LuaZongMenPracticeTaskView:OnTick()
    if self.m_FinishTime then
        local remainTime = self.m_FinishTime - CServerTimeMgr.Inst.timeStamp
        self.CountDownLabel.text = SafeStringFormat3("%s[c][ff800a]%s[-][/c]", LocalString.GetString("剩余时间："), self:GetRemainTimeText(remainTime))
    else
        self.CountDownLabel.text = nil
    end
end

function LuaZongMenPracticeTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end