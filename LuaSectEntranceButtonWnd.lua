local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CMainCamera = import "L10.Engine.CMainCamera"
local SectEntranceTrapType = import "L10.Engine.Scene.CRenderScene+SectEntranceTrapType"
LuaSectEntranceButtonWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSectEntranceButtonWnd, "SelfBtnAnchor", "SelfBtnAnchor", GameObject)
RegistChildComponent(LuaSectEntranceButtonWnd, "SelfBtn", "SelfBtn", GameObject)
RegistChildComponent(LuaSectEntranceButtonWnd, "TaskBtnAnchor", "TaskBtnAnchor", GameObject)
RegistChildComponent(LuaSectEntranceButtonWnd, "TaskBtn", "TaskBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaSectEntranceButtonWnd, "m_DefaultButtonWorldPos")
RegistClassMember(LuaSectEntranceButtonWnd, "m_DefaultSectTrapInfo")
RegistClassMember(LuaSectEntranceButtonWnd, "m_TaskButtonWorldPos")
RegistClassMember(LuaSectEntranceButtonWnd, "m_TaskSectTrapInfo")

function LuaSectEntranceButtonWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.SelfBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelfBtnClick()
	end)
	UIEventListener.Get(self.TaskBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTaskBtnClick()
	end)
	self:OnSendExtraTrapInfo()
    --@endregion EventBind end
end

function LuaSectEntranceButtonWnd:Update()
	self:UpdateButtonPos(self.m_DefaultSectTrapInfo, self.m_DefaultButtonWorldPos, self.SelfBtnAnchor)
	self:UpdateButtonPos(self.m_TaskSectTrapInfo, self.m_TaskButtonWorldPos, self.TaskBtnAnchor)
end

function LuaSectEntranceButtonWnd:OnEnable()
	g_ScriptEvent:AddListener("OnLeaveZongMen", self, "OnLeaveZongMen")
	g_ScriptEvent:AddListener("OnSendExtraTrapInfo", self, "OnSendExtraTrapInfo")
end

function LuaSectEntranceButtonWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnLeaveZongMen", self, "OnLeaveZongMen")
	g_ScriptEvent:RemoveListener("OnSendExtraTrapInfo", self, "OnSendExtraTrapInfo")
end

function LuaSectEntranceButtonWnd:OnLeaveZongMen()
	if self.m_TaskSectTrapInfo then return end
	CUIManager.CloseUI("SectEntranceButtonWnd")
end

function LuaSectEntranceButtonWnd:UpdateButtonPos(info, buttonWorldPos, btnAnchor)
	if not info then
		btnAnchor.transform.position = Vector3(0,3000,0)
		return
	end
	local viewPos = CMainCamera.Main:WorldToViewportPoint(buttonWorldPos)
	local isInView = viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
	local screenPos = isInView and CMainCamera.Main:WorldToScreenPoint(buttonWorldPos) or Vector3(0,3000,0)
	screenPos.z = 0
	local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
	btnAnchor.transform.position = worldPos
end

function LuaSectEntranceButtonWnd:OnSendExtraTrapInfo()
	local sectTrapInfoList = CRenderScene.Inst:GetExtraSectTrapInfo()
	self.m_DefaultSectTrapInfo,self.m_TaskSectTrapInfo = nil,nil
	for i = 0, sectTrapInfoList.Count - 1 do
		local info = sectTrapInfoList[i]
		if info.trapType == SectEntranceTrapType.Default then
			self.m_DefaultSectTrapInfo = info
			local fazhenWorldPos = Utility.GridPos2WorldPos(self.m_DefaultSectTrapInfo.pos.x, self.m_DefaultSectTrapInfo.pos.y)
			self.m_DefaultButtonWorldPos = Vector3(fazhenWorldPos.x, fazhenWorldPos.y + Menpai_Setting.GetData("SectEntranceButtonHeight").Value, fazhenWorldPos.z)
		elseif info.trapType == SectEntranceTrapType.SchoolSectTask then
			self.m_TaskSectTrapInfo = info
			local fazhenWorldPos = Utility.GridPos2WorldPos(self.m_TaskSectTrapInfo.pos.x, self.m_TaskSectTrapInfo.pos.y)
			self.m_TaskButtonWorldPos = Vector3(fazhenWorldPos.x, fazhenWorldPos.y + Menpai_Setting.GetData("SectEntranceButtonHeight").Value - 0.5, fazhenWorldPos.z)
		end
	end
	if not self.m_DefaultSectTrapInfo or not self.m_TaskSectTrapInfo then
		CUIManager.CloseUI("SectEntranceButtonWnd");
	end
end
--@region UIEvent

function LuaSectEntranceButtonWnd:OnSelfBtnClick()
	Gac2Gas.RequestTrackToSectEntrance()
end

function LuaSectEntranceButtonWnd:OnTaskBtnClick()
	Gac2Gas.RequestEnterFakeSectScene(LuaZongMenMgr.mAddFakeEntranceSectId)
end
--@endregion UIEvent

