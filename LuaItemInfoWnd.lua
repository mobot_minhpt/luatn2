local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local Utility = import "L10.Engine.Utility"
local UIWidget = import "UIWidget"
local UITable = import "UITable"
local UIRoot = import "UIRoot"
local UIRect = import "UIRect"
local UInt64StringKeyValuePair = import "L10.Game.UInt64StringKeyValuePair"
local StringBuilder = import "System.Text.StringBuilder"
local String = import "System.String"
local Screen = import "UnityEngine.Screen"
local RegexOptions = import "System.Text.RegularExpressions.RegexOptions"
local Regex = import "System.Text.RegularExpressions.Regex"
local PackageItemInfo = import "L10.UI.CItemInfoMgr+PackageItemInfo"
local Object = import "System.Object"
local NGUITools = import "NGUITools"
local NGUIText = import "NGUIText"
local NGUIMath = import "NGUIMath"
local MsgPackImpl = import "MsgPackImpl"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local L10 = import "L10"
local Item_Item = import "L10.Game.Item_Item"
local HouseConst = import "L10.Game.HouseConst"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Extensions = import "Extensions"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EnumNumber = import "L10.Game.EnumNumber"
local EnumMapiLingfuPos = import "L10.Game.EnumMapiLingfuPos"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumItemFlag = import "L10.Game.EnumItemFlag"
local EnumFreightEquipmentType = import "L10.Game.EnumFreightEquipmentType"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CZuoQiEquipProp = import "L10.Game.CZuoQiEquipProp"
local CUITexture = import "L10.UI.CUITexture"
local CUIManager = import "L10.UI.CUIManager"
local CTooltip = import "L10.UI.CTooltip"
local CTitleDescTemplate = import "L10.UI.CTitleDescTemplate"
local CSpringFestivalMgr = import "L10.Game.CSpringFestivalMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSecondTitleTextInfoRow = import "L10.UI.CSecondTitleTextInfoRow"
local CQishuFurnitureTemplate = import "L10.UI.CQishuFurnitureTemplate"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQianKunDaiMgr = import "L10.Game.CQianKunDaiMgr"
local CPresentMgr = import "L10.Game.CPresentMgr"
local Constants = import "L10.Game.Constants"
local Color = import "UnityEngine.Color"
local CMapiView = import "L10.UI.CMapiView"
local CMapiProp = import "L10.Game.CMapiProp"
local CMapiLingfuItemData = import "L10.Game.CMapiLingfuItemData"
local CMapiBabyInfo = import "L10.Game.CMapiBabyInfo"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CLingfuView = import "L10.UI.CLingfuView"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAndEquipTipButton = import "L10.UI.CItemAndEquipTipButton"
local CItem = import "L10.Game.CItem"
local CHouseDataForItemDisplay = import "L10.Game.CHouseDataForItemDisplay"
local CFirstTitleTextWithSpriteRow = import "L10.UI.CFirstTitleTextWithSpriteRow"
local CFirstTitleTextInfoRow = import "L10.UI.CFirstTitleTextInfoRow"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipment = import "L10.Game.CEquipment"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CCJBMgr = import "L10.Game.CCJBMgr"
local CChuanjiabaoAppearanceType = import "L10.Game.CChuanjiabaoAppearanceType"
local CChatLinkMgr = import "CChatLinkMgr"
local CBaseItemInfoRow = import "L10.UI.CBaseItemInfoRow"
local BodyItemInfo = import "L10.UI.CItemInfoMgr+BodyItemInfo"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local DateTime=import "System.DateTime"
local CChuanjiabaoWordType = import "L10.Game.CChuanjiabaoWordType"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local DateTimeKind = import "System.DateTimeKind"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"

CLuaItemInfoWnd = class()
RegistClassMember(CLuaItemInfoWnd,"background")
RegistClassMember(CLuaItemInfoWnd,"table")
RegistClassMember(CLuaItemInfoWnd,"iconTexture")
RegistClassMember(CLuaItemInfoWnd,"nameLabel")
RegistClassMember(CLuaItemInfoWnd,"availableLevelLabel")
RegistClassMember(CLuaItemInfoWnd,"categoryLabel")
RegistClassMember(CLuaItemInfoWnd,"qualitySprite")
RegistClassMember(CLuaItemInfoWnd,"bindSprite")
RegistClassMember(CLuaItemInfoWnd,"disableSprite")
RegistClassMember(CLuaItemInfoWnd,"infoBtn")
RegistClassMember(CLuaItemInfoWnd,"detailBtn")
RegistClassMember(CLuaItemInfoWnd,"labelTemplate")
RegistClassMember(CLuaItemInfoWnd,"contentTable")
RegistClassMember(CLuaItemInfoWnd,"contentScrollView")
RegistClassMember(CLuaItemInfoWnd,"eyeBtn")
RegistClassMember(CLuaItemInfoWnd,"minContentWidth")
RegistClassMember(CLuaItemInfoWnd,"maxContentWidth")
RegistClassMember(CLuaItemInfoWnd,"NameColor")
RegistClassMember(CLuaItemInfoWnd,"ValueColor")
RegistClassMember(CLuaItemInfoWnd,"buttonTable")
RegistClassMember(CLuaItemInfoWnd,"buttonTemplate")
RegistClassMember(CLuaItemInfoWnd,"buttonsBg")
RegistClassMember(CLuaItemInfoWnd,"firstTitleDescTempalte")
RegistClassMember(CLuaItemInfoWnd,"secondTitleDescTemplate")
RegistClassMember(CLuaItemInfoWnd,"starTemplate")
RegistClassMember(CLuaItemInfoWnd,"wordTemplate")
RegistClassMember(CLuaItemInfoWnd,"spaceTemplate")
RegistClassMember(CLuaItemInfoWnd,"labelTemplate2")
RegistClassMember(CLuaItemInfoWnd,"firstTitleDescWithSpriteTemplate")
RegistClassMember(CLuaItemInfoWnd,"skillTemplate")
RegistClassMember(CLuaItemInfoWnd,"skillIconTemplate")
RegistClassMember(CLuaItemInfoWnd,"highlightStar")
RegistClassMember(CLuaItemInfoWnd,"normalStar")
RegistClassMember(CLuaItemInfoWnd,"baseItemInfo")
RegistClassMember(CLuaItemInfoWnd,"itemName")
RegistClassMember(CLuaItemInfoWnd,"showInfoButton")
RegistClassMember(CLuaItemInfoWnd,"showInfoAction")
RegistClassMember(CLuaItemInfoWnd,"eyeInfoAction")
RegistClassMember(CLuaItemInfoWnd,"s_HiddenPeriodIfExpiredTimeZero")
RegistClassMember(CLuaItemInfoWnd,"m_QueryPresentItemDataReturn")

RegistClassMember(CLuaItemInfoWnd,"m_Wnd")
RegistClassMember(CLuaItemInfoWnd,"m_IsFurnitureLimitStatusRequested")
RegistClassMember(CLuaItemInfoWnd,"m_ItemTipCallbacks")
RegistClassMember(CLuaItemInfoWnd,"m_PaperDurationLimit")
RegistClassMember(CLuaItemInfoWnd,"m_ItemId")
RegistClassMember(CLuaItemInfoWnd,"m_ChuanjiabaoArtistResultProcessed")--只处理一次


CLuaItemInfoWnd.s_EnableReuseButtons = true
CLuaItemInfoWnd.s_FromCheckGemGroupWnd = false--从镶嵌石之灵界面打开，需要判断第二套宝石

function CLuaItemInfoWnd:Awake()
    self.m_ItemTipCallbacks={}
    self.m_ItemTipCallbacks[21004937]=self.ShowYiRongDan--剑心琉璃易容丹
    self.m_ItemTipCallbacks[21102079]=self.ShowYiRongDan_Common
    self.m_ItemTipCallbacks[21102080]=self.ShowYiRongDan_Common
    self.m_ItemTipCallbacks[21102081]=self.ShowYiRongDan_Common
    self.m_ItemTipCallbacks[21102082]=self.ShowYiRongDan_Common

    self.m_IsFurnitureLimitStatusRequested = false
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.background = self.transform:Find("Table/Background"):GetComponent(typeof(UISprite))
    self.table = self.transform:Find("Table"):GetComponent(typeof(UITable))
    self.iconTexture = self.transform:Find("Table/Background/Header/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
    self.nameLabel = self.transform:Find("Table/Background/Header/ItemNameLabel"):GetComponent(typeof(UILabel))
    self.availableLevelLabel = self.transform:Find("Table/Background/Header/AvailableLevelLabel"):GetComponent(typeof(UILabel))
    self.categoryLabel = self.transform:Find("Table/Background/Header/CategoryLabel"):GetComponent(typeof(UILabel))
    self.qualitySprite = self.transform:Find("Table/Background/Header/ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
    self.bindSprite = self.transform:Find("Table/Background/Header/ItemCell/BindSprite"):GetComponent(typeof(UISprite))
    self.disableSprite = self.transform:Find("Table/Background/Header/ItemCell/DisableSprite"):GetComponent(typeof(UISprite))
    self.infoBtn = self.transform:Find("Table/Background/Header/InfoButton").gameObject
    self.detailBtn = self.transform:Find("Table/Background/Header/DetailButton").gameObject
    self.labelTemplate = self.transform:Find("Table/Background/Templates/LabelTemplate").gameObject
    self.contentTable = self.transform:Find("Table/Background/ContentScrollView/Table"):GetComponent(typeof(UITable))
    self.contentScrollView = self.transform:Find("Table/Background/ContentScrollView"):GetComponent(typeof(UIScrollView))
    self.eyeBtn = self.transform:Find("Table/Background/ContentScrollView/EyeButton").gameObject
    self.minContentWidth = 512
    self.maxContentWidth = 600
    self.NameColor = NGUIText.ParseColor24("FFED5F",0)
    self.ValueColor = NGUIText.ParseColor24("E8D0AA",0)
    self.buttonTable = self.transform:Find("Table/Background/Buttons"):GetComponent(typeof(UITable))
    self.buttonTemplate = self.transform:Find("Table/Background/Templates/ButtonTemplate").gameObject
    self.buttonsBg = self.transform:Find("Table/Background/ButtonsBg"):GetComponent(typeof(UIWidget))
    self.firstTitleDescTempalte = self.transform:Find("Table/Background/Templates/FirstTitleDescTemplate").gameObject
    self.secondTitleDescTemplate = self.transform:Find("Table/Background/Templates/SecondTitleDescTemplate").gameObject
    self.starTemplate = self.transform:Find("Table/Background/Templates/StarTemplate").gameObject
    self.wordTemplate = self.transform:Find("Table/Background/Templates/WordTemplate").gameObject
    self.spaceTemplate = self.transform:Find("Table/Background/Templates/SpaceTemplate").gameObject
    self.labelTemplate2 = self.transform:Find("Table/Background/Templates/LabelTemplate2").gameObject
    self.firstTitleDescWithSpriteTemplate = self.transform:Find("Table/Background/Templates/FirstTitleDescWithSpriteTemplate").gameObject
    self.skillTemplate = self.transform:Find("Table/Background/Templates/SkillTemplate").gameObject
    self.skillIconTemplate = self.transform:Find("Table/Background/Templates/SkillTemplate/Skill").gameObject
    self.m_PaperDurationLimit = self.transform:Find("Table/Background/Header/ItemCell/PaperDuration"):GetComponent(typeof(UILabel))
    self.highlightStar = "playerAchievementView_star_1"
    self.normalStar = "playerAchievementView_star_2"
    self.baseItemInfo = nil
    self.itemName = nil
    self.showInfoButton = false
    self.showInfoAction = nil
    self.eyeInfoAction = nil
    self.s_HiddenPeriodIfExpiredTimeZero = true
    self.m_QueryPresentItemDataReturn = false

    UIEventListener.Get(self.infoBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnInfoButtonClick(go)
    end)
    UIEventListener.Get(self.eyeBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        self:OnEyeButtonClicked(go)
    end)

end

function CLuaItemInfoWnd:Init( )

    self.m_QueryPresentItemDataReturn = false

    self.baseItemInfo = nil
    self.infoBtn:SetActive(false)
    self.eyeBtn:SetActive(false)
    self.detailBtn:SetActive(false)
    self.m_PaperDurationLimit.gameObject:SetActive(false)
    self.itemName = nil
    self.showInfoButton = false
    self.showInfoAction = nil
    self.eyeInfoAction = nil
    local curItemTemplate = nil
    repeat
        local default = CItemInfoMgr.showType
        if default == CItemInfoMgr.ShowType.Package then
            do
                local item = CItemMgr.Inst:GetById(CItemInfoMgr.packageItemInfo.itemId)
                if item ~= nil then
                    self.baseItemInfo = CItemInfoMgr.packageItemInfo
                    self:Init0(item)
                    curItemTemplate = Item_Item.GetData(item.TemplateId)
                end
                break
            end
        elseif default == CItemInfoMgr.ShowType.Body then
            do
                local item = CItemMgr.Inst:GetById(CItemInfoMgr.bodyItemInfo.itemId)
                if item ~= nil then
                    self.baseItemInfo = CItemInfoMgr.bodyItemInfo
                    self:Init0(item)
                    curItemTemplate = Item_Item.GetData(item.TemplateId)
                end
                break
            end
        elseif default == CItemInfoMgr.ShowType.Link then
            do
                local item = CItemInfoMgr.linkItemInfo.item
                if item ~= nil then
                    self.baseItemInfo = CItemInfoMgr.linkItemInfo
                    self:Init0(item)
                    curItemTemplate = Item_Item.GetData(item.TemplateId)
                else
                    local template = CItemMgr.Inst:GetItemTemplate(CItemInfoMgr.linkItemInfo.templateId)
                    if template ~= nil then
                        self.baseItemInfo = CItemInfoMgr.linkItemInfo
                        self:Init1(template, false, CItemInfoMgr.linkItemInfo.isBind)
                        curItemTemplate = template
                    end
                end
                break
            end
        elseif default == CItemInfoMgr.ShowType.Signin then
            do
                local template = CItemMgr.Inst:GetItemTemplate(CItemInfoMgr.linkItemInfo.templateId)
                if template ~= nil then
                    self.baseItemInfo = CItemInfoMgr.linkItemInfo
                    self:Init1(template, true, false)
                    curItemTemplate = template
                end
                break
            end
        elseif default == CItemInfoMgr.ShowType.FreightEquip then
            do
                self.baseItemInfo = CItemInfoMgr.linkItemInfo
                self:Init2(CItemInfoMgr.freightEquipInfo)
                break
            end
        elseif default == CItemInfoMgr.ShowType.FurnitureStore then
            do
                local itemid = CQishuFurnitureTemplate.CurFurnitureItemId
                if itemid ~= nil then
                    local item = CItemMgr.Inst:GetById(itemid)
                    if item ~= nil then
                        self.baseItemInfo = CItemInfoMgr.linkItemInfo
                        self:Init0(item)
                        curItemTemplate = Item_Item.GetData(item.TemplateId)
                    end
                end
                break
            end
        elseif default == CItemInfoMgr.ShowType.FurnitureRepository then
            do
                self.baseItemInfo = CItemInfoMgr.linkItemInfo
                local FurnitureTemplateId = CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId
                self:Init4RepositoryFurniture(FurnitureTemplateId)
                break
            end
        elseif default == CItemInfoMgr.ShowType.ChuanjiabaoRepository then
            do
                local chuanjiabaoId = CLuaFurnitureSmallTypeItem.CurShowChuanjiabaoItemId
                if chuanjiabaoId ~= nil then
                    local item = CItemMgr.Inst:GetById(chuanjiabaoId)
                    if item ~= nil then
                        self.baseItemInfo = CItemInfoMgr.linkItemInfo
                        self:Init0(item)
                        curItemTemplate = Item_Item.GetData(item.TemplateId)
                    end
                end
                break
            end
        elseif default == CItemInfoMgr.ShowType.QianKunDai then
            do
                local item = CItemInfoMgr.linkItemInfo.item
                if item ~= nil then
                    self.baseItemInfo = CItemInfoMgr.linkItemInfo
                    self:Init0(item)
                    curItemTemplate = Item_Item.GetData(item.TemplateId)
                elseif HouseFish_AllFishes.GetData(CItemInfoMgr.linkItemInfo.templateId) then
                    local template = CItemMgr.Inst:GetItemTemplate(CItemInfoMgr.linkItemInfo.templateId)
                    if template ~= nil then
                        self.baseItemInfo = CItemInfoMgr.linkItemInfo
                        self:Init1(template, false, true)
                        curItemTemplate = template
                    end
                end
                break
            end
        elseif default == CItemInfoMgr.ShowType.MajiuMapi then
            do
                self.baseItemInfo = CItemInfoMgr.linkItemInfo
                self:Init4MajiuMapi()
            end
            break
        elseif default == CItemInfoMgr.ShowType.CityWarUnit then
            do
                self.baseItemInfo = CItemInfoMgr.linkItemInfo
                local unitTemplateId = CCityWarMgr.Inst.CurShowUnitTemplateId;
                self:Init4CityWarUnit(unitTemplateId)
                break
            end
            break
        else
            break
        end
    until 1
    local extern
    if curItemTemplate ~= nil then
        extern = curItemTemplate.Name
    else
        extern = ""
    end
    self.itemName = extern
    self.infoBtn:SetActive((curItemTemplate ~= nil and curItemTemplate.Type == EnumItemType_lua.Gem and GameSetting_Common_Wapper.Inst.JingLingEnabled) or self.showInfoButton)
end
function CLuaItemInfoWnd:OnDetailButtonClick( itemid, appearInfo)
    CCJBMgr.Inst:OpenTipShowByData(itemid, appearInfo, false)
end
function CLuaItemInfoWnd:Update( )
    self.m_Wnd:ClickThroughToClose()
end
function CLuaItemInfoWnd:ItemCanUseLevelExceedPlayerMaxLevel( canUseLevel)
    if CClientMainPlayer.Inst == nil then
        return false
    end
    if CClientMainPlayer.Inst.HasFeiSheng then
        if canUseLevel > GameSetting_Common_Wapper.Inst.CurrentPlayerMaxFeiShengGrade then
            return true
        end
    else
        if canUseLevel > GameSetting_Common_Wapper.Inst.CurrentPlayerMaxGrade then
            return true
        end
    end
    return false
end
function CLuaItemInfoWnd:Init0( item)
    if not item.IsItem then
        self.m_Wnd:Close()
        return
    end
    self.m_ItemId = item.Id

    self.iconTexture:Clear()
    self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    self.bindSprite.spriteName = item.BindOrEquipCornerMark
    self.disableSprite.enabled = not item.MainPlayerIsFit
    local itemTemplate = Item_Item.GetData(item.TemplateId)
    if itemTemplate ~= nil then
        --iconTexture.LoadMaterial(itemTemplate.Icon);
        self.iconTexture:LoadMaterial(item.Icon)
        self.nameLabel.text = itemTemplate.Name
        if item:FlagIsSet(EnumItemFlag.NameInExtraVarData) then
            self.nameLabel.text = item.ColoredName
        end

        self.nameLabel.color = item.Item.Color
        --sftodo
        -- self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplate, nil, false)
        self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplate.NameColor)
        --等级
        local level = 0
        if CClientMainPlayer.Inst ~= nil then
            level = CClientMainPlayer.Inst.Level
        end
        if item.Grade <= 1 then
            self.availableLevelLabel.text = nil
        elseif item.Grade > level then
            if self:ItemCanUseLevelExceedPlayerMaxLevel(item.Grade) then
                self.availableLevelLabel.text = LocalString.GetString("[c][FF0000]不可用[-][/c]")
            else
                self.availableLevelLabel.text = System.String.Format(LocalString.GetString("[c][FF0000]{0}级[-][/c]"), item.Grade)
            end
        else
            self.availableLevelLabel.text = System.String.Format(LocalString.GetString("[c][FFFFFF]{0}级[-][/c]"), item.Grade)
        end

        --类型
        self.categoryLabel.text = nil
        local data = Item_Type.GetData(itemTemplate.Type)
        if data ~= nil then
            --TODO 需要加入对等级、职业、性别限制的检查，策划表中目前还没有数据
            self.categoryLabel.text = System.String.Format("[c][FFFFFF]{0}[-][/c]", data.Name)
        end
    end

    --下面这些分支尽量不要再加啦，不好维护！！！
    if item.Item.Type == EnumItemType_lua.Chuanjiabao then
        self:InitChuanJiaBao(item)
        return
    elseif item.Item.Type == EnumItemType_lua.EvaluatedZhushabi then
        self:InitEvaluatedZhushabi(item)
        return
    elseif item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao then
        self:InitUnevaluateChuanjiabao(item)
        return
    elseif item.TemplateId == CQingQiuMgr.Inst.m_DivinationItemId then
        self:InitDivinationItem(item, -1, -1, 0)
        return
    elseif item.TemplateId == GameSetting_Common.GetData().SoulItemId then
        self:InitSoulItem(item)
        return
    elseif item.TemplateId == GameSetting_Common.GetData().DogTagItemTempId or item.TemplateId == GameSetting_Common.GetData().AdvancedDogTagId then
        self:InitDogTagItem(item.TemplateId, item.Item.ExtraVarData.Data)
        return
    elseif CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) > 0 then
        self:InitLingfuItem(Item_Item.GetData(item.TemplateId), item.Item.LingfuItemInfo)
        return
    elseif CZuoQiMgr.IsMaPi(item.TemplateId) then
        self:InitMapiItem(item)
        return
    end
    --上面这些分支尽量不要再加啦，不好维护！！！

    Extensions.RemoveAllChildren(self.contentTable.transform)

    if item.Item.Type == EnumItemType_lua.Map then
        if item.Item.ExtraData.Data ~= nil and item.Item.ExtraData.Data.varbinary ~= nil then
            local id = item.Item.ExtraData.Data.varbinary.StringData
            if not System.String.IsNullOrEmpty(id) then
                local pos = WaBao_Place.GetData(System.UInt32.Parse(id)).Point
                local coord = System.String.Format("{0}({1},{2})", PublicMap_PublicMap.GetData(pos[0]).Name, pos[1], pos[2])
                self:AddItemToTable(coord, Color.green)
                --self.AddItemToTable("", Color.white);
            end
        end
    end

    local descBuilder = NewStringBuilderWraper(StringBuilder)
    local fishData = HouseFish_AllFishes.GetData(item.TemplateId)
    if fishData then
        descBuilder:Append(System.String.Format(item.Description, fishData.Score))
    elseif item.Item.Type == EnumItemType_lua.Zhuangshiwu then
        local zswdata = CClientFurnitureMgr.Inst:GetChuanjiabaoInZhuangshiwuData(item.TemplateId)
        if zswdata ~= nil and zswdata.Repair > 0 then
            descBuilder:Append(System.String.Format(item.Description, zswdata.Repair))
        else
            descBuilder:Append(item.Description)
        end
    elseif item.Item.Type == EnumItemType_lua.Mount then
        descBuilder:Append(CChatLinkMgr.TranslateToNGUIText(CItem.GetZuoQiDescription(item.Item.TemplateId,true)))
    elseif LuaChunJie2022Mgr.IsHeKa(item.TemplateId) then
        local data = MsgPackImpl.unpack(item.Item.ExtraVarData.Data)
        if data and data.Count >= 6 then
            local name = tostring(data[2])
            descBuilder:Append(g_MessageMgr:FormatMessage("ChunJie2022_HeKaDesc",name))
        end
    else
        descBuilder:Append(item.Description)
    end
    --判断是否为限制使用物品
    if item.TimesGroup > 0 and item.TimesPerDay > 0 and CClientMainPlayer.Inst ~= nil then
        local useTimes = CClientMainPlayer.Inst.ItemProp:GetItemUseTimes(item.TimesGroup, item.ItemPeriod)
        local remainTimes = item.TimesPerDay - useTimes


        if itemTemplate.TimesPerDayParam > 0 then
            local timesPerDayLimit = item.Item:GetTimesPerDayWithParam(itemTemplate.TimesPerDayParam)
            remainTimes = timesPerDayLimit - useTimes
        end


        if remainTimes < 0 then
            remainTimes = 0
        end
        local period = CClientMainPlayer.Inst.ItemProp:GetPeriodName(item.ItemPeriod)

        if item.ItemUseTimeDescription ~= nil then
            local timesPerDay = System.String.Format(item.ItemUseTimeDescription, period, remainTimes)
            descBuilder:Append("\n")
            descBuilder:Append(timesPerDay)
        end
    end

    --家园装饰物
    self:CheckAddFurnitureInfo(item)
    self:CheckAddSeaFishingWords(item)

    self:AddItemToTable(ToStringWrap(descBuilder), self.ValueColor)

    self:CheckAndAddLiuYiItemInfo(item)

    self:CheckAndAddBiWuTeamMemberInfo(item)

    self:CheckAndAddPresentInfo(item)

    self:CheckAndAddDuoHunFanInfo(item)

    self:CheckAndAddHunPoInfo(item)

    --抢亲道具
    self:CheckAndAddQiangQinInfo(item)

    self:CheckAndAddDiqiInfo(item)
    --海钓鱼饵的处理
    self:SeaFishingFoodExtraDes(item.TemplateId)

    self:ProcessActionTimes(item)

    if item.Item.Type == EnumItemType_lua.PuzzleMap then
        if item.Item.ExtraData.Data ~= nil and item.Item.ExtraData.Data.varbinary ~= nil then
            local id = item.Item.ExtraData.Data.varbinary.StringData
            if not System.String.IsNullOrEmpty(id) then
                local puzzlePlace = WaBao_PuzzlePlace.GetData(System.UInt32.Parse(id))
                --uint[] pos = puzzlePlace.Point;
                --string coord = string.Format("{0}({1},{2})", PublicMap_PublicMap.GetData(pos[0]).Name, pos[1], pos[2]);
                --self.AddItemToTable(coord, Color.green);
                --self.AddItemToTable("", Color.white);
                self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(puzzlePlace.Puzzle, false), self.ValueColor)
            end
        end
    end
    if CSpringFestivalMgr.Instance:IsXiaoji(item.TemplateId) then
        local count = math.floor(tonumber(item.Item.ExtraVarData.StringData or 0))
        count = math.floor(math.min(count, ChunJie_Setting.GetData().JieZaiNeedHuoLi))
        local str = System.String.Format(LocalString.GetString("成长度：{0}/{1}"), count, ChunJie_Setting.GetData().JieZaiNeedHuoLi)
        self:AddItemToTable(str, self.ValueColor)
    end
    if CSpringFestivalMgr.Instance:IsJingnang(item.TemplateId) then
        local str = g_MessageMgr:FormatMessage("SPRINGFESTIVAL_WISH_CONTENT", item.Item.ExtraVarData.StringData)
        str = CChatLinkMgr.TranslateToNGUIText(str, false)
        self:AddItemToTable(str, Color.white)
    end

    if item.TemplateId == GameSetting_Common.GetData().ShanHaiJingItemId then
        if item.Item.ExtraData.Data ~= nil and item.Item.ExtraData.Data.varbinary ~= nil then
            local data = item.Item.ExtraData.Data.varbinary.StringData

            if not System.String.IsNullOrEmpty(data) then
                local dataArray = CommonDefs.StringSplit_ArrayChar(data, ",")
                if dataArray.Length >= 2 then
                    local id = System.Int32.Parse(dataArray[0])
                    local descName = dataArray[1]
                    local itemPlace = ShiTu_ShanHaiJingItemPlace.GetData(id)
                    if descName == "s" then
                        self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(itemPlace.ShiFu, false), self.ValueColor)
                    elseif descName == "t" then
                        self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(itemPlace.TuDi, false), self.ValueColor)
                    end
                end
            end
        end
    end

    if item.TemplateId == YuanXiao_Setting.GetData().RiddleLanternItemId then
        local raw = item.Item.ExtraVarData.Data
        if raw ~= nil then
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            local itemId = 0
            local answer = ""
            local playerName = ""
            if list ~= nil and list.Count >= 4 then
                itemId = math.floor(tonumber(list[0] or 0))
                answer = tostring(list[1])
                playerName = tostring(list[2])
            end

            local info = YuanXiao_QuestionPool2.GetData(itemId)
            if info ~= nil then
                self:AddItemToTable(LocalString.GetString("灯谜:") .. info.Question, Color.white)
                if not System.String.IsNullOrEmpty(answer) then
                    self:AddItemToTable(LocalString.GetString("答案:") .. answer, Color.white)
                end
                if not System.String.IsNullOrEmpty(playerName) then
                    self:AddItemToTable(System.String.Format(LocalString.GetString("({0}已答对)"), playerName), Color.white)
                end
            end
        end
    end


    --如果是易容丹
    local ret,callback=self:GetItemTipCallback(item.TemplateId)
    if ret then
        callback(self,item)
    elseif CLingShouMgr.Inst.EngageItemId == item.TemplateId then
        self:ShowLingShouEngageInfo(item)
    elseif Constants.BabyFangShengExpItemId == item.TemplateId then
        self:ShowBabyFangShengExp(item)
    end

    if item.Item.Type == EnumItemType_lua.Book or item.Item.Type == EnumItemType_lua.Jueji then
        local book = CItemMgr.Inst:GetItemTemplate(item.Item.TemplateId)
        local raceCheckList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, Int32)), Int32, book.RaceCheck)
        if not CommonDefs.ListContains(raceCheckList, typeof(Int32), CClientMainPlayer.Inst.BasicProp.Class) then
            self:AddItemToTable(LocalString.GetString("(不可领悟)"), Color.red)
        else
            local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
            CommonDefs.DictIterate(skillBooks, DelegateFactory.Action_object_object(function (___key, ___value)
                local pair = {}
                pair.Key = ___key
                pair.Value = ___value
                if item.Item.TemplateId == pair.Value.Key then
                    if not CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(pair.Key) then
                        self:AddItemToTable(LocalString.GetString("(未领悟)"), Color.red)
                    else
                        self:AddItemToTable(LocalString.GetString("(已领悟)"), Color.green)
                    end
                    return
                end
            end))
        end
    end

    self:CheckAndAddPreciousProtectTimeInfo(item)
    self:CheckAndAddMinTradePrice(item)
    self:CheckChristmasRecievedCard(item)
    self:CheckShuangshiyi2020Vouchers(item)
    self:CheckKunxiansuoItem(item)

    --#region 持有时间
    if item.Item.ExtraVarData.Data ~= nil and item.Item.ExtraVarData.StringData ~= nil then
        local raw = item.Item.ExtraVarData.StringData
        if not System.String.IsNullOrEmpty(raw) then
            local extraVarData = TypeAs(L10.Game.Utils.Json.Deserialize(raw), typeof(MakeGenericClass(Dictionary, String, Object)))
            if extraVarData ~= nil and CommonDefs.DictContains(extraVarData, typeof(String), "ObtainTime") then
                local obtainTime = math.floor(tonumber(CommonDefs.DictGetValue(extraVarData, typeof(String), "ObtainTime") or 0))
                local span = CServerTimeMgr.Inst:GetZone8Time():Subtract(CServerTimeMgr.ConvertTimeStampToZone8Time(obtainTime))
                if span.TotalSeconds > 0 then
                    local totalSeconds = math.floor(span.TotalSeconds)
                    local hours = math.floor(totalSeconds / 3600)
                    local minutes = math.floor(totalSeconds % 3600 / 60)
                    local seconds = totalSeconds % 60
                    self:AddItemToTable("", Color.white)
                    local timeStr = System.String.Format(LocalString.GetString("{0}小时{1}分{2}秒"), hours, minutes, seconds)
                    if not System.String.IsNullOrEmpty(itemTemplate.ExtraAttribute) then
                        self:AddItemToTable(g_MessageMgr:Format(itemTemplate.ExtraAttribute, timeStr), Color.yellow)
                    else
                        self:AddItemToTable(System.String.Format(LocalString.GetString("已持有时间: {0}"), timeStr), Color.yellow)
                    end
                end
            end
        end
    end
    --#endregion

    self:CheckAndAddFeiShengInfo(item, itemTemplate)

    --靓号ID
    self:CheckAndAddLiangHaoId(item, itemTemplate)

    --经验丹书道具增加经验衰减说明
    self:CheckAndAddExpBookDiscountInfo(item, itemTemplate)

    --#region 有效期
    self:CheckAndAddPeriod(item, itemTemplate)
    --#endregion

    if item.TemplateId == Constants.HuoGuoQueueItemId then
        self:InitHuoGuoQueueItem(item)
    end

    if CItemInfoMgr.showType == CItemInfoMgr.ShowType.QianKunDai and CQianKunDaiMgr.Inst:IsQianKunDaiJiFenItem(item.TemplateId) then
        local desc = CQianKunDaiMgr.Inst:GetQianKunDaiItemDesc(item.TemplateId)
        self:AddItemToTable(desc, Color.yellow)
    end

    if item.TemplateId == ZhouNianQing_Setting.GetData().ZhaoHuiJiangZhangId then
        if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
            local raw = item.Item.ExtraVarData.Data
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            if list.Count == 2 then
                local playerId = tonumber(list[0])
                local name = tostring(list[1])

                local desc = LocalString.GetString("召回人：") .. name
                self:AddItemToTable(desc, Color.green)
            end
        end
    end

    --城战战龙运输箱
    if item.TemplateId == CLuaCityWarMgr.ZhanLongGrabMaterialItemTemplateId then
        local cnt = item.Item.ExtraVarData.StringData and item.Item.ExtraVarData.StringData or "0"
        local desc = LocalString.GetString("资材数量：") ..cnt
        self:AddItemToTable(desc, Color.yellow)
    end
    --城战战龙战旗
    if item.TemplateId == CLuaCityWarMgr.ZhanLongFlagTemplateId then
        local content = item.Item.ExtraVarData.StringData and item.Item.ExtraVarData.StringData or LocalString.GetString("无")
        self:AddItemToTable(LocalString.GetString("标语：")..content, Color.yellow)
    end


    -- 五色丝
    local dwdata = DuanWu_Setting.GetData()
    if item.TemplateId == dwdata.WuSeSiTemplateId then
		if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
			local raw = item.Item.ExtraVarData.Data
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            if list.Count == 2 then
                local playerId = tonumber(list[0])
                local name = tostring(list[1])
				local desc = System.String.Format(LocalString.GetString(dwdata.DisplayTxt), name)
                self:AddItemToTable(desc, self.ValueColor)
            elseif list.Count == 1 then
                local playerId = tonumber(list[0])
                local desc = System.String.Format(LocalString.GetString(dwdata.DisplayTxt), playerId)
                self:AddItemToTable(desc, self.ValueColor)
			end
		end
	end
    -- 五色丝2020
    local dw2020 = DuanWu2020_Setting.GetData()
    if item.TemplateId == dw2020.WuSeSiTemplateId then
        if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
            local raw = item.Item.ExtraVarData.Data
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            if list.Count == 2 then
                local playerId = list[0]
                local name = list[1]
                local desc = SafeStringFormat3(dw2020.DisplayTxt1, name)
                -- local desc = System.String.Format(LocalString.GetString(dw2020.DisplayTxt1), name)
                self:AddItemToTable(desc, self.ValueColor)
            end
        end
    elseif item.TemplateId == dw2020.QingXiWuSeSiTemplateId then
		if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
			local raw = item.Item.ExtraVarData.Data
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            if list.Count == 3 then
                local playerId = list[0]
                local name = list[1]
                local matename = list[2]
                local desc = SafeStringFormat3(dw2020.DisplayTxt2, name, matename)
                self:AddItemToTable(desc, self.ValueColor)
            end
            -- TODO 如果参数不一致应该报错的
		end
    end

    --唱片道具
    self:ProcessMusicItem(item)

    -- 情人保险礼包
    self:ValentineBaoXianLiBao(item)

    self:ProcessTimeLimitItem(item)

    -- 克符道具
    self:ProcessAntiProfessionItem(item)

    --宗门信物显示入门规则
    self:ProcessSectXinWu(item)

    --处理按钮显示
    self:ShowButtons(self.baseItemInfo)

    self:LayoutWnd()
end

function CLuaItemInfoWnd:InitPaperItem(zswId)
    self.m_PaperOutOfDateTime = nil  
    self.m_PaperDurationLimit.gameObject:SetActive(false)
    local templateId = Zhuangshiwu_Zhuangshiwu.GetData(zswId).ItemId
    local paperData = House_WallPaper.GetDataBySubKey("ItemID",templateId)
    if not paperData then         
        return
    end
    if paperData.Duration == -1 then
        return
    end
    local paperId = paperData.Id
    if CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2 then
        local map = CClientHouseMgr.Inst.mCurFurnitureProp2.WallFloorOutOfDateTime
        if CommonDefs.IsDic(map) and CommonDefs.DictTryGet(map,typeof(Byte),paperId,typeof(UInt32)) then
            self.m_PaperDurationLimit.gameObject:SetActive(true)
            local outDateTime = CommonDefs.DictGetValue(map, typeof(Byte), paperId)
            self.m_PaperOutOfDateTime = outDateTime
            local now = CServerTimeMgr.Inst.timeStamp
            local daysRemain = math.ceil((outDateTime - now) / 3600 / 24)
            local color = NGUIText.ParseColor24("58FF23", 0)
            if daysRemain < 0 then
                self.m_PaperDurationLimit.text = LocalString.GetString("过期")
                color = NGUIText.ParseColor24("FF2B2B", 0)
            else
                self.m_PaperDurationLimit.text = SafeStringFormat3(LocalString.GetString("%d天"),daysRemain)
            end
            self.m_PaperDurationLimit.transform:Find("PaperDurationBg"):GetComponent(typeof(UITexture)).color = color
        end
    end
end

function CLuaItemInfoWnd:ProcessActionTimes(item)
    local itemTemplate = Item_Item.GetData(item.TemplateId)
    if itemTemplate.ActionTimes > 0 then
        local actionTimesStr = System.String.Format(LocalString.GetString("剩余次数:[c][00FF00]{0}[-][/c]"), item.Item.RemainTimes)
        self:AddItemToTable(actionTimesStr, self.ValueColor)
    elseif itemTemplate.ID == 21006125 then
        local formulaId = GameplayItem_Setting.GetData().PermanentProp_ReviseLimitFormulaId
        local formula = GetFormula(formulaId)
        if formula then
            if CClientMainPlayer.Inst then
                local fightProp = CClientMainPlayer.Inst.FightProp
                local soulCoreProp = CClientMainPlayer.Inst.SkillProp.SoulCore
                local maxTimes = formula and formula(nil, nil, {soulCoreProp.Level})
                local useTiems = 0
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentCor)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentSta)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentStr)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentInt)
                useTiems = useTiems + fightProp:GetParam(EPlayerFightProp.RevisePermanentAgi)
                local actionTimesStr = System.String.Format(LocalString.GetString("剩余次数:[c][00FF00]{0}[-][/c]"),maxTimes - useTiems)
                self:AddItemToTable(actionTimesStr, self.ValueColor)
            end
        end
    end
end

function CLuaItemInfoWnd:ProcessSectXinWu(item)
    if item then
        if item.TemplateId ~= LuaZongMenMgr:GetXingWuTemplateID() then return end
        local extraData = item.Item.ExtraVarData
        if extraData then
            local arr = MsgPackImpl.unpack(extraData.Data) 
            if arr then
                local ruMenGuiZe = arr
                if ruMenGuiZe.Count > 0 then
                    self:AddItemToTable(LocalString.GetString("入派标准:"), Color.green)
                end
                for i = 0, ruMenGuiZe.Count - 1 do
                    local id = ruMenGuiZe[i]
                    local text = Menpai_JoinStandard.GetData(id).Description
                    self:AddItemToTable(text, Color.green)
                end
            end
        end
    end
end

function CLuaItemInfoWnd:ProcessTimeLimitItem(item)
    if not PlayerShop_TimeLimitListing.Exists(item.TemplateId) then return end
    -- 当前处于可上架期间，余XX天后不可上架
    -- 限时上架物品，当前不处于可上架期间
    local data = PlayerShop_TimeLimitListing.GetData(item.TemplateId)
    local now = CServerTimeMgr.Inst:GetZone8Time()

    local nowTime = math.floor(CServerTimeMgr.Inst.timeStamp)
    local startTime = DateTime.Parse(data.StartTime)
    startTime = startTime:Subtract(CServerTimeMgr.UtcStart).TotalSeconds-CServerTimeMgr.s_ZoneValue*3600

    local endTime = DateTime.Parse(data.EndTime)
    endTime = endTime:Subtract(CServerTimeMgr.UtcStart).TotalSeconds-CServerTimeMgr.s_ZoneValue*3600

    if nowTime<=startTime then
        local diff=startTime-nowTime
        local dayDiff = math.floor(diff/86400)--24*60*60
        if dayDiff>0 then
            self:AddItemToTable(SafeStringFormat3(LocalString.GetString("限时上架物品，请于%d天后上架"),dayDiff), self.ValueColor)
        else
            local hourDiff = math.floor(diff/3600)
            self:AddItemToTable(SafeStringFormat3(LocalString.GetString("限时上架物品，请于%d小时后上架"),hourDiff), self.ValueColor)
        end
    elseif nowTime>=endTime then
        self:AddItemToTable(LocalString.GetString("限时上架物品，当前不处于可上架期间"), self.ValueColor)
    else
        local diff=endTime-nowTime
        local dayDiff = math.floor(diff/86400)
        if dayDiff>0 then
            self:AddItemToTable(SafeStringFormat3(LocalString.GetString("当前处于可上架期间，余%s天后不可上架"),dayDiff), self.ValueColor)
        else
            local hourDiff = math.floor(diff/3600)
            -- if hourDiff>0 then
            self:AddItemToTable(SafeStringFormat3(LocalString.GetString("当前处于可上架期间，余%s小时后不可上架"),hourDiff), self.ValueColor)
            -- end
        end
    end
end

function CLuaItemInfoWnd:ProcessMusicItem(item)
  if item.TemplateId ~= 21030608 then return end

  if not item.Item.ExtraVarData or not item.Item.ExtraVarData.Data then return end
  local list = TypeAs(MsgPackImpl.unpack(item.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
  if list.Count < 4 then return end
  --BG Music
  if list[0] == 0 then
    local musicName = list[1]
    self:AddSpace()
    self:AddItemToTable(SafeStringFormat3(CChatLinkMgr.TranslateToNGUIText(LocalString.GetString("#Y----“%s”")), musicName), self.ValueColor)
    self:AddItemToTable(LocalString.GetString("有效期：剩余")..CLuaMusicBoxMgr:GetMusicLeftTimeStr(item.Item:GetRealExpiredTime()), Color.green)
    self:LayoutWnd()
  else
    local playerId = list[3]
    Gac2Gas.QueryDiskItemCreatorName(playerId)
  end
end

function CLuaItemInfoWnd:ShowBabyFangShengExp( item)
    --返还经验：XXXXXX
    if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
        local raw = item.Item.ExtraVarData.Data
        local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
        if list.Count > 0 then
            local exp = math.floor(tonumber(list[0] or 0))
            local desc = System.String.Format(LocalString.GetString("返还经验：[c][ffff00]{0}[-][/c]"), exp)
            self:AddItemToTable(desc, self.ValueColor)
        end
    end
end
function CLuaItemInfoWnd:ShowLingShouEngageInfo( item)
    local list = TypeAs(MsgPackImpl.unpack(item.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
    if list.Count > 9 then
        local playerName1 = tostring(list[1])
        local lingshouName1 = tostring(list[4])
        local playerName2 = tostring(list[6])
        local lingshouName2 = tostring(list[9])
        self:AddItemToTable(System.String.Format(LocalString.GetString("{0}家的[c][ffff00]{1}[-][/c]"), playerName1, lingshouName1), Color.white)
        self:AddItemToTable(System.String.Format(LocalString.GetString("{0}家的[c][ffff00]{1}[-][/c]"), playerName2, lingshouName2), Color.white)
    end
end

function CLuaItemInfoWnd:InitDivinationItem( item, finishedTime, totalTime, taskId)
    Extensions.RemoveAllChildren(self.contentTable.transform)
    local data = Item_Item.GetData(item.TemplateId)
    if data == nil then
        return
    end

    self:AddItemToTable(CItem.GetItemDescription(item.TemplateId,true), self.ValueColor)

    self:CheckAndAddFeiShengInfo(item, data)

    if item.Item:IsExpire() then
        self:AddItemToTable(LocalString.GetString("\n已过期"), Color.red)
        self:ShowButtons(self.baseItemInfo)
        self:LayoutWnd()
        return
    end
    if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eDivination) then
        local info = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eDivination)
        if info.ExpiredTime < CServerTimeMgr.Inst.timeStamp then
            self:AddItemToTable(LocalString.GetString("\n已过期"), Color.red)
        else
            if finishedTime < 0 then
                Gac2Gas.QueryDivinationLuckyTaskInfo(item.Id)
            else
                local sb = NewStringBuilderWraper(StringBuilder, LocalString.GetString("\n今日幸运玩法：\n"))
                local taskData = Task_Task.GetData(taskId)
                if taskData ~= nil then
                    sb:Append(taskData.Display)
                end
                sb:Append(System.String.Format("({0}/{1})", finishedTime, totalTime))
                self:AddItemToTable(ToStringWrap(sb), self.ValueColor)
            end
            local expiredTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.ExpiredTime)
            self:AddItemToTable(System.String.Format(LocalString.GetString("有效期: {0}"), ToStringWrap(expiredTime, "yyyy-MM-dd HH:mm")), Color.green)
        end
    end

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:InitSoulItem( item)
    Extensions.RemoveAllChildren(self.contentTable.transform)

    local itemData = Item_Item.GetData(item.TemplateId)
    if itemData == nil then
        return
    end
    self:AddItemToTable(item.Description, self.ValueColor)

    if item.Item.ExtraVarData.Data ~= nil then
        local data = TypeAs(MsgPackImpl.unpack(item.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
        if data ~= nil and data.Count >= 8 then
            local equipTemplateId = math.floor(tonumber(data[6] or 0))
            local isFake = false

            if data.Count >= 9 then
                isFake = CommonDefs.Convert_ToBoolean(data[8])
            end
            local equipColor = CEquipment.GetColor(equipTemplateId)

            local equipName = LocalString.GetString("空")
            local equip = EquipmentTemplate_Equip.GetData(equipTemplateId)
            if equip ~= nil then
                if isFake and not System.String.IsNullOrEmpty(equip.AliasName) then
                    equipName = equip.AliasName
                else
                    equipName = equip.Name
                end
            end
            equipName = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(equipColor), equipName)
            local equipMessage = System.String.Format(LocalString.GetString("\n对应装备名: {0}"), equipName)
            self:AddItemToTable(equipMessage, Color.green)

            local lostSoulTime = math.floor(tonumber(data[2] or 0))
            local duration = math.floor(tonumber(data[3] or 0))
            local validTime = System.String.Format(LocalString.GetString("有效期: {0}"), ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(lostSoulTime):AddDays(duration), "yyyy-MM-dd HH:mm"))
            self:AddItemToTable(validTime, Color.green)

            local pricePerDay = math.floor(tonumber(data[4] or 0))
            local fixPrice = System.String.Format(LocalString.GetString("对应装备单天修理价格: {0}"), pricePerDay)
            self:AddItemToTable(fixPrice, Color.green)

            local ownerName = tostring(data[0])
            local ownerId = math.floor(tonumber(data[1] or 0))
            local ownerMessage = System.String.Format(LocalString.GetString("装备拥有者: {0}({1})"), ownerName, ownerId)
            self:AddItemToTable(ownerMessage, Color.green)

            self:CheckAndAddFeiShengInfo(item, itemData)

            if CommonDefs.op_LessThanOrEqual_DateTime_DateTime(CServerTimeMgr.ConvertTimeStampToZone8Time(lostSoulTime):AddDays(duration), CServerTimeMgr.Inst:GetZone8Time()) then
                self:AddItemToTable(LocalString.GetString("\n已过期"), Color.red)
            end
        end
    end
    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:InitMapiItem( item)

    Extensions.RemoveAllChildren(self.contentTable.transform)
    local setting = ZuoQi_Setting.GetData()
    local prop = nil
    if item.Item.TemplateId == setting.MapiXiaomaItemId then
        local babyInfo = CreateFromClass(CMapiBabyInfo)
        if babyInfo:LoadFromString(item.Item.ExtraVarData.Data) then
            prop = CZuoQiMgr.CreateMapiPropFromBabyInfo(babyInfo)
        end
    else
        prop = CreateFromClass(CMapiProp)
        if not prop:LoadFromString(item.Item.ExtraVarData.Data) then
            prop = nil
        end
    end
    if prop ~= nil then
        local icon = setting.MapiIcon[prop.FuseId]
        if prop.Quality == 4 then
            icon = setting.SpecialMapiIcon[0]
        elseif prop.Quality == 5 then
			icon = setting.SpecialMapiIcon[1]
		end
        self.iconTexture:LoadMaterial(icon)


        self.nameLabel.color = CZuoQiMgr.GetMapiColor(prop.Quality)
        self.qualitySprite.spriteName = CZuoQiMgr.GetMapiItemCellBorder(prop.Quality)

        local default
        if prop.Gender == 0 then
            default = LocalString.GetString("阳")
        else
            default = LocalString.GetString("阴")
        end
        local gender = default
        local genderColor = prop.Gender == 0 and "519FFF" or "FF88FF"
        local categoryMessage
        if item.TemplateId == setting.MapiXiaomaItemId then
            categoryMessage = LocalString.GetString("小马")
            gender = LocalString.GetString("未知性别")
            genderColor = "FFFFFF"
            local itemData = Item_Item.GetData(item.TemplateId)
            if itemData ~= nil then
                self.nameLabel.text = itemData.Name
            end
        else
            -- self.nameLabel.text = prop.Name
            categoryMessage = LocalString.GetString("成年")
        end

        local generation = math.max(1, prop.Generation)
        self.categoryLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("{0}代{1}[c][{2}]({3})[-][/c]"), {generation, categoryMessage, genderColor, gender})

        -- 马匹评分
        self:AddFirstTitleDesc(LocalString.GetString("马匹评分"), tostring(CZuoQiMgr.GetMapiPingFen(prop)))

        -- 马匹品种：SpeciesName + type
        self:AddSpace()
        local speciesName = CZuoQiMgr.GetAppearanceName(prop.PinzhongId, prop.Quality)
        local type = CZuoQiMgr.GetAttributeName(prop.AttributeNameId)
        local pinZhongMessage = System.String.Format(LocalString.GetString("{0} · {1}"), speciesName, type)
        self:AddFirstTitleDesc(LocalString.GetString("马匹品种"), pinZhongMessage)

        -- 马匹身高
        self:AddSpace()
        local height = prop.Chicun * setting.MapiHight
        self:AddFirstTitleDesc(LocalString.GetString("马匹身高"), System.String.Format(LocalString.GetString("{0}米"), round2(height, 2)))

        if item.TemplateId ~= setting.MapiXiaomaItemId then
            -- 竞速属性
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("竞速属性"), "")
            self:AddSecondTitleDesc(LocalString.GetString("速度"), self:GetAttrWithRank(tostring(prop.Speed), self:GetMapiAttrIndentiy(prop.Speed)))
            self:AddSecondTitleDesc(LocalString.GetString("耐力"), self:GetAttrWithRank(tostring(prop.Naili), self:GetMapiAttrIndentiy(prop.Naili)))
            self:AddSecondTitleDesc(LocalString.GetString("灵巧"), self:GetAttrWithRank(tostring(prop.Lingqiao), self:GetMapiAttrIndentiy(prop.Lingqiao)))

            -- 地形适应
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("地形适应"), "")
            self:AddSecondTitleDesc(LocalString.GetString("草地"), self:GetAttrWithRank(tostring(prop.CaodiShiying), self:GetMapiAttrIndentiy(prop.CaodiShiying)))
            self:AddSecondTitleDesc(LocalString.GetString("砂地"), self:GetAttrWithRank(tostring(prop.NidiShiying), self:GetMapiAttrIndentiy(prop.NidiShiying)))
            self:AddSecondTitleDesc(LocalString.GetString("岩地"), self:GetAttrWithRank(tostring(prop.YandiShiying), self:GetMapiAttrIndentiy(prop.YandiShiying)))
        end

        -- 竞速技能，默认都是0
        if prop.Skills ~= nil and prop.Skills.Length ~= 0 then
            local hasSkill = false
            do
                local i = 0
                while i < prop.Skills.Length do
                    if prop.Skills[i] ~= 0 then
                        hasSkill = true
                    end
                    i = i + 1
                end
            end
            if hasSkill then
                self:AddSpace()
                self:AddFirstTitleDesc(LocalString.GetString("竞速技能"), "")
                self:AddSkill(prop.Skills)
            else
                self:AddSpace()
                self:AddItemToTable(LocalString.GetString("无遗传技能"), NGUIText.ParseColor24("ACF8FF", 0))
            end
        else
            self:AddSpace()
            self:AddItemToTable(LocalString.GetString("无遗传技能"), NGUIText.ParseColor24("ACF8FF", 0))
        end

        -- 其他描述
        self:AddItemToTable(item.Description, self.ValueColor)

        self:CheckAndAddFeiShengInfo(item, Item_Item.GetData(item.TemplateId))

        self.eyeBtn:SetActive(true)
        self.eyeInfoAction = DelegateFactory.Action(function ()
            CZuoQiMgr.Inst:ShowMapiPreview(prop)
        end)
    end
    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:GetMapiAttrIndentiy( attr)
    local setting = ZuoQi_Setting.GetData()
    local counter = - 1
    do
        local i = 0
        while i < setting.MapiTipsGrade.Length do
            if attr >= setting.MapiTipsGrade[i] then
                counter = i
            end
            i = i + 1
        end
    end
    if counter < 0 or counter >= setting.MapiTipsGradeDes.Length then
        return ""
    else
        return System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(CZuoQiMgr.GetMapiAttrRankColor(counter + 1)), setting.MapiTipsGradeDes[counter])
    end
end
function CLuaItemInfoWnd:GetAttrWithRank( attr, rank)
    local sb = NewStringBuilderWraper(StringBuilder, attr)
    local attrLength = CommonDefs.StringLength(attr)
    do
        local i = 0
        while i < 8 - attrLength do
            sb:Append("  ")
            i = i + 1
        end
    end
    sb:Append(rank)
    return ToStringWrap(sb)
end
function CLuaItemInfoWnd:InitLingfuItem( template, data)

    Extensions.RemoveAllChildren(self.contentTable.transform)

    --CMapiLingfuItemData data = item.Item.LingfuItemInfo;
    if data == nil then
        return
    end

    local taozhuang = ZuoQi_MapiTaozhuang.GetData(data.TaozhuangId)
    local qualityColor = CZuoQiMgr.GetLingFuColor(data.Quality)
    self.nameLabel.text = System.String.Format("{0}({1})", template.Name, taozhuang.Name)
    self.nameLabel.color = qualityColor
    self.qualitySprite.spriteName = CZuoQiMgr.GetLingFuItemCellBorder(data.Quality)

    self.categoryLabel.text = CZuoQiMgr.GetLingFuPos(CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), data.Pos))

    self:AddSpace()

    local fullDuration = ZuoQi_Setting.GetData().LingfuDurability[data.Quality - 1]
    local c = data.Duration > 0 and Color.white or Color.red
    self:AddFirstTitleDesc(LocalString.GetString("灵符耐久"), System.String.Format(LocalString.GetString("[c][{0}]{1}/{2}[-][/c]"), NGUIText.EncodeColor24(c), math.floor(math.ceil(data.Duration)), fullDuration))

    if data.QianghuaJieshu > 0 then
        self:AddStar(System.String.Format(LocalString.GetString("{0}阶强化"), EnumChineseDigits.GetDigit(data.QianghuaJieshu)), data.QianghuaXingji, CZuoQiMgr.sMaxQianghuaXingji)
    end

    self.showInfoButton = true
    self.showInfoAction = DelegateFactory.Action(function ()
        local message = System.String.Format(LocalString.GetString("可获得词条数：{0}"), ZuoQi_Setting.GetData().LingfuMaxWordNum[data.Quality - 1])
        local maxDeltLvl = ZuoQi_LingfuQianghua.GetData(CZuoQiMgr.sMaxQianghuaJieshu).TaozhuangWordAdd[CZuoQiMgr.sMaxQianghuaXingji - 1]
        local maxQianghuaIncStr = System.String.Format(LocalString.GetString("\n灵符强化套装属性最大加成 [c][00FF00]{0:N2}%[-][/c]"), CZuoQiMgr.CalcTaozhuangAttrIncrease(data, maxDeltLvl) * 100)
        CTooltip.Show(message .. maxQianghuaIncStr, self.transform, CTooltip.AlignType.Default)
    end)

    -- 灵符评分 = sum(词条评分)，2 4 6位置的灵符没有词条
    if not CZuoQiMgr.LingFuHasCiTiao(CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), data.Pos)) then
        self:AddSpace()
        self:AddFirstTitleDesc(LocalString.GetString("灵符评分"), LocalString.GetString("无"))
        self:AddSpace()
        self:AddFirstTitleDesc(LocalString.GetString("基础词条"), "")
        self:AddItemToTable(LocalString.GetString("  该位置的灵符没有基础词条"), Color.white)
    else
        if data.Words ~= nil and data.Words.Count > 0 then
            local extraLvl = 0
            local qianghuaData = ZuoQi_LingfuQianghua.GetData(data.QianghuaJieshu)
            if qianghuaData ~= nil and qianghuaData.WordAdd.Length >= data.QianghuaXingji and data.QianghuaXingji > 0 then
                extraLvl = qianghuaData.WordAdd[data.QianghuaXingji - 1]
            end

            local pingfen = 0
            CommonDefs.DictIterate(data.Words, DelegateFactory.Action_object_object(function (___key, ___value)
                local v = {}
                v.Key = ___key
                v.Value = ___value
                if Word_Word.Exists(v.Key + extraLvl) then
                    local word = Word_Word.GetData(v.Key + extraLvl)
                    pingfen = pingfen + math.floor(tonumber(word.Mark))
                end
            end))
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("灵符评分"), tostring(pingfen))

            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("基础词条"), "")
            local itemProp = CClientMainPlayer.Inst.ItemProp
            CommonDefs.DictIterate(data.Words, DelegateFactory.Action_object_object(function (___key, ___value)
                local v = {}
                v.Key = ___key
                v.Value = ___value
                if Word_Word.Exists(v.Key) and Word_Word.Exists(v.Key + extraLvl) then
                    local word = Word_Word.GetData(v.Key)
                    local qianghuaStr = CZuoQiMgr.CalcWordAttrIncreaseStr(v.Key, extraLvl, 1)
                    self:AddWord(System.String.Format(LocalString.GetString("[c]【{0}】{1}[-]"), word.Name, qianghuaStr), qualityColor)
                end
            end))
        else
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("灵符评分"), "0")
        end
    end


    -- 套装
    self:AddSpace()
    if taozhuang ~= nil then
        self:AddFirstTitleDesc(LocalString.GetString("套装属性"), "")

        -- 包裹中打开，此时不需要比较，也没有生效的套装
        -- 在灵符界面中打开，需要比较，并有套装是否生效的要求
        -- 查看下童套装的灵符最小强化阶数和强化星级
        local taozhuangCount = 0
        local qianghuaJieshu = CZuoQiMgr.sMaxQianghuaJieshu + 1
        local qianghuaXingji = CZuoQiMgr.sMaxQianghuaXingji + 1

        if TypeIs(self.baseItemInfo, typeof(PackageItemInfo)) then
            taozhuangCount = 0
        elseif TypeIs(self.baseItemInfo, typeof(LinkItemInfo)) and (TypeAs(self.baseItemInfo, typeof(LinkItemInfo))).needCompare then
            -- 遍历该马匹所佩戴的灵符，看是否隶属于这个套装，属于的话就+1
            local selectedMapi = CMapiView.sCurZuoqiId
            if CZuoQiMgr.TabIndex == 2 then
                selectedMapi = CLingfuView.sCurZuoQiId
            end
            if selectedMapi ~= nil then
                local zuoqiEquip = nil
                (function ()
                    local __try_get_result
                    __try_get_result, zuoqiEquip = CommonDefs.DictTryGet(CClientMainPlayer.Inst.ItemProp.ZuoQiEquip, typeof(String), selectedMapi, typeof(CZuoQiEquipProp))
                    return __try_get_result
                end)()
                if zuoqiEquip ~= nil and zuoqiEquip.Equip ~= nil then
                    do
                        local i = 0
                        while i < zuoqiEquip.Equip.Length do
                            local equip = CItemMgr.Inst:GetById(zuoqiEquip.Equip[i])
                            if equip ~= nil then
                                local equipData = equip.Item.LingfuItemInfo
                                if equipData.TaozhuangId == data.TaozhuangId and equipData.Duration > 0 then
                                    taozhuangCount = taozhuangCount + 1
                                    if equipData.QianghuaJieshu < qianghuaJieshu then
                                        qianghuaJieshu = equipData.QianghuaJieshu
                                        qianghuaXingji = equipData.QianghuaXingji
                                    elseif qianghuaJieshu == equipData.QianghuaJieshu then
                                        qianghuaXingji = math.min(qianghuaXingji, equipData.QianghuaXingji)
                                    end
                                end
                            end
                            i = i + 1
                        end
                    end
                end
            end
            -- 查看他人马匹时也需要显示套装信息
            if not selectedMapi and CZuoQiMgr.Inst.m_CurrentMapi.MapiInfo then
                for i = 0, CZuoQiMgr.Inst.m_CurrentMapiEquipList.Count-1 do
                    local equipId = CZuoQiMgr.Inst.m_CurrentMapiEquipList[i]
                    if not System.String.IsNullOrEmpty(equipId) then
                        local taozhuangId = CZuoQiMgr.Inst.m_CurrentMapiTaoZhuangList[i]
                        if data.TaozhuangId == taozhuangId then
                            taozhuangCount = taozhuangCount + 1
                        end
                    end
                end
            end
        end

        local extraLvl = 0
        local qianghuaData = ZuoQi_LingfuQianghua.GetData(qianghuaJieshu)
        if qianghuaData ~= nil and qianghuaData.TaozhuangWordAdd.Length >= qianghuaXingji and qianghuaXingji > 0 then
            extraLvl = qianghuaData.TaozhuangWordAdd[qianghuaXingji - 1]
        end

        --#region 灵符套装
        self:AddSpace()
        if taozhuang.Suit2Attr ~= nil and taozhuang.Suit2Attr.Length > 0 then
            if taozhuangCount >= 2 then
                self:AddItemToTable(System.String.Format(LocalString.GetString("【{0}】 2/2"), taozhuang.Name), Color.green)
            else
                self:AddItemToTable(System.String.Format(LocalString.GetString("【{0}】 {1}/2"), taozhuang.Name, taozhuangCount), Color.grey)
            end
        end
        if taozhuang.Suit4Attr ~= nil and taozhuang.Suit4Attr.Length > 0 then
            if taozhuangCount >= 4 then
                self:AddItemToTable(System.String.Format(LocalString.GetString("【{0}】 4/4"), taozhuang.Name), Color.green)
            else
                self:AddItemToTable(System.String.Format(LocalString.GetString("【{0}】 {1}/4"), taozhuang.Name, taozhuangCount), Color.grey)
            end
        end
        if taozhuang.Suit6Attr ~= nil and taozhuang.Suit6Attr.Length > 0 then
            if taozhuangCount >= 6 then
                self:AddItemToTable(System.String.Format(LocalString.GetString("【{0}】 6/6"), taozhuang.Name), Color.green)
            else
                self:AddItemToTable(System.String.Format(LocalString.GetString("【{0}】 {1}/6"), taozhuang.Name, taozhuangCount), Color.grey)
            end
        end

        if taozhuang.Suit2Attr ~= nil and taozhuang.Suit2Attr.Length > 0 then
            do
                local i = 0
                while i < taozhuang.Suit2Attr.Length do
                    local word = Word_Word.GetData(taozhuang.Suit2Attr[i])
                    local wordMark = Word_Word.GetData(taozhuang.Suit2Attr[i] + extraLvl)
                    if taozhuangCount >= 2 then
                        local qianghuaStr = CZuoQiMgr.CalcWordAttrIncreaseStr(taozhuang.Suit2Attr[i], extraLvl, 1)
                        self:AddWord(System.String.Format(LocalString.GetString("[c]【{0}】{1}(评分+{2})[-]"), word.Name, qianghuaStr, wordMark.Mark), Color.green)
                    else
                        self:AddWord(System.String.Format(LocalString.GetString("[c]【{0}】{1}(评分+{2})[-]"), word.Name, word.Description, word.Mark), Color.grey)
                    end
                    i = i + 1
                end
            end
        end
        if taozhuang.Suit4Attr ~= nil and taozhuang.Suit4Attr.Length > 0 then
            do
                local i = 0
                while i < taozhuang.Suit4Attr.Length do
                    local word = Word_Word.GetData(taozhuang.Suit4Attr[i])
                    local wordMark = Word_Word.GetData(taozhuang.Suit4Attr[i] + extraLvl)
                    if taozhuangCount >= 4 then
                        local qianghuaStr = CZuoQiMgr.CalcWordAttrIncreaseStr(taozhuang.Suit4Attr[i], extraLvl, 1)
                        self:AddWord(System.String.Format(LocalString.GetString("[c]【{0}】{1}(评分+{2})[-]"), word.Name, qianghuaStr, wordMark.Mark), Color.green)
                    else
                        self:AddWord(System.String.Format(LocalString.GetString("[c]【{0}】{1}(评分+{2})[-]"), word.Name, word.Description, word.Mark), Color.grey)
                    end
                    i = i + 1
                end
            end
        end
        if taozhuang.Suit6Attr ~= nil and taozhuang.Suit6Attr.Length > 0 then
            do
                local i = 0
                while i < taozhuang.Suit6Attr.Length do
                    local word = Word_Word.GetData(taozhuang.Suit6Attr[i])
                    local wordMark = Word_Word.GetData(taozhuang.Suit6Attr[i] + extraLvl)
                    if taozhuangCount >= 6 then
                        local qianghuaStr = CZuoQiMgr.CalcWordAttrIncreaseStr(taozhuang.Suit6Attr[i], extraLvl, 1)
                        self:AddWord(System.String.Format(LocalString.GetString("[c]【{0}】{1}(评分+{2})[-]"), word.Name, qianghuaStr, wordMark.Mark), Color.green)
                    else
                        self:AddWord(System.String.Format(LocalString.GetString("[c]【{0}】{1}(评分+{2})[-]"), word.Name, word.Description, word.Mark), Color.grey)
                    end
                    i = i + 1
                end
            end
        end
        --#endregion
    end

    self:CheckAndAddFeiShengInfoByTemplate(template)

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:InitDogTagItem(templateId, extraVarData)
    Extensions.RemoveAllChildren(self.contentTable.transform)

    local itemData = Item_Item.GetData(templateId)
    if itemData == nil then
        return
    end

    if extraVarData ~= nil then
        local data = TypeAs(MsgPackImpl.unpack(extraVarData), typeof(MakeGenericClass(List, Object)))
        if data ~= nil and data.Count >= 5 then
            self.nameLabel.text = System.String.Format(LocalString.GetString("{0}的{1}"), data[0], itemData.Name)
            self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(CItem.GetItemDescription(templateId,true)), self.ValueColor)
            local attackMessage = System.String.Format(LocalString.GetString("\n击杀者: {0}"), data[2])
            self:AddItemToTable(attackMessage, Color.green)

            self:CheckAndAddFeiShengInfoByTemplate(itemData)
        end
    end

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:InitHuoGuoQueueItem( item)
    if item.Item.ExtraData.Data ~= nil then
        local id = 0
        local default
        default, id = System.Int32.TryParse(item.Item.ExtraData.Data.varbinary.StringData)
        self:AddItemToTable(System.String.Format(LocalString.GetString("号码：{0}号"), id), Color.yellow)
    end
end
function CLuaItemInfoWnd:QueryDivinationLuckyTaskInfoResult( args)
    local luckyTaskId, taskId, needFinishTime, todayFinishTime, itemId=args[0],args[1],args[2],args[3],args[4]
    if itemId ~= self.baseItemInfo.itemId then
        return
    end
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        self:InitDivinationItem(item, todayFinishTime, needFinishTime, taskId)
    end
end

function CLuaItemInfoWnd:AddChuanJiaBaoStar( title, star,washedStar)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.starTemplate)
    instance:SetActive(true)
    instance:GetComponent(typeof(UILabel)).text = title

    local table = instance.transform:Find("table").gameObject
    Extensions.RemoveAllChildren(table.transform)

    local starItem = instance.transform:Find("Star").gameObject
    starItem:SetActive(false)

    local c1 = math.max(star,washedStar)--data.WordInfo.Star
    local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score
    for i=1,c2 do
        local g = NGUITools.AddChild(table,starItem)
        g:SetActive(true)

        local sprite = g:GetComponent(typeof(UISprite))
        sprite.alpha = 1

        if i<=c1 then
            sprite.spriteName = "playerAchievementView_star_1"
            if i<=star and i>washedStar then
                sprite.alpha = 0.3
            end
        else
            sprite.spriteName = "playerAchievementView_star_2"
        end
    end
    table:GetComponent(typeof(UITable)):Reposition()
end

function CLuaItemInfoWnd:InitChuanJiaBao( item)
    self.qualitySprite.spriteName =  CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoBorder(item)

    Extensions.RemoveAllChildren(self.contentTable.transform)

    local data = item.Item.ChuanjiabaoItemInfo
    if data == nil then
        return
    end
    local wordInfo = data.WordInfo
    local washedStar = wordInfo.WashedStar or 0

    self.detailBtn:SetActive(true)
    UIEventListener.Get(self.detailBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaCJBPreviewWnd.m_WashedStar = wordInfo.WashedStar
        CLuaCJBPreviewWnd.m_Star = wordInfo.Star
        CCJBMgr.Inst:OpenTipShowByData(item.Id, data.AppearInfo, false)
    end)

    self.nameLabel.text = item.Name

    local cjbTemplate = Item_Item.GetData(item.TemplateId)
    if cjbTemplate ~= nil then
        self.categoryLabel.text = cjbTemplate.Name
    end



    local star =washedStar>0 and washedStar or wordInfo.Star
    local name = cjbTemplate.Name
    local cjbSetting = House_ChuanjiabaoSetting.GetData()
    local lv = 0
    if string.find(name,LocalString.GetString("1级")) then
        lv = cjbSetting.ChuanjiabaoLowLevel
    elseif string.find(name,LocalString.GetString("2级")) then
        lv = cjbSetting.ChuanjiabaoNormalLevel
    elseif string.find(name,LocalString.GetString("3级")) then
        lv = cjbSetting.ChuanjiabaoHighLevel
    end
    local numOfWord = wordInfo.Words.Count
    if IsZhuShaBiXiLianOpen() then
        self:AddFirstTitleDesc(LocalString.GetString("基础属性"),"")
        self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("家园基础属性 %.2f%%"),CLuaZhuShaBiXiLianMgr.GetWashBaseProp(lv,star,numOfWord)*100), Color.white)
        self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("向天借命几率 %.2f%%"), CLuaZhuShaBiXiLianMgr.GetBorrowLifeProp(star,numOfWord)*100), Color.white)
    
        if data.WordInfo.Type == EChuanjiabaoType_lua.RenGe then
            local zhushabiData = ChuanjiabaoModel_RenGeWordOption.GetData(data.WordInfo.TypeParam)
            local zhushabiClasses = ChuanjiabaoModel_ZhuShaBiSubType.GetData(zhushabiData.ZhushabiSubType).Classes
            local classListStr = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhushabiClasses[0]))
            for i = 1, zhushabiClasses.Length - 1 do
                classListStr = classListStr..LocalString.GetString("、")..Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhushabiClasses[i]))
            end
            self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("职业 %s"), classListStr), Color.white)
        end
    end
    -- self:AddStar(LocalString.GetString("星级"), data.WordInfo.Star, ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score)

    if washedStar>0 then--洗过
        self:AddChuanJiaBaoStar(LocalString.GetString("星级"), wordInfo.Star,data.WordInfo.WashedStar or 0)
    else--没洗过
        self:AddChuanJiaBaoStar(LocalString.GetString("星级"), wordInfo.Star,wordInfo.Star)
    end

    local template = Item_Item.GetData(item.TemplateId)

    local zsw = CClientFurnitureMgr.Inst:GetChuanjiabaoInZhuangshiwuData(item.TemplateId)
    if zsw ~= nil then
        local c = data.BasicInfo.Duration > 0 and Color.white or Color.red
        local durationText = SafeStringFormat3("[c][%s]%s/%s[-][/c]", NGUIText.EncodeColor24(c), data.BasicInfo.Duration, zsw.Duration)
        self:AddFirstTitleDesc(LocalString.GetString("耐久度"), durationText)
        local location = Zhuangshiwu_Location.GetData(zsw.Location)
        if location ~= nil then
            self:AddFirstTitleDesc(LocalString.GetString("可置于"), location.Location)
        end
    end

    self:AddSpace()
    local words = data.WordInfo.Words
    CommonDefs.DictIterate(words, DelegateFactory.Action_object_object(function (___key, ___value)
        local word = Word_Word.GetData(___key)
        if word ~= nil then
            self:AddWord(SafeStringFormat3(LocalString.GetString("[c][%s]【%s】%s[-]"), CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoWordColor(words.Count), word.Name, word.Description), Color.white)
        end
    end))
    if CLuaZhuShaBiXiLianMgr.CanXiLian(wordInfo) then
        if wordInfo.WashCount and wordInfo.WashCount>0 then
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("洗炼积分"),  wordInfo.WashCount)
        else
            self:AddSpace()
            self:AddItemToTable(LocalString.GetString("可洗炼"),  Color.yellow)
        end
    end

    self:AddSpace()
    CClientChuanJiaBaoMgr.Inst.zhiciArtist = CreateFromClass(UInt64StringKeyValuePair, data.AppearInfo.Artist, "")
    CClientChuanJiaBaoMgr.Inst.jianbiArtist = CreateFromClass(UInt64StringKeyValuePair, data.WordInfo.JiandingArtist, "")
    CClientChuanJiaBaoMgr.Inst.kaiguangArtist = CreateFromClass(UInt64StringKeyValuePair, data.BasicInfo.KaiguangArtist, "")
    CClientChuanJiaBaoMgr.Inst.currentRequestItemId = item.Id
    Gac2Gas.RequestChuanjiabaoArtistName(data.AppearInfo.Artist, data.WordInfo.JiandingArtist, data.BasicInfo.KaiguangArtist)

    self:CheckAndAddFeiShengInfo(item, cjbTemplate)

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:InitUnevaluateChuanjiabao( item)
    local data = item.Item.UnevaluatedChuanjiabaoAppearanceInfo
    if data == nil then
        return
    end
    self.detailBtn:SetActive(true)
    UIEventListener.Get(self.detailBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDetailButtonClick(item.Id, data)
    end)
    Extensions.RemoveAllChildren(self.contentTable.transform)
    self.nameLabel.text = data.Name
    self:AddStar(LocalString.GetString("星级"), data.Star, ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score)
    self:AddSpace()
    self:AddItemToTable(item.Description, self.ValueColor)
    self:AddSpace()
    CClientChuanJiaBaoMgr.Inst.unevaluateChuanjiabao = item
    CClientChuanJiaBaoMgr.Inst.chuanjiabaoModelArtistId = data.Artist
    Gac2Gas.RequestChuanjiabaoModelArtistName(data.Artist)

    self:CheckAndAddFeiShengInfo(item, Item_Item.GetData(item.TemplateId))

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:ChuanjiabaoModelArtistNameReturn( )
    local item = CClientChuanJiaBaoMgr.Inst.unevaluateChuanjiabao
    local data = CreateFromClass(CChuanjiabaoAppearanceType)
    data:LoadFromString(item.Item.ExtraVarData.Data)
    if data == nil then
        return
    end
    Extensions.RemoveAllChildren(self.contentTable.transform)
    self:AddStar(LocalString.GetString("星级"), data.Star, ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score)
    self:AddSpace()
    self:AddItemToTable(item.Description, self.ValueColor)
    self:AddSpace()
    self:AddItemToTable(System.String.Format(LocalString.GetString("{0}[c][acf8ff]制瓷[-]"), CClientChuanJiaBaoMgr.Inst.chuanjiabaoModelArtistName), Color.white)
    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:InitEvaluatedZhushabi( item)
    self.qualitySprite.spriteName  = CClientChuanJiaBaoMgr.Inst:GetZhushabiBorder(item)

    self:AlignEvaluateZhushabi(item)

    self:CheckAndAddFeiShengInfo(item, Item_Item.GetData(item.TemplateId))

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()

    local data = CreateFromClass(CChuanjiabaoWordType)
    data:LoadFromString(item.Item.ExtraVarData.Data)

    CClientChuanJiaBaoMgr.Inst.zhushabiArtistId = data.JiandingArtist
    CClientChuanJiaBaoMgr.Inst.evaluateZhushabi = item
    Gac2Gas.RequestZhushabiArtistName(data.JiandingArtist, 0)
end
function CLuaItemInfoWnd:AlignEvaluateZhushabi( item)
    Extensions.RemoveAllChildren(self.contentTable.transform)
    local data = CreateFromClass(CChuanjiabaoWordType)
    data:LoadFromString(item.Item.ExtraVarData.Data)

    local washedStar = data.WashedStar or 0
    local star =washedStar>0 and washedStar or data.Star
    local name = Item_Item.GetData(item.TemplateId).Name
    local cjbSetting = House_ChuanjiabaoSetting.GetData()
    local lv = 0
    if string.find(name,LocalString.GetString("1级")) then
        lv = cjbSetting.ChuanjiabaoLowLevel
    elseif string.find(name,LocalString.GetString("2级")) then
        lv = cjbSetting.ChuanjiabaoNormalLevel
    elseif string.find(name,LocalString.GetString("3级")) then
        lv = cjbSetting.ChuanjiabaoHighLevel
    end
    local numOfWord = data.Words.Count

    if IsZhuShaBiXiLianOpen() then
        self:AddFirstTitleDesc(LocalString.GetString("基础属性"),"")
        self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("家园基础属性 %.2f%%"),CLuaZhuShaBiXiLianMgr.GetWashBaseProp(lv,star,numOfWord)*100), Color.white)
        self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("向天借命几率 %.2f%%"), CLuaZhuShaBiXiLianMgr.GetBorrowLifeProp(star,numOfWord)*100), Color.white)
        
        if data.Type == EChuanjiabaoType_lua.RenGe then
            local zhushabiData = ChuanjiabaoModel_RenGeWordOption.GetData(data.TypeParam)
            local zhushabiClasses = ChuanjiabaoModel_ZhuShaBiSubType.GetData(zhushabiData.ZhushabiSubType).Classes
            local classListStr = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhushabiClasses[0]))
            for i = 1, zhushabiClasses.Length - 1 do
                classListStr = classListStr..LocalString.GetString("、")..Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhushabiClasses[i]))
            end
            self:AddItemToTable(SafeStringFormat3("   "..LocalString.GetString("职业 %s"), classListStr), Color.white)
        end
    end

    if data.WashedStar and data.WashedStar>0 then--洗过
        self:AddChuanJiaBaoStar(LocalString.GetString("星级"), data.Star,data.WashedStar)
    else--没洗过
        self:AddChuanJiaBaoStar(LocalString.GetString("星级"), data.Star,data.Star)
    end

    local words = data.Words
    CommonDefs.DictIterate(words, DelegateFactory.Action_object_object(function (___key, ___value)
        local word = Word_Word.GetData(___key)
        if word ~= nil then
            self:AddWord(SafeStringFormat3(LocalString.GetString("[c][%s]【%s】%s[-]"), CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoWordColor(words.Count), word.Name, word.Description), Color.white)
        end
    end))

    if CLuaZhuShaBiXiLianMgr.CanXiLian(data) then
        if data.WashCount and data.WashCount>0 then
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("洗炼积分"),  data.WashCount)
        else
            self:AddSpace()
            self:AddItemToTable(LocalString.GetString("可洗炼"),  Color.yellow)
        end
    end

    self:AddSpace()

    self:AddItemToTable(item.Description, self.ValueColor)
    self:AddSpace()

    --先判断是否应该显示
    if data.WashedStar and data.WashedStar>0 then
        self.detailBtn:SetActive(true)
        UIEventListener.Get(self.detailBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            CLuaZhuShaBiStarWnd.m_WashedStar = data.WashedStar
            CLuaZhuShaBiStarWnd.m_Star = data.Star
            CUIManager.ShowUI(CLuaUIResources.ZhuShaBiStarWnd)
        end)
    end
end
function CLuaItemInfoWnd:RequestZhushabiArtistName( )

    local item = CClientChuanJiaBaoMgr.Inst.evaluateZhushabi

    self:AlignEvaluateZhushabi(item)

    self:AddItemToTable(SafeStringFormat3(LocalString.GetString("%s[c][acf8ff]定笔[-]"), CClientChuanJiaBaoMgr.Inst.zhushabiArtistName), Color.white)

    self:CheckAndAddPreciousProtectTimeInfo(item)

    self:CheckAndAddFeiShengInfo(item, Item_Item.GetData(item.TemplateId))

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:OnChuanjiabaoArtistResult( args )
    if CClientChuanJiaBaoMgr.Inst.currentRequestItemId ~= self.m_ItemId then return end
    --只处理一次
    if self.m_ChuanjiabaoArtistResultProcessed then return end
    self.m_ChuanjiabaoArtistResultProcessed = true

    local item = CItemMgr.Inst:GetById(self.m_ItemId)
    if item ~= nil and item.IsItem and item.Item.Type == EnumItemType_lua.Chuanjiabao then
        local data = item.Item.ChuanjiabaoItemInfo
        if CClientChuanJiaBaoMgr.Inst.zhiciArtist.Key == data.AppearInfo.Artist then
            self:AddItemToTable(System.String.Format(LocalString.GetString("{0}[c][acf8ff]制瓷[-]"), CClientChuanJiaBaoMgr.Inst.zhiciArtist.Value), Color.white)
        end
        if CClientChuanJiaBaoMgr.Inst.jianbiArtist.Key == data.WordInfo.JiandingArtist then
            self:AddItemToTable(System.String.Format(LocalString.GetString("{0}[c][acf8ff]定笔[-]"), CClientChuanJiaBaoMgr.Inst.jianbiArtist.Value), Color.white)
        end
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(data.BasicInfo.KaiguangTime)
        if CClientChuanJiaBaoMgr.Inst.kaiguangArtist.Key == data.BasicInfo.KaiguangArtist then
            self:AddLabel2(System.String.Format(LocalString.GetString("{0}[c][acf8ff]开光[-]"), CClientChuanJiaBaoMgr.Inst.kaiguangArtist.Value), System.String.Format(LocalString.GetString("{0}[c][acf8ff]年[-]{1}[c][acf8ff]月[-]{2}[c][acf8ff]日[-]"), time.Year, time.Month, time.Day))
        end
    end

    self:LayoutWnd()
end
function CLuaItemInfoWnd:CheckAndAddQiangQinInfo( item)
    if item.TemplateId == Constants.QingQingDaoJu then
        if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
            local list = TypeAs(MsgPackImpl.unpack(item.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
            if list.Count == 2 then
                local playerId = tonumber(list[0])
                local name = tostring(list[1])

                local desc = LocalString.GetString("结婚对象：") .. name
                self:AddItemToTable(desc, self.ValueColor)
            end
        end
    end
end
function CLuaItemInfoWnd:CheckAndAddPresentInfo( item)

    if CommonDefs.HashSetContains(CPresentMgr.Inst.PresentSet, typeof(UInt32), item.TemplateId) then
        local data = GameplayItem_PresentItem.GetData(item.TemplateId)
        if data == nil or data.Type ~= 1 then
            return
        end

        if not self.m_QueryPresentItemDataReturn then
            Gac2Gas.QueryPresentItemRecieverName(item.Id)
            --return;
        end

        local recvName = CPresentMgr.Inst:GetReceiverNameByItemId(item.Id)
        local sendCount = CPresentMgr.Inst:GetReceiverCountByItemId(item.Id)

        if recvName == nil then
            recvName = " "
        end
        if sendCount < 0 then
            sendCount = 0
        end

        local text = System.String.Format(LocalString.GetString("\n[c][{0}]{1}[-][/c]"), NGUIText.EncodeColor24(self.ValueColor), g_MessageMgr:Format(data.TipDescription, recvName, sendCount))
        self:AddItemToTable(text, self.ValueColor)
    end
end
function CLuaItemInfoWnd:CheckAndAddBiWuTeamMemberInfo( item)
    if item.Item.TemplateId == BiWu_Setting.GetData().OutExpItemId then
        if item.Item.ExtraData.Data ~= nil and item.Item.ExtraData.Data.varbinary ~= nil then
            local memberNames = item.Item.ExtraData.Data.varbinary.StringData
            if not System.String.IsNullOrEmpty(memberNames) then
                self:AddItemToTable(System.String.Format(LocalString.GetString("队友分别为: {0}"), memberNames), Color.green)
            end
        end
    end
end
function CLuaItemInfoWnd:CheckAndAddLiuYiItemInfo( item)
    if nil == item.Item.ExtraVarData.Data then
        return
    end
    if item.Item.TemplateId >= 21030257 and item.Item.TemplateId <= 21030260 then
        local data = TypeAs(MsgPackImpl.unpack(item.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
        if data ~= nil and data.Count >= 2 then
            local addStr = ""
            if item.Item.TemplateId == 21030257 then
                addStr = LocalString.GetString("戳破了[c]<link player={0},{1}>[/c]的七彩泡泡")
            elseif item.Item.TemplateId == 21030258 then
                addStr = LocalString.GetString("七彩泡泡被[c]<link player={0},{1}>[/c]戳破")
            elseif item.Item.TemplateId == 21030259 then
                addStr = LocalString.GetString("将[c]<link player={0},{1}>[/c]吹入了神奇泡泡")
            elseif item.Item.TemplateId == 21030260 then
                addStr = LocalString.GetString("被[c]<link player={0},{1}>[/c]吹入了神奇泡泡")
            end

            self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(System.String.Format(addStr, ToStringWrap(data[0]), ToStringWrap(data[1])), false), Color.green)
        end
    end
end
function CLuaItemInfoWnd:CheckAndAddDiqiInfo( item)
    local setting = House_Setting.GetData()

    if item.Item.TemplateId == setting.ContractItemId
    or item.Item.TemplateId == setting.PreciousContractItemId
    or item.Item.TemplateId == setting.FamousHouseContract then
        if item.Item.ExtraData.Data ~= nil then
            if System.String.IsNullOrEmpty(item.Item.LastDiqiOwnerName) then
                local d = CreateFromClass(CHouseDataForItemDisplay)
                d:LoadFromString(item.Item.ExtraVarData.Data)

                Gac2Gas.RequestDiqiLastOwnerName(item.Id, d.PrevOwnerId)
                return
            end
            local info = CreateFromClass(CHouseDataForItemDisplay)
            info:LoadFromString(item.Item.ExtraVarData.Data)

            self:AddFirstTitleDesc(LocalString.GetString("格局"), CClientHouseMgr.GetGejuName(info.Wuxing))

            self:AddFirstTitleDesc(LocalString.GetString("包含建筑"), "")

            local levelName = LocalString.GetString("级")

            self:AddSecondTitleDesc(LocalString.GetString("主房屋"), tostring(info.ZhengwuGrade) .. levelName)
            self:AddSecondTitleDesc(LocalString.GetString("仓库"), tostring(info.CangkuGrade) .. levelName)
            do
                local i = 0
                while i < HouseConst.MaxGardenNum do
                    if CommonDefs.DictContains(info.GardenGrade, typeof(Byte), i) then
                        self:AddSecondTitleDesc(LocalString.GetString("苗圃") .. EnumNumber.GetNum(i + 1), tostring(CommonDefs.DictGetValue(info.GardenGrade, typeof(Byte), i)) .. levelName)
                    end
                    i = i + 1
                end
            end

            --判定一下是否老数据 正屋6级时海梦泽至少是1级，否则为老数据不显示海梦泽等级
            if not (info.ZhengwuGrade >= 6 and info.PoolGrade <=0) then
                self:AddSecondTitleDesc(LocalString.GetString("海梦泽"), tostring(info.PoolGrade) .. levelName)
            end

            self:AddFirstTitleDesc(LocalString.GetString("装饰物"), tostring(info.RepoItemNum))

            self:AddFirstTitleDesc(LocalString.GetString("仓库资材"), "")

            self:AddSecondTitleDesc(LocalString.GetString("粮食"), tostring(info.Food))
            self:AddSecondTitleDesc(LocalString.GetString("木材"), tostring(info.Wood))
            self:AddSecondTitleDesc(LocalString.GetString("石材"), tostring(info.Stone))
            self:AddFirstTitleDesc(LocalString.GetString("雪景"), info.IsOpenSnowStyle==1 and LocalString.GetString("开启") or LocalString.GetString("未开启"))

            self:AddFirstTitleDesc(LocalString.GetString("前主人"), item.Item.LastDiqiOwnerName)

            -- 镜中府邸
            if info.FurnitureLimitStatus==1 then
                self:AddFirstTitleDesc(LocalString.GetString("镜中府邸"), LocalString.GetString("已使用"))
            else
                --可能是老数据，没有设置该状态，请求服务器该参数
                if not self.m_IsFurnitureLimitStatusRequested then
                    self.m_IsFurnitureLimitStatusRequested = true
                    Gac2Gas.RequestHouseFurnitureLimitStatus(item.Id,info.Id)
                end
            end

            -- 当前天空盒
            if info.SkyBoxKind and info.SkyBoxKind ~= 0 and info.SkyBoxKind ~= 6 then
                local skyBox = House_SkyBoxKind.GetData(info.SkyBoxKind)
                if skyBox then
                    local item = Item_Item.GetData(skyBox.ItemID)
                    if item then
                        self:AddFirstTitleDesc(LocalString.GetString("当前天空"), item.Name)
                    end
                end
            end

            -- 已解锁的天空盒
            local skyBoxList = {}
            House_SkyBoxKind.ForeachKey(function(key)
                if key ~= 6 then--默认天空盒
                    local sky = House_SkyBoxKind.GetData(key)
                    if sky.UseDuration == "~" then--有使用期限的天空盒不在地契中显示
                        local isUnlock = info.SkyBoxUnlocked:GetBit(key)
                        local item = Item_Item.GetData(sky.ItemID)
                        if item and isUnlock then
                            table.insert(skyBoxList, item.Name)
                        end
                    end
                end
            end)

            if #skyBoxList > 0 then
                self:AddFirstTitleDesc(LocalString.GetString("解锁天空"), "")
                for i = 1, #skyBoxList do
                    self:AddSecondTitleDesc(skyBoxList[i], "")
                end
            end
        end
    end
end
function CLuaItemInfoWnd:CheckAndAddFeiShengInfo( item, template)
    self:CheckAndAddFeiShengInfoByTemplate(template)
end
function CLuaItemInfoWnd:CheckAndAddFeiShengInfoByTemplate( template)
    if not CFeiShengMgr.Inst.IsOpen then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end
    --等级一致的时候不显示
    if template.GradeCheck == template.GradeCheckFS1 then
        return
    end
    local maxGrade = GameSetting_Common_Wapper.Inst.CurrentPlayerMaxGrade
    local maxFeiShengGrade = GameSetting_Common_Wapper.Inst.CurrentPlayerMaxFeiShengGrade
    local playerLevel = CClientMainPlayer.Inst.Level
    if template.GradeCheck <= maxGrade and template.GradeCheckFS1 <= maxFeiShengGrade then
        self:AddItemToTable(System.String.Format(LocalString.GetString("凡身使用等级{0}级，仙身使用等级{1}级"), template.GradeCheck, template.GradeCheckFS1), Color.red)
    elseif template.GradeCheck > maxGrade and template.GradeCheckFS1 <= maxFeiShengGrade then
        self:AddItemToTable(System.String.Format(LocalString.GetString("凡身不可使用，仙身使用等级{0}级"), template.GradeCheckFS1), Color.red)
    elseif template.GradeCheck <= maxGrade and template.GradeCheckFS1 > maxFeiShengGrade then
        self:AddItemToTable(System.String.Format(LocalString.GetString("凡身使用等级{0}级，仙身不可使用"), template.GradeCheck), Color.red)
    end
end
function CLuaItemInfoWnd:CheckAndAddDuoHunFanInfo( item)
    self.m_DuoHunTargetId = nil
    self.m_DuoHunFanItem = nil
    if item.IsItem and item.Item.IsDuoHunFan then
        self.m_DuoHunTargetId = item.Item:GetDuoHunPlayerId()
        self.m_DuoHunFanItem = item
        if self.m_DuoHunTargetId > 0 then
            if System.String.IsNullOrEmpty(item.Item.DuoHunTargetName) then
                Gac2Gas.RequestPlayerName(self.m_DuoHunTargetId)
                ---Gac2Gas.QueryDuoHunFanPlayerName(item.Id, item.Item.OwnerId)
            else
                local text = System.String.Format(LocalString.GetString("\n夺魂目标: [c][00ff00]{0}[-][/c]"), item.Item.DuoHunTargetName)
                self:AddItemToTable(text, self.ValueColor)
                if item.Item.IsDuoHunFanTimeout then
                    self:AddItemToTable(LocalString.GetString("\n已过期"), Color.red)
                else
                    local leftTime = item.Item:GetDuoHunLeftTime()
                    if leftTime > 0 then
                        self:AddItemToTable(System.String.Format(LocalString.GetString("\n剩余时间: {0}"), Utility.CalcCDTimeLeft(leftTime * 1000)), Color.red)
                    end
                end
            end
        end
    end
end
function CLuaItemInfoWnd:OnSendPlayerName(playerId,playerName)
    if playerId == self.m_DuoHunTargetId then
        local item = self.m_DuoHunFanItem
        if not item then
            return
        end
        item.Item.DuoHunTargetName = playerName
        self:Init0(item)
    end
end
function CLuaItemInfoWnd:CheckAndAddHunPoInfo( item)

    if item.IsItem and item.Item.IsHunPo then
        local targetId = item.Item:GetHunPoPlayerId()
        if targetId > 0 then
            if System.String.IsNullOrEmpty(item.Item.DuoHunTargetName) then
                Gac2Gas.QueryHunPoPlayerName(item.Id, item.Item.OwnerId)
            else
                local text = System.String.Format(LocalString.GetString("\n[c][00ff00]{0}[-][/c]的魂魄"), item.Item.DuoHunTargetName)
                self:AddItemToTable(text, self.ValueColor)
            end
        end
    end
end

function CLuaItemInfoWnd:CheckAndAddLiangHaoId(item, itemTemplate)
    if item and item.TemplateId == LuaLiangHaoMgr.GetLiangHaoItemTemplateId() then
        if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
            local raw = item.Item.ExtraVarData.Data
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            if list and list.Count >= 2 then
                local lianghaoId = tonumber(list[0])
                local expiredTime = list[1]
                local desc = SafeStringFormat3(LocalString.GetString("靓号ID: %s%d%s"), "[c][FA800A]", lianghaoId, "[-][/c]")
                self:AddItemToTable(desc, Color.yellow)
            end
        end
    end
end

function CLuaItemInfoWnd:CheckAndAddExpBookDiscountInfo(item, itemTemplate)
    if CommonDefs.HashSetContains(GameSetting_Client.GetData().EXP_BOOK_NEED_DISCOUNT, typeof(UInt32), itemTemplate.ID) then
        local mainplayer = CClientMainPlayer.Inst
        if mainplayer and CServerTimeMgr.Inst.timeStamp - mainplayer.BasicProp.CreateTime >= GameSetting_Client.GetData().EXP_BOOK_DISCOUNT_TIME * 86400 then
            local fanshenLevel = mainplayer.HasFeiSheng and mainplayer.PlayProp.FeiShengData.FeiShengLevel or mainplayer.Level
            local createDays =  math.floor((CServerTimeMgr.Inst.timeStamp - mainplayer.BasicProp.CreateTime) / 86400)
            local val = AllFormulas.Action_Formula[1020].Formula(nil, nil, {fanshenLevel, createDays})
            local message = g_MessageMgr:Format(GameSetting_Client.GetData().EXP_BOOK_DISCOUNT_MESSAGE, NumberTruncate(val,2)) --保留两位小数截断
            self:AddItemToTable(message, Color.yellow)
        end
    end
end

function CLuaItemInfoWnd:CheckAndAddPeriod( item, itemTemplate)
    --如果道具记录的过期时间为0，则不显示过期时间 refs #72068
    local realExpiredTimestamp = item.Item:GetRealExpiredTime()
    if self.s_HiddenPeriodIfExpiredTimeZero and realExpiredTimestamp == 0 then
        return
    end
    --客户端仅根据Period字段来确定显示方式，具体显示值需要读取CItem.ExpiredTime
    if not System.String.IsNullOrEmpty(itemTemplate.Period) then
        local regex_for_fixed_period = CreateFromClass(Regex, "^(m|w):(\\d+):(\\d+)$", RegexOptions.Singleline)
        local isFixedPreiod = regex_for_fixed_period:IsMatch(itemTemplate.Period)
        local hours = 0
        local default
        default, hours = System.UInt32.TryParse(itemTemplate.Period)

        if isFixedPreiod or default then
            if isFixedPreiod or hours > 0 then
                local expiredTime = CServerTimeMgr.ConvertTimeStampToZone8Time(realExpiredTimestamp)
                local span = expiredTime:Subtract(CServerTimeMgr.Inst:GetZone8Time())
                if span.TotalHours > 0 then
                  if span.TotalHours > GameSetting_Client.GetData().ItemExpiringNoticeTime then
                    if span.TotalDays >= 1 then
                      self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: 剩余{0}天"), math.ceil(span.TotalDays)), Color.green)
                    else
                      if span.TotalSeconds < 3600 then
                        self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: 剩余{0}分"), math.ceil(span.TotalSeconds / 60)), Color.green)
                      else
                        self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: 剩余{0}小时"), math.ceil(span.TotalHours)), Color.green)
                      end
                    end
                  else
                    if span.TotalDays >= 1 then
                      self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: 剩余{0}天"), math.ceil(span.TotalDays))..'[c][FF0000]'.. LocalString.GetString(' (即将过期)'), Color.green)
                    else
                      if span.TotalSeconds < 3600 then
                        self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: 剩余{0}分"), math.ceil(span.TotalSeconds / 60))..'[c][FF0000]'.. LocalString.GetString(' (即将过期)'), Color.green)
                      else
                        self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: 剩余{0}小时"), math.ceil(span.TotalHours))..'[c][FF0000]'.. LocalString.GetString(' (即将过期)'), Color.green)
                      end
                    end
                  end
                else
                  self:AddItemToTable(LocalString.GetString("\n已过期"), Color.red)
                end
            end
        else
            local regex = CreateFromClass(Regex, "(\\d+)-(\\d+)-(\\d+)-(\\d+)-(\\d+)", RegexOptions.Singleline)
            local match = regex:Match(itemTemplate.Period)
            if match.Success then
                local expiredTime = CServerTimeMgr.ConvertTimeStampToZone8Time(realExpiredTimestamp)
                local span = expiredTime:Subtract(CServerTimeMgr.Inst:GetZone8Time())
                if CommonDefs.op_GreaterThan_DateTime_DateTime(expiredTime, CServerTimeMgr.Inst:GetZone8Time()) then
                  if span.TotalHours > GameSetting_Client.GetData().ItemExpiringNoticeTime then
                    self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: {0}"), ToStringWrap(expiredTime, "yyyy-MM-dd HH:mm")), Color.green)
                  else
                    self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: {0}"), ToStringWrap(expiredTime, "yyyy-MM-dd HH:mm")) ..'[c][FF0000]'.. LocalString.GetString(' (即将过期)') , Color.green)
                  end
                else
                    self:AddItemToTable(LocalString.GetString("\n已过期"), Color.red)
                end
            end
        end
    else
        --有一种情况是策划表Period为空，但是服务器设置了过期时间，这里还是需要以一种默认行为显示一下
        local expiredTime = CServerTimeMgr.ConvertTimeStampToZone8Time(realExpiredTimestamp)
        if realExpiredTimestamp>0 then
            local span = expiredTime:Subtract(CServerTimeMgr.Inst:GetZone8Time())
            if CommonDefs.op_GreaterThan_DateTime_DateTime(expiredTime, CServerTimeMgr.Inst:GetZone8Time()) then
                if span.TotalHours > GameSetting_Client.GetData().ItemExpiringNoticeTime then
                self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: {0}"), ToStringWrap(expiredTime, "yyyy-MM-dd HH:mm")), Color.green)
                else
                self:AddItemToTable(System.String.Format(LocalString.GetString("\n有效期: {0}"), ToStringWrap(expiredTime, "yyyy-MM-dd HH:mm")) ..'[c][FF0000]'.. LocalString.GetString(' (即将过期)') , Color.green)
                end
            else
                self:AddItemToTable(LocalString.GetString("\n已过期"), Color.red)
            end
        end
    end
end
function CLuaItemInfoWnd:Init4MajiuMapi( )
    if CZuoQiMgr.s_SelectMajiuCurMapi ~= nil then
        local prop = nil
        CommonDefs.DictIterate(CZuoQiMgr.Inst.m_MajiuMapiInfo, DelegateFactory.Action_object_object(function (___key, ___value)
            local v = {}
            v.Key = ___key
            v.Value = ___value
            if v.Value.ZuoQiId == CZuoQiMgr.s_SelectMajiuCurMapi then
                prop = v.Value.mapiDisplayInfo
                return
            end
        end))
        local setting = ZuoQi_Setting.GetData()
        if prop ~= nil then
            local icon = setting.MapiIcon[prop.FuseId]
            if prop.Quality == 4 then
                icon = setting.SpecialMapiIcon[0]
            elseif prop.Quality == 5 then
				icon = setting.SpecialMapiIcon[1]
			end
            self.iconTexture:LoadMaterial(icon)

            self.disableSprite.gameObject:SetActive(false)

            self.nameLabel.color = CZuoQiMgr.GetMapiColor(prop.Quality)
            self.qualitySprite.spriteName = CZuoQiMgr.GetMapiItemCellBorder(prop.Quality)

            local default
            if prop.Gender == 0 then
                default = LocalString.GetString("阳")
            else
                default = LocalString.GetString("阴")
            end
            local gender = default
            local genderColor = prop.Gender == 0 and "519FFF" or "FF88FF"
            local categoryMessage
            self.nameLabel.text = prop.Name
            categoryMessage = LocalString.GetString("成年")

            local generation = math.max(1, prop.Generation)
            self.categoryLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("{0}代{1}[c][{2}]({3})[-][/c]"), {generation, categoryMessage, genderColor, gender})

            -- 马匹评分
            self:AddFirstTitleDesc(LocalString.GetString("马匹评分"), tostring(CZuoQiMgr.GetMapiPingFen(prop)))

            -- 马匹品种：SpeciesName + type
            self:AddSpace()
            local speciesName = CZuoQiMgr.GetAppearanceName(prop.PinzhongId, prop.Quality)
            local type = CZuoQiMgr.GetAttributeName(prop.AttributeNameId)
            local pinZhongMessage = System.String.Format(LocalString.GetString("{0} · {1}"), speciesName, type)
            self:AddFirstTitleDesc(LocalString.GetString("马匹品种"), pinZhongMessage)

            -- 马匹身高
            self:AddSpace()
            local height = prop.Chicun * setting.MapiHight
            self:AddFirstTitleDesc(LocalString.GetString("马匹身高"), System.String.Format(LocalString.GetString("{0}米"), round2(height, 2)))

            -- 竞速属性
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("竞速属性"), "")
            self:AddSecondTitleDesc(LocalString.GetString("速度"), self:GetAttrWithRank(tostring(prop.Speed), self:GetMapiAttrIndentiy(prop.Speed)))
            self:AddSecondTitleDesc(LocalString.GetString("耐力"), self:GetAttrWithRank(tostring(prop.Naili), self:GetMapiAttrIndentiy(prop.Naili)))
            self:AddSecondTitleDesc(LocalString.GetString("灵巧"), self:GetAttrWithRank(tostring(prop.Lingqiao), self:GetMapiAttrIndentiy(prop.Lingqiao)))

            -- 地形适应
            self:AddSpace()
            self:AddFirstTitleDesc(LocalString.GetString("地形适应"), "")
            self:AddSecondTitleDesc(LocalString.GetString("草地"), self:GetAttrWithRank(tostring(prop.CaodiShiying), self:GetMapiAttrIndentiy(prop.CaodiShiying)))
            self:AddSecondTitleDesc(LocalString.GetString("砂地"), self:GetAttrWithRank(tostring(prop.NidiShiying), self:GetMapiAttrIndentiy(prop.NidiShiying)))
            self:AddSecondTitleDesc(LocalString.GetString("岩地"), self:GetAttrWithRank(tostring(prop.YandiShiying), self:GetMapiAttrIndentiy(prop.YandiShiying)))

            -- 竞速技能，默认都是0
            if prop.Skills ~= nil and prop.Skills.Length ~= 0 then
                local hasSkill = false
                do
                    local i = 0
                    while i < prop.Skills.Length do
                        if prop.Skills[i] ~= 0 then
                            hasSkill = true
                        end
                        i = i + 1
                    end
                end
                if hasSkill then
                    self:AddSpace()
                    self:AddFirstTitleDesc(LocalString.GetString("竞速技能"), "")
                    self:AddSkill(prop.Skills)
                else
                    self:AddSpace()
                    self:AddItemToTable(LocalString.GetString("无遗传技能"), NGUIText.ParseColor24("ACF8FF", 0))
                end
            else
                self:AddSpace()
                self:AddItemToTable(LocalString.GetString("无遗传技能"), NGUIText.ParseColor24("ACF8FF", 0))
            end

            local itemtid = CZuoQiMgr.GetMapiItemTIdByQuality(prop.Quality)
            local item = Item_Item.GetData(itemtid)
            if item ~= nil then
                self:AddItemToTable(item.Description, self.ValueColor)
            end

            self.eyeBtn:SetActive(true)
            self.eyeInfoAction = DelegateFactory.Action(function ()
                CZuoQiMgr.Inst:ShowMapiPreview(prop)
            end)
        end
        self:ShowButtons(self.baseItemInfo)
        self:LayoutWnd()
    end
end
function CLuaItemInfoWnd:Init4RepositoryFurniture( templateId)
    self:InitPaperItem(templateId)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    if data == nil then
        self.m_Wnd:Close()
        return
    end


    local spriteName = ""
    local icon = ""
    local nameText = ""
    local desc = ""
    if data.Type == EnumFurnitureType_lua.eJianzhu then
        spriteName = Constants.DefaultItemCellBorder
        local zsw = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
        icon = zsw and zsw.Icon or ""
        nameText = data.Name

        self.nameLabel.color = Color.white
        desc = data.Desc
    else
        local itemtid = data.ItemId
        local item = Item_Item.GetData(itemtid)
        if item == nil then
            self.m_Wnd:Close()
            return
        end
        spriteName = CUICommonDef.GetItemCellBorder(item, nil, false)
        icon = item.Icon
        nameText = item.Name

        self.nameLabel.color = CItem.GetColor(item.ID)
        desc = CItem.GetItemDescription(item.ID,true)
    end

    self.iconTexture:Clear()
    self.qualitySprite.spriteName = spriteName
    self.bindSprite.spriteName = nil
    self.iconTexture:LoadMaterial(icon)

    self.disableSprite.enabled = (CClientHouseMgr.Inst:GetCurHouseGrade() < data.HouseGrade)

    self.nameLabel.text = nameText

    self.availableLevelLabel.text = String.Format(LocalString.GetString("家园{0}级"), data.HouseGrade)
    self.categoryLabel.text = String.Format(LocalString.GetString("{0}级{1}"), data.Grade, CClientFurnitureMgr.GetTypeNameByType(data.Type))


    Extensions.RemoveAllChildren(self.contentTable.transform)

    local place = ""
    local locationDefine = Zhuangshiwu_Location.GetData(data.Location)
    if locationDefine ~= nil then
        place = locationDefine.Location
    end

    self:AddFirstTitleDesc(LocalString.GetString("可置于"), place)

    if data.Type ~= EnumFurnitureType_lua.eJianzhu then
        self:AddFirstTitleDesc(LocalString.GetString("摆放"), tostring(CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(templateId)))
        self:AddFirstTitleDesc(LocalString.GetString("剩余"), tostring(CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(templateId)))
        --self.AddItemToTable(String.Format(LocalString.GetString("同类装饰物已摆放{0}个，最多可摆放{1}个")
        --    , CClientFurnitureMgr.Inst.GetPlacedFurnitureCountByType(data.Type)
        --    , CClientFurnitureMgr.GetRepositoryNumLimit(CClientHouseMgr.Inst.GetCurHouseGrade(), data.Type, data.SubType))
        --    , Color.white);
    end

    self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(desc, false), self.ValueColor)

	local spring2020TempIds = Zhuangshiwu_Setting.GetData().SpringFestival2020TempIds
	local isSpringFurniture = false
	for i = 0, spring2020TempIds.Length-1 do
		if templateId == spring2020TempIds[i] then
			isSpringFurniture = true
			break;
		end
	end

	if isSpringFurniture then
		local DateTimeKind = import "System.DateTimeKind"
		local min, hour, day, month = string.match(Zhuangshiwu_Setting.GetData().SpringFestival2020EndCron, "(%d+) (%d+) (%d+) (%d+)")
		min, hour, day, month = tonumber(min), tonumber(hour), tonumber(day), tonumber(month)
		local utcendtime = CreateFromClass(DateTime, 2020, month, day, hour, min, 0, DateTimeKind.Utc)
		local utctimestamp = utcendtime:Subtract(CServerTimeMgr.UtcStart).TotalSeconds - CServerTimeMgr.s_ZoneValue*3600
		local diff = utctimestamp - CServerTimeMgr.Inst.timeStamp
		local days = math.floor(diff/24/3600)

		desc = SafeStringFormat(LocalString.GetString("有效期：剩余%d天"), days)
		self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(desc, false), Color.green)
    end
    
    local skyBoxKind = CLuaHouseMgr.GetSkyBoxItemByZSWId(templateId)
    if skyBoxKind then
        local SkyBoxUseTime = House_Setting.GetData().SkyBoxUseTime
        if CommonDefs.DictContains_LuaCall(SkyBoxUseTime, tostring(skyBoxKind.Kind)) then
            desc = SafeStringFormat(LocalString.GetString("有效期：%s"), CommonDefs.DictGetValue_LuaCall(SkyBoxUseTime, tostring(skyBoxKind.Kind)))
            self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(desc, false), Color.green)
        end
    end

    if self.m_PaperOutOfDateTime then
        local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(self.m_PaperOutOfDateTime)
        local timeStr = System.String.Format("{0:yyyy-MM-dd}", dt)
        desc = SafeStringFormat(LocalString.GetString("有效期：%s"), timeStr)
		self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(desc, false), Color.green)
    end
    
    --处理按钮显示
    self:ShowButtons(self.baseItemInfo)

    self:LayoutWnd()
end
function CLuaItemInfoWnd:AddFirstTitleDescWithSprite( title, desc, sprite, rotate)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.firstTitleDescWithSpriteTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CFirstTitleTextWithSpriteRow)):Init(title, desc, sprite, rotate)
end
function CLuaItemInfoWnd:CheckAddFurnitureStolenInfo( item)
    if CItemInfoMgr.showType == CItemInfoMgr.ShowType.FurnitureStore and CClientHouseMgr.Inst.mFurnitureProp ~= nil then
        local bIsStolen = false
        local restTimeTxt = ""
        local furnitureStolenTime = House_Setting.GetData().FurnitureStolenTime
        CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.StolenFurnitureList, DelegateFactory.Action_object_object(function (___key, ___value)
            local v = {}
            v.Key = ___key
            v.Value = ___value
            if v.Key == item.Id and v.Value.StolenTime ~= 0 and CServerTimeMgr.Inst.timeStamp - v.Value.StolenTime < furnitureStolenTime then
                bIsStolen = true
                local restTime = math.floor((furnitureStolenTime - CServerTimeMgr.Inst.timeStamp + v.Value.StolenTime))

                local val = math.floor(restTime / 3600)
                if val > 0 then
                    restTimeTxt = restTimeTxt .. (val .. LocalString.GetString("时"))
                end
                val = math.floor((restTime % 3600) / 60)
                if val > 0 then
                    restTimeTxt = restTimeTxt .. (val .. LocalString.GetString("分"))
                end
                val = restTime % 60
                restTimeTxt = restTimeTxt .. (val .. LocalString.GetString("秒"))
            end
        end))
        if bIsStolen then
            local msg = g_MessageMgr:FormatMessage("Furniture_Is_Stolen_Tooltip", restTimeTxt)
            self:AddItemToTable(msg, Color.red)
        end
    end
end
function CLuaItemInfoWnd:CheckAddFurnitureInfo( item)
    local data = Item_Item.GetData(item.Item.TemplateId)
    if data == nil then
        return
    end
    local zswid = CClientFurnitureMgr.GetZhuangshiwuIdFromItemData(data)
    local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(zswid)
    if zswdata == nil then
        return
    end

    self.availableLevelLabel.text = String.Format(LocalString.GetString("家园{0}级"), zswdata.HouseGrade)
    self.categoryLabel.text = String.Format(LocalString.GetString("{0}级{1}"), zswdata.Grade, CClientFurnitureMgr.GetTypeNameByType(zswdata.Type))

    local place = ""
    local zswloca = Zhuangshiwu_Location.GetData(zswdata.Location)
    if zswloca ~= nil then
        place = zswloca.Location
    end
    self:AddFirstTitleDesc(LocalString.GetString("可置于"), place)

    local raw = item.Item.ExtraVarData.Data
    if raw ~= nil then
        local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
        if list ~= nil and list.Count == 6 then
            if data.NameColor ~= "COLOR_ORANGE" then
                local fengshui = math.floor(tonumber(list[0] or 0))
                local haohua = math.floor(tonumber(list[1] or 0))
                local shushi = math.floor(tonumber(list[2] or 0))
                local doubleDuration = tonumber(list[3])
                local wuxing = math.floor(tonumber(list[4] or 0))
                local duration = math.floor(doubleDuration)

                local extraData = tostring(list[5])
                self:AddFirstTitleDesc(LocalString.GetString("五行"), CClientFurnitureMgr.GetWuxingNameByType(wuxing))

                local c = duration > 0 and Color.white or Color.red
                local durationText = SafeStringFormat3("[c][%s]%d/%d[-][/c]", NGUIText.EncodeColor24(c), duration, zswdata.Duration)

                self:AddFirstTitleDesc(LocalString.GetString("耐久"), durationText)
                local maxCoef = CClientFurnitureMgr.GetMaxAttributeCoefficiency()
                if CItemInfoMgr.showType == CItemInfoMgr.ShowType.FurnitureStore and CClientHouseMgr.Inst:IsWuxingImprove(wuxing) then
                    local maxVal = math.floor((maxCoef * zswdata.FengShui))
                    self:AddFirstTitleDescWithSprite(LocalString.GetString("风水度"), ((tostring(fengshui) .. "(") .. tostring(maxVal)) .. ")", "common_fight_rank_up", 0)
                    maxVal = math.floor((maxCoef * zswdata.HaoHua))
                    self:AddFirstTitleDescWithSprite(LocalString.GetString("豪华度"), ((tostring(haohua) .. "(") .. tostring(maxVal)) .. ")", "common_fight_rank_up", 0)
                    maxVal = math.floor((maxCoef * zswdata.ShuShi))
                    self:AddFirstTitleDescWithSprite(LocalString.GetString("舒适度"), ((tostring(shushi) .. "(") .. tostring(maxVal)) .. ")", "common_fight_rank_up", 0)
                else
                    local maxVal = math.floor((maxCoef * zswdata.FengShui))
                    self:AddFirstTitleDesc(LocalString.GetString("风水度"), ((tostring(fengshui) .. "(") .. tostring(maxVal)) .. ")")
                    maxVal = math.floor((maxCoef * zswdata.HaoHua))
                    self:AddFirstTitleDesc(LocalString.GetString("豪华度"), ((tostring(haohua) .. "(") .. tostring(maxVal)) .. ")")
                    maxVal = math.floor((maxCoef * zswdata.ShuShi))
                    self:AddFirstTitleDesc(LocalString.GetString("舒适度"), ((tostring(shushi) .. "(") .. tostring(maxVal)) .. ")")
                end
            end

            if CClientHouseMgr.Inst.mBasicProp ~= nil and item.Item:FlagIsSet(EnumItemFlag.BINDED) and data.NameColor ~= "COLOR_ORANGE" and CItemInfoMgr.showType == CItemInfoMgr.ShowType.FurnitureStore then
                local ownerName = ""
                if item.Item.OwnerId == CClientHouseMgr.Inst.mBasicProp.OwnerId then
                    ownerName = CClientHouseMgr.Inst.ownerName1
                else
                    ownerName = CClientHouseMgr.Inst.ownerName2
                end

                self:AddFirstTitleDesc(LocalString.GetString("归属人"), ownerName)
            end

            self:CheckAddFurnitureStolenInfo(item)
        end
    end
end
function CLuaItemInfoWnd:CheckAddSeaFishingWords(item)
    local data = HouseFish_AllFishes.GetData(item.Item.TemplateId)
    if not data then
        return
    end
    if data.IsZhenZhai ~= 1 then
        return
    end

    if item.Item.ExtraVarData and item.Item.ExtraVarData.Data then
        local zhenzhaiyu = CZhengzhaiFishType()
        if zhenzhaiyu:LoadFromString(item.Item.ExtraVarData.Data) then
            local duration = zhenzhaiyu.Duration
            local wordId = zhenzhaiyu.WordId
            local attr = LocalString.GetString("暂无")
            if wordId and wordId~=0 and Word_Word.GetData(wordId) then
                attr = Word_Word.GetData(wordId).Description
            end
            self:AddFirstTitleDesc(LocalString.GetString("灵气"), duration)
            self:AddFirstTitleDesc(LocalString.GetString("属性"), attr)
        end
    end

end

function CLuaItemInfoWnd:CheckAndAddPreciousProtectTimeInfo(item)
    --#region 珍品保护时间
    --if (item.Item.IsPrecious()) //refs #62597 【道具】道具处于保护期的时候，在tips显示出来
    if item.Item.LastTradeTime>0 and item.IsBinded and CPlayerShopMgr.IsBindAfterTradeItem(item.TemplateId) then
        self:AddItemToTable("", Color.white)
        self:AddItemToTable(LocalString.GetString("保护时间: 永久"), Color.red)
    elseif LuaAppearancePreviewMgr:IsCBGTradeItem(item.TemplateId) then
        local t = g_MessagePack.unpack(item.Item.ExtraVarData.Data)
        if t and #t>0 then
            local lastCbgTradeTime = t[1] or 0
            local lastTakebackTime = t[2] or 0
            if (lastCbgTradeTime > 0 or lastTakebackTime > 0 ) then
                local cbgSetting = Cangbaoge_Setting.GetData()
                local nextCanTradeTime = (math.max(lastCbgTradeTime + cbgSetting.ItemTradeMinTime * 24 * 3600,  lastTakebackTime + cbgSetting.ItemTradeTakebackCd))
                local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(nextCanTradeTime)
                local span = dt:Subtract(CServerTimeMgr.Inst:GetZone8Time())
                if span.TotalSeconds > 0 then
                    self:AddItemToTable("", Color.white)
                    self:AddItemToTable(System.String.Format(LocalString.GetString("保护时间: {0}"), ToStringWrap(dt, "yyyy-MM-dd HH:mm")), Color.red)
                end
            end
        end
    else
        local hours = PreciousEquipment_Setting.GetData().ItemTradeFrozenDuration
        local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(item.Item.LastTradeTime):AddHours(hours)
        local span = dt:Subtract(CServerTimeMgr.Inst:GetZone8Time())
        if span.TotalSeconds > 0 then
            self:AddItemToTable("", Color.white)
            self:AddItemToTable(System.String.Format(LocalString.GetString("保护时间: {0}"), ToStringWrap(dt, "yyyy-MM-dd HH:mm")), Color.red)
        end
    end
    --#endregion
end

function CLuaItemInfoWnd:CheckAndAddMinTradePrice( item)
    if not item.IsBinded and item.Item:IsPrecious() and not LuaAuctionMgr.NeedShowStartPrice(item.TemplateId) then
        self:AddItemToTable(System.String.Format(LocalString.GetString("\n最低易市交易价格: {0}"), item.Item:GetPreciousPrice()), Color.yellow)
    end
end
function CLuaItemInfoWnd:CheckChristmasRecievedCard( item)
    if item.IsItem and item.TemplateId == 21003525 and item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
        local data = TypeAs(MsgPackImpl.unpack(item.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
        if data ~= nil and data.Count > 7 then
            local senderName = ToStringWrap(data[5])
            self:AddItemToTable(System.String.Format(LocalString.GetString("来自：{0}"), senderName), Color.yellow)
        end
    end
end

-- 双十一福利券活动
function CLuaItemInfoWnd:CheckShuangshiyi2020Vouchers(item)
    if item.IsItem and item.TemplateId == Double11_Setting.GetData().VoucherTemplateId then
        local contentStr = LocalString.GetString("[ACF8FF]购买双十礼包对应奖励:\n")
        if CommonDefs.IS_HMT_CLIENT then
            contentStr = LocalString.GetString("[ACF8FF]购买周年庆礼包对应奖励:\n")
        end
        local formatStr = LocalString.GetString("[FFFF00]%s：[E8D0AA]返利%d绑定灵玉\n")

        Double11_Vouchers.Foreach(function (key, data)
            local itemData = Item_Item.GetData(data.ItemId)
            local rewardStr = SafeStringFormat3(formatStr, itemData.Name, data.Jade)
            contentStr = contentStr .. rewardStr
        end)

        local exChangeTime = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShuangshiyi2020Voucher)
        local currentTime =  Double11_Setting.GetData().VoucherMaxUseTime - exChangeTime
        contentStr = contentStr .. LocalString.GetString("[FF0000]剩余兑换次数：") ..currentTime
        self:AddItemToTable(contentStr, Color.white)
    end
end

function CLuaItemInfoWnd:CheckKunxiansuoItem(item)
    if item.IsItem and item.TemplateId == LianHua_Setting.GetData().InitedKunXianSuoItemId then
        if item.Item.ExtraVarData ~= nil then
            local data = item.Item.ExtraVarData.StringData
            if data ~= nil then
                local contentStr = LocalString.GetString("[FFFF00]目标：")
                contentStr = contentStr .. data
                self:AddItemToTable(contentStr, Color.white)
            end
        end
    elseif item.TemplateId == LianHua_Setting.GetData().KunXianSuoItemId  then
        local contentStr = LocalString.GetString("[888888]目标：暂无")
        self:AddItemToTable(contentStr, Color.white)
    end
end


function CLuaItemInfoWnd:Init1( template, isSigninItemInfo, isBind)
    if template == nil then
        self.m_Wnd:Close()
        return
    end
    self.iconTexture:Clear()
    self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(template, nil, false)
    self.bindSprite.spriteName = nil

    if self:CheckTemplatePeriodExpired(template) then
        -- 过期物品
        self.iconTexture:LoadMaterial("UI/Texture/Item_Other/Material/other_444.mat")
    else
        self.iconTexture:LoadMaterial(template.Icon)
    end
    self.disableSprite.enabled = not CItem.GetMainPlayerIsFit(template.ID, false)
    self.nameLabel.text = template.Name
    self.nameLabel.color = CItem.GetColor(template.ID)

    --等级
    local level = 0
    if CClientMainPlayer.Inst ~= nil then
        level = CClientMainPlayer.Inst.Level
    end
    local adjustedGradeCheck = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(template)
    if adjustedGradeCheck <= 1 then
        self.availableLevelLabel.text = nil
    elseif adjustedGradeCheck > level then
        if self:ItemCanUseLevelExceedPlayerMaxLevel(adjustedGradeCheck) then
            self.availableLevelLabel.text = LocalString.GetString("[c][FF0000]不可用[-][/c]")
        else
            self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][FF0000]%d级[-][/c]"), adjustedGradeCheck)
        end
    else
        self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][FFFFFF]%d级[-][/c]"), adjustedGradeCheck)
    end

    --处理宝石逻辑
    local inlayed,inlayedIn1st,inlayedIn2st = self:ProcessStone(template)

    --类型
    self.categoryLabel.text = nil
    local data = Item_Type.GetData(template.Type)
    if data ~= nil then
        --TODO 需要加入对等级、职业、性别限制的检查，策划表中目前还没有数据
        self.categoryLabel.text = System.String.Format("[c][ACF9FF]{0}[-][/c]", data.Name)
    end

    Extensions.RemoveAllChildren(self.contentTable.transform)
    self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(CItem.GetItemDescription(template.ID,true), false), self.ValueColor)

    --宝石第二套
    if CLuaItemInfoWnd.s_FromCheckGemGroupWnd then
        if CLuaCheckGemGroupWnd.m_HoleSetIndex==2 and not inlayed and inlayedIn1st then
            self:AddItemToTable(LocalString.GetString("已镶嵌于第一套，可共用"), NGUIText.ParseColor24("00C932",0))
        elseif not inlayed and inlayedIn2st then
            self:AddItemToTable(LocalString.GetString("已镶嵌于第二套，可共用"), NGUIText.ParseColor24("00C932",0))
        end
    end

    if isSigninItemInfo then
        local signinInfo = ""
        local days = CSigninMgr.Inst.itemDate - CSigninMgr.Inst.signedDays
        if days <= 0 then
            signinInfo = LocalString.GetString("该奖励已领取")
            self:AddItemToTable(signinInfo, Color.green)
        else
            signinInfo = System.String.Format(LocalString.GetString("还需累计签到{0}天"), days)
            self:AddItemToTable(signinInfo, Color.red)
        end
    end
    if template.Type == EnumItemType_lua.Book or template.Type == EnumItemType_lua.Jueji then
        if CClientMainPlayer.Inst ~= nil then
            local book = CItemMgr.Inst:GetItemTemplate(template.ID)
            local raceCheckList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, Int32)), Int32, book.RaceCheck)
            if not CommonDefs.ListContains(raceCheckList, typeof(Int32), CClientMainPlayer.Inst.BasicProp.Class) then
                self:AddItemToTable(LocalString.GetString("(不可领悟)"), Color.red)
            else
                local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
                CommonDefs.DictIterate(skillBooks, DelegateFactory.Action_object_object(function (___key, ___value)
                    if template.ID == ___key then
                        if not CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(___key) then
                            self:AddItemToTable(LocalString.GetString("(未领悟)"), Color.red)
                        else
                            self.bindSprite.spriteName = Constants.ItemLearnedSpriteName
                            self:AddItemToTable(LocalString.GetString("(已领悟)"), Color.green)
                        end
                        return
                    end
                end))
            end
        end
    end

    if isBind then
        self.bindSprite.spriteName = Constants.ItemBindedSpriteName
    end

    if CZuoQiMgr.GetLingfuPosByTemplateId(template.ID) > 0 and CItemInfoMgr.showType == CItemInfoMgr.ShowType.Link then
        if CItemInfoMgr.linkItemExtraUD ~= nil then
            local info = CreateFromClass(CMapiLingfuItemData)
            info:LoadFromString(CItemInfoMgr.linkItemExtraUD)
            self:InitLingfuItem(Item_Item.GetData(template.ID), info)
            return
        end
    end
    if CItemInfoMgr.showType == CItemInfoMgr.ShowType.QianKunDai and CQianKunDaiMgr.Inst:IsQianKunDaiJiFenItem(template.ID) then
        local desc = CQianKunDaiMgr.Inst:GetQianKunDaiItemDesc(template.ID)
        self:AddItemToTable(desc, Color.yellow)
    end

    --经验丹书道具增加经验衰减说明
    self:CheckAndAddExpBookDiscountInfo(nil, template)

    self:ProcessDogTag(template.ID)

    self:ProcessLianHuaItem(template.ID)

    self:CheckAndAddFeiShengInfoByTemplate(template)

    self:ProcessTianQiTimeLimitByTemplate(template)

    --海钓鱼饵的处理
    self:SeaFishingFoodExtraDes(template.ID)

    --处理按钮显示
    self:ShowButtons(self.baseItemInfo)

    self:LayoutWnd()
end

function CLuaItemInfoWnd:SeaFishingFoodExtraDes(templateId)
    local t = LuaSeaFishingMgr.TryGetYuErPlayerLevel(templateId)
    if not t then return end
    local needPlayerLevel = t[1]
    local yuerLevel = t[2]

    --角色
    local player = CClientMainPlayer.Inst
    if not player then return end

    local level = player.Level
	if player.HasFeiSheng then
		level = player.XianShenLevel
	end
    if level < needPlayerLevel then
        self:AddItemToTable(LocalString.GetString("当前人物等级不足，无法使用该鱼饵"), Color.red)
    end
    --鱼竿
    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
    local yuganLevel = 0
    for i=1,equipListOnBody.Count,1 do
        local data = HouseFish_PoleType.GetDataBySubKey("TemplateID",equipListOnBody[i-1].templateId)
		if data then
			yuganLevel = data.PoleLevel
		end
    end
    if yuerLevel > yuganLevel then
        self:AddItemToTable(LocalString.GetString("当前未装备鱼竿或鱼竿等级不足"), Color.red)
    end
end

function CLuaItemInfoWnd:ProcessLianHuaItem(templateId)
    local data = LianHua_Item.GetData(templateId)
    if data~=nil and LuaZongMenMgr.m_RefineMonsterItemLabels ~=nil then
        local quality = data.Quality
        for i=1,#LuaZongMenMgr.m_RefineMonsterItemLabels do
            self:AddItemToTable(LuaZongMenMgr.m_RefineMonsterItemLabels[i], Color.white)
        end
    end

    if LuaZongMenMgr.m_RefineMonsterCommonItem and CZuoQiMgr.IsMaPi(LuaZongMenMgr.m_RefineMonsterCommonItem.TemplateId)then
        local item = LuaZongMenMgr.m_RefineMonsterCommonItem
        local setting = ZuoQi_Setting.GetData()
        local prop = nil
        if item.Item.TemplateId == setting.MapiXiaomaItemId then
            local babyInfo = CreateFromClass(CMapiBabyInfo)
            if babyInfo:LoadFromString(item.Item.ExtraVarData.Data) then
                prop = CZuoQiMgr.CreateMapiPropFromBabyInfo(babyInfo)
            end
        else
            prop = CreateFromClass(CMapiProp)
            if not prop:LoadFromString(item.Item.ExtraVarData.Data) then
                prop = nil
            end
        end
        if prop ~= nil then
            local icon = setting.MapiIcon[prop.FuseId]
            if prop.Quality == 4 then
                icon = setting.SpecialMapiIcon[0]
            elseif prop.Quality == 5 then
                icon = setting.SpecialMapiIcon[1]
            end
            self.iconTexture:LoadMaterial(icon)
        end
    end

    LuaZongMenMgr.m_RefineMonsterItemLabels = nil
    LuaZongMenMgr.m_RefineMonsterCommonItem = nil
end

function CLuaItemInfoWnd:ProcessAntiProfessionItem(item)
    if LuaZongMenMgr:IsAntiprofessionItem(item.TemplateId) then
        local duration = SoulCore_Settings.GetData().AntiProfession_BuffDuration
        local hours = math.floor(duration / 3600)

        if item.Item.ExtraData.Data ~= nil and item.Item.ExtraData.Data.varbinary ~= nil then
            local extraStr = item.Item.ExtraData.Data.varbinary.StringData
            local dataTable = LuaSkillMgr:UnpakExtraAntiProfessionItemStr(extraStr)

            local calLevelTable = {}
            local effectsTable = {}
            for k, v in ipairs(dataTable) do
                local idx, level = v[1], v[2]
                if CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, idx) then
                    local anti = CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[idx]
                    local profession = anti.Profession
                    local curLevel = LuaZongMenMgr:GetAdjustAntiProfessionSkillLevel(idx, anti.CurLevel)
                    if profession > 0 and curLevel > 0 then
                        local realLevel = math.min(curLevel, level)
                       
                        table.insert(calLevelTable, {idx, level})
                        table.insert(effectsTable, {profession, realLevel, level})
                    end
                end
            end
            local antiLevel, antiId = LuaZongMenMgr:CalAntiProfessionLevel(calLevelTable)

            self:AddFirstTitleDesc(LocalString.GetString("符力"), SafeStringFormat3("%.1f", antiLevel))
            self:AddFirstTitleDesc(LocalString.GetString("克符效果"), "")
            for k, v in ipairs(effectsTable) do
                local profession = v[1]
                local realLevel = v[2]
                local oriLevel = v[3]
                local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)

                local effectStr = ""
                if realLevel == oriLevel then
                    local data =  SoulCore_AntiProfessionLevel.GetData(realLevel)
                    effectStr = SafeStringFormat3("+%.1f%%", data and data.AntiProfessionMul * 100 or 0)
                else
                    local realData =  SoulCore_AntiProfessionLevel.GetData(realLevel)
                    local oriData =  SoulCore_AntiProfessionLevel.GetData(oriLevel)
                    effectStr = SafeStringFormat3("[c][%s]+%.1f%%[-][/c] (%.1f%%)", Constants.ColorOfFeiSheng, realData and realData.AntiProfessionMul * 100 or 0, oriData and oriData.AntiProfessionMul * 100 or 0) 
                end
                local str = SafeStringFormat3(LocalString.GetString("对%s伤害 %s"), Profession.GetFullName(proClass) , effectStr)
                self:AddWord(str, Color.white)
            end
            self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[c][00ff60]使用后可获得%d小时职业克符效果[-][/c]"), hours), self.ValueColor)
        end
        local extraData = nil
    end
end

function CLuaItemInfoWnd:ProcessTianQiTimeLimitByTemplate(itemTemplate)
    local TimeLimitItem = TianQiMysticalShop_Setting.GetData().TimeLimitItem

    local istimelimit = false
    local timelimitdesc
    if  TimeLimitItem and CommonDefs.DictContains(TimeLimitItem, typeof(uint), itemTemplate.ID) then
        istimelimit = true
        timelimitdesc = TimeLimitItem[itemTemplate.ID]
    end

    if not istimelimit then return end

    if timelimitdesc then
        self:AddItemToTable(timelimitdesc, Color.yellow)
    end
end

function CLuaItemInfoWnd:ProcessDogTag(templateId)
  if templateId == GameSetting_Common.GetData().DogTagItemTempId or templateId == GameSetting_Common.GetData().AdvancedDogTagId then
    self:InitDogTagItem(templateId, CItemInfoMgr.linkItemExtraUD)
  end
end

function CLuaItemInfoWnd:ProcessStone(template)
    --如果在镶嵌石之灵的界面打开的时候，
    local inlayed = false
    local inlayedIn1st = false--是否镶嵌于第一套
    local inlayedIn2st = false--是否镶嵌于第二套
    if template.Type == EnumItemType_lua.Gem and CEquipmentProcessMgr.Inst.SelectEquipment ~= nil then
        local jewel = Jewel_Jewel.GetData(template.ID)
        if jewel and jewel.JewelLevel == 1 then
            local JewelKind = jewel.JewelKind
            local bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(template.ID)
            local equip = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId).Equip
            --第二套已经引用的、第二套已经安装的
            if CLuaItemInfoWnd.s_FromCheckGemGroupWnd then
                if CLuaCheckGemGroupWnd.m_HoleSetIndex==2 then--通过第二套的界面来查看宝石
                    local lookup = {}
                    for i=1,equip.Hole do
                        local id = equip.SecondaryHoleItems[i]
                        if id>0 then
                            if id<100 then--索引
                                local kind = Jewel_Jewel.GetData(equip.HoleItems[id]).JewelKind
                                lookup[kind] = true
                            else
                                local kind = Jewel_Jewel.GetData(id).JewelKind
                                lookup[kind] = true
                            end
                        end
                        local jewelInHole = Jewel_Jewel.GetData(equip.HoleItems[i])
                        if jewelInHole and jewel.JewelKind == JewelKind then
                            inlayedIn1st = true
                        end
                    end

                    if lookup[jewel.JewelKind] then
                        inlayed = true
                    end
                else
                    for i=1,equip.Hole do
                        local jewelInHole = Jewel_Jewel.GetData(equip.HoleItems[i])
                        if jewelInHole and JewelKind == jewelInHole.JewelKind then
                            inlayed = true
                        end
                    end
                    if equip.HoleSetCount==2 then
                        --看看第二套有没有使用
                        for i=1,equip.Hole do
                            local tempId = equip.SecondaryHoleItems[i]
                            if tempId>100 then
                                local jewelInfo1 = Jewel_Jewel.GetData(tempId)
                                if jewelInfo1.JewelKind==JewelKind then
                                    inlayedIn2st = true
                                end
                            end
                        end
                    end
                end
            end

            if inlayed then
                self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]已镶嵌[-][/c]"), NGUIText.EncodeColor24(Color.green))
            else
                if bindCount + notbindCount > 0 then
                    self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]可镶嵌[-][/c]"), NGUIText.EncodeColor24(Color.yellow))
                else
                    self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("[c][%s]未获得[-][/c]"), NGUIText.EncodeColor24(Color.red))
                end
            end
        end
    end
    return inlayed,inlayedIn1st,inlayedIn2st
end

function CLuaItemInfoWnd:Init2( equip)
    if equip == nil then
        self.m_Wnd:Close()
        return
    end

    -- print(equip,equip.Color)
    self.iconTexture:Clear()
    self.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
    self.bindSprite.spriteName = nil
    self.iconTexture:LoadMaterial(equip.Icon)
    self.disableSprite.enabled = false
    self.nameLabel.text = LocalString.GetString("装备")

    self.nameLabel.color = NGUIMath.IntToColor(math.floor(tonumber(equip.Color == 5 and "FF5050FF" or "FF88FFFF" or 0, 16)))
    self.availableLevelLabel.text = ""
    self.categoryLabel.text = ""

    Extensions.RemoveAllChildren(self.contentTable.transform)

    local lowerGrade = equip.MinGrade
    local upperGrade = equip.MaxGrade
    local qualityType = equip.Color
    local equipType = equip.Type

    local text = LocalString.GetString("等级")
    text = text .. (((tostring(lowerGrade) .. LocalString.GetString("至")) .. tostring(upperGrade)) .. LocalString.GetString("之间的"))
    local default
    if qualityType == 5 then
        default = LocalString.GetString("[FF5050]红色[-]")
    else
        default = LocalString.GetString("[FF88FF]紫色[-]")
    end
    text = text .. default
    text = text .. EnumFreightEquipmentType.GetEquipmentType(System.Int32.Parse(CommonDefs.StringSplit_ArrayChar(equipType, ",")[0]))
    self:AddItemToTable("", self.ValueColor)
    self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(text, false), self.ValueColor)
    self:AddItemToTable("", self.ValueColor)

    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end
function CLuaItemInfoWnd:AddItemToTable( text, color)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.labelTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = text
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).color = color
end
function CLuaItemInfoWnd:AddFirstTitleDesc( title, desc)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.firstTitleDescTempalte)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CFirstTitleTextInfoRow)):Init(title, desc)
end
function CLuaItemInfoWnd:AddSecondTitleDesc( title, desc)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.secondTitleDescTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CSecondTitleTextInfoRow)):Init(title, desc)
end
function CLuaItemInfoWnd:AddStar( title, count, totalCount)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.starTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(UILabel)).text = title
    local table = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UITable))
    local sprite = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UISprite))
    sprite.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(table.transform)
    do
        local i = 0
        while i < totalCount do
            local star = NGUITools.AddChild(table.gameObject, sprite.gameObject)
            star:SetActive(true)
            local default
            if i < count then
                default = self.highlightStar
            else
                default = self.normalStar
            end
            CommonDefs.GetComponent_GameObject_Type(star, typeof(UISprite)).spriteName = default
            i = i + 1
        end
    end
    table:Reposition()
end
function CLuaItemInfoWnd:AddSkill( skills)
    if skills.Length > 0 then
        local instance = NGUITools.AddChild(self.contentTable.gameObject, self.skillTemplate)
        instance:SetActive(true)
        self.skillIconTemplate:SetActive(false)
        local table = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UITable))

        do
            local i = 0
            while i < skills.Length do
                if skills[i] ~= 0 then
                    local skillId = skills[i]
                    local iconObject = NGUITools.AddChild(table.gameObject, self.skillIconTemplate)
                    iconObject:SetActive(true)
                    local icon = CommonDefs.GetComponentInChildren_GameObject_Type(iconObject, typeof(CUITexture))
                    local mapiSkill = ZuoQi_MapiSkill.GetData(skillId)
                    if mapiSkill ~= nil and icon ~= nil then
                        icon:LoadSkillIcon(mapiSkill.Icon)
                    end
                    -- 添加点击效果
                    UIEventListener.Get(iconObject).onClick = DelegateFactory.VoidDelegate(function (go)
                        CSkillInfoMgr.ShowSkillInfoWnd(skillId, self.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MapiSkill, true, 0, 0, nil)
                    end)
                end
                i = i + 1
            end
        end
    end
end
function CLuaItemInfoWnd:AddWord( word, color)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.wordTemplate)
    instance:SetActive(true)
    CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UILabel)).text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(color), word)
end
function CLuaItemInfoWnd:AddLabel2( label1, label2)
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.labelTemplate2)
    instance:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTitleDescTemplate)):Init(label1, label2)
end
function CLuaItemInfoWnd:AddSpace( )
    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.spaceTemplate)
    instance:SetActive(true)
end
function CLuaItemInfoWnd:ShowButtons( baseInfo)

    self.buttonsBg.gameObject:SetActive(false)
    if CLuaItemInfoWnd.s_EnableReuseButtons then
        local childCount = self.buttonTable.transform.childCount
        do
            local i = 0
            while i < childCount do
                self.buttonTable.transform:GetChild(i).gameObject:SetActive(false)
                i = i + 1
            end
        end
    else
        Extensions.RemoveAllChildren(self.buttonTable.transform)
    end

    local actionPairs = baseInfo.actionPairs

    if actionPairs ~= nil and actionPairs.Count > 0 then
        do
            local i = 0
            while i < actionPairs.Count do
                local instance = nil
                if CLuaItemInfoWnd.s_EnableReuseButtons and i < self.buttonTable.transform.childCount then
                    instance = self.buttonTable.transform:GetChild(i).gameObject
                else
                    instance = CUICommonDef.AddChild(self.buttonTable.gameObject, self.buttonTemplate)
                end
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CItemAndEquipTipButton)):Init(actionPairs[i].Key, false, false)
                if actionPairs[i].Value ~= nil then
                    local action = actionPairs[i].Value
                    UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function (go)
                        if baseInfo.dataSourceIsNull then
                            self.m_Wnd:Close()
                            return
                        else
                            invoke(action)
                        end
                    end)
                end
                i = i + 1
            end
        end
    end

    self.buttonTable:Reposition()
end
function CLuaItemInfoWnd:OnInfoButtonClick( go)
    if self.showInfoAction ~= nil then
        invoke(self.showInfoAction)
        return
    end
    if not System.String.IsNullOrEmpty(self.itemName) then
        CJingLingMgr.Inst:ShowJingLingWnd(LocalString.GetString("【System】") .. self.itemName, "o_gamepage", true, false, nil, false)
    end
end
function CLuaItemInfoWnd:OnEyeButtonClicked( go)
    if self.eyeInfoAction ~= nil then
        invoke(self.eyeInfoAction)
    end
end
function CLuaItemInfoWnd:OnSetItemAt( args)
    local place, position, oldItemId, newItemId=args[0],args[1],args[2],args[3]

    --check place
    repeat
        local default = place
        if default == EnumItemPlace.Body then
            if not (TypeIs(self.baseItemInfo, typeof(BodyItemInfo))) then
                return
            end
            break
        elseif default == EnumItemPlace.Bag or default == EnumItemPlace.Task then
            if not (TypeIs(self.baseItemInfo, typeof(PackageItemInfo))) then
                return
            end
            break
        end
    until 1
    local pos = self.baseItemInfo.position
    --check position
    if pos ~= position then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end

    local itemProp = CClientMainPlayer.Inst.ItemProp
    local id = itemProp:GetItemAt(place, position)
    --这个位置上的物品 不是原来的物品，关掉
    if id ~= self.baseItemInfo.itemId then
        self.m_Wnd:Close()
        return
    end

    -- bingo，更新下
    local item = CItemMgr.Inst:GetById(id)
    if item ~= nil then
        self:Init0(item)
        return
    else
        self.m_Wnd:Close()
    end
end
function CLuaItemInfoWnd:OnItemUseTimesUpdate( args)
    local place, position=args[0],args[1]

    if CClientMainPlayer.Inst == nil then
        return
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local id = itemProp:GetItemAt(place, position)

    if id == nil or self.baseItemInfo == nil then
        return
    end
    if id ~= self.baseItemInfo.itemId then
        return
    end
    local item = CItemMgr.Inst:GetById(id)
    if item ~= nil then
        self:Init0(item)
    end
end
function CLuaItemInfoWnd:OnSendItem( args)
    local itemId=args[0]

    if itemId ~= self.baseItemInfo.itemId then
        return
    end
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        self:Init0(item)
    end
end
function CLuaItemInfoWnd:OnQueryPresentItemReceiverNameReturn( args)
    local itemId=args[0]

    if itemId ~= self.baseItemInfo.itemId then
        return
    end
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        self.m_QueryPresentItemDataReturn = true
        self:Init0(item)
    end
end
function CLuaItemInfoWnd:OnQueryDiqiOwnerNameResult( args)
    local item = args[0]

    if item ~= nil then
        self:Init0(item)
    end
end
function CLuaItemInfoWnd:OnQueryDuoHunFanPlayerNameReturn( args)
    local item=args[0]

    if item ~= nil then
        self:Init0(item)
    end
end
function CLuaItemInfoWnd:OnQueryHunPoPlayerNameReturn( args )
    local item=args[0]

    if item ~= nil then
        self:Init0(item)
    end
end
function CLuaItemInfoWnd:LayoutWnd( )
    local maxWidth = 0
    do
        local i = 0
        while i < self.contentTable.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.contentTable.transform:GetChild(i), typeof(CBaseItemInfoRow))
            if row ~= nil then
                local rowMaxWidth = row:GetMaxContentWidth()
                if maxWidth < rowMaxWidth then
                    maxWidth = rowMaxWidth
                end
            end
            i = i + 1
        end
    end

    local actualContentWidth = math.min(math.max(maxWidth, self.minContentWidth), self.maxContentWidth)

    do
        local i = 0
        while i < self.contentTable.transform.childCount do
            local row = CommonDefs.GetComponent_Component_Type(self.contentTable.transform:GetChild(i), typeof(CBaseItemInfoRow))
            if row ~= nil then
                row:SetContentWidth(actualContentWidth)
            end
            i = i + 1
        end
    end

    self.contentTable:Reposition()

    self.background.width = math.ceil(actualContentWidth + math.abs(self.contentScrollView.panel.leftAnchor.absolute) + math.abs(self.contentScrollView.panel.rightAnchor.absolute))

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(self.contentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(self.contentScrollView.panel.bottomAnchor.absolute)

    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + self.contentScrollView.panel.clipSoftness.y * 2
    local displayWndHeight = math.min(math.max(totalWndHeight, contentTopPadding + contentBottomPadding + 10), virtualScreenHeight)

    --设置背景高度
    self.background.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.transform, typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end

    self.buttonTable.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(self.background.localCorners[2], Vector3(6, - 12))
    local abandonName = LocalString.GetString("回收")
    local visibleChildCount = 0
    local lastVisibleChild = nil
    local childCount = self.buttonTable.transform.childCount
    do
        local i = 0
        while i < childCount do
            if self.buttonTable.transform:GetChild(i).gameObject.activeSelf then
                visibleChildCount = visibleChildCount + 1
                lastVisibleChild = self.buttonTable.transform:GetChild(i)
            end
            i = i + 1
        end
    end
    if visibleChildCount >= 1 then
        local last = lastVisibleChild
        local abandonBtnExists = (CommonDefs.GetComponent_Component_Type(last, typeof(CItemAndEquipTipButton)).Text == abandonName)
        if abandonBtnExists and visibleChildCount > 1 then
            self.buttonsBg.gameObject:SetActive(true)
            last.gameObject:SetActive(false)
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.background.transform, self.buttonTable.transform)
            self.buttonsBg.height = math.floor((b.size.y + 40 --[[20 * 2]]))
            self.buttonsBg.transform.localPosition = b.center
            last.gameObject:SetActive(true)
            Extensions.SetLocalPositionY(last, last.localPosition.y - 20)
        elseif not abandonBtnExists then
            self.buttonsBg.gameObject:SetActive(true)
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.background.transform, self.buttonTable.transform)
            self.buttonsBg.height = math.floor((b.size.y + 40 --[[20 * 2]]))
            self.buttonsBg.transform.localPosition = b.center
        end
    end

    self.contentScrollView:ResetPosition()




    --设置Body位置
    --Vector4 v = body.panel.baseClipRegion;
    --v.w = heightOfBody; //设置裁剪区高度
    --v.y = background.height * 0.5f - heightOfHeader - heightOfBody * 0.5f; //设置裁剪区中心的位置
    --body.panel.baseClipRegion = v;//将裁剪区应用到body
    --body.ResetPosition(); //将内容对齐到裁剪区顶部
    --//设置Footer位置
    --footerSplit.enabled = (heightOfBody > minHeightOfBody);
    --footer.transform.SetLocalPositionY(background.localSize.y*0.5f-heightOfHeader-heightOfBody);
    --//设置Button位置
    --button.transform.SetLocalPositionY(background.localSize.y*0.5f-heightOfHeader-heightOfBody-heightOfFooter);

    self.background.transform.localPosition = Vector3.zero
    self.table:Reposition()

    if self.infoBtn.activeSelf then
        local widget = CommonDefs.GetComponent_GameObject_Type(self.infoBtn, typeof(UIWidget))
        if widget ~= nil then
            widget:ResetAnchors()
        end
    end


    if self.baseItemInfo ~= nil then
        self:AdjustPosition(self.baseItemInfo.alignType, self.baseItemInfo.targetCenterPos, self.baseItemInfo.targetSize)
    end
end
function CLuaItemInfoWnd:TotalHeightOfScrollViewContent( )
    return NGUIMath.CalculateRelativeWidgetBounds(self.contentTable.transform).size.y + self.contentTable.padding.y * 2
end
function CLuaItemInfoWnd:AdjustPosition( type, worldPos, targetSize)
    local selfBounds
    repeat
        local default = type
        if default == CItemInfoMgr.AlignType.Default then
            self.table.transform.localPosition = Vector3.zero
            break
        elseif default == CItemInfoMgr.AlignType.Left then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(self.table.transform.parent:InverseTransformPoint(worldPos).x - targetSize.x * 0.5 - selfBounds.size.x * 0.5, self.table.transform.parent:InverseTransformPoint(worldPos).y + targetSize.y * 0.5 - selfBounds.size.y * 0.5, 0)
            break
        elseif default == CItemInfoMgr.AlignType.Right then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(self.table.transform.parent:InverseTransformPoint(worldPos).x + targetSize.x * 0.5 + selfBounds.size.x * 0.5, self.table.transform.parent:InverseTransformPoint(worldPos).y + targetSize.y * 0.5 - selfBounds.size.y * 0.5, 0)
            break
        elseif default == CItemInfoMgr.AlignType.Top then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(self.table.transform.parent:InverseTransformPoint(worldPos).x, self.table.transform.parent:InverseTransformPoint(worldPos).y + targetSize.y * 0.5 + selfBounds.size.y * 0.5, 0)
            break
        elseif default == CItemInfoMgr.AlignType.ScreenLeft then
            do
                local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
                local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
                self.table.transform.localPosition = Vector3(- virtualScreenWidth * 0.25, 0, 0)
            end
            break
        elseif default == CItemInfoMgr.AlignType.ScreenRight then
            do
                local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
                local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
                self.table.transform.localPosition = Vector3(virtualScreenWidth * 0.25, 0, 0)
            end
            break
        end
    until 1

    --限制在屏幕范围内
    local selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
    local wp = self.table.transform.localPosition
    self.table.transform.localPosition = CUICommonDef.ConstrainToScreenArea(wp.x,wp.y,selfBounds.size.x,selfBounds.size.y)
end
function CLuaItemInfoWnd:GetSummonLingShouButton( )
    if self.baseItemInfo ~= nil then
        do
            local i = 0
            while i < self.buttonTable.transform.childCount do
                local child = self.buttonTable.transform:GetChild(i)
                if (CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel)).text == LocalString.GetString("召唤灵兽")) then
                    return child.gameObject
                end
                i = i + 1
            end
        end
    end
    return nil
end
function CLuaItemInfoWnd:GetUseButton( )
    if self.buttonTable.transform.childCount > 0 then
        for i=1,self.buttonTable.transform.childCount do
            local child = self.buttonTable.transform:GetChild(i-1)
            local label = child:Find("Label"):GetComponent(typeof(UILabel))
            if label.text == LocalString.GetString("使用") then
                return child.gameObject
            end
        end
        -- return self.buttonTable.transform:GetChild(0).gameObject
    end
    if CGuideMgr.Inst:IsInPhase(46) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
    return nil
end
function CLuaItemInfoWnd:GetEquipButton( )
    local find = nil
    do
        local i = 0
        while i < self.buttonTable.transform.childCount do
            local child = self.buttonTable.transform:GetChild(i)
            local label = CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel))
            if label ~= nil and (label.text == LocalString.GetString("装备")) then
                L10.Game.Guide.CGuideMessager.activeEvent = true
                find = child.gameObject
            end
            i = i + 1
        end
    end
    if find == nil then
        do
            local i = 0
            while i < self.buttonTable.transform.childCount do
                local child = self.buttonTable.transform:GetChild(i)
                if (CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel)).text == LocalString.GetString("更多")) then
                    L10.Game.Guide.CGuideMessager.activeEvent = false
                    find = child.gameObject
                end
                i = i + 1
            end
        end
    end
    --if (buttonTable.transform.childCount > 1)
    --    return buttonTable.transform.GetChild(1).gameObject;
    return find
end
function CLuaItemInfoWnd:GetDecorateButton( )
    local find = nil
    do
        local i = 0
        while i < self.buttonTable.transform.childCount do
            local child = self.buttonTable.transform:GetChild(i)
            local label = CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel))
            if label ~= nil and (label.text == LocalString.GetString("装修")) then
                find = child.gameObject
            end
            i = i + 1
        end
    end
    return find
end
function CLuaItemInfoWnd:GetAutoButton( )
    local find = nil
    do
        local i = 0
        while i < self.buttonTable.transform.childCount do
            local child = self.buttonTable.transform:GetChild(i)
            if (CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel)).text == LocalString.GetString("自动")) then
                L10.Game.Guide.CGuideMessager.activeEvent = true
                find = child.gameObject
            end
            i = i + 1
        end
    end
    if find == nil then
        do
            local i = 0
            while i < self.buttonTable.transform.childCount do
                local child = self.buttonTable.transform:GetChild(i)
                if (CommonDefs.GetComponentInChildren_Component_Type(child, typeof(UILabel)).text == LocalString.GetString("更多")) then
                    L10.Game.Guide.CGuideMessager.activeEvent = false
                    find = child.gameObject
                end
                i = i + 1
            end
        end
    end
    return find
end
function CLuaItemInfoWnd:GetGuideGo( methodName)
    if methodName == "GetSummonLingShouButton" then
        return self:GetSummonLingShouButton()
    elseif methodName == "GetUseButton" then
        return self:GetUseButton()
    elseif methodName == "GetEquipButton" then
        return self:GetEquipButton()
    elseif methodName == "GetAutoButton" then
        return self:GetAutoButton()
    elseif methodName == "GetDecorateButton" then
        return self:GetDecorateButton()
    else
        return nil
    end
end


--这里主要是处理剑心琉璃易容丹的显示,
function CLuaItemInfoWnd:ShowYiRongDan(item)--CCommonItem
    if item.Item.ExtraData.Data ~= nil and item.Item.ExtraData.Data.varbinary ~= nil then
        local raw = item.Item.ExtraData.Data.varbinary.StringData
        if raw and raw~="" then
            local splits = g_LuaUtil:StrSplit(raw, ":")
            local templateId = math.floor(tonumber(splits[1] or 0))
            local evolveGrade = math.floor(tonumber(splits[2] or 0))
            --因为一种形态的剑心琉璃需要有多重颜色和翅膀，灵兽表没办法填这么多种颜色和特效，所以就分散到几行灵兽数据里面去填写，灵兽表里面只是显示用的

            local lingshouData = LingShou_LingShou.GetData(templateId)
            local name=lingshouData and lingshouData.Name or ""
            local desc= LocalString.GetString("易容目标：") .. name
            self:AddItemToTable(desc, self.ValueColor)
            desc=""
            local keys = {}
            QuanMinPK_Stage.ForeachKey(function(k)
                table.insert( keys,k )
            end)

            local key = 0
            for i,v in ipairs(keys) do
                local designData=QuanMinPK_Stage.GetData(v)
                for tp, color, weaponColor, lingshouId, grade in string.gmatch(designData.Color2PiXiang, "(%d+),(%d+),(%d+),(%d+),(%d+);?") do
                    lingshouId=tonumber(lingshouId)
                    if lingshouId==templateId then
                        key=v
                        break
                    end
                end
            end

            if key==2 then
                desc=LocalString.GetString("本初")
            elseif key==3 then
                desc=LocalString.GetString("一变")
            elseif key==4 then
                desc=LocalString.GetString("二变")
            elseif key==5 then
                desc=LocalString.GetString("三变")
            end

            self:AddItemToTable(LocalString.GetString("形态：")..desc, self.ValueColor)
            desc = LocalString.GetString("灵兽类型：") .. CLingShouBaseMgr.GetLingShouTypeTempalteDesc(templateId)
            self:AddItemToTable(desc, self.ValueColor)

            for i,v in ipairs(keys) do
                local designData=QuanMinPK_Stage.GetData(v)
                for tp, color, weaponColor, lingshouId, grade in string.gmatch(designData.Color2PiXiang, "(%d+),(%d+),(%d+),(%d+),(%d+);?") do
                    color, weaponColor,lingshouId,grade=tonumber(color),tonumber(weaponColor),tonumber(lingshouId),tonumber(grade)
                    if lingshouId==templateId and evolveGrade==grade then
                        if designData.ColorEditable>0 then
                            local t=g_LuaUtil:StrSplit(designData.ColorDes,",")
                            self:AddItemToTable(SafeStringFormat3( LocalString.GetString("颜色：%s"),t[color] ), self.ValueColor)
                        else
                            self:AddItemToTable(LocalString.GetString("颜色：默认"), self.ValueColor)
                        end
                        if designData.WeaponColorEditable>0 then
                            local t=g_LuaUtil:StrSplit(designData.WeaponDes,",")
                            self:AddItemToTable(SafeStringFormat3( LocalString.GetString("翅膀：%s"),t[weaponColor] ), self.ValueColor)
                        else
                            self:AddItemToTable(LocalString.GetString("翅膀：无"), self.ValueColor)
                        end
                        break
                    end
                end
            end
        end
    end
end
function CLuaItemInfoWnd:ShowYiRongDan_Common(item)
    if item.Item.ExtraData.Data ~= nil and item.Item.ExtraData.Data.varbinary ~= nil then
        local raw = item.Item.ExtraData.Data.varbinary.StringData
        if raw~=nil and raw~="" then
            local splits = CommonDefs.StringSplit_ArrayChar(raw, ":")
            local templateId = math.floor(tonumber(splits[0] or 0))
            local evolveGrade = math.floor(tonumber(splits[1] or 0))

            local desc = (LocalString.GetString("易容目标：") .. CLingShouBaseMgr.GetEvolveGradeDesc(evolveGrade)) .. CLingShouBaseMgr.GetLingShouName(templateId)
            self:AddItemToTable(desc, self.ValueColor)
            desc = LocalString.GetString("灵兽类型：") .. CLingShouBaseMgr.GetLingShouTypeTempalteDesc(templateId)
            self:AddItemToTable(desc, self.ValueColor)
        end
    end
end


function CLuaItemInfoWnd:GetItemTipCallback(templateId)
    if self.m_ItemTipCallbacks[templateId] then
        return true, self.m_ItemTipCallbacks[templateId]
    end
    return false
end

function CLuaItemInfoWnd:Init4CityWarUnit(unitTemplateId)
    local data = CityWar_Unit.GetData(unitTemplateId)
    local level1Data = CityWar_Unit.GetData(math.floor(unitTemplateId/100) * 100 + 1)
    if data == nil or level1Data == nil then
        self.m_Wnd:Close()
        return
    end
    local typeNameList = CLuaCityWarMgr.UnitTypeName
    local typeId = level1Data.UIType

    self.nameLabel.color = Color.green
    self.iconTexture:Clear()
    self.qualitySprite.spriteName = Constants.DarkBlueItemCellBorder
    self.bindSprite.spriteName = nil
    self.iconTexture:LoadMaterial(data.Icon)

    self.disableSprite.enabled = false

    self.nameLabel.text = data.Name

    self.availableLevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), data.ID%100)
    self.categoryLabel.text = SafeStringFormat3(LocalString.GetString("%s类"), typeNameList[typeId - 1])

    Extensions.RemoveAllChildren(self.contentTable.transform)


    self:AddFirstTitleDesc(LocalString.GetString("资材消耗"), data.Material)
    self:AddFirstTitleDesc(LocalString.GetString("已摆放"), CLuaCityWarMgr:GetPlacedCountForTemplateId(unitTemplateId))
    self:AddFirstTitleDesc(LocalString.GetString("耐久"), tostring(CLuaCityWarMgr.GetCityWarUnitInfoForTip(data.MonsterId) or "-"))
    local unitType = data.Type
    local limitList = {}
    for level = 1, 5 do
        local levelDData = CityWar_CityGrade.GetData(level)
        if levelDData then
            local limit = CLuaCityWarMgr:GetTypeLimit(levelDData, unitType) or 0
            table.insert(limitList, limit)
        end
    end

    local msg = SafeStringFormat3(data.Desc, limitList[1], limitList[2], limitList[3], limitList[4], limitList[5])

    self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(msg, false), self.ValueColor)
    self:ShowButtons(self.baseItemInfo)
    self:LayoutWnd()
end

function CLuaItemInfoWnd:ValentineBaoXianLiBao(item)
    if LuaValentine2019Mgr.IsValentineBaoXianLiBao(item.TemplateId) then
        if item.Item.ExtraVarData ~= nil and item.Item.ExtraVarData.Data ~= nil then
            local raw = item.Item.ExtraVarData.Data
            local list = TypeAs(MsgPackImpl.unpack(raw), typeof(MakeGenericClass(List, Object)))
            if list and list.Count == 3 then
                local obtainTime = list[0]
                local partnerName = list[2]
                local span = CServerTimeMgr.Inst:GetZone8Time():Subtract(CServerTimeMgr.ConvertTimeStampToZone8Time(obtainTime))
                if span.TotalSeconds > 0 then
                    local totalSeconds = math.floor(span.TotalSeconds)
                    local hours = math.floor(totalSeconds / 3600)
                    local minutes = math.floor(totalSeconds % 3600 / 60)
                    local seconds = totalSeconds % 60
                    local timeStr = System.String.Format(LocalString.GetString("{0}小时{1}分{2}秒"), hours, minutes, seconds)
                    local desc = System.String.Format(LocalString.GetString("你与<{0}>的侠侣情缘保单\n当前已持有{1}"), partnerName, timeStr)
                    self:AddItemToTable(desc, Color.yellow)
                end
            end
        end
    end
end

function CLuaItemInfoWnd:CheckTemplatePeriodExpired(itemTemplate)
    if not System.String.IsNullOrEmpty(itemTemplate.Period) then
        local _, _, year, month, day, hour, minute = string.find(itemTemplate.Period, "(%d+)-(%d+)-(%d+)-(%d+)-(%d+)")
        if year and month and day and hour and minute then
            local expiredTime = CreateFromClass(DateTime, year, month, day, hour, minute, 0, DateTimeKind.Utc)
            if not CommonDefs.op_GreaterThan_DateTime_DateTime(expiredTime, CServerTimeMgr.Inst:GetZone8Time()) then
                return true
            end
        end
    end
    return false
end

function CLuaItemInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("UpdateItemUseTimes", self, "OnItemUseTimesUpdate")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("OnQueryPresentItemReceiverNameReturn", self, "OnQueryPresentItemReceiverNameReturn")
    g_ScriptEvent:AddListener("OnQueryDuoHunFanPlayerNameReturn", self, "OnQueryDuoHunFanPlayerNameReturn")
    g_ScriptEvent:AddListener("OnQueryHunPoPlayerNameReturn", self, "OnQueryHunPoPlayerNameReturn")
    g_ScriptEvent:AddListener("RequestChuanjiabaoArtistResult", self, "OnChuanjiabaoArtistResult")
    g_ScriptEvent:AddListener("OnQueryDiqiOwnerName", self, "OnQueryDiqiOwnerNameResult")
    g_ScriptEvent:AddListener("RequestZhushabiArtistName", self, "RequestZhushabiArtistName")
    g_ScriptEvent:AddListener("OnRequestChuanjiabaoModelArtistNameReturn", self, "ChuanjiabaoModelArtistNameReturn")
    g_ScriptEvent:AddListener("QueryDivinationLuckyTaskInfo", self, "QueryDivinationLuckyTaskInfoResult")
    g_ScriptEvent:AddListener(EnumMessage.UpdateCityWarUnitInfoForTip, self, "UpdateCityWarUnitInfoForTip")
    g_ScriptEvent:AddListener("RespDiskItemCreatorName", self, "RespDiskItemCreatorName")
    g_ScriptEvent:AddListener("SendPlayerName", self, "OnSendPlayerName") 
end

function CLuaItemInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("UpdateItemUseTimes", self, "OnItemUseTimesUpdate")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("OnQueryPresentItemReceiverNameReturn", self, "OnQueryPresentItemReceiverNameReturn")
    g_ScriptEvent:RemoveListener("OnQueryDuoHunFanPlayerNameReturn", self, "OnQueryDuoHunFanPlayerNameReturn")
    g_ScriptEvent:RemoveListener("OnQueryHunPoPlayerNameReturn", self, "OnQueryHunPoPlayerNameReturn")
    g_ScriptEvent:RemoveListener("RequestChuanjiabaoArtistResult", self, "OnChuanjiabaoArtistResult")
    g_ScriptEvent:RemoveListener("OnQueryDiqiOwnerName", self, "OnQueryDiqiOwnerNameResult")
    g_ScriptEvent:RemoveListener("RequestZhushabiArtistName", self, "RequestZhushabiArtistName")
    g_ScriptEvent:RemoveListener("OnRequestChuanjiabaoModelArtistNameReturn", self, "ChuanjiabaoModelArtistNameReturn")
    g_ScriptEvent:RemoveListener("QueryDivinationLuckyTaskInfo", self, "QueryDivinationLuckyTaskInfoResult")
    g_ScriptEvent:RemoveListener(EnumMessage.UpdateCityWarUnitInfoForTip, self, "UpdateCityWarUnitInfoForTip")
    g_ScriptEvent:RemoveListener("RespDiskItemCreatorName", self, "RespDiskItemCreatorName")
    g_ScriptEvent:RemoveListener("SendPlayerName", self, "OnSendPlayerName") 
end

function CLuaItemInfoWnd:RespDiskItemCreatorName(playerId, playerName)
  local item = CItemMgr.Inst:GetById(self.baseItemInfo.itemId)
  if item.TemplateId ~= 21030608 then return end
  if not item.Item.ExtraVarData or not item.Item.ExtraVarData.Data then return end

  local list = TypeAs(MsgPackImpl.unpack(item.Item.ExtraVarData.Data), typeof(MakeGenericClass(List, Object)))
  if list.Count < 4 then return end
  if playerId ~= list[3] then return end
  local musicName = list[1]

  self:AddSpace()
  self:AddItemToTable(SafeStringFormat3(CChatLinkMgr.TranslateToNGUIText(LocalString.GetString("#Y----来自#G%s#n的“%s”")), playerName, musicName), self.ValueColor)
  self:AddItemToTable(LocalString.GetString("有效期：剩余")..CLuaMusicBoxMgr:GetMusicLeftTimeStr(item.Item:GetRealExpiredTime()), Color.green)
  self:LayoutWnd()
end

function CLuaItemInfoWnd:UpdateCityWarUnitInfoForTip(monsterId, context)
    if CItemInfoMgr.showType == CItemInfoMgr.ShowType.CityWarUnit then
        local data = CityWar_Unit.GetData(CCityWarMgr.Inst.CurShowUnitTemplateId)
        if data and data.MonsterId == monsterId then
            self:Init4CityWarUnit(data.ID)
        end
    end
end

function CLuaItemInfoWnd:OnDestroy()
    CLuaItemInfoWnd.s_FromCheckGemGroupWnd = false
end
