local UITabBar = import "L10.UI.UITabBar"
local QnTabButton = import "L10.UI.QnTabButton"

CLuaSoulCoreWnd = class()

RegistClassMember(CLuaSoulCoreWnd,"m_Tabs")
RegistClassMember(CLuaSoulCoreWnd,"m_InfoView")
RegistClassMember(CLuaSoulCoreWnd,"m_AntiProView")
RegistClassMember(CLuaSoulCoreWnd,"m_TitleLab")
RegistClassMember(CLuaSoulCoreWnd,"m_Tab1")
RegistClassMember(CLuaSoulCoreWnd,"m_Tab2")
RegistClassMember(CLuaSoulCoreWnd,"m_Lab")
RegistClassMember(CLuaSoulCoreWnd,"m_Lock")
RegistClassMember(CLuaSoulCoreWnd,"m_LastLockState")

function CLuaSoulCoreWnd:Awake()
    self:InitComponents()
end

function CLuaSoulCoreWnd:InitComponents()
    self.m_Tabs         = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetComponent(typeof(UITabBar))
    self.m_InfoView     = self.transform:Find("SoulCoreInfoView").gameObject
    self.m_AntiProView  = self.transform:Find("AntiProfessionView").gameObject
    self.m_TitleLab     = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_Tab1         = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetChild(0):GetComponent(typeof(QnTabButton))
    self.m_Tab2         = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetChild(1):GetComponent(typeof(QnTabButton))
    self.m_Lab          = self.m_Tab2.transform:Find("Label").gameObject
    self.m_Lock         = self.m_Tab2.transform:Find("Lock").gameObject
end

function CLuaSoulCoreWnd:Init()
    self.m_LastLockState = false

    self.m_Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)
    self.m_Tabs:ChangeTab(LuaZongMenMgr.m_SoulCoreWndTabIndex or 0, false)
    LuaZongMenMgr.m_SoulCoreWndTabIndex = nil

    self:OnUpdateMainPlayerSoulCoreLevel()
end

function CLuaSoulCoreWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
end


function CLuaSoulCoreWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
end

function CLuaSoulCoreWnd:OnTabChange(index)
    if index == 1 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.SkillProp.SoulCore.Level < SoulCore_Settings.GetData().AntiprofessionUnlockLv then
        index = 0
        g_MessageMgr:ShowMessage("ANTIPROFESSION_LOCK")
        self.m_Tabs.selectedIndex = 0
        self.m_Tab1.Selected = true
        self.m_Tab2.Selected = false
    end
    self.m_TitleLab.text = index == 0 and LocalString.GetString("灵核") or LocalString.GetString("克符")
    self.m_InfoView:SetActive(index == 0)
    self.m_AntiProView:SetActive(index == 1)
end

function CLuaSoulCoreWnd:OnUpdateMainPlayerSoulCoreLevel()
    if CClientMainPlayer.Inst == nil then
        return
    end

    local isLock = CClientMainPlayer.Inst.SkillProp.SoulCore.Level < SoulCore_Settings.GetData().AntiprofessionUnlockLv
    if isLock == self.m_LastLockState then
        return
    end
    self.m_LastLockState = isLock

    self.m_Lab:SetActive(not isLock)
    self.m_Lock:SetActive(isLock)
end