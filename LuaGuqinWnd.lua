--import
local Object            = import "System.Object"
local GameObject		= import "UnityEngine.GameObject"
local Vector3           = import "UnityEngine.Vector3"

local DelegateFactory   = import "DelegateFactory"
local SoundManager      = import "SoundManager"

local CommonDefs        = import "L10.Game.CommonDefs"

local CUIManager        = import "L10.UI.CUIManager"
--define
CLuaGuqinWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaGuqinWnd, "Item1",		    GameObject)
RegistChildComponent(CLuaGuqinWnd, "Item2",		    GameObject)
RegistChildComponent(CLuaGuqinWnd, "Item3",		    GameObject)
RegistChildComponent(CLuaGuqinWnd, "Item4",		    GameObject)
RegistChildComponent(CLuaGuqinWnd, "Item5",		    GameObject)
RegistChildComponent(CLuaGuqinWnd, "CloseButton",	GameObject)
RegistChildComponent(CLuaGuqinWnd, "QinZhen",	    GameObject)
RegistChildComponent(CLuaGuqinWnd, "SelectFx",	    GameObject)
RegistChildComponent(CLuaGuqinWnd, "ClickFx",	    GameObject)

--RegistClassMember
RegistClassMember(CLuaGuqinWnd, "m_guideIndex")
RegistClassMember(CLuaGuqinWnd, "m_index")
RegistClassMember(CLuaGuqinWnd, "m_playOrder")
RegistClassMember(CLuaGuqinWnd, "m_items")
RegistClassMember(CLuaGuqinWnd, "delayTick")
RegistClassMember(CLuaGuqinWnd, "canClick")

CLuaGuqinWnd.TaskID = 0
CLuaGuqinWnd.OrderID = 0
function CLuaGuqinWnd.Show(taskid,orderid)
    CLuaGuqinWnd.TaskID = taskid
    CLuaGuqinWnd.OrderID = orderid
    CUIManager.ShowUI(CLuaUIResources.GuqinWnd)
end

--@region flow function

function CLuaGuqinWnd:Init()
    self.m_index = 0
    self.m_guideIndex = 1
    self.canClick = true

    self.m_items={}
    self.m_items[1] = {Ctrl = self.Item1,Audio = "UI_GuQin_Do"}
    self.m_items[2] = {Ctrl = self.Item2,Audio = "UI_GuQin_Re"}
    self.m_items[3] = {Ctrl = self.Item3,Audio = "UI_GuQin_Mi"}
    self.m_items[4] = {Ctrl = self.Item4,Audio = "UI_GuQin_So"}
    self.m_items[5] = {Ctrl = self.Item5,Audio = "UI_GuQin_La"}

    local orderdata = ZhuJueJuQing_GuQinOrder.GetData(CLuaGuqinWnd.OrderID)
    if orderdata == nil then--没有弹奏信息
         return
    end 
    local orderstr = orderdata.PlayOrder
    self.m_playOrder = g_LuaUtil:StrSplit(orderstr,",")

    local qinbotrans = self.QinZhen.transform
    for i=1,#self.m_items do
        local click = function(go)
            self:OnItemClick(i)
        end
        CommonDefs.AddOnClickListener(self.m_items[i].Ctrl,DelegateFactory.Action_GameObject(click),false)

        local qinbo = qinbotrans:Find("fx"..i).gameObject
        local xian = qinbotrans:Find("xian"..i).gameObject
        self.m_items[i].Fx = qinbo
        self.m_items[i].Xian = xian
    end

    self:SetguideItem(self.m_guideIndex)
end

function CLuaGuqinWnd:OnEnable()
    local click = function (go)
        self:OnCloseBtnClick()
    end
    CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(click),false)
    SoundManager.Inst:StopBGMusic()
end

function CLuaGuqinWnd:OnDisable()
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
    end
    SoundManager.Inst:StartBGMusic()
end

--@endregion

--[[
    @desc: 设置引导的物品
    author:CodeGize
    time:2019-07-12 11:12:45
    --@guideindex: 
    @return:
]]
function CLuaGuqinWnd:SetguideItem(guideindex)
    if self.m_index > 0 then
        self:ShowItemGlow(self.m_index,false)
    end
    self.m_guideIndex = guideindex
    if self.m_guideIndex > #self.m_playOrder then
        return
    end
    local audio = self.m_playOrder[self.m_guideIndex]
    if audio == "1" then
        self.m_index = 1
    elseif audio == "2" then
        self.m_index = 2
    elseif audio == "3" then
        self.m_index = 3
    elseif audio == "5" then
        self.m_index = 4
    elseif audio == "6" then
        self.m_index = 5
    end
    self:ShowItemGlow(self.m_index,true)
end

function CLuaGuqinWnd:ShowItemGlow(index,tf)
    if index <= 0 or index > #self.m_items then return end
    local ctrl = self.m_items[index].Ctrl
    --ctrl.transform:Find("glow").gameObject:SetActive(tf)
    self.SelectFx.transform.localPosition = ctrl.transform.localPosition
    self.SelectFx:SetActive(tf)
end

function CLuaGuqinWnd:OnItemClick(index)
    if index == self.m_index and self.canClick then
        self.canClick = false
        self:ShowItemGlow(index,false)
        SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/UI_GuQin/"..self.m_items[index].Audio,Vector3.zero, nil, 0)
        self.m_items[index].Fx:SetActive(true)
        self.m_items[index].Xian:SetActive(false)
        self.ClickFx.transform.localPosition = self.m_items[index].Ctrl.transform.localPosition
        self.ClickFx:SetActive(true)
        local tick = function()
            self.m_items[index].Fx:SetActive(false)
            self.m_items[index].Xian:SetActive(true)
            self.ClickFx:SetActive(false)
            if self.m_guideIndex == #self.m_playOrder then
                self:OnSuccess()
                CUIManager.CloseUI(CLuaUIResources.GuqinWnd)
            end
            self.canClick = true
            local nextindex = self.m_guideIndex + 1
            self:SetguideItem(nextindex)
        end

        self:DoTickOnce(tick,1)
    else
        g_MessageMgr:ShowMessage("GUQIN_NEED_INORDER")
    end
end

function CLuaGuqinWnd:OnSuccess()
    local data  = ZhuJueJuQing_GuQinOrder.GetData(CLuaGuqinWnd.OrderID)
    if data == nil then return end

    local empty = CreateFromClass(MakeGenericClass(List, Object))
    empty = MsgPackImpl.pack(empty)
    Gac2Gas.FinishClientTaskEventWithConditionId(CLuaGuqinWnd.TaskID, "tanguqin", empty, CLuaGuqinWnd.OrderID)
end

function CLuaGuqinWnd:DoTickOnce(callback,second)
    if self.delayTick then 
		UnRegisterTick(self.delayTick)
		self.delayTick = nil
	end
	self.delayTick = RegisterTickOnce(callback,second * 1000)
end

function CLuaGuqinWnd:OnCloseBtnClick()
    local msg = g_MessageMgr:FormatMessage("GUQIN_Close_TIP")
    local okFunc = function()
        CUIManager.CloseUI(CLuaUIResources.GuqinWnd)
    end
    g_MessageMgr:ShowOkCancelMessage(msg,okFunc,nil,nil,nil,false)
end
