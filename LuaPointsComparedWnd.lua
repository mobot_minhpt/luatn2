local UIProgressBar=import "UIProgressBar"

local LuaGameObject=import "LuaGameObject"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local Extensions = import "Extensions"


CLuaPointsComparedWnd=class()
RegistChildComponent(CLuaPointsComparedWnd, "PlayerInfo", GameObject)
RegistChildComponent(CLuaPointsComparedWnd, "Detail", GameObject)

RegistClassMember(CLuaPointsComparedWnd,"event")

function CLuaPointsComparedWnd:Awake()
	self.event={}
	self.event[1] = self.Detail.gameObject.transform:Find("event1").gameObject.transform
	self.event[2] = self.Detail.gameObject.transform:Find("event2").gameObject.transform
	self.event[3] = self.Detail.gameObject.transform:Find("event3").gameObject.transform
	self.event[4] = self.Detail.gameObject.transform:Find("event4").gameObject.transform
	self.event[5] = self.Detail.gameObject.transform:Find("event5").gameObject.transform
	
end

function CLuaPointsComparedWnd:Init()
	
end

function CLuaPointsComparedWnd:InitData(groupScoreList)
	local leftTotalScore = 0
	local rightTotalScore = 0
	local leftScore = {}
	local rightScore = {}
	for i=1,5 do
		leftScore[i]=0
		rightScore[i]=0
	end
	if groupScoreList then
		for i = 0, groupScoreList.Count - 1, 2 do
			leftScore[groupScoreList[i]]=groupScoreList[i + 1]
		end
	end
	local rightEvents = CLuaPointsDetailWnd.m_extraData
	local targetItemtf = CLuaPointsDetailWnd.m_targetData.transform

	    local rankIndex =CLuaPointsDetailWnd.m_targetIndex

	local scoreLabel=LuaGameObject.GetChildNoGC(targetItemtf,"label3").label
    local nameLabel=LuaGameObject.GetChildNoGC(targetItemtf,"NameLabel").label
    if CClientMainPlayer.Inst then
    	CommonDefs.GetComponent_Component_Type(self.PlayerInfo.gameObject.transform:Find("LeftNameLabel"), typeof(UILabel)).text=CClientMainPlayer.Inst.Name
    else
    	CommonDefs.GetComponent_Component_Type(self.PlayerInfo.gameObject.transform:Find("LeftNameLabel"), typeof(UILabel)).text=""
	end
	CommonDefs.GetComponent_Component_Type(self.PlayerInfo.gameObject.transform:Find("RightNameLabel"), typeof(UILabel)).text=nameLabel.text

   

    for i=1,5 do
    	rightScore[i]=rightEvents[(rankIndex-1)*6+i]
		local leftLabel = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("left"), typeof(UILabel))
		local rightLabel = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("right"), typeof(UILabel))
		local arrow = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("Arrow"), typeof(UISprite))
		

		if leftScore[i]+rightScore[i]==0 then
			leftLabel.text=SafeStringFormat3(LocalString.GetString("%2d%% %2d"), 50,leftScore[i])
			rightLabel.text=SafeStringFormat3(LocalString.GetString("%2d%% %2d"), 50,leftScore[i])
			CommonDefs.GetComponent_Component_Type(leftLabel.gameObject.transform:Find("ProgressBar"), typeof(UIProgressBar)).value=0.5
			CommonDefs.GetComponent_Component_Type(rightLabel.gameObject.transform:Find("ProgressBar"), typeof(UIProgressBar)).value=0.5
		else
			leftLabel.text=SafeStringFormat3(LocalString.GetString("%2d%% %2d"), 100*leftScore[i]/(leftScore[i]+rightScore[i]),leftScore[i])
			rightLabel.text=SafeStringFormat3(LocalString.GetString("%2d%% %2d"), 100*rightScore[i]/(leftScore[i]+rightScore[i]),rightScore[i])	
			CommonDefs.GetComponent_Component_Type(leftLabel.gameObject.transform:Find("ProgressBar"), typeof(UIProgressBar)).value=leftScore[i]/(leftScore[i]+rightScore[i])
			CommonDefs.GetComponent_Component_Type(rightLabel.gameObject.transform:Find("ProgressBar"), typeof(UIProgressBar)).value=rightScore[i]/(leftScore[i]+rightScore[i])
		end
		leftTotalScore=leftTotalScore+leftScore[i]
		rightTotalScore=rightTotalScore+rightScore[i]

		if leftScore[i]>rightScore[i] then
			Extensions.SetLocalRotationZ(arrow.transform, 0)
			arrow.color=Color.green
		elseif leftScore[i]==rightScore[i] then
			arrow.gameObject:SetActive(false)
		else
			Extensions.SetLocalRotationZ(arrow.transform, 180)
			arrow.color=Color.red
		end
	end
	CommonDefs.GetComponent_Component_Type(self.PlayerInfo.gameObject.transform:Find("LeftPointsLabel"), typeof(UILabel)).text=leftTotalScore
	CommonDefs.GetComponent_Component_Type(self.PlayerInfo.gameObject.transform:Find("RightPointsLabel"), typeof(UILabel)).text=rightTotalScore

	local arrow = CommonDefs.GetComponent_Component_Type(self.PlayerInfo.gameObject.transform:Find("Arrow"), typeof(UISprite))
	arrow.gameObject:SetActive(true)
	if leftTotalScore>rightTotalScore then
		Extensions.SetLocalRotationZ(arrow.transform, 0)
		arrow.color=Color.green
	elseif leftTotalScore==rightTotalScore then
		arrow.gameObject:SetActive(false)
	else
		Extensions.SetLocalRotationZ(arrow.transform, 180)
		arrow.color=Color.red
	end
end

function CLuaPointsComparedWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryMergeBattlePersonalScoresResult", self, "InitData")
	Gac2Gas.QueryMergeBattlePersonalScores()
end

function CLuaPointsComparedWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryMergeBattlePersonalScoresResult", self, "InitData")
	
end




