local CScene = import "L10.Game.CScene"

DuanWu2020Mgr = class()
DuanWu2020Mgr.m_rewardItemId = 0
DuanWu2020Mgr.m_hpPercent = 100
DuanWu2020Mgr.m_JiuGeShiHunResult = false
DuanWu2020Mgr.m_MonsterKillCount = 0

function DuanWu2020Mgr:JiuGeShiHunResult(bWin, hpPercent, rewardItemId)
    self.m_JiuGeShiHunResult = bWin
    if bWin then
        self.m_rewardItemId = rewardItemId
        self.m_hpPercent = hpPercent
    end
    local item = Item_Item.GetData(rewardItemId)
    if (item and bWin) or (not bWin)then
        CUIManager.ShowUI(CLuaUIResources.JiuGeShiHunResultWnd)
    end
end

function DuanWu2020Mgr:JiuGeShiHunHpPercent(hpPercent)
    self.m_hpPercent = hpPercent
    g_ScriptEvent:BroadcastInLua("JiuGeShiHunHpPercentUpdate")
end

function DuanWu2020Mgr:TopAndRightMenuWndInit(gamePlayId,wnd)
    if gamePlayId == JiuGeShiHun_Setting.GetData().GamePlayId then
        wnd.contentNode:SetActive(false)
        CUIManager.ShowUI(CLuaUIResources.JiuGeShiHunTopAndRightWnd)
    else
        CUIManager.CloseUI(CLuaUIResources.JiuGeShiHunTopAndRightWnd)
    end
end

function DuanWu2020Mgr:IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == JiuGeShiHun_Setting.GetData().GamePlayId then
           return true
        end
    end
    return false
end

function DuanWu2020Mgr:JiuGeShiHunMonsterKillCount(monsterKillCount, monsterTotalCount)
    self.m_MonsterKillCount = monsterKillCount
    g_ScriptEvent:BroadcastInLua("JiuGeShiHunMonsterKillCountUpdate")
end
