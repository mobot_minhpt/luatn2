local CButton = import "L10.UI.CButton"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local DelegateFactory  = import "DelegateFactory"
local Object = import "System.Object"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaShiTuRecommendWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuRecommendWnd, "RecommendView", "RecommendView", GameObject)
RegistChildComponent(LuaShiTuRecommendWnd, "RightGrid", "RightGrid", UIGrid)
RegistChildComponent(LuaShiTuRecommendWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaShiTuRecommendWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaShiTuRecommendWnd, "LeftBaiShiButton", "LeftBaiShiButton", CButton)
RegistChildComponent(LuaShiTuRecommendWnd, "AnnouncementButton", "AnnouncementButton", CButton)
RegistChildComponent(LuaShiTuRecommendWnd, "ChangeButton", "ChangeButton", CButton)
RegistChildComponent(LuaShiTuRecommendWnd, "RightBaiShiButton", "RightBaiShiButton", CButton)
RegistChildComponent(LuaShiTuRecommendWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaShiTuRecommendWnd, "Empty", "Empty", GameObject)
RegistChildComponent(LuaShiTuRecommendWnd, "EmptyLabel", "EmptyLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuRecommendWnd, "m_IsTudiView")
RegistClassMember(LuaShiTuRecommendWnd, "m_List")
RegistClassMember(LuaShiTuRecommendWnd, "m_PlayerIdIsWaiMen")

function LuaShiTuRecommendWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeftBaiShiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBaiShiButtonClick()
	end)


	
	UIEventListener.Get(self.AnnouncementButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAnnouncementButtonClick()
	end)


	
	UIEventListener.Get(self.ChangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeButtonClick()
	end)


	
	UIEventListener.Get(self.RightBaiShiButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBaiShiButtonClick()
	end)


	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

    --@endregion EventBind end
end

function LuaShiTuRecommendWnd:Init()
	self.Template.gameObject:SetActive(false)
	self.RecommendView.gameObject:SetActive(false)
	self.m_IsTudiView = CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.TU
	self.m_PlayerIdIsWaiMen = {}
	self.TopLabel.text = self.m_IsTudiView and LocalString.GetString("根据你在求师公告登记的信息，为你推荐的师父如下") or LocalString.GetString("根据你在收徒公告登记的信息，为你推荐的徒弟如下")
	self.EmptyLabel.text = self.m_IsTudiView and LocalString.GetString("暂无更多师父可供推荐") or LocalString.GetString("暂无更多徒弟可供推荐")
	self.LeftBaiShiButton.Text = self.m_IsTudiView and LocalString.GetString("拜师") or LocalString.GetString("收徒")
	self.AnnouncementButton.Text = self.m_IsTudiView and LocalString.GetString("求师公告") or LocalString.GetString("收徒公告")
	self.RightBaiShiButton.Text = self.m_IsTudiView and LocalString.GetString("一键拜师") or LocalString.GetString("一键收徒")
	self:Show()
end

function LuaShiTuRecommendWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendRecommendShiFu", self, "Show")
	g_ScriptEvent:AddListener("OnSendRecommendTuDi", self, "Show")
end

function LuaShiTuRecommendWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendRecommendShiFu", self, "Show")
	g_ScriptEvent:RemoveListener("OnSendRecommendTuDi", self, "Show")
end

function LuaShiTuRecommendWnd:Show()
	local recommendInfo = nil
	if self.m_IsTudiView then
		self.m_List = LuaShiTuMgr.m_RecommendShiFuList
		recommendInfo = LuaShiTuMgr.m_BestMatchRecommendShiFu
	else
		self.m_List = LuaShiTuMgr.m_RecommendTuDiList
		recommendInfo = LuaShiTuMgr.m_BestMatchRecommendTuDi
	end
	if recommendInfo.playerId then
		self:InitItem(self.RecommendView, recommendInfo)
		if recommendInfo.waiMen then
			self.RecommendView.transform:Find("Best").gameObject:SetActive(false)
		end
	end
	self.RecommendView.gameObject:SetActive(recommendInfo.playerId)
	Extensions.RemoveAllChildren(self.RightGrid.transform)
	local isHideRightBaiShiButton = true
	for i = 1,#self.m_List do
		local obj = NGUITools.AddChild(self.RightGrid.gameObject, self.Template.gameObject)
		self:InitItem(obj, self.m_List[i])
		obj.gameObject:SetActive(true)
		if not self.m_List[i].isApplied then
			isHideRightBaiShiButton = false
		end
	end
	self.RightBaiShiButton.Enabled = not isHideRightBaiShiButton
	self.RightBaiShiButton.Text = (isHideRightBaiShiButton and #self.m_List > 0) and LocalString.GetString("已申请") or (self.m_IsTudiView and LocalString.GetString("一键拜师") or LocalString.GetString("一键收徒"))
	self.Empty.gameObject:SetActive(#self.m_List == 0)
	self.RightGrid:Reposition()
end

function LuaShiTuRecommendWnd:InitItem(go, data)
	local portrait = go.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
	local personalSpaceButton = go.transform:Find("PersonalSpaceButton/Button").gameObject
	local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local tagTemplate = go.transform:Find("DeclarationLabelView/TagTemplate")
	local tagGrid = go.transform:Find("DeclarationLabelView/TagGrid"):GetComponent(typeof(UIGrid))
	local tagGrid2 = go.transform:Find("DeclarationLabelView/TagGrid2"):GetComponent(typeof(UIGrid))
	local lvLabel = go.transform:Find("LvLabel"):GetComponent(typeof(UILabel))
	local genderMale = go.transform:Find("GenderMale").gameObject
	local genderFemale = go.transform:Find("GenderFemale").gameObject
	local type1Root = go.transform:Find("Type1").gameObject
	local type1Label = go.transform:Find("Type1/Label"):GetComponent(typeof(UILabel))
	local type2Root = go.transform:Find("Type2").gameObject
	local type2Label = go.transform:Find("Type2/Label"):GetComponent(typeof(UILabel))
	local applyRoot = go.transform:Find("Apply").gameObject

	Extensions.RemoveAllChildren(tagGrid.transform)
	Extensions.RemoveAllChildren(tagGrid2.transform)
	tagTemplate.gameObject:SetActive(false)
	for i = 1, 4 do
		local id = data.tagList[i]
		if id then
			local obj = NGUITools.AddChild((i > 2) and tagGrid2.gameObject or tagGrid.gameObject, tagTemplate.gameObject)
			obj.gameObject:SetActive(true)
			local tagData = ShiTu_Label.GetData(id)
			local type = tagData.type
			local color = NGUIText.ParseColor32(ShiTu_Setting.GetData().ChooseLabelColor[type -1],0)	
			obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = tagData.text
			obj.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = color
		end
	end
	tagGrid.maxPerLine = #data.tagList > 1 and 2 or 1
	tagGrid2.maxPerLine = #data.tagList > 3 and 2 or 1
	tagGrid:Reposition()
	tagGrid2:Reposition()
	portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1), false)
	UIEventListener.Get(personalSpaceButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(data.playerId,0)
	end)
	nameLabel.text = data.playerName
	-- declarationLabel.text = data.declaration
	lvLabel.text = data and SafeStringFormat3("Lv.%d",data.level) or ""
	lvLabel.color = NGUIText.ParseColor24(data.hasFeiSheng and "fe7900" or (go == self.RecommendView.gameObject and "ffffff" or "373737"), 0)
	-- genderMale.gameObject:SetActive(EnumGender_lua.Male == data.gender)
	-- genderFemale.gameObject:SetActive(EnumGender_lua.Female == data.gender)
	genderMale.gameObject:SetActive(false)
	genderFemale.gameObject:SetActive(false)
	type1Label.text = self.m_IsTudiView and LocalString.GetString("可拜师") or LocalString.GetString("可收徒")
	type2Label.text = self.m_IsTudiView and LocalString.GetString("可入外门") or LocalString.GetString("可收外门")
	type1Root.gameObject:SetActive(not data.isApplied and not data.waiMen)
	type2Root.gameObject:SetActive(not data.isApplied and data.waiMen)
	self.m_PlayerIdIsWaiMen[data.playerId] = data.waiMen
	applyRoot.gameObject:SetActive(data.isApplied)
	local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
	CommonDefs.DictSet(dict, typeof(cs_string), "waiMen", typeof(Object), data.waiMen)
	if not data.isApplied then
		if self.m_IsTudiView then
			CommonDefs.DictSet(dict, typeof(cs_string), "BaiShiId", typeof(Object), data.playerId)
		else
			CommonDefs.DictSet(dict, typeof(cs_string), "ShouTuId", typeof(Object), data.playerId)
		end
	end
	if go ~= self.RecommendView.gameObject then
		UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
			local pos = Vector3(540, 0, 0)
			pos = self.transform:TransformPoint(pos)
			CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.ShiTu, EChatPanel.Undefined, nil, nil, dict, pos, AlignType1.Default)
		end)
	else
		self.LeftBaiShiButton.gameObject:SetActive(not data.isApplied)
		UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			local pos = Vector3(540, 0, 0)
			pos = self.transform:TransformPoint(pos)
			CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.ShiTu, EChatPanel.Undefined, nil, nil, dict, pos, AlignType1.Default)
		end)
	end
end

--@region UIEvent

function LuaShiTuRecommendWnd:OnLeftBaiShiButtonClick()
	local t = {}
	if self.m_IsTudiView then
		if LuaShiTuMgr.m_BestMatchRecommendShiFu.playerId then
			local isWaiMen = self.m_PlayerIdIsWaiMen[LuaShiTuMgr.m_BestMatchRecommendShiFu.playerId] and true or false
			t[LuaShiTuMgr.m_BestMatchRecommendShiFu.playerId] = isWaiMen
			LuaShiTuMgr.m_BestMatchRecommendShiFu.isApplied = true
		end
		Gac2Gas.RequestBaiShiToMultiPlayers(g_MessagePack.pack(t))
		g_ScriptEvent:BroadcastInLua("OnSendRecommendShiFu")
	else
		if LuaShiTuMgr.m_BestMatchRecommendTuDi.playerId then
			local isWaiMen = self.m_PlayerIdIsWaiMen[LuaShiTuMgr.m_BestMatchRecommendTuDi.playerId] and true or false
			t[LuaShiTuMgr.m_BestMatchRecommendTuDi.playerId] = isWaiMen
			LuaShiTuMgr.m_BestMatchRecommendTuDi.isApplied = true
		end
		Gac2Gas.RequestShouTuToMultiPlayers(g_MessagePack.pack(t))
		g_ScriptEvent:BroadcastInLua("OnSendRecommendTuDi")
	end
end

function LuaShiTuRecommendWnd:OnAnnouncementButtonClick()
	if self.m_IsTudiView then
		Gac2Gas.RequestMyBaiShiQuestionInfo()
	else
		Gac2Gas.RequestMyShouTuQuestionInfo()
	end
end

function LuaShiTuRecommendWnd:OnChangeButtonClick()
	if self.m_IsTudiView then
		Gac2Gas.RequestRecommendShiFu(false)
	else
		Gac2Gas.RequestRecommendTuDi(false)
	end
end

function LuaShiTuRecommendWnd:OnRightBaiShiButtonClick()
	local t = {}
	if self.m_IsTudiView then
		if LuaShiTuMgr.m_BestMatchRecommendShiFu.playerId then
			local isWaiMen = self.m_PlayerIdIsWaiMen[LuaShiTuMgr.m_BestMatchRecommendShiFu.playerId] and true or false
			t[LuaShiTuMgr.m_BestMatchRecommendShiFu.playerId] = isWaiMen
			LuaShiTuMgr.m_BestMatchRecommendShiFu.isApplied = true
		end
		for i = 1, #LuaShiTuMgr.m_RecommendShiFuList do
			local isWaiMen = self.m_PlayerIdIsWaiMen[LuaShiTuMgr.m_RecommendShiFuList[i].playerId] and true or false
			t[LuaShiTuMgr.m_RecommendShiFuList[i].playerId] = isWaiMen
			LuaShiTuMgr.m_RecommendShiFuList[i].isApplied = true
		end
		Gac2Gas.RequestBaiShiToMultiPlayers(g_MessagePack.pack(t))
		g_ScriptEvent:BroadcastInLua("OnSendRecommendShiFu")
	else
		if LuaShiTuMgr.m_BestMatchRecommendTuDi.playerId then
			local isWaiMen = self.m_PlayerIdIsWaiMen[LuaShiTuMgr.m_BestMatchRecommendTuDi.playerId] and true or false
			t[LuaShiTuMgr.m_BestMatchRecommendTuDi.playerId] = isWaiMen
			LuaShiTuMgr.m_BestMatchRecommendTuDi.isApplied = true
		end
		for i = 1, #LuaShiTuMgr.m_RecommendTuDiList do
			local isWaiMen = self.m_PlayerIdIsWaiMen[LuaShiTuMgr.m_RecommendTuDiList[i].playerId] and true or false
			t[LuaShiTuMgr.m_RecommendTuDiList[i].playerId] = isWaiMen
			LuaShiTuMgr.m_RecommendTuDiList[i].isApplied = true
		end
		Gac2Gas.RequestShouTuToMultiPlayers(g_MessagePack.pack(t))
		g_ScriptEvent:BroadcastInLua("OnSendRecommendTuDi")
	end
end

function LuaShiTuRecommendWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ShiTuRecommendWnd_ReadMe")
end


--@endregion UIEvent

