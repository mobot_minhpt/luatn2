local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaQiaoDuoRuYiSignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQiaoDuoRuYiSignWnd, "HuLuWaTabBar", "HuLuWaTabBar", UITabBar)
RegistChildComponent(LuaQiaoDuoRuYiSignWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaQiaoDuoRuYiSignWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaQiaoDuoRuYiSignWnd, "LockedLabel", "LockedLabel", UILabel)
RegistChildComponent(LuaQiaoDuoRuYiSignWnd, "SignBtn", "SignBtn", GameObject)

--@endregion RegistChildComponent end

function LuaQiaoDuoRuYiSignWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

	UIEventListener.Get(self.SignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignBtnClick()
	end)

    --@endregion EventBind end
	self.HuLuWaTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.HuLuWaTabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnHuLuWaChange(go, index)
    end), true)
end

function LuaQiaoDuoRuYiSignWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryRuYiHuluwaUnlockDataResult", self, "OnRefreshHuLuState")
end

function LuaQiaoDuoRuYiSignWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryRuYiHuluwaUnlockDataResult", self, "OnRefreshHuLuState")
end

function LuaQiaoDuoRuYiSignWnd:Init()
	self.HuLuWaTabBar.IgonreRepeatClick = false
	self:OnRefreshHuLuState()
	--self:InitSignButton()

	Gac2Gas.QueryRuYiHuluwaUnlockData()
end

function LuaQiaoDuoRuYiSignWnd:OnRefreshHuLuState()
	HuluBrothers_Transform.ForeachKey(function(key)
		local idx = key
		local data = HuluBrothers_Transform.GetData(idx)
		local unLock = LuaHuLuWa2022Mgr.m_QiaoDuoRuYiUnLockDic[idx]
		self:InitHuLu(idx,unLock)
	end)
	self:InitSignButton()
end

function LuaQiaoDuoRuYiSignWnd:InitHuLu(idx,unLock)
	local item = self.HuLuWaTabBar:GetTabGoByIndex(idx-1)
	if not item then  return end
	local lock = item.transform:Find("lock").gameObject
	lock:SetActive(unLock ~= true)
end

function LuaQiaoDuoRuYiSignWnd:InitSignButton()
	local needShow = true
	HuluBrothers_Transform.ForeachKey(function(key)
		local idx = key
		local unLock = LuaHuLuWa2022Mgr.m_QiaoDuoRuYiUnLockDic[idx]

		if not unLock then
			needShow = false
		end
	end)
	self.SignBtn:SetActive(needShow and not LuaHuLuWa2022Mgr.m_IsHideBtn)
	self.LockedLabel.gameObject:SetActive(not needShow)
end

function LuaQiaoDuoRuYiSignWnd:OnHuLuWaChange(go, index)
	local idx = index + 1
	local bUnLock = LuaHuLuWa2022Mgr.m_QiaoDuoRuYiUnLockDic[idx]
	LuaHuLuWa2022Mgr.OpenUnLockWnd(idx,bUnLock)
end

--@region UIEvent

function LuaQiaoDuoRuYiSignWnd:OnRankBtnClick()
	LuaHuLuWa2022Mgr.OpenQiaoDuoRuYiRankWnd()
end

function LuaQiaoDuoRuYiSignWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("QiaoDuoRuYi_Sign_Tip")
end

function LuaQiaoDuoRuYiSignWnd:OnSignBtnClick()
	Gac2Gas.RequestEnterRuYiHole()
end

--@endregion UIEvent

