local CButton = import "L10.UI.CButton"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CScene=import "L10.Game.CScene"

LuaWuYi2021MingXingPlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "Record", "Record", GameObject)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "Final", "Final", GameObject)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "TitleLabelFinal", "TitleLabelFinal", UILabel)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "HPBar", "HPBar", UISlider)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "LeaveButton", "LeaveButton", CButton)
RegistChildComponent(LuaWuYi2021MingXingPlayingWnd, "Bg", "Bg", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_CountLabels")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_Descs")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_Marks")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_Roots")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_HPUpdateListener")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_BossDeathListener")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_SkillListener")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_CountdownListener")
RegistClassMember(LuaWuYi2021MingXingPlayingWnd, "m_TriggerMark")

function LuaWuYi2021MingXingPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

	
	UIEventListener.Get(self.LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2021MingXingPlayingWnd:Init()
    self.m_CountLabels = {}
    self.m_Descs = {}
    self.m_Marks = {}
    self.m_Roots = {}
    self.m_TriggerMark = false

    for i = 1, 3 do
        self.m_Roots[i] = self.transform:Find("Anchor/Record/Content/" .. i).gameObject
        self.m_CountLabels[i] = self.transform:Find("Anchor/Record/Content/" .. i .. "/Count"):GetComponent(typeof(UILabel))
        self.m_Descs[i] = self.transform:Find("Anchor/Record/Content/" .. i .. "/Desc"):GetComponent(typeof(UILabel))
        self.m_Marks[i] = self.transform:Find("Anchor/Record/Content/" .. i .. "/Mark").gameObject
    end

    if LuaWuYi2021Mgr.m_Stage ~= nil and LuaWuYi2021Mgr.m_MonsterList~=nil and LuaWuYi2021Mgr.m_BossEngineId~=nil then
        self:SyncStatus(LuaWuYi2021Mgr.m_Stage, LuaWuYi2021Mgr.m_MonsterList, LuaWuYi2021Mgr.m_BossEngineId)
    end
end

function LuaWuYi2021MingXingPlayingWnd:SyncStatus(stage, list, bossEngineId)
    -- 处理STAGE从1开始
    local setting = WuYi2021_Setting.GetData()
    self.Record:SetActive(stage>=1 and stage<=3)
    self.Final:SetActive(stage >= 4)
    self.HPBar.gameObject:SetActive(stage == 4)
    self.Bg:SetActive(stage == 4)

    for i = 1, 3 do
        self.m_Roots[i]:SetActive(false)
    end

    if stage>=1 and stage <= 3 then
        self.TitleLabel.text = setting.JXMXBattleDesc[stage-1]
        for i = 0, list.Count - 2, 2 do
            if i <= 4 then
                local monsterTemplateId = list[i]
                local remainCount = list[i+1]
                local data = Monster_Monster.GetData(monsterTemplateId)
                self.m_Roots[i/2+1]:SetActive(true)
                if data then
                    local name = data.Name
                    self.m_Descs[i/2+1].text =  SafeStringFormat3(LocalString.GetString("击败%.6s"), name)
                end
                self.m_CountLabels[i/2+1].text = SafeStringFormat3(LocalString.GetString("（%d/3）"), 3 - remainCount)
                self.m_Marks[i/2+1]:SetActive(remainCount == 0)
            end
        end
    elseif stage >= 4 then
        self.TitleLabelFinal.text = setting.JXMXBattleDesc[stage - 1]
    end
end

function LuaWuYi2021MingXingPlayingWnd:UpdateRemainTime()
    local totalSeconds = 0
    if CScene.MainScene then
        totalSeconds = CScene.MainScene.ShowTime
    end

    if totalSeconds < 0 then
        totalSeconds = 0
    end

    if totalSeconds >= 3600 then
        self.RemainTimeLabel.text = SafeStringFormat3("[ACF9FF]%02d:%02d:%02d[-]", math.floor(totalSeconds/3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        self.RemainTimeLabel.text = SafeStringFormat3("[ACF9FF]%02d:%02d[-]", math.floor(totalSeconds / 60), math.floor(totalSeconds % 60))
    end
end


function LuaWuYi2021MingXingPlayingWnd:OnEnable( )
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncWuYi2021JXMXPlayInfo", self, "SyncStatus")
    if self.m_HPUpdateListener == nil then
        self.m_HPUpdateListener = DelegateFactory.Action_uint(function(engineId)
            if LuaWuYi2021Mgr.m_BossEngineId == engineId then
                local boss = CClientObjectMgr.Inst:GetObject(engineId)
                if boss then
                    local val = boss.Hp / boss.HpFull
                    self.HPBar.value = val
                    if val <= 0.1 and self.m_TriggerMark == false then
                        self.m_TriggerMark = true
                        LuaWuYi2021Mgr:ShowBossHighlight()
                    end
                end
            end
        end)
    end

    if self.m_SkillListener == nil then
        self.m_SkillListener = DelegateFactory.Action_uint(function(skillID)
            LuaWuYi2021Mgr:ShowBtnHighLight()
        end)
    end

    if self.m_CountdownListener == nil then
        self.m_CountdownListener = DelegateFactory.Action_int(function(leftTime)
            self:UpdateRemainTime()
        end)
    end

    EventManager.AddListenerInternal(EnumEventType.HpUpdate, self.m_HPUpdateListener)
    EventManager.AddListenerInternal(EnumEventType.CurrentHpFullUpdate, self.m_HPUpdateListener)
    EventManager.AddListenerInternal(EnumEventType.GetNewSkill, self.m_SkillListener)
    EventManager.AddListenerInternal(EnumEventType.SceneRemainTimeUpdate, self.m_CountdownListener)
end

function LuaWuYi2021MingXingPlayingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncWuYi2021JXMXPlayInfo", self, "SyncStatus")
    EventManager.RemoveListenerInternal(EnumEventType.HpUpdate, self.m_HPUpdateListener)
    EventManager.RemoveListenerInternal(EnumEventType.CurrentHpFullUpdate, self.m_HPUpdateListener)
    EventManager.RemoveListenerInternal(EnumEventType.GetNewSkill, self.m_SkillListener)
    EventManager.RemoveListenerInternal(EnumEventType.SceneRemainTimeUpdate, self.m_CountdownListener)

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
    LuaWuYi2021Mgr:Reset()
end

function LuaWuYi2021MingXingPlayingWnd:OnHideTopAndRightTipWnd( )
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@region UIEvent

function LuaWuYi2021MingXingPlayingWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaWuYi2021MingXingPlayingWnd:OnLeaveButtonClick()
    CGamePlayMgr.Inst:LeavePlay()
end


--@endregion UIEvent
