local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaUnLockVoiceAchievementWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaUnLockVoiceAchievementWnd, "AchievementName", "AchievementName", UILabel)
RegistChildComponent(LuaUnLockVoiceAchievementWnd, "AchievementDesc", "AchievementDesc", UILabel)
RegistChildComponent(LuaUnLockVoiceAchievementWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaUnLockVoiceAchievementWnd, "LinkLabel", "LinkLabel", GameObject)

--@endregion RegistChildComponent end

function LuaUnLockVoiceAchievementWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.LinkLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLinkLabelClick()
	end)


    --@endregion EventBind end
end

function LuaUnLockVoiceAchievementWnd:Init()
	if not LuaVoiceAchievementMgr.UnLockedAchievementId then
		CUIManager.CloseUI(CLuaUIResources.UnLockVoiceAchievementWnd)
		return
	end
	local achievement = Achievement_Achievement.GetData(LuaVoiceAchievementMgr.UnLockedAchievementId)
	if not achievement then
		CUIManager.CloseUI(CLuaUIResources.UnLockVoiceAchievementWnd)
		return
	end
	self.AchievementName.text = achievement.Name

	local vdata = VoiceAchievement_VoiceAchievement.GetDataBySubKey("AchievementID",LuaVoiceAchievementMgr.UnLockedAchievementId)
	local msg = achievement.RequirementDisplay
	if vdata then
		msg = vdata.ShareText
		local index2 = string.find(msg,"！")
		if index2 and index2 > 0 then
			msg = string.sub(msg,1,index2-1)
		end
	end
	self.AchievementDesc.text = msg
end

function LuaUnLockVoiceAchievementWnd:OnDisable()
	LuaVoiceAchievementMgr.UnLockedAchievementId = nil
end

--@region UIEvent

function LuaUnLockVoiceAchievementWnd:OnLinkLabelClick()
	local data = VoiceAchievement_VoiceAchievement.GetDataBySubKey("AchievementID",LuaVoiceAchievementMgr.UnLockedAchievementId)
	if data then
		LuaVoiceAchievementMgr.OpenAchievementWnd(data.ID)
		CUIManager.CloseUI(CLuaUIResources.UnLockVoiceAchievementWnd)
	end
end

--@endregion UIEvent

