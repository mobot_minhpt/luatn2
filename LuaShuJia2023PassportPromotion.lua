LuaShuJia2023PassportPromotion = class()

function LuaShuJia2023PassportPromotion:OnEnable()
    self:ShowPromotion()
end

function LuaShuJia2023PassportPromotion:ShowPromotion()
    local root = self.transform:Find("Root")
    if LuaActivityRedDotMgr:IsRedDot(51) then
        LuaActivityRedDotMgr:OnRedDotClicked(51)
        root.gameObject:SetActive(true)
        UIEventListener.Get(root:Find("Darkbg").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            root.gameObject:SetActive(false)
        end)
    else
        root.gameObject:SetActive(false)
    end
end
