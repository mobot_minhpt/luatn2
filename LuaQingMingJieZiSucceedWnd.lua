CLuaQingMingJieZiSucceedWnd = class()

RegistChildComponent(CLuaQingMingJieZiSucceedWnd, "GetAwardButton", GameObject)

function CLuaQingMingJieZiSucceedWnd:Awake()
    UIEventListener.Get(self.GetAwardButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        CLuaQingMing2020Mgr.NeedAlert = false
        CUIManager.ShowUI(CLuaUIResources.QingMingJieZiXiaoChouWnd)
        CUIManager.CloseUI(CLuaUIResources.QingMingJieZiSucceedWnd)
    end)
end
