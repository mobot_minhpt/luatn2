local CUILongPressButton=import "L10.UI.CUILongPressButton"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CUITexture=import "L10.UI.CUITexture"
local UIScrollView=import "UIScrollView"
local Item_Item=import "L10.Game.Item_Item"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local UIScrollViewIndicator=import "UIScrollViewIndicator"
local CItem=import "L10.Game.CItem"
--支持行为：长按、
CLuaCommonItemListWnd=class()
RegistClassMember(CLuaCommonItemListWnd,"m_Template")

function CLuaCommonItemListWnd:Init()
    self.m_Template=FindChild(self.transform,"Template").gameObject
    self.m_Template:SetActive(false)

    self.m_Template:AddComponent(typeof(CUILongPressButton))
    local cmp=self.m_Template:AddComponent(typeof(CItemCountUpdate))
    cmp.countLabel=self.m_Template.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))


    local grid=FindChild(self.transform,"Grid")
    for i,v in ipairs(CLuaCommonItemListMgr.m_DataList) do
        local go=NGUITools.AddChild(grid.gameObject,self.m_Template)
        go:SetActive(true)
        self:InitItem(go,v,i)
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

    local panel=FindChild(self.transform,"ScrollView"):GetComponent(typeof(UIPanel))
    panel:GetComponent(typeof(UIScrollView)).mCalculatedBounds=false
    
    local bg=FindChild(self.transform,"BackGround"):GetComponent(typeof(UISprite))
	bg.height = math.min(574, NGUIMath.CalculateRelativeWidgetBounds(grid).size.y + math.abs(panel.topAnchor.absolute) + math.abs(panel.bottomAnchor.absolute)+20)

    local bounds=NGUIMath.CalculateRelativeWidgetBounds(CLuaCommonItemListMgr.m_TargetTransform)
    local size=bounds.size
	local newNGUIWorldCenter = CUICommonDef.AdjustPosition(CLuaCommonItemListMgr.m_AlignType, bg.width, bg.height, CLuaCommonItemListMgr.m_TargetTransform.position, size.x,size.y)
    bg.transform.localPosition = newNGUIWorldCenter

    local indicator=FindChild(self.transform,"ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    indicator:Layout()
    
end

function CLuaCommonItemListWnd:InitItem(go,data,index)
    local tf=go.transform
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
    local templateId=data.itemTemplateId
    local itemTemplateData = Item_Item.GetData(templateId)
    icon:LoadMaterial(itemTemplateData.Icon)

    local mask=tf:Find("Mask").gameObject
    mask:SetActive(false)

    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel=tf:Find("DescLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=itemTemplateData.Name
    
    descLabel.text=CLuaCommonItemListMgr.GetCustomDescription(templateId) or CItem.GetItemDescription(templateId,true)


    local countUpdate=go:GetComponent(typeof(CItemCountUpdate))
    countUpdate.templateId=templateId
    countUpdate.format="{0}"

    local function OnCountChange(val)
        if val>=1 then
            mask:SetActive(false)
            countUpdate.format="{0}"
        else
            mask:SetActive(true)
            countUpdate.format="[ff0000]{0}[-]"
        end
    end
    countUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    countUpdate:UpdateCount()

    local function longPressCallback(go)
        if countUpdate.count==0 then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, go.transform, AlignType.Left)
        elseif countUpdate.count>0 then
            --消耗
            if CLuaCommonItemListMgr.m_Callback then
                CLuaCommonItemListMgr.m_Callback(index,CLuaCommonItemListMgr.m_DataList[index])
            end
        end
    end
    local longPressButton=go:GetComponent(typeof(CUILongPressButton))
    longPressButton.longPressShift = CLuaCommonItemListMgr.m_LongPressShift
    longPressButton.longPressShiftDuration = CLuaCommonItemListMgr.m_LongPressShiftDuration
    longPressButton.minDelta = 1.0 / CLuaCommonItemListMgr.m_LongPressMaxCallbackPerSecond
    longPressButton.callback=DelegateFactory.Action_GameObject(longPressCallback)
    
end


return CLuaCommonItemListWnd