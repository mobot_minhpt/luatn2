-- Auto Generated!!
local CJieBaiRankInfoMgr = import "L10.UI.CJieBaiRankInfoMgr"
local CJieBaiRankInfoWnd = import "L10.UI.CJieBaiRankInfoWnd"
local CJieBaiRankPlayerTemplate = import "L10.UI.CJieBaiRankPlayerTemplate"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
CJieBaiRankInfoWnd.m_Init_CS2LuaHook = function (this) 
    this.title.text = CJieBaiRankInfoMgr.jiebaiTitle
    Extensions.RemoveAllChildren(this.table.transform)
    Extensions.RemoveAllChildren(this.spriteTable.transform)
    this.spriteTemplate:SetActive(false)
    this.playerTemplate:SetActive(false)
    do
        local i = 0
        while i < CJieBaiRankInfoMgr.jiebaiInfo.Count do
            local instance = NGUITools.AddChild(this.table.gameObject, this.playerTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CJieBaiRankPlayerTemplate))
            if template ~= nil then
                template:Init(CJieBaiRankInfoMgr.jiebaiInfo[i])
            end
            i = i + 1
        end
    end
    this.table:Reposition()

    do
        local i = 0
        while i < CJieBaiRankInfoMgr.jiebaiInfo.Count - 1 do
            local instance = NGUITools.AddChild(this.spriteTable.gameObject, this.spriteTemplate)
            instance:SetActive(true)
            i = i + 1
        end
    end
    this.spriteTable:Reposition()

    this.background.width = (this.maxWidth + math.floor(this.table.padding.x) * 2) * CJieBaiRankInfoMgr.jiebaiInfo.Count + this.offset
end

