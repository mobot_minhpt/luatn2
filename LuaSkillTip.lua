local XiaLvPK_ColorSkill = import "L10.Game.XiaLvPK_ColorSkill"
local UIWidget = import "UIWidget"
local UIRoot = import "UIRoot"
local UILabel = import "UILabel"
local SkillKind = import "L10.Game.SkillKind"
local SkillCategory = import "L10.Game.SkillCategory"
local Skill_TempSkill = import "L10.Game.Skill_TempSkill"
local Skill_ColorSkill = import "L10.Game.Skill_ColorSkill"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Screen = import "UnityEngine.Screen"
local PlayerSettings = import "L10.Game.PlayerSettings"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local EventManager = import "EventManager"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumEventType = import "EnumEventType"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local Constants = import "L10.Game.Constants"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CButton = import "L10.UI.CButton"


CLuaSkillTip = class()
RegistClassMember(CLuaSkillTip,"template")
RegistClassMember(CLuaSkillTip,"body")
RegistClassMember(CLuaSkillTip,"background")
RegistClassMember(CLuaSkillTip,"contentTable")
RegistClassMember(CLuaSkillTip,"verticalGap")
RegistClassMember(CLuaSkillTip,"colorSystemRoot")
RegistClassMember(CLuaSkillTip,"colorBtns")
RegistClassMember(CLuaSkillTip,"identifySkillRoot")
RegistClassMember(CLuaSkillTip,"identifySkillButtonsBg")
RegistClassMember(CLuaSkillTip,"identifySkillButtonTemplate")
RegistClassMember(CLuaSkillTip,"identifySkillButtonTable")
RegistClassMember(CLuaSkillTip,"curSkillId")
RegistClassMember(CLuaSkillTip,"m_OriginSkillId")
RegistClassMember(CLuaSkillTip,"m_IsTianFuSkill")
RegistClassMember(CLuaSkillTip,"m_ModifySkillLevel")

CLuaSkillTip.namecolor = "FFED5F"
CLuaSkillTip.valuecolor = "E8D0AA"
CLuaSkillTip.lockcolor = "FF0000"
CLuaSkillTip.nextLevelDescColor = "847E78"
CLuaSkillTip.curLevelColor = "ACF9FF"

function CLuaSkillTip:Awake()
    self.template = self.transform:Find("Body/Template").gameObject
    self.body = self.transform:Find("Body"):GetComponent(typeof(UIScrollView))
    self.background = self.transform:Find("Background"):GetComponent(typeof(UIWidget))
    self.contentTable = self.transform:Find("Body/Table"):GetComponent(typeof(UITable))
self.verticalGap = 40
    self.colorSystemRoot = self.transform:Find("ColorSystem").gameObject
self.colorBtns = {}
local colorbtnTf = self.transform:Find("ColorSystem/Table")
self.colorBtns[1] = colorbtnTf:GetChild(0):GetComponent(typeof(CButton))
self.colorBtns[2] = colorbtnTf:GetChild(1):GetComponent(typeof(CButton))
self.colorBtns[3] = colorbtnTf:GetChild(2):GetComponent(typeof(CButton))
self.colorBtns[4] = colorbtnTf:GetChild(3):GetComponent(typeof(CButton))

    self.identifySkillRoot = self.transform:Find("IdentifySkill").gameObject
    self.identifySkillButtonsBg = self.transform:Find("IdentifySkill/ButtonBg"):GetComponent(typeof(UIWidget))
    self.identifySkillButtonTemplate = self.transform:Find("IdentifySkill/ButtonTemplate").gameObject
    self.identifySkillButtonTable = self.transform:Find("IdentifySkill/ButtonTable"):GetComponent(typeof(UITable))
    self.curSkillId = 0
    self.m_OriginSkillId = 0
    self.m_IsTianFuSkill = false
    self.m_ModifySkillLevel = 0

end

function CLuaSkillTip:ShowName( skill, skillProp, playerLevel) 
    local skillId = skill.ID
    if CSkillInfoMgr.skillInfo.feisheng and self.m_ModifySkillLevel ~= self.m_OriginSkillId%100 then
        local title = self.m_IsTianFuSkill and LocalString.GetString("[%s]%s[-][%s] [%s]%s级[-]") or LocalString.GetString("[%s]%s[-][%s] [%s]修正%s级[-]")
        self:AddItemToTable(SafeStringFormat3(title, 
            CLuaSkillTip.namecolor, 
            skill.Name, 
            CLuaSkillTip.curLevelColor, 
            Constants.ColorOfFeiSheng, 
            skill.Level))
    else
        self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[%s]%s[-][%s] %s级[-]"), 
            CLuaSkillTip.namecolor, 
            skill.Name, 
            CLuaSkillTip.curLevelColor, 
            skill.Level))
    end
end
function CLuaSkillTip:ShowNeedLevel( skill, playerLevel) 
    if CLuaDesignMgr.Skill_AllSkills_GetIsIdentifySkill(skill) then
        return
    end
    --鉴定技能不显示需求等级

    if CSkillInfoMgr.skillInfo.feisheng and self.m_ModifySkillLevel ~= self.m_OriginSkillId%100 then
        if skill.Level <= Constants.MaxSkillUpgradeLevel then
            local title = self.m_IsTianFuSkill and LocalString.GetString("需求等级:") or LocalString.GetString("修正需求等级:")
            if skill.PlayerLevel > playerLevel then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    Constants.ColorOfFeiSheng, 
                    title, 
                    CLuaSkillTip.lockcolor, 
                    skill.PlayerLevel))
            else
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    Constants.ColorOfFeiSheng, 
                    title, 
                    CLuaSkillTip.valuecolor, 
                    skill.PlayerLevel))
            end
        end
    else
        if skill.Level <= Constants.MaxSkillUpgradeLevel then
            if skill.PlayerLevel > playerLevel then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    CLuaSkillTip.namecolor, 
                    LocalString.GetString("需求等级:"), 
                    CLuaSkillTip.lockcolor, 
                    skill.PlayerLevel))
            else
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    CLuaSkillTip.namecolor, 
                    LocalString.GetString("需求等级:"), 
                    CLuaSkillTip.valuecolor, 
                    skill.PlayerLevel))
            end
        end
    end
end
function CLuaSkillTip:ShowShiMenMiShuNeedLevel(skill, playerLevel, soulCoreLevel)
    local lv = math.floor(skill.ID % 100)
    local lvData = SoulCore_ShiMenMiShuLevel.GetData(lv)
    local needSoulCoreLv = lvData.SoulCoreLevel
    local needPlayerLevel = lvData.PlayerLevel

    self:ShowNeedSoulCoreLvAndPlayerLv(skill, playerLevel, soulCoreLevel, needPlayerLevel, needSoulCoreLv)
end

function CLuaSkillTip:ShowHpSkillNeedLevel(skill, playerLevel, soulCoreLevel)
    local lvData = SoulCore_HpSkills.GetData(skill.ID)
    local needSoulCoreLv = lvData.SoulCoreLearnLv
    local needPlayerLevel = lvData.PlayerLearnLv

    self:ShowNeedSoulCoreLvAndPlayerLv(skill, playerLevel, soulCoreLevel, needPlayerLevel, needSoulCoreLv)
end

function CLuaSkillTip:ShowNeedSoulCoreLvAndPlayerLv(skill, playerLevel, soulCoreLevel, needPlayerLevel, needSoulCoreLv)
    if CSkillInfoMgr.skillInfo.feisheng and self.m_ModifySkillLevel ~= self.m_OriginSkillId%100 then
        if skill.Level <= SoulCore_ShiMenMiShuLevel.GetDataCount() then
            if needPlayerLevel > playerLevel then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    Constants.ColorOfFeiSheng, 
                    LocalString.GetString("修正需求等级:"), 
                    CLuaSkillTip.lockcolor, 
                    needPlayerLevel))
            else
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    Constants.ColorOfFeiSheng, 
                    LocalString.GetString("修正需求等级:"), 
                    CLuaSkillTip.valuecolor, 
                    needPlayerLevel))
            end

            if needSoulCoreLv > soulCoreLevel then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    Constants.ColorOfFeiSheng, 
                    LocalString.GetString("修正需求灵核等级:"), 
                    CLuaSkillTip.lockcolor, 
                    needSoulCoreLv))
            else
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    Constants.ColorOfFeiSheng, 
                    LocalString.GetString("修正需求灵核等级:"), 
                    CLuaSkillTip.valuecolor, 
                    needSoulCoreLv))
            end
        end
    else
        if skill.Level <= SoulCore_ShiMenMiShuLevel.GetDataCount() then
            if needPlayerLevel > playerLevel then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    CLuaSkillTip.namecolor, 
                    LocalString.GetString("需求等级:"), 
                    CLuaSkillTip.lockcolor, 
                    needPlayerLevel))
            else
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    CLuaSkillTip.namecolor, 
                    LocalString.GetString("需求等级:"), 
                    CLuaSkillTip.valuecolor, 
                    needPlayerLevel))
            end

            if needSoulCoreLv > soulCoreLevel then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    CLuaSkillTip.namecolor, 
                    LocalString.GetString("需求灵核等级:"), 
                    CLuaSkillTip.lockcolor, 
                    needSoulCoreLv))
            else
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", 
                    CLuaSkillTip.namecolor, 
                    LocalString.GetString("需求灵核等级:"), 
                    CLuaSkillTip.valuecolor, 
                    needSoulCoreLv))
            end
        end
    end
end


function CLuaSkillTip:InitFeiShengInfo( pskillId, playerLevel, skillProp) 
    self.m_IsTianFuSkill = false

    local clsId = math.floor(self.curSkillId / 100)
    local skillData = Skill_AllSkills.GetData(pskillId)
    if skillData ~= nil and skillData.Kind == 2 then
        self.m_IsTianFuSkill = true
    end

    --mptz天赋技能可以调整 
    --某个装备的词条会影响天赋技能的等级
    if self.m_IsTianFuSkill and (CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerMPTZ or CLuaSkillInfoWnd.FromAttrGroup) then
        self.m_OriginSkillId = self.curSkillId
    else
        self.m_OriginSkillId = skillProp:GetOriginalSkillIdByCls(clsId)
    end

    local checked, realLv = LuaWuLiangMgr:CheckSkillModify(clsId, self.curSkillId % 100)
    if checked  and realLv ~= self.curSkillId % 100 then
        self.m_ModifySkillLevel = realLv
        if self.m_ModifySkillLevel>0 then
            self.curSkillId = clsId * 100 + self.m_ModifySkillLevel
        end
        return
    end

    if LuaZongMenMgr:IsShiMenMiShuCls(clsId) then
        self.m_ModifySkillLevel = LuaZongMenMgr:GetPlayerAdjustShiMenMiShuSkillLv(self.m_OriginSkillId ,playerLevel, skillProp)
        if self.m_ModifySkillLevel>0 then
            self.curSkillId = clsId * 100 + self.m_ModifySkillLevel
        end
        return
    end

    if SoulCore_Settings.GetData().HpSkills == clsId then
        self.m_OriginSkillId = self.curSkillId
        self.m_ModifySkillLevel = LuaZongMenMgr:GetAdjustHpSkillLevel(self.m_OriginSkillId % 100, playerLevel)
        if self.m_ModifySkillLevel>0 then
            self.curSkillId = clsId * 100 + self.m_ModifySkillLevel
        end
        return
    end

    self.m_ModifySkillLevel = skillProp:GetFeiShengModifyLevel(self.m_OriginSkillId, skillData.Kind, playerLevel)
    if self.m_IsTianFuSkill and (CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerMPTZ or CLuaSkillInfoWnd.FromAttrGroup) then
        self.curSkillId = clsId*100 + self.m_ModifySkillLevel
    else
        local originSkillIdWithDeltaLevel = skillProp:GetSkillIdWithDeltaByCls(clsId, playerLevel)
        --这里有可能显示的技能是临时的玩法技能，这个时候计算出来的id是0
        if originSkillIdWithDeltaLevel>0 then
            self.curSkillId = originSkillIdWithDeltaLevel
        end
    end

    self:AdjustTianFuSkillLevelByShenBing(self.curSkillId, self.m_IsTianFuSkill, self.m_ModifySkillLevel, skillData, playerLevel, skillProp)
end

function CLuaSkillTip:AdjustTianFuSkillLevelByShenBing(skillId, isTianFuSkill, feishengModifyLevel, skillData, playerLevel, skillProp)
    --仿照之前章清的一段处理这里针对主角技能更换界面的天赋技能做一个技能修正 refs #100242，处理方法参考了其修改的CTianFuSkillItemCell.cs(svn revision 334520)
    if isTianFuSkill then
        local clsId = math.floor(skillId /100)
        local tianFuSkillDeltaLevel = skillProp:GetTianFuSkilleltaLevel(clsId, playerLevel)
        local totalLevel=feishengModifyLevel
        if tianFuSkillDeltaLevel>0 and totalLevel>0 then
            local totalLevel = math.min(skillData._Before_Extend_Level, totalLevel + tianFuSkillDeltaLevel)
            self.m_ModifySkillLevel = totalLevel
            self.curSkillId = clsId * 100 + self.m_ModifySkillLevel
        end
    end
end

function CLuaSkillTip:OnTianFuSkillLevelIsZero( )
    local skill = Skill_AllSkills.GetData(self.m_OriginSkillId)
    self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[%s]%s[-][%s] [%s]修正%s级[-]"), 
        CLuaSkillTip.namecolor, 
        skill.Name, 
        CLuaSkillTip.curLevelColor, 
        Constants.ColorOfFeiSheng, 
        self.m_ModifySkillLevel))
    self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[c][%s]该天赋暂不生效[-][/c]"), Constants.ColorOfFeiSheng))
    self.colorSystemRoot:SetActive(false)
    self.contentTable:Reposition()
    self:CheckIfShowIdentifyAction()
    self:LayoutWnd()
end
function CLuaSkillTip:InitSkill( pskillId, playerLevel, xiuweiGrade, skillProp) 
    self.curSkillId = pskillId
    --bool tianfuIs0 = false;
    -- print(pskillId,CSkillInfoMgr.skillInfo.feisheng)
    if CSkillInfoMgr.skillInfo.feisheng then
        --天赋0级特殊处理
        self:InitFeiShengInfo(pskillId, playerLevel, skillProp) --注释:这地方的实际逻辑是在飞升的情况下，根据不同条件会修改curSkillId
    end


    Extensions.RemoveAllChildren(self.contentTable.transform)

    local skill = Skill_AllSkills.GetData(self.curSkillId)
    --天赋技能0级进这个分支
    if CSkillInfoMgr.skillInfo.feisheng and self.m_IsTianFuSkill and self.m_ModifySkillLevel == 0 then
        self:OnTianFuSkillLevelIsZero()
        return
    end

    local preSkills = skill.AdjustedPreSkills
    local preSkillStr = ""
    local builder = ""-- NewStringBuilderWraper(StringBuilder)
    if preSkills ~= nil and preSkills.Length > 0 then
        do
            local i = 0
            while i < preSkills.Length do
                builder = builder .. "\n"
                local preSkill = Skill_AllSkills.GetData(preSkills[i][0] * 100 + 1)
                if skillProp:IsSkillClsExist(preSkills[i][0]) then
                    local preskilllevel = skillProp:GetSkillIdWithDeltaByCls(preSkills[i][0], playerLevel) - preSkills[i][0] * 100
                    if preskilllevel >= preSkills[i][1] then
                        builder = builder .. "["
                        builder = builder .. CLuaSkillTip.valuecolor
                        builder = builder .. "]"
                    else
                        builder = builder .. "["
                        builder = builder .. CLuaSkillTip.lockcolor
                        builder = builder .. "]"
                    end
                else
                    builder = builder .. "["
                    builder = builder .. CLuaSkillTip.lockcolor
                    builder = builder .. "]"
                end
                builder = builder .. preSkill.Name

                builder = builder .. " "
                builder = builder .. SafeStringFormat3(LocalString.GetString("%s级"), preSkills[i][1])
                builder = builder .. "[-]"
                i = i + 1
            end
        end
    end
    preSkillStr = builder -- ToStringWrap(builder)

    if skill ~= nil then
        self:ShowName(skill, skillProp, playerLevel)
        self:ShowIdentifySkillBodyPosition(skill)
        -- print(skill.ID)
        local skillHead = math.floor(self.curSkillId / 100000) 
        if skillHead == 921 or skillHead == 924 then
            self:AddLingShouBabySkillNeedLevel(self.curSkillId, playerLevel)
            self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.valuecolor, CChatLinkMgr.TranslateToNGUIText(skill.Display, false)))

            local next = Skill_AllSkills.GetData(Skill_AllSkills.GetNextLevelSkillId(skill.ID))
            if next ~= nil then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.namecolor, LocalString.GetString("下一级")))
                self:AddLingShouBabySkillNeedLevel(self.curSkillId + 1, playerLevel)
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.nextLevelDescColor, CChatLinkMgr.TranslateToNGUIText(next.Display, false)))
            end
            self:AddLingShouBabySkillDesc()
        elseif skillHead == 960 then
            -- 仙职技能
            self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.valuecolor, CChatLinkMgr.TranslateToNGUIText(skill.Display, false)))
            if skill.Category ~= 0 then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.valuecolor, SafeStringFormat3(LocalString.GetString("冷却时间: %s小时"), math.floor(skill.CD / 3600))))
            end
            

            local nextSkill = Skill_AllSkills.GetData(Skill_AllSkills.GetNextLevelSkillId(skill.ID))
            if nextSkill ~= nil then
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.namecolor, LocalString.GetString("下一级")))
                
                self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.nextLevelDescColor, CChatLinkMgr.TranslateToNGUIText(nextSkill.Display, false)))
                if nextSkill.Category ~= 0 then
                    self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.nextLevelDescColor, SafeStringFormat3(LocalString.GetString("冷却时间: %s小时"), math.floor(nextSkill.CD / 3600))))
                end
            end

        else
            if skill.EKind == SkillKind.TianFuSkill then
                if skill.NeedXiuweiLevel > xiuweiGrade then
                    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", CLuaSkillTip.namecolor, LocalString.GetString("需要修为:"), CLuaSkillTip.lockcolor, skill.NeedXiuweiLevel))
                else
                    self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", CLuaSkillTip.namecolor, LocalString.GetString("需要修为:"), CLuaSkillTip.valuecolor, skill.NeedXiuweiLevel))
                end
            end

            self:ShowSkillDesc(skill, playerLevel, xiuweiGrade, skillProp, preSkillStr)
        end

        self:InitColorSystem(self.curSkillId, playerLevel, xiuweiGrade, skillProp)

        self:ShowIdentifySkillExtDesc(skill)
    end
    self.contentTable:Reposition()
    self:CheckIfShowIdentifyAction()
    self:LayoutWnd()
end


function CLuaSkillTip:ShowSkillDesc( skill, playerLevel, xiuweiGrade, skillProp, preSkillStr) 
    local cls = math.floor(skill.ID / 100)
    if not CSkillInfoMgr.isXiaLvSkill then
        if LuaZongMenMgr:IsShiMenMiShuCls(cls) then
            self:ShowShiMenMiShuNeedLevel(skill, playerLevel, LuaZongMenMgr:GetPlayerAdjustSoulCoreLevel(0, playerLevel, skillProp))
        elseif SoulCore_Settings.GetData().HpSkills == cls then
            self:ShowHpSkillNeedLevel(skill, playerLevel, LuaZongMenMgr:GetPlayerAdjustSoulCoreLevel(0, playerLevel, skillProp))
        else
            self:ShowNeedLevel(skill, playerLevel)
        end
    end
    local attackMode = CLuaDesignMgr.Skill_AllSkills_GetAttackModeDisplay(skill)

    if skill.ECategory ~= SkillCategory.Passive then
        self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", CLuaSkillTip.namecolor, LocalString.GetString("消耗法力:"), CLuaSkillTip.valuecolor, CSkillMgr.Inst:GetSkillMpWithDelta(skill.ID)))
        if not CLuaDesignMgr.Skill_AllSkills_GetIsIdentifySkill(skill) and skill.Range > 2 then
            self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%.2f[-]", CLuaSkillTip.namecolor, LocalString.GetString("施法距离:"), CLuaSkillTip.valuecolor, skill.Range))
        end
        self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[%s]%s[-][%s]%s秒[-]"), CLuaSkillTip.namecolor, LocalString.GetString("冷却时间:"), CLuaSkillTip.valuecolor, skill.CDString))
        if not CLuaDesignMgr.Skill_AllSkills_GetIsIdentifySkill(skill) then
            self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]",CLuaSkillTip.namecolor, LocalString.GetString("攻击类型:"), CLuaSkillTip.valuecolor, attackMode))
        end
    end

    if preSkillStr and preSkillStr~="" then
        self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", CLuaSkillTip.namecolor, LocalString.GetString("前置技能:"), CLuaSkillTip.valuecolor, preSkillStr))
    end

    local default
    if PlayerSettings.SkillSummaryDescEnabled and skill.Display_jian and skill.Display_jian~="" then
        default = skill.Display_jian
    else
        default = skill.Display
    end
    if SoulCore_Settings.GetData().HpSkills == cls then
        local skillData = Skill_AllSkills.GetData(skill.ID)
        default = default .. LuaZongMenMgr:GetHpSkillExtraDisplay(skillData)
    end
    self:AddItemToTable(SafeStringFormat3("[%s]%s[-]", CLuaSkillTip.valuecolor, CChatLinkMgr.TranslateToNGUIText(default, false)))

    self:CheckAndAddUnlearn(self.curSkillId, playerLevel, xiuweiGrade, skillProp)
end
function CLuaSkillTip:AddLingShouBabySkillDesc( )
    if CLingShouMgr.IsLingShouBabySkill(self.curSkillId) then
        local msg = g_MessageMgr:FormatMessage("LingShouBabySkill_Explain")
        self:AddItemToTable(CChatLinkMgr.TranslateToNGUIText(msg, false))
    end
end
function CLuaSkillTip:AddLingShouBabySkillNeedLevel( skillId, playerLevel) 
    if CLingShouMgr.IsLingShouBabySkill(skillId) then
        --string namecolor = "FFED5F";
        --string valuecolor = "E8D0AA";

        local skill = Skill_AllSkills.GetData(skillId)
        if skill ~= nil then
            self:AddItemToTable(SafeStringFormat3("[%s]%s[-][%s]%s[-]", CLuaSkillTip.namecolor, LocalString.GetString("需求等级:"), CLuaSkillTip.valuecolor, skill.PlayerLevel))
        end
    end
end
function CLuaSkillTip:ShowIdentifySkillBodyPosition( skill) 

    if skill ~= nil and CLuaDesignMgr.Skill_AllSkills_GetIsIdentifySkill(skill) then
        local source = CSkillInfoMgr.skillInfo.identifySkillSource
        local pos = 0
        if source == CSkillInfoMgr.EnumIdentifySkillSource.MainPlayer then
            local item = CItemMgr.Inst:GetById(CSkillInfoMgr.skillInfo.identifySkillEquipId)
            if item ~= nil and item.IsEquip then
                local template = EquipmentTemplate_Equip.GetData(item.TemplateId)
                pos = template.Type
                --无需知道具体是哪个戒指或者手镯
            end
        elseif source == CSkillInfoMgr.EnumIdentifySkillSource.OtherPlayer then
            CommonDefs.DictIterate(CPlayerInfoMgr.PlayerInfo.pos2equipDict, DelegateFactory.Action_object_object(function (___key, ___value) 
                local pair = {}
                pair.Key = ___key
                pair.Value = ___value
                if pair.Value.Id == CSkillInfoMgr.skillInfo.identifySkillEquipId then
                    pos = pair.Key
                    return
                end
            end))
        end
        self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[%s]%s[-][%s]%s[-]"), CLuaSkillTip.namecolor, LocalString.GetString("装备部位:"), CLuaSkillTip.valuecolor, CItemMgr.Inst:GetBodyPositionName(pos)))
    end
end
function CLuaSkillTip:ShowIdentifySkillExtDesc( skill) 

    if skill ~= nil and CLuaDesignMgr.Skill_AllSkills_GetIsIdentifySkill(skill) then
        local equip = nil
        if CSkillInfoMgr.skillInfo.identifySkillSource == CSkillInfoMgr.EnumIdentifySkillSource.MainPlayer then
            local item = CItemMgr.Inst:GetById(CSkillInfoMgr.skillInfo.identifySkillEquipId)
            if item ~= nil and item.IsEquip then
                equip = item.Equip
            end
        elseif CSkillInfoMgr.skillInfo.identifySkillSource == CSkillInfoMgr.EnumIdentifySkillSource.OtherPlayer then
            CommonDefs.DictIterate(CPlayerInfoMgr.PlayerInfo.pos2equipDict, DelegateFactory.Action_object_object(function (___key, ___value) 
                -- local pair = {}
                -- pair.Key = ___key
                -- pair.Value = ___value
                if ___value.Id == CSkillInfoMgr.skillInfo.identifySkillEquipId then
                    equip = ___value
                    return
                end
            end))
        end
        if equip ~= nil then
            if equip.IsIdentifySkillDisabled then
                self:AddItemToTable(LocalString.GetString("[AC6112](已禁用)[-]"))
            end
            self:AddItemToTable(equip.IdentifySkillExpireTimeInfo)
            local extendInfo = equip.IdentifySkillExtendTimeInfo
            if extendInfo and extendInfo~="" then
                self:AddItemToTable(extendInfo)
            end
        end
    end
end
function CLuaSkillTip:InitColorSystem( skillId, playerLevel, xiuweiGrade, skillProp) 

    if CSkillInfoMgr.skillInfoContext ~= CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference 
        and CSkillInfoMgr.skillInfoContext ~= CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillUpdate 
        and CSkillInfoMgr.skillInfoContext ~= CSkillInfoMgr.EnumSkillInfoContext.XiaLvSkill 
        and CSkillInfoMgr.skillInfoContext ~= CSkillInfoMgr.EnumSkillInfoContext.XiaLvSelectedSkill then
        self.colorSystemRoot:SetActive(false)
        return
    end

    local cls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)
    local idx = - 1
    if CSkillMgr.Inst:IsColorSystemBaseSkill(cls) or CXialvPKMgr.Inst:IsColorSystemBaseSkill(cls) then
        self.colorSystemRoot:SetActive(true)
        idx = 0
    elseif CSkillMgr.Inst:IsColorSystemSkill(cls) then
        self.colorSystemRoot:SetActive(true)
        idx = Skill_ColorSkill.GetData(cls).Color_ID
    elseif CXialvPKMgr.Inst:IsColorSystemSkill(cls) then
        self.colorSystemRoot:SetActive(true)
        idx = XiaLvPK_ColorSkill.GetData(cls).Color_ID
    else
        self.colorSystemRoot:SetActive(false)
    end
    if self.colorSystemRoot.activeSelf then
        do
            local i = 0
            while i < #self.colorBtns do
                self.colorBtns[i+1].Selected = (idx == i)
                i = i + 1
            end
        end
    end
end
function CLuaSkillTip:InitMapi( mapiSkillId) 
    self.curSkillId = mapiSkillId

    Extensions.RemoveAllChildren(self.contentTable.transform)
    local mapiSkill = ZuoQi_MapiSkill.GetData(mapiSkillId)
    if mapiSkill ~= nil then
        --const string namecolor = "FFED5F";
        --const string valuecolor = "E8D0AA";
        self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[%s]%s[-]"), CLuaSkillTip.namecolor, mapiSkill.Name))
        self:AddItemToTable(SafeStringFormat3(LocalString.GetString("[%s]%s[-]"), CLuaSkillTip.valuecolor, mapiSkill.Desc))
    end
    self:InitColorSystem(mapiSkillId, 0, 0, nil)
    self.contentTable:Reposition()
    self:CheckIfShowIdentifyAction()
    self:LayoutWnd()
end
function CLuaSkillTip:CheckAndAddUnlearn( skillId, playerLevel, xiuweiGrade, skillProp) 
    if CLuaSkillInfoWnd.FromAttrGroup then return end

    if skillProp:IsOriginalSkillClsExist(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) then
        return
    end
    do
        local i = 0
        while i < skillProp.TianFuSkillForAI.Length do
            if skillProp.TianFuSkillForAI[i] == skillId then
                return
            end
            i = i + 1
        end
    end
    if CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerMPTZ then
        local cls = CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)
        do
            local i = 0
            while i < skillProp.NewActiveSkillForAI.Length do
                if skillProp.NewActiveSkillForAI[i] == cls then
                    return
                end
                i = i + 1
            end
        end
    end

    -- 临时技能不显示“未领悟”
    if Skill_TempSkill.Exists(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) then
        return
    end
    -- 装备鉴定的技能不显示"未领悟"
    if CSkillMgr.Inst:IsIdentifySkill(CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)) then
        return
    end
    
    if SoulCore_Settings.GetData().HpSkills == math.floor(skillId / 100) and skillId % 100 ~= 0 then
        return
    end

    -- 无量玩法中的技能不显示未领悟
    if CUIManager.IsLoaded(CLuaUIResources.WuLiangCompanionInfoWnd) then
        return
    end

    --天赋技能在mptz技能面板上不处理
    if self.m_IsTianFuSkill and CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerMPTZ then
        return
    end

    -- 冰天试炼不显示
    if CUIManager.IsLoaded(CLuaUIResources.SnowManPVEPlayWnd) then
        return
    end
    if CUIManager.IsLoaded(CLuaUIResources.SnowAdventureSnowManWnd) then
        return
    end
    self:AddItemToTable(SafeStringFormat3("[FF0000]%s[-]", LocalString.GetString("(未领悟)")))
end
function CLuaSkillTip:AddItemToTable( text) 

    local instance = NGUITools.AddChild(self.contentTable.gameObject, self.template)
    instance:SetActive(true)
    local label = instance.transform:GetComponent(typeof(UILabel))
    label.text = SafeStringFormat3("[c]%s[/c]", text)
end
function CLuaSkillTip:LayoutWnd( )

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale
    local virtualScreenHeight = Screen.height * scale

    local minHeightOfBody = 0
    local maxheightOfBody = virtualScreenHeight - self.verticalGap

    local contentHeight = self:TotalHeightOfScrollViewContent()
    local heightOfBody = math.min(math.max(contentHeight + self.body.panel.clipSoftness.y * 2, minHeightOfBody), maxheightOfBody)

    --设置背景高度
    self.background.height = math.floor((heightOfBody + self.verticalGap))

    --设置Body位置
    self.body.panel:ResetAndUpdateAnchors()
    self.body:ResetPosition()
    Extensions.SetLocalPositionX(self.contentTable.transform, self.body.panel.baseClipRegion.x - NGUIMath.CalculateRelativeWidgetBounds(self.contentTable.transform).size.x * 0.5)

    --设置色系技能显示
    if self.colorSystemRoot.activeSelf then
        -- CommonDefs.GetComponent_GameObject_Type(self.colorSystemRoot, typeof(UIWidget)):ResetAndUpdateAnchors()
        self.colorSystemRoot:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    end

    --设置鉴定技能按钮显示
    if self.identifySkillButtonsBg.gameObject.activeSelf then
        local bgPos = self.background.transform.localPosition
        self.identifySkillButtonTable.transform.localPosition = Vector3(bgPos.x+self.background.width * 0.5 + 6, bgPos.y+self.background.height * 0.5 - 12,bgPos.z)
        
        local b = NGUIMath.CalculateRelativeWidgetBounds(self.identifySkillButtonTable.transform.parent, self.identifySkillButtonTable.transform)
        self.identifySkillButtonsBg.height = math.floor((b.size.y + 40 --[[20 * 2]]))
        self.identifySkillButtonsBg.transform.localPosition = b.center
    end
end
function CLuaSkillTip:TotalHeightOfScrollViewContent( )

    local n = self.contentTable.transform.childCount
    local totalHeight = 0
    do
        local i = 0
        while i < n do
            totalHeight = totalHeight + self.contentTable.transform:GetChild(i):GetComponent(typeof(UIWidget)).localSize.y
            i = i + 1
        end
    end
    return totalHeight
end
function CLuaSkillTip:Start( )
    do
        local i = 0
        while i < #self.colorBtns do
            UIEventListener.Get(self.colorBtns[i+1].gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnColorButtonClick(go)
            end)
            i = i + 1
        end
    end
    self.identifySkillButtonTemplate:SetActive(false)
end
function CLuaSkillTip:OnColorButtonClick( go) 
    do
        local i = 0
        while i < #self.colorBtns do
            if self.colorBtns[i+1].gameObject == go then
                local cls = CLuaDesignMgr.Skill_AllSkills_GetClass(self.curSkillId)
                if CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference 
                or CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillUpdate then
                    Gac2Gas.ApplyColorSystemSkill(CSkillMgr.Inst:GetReplaceColorSystemSkillCls(cls, i))
                    break
                end
                if CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.XiaLvSkill then
                    EventManager.BroadcastInternalForLua(EnumEventType.XiaLvPkUpdateColorSkill, {cls, i})
                    break
                end
                if CSkillInfoMgr.skillInfoContext == CSkillInfoMgr.EnumSkillInfoContext.XiaLvSelectedSkill then
                    EventManager.BroadcastInternalForLua(EnumEventType.XiaLvPkUpdateSelectedColorSkill, {cls, i})
                    break
                end
            end
            i = i + 1
        end
    end
end
function CLuaSkillTip:CheckIfShowIdentifyAction( )
    self.identifySkillRoot:SetActive(false)
    --仅在技能设置界面处可用,其他地方不能用
    if CSkillInfoMgr.skillInfoContext ~= CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference then
        return
    end
    local skill = Skill_AllSkills.GetData(self.curSkillId)
    if skill == nil or not CSkillMgr.Inst:IsIdentifySkill(CLuaDesignMgr.Skill_AllSkills_GetClass(skill.ID)) then
        return
    end
    local item = CItemMgr.Inst:GetById(CSkillInfoMgr.skillInfo.identifySkillEquipId)
    if item == nil or not item.IsEquip then
        return
    end
    local equip = item.Equip
    if equip ~= nil then
        self.identifySkillRoot:SetActive(true)
        local equipId = equip.Id
        local identifyAction = { 
            LocalString.GetString("鉴定"), 
            function () 
                CItemMgr.Inst:IdentifySkill(equipId)
                CSkillInfoMgr.CloseSkillInfoWnd()
            end
        }

        local enableSkillAction = {
            LocalString.GetString("开启技能"), 
            function () 
                CItemMgr.Inst:EnableSkill(equipId)
            end
        }


        local disableSkillAction = {
            LocalString.GetString("禁用技能"), 
            function () 
                CItemMgr.Inst:DisableSkill(equipId)
            end
        }

        local extendSkillAction = {
            LocalString.GetString("补益技能"), 
            function () 
                CItemMgr.Inst:ExtendSkill(equipId)
            end
        }

        local actionPairs = {}
        table.insert( actionPairs, identifyAction )
        if skill.ECategory == SkillCategory.Passive then
            if equip.IdentifySkillDisable > 0 then
                table.insert( actionPairs, enableSkillAction )
            else
                table.insert( actionPairs, disableSkillAction )
            end
        end
        if equip.IdentifyEndTime <= CServerTimeMgr.Inst.timeStamp then
            table.insert( actionPairs, extendSkillAction )
        end

        local childCount = self.identifySkillButtonTable.transform.childCount
        do
            local i = 0
            while i < childCount do
                self.identifySkillButtonTable.transform:GetChild(i).gameObject:SetActive(false)
                i = i + 1
            end
        end

        do
            local i = 0
            while i < #actionPairs do
                local instance = nil
                if i < self.identifySkillButtonTable.transform.childCount then
                    instance = self.identifySkillButtonTable.transform:GetChild(i).gameObject
                else
                    instance = CUICommonDef.AddChild(self.identifySkillButtonTable.gameObject, self.identifySkillButtonTemplate)
                end
                instance:SetActive(true)
                instance:GetComponent(typeof(CButton)).Text = actionPairs[i+1][1]
                local action = actionPairs[i+1][2]
                UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function (go) 
                    action()
                end)
                i = i + 1
            end
        end


        self.identifySkillButtonTable:Reposition()
    end
end
function CLuaSkillTip:GetHuaHunSkillColor( )
    if self.colorSystemRoot.activeSelf then
        return self.colorBtns[1+1].gameObject
    end
    return nil
end
