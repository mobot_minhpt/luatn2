-- Auto Generated!!
local CCargoListItemCell = import "L10.UI.CCargoListItemCell"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local GuildFreight_Equipments = import "L10.Game.GuildFreight_Equipments"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local String = import "System.String"
local CItemBagMgr = import "L10.Game.CItemBagMgr"

CCargoListItemCell.m_Init_CS2LuaHook = function (this, index, isCheckOtherPlayer) 
    this.maskGo:SetActive(false)
    local cargoInfo = CFreightMgr.Inst.cargoList[index]
    this.isCheckOtherPlayer = isCheckOtherPlayer
    if isCheckOtherPlayer then
        this.maskGo:SetActive(cargoInfo.status ~= EnumGuildFreightStatus.eNotFilledNeedHelp)
    end
    this.highlight.enabled = false
    this.item = CItemMgr.Inst:GetItemTemplate(cargoInfo.templateId)
    --helpTag.SetActive(false);
    this.amountLabel.text = ""
    this.cargoIndex = index
    this.filled:SetActive(false)

    if this.item == nil then
        this.equip = GuildFreight_Equipments.GetData(cargoInfo.templateId)
        if this.equip == nil then
            --GuildFreight_ExtraItems extraItem = GuildFreight_ExtraItems.GetData(cargoInfo.templateId);
            --if (extraItem != null)
            --{
            --    cargoInfo.templateId = extraItem.ItemID;
            --    item = CItemMgr.Inst.GetItemTemplate(cargoInfo.templateId);
            --    if (item == null)
            --        return;
            --}
            --else return;
            return
        end
    end

    local hasFilled = cargoInfo.status == EnumGuildFreightStatus.eFilled or cargoInfo.status == EnumGuildFreightStatus.eFilledByHelp
    this.filled:SetActive(hasFilled)
    if this.item == nil and this.equip ~= nil then
        this:CalFitEquipCount(index, this.equip)
    end
    if CClientMainPlayer.Inst.Id == CFreightMgr.Inst.playerId then
        if this.item ~= nil then
            local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, cargoInfo.templateId)
            if CFreightMgr.Inst:IsItemSkillBook(cargoInfo.templateId) then
                count = count + CFreightMgr.Inst:GetSkillBookAmountInItemBag(cargoInfo.templateId)
            elseif this.item.Type == EnumItemType_lua.StatusMedichine then
                local bindCount, unbindCount = CItemBagMgr.Inst:GetItemCountByTemplateIdInItemBag(cargoInfo.templateId)
                count = count + bindCount + unbindCount
            end
            this.fillAvailable.enabled = count >= cargoInfo.amount and not hasFilled
        else
            this.fillAvailable.enabled = this:CalFitEquipCount(index, this.equip) >= cargoInfo.amount and not hasFilled
        end
    else
        this.fillAvailable.enabled = false
    end

    if this.item ~= nil then
        this.icon:LoadMaterial(this.item.Icon)
        this.quality.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    elseif this.equip ~= nil then
        this.icon:LoadMaterial(this.equip.Icon)
        this.quality.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), this.equip.Color))
    end

    this:UpdateCargoStatus(cargoInfo)
end
CCargoListItemCell.m_UpdateCargoStatus_CS2LuaHook = function (this, cargoInfo) 
    if this.isCheckOtherPlayer then
        this.maskGo:SetActive(cargoInfo.status ~= EnumGuildFreightStatus.eNotFilledNeedHelp)
    end
    if cargoInfo.status == EnumGuildFreightStatus.eNotFilledNeedHelp then
        this.amountLabel.text = LocalString.GetString("求助中")
        this.amountLabel.color = Color.green
    elseif cargoInfo.status == EnumGuildFreightStatus.eFilled or cargoInfo.status == EnumGuildFreightStatus.eFilledByHelp then
        this.amountLabel.text = LocalString.GetString("已装填")
        this.amountLabel.color = Color.white
    else
        this.amountLabel.color = Color.white
        local bindCount, notbindCount, totalCount
        bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(cargoInfo.templateId)
        totalCount = bindCount + notbindCount
        if CFreightMgr.Inst:IsItemSkillBook(cargoInfo.templateId) then
            totalCount = totalCount + CFreightMgr.Inst:GetSkillBookAmountInItemBag(cargoInfo.templateId)
        elseif this.item.Type == EnumItemType_lua.StatusMedichine then
            local bindCount, unbindCount = CItemBagMgr.Inst:GetItemCountByTemplateIdInItemBag(cargoInfo.templateId)
            totalCount = totalCount + bindCount + unbindCount
        end
        if totalCount >= cargoInfo.amount then
            this.amountLabel.text = System.String.Format("{0}/{1}", totalCount, cargoInfo.amount)
        else
            this.amountLabel.text = System.String.Format("[c][FF0000]{0}[-]/{1}", totalCount, cargoInfo.amount)
        end
    end
end
CCargoListItemCell.m_FillCargo_CS2LuaHook = function (this) 
    this.filled:SetActive(true)
    this.fillAvailable.enabled = false
    if CFreightMgr.Inst.cargoList[this.cargoIndex].status == EnumGuildFreightStatus.eFilled then
        --helpTag.SetActive(false);
        this:UpdateCargoStatus(CFreightMgr.Inst.cargoList[this.cargoIndex])
    elseif CFreightMgr.Inst.cargoList[this.cargoIndex].status == EnumGuildFreightStatus.eFilledByHelp then
        this:UpdateCargoStatus(CFreightMgr.Inst.cargoList[this.cargoIndex])
    end
end
CCargoListItemCell.m_CalFitEquipCount_CS2LuaHook = function (this, index, equipment) 
    local count = 0
    if equipment ~= nil then
        local list = CreateFromClass(MakeGenericClass(List, String))

        local lowerGrade = equipment.MinGrade
        local upperGrade = equipment.MaxGrade
        local qualityType = equipment.Color
        local equipType = equipment.Type
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        do
            local i = 1
            while i <= bagSize do
                local continue
                repeat
                    local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
                    if not System.String.IsNullOrEmpty(id) then
                        local equip = CItemMgr.Inst:GetById(id)
                        if not equip.IsEquip then
                            continue = true
                            break
                        end
                        local template = EquipmentTemplate_Equip.GetData(equip.TemplateId)
                        if template == nil then
                            continue = true
                            break
                        end
                        if equip.Grade <= upperGrade and equip.Grade >= lowerGrade and this:CheckType(CommonDefs.StringSplit_ArrayChar(equipType, ","), template.Type) and (EnumToInt(equip.Equip.Color) == qualityType or equip.Equip.Color == (bit.bor(EnumQualityType.Dark, CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), qualityType)))) then
                            CommonDefs.ListAdd(list, typeof(String), equip.Id)
                            count = count + 1
                        end
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end
        if not CommonDefs.DictContains(CFreightEquipSubmitMgr.submitEquipList, typeof(Int32), index) then
            CommonDefs.DictAdd(CFreightEquipSubmitMgr.submitEquipList, typeof(Int32), index, typeof(MakeGenericClass(List, String)), list)
        else
            CommonDefs.DictSet(CFreightEquipSubmitMgr.submitEquipList, typeof(Int32), index, typeof(MakeGenericClass(List, String)), list)
        end
    end
    return count
end
CCargoListItemCell.m_CheckType_CS2LuaHook = function (this, type, t) 
    do
        local i = 0
        while i < type.Length do
            if t == System.Int32.Parse(type[i]) then
                return true
            end
            i = i + 1
        end
    end
    return false
end
