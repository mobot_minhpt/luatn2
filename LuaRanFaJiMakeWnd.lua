local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local QnButton = import "L10.UI.QnButton"
local CItemMgr = import "L10.Game.CItemMgr"
local UITable = import "UITable"
local Vector2 = import "UnityEngine.Vector2"
local CUIFx = import "L10.UI.CUIFx"
local LuaTweenUtils = import "LuaTweenUtils"
local NGUITools = import "NGUITools"

CLuaRanFaJiMakeWnd = class()
--CLuaRanFaJiMakeWnd.Path = "ui/ranfa/LuaRanFaJiMakeWnd"
CLuaRanFaJiMakeWnd.RecipeIndex = 1
RegistClassMember(CLuaRanFaJiMakeWnd, "m_NeedItemIdTable")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_NeedItemCntTable")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_GotObjTable")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_CntLabelTable")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_MakeQnButton")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_ItemTransTable")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_ItemFxTable")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_FxObj")
RegistClassMember(CLuaRanFaJiMakeWnd, "m_EndFxPosTable")

function CLuaRanFaJiMakeWnd:Init()
    local designData = RanFaJi_RanFaJi.GetData(CLuaRanFaJiMakeWnd.RecipeIndex)
    self.m_NeedItemIdTable = {designData.RongCaiYeItemID, designData.HuaBan1ItemID, designData.HuaBan2ItemID}
    self.m_NeedItemCntTable = {designData.RongCaiYeNum, designData.HuaBan1Num, designData.HuaBan2Num}
    if designData.HuaBan3ItemID > 0 then
        table.insert(self.m_NeedItemIdTable, designData.HuaBan3ItemID)
        table.insert(self.m_NeedItemCntTable, designData.HuaBan3Num)
    end
    self.transform:Find("Offset/RecipeLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s染发配方"), designData.ColorName)
    self.m_MakeQnButton = self.transform:Find("Offset/MakeBtn"):GetComponent(typeof(QnButton))
    UIEventListener.Get(self.m_MakeQnButton.gameObject).onClick = LuaUtils.VoidDelegate(function ()
        self:PlayFx()
    end)

    self.transform:Find("Offset/Tex"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(designData.ColorRGB, 0)

    self.m_GotObjTable = {}
    self.m_CntLabelTable = {}
    self.m_ItemTransTable = {}
    self.m_ItemFxTable = {}
    self.m_Fx = self.transform:Find("Offset/MakeFx"):GetComponent(typeof(CUIFx))
    self.m_EndFxPosTable = {self.m_Fx.transform.localPosition.x, self.m_Fx.transform.localPosition.y, self.m_Fx.transform.localPosition.z}
    local rootObj = self.transform:Find("Offset").gameObject
    for i = 1, #self.m_NeedItemIdTable do
        local itemRootTrans = self.transform:Find("Offset/ItemRoot/Item"..i)
        table.insert(self.m_ItemTransTable, itemRootTrans)
        table.insert(self.m_ItemFxTable, NGUITools.AddChild(rootObj, self.m_Fx.gameObject):GetComponent(typeof(CUIFx)))
        table.insert(self.m_GotObjTable, itemRootTrans:Find("Got").gameObject)
        table.insert(self.m_CntLabelTable, itemRootTrans:Find("CntLabel"):GetComponent(typeof(UILabel)))
        self:InitItem(itemRootTrans, i)
    end
    if #self.m_NeedItemIdTable < 4 then
        self.transform:Find("Offset/ItemRoot/Item4").gameObject:SetActive(false)
        local uiTable = self.transform:Find("Offset/ItemRoot"):GetComponent(typeof(UITable))
        uiTable.padding = Vector2(57, 0)
        uiTable:Reposition()
    end
    self:ResetItemCnt()
end

function CLuaRanFaJiMakeWnd:PlayFx()
    for k, v in pairs(self.m_ItemFxTable) do
        v:LoadFx("fx/ui/prefab/UI_ranseji_dandao.prefab")
        v.transform.position = self.m_ItemTransTable[k].position
        LuaTweenUtils.TweenPosition(v.transform, self.m_EndFxPosTable[1], self.m_EndFxPosTable[2], self.m_EndFxPosTable[3], 0.3)
    end
    self.m_MakeQnButton.Enabled = false
    RegisterTickOnce(function()
            for _, v in pairs(self.m_ItemFxTable) do
                v:DestroyFx()
            end
            self.m_Fx:LoadFx("fx/ui/prefab/UI_ranseji_hecheng.prefab")
            RegisterTickOnce(function()
                self.m_Fx:DestroyFx()
                Gac2Gas.HeChengRanFaJi(CLuaRanFaJiMakeWnd.RecipeIndex)
                self.m_MakeQnButton.Enabled = true
            end, 300)
        end, 300)
end

function CLuaRanFaJiMakeWnd:InitItem(trans, index)
    local itemId = self.m_NeedItemIdTable[index]
    local itemData = Item_Item.GetData(itemId)
	if itemData then
        local texObj = trans:Find("Texture").gameObject
        texObj:GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
        UIEventListener.Get(texObj).onClick = LuaUtils.VoidDelegate(function()
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.ScreenRight, 0, 0, 0, 0)
        end)
        trans:Find("NameLabel"):GetComponent(typeof(UILabel)).text = itemData.Name
        UIEventListener.Get(self.m_GotObjTable[index]).onClick = LuaUtils.VoidDelegate(function ( go)
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, go.transform, AlignType.Right)
        end)
	end
end

function CLuaRanFaJiMakeWnd:ResetItemCnt()
    local bEnough = true
    for k, v in ipairs(self.m_NeedItemIdTable) do
        local cnt = CItemMgr.Inst:GetItemCount(v)
        self.m_CntLabelTable[k].text = cnt.."/"..self.m_NeedItemCntTable[k]
        local bCurEnough = cnt >= self.m_NeedItemCntTable[k]
        bEnough = bEnough and bCurEnough
        self.m_GotObjTable[k]:SetActive(not bCurEnough)
    end
    self.m_MakeQnButton.Enabled = bEnough
end

function CLuaRanFaJiMakeWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "ResetItemCnt")
	g_ScriptEvent:AddListener("SetItemAt", self, "ResetItemCnt")
end

function CLuaRanFaJiMakeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "ResetItemCnt")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "ResetItemCnt")
end
