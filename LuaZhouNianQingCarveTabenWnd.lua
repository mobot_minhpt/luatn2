local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CCraveData = import "L10.Game.CCraveData"
local CCommonItemSelectCellData = import "L10.UI.CCommonItemSelectCellData"

LuaZhouNianQingCarveTabenWnd = class()

RegistClassMember(LuaZhouNianQingCarveTabenWnd, "m_AimRing")
RegistClassMember(LuaZhouNianQingCarveTabenWnd, "m_ConfirmBtn")
RegistClassMember(LuaZhouNianQingCarveTabenWnd, "m_TargetId")
RegistClassMember(LuaZhouNianQingCarveTabenWnd, "m_CarveLabel")

function LuaZhouNianQingCarveTabenWnd:Awake()
    self.m_CarveLabel = self.transform:Find("Anchor/CarveLabel"):GetComponent(typeof(UILabel))
    self.transform:Find("Anchor/Instruction"):GetComponent(typeof(UILabel)).text = ZhouNianQing2023_PSZJSetting.GetData().CarveTabenTitleStr
    --self.m_CarveLabel.text = ""
    UIEventListener.Get(self.transform:Find("Anchor/ConfirmBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOkButtonClick(go)
    end)
end

function LuaZhouNianQingCarveTabenWnd:OnOkButtonClick(go) 
    if self.m_TargetId == nil then
        g_MessageMgr:ShowMessage("Taben_Equip_Material_Not_Exist")
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("SOULMATE_TABEN_CARVE_CONFIRM"),
            DelegateFactory.Action(function() self:DoTaben() end),nil, nil, nil, false)
    end
end
function LuaZhouNianQingCarveTabenWnd:DoTaben()
    local t = CItemMgr.Inst:GetItemInfo(self.m_TargetId)
    Gac2Gas.Anniv2023PSZJ_RequestCraveName(EnumToInt(t.place), t.pos, t.itemId)
    CUIManager.CloseUI(CLuaUIResources.ZhouNianQingCarveTabenWnd)
end

function LuaZhouNianQingCarveTabenWnd:Init()
    self:InitTargetInfo(nil)
end

function LuaZhouNianQingCarveTabenWnd:InitTargetInfo(itemId)
    local transform = FindChild(self.transform,"AimRing")
    local icon = transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("quality"):GetComponent(typeof(UISprite))
    local bind = transform:Find("Bind"):GetComponent(typeof(UISprite))
    local add = transform:Find("Add").gameObject

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OpenEquipSelectWnd()
    end)

    if not itemId then
        icon.material = nil
        add:SetActive(true)
        quality.spriteName = CUICommonDef.GetItemCellBorder()
        bind.spriteName = ""
        --self.m_CarveLabel.text = ""
        return
    end

    local ring = CItemMgr.Inst:GetById(itemId)
    add:SetActive(false)

    icon:LoadMaterial(ring.Icon)
    quality.spriteName = CUICommonDef.GetItemCellBorder(ring.Equip.QualityType)
    bind.spriteName = ring.BindOrEquipCornerMark

    --[[
    local str = ""
    local craveData = CreateFromClass(CCraveData)
	craveData:LoadFromString(ring.Equip.CraveData.Data)
	if craveData.Key == 1 and craveData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
		local list = MsgPackImpl.unpack(craveData.Data.Data)
        if list and list.Count >= 3 then
            str = list[1]..LocalString.GetString("与")..list[2]..LocalString.GetString("的知己手镯")
        end
    end
    self.m_CarveLabel.text = str
    --]]
end

function LuaZhouNianQingCarveTabenWnd:OpenEquipSelectWnd()
    if not CClientMainPlayer.Inst then return end
    local title = LocalString.GetString("选择要刻字的拓本手镯")
    local datas = CreateFromClass(MakeGenericClass(List, CCommonItemSelectCellData))
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize do
        local id = itemProp:GetItemAt(EnumItemPlace.Bag, i)
        if id and id ~= "" then
            local equip = CItemMgr.Inst:GetById(id)
            if equip ~= nil and equip.IsBinded and equip.IsEquip and equip.Equip.CraveData.Data then
                local templateId = equip.TemplateId
                local template = EquipmentTemplate_Equip.GetData(templateId)
                if template.Type == EnumBodyPosition_lua.Bracelet then
                    local craveData = CreateFromClass(CCraveData)
                    craveData:LoadFromString(equip.Equip.CraveData.Data)
                    if craveData.Key == 1 and craveData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
                        local list = MsgPackImpl.unpack(craveData.Data.Data)
                        if list and list.Count < 3 then
                            local data = CreateFromClass(CCommonItemSelectCellData, equip.Id, equip.TemplateId, equip.IsBinded, equip.Amount)
                            CommonDefs.ListAdd(datas, typeof(CCommonItemSelectCellData), data)
                        end
                    end
                end
            end
        end
    end

    local checkBracelet = function(bracelet)
        if bracelet and bracelet.CraveData.Data then
            local craveData = CreateFromClass(CCraveData)
            craveData:LoadFromString(bracelet.CraveData.Data)
            if craveData.Key == 1 and craveData.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
                local list = MsgPackImpl.unpack(craveData.Data.Data)
                if list and list.Count < 3 then
                    local data = CreateFromClass(CCommonItemSelectCellData, bracelet.Id, bracelet.TemplateId, true, 1)
                    CommonDefs.ListAdd(datas, typeof(CCommonItemSelectCellData), data)
                end
            end
        end
    end
    checkBracelet(CItemMgr.Inst:GetEquipByBodyPos(9))
    checkBracelet(CItemMgr.Inst:GetEquipByBodyPos(12))

    if datas.Count == 0 then
        g_MessageMgr:ShowMessage("ANNIV2023PSZJ_TABEN_HAVE_NO_BRACELET")
    else
        local initfunc = function()
            return datas
        end
        local selectfunc = function(itemId, templateId)
            self.m_TargetId = itemId
            local equipment = EquipmentTemplate_Equip.GetData(templateId)
            if not (equipment ~= nil and equipment.Type == EnumBodyPosition_lua.Bracelet) then
                return
            end
            self:InitTargetInfo(itemId)
        end
        LuaCommonItemSelectMgr.ShowSelectWnd(title,nil,initfunc,selectfunc)
    end
end
