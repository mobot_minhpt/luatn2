local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CTeamMgr = import "L10.Game.CTeamMgr"


LuaGuildTerritorialWarsPlayView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsPlayView, "Button", "Button", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsPlayView, "TaskDesc", "TaskDesc", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsPlayView, "ButtonText", "ButtonText", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsPlayView, "MapBtn", "MapBtn", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsPlayView, "m_IsInit")
RegistClassMember(LuaGuildTerritorialWarsPlayView, "m_Tick")
RegistClassMember(LuaGuildTerritorialWarsPlayView,"m_IsLeader")

function LuaGuildTerritorialWarsPlayView:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)

	UIEventListener.Get(self.TaskDesc.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTaskDescClick()
	end)

	UIEventListener.Get(self.MapBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMapBtnClick()
	end)
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsPlayView:OnEnable()
	self.MapBtn.gameObject:SetActive(false)
    self:CancelTick()
	self.m_Tick = RegisterTick(function()
		LuaGuildTerritorialWarsMgr:ShowGuildTerritorialWarsPlayView()
		LuaGuildTerritorialWarsMgr:ShowOccupyFx()
	end,500)
	if LuaGuildTerritorialWarsMgr:IsInPlay() then
		RegisterTickOnce(function()
			CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.GuildTerritorialWarsMap)
		end,2000)
	end
	g_ScriptEvent:AddListener("TeamInfoChange",self,"OnTeamInfoChange")
	self:OnTeamInfoChange()
end

function LuaGuildTerritorialWarsPlayView:OnDisable()
	self:CancelTick()
	g_ScriptEvent:RemoveListener("TeamInfoChange",self,"OnTeamInfoChange")
end

function LuaGuildTerritorialWarsPlayView:OnTeamInfoChange()
	self.m_IsLeader = CClientMainPlayer.Inst and CTeamMgr.Inst:IsTeamLeader(CClientMainPlayer.Inst.Id)
	self.ButtonText.text = self.m_IsLeader and g_MessageMgr:FormatMessage("GuildTerritoryWarRequestTeamMemberTeleportButtonText")
	or g_MessageMgr:FormatMessage("TerritoryWarTeleportToGuildLeaderButtonText") 
end

function LuaGuildTerritorialWarsPlayView:CancelTick()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
--@region UIEvent

function LuaGuildTerritorialWarsPlayView:OnButtonClick()
	if not self.m_IsLeader then
		local msg = g_MessageMgr:FormatMessage("TerritoryWarTeleportToGuildLeader_Confirm")
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			Gac2Gas.TerritoryWarTeleportToGuildLeader()
		end),nil,LocalString.GetString("前往"),LocalString.GetString("取消"),false)
		return 
	end
	Gac2Gas.GuildTerritoryWarRequestTeamMemberTeleport()
end

function LuaGuildTerritorialWarsPlayView:OnTaskDescClick()
	Gac2Gas.RequestTerritoryWarMapOverview()
end

function LuaGuildTerritorialWarsPlayView:OnMapBtnClick()
	Gac2Gas.RequestTerritoryWarMapOverview()
end
--@endregion UIEvent