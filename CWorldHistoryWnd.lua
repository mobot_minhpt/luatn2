-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CWorldHistoryWnd = import "L10.UI.CWorldHistoryWnd"
local DateTime = import "System.DateTime"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Sanjiefengyun_BangHuiJiLu = import "L10.Game.Sanjiefengyun_BangHuiJiLu"
local Sanjiefengyun_GeRenJiLu = import "L10.Game.Sanjiefengyun_GeRenJiLu"
local Sanjiefengyun_LiCheng = import "L10.Game.Sanjiefengyun_LiCheng"
local Sanjiefengyun_QuanFuZhiZui = import "L10.Game.Sanjiefengyun_QuanFuZhiZui"
local System = import "System"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local WorldHistory_History_Data = import "L10.Game.WorldHistory_History_Data"
CWorldHistoryWnd.m_Init_CS2LuaHook = function (this) 
    this.totalInfoNode:SetActive(false)
    UIEventListener.Get(this.backNode).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
    this.template1:SetActive(false)
    this.template2:SetActive(false)
    this.template3_1:SetActive(false)
    this.template3_2:SetActive(false)
    this.template4:SetActive(false)
end
CWorldHistoryWnd.m_InitPanel_CS2LuaHook = function (this) 
    this.totalInfoNode:SetActive(true)
    --_table.transform.RemoveAllChildren();

    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
    this.tabBar:ChangeTab(0, false)
end
CWorldHistoryWnd.m_SetTabColor_CS2LuaHook = function (this, index) 
    do
        local i = 0
        while i < this.tabSprite.Length do
            if index == i then
                this.tabSprite[i].color = CommonDefs.ImplicitConvert_Color_Color32(this.tabColor2)
            else
                this.tabSprite[i].color = CommonDefs.ImplicitConvert_Color_Color32(this.tabColor1)
            end
            i = i + 1
        end
    end
end
CWorldHistoryWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    Extensions.RemoveAllChildren(this._table.transform)
    this.tab1RelateNode:SetActive(false)
    if index == 0 then
        this:InitHistoryInfo()
    elseif index == 1 then
        this:InitServerMostInfo()
    elseif index == 2 then
        this:InitPersonalInfo()
    elseif index == 3 then
        this:InitGuildInfo()
    end
    this:SetTabColor(index)
end
CWorldHistoryWnd.m_GetPlayerName_CS2LuaHook = function (this, roles, playerId) 
    do
        local i = 0
        while i < roles.Length do
            local data = roles[i]
            if data.role_id == playerId then
                return data.role_name
            end
            i = i + 1
        end
    end

    return CWorldHistoryWnd.DefaultPlayerName
end
CWorldHistoryWnd.m_HistoryInfoBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    if this.tabBar.SelectedIndex ~= 0 then
        return
    end

    if ret ~= nil and ret.code == 1 and ret.data.Length > 0 then
        this.emptyNode:SetActive(false)
        local dataList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, WorldHistory_History_Data)), WorldHistory_History_Data, ret.data)
        CommonDefs.ListSort1(dataList, typeof(WorldHistory_History_Data), MakeDelegateFromCSFunction(this.CompareArmor, MakeGenericClass(Comparison, WorldHistory_History_Data), this))

        do
            local i = 0
            while i < dataList.Count do
                local data = dataList[i]
                if data ~= nil then
                    local showTextInfo = Sanjiefengyun_LiCheng.GetData(data.id)
                    if showTextInfo ~= nil and showTextInfo.status == 0 then
                        local node = NGUITools.AddChild(this._table.gameObject, this.template1)
                        node:SetActive(true)
                        local timeInfo = DateTime.ParseExact(data.time, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture)
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = CommonDefs.String_Format_String_ArrayObject("{0}{1}-{2}-{3}", {CWorldHistoryWnd.TimeLabelPreString, timeInfo.Year, timeInfo.Month, timeInfo.Day})
                        local showText = showTextInfo.Message
                        if data.id == 1 then
                            showText = g_MessageMgr:Format(showText, data.server_name)
                        elseif data.id == 2 then
                            showText = g_MessageMgr:Format(showText, data.detail, data.server_name)
                        elseif data.id == 3 then
                            local stringarray = CommonDefs.StringSplit_ArrayChar(data.detail, ";")
                            if stringarray.Length == 2 then
                                local guildname = stringarray[0]
                                local playerId = System.UInt64.Parse(stringarray[1])
                                showText = g_MessageMgr:Format(showText, playerId, this:GetPlayerName(ret.roles, playerId), data.server_name, guildname)
                            end
                        elseif data.id == 4 then
                            local stringarray = CommonDefs.StringSplit_ArrayChar(data.detail, ";")
                            if stringarray.Length == 3 then
                                local playerId1 = System.UInt64.Parse(stringarray[0])
                                local playerId2 = System.UInt64.Parse(stringarray[1])
                                local marryDay = System.Int32.Parse(stringarray[2])
                                local ss = ""
                                if marryDay > 0 then
                                    ss = System.String.Format(LocalString.GetString("，如今已相爱{0}天。"), marryDay)
                                end
                                showText = g_MessageMgr:Format(showText, playerId1, this:GetPlayerName(ret.roles, playerId1), playerId2, this:GetPlayerName(ret.roles, playerId2), data.server_name, ss)
                            end
                        elseif data.id == 5 then
                            local stringarray = CommonDefs.StringSplit_ArrayChar(data.detail, ";")
                            if stringarray.Length == 4 then
                                local leaderId = System.UInt64.Parse(stringarray[0])
                                local playerarray = CommonDefs.StringSplit_ArrayChar(stringarray[1], ",")
                                local rank = System.Int32.Parse(stringarray[2])
                                local jie = System.Int32.Parse(stringarray[3])

                                if playerarray.Length > 0 then
                                    local teamplayername = ""
                                    local rankstring = LocalString.GetString("最终止步小组赛")
                                    if rank == 16 then
                                        rankstring = LocalString.GetString("最终止步16强")
                                    elseif rank == 8 then
                                        rankstring = LocalString.GetString("最终止步8强")
                                    elseif rank == 4 then
                                        rankstring = LocalString.GetString("最终荣获殿军")
                                    elseif rank == 3 then
                                        rankstring = LocalString.GetString("最终荣获季军")
                                    elseif rank == 2 then
                                        rankstring = LocalString.GetString("最终荣获亚军")
                                    elseif rank == 1 then
                                        rankstring = LocalString.GetString("最终荣获冠军")
                                    end

                                    do
                                        local j = 0
                                        while j < playerarray.Length do
                                            local teamplayerId = System.UInt64.Parse(playerarray[j])
                                            if j == playerarray.Length - 1 then
                                                teamplayername = (teamplayername .. this:GetPlayerName(ret.roles, teamplayerId)) .. "#C544014。"
                                            else
                                                teamplayername = (teamplayername .. this:GetPlayerName(ret.roles, teamplayerId)) .. "#C544014、#Cfb670e"
                                            end
                                            j = j + 1
                                        end
                                    end


                                    showText = g_MessageMgr:Format(showText, leaderId, this:GetPlayerName(ret.roles, leaderId), data.server_name, jie, rankstring, teamplayername)
                                end
                            end
                        elseif data.id == 6 or data.id == 7 or data.id == 11 then
                            local playerId = System.UInt64.Parse(data.detail)
                            showText = g_MessageMgr:Format(showText, playerId, this:GetPlayerName(ret.roles, playerId), data.server_name)
                        elseif data.id == 8 then
                            local stringarray = CommonDefs.StringSplit_ArrayChar(data.detail, ";")
                            if stringarray.Length == 2 then
                                local playerId = System.UInt64.Parse(stringarray[0])
                                local bname = stringarray[1]
                                showText = g_MessageMgr:Format(showText, playerId, this:GetPlayerName(ret.roles, playerId), bname)
                            end
                        elseif data.id == 9 then
                            showText = g_MessageMgr:Format(showText, data.detail)
                        elseif data.id == 10 then
                            local stringarray = CommonDefs.StringSplit_ArrayChar(data.detail, ";")
                            if stringarray.Length == 2 then
                                local playerId1 = System.UInt64.Parse(stringarray[0])
                                local playerId2 = System.UInt64.Parse(stringarray[1])
                                showText = g_MessageMgr:Format(showText, playerId1, this:GetPlayerName(ret.roles, playerId1), playerId2, this:GetPlayerName(ret.roles, playerId2), data.server_name)
                            end
                        end

                        CommonDefs.GetComponent_Component_Type(node.transform:Find("text"), typeof(UILabel)).text = showText
                        UIEventListener.Get(node.transform:Find("text").gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
                            local url = CommonDefs.GetComponent_Component_Type(node.transform:Find("text"), typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
                            if url ~= nil then
                                CChatLinkMgr.ProcessLinkClick(url, nil)
                            end
                        end)
                    end
                end
                i = i + 1
            end
        end
        this._table:Reposition()
        this._scrollView:ResetPosition()

        this.tab1RelateNode:SetActive(true)
    else
        this.emptyNode:SetActive(true)
        --this.Close();
        return
    end
end
CWorldHistoryWnd.m_SeverMostInfoBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    if this.tabBar.SelectedIndex ~= 1 then
        return
    end
    if ret ~= nil and ret.code == 1 and ret.data.Length > 0 then
        this.emptyNode:SetActive(false)
        do
            local i = 0
            while i < ret.data.Length do
                local data = ret.data[i]
                if data ~= nil then
                    local showTextInfo = Sanjiefengyun_QuanFuZhiZui.GetData(data.id)
                    if showTextInfo ~= nil and showTextInfo.status == 0 then
                        local node = NGUITools.AddChild(this._table.gameObject, this.template2)
                        node:SetActive(true)
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("info1"), typeof(UILabel)).text = g_MessageMgr:Format(showTextInfo.Name)
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("info2"), typeof(UILabel)).text = data.server_name
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("text"), typeof(UILabel)).text = g_MessageMgr:Format(showTextInfo.Description, data.rate)
                    end
                end
                i = i + 1
            end
        end
        this._table:Reposition()
        this._scrollView:ResetPosition()
    else
        this.emptyNode:SetActive(true)
        --this.Close();
        return
    end
end
CWorldHistoryWnd.m_PersonalInfoBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    if this.tabBar.SelectedIndex ~= 2 then
        return
    end
    if ret ~= nil and ret.code == 1 and ret.data.Length > 0 then
        this.emptyNode:SetActive(false)
        do
            local i = 0
            while i < ret.data.Length do
                local data = ret.data[i]
                if data ~= nil then
                    local showTextInfo = Sanjiefengyun_GeRenJiLu.GetData(data.id)
                    if showTextInfo ~= nil then
                        local pararray = CommonDefs.StringSplit_ArrayChar(showTextInfo.DataName, ";")
                        local node = nil
                        if pararray.Length == 3 or pararray.Length == 2 then
                            node = NGUITools.AddChild(this._table.gameObject, this.template3_1)
                        end
                        --else if (pararray.Length == 5)
                        --{
                        --    //node = NGUITools.AddChild(_table.gameObject, template3_2);
                        --    speDataList.Add(data);
                        --}

                        if node ~= nil and showTextInfo ~= nil and showTextInfo.status == 0 and (pararray.Length == 3 or pararray.Length == 2) then
                            node:SetActive(true)
                            CommonDefs.GetComponent_Component_Type(node.transform:Find("title"), typeof(UILabel)).text = g_MessageMgr:Format(showTextInfo.Name)
                            CommonDefs.GetComponent_Component_Type(node.transform:Find("des"), typeof(UILabel)).text = g_MessageMgr:Format(showTextInfo.Description)
                            if data.extra_role_id > 0 then
                                CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle1"), typeof(UILabel)).text = g_MessageMgr:Format(pararray[0], CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("<link player={0},{1}>与<link player={2},{3}>"), {data.role_id, this:GetPlayerName(ret.roles, data.role_id), data.extra_role_id, this:GetPlayerName(ret.roles, data.extra_role_id)}))
                            else
                                CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle1"), typeof(UILabel)).text = g_MessageMgr:Format(pararray[0], System.String.Format("<link player={0},{1}>", data.role_id, this:GetPlayerName(ret.roles, data.role_id)))
                            end
                            UIEventListener.Get(node.transform:Find("infotitle1").gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
                                local url = CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle1"), typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
                                if url ~= nil then
                                    CChatLinkMgr.ProcessLinkClick(url, nil)
                                end
                            end)
                            --node.transform.FindChild("infotext1").GetComponent<UILabel>().text = MessageMgr.Inst.Format("<link player=%s,%s>", new object[]{ data.role_id, GetPlayerName(ret.roles, data.role_id)});
                            if pararray.Length == 3 then
                                local timeInfo = DateTime.ParseExact(data.detail, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture)
                                CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle2"), typeof(UILabel)).text = g_MessageMgr:Format(pararray[1], CommonDefs.String_Format_String_ArrayObject("{0}{1}-{2}-{3}", {CWorldHistoryWnd.TimeLabelPreString, timeInfo.Year, timeInfo.Month, timeInfo.Day}))

                                --node.transform.FindChild("infotext2").GetComponent<UILabel>().text = string.Format("{0}{1}-{2}-{3}", TimeLabelPreString, timeInfo.Year, timeInfo.Month, timeInfo.Day);

                                CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle3"), typeof(UILabel)).text = g_MessageMgr:Format(pararray[2], data.record)
                                --node.transform.FindChild("infotext3").GetComponent<UILabel>().text = data.record.ToString();
                            else
                                node.transform:Find("infotitle2").gameObject:SetActive(false)
                                node.transform:Find("infotext2").gameObject:SetActive(false)

                                CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle3"), typeof(UILabel)).text = g_MessageMgr:Format(pararray[1], data.record)
                                --node.transform.FindChild("infotext3").GetComponent<UILabel>().text = data.record.ToString();
                            end
                        end
                    end
                end
                i = i + 1
            end
        end
        this._table:Reposition()
        this._scrollView:ResetPosition()
    else
        this.emptyNode:SetActive(true)
        --this.Close();
        return
    end
end
CWorldHistoryWnd.m_GuildInfoBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    if this.tabBar.SelectedIndex ~= 3 then
        return
    end
    if ret ~= nil and ret.code == 1 and ret.data.Length > 0 then
        this.emptyNode:SetActive(false)
        do
            local i = 0
            while i < ret.data.Length do
                local data = ret.data[i]
                if data ~= nil then
                    local showTextInfo = Sanjiefengyun_BangHuiJiLu.GetData(data.id)

                    if showTextInfo ~= nil and showTextInfo.status == 0 then
                        local pararray = CommonDefs.StringSplit_ArrayChar(showTextInfo.DataName, ";")
                        if pararray.Length == 2 then
                            local node = NGUITools.AddChild(this._table.gameObject, this.template4)
                            node:SetActive(true)
                            CommonDefs.GetComponent_Component_Type(node.transform:Find("title"), typeof(UILabel)).text = g_MessageMgr:Format(showTextInfo.Name)
                            CommonDefs.GetComponent_Component_Type(node.transform:Find("des"), typeof(UILabel)).text = g_MessageMgr:Format(showTextInfo.Message)
                            CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle1"), typeof(UILabel)).text = g_MessageMgr:Format(pararray[0], data.guild_name)
                            CommonDefs.GetComponent_Component_Type(node.transform:Find("infotitle2"), typeof(UILabel)).text = g_MessageMgr:Format(pararray[1], data.detail)
                        end
                    end
                end
                i = i + 1
            end
        end
        this._table:Reposition()
        this._scrollView:ResetPosition()
    else
        this.emptyNode:SetActive(true)
        --this.Close();
        return
    end
end
