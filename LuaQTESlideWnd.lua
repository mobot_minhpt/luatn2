local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local UnityEngine = import "UnityEngine"
local CUIFx = import "L10.UI.CUIFx"

LuaQTESlideWnd = class()
RegistClassMember(LuaQTESlideWnd, "DirectionIndicator")
RegistClassMember(LuaQTESlideWnd, "SlideFx")
RegistClassMember(LuaQTESlideWnd, "CountDownBar")
RegistClassMember(LuaQTESlideWnd, "SlideArea")

RegistClassMember(LuaQTESlideWnd, "curTime")
RegistClassMember(LuaQTESlideWnd, "totTime")
RegistClassMember(LuaQTESlideWnd, "slideDirection")

RegistClassMember(LuaQTESlideWnd, "startDrag")
RegistClassMember(LuaQTESlideWnd, "dragXSum")
RegistClassMember(LuaQTESlideWnd, "dragYSum")

LuaQTESlideWnd.DragThreshold = 150.0

function LuaQTESlideWnd:Awake()
    self.DirectionIndicator = self.transform:Find("Button/DirectionIndicator").gameObject
    self.SlideFx = self.transform:Find("Button/DirectionIndicator/Fx"):GetComponent(typeof(CUIFx))
    self.CountDownBar = self.transform:Find("CountDownBar"):GetComponent(typeof(UITexture))
    self.SlideArea = self.transform:Find("SlideArea").gameObject

    self.curTime, self.totTime = 0, 0
    self.startDrag, self.dragXSum, self.dragYSum = false, 0, 0
    self.gameObject:SetActive(false)
end

function LuaQTESlideWnd:Start()
    self.SlideFx:LoadFx(CLuaUIFxPaths.QTESlideFx)
end

function LuaQTESlideWnd:Init()
    if LuaQTEMgr.CurrentQTEType ~= EnumQTEType.Slide then
        return
    end

    CommonDefs.AddOnPressListener(
        self.SlideArea,
        DelegateFactory.Action_GameObject_bool(
            function(go, isPressed)
                self:OnPress(go, isPressed)
            end
        ),
        false
    )

    CommonDefs.AddOnDragListener(
        self.SlideArea,
        DelegateFactory.Action_GameObject_Vector2(
            function(go, delta)
                self:OnDrag(go, delta)
            end
        ),
        false
    )

    self.curTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteTimeOffset)
    self.totTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteDuration)
    self.startDrag = false
    self.dragXSum = 0.0
    self.dragYSum = 0.0
    self.slideDirection = tonumber(LuaQTEMgr.CurrentQTEInfo.slideDirection)

    local rot = self.DirectionIndicator.transform.localEulerAngles
    if self.slideDirection == EnumQTESlideDirection.Up then
        rot.z = 0
    elseif self.slideDirection == EnumQTESlideDirection.Left then
        rot.z = 90
    elseif self.slideDirection == EnumQTESlideDirection.Down then
        rot.z = -180
    elseif self.slideDirection == EnumQTESlideDirection.Right then
        rot.z = -90
    end
    self.DirectionIndicator.transform.localEulerAngles = rot

    self.transform.localPosition =
        LuaQTEMgr.ParsePosition(self.gameObject, LuaQTEMgr.CurrentQTEInfo.xPos, LuaQTEMgr.CurrentQTEInfo.yPos)

    self.gameObject:SetActive(true)
end

function LuaQTESlideWnd:OnPress(go, isPressed)
    self.startDrag = isPressed
    self.dragXSum, self.dragYSum = 0.0, 0.0
end

function LuaQTESlideWnd:OnDrag(go, delta)
    if not self.startDrag then
        return
    end

    self.dragXSum = self.dragXSum + delta.x
    self.dragYSum = self.dragYSum + delta.y

    if math.abs(self.dragXSum) + math.abs(self.dragYSum) >= LuaQTESlideWnd.DragThreshold then
        local res = false
        if self.slideDirection == EnumQTESlideDirection.Up then
            res = self.dragYSum > 0 and math.abs(self.dragYSum) > math.abs(self.dragXSum)
        elseif self.slideDirection == EnumQTESlideDirection.Down then
            res = self.dragYSum < 0 and math.abs(self.dragYSum) > math.abs(self.dragXSum)
        elseif self.slideDirection == EnumQTESlideDirection.Left then
            res = self.dragXSum < 0 and math.abs(self.dragXSum) > math.abs(self.dragYSum)
        elseif self.slideDirection == EnumQTESlideDirection.Right then
            res = self.dragXSum > 0 and math.abs(self.dragXSum) > math.abs(self.dragYSum)
        end

        self.dragXSum, self.dragYSum = 0.0, 0.0
        self.startDrag = false

        if res then
            self:QTESuccess()
        else
            self:QTEFail()
        end
    end
end

function LuaQTESlideWnd:Update()
    local deltaTime = UnityEngine.Time.deltaTime
    self.curTime = self.curTime + deltaTime
    local timeNow = math.min(self.totTime, math.max(self.curTime, 0.0))

    local ratio = 0.0
    if self.totTime ~= 0 then
        ratio = 1 - timeNow / self.totTime
    end
    self.CountDownBar.fillAmount = ratio

    if ratio <=0 then
        self:QTEFail()
    end
end

function LuaQTESlideWnd:QTESuccess()
    LuaQTEMgr.TriggerQTEFinishFx(true, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(true)
    self:Reset()
end

function LuaQTESlideWnd:QTEFail()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(false)
    self:Reset()
end

function LuaQTESlideWnd:OnTimeLimitExceeded()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    self:Reset()
end

function LuaQTESlideWnd:Reset()
    self.curTime, self.totTime = 0, 0
    self.gameObject:SetActive(false)
    g_ScriptEvent:BroadcastInLua("OnQTESubWndClose")
end
