local DelegateFactory        = import "DelegateFactory"
local CPopupMenuInfoMgr      = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData      = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local AlignType              = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local QnCheckBoxGroup        = import "L10.UI.QnCheckBoxGroup"
local QnNumberInput          = import "L10.UI.QnNumberInput"
local CButton                = import "L10.UI.CButton"
local Profession             = import "L10.Game.Profession"
local EnumClass              = import "L10.Game.EnumClass"
local QnSelectableButton     = import "L10.UI.QnSelectableButton"
local CWordFilterMgr         = import "L10.Game.CWordFilterMgr"

LuaTeamRecruitPostWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaTeamRecruitPostWnd, "targetButton")
RegistClassMember(LuaTeamRecruitPostWnd, "targetArrow")
RegistClassMember(LuaTeamRecruitPostWnd, "targetArray")
RegistClassMember(LuaTeamRecruitPostWnd, "targetIndex")
RegistClassMember(LuaTeamRecruitPostWnd, "levelCheckBoxGroup")
RegistClassMember(LuaTeamRecruitPostWnd, "levelInput")
RegistClassMember(LuaTeamRecruitPostWnd, "customLevel")
RegistClassMember(LuaTeamRecruitPostWnd, "levelRangeTbl")
RegistClassMember(LuaTeamRecruitPostWnd, "maxLevel")
RegistClassMember(LuaTeamRecruitPostWnd, "recruitTbl")
RegistClassMember(LuaTeamRecruitPostWnd, "typeSelectTbl")
RegistClassMember(LuaTeamRecruitPostWnd, "classSelectTbl")
RegistClassMember(LuaTeamRecruitPostWnd, "maxEquipScore")
RegistClassMember(LuaTeamRecruitPostWnd, "msgInput")
RegistClassMember(LuaTeamRecruitPostWnd, "durationCheckBoxGroup")
RegistClassMember(LuaTeamRecruitPostWnd, "contactSelectableButton")

function LuaTeamRecruitPostWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local anchor = self.transform:Find("Anchor")
    self.targetButton = anchor:Find("Target/Button"):GetComponent(typeof(CButton))
    UIEventListener.Get(self.targetButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTargetButtonClick(go)
	end)
    self.targetArrow = self.targetButton.transform:Find("Arrow"):GetComponent(typeof(UIBasicSprite))

    local hasMyRecruit = LuaTeamRecruitMgr:HasMyRecruit()
    self:InitTarget(hasMyRecruit)

    self.customLevel = anchor:Find("Level/Custom").gameObject
    self.levelCheckBoxGroup = anchor:Find("Level"):GetComponent(typeof(QnCheckBoxGroup))
    self:InitLevelCheckBox()

    self.levelInput = {}
    self.maxLevel = 160
    for i = 1, 2 do
        self.levelInput[i] = self.customLevel.transform:Find("Input"..i):GetComponent(typeof(QnNumberInput))
        self.levelInput[i].OnMaxValueButtonClick = DelegateFactory.Action(function ()
            self.levelInput[i]:ForceSetText(tostring(self.maxLevel), false)
        end)
        self.levelInput[i].OnValueChanged = DelegateFactory.Action_string(function (text)
            self:OnLevelInputValueChanged(i)
        end)
    end
    self:InitLevel(hasMyRecruit)

    self:InitRecruitTbl()
    self:InitRecruit(hasMyRecruit)

    self.msgInput = anchor:Find("Message/Input"):GetComponent(typeof(UIInput))
    self.msgInput.characterLimit = TeamRecruit_Setting.GetData().MsgLen
    self.durationCheckBoxGroup = anchor:Find("Duration"):GetComponent(typeof(QnCheckBoxGroup))
    self:InitDurationCheckBox()
    self.contactSelectableButton = anchor:Find("Contact/Sprite"):GetComponent(typeof(QnSelectableButton))
    self:InitOther(hasMyRecruit)

    UIEventListener.Get(anchor:Find("PostButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPostButtonClick()
	end)
end

function LuaTeamRecruitPostWnd:InitTarget(hasMyRecruit)
    local myData = LuaTeamRecruitMgr.myData

    if hasMyRecruit then
        self.targetIndex = myData.target
        self.targetButton.Text = TeamRecruit_Type.GetData(myData.target).Name
    else
        self.targetButton.Text = "-"
    end
end

function LuaTeamRecruitPostWnd:InitLevel(hasMyRecruit)
    local myData = LuaTeamRecruitMgr.myData

    if hasMyRecruit then
        local isCustom = true
        for i, data in ipairs(self.levelRangeTbl) do
            if data[1] == myData.gardeStart and data[2] == myData.gardeEnd then
                self.levelCheckBoxGroup:SetSelect(i - 1, true)
                isCustom = false
                break
            end
        end
        if isCustom then
            self.levelCheckBoxGroup:SetSelect(5, true)
            self.levelInput[1]:ForceSetText(myData.gardeStart, false)
            self.levelInput[2]:ForceSetText(myData.gardeEnd, false)
        else
            self.levelInput[1]:ForceSetText(tostring(1), false)
            self.levelInput[2]:ForceSetText(tostring(self.maxLevel), false)
        end
    else
        local level = CClientMainPlayer.Inst.Level
        for i, data in ipairs(self.levelRangeTbl) do
            if level >= data[1] and level <= data[2] then
                self.levelCheckBoxGroup:SetSelect(i - 1, true)
                break
            end
        end
        self.levelInput[1]:ForceSetText(tostring(1), false)
        self.levelInput[2]:ForceSetText(tostring(self.maxLevel), false)
    end
end

function LuaTeamRecruitPostWnd:InitRecruit(hasMyRecruit)
    local myData = LuaTeamRecruitMgr.myData

    if hasMyRecruit then
        local typeList = myData.typeList
        for i, tbl in ipairs(self.recruitTbl) do
            if typeList[i] then
                self.typeSelectTbl[i] = typeList[i][1]
                tbl.typeButton.Text = TeamRecruit_Feature.GetData(typeList[i][1]).Name
                if typeList[i][2] > 0 then
                    self.classSelectTbl[i] = typeList[i][2]
                    tbl.classButton.Text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), typeList[i][2]))
                else
                    tbl.classButton.Text = LocalString.GetString("不限")
                end
                tbl.equipScoreInput:ForceSetText(tostring(typeList[i][3] > 0 and typeList[i][3] or 0), false)
            else
                tbl.typeButton.Text = "-"
                tbl.classButton.Text = "-"
                tbl.equipScoreInput:ForceSetText(tostring(0), false)
            end
        end
    else
        for _, tbl in ipairs(self.recruitTbl) do
            tbl.typeButton.Text = "-"
            tbl.classButton.Text = "-"
            tbl.equipScoreInput:ForceSetText(tostring(0), false)
        end
    end
    self:UpdateRecruitEnable()
end

function LuaTeamRecruitPostWnd:InitOther(hasMyRecruit)
    local myData = LuaTeamRecruitMgr.myData

    self.msgInput.value = hasMyRecruit and myData.msg or ""
    if hasMyRecruit then
        local durations = TeamRecruit_Setting.GetData().Durations
        local id = 1
        for i = 0, durations.Length - 1 do
            if myData.duration / 3600 == durations[i] then
                id = i
                break
            end
        end
        self.durationCheckBoxGroup:SetSelect(id, true)
    else
        self.durationCheckBoxGroup:SetSelect(1, true)
    end
    self.contactSelectableButton:SetSelected(hasMyRecruit and myData.check or false, false)
end

function LuaTeamRecruitPostWnd:InitLevelCheckBox()
    self.levelRangeTbl = {{1, 69}, {70, 89}, {90, 109}, {110, 129}, {130, 160}}
    local tbl = {}
    for _, data in ipairs(self.levelRangeTbl) do
        table.insert(tbl, SafeStringFormat3("%d-%d", data[1], data[2]))
    end
    table.insert(tbl, LocalString.GetString("自定义"))

    self.levelCheckBoxGroup:InitWithOptions(Table2Array(tbl, MakeArrayClass(System.String)), false, false)
    self.levelCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkBox, index)
        self.customLevel:SetActive(index == 5)
	end)
end

function LuaTeamRecruitPostWnd:InitRecruitTbl()
    self.maxEquipScore = 999999
    self.typeSelectTbl = {}
    self.classSelectTbl = {}

    self.recruitTbl = {}
    local parent = self.transform:Find("Anchor/Recruit/Table")
    for i = 1, 4 do
        local child = parent:GetChild(i - 1)

        local tbl = {}
        tbl.indexLabel = child:Find("Label"):GetComponent(typeof(UILabel))
        tbl.typeLabel = child:Find("Type"):GetComponent(typeof(UILabel))
        tbl.typeButton = child:Find("Type/Button"):GetComponent(typeof(CButton))
        tbl.typeButtonArrow = child:Find("Type/Button/Arrow"):GetComponent(typeof(UIBasicSprite))
        UIEventListener.Get(tbl.typeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTypeButtonClick(i)
        end)

        tbl.classLabel = child:Find("Class"):GetComponent(typeof(UILabel))
        tbl.classButton = child:Find("Class/Button"):GetComponent(typeof(CButton))
        tbl.classButtonArrow = child:Find("Class/Button/Arrow"):GetComponent(typeof(UIBasicSprite))
        UIEventListener.Get(tbl.classButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnClassButtonClick(i)
        end)

        tbl.equipScoreLabel = child:Find("EquipScore"):GetComponent(typeof(UILabel))
        tbl.equipScoreInput = child:Find("EquipScore/Input"):GetComponent(typeof(QnNumberInput))
        tbl.equipScoreInput.OnMaxValueButtonClick = DelegateFactory.Action(function ()
            tbl.equipScoreInput:ForceSetText(tostring(self.maxEquipScore), false)
        end)
        tbl.equipScoreInput.OnValueChanged = DelegateFactory.Action_string(function (text)
            self:OnEquipScoreInputValueChanged(i)
        end)

        self.recruitTbl[i] = tbl
    end
end

function LuaTeamRecruitPostWnd:UpdateRecruitEnable()
    for i = 1, 4 do
        local tbl = self.recruitTbl[i]
        local typeEnable = i == 1 or self.typeSelectTbl[i - 1] ~= nil
        tbl.indexLabel.color = NGUIText.ParseColor24(typeEnable and "FFFFFF" or "B3B3B3", 0)
        tbl.typeLabel.color = NGUIText.ParseColor24(typeEnable and "FFFFFF" or "B3B3B3", 0)
        tbl.typeButton.Enabled = typeEnable
        tbl.typeButton.label.color = NGUIText.ParseColor24(typeEnable and "8EDBFF" or "B3B3B3", 0)
        tbl.typeButtonArrow.color = NGUIText.ParseColor24(typeEnable and "8EDBFF" or "B3B3B3", 0)

        local classEnable = self.typeSelectTbl[i] ~= nil
        tbl.classLabel.color = NGUIText.ParseColor24(classEnable and "FFFFFF" or "B3B3B3", 0)
        tbl.equipScoreLabel.color = NGUIText.ParseColor24(classEnable and "FFFFFF" or "B3B3B3", 0)
        tbl.classButton.Enabled = classEnable
        tbl.classButton.label.color = NGUIText.ParseColor24(classEnable and "8EDBFF" or "B3B3B3", 0)
        tbl.classButtonArrow.color = NGUIText.ParseColor24(classEnable and "8EDBFF" or "B3B3B3", 0)
        LuaUtils.SetLocalPositionZ(tbl.equipScoreInput.transform, classEnable and 0 or -1)
        tbl.equipScoreInput.Editable = classEnable
        tbl.equipScoreInput.m_Label.color = NGUIText.ParseColor24(classEnable and "8EDBFF" or "B3B3B3", 0)
    end
end

function LuaTeamRecruitPostWnd:InitDurationCheckBox()
    local tbl = {}
    local durations = TeamRecruit_Setting.GetData().Durations
    for i = 0, durations.Length - 1 do
        table.insert(tbl, SafeStringFormat3(LocalString.GetString("%d小时"), durations[i]))
    end

    self.durationCheckBoxGroup:InitWithOptions(Table2Array(tbl, MakeArrayClass(System.String)), false, false)
end

function LuaTeamRecruitPostWnd:Init()
end

--@region UIEvent

function LuaTeamRecruitPostWnd:OnTargetButtonClick(go)
    if not self.targetArray then
        local tbl = {}
        local count = TeamRecruit_Type.GetDataCount()
        for i = 1, count do
            local name = TeamRecruit_Type.GetData(i).Name
            table.insert(tbl, PopupMenuItemData(name, DelegateFactory.Action_int(function (_)
                self.targetIndex = i
                self.targetButton.Text = name
            end), false, nil, EnumPopupMenuItemStyle.Default))
        end
        self.targetArray = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
    end

    self.targetArrow.flip = UIBasicSprite.Flip.Vertically
    CPopupMenuInfoMgr.ShowPopupMenu(self.targetArray, go.transform, AlignType.Bottom, 1, DelegateFactory.Action(function ( ... )
        self.targetArrow.flip = UIBasicSprite.Flip.Nothing
    end), 300, true, 296)
end

function LuaTeamRecruitPostWnd:OnLevelInputValueChanged(i)
    if System.String.IsNullOrEmpty(self.levelInput[i].Text) then
        self.levelInput[i]:ForceSetText(tostring(0), false)
    else
        local level = System.UInt32.Parse(self.levelInput[i].Text)
        if level > self.maxLevel then
            self.levelInput[i]:ForceSetText(tostring(self.maxLevel), false)
        else
            self.levelInput[i]:ForceSetText(tostring(level), false)
        end
    end
end

function LuaTeamRecruitPostWnd:OnTypeButtonClick(id)
    local typeTable = {}
    local count = TeamRecruit_Feature.GetDataCount()
    for i = 1, count do
        local name = TeamRecruit_Feature.GetData(i).Name
        table.insert(typeTable, PopupMenuItemData(name, DelegateFactory.Action_int(function (_)
            if self.typeSelectTbl[id] and self.typeSelectTbl[id] == i then return end
            self.typeSelectTbl[id] = i
            self.recruitTbl[id].typeButton.Text = name
            self.classSelectTbl[id] = nil
            self.recruitTbl[id].classButton.Text = LocalString.GetString("不限")
            self:UpdateRecruitEnable()
        end), false, nil, EnumPopupMenuItemStyle.Default))
    end
    local typeArray = Table2Array(typeTable, MakeArrayClass(PopupMenuItemData))

    self.recruitTbl[id].typeButtonArrow.flip = UIBasicSprite.Flip.Vertically
    CPopupMenuInfoMgr.ShowPopupMenu(typeArray, self.recruitTbl[id].typeButton.transform, AlignType.Bottom, 1, DelegateFactory.Action(function ( ... )
        self.recruitTbl[id].typeButtonArrow.flip = UIBasicSprite.Flip.Nothing
    end), 300, true, 296)
end

function LuaTeamRecruitPostWnd:OnClassButtonClick(id)
    local classTable = {}
    local class = TeamRecruit_Feature.GetData(self.typeSelectTbl[id]).Class
    for i = 0, class.Length - 1 do
        local name = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class[i]))
        table.insert(classTable, PopupMenuItemData(name, DelegateFactory.Action_int(function (_)
            self.classSelectTbl[id] = class[i]
            self.recruitTbl[id].classButton.Text = name
        end), false, nil, EnumPopupMenuItemStyle.Default))
    end
    table.insert(classTable, PopupMenuItemData(LocalString.GetString("不限"), DelegateFactory.Action_int(function (_)
        self.classSelectTbl[id] = nil
        self.recruitTbl[id].classButton.Text = LocalString.GetString("不限")
    end), false, nil, EnumPopupMenuItemStyle.Default))
    local classArray = Table2Array(classTable, MakeArrayClass(PopupMenuItemData))

    self.recruitTbl[id].classButtonArrow.flip = UIBasicSprite.Flip.Vertically
    CPopupMenuInfoMgr.ShowPopupMenu(classArray, self.recruitTbl[id].classButton.transform, AlignType.Bottom, 1, DelegateFactory.Action(function ( ... )
        self.recruitTbl[id].classButtonArrow.flip = UIBasicSprite.Flip.Nothing
    end), 300, true, 296)
end

function LuaTeamRecruitPostWnd:OnEquipScoreInputValueChanged(id)
    local input = self.recruitTbl[id].equipScoreInput
    if System.String.IsNullOrEmpty(input.Text) then
        input:ForceSetText(tostring(0), false)
    else
        input:ForceSetText(tostring(System.UInt32.Parse(input.Text)), false)
    end
end

function LuaTeamRecruitPostWnd:OnPostButtonClick()
    if not self.targetIndex then
        g_MessageMgr:ShowMessage("TEAM_RECRUIT_NEED_SELECT_TARGET")
        return
    end

    local levelRange
    if self.levelCheckBoxGroup.CurrentIndex == 5 then
        levelRange = {System.UInt32.Parse(self.levelInput[1].Text), System.UInt32.Parse(self.levelInput[2].Text)}

        if levelRange[1] == 0 or levelRange[2] == 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("自定义等级不可设置为0"))
            return
        end
        if levelRange[1] > levelRange[2] then
            g_MessageMgr:ShowMessage("TEAM_RECRUIT_MIN_LEVEL_IS_LARGER")
            return
        end
    else
        levelRange = self.levelRangeTbl[self.levelCheckBoxGroup.CurrentIndex + 1]
    end

    local ret = CWordFilterMgr.Inst:DoFilterOnSend(self.msgInput.value, nil, nil, true)
    local msg = ret.msg

	if msg == nil or ret.shouldBeIgnore then
		return
	end

    if not self.typeSelectTbl[1] then
        g_MessageMgr:ShowMessage("TEAM_RECRUIT_NEED_AT_LEAST_ONE_INTENTION")
        return
    end

    local list = {}
    for i = 1, #self.typeSelectTbl do
        table.insert(list, {self.typeSelectTbl[i], self.classSelectTbl[i] or 0, System.UInt32.Parse(self.recruitTbl[i].equipScoreInput.Text)})
    end

    local durations = TeamRecruit_Setting.GetData().Durations
    local duration = durations[self.durationCheckBoxGroup.CurrentIndex] * 3600

    local bCheck = self.contactSelectableButton:isSeleted()
    Gac2Gas.RequestAddTeamRecruit(self.targetIndex, levelRange[1], levelRange[2], msg, g_MessagePack.pack(list), duration, bCheck)
end

--@endregion UIEvent
