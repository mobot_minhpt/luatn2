local CommonDefs        = import "L10.Game.CommonDefs"
local DelegateFactory   = import "DelegateFactory"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local File              = import "System.IO.File"

LuaAvatarReplaceWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaAvatarReplaceWnd, "oldAvatar")
RegistClassMember(LuaAvatarReplaceWnd, "noOldAvatar")
RegistClassMember(LuaAvatarReplaceWnd, "newAvatar")
RegistClassMember(LuaAvatarReplaceWnd, "cancelButton")
RegistClassMember(LuaAvatarReplaceWnd, "confirmButton")

function LuaAvatarReplaceWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

function LuaAvatarReplaceWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.oldAvatar = anchor:Find("OldAvatar"):GetComponent(typeof(UITexture))
    self.noOldAvatar =  self.oldAvatar.transform:Find("Label").gameObject
    self.newAvatar = anchor:Find("NewAvatar"):GetComponent(typeof(UITexture))
    self.cancelButton = anchor:Find("CancelButton").gameObject
    self.confirmButton = anchor:Find("ConfirmButton").gameObject
end

function LuaAvatarReplaceWnd:InitEventListener()
    UIEventListener.Get(self.cancelButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCancelButtonClick()
    end)

    UIEventListener.Get(self.confirmButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnConfirmButtonClick()
    end)
end

function LuaAvatarReplaceWnd:InitActive()
    self.noOldAvatar:SetActive(false)
end


function LuaAvatarReplaceWnd:Init()
    self:InitOldAvatar()
    self:InitNewAvatar()
end

-- 现在的头像
function LuaAvatarReplaceWnd:InitOldAvatar()
    if not CClientMainPlayer.Inst then return end
    CPersonalSpaceMgr.Inst:GetUserProfile(CClientMainPlayer.Inst.Id, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (data)
        if data.code == 0 then
            local _data = data.data
            if not System.String.IsNullOrEmpty(_data.photo) then
                CPersonalSpaceMgr.Inst:DownLoadPortraitPic(_data.photo, self.oldAvatar, nil)
            else
                self.noOldAvatar:SetActive(true)
            end
        end
    end), nil)
end

-- 截图头像
function LuaAvatarReplaceWnd:InitNewAvatar()
    CUICommonDef.LoadLocalFile2Texture(self.newAvatar, LuaWuMenHuaShiMgr.avatarFilePath, self.newAvatar.width, self.newAvatar.height)
end

--@region UIEvent

function LuaAvatarReplaceWnd:OnCancelButtonClick()
    CUIManager.CloseUI(CLuaUIResources.AvatarReplaceWnd)
end

function LuaAvatarReplaceWnd:OnConfirmButtonClick()
    local data = File.ReadAllBytes(LuaWuMenHuaShiMgr.avatarFilePath)
    local onFinished = DelegateFactory.Action_string(function (url)
        CPersonalSpaceMgr.Inst:SetPhoto(url, nil, nil)
        g_MessageMgr:ShowMessage("MENGDAO_TUPIAN_SHANGCHUANCHENGGONG")
        CUIManager.CloseUI(CLuaUIResources.AvatarReplaceWnd)
    end)

    CPersonalSpaceMgr.Inst:UploadPic("xinshenghuaji", data, onFinished)
end

--@endregion UIEvent

