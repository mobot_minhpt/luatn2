local UIWidget = import "UIWidget"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local UILabel = import "UILabel"
local UIPanel = import "UIPanel"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaNpcInvitedInfoWnd = class()

RegistChildComponent(LuaNpcInvitedInfoWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaNpcInvitedInfoWnd, "NpcName", "NpcName", UILabel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "NpcIcon", "NpcIcon", CUITexture)
RegistChildComponent(LuaNpcInvitedInfoWnd, "NpcLevel", "NpcLevel", UILabel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "NpcLocation", "NpcLocation", UILabel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaNpcInvitedInfoWnd, "ScrollView", "ScrollView", UIPanel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "ProgressSprite", "ProgressSprite", UISprite)
RegistChildComponent(LuaNpcInvitedInfoWnd, "Table", "Table", UITable)--padding
RegistChildComponent(LuaNpcInvitedInfoWnd, "ItemTemplate", "ItemTemplate", BoxCollider)
RegistChildComponent(LuaNpcInvitedInfoWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaNpcInvitedInfoWnd, "EnchantAbilityDescription", "EnchantAbilityDescription", UILabel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "ZongPaiNameLabel", "ZongPaiNameLabel", UILabel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "CompetitionView", "CompetitionView", UIWidget)
RegistChildComponent(LuaNpcInvitedInfoWnd, "MyDaoYiViewInfoLabel", "MyDaoYiViewInfoLabel", UILabel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "ZongPaiView", "ZongPaiView", UIWidget)
RegistChildComponent(LuaNpcInvitedInfoWnd, "EnchantAbilityView", "EnchantAbilityView", UIWidget)
RegistChildComponent(LuaNpcInvitedInfoWnd, "MyDaoYiView", "MyDaoYiView", UIWidget)
RegistChildComponent(LuaNpcInvitedInfoWnd, "QualityLabel", "QualityLabel", UILabel)
RegistChildComponent(LuaNpcInvitedInfoWnd, "RewardButton", "RewardButton", GameObject)
RegistClassMember(LuaNpcInvitedInfoWnd,"m_EnchantAbilityViewInitialHeight")
RegistClassMember(LuaNpcInvitedInfoWnd,"m_CompetitionViewInitialHeight")

function LuaNpcInvitedInfoWnd:OnDisable()
    LuaInviteNpcMgr.InviterList = {}
end

function LuaNpcInvitedInfoWnd:Init()
    self.m_EnchantAbilityViewInitialHeight = self.EnchantAbilityView.height
    self.m_CompetitionViewInitialHeight = self.CompetitionView.height
    self:InitNpcInfoView()
    self.CompetitionView.gameObject:SetActive(#LuaInviteNpcMgr.InviterList > 0)
    self.MyDaoYiViewInfoLabel.gameObject:SetActive(true)
    self.MyDaoYiViewInfoLabel.text = LuaInviteNpcMgr.MyInviterData.daoYiAddonValue ~= 0 and SafeStringFormat3(LocalString.GetString("我宗派灵材总和(%d+%d)"),LuaInviteNpcMgr.MyInviterData.daoyiValue,LuaInviteNpcMgr.MyInviterData.daoYiAddonValue)
        or SafeStringFormat3(LocalString.GetString("我宗派灵材总和(%d)"),LuaInviteNpcMgr.MyInviterData.daoyiValue)
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaInviteNpcMgr.InviterList >= 3 and 3 or #LuaInviteNpcMgr.InviterList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    
    self.TableView:ReloadData(false,false)
    self:InitTipHeight()
    UIEventListener.Get(self.RewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardButtonClick()
	end)
    self.RewardButton.gameObject:SetActive(not LuaInviteNpcMgr.MyInviterData.isRewarded)
end

function LuaNpcInvitedInfoWnd:InitNpcInfoView()
    local info = LuaInviteNpcMgr.NpcInfo
    local npc = NPC_NPC.GetData(info.npcId)
    local inviteNpc = SectInviteNpc_NPC.GetData(info.npcId)
    local enchant = SectInviteNpc_EnchantAbility.GetData(inviteNpc.EnchantID)
    self.ZongPaiNameLabel.text = info.sectName
    self.ZongPaiView.gameObject:SetActive(info.sectName)
    self.NpcName.text = info.name 
    --self.NpcLevel.text = SafeStringFormat3(LocalString.GetString("%d级"),info.grade)
    local scene = PublicMap_PublicMap.GetData(info.sceneTemplateId)
    local pos = inviteNpc.PosInScene
    self.NpcLocation.text = SafeStringFormat3(LocalString.GetString("[%s,%d,%d]"),scene.Name,pos[1],pos[2])
    self.NpcIcon:LoadNPCPortrait(npc.Portrait)    
    self.EnchantAbilityDescription.text = enchant and enchant.EnchantEffectDescribe or ""
    local qualityTextArr = {"",LocalString.GetString("黄"),LocalString.GetString("玄"),LocalString.GetString("地"),LocalString.GetString("天")}
    local qualityColorArr = {"ffffff","FFFE91","519FFF","FF5050","FF88FF"}
    self.QualityLabel.text = CUICommonDef.TranslateToNGUIText(qualityTextArr[info.grade])
    self.QualityLabel.color = NGUIText.ParseColor24( qualityColorArr[info.grade], 0)
end

function LuaNpcInvitedInfoWnd:InitTipHeight()
    local oriHeight = self.Background.height
    local count = (#LuaInviteNpcMgr.InviterList >= 3) and 0 or (3 - #LuaInviteNpcMgr.InviterList)
    local totalItemTopHeight = self.CompetitionView.height - 3 * (self.Table.padding.y + self.ItemTemplate.size.y)
    local reduceDescriptionHeight = self.EnchantAbilityDescription.fontSize * 4 + 32 - self.EnchantAbilityDescription.height
    local reduceZongPaiViewHeight = self.ZongPaiView.height * (self.ZongPaiView.gameObject.activeSelf and 0 or 1)
    local reduceTotalItemHeight = count * (self.Table.padding.y + self.ItemTemplate.size.y) + ((count == 3 or not self.CompetitionView.gameObject.activeSelf) and totalItemTopHeight or 0)
    local reduceMyDaoYiViewInfoHeight = self.MyDaoYiView.height * (self.MyDaoYiViewInfoLabel.gameObject.activeSelf and 0 or 1)
    --print(reduceDescriptionHeight, reduceZongPaiViewHeight , reduceTotalItemHeight , reduceMyDaoYiViewInfoHeight)
    self.Background.height = oriHeight - reduceDescriptionHeight - reduceZongPaiViewHeight - reduceTotalItemHeight - reduceMyDaoYiViewInfoHeight
    self.CompetitionView.height = self.m_CompetitionViewInitialHeight - reduceTotalItemHeight
    self.EnchantAbilityView.height = self.m_EnchantAbilityViewInitialHeight - reduceDescriptionHeight
end

function LuaNpcInvitedInfoWnd:InitItem(item,row)
    local inviter = LuaInviteNpcMgr.InviterList[row+1]
    local infoLabel = item.transform:Find("InfoLabel"):GetComponent(typeof(UILabel))
    local rankTextArr = {LocalString.GetString("一"),LocalString.GetString("二"),LocalString.GetString("三")}
    local isMyRank = CClientMainPlayer.Inst and inviter.sectId == CClientMainPlayer.Inst.BasicProp.SectId
    local daoyiValue = inviter.daoyiValue
    if not isMyRank then
        local lod = 1000
        local vMin = math.floor(daoyiValue / lod)
        local vMax = math.ceil(daoyiValue / lod)
        if vMax == vMin then vMax = vMin + 1 end
        daoyiValue = SafeStringFormat3("%s-%s", vMin * lod, vMax * lod)
    elseif LuaInviteNpcMgr.MyInviterData.daoYiAddonValue ~= 0 then
        daoyiValue = SafeStringFormat3(LocalString.GetString("%d+%d"),LuaInviteNpcMgr.MyInviterData.daoyiValue,LuaInviteNpcMgr.MyInviterData.daoYiAddonValue)
    end
    infoLabel.text = SafeStringFormat3(LocalString.GetString("%s.%s(灵材总和%s)"),row+1,inviter.sectName,daoyiValue)
    infoLabel.color = isMyRank and Color.green or Color.white
    if isMyRank then
        self.MyDaoYiViewInfoLabel.gameObject:SetActive(false)
    end
end
--@region UIEvent
function LuaNpcInvitedInfoWnd:OnRewardButtonClick()
    g_MessageMgr:ShowMessage("NpcInvitedInfoWnd_Reward_Info")
end
--@endregion

