local CButton = import "L10.UI.CButton"
local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local CScene = import "L10.Game.CScene"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaShuJia2023WorldEventWnd = class()

RegistClassMember(LuaShuJia2023WorldEventWnd, "m_DescLabel")
RegistClassMember(LuaShuJia2023WorldEventWnd, "m_Table")
RegistClassMember(LuaShuJia2023WorldEventWnd, "m_ScrollView")
RegistClassMember(LuaShuJia2023WorldEventWnd, "m_Template")
RegistClassMember(LuaShuJia2023WorldEventWnd, "m_RefreshBtn")
RegistClassMember(LuaShuJia2023WorldEventWnd, "m_EnterBtn")

RegistClassMember(LuaShuJia2023WorldEventWnd, "m_SceneList")

function LuaShuJia2023WorldEventWnd:Awake()
    self.m_DescLabel = self.transform:Find("Anchor/DescLabel"):GetComponent(typeof(UILabel))
    self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_Template = self.transform:Find("Anchor/Template").gameObject
    self.m_Template:SetActive(false)
    self.m_RefreshBtn = self.transform:Find("Anchor/RefreshBtn").gameObject
    self.m_EnterBtn = self.transform:Find("Anchor/EnterBtn").gameObject
    self.m_ScrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
    
    self.m_DescLabel.text = ShuJia2023_Setting.GetData("WorldEventTaskDesc").Value
    local rewardItems = ShuJia2023_Setting.GetData("WorldEventRewardItems").Value
    local i = 1
    for itemId in string.gmatch(rewardItems, "([^;]+)") do
        itemId = tonumber(itemId)
        self:InitOneItem(self.transform:Find("Anchor/Rewards/Item3").gameObject, itemId)
        i = i + 1
    end

    UIEventListener.Get(self.m_EnterBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI("ShuJia2023WorldEventWnd")
        LuaCopySceneChosenMgr.OnSceneSelected("")
    end)

    UIEventListener.Get(self.m_RefreshBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaShuJia2023Mgr:OpenWorldEventWnd()
    end)
end

function LuaShuJia2023WorldEventWnd:Init()
    self.m_SceneList = LuaCopySceneChosenMgr.CandidateScenes
    if not self.m_SceneList or #self.m_SceneList == 0 then
        CUIManager.CloseUI("ShuJia2023WorldEventWnd")
        return
    end
    self:LoadItems()
    --CUICommonDef.SetActive(self.m_EnterBtn, LuaCopySceneChosenMgr.ShowRandomButton, true)
end

function LuaShuJia2023WorldEventWnd:LoadItems()
    self.m_Table.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.m_Table.transform)

    for _,scene in pairs(self.m_SceneList) do
        local cur = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
        cur:SetActive(true)
        self:InitSingleItem(
            cur,
            LocalString.GetString("分线") .. tostring(scene.index),
            scene.playerNum,
            scene.maxPlayerNum,
            scene.sceneId
        )

        UIEventListener.Get(cur).onClick =
            DelegateFactory.VoidDelegate(
            function(go)
                self:OnItemClick(go, scene)
            end
        )
    end

    self.m_Table.gameObject:SetActive(true)
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaShuJia2023WorldEventWnd:InitSingleItem(go, branchName, playerNumber, maxPlayerNumber, sceneId)
    local branchNameLabel = go.transform:Find("BranchLabel"):GetComponent(typeof(UILabel))
    local playerNumberLabel = go.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local limitLabel = go.transform:Find("LimitLabel"):GetComponent(typeof(UILabel))
    local teamMemberIcon = go.transform:Find("CurrentFlag").gameObject
    local fullBg = go.transform:Find("FullBg").gameObject
    go.transform:Find("ChosenBg").gameObject:SetActive(false)
    
    branchNameLabel.text = branchName
    playerNumberLabel.text = tostring(playerNumber)
    limitLabel.text = tostring(maxPlayerNumber)
    if playerNumber >= maxPlayerNumber then
        fullBg:SetActive(true)
        playerNumberLabel.color = Color.red
    elseif playerNumber >= tonumber(ShuJia2023_Setting.GetData("WorldEventBranchCongestion").Value) then
        fullBg:SetActive(false)
        playerNumberLabel.color = Color.yellow
    else
        fullBg:SetActive(false)
        playerNumberLabel.color = Color.green
    end

    teamMemberIcon:SetActive(self:CheckTeamMember(sceneId))
end

function LuaShuJia2023WorldEventWnd:OnItemClick(go, scene)
    if CGameVideoMgr.Inst:IsInGameVideoScene() then
        CUIManager.CloseUI("ShuJia2023WorldEventWnd")
        g_MessageMgr:ShowMessage("STATUS_CONFLICT", LocalString.GetString("观战"))
    else
        if scene.sceneId == CScene.MainScene.SceneId then
            g_MessageMgr:ShowMessage("Message_ChuanSong_Current_Scene_Tips")
        elseif scene.playerNum >= scene.maxPlayerNum then
            g_MessageMgr:ShowMessage("Scene_Player_Count_Max")
        else
            CUIManager.CloseUI("ShuJia2023WorldEventWnd")
            go.transform:Find("ChosenBg").gameObject:SetActive(true)
            LuaCopySceneChosenMgr.OnSceneSelected(scene.sceneId)
        end
    end
end

function LuaShuJia2023WorldEventWnd:CheckTeamMember(sceneId)
    if CTeamMgr.Inst:TeamExists() then
        local memberList = CTeamMgr.Inst.Members
        for i = 0, memberList.Count - 1 do
            if memberList[i].m_SceneId == sceneId then
                return true
            end
        end
    end
    return false
end

function LuaShuJia2023WorldEventWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end