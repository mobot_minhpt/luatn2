LuaChunJie2022Mgr = {}

LuaChunJie2022Mgr.m_Tasks = nil

function LuaChunJie2022Mgr.IsHeKa(templateId)
    return templateId==21041299
end

function LuaChunJie2022Mgr.Enter()
    --时间段内
    g_ScriptEvent:RemoveListener("FinishTask", LuaChunJie2022Mgr, "OnFinishTask")
    g_ScriptEvent:AddListener("FinishTask", LuaChunJie2022Mgr, "OnFinishTask")
end

function LuaChunJie2022Mgr.Leave()
    g_ScriptEvent:RemoveListener("FinishTask", LuaChunJie2022Mgr, "OnFinishTask")
end
function LuaChunJie2022Mgr:OnFinishTask(args)
    local taskId = args[0]
    if not LuaChunJie2022Mgr.m_Tasks then
        LuaChunJie2022Mgr.m_Tasks = {}
        local idx = 1
        ChunJie_XunZhaoNianWei2022.Foreach(function(key,data)
            LuaChunJie2022Mgr.m_Tasks[data.Task2] = idx
            idx=idx+1
        end)
    end
    if LuaChunJie2022Mgr.m_Tasks[taskId] then
        LuaXunZhaoNianWeiWnd.s_FinishTaskIdx = LuaChunJie2022Mgr.m_Tasks[taskId]
	    CUIManager.ShowUI(CLuaUIResources.XunZhaoNianWeiWnd)
    end
end
