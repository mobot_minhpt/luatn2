-- Auto Generated!!
local CConversationMgr = import "L10.Game.CConversationMgr"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CKeJuWnd = import "L10.UI.CKeJuWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumKeJuStatus = import "L10.Game.EnumKeJuStatus"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local KeJu_NPCTalk = import "L10.Game.KeJu_NPCTalk"
local KeJu_Setting = import "L10.Game.KeJu_Setting"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CUIManager = import "L10.UI.CUIManager"
CKeJuWnd.m_Init_CS2LuaHook = function (this) 
    this.countDownLabel.text = ""
    this.countDownLabel.transform.localPosition = this.countDownPos1
    this.xiangshiBg:SetActive(false)
    this.huishiBg:SetActive(false)
    this.countDownBg:SetActive(false)
    repeat
        local default = CKeJuMgr.Inst.CurrentStatus
        if default == (EnumKeJuStatus.xiangshi) then
            this.xiangshi.gameObject:SetActive(true)
            this.huishi.gameObject:SetActive(false)
            this.wenshi.gameObject:SetActive(false)
            this.titleLabel.text = LocalString.GetString("科举乡试")
            this.xiangshi:Init(this.questionTemplate)
            this.xiangshiBg:SetActive(true)
            break
        elseif default == (EnumKeJuStatus.huishi) then
            this.xiangshi.gameObject:SetActive(false)
            this.huishi.gameObject:SetActive(true)
            this.wenshi.gameObject:SetActive(false)
            this.titleLabel.text = LocalString.GetString("科举会试")
            this.huishi:Init(this.questionTemplate)
            this.huishiBg:SetActive(true)
            LuaDanMuMgr:InitDanMuMgr()
            break
        elseif default == (EnumKeJuStatus.dianshi) then
            this.titleLabel.text = LocalString.GetString("御前科举")
            this.huishi.gameObject:SetActive(true)
            this.huishi:Init(this.questionTemplate)
            this.wenshi.gameObject:SetActive(false)
            this.huishiBg:SetActive(true)
            LuaDanMuMgr:InitDanMuMgr()
            break
        elseif default == (EnumKeJuStatus.huishiCountDown) then
            this.xiangshi.gameObject:SetActive(false)
            this.huishi.gameObject:SetActive(true)
            this.wenshi.gameObject:SetActive(false)
            this.titleLabel.text = LocalString.GetString("科举会试")
            this.huishi:Init(this.questionTemplate)
            this.countDownLabel.transform.localPosition = this.countDownPos2
            this.huishiBg:SetActive(true)
            this.countDownBg:SetActive(true)
            LuaDanMuMgr:InitDanMuMgr()
            break
        elseif default == (EnumKeJuStatus.wenshi) then
            this.titleLabel.text = LocalString.GetString("文试")
            this.xiangshi.gameObject:SetActive(false)
            this.huishi.gameObject:SetActive(false)
            this.wenshi.gameObject:SetActive(true)
            this.wenshi:Init(this.questionTemplate)
            this.huishiBg:SetActive(false)
            this.wenshiBg:SetActive(true)
            if CKeJuMgr.Inst.wenshiValid then
                this.closeButton3.gameObject:SetActive(false)
            end
            break
        else
            break
        end
    until 1
end
CKeJuWnd.m_OnEnable_CS2LuaHook = function (this) 
    CLuaKeJuWnd:OnEnable(this)
    UIEventListener.Get(this.closeButton1).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton1).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.closeButton2).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton2).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.closeButton3).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton3).onClick, MakeDelegateFromCSFunction(this.OnWenshiCloseBtnClicked, VoidDelegate, this), true)

    EventManager.AddListener(EnumEventType.KeJuHuiShiFinished, MakeDelegateFromCSFunction(this.ShowDialog, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.KeJuDianShiCountDown, MakeDelegateFromCSFunction(this.StartDianshiCountDown, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.OnSceneInit, Action0, this))
    EventManager.AddListener(EnumEventType.RenderSceneDestroy, MakeDelegateFromCSFunction(this.OnSceneDestroy, Action0, this))
    EventManager.AddListener(EnumEventType.KeJuOnSendQuestion, MakeDelegateFromCSFunction(this.OnKeJuSendQuestion, Action0, this))

    if CKeJuMgr.Inst.CurrentStatus == EnumKeJuStatus.huishiCountDown then
        this:StartDianshiCountDown(CKeJuMgr.Inst.huishiBeginCountDown)
    end
end
CKeJuWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton1).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton1).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.closeButton2).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton2).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.closeButton3).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton3).onClick, MakeDelegateFromCSFunction(this.OnWenshiCloseBtnClicked, VoidDelegate, this), false)

    EventManager.RemoveListener(EnumEventType.KeJuHuiShiFinished, MakeDelegateFromCSFunction(this.ShowDialog, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.KeJuDianShiCountDown, MakeDelegateFromCSFunction(this.StartDianshiCountDown, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.OnSceneInit, Action0, this))
    EventManager.RemoveListener(EnumEventType.RenderSceneDestroy, MakeDelegateFromCSFunction(this.OnSceneDestroy, Action0, this))
    EventManager.RemoveListener(EnumEventType.KeJuOnSendQuestion, MakeDelegateFromCSFunction(this.OnKeJuSendQuestion, Action0, this))
    CLuaKeJuWnd:OnDisable(this)
    Gac2Gas.RequestStopWatchKeJu()

    CommonDefs.ListClear(CKeJuMgr.Inst.rankList)
    if this.cancel ~= nil then
        invoke(this.cancel)
    end
end
CKeJuWnd.m_OnCloseBtnClicked_CS2LuaHook = function (this, go) 

    if CKeJuMgr.Inst.CurrentStatus ~= EnumKeJuStatus.xiangshi and CKeJuMgr.Inst.currentQuestionIndex ~= KeJu_Setting.GetData().DianShiQuestionNum then
        g_MessageMgr:ShowMessage("KEJU_TWO_CONFIRMATION")
    end
    this:Close()
end
CKeJuWnd.m_OnWenshiCloseBtnClicked_CS2LuaHook = function (this, go) 
    local message = g_MessageMgr:FormatMessage("CONFIRM_LEAVE_XIALVPK_WATCH")
    MessageWndManager.ShowConfirmMessage(message, 15, false, DelegateFactory.Action(function () 
        Gac2Gas.RequestLeavePlay()
        this:Close()
    end), nil)
end
CKeJuWnd.m_ShowDialog_CS2LuaHook = function (this) 

    this.huishi:Init(this.questionTemplate)
    this.huishi:DisableCountDown()
    --CConversationMgr.Inst.OpenStoryDialogWithAutoPlay(KeJu_Setting.GetData().Before_DianShi_Talk, true, 3000);
    local randomIndex = UnityEngine_Random(1, KeJu_NPCTalk.GetDataCount())
    CConversationMgr.Inst:OpenStoryDialogWithAutoPlay(KeJu_NPCTalk.GetData(randomIndex).Value, true, 3000, nil)
end
CKeJuWnd.m_StartDianshiCountDown_CS2LuaHook = function (this, count) 

    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.cancel = L10.Engine.CTickMgr.Register(MakeDelegateFromCSFunction(this.DianshiCountDown, Action0, this), 1000, ETickType.Loop)
    this.countDown = count
    this:DianshiCountDown()
end
CKeJuWnd.m_DianshiCountDown_CS2LuaHook = function (this) 

    this.countDown = this.countDown - 1
    if this.countDown <= 0 then
        if this.cancel ~= nil then
            invoke(this.cancel)
        end
        this.countDownLabel.text = ""
    else
        this.countDownLabel.text = tostring(this.countDown)
    end
end

--新添加了查看弹幕按钮，这是暂时解决方案
CLuaKeJuWnd = {}
CLuaKeJuWnd.m_ShowDanMuViewBtn = nil
CLuaKeJuWnd.m_Wnd = nil
function CLuaKeJuWnd:OnEnable(this)
    self.m_Wnd = this
    self.m_ShowDanMuViewBtn = this.transform:Find("Anchor/HuishiWindow/ChatInput/ShowDanMuBtn").gameObject
    UIEventListener.Get(self.m_ShowDanMuViewBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        CUIManager.ShowUI(CLuaUIResources.DanMuViewWnd)
    end)
    g_ScriptEvent:AddListener("KeJuQuestionAnswerUpdate", self, "OnKeJuQuestionAnswerUpdate")
end

function CLuaKeJuWnd:OnDisable(this)
    self.m_ShowDanMuViewBtn = nil
    g_ScriptEvent:RemoveListener("KeJuQuestionAnswerUpdate", self, "OnKeJuQuestionAnswerUpdate")
end

function CLuaKeJuWnd:OnKeJuQuestionAnswerUpdate()
    local default = CKeJuMgr.Inst.CurrentStatus
    if self.m_Wnd and default == (EnumKeJuStatus.xiangshi) and CKeJuMgr.Inst.AnswerList.Count == 0 then
        self.m_Wnd.xiangshi.transform:Find("ShareButton").gameObject:SetActive(false)
    end
end
