-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CZuoQiView = import "L10.UI.CZuoQiView"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"
local ZuoQiData = import "L10.Game.ZuoQiData"
CZuoQiView.m_Init_CS2LuaHook = function (this) 
    local zuoqitid = 0
    -- 如果找到了当前选中的，需要那一行改成选中状态
    if CZuoQiMgr.Inst.curSelectedZuoqiId ~= nil and CZuoQiMgr.Inst.curSelectedZuoqiTid ~= 0 then
        zuoqitid = CZuoQiMgr.Inst.curSelectedZuoqiTid
    elseif CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId ~= nil and CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId ~= "" and CZuoQiMgr.Inst.curSelectedZuoqiId == nil and CZuoQiMgr.Inst.curSelectedZuoqiTid == 0 then
        zuoqitid = CClientMainPlayer.Inst.AppearanceProp.ZuoQiTemplateId
    elseif (CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId == nil or CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId == "") and CZuoQiMgr.Inst.curSelectedZuoqiId == nil and CZuoQiMgr.Inst.curSelectedZuoqiTid == 0 then
        local l = CreateFromClass(MakeGenericClass(List, ZuoQiData))
        CommonDefs.ListIterate(CZuoQiMgr.Inst.zuoqis, DelegateFactory.Action_object(function (___value) 
            local zuoqi = ___value
            if zuoqi.id == CClientMainPlayer.Inst.BasicProp.DefaultZuoQiId then
                CommonDefs.ListInsert(l, 0, typeof(ZuoQiData), zuoqi)
            else
                CommonDefs.ListInsert(l, l.Count, typeof(ZuoQiData), zuoqi)
            end
        end))
        if l.Count > 0 then
            zuoqitid = l[0].templateId
        end
    end

    if zuoqitid == 0 then
        this.previewSection:SetActive(true)
        this.attributeSection:SetActive(false)

        CUICommonDef.SetActive(this.previewButton, false, true)
        CUICommonDef.SetActive(this.attributeButton, false, true)
    else
        CUICommonDef.SetActive(this.previewButton, true, true)
        CUICommonDef.SetActive(this.attributeButton, true, true)

        this.previewSection:SetActive(true)
        this.attributeSection:SetActive(false)
        CommonDefs.GetComponent_GameObject_Type(this.previewButton, typeof(CButton)).Selected = true
        CommonDefs.GetComponent_GameObject_Type(this.attributeButton, typeof(CButton)).Selected = false

        this:InitAttributeList(zuoqitid)
    end
end
CZuoQiView.m_InitAttributeList_CS2LuaHook = function (this, zuoqitid) 
    Extensions.RemoveAllChildren(this.attributeTbl.transform)
    this.attributeTemplate:SetActive(false)

    local data = ZuoQi_ZuoQi.GetData(zuoqitid)
    if data == nil then
        return
    end

    local dropRate = data.FallingRate
    local dropStr = System.String.Format(LocalString.GetString("落马率 {0}%"), math.floor(dropRate * 100))
    local dropGo = NGUITools.AddChild(this.attributeTbl.gameObject, this.attributeTemplate)
    dropGo:SetActive(true)
    local dropLabelTrans = dropGo.transform:Find("Label")
    if dropLabelTrans ~= nil then
        local label = CommonDefs.GetComponent_GameObject_Type(dropLabelTrans.gameObject, typeof(UILabel))
        label.text = dropStr
    end

    local desc = data.Description
    --local arrays = Regex.Split(desc, ";")
    local arrays = CommonDefs.StringSplit_ArrayChar(desc, ";")
    CommonDefs.EnumerableIterate(arrays, DelegateFactory.Action_object(function (___value) 
        local s = ___value
        local go = NGUITools.AddChild(this.attributeTbl.gameObject, this.attributeTemplate)
        go:SetActive(true)

        local labelTrans = go.transform:Find("Label")
        if labelTrans ~= nil then
            local label = CommonDefs.GetComponent_GameObject_Type(labelTrans.gameObject, typeof(UILabel))
            label.text = s
        end
    end))

    --坐骑行囊空间
    if CSwitchMgr.IsEnableRiderPackage then
        local str = System.String.Format(LocalString.GetString("坐骑行囊空间  {0}格"), data.CarryBagSize)
        local instance = NGUITools.AddChild(this.attributeTbl.gameObject, this.attributeTemplate)
        instance:SetActive(true)
        local trans = instance.transform:Find("Label")
        if trans ~= nil then
            local label = CommonDefs.GetComponent_GameObject_Type(trans.gameObject, typeof(UILabel))
            label.text = str
        end
    end

    this.attributeTbl:Reposition()
    this.scrollView:ResetPosition()
end
CZuoQiView.m_OnAttributeBtnClicked_CS2LuaHook = function (this, go) 
    this.attributeSection:SetActive(true)
    this.previewSection:SetActive(false)
    CommonDefs.GetComponent_GameObject_Type(this.previewButton, typeof(CButton)).Selected = false
    CommonDefs.GetComponent_GameObject_Type(this.attributeButton, typeof(CButton)).Selected = true
    this.attributeTbl:Reposition()
    this.scrollView:ResetPosition()
end
CZuoQiView.m_OnPreviewBtnClicked_CS2LuaHook = function (this, go) 
    this.attributeSection:SetActive(false)
    this.previewSection:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(this.previewButton, typeof(CButton)).Selected = true
    CommonDefs.GetComponent_GameObject_Type(this.attributeButton, typeof(CButton)).Selected = false
end
CZuoQiView.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    this:Close()
    CZuoQiMgr.Inst.curSelectedZuoqiId = nil
    CZuoQiMgr.Inst.curSelectedZuoqiTid = 0


    --判断是不是在引导
    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(33) and L10.Game.Guide.CGuideMgr.Inst:IsInSubPhase(3) then
        --如果已经上马了，则跳到下一步
        if CClientMainPlayer.Inst ~= nil then
            if not System.String.IsNullOrEmpty(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId) then
                --有坐骑了
                --进入下一阶段
                L10.Game.Guide.CGuideMgr.Inst:NextSubPhase()
            end
        end
    end
end
