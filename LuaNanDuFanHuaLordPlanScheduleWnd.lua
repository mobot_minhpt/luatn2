local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UITable = import "UITable"
local Task_Task = import "L10.Game.Task_Task"

LuaNanDuFanHuaLordPlanScheduleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaLordPlanScheduleWnd, "FameTemplate", "FameTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordPlanScheduleWnd, "FameGrid", "FameGrid", UIGrid)
RegistChildComponent(LuaNanDuFanHuaLordPlanScheduleWnd, "ScheduleTemplate", "ScheduleTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordPlanScheduleWnd, "SureButton", "SureButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaLordPlanScheduleWnd, "Table", "Table", GameObject)

--@endregion RegistChildComponent end
LuaNanDuFanHuaLordPlanScheduleWnd.PeriodId = nil
function LuaNanDuFanHuaLordPlanScheduleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitData()
    self:InitFameGrid()
    self:InitUIEvent()
end

function LuaNanDuFanHuaLordPlanScheduleWnd:Init()

end

function LuaNanDuFanHuaLordPlanScheduleWnd:InitData()
    self.lordData = {}
    self.fashionData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.lordData = data[1]
            self.fashionData = data[5]
        end
    end
    local fashion1Id = self.fashionData[1] and self.fashionData[1] or 0
    self.fashionAddValue = {}
    if fashion1Id ~= 0 then
        self:ParseFashionAttrChange(self.fashionAddValue, NanDuFanHua_LordFashion.GetData(fashion1Id).AttrChange)
    end

    local fashion2Id = self.fashionData[2] and self.fashionData[2] or 0
    if fashion2Id ~= 0 then
        self:ParseFashionAttrChange(self.fashionAddValue, NanDuFanHua_LordFashion.GetData(fashion2Id).AttrChange)
    end

    local zuoqiId = self.fashionData[3] and self.fashionData[3] or 0
    if zuoqiId ~= 0 then
        self:ParseFashionAttrChange(self.fashionAddValue, NanDuFanHua_LordZuoQi.GetData(zuoqiId).AttrChange)
    end
    
    self.openTabIndex = 0
    
    self.taskData = {}
    NanDuFanHua_LordTask.Foreach(function(k, v)
        if self.taskData[v.Fame] == nil then
            self.taskData[v.Fame] = {}
        end
        
        --check一堆条件之后再insert
        if self:CheckHonour(v.HonourRequire) and self:CheckMoney(v.MoneyRequire) and self:CheckFame(v.Fame, v.FameRequire) then
            table.insert(self.taskData[v.Fame], NanDuFanHua_LordTask.GetData(v.ID))
        end
    end)
end

function LuaNanDuFanHuaLordPlanScheduleWnd:ParseFashionAttrChange(tbl, parseText)
    local attrList = g_LuaUtil:StrSplit(parseText, ";")
    for i = 1, #attrList-1 do
        local subKeyValuePair = g_LuaUtil:StrSplit(attrList[i], ",")
        tbl[tonumber(subKeyValuePair[1])] = (tbl[tonumber(subKeyValuePair[1])] and tbl[tonumber(subKeyValuePair[1])] or 0) + tonumber(subKeyValuePair[2])
    end
end

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckHonour(info)
    if info == "" then
        return true
    end

    local sign, value = string.match(info, "([<>])(%d+)")
    sign = (sign == ">")
    value = tonumber(value)
    local playerValue = self.lordData[LuaYiRenSiMgr.LordPropertyKey.YiZhangIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.YiZhangIndex] or 0
    playerValue = playerValue + (self.fashionAddValue[6] and self.fashionAddValue[6] or 0)
    if sign then
        return playerValue > value
    else
        return playerValue < value
    end
end

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckMoney(info)
    if info == "" then
        return true
    end

    local sign, value = string.match(info, "([<>])(%d+)")
    sign = (sign == ">")
    value = tonumber(value)

    if value > 0 then
        local playerValue = self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
        if sign then
            return playerValue > value
        else
            return playerValue < value
        end
    elseif value == 0 then
        local playerValue = self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
        local playerDebutValue = self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] or 0
        if sign then
            return playerValue > 0
        else
            return -playerDebutValue < 0 and playerValue > 0
        end
    else    
        local playerValue = self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] or 0
        if sign then
            return -playerValue > value
        else
            return -playerValue < value
        end
    end
end

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckFame(fameId, info)
    if info == "" then
        return true
    end

    local sign, value = string.match(info, "([<>])(%d+)")
    sign = (sign == ">")
    value = tonumber(value)
    local playerValue = self.lordData[fameId+1] and self.lordData[fameId+1] or 0

    if sign then
        return playerValue > value
    else
        return playerValue < value
    end
end

function LuaNanDuFanHuaLordPlanScheduleWnd:InitUIEvent()
    UIEventListener.Get(self.SureButton).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.fameScheduleId and self.fameScheduleId > 0 then
            Gac2Gas.NanDuLordAcceptSchedule(LuaNanDuFanHuaLordPlanScheduleWnd.PeriodId, self.fameScheduleId)
            CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaLordPlanScheduleWnd)
        end
    end)
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaLordPlanScheduleWnd:InitFameGrid()
    local grid = self.FameGrid
    local template = self.FameTemplate
    
    self.fameObjList = {}
    
    for fameId = 1, #self.taskData do
        local fameObj = NGUITools.AddChild(grid.gameObject, template)
        fameObj:SetActive(true)
        table.insert(self.fameObjList, fameObj)
        fameObj.transform:Find("NormalLabel"):GetComponent(typeof(UILabel)).text = LuaYiRenSiMgr.LordFameName[fameId]
        UIEventListener.Get(fameObj).onClick = DelegateFactory.VoidDelegate(function (p)
            self:OnClickFame(fameId)
        end)
    end
    grid:Reposition()
    
    self:OnClickFame(1)
end

function LuaNanDuFanHuaLordPlanScheduleWnd:OnClickFame(fameId)
    if self.openTabIndex == fameId then
        return
    end
    for i = 1, #self.fameObjList do
        self.fameObjList[i].transform:Find("Selected").gameObject:SetActive(i == fameId)
    end
    self.openTabIndex = fameId
    self:RefreshScheduleWidget(fameId)
end


function LuaNanDuFanHuaLordPlanScheduleWnd:RefreshScheduleWidget(fameId)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.scheduleObjList = {}
    self.indexInList = 0
    self.fameScheduleId = 0
    local fameTaskData = self.taskData[fameId]
    local grid = self.Table
    local template = self.ScheduleTemplate
    for i = 1, #fameTaskData do
        local scheduleObj = NGUITools.AddChild(grid.gameObject, template)
        scheduleObj:SetActive(true)
        local infoObj = scheduleObj.transform:Find("LordScheduleInfo")
        table.insert(self.scheduleObjList, scheduleObj)
        infoObj.transform:Find("InfoTitle"):GetComponent(typeof(UILabel)).text = fameTaskData[i].Schedule
        infoObj.transform:Find("InfoTitle/Introduction"):GetComponent(typeof(UILabel)).text = fameTaskData[i].Introduction
        scheduleObj.transform:Find("MainTask").gameObject:SetActive(self:CheckMainTaskTag(fameTaskData[i]))

        UIEventListener.Get(scheduleObj).onClick = DelegateFactory.VoidDelegate(function (p)
            self:OnClickSchedule(i, fameTaskData[i].ID)
        end)
    end
    grid.transform:GetComponent(typeof(UITable)):Reposition()
end

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckMainTaskTag(fameScheduleCfg)
    local specialEvent = fameScheduleCfg.SpecialEvent
    if specialEvent and specialEvent ~= "" then
        local startWith = "AcceptRandomTask"
        if specialEvent:sub(1, #startWith) == startWith then
            local taskIdList = {}
            for num in specialEvent:gmatch("(%d+),?") do
                table.insert(taskIdList, tonumber(num))
            end
            for i = 1, #taskIdList do
                local taskId = taskIdList[i]
                --MainTask为1、TaskCheck没有或已完成、ExtraLimit符合条件的、玩家未完成且不在进行中的任务
                local taskCfg = Task_Task.GetData(taskId)
                if self:CheckMainTask(taskCfg) and self:CheckTaskCheck(taskCfg) and self:CheckExtraLimit(taskCfg) and self:CheckPlayerTask(taskCfg) then
                    return true
                end
            end
        end
    end
    return false
end 

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckMainTask(taskCfg)
    return taskCfg.MainTask == 1
end

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckTaskCheck(taskCfg)
    return taskCfg.TaskCheck == 0 or (CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskCfg.TaskCheck))
end

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckExtraLimit(taskCfg)
    --临时的解析方法
    local extraLimitString = taskCfg.ExtraLimit

    if extraLimitString and extraLimitString ~= "" then
        local ret = true
        local subList = g_LuaUtil:StrSplit(extraLimitString, ";")
        local startWithLordMoney = "Accept:NanDuLordMoney,"
        local startWithTaskFinished = "Accept:taskfinished,"
        for i = 1, #subList do
            local subString = subList[i]
            if subString:sub(1, #startWithLordMoney) == startWithLordMoney then
                local isSubS, lowerS, upperS = subString:match(startWithLordMoney .. "(%d+),(%d+),(%d+)")
                local isSub = tonumber(isSubS)
                local lower = tonumber(lowerS)
                local upper = tonumber(upperS)
                local curr = 0
                if isSub == 1 then
                    --submoney
                    curr = self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.subMoneyIndex] or 0
                else
                    --money
                    curr = self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
                end
                ret = ret and (lower <= curr and curr <= upper)
            elseif subString:sub(1, #startWithTaskFinished) == startWithTaskFinished then
                local taskIdS = subString:match(startWithTaskFinished .. "(%d+)")
                local taskId = tonumber(taskIdS)
                if CClientMainPlayer.Inst then
                    local isFinishedTask = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskId)
                    ret = ret and isFinishedTask
                end
            end
        end
        
        return ret
    end
    return true
end

function LuaNanDuFanHuaLordPlanScheduleWnd:CheckPlayerTask(taskCfg)
    local taskId = taskCfg.ID
    if CClientMainPlayer.Inst then
        local isFinishedTask = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskId)
        local isCurrentTask = CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), taskId)
        return not (isFinishedTask or isCurrentTask)
    end
    return false
end

function LuaNanDuFanHuaLordPlanScheduleWnd:OnClickSchedule(indexInList, fameScheduleId)
    if self.indexInList == indexInList and self.fameScheduleId == fameScheduleId then
        return
    end
    for i = 1, #self.scheduleObjList do
        self.scheduleObjList[i].transform:Find("Highlight").gameObject:SetActive(i == indexInList)
    end
    self.indexInList = indexInList
    self.fameScheduleId = fameScheduleId
end

function LuaNanDuFanHuaLordPlanScheduleWnd:GetGuideGo(methodName)
    if methodName == "GetShiLiQingHuaiButton" then
        return self.FameGrid.transform:GetChild(2).gameObject
    elseif methodName == "GetPaoZaoScheduleButton" then
        return self.scheduleObjList[2]
    end
end