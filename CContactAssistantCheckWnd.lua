-- Auto Generated!!
local CContactAssistantCheckWnd = import "L10.UI.CContactAssistantCheckWnd"
local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local String = import "System.String"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
CContactAssistantCheckWnd.m_CheckPercent_CS2LuaHook = function (this, str) 
    local percent = 0
    local stateStringArray = CommonDefs.StringSplit_ArrayChar(CContactAssistantCheckWnd.StateString, "#")
    local totalLength = stateStringArray.Length - 1
    if totalLength == 0 then
        return 1
    end
    do
        local i = 0
        while i < stateStringArray.Length do
            if (stateStringArray[i] == str) then
                percent = i / totalLength
                return percent
            end
            i = i + 1
        end
    end

    return - 1
end
CContactAssistantCheckWnd.m_InitState_CS2LuaHook = function (this, message) 
    this.totalNode:SetActive(true)

    local dataArray = CommonDefs.StringSplit_ArrayChar(message, "\n")

    if dataArray == nil or dataArray.Length <= 0 then
        L10.CLogMgr.Log("SyncGetAnswerResult Message:" .. message)
        this:Close()
        return
    end

    local result = dataArray[0]
    if (result == "200") then
        local resultData = dataArray[2]
        --Dictionary<string, object> jsonDict = L10.Game.Utils.Json.Deserialize(resultData) as Dictionary<string, object>;
        --Object[] jsonList = L10.Game.Utils.Json.Deserialize(resultData) as Object[];
        local jsonList = TypeAs(L10.Game.Utils.Json.Deserialize(resultData), typeof(MakeGenericClass(List, Object)))

        if jsonList ~= nil and jsonList.Count > 0 then
            local maxPrecent = 0
            local maxNode = nil

            do
                local i = 0
                while i < jsonList.Count do
                    local jsonDict = TypeAs(jsonList[i], typeof(MakeGenericClass(Dictionary, String, Object)))
                    if CommonDefs.DictContains(jsonDict, typeof(String), "content") then
                        local content = ToStringWrap(CommonDefs.DictGetValue(jsonDict, typeof(String), "content"))
                        if StringStartWith(content, CContactAssistantCheckWnd.PreStrig) then
                            local stage = CommonDefs.StringTrimStart(content, {CommonDefs.StringToCharArray(CContactAssistantCheckWnd.PreStrig)})
                            local percent = this:CheckPercent(stage)
                            if percent >= 0 then
                                local node = NGUITools.AddChild(this.fatherNode, this.templateNode)
                                node:SetActive(true)
                                node.transform.localPosition = Vector3(this.maxLength * percent, 0, 0)
                                CommonDefs.GetComponent_Component_Type(node.transform:Find("text"), typeof(UILabel)).text = stage

                                if maxPrecent < percent then
                                    maxPrecent = percent
                                    maxNode = node
                                end
                            end
                        end
                    end
                    i = i + 1
                end
            end

            this.progressBar.value = maxPrecent
            if maxNode ~= nil then
                maxNode.transform:Find("normal").gameObject:SetActive(false)
                maxNode.transform:Find("last").gameObject:SetActive(true)
            end
        else
            this:Close()
            return
        end
    else
        --fail
        L10.CLogMgr.Log("SyncGetAnswerResult Message:" .. message)
        this:Close()
        return
    end
end
CContactAssistantCheckWnd.m_Init_CS2LuaHook = function (this) 
    this.totalNode:SetActive(false)
    this.templateNode:SetActive(false)
    Extensions.RemoveAllChildren(this.fatherNode.transform)
    this.contentText.text = CContactAssistantMgr.Inst.checkStateInfo
    Gac2Gas.GetAnswerByQuestionId(tostring(CContactAssistantMgr.Inst.checkStateQid))
end
