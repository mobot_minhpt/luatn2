local CUITexture = import "L10.UI.CUITexture"

local GameObject = import "UnityEngine.GameObject"

local UITable = import "UITable"

local UIScrollView = import "UIScrollView"

local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QnButton = import "L10.UI.QnButton"
local QnButtonState = import "L10.UI.QnButton+QnButtonState"
local UITexture = import "UITexture"
local UIGrid = import "UIGrid"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"

LuaHengYuDangKouMonsterInfoWnd = class()
RegistClassMember(LuaHengYuDangKouMonsterInfoWnd, "IsInGamePlay") -- 是否在副本里
RegistClassMember(LuaHengYuDangKouMonsterInfoWnd, "Type") -- 副本类型
RegistClassMember(LuaHengYuDangKouMonsterInfoWnd, "EnumMonsterStatus") -- 怪物状态类型
RegistClassMember(LuaHengYuDangKouMonsterInfoWnd, "DataCount") -- 总数据量
RegistClassMember(LuaHengYuDangKouMonsterInfoWnd, "DataList") -- 总数据表
RegistClassMember(LuaHengYuDangKouMonsterInfoWnd, "DotList") -- 页码点对象数组
RegistClassMember(LuaHengYuDangKouMonsterInfoWnd, "LabelList") -- 段落数组

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "MonsterName", "MonsterName", UILabel)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "MonsterImage", "MonsterImage", CUITexture)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "MonsterStatus", "MonsterStatus", UILabel)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "MonsterInfo", "MonsterInfo", UIScrollView)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "PageButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "TextTable", "TextTable", UITable)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "Label", "Label", GameObject)
RegistChildComponent(LuaHengYuDangKouMonsterInfoWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouMonsterInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

-- 初始化Grid和Table
function LuaHengYuDangKouMonsterInfoWnd:InitGridAndTable()
    self.DotList = {}
    for i = 0, 5 do
        table.insert(self.DotList, self.Grid.transform:GetChild(i):GetComponent(typeof(UITexture)))
    end
    self.DotList[6].gameObject:SetActive(self.Type == EnumHengYuDangKouStage.eHard)
    self.Grid:Reposition()

    self.LabelList = {}
end
-- 获取怪物数据
function LuaHengYuDangKouMonsterInfoWnd:GetData()
    self.DataCount = (self.Type == EnumHengYuDangKouStage.eNormal and 5 or 6)
    self.DataList = {}
    local MonsterTable = LuaHengYuDangKouMgr:InitMonsterInfo()
    for i = 1, self.DataCount do
        local info = MonsterTable[self.Type][i]
        table.insert(self.DataList, {
            MonsterName = LocalString.GetString(info.MonsterName),
            MonsterIntro = info.MonsterIntro,
            MonsterIcon = info.MonsterLIcon,
            MonsterStatus = self.EnumMonsterStatus.NotAppear,
        })
    end
end
-- 设置怪物状态
function LuaHengYuDangKouMonsterInfoWnd:SetMonsterStatus(status)
    if not self.IsInGamePlay then return end -- 不在副本内不需要执行
    if status == self.EnumMonsterStatus.NotAppear then
        self.MonsterStatus.text = LocalString.GetString("尚未出现")
        self.MonsterStatus.color = NGUIText.ParseColor24("97b6d1", 0)
    elseif status == self.EnumMonsterStatus.Battle then
        self.MonsterStatus.text = LocalString.GetString("挑战中")
        self.MonsterStatus.color = NGUIText.ParseColor24("84ff7e", 0)
    elseif status == self.EnumMonsterStatus.Defeated then
        self.MonsterStatus.text = LocalString.GetString("已击败")
        self.MonsterStatus.color = NGUIText.ParseColor24("97b6d1", 0)
    end
end
-- 刷新怪物状态
function LuaHengYuDangKouMonsterInfoWnd:RefreshMonsterInfo()
    if not self.IsInGamePlay then return end -- 不在副本内不需要执行
    local now = LuaHengYuDangKouMgr.m_SceneInfo.progress
    for i = 1, self.DataCount do
        if i < now then
            self.DataList[i].MonsterStatus = self.EnumMonsterStatus.Defeated
        elseif i == now then
            self.DataList[i].MonsterStatus = self.EnumMonsterStatus.Battle
        else
            self.DataList[i].MonsterStatus = self.EnumMonsterStatus.NotAppear
        end
    end
end
-- 刷新怪物文本表
function LuaHengYuDangKouMonsterInfoWnd:RefreshTextTable(index)
    self.Template:SetActive(false)
    Extensions.RemoveAllChildren(self.TextTable.transform)
    local textcontent = self.DataList[index].MonsterIntro
    textcontent = g_MessageMgr:FormatMessage("CUSTOM_STRING", textcontent)
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(textcontent)
    if info then
        for i = 0, info.paragraphs.Count - 1 do
            local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.Template)
            paragraphGo:SetActive(true)
            paragraphGo.transform:Find("Label"):GetComponent(typeof(UILabel)).text = info.paragraphs[i]
        end
    end
    self.TextTable:Reposition()
    self.MonsterInfo:ResetPosition()
end
-- 显示第page个怪物
function LuaHengYuDangKouMonsterInfoWnd:ShowPage(page)
    if page < 1 then page = 1 end
    if page > self.DataCount then page = self.DataCount end
    self.MonsterName.text = self.DataList[page].MonsterName
    self.MonsterImage:LoadMaterial(self.DataList[page].MonsterIcon)
    self:SetMonsterStatus(self.DataList[page].MonsterStatus)
    self:RefreshTextTable(page)

    for i = 1, self.DataCount do
        self.DotList[i].color = NGUIText.ParseColor24(i == page and "7FA5DC" or "153159", 0)
    end
    self.PageButton:SetValue(page, false)
    self.PageButton.IncreaseButton.gameObject:SetActive(page < self.DataCount)
    self.PageButton.DecreaseButton.gameObject:SetActive(page > 1)
end
function LuaHengYuDangKouMonsterInfoWnd:Init()
    self.EnumMonsterStatus = {
        NotAppear = 0, -- 尚未出现
        Battle = 1,    -- 挑战中
        Defeated = 2,  -- 已击败
    }
    self.IsInGamePlay = LuaHengYuDangKouMgr:IsInPlay()
    if self.IsInGamePlay then
        self.Type = LuaHengYuDangKouMgr.m_SceneInfo.stage
    else
        self.Type = EnumHengYuDangKouStage.eHard -- 如果在副本外打开窗口则显示精英信息
    end
    self.MonsterStatus.gameObject:SetActive(self.IsInGamePlay) -- 如果在副本外打开则不显示状态栏
    self:InitGridAndTable()
    self:GetData()
    self.PageButton:SetMinMax(1, self.DataCount, 1)
    self:RefreshMonsterInfo()

    if self.IsInGamePlay then
        self:ShowPage(LuaHengYuDangKouMgr.m_ClickId)
    else
        self:ShowPage(1)
    end
    -- 切换页面事件
    self.PageButton.onValueChanged = DelegateFactory.Action_uint(function(page)
        self:ShowPage(page)
    end)
end

--@region UIEvent

--@endregion UIEvent

