require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaLiangHaoSettingWnd = class()

RegistClassMember(LuaLiangHaoSettingWnd, "m_LiangHaoLabel")
RegistClassMember(LuaLiangHaoSettingWnd, "m_ExpireTimeLabel")

RegistClassMember(LuaLiangHaoSettingWnd, "m_ShowIconCheckBox")
RegistClassMember(LuaLiangHaoSettingWnd, "m_ShowDigitIconCheckBox")
RegistClassMember(LuaLiangHaoSettingWnd, "m_RefuseAddFriendRequestCheckBox")
RegistClassMember(LuaLiangHaoSettingWnd, "m_RefuseStrangerMsgCheckBox")

RegistClassMember(LuaLiangHaoSettingWnd, "m_InfoButton")
RegistClassMember(LuaLiangHaoSettingWnd, "m_GiveUpButton")
RegistClassMember(LuaLiangHaoSettingWnd, "m_RenewButton")

function LuaLiangHaoSettingWnd:Init()

	self.m_LiangHaoLabel = self.transform:Find("Anchor/LiangHaoLabel"):GetComponent(typeof(UILabel))
	self.m_ExpireTimeLabel = self.transform:Find("Anchor/LiangHaoLabel/Label"):GetComponent(typeof(UILabel))

	self.m_ShowIconCheckBox = self.transform:Find("Anchor/ShowIconCheckBox"):GetComponent(typeof(QnCheckBox))
	self.m_ShowDigitIconCheckBox = self.transform:Find("Anchor/ShowDigitIconCheckBox"):GetComponent(typeof(QnCheckBox))
	self.m_RefuseAddFriendRequestCheckBox = self.transform:Find("Anchor/RefuseAddFriendRequestCheckBox"):GetComponent(typeof(QnCheckBox))
	self.m_RefuseStrangerMsgCheckBox = self.transform:Find("Anchor/RefuseStrangerMsgCheckBox"):GetComponent(typeof(QnCheckBox))
	self.m_InfoButton = self.transform:Find("Anchor/InfoButton").gameObject
	self.m_GiveUpButton = self.transform:Find("Anchor/GiveUpButton").gameObject
	self.m_RenewButton = self.transform:Find("Anchor/RenewButton").gameObject
	
	CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_GiveUpButton, DelegateFactory.Action_GameObject(function(go) self:OnGiveUpButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_RenewButton, DelegateFactory.Action_GameObject(function(go) self:OnRenewButtonClick() end), false)

	self.m_ShowIconCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
		local val = 1
		if selected then val = 0 end
		self:SetLiangHaoPrivilege(EnumLiangHaoPrivilegeType.HideIcon, val)
	end)
	self.m_ShowDigitIconCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
		local val = 0
		if selected then val = 1 end
		self:SetLiangHaoPrivilege(EnumLiangHaoPrivilegeType.ShowDigitIcon, val)
	end)
	self.m_RefuseAddFriendRequestCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
		local val = 0
		if selected then val = 1 end
		self:SetLiangHaoPrivilege(EnumLiangHaoPrivilegeType.RefuseAddCrossFriend, val)
	end)
	self.m_RefuseStrangerMsgCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
		local val = 0
		if selected then val = 1 end
		self:SetLiangHaoPrivilege(EnumLiangHaoPrivilegeType.RefuseCrossStrangerMessage, val)
	end)

	self:InitLiangHaoInfo()
end

function LuaLiangHaoSettingWnd:SetLiangHaoPrivilege(tp,val)
	LuaLiangHaoMgr.RequestSetLiangHaoPrivilege(tp, val)
end

function LuaLiangHaoSettingWnd:InitLiangHaoInfo()
	if not self.m_ShowIconCheckBox then return end

	local profileInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.ProfileInfo

	if not profileInfo then return end

	self.m_LiangHaoLabel.text = tostring(profileInfo.LiangHaoId)
	local str = nil
	local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(profileInfo.LiangHaoExpiredTime)
	if CServerTimeMgr.Inst.timeStamp>profileInfo.LiangHaoExpiredTime then
        local now = CServerTimeMgr.Inst:GetZone8Time()
        str = SafeStringFormat3(LocalString.GetString("[c][ff0000]已过期%d天[-][/c]"), math.ceil(now:Subtract(endTime).TotalDays))
        self.m_ExpireTimeLabel.text = str
	else
		str = ToStringWrap(endTime, "yyyy-MM-dd")
		self.m_ExpireTimeLabel.text = SafeStringFormat3(LocalString.GetString("有效期至 %s"), str)
	end

	self.m_ShowIconCheckBox.Text = SafeStringFormat3(LocalString.GetString("显示靓号图标(如:%s%s)"), CClientMainPlayer.Inst.Name, CUICommonDef.GetLiangHaoIcon(profileInfo, true, true) or "")
	self.m_ShowIconCheckBox:SetSelected(profileInfo.HideLiangHaoIcon==0, true)
	self.m_ShowDigitIconCheckBox:SetSelected(profileInfo.ShowDigitIcon~=0, true)
	self.m_RefuseAddFriendRequestCheckBox:SetSelected(CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.RefuseAddCrossFriend), true)
	self.m_RefuseStrangerMsgCheckBox:SetSelected(CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.RefuseCrossStrangerMessage),true)
end

function LuaLiangHaoSettingWnd:OnEnable()
	g_ScriptEvent:AddListener("OnLiangHaoPrivilegeSettingUpdate", self, "OnLiangHaoPrivilegeSettingUpdate")
	g_ScriptEvent:AddListener("OnSyncLiangHaoInfo", self, "OnSyncLiangHaoInfo")
end

function LuaLiangHaoSettingWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnLiangHaoPrivilegeSettingUpdate", self, "OnLiangHaoPrivilegeSettingUpdate")
	g_ScriptEvent:RemoveListener("OnSyncLiangHaoInfo", self, "OnSyncLiangHaoInfo")
end

function LuaLiangHaoSettingWnd:OnLiangHaoPrivilegeSettingUpdate()
	self:InitLiangHaoInfo()
end

function LuaLiangHaoSettingWnd:OnSyncLiangHaoInfo()
	self:InitLiangHaoInfo()
end

function LuaLiangHaoSettingWnd:OnInfoButtonClick()
	g_MessageMgr:ShowMessage("Lianghao_Readme")
end

function LuaLiangHaoSettingWnd:OnGiveUpButtonClick()
	LuaLiangHaoMgr.RequestGiveUpLiangHao()
	self:Close()
end

function LuaLiangHaoSettingWnd:OnRenewButtonClick()
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	LuaLiangHaoMgr.ShowLiangHaoRenewWnd(mainplayer.Id, mainplayer.BasicProp.ProfileInfo.LiangHaoId)
end

function LuaLiangHaoSettingWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
