local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

LuaZYJ2022MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZYJ2022MainWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaZYJ2022MainWnd, "Btn1", "Btn1", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "Btn2", "Btn2", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "Btn3", "Btn3", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "Btn4", "Btn4", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "Item3", "Item3", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "BuyBtn", "BuyBtn", GameObject)
RegistChildComponent(LuaZYJ2022MainWnd, "GSNZTime", "GSNZTime", UILabel)

--@endregion RegistChildComponent end

function LuaZYJ2022MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    UIEventListener.Get(self.Btn1.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnBtn1Click()
        end
    )

    UIEventListener.Get(self.Btn2.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnBtn2Click()
        end
    )

    UIEventListener.Get(self.Btn3.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnBtn3Click()
        end
    )

    UIEventListener.Get(self.Btn4.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnBtn4Click()
        end
    )

    UIEventListener.Get(self.BuyBtn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnBuyBtnClick()
        end
    )

    --@endregion EventBind end
end

function LuaZYJ2022MainWnd:Init()
    if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
        -- HMT不开几个活动
		self.transform:Find("YaoZhengQingYin").gameObject:SetActive(false)
		self.transform:Find("Buy").gameObject:SetActive(false)
	end

    CScheduleMgr.Inst:RequestActivity()
    local setting = ZhongYuanJie_Setting2022.GetData()
    self.TimeLabel.text = setting.MainTimeDesc
    self.GSNZTime.text = setting.GSNZ_OpenTime
    local ctrls = {self.Item1, self.Item2, self.Item3}
    local items = setting.MainRewardItems
    local itemnames = setting.MainRewardItemNames
    local iconBg = {self.transform:Find("Reward/Bg01"),self.transform:Find("Reward/Bg02"),self.transform:Find("Reward/Bg03")}
    for i = 0, 2 do
        if i < items.Length then
            local itemid = items[i]
            self:InitItem(itemid, ctrls[i + 1], itemnames[i])
            iconBg[i + 1].gameObject:SetActive(true)
        else
            ctrls[i+1]:SetActive(false)
            iconBg[i + 1].gameObject:SetActive(false)
        end
    end
end

function LuaZYJ2022MainWnd:OnEnable()
    CLuaScheduleMgr.BuildInfos()
    g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")
end

function LuaZYJ2022MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")
end

function LuaZYJ2022MainWnd:OnUpdateActivity()
    CLuaScheduleMgr.BuildInfos()
end

function LuaZYJ2022MainWnd:InitItem(itemid, item, itemname)
    local itemcfg = Item_Item.GetData(itemid)
    local trans = item.transform
    local iconTex = FindChildWithType(trans, "Panel/Icon", typeof(CUITexture))
    local ctTxt = FindChildWithType(trans, "AmountLabel", typeof(UILabel))
    if itemcfg then
        iconTex:LoadMaterial(itemcfg.Icon)
    end
    ctTxt.text = itemname
    UIEventListener.Get(item.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    )
end

--@region UIEvent

function LuaZYJ2022MainWnd:OnBtn1Click()
    local setting = ZhongYuanJie_Setting2022.GetData()
    local id = setting.MDJS_ID
    CLuaScheduleMgr.ShowJieRiScheduleInfo(id, "ZHONGYUANJIE2022_GUSHUNINGZHU_NOTOPEN_TEST")
end

function LuaZYJ2022MainWnd:OnBtn2Click()
    local setting = ZhongYuanJie_Setting2022.GetData()
    local id = setting.YYMJ_ID
    CLuaScheduleMgr.ShowJieRiScheduleInfo(id, "ZHONGYUANJIE2022_GUSHUNINGZHU_NOTOPEN_TEST")
end

function LuaZYJ2022MainWnd:OnBtn3Click()
    if ZhongYuanJie2022Mgr.IsGSNZOpen then
        CUIManager.ShowUI(CLuaUIResources.GSNZEnterWnd)
    else
        g_MessageMgr:ShowMessage("ZHONGYUANJIE2022_GUSHUNINGZHU_NOTOPEN")
    end
end

function LuaZYJ2022MainWnd:OnBtn4Click()
    local setting = ZhongYuanJie_Setting2022.GetData()
    local id = setting.YZQY_ID
    CLuaScheduleMgr.ShowJieRiScheduleInfo(id)
end

function LuaZYJ2022MainWnd:OnBuyBtnClick()
    local setting = ZhongYuanJie_Setting2022.GetData()
    local templateId = setting.YZQY_PROP_TEMPLATE_ID
    CShopMallMgr.ShowLinyuShoppingMall(templateId)
end

--@endregion UIEvent
