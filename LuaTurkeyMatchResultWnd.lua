local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CForcesMgr = import "L10.Game.CForcesMgr"
local Animation = import "UnityEngine.Animation"

LuaTurkeyMatchResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTurkeyMatchResultWnd, "WinPicture", "WinPicture", GameObject)
RegistChildComponent(LuaTurkeyMatchResultWnd, "FailPicture", "FailPicture", GameObject)
RegistChildComponent(LuaTurkeyMatchResultWnd, "ScoreLab", "ScoreLab", UILabel)
RegistChildComponent(LuaTurkeyMatchResultWnd, "RankLab", "RankLab", UILabel)
RegistChildComponent(LuaTurkeyMatchResultWnd, "DamageLab", "DamageLab", UILabel)
RegistChildComponent(LuaTurkeyMatchResultWnd, "Time", "Time", GameObject)
RegistChildComponent(LuaTurkeyMatchResultWnd, "TimeLab", "TimeLab", UILabel)
RegistChildComponent(LuaTurkeyMatchResultWnd, "ShareBtn", "ShareBtn", QnButton)
RegistChildComponent(LuaTurkeyMatchResultWnd, "PlayAgainBtn", "PlayAgainBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaTurkeyMatchResultWnd, "m_HideWhenCapture")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_Snowman")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_SnowmanBg")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_RedSnowman")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_DamageOffset")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_DamageCrown")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_RedMat")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_BlueMat")
RegistClassMember(LuaTurkeyMatchResultWnd, "m_Animation")

function LuaTurkeyMatchResultWnd:Awake()
    self.m_Snowman          = self.transform:Find("Bg/Snowman"):GetComponent(typeof(UITexture))
    self.m_SnowmanBg        = self.transform:Find("Bg/Snowman/Bg"):GetComponent(typeof(UITexture))
    self.m_RedSnowman       = self.transform:Find("Bg/Red"):GetComponent(typeof(UITexture))
    self.m_DamageOffset     = self.transform:Find("Anchor/Right/GamePlayInfo/Damage/Offset")
    self.m_DamageCrown      = self.transform:Find("Anchor/Right/GamePlayInfo/Damage/Offset/Crown").gameObject
    self.m_HideWhenCapture  = self.transform:Find("Anchor/Right/HideWhenCapture").gameObject
    self.m_Animation        = self.transform:GetComponent(typeof(Animation))
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.ShareBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnShareBtnClick()
    end)
    self.PlayAgainBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CUIManager.CloseUI(CLuaUIResources.TurkeyMatchResultWnd)
        CUIManager.ShowUI(CLuaUIResources.Christmas2022MainWnd)
    end)

    self.m_RedMat = self.m_RedSnowman.material
    self.m_BlueMat = self.m_Snowman.material

    if CommonDefs.IS_VN_CLIENT then
        self.ShareBtn.gameObject:SetActive(false)
    end
end

function LuaTurkeyMatchResultWnd:Init()
    local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
    local isWin = LuaChristmas2022Mgr.m_IsWin
    local score = LuaChristmas2022Mgr.m_Score
    local rank = LuaChristmas2022Mgr.m_Rank
    local selfDamage = LuaChristmas2022Mgr.m_SelfDamage or 0
    local teamTotalDamage = LuaChristmas2022Mgr.m_TeamTotalDamage or 0
    local killTurkeyTime = LuaChristmas2022Mgr.m_KillTurkeyTime or 0
    local percent = selfDamage > 0 and selfDamage * 100 / teamTotalDamage or 0
    local isFirstDamager = LuaChristmas2022Mgr.m_IsFirstDamager 

    LuaUtils.SetLocalPositionY(self.m_DamageOffset, isWin and 38 or 0)
    self.WinPicture:SetActive(isWin)
    self.FailPicture:SetActive(not isWin)
    self.Time:SetActive(isWin)
    self.ScoreLab.text = score
    self.RankLab.text = rank > 0 and SafeStringFormat3(LocalString.GetString("排名更新至%d名"), rank) or ""
    self.DamageLab.text = SafeStringFormat3(LocalString.GetString("%.f(占我方%.f%%)"), selfDamage, percent)
    self.TimeLab.text = self:GetRemainTimeText(killTurkeyTime)
    self.m_Snowman.material = myForce == 1 and self.m_RedMat or self.m_BlueMat
    self.m_SnowmanBg.color = NGUIText.ParseColor24(myForce == 1 and "A43029" or "186DB5", 0)
    self.m_DamageCrown:SetActive(isFirstDamager)

    if isWin then
        self.m_Animation:Play("turkeymatchresultwnd_slshow")
    else
        self.m_Animation:Play("turkeymatchresultwnd_sbshow")
    end
end

--@region UIEvent

--@endregion UIEvent
function LuaTurkeyMatchResultWnd:OnShareBtnClick()
    CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.TurkeyMatchResultWnd, self.m_HideWhenCapture)
end

function LuaTurkeyMatchResultWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),  
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60), 
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end