-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local CFirstChargeGiftWindow = import "L10.UI.CFirstChargeGiftWindow"
local Charge_ChargeSetting = import "L10.Game.Charge_ChargeSetting"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
CFirstChargeGiftWindow.m_Awake_CS2LuaHook = function (this) 
    this.chargeInfo = CClientMainPlayer.Inst.ItemProp.FirstCharge
    local default
    if this.chargeInfo.ChargeTime > 0 then
        default = LocalString.GetString("领取奖励")
    else
        default = LocalString.GetString("前往充值")
    end
    this.chargeLabel.text = default
    local chargeSetting = Charge_ChargeSetting.GetData("FirstChargeGift")
    this.giftInfo = CommonDefs.StringSplit_ArrayChar(chargeSetting.Value, ";")
    if this.giftInfo ~= nil then
        Extensions.RemoveAllChildren(this.table.transform)
        this.giftTemplate:SetActive(false)
        do
            local i = 0
            while i < this.giftInfo.Length do
                local info = CommonDefs.StringSplit_ArrayChar(this.giftInfo[i], ",")
                if info ~= nil and info.Length == 2 then
                    local gift = NGUITools.AddChild(this.table.gameObject, this.giftTemplate)
                    CommonDefs.GetComponent_GameObject_Type(gift, typeof(CFirstChargeGiftCell)):Init(System.UInt32.Parse(info[0]), System.UInt32.Parse(info[1]))
                    gift:SetActive(true)
                end
                i = i + 1
            end
        end
        this.table:Reposition()
    end
end
CFirstChargeGiftWindow.m_OnChargeBtnClick_CS2LuaHook = function (this, go) 
    --MessageMgr.Inst.ShowMessage("FUNCTION_NOT_OPEN");
    --MessageMgr.Inst.ShowMessage("FUNCTION_NOT_OPEN");
    if this.chargeInfo ~= nil and this.chargeInfo.ChargeTime > 0 then
        Gac2Gas.RequestGetFirstChargeGift()
    else
        CUIManager.CloseUI(CUIResources.WelfareWnd)
        CShopMallMgr.ShowChargeWnd()
    end
end
