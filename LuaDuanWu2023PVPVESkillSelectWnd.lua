local CButton        = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSocialWndMgr  = import "L10.UI.CSocialWndMgr"
local EChatPanel     = import "L10.Game.EChatPanel"
local CSkillInfoMgr  = import "L10.UI.CSkillInfoMgr"

LuaDuanWu2023PVPVESkillSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2023PVPVESkillSelectWnd, "grid", "Grid", UIGrid)
RegistChildComponent(LuaDuanWu2023PVPVESkillSelectWnd, "template", "Template", GameObject)
RegistChildComponent(LuaDuanWu2023PVPVESkillSelectWnd, "okButton", "OkButton", CButton)
RegistChildComponent(LuaDuanWu2023PVPVESkillSelectWnd, "chatButton", "ChatButton", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaDuanWu2023PVPVESkillSelectWnd, "tick")
RegistClassMember(LuaDuanWu2023PVPVESkillSelectWnd, "curSelect")

function LuaDuanWu2023PVPVESkillSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.okButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)

	UIEventListener.Get(self.chatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)
    --@endregion EventBind end
	self.template:SetActive(false)
end

function LuaDuanWu2023PVPVESkillSelectWnd:Init()
	self:ClearTick()
	self:UpdateTime()
	self.tick = RegisterTick(function()
		self:UpdateTime()
	end, 1000)

	Extensions.RemoveAllChildren(self.grid.transform)
	local setting = Duanwu2023_PVPVESetting.GetData()
	local skillIds = setting.TempSkillIds
	local skillDesc = setting.TempSkillDesc
	local skillDescTbl = {}
	for desc in string.gmatch(skillDesc, "([^;]+)") do
		table.insert(skillDescTbl, desc)
	end

	for i = 0, skillIds.Length - 1 do
		local child = NGUITools.AddChild(self.grid.gameObject, self.template)
		child:SetActive(true)
		local skillId = skillIds[i]
		local skillData = Skill_AllSkills.GetData(skillId)
		local icon = child.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
		icon:LoadSkillIcon(skillData.SkillIcon)
		child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = skillData.Name
		child.transform:Find("Desc"):GetComponent(typeof(UILabel)).text = skillDescTbl[i + 1]
		UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnSkillClick(i)
        end)

		UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CSkillInfoMgr.ShowSkillInfoWnd(skillId, true, 0, 0, nil)
        end)
	end
	self.grid:Reposition()
	self.curSelect = -1
	self:OnSkillClick(0)
end

function LuaDuanWu2023PVPVESkillSelectWnd:UpdateTime()
	local endTimeStamp = LuaDuanWu2023Mgr.skillSelectEndTimeStamp
	local leftTime = math.ceil(endTimeStamp - CServerTimeMgr.Inst.timeStamp)
	if leftTime <= 0 then
		if self.curSelect and self.curSelect >= 0 then
			Gac2Gas.DuanWu2023PVPVESeleteTempSkill(self.curSelect + 1)
		end
		CUIManager.CloseUI(CLuaUIResources.DuanWu2023PVPVESkillSelectWnd)
		return
	end

	self.okButton.Text = SafeStringFormat3(LocalString.GetString("确认选择(%d)"), leftTime)
end

function LuaDuanWu2023PVPVESkillSelectWnd:ClearTick()
	if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

function LuaDuanWu2023PVPVESkillSelectWnd:OnDestroy()
	self:ClearTick()
	LuaDuanWu2023Mgr.skillSelectEndTimeStamp = nil
end

--@region UIEvent

function LuaDuanWu2023PVPVESkillSelectWnd:OnOkButtonClick()
	Gac2Gas.DuanWu2023PVPVESeleteTempSkill(self.curSelect + 1)
	CUIManager.CloseUI(CLuaUIResources.DuanWu2023PVPVESkillSelectWnd)
end

function LuaDuanWu2023PVPVESkillSelectWnd:OnSkillClick(i)
	if self.curSelect == i then return end

	if self.curSelect >= 0 then
		self.grid.transform:GetChild(self.curSelect):GetComponent(typeof(CButton)).Selected = false
	end

	self.curSelect = i
	self.grid.transform:GetChild(self.curSelect):GetComponent(typeof(CButton)).Selected = true
end

function LuaDuanWu2023PVPVESkillSelectWnd:OnChatButtonClick()
	CSocialWndMgr.ShowChatWnd(EChatPanel.Ally)
end

--@endregion UIEvent
