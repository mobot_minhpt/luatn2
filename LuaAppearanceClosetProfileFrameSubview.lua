local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetProfileFrameSubview = class()

RegistChildComponent(LuaAppearanceClosetProfileFrameSubview,"m_ProfileFrameItem", "CommonClosetSingleLineItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetProfileFrameSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetProfileFrameSubview,"m_MainPlayerProfileFrame", "MainPlayerProfileFrame", GameObject)
RegistChildComponent(LuaAppearanceClosetProfileFrameSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetProfileFrameSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceClosetProfileFrameSubview, "m_AllFrames")
RegistClassMember(LuaAppearanceClosetProfileFrameSubview, "m_SelectedDataId")

function LuaAppearanceClosetProfileFrameSubview:Awake()
end

function LuaAppearanceClosetProfileFrameSubview:Init()
    self:PlayAppearAnimation()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetProfileFrameSubview:PlayAppearAnimation()
    self.m_MainPlayerProfileFrame:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.m_MainPlayerProfileFrame.transform:GetComponent(typeof(UIWidget)), 0, 1, 0.8)
    local portraitTexture = self.m_MainPlayerProfileFrame.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    portraitTexture:LoadNPCPortrait(CClientMainPlayer.Inst and CUICommonDef.GetDieKeSpecialPortraitName(CClientMainPlayer.Inst) or "")
end

function LuaAppearanceClosetProfileFrameSubview:LoadData()
    self.m_AllFrames = LuaAppearancePreviewMgr:GetAllProfileFrameInfo(true)

    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, # self.m_AllFrames do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ProfileFrameItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllFrames[i])
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetProfileFrameSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseProfileFrame()
end

function LuaAppearanceClosetProfileFrameSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllFrames[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetProfileFrameSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local disabledGo = itemGo.transform:Find("Item/Disabled").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    iconTexture:LoadMaterial(appearanceData.icon)
    nameLabel.text = appearanceData.name
    conditionLabel.text = LuaAppearancePreviewMgr:GetProfileFrameConditionText(appearanceData.id)
    if CClientMainPlayer.Inst then
        disabledGo:SetActive(LuaAppearancePreviewMgr:IsProfileFrameExpired(appearanceData.id))
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseProfileFrame(appearanceData.id))
    else
        disabledGo:SetActive(false)
        cornerGo:SetActive(false)
    end
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetProfileFrameSubview:OnItemClick(go)
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_AllFrames[i+1].id
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    self:UpdateItemDisplay()
end

function LuaAppearanceClosetProfileFrameSubview:UpdateItemDisplay()
    local frameTexture = self.m_MainPlayerProfileFrame.transform:Find("Icon/Frame"):GetComponent(typeof(CUITexture))
    local frameNameLabel = self.m_MainPlayerProfileFrame.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local frameId = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if frameId==0 then
        frameTexture:Clear()
        frameNameLabel.text = ""
        self.m_ItemDisplay.gameObject:SetActive(false)
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllFrames do
        if self.m_AllFrames[i].id == frameId then
            appearanceData = self.m_AllFrames[i]
            break
        end
    end
    frameTexture:LoadMaterial(appearanceData.icon)
    frameNameLabel.text = appearanceData.name
    self.m_ItemDisplay.gameObject:SetActive(true)
    local exist = LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseProfileFrame(appearanceData.id)
    local expired = exist and LuaAppearancePreviewMgr:IsProfileFrameExpired(appearanceData.id) or false
    local buttonTbl = {}
    if exist then
        if inUse then
            table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
        elseif expired then
            table.insert(buttonTbl, {text=LocalString.GetString("丢弃"), isYellow=false, action=function(go) self:OnDiscardButtonClick() end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
        end
    else
        table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
    end

    self.m_ItemDisplay:Init(buttonTbl)
end

function LuaAppearanceClosetProfileFrameSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetProfileFrame(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetProfileFrameSubview:OnDiscardButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestDiscardProfileFrame(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetProfileFrameSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetProfileFrame(0)
end

function LuaAppearanceClosetProfileFrameSubview:OnMoreButtonClick(go)
    local frameId = self.m_SelectedDataId and self.m_SelectedDataId or 0
    local appearanceData = nil
    for i=1,#self.m_AllFrames do
        if self.m_AllFrames[i].id == frameId then
            appearanceData = self.m_AllFrames[i]
            break
        end
    end
    if appearanceData then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(appearanceData.itemGetId, true, go.transform)
    end
end

function  LuaAppearanceClosetProfileFrameSubview:OnEnable()
    g_ScriptEvent:AddListener("UnlockMainPlayerProfileFrame", self, "OnUnlockMainPlayerProfileFrame")
    g_ScriptEvent:AddListener("UpdateMainPlayerProfileInfo", self, "OnUpdateMainPlayerProfileInfo")
end

function  LuaAppearanceClosetProfileFrameSubview:OnDisable()
    g_ScriptEvent:RemoveListener("UnlockMainPlayerProfileFrame", self, "OnUnlockMainPlayerProfileFrame")
    g_ScriptEvent:RemoveListener("UpdateMainPlayerProfileInfo", self, "OnUpdateMainPlayerProfileInfo")
end

function LuaAppearanceClosetProfileFrameSubview:OnUnlockMainPlayerProfileFrame(args)
    self:LoadData()
end

function LuaAppearanceClosetProfileFrameSubview:OnUpdateMainPlayerProfileInfo()
    self:SetDefaultSelection()
    self:LoadData()
end
