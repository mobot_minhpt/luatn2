local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"

LuaGetHaoYiXingHaiTangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGetHaoYiXingHaiTangWnd, "HaiTangLabel", "HaiTangLabel", UILabel)
RegistChildComponent(LuaGetHaoYiXingHaiTangWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGetHaoYiXingHaiTangWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaGetHaoYiXingHaiTangWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaGetHaoYiXingHaiTangWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaGetHaoYiXingHaiTangWnd, "TopLabel", "TopLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaGetHaoYiXingHaiTangWnd, "m_TabIndex")

function LuaGetHaoYiXingHaiTangWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)

	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)
	self.TopLabel.transform.localPosition = Vector3(-315, -45, 0)
	self.TopLabel.width = 890
	self.TopLabel.height = 50
	local sprite = self.TopLabel.transform.transform:Find("Sprite"):GetComponent(typeof(UISprite))
	sprite.width = 840
	sprite.height = 44
    --@endregion EventBind end
end

function LuaGetHaoYiXingHaiTangWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncErHaTCGData",self,"OnUpdateData")
	g_ScriptEvent:AddListener("OnErHaTCGBuyHaiTangSucc",self,"OnUpdateData")
end

function LuaGetHaoYiXingHaiTangWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncErHaTCGData",self,"OnUpdateData")
	g_ScriptEvent:RemoveListener("OnErHaTCGBuyHaiTangSucc",self,"OnUpdateData")
end

function LuaGetHaoYiXingHaiTangWnd:OnUpdateData()
	self.HaiTangLabel.text = SafeStringFormat3(LocalString.GetString("已拥有%d朵"),LuaHaoYiXingMgr.m_HaitangCount)
end

function LuaGetHaoYiXingHaiTangWnd:Init()
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(v)
        self.QnCostAndOwnMoney:SetCost(self.m_TabIndex == 0 and tonumber(SpokesmanTCG_ErHaSetting.GetData("BuyHaiTangNeedSilver").Value)* v or
		tonumber(SpokesmanTCG_ErHaSetting.GetData("BuyHaiTangNeedJade").Value) * v) 
    end)
	self.TopLabel.text = g_MessageMgr:FormatMessage("HaoYiXingCard_TianQi_Msg")
	self.QnIncreseAndDecreaseButton:SetMinMax(1,999,1)
	self.QnIncreseAndDecreaseButton:SetValue(1)
	self.HaiTangLabel.text = SafeStringFormat3(LocalString.GetString("已拥有%d朵"),LuaHaoYiXingMgr.m_HaitangCount)
	self.Tabs:ChangeTab(1, false)
end

--@region UIEvent

function LuaGetHaoYiXingHaiTangWnd:OnTabsTabChange(index)
	self.QnCostAndOwnMoney:SetType(index == 0 and EnumMoneyType_lua.YinLiang or EnumMoneyType_lua.LingYu, 0, false)
	self.QnCostAndOwnMoney:SetCost(index == 0 and tonumber(SpokesmanTCG_ErHaSetting.GetData("BuyHaiTangNeedSilver").Value)* self.QnIncreseAndDecreaseButton:GetValue() or
	tonumber(SpokesmanTCG_ErHaSetting.GetData("BuyHaiTangNeedJade").Value)* self.QnIncreseAndDecreaseButton:GetValue())
	self.m_TabIndex = index
end

function LuaGetHaoYiXingHaiTangWnd:OnButtonClick()
	Gac2Gas.ErHaTCGBuyHaiTang(self.m_TabIndex == 0,self.QnIncreseAndDecreaseButton:GetValue())
end

--@endregion UIEvent

