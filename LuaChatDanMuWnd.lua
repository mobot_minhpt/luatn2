local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Vector3 = import "UnityEngine.Vector3"
local CChatMgr = import "L10.Game.CChatMgr"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CChatHelper = import "L10.UI.CChatHelper"

LuaChatDanMuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChatDanMuWnd, "ScreenBound", UIWidget)
RegistChildComponent(LuaChatDanMuWnd, "Pool", CUIGameObjectPool)

RegistClassMember(LuaChatDanMuWnd, "m_AttentionPlayerTbl")
RegistClassMember(LuaChatDanMuWnd, "m_BulletList")
--@endregion RegistChildComponent end

function LuaChatDanMuWnd:Awake()
    self.m_OnNewVoiceTextReady = DelegateFactory.Action_uint(function(voiceId, text)
        self:OnNewVoiceTextReady()
    end)
end

function LuaChatDanMuWnd:Init()
    self:UpdateAttentionPlayerTbl()
end

function LuaChatDanMuWnd:OnRecvChatMsg(channelId)
    local msg = CChatMgr.Inst:GetLatestMsg()
    local channel = CChatHelper.GetChannelByName(msg.channelName)
    if msg ~= nil and true then
        self:OnAddBullet(msg)
    end
end

function LuaChatDanMuWnd:OnAddBullet(msg)
    local bullet = self.Pool:GetFromPool(0)
    if self.m_AttentionPlayerTbl and self.m_AttentionPlayerTbl[msg.fromUserId] then
        bullet = self.Pool:GetFromPool(1)
    end
    local luascrip = bullet:GetComponent(typeof(CCommonLuaScript))
    if luascrip then
        bullet.transform.parent = self.ScreenBound.transform
        bullet.transform.localScale = Vector3.one
        bullet:SetActive(true)
        luascrip.m_LuaSelf:Init(msg, self.ScreenBound.width, self.ScreenBound.height)
    end
end

function LuaChatDanMuWnd:OnChatDanMuRecycle(bullet)
    if bullet and bullet.gameObject then
        self.Pool:Recycle(bullet.gameObject)
    end
end

function LuaChatDanMuWnd:UpdateAttentionPlayerTbl()
    self.m_AttentionPlayerTbl = {}
    for i = 1, #LuaChatMgr.m_DanMuAttentionPlayerList do
        local playerId = LuaChatMgr.m_DanMuAttentionPlayerList[i]
        if playerId ~= nil then
            self.m_AttentionPlayerTbl[playerId] = true
        end
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaChatDanMuWnd:OnEnable()
    g_ScriptEvent:AddListener("RecvChatMsg", self, "OnRecvChatMsg")
    g_ScriptEvent:AddListener("ChatDanMuRecycle", self, "OnChatDanMuRecycle")
end

function LuaChatDanMuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RecvChatMsg", self, "OnRecvChatMsg")
    g_ScriptEvent:AddListener("ChatDanMuRecycle", self, "OnChatDanMuRecycle")
end