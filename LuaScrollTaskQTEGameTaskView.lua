local UILabel = import "UILabel"

LuaScrollTaskQTEGameTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaScrollTaskQTEGameTaskView, "GameDesc", "GameDesc", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaScrollTaskQTEGameTaskView, "m_IsPlaying")

function LuaScrollTaskQTEGameTaskView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaScrollTaskQTEGameTaskView:Start()

end

function LuaScrollTaskQTEGameTaskView:InitGame(gameId, taskId, callback)
    self:LoadDesignData(gameId)

    self.m_Callback = callback

    self.m_IsPlaying = true
    -- -- 不阻塞
    -- self:OnSuccess()
end

function LuaScrollTaskQTEGameTaskView:LoadDesignData(gameId)
    self.GameDesc.text = ScrollTask_QTEGame.GetData(gameId).Desc
end

--@region UIEvent

--@endregion UIEvent


function LuaScrollTaskQTEGameTaskView:OnEnable()
    g_ScriptEvent:AddListener("QTEWndInitMessage", self, "OnQTEWndInitMessage")
    g_ScriptEvent:AddListener("OnQteFinished", self, "OnQteFinished")
end

function LuaScrollTaskQTEGameTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("QTEWndInitMessage", self, "OnQTEWndInitMessage")
    g_ScriptEvent:RemoveListener("OnQteFinished", self, "OnQteFinished")
end

function LuaScrollTaskQTEGameTaskView:OnQTEWndInitMessage(message)
    if message==nil or message=="" then
        self.GameDesc.gameObject:SetActive(false)
        return
    else
        self.GameDesc.gameObject:SetActive(true)
    end
    self.GameDesc.text = SafeStringFormat3("[c][%s]%s[-][/c]", 
        NGUIText.EncodeColor24(self.GameDesc.color), CChatLinkMgr.TranslateToNGUIText(message, false))
    self.GameDesc:UpdateNGUIText()
end

function LuaScrollTaskQTEGameTaskView:OnQteFinished(bSuccess)
    self:OnSuccess()
    -- body
end

function LuaScrollTaskQTEGameTaskView:OnSuccess()
    if self.m_IsPlaying then
        self.m_IsPlaying = false
        self:OnGameFinish()
    end
end

function LuaScrollTaskQTEGameTaskView:OnGameFinish()
    if self.m_Callback then
        self.m_Callback()
    end
end

