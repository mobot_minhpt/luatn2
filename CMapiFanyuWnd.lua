-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CMapiFanyuWnd = import "L10.UI.CMapiFanyuWnd"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CSelectMapiWnd = import "L10.UI.CSelectMapiWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CMapiFanyuWnd.m_Init_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        this:Close()
        return
    end

    if CMapiFanyuWnd.sStarterPlayerId == 0 then
        CMapiFanyuWnd.sStarterPlayerId = CClientMainPlayer.Inst.Id
    end

    this.leftAddBtn:SetActive(true)
    this.leftAddMapiLabel.gameObject:SetActive(true)
    this.leftMapiName.gameObject:SetActive(false)
    this.leftGenderSprite.gameObject:SetActive(false)
    this.leftGradeLabel.gameObject:SetActive(false)
    this.leftPinzhongLabel.gameObject:SetActive(false)
    this.leftGenerationLabel.gameObject:SetActive(false)

    this.rightAddBtn:SetActive(false)
    this.rightAddMapiLabel.gameObject:SetActive(false)
    this.rightMapiName.gameObject:SetActive(false)
    this.rightGenderSprite.gameObject:SetActive(false)
    this.rightGradeLabel.gameObject:SetActive(false)
    this.rightPinzhongLabel.gameObject:SetActive(false)
    this.rightGenerationLabel.gameObject:SetActive(false)

    this.needItemNumLabel.gameObject:SetActive(false)
    this.needItemTexture.gameObject:SetActive(false)
    this.needItemGetLabel.gameObject:SetActive(false)
    this.needItemDisableSprite.gameObject:SetActive(true)
    this.statusLabel.gameObject:SetActive(false)
    this.wordLabel.gameObject:SetActive(false)

    this.needItemDisableSprite.transform.parent.gameObject:SetActive(false)

    CUICommonDef.SetActive(this.BtnStart, false, true)

    this:InitFanyuStatus()

    -- 接受邀请打开窗口时，收到rpc回来的时候，可能窗口还没打开好，导致这个消息收不到
    if CMapiFanyuWnd.sLeftPid ~= 0 and CMapiFanyuWnd.sRightPid ~= 0 then
        this:OnSendYumaWithFriendInfo()
        return
    end
end
CMapiFanyuWnd.m_InitFanyuStatus_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        this:Close()
        return
    end

    -- 先找一下繁育状态，找一下有没有正在繁育的马匹，有的话列出来，没有的话还是初始状态
    local zqdata = nil
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            local curzq = CZuoQiMgr.Inst.zuoqis[i]
            if curzq.mapiInfo ~= nil and curzq.mapiInfo.FanyuStartTime ~= 0 then
                zqdata = curzq
                this.mCurMapiInfo = zqdata.mapiInfo
                CMapiFanyuWnd.sStarterPlayerId = math.floor(zqdata.mapiInfo.BabyInfo.StarterPlayerId)
                break
            end
            i = i + 1
        end
    end

    if CMapiFanyuWnd.sStarterPlayerId == CClientMainPlayer.Inst.Id then
        --如果是发起人者
        if zqdata == nil then
            --什么也不用做，init里面已经初始化好了
        else
            local babyInfo = zqdata.mapiInfo.BabyInfo
            local parentAppearance = babyInfo.ParentAppearance

            this:InitLeftMapi(nil, zqdata.mapiInfo)
            this:InitRightMapi(parentAppearance, nil)
        end
    else
        -- 如果不是发起者
        if zqdata == nil and CMapiFanyuWnd.sStarterPlayerId ~= 0 and CMapiFanyuWnd.sStarterPlayerId ~= CClientMainPlayer.Inst.Id and CMapiFanyuWnd.sLeftZuoQi ~= nil then
            this:InitLeftMapi(nil, CMapiFanyuWnd.sLeftZuoQi.MapiInfo)
        elseif zqdata ~= nil and CMapiFanyuWnd.sStarterPlayerId ~= CClientMainPlayer.Inst.Id then
            local babyInfo = zqdata.mapiInfo.BabyInfo
            local parentAppearance = babyInfo.ParentAppearance

            this:InitRightMapi(parentAppearance, nil)
            this:InitLeftMapi(nil, zqdata.mapiInfo)
        end
    end

    if this.mCurMapiInfo ~= nil then
        this:UpdateStatusLabel()
        this.mTick = CTickMgr.Register(DelegateFactory.Action(function () 
            this:UpdateStatusLabel()
        end), CMapiFanyuWnd.STickInterval * 1000, ETickType.Loop)
    end
end
CMapiFanyuWnd.m_UpdateStatusLabel_CS2LuaHook = function (this) 
    if this.mCurMapiInfo ~= nil then
        this.BtnStart:SetActive(false)

        if this.setting == nil then
            this.setting = ZuoQi_Setting.GetData()
        end

        local chengzhangTime = this.setting.MapiChengzhangNeedTime
        local needtime = this.mCurMapiInfo.FanyuNeedTime
        local startTime = this.mCurMapiInfo.FanyuStartTime
        local curTime = CServerTimeMgr.Inst.timeStamp
        local percent = math.max(0, math.min(1, (curTime - startTime) / (needtime + chengzhangTime)))

        local msg = nil
        do
            local i = 0
            while i < this.setting.MapiFanyuMessages.Length do
                local gap = System.Single.Parse(this.setting.MapiFanyuMessages[i])
                if percent > gap then
                    msg = this.setting.MapiFanyuMessages[i + 1]
                end
                i = i + 2
            end
        end

        local message = ""
        if msg ~= nil and this.mCurMapiInfo.BabyInfo ~= nil and this.mCurMapiInfo.BabyInfo.ParentAppearance ~= nil then
            message = g_MessageMgr:FormatMessage(msg, this.mCurMapiInfo.Name, this.mCurMapiInfo.BabyInfo.ParentAppearance.Name)
        end
        this.wordLabel.text = message
        this.wordLabel.gameObject:SetActive(true)

        if percent < 1 then
            this.statusLabel.text = LocalString.GetString("繁育中")
        else
            this.statusLabel.text = LocalString.GetString("繁育完成")
        end
        this.statusLabel.gameObject:SetActive(true)
    end
end
CMapiFanyuWnd.m_InitLeftMapi_CS2LuaHook = function (this, parentAppearance, mapiProp) 
    if parentAppearance ~= nil then
        this.leftGenderSprite.spriteName = (parentAppearance.Gender == EnumMapiGender_lua.eMale) and "lingshoujiehun_yang" or "lingshoujiehun_yin"

        this.leftMapiLoader:Init(this:GetMapiPropFromMapiAppearance(parentAppearance), nil, nil, -135, nil)

        this.leftMapiName.text = parentAppearance.Name

        local appearanceName = CZuoQiMgr.GetAppearanceName(parentAppearance.PinzhongId, parentAppearance.Quality)
        local attrName = CZuoQiMgr.GetAttributeName(parentAppearance.AttributeNameId)
        this.leftPinzhongLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), appearanceName, attrName)
        this.leftGradeLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(parentAppearance.Level))
        this.leftGenerationLabel.text = System.String.Format(LocalString.GetString("{0}代"), parentAppearance.Generation)

        this.leftGenderSprite.gameObject:SetActive(true)
        this.leftPinzhongLabel.gameObject:SetActive(true)
        this.leftMapiName.gameObject:SetActive(true)
        this.leftGradeLabel.gameObject:SetActive(true)
    elseif mapiProp ~= nil then
        this.leftGenderSprite.spriteName = (mapiProp.Gender == EnumMapiGender_lua.eMale) and "lingshoujiehun_yang" or "lingshoujiehun_yin"

        this.leftMapiLoader:Init(mapiProp, nil, nil, -135, nil)

        this.leftMapiName.text = mapiProp.Name

        local appearanceName = CZuoQiMgr.GetAppearanceName(mapiProp.PinzhongId, mapiProp.Quality)
        local attrName = CZuoQiMgr.GetAttributeName(mapiProp.AttributeNameId)
        this.leftPinzhongLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), appearanceName, attrName)
        this.leftGradeLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(mapiProp.Level))
        this.leftGenerationLabel.text = System.String.Format(LocalString.GetString("{0}代"), mapiProp.Generation)

        this.leftGenderSprite.gameObject:SetActive(true)
        this.leftPinzhongLabel.gameObject:SetActive(true)
        this.leftMapiName.gameObject:SetActive(true)
        this.leftGradeLabel.gameObject:SetActive(true)
    end

    if mapiProp ~= nil or parentAppearance ~= nil then
        this.leftAddBtn:SetActive(false)
        this.leftAddMapiLabel.gameObject:SetActive(false)
        this.leftGenerationLabel.gameObject:SetActive(true)
    end
end
CMapiFanyuWnd.m_InitRightMapi_CS2LuaHook = function (this, parentAppearance, mapiProp) 
    if parentAppearance ~= nil then
        this.rightGenderSprite.spriteName = (parentAppearance.Gender == EnumMapiGender_lua.eMale) and "lingshoujiehun_yang" or "lingshoujiehun_yin"

        this.rightMapiLoader:Init(this:GetMapiPropFromMapiAppearance(parentAppearance), nil, nil, -135, nil)

        this.rightMapiName.text = parentAppearance.Name

        local appearanceName = CZuoQiMgr.GetAppearanceName(parentAppearance.PinzhongId, parentAppearance.Quality)
        local attrName = CZuoQiMgr.GetAttributeName(parentAppearance.AttributeNameId)
        this.rightPinzhongLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), appearanceName, attrName)
        this.rightGradeLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(parentAppearance.Level))
        this.rightGenerationLabel.text = System.String.Format(LocalString.GetString("{0}代"), parentAppearance.Generation)
    elseif mapiProp ~= nil then
        this.rightGenderSprite.spriteName = (mapiProp.Gender == EnumMapiGender_lua.eMale) and "lingshoujiehun_yang" or "lingshoujiehun_yin"

        this.rightMapiLoader:Init(mapiProp, nil, nil, -135, nil)

        this.rightMapiName.text = mapiProp.Name

        local appearanceName = CZuoQiMgr.GetAppearanceName(mapiProp.PinzhongId, mapiProp.Quality)
        local attrName = CZuoQiMgr.GetAttributeName(mapiProp.AttributeNameId)
        this.rightPinzhongLabel.text = System.String.Format(LocalString.GetString("{0}·{1}"), appearanceName, attrName)
        this.rightGradeLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(mapiProp.Level))
        this.rightGenerationLabel.text = System.String.Format(LocalString.GetString("{0}代"), mapiProp.Generation)
    end

    if mapiProp ~= nil or parentAppearance ~= nil then
        this.rightGenderSprite.gameObject:SetActive(true)
        this.rightPinzhongLabel.gameObject:SetActive(true)
        this.rightMapiName.gameObject:SetActive(true)
        this.rightGradeLabel.gameObject:SetActive(true)
        this.rightGenerationLabel.gameObject:SetActive(true)

        this.rightAddBtn:SetActive(false)
        this.rightAddMapiLabel.gameObject:SetActive(false)
    end
end
CMapiFanyuWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.leftAddBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.leftAddBtn).onClick, MakeDelegateFromCSFunction(this.OnLeftAddButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.rightAddBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rightAddBtn).onClick, MakeDelegateFromCSFunction(this.OnRightAddButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.BtnStart).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnStart).onClick, MakeDelegateFromCSFunction(this.OnStartButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.BtnQuestion).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnQuestion).onClick, MakeDelegateFromCSFunction(this.OnQuestionButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.needItemDisableSprite.transform.parent.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.needItemDisableSprite.transform.parent.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemNeedClick, VoidDelegate, this), true)

    EventManager.AddListener(EnumEventType.OnSendYumaWithFriendInfo, MakeDelegateFromCSFunction(this.OnSendYumaWithFriendInfo, Action0, this))
    EventManager.AddListener(EnumEventType.OnStartYumaSuccess, MakeDelegateFromCSFunction(this.OnStartYumaSuccess, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListener(EnumEventType.OnInviteYumaSuccess, MakeDelegateFromCSFunction(this.OnInviteYumaSuccess, Action0, this))
    EventManager.AddListener(EnumEventType.OnPlayerDenyYumaInvitation, MakeDelegateFromCSFunction(this.OnPlayerDenyYumaInvitation, Action0, this))
end
CMapiFanyuWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.leftAddBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.leftAddBtn).onClick, MakeDelegateFromCSFunction(this.OnLeftAddButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.rightAddBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rightAddBtn).onClick, MakeDelegateFromCSFunction(this.OnRightAddButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.BtnStart).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnStart).onClick, MakeDelegateFromCSFunction(this.OnStartButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.BtnQuestion).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnQuestion).onClick, MakeDelegateFromCSFunction(this.OnQuestionButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.needItemDisableSprite.transform.parent.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.needItemDisableSprite.transform.parent.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemNeedClick, VoidDelegate, this), false)

    EventManager.RemoveListener(EnumEventType.OnSendYumaWithFriendInfo, MakeDelegateFromCSFunction(this.OnSendYumaWithFriendInfo, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnStartYumaSuccess, MakeDelegateFromCSFunction(this.OnStartYumaSuccess, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListener(EnumEventType.OnInviteYumaSuccess, MakeDelegateFromCSFunction(this.OnInviteYumaSuccess, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnPlayerDenyYumaInvitation, MakeDelegateFromCSFunction(this.OnPlayerDenyYumaInvitation, Action0, this))
end
CMapiFanyuWnd.m_OnItemNeedClick_CS2LuaHook = function (this, go) 
    local setting = ZuoQi_Setting.GetData()
    local hasItemCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, setting.MapiFanyuNeedItemId)
    local enough = hasItemCount >= CMapiFanyuWnd.sNeedItemCount
    if enough then
        CItemInfoMgr.ShowLinkItemTemplateInfo(setting.MapiFanyuNeedItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(setting.MapiFanyuNeedItemId, false, this, AlignType.Default, 0, 0, 0, 0)
    end
end
CMapiFanyuWnd.m_OnRightAddButtonClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local houseId = CClientMainPlayer.Inst.ItemProp.HouseId
    if System.String.IsNullOrEmpty(houseId) then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你没有家园，无法进行繁育"))
        return
    end

    CSelectMapiWnd.SBShowInviteFriend = (CClientMainPlayer.Inst.Id == CMapiFanyuWnd.sStarterPlayerId)
    CUIManager.ShowUI(CUIResources.SelectMapiWnd)
end
CMapiFanyuWnd.m_OnStartButtonClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if not (CMapiFanyuWnd.sLeftPid ~= nil and CMapiFanyuWnd.sRightPid ~= nil and CMapiFanyuWnd.sLeftZuoQi ~= nil and CMapiFanyuWnd.sRightZuoQi ~= nil) then
        return
    end

    if CMapiFanyuWnd.sLeftPid == CMapiFanyuWnd.sRightPid then
        local msg = g_MessageMgr:FormatMessage("Mapi_Fanyu_Self_Confirm")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            Gac2Gas.RequestYumaSelf(CMapiFanyuWnd.sLeftZuoQi.Id, CMapiFanyuWnd.sRightZuoQi.Id)
        end), nil, nil, nil, false)
    else
        local msg = g_MessageMgr:FormatMessage("Mapi_Fanyu_With_Friend_Confirm")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            local pid = CClientMainPlayer.Inst.Id
            local partnerId = (pid == CMapiFanyuWnd.sLeftPid) and CMapiFanyuWnd.sRightPid or CMapiFanyuWnd.sLeftPid

            Gac2Gas.ConfirmStartYumaWithFriend(partnerId)
        end), nil, nil, nil, false)
    end
end
CMapiFanyuWnd.m_OnInviteYumaSuccess_CS2LuaHook = function (this) 
    this:UnRegisterInviteTick()

    this.tickTime = CMapiFanyuWnd.sInviteFanyuWaitTime
    this.rightAddBtn.gameObject:SetActive(false)

    local tickFunc = DelegateFactory.Action(function () 
        if this.tickTime >= 0 then
            this.rightAddMapiLabel.text = System.String.Format(LocalString.GetString("等待好友接受邀请({0})"), this.tickTime)
            this.tickTime = this.tickTime - 1
        else
            this:UnRegisterInviteTick()
            if CMapiFanyuWnd.sLeftPid ~= 0 and CMapiFanyuWnd.sLeftZuoQi ~= nil then
                this.rightAddBtn.gameObject:SetActive(true)
                this.rightAddMapiLabel.text = LocalString.GetString("点击加号添加配对马匹")
            end
        end
    end)

    this.mInviteTick = CTickMgr.Register(tickFunc, 1000, ETickType.Loop)
    invoke(tickFunc)
    CUIManager.CloseUI(CUIResources.SelectMapiWnd)
end
CMapiFanyuWnd.m_OnPlayerDenyYumaInvitation_CS2LuaHook = function (this) 
    this:UnRegisterInviteTick()
    this.rightAddBtn.gameObject:SetActive(true)
    this.rightAddMapiLabel.text = LocalString.GetString("点击加号添加配对马匹")
    CUIManager.CloseUI(CUIResources.SelectMapiWnd)
end
CMapiFanyuWnd.m_OnSendYumaWithFriendInfo_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local setting = ZuoQi_Setting.GetData()
    local itemtid = setting.MapiFanyuNeedItemId
    local itemdata = Item_Item.GetData(itemtid)
    local hasItemCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemtid)

    if CMapiFanyuWnd.sLeftZuoQi ~= nil and CMapiFanyuWnd.sLeftZuoQi.MapiInfo ~= nil then
        this:InitLeftMapi(nil, CMapiFanyuWnd.sLeftZuoQi.MapiInfo)
    end
    if CMapiFanyuWnd.sRightZuoQi ~= nil and CMapiFanyuWnd.sRightZuoQi.MapiInfo ~= nil then
        this:InitRightMapi(nil, CMapiFanyuWnd.sRightZuoQi.MapiInfo)
    end
    if CMapiFanyuWnd.sLeftPid ~= 0 and CMapiFanyuWnd.sRightPid == 0 and CMapiFanyuWnd.sLeftZuoQi ~= nil and CMapiFanyuWnd.sRightZuoQi == nil then
        --如果发起人选择了自己的左边马匹，还没有邀请好友或没有选择右边的马匹
        this.leftAddBtn.gameObject:SetActive(false)
        this.leftAddMapiLabel.gameObject:SetActive(false)

        this.rightAddBtn.gameObject:SetActive(true)
        this.rightAddMapiLabel.text = LocalString.GetString("点击加号添加配对马匹")
        this.rightAddMapiLabel.gameObject:SetActive(true)
    elseif CMapiFanyuWnd.sLeftPid == CMapiFanyuWnd.sRightPid and CMapiFanyuWnd.sLeftPid ~= 0 and CMapiFanyuWnd.sRightPid ~= 0 and CMapiFanyuWnd.sLeftZuoQi ~= nil and CMapiFanyuWnd.sRightZuoQi ~= nil then
        -- 如果是同人育马，而且已经选好了马匹
        this.rightAddBtn.gameObject:SetActive(false)
        this.rightAddMapiLabel.gameObject:SetActive(false)

        local enough = hasItemCount >= CMapiFanyuWnd.sNeedItemCount

        this.needItemDisableSprite.gameObject:SetActive(not enough)
        this.needItemGetLabel.gameObject:SetActive(not enough)
        this.needItemNumLabel.text = System.String.Format("{0}/{1}", hasItemCount, CMapiFanyuWnd.sNeedItemCount)
        this.needItemNumLabel.color = enough and Color.white or Color.red
        this.needItemTexture:LoadMaterial(itemdata.Icon)

        this.needItemTexture.gameObject:SetActive(true)
        this.needItemNumLabel.gameObject:SetActive(true)

        this.needItemDisableSprite.transform.parent.gameObject:SetActive(true)

        CUICommonDef.SetActive(this.BtnStart, enough, true)
    elseif CMapiFanyuWnd.sLeftPid ~= 0 and CMapiFanyuWnd.sRightPid ~= 0 and CMapiFanyuWnd.sLeftZuoQi ~= nil and CMapiFanyuWnd.sRightZuoQi == nil then
        -- 如果发起人选择了马匹，邀请了好友，但是好友还没有选择马匹
        CMapiFanyuWnd.sStarterPlayerId = CMapiFanyuWnd.sLeftPid
        if CMapiFanyuWnd.sLeftPid == CClientMainPlayer.Inst.Id then
            -- 是自己发起的育马，右边的马匹不用显示加
            this:UnRegisterInviteTick()

            this.rightAddBtn.gameObject:SetActive(false)
            this.rightAddMapiLabel.text = LocalString.GetString("等待好友选择配对马匹")
            this.rightAddMapiLabel.gameObject:SetActive(true)
        else
            -- 是别人发起的育马，右边的马匹显示加号
            this.rightAddMapiLabel.gameObject:SetActive(true)
            this.rightAddMapiLabel.text = LocalString.GetString("点击加号添加配对马匹")
            this.rightAddBtn.gameObject:SetActive(true)
        end
    elseif CMapiFanyuWnd.sLeftPid ~= CMapiFanyuWnd.sRightPid and CMapiFanyuWnd.sLeftPid ~= 0 and CMapiFanyuWnd.sRightPid ~= 0 and CMapiFanyuWnd.sLeftZuoQi ~= nil and CMapiFanyuWnd.sRightZuoQi ~= nil then
        -- 如果是与好友一起育马，且好友也选好了马匹
        CMapiFanyuWnd.sStarterPlayerId = CMapiFanyuWnd.sLeftPid
        if CMapiFanyuWnd.sLeftPid == CClientMainPlayer.Inst.Id then
            local enough = hasItemCount >= CMapiFanyuWnd.sNeedItemCount

            this.needItemDisableSprite.gameObject:SetActive(not enough)
            this.needItemGetLabel.gameObject:SetActive(not enough)
            this.needItemNumLabel.text = System.String.Format("{0}/{1}", hasItemCount, CMapiFanyuWnd.sNeedItemCount)
            this.needItemNumLabel.color = enough and Color.white or Color.red
            this.needItemTexture:LoadMaterial(itemdata.Icon)

            this.needItemTexture.gameObject:SetActive(true)
            this.needItemNumLabel.gameObject:SetActive(true)

            this.needItemDisableSprite.transform.parent.gameObject:SetActive(true)

            if not CMapiFanyuWnd.sLeftConfirmed then
                this.BtnStart:SetActive(true)
                CUICommonDef.SetActive(this.BtnStart, enough, true)
            end
            if CMapiFanyuWnd.sLeftConfirmed and not CMapiFanyuWnd.sRightConfirmed then
                this.statusLabel.text = LocalString.GetString("等待对方确认")
                this.statusLabel.gameObject:SetActive(true)

                this.BtnStart:SetActive(false)
            end
        else
            this.rightAddBtn.gameObject:SetActive(false)
            this.rightAddMapiLabel.gameObject:SetActive(false)

            if not CMapiFanyuWnd.sRightConfirmed then
                CUICommonDef.SetActive(this.BtnStart, true, true)
            elseif CMapiFanyuWnd.sRightConfirmed and not CMapiFanyuWnd.sLeftConfirmed then
                this.statusLabel.text = LocalString.GetString("等待对方确认")
                this.statusLabel.gameObject:SetActive(true)

                this.BtnStart:SetActive(false)
            end
        end
    end
end
CMapiFanyuWnd.m_OnSendItem_CS2LuaHook = function (this, itemId) 
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        local setting = ZuoQi_Setting.GetData()
        if item.TemplateId == setting.MapiFanyuNeedItemId then
            local hasItemCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, setting.MapiFanyuNeedItemId)
            local enough = hasItemCount >= CMapiFanyuWnd.sNeedItemCount

            this.needItemDisableSprite.gameObject:SetActive(not enough)
            this.needItemGetLabel.gameObject:SetActive(not enough)
            this.needItemNumLabel.text = System.String.Format("{0}/{1}", hasItemCount, CMapiFanyuWnd.sNeedItemCount)
            this.needItemNumLabel.color = enough and Color.white or Color.red

            CUICommonDef.SetActive(this.BtnStart, enough, true)
        end
    end
end
CMapiFanyuWnd.m_ClearData_CS2LuaHook = function (this) 
    CMapiFanyuWnd.sLeftZuoQi = nil
    CMapiFanyuWnd.sRightZuoQi = nil
    CMapiFanyuWnd.sLeftConfirmed = false
    CMapiFanyuWnd.sRightConfirmed = false
    CMapiFanyuWnd.sLeftPid = 0
    CMapiFanyuWnd.sRightPid = 0
    CMapiFanyuWnd.sStarterPlayerId = 0

    this.mCurMapiInfo = nil
end
CMapiFanyuWnd.m_OnDestroy_CS2LuaHook = function (this) 
    this:ClearData()

    if this.mTick ~= nil then
        invoke(this.mTick)
        this.mTick = nil
    end

    this:UnRegisterInviteTick()

    Gac2Gas.CancelYuma()
end
