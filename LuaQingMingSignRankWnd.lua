local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"

local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaQingMingSignRankWnd = class()

RegistChildComponent(CLuaQingMingSignRankWnd, "SignButton", QnButton)
RegistChildComponent(CLuaQingMingSignRankWnd, "AwardCountLabel", UILabel)
RegistChildComponent(CLuaQingMingSignRankWnd, "RuleView", GameObject)
RegistChildComponent(CLuaQingMingSignRankWnd, "RankView", GameObject)
RegistChildComponent(CLuaQingMingSignRankWnd, "RadioBox", QnRadioBox)
RegistChildComponent(CLuaQingMingSignRankWnd, "RuleTable", UITable)
RegistChildComponent(CLuaQingMingSignRankWnd, "ParagraphTemplate", GameObject)
RegistChildComponent(CLuaQingMingSignRankWnd, "SignLabel", GameObject)
RegistChildComponent(CLuaQingMingSignRankWnd, "DisableLabel", GameObject)

RegistClassMember(CLuaQingMingSignRankWnd,"isRuleInited")

function CLuaQingMingSignRankWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshQingMing2020MatchSate",self,"OnRefreshQingMing2020MatchSate")
    g_ScriptEvent:AddListener("QingMing2020PvpQueryRankResult",self,"OnQingMing2020PvpQueryRankResult")
end

function CLuaQingMingSignRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshQingMing2020MatchSate",self,"OnRefreshQingMing2020MatchSate")
    g_ScriptEvent:RemoveListener("QingMing2020PvpQueryRankResult",self,"OnQingMing2020PvpQueryRankResult")

end

function CLuaQingMingSignRankWnd:Awake()
    self.isRuleInited = false
    self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        if index == 0 then
            self.RuleView:SetActive(true)
            self.RankView:SetActive(false)
        elseif index == 1 then
            self.RuleView:SetActive(false)
            self.RankView:SetActive(true)
            Gac2Gas.QingMing2020PvpQueryRank()
        end
    end)

    UIEventListener.Get(self.SignButton.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
        local playTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(CLuaQingMing2020Mgr.eQingMing2020PvpPlayTimes)
        if playTimes < 1 then
            self:OnSignBtnClick()
        else
            local msg = g_MessageMgr:FormatMessage("QingMing2020_Pvp_Sign_Pay",QingMing2019_QingMing2020Pvp.GetData().SignUpCost)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                self:OnSignBtnClick()
            end), nil, nil, nil, false)
        end
    end)
end

function CLuaQingMingSignRankWnd:Init()
    Gac2Gas.QingMing2020PvpCheckInMatching()
    self.RadioBox:ChangeTo(0)
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(g_MessageMgr:FormatMessage("QingMing2020_Pvp_Rule"))
    Extensions.RemoveAllChildren(self.RuleTable.transform)
    if info ~= nil then
        do
            local i = 0
            while i < info.paragraphs.Count do
                local paragraphGo = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate)
                paragraphGo:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
                i = i + 1
            end
        end
        self.RuleTable:Reposition()
        --this.scrollView:ResetPosition()
    end
    
    local getAwardNum = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eQingMing2020PvpRewardTimes)
    self.AwardCountLabel.text = SafeStringFormat3("(%d/%d)", getAwardNum, QingMing2019_QingMing2020Pvp.GetData().MaxRewardTimesOneDay)

end

function CLuaQingMingSignRankWnd:OnSignBtnClick()
    Gac2Gas.QingMing2020PvpSignUpPlay()
end
-- call back
function CLuaQingMingSignRankWnd:OnRefreshQingMing2020MatchSate(isInMatch)
    self.SignLabel:SetActive(not isInMatch)
    self.DisableLabel:SetActive(isInMatch)
    self.SignButton.Enabled = not isInMatch
end

function CLuaQingMingSignRankWnd:OnQingMing2020PvpQueryRankResult()
    self.RuleView:SetActive(false)
    self.RankView:SetActive(true)
    local rankView = self.RankView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    rankView:Refresh()
end

