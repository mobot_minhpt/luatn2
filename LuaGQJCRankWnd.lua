require("common/common_include")

local UIGrid = import "UIGrid"
local UITable = import "UITable"
local PlayerGradeGroup = import "L10.Game.PlayerGradeGroup"
local GuoQingJiaoChang_Setting = import "L10.Game.GuoQingJiaoChang_Setting"
local CRankData = import "L10.UI.CRankData"
local Constants = import "L10.Game.Constants"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIWidget = import "UIWidget"
local UISimpleTableView = import "L10.UI.UISimpleTableView"

LuaGQJCRankWnd = class()

RegistClassMember(LuaGQJCRankWnd, "TabGrid")
RegistClassMember(LuaGQJCRankWnd, "TabTemplate")
RegistClassMember(LuaGQJCRankWnd, "HintLabel")

RegistClassMember(LuaGQJCRankWnd, "MainPlayerInfo")
RegistClassMember(LuaGQJCRankWnd, "RankTable")
RegistClassMember(LuaGQJCRankWnd, "ItemTemplate")
RegistClassMember(LuaGQJCRankWnd, "TableView")

RegistClassMember(LuaGQJCRankWnd, "NormalTableBgHeight")
RegistClassMember(LuaGQJCRankWnd, "HigherTableBgHeight")
RegistClassMember(LuaGQJCRankWnd, "TableBg")

RegistClassMember(LuaGQJCRankWnd, "TabItems")
RegistClassMember(LuaGQJCRankWnd, "SelectedIndex")
RegistClassMember(LuaGQJCRankWnd, "RankList")


function LuaGQJCRankWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaGQJCRankWnd:InitClassMembers()
	self.TabGrid = self.transform:Find("Anchor/RankGroupView/ScollView"):GetComponent(typeof(UIGrid))
	self.TabTemplate = self.transform:Find("Anchor/RankGroupView/TabTemplate").gameObject
	self.TabTemplate:SetActive(false)
	self.HintLabel = self.transform:Find("Anchor/RankGroupView/HintLabel"):GetComponent(typeof(UILabel))

	self.MainPlayerInfo = self.transform:Find("Anchor/RankList/MainPlayerInfo").gameObject
	self.RankTable = self.transform:Find("Anchor/RankList/TableBody/Table"):GetComponent(typeof(UITable))
	self.ItemTemplate = self.transform:Find("Anchor/RankList/ItemTemplate").gameObject
	self.ItemTemplate:SetActive(false)
	self.TableView = self.transform:Find("Anchor/RankList/TableBody"):GetComponent(typeof(UISimpleTableView))

	self.TableBg = self.transform:Find("Anchor/RankList/TableBg"):GetComponent(typeof(UIWidget))

end

function LuaGQJCRankWnd:InitValues()
	self.NormalTableBgHeight = 686
	self.HigherTableBgHeight = 760
	self.SelectedIndex = -1
	self.TabItems = {}
	self.RankList = {}

	local setting = GuoQingJiaoChang_Setting.GetData()
	local time = string.gsub(setting.ClearRankTime, LocalString.GetString("‘"), "")
	self.HintLabel.text = SafeStringFormat3(LocalString.GetString("[FFFE91]排行榜每日%s重置[-]"), time)

	self:InitTabItems()
end

function LuaGQJCRankWnd:InitTabItems()
	CUICommonDef.ClearTransform(self.TabGrid.transform)
	for i = 0, PlayerGradeGroup.Groups.Length -1, 1 do
		local go = NGUITools.AddChild(self.TabGrid.gameObject, self.TabTemplate)
		self:InitTabItem(go, i)
		go:SetActive(true)
		table.insert(self.TabItems, go)
	end
	self.TabGrid:Reposition()

	-- 默认打开自己所在等级段
	local myGroupIndex = PlayerGradeGroup.GetGroupIndexByGrade(CClientMainPlayer.Inst.Level)
	if myGroupIndex < 0 then myGroupIndex = 0 end
	self:OnTabItemClicked(self.TabItems[myGroupIndex+1], myGroupIndex)

end

function LuaGQJCRankWnd:InitTabItem(go, index) --index [0, 4]
	
	local tabLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	local tabName = PlayerGradeGroup.GetGroupNameByIndex(index)
	tabLabel.text = tabName

	local onTabClicked = function (go)
		self:OnTabItemClicked(go, index)
	end

	local selectableButton = go:GetComponent(typeof(QnSelectableButton))
	selectableButton.OnClick = DelegateFactory.Action_QnButton(onTabClicked)

end

function LuaGQJCRankWnd:OnTabItemClicked(go, index) -- index [0, 4]

	self.SelectedIndex = index
	
	for i = 1, #self.TabItems, 1 do
		local selectableButton = self.TabItems[i]:GetComponent(typeof(QnSelectableButton))
		selectableButton:SetSelected(index == (i-1), false)
	end

	local rankGroup = GuoQingJiaoChang_Setting.GetData().GroupRankId
	local rankId = rankGroup[index][1]
	Gac2Gas.QueryRank(rankId)
end

function LuaGQJCRankWnd:OnEnable()
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaGQJCRankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaGQJCRankWnd:OnRankDataReady(args)

	-- 是否要显示自己的信息
	local myGroupIndex = PlayerGradeGroup.GetGroupIndexByGrade(CClientMainPlayer.Inst.Level)
	if myGroupIndex == self.SelectedIndex then
		self.MainPlayerInfo:SetActive(true)

		local myInfo = CRankData.Inst.MainPlayerRankInfo
		self:InitMyInfo(myInfo)

		if self.TableBg.height ~= self.NormalTableBgHeight then
			self.TableBg.height = self.NormalTableBgHeight
			self.TableView.scrollView.panel:ResetAndUpdateAnchors()
		end

	else
		self.MainPlayerInfo:SetActive(false)
		if self.TableBg.height ~= self.HigherTableBgHeight then
			self.TableBg.height = self.HigherTableBgHeight
			self.TableView.scrollView.panel:ResetAndUpdateAnchors()
		end
	end

	
	self.RankList = CRankData.Inst.RankList
	CUICommonDef.ClearTransform(self.RankTable.transform)
	for i = 0, self.RankList.Count-1, 1 do
		local go = NGUITools.AddChild(self.RankTable.gameObject, self.ItemTemplate)
		self:InitRankItem(go, self.RankList[i], i)
		go:SetActive(true)
	end
	self.RankTable:Reposition()
end

function LuaGQJCRankWnd:InitRankItem(go, info, index)

	local RankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local RankImage = go.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
	local PlayerNameLabel = go.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local ResultLabel = go.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))
	local AverageKillLabel = go.transform:Find("AverageKillLabel"):GetComponent(typeof(UILabel))
	local AverageFinishLabel = go.transform:Find("AverageFinishLabel"):GetComponent(typeof(UILabel))
	local ScoreLabel = go.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
	local BG = go.transform:GetComponent(typeof(UISprite))

	local bgIdx = index - math.floor(index / 2) * 2

	if bgIdx == 1 then
		BG.spriteName = Constants.OddBgSpirite
	else
		BG.spriteName = Constants.EvenBgSprite
	end

	if info.Rank > 0 and info.Rank <= 3 then
    	RankImage.gameObject:SetActive(true)
    	if info.Rank == 1 then
    		RankImage.spriteName = Constants.RankFirstSpriteName
    	elseif info.Rank == 2 then
    		RankImage.spriteName = Constants.RankSecondSpriteName
    	elseif info.Rank == 3 then
    		RankImage.spriteName = Constants.RankThirdSpriteName
    	end
    else
    	RankImage.gameObject:SetActive(false)
    end

    ScoreLabel.text = tostring(info.Value)

    local list = MsgPackImpl.unpack(info.extraData)
    if list.Count < 6 then return end
    local winNum = list[0]
    local tieNum = list[1]
    local loseNum = list[2]
    local killNum = list[3]
    local reliveNum = list[4]
    local clearNum = list[5]
    PlayerNameLabel.text = info.Name
    RankLabel.text = tostring(info.Rank)
    ResultLabel.text = SafeStringFormat3("%s-%s-%s", tostring(winNum), tostring(tieNum), tostring(loseNum))
    local totalNum = winNum + tieNum + loseNum
    local averageKillNum = 0
    if totalNum ~= 0 then
    	averageKillNum = killNum / totalNum
    end
    AverageKillLabel.text = tostring(SafeStringFormat3("%.1f", averageKillNum))
    AverageFinishLabel.text = tostring(clearNum)

end

function LuaGQJCRankWnd:InitMyInfo(myInfo)

	local RankLabel = self.MainPlayerInfo.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local RankImage = self.MainPlayerInfo.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
	local PlayerNameLabel = self.MainPlayerInfo.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local ResultLabel = self.MainPlayerInfo.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))
	local AverageKillLabel = self.MainPlayerInfo.transform:Find("AverageKillLabel"):GetComponent(typeof(UILabel))
	local AverageFinishLabel = self.MainPlayerInfo.transform:Find("AverageFinishLabel"):GetComponent(typeof(UILabel))
	local ScoreLabel = self.MainPlayerInfo.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

	PlayerNameLabel.text = CClientMainPlayer.Inst.Name
	if myInfo.Rank > 0 then
        RankLabel.text = tostring(myInfo.Rank)
    else
        RankLabel.text=LocalString.GetString("未上榜")
        ScoreLabel.text = "0"
        ResultLabel.text = "0-0-0"
        AverageKillLabel.text = "0"
    	AverageFinishLabel.text = "0"
    	RankImage.gameObject:SetActive(false)
    	return
    end
    if myInfo.Rank > 0 and myInfo.Rank <= 3 then
    	RankImage.gameObject:SetActive(true)
    	if myInfo.Rank == 1 then
    		RankImage.spriteName = Constants.RankFirstSpriteName
    	elseif myInfo.Rank == 2 then
    		RankImage.spriteName = Constants.RankSecondSpriteName
    	elseif myInfo.Rank == 3 then
    		RankImage.spriteName = Constants.RankThirdSpriteName
    	end
    else
    	RankImage.gameObject:SetActive(false)
    end

    ScoreLabel.text = tostring(myInfo.Value)
    local list = MsgPackImpl.unpack(myInfo.extraData)
    local winNum = list[0]
    local tieNum = list[1]
    local loseNum = list[2]
    local killNum = list[3]
    local reliveNum = list[4]
    local clearNum = list[5]
    ResultLabel.text = SafeStringFormat3("%s-%s-%s", tostring(winNum), tostring(tieNum), tostring(loseNum))
    local totalNum = winNum + tieNum + loseNum
    local averageKillNum = 0
    if totalNum ~= 0 then
    	averageKillNum = killNum / totalNum
    end
    AverageKillLabel.text = tostring(SafeStringFormat3("%.1f", averageKillNum))
    AverageFinishLabel.text = tostring(clearNum)

end

return LuaGQJCRankWnd
