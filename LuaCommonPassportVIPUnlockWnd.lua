local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local CPayMgr                = import "L10.Game.CPayMgr"
local CShopMallMgr           = import "L10.UI.CShopMallMgr"

LuaCommonPassportVIPUnlockWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCommonPassportVIPUnlockWnd, "vip1UnlockButton")
RegistClassMember(LuaCommonPassportVIPUnlockWnd, "vip2UnlockButton")
RegistClassMember(LuaCommonPassportVIPUnlockWnd, "itemTemplate")

RegistClassMember(LuaCommonPassportVIPUnlockWnd, "passId")
RegistClassMember(LuaCommonPassportVIPUnlockWnd, "hasVIP2")

RegistClassMember(LuaCommonPassportVIPUnlockWnd, "vip1Unlock")
RegistClassMember(LuaCommonPassportVIPUnlockWnd, "vip2Unlock")

function LuaCommonPassportVIPUnlockWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.vip1Unlock = false
    self.vip2Unlock = false
end

function LuaCommonPassportVIPUnlockWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
    g_ScriptEvent:AddListener("CommonPassortUnlockVipSuccess", self, "OnCommonPassortUnlockVipSuccess")
end

function LuaCommonPassportVIPUnlockWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
    g_ScriptEvent:RemoveListener("CommonPassortUnlockVipSuccess", self, "OnCommonPassortUnlockVipSuccess")
end

function LuaCommonPassportVIPUnlockWnd:OnUpdatePassDataWithId(id)
    if self.passId == id then
        self:UpdateUnlockButton()
    end
end

function LuaCommonPassportVIPUnlockWnd:OnCommonPassortUnlockVipSuccess(id, vip1Unlock, vip2Unlock)
    if self.passId == id then
        if vip1Unlock then self.vip1Unlock = true end
        if vip2Unlock then self.vip2Unlock = true end
    end
end

function LuaCommonPassportVIPUnlockWnd:Init()
    self.passId = LuaCommonPassportMgr.passId
    self.hasVIP2 = LuaCommonPassportMgr:HasVIP2(self.passId)
    local clientSettingData = Pass_ClientSetting.GetData(self.passId)
    local settingData = Pass_Setting.GetData(self.passId)
    local unlockType = settingData.UnlockType
    local names = clientSettingData.BattlePassNames
    local originVipCost = clientSettingData.OriginVipCost
    local vipCost = settingData.VipCost
    local progressRewards = LuaCommonPassportMgr:ParseProgressRewards(self.passId)

    local anchor = self.transform:Find("Anchor")
    self.itemTemplate = anchor:Find("ItemTemplate").gameObject
    self.itemTemplate:SetActive(false)

    local style1 = anchor:Find("Style1")
    local style2 = anchor:Find("Style2")
    style1.gameObject:SetActive(self.hasVIP2)
    style2.gameObject:SetActive(not self.hasVIP2)

    local vip1Root = self.hasVIP2 and style1:Find("VIP1") or style2:Find("VIP1")
    self:InitOneVip(vip1Root, names[1], originVipCost[0], unlockType, vipCost[0], self:GetLevelUp(progressRewards[1].progress), clientSettingData.Vip1ShowAward, progressRewards[1].mailId, progressRewards[1].rate)
    self.vip1UnlockButton = vip1Root:Find("UnlockButton"):GetComponent(typeof(QnButton))
    UIEventListener.Get(self.vip1UnlockButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnVIP1UnlockButtonClick()
    end)
    if self.hasVIP2 then
        local vip2Root = style1:Find("VIP2")
        self:InitOneVip(vip2Root, names[2], originVipCost[1], unlockType, vipCost[1], self:GetLevelUp(progressRewards[2].progress), clientSettingData.Vip2ShowAward, progressRewards[2].mailId, progressRewards[2].rate)
        self.vip2UnlockButton = vip2Root:Find("UnlockButton"):GetComponent(typeof(QnButton))
        UIEventListener.Get(self.vip2UnlockButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnVIP2UnlockButtonClick()
        end)
    end

    anchor:Find("QnCostAndOwnMoney").gameObject:SetActive(unlockType == 2)
    self:UpdateUnlockButton()
end

-- 获取将升多少级
function LuaCommonPassportVIPUnlockWnd:GetLevelUp(addProgress)
    local oldLevel = LuaCommonPassportMgr:GetLevel(self.passId)
    local rewardDesignData = LuaCommonPassportMgr:GetRewardDesignData(self.passId)
    local newProgress = LuaCommonPassportMgr:GetProgress(self.passId) + addProgress

    -- 计算新等级
    local count = #rewardDesignData
    local newLevel
    for i = 1, count do
        if newProgress < rewardDesignData[i].totalProgress then
            local lastLevel = i == 1 and 0 or rewardDesignData[i - 1].level
            local lastProgress = i == 1 and 0 or rewardDesignData[i - 1].totalProgress
            newLevel = lastLevel + math.floor((newProgress - lastProgress) / rewardDesignData[i].progress)
            break
        end

        if i == count then
            newLevel = rewardDesignData[i].level + math.floor((newProgress - rewardDesignData[i].totalProgress) / rewardDesignData[i].progress)
            break
        end
    end
    return newLevel - oldLevel
end

function LuaCommonPassportVIPUnlockWnd:InitOneVip(root, name, originCost, unlockType, vipCost, levelUp, vipShowAward, mailId, rate)
    root:Find("Title"):GetComponent(typeof(UILabel)).text = name
    local originCostStr = SafeStringFormat3(LocalString.GetString("原价%d%s"), originCost, unlockType == 1 and LocalString.GetString("元") or "")
    root:Find("Price"):GetComponent(typeof(UILabel)).text = originCostStr
    root:Find("Price/Sprite").gameObject:SetActive(unlockType == 2)

    local detailTable = root:Find("Detail/ScrollView/Table")
    detailTable:Find("UnlockNow/Label1"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("解锁%s"), name)
    detailTable:Find("UnlockNow/Label2"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("立即提升%d级"), levelUp)
    local upgradeAwardGrid = detailTable:Find("UpgradeAward/Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(upgradeAwardGrid.transform)
    for itemId, count in string.gmatch(vipShowAward, "(%d+),(%d+)") do
        local child = NGUITools.AddChild(upgradeAwardGrid.gameObject, self.itemTemplate)
        child.gameObject:SetActive(true)
        child.transform:GetComponent(typeof(CQnReturnAwardTemplate)):Init(Item_Item.GetData(tonumber(itemId)), tonumber(count))
    end
    upgradeAwardGrid:Reposition()

    local additionAward = detailTable:Find("AdditionAward")
    if additionAward then
        if mailId and mailId > 0 then
            local items = Mail_Mail.GetData(mailId).Items
            local additionAwardGrid = additionAward:Find("Grid"):GetComponent(typeof(UIGrid))
            Extensions.RemoveAllChildren(additionAwardGrid.transform)
            for itemId, count, bind in string.gmatch(items, "(%d+),(%d+),(%d+)") do
                local child = NGUITools.AddChild(additionAwardGrid.gameObject, self.itemTemplate)
                child.gameObject:SetActive(true)
                child.transform:GetComponent(typeof(CQnReturnAwardTemplate)):Init(Item_Item.GetData(tonumber(itemId)), tonumber(count))
            end
            additionAwardGrid:Reposition()
        else
            additionAward.gameObject:SetActive(false)
        end
    end

    local uiTable = detailTable:GetComponent(typeof(UITable))
    if uiTable then uiTable:Reposition() end

    root:Find("UnlockButton/Locked/Sprite").gameObject:SetActive(unlockType == 2)
    local vipCostStr = SafeStringFormat3("%d%s", vipCost, unlockType == 1 and LocalString.GetString("元") or "")
    root:Find("UnlockButton"):GetComponent(typeof(QnButton)).Text = vipCostStr
    local addition = root:Find("Addition")
    if addition then
        if rate > 0 then
            addition.gameObject:SetActive(true)
            addition:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("+%d%%任务经验获得"), math.floor(rate * 100))
            addition:Find("Sprite"):GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
        else
            addition.gameObject:SetActive(false)
        end
    end
end


function LuaCommonPassportVIPUnlockWnd:UpdateUnlockButton()
    local isVIP1Open = LuaCommonPassportMgr:GetIsVIP1Open(self.passId)
    self.vip1UnlockButton.Enabled = not isVIP1Open
    self.vip1UnlockButton.transform:Find("Locked").gameObject:SetActive(not isVIP1Open)
    self.vip1UnlockButton.transform:Find("Unlocked").gameObject:SetActive(isVIP1Open)
    if self.hasVIP2 then
        local isVIP2Open = LuaCommonPassportMgr:GetIsVIP2Open(self.passId)
        self.vip2UnlockButton.Enabled = not isVIP2Open
        self.vip2UnlockButton.transform:Find("Locked").gameObject:SetActive(not isVIP2Open)
        self.vip2UnlockButton.transform:Find("Unlocked").gameObject:SetActive(isVIP2Open)
    end
end

function LuaCommonPassportVIPUnlockWnd:GetPID(vip)
    local tbl = {}
    local vipPID = Pass_Setting.GetData(self.passId).VipPID
    for pid in string.gmatch(vipPID, "([^;]+)") do
        table.insert(tbl, pid)
    end
    return tbl[vip]
end

-- 使用灵玉解锁Vip
function LuaCommonPassportVIPUnlockWnd:UnlockVipByJade(type)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end

    local needJade = Pass_Setting.GetData(self.passId).VipCost[type - 1]
    if mainPlayer.Jade < needJade then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function ()
            CShopMallMgr.ShowChargeWnd()
        end, nil, LocalString.GetString("前往"), nil, false)
    else
        local name = Pass_ClientSetting.GetData(self.passId).BattlePassNames[type]
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("COMMON_PASSPORT_VIP_UNLOCK_CONFIRM", needJade, name), function ()
            Gac2Gas.RequestBuyPassVipByJade(self.passId, type)
        end, nil, LocalString.GetString("购买"), nil, false)
    end
end

function LuaCommonPassportVIPUnlockWnd:OnDestroy()
    if self.vip1Unlock or self.vip2Unlock then
        g_ScriptEvent:BroadcastInLua("CommonPassportPlayVipUnlockVfx", self.passId, self.vip1Unlock, self.vip2Unlock)
    end
end

--@region UIEvent

function LuaCommonPassportVIPUnlockWnd:OnVIP1UnlockButtonClick()
    local unlockType = Pass_Setting.GetData(self.passId).UnlockType
    if unlockType == 1 then
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(self:GetPID(1)), 0)
    elseif unlockType == 2 then
        self:UnlockVipByJade(1)
    end
end

function LuaCommonPassportVIPUnlockWnd:OnVIP2UnlockButtonClick()
    local unlockType = Pass_Setting.GetData(self.passId).UnlockType
    if unlockType == 1 then
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(self:GetPID(2)), 0)
    elseif unlockType == 2 then
        self:UnlockVipByJade(2)
    end
end

--@endregion UIEvent
