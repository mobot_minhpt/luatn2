local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaChunJie2023ShengYanResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023ShengYanResultWnd, "RankBtn", GameObject)
RegistChildComponent(LuaChunJie2023ShengYanResultWnd, "PlayAgainBtn", GameObject)

RegistChildComponent(LuaChunJie2023ShengYanResultWnd, "RankLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShengYanResultWnd, "ScoreLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShengYanResultWnd, "RewardGrid", UIGrid)
RegistChildComponent(LuaChunJie2023ShengYanResultWnd, "FullLabel", UILabel)
--@endregion RegistChildComponent end

function LuaChunJie2023ShengYanResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.RankBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRankBtnClick()
    end)
    UIEventListener.Get(self.PlayAgainBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnPlayAgainBtnClick()
    end)
end

function LuaChunJie2023ShengYanResultWnd:Init()
    self.RankLabel.text = tostring(LuaChunJie2023Mgr.TTSY_FinalRank)
    self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("%d分"), LuaChunJie2023Mgr.TTSY_FinalScore)
    local isNoReward = LuaChunJie2023Mgr.TTSY_Rewards == nil or #LuaChunJie2023Mgr.TTSY_Rewards == 0
    self.RewardGrid.transform.parent.gameObject:SetActive(not isNoReward)
    self.FullLabel.gameObject:SetActive(isNoReward)
    if not isNoReward then 
        -- 奖励
        for i = 0, self.RewardGrid.transform.childCount - 1 do
            local item = self.RewardGrid.transform:GetChild(i)
            if i < #LuaChunJie2023Mgr.TTSY_Rewards then
                item:GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(LuaChunJie2023Mgr.TTSY_Rewards[i+1]), 1)
                item.gameObject:SetActive(true)
            else
                item.gameObject:SetActive(false)
            end
        end
        self.RewardGrid:Reposition()
    else
        self:ShowNoRewardReason()
    end
end

function LuaChunJie2023ShengYanResultWnd:ShowNoRewardReason()
    local setData = ChunJie_TTSYSetting.GetData()
    local rank = LuaChunJie2023Mgr.TTSY_FinalRank
    local score = LuaChunJie2023Mgr.TTSY_FinalScore
    local firstWin = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_ChampionTimes)
    local reward1 = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_DailyRewardTimes_1)
    local reward2 = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_DailyRewardTimes_2)
    local reward3 = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_DailyRewardTimes_3)
    local isFull = firstWin ~= 0 and reward1 ~= 0 and reward2 ~= 0 and reward3 ~= 0
    local isRankLimit = false
    if rank >= 2 and rank <= 5 then
        isRankLimit = firstWin == 0 or reward1 == 0
    elseif rank >= 6 then
        isRankLimit = firstWin == 0 or reward1 == 0 or reward2 == 0
    end
    local isScoreLimit = score < setData.RewardScoreLimit
    if not isFull and isRankLimit then
        self.FullLabel.text = setData.RankLimitString
    elseif not isFull and isScoreLimit then
        self.FullLabel.text = setData.ScoreLimitString
    else
        self.FullLabel.text = setData.RewardFullString
    end
end

--@region UIEvent
function LuaChunJie2023ShengYanResultWnd:OnRankBtnClick()
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShengYanRankWnd)
end
function LuaChunJie2023ShengYanResultWnd:OnPlayAgainBtnClick()
	CUIManager.CloseUI(CLuaUIResources.ChunJie2023ShengYanResultWnd)
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShengYanEnterWnd)
end

--@endregion UIEvent

