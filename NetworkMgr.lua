local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaNetworkMgr = class()
LuaNetworkMgr.m_HeartBeatTick = nil
LuaNetworkMgr.m_HeartBeatDelayTime = 0
LuaNetworkMgr.m_MainPlayerInValidCount = 0

function LuaNetworkMgr:OnMainPlayerCreated()
	LuaNetworkMgr.m_HeartBeatDelayTime = 0
	self:StartRTTTick()
end

function LuaNetworkMgr:StartRTTTick()
	if not LuaNetworkMgr.m_HeartBeatTick then
		LuaNetworkMgr.m_HeartBeatTick = RegisterTick(function()
			-- gac2gas内部执行时通过CheckBeforeSendRpc会判空处理，根据洪磊的建议这里直接发就行
			self:SendRttStartRPC()
		end, 2000)
		self:SendRttStartRPC()
	end
end

function LuaNetworkMgr:SendRttStartRPC()
	if not CLoginMgr.Inst:IsInGame() then --不在游戏里面不发rpc
		self.m_MainPlayerInValidCount = 0
		return
	end
	if CClientMainPlayer.Inst == nil then
		self.m_MainPlayerInValidCount = self.m_MainPlayerInValidCount + 1
	else
		self.m_MainPlayerInValidCount = 0
	end
	if self.m_MainPlayerInValidCount == 3 then --连续三次player不存在
		Gac2Gas.TestGatewayRttStart(self:GetCurrentMilliSecond(), true)
		self.m_MainPlayerInValidCount = 0
	else
		Gac2Gas.TestGatewayRttStart(self:GetCurrentMilliSecond(), false)
	end
end

function LuaNetworkMgr:GetCurrentMilliSecond()
	return CClientMainPlayer.GetCurrentMilliSecond()
end

function LuaNetworkMgr:ResetDelayTime()
	self.m_HeartBeatDelayTime = 0
	g_ScriptEvent:BroadcastInLua("OnRTTReturn")
end

-----------------
--RPC
-----------------

function Gas2Gac.TestGatewayRttEnd(time)
	LuaNetworkMgr.m_HeartBeatDelayTime = CClientMainPlayer.GetCurrentMilliSecond() - time
	g_ScriptEvent:BroadcastInLua("OnRTTReturn")
end
