local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"

LuaGSNZPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGSNZPlayWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaGSNZPlayWnd, "InfoProgress", "InfoProgress", UISlider)
RegistChildComponent(LuaGSNZPlayWnd, "InfoText", "InfoText", UILabel)
RegistChildComponent(LuaGSNZPlayWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaGSNZPlayWnd, "Side1", "Side1", GameObject)
RegistChildComponent(LuaGSNZPlayWnd, "Side2", "Side2", GameObject)
RegistChildComponent(LuaGSNZPlayWnd, "Warning", "Warning", GameObject)

--@endregion RegistChildComponent end

function LuaGSNZPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    UIEventListener.Get(self.ExpandButton.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnExpandButtonClick()
        end
    )

    --@endregion EventBind end
end

function LuaGSNZPlayWnd:Init()
    self.Side1:SetActive(ZhongYuanJie2022Mgr.GSNZType == 1)
    self.Side2:SetActive(ZhongYuanJie2022Mgr.GSNZType ~= 1)
    self:OnSyncProcessBar()
end

function LuaGSNZPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncGSNZProcess", self, "OnSyncProcessBar")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaGSNZPlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncGSNZProcess", self, "OnSyncProcessBar")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaGSNZPlayWnd:OnSyncProcessBar()
    local value = ZhongYuanJie2022Mgr.GSNZProcess
    local msg = ""
    if ZhongYuanJie2022Mgr.GSNZType == 1 then
        msg = g_MessageMgr:Format(LocalString.GetString("古树凝珠%s%，助力促成!"), value)
    else
        msg = g_MessageMgr:Format(LocalString.GetString("古树凝珠%s%，尽力阻止!"), value)
    end
    self.InfoText.text = msg

    self.InfoProgress.value = value / 100

    local setting = ZhongYuanJie_Setting2022.GetData()
    local min = setting.TreeMinThreshold
    local max = setting.TreeMaxThreshold
    if value <= min or value >= max then
        if not self.Warning.activeInHierarchy then
            self.Warning:SetActive(true)
        end
    else
        if self.Warning.activeInHierarchy then
            self.Warning:SetActive(false)
        end
    end
end

function LuaGSNZPlayWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
    self:SetUIActive(true)
end

function LuaGSNZPlayWnd:SetUIActive(state)
    self.Content:SetActive(state)
end

--@region UIEvent

function LuaGSNZPlayWnd:OnExpandButtonClick()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    self:SetUIActive(false)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent
