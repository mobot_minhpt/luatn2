local GameObject                = import "UnityEngine.GameObject"
local Vector4                   = import "UnityEngine.Vector4"
local Color                     = import "UnityEngine.Color"
local CUIManager                = import "L10.UI.CUIManager"
local CUICommonDef              = import "L10.UI.CUICommonDef"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local QnTableView               = import "L10.UI.QnTableView"
local QnButton                  = import "L10.UI.QnButton"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local CUICenterOnChild          = import "L10.UI.CUICenterOnChild"
local UILabel                   = import "UILabel"
local UISprite                  = import "UISprite"
local UIGrid                    = import "UIGrid"
local LuaUtils                  = import "LuaUtils"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
local LuaTweenUtils             = import "LuaTweenUtils"
local NGUIMath                  = import "NGUIMath"
local PathMode                  = import "DG.Tweening.PathMode"
local PathType                  = import "DG.Tweening.PathType"
local Ease                      = import "DG.Tweening.Ease"
local CTickMgr                  = import "L10.Engine.CTickMgr"
local ETickType                 = import "L10.Engine.ETickType"

--天成酒壶活动主界面奖励页签
CLuaTcjhMainRewardTab = class()

RegistChildComponent(CLuaTcjhMainRewardTab, "NextLabel",		UILabel)
RegistChildComponent(CLuaTcjhMainRewardTab, "TimeLb",		    UILabel)
RegistChildComponent(CLuaTcjhMainRewardTab, "ItemCell1",		GameObject)
RegistChildComponent(CLuaTcjhMainRewardTab, "ItemCell2",		GameObject)
RegistChildComponent(CLuaTcjhMainRewardTab, "ItemCell3",		GameObject)
RegistChildComponent(CLuaTcjhMainRewardTab, "AdvLockImg",		GameObject)
RegistChildComponent(CLuaTcjhMainRewardTab, "AdvPassImg",		GameObject)
RegistChildComponent(CLuaTcjhMainRewardTab, "OneKeyBtn",		GameObject)
RegistChildComponent(CLuaTcjhMainRewardTab, "ShopBtn",		    GameObject)
RegistChildComponent(CLuaTcjhMainRewardTab, "QnTableView1",		QnTableView)
RegistChildComponent(CLuaTcjhMainRewardTab, "Grid",             UIGrid)
RegistChildComponent(CLuaTcjhMainRewardTab, "Indicators",       UIGrid)
RegistChildComponent(CLuaTcjhMainRewardTab, "Sprite3",          GameObject)


RegistClassMember(CLuaTcjhMainRewardTab,  "CurImpLv")
RegistClassMember(CLuaTcjhMainRewardTab,  "SelectedItem")
RegistClassMember(CLuaTcjhMainRewardTab,  "RideItems")
RegistClassMember(CLuaTcjhMainRewardTab,  "Isdirty")
RegistClassMember(CLuaTcjhMainRewardTab,  "m_RollBannerTick")
RegistClassMember(CLuaTcjhMainRewardTab,  "m_RollBannerIndex")

function CLuaTcjhMainRewardTab:Awake()
    self.CurImpLv = -1
    self.SelectedItem = nil
    self.RideItems = 21020986--{21030898,21030880,21005306,21005448,21005482} #144712
    UIEventListener.Get(self.OneKeyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOneKeyBtnClick()
    end)

    UIEventListener.Get(self.ShopBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShopBtnClick()
    end)

    UIEventListener.Get(self.Sprite3).onClick = DelegateFactory.VoidDelegate(function(go)
        local passtype = LuaHanJiaMgr.TcjhMainData.PassType
        if passtype == 0 then
            CUIManager.ShowUI(CLuaUIResources.TcjhBuyAdvPassWnd)
        end
    end)

    self.QnTableView1.m_ScrollView.horizontalScrollBar.OnChangeValue = DelegateFactory.Action_float(function(value)
        self:OnScroll(value)
    end)

    

    local centerOnChild = self.Grid:GetComponent(typeof(CUICenterOnChild))
    centerOnChild.onCenter = LuaUtils.OnCenterCallback(function(go)
        local index = go.transform:GetSiblingIndex()
        self.m_RollBannerIndex = index
        for i = 1,self.Indicators.transform.childCount do
            local sprite = self.Indicators.transform:GetChild(i-1):GetComponent(typeof(UISprite))
            if i == index + 1 then
                sprite.color = Color.white
            else
                sprite.color = Color(0.3,0.5,0.8)
            end
        end
    end)
    centerOnChild:CenterOnInstant(self.Grid:GetChild(0))

    for i = 0, 4 do
        local go = self.Grid:GetChild(i)
        local ridelabel = LuaGameObject.GetChildNoGC(go.transform,"RideLabel").label
        local ridedeslabel = LuaGameObject.GetChildNoGC(go.transform,"RideDesLabel").label

        local cfgid = self.RideItems
        local cfg = Item_Item.GetData(cfgid)
        if cfg then
            ridelabel.text = cfg.Name
            local fmt = LocalString.GetString("[f3c484]解锁翠琼酒壶并完成任务即可获得[-][d95c6c][%s][-]")
            local text = nil
            ridedeslabel.text = SafeStringFormat3(fmt,cfg.Name)

            UIEventListener.Get(ridedeslabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(cfgid, false, nil, AlignType.Default, 0, 0, 0, 0) 
            end)

        else
            ridelabel.text = ""
            ridedeslabel.text = ""
        end
    end

    g_ScriptEvent:AddListener("OnTcjhDataChange", self, "OnTcjhDataChange")
    self:OnTcjhDataChange()
end

function CLuaTcjhMainRewardTab:OnDestroy()
    g_ScriptEvent:RemoveListener("OnTcjhDataChange", self, "OnTcjhDataChange")
end

function CLuaTcjhMainRewardTab:OnScroll(value)

    local data = LuaHanJiaMgr.TcjhMainData

    local vdelta = 5.2
    local impcount = 5
    local index = math.ceil((data.CfgMaxLv - vdelta) * value+vdelta)
    local implv = (math.floor((index - 1) / impcount) + 1) * impcount 

    if implv == self.CurImpLv then return end
    self.CurImpLv = implv

    self:RefreshImpItems()
    
end

function CLuaTcjhMainRewardTab:RefreshImpItems()
    local implv = self.CurImpLv
    local data = LuaHanJiaMgr.TcjhMainData
    local nmlitems,advitems = self:GetLvItems(implv)

    local cell1 = self.ItemCell1.transform
    local cell2 = self.ItemCell2.transform
    local cell3 = self.ItemCell3.transform

    if data.RewardNmlLv < implv or (data.PassType ~= 0 and data.RewardAdvLv < implv) then
        self.NextLabel.text = SafeStringFormat3(LocalString.GetString("%d级可领"),implv)
    else
        self.NextLabel.text = LocalString.GetString("已领取")
    end

    local rewardlv1 = data.RewardNmlLv
    local rewardlv2 = data.RewardAdvLv
    if LuaHanJiaMgr.TcjhMainData.PassType == 0 then 
        rewardlv2 = -1
    end
    if nmlitems == nil or implv > data.MaxLv then
        self:FillItemCell(cell1,nil,implv,rewardlv1,false)
    else
        self:FillItemCell(cell1,nmlitems[1],implv,rewardlv1,false)
    end

    if advitems == nil then 
        self:FillItemCell(cell2,nil,implv,rewardlv2,false)
        self:FillItemCell(cell3,nil,implv,rewardlv2,false)
    else
        self:FillItemCell(cell2,advitems[1],implv,rewardlv2,false)
        self:FillItemCell(cell3,advitems[2],implv,rewardlv2,false)
    end
end

function CLuaTcjhMainRewardTab:OnEnable()
    if self.Isdirty then
        self:OnTcjhDataChange()
    end
    local centerOnChild = self.Grid:GetComponent(typeof(CUICenterOnChild))
    self:AutoRollBanner(self.Grid,centerOnChild)
end

function CLuaTcjhMainRewardTab:OnDisable()
    if self.m_RollBannerTick then
        UnRegisterTick(self.m_RollBannerTick)
        self.m_RollBannerTick = nil
    end

    if self.m_DelayTick ~= nil then
        invoke(self.m_DelayTick)
    end
end

function CLuaTcjhMainRewardTab:AutoRollBanner(table,centerOnChild)
    if self.m_RollBannerTick then
        UnRegisterTick(self.m_RollBannerTick)
        self.m_RollBannerTick = nil
    end
    self.m_RollBannerTick = RegisterTick(function ()
        centerOnChild:CenterOnInstant(self.Grid:GetChild((self.m_RollBannerIndex + 1) % 5))
    end,3000)
end

function CLuaTcjhMainRewardTab:OnTcjhDataChange()
    if not self.gameObject.activeSelf then
        self. Isdirty = true
        return 
    end
    self. Isdirty = false
    local data = LuaHanJiaMgr.TcjhMainData

    local lv = data.Lv
    local lvmax = data.CfgMaxLv

    local passtype = data.PassType --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版
    self.AdvLockImg:SetActive(passtype == 0)
    CUICommonDef.SetGrey(self.AdvPassImg,passtype == 0)

    local setting = HanJia2021_TianChengSetting.GetData()
    local year, month, day, hour, min = string.match(setting.BeginTime, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")
    local  year2, month2, day2, hour2, min2 =string.match(setting.FinishTime, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")
    self.TimeLb.text = SafeStringFormat3(LocalString.GetString("活动时间：%d年%d月%d日-%d月%d日"),year,month,day,month2,day2)

    local initfunc = function(item,index)
        self:FillContentItem(item,index)
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(lvmax,initfunc)
    self.QnTableView1:ReloadData(true, true)
    self.QnTableView1.m_ScrollView:InvalidateBounds()

    if self.m_DelayTick ~= nil then
        invoke(self.m_DelayTick)
    end
    self.m_DelayTick = CTickMgr.RegisterUpdate(DelegateFactory.Action_uint(function (delta) 
        self.QnTableView1:ScrollToRow(lv)
    end), ETickType.Once)

    self:RefreshImpItems()

    local havereward = lv > data.RewardNmlLv or (passtype ~= 0 and lv > data.RewardAdvLv)
    local btn = self.OneKeyBtn:GetComponent(typeof(QnButton))
    btn.Enabled = havereward
end

function CLuaTcjhMainRewardTab:FillContentItem(item,index)
    local lv = index + 1



    local curlv = LuaHanJiaMgr.TcjhMainData.Lv

    local lvlbstr = "Lv1/Label"
    if lv > curlv then 
        lvlbstr = "Lv2/Label"
    end
    local lvlb = item.transform:Find(lvlbstr):GetComponent(typeof(UILabel))
    lvlb.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(lv))
    
    local selectgo = LuaGameObject.GetChildNoGC(item.transform,"SelectImg").gameObject
    selectgo:SetActive(curlv == lv)

    local lvbg1 = LuaGameObject.GetChildNoGC(item.transform,"Lv1").gameObject
    local lvbg2 = LuaGameObject.GetChildNoGC(item.transform,"Lv2").gameObject
    lvbg1:SetActive(lv <= curlv)
    lvbg2:SetActive(lv > curlv)

    local nmlitems,advitems = self:GetLvItems(lv)
    if lv <= LuaHanJiaMgr.TcjhMainData.MaxLv then
        self:FillNmlItems(item,nmlitems,lv)
    end
    self:FillAdvItems(item,advitems,lv)
end

function CLuaTcjhMainRewardTab:FillAdvItems(item,advitems,lv)
    local cell1 = item.transform:Find("Item2/ItemCell1")
    local cell2 = item.transform:Find("Item2/ItemCell2")
    local rewardlv = LuaHanJiaMgr.TcjhMainData.RewardAdvLv
    if LuaHanJiaMgr.TcjhMainData.PassType == 0 then 
        rewardlv = -1
    end
    if advitems == nil then 
        self:FillItemCell(cell1,nil,lv,rewardlv,true)
        self:FillItemCell(cell2,nil,lv,rewardlv,true)
    else
        self:FillItemCell(cell1,advitems[1],lv,rewardlv,true)
        self:FillItemCell(cell2,advitems[2],lv,rewardlv,true)
    end
end

function CLuaTcjhMainRewardTab:FillNmlItems(item,nmlitems,lv)
    local cell = item.transform:Find("Item1/ItemCell")
    local rewardlv = LuaHanJiaMgr.TcjhMainData.RewardNmlLv
    if nmlitems == nil then 
        self:FillItemCell(cell,nil,lv,rewardlv,true)
    else
        self:FillItemCell(cell,nmlitems[1],lv,rewardlv,true)
    end    
end

function CLuaTcjhMainRewardTab:FillItemCell(cell,data,lv,rewardlv,inscroll)
    if data == nil then 
        cell.gameObject:SetActive(false)
    else
        local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
        local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
        local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label
        local checkgo = LuaGameObject.GetChildNoGC(cell,"ClearupCheckbox").gameObject
        local lockgo = LuaGameObject.GetChildNoGC(cell,"LockedSprite").gameObject
        local fx = LuaGameObject.GetChildNoGC(cell,"Fx").uiFx

        local itemid = data.ItemID
        local count = data.Count
        local isBind = data.IsBind

        local curlv = LuaHanJiaMgr.TcjhMainData.Lv
        
        local itemcfg = Item_Item.GetData(itemid)

        if itemcfg then
            icon:LoadMaterial(itemcfg.Icon)
        end

        bindgo:SetActive(tonumber(isBind) == 1)

        
        checkgo:SetActive(lv <= rewardlv)
        lockgo:SetActive(lv > curlv or rewardlv < 0)

        if inscroll then
            fx.ScrollView = self.QnTableView1.m_ScrollView
        end

        local canreward = lv > rewardlv and lv <= curlv and rewardlv >= 0
        if canreward then 
            local b = NGUIMath.CalculateRelativeWidgetBounds(cell)
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
            fx.transform.localPosition = waypoints[0]
            fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
            LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        else
            fx:DestroyFx()
        end
        
        UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if canreward then
                Gac2Gas.RequestGetTianChengLevelReward(lv)
            else
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0) 
            end
            if self.SelectedItem ~= go then
                if self.SelectedItem then
                    self:SelectItem(self.SelectedItem,false)
                end
                self.SelectedItem = go
                self:SelectItem(self.SelectedItem,true)
            end
        end)

        if tonumber(count) <= 1 then
            countlb.text = ""
        else
            countlb.text = count
        end

        cell.gameObject:SetActive(true)
    end
end

function CLuaTcjhMainRewardTab:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function CLuaTcjhMainRewardTab:GetLvItems(lv)
    local data = LuaHanJiaMgr.TcjhMainData
    local curcfg = data.Cfg[lv]
    if curcfg == nil then 
        return nil,nil
    else
        return curcfg.NmlItems,curcfg.AdvItems
    end
end

--@region UIEvent

function CLuaTcjhMainRewardTab:OnOneKeyBtnClick()
    local data = LuaHanJiaMgr.TcjhMainData
    if data.Lv > data.RewardNmlLv or (data.PassType ~= 0 and data.Lv > data.RewardAdvLv) then
        local lv = data.Lv
        Gac2Gas.RequestGetTianChengLevelReward(lv)
    end
end

function CLuaTcjhMainRewardTab:OnShopBtnClick()
    local setting = HanJia2021_TianChengSetting.GetData()
    local shopid = setting.ShopID
    CLuaNPCShopInfoMgr.ShowScoreShopById(shopid)
end

--@endregion
