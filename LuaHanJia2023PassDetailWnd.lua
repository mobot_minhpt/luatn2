local GameObject                = import "UnityEngine.GameObject"
local Vector3                   = import "UnityEngine.Vector3"
local Quaternion                = import "UnityEngine.Quaternion"
local CommonDefs                = import "L10.Game.CommonDefs"
local CPayMgr                   = import "L10.Game.CPayMgr"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CUITexture				= import "L10.UI.CUITexture"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LuaGameObject             = import "LuaGameObject"
local LocalString               = import "LocalString"
local UIGrid                    = import "UIGrid"
local UIDragScrollView = import "UIDragScrollView"

LuaHanJia2023PassDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHanJia2023PassDetailWnd, "Grid1", "Grid1", UIGrid)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "BuyAdvPassBtn1", "BuyAdvPassBtn1", GameObject)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "Grid22", "Grid22", UIGrid)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "BuyAdvPassBtn2", "BuyAdvPassBtn2", GameObject)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "Title1PriceLabel", "Title1PriceLabel", UILabel)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "Title2PriceLabel", "Title2PriceLabel", UILabel)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "Grid21", "Grid21", UIGrid)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "ItemIcon2", "ItemIcon2", CUITexture)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "ItemIcon1", "ItemIcon1", CUITexture)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "ImproveLabel1", "ImproveLabel1", UILabel)
RegistChildComponent(LuaHanJia2023PassDetailWnd, "ImproveLabel2", "ImproveLabel2", UILabel)

--@endregion RegistChildComponent end

function LuaHanJia2023PassDetailWnd:Awake()
    self.SelectedItem = nil
    UIEventListener.Get(self.BuyAdvPassBtn1).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick(1)
    end)
    UIEventListener.Get(self.BuyAdvPassBtn2).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick(2)
    end)

    g_ScriptEvent:AddListener("HanJia2023_XianKeLaiPassTypeChange", self, "OnPassTypeChange")
end

function LuaHanJia2023PassDetailWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("HanJia2023_XianKeLaiPassTypeChange", self, "OnPassTypeChange")
end

function LuaHanJia2023PassDetailWnd:OnPassTypeChange()
    local setting = HanJia2023_XianKeLaiSetting.GetData()
    local passtype = LuaHanJia2023Mgr.XKLData.PassType

    local buybtnlb1 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn1.transform,"Label").label
    if passtype == 1 or passtype == 3 then
        buybtnlb1.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn1,false,true)
    else
        buybtnlb1.text = setting.BuyPrice1
    end
    
    local buybtnlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"Label").label
    local buybtnextlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"ExtLabel").label
    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({";"}, 1, MakeArrayClass(System.String))
    local btnstrs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(setting.BuyPrice2, splits, op)
    if passtype == 2 or passtype == 3 then
        buybtnlb2.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn2,false,true)
    else
        buybtnlb2.text = btnstrs[0]
    end
    buybtnextlb2.text = btnstrs[1]
end

function LuaHanJia2023PassDetailWnd:Init()
    local setting = HanJia2023_XianKeLaiSetting.GetData()

    self.Title1PriceLabel.text = setting.OriPrice1
    self.Title2PriceLabel.text = setting.OriPrice2
    
    local passtype = LuaHanJia2023Mgr.XKLData.PassType
    local buybtnlb1 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn1.transform,"Label").label
    if passtype == 1 or passtype == 3 then
        buybtnlb1.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn1,false,true)
    else
        buybtnlb1.text = setting.BuyPrice1
    end

    local buybtnlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"Label").label
    local buybtnextlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"ExtLabel").label

    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({";"}, 1, MakeArrayClass(System.String))
    local btnstrs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(setting.BuyPrice2, splits, op)
    if passtype == 2 or passtype == 3 then
        buybtnlb2.text = LocalString.GetString("已解锁")
        CUICommonDef.SetActive(self.BuyAdvPassBtn2,false,true)
    else
        buybtnlb2.text = btnstrs[0]
    end
    buybtnextlb2.text = btnstrs[1]
    
    local vip1ItemId = setting.Vip1ItemID
    local vip1AddValue = setting.Vip1AddValue
    local vip2ItemId = setting.Vip2ItemID
    local vip2AddValue = setting.Vip2AddValue
    self.ImproveLabel1.text = SafeStringFormat3(LocalString.GetString("仙客来提升%d级"), vip1AddValue / 100)
    self.ImproveLabel2.text = SafeStringFormat3(LocalString.GetString("仙客来提升%d级"), vip2AddValue / 100)
    local materialPathList = g_LuaUtil:StrSplit(setting.BattlePassVipIcon,";")
    self.ItemIcon1:LoadMaterial(materialPathList[1])
    self.ItemIcon2:LoadMaterial(materialPathList[2])

    local des1 = setting.Vip1Prize
    local des2 = setting.Vip2Prize

    local nmldesarray = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(des1, splits, op)
    local advdesarray = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(des2, splits, op)
    self:FillItems1(nmldesarray)
    self:FillItems2(advdesarray)
end

function LuaHanJia2023PassDetailWnd:FillItems(desarray,grid,isleft)
    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({","}, 1, MakeArrayClass(System.String))
    local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(desarray, splits, op)
    local len = strs.Length
    for i=0,len-1,2 do
        local go = GameObject.Instantiate(self.ItemCell, Vector3.zero, Quaternion.identity)
        go.transform.parent = grid.transform
        go.transform.localScale = Vector3.one
        self:FillItem(go,strs[i],strs[i+1],isleft)
        go:SetActive(true)
    end
    grid:Reposition()
end

function LuaHanJia2023PassDetailWnd:FillItems1(desarray)
    self:FillItems(desarray[0],self.Grid1,true)
end

function LuaHanJia2023PassDetailWnd:FillItems2(desarray)
    self:FillItems(desarray[0],self.Grid21,false)
    self:FillItems(desarray[1],self.Grid22,false)
end

function LuaHanJia2023PassDetailWnd:FillItem(item,itemid,itemcount,isleft)
    local itemcfg = Item_Item.GetData(itemid)
    local iconTex = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ctTxt = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    if itemcfg then
        iconTex:LoadMaterial(itemcfg.Icon)
    end

    if tonumber(itemcount) <= 1 then
        ctTxt.text = ""
    else
        ctTxt.text = itemcount
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local atype
        if isleft then
            atype = AlignType.Default
        else
            atype = AlignType.Default
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, atype, 0, 0, 0, 0)
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end
    end)
end

function LuaHanJia2023PassDetailWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

--@region UIEvent

function LuaHanJia2023PassDetailWnd:OnBuyAdvPassBtnClick(type)
    local setting = HanJia2023_XianKeLaiSetting.GetData()
    if type == 1 then
        Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMallLimit, setting.Vip1ItemID, 1)
        --CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(setting.Vip1PID), 0)
    else
        Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMallLimit, setting.Vip2ItemID, 1)
        --CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(setting.Vip2PID), 0)
    end
end

--@endregion
