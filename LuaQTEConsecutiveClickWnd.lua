local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local UnityEngine = import "UnityEngine"
local CUIFx = import "L10.UI.CUIFx"

LuaQTEConsecutiveClickWnd = class()
RegistClassMember(LuaQTEConsecutiveClickWnd, "Button")
RegistClassMember(LuaQTEConsecutiveClickWnd, "CountDownBar")
RegistClassMember(LuaQTEConsecutiveClickWnd, "ProgressBar")
RegistClassMember(LuaQTEConsecutiveClickWnd, "curTime")
RegistClassMember(LuaQTEConsecutiveClickWnd, "totTime")
RegistClassMember(LuaQTEConsecutiveClickWnd, "curProgress")
RegistClassMember(LuaQTEConsecutiveClickWnd, "decreaseRate")
RegistClassMember(LuaQTEConsecutiveClickWnd, "increaseRate")
RegistClassMember(LuaQTEConsecutiveClickWnd, "UIFx")
RegistClassMember(LuaQTEConsecutiveClickWnd, "Circle")
RegistClassMember(LuaQTEConsecutiveClickWnd, "HintFx")


function LuaQTEConsecutiveClickWnd:Awake()
    self.Button = self.transform:Find("Button").gameObject
    self.CountDownBar = self.transform:Find("Button/CountDownBar"):GetComponent(typeof(UITexture))
    self.ProgressBar = self.transform:Find("ProgressBar"):GetComponent(typeof(UITexture))
    self.UIFx = self.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self.Circle = self.transform:Find("Button/Circle").gameObject
    self.HintFx = self.transform:Find("Button/HintFx"):GetComponent(typeof(CUIFx))

    self.curTime, self.totTime, self.curProgress, self.decreaseRate, self.increaseRate = 0, 0, 0, 0, 0
    self.gameObject:SetActive(false)
end

function LuaQTEConsecutiveClickWnd:Start()
    self.UIFx:LoadFx(CLuaUIFxPaths.QTEConsecutiveClickFx)
end

function LuaQTEConsecutiveClickWnd:Init()
    if LuaQTEMgr.CurrentQTEType ~= EnumQTEType.ConsecutiveClick then
        return
    end

    CommonDefs.AddOnClickListener(
        self.Button,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnButtonClicked(go)
            end
        ),
        false
    )
    self.curTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteTimeOffset)
    self.totTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteDuration)
    self.curProgress = tonumber(LuaQTEMgr.CurrentQTEInfo.initProgress)
    self.decreaseRate = tonumber(LuaQTEMgr.CurrentQTEInfo.decreaseRate)
    self.increaseRate = tonumber(LuaQTEMgr.CurrentQTEInfo.increaseRate)

    self.transform.localPosition =
        LuaQTEMgr.ParsePosition(self.gameObject, LuaQTEMgr.CurrentQTEInfo.xPos, LuaQTEMgr.CurrentQTEInfo.yPos)

    self.HintFx:DestroyFx()
    self.gameObject:SetActive(true)

    if LuaQTEMgr.CurrentQTEInfo.HintFx == nil or LuaQTEMgr.CurrentQTEInfo.HintFx == "" then
        self.Circle:SetActive(true)
    else
        self.Circle:SetActive(false)
        self.HintFx:LoadFx(SafeStringFormat3("Fx/UI/Prefab/%s.prefab", LuaQTEMgr.CurrentQTEInfo.HintFx))
    end
end

function LuaQTEConsecutiveClickWnd:Update()
    local deltaTime = UnityEngine.Time.deltaTime
    self.curTime = self.curTime + deltaTime
    local timeNow = math.min(self.totTime, math.max(self.curTime, 0.0))

    local ratio = 0.0
    if self.totTime ~= 0 then
        ratio = 1 - timeNow / self.totTime
    end
    self.CountDownBar.fillAmount = ratio

    if ratio <=0 then
        self:QTEFail()
        return
    end

    if self.curProgress >= 1.0 then -- 连续快速点击成功
        self:QTESuccess()
    else
        self.curProgress = self.curProgress - self.decreaseRate * deltaTime
        if self.curProgress <= 0.0 then
            self:QTEFail()
        else
            self.ProgressBar.fillAmount = self.curProgress
        end
    end
end

function LuaQTEConsecutiveClickWnd:OnButtonClicked(go)
    self.curProgress = self.curProgress + self.increaseRate
end

function LuaQTEConsecutiveClickWnd:QTESuccess()
    LuaQTEMgr.TriggerQTEFinishFx(true, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(true)
    self:Reset()
end

function LuaQTEConsecutiveClickWnd:QTEFail()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(false)
    self:Reset()
end

function LuaQTEConsecutiveClickWnd:OnTimeLimitExceeded()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    self:Reset()
end

function LuaQTEConsecutiveClickWnd:Reset()
    self.curTime, self.totTime, self.curProgress, self.decreaseRate, self.increaseRate = 0, 0, 0, 0, 0
    self.gameObject:SetActive(false)
    g_ScriptEvent:BroadcastInLua("OnQTESubWndClose")
end
