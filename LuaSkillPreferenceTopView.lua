local UITabBar = import "L10.UI.UITabBar"
local SkillSeriesInfo = import "L10.Game.SkillSeriesInfo"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local Extensions = import "Extensions"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CItemMgr = import "L10.Game.CItemMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local TouchPhase = import "UnityEngine.TouchPhase"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local SkillCategory = import "L10.Game.SkillCategory"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

LuaSkillPreferenceTopView = class()
RegistChildComponent(LuaSkillPreferenceTopView,"m_SkillItemTemplate","SkillItemTemplate", GameObject)
RegistChildComponent(LuaSkillPreferenceTopView,"m_LevelScaleTemplate","LevelScaleTemplate", GameObject)
RegistChildComponent(LuaSkillPreferenceTopView,"m_ArrowTemplate","ArrowTemplate", GameObject)
RegistChildComponent(LuaSkillPreferenceTopView,"m_IdentifySkillTemplate","IdentifySkillTemplate", GameObject)
RegistChildComponent(LuaSkillPreferenceTopView,"m_FollowFingerIcon","CloneItem", GameObject)
RegistChildComponent(LuaSkillPreferenceTopView,"m_TabBar","Tabs", UITabBar)
RegistChildComponent(LuaSkillPreferenceTopView,"m_SkillMapRoot","SkillMap", GameObject)
RegistChildComponent(LuaSkillPreferenceTopView,"m_IdentifySkillTable","IdentifySkillTable", UITable)
RegistChildComponent(LuaSkillPreferenceTopView,"m_ScaleLine","ScaleLine", UISprite)
RegistChildComponent(LuaSkillPreferenceTopView,"m_TipTopPos","TipTopPos", Transform)


RegistClassMember(LuaSkillPreferenceTopView,"m_SkillSeries") --技能系别
RegistClassMember(LuaSkillPreferenceTopView,"m_TabIndex")
RegistClassMember(LuaSkillPreferenceTopView,"m_IdentifySkillTabIndex")
RegistClassMember(LuaSkillPreferenceTopView,"m_IsInit")
RegistClassMember(LuaSkillPreferenceTopView,"m_ItemCellDict")
RegistClassMember(LuaSkillPreferenceTopView,"m_BodyPos2ItemCellDict")
RegistClassMember(LuaSkillPreferenceTopView,"m_ItemCells")
RegistClassMember(LuaSkillPreferenceTopView,"m_IsDragging")
RegistClassMember(LuaSkillPreferenceTopView,"m_LastDragPos")
RegistClassMember(LuaSkillPreferenceTopView,"m_FingerIndex")
RegistClassMember(LuaSkillPreferenceTopView,"m_SelectedIndex")
RegistClassMember(LuaSkillPreferenceTopView,"m_SkillId")
RegistClassMember(LuaSkillPreferenceTopView,"m_CurSwitchYingLingState")
RegistClassMember(LuaSkillPreferenceTopView,"m_ShiMenMiShuCell")
RegistClassMember(LuaSkillPreferenceTopView,"m_HpSkillCell")

function LuaSkillPreferenceTopView:Awake()

    if not self.m_IsInit then
        self.m_CurSwitchYingLingState = LuaSkillMgr.GetDefaultYingLingState()
        self.m_IsInit = true
        self.m_IsDragging = false
        self.m_SkillItemTemplate:SetActive(false)
        self.m_LevelScaleTemplate:SetActive(false)
        self.m_ArrowTemplate:SetActive(false)
        self.m_IdentifySkillTemplate:SetActive(false)
        self.m_FollowFingerIcon:SetActive(false)

        self.m_IdentifySkillTabIndex = 3

        self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
            self:OnTabChange(go, index)
        end)

        self:InitSkillSeries()
        self:InitIdentifySkill()
    end
    self.m_TabBar:ChangeTab(0, false)
end

function LuaSkillPreferenceTopView:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnSkillPropUpdate")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SkillPreferenceViewSwitchYingLingState",self,"OnYingLingStateSwitch")
    self.m_TabBar:ChangeTab(0, false)
end

function LuaSkillPreferenceTopView:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnSkillPropUpdate")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SkillPreferenceViewSwitchYingLingState",self,"OnYingLingStateSwitch")
end

function LuaSkillPreferenceTopView:Update()
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end

    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        CSkillInfoMgr.CloseSkillInfoWnd()
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.m_FollowFingerIcon.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            if self.m_LastDragPos ~= Input.mousePosition then
                CSkillInfoMgr.CloseSkillInfoWnd()
            end
            self.m_FollowFingerIcon.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition

            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.m_FollowFingerIcon.gameObject:SetActive(false)
    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject and hoveredObject.name == "__SkillIcon__" then
        g_ScriptEvent:BroadcastInLua("SkillPreferenceView_SkillTree_OnDragComplete",
                {self.m_SkillId, hoveredObject})
    end
    self.m_IsDragging = false
    self.m_FingerIndex = -1
    self.m_SkillId = UInt32.MaxValue
end

function LuaSkillPreferenceTopView:GetCurSwitchYingLingState()
    if (self.m_CurSwitchYingLingState == nil) or (self.m_CurSwitchYingLingState == EnumToInt(EnumYingLingState.eDefault)) then
        self.m_CurSwitchYingLingState = LuaSkillMgr.GetDefaultYingLingState()
    end
    return self.m_CurSwitchYingLingState
end

function LuaSkillPreferenceTopView:InitSkillSeries()
    if self.m_SkillSeries == nil then
        local mainPlayer = CClientMainPlayer.Inst
        if mainPlayer ~= nil then
            if self.m_SkillSeries == nil then
                local series = mainPlayer.SkillSeries
                self.m_SkillSeries = CreateFromClass(MakeArrayClass(SkillSeriesInfo), series.Length - 1)
                CommonDefs.Array_Copy_Array_Array_Int32(series, self.m_SkillSeries, series.Length - 1)
                --剔除绝技
            end
        end
    end
end

function LuaSkillPreferenceTopView:InitIdentifySkill()
    if self.m_SkillSeries ~= nil then
        local labels = nil
        local go = self.m_TabBar:GetTabGoByIndex(self.m_IdentifySkillTabIndex)
        if go ~= nil then
            go:SetActive(true)
            --避免InitWithLabels报错
            labels = CreateFromClass(MakeArrayClass(String), self.m_SkillSeries.Length + 1)
            labels[self.m_SkillSeries.Length] = LocalString.GetString("其它")
        else
            labels = CreateFromClass(MakeArrayClass(String), self.m_SkillSeries.Length)
        end
        for i = 0, self.m_SkillSeries.Length - 1 do
            labels[i] = self.m_SkillSeries[i].Value
        end
        self.m_TabBar:InitWithLabels(labels)
        if go ~= nil then
            go:SetActive(CSwitchMgr.EnableIdentifySkill)
        end
    end
end
----------------------------------------
---Event
----------------------------------------
function LuaSkillPreferenceTopView:OnSendItem(data)
    local itemId = data[0]
    if self.m_TabBar.SelectedIndex ~= self.m_IdentifySkillTabIndex then
        return
    end

    if CItemMgr.Inst:FindItemPosition(EnumItemPlace.Body, itemId) <= 0 then
        return
    end

    local item = CItemMgr.Inst:GetById(itemId)
    if item and item.IsEquip then
        self:UpdateIndentifySkills()
    end
end

function LuaSkillPreferenceTopView:OnYingLingStateSwitch(state)
    self.m_CurSwitchYingLingState = state

    if self.m_TabIndex == self.m_IdentifySkillTabIndex then
        return
    end
    local idx = 1
    if #self.m_ItemCells then
        for i = 1,#self.m_ItemCells do
            if self.m_ItemCells[i].selectedSprite.enabled then
                idx = i
                break
            end
        end
    end
    self:LoadCommonSkills()
    if idx >= 1 and idx <= #self.m_ItemCells then
        self:OnSkillItemClick(self.m_ItemCells[idx].gameObject, false)
    end
end

function LuaSkillPreferenceTopView:OnSkillPropUpdate()
    if self.m_TabBar.SelectedIndex == self.m_IdentifySkillTabIndex then
        self:UpdateIndentifySkills()

        self:UpdateShiMenMiShuSkillItemData(self.m_ShiMenMiShuCell)
        if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
            self:UpdateHpSkillItemData(self.m_HpSkillCell)
        end
    else
        self:UpdateSkills()
    end
end

function LuaSkillPreferenceTopView:OnTabChange(go, index)
    if self.m_SkillSeries == nil then
        return
    end

    self.m_TabIndex = index

    if index == self.m_IdentifySkillTabIndex then
        self:LoadIdentifySkills()
    else
        self:LoadCommonSkills()
    end
end
----------------------------------------
---技能面板显示相关
----------------------------------------
function LuaSkillPreferenceTopView:LoadCommonSkills()
    self.m_SkillMapRoot:SetActive(true)
    self.m_IdentifySkillTable.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.m_ScaleLine.transform)
    self.m_ItemCellDict = {}
    self.m_BodyPos2ItemCellDict = {}
    self.m_ItemCells = {}
    self.m_ShiMenMiShuCell = nil
    self.m_HpSkillCell = nil

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    if self.m_SkillSeries == nil or self.m_TabBar.SelectedIndex < 0 or
            self.m_TabBar.SelectedIndex >= self.m_SkillSeries.Length then
        return
    end

    local info = CSkillMgr.Inst:GetPlayerProfessionSkillInfo(mainPlayer.Class, mainPlayer.SkillProp, self.m_SkillSeries[self.m_TabBar.SelectedIndex].Key, mainPlayer.Level)
    local professionSkillIds = info.professionSkillIds

    self:LoadLevelScaleLine(info)

    if mainPlayer.IsYingLing then
        professionSkillIds = LuaSkillMgr.GetYingLingNewProfessionSkillIds(professionSkillIds,  self:GetCurSwitchYingLingState())
    end

    for i = 0, professionSkillIds.Count - 1 do
        self:InitSkillItem(professionSkillIds[i].Key)
    end

    for i = 0, professionSkillIds.Count - 1 do
        self:LoadArrow(professionSkillIds[i].Key)
    end

    if #self.m_ItemCells > 0 then
        self:OnSkillItemClick(1, false)
    end
end

function LuaSkillPreferenceTopView:LoadIdentifySkills()
    self.m_SkillMapRoot:SetActive(false)
    self.m_IdentifySkillTable.gameObject:SetActive(true)
    self.m_ItemCellDict = {}
    self.m_BodyPos2ItemCellDict = {}
    self.m_ItemCells = {}
    self.m_ShiMenMiShuCell = nil
    self.m_HpSkillCell = nil

    Extensions.RemoveAllChildren(self.m_IdentifySkillTable.transform)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    for i=1,12 do
        if CItemMgr.Inst:IsIdentifiableBodyPosition(i) then
            self:InitIdentifySkillItem(i)
        end
    end

    self:InitShiMenMiShuSkillItem()
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        self:InitHpSkillItem()
    end

    self.m_IdentifySkillTable:Reposition()
end

function LuaSkillPreferenceTopView:SetIdentifySkillLabel(cell, clsExists, equip)
    local disableLabel = cell.transform:Find("DisableLabel")
    if disableLabel ~= nil then
        disableLabel.gameObject:SetActive(clsExists and equip ~= nil and equip.IsIdentifiable)
        if disableLabel.gameObject.activeSelf then
            local label = disableLabel:GetComponent(typeof(UILabel))
            if equip.IsIdentifySkillDisabled then
                label.text = LocalString.GetString("[FFAE1D]已禁用[-]")
            else
                label.text = nil
            end
        end
    end
end

function LuaSkillPreferenceTopView:SetZongMenSkillDisableLabel(cell, clsExists)
    local disableLabel = cell.transform:Find("DisableLabel")
    if disableLabel ~= nil then
        disableLabel.gameObject:SetActive(clsExists)
        if disableLabel.gameObject.activeSelf then
            local label = disableLabel:GetComponent(typeof(UILabel))
            label.text = nil
        end
    end
end

function LuaSkillPreferenceTopView:LoadLevelScaleLine(info)
    local unlockLevels = info.unlockLevels
    if unlockLevels.Count == 0 then
        return
    end
    local layoutDict = info.layoutDict
    CommonDefs.ListSort1(unlockLevels, typeof(UInt32), DelegateFactory.Comparison_uint(function (level1, level2)
        return NumberCompareTo(level1, level2)
    end))

    for i = 0, unlockLevels.Count - 1 do
        local item = NGUITools.AddChild(self.m_ScaleLine.gameObject, self.m_LevelScaleTemplate)
        item:SetActive(true)
        Extensions.SetLocalPositionX(item.transform, CommonDefs.DictGetValue(layoutDict, typeof(UInt32), unlockLevels[i]) * 900 + 50 )
        CommonDefs.GetComponent_GameObject_Type(item, typeof(UILabel)).text = tostring(unlockLevels[i])
        CommonDefs.GetComponentInChildren_GameObject_Type(item, typeof(UISprite)):ResetAndUpdateAnchors()
    end
end

function LuaSkillPreferenceTopView:LoadArrow(skillId)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local data = Skill_AllSkills.GetData(skillId)
    local _pairs = data.AdjustedPreSkills
    if _pairs ~= nil and _pairs.Length > 0 then
        for j = 0,_pairs.Length - 1 do
            local realFromSkillCls = _pairs[j][0]
            if CSkillMgr.Inst:IsColorSystemBaseSkill(realFromSkillCls) then
                realFromSkillCls = mainPlayer.SkillProp:GetInEffectColorSystemSkill(realFromSkillCls)
            end

            -- 影灵破云击特殊处理
            if mainPlayer.IsYingLing then
                local cls = LuaSkillMgr.GetYingLingRealSkillCls(realFromSkillCls,self:GetCurSwitchYingLingState())
                if cls then
                    realFromSkillCls = cls
                end
            end

            if self.m_ItemCellDict[realFromSkillCls] then
                --生成一条箭头
                local item = NGUITools.AddChild(self.m_ScaleLine.gameObject, self.m_ArrowTemplate)
                item:SetActive(true)
                local sprite = CommonDefs.GetComponent_GameObject_Type(item, typeof(UISprite))
                local from = self.m_ItemCellDict[realFromSkillCls]
                local to = self.m_ItemCellDict[CLuaDesignMgr.Skill_AllSkills_GetClass(skillId)]
                sprite.width = math.floor((to.transform.localPosition.x - from.transform.localPosition.x - to.Width * 0.5 - from.Width * 0.5))
                sprite.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(from.transform.localPosition, CommonDefs.op_Multiply_Vector3_Single(CommonDefs.op_Multiply_Vector3_Single(Vector3.right, from.Width), 0.5))
            end
        end
    end
end

function LuaSkillPreferenceTopView:InitSkillItem(skillId)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local item = NGUITools.AddChild(self.m_ScaleLine.gameObject, self.m_SkillItemTemplate)
    item:SetActive(true)
    local data = Skill_AllSkills.GetData(skillId)
    local skillClass = data.SkillClass
    local cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CSkillItemCell))
    local clsExists = mainPlayer.SkillProp:IsOriginalSkillClsExist(skillClass)
    local originSkillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(skillClass)
    local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(skillClass, mainPlayer.Level)
    local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
    local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillIdWithDeltaLevel) or 0
    local feishengLevel = clsExists and mainPlayer.SkillProp:GetFeiShengModifyLevel(originSkillId, data.Kind, mainPlayer.Level) or 0
    local delta = originWithDeltaLevel - feishengLevel
    if mainPlayer.IsInXianShenStatus then
        cell:InitFeiSheng(true, feishengLevel)
    else
        cell:InitFeiSheng(false, 0)
    end

    local isUnUse = LuaSkillMgr.IsYingLingSkillCanUse(skillClass, self:GetCurSwitchYingLingState())

    if  isUnUse then
        cell:Init(skillClass, originLevel, delta, true, data.SkillIcon, true, nil)
        cell.disableSprite.alpha = 0.5
    else
        cell:Init(skillClass, originLevel, delta, true, data.SkillIcon, originLevel == 0, nil)
    end

    local nextIdx = #self.m_ItemCells + 1
    UIEventListener.Get(cell.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(nextIdx)
    end)
    UIEventListener.Get(cell.gameObject).onPress =  DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPressSkillIcon(nextIdx, isPressed, skillClass)
    end)
    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCommonSkillItemClick(nextIdx)
    end)
    item.transform.localPosition = Vector3((data.Layout[1] / 1000) * 900 + 50 , data.Layout[0] > 0 and 100 or - 100, 0)
    self.m_ItemCellDict[cell.ClassId] = cell
    table.insert(self.m_ItemCells, cell)
end

function LuaSkillPreferenceTopView:InitIdentifySkillItem(pos)

    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    local cell, item = self:UpdateIdentifySkillItemData(pos)
    if (not cell) or (not item) then
        return
    end

    local idx = #self.m_ItemCells

    UIEventListener.Get(cell.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(idx)
    end)
    UIEventListener.Get(cell.gameObject).onPress =  DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPressSkillIcon(idx, isPressed, cell.ClassId)
    end)
    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCommonSkillItemClick(idx)
    end)
end

function LuaSkillPreferenceTopView:UpdateIdentifySkillItemData(pos, cell)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return nil,nil
    end

    local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Body, pos)
    local commonItem = CItemMgr.Inst:GetById(itemId)
    local skillId = 0
    if commonItem ~= nil and commonItem.IsEquip and commonItem.Equip.IsIdentifiable then
        skillId = commonItem.Equip.FixedIdentifySkillId
    end

    local item = nil
    if cell == nil then
        item = CUICommonDef.AddChild(self.m_IdentifySkillTable.gameObject, self.m_IdentifySkillTemplate)
        item:SetActive(true)
        cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CSkillItemCell))
    end
    local clsExists = Skill_AllSkills.Exists(skillId)
    local originSkillId = skillId
    local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
    local feishengLevel = originLevel
    local delta = 0
    cell:InitFeiSheng(mainplayer.IsInXianShenStatus, mainplayer.IsInXianShenStatus and feishengLevel or 0)
    local data = Skill_AllSkills.GetData(skillId)
    local text = (not clsExists) and CItemMgr.Inst:GetBodyPositionName(pos) or nil
    local inEffect = commonItem ~= nil and mainplayer.SkillProp:IsIdentifySkillInEffect(skillId, commonItem.Id)
    local extern = data and data.SkillIcon or nil
    cell:Init((data == nil) and 0 or data.SkillClass, originLevel, delta, true, extern, not inEffect, text)
    self:SetIdentifySkillLabel(cell.gameObject, clsExists, commonItem and  commonItem.Equip or nil)

    self.m_BodyPos2ItemCellDict[pos] = cell
    table.insert(self.m_ItemCells, cell)
    return cell, item
end

function LuaSkillPreferenceTopView:InitShiMenMiShuSkillItem()
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    local cell, item = self:UpdateShiMenMiShuSkillItemData()
    if (not cell) or (not item) then
        return
    end

    table.insert(self.m_ItemCells, cell)
    local idx = #self.m_ItemCells

    UIEventListener.Get(cell.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(idx)
    end)
    UIEventListener.Get(cell.gameObject).onPress =  DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPressSkillIcon(idx, isPressed, cell.ClassId)
    end)
    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnIndentifySkillItemClick(idx)
    end)
end

function LuaSkillPreferenceTopView:UpdateShiMenMiShuSkillItemData(cell)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    
    local skillClass = SoulCore_ShiMenMiShu.GetData(EnumToInt(mainPlayer.Class)).SkillCls
    local skillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(skillClass)
    local data = Skill_AllSkills.GetData(skillId)

    local item = nil
    if cell == nil then
        item = CUICommonDef.AddChild(self.m_IdentifySkillTable.gameObject, self.m_IdentifySkillTemplate)
        item:SetActive(true)
        cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CSkillItemCell))
    end

    local clsExists = mainPlayer.SkillProp:IsOriginalSkillClsExist(skillClass)
    local originSkillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(skillClass)
    local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(skillClass, mainPlayer.Level)
    local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
    local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillIdWithDeltaLevel) or 0
    local feishengLevel = clsExists and LuaZongMenMgr:GetPlayerAdjustShiMenMiShuSkillLv(skillId, mainPlayer.Level, mainPlayer.SkillProp) or 0
    local delta = 0
    if mainPlayer.IsInXianShenStatus then
        cell:InitFeiSheng(true, feishengLevel)
    else
        cell:InitFeiSheng(false, 0)
    end

    local isUnUse = LuaSkillMgr.IsYingLingSkillCanUse(skillClass, self:GetCurSwitchYingLingState())

    if  isUnUse then
        cell:Init(skillClass, originLevel, delta, true, data and data.SkillIcon or nil, true, clsExists and "" or LocalString.GetString("秘术"))
        cell.disableSprite.alpha = 0.5
    else
        cell:Init(skillClass, originLevel, delta, true, data and data.SkillIcon or nil, originLevel == 0, clsExists and "" or LocalString.GetString("秘术"))
    end
    self:SetZongMenSkillDisableLabel(cell.gameObject, clsExists)
    
    self.m_ShiMenMiShuCell = cell
    return cell, item
end

function LuaSkillPreferenceTopView:InitHpSkillItem()
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    local cell, item = self:UpdateHpSkillItemData()
    if (not cell) or (not item) then
        return
    end

    table.insert(self.m_ItemCells, cell)
    local idx = #self.m_ItemCells

    UIEventListener.Get(cell.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(go)
        self:OnDragSkillIconStart(idx)
    end)
    UIEventListener.Get(cell.gameObject).onPress =  DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPressSkillIcon(idx, isPressed, cell.ClassId)
    end)
    UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnIndentifySkillItemClick(idx)
    end)
end

function LuaSkillPreferenceTopView:UpdateHpSkillItemData(cell)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    local item = nil
    if cell == nil then
        item = CUICommonDef.AddChild(self.m_IdentifySkillTable.gameObject, self.m_IdentifySkillTemplate)
        item:SetActive(true)
        cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CSkillItemCell))
    end

    local skillClass = SoulCore_Settings.GetData().HpSkills
    local originLevel = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.HpSkillLevel, 1) and CClientMainPlayer.Inst.SkillProp.HpSkillLevel[1] or 0
    local data = Skill_AllSkills.GetData(skillClass * 100 + originLevel)
    local originSkillId = skillClass * 100 + originLevel
    local clsExists = originLevel ~= 0
    if not clsExists then
        originSkillId = skillClass * 100 
    end
    local feishengLevel = clsExists and LuaZongMenMgr:GetAdjustHpSkillLevel(originLevel, CClientMainPlayer.Inst.Level) or 0
    local delta = 0
    if mainPlayer.IsInXianShenStatus then
        cell:InitFeiSheng(true, feishengLevel)
    else
        cell:InitFeiSheng(false, 0)
    end

    local isUnUse = LuaSkillMgr.IsYingLingSkillCanUse(skillClass, self:GetCurSwitchYingLingState())

    if  isUnUse then
        cell:Init(skillClass, originLevel, delta, true, data and data.SkillIcon or nil, true, clsExists and "" or LocalString.GetString("长生诀"))
        cell.disableSprite.alpha = 0.5
    else
        cell:Init(skillClass, originLevel, delta, true, data and data.SkillIcon or nil, originLevel == 0, clsExists and "" or LocalString.GetString("长生诀"))
    end
    self:SetZongMenSkillDisableLabel(cell.gameObject, clsExists)
    
    self.m_HpSkillCell = cell
    return cell, item
end

function LuaSkillPreferenceTopView:UpdateSkills()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end


    for i = 1, #self.m_ItemCells do
        local classId = self.m_ItemCells[i].ClassId

        if CSkillMgr.Inst:IsColorSystemBaseSkill(classId) or CSkillMgr.Inst:IsColorSystemSkill(classId) then
            --对技能进行替换
            local replaceSkillCls = mainPlayer.SkillProp:GetInEffectColorSystemSkill(classId)
            if replaceSkillCls ~= classId then
                --色系技能发生了变化
                classId = replaceSkillCls
                self.m_ItemCells[i]:UpdateColorSystemSkillIcon(classId)
            end
        end

        local clsExists = mainPlayer.SkillProp:IsOriginalSkillClsExist(classId)
        local originSkillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(classId)
        local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(classId, mainPlayer.Level)
        local originLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillId) or 0
        local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillIdWithDeltaLevel) or 0
        local feishengLevel = mainPlayer.SkillProp:GetFeiShengModifyLevel(originSkillId, EnumToInt(self.m_ItemCells[i].Kind), mainPlayer.Level)
        local delta = originWithDeltaLevel - feishengLevel
        if mainPlayer.IsInXianShenStatus then
            self.m_ItemCells[i]:InitFeiSheng(true, feishengLevel)
        else
            self.m_ItemCells[i]:InitFeiSheng(false, 0)
        end

        local unused = LuaSkillMgr.IsYingLingSkillCanUse(classId, self:GetCurSwitchYingLingState())
        if  unused then
            self.m_ItemCells[i]:UpdateLevel(classId, originLevel, delta, true, true, nil)
            self.m_ItemCells[i].disableSprite.alpha = 0.5
        else
            self.m_ItemCells[i]:UpdateLevel(classId, originLevel, delta, true, originLevel == 0, nil)
        end
    end


    if  self.m_SelectedIndex > 0 and self.m_SelectedIndex <= #self.m_ItemCells then
        g_ScriptEvent:BroadcastInLua("SkillPreferenceView_SkillTree_OnSkillClassSelect",
                {self.m_ItemCells[self.m_SelectedIndex].ClassId})
    end
end

function LuaSkillPreferenceTopView:UpdateIndentifySkills()
    if not self.m_IdentifySkillTable.gameObject.activeSelf then
        return
    end

    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end

    for pos, cell in pairs(self.m_BodyPos2ItemCellDict) do
        self:UpdateIdentifySkillItemData(pos, cell)
    end
end
----------------------------------------
---技能替换相关
----------------------------------------
function LuaSkillPreferenceTopView:OnDragSkillIconStart(idx)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    if (#self.m_ItemCells < idx) or (idx < 1) then
        return
    end

    local skillClassId = self.m_ItemCells[idx].ClassId
    local SkillProp = mainPlayer.SkillProp
    local clsExists = SkillProp:IsOriginalSkillClsExist(skillClassId)
    if not clsExists then
        return
    end

    local originSkillIdWithDeltaLevel = mainPlayer.SkillProp:GetSkillIdWithDeltaByCls(skillClassId, mainPlayer.Level)
    local originWithDeltaLevel = clsExists and CLuaDesignMgr.Skill_AllSkills_GetLevel(originSkillIdWithDeltaLevel) or 0
    self.m_SkillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(skillClassId,originWithDeltaLevel)

    local go = self.m_ItemCells[idx]
    if not self:CheckReplaceSkill(skillClassId, go) then
        return
    end

    self.m_FollowFingerIcon.gameObject:SetActive(true)
    CommonDefs.GetComponentInChildren_GameObject_Type(self.m_FollowFingerIcon, typeof(CUITexture))
        :LoadSkillIcon(Skill_AllSkills.GetData(self.m_SkillId).SkillIcon)
    self.m_IsDragging = true
    self.m_LastDragPos = Input.mousePosition

    if CommonDefs.IsInMobileDevice() then
        self.m_FingerIndex = Input.GetTouch(0).fingerId
        local pos = Input.GetTouch(0).position
        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
    end

    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Switch, Vector3.zero)
end

function LuaSkillPreferenceTopView:CheckReplaceSkill(skillClassId, go)

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return false
    end

    if not self:CheckReplaceIdentifySkill(skillClassId, go) then
        return false
    end
    local skillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(skillClassId)

    local data = Skill_AllSkills.GetData(skillId)
    if data.ECategory == SkillCategory.Passive then
        --被动技能不能更换
        g_MessageMgr:ShowMessage("Passive_Skills_Move")
        return false
    end

    if not self:CheckReplaceYingLingSkill(skillClassId, go) then
        return false
    end

    return true
end

function LuaSkillPreferenceTopView:CheckReplaceIdentifySkill(skillClassId, go)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return false
    end
    local SkillProp = mainPlayer.SkillProp
    if CSkillMgr.Inst:IsIdentifySkill(skillClassId) then
        --对于鉴定技能，需要检查当前鉴定技能是否已经生效
        --有可能被禁用或者已过期或者是被高等级替换

        for k, v in pairs(self.m_BodyPos2ItemCellDict) do
            if v.gameObject == go then
                local equip = CItemMgr.Inst.GetEquipByBodyPos(k)
                local __try_get_result, equipId = CommonDefs.DictTryGet(SkillProp.IdentifySkills, typeof(Int32), skillClassId, typeof(String))
                if equipId  == nil  or equip.Id ~= equipId then
                    return false
                end
                break
            end
        end
    end
    return true
end

function LuaSkillPreferenceTopView:CheckReplaceYingLingSkill(skillClassId, go)

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return false
    end

    if mainPlayer.IsYingLing then
        --影灵不同形态的技能不能更换
        local isWarning = false
        Skill_YingLingSkill.ForeachKey(function (key)
            if key == self:GetCurSwitchYingLingState() then
                return false
            end
            local v = Skill_YingLingSkill.GetData(key)
            for i = 0, v.Skills.Length - 1 do
                local clsId = v.Skills[i]
                if clsId == skillClassId then
                    isWarning = true
                    break
                end
            end
        end)
        if isWarning then
            g_MessageMgr:ShowMessage("YingLing_WrongSkill_Move")
            return false
        end
    end
    return true
end
----------------------------------------
---技能选中相关
----------------------------------------
function LuaSkillPreferenceTopView:OnPressSkillIcon(idx, pressed, skillClassId)
    if pressed then
        self:OnSkillItemClick(idx, false)
    end
end

function LuaSkillPreferenceTopView:OnCommonSkillItemClick(idx)
    self:OnSkillItemClick(idx, true)
end

function LuaSkillPreferenceTopView:OnIndentifySkillItemClick(idx)
    self:OnSkillItemClick(idx, true)
end

function LuaSkillPreferenceTopView:OnSkillItemClick(idx, showTip)
    for i = 1,#self.m_ItemCells do
        if idx == i then
            self.m_SelectedIndex = i
            self.m_ItemCells[i].Selected = true
            if showTip then
                local equipId = nil
                if CSkillMgr.Inst:IsIdentifySkill(self.m_ItemCells[i].ClassId) then
                    for k, v in pairs(self.m_BodyPos2ItemCellDict) do
                        if v == self.m_ItemCells[i] then
                            local equip = CItemMgr.Inst:GetEquipByBodyPos(k)
                            if equip ~= nil then
                                equipId = equip.Id
                            end
                            break
                        end
                    end
                end
                CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(
                        self.m_ItemCells[i].ClassId,
                        (self.m_ItemCells[i].OriginWithDeltaLevel == 0) and 1 or self.m_ItemCells[i].OriginWithDeltaLevel),
                        equipId, true, self.m_TipTopPos.position,
                        CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference, true, 0, 0, nil)
            end
        else
            self.m_ItemCells[i].Selected = false
        end
    end

    if self.m_SelectedIndex > 0 and self.m_SelectedIndex <= #self.m_ItemCells then
        g_ScriptEvent:BroadcastInLua("SkillPreferenceView_SkillTree_OnSkillClassSelect",
                {self.m_ItemCells[self.m_SelectedIndex].ClassId})
    end

end
----------------------------------------
---引导相关
----------------------------------------
function LuaSkillPreferenceTopView:Get6thSkillTab()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return nil
    end
    local skillClass = CLuaGuideMgr.Get6thSkillClass()
    if mainPlayer.SkillProp:IsOriginalSkillClsExist(skillClass) then
        local skillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(skillClass)
        local data = Skill_AllSkills.GetData(skillId)
        if data ~= nil then
            return self.m_TabBar:GetTabGoByIndex(data.SeriesIndex - 1)
        end
    end
    return nil
end

function LuaSkillPreferenceTopView:Get6thSkillItem()
    local skillClass = CLuaGuideMgr.Get6thSkillClass()
    if #self.m_ItemCells > 0 then
        for i = 1,#self.m_ItemCells do
            if self.m_ItemCells[i].ClassId == skillClass then
                return self.m_ItemCells[i].gameObject
            end
        end
    end
    return nil
end

function LuaSkillPreferenceTopView:GetHuaHunColorSkillTab()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return nil
    end

    local skillCls2Id = mainPlayer.SkillProp.SkillCls2Id
    local triggerNext = false
    local __itr_is_triger_return = nil
    local __itr_result = nil
    CommonDefs.DictIterateWithRet(skillCls2Id, DelegateFactory.DictIterFunc(function (___key, ___value)
        local item = {}
        item.Key = ___key
        item.Value = ___value
        local clsId = item.Key

        local skillId = item.Value
        if CSkillMgr.Inst:IsColorSystemBaseSkill(clsId) then
            local lv = skillId % 100
            local open = false
            Skill_ColorSkill.Foreach(function (key, val)
                if val.BaseSkill_ID == clsId then
                    if lv >= val.NeedBaseSkillLevel then
                        open = true
                    end
                end
            end)
            if open then
                local data = Skill_AllSkills.GetData(skillId)
                if data ~= nil then
                    if self.m_TabBar.SelectedIndex == data.SeriesIndex - 1 then
                        triggerNext = true
                        __itr_is_triger_return = true
                        __itr_result = nil
                        return true
                    end
                    __itr_is_triger_return = true
                    __itr_result = self.m_TabBar:GetTabGoByIndex(data.SeriesIndex - 1)
                    return true
                end
            end
        end
    end))
    if __itr_is_triger_return == true then
        if triggerNext then
            if CGuideMgr.Inst:IsInPhase(52) then
                CGuideMgr.Inst:TriggerGuide(6)
            end
            return nil
        end
        return __itr_result
    end
    CGuideMgr.Inst:EndCurrentPhase()
    return nil
end

function LuaSkillPreferenceTopView:GetHuaHunColorSkillItem()
    if CClientMainPlayer.Inst == nil then
        return nil
    end

    local skillCls2Id = CClientMainPlayer.Inst.SkillProp.SkillCls2Id
    if #self.m_ItemCells > 0  then
        for i = 1,#self.m_ItemCells do
            local clsId = self.m_ItemCells[i].ClassId
            if CommonDefs.DictContains(skillCls2Id, typeof(UInt32), clsId) then
                if CSkillMgr.Inst:IsColorSystemBaseSkill(clsId) then
                    local lv = self.m_ItemCells[i].OriginLevel
                    local open = false
                    Skill_ColorSkill.Foreach(function (key, val)
                        if val.BaseSkill_ID == clsId then
                            if lv >= val.NeedBaseSkillLevel then
                                open = true
                            end
                        end
                    end)
                    if open then
                        return self.m_ItemCells[i].gameObject
                    end
                end
            end
        end
    end
    CGuideMgr.Inst:EndCurrentPhase()
    return nil
end