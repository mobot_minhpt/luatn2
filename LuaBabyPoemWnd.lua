require("common/common_include")
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local UICommonDef = import "L10.UI.CUICommonDef"

LuaBabyPoemWnd = class()

RegistClassMember(LuaBabyPoemWnd, "m_PoemLabel")
RegistClassMember(LuaBabyPoemWnd, "m_InscribeLabel")
RegistClassMember(LuaBabyPoemWnd, "m_ShareButton")

function LuaBabyPoemWnd:Awake()
end

function LuaBabyPoemWnd:Init()
    self.m_PoemLabel = self.transform:Find("Anchor/PoemLabel"):GetComponent(typeof(UILabel))
    self.m_InscribeLabel = self.transform:Find("Anchor/InscribeLabel"):GetComponent(typeof(UILabel))
    self.m_ShareButton = self.transform:Find("Anchor/ShareButton").gameObject

    self.m_PoemLabel.text = LuaBabyMgr.m_PoemContent
    self.m_InscribeLabel.text = LuaBabyMgr.m_PoemInscribe

    UIEventListener.Get(self.m_ShareButton).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnClickShareButton(go)
        end
    )
end

function LuaBabyPoemWnd:OnClickShareButton(go)
    UICommonDef.CaptureScreenAndShare()
end

function LuaBabyPoemWnd:OnEnable()
end

function LuaBabyPoemWnd:OnDisable()
end

function LuaBabyPoemWnd:OnDestroy()
end
