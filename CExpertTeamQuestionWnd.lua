-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CExpertTeamQuestionWnd = import "L10.UI.CExpertTeamQuestionWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Object = import "UnityEngine.Object"
local Object1 = import "System.Object"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CExpertTeamQuestionWnd.m_DeleteQuestionBack_CS2LuaHook = function (this, qid) 
    if this.saveNode ~= nil then
        this.saveNode.transform.parent = nil
        Object.Destroy(this.saveNode)
        CommonDefs.DictSet(this.saveDeleteCache, typeof(UInt64), this.saveQid, typeof(Boolean), true)
        this.allQuestion_table:Reposition()
        this.allQuestion_scrollView:ResetPosition()
        this.recentQuestion_table:Reposition()
        this.recentQuestion_scrollView:ResetPosition()
    end
end
CExpertTeamQuestionWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)

    UIEventListener.Get(this.sumCheckBtn).onClick = MakeDelegateFromCSFunction(this.OpenSumInfoNode, VoidDelegate, this)

    this.allQuestion_templateNode:SetActive(false)
    this.recentQuestion_templateNode:SetActive(false)
    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
    this.tabBar:ChangeTab(0, false)
end
CExpertTeamQuestionWnd.m_OpenSumInfoNode_CS2LuaHook = function (this, go) 
    this.sumInfoNode:SetActive(true)

    local bgButton = this.sumInfoNode.transform:Find("darkbg").gameObject
    UIEventListener.Get(bgButton).onClick = DelegateFactory.VoidDelegate(function (_go) 
        this.sumInfoNode:SetActive(false)
    end)
    local label1 = CommonDefs.GetComponent_Component_Type(this.sumInfoNode.transform:Find("node/Option1/num"), typeof(UILabel))
    local label2 = CommonDefs.GetComponent_Component_Type(this.sumInfoNode.transform:Find("node/Option2/num"), typeof(UILabel))
    --UILabel label3 = sumInfoNode.transform.FindChild("node/Option3/num").GetComponent<UILabel>();
    local label4 = CommonDefs.GetComponent_Component_Type(this.sumInfoNode.transform:Find("node/Option4/num"), typeof(UILabel))
    if CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.WeekAExpireTime > CServerTimeMgr.Inst.timeStamp then
        label1.text = tostring(CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.WeekAnswerTimes)
    else
        label1.text = "0"
    end
    label2.text = tostring(CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.AcceptTimes)
    --label3.text = CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.ExpertLevel.ToString();
    label4.text = tostring((CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalScore - CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalConsumeScore))
end
CExpertTeamQuestionWnd.m_TypeChooseBtn_CS2LuaHook = function (this, go) 
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.typeArray[0], MakeDelegateFromCSFunction(this.TypeSetting, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.typeArray[1], MakeDelegateFromCSFunction(this.TypeSetting, MakeGenericClass(Action1, Int32), this), false, nil))
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), go.transform, CPopupMenuInfoMgr.AlignType.Top)
end
CExpertTeamQuestionWnd.m_TypeSetting_CS2LuaHook = function (this, row) 
    local nowChoose = this.typeArray[row]
    CommonDefs.GetComponent_Component_Type(this.allQuestion_popListBtn.transform:Find("Label"), typeof(UILabel)).text = nowChoose
    if nowChoose == LocalString.GetString("时间排序") then
        this.SortType = 1
    elseif nowChoose == LocalString.GetString("积分排序") then
        this.SortType = 2
    end
    if not System.String.IsNullOrEmpty(this.SaveTotalQuestion) then
        this:InitAllQuestionList(this.SaveTotalQuestion)
    end
end
CExpertTeamQuestionWnd.m_HotChooseBtn_CS2LuaHook = function (this, go) 
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.hotStringArray[0], MakeDelegateFromCSFunction(this.TypeHot, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.hotStringArray[1], MakeDelegateFromCSFunction(this.TypeHot, MakeGenericClass(Action1, Int32), this), false, nil))
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), go.transform, CPopupMenuInfoMgr.AlignType.Top)
end
CExpertTeamQuestionWnd.m_TypeHot_CS2LuaHook = function (this, row) 
    local nowChoose = this.hotStringArray[row]
    CommonDefs.GetComponent_Component_Type(this.allQuestion_popListBtn.transform:Find("Label"), typeof(UILabel)).text = nowChoose
    if row == 0 then
        CExpertTeamMgr.Inst:GetTopQuestion(1, MakeDelegateFromCSFunction(this.InitHotQuestionList, MakeGenericClass(Action1, String), this), nil)
    elseif row == 1 then
        CExpertTeamMgr.Inst:GetTopQuestion(2, MakeDelegateFromCSFunction(this.InitHotQuestionList, MakeGenericClass(Action1, String), this), nil)
    end
end
CExpertTeamQuestionWnd.m_SelfAnswChooseBtn_CS2LuaHook = function (this, go) 
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.selfAnswStringArray[0], MakeDelegateFromCSFunction(this.TypeSelfAnsw, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(this.selfAnswStringArray[1], MakeDelegateFromCSFunction(this.TypeSelfAnsw, MakeGenericClass(Action1, Int32), this), false, nil))
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), go.transform, CPopupMenuInfoMgr.AlignType.Top)
end
CExpertTeamQuestionWnd.m_TypeSelfAnsw_CS2LuaHook = function (this, row) 
    local nowChoose = this.selfAnswStringArray[row]
    CommonDefs.GetComponent_Component_Type(this.allQuestion_popListBtn.transform:Find("Label"), typeof(UILabel)).text = nowChoose
    if row == 0 then
        this.myAnswerShowType = 1
        CExpertTeamMgr.Inst:GetSelfAnswer(MakeDelegateFromCSFunction(this.InitSelfAnswerList, MakeGenericClass(Action1, String), this), nil)
    elseif row == 1 then
        this.myAnswerShowType = 2
        CExpertTeamMgr.Inst:GetSelfAnswer(MakeDelegateFromCSFunction(this.InitSelfAnswerList, MakeGenericClass(Action1, String), this), nil)
    end
end
CExpertTeamQuestionWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    if index == 0 then
        this.allQuestionNode:SetActive(true)
        this.recentQuestionNode:SetActive(false)
        this:InitAllQuestionView()
    elseif index == 1 then
        this.allQuestionNode:SetActive(false)
        this.recentQuestionNode:SetActive(true)
        this:InitRecentQuestionView()
    elseif index == 2 then
        this.allQuestionNode:SetActive(true)
        this.recentQuestionNode:SetActive(false)
        this:InitHotQuestionView()
    elseif index == 3 then
        this.allQuestionNode:SetActive(true)
        this.recentQuestionNode:SetActive(false)
        this:InitMyAnswerView()
    end
end
CExpertTeamQuestionWnd.m_InitAllQuestionView_CS2LuaHook = function (this) 
    this.allQuestion_refreshBtn:SetActive(true)
    CommonDefs.GetComponent_Component_Type(this.allQuestion_popListBtn.transform:Find("Label"), typeof(UILabel)).text = this.typeArray[0]
    UIEventListener.Get(this.allQuestion_popListBtn).onClick = MakeDelegateFromCSFunction(this.TypeChooseBtn, VoidDelegate, this)

    Extensions.RemoveAllChildren(this.allQuestion_table.transform)
    this.allQuestion_emptyNode:SetActive(false)
    UIEventListener.Get(this.allQuestion_refreshBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        CExpertTeamMgr.Inst:GetRandQuestion(MakeDelegateFromCSFunction(this.InitAllQuestionList, MakeGenericClass(Action1, String), this), nil)
    end)

    CExpertTeamMgr.Inst:GetRandQuestion(MakeDelegateFromCSFunction(this.InitAllQuestionList, MakeGenericClass(Action1, String), this), nil)
end
CExpertTeamQuestionWnd.m_TimeSort_CS2LuaHook = function (this, a, b) 
    local aData = TypeAs(a, typeof(MakeGenericClass(Dictionary, String, Object1)))
    local bData = TypeAs(b, typeof(MakeGenericClass(Dictionary, String, Object1)))

    local aTime = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(aData, typeof(String), "addtime")))
    local bTime = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(bData, typeof(String), "addtime")))
    return NumberCompareTo(bTime, aTime)
end
CExpertTeamQuestionWnd.m_ScoreSort_CS2LuaHook = function (this, a, b) 
    local aData = TypeAs(a, typeof(MakeGenericClass(Dictionary, String, Object1)))
    local bData = TypeAs(b, typeof(MakeGenericClass(Dictionary, String, Object1)))
    local aScore = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(aData, typeof(String), "reward_score")))
    local bScore = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(bData, typeof(String), "reward_score")))

    return NumberCompareTo(bScore, aScore)
end
CExpertTeamQuestionWnd.m_InitQuestionInfo_CS2LuaHook = function (this, data, table, scrollview, emptyNode) 
    if data ~= nil and data.Count > 0 then
        emptyNode:SetActive(false)
        do
            local i = 0
            while i < data.Count do
                local _data = TypeAs(data[i], typeof(MakeGenericClass(Dictionary, String, Object1)))
                if CommonDefs.DictContains(this.saveDeleteCache, typeof(UInt64), System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "id")))) then
                    return
                end

                local node = NGUITools.AddChild(table.gameObject, this.allQuestion_templateNode)
                node:SetActive(true)

                local playerIconNode = node.transform:Find("icon").gameObject
                CExpertTeamMgr.Inst:SetPlayerIcon(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "role_id"))), ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "avatar")), playerIconNode, nil, ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "content")), System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "id"))), 0)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("detail"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "content"))
                CommonDefs.GetComponent_Component_Type(node.transform:Find("score/num"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "reward_score"))
                CommonDefs.GetComponent_Component_Type(node.transform:Find("answer"), typeof(UILabel)).text = g_MessageMgr:FormatMessage("JINGLINGEXPERT_REPLY_AMOUNT", ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "answer_number")))

                local top = 0
                if CommonDefs.DictContains(_data, typeof(String), "top") then
                    top = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "top")))
                end
                local starNode = node.transform:Find("icon/star").gameObject
                local jinglingNode = node.transform:Find("icon/jingling").gameObject
                if top == 0 then
                    starNode:SetActive(false)
                    jinglingNode:SetActive(false)
                elseif top == 1 then
                    starNode:SetActive(true)
                    jinglingNode:SetActive(false)
                elseif top == 2 then
                    starNode:SetActive(false)
                    jinglingNode:SetActive(true)
                end

                local answerBtn = node.transform:Find("AnotherBtn").gameObject
                if CommonDefs.DictContains(_data, typeof(String), "adopt") then
                    local adoptnum = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "adopt")))
                    if adoptnum > 0 then
                        answerBtn:SetActive(false)
                    else
                        answerBtn:SetActive(true)
                    end
                else
                    answerBtn:SetActive(true)
                end
                UIEventListener.Get(answerBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                    this:OpenAnswerWnd(_data)
                end)

                local bgBtn = node.transform:Find("bgbutton").gameObject
                UIEventListener.Get(bgBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                    this:OpenDetailWnd(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "id"))))
                    this.saveNode = node
                    this.saveQid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "id")))
                end)
                i = i + 1
            end
        end

        table:Reposition()
        scrollview:ResetPosition()
    else
        emptyNode:SetActive(true)
    end
end
CExpertTeamQuestionWnd.m_InitAllQuestionList_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    this.SaveTotalQuestion = ret
    Extensions.RemoveAllChildren(this.allQuestion_table.transform)

    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object1)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    local data = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(List, Object1)))
    local noSortList = CreateFromClass(MakeGenericClass(List, Object1))
    local sortList = CreateFromClass(MakeGenericClass(List, Object1))
    CommonDefs.ListIterate(data, DelegateFactory.Action_object(function (___value) 
        local o = ___value
        local continue
        repeat
            local aData = TypeAs(o, typeof(MakeGenericClass(Dictionary, String, Object1)))
            if CommonDefs.DictContains(aData, typeof(String), "top") then
                if System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(aData, typeof(String), "top"))) > 0 then
                    CommonDefs.ListAdd(noSortList, typeof(Object1), o)
                    continue = true
                    break
                end
            end
            CommonDefs.ListAdd(sortList, typeof(Object1), o)
            continue = true
        until 1
        if not continue then
            return
        end
    end))

    if this.SortType == 1 then
        CommonDefs.ListSort1(sortList, typeof(Object1), MakeDelegateFromCSFunction(this.TimeSort, MakeGenericClass(Comparison, Object1), this))
    elseif this.SortType == 2 then
        CommonDefs.ListSort1(sortList, typeof(Object1), MakeDelegateFromCSFunction(this.ScoreSort, MakeGenericClass(Comparison, Object1), this))
    end

    CommonDefs.ListAddRange(noSortList, sortList)

    if result then
        this:InitQuestionInfo(noSortList, this.allQuestion_table, this.allQuestion_scrollView, this.allQuestion_emptyNode)
    end
end
CExpertTeamQuestionWnd.m_InitRecentQuestionView_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.recentQuestion_table.transform)
    this.recentQuestion_templateNode:SetActive(false)

    CExpertTeamMgr.Inst:GetRecentAcceptQuestion(MakeDelegateFromCSFunction(this.InitRecentQuestionList, MakeGenericClass(Action1, String), this), nil)
end
CExpertTeamQuestionWnd.m_InitAdoptInfo_CS2LuaHook = function (this, data, table, scrollview, emptyNode) 
    if data ~= nil and data.Count > 0 then
        emptyNode:SetActive(false)
        do
            local i = 0
            while i < data.Count do
                local _data = TypeAs(data[i], typeof(MakeGenericClass(Dictionary, String, Object1)))
                if CommonDefs.DictContains(this.saveDeleteCache, typeof(UInt64), System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "qid")))) then
                    return
                end
                local expertData = TypeAs(CommonDefs.DictGetValue(_data, typeof(String), "expert"), typeof(MakeGenericClass(Dictionary, String, Object1)))
                local node = NGUITools.AddChild(table.gameObject, this.recentQuestion_templateNode)
                node:SetActive(true)

                local playerIconNode = node.transform:Find("icon").gameObject
                CExpertTeamMgr.Inst:SetPlayerIcon(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "role_id"))), ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "avatar")), playerIconNode, nil, ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "content")), System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "qid"))), 0)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("detail"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "content"))
                CommonDefs.GetComponent_Component_Type(node.transform:Find("answernum/num"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "answer_number"))
                CExpertTeamMgr.SetUILabelText(CommonDefs.GetComponent_Component_Type(node.transform:Find("answer"), typeof(UILabel)), ((("[c][8AC2D8][" .. ToStringWrap(CommonDefs.DictGetValue(expertData, typeof(String), "role_name"))) .. "][-][/c][c][888B90]") .. ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "answer"))) .. "[-][/c]")

                local bgBtn = node.transform:Find("bgbutton").gameObject
                UIEventListener.Get(bgBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                    this:OpenDetailWnd(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "qid"))))
                    this.saveNode = node
                    this.saveQid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "qid")))
                end)
                i = i + 1
            end
        end

        table:Reposition()
        scrollview:ResetPosition()
    else
        emptyNode:SetActive(true)
    end
end
CExpertTeamQuestionWnd.m_InitRecentQuestionList_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    Extensions.RemoveAllChildren(this.recentQuestion_table.transform)

    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object1)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    local data = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(List, Object1)))
    if result then
        this:InitAdoptInfo(data, this.recentQuestion_table, this.recentQuestion_scrollView, this.recentQuestion_emptyNode)
    end
end
CExpertTeamQuestionWnd.m_InitHotQuestionList_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    Extensions.RemoveAllChildren(this.allQuestion_table.transform)

    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object1)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    local data = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(List, Object1)))

    if result then
        this:InitQuestionInfo(data, this.allQuestion_table, this.allQuestion_scrollView, this.allQuestion_emptyNode)
    end
end
CExpertTeamQuestionWnd.m_InitHotQuestionView_CS2LuaHook = function (this) 
    this.allQuestion_refreshBtn:SetActive(false)
    UIEventListener.Get(this.allQuestion_popListBtn).onClick = MakeDelegateFromCSFunction(this.HotChooseBtn, VoidDelegate, this)

    Extensions.RemoveAllChildren(this.allQuestion_table.transform)
    this.allQuestion_emptyNode:SetActive(false)

    CommonDefs.GetComponent_Component_Type(this.allQuestion_popListBtn.transform:Find("Label"), typeof(UILabel)).text = this.hotStringArray[0]
    --CExpertTeamMgr.Inst.GetRandQuestion(InitAllQuestionList);
    CExpertTeamMgr.Inst:GetTopQuestion(1, MakeDelegateFromCSFunction(this.InitHotQuestionList, MakeGenericClass(Action1, String), this), nil)
end
CExpertTeamQuestionWnd.m_InitSelfAnswerList_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    Extensions.RemoveAllChildren(this.allQuestion_table.transform)

    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object1)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    --List<object> data = retDic["data"] as List<object>;

    if result then
        --InitQuestionInfo(data, allQuestion_table, allQuestion_scrollView, allQuestion_emptyNode);
        local dataDic = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(Dictionary, String, Object1)))
        if this.myAnswerShowType == 1 then
            local data = TypeAs(CommonDefs.DictGetValue(dataDic, typeof(String), "no_adopt"), typeof(MakeGenericClass(List, Object1)))
            this:InitQuestionInfo(data, this.allQuestion_table, this.allQuestion_scrollView, this.allQuestion_emptyNode)
        elseif this.myAnswerShowType == 2 then
            local data = TypeAs(CommonDefs.DictGetValue(dataDic, typeof(String), "adopt"), typeof(MakeGenericClass(List, Object1)))
            this:InitAdoptInfo(data, this.allQuestion_table, this.allQuestion_scrollView, this.allQuestion_emptyNode)
        end
    end
end
CExpertTeamQuestionWnd.m_InitMyAnswerView_CS2LuaHook = function (this) 
    this.allQuestion_refreshBtn:SetActive(false)
    UIEventListener.Get(this.allQuestion_popListBtn).onClick = MakeDelegateFromCSFunction(this.SelfAnswChooseBtn, VoidDelegate, this)
    this.myAnswerShowType = 1
    CommonDefs.GetComponent_Component_Type(this.allQuestion_popListBtn.transform:Find("Label"), typeof(UILabel)).text = this.selfAnswStringArray[0]

    Extensions.RemoveAllChildren(this.allQuestion_table.transform)
    this.allQuestion_emptyNode:SetActive(false)

    CExpertTeamMgr.Inst:GetSelfAnswer(MakeDelegateFromCSFunction(this.InitSelfAnswerList, MakeGenericClass(Action1, String), this), nil)
end
