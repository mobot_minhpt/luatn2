require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture = import "L10.UI.CUITexture"
local CScene = import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Utility = import "L10.Engine.Utility"
local Time = import "UnityEngine.Time"
local CPos = import "L10.Engine.CPos"
local CTrackMgr = import "L10.Game.CTrackMgr"
local Input = import "UnityEngine.Input"
local EMinimapCategory = import "L10.UI.EMinimapCategory"
local EMinimapMarkType = import "L10.UI.EMinimapMarkType"
local NPC_NPC = import "L10.Game.NPC_NPC"
local CMiniMap = import "L10.UI.CMiniMap"
local CMiniMapMarkTemplate = import "L10.UI.CMiniMapMarkTemplate"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CMapPathDrawer = import "L10.UI.CMapPathDrawer"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"
local WndType = import "L10.UI.WndType"
local NGUIMath = import "NGUIMath"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CMiniMapRoutingWnd = import "L10.UI.CMiniMapRoutingWnd"
local NGUIText = import "NGUIText"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local LuaTweenUtils = import "LuaTweenUtils"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local Extensions = import "Extensions"
local Ease = import "DG.Tweening.Ease"
local CTooltip = import "L10.UI.CTooltip"
local AlignType = import "L10.UI.CTooltip+AlignType"

CLuaCityWarSecondaryMapWnd = class()
CLuaCityWarSecondaryMapWnd.Path = "ui/citywar/LuaCityWarSecondaryMapWnd"
CLuaCityWarSecondaryMapWnd.Multiple = 5
CLuaCityWarSecondaryMapWnd.ArrowMoveDelta = 1000
CLuaCityWarSecondaryMapWnd.ArrowMoveTime = 0.5
CLuaCityWarSecondaryMapWnd.LocateMoveTime = 0.3
CLuaCityWarSecondaryMapWnd.PlayerPrefsKey = "CityWarSecondaryMapSetting"
CLuaCityWarSecondaryMapWnd.DragMinTime = 0.05
CLuaCityWarSecondaryMapWnd.WidgetTweenTime = 0.2
CLuaCityWarSecondaryMapWnd.CityWidth = 96

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_CityId2ObjTable")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_LastSelectedCityObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_MainPlayerTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_OtherPlayerTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_TeamMemberTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_DestTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_MarkTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_BiaoCheObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_MapRootObj")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_PressBeginTime")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_IsPressed")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_PressObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_PressCityId")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_LastSelectedNpc")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_UpdateMapTick")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_PathDrawer")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_HideRootObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_InfoButtonObj")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_BiaoCheRoot")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_BiaoCheInfo")

-- Three-Level Map
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_SecondRootObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdRootObj")
-- For Tween Animation
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_SecondWidget")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdWidget")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_bThirdMap")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_SwitchBtnSprite")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdCityId2ObjTable")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdMapRootObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThumbnailTrans")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdMainPlayerTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdDestTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdEnemyPlayerTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdPathDrawer")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_SecondGateTemplateObj")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdGateTemplateObj")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_DragStartTime")

-- New Map Parameter
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_LeftBottomMapPos")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ActualMapWidth")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdUIMapWidth")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_PrimaryMapBtn")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_IsOccupyOpen")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_TipButtonObj")

RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_SecondMapCurBiaoCheTemplate")
RegistClassMember(CLuaCityWarSecondaryMapWnd, "m_ThirdMapCurBiaoCheTemplate")

function CLuaCityWarSecondaryMapWnd:IsMyMap()
	if CScene.MainScene and CScene.MainScene.SceneTemplateId == CLuaCityWarMgr.CurrentMapId then return true end
	return false
end

function CLuaCityWarSecondaryMapWnd:Init( ... )
	CUIManager.CloseUI(CLuaUIResources.CityWarPrimaryMapWnd)
	--触发引导
	CLuaGuideMgr.TryTriggerCityWarSecondaryMapGuide()
end

function CLuaCityWarSecondaryMapWnd:Awake( ... )
		self.m_IsOccupyOpen = false
    Gac2Gas.QueryCityWarOccupyOpen()

    local a, b = string.match(CityWar_Setting.GetData().MapAnglePos, "(%d+),%d+;(%d+),%d+;")
    self.m_LeftBottomMapPos = a
    self.m_ActualMapWidth = b - a
    self.m_ThirdUIMapWidth = tonumber(CityWar_Setting.GetData().ThirdMapSize)

    Gac2Gas.QueryTerritoryMapDetail(CLuaCityWarMgr.CurrentMapId)

    if self:IsMyMap() and CClientMainPlayer.Inst ~= nil then
	    Gac2Gas.QuerySceneInfo(CClientMainPlayer.Inst.EngineId)
	    Gac2Gas.QueryMMapPosInfo()
	end

    local resNameTable = {[1]="kfzcshamo", [2]="kfzclvdi", [3]="kfzcxue", [4]="kfzczhaoze", [0]="kfzcjydt"}
    local resName = resNameTable[CityWar_Map.GetData(CLuaCityWarMgr.CurrentMapId).Zone]
    self.transform:Find("BG"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/NonTransparent/Material/"..resName..".mat")
    self.m_MainPlayerTemplateObj = self.transform:Find("BG/Second/Root/MainPlayer").gameObject
    self.m_OtherPlayerTemplateObj = self.transform:Find("BG/Second/OtherPlayer").gameObject
    self.m_TeamMemberTemplateObj = self.transform:Find("BG/Second/TeamMember").gameObject
    self.m_DestTemplateObj = self.transform:Find("BG/Second/Root/Dest").gameObject
    self.m_MarkTemplateObj = self.transform:Find("BG/Second/MarkTemplate").gameObject
    self.m_MarkTemplateObj:SetActive(false)
    self.m_DestTemplateObj:SetActive(false)
    self.m_MainPlayerTemplateObj:SetActive(false)
    self.m_OtherPlayerTemplateObj:SetActive(false)
    self.m_TeamMemberTemplateObj:SetActive(false)
    self.m_MapRootObj = self.transform:Find("BG/Second/Root").gameObject
    self.m_PathDrawer = self.m_MapRootObj.transform:Find("MapPathDrawer"):GetComponent(typeof(CMapPathDrawer))
    self.m_HideRootObj = self.transform:Find("HideRoot").gameObject
    self.m_HideRootObj:SetActive(self:IsMyMap())
    self.m_ThirdGateTemplateObj = self.transform:Find("BG/Third/Map/Gate").gameObject
    self.m_ThirdGateTemplateObj:SetActive(false)
    self.m_SecondGateTemplateObj = self.transform:Find("BG/Second/Gate").gameObject
    self.m_SecondGateTemplateObj:SetActive(false)

    self.m_BiaoCheObj = self.transform:Find("BG/Second/Root/BiaoCheMark").gameObject
    self.m_BiaoCheRoot = self.transform:Find("BiaoChe").gameObject
    self.m_BiaoCheInfo = {}
    self:ParseBiaoCheInfo(0,0,0,nil)
    UIEventListener.Get(self.m_BiaoCheObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBiaoCheObjClick()
	end)

	self.m_PrimaryMapBtn = self.transform:Find("ButtonRoot/PrimaryMapBtn").gameObject
	UIEventListener.Get(self.m_PrimaryMapBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
		CLuaCityWarMgr:OpenPrimaryMap()
	end)
	UIEventListener.Get(self.transform:Find("ButtonRoot/BackCityBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestBackToOwnCity()
	end)
	UIEventListener.Get(self.transform:Find("ButtonRoot/BackServerBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestLeaveCityWarTerritory()
	end)

	UIEventListener.Get(self.transform:Find("FieldBg").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBgClick(...)
	end)

	local biaocheBtn = self.transform:Find("ButtonRoot/EscortBtn").gameObject
	UIEventListener.Get(biaocheBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBiaoCheMenuButtonClick(biaocheBtn)
	end)

	self.m_TipButtonObj = self.transform:Find("InfoButton").gameObject
	UIEventListener.Get(self.m_TipButtonObj).onClick = LuaUtils.VoidDelegate(function ()
		self:OnTipButtonClick()
	end)

	local mapData = CityWar_Map.GetData(CLuaCityWarMgr.CurrentMapId)
	if mapData then
		local colorCodeTbl = {[1] = "f4c85e", [2] = "8cb643", [3] = "8ceafb", [4] = "dacaa2", [0] = "fdea4d"}
		local nameLabel = self.transform:Find("Title/NameLabel"):GetComponent(typeof(UILabel))
		nameLabel.color = NGUIText.ParseColor24(colorCodeTbl[mapData.Zone], 0)
		nameLabel.text = mapData.Name
		local levelLabel = self.transform:Find("Title/LevelLabel"):GetComponent(typeof(UILabel))
		levelLabel.text = mapData.Level..LocalString.GetString("\n级领土")
		levelLabel.color = nameLabel.color

		local neighborTrans = self.transform:Find("Neighbor")
		local subNameLabel = neighborTrans:Find("NameLabel"):GetComponent(typeof(UILabel))
		subNameLabel.text = mapData.Name
		subNameLabel.color = NGUIText.ParseColor24(colorCodeTbl[mapData.Zone], 0)

		local neighborMapId = {mapData.EastMap, mapData.WestMap, mapData.SouthMap, mapData.NorthMap}
		local neighborTagName = {"East", "West", "South", "North"}
		for i = 1, #neighborMapId do
			local rootObj = neighborTrans:Find(neighborTagName[i]).gameObject
			if neighborMapId[i] > 0 then
				local neighborMapData = CityWar_Map.GetData(neighborMapId[i])
				local neighborNameLabel = rootObj.transform:Find("Label"):GetComponent(typeof(UILabel))
				neighborNameLabel.text = neighborMapData.Name
				neighborNameLabel.color = NGUIText.ParseColor24(colorCodeTbl[neighborMapData.Zone], 0)
			else
				rootObj:SetActive(false)
			end
		end
	end

	self:InitMap()
	self:InitThirdMap()

	self:InitMainPlayer()
	self:InitThirdMapMainPlayer()

	self:CancelMapTick()

	if self:IsMyMap() then
		self.m_UpdateMapTick = RegisterTick(function ( ... )
			self:UpdateMapTick()
		end, 1000)
		self:UpdateMapTick()
		self.m_InfoButtonObj = self.transform:Find("HideRoot/InfoButton").gameObject
		UIEventListener.Get(self.m_InfoButtonObj).onClick = LuaUtils.VoidDelegate(function (go)
			self:OnInfoBtnClick(go)
		end)
		self:OnInfoBtnClick(nil)
		UIEventListener.Get(self.transform:Find("HideRoot/RoutingButton").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
			CUIManager.ShowUI(CUIResources.MiniMapRoutingWnd)
			CMiniMapRoutingWnd.RouteTo = DelegateFactory.Func_int_int_bool(function (x, y)
				self:Track(CPos(x, y))
				return true
			end)
		end)

		self:InitGate()
	end
end

function CLuaCityWarSecondaryMapWnd:OnTipButtonClick( ... )
	local str = self.m_IsOccupyOpen and g_MessageMgr:FormatMessage("KFCZ_KAIZHAN_MAP_INTERFACE_TIP") or g_MessageMgr:FormatMessage("KFCZ_XIUZHAN_MAP_INTERFACE_TIP")
	CTooltip.Show(str, self.m_TipButtonObj.transform, AlignType.Bottom)
end

function CLuaCityWarSecondaryMapWnd:InitGate( ... )
	if not CLuaCityWarMgr:IsOwnCurrentCity() then return end
    CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
		local unitData = ___value
        if CityWar_Unit.GetData(unitData.TemplateId).Type == 3 then
        	local secondObj = NGUITools.AddChild(self.m_MapRootObj, self.m_SecondGateTemplateObj)
        	secondObj:SetActive(true)
        	secondObj.transform.localPosition = self:GetSecondMapUIPos(unitData.ServerPos.x, unitData.ServerPos.z)
        	Extensions.SetLocalRotationZ(secondObj.transform, unitData.ServerRotateY + 90)
        	local thirdObj = NGUITools.AddChild(self.m_ThirdMapRootObj, self.m_ThirdGateTemplateObj)
        	thirdObj:SetActive(true)
        	thirdObj.transform.localPosition = self:GetThirdMapUIPos(unitData.ServerPos.x, unitData.ServerPos.z)
        	Extensions.SetLocalRotationZ(thirdObj.transform, unitData.ServerRotateY + 90)
        end
	end))
end

function CLuaCityWarSecondaryMapWnd:OnBgClick( ... )
	local worldPos = CUIManager.instance.MainCamera:ScreenToWorldPoint(Input.mousePosition)
	local deltaPos = self.m_MapRootObj.transform:InverseTransformPoint(worldPos)
	local gridPos = CPos(deltaPos.x * self.m_ActualMapWidth / 1000 + self.m_LeftBottomMapPos, deltaPos.y * self.m_ActualMapWidth / 1000 + self.m_LeftBottomMapPos)
	self:Track(gridPos)
end

function CLuaCityWarSecondaryMapWnd:Track(gridPos)
	if not CLuaCityWarMgr:CanTrack() then return end

	local pixelPos = Utility.GridPos2PixelPos(gridPos)
	CTrackMgr.Inst:Track(nil, CLuaCityWarMgr.CurrentMapId, pixelPos, Utility.Grid2Pixel(2.0), nil, nil, nil, nil, true)
	self.m_DestTemplateObj:SetActive(true)
	self.m_DestTemplateObj.transform.localPosition = self:GetSecondMapUIPos(gridPos.x, gridPos.y)

	--Three-Level Map
	self.m_ThirdDestTemplateObj:SetActive(true)
	self.m_ThirdDestTemplateObj.transform.localPosition = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
end

function CLuaCityWarSecondaryMapWnd:UpdateMapPath(playerMapPos)
	local anyKey = CommonDefs.DEVICE_PLATFORM == "pc" and Input.anyKey or false
	if anyKey or (not CLuaCityWarMgr:CanTrack()) then
 		self.m_PathDrawer:ClearCurrentPath()
 		self.m_DestTemplateObj:SetActive(false)
		return
	end
	local movePath = CClientMainPlayer.Inst.EngineObject.CurrentMovePath
    local wayPoint = CClientMainPlayer.Inst.EngineObject.WayPoint
    if movePath ~= nil then
        local listPos = CreateFromClass(MakeGenericClass(List, Vector3))
        do
            local i = wayPoint
            while i < movePath.PathSize do
                local tmp1 = i
                local tmp2 = 0
                local default
                default, tmp1, tmp2 = movePath:GetPixelPosAt(tmp1, tmp2)
                local pixelPos = default
                local gridPos = Utility.PixelPos2GridPos(pixelPos)
                local mapPos = self:GetSecondMapUIPos(gridPos.x, gridPos.y)
                CommonDefs.ListAdd(listPos, typeof(Vector3), mapPos)
                i = i + 1
            end
        end
        local endPos = Utility.PixelPos2GridPos(CTrackMgr.Inst:GetCurSceneEndPos())
        self.m_DestTemplateObj:SetActive(true)
		self.m_DestTemplateObj.transform.localPosition = self:GetSecondMapUIPos(endPos.x, endPos.y)
        CommonDefs.ListAdd(listPos, typeof(Vector3), self:GetSecondMapUIPos(endPos.x, endPos.y))
        self.m_PathDrawer:UpdatePlayerPath(listPos, playerMapPos)
 	else
 		self.m_DestTemplateObj:SetActive(false)
    end
end

function CLuaCityWarSecondaryMapWnd:InitMainPlayer( ... )
	if CScene.MainScene and CScene.MainScene.SceneTemplateId == CLuaCityWarMgr.CurrentMapId and CClientMainPlayer.Inst then
		--local obj = NGUITools.AddChild(self.m_MapRootObj, self.m_MainPlayerTemplateObj)
		--obj:SetActive(true)
		self.m_MainPlayerTemplateObj:SetActive(true)
		local gridPos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
		local playerMapPos = self:GetSecondMapUIPos(gridPos.x, gridPos.y)
		self.m_MainPlayerTemplateObj.transform.localPosition = playerMapPos
		local x, y = CClientMainPlayer.Inst.RO.transform.forward.x, CClientMainPlayer.Inst.RO.transform.forward.z

		local angle = math.atan2(y, x) * 180 / math.pi
		self.m_MainPlayerTemplateObj.transform.localEulerAngles = Vector3(0, 0, angle)
		self:UpdateMapPath(playerMapPos)
	end
end

function CLuaCityWarSecondaryMapWnd:GetSecondMapUIPos(x, y)
	return Vector3((x - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * 1000, (y - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * 1000, 0)
end

function CLuaCityWarSecondaryMapWnd:GetThirdMapUIPos(x, y)
	return Vector3((x - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * self.m_ThirdUIMapWidth, (y - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * self.m_ThirdUIMapWidth, 0)
end

function CLuaCityWarSecondaryMapWnd:InitMap( ... )
	local cityTemplateObj = self.transform:Find("BG/Second/City").gameObject
	--Resize city template
	local width = CLuaCityWarSecondaryMapWnd.CityWidth / self.m_ActualMapWidth * 1000
	local bg1 = cityTemplateObj:GetComponent(typeof(UISprite))
	local bg2 = cityTemplateObj.transform:Find("bg"):GetComponent(typeof(UISprite))
	bg1.width = width
	bg1.height = width
	bg2.width = width
	bg2.height = width

	local lefttop = Vector3(-width / 2 + 25, width / 2 - 25, 0)
	local fightingTrans = cityTemplateObj.transform:Find("Fighting")
	fightingTrans.localPosition = lefttop
	local protectedTrans = cityTemplateObj.transform:Find("Protected")
	protectedTrans.localPosition = lefttop

	width = width + 5
	local selectedSprite = cityTemplateObj.transform:Find("Selected"):GetComponent(typeof(UISprite))
	selectedSprite.width = width
	selectedSprite.height = width
	width = width + 20
	local majorcitySprite = cityTemplateObj.transform:Find("MajorFlag"):GetComponent(typeof(UISprite))
	majorcitySprite.width = width
	majorcitySprite.height = width

	cityTemplateObj:SetActive(false)

	self.m_CityId2ObjTable = {}
	CityWar_MapCity.Foreach(function(k, v)
		if v.MapID == CLuaCityWarMgr.CurrentMapId then
			local obj = NGUITools.AddChild(self.m_MapRootObj, cityTemplateObj)
			obj:SetActive(true)
			local x, y, w, l = string.match(v.Rect, "(%d+),(%d+),(%d+),(%d+)")
			obj.transform.localPosition = self:GetSecondMapUIPos(x + (w - x) / 2, y + (l - y) / 2)
			local childName = {"Selected", "Occupied", "MyGuild", "Fighting", "Protected"}
			for j = 1, #childName do
				obj.transform:Find(childName[j]).gameObject:SetActive(false)
			end

			local isMajorCity = CLuaCityWarMgr:IsMajorCity(k)
			obj.transform:Find("MajorFlag").gameObject:SetActive(isMajorCity)

			UIEventListener.Get(obj).onPress = LuaUtils.BoolDelegate(function (go, isPressed)
				self:OnPress(go, isPressed, k)
			end)
			self.m_CityId2ObjTable[k] = obj
		end
	end)
end

function CLuaCityWarSecondaryMapWnd:OnPress(go, isPressed, cityId)
	self.m_IsPressed = isPressed
	self.m_PressObj = go
	self.m_PressCityId = cityId
	if isPressed then self.m_PressBeginTime = Time.realtimeSinceStartup return end

	local deltaTime = Time.realtimeSinceStartup - self.m_PressBeginTime
	if deltaTime <= 0.2 then
		self:OnCityClick(go, cityId)
	end
end

function CLuaCityWarSecondaryMapWnd:Update( ... )
	if self.m_IsPressed then
		local deltaTime = Time.realtimeSinceStartup - self.m_PressBeginTime
		if deltaTime > 0.2 then
			self:OnCityPress(self.m_PressObj, self.m_PressCityId)
			self.m_IsPressed = false
			return
		end
	end
end

function CLuaCityWarSecondaryMapWnd:OnCityClick(go, cityId)
	--local x, y, w, l = string.match(CityWar_MapCity.GetData(cityId).Rect, "(%d+),(%d+),(%d+),(%d+)")
	--self:Track(CPos(x + (w - x) / 2, y + (l - y) / 2))
	self:OnBgClick(nil)
end

function CLuaCityWarSecondaryMapWnd:OnCityPress(go, cityId)
	if self.m_LastSelectedCityObj then
		self.m_LastSelectedCityObj.transform:Find("Selected").gameObject:SetActive(false)
	end
	self.m_LastSelectedCityObj = go
	go.transform:Find("Selected").gameObject:SetActive(true)
	Gac2Gas.QueryTerritoryCityTips(CLuaCityWarMgr.CurrentMapId, cityId)
end

function CLuaCityWarSecondaryMapWnd:QueryTerritoryMapDetailResult(mapId, guildCityTemplateId, mapDetailUD)
	if mapId ~= CLuaCityWarMgr.CurrentMapId then return end

	local guildCityData = guildCityTemplateId > 0 and CityWar_MapCity.GetData(guildCityTemplateId)
	CLuaCityWarMgr.MapInfoMyGuildMapId =  guildCityData and guildCityData.MapID or 0
	CLuaCityWarMgr.MapInfoMyGuildCityId = guildCityTemplateId

	local dict = MsgPackImpl.unpack(mapDetailUD)
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key,val)
		local cityId = tonumber(key)
		self:UpdateCityStatus(self.m_CityId2ObjTable[cityId], guildCityTemplateId, cityId, val[0], val[1], val[2])
		self:UpdateCityStatus(self.m_ThirdCityId2ObjTable[cityId], guildCityTemplateId, cityId, val[0], val[1], val[2])
	end))
end

function CLuaCityWarSecondaryMapWnd:UpdateCityStatus(cityObj, guildCityTemplateId, cityId, occupied, fighting, protected)
	if cityObj then
		if cityId == guildCityTemplateId then
			local myGuildObj = cityObj.transform:Find("MyGuild").gameObject
			myGuildObj:SetActive(true)
			if not CLuaCityWarMgr:IsMajorCity(cityId) then
				myGuildObj:GetComponent(typeof(UISprite)).spriteName = "citywarprimarymapwnd_chengchiweizhi_02"
			end
		end
		cityObj.transform:Find("Occupied").gameObject:SetActive(occupied > 0 and cityId ~= guildCityTemplateId)
		local fightingTrans = cityObj.transform:Find("Fighting")
		fightingTrans.gameObject:SetActive(fighting > 0)
		if fighting == 2 then
			fightingTrans:GetComponent(typeof(UISprite)).spriteName = "guanningwushen_02"
		end
		cityObj.transform:Find("Protected").gameObject:SetActive(protected > 0)
	end
end

function CLuaCityWarSecondaryMapWnd:UpdateObjPosInfo(argv)
	if not CScene.MainScene or CScene.MainScene.SceneTemplateId ~= CLuaCityWarMgr.CurrentMapId then return end

	local playerList = argv[0]
	local mainplayerEngineId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.EngineId or 0
	for i = 0, playerList.Count - 1, 6 do
		if playerList[i + 1] ~= mainplayerEngineId then
			local tempalteObj = playerList[i + 4] == true and self.m_TeamMemberTemplateObj or self.m_OtherPlayerTemplateObj
			local obj = NGUITools.AddChild(self.m_MapRootObj, tempalteObj)
			obj:SetActive(true)
			obj.transform.localPosition = self:GetSecondMapUIPos(playerList[i + 2], playerList[i + 3])

			--Three-Level Map
			local objj = NGUITools.AddChild(self.m_ThirdMapRootObj, tempalteObj)
			objj:SetActive(true)
			objj.transform.localPosition = self:GetThirdMapUIPos(playerList[i + 2], playerList[i + 3])
		end
	end
end

function CLuaCityWarSecondaryMapWnd:UpdateNpcInfo(argv)
	if not CScene.MainScene or CScene.MainScene.SceneTemplateId ~= CLuaCityWarMgr.CurrentMapId then return end

	local npcList = argv[0]
	for i = 0, npcList.Count - 1, 4 do
		local templateId = npcList[i + 1]
		local data = NPC_NPC.GetData(templateId)
		if data ~= nil and data.HideInMap <= 0 then
			local obj = NGUITools.AddChild(self.m_MapRootObj, self.m_MarkTemplateObj)
			obj:SetActive(true)
			obj.transform.localPosition = self:GetSecondMapUIPos(npcList[i + 2], npcList[i + 3])
			local template = obj:GetComponent(typeof(CMiniMapMarkTemplate))
			if template ~= nil then
				template.EngineId = npcList[i]
				local pos = CPos(npcList[i + 2], npcList[i + 3])
				template:UpdateView(EMinimapCategory.Normal, data.IsConcerned > 0 and EMinimapMarkType.Npc_Concerned or EMinimapMarkType.Npc_NotConcerned, data.Name, "")
				template.OnButtonSelected_02 = DelegateFactory.Action_QnSelectableButton_bool(function (btn, selected)
					if selected then
						if self.m_LastSelectedNpc then self.m_LastSelectedNpc:SetSelected(false) end
						self.m_LastSelectedNpc = template
						if CLuaCityWarMgr:CanTrack() then
							local pixelPos = Utility.GridPos2PixelPos(pos)
							CTrackMgr.Inst:Track(nil, CLuaCityWarMgr.CurrentMapId, pixelPos, 0, DelegateFactory.Action(function ( ... )
								local npc = CClientObjectMgr.Inst:GetObject(template.EngineId)
								if npc then npc:OnSelected() end
							end), nil, nil, nil, true)
						end
					end
				end)
			end
		end
	end
end

function CLuaCityWarSecondaryMapWnd:OnInfoBtnClick(go)
	local contentColor = 4294967295
    --白色
    if CScene.MainScene ~= nil then
        local data = PublicMap_PublicMap.GetData(CScene.MainScene.SceneTemplateId)
        local tips = CreateFromClass(MakeGenericClass(List, String))
        if data ~= nil then
            if data.AllowPK then
                if CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst) then
                    contentColor = 4294967295
                    CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
                else
                    CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("可进行PK战斗"))
                    contentColor = 4278190335
                    --红色
                    if data.EnablePK then
                        CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("恶意杀人增加PK值"))
                        local default
                        if data.IsDeathPunish then
                            default = LocalString.GetString("死亡有惩罚")
                        else
                            default = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(tips, typeof(String), default)
                    else
                        CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("恶意杀人不增加PK值"))
                        local extern
                        if data.IsDeathPunish then
                            extern = LocalString.GetString("死亡有惩罚")
                        else
                            extern = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(tips, typeof(String), extern)
                    end
                    local ref
                    if data.CanDuoHun then
                        ref = LocalString.GetString("可夺魂")
                    else
                        ref = LocalString.GetString("不可夺魂")
                    end
                    CommonDefs.ListAdd(tips, typeof(String), ref)
                end
            else
                contentColor = 4294967295
                --白色
                CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
            end
        end
        if (data.DecPK and not CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst)) or CScene.MainScene.SceneTemplateId == 16100021 then
            CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("在此区域活动可减少PK值"))
        else
            CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("此区域活动无法减少PK值"))
        end
        CommonDefs.GetComponent_Component_Type(self.m_InfoButtonObj.transform:GetChild(0), typeof(UISprite)).color = NGUIMath.HexToColor(contentColor)
        --只有点击真正点击调用的才弹出窗口
        if go ~= nil then
            CMessageTipMgr.Inst:Display(WndType.Tooltip, true, LocalString.GetString("当前场景须知"), tips, 400, go.transform, AlignType.Top, contentColor)
        end
    end
end

function CLuaCityWarSecondaryMapWnd:QueryCityInfoResult( ... )
	CUIManager.ShowUI(CLuaUIResources.CityMapInfoTip)
end

function CLuaCityWarSecondaryMapWnd:MainPlayerMoveStepped( ... )
	self:InitMainPlayer()
	self:InitThirdMapMainPlayer()
end

function CLuaCityWarSecondaryMapWnd:MainPlayerMoveEnded( ... )
	self.m_PathDrawer:ClearCurrentPath()
 	self.m_DestTemplateObj:SetActive(false)
 	self.m_ThirdPathDrawer:ClearCurrentPath()
 	self.m_ThirdDestTemplateObj:SetActive(false)
end

function CLuaCityWarSecondaryMapWnd:QueryCityWarOccupyOpenResult( bOpen )
	self.m_IsOccupyOpen = bOpen
	self:OnTipButtonClick()
end

function CLuaCityWarSecondaryMapWnd:UpdateYayunBiaoCheMapPos(posData)
	if self.m_SecondMapCurBiaoCheTemplate ~= nil then
        self.m_SecondMapCurBiaoCheTemplate:SetActive(false)
    end
    if self.m_ThirdMapCurBiaoCheTemplate ~= nil then
        self.m_ThirdMapCurBiaoCheTemplate:SetActive(false)
    end

    if posData ~= nil and posData.Count > 0 then
		local category = CMiniMap.GetMinimapCategory()
        local engineId = math.floor(tonumber(posData[0] or 0))
        local x = math.floor(tonumber(posData[1] or 0))
        local z = math.floor(tonumber(posData[2] or 0))
        if engineId<=0 then return end

        if self.m_SecondMapCurBiaoCheTemplate == nil then
        	self.m_SecondMapCurBiaoCheTemplate = NGUITools.AddChild(self.m_MapRootObj, self.m_MarkTemplateObj)
        end
        self:InitCurrentBiaoCheMarkTemplate(self.m_SecondMapCurBiaoCheTemplate,self:GetSecondMapUIPos(x, z), engineId, category)

        if self.m_ThirdMapCurBiaoCheTemplate == nil then
        	self.m_ThirdMapCurBiaoCheTemplate = NGUITools.AddChild(self.m_ThirdMapRootObj, self.m_MarkTemplateObj)
        end
        self:InitCurrentBiaoCheMarkTemplate(self.m_ThirdMapCurBiaoCheTemplate,self:GetThirdMapUIPos(x, z), engineId, category)
    end
end

function CLuaCityWarSecondaryMapWnd:InitCurrentBiaoCheMarkTemplate(obj, mapUIPos, engineId, category)
	obj:SetActive(true)
	obj.transform.localPosition = mapUIPos
	local template = obj:GetComponent(typeof(CMiniMapMarkTemplate))
	if template ~= nil then
		template.EngineId = engineId
		local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
		local name = clientObj and clientObj.Name or ""
		template:UpdateView(category, EMinimapMarkType.Monster_Friend, name, "")
	end
end

function CLuaCityWarSecondaryMapWnd:UpdateMapTick()
	--刷新跟随的镖车位置
    if CLuaCityWarMgr.MainPlayerBiaoCheIsValidForCityWarMap() then
    	Gac2Gas.QueryFollwingBiaoChePosOnMMap()
    else
    	if self.m_SecondMapCurBiaoCheTemplate ~= nil then
        	self.m_SecondMapCurBiaoCheTemplate:SetActive(false)
	    end
	    if self.m_ThirdMapCurBiaoCheTemplate ~= nil then
	        self.m_ThirdMapCurBiaoCheTemplate:SetActive(false)
	    end
    end
end

function CLuaCityWarSecondaryMapWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QueryTerritoryMapDetailResult", self, "QueryTerritoryMapDetailResult")
	g_ScriptEvent:AddListener("QueryCityInfoResult", self, "QueryCityInfoResult")
	g_ScriptEvent:AddListener("UpdateObjPosInfo", self, "UpdateObjPosInfo")
	g_ScriptEvent:AddListener("UpdateNpcInfo", self, "UpdateNpcInfo")
	g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "MainPlayerMoveStepped")
	g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
	g_ScriptEvent:AddListener("SyncBiaoCheNpcPos", self, "SyncBiaoCheNpcPos")
	g_ScriptEvent:AddListener("QueryEnemyPlayerPosResult", self, "QueryEnemyPlayerPosResult")
	g_ScriptEvent:AddListener("QueryCityWarOccupyOpenResult", self, "QueryCityWarOccupyOpenResult")
	g_ScriptEvent:AddListener("UpdateYayunBiaoCheMapPos", self, "UpdateYayunBiaoCheMapPos")
	Gac2Gas.QueryBiaoChePosOnMap()
end

function CLuaCityWarSecondaryMapWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QueryTerritoryMapDetailResult", self, "QueryTerritoryMapDetailResult")
	g_ScriptEvent:RemoveListener("QueryCityInfoResult", self, "QueryCityInfoResult")
	g_ScriptEvent:RemoveListener("UpdateObjPosInfo", self, "UpdateObjPosInfo")
	g_ScriptEvent:RemoveListener("UpdateNpcInfo", self, "UpdateNpcInfo")
	g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "MainPlayerMoveStepped")
	g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
	g_ScriptEvent:RemoveListener("SyncBiaoCheNpcPos", self, "SyncBiaoCheNpcPos")
	g_ScriptEvent:RemoveListener("QueryEnemyPlayerPosResult", self, "QueryEnemyPlayerPosResult")
	g_ScriptEvent:RemoveListener("QueryCityWarOccupyOpenResult", self, "QueryCityWarOccupyOpenResult")
	g_ScriptEvent:RemoveListener("UpdateYayunBiaoCheMapPos", self, "UpdateYayunBiaoCheMapPos")
end

function CLuaCityWarSecondaryMapWnd:OnDestroy( ... )
	self:CancelMapTick()
end

function CLuaCityWarSecondaryMapWnd:CancelMapTick( ... )
	if self.m_UpdateMapTick then
		UnRegisterTick(self.m_UpdateMapTick)
		self.m_UpdateMapTick = nil
	end
end

function CLuaCityWarSecondaryMapWnd:SyncBiaoCheNpcPos(npcTemplateId, x, y, biaoCheInfoUd)
	self:ParseBiaoCheInfo(npcTemplateId, x, y, biaoCheInfoUd)

end

function CLuaCityWarSecondaryMapWnd:ParseBiaoCheInfo(npcTemplateId, x, y, biaoCheInfoUd)
	if biaoCheInfoUd then
		local data = MsgPackImpl.unpack(biaoCheInfoUd)
		if data and data.Count >= 7 then
			self.m_BiaoCheInfo.biaoCheFMapId = tonumber(data[0])
			self.m_BiaoCheInfo.biaoCheSMapId = tonumber(data[1])
			self.m_BiaoCheInfo.biaoCheFProgress = tonumber(data[2])
			self.m_BiaoCheInfo.biaoCheSProgress = tonumber(data[3])
			self.m_BiaoCheInfo.biaoCheFStatus = tonumber(data[4])
			self.m_BiaoCheInfo.biaoCheSStatus = tonumber(data[5])
			self.m_BiaoCheInfo.biaoCheCloseTime = tonumber(data[6])
			self.m_BiaoCheInfo.biaoCheTemplateId = npcTemplateId
			self.m_BiaoCheInfo.biaoCheGridPosX = x
			self.m_BiaoCheInfo.biaoCheGridPosY = y
		end
	end
	local mapId = 0
	local progress = 0
	local status = 0
	local closeTime = 0
	if self.m_BiaoCheInfo then
		if self.m_BiaoCheInfo.biaoCheFMapId and CLuaCityWarMgr.CurrentMapId ==  self.m_BiaoCheInfo.biaoCheFMapId then
			mapId = self.m_BiaoCheInfo.biaoCheFMapId
			progress = self.m_BiaoCheInfo.biaoCheFProgress
			status = self.m_BiaoCheInfo.biaoCheFStatus
		elseif self.m_BiaoCheInfo.biaoCheSMapId and CLuaCityWarMgr.CurrentMapId ==  self.m_BiaoCheInfo.biaoCheSMapId then
			mapId = self.m_BiaoCheInfo.biaoCheSMapId
			progress = self.m_BiaoCheInfo.biaoCheSProgress
			status = self.m_BiaoCheInfo.biaoCheSStatus
		end
		closeTime = self.m_BiaoCheInfo.biaoCheCloseTime
	end
	if CLuaCityWarMgr.CurrentMapId == mapId and self:IsMyMap() then
		--仅玩家当前所在的地图上，才显示镖车位置
		self.m_BiaoCheObj:SetActive(true)
		self.m_BiaoCheObj.transform.localPosition = self:GetSecondMapUIPos(x, y)
	else
		self.m_BiaoCheObj:SetActive(false)
	end

	self:SetBiaoCheAppearance(mapId, progress, status, closeTime)
end

function CLuaCityWarSecondaryMapWnd:OnBiaoCheObjClick()
	if not self.m_BiaoCheInfo then return end
	local mapId = nil
	if self.m_BiaoCheInfo.biaoCheFMapId and CLuaCityWarMgr.CurrentMapId ==  self.m_BiaoCheInfo.biaoCheFMapId then
		mapId = self.m_BiaoCheInfo.biaoCheFMapId
	elseif self.m_BiaoCheInfo.biaoCheSMapId and CLuaCityWarMgr.CurrentMapId ==  self.m_BiaoCheInfo.biaoCheSMapId then
		mapId = self.m_BiaoCheInfo.biaoCheSMapId
	end
	if CLuaCityWarMgr.CurrentMapId == mapId and self:IsMyMap() then
		CTrackMgr.Inst:FindNPC(self.m_BiaoCheInfo.biaoCheTemplateId or 0, mapId, self.m_BiaoCheInfo.biaoCheGridPosX or 0, self.m_BiaoCheInfo.biaoCheGridPosY or 0, nil, nil)
	end
end

function CLuaCityWarSecondaryMapWnd:SetBiaoCheAppearance(mapId, progress, status, closeTime)
	if mapId and CityWar_Map.Exists(mapId) and CLuaCityWarMgr.CurrentMapId == mapId then
		self.m_BiaoCheRoot:SetActive(true)
		local script = self.m_BiaoCheRoot:GetComponent(typeof(CCommonLuaScript))
		script.m_LuaSelf:Init(mapId, progress, status, closeTime)
	else
		self.m_BiaoCheRoot:SetActive(false)
	end
end

function CLuaCityWarSecondaryMapWnd:OnBiaoCheMenuButtonClick(go)
	CLuaCityWarMgr.ShowBiaoCheMenu(go.transform.position,
		go:GetComponent(typeof(UIWidget)).width,
		go:GetComponent(typeof(UIWidget)).height,
		self.m_BiaoCheInfo)
end


---------- Three-Level Map Begin
function CLuaCityWarSecondaryMapWnd:InitThirdMap( ... )
    self.m_ThirdMainPlayerTemplateObj = self.transform:Find("BG/Third/Map/Root/MainPlayer").gameObject
    self.m_ThirdDestTemplateObj = self.transform:Find("BG/Third/Map/Root/Dest").gameObject
    self.m_ThirdEnemyPlayerTemplateObj = self.transform:Find("BG/Third/Map/EnemyPlayer").gameObject
    self.m_ThirdDestTemplateObj:SetActive(false)
    self.m_ThirdMainPlayerTemplateObj:SetActive(false)
    self.m_ThirdEnemyPlayerTemplateObj:SetActive(false)
    self.m_ThirdPathDrawer = self.transform:Find("BG/Third/Map/Root/MapPathDrawer"):GetComponent(typeof(CMapPathDrawer))

	self.m_SecondRootObj = self.transform:Find("BG/Second").gameObject
	self.m_ThirdRootObj = self.transform:Find("BG/Third").gameObject
	self.m_SecondRootObj:SetActive(true)
	self.m_ThirdRootObj:SetActive(true)
	self.m_SecondWidget = self.m_SecondRootObj:GetComponent(typeof(UIWidget))
	self.m_ThirdWidget = self.m_ThirdRootObj:GetComponent(typeof(UIWidget))

	self.m_SwitchBtnSprite = self.transform:Find("SwitchMapBtn"):GetComponent(typeof(UISprite))
	local transTbl = {"LeftArrow", "RightArrow", "UpArrow", "DownArrow"}
	local moveDeltaTbl = {{1, 0}, {-1, 0}, {0, -1}, {0, 1}}
	for i = 1, #transTbl do
		UIEventListener.Get(self.m_ThirdRootObj.transform:Find("Map/Operation/"..transTbl[i]).gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:ThirdMapMove(moveDeltaTbl[i][1] * CLuaCityWarSecondaryMapWnd.ArrowMoveDelta, moveDeltaTbl[i][2] * CLuaCityWarSecondaryMapWnd.ArrowMoveDelta, CLuaCityWarSecondaryMapWnd.ArrowMoveTime)
		end)
	end

	local touchObj = self.m_ThirdRootObj.transform:Find("Map/TouchBg").gameObject
	UIEventListener.Get(touchObj).onDragStart = LuaUtils.VoidDelegate(function ( ... )
		self.m_DragStartTime = Time.realtimeSinceStartup
	end)
	UIEventListener.Get(touchObj).onDrag = LuaUtils.VectorDelegate(function (go, delta)
		if Time.realtimeSinceStartup - self.m_DragStartTime < CLuaCityWarSecondaryMapWnd.DragMinTime then return end
		self:ThirdMapMove(delta.x, delta.y, 0)
	end)
	UIEventListener.Get(self.m_ThirdRootObj.transform:Find("Map/TouchBg").gameObject).onClick = LuaUtils.VoidDelegate(function (...)
		self:ThirdMapClick()
	end)
	self.m_ThumbnailTrans = self.m_ThirdRootObj.transform:Find("Map/Thumbnail/CityRoot/Scope")

	self.m_bThirdMap = false
	self.m_SwitchBtnSprite.spriteName = self.m_bThirdMap and "common_icon_suoxiao" or "common_icon_fangda"

	UIEventListener.Get(self.m_SwitchBtnSprite.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:SwitchMap(not self.m_bThirdMap, true)
	end)
	self:SwitchMap(PlayerPrefs.GetInt(CLuaCityWarSecondaryMapWnd.PlayerPrefsKey, 0) > 0, false)

	UIEventListener.Get(self.transform:Find("BG/Third/LocateMeBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:LocateMe(CLuaCityWarSecondaryMapWnd.LocateMoveTime)
	end)

	local cityTemplateObj = self.transform:Find("BG/Third/Map/City").gameObject
	--Resize city template
	local width = CLuaCityWarSecondaryMapWnd.CityWidth / self.m_ActualMapWidth * self.m_ThirdUIMapWidth
	local bg1 = cityTemplateObj:GetComponent(typeof(UISprite))
	local bg2 = cityTemplateObj.transform:Find("Sprite"):GetComponent(typeof(UISprite))
	bg1.width = width
	bg1.height = width
	bg2.width = width
	bg2.height = width

	local lefttop = Vector3(-width / 2 + 25, width / 2 - 25, 0)
	local fightingTrans = cityTemplateObj.transform:Find("Fighting")
	fightingTrans.localPosition = lefttop
	local protectedTrans = cityTemplateObj.transform:Find("Protected")
	protectedTrans.localPosition = lefttop

	width = width + 25
	local majorcitySprite = cityTemplateObj.transform:Find("MajorFlag/Sprite"):GetComponent(typeof(UISprite))
	majorcitySprite.width = width
	majorcitySprite.height = width
	cityTemplateObj:SetActive(false)
	self.m_ThirdMapRootObj = self.transform:Find("BG/Third/Map/Root").gameObject

	self.m_ThirdCityId2ObjTable= {}
	CityWar_MapCity.Foreach(function(k, v)
		if v.MapID == CLuaCityWarMgr.CurrentMapId then
			local obj = NGUITools.AddChild(self.m_ThirdMapRootObj, cityTemplateObj)
			obj:SetActive(true)
			local x, y, w, l = string.match(v.Rect, "(%d+),(%d+),(%d+),(%d+)")
			obj.transform.localPosition = self:GetThirdMapUIPos((x + (w - x) / 2), (y + (l - y) / 2))
			local childName = {"Occupied", "MyGuild", "Fighting", "Protected"}
			for j = 1, #childName do
				obj.transform:Find(childName[j]).gameObject:SetActive(false)
			end
			obj.transform:Find("Name"):GetComponent(typeof(UILabel)).text = v.Name

			local isMajorCity = CLuaCityWarMgr:IsMajorCity(k)
			obj.transform:Find("MajorFlag").gameObject:SetActive(isMajorCity)

			self.m_ThirdCityId2ObjTable[k] = obj
		end
	end)
	self:LocateMe(0)
end

function CLuaCityWarSecondaryMapWnd:TweenAlpha(widget, startVal, endVal, time)
	local tweener = LuaTweenUtils.TweenFloat(startVal, endVal, time, function ( val )
		widget.alpha = val
	end)
	LuaTweenUtils.SetEase(tweener, Ease.InOutQuint)
end

function CLuaCityWarSecondaryMapWnd:TweenScale(trans, startVal, endVal, time)
	local tweener = LuaTweenUtils.TweenScale(trans, startVal, endVal, time)
	LuaTweenUtils.SetEase(tweener, Ease.InOutQuint)
end

function CLuaCityWarSecondaryMapWnd:SwitchMap(bThirdMap, bAnimation)
	self.m_bThirdMap = bThirdMap
	local time = bAnimation and CLuaCityWarSecondaryMapWnd.WidgetTweenTime or 0

	self.m_ThirdRootObj:SetActive(bThirdMap)
	self.m_SecondRootObj:SetActive(not bThirdMap)

	if bThirdMap then
		self:TweenAlpha(self.m_ThirdWidget, 0, 1, time)
		self:TweenScale(self.m_ThirdWidget.transform, Vector3(0.8, 0.8, 1), Vector3(1, 1, 1), time)

		self:TweenAlpha(self.m_SecondWidget, 1, 0, time)
		self:TweenScale(self.m_SecondWidget.transform, Vector3(1, 1, 1), Vector3(1.2, 1.2, 1), time)
	else
		self:TweenAlpha(self.m_SecondWidget, 0, 1, time)
		self:TweenScale(self.m_SecondWidget.transform, Vector3(1.2, 1.2, 1), Vector3(1, 1, 1), time)

		self:TweenAlpha(self.m_ThirdWidget, 1, 0, time)
		self:TweenScale(self.m_ThirdWidget.transform, Vector3(1, 1, 1), Vector3(0.8, 0.8, 1), time)
	end

	self.m_SwitchBtnSprite.spriteName = bThirdMap and "common_icon_suoxiao" or "common_icon_fangda"
	PlayerPrefs.SetInt(CLuaCityWarSecondaryMapWnd.PlayerPrefsKey, bThirdMap and 1 or 0)
end

function CLuaCityWarSecondaryMapWnd:ThirdMapMove(deltaX, deltaY, time)
	local endX = self.m_ThirdMapRootObj.transform.localPosition.x + deltaX
	local endY = self.m_ThirdMapRootObj.transform.localPosition.y + deltaY
	endX = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, endX))
	endY = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, endY))
	LuaTweenUtils.TweenPosition(self.m_ThirdMapRootObj.transform, endX, endY, 0, time)
	self:UpdateThumbnail(endX, endY)
end

function CLuaCityWarSecondaryMapWnd:UpdateThumbnail(x, y)
	local xx = math.abs(x) * 0.04
	local yy = math.abs(y) * 0.04
	self.m_ThumbnailTrans.localPosition = Vector3(xx, yy, 0)
end

function CLuaCityWarSecondaryMapWnd:InitThirdMapMainPlayer( ... )
	if CScene.MainScene and CScene.MainScene.SceneTemplateId == CLuaCityWarMgr.CurrentMapId and CClientMainPlayer.Inst then
		self.m_ThirdMainPlayerTemplateObj:SetActive(true)
		local gridPos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
		local playerMapPos = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
		self.m_ThirdMainPlayerTemplateObj.transform.localPosition = playerMapPos
		local x, y = CClientMainPlayer.Inst.RO.transform.forward.x, CClientMainPlayer.Inst.RO.transform.forward.z

		local angle = math.atan2(y, x) * 180 / math.pi
		self.m_ThirdMainPlayerTemplateObj.transform.localEulerAngles = Vector3(0, 0, angle)
		self:UpdateThirdMapPath(playerMapPos)
	end
end

function CLuaCityWarSecondaryMapWnd:UpdateThirdMapPath(playerMapPos)
	local anyKey = CommonDefs.DEVICE_PLATFORM == "pc" and Input.anyKey or false
	if anyKey or (not CLuaCityWarMgr:CanTrack()) then
 		self.m_ThirdPathDrawer:ClearCurrentPath()
 		self.m_ThirdDestTemplateObj:SetActive(false)
		return
	end
	local movePath = CClientMainPlayer.Inst.EngineObject.CurrentMovePath
    local wayPoint = CClientMainPlayer.Inst.EngineObject.WayPoint
    if movePath ~= nil then
        local listPos = CreateFromClass(MakeGenericClass(List, Vector3))
        do
            local i = wayPoint
            while i < movePath.PathSize do
                local tmp1 = i
                local tmp2 = 0
                local default
                default, tmp1, tmp2 = movePath:GetPixelPosAt(tmp1, tmp2)
                local pixelPos = default
                local gridPos = Utility.PixelPos2GridPos(pixelPos)
                local mapPos = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
                CommonDefs.ListAdd(listPos, typeof(Vector3), mapPos)
                i = i + 1
            end
        end
        local endPos = Utility.PixelPos2GridPos(CTrackMgr.Inst:GetCurSceneEndPos())
        self.m_ThirdDestTemplateObj:SetActive(true)
        local endPoss = self:GetThirdMapUIPos(endPos.x, endPos.y)
		self.m_ThirdDestTemplateObj.transform.localPosition = endPoss
        CommonDefs.ListAdd(listPos, typeof(Vector3), endPoss)
        self.m_ThirdPathDrawer:UpdatePlayerPath(listPos, playerMapPos)
 	else
 		self.m_ThirdDestTemplateObj:SetActive(false)
    end
end

function CLuaCityWarSecondaryMapWnd:LocateMe(time)
	local gridPos = CPos(400, 400)
	if CScene.MainScene and CScene.MainScene.SceneTemplateId == CLuaCityWarMgr.CurrentMapId and CClientMainPlayer.Inst then
		gridPos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
	end
	local uiPos = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
	local x = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, -1 * uiPos.x))
	local y = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, -1 * uiPos.y))
	--self.m_ThirdMapRootObj.transform.localPosition = Vector3(x, y, 0)
	LuaTweenUtils.TweenPosition(self.m_ThirdMapRootObj.transform, x, y, 0, time)
	self:UpdateThumbnail(x, y)
end

function CLuaCityWarSecondaryMapWnd:ThirdMapClick()
	local worldPos = CUIManager.instance.MainCamera:ScreenToWorldPoint(Input.mousePosition)
	local deltaPos = self.m_ThirdMapRootObj.transform:InverseTransformPoint(worldPos)
	local gridPos = CPos(deltaPos.x * self.m_ActualMapWidth / self.m_ThirdUIMapWidth + self.m_LeftBottomMapPos, deltaPos.y * self.m_ActualMapWidth / self.m_ThirdUIMapWidth + self.m_LeftBottomMapPos)
	self:Track(gridPos)
end

function CLuaCityWarSecondaryMapWnd:QueryEnemyPlayerPosResult( dataUD )
	if not CScene.MainScene or CScene.MainScene.SceneTemplateId ~= CLuaCityWarMgr.CurrentMapId then return end

	local playerList = MsgPackImpl.unpack(dataUD)
	for i = 0, playerList.Count - 1, 4 do
		local obj = NGUITools.AddChild(self.m_ThirdMapRootObj, self.m_ThirdEnemyPlayerTemplateObj)
		obj:SetActive(true)
		obj.transform.localPosition = self:GetThirdMapUIPos(playerList[i + 2], playerList[i + 3])
	end
end
---------- Three-Level Map End



function CLuaCityWarSecondaryMapWnd:GetGuideGo(methodName)
    if methodName=="GetPrimaryMapButton" then
		return self.m_PrimaryMapBtn
	end
    return nil
end
