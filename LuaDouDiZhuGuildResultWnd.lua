CLuaDouDiZhuGuildResultWnd=class()
RegistClassMember(CLuaDouDiZhuGuildResultWnd,"m_Tick")
RegistClassMember(CLuaDouDiZhuGuildResultWnd,"m_LeftTime")
RegistClassMember(CLuaDouDiZhuGuildResultWnd,"m_OkLabel")
RegistClassMember(CLuaDouDiZhuGuildResultWnd,"m_OkButton")

function CLuaDouDiZhuGuildResultWnd:Init()
    local UILabelType=typeof(UILabel)
    -- FindChild(self.transform,"CancleLable"):GetComponent(UILabelType).text=LocalString.GetString("离开")
    self.m_OkLabel=FindChild(self.transform,"OkLabel"):GetComponent(UILabelType)
    self.m_OkLabel.text=LocalString.GetString("回到帮会")

    local titleLabel = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(UILabelType)
    titleLabel.text = CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eCrossPlay and LocalString.GetString("跨服比赛积分结算界面") or LocalString.GetString("斗地主积分结算")


    self.m_OkButton=FindChild(self.transform,"OkButton").gameObject
    if CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eCrossPlay then--跨服斗地主
        self.m_OkButton:SetActive(false)
    else
        UIEventListener.Get(self.m_OkButton).onClick=LuaUtils.VoidDelegate(function(go)
            --回到帮会
            Gac2Gas.RequestLeaveDouDiZhu()
            CUIManager.CloseUI(CLuaUIResources.DouDiZhuGuildResultWnd)
        end)
    end


    --初始化数据
    local myId=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local templateItem=FindChild(self.transform,"Item").gameObject
    templateItem:SetActive(false)    
    local grid=FindChild(self.transform,"Grid").gameObject
    CUICommonDef.ClearTransform(grid.transform)
    
    if CLuaDouDiZhuMgr.m_Result then
        for i,v in ipairs(CLuaDouDiZhuMgr.m_Result) do
            local item = NGUITools.AddChild(grid, templateItem)
            item:SetActive(true)
            local tf=item.transform
            local nameLabel=FindChild(tf,"NameLabel"):GetComponent(UILabelType)
            nameLabel.text=v.name
            local label1=FindChild(tf,"Label1"):GetComponent(UILabelType)
            label1.text=v.duZhuScore
            local label2=FindChild(tf,"Label2"):GetComponent(UILabelType)
            label2.text=v.duZhuPower
            local label3=FindChild(tf,"Label3"):GetComponent(UILabelType)
            label3.text=self:FormatTime(v.usedTime)
            local label4=FindChild(tf,"Label4"):GetComponent(UILabelType)
            label4.text=v.score
            if myId==v.id then
                LuaUtils.SetUIWidgetColor(nameLabel,0,255,0,255)
                LuaUtils.SetUIWidgetColor(label1,0,255,0,255)
                LuaUtils.SetUIWidgetColor(label2,0,255,0,255)
                LuaUtils.SetUIWidgetColor(label3,0,255,0,255)
                LuaUtils.SetUIWidgetColor(label4,0,255,0,255)
            end
            
        end
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

    --倒计时
    self.m_LeftTime=5
    self.m_OkLabel.text =LocalString.GetString("回到帮会").."(5)"
    self.m_Tick = RegisterTickWithDuration(function ()
		if not self:OnTick() then 
			UnRegisterTick(self.m_Tick)
            self.m_Tick=nil
            -- CUIManager.CloseUI(CUIResources.GamePlayResultWnd2)
        end
    end,1000,6*1000)
end
function CLuaDouDiZhuGuildResultWnd:OnTick()
	if self.m_OkLabel == nil then return false end

    self.m_LeftTime = self.m_LeftTime - 1
    if self.m_LeftTime>=0 then
        self.m_OkLabel.text =LocalString.GetString("回到帮会")..SafeStringFormat3("(%d)",self.m_LeftTime)
        return true
    end
    self.m_OkLabel.text=LocalString.GetString("回到帮会")
    return false
end
function CLuaDouDiZhuGuildResultWnd:FormatTime(time)
    local minute=math.floor(time/60)
    local second=time%60
    return SafeStringFormat3("%02d:%02d",minute,second)
end
function CLuaDouDiZhuGuildResultWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
end
