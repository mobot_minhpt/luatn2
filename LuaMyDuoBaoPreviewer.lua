local Extensions = import "Extensions"
local CDuoBaoInvestPlayerInfo = import "L10.UI.CDuoBaoInvestPlayerInfo"
local CDuoBaoMgr = import "L10.UI.CDuoBaoMgr"
local String = import "System.String"

LuaMyDuoBaoPreviewer = class()

RegistChildComponent(LuaMyDuoBaoPreviewer,"m_ItemNameLabel","ItemNameLabel", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CurrentStatusLabel","CurrentStatusLabel", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_MyProbilityLabel","MyProbilityLabel", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CurrentGroupIndexLabel","CurrentGroupIndex", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CurrengGroupProgressLabel","CurrengGroupProgress", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_MyMoneySprite","MyMoneySprite", UISprite)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_MyInvestLabel","MyInvest", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_WinnerMoneySprite","WinnerMoneySprite", UISprite)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_WinnerNameLabel","WinnerNameLabel", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_WinnerInvestLabel","WinnerInvest", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_YikoujiaObj","Yikoujia", GameObject)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CrossServerObj","CrossServer", GameObject)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_ItemTemplate","ItemTemplate", GameObject)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CrossServerItemTemplate","CrossServerItemTemplate", GameObject)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CrossServerWinnerNameLabel","CrossServerWinnerNameLabel", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CrossServerWinnerInvest","CrossServerWinnerInvest", UILabel)
RegistChildComponent(LuaMyDuoBaoPreviewer,"m_CrossServerWinnerMoneySprite","CrossServerWinnerMoneySprite", UISprite)

RegistClassMember(LuaMyDuoBaoPreviewer, "m_InvestPlayerList")
RegistClassMember(LuaMyDuoBaoPreviewer, "m_CrossServerInvestPlayerList")

function LuaMyDuoBaoPreviewer:Awake()
    self.m_InvestPlayerList = {}
    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_ItemTemplate.gameObject:SetActive(false)
    self.m_CrossServerItemTemplate.gameObject:SetActive(false)
    self.m_CrossServerWinnerNameLabel.gameObject:SetActive(false)
    self.m_CrossServerWinnerInvest.gameObject:SetActive(false)
end

function LuaMyDuoBaoPreviewer:UpdateData(data)
    local status, info = data[0], data[1]
    if not info then
        self.gameObject:SetActive(false)
        return
    end
    self.gameObject:SetActive(true)
    self.m_YikoujiaObj:SetActive(info.Need_FenShu == 1)
    self.m_CrossServerObj.gameObject:SetActive(info.IsCrossServer ~= 0)
    self.m_MyMoneySprite.spriteName = CDuoBaoMgr.Inst:GetMoneySpriteName(info)
    self.m_ItemNameLabel.text = info.ItemName

    if info.IsPublished then
        self.m_CurrentStatusLabel.text = (CClientMainPlayer.Inst and info.WinnerId == CClientMainPlayer.Inst.Id) and
                LocalString.GetString("[ffff00]恭喜您获得该物品[-]") or LocalString.GetString("[00ff00]结果已揭晓[-]")
        self.m_WinnerNameLabel.gameObject:SetActive(true)
        self.m_WinnerInvestLabel.gameObject:SetActive(true)
        self.m_CrossServerWinnerNameLabel.gameObject:SetActive(false)
        self.m_CrossServerWinnerInvest.gameObject:SetActive(false)
        self.m_WinnerNameLabel.text = info.WinnerName
        if info.IsCrossServer ~= 0 then
            self.m_WinnerNameLabel.gameObject:SetActive(false)
            self.m_WinnerInvestLabel.gameObject:SetActive(false)
            self.m_CrossServerWinnerNameLabel.gameObject:SetActive(true)
            self.m_CrossServerWinnerInvest.gameObject:SetActive(true)
            self.m_CrossServerWinnerMoneySprite.spriteName = CDuoBaoMgr.Inst:GetMoneySpriteName(info)
            self.m_CrossServerWinnerNameLabel.text = SafeStringFormat3("%s(%s)", info.WinnerName, info.Servername)
            self.m_CrossServerWinnerInvest.text = CDuoBaoMgr.Inst:IsLingYuDuoBao(info) and
                    (String.Format("{0}", info.WinnerInvestInFenShu * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info))) or
                    (String.Format(CommonDefs.IS_VN_CLIENT and LocalString.GetString("{0} 万") or "{0}w", info.WinnerInvestInFenShu * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info) / 10000))
        end
        self.m_WinnerMoneySprite.spriteName = CDuoBaoMgr.Inst:GetMoneySpriteName(info)

        self.m_WinnerInvestLabel.text = CDuoBaoMgr.Inst:IsLingYuDuoBao(info) and
                (String.Format("{0}", info.WinnerInvestInFenShu * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info))) or
                (String.Format(CommonDefs.IS_VN_CLIENT and LocalString.GetString("{0} 万") or "{0}w", info.WinnerInvestInFenShu * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info) / 10000))
    else
        self.m_CurrentStatusLabel.text = LocalString.GetString("[ffff00]进行中[-]")
        self.m_WinnerNameLabel.gameObject:SetActive(false)
        self.m_WinnerInvestLabel.gameObject:SetActive(false)
        self.m_CrossServerWinnerNameLabel.gameObject:SetActive(false)
        self.m_CrossServerWinnerInvest.gameObject:SetActive(false)
    end

    local probility = 100.0 * info.MyInvestInFenShu / info.Need_FenShu
    self.m_MyProbilityLabel.text = String.Format("{0:N1}%", probility)
    self.m_CurrentGroupIndexLabel.text = String.Format(LocalString.GetString("第{0}组"), info.GroupIndex)
    self.m_CurrengGroupProgressLabel.text = String.Format("{0}/{1}", info.CurrentGroupProgress, info.Need_FenShu)
    self.m_WinnerMoneySprite.spriteName = CDuoBaoMgr.Inst:GetMoneySpriteName(info)
    self.m_MyInvestLabel.text = CDuoBaoMgr.Inst:IsLingYuDuoBao(info) and
            String.Format(LocalString.GetString("{0:N0}({1}份)"), info.MyInvestInFenShu * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info), info.MyInvestInFenShu) or
            String.Format(LocalString.GetString("{0:N0}w({1}份)"), info.MyInvestInFenShu * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info) / 10000, info.MyInvestInFenShu)

    Extensions.RemoveAllChildren(self.m_Grid.transform)
    for i = 1, 6 do
        if i <= info.TopInvestUsers.Count then
            local go =  NGUITools.AddChild(self.m_Grid.gameObject,(info.IsCrossServer ~= 0) and self.m_CrossServerItemTemplate.gameObject or self.m_ItemTemplate.gameObject)
            go:SetActive(true)
            local cinfo  = go:GetComponent(typeof(CDuoBaoInvestPlayerInfo))
            cinfo:UpdateData(info, info.TopInvestUsers[i - 1].Key, info.TopInvestUsers[i - 1].Value)
        end
    end
    self.m_Grid:Reposition()
end