local DelegateFactory = import "DelegateFactory"
local MessageMgr = import "L10.Game.MessageMgr"


CLuaQMPKRegisterWnd = class()
RegistClassMember(CLuaQMPKRegisterWnd, "m_RuleLabel")
RegistClassMember(CLuaQMPKRegisterWnd, "m_TipLabel")
RegistClassMember(CLuaQMPKRegisterWnd, "m_RegisterButton")


function CLuaQMPKRegisterWnd:Init()
	self.m_RuleLabel.text = MessageMgr.Inst:FormatMessage("QMPK_BAOMING_MSG", {})
	self.m_TipLabel:SetActive(false)
	self.m_RegisterButton.gameObject:SetActive(true)
	UIEventListener.Get(self.m_RegisterButton.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
		Gac2Gas.RequestRegisterQmpkPersonalInfo()
	end)
end

function CLuaQMPKRegisterWnd:RegisterSuccess( )
	self.m_TipLabel:SetActive(true)
	self.m_RegisterButton.gameObject:SetActive(false)
end

function CLuaQMPKRegisterWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("QmpkRegisterPersonalInfoSuccess", self, "RegisterSuccess")
end

function CLuaQMPKRegisterWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("QmpkRegisterPersonalInfoSuccess", self, "RegisterSuccess")
end

return CLuaQMPKRegisterWnd