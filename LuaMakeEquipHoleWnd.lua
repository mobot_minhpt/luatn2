local NGUIText = import "NGUIText"
local MessageWndManager = import "L10.UI.MessageWndManager"
local IdPartition = import "L10.Game.IdPartition"
local Color = import "UnityEngine.Color"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentInlayMgr = import "L10.Game.CEquipmentInlayMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"


CLuaMakeEquipHoleWnd = class()
RegistClassMember(CLuaMakeEquipHoleWnd,"descLabel")
RegistClassMember(CLuaMakeEquipHoleWnd,"iconTexture")
RegistClassMember(CLuaMakeEquipHoleWnd,"countLabel")
RegistClassMember(CLuaMakeEquipHoleWnd,"acquireGo")
RegistClassMember(CLuaMakeEquipHoleWnd,"okBtn")
RegistClassMember(CLuaMakeEquipHoleWnd,"diamondBtn")
RegistClassMember(CLuaMakeEquipHoleWnd,"itemCountInBag")
RegistClassMember(CLuaMakeEquipHoleWnd,"costCount")
RegistClassMember(CLuaMakeEquipHoleWnd,"ItemNameLabel")
RegistClassMember(CLuaMakeEquipHoleWnd,"costItem")

function CLuaMakeEquipHoleWnd:Awake()
    self.descLabel = self.transform:Find("Anchor/TextLabel"):GetComponent(typeof(UILabel))
    self.iconTexture = self.transform:Find("Anchor/Diamond/Icon"):GetComponent(typeof(CUITexture))
    self.countLabel = self.transform:Find("Anchor/Diamond/Count"):GetComponent(typeof(UILabel))
    self.acquireGo = self.transform:Find("Anchor/Diamond/Acquire").gameObject
    self.okBtn = self.transform:Find("Anchor/OkButton").gameObject
    self.diamondBtn = self.transform:Find("Anchor/Diamond").gameObject
self.itemCountInBag = 0
self.costCount = 0
self.ItemNameLabel = nil
self.costItem = nil

    UIEventListener.Get(self.okBtn).onClick =DelegateFactory.VoidDelegate(function(go)
        self:OnOkBtnClicked(go)
    end)
    UIEventListener.Get(self.diamondBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.costItem.ID, self.itemCountInBag >= self.costCount, go.transform, AlignType.Right)
    end)
end

function CLuaMakeEquipHoleWnd:GetMakeHoleCost( item) 
    if item.IsEquip then
        if IdPartition.IdIsTalisman(item.TemplateId) then
            local costList = EquipHole_Setting.GetData().FaBaoMakeHoleCost
            if item.Equip.Hole >= 0 and item.Equip.Hole < costList.Length then
                return costList[item.Equip.Hole][1], costList[item.Equip.Hole][2]
            end
        else
            local config = CEquipmentInlayMgr.Inst:GetMakeHoleConfig(item.Equip.Grade)
            return config.costItemTemplateId, config:GetCostCount(item.Equip.Hole + 1)
        end
    end
    return 0,0
end
function CLuaMakeEquipHoleWnd:Init( )
    if CEquipmentProcessMgr.Inst.SelectEquipment == nil then
        return
    end
    local equipId = CEquipmentProcessMgr.Inst.SelectEquipment.itemId
    local equip = CItemMgr.Inst:GetById(equipId)
    local info = CLuaEquipMgr.SelectedEquipmentHoleInfo

    local equipTemplate = EquipmentTemplate_Equip.GetData(equip.TemplateId)

    local costTemplateId,v = self:GetMakeHoleCost(equip)
    self.costCount = v

    local template = Item_Item.GetData(costTemplateId)
    self.costItem = template
    self.ItemNameLabel = template.Name
    if equip.IsEquip and equip.Equip.IsFaBao then
        self.descLabel.text = SafeStringFormat3(LocalString.GetString("本次打孔消耗1个%s"), template.Name)
    else
        self.descLabel.text = SafeStringFormat3(LocalString.GetString("此装备打孔需要消耗%s"), template.Name)
    end
    self.iconTexture:LoadMaterial(template.Icon)

    local bindItemCountInBag, notBindItemCountInBag
    bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(costTemplateId)

    self.itemCountInBag = bindItemCountInBag + notBindItemCountInBag

    if self.costCount <= self.itemCountInBag then
        self.countLabel.text = SafeStringFormat3("%d/%d", self.costCount, self.itemCountInBag)
        self.acquireGo:SetActive(false)
    else
        self.countLabel.text = System.String.Format("([c][{0}]{2}[/c][-]/{1})", NGUIText.EncodeColor24(Color.red), self.costCount, self.itemCountInBag)
        self.acquireGo:SetActive(true)
        UIEventListener.Get(self.acquireGo).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.costItem.ID, false, self.diamondBtn.transform, AlignType.Right)
        end)
    end
end
function CLuaMakeEquipHoleWnd:OnEnable( )
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
end
function CLuaMakeEquipHoleWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
end

function CLuaMakeEquipHoleWnd:OnSendItem(args)
    CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
    self:Init()
end
function CLuaMakeEquipHoleWnd:OnSetItemAt(args)
    CUIManager.CloseUI(CUIResources.ItemAccessListWnd)
    self:Init()
end

function CLuaMakeEquipHoleWnd:OnOkBtnClicked( go) 
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    if equipInfo == nil then
        return
    end

    if not CEquipmentProcessMgr.Inst:CheckOperationConflict(equipInfo) then
        return
    end

    if self.itemCountInBag < self.costCount then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.costItem.ID, false, self.diamondBtn.transform, AlignType.Right)
        return
    end

    local commonItem = CItemMgr.Inst:GetById(equipInfo.itemId)
    if commonItem == nil or not commonItem.IsEquip then
        return
    end

    local secondWordSet = commonItem.Equip.HasTwoWordSet and commonItem.Equip.IsSecondWordSetActive or false
    if commonItem.Equip:HasTempWordDataInWordSet(secondWordSet) then
        local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function() self:RequestMaskHole() end), nil, nil, nil, false)
    elseif commonItem.Equip:HasTempWordDataInWordSet(not secondWordSet) then
        local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE_OTHERSET")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function() self:RequestMaskHole() end), nil, nil, nil, false)
    else
        self:RequestMaskHole()
    end

    CUIManager.CloseUI(CUIResources.MakeEquipHoleWnd)
end
function CLuaMakeEquipHoleWnd:RequestMaskHole( )

    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipment
    local commonItem = CItemMgr.Inst:GetById(equipInfo.itemId)
    if not commonItem.Equip.IsBinded then
        local message = g_MessageMgr:FormatMessage("Equipment_Make_Hole_Confirm")
        if commonItem.Equip.IsFaBao then
            message = g_MessageMgr:FormatMessage("FaBao_NotBind_MakeHole_Confirm")
        end
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
            Gac2Gas.MakeHoleOnEquipment(EnumToInt(equipInfo.place), equipInfo.pos, equipInfo.itemId)
        end), nil, nil, nil, false)
    else
        Gac2Gas.MakeHoleOnEquipment(EnumToInt(equipInfo.place), equipInfo.pos, equipInfo.itemId)
    end
end

