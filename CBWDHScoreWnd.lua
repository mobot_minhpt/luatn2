-- Auto Generated!!
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHScoreItem = import "L10.UI.CBWDHScoreItem"
local CBWDHScoreWnd = import "L10.UI.CBWDHScoreWnd"
local DelegateFactory = import "DelegateFactory"
local Gac2Gas = import "L10.Game.Gac2Gas"
local UInt32 = import "System.UInt32"
CBWDHScoreWnd.m_Init_CS2LuaHook = function (this) 
    Gac2Gas.QueryBiWuScore()
    this.tableView.m_DataSource = this
end
CBWDHScoreWnd.m_OnQueryBiWuScoreResult_CS2LuaHook = function (this, list) 

    this.stageList = InitializeList(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, 1, 2, 3, 4, 5)
    local myStage = CBiWuDaHuiMgr.Inst:GetMyStage()
    this.historyScore = list

    this.tableView:ReloadData(false, false)
    --this.tableView:SetSelectRow((myStage - 1), true)
end
CBWDHScoreWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < 5 then
        local cmp = TypeAs(this.tableView:GetFromPool(0), typeof(CBWDHScoreItem))
        local shopids = {34000088,34000089,34000090,34000091,34000092}
        if this.historyScore ~= nil then
            local stage = this.stageList[row]
            local score = 0
            local findIndex = this.historyScore:FindIndex(DelegateFactory.Predicate_UIntUIntKeyValuePair(function (p) 
                return p.Key == stage
            end))
            if findIndex >= 0 then
                score = this.historyScore[findIndex].Value
            end
            cmp:Init(stage, score, row,shopids[row+1])
        else
            cmp:Init((row + 1), 0, row,shopids[row+1])
        end
        return cmp
    end
    return nil
end
