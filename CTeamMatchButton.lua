-- Auto Generated!!
local CTeamMatchButton = import "L10.UI.CTeamMatchButton"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
CTeamMatchButton.m_ChangeTeamMatchingState_CS2LuaHook = function (this) 
    if CTeamMatchMgr.Inst.IsTeamMatching then
        this.label.text = LocalString.GetString("取消匹配")
    else
        this.label.text = LocalString.GetString("自动匹配")
    end
end
CTeamMatchButton.m_OnClick_CS2LuaHook = function (this) 
    --如果队伍满员了
    --如果队伍满员了
    if CTeamMgr.Inst:TeamExists() and CTeamMgr.Inst.IsTeamFull then
        g_MessageMgr:ShowMessage("TEAM_IS_FULL")
    else
        if CTeamMatchMgr.Inst.IsTeamMatching then
            Gac2Gas.AskForLeaveMatching()
        else
            if not CTeamMatchMgr.Inst.HasMatchTarget then
                g_MessageMgr:ShowMessage("Choose_Target")
            else
                --TODO 凯旋加一下matchNewhand的参数
                CTeamMatchMgr.Inst:AskForTeamMatchingForLeader(CTeamMatchMgr.Inst.selectedActivityId, CTeamMatchMgr.Inst.MinLevel, CTeamMatchMgr.Inst.MaxLevel, CTeamMatchMgr.Inst.IncludeNewbie)
            end
        end
    end
end
