local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"

LuaWarmChristmasEveWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

--@endregion RegistChildComponent end
RegistClassMember(LuaWarmChristmasEveWnd, "m_Animation")
RegistClassMember(LuaWarmChristmasEveWnd, "m_Tick")

function LuaWarmChristmasEveWnd:Awake()
    self.m_Animation = self.transform:GetComponent(typeof(Animation))
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaWarmChristmasEveWnd:Init()
    self:HideOrShowUI(true)
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:OnTick()
    end, self.m_Animation.clip.length * 1000 - 800)
end

--@region UIEvent

--@endregion UIEvent

function LuaWarmChristmasEveWnd:OnTick()
    LuaChristmas2023Mgr:Christmas2023GuaGuaKaDaJiangEnd()
    CUIManager.CloseUI(CLuaUIResources.WarmChristmasEveWnd)
end

function LuaWarmChristmasEveWnd:OnDestroy()
    self:HideOrShowUI(false)
    UnRegisterTick(self.m_Tick)
end

function LuaWarmChristmasEveWnd:HideOrShowUI(bHide)
    local bShow = not bHide
    if bShow then
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, true, false)
        CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, true, false)
    else
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, true, false, false)
        CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, true, false, false)
    end
end