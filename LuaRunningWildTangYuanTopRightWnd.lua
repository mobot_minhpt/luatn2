local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"

LuaRunningWildTangYuanTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaRunningWildTangYuanTopRightWnd, "TopNode1", "TopNode1", GameObject)
RegistChildComponent(LuaRunningWildTangYuanTopRightWnd, "TopNode2", "TopNode2", GameObject)
RegistChildComponent(LuaRunningWildTangYuanTopRightWnd, "TopNode3", "TopNode3", GameObject)
RegistChildComponent(LuaRunningWildTangYuanTopRightWnd, "TopNode4", "TopNode4", GameObject)
RegistChildComponent(LuaRunningWildTangYuanTopRightWnd, "TopNodeSelf", "TopNodeSelf", GameObject)
RegistChildComponent(LuaRunningWildTangYuanTopRightWnd, "ExplandBtn", "ExplandBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaRunningWildTangYuanTopRightWnd,"m_RankSprits")
function LuaRunningWildTangYuanTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.ExplandBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExplandBtnClick()
	end)
    self.m_RankSprits = {
        [1]= "UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_01.mat",
        [2]="UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_02.mat",
        [3]="UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_03.mat",
    }
end

function LuaRunningWildTangYuanTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncTangYuan2022PlayInfo", self, "OnSyncTangYuan2022PlayInfo")
end

function LuaRunningWildTangYuanTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncTangYuan2022PlayInfo", self, "OnSyncTangYuan2022PlayInfo")
end

function LuaRunningWildTangYuanTopRightWnd:Init()
    self:InitTopNode(true,1,{})
    self:InitTopNode(false,1,{})
    self:InitTopNode(false,2,{})
    self:InitTopNode(false,3,{})
    self:InitTopNode(false,4,{})
    if LuaTangYuan2022Mgr.PlayerInfo and next(LuaTangYuan2022Mgr.PlayerInfo) then
        for i = 1, #LuaTangYuan2022Mgr.PlayerInfo, 1 do
            local playerInfo = LuaTangYuan2022Mgr.PlayerInfo[i]
            local isSelf = playerInfo.playerId == CClientMainPlayer.Inst.Id
            if isSelf then
                --显示非自己排名处的自己的排名
                self:InitTopNode(false,playerInfo.rank,playerInfo)
            end
            self:InitTopNode(isSelf,playerInfo.rank,playerInfo)
        end
    end
end

function LuaRunningWildTangYuanTopRightWnd:OnExplandBtnClick()
    self.ExplandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaRunningWildTangYuanTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExplandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaRunningWildTangYuanTopRightWnd:InitTopNode(isSelf,rank,info)
    local node
    if not isSelf then
        node = self["TopNode"..rank]
    else
        node = self.TopNodeSelf
    end

    local nameLabel = node.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
    local scoreLabel = node.transform:Find("Score"):GetComponent(typeof(UILabel))

    local name = ""
    local score = ""
    if isSelf then
        local rankLabel = node.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
        local rankSprite = node.transform:Find("RankSprite"):GetComponent(typeof(CUITexture))

        if not info or not next(info) then
           rank = 1
           name = CClientMainPlayer.Inst.RealName
        else
            rank = info.rank
            name = info.playerName
            score = info.score
        end
        if rank <= 3 then
            rankSprite:LoadMaterial(self.m_RankSprits[rank])
            rankLabel.text = nil
            rankSprite.gameObject:SetActive(true)
        else
            rankLabel.text = rank
            rankSprite.gameObject:SetActive(false)
        end
    else
        name = info.playerName
        score = info.score
    end
    nameLabel.text = name 
    scoreLabel.text = score
end

function LuaRunningWildTangYuanTopRightWnd:OnSyncTangYuan2022PlayInfo(infoUD)
    local info = MsgPackImpl.unpack(infoUD)
	if not info then return end

    LuaTangYuan2022Mgr.PlayerInfo = {}
	for i = 0, info.Count-1, 5 do
        local playerInfo = {}
		playerInfo.rank = info[i]
		playerInfo.playerId = info[i+1]
		playerInfo.playerName = info[i+2]
		playerInfo.class = info[i+3]
		playerInfo.score = info[i+4]
        table.insert(LuaTangYuan2022Mgr.PlayerInfo,playerInfo)
        local isSelf = playerInfo.playerId == CClientMainPlayer.Inst.Id
        if isSelf then
            --显示非自己排名处的自己的排名
            self:InitTopNode(false,playerInfo.rank,playerInfo)
        end
        self:InitTopNode(isSelf,playerInfo.rank,playerInfo)
	end
    EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
end
--@region UIEvent

--@endregion UIEvent

