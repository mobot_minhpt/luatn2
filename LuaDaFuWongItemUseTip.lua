local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"

LuaDaFuWongItemUseTip = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongItemUseTip, "ItemName", "ItemName", UILabel)
RegistChildComponent(LuaDaFuWongItemUseTip, "ItemTexture", "ItemTexture", CUITexture)
RegistChildComponent(LuaDaFuWongItemUseTip, "Desc", "Desc", UILabel)
RegistChildComponent(LuaDaFuWongItemUseTip, "UseButton", "UseButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongItemUseTip, "m_id")
RegistClassMember(LuaDaFuWongItemUseTip, "m_NoUseLabel")
function LuaDaFuWongItemUseTip:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.UseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUseButtonClick()
	end)


    --@endregion EventBind end
	self.m_NoUseLabel = self.transform:Find("Background/Right/NoUseLabel").gameObject
	self.m_NoUseLabel.gameObject:SetActive(false)
end

function LuaDaFuWongItemUseTip:Init()
	self.m_id = LuaDaFuWongMgr.CurItemId
	local data = DaFuWeng_Card.GetData(self.m_id)
	if not data then return end
	self:InitLeft(data)
	self:InitRight(data)
	self:OnStageUpdate(LuaDaFuWongMgr.CurStage)
end
function LuaDaFuWongItemUseTip:InitRight(data)
	self.Desc.text = g_MessageMgr:Format(data.Describe)
end

function LuaDaFuWongItemUseTip:InitLeft(data)
	self.ItemName.text = data.Name
	self.ItemTexture:LoadMaterial(data.Icon)
end

function LuaDaFuWongItemUseTip:OnStageUpdate(CurStage)
	local canUse = CurStage == EnumDaFuWengStage.RoundCard and LuaDaFuWongMgr.IsMyRound
	self.UseButton.gameObject:SetActive(canUse)
	self.m_NoUseLabel.gameObject:SetActive(not canUse)
end

--@region UIEvent

function LuaDaFuWongItemUseTip:OnUseButtonClick()
	LuaDaFuWongMgr:OnShowItemWnd(self.m_id)
	CUIManager.CloseUI(CLuaUIResources.DaFuWongItemUseTip)
end

--@endregion UIEvent
function LuaDaFuWongItemUseTip:OnEnable()
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongItemUseTip:OnDisable()
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
	g_ScriptEvent:BroadcastInLua("OnCloseDaFuWongItemUseTip",self.m_id)
end
