-- Auto Generated!!
local NativeTools=import "L10.Engine.NativeTools"
local Application = import "UnityEngine.Application"
local BoolDelegate = import "UIEventListener+BoolDelegate"
local Boolean = import "System.Boolean"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CChatHistoryMgr = import "L10.Game.CChatHistoryMgr"
local CChatInputLink = import "CChatInputLink"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local CChatListBaseItem = import "L10.UI.CChatListBaseItem"
local CChatMgr = import "L10.Game.CChatMgr"
local CChatWnd = import "L10.UI.CChatWnd"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CVoiceMsg = import "L10.Game.CVoiceMsg"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"
local EnumEventType = import "EnumEventType"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local MessageData = import "L10.UI.MessageData"
local OnClippingMoved = import "UIPanel+OnClippingMoved"
local PlayerSettings = import "L10.Game.PlayerSettings"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local String = import "System.String"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local UIPanel = import "UIPanel"
local VoicePlayInfo = import "L10.Game.VoicePlayInfo"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CChatWnd.m_Init_CS2LuaHook = function (this, channel) 

    this.tableView:Clear()
    this.tableView.dataSource = this
    CommonDefs.ListClear(this.allData)
    this.hasNewMsg = false
    local p = this.tableView.scrollView.panel

    this:SetAutoVoiceVisibility(channel)
    this:SetChatInputVisibility(channel)
    this:SetCCInfoVisibility(channel)
    this:SetGuildCCInfoVisibility(channel)
    this:SetTeamCCInfoVisibility(channel)
    CChatWndExt:SetGuildAIDCCInfoVisibility(channel)
    CChatWndExt.InitFilterWordCtrl(this,channel)
    CChatWndExt.InitFilterLanguageCtrl(this,channel)

    if channel == EChatPanel.System or channel == EChatPanel.Fight then
        this.scrollViewBg.topAnchor.absolute = this.expandTopAnchorOffset
        this.scrollViewBg.bottomAnchor.absolute = this.expandBottomAnchorOffset
        this.scrollViewBg:ResetAndUpdateAnchors()
        this.splitLine.enabled = false
        this.splitLine:ResetAndUpdateAnchors()
        p:ResetAndUpdateAnchors()
    elseif channel == EChatPanel.CC then
        this.scrollViewBg.topAnchor.absolute = this.ccTopAnchorOffset
        this.scrollViewBg.bottomAnchor.absolute = this.ccBottomAnchorOffset
        this.scrollViewBg:ResetAndUpdateAnchors()
        this.splitLine.enabled = true
        this.splitLine:ResetAndUpdateAnchors()
        p:ResetAndUpdateAnchors()
    elseif channel == EChatPanel.Guild and CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCCChatMgr.Inst.EnableGuildCC and (CGuildMgr.Inst.CanSetCCCommander or CCCChatMgr.Inst:GuildCommanderExist()) then
        this.scrollViewBg.topAnchor.absolute = this.normalTopAnchorOffset - this.guildCCBg.height
        this.scrollViewBg.bottomAnchor.absolute = this.normalBottomAnchorOffset
        this.scrollViewBg:ResetAndUpdateAnchors()
        this.splitLine.enabled = true
        this.splitLine:ResetAndUpdateAnchors()
        p:ResetAndUpdateAnchors()
    elseif channel == EChatPanel.TeamGroup and CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCCChatMgr.Inst.EnableTeamGroupCC and (CTeamGroupMgr.Instance:MainPlayerIsManager(true) or CCCChatMgr.Inst:TeamGroupCommanderExist()) then
        this.scrollViewBg.topAnchor.absolute = this.normalTopAnchorOffset - this.teamGroupCCBg.height
        this.scrollViewBg.bottomAnchor.absolute = this.normalBottomAnchorOffset
        this.scrollViewBg:ResetAndUpdateAnchors()
        this.splitLine.enabled = true
        this.splitLine:ResetAndUpdateAnchors()
        p:ResetAndUpdateAnchors()
    elseif channel == EChatPanel.AID and CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCCChatMgr.Inst.EnableGuildCC and LuaChatMgr:GuildAIDCommanderExists() then
        this.scrollViewBg.topAnchor.absolute = this.normalTopAnchorOffset - this.guildAIDCCBg.height
        this.scrollViewBg.bottomAnchor.absolute = this.normalBottomAnchorOffset
        this.scrollViewBg:ResetAndUpdateAnchors()
        this.splitLine.enabled = true
        this.splitLine:ResetAndUpdateAnchors()
        p:ResetAndUpdateAnchors()
    else
        this.scrollViewBg.topAnchor.absolute = this.normalTopAnchorOffset
        this.scrollViewBg.bottomAnchor.absolute = this.normalBottomAnchorOffset
        this.scrollViewBg:ResetAndUpdateAnchors()
        this.splitLine.enabled = true
        this.splitLine:ResetAndUpdateAnchors()
        p:ResetAndUpdateAnchors()
    end
    
    this.tableView.table.transform.localPosition = Extensions.GetTopPos(this.tableView.scrollView)
    this.newMsgBg:ResetAndUpdateAnchors()
    this:SetNewMsgButtonVisible(false)
    CRecordingInfoMgr.CloseRecordingWnd(true)
    this.channel = channel
    this.qnCheckBox.OnValueChanged = MakeDelegateFromCSFunction(this.OnAutoVoiceCheckValueChanged, MakeGenericClass(Action1, Boolean), this)
    this:InitAutoVoiceCheckBox()
    this:RefreshChatMessages()

    this:UpdateInputType()
    this.chatInput.atPlayerEnabled = (channel == EChatPanel.Guild)
    --仅帮会频道允许@功能
end
CChatWnd.m_SetAutoVoiceVisibility_CS2LuaHook = function (this, channel) 
    local visible = (
        channel ~= EChatPanel.System 
        and channel ~= EChatPanel.Fight 
        and channel ~= EChatPanel.CC 
        and channel ~= EChatPanel.WorldFilter)
    this.autoVoiceBg.gameObject:SetActive(visible)
    if visible then
        this.autoVoiceBg:ResetAndUpdateAnchors()
    end
end
CChatWnd.m_SetChatInputVisibility_CS2LuaHook = function (this, channel) 
    local visible = (channel ~= EChatPanel.System and channel ~= EChatPanel.Fight and channel ~= EChatPanel.WorldFilter)
    this.chatInputBg.gameObject:SetActive(visible)
    if visible then
        this.chatInputBg:ResetAndUpdateAnchors()
    end
end
CChatWnd.m_SetCCInfoVisibility_CS2LuaHook = function (this, channel) 
    local visible = (channel == EChatPanel.CC)
    this.ccInfoBg.gameObject:SetActive(visible)
    if visible then
        this.ccInfoBg:ResetAndUpdateAnchors()
    end
end
CChatWnd.m_SetGuildCCInfoVisibility_CS2LuaHook = function (this, channel) 
    local visible = (channel == EChatPanel.Guild and CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCCChatMgr.Inst.EnableGuildCC and (CGuildMgr.Inst.CanSetCCCommander or CCCChatMgr.Inst:GuildCommanderExist()))
    this.guildCCBg.gameObject:SetActive(visible)
    if visible then
        this.guildCCBg:ResetAndUpdateAnchors()
    end
end
CChatWnd.m_SetTeamCCInfoVisibility_CS2LuaHook = function (this, channel) 
    local visible = (channel == EChatPanel.TeamGroup and CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCCChatMgr.Inst.EnableTeamGroupCC and (CTeamGroupMgr.Instance:MainPlayerIsManager(true) or CCCChatMgr.Inst:TeamGroupCommanderExist()))
    this.teamGroupCCBg.gameObject:SetActive(visible)
    if visible then
        this.teamGroupCCBg:ResetAndUpdateAnchors()
    end
end
CChatWnd.m_UpdateAnchors_CS2LuaHook = function (this) 

    if not Application.isPlaying then
        return
    end
    --PC版调整分辨率时避免外置聊天框显示异常
    this:Init(this.channel)
end
CChatWnd.m_UpdateInputType_CS2LuaHook = function (this) 
    this.keyboardInput:SetActive(not CChatWnd.useVoiceInput)
    this.voiceButton.gameObject:SetActive(CChatWnd.useVoiceInput)
    this.voiceButton.Text = LocalString.GetString("按下发言")
    this.switchButton:SetBackgroundSprite(CChatWnd.useVoiceInput and Constants.ChatInput_KeyboardInputIcon or Constants.ChatInput_VoiceInputIcon)
end
CChatWnd.m_InitAutoVoiceCheckBox_CS2LuaHook = function (this) 
    --这里排除不需要自动语音设置的频道，目前CC、战斗和系统频道是无需此设置， Friend是特殊处理的，也无需此处理
    if  this.channel ~= EChatPanel.Friend and this.channel ~= EChatPanel.System and this.channel ~= EChatPanel.CC and this.channel ~= EChatPanel.Fight then
        this.qnCheckBox.Selected = PlayerSettings.GetAutoVoiceInChannel(this.channel)
    else
        this.qnCheckBox.Selected = false
    end
    
    this.qnCheckBox.Text = System.String.Format(LocalString.GetString("自动播放{0}频道语音"), CChatHelper.GetChannelName(this.channel))
end
CChatWnd.m_OnAutoVoiceCheckValueChanged_CS2LuaHook = function (this, val) 

    --这里排除不需要自动语音设置的频道，目前CC、战斗和系统频道是无需此设置， Friend是特殊处理的，也无需此处理
    if  this.channel ~= EChatPanel.Friend and this.channel ~= EChatPanel.System and this.channel ~= EChatPanel.CC and this.channel ~= EChatPanel.Fight then
        PlayerSettings.SetAutoVoiceInChannel(this.channel, this.qnCheckBox.Selected)
    end
end
CChatWnd.m_OnJoinCCStream_CS2LuaHook = function (this, type) 
    this:RefeshListenToZhiboLabel()
end
CChatWnd.m_RefeshListenToZhiboLabel_CS2LuaHook = function (this) 
    local info = CCCChatMgr.Inst:GetCurrentStation_V2()
    this.listenToZhiboLabel.gameObject:SetActive(this.channel == EChatPanel.CC and this.allData.Count == 0 and info ~= nil and not CCMiniAPIMgr.Inst:IsInZhuboStream())
    this.listenToZhiboLabel.text = LocalString.GetString("点击播放按钮观看主播直播")
end
CChatWnd.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    if index < 0 and index >= this.allData.Count then
        return nil
    end
    local cellIdentifier = nil
    local template = nil
    local data = this.allData[index]
    local isOpp = data.isOpposite
    if data.usedForDisplayTime then
        cellIdentifier = "TimeSplitCell"
        template = this.timesplitTemplate
    elseif data.usedForDisplayOfflineMsgSplit then
        cellIdentifier = "TimeSplitCell"
        template = this.timesplitTemplate
    elseif data.channel == EChatPanel.System then
        cellIdentifier = "SystemChannelCell"
        template = this.systemChannelTemplate
    elseif data.channel == EChatPanel.Fight then
        cellIdentifier = "SystemChannelCell"
        template = this.systemChannelTemplate
    elseif data.senderId == 0 then
        if data.isHongBao then
            cellIdentifier = "OppositeHongBaoTemplate"
            template = this.oppositeHongBaoTemplate
        elseif data.voiceAchievementId > 0 then
            cellIdentifier = "OppositeVoiceAchievementItem"
            template = this.oppositeVoiceAchievementItem
            isOpp = true
        else
            cellIdentifier = "SystemChannelCell"
            template = this.systemChannelTemplate
        end
    elseif data.isOpposite then
        if data.isHongBao then
            cellIdentifier = "OppositeHongBaoTemplate"
            template = this.oppositeHongBaoTemplate
        elseif data.isPic then
            cellIdentifier = "OppositePicTemplate"
            template = this.oppositePicTemplate
        elseif data.voiceAchievementId > 0 then
            cellIdentifier = "OppositeVoiceAchievementItem"
            template = this.oppositeVoiceAchievementItem
            isOpp = true
        else
            cellIdentifier = "OppositeTemplate"
            template = this.oppositeTemplate
        end
    else
        if data.isHongBao then
            cellIdentifier = "SelfHongBaoTemplate"
            template = this.selfHongBaoTemplate
        elseif data.isPic then
            cellIdentifier = "SelfPicTemplate"
            template = this.selfPicTemplate
        elseif data.voiceAchievementId > 0 then
            cellIdentifier = "SelfsiteVoiceAchievementItem"
            template = this.selfVoiceAchievementItem
            isOpp = false
        else
            cellIdentifier = "SelfTemplate"
            template = this.selfTemplate
        end
    end

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end

    local chatListItem = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CChatListBaseItem))
    if chatListItem then
        chatListItem:Init(data)
        chatListItem.onReLayoutDelegate = MakeDelegateFromCSFunction(this.OnChatListItemReLayout, MakeGenericClass(Action1, CChatListBaseItem), this)
    end
    
    local luascrip = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CCommonLuaScript))--CCommonLuaScript
    if luascrip then
        luascrip.m_LuaSelf:InitItem(data,isOpp)
    end
    

    return cell
end
CChatWnd.m_AppendChatMessages_CS2LuaHook = function (this, messages, forceDisplay) 

    if messages == nil or messages.Length == 0 then
        this:RefeshListenToZhiboLabel()
        return
    end

    local lastChildIsVisible = (this:NumberOfRows() == 0 or (this:NumberOfRows() > 0 and this.tableView:IsVisible(this:NumberOfRows() - 1)))

    if lastChildIsVisible or forceDisplay then
        this:SetNewMsgButtonVisible(false)
    else
        for i = messages.Length - 1, 0, - 1 do
            if not messages[i].usedForDisplayTime then
                this.unreadCountOfCurChannel = this.unreadCountOfCurChannel + 1
            end
        end
        this:SetNewMsgButtonVisible(true)
    end
    if this.channel == EChatPanel.WorldFilter then
        for i = 0, messages.Length-1 do
            local msg = messages[i]
            if  msg.isHongBao == false and msg.isPic == false then
                local res = CChatWndExt.ProcessFilter(messages[i].text)
                if res then 
                    CommonDefs.ListAdd_LuaCall(this.allData,messages[i])
                end
            end
        end
    else
        CommonDefs.ListAddRange(this.allData, messages)
    end

    this:RefeshListenToZhiboLabel()

    if CommonDefs.IsInWifiNetwork then
        for i = messages.Length - 1, 0, - 1 do
            this:PreDownloadVoice(messages[i])
            --预先下载声音文件
        end
    end
    if lastChildIsVisible or forceDisplay then
        --从底部加载
        this.tableView:LoadDataFromTail()
    else
        --原位置刷新
        this.tableView:UpdateIndexes(0)
    end
end
CChatWnd.m_AddChatMsg_CS2LuaHook = function (this, channelId) 

    if not this:IsChannelMatch(this.channel, channelId) then
        return
    end

    local msg = CChatMgr.Inst:GetLatestMsg(channelId)
    this:AppendChatMessages(Table2ArrayWithCount({CreateFromClass(MessageData, msg, this.channel)}, 1, MakeArrayClass(MessageData)), false)
end
CChatWnd.m_IsChannelMatch_CS2LuaHook = function (this, curchannel, channelId) 

    local channelName = CChatMgr.Inst:GetChannelNameById(channelId)
    if channelName == nil then
        return false
    end

    local echannel =  CChatHelper.GetChannelByName(channelName)
    if echannel == curchannel then 
        return true
    end

    if curchannel == EChatPanel.WorldFilter and echannel == EChatPanel.World then
        return true
    end

    return false
end
CChatWnd.m_Start_CS2LuaHook = function (this)
    this.chatInput.OnSend = CommonDefs.CombineListner_Action_string(this.chatInput.OnSend, MakeDelegateFromCSFunction(this.OnSend, MakeGenericClass(Action1, String), this), true)
    this.chatInput.needAppendToChatHistory = true
    UIEventListener.Get(this.switchButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.switchButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnSwitchButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.voiceButton.gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.voiceButton.gameObject).onPress, MakeDelegateFromCSFunction(this.OnVoiceButtonPress, BoolDelegate, this), true)
    UIEventListener.Get(this.emoticonButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.emoticonButton).onClick, MakeDelegateFromCSFunction(this.OnEmoticonButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.historyButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.historyButton).onClick, MakeDelegateFromCSFunction(this.OnHistoryButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.newMsgButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.newMsgButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnNewMsgButtonClick, VoidDelegate, this), true)
    CommonDefs.GetComponent_Component_Type(this.tableView.scrollView, typeof(UIPanel)).onClipMove = CommonDefs.CombineListner_OnClippingMoved(CommonDefs.GetComponent_Component_Type(this.tableView.scrollView, typeof(UIPanel)).onClipMove, MakeDelegateFromCSFunction(this.OnClipMove, OnClippingMoved, this), true)
    if this.chatHistoryMenu ~= nil then
        this.chatHistoryMenu.gameObject:SetActive(false)
    end
    if this.rotateButton ~= nil then
        this.rotateButton:SetActive(true)
        UIEventListener.Get(this.rotateButton).onClick = MakeDelegateFromCSFunction(this.OnRotateButtonClick, VoidDelegate, this)
    end
    if not this:IsPortraitSocialWndEnable() or Application.platform == RuntimePlatform.WindowsPlayer then
        if this.rotateButton ~= nil then
            this.rotateButton:SetActive(false)
        end
    end
    if this.outChatButton then
        this.outChatButton:SetActive(false)
    end
	if CommonDefs.IsPCGameMode() then
        if this.rotateButton ~= nil then
            this.rotateButton:SetActive(false)
        end
        if this.outChatButton then
            this.outChatButton:SetActive(true)
            UIEventListener.Get(this.outChatButton).onClick = DelegateFactory.VoidDelegate(function(go)
                local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
                local Object=import "System.Object"
                if NativeTools.IsMuMuDevice() then
                    if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
                        --关闭的时候需要确认
                        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Mumu_Device_Alert"),
                            function()
                                CWinSocialWndMgr.Inst:OpenOrCloseWinSocialWnd()
                                g_DeviceMgr.SetMuMuChatClose()

                                local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
                                CommonDefs.DictAdd_LuaCall(dict,"behaviour", "close")
                                Gac2Gas.RequestRecordClientLog("MuMu_ChatWnd_Change", MsgPackImpl.pack(dict))

                            end, nil, nil, nil, false)
                    else
                        CWinSocialWndMgr.Inst:OpenOrCloseWinSocialWnd()
                        g_DeviceMgr.SetMuMuChatOpen()

                        local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
                        CommonDefs.DictAdd_LuaCall(dict,"behaviour", "open")
                        Gac2Gas.RequestRecordClientLog("MuMu_ChatWnd_Change", MsgPackImpl.pack(dict))
                    end
                else
                    CWinSocialWndMgr.Inst:OpenOrCloseWinSocialWnd()
                end
            end)
        end
    end

    local filterButton = this.transform:FindChild("Anchor/AutoVoice/FilterPanel/FilterButton")
    if filterButton then
        UIEventListener.Get(filterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CChatWndExt.OnFilterButtonClick(this)
        end)
    end
end
CChatWnd.m_SaveTempInput_CS2LuaHook = function (this) 
    this.chatInput:SaveTempInput(CChatHistoryMgr.EnumTempInputType.Chat)
end
CChatWnd.m_RegisterEvents_CS2LuaHook = function (this)
    EventManager.AddListenerInternal(EnumEventType.RecvChatMsg, MakeDelegateFromCSFunction(this.OnRecvChatMsg, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnChatOfflinePushMsgEnd, MakeDelegateFromCSFunction(this.OnChatOfflinePushMsgEnd, MakeGenericClass(Action1, UInt32), this))
    EventManager.AddListener(EnumEventType.OnSendChatMsgSuccess, MakeDelegateFromCSFunction(this.ClearChatInput, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnWithdrawPlayerChatMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action2, UInt64, MakeGenericClass(HashSet, String)), this))
    EventManager.AddListenerInternal(EnumEventType.OnJoinCCStream, MakeDelegateFromCSFunction(this.OnJoinCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.AddListenerInternal(EnumEventType.OnQuitCCStream, MakeDelegateFromCSFunction(this.OnQuitCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.AddListener(EnumEventType.OnGuildCommanderUpdate, MakeDelegateFromCSFunction(this.OnGuildCommanderUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.OnCanSetGuildCommanderRightChanged, MakeDelegateFromCSFunction(this.OnCanSetGuildCommanderRightChanged, Action0, this))
    CChatHistoryMgr.Inst.m_OnRecordSelect = CommonDefs.CombineListner_Action_string_Dictionary_int_CChatInputLink(CChatHistoryMgr.Inst.m_OnRecordSelect, MakeDelegateFromCSFunction(this.OnHistorySelect, MakeGenericClass(Action2, String, MakeGenericClass(Dictionary, Int32, CChatInputLink)), this), true)
    g_ScriptEvent:AddListener("OnGuildAIDCommanderUpdate", CChatWndExt, "OnGuildAIDCommanderUpdate")
    g_ScriptEvent:AddListener("OnWithdrawPlayerChatVoiceMsg", CChatWndExt, "OnWithdrawPlayerChatVoiceMsg")
end
CChatWnd.m_UnRegisterEvents_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.RecvChatMsg, MakeDelegateFromCSFunction(this.OnRecvChatMsg, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnChatOfflinePushMsgEnd, MakeDelegateFromCSFunction(this.OnChatOfflinePushMsgEnd, MakeGenericClass(Action1, UInt32), this))
    EventManager.RemoveListener(EnumEventType.OnSendChatMsgSuccess, MakeDelegateFromCSFunction(this.ClearChatInput, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnWithdrawPlayerChatMsg, MakeDelegateFromCSFunction(this.OnWithdrawPlayerMsg, MakeGenericClass(Action2, UInt64, MakeGenericClass(HashSet, String)), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnJoinCCStream, MakeDelegateFromCSFunction(this.OnJoinCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnQuitCCStream, MakeDelegateFromCSFunction(this.OnQuitCCStream, MakeGenericClass(Action1, EnumCCMiniStreamType), this))
    EventManager.RemoveListener(EnumEventType.OnGuildCommanderUpdate, MakeDelegateFromCSFunction(this.OnGuildCommanderUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnCanSetGuildCommanderRightChanged, MakeDelegateFromCSFunction(this.OnCanSetGuildCommanderRightChanged, Action0, this))
    CChatHistoryMgr.Inst.m_OnRecordSelect = CommonDefs.CombineListner_Action_string_Dictionary_int_CChatInputLink(CChatHistoryMgr.Inst.m_OnRecordSelect, MakeDelegateFromCSFunction(this.OnHistorySelect, MakeGenericClass(Action2, String, MakeGenericClass(Dictionary, Int32, CChatInputLink)), this), false)
    g_ScriptEvent:RemoveListener("OnGuildAIDCommanderUpdate", CChatWndExt, "OnGuildAIDCommanderUpdate")
    g_ScriptEvent:RemoveListener("OnWithdrawPlayerChatVoiceMsg", CChatWndExt, "OnWithdrawPlayerChatVoiceMsg")
end
CChatWnd.m_ClearChatInput_CS2LuaHook = function (this) 

    if this.channel ~= EChatPanel.Friend then
        this.chatInput:Clear()
    end
end
CChatWnd.m_OnSwitchButtonClick_CS2LuaHook = function (this, go) 

    CChatWnd.useVoiceInput = not CChatWnd.useVoiceInput
    this:UpdateInputType()
end
CChatWnd.m_OnVoiceButtonPress_CS2LuaHook = function (this, go, pressed) 

    if pressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.Chat, this.channel, go, EnumVoicePlatform.Default, false)
        this.voiceButton.Text = LocalString.GetString("松开结束")
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
        this.voiceButton.Text = LocalString.GetString("按下发言")
    end
end
CChatWnd.m_OnEmoticonButtonClick_CS2LuaHook = function (this, go) 

    CChatInputMgr.ShowChatInputWnd(CChatInputMgr.EParentType.ChatWnd, this, go, this.channel, 0, 0)
end
CChatWnd.m_OnHistoryButtonClick_CS2LuaHook = function (this, go) 
    if this.chatHistoryMenu ~= nil then
        this.chatHistoryMenu:Init()
    end
end
CChatWnd.m_OnHistorySelect_CS2LuaHook = function (this, text, existingLinks) 
    this.chatInput:RelaceContent(text, existingLinks)
    if this.chatHistoryMenu ~= nil then
        this.chatHistoryMenu.gameObject:SetActive(false)
    end
end
CChatWnd.m_PreDownloadVoice_CS2LuaHook = function (this, data) 

    local voiceMsg = CVoiceMsg.Parse(data.text)
    if voiceMsg ~= nil then
        if not System.IO.File.Exists(L10.Game.HTTPHelper.CACHE_DIR .. voiceMsg.VoiceId) then
            L10.Game.HTTPHelper.DownloadVoice(CreateFromClass(VoicePlayInfo, voiceMsg.VoiceId, EnumVoicePlatform.Default, nil), nil)
        end
    end
end
CChatWnd.m_SetNewMsgButtonVisible_CS2LuaHook = function (this, visible) 

    if visible then
        if not this.newMsgButton.gameObject.activeSelf then
            this.newMsgButton.gameObject:SetActive(true)
        end
        this.newMsgButton.Text = tostring(this.unreadCountOfCurChannel) .. LocalString.GetString("条未读消息")
    else
        this.unreadCountOfCurChannel = 0
        if this.newMsgButton.gameObject.activeSelf then
            this.newMsgButton.gameObject:SetActive(false)
        end
    end
end
CChatWnd.m_OnNewMsgButtonClick_CS2LuaHook = function (this, go) 

    this:SetNewMsgButtonVisible(false)
    this.tableView:LoadDataFromTail()
end
CChatWnd.m_OnClipMove_CS2LuaHook = function (this, panel) 

    local lastChildIsVisible = (this:NumberOfRows() == 0 or (this:NumberOfRows() > 0 and this.tableView:IsVisible(this:NumberOfRows() - 1)))

    if lastChildIsVisible then
        this:SetNewMsgButtonVisible(false)
    end
end
CChatWnd.m_OnChatListItemReLayout_CS2LuaHook = function (this, baseItem) 

    if baseItem == nil or this == nil or not this.enabled then
        return
    end
    this.tableView:UpdateLayout(baseItem.transform)
end

CChatWnd.m_Awake_CS2LuaHook = function (this) 
    CChatWndExt:InitWnd(this)
end
CChatWnd.m_OnDestroy_CS2LuaHook = function (this) 
    CChatWndExt:DestroyWnd(this)
end

local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CQnSymbolParser = import "CQnSymbolParser"
local CVoiceMgr = import "L10.Game.CVoiceMgr"
CChatWndExt = {}

CChatWndExt.m_Wnd = nil

function CChatWndExt.InitFilterWordCtrl(pwnd,channel)
    local fwview = pwnd.transform:FindChild("Anchor/FilterWordView")
    if fwview == nil then return end
    local view = fwview.gameObject
    if channel == EChatPanel.WorldFilter then
        view:SetActive(true)
        local script = view:GetComponent(typeof(CCommonLuaScript))
        script.m_LuaSelf:Init(pwnd)
    else
        view:SetActive(false)
    end
end

function CChatWndExt.InitFilterLanguageCtrl(pwnd,channel)
    local filterPanel = pwnd.transform:FindChild("Anchor/AutoVoice/FilterPanel")
    if filterPanel == nil then return end
    if channel == EChatPanel.World then
        filterPanel.gameObject:SetActive(true)
        local setLanguageView = filterPanel:Find("SetLanguageView")
        setLanguageView.gameObject:SetActive(false)
        filterPanel:Find("FilterButton/Highlight").gameObject:SetActive(false)
        
        local languageItemList = {}
        for i = 1, LocalString.languageCount-1 do
            table.insert(languageItemList, setLanguageView:Find("Anchor/Offset/Table/Language" .. i))
        end

        local filterLanguageValue = PlayerPrefs.GetInt("filterLanguageInChatWnd", math.pow(2, LocalString.languageCount)-1)
        local currentSelectLanguage = setLanguageView:Find("Anchor/Offset/CurrentSelectLanguage")
        local itemIndex = 1
        for i = 0, LocalString.languageCount-1 do
            if i ~= LocalString.languageId then
                languageItemList[itemIndex]:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetLanguageName(i, false)
                local curLanguageIsOpen = bit.band(filterLanguageValue, bit.lshift(1, i)) > 0
                languageItemList[itemIndex]:Find("Box/Checkmark").gameObject:SetActive(curLanguageIsOpen)
                local t = itemIndex
                UIEventListener.Get(languageItemList[itemIndex].gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    filterLanguageValue = bit.bxor(filterLanguageValue, bit.lshift(1, i))
                    languageItemList[t]:Find("Box/Checkmark").gameObject:SetActive(bit.band(filterLanguageValue, bit.lshift(1, i)) > 0)
                    PlayerPrefs.SetInt("filterLanguageInChatWnd", filterLanguageValue)
                end)

                itemIndex = itemIndex + 1
            end
        end
        currentSelectLanguage:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetLanguageName(LocalString.languageId, false)
    else
        filterPanel.gameObject:SetActive(false)
    end
end

function CChatWndExt.OnFilterButtonClick(pwnd)
    local setLanguageView = pwnd.transform:FindChild("Anchor/AutoVoice/FilterPanel/SetLanguageView")
    local highlight = pwnd.transform:FindChild("Anchor/AutoVoice/FilterPanel/FilterButton/Highlight")
    if setLanguageView then
        highlight.gameObject:SetActive(not setLanguageView.gameObject.activeSelf)
        setLanguageView.gameObject:SetActive(not setLanguageView.gameObject.activeSelf)
    end
end



function CChatWndExt.ProcessFilter(msg)
    if LuaChatMgr.FilterWords == nil then return false end

    local str = ""
    local voiceMsg = CVoiceMsg.Parse(msg)
    if voiceMsg ~= nil then     --语音，内容变更为语音内容
        str = voiceMsg.Text
        if System.String.IsNullOrEmpty(str) then
            str = CVoiceMgr.Inst:GetVoiceText(voiceMsg.VoiceId)
        end
    else
        str = CQnSymbolParser.FilterExceededEmoticons(msg,0)--过滤表情
        str = NGUIText.StripSymbols(CQnSymbolParser.ConvertQnTextToNGUIText(str))--过滤ngui标签
    end

    for i=1,#LuaChatMgr.FilterWords do
        local fw = LuaChatMgr.FilterWords[i]
        if fw.Enabled and not System.String.IsNullOrEmpty(fw.Word) then
            if CommonDefs.StringIndexOf_String(str,fw.Word) >= 0 then
                return true
            end
        end
    end

    return false
end

function CChatWndExt:InitWnd(this)
    self.m_Wnd = this
end

function CChatWndExt:DestroyWnd(this)
    if self.m_Wnd == this then --这里有个非常隐晦的时机问题，从常规聊天切到竖版聊天时常规聊天调用OnDestroy之前竖版聊天就初始化完成了，这里最小化的修复一下
        self.m_Wnd = nil
    end
end

function CChatWndExt:OnGuildAIDCommanderUpdate()
    local this = self.m_Wnd
    if this.channel == EChatPanel.AID and CCCChatMgr.Inst.EnableGuildCC then
        this:Init(this.channel)
    end
end

function CChatWndExt:SetGuildAIDCCInfoVisibility(channel)
    local this = self.m_Wnd
    if not this then return end
    local visible = (channel == EChatPanel.AID and CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCCChatMgr.Inst.EnableGuildCC and LuaChatMgr:GuildAIDCommanderExists())
    this.guildAIDCCBg.gameObject:SetActive(visible)
    if visible then
        this.guildAIDCCBg:ResetAndUpdateAnchors()
    end
end

function CChatWndExt:OnWithdrawPlayerChatVoiceMsg(args)
    local this = self.m_Wnd
    local playerId = args[0]
    local channelId = args[1]
    local voiceId = args[2]
    local channelName = CChatMgr.Inst:GetChannelNameById(channelId)
    local channel = CChatHelper.GetChannelByName(channelName)
    if channel == this.channel then
        this:Init(this.channel)
    end
end