LuaZongMenRenMianMessageBoxWnd = class()

RegistChildComponent(LuaZongMenRenMianMessageBoxWnd,"m_TextLabel1","TextLabel1", UILabel)
RegistChildComponent(LuaZongMenRenMianMessageBoxWnd,"m_TextLabel2","TextLabel2", UILabel)
RegistChildComponent(LuaZongMenRenMianMessageBoxWnd,"m_OkButton","OkButton", GameObject)
RegistChildComponent(LuaZongMenRenMianMessageBoxWnd,"m_CancelButton","CancelButton", GameObject)

function LuaZongMenRenMianMessageBoxWnd:Init()
    self.m_TextLabel1.text = CUICommonDef.TranslateToNGUIText(LuaZongMenMgr.m_ConfirmRenMianMsg1) 
    self.m_TextLabel2.text = CUICommonDef.TranslateToNGUIText(LuaZongMenMgr.m_ConfirmRenMianMsg2)
    LuaZongMenMgr.m_ConfirmRenMianMsg1,LuaZongMenMgr.m_ConfirmRenMianMsg2 = "",""
    UIEventListener.Get(self.m_CancelButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI(CLuaUIResources.ZongMenRenMianMessageBoxWnd)
    end)
    UIEventListener.Get(self.m_OkButton).onClick = DelegateFactory.VoidDelegate(function (p)
        if CClientMainPlayer.Inst then
            Gac2Gas.RequestBatchChangeSectOffice(CClientMainPlayer.Inst.BasicProp.SectId,LuaZongMenMgr.m_ConfirmRenMianRmTbl,LuaZongMenMgr.m_ConfirmRenMianAddTbl)
            CUIManager.CloseUI(CLuaUIResources.ZongMenRenMianMessageBoxWnd)
            if LuaZongMenMgr.m_IsChangedZhangMenWeiJie then
                CUIManager.CloseUI(CLuaUIResources.ZongMenWeiJieWnd)
                LuaZongMenMgr.m_IsChangedZhangMenWeiJie = false
            end
        end   
    end)
end