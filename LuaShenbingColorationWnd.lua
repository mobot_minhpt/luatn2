local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local Object = import "System.Object"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local UITexture = import "UITexture"
local UIEventListener = import "UIEventListener"
local DelegateFactory = import "DelegateFactory"
local Screen = import "UnityEngine.Screen"
local NGUIText = import "NGUIText"
local LuaUtils = import "LuaUtils"
local QnTableView = import "L10.UI.QnTableView"
local Initialization_Init = import "L10.Game.Initialization_Init"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local CUITexture = import "L10.UI.CUITexture"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local EnumPersistPlayDataKey = import "L10.Game.EnumPersistPlayDataKey"
local EnumGameSetting = import "L10.Game.EnumGameSetting"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EnumPropertyAppearanceSettingBit = import "L10.Game.CPropertyAppearance+EnumPropertyAppearanceSettingBit"


CLuaShenbingColorationWnd = class()
RegistClassMember(CLuaShenbingColorationWnd, "m_EquipList")
RegistClassMember(CLuaShenbingColorationWnd, "m_TableView")
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentRowIndex")
RegistClassMember(CLuaShenbingColorationWnd, "m_EquipPlace")
RegistClassMember(CLuaShenbingColorationWnd, "m_EquipPos")
RegistClassMember(CLuaShenbingColorationWnd, "m_EquipId")
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentRegionIndex")

--Model
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentRotation")
RegistClassMember(CLuaShenbingColorationWnd, "m_ModelTexture")
RegistClassMember(CLuaShenbingColorationWnd, "m_ModelCameraName")
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentRenderObject")
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentAppearance")
RegistClassMember(CLuaShenbingColorationWnd, "m_MainPlayerAppearance")
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentColorPosition")
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentEquip")
RegistClassMember(CLuaShenbingColorationWnd, "m_CurrentEquipType")

--Coloration
RegistClassMember(CLuaShenbingColorationWnd, "m_QnRadioBox")
RegistClassMember(CLuaShenbingColorationWnd, "m_ColorObjTable")
RegistClassMember(CLuaShenbingColorationWnd, "m_AddObjTable")
RegistClassMember(CLuaShenbingColorationWnd, "m_VInput")
RegistClassMember(CLuaShenbingColorationWnd, "m_SInput")

-- {0, hid, hcnt, vid, vcnt, sid, scnt}
RegistClassMember(CLuaShenbingColorationWnd, "m_CacheColorIdTable")
RegistClassMember(CLuaShenbingColorationWnd, "m_HValue")
RegistClassMember(CLuaShenbingColorationWnd, "m_SValue")
RegistClassMember(CLuaShenbingColorationWnd, "m_VValue")

--Setting
RegistClassMember(CLuaShenbingColorationWnd, "m_SettingButtonsTable")
RegistClassMember(CLuaShenbingColorationWnd, "m_SettingValueTable")
RegistClassMember(CLuaShenbingColorationWnd, "m_HideBackColorCheckbox")

function CLuaShenbingColorationWnd:IsEquipFit(templateId)
	if not CClientMainPlayer.Inst then return false end
	local template = EquipmentTemplate_Equip.GetData(templateId)
	if not template then return false end
	local cls = CClientMainPlayer.Inst.Class
	local gender = CClientMainPlayer.Inst.Gender
	local professionFit = false
	if template.Race then
		for i = 0, template.Race.Length - 1 do
			if CommonDefs.ConvertIntToEnum(typeof(EnumClass), template.Race[i]) == cls then
				professionFit = true
				break
			end
		end
	end
	if not professionFit then return false end

	if template.Sex then
		for i = 0, template.Sex.Length - 1 do
			if CommonDefs.ConvertIntToEnum(typeof(EnumGender), template.Sex[i]) == gender then
				return true
			end
		end
	end
	return false
end

function CLuaShenbingColorationWnd:InitEquipList()
	self.m_EquipList = {}
	local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
	for i = 1, #placeTable do
		local equipList = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(placeTable[i])
		for i = 0, equipList.Count - 1 do
			local equip = CItemMgr.Inst:GetById(equipList[i].itemId)
			if equip ~= nil and equip.IsEquip and equip.Equip.HolyTrainLevel >= 5 and self:IsEquipFit(equip.TemplateId) then
				local designType = EquipmentTemplate_Equip.GetData(equip.TemplateId).Type
				-- only equipment, body
				if designType == 1 or designType == 4 then
					if CClientMainPlayer.Inst and CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class) then
						table.insert(self.m_EquipList, equipList[i])
					else
						if equip.Equip.SubHand == 0 then
							table.insert(self.m_EquipList, equipList[i])
						end
					end
				end
			end
		end
	end

	if #self.m_EquipList <= 0 then
		g_MessageMgr:ShowMessage("ShenBing_Coloration_No_Equip")
		CUIManager.CloseUI(CLuaUIResources.ShenBingColorationWnd)
		return
	end

	self.m_TableView = self.transform:Find("MasterView"):GetComponent(typeof(QnTableView))
	local initItem = function (item, index)
        if item ~= nil then
        	item:Init(self.m_EquipList[index + 1])
        end
    end

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(#self.m_EquipList, initItem)
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.m_TableView:ReloadData(true, false)
    self.m_TableView:SetSelectRow(0, true)
end

function CLuaShenbingColorationWnd:OnSelectAtRow(row)
	local equip = CItemMgr.Inst:GetById(self.m_EquipList[row + 1].itemId)
	if not equip then
		return
	end
	LuaShenbingMgr.ShenbingQianyuanItemTable = {}
	self.m_CurrentRowIndex = row

	local placeTable = {EnumItemPlace.Body, EnumItemPlace.Bag}
	for i = 1, #placeTable do
		local pos = CItemMgr.Inst:FindItemPosition(placeTable[i], equip.Id)
		if pos > 0 then
			self.m_EquipPlace = placeTable[i]
			self.m_EquipId = equip.Id
			self.m_EquipPos = pos
			break
		end
	end
	self:InitRadioBox(equip.TemplateId)
	self:ClearColorData()
	self:LoadEquip(equip)
end

function CLuaShenbingColorationWnd:InitRadioBox(templateId)
	local designData = EquipmentTemplate_Equip.GetData(templateId)
	local visibleGroup = {}
	local hasTwoArea = true
	if designData.Type == 3 then
		hasTwoArea = false
	elseif designData.Type == 4 then
		local initData = Initialization_Init.GetData(EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.Gender))
		hasTwoArea = initData.ShenbingColorCnt > 1
	end

	table.insert(visibleGroup, 0)
	if hasTwoArea then table.insert(visibleGroup, 1) end

	local title = hasTwoArea and LocalString.GetString("染色区域1") or LocalString.GetString("染色区域")
	self.m_QnRadioBox:SetTitle(0, title)

	self.m_QnRadioBox:SetVisibleGroup(Table2Array(visibleGroup, MakeArrayClass(Int32)), true)
end

function CLuaShenbingColorationWnd:LoadEquip(equip)
	self.m_CurrentEquip = equip
	self.m_QnRadioBox:ChangeTo(0, true)
	local interface = LuaDefaultModelTextureLoader.Create(function (ro)
		self:LoadModel(ro, equip)
	end)
	self.m_CurrentRotation = 180
	local y, z = -1, 4.66
	local designData = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	-- 傀儡
	if designData.ShenBingType == 19 then
		y, z = -1.5, 6.7
	end

	-- hide equip setting
	self.m_SettingButtonsTable[3].gameObject:SetActive(designData.ShenBingType <= 0 or designData.ShenBingType >= 100)

	local tx = CUIManager.CreateModelTexture(self.m_ModelCameraName, interface, self.m_CurrentRotation, 0.05, y, z, false, false, 1.0)
	self.m_ModelTexture.mainTexture = tx
	CUIManager.SetModelPosition(self.m_ModelCameraName, Vector3(0.05, y, z))
end

function CLuaShenbingColorationWnd:LoadModel(ro, equip)
	if not CClientMainPlayer.Inst then return end

	local appear = CClientMainPlayer.Inst.AppearanceProp:Clone()
	-- wear on it
	appear.Equipment = self:CopyCSArray(appear.Equipment)
	appear.ShenBingFirstModelId = self:CopyCSArray(appear.ShenBingFirstModelId)
	appear.ShenBingSecondModelId = self:CopyCSArray(appear.ShenBingSecondModelId)
	appear.ShenBingRanSeData = self:CopyCSArray(appear.ShenBingRanSeData)
	local designData = EquipmentTemplate_Equip.GetData(equip.TemplateId)
	if not designData then return end

	local pos = designData.Type
	if pos==1 and CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class) then
		if equip.Equip.SubHand > 0 then
			appear.Equipment[1] = 0
			pos = 2
		else
			appear.Equipment[2] = 0
		end
	end
	
	appear.Equipment[pos] = equip.TemplateId
	appear.ShenBingFirstModelId[pos] = equip.Equip.HolyFirstModelId
	appear.ShenBingSecondModelId[pos] = equip.Equip.HolySecondModelId

	-- dont hide head and body
	if designData.Type == 3 then -- 头盔
		appear.HideShenBingHeadwear = 0
	elseif designData.Type == 4 then -- 身体
		appear.HideShenBingClothes = 0
	end

	-- equipment type to color pos
	local type2Pos = {1, 4, 3, 2}
	appear.ShenBingRanSeData[type2Pos[pos]] = equip.Equip.ShenBingColor

	-- apply the settings
	local hideFx = LuaShenbingMgr.ColorationSettingValueTable[1] ~= 1
	appear.HideSuitIdToOtherPlayer = hideFx and 1 or 0
	appear.HideGemFxToOtherPlayer = hideFx and 1 or 0
	appear.IntesifySuitId = hideFx and 0 or appear.IntesifySuitId
	local hideWing = LuaShenbingMgr.ColorationSettingValueTable[2] ~= 1
	appear.HideWing = hideWing and 1 or 0
	-- hide equip and shield for body and head
	if designData.Type ~= 1 and LuaShenbingMgr.ColorationSettingValueTable[3] ~= 1 then
		appear.Equipment[1] = 0
		appear.Equipment[2] = 0
	end


	-- change fx to fit equip
	local fxId = LuaShenbingMgr:GetWeaponFxID(designData.ShenBingType,equip.Equip.HolyTrainLevel)
	appear.ShenBingTrainLvFxId[pos] = fxId

	-- need to hide the fasion and taben and switch to feisheng
	appear.FeiShengAppearanceXianFanStatus = 1
	appear.HideBodyFashionEffect = 1
	appear.HideHeadFashionEffect = 1
	appear.HideWeaponFashionEffect = 1

	--TOTO: delete it
	if designData.Type == 3 or designData.Type == 4 then
		appear.ShenBingFirstModelId[designData.Type] = 1
	end

	--self:ClearCSArray(appear.ShenBingTrainLvFxId)
	self.m_CurrentRenderObject = ro
	self.m_CurrentAppearance = appear
	self.m_CurrentColorPosition = type2Pos[pos]
	self.m_CurrentEquipType = designData.Type
	self.m_MainPlayerAppearance = CClientMainPlayer.Inst.AppearanceProp

	CClientMainPlayer.LoadResource(ro, appear, true, 1, 0, false, 0, true, 0, false, nil)
end

-- color 16bit
function CLuaShenbingColorationWnd:PreviewModel(region, color)
	local ret = self.m_CurrentAppearance.ShenBingRanSeData[self.m_CurrentColorPosition]
	if region == 1 then
		ret = bit.bor(bit.band(ret, bit.bnot(2^16 - 1)), color)
	elseif region == 2 then
		ret = bit.bor(bit.lshift(color, 16), bit.band(ret, 2^16 - 1))
	end
	ret = tonumber("0x"..bit.tohex(ret))
	self.m_CurrentAppearance.ShenBingRanSeData[self.m_CurrentColorPosition] = ret
	CClientMainPlayer.LoadResource(self.m_CurrentRenderObject, self.m_CurrentAppearance, true, 1, 0, false, 0, true, 0, false, nil)
end

function CLuaShenbingColorationWnd:ClearCSArray(array)
	for i = 0, array.Length - 1 do
		array[i] = 0
	end
end

function CLuaShenbingColorationWnd:CopyCSArray(src)
	local dest = {}
	for i = 0, src.Length - 1 do
		table.insert(dest, src[i])
	end
	return Table2Array(dest, MakeArrayClass(UInt32))
end

function CLuaShenbingColorationWnd:ColorShenbing( ... )
	local hid1 = self.m_CacheColorIdTable[1] and self.m_CacheColorIdTable[1].hid or 0
	local hid2 = self.m_CacheColorIdTable[2] and self.m_CacheColorIdTable[2].hid or 0
	local sid1 = self.m_CacheColorIdTable[1] and self.m_CacheColorIdTable[1].sid or 0
	local sid2 = self.m_CacheColorIdTable[2] and self.m_CacheColorIdTable[2].sid or 0
	local vid1 = self.m_CacheColorIdTable[1] and self.m_CacheColorIdTable[1].vid or 0
	local vid2 = self.m_CacheColorIdTable[2] and self.m_CacheColorIdTable[2].vid or 0

	local hct1 = self.m_CacheColorIdTable[1] and 1 or 0
	local hct2 = self.m_CacheColorIdTable[2] and 1 or 0
	local sct1 = self.m_CacheColorIdTable[1] and self.m_CacheColorIdTable[1].scnt or 0
	local sct2 = self.m_CacheColorIdTable[2] and self.m_CacheColorIdTable[2].scnt or 0
	local vct1 = self.m_CacheColorIdTable[1] and self.m_CacheColorIdTable[1].vcnt or 0
	local vct2 = self.m_CacheColorIdTable[2] and self.m_CacheColorIdTable[2].vcnt or 0

	local r1, g1, b1 = self:GetEquipColor(self.m_CurrentEquip.Equip, 1)
	local r2, g2, b2 = self:GetEquipColor(self.m_CurrentEquip.Equip, 2)
	local canColor1 = hid1 > 0 or (r1 and (sid1 > 0 or vid1 > 0))
	local canColor2 = hid2 > 0 or (r2 and (sid2 > 0 or vid2 > 0))
	if not canColor1 and not canColor2 then
		g_MessageMgr:ShowMessage("ShenBing_Coloration_No_Color")
		return
	end


	-- check preview color
	local cols = {hid1, hid2, vid1, vid2, sid1, sid2}
	local cts = {hct1, hct2, vct1, vct2, sct1, sct2}
	for i = 1, #cols do
		if cols[i] > 0 then
			local ct = self:GetColorCnt(cols[i])
			local inputvalue = cts[i]
			if ct < inputvalue then
				g_MessageMgr:ShowOkCancelMessage(ShenBing_Coloration.GetData(cols[i]).Name..LocalString.GetString("染料不足，是否立即前往制作"), function ( ... )
				CLuaShenbingColorationWnd.m_ColorType = math.floor((i + 1) / 2)
				CUIManager.ShowUI(CLuaUIResources.ShenBingColorComposeWnd)
				end, nil, nil, nil, false)
				return
			end
		end
	end

	local msg = ""
	if canColor1 and canColor2 then
		msg = g_MessageMgr:FormatMessage("COLORATION_AREA_CONFIRM", ShenBing_Coloration.GetData(hid1).Name, ShenBing_Coloration.GetData(hid2).Name)
	else
		msg = g_MessageMgr:FormatMessage("ShenBing_Coloration_Confirm")
	end
	g_MessageMgr:ShowOkCancelMessage(msg, function ( ... )
		self:ColorShenbingInternal({canColor1, canColor2})
	end, nil, nil, nil, false)
end

function CLuaShenbingColorationWnd:ColorShenbingInternal( canColorTbl )
	for i = 1, 2 do
		local hid = (self.m_CacheColorIdTable[i] and self.m_CacheColorIdTable[i].hid) or 0
		local sid = (self.m_CacheColorIdTable[i] and self.m_CacheColorIdTable[i].sid) or 0
		local scnt = (self.m_CacheColorIdTable[i] and self.m_CacheColorIdTable[i].scnt) or 0
		local vid = (self.m_CacheColorIdTable[i] and self.m_CacheColorIdTable[i].vid) or 0
		local vcnt = (self.m_CacheColorIdTable[i] and self.m_CacheColorIdTable[i].vcnt) or 0
		if canColorTbl[i] then
			local color = self:GetColor(i)
			local list = CreateFromClass(MakeGenericClass(List, Object))
			CommonDefs.ListAdd_LuaCall(list, hid)
			CommonDefs.ListAdd_LuaCall(list, 1)
			CommonDefs.ListAdd_LuaCall(list, sid)
			CommonDefs.ListAdd_LuaCall(list, scnt)
			CommonDefs.ListAdd_LuaCall(list, vid)
			CommonDefs.ListAdd_LuaCall(list, vcnt)
			Gac2Gas.RequestShenBingRanSe(self.m_EquipPlace, self.m_EquipPos, self.m_EquipId, MsgPackImpl.pack(list), i, color)
		end
	end
end

function CLuaShenbingColorationWnd:BackToDefault( ... )
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ShenBing_Coloration_Reset_Confirm"), function ( ... )
		Gac2Gas.RequestResetShenBingRanSe(self.m_EquipPlace, self.m_EquipPos, self.m_EquipId)
	end, nil, nil, nil, false)
end

function CLuaShenbingColorationWnd:ResetColorSuccess( ... )
	self:ClearColorData()
	self.m_CurrentEquip = CItemMgr.Inst:GetById(self.m_EquipId)
	self:LoadEquip(self.m_CurrentEquip)
end

function CLuaShenbingColorationWnd:Awake( ... )
	UIEventListener.Get(self.transform:Find("DevelopBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:ColorShenbing()
	end)
	UIEventListener.Get(self.transform:Find("BackToDefault").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:BackToDefault()
	end)
	UIEventListener.Get(self.transform:Find("CancelAll").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:CancelAll()
	end)
	UIEventListener.Get(self.transform:Find("TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		g_MessageMgr:ShowMessage("Shenbing_Coloration_Tips")
	end)

	self.m_ModelTexture = self.transform:Find("TextureRoot/ModelTexture"):GetComponent(typeof(UITexture))
	UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:DragModel(delta)
	end)

	local rootTrans = self.transform:Find("ColorationRoot")
	self.m_QnRadioBox = rootTrans:Find("QnRadioBox"):GetComponent(typeof(QnRadioBox))
	self.m_QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function (btn, index)
		self:SelectRegion(index)
	end)
	self.m_SInput = rootTrans:Find("SInput"):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_SInput:SetMinMax(0, 0, 1)
	self.m_SInput.onValueChanged = DelegateFactory.Action_uint(function ( ... )
		self:ChangeColor()
	end)
	self.m_VInput = rootTrans:Find("VInput"):GetComponent(typeof(QnAddSubAndInputButton))
	self.m_VInput:SetMinMax(0, 0, 1)
	self.m_VInput.onValueChanged = DelegateFactory.Action_uint(function ( ... )
		self:ChangeColor()
	end)

	self.m_AddObjTable = {}
	self.m_ColorObjTable = {}
	local types = {"H", "V", "S"}
	for i = 1, #types do
		local r = rootTrans:Find(types[i])
		table.insert(self.m_AddObjTable, r:Find("Add").gameObject)
		table.insert(self.m_ColorObjTable, r:Find("Color").gameObject)
		self.m_ColorObjTable[i]:SetActive(false)
		UIEventListener.Get(self.m_AddObjTable[i]).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:OpenColorChooseWnd(...)
		end)
		UIEventListener.Get(self.m_ColorObjTable[i]).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:OpenColorChooseWnd(...)
		end)
	end

	-- Setting
	if not LuaShenbingMgr.ColorationSettingValueTable then
		LuaShenbingMgr.ColorationSettingValueTable = {1, 1, 1}
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
			local gameSetting = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.PersistPlayData, EnumToInt(EnumPersistPlayDataKey.eCommonGameSetting))
			if gameSetting then
				local settingDict = MsgPackImpl.unpack(gameSetting.Data)
				if settingDict then
					for i = EnumToInt(EnumGameSetting.ShenBingColorationHideFx), EnumToInt(EnumGameSetting.ShenBingColorationHideEquip) do
						local val = CommonDefs.DictGetValue_LuaCall(settingDict, i)
						if val and val == "1" then
							LuaShenbingMgr.ColorationSettingValueTable[i - EnumToInt(EnumGameSetting.ShenBingColorationHideFx) + 1] = 0
						end
					end
				end
			end
		end
	end

	self.m_SettingButtonsTable = {}
	local nameTags = {"Fx", "Wing", "Equip"}
	for i = 1, #nameTags do
		local btn = self.transform:Find("Setting/"..nameTags[i]):GetComponent(typeof(QnSelectableButton))
		table.insert(self.m_SettingButtonsTable, btn)
		btn:SetSelected(LuaShenbingMgr.ColorationSettingValueTable[i] == 1, false)
		btn.OnButtonSelected = DelegateFactory.Action_bool(function ( ... )
			self:SettingBtnClick()
		end)
	end
end

function CLuaShenbingColorationWnd:SettingBtnClick()
	for i = 1, #LuaShenbingMgr.ColorationSettingValueTable do
		local val = self.m_SettingButtonsTable[i]:isSeleted() and 1 or 0
		if LuaShenbingMgr.ColorationSettingValueTable[i] ~= val then
			LuaShenbingMgr.ColorationSettingValueTable[i] = val
			Gac2Gas.SetGameSettingInfo(EnumToInt(EnumGameSetting.ShenBingColorationHideFx) + i - 1, val == 1 and "0" or "1")
		end
	end

	-- apply the settings
	local hideFx = LuaShenbingMgr.ColorationSettingValueTable[1] ~= 1
	self.m_CurrentAppearance.HideSuitIdToOtherPlayer = hideFx and 1 or 0
	self.m_CurrentAppearance.HideGemFxToOtherPlayer = hideFx and 1 or 0
	self.m_CurrentAppearance.IntesifySuitId = hideFx and 0 or self.m_MainPlayerAppearance.IntesifySuitId
	local hideWing = LuaShenbingMgr.ColorationSettingValueTable[2] ~= 1
	self.m_CurrentAppearance.HideWing = hideWing and 1 or 0
	-- hide equip and shield for body and head
	if self.m_CurrentEquipType ~= 1 then
		local hideEquip = LuaShenbingMgr.ColorationSettingValueTable[3] ~= 1
		self.m_CurrentAppearance.Equipment[1] = hideEquip and 0 or self.m_MainPlayerAppearance.Equipment[1]
		self.m_CurrentAppearance.Equipment[2] = hideEquip and 0 or self.m_MainPlayerAppearance.Equipment[2]
	end

	CClientMainPlayer.LoadResource(self.m_CurrentRenderObject, self.m_CurrentAppearance, true, 1, 0, false, 0, true, 0, false, nil)
end

function CLuaShenbingColorationWnd:SelectRegion(index)
	self.m_CurrentRegionIndex = index + 1

	--Init region
	if not self.m_CacheColorIdTable then return end
	local regionColorTable = self.m_CacheColorIdTable[self.m_CurrentRegionIndex]
	if regionColorTable then
		--H
		local hid = regionColorTable.hid or 0
		self.m_ColorObjTable[1]:SetActive(hid > 0)
		self.m_AddObjTable[1]:SetActive(hid <= 0)
		if hid > 0 then
			local color = NGUIText.ParseColor(ShenBing_Coloration.GetData(hid).RGB, 0)
			local colTex = self.m_ColorObjTable[1]:GetComponent(typeof(CUITexture))
			colTex.color = color
			colTex:LoadMaterial(LuaShenbingMgr.BasicColorTexPath)
		end

		--V
		local vid = regionColorTable.vid or 0
		self.m_ColorObjTable[2]:SetActive(vid > 0)
		self.m_AddObjTable[2]:SetActive(vid <= 0)
		if vid > 0 then
			if ShenBing_Coloration.GetData(vid).RGB == "1" then
				self.m_ColorObjTable[2]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.ValueIncColorTexPath)
			else
				self.m_ColorObjTable[2]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.ValueDecColorTexPath)
			end
			local cnt = math.min(regionColorTable.vct,self:GetColorCnt(vid))
			self.m_VInput:SetMinMax(0, self:GetColorCnt(vid), 1)
			self.m_VInput:SetValue(regionColorTable.vcnt)
		else
			self.m_VInput:SetMinMax(0, 0, 1)
		end

		--S
		local sid = regionColorTable.sid or 0
		self.m_ColorObjTable[3]:SetActive(sid > 0)
		self.m_AddObjTable[3]:SetActive(sid <= 0)
		if sid > 0 then
			if ShenBing_Coloration.GetData(sid).RGB == "1" then
				self.m_ColorObjTable[3]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.SaturationIncColorTexPath)
			else
				self.m_ColorObjTable[3]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.SaturationDecColorTexPath)
			end
			local cnt = math.min(regionColorTable.sct,self:GetColorCnt(sid))
			self.m_SInput:SetMinMax(0, cnt , 1)
			self.m_SInput:SetValue(regionColorTable.scnt)
		else
			self.m_SInput:SetMinMax(0, 0, 1)
		end
	end
end

function CLuaShenbingColorationWnd:OpenColorChooseWnd( go )
	local index = -1
	for i = 1, 3 do
		if go == self.m_AddObjTable[i] or go == self.m_ColorObjTable[i] then
			index = i
			break
		end
	end
	if index <= 0 then
		return
	end
	CLuaShenbingColorationWnd.m_ColorType = index
	CUIManager.ShowUI(CLuaUIResources.ShenBingColorChooseWnd)
end

function CLuaShenbingColorationWnd:SelectColor( id )
	local index = CLuaShenbingColorationWnd.m_ColorType
	-- H
	if index == 1 then
		self.m_AddObjTable[index]:SetActive(id <= 0)
		self.m_ColorObjTable[index]:SetActive(id > 0)
		self.m_CacheColorIdTable[self.m_CurrentRegionIndex].hid = id
		if id > 0 then
			local color = NGUIText.ParseColor(ShenBing_Coloration.GetData(id).RGB, 0)
			local colTex = self.m_ColorObjTable[1]:GetComponent(typeof(CUITexture))
			colTex.color = color
			colTex:LoadMaterial(LuaShenbingMgr.BasicColorTexPath)
		end
		if id > 0 then
			-- refresh the sid and vid
			local sid = self.m_CacheColorIdTable[self.m_CurrentRegionIndex].sid or 0
			if sid > 0 then self:UpdateSV(sid, 3, false) end
			local vid = self.m_CacheColorIdTable[self.m_CurrentRegionIndex].vid or 0
			if vid > 0 then self:UpdateSV(vid, 2, false) end
		elseif id <= 0 then
			self:UpdateSV(0, 3, false)
			self:UpdateSV(0, 2, false)
		end
	else
		self:UpdateSV(id, index, true)
	end
	self:ChangeColor()
end

-- Get equip color from index
function CLuaShenbingColorationWnd:GetEquipColor(equip, index)
	local color = equip.ShenBingColor
	if index == 1 then
		color = bit.band(color, 0xFFFF)
	elseif index == 2 then
		color = bit.rshift(color, 16)
	else
		return
	end
	if bit.band(color, 0x8000) <= 0 then return end
	color = bit.band(color, 0x7FFF)
	return bit.rshift(color, 10) / 31, bit.band(bit.rshift(color, 5), 31) / 31, bit.band(color, 31) / 31
end

function CLuaShenbingColorationWnd:UpdateSV(id, index, bShowMsg)
	local hid = self.m_CacheColorIdTable[self.m_CurrentRegionIndex].hid or 0
	local oriR, oriG, oriB = self:GetEquipColor(self.m_CurrentEquip.Equip, self.m_CurrentRegionIndex)
	if hid <= 0 and (not oriR) then
		if bShowMsg then
			g_MessageMgr:ShowMessage("ShenBing_Choose_Basic_Color_First")
			return
		end
	end

	self.m_AddObjTable[index]:SetActive(id <= 0)
	self.m_ColorObjTable[index]:SetActive(id > 0)

	-- clear
	if id <= 0 then
		if index == 2 then
			self.m_VInput:SetMinMax(0, 0, 1)
			self.m_CacheColorIdTable[self.m_CurrentRegionIndex].vid = 0
		elseif index == 3 then
			self.m_SInput:SetMinMax(0, 0, 1)
			self.m_CacheColorIdTable[self.m_CurrentRegionIndex].sid = 0
		end
		return
	end

	-- support no basic color
	if hid > 0 then
		local color = NGUIText.ParseColor(ShenBing_Coloration.GetData(hid).RGB, 0)
		oriR, oriG, oriB = color.r, color.g, color.b
	end

	self.m_HValue, self.m_SValue, self.m_VValue = self:RgbToHsv(oriR, oriG, oriB)

	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp then
		--local cnt = CommonDefs.DictGetValue(CClientMainPlayer.Inst.ItemProp.ShenBingRanLiao, typeof(UInt32), id)
		-- For previewing saturation and value
		local cnt = 100

		local needCnt = 0
		-- V
		if index == 2 then
			if ShenBing_Coloration.GetData(id).RGB == "1" then
				self.m_ColorObjTable[2]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.ValueIncColorTexPath)
				needCnt = math.max(0, math.floor((ShenBing_Setting.GetData().ColorationVMax - self.m_VValue) / ShenBing_Setting.GetData().ColorationVDelta))
			else
				self.m_ColorObjTable[2]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.ValueDecColorTexPath)
				needCnt = math.max(0, math.floor((self.m_VValue - ShenBing_Setting.GetData().ColorationVMin) / ShenBing_Setting.GetData().ColorationVDelta))
			end
			cnt = math.min(cnt, needCnt)
			self.m_VInput:SetMinMax(0, cnt, 1)
			if cnt > 0 then self.m_VInput:SetValue(1) end
			self.m_CacheColorIdTable[self.m_CurrentRegionIndex].vid = id
			self.m_CacheColorIdTable[self.m_CurrentRegionIndex].vct = cnt
		elseif index == 3 then
			if ShenBing_Coloration.GetData(id).RGB == "1" then
				self.m_ColorObjTable[3]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.SaturationIncColorTexPath)
				needCnt = math.floor((1 - self.m_SValue) / ShenBing_Setting.GetData().ColorationSDelta)
			else
				self.m_ColorObjTable[3]:GetComponent(typeof(CUITexture)):LoadMaterial(LuaShenbingMgr.SaturationDecColorTexPath)
				needCnt = math.max(0, math.floor((self.m_SValue - ShenBing_Setting.GetData().ColorationSMin) / ShenBing_Setting.GetData().ColorationSDelta))
			end
			cnt = math.min(cnt, needCnt)
			self.m_SInput:SetMinMax(0, cnt, 1)
			if cnt > 0 then self.m_SInput:SetValue(1) end
			self.m_CacheColorIdTable[self.m_CurrentRegionIndex].sid = id
			self.m_CacheColorIdTable[self.m_CurrentRegionIndex].sct = cnt
		end
	end
end

function CLuaShenbingColorationWnd:GetColorCnt(id)
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.ItemProp then
		return 0
	end
	local cnt = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.ItemProp.ShenBingRanLiao, id)
	if not cnt then return 0 end
	return cnt
end

function CLuaShenbingColorationWnd:ClearColorData( ... )
	self.m_CacheColorIdTable = {{}, {}}
	for i = 1, 3 do
		self.m_AddObjTable[i]:SetActive(true)
		self.m_ColorObjTable[i]:SetActive(false)
	end
	self.m_SInput:SetMinMax(0, 0, 1)
	self.m_VInput:SetMinMax(0, 0, 1)
end

function CLuaShenbingColorationWnd:ShenBingColorationSuccess( ... )
	self:ClearColorData()
end

function CLuaShenbingColorationWnd:GetColor(regionIndex)
	local hid = self.m_CacheColorIdTable[regionIndex].hid or 0
	--if hid <= 0 then return 0 end
	--Support no basic color
	local r, g, b
	if hid > 0 then
		local oriColor = tonumber("0x" .. ShenBing_Coloration.GetData(hid).RGB)
		r, g, b = bit.rshift(bit.band(oriColor, 0x00FF0000), 16) / 255, bit.rshift(bit.band(oriColor, 0x0000FF00), 8) / 255, bit.band(oriColor, 0x000000FF) / 255
	else
		r, g, b = self:GetEquipColor(self.m_CurrentEquip.Equip, regionIndex)
		if not r then return 0 end
	end
	local h, s, v = self:RgbToHsv(r, g, b)
	self.m_HValue, self.m_SValue, self.m_VValue = h, s, v
	local sid = self.m_CacheColorIdTable[regionIndex].sid or 0
	if sid > 0 then
		local val = self.m_CacheColorIdTable[regionIndex].scnt or 0
		if val > 0 then
			val = val * ShenBing_Setting.GetData().ColorationSDelta
			--add Saturation
			if ShenBing_Coloration.GetData(sid).RGB == "1" then
				s = s + val
			else
				s = s - val
			end
		end
	end
	local vid = self.m_CacheColorIdTable[regionIndex].vid or 0
	if vid > 0 then
		local val = self.m_CacheColorIdTable[regionIndex].vcnt or 0
		if val > 0 then
			val = val *ShenBing_Setting.GetData().ColorationVDelta
			--add Value
			if ShenBing_Coloration.GetData(vid).RGB == "1" then
				v = v + val
			else
				v = v - val
			end
		end
	end
	r, g, b = self:HsvToRgb(h, s, v)
	r, g, b = math.floor(r*31), math.floor(g*31), math.floor(b*31)
	local color = bit.bor(bit.lshift(1, 15), bit.lshift(r, 10), bit.lshift(g, 5), b)
	return color
end

function CLuaShenbingColorationWnd:ChangeColor()
	local hid = self.m_CacheColorIdTable[self.m_CurrentRegionIndex].hid or 0
	--if hid <= 0 then return end

	-- store the count
	local regionColorTable = self.m_CacheColorIdTable[self.m_CurrentRegionIndex]
	if regionColorTable then
		if regionColorTable.sid then
			regionColorTable.scnt = self.m_SInput:GetValue()
		end
		if regionColorTable.vid then
			regionColorTable.vcnt = self.m_VInput:GetValue()
		end
	end

	self:PreviewModel(self.m_CurrentRegionIndex, self:GetColor(self.m_CurrentRegionIndex))
end

function CLuaShenbingColorationWnd:CancelAll( ... )
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("ShenBing_Coloration_Cancel_Confirm"), function ( ... )
		self:ClearColorData()
		self:LoadEquip(self.m_CurrentEquip)
	end, nil, nil, nil, false)
end

function CLuaShenbingColorationWnd:DragModel(delta)
	local deltaVal = -delta.x / Screen.width * 360
	self.m_CurrentRotation = self.m_CurrentRotation + deltaVal
	CUIManager.SetModelRotation(self.m_ModelCameraName, self.m_CurrentRotation)
end

function CLuaShenbingColorationWnd:Init()
	self.m_ModelCameraName = "__ShenBingColorationWnd__"
	self:InitEquipList()
	self:InitBackColorSetting()
end

function CLuaShenbingColorationWnd:OnDestroy( ... )
	CUIManager.DestroyModelTexture(self.m_ModelCameraName)
end

function CLuaShenbingColorationWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("ShenbingColorSelected", self, "SelectColor")
	g_ScriptEvent:AddListener("ResetShenBingColorSuccess", self, "ResetColorSuccess")
	g_ScriptEvent:AddListener("ShenBingColorationSuccess", self, "ShenBingColorationSuccess")
	g_ScriptEvent:AddListener("MainPlayerAppearanceUpdate", self, "MainPlayerAppearanceUpdate")
end

function CLuaShenbingColorationWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("ShenbingColorSelected", self, "SelectColor")
	g_ScriptEvent:RemoveListener("ResetShenBingColorSuccess", self, "ResetColorSuccess")
	g_ScriptEvent:RemoveListener("ShenBingColorationSuccess", self, "ShenBingColorationSuccess")
	g_ScriptEvent:RemoveListener("MainPlayerAppearanceUpdate", self, "MainPlayerAppearanceUpdate")
end

function CLuaShenbingColorationWnd:MainPlayerAppearanceUpdate()
	self:UpdateBackColorSetting()
end

function CLuaShenbingColorationWnd:InitBackColorSetting()
	local hideBackColorCheckbox = self.transform:Find("HideBackColorCheckbox"):GetComponent(typeof(QnCheckBox))
	hideBackColorCheckbox.gameObject:SetActive(false)
	if not CClientMainPlayer.Inst then return end
	if Initialization_Init.GetData(EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.AppearanceGender)).ShenbingBackColorCnt <= 0 then return end
	hideBackColorCheckbox.gameObject:SetActive(true)
	hideBackColorCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
		Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.ShowShenBingBeiShiColor), checked and 0 or 1)
		self:UpdateBackColorSetting()
	end)
	self.m_HideBackColorCheckbox = hideBackColorCheckbox
	self.m_HideBackColorCheckbox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.ShowShenBingBeiShiColor == 0, true)
end

function CLuaShenbingColorationWnd:UpdateBackColorSetting()
	if self.m_HideBackColorCheckbox then
		self.m_HideBackColorCheckbox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.ShowShenBingBeiShiColor == 0, true)
		self.m_CurrentAppearance.ShowShenBingBeiShiColor = CClientMainPlayer.Inst.AppearanceProp.ShowShenBingBeiShiColor
		CClientMainPlayer.LoadResource(self.m_CurrentRenderObject, self.m_CurrentAppearance, true, 1, 0, false, 0, true, 0, false, nil)
	end
end


--[[
 * Converts an RGB color value to HSV. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and v in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSV representation
]]
function CLuaShenbingColorationWnd:RgbToHsv(r, g, b)
  --r, g, b = r / 255, g / 255, b / 255
  local max, min = math.max(r, g, b), math.min(r, g, b)
  local h, s, v
  v = max

  local d = max - min
  if max == 0 then s = 0 else s = d / max end

  if max == min then
    h = 0 -- achromatic
  else
    if max == r then
    h = (g - b) / d
    if g < b then h = h + 6 end
    elseif max == g then h = (b - r) / d + 2
    elseif max == b then h = (r - g) / d + 4
    end
    h = h / 6
  end

  return h, s, v
end

--[[
 * Converts an HSV color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes h, s, and v are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  v       The value
 * @return  Array           The RGB representation
]]
function CLuaShenbingColorationWnd:HsvToRgb(h, s, v)
  local r, g, b

  local i = math.floor(h * 6);
  local f = h * 6 - i;
  local p = v * (1 - s);
  local q = v * (1 - f * s);
  local t = v * (1 - (1 - f) * s);

  i = i % 6

  if i == 0 then r, g, b = v, t, p
  elseif i == 1 then r, g, b = q, v, p
  elseif i == 2 then r, g, b = p, v, t
  elseif i == 3 then r, g, b = p, q, v
  elseif i == 4 then r, g, b = t, p, v
  elseif i == 5 then r, g, b = v, p, q
  end

  return r, g, b
  --return r * 255, g * 255, b * 255
end
