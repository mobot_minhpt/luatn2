LuaYuanxiaoMgr = {}

function LuaYuanxiaoMgr:ShowResultWnd(myScore, teamScore)
	LuaYuanxiaoMgr.MyScore = myScore
	LuaYuanxiaoMgr.TeamScore = teamScore
	CUIManager.ShowUI("YuanxiaoResultWnd")
end

function Gas2Gac.TangYuanPlaySyncPlayInfo(score, eventRemainTime,infoU)
	local infoList = MsgPackImpl.unpack(infoU)
	if infoList.Count ~= 4 then return end

	local pi, piSAT, xian, xianSAT = infoList[0], infoList[1], infoList[2], infoList[3]
	LuaYuanxiaoMgr.pi = tonumber(pi)
	LuaYuanxiaoMgr.piSAT = tonumber(piSAT)
	LuaYuanxiaoMgr.xian = tonumber(xian)
	LuaYuanxiaoMgr.xianSAT = tonumber(xianSAT)

	LuaYuanxiaoMgr.score = score

	LuaYuanxiaoMgr.remainTime = eventRemainTime
	g_ScriptEvent:BroadcastInLua("YuanXiaoUpdateInfo")
end

function Gas2Gac.TangYuanPlayScoreAdd(bSelfGain, incScore)
	LuaYuanxiaoMgr.addScore = incScore
	LuaYuanxiaoMgr.isMyScore = bSelfGain
	g_ScriptEvent:BroadcastInLua("YuanXiaoAddScore")
end


