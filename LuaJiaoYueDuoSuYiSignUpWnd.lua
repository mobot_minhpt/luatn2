local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaJiaoYueDuoSuYiSignUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "SignUpBtn")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "SignUpBtnLabel")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "JoinReward")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "WinReward")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "MatchLabel")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "TipBtn")

RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "isMatching")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "startTime")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "lastTime")
RegistClassMember(LuaJiaoYueDuoSuYiSignUpWnd, "m_RefreshTick")


function LuaJiaoYueDuoSuYiSignUpWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUI()
    self.transform:Find("Anchor/BottomView/TimeLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("9月12日-9月18日 11:00-13:00、18:00-20:00开启匹配")
end

function LuaJiaoYueDuoSuYiSignUpWnd:InitUI()
    self.startTime = Task_Task.GetData(22029913).StartTime
    self.lastTime = Task_Task.GetData(22029913).LastTime

    self.SignUpBtn = self.transform:Find("Anchor/BottomView/OkButton")
    self.SignUpBtnLabel = self.SignUpBtn:Find("Label"):GetComponent(typeof(UILabel))
    self.JoinReward = self.transform:Find("Anchor/TopView/Reward/Item1")
    self.WinReward = self.transform:Find("Anchor/TopView/Reward/Item2")
    self.MatchLabel = self.transform:Find("Anchor/BottomView/MatchLabel")
    self.TipBtn = self.transform:Find("Anchor/BottomView/TipButton")

    self:InitOneItem(self.JoinReward.gameObject, 21052370)
    self:InitOneItem(self.WinReward.gameObject, 21052371)

    local joinReward = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eZhongQiu2022JYDSY_PartakeReward)
    local winReward = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eZhongQiu2022JYDSY_WinReward)
    if joinReward and joinReward ~= 0 then
        self.JoinReward:Find("Mask").gameObject:SetActive(true)
        self.JoinReward:Find("CheckboxSprite").gameObject:SetActive(true)
    else
        self.JoinReward:Find("Mask").gameObject:SetActive(false)
        self.JoinReward:Find("CheckboxSprite").gameObject:SetActive(false)
    end
    if winReward and winReward ~= 0 then
        self.WinReward:Find("Mask").gameObject:SetActive(true)
        self.WinReward:Find("CheckboxSprite").gameObject:SetActive(true)
    else
        self.WinReward:Find("Mask").gameObject:SetActive(false)
        self.WinReward:Find("CheckboxSprite").gameObject:SetActive(false)
    end

    CommonDefs.AddOnClickListener(
        self.SignUpBtn.gameObject, 
        DelegateFactory.Action_GameObject(function()
            self:OnSignUpBtnClick()
        end), 
        false
    )

    CommonDefs.AddOnClickListener(
        self.TipBtn.gameObject,
        DelegateFactory.Action_GameObject(function()
            g_MessageMgr:ShowMessage("2022Zhongqiu_MoonCake_Rules")
        end),
        false
    )
end

function LuaJiaoYueDuoSuYiSignUpWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaJiaoYueDuoSuYiSignUpWnd:CanJoin()
    local dd = {11, 18}
    local dt = {}
    local now = CServerTimeMgr.Inst:GetZone8Time()
    for k = 1, #dd do 
        table.insert(dt, CreateFromClass(DateTime, now.Year, now.Month, now.Day, dd[k], 0, 0))
    end

    for k = 1, #dt do 
        if now:CompareTo(dt[k]) < 0 then
            return false
        elseif now:CompareTo(dt[k]) >= 0 and now:CompareTo(dt[k]:AddMinutes(self.lastTime * 1.0)) <= 0 then
            return true
        end
    end
    return false
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnSignUpBtnClick()
    if not self:CanJoin() then
        g_MessageMgr:ShowMessage("2022Zhongqiu_MoonCakee_NotOpen")
        return
    end
    if self.isMatching then
		Gac2Gas.GlobalMatch_RequestCancelSignUp(ZhongQiu2022_Setting.GetData().MoonCakeGamePlayId)
	else
		Gac2Gas.GlobalMatch_RequestSignUp(ZhongQiu2022_Setting.GetData().MoonCakeGamePlayId)
	end
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
	self:OnState(isInMatching)
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
	if success then
		self:OnState(true)
	end
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
	if success then
		self:OnState(false)
	end
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnState(isMatching)
	self.isMatching = isMatching
	self.MatchLabel.gameObject:SetActive(isMatching)

	--local btnSp = self.SignUpBtn.transform:GetComponent(typeof(UISprite))
	if not isMatching then
		self.SignUpBtnLabel.text = LocalString.GetString("报名参加")
		--btnSp.spriteName = "common_btn_01_yellow"
	else
		self.SignUpBtnLabel.text = LocalString.GetString("取消报名")
		--btnSp.spriteName = "common_btn_01_blue"
	end
end

function LuaJiaoYueDuoSuYiSignUpWnd:Init()
    if CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(ZhongQiu2022_Setting.GetData().MoonCakeGamePlayId, CClientMainPlayer.Inst.Id)
    end

    self:CancelRefreshTick()
    self:RefreshTickFunc()
    self.m_RefreshTick = RegisterTick(function()
        self:RefreshTickFunc()
    end, 1000)
end

function LuaJiaoYueDuoSuYiSignUpWnd:OnDestroy()
    self:CancelRefreshTick()
end

--@region UIEvent

--@endregion UIEvent

function LuaJiaoYueDuoSuYiSignUpWnd:RefreshTickFunc()
    if not self:CanJoin() then
        self:OnState(false)
    end
end

function LuaJiaoYueDuoSuYiSignUpWnd:CancelRefreshTick()
    if self.m_RefreshTick then
        invoke(self.m_RefreshTick)
        self.m_RefreshTick = nil
    end
end
