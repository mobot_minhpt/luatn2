local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"

LuaGuildTerritorialWarsScoreSpeedDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsScoreSpeedDetailWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaGuildTerritorialWarsScoreSpeedDetailWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsScoreSpeedDetailWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsScoreSpeedDetailWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsScoreSpeedDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsScoreSpeedDetailWnd:Init()
    self.Template.gameObject:SetActive(false)
    local curScoreInfo = LuaGuildTerritorialWarsMgr.m_CurScoreInfo
    local obj1 = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
    obj1.gameObject:SetActive(true)
    obj1.transform:Find("LeftLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("我帮占领积分")
    local extraSpeed = 0
    for i = 0, curScoreInfo.fromSlaveSpeed.Count - 1,2 do
        local guildName, speed = curScoreInfo.fromSlaveSpeed[i], curScoreInfo.fromSlaveSpeed[i + 1]
        extraSpeed = extraSpeed + speed
        local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
        obj.gameObject:SetActive(true)
        obj.transform:Find("LeftLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("俘虏[fffe91]%s"), guildName)
        obj.transform:Find("RightLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[00ff60]+%d积分/分钟"), speed)
    end
    for i = 0, curScoreInfo.toMasterSpeed.Count - 1,2 do
        local guildName, speed = curScoreInfo.toMasterSpeed[i], curScoreInfo.toMasterSpeed[i + 1]
        extraSpeed = extraSpeed - speed
        local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
        obj.gameObject:SetActive(true)
        obj.transform:Find("LeftLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("被[fffe91]%s[ffffff]俘虏"), guildName)
        obj.transform:Find("RightLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[ff5050]%d积分/分钟"), -speed)
    end
    obj1.transform:Find("RightLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("+%d积分/分钟"), curScoreInfo.scoreSpeed - extraSpeed)
    self.Grid:Reposition()
    self.Background.height = self.Background.height + (curScoreInfo.fromSlaveSpeed.Count/ 2 + curScoreInfo.toMasterSpeed.Count/2) * self.Grid.cellHeight
end

--@region UIEvent

--@endregion UIEvent

