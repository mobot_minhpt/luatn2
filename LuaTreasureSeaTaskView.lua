local CTeamMgr = import "L10.Game.CTeamMgr"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaTreasureSeaTaskView = class()
RegistClassMember(LuaTreasureSeaTaskView, "Table")
RegistClassMember(LuaTreasureSeaTaskView, "Bg")
RegistClassMember(LuaTreasureSeaTaskView, "LeaveBtn")
RegistClassMember(LuaTreasureSeaTaskView, "RuleBtn")
RegistClassMember(LuaTreasureSeaTaskView, "CollectLabel")
RegistClassMember(LuaTreasureSeaTaskView, "EventLabel")
RegistClassMember(LuaTreasureSeaTaskView, "CountdownLabel")

RegistClassMember(LuaTreasureSeaTaskView, "m_Tick")
RegistClassMember(LuaTreasureSeaTaskView, "m_PvpGold2Win")
RegistClassMember(LuaTreasureSeaTaskView, "m_HuntingDuration")
RegistClassMember(LuaTreasureSeaTaskView, "m_HuntingMsg")
RegistClassMember(LuaTreasureSeaTaskView, "m_CurEventInfo")

function LuaTreasureSeaTaskView:Awake()
    self.Table = self.transform:Find("Anchor/StatesRoot"):GetComponent(typeof(UITable))
    self.Bg = self.transform:Find("Anchor/Bg"):GetComponent(typeof(UISprite))
    self.LeaveBtn = self.transform:Find("Anchor/LeaveButton").gameObject
    self.RuleBtn = self.transform:Find("Anchor/RuleButton").gameObject
    self.CollectLabel = self.transform:Find("Anchor/StatesRoot/CollectLabel"):GetComponent(typeof(UILabel))
    self.EventLabel = self.transform:Find("Anchor/StatesRoot/Event/Label"):GetComponent(typeof(UILabel))
    self.CountdownLabel = self.transform:Find("Anchor/StatesRoot/Countdown/Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.LeaveBtn).onClick = DelegateFactory.VoidDelegate(function()
        local msg = CTeamMgr.Inst:MainPlayerIsTeamLeader() and g_MessageMgr:FormatMessage("NAVALWAR_PVE_TEAM_LEADER_LEAVE_SCENE") or g_MessageMgr:FormatMessage("LEAVE_COPY_CONFIRM")
            g_MessageMgr:ShowOkCancelMessage(msg, 
                function()
                    Gac2Gas.RequestLeavePlay()
                end, nil, nil, nil, false)
    end)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function()
        LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(7)
    end)

    self.m_HuntingDuration = NavalWar_Setting.GetData().PvpHuntingDelayTeleportDuration
    self.m_PvpGold2Win = NavalWar_Setting.GetData().PvpGold2Win
    self.m_HuntingMsg = {
        "HaiZhan_PVP_Hunting_Before_Teleport",
        "HaiZhan_PVP_Hunted_Before_Teleport",
        "HaiZhan_PVP_Hunting_After_Teleport",
        "HaiZhan_PVP_Hunted_After_Teleport",
    }
end

function LuaTreasureSeaTaskView:Init()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
    self:OnTick()
    self:Layout()
end

function LuaTreasureSeaTaskView:OnEnable()
    self:OnNavalPvpSyncHuntInfo(LuaHaiZhanMgr.s_HuntInfo)
    g_ScriptEvent:AddListener("NavalPvpGoldChange", self, "OnNavalPvpGoldChange")
    g_ScriptEvent:AddListener("NavalPvpSyncHuntInfo", self, "OnNavalPvpSyncHuntInfo")
end

function LuaTreasureSeaTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("NavalPvpGoldChange", self, "OnNavalPvpGoldChange")
    g_ScriptEvent:RemoveListener("NavalPvpSyncHuntInfo", self, "OnNavalPvpSyncHuntInfo")
end

function LuaTreasureSeaTaskView:OnDestroy()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = nil
end

function LuaTreasureSeaTaskView:OnTick()
    if CScene.MainScene ~= nil then
        if CScene.MainScene.ShowTimeCountDown then
            self.CountdownLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.CountdownLabel.text = nil
        end
    else
        self.CountdownLabel.text = nil
    end
    self:OnNavalPvpGoldChange()

    if self.m_CurEventInfo then
        local leftTime = math.max(0, math.floor(self.m_CurEventInfo.startTime + self.m_HuntingDuration - CServerTimeMgr.Inst.timeStamp))
        self.EventLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage(self.m_CurEventInfo.msg, self.m_CurEventInfo.name, leftTime))
        self:Layout()
    end
end

function LuaTreasureSeaTaskView:OnNavalPvpGoldChange(shipId, delta)
    if not CClientMainPlayer.Inst then return end
    local id = math.abs(CClientMainPlayer.Inst.AppearanceProp.DriverId or 0)
    local myShipId = LuaHaiZhanMgr.s_EngineId2ShipId[id] or 0
    if myShipId == 0 then myShipId = LuaHaiZhanMgr.s_MyShipId end
    if (not shipId or shipId == myShipId) then 
        self.CollectLabel.text = SafeStringFormat3(LocalString.GetString("收集[ffff00]%d[-]/%d枚金币"), LuaHaiZhanMgr.s_ShipId2Gold[myShipId] or 0, self.m_PvpGold2Win)
    end
end

-- @param huntingInfoU: local info = {
--     attShipId = shipId,
--     defShipId = targetShipId,
--     startTime = os.time(),
--     attName = attName,
--     defName = defName,
--     stage = 1,
--     eventId = eventId,
-- }
-- 如果 info 为空，表示结束狩猎
function LuaTreasureSeaTaskView:OnNavalPvpSyncHuntInfo(info)
    if info and next(info) then
        self.EventLabel.transform.parent.gameObject:SetActive(true)
        local hunting = info.attShipId == LuaHaiZhanMgr.s_MyShipId
        self.m_CurEventInfo = {
            stage = info.stage,
            startTime = info.startTime,
            msg = self.m_HuntingMsg[2 * (info.stage - 1) + (hunting and 1 or 2)],
            name = info[hunting and "defName" or "attName"],
        }
        if info.stage == 1 then
            self.EventLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage(self.m_CurEventInfo.msg, self.m_CurEventInfo.name, self.m_HuntingDuration))
        else
            self.EventLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage(self.m_CurEventInfo.msg, self.m_CurEventInfo.name))
        end
    else
        self.m_CurEventInfo = nil
        self.EventLabel.transform.parent.gameObject:SetActive(false)
    end
    self:Layout()
end

function LuaTreasureSeaTaskView:Layout()
    self.Table:Reposition()
    local contentHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + self.Table.padding.y * 2
    self.Bg.height = math.ceil(contentHeight)
end

function LuaTreasureSeaTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end