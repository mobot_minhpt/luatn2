-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CFriendListItem = import "L10.UI.CFriendListItem"
local CFriendRecentListView = import "L10.UI.CFriendRecentListView"
local CFriendView = import "L10.UI.CFriendView"
local CIMMgr = import "L10.Game.CIMMgr"
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CWeddingMgr = import "L10.Game.CWeddingMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EnumGmStatus = import "L10.Game.EnumGmStatus"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EventManager = import "EventManager"
local FriendItemData = import "FriendItemData"
local GameObject = import "UnityEngine.GameObject"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Object = import "System.Object"
local RelationshipType = import "L10.Game.RelationshipType"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"

CFriendRecentListView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    if index < 0 and index >= this.allData.Count then
        return nil
    end
    local cellIdentifier = "FriendListItemCell"
    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.friendItemTemplate, cellIdentifier)
    end

    local item = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CFriendListItem))

    local OpButtonVisible = true
    local data = this.allData[index]
    if data.type ~= CFriendListItem.ItemType.Friend then
        OpButtonVisible = false
    end
    item:Init(data, false, OpButtonVisible)
    this:UpdateInfoText(item, data.playerId)
    if OpButtonVisible then
        item.OnOperationButtonClick = MakeDelegateFromCSFunction(this.OnExtendMenuButtonClick, MakeGenericClass(Action1, UInt64), this)
    end
    local npctag = LuaGameObject.GetChildNoGC(item.transform,"NpcUnrepliedTag")
    local tf = false
    local word = ""
    if item.FriendItemType == CFriendListItem.ItemType.JingLing then
        item.AlertVisible = CJingLingMgr.Inst.ExistsUnreadMsgs or CExpertTeamMgr.Inst.ExistsNewAnswer
    else
        if LuaNPCChatMgr:IsChatNpc(data.playerId) then
            tf,word = LuaNPCChatMgr:GetResponseStr(data.playerId)
            item.AlertVisible = LuaNPCChatMgr:NeedAlert(item.PlayerId)
        else
            item.AlertVisible = CIMMgr.Inst:ShowNewMessageAlert(item.PlayerId)
        end
    end
    if npctag then
        npctag.gameObject:SetActive(tf)
        local npctaglabel = LuaGameObject.GetChildNoGC(npctag.transform,"Label").label
        npctaglabel.text = word
    end
    return cell
end
CFriendRecentListView.m_UpdateInfoText_CS2LuaHook = function (this, item, playerId) 

    if this.curTabIndex == 1 then
        local interactType = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.RelationshipProp:GetInteractType(playerId) or 0
        item.InfoText = GameSetting_Common_Wapper.Inst:GetRecentInteractionText(interactType)
    else
        if CWeddingMgr.Inst:IsMyHusband(playerId) then
            item.InfoText = LocalString.GetString("[FFAEF2]夫君[-]")
        elseif CWeddingMgr.Inst:IsMyWife(playerId) then
            item.InfoText = LocalString.GetString("[FFAEF2]娘子[-]")
        elseif CShiTuMgr.Inst:IsCurrentShiFu(playerId) then
            item.InfoText = LocalString.GetString("师父")
        elseif CShiTuMgr.Inst:IsCurrentTuDi(playerId) then
            item.InfoText = LocalString.GetString("徒弟")
        elseif CIMMgr.IM_GM_ID == playerId then
            item.InfoText = LocalString.GetString("客服")
        else
            local text = CJieBaiMgr.Inst:GetExistJieBaiRelationByID(playerId, false)
            if not System.String.IsNullOrEmpty(text) then
                item.InfoText = text
            else
                item.InfoText = nil
            end
        end
    end
end
CFriendRecentListView.m_OnRowSelected_CS2LuaHook = function (this, index) 

    local go = this.tableView:GetCellByIndex(index)
    if go ~= nil then
        local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
        if this.OnItemClickDelegate ~= nil then
            GenericDelegateInvoke(this.OnItemClickDelegate, Table2ArrayWithCount({item}, 1, MakeArrayClass(Object)))
        end
        if item.FriendItemType == CFriendListItem.ItemType.JingLing then
            this.tableView.SelectedIndex = - 1
        end
    end
end
CFriendRecentListView.m_OnEnable_CS2LuaHook = function (this) 

    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
    if this.curTabIndex < 0 or this.curTabIndex > 1 then
        this.curTabIndex = 0
    end
    this.tabBar:ChangeTab(this.curTabIndex, false)
    EventManager.AddListenerInternal(EnumEventType.OnIMMsgReadStatusChanged, MakeDelegateFromCSFunction(this.UpdateFriendListAlert_WithPlayerId, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.MainplayerRelationshipBasicInfoUpdate, MakeDelegateFromCSFunction(this.MainplayerRelationshipBasicInfoUpdate, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.OnUpdateSpecialFriendInfo, MakeDelegateFromCSFunction(this.OnUpdateSpecialFriendInfo, Action0, this))
    EventManager.AddListener(EnumEventType.OnJingLingIMReadStatusChanged, MakeDelegateFromCSFunction(this.OnJingLingIMReadStatusChanged, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.MainplayerInteractTypeUpdate, MakeDelegateFromCSFunction(this.OnMainplayerInteractTypeUpdate, MakeGenericClass(Action2, UInt64, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.OnShiTuRelationshipChanged, MakeDelegateFromCSFunction(this.OnShiTuRelationshipChanged, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.OnWeddingRelationshipChanged, MakeDelegateFromCSFunction(this.OnWeddingRelationshipChanged, MakeGenericClass(Action1, UInt64), this))

    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddRecent, MakeDelegateFromCSFunction(this.OnAddRecent, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_DelRecent, MakeDelegateFromCSFunction(this.OnDeleteRecent, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddInteract, MakeDelegateFromCSFunction(this.OnAddInteract, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_DelInteract, MakeDelegateFromCSFunction(this.OnDeleteInteract, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_InformOnline, MakeDelegateFromCSFunction(this.InformOnline, MakeGenericClass(Action2, UInt64, Boolean), this))
end
CFriendRecentListView.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnIMMsgReadStatusChanged, MakeDelegateFromCSFunction(this.UpdateFriendListAlert_WithPlayerId, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.MainplayerRelationshipBasicInfoUpdate, MakeDelegateFromCSFunction(this.MainplayerRelationshipBasicInfoUpdate, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.OnUpdateSpecialFriendInfo, MakeDelegateFromCSFunction(this.OnUpdateSpecialFriendInfo, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnJingLingIMReadStatusChanged, MakeDelegateFromCSFunction(this.OnJingLingIMReadStatusChanged, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.MainplayerInteractTypeUpdate, MakeDelegateFromCSFunction(this.OnMainplayerInteractTypeUpdate, MakeGenericClass(Action2, UInt64, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnShiTuRelationshipChanged, MakeDelegateFromCSFunction(this.OnShiTuRelationshipChanged, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnWeddingRelationshipChanged, MakeDelegateFromCSFunction(this.OnWeddingRelationshipChanged, MakeGenericClass(Action1, UInt64), this))

    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddRecent, MakeDelegateFromCSFunction(this.OnAddRecent, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_DelRecent, MakeDelegateFromCSFunction(this.OnDeleteRecent, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddInteract, MakeDelegateFromCSFunction(this.OnAddInteract, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_DelInteract, MakeDelegateFromCSFunction(this.OnDeleteInteract, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_InformOnline, MakeDelegateFromCSFunction(this.InformOnline, MakeGenericClass(Action2, UInt64, Boolean), this))
end
CFriendRecentListView.m_LoadRecentList_CS2LuaHook = function (this) 

    this.tableView:Clear()
    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    CommonDefs.ListClear(this.allData)

    local type = RelationshipType.Recent
    if this.curTabIndex == 0 then
        type = RelationshipType.Recent
    elseif this.curTabIndex == 1 then
        type = RelationshipType.Interact
    else
        return
    end

    if CClientMainPlayer.Inst ~= nil then
        local default
        if (type == RelationshipType.Recent) then
            default = CIMMgr.Inst.RecentList
        else
            default = CIMMgr.Inst.InteractList
        end
        local list = default
        do
            local i = 0
            while i < list.Count do
                local playerId = list[i]
                CommonDefs.ListAdd(this.allData, typeof(FriendItemData), CreateFromClass(FriendItemData, CIMMgr.Inst:GetBasicInfo(playerId)))
                i = i + 1
            end
        end

        if type == RelationshipType.Recent then
            -- 临时加一下有聊天消息的NPC好友
            NpcChat_ChatNpc.Foreach(function(key,obj) 
                local NPC = NPC_NPC.GetData(key)
                local isFriend = CClientMainPlayer.Inst.RelationshipProp.NpcFriendSet:GetBit(obj.SN) 
                if NPC and isFriend then
                    local status = LuaNPCChatMgr.Id2ChatStatus[key]
                    if status and status > 0 then--有交互消息
                        local Data =  CreateFromClass(FriendItemData,NPC.ID,1,NPC.Name, NPC.Portrait, CFriendListItem.ItemType.ChatBot, true)
                        CommonDefs.ListAdd(this.allData, typeof(FriendItemData), Data)
                    end
                end
            end)
            --对最近联系人排序规则： 排序由IMMgr处理，有未读消息时，客户端将有未读消息联系人提前
            CommonDefs.ListClear(this.list1)
            CommonDefs.ListClear(this.list2)
            do
                local i = 0
                while i < this.allData.Count do
                    if CIMMgr.Inst:ShowNewMessageAlert(this.allData[i].playerId) then
                        CommonDefs.ListAdd(this.list1, typeof(FriendItemData), this.allData[i])
                    else
                        CommonDefs.ListAdd(this.list2, typeof(FriendItemData), this.allData[i])
                    end
                    i = i + 1
                end
            end
            CommonDefs.ListClear(this.allData)
            CommonDefs.ListAddRange(this.allData, this.list1)
            CommonDefs.ListAddRange(this.allData, this.list2)


            if CIMMgr.Inst.HasVipGmSession then
                local data = CreateFromClass(FriendItemData, CIMMgr.IM_VIP_GM_ID, 1, CIMMgr.IM_VIP_GM_NAME, CIMMgr.Inst:GetVIPGMPortrait(), CFriendListItem.ItemType.GM, true)
                data.isOnline = CIMMgr.Inst.VipGmStatus:Equals(EnumGmStatus.Online)
                CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data)
            end

            if CIMMgr.Inst.HasGmSession then
                local data = CreateFromClass(FriendItemData, CIMMgr.IM_GM_ID, 1, LocalString.GetString("心易专属"), CIMMgr.Inst:GetGMPortrait(), CFriendListItem.ItemType.GM, true)
                data.isOnline = CIMMgr.Inst.GmStatus:Equals(EnumGmStatus.Online)
                CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data)
            end

            if GameSetting_Common_Wapper.Inst.JingLingEnabled then
                local data2 = CreateFromClass(FriendItemData, CIMMgr.IM_JINGLING_ID, 1, GameSetting_Common_Wapper.Inst.JingLingDisplayName, GameSetting_Common_Wapper.Inst.JingLingPortrait, CFriendListItem.ItemType.JingLing, true)
                CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data2)
            end

            local data1 = CreateFromClass(FriendItemData, CIMMgr.IM_SYSTEM_ID, 1, LocalString.GetString("系统消息"), Constants.IM_ICON_SYSTEM, CFriendListItem.ItemType.System, true)
            CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data1)
        end
    end


    this.tableView:LoadData(0, false)

    do
        local i = 0
        while i < this.allData.Count do
            if this.allData[i].playerId == CFriendView.ChatOppositeId then
                this.tableView.SelectedIndex = i
                this:OnRowSelected(i)
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_UpdateFriendListAlert_WithPlayerId_CS2LuaHook = function (this, playerId) 

    do
        local i = 0
        while i < this.allData.Count do
            if this.allData[i].playerId == playerId then
                local go = this.tableView:GetCellByIndex(i)
                if go ~= nil then
                    local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))

                    local tf = false
                    local word = ""
                    local npctag = LuaGameObject.GetChildNoGC(item.transform,"NpcUnrepliedTag")
                    
                    if LuaNPCChatMgr:IsChatNpc(playerId) then
                        tf,word = LuaNPCChatMgr:GetResponseStr(playerId)
                        item.AlertVisible = LuaNPCChatMgr:NeedAlert(item.PlayerId)
                    else
                        item.AlertVisible = CIMMgr.Inst:ShowNewMessageAlert(item.PlayerId)
                    end
                    if npctag then
                        npctag.gameObject:SetActive(tf)
                        local npctaglabel = LuaGameObject.GetChildNoGC(npctag.transform,"Label").label
                        npctaglabel.text = word
                    end
                end
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_UpdateFriendListAlert_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.FriendItemType == CFriendListItem.ItemType.JingLing then
                    item.AlertVisible = CJingLingMgr.Inst.ExistsUnreadMsgs or CExpertTeamMgr.Inst.ExistsNewAnswer
                else
                    if LuaNPCChatMgr:IsChatNpc(item.PlayerId) then
                        item.AlertVisible = LuaNPCChatMgr:NeedAlert(item.PlayerId)
                    else
                        item.AlertVisible = CIMMgr.Inst:ShowNewMessageAlert(item.PlayerId)
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_OnExtendMenuButtonClick_CS2LuaHook = function (this, playerId) 

    local msgs = CIMMgr.Inst:GetOppositeIMMsgsByID(playerId, 4)
    local nearestMsgs = Table2ArrayWithCount({"", "", ""}, 3, MakeArrayClass(System.String))
    do
        local i = 1
        while i < msgs.Count do
            if i <= nearestMsgs.Length then
                nearestMsgs[i - 1] = msgs[i].Content
            end
            i = i + 1
        end
    end
    local default
    if msgs.Count > 0 then
        default = msgs[0].Content
    else
        default = nil
    end
    local curMsg = default

    if this.curTabIndex == 0 then
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.RecentInFriendWnd, EChatPanel.Undefined, curMsg, nearestMsgs, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    elseif this.curTabIndex == 1 then
        CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.RecentInteractionInFriendWnd, EChatPanel.Undefined, curMsg, nearestMsgs, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end
end
CFriendRecentListView.m_MainplayerRelationshipBasicInfoUpdate_CS2LuaHook = function (this, playerId) 
    local info = CIMMgr.Inst:GetBasicInfo(playerId)
    if info == nil then
        return
    end
    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                if this.allData[i].playerId == playerId then
                    this.allData[i] = CreateFromClass(FriendItemData, info)
                end
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    local data = this.allData[i]
                    item:RefreshBasicInfo(playerId, data.isOnline, data.name, data.portraitName, data.level, data.isInXianShenStatus)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_OnJingLingIMReadStatusChanged_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.FriendItemType == CFriendListItem.ItemType.JingLing then
                    item.AlertVisible = CJingLingMgr.Inst.ExistsUnreadMsgs or CExpertTeamMgr.Inst.ExistsNewAnswer
                    break
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_OnMainplayerInteractTypeUpdate_CS2LuaHook = function (this, targetId, interactType) 

    if this.curTabIndex == 1 then
        do
            local i = 0
            while i < this.allData.Count do
                local continue
                repeat
                    local go = this.tableView:GetCellByIndex(i)
                    if go == nil then
                        continue = true
                        break
                    end
                    local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                    if item.PlayerId == targetId then
                        this:UpdateInfoText(item, targetId)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 1
            end
        end
    end
end
CFriendRecentListView.m_OnShiTuRelationshipChanged_CS2LuaHook = function (this, playerId) 

    if this.curTabIndex ~= 0 then
        return
    end

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    this:UpdateInfoText(item, playerId)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_OnWeddingRelationshipChanged_CS2LuaHook = function (this, playerId) 

    if this.curTabIndex ~= 0 then
        return
    end

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    this:UpdateInfoText(item, playerId)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_OnAddRecent_CS2LuaHook = function (this, playerId) 
    if this.curTabIndex ~= 0 then
        return
    end
    this:ReloadData()
end
CFriendRecentListView.m_OnDeleteRecent_CS2LuaHook = function (this, playerId) 
    if this.curTabIndex ~= 0 then
        return
    end
    this:LocalDeleteAndRefresh(playerId)
end
CFriendRecentListView.m_OnAddInteract_CS2LuaHook = function (this, playerId) 
    if this.curTabIndex ~= 1 then
        return
    end
    this:ReloadData()
end
CFriendRecentListView.m_OnDeleteInteract_CS2LuaHook = function (this, playerId) 
    if this.curTabIndex ~= 1 then
        return
    end
    this:LocalDeleteAndRefresh(playerId)
end
CFriendRecentListView.m_InformOnline_CS2LuaHook = function (this, playerId, online) 
    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    local data = this.allData[i]
                    data.isOnline = online
                    this.allData[i] = data
                    item:UpdateOnlineStatus(data.portraitName, data.isOnline)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendRecentListView.m_LocalDeleteAndRefresh_CS2LuaHook = function (this, playerId) 
    local firstVisible = this.tableView:GetFirstVisibleIndex()
    local firstVisiblePlayerId = 0
    local deletedIndex = - 1
    do
        local i = 0
        while i < this.allData.Count do
            local data = this.allData[i]
            if data.playerId == playerId then
                deletedIndex = i
            end
            i = i + 1
        end
    end
    if deletedIndex == - 1 then
        return
    end

    if firstVisible >= 0 and firstVisible < this.allData.Count then
        firstVisiblePlayerId = this.allData[firstVisible].playerId
    end

    CommonDefs.ListRemoveAt(this.allData, deletedIndex)

    if firstVisiblePlayerId > 0 then
        local startIndex = 0
        local selectedIndex = - 1
        do
            local i = 0
            while i < this.allData.Count do
                local data = this.allData[i]
                if data.playerId == firstVisiblePlayerId then
                    startIndex = i
                end
                if data.playerId == CFriendView.ChatOppositeId then
                    selectedIndex = i
                end
                i = i + 1
            end
        end
        this.tableView:RefreshData(startIndex, selectedIndex)
        if selectedIndex == - 1 and this.OnItemClickDelegate ~= nil then
            GenericDelegateInvoke(this.OnItemClickDelegate, Table2ArrayWithCount({nil}, 1, MakeArrayClass(Object)))
        end
    else
        this:ReloadData()
    end
end
