local CUIFx = import "L10.UI.CUIFx"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

LuaJuDianBattleLevelUpTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleLevelUpTipWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaJuDianBattleLevelUpTipWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaJuDianBattleLevelUpTipWnd, "Fx", "Fx", CUIFx)

--@endregion RegistChildComponent end

function LuaJuDianBattleLevelUpTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaJuDianBattleLevelUpTipWnd:Init()
    local fightLevel =LuaJuDianBattleMgr.ZhanYiLevel

	local fxPath = {
		"fx/ui/prefab/UI_ZhanyiLevel_Blue.prefab",
		"fx/ui/prefab/UI_ZhanyiLevel_Red.prefab",
		"fx/ui/prefab/UI_ZhanyiLevel_Purple.prefab"
	}

    if fightLevel <= 1 then
		self.Fx:LoadFx(fxPath[1])
		self.Icon:LoadMaterial(JuDianZhanYiTexture.Level1)
	elseif fightLevel == 2 then
		self.Fx:LoadFx(fxPath[2])
		self.Icon:LoadMaterial(JuDianZhanYiTexture.Level2)
	else
		self.Fx:LoadFx(fxPath[3])
		self.Icon:LoadMaterial(JuDianZhanYiTexture.Level3)
	end
    self.LevelLabel.text = "lv." .. tostring(fightLevel)

	self.m_Tick = RegisterTickOnce(function ()
		CUIManager.CloseUI("JuDianBattleLevelUpTipWnd")
	end, 3000)
end

function LuaJuDianBattleLevelUpTipWnd:OnDisable()
	UnRegisterTick(self.m_Tick)
end
--@region UIEvent

--@endregion UIEvent

