local UITable = import "UITable"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local CScene=import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CCustomHpAndMpBar = import "L10.UI.CCustomHpAndMpBar"
local EnumClass = import "L10.Game.EnumClass"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientMonster = import "L10.Game.CClientMonster"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"

LuaWuLiangCompanionView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangCompanionView, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionView, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionView, "Table", "Table", UITable)
RegistChildComponent(LuaWuLiangCompanionView, "BgTexture", "BgTexture", UISprite)
RegistChildComponent(LuaWuLiangCompanionView, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaWuLiangCompanionView, "RightBtn", "RightBtn", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangCompanionView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaWuLiangCompanionView:Init()
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local data = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", gamePlayId)

    if data then
        local id = data.Id
        local level = math.floor(id/100)
        local difficult = id - level*100

        local difficultStr = {LocalString.GetString("苦"), LocalString.GetString("集"), LocalString.GetString("灭")}
        local str = SafeStringFormat3(LocalString.GetString("%s·第%s层"), difficultStr[difficult], Extensions.ConvertToChineseString(level))

        -- 文字显示
        self.TitleLabel.text = str
        self.ContentLabel.text = g_MessageMgr:FormatMessage("WuLiang_Task_Wnd_View_Desc")
    end

    UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)

	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)
end

function LuaWuLiangCompanionView:OnLeftBtnClick()
    -- 离开
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaWuLiangCompanionView:OnRightBtnClick()
    -- 规则
    if CScene.MainScene then
        local gameplayId = CScene.MainScene.GamePlayDesignId
        local data = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", gameplayId)

        print("WuLiang_Gameplay_Desc_" .. tostring(data.Id))
        g_MessageMgr:ShowMessage("WuLiang_Gameplay_Desc_" .. tostring(data.Id))
    end
end

function LuaWuLiangCompanionView:UpdataHuoBanInfo()
    local huoBanData = LuaWuLiangMgr.huobanSyncData

    for i =1,2 do
        local item = self.Table.transform:Find(tostring(i))
        item.gameObject:SetActive(false)
    end

    local index = 1
    for id, data in pairs(huoBanData) do
        local item = self.Table.transform:Find(tostring(index))
        index = index + 1
        item.gameObject:SetActive(true)

        if not self.m_HasInited then
            local clsSprite = item:Find("ClsSprite"):GetComponent(typeof(UISprite))
            local nameLabel = item:Find("NameLabel"):GetComponent(typeof(UILabel))
            local icon = item:Find("Portrait"):GetComponent(typeof(CUITexture))
            local monsterData = Monster_Monster.GetData(id)
            local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(id)
            clsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), huoBanData.Class))
            icon:LoadNPCPortrait(monsterData.HeadIcon, false)
            nameLabel.text = huoBanData.Name

            UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                local monsterObj = nil
                CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
                    if TypeIs(obj, typeof(CClientMonster)) then
                        if obj and obj.TemplateId == id then
                            monsterObj = obj
                        end
                    end
                end))
    
                if monsterObj and CClientMainPlayer.Inst then
                    CClientMainPlayer.Inst.Target = monsterObj
                end
            end)
        end

        -- 只用每次更新血量
        local hpBar = item:Find("HPBar"):GetComponent(typeof(CCustomHpAndMpBar))
        local hpFull = data.Attr.HpFull

        hpBar:UpdateValue(data.Hp, hpFull, hpFull)

        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            data.MonsterId = id
            LuaWuLiangMgr:OpenHuoBanTipWnd(data) 
        end)
    end

    self.Table:Reposition()
    local h = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform)
	self.BgTexture.height = h.size.y

    self.m_HasInited = true
end

function LuaWuLiangCompanionView:OnEnable()
    self.m_HasInited = false
    g_ScriptEvent:AddListener("WuLiangShenJing_SyncHuoBan", self, "UpdataHuoBanInfo")
end

function LuaWuLiangCompanionView:OnDisable()
    g_ScriptEvent:RemoveListener("WuLiangShenJing_SyncHuoBan", self, "UpdataHuoBanInfo")
end

--@region UIEvent

--@endregion UIEvent

