
LuaImageRuleMgr = {}

LuaImageRuleMgr.m_ImageRuleInfo = {}

function LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
    if imagePaths==nil or #imagePaths==0 then return end
    self.m_ImageRuleInfo.imagePaths = imagePaths or {}
    self.m_ImageRuleInfo.msgs = msgs or {}
    CUIManager.ShowUI("CommonImageRuleWnd")
end




