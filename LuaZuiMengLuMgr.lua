local CActivityMgr = import "L10.Game.CActivityMgr"
local CommonDefs = import "L10.Game.CommonDefs"

LuaZuiMengLuMgr = {}
LuaZuiMengLuMgr.infos = nil
LuaZuiMengLuMgr.Title = LocalString.GetString("[罪梦录]探查线索")
LuaZuiMengLuMgr.Description = LocalString.GetString("寻找关键线索，收集信息。")

function LuaZuiMengLuMgr:Init()
    self.infos = {}
    ZhongYuanJie_ZuiMengLuThread.Foreach(function (ThreadId, data)
        if data.ThreadGameplay ~= 0 then
            table.insert(self.infos, data.ThreadGameplay)
        end
        CommonDefs.ListIterate(data.RelativeGameplays, DelegateFactory.Action_object(function (___value)
            local v = ___value
            if v ~= nil and v ~= 0 then
                table.insert(self.infos, v)
            end
        end))
    end)
end

function LuaZuiMengLuMgr:OnMainPlayerCreated(gamePlayId)
    if self.infos == nil then
        self:Init()
    end
    for i, v in ipairs(self.infos) do
        if gamePlayId == v then
            CUIManager.CloseUI(CUIResources.PackageWnd);
            CActivityMgr.Inst:UpdateAcitivity(self.Title, self.Description, DelegateFactory.Action(function()
	            CUIManager.ShowUI(CLuaUIResources.ZuiMengLuWnd)
            end), 0);
            return
        end
    end
    self:Leave()
end

function LuaZuiMengLuMgr:Leave()
    CActivityMgr.Inst:DeleteActivity(self.Title)
end



