local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISprite = import "UISprite"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"

LuaZongMenJoinStandardSearchResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZongMenJoinStandardSearchResultWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaZongMenJoinStandardSearchResultWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaZongMenJoinStandardSearchResultWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenJoinStandardSearchResultWnd, "m_BgInitialHeight")

function LuaZongMenJoinStandardSearchResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaZongMenJoinStandardSearchResultWnd:Init()
    self.m_BgInitialHeight = self.Background.height
    local gridHeight = self.Grid.cellHeight
    self.Template.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Grid.transform)
    local bgHeight = self.m_BgInitialHeight - gridHeight
    for i = 0, LuaZongMenMgr.m_GuiZe.Length - 1 do
        local id = tonumber(LuaZongMenMgr.m_GuiZe[i])
        local data = Menpai_JoinStandard.GetData(id)
        if data then
            local obj = CUICommonDef.AddChild(self.Grid.gameObject, self.Template.gameObject)
            obj.gameObject:SetActive(true)
            local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = data and data.Description or "---"
            bgHeight = bgHeight + gridHeight
        end
    end
    self.Grid:Reposition()
    self.Background.height = bgHeight
end

--@region UIEvent

--@endregion UIEvent

