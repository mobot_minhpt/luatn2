local CPlayUpdateAlertMgr=import "L10.Game.CPlayUpdateAlertMgr"
local CUITexture = import "L10.UI.CUITexture"
local ClientAction=import "L10.UI.ClientAction"
local UIWidget=import "UIWidget"
CLuaNewActivityWnd =class()
RegistClassMember(CLuaNewActivityWnd,"m_DataList")
RegistClassMember(CLuaNewActivityWnd,"m_Pic")
RegistClassMember(CLuaNewActivityWnd,"m_Angles")
RegistClassMember(CLuaNewActivityWnd,"m_PicsTf")
RegistClassMember(CLuaNewActivityWnd,"m_DeltaDegree")
RegistClassMember(CLuaNewActivityWnd,"m_UITextures")
RegistClassMember(CLuaNewActivityWnd,"m_UITextureBorders")
RegistClassMember(CLuaNewActivityWnd,"m_IndicatorsTf")

RegistClassMember(CLuaNewActivityWnd,"m_JieRiWndDict")

function CLuaNewActivityWnd:Init()
    Gac2Gas.IgnorePlayUpdateAlert()

    self.m_JieRiWndDict = {}
    Task_JieRiGroup.Foreach(function(k, v)
        if not System.String.IsNullOrEmpty(v.OpenWnd) then
            self.m_JieRiWndDict[v.OpenWnd] = true
        end
    end)

    self.m_DataList = {}
    self.m_Pic = FindChild(self.transform,"Pic").gameObject
    self.m_Pic:SetActive(false)
    local dataList = CPlayUpdateAlertMgr.Inst:GetValidPlayUpdateAlertTemplates()
    for i=1,dataList.Count do
        if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer and self:IsOpenJieRiWnd(dataList[i-1])) then
            table.insert( self.m_DataList,dataList[i-1] )
        end
    end
    if #self.m_DataList == 0 then
        CUIManager.CloseUI(CLuaUIResources.NewActivityWnd)
    end
    table.sort( self.m_DataList, function(a,b)
        if a.Priority == b.Priority then
            return a.ID<b.ID
        else
            return a.Priority>b.Priority
        end
    end )

    self.m_UITextures={}
    self.m_UITextureBorders={}
    self.m_PicsTf = FindChild(self.transform,"Pics")
    local picsGo = FindChild(self.transform,"Pics").gameObject
    Extensions.RemoveAllChildren(picsGo.transform)
    for i,v in ipairs(self.m_DataList) do
        local go = NGUITools.AddChild(picsGo,self.m_Pic)
        go:SetActive(true)
        local texture = go:GetComponent(typeof(CUITexture))
        texture:LoadMaterial(v.Photo)
        local uiTexture = go:GetComponent(typeof(UITexture))
        table.insert( self.m_UITextures,uiTexture )

        local border = go.transform:GetChild(0).gameObject:GetComponent(typeof(UIWidget))
        table.insert( self.m_UITextureBorders,border )

        UIEventListener.Get(go).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
            self:OnDrag(g,delta)
        end)
        UIEventListener.Get(go).onDragEnd = DelegateFactory.VoidDelegate(function(g)
            self:OnDragEnd(g)
        end)
        UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function(g)
            self:OnDragStart(g)
        end)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnClick(go)
        end)
        
    end
    self.m_Angles={}
    self.m_DeltaDegree = math.floor(360/#self.m_DataList)
    for i,v in ipairs(self.m_DataList) do
        self.m_Angles[i] = (i-1)*self.m_DeltaDegree
    end
    self:UpdatePosition()


    local indicator=FindChild(self.transform,"Indicator").gameObject
    indicator:SetActive(false)

    self.m_IndicatorsTf=FindChild(self.transform,"Indicators")
    CUICommonDef.ClearTransform(self.m_IndicatorsTf)

    if #self.m_DataList>1 then
        for i,v in ipairs(self.m_DataList) do
            local igo=NGUITools.AddChild(self.m_IndicatorsTf.gameObject,indicator)
            igo:SetActive(true)
        end
        self.m_IndicatorsTf:GetComponent(typeof(UIGrid)):Reposition()
    end

    self:UpdateIndicator(1)
end

function CLuaNewActivityWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerCreated",self,"OnMainPlayerCreated")
end

function CLuaNewActivityWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerCreated",self,"OnMainPlayerCreated")
end

function CLuaNewActivityWnd:IsOpenJieRiWnd(data)
    local _, __, wndName = string.find(data and data.Action or "", "OpenWnd%((.+),show_wnd%)")
    return wndName and self.m_JieRiWndDict[wndName]
end

function CLuaNewActivityWnd:OnMainPlayerCreated()
    self:Init()
end

function CLuaNewActivityWnd:OnClick(go)
    local index=go.transform:GetSiblingIndex()+1
    local data = self.m_DataList[index]
    if data and data.Action~="" and data.Action~=nil then
        ClientAction.DoAction(data.Action)
        CUIManager.CloseUI(CLuaUIResources.NewActivityWnd)
    else
        -- CUIManager.CloseUI(CLuaUIResources.NewActivityWnd)
    end
end

function CLuaNewActivityWnd:UpdateIndicator(index)
    for i=1,self.m_IndicatorsTf.childCount do
        local sprite = self.m_IndicatorsTf:GetChild(i-1):GetComponent(typeof(UISprite))
        if i==index then
            sprite.color=Color.white
        else
            sprite.color=Color(0.3,0.5,0.8)
        end
    end

end
function CLuaNewActivityWnd:OnDrag(go,delta)
    local moveDeg = 57.2958*math.asin(math.max(-1,math.min(delta.x/400,1)))
    for i,v in ipairs(self.m_Angles) do
        local angle = v + moveDeg
        self.m_Angles[i] = angle - math.floor(angle/360)*360
    end

    self:UpdatePosition()
end
function CLuaNewActivityWnd:OnDragStart(go)
    for i,v in ipairs(self.m_Angles) do
        local tf = self.m_PicsTf:GetChild(i-1)
        LuaTweenUtils.DOKill(tf,false)
    end
end
function CLuaNewActivityWnd:OnDragEnd(go)
    local selectIndex=0

    local index=go.transform:GetSiblingIndex()+1
    local x = go.transform.localPosition.x
    local scale = go.transform.localScale.x
    --看划过的距离
    if x>-50 and x<50 and scale>0.7 then
        selectIndex=index
    elseif x<0 then
        selectIndex=index+1
    elseif x>0 then
        selectIndex=index-1
    end
    -- if x<-50 then 
    --     selectIndex=index+1 
    -- elseif x>50 then 
    --     selectIndex=index-1
    -- else
    --     selectIndex=index
    -- end
    if selectIndex<1 then selectIndex=selectIndex+#self.m_Angles end
    if selectIndex>#self.m_Angles then selectIndex=selectIndex-#self.m_Angles end


    for i,v in ipairs(self.m_Angles) do
        local degree = (i-selectIndex)*self.m_DeltaDegree
        degree = degree - math.floor(degree/360)*360

        local moveTo =  math.sin(0.0174533*degree)*250
        local scale = math.cos(0.0174533*degree)*0.2+0.8
        scale=math.max(0,math.min(1,scale))

        local tf = self.m_PicsTf:GetChild(i-1)

        LuaTweenUtils.TweenPositionX(tf,moveTo,0.3)
        LuaTweenUtils.TweenScaleTo(tf, Vector3(scale,scale,1),0.3)

        self.m_UITextures[i].depth = math.floor(scale*100)*2
        self.m_UITextures[i].alpha=scale*scale>0.95 and 1 or scale*scale*scale*scale
        self.m_UITextureBorders[i].depth = self.m_UITextures[i].depth-1

        self.m_Angles[i]=degree
    end

    self:UpdateIndicator(selectIndex)
end

function CLuaNewActivityWnd:UpdatePosition()
    for i,v in ipairs(self.m_DataList) do
        local tf = self.m_PicsTf:GetChild(i-1)

        local angle=self.m_Angles[i]
        angle = angle - math.floor(angle/360)*360

        local offset = math.sin(0.0174533*angle)*250
        LuaUtils.SetLocalPositionX( tf,offset)

        local scale = math.cos(0.0174533*angle)*0.2+0.8
        LuaUtils.SetLocalScale(tf,scale,scale,1)

        self.m_UITextures[i].depth = math.floor(scale*100)*2
        self.m_UITextures[i].alpha=scale*scale>0.95 and 1 or scale*scale*scale*scale
        self.m_UITextureBorders[i].depth = self.m_UITextures[i].depth-1
    end
end