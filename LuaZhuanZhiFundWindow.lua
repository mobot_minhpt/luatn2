local CButton = import "L10.UI.CButton"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CUITexture = import "L10.UI.CUITexture"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local ClientAction = import "L10.UI.ClientAction"

LuaZhuanZhiFundWindow = class()

RegistChildComponent(LuaZhuanZhiFundWindow, "BuyBtn", CButton)
RegistChildComponent(LuaZhuanZhiFundWindow, "BenifitHintLabel", UILabel)
RegistChildComponent(LuaZhuanZhiFundWindow, "TimeHintLabel", UILabel)
RegistChildComponent(LuaZhuanZhiFundWindow, "PriceLabel", UILabel)

RegistChildComponent(LuaZhuanZhiFundWindow, "FundTable", UITable)
RegistChildComponent(LuaZhuanZhiFundWindow, "FundItem", GameObject)
RegistChildComponent(LuaZhuanZhiFundWindow, "Scrollview", UIScrollView)

RegistClassMember(LuaZhuanZhiFundWindow, "FundInfos")
RegistClassMember(LuaZhuanZhiFundWindow, "BuyCountDownTick")    -- 购买倒计时
RegistClassMember(LuaZhuanZhiFundWindow, "ReceiveCountDownTick")    -- 领取倒计时


function LuaZhuanZhiFundWindow:Awake()
    self.BenifitHintLabel.text = g_MessageMgr:FormatMessage("ZhuanZhi_Fund_Benifit")
    self.TimeHintLabel.text = g_MessageMgr:FormatMessage("ZhuanZhi_Fund_Time_Limit")
end

function LuaZhuanZhiFundWindow:Init()
    self.FundItem:SetActive(false)

    self:CancelBuyTick()
    self:CancelRecieveTick()

    local setting = ProfessionTransfer_Setting.GetData()
    self.PriceLabel.text = SafeStringFormat3("%s", tostring(setting.TransferFundJade))

    CommonDefs.AddOnClickListener(self.BuyBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnBuyBtnClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.transform:Find("Title/TipBtn").gameObject, DelegateFactory.Action_GameObject(function (go)
        g_MessageMgr:ShowMessage("PROFESSION_TRANSFER_FUND_WINDOW_TIPS")
    end), false)

    Gac2Gas.QueryClassTransferFund()
    Gac2Gas.IgnoreClassTransferFundAlert() -- 取消小红点
end

--@desc 更新基金和按钮信息
function LuaZhuanZhiFundWindow:SyncZhuanZhiFundInfo()
    self:UpdateFundInfo()
    self:UpdateButtonStatus()
end

--@desc 更新基金信息
function LuaZhuanZhiFundWindow:UpdateFundInfo()
    self.FundInfos = {}
    self.fundAwardTemplateId2YuanBao = {}
    local rawFundYuanBaoIds = ProfessionTransfer_Setting.GetData().FundYuanBaoIds
    local subStrList = g_LuaUtil:StrSplit(rawFundYuanBaoIds, ";")
    for i = 1, #subStrList do
        local subStr = subStrList[i]
        if subStr ~= "" then
            local itemCountPair = g_LuaUtil:StrSplit(subStr, ",")
            local templateId = tonumber(itemCountPair[1])
            local yuanBao = tonumber(itemCountPair[2])
            self.fundAwardTemplateId2YuanBao[templateId] = yuanBao
        end
    end

    ProfessionTransfer_FundTask.Foreach(function (key)
        local data = ProfessionTransfer_FundTask.GetData(key)
        local info = LuaProfessionTransferMgr.FundProgressInfos[key]

        table.insert(self.FundInfos, {
            ID = data.ID,
            Desc = data.Desc,
            CurrentProgress = (info and info.progress) or 0,
            NeedProgress = data.NeedProgress,
            AwardItem = data.AwardItem,
            IsFinished = (info and info.isFinished) or false,
            IsTaken = (info and info.isAwarded) or false,
        })
    end)

    -- 排序
    -- 已经领取的放到最后
    -- 已经完成但是未领取的放到最前面
    -- 其他按照ID排序
    table.sort(self.FundInfos, function ( a, b )

        if a.IsTaken and not b.IsTaken then
            return false
        elseif not a.IsTaken and b.IsTaken then
            return true
        else
            if a.IsFinished and not b.IsFinished then
                return true
            elseif not a.IsFinished and b.IsFinished then
                return false
            end
        end
        return a.ID < b.ID
    end)

    CUICommonDef.ClearTransform(self.FundTable.transform)

    for i = 1, #self.FundInfos do
        local info = self.FundInfos[i]
        local go = NGUITools.AddChild(self.FundTable.gameObject, self.FundItem)

        self:InitFundItem(go, info)
        go:SetActive(true)
    end

    self.FundTable:Reposition()
    self.Scrollview:ResetPosition()
end


--@desc 初始化基金item
function LuaZhuanZhiFundWindow:InitFundItem(item, info)
    if not item or not info then return end

    local fundConditionLabel = item.transform:Find("FundConditionLabel"):GetComponent(typeof(UILabel))
    local grid = item.transform:Find("AwardList/Grid"):GetComponent(typeof(UIGrid))
    local award = item.transform:Find("AwardList/Award").gameObject
    award:SetActive(false)

    local status = item.transform:Find("Status").gameObject
    local getBonusBtn = item.transform:Find("Status/GetBonusBtn"):GetComponent(typeof(CButton))
    local progressLabel = item.transform:Find("Status/ProgressLabel"):GetComponent(typeof(UILabel))
    local gotStatus = item.transform:Find("Status/GotStatus").gameObject
    status:SetActive(false)

    fundConditionLabel.text = info.Desc

    -- 奖励信息
    CUICommonDef.ClearTransform(grid.transform)
    for i = 0, info.AwardItem.Length-1, 3 do
        local itemId = info.AwardItem[i]
        local count = info.AwardItem[i+1]
        local isBind = info.AwardItem[i+2] == 1

        local go = NGUITools.AddChild(grid.gameObject, award)
        self:InitFundAwardItem(go, itemId, count, isBind)
        go:SetActive(true)
    end

    grid:Reposition()

    -- todo 基金领取按钮的状态
    status:SetActive(LuaProfessionTransferMgr.FundPurchased)
    if not info.IsFinished then
        -- 进度未达成
        gotStatus:SetActive(false)
        getBonusBtn.gameObject:SetActive(false)
        progressLabel.gameObject:SetActive(true)
        progressLabel.text = SafeStringFormat3("%d/%d", info.CurrentProgress, info.NeedProgress)
    else
        progressLabel.gameObject:SetActive(false)
        progressLabel.text = nil

        if info.IsTaken then
            gotStatus:SetActive(true)
            getBonusBtn.gameObject:SetActive(false)
        else
            gotStatus:SetActive(false)
            getBonusBtn.gameObject:SetActive(true)

            CommonDefs.AddOnClickListener(getBonusBtn.gameObject, DelegateFactory.Action_GameObject(function ( go )
                Gac2Gas.GetClassTransferFundTaskAward(info.ID)
            end), false)
        end
    end

end

--@desc 初始化基金奖励Item
function LuaZhuanZhiFundWindow:InitFundAwardItem(go, itemId, count, isBind)
    if not go then return end

    local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local amount = go.transform:Find("Amount"):GetComponent(typeof(UILabel))
    local quality = go.transform:Find("Quality"):GetComponent(typeof(UISprite))
    local bind = go.transform:Find("Bind").gameObject

    local item = Item_Item.GetData(itemId)
    if not item then return end

    icon:LoadMaterial(item.Icon)
    --amount.text = tostring(count)
    local isYuanBao = self.fundAwardTemplateId2YuanBao[itemId] ~= nil
    if isYuanBao then
        amount.text = tostring(self.fundAwardTemplateId2YuanBao[itemId]) .. LocalString.GetString("元宝")
    else
        amount.text = count > 1 and tostring(count) or ""
    end
    
    bind:SetActive(isBind)
    quality.spriteName = CUICommonDef.GetItemCellBorder(item.NameColor)

    CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (gameobject)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end

--@desc 购买转职基金
function LuaZhuanZhiFundWindow:OnBuyBtnClicked(go)
    --判断玩家有没有转职过
    local haveTransfer = LuaProfessionTransferMgr.TransferInDuration4Fund()

    if haveTransfer then
        local msg = g_MessageMgr:FormatMessage("ZhuanZhi_Buy_Second_Confirm")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            local setting = ProfessionTransfer_Setting.GetData()
            local jadeWithBind = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
            if jadeWithBind >= setting.TransferFundJade then
                Gac2Gas.RequestBuyClassTransferFund()
            else
                -- 灵玉不够
                MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function ()
                    CShopMallMgr.ShowChargeWnd()
                end), nil, LocalString.GetString("充值"), nil, false)
            end

        end), nil, nil, nil, false)
    else
        local msg = g_MessageMgr:FormatMessage("ZhuanZhi_Go_Transfer_First")
        --需在活动期间转职才能购买转职基金，是否立即前往转职？
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            local action = ProfessionTransfer_Setting.GetData().FindTransferNPC
            if action then
                CUIManager.CloseUI("WelfareWnd")
                ClientAction.DoAction(action)
            end
        end), nil, nil, nil, false)
    end
end

--@desc 更新按钮信息(包括购买按钮状态/倒计时 购买后可领取倒计时)
function LuaZhuanZhiFundWindow:UpdateButtonStatus()
    if LuaProfessionTransferMgr.FundPurchased then
        -- 如果已经购买，开启领取倒计时，关闭购买倒计时
        self.BuyBtn.Enabled = false
        self.BuyBtn.Text = LocalString.GetString("已购买")
        self:ReceiveCountDown()
        self:CancelBuyTick()
        self:CancelRecieveTick()
        self.ReceiveCountDownTick = RegisterTick(function ()
            self:ReceiveCountDown()
        end, 1000)
    else
        -- 如果没有购买，开启购买倒计时，关闭领取倒计时
        self.BuyBtn.Enabled = true
        self.BuyBtn.Text = LocalString.GetString("购买")
        self:BuyCountDown()
        self:CancelRecieveTick()
        self:CancelBuyTick()
        self.BuyCountDownTick = RegisterTick(function ()
            self:BuyCountDown()
        end, 1000)
    end
end

--@desc 领取倒计时相关处理
function LuaZhuanZhiFundWindow:ReceiveCountDown()
    -- 计算领取倒计时
    local leftTime = LuaProfessionTransferMgr.FundTaskExpireTime - CServerTimeMgr.Inst.timeStamp
    if leftTime > 0 then
        self.TimeHintLabel.text = SafeStringFormat3(LocalString.GetString("领取倒计时 %s"), self:ParseTime(leftTime))
    else
        self.TimeHintLabel.text = LocalString.GetString("领取已超时")
        self:CancelRecieveTick()
    end
end

--@desc 购买倒计时相关处理
function LuaZhuanZhiFundWindow:BuyCountDown()
    -- 计算购买倒计时
    local leftTime = LuaProfessionTransferMgr.FundEndTime - CServerTimeMgr.Inst.timeStamp
    if leftTime > 0 then
        self.BuyBtn.Text = SafeStringFormat3(LocalString.GetString("购买(余 %s)"), self:ParseTime(leftTime))
    else
        self.BuyBtn.Text = LocalString.GetString("购买已超时")
        self.BuyBtn.Enabled = false
        self:CancelBuyTick()
    end
end

function LuaZhuanZhiFundWindow:CancelBuyTick()
    if self.BuyCountDownTick then
		UnRegisterTick(self.BuyCountDownTick)
		self.BuyCountDownTick = nil
	end
end

function LuaZhuanZhiFundWindow:CancelRecieveTick()
    if self.ReceiveCountDownTick then
		UnRegisterTick(self.ReceiveCountDownTick)
		self.ReceiveCountDownTick = nil
	end
end

--@desc 处理修改时间格式
function LuaZhuanZhiFundWindow:ParseTime(leftTime)
    if leftTime <= 0 then
        return LocalString.GetString("已过期")
    end

    if leftTime < 24 * 3600 then
        local hour = math.floor(leftTime / 3600)
        local minute = math.floor(leftTime % 3600 / 60)
        local sec = math.floor((leftTime)%60)
        
        return SafeStringFormat3("%d:%d:%d", hour, minute, sec)
    else
        local days = math.floor(leftTime / (24 * 3600))
        return SafeStringFormat3(LocalString.GetString("%d天"), days)
    end
end

function LuaZhuanZhiFundWindow:OnEnable()
    g_ScriptEvent:AddListener("SyncZhuanZhiFundInfo", self, "SyncZhuanZhiFundInfo")
    g_ScriptEvent:AddListener("UpdateZhuanZhiFundProgressInfo", self, "UpdateFundInfo")
    g_ScriptEvent:AddListener("GetZhuanZhiFundAwardSuccess", self, "UpdateFundInfo")
    -- 
end

function LuaZhuanZhiFundWindow:OnDisable()
    g_ScriptEvent:RemoveListener("SyncZhuanZhiFundInfo", self, "SyncZhuanZhiFundInfo")
    g_ScriptEvent:RemoveListener("UpdateZhuanZhiFundProgressInfo", self, "UpdateFundInfo")
    g_ScriptEvent:RemoveListener("GetZhuanZhiFundAwardSuccess", self, "UpdateFundInfo")
    self:CancelBuyTick()
    self:CancelRecieveTick()
end

return LuaZhuanZhiFundWindow
