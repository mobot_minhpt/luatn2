local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaBaoYuDuoYu2023ChangePhaseWnd = class()
LuaBaoYuDuoYu2023ChangePhaseWnd.m_stage = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBaoYuDuoYu2023ChangePhaseWnd, "Tex", "Tex", CUITexture)
RegistChildComponent(LuaBaoYuDuoYu2023ChangePhaseWnd, "ShowText", "ShowText", UILabel)

--@endregion RegistChildComponent end

function LuaBaoYuDuoYu2023ChangePhaseWnd:Awake()
    --@region EventBind: Dont Modify Manually!


    self.bossIconMaterialList = {}
    local BossIconRawData = Double11_BYDLY.GetData().BossIconMaterialList
    local rawDataList = g_LuaUtil:StrSplit(BossIconRawData, ";")
    for i = 1, #rawDataList do
        if rawDataList[i] ~= "" then
            table.insert(self.bossIconMaterialList, rawDataList[i])
        end
    end
    
    --@endregion EventBind end
    if LuaBaoYuDuoYu2023ChangePhaseWnd.m_stage == 2 then
        self.ShowText.text = LocalString.GetString("第二阶段来袭！")
        self.Tex:LoadMaterial(self.bossIconMaterialList[2])
    elseif LuaBaoYuDuoYu2023ChangePhaseWnd.m_stage == 3 then
        self.ShowText.text = LocalString.GetString("第三阶段来袭！")
        self.Tex:LoadMaterial(self.bossIconMaterialList[3])
    end
    self.m_delayShowRewardTick = RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.BaoYuDuoYu2023ChangePhaseWnd)
    end, 3000)
end

function LuaBaoYuDuoYu2023ChangePhaseWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

