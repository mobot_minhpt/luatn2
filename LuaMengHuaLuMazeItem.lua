require("common/common_include")

local LuaTweenUtils = import "LuaTweenUtils"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local MengHuaLu_Monster = import "L10.Game.MengHuaLu_Monster"
local CUITexture = import "L10.UI.CUITexture"
local MengHuaLu_Skill = import "L10.Game.MengHuaLu_Skill"
local Vector3 = import "UnityEngine.Vector3"
local Ease = import "DG.Tweening.Ease"
local UISprite = import "UISprite"
local MengHuaLu_Item = import "L10.Game.MengHuaLu_Item"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFx = import "L10.UI.CUIFx"
local MessageMgr = import "L10.Game.MessageMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local TweenPosition = import "TweenPosition"
local MengHuaLu_Setting = import "L10.Game.MengHuaLu_Setting"
local LuaUtils = import "LuaUtils"
local UIWidget = import "UIWidget"
local UIPanel = import "UIPanel"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaMengHuaLuMazeItem = class()

RegistChildComponent(LuaMengHuaLuMazeItem, "AvailableBG", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "NotAvailableBG", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "ItemView", GameObject)

RegistChildComponent(LuaMengHuaLuMazeItem, "ItemIcon", CUITexture)
RegistChildComponent(LuaMengHuaLuMazeItem, "ItemQualitySprite", UISprite)
RegistChildComponent(LuaMengHuaLuMazeItem, "GateIcon", CUITexture)
RegistChildComponent(LuaMengHuaLuMazeItem, "ItemBottom", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "ShopIcon", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "BossIcon", GameObject)

RegistChildComponent(LuaMengHuaLuMazeItem, "MonsterView", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "MonsterTexture", CUITexture)
RegistChildComponent(LuaMengHuaLuMazeItem, "AttackValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMazeItem, "AttackProperty", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "HPValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMazeItem, "RoundValueLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMazeItem, "RoundProperty", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "HPChangLabel", UILabel)
RegistChildComponent(LuaMengHuaLuMazeItem, "HPProperty", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "Border", UISprite)
RegistChildComponent(LuaMengHuaLuMazeItem, "IsLocked", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "Fx", CUIFx)
RegistChildComponent(LuaMengHuaLuMazeItem, "MonsterFX", CUIFx)

-- 用于处理boss怪物
RegistChildComponent(LuaMengHuaLuMazeItem, "BG", UIWidget)
RegistChildComponent(LuaMengHuaLuMazeItem, "BossDropFx", CUIFx)

-- 用于处理捡起的动效
RegistChildComponent(LuaMengHuaLuMazeItem, "AttackIcon", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "HPIcon", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "MPIcon", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "MoneyIcon", GameObject)
RegistChildComponent(LuaMengHuaLuMazeItem, "SkillIcon", GameObject)

-- 用于处理释放完技能
RegistChildComponent(LuaMengHuaLuMazeItem, "SkillMask", UIPanel)

-- 获得物品的特效
RegistClassMember(LuaMengHuaLuMazeItem, "MazeId")

-- 用来显示怪物的模型，包括特效、动作
RegistClassMember(LuaMengHuaLuMazeItem, "ModelTextureName")
RegistClassMember(LuaMengHuaLuMazeItem, "ModelTextureLoader")
RegistClassMember(LuaMengHuaLuMazeItem, "AniName")
RegistClassMember(LuaMengHuaLuMazeItem, "FXId")

RegistClassMember(LuaMengHuaLuMazeItem, "ScoreList") -- 用于处理飘字
RegistClassMember(LuaMengHuaLuMazeItem, "MazeInfo")
RegistClassMember(LuaMengHuaLuMazeItem, "TempMazeInfo") -- 用于怪物受击和更新
RegistClassMember(LuaMengHuaLuMazeItem, "SelectedSkillId") -- 用于选中技能释放

RegistClassMember(LuaMengHuaLuMazeItem, "ActionQueue")
RegistClassMember(LuaMengHuaLuMazeItem, "IntervalTime")
RegistClassMember(LuaMengHuaLuMazeItem, "TweenList")

function LuaMengHuaLuMazeItem:Init(isAvailable, mazeId)
	self:InitValues(mazeId)
	self.Fx:DestroyFx()
	self.BossDropFx:DestroyFx()
	self:ResetPickItem()
	self:DestroyMonsterdata()

	if not isAvailable then
		self.AvailableBG:SetActive(true)
		self.NotAvailableBG:SetActive(false)
		self.ItemView:SetActive(false)
		self.MonsterView:SetActive(false)
		self.IsLocked:SetActive(false)
	end

end

-- 复原原有item的位置和漂浮效果
function LuaMengHuaLuMazeItem:ResetPickItem()
	local tween = self.ItemIcon.transform:GetComponent(typeof(TweenPosition))
	if tween then
		tween.enabled = true
	end
	self.ItemIcon.transform.localPosition = Vector3(0, 5, 0)
	LuaUtils.SetWidgetsAlpha(self.ItemIcon.transform, 1)
	self.ItemIcon.texture.alpha = 1
	self.ItemQualitySprite.alpha = 1
end

-- 怪物贴图和怪物actionQueue比较耗费资源，格子状态变成非monster后需要清理
function LuaMengHuaLuMazeItem:DestroyMonsterdata()
	self.ActionQueue = {}
	self.MonsterFX:DestroyFx()
	self.AniName = nil
	self.FXId = 0
	self:BossScale(200, 200, false) -- 恢复boss格子的大小
	self.TempMazeInfo = nil
	CUIManager.DestroyModelTexture(SafeStringFormat3("__MengHuaLuMazeItem__%s__", tostring(self.MazeId)))
end

function LuaMengHuaLuMazeItem:InitValues(mazeId)
	self.AniName = nil
	self.FXId = 0
	self.MazeId = mazeId
	self.MazeInfo = nil
	self.ActionQueue = {}
	self.SelectedSkillId = 0
	self.IntervalTime = 0.8
	self.TweenList = {}

	-- 如果是固定位置的门，则修改Fx位置
	if self.MazeId == 1 then
		self.Fx.transform.localPosition = Vector3(0, -25, 0)
	end

	local onItemClicked = function (go)
		self:OnItemClicked(go)
	end
	--CommonDefs.AddOnClickListener(self.gameObject, DelegateFactory.Action_GameObject(onItemClicked), false)
	CommonDefs.AddOnClickListener(self.BG.gameObject, DelegateFactory.Action_GameObject(onItemClicked), false)

	local LongPressButton = self.BG.transform:GetComponent(typeof(UILongPressButton))
	LongPressButton.OnLongPressDelegate = DelegateFactory.Action(function ()
		self:OnItemLongPressed()
	end)

	self.ModelTextureName = SafeStringFormat3("__MengHuaLuMazeItem__%s__", tostring(self.MazeId))
	if not self.ModelTextureLoader then
		self.ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
            self:LoadModel(ro)
        end)
	end
end

function LuaMengHuaLuMazeItem:OnItemClicked(go)
	if not self.MazeInfo then return end

	if self.MazeInfo.MazeIsLocked then
		MessageMgr.Inst:ShowMessage("MENGHUALU_GRID_CANNOT_OPEN_MONSTER", {})
		return
	end

	if self.MazeInfo.MazeColor == EnumMengHuaLuGridColor.eCanOpen then
		-- 可点开状态
		Gac2Gas.RequestOpenMengHuaLuGrid(self.MazeId)
	elseif self.MazeInfo.MazeColor == EnumMengHuaLuGridColor.eCanNotOpen then
		-- 进行提示
		MessageMgr.Inst:ShowMessage("MENGHUALU_GRID_CANNOT_OPEN", {})
	else
		if self.MazeInfo.ItemId and self.MazeInfo.ItemId ~= 0 then
			-- 如果是门的道具，则提示
			local setting = MengHuaLu_Setting.GetData()
			if self.MazeInfo.ItemId == setting.EntranceCloseItemId then
				MessageMgr.Inst:ShowMessage("MENGHUALU_NOT_GETKEY", {})
				return
			end

			-- 请求获取该物品
			local item = MengHuaLu_Item.GetData(self.MazeInfo.ItemId)
			if not System.String.IsNullOrEmpty(item.DoubleCheckMsg) then
				local msg = g_MessageMgr:FormatMessage(item.DoubleCheckMsg)
				MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
					Gac2Gas.RequestPickMengHuaLuItem(self.MazeId)
				end), nil, nil, nil, false)
			else
				Gac2Gas.RequestPickMengHuaLuItem(self.MazeId)
			end
			return
		end

		if self.MazeInfo.MonsterInfo then
			-- 如果是怪物，弹提示
			if not LuaMengHuaLuMgr.CheckMazeClick() then return end
			if self.SelectedSkillId ~= 0 then
				Gac2Gas.RequestUseMengHuaLuSkill(self.SelectedSkillId, self.MazeId)
				self:CancelCanBeSkillAt()
				self.SkillMask.gameObject:SetActive(false)
				g_ScriptEvent:BroadcastInLua("MengHuaLuHideSkillView")
				g_ScriptEvent:BroadcastInLua("CancelCanBeSkillAt")
			else
				Gac2Gas.RequestMengHuaLuAttack(self.MazeId)
			end
		end
	end 
end

function LuaMengHuaLuMazeItem:UpdateMazeInfoForPlay()
	-- 进入下一层，要去掉所有动作
	self:DestroyTweenList()
	self:UpdateMazeInfo()
end

-- 更新格子信息
function LuaMengHuaLuMazeItem:UpdateMazeInfo()
	local info = LuaMengHuaLuMgr.m_MazeInfos[self.MazeId]

	if info then
		self.MazeInfo = info
		if info.MazeColor == EnumMengHuaLuGridColor.eCanOpen then
			-- 可点开状态
			self:SetAvailable()
		elseif info.MazeColor == EnumMengHuaLuGridColor.eCanNotOpen then
			-- 不可点开状态
			self:ResetPickItem()
			self:SetNotAvailable()
		else
			-- 已打开状态
			self:SetOpened()
			if info.ItemId and info.ItemId ~= 0 then
				self:LoadItem(info)
			else
				self:ResetPickItem()
			end

			if info.MonsterInfo then
				self:LoadMonster(info)
			else
				self:DestroyMonsterdata()
			end
		end
		
		self.IsLocked:SetActive(info.MazeIsLocked)
	end
end

function LuaMengHuaLuMazeItem:SetAvailable()
	self.AvailableBG:SetActive(true)
	self.NotAvailableBG:SetActive(false)
	self.ItemView:SetActive(false)
	self.MonsterView:SetActive(false)
	self.IsLocked:SetActive(false)
	self:ResetPickItem()
	self:DestroyMonsterdata()
end

function LuaMengHuaLuMazeItem:SetNotAvailable()
	self.AvailableBG:SetActive(false)
	self.NotAvailableBG:SetActive(true)
	self.ItemView:SetActive(false)
	self.MonsterView:SetActive(false)
	self.IsLocked:SetActive(false)
	self:ResetPickItem()
	self:DestroyMonsterdata()
end

function LuaMengHuaLuMazeItem:SetOpened()
	self.AvailableBG:SetActive(false)
	self.NotAvailableBG:SetActive(false)
	self.ItemView:SetActive(false)
	self.MonsterView:SetActive(false)
	self.IsLocked:SetActive(false)
end

-- 加载物品
function LuaMengHuaLuMazeItem:LoadItem(info)
	if not info then return end

	self:DestroyMonsterdata()

	self.AvailableBG:SetActive(false)
	self.NotAvailableBG:SetActive(false)
	self.ItemView:SetActive(true)
	self.MonsterView:SetActive(false)
	self.IsLocked:SetActive(false)

	local item = MengHuaLu_Item.GetData(info.ItemId)
	if not item then return end

	local setting = MengHuaLu_Setting.GetData()
	self.GateIcon:Clear()
	self.ItemIcon:Clear()
	self.ShopIcon:SetActive(false)
	self.ItemIcon.gameObject:SetActive(false)
	self.ItemBottom:SetActive(false)
	self.GateIcon.gameObject:SetActive(false)
	self.BossIcon:SetActive(false)

	if info.ItemId == setting.EntranceOpenItemId or info.ItemId == setting.EntranceCloseItemId then
		self.Fx:DestroyFx()
		self.GateIcon.gameObject:SetActive(true)
		self.GateIcon:LoadMaterial(item.Icon)
		-- 传送门打开的特效
		if info.ItemId == setting.EntranceOpenItemId then
			self.Fx:LoadFx("fx/ui/prefab/UI_chuanshongmen01.prefab")
			self.BossIcon:SetActive(LuaMengHuaLuMgr.NextIsBoss())
		end
	elseif info.ItemId == setting.ShopItemId then
		self.ShopIcon:SetActive(true)
	else
		LuaUtils.SetWidgetsAlpha(self.ItemIcon.transform, 1)
		self.ItemIcon.gameObject:SetActive(true)
		self.ItemBottom:SetActive(true)
		self.ItemIcon:LoadMaterial(item.Icon)
		self.ItemQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Color)
	end
	
end

-- 加载怪物
function LuaMengHuaLuMazeItem:LoadMonster(info)
	if not info then return end

	self.AvailableBG:SetActive(false)
	self.NotAvailableBG:SetActive(false)
	self.ItemView:SetActive(false)
	self.MonsterView:SetActive(true)
	self.IsLocked:SetActive(false)
	self.TempMazeInfo = nil

	local camera = -1.8
	local monster = MengHuaLu_Monster.GetData(info.MonsterInfo.MonsterId)
	if monster then
		camera = monster.Camera
	end
	self.MonsterTexture.mainTexture = CUIManager.CreateModelTexture(self.ModelTextureName, self.ModelTextureLoader, 180, 0.05, camera, 6.9, false, true, 2)
	self:UpdateMonsterProps(info)

	-- 只有主动怪显示
	if monster.ActionRound ~= 0 then
		self.RoundValueLabel.text = tostring(info.MonsterInfo.ActionRound+1)
	end
	self.RoundProperty:SetActive(monster.ActionRound ~= 0)
	self.HPChangLabel.alpha = 0
	self.MonsterFX:DestroyFx()

	-- 是否是boss
	if monster.IsBoss == 1 then
		self:BossScale(600, 600, true)
	else
		self:BossScale(200, 200, false)
	end

	-- 是否是友方怪
	if monster.IsFriend == 1 then
		self.Border.spriteName = "frame_blue_light"
	else
		self.Border.spriteName = "frame_red_light"
	end

end

function LuaMengHuaLuMazeItem:BossScale(width, height, isBoss)

	if self.BG.width ~= width or self.BG.height ~= height or isBoss then
		-- box
		self.Fx:DestroyFx()
		self.MonsterFX:DestroyFx()

		local boxCollider = self.BG.transform:GetComponent(typeof(BoxCollider))
		boxCollider.size = Vector3(width, height, 1)

		-- BG
		self.BG.width = width
		self.BG.height = height

		-- 提高层级
		if isBoss then
			self.BG.depth = 3
		else
			self.BG.depth = 2
		end

		--monsterTexture
		self.MonsterTexture.texture.width = width
		self.MonsterTexture.texture.height = height

		-- 更新位置
		self.AttackProperty:SetActive(false)
		self.AttackProperty:SetActive(true)

		self.HPProperty:SetActive(false)
		self.HPProperty:SetActive(true)
		
		-- 需要根据具体的情况决定是否需要显示
		local isActive = self.RoundProperty.activeSelf
		self.RoundProperty:SetActive(false)
		self.RoundProperty:SetActive(true)
		self.RoundProperty:SetActive(isActive)

		self.Border.gameObject:SetActive(false)
		self.Border.gameObject:SetActive(true)

	end
	
end

-- 加载monster模型(包括动作和特效)
function LuaMengHuaLuMazeItem:LoadModel(ro)
	if self.MazeInfo and self.MazeInfo.MonsterInfo then
		local data = MengHuaLu_Monster.GetData(self.MazeInfo.MonsterInfo.MonsterId)
		local resName = data.ResName
		local resid = SafeStringFormat3("_%02d", data.ResId)
		local prefabname = "Assets/Res/Character/NPC/" .. resName .. "/Prefab/" ..resName .. resid .. ".prefab"
		ro:LoadMain(prefabname, nil, false, false)
		ro.Scale = data.Scale
		ro.Direction = data.Direction

		if self.FXId ~= 0 then
			CEffectMgr.Inst:AddObjectFX(self.FXId, ro, 0, ro.Scale, 1.0, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
			self.FXId = 0
		end

		if self.AniName then
			ro:DoAni(self.AniName, false, 0, 1.0, 0.15, true, 1.0)
			ro.AniEndCallback = DelegateFactory.Action(function ()
				ro.AniEndCallback = nil
				self.AniName = nil
				ro:DoAni("stand01", true, 0, 1.0, 0.15, true, 1.0)
			end)
		else
			ro:DoAni("stand01", true, 0, 1.0, 0.15, true, 1.0)
		end
	end
end

function LuaMengHuaLuMazeItem:UpdateMonsterProps(info)
	if info and info.MonsterInfo then
		self.HPValueLabel.text = tostring(info.MonsterInfo.Hp)
		self.AttackValueLabel.text = tostring(info.MonsterInfo.Attack)
	end
end

-- 更新怪物的信息
function LuaMengHuaLuMazeItem:SyncMengHuaLuCharacterFightProp(grid, propKey, propValue)
	if grid == self.MazeId then
		local info = LuaMengHuaLuMgr.m_MazeInfos[self.MazeId]
		self:UpdateMonsterProps(info)
		-- 增加tempMazeInfo的更新
		if self.TempMazeInfo and self.TempMazeInfo.MonsterInfo then
			if propKey == 1 then
				self.TempMazeInfo.MonsterInfo.Attack = propValue
			elseif propKey == 5 then
				self.TempMazeInfo.MonsterInfo.Hp = propValue
			end
		end
	end
end

-- 更新单个格子（一般是操作之后的交互）
function LuaMengHuaLuMazeItem:UpdateSingleMazeItem(mazeInfo)
	if mazeInfo.MazeID == self.MazeId then
		
		-- 打开的特效延后
		if self.MazeInfo.MazeColor == EnumMengHuaLuGridColor.eCanOpen and mazeInfo.MazeColor == EnumMengHuaLuGridColor.eOpened then
			self.Fx:DestroyFx()
			self.Fx:LoadFx("fx/ui/prefab/UI_gezidakai01.prefab")
		end

		if self.MazeInfo.ItemId ~= 0 then

			-- 检查是否是item被获取
			if mazeInfo.ItemId == 0 then
				LuaMengHuaLuMgr.m_MazeInfos[self.MazeId] = mazeInfo
				self:ShowItemPickAni()
			else
				LuaMengHuaLuMgr.m_MazeInfos[self.MazeId] = mazeInfo
				if mazeInfo.ItemId ~= 0 and self.MazeInfo.ItemId ~= mazeInfo.ItemId then
					self:UpdateMazeInfo()
				end
			end
			
		elseif self.MazeInfo.MonsterInfo ~= nil then
			if mazeInfo.MonsterInfo then
				if mazeInfo.MonsterInfo.Hp == self.MazeInfo.MonsterInfo.Hp then
					LuaMengHuaLuMgr.m_MazeInfos[self.MazeId] = mazeInfo
					if self:MonsterInfoNeedRefresh(mazeInfo) then
						-- 如果monsterId，血量，actionRound相同，则立刻刷新（怪物攻击）
						self:UpdateMazeInfo()
					end
				else
					-- 怪物被打，需要等动画播完了再重新load
					self.TempMazeInfo = mazeInfo
				end
			else
				-- 怪物被打(可能被打死了)，需要等动画播完了再重新load
				-- 如果怪物是boss，增加特效
				local monster = MengHuaLu_Monster.GetData(self.MazeInfo.MonsterInfo.MonsterId)
				if monster and monster.IsBoss == 1 then
					self.BossDropFx:DestroyFx()
					self.BossDropFx:LoadFx("fx/ui/prefab/UI_douhunlibao.prefab")
				end
				self.TempMazeInfo = mazeInfo
			end
		else
			-- 如果已经是打开的门，且要更新的也是打开的门的话，则不更新
			local setting = MengHuaLu_Setting.GetData()
			if mazeInfo.ItemId == setting.EntranceOpenItemId and self.MazeInfo.ItemId == setting.EntranceOpenItemId then
				return
			end
			LuaMengHuaLuMgr.m_MazeInfos[self.MazeId] = mazeInfo
			self:UpdateMazeInfo()
		end
	end
end

-- 怪物格子是否需要刷新
function LuaMengHuaLuMazeItem:MonsterInfoNeedRefresh(newMazeInfo)
	return newMazeInfo.MonsterInfo.Hp ~= self.MazeInfo.MonsterInfo.Hp or newMazeInfo.MonsterInfo.Attack ~= self.MazeInfo.MonsterInfo.Attack
	or newMazeInfo.MonsterInfo.MonsterId ~= self.MazeInfo.MonsterInfo.MonsterId or newMazeInfo.MonsterInfo.ActionRound ~= self.MazeInfo.MonsterInfo.ActionRound
end

----------------------- 物品动画相关 -----------------------

function LuaMengHuaLuMazeItem:ShowItemPickAni()
	local item = MengHuaLu_Item.GetData(self.MazeInfo.ItemId)
	if item.DongXiao ~= 0 and item.DongXiao ~= 6 then
		local target = self:GetItemPosTo(item)
		if target then
			-- 将原有的上下浮动去掉
			local tween = self.ItemIcon.transform:GetComponent(typeof(TweenPosition))
			if tween then
				tween.enabled = false
			end

			local worldPos = target.transform:TransformPoint(self.ItemIcon.transform.localPosition)
			local targetLocalPos = self.ItemIcon.transform:InverseTransformPoint(worldPos)
			self.ItemBottom:SetActive(false)
			local tween1 = LuaTweenUtils.TweenPosition(self.ItemIcon.transform, targetLocalPos.x, targetLocalPos.y, targetLocalPos.z, 0.3)
			table.insert(self.TweenList, tween1)
			local tween2 = LuaTweenUtils.TweenAlpha(self.ItemIcon.transform, 1, 0, 0.3, function ()
				self:UpdateMazeInfo()
				--[[local tweener1 = LuaTweenUtils.TweenScale(target.transform, Vector3(1, 1, 1), Vector3(1.5, 1.5, 1.5), 0.2)
				LuaTweenUtils.SetEase(tweener1, Ease.InOutQuint)
				LuaTweenUtils.OnComplete(tweener1, function ()
					local tweener2 = LuaTweenUtils.TweenScale(target.transform, Vector3(1.5, 1.5, 1.5), Vector3(1, 1, 1), 0.2)
					table.insert(self.TweenList, tweener2)
				end)
				table.insert(self.TweenList, tweener1)--]]
			end)
			table.insert(self.TweenList, tween2)
		end
	else
		-- 没有动效立刻更新
		self:UpdateMazeInfo()
	end
end

-- 获得动效的位置
function LuaMengHuaLuMazeItem:GetItemPosTo(item)
	if item.DongXiao == 1 then
		return self.AttackIcon
	elseif item.DongXiao == 2 then
		return self.HPIcon
	elseif item.DongXiao == 3 then
		return self.MPIcon
	elseif item.DongXiao == 4 then
		return self.MoneyIcon
	elseif item.DongXiao == 5 then
		return self.SkillIcon
	end
	return nil
end

----------------------- 物品动画相关 end -----------------------



----------------------- monster动画相关 -----------------------

-- 怪物攻击动作
function LuaMengHuaLuMazeItem:MonsterAttack(skillId)
	local skill = MengHuaLu_Skill.GetData(skillId)
	if not skill then return end

	self.AniName = skill.CastAnimation
	self.FXId = skill.AttackFX
	self.MonsterFX:DestroyFx()

	local camera = -1.8
	local monster = MengHuaLu_Monster.GetData(self.MazeInfo.MonsterInfo.MonsterId)
	if monster then
		camera = monster.Camera
	end
	self.MonsterTexture.mainTexture = CUIManager.CreateModelTexture(self.ModelTextureName, self.ModelTextureLoader, 180, 0.05, camera, 6.9, false, true, 2)
	
end


-- 怪物受击动作
function LuaMengHuaLuMazeItem:MonsterSuffer(skillId)
	local skill = MengHuaLu_Skill.GetData(skillId)
	if not skill then return end

	local camera = -1.8
	if self.MazeInfo.MonsterInfo then
		local monster = MengHuaLu_Monster.GetData(self.MazeInfo.MonsterInfo.MonsterId)
		if monster then
			camera = monster.Camera
		end
	end
	self.FXId = skill.SufferFX
	self.AniName = skill.SufferAnimation
	self.MonsterTexture.mainTexture = CUIManager.CreateModelTexture(self.ModelTextureName, self.ModelTextureLoader, 180, 0.05, camera, 6.9, false, true, 2)
	self.MonsterFX:DestroyFx()
	if skill.SufferFX_UI == 0 then
		-- 不显示怪物受击特效
		self.MonsterFX:LoadFx("fx/ui/prefab/UI_guaiwushouji01.prefab")
	end
	self:SufferTween(skill.ShockRange)
end

-- 攻击miss只展示一个特效
function LuaMengHuaLuMazeItem:MonsterMiss()
	self.MonsterFX:DestroyFx()
	self.MonsterFX:LoadFx("fx/ui/prefab/UI_shishou.prefab")
end

-- 受击时的缩放效果
function LuaMengHuaLuMazeItem:SufferTween(shockRange)
	if shockRange ~= 0 then
		-- 不处理
		local tween1 = self:GetMonsterScaleTween(1, shockRange, 0.15)
		LuaTweenUtils.OnComplete(tween1, function ()
			local tween2 = self:GetMonsterScaleTween(shockRange, 1, 0.15)
			LuaTweenUtils.OnComplete(tween2, function ()
				local tween3 = self:GetMonsterScaleTween(1, shockRange, 0.15)
				LuaTweenUtils.OnComplete(tween3, function ()
					local tween4 = self:GetMonsterScaleTween(shockRange, 1, 0.15)
					LuaTweenUtils.OnComplete(tween4, function ()
						local tween5 = self:GetMonsterScaleTween(1, 1, 0)
						LuaTweenUtils.SetDelay(tween5, 0.2)
						LuaTweenUtils.OnComplete(tween5, function ()
							-- 更新moster的信息
							if self.TempMazeInfo then
								LuaMengHuaLuMgr.m_MazeInfos[self.MazeId] = self.TempMazeInfo
								self.TempMazeInfo = nil
								self:UpdateMazeInfo()
							end
						end)
						table.insert(self.TweenList, tween5)
					end)
					table.insert(self.TweenList, tween4)
				end)
				table.insert(self.TweenList, tween3)
			end)
			table.insert(self.TweenList, tween2)
		end)
		table.insert(self.TweenList, tween1)
	else
		if self.TempMazeInfo then
			LuaMengHuaLuMgr.m_MazeInfos[self.MazeId] = self.TempMazeInfo
			self.TempMazeInfo = nil
			self:UpdateMazeInfo()
		end
	end
	
end

function LuaMengHuaLuMazeItem:GetMonsterScaleTween(origin, final, time)
	local tween = LuaTweenUtils.TweenScale(self.MonsterTexture.transform, Vector3(origin, origin, 1), Vector3(final, final, 1), time)
	LuaTweenUtils.SetEase(tween, Ease.InOutQuint)
	return tween
end

function LuaMengHuaLuMazeItem:Update()
	if not self.StartTime then
		self.StartTime = Time.realtimeSinceStartup
	end
	if not self.IntervalTime then
		self.IntervalTime = 0.8
	end

	if Time.realtimeSinceStartup - self.StartTime > self.IntervalTime then
		local action = self:GetActionToExecute()
		if action then
			self.StartTime = Time.realtimeSinceStartup
			self.IntervalTime = action.Interval
			self:ExecuteAction(action.SrcGridId, action.DestGridId, action.SkillId, action.IsMissed)
		end
	end
end

function LuaMengHuaLuMazeItem:ExecuteAction(srcGridId, destGridId, skillId, isMissed)
	local skill = MengHuaLu_Skill.GetData(skillId)
	if not skill then return end

	if srcGridId == self.MazeId then
		self:MonsterAttack(skillId)
	end
	if destGridId == self.MazeId then
		if not isMissed then
			self:MonsterSuffer(skillId)
		else
			self:MonsterMiss()
		end
	end
end


function LuaMengHuaLuMazeItem:AddAction(action)
	if action.SrcGridId == self.MazeId or action.DestGridId == self.MazeId then
		table.insert(self.ActionQueue, action)
	end
end

function LuaMengHuaLuMazeItem:GetActionToExecute()
	if self.ActionQueue and #self.ActionQueue > 0 then
		return table.remove(self.ActionQueue, 1)
	end
	return nil
end

----------------------- monster动画相关 end -----------------------


function LuaMengHuaLuMazeItem:ShowCanBeSkillAt(skillId)
	if not self.MazeInfo then return end
	-- 只有点开的才能被选为释放技能单位
	if self.MazeInfo.MazeColor == EnumMengHuaLuGridColor.eOpened then
		if not self.MazeInfo.MonsterInfo then return end
		-- 必须不是友方怪
		local monster = MengHuaLu_Monster.GetData(self.MazeInfo.MonsterInfo.MonsterId)
		if monster.IsFriend == 1 then return end

		self.SelectedSkillId = skillId
		self:SetDepth((self.SkillMask.depth+1))
	end
end

function LuaMengHuaLuMazeItem:CancelCanBeSkillAt()
	if self.SelectedSkillId ~= 0 then
		self.SelectedSkillId = 0
		self:SetDepth(-(self.SkillMask.depth+1))
	end
end

-- 更新对应的tempMazeInfo
function LuaMengHuaLuMazeItem:MengHuaLuUpdateMonsterBuffs(grid, buffList)
	if self.MazeId == grid then
		if self.TempMazeInfo and self.TempMazeInfo.MonsterInfo then
			self.TempMazeInfo.MonsterInfo.BuffData = buffList
		end
	end
end

function LuaMengHuaLuMazeItem:OnEnable()
	g_ScriptEvent:AddListener("AddMonsterAction", self, "AddAction")
	g_ScriptEvent:AddListener("UpdateMazeInfo", self, "UpdateMazeInfoForPlay")
	g_ScriptEvent:AddListener("UpdateSingleMazeItem", self, "UpdateSingleMazeItem")
	g_ScriptEvent:AddListener("ShowCharacterHpChange", self, "ShowCharacterHpChange")
	g_ScriptEvent:AddListener("ShowCanBeSkillAt", self, "ShowCanBeSkillAt")
	g_ScriptEvent:AddListener("CancelCanBeSkillAt", self, "CancelCanBeSkillAt")
	g_ScriptEvent:AddListener("SyncMengHuaLuCharacterFightProp", self, "SyncMengHuaLuCharacterFightProp")
	g_ScriptEvent:AddListener("SyncMengHuaLuCharacterDie", self, "SyncMengHuaLuCharacterDie")
	g_ScriptEvent:AddListener("MengHuaLuUpdateMonsterBuffs", self, "MengHuaLuUpdateMonsterBuffs")
end

function LuaMengHuaLuMazeItem:OnDisable()
	g_ScriptEvent:RemoveListener("AddMonsterAction", self, "AddAction")
	g_ScriptEvent:RemoveListener("UpdateMazeInfo", self, "UpdateMazeInfoForPlay")
	g_ScriptEvent:RemoveListener("UpdateSingleMazeItem", self, "UpdateSingleMazeItem")
	g_ScriptEvent:RemoveListener("ShowCharacterHpChange", self, "ShowCharacterHpChange")
	g_ScriptEvent:RemoveListener("ShowCanBeSkillAt", self, "ShowCanBeSkillAt")
	g_ScriptEvent:RemoveListener("CancelCanBeSkillAt", self, "CancelCanBeSkillAt")
	g_ScriptEvent:RemoveListener("SyncMengHuaLuCharacterFightProp", self, "SyncMengHuaLuCharacterFightProp")
	g_ScriptEvent:RemoveListener("SyncMengHuaLuCharacterDie", self, "SyncMengHuaLuCharacterDie")
	g_ScriptEvent:RemoveListener("MengHuaLuUpdateMonsterBuffs", self, "MengHuaLuUpdateMonsterBuffs")
	self:DestroyTweenList()
end

function LuaMengHuaLuMazeItem:OnDestroy()
	self:DestroyMonsterdata()
end

function LuaMengHuaLuMazeItem:DestroyTweenList()
	if not self.TweenList or #self.TweenList <= 0 then return end
	for i = 1, #self.TweenList, 1 do
		LuaTweenUtils.Kill(self.TweenList[i], false)
	end
end

function LuaMengHuaLuMazeItem:SyncMengHuaLuCharacterDie(grid)
	if self.MazeId == grid then
		-- 怪物不是被技能打死的
		local updateTween = self:GetMonsterScaleTween(1, 1, 0)
		LuaTweenUtils.SetDelay(updateTween, 1.0)
		LuaTweenUtils.OnComplete(updateTween, function ()
			-- 更新moster的信息
			if self.TempMazeInfo then
				LuaMengHuaLuMgr.m_MazeInfos[self.MazeId] = self.TempMazeInfo
				self.TempMazeInfo = nil
				self:UpdateMazeInfo()
			end
		end)
		table.insert(self.TweenList, updateTween)
	end
end

function LuaMengHuaLuMazeItem:ShowCharacterHpChange(grid, hpChange)
	if grid == self.MazeId and self.MazeInfo and self.MazeInfo.MonsterInfo then
		-- 怪物加血不飘字
		if hpChange > 0 then return end
		self.HPChangLabel.text = tostring(hpChange)
		local tween1 = LuaTweenUtils.TweenAlpha(self.HPChangLabel.transform, 0, 1, 0.3)
		local tween2 = LuaTweenUtils.TweenPositionY(self.HPChangLabel.transform, 60, 0.3)
		table.insert(self.TweenList, tween1)
		table.insert(self.TweenList, tween2)
		local tween3 = LuaTweenUtils.TweenAlpha(self.HPChangLabel.transform, 1, 0, 0.5)
		LuaTweenUtils.SetDelay(tween3, 1.0)
		LuaTweenUtils.OnComplete(tween3, function ()
			local tween4 = LuaTweenUtils.TweenPositionY(self.HPChangLabel.transform, 0, 0)
			table.insert(self.TweenList, tween4)
		end)
		table.insert(self.TweenList, tween3)
	end
end

function LuaMengHuaLuMazeItem:SetDepth(depth)
	if depth > 0 then
		self.gameObject:AddComponent(typeof(UIPanel))
		local panel = self.transform:GetComponent(typeof(UIPanel))
		panel.depth = panel.depth + depth
		self.Fx:DestroyFx()
		self.MonsterFX:DestroyFx()
		self.BossDropFx:DestroyFx()

		self.gameObject:SetActive(false)
		self.gameObject:SetActive(true)
	else
		local panel = self.transform:GetComponent(typeof(UIPanel))
		panel.depth = panel.depth + depth
		GameObject.Destroy(self.transform:GetComponent(typeof(UIPanel)))
	end

	if self.MazeInfo and self.MazeInfo.MonsterInfo then
		local monster = MengHuaLu_Monster.GetData(self.MazeInfo.MonsterInfo.MonsterId)
		if monster and monster.IsBoss == 1 then
			self:BossScale(600, 600, true)
		else
			self:BossScale(200, 200, false)
		end
	end
end

function LuaMengHuaLuMazeItem:OnItemLongPressed()
	if self.MazeInfo.MonsterInfo then
		-- 如果是怪物，显示mosnter界面
		LuaMengHuaLuMgr.ShowMonsterInfo(self.MazeId)
	end
end


return LuaMengHuaLuMazeItem
