local LuaUtils = import "LuaUtils"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CRankData = import "L10.UI.CRankData"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local Vector3 = import "UnityEngine.Vector3"

LuaBaoZhuPlayShowWnd = class()
RegistChildComponent(LuaBaoZhuPlayShowWnd,"WndQnRadioBox", QnRadioBox)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"node1", GameObject)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"node2", GameObject)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"tipLabel", UILabel)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"attendBtn", GameObject)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"tipLabel1", GameObject)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"CloseButton", GameObject)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"ItemTemplate", GameObject)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"TableBody", CUIRestrictScrollView)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"Grid", UIGrid)
RegistChildComponent(LuaBaoZhuPlayShowWnd,"EmptyNode", GameObject)

--RegistClassMember(LuaBaoZhuPlayShowWnd,"m_EvaluateInterval")
--RegistClassMember(LuaBaoZhuPlayShowWnd,"m_LastEvaluateTime")

function LuaBaoZhuPlayShowWnd:Close()
  CUIManager.CloseUI("BaoZhuPlayShowWnd")
end

function LuaBaoZhuPlayShowWnd:ShowNode1()
  self.node1:SetActive(true)
  self.node2:SetActive(false)

  local msg = g_MessageMgr:FormatMessage("BaoZhuPlayRule")
  self.node1.transform:Find('scrollView/table/templateNode/tableLabel'):GetComponent(typeof(UILabel)).text = msg
end

function LuaBaoZhuPlayShowWnd:ClearNode2()
  Extensions.RemoveAllChildren(self.Grid.transform)
  self.ItemTemplate:SetActive(false)
  self.EmptyNode:SetActive(false)
end

function LuaBaoZhuPlayShowWnd:ShowNode2()
  self.node1:SetActive(false)
  self.node2:SetActive(true)

  self:ClearNode2()
  Gac2Gas.QueryRank(Chunjie2021_CareFirework.GetData().RankId)
end

function LuaBaoZhuPlayShowWnd:InitRankNodeData(node,data)
  node:SetActive(true)
  local rankLabel = node.transform:Find('RankLabel'):GetComponent(typeof(UILabel))
  local rankSprite = node.transform:Find('RankLabel/RankImage'):GetComponent(typeof(UISprite))
  rankSprite.gameObject:SetActive(true)
  local rank=data.rank
  if rank==1 then
    rankSprite.spriteName="Rank_No.1"
  elseif rank==2 then
    rankSprite.spriteName="Rank_No.2png"
  elseif rank==3 then
    rankSprite.spriteName="Rank_No.3png"
  else
    rankLabel.text=tostring(rank)
    rankSprite.gameObject:SetActive(false)
  end

  node.transform:Find('NameLabel'):GetComponent(typeof(UILabel)).text = data.name
  node.transform:Find('ScoreLabel3'):GetComponent(typeof(UILabel)).text = data.score
  node.transform:Find('ScoreLabel1'):GetComponent(typeof(UILabel)).text = data.score1
  node.transform:Find('ScoreLabel2'):GetComponent(typeof(UILabel)).text = data.score2

  if rank % 2 == 1 then
    local bg = node.transform:Find('bg').gameObject
    bg:SetActive(false)
  end

  local nameBtn = node.transform:Find('NameLabel').gameObject
  UIEventListener.Get(nameBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
    CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
  end)
end

function LuaBaoZhuPlayShowWnd:OnRankDataReady()
--	local PlayerRankInfo = CRankData.Inst.MainPlayerRankInfo
--	if PlayerRankInfo then
--		local selfData = {name = CClientMainPlayer.Inst.Name,rank = PlayerRankInfo.Rank,score = PlayerRankInfo.Value,job = CClientMainPlayer.Inst.Class}
--		self:InitRankNodeData(self.selfNode,selfData)
--	end

	Extensions.RemoveAllChildren(self.Grid.transform)

	if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
		do
			local i = 0
			while i < CRankData.Inst.RankList.Count do
				local info = CRankData.Inst.RankList[i]
				local go = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate)
        local extraList = MsgPackImpl.unpack(info.extraData)

        local score1 = extraList[0]
        local score2 = extraList[1]

				local data = {name = info.Name,rank = info.Rank,score = info.Value,score1 = score1, score2 = score2,playerId = info.PlayerIndex}
				self:InitRankNodeData(go,data)
				i = i + 1
			end
      if i == 0 then
        self.EmptyNode:SetActive(true)
      else
        self.EmptyNode:SetActive(false)
      end
		end

		self.Grid:Reposition()
		self.TableBody:ResetPosition()
  else
    self.EmptyNode:SetActive(true)
	end
end

function LuaBaoZhuPlayShowWnd:Init()

  UIEventListener.Get(self.CloseButton).onClick = LuaUtils.VoidDelegate(function ( ... )
    self:Close()
  end)

  self.WndQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
    if index == 0 then
      self:ShowNode1()
    elseif index == 1 then
      self:ShowNode2()
    end
  end)

  if LuaBaoZhuPlayMgr.InitRank then
    self.WndQnRadioBox:ChangeTo(1,true)
    LuaBaoZhuPlayMgr.InitRank = nil
  else
    self.WndQnRadioBox:ChangeTo(0,true)
  end

  self:InitBottom()
  self:InitRestNum()
end

function LuaBaoZhuPlayShowWnd:InitBottom()
  if LuaBaoZhuPlayMgr.InMatch then
    self.tipLabel1:SetActive(true)

    self.attendBtn.transform:Find('label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('取消报名')
    UIEventListener.Get(self.attendBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
      Gac2Gas.CareFireworkCancelSignUp()
    end)
  else
    self.tipLabel1:SetActive(false)

    self.attendBtn.transform:Find('label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('报名参与')
    UIEventListener.Get(self.attendBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
      Gac2Gas.SignupCareFireworkMatch()
    end)
  end

end

function LuaBaoZhuPlayShowWnd:InitRestNum()
  local maxNum = Chunjie2021_CareFirework.GetData().Phase2MaxRewardTimes
  local restNum = LuaBaoZhuPlayMgr.PlayUseNum
  self.tipLabel.text = SafeStringFormat3(LocalString.GetString('个人奖励次数(%s/%s)'),restNum,maxNum)
end

function LuaBaoZhuPlayShowWnd:OnDestroy()
end

function LuaBaoZhuPlayShowWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("UpdateBaoZhuPlayShowInfo", self, "InitBottom")
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaBaoZhuPlayShowWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("UpdateBaoZhuPlayShowInfo", self, "InitBottom")
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end


return LuaBaoZhuPlayShowWnd
