-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CGuildMemberChartWnd = import "L10.UI.CGuildMemberChartWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local GuildMemberExportMgr = import "L10.Game.GuildMemberExportMgr"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildMemberChartWnd.m_Init_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.itemTable.transform)
    CommonDefs.ListClear(this.buttons)
    do
        local i = 0
        while i < GuildMemberExportMgr.Inst.allItems.Count do
            local instance = CUICommonDef.AddChild(this.itemTable.gameObject, this.itemTemplate)
            instance:SetActive(true)
            local button = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CButton))
            button.Text = GuildMemberExportMgr.Inst.allItems[i].name
            UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
            CommonDefs.ListAdd(this.buttons, typeof(CButton), button)
            i = i + 1
        end
    end
    this.itemTable:Reposition()
    this.itemScrollView:ResetPosition()
    this:SetPieSelected(true)
    this:OnItemClick(this.buttons[0].gameObject)
end
CGuildMemberChartWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.barBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.barBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnBarButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.pieBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.pieBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnPieButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
end
CGuildMemberChartWnd.m_OnBarButtonClick_CS2LuaHook = function (this, go) 
    this:SetPieSelected(false)
    do
        local i = 0
        while i < this.buttons.Count do
            if this.buttons[i].Selected then
                this:ShowChart(i, false)
                break
            end
            i = i + 1
        end
    end
end
CGuildMemberChartWnd.m_OnPieButtonClick_CS2LuaHook = function (this, go) 
    this:SetPieSelected(true)
    do
        local i = 0
        while i < this.buttons.Count do
            if this.buttons[i].Selected then
                this:ShowChart(i, true)
                break
            end
            i = i + 1
        end
    end
end
CGuildMemberChartWnd.m_OnItemClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.buttons.Count do
            if this.buttons[i].gameObject == go then
                this.buttons[i].Selected = true
                this:ShowChart(i, this.pieBtn.Selected)
            else
                this.buttons[i].Selected = false
            end
            i = i + 1
        end
    end
end
CGuildMemberChartWnd.m_ShowChart_CS2LuaHook = function (this, index, isPieChart) 
    if index < 0 or index >= GuildMemberExportMgr.Inst.allItems.Count then
        return
    end
    local item = GuildMemberExportMgr.Inst.allItems[index]

    this.pieChartView.gameObject:SetActive(isPieChart)
    this.barChartView.gameObject:SetActive(not isPieChart)
    if isPieChart then
        local values = CreateFromClass(MakeArrayClass(System.Single), item.values.Length)
        do
            local i = 0
            while i < values.Length do
                values[i] = item.values[i]
                i = i + 1
            end
        end
        this.pieChartView:Init(values, item.texts)
    else
        local values = CreateFromClass(MakeArrayClass(System.Int32), item.values.Length)
        do
            local i = 0
            while i < values.Length do
                values[i] = item.values[i]
                i = i + 1
            end
        end
        this.barChartView:Init(values, item.texts)
    end
end
