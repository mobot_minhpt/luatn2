-- Auto Generated!!
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CEquipSearchParams = import "L10.UI.CYuanbaoMarketMgr+CEquipSearchParams"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local CYuanbaoMarketWnd = import "L10.UI.CYuanbaoMarketWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local Mall_YuanBaoMarket = import "L10.Game.Mall_YuanBaoMarket"
local MsgPackImpl = import "MsgPackImpl"
local PreSellContextType = import "L10.UI.PreSellContextType"
local QnAddSubInputButtonMgr = import "L10.UI.QnAddSubInputButtonMgr"
local QnButton = import "L10.UI.QnButton"
local QnTabButton = import "L10.UI.QnTabButton"
local System = import "System"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local SearchOption = import "L10.UI.SearchOption"
local EClearOption = import "L10.Engine.EClearOption"

CYuanbaoMarketWnd.m_Init_CS2LuaHook = function (this) 
    CPlayerShopMgr.Inst:LoadHistories()
    this.titleLabel.text = LocalString.GetString("交易市场")
    this.tabView.OnSelect = MakeDelegateFromCSFunction(this.OnTabViewChanged, MakeGenericClass(Action2, QnTabButton, Int32), this)
    this.tabView:ChangeTo(CYuanbaoMarketMgr.TabViewIndex)

    this.sellBuyRadioBox.OnSelect = MakeDelegateFromCSFunction(this.OnSellBuyRadioBoxSelect, MakeGenericClass(Action2, QnButton, Int32), this)
    this.sellBuyRadioBox:ChangeTo(CYuanbaoMarketMgr.RadioBoxSelectIndex, true)

    --设置玩家商店不开放
    if not L10.Engine.CSwitchMgr.PlayerShopEnabled then
        this.tabView:SetTabsNotOpen(Table2ArrayWithCount({1}, 1, MakeArrayClass(System.Int32)))
    end
    --在跨服上不去查询数据
    if CClientMainPlayer.Inst and not CClientMainPlayer.Inst.IsCrossServerPlayer then
        Gac2Gas.GetWolrdOnShelfCount()
        Gac2Gas.GetClassfiedOnShelfData()
    end
    if CYuanbaoMarketWnd.s_forceGcOnInit then
        CResourceMgr.Inst:DoClear(EClearOption.ForceUnloadAsset)
    end
end
CYuanbaoMarketWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this)
    EventManager.AddListenerInternal(EnumEventType.PreSellMarketItemDone, MakeDelegateFromCSFunction(this.OnPreSellDone, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))
    this.auctionTabGO:SetActive(CAuctionMgr.IsOpen())
end
CYuanbaoMarketWnd.m_OnDisable_CS2LuaHook = function (this) 
    CYuanbaoMarketMgr.ClearYuanbaoMarketBuyData()
    CYuanbaoMarketMgr.ClearYuanbaoMarketSellData()
    CYuanbaoMarketMgr.ClearPlayerShopData()
    EventManager.RemoveListenerInternal(EnumEventType.PreSellMarketItemDone, MakeDelegateFromCSFunction(this.OnPreSellDone, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))
end
CYuanbaoMarketWnd.m_OnSellBuyRadioBoxSelect_CS2LuaHook = function (this, button, index) 
    if index == 0 then
        this.buyWindow.gameObject:SetActive(true)
        this.sellWindow.gameObject:SetActive(false)
        this.buyWindow:Init()
    else
        this.buyWindow.gameObject:SetActive(false)
        this.sellWindow.gameObject:SetActive(true)
        this.sellWindow:Init(CYuanbaoMarketMgr.SellItemId)
    end
end
CYuanbaoMarketWnd.m_OnTabViewChanged_CS2LuaHook = function (this, button, index) 
    CYuanbaoMarketMgr.TabViewIndex = index
    if index == 0 then
        this.titleLabel.text = LocalString.GetString("交易市场")
        this.buyWindow:Init()
    elseif index == 1 then
        this.titleLabel.text = LocalString.GetString("玩家商店")
    elseif index == 2 then
        this.titleLabel.text = LocalString.GetString("拍卖行")
    end

    EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {CUIResources.YuanBaoMarketWnd.Name})
end
CYuanbaoMarketWnd.m_OnPreSellDone_CS2LuaHook = function (this, itemId, maxCount, price) 
    --上下文不一致，不处理该消息
    --上下文不一致，不处理该消息
    if CUICommonDef.PreSellContext ~= PreSellContextType.YuanBaoMarket then
        return
    end
    local message = g_MessageMgr:FormatMessage("MARKET_PRESELL_DONE", price)
    CLuaNumberInputMgr.ShowNumInputBox(1, maxCount, QnAddSubInputButtonMgr.CurValue, function (count) 
        Gac2Gas.SellMarketItem(itemId, count)
    end, message, 15)
end
CYuanbaoMarketWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "Get2ndTab" then
        return this.tabView:GetTabGameObject(1)
    elseif methodName == "GetSellTabButton" then
        if this.sellTabButton.activeInHierarchy then
            return this.sellTabButton
        end
        return nil
    elseif methodName == "GetWatchInfoButton" then
        return this:GetWatchInfoButton()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误 ") .. methodName)
        return nil
    end
end

CYuanbaoMarketMgr.m_ShowPlayerShop_CS2LuaHook = function (templateId) 
    CYuanbaoMarketMgr.ClearPlayerShopData()
    CYuanbaoMarketMgr.TabViewIndex = 1
    CYuanbaoMarketMgr.SearchTemplateId = templateId

    CYuanbaoMarketMgr.Jump2SelectType = true

    local equipId = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
    local equip = EquipmentTemplate_Equip.GetData(equipId)
    if equip then
        local color = equip.Color
        if EquipmentTemplate_Color.GetData(color).Name == LocalString.GetString("鬼装") or EquipmentTemplate_Color.GetData(color).Name == LocalString.GetString("蓝色") then
            CYuanbaoMarketMgr.SearchEquipValueable = SearchOption.Valueable
        end
    end

    CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
end
CYuanbaoMarketMgr.m_BuyFurnitureInYuanBaoMarket_CS2LuaHook = function (furnitureType, level) 
    CYuanbaoMarketMgr.ClearPlayerShopData()
    CYuanbaoMarketMgr.TabViewIndex = 1
    CYuanbaoMarketMgr.PlayerShopBuyRowIdx = 1
    CYuanbaoMarketMgr.PlayerShopBuySectionIdx = 7

    CYuanbaoMarketMgr.FurnitureSearchParam = CreateFromClass(CEquipSearchParams)
    CYuanbaoMarketMgr.FurnitureSearchParam.Type = furnitureType
    CYuanbaoMarketMgr.FurnitureSearchParam.Level = level

    CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
end
CYuanbaoMarketMgr.m_ShowPlayerShopBuySection_CS2LuaHook = function (buysection, buyrow) 

    CYuanbaoMarketMgr.ClearPlayerShopData()
    CYuanbaoMarketMgr.TabViewIndex = 1
    CYuanbaoMarketMgr.PlayerShopBuyRowIdx = buyrow
    CYuanbaoMarketMgr.PlayerShopBuySectionIdx = buysection
    CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
end
CYuanbaoMarketMgr.m_ShowPlayerShopEquipSearch_CS2LuaHook = function (level, color, type, subtype) 
    CYuanbaoMarketMgr.EquipSearchParams = CreateFromClass(CEquipSearchParams)
    CYuanbaoMarketMgr.EquipSearchParams.Level = level
    CYuanbaoMarketMgr.EquipSearchParams.Color = color
    CYuanbaoMarketMgr.EquipSearchParams.Type = type
    CYuanbaoMarketMgr.EquipSearchParams.SubType = subtype

    if CYuanbaoMarketMgr.EquipSearchParams.IsTalisman then
        CYuanbaoMarketMgr.ShowPlayerShopBuySection(1, 2)
    else
        CYuanbaoMarketMgr.ShowPlayerShopBuySection(1, 0)
    end
end
CYuanbaoMarketMgr.m_ClearYuanbaoMarketBuyData_CS2LuaHook = function () 

    CYuanbaoMarketMgr.RadioBoxSelectIndex = 0
    CYuanbaoMarketMgr.SelectMarketIndex = 0
    CYuanbaoMarketMgr.BuyItemTemplateId = 0
    CYuanbaoMarketMgr.BuyWindowSectionIdx = 0
    CYuanbaoMarketMgr.BuyWindowRowIdx = 0
end
CYuanbaoMarketMgr.m_ClearPlayerShopData_CS2LuaHook = function () 

    CYuanbaoMarketMgr.SearchTemplateId = 0
    CYuanbaoMarketMgr.PlayerShopBuySectionIdx = 0
    CYuanbaoMarketMgr.PlayerShopBuyRowIdx = 0

    CYuanbaoMarketMgr.Jump2SelectType = false
    CYuanbaoMarketMgr.SearchEquipValueable = SearchOption.Normal
end
CYuanbaoMarketMgr.m_SellItemInYuanBaoMarket_CS2LuaHook = function (itemId) 

    CYuanbaoMarketMgr.ClearYuanbaoMarketSellData()
    CYuanbaoMarketMgr.TabViewIndex = 0
    CYuanbaoMarketMgr.RadioBoxSelectIndex = 1
    CYuanbaoMarketMgr.SellItemId = itemId
    CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
end
CYuanbaoMarketMgr.m_BuyItemInYuanBaoMarket_CS2LuaHook = function (templateId) 

    CYuanbaoMarketMgr.ClearYuanbaoMarketBuyData()
    CYuanbaoMarketMgr.TabViewIndex = 0
    CYuanbaoMarketMgr.BuyItemTemplateId = templateId
    local yuanbaoItem = Mall_YuanBaoMarket.GetData(templateId)
    if yuanbaoItem ~= nil then
        CYuanbaoMarketMgr.BuyWindowSectionIdx = yuanbaoItem.Category - 1
        CYuanbaoMarketMgr.BuyWindowRowIdx = yuanbaoItem.Category2 - 1
    end
    CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
end
CYuanbaoMarketMgr.m_OpenYuanBaoSellMarket_CS2LuaHook = function (sectionIndex, rowIndex) 

    CYuanbaoMarketMgr.ClearYuanbaoMarketBuyData()
    CYuanbaoMarketMgr.ClearYuanbaoMarketSellData()
    CYuanbaoMarketMgr.TabViewIndex = 0
    CYuanbaoMarketMgr.BuyWindowSectionIdx = sectionIndex
    CYuanbaoMarketMgr.BuyWindowRowIdx = rowIndex
    CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
end
Gas2Gac.PlayerBuyMallItemResult = function (regionId, itemTemplateId, itemCount, result, failedReason, extra) 
    -- resionId: 
    -- eJade = 1,                                  -- 灵玉商店
    -- eJadeLimit = 2,                             -- 灵玉商店限量区
    -- eYuanBao = 3,                               -- 元宝商店
    -- eMarket = 4,                                -- 元宝市场

    -- failedReason:
    --	eException = 0,
    --	eGmForbid = 1,
    --	eConflictStatus = 2,
    --	eItemNotFound = 3,
    --	eNotEnoughMoney = 4,
    --	eNotEnoughBagSpace = 5,
    --	eLimit = 6,

    if result then
        EventManager.BroadcastInternalForLua(EnumEventType.PlayerBuyMallItemSuccess, {regionId, itemTemplateId, itemCount, extra})
    end
end
Gas2Gac.ReplyAutoShangJiaItem = function (data) 
    local itemTbl = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, UInt32)))
    local targetData = MsgPackImpl.unpack(data)--, typeof(MakeGenericClass(List, Object)))

    do
        local i = 0
        while i < targetData.Count do
            local regionId = math.floor(tonumber(targetData[i] or 0))
            local itemId = math.floor(tonumber(targetData[i + 1] or 0))
            if not CommonDefs.DictContains(itemTbl, typeof(Int32), regionId) then
                CommonDefs.DictAdd(itemTbl, typeof(Int32), regionId, typeof(MakeGenericClass(List, UInt32)), CreateFromClass(MakeGenericClass(List, UInt32)))
            end
            CommonDefs.ListAdd(CommonDefs.DictGetValue(itemTbl, typeof(Int32), regionId), typeof(UInt32), itemId)
            i = i + 2
        end
    end

    CommonDefs.DictIterate(itemTbl, DelegateFactory.Action_object_object(function (___key, ___value) 
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        EventManager.BroadcastInternalForLua(EnumEventType.AutoShangJiaItemUpdate, {kv.Key, kv.Value})
    end))
end
Gas2Gac.ReplyAutoShangJiaItemMoneyAndDiscount = function (data) 
    local itemTbl = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, UInt32)))
    local targetData = MsgPackImpl.unpack(data)

    do
        local i = 0
        while i < targetData.Count do
            local regionId = math.floor(tonumber(targetData[i] or 0))
            local itemId = math.floor(tonumber(targetData[i + 1] or 0))
            if not CommonDefs.DictContains(itemTbl, typeof(Int32), regionId) then
                CommonDefs.DictAdd(itemTbl, typeof(Int32), regionId, typeof(MakeGenericClass(List, UInt32)), CreateFromClass(MakeGenericClass(List, UInt32)))
            end
            CommonDefs.ListAdd(CommonDefs.DictGetValue(itemTbl, typeof(Int32), regionId), typeof(UInt32), itemId)
            i = i + 4
        end
    end

    CommonDefs.DictIterate(itemTbl, DelegateFactory.Action_object_object(function (___key, ___value) 
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        EventManager.BroadcastInternalForLua(EnumEventType.AutoShangJiaItemUpdate, {kv.Key, kv.Value})
    end))
end
Gas2Gac.SyncManualShangJiaItem = function (data)
    -- local itemTbl = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, UInt32)))
    local result = MsgPackImpl.unpack(data)
    -- print("SyncManualShangJiaItem", result.Count)
    for i = 0, result.Count-1, 4 do
        local regionId = math.floor(tonumber(result[i] or 0))
        local itemId = math.floor(tonumber(result[i+1] or 0))
        local expireTime = math.floor(tonumber(result[i+2] or 0))
        local isClientShow = math.floor(tonumber(result[i+3] or 0))
        -- print(regionId, itemId, expireTime, isClientShow)

        -- local now = os.time()
        -- if expireTime > 0 and now < expireTime and isClientShow > 0 then
        --     if not CommonDefs.DictContains(itemTbl, typeof(Int32), regionId) then
        --         CommonDefs.DictAdd(itemTbl, typeof(Int32), regionId, typeof(MakeGenericClass(List, UInt32)), CreateFromClass(MakeGenericClass(List, UInt32)))
        --     end
        --     CommonDefs.ListAdd(CommonDefs.DictGetValue(itemTbl, typeof(Int32), regionId), typeof(UInt32), itemId)
        -- end
    end

    -- CommonDefs.DictIterate(itemTbl, DelegateFactory.Action_object_object(function (___key, ___value) 
    --     local kv = {}
    --     kv.Key = ___key
    --     kv.Value = ___value
    --     EventManager.BroadcastInternalForLua(EnumEventType.AutoShangJiaItemUpdate, {kv.Key, kv.Value})
    -- end))
end

