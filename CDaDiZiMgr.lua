-- Auto Generated!!
local BiWu_Setting = import "L10.Game.BiWu_Setting"
local Buff_Buff = import "L10.Game.Buff_Buff"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonDPSInfoDisplayMgr = import "L10.UI.CCommonDPSInfoDisplayMgr"
local CDaDiZiMgr = import "L10.Game.CDaDiZiMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CScene = import "L10.Game.CScene"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DaDiZiBuffInfo = import "L10.Game.DaDiZiBuffInfo"
local DaDiZiWatchInfo = import "L10.Game.DaDiZiWatchInfo"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumClass = import "L10.Game.EnumClass"
local EnumDPSInfoDisplayType = import "L10.UI.EnumDPSInfoDisplayType"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local String = import "System.String"
CDaDiZiMgr.m_ParsePlayerInfo_CS2LuaHook = function (this, info, playerBasic_U, playerBuff_U) 
    local list = TypeAs(MsgPackImpl.unpack(playerBasic_U), typeof(MakeGenericClass(List, Object)))
    if list.Count == 1 then
        --DaDiZiPlayerInfo info = CDaDiZiMgr.Inst.playerInfo1;
        info.playerId = tonumber(list[0])
        info.playerName = ""
        info.cls = 0
        info.gender = 0
        info.level = 0
        info.hp = 0
        info.curHpFull = 0
        info.hpFull = 0

        info.mp = 0
        info.curMpFull = 0
        info.mpFull = 0

        info.DPS = 0
        info.buffs = nil
        info.debuffs = nil
    else
        --DaDiZiPlayerInfo info = CDaDiZiMgr.Inst.playerInfo1;
        info.playerId = tonumber(list[0])
        info.playerName = tostring(list[1])
        info.cls = math.floor(tonumber(list[2] or 0))
        info.gender = math.floor(tonumber(list[3] or 0))
        info.level = math.floor(tonumber(list[4] or 0))

        info.hp = tonumber(list[5])
        info.curHpFull = tonumber(list[6])
        info.hpFull = tonumber(list[7])

        info.mp = tonumber(list[8])
        info.curMpFull = tonumber(list[9])
        info.mpFull = tonumber(list[10])

        info.DPS = tonumber(list[11])

        info.buffs = CreateFromClass(MakeGenericClass(List, DaDiZiBuffInfo))
        info.debuffs = CreateFromClass(MakeGenericClass(List, DaDiZiBuffInfo))

        local list2 = TypeAs(MsgPackImpl.unpack(playerBuff_U), typeof(MakeGenericClass(List, Object)))
        local kind2buffDict = CreateFromClass(MakeGenericClass(Dictionary, Int32, DaDiZiBuffInfo))
        do
            local i = 0
            while i < list2.Count do
                local key = math.floor(tonumber(list2[i] or 0))
                local time = math.floor(tonumber(list2[i + 1] or 0))
                local b = Buff_Buff.GetData(key)
                if b ~= nil then
                    if b.Kind >= 0 then
                        if b.Kind > 0 then
                            if CommonDefs.DictContains(kind2buffDict, typeof(Int32), b.Kind) then
                                local bi = CommonDefs.DictGetValue(kind2buffDict, typeof(Int32), b.Kind).buffId
                                --取最大的buff剩余时间
                                if time > CommonDefs.DictGetValue(kind2buffDict, typeof(Int32), b.Kind).endTime then
                                    CommonDefs.DictGetValue(kind2buffDict, typeof(Int32), b.Kind).buffId = key
                                    CommonDefs.DictGetValue(kind2buffDict, typeof(Int32), b.Kind).endTime = time
                                end
                            else
                                CommonDefs.DictAdd(kind2buffDict, typeof(Int32), b.Kind, typeof(DaDiZiBuffInfo), (function () 
                                    local __localValue = CreateFromClass(DaDiZiBuffInfo)
                                    __localValue.buffId = key
                                    __localValue.endTime = time
                                    return __localValue
                                end)())
                            end
                        else
                            CommonDefs.ListAdd(info.buffs, typeof(DaDiZiBuffInfo), (function () 
                                local __localValue = CreateFromClass(DaDiZiBuffInfo)
                                __localValue.buffId = key
                                __localValue.endTime = time
                                return __localValue
                            end)())
                        end
                    else
                        CommonDefs.ListAdd(info.debuffs, typeof(DaDiZiBuffInfo), (function () 
                            local __localValue = CreateFromClass(DaDiZiBuffInfo)
                            __localValue.buffId = key
                            __localValue.endTime = time
                            return __localValue
                        end)())
                    end
                end
                i = i + 2
            end
        end

        CommonDefs.DictIterate(kind2buffDict, DelegateFactory.Action_object_object(function (___key, ___value) 
            local pair = {}
            pair.Key = ___key
            pair.Value = ___value
            CommonDefs.ListAdd(info.buffs, typeof(DaDiZiBuffInfo), pair.Value)
        end))
        CommonDefs.ListSort1(info.buffs, typeof(DaDiZiBuffInfo), MakeDelegateFromCSFunction(this.SortFunc, MakeGenericClass(Comparison, DaDiZiBuffInfo), this))
        CommonDefs.ListSort1(info.debuffs, typeof(DaDiZiBuffInfo), MakeDelegateFromCSFunction(this.SortFunc, MakeGenericClass(Comparison, DaDiZiBuffInfo), this))
    end
    return info
end
CDaDiZiMgr.m_SortFunc_CS2LuaHook = function (this, p1, p2) 
    if p1.endTime > p2.endTime then
        return - 1
    elseif p1.endTime < p2.endTime then
        return 1
    else
        return 0
    end
end
CDaDiZiMgr.m_GetStageDesc_CS2LuaHook = function (this, stage) 
    local rtn = ""
    repeat
        local default = stage
        if default == 0 then
            break
        elseif default == 1 then
            rtn = LocalString.GetString("8强赛正在进行")
            break
        elseif default == 2 then
            rtn = LocalString.GetString("4强赛正在进行")
            break
        elseif default == 3 then
            rtn = LocalString.GetString("半决赛正在进行")
            break
        elseif default == 4 then
            rtn = LocalString.GetString("决赛正在进行")
            break
        end
    until 1
    return rtn
end
CDaDiZiMgr.m_GetStageName_CS2LuaHook = function (this, stage) 
    local rtn = ""
    repeat
        local default = stage
        if default == 0 then
            break
        elseif default == 1 then
            rtn = LocalString.GetString("8强赛")
            break
        elseif default == 2 then
            rtn = LocalString.GetString("4强赛")
            break
        elseif default == 3 then
            rtn = LocalString.GetString("半决赛")
            break
        elseif default == 4 then
            rtn = LocalString.GetString("决赛")
            break
        end
    until 1
    return rtn
end
CDaDiZiMgr.m_IsInDaDiZiBattleScene_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.PlayProp.PlayId == CDaDiZiMgr.battleSceneId or CClientMainPlayer.Inst.PlayProp.PlayId == 51100833 then
            return true
        end
    end
    return false
end
CDaDiZiMgr.m_IsInDaDiZiPrepareScene_CS2LuaHook = function (this) 
    if CScene.MainScene == nil then
        return false
    end
    local data = BiWu_Setting.GetData()
    if data ~= nil then
        if CScene.MainScene.SceneTemplateId == CDaDiZiMgr.prepareSceneTemplateId then
            return true
        end
    end
    return false
end

function Gas2Gac.SendShouXiDaDiZiBiWuWatchInfo(player1Basic_U, player1Buff_U, player2Basic_U, player2Buff_U) 
    CDaDiZiMgr.Inst.playerInfo1 = CDaDiZiMgr.Inst:ParsePlayerInfo(CDaDiZiMgr.Inst.playerInfo1, player1Basic_U, player1Buff_U)
    CDaDiZiMgr.Inst.playerInfo2 = CDaDiZiMgr.Inst:ParsePlayerInfo(CDaDiZiMgr.Inst.playerInfo2, player2Basic_U, player2Buff_U)

    EventManager.Broadcast(EnumEventType.SendShouXiDaDiZiBiWuWatchInfo)

    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.Id ~= CDaDiZiMgr.Inst.playerInfo1.playerId and CClientMainPlayer.Inst.Id ~= CDaDiZiMgr.Inst.playerInfo2.playerId then
            if not CUIManager.IsLoaded(CUIResources.DanmuWnd) then
                CUIManager.ShowUI(CUIResources.DanmuWnd)
                g_ScriptEvent:BroadcastInLua("ShowDanmuEntry",true)
            end

            if not CUIManager.IsLoaded(CUIResources.DaDiZiWatchStateWnd) then
                CUIManager.ShowUI(CUIResources.DaDiZiWatchStateWnd)
            end
        else
            CCommonDPSInfoDisplayMgr.Inst:UpdateInfo(EnumDPSInfoDisplayType.DaDiZi, CDaDiZiMgr.Inst.playerInfo1.playerName, math.floor(CDaDiZiMgr.Inst.playerInfo1.DPS), CDaDiZiMgr.Inst.playerInfo2.playerName, math.floor(CDaDiZiMgr.Inst.playerInfo2.DPS))


            if not CUIManager.IsLoaded(CUIResources.DaDiZiStateWnd) then
                CUIManager.ShowUI(CUIResources.DaDiZiStateWnd)
            end
        end
    end
end
Gas2Gac.ReplyShouXiDaDiZiBiWuOverView = function (zhiye, currentStage, NameTbl, stage1Data, stage2Data, stage3Data, stage4Data, stage5Data) 
    CDaDiZiMgr.Inst.cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), zhiye)
    CDaDiZiMgr.Inst.stage = currentStage
    --////////////////////////////////////////////////////////////////////////
    CommonDefs.ListClear(CDaDiZiMgr.Inst.nameList)
    local list = TypeAs(MsgPackImpl.unpack(NameTbl), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < list.Count do
            CommonDefs.ListAdd(CDaDiZiMgr.Inst.nameList, typeof(String), tostring(list[i]))
            i = i + 1
        end
    end
    --////////////////////////////////////////////////////////////////////////
    CommonDefs.ListClear(CDaDiZiMgr.Inst.stage1Data)
    list = TypeAs(MsgPackImpl.unpack(stage1Data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < list.Count do
            CommonDefs.ListAdd(CDaDiZiMgr.Inst.stage1Data, typeof(Double), tonumber(list[i]))
            i = i + 1
        end
    end
    --////////////////////////////////////////////////////////////////////////
    CommonDefs.ListClear(CDaDiZiMgr.Inst.stage2Data)
    list = TypeAs(MsgPackImpl.unpack(stage2Data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < list.Count do
            CommonDefs.ListAdd(CDaDiZiMgr.Inst.stage2Data, typeof(Double), tonumber(list[i]))
            i = i + 1
        end
    end
    --////////////////////////////////////////////////////////////////////////
    CommonDefs.ListClear(CDaDiZiMgr.Inst.stage3Data)
    list = TypeAs(MsgPackImpl.unpack(stage3Data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < list.Count do
            CommonDefs.ListAdd(CDaDiZiMgr.Inst.stage3Data, typeof(Double), tonumber(list[i]))
            i = i + 1
        end
    end
    --////////////////////////////////////////////////////////////////////////
    CommonDefs.ListClear(CDaDiZiMgr.Inst.stage4Data)
    list = TypeAs(MsgPackImpl.unpack(stage4Data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < list.Count do
            CommonDefs.ListAdd(CDaDiZiMgr.Inst.stage4Data, typeof(Double), tonumber(list[i]))
            i = i + 1
        end
    end
    --////////////////////////////////////////////////////////////////////////
    CommonDefs.ListClear(CDaDiZiMgr.Inst.stage5Data)
    list = TypeAs(MsgPackImpl.unpack(stage5Data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < list.Count do
            CommonDefs.ListAdd(CDaDiZiMgr.Inst.stage5Data, typeof(Double), tonumber(list[i]))
            i = i + 1
        end
    end

    EventManager.Broadcast(EnumEventType.ReplyShouXiDaDiZiBiWuOverView)
end
Gas2Gac.ReplyShouXiDaDiZiBiWuStageDetails = function (zhiye, currentStage, data) 
    L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails __________________" .. currentStage)
    local obj = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    local list = CreateFromClass(MakeGenericClass(List, DaDiZiWatchInfo))
    do
        local i = 0
        while i < obj.Count do
            --L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails  " + Convert.ToInt64(obj[i]));
            --L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails  " + Convert.ToInt64(obj[i+1]));
            --L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails  " + Convert.ToString(obj[i+2]));
            --L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails  " + Convert.ToString(obj[i+3]));
            --L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails  " + Convert.ToInt64(obj[i+4]));
            --L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails  " + Convert.ToInt64(obj[i+5]));
            --L10.CLogMgr.Log("ReplyShouXiDaDiZiBiWuStageDetails=====================");
            local playerId1 = math.floor(tonumber(obj[i] or 0))
            local playerId2 = math.floor(tonumber(obj[i + 1] or 0))
            if playerId1 > 0 and playerId2 > 0 then
                local info = CreateFromClass(DaDiZiWatchInfo)
                info.playerId1 = playerId1
                info.playerId2 = playerId2
                info.playerName1 = tostring(obj[i + 2])
                info.playerName2 = tostring(obj[i + 3])
                info.playId = math.floor(tonumber(obj[i + 4] or 0))
                info.winPlayerId = math.floor(tonumber(obj[i + 5] or 0))
                CommonDefs.ListAdd(list, typeof(DaDiZiWatchInfo), info)
            end
            i = i + 6
        end
    end
    EventManager.BroadcastInternalForLua(EnumEventType.ReplyShouXiDaDiZiBiWuStageDetails, {zhiye, currentStage, currentStage, list})
end
