local UITexture = import "UITexture"
local Profession  = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local UILabel = import "UILabel"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CClientMonster = import "L10.Game.CClientMonster"
local Flip = import "UIBasicSprite+Flip"
local Monster_Monster = import "L10.Game.Monster_Monster"

LuaWuLiangCompanionAttributeRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "SkillTable", "SkillTable", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "CompanionBtn", "CompanionBtn", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "ZhiYeSprite", "ZhiYeSprite", UISprite)
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "CompanionLabel", "CompanionLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "AttributeTable", "AttributeTable", GameObject)
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "BtnLabel", "BtnLabel", UILabel)
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "ModelTexture", "ModelTexture", UITexture)
RegistChildComponent(LuaWuLiangCompanionAttributeRoot, "RuleBtn", "RuleBtn", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangCompanionAttributeRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CompanionBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCompanionBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


    --@endregion EventBind end
end

function LuaWuLiangCompanionAttributeRoot:InitShow()
	if LuaWuLiangMgr.huobanData == nil or LuaWuLiangMgr.huobanSelectId == nil or LuaWuLiangMgr.huobanSelectId == 0 then
		return
	end
	
	self.huoBanId = LuaWuLiangMgr.huobanSelectId
    self.huoBanInfo = LuaWuLiangMgr.huobanData.HuoBan[LuaWuLiangMgr.huobanSelectId]

	local monsterData = Monster_Monster.GetData(self.huoBanId)
    local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(self.huoBanId)

	-- 加载模型
	self.m_TextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadHuoBan(ro)
    end)
    self.ModelTexture.mainTexture = CUIManager.CreateModelTexture("__WuLiangCompanionAttributeRoot20220615__", self.m_TextureLoader, 180, 0, -1, 6, false,true,1, true, true)
	self.ModelTexture.flip = Flip.Nothing

	self.ZhiYeSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), huoBanData.Class))
	self.CompanionLabel.text = huoBanData.Name
	
	-- 显示技能信息
    LuaWuLiangMgr:ShowSkill(self.SkillTable, self.huoBanId, self.huoBanInfo.SkillLevelTbl, function(index, skillId)
		local lv = 1
		local addLv = self.huoBanInfo.SkillLevelTbl[skillId]
		if addLv then
			lv = lv + addLv
		end
		CSkillInfoMgr.ShowSkillInfoWnd(skillId*100+lv, true, CClientMainPlayer.Inst.Level, 0, nil)
	end)

	-- 显示属性
	local level = self.huoBanInfo.InheritLevel or 0
	--LuaWuLiangMgr:ShowAttr(self.AttributeTable, level, self.huoBanId)

	if self.queryHuoBanId == nil or self.queryHuoBanId == 0 or self.queryHuoBanId ~= self.huoBanId then
		self.queryHuoBanId = self.huoBanId
		if self.huoBanId ~= 0 then
			Gac2Gas.WuLiangShenJing_QueryHuoBanProp(self.huoBanId, false)
		end
	end
	
	-- 显示按钮
	local hasChuZhan = self:HasChuZhan(self.huoBanId)
	if hasChuZhan then
		self.BtnLabel.text = LocalString.GetString("休息")
	else
		self.BtnLabel.text = LocalString.GetString("出战")
	end
end

function LuaWuLiangCompanionAttributeRoot:HasChuZhan(monsterId)
    for i, id in pairs(LuaWuLiangMgr.huobanData.ChuZhan) do
        if monsterId == id then
            return true
        end
    end
    return false
end

function LuaWuLiangCompanionAttributeRoot:LoadHuoBan(ro)
	local monsterData = Monster_Monster.GetData(self.huoBanId)
	local path = CClientMonster.GetMonsterPrefabPath(monsterData)
	if string.find(path, "Special") == nil then
		ro:LoadMain(path, nil, false, false, false)
	else
		ro:LoadMain(path, nil, false, true, false)
	end

	if self.huoBanId == 18007720 then
		ro:DoAni("stand01_02", true, 0, 1, 0, true, 1)
	end
end

function LuaWuLiangCompanionAttributeRoot:OnShowAttr(huobanId, data)
	if huobanId == self.huoBanId then
		LuaWuLiangMgr:ShowAttrNew(self.AttributeTable,huobanId, data)
	end
end

function LuaWuLiangCompanionAttributeRoot:OnEnable()
	self.huoBanId = 0
	self.queryHuoBanId = 0
	self:InitShow()
    g_ScriptEvent:AddListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitShow")
    g_ScriptEvent:AddListener("WuLiangShenJing_QueryHuoBanPropResult", self, "OnShowAttr")
    g_ScriptEvent:AddListener("WuLiangShenJing_SetChuZhan", self, "InitShow")
end

function LuaWuLiangCompanionAttributeRoot:OnDisable()
	CUIManager.DestroyModelTexture("__WuLiangCompanionAttributeRoot20220615__")
    g_ScriptEvent:RemoveListener("LuaWuLiangCompanionInfoWnd_SelectIndex", self, "InitShow")
    g_ScriptEvent:RemoveListener("WuLiangShenJing_QueryHuoBanPropResult", self, "OnShowAttr")
    g_ScriptEvent:RemoveListener("WuLiangShenJing_SetChuZhan", self, "InitShow")
end

--@region UIEvent

function LuaWuLiangCompanionAttributeRoot:OnCompanionBtnClick()
	local hasChuZhan = self:HasChuZhan(self.huoBanId)
	local list = {}

	for i, id in pairs(LuaWuLiangMgr.huobanData.ChuZhan) do
        if id ~= self.huoBanId then
			table.insert(list, id)
		end
    end

	if not hasChuZhan then
		table.insert(list, self.huoBanId)
	end

	if #list > 2 then
		g_MessageMgr:ShowMessage("WuLiang_HuoBan_Count_Limited")
	else
		Gac2Gas.WuLiangShenJing_SetChuZhan(g_MessagePack.pack(list))
	end
end

function LuaWuLiangCompanionAttributeRoot:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("WuLiang_HuoBan_Tip")
end


--@endregion UIEvent

