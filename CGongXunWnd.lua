-- Auto Generated!!
local AntiExchange_Setting = import "L10.Game.AntiExchange_Setting"
local CButton = import "L10.UI.CButton"
local CGongXunMgr = import "L10.Game.CGongXunMgr"
local CGongXunWnd = import "L10.UI.CGongXunWnd"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local DataObject = import "L10.UI.CGongXunWnd+DataObject"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local PlayerSettings = import "L10.Game.PlayerSettings"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGongXunWnd.m_ClosePanel_CS2LuaHook = function (this) 
    local totalDeletePoint = 0
    local deleteDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 then
            CommonDefs.DictSet(deleteDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.deleteLevel)
            totalDeletePoint = totalDeletePoint + dataObject.data.deleteLevel
        end
    end))

    if totalDeletePoint > 0 then
        local returnRatio = System.Int32.Parse(AntiExchange_Setting.GetData("ReturnRatio").Value)

        MessageWndManager.ShowOKCancelMessage(System.String.Format(CGongXunWnd.DeleteStringTip, totalDeletePoint, returnRatio * totalDeletePoint), DelegateFactory.Action(function () 
            Gac2Gas.RequestSubAntiPointBegin()

            CommonDefs.DictIterate(deleteDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
                local data = {}
                data.Key = ___key
                data.Value = ___value
                local deleteNum = data.Value
                if deleteNum > 0 then
                    Gac2Gas.RequestSubAntiPoint(data.Key, deleteNum)
                end
            end))

            Gac2Gas.RequestSubAntiPointEnd()
            this:Close()
        end), DelegateFactory.Action(function () 
            this:Close()
        end), nil, nil, false)
    else
        this:Close()
    end
end
CGongXunWnd.m_SaveDeletePoint_CS2LuaHook = function (this) 
    local totalDeletePoint = 0
    local deleteDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 then
            CommonDefs.DictSet(deleteDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.deleteLevel)
            totalDeletePoint = totalDeletePoint + dataObject.data.deleteLevel
        end
    end))

    if totalDeletePoint > 0 then
        local returnRatio = System.Int32.Parse(AntiExchange_Setting.GetData("ReturnRatio").Value)
        MessageWndManager.ShowOKCancelMessage(System.String.Format(CGongXunWnd.DeleteStringTip, totalDeletePoint, returnRatio * totalDeletePoint), DelegateFactory.Action(function () 
            Gac2Gas.RequestSubAntiPointBegin()

            CommonDefs.DictIterate(deleteDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
                local data = {}
                data.Key = ___key
                data.Value = ___value
                local deleteNum = data.Value
                if deleteNum > 0 then
                    Gac2Gas.RequestSubAntiPoint(data.Key, deleteNum)
                end
            end))

            Gac2Gas.RequestSubAntiPointEnd()
        end), nil, nil, nil, false)
    end
end
CGongXunWnd.m_ResetAddPoint_CS2LuaHook = function (this) 
    local addDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.addLevel > 0 then
            CommonDefs.DictSet(addDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.addLevel)
        end
    end))

    CommonDefs.DictIterate(addDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local addNum = data.Value
        if addNum > 0 then
            local _data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).data
            if _data.addLevel > 0 then
                _data.addLevel = 0
                local _dataObject = CreateFromClass(DataObject)
                _dataObject.data = _data
                _dataObject.node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).node
                this:SetNodeFunc(_dataObject)
            end
        end
    end))
end
CGongXunWnd.m_ResetDeletePoint_CS2LuaHook = function (this, go) 
    CommonDefs.GetComponent_GameObject_Type(this.saveDeletePointBtn, typeof(CButton)).Enabled = false
    CommonDefs.GetComponent_GameObject_Type(this.resetDeletePointBtn, typeof(CButton)).Enabled = false
    UIEventListener.Get(this.saveDeletePointBtn).onClick = nil
    UIEventListener.Get(this.resetDeletePointBtn).onClick = nil

    local deleteDataDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 then
            CommonDefs.DictSet(deleteDataDic, typeof(UInt32), dataObject.data.skillCls, typeof(Int32), dataObject.data.deleteLevel)
        end
    end))

    CommonDefs.DictIterate(deleteDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local deleteNum = data.Value
        if deleteNum > 0 then
            local _data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).data
            if _data.deleteLevel > 0 then
                _data.deleteLevel = 0
                local _dataObject = CreateFromClass(DataObject)
                _dataObject.data = _data
                _dataObject.node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), data.Key).node
                this:SetNodeFunc(_dataObject)
            end
        end
    end))
end
CGongXunWnd.m_SetDeletePointBtn_CS2LuaHook = function (this) 
    CommonDefs.GetComponent_GameObject_Type(this.saveDeletePointBtn, typeof(CButton)).Enabled = false
    CommonDefs.GetComponent_GameObject_Type(this.resetDeletePointBtn, typeof(CButton)).Enabled = false
    UIEventListener.Get(this.saveDeletePointBtn).onClick = nil
    UIEventListener.Get(this.resetDeletePointBtn).onClick = nil

    CommonDefs.DictIterate(this.saveDataDic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local dataObject = data.Value
        if dataObject.data.deleteLevel > 0 then
            CommonDefs.GetComponent_GameObject_Type(this.saveDeletePointBtn, typeof(CButton)).Enabled = true
            CommonDefs.GetComponent_GameObject_Type(this.resetDeletePointBtn, typeof(CButton)).Enabled = true
            UIEventListener.Get(this.saveDeletePointBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                this:SaveDeletePoint()
            end)
            UIEventListener.Get(this.resetDeletePointBtn).onClick = MakeDelegateFromCSFunction(this.ResetDeletePoint, VoidDelegate, this)
            return
        end
    end))
end
CGongXunWnd.m_SubPointClick_CS2LuaHook = function (this, dataObject) 
    local data = dataObject.data
    if data.deleteLevel < data.nowLevel then
        local default = data
        default.deleteLevel = default.deleteLevel + 1
    else
        return
    end

    local _dataObject = CreateFromClass(DataObject)
    _dataObject.data = data
    _dataObject.node = dataObject.node
    this:SetNodeFunc(_dataObject)
    this:SetDeletePointBtn()
end
CGongXunWnd.m_AddPointClick_CS2LuaHook = function (this, dataObject) 
    local data = dataObject.data
    if data.deleteLevel > 0 then
        local default = data
        default.deleteLevel = default.deleteLevel - 1
    else
        return
    end

    local _dataObject = CreateFromClass(DataObject)
    _dataObject.data = data
    _dataObject.node = dataObject.node
    this:SetNodeFunc(_dataObject)
    this:SetDeletePointBtn()
end
CGongXunWnd.m_SetNodeFunc_CS2LuaHook = function (this, dataObject) 
    local data = dataObject.data
    CommonDefs.DictSet(this.saveDataDic, typeof(UInt32), data.skillCls, typeof(DataObject), dataObject)

    local node = dataObject.node
    local subbtn = node.transform:Find("subbtn").gameObject
    local addbtn = node.transform:Find("addbtn").gameObject
    local nameLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel))
    local numLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel))
    local changeLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("changenum"), typeof(UILabel))
    local fullLabelNode = node.transform:Find("fullstring").gameObject


    local showLevel = data.nowLevel - data.deleteLevel

    CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel)).text = tostring(showLevel)

    if data.nowLevel == 0 and data.deleteLevel == 0 and data.addLevel == 0 then
        subbtn:SetActive(false)
        addbtn:SetActive(false)
        changeLabel.gameObject:SetActive(false)
    else
        subbtn:SetActive(true)
        addbtn:SetActive(true)
        changeLabel.gameObject:SetActive(true)
    end

    if data.deleteLevel > 0 then
        CommonDefs.GetComponent_GameObject_Type(addbtn, typeof(CButton)).Enabled = true
        UIEventListener.Get(addbtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:AddPointClick(dataObject)
        end)
    else
        CommonDefs.GetComponent_GameObject_Type(addbtn, typeof(CButton)).Enabled = false
        UIEventListener.Get(addbtn).onClick = nil
    end

    if data.nowLevel <= 0 or data.nowLevel == data.deleteLevel then
        CommonDefs.GetComponent_GameObject_Type(subbtn, typeof(CButton)).Enabled = false
        UIEventListener.Get(subbtn).onClick = nil
    else
        CommonDefs.GetComponent_GameObject_Type(subbtn, typeof(CButton)).Enabled = true
        UIEventListener.Get(subbtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:SubPointClick(dataObject)
        end)
    end

    local changeNumText = ""
    if data.addLevel > 0 then
        changeNumText = changeNumText .. (("[c][00FF00](+" .. data.addLevel) .. ")[-][/c]")
    end
    if data.deleteLevel > 0 then
        changeNumText = changeNumText .. (("[c][FF0000](-" .. data.deleteLevel) .. ")[-][/c]")
    end
    if data.addLevel <= 0 and data.deleteLevel <= 0 then
        changeNumText = "0"
    end

    if showLevel >= data.maxLevel then
        fullLabelNode:SetActive(true)
    else
        fullLabelNode:SetActive(false)
    end

    CommonDefs.GetComponent_Component_Type(node.transform:Find("changenum"), typeof(UILabel)).text = changeNumText
end
CGongXunWnd.m_SetDeletePointBack_CS2LuaHook = function (this, list) 
    do
        local i = 0
        while i < list.Count - 1 do
            local skillCls = list[i]
            local deleteLevel = list[i + 1]

            local data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).data
            local node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).node
            data.nowLevel = data.nowLevel - deleteLevel
            data.deleteLevel = 0
            data.addLevel = 0

            local dataObject = CreateFromClass(DataObject)
            dataObject.data = data
            dataObject.node = node

            this:SetNodeFunc(dataObject)
            i = i + 2
        end
    end

    this:SetDeletePointBtn()
end
CGongXunWnd.m_SetDistributePointBack_CS2LuaHook = function (this, list, remain) 
    this:SetUnDistributePoint(remain)
    this:ResetAddPoint()

    do
        local i = 0
        while i < list.Count - 1 do
            local skillCls = list[i]
            local addLevel = list[i + 1]
            if CommonDefs.DictContains(this.saveDataDic, typeof(UInt32), skillCls) then
                local data = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).data
                local node = CommonDefs.DictGetValue(this.saveDataDic, typeof(UInt32), skillCls).node
                data.nowLevel = data.nowLevel + addLevel
                data.addLevel = addLevel

                local dataObject = CreateFromClass(DataObject)
                dataObject.data = data
                dataObject.node = node

                this:SetNodeFunc(dataObject)
            end
            i = i + 2
        end
    end

    this:SetDeletePointBtn()
end
CGongXunWnd.m_SetSingleNodeData_CS2LuaHook = function (this, node, data) 
    if not node.activeSelf then
        node:SetActive(true)
    end
    local subbtn = node.transform:Find("subbtn").gameObject
    local nameLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel))
    local numLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("num"), typeof(UILabel))
    local changeLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("changenum"), typeof(UILabel))

    nameLabel.text = data.name

    local dataObject = CreateFromClass(DataObject)
    dataObject.data = data
    dataObject.node = node

    this:SetNodeFunc(dataObject)
end
CGongXunWnd.m_InitList_CS2LuaHook = function (this) 
    CommonDefs.DictClear(this.saveDataDic)
    Extensions.RemoveAllChildren(this.listTable.transform)
    local count = CGongXunMgr.Inst.showDataList.Count
    local nowType = ""
    local nowNode = nil
    if count > 0 then
        do
            local i = 0
            while i < count do
                local data = CGongXunMgr.Inst.showDataList[i]
                if not (nowType == data.type) then
                    local barNode = NGUITools.AddChild(this.listTable.gameObject, this.BarItemTemplate)
                    barNode:SetActive(true)
                    CommonDefs.GetComponent_Component_Type(barNode.transform:Find("Label"), typeof(UILabel)).text = data.type
                    nowType = data.type
                    nowNode = nil
                end

                if nowNode == nil then
                    local node = NGUITools.AddChild(this.listTable.gameObject, this.InfoItemTemplate)
                    node:SetActive(true)
                    node.transform:Find("info_r").gameObject:SetActive(false)
                    this:SetSingleNodeData(node.transform:Find("info_l").gameObject, data)
                    nowNode = node
                else
                    this:SetSingleNodeData(nowNode.transform:Find("info_r").gameObject, data)
                    nowNode = nil
                end
                i = i + 1
            end
        end

        this.listTable:Reposition()
        this.listScrollView:ResetPosition()
    end
end
CGongXunWnd.m_UsePoint_CS2LuaHook = function (this, go) 
    if PlayerSettings.UseAntiPointAlert <= 0 then
        MessageWndManager.ShowMessageWithCheckBox(CGongXunWnd.UsePointTipText, CGongXunWnd.UsePointClickText, false, DelegateFactory.Action_bool(function (sign) 
            if sign then
                PlayerSettings.UseAntiPointAlert = 1
            end

            Gac2Gas.RequestDistributeAntiPoint()
        end), DelegateFactory.Action_bool(function (sign) 
            if sign then
                PlayerSettings.UseAntiPointAlert = 1
            end
        end), nil, nil)
    else
        Gac2Gas.RequestDistributeAntiPoint()
    end
end
CGongXunWnd.m_SetUnDistributePoint_CS2LuaHook = function (this, point) 
    this.unDistributePointLabel.text = tostring(point)
    if point <= 0 then
        this.unDistributePointLabel.color = Color.red
        CommonDefs.GetComponent_GameObject_Type(this.usePointBtn, typeof(CButton)).Enabled = false
        UIEventListener.Get(this.usePointBtn).onClick = nil
    else
        this.unDistributePointLabel.color = Color.green
        CommonDefs.GetComponent_GameObject_Type(this.usePointBtn, typeof(CButton)).Enabled = true
        UIEventListener.Get(this.usePointBtn).onClick = MakeDelegateFromCSFunction(this.UsePoint, VoidDelegate, this)
    end
end
CGongXunWnd.m_InitExchangePointPanel_CS2LuaHook = function (this) 
    this.exchangePointBtn.transform:Find("RedPoint").gameObject:SetActive(false)
    this.ExchangePointPanel:SetActive(true)

    local cancelBtn = this.ExchangePointPanel.transform:Find("CancelButton").gameObject
    local submitBtn = this.ExchangePointPanel.transform:Find("SubmitButton").gameObject
    local gongxunNumLabel = CommonDefs.GetComponent_Component_Type(this.ExchangePointPanel.transform:Find("gongxunnum"), typeof(UILabel))
    local kangxinNumLabel = CommonDefs.GetComponent_Component_Type(this.ExchangePointPanel.transform:Find("kangxinnum"), typeof(UILabel))
    local numInput = CommonDefs.GetComponent_Component_Type(this.ExchangePointPanel.transform:Find("BuyArea/Label/QnIncreseAndDecreaseButton"), typeof(QnAddSubAndInputButton))
    numInput:SetValue(0, true)
    numInput:SetMinMax(0, CGongXunMgr.Inst.maxExchangePoint, 1)

    gongxunNumLabel.text = tostring(CGongXunMgr.Inst.totalGongXun)
    kangxinNumLabel.text = tostring(CGongXunMgr.Inst.maxExchangePoint)
    if CGongXunMgr.Inst.maxExchangePoint <= 0 then
        kangxinNumLabel.color = Color.red
    else
        kangxinNumLabel.color = Color.green
    end

    UIEventListener.Get(cancelBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this.ExchangePointPanel:SetActive(false)
    end)

    UIEventListener.Get(submitBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        Gac2Gas.RequestExchangeAnti(numInput:GetValue())
    end)
end
CGongXunWnd.m_Init_CS2LuaHook = function (this) 
    this.BarItemTemplate:SetActive(false)
    this.InfoItemTemplate:SetActive(false)
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:ClosePanel()
    end)

    this:InitList()

    this:SetUnDistributePoint(CGongXunMgr.Inst.unDistributeAnti)
    if CGongXunMgr.Inst.restExchangeNum > 0 then
        this.exchangePointBtn.transform:Find("RedPoint").gameObject:SetActive(true)
    else
        this.exchangePointBtn.transform:Find("RedPoint").gameObject:SetActive(false)
    end

    this:SetDeletePointBtn()

    UIEventListener.Get(this.exchangePointBtn).onClick = MakeDelegateFromCSFunction(this.RequestOpenExchangePointPanel, VoidDelegate, this)
    UIEventListener.Get(this.tipBtn).onClick = MakeDelegateFromCSFunction(this.ShowTips, VoidDelegate, this)
end
