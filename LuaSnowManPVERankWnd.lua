local QnTableView = import "L10.UI.QnTableView"

local UISimpleTableView = import "L10.UI.UISimpleTableView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UILabel = import "UILabel"
local Profession = import "L10.Game.Profession"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"

LuaSnowManPVERankWnd = class()
LuaSnowManPVERankWnd.selectMonsterType = 1
LuaSnowManPVERankWnd.selectProfessionType = 1

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowManPVERankWnd, "RowCellTemplate", "RowCellTemplate", GameObject)
RegistChildComponent(LuaSnowManPVERankWnd, "SnowManGrid", "SnowManGrid", UITabBar)
RegistChildComponent(LuaSnowManPVERankWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaSnowManPVERankWnd, "TabScollView", "TabScollView", UITabBar)
RegistChildComponent(LuaSnowManPVERankWnd, "Tip", "Tip", UILabel)
RegistChildComponent(LuaSnowManPVERankWnd, "TableBody", "TableBody", QnTableView)
RegistChildComponent(LuaSnowManPVERankWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaSnowManPVERankWnd, "myRankItem", "myRankItem", GameObject)

--@endregion RegistChildComponent end

function LuaSnowManPVERankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.Tip.text = g_MessageMgr:FormatMessage("HanJia2023_SnowManPVE_Rank_Tip")

    self:InitSnowManTab()
    self:InitProfessionTab()
end

function LuaSnowManPVERankWnd:Init()
    --请求打开页面的初始rank数据

    self.TableBody.m_DataSource = DefaultTableViewDataSource.Create(
            function()
                return #self.m_Data
            end,
            function(item, index)
                item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
                self:InitItem(item, index+1, self.m_Data[index+1])
            end
    )

    self.TableBody.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_Data[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

    self:QueryRank(LuaSnowManPVERankWnd.selectMonsterType, LuaSnowManPVERankWnd.selectProfessionType)
end

function LuaSnowManPVERankWnd:QueryRank(selectMonsterType, selectProfessionType)
    local seasonId = LuaHanJia2023Mgr:GetSeason()
    Gac2Gas.SeasonRank_QueryRank(3000+ 100*selectMonsterType + selectProfessionType, seasonId)
end

function LuaSnowManPVERankWnd:InitSnowManTab()
    HanJia2023_BingTianShiLian.Foreach(function(id, data)
        local itemGO = CommonDefs.Object_Instantiate(self.RowCellTemplate)
        itemGO.transform.parent = self.SnowManGrid.transform
        itemGO.transform.localScale = Vector3.one
        itemGO.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.Name
        itemGO:SetActive(true)
        if id == LuaSnowManPVERankWnd.selectMonsterType then
            itemGO:GetComponent(typeof(CButton)).Selected = true
        end
    end)

    self.SnowManGrid.OnTabChange = DelegateFactory.Action_GameObject_int(function (_, index)
        local clickMonsterType = index+1
        if clickMonsterType ~= LuaSnowManPVERankWnd.selectMonsterType then
            LuaSnowManPVERankWnd.selectMonsterType = clickMonsterType
            self:QueryRank(LuaSnowManPVERankWnd.selectMonsterType, LuaSnowManPVERankWnd.selectProfessionType)
        end
    end)
end

function LuaSnowManPVERankWnd:InitProfessionTab()
    --先创建自己的职业, 然后再依次创建剩余其他职业
    self.classList = {}
    local myProfession = 0
    if CClientMainPlayer.Inst then
        myProfession = EnumToInt(CClientMainPlayer.Inst.Class)
    end
    table.insert(self.classList, myProfession)
    for i = 1, 12 do
        if i ~= myProfession then
            table.insert(self.classList, i)
        end
    end

    for i = 1, #self.classList do
        local itemGO = CommonDefs.Object_Instantiate(self.TabTemplate)
        itemGO.transform.parent = self.TabScollView.transform
        itemGO.transform.localScale = Vector3.one
        local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.classList[i])
        local proName = Profession.GetFullName(proClass)
        itemGO.transform:Find("Label"):GetComponent(typeof(UILabel)).text = proName
        itemGO:SetActive(true)

        if self.classList[i] == LuaSnowManPVERankWnd.selectProfessionType then
            itemGO:GetComponent(typeof(CButton)).Selected = true
        end
    end
    self.TabScollView.OnTabChange = DelegateFactory.Action_GameObject_int(function (_, index)
        local clickProfessionType = self.classList[index+1]
        if clickProfessionType ~= LuaSnowManPVERankWnd.selectProfessionType then
            LuaSnowManPVERankWnd.selectProfessionType = clickProfessionType
            self:QueryRank(LuaSnowManPVERankWnd.selectMonsterType, LuaSnowManPVERankWnd.selectProfessionType)
        end
    end)
end

--@region UIEvent

--@endregion UIEvent

function LuaSnowManPVERankWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
end

function LuaSnowManPVERankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
end

function LuaSnowManPVERankWnd:OnSeasonRank_QueryRankResult(rankType, seasonId, selfData, rankData)
    local showRankNum = 50
    
    self.m_Data = {}
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local myrank = 0
    local selectSeasonId = LuaHanJia2023Mgr:GetSeason()
    local selectRankType = 3000 + 100*LuaSnowManPVERankWnd.selectMonsterType + LuaSnowManPVERankWnd.selectProfessionType
    if rankType == selectRankType and seasonId == selectSeasonId then
        local rawData = MsgPackImpl.unpack(selfData)
        local myRankData = {}
        if CommonDefs.IsDic(rawData) then
            local t = {}
            CommonDefs.DictIterate(rawData, DelegateFactory.Action_object_object(function (___key, ___value)
                t[tostring(___key)] = ___value
            end))
            myRankData = t
        end

        local rawData2 = MsgPackImpl.unpack(rankData)
        if rawData2.Count > 0 then
            for i = 1, math.min(rawData2.Count, showRankNum) do
                local rawData3 = rawData2[i-1]
                local t = {}
                if CommonDefs.IsDic(rawData3) then
                    CommonDefs.DictIterate(rawData3, DelegateFactory.Action_object_object(function (___key, ___value)
                        t[tostring(___key)] = ___value
                    end))
                end
                if t.playerId and t.playerId == myId then
                    myrank = i
                end
                table.insert(self.m_Data, t)
            end
        end
        self:InitItem(self.myRankItem, myrank, myRankData, true)
        self.TableBody:ReloadData(true,false)
    end
end

function LuaSnowManPVERankWnd:InitItem(item, rank, info, isMyRankItem)
    local tf = item.transform:Find("Table")
    local rankLabel = tf:Find("rank"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("rank/rankImage"):GetComponent(typeof(UISprite))
    local nameLabel = tf:Find("name"):GetComponent(typeof(UILabel))
    local levelLabel = tf:Find("level"):GetComponent(typeof(UILabel))
    local difficultyLabel = tf:Find("difficulty"):GetComponent(typeof(UILabel))
    if isMyRankItem == nil then
        isMyRankItem = false
    end

    if isMyRankItem then
        local myProfession = 0
        local myName = ""
        local HasFeiSheng = false
        local XianShenLevel = 0
        local FanShenLevel = 0
        if CClientMainPlayer.Inst then
            myProfession = EnumToInt(CClientMainPlayer.Inst.Class)
            myName = CClientMainPlayer.Inst.Name

            HasFeiSheng = CClientMainPlayer.Inst.HasFeiSheng
            XianShenLevel = HasFeiSheng and CClientMainPlayer.Inst.XianShenLevel or 0
            FanShenLevel = HasFeiSheng and CClientMainPlayer.Inst.PlayProp.FeiShengData.FeiShengLevel or CClientMainPlayer.Inst.Level
        end
        if LuaSnowManPVERankWnd.selectProfessionType == myProfession then
            --是玩家对应的职业
            if info and info.name == nil then
                --从来没有挑战过
                rankLabel.text = LocalString.GetString("未上榜")
                rankSprite.spriteName = ""
                nameLabel.text = myName
                difficultyLabel.text = 0
                --levelLabel.text = ""
                if HasFeiSheng then
                    levelLabel.text = SafeStringFormat3("[c][%s][%s]%d[-]/%d[-][/c]", NGUIText.EncodeColor24(levelLabel.color), L10.Game.Constants.ColorOfFeiSheng, XianShenLevel, FanShenLevel)
                else
                    levelLabel.text = tostring(FanShenLevel)
                end
            else
                --挑战过有数据了
                rankLabel.text = ""
                rankSprite.spriteName = ""
                if rank == 1 then
                    rankSprite.spriteName = "Rank_No.1"
                elseif rank == 2 then
                    rankSprite.spriteName = "Rank_No.2png"
                elseif rank == 3 then
                    rankSprite.spriteName = "Rank_No.3png"
                else
                    rankLabel.text = rank > 0 and tostring(rank) or LocalString.GetString("未上榜")
                end

                nameLabel.text = info.name
                difficultyLabel.text = info.Score
                if info.HasFeiSheng then
                    levelLabel.text = SafeStringFormat3("[c][%s][%s]%d[-]/%d[-][/c]", NGUIText.EncodeColor24(levelLabel.color), L10.Game.Constants.ColorOfFeiSheng, info.XianShenLevel, info.FanShenLevel)
                else
                    levelLabel.text = tostring(info.FanShenLevel)
                end
            end
        else
            --不是玩家对应的职业
            rankLabel.text = ""
            rankSprite.spriteName = ""
            nameLabel.text = myName
            difficultyLabel.text = ""
            if HasFeiSheng then
                levelLabel.text = SafeStringFormat3("[c][%s][%s]%d[-]/%d[-][/c]", NGUIText.EncodeColor24(levelLabel.color), L10.Game.Constants.ColorOfFeiSheng, XianShenLevel, FanShenLevel)
            else
                levelLabel.text = tostring(FanShenLevel)
            end
        end
        item:SetActive(true)
    else
        rankLabel.text = ""
        rankSprite.spriteName = ""
        if rank == 1 then
            rankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            rankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            rankSprite.spriteName = "Rank_No.3png"
        else
            rankLabel.text = rank > 0 and tostring(rank) or LocalString.GetString("未上榜")
        end

        nameLabel.text = info.name
        difficultyLabel.text = info.Score
        if info.HasFeiSheng then
            levelLabel.text = SafeStringFormat3("[c][%s][%s]%d[-]/%d[-][/c]", NGUIText.EncodeColor24(levelLabel.color), L10.Game.Constants.ColorOfFeiSheng, info.XianShenLevel, info.FanShenLevel)
        else
            levelLabel.text = tostring(info.FanShenLevel)
        end
    end
end
