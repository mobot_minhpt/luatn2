require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CButton = import "L10.UI.CButton"
local MessageWndManager = import "L10.UI.MessageWndManager"
local GameObject = import "UnityEngine.GameObject"
local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"
local BabyMgr = import "L10.Game.BabyMgr"
local LocalString = import "LocalString"
local Expression_BabyShow = import "L10.Game.Expression_BabyShow"

LuaBabyExpressionUnlockWnd=class()
RegistChildComponent(LuaBabyExpressionUnlockWnd,"closeBtn", GameObject)
RegistChildComponent(LuaBabyExpressionUnlockWnd,"oneNode", GameObject)
RegistChildComponent(LuaBabyExpressionUnlockWnd,"twoNode", GameObject)
RegistChildComponent(LuaBabyExpressionUnlockWnd,"unlockBtn", GameObject)

function LuaBabyExpressionUnlockWnd:InitBabyShow(babyInfo,nameText,babyTexture)
  nameText.text = babyInfo.Name
  local babyAppear = BabyMgr.Inst:ToBabyAppearance(babyInfo)
  -- 将外观设置为真实的阶段
  babyAppear.Status = babyInfo.Status
  babyTexture:Init(babyAppear, -180, LuaBabyMgr.m_NowShowExpressionId, 0)
end

function LuaBabyExpressionUnlockWnd:CheckExpressionValid(babyIndex,expressionId)
  local babyInfo = BabyMgr.Inst:GetBabyById(babyIndex)
  local expressionInfo = Expression_BabyShow.GetData(expressionId)
  if expressionInfo then
    if expressionInfo.BabyStatus > babyInfo.Status then
      return false
    end
  end
  return true
end

function LuaBabyExpressionUnlockWnd:CheckBabyAlUnlock(babyIndex,expressionId)
  local babyInfo = BabyMgr.Inst:GetBabyById(babyIndex)
  local expressionInfo = Expression_BabyShow.GetData(expressionId)
  if expressionInfo then
    if expressionInfo.LockGroup == 0 then
      return false
    end

    if babyInfo.Props.UnLockExpression[expressionInfo.LockGroup] and babyInfo.Props.UnLockExpression[expressionInfo.LockGroup] > 0 then
      return false
    end
  end
  return true
end

function LuaBabyExpressionUnlockWnd:UnLockExpressionClick(babyIndex)
  local checkUnlock = self:CheckBabyAlUnlock(babyIndex,LuaBabyMgr.m_NowShowExpressionId)
  if not checkUnlock then
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该宝宝已解锁此动作!"))
    return
  end

  local check = self:CheckExpressionValid(babyIndex,LuaBabyMgr.m_NowShowExpressionId)
  if check then
    Gac2Gas.RequestActiveBabyExpression(babyIndex,LuaBabyMgr.m_NowShowExpressionId)
    CUIManager.CloseUI(CLuaUIResources.BabyExpressionUnlockWnd)
  else
    local expressionInfo = Expression_BabyShow.GetData(LuaBabyMgr.m_NowShowExpressionId)
    if expressionInfo then
      local statusName = {LocalString.GetString("婴儿"),LocalString.GetString("幼儿"),LocalString.GetString("少年")}
      MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("BABY_EXPRESSUNLOCK_NOTREACHSTATUS", expressionInfo.ExpName, statusName[expressionInfo.BabyStatus]), DelegateFactory.Action(function ()
        Gac2Gas.RequestActiveBabyExpression(babyIndex,LuaBabyMgr.m_NowShowExpressionId)
        CUIManager.CloseUI(CLuaUIResources.BabyExpressionUnlockWnd)
      end), nil, nil, nil, false)
    end
  end
end

function LuaBabyExpressionUnlockWnd:InitShowNode(babyIdList)
  if #babyIdList == 1 then
    self.oneNode:SetActive(true)
    self.twoNode:SetActive(false)
    local babyInfo = BabyMgr.Inst:GetBabyById(babyIdList[1])
    local nameText = self.oneNode.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local babyTexture = self.oneNode.transform:Find("babyTexture"):GetComponent(typeof(CBabyModelTextureLoader))
    self:InitBabyShow(babyInfo,nameText,babyTexture)

    local unlockBtnScript = self.unlockBtn:GetComponent(typeof(CButton))
    if unlockBtnScript then
      unlockBtnScript.Enabled = true
    end
    local babyIndex = babyIdList[1]
    local onSubmitClick = function(go)
      self:UnLockExpressionClick(babyIndex)
  	end
    CommonDefs.AddOnClickListener(self.unlockBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
  elseif #babyIdList >= 2 then
    self.oneNode:SetActive(false)
    self.twoNode:SetActive(true)
    local babyInfo = BabyMgr.Inst:GetBabyById(babyIdList[1])
    local nameText = self.twoNode.transform:Find("left/NameLabel"):GetComponent(typeof(UILabel))
    local babyTexture = self.twoNode.transform:Find("left/babyTexture"):GetComponent(typeof(CBabyModelTextureLoader))
    self:InitBabyShow(babyInfo,nameText,babyTexture)
    local babyInfo2 = BabyMgr.Inst:GetBabyById(babyIdList[2])
    local nameText2 = self.twoNode.transform:Find("right/NameLabel"):GetComponent(typeof(UILabel))
    local babyTexture2 = self.twoNode.transform:Find("right/babyTexture"):GetComponent(typeof(CBabyModelTextureLoader))
    self:InitBabyShow(babyInfo2,nameText2,babyTexture2)
    local unlockBtnScript = self.unlockBtn:GetComponent(typeof(CButton))
    if unlockBtnScript then
      unlockBtnScript.Enabled = false
    end

    local babyIndex = 0
    local btn1 = self.twoNode.transform:Find("left").gameObject
    local btn2 = self.twoNode.transform:Find("right").gameObject
    local btn1Light = self.twoNode.transform:Find("left/light").gameObject
    local btn2Light = self.twoNode.transform:Find("right/light").gameObject
    btn1Light:SetActive(false)
    btn2Light:SetActive(false)
    local btn1Click = function(go)
      babyIndex = babyIdList[1]
      if unlockBtnScript then
        unlockBtnScript.Enabled = true
      end
      btn1Light:SetActive(true)
      btn2Light:SetActive(false)
    end
    local btn2Click = function(go)
      babyIndex = babyIdList[2]
      if unlockBtnScript then
        unlockBtnScript.Enabled = true
      end
      btn1Light:SetActive(false)
      btn2Light:SetActive(true)
    end
    CommonDefs.AddOnClickListener(btn1,DelegateFactory.Action_GameObject(btn1Click),false)
    CommonDefs.AddOnClickListener(btn2,DelegateFactory.Action_GameObject(btn2Click),false)

    local onSubmitClick = function(go)
      self:UnLockExpressionClick(babyIndex)
  	end
    CommonDefs.AddOnClickListener(self.unlockBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
  end
end

function LuaBabyExpressionUnlockWnd:Init()
	local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.BabyExpressionUnlockWnd)
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  local babyIdList = BabyMgr.Inst:GetBabyIdList()
  local babyTable = {}
  if not babyIdList or babyIdList.Count == 0 then
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("没有可以解锁此表情的宝宝"))
    CUIManager.CloseUI(CLuaUIResources.BabyExpressionUnlockWnd)
    return
  end

  for i=0,babyIdList.Count-1 do
    local babyIndex = babyIdList[i]
    local checkUnlock = self:CheckBabyAlUnlock(babyIndex,LuaBabyMgr.m_NowShowExpressionId)
    local checkValid = self:CheckExpressionValid(babyIndex,LuaBabyMgr.m_NowShowExpressionId)
    if checkUnlock and checkValid then
      table.insert(babyTable,babyIndex)
    end
  end

  if #babyTable == 0 then
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("没有可以解锁此表情的宝宝"))
    CUIManager.CloseUI(CLuaUIResources.BabyExpressionUnlockWnd)
    return
  end

  self.oneNode:SetActive(false)
  self.twoNode:SetActive(false)

  local unlockBtnScript = self.unlockBtn:GetComponent(typeof(CButton))
  if unlockBtnScript then
    unlockBtnScript.Enabled = false
  end



  self:InitShowNode(babyTable)
end

function LuaBabyExpressionUnlockWnd:OnDestroy()

end

function LuaBabyExpressionUnlockWnd:OnBabyInfoDelete()
	CUIManager.CloseUI(CLuaUIResources.BabyExpressionUnlockWnd)
end

function LuaBabyExpressionUnlockWnd:OnEnable()
	g_ScriptEvent:AddListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

function LuaBabyExpressionUnlockWnd:OnDisable()
	g_ScriptEvent:RemoveListener("BabyInfoDelete", self, "OnBabyInfoDelete")
end

return LuaBabyExpressionUnlockWnd
