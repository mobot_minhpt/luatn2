local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UISlider=import "UISlider"
local Profession = import "L10.Game.Profession"
local QnTableView=import "L10.UI.QnTableView"
local QnTabView=import "L10.UI.QnTabView"
local PlayerSettings=import "L10.Game.PlayerSettings"
local QnCheckBox=import "L10.UI.QnCheckBox"

CLuaCityWarMonsterSiegeSituationWnd = class()
RegistClassMember(CLuaCityWarMonsterSiegeSituationWnd,"m_TabView")
RegistClassMember(CLuaCityWarMonsterSiegeSituationWnd,"m_TableView")
RegistClassMember(CLuaCityWarMonsterSiegeSituationWnd,"m_DataList")
RegistClassMember(CLuaCityWarMonsterSiegeSituationWnd,"m_MemberInfo")

RegistClassMember(CLuaCityWarMonsterSiegeSituationWnd,"m_CacheData")
RegistClassMember(CLuaCityWarMonsterSiegeSituationWnd,"m_CheckBox")

-- EnumFightDataType = {
--     eDps = 1,
--     eHeal = 2,
--     eUnderDamage = 3,
-- }
function CLuaCityWarMonsterSiegeSituationWnd:Init()
     self.m_CheckBox = FindChild(self.transform,"Checkbox"):GetComponent(typeof(QnCheckBox))
    self.m_CheckBox:SetSelected(PlayerSettings.HideNoneEnemyInMonsterSiege, true)
    self.m_CheckBox.OnValueChanged = DelegateFactory.Action_bool(function(val)
        PlayerSettings.HideNoneEnemyInMonsterSiege = val
    end)

    self.m_DataList={}
    self.m_MemberInfo=nil
    self.m_CacheData={}
    self.m_TableView = FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            return #self.m_DataList
        end,
        function(item,row)
            local index = self.m_TabView.CurrentIndex
            local key = index==3 and 1 or 0
            local item=self.m_TableView:GetFromPool(key)
            if index==3 then
                self:InitMemberItem(item.transform,row)
            else
                self:InitItem(item.transform,row)
            end
            return item
        end)

    self.m_TabView=FindChild(self.transform,"TabView"):GetComponent(typeof(QnTabView))
    self.m_TabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function(btn,index)
        self.m_TableView:Clear()
        if index==0 or index==1 or index==2 then
            if self.m_CacheData[index+1] then
                self.m_DataList = self.m_CacheData[index+1]
                self.m_TableView:ReloadData(true,false)
            else
                Gac2Gas.QueryMonsterSiegeFightData(index+1)
            end
        elseif index==3 then
            if self.m_MemberInfo then
                self.m_DataList=self.m_MemberInfo
                self.m_TableView:ReloadData(true,false)
            else
                Gac2Gas.QueryMonsterSiegfeCopyMemberInfo()
            end
        end
    end)

    self.m_TabView:ChangeTo(0)

    local broadcastButton = FindChild(self.transform,"BroadcastBtn").gameObject
    UIEventListener.Get(broadcastButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local index = self.m_TabView.CurrentIndex
        if index==0 or index==1 or index==2 then
            Gac2Gas.BroadcastMonsterSiegeFightData(index+1)
        end
    end)

    -- {"QueryMonsterSiegeFightData", "I", nil, nil, "lua"}, --dataType 
	-- {"ResetMonsterSiegeFightData", "I", nil, nil, "lua"}, --dataType 
    -- {"BroadcastMonsterSiegeFightData", "I", nil, nil, "lua"}, --dataType 
    
    -- SendMonsterSiegeFightData
end

function CLuaCityWarMonsterSiegeSituationWnd:OnEnable()
    g_ScriptEvent:AddListener("SendMonsterSiegeFightData", self, "OnSendMonsterSiegeFightData")
    g_ScriptEvent:AddListener("SendMonsterSiegeCopyMemberInfo", self, "OnSendMonsterSiegeCopyMemberInfo")
    
end

function CLuaCityWarMonsterSiegeSituationWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendMonsterSiegeFightData", self, "OnSendMonsterSiegeFightData")
    g_ScriptEvent:RemoveListener("SendMonsterSiegeCopyMemberInfo", self, "OnSendMonsterSiegeCopyMemberInfo")

end

function CLuaCityWarMonsterSiegeSituationWnd:OnSendMonsterSiegeFightData(dataType,dataUD, playerCount, totalValue)
    local list = {}
    local raw = MsgPackImpl.unpack(dataUD)
    for i=1,raw.Count,4 do
        local item = {
            playerId = raw[i-1],
            playerName = raw[i],
            playerClass = raw[i+1],
            dataValue = raw[i+2],
        }
        if totalValue==0 then
            item.progress = 0
        else
            item.progress = item.dataValue/totalValue
        end
        table.insert( list,item)
    end

    table.sort( list,function(a,b)
        if a.dataValue>b.dataValue then return true
        elseif a.dataValue<b.dataValue then return false
        else return a.playerId<b.playerId end
    end )
    self.m_CacheData[dataType] = list

    if self.m_TabView.CurrentIndex==dataType-1 then
        self.m_DataList=list
        self.m_TableView:ReloadData(true,false)
    end
end

function CLuaCityWarMonsterSiegeSituationWnd:OnSendMonsterSiegeCopyMemberInfo(dataUD, playerCount)
    local list = {}
    local raw = MsgPackImpl.unpack(dataUD)
    for i=1,raw.Count,5 do
        table.insert( list,{
            playerId = raw[i-1],
            playerName = raw[i],
            playerClass = raw[i+1],
            playerGrade = raw[i+2],
            isInTeam = raw[i+3]
        })
    end
    self.m_MemberInfo = list
    if self.m_TabView.CurrentIndex==3 then
        self.m_DataList=self.m_MemberInfo
        self.m_TableView:ReloadData(true,false)
    end
end

function CLuaCityWarMonsterSiegeSituationWnd:InitItem(transform,row)
    local info = self.m_DataList[row+1]

    local rankLabel = transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local slider = transform:Find("ProgressBar"):GetComponent(typeof(UISlider))

    local icon = transform:Find("ProfessionIcon"):GetComponent(typeof(UISprite))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local percentageLabel = transform:Find("PercentageLabel"):GetComponent(typeof(UILabel))

    rankLabel.text = tostring(row+1)
    nameLabel.text = info.playerName
    icon.spriteName = Profession.GetIconByNumber(info.playerClass)

    valueLabel.text = tostring(info.dataValue)

    slider.value = info.progress
    percentageLabel.text = tostring(math.floor( info.progress*1000 )/10).."%"
    
end

function CLuaCityWarMonsterSiegeSituationWnd:InitMemberItem(transform,row)
    local icon = transform:Find("ProfessionIcon"):GetComponent(typeof(UISprite))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local teamLabel = transform:Find("TeamLabel"):GetComponent(typeof(UILabel))

    local info = self.m_MemberInfo[row+1]
    icon.spriteName = Profession.GetIconByNumber(info.playerClass)
    nameLabel.text = info.playerName
    levelLabel.text = "Lv."..tostring(info.playerGrade)
    teamLabel.text = info.isInTeam>0 and LocalString.GetString("组队") or LocalString.GetString("未组队")
end