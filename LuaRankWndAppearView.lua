local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local EasyTouch = import "EasyTouch"
local Profession = import "L10.Game.Profession"
local Screen = import "UnityEngine.Screen"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CRankData = import "L10.UI.CRankData"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CTitleMgr = import "L10.Game.CTitleMgr"
local Camera = import "UnityEngine.Camera"
local PlayerSettings = import "L10.Game.PlayerSettings"
local RenderTexture = import "UnityEngine.RenderTexture"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local LayerDefine = import "L10.Engine.LayerDefine"
local Object = import "UnityEngine.Object"
local GameObject = import "UnityEngine.GameObject"

LuaRankWndAppearView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaRankWndAppearView, "modelTexture", "modelTexture", UITexture)
RegistChildComponent(LuaRankWndAppearView, "PlayerNameLab", "PlayerNameLab", UILabel)
RegistChildComponent(LuaRankWndAppearView, "Clazz", "Clazz", UISprite)
RegistChildComponent(LuaRankWndAppearView, "TitleLab", "TitleLab", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaRankWndAppearView, "defaultModelLocalPos")
RegistClassMember(LuaRankWndAppearView, "maxPreviewScale")
RegistClassMember(LuaRankWndAppearView, "maxPreviewOffsetY")
RegistClassMember(LuaRankWndAppearView, "localModelScale")
RegistClassMember(LuaRankWndAppearView, "identifier")
RegistClassMember(LuaRankWndAppearView, "m_Rot")
RegistClassMember(LuaRankWndAppearView, "m_PlayerIndex")
RegistClassMember(LuaRankWndAppearView, "m_AppearanceProp")
RegistClassMember(LuaRankWndAppearView, "m_IsInitedYiChu")
RegistClassMember(LuaRankWndAppearView, "m_Cam")
RegistClassMember(LuaRankWndAppearView, "m_YichuGo")
RegistClassMember(LuaRankWndAppearView, "m_ShowModel")
RegistClassMember(LuaRankWndAppearView, "m_FullDetailTemplate")
RegistClassMember(LuaRankWndAppearView, "m_FullDetailTable")

function LuaRankWndAppearView:Awake()
    self.m_Rot = 180

    CommonDefs.AddOnDragListener(self.modelTexture.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)

    self.identifier = SafeStringFormat3("__%s__", tostring(self.modelTexture.gameObject:GetInstanceID()))
    self.maxPreviewScale = 1.5
    self.localModelScale = 1
    self.maxPreviewOffsetY = 0.5
    self.defaultModelLocalPos = Vector3.zero
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z  = -0.9,-0.9,5
    self.PlayerNameLab.alpha = 0
    self.m_Ratio = 1
end

function LuaRankWndAppearView:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "UpdateInfo")
    g_ScriptEvent:AddListener("OnRankDetailViewSelectedPlayer", self, "OnRankDetailViewSelectedPlayer")
    g_ScriptEvent:AddListener("SendPlayerAppearance", self, "OnSendPlayerAppearance")
    if not CommonDefs.IsInMobileDevice() then
        g_ScriptEvent:AddListener("OnWinScreenChangeAfterResolutionCheck", self, "UpdateModelTexture")
    else
        g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateModelTexture")
    end
end

function LuaRankWndAppearView:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "UpdateInfo")
    g_ScriptEvent:RemoveListener("OnRankDetailViewSelectedPlayer", self, "OnRankDetailViewSelectedPlayer")
    g_ScriptEvent:RemoveListener("SendPlayerAppearance", self, "OnSendPlayerAppearance")
    if not CommonDefs.IsInMobileDevice() then
        g_ScriptEvent:RemoveListener("OnWinScreenChangeAfterResolutionCheck", self, "UpdateModelTexture")
    else
        g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateModelTexture")
    end
end

--@region UIEvent
function LuaRankWndAppearView:OnScreenDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
    self.m_Rot = self.m_Rot + rot
    self:SetModelRot(self.m_Rot)
end
--@endregion UIEvent

function LuaRankWndAppearView:UpdateInfo()
    local PlayerRankInfo = CRankData.Inst.MainPlayerRankInfo
    local listInfo = CRankData.Inst.RankList
    local showModel = false
    if listInfo.Count > 0 then
        local info = listInfo[0]
        local setting = Rank_SettingNew.GetData(CRankData.GetRankSheetKey(info.rankSheet))
        showModel = setting.ShowModel > 0
        if showModel then
            if self.m_PlayerIndex ~= info.PlayerIndex then
                self.m_PlayerIndex = info.PlayerIndex
                Gac2Gas.QueryPlayerAppearance(info.PlayerIndex, "RankWnd")
            end
        end
    end
    if not showModel then
        self:OnSendPlayerAppearance()
    end
    self.m_ShowModel = showModel
end

function LuaRankWndAppearView:OnRankDetailViewSelectedPlayer(rankItemData)
    if self.m_ShowModel and self.m_PlayerIndex ~= rankItemData.playerId then
        self.m_PlayerIndex = rankItemData.playerId
        Gac2Gas.QueryPlayerAppearance(rankItemData.playerId, "RankWnd")
    end
end

function LuaRankWndAppearView:OnSendPlayerAppearance(targetPlayerId, context, isSucc, name, gender, class, titleId, titleName, appearInfo)
    if isSucc and context == "RankWnd" and appearInfo then
        appearInfo.ResourceId = 0
        self.m_AppearanceProp = appearInfo
        Fashion_FashionPreview.Foreach(function (key, data)
            if data.Class == class and data.Gender == gender then
                self.maxPreviewScale = data.MaxPreviewScale
                self.maxPreviewOffsetY = data.MaxPreviewOffsetY
            end
        end)

        self.PlayerNameLab.alpha = 1
        self.PlayerNameLab.text = name
        self.Clazz.spriteName = Profession.GetIconByNumber(class)
        self.TitleLab.text = titleName
        local title = Title_Title.GetData(titleId)
        if title ~= nil then
            self.TitleLab.color = CTitleMgr.Inst:GetTitleColor(titleId)
        end
        self:InitModel()
    else
        self.m_PlayerIndex = nil
        self.PlayerNameLab.alpha = 0
        self.modelTexture.mainTexture = nil
        self.m_AppearanceProp = nil
        self:InitModel()
    end
end

function LuaRankWndAppearView:InitModel()
    self.m_Rot = 180
    CUIManager.CreateModelTexture(self.identifier, LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_Cam = CUIManager.instance.transform:Find(self.identifier):GetComponent(typeof(Camera))
        self.m_Cam.farClipPlane = 50
        self.modelTexture.mainTexture = self:UpdateModelTexture()
        self:CreateModel(ro)
    end), self.m_Rot, self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z, false, true, 1.0, true, false)

    self:SetModelPosAndScale(self.defaultModelLocalPos, 1)
    self:SetModelRot(self.m_Rot)
end

function LuaRankWndAppearView:CreateModel(renderObj)
    local appearanceData = self.m_AppearanceProp
    if appearanceData ~= nil then
        CClientMainPlayer.LoadResource(renderObj, appearanceData, true, 1, 0, false, appearanceData.ResourceId, false, 0, false, nil)
        if appearanceData.ResourceId ~= 0 then
            local scale = 1
            local data = Transform_Transform.GetData(appearanceData.ResourceId)
            if data ~= nil then
                scale = data.PreviewScale
            end
            renderObj.Scale = scale
        end
    end

    if not self.m_IsInitedYiChu then
        CSharpResourceLoader.Inst:LoadGameObject("Levels/Dynamic/yichu_shangchengbg.prefab", DelegateFactory.Action_GameObject(function(obj)
            if self.identifier == nil then
                return
            end
            local modelRoot = CUIManager.instance.transform:Find(self.identifier .. "/ModelRoot")
            if CommonDefs.IsUnityObjectNull(modelRoot) then
                return
            end
            local go = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity)
            NGUITools.SetLayer(go, LayerDefine.ModelForNGUI_3D)
            go.transform.parent = modelRoot
            go.transform.localPosition = Vector3(45.65, 0, -131.7)
            go.transform.localRotation = Quaternion.identity
            go.transform.localScale = Vector3.one
            go.transform.parent = go.transform.parent.parent
            self.m_YichuGo = go
            self:CheckYiChuBoWen()
            self:AdjustModelPos()
        end))
        self.m_IsInitedYiChu = true
    else
        self:CheckYiChuBoWen()
    end
end

function LuaRankWndAppearView:CheckYiChuBoWen()
    if not CommonDefs.IsUnityObjectNull(self.m_YichuGo) then
        local bowen = self.m_YichuGo.transform:Find("yichu_liusha002/Particle System (3)")
        if bowen then
            bowen.gameObject:SetActive(self.m_AppearanceProp ~= nil)
        end
    end
end

function LuaRankWndAppearView:SetModelRot(rotY)
    CUIManager.SetModelRotation(self.identifier, rotY)
end

function LuaRankWndAppearView:SetModelPosAndScale(pos, scale)
    self.localModelScale = scale
    pos = Vector3(pos.x / scale, pos.y / scale, pos.z /scale)
    CUIManager.SetModelPosition(self.identifier, pos)
end

function LuaRankWndAppearView:UpdateModelTexture()
    if CommonDefs.IsUnityObjectNull(self.m_Cam)then
        return
    end
    local rt = self.m_Cam.targetTexture
    if rt then
        Object.Destroy(rt)
    end

    local screenHeight = PlayerSettings.s_EnableHighResolutionLimit and PlayerSettings.s_HighResolutionLimit or CommonDefs.GameScreenHeight
    local screenWidth = PlayerSettings.s_EnableHighResolutionLimit and (CUIManager.ScreenWidthScale * screenHeight * Screen.width / Screen.height) or CommonDefs.GameScreenWidth

    if screenHeight < 720 then
        self.m_Ratio = 720 / screenHeight
        screenHeight = 720
        screenWidth = screenWidth * self.m_Ratio
    else
        self.m_Ratio = 1
    end

    rt = RenderTexture(screenWidth, screenHeight, 24, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default)
    rt.wrapMode = TextureWrapMode.Clamp
    rt.filterMode = FilterMode.Bilinear
    rt.anisoLevel = 2
    rt.name = self.m_Cam.name
    self.m_Cam.targetTexture = rt

    if self.modelTexture then
        self.modelTexture.mainTexture = rt
    end
    self.m_Cam:Render()
    self:AdjustModelPos()

    return rt
end

function LuaRankWndAppearView:AdjustModelPos()
    -- 获取UI PlayerNameLab屏幕x坐标用于调整模型位置
    local labScreenPos = CUIManager.UIMainCamera:WorldToScreenPoint(self.PlayerNameLab.transform.position)
    local modelRoot = CUIManager.instance.transform:Find(self.identifier .. "/ModelRoot")
    if modelRoot and not CommonDefs.IsUnityObjectNull(self.m_YichuGo) then
        self.m_Rot = 180
        self:SetModelRot(self.m_Rot)
        local screenPos = self.m_Cam:WorldToScreenPoint(modelRoot.position)
        local newScreenPos = Vector3(labScreenPos.x * self.m_Ratio - 8.5, 85, 5)
        modelRoot.position = self.m_Cam:ScreenToWorldPoint(newScreenPos)
        self.defaultModelLocalPos = modelRoot.localPosition
        
        self.m_YichuGo.transform.parent = modelRoot
        self.m_YichuGo.transform.localPosition = Vector3(45.65, 0, -131.7)
        self.m_YichuGo.transform.localRotation = Quaternion.identity
        self.m_YichuGo.transform.localScale = Vector3.one
        self.m_YichuGo.transform.parent = self.m_YichuGo.transform.parent.parent
    end
end

function LuaRankWndAppearView:OnDestroy()
    self.modelTexture.mainTexture = nil
    CUIManager.DestroyModelTexture(self.identifier)
end
