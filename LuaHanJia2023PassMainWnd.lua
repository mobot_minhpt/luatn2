local GameObject                = import "UnityEngine.GameObject"
local CScheduleMgr              = import "L10.Game.CScheduleMgr"
local CommonDefs                = import "L10.Game.CommonDefs"
local CUITexture				= import "L10.UI.CUITexture"
local CUIManager                = import "L10.UI.CUIManager"
local QnTabButton               = import "L10.UI.QnTabButton"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
local UISlider                  = import "UISlider"
local Animation = import "UnityEngine.Animation"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Vector4 = import "UnityEngine.Vector4"

LuaHanJia2023PassMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHanJia2023PassMainWnd, "BuyAdvPassBtn", "BuyAdvPassBtn", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "HelpBtn", "HelpBtn", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "LvBg", "LvBg", CUITexture)
RegistChildComponent(LuaHanJia2023PassMainWnd, "ExpSlider", "ExpSlider", UISlider)
RegistChildComponent(LuaHanJia2023PassMainWnd, "LvLb", "LvLb", UILabel)
RegistChildComponent(LuaHanJia2023PassMainWnd, "BuyLevelBtn", "BuyLevelBtn", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "NeedExpLb", "NeedExpLb", UILabel)
RegistChildComponent(LuaHanJia2023PassMainWnd, "RewardBtn", "RewardBtn", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "TaskBtn", "TaskBtn", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "RewardTabPanel", "RewardTabPanel", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "TaskTabPanel", "TaskTabPanel", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "ExtSprite", "ExtSprite", GameObject)
RegistChildComponent(LuaHanJia2023PassMainWnd, "ResetBattlePassButton", "ResetBattlePassButton", GameObject)

function LuaHanJia2023PassMainWnd:Awake()
    
    self.CurTabIndex = -1
    
    self.ExtPanel = self.transform:Find("ExtPanel")
    self.HiddenCloseExtButton = self.ExtPanel:Find("HiddenCloseExtButton")
    self.ExtItem = self.ExtPanel:Find("AddRateItem")
    self.ExtTable = self.ExtPanel:Find("AddRateTable")

    UIEventListener.Get(self.HelpBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHelpBtnClick()
    end)

    UIEventListener.Get(self.BuyAdvPassBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick()
    end)

    UIEventListener.Get(self.BuyLevelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyLevelBtnClick()
    end)

    UIEventListener.Get(self.ExtSprite).onClick = DelegateFactory.VoidDelegate(function(go)
        self.ExtPanel.gameObject:SetActive(true)
        self:RefreshExtData()
    end)

    UIEventListener.Get(self.HiddenCloseExtButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self.ExtPanel.gameObject:SetActive(false)
    end)
    
    UIEventListener.Get(self.ResetBattlePassButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnResetBattlePassButtonClick()
    end)

    UIEventListener.Get(self.RewardBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectTab(0)
    end)

    UIEventListener.Get(self.TaskBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSelectTab(1)
    end)
    self:OnSelectTab(0)

    Gac2Gas.RequestOpenXianKeLaiWnd()
 
    g_ScriptEvent:AddListener("HanJia2023_SyncXianKeLianData", self, "OnXianKeLaiDataChange")
    g_ScriptEvent:AddListener("ShowUIPreDraw", self, "OnShowUIPreDraw")
    
    self.clickBuyLevel = false
end

function LuaHanJia2023PassMainWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXianKeLianData", self, "OnXianKeLaiDataChange")
    g_ScriptEvent:RemoveListener("ShowUIPreDraw", self, "OnShowUIPreDraw")
end

function LuaHanJia2023PassMainWnd:Init()
    self:OnXianKeLaiDataChange()
    self.transform:GetComponent(typeof(Animation)):Play("hanjia2023passmainwnd_show")
end

function LuaHanJia2023PassMainWnd:OnEnable()
end

function LuaHanJia2023PassMainWnd:OnXianKeLaiDataChange()

    local cfg = LuaHanJia2023Mgr.XKLData.Cfg
    local lv = LuaHanJia2023Mgr.XKLData.Lv
    if lv == nil then
        return
    end

    local lvexp = self:GetLvExp(lv)
    local curexp =  LuaHanJia2023Mgr.XKLData.Exp - lvexp  --为当前等级下的经验值，不会超过当前等级所需经验的上上限
    local passtype = LuaHanJia2023Mgr.XKLData.PassType --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版,3全买
    local maxResetTime = HanJia2023_XianKeLaiSetting.GetData().MaxResetNum
    local curcfg = cfg[lv]
    
    if curcfg then
        self.LvLb.text = tostring(lv)
        --self.LvBg:LoadMaterial(curcfg.Icon)
        if lv >= LuaHanJia2023Mgr.XKLData.CfgMaxLv then
            self.ExpSlider.value = 1

            if LuaHanJia2023Mgr.XKLData.resetNum >= maxResetTime then
                --重置次数也满了
                self.NeedExpLb.text = g_MessageMgr:FormatMessage("HanJia2023_BattlePass_NoMoreResetTips")
                self.BuyLevelBtn:SetActive(false)
                self.ResetBattlePassButton:SetActive(false)
            else    
                --还有剩余重置次数
                self.NeedExpLb.text = g_MessageMgr:FormatMessage("HanJia2023_BattlePass_ResetTips")
                self.BuyLevelBtn:SetActive(false)
                self.ResetBattlePassButton:SetActive(true)
            end
        else
            local nextcfg = cfg[lv+1]
            local needexp = nextcfg.NeedXianLu - curexp
            self.NeedExpLb.text = LocalString.GetString("升至下级还需")..needexp
            self.ExpSlider.value = curexp / nextcfg.NeedXianLu
            self.BuyLevelBtn:SetActive(true)
            self.ResetBattlePassButton:SetActive(false)
        end
    end

    --self.BuyLevelBtn:SetActive(passtype~=0)
    self.BuyAdvPassBtn:SetActive(passtype ~= 3 or (passtype == 3 and lv < LuaHanJia2023Mgr.XKLData.CfgMaxLv))
    if passtype ~= 3 then
        self.BuyAdvPassBtn.transform:Find("BuyLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("解锁高级花蕊")
    else    
        --BuyAdvPassBtn被用来显示购买等级了
        self.BuyAdvPassBtn.transform:Find("BuyLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("提升花蕊等级")
    end

    self.ExtSprite:SetActive(passtype == 2 or passtype == 3 or LuaHanJia2023Mgr.XKLData.addRate ~= 0)--豪华酒壶
    local addVal = LuaHanJia2023Mgr.XKLData.addRate * 100
    if passtype == 2 or passtype == 3 then
        addVal = addVal + 20
    end
    self.ExtSprite.transform:Find("ExtLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("+") .. (addVal)  .. LocalString.GetString("%任务仙露获得")
    
    local fxNode = self.BuyAdvPassBtn.transform:Find('Fx'):GetComponent(typeof(CUIFx))
    if passtype == 3 then
        self.BuyAdvPassBtn.transform:Find("Fx/liuguang").gameObject:SetActive(false)
        local tex = self.BuyAdvPassBtn.transform:GetComponent(typeof(UISprite))
        fxNode:DestroyFx()
        fxNode:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        local width = tex.width / 0.7
        local height = tex.height
        CUIFx.DoAni(Vector4(-width/2, height/2, width, height), false, fxNode)
    end
    
    local fx = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn.transform,"Fx").uiFx
    fx.Visible = (passtype ~= 3 or (passtype == 3 and lv < LuaHanJia2023Mgr.XKLData.CfgMaxLv and (not self.clickBuyLevel)))

    local alertgo1 =  LuaGameObject.GetChildNoGC(self.RewardBtn.transform, "AlertSprite").gameObject
    local alertgo2 =  LuaGameObject.GetChildNoGC(self.TaskBtn.transform, "AlertSprite").gameObject

    local alert1 = self:HaveReward()
    local alert2 = self:HaveTaskReward()
    alertgo1:SetActive(alert1)
    alertgo2:SetActive(alert2)
    if alert1 or alert2 then
        CScheduleMgr.Inst:SetAlertState(42010102,CScheduleMgr.EnumAlertState.Show,true)
    else
        CScheduleMgr.Inst:SetAlertState(42010102,CScheduleMgr.EnumAlertState.Hide,true)
    end

    if lv == LuaHanJia2023Mgr.XKLData.CfgMaxLv then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.HanJia2023BattlePassResetGuide)
    end
end

function LuaHanJia2023PassMainWnd:HaveTaskReward()
    --现在任务奖励的自动领取的
    return false
end

function LuaHanJia2023PassMainWnd:HaveReward()
    return LuaHanJia2023Mgr:CheckHaveReward()
end

function LuaHanJia2023PassMainWnd:GetLvExp(lv)
    local res = 0
    local cfg = LuaHanJia2023Mgr.XKLData.Cfg
    local min = math.min(#cfg, lv)
    for i = 1, min do
        res  = res + cfg[i].NeedXianLu
    end
    return res
end



--@region UIEvent

--[[
    @desc: 页签切换
    author:{author}
    time:2020-10-30 10:49:43
    --@tab: 0:奖励页签；1:任务页签
    @return:
]]
function LuaHanJia2023PassMainWnd:OnSelectTab(tab)
    if self.CurTabIndex == tab then return end
    self.CurTabIndex = tab
    self.RewardTabPanel:SetActive(self.CurTabIndex == 0)
    self.TaskTabPanel:SetActive(self.CurTabIndex == 1)
    if self.CurTabIndex == 1 then
        Gac2Gas.RequestOpenXianKeLaiTaskWnd()
    end
    
    local rewardbtn = CommonDefs.GetComponent_GameObject_Type(self.RewardBtn,typeof(QnTabButton))
    local taskbtn = CommonDefs.GetComponent_GameObject_Type(self.TaskBtn,typeof(QnTabButton))
    rewardbtn.Selected = self.CurTabIndex == 0
    taskbtn.Selected = self.CurTabIndex == 1
    rewardbtn.transform:Find("Selected").gameObject:SetActive(self.CurTabIndex == 0)
    taskbtn.transform:Find("Selected").gameObject:SetActive(self.CurTabIndex == 1)

end

function LuaHanJia2023PassMainWnd:OnHelpBtnClick()
    g_MessageMgr:ShowMessage("XianKeLai_BattlePass_Rule")
end

function LuaHanJia2023PassMainWnd:OnBuyAdvPassBtnClick()
    local passtype = LuaHanJia2023Mgr.XKLData.PassType
    if passtype ~= 3 then
        CUIManager.ShowUI(CLuaUIResources.HanJia2023PassDetailWnd)
    else
        self:OnBuyLevelBtnClick()
    end
end

function LuaHanJia2023PassMainWnd:OnBuyLevelBtnClick()
    CUIManager.ShowUI(CLuaUIResources.TcjhBuyLevelWnd)
    self.clickBuyLevel = true
    self:OnXianKeLaiDataChange()
end

function LuaHanJia2023PassMainWnd:OnResetBattlePassButtonClick()
    local passtype = LuaHanJia2023Mgr.XKLData.PassType
    local resetCostList = HanJia2023_XianKeLaiSetting.GetData().ResetLevelCost
    local price = 0
    if passtype == 2 or passtype == 3 then
        price = resetCostList[2]
    elseif passtype == 1 then
        price = resetCostList[1]
    else
        price = resetCostList[0]
    end
    local msg = g_MessageMgr:FormatMessage("HanJia2023_BattlePass_ResetReconfirm", price)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
            function()
                Gac2Gas.RequestResetXianKeLaiLevel()
            end
    ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function LuaHanJia2023PassMainWnd:OnShowUIPreDraw(args)
    local show, name = args[0], args[1]
    if name == "HanJia2023PassDetailWnd" and show == false and CUIManager.instance then
        --特殊处理一下，当关闭购买界面的时候, 播放一下主界面的动效
        self.transform:GetComponent(typeof(Animation)):Play("hanjia2023passmainwnd_show")
    end
end

function LuaHanJia2023PassMainWnd:GetGuideGo(methodName)
    if methodName == "GetOneKeyButton" then
        return self.RewardTabPanel.transform:Find("OneKeyBtn").gameObject
    elseif methodName == "GetTaskButton" then
        return self.TaskBtn
    elseif methodName == "GetBuyLevelBtn" then
        return self.BuyLevelBtn
    elseif methodName == "GetBuyAdvPassBtn" then    
        return self.BuyAdvPassBtn
    elseif methodName == "GetResetButton" then
        return self.ResetBattlePassButton
    end
    return nil
end

function LuaHanJia2023PassMainWnd:RefreshExtData()
    Extensions.RemoveAllChildren(self.ExtTable)
    if LuaHanJia2023Mgr.XKLData and LuaHanJia2023Mgr.XKLData.addRate ~= 0 then
        local obj = NGUITools.AddChild(self.ExtTable.gameObject, self.ExtItem.gameObject)
        obj:SetActive(true)
        obj.transform:Find("rateName"):GetComponent(typeof(UILabel)).text = LocalString.GetString("新服加成")
        obj.transform:Find("rateValue"):GetComponent(typeof(UILabel)).text = "+" .. LuaHanJia2023Mgr.XKLData.addRate * 100 .. "%"
    end

    if LuaHanJia2023Mgr.XKLData then
        local passtype = LuaHanJia2023Mgr.XKLData.PassType
        if passtype == 2 or passtype == 3 then
            local obj = NGUITools.AddChild(self.ExtTable.gameObject, self.ExtItem.gameObject)
            obj:SetActive(true)
            obj.transform:Find("rateName"):GetComponent(typeof(UILabel)).text = LocalString.GetString("仙姿奇蕊")
            obj.transform:Find("rateValue"):GetComponent(typeof(UILabel)).text = LocalString.GetString("+20%")
        end        
    end
    
    self.ExtTable.transform:GetComponent(typeof(UIGrid)):Reposition()
    
    local childCount = self.ExtTable.childCount
    self.ExtPanel:Find("Bg"):GetComponent(typeof(UIWidget)).height = 18 + 72 * childCount
end

--@endregion
