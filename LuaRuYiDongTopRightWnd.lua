local UISlider = import "UISlider"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CScene = import "L10.Game.CScene"
local UITexture = import "UITexture"

LuaRuYiDongTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRuYiDongTopRightWnd, "Slider", "Slider", UISlider)
RegistChildComponent(LuaRuYiDongTopRightWnd, "BossIcon1", "BossIcon1", CUITexture)
RegistChildComponent(LuaRuYiDongTopRightWnd, "BossIcon2", "BossIcon2", CUITexture)
RegistChildComponent(LuaRuYiDongTopRightWnd, "BossIcon3", "BossIcon3", CUITexture)
RegistChildComponent(LuaRuYiDongTopRightWnd, "BossIcon4", "BossIcon4", CUITexture)
RegistChildComponent(LuaRuYiDongTopRightWnd, "ExpandButton", "ExpandButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaRuYiDongTopRightWnd,"m_Difficulty")
RegistClassMember(LuaRuYiDongTopRightWnd,"m_TotalCount")
function LuaRuYiDongTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Difficulty = 0
    self.m_TotalCount = 4
    UIEventListener.Get(self.ExpandButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
end

function LuaRuYiDongTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncHuluBrotherBossProgress", self, "OnSyncHuluBrotherBossProgress")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaRuYiDongTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHuluBrotherBossProgress", self, "OnSyncHuluBrotherBossProgress")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaRuYiDongTopRightWnd:Init()
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local setting = HuluBrothers_Setting.GetData()
    
    if gamePlayId == setting.NormalPlayId then
        self.m_Difficulty = 0
    elseif gamePlayId == setting.HardPlayId then
        self.m_Difficulty = 1
    elseif gamePlayId == setting.HellPlayId then
        self.m_Difficulty = 2
    end
    local bossIcons = setting.RuYiDongBoss
    for i=0,3,1 do 
        local path = bossIcons[self.m_Difficulty*4 + i]
        local icon = self[SafeStringFormat3("BossIcon%d",i+1)]
        icon:LoadMaterial(SafeStringFormat3("UI/Texture/Portrait/Material/%s.mat",path))
    end

    if not LuaHuLuWa2022Mgr.m_KillCount then
        LuaHuLuWa2022Mgr.m_KillCount = 0
    end
    --self:InitProgress(LuaHuLuWa2022Mgr.m_KillCount,self.m_Difficulty)
    self:InitProgress(0,self.m_Difficulty)

end

function LuaRuYiDongTopRightWnd:InitProgress(killcount)
    --隐藏 已击杀
    for i=1,killcount,1 do
        local bossIcon = self[SafeStringFormat3("BossIcon%d",i)]
        bossIcon.gameObject:SetActive(false)
    end

    --Highlight 正在击杀
    local highlightIcon = self[SafeStringFormat3("BossIcon%d",killcount+1)]

    if highlightIcon then
        highlightIcon.gameObject:SetActive(true)
        highlightIcon.transform:GetComponent(typeof(UITexture)).width = 90
        highlightIcon.transform:GetComponent(typeof(UITexture)).height = 90
        local highLight = highlightIcon.transform:Find("Highlight")
        highLight.gameObject:SetActive(true)
    end
    --Normal 还未进行到
    for j=killcount+2,self.m_TotalCount,1 do
        local bossIcon = self[SafeStringFormat3("BossIcon%d",j)]
        bossIcon.gameObject:SetActive(true)
        bossIcon.transform:GetComponent(typeof(UITexture)).width = 70
        bossIcon.transform:GetComponent(typeof(UITexture)).height = 70
        local highLight = bossIcon.transform:Find("Highlight")
        highLight.gameObject:SetActive(false)
    end

    --进度条
    self.Slider.value = killcount / self.m_TotalCount
end

function LuaRuYiDongTopRightWnd:OnSyncHuluBrotherBossProgress(killcount)
    --print("OnSyncHuluBrotherBossProgress",killcount)
    self:InitProgress(killcount)
end

function LuaRuYiDongTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

--@region UIEvent

--@endregion UIEvent

