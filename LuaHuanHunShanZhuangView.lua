local UILabel = import "UILabel"
local UITable = import "UITable"
local UISprite = import "UISprite"

local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CScene = import "L10.Game.CScene"

LuaHuanHunShanZhuangView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuanHunShanZhuangView, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaHuanHunShanZhuangView, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaHuanHunShanZhuangView, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaHuanHunShanZhuangView, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaHuanHunShanZhuangView, "Table", "Table", UITable)
RegistChildComponent(LuaHuanHunShanZhuangView, "BgTexture", "BgTexture", UISprite)

--@endregion RegistChildComponent end

RegistClassMember(LuaHuanHunShanZhuangView, "m_Content")

function LuaHuanHunShanZhuangView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)


	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
end

function LuaHuanHunShanZhuangView:Init()
    
end

function LuaHuanHunShanZhuangView:OnEnable()
	self:OnUpdateGamePlayStageInfo(LuaHuanHunShanZhuangMgr.m_TaskStageGamePlayId, LuaHuanHunShanZhuangMgr.m_TaskSategTitle, LuaHuanHunShanZhuangMgr.m_TaskContent)
    g_ScriptEvent:AddListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "CountdownTickFunc")
end

function LuaHuanHunShanZhuangView:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "CountdownTickFunc")
end

--@region UIEvent

function LuaHuanHunShanZhuangView:OnLeftBtnClick()
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaHuanHunShanZhuangView:OnRightBtnClick()
	--g_MessageMgr:ShowMessage("ShuJia2022_HuanHunShanZhuangNei_Tips")
	LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(3)
end

--@endregion UIEvent

function LuaHuanHunShanZhuangView:GetRemainTimeText(totalSeconds)
	if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaHuanHunShanZhuangView:CountdownTickFunc()
	self.ContentLabel.text = self.m_Content.."\n[c][ACF9FF]"..LocalString.GetString("剩余时间：").."[FFFFFF]"..self:GetRemainTimeText(CScene.MainScene.ShowTime)
end

function LuaHuanHunShanZhuangView:OnUpdateGamePlayStageInfo(gameplayId, title, content)
	self.TitleLabel.text = title and CUICommonDef.TranslateToNGUIText(title) or ""
	self.m_Content = content and CUICommonDef.TranslateToNGUIText(content) or ""

	self:CountdownTickFunc()

	--self.Table:Reposition()
	self.BgTexture.height = math.max(130, NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + 30)
end

