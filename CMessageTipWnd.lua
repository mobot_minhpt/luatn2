-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CLogMgr = import "L10.CLogMgr"
local CMessageTipInfo = import "L10.UI.CMessageTipInfo"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CMessageTipWnd = import "L10.UI.CMessageTipWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CTipTitleItem = import "CTipTitleItem"
local CTooltip = import "L10.UI.CTooltip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Extensions = import "Extensions"
local NGUIMath = import "NGUIMath"
local Screen = import "UnityEngine.Screen"
local ScreenOrientation = import "UnityEngine.ScreenOrientation"
local String = import "System.String"
local UIRect = import "UIRect"
local UIRoot = import "UIRoot"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local WndType = import "L10.UI.WndType"
CMessageTipWnd.m_Init_CS2LuaHook = function (this) 
    -- 根据style改变背景
    if CMessageTipMgr.Inst.Style then
        if CMessageTipMgr.Inst.Style == 1 then
            this.background.spriteName = "common_hpandmp_hp"
        end
    end
    Extensions.RemoveAllChildren(this.contentTable.transform)
    if CMessageTipMgr.Inst.TitleVisible then
        local titleGo = CUICommonDef.AddChild(this.contentTable.gameObject, this.titleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(CMessageTipMgr.Inst.Title)

        if CMessageTipMgr.Inst.Style then
            if CMessageTipMgr.Inst.Style == 1 then
                titleGo.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = "common_hpandmp_hp"
            end
        end
    end
    do
        local i = 0
        while i < CMessageTipMgr.Inst.Paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(this.contentTable.gameObject, this.paragraphTemplate)
            paragraphGo:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(CMessageTipMgr.Inst.Paragraphs[i], CMessageTipMgr.Inst.ContentColor)
            i = i + 1
        end
    end

    this.suffixMsgLabel.gameObject:SetActive(CMessageTipMgr.Inst.SuffixVisible)
    this.suffixMsgLabel.text = CMessageTipMgr.Inst.Suffix

    this.contentTable:Reposition()
    this:LayoutWnd()
    if CMessageTipMgr.Inst.AlignType ~= CTooltip.AlignType.Absolute then
        this:AdjustPosition(CMessageTipMgr.Inst.AlignType, CMessageTipMgr.Inst.TargetCenterPos, CMessageTipMgr.Inst.TargetSize)
    end
end
CMessageTipWnd.m_AdjustPosition_CS2LuaHook = function (this, type, worldPos, targetSize) 
    local localPos = this.background.transform.parent:InverseTransformPoint(worldPos)
    repeat
        local default = type
        if default == CTooltip.AlignType.Default then
            this.background.transform.localPosition = localPos
            break
        elseif default == CTooltip.AlignType.Left then
            this.background.transform.localPosition = Vector3(localPos.x - this.background.localSize.x * 0.5 - targetSize.x * 0.5 - 10, localPos.y, 0)
            break
        elseif default == CTooltip.AlignType.Right then
            this.background.transform.localPosition = Vector3(localPos.x + this.background.localSize.x * 0.5 + targetSize.x * 0.5 + 10, localPos.y, 0)
            break
        elseif default == CTooltip.AlignType.Bottom then
            do
                local selfBounds = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform)
                this.background.transform.localPosition = Vector3(localPos.x, localPos.y - targetSize.y * 0.5 - selfBounds.size.y * 0.5 - 10, 0)
                break
            end
        elseif default == CTooltip.AlignType.Top then
            do
                local selfBounds = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform)
                this.background.transform.localPosition = Vector3(localPos.x, localPos.y + targetSize.y * 0.5 + selfBounds.size.y * 0.5 + 10, 0)
                break
            end
        end
    until 1
    local scale = UIRoot.GetPixelSizeAdjustment(this.background.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale
    local offsetX = this.background.transform.localPosition.x
    local offsetY = this.background.transform.localPosition.y
    --左右不超出屏幕范围
    if offsetX < - virtualScreenWidth * 0.5 + this.background.width * 0.5 then
        offsetX = - virtualScreenWidth * 0.5 + this.background.width * 0.5 + 25
    end
    if offsetX > (virtualScreenWidth * 0.5 - this.background.width * 0.5) then
        offsetX = virtualScreenWidth * 0.5 - this.background.width * 0.5 - 25
    end
    --上下不超出屏幕范围
    if offsetY < (- virtualScreenHeight * 0.5 + this.background.height * 0.5) then
        offsetY = - virtualScreenHeight * 0.5 + this.background.height * 0.5
    end
    if offsetY > (virtualScreenHeight * 0.5 - this.background.height * 0.5) then
        offsetY = virtualScreenHeight * 0.5 - this.background.height * 0.5
    end
    this.background.transform.localPosition = Vector3(offsetX, offsetY, 0)
end
CMessageTipWnd.m_LayoutWnd_CS2LuaHook = function (this) 
    this.contentScrollView.panel.bottomAnchor.absolute = 30
    if CMessageTipMgr.Inst.SuffixVisible then
        this.contentScrollView.panel.bottomAnchor.absolute = 80
    end

    local scale = UIRoot.GetPixelSizeAdjustment(this.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(this.contentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(this.contentScrollView.panel.bottomAnchor.absolute)

    --设置背景宽度
    if CMessageTipMgr.Inst.BgWidth > 0 then
        this.background.width = CMessageTipMgr.Inst.BgWidth
        local titleItems = CommonDefs.GetComponentsInChildren_Component_Type(this.contentTable, typeof(CTipTitleItem))
        do
            local i = 0
            while i < titleItems.Length do
                titleItems[i]:UpdateWidth(this.background.width - 100)
                i = i + 1
            end
        end
        local paramItems = CommonDefs.GetComponentsInChildren_Component_Type(this.contentTable, typeof(CTipParagraphItem))
        do
            local i = 0
            while i < paramItems.Length do
                paramItems[i]:UpdateWidth(this.background.width - 100)
                i = i + 1
            end
        end
        this.contentTable.repositionNow = true
    end

    --计算高度
    local totalWndHeight = this:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + this.contentScrollView.panel.clipSoftness.y * 2 + 1
    --加1避免由于精度误差导致scrollview刚好显示全table内容时可以滚动的问题
    local displayWndHeight = math.min(math.max(totalWndHeight, this.minHeight), virtualScreenHeight)

    --设置背景高度
    this.background.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(this, typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end
    this.contentScrollView:ResetPosition()
    this.scrollViewIndicator:Layout()
    this.background.transform.localPosition = Vector3.zero
end


CMessageTipMgr.m_Display_UInt32_String_CS2LuaHook = function (this, messageId, message) 
    this.m_TipWndType = WndType.TooltipWithoutMask
    this.m_TargetSize = Vector2.zero
    this.m_TargetCenterPos = Vector3.zero
    this.m_BgWidth = - 1
    this.m_AlignType = CTooltip.AlignType.Absolute
    this.m_ContentColor = 4294967295

    if System.String.IsNullOrEmpty(message) then
        return
    end

    local tipMatch = CMessageTipMgr.regex_Tip:Match(message)
    if not tipMatch.Success then
        CLogMgr.Log("message format wrong! messageId :" .. messageId)
        return
    end

    local styleStr = ToStringWrap(tipMatch.Groups[2])
    if styleStr == "" then
        this.m_Style = 0
    else
        this.m_Style = tonumber(styleStr)
    end

    local tipContent = ToStringWrap(tipMatch.Groups[3])

    local titleMatch = CMessageTipMgr.regex_Title:Match(tipContent)
    if titleMatch.Success then
        this.m_TitleVisible = true
        this.m_Title = ToStringWrap(titleMatch.Groups[1])
    else
        this.m_TitleVisible = false
        this.m_Title = ""
    end
    CommonDefs.ListClear(this.m_Paragraphs)
    local paragraphMatchCollection = CMessageTipMgr.regex_Paragraph:Matches(tipContent)
    if paragraphMatchCollection.Count == 0 then
        return
    end
    do
        local i = 0
        while i < paragraphMatchCollection.Count do
            CommonDefs.ListAdd(this.m_Paragraphs, typeof(String), ToStringWrap(paragraphMatchCollection[i].Groups[3]))
            i = i + 1
        end
    end
    local suffixMatch = CMessageTipMgr.regex_post_desc:Match(tipContent)
    if suffixMatch.Success then
        this.m_SuffixVisible = true
        this.m_Suffix = ToStringWrap(suffixMatch.Groups[1])
    else
        this.m_SuffixVisible = false
        this.m_Suffix = ""
    end
    CUIManager.ShowUI(CUIResources.MessageTipWnd)
end
CMessageTipMgr.m_Display_WndType_Boolean_String_List_Int32_Transform_AlignType_UInt32_CS2LuaHook = function (this, wndtype, titleVisible, title, paragraphs, bgWidth, target, alignType, contentColor) 
    this.m_TipWndType = wndtype
    this.m_TitleVisible = titleVisible
    this.m_Title = CChatLinkMgr.TranslateToNGUIText(title, false)
    this.m_ContentColor = contentColor
    if target ~= nil then
        local b = NGUIMath.CalculateRelativeWidgetBounds(target)
        this.m_TargetSize = Vector2(b.size.x, b.size.y)
        this.m_TargetCenterPos = target.transform:TransformPoint(b.center)
    end
    this.m_BgWidth = bgWidth
    this.m_AlignType = alignType

    CommonDefs.ListClear(this.m_Paragraphs)
    if paragraphs ~= nil and paragraphs.Count > 0 then
        do
            local i = 0
            while i < paragraphs.Count do
                CommonDefs.ListAdd(this.m_Paragraphs, typeof(String), CChatLinkMgr.TranslateToNGUIText(paragraphs[i], false))
                i = i + 1
            end
        end
    end
    if this.m_Paragraphs.Count == 0 then
        return
    end
    CUIManager.ShowUI(CUIResources.MessageTipWnd)
end
CMessageTipMgr.m_ExtractTitleAndParagraphs_CS2LuaHook = function (this, input) 
    if System.String.IsNullOrEmpty(input) then
        return nil
    end

    local tipMatch = CMessageTipMgr.regex_Tip:Match(input)
    if not tipMatch.Success then
        CLogMgr.Log("input format wrong! @ MessageTipWnd.ExtractTitleAndParagraphs")
        return nil
    end

    local titleVisible = false
    local title = ""

    local tipContent = ToStringWrap(tipMatch.Groups[3])

    local titleMatch = CMessageTipMgr.regex_Title:Match(tipContent)
    if titleMatch.Success then
        titleVisible = true
        title = ToStringWrap(titleMatch.Groups[1])
    else
        titleVisible = false
        title = ""
    end
    local paragraphMatchCollection = CMessageTipMgr.regex_Paragraph:Matches(tipContent)
    if paragraphMatchCollection.Count == 0 then
        return nil
    end
    local paragrahs = CreateFromClass(MakeGenericClass(List, String))
    local paragraph_names = CreateFromClass(MakeGenericClass(List, String))
    do
        local i = 0
        while i < paragraphMatchCollection.Count do
            CommonDefs.ListAdd(paragraph_names, typeof(String), ToStringWrap(paragraphMatchCollection[i].Groups[2]))
            CommonDefs.ListAdd(paragrahs, typeof(String), ToStringWrap(paragraphMatchCollection[i].Groups[3]))
            i = i + 1
        end
    end

    return CreateFromClass(CMessageTipInfo, titleVisible, title, paragraph_names, paragrahs)
end
