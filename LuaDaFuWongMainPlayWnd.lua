local QnButton = import "L10.UI.QnButton"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local UITable = import "UITable"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CScene=import "L10.Game.CScene"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Vector2 = import "UnityEngine.Vector2"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Ease = import "DG.Tweening.Ease"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local Animation = import "UnityEngine.Animation"
local CSettingInfoMgr = import "L10.UI.CSettingInfoMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
LuaDaFuWongMainPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongMainPlayWnd, "PlayerTemplate", "PlayerTemplate", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "CardTemplate", "CardTemplate", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "Touzi", "Touzi", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "PlayerInfoTable", "PlayerInfoTable", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "Bottom", "Bottom", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "Top", "Top", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "MoneyLabel", "MoneyLabel", UILabel)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "TotalMoneyLabel", "TotalMoneyLabel", UILabel)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "LeaveButton", "LeaveButton", CButton)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "CardTable", "CardTable", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "LoudSpeakLabel", "LoudSpeakLabel", UILabel)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "LoudSpeak", "LoudSpeak", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "NoHouseLabel", "NoHouseLabel", UILabel)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "HouseTable", "HouseTable", UITable)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "ItemTable", "ItemTable", UITable)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "NoItemLabel", "NoItemLabel", UILabel)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "Table", "Table", UITable)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "Pool", "Pool", CUIGameObjectPool)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "Rules_Btn", "Rules_Btn", QnButton)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "LoudSpeak_Btn", "LoudSpeak_Btn", QnButton)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "Setting_Btn", "Setting_Btn", QnButton)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "CoundDown", "CoundDown", UILabel)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "RoundCount", "RoundCount", UILabel)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "JiaoYiHangBtn", "JiaoYiHangBtn", CButton)
RegistChildComponent(LuaDaFuWongMainPlayWnd, "ExtraLabel", "ExtraLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_playerView")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_goodsView")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_landView")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_itemView")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_CardFxView")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_isShowRightTop")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_isShowRightTopTween")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_TouZiTick")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_curSelectPlayerView")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_SelectItem")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_JunWangLingTick")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_ItemChangeEndPosX")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_ItemTablePaddingX")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_ItemCUIFxList")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_TuoWeiOriLocation")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_ItemChangeWnd")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_ModelTextureLoader")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_CanRollTouZi")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_TouZiRo")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_MaxCardNum")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_TouZiPath")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_GetCardFx")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_CameraMoveArea")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_SeriesBorderName")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_StartFx")
RegistClassMember(LuaDaFuWongMainPlayWnd, "m_RoundHasShowFx")
function LuaDaFuWongMainPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)


	
	UIEventListener.Get(self.JiaoYiHangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJiaoYiHangBtnClick()
	end)
	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


	
	UIEventListener.Get(self.Rules_Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRules_BtnClick()
	end)


	UIEventListener.Get(self.LoudSpeak_Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLoudSpeak_BtnClick()
	end)
	UIEventListener.Get(self.Setting_Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSetting_BtnClick()
	end)

    --@endregion EventBind end
	self.m_CameraMoveArea = self.transform:Find("CameraMoveArea").gameObject
	UIEventListener.Get(self.m_CameraMoveArea.gameObject).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
	    LuaDaFuWongMgr:OnDragCamera(delta)
	end)
	self.m_CameraMoveArea.gameObject:SetActive(false)
	self.m_ItemChangeEndPosX = -150
	self.m_JunWangLingTick = {}	-- 郡王令动效Tick
	self.m_playerView = nil
	self.m_landView = nil	-- 地块
	self.m_itemView = nil	-- 秘宝
	self.m_CardFxView = nil
	self.m_goodsView = nil	-- 货物
	self.m_isShowRightTop = false
	self.m_isShowRightTopTween = false
	self.m_TouZiTick = false
	self.m_ItemChangeWnd = self.transform:Find("ShowArea/DaFuWongItemChangeWnd")
	self.m_ItemChangeWnd.gameObject:SetActive(false)
	self.m_curSelectPlayerView = 0
	self.m_SelectItem = 0
	self.m_ItemTablePaddingX = {0,-22,-40} -- 道具个数为<3,4,5时分别对应的tablePaddingX值
	self.m_ItemCUIFxList = {}
	local card = self.transform:Find("ShowArea/Card")
	self.m_ItemCUIFxList[2] = card.transform:Find("CUIFxshifang"):GetComponent(typeof(CUIFx))
	self.m_ItemCUIFxList[1] = card.transform:Find("CUIFxxishou"):GetComponent(typeof(CUIFx))
	self.m_ItemCUIFxList[3] = card.transform:Find("CUIFxtuowei"):GetComponent(typeof(CUIFx))
	self.m_TuoWeiOriLocation = self.m_ItemCUIFxList[3].transform.localPosition
	self.m_ModelTextureLoader = nil
	self.m_CanRollTouZi = false
	self.m_TouZiRo = nil
	self.m_MaxCardNum = DaFuWeng_Setting.GetData().MaxCardNum
	self.m_GetCardFx = self.transform:Find("Panel/xinzeng")
	self.m_SeriesBorderName = "common_itemcell_01_border"
	self.m_StartFx = self.transform:Find("ShowArea/StartFx").gameObject
	self.m_RoundHasShowFx = false

end

function LuaDaFuWongMainPlayWnd:Init()
	self.transform:Find("ShowArea/Template").gameObject:SetActive(false)
	self.Touzi.gameObject:SetActive(true)
	self.Touzi:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
	self.m_CameraMoveArea.gameObject:SetActive(LuaDaFuWongMgr.CanMoveCameraStage and LuaDaFuWongMgr.CanMoveCameraStage[LuaDaFuWongMgr.CurStage])
	self:InitPlayerList()
	self:InitLandAndGoods()
	self:InitTouZi()
	self:InitItemList()
	self:InitLoudSpeak()
	self:OnPlayerRound(LuaDaFuWongMgr.CurRoundPlayer)
	self.CoundDown.text = nil
	self:ShowRightTopArea()
	self.transform:Find("ShowArea/MiniMap"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init()
	if CClientMainPlayer.Inst then
		local id = CClientMainPlayer.Inst.Id 
		if id == LuaDaFuWongMgr.CurRoundPlayer then
			if LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RoundCard then
				self:ShowTouZi()
			elseif  LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RoundDice then
				if LuaDaFuWongMgr.RoundInfo then
					self:ShowTouZiAni(LuaDaFuWongMgr.RoundInfo.diceResult)
				end
			end
		end
	end
	self.ExtraLabel.gameObject:SetActive(false)
	self.RoundCount.text = LuaDaFuWongMgr.CurRound and LuaDaFuWongMgr.CurRound.round or 0
	CUIManager.SetUIVisibility(CLuaUIResources.DaFuWongMainPlayWnd, LuaDaFuWongMgr.CurStage and LuaDaFuWongMgr.CurStage ~= EnumDaFuWengStage.GameStart and LuaDaFuWongMgr.CurStage ~= EnumDaFuWengStage.GameEnd, "Dafuweng")
	self:UpdataRoundView()
	self:UpdateJiaoyihangBtnRedDot()
end

-- 左上角玩家
function LuaDaFuWongMainPlayWnd:InitPlayerList()
	if not LuaDaFuWongMgr.PlayerInfo then return end
	if not self.m_playerView then 
		self.m_playerView = {} 
		Extensions.RemoveAllChildren(self.PlayerInfoTable.transform)
	end
	local order = LuaDaFuWongMgr.Index2Id
	for k,v in pairs(order) do
		local id = v
		if not self.m_playerView[id] then 
			local go = CUICommonDef.AddChild(self.PlayerInfoTable.gameObject,self.PlayerTemplate)
			self.m_playerView[id] = go
			go.gameObject:SetActive(true)
			go.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitData(id,LuaDaFuWongMgr.PlayerInfo[id])
		end
	end
	self.PlayerInfoTable:GetComponent(typeof(UITable)):Reposition()
	self:OnPlayerRound(LuaDaFuWongMgr.CurRoundPlayer)
end

-- 右上角房产货物资产
function LuaDaFuWongMainPlayWnd:InitLandAndGoods()
	Extensions.RemoveAllChildren(self.HouseTable.transform)
	Extensions.RemoveAllChildren(self.ItemTable.transform)
	self.HouseTable.gameObject:SetActive(false)
	self.ItemTable.gameObject:SetActive(false)
	self.NoHouseLabel.gameObject:SetActive(true)
	self.NoItemLabel.gameObject:SetActive(true)
	self.MoneyLabel.text = nil
	self.TotalMoneyLabel.text = nil
	self.m_itemView = {}
	self.m_CardFxView = {}
	self.m_landView = {}
	self:UpdateLands()
	self:UpdateGoods()
	self:UpdataMoney()
	
	self.Table:Reposition()
end
function LuaDaFuWongMainPlayWnd:UpdateLands()
	if not CClientMainPlayer.Inst then return end
	local id = CClientMainPlayer.Inst.Id
	
	local data = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id].LandsInfo
	if not data then
		self.HouseTable.gameObject:SetActive(false)
		self.NoHouseLabel.gameObject:SetActive(true)
		self.HouseTable:Reposition()
		self.Table:Reposition()
		return
	end
	-- 同系列连在一起，LandsInfo先排好序
	local LandsInfo = {}
	for k,v in pairs(data) do
		if v then table.insert(LandsInfo,k) end
	end
	if #LandsInfo <= 0 then
		self.HouseTable.gameObject:SetActive(false)
		self.NoHouseLabel.gameObject:SetActive(true)
		self.HouseTable:Reposition()
		self.Table:Reposition()
		return
	end
	table.sort(LandsInfo,function(a,b)
		local series1 = LuaDaFuWongMgr.LandInfo[a].series
		local series2 = LuaDaFuWongMgr.LandInfo[b].series
		return series1 < series2
	end)
	self.HouseTable.gameObject:SetActive(true)
	self.NoHouseLabel.gameObject:SetActive(false)
	local tableChildCount = self.HouseTable.transform.childCount
	for i = math.max(#LandsInfo,tableChildCount),1,-1 do
		if i > #LandsInfo and i <= tableChildCount then
			local go = self.HouseTable.transform:GetChild(i - 1)
			if go then self.Pool:Recycle(go.gameObject) end
			--self.m_landView[i] = nil
		else
			--local go = nil
			if i > tableChildCount then
				local go = self.Pool:GetFromPool(0)
				go.transform.parent = self.HouseTable.transform
				go.transform.localScale = Vector3.one
				go.gameObject:SetActive(true)
				--self.m_landView[i] = go.gameObject
			-- else
			-- 	go = self.HouseTable.transform:GetChild(i - 1).gameObject
			end
			--self:InitOneLandCell(go,LandsInfo[i])
		end
	end
	self.HouseTable:Reposition()
	self.Table:Reposition()
	tableChildCount = self.HouseTable.transform.childCount
	for i = 1 ,tableChildCount do
		local go = self.HouseTable.transform:GetChild(i - 1).gameObject
		self:InitOneLandCell(go,LandsInfo[i])
		--self.m_landView[i] = go
	end
end
function LuaDaFuWongMainPlayWnd:InitOneLandCell(go,id)
	local landData = DaFuWeng_Land.GetData(id)
	if not landData or not LuaDaFuWongMgr.LandInfo or not LuaDaFuWongMgr.LandInfo[id] or not go then return end
	local curdata = LuaDaFuWongMgr.LandInfo[id]
	go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(landData.Icon)
	go.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = landData.Name
	go.transform:Find("Cost"):GetComponent(typeof(UILabel)).text = curdata.curCost
	local upOrdown = go.transform:Find("UpOrDown"):GetComponent(typeof(UISprite))
	if curdata.cost[curdata.lv - 1] == curdata.curCost then
		upOrdown.gameObject:SetActive(false)
	else
		upOrdown.gameObject:SetActive(true)
		upOrdown.spriteName = curdata.cost[curdata.lv - 1] < curdata.curCost and "common_arrow_07_red" or "common_arrow_07_green"
	end
	-- 系列背景
	local typeBg = go.transform:Find("TypeBg"):GetComponent(typeof(UITexture))
	typeBg.gameObject:SetActive(false)
	local QualitySprite = go.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local borderName = LuaDaFuWongMgr.SeriesInfo[curdata.series] and LuaDaFuWongMgr.SeriesInfo[curdata.series].border
	if not borderName then 
		QualitySprite.spriteName = self.m_SeriesBorderName
	else
		QualitySprite.spriteName = borderName
	end
	UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaDaFuWongMgr:OnShowDetailLandWnd(id)
	end)
end

function LuaDaFuWongMainPlayWnd:UpdateGoods()
	if not CClientMainPlayer.Inst then return end
	local id = CClientMainPlayer.Inst.Id
	local data = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id].GoodsInfo
	if not data then
		self.ItemTable.gameObject:SetActive(false)
		self.NoItemLabel.gameObject:SetActive(true)
		self.ItemTable:Reposition()
		self.Table:Reposition()
		return 
	end
	local GoodsInfo = {}
	for k,v in pairs(data) do
		if v then table.insert(GoodsInfo,v) end
	end
	if #GoodsInfo <= 0 then 
		self.ItemTable.gameObject:SetActive(false)
		self.NoItemLabel.gameObject:SetActive(true)
		self.ItemTable:Reposition()
		self.Table:Reposition()
		return 
	end
	self.ItemTable.gameObject:SetActive(true)
	self.NoItemLabel.gameObject:SetActive(false)
	local tableChildCount = self.ItemTable.transform.childCount
	for i = math.max(#GoodsInfo,tableChildCount),1,-1 do
		if i > #GoodsInfo and i <= tableChildCount then
			local go = self.ItemTable.transform:GetChild(i - 1).gameObject
			if go then
				self.Pool:Recycle(go.gameObject)
				self.m_goodsView = nil
			end
		else
			local go = nil
			if i > tableChildCount then
				go = self.Pool:GetFromPool(0)
				go.transform.parent = self.ItemTable.transform
				go.transform.localScale = Vector3.one
				go.gameObject:SetActive(true)
				self.m_goodsView = go.gameObject
			elseif i <= tableChildCount then
				go = self.ItemTable.transform:GetChild(i - 1).gameObject
			end
			self:InitOneGoodsCell(go,GoodsInfo[#GoodsInfo - i + 1])
		end
	end
	self.ItemTable:Reposition()
	self.Table:Reposition()
end

function LuaDaFuWongMainPlayWnd:InitOneGoodsCell(go,data)
	local goodsData = DaFuWeng_Goods.GetData(data.goodsId)
	if not goodsData or not go then return end
	local priceInfo = LuaDaFuWongMgr:GetGoodsCurPriceWithId(data.goodsId)
	go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(goodsData.Icon)
	go.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = goodsData.Name
	go.transform:Find("Cost"):GetComponent(typeof(UILabel)).text = priceInfo.price
	local upOrdown =  go.transform:Find("UpOrDown"):GetComponent(typeof(UISprite))
	upOrdown.gameObject:SetActive(true)
	if math.floor(math.abs(priceInfo.changeValue * 100)) == 0 then
		upOrdown.gameObject:SetActive(false)
	else
		upOrdown.gameObject:SetActive(true)
		upOrdown.spriteName = priceInfo.changeValue > 0 and "common_arrow_07_red" or "common_arrow_07_green"
	end
	go.transform:Find("TypeBg").gameObject:SetActive(false)
	local QualitySprite = go.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	QualitySprite.spriteName = self.m_SeriesBorderName
	UIEventListener.Get(go.gameObject).onClick = nil
end
function LuaDaFuWongMainPlayWnd:UpdataMoney()
	if not CClientMainPlayer.Inst then return end
	local id = CClientMainPlayer.Inst.Id
	if not LuaDaFuWongMgr.PlayerInfo or not LuaDaFuWongMgr.PlayerInfo[id]  then return end
	if LuaDaFuWongMgr.PlayerInfo[id].Money then
		self.MoneyLabel.text = LuaDaFuWongMgr.PlayerInfo[id].Money
	end
	if LuaDaFuWongMgr.PlayerInfo[id].TotalMoney then
		self.TotalMoneyLabel.text = LuaDaFuWongMgr.PlayerInfo[id].TotalMoney
	end
end
-- 倒计时
function LuaDaFuWongMainPlayWnd:UpdateCountDown()
	if not self.CoundDown then return end
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.CoundDown.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.CoundDown.text = nil
        end
    else
        self.CoundDown.text = nil
    end
end
-- 世界事件通报
function LuaDaFuWongMainPlayWnd:InitLoudSpeak()
	self.LoudSpeak.gameObject:SetActive(false)
	self.m_StartFx:SetActive(false)
end
function LuaDaFuWongMainPlayWnd:ShowWorldEvent()
	local EventInfo = LuaDaFuWongMgr.EventInfo
	if not EventInfo then return end
	local id = EventInfo[1]
	if not EventInfo.Data then
		EventInfo.Data = DaFuWeng_WorldEvent.GetData(id)
	end
	local text = ""
	if not EventInfo.Data then
		text = g_MessageMgr:FormatMessage("DaFuWeng_No_WorldEvet")
	else
		text = EventInfo.Data.Describe
	end
	LuaTweenUtils.DOKill(self.LoudSpeakLabel.transform, false)
	self.LoudSpeak.gameObject:SetActive(true)
	self.LoudSpeakLabel.text = text
	self.LoudSpeak:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    local speed = 70
    local textLabel = self.LoudSpeakLabel
    local clip = self.LoudSpeak.transform:Find("LoudSpeakPanel"):GetComponent(typeof(UIPanel))
    
	textLabel:UpdateNGUIText()
	NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(textLabel.text)
	textLabel.width = math.ceil(size.x)

	local duration = (textLabel.localSize.x + clip.baseClipRegion.z) / speed
	local startPos = clip.baseClipRegion.x + clip.baseClipRegion.z * 0.5
	local endPos = clip.baseClipRegion.x - clip.baseClipRegion.z * 0.5 - textLabel.localSize.x

	Extensions.SetLocalPositionX(textLabel.transform, startPos)

	local tween = LuaTweenUtils.DOLocalMoveX(textLabel.transform, endPos, duration, true)

	local func = DelegateFactory.TweenCallback(function () 
		self.LoudSpeak.gameObject:SetActive(false)
	end)

	CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(tween,Ease.Linear), func)
end

-- 骰子
function LuaDaFuWongMainPlayWnd:InitTouZi()
	local touzipath = "assets/res/character/npc/lnpc999/prefab/lnpc999_01.prefab"
	if not self.m_TouZiRo then
		self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function(ro)
			self.m_TouZiRo = ro
			ro:LoadMain(touzipath)
			ro:DoAni("stand01", false, 0, 1, 0, true, 1)
		end)
	else
		self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Load(self.m_TouZiRo)
		self.m_TouZiRo:DoAni("stand01", false, 0, 1, 0, true, 1)
	end
	
	local texture = self.Touzi.transform:Find("FakeModel/Texture"):GetComponent(typeof(UITexture))
	texture.mainTexture = CUIManager.CreateModelTexture("__DaFuWongTouZi__",self.m_ModelTextureLoader,0,0,0,3,false,true,0.8)
	UIEventListener.Get(self.Touzi.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClickTouzi()
	end)
	self:ResetTouZi()
end
function LuaDaFuWongMainPlayWnd:ResetTouZi()
	-- 重置骰子位置
	if self.m_TouZiRo then
		self.m_TouZiRo.Direction = 0
		self.m_TouZiRo:SetRotationXZ(0,0)
		self.m_TouZiRo:DoAni("stand01", true, 0, 1, 0, true, 1)
	end
	self.Touzi.transform:Find("BG").gameObject:SetActive(true)
	local model = self.Touzi.transform:Find("FakeModel")
	LuaUtils.SetLocalPosition(model.transform,-7,0,0)
	LuaUtils.SetLocalScale(model.transform,1,1,1)
	self.m_CanRollTouZi = false
	self.Touzi.gameObject:SetActive(false)
	if self.m_TouZiTick then UnRegisterTick(self.m_TouZiTick) self.m_TouZiTick = nil end
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.MianDice)
end
function LuaDaFuWongMainPlayWnd:ShowTouZi()
	self:ResetTouZi()
	self:ShowPlayerRoundFx()
	self.Touzi.gameObject:SetActive(true)
	self.Touzi.transform:Find("CountDown").gameObject:SetActive(true)
	self.Touzi:GetComponent(typeof(QnButton)).enableAnimation = true
	self.Touzi:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
	-- 倒计时Tick
	if LuaDaFuWongMgr.IsMyRound then self.m_CanRollTouZi = true end
	local countDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundCard).Time
	local endFunc = function() end
	local durationFunc = function(time) 
		if self.Touzi then 
			self.Touzi.transform:Find("CountDown"):GetComponent(typeof(UILabel)).text = time
			self.Touzi.transform:Find("BG/Texture").gameObject:SetActive(time <= DaFuWeng_Setting.GetData().ShowTouZiUseHightLight)
		end
	end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.MianDice,countDown,durationFunc,endFunc)
end

function LuaDaFuWongMainPlayWnd:ShowTouZiAni(dot)
	if not dot then return end
	dot = math.max(math.min(dot,6),1)
	self:ResetTouZi()
	self.m_StartFx:SetActive(false)
	self.Touzi.gameObject:SetActive(true)
	self.Touzi.transform:Find("CountDown").gameObject:SetActive(false)
	self.Touzi.transform:Find("BG/Texture").gameObject:SetActive(false)
	self.Touzi:GetComponent(typeof(QnButton)).enableAnimation = false
	self.Touzi:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
	SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Dice", Vector3.zero, nil, 0)
	LuaUtils.SetLocalScale(self.Touzi.transform,1,1,1)
	local model = self.Touzi.transform:Find("FakeModel")
	local pos = Vector3(0,0,0)
	local point = self.m_ItemChangeWnd.transform:TransformPoint(Vector3(pos.x,pos.y,pos.z))
	local localTarget = self.Touzi.transform:InverseTransformPoint(point)
	local middlePath = Vector3(localTarget.x + 500,localTarget.y + 300,localTarget.z)
	local path = self:CalculateCubicBezierPos(3, Vector3(-7,0,0), localTarget, middlePath)
	--local tween = LuaTweenUtils.TweenPosition(model.transform, localTarget.x, localTarget.y, localTarget.z, 0.8)
	local tween = LuaTweenUtils.DOLuaLocalPathOnce(model.transform, Table2Array(path, MakeArrayClass(Vector3)),0.9,PathType.CatmullRom, PathMode.Sidescroller2D, 10,Ease.InOutQuint)
	if self.m_TouZiRo then
		self.m_TouZiTick = RegisterTickOnce(function ()
			self.m_TouZiRo:DoAni("selfrotate", true, 0, 1, 0.1, true, 1)
			local scaleTween =  LuaTweenUtils.TweenScaleTo(model.transform,Vector3(2,2,1),1.5)
		end ,200)
	end
	local RolAni = function()
		self.Touzi.transform:Find("BG").gameObject:SetActive(false)
		local rotation = {}
		rotation[1] = {60,-7,0}
		rotation[2] = {0,50,0}
		rotation[3] = {-30,-50,0}
		rotation[4] = {-30,130,0}
		rotation[5] = {-5,-135,0}
		rotation[6] = {50,140,-35}
		-- 40,-55,-60  / -25,-55,15 / 
		local timeLen = 1.2
		local interval = 0.1
		local times = math.floor(timeLen/interval)
		local deltaRotation = {rotation[dot][1] / (times),rotation[dot][2] / (times),rotation[dot][3] / (times) }
		local curtime = 1
		if self.m_TouZiRo then	
			if self.m_TouZiTick then UnRegisterTick(self.m_TouZiTick) self.m_TouZiTick = nil end		
			self.m_TouZiTick = RegisterTickWithDuration(function()
				if curtime >= times then
					self.m_TouZiRo.Direction = rotation[dot][2]
					self.m_TouZiRo:SetRotationXZ(rotation[dot][1],rotation[dot][3])
					self.m_TouZiRo:DoAni("stand01", true, 0, 1, 0.1, true, 1)
					return
				end

				if self.m_TouZiRo then
					self.m_TouZiRo.Direction = self:GetResultval(deltaRotation[2] * curtime,rotation[dot][2])
					self.m_TouZiRo:SetRotationXZ(self:GetResultval(deltaRotation[1] * curtime,rotation[dot][1]),self:GetResultval(deltaRotation[3] * curtime,rotation[dot][3]))
				end
				curtime = curtime + 1
			end,interval * 1000,timeLen * 1000)
		end
	end
	LuaTweenUtils.OnComplete(tween, RolAni)
end
function LuaDaFuWongMainPlayWnd:GetResultval(data,totaldata)
	if totaldata > 0 then
		return math.min(data,totaldata)
	elseif totaldata < 0 then
		return math.max(data,totaldata)
	else
		return data
	end
end

function LuaDaFuWongMainPlayWnd:CalculateCubicBezierPos(pointnum, startPos, endPos, tangentPos)
	local respos = {}
	for i = 1,pointnum do
		respos[i] = Vector3(self:CalculateCubicBezierValue(i / pointnum, startPos.x, endPos.x, tangentPos.x),
                    self:CalculateCubicBezierValue(i / pointnum, startPos.y, endPos.y, tangentPos.y),
                    self:CalculateCubicBezierValue(i / pointnum, startPos.z, endPos.z, tangentPos.z))
	end
    return respos
end
function LuaDaFuWongMainPlayWnd:CalculateCubicBezierValue(t, startVal, endVal, tangentVal)
    return (1 - t) * (1 - t) * startVal + 2 * t * (1 - t) * tangentVal + t * t * endVal
end

function LuaDaFuWongMainPlayWnd:OnClickTouzi()
	if not self.m_CanRollTouZi then return end--g_MessageMgr:ShowMessage("DaFuWeng_Cannot_ThrowDice") return end
	Gac2Gas.DaFuWengThrowDice()
end
-- 左下角道具
function LuaDaFuWongMainPlayWnd:InitItemList()
	for i = 1,#self.m_ItemCUIFxList do
		self.m_ItemCUIFxList[i].gameObject:SetActive(false)
	end
	local cardFxTable = self.transform:Find("Panel/xinzeng/kgd"):GetComponent(typeof(UITable))
	Extensions.RemoveAllChildren(cardFxTable.transform)
	Extensions.RemoveAllChildren(self.CardTable.transform)
	self.m_itemView = {}
	self.m_CardFxView = {}
	local index = 1
	for i = 1,self.m_MaxCardNum do
		local go = CUICommonDef.AddChild(self.CardTable.gameObject,self.CardTemplate)
		local highlight = go.transform:Find("HighLight").gameObject
		local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
		local nameLabel = go.transform:Find("Name"):GetComponent(typeof(UILabel))
		go.transform:Find("CUIFxshifang").gameObject:SetActive(false)
		local fxGo = go.transform:Find("CUIFxshuaxiao").gameObject
		fxGo.gameObject:SetActive(false)
		go.gameObject:SetActive(true)
		highlight:SetActive(true)
		self:UpdateItemDepth(go,index)
		index = index + 1
		self.m_itemView[i] = {id = 0,go = go,highlight = highlight, icon = icon,name = nameLabel, fxGo = fxGo}
		UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnClickItem(i,true)
			self:ShowCardFx(false)
		end)
	end
	--index = 1
	for i = 1,self.m_MaxCardNum do
		local Fxgo = CUICommonDef.AddChild(cardFxTable.gameObject,self.CardTemplate)
		local height = Fxgo.transform:Find("HighLight")
		height.gameObject:SetActive(true)
		local FxGoIcon = Fxgo.transform:Find("Icon"):GetComponent(typeof(CUITexture))
		local FxGoNameLabel = Fxgo.transform:Find("Name"):GetComponent(typeof(UILabel))
		Fxgo.transform:Find("CUIFxshifang").gameObject:SetActive(false)
		--self:UpdateItemDepth(Fxgo,index)
		--index = index + 1
		self.m_CardFxView[i] = {id = 0,go = Fxgo, icon = FxGoIcon,name = FxGoNameLabel}
		height.gameObject:SetActive(false)
	end
	cardFxTable:Reposition()
	self.m_GetCardFx.gameObject:SetActive(false)
	self.CardTable:GetComponent(typeof(UITable)):Reposition()
	self:UpdateItemListView()
end
function LuaDaFuWongMainPlayWnd:UpdateItemDepth(go,index)
	local depth = {0,7,3,4,13,14,9}
	local widgetList = CommonDefs.GetComponentsInChildren_Component_Type(go.transform,typeof(UIWidget))
    for i = 0,widgetList.Length - 1 do
        widgetList[i].depth = depth[i + 1] + (index - 1) * 15
    end
end
function LuaDaFuWongMainPlayWnd:UpdateItemListView()
	if not CClientMainPlayer.Inst then return end
	local id = CClientMainPlayer.Inst.Id
	local itemsList = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[id].CardInfo
	if not itemsList then 
		for k,v in pairs(self.m_itemView) do
			v.go.gameObject:SetActive(false)
			v.highlight.gameObject:SetActive(false)
		end
		return 
	end
	
	local data = {}
	for k,v in pairs(itemsList) do
		if v and v >= 1 then
			for i = 1,v do
				table.insert(data,k)
			end
		end
	end
	for i = #self.m_itemView,1,-1 do
		if data[i] then
			local cardInfo = DaFuWeng_Card.GetData(data[i])
			self.m_itemView[i].id = data[i]
			self.m_itemView[i].go.gameObject:SetActive(true)
			self.m_itemView[i].highlight.gameObject:SetActive(false)
			self.m_itemView[i].icon:LoadMaterial(cardInfo.Icon)
			self.m_itemView[i].name.text = LocalString.StrH2V(cardInfo.Name,true)
			LuaUtils.SetLocalScale(self.m_itemView[i].go.transform,1,1,1)
			LuaUtils.SetLocalPositionY(self.m_itemView[i].go.transform,7.8)
		else
			self.m_itemView[i].go.gameObject:SetActive(false)
		end
	end

	local table = self.CardTable:GetComponent(typeof(UITable))
	if #data <= 3 then
		table.padding = Vector2(self.m_ItemTablePaddingX[1],table.padding.y)
	elseif #data == 4 then
		table.padding = Vector2(self.m_ItemTablePaddingX[2],table.padding.y)
	else
		table.padding = Vector2(self.m_ItemTablePaddingX[3],table.padding.y)
	end
	table:Reposition()
	self:ShowCardFx(true)
	self:OnClickItem(self.m_SelectItem,false)
end

function LuaDaFuWongMainPlayWnd:OnClickItem(id,showWnd)
	local view = self.m_itemView and self.m_itemView[id]
	if not view then return end
	if self.m_SelectItem ~= id or id ~= 0 then
		local lastView = self.m_itemView[self.m_SelectItem]
		if lastView then 
			lastView.highlight.gameObject:SetActive(false)
			LuaUtils.SetLocalScale(lastView.go.transform,1,1,1)
			LuaUtils.SetLocalPositionY(lastView.go.transform,7.8)
		end
		view.highlight.gameObject:SetActive(true)
		view.fxGo.gameObject:SetActive(false)
		LuaUtils.SetLocalScale(view.go.transform,1.2,1.2,1.2)
		LuaUtils.SetLocalPositionY(view.go.transform,35)
		self.m_SelectItem = id
	end
	if showWnd then LuaDaFuWongMgr:OnShowItemDetailWnd(view.id) end
end

function LuaDaFuWongMainPlayWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaDaFuWongMainPlayWnd:ShowRightTopArea()
	local height = self.Top.transform:Find("Panel"):GetComponent(typeof(UIPanel)).baseClipRegion.w
	local posYtop = self.m_isShowRightTop and 0 or height
	local posYBg = self.m_isShowRightTop and  50 or (50 + height)
	self.m_isShowRightTopTween = true
	local moveitem = self.Top.transform:Find("Panel/MoveItem")
	local bg = self.Top.transform:Find("bg")
	local tween1 = LuaTweenUtils.TweenPositionY(moveitem.transform,posYtop,0.5)
	LuaTweenUtils.TweenPositionY(bg.transform,posYBg,0.5)
	--local tween2 = LuaTweenUtils.TweenPositionY(self.Bottom.transform,posYbot,0.5)
	
	LuaTweenUtils.OnComplete(tween1, function ()
		self.m_isShowRightTopTween = false
		self.ExpandButton.transform.localEulerAngles = self.m_isShowRightTop and Vector3(0, 0, 0) or Vector3(0, 0, 180)
		self.Bottom.transform:Find("Texture").gameObject:SetActive(self.m_isShowRightTop)
	end)
end

function LuaDaFuWongMainPlayWnd:ShowPlayerRoundFx()
	if self.m_RoundHasShowFx then return end
	self.m_StartFx:SetActive(true)
	self.transform:Find("ShowArea"):GetComponent(typeof(Animation)):Play("dafuwongmainplaywnd_startfx")
	local glow = self.m_StartFx.transform:Find("vfx/glow")
	LuaUtils.SetLocalPosition(glow.transform,43,7,0)
	local targetPos = self.Touzi.transform.localPosition
	local tween = LuaTweenUtils.SetEase(LuaTweenUtils.TweenPosition(glow.transform,targetPos.x,targetPos.y,targetPos.z,0.3), Ease.OutCubic)
	LuaTweenUtils.SetDelay(tween,1)
	self.m_RoundHasShowFx = true
end

function LuaDaFuWongMainPlayWnd:OnPlayerRound(playerId)
	local curView = self.m_playerView and self.m_playerView[self.m_curSelectPlayerView]
	if curView then
		curView.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:ResetHightLight()
	end
	if playerId == 0 then return end
	local go = self.m_playerView and self.m_playerView[playerId]
	if go then
		self.m_curSelectPlayerView = playerId
		go.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:SetHightLight()
	end
	self:ResetTouZi()
	self.m_RoundHasShowFx = false
	self.PlayerInfoTable:GetComponent(typeof(UITable)):Reposition()
	--self:OnClickItem(0)
end
function LuaDaFuWongMainPlayWnd:OnPlayerInfoChange(playerId)
	if not self.m_playerView or not self.m_playerView[playerId] then 
		self:InitPlayerList()
	else
		local data = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[playerId]
		local go = self.m_playerView and self.m_playerView[playerId]
		if data and go then
			go.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitData(playerId,data)
		end
	end
	
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
		self:UpdataMoney()
	end
	self.PlayerInfoTable:GetComponent(typeof(UITable)):Reposition()
end
--@region UIEvent

function LuaDaFuWongMainPlayWnd:OnLeaveButtonClick()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("DaFuWeng_LeaveGamePlay_Confirm"), function ()
		Gac2Gas.RequestLeavePlay()
	end,nil, nil, nil, false)
end

function LuaDaFuWongMainPlayWnd:OnExpandButtonClick()
	if not self.m_isShowRightTopTween then
		self.m_isShowRightTop =  not self.m_isShowRightTop
		self:ShowRightTopArea()
	end
end

function LuaDaFuWongMainPlayWnd:ItemExchage(newItemList)
	self:ClearTick()
	SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Exchange", Vector3.zero, nil, 0)
	local list = LuaDaFuWongMgr.ItemExchageTimeList
	local timeList = {list[1],list[2],list[3],list[4]}
	local cardtable = self.CardTable:GetComponent(typeof(UITable))
	local count = 0
	local endCount = 0
	local needShowMyCard = false
	if CClientMainPlayer.Inst then
		local MyId = CClientMainPlayer.Inst.Id
		needShowMyCard = (MyId == LuaDaFuWongMgr.CurAniInfo.playerId or MyId == LuaDaFuWongMgr.CurAniInfo.selectPlayerId)
	end
	local data = {}
	for k,v in pairs(newItemList) do
		if v and v >= 1 then
			for i = 1,v do
				table.insert(data,k)
			end
		end
	end
	endCount = #data
	if needShowMyCard then
		-- Table收起
		for i = #self.m_itemView,1,-1 do
			if self.m_itemView[i].go.activeSelf then
				count = count + 1
				LuaTweenUtils.TweenPositionX(self.m_itemView[i].go.transform,self.m_ItemChangeEndPosX,timeList[1])
			end
		end
	end
	-- table渐隐
	local func1 = function()
		if needShowMyCard then
			if count > 0 then
				self.m_ItemCUIFxList[1].gameObject:SetActive(true)
			end
			local widght = self.CardTable.transform:GetComponent(typeof(UIWidget))
			LuaTweenUtils.TweenAlpha(widght,1,0,timeList[2])
		end
		self.m_ItemChangeWnd.gameObject:SetActive(true)
		self.m_ItemChangeWnd:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init()
	end

	-- 数据更新,连线动效
	local func2 = function()
		for i = #self.m_itemView,1,-1 do
			if data[i] then
				local cardInfo = DaFuWeng_Card.GetData(data[i])
				self.m_itemView[i].go.gameObject:SetActive(true)
				self.m_itemView[i].id = data[i]
				self.m_itemView[i].highlight.gameObject:SetActive(false)
				self.m_itemView[i].icon:LoadMaterial(cardInfo.Icon)
				self.m_itemView[i].name.text = LocalString.StrH2V(cardInfo.Name,true)
			else
				self.m_itemView[i].id = 0
				self.m_itemView[i].go.gameObject:SetActive(false)
			end
		end
		self.CardTable.transform:GetComponent(typeof(UITable)):Reposition()
		if count > 0 then
			local pos = LuaDaFuWongMgr.ItemExchagePos
			local moveItem = self.m_ItemCUIFxList[3].gameObject
			-- 连线Fx
			LuaUtils.SetLocalPosition(moveItem.transform,self.m_TuoWeiOriLocation.x,self.m_TuoWeiOriLocation.y,self.m_TuoWeiOriLocation.z)
			moveItem:SetActive(true)
			local point = self.m_ItemChangeWnd.transform:TransformPoint(pos)
			local localTarget = self.CardTable.transform:InverseTransformPoint(point)
			LuaTweenUtils.TweenPosition(moveItem.transform, localTarget.x, localTarget.y, localTarget.z, timeList[3])
		end
		
	end
	local pos = {}
	-- 连线特效隐去
	local func3 = function()
		self.m_ItemCUIFxList[3].gameObject:SetActive(false)
		for i = #self.m_itemView,1,-1 do
			if self.m_itemView[i].id ~= 0 then
				self.m_itemView[i].go.gameObject:SetActive(true)
				pos[i] = self.m_itemView[i].go.transform.localPosition.x
			else
				self.m_itemView[i].go.gameObject:SetActive(false)
			end
			Extensions.SetLocalPositionX(self.m_itemView[i].go.transform, self.m_ItemChangeEndPosX)
		end
	end
	local func4 = function()
		self:EndItemExchange(endCount,pos)
	end

	-- 根据数量设置Table的PaddingX
	if endCount <= 3 then
		cardtable.padding = Vector2(self.m_ItemTablePaddingX[1],cardtable.padding.y)
	elseif endCount == 4 then
		cardtable.padding = Vector2(self.m_ItemTablePaddingX[2],cardtable.padding.y)
	else
		cardtable.padding = Vector2(self.m_ItemTablePaddingX[3],cardtable.padding.y)
	end

	self.m_JunWangLingTick[1] = RegisterTickOnce(func1,timeList[1] * 1000)
	if needShowMyCard then
		self.m_JunWangLingTick[2] = RegisterTickOnce(func2,(timeList[1] + timeList[2]) * 1000)
		self.m_JunWangLingTick[3] = RegisterTickOnce(func3,(timeList[1] + timeList[2] + timeList[3]) * 1000)
		self.m_JunWangLingTick[4] = RegisterTickOnce(func4,(timeList[1] + timeList[2] + timeList[3] + timeList[4]) * 1000)
	end
end
function LuaDaFuWongMainPlayWnd:EndItemExchange(itemCount,pos)
	local count = 1
	local list = LuaDaFuWongMgr.ItemExchageTimeList
	local timeList = {list[5],list[6],list[7],list[8]}
	self.m_ItemCUIFxList[1].gameObject:SetActive(false)
	local table = self.CardTable:GetComponent(typeof(UITable))
	local widght = table.transform:GetComponent(typeof(UIWidget))
	-- 连线效果,显示特效
	if itemCount > 0 then
		local moveItem = self.m_ItemCUIFxList[3].gameObject
		moveItem:SetActive(true)
		local localTarget = self.m_TuoWeiOriLocation
		local pos = LuaDaFuWongMgr.ItemExchagePos
		-- 连线Fx
		local point = self.m_ItemChangeWnd.transform:TransformPoint(pos)
		local localOriPos = self.CardTable.transform:InverseTransformPoint(point)
		LuaUtils.SetLocalPosition(moveItem.transform,localOriPos.x,localOriPos.y,localOriPos.z)
		moveItem:SetActive(true)
		LuaTweenUtils.TweenPosition(moveItem.transform, localTarget.x, localTarget.y, localTarget.z, timeList[1])
	end
	-- 连线隐去，特效显示
	local func1 = function()
		if itemCount > 0 then
			self.m_ItemCUIFxList[3].gameObject:SetActive(false)
			self.m_ItemCUIFxList[2].gameObject:SetActive(true)
		end
	end
	-- table渐显
	local func2 = function()
		LuaTweenUtils.TweenAlpha(widght,0,1,timeList[3])
	end
	-- table展开
	local func3 = function()
		for i = #self.m_itemView,1,-1 do
			if self.m_itemView[i].go.activeSelf then
				LuaTweenUtils.TweenPositionX(self.m_itemView[i].go.transform,pos[i],timeList[4])
				count = count + 1
			end
		end
	end
	-- tableReposition
	local func4 = function()
		self.m_ItemCUIFxList[2].gameObject:SetActive(false)
		self.CardTable.transform:GetComponent(typeof(UITable)):Reposition()
	end
	self.m_JunWangLingTick[5] = RegisterTickOnce(func1,timeList[1] * 1000)
	self.m_JunWangLingTick[6] = RegisterTickOnce(func2,(timeList[1] + timeList[2]) * 1000)
	self.m_JunWangLingTick[7] = RegisterTickOnce(func3,(timeList[1] + timeList[2] + timeList[3]) * 1000)
	self.m_JunWangLingTick[8] = RegisterTickOnce(func4,(timeList[1] + timeList[2] + timeList[3] + timeList[4]) * 1000)
end

function LuaDaFuWongMainPlayWnd:GetItemTablePositionX(index)
	-- local paddingX = self.CardTable:GetComponent(typeof(UITable)).padding.x
	-- local cellX = self.CardTemplate.transform:Find("Sprite"):GetComponent(typeof(UIWidget)).width
	-- local oriPos = cellX / 2 + paddingX - 199.5
	-- if index == 1 then
	-- 	return oriPos 
	-- else
	-- 	return oriPos - (index - 1) * (cellX + paddingX * 2)
	-- end
end

function LuaDaFuWongMainPlayWnd:DaFuWengFinishCurStage(stage)
	local timeList = {LuaDaFuWongMgr.ItemExchageTimeList[9],LuaDaFuWongMgr.ItemExchageTimeList[10],LuaDaFuWongMgr.ItemExchageTimeList[11],LuaDaFuWongMgr.ItemExchageTimeList[12]}
	local data = {}
	if LuaDaFuWongMgr.CurRewardCardInfo then
		-- 播获得道具卡动效
		for k,v in pairs(LuaDaFuWongMgr.CurRewardCardInfo) do
			if v and v >= 1 then
				for i = 1,v do
					table.insert(data,k)
				end
			end
		end
	end
	if #data > 0 then
		SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Get_Item", Vector3.zero, nil, 0)
		self.m_GetCardFx.gameObject:SetActive(true)
		for i = 1,self.m_MaxCardNum do
			if data[i] then
				local cardInfo = DaFuWeng_Card.GetData(data[i])
				self.m_CardFxView[i].id = data[i]
				self.m_CardFxView[i].go.gameObject:SetActive(true)
				self.m_CardFxView[i].icon:LoadMaterial(cardInfo.Icon)
				self.m_CardFxView[i].name.text = LocalString.StrH2V(cardInfo.Name,true)
				self.m_CardFxView[i].go.transform:Find("CUIFxshifang").gameObject:SetActive(false)
			else
				self.m_CardFxView[i].go.gameObject:SetActive(false)
			end
		end
		self.m_GetCardFx.transform:Find("kgd"):GetComponent(typeof(UITable)):Reposition()
		self.m_GetCardFx.gameObject:GetComponent(typeof(Animation)):Play("dafuwongmainplaywnd_xinzeng")
		local func1 = function()
			for k,v in pairs(self.m_CardFxView) do
				if v.go.activeSelf then
					v.go.transform:Find("CUIFxshifang").gameObject:SetActive(true)
				end
			end
		end
		local func2 = function()
			local glowfx = self.m_GetCardFx.transform:Find("vfx")
			glowfx.gameObject:SetActive(true)
			local localTarget = self.transform:Find("ShowArea/Card").transform.localPosition
			local localTargetx = self.transform:Find("ShowArea/Card/CardTable").transform.localPosition.x
			LuaTweenUtils.TweenPosition(glowfx.transform, localTargetx, localTarget.y, localTarget.z, timeList[3])
		end
		local func3 = function()
			self.m_GetCardFx.gameObject:SetActive(false)
			local glowfx = self.m_GetCardFx.transform:Find("vfx")
			LuaUtils.SetLocalPosition(glowfx.transform, 0, 0, 0)
			if LuaDaFuWongMgr.IsMyRound then
				Gac2Gas.DaFuWengFinishCurStage(stage)
			end
		end
			self.m_JunWangLingTick[10] = RegisterTickOnce(func1,timeList[1] * 1000)
		if LuaDaFuWongMgr.IsMyRound or stage == EnumDaFuWengStage.RoundWorldEvent then
			CUIManager.CloseUI(CLuaUIResources.DaFuWongWorldEventWnd)
			self.m_JunWangLingTick[11] = RegisterTickOnce(func2,(timeList[1] + timeList[2]) * 1000)
			self.m_JunWangLingTick[13] = RegisterTickOnce(func3,(timeList[1] + timeList[2] + timeList[3] + timeList[4] ) * 1000)
		end
		LuaDaFuWongMgr.CurRewardCardInfo = nil
	else
		if LuaDaFuWongMgr.IsMyRound then
			Gac2Gas.DaFuWengFinishCurStage(stage)
		end
		LuaDaFuWongMgr.CurRewardCardInfo = nil
	end
end

function LuaDaFuWongMainPlayWnd:ClearTick()
	for k,v in pairs(self.m_JunWangLingTick) do
		UnRegisterTick(v)
		v = nil
	end
	self.m_JunWangLingTick = {}
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.MianDice)
	if self.m_TouZiTick then UnRegisterTick(self.m_TouZiTick) self.m_TouZiTick = nil end
end
function LuaDaFuWongMainPlayWnd:OnRules_BtnClick()
	CUIManager.ShowUI(CLuaUIResources.DaFuWongRuleWnd)
end

function LuaDaFuWongMainPlayWnd:OnLoudSpeak_BtnClick()
	self:ShowWorldEvent()
end
function LuaDaFuWongMainPlayWnd:OnSetting_BtnClick()
	CSettingInfoMgr.ShowSettingWnd(0)
end
function LuaDaFuWongMainPlayWnd:OnStageUpdate(CurStage)
	self:UpdataRoundView()
	self.RoundCount.text = LuaDaFuWongMgr.CurRound and LuaDaFuWongMgr.CurRound.round or 0
	-- if CurStage == EnumDaFuWengStage.RoundCard and LuaDaFuWongMgr.IsMyRound then
	-- 	self.JiaoYiHangBtn.gameObject:GetComponent(typeof(UIWidget)).alpha = 1
	-- else
	-- 	self.JiaoYiHangBtn.gameObject:GetComponent(typeof(UIWidget)).alpha = 0.5
	-- end
	self.m_CameraMoveArea.gameObject:SetActive(LuaDaFuWongMgr.CanMoveCameraStage and LuaDaFuWongMgr.CanMoveCameraStage[CurStage])
	-- if CurStage == EnumDaFuWengStage.RoundDice or CurStage == EnumDaFuWengStage.RoundCard then
	-- else
	-- 	self.Touzi.gameObject:SetActive(false)
	-- end
	if not (CurStage == EnumDaFuWengStage.RoundDice or CurStage == EnumDaFuWengStage.RoundCard) then
		self.Touzi.gameObject:SetActive(false)
	end
	CUIManager.SetUIVisibility(CLuaUIResources.DaFuWongMainPlayWnd, CurStage ~= EnumDaFuWengStage.GameStart and CurStage ~= EnumDaFuWengStage.GameEnd, "Dafuweng")
	if CurStage ~= EnumDaFuWengStage.GameStart and CurStage ~= EnumDaFuWengStage.GameEnd then
		LuaDaFuWongMgr:InitDaFuWongChannel()
	end
end

function LuaDaFuWongMainPlayWnd:ShowCardFx(needShow)
	if not self.m_itemView then return end
	
	for k,v in pairs(self.m_itemView) do
		if v.go.gameObject.activeSelf then
			v.fxGo.gameObject:SetActive(LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RoundCard and LuaDaFuWongMgr.IsMyRound and needShow and self.m_SelectItem == 0)
		end
	end
end

function LuaDaFuWongMainPlayWnd:DoCardAniFlyToPlayer(cardId,SelectPlayerId)
	if not self.m_itemView or not self.m_playerView then return end
	local cardView = nil
	local PlayerView = self.m_playerView[SelectPlayerId]
	for k,v in pairs(self.m_itemView) do
		if v.id == cardId and not cardView then cardView = v.go break end
	end
	if not cardView or not PlayerView then return end
	cardView.gameObject:SetActive(false)
	local CardLocalPos = cardView.transform.localPosition
	local PlayerLocalPos = PlayerView.transform.localPosition
	local CardTableLocalPos = self.CardTable.transform.localPosition
	local startPos = Vector3(CardTableLocalPos.x + CardLocalPos.x , CardTableLocalPos.y + CardLocalPos.y, 0)
	local moveItem = self.m_ItemCUIFxList[3].gameObject
	local PlayerInfoTablePos = self.PlayerInfoTable.transform.localPosition
	local PlayerInfo = self.transform:Find("ShowArea/PlayerInfo")
	moveItem.gameObject:SetActive(true)
	local targetPos = Vector3(PlayerLocalPos.x + PlayerInfoTablePos.x,PlayerLocalPos.y + PlayerInfoTablePos.y,0)
	local point = PlayerInfo.transform:TransformPoint(targetPos)
	local localTarget = self.transform:Find("ShowArea/Card"):InverseTransformPoint(point)
	LuaUtils.SetLocalPosition(moveItem.transform,startPos.x,startPos.y,startPos.z)
	local tween = LuaTweenUtils.TweenPosition(moveItem.transform, localTarget.x - 90, localTarget.y , localTarget.z, 1)
	LuaTweenUtils.OnComplete(tween, function () moveItem.gameObject:SetActive(false)end)
end
function LuaDaFuWongMainPlayWnd:OnCloseDaFuWongItemUseTip(itemID)
	local lastView = self.m_itemView and self.m_itemView[self.m_SelectItem]
	if lastView then
		lastView.highlight.gameObject:SetActive(false)
		LuaUtils.SetLocalScale(lastView.go.transform,1,1,1)
		LuaUtils.SetLocalPositionY(lastView.go.transform,7.8)
		self.m_SelectItem = 0
	end
end
function LuaDaFuWongMainPlayWnd:OnJiaoYiHangBtnClick()
	LuaActivityRedDotMgr:OnRedDotClicked(57)
	self.JiaoYiHangBtn.transform:Find("CUIFx"):GetComponent(typeof(CUIFx)):DestroyFx()
	CUIManager.ShowUI(CLuaUIResources.DaFuWongJiaoyiHangWnd)
end

function LuaDaFuWongMainPlayWnd:UpdataRoundView()
	local roundData = LuaDaFuWongMgr.CurRound 
	local desc = DaFuWeng_Setting.GetData().RoundExtraLabel
	if not roundData or not roundData.RoundIndex then 
		self.ExtraLabel.gameObject:SetActive(false)
		self.RoundCount.transform:Find("bg"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("453B3B",0)
		return
	end
	if roundData.RoundIndex < 0 then
		self.ExtraLabel.gameObject:GetComponent(typeof(TweenAlpha)).enabled = false
	else
		local tweenalpha = self.ExtraLabel.gameObject:GetComponent(typeof(TweenAlpha))
		tweenalpha.enabled = true
		tweenalpha.duration = roundData.ShakeRate
	end
	self.RoundCount.transform:Find("bg"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(roundData.BgColorCode,0)
	self.ExtraLabel.gameObject:SetActive(true)
	self.ExtraLabel.text = SafeStringFormat3(desc,roundData.Cost)
end

function LuaDaFuWongMainPlayWnd:GetGuideGo(methodName)
	if methodName == "GetJiaoYiHangBtn" then
        return self.JiaoYiHangBtn.gameObject
	end
end

function LuaDaFuWongMainPlayWnd:UpdateJiaoyihangBtnRedDot()
	local fx = self.JiaoYiHangBtn.transform:Find("CUIFx"):GetComponent(typeof(CUIFx))
	fx:DestroyFx()
	if LuaActivityRedDotMgr:IsRedDot(57) then
		fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
		local path={Vector3(-55,55,0),Vector3(55,55,0),Vector3(55,-55,0),Vector3(-55,-55,0),Vector3(-55,55,0)}
        local array = Table2Array(path, MakeArrayClass(Vector3))
        LuaUtils.SetLocalPosition(fx.transform,-55,55,0)
        LuaTweenUtils.DODefaultLocalPath(fx.transform,array,2)
	end
end

--@endregion UIEvent
function LuaDaFuWongMainPlayWnd:OnEnable()
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "UpdateCountDown")
	g_ScriptEvent:AddListener("OnDaFuWongPlayerRound", self, "OnPlayerRound")
	g_ScriptEvent:AddListener("OnDaFuWongPlayerInfoChange", self, "OnPlayerInfoChange")
	g_ScriptEvent:AddListener("OnDaFuWongItemExchage",self,"ItemExchage")
	g_ScriptEvent:AddListener("OnDaFuWongMainPlayerLandInfoUpdate",self,"UpdateLands")
	g_ScriptEvent:AddListener("OnDaFuWongMainPlayerGoodsInfoUpdate",self,"UpdateGoods")
	g_ScriptEvent:AddListener("OnDaFuWongMainPlayerCardChange",self,"UpdateItemListView")
	g_ScriptEvent:AddListener("OnDaFuWongMainPlayerRolTouZi",self,"ShowTouZiAni")
	g_ScriptEvent:AddListener("OnDaFuWongMainWaitPlayerRolTouZi",self,"ShowTouZi")
	g_ScriptEvent:AddListener("OnDaFuWengWordEventUpdate",self,"ShowWorldEvent")
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
	g_ScriptEvent:AddListener("OnDaFuWengFinishCurStage",self,"DaFuWengFinishCurStage")
	g_ScriptEvent:AddListener("DaFuWengCardAniFlyToPlayer",self,"DoCardAniFlyToPlayer")
	g_ScriptEvent:AddListener("OnCloseDaFuWongItemUseTip",self,"OnCloseDaFuWongItemUseTip")
end

function LuaDaFuWongMainPlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "UpdateCountDown")
	g_ScriptEvent:RemoveListener("OnDaFuWongPlayerRound", self, "OnPlayerRound")
	g_ScriptEvent:RemoveListener("OnDaFuWongPlayerInfoChange", self, "OnPlayerInfoChange")
	g_ScriptEvent:RemoveListener("OnDaFuWongItemExchage",self,"ItemExchage")
	g_ScriptEvent:RemoveListener("OnDaFuWongMainPlayerLandInfoUpdate",self,"UpdateLands")
	g_ScriptEvent:RemoveListener("OnDaFuWongMainPlayerGoodsInfoUpdate",self,"UpdateGoods")
	g_ScriptEvent:RemoveListener("OnDaFuWongMainPlayerCardChange",self,"UpdateItemListView")
	g_ScriptEvent:RemoveListener("OnDaFuWongMainPlayerRolTouZi",self,"ShowTouZiAni")
	g_ScriptEvent:RemoveListener("OnDaFuWongMainWaitPlayerRolTouZi",self,"ShowTouZi")
	g_ScriptEvent:RemoveListener("OnDaFuWengWordEventUpdate",self,"ShowWorldEvent")
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
	g_ScriptEvent:RemoveListener("OnDaFuWengFinishCurStage",self,"DaFuWengFinishCurStage")
	g_ScriptEvent:RemoveListener("DaFuWengCardAniFlyToPlayer",self,"DoCardAniFlyToPlayer")
	g_ScriptEvent:RemoveListener("OnCloseDaFuWongItemUseTip",self,"OnCloseDaFuWongItemUseTip")
	self:ClearTick()
	if self.m_TouZiRo then
		self.m_TouZiRo:Destroy()
		self.m_TouZiRo = nil
	end
	self.Touzi.transform:Find("FakeModel/Texture"):GetComponent(typeof(UITexture)).mainTexture=nil
    CUIManager.DestroyModelTexture("__DaFuWongTouZi__")
end
