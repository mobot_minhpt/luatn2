-- Auto Generated!!
local CBWDHChampionMemberItem = import "L10.UI.CBWDHChampionMemberItem"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
CBWDHChampionMemberItem.m_Init_CS2LuaHook = function (this, info, row, isLeader) 
    if info == nil then
        this.nameLabel.text = ""
        this.gradeLabel.text = ""
        this.clsSprite.spriteName = nil
        this.leaderSprite.enabled = false
        return
    end
    this.PlayerId = info.playerdId
    if isLeader then
        this.leaderSprite.enabled = true
    else
        this.leaderSprite.enabled = false
    end
    this.nameLabel.text = info.name

    this.gradeLabel.text = System.String.Format("lv.{0}", info.grade)

    this.clsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.role))

    if row % 2 == 0 then
        this:SetBackgroundTexture(Constants.NewEvenBgSprite)
    else
        this:SetBackgroundTexture(Constants.NewOddBgSprite)
    end
end
