require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaLiangHaoRenewWnd = class()

RegistClassMember(LuaLiangHaoRenewWnd, "m_LiangHaoLabel")
RegistClassMember(LuaLiangHaoRenewWnd, "m_PlayerLabel")
RegistClassMember(LuaLiangHaoRenewWnd, "m_FriendButton")

RegistClassMember(LuaLiangHaoRenewWnd, "m_FromDateLabel")
RegistClassMember(LuaLiangHaoRenewWnd, "m_ExpireDateLabel")

RegistClassMember(LuaLiangHaoRenewWnd, "m_MoneyCtrl")
RegistClassMember(LuaLiangHaoRenewWnd, "m_RenewButton")

function LuaLiangHaoRenewWnd:Init()

	self.m_LiangHaoLabel = self.transform:Find("Anchor/LiangHaoLabel"):GetComponent(typeof(UILabel))
	self.m_PlayerLabel = self.transform:Find("Anchor/LiangHaoLabel/Label"):GetComponent(typeof(UILabel))
	self.m_FriendButton = self.transform:Find("Anchor/LiangHaoLabel/FriendButton").gameObject

	self.m_FromDateLabel = self.transform:Find("Anchor/FromDateLabel"):GetComponent(typeof(UILabel))
	self.m_ExpireDateLabel = self.transform:Find("Anchor/ExpireDateLabel"):GetComponent(typeof(UILabel))

	self.m_MoneyCtrl = self.transform:Find("Anchor/MoneyCtrl"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_RenewButton = self.transform:Find("Anchor/RenewButton").gameObject
	
	CommonDefs.AddOnClickListener(self.m_FriendButton, DelegateFactory.Action_GameObject(function(go) self:OnFriendButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_RenewButton, DelegateFactory.Action_GameObject(function(go) self:OnRenewButtonClick() end), false)

	self:InitLiangHaoInfo()

end

function LuaLiangHaoRenewWnd:InitLiangHaoInfo()

	local renewInfo = LuaLiangHaoMgr.RenewPlayerInfo

	self.m_LiangHaoLabel.text = tostring(renewInfo.lianghaoId)
	self.m_PlayerLabel.text = SafeStringFormat3(LocalString.GetString("%s的ID"), renewInfo.name)

	
	if CServerTimeMgr.Inst.timeStamp>renewInfo.oldExpiredTime then --原来的已过期
		local now = CServerTimeMgr.Inst:GetZone8Time()
        self.m_FromDateLabel.text = ToStringWrap(now, "yyyy-MM-dd")
   
	else --原来的未过期
		local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(renewInfo.oldExpiredTime)
		self.m_FromDateLabel.text = ToStringWrap(endTime, "yyyy-MM-dd")
	end

	local time =  CServerTimeMgr.ConvertTimeStampToZone8Time(renewInfo.newExpiredTime)
	self.m_ExpireDateLabel.text = ToStringWrap(time, "yyyy-MM-dd")..SafeStringFormat3(LocalString.GetString("(%d天)"), renewInfo.addDay)

	self.m_MoneyCtrl:SetCost(renewInfo.renewPrice)
	
end

function LuaLiangHaoRenewWnd:OnFriendButtonClick()
	LuaLiangHaoMgr.ShowLiangHaoFriendListWnd()
end

function LuaLiangHaoRenewWnd:OnRenewButtonClick()
	local renewInfo = LuaLiangHaoMgr.RenewPlayerInfo

	local msg = nil
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == renewInfo.id then --自己续费
		msg = g_MessageMgr:FormatMessage("Lianghao_SelfXuFei_Confirm", renewInfo.renewPrice, renewInfo.lianghaoId)
	else
		msg = g_MessageMgr:FormatMessage("Lianghao_XuFei_Confirm", renewInfo.renewPrice, renewInfo.name, renewInfo.lianghaoId)
	end
	MessageWndManager.ShowOKCancelMessage(msg, 
		DelegateFactory.Action(function()
			LuaLiangHaoMgr.RequestRenewLiangHao(renewInfo.id, renewInfo.name, renewInfo.lianghaoId, renewInfo.renewPrice)
			self:Close()
		end),nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function LuaLiangHaoRenewWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
