local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"
local UILabel = import "UILabel"
local UITable = import "UITable"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaCookingResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCookingResultWnd, "SuccessResult", "SuccessResult", GameObject)
RegistChildComponent(LuaCookingResultWnd, "BadResult", "BadResult", GameObject)
RegistChildComponent(LuaCookingResultWnd, "CloseButton2", "CloseButton2", GameObject)
RegistChildComponent(LuaCookingResultWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaCookingResultWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaCookingResultWnd, "StarTemplate", "StarTemplate", GameObject)
RegistChildComponent(LuaCookingResultWnd, "StarsTable", "StarsTable", UITable)
RegistChildComponent(LuaCookingResultWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaCookingResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaCookingResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaCookingResultWnd, "Food", "Food", CUITexture)
--@endregion RegistChildComponent end


function LuaCookingResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CloseButton2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButton2Click()
	end)

	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    --@endregion EventBind end
end

function LuaCookingResultWnd:OnEnable()
	g_ScriptEvent:AddListener("ShareFinished", self, "OnShareFinished")
    g_ScriptEvent:AddListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
end

function LuaCookingResultWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ShareFinished", self, "OnShareFinished")
    g_ScriptEvent:RemoveListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
end

function LuaCookingResultWnd:OnShareFinished(argv)
    local code, result = argv[0], argv[1]
    if result then
		local t = LuaCookBookMgr.m_ShangShiSendCookingResult
        Gac2Gas.ShangShiShareCooking(t.cookbookId)
    end
end

function LuaCookingResultWnd:OnPersonalSpaceShareFinished()
	local t = LuaCookBookMgr.m_ShangShiSendCookingResult
    Gac2Gas.ShangShiShareCooking(t.cookbookId)
end

function LuaCookingResultWnd:Init()
	LuaCookBookMgr:InitMyFoodIngredientData()
	local t = LuaCookBookMgr.m_ShangShiSendCookingResult
	local data = ShangShi_Cookbook.GetData(t.cookbookId)
	self.Food:LoadMaterial(data.Icon)
	Extensions.SetLocalPositionZ(self.Food.transform, not t.isSucc and -1 or 0)
	self.SuccessResult.gameObject:SetActive(t.isSucc)
	self.BadResult.gameObject:SetActive(not t.isSucc)
	self.ReadMeLabel.text = t.isSucc and data.ShareWndDesc or g_MessageMgr:FormatMessage("CookingResultWnd_ReadMe")
	self.NameLabel.text = data.Name
	self.StarTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.StarsTable.transform)
	for i = 1, t.star do
		local obj = NGUITools.AddChild(self.StarsTable.gameObject, self.StarTemplate.gameObject)
		obj.gameObject:SetActive(true)
	end
	self.StarsTable:Reposition()
	self.Fx:LoadFx(not t.isSucc and "fx/ui/prefab/UI_caipu_shibai.prefab" or (t.star == 5 and "fx/ui/prefab/UI_caipu_juepin.prefab" or "fx/ui/prefab/UI_caipu_putong.prefab"))
end

--@region UIEvent

function LuaCookingResultWnd:OnCloseButton2Click()
	CUIManager.CloseUI(CLuaUIResources.CookingResultWnd)
end

function LuaCookingResultWnd:OnShareButtonClick()
	self.CloseButton:SetActive(false)
	CUICommonDef.CaptureScreen(
		"screenshot",
		true,
		false,
		self.ShareButton,
		DelegateFactory.Action_string_bytes(
				function(fullPath, jpgBytes)
					self.CloseButton:SetActive(true)
					if CLuaShareMgr.IsOpenShare() then
						ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
					else
						CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
					end
				end
		),
		false
)
end

--@endregion UIEvent

