local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local CGuildSortButton = import "L10.UI.CGuildSortButton"

LuaGuildTerritorialWarsContributionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsContributionWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaGuildTerritorialWarsContributionWnd, "TableView1", "TableView1", QnTableView)
RegistChildComponent(LuaGuildTerritorialWarsContributionWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsContributionWnd, "WeekView", "WeekView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsContributionWnd, "SeasonView", "SeasonView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsContributionWnd, "TableView2", "TableView2", QnTableView)
RegistChildComponent(LuaGuildTerritorialWarsContributionWnd, "HeaderRadioBox", "HeaderRadioBox", QnRadioBox)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsContributionWnd,"m_List1")
RegistClassMember(LuaGuildTerritorialWarsContributionWnd,"m_List2")
RegistClassMember(LuaGuildTerritorialWarsContributionWnd,"m_TabIndex")
RegistClassMember(LuaGuildTerritorialWarsContributionWnd,"m_SortFlags")
RegistClassMember(LuaGuildTerritorialWarsContributionWnd,"m_IsQuarterly")
RegistClassMember(LuaGuildTerritorialWarsContributionWnd,"m_SortType")

function LuaGuildTerritorialWarsContributionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsContributionWnd:Init()
    self.WeekView.gameObject:SetActive(false)
    self.SeasonView.gameObject:SetActive(false)
    self.BottomLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsContributionWnd_BottomLabel")
    self.TableView1.m_DataSource = DefaultTableViewDataSource.Create(
        function() return self.m_List1 and #self.m_List1 or 0 end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self.TableView1.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)
    self.TableView2.m_DataSource = DefaultTableViewDataSource.Create(
        function() return self.m_List2 and #self.m_List2 or 0 end,
        function(item,row)
            self:InitItem(item,row)
        end)
    self.TableView2.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnSelectAtRow(row)
    end)
    self.m_SortFlags = {}
    self.HeaderRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        self:OnHeadQnRadioBoxSelect(btn, index)
    end)
    self.Tabs:ChangeTab(0,false)
end

function LuaGuildTerritorialWarsContributionWnd:InitItem(item,row)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local classSprite = item.transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local lvLabel = item.transform:Find("LvLabel"):GetComponent(typeof(UILabel))
    local friend = item.transform:Find("Friend")
    local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local valLabel = item.transform:Find("ValLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local my = item.transform:Find("My")

    local data = (self.m_TabIndex == 0) and self.m_List1[row + 1] or self.m_List2[row + 1]
    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local labelColor = Color.white
    if playerId == data.playerId then
        labelColor = Color.green
    end
    nameLabel.text = data.playerName
    nameLabel.color = labelColor
    classSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.class))
    lvLabel.text = data.level
    lvLabel.color = data.hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or labelColor
    friend.gameObject:SetActive(data.isForeignAid)
    numLabel.text = data.killCount
    numLabel.color = labelColor
    valLabel.text = data.lingqi
    valLabel.color = labelColor
    scoreLabel.text = data.pingjia
    scoreLabel.color = labelColor
    my.gameObject:SetActive(playerId == data.playerId)
    local minute = math.floor(data.playTime / 60)
    local hour = math.floor(minute / 60)
    if hour > 0 then
        timeLabel.text = SafeStringFormat3(LocalString.GetString("%d小时"), hour)
    else
        timeLabel.text = SafeStringFormat3(LocalString.GetString("%d分钟"), minute)
    end
    timeLabel.color = labelColor
    item:SetBackgroundTexture((row % 2 == 1) and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
end

function LuaGuildTerritorialWarsContributionWnd:SortList(index)
    local list = self.m_List1
    if self.m_IsQuarterly then
        list = self.m_List2
    end
    self.m_SortType = index
    local sortKeyArray = {"class","level","killCount","lingqi","playTime","pingjia"}
    local flag = self.m_SortFlags[index]
    local sortKey = sortKeyArray[index]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if not list then
        return 
    end
    table.sort(list,function (a, b)
        if myId == a.playerId then 
            return true 
        elseif myId == b.playerId then 
            return false 
        end
        if not sortKey or not flag then
            return a.playerId < b.playerId
        else
            local val = a[sortKey] - b[sortKey]
            if val == 0 then
                return a.playerId < b.playerId
            end
            return (val * flag) < 0
        end
    end)
end

function LuaGuildTerritorialWarsContributionWnd:OnEnable()
    g_ScriptEvent:AddListener("SendGTWRelatedPlayContributeInfo", self, "OnContributeInfo")
end

function LuaGuildTerritorialWarsContributionWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendGTWRelatedPlayContributeInfo", self, "OnContributeInfo")
end

function LuaGuildTerritorialWarsContributionWnd:OnContributeInfo(isQuarterly)
    self.m_List1 = LuaGuildTerritorialWarsMgr.m_WeeklyContributeInfo
    self.m_List2 = LuaGuildTerritorialWarsMgr.m_QuarterlyContributeInfo
    self.m_IsQuarterly = isQuarterly
    for i = 0, self.HeaderRadioBox.m_RadioButtons.Length - 1 do
        self.HeaderRadioBox.m_RadioButtons[i]:SetSelected(false, false)
    end
    self.m_SortType = nil
    self:SortList(self.m_SortType)
    if isQuarterly then
        self.TableView2:ReloadData(true, true)
    else
        self.TableView1:ReloadData(true, true)
    end
end

--@region UIEvent
function LuaGuildTerritorialWarsContributionWnd:OnTabsTabChange(index)
    self.m_TabIndex = index
    self.WeekView.gameObject:SetActive(index == 0)
    self.SeasonView.gameObject:SetActive(index == 1)
    self.m_SortFlags = {}
    Gac2Gas.RequestOpenGTWRelatedPlayContributeWnd(index == 1)
end

function LuaGuildTerritorialWarsContributionWnd:OnSelectAtRow(row)
    local data = (self.m_TabIndex == 0) and self.m_List1[row + 1] or self.m_List2[row + 1]
    if not CClientMainPlayer.IsPlayerId(data.playerId) then
		CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.PlotDiscussion, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end
end

function LuaGuildTerritorialWarsContributionWnd:OnHeadQnRadioBoxSelect(btn, index)
    local sortButton = TypeAs(btn, typeof(CGuildSortButton))
    if self.m_SortFlags[index + 1] == nil then 
        self.m_SortFlags[index + 1] = -1 
    else
        self.m_SortFlags[index + 1] = -self.m_SortFlags[index + 1]
    end
    sortButton:SetSortTipStatus(self.m_SortFlags[index + 1] > 0)
    self:SortList(index + 1)
    if self.m_IsQuarterly then
        self.TableView2:ReloadData(true, true)
    else
        self.TableView1:ReloadData(true, true)
    end
end
--@endregion UIEvent

