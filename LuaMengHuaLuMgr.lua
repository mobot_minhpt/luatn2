local MengHuaLu_Setting = import "L10.Game.MengHuaLu_Setting"
local Time = import "UnityEngine.Time"
local MessageMgr = import "L10.Game.MessageMgr"
local MengHuaLu_Monster = import "L10.Game.MengHuaLu_Monster"

EnumMengHuaLuGridColor = {
	eOpened = 1, -- 已经点开
	eCanOpen = 2,	-- 能点
	eCanNotOpen = 3,	-- 不能点
}

LuaMengHuaLuMgr = {}

LuaMengHuaLuMgr.m_TestUIFx = "fx/ui/prefab/UI_paizi_dengji01.prefab"

LuaMengHuaLuMgr.m_ActionQueue = {} -- 梦华录执行的queue

function LuaMengHuaLuMgr.AddAction(action)
 	table.insert(LuaMengHuaLuMgr.m_ActionQueue, action)
end

function LuaMengHuaLuMgr.GetActionToExecute()
	if #LuaMengHuaLuMgr.m_ActionQueue > 0 then
		return table.remove(LuaMengHuaLuMgr.m_ActionQueue, 1)
	end
	return nil
end

--Action格式
--[[{
	SrcGridId = 0,
	DestGridId = 5,
	SkillId = 5,
	ItemId = 5,
	Interval = 1.5, -- 间隔时间
}--]]

--MazeInfo格式
--[[{
	MazeID = 1,
	MazeColor = EnumMengHuaLuGridColor.eWhite
	MazeIsLocked = true,
	ItemId = 10001,
	MonsterInfo = {monsterId, hp, att, actionRound, buffData},
}--]]

LuaMengHuaLuMgr.m_BabyInfo = {}
LuaMengHuaLuMgr.m_MazeInfos = {}
LuaMengHuaLuMgr.m_Layer = 1

LuaMengHuaLuMgr.m_IsOpen = true
-- 开始梦华录
function LuaMengHuaLuMgr.StartMengHuaLu()
	if not LuaMengHuaLuMgr.m_IsOpen then
		MessageMgr.Inst:ShowMessage("MENGHUALU_NOT_OPEN", {})
		return
	end
	if not CUIManager.IsLoaded(CLuaUIResources.MengHuaLuWnd) then
		CUIManager.ShowUI(CLuaUIResources.MengHuaLuWnd)
	end
end

LuaMengHuaLuMgr.m_StartTime = 0
function LuaMengHuaLuMgr.CheckMazeClick()
	if Time.realtimeSinceStartup - LuaMengHuaLuMgr.m_StartTime > 0.8 then
		LuaMengHuaLuMgr.m_StartTime = Time.realtimeSinceStartup
		return true
	else
		return false
	end
end

-- 打开怪物信息
LuaMengHuaLuMgr.m_SelectedMonsterGrid = 0
function LuaMengHuaLuMgr.ShowMonsterInfo(grid)
	LuaMengHuaLuMgr.m_SelectedMonsterGrid = grid
	CUIManager.ShowUI(CLuaUIResources.MengHuaLuMonsterWnd)
end

-- 格子数据解析
function LuaMengHuaLuMgr.LoadGrid(info)
	local grid, color, locked  = info.grid, info.color
	local locked = false

	if grid ~= 0 then
		locked = info.locked
	end

	if grid == 0 then
		-- 宝宝数据
		if CommonDefs.DictContains_LuaCall(info, "character") then
			LuaMengHuaLuMgr.LoadPlayPlayer(info.character)
		end
		return nil
	else
		local mazeInfo = {}
		mazeInfo.MazeID = grid
		mazeInfo.MazeColor = color
		mazeInfo.MazeIsLocked = locked

		if CommonDefs.DictContains_LuaCall(info, "itemId") then
			mazeInfo.ItemId = info.itemId
		else
			mazeInfo.ItemId = 0
		end

		if CommonDefs.DictContains_LuaCall(info, "character") then
			mazeInfo.MonsterInfo = LuaMengHuaLuMgr.LoadMonster(info.character)
		else
			mazeInfo.MonsterInfo = nil
		end
		
		return mazeInfo
	end

end

-- boss掉落物物品解析
function LuaMengHuaLuMgr.LoadDropGrid(info)
	local grid = info[0]
	local itemId = info[1]

	local mazeInfo = {}
	mazeInfo.MazeID = grid
	mazeInfo.MazeColor = 1
	mazeInfo.MazeIsLocked = false
	mazeInfo.ItemId = itemId
	mazeInfo.MonsterInfo = nil

	return mazeInfo
end

-- 宝宝数据解析
function LuaMengHuaLuMgr.LoadPlayPlayer(data)
	local hp, mp, att, gold, skillTbl, avtiveQiChangList, buffDataList, appearanceData = data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]

	local babyInfo = {}
	babyInfo.Hp = hp
	babyInfo.Mp = mp
	babyInfo.Attack = att
	babyInfo.Gold = gold
	babyInfo.Skills = skillTbl
	babyInfo.AvtiveQiChangList = avtiveQiChangList
	babyInfo.BuffDataList = buffDataList
	babyInfo.AppearanceData = appearanceData
	LuaMengHuaLuMgr.m_BabyInfo = babyInfo

	g_ScriptEvent:BroadcastInLua("MengHuaLuUpdateBaby", babyInfo)
	-- skillTbl 是 [id]=count
	-- avtiveQiChangList 是qiChangid的list
	-- buffDataList 是buff数据的list 参照 SynMengHuaLuBuffData 这个rpc 格式是{{buffId, LeftRound}, {}}

end

-- MonsterInfo = {MonsterId, Hp, Attack, ActionRound, BuffData}
function LuaMengHuaLuMgr.LoadMonster(data)
	local monsterInfo = {}
	local monsterId, hp, att, actionRound, buffData = data[0], data[1], data[2], data[3], data[4]
	-- buff数据暂时不确定是否同步
	monsterInfo.MonsterId = monsterId
	monsterInfo.Hp = hp
	monsterInfo.Attack = att
	monsterInfo.ActionRound = actionRound
	monsterInfo.BuffData = buffData
	return monsterInfo
end

-- 打开商店
LuaMengHuaLuMgr.m_Goods = nil
LuaMengHuaLuMgr.m_ShopQiChangs = nil
function LuaMengHuaLuMgr.OpenMengHuaLuShop(goods, qichangInfo)
	LuaMengHuaLuMgr.m_Goods = goods
	LuaMengHuaLuMgr.m_ShopQiChangs = qichangInfo
	if CUIManager.IsLoaded(CLuaUIResources.MengHuaLuShopWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateMengHuaLuGoods")
	else
		CUIManager.ShowUI(CLuaUIResources.MengHuaLuShopWnd)
	end
end


-- 打开结算界面
LuaMengHuaLuMgr.m_ResultRank = 0
LuaMengHuaLuMgr.m_ResultTotalRank = 0
LuaMengHuaLuMgr.m_ResultLayer = 0
-- 新增结算显示数据
LuaMengHuaLuMgr.m_ResultOpenGridNum = 0
LuaMengHuaLuMgr.m_ResultKillMonsterNum = 0
LuaMengHuaLuMgr.m_ResultTotalGoldNum = 0
LuaMengHuaLuMgr.m_ResultTotalSkillNum = 0
LuaMengHuaLuMgr.m_ResultBossNum = 0
LuaMengHuaLuMgr.m_ResultMaxLayer = 0

function LuaMengHuaLuMgr.ShowResultWnd(rank, toralNum, data_U)
	local data = MsgPackImpl.unpack(data_U)
	local openGridNum = data[0]
	local killMonsterNum = data[1]
	local totalGoldNum = data[2]
	local totalSkillNum = data[3]
	local bossNum = data[4]
	local maxLayer = data[5]
	local layer = data[6]

	LuaMengHuaLuMgr.m_ResultRank = rank
	LuaMengHuaLuMgr.m_ResultTotalRank = toralNum
	LuaMengHuaLuMgr.m_ResultLayer = layer

	LuaMengHuaLuMgr.m_ResultOpenGridNum = openGridNum
	LuaMengHuaLuMgr.m_ResultKillMonsterNum = killMonsterNum
	LuaMengHuaLuMgr.m_ResultTotalGoldNum = totalGoldNum
	LuaMengHuaLuMgr.m_ResultTotalSkillNum = totalSkillNum
	LuaMengHuaLuMgr.m_ResultBossNum = bossNum
	LuaMengHuaLuMgr.m_ResultMaxLayer = maxLayer

	CUIManager.ShowUI(CLuaUIResources.MengHuaLuResultWnd)
end

-- 打开宝宝的buff界面
LuaMengHuaLuMgr.m_BuffList = nil
function LuaMengHuaLuMgr.ShowBuffList(buffList)
	LuaMengHuaLuMgr.m_BuffList = buffList
	CUIManager.ShowUI(CLuaUIResources.MengHuaLuBuffListWnd)
end

-- 打开技能提示
LuaMengHuaLuMgr.m_SelectedSkillInfo = nil
function LuaMengHuaLuMgr.ShowSkillTip(selectedSkillInfo)
 	LuaMengHuaLuMgr.m_SelectedSkillInfo = selectedSkillInfo
 	CUIManager.ShowUI(CLuaUIResources.MengHuaLuSkillTip)
 end 

-- 关闭技能提示
function LuaMengHuaLuMgr.CloseSkillTip()
	LuaMengHuaLuMgr.m_SelectedSkillInfo = nil
	CUIManager.CloseUI(CLuaUIResources.MengHuaLuSkillTip)
end

-- 玩家传送后，重新请求梦华录数据
function LuaMengHuaLuMgr.OnMainPlayerCreated()
	if CUIManager.IsLoaded(CLuaUIResources.MengHuaLuWnd) then
		-- 如果打开了梦华录界面，则重新请求数据
		Gac2Gas.RequestStartMengHuaLuWithChoice(false)
	end
end

-- 下一次是boss关
function LuaMengHuaLuMgr.NextIsBoss()
	local setting = MengHuaLu_Setting.GetData()
	if LuaMengHuaLuMgr.m_Layer < setting.MinBossLayer then
		return LuaMengHuaLuMgr.m_Layer == (setting.MinBossLayer - 1)
	else
		return (LuaMengHuaLuMgr.m_Layer - setting.MinBossLayer) % setting.BossLayerInterval == (setting.BossLayerInterval - 1)
	end
end

-- 是否是boss层
LuaMengHuaLuMgr.m_BossDropTick = nil
function LuaMengHuaLuMgr.IsBossLayer()
	local setting = MengHuaLu_Setting.GetData()
	if LuaMengHuaLuMgr.m_Layer <= setting.MinBossLayer then
		return LuaMengHuaLuMgr.m_Layer == setting.MinBossLayer
	else
		return (LuaMengHuaLuMgr.m_Layer - setting.MinBossLayer) % setting.BossLayerInterval == 0
	end
end

-- 是否拥有可以释放技能的对象
function LuaMengHuaLuMgr.HasSkillTarget(skillId)
	for i = 1, #LuaMengHuaLuMgr.m_MazeInfos do
		local info = LuaMengHuaLuMgr.m_MazeInfos[i]
		if info.MazeColor == EnumMengHuaLuGridColor.eOpened and info.MonsterInfo then
			local monster = MengHuaLu_Monster.GetData(info.MonsterInfo.MonsterId)
			if monster.IsFriend == 0 then
				return true
			end
		end
	end
	return false
end
