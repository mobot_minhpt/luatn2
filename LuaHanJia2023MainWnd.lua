local UILabel = import "UILabel"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CScheduleMgr = import "L10.Game.CScheduleMgr"


LuaHanJia2023MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHanJia2023MainWnd, "TimeLb", "TimeLb", UILabel)
RegistChildComponent(LuaHanJia2023MainWnd, "ExchangeButton", "ExchangeButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "GiftButton", "GiftButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "NewButton", "NewButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "SingleAdventureButton", "SingleAdventureButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "MultipleAdventureButton", "MultipleAdventureButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "SnowmanPVEButton", "SnowmanPVEButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "LotteryButton", "LotteryButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "BattlePassButton", "BattlePassButton", GameObject)
RegistChildComponent(LuaHanJia2023MainWnd, "common_vfx_dianjifankui", "common_vfx_dianjifankui", GameObject)

--@endregion RegistChildComponent end

function LuaHanJia2023MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
    CScheduleMgr.Inst:RequestActivity()

end

function LuaHanJia2023MainWnd:Init()

end

function LuaHanJia2023MainWnd:InitWndData()
    self.hanjiaJieRiId = 27
    self.singleActivityId = 42030294
    self.multipleActivityId = 42030300
    self.snowmanPVEActivityId = 42030295
    self.lotteryActivityId = 42030296
    self.battlePassActivityId = 42030297

    self.chunjieJieRiId = 22
    self.springCouponsActivityId = 42030282
    self.newLimitTimeActivityId = 42030283
end

function LuaHanJia2023MainWnd:RefreshConstUI()
    self.TimeLb.text = g_MessageMgr:FormatMessage("HanJia2023_MainWnd_OpenTimeTips")
    --PC包下 这个gameObject会被默认打开, 但是Unity不会.
    self.common_vfx_dianjifankui:SetActive(false)
end

function LuaHanJia2023MainWnd:RefreshVariableUI()
    self:OnSyncXuePoLiXianData()
    self:OnSyncXianKeLaiData()
end

function LuaHanJia2023MainWnd:InitUIEvent()
    UIEventListener.Get(self.SingleAdventureButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:CheckCanJoin(self.singleActivityId, self.hanjiaJieRiId, function()
            self:ClearTick()
            LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId = nil
            self:AddTick(self.SingleAdventureButton, CLuaUIResources.SnowAdventureSingleStageMainWnd)
        end)
    end)

    UIEventListener.Get(self.MultipleAdventureButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:CheckCanJoin(self.multipleActivityId, self.hanjiaJieRiId, function()
            if LuaHanJia2023Mgr.snowAdventureData.isMultipleOpen then
                self:ClearTick()
                self:AddTick(self.MultipleAdventureButton, CLuaUIResources.SnowAdventureMultipleStageMainWnd)
            end
        end, function() g_MessageMgr:ShowMessage("HanJia2023_Multiple_Not_Open") end)
    end)

    UIEventListener.Get(self.SnowmanPVEButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:CheckCanJoin(self.snowmanPVEActivityId, self.hanjiaJieRiId, function()
            self:ClearTick()
            self:AddTick(self.SnowmanPVEButton, CLuaUIResources.SnowManPVEPlayWnd)
        end)
    end)

    UIEventListener.Get(self.LotteryButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:CheckCanJoin(self.lotteryActivityId, self.hanjiaJieRiId, function()
            self:ClearTick()
            self:AddTick(self.LotteryButton, CLuaUIResources.HanJia2023LotteryWnd)
        end)
    end)

    UIEventListener.Get(self.BattlePassButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:CheckCanJoin(self.battlePassActivityId, self.hanjiaJieRiId, function()
            self:ClearTick()
            self:AddTick(self.BattlePassButton, CLuaUIResources.HanJia2023PassMainWnd)
        end)
    end)

    UIEventListener.Get(self.GiftButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.SpringCouponsWnd)
    end)

    UIEventListener.Get(self.NewButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CShopMallMgr.SelectMallIndex = 0
        CShopMallMgr.SelectCategory = 6
        CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
    end)

    UIEventListener.Get(self.ExchangeButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local setting = HanJia2023_XianKeLaiSetting.GetData()
        local shopid = setting.ShopID
        CLuaNPCShopInfoMgr.ShowScoreShopById(shopid)
    end)
end

function LuaHanJia2023MainWnd:CheckCanJoin(activityId, JieRiId, passExtraCB, notPassExtraCB)
    if self:CheckScheduleOpen(activityId, JieRiId, true, true) then
        if CScheduleMgr.Inst:IsCanJoinSchedule(activityId, true) then
            --在活动时间里
            if passExtraCB then
                passExtraCB()
            end
        else
            local scheduleData = Task_Schedule.GetData(activityId)
            CLuaScheduleMgr.selectedScheduleInfo = {
                activityId = activityId,
                taskId = scheduleData.TaskID[0],
                TotalTimes = 0,
                DailyTimes = 0,
                ActivityTime = scheduleData.ExternTip,
                ShowJoinBtn = false
            }
            CLuaScheduleInfoWnd.s_UseCustomInfo = true
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
            if notPassExtraCB then
                notPassExtraCB()
            end
        end
    end
end

function LuaHanJia2023MainWnd:ClearTick()
    self.common_vfx_dianjifankui:SetActive(false)
    if self.clickTick then
        UnRegisterTick(self.clickTick)
        self.clickTick = nil
    end
end

function LuaHanJia2023MainWnd:AddTick(root, wndName)
    if wndName then
        self.common_vfx_dianjifankui:SetActive(true)
        self.common_vfx_dianjifankui.transform:SetParent(root.transform:Find("Bg/EffectRoot"))
        self.common_vfx_dianjifankui.transform.localScale = Vector3.one
        self.common_vfx_dianjifankui.transform.localEulerAngles = Vector3.zero
        self.common_vfx_dianjifankui.transform.localPosition = Vector3.zero
        self.clickTick = RegisterTickOnce(function()
            self.common_vfx_dianjifankui:SetActive(false)
            CUIManager.ShowUI(wndName)
        end, 150)
    end
end

function LuaHanJia2023MainWnd:OnEnable()
    --请求雪魄数据
    Gac2Gas.RequestXuePoLiXianData()
    Gac2Gas.QueryXianKeLaiPlayData()
    g_ScriptEvent:AddListener("HanJia2023_SyncXuePoLiXianData", self, "OnSyncXuePoLiXianData")
    g_ScriptEvent:AddListener("HanJia2023_SyncXianKeLianData", self, "OnSyncXianKeLaiData")
    g_ScriptEvent:AddListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
    g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")

end

function LuaHanJia2023MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXuePoLiXianData", self, "OnSyncXuePoLiXianData")
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXianKeLianData", self, "OnSyncXianKeLaiData")
    g_ScriptEvent:RemoveListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
    g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")

end

function LuaHanJia2023MainWnd:OnUpdateActivity()
    self.GiftButton:SetActive(CScheduleMgr.Inst:IsCanJoinSchedule(self.springCouponsActivityId, true))
    self.NewButton:SetActive(CScheduleMgr.Inst:IsCanJoinSchedule(self.newLimitTimeActivityId, true))
end

function LuaHanJia2023MainWnd:OnEnterScene()
    CUIManager.CloseUI(CLuaUIResources.HanJia2023MainWnd)
end

function LuaHanJia2023MainWnd:OnSyncXuePoLiXianData()
    --数据先判空!
    if LuaHanJia2023Mgr.snowAdventureData then
        local singlePassNumber = 0
        local singleTotalNumber = 0
        local multipleRewardNumber = 0
        local multipleTotalNumber = 0

        for k, v in pairs(LuaHanJia2023Mgr.snowAdventureData.singleInfo) do
            if k < 400 then
                singleTotalNumber = singleTotalNumber + 1
                if v then
                    singlePassNumber = singlePassNumber + 1
                end
            else
                local getRewardTimes = LuaHanJia2023Mgr.snowAdventureData.rewardInfo[k] or 0
                multipleRewardNumber = multipleRewardNumber + getRewardTimes
                multipleTotalNumber = multipleTotalNumber + HanJia2023_XuePoLiXianLevel.GetData(k).RewardTimes
            end
        end
        self.SingleAdventureButton.transform:Find("StateLb"):GetComponent(typeof(UILabel)).text =
        LocalString.GetString("通关 ") .. singlePassNumber .. "/" .. singleTotalNumber

        self.MultipleAdventureButton.transform:Find("StateLb"):GetComponent(typeof(UILabel)).text =
        LocalString.GetString("奖励 ") .. multipleRewardNumber .. "/" .. multipleTotalNumber

        self.SingleAdventureButton.transform:Find("Closed").gameObject:SetActive(not self:CheckScheduleOpen(self.singleActivityId, self.hanjiaJieRiId, false, false))
        self.MultipleAdventureButton.transform:Find("Closed").gameObject:SetActive(not self:CheckScheduleOpen(self.multipleActivityId, self.hanjiaJieRiId, false, false))
        self.SnowmanPVEButton.transform:Find("Closed").gameObject:SetActive(not self:CheckScheduleOpen(self.snowmanPVEActivityId, self.hanjiaJieRiId, false, false))
        self.LotteryButton.transform:Find("Closed").gameObject:SetActive(not self:CheckScheduleOpen(self.lotteryActivityId, self.hanjiaJieRiId,  false, false))
        self.BattlePassButton.transform:Find("Closed").gameObject:SetActive(not self:CheckScheduleOpen(self.battlePassActivityId, self.hanjiaJieRiId, false, false))

        self.GiftButton:SetActive(CScheduleMgr.Inst:IsCanJoinSchedule(self.springCouponsActivityId, true))
        self.NewButton:SetActive(CScheduleMgr.Inst:IsCanJoinSchedule(self.newLimitTimeActivityId, true))
    end
end

function LuaHanJia2023MainWnd:OnSyncXianKeLaiData()
    --数据先判空!
    if LuaHanJia2023Mgr.XKLData then
        self.BattlePassButton.transform:Find("StateLb"):GetComponent(typeof(UILabel)).text =
        LocalString.GetString("Lv.") .. LuaHanJia2023Mgr.XKLData.Lv .. "/" .. LuaHanJia2023Mgr.XKLData.CfgMaxLv
    else
        self.BattlePassButton.transform:Find("StateLb"):GetComponent(typeof(UILabel)).text = LocalString.GetString("Lv.0/100")
    end
end

function LuaHanJia2023MainWnd:CheckScheduleOpen(activityId, JieRiId, showLevelMsg, showTaskNotOpenMsg)
    local scheduleData = Task_Schedule.GetData(activityId)
    local taskData = Task_Task.GetData(scheduleData.TaskID[0])
    local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0

    if taskData and playerLv < taskData.Level then
        if showLevelMsg then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("参加%s玩法需%s级"), scheduleData.TaskName, tostring(taskData.Level)))
        end
        return true
    elseif taskData and playerLv >= taskData.Level then
        --判断完等级之后, 再根据服务器数据判断一下这个玩法有没有开启吧..
        local openFestivalTasks = CLuaScheduleMgr.m_OpenFestivalTasks[JieRiId] or {}
        local taskId = scheduleData.TaskID[0]
        for i = 1, #openFestivalTasks do
            if openFestivalTasks[i] == taskId then
                return true
            end
        end
        --这个活动没开启
        if showTaskNotOpenMsg then
            g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
        end
        return false
    else
        --这个是保险起见, 一般不会跑到这里
        if showTaskNotOpenMsg then
            g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
        end
        return false
    end
end


--@region UIEvent

--@endregion UIEvent

