require("common/common_include")

local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local UIInput = import "UIInput"
local CUICommonDef = import "L10.UI.CUICommonDef"


CLuaCommonAnnounceWnd = class()
CLuaCommonAnnounceWnd.Path = "ui/common/LuaCommonAnnounceWnd"
RegistClassMember(CLuaCommonAnnounceWnd, "m_UIInput")

function CLuaCommonAnnounceWnd:Init()
    self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel)).text = CLuaCommonAnnounceMgr.m_WndTitle
    self.transform:Find("Anchor/MottoInput/Label"):GetComponent(typeof(UILabel)).text = CLuaCommonAnnounceMgr.m_InputTip
    self.transform:Find("Anchor/Static/Label"):GetComponent(typeof(UILabel)).text = CLuaCommonAnnounceMgr.m_MaxByteLengthTip

    UIEventListener.Get(self.transform:Find("Anchor/CancleBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
        CUIManager.CloseUI(CLuaUIResources.CommonAnnounceWnd)
    end)
    UIEventListener.Get(self.transform:Find("Anchor/ChangeBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
        CLuaCommonAnnounceMgr.m_ChangeFunc(self.m_UIInput.value)
    end)
    self.m_UIInput = self.transform:Find("Anchor/MottoInput"):GetComponent(typeof(UIInput))
    if CLuaCommonAnnounceMgr.m_InputValue and CLuaCommonAnnounceMgr.m_InputValue ~= "" then
      self.m_UIInput.value = CLuaCommonAnnounceMgr.m_InputValue
    end
    CommonDefs.AddEventDelegate(self.m_UIInput.onChange, DelegateFactory.Action(function ( ... )
        local str = self.m_UIInput.value
            if CUICommonDef.GetStrByteLength(str) > CLuaCommonAnnounceMgr.m_MaxByteLength then
            repeat
                str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
            until not (CUICommonDef.GetStrByteLength(str) > CLuaCommonAnnounceMgr.m_MaxByteLength)
            self.m_UIInput.value = str
        end
    end))
end
