local DelegateFactory       = import "DelegateFactory"
local UITabBar              = import "L10.UI.UITabBar"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel            = import "L10.Game.EChatPanel"
local AlignType             = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr        = import "CPlayerInfoMgr"
local Profession            = import "L10.Game.Profession"

LuaCuJuRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCuJuRankWnd, "rankLabel")
RegistClassMember(LuaCuJuRankWnd, "myRank")
RegistClassMember(LuaCuJuRankWnd, "myJob")
RegistClassMember(LuaCuJuRankWnd, "myName")
RegistClassMember(LuaCuJuRankWnd, "myWDL")
RegistClassMember(LuaCuJuRankWnd, "myGoal")
RegistClassMember(LuaCuJuRankWnd, "mySteal")
RegistClassMember(LuaCuJuRankWnd, "tableView")
RegistClassMember(LuaCuJuRankWnd, "tabBar")

RegistClassMember(LuaCuJuRankWnd, "myRankNum")
RegistClassMember(LuaCuJuRankWnd, "myRankInfo")
RegistClassMember(LuaCuJuRankWnd, "rankInfo")
RegistClassMember(LuaCuJuRankWnd, "rankSpriteTbl")

function LuaCuJuRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
end

-- 初始化UI组件
function LuaCuJuRankWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.rankLabel = anchor:Find("Info/Rank"):GetComponent(typeof(UILabel))
    self.myRank = anchor:Find("SelfInfo/Rank"):GetComponent(typeof(UILabel))
    self.myName = anchor:Find("SelfInfo/Name"):GetComponent(typeof(UILabel))
    self.myJob = anchor:Find("SelfInfo/Job"):GetComponent(typeof(UISprite))
    self.myWDL = anchor:Find("SelfInfo/WDL"):GetComponent(typeof(UILabel))
    self.myGoal = anchor:Find("SelfInfo/Goal"):GetComponent(typeof(UILabel))
    self.mySteal = anchor:Find("SelfInfo/Steal"):GetComponent(typeof(UILabel))
    self.tableView = anchor:Find("TableView"):GetComponent(typeof(QnTableView))
    self.tabBar = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetComponent(typeof(UITabBar))
end

function LuaCuJuRankWnd:OnEnable()
    g_ScriptEvent:AddListener("CuJuRankUpdate", self, "OnCuJuRankUpdate")
end

function LuaCuJuRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("CuJuRankUpdate", self, "OnCuJuRankUpdate")
end

function LuaCuJuRankWnd:OnCuJuRankUpdate(isTotalData, myRank, myRankInfo, totalRank)
    local index = isTotalData and 1 or 0
    if myRankInfo then
        self.myRankNum[index] = myRank
        self.myRankInfo[index] = myRankInfo
    end

    if totalRank then
        self.rankInfo[index] = {}
        for i = 1, #totalRank do
            table.insert(self.rankInfo[index], totalRank[i])
        end
    end

    if index == self.tabBar.SelectedIndex then
        self:UpdateMainPlayerInfo()
        self.tableView:ReloadData(true, false)
    end
end


function LuaCuJuRankWnd:Init()
    self:InitMainPlayerInfo()
    self:InitTableView()
    self.tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index)
    end)
    self.tabBar:ChangeTab(0, true)

    Gac2Gas.QueryCuJuWeekRank()
    Gac2Gas.QueryCuJuTotalRank()
end

function LuaCuJuRankWnd:OnTabChange(index)
    if index == 0 then
        self.rankLabel.text = LocalString.GetString("本周排名")
    else
        self.rankLabel.text = LocalString.GetString("历史排名")
    end
    self:UpdateMainPlayerInfo()
    self.tableView:ReloadData(true, false)
end

function LuaCuJuRankWnd:InitMainPlayerInfo()
    self.myRankNum = {}
    self.myRankInfo = {}
    self.myName.text = CClientMainPlayer.Inst.RealName
    self.myJob.spriteName = Profession.GetIconByNumber(EnumToInt(CClientMainPlayer.Inst.Class))
end

function LuaCuJuRankWnd:UpdateMainPlayerInfo()
    local index = self.tabBar.SelectedIndex
    local info = self.myRankInfo[index]
    if not info then
        self.myRank.text = LocalString.GetString("未上榜")
        self.myWDL.text = LocalString.GetString("—")
        self.myGoal.text = LocalString.GetString("—")
        self.mySteal.text = LocalString.GetString("—")
        return
    end

    self.myRank.text = (self.myRankNum[index] and self.myRankNum[index] > 0) and self.myRankNum[index] or LocalString.GetString("未上榜")

    local isWeek = index == 0
    self.myGoal.text = (isWeek and info.WeekGoalCount or info.TotalGoalCount) or 0
    self.mySteal.text = (isWeek and info.WeekStealCount or info.TotalStealCount) or 0
    local win = (isWeek and info.WeekWinCount or info.TotalWinCount) or 0
    local draw = (isWeek and info.WeekDrawCount or info.TotalDrawCount) or 0
    local lose = (isWeek and info.WeekLoseCount or info.TotalLoseCount) or 0
    self.myWDL.text = SafeStringFormat3("%d/%d/%d", win, draw, lose)
end


function LuaCuJuRankWnd:InitTableView()
    self.rankInfo = {}
    self.rankInfo[0] = {}
    self.rankInfo[1] = {}
    self.rankSpriteTbl = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}

    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankInfo[self.tabBar.SelectedIndex]
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item, index)
        end
    )
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.rankInfo[self.tabBar.SelectedIndex][row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.Id, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)
end

function LuaCuJuRankWnd:InitItem(item, index)
    local tf = item.transform

    local job = tf:Find("Job"):GetComponent(typeof(UISprite))
    local name = tf:Find("Name"):GetComponent(typeof(UILabel))
    local rankNum = tf:Find("Rank"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("Rank/Sprite"):GetComponent(typeof(UISprite))
    local wdl = tf:Find("WDL"):GetComponent(typeof(UILabel))
    local goal = tf:Find("Goal"):GetComponent(typeof(UILabel))
    local steal = tf:Find("Steal"):GetComponent(typeof(UILabel))

    local info = self.rankInfo[self.tabBar.SelectedIndex][index + 1]
    local rank = index + 1

    rankNum.text = ""
    rankSprite.spriteName = ""
    if rank >= 1 and rank <= 3 then
        rankSprite.spriteName = self.rankSpriteTbl[rank]
    else
        rankNum.text = rank
    end

    local isWeek = self.tabBar.SelectedIndex == 0
    job.spriteName = Profession.GetIconByNumber(EnumToInt(info.Class))
    name.text = info.Name
    goal.text = (isWeek and info.WeekGoalCount or info.TotalGoalCount) or 0
    steal.text = (isWeek and info.WeekStealCount or info.TotalStealCount) or 0
    local win = (isWeek and info.WeekWinCount or info.TotalWinCount) or 0
    local draw = (isWeek and info.WeekDrawCount or info.TotalDrawCount) or 0
    local lose = (isWeek and info.WeekLoseCount or info.TotalLoseCount) or 0
    wdl.text = SafeStringFormat3("%d/%d/%d", win, draw, lose)
end

--@region UIEvent
--@endregion UIEvent
