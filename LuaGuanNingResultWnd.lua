local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CScene=import "L10.Game.CScene"
local Vector4=import "UnityEngine.Vector4"
local UILabel = import "UILabel"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CUITexture = import "L10.UI.CUITexture"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIFx = import "L10.UI.CUIFx"
local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local EChatPanel = import "L10.Game.EChatPanel"

require "ui/guanning/LuaGuanNingResultPane"

CLuaGuanNingResultWnd = class()
RegistClassMember(CLuaGuanNingResultWnd,"huxiaoScoreLabel")
RegistClassMember(CLuaGuanNingResultWnd,"longyinScoreLabel")
RegistClassMember(CLuaGuanNingResultWnd,"occupyScoreLabel")
RegistClassMember(CLuaGuanNingResultWnd,"requestDoubleScoreBtn")
RegistClassMember(CLuaGuanNingResultWnd,"requestDoubleScoreNode")
RegistClassMember(CLuaGuanNingResultWnd,"scoreLabel")
RegistClassMember(CLuaGuanNingResultWnd,"uiFx")
RegistClassMember(CLuaGuanNingResultWnd,"fxWidget")
RegistClassMember(CLuaGuanNingResultWnd,"battleDataButton")
RegistClassMember(CLuaGuanNingResultWnd,"clsRankButton")
RegistClassMember(CLuaGuanNingResultWnd,"clsRankLabel")
RegistClassMember(CLuaGuanNingResultWnd,"huxiaoResultSprite")
RegistClassMember(CLuaGuanNingResultWnd,"longyinResultSprite")
RegistClassMember(CLuaGuanNingResultWnd,"resultPane1")
RegistClassMember(CLuaGuanNingResultWnd,"resultPane2")
RegistClassMember(CLuaGuanNingResultWnd,"shareButton")
RegistClassMember(CLuaGuanNingResultWnd,"FilterCls")
RegistClassMember(CLuaGuanNingResultWnd,"mHuxiaoPlayInfoList")
RegistClassMember(CLuaGuanNingResultWnd,"mLongyinPlayInfoList")

RegistClassMember(CLuaGuanNingResultWnd, "m_CommandEvaluation")
RegistClassMember(CLuaGuanNingResultWnd, "m_CommandEvaluationTick")
RegistClassMember(CLuaGuanNingResultWnd, "m_PraisedHint")
RegistClassMember(CLuaGuanNingResultWnd, "m_ThumbUpDataInited")
RegistClassMember(CLuaGuanNingResultWnd, "m_LastPraisedCnt")
RegistClassMember(CLuaGuanNingResultWnd, "m_PraisedCloseTick")
RegistClassMember(CLuaGuanNingResultWnd, "m_PraisedRefreshTick")

RegistChildComponent(CLuaGuanNingResultWnd, "m_BattleDataButtonAlertSprite", "BattleDataButtonAlertSprite", GameObject)
RegistChildComponent(CLuaGuanNingResultWnd, "m_BreakRecordFx", "BreakRecordFx", CUIFx)

RegistClassMember(CLuaGuanNingResultWnd,"m_IsTrainScene")
RegistClassMember(CLuaGuanNingResultWnd, "m_GameEnd")

function CLuaGuanNingResultWnd:Awake()
    self.m_IsTrainScene=false
    if CScene.MainScene and CScene.MainScene.GamePlayDesignId==51100208 then
        self.m_IsTrainScene=true
    end
    
    self.huxiaoScoreLabel = self.transform:Find("Anchor/Left/Statics/ScoreLabel"):GetComponent(typeof(UILabel))
    self.longyinScoreLabel = self.transform:Find("Anchor/Right/Statics/ScoreLabel"):GetComponent(typeof(UILabel))
    self.occupyScoreLabel = {}
    self.occupyScoreLabel[0]=FindChild(self.transform,"OccupyScoreLabel1"):GetComponent(typeof(UILabel))
    self.occupyScoreLabel[1]=FindChild(self.transform,"OccupyScoreLabel2"):GetComponent(typeof(UILabel))

    self.requestDoubleScoreBtn = self.transform:Find("Anchor/RequestDoubleScore/RquestDoubelScoreBtn").gameObject
    self.requestDoubleScoreNode = self.transform:Find("Anchor/RequestDoubleScore").gameObject
    self.scoreLabel = self.transform:Find("Anchor/RequestDoubleScore/ScoreLabel"):GetComponent(typeof(UILabel))
    self.uiFx = self.transform:Find("Anchor/RequestDoubleScore/RquestDoubelScoreBtn/Fx"):GetComponent(typeof(CUIFx))
    self.fxWidget = self.transform:Find("Anchor/RequestDoubleScore/RquestDoubelScoreBtn"):GetComponent(typeof(UIWidget))
    self.battleDataButton = self.transform:Find("Anchor/BattleDataButon").gameObject
    self.clsRankButton = self.transform:Find("Anchor/ClassRankButton").gameObject
    self.clsRankLabel = self.transform:Find("Anchor/ClassRankButton/Label"):GetComponent(typeof(UILabel))
    self.huxiaoResultSprite = self.transform:Find("Anchor/Sprite/Sprite"):GetComponent(typeof(UISprite))
    self.longyinResultSprite = self.transform:Find("Anchor/Sprite/Sprite (1)"):GetComponent(typeof(UISprite))

    self.m_CommandEvaluation = self.transform:Find("Anchor/CommandEvaluation").gameObject
    self.m_CommandEvaluation:SetActive(false)
    UIEventListener.Get(self.m_CommandEvaluation.transform:Find("CloseButton").gameObject).onClick = 
        DelegateFactory.VoidDelegate(function(go) self.m_CommandEvaluation:SetActive(false) end)
    UIEventListener.Get(self.m_CommandEvaluation.transform:Find("NoCommentButton").gameObject).onClick = 
        DelegateFactory.VoidDelegate(function(go) self.m_CommandEvaluation:SetActive(false) end)
    UIEventListener.Get(self.m_CommandEvaluation.transform:Find("GoodButton").gameObject).onClick = 
        DelegateFactory.VoidDelegate(function(go) 
            if CLuaGuanNingMgr.m_PlayId and CLuaGuanNingMgr.m_ServerGroupId then
                Gac2Gas.ReportGnjcZhiHuiEvaluateResult(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId, 1) 
                self.m_CommandEvaluation:SetActive(false)
            end
        end)
    UIEventListener.Get(self.m_CommandEvaluation.transform:Find("BadButton").gameObject).onClick = 
        DelegateFactory.VoidDelegate(function(go) 
            if CLuaGuanNingMgr.m_PlayId and CLuaGuanNingMgr.m_ServerGroupId then
                Gac2Gas.ReportGnjcZhiHuiEvaluateResult(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId, 2) 
                self.m_CommandEvaluation:SetActive(false)
            end
        end)

    self.m_PraisedHint = self.transform:Find("Anchor/PraisedHint").gameObject
    self.m_PraisedHint:SetActive(false)

    self.resultPane1 = CLuaGuanNingResultPane:new()
    self.resultPane2 = CLuaGuanNingResultPane:new()
    self.resultPane1:Init(self.transform:Find("Anchor/Left"))
    self.resultPane2:Init(self.transform:Find("Anchor/Right"))

    self.shareButton = self.transform:Find("Anchor/ShareButton").gameObject
    -- self.m_ShareIndex = 0
    self.FilterCls = false
    self.mHuxiaoPlayInfoList = {}
    self.mLongyinPlayInfoList = {}

    self.huxiaoResultSprite.gameObject:SetActive(false)
    self.longyinResultSprite.gameObject:SetActive(false)
    self.battleDataButton:SetActive(false)

    self.uiFx.gameObject:SetActive(false)

    UIEventListener.Get(self.requestDoubleScoreBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:RequestGetDoubleScore(go) end)

    UIEventListener.Get(self.battleDataButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickBattleDataButton(go) end)
    UIEventListener.Get(self.clsRankButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickClsRankButton(go) end)
    self.FilterCls = false
    self.clsRankLabel.text = LocalString.GetString("本职业排行")

    UIEventListener.Get(self.shareButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickShareButton(go) end)
    --渠道服只能分享至梦岛
    if CLuaGuanNingMgr.IsOnlyShare2PersonalSpace() then
        self.shareButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text=LocalString.GetString("分享至梦岛")
    end
    self.shareButton:SetActive(false)
    self.m_BattleDataButtonAlertSprite:SetActive(false)
end

function CLuaGuanNingResultWnd:Init( )
    self.m_ThumbUpDataInited = false

    -- print("CLuaGuanNingResultWnd:Init")
    --每次加载，需要重新请求一次，刷新界面
    self:Request()

    if CLuaGuanNingMgr.m_IsGameEnd then
        self.scoreLabel.text = SafeStringFormat3(LocalString.GetString("获得关宁积分：%d"), CLuaGuanNingMgr.m_Score or 0)
    else
        self.scoreLabel.text = ""
    end

    self:UpdateDoubleScoreShow()
end

function CLuaGuanNingResultWnd:UpdateDoubleScoreShow( )
    local label = self.requestDoubleScoreBtn.transform:Find("Label").gameObject:GetComponent(typeof(UILabel))
    if label ~= nil then
        local DoubleScoreTime = GuanNing_Setting.GetData().DoubleScoreTime
        local times = CLuaGuanNingMgr.m_LeftDoubleTimes
        label.text = SafeStringFormat3(LocalString.GetString("领取本周双倍(%d/%d)"),times,DoubleScoreTime)

        if times == 0 then
            CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
        else
            CUICommonDef.SetActive(self.requestDoubleScoreBtn, true, true)
        end
    end
    if self.m_IsTrainScene then
        self.requestDoubleScoreBtn:SetActive(false)
    end
    --游戏没结束 
    if not CLuaGuanNingMgr.m_IsGameEnd then
        CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
    end

    if CLuaGuanNingMgr.m_Score == 0 then
        CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
    end
end

function CLuaGuanNingResultWnd:OnClickShareButton( go) 
    CLuaGuanNingMgr.Share2Web(1)
end

function CLuaGuanNingResultWnd:OnClickBattleDataButton( go) 
    CLuaGuanNingMgr.ShowLastBattleData = true
    self.m_BattleDataButtonAlertSprite:SetActive(false)
    CUIManager.ShowUI(CUIResources.GuanNingBattleDataWnd)
end
function CLuaGuanNingResultWnd:OnClickClsRankButton( go) 
    --默认是全职业排行
    if not self.FilterCls then
        self.FilterCls = true
        self.clsRankLabel.text = LocalString.GetString("全职业排行")
        --过滤
        self:FilterMyCls()
    else
        self.FilterCls = false
        self.clsRankLabel.text = LocalString.GetString("本职业排行")
        self:FilterAllCls()
    end
    self.resultPane1:InitData(self.mHuxiaoPlayInfoList)
    self.resultPane2:InitData(self.mLongyinPlayInfoList)
end
function CLuaGuanNingResultWnd:FilterAllCls( )
    -- CommonDefs.ListClear(self.mHuxiaoPlayInfoList)
    -- CommonDefs.ListClear(self.mLongyinPlayInfoList)
    self.mHuxiaoPlayInfoList={}
    self.mLongyinPlayInfoList={}

    
    for i,v in ipairs(CLuaGuanNingMgr.m_HuxiaoPlayInfoList) do
        table.insert( self.mHuxiaoPlayInfoList,v )
    end
    for i,v in ipairs(CLuaGuanNingMgr.m_LongyinPlayInfoList) do
        table.insert( self.mLongyinPlayInfoList,v )
    end
end
function CLuaGuanNingResultWnd:FilterMyCls( )
    self.mHuxiaoPlayInfoList={}
    self.mLongyinPlayInfoList={}
    if CClientMainPlayer.Inst == nil then
        return
    end
    local cls = EnumToInt(CClientMainPlayer.Inst.Class)

    for i,v in ipairs(CLuaGuanNingMgr.m_HuxiaoPlayInfoList) do
        if v.cls==cls then
            table.insert( self.mHuxiaoPlayInfoList,v )
        end
    end
    
    for i,v in ipairs(CLuaGuanNingMgr.m_LongyinPlayInfoList) do
        if v.cls==cls then
            table.insert( self.mLongyinPlayInfoList,v )
        end
    end
end
function CLuaGuanNingResultWnd:RequestGetDoubleScore( go) 
    local msg = g_MessageMgr:FormatMessage("GuanNing_DoubleScore_Confirm")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        Gac2Gas.RequestGetDoubleScore()
        --self.Close();
    end), nil, nil, nil, false)
end
function CLuaGuanNingResultWnd:Request( )
    --查询结果
    Gac2Gas.QueryGnjcPlayInfo()
    if CLuaGuanNingMgr.m_IsGameEnd then
        self.m_GameEnd = true
        Gac2Gas.QueryGuanNingHistoryWeekData()
    end
end
function CLuaGuanNingResultWnd:OnEnable( )
    g_ScriptEvent:AddListener("QueryGnjcPlayInfo", self, "QueryGnjcPlayInfo")
    g_ScriptEvent:AddListener("UpdatePlayerGnjcScore", self, "OnUpdatePlayerGnjcScore")
    g_ScriptEvent:AddListener("GetGuanNingDoubleScoreSuccess", self, "OnGetGuanNingDoubleScoreSuccess")
    g_ScriptEvent:AddListener("GuanNingBreakRecords", self, "OnGuanNingBreakRecords")
    g_ScriptEvent:AddListener("SendGnjcDianZanData", self, "OnSendGnjcDianZanData")
    g_ScriptEvent:AddListener("SendGnjcEvaluateZhiHuiAlert", self, "OnEvaluateZhiHuiAlert")
    CLuaGuanNingMgr:LoadEvaluateZhiHuiAlert()
end
function CLuaGuanNingResultWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("QueryGnjcPlayInfo", self, "QueryGnjcPlayInfo")
    g_ScriptEvent:RemoveListener("UpdatePlayerGnjcScore", self, "OnUpdatePlayerGnjcScore")
    g_ScriptEvent:RemoveListener("GetGuanNingDoubleScoreSuccess", self, "OnGetGuanNingDoubleScoreSuccess")
    g_ScriptEvent:RemoveListener("GuanNingBreakRecords", self, "OnGuanNingBreakRecords")
    g_ScriptEvent:RemoveListener("SendGnjcDianZanData", self, "OnSendGnjcDianZanData")
    g_ScriptEvent:RemoveListener("SendGnjcEvaluateZhiHuiAlert", self, "OnEvaluateZhiHuiAlert")
    CLuaGuanNingMgr.m_BreakRecords = {}
    self:CancelCommandEvaluationTick()
    self:CancelPraisedTick()
end

function CLuaGuanNingResultWnd:OnSendGnjcDianZanData(playId, serverGroupId, praisedCnt, tUpPlayerList)
    CLuaGuanNingMgr.m_PlayId = playId
    CLuaGuanNingMgr.m_ServerGroupId = serverGroupId

    if not self.m_ThumbUpDataInited then
        self.resultPane1:InitThumbUp(tUpPlayerList)
        self.resultPane2:InitThumbUp(tUpPlayerList)

        self:CancelPraisedTick()
        self.m_PraisedRefreshTick = CTickMgr.Register(DelegateFactory.Action(function()
            if CLuaGuanNingMgr.m_PlayId and CLuaGuanNingMgr.m_ServerGroupId then
                Gac2Gas.RequestGnjcDianZanData(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId)
            end
        end), CLuaGuanNingMgr.m_PraisedRefreshInterval * 1000, ETickType.Loop)
    else
        self.resultPane1:RefreshThumbUp(tUpPlayerList)
        self.resultPane2:RefreshThumbUp(tUpPlayerList)
        if praisedCnt > self.m_LastPraisedCnt then
            self.m_PraisedHint.transform:Find("Fx"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_Guanning_beizan.prefab")
            if not self.m_PraisedHint.activeSelf then
                self.m_PraisedHint.transform:Find("Label"):GetComponent(typeof(UILabel)).text = "+"..(praisedCnt - self.m_LastPraisedCnt)
                self.m_PraisedHint:SetActive(true)
                self.m_PraisedCloseTick = CTickMgr.Register(DelegateFactory.Action(function()
                    self.m_PraisedHint.transform:Find("Fx"):GetComponent(typeof(CUIFx)):DestroyFx()
                    self.m_PraisedHint:SetActive(false)
                end), CLuaGuanNingMgr.m_PraisedCloseTime * 1000, ETickType.Once)
            else
                local label = self.m_PraisedHint.transform:Find("Label"):GetComponent(typeof(UILabel))
                label.text = "+"..(tonumber(string.sub(label.text, 2)) + praisedCnt - self.m_LastPraisedCnt)
            end
        end
    end

    self.m_LastPraisedCnt = praisedCnt
    self.m_ThumbUpDataInited = true
end

function CLuaGuanNingResultWnd:CancelPraisedTick()
    self.m_PraisedHint.transform:Find("Fx"):GetComponent(typeof(CUIFx)):DestroyFx()
    self.m_PraisedHint:SetActive(false)
    if self.m_PraisedCloseTick then
        invoke(self.m_PraisedCloseTick)
        self.m_PraisedCloseTick = nil
    end
    if self.m_PraisedRefreshTick then
        invoke(self.m_PraisedRefreshTick)
        self.m_PraisedRefreshTick = nil
    end 
end

function CLuaGuanNingResultWnd:CancelCommandEvaluationTick()
    if self.m_CommandEvaluationTick then
        invoke(self.m_CommandEvaluationTick)
        self.m_CommandEvaluationTick = nil
    end
end

function CLuaGuanNingResultWnd:OnEvaluateZhiHuiAlert(playerId, playerName, playerClass, playerGender, showTime)
    self:CancelCommandEvaluationTick()
    self.m_CommandEvaluation:SetActive(true)
    UIEventListener.Get(self.m_CommandEvaluation.transform:Find("Mask/Avatar").gameObject).onClick = 
        DelegateFactory.VoidDelegate(function(go) 
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
        end)
    self.m_CommandEvaluation.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = playerName
    local cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass)
    local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), playerGender)
    self.m_CommandEvaluation.transform:Find("Mask/Avatar"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender))
    showTime = showTime or GuanNing_Setting.GetData().CommandEvaluationCloseTime
    self.m_CommandEvaluationTick = CTickMgr.Register(DelegateFactory.Action(function()
        self.m_CommandEvaluation:SetActive(false)
    end), showTime * 1000, ETickType.Once)
end

function CLuaGuanNingResultWnd:OnGuanNingBreakRecords()
    self.m_BattleDataButtonAlertSprite:SetActive(true)
    self.m_BreakRecordFx:LoadFx("fx/ui/prefab/UI_gerenxinjilu.prefab")
    --EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_gerenxinjilu.prefab"})
end
function CLuaGuanNingResultWnd:OnGetGuanNingDoubleScoreSuccess( leftDoubleTimes) 
    self:UpdateDoubleScoreShow()
    CUICommonDef.SetActive(self.requestDoubleScoreBtn, false, true)
end

function CLuaGuanNingResultWnd:OnUpdatePlayerGnjcScore( score, expNum, yinPiao) 
    self.scoreLabel.text = SafeStringFormat3(LocalString.GetString("获得关宁积分：%d"), score)
    if score >= 400 then
        self:PlayEffect()
    end
    self:UpdateDoubleScoreShow()
end
function CLuaGuanNingResultWnd:QueryGnjcPlayInfo( )
    if CClientMainPlayer.Inst == nil then
        return
    end
    self:UpdateDoubleScoreShow()
    if CLuaGuanNingMgr.m_IsGameEnd then
        self.huxiaoResultSprite.gameObject:SetActive(true)
        self.longyinResultSprite.gameObject:SetActive(true)
        self.battleDataButton:SetActive(true)
        self.shareButton:SetActive(true)

        if CLuaGuanNingMgr.m_HuxiaoScore > CLuaGuanNingMgr.m_LongyinScore then
            --
            CUICommonDef.SetActive(self.huxiaoResultSprite.gameObject, true, true)
            CUICommonDef.SetActive(self.longyinResultSprite.gameObject, false, true)
            self.huxiaoResultSprite.spriteName = "common_fight_result_win"
            self.longyinResultSprite.spriteName = "common_fight_result_lose"
        elseif CLuaGuanNingMgr.m_HuxiaoScore < CLuaGuanNingMgr.m_LongyinScore then
            CUICommonDef.SetActive(self.huxiaoResultSprite.gameObject, false, true)
            CUICommonDef.SetActive(self.longyinResultSprite.gameObject, true, true)
            self.huxiaoResultSprite.spriteName = "common_fight_result_lose"
            self.longyinResultSprite.spriteName = "common_fight_result_win"
        else
            --平局
            CUICommonDef.SetActive(self.huxiaoResultSprite.gameObject, true, true)
            CUICommonDef.SetActive(self.longyinResultSprite.gameObject, true, true)
            self.huxiaoResultSprite.spriteName = "common_fight_result_tie"
            self.longyinResultSprite.spriteName = "common_fight_result_tie"
        end
    end

    --训练场不显示按钮
    if self.m_IsTrainScene then
        self.requestDoubleScoreBtn:SetActive(false)
        self.scoreLabel.gameObject:SetActive(false)

        self.battleDataButton:SetActive(false)
        self.shareButton:SetActive(false)
    end

    if CLuaGuanNingMgr.lastBattleData == nil then
        self.battleDataButton:SetActive(false)
        self.shareButton:SetActive(false)
    end

    if not CSwitchMgr.EnableGuanNingShare then
        self.shareButton:SetActive(false)
    end

    --先找到位置

    if self.FilterCls then
        self:FilterMyCls()
    else
        self:FilterAllCls()
    end

    self.resultPane1:InitData(self.mHuxiaoPlayInfoList)
    self.resultPane2:InitData(self.mLongyinPlayInfoList)
    
    self.resultPane1:Scroll2MyRow()
    self.resultPane2:Scroll2MyRow()

    self.huxiaoScoreLabel.text = tostring(CLuaGuanNingMgr.m_HuxiaoScore)
    self.longyinScoreLabel.text = tostring(CLuaGuanNingMgr.m_LongyinScore)
    if #CLuaGuanNingMgr.m_OccupyScores == 2 then
        self.occupyScoreLabel[0].text = tostring(CLuaGuanNingMgr.m_OccupyScores[1])
        self.occupyScoreLabel[1].text = tostring(CLuaGuanNingMgr.m_OccupyScores[2])
    end

    if CLuaGuanNingMgr.m_IsGameEnd and CLuaGuanNingMgr.m_CommanderOpen then
        CLuaGuideMgr.TryTriggerGuanNingThumbsUpGuide()
    end
end


function CLuaGuanNingResultWnd:DoAni( bounds, isEllipsePath) 
    LuaTweenUtils.DOKill(self.uiFx.transform, false)
    
    local waypoints = CUICommonDef.GetWayPoints(bounds, 1, isEllipsePath)
    self.uiFx.transform.localPosition = waypoints[0]
    LuaTweenUtils.DOLuaLocalPath(self.uiFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
end
function CLuaGuanNingResultWnd:PlayEffect( )
    self.uiFx:DestroyFx()
    self.uiFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
    self:DoAni(Vector4(-self.fxWidget.width * 0.5, self.fxWidget.height * 0.5, self.fxWidget.width, self.fxWidget.height), false)
end

function CLuaGuanNingResultWnd:OnDestroy()
    CLuaGuanNingMgr.m_PlayerServerNameCache = {}
    if self.m_GameEnd then
        CLuaGuanNingMgr.m_HuXiaoCommanderId = 0
        CLuaGuanNingMgr.m_LongYinCommanderId = 0
        EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
    end
end

function CLuaGuanNingResultWnd:GetGuideGo(methodName)
    if methodName == "GetThumbsUpBtn" then
        if CLuaGuanNingMgr:GetMyForce() == 0 then
            local tbl = self.transform:Find("Anchor/Left/Content/ScrollView/Table")
            if tbl.childCount > 0 then
                local go = tbl:GetChild(0):Find("LThumbUp/Texture").gameObject
                if go.activeSelf then
                    return go
                elseif tbl.childCount > 1 then 
                    return tbl:GetChild(1):Find("LThumbUp/Texture").gameObject
                end
            end
        else
            local tbl = self.transform:Find("Anchor/Right/Content/ScrollView/Table")
            if tbl.childCount > 0 then
                local go = tbl:GetChild(0):Find("RThumbUp/Texture").gameObject
                if go.activeSelf then
                    return go
                elseif tbl.childCount > 1 then 
                    return tbl:GetChild(1):Find("RThumbUp/Texture").gameObject
                end
            end
        end
    elseif methodName == "GetBattleDataBtn" then
        return self.battleDataButton
	end 
    return nil
end

return CLuaGuanNingResultWnd
