local MessageWndManager = import "L10.UI.MessageWndManager"
local Item_Item=import "L10.Game.Item_Item"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Mall_YuanBaoMall=import "L10.Game.Mall_YuanBaoMall"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local QnTabView = import "L10.UI.QnTabView"
local Mall_LingYuMall=import "L10.Game.Mall_LingYuMall"
local CTooltip = import "L10.UI.CTooltip"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local Collider=import "UnityEngine.Collider"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local UIToggle=import "UIToggle"
local LingShou_Skills=import "L10.Game.LingShou_Skills"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local LingShou_Setting=import "L10.Game.LingShou_Setting"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CUITexture=import "L10.UI.CUITexture"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItem = import "L10.Game.CItem"
local CClientFormula = import "L10.Game.CClientFormula"
local CChatLinkMgr = import "CChatLinkMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local QnSelectableButton=import "L10.UI.QnSelectableButton"


CLuaLingShouLearnSkillWnd = class()
RegistClassMember(CLuaLingShouLearnSkillWnd,"skillTemplateIdList")
RegistClassMember(CLuaLingShouLearnSkillWnd,"skillTable")
RegistClassMember(CLuaLingShouLearnSkillWnd,"nameLabel")
RegistClassMember(CLuaLingShouLearnSkillWnd,"descLabel")
RegistClassMember(CLuaLingShouLearnSkillWnd,"icon")
RegistClassMember(CLuaLingShouLearnSkillWnd,"returnBtn")
RegistClassMember(CLuaLingShouLearnSkillWnd,"learnBtn")
RegistClassMember(CLuaLingShouLearnSkillWnd,"templateId")
RegistClassMember(CLuaLingShouLearnSkillWnd,"additionLabel")
RegistClassMember(CLuaLingShouLearnSkillWnd,"lockNumLabel")
RegistClassMember(CLuaLingShouLearnSkillWnd,"lockBtn")
RegistClassMember(CLuaLingShouLearnSkillWnd,"lockBtnLabel")

RegistClassMember(CLuaLingShouLearnSkillWnd,"m_SkillItems")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_JieBanSkillItems")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_Editing")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_LockState")

RegistClassMember(CLuaLingShouLearnSkillWnd,"m_TabView")

RegistClassMember(CLuaLingShouLearnSkillWnd,"m_LingShouSkillTf")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_JieBanSkillTf")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_JieBanSkillBookList")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_SkillItem")

RegistClassMember(CLuaLingShouLearnSkillWnd,"m_LockedPartnerSkillType")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_LockedPartnerSkillPos")


RegistClassMember(CLuaLingShouLearnSkillWnd,"m_NormalSkillBookIds")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_ProfessionalSkillBookIds")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_BookItemId2SkillClsLookup")

RegistClassMember(CLuaLingShouLearnSkillWnd,"m_MoneyCtrl")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_LingYuCtrl")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_IsLingShouJieBan")
RegistClassMember(CLuaLingShouLearnSkillWnd,"m_Item2ClassLookup")

CLuaLingShouLearnSkillWnd.m_AutoBuyJieBanSkillBook=false

function CLuaLingShouLearnSkillWnd:InitLockState()
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details then
        if self:IsJieBan() then
            local partnerInfo  = details.data.Props.PartnerInfo
            for i=1,8 do
                self.m_LockState[i] = false
            end
            local lockedPos = partnerInfo.LockedPartnerSkillPos
            if lockedPos>0 then
                if partnerInfo.LockedPartnerSkillType==1 and partnerInfo.PartnerNormalSkills[lockedPos]>0 then
                    self.m_LockState[partnerInfo.LockedPartnerSkillPos] = true
                elseif partnerInfo.LockedPartnerSkillType==2 and partnerInfo.PartnerProfessionalSkills[lockedPos]>0 then
                    self.m_LockState[partnerInfo.LockedPartnerSkillPos+5] = true
                end
            end
        else
            for i=1,8 do--8个技能
                self.m_LockState[i] = details.data.Props.SkillSlotLockState[i]>0
            end
        end
    end
end
function CLuaLingShouLearnSkillWnd:SetLockState(pos,state)
    self.m_LockState[pos] = state
end
function CLuaLingShouLearnSkillWnd:GetLockState(pos)
    if self:IsJieBan() then
        return self.m_LockState[pos]
    else
        if pos==1 or pos==2 then return false end
        return self.m_LockState[pos]
    end
end
function CLuaLingShouLearnSkillWnd:GetLockCount()
    local ret=0
    if self:IsJieBan() then
        for i=1,8 do
            ret = ret + (self.m_LockState[i] and 1 or 0)
        end
    else
        for i=3,8 do--忽略前面2个
            ret=ret+(self.m_LockState[i] and 1 or 0)
        end
    end
    return ret
end
function CLuaLingShouLearnSkillWnd:GetLockData()
    if not self:IsJieBan() then
        local ret=0
        for i=3,8 do
            if self.m_LockState[i] then
                ret = ret + (bit.lshift(1, (i - 1)))    
            end
        end
        return ret
    else

        for i=1,8 do
            if self.m_LockState[i] then
                if i>5 then
                    return 2,i-5--skilltype, skillPos
                else
                    return 1,i--skilltype, skillPos
                end
            end
        end
        return 0,0
    end
end
function CLuaLingShouLearnSkillWnd:GetNormalSkillCount()
    local count=0
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details then
        local partnerInfo  = details.data.Props.PartnerInfo
        local skillList=partnerInfo.PartnerNormalSkills
        for i=1,5 do
            if skillList[i]>0 then
                count=count+1
            end
        end
    end
    return count
end
function CLuaLingShouLearnSkillWnd:GetProfessionalSkillCount()
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    local count=0
    if details then
        local partnerInfo  = details.data.Props.PartnerInfo
        local skillList=partnerInfo.PartnerProfessionalSkills
        for i=1,3 do
            if skillList[i]>0 then
                count=count+1
            end
        end
    end
    return count
end



function CLuaLingShouLearnSkillWnd:Awake()

    self.m_NormalSkillBookIds={}
    self.m_ProfessionalSkillBookIds={}
    local setting = LingShouPartner_Setting.GetData()
    local normalSkillBooks = setting.NormalSkillBookSet
    for i=1,normalSkillBooks.Length do
        self.m_NormalSkillBookIds[normalSkillBooks[i-1]] = true
        -- table.insert( self.m_NormalSkillBookIds,normalSkillBooks[i-1] )
    end

    local professionalSkillBooks = setting.ProfessionalSkillBookSet
    for i=1,professionalSkillBooks.Length,2 do
        self.m_ProfessionalSkillBookIds[professionalSkillBooks[i-1]] = true
        -- table.insert( self.m_ProfessionalSkillBookIds,professionalSkillBooks[i-1] )
    end

    self.skillTemplateIdList = {}
    self.m_SkillItems={}
    self.m_Editing=false
    self.m_LockState={}

    self.m_LingShouSkillTf=FindChild(self.transform,"LingShouSkills")
    self.m_JieBanSkillTf=FindChild(self.transform,"JieBanSkills")

    self.m_TabView=FindChild(self.transform,"TabBar"):GetComponent(typeof(QnTabView))
    self.m_SkillItem=FindChild(self.transform,"SkillItem").gameObject
    self.m_SkillItem:SetActive(false)

    self.skillTable = self.transform:Find("Content/GameObject/SkillTable"):GetComponent(typeof(QnTableView))
    self.nameLabel = self.transform:Find("Content/2/NameLabel"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("Content/2/Desc/DescLabel"):GetComponent(typeof(UILabel))
    self.icon = self.transform:Find("Content/2/Icon/Icon"):GetComponent(typeof(CUITexture))
    self.returnBtn = self.transform:Find("Content/Common/LockBtn").gameObject
    self.learnBtn = self.transform:Find("Content/2/LearnBtn").gameObject
    self.additionLabel = self.transform:Find("Content/Common/AdditionLabel"):GetComponent(typeof(UILabel))
    self.lockNumLabel = self.transform:Find("Content/Common/LockNumLabel"):GetComponent(typeof(UILabel))
    self.lockBtn = self.transform:Find("Content/Common/LockBtn").gameObject
    self.lockBtnLabel = self.transform:Find("Content/Common/LockBtn/LockBtnlabel"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.learnBtn).onClick=DelegateFactory.VoidDelegate(function(go) self:OnClickLearnButton(go) end)
    UIEventListener.Get(self.lockBtn).onClick=DelegateFactory.VoidDelegate(function(go) self:OnClickLockButton(go) end)




    self:InitSkillLockTable(self.transform:Find("Content/Common/LingShouSkills/Skills"))
    self:InitJieBanSkillLockTable()

    self.m_TabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function(btn,index)
        self:OnSelectTabView(index)
    end)
    -- self:OnSelectTabView(0)

    local lingShouId = CLingShouMgr.Inst.selectedLingShou
    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    self.m_IsLingShouJieBan = lingShouId==bindPartenerLingShouId
end

function CLuaLingShouLearnSkillWnd:GetCurrentSkillTabIndex()
    return self.m_TabView.CurrentIndex
end
function CLuaLingShouLearnSkillWnd:IsJieBan()
    return self.m_TabView.CurrentIndex==1
end
function CLuaLingShouLearnSkillWnd:OnSelectTabView(index)
    if index==0 then
        self.m_LingShouSkillTf.gameObject:SetActive(true)
        self.m_JieBanSkillTf.gameObject:SetActive(false)
        self.additionLabel.gameObject:SetActive(true)
    elseif index==1 then
        self.m_LingShouSkillTf.gameObject:SetActive(false)
        self.m_JieBanSkillTf.gameObject:SetActive(true)
        self.additionLabel.gameObject:SetActive(false)
    end


    local lingShouId = CLingShouMgr.Inst.selectedLingShou
    local details = CLingShouMgr.Inst:GetLingShouDetails(lingShouId)
    if details then
        --看看是否结伴
        if self:IsJieBan() then
            self:InitJieBanSkillLockData(details)
        else
            self:InitSkillLockData(details)
        end
        self:UpdateLockNum()
    end

    self.skillTable:ReloadData(false, false)
    self.skillTable:SetSelectRow(0, true)
end

function CLuaLingShouLearnSkillWnd:InitLearnSkillCost(templateId)
    local transform = self.transform:Find("Content/2/Learn")
    local getNode = transform:Find("Icon/GetNode")
    local countUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local icon = transform:Find("Icon/Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = FindChild(transform,"NameLabel"):GetComponent(typeof(UILabel))
    local moneyCtrl = transform:Find("QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    local lingyuCtrl = transform:Find("QnCostAndOwnMoney2"):GetComponent(typeof(CCurentMoneyCtrl))
    self.m_MoneyCtrl = moneyCtrl
    self.m_LingYuCtrl = lingyuCtrl--有可能是元宝

    local autoBuyToggle = transform:Find("AutoBuyToggle"):GetComponent(typeof(QnSelectableButton))
    local toggleLabel = autoBuyToggle.transform:Find("Label"):GetComponent(typeof(UILabel))

    local getBtn = transform:Find("Icon").gameObject

    countUpdate.templateId = templateId
    countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        if val == 0 then
            countUpdate.format = "[ff0000]{0}[-]/1"
            getNode.gameObject:SetActive(true)
        else
            countUpdate.format = "{0}/1"
            getNode.gameObject:SetActive(false)
        end

        if self:IsJieBan() then
            if not autoBuyToggle.m_IsSelected then--没有自动消耗灵玉
                lingyuCtrl.gameObject:SetActive(false)
                getBtn.gameObject:SetActive(true)
            else
                if val>0 then
                    lingyuCtrl.gameObject:SetActive(false)
                    getBtn.gameObject:SetActive(true)
                else
                    lingyuCtrl.gameObject:SetActive(true)
                    getBtn.gameObject:SetActive(false)
                end
            end
        end
    end)

    autoBuyToggle.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        CLuaLingShouLearnSkillWnd.m_AutoBuyJieBanSkillBook=b
        if not autoBuyToggle.m_IsSelected then--没有自动消耗灵玉
            lingyuCtrl.gameObject:SetActive(false)
            getBtn.gameObject:SetActive(true)
        else
            if countUpdate.count>0 then
                lingyuCtrl.gameObject:SetActive(false)
                getBtn.gameObject:SetActive(true)
            else
                lingyuCtrl.gameObject:SetActive(true)
                getBtn.gameObject:SetActive(false)
            end
        end
    end)
    autoBuyToggle:SetSelected(CLuaLingShouLearnSkillWnd.m_AutoBuyJieBanSkillBook,false)

    UIEventListener.Get(getBtn).onClick=DelegateFactory.VoidDelegate(function(go)
        if countUpdate.count == 0 then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(countUpdate.templateId, false, go.transform, CTooltip.AlignType.Right)
        end
    end)


    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    if template ~= nil then
        nameLabel.text = template.Name
        icon:LoadMaterial(template.Icon)
    else
        nameLabel.text = ""
        icon:Clear()
    end

    if self:IsJieBan() then

    else
        moneyCtrl:SetCost(LingShou_Setting.GetData().LearnSkillCost)
    end

    if self:IsJieBan() then
        autoBuyToggle.gameObject:SetActive(true)

        local setting = LingShouPartner_Setting.GetData()
        local matId = 0--setting.NormalSkillBookBoxId

        local setting = LingShouPartner_Setting.GetData()
        if self.m_NormalSkillBookIds[templateId] then
            matId = setting.NormalSkillBookBoxId
            moneyCtrl:SetCost(setting.LearnNormalSkill_YinPiao)
            toggleLabel.text=LocalString.GetString("数量不足自动消耗元宝")
            lingyuCtrl:SetType(EnumMoneyType.YuanBao,EnumPlayScoreKey.NONE,true)
            local yuanbaoCost = Mall_YuanBaoMall.GetData(matId)
            if yuanbaoCost then
                lingyuCtrl:SetCost(yuanbaoCost.YuanBao)--消耗一个
            else
                lingyuCtrl:SetCost(0)
            end
        elseif self.m_ProfessionalSkillBookIds[templateId] then
            matId = setting.ProfessionalSkillBookBoxId
            moneyCtrl:SetCost(setting.LearnProfessionalSkill_YinPiao)

            toggleLabel.text=LocalString.GetString("数量不足自动消耗灵玉")
            lingyuCtrl:SetType(EnumMoneyType.LingYu_WithBind,EnumPlayScoreKey.NONE,true)
            local lingyuCost = Mall_LingYuMall.GetData(matId)
            if lingyuCost then
                lingyuCtrl:SetCost(lingyuCost.Jade)--消耗一个
            else
                lingyuCtrl:SetCost(0)
            end
        end



    else
        autoBuyToggle.gameObject:SetActive(false)
        getBtn.gameObject:SetActive(true)
        lingyuCtrl.gameObject:SetActive(false)
        moneyCtrl:SetCost(LingShou_Setting.GetData().LearnSkillCost)

    end

    countUpdate:UpdateCount()
end

function CLuaLingShouLearnSkillWnd:Init( )

    self.m_BookItemId2SkillClsLookup = {}
    local keys = {}
    LingShouPartner_Skills.ForeachKey(function(key)
        table.insert( keys,key )
    end)
    for i,v in ipairs(keys) do
        local data = LingShouPartner_Skills.GetData(v)
        self.m_BookItemId2SkillClsLookup[data.ItemTempId] = v
    end

    --排序
    self.skillTemplateIdList = {}
    local countDic = {}
    local list = LingShou_Setting.GetData().SkillItemIds
    CommonDefs.ListIterate(list, DelegateFactory.Action_object(function (___value) 
        local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(___value)
        local count = bindItemCountInBag + notBindItemCountInBag
        countDic[___value] = count
        table.insert( self.skillTemplateIdList, ___value )
    end))
    table.sort( self.skillTemplateIdList, function(a,b)
        if countDic[a]>countDic[b] then
            return true
        elseif countDic[a]<countDic[b] then
            return false
        else
            return a<b
        end
    end )

    self.m_JieBanSkillBookList={}
    local setting = LingShouPartner_Setting.GetData()
    local bookList = setting.JieBanSkillBookList
    local countDic = {}
    for i=1,bookList.Length do
        local id = bookList[i-1]
        local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(id)
        local count = bindItemCountInBag + notBindItemCountInBag
        countDic[id] = count
        table.insert( self.m_JieBanSkillBookList,bookList[i-1] )
    end
    table.sort( self.m_JieBanSkillBookList, function(a,b)
        if countDic[a]>countDic[b] then
            return true
        elseif countDic[a]<countDic[b] then
            return false
        else
            local skillId1 = self.m_BookItemId2SkillClsLookup[a]
            local skillId2 = self.m_BookItemId2SkillClsLookup[b]
            local priority1 = LingShouPartner_Skills.GetData(skillId1).Priority
            local priority2 = LingShouPartner_Skills.GetData(skillId2).Priority
            if priority1 > priority2 then
                return true
            elseif priority1 < priority2 then
                return false
            else
                return a < b
            end
        end
    end )

    self.skillTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            if self:GetCurrentSkillTabIndex()==0 then
                return #self.skillTemplateIdList
            else
                return #self.m_JieBanSkillBookList
            end
        end,
        function(item, index)
            if self:GetCurrentSkillTabIndex()==0 then
                self:InitItem(item.transform,self.skillTemplateIdList[index+1])
            else
                self:InitItem(item.transform,self.m_JieBanSkillBookList[index+1])
            end
        end)

    self.skillTable.OnSelectAtRow = DelegateFactory.Action_int(function (row) 
        --更新标题
        if self:GetCurrentSkillTabIndex()==0 then
            self.templateId = self.skillTemplateIdList[row+1]
        else
            self.templateId = self.m_JieBanSkillBookList[row+1]
        end
        -- print(self.templateId)
        local template = CItemMgr.Inst:GetItemTemplate(self.templateId)
        if template ~= nil then
            self.nameLabel.text = template.Name
            self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(CItem.GetItemDescription(self.templateId,true), false)
            self.icon:LoadMaterial(template.Icon)
        else
            self.nameLabel.text = ""
            self.descLabel.text = ""
            self.icon:Clear()
        end

        self:InitLearnSkillCost(self.templateId)
    end)

    local lingShouId = CLingShouMgr.Inst.selectedLingShou
    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    local jiebanButton = self.m_TabView.transform:Find("Tab2").gameObject
    local skillButton = self.m_TabView.transform:Find("Tab1").gameObject
    local jiebanLabel = jiebanButton.transform:Find("JieBanLabel"):GetComponent(typeof(UILabel))


    self.m_TabView:ChangeTo(bindPartenerLingShouId==lingShouId and 1 or 0)

    self.skillTable:ReloadData(false, false)
    self.skillTable:SetSelectRow(0, true)
end
function CLuaLingShouLearnSkillWnd:UpdateAdditionDesc( )
    local data = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if data ~= nil then
        local addition = CClientFormula.Inst:Formula_201(CLuaLingShouMgr.GetLingShouSkillCount(data.data))
        self.additionLabel.text = g_MessageMgr:FormatMessage("LINGSHOU_SKILL_TIP", tostring(addition))
    else
        self.additionLabel.text = ""
    end
    self:UpdateLockNum()
end

function CLuaLingShouLearnSkillWnd:UpdateLockNum( )
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details ~= nil then
        if self:GetCurrentSkillTabIndex()==0 then
            self.lockNumLabel.text = SafeStringFormat3("%d/%d", details.lockSkillCount, details.maxLockSkillCount)
        else
            local partnerInfo  = details.data.Props.PartnerInfo

            self.lockNumLabel.text = "0/1"
            local lockedPos = partnerInfo.LockedPartnerSkillPos
            if lockedPos>0 then
                if partnerInfo.LockedPartnerSkillType==1 and partnerInfo.PartnerNormalSkills[lockedPos]>0 then
                    self.lockNumLabel.text = "1/1"
                elseif partnerInfo.LockedPartnerSkillType==2 and partnerInfo.PartnerProfessionalSkills[lockedPos]>0 then
                    self.lockNumLabel.text = "1/1"
                end
            end
        end
    else
        self.lockNumLabel.text = ""
    end

end
function CLuaLingShouLearnSkillWnd:OnClickLearnButton( go) 
    local template = CItemMgr.Inst:GetItemTemplate(self.templateId)
    --学习技能
    local find, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, self.templateId)
    if self:GetCurrentSkillTabIndex() == 0 then--判断一下是否是结伴状态
        if self.m_IsLingShouJieBan then
            g_MessageMgr:ShowMessage("LingShou_JieBan_Cannot_Learn_Skill")
        else
            if find then
                Gac2Gas.LingShouRequestLearnNewSkill(CLingShouMgr.Inst.selectedLingShou, EnumItemPlace_lua.Bag, pos, itemId, self.templateId, false)
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(self.templateId, false, self.learnBtn.transform, AlignType.Right)
            end
        end
    elseif self:GetCurrentSkillTabIndex()==1 then
        if not self.m_IsLingShouJieBan then
            g_MessageMgr:ShowMessage("LingShou_NoJieBan_Cannot_Learn_Skill")
            return
        end
        --
        if not find and not CLuaLingShouLearnSkillWnd.m_AutoBuyJieBanSkillBook then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(self.templateId, false, self.learnBtn.transform, AlignType.Right)
            return
        end
        
        if not self.m_MoneyCtrl.moneyEnough then
            g_MessageMgr:ShowMessage("LINGSHOU_LEARN_SKILL_NO_MONEY")
            return
        end
        if not self.m_Item2ClassLookup then
            self.m_Item2ClassLookup = {}
            LingShouPartner_Skills.Foreach(function(k,v)
                if v.OwnerClassRequire>0 then
                    self.m_Item2ClassLookup[v.ItemTempId] = v.OwnerClassRequire
                end
            end)
        end
        local myCls = CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) or 0

        --玩家学习本职业的职业结伴技能
        if self.m_Item2ClassLookup[self.templateId]==myCls then
            local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
            if details then
                local partnerInfo  = details.data.Props.PartnerInfo
                local skillList=partnerInfo.PartnerProfessionalSkills
                --是否有非本职业的职业结伴技能
                local have = false
                for i=1,3 do
                    if skillList[i]%100>=1 then
                        local skillCls = math.floor(skillList[i]/100)
                        if LingShouPartner_Skills.Exists(skillCls) then
                            local ownerClassRequire = LingShouPartner_Skills.GetData(skillCls).OwnerClassRequire
                            if ownerClassRequire>0 and ownerClassRequire~=myCls then
                                have = true
                                break
                            end
                        end
                    end
                end
                if have then
                    local msg = g_MessageMgr:FormatMessage("LingShou_LearnSkill_CanChange",Item_Item.GetData(self.templateId).Name)
                    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                        self:RequestLearnJieBanSkill(find,pos,itemId)
                    end), nil, nil, nil, false)
                    return
                end
    
            end
        end

        self:RequestLearnJieBanSkill(find,pos,itemId)
    end
end
function CLuaLingShouLearnSkillWnd:RequestLearnJieBanSkill(find,pos, itemId)
    local function needAlert()
        local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
        local count=0
        if details then
            local partnerInfo  = details.data.Props.PartnerInfo
            if self.m_NormalSkillBookIds[self.templateId] then
                local skillList=partnerInfo.PartnerNormalSkills
                for i=1,5 do
                    if not self:GetLockState(i) and skillList[i]%100>1 then
                        return true
                    end
                end
            else
                local skillList=partnerInfo.PartnerProfessionalSkills
                local count = 0
                local overLevel = false
                for i=1,3 do
                    if skillList[i]%100>=1 then
                        count=count+1
                    end
                    if skillList[i]%100>1 then
                        overLevel = true
                    end
                end
                if count==3 and overLevel then--概率100% 只有3个
                    return true
                end
            end
        end
        return false
    end

    local function callback()
        if find then
            Gac2Gas.RequestLingShouLearnPartnerSkill(pos, itemId, CLingShouMgr.Inst.selectedLingShou)
        else
            if CLuaLingShouLearnSkillWnd.m_AutoBuyJieBanSkillBook then
                if self.m_LingYuCtrl.moneyEnough then
                    Gac2Gas.RequestLingShouLearnPartnerSkillBySkillCls(CLingShouMgr.Inst.selectedLingShou,self.m_BookItemId2SkillClsLookup[self.templateId])
                else
                    self.m_LingYuCtrl:AddMoney(nil)
                end
            end
        end
    end

    if needAlert() then
        local msg = g_MessageMgr:FormatMessage("JieBan_Replace_HighLvSkill_Confirm",Item_Item.GetData(self.templateId).Name)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            callback()
        end), nil, nil, nil, false)
        
    else
        callback()
    end
end

function CLuaLingShouLearnSkillWnd:OnClickLockButton( go) 
    if self.lockBtnLabel.text == LocalString.GetString("技能锁定") then
        local data = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
        if data ~= nil then
            if not self:IsJieBan() then
                if data.maxLockSkillCount == 0 then
                    g_MessageMgr:ShowMessage("LingShou_0_SkillLock")
                else
                    self.m_Editing=true
                    self:EnterLingShouSkillLockEditMode()
                    self.lockBtnLabel.text = LocalString.GetString("确定")
                    g_MessageMgr:ShowMessage("LingShou_SkillLock_Tip")
                end
            else
                self.m_Editing=true
                self:EnterLingShouSkillLockEditMode()
                self.lockBtnLabel.text = LocalString.GetString("确定")
                g_MessageMgr:ShowMessage("LingShou_SkillLock_Tip")
            end
        end
    else
        self.lockBtnLabel.text = LocalString.GetString("技能锁定")

        --保存锁定的情况
        if self:IsJieBan() then
            local skillType,skillPos = self:GetLockData()
            -- print(CLingShouMgr.Inst.selectedLingShou,skillType,skillPos)
            Gac2Gas.RequestLockLingShouPartnerSkillPos(CLingShouMgr.Inst.selectedLingShou,skillType,skillPos)--lingshouId, skilltype, skillPos
        else
            Gac2Gas.RequestSetSkillSlotLockState(CLingShouMgr.Inst.selectedLingShou, self:GetLockData())
        end
        --把锁定状态重置回去
        -- local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
        -- if details ~= nil then
        self:InitLockState()
        -- end
        self:LeaveLingShouSkillLockEditMode()
        self.m_Editing=false
    end
end


function CLuaLingShouLearnSkillWnd:InitItem(transform,templateId)
    local countLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    local itemCountUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local maskSprite = transform:Find("Mask"):GetComponent(typeof(UISprite))
    local learnedGo = transform:Find("Learn").gameObject

    learnedGo:SetActive(false)
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)


    if self:IsJieBan() then
        local skillCls=self.m_BookItemId2SkillClsLookup[templateId]

        local partnerInfo  = details.data.Props.PartnerInfo
        local skillList=partnerInfo.PartnerNormalSkills
        for i=1,5 do
            if skillList[i]>0 then
                if skillCls == math.floor(skillList[i]/100) then
                    learnedGo:SetActive(true)
                    break
                end
            end
        end
        local skillList=partnerInfo.PartnerProfessionalSkills
        for i=1,3 do
            if skillList[i]>0 then
                if skillCls == math.floor(skillList[i]/100) then
                    learnedGo:SetActive(true)
                    break
                end
            end
        end

    else
        local array = details.data.Props.SkillListForSave
        for i=1,array.Length do
            local item = array[i-1]
            if item>0 then
                local skillCls = math.floor(item/100)
                local data=LingShou_Skills.GetData(skillCls)
                if data and data.ItemTempId==templateId then
                    learnedGo:SetActive(true)
                    break
                end
            end
        end
    end

    local template = CItemMgr.Inst:GetItemTemplate(templateId)
    if template ~= nil then
        icon:LoadMaterial(template.Icon)
    else
        icon.material = nil
    end

    itemCountUpdate.templateId = templateId
    itemCountUpdate.onChange = DelegateFactory.Action_int(function (cnt) 
        if cnt == 0 then
            itemCountUpdate.countLabel.text = ""
            maskSprite.enabled = true
        else
            maskSprite.enabled = false
        end
    end)
    itemCountUpdate:UpdateCount()
end

function CLuaLingShouLearnSkillWnd:OnEnable()
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "OnGetLingShouDetails")
    g_ScriptEvent:AddListener("UpdateLingShouSkillProp", self, "OnUpdateLingShouSkillProp")
    g_ScriptEvent:AddListener("UpdateLingShouSkillSlotLockState", self, "OnUpdateLingShouSkillSlotLockState")
    g_ScriptEvent:AddListener("SyncLingShouPartnerInfo", self, "OnSyncLingShouPartnerInfo")


    self:UpdateAdditionDesc()
    --sf todo
    -- UpdateLingShouSkillSlotLockState
end

function CLuaLingShouLearnSkillWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "OnGetLingShouDetails")
    g_ScriptEvent:RemoveListener("UpdateLingShouSkillProp", self, "OnUpdateLingShouSkillProp")
    g_ScriptEvent:RemoveListener("UpdateLingShouSkillSlotLockState", self, "OnUpdateLingShouSkillSlotLockState")
    g_ScriptEvent:RemoveListener("SyncLingShouPartnerInfo", self, "OnSyncLingShouPartnerInfo")
end
function CLuaLingShouLearnSkillWnd:OnSyncLingShouPartnerInfo(lingShouId,partnerInfo)
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        local details = CLingShouMgr.Inst:GetLingShouDetails(lingShouId)
        if details then
            if self:IsJieBan() then
                self:InitJieBanSkillLockData(details)
            else
                self:InitSkillLockData(details)
            end
            self:UpdateLockNum()

            if self:IsJieBan() then--刷新结伴技能的学习状态
                local lookup={}
                local partnerInfo  = details.data.Props.PartnerInfo
                local skillList=partnerInfo.PartnerNormalSkills
                for i=1,5 do
                    if skillList[i]>0 then
                        local skillCls = math.floor(skillList[i]/100)
                        lookup[LingShouPartner_Skills.GetData(skillCls).ItemTempId] = true
                    end
                end
                local skillList=partnerInfo.PartnerProfessionalSkills
                for i=1,3 do
                    if skillList[i]>0 then
                        local skillCls = math.floor(skillList[i]/100)
                        lookup[LingShouPartner_Skills.GetData(skillCls).ItemTempId] = true
                    end
                end

                --刷新界面
                for i=1,self.skillTable.m_ItemList.Count do
                    local item = self.skillTable.m_ItemList[i-1]
                    if item then
                        local row=item.Row
                        local learnedGo = item.transform:Find("Learn").gameObject
                        if lookup[self.m_JieBanSkillBookList[row+1]] then
                            learnedGo:SetActive(true)
                        else
                            learnedGo:SetActive(false)
                        end
                    end
                end
            end
        end
    end
end

function CLuaLingShouLearnSkillWnd:OnUpdateLingShouSkillSlotLockState(args)
    local lingshouId= args[0]
    if CLingShouMgr.Inst.selectedLingShou==lingshouId then
        self:UpdateLockNum()
        self:InitLockState()

        for i,v in ipairs(self.m_SkillItems) do
            local transform = v
            local setLockTf = transform:Find("HasSkill/SetLock")
            local lockSprite = transform:Find("HasSkill/Lock/Sprite"):GetComponent(typeof(UISprite))

            if self.m_Editing then
            else
                if self:GetLockState(i) then
                    lockSprite.gameObject:SetActive(true)
                else
                    lockSprite.gameObject:SetActive(false)
                end
            end
        end

    end
end
function CLuaLingShouLearnSkillWnd:OnGetLingShouDetails(args)
    local lingshouId,details=args[0],args[1]
    if lingshouId == CLingShouMgr.Inst.selectedLingShou then
        self:RefreshLearnSkillItem(details)
        --锁定状态
        self:InitSkillLockData(details)
    end
end
function CLuaLingShouLearnSkillWnd:RefreshLearnSkillItem(details)
    local lookup={}
    local array = details.data.Props.SkillListForSave
    for i=1,array.Length do
        local item = array[i-1]
        if item>0 then
            local skillCls = math.floor(item/100)
            -- lookup[skillCls] = true
            local data=LingShou_Skills.GetData(skillCls)
            if data then
                lookup[data.ItemTempId] = true
            end
        end
    end
    --刷新界面
    for i=1,self.skillTable.m_ItemList.Count do
        local item = self.skillTable.m_ItemList[i-1]
        if item then
            local row=item.Row
            local learnedGo = item.transform:Find("Learn").gameObject
            if lookup[self.skillTemplateIdList[row+1]] then
                learnedGo:SetActive(true)
            else
                learnedGo:SetActive(false)
            end
        end
    end
end

function CLuaLingShouLearnSkillWnd:OnUpdateLingShouSkillProp(args)
    local lingshouId,skills=args[0],args[1]
    if CLingShouMgr.Inst.selectedLingShou==lingshouId then
        self:UpdateAdditionDesc()
        -- self:OnGetLingShouDetails(lingshouId, CLingShouMgr.Inst:GetLingShouDetails(lingshouId))

        local details = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
        if details ~= nil then
            self:RefreshLearnSkillItem(details)
        --锁定
            self:InitSkillLockData(details)
        end
    end
end

function CLuaLingShouLearnSkillWnd:InitSkillLockTable(transform)
    self.m_SkillItems={}
    local grid = transform:Find("Grid"):GetComponent(typeof(UIGrid))

    for i=1,8 do
        local go = CommonDefs.Object_Instantiate(self.m_SkillItem)
        if i==1 then
            go.transform.parent = transform
            go.transform.localPosition = Vector3(58, 10,0)
        elseif i == 2 then
            go.transform.parent = transform
            go.transform.localPosition = Vector3(207, 10,0)
        else
            go.transform.parent = grid.transform
        end
        go:SetActive(true)
        go.transform.localScale = Vector3.one
        table.insert( self.m_SkillItems,go.transform )
    end

    grid:Reposition()

    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details ~= nil then
        self:InitSkillLockData(details)
    end
end

function CLuaLingShouLearnSkillWnd:InitJieBanSkillLockTable()
    self.m_JieBanSkillItems={}
    local transform = FindChild(self.transform,"JieBanSkills")
    local grid1 = FindChild(transform,"Grid1")
    local grid2 = FindChild(transform,"Grid2")
    for i=1,5 do
        local go=NGUITools.AddChild(grid1.gameObject,self.m_SkillItem)
        go:SetActive(true)
        table.insert( self.m_JieBanSkillItems,go.transform )
    end
    grid1:GetComponent(typeof(UIGrid)):Reposition()
    for i=1,3 do
        local go=NGUITools.AddChild(grid2.gameObject,self.m_SkillItem)
        go:SetActive(true)
        table.insert( self.m_JieBanSkillItems,go.transform )
    end
    grid2:GetComponent(typeof(UIGrid)):Reposition()

    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details ~= nil then
        self:InitJieBanSkillLockData(details)
    end
end

function CLuaLingShouLearnSkillWnd:InitJieBanSkillLockData(details)
    if details then
        self:InitLockState()
        local partnerInfo  = details.data.Props.PartnerInfo
        local skillList1=partnerInfo.PartnerNormalSkills
        for i=1,5 do
            self:InitLockItem(self.m_JieBanSkillItems[i],skillList1[i],i)
        end
        local skillList2=partnerInfo.PartnerProfessionalSkills
        for i=1,3 do
            self:InitLockItem(self.m_JieBanSkillItems[i+5],skillList2[i],i+5)
        end
    end
end

function CLuaLingShouLearnSkillWnd:InitSkillLockData(details)
    if details then
        self:InitLockState()
        local skillList = details.data.Props.SkillListForSave
        for i=1,8 do
            self:InitLockItem(self.m_SkillItems[i],skillList[i],i)
        end
    end
end


function CLuaLingShouLearnSkillWnd:InitLockItem(transform,skillId,pos)
    -- print("InitLockItem",pos)
    local icon = transform:Find("HasSkill/Texture"):GetComponent(typeof(CUITexture))
    local levelLabel = transform:Find("HasSkill/LevelLabel"):GetComponent(typeof(UILabel))
    local hasSkillTf = transform:Find("HasSkill")
    local lockTf = transform:Find("HasSkill/Lock")
    local setLockTf = transform:Find("HasSkill/SetLock")
    local lockSprite = transform:Find("HasSkill/Lock/Sprite"):GetComponent(typeof(UISprite))
    local lockToggle = transform:Find("HasSkill/SetLock"):GetComponent(typeof(UIToggle))

    lockTf.gameObject:SetActive(true)
    setLockTf.gameObject:SetActive(false)
    local  state = self:GetLockState(pos)
    if state then
        lockSprite.gameObject:SetActive(true)
    else
        lockSprite.gameObject:SetActive(false)
    end

    local template = Skill_AllSkills.GetData(skillId)
    if template ~= nil then
        hasSkillTf.gameObject:SetActive(true)
        icon:LoadSkillIcon(template.SkillIcon)
        levelLabel.text = "Lv." .. template.Level

        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = true
        end
    else
        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = false
        end
        hasSkillTf.gameObject:SetActive(false)
        icon.material = nil
        levelLabel.text = ""
    end

    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if self:IsJieBan() then
            if self.m_Editing then
                local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
                if details then
                    if not lockToggle.value and self:GetLockCount()>=1 then
                        g_MessageMgr:ShowMessage("JieBan_LockFail_MoreThan2")
                    elseif not lockToggle.value and pos<=5 and self:GetNormalSkillCount()==1 then
                        g_MessageMgr:ShowMessage("JieBan_LockFail_SelectAllCommon")
                    elseif not lockToggle.value and pos>=6 and self:GetProfessionalSkillCount()==1 then
                            g_MessageMgr:ShowMessage("JieBan_LockFail_SelectAllZhiYe")
                    else
                        lockToggle.value = not lockToggle.value
                        self:SetLockState(pos,lockToggle.value)
                    end
                end
            else
                if skillId>0 then CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil) end
            end
        else
            if self.m_Editing and pos~=1 and pos~=2 then--前两个位置不能编辑
                local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
                if details then
                    if not lockToggle.value and self:GetLockCount()>=details.maxLockSkillCount then
                        g_MessageMgr:ShowMessage("LingShou_SkillLock_OverLimit", details.maxLockSkillCount)
                    else
                        lockToggle.value = not lockToggle.value
                        self:SetLockState(pos,lockToggle.value)
                    end
                end
            else
                if skillId>0 then CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil) end
            end
        end
    end)

end

function CLuaLingShouLearnSkillWnd:EnterLingShouSkillLockEditMode()
    local function lock(i,transform)
        local setLockTf = transform:Find("HasSkill/SetLock")
        local lockTf = transform:Find("HasSkill/Lock")

        lockTf.gameObject:SetActive(false)
        if not self:IsJieBan() then
            if i~=1 and i~=2 then
                setLockTf.gameObject:SetActive(true)
            end
        else
            setLockTf.gameObject:SetActive(true)
        end

        local lockToggle = transform:Find("HasSkill/SetLock"):GetComponent(typeof(UIToggle))
        if self:GetLockState(i) then
            lockToggle.value=true
        else
            lockToggle.value=false
        end
    end

    if not self:IsJieBan() then
        for i,v in ipairs(self.m_SkillItems) do
            lock(i,v)
        end
    else
        for i,v in ipairs(self.m_JieBanSkillItems) do
            lock(i,v)
        end
    end
end

function CLuaLingShouLearnSkillWnd:LeaveLingShouSkillLockEditMode(details)

    local function unlock(i,transform)
        local setLockTf = transform:Find("HasSkill/SetLock")
        local lockTf = transform:Find("HasSkill/Lock")
        local lockSprite = transform:Find("HasSkill/Lock/Sprite"):GetComponent(typeof(UISprite))

        lockTf.gameObject:SetActive(true)
        setLockTf.gameObject:SetActive(false)

        if self:GetLockState(i) then
            lockSprite.gameObject:SetActive(true)
        else
            lockSprite.gameObject:SetActive(false)
        end
    end

    if self:GetCurrentSkillTabIndex()==0 then
        for i,v in ipairs(self.m_SkillItems) do
            unlock(i,v)
        end
    else
        for i,v in ipairs(self.m_JieBanSkillItems) do
            unlock(i,v)
        end
    end
end
