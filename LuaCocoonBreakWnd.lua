local UIPanel = import "UIPanel"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"

LuaCocoonBreakWnd= class()

RegistChildComponent(LuaCocoonBreakWnd, "m_Finger","Finger", GameObject)
RegistChildComponent(LuaCocoonBreakWnd, "m_Center","Center", GameObject)
RegistChildComponent(LuaCocoonBreakWnd, "m_Texture","Texture", UITexture)

RegistClassMember(LuaCocoonBreakWnd, "m_NumOfClickTimes")
RegistClassMember(LuaCocoonBreakWnd, "m_StateNum")
RegistClassMember(LuaCocoonBreakWnd, "m_TickInterval")
RegistClassMember(LuaCocoonBreakWnd, "m_FadeTimeList")
RegistClassMember(LuaCocoonBreakWnd, "m_Amount")
RegistClassMember(LuaCocoonBreakWnd, "m_HideExceptList")
RegistClassMember(LuaCocoonBreakWnd, "m_Tick")
RegistClassMember(LuaCocoonBreakWnd, "m_AimAmount")
RegistClassMember(LuaCocoonBreakWnd, "m_AimAmountList")
RegistClassMember(LuaCocoonBreakWnd, "m_State")

function LuaCocoonBreakWnd:Init()
    self.m_NumOfClickTimes = 0
    self.m_AimAmount ,self.m_Amount = 0,0
    self.m_State = 0
    self.m_TickInterval = 1000 / 30
    self:InitParam()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
    UIEventListener.Get(self.m_Center).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCenterClick()
    end)
    UIEventListener.Get(self.m_Texture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBgClick()
    end)
    self.m_Texture.material:SetFloat("_Amount",self.m_Amount)
    self.m_Texture.gameObject:SetActive(false)
    self.m_Texture.gameObject:SetActive(true)
    self.m_Tick = RegisterTick(function ()
        self:CocoonBreak()
    end,self.m_TickInterval)
end

function LuaCocoonBreakWnd:InitParam()
    self.m_StateNum = 5
    self.m_FadeTimeList = {500,1000,1000,1000,1000}
    self.m_AimAmountList = {0.2,0.4,0.6,0.8,1.0}
end

function LuaCocoonBreakWnd:OnEnable()
    local hideExcept ={ "HeadInfoWnd", "PopupNoticeWnd"}
    self.m_HideExceptList = Table2List(hideExcept, MakeGenericClass(List, cs_string))
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, self.m_HideExceptList, false, false)
    CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, self.m_HideExceptList, false, false)
end

function LuaCocoonBreakWnd:OnDisable()
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, self.m_HideExceptList, false, false)
    CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, self.m_HideExceptList, false, false)
    self:CancelTick()
    if self.m_State == self.m_StateNum then
        self:FinishTask()
    end
end

function LuaCocoonBreakWnd:OnBgClick()
    if self.m_NumOfClickTimes >= 3 then
        self:OnAimAmountChange()
    end
end

function LuaCocoonBreakWnd:OnCenterClick()
    self.m_NumOfClickTimes = self.m_NumOfClickTimes + 1
    if self.m_NumOfClickTimes == 3 then
        self.m_Finger:SetActive(false)
    end
    self:OnAimAmountChange()
end

function LuaCocoonBreakWnd:OnAimAmountChange()
    if self.m_AimAmount <= self.m_Amount then
        self.m_State = self.m_State + 1
        if self.m_State > self.m_StateNum then self.m_State = self.m_StateNum end
        self.m_AimAmount = self.m_AimAmountList[self.m_State]
    end
end

function LuaCocoonBreakWnd:CocoonBreak()
    if self.m_State < 1 or self.m_State > self.m_StateNum then return end
    if self.m_Amount < self.m_AimAmount then
        self.m_Amount = self.m_Amount + 1 / self.m_StateNum / (self.m_FadeTimeList[self.m_State] / self.m_TickInterval)
    end
    self.m_Texture.material:SetFloat("_Amount",self.m_Amount)
    self.m_Texture.gameObject:SetActive(false)
    self.m_Texture.gameObject:SetActive(true)
    if self.m_Amount >= 1 then
        CUIManager.CloseUI(CLuaUIResources.CocoonBreakWnd)
    end
end

function LuaCocoonBreakWnd:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaCocoonBreakWnd:FinishTask()
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_CocoonBreakWnd_TaskId, "CocoonBreak")
end
