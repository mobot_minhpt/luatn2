
LuaChunJie2024XingYunQianWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024XingYunQianWnd, "thankButton", "ThankButton", GameObject)
RegistChildComponent(LuaChunJie2024XingYunQianWnd, "silver", "Silver", UILabel)
RegistChildComponent(LuaChunJie2024XingYunQianWnd, "likeButton", "LikeButton", GameObject)
RegistChildComponent(LuaChunJie2024XingYunQianWnd, "envyButton", "EnvyButton", GameObject)
--@endregion RegistChildComponent end

function LuaChunJie2024XingYunQianWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.thankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnThankButtonClick()	end)

	UIEventListener.Get(self.likeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnLikeButtonClick()	end)

	UIEventListener.Get(self.envyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnEnvyButtonClick()	end)

    --@endregion EventBind end
end

function LuaChunJie2024XingYunQianWnd:Init()

end

--@region UIEvent

function LuaChunJie2024XingYunQianWnd:OnThankButtonClick()
end

function LuaChunJie2024XingYunQianWnd:OnLikeButtonClick()
end

function LuaChunJie2024XingYunQianWnd:OnEnvyButtonClick()
end

--@endregion UIEvent
