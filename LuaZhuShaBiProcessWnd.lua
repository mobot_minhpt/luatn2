local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CChuanjiabaoWordType = import "L10.Game.CChuanjiabaoWordType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CClientChuanJiaBaoMgr=import "L10.Game.CClientChuanJiaBaoMgr"
local UIGrid=import "UIGrid"
local QnTabView=import "L10.UI.QnTabView"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CItemMgr=import "L10.Game.CItemMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CButton = import "L10.UI.CButton"
local CUIFx = import "L10.UI.CUIFx"

CLuaZhuShaBiProcessWnd = class()
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_TabView")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_TableView")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_TabViewRight")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_ShengXingView")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_ZhuShaBiList")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_ChuanJiaBaoList")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_ZhuShaBiShengXingList")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_SelectedItemId")
RegistClassMember(CLuaZhuShaBiProcessWnd, "CurrentSelectTab")

RegistClassMember(CLuaZhuShaBiProcessWnd, "m_JingPingGo")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_JingPingIcon")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_JingPingMask")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_JingPingCountLabel")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_ChuanJiaBaoInHouseIds")
RegistClassMember(CLuaZhuShaBiProcessWnd, "m_TargetStar")


CLuaZhuShaBiProcessWnd.m_SelectedItemId = nil
CLuaZhuShaBiProcessWnd.m_TargetStar = nil

function CLuaZhuShaBiProcessWnd:Awake()
    self.m_ZhuShaBiList={}
    self.m_ChuanJiaBaoList={}
    --self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab"):GetComponent(typeof(QnTabButton)).Selected=true

    local detailTf = self.transform:Find("Anchor/XiLianPanel/Details")

    local autoXiLianButton = detailTf:Find("AutoXiLianButton").gameObject
    local xilianButton = detailTf:Find("XiLianButton").gameObject
    UIEventListener.Get(autoXiLianButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        if CLuaZhuShaBiProcessWnd.m_SelectedItemId then
            CLuaZhuShaBiXiLianMgr.InitCondition()
            CUIManager.ShowUI(CLuaUIResources.ZhuShaBiAutoXiLianWnd)
        end
    end)
    UIEventListener.Get(xilianButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        if CLuaZhuShaBiProcessWnd.m_SelectedItemId then
            CUIManager.ShowUI(CLuaUIResources.ZhuShaBiXiLianWnd)
        end
    end)
    local tipButton = detailTf:Find("TipButton").gameObject
    UIEventListener.Get(tipButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("ChuanJiaBao_Wash_Tip")
    end)

    local shengXingTipButton = self.transform:Find("Anchor/ShengXingPanel/TipButton").gameObject
    UIEventListener.Get(shengXingTipButton).onClick  =DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("ChuanJiaBao_Wash_Tip")
    end)

    local jingpingTf = detailTf:Find("JingPing")
    self.m_JingPingGo=jingpingTf.gameObject
    self.m_JingPingIcon = jingpingTf:Find("Icon"):GetComponent(typeof(CUITexture))

    self.m_JingPingMask=jingpingTf:Find("GetNode").gameObject
    self.m_JingPingCountLabel = jingpingTf:Find("CountLabel"):GetComponent(typeof(UILabel))

    self:UpdateJingPingCount()

    self.CurrentSelectTab = 0
end
function CLuaZhuShaBiProcessWnd:Init()
    self.m_TabView = self.transform:Find("Anchor/XiLianPanel/TabBar"):GetComponent(typeof(QnTabView))
    self.m_TabViewRight = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetComponent(typeof(QnTabView))
    self.m_ShengXingView=self.transform:Find("Anchor/ShengXingPanel/ShengXingView"):GetComponent(typeof(QnTableView))
    self.m_TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
        CLuaZhuShaBiProcessWnd.m_SelectedItemId = nil
        self:InitDetails( nil )
        self.m_TableView:SetSelectRow(0,true)
        self.m_TableView:ReloadData(false, false)
    end)

    self.m_TabViewRight.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
        CLuaZhuShaBiProcessWnd.m_SelectedItemId = nil
        if index == 0 then 
            self.CurrentSelectTab = 0
            self:InitXiLianView()
        else
            self.CurrentSelectTab = 1
            self:InitShenXingView(nil)
        end
        
    end)

    self.m_TableView=self.transform:Find("Anchor/XiLianPanel/MasterView"):GetComponent(typeof(QnTableView))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            if self.m_TabView.CurrentSelectTab==0 then
                --朱砂笔
                return #self.m_ZhuShaBiList 
            else
                --传家宝
                return #self.m_ChuanJiaBaoList 
            end
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        if self.m_TabView.CurrentSelectTab==0 then
            --朱砂笔
            CLuaZhuShaBiProcessWnd.m_SelectedItemId = self.m_ZhuShaBiList[row+1]
        else
            --传家宝
            CLuaZhuShaBiProcessWnd.m_SelectedItemId = self.m_ChuanJiaBaoList[row+1]
        end
        self:InitDetails( CLuaZhuShaBiProcessWnd.m_SelectedItemId )
    end)

    if CClientMainPlayer.Inst.IsCrossServerPlayer then
        self:QueryChuanjiabaoResultReturn()
    else
        if CClientHouseMgr.Inst:HaveHouse() then
            Gac2Gas.QueryChuanjiabaoInHouse()
        else
            self:QueryChuanjiabaoResultReturn()
        end
    end


    self.m_ShengXingView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_ZhuShaBiShengXingList
        end,
        function(item,index)
            self:InitItem(item,index)
        end)

    self.m_ShengXingView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        CLuaZhuShaBiProcessWnd.m_SelectedItemId = self.m_ZhuShaBiShengXingList[row+1]
        self:InitShengXingDetails(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    end)
    
    local targetButton = self.transform:Find("Anchor/ShengXingPanel/Target/TargetButton").gameObject
    
    local function onClickSendButton(go)
        self:OnClickTargetButton(go)
    end

    CommonDefs.AddOnClickListener(targetButton,DelegateFactory.Action_GameObject(onClickSendButton),false)
   
end

function CLuaZhuShaBiProcessWnd:InitXiLianView()
    self:InitDetails( nil )
    self.m_TableView:SetSelectRow(0,true)
    self.m_TableView:ReloadData(false, false)
    local shengXingPanel = self.transform:Find("Anchor/ShengXingPanel")
    local xiLianPanel = self.transform:Find("Anchor/XiLianPanel")
    xiLianPanel.gameObject:SetActive(true)
    shengXingPanel.gameObject:SetActive(false) 
    self.transform:Find("Anchor/Hint").gameObject:SetActive(false)  
end

function CLuaZhuShaBiProcessWnd:InitItem(item,row)
    local itemId = nil
    if self.m_TabViewRight.CurrentSelectTab == 1 then
        itemId = self.m_ZhuShaBiShengXingList[row+1]
    elseif self.m_TabView.CurrentSelectTab==0 then
        itemId = self.m_ZhuShaBiList[row+1]
    else
        itemId = self.m_ChuanJiaBaoList[row+1]
    end
    if itemId then
        local commonItem = CItemMgr.Inst:GetById(itemId)
        -- local design = Item_ItemcommonItem.TemplateId
        local texture = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        texture:LoadMaterial(commonItem.Icon)

        UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)

        local bindSprite = item.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
        bindSprite.gameObject:SetActive(true)
        
        local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = commonItem.Name

        local qualitySprite = item.transform:Find("BorderSprite"):GetComponent(typeof(UISprite))

        local starItem = item.transform:Find("Star").gameObject
        starItem:SetActive(false)

        local starGrid = item.transform:Find("StarGrid").gameObject
        Extensions.RemoveAllChildren(starGrid.transform)
        
        if self.m_TabView.CurrentSelectTab==0 then
            qualitySprite.spriteName = CClientChuanJiaBaoMgr.Inst:GetZhushabiBorder(commonItem)
        else
            qualitySprite.spriteName = CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoBorder(commonItem)
        end

        if commonItem.Item then
            local data = nil
            if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
                bindSprite.spriteName = commonItem.BindOrEquipCornerMark
                if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                    data = CreateFromClass(CChuanjiabaoWordType)
                    data:LoadFromString(commonItem.Item.ExtraVarData.Data)
                end
            else--传家宝
                if self.m_ChuanJiaBaoInHouseIds[itemId] then
                    bindSprite.spriteName = "common_icon_equip"
                else
                    bindSprite.gameObject:SetActive(false)
                end
                data = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
            end

            if data then
                local star = data.Star
                local washedStar = data.WashedStar or 0

                local hasWashResult = washedStar>0--data.LastWashResult and data.LastWashResult.Words.Count>0
                if not hasWashResult then
                    washedStar = star
                end

                local c1 = math.max(star,washedStar)
                local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score
                for i=1,c2 do
                    local g = NGUITools.AddChild(starGrid,starItem)
                    g:SetActive(true)

                    local sprite = g:GetComponent(typeof(UISprite))
                    sprite.alpha = 1

                    if i<=c1 then
                        sprite.spriteName = "playerAchievementView_star_1"
                        if i<=star and i>washedStar then
                            sprite.alpha = 0.3
                        end
                    else
                        sprite.spriteName = "playerAchievementView_star_2"
                    end
                end
                starGrid:GetComponent(typeof(UIGrid)):Reposition()
            end
        end
    end

end
function CLuaZhuShaBiProcessWnd:InitDetails(itemId)
    local detailTf = self.transform:Find("Anchor/XiLianPanel/Details")

    local itemTf = detailTf:Find("Item")

    if CLuaZhuShaBiProcessWnd.m_SelectedItemId then
        UIEventListener.Get(itemTf.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemInfo(CLuaZhuShaBiProcessWnd.m_SelectedItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    else
        UIEventListener.Get(itemTf.gameObject).onClick = nil
    end

    local bindSprite = itemTf:Find("Mark"):GetComponent(typeof(UISprite))
    bindSprite.gameObject:SetActive(true)   
    local texture = itemTf:Find("Icon"):GetComponent(typeof(CUITexture))

    local commonItem = CItemMgr.Inst:GetById(itemId)
    if commonItem then
        texture:LoadMaterial(commonItem.Icon)
        if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
            bindSprite.spriteName = commonItem.BindOrEquipCornerMark
        else
            if self.m_ChuanJiaBaoInHouseIds[itemId] then
                bindSprite.spriteName = "common_icon_equip"
            else
                bindSprite.gameObject:SetActive(false)
            end
        end
        self:UpdateJingPingCount()
    else
        texture:Clear()
        bindSprite.spriteName =nil
    end
end

function CLuaZhuShaBiProcessWnd:OnClickTargetButton(go)
    local function SelectAction(index)
        local targetStarLabel = self.transform:Find("Anchor/ShengXingPanel/Target/StarLabel"):GetComponent(typeof(UILabel))
                
        if index==0 then
            self:RefreshTarget(7)
            
        elseif index==1 then
            self:RefreshTarget(8)
        end       
    end

    local selectShareItem=DelegateFactory.Action_int(SelectAction)

    local t={}
    local item=PopupMenuItemData(LocalString.GetString("7星"),selectShareItem,false,nil)
    table.insert(t, item)
    local item=PopupMenuItemData(LocalString.GetString("8星"),selectShareItem,false,nil)
    table.insert(t, item)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Top,1,nil,600,true,266)
end

function CLuaZhuShaBiProcessWnd:InitShenXingView(itemId)
    local shengXingPanel = self.transform:Find("Anchor/ShengXingPanel")
    local xiLianPanel = self.transform:Find("Anchor/XiLianPanel")
    if #self.m_ZhuShaBiShengXingList == 0 then
        xiLianPanel.gameObject:SetActive(false)
        shengXingPanel.gameObject:SetActive(false)
        self.transform:Find("Anchor/Hint").gameObject:SetActive(true)  
        return
    end

    xiLianPanel.gameObject:SetActive(false)
    shengXingPanel.gameObject:SetActive(true)
    self.transform:Find("Anchor/Hint").gameObject:SetActive(false)
    self.m_ShengXingView:ReloadData(false, false)
    self.m_ShengXingView:SetSelectRow(0,true)    
end

function CLuaZhuShaBiProcessWnd:InitShengXingDetails(itemId)
    local starItem = self.transform:Find("Anchor/ShengXingPanel/Target/Star").gameObject
    starItem:SetActive(false)
    local grid = self.transform:Find("Anchor/ShengXingPanel/Target/StarGrid").gameObject
    Extensions.RemoveAllChildren(grid.transform)

    local commonItem = CItemMgr.Inst:GetById(itemId)
    local item = self.transform:Find("Anchor/ShengXingPanel/ShengXingDetails/Item")
    local texture = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    texture:LoadMaterial(commonItem.Icon)

    UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    local bindSprite = item.transform:Find("Mark"):GetComponent(typeof(UISprite))
    bindSprite.spriteName = commonItem.BindOrEquipCornerMark

    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = commonItem.Name

    local qualitySprite = item:GetComponent(typeof(UISprite))
    qualitySprite.spriteName = CClientChuanJiaBaoMgr.Inst:GetZhushabiBorder(commonItem)

    local data = nil
    data = CreateFromClass(CChuanjiabaoWordType)
    data:LoadFromString(commonItem.Item.ExtraVarData.Data)
    if data then
        local star = data.Star
        local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score
        for i=1,c2 do
            local g = NGUITools.AddChild(grid,starItem)
            g:SetActive(true)
            local sprite = g:GetComponent(typeof(UISprite))
            sprite.alpha = 1
            if i<=star + 1 then
                sprite.spriteName = "playerAchievementView_star_1"
            else
                sprite.spriteName = "playerAchievementView_star_2"
            end            
        end
        grid:GetComponent(typeof(UIGrid)):Reposition()

        local targetStarLabel = self.transform:Find("Anchor/ShengXingPanel/Target/StarLabel"):GetComponent(typeof(UILabel))
        targetStarLabel.text = SafeStringFormat3(LocalString.GetString("%d星"), star+1)
        local originalStarLabel = self.transform:Find("Anchor/ShengXingPanel/ShengXingDetails/Item/StarLabel"):GetComponent(typeof(UILabel))
        originalStarLabel.text = SafeStringFormat3(LocalString.GetString("%d星"), star)
        self.m_TargetStar = star + 1     
        self:RefreshTarget(self.m_TargetStar)
    end   
    
end

function CLuaZhuShaBiProcessWnd:RefreshTarget(targetStar)
    local commonItem = CItemMgr.Inst:GetById(self.m_SelectedItemId)
    local data = nil
    data = CreateFromClass(CChuanjiabaoWordType)
    data:LoadFromString(commonItem.Item.ExtraVarData.Data)
    local star = data.Star
    local targetButton = self.transform:Find("Anchor/ShengXingPanel/Target/TargetButton").gameObject
    local shengXingButton = self.transform:Find("Anchor/ShengXingPanel/ShengXingDetails/ShengXingButton"):GetComponent(typeof(CButton))
    local starLabel = self.transform:Find("Anchor/ShengXingPanel/Target/StarLabel").gameObject
    local label = self.transform:Find("Anchor/ShengXingPanel/Target/Label").gameObject
    local overLabel = self.transform:Find("Anchor/ShengXingPanel/Target/OverLabel").gameObject
    if star == 6 then
        targetButton:SetActive(true)
        shengXingButton.Enabled = true
        starLabel:SetActive(true)
        label:SetActive(true)
        overLabel:SetActive(false)
    elseif star == 7 then
        targetButton:SetActive(false)
        shengXingButton.Enabled = true
        starLabel:SetActive(true)
        label:SetActive(true)
        overLabel:SetActive(false)
    elseif star == 8 then
        targetButton:SetActive(false)
        shengXingButton.Enabled = false
        starLabel:SetActive(false)
        label:SetActive(false)
        overLabel:SetActive(true)
    end
    self.m_TargetStar = targetStar
    local targetStarLabel = self.transform:Find("Anchor/ShengXingPanel/Target/StarLabel"):GetComponent(typeof(UILabel))
    targetStarLabel.text = SafeStringFormat3(LocalString.GetString("%d星"), targetStar)
    local starItem = self.transform:Find("Anchor/ShengXingPanel/Target/Star").gameObject
    starItem:SetActive(false)
    local grid = self.transform:Find("Anchor/ShengXingPanel/Target/StarGrid").gameObject
    local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score

    for i=1,c2 do
        local g = grid.transform:GetChild(i-1)
        local sprite = g:Find("Sprite"):GetComponent(typeof(UISprite))       
        if i<=targetStar then
            sprite.spriteName = "playerAchievementView_star_1"
        else
            sprite.spriteName = "playerAchievementView_star_2"
        end
    end
    self:RefreshShengXingCost()
end

function CLuaZhuShaBiProcessWnd:RefreshShengXingCost()
    local commonItem = CItemMgr.Inst:GetById(self.m_SelectedItemId)
    if commonItem then
        local wordInfo = CreateFromClass(CChuanjiabaoWordType)
        wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
        local star =  wordInfo.Star
        local name = Item_Item.GetData(commonItem.TemplateId).Name
        local lv = 0
        if string.find(name,LocalString.GetString("1级")) then
            lv=1
        elseif string.find(name,LocalString.GetString("2级")) then
            lv=2
        elseif string.find(name,LocalString.GetString("3级")) then
            lv=3
        end
        local costNum=0
        local starData = ChuanjiabaoModel_ImproveStar.GetData(lv)
        if star==6 and self.m_TargetStar==7 then
            costNum = starData.Star6
        elseif star==6 and self.m_TargetStar==8 then
            costNum = starData.Star6 + starData.Star7
        elseif star==7 and self.m_TargetStar==8 then
            costNum = starData.Star7
        end

        local costItemId = ChuanjiabaoModel_Setting.GetData().ImproveStarItem
        local costItem = self.transform:Find("Anchor/ShengXingPanel/ShengXingDetails/JingPing")
        local texture = costItem:Find("Icon"):GetComponent(typeof(CUITexture))
        texture:LoadMaterial(Item_Item.GetData(costItemId).Icon)
        local nameLabel = costItem:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = Item_Item.GetData(costItemId).Name
        local countLabel = costItem:Find("CountLabel"):GetComponent(typeof(UILabel))
        local c,d = CItemMgr.Inst:CalcItemCountInBagByTemplateId(costItemId)
        local count = c + d
        if count<costNum then
            countLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", count, costNum)
        else
            countLabel.text = SafeStringFormat3("[00FF00]%d[-]/%d", count, costNum)
        end
        
        local mask = costItem:Find("GetNode").gameObject
        if count>0 then
            mask:SetActive(false)
        else
            mask:SetActive(true)
        end

        UIEventListener.Get(costItem.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if count>0 then
                CItemInfoMgr.ShowLinkItemTemplateInfo(costItemId,false,nil,AlignType.Default, 0, 0, 0, 0)
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(costItemId, true, nil, AlignType1.Right)
            end
        end)

        local shengXingButton = self.transform:Find("Anchor/ShengXingPanel/ShengXingDetails/ShengXingButton").gameObject
        UIEventListener.Get(shengXingButton).onClick  =DelegateFactory.VoidDelegate(function(go)
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZhuShaBi_ShengXing_Confirm",costNum,self.m_TargetStar), DelegateFactory.Action(function () 
                local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_SelectedItemId)
                if itemInfo then
                    Gac2Gas.RequestImproveChuanjiabaoStar(EnumToInt(itemInfo.place),itemInfo.pos,self.m_SelectedItemId, self.m_TargetStar)--place, pos, itemId
                end
            end), nil, nil, nil, false)
        end)
        
    end
    
end

function CLuaZhuShaBiProcessWnd:OnDestroy()
    CLuaZhuShaBiProcessWnd.m_SelectedItemId = nil
end

function CLuaZhuShaBiProcessWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:AddListener("QueryChuanjiabaoInHouseResult",self,"QueryChuanjiabaoResultReturn")
end
function CLuaZhuShaBiProcessWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:RemoveListener("QueryChuanjiabaoInHouseResult",self,"QueryChuanjiabaoResultReturn")
end


function CLuaZhuShaBiProcessWnd:QueryChuanjiabaoResultReturn( )
    self.m_ZhuShaBiList={}
    self.m_ChuanJiaBaoList={}
    self.m_ZhuShaBiShengXingList={}
    self.m_ChuanJiaBaoInHouseIds={}
    local tabIndex = 0

    local func = function(itemId)
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if commonItem and commonItem.IsItem then
            local type = commonItem.Item.Type
            local wordInfo = nil
            if type==EnumItemType_lua.EvaluatedZhushabi then--EnumItemType.EvaluatedZhushabi
                if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                    wordInfo = CreateFromClass(CChuanjiabaoWordType)
                    wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
                end
                if wordInfo and CLuaZhuShaBiXiLianMgr.CanXiLian(wordInfo) then
                    if CLuaZhuShaBiProcessWnd.m_SelectedItemId == itemId then
                        table.insert( self.m_ZhuShaBiList, 1, itemId )
                        if wordInfo.Star >= 6 and commonItem.IsBinded then
                            table.insert(self.m_ZhuShaBiShengXingList, 1,itemId)
                        end
                        tabIndex=0
                    else
                        table.insert( self.m_ZhuShaBiList, itemId )
                        if wordInfo.Star >= 6 and commonItem.IsBinded then
                            table.insert(self.m_ZhuShaBiShengXingList, itemId)
                        end
                    end
                end
            elseif type==EnumItemType_lua.Chuanjiabao then--Chuanjiabao
                wordInfo = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
                if wordInfo and CLuaZhuShaBiXiLianMgr.CanXiLian(wordInfo) then
                    if CLuaZhuShaBiProcessWnd.m_SelectedItemId == itemId then
                        table.insert( self.m_ChuanJiaBaoList,1, itemId )
                        tabIndex=1
                    else
                        table.insert( self.m_ChuanJiaBaoList, itemId )
                    end
                end
            end
        end
    end

    if CClientHouseMgr.Inst:HaveHouse() and CClientChuanJiaBaoMgr.Inst.chuanjiabaoGroup then
        local dic = CClientChuanJiaBaoMgr.Inst.chuanjiabaoGroup.Chuanjiabaos
        local current = CClientChuanJiaBaoMgr.Inst.chuanjiabaoGroup.CurrentActiveSuite
        -- CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, ___value)
        local data = CommonDefs.DictGetValue_LuaCall(dic,current)
        if data then
            local ids = data.ChuanjiabaoIds
            for i=1,ids.Length do
                local id = ids[i-1]
                if id and id~="" then
                    func(id)
                    self.m_ChuanJiaBaoInHouseIds[id] = id
                end
            end
        end
    end
    -- end))

    local itemProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp or nil
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i=1,bagSize do
        local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
        func(itemId)
    end
    self.m_TabView:ChangeTo(tabIndex)
end
function CLuaZhuShaBiProcessWnd:OnSendItem(args)
    local itemId = args[0]
    local commonItem = CItemMgr.Inst:GetById(itemId)
	if commonItem then
        local setting = ChuanjiabaoModel_Setting.GetData()
        if commonItem.TemplateId == setting.WashYuJingPing or commonItem.TemplateId == setting.WashZiJingPing then
            self:UpdateJingPingCount()
        end
    end

    for i=1,self.m_TableView.m_ItemList.Count do
        local item = self.m_TableView.m_ItemList[i-1]
        local row = item.Row
        if self.m_TabView.CurrentSelectTab==0 then
            if self.m_ZhuShaBiList[row+1] == itemId then
                self:InitItem(item,row)
            end
        else
            if self.m_ChuanJiaBaoList[row+1] == itemId then
                self:InitItem(item,row)
            end
        end
    end

    for i=1,self.m_ShengXingView.m_ItemList.Count do
        local item = self.m_ShengXingView.m_ItemList[i-1]
        local row = item.Row
        if self.m_ZhuShaBiShengXingList[row+1] == itemId then
            self:InitItem(item,row)
            self.m_TargetStar = self.m_TargetStar + 1
            self:InitShengXingDetails(self.m_SelectedItemId)
            local successedFx = self.transform:Find("Anchor/ShengXingPanel/FxNode"):GetComponent(typeof(CUIFx))
            successedFx:LoadFx("Fx/UI/Prefab/UI_yuanshixingji_shengji.prefab")
        end
    end
end

function CLuaZhuShaBiProcessWnd:OnSetItemAt(args)
    local place, pos, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    local update = false
    local setting = ChuanjiabaoModel_Setting.GetData()

    if oldItemId and oldItemId~="" then
        local commonItem = CItemMgr.Inst:GetById(oldItemId)
        if not commonItem then return end
        if commonItem.TemplateId == setting.WashYuJingPing or commonItem.TemplateId == setting.WashZiJingPing then
            update=true
        end
    end
    if newItemId and newItemId~="" then
        local commonItem = CItemMgr.Inst:GetById(newItemId)
        if not commonItem then return end
        if commonItem.TemplateId == setting.WashYuJingPing or commonItem.TemplateId == setting.WashZiJingPing then
            update=true
        end
    end
    if update then
        self:UpdateJingPingCount()
        self:RefreshShengXingCost()
    end
end

function CLuaZhuShaBiProcessWnd:UpdateJingPingCount()
    local setting = ChuanjiabaoModel_Setting.GetData()
    local c,d = CItemMgr.Inst:CalcItemCountInBagByTemplateId(setting.WashZiJingPing)
    local a,b = 0,0
    local isBinded = false
    local commonItem = CItemMgr.Inst:GetById(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    if commonItem then
        isBinded = commonItem.IsBinded
    end
    local count = c+d
    if isBinded then
        a,b = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(setting.WashYuJingPing)
        count = a+b+c+d
    end

    self.m_JingPingCountLabel.text = tostring(count)
    if count>0 then
        self.m_JingPingMask:SetActive(false)
    else
        self.m_JingPingMask:SetActive(true)
    end

    local jingping = setting.WashZiJingPing
    if isBinded and a+b>0 then
        jingping = setting.WashYuJingPing
        self.m_JingPingIcon:LoadMaterial(Item_Item.GetData(setting.WashYuJingPing).Icon)
    else
        self.m_JingPingIcon:LoadMaterial(Item_Item.GetData(setting.WashZiJingPing).Icon)
    end
    UIEventListener.Get(self.m_JingPingGo).onClick = DelegateFactory.VoidDelegate(function(go)
        if count>0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(jingping,false,nil,AlignType.Default, 0, 0, 0, 0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(jingping, true, nil, AlignType1.Right)
        end
    end)
end

function CLuaZhuShaBiProcessWnd:OnDestroy()
    CLuaZhuShaBiProcessWnd.m_SelectedItemId = nil
end
