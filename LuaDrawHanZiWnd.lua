require("common/common_include")

local CHuanLingLineDrawer = import "L10.UI.CHuanLingLineDrawer"
local CUITexture = import "L10.UI.CUITexture"
local CHandDrawMgr = import "L10.Game.CHandDrawMgr"
local CDrawLineMgr = import "L10.Game.CDrawLineMgr"
local Color = import "UnityEngine.Color"
local CUIFx = import "L10.UI.CUIFx"
local UIRoot = import "UIRoot"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local CFadeEffect = import "L10.UI.CFadeEffect"

LuaDrawHanZiWnd = class()

RegistChildComponent(LuaDrawHanZiWnd, "LineDrawer", CHuanLingLineDrawer)
RegistChildComponent(LuaDrawHanZiWnd, "FinishButton", GameObject)
RegistChildComponent(LuaDrawHanZiWnd, "SimplePicture", CUITexture)
RegistChildComponent(LuaDrawHanZiWnd, "FinalPicture", CUITexture)
RegistChildComponent(LuaDrawHanZiWnd, "HuaZhiTextureBG", GameObject)
RegistChildComponent(LuaDrawHanZiWnd, "BgTexture", GameObject)
RegistChildComponent(LuaDrawHanZiWnd, "DrawHint", UILabel)
RegistChildComponent(LuaDrawHanZiWnd, "Mark1", GameObject)
RegistChildComponent(LuaDrawHanZiWnd, "Mark2", GameObject)
RegistChildComponent(LuaDrawHanZiWnd, "Mark3", GameObject)
RegistChildComponent(LuaDrawHanZiWnd, "Mark4", GameObject)
RegistChildComponent(LuaDrawHanZiWnd, "FinalEffect", CUITexture) -- 写完后的整体显示
RegistChildComponent(LuaDrawHanZiWnd, "FinalEffectBG", CUITexture)-- 写完后的背景
RegistChildComponent(LuaDrawHanZiWnd, "AppearFX", CUIFx)

RegistClassMember(LuaDrawHanZiWnd, "CharacterIndex") -- 1-显  2-隐
RegistClassMember(LuaDrawHanZiWnd, "AfterTexture")
RegistClassMember(LuaDrawHanZiWnd, "IsWinSocialWndOpened")

function LuaDrawHanZiWnd:Init()
    self.FinalPicture.gameObject:SetActive(false)
    self.CharacterIndex = LuaZhuJueJuQingMgr.m_HanZiIndex

    local mat = self:GetCharacterMatByIndex(self.CharacterIndex)
    if not mat then
        CUIManager.CloseUI(CLuaUIResources.DrawHanZiWnd)
        return
    end
    self.SimplePicture:LoadMaterial(mat)
    self.FinalPicture:LoadMaterial(mat)
    
    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        self.IsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
    end

    self.FinalEffect:Clear()
    self.FinalEffectBG:Clear()
    self.AfterTexture = nil

    CHandDrawMgr.Inst.NeedDraw = true
    CDrawLineMgr.Inst.LineColor = Color.grey
    CDrawLineMgr.Inst.LineStartWidth = 0.07
    CDrawLineMgr.Inst.LineEndWidth = 0.07
    CDrawLineMgr.Inst.PenMaterialPath = "UI/Texture/Transparent/Material/housewnd_zhushabi_01.mat"
    self.LineDrawer:Init()
    CDrawLineMgr.Inst:AllowDraw()

    CommonDefs.AddOnClickListener(self.FinishButton, DelegateFactory.Action_GameObject(function (go)
        self:OnFinishButtonClicked(go)
    end), false)
end

function LuaDrawHanZiWnd:GetCharacterMatByIndex(index)
    if index == 1 then
        return "UI/Texture/Transparent/Material/HandDrawWnd_xian.mat"
    elseif index == 2 then
        return "UI/Texture/Transparent/Material/HandDrawWnd_yin.mat"
    end
    return nil
end

function LuaDrawHanZiWnd:OnFinishButtonClicked(go)
    CDrawLineMgr.Inst:ForbidDraw()

    if self:IsDrawPassed() then
        self.SimplePicture.gameObject:SetActive(false)
        self.DrawHint.gameObject:SetActive(false)
        self.FinishButton:SetActive(false)
        self.HuaZhiTextureBG:SetActive(false)

        self.LineDrawer:ClearAllLines()
        CFadeEffect.AliveDuration = 1.0
        CFadeEffect.IsDisappear = false
        self.FinalPicture.gameObject:SetActive(true)
        CommonDefs.GetComponent_Component_Type(self.FinalPicture, typeof(CFadeEffect)):Init()

        self.AppearFX:LoadFx("fx/ui/prefab/UI_mapilinfuzhaohuan_baokai_huang.prefab")
        self.BgTexture:SetActive(false)

        RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.DrawHanZiWnd)
        end, 3000)

    else
        g_MessageMgr:ShowMessage("HAND_DRAW_NOT_MATCH")
        self.LineDrawer:ClearAllLines()
        CDrawLineMgr.Inst:AllowDraw()
    end
end

function LuaDrawHanZiWnd:GetCutOut(textureDrawn)
    local centerX = textureDrawn.width / 2
    local centerY = textureDrawn.height / 2

    local width_temp = math.floor(math.abs(self.Mark2.transform.localPosition.x - self.Mark1.transform.localPosition.x))
    local height_temp = math.floor(math.abs(self.Mark3.transform.localPosition.y - self.Mark1.transform.localPosition.y))
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local width = math.floor(width_temp / scale)
    local height = math.floor(height_temp / scale)

    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
            width = self:AdjustForWinWidth(width)
            height = self:AdjustForWinHeight(height)
        end
    end

    local startX = math.floor(centerX - width / 2)
    local startY = math.floor(centerY - height / 2)

    local tmp = textureDrawn:GetPixels(startX, startY, width, height)
    self.AfterTexture = Texture2D(width, height, TextureFormat.ARGB32, false)
    self.AfterTexture:SetPixels(tmp)
    self.AfterTexture:Apply()
    self.AfterTexture.name = "after"

    return self.AfterTexture
end

function LuaDrawHanZiWnd:AdjustForWinWidth(width)
    return width
end

function LuaDrawHanZiWnd:AdjustForWinHeight(height)
    return height
end

function LuaDrawHanZiWnd:IsDrawPassed()
    local drawTexture = self.LineDrawer:GetLineTexture()
    local cutOutTexture = self:GetCutOut(drawTexture)
    local checkTexture = TypeAs(self.SimplePicture.material:GetTexture("_AlphaTex"), typeof(Texture2D))
    local compareTexture = CHandDrawMgr.Inst:ScaleTexture(cutOutTexture, checkTexture.width, checkTexture.height)
    local totalPixelCount = checkTexture.width * checkTexture.height

    local origin = checkTexture:GetPixels()
    local draws = compareTexture:GetPixels()

    -- 计算符合百分比
    local fitCheckTotal = 0
    local fitPassCounter = 0
    -- 计算不符合百分比
    local unfitCheckTotal = 0
    local unfitPassCounter = 0

    if origin.Length == draws.Length then
        for i = 0, origin.Length-1, 20 do -- 采样为5
            if origin[i].a ~= 0 then
                fitCheckTotal = fitCheckTotal + 1
                if draws[i].a ~= 0 then
                    fitPassCounter = fitPassCounter + 1
                end
            else
                unfitCheckTotal = unfitCheckTotal + 1
                if draws[i].a == 0 then
                    unfitPassCounter = unfitPassCounter + 1
                end
            end
        end
    end
    if fitCheckTotal ~= 0 and fitPassCounter / fitCheckTotal > 0.5 and unfitPassCounter > 0 and unfitPassCounter / unfitCheckTotal > 0.7 then
        Gac2Gas.FinishDrawHanZi(LuaZhuJueJuQingMgr.m_HanZiTaskId, LuaZhuJueJuQingMgr.m_HanZiIndex)
        return true
    end
    return false
end

function LuaDrawHanZiWnd:Update()
    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        if self.IsWinSocialWndOpened ~= CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
            CUIManager.CloseUI(CLuaUIResources.DrawHanZiWnd)
            return
        end
    end
end

return LuaDrawHanZiWnd
