local GameObject                = import "UnityEngine.GameObject"
local CUIManager                = import "L10.UI.CUIManager"
local CCurentMoneyCtrl          = import "L10.UI.CCurentMoneyCtrl"
local QnButton                  = import "L10.UI.QnButton"
local QnAdvanceGridView         = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local QnNewSlider               = import "L10.UI.QnNewSlider"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"

LuaZhouNianQingPassportBuyLevelWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "BuyBtn", "BuyBtn", GameObject)
RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "Slider2", "Slider2", QnNewSlider)
RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "DelBtn", "DelBtn", GameObject)
RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "AddBtn", "AddBtn", GameObject)
RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "TitleLabel", "TitleLabel", UILabel)
--RegistChildComponent(LuaZhouNianQingPassportBuyLevelWnd, "BuyLvLabel", "BuyLvLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "CurLevel")
RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "BuyLevel")
RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "IgnorModify")
RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "SelectedItem")
RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "BuyExp")
RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "BuyCost")
RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "BuyLabel")
RegistClassMember(LuaZhouNianQingPassportBuyLevelWnd, "LvUpLabel")

function LuaZhouNianQingPassportBuyLevelWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.BuyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyBtnClick()
    end)

    UIEventListener.Get(self.DelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnModifyLevel(-1)
    end)

    UIEventListener.Get(self.AddBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnModifyLevel(1)
    end)
    --@endregion EventBind end

    self.BuyLabel = self.transform:Find("Slider2/Desc/BuyLabel"):GetComponent(typeof(UILabel))
    self.LvUpLabel = self.transform:Find("Slider2/Desc/LvUpLabel"):GetComponent(typeof(UILabel))

    self.BuyLevel = 1
    self.BuyExp = 0
    self.BuyCost = 0
    self.IgnorModify = false
    self.SelectedItem = nil

    self.Slider2.OnValueChanged = DelegateFactory.Action_float(function(value)
        if self.IgnorModify then 
            self.IgnorModify = false
            return 
        end
        local maxLv = ZhanLing_Reward.GetDataCount()
        local lv, progress = LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
        local buymax = maxLv - lv
        self.BuyLevel = math.min(math.max(math.floor((maxLv-lv)*value),1),buymax)
        self:InitBuyInfo()
    end)
end

function LuaZhouNianQingPassportBuyLevelWnd:Init()
    local maxLv = ZhanLing_Reward.GetDataCount()
    local lv, progress = LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
    if lv >= maxLv then
        self.TitleLabel.text = g_MessageMgr:FormatMessage("Tcjh_BuyLevelMax")
    else
        local lv = self.BuyLevel + lv
        if lv <= maxLv then
            self:OnModifyLevel(0)
        end
    end
end

--@region UIEvent
function LuaZhouNianQingPassportBuyLevelWnd:OnModifyLevel(value)
    local maxLv = ZhanLing_Reward.GetDataCount()
    local lv, progress = LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
    self.IgnorModify = true
    local buymax = maxLv - lv
    self.BuyLevel = math.min(math.max(self.BuyLevel + value,1),buymax)
    self.Slider2.m_Value = self.BuyLevel / buymax
    self:InitBuyInfo()
end

--@endregion UIEvent


function LuaZhouNianQingPassportBuyLevelWnd:AddItem(item,items,itemids)
    if item == nil then return end
    if itemids[item[1]] == nil then
        itemids[item[1]] = item[2]
        items[#items+1] = item
    else
        itemids[item[1]] = itemids[item[1]] + item[2]
    end
end

function LuaZhouNianQingPassportBuyLevelWnd:InitBuyInfo()
    local maxLv = ZhanLing_Reward.GetDataCount()
    local curLv, progress = LuaZhouNianQing2023Mgr:GetCurZhanLingLevel()
    local lv = self.BuyLevel + curLv
    local exp = 0
    local itemdatas = {}            --itemarray:    index   ->  item={ItemID,Count,IsBind}
    local itemids = {}              --itemdic:      itemid  ->  count
    local vip1 = LuaZhouNianQing2023Mgr.m_IsVip1 and 1 or 0
    local vip2 = LuaZhouNianQing2023Mgr.m_IsVip2 and 1 or 0
    local curtype = 1*vip1 + 2*vip2

    for i = curLv + 1, lv do         --从下一级开始
        local cfg = LuaZhouNianQing2023Mgr.m_ZhanLingData[i]     
        if cfg then 
            exp = cfg.needProgress
            if cfg.item1 then
                self:AddItem(cfg.item1,itemdatas,itemids)
            end
            if curtype == 1 or curtype == 3 then
                if cfg.item2 then
                    self:AddItem(cfg.item2,itemdatas,itemids)
                end
            end

            if curtype == 2 or curtype == 3 then
                if cfg.item3 then
                    self:AddItem(cfg.item3,itemdatas,itemids)
                end
            end
        end
    end

    if exp > 0 then
        exp = exp - progress
    end
    --exp = math.ceil(exp / 5) * 5

    local setting = ZhanLing_Setting.GetData()
    local cost1, cost2 = setting.BuyProgressCostJade, setting.BuyProgressCostJade2
    local exp20 = LuaZhouNianQing2023Mgr.m_ZhanLingData[setting.ChangePriceLevel].needProgress - progress

    local cost
    if curLv >= 20 then
        cost = math.ceil(exp / cost2)
        exp = cost * cost2
    elseif lv <= 20 then
        cost = math.ceil(exp / cost1)
        exp = cost * cost1
    else
        exp20 = math.ceil(exp20 / cost1) * cost1
        cost = exp20 / cost1 + math.ceil((exp - exp20) / cost2)
        exp = (cost - exp20 / cost1) * cost2 + exp20
    end
    
    self.BuyLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]购买[FFFF00]%d[-]"), exp)
    self.LvUpLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]升至[FFFF00]%d级[-][ACF8FF]"), lv)

    self.TitleLabel.text = g_MessageMgr:FormatMessage("Tcjh_BuyLevelTitle",lv)
    
    self.QnCostAndOwnMoney:SetType(4, 0, false)
    self.QnCostAndOwnMoney:SetCost(cost)

    self.BuyExp = exp
    self.BuyCost = cost

    local len = #itemdatas
    local initfunc = function(item,index)
        local data = itemdatas[index+1]
        local count = itemids[data[1]]
        self:FillItemCell(item,data,count)
    end
    self.TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.TableView:Clear()
    self.TableView:ReloadData(true, true)

    local dbtn = self.DelBtn:GetComponent(typeof(QnButton))
    local abtn = self.AddBtn:GetComponent(typeof(QnButton))

    local buymax = maxLv - curLv
    dbtn.Enabled = self.BuyLevel > 1
    abtn.Enabled = self.BuyLevel < buymax
end

function LuaZhouNianQingPassportBuyLevelWnd:FillItemCell(item,data,count)
    local cell = item.transform
    local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
    local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
    local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label

    local itemid = data[1]
    --local isBind = data.IsBind
    
    local itemcfg = Item_Item.GetData(itemid)

    if itemcfg then
        icon:LoadMaterial(itemcfg.Icon)
    end

    --bindgo:SetActive(tonumber(isBind) == 1)

    if tonumber(count) <= 1 then
        countlb.text = ""
    else
        countlb.text = count
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0) 
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end
    end)

    item.gameObject:SetActive(true)
end

function LuaZhouNianQingPassportBuyLevelWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function LuaZhouNianQingPassportBuyLevelWnd:OnBuyBtnClick()
    local msg = g_MessageMgr:FormatMessage("ANNIV2023_ZHANLING_BUY_CONFIRM",self.BuyCost,self.BuyExp)
    g_MessageMgr:ShowOkCancelMessage(msg,function()
        Gac2Gas.RequestBuyZhanLingLevel(LuaZhouNianQing2023Mgr:GetCurZhanLingLevel() + self.BuyLevel)
        CUIManager.CloseUI(CLuaUIResources.ZhouNianQingPassportBuyLevelWnd)
    end,nil,nil,nil,false)
end

--[[function LuaZhouNianQingPassportBuyLevelWnd:GetGuideGo(methodName)
    if methodName == "GetCloseButton" then
        return self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
    end
    return nil
end--]]
