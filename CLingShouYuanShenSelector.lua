-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouYuanShenSelector = import "L10.UI.CLingShouYuanShenSelector"
local CLingShouYuanShenSelectorItem = import "L10.UI.CLingShouYuanShenSelectorItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local NGUIMath = import "NGUIMath"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLingShouYuanShenSelector.m_Init_CS2LuaHook = function (this) 

    --Vector3 localPos = this.anchorNode.parent.InverseTransformPoint(topAnchorPosition);
    this.anchorNode.localPosition = Vector3(- 322, - 110)
    -- localPos;

    CUICommonDef.ClearTransform(this.tf)
    local infos = CItemMgr.Inst:GetAllLingShouYuanShen()
    do
        local i = 0
        while i < infos.Count do
            local go = TypeAs(CommonDefs.Object_Instantiate(this.itemPrefab), typeof(GameObject))
            go.transform.parent = this.tf
            go.transform.localScale = Vector3.one
            go:SetActive(true)
            local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CLingShouYuanShenSelectorItem))
            cmp:Init(infos[i])
            UIEventListener.Get(go).onClick = MakeDelegateFromCSFunction(this.OnClickItem, VoidDelegate, this)
            i = i + 1
        end
    end

    this.grid:Reposition()
end
CLingShouYuanShenSelector.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) then
        return nil
    end

    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local applyAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("使用"), MakeDelegateFromCSFunction(this.OnApply, Action0, this))
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), applyAction)
    return actionPairs
end
CLingShouYuanShenSelector.m_OnApply_CS2LuaHook = function (this) 
    if this.selectItem ~= nil then
        Gac2Gas.RequestUseItem(EnumToInt(this.selectItem.place), this.selectItem.pos, this.selectItem.itemId, "")
    end
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
    this:Close()
end
CLingShouYuanShenSelector.m_OnClickItem_CS2LuaHook = function (this, go) 
    local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CLingShouYuanShenSelectorItem))
    this.selectItem = cmp:GetItemInfo()
    if this.selectItem ~= nil then
        --CItemInfoMgr.ShowPackageItemInfo(selectItem.itemId, selectItem.pos, this);
        local b1 = NGUIMath.CalculateAbsoluteWidgetBounds(this.anchorNode)
        CItemInfoMgr.ShowLinkItemInfo(this.selectItem.itemId, false, this, CItemInfoMgr.AlignType.Right, b1.center.x + b1.extents.x, b1.center.y + b1.extents.y, 2, 2)
    end
end
