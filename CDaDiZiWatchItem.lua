-- Auto Generated!!
local CDaDiZiWatchItem = import "L10.UI.CDaDiZiWatchItem"
local Constants = import "L10.Game.Constants"
local LocalString = import "LocalString"
CDaDiZiWatchItem.m_Init_CS2LuaHook = function (this, info, row) 
    if row % 2 == 0 then
        this:SetBackgroundTexture(Constants.EvenBgSprite)
    else
        this:SetBackgroundTexture(Constants.OddBgSpirite)
    end

    this.nameLabel1.text = info.playerName1
    this.nameLabel2.text = info.playerName2
    if info.winPlayerId == info.playerId1 then
        this.statusSprite1.enabled = true
        this.statusSprite2.enabled = true

        this.statusSprite1.spriteName = "common_fight_result_win"
        this.statusSprite2.spriteName = "common_fight_result_lose"

        this.stateLabel.text = LocalString.GetString("已结束")
    elseif info.winPlayerId == info.playerId2 then
        this.statusSprite1.enabled = true
        this.statusSprite2.enabled = true

        this.statusSprite1.spriteName = "common_fight_result_lose"
        this.statusSprite2.spriteName = "common_fight_result_win"

        this.stateLabel.text = LocalString.GetString("已结束")
    elseif info.winPlayerId == 0 then
        this.statusSprite1.enabled = true
        this.statusSprite2.enabled = true

        this.statusSprite1.spriteName = "common_fight_result_lose"
        this.statusSprite2.spriteName = "common_fight_result_lose"
        this.stateLabel.text = LocalString.GetString("已结束")
    else
        this.statusSprite1.enabled = false
        this.statusSprite2.enabled = false

        this.stateLabel.text = LocalString.GetString("VS")
    end
end
