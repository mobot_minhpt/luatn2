local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Extensions = import "Extensions"

LuaGuildLeagueCrossTrainAdditionWnd = class()

RegistClassMember(LuaGuildLeagueCrossTrainAdditionWnd, "m_Server1Label")
RegistClassMember(LuaGuildLeagueCrossTrainAdditionWnd, "m_Server1ValueLabel")
RegistClassMember(LuaGuildLeagueCrossTrainAdditionWnd, "m_Server2Label")
RegistClassMember(LuaGuildLeagueCrossTrainAdditionWnd, "m_Server2ValueLabel")
RegistClassMember(LuaGuildLeagueCrossTrainAdditionWnd, "m_Table")
RegistClassMember(LuaGuildLeagueCrossTrainAdditionWnd, "m_TrainTypeLabelTemplate")
RegistClassMember(LuaGuildLeagueCrossTrainAdditionWnd, "m_AttributeLabelTemplate")

function LuaGuildLeagueCrossTrainAdditionWnd:Awake()

    self.m_Server1Label =  self.transform:Find("Anchor/Server1Label"):GetComponent(typeof(UILabel))
    self.m_Server1ValueLabel =  self.transform:Find("Anchor/Server1Label/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_Server2Label =  self.transform:Find("Anchor/Server2Label"):GetComponent(typeof(UILabel))
    self.m_Server2ValueLabel =  self.transform:Find("Anchor/Server2Label/ValueLabel"):GetComponent(typeof(UILabel))

    self.m_Table = self.transform:Find("Anchor/Table"):GetComponent(typeof(UITable))
    self.m_TrainTypeLabelTemplate = self.transform:Find("Anchor/TrainTypeLabelTemplate").gameObject
    self.m_AttributeLabelTemplate =  self.transform:Find("Anchor/AttributeLabelTemplate").gameObject

    self.m_TrainTypeLabelTemplate:SetActive(false)
    self.m_AttributeLabelTemplate:SetActive(false)
end

function LuaGuildLeagueCrossTrainAdditionWnd:Init()

    local trainAdditionInfo = LuaGuildLeagueCrossMgr.m_TrainAdditionInfo.additionInfo
    self.m_Server1Label.text = LuaGuildLeagueCrossMgr.m_TrainAdditionInfo.server1Name
    self.m_Server2Label.text = LuaGuildLeagueCrossMgr.m_TrainAdditionInfo.server2Name

    local sum = {}
    for k,v in pairs(trainAdditionInfo) do
        sum[k] = 0
        for __,data in pairs(v) do
            sum[k] = sum[k] + data.level
        end
    end
    
    self.m_Server1ValueLabel.text = sum[1]<0 and "-" or "[c]"..self:GetValueColor(sum[1]<sum[2])..SafeStringFormat3(LocalString.GetString("总%d级"), sum[1])
    self.m_Server2ValueLabel.text = sum[2]<0 and "-" or "[c]"..self:GetValueColor(sum[2]<sum[1])..SafeStringFormat3(LocalString.GetString("总%d级"), sum[2])

    local attributeNames = {
        LocalString.GetString("人物气血"),
        LocalString.GetString("箭塔气血"),
        LocalString.GetString("神兽和主炮气血"),
        LocalString.GetString("人物气血"),
        LocalString.GetString("攻/防御增益初始数"),
    }

    local genDesc = function(serverIdx,i)
        local isLess = (serverIdx==1 and trainAdditionInfo[serverIdx][i].effect<trainAdditionInfo[serverIdx+1][i].effect) or
                         (serverIdx==2 and trainAdditionInfo[serverIdx][i].effect<trainAdditionInfo[serverIdx-1][i].effect) 
        local color = self:GetValueColor(isLess)
        local dataMissing = (trainAdditionInfo[serverIdx][i].level<0)
        local ret = ""
        if i<=3 then
            ret = dataMissing and "-" or SafeStringFormat3(LocalString.GetString("%d级: +%d%%"), trainAdditionInfo[serverIdx][i].level, trainAdditionInfo[serverIdx][i].effect)
        elseif i==4 then
            ret = dataMissing and "-" or SafeStringFormat3(LocalString.GetString("%d级: +%d点"), trainAdditionInfo[serverIdx][i].level, trainAdditionInfo[serverIdx][i].effect)
        else
            ret = dataMissing and "-" or SafeStringFormat3(LocalString.GetString("%d级: 各加%d次"), trainAdditionInfo[serverIdx][i].level, trainAdditionInfo[serverIdx][i].effect)
        end
        ret = "[c]"..color..ret
        return ret
    end

    Extensions.RemoveAllChildren(self.m_Table.transform)

    local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_TrainTypeLabelTemplate)
    child:SetActive(true)
    child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("资源培养加成")
    for i=1,3 do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_AttributeLabelTemplate)
        child:SetActive(true)
        child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = attributeNames[i]
        child.transform:Find("Server1AdditionLabel"):GetComponent(typeof(UILabel)).text = genDesc(1, i)
        child.transform:Find("Server2AdditionLabel"):GetComponent(typeof(UILabel)).text = genDesc(2, i)
    end

    local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_TrainTypeLabelTemplate)
    child:SetActive(true)
    child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("任务培养加成")
    for i=4,5 do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_AttributeLabelTemplate)
        child:SetActive(true)
        child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = attributeNames[i]
        child.transform:Find("Server1AdditionLabel"):GetComponent(typeof(UILabel)).text = genDesc(1, i)
        child.transform:Find("Server2AdditionLabel"):GetComponent(typeof(UILabel)).text = genDesc(2, i)
    end
    self.m_Table:Reposition()
end

function LuaGuildLeagueCrossTrainAdditionWnd:GetValueColor(isLess)
    return isLess and "[FFFFFF]" or "[00FF00]" 
end

