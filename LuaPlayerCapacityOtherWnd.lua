local Color = import "UnityEngine.Color"


CLuaPlayerCapacityOtherWnd = class()
RegistClassMember(CLuaPlayerCapacityOtherWnd,"template")
RegistClassMember(CLuaPlayerCapacityOtherWnd,"playerCapacityLabel")
RegistClassMember(CLuaPlayerCapacityOtherWnd,"table")
RegistClassMember(CLuaPlayerCapacityOtherWnd,"titleLabel")
RegistClassMember(CLuaPlayerCapacityOtherWnd,"childTemplate")

CLuaPlayerCapacityOtherWnd.playerId = 0
CLuaPlayerCapacityOtherWnd.name = nil

function CLuaPlayerCapacityOtherWnd:Awake()
    self.template = self.transform:Find("Anchor/ScrollView/Template").gameObject
    self.template:SetActive(false)
    self.childTemplate = self.transform:Find("Anchor/ScrollView/ChildTemplate").gameObject
    self.childTemplate:SetActive(false)

    self.playerCapacityLabel = self.transform:Find("Anchor/PlayerCapacityLabel"):GetComponent(typeof(UILabel))
    self.playerCapacityLabel.text = ""
    self.table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.titleLabel = self.transform:Find("Anchor/NameLabel"):GetComponent(typeof(UILabel))
    self.titleLabel.text = ""
end

function CLuaPlayerCapacityOtherWnd:Init()
    local name = CLuaPlayerCapacityOtherWnd.name
    local playerId=CLuaPlayerCapacityOtherWnd.playerId 
    Gac2Gas.QueryPlayerCapacity(playerId)
    self.titleLabel.text = SafeStringFormat3("%s(ID:%s)", name, playerId);
end

function CLuaPlayerCapacityOtherWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdatePlayerCapacity", self, "UpdatePlayerCapacity")

end
function CLuaPlayerCapacityOtherWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdatePlayerCapacity", self, "UpdatePlayerCapacity")
end

function CLuaPlayerCapacityOtherWnd:UpdatePlayerCapacity( id, capacity, detailData) 
    if id == CLuaPlayerCapacityOtherWnd.playerId then
        CLuaPlayerCapacityMgr.InitModuleDef()
        for i,v in ipairs(CLuaPlayerCapacityMgr.m_ModuleDef) do
        if not v.ignore then
            local go = NGUITools.AddChild(self.table.gameObject,self.template)
            go:SetActive(true)
            local tf = go.transform
            local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
            nameLabel.text = v.name

            local grid = tf:Find("Grid"):GetComponent(typeof(UIGrid))
            for j,subinfo in ipairs(v.sub) do
                local child = NGUITools.AddChild(grid.gameObject,self.childTemplate)
                child:SetActive(true)
                local descLabel = child.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
                descLabel.text = subinfo[2]
                local label1 = child.transform:Find("Label1"):GetComponent(typeof(UILabel))

                local subkey = subinfo[1]
                local otherValue =0
                if subkey=="ShiMenValue_ZongMen" then
                    local v1 = math.floor(tonumber(CommonDefs.DictGetValue(detailData, typeof(String), "ShiMenValue_HpSkill") or 0))
                    local v2 = math.floor(tonumber(CommonDefs.DictGetValue(detailData, typeof(String), "ShiMenValue_AntiPro") or 0))
                    otherValue = v1+v2
                else
                    otherValue = math.floor(tonumber(CommonDefs.DictGetValue(detailData, typeof(String), subinfo[1]) or 0))
                end
                label1.text = tostring(otherValue)
                local label2 = child.transform:Find("Label2"):GetComponent(typeof(UILabel))
                local visible = true
                if subinfo.condition then
                    visible = subinfo.condition()
                end
                label2.gameObject:SetActive(visible)
                local myValue = math.floor(CLuaPlayerCapacityMgr.GetValue(subinfo[1]))
                label2.text = tostring(myValue)
                if myValue>=otherValue then
                    label2.color =Color.green--NGUIText.ParseColor("ff0000", 0)
                else
                    label2.color = Color.red--NGUIText.ParseColor("ff0000", 0)
                end
            end
            grid:Reposition()
            go:GetComponent(typeof(UISprite)).height = #v.sub*64+25
        end
        end

        self.table:Reposition()

        self.playerCapacityLabel.text = tostring((math.floor(capacity)))
    end
end
