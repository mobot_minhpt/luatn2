-- Auto Generated!!
local CRankTableHeader = import "L10.UI.CRankTableHeader"
local UISprite = import "UISprite"
CRankTableHeader.m_Init_CS2LuaHook = function (this, mainPlayerRankInfo, width) 
    do
        local i = 0
        while i < this.columns.Length do
            if i < mainPlayerRankInfo.Count then
                this.columns[i].text = mainPlayerRankInfo[i]
                this.columns[i].width = width[i]
                local sprite = CommonDefs.GetComponentInChildren_Component_Type(this.columns[i], typeof(UISprite))
                if sprite ~= nil then
                    sprite:UpdateAnchors()
                end
            else
                this.columns[i].text = ""
                this.columns[i].width = 0
            end
            i = i + 1
        end
    end
    this.table:Reposition()
end
