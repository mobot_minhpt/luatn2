local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaQiXi2022Mgr = {}

LuaQiXi2022Mgr.m_PlotWndShowFlowChart = false
LuaQiXi2022Mgr.m_PlotWndResultStage = -1 
LuaQiXi2022Mgr.m_HasOpenQiXi2022PlotWnd = false
LuaQiXi2022Mgr.m_ClosePlotWndTime = 0
LuaQiXi2022Mgr.m_OptionData = {}
LuaQiXi2022Mgr.m_ReturnGoodsWndData = {}

function LuaQiXi2022Mgr:SFEQ_SyncOptionInfo(optionData_U)
    self.m_OptionData = g_MessagePack.unpack(optionData_U)
    if CUIManager.IsLoaded(CLuaUIResources.QiXi2022ChoosePlotWnd) then
        g_ScriptEvent:BroadcastInLua("SFEQ_SyncOptionInfo")
    else
        CUIManager.ShowUI(CLuaUIResources.QiXi2022ChoosePlotWnd)
    end
end

function LuaQiXi2022Mgr:SFEQ_ChangeOptionText(textStatus)
    g_ScriptEvent:BroadcastInLua("SFEQ_ChangeOptionText", textStatus)
end

function LuaQiXi2022Mgr:SFEQ_OnOptionCountDownTick(countDown)
    g_ScriptEvent:BroadcastInLua("SFEQ_OnOptionCountDownTick", countDown)
end

function LuaQiXi2022Mgr:SFEQ_PlayerClickOption(playerId,optionId)
    g_ScriptEvent:BroadcastInLua("SFEQ_PlayerClickOption", playerId,optionId)
end

function LuaQiXi2022Mgr:SFEQ_StartConfirmOption(optionId, countDown, textStatus)
    g_ScriptEvent:BroadcastInLua("SFEQ_OnOptionCountDownTick", countDown)
    g_ScriptEvent:BroadcastInLua("SFEQ_ChangeOptionText", textStatus)
end

function LuaQiXi2022Mgr:SFEQ_CancelConfirmOption(countDown, textStatus)
    g_ScriptEvent:BroadcastInLua("SFEQ_OnOptionCountDownTick", countDown)
    g_ScriptEvent:BroadcastInLua("SFEQ_ChangeOptionText", textStatus)
end

function LuaQiXi2022Mgr:SFEQ_EndSelectOption(optionId)
    CUIManager.CloseUI(CLuaUIResources.QiXi2022ChoosePlotWnd)
end

function LuaQiXi2022Mgr:OpenQiXi2022ReturnGoodsWnd(data)
    self.m_ReturnGoodsWndData = g_MessagePack.unpack(data)
	CUIManager.ShowUI(CLuaUIResources.QiXi2022ReturnGoodsWnd)
end

function LuaQiXi2022Mgr:SFEQ_WPFL_PlayerOpenWnd(playerId)
    g_ScriptEvent:BroadcastInLua("SFEQ_WPFL_PlayerOpenWnd", playerId)
end

function LuaQiXi2022Mgr:SFEQ_WPFL_SyncClickOrReleaseItemResult(itemId, bCanClick, playerId)
    g_ScriptEvent:BroadcastInLua("SFEQ_WPFL_SyncClickOrReleaseItemResult", itemId, bCanClick, playerId)
end

function LuaQiXi2022Mgr:SFEQ_WPFL_PlayerCloseWnd(playerId)
    g_ScriptEvent:BroadcastInLua("SFEQ_WPFL_PlayerCloseWnd", playerId)
end

function LuaQiXi2022Mgr:SFEQ_WPFL_PutItemSuccess(itemId)
    g_ScriptEvent:BroadcastInLua("SFEQ_WPFL_PutItemSuccess", itemId)
end

function LuaQiXi2022Mgr:SFEQ_WPFL_EndPlay()
    CUIManager.CloseUI(CLuaUIResources.QiXi2022ReturnGoodsWnd)
end

LuaQiXi2022Mgr.m_XW_Distance = 0
function LuaQiXi2022Mgr:SFEQ_XW_UpdateDistance(distance)
    self.m_XW_Distance = distance
    print("SFEQ_XW_UpdateDistance", distance)
    g_ScriptEvent:BroadcastInLua("SFEQ_XW_UpdateDistance", distance)
end

LuaQiXi2022Mgr.m_PickNum = 0
function LuaQiXi2022Mgr:SFEQ_XW_SyncPickNum(pickNum)
    print(pickNum)
    self.m_PickNum = pickNum
    self:ShowTaskView()
end

LuaQiXi2022Mgr.m_XW_SyncEndTime = 0
function LuaQiXi2022Mgr:SFEQ_XW_SyncEndTime(endTime)
    self.m_XW_SyncEndTime = endTime
    self:ShowTaskView()
end

LuaQiXi2022Mgr.m_XW_TargetPosX = 0
LuaQiXi2022Mgr.m_XW_TargetPosY = 0
function LuaQiXi2022Mgr:SFEQ_XW_SyncTargetPos(targetX, targetY)
    self.m_XW_TargetPosX = targetX
    self.m_XW_TargetPosY = targetY
    g_ScriptEvent:BroadcastInLua("SFEQ_XW_SyncTargetPos", self.m_XW_TargetPosX, self.m_XW_TargetPosY, true)
    CUIManager.ShowUI(CLuaUIResources.QiXi2022FindItemSkillWnd)
end

function LuaQiXi2022Mgr:SFEQ_XW_EndPlay()
    self.m_XW_TargetPosX = 0
    self.m_XW_TargetPosY = 0
    self.m_TaskStage =  0
    g_ScriptEvent:BroadcastInLua("SFEQ_XW_SyncTargetPos", self.m_XW_TargetPosX, self.m_XW_TargetPosY, false)
end

--playerInfo_U是列表，存了2个玩家的数据：
LuaQiXi2022Mgr.m_EndingResultData = {}
function LuaQiXi2022Mgr:SFEQ_SyncEndingResult(endingId, playerInfo_U)
    self.m_EndingResultData.playerInfo = g_MessagePack.unpack(playerInfo_U)[1]
    LuaQiXi2022Mgr.m_PlotWndResultStage = endingId
	CUIManager.ShowUI(CLuaUIResources.QiXi2022PlotResultWnd)
end

LuaQiXi2022Mgr.m_TaskStage = 0
function LuaQiXi2022Mgr:SFEQ_SyncStage(stage)
    self.m_TaskStage = stage
    self:ShowTaskView()
    self:StartTick()
end

LuaQiXi2022Mgr.m_IsFindItem = false
function LuaQiXi2022Mgr:ShowTaskView()
    local data = QiXi2022_SFEQTaskDescription.GetData(self.m_TaskStage)
    if not data then return end
    local msg = SafeStringFormat3(data.Text,"%s")
    local isFindItem = false
    local xunwuStageID = QiXi2022_Setting.GetData().XunwuStageID
    for i = 0, xunwuStageID.Length - 1 do
        isFindItem = isFindItem or (xunwuStageID[i] == self.m_TaskStage)
    end
    local isReturnItem = self.m_TaskStage == 2 or self.m_TaskStage == 8
    if isFindItem then
        local t = self.m_XW_SyncEndTime - CServerTimeMgr.Inst.timeStamp
        local s = SafeStringFormat3("%02d:%02d", math.floor(t % 3600 / 60),t % 60)
        msg = SafeStringFormat3(data.Text, self.m_PickNum, s)
        if not self.m_IsFindItem then
            self:ShowFindItemRuleWnd()
        end
    elseif isReturnItem then
        local t = self.m_XW_SyncEndTime - CServerTimeMgr.Inst.timeStamp
        local s = SafeStringFormat3("%02d:%02d", math.floor(t % 3600 / 60),t % 60)
        msg = SafeStringFormat3(data.Text,  s)
    end
    self.m_IsFindItem = isFindItem
    g_ScriptEvent:BroadcastInLua("SFEQ_XW_SyncTargetPos", self.m_XW_TargetPosX, self.m_XW_TargetPosY, isFindItem)
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate", msg,true, isFindItem, nil,  isFindItem and LocalString.GetString("规则") or nil,nil, function(go)
        self:ShowFindItemRuleWnd()
    end)
end

function LuaQiXi2022Mgr:ShowFindItemRuleWnd()
    g_MessageMgr:ShowMessage("QiXi2022_Guide_Message")
end

function LuaQiXi2022Mgr:IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == QiXi2022_Setting.GetData().MainGameplayID or gamePlayId == QiXi2022_Setting.GetData().Gameplay2ID then
            return true
        end
    end

    return false
end

LuaQiXi2022Mgr.m_Tick = nil
function LuaQiXi2022Mgr:StartTick()
    self:CancelTick()
    g_ScriptEvent:BroadcastInLua("SFEQ_XW_SyncTargetPos", self.m_XW_TargetPosX, self.m_XW_TargetPosY, false)
    if not self:IsInPlay() then
        return
    end
    self.m_Tick = RegisterTick(function ()
        if self:IsInPlay() then
            self:ShowTaskView()
        else
            self:CancelTick()
        end
    end,500)
end

function LuaQiXi2022Mgr:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaQiXi2022Mgr:OnMainPlayerDestroyed()
    self:CancelTick()
    if not self:IsInPlay() then
        return
    end
    self:StartTick()
end

function LuaQiXi2022Mgr:AcceptInvitationConfirm(playerName)
    local msg = g_MessageMgr:FormatMessage("QiXi2022PlantTrees_Accept_Invitation", playerName)
    MessageWndManager.ShowDefaultConfirmMessage(msg, 30, false, DelegateFactory.Action(function()
        Gac2Gas.YYS_AcceptInvitation()
        Gac2Gas.YYS_RequestOpenTab()
        CUIManager.ShowUI(CLuaUIResources.QiXi2022PlantTreesWnd)
    end),  DelegateFactory.Action(function()
        Gac2Gas.YYS_RejectInvitation()
    end), false,nil, LocalString.GetString("接受"), LocalString.GetString("再想想"))
end

function LuaQiXi2022Mgr:AcceptInvitationAlert(bInvitor, playerName)
    local msg = ""
    if bInvitor then
        msg = g_MessageMgr:FormatMessage("QiXi2022PlantTrees_Accept_Alert", playerName)
    else
        msg = g_MessageMgr:FormatMessage("QiXi2022PlantTrees_Accept_Guest_Alert", playerName)
    end
    MessageWndManager.ShowOKMessage(msg, nil, DelegateFactory.Action(function()
        CUIManager.ShowUI(CLuaUIResources.QiXi2022PlantTreesWnd)
    end))
end

function LuaQiXi2022Mgr:YYS_SendInvitation(playerName)
    self:AcceptInvitationConfirm(playerName)
end

function LuaQiXi2022Mgr:YYS_PlantTreeSuccess(bInvitor, playerName)
    if CUIManager.IsLoaded(CLuaUIResources.QiXi2022PlantTreesWnd) then
        g_ScriptEvent:BroadcastInLua("YYS_PlantTreeSuccess")
        return
    end
    self:AcceptInvitationAlert(bInvitor, playerName)
end

function LuaQiXi2022Mgr:YYS_UpdateMissionProgress(kind, id, value, bFinish, bClaimReward)
    g_ScriptEvent:BroadcastInLua("YYS_UpdateMissionProgress", kind, id, value, bFinish, bClaimReward)
end

LuaQiXi2022Mgr.m_IsFirstOpenPlantTreesWnd = false
LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData = nil
LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndDailyTaskData = nil
function LuaQiXi2022Mgr:YYS_IsFirstOpenTab(bFirstOpen, playData, taskData)
    self.m_QiXi2022PlantTreesWndPlayData = g_MessagePack.unpack(playData)
    if taskData then
        self.m_QiXi2022PlantTreesWndDailyTaskData = g_MessagePack.unpack(taskData)
    end
    if not CUIManager.IsLoaded(CLuaUIResources.QiXi2022PlantTreesWnd) then
        self.m_IsFirstOpenPlantTreesWnd = bFirstOpen
        CUIManager.ShowUI(CLuaUIResources.QiXi2022PlantTreesWnd)
    end
end

function LuaQiXi2022Mgr:YYS_SyncPlayData(playData, taskData)
    self.m_QiXi2022PlantTreesWndPlayData = g_MessagePack.unpack(playData)
    if taskData then
        self.m_QiXi2022PlantTreesWndDailyTaskData = g_MessagePack.unpack(taskData)
    end
    g_ScriptEvent:BroadcastInLua("YYS_SyncPlayData")
end

function LuaQiXi2022Mgr:SFEQ_SyncPlayData(playData)
    g_ScriptEvent:BroadcastInLua("SFEQ_SyncPlayData", g_MessagePack.unpack(playData))
end

function LuaQiXi2022Mgr:QiXi2022_SyncActivityInfo(bIsOpen, bIsOpenPlay)
    g_ScriptEvent:BroadcastInLua("QiXi2022_SyncActivityInfo", bIsOpen, bIsOpenPlay)
end

function LuaQiXi2022Mgr:YYS_OnPlayerRejectInvitation()
end

function LuaQiXi2022Mgr:SFEQ_WPFL_ClickItemFail(itemId)
    g_ScriptEvent:BroadcastInLua("SFEQ_WPFL_ClickItemFail", itemId)
end

function LuaQiXi2022Mgr:QiXi2023PlantTreeRewardCheck()
    local showRedDot1,showRedDot2 = false,false
    local havePartner = false
    if LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData then
        local tempPlayData = LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData
        local jieyuanData = tempPlayData[4]
        havePartner = (tempPlayData[1] ~= nil)
        if jieyuanData then
            QiXi2022_DisposableTask.ForeachKey(function (k)
                local status = jieyuanData[k]
                if status then
                    showRedDot2 = showRedDot2 or (status == 1)
                end
            end)
        end
    end

    if LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndDailyTaskData then
        local tempPlayData = LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndDailyTaskData
        local m_DailyTaskData = tempPlayData
        if m_DailyTaskData then
            QiXi2022_DailyTask.ForeachKey(function (k)
                local arr = m_DailyTaskData[k]
                if arr then
                    local progress, status = arr[1], arr[2]
                    showRedDot1 = showRedDot1 or (status == 1)
                end
            end)
        end
    end
    
    return (showRedDot1 or showRedDot2) and havePartner
end 