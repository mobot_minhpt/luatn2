local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local String = import "System.String"
-- local Guild_JobAppoint = import "L10.Game.Guild_JobAppoint"
-- local EGuildJob = import "L10.Game.EGuildJob"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UIDefaultTableView=import "L10.UI.UIDefaultTableView"
local UIDefaultTableViewCell=import "L10.UI.UIDefaultTableViewCell"
local CellIndexType=import "L10.UI.UITableView+CellIndexType"
local CellIndex=import "L10.UI.UITableView+CellIndex"
-- local Guild_Job = import "L10.Game.Guild_Job"
local UITable=import "UITable"


CLuaGuildPowerSettingWnd = class()
RegistClassMember(CLuaGuildPowerSettingWnd,"closeButton")
RegistClassMember(CLuaGuildPowerSettingWnd,"grid1")
RegistClassMember(CLuaGuildPowerSettingWnd,"grid2")
RegistClassMember(CLuaGuildPowerSettingWnd,"itemPrefab")
RegistClassMember(CLuaGuildPowerSettingWnd,"masterView")
RegistClassMember(CLuaGuildPowerSettingWnd,"resetBtn")
RegistClassMember(CLuaGuildPowerSettingWnd,"saveBtn")
RegistClassMember(CLuaGuildPowerSettingWnd,"itemList")
RegistClassMember(CLuaGuildPowerSettingWnd,"mTitle")
RegistClassMember(CLuaGuildPowerSettingWnd,"mJob")

RegistClassMember(CLuaGuildPowerSettingWnd,"memberInfoList")
RegistClassMember(CLuaGuildPowerSettingWnd,"mJobList")

RegistClassMember(CLuaGuildPowerSettingWnd,"m_OccpuationIds")
RegistClassMember(CLuaGuildPowerSettingWnd,"m_NeedSaveLookup")


function CLuaGuildPowerSettingWnd:Awake()
    self.m_NeedSaveLookup={}
    self.m_OccpuationIds={}

    self.grid1 = self.transform:Find("Anchor/DetailView/ScrollView/Table/Content1/Grid"):GetComponent(typeof(UIGrid))
    self.grid2 = self.transform:Find("Anchor/DetailView/ScrollView/Table/Content2/Grid"):GetComponent(typeof(UIGrid))
    self.itemPrefab = self.transform:Find("Anchor/DetailView/Pool/Item").gameObject
    self.masterView = self.transform:Find("Anchor/MasterView"):GetComponent(typeof(UIDefaultTableView))

    self.masterView:Init(function()
            return #self.mJobList
        end,
        function(section)
            local job = self.mJobList[section+1]
            return #self.memberInfoList[job]
        end,
        function(cell,index)
            self:InitTableViewCell(cell,index)
        end,
        function(index,expanded)
            if index.type == CellIndexType.Section then
                -- local job = self.mJobList[index.section+1]
            end
        end,
        function(index)
            local job = self.mJobList[index.section+1]
            local info = self.memberInfoList[job][index.row+1]
            self:OnSelected(info)
        end
    )


    self.resetBtn = self.transform:Find("Anchor/DetailView/ResetButton").gameObject
    self.saveBtn = self.transform:Find("Anchor/DetailView/SaveButton").gameObject
self.itemList = {}

self.mTitle = 0
--self.mJob = ?
    self.itemPrefab:SetActive(false)
    UIEventListener.Get(self.resetBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickResetButton(go)
    end) 
    UIEventListener.Get(self.saveBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:SaveGuildPowers()
    end) 
end

function CLuaGuildPowerSettingWnd:InitTableViewCell(cell,index)
    local viewCell = cell:GetComponent(typeof(UIDefaultTableViewCell))
    local nameLabel = cell.transform:Find("Name"):GetComponent(typeof(UILabel))
    if index.type == CellIndexType.Section then
        local name = ""
        local job = self.mJobList[index.section+1]
        if job==1 then
            name = LocalString.GetString("帮主")
        elseif job==2 then
            name = LocalString.GetString("副帮主")
        elseif job==3 then
            name = LocalString.GetString("长老")
        elseif job==4 then
            name = LocalString.GetString("堂主")
        elseif job==5 then
            name = LocalString.GetString("香主")
        end
        nameLabel.text = name
    else
        local job = self.mJobList[index.section+1]
        local info = self.memberInfoList[job][index.row+1]
        local memberDynamicInfo = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), info.PlayerId)
        nameLabel.text = memberDynamicInfo and memberDynamicInfo.Name or ""
    end
end


function CLuaGuildPowerSettingWnd:InitMasterView()
    self.memberInfoList = {}

    CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildMemberInfo.Infos, DelegateFactory.Action_object_object(function (___key, ___value) 
        local memberInfo = ___value
        local title = memberInfo.Title

        if title == 1 then
            self.memberInfoList[1] = { memberInfo }
        elseif title == 2 then
            self.memberInfoList[2] = { memberInfo }
        elseif title == 3 then
            if not self.memberInfoList[3] then self.memberInfoList[3] = {} end
            table.insert( self.memberInfoList[3], memberInfo )
        elseif title>10 and math.floor(title/10)==1 then
            if not self.memberInfoList[4] then self.memberInfoList[4] = {} end
            table.insert( self.memberInfoList[4], memberInfo )
        elseif title>20 and math.floor(title/10)==2 then
            if not self.memberInfoList[5] then self.memberInfoList[5] = {} end
            table.insert( self.memberInfoList[5], memberInfo )
        end

    end))
    self.mJobList={}
    for k,v in pairs(self.memberInfoList) do
        table.insert( self.mJobList,k )
    end

    self.masterView:LoadData(CellIndex(0, 0, CellIndexType.Row), true)
end

function CLuaGuildPowerSettingWnd:OnClickResetButton( go) 
    for i,v in ipairs(self.itemList) do
        if self.m_NeedSaveLookup[v] then
            local toggle = v:Find("Toggle"):GetComponent(typeof(UIToggle))
            local config = self:GetJobConfig(self.m_OccpuationIds[i], self.mTitle)
            if config ==2 then
                toggle.value = true
            elseif config==3 then
                toggle.value = false
            end
        end
    end
    self:SaveGuildPowers()
end
function CLuaGuildPowerSettingWnd:SaveGuildPowers( )

    local Dictionary_String_Object = MakeGenericClass(Dictionary, String, Object)

    local dataDic = CreateFromClass(Dictionary_String_Object)

    for i,v in ipairs(self.itemList) do
        if self.m_NeedSaveLookup[v] then
            local toggle = self:ItemGetValue(v) --self.itemList[i]:GetValue()
            CommonDefs.DictAdd(dataDic, typeof(String), tostring(self.m_OccpuationIds[i]), typeof(Int32), toggle and 1 or 0)
        end
    end

    local dic = CreateFromClass(Dictionary_String_Object)
    CommonDefs.DictAdd(dic, typeof(String), tostring(self.mTitle), typeof(Object), dataDic)
    Gac2Gas.RequestSetGuildRights(MsgPackImpl.pack(dic))
end

function CLuaGuildPowerSettingWnd:Init( )
    CLuaGuildMgr.GetGuildRightsInfo()
    self.itemList={}
    local keys = {}
    Guild_JobAppoint.ForeachKey(function (key)
            table.insert(keys, key)
        end)


	for i,v in ipairs(keys) do
        local data = Guild_JobAppoint.GetData(v)
        local go = nil
        if data.Location == 1 then
            go = NGUITools.AddChild(self.grid1.gameObject,self.itemPrefab)
            go:SetActive(true)

        elseif data.Location ==2 then
            go = NGUITools.AddChild(self.grid2.gameObject,self.itemPrefab)
            go:SetActive(true)
        end
        if go then
            local label = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            local job = Guild_Job.GetData(data.OccpuationID)
            table.insert( self.m_OccpuationIds, data.OccpuationID)
            label.text = job.OccpuationName
            table.insert( self.itemList,go.transform )
        end
    end

    self.grid1:Reposition()
    self.grid2:Reposition()

    self.transform:Find("Anchor/DetailView/ScrollView/Table"):GetComponent(typeof(UITable)):Reposition()
end

function CLuaGuildPowerSettingWnd:OnSelected( memberInfo) 
    if memberInfo ~= nil then
        self.mTitle = memberInfo.Title
        for i,v in ipairs(self.itemList) do
            self:InitItem(v,self.m_OccpuationIds[i], memberInfo.PlayerId, memberInfo.Title)
        end

    else
        self.mTitle = 0
        for i,v in ipairs(self.itemList) do
            self:ItemDisableEdit(v, memberInfo.PlayerId, memberInfo.Title)
        end
    end
end
-- 1：默认有权限，无法修改
-- 0：默认无权限，无法修改
-- 2:缺省，系统缺省赋予某阶级的权限，可以禁止
-- 3:授予，系统缺省未赋予某阶级的权限，可以赋予
function CLuaGuildPowerSettingWnd:GetJobConfig(occpuationId, title)
    local jobData = Guild_Job.GetData(occpuationId)
    if title == 1 then
        return jobData.eBangZhu
    elseif title ==2 then
        return jobData.eFuBangZhu
    elseif title ==3 then
        return jobData.eZhangLao
    elseif math.floor(title / 10) == 1 then
        return jobData.eTangZhu
    elseif math.floor(title / 10) == 2 then
        return jobData.eXiangZhu
    end
    return 0
end

function CLuaGuildPowerSettingWnd:InitItem(transform,occpuationId,playerId,title)
    local blockGo = transform:Find("Sprite").gameObject
    local toggle = transform:Find("Toggle"):GetComponent(typeof(UIToggle))
    local tickGo = transform:Find("Tick").gameObject
    local label = transform:Find("NameLabel"):GetComponent(typeof(UILabel))

    local config = self:GetJobConfig(occpuationId,title)
    self.m_NeedSaveLookup[transform] = config ==2 or config==3 

    if config == 0 then
        tickGo:SetActive(false)
        blockGo:SetActive(true)
        toggle.gameObject:SetActive(false)
    elseif config ==1 then
        tickGo:SetActive(true)
        blockGo:SetActive(false)
        toggle.gameObject:SetActive(false)
    else
        local hasVal = false
        local val = 0
        local rights = CLuaGuildMgr.m_GuildRightsDic and CLuaGuildMgr.m_GuildRightsDic[title] or {}-- CommonDefs.DictGetValue_LuaCall(CGuildMgr.Inst.m_GuildRightsDic, title)
        -- if rights then
        local getval =rights and rights[occpuationId]
        if getval then
            hasVal = true
            val = getval
        end
        -- end

        if not hasVal then
            toggle.value = config==2 and true or false
        else
            toggle.value = val == 1
        end

        blockGo:SetActive(false)
        tickGo:SetActive(false)
        toggle.gameObject:SetActive(true)
    end
end

function CLuaGuildPowerSettingWnd:ItemSetNotClick(transform)
    local toggle = transform:Find("Toggle"):GetComponent(typeof(UIToggle))
    CUICommonDef.SetActive(toggle.gameObject, false,true)
end
function CLuaGuildPowerSettingWnd:ItemSetClick(transform)
    local toggle = transform:Find("Toggle"):GetComponent(typeof(UIToggle))
    CUICommonDef.SetActive(toggle.gameObject, true, true)
end

function CLuaGuildPowerSettingWnd:ItemGetValue(transform)
    local toggle = transform:Find("Toggle"):GetComponent(typeof(UIToggle))
    return toggle.value
end

function CLuaGuildPowerSettingWnd:OnGetGuildRightsInfo( )
    self:InitMasterView()
    --如果不是帮主不能操作界面
    local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0
    if CGuildMgr.Inst.m_GuildMemberInfo ~= nil then
        local selfInfo = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), myId)
        if selfInfo and selfInfo.Title == 1 then
            for i,v in ipairs(self.itemList) do
                self:ItemSetClick(v)
            end
        else
            for i,v in ipairs(self.itemList) do
                self:ItemSetNotClick(v)
            end
            CUICommonDef.SetActive(self.resetBtn, false, true)
            CUICommonDef.SetActive(self.saveBtn, false, true)
        end
    end
end

function CLuaGuildPowerSettingWnd:OnEnable()
    g_ScriptEvent:AddListener("RequestSetGuildRightsSuccess", self, "OnRequestSetGuildRightsSuccess")
    g_ScriptEvent:AddListener("SendGuildInfo_GuildRightsInfo", self, "OnGetGuildRightsInfo")
    
end
function CLuaGuildPowerSettingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RequestSetGuildRightsSuccess", self, "OnRequestSetGuildRightsSuccess")
    g_ScriptEvent:RemoveListener("SendGuildInfo_GuildRightsInfo", self, "OnGetGuildRightsInfo")
end

function CLuaGuildPowerSettingWnd:OnRequestSetGuildRightsSuccess( )
    local rights= {}
    for i,v in ipairs(self.itemList) do
        if self.m_NeedSaveLookup[v] then
            local toggle = self:ItemGetValue(v)
            rights[self.m_OccpuationIds[i]] =  toggle and 1 or 0
        end
    end
    CLuaGuildMgr.SetRightInfo(self.mTitle, rights)
end
