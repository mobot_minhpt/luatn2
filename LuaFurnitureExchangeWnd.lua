local UIGrid = import "UIGrid"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CPackageViewItemResource = import "L10.UI.CPackageViewItemResource"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
--local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CResourceMgr  = import "L10.Engine.CResourceMgr"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local QnCheckBox = import "L10.UI.QnCheckBox"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local CItemInfoMgr_AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local UILongPressButton = import "L10.UI.UILongPressButton"
local GameObject = import "UnityEngine.GameObject"

CLuaFurnitureExchangeWnd=class()
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_ItemCellTemplate", CPackageItemCell)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_Grid", UIGrid)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_ScrollView", CUIRestrictScrollView)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_TargetNum", UILabel)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_NeedHouseScoreLbl", UILabel)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_TotalHouseScoreLbl", UILabel)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_TargetNameLbl", UILabel)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_ExchangeInfoLbl", UILabel)
RegistChildComponent(CLuaFurnitureExchangeWnd, "m_DoBtn", CButton)
RegistChildComponent(CLuaFurnitureExchangeWnd, "CloseButton", CButton)
RegistClassMember(CLuaFurnitureExchangeWnd, "m_Cells")
RegistClassMember(CLuaFurnitureExchangeWnd, "m_ItemTempId2ZTId")
RegistClassMember(CLuaFurnitureExchangeWnd, "m_CurrentTotalScoreNeeded")
RegistClassMember(CLuaFurnitureExchangeWnd, "m_SelectedItemId")
RegistClassMember(CLuaFurnitureExchangeWnd, "m_Tab")
RegistClassMember(CLuaFurnitureExchangeWnd, "m_FurnitureTemplate")
RegistClassMember(CLuaFurnitureExchangeWnd, "m_Furnitures")
RegistClassMember(CLuaFurnitureExchangeWnd, "m_EmptyTip")

function CLuaFurnitureExchangeWnd:Awake()
	CUIManager.CloseUI(CUIResources.PackageWnd)
	self.m_Furnitures = {}
	self.m_Cells = {}
	self.m_ItemTempId2ZTId = {}
	Zhuangshiwu_Zhuangshiwu.Foreach(function (tid, designData)
		self.m_ItemTempId2ZTId[designData.ItemId] = tid
	end)
end

function CLuaFurnitureExchangeWnd:Init()
	self.m_EmptyTip = self.transform:Find("Root/EmptyTip").gameObject
	self.m_TipLabel = self.transform:Find("Root/GameObject").gameObject
	self.m_ItemCellTemplate.gameObject:SetActive(false)
	self.m_Tab = self.transform:Find("Root/Tab").gameObject:GetComponent(typeof(QnRadioBox))
	self.m_Tab.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		if index+1 == CLuaClientFurnitureMgr.ShowTableType then return end
		if index == 0 and not CClientHouseMgr.Inst:IsHouseOwner() then -- 没有自己的房子
			self.m_Tab:ChangeTo(1)
			g_MessageMgr:ShowMessage("ExchangeFurniture_NeedHouse")
			return
		end
		CLuaClientFurnitureMgr.ShowTableType = index+1
		self:ShowItems()
	end)
	self.m_FurnitureTemplate = self.transform:Find("Root/m_FurnitureTemplate").gameObject
	self.m_FurnitureTemplate:SetActive(false)
	UIEventListener.Get(self.transform:Find("Root/TipButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("ExchangeFurniture_Rule")
    end)
	CommonDefs.AddOnClickListener(self.m_DoBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		if CLuaClientFurnitureMgr.ShowTableType == 1 then  -- 仓库
			local furnitureIds = {}
			local color = nil
			for i, v in ipairs(self.m_Furnitures) do -- {furniture, RepairNum, templateId, count, recyleNum}
				if #v > 0 and v[1].transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox)).Selected then
					table.insert(furnitureIds, {v[3], v[5], i})
					local designData = Item_Item.GetData(Zhuangshiwu_Zhuangshiwu.GetData(v[3]).ItemId)
					if designData.NameColor== "COLOR_PURPLE" then
						color = LocalString.GetString("紫")
					end
				end
			end

			if CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.JiaYuanJiFen) < self.m_CurrentTotalScoreNeeded then
				g_MessageMgr:ShowMessage("FURNITURE_EXCHANGE_NO_SCORE")
				return
			end

			if next(furnitureIds) then
				local function ok()
					for _, data in ipairs(furnitureIds) do
						Gac2Gas.RequestExchangeRepoFurnitureToRepairMeterial(data[1], data[2]) -- templateId, count
					end
					-- CUIManager.CloseUI(CUIResources.FurnitureExchangeWnd)
					-- CClientFurnitureMgr.Inst:StopFurnish()
					for _, data in ipairs(furnitureIds) do
						local furniture = self.m_Furnitures[data[3]]
						furniture[1].transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox)).Selected = false
						local CountLabel = furniture[1].transform:Find("AmountLabel").gameObject
						CountLabel:SetActive(furniture[4] > 1)
						CountLabel:GetComponent(typeof(UILabel)).text = tostring(furniture[4])
						furniture[5] = 0
					end
					self.m_Num = 0
					self.m_TotalRepaire = 0
					self.m_TargetNum.text = tostring(0)
					self.m_CurrentTotalScoreNeeded = self.m_Num
					self.m_NeedHouseScoreLbl.text = tostring(self.m_Num)
				end
				if color then
					local msg = g_MessageMgr:FormatMessage("FURNITURE_EXCHANGE_CONFIRM", color)
					g_MessageMgr:ShowOkCancelMessage(msg, ok, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), true)
				else
					ok()
				end
			else
				g_MessageMgr:ShowMessage("FURNITURE_EXCHANGE_NEED_ITEM")
				return
			end
		else
			local itemIds = {}
			local color = nil
			for itemCell, data in pairs(self.m_Cells) do
				local itemId = data[1]
				if itemCell.isChecked then
					table.insert(itemIds, itemId)
					local item = CItemMgr.Inst:GetById(itemId)
					local itemTempId = item and item.TemplateId
					local designData = itemTempId and Item_Item.GetData(itemTempId)
					if designData.NameColor== "COLOR_PURPLE" then
						color = LocalString.GetString("紫")
					end
				end
			end

			if CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.JiaYuanJiFen) < self.m_CurrentTotalScoreNeeded then
				g_MessageMgr:ShowMessage("FURNITURE_EXCHANGE_NO_SCORE")
				return
			end

			if next(itemIds) then
				local function ok()
					Gac2Gas.RequestExchangeFurnitureWithRepaireMeterialBegin()
					for _, itemId in ipairs(itemIds) do
						Gac2Gas.RequestExchangeFurnitureWithRepaireMeterial(itemId)
					end
					Gac2Gas.RequestExchangeFurnitureWithRepaireMeterialEnd()
					CUIManager.CloseUI(CUIResources.FurnitureExchangeWnd)
				end
				if color then
					local msg = g_MessageMgr:FormatMessage("FURNITURE_EXCHANGE_CONFIRM", color)
					g_MessageMgr:ShowOkCancelMessage(msg, ok, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), true)
				else
					ok()
				end
			else
				g_MessageMgr:ShowMessage("FURNITURE_EXCHANGE_NEED_ITEM")
				return
			end
		end
	end), false)

	CommonDefs.AddOnClickListener(self.CloseButton.gameObject, DelegateFactory.Action_GameObject(function (go)
		CUIManager.CloseUI(CUIResources.FurnitureExchangeWnd)
	end), false)

	local msg = g_MessageMgr:FormatMessage("FURNITURE_EXCHANGE_TIPS")
	if msg and #msg > 0 then
		self.m_ExchangeInfoLbl.text = msg
	end
	self.Inited = {false, false}
	self:ShowItems()
end

function CLuaFurnitureExchangeWnd:ShowItems()
	local current = CLuaClientFurnitureMgr.ShowTableType
	local another = (CLuaClientFurnitureMgr.ShowTableType == 1) and 2 or 1
	self.m_Tab:ChangeTo(current - 1)
	self:SetState(current, true)
	self:SetState(another, false)

	self.m_Grid:Reposition()
	self.m_ScrollView:ResetPosition()

	self.m_TotalHouseScoreLbl.text = tostring(CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.JiaYuanJiFen))

end

function CLuaFurnitureExchangeWnd:SetState(type, state)
	if self.Inited[type] == false and state == false then return end
	if self.Inited[type] == false then 
		self:InitItem(type) 
		self.Inited[type] = true
	end
	-- 已经初始化过了

	if type == 1 then
		for i, v in ipairs(self.m_Furnitures) do
			if #v > 0 then v[1]:SetActive(state) end
		end
	else
		for k, v in pairs(self.m_Cells) do
			k.gameObject:SetActive(state)
		end
	end
	if state then
		local count = 0
		self.m_CurrentTotalScoreNeeded = 0
		self.m_TotalRepaire = 0
		self.m_Num = 0
		self.m_NeedHouseScoreLbl.text = "0"
		self.m_TargetNum.text = "0"

		if type == 1 then
			if self.m_longPressSelected and #self.m_Furnitures[self.m_longPressSelected] > 0 then
				self.m_Furnitures[self.m_longPressSelected][1].transform:Find("SelectedSprite").gameObject:SetActive(false)
				self.m_longPressSelected = nil
			end
			for i, v in ipairs(self.m_Furnitures) do
				if #v > 0 then
					v[1].transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox)).Selected = false
					--recycle
					if v[4] > 1 and v[5] > 0 then
						v[1].transform:Find("AmountLabel").gameObject:GetComponent(typeof(UILabel)).text = tostring(v[4])
						v[5] = 0
					end
					count = count + 1
				end
			end
		else
			for k, data in pairs(self.m_Cells) do
				k.isChecked = false
				if data[2] > 0 then
					-- local t = CItemMgr.Inst:GetById(k.ItemId).Item.Count
					-- k.amountLabel.gameObject:SetActive(t > 1)
					-- k.amountLabel.text = tostring(t)
					data[2] = 0
				end
				count = count + 1
			end
		end

		self.m_EmptyTip:SetActive(count == 0)
		if count == 0 then
			self.m_EmptyTip:GetComponent(typeof(UILabel)).text = (type == 1 and LocalString.GetString("装修仓库中暂无可拆解的家具") or LocalString.GetString("包裹中暂无可拆解的家具") )
		end
		self.m_TipLabel:SetActive(count > 0)
	end
end

function CLuaFurnitureExchangeWnd:AddFurnitureToList(data, idx)
	local templateId = data[1]
	local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
	local count = data[2]
	local itemdata = Item_Item.GetData(zswdata.ItemId)
	local cell = NGUITools.AddChild(self.m_Grid.gameObject, self.m_FurnitureTemplate)
	cell:SetActive(true)
	cell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(itemdata.Icon)

	local CountLabel = cell.transform:Find("AmountLabel").gameObject
	local RepairNum = zswdata.Repair
	local index = #self.m_Furnitures + 1
	CountLabel:SetActive(count > 1)
	CountLabel:GetComponent(typeof(UILabel)).text = tostring(count)
	cell.transform:Find("QualitySprite"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(itemdata, nil, false)

	local function func()
		local Checkbox = cell.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
		local state = not Checkbox.Selected
		local count = self.m_Furnitures[index][4]

		-- 更新家园积分和废旧家具材料数量
		local function update(val)
			local data = self.m_Furnitures[index]
			local recyleNum = state and val or -data[5]
			self.m_Num = self.m_Num + recyleNum
			self.m_TotalRepaire = self.m_TotalRepaire + RepairNum*recyleNum
			self.m_TargetNum.text = tostring(math.ceil(self.m_TotalRepaire/Zhuangshiwu_Setting.GetData().MerterialPerItem))
			self.m_CurrentTotalScoreNeeded = self.m_Num
			self.m_NeedHouseScoreLbl.text = tostring(self.m_Num)
			data[5] = val --recycle
		end
		
		if state == false then
			CountLabel:SetActive(count > 1)
			CountLabel:GetComponent(typeof(UILabel)).text = tostring(count)
			update(0)
			Checkbox.Selected = false
		elseif count > 1 then
			CLuaNumberInputMgr.ShowNumInputBox(1, count, 1, function (val)
				CountLabel:GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d(-[c][ffff00]%d[-][/c])", count, val)
				update(val)
				Checkbox.Selected = true
			end, SafeStringFormat3(LocalString.GetString("请选择%s的拆解数量"), itemdata.Name), -1)
		else
			CountLabel:GetComponent(typeof(UILabel)).text = tostring(count)
			update(count)
			Checkbox.Selected = true
		end
		
	end
	cell:GetComponent(typeof(UILongPressButton)).OnClickDelegate = DelegateFactory.Action(func)
	local function longPressFunc()
		if self.m_longPressSelected and #self.m_Furnitures[self.m_longPressSelected] > 0 then
			self.m_Furnitures[self.m_longPressSelected][1].transform:Find("SelectedSprite").gameObject:SetActive(false)
		end
		self.m_longPressSelected = index
		self.m_Furnitures[self.m_longPressSelected][1].transform:Find("SelectedSprite").gameObject:SetActive(true)

		local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs(nil, templateId)
		local actionSource=DefaultItemActionDataSource.Create(cnt,actions,names)
		CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = templateId
		CItemInfoMgr.showType = ShowType.FurnitureRepository
		CItemInfoMgr.linkItemInfo = LinkItemInfo(templateId, false, actionSource)
		CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
		CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
	end
	cell:GetComponent(typeof(UILongPressButton)).OnLongPressDelegate = DelegateFactory.Action(longPressFunc)
	cell.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox)).Selected = false
	table.insert(self.m_Furnitures, {cell, RepairNum, templateId, count, 0})

	if templateId == self.m_SelectedItemId then
		func()
	end
end

function CLuaFurnitureExchangeWnd:InitItem(type)
	local cellWidth = self.m_Grid.cellWidth
	local cellHeight = self.m_Grid.cellHeight
	local maxPerLine = self.m_Grid.maxPerLine
	local isHorizontal = self.m_Grid.arrangement == UIGrid.Arrangement.Horizontal
	local orange, blue, red, purple = {}, {}, {}, {}

	local function collect_fun(zswdata, color, id, IsBinded)
		if zswdata and zswdata.Type ~= EnumFurnitureType_lua.eLingshou and --灵兽
			zswdata.Type ~= EnumFurnitureType_lua.eMapi and --马匹
			zswdata.Type ~= EnumFurnitureType_lua.eNpc and --
			zswdata.Type ~= EnumFurnitureType_lua.eWallPaper and --墙纸
			zswdata.Type ~= EnumFurnitureType_lua.eZswTemplate and --模板
			zswdata.Type ~= EnumFurnitureType_lua.eSkyBox and --天空盒
			zswdata.Type ~= EnumFurnitureType_lua.eChuanjiabao and --传家宝
			zswdata.Type ~= EnumFurnitureType_lua.eJianzhu and --建筑
			zswdata.NoDecompose ~= 1 then
			if color == "COLOR_ORANGE" then
				if IsBinded then
					table.insert(orange, 1, id)
				else
					table.insert(orange, id)
				end
			elseif color == "COLOR_BLUE" then
				if IsBinded then
					table.insert(blue, 1, id)
				else
					table.insert(blue, id)
				end
			elseif color == "COLOR_RED" then
				if IsBinded then
					table.insert(red, 1, id)
				else
					table.insert(red, id)
				end
			elseif color == "COLOR_PURPLE" then
				if IsBinded then
					table.insert(purple, 1, id)
				else
					table.insert(purple, id)
				end
			end
		end
	end

	if type == 1 then
		local QiShuDict = {}
		if CClientHouseMgr.Inst.mFurnitureProp ~= nil then
			CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.QiShuFurnitureData, DelegateFactory.Action_object_object(function (itemid, ___value) 
				local item = CItemMgr.Inst:GetById(itemid)
				if item ~= nil then
					local itemdata = Item_Item.GetData(item.TemplateId)
					if itemdata ~= nil then
						local name = itemdata.Name
						if QiShuDict[name] then
							QiShuDict[name] = QiShuDict[name] + 1
						else
							QiShuDict[name] = 1
						end
					end
				end
			end))
			CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.RepositoryData,DelegateFactory.Action_object_object(function (templateId, count)
				if count > 0 then
					local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
					if QiShuDict[zswdata.Name] then count = count - QiShuDict[zswdata.Name] end --去掉气数装饰物
					if count == 0 then return end
					local item = Item_Item.GetData(zswdata.ItemId)
					collect_fun(zswdata, item and item.NameColor, {templateId, count})
				end
			end))
		end

		self.m_NeedHouseScoreLbl.text = "0"
		self.m_TargetNum.text = "0"

		local index = 0
		for _, templateId in ipairs(orange) do
			self:AddFurnitureToList(templateId, index)
			index = index + 1
		end
		for _, templateId in ipairs(blue) do
			self:AddFurnitureToList(templateId, index)
			index = index + 1
		end
		for _, templateId in ipairs(red) do
			self:AddFurnitureToList(templateId, index)
			index = index + 1
		end
		for _, templateId in ipairs(purple) do
			self:AddFurnitureToList(templateId, index)
			index = index + 1
		end
	else
		CPackageViewItemResource.Inst.Resource = self.m_ItemCellTemplate.gameObject

		local n = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)

		for i = 0, n-1 do
			local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
			local item = itemId and CItemMgr.Inst:GetById(itemId)
			local itemTemplateId = item and item.TemplateId
			local data = itemTemplateId and Item_Item.GetData(itemTemplateId)
			local tid = itemTemplateId and self.m_ItemTempId2ZTId[itemTemplateId]
			local zswdata = tid and Zhuangshiwu_Zhuangshiwu.GetData(tid)
			collect_fun(zswdata, data and data.NameColor, itemId, item and item.IsBinded)
		end

		self.m_NeedHouseScoreLbl.text = "0"
		self.m_TargetNum.text = "0"

		local function add_item_to_list(itemId, idx)
			local item = itemId and CItemMgr.Inst:GetById(itemId)
			local itemTemplateId = item and item.TemplateId
			local tid = itemTemplateId and self.m_ItemTempId2ZTId[itemTemplateId]
			if tid then
				local cell = CResourceMgr.Inst:Instantiate(CPackageViewItemResource.Inst, false, 1)
				local x, y = idx-1, 0
				if maxPerLine > 0 then
					x, y = idx%maxPerLine, math.floor((idx+1)/maxPerLine)
				end
				cell.transform.parent = self.m_Grid.transform
				if isHorizontal then
					cell.transform.localPosition = Vector3(cellWidth*x, (-cellHeight)*y, 0)
				else
					cell.transform.localPosition = Vector3(cellWidth*y, (-cellHeight)*x, 0)
				end

				cell.transform.localRotation = Quaternion.identity
				cell.transform.localScale = Vector3.one
				cell:SetActive(true)
				local itemCell = CommonDefs.LuaGetComponent(cell, typeof(CPackageItemCell))
				local function func(itemCell)
					local isChecked = not itemCell.isChecked
					local count = CItemMgr.Inst:GetById(itemCell.ItemId).Item.Count
					local function update()
						local totalRepaire = 0
						local num = 0
						for cachedItemCell, data in pairs(self.m_Cells) do
							if cachedItemCell.isChecked then
								local itemId = cachedItemCell.ItemId
								local item = CItemMgr.Inst:GetById(itemId)
								if not item then return end
	
								local itemTemplateId = item.TemplateId
								local ztid = self.m_ItemTempId2ZTId[itemTemplateId]
								local designData = ztid and Zhuangshiwu_Zhuangshiwu.GetData(ztid)
								local repaire = designData.Repair * data[2]
								totalRepaire = totalRepaire + repaire
								num = num + data[2]
							end
						end
	
						self.m_TargetNum.text = tostring(math.ceil(totalRepaire/Zhuangshiwu_Setting.GetData().MerterialPerItem))
						self.m_CurrentTotalScoreNeeded = num
	
						self.m_NeedHouseScoreLbl.text = tostring(num)
					end

					-- if isChecked and count > 1 then
					-- 	CLuaNumberInputMgr.ShowNumInputBox(1, count, 1, function (val)
					-- 	itemCell.amountLabel.text = SafeStringFormat3("%d(-[c][ffff00]%d[-][/c])", count, val)
					-- 	itemCell.isChecked = true
					-- 	self.m_Cells[itemCell][2] = val
					-- 	update()
					-- end, LocalString.GetString("请选中拆解数量"), -1)
					-- else
						itemCell.amountLabel.gameObject:SetActive(count > 1)
						itemCell.amountLabel.text = tostring(count)
						itemCell.isChecked = isChecked
						self.m_Cells[itemCell][2] = isChecked and count or 0
						update()
					-- end
				end
				itemCell.OnItemClickDelegate = DelegateFactory.Action_CPackageItemCell(func)
				local function longPressFunc(itemCell)
					for cachedItemCell, _ in pairs(self.m_Cells) do
						cachedItemCell.Selected = false
					end
					itemCell.Selected = true
					CItemInfoMgr.ShowLinkItemInfo(item, false, nil, AlignType.ScreenRight, itemCell.transform.position.x, itemCell.transform.position.y, 0, 0, 0)
				end
				itemCell.OnItemLongPressDelegate = DelegateFactory.Action_CPackageItemCell(longPressFunc)
				itemCell:Init(itemId, false, true, false, nil)
				itemCell.Selected = false

				self.m_Cells[itemCell] = {itemId, 0}

				if itemId == self.m_SelectedItemId then
					func(itemCell)
				end

			end
		end

		local index = 0
		for _, itemId in ipairs(orange) do
			add_item_to_list(itemId, index)
			index = index + 1
		end
		for _, itemId in ipairs(blue) do
			add_item_to_list(itemId, index)
			index = index + 1
		end
		for _, itemId in ipairs(red) do
			add_item_to_list(itemId, index)
			index = index + 1
		end
		for _, itemId in ipairs(purple) do
			add_item_to_list(itemId, index)
			index = index + 1
		end
	end

end

function CLuaFurnitureExchangeWnd:OnEnable()
	g_ScriptEvent:AddListener("SetRepositoryFurnitureCount",self,"UpdateRepositoryFurnitureCount")
end

function CLuaFurnitureExchangeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SetRepositoryFurnitureCount",self,"UpdateRepositoryFurnitureCount")
	self.m_Cells = {}
	self.m_Furnitures = {}
end

function CLuaFurnitureExchangeWnd:UpdateRepositoryFurnitureCount(templateId, count)
	-- 顺便更新家园积分
	self.m_TotalHouseScoreLbl.text = tostring(CClientMainPlayer.Inst.PlayProp:GetScoreByKey(EnumPlayScoreKey.JiaYuanJiFen))
	-- count 包括了气数装饰物，先减掉
	local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
	if CClientHouseMgr.Inst.mFurnitureProp ~= nil then
		CommonDefs.DictIterate(CClientHouseMgr.Inst.mFurnitureProp.QiShuFurnitureData, DelegateFactory.Action_object_object(function (itemid, ___value) 
			local item = CItemMgr.Inst:GetById(itemid)
			if item ~= nil then
				local itemdata = Item_Item.GetData(item.TemplateId)
				if itemdata ~= nil then
					if itemdata.Name == zswdata.Name then
						count = count - 1
					end
				end
			end
		end))
	end

	if self.m_Furnitures == nil then return end
    for i, v in ipairs(self.m_Furnitures) do --cell, RepairNum, templateId, count, countSelected
		if #v > 0 and v[3] == templateId then
			if count == 0 then
				local recyleNum = -v[5]
				self.m_Num = self.m_Num + recyleNum
				self.m_TotalRepaire = self.m_TotalRepaire + v[2]*recyleNum
				self.m_TargetNum.text = tostring(math.ceil(self.m_TotalRepaire/Zhuangshiwu_Setting.GetData().MerterialPerItem))
				self.m_CurrentTotalScoreNeeded = self.m_Num
				self.m_NeedHouseScoreLbl.text = tostring(self.m_Num)
				-- recycle
				self.m_Furnitures[i][1]:SetActive(false)
				self.m_Furnitures[i][1].transform.parent = nil
				GameObject.Destroy(self.m_Furnitures[i][1])
				self.m_Furnitures[i] = {}   --代替remove
			elseif count < v[5] then
				local recyleNum = count - v[5]
				self.m_Num = self.m_Num + recyleNum
				self.m_TotalRepaire = self.m_TotalRepaire + v[2]*recyleNum
				self.m_TargetNum.text = tostring(math.ceil(self.m_TotalRepaire/Zhuangshiwu_Setting.GetData().MerterialPerItem))
				self.m_CurrentTotalScoreNeeded = self.m_Num
				self.m_NeedHouseScoreLbl.text = tostring(self.m_Num)

				v[4] = count
				v[5] = count
				local CountLabel = v[1].transform:Find("AmountLabel").gameObject
				CountLabel:SetActive(count > 1)
				CountLabel:GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d(-[c][ffff00]%d[-][/c])", count, count)
			else
				v[4] = count
				local CountLabel = v[1].transform:Find("AmountLabel").gameObject
				CountLabel:SetActive(count > 1)
				CountLabel:GetComponent(typeof(UILabel)).text = v[5] > 0 and SafeStringFormat3("%d(-[c][ffff00]%d[-][/c])", count, v[5]) or tostring(count)
			end
			self.m_Grid.repositionNow = true
			self.m_Grid:Reposition()
			return
		end
	end
	if count > 0 then
		self:AddFurnitureToList({templateId, count})
		self.m_Grid.repositionNow = true
		self.m_Grid:Reposition()
	end
end
