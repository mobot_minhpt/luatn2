-- Auto Generated!!
local CHorseRaceMgr = import "L10.Game.CHorseRaceMgr"
local CHorseRaceSelectWnd = import "L10.UI.CHorseRaceSelectWnd"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
CHorseRaceSelectWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    if System.String.IsNullOrEmpty(CHorseRaceMgr.Inst.currentSelectHorseId) then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NO_HOUSE_SELECTED_CONFIRM"), MakeDelegateFromCSFunction(this.DoClose, Action0, this), nil, nil, nil, false)
    else
        this:Close()
    end
end
CHorseRaceSelectWnd.m_DoClose_CS2LuaHook = function (this)
    if not this then
        return
    end
    
    --默认选马
    if CZuoQiMgr.Inst.zuoqis ~= nil and CZuoQiMgr.Inst.zuoqis.Count > 0 then
        Gac2Gas.MPTYSFinishSelectHorse(CZuoQiMgr.Inst.zuoqis[0].id)
    end
    this:Close()
end
