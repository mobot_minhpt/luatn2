local UIBasicSprite = import "UIBasicSprite"
-- local CFightingSpiritDamageWnd = import "L10.UI.CFightingSpiritDamageWnd"
local CBiWuDaHuiMgr=import "L10.Game.CBiWuDaHuiMgr"

CLuaFightingSpiritDamageWnd = class()
RegistClassMember(CLuaFightingSpiritDamageWnd,"NameLabel1")
RegistClassMember(CLuaFightingSpiritDamageWnd,"NameLabel2")
RegistClassMember(CLuaFightingSpiritDamageWnd,"DamageLabel1")
RegistClassMember(CLuaFightingSpiritDamageWnd,"DamageLabel2")
RegistClassMember(CLuaFightingSpiritDamageWnd,"Button")
RegistClassMember(CLuaFightingSpiritDamageWnd,"InfoRoot")
RegistClassMember(CLuaFightingSpiritDamageWnd,"m_KillNum1Label")
RegistClassMember(CLuaFightingSpiritDamageWnd,"m_KillNum2Label")
CLuaFightingSpiritDamageWnd.Name1=nil
CLuaFightingSpiritDamageWnd.Name2=nil
function CLuaFightingSpiritDamageWnd:Awake()
    self.NameLabel1 = self.transform:Find("ShowArea/InfoRoot/BG1/NameLabel"):GetComponent(typeof(UILabel))
    self.NameLabel2 = self.transform:Find("ShowArea/InfoRoot/BG2/NameLabel"):GetComponent(typeof(UILabel))
    self.DamageLabel1 = self.transform:Find("ShowArea/InfoRoot/BG1/Label"):GetComponent(typeof(UILabel))
    self.DamageLabel2 = self.transform:Find("ShowArea/InfoRoot/BG2/Label"):GetComponent(typeof(UILabel))
    self.Button = self.transform:Find("ShowArea/ExpandButton"):GetComponent(typeof(UISprite))
    self.InfoRoot = self.transform:Find("ShowArea/InfoRoot").gameObject
    UIEventListener.Get(self.Button.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        self:onButtonClick(go)
    end)
    self.m_KillNum1Label = self.transform:Find("ShowArea/InfoRoot/BG1/KillNumLabel"):GetComponent(typeof(UILabel))
    self.m_KillNum1Label.gameObject:SetActive(false)
    self.m_KillNum2Label = self.transform:Find("ShowArea/InfoRoot/BG2/KillNumLabel"):GetComponent(typeof(UILabel))
    self.m_KillNum2Label.gameObject:SetActive(false)
end

function CLuaFightingSpiritDamageWnd:Init()
    if CLuaFightingSpiritDamageWnd.Name1 and CLuaFightingSpiritDamageWnd.Name2 then
        self.NameLabel1.text = CUICommonDef.cutDown(LocalString.TranslateAndFormatText(CLuaFightingSpiritDamageWnd.Name1))
        self.NameLabel2.text = CUICommonDef.cutDown(LocalString.TranslateAndFormatText(CLuaFightingSpiritDamageWnd.Name2))
    end
end

function CLuaFightingSpiritDamageWnd:onButtonClick( go) 
    if self.InfoRoot.activeSelf then
        self.Button.flip = UIBasicSprite.Flip.Nothing
        self.InfoRoot:SetActive(false)
    else
        self.Button.flip = UIBasicSprite.Flip.Horizontally
        self.InfoRoot:SetActive(true)
    end
end
function CLuaFightingSpiritDamageWnd:OnEnable()
    g_ScriptEvent:AddListener("FightingSpiritTeamDamageReceive",self,"onDamageReceive")
end
function CLuaFightingSpiritDamageWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FightingSpiritTeamDamageReceive",self,"onDamageReceive")
end
function CLuaFightingSpiritDamageWnd:onDamageReceive(forceDPS_U)
    local tmp = MsgPackImpl.unpack(forceDPS_U)
    local damage1,damage2=0,0
    local killNum1,killNum2=0,0
    for i=1,tmp.Count do
        local data = tmp[i-1]
        local index=data[0]
        if CBiWuDaHuiMgr.Inst.inBiWu or LuaColiseumMgr:IsInPlay() then--比武里面 第一个是防守 第二个是进攻 蓝色防守 红色进攻
            if index==0 then
                self.DamageLabel2.text=tostring(math.floor(data[1]))
                if data.Count>=3 then
                    self.m_KillNum2Label.gameObject:SetActive(true)
                    self.m_KillNum2Label.text = tostring(data[2])
                else
                    self.m_KillNum2Label.gameObject:SetActive(false)
                end
            else
                self.DamageLabel1.text=tostring(math.floor(data[1]))
                if data.Count>=3 then
                    self.m_KillNum1Label.gameObject:SetActive(true)
                    self.m_KillNum1Label.text = tostring(data[2])
                else
                    self.m_KillNum1Label.gameObject:SetActive(false)
                end
            end
        else
            if index==0 then
                self.DamageLabel1.text=tostring(math.floor(data[1]))
                if data.Count>=3 then
                    self.m_KillNum1Label.gameObject:SetActive(true)
                    self.m_KillNum1Label.text = tostring(data[2])
                else
                    self.m_KillNum1Label.gameObject:SetActive(false)
                end
            else
                self.DamageLabel2.text=tostring(math.floor(data[1]))
                if data.Count>=3 then
                    self.m_KillNum2Label.gameObject:SetActive(true)
                    self.m_KillNum2Label.text = tostring(data[2])
                else
                    self.m_KillNum2Label.gameObject:SetActive(false)
                end
            end
        end
    end
end
