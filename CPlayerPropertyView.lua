-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerPropertyView = import "L10.UI.CPlayerPropertyView"
local CTitleMgr = import "L10.Game.CTitleMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local L10 = import "L10"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local Profession = import "L10.Game.Profession"
local String = import "System.String"
local Title_Title = import "L10.Game.Title_Title"
local Transform_Transform = import "L10.Game.Transform_Transform"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VectorDelegate = import "UIEventListener+VectorDelegate"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CButtonClippable = import "L10.UI.CButtonClippable"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local Vector3 = import "UnityEngine.Vector3"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
CPlayerPropertyView.m_Init_CS2LuaHook = function (this) 
    this.titleLabel.text = nil
    this.playerNameLabel.text = nil
    this.playerLevelLabel.text = nil
    this.playerId.label.text = nil
    this.playerClassLabel.text = nil
    this.curRotation = 180
    local mainPlayer = CClientMainPlayer.Inst
    if CClientMainPlayer.Inst ~= nil then
        this:OnTitleUpdate(0)
        this.playerNameLabel.text = mainPlayer.Name
        this:UpdateLevel()
        this.playerId.content = tostring(mainPlayer.Id)
        this.playerClassLabel.text = Profession.GetFullName(mainPlayer.Class)
        --OnHideHelmetUpdate();
    end
    this:LoadModel()
    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
    this.tabBar:ChangeTab(this.index, false)
    --UIEventListener.Get(showHelmetSwitchButton).onClick = this.OnHideHelmetButtonClick;
    this:UpdateSettingAlert()

end
CPlayerPropertyView.m_UpdateSettingAlert_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eCommonGameSetting) then
        this.settingBtnAlert:SetActive(true)
        return
    end
    local gameSetting = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eCommonGameSetting)
    local settingDic = TypeAs(MsgPackImpl.unpack(gameSetting.Data), typeof(MakeGenericClass(Dictionary, String, Object)))
    local key = tostring((EnumGameSetting_lua.eCheckShenbinHideButton))
    this.settingBtnAlert:SetActive(not CommonDefs.DictContains(settingDic, typeof(String), key) or ToStringWrap(CommonDefs.DictGetValue(settingDic, typeof(String), key)) ~= "1")
end
CPlayerPropertyView.m_Load_CS2LuaHook = function (this, renderObj) 
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        local enablecloth = QnModelPreviewer.EnableCloth
        CClientMainPlayer.LoadResource(renderObj, mainPlayer.AppearanceProp, true, 1, 0, false, mainPlayer.AppearanceProp.ResourceId, false, 0, false, nil, false, enablecloth)
        if mainPlayer.AppearanceProp.ResourceId ~= 0 then
            local scale = 1
            local data = Transform_Transform.GetData(mainPlayer.AppearanceProp.ResourceId)
            if data ~= nil then
                scale = data.PreviewScale
            end
            renderObj.Scale = scale
            LuaPlayerPropertyView.maxPreviewScale = nil
            LuaPlayerPropertyView.maxModelPos = nil
            LuaUIModelPreviewMgr:RemoveModelPreviewScaleTransformListener("__MainPlayerModelCamera__")
        else
            LuaPlayerPropertyView:SetModelTransformData(mainPlayer.AppearanceProp.Class,mainPlayer.AppearanceProp.Gender,renderObj)
        end
    end
end
CPlayerPropertyView.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.infoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.infoBtn).onClick, MakeDelegateFromCSFunction(this.OnInfoButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.settingBtn).onClick = MakeDelegateFromCSFunction(this.OnSettingBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.titleButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.titleButton).onClick, MakeDelegateFromCSFunction(this.OnTitleButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.swipeGo).onDrag = CommonDefs.CombineListner_VectorDelegate(UIEventListener.Get(this.swipeGo).onDrag, MakeDelegateFromCSFunction(this.OnDragModel, VectorDelegate, this), true)
    UIEventListener.Get(this.xingGuanBtn).onClick = MakeDelegateFromCSFunction(this.OnXingGuanBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.transform:Find("TopPanel/WuXingBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.WuXingMingGeWnd)
    end)
	UIEventListener.Get(this.soulCoreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.SoulCoreWnd)
    end)
end
CPlayerPropertyView.m_OnEnable_CS2LuaHook = function (this)
    LuaPlayerPropertyView:OnEnable(this)
    --EventManager.AddListener(EnumEventType.MainPlayerTotalEquipScoreUpdate, this.UpdateEquipScore);
    EventManager.AddListener(EnumEventType.MainPlayerLevelChange, MakeDelegateFromCSFunction(this.UpdateLevel, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerAppearanceUpdate, MakeDelegateFromCSFunction(this.LoadModel, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnTitleUpdate, MakeDelegateFromCSFunction(this.OnTitleUpdate, MakeGenericClass(Action1, UInt32), this))
    --EventManager.AddListener(EnumEventType.OnHideHelmetUpdate, this.OnHideHelmetUpdate);

    this.xingGuanBtn:SetActive(false)
    local soulCoreLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 0
    this.soulCoreBtn:SetActive(LuaZongMenMgr.m_EnableSoulCore and soulCoreLv > 0)
    this.transform:Find("TopPanel/WuXingBtn").gameObject:SetActive(false)
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp ~= nil and CClientMainPlayer.Inst.PlayProp.XingguanProperty.ActiveFlag > 0 then
        this.xingGuanBtn:SetActive(true)
    end
    --if (CanHoldDunpaiClass())
    --{
    --showDunpaiSwitchButton.SetActive(true);
    --showHelmetSwitchButton.transform.SetLocalPositionX(-640);
    --UIEventListener.Get(showDunpaiSwitchButton).onClick = this.OnHideDunpaiButtonClick;
    --    OnHideDunpaiUpdate();
    --}
    --else
    --{
    --    showDunpaiSwitchButton.SetActive(false);
    --    showHelmetSwitchButton.transform.SetLocalPositionX(-555);
    --}
end
CPlayerPropertyView.m_OnDisable_CS2LuaHook = function (this)
    LuaPlayerPropertyView:OnDisable()
    --EventManager.RemoveListener(EnumEventType.MainPlayerTotalEquipScoreUpdate, this.UpdateEquipScore);
    EventManager.RemoveListener(EnumEventType.MainPlayerLevelChange, MakeDelegateFromCSFunction(this.UpdateLevel, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerAppearanceUpdate, MakeDelegateFromCSFunction(this.LoadModel, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnTitleUpdate, MakeDelegateFromCSFunction(this.OnTitleUpdate, MakeGenericClass(Action1, UInt32), this))
end
CPlayerPropertyView.m_OnTitleUpdate_CS2LuaHook = function (this, titleId) 
    local title = Title_Title.GetData(CClientMainPlayer.Inst.PlayProp.ShowTitleId)
    if title ~= nil then
        this.titleLabel.text = CTitleMgr.Inst.FullName
        this.titleLabel.color = CTitleMgr.Inst:GetTitleColor(title)
    end
end
CPlayerPropertyView.m_OnSettingBtnClick_CS2LuaHook = function (this, go) 
    if this.settingBtnAlert.activeSelf then
        Gac2Gas.SetGameSettingInfo(EnumGameSetting_lua.eCheckShenbinHideButton, "1")
        this.settingBtnAlert:SetActive(false)
    end
    -- local luaFunc = (Lua.Inst:GetLuaTable("LuaLiangHaoMgr")["ShowPlayerAppearanceSettingWnd"])
    -- Table2Array({luaFunc(go.transform)}, MakeArrayClass(Object))
    -- luaFunc:Dispose()
    -- luaFunc = nil
    LuaLiangHaoMgr.ShowPlayerAppearanceSettingWnd(go.transform)
end
CPlayerPropertyView.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    this.index = index
    if index == 0 then
        this.basicPropertyView.gameObject:SetActive(true)
        this.detailPropertyView.gameObject:SetActive(false)
        this.basicPropertyView:Init()
    elseif index == 1 then
        this.basicPropertyView.gameObject:SetActive(false)
        this.detailPropertyView.gameObject:SetActive(true)
        this.detailPropertyView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init()
    end
end
CPlayerPropertyView.m_GetXingGuanButton_CS2LuaHook = function (this) 
    if this.xingGuanBtn.activeSelf then
        return this.xingGuanBtn
    else
        if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(L10.Game.Guide.GuideDefine.Phase_FeiShengXingGuan) then
            L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        end
        return nil
    end
end

CPlayerPropertyView.m_LoadModel_CS2LuaHook = function (this) 
    this.modelTexture.mainTexture = CUIManager.CreateModelTexture("__MainPlayerModelCamera__", this,180,
    LuaPlayerPropertyView.defaultModelLocalPos.x,
    LuaPlayerPropertyView.defaultModelLocalPos.y,
    LuaPlayerPropertyView.defaultModelLocalPos.z,false,true,
    LuaPlayerPropertyView.DefauleModelScale,true,false)
end

LuaPlayerPropertyView = {}
LuaPlayerPropertyView.m_View = nil

LuaPlayerPropertyView.DefauleModelScale = nil
LuaPlayerPropertyView.defaultModelLocalPos = nil
LuaPlayerPropertyView.maxPreviewScale = nil
LuaPlayerPropertyView.maxModelPos = nil

function LuaPlayerPropertyView:OnEnable(view)
    self.m_View = view
    self:InitRenderModelData()
    self:OnSyncLiangHaoInfo()
    self:CheckSoulCoreRedPoint()
    self:InitPropertyShareBtn()
    g_ScriptEvent:AddListener("OnSyncLiangHaoInfo", LuaPlayerPropertyView, "OnSyncLiangHaoInfo")
    g_ScriptEvent:AddListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
    g_ScriptEvent:AddListener("OnOpenSoulCoreWnd", self, "OnOpenSoulCoreWnd")
    if LuaZongMenMgr.m_IsOpen then
        local soulCoreLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 0
        local showSoulCore = soulCoreLv > 0

        if not showSoulCore then
            Gac2Gas.QueryPlayerMingGe()
        end
    end
    LuaPlayerPropertyView:RefreshUIModelPreviewListener()
end

function LuaPlayerPropertyView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncLiangHaoInfo", LuaPlayerPropertyView, "OnSyncLiangHaoInfo")
    g_ScriptEvent:RemoveListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreLevel", self, "OnUpdateMainPlayerSoulCoreLevel")
    g_ScriptEvent:RemoveListener("OnOpenSoulCoreWnd", self, "OnOpenSoulCoreWnd")
    self.m_View = nil
    LuaUIModelPreviewMgr:RemoveModelPreviewScaleTransformListener("__MainPlayerModelCamera__")
end

function LuaPlayerPropertyView:OnSyncPlayerMingGe()
    local soulCoreLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 0
    local showSoulCore = soulCoreLv > 0

    self.m_View.soulCoreBtn:SetActive(showSoulCore)
    self.m_View.transform:Find("TopPanel/WuXingBtn").gameObject:SetActive(not showSoulCore)
end

function LuaPlayerPropertyView:OnUpdateMainPlayerSoulCoreLevel()
    local soulCoreLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 0
    local showSoulCore = soulCoreLv > 0

    self.m_View.soulCoreBtn:SetActive(showSoulCore)
    self.m_View.transform:Find("TopPanel/WuXingBtn").gameObject:SetActive(not showSoulCore)
end

function LuaPlayerPropertyView:OnSyncLiangHaoInfo()
    local playerIdComponent = self.m_View.playerId
    local playerLiangHaoComponent = self.m_View.transform:Find("TopPanel/PlayerID/PlayerLiangHaoLabel"):GetComponent(typeof(CButtonClippable))
    local mainPlayer = CClientMainPlayer.Inst
    local lianghaoId = mainPlayer and mainPlayer.BasicProp.ProfileInfo.LiangHaoId or 0
    local expiredTime = mainPlayer and mainPlayer.BasicProp.ProfileInfo.LiangHaoExpiredTime or 0
    if lianghaoId>0 and expiredTime>CServerTimeMgr.Inst.timeStamp then
        playerIdComponent.gameObject:SetActive(false)
        playerLiangHaoComponent.gameObject:SetActive(true)
        playerLiangHaoComponent.content = tostring(lianghaoId)
    else
        playerIdComponent.gameObject:SetActive(true)
        playerLiangHaoComponent.gameObject:SetActive(false)
    end
end

function LuaPlayerPropertyView:CheckSoulCoreRedPoint()
    if CClientMainPlayer.Inst == nil or LuaZongMenMgr.m_LastOpenSoulCoreWndLv == nil then
        return false
    end
    local mainPlayer = CClientMainPlayer.Inst
    local soulCoreLevel = mainPlayer.SkillProp.SoulCore.Level
    if soulCoreLevel > 0 and soulCoreLevel ~= LuaZongMenMgr.m_LastOpenSoulCoreWndLv then
        self.m_View.soulCoreBtn.transform:Find("redpoint").gameObject:SetActive(LuaZongMenMgr:IsMainPlayerSoulCoreUpgradeable())
    end
end

function LuaPlayerPropertyView:OnOpenSoulCoreWnd()
    self.m_View.soulCoreBtn.transform:Find("redpoint").gameObject:SetActive(false)
end

function LuaPlayerPropertyView:InitPropertyShareBtn()
    local ShowBtn = self.m_View.transform:Find("ShareButton").gameObject
    ShowBtn:SetActive(false)
    if not LuaPlayerPropertyMgr.PropertyGuide or not CClientMainPlayer.Inst then return end
    if CClientMainPlayer.Inst.Level > PropGuide_Setting.GetData().ShowPropertyLevel then
        self.m_View.tabBar.transform.localPosition = Vector3(430,342,0)
        ShowBtn:SetActive(true)
    end
    UIEventListener.Get(ShowBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CUICommonDef.CaptureScreenUIAndShare(CUIResources.MainPlayerWnd,ShowBtn)
	end)
end
function LuaPlayerPropertyView:InitRenderModelData()
    self.DefauleModelScale = 1
    self.defaultModelLocalPos = Vector3.zero
    self.defaultModelLocalPos.x, self.defaultModelLocalPos.y, self.defaultModelLocalPos.z = 0,-0.95,5.06
end
function LuaPlayerPropertyView:SetModelTransformData(class,gender,ro)
    if class == EnumClass.YingLing and gender == EnumGender.Monster then 
        self.maxPreviewScale = 2
    else
        self.maxPreviewScale = 4
    end
    if ro:IsAllFinished() then
        local Headtransform = ro:GetSlotTransform("Head",false)
        self.maxModelPos = Vector3.zero
        self.maxModelPos.y = -1 * Headtransform.localPosition.y
        self.maxModelPos.x = self.defaultModelLocalPos.x / self.maxPreviewScale
        self.maxModelPos.z = self.defaultModelLocalPos.z / self.maxPreviewScale
        LuaPlayerPropertyView:RefreshUIModelPreviewListener()
    else
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
            local Headtransform = renderObject:GetSlotTransform("Head",false)
            self.maxModelPos = Vector3.zero
            self.maxModelPos.y = -1 * Headtransform.localPosition.y
            self.maxModelPos.x = self.defaultModelLocalPos.x / self.maxPreviewScale
            self.maxModelPos.z = self.defaultModelLocalPos.z / self.maxPreviewScale
            LuaPlayerPropertyView:RefreshUIModelPreviewListener()
        end))
    end
end

function LuaPlayerPropertyView:RefreshUIModelPreviewListener()
    if self.maxModelPos then
        LuaUIModelPreviewMgr:AddModelPreviewScaleTransformListener("__MainPlayerModelCamera__",
        self.defaultModelLocalPos,self.DefauleModelScale,
        self.maxPreviewScale,self.maxModelPos,
        CUIResources.MainPlayerWnd,false)
    end
end