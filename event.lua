EventManager = import "EventManager"
EnumEventType=import "EnumEventType"
Extensions = import "Extensions"

csharp_events = {}
local csharp_event_names = {
    "GasDisconnect",
    "MainPlayerCreated",
    "MainPlayerLevelChange",
    "MainPlayerPlayPropUpdate",
    "MultiQuizAnswered",
    "HideTopAndRightTipWnd",
    "DiceEndBet",
    "DiceEndDice",
    "DicePlayResult",
    "PlayerShakeDevice",
    "QueryPlayerCurrentBiWuScore",
    "RequestGiftLimitResult",
    "TowerDefenseSyncPlayStatusInfo",
    "TowerDefenseSyncMyPlayPlayerInfo",
    "TowerDefenseSyncFurnitureInfo",
    "TowerDefenseSyncFurniturePlacePos",
    "TowerDefenseSyncPlayPlayerInfoList",
    "TowerDefenseSyncHouseHp",
    "UpdateLingShouBabyInfo",
    "UpdateLingShouMarryInfo",
    "UpdateLingShouBabyLastWashResult",
    "UpdateLingShouExp",
    "TeamGroupMemberInfoReceived",
    "GameVideoListDownloaded",
    "GameVideoDownloaded",
    "TeamGroupTargetChange",
    "TeamGroupSelfPositionChange",
    "TeamGroupLeaderRefresh",
    "SendCurrentHuoGuoMeterial",
    "SubmitHuoGuoMeterialSuccess",
    "UpdateItemCount",
    "CarnivalLotteryResult",
    "SculptureWeddingRingSuccess",
    "ChaoduRollDice",
    "LoveQingQiuAnswerQuestion",
    "UpdateMallLimit",
    "UpdateShangXiaJiaZheKouLiBao",
    "GetAllLingShouOverview",
    "LingShouHeBaZiResult",
    "OnAddPhotoFilterEffectComplete",
    "GetLingShouDetails",
    "SyncHuLuDrop",
    "LianLianKanPlayerChessDone",
    "LianLianKanMatchTwoChessmanSuccess",
    "LianLianKanSyncPlayerChessData",
    "ReplyLianLianKanFinalOverView",
    "OnRankDataReady",
    "LLWSyncSeatInfo",
    "Guide_ChangeView",
    "QueryHeChengGuiCostReturn",
    "GhostComposeSelectEquip",
    "SendItem",
    "RequestCanHeChengGuiReturn",
    "RequestCanSwitchEquipWordSetReturn",
    "NotifyHeChengGuiSuccess",
    "BuyItemFromSecretShopSuccess",
    "OnShouYuanZhenYingUpdate",
    "OnQianKunDaiRefineDone",
    "OnFaBaoDingRefineDone",
    "OnFaBaoDingSelectChange",
    "QianKunDaiShakeFinish",
    "SendXianFanPropertyCompareResult",
    "SceneRemainTimeUpdate",
    "CanLeaveGamePlay",
    "ShowTimeCountDown",
    "TryConfirmBaptizeWordResult",
    "RequestEquipPropertyChangedWithTempWordsReturn",
    "OnEquipScoreImprovementInfoReceived",
    "OnJingLingWebImageReady",
    "OnJingLingProcessTextInputReady",
    "ClientObjCreate",
    "UpdateDLNDraw",
    "KeJuReplayInfoReturn",
    "UpdateScheduleTime",
    "UpdateActivity",
    "OnVoiceTranslateFinished",
    "UpdateDLNState",
    "SetItemAt",
    "UpdateEquipDisable",
    "MainPlayerEquipDurationUpdate",
    "MainPlayerTotalEquipScoreUpdate",
    "EquipSuccess",
    "OnTalismanMenuSelect",
    "OnTalismanMenuClose",
    "QMPKPrayBuffWordResult",
    "SelectLingShouAtRow",
    "QMPKReplySelfPersonalInfo",
    "QMPKSyncLoginCount",
    "QMPKUpdateFreeMatchingState",
    "QMPKQueryBattleStatusResult",
    "UpdateDouHunFightInfoToWatcher",
    "UpdateSubstituePlayerInfo",
    "QMPKQueryZhanDuiResult",
    "QMPKQueryZhanDuiInfoResult",
    "QMPKRequestJoinQmpkZhanDuiSuccess",
    "QMPKUpdateZhanDuiNameAndSlogan",
    "OnMajiuMapiStatusChanged",
    "OnARInited",
    "AROnPlaneAdded",
    "AICameraMoveEnd",
    "OnFreightSubmitEquipSelected",
    "IMShouldShowAlertValueChange",
    "GuildInfoReceived",
    "RequestOperationInGuildSucceed",
    "OnZhuboStationListUpdate",
    "OnZhuboStationInfoUpdate",
    "OnZhuboPortraitReady",
    "OnCCFollowingUpdate",
    "OnRecvMsgFromCC",
    "UpdateTempPlayTimesWithKey",
    "UpdateTempPlayDataWithKey",
    "MainPlayerUpdateMoney",
    "MainPlayerRelationship_AddBlack",
    "OnLoadCityWarLayoutSuccess",
    "UpdateAssistLingShouId",
    "SyncLastLingShouId",
    "UpdateFuTiLingShouId",
    "OnSyncMingWangData",
    "OnPracticeLianShenSkillSuccess",
    "FreightItemInfoGet",
    "OnARPlaneTouched",
    "UpdateObjPosInfo",
    "ChessmanObjectSelected",
    "UpdateNpcInfo",
    "MainPlayerMoveStepped",
    "MainPlayerMoveEnded",
    "UpdateTask",
    "FinishTask",
    "OnQueryPlayerBasicInfoDone",
    "OnQueryPlayerEquipmentInfoDone",
    "OnQueryPlayerIntensifySuitInfoDone",
    "OnQueryPlayerHousePrayWordsDone",
    "OnSendOffLinePlayerHouse",
    "MyGuildInfoReceived",
    "OnGuildForbiddenToSpeakInfoUpdate",
    "UnisdkGetUserTickResult",
    "UpdateLingShouSkillProp",
    "UpdateLingShouSkillSlotLockState",
    "BabyInfoUpdate",
    "BabyInfoDelete",
    "BabyExpressionActionChange",
    "UpdateLingShouOverview",
    "QianKunDaiHeChengSuccess",
    "ShowUIPreDraw",
    "OnWebBrowserClose",
    "AppFocusChange",
    "RenderSceneInit",
    "UnisdkOnExtendFuncCall",
    "OnGetQueryOrderResult",
    "MainPlayerSkillPropUpdate",
    "MainPlayerCastSkillSuccess",
    "OnSendHousePrayResult",
    "ReplyRemainHongBaoSendTimes",
    "ReplySendHongBaoResult",
    "OnQnReturnInfoUpdate",
    "OnSigninInfoUpdate",
    "ItemPropUpdated",
    "OnlineTimeAwardReceived",
    "HolidayOnlineTimeAwardReceived",
    "HolidayOnlineTimeEnd",
    "LoginDaysAwardReceived",
    "QianDaoDaysAwardReceived",
    "LvUpAwardReceived",
    "OnHolidaySigninInfoUpdate",
    "OnCheckInviteFriend",
    "OnCheckBindPhone",
    "OnCheckQnReturn",
    "OnHuiliuAwardGet",
    "OnHuiliuInfoUpdate",
    "OnQueryDoubleExpInfoReturn",
    "OnNewChargeActivity",
    "OnMonthCardAlertStatusChange",
    "JiFenWebInfoResult",
    "AddPlayerAchievement",
    "UpdatePlayerAchievementProgress",
    "ViewLoadingQueueFinish",
    "OnClientNpcSelected",
    "OnVoiceRecordFinish",
    "UploadWebVoiceViaPersonalSpaceFinished",
    "LoadingSceneFinished",
    "CaptureHideNoticeWndSettingChanged",
    "OnHeadInfoOptionChangeBySetting",
    "ShareFinished",
    "OnScreenChange",
    "OnWinScreenChangeAfterResolutionCheck",
    "OnClientObjectSelected",
    "OnGacAllModulesStartUp",
    "CharacterCreationCGStop",
    "OnItemCanOpen",
    "SwitchYingLingStateForChangeSkills",
    "UpdateYingLingState",
    "UpdateQYXTHonorTeams",
    "WeatherIsChanged",
    "OnPlayerRename",
    "OnSoundEnableChanged",
    "OnAmbientSoundEnableChanged",
    "OnSyncHorseRaceRankAndProgress",
    "OnCCPlayerBeginPlay",
    "OnCCPlayerEndPlay",
    "OnCCPlayerVbrInfoUpdated",
    "OnFmodMarkerTrigger",
    "QMPKTaoTaiSaiStatusResult",
    "OnZhaiXingObjectStateChange",
    "OnClickScrollTaskMusicGameHitItem",
    "OnQueryJingLingFinished",
    "GetAllLingShouOverviewWithContext",
    "MailAcceptAwardsDone",
    "OnMailInfoRequestDone",
    "MailDeleteDone",
    "BaptizeWordResult",
    "BaptizeFail",
    "OnLoadFurnitureLayoutSuccess",
    "OnPlacedFurnitureChanged",
    "OnFurnitureMoveChoose",
    "EquipmentSuitUpdate",
    "EquipmentGemGroupUpdate",
    "SelectEquipment",
    "SwitchEquipmentTabIndex",
    "OnRequestKouDaoStatInfoFinished",
    "OnRequestResetStatInfoFinished",
    "OnRequestKouDaoSceneInfoFinished",
    "OnRequestBangHuaStatInfoFinished",
    "OnQueryBangHuaScenePlayerInfoResult",
    "OnCollectHouseUpdate",
    "HouseIdChanged",
    "OnUpdateSingleCommunityData",
    "OnPickerItemSelected",
    "RequestCanUpgradeEquipStoneResult",
    "UpdateItemUseTimes",
    "OnQueryPresentItemReceiverNameReturn",
    "OnQueryDuoHunFanPlayerNameReturn",
    "OnQueryHunPoPlayerNameReturn",
    "RequestChuanjiabaoArtistResult",
    "OnQueryDiqiOwnerName",
    "RequestZhushabiArtistName",
    "OnRequestChuanjiabaoModelArtistNameReturn",
    "QueryDivinationLuckyTaskInfo",
    "OnReplyAdjustedEquipInfo",
    "OnQueryEquipGiverNameReturn",
    "OnSyncMainPlayerAppearanceProp",
    "SendDanMu",
    "UpdateGameVideoProgress",
    "MainPlayerDestroyed",
    "StartLoadScene",
    "OnExistingRoleInfoUpdate",
    "LoginSceneLoaded",
    "StartUnloadScene",
    "ServerNotifyRetryLogin",
    "MsgFrom3DTouch",
    "OnLaunchAppFromDeepLink",
    "QMPKCharacterNameExist",
    "EnterPreLoginMode",
    "PreLoginModeCharacterExists",
    "UpdateCooldown",
    "CookingPlayShakeFinished",
    "SendMallMarketItemLimitUpdate",
    "AutoShangJiaItemUpdate",
    "MarketItemRequestSuccess",
    "SelectPlayer",
    "PlayerLogin",
    "TalismanFxUpdate",
    "UpdateCurrentTask",
    "OnUpdateAcceptableTaskReadStatus",
    "GetAchievementAwardSuccess",
    "OnRideOnOffVehicle",
    "OnQuickBuyItemFromPlayerShopSuccess",
    "PlayerBuyMallItemSuccess",
    "OnQuickBuyItemFromPlayerShopFail",
    "JoyStickDragging",
    "PlayerDragJoyStick",
    "QueryHouseLingqiResult",
    "UpdateChuanjiabaoYuanbaoCost",
    "PlayerMakeZhushabiSuccess",
    "PreSellMarketItemDone",
    "OnQueryPlayerPrayInfoResult",
    "QueryPlayerShopOnShelfNumResult",
    "QueryPlayerShopRecommendPriceResult",
    "PlayerClickGround",
    "QueryPlayerGuildInfoDone",
    "ClientObjDestroy",
    "HpUpdate",
    "OnDarenUpdated",
    "ReloadSettings",
    "SyncSecondaryPasswordInfo",
    "ReplyLiBaoState",
    "UpgradeLifeSkillSuccessed",
    "OnCapsturePartOfWinSucceed",
    "MouseScrollWheel",
    "SyncDynamicActivitySimpleInfo",
    "OnAddFashion",
    "OnTakeOnOffFashion",
    "OnFashionOrVehicleLimitChanged",
    "ChangeFashionStatus",
    "OnMultiColorSetSuccess",
    "WingFxUpdate",
    "OnMultiColorFashionLoaded",
    "OnLoginConnectStatusChanged",
    "MainPlayerAppearanceUpdate",
    "PlayerAppearanceUpdate",
    "OnReceiveNewMailTemplate",
    "OnSwitchSkillSetSuccess",
    "OnHeartBeatDone",
    "OnPackageWarehouseOpen",
    "KeJuQuestionAnswerUpdate",
    "MainPlayerPVPSkillPropUpdate",
    "SwitchYingLingMenPaiRobotState",
    "ReplyDynamicActivityInfo",
    "ClientFurnitureCreate",
    "ClientFurnitureDestroy",
    "MainCameraAngleChanged",
    "HaiZhanCameraDrag",
    "OnTeamCCSettingTypeUpdate",
    "OnApplicationPause",
    "OnUpdateExtraInfo",
    "OnUpdateHouseResource",
    "OnSendHouseData",
    "DieKeUnPossessByOther",
    "NotifyChargeDone",
    "OnTargetProfessionSelected",
    "HouseSetPlayerPuppetNameSucc",
    "HouseUpdatePuppetData",
    "HouseSetCPPuppetDiffSucc",
    "OnFillCargoSuccess",
    "OnRequestHelpSuccess",
    "OnRequestHelperNameSuccess",
    "FreightCargoRequestHelpCd",
    "FreightHelpTimesGet",
    "EquipRemindUpdate",
    "OnQueryHouseMemberAtHome",
    "DieKeDoPossessOther",
    "SpeakingCreate",
    "QueryRoomerPrivalegeInfoResult",
    "LingShouUpdateStatus",
    "ClientObjDie",
    "UpdatePlayerForceInfo",
    "PinchHideUI",
    "GuideEnd",
    "GuideStart",
    "ChangeTarget",
    "RefreshTaskStatus",
    "OnShowBangHuaZhangKuan",
    "OnActivityShownInTaskUpdate",
    "ShishijingTotalDamageUpdate",
    "TopRightWeekendPlayOpen",
    "OnDashenBindStatusChange",
    "PlayerDragJoyStickComplete",
    "OnCommonPlayerListDataUpdate",
    "UpdateHouseCeilingVisibility",
    "OnWarehouseNumUpdate",
    "OnSendExtraTrapInfo",
    "OnChangeHousePaper",
    "AcceptTask",
    "BeginCreatePlayer",
    "TeamInfoChange",
    "SyncXingguanProp",
    "MarriageCertInfoUpdate",
    "OnSyncMarriageAnniversaryInfo",
    "OnJoinCCStream",
    "OnQuitCCStream",
    "CC_RemovePlayerEId",
    "CC_SetPlayerEId",
    "CC_ClearPlayerEId",
    "OnGetCCSpeakingList",
    "OnVoicePlayStart",
    "OnVoicePlayFinish",
    "QueryTradeRecordsResult",
	"OnExpressionActionMgrLoadAppendClipFinished",
    "OnDoublePickerItemSelected",
    "OnCeBaziWaitPartner",
    "OnCeBaziFail",
    "ThumbnailChatExpandChange",
    "PlayerSendHongBao",
    "OnCheckFirstCharge",
    "GetInOffCombatVehicle",
    "GroupIMSetGroupName",
    "GroupIMSetGroupAnnouncement",
    "PlayerLeaveGroupIM",
    "MainPlayerLeaveGroupIM",
    "PlayerJoinGroupIM",
    "GroupIMSyncData",
    "UpdateGuildLeagueInfo",
    "MainPlayerJoinGroupIM",
    "OnShiTuRelationshipChanged",
    "OnBuffInfoUpdate",
    "TeamMemberInfoChange",
    "MainPlayerGetInVehicleAsDriver",
    "MainPlayerGetInVehicleAsPassenger",
    "UpdateIMMsg",
    "EvaluateContactAssistantSign",
    "OnTitleUpdate",
    "OnAddBuffFX",
    "OnRemoveBuffFX",
    "OnAutoAcceptTeamFollowValueChanged",
    "TeamRecruitSearchInfo",
    "TeamRecruitSelfInfo",
    "TeamRecruitSendSucc",
    "RecvChatMsg",
    "MainplayerRelationshipPropUpdate",
    "MainplayerRelationshipBasicInfoUpdate",
    "MainPlayerRelationship_InformOnline",
    "OpenOrCloseWinSocialWnd",
    "UpdateMainPlayerExpressionTxt",
    "OnRightMenuWndExpandedStatusChanged",
    "UpdatePersistPlayDataWithKey",
    "StartLoadSceneFallBack",
    "TrackSymbolStatusChange",
    "OnWithdrawPlayerChatVoiceMsg",
    "OnGetHouseNewsRewardSuccess",
    "AppearancePropertySettingInfoReturn",
    "UnlockMainPlayerProfileFrame",
    "UpdateMainPlayerProfileInfo",
    "UpdatePlayerExpressionAppearance",
    "ReviewTask",
    "KaiShu2023ReturnResult",
    "CutsceneSlowDownStill",
    "CutsceneActorReady",
    "PinchFaceHubWorkListUpdate",
    "PinchFaceHubWorkUpdate",
    "PinchFaceHubWorkPicLoadFinished",
    "PinchFaceHubWorkDataLoadFinished",
    "OnReviewFacialPicBegin",
    "OnReviewFacialPicEnd",
    "ResetWordSuccess",
    "ChouBingSuccess",
    "ResetTalismanWordFailed",
    "BackToLoginScene",
    "SendQueueStatus",
    "OnPinch",
    "QueryOnlineReverseFriendsFinished",
    "ChangeSEARegion",
}

local t = {}
for i,v in ipairs(csharp_event_names) do
    table.insert( t, EnumEventType[v])
    local k = EnumToInt(EnumEventType[v])
    csharp_events[k] = v
end
EventManager.RegisterLuaEvents(t)
t = nil
csharp_event_names = nil

EventManager.s_CallLuaMethod = function(idx,params)
    g_ScriptEvent:BroadcastInLua(csharp_events[idx],params)
end
