local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local UITable = import "UITable"
local UIRoot = import "UIRoot"
local Task_Task = import "L10.Game.Task_Task"
local Screen = import "UnityEngine.Screen"
local NGUIMath = import "NGUIMath"
local Item_Item = import "L10.Game.Item_Item"
local CYiTiaoLongMgr = import "L10.Game.CYiTiaoLongMgr"
local CUITexture=import "L10.UI.CUITexture"
local UIScrollViewIndicator=import "UIScrollViewIndicator"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local UILabelOverflow = import "UILabel+Overflow"


CLuaScheduleInfoWnd = class()
RegistClassMember(CLuaScheduleInfoWnd,"nameLabel")
RegistClassMember(CLuaScheduleInfoWnd,"timeLabel")
RegistClassMember(CLuaScheduleInfoWnd,"formLabel")
RegistClassMember(CLuaScheduleInfoWnd,"levelLabel")
RegistClassMember(CLuaScheduleInfoWnd,"descLabel")
RegistClassMember(CLuaScheduleInfoWnd,"rewardItemPrefab")
RegistClassMember(CLuaScheduleInfoWnd,"icon")
RegistClassMember(CLuaScheduleInfoWnd,"countTf")
RegistClassMember(CLuaScheduleInfoWnd,"countLabel")
RegistClassMember(CLuaScheduleInfoWnd,"huoliTf")
RegistClassMember(CLuaScheduleInfoWnd,"huoliLabel")
RegistClassMember(CLuaScheduleInfoWnd,"okButton")
RegistClassMember(CLuaScheduleInfoWnd,"m_ScheduleInfo")
RegistClassMember(CLuaScheduleInfoWnd,"bgSprite")
RegistClassMember(CLuaScheduleInfoWnd,"contentTf")
RegistClassMember(CLuaScheduleInfoWnd,"scrollContentTf")
RegistClassMember(CLuaScheduleInfoWnd,"scrollView")
RegistClassMember(CLuaScheduleInfoWnd,"scrollViewIndicator")
RegistClassMember(CLuaScheduleInfoWnd,"rewardGrid")

RegistClassMember(CLuaScheduleInfoWnd,"m_Table")
RegistClassMember(CLuaScheduleInfoWnd,"m_TaskTemplate")

CLuaScheduleInfoWnd.s_UseCustomInfo = false

function CLuaScheduleInfoWnd:Awake()
    self.nameLabel = self.transform:Find("Content/Header/ItemNameLabel"):GetComponent(typeof(UILabel))
    self.timeLabel = self.transform:Find("Content/ScrollView/ScrollContent/Statics/TimeLabel"):GetComponent(typeof(UILabel))
    self.formLabel = self.transform:Find("Content/ScrollView/ScrollContent/Statics/FormLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("Content/ScrollView/ScrollContent/Statics/LevelLabel"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("Content/ScrollView/ScrollContent/Table/Desc/DescLabel"):GetComponent(typeof(UILabel))
    self.rewardItemPrefab = self.transform:Find("Content/ItemCell").gameObject
    self.icon = self.transform:Find("Content/Header/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
    self.countTf = self.transform:Find("Content/Header/CountLabel")
    self.countLabel = self.transform:Find("Content/Header/CountLabel"):GetComponent(typeof(UILabel))
    self.huoliTf = self.transform:Find("Content/Header/HuoliLabel")
    self.huoliLabel = self.transform:Find("Content/Header/HuoliLabel"):GetComponent(typeof(UILabel))
    self.okButton = self.transform:Find("Content/ScrollView/ScrollContent/Table/BottomRegion/Button1").gameObject
--self.m_ScheduleInfo = ?
    self.bgSprite = self.transform:Find("Backgournd"):GetComponent(typeof(UISprite))
    self.contentTf = self.transform:Find("Content/ScrollView/ScrollContent")
    self.scrollContentTf = self.transform:Find("Content/ScrollView/ScrollContent")
    self.scrollView = self.transform:Find("Content/ScrollView"):GetComponent(typeof(UIScrollView))
    self.scrollViewIndicator = self.transform:Find("Content/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    self.rewardGrid = self.transform:Find("Content/ScrollView/ScrollContent/Table/Container/Scroll/RewardTf"):GetComponent(typeof(UIGrid))

    self.m_Table=self.transform:Find("Content/ScrollView/ScrollContent/Table"):GetComponent(typeof(UITable))

    self.rewardItemPrefab:SetActive(false)
    UIEventListener.Get(self.okButton).onClick =DelegateFactory.VoidDelegate(function(go)
        self:OnClickButton(go)
    end)
end

function CLuaScheduleInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("ShenYao_QueryRemainTimesResult", self, "ShenYao_QueryRemainTimesResult")
    g_ScriptEvent:AddListener("GetServerOpenTime", self, "OnGetServerOpenTime")
end

function CLuaScheduleInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShenYao_QueryRemainTimesResult", self, "ShenYao_QueryRemainTimesResult")
    g_ScriptEvent:RemoveListener("GetServerOpenTime", self, "OnGetServerOpenTime")
end

function CLuaScheduleInfoWnd:ShenYao_QueryRemainTimesResult(RemainRewardTimes, RewardTimesToday, RemainJoinTimes, JoinTimesToday)
    LuaXinBaiLianDongMgr.m_ShenYaoRewardTimes = RewardTimesToday
    LuaXinBaiLianDongMgr.m_ShenYaoMaxRewardTimes = RewardTimesToday + RemainRewardTimes
    LuaXinBaiLianDongMgr.m_ShenYaoJoinTimes = JoinTimesToday
    LuaXinBaiLianDongMgr.m_ShenYaoMaxJoinTimes = JoinTimesToday + RemainJoinTimes

    self.countTf.gameObject:SetActive(true)
    local label = self.countTf:Find("Label"):GetComponent(typeof(UILabel))
    label.overflowMethod = UILabelOverflow.ResizeFreely
    label.text = LocalString.GetString("奖励")
    LuaUtils.SetLocalPositionX(self.countTf, 53)
    self.countLabel.text = SafeStringFormat3("%d", LuaXinBaiLianDongMgr.m_ShenYaoMaxRewardTimes - LuaXinBaiLianDongMgr.m_ShenYaoRewardTimes)

    self.huoliLabel.gameObject:SetActive(true)
    label = self.huoliTf:Find("Label"):GetComponent(typeof(UILabel))
    label.overflowMethod = UILabelOverflow.ResizeFreely
    label.text = LocalString.GetString("次数")
    LuaUtils.SetLocalPositionX(self.huoliTf, 320)
    self.huoliLabel.text = SafeStringFormat3("%d", LuaXinBaiLianDongMgr.m_ShenYaoMaxJoinTimes - LuaXinBaiLianDongMgr.m_ShenYaoJoinTimes)
end

function CLuaScheduleInfoWnd:Init( )
    local info=nil
    if CLuaScheduleMgr.selectedWeeklyScheduleInfo then
        info=CLuaScheduleMgr.selectedWeeklyScheduleInfo
    else
        info=CLuaScheduleMgr.selectedScheduleInfo
    end
    self.m_ScheduleInfo =info

    local schedule = Task_Schedule.GetData(info.activityId)
    if not schedule then return end
    if schedule.Type == "ShenYao" then
        Gac2Gas.ShenYao_QueryRemainTimes()
    end
    -- self:InitBase(info)
    self.nameLabel.text = schedule.TaskName
    if schedule.Mode == 1 then
        self.formLabel.text = LocalString.GetString("单人任务")
    else
        self.formLabel.text = LocalString.GetString("组队任务")
    end
    --////////////////////////////////////////////////////////////////////////
    self.icon.material = nil
    if CLuaScheduleInfoWnd.s_UseCustomInfo and info.Icon then
        self.icon:LoadMaterial(info.Icon)
    else
        --图标
        if schedule.Reward ~= nil and schedule.Reward.Length > 0 then
            local id = schedule.Reward[0]
            local template = Item_Item.GetData(id)
            if template ~= nil then
                self.icon:LoadMaterial(template.Icon)
            else
                local equipTemplate = EquipmentTemplate_Equip.GetData(id)
                if equipTemplate ~= nil then
                    self.icon:LoadMaterial(equipTemplate.Icon)
                end
            end
        end
    end

    self:InitRewards(schedule.Reward)

    if info.TotalTimes == 0 or schedule.Type == "NewBieSchoolTask" then
        self.countTf.gameObject:SetActive(false)
        self.countLabel.enabled = false
    else
        self.countTf.gameObject:SetActive(true)
        self.countLabel.enabled = true
        self.countLabel.text = SafeStringFormat3( "%d/%d", info.FinishedTimes, info.TotalTimes)
    end

    --活力显示
    if (CLuaScheduleInfoWnd.s_UseCustomInfo and info.DailyTimes == 0) or CLuaScheduleMgr.GetDailyTimes(schedule) == 0 then
        self.huoliTf.gameObject:SetActive(false)
        self.huoliLabel.text = ""
    else
        self.huoliTf.gameObject:SetActive(true)
        local curHuoli = CLuaScheduleMgr.GetHuoli(schedule.HuoLi)
        local huoli = math.min(CLuaScheduleMgr.GetDailyTimes(schedule), info.FinishedTimes) * curHuoli
        if not CLuaScheduleMgr.NeedShowHuoli(schedule) then
            self.huoliLabel.gameObject:SetActive(false)
            self.huoliLabel.text = "-/-"
        else
            self.huoliLabel.gameObject:SetActive(true)
            self.huoliLabel.text = SafeStringFormat3( "%d/%d",  huoli, CLuaScheduleMgr.GetDailyTimes(schedule) * curHuoli)
        end
    end


    local template = Task_Task.GetData(info.taskId)
    self.m_TaskTemplate = template
    -- 任务等级显示
    self:SetTaskLevel()

    local taskDesc = template.TaskDescription
    if template.GamePlay == "YiTiaoLong" then
        taskDesc = CYiTiaoLongMgr.Inst:GetYiTiaoLongTaskDesc(template.ID)
    end
    self.descLabel.text = CUICommonDef.TranslateToNGUIText(taskDesc)


    --timelabel的显示
    if CLuaScheduleInfoWnd.s_UseCustomInfo and info.ActivityTime then
        self.timeLabel.text = info.ActivityTime
    else
        if CLuaScheduleMgr.selectedWeeklyScheduleInfo then
            if schedule.ExternTip and schedule.ExternTip~="" then
                self.timeLabel.text = SafeStringFormat3( LocalString.GetString("%s, %02d:%02d开始"), schedule.ExternTip, info.hour,info.minute )
            else
                self.timeLabel.text = SafeStringFormat3( LocalString.GetString("%02d:%02d开始"), info.hour,info.minute )
            end
        else
            if info.lastingTime<0 then--日常
                self.timeLabel.text = LocalString.GetString("全天")
            else--限时 节日
                local startTime = info.hour * 60 + info.minute
                local endTime = math.min(1439, startTime + info.lastingTime)
                local endHour = (math.floor(endTime / 60))
                local endMinute = endTime % 60
                --计算时间
                local tip = schedule.ExternTip
                if tip and tip~="" then
                    if StringStartWith(tip,"text:") then
                        self.timeLabel.text = string.sub(tip,6)
                    else
                        self.timeLabel.text = SafeStringFormat3( "%s, %02d:%02d-%02d:%02d",schedule.ExternTip,info.hour, info.minute, endHour, endMinute )
                    end
                else
                    self.timeLabel.text = SafeStringFormat3( "%02d:%02d-%02d:%02d",info.hour, info.minute, endHour, endMinute )
                end
            end
        end
    end

    --参加按钮的显示
    if CLuaScheduleInfoWnd.s_UseCustomInfo and info.ShowJoinBtn ~= nil then
        self.okButton.transform.parent.gameObject:SetActive(info.ShowJoinBtn)
    else
        if CLuaScheduleMgr.selectedWeeklyScheduleInfo then
            self.okButton.transform.parent.gameObject:SetActive(false)
        else
            if CLuaScheduleMgr.CanShowButton(info) then
                self.okButton.transform.parent.gameObject:SetActive(true)
            else
                self.okButton.transform.parent.gameObject:SetActive(false)
            end
        end
    end

    self:ResizeUI()
end

function CLuaScheduleInfoWnd:OnClickButton( go) 
    CLuaScheduleMgr.TrackSchedule(self.m_ScheduleInfo)
    CUIManager.CloseUI(CLuaUIResources.SchedulePicWnd)
    CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
    CUIManager.CloseUI(CLuaUIResources.ScheduleInfoWnd)
end

function CLuaScheduleInfoWnd:ResizeUI( )
    self.descLabel:UpdateNGUIText()
    self.m_Table:Reposition()
    self.bgSprite.height = 1
    local height = math.floor(NGUIMath.CalculateRelativeWidgetBounds(self.scrollContentTf).size.y)
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = math.floor((Screen.height * scale))

    self.bgSprite.height = math.min(height + 176 + 10 + 20, virtualScreenHeight)

    self.bgSprite.transform.localPosition = Vector3(0, math.floor(self.bgSprite.height / 2))
    self.scrollView.panel:ResetAndUpdateAnchors()
    self.scrollViewIndicator:Layout()
end

function CLuaScheduleInfoWnd:InitRewards( Reward) 
    for i=1,Reward.Length do
        local item=Reward[i-1]
        if item > 0 then
            local itemData = Item_Item.GetData(item)
            if itemData ~= nil and itemData.Status ~= 3 then
                --实例化
                local go =NGUITools.AddChild(self.rewardGrid.gameObject,self.rewardItemPrefab)
                go:SetActive(true)
                self:InitRewardItem(go.transform,item)
            end
        end
    end
    self.rewardGrid:Reposition()
end

function CLuaScheduleInfoWnd:InitRewardItem(transform,id)
    local icon=transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local itemData=Item_Item.GetData(id)
    if itemData then
        icon:LoadMaterial(itemData.Icon)
    else
        local equipData=EquipmentTemplate_Equip.GetData(id)
        if equipData then
            icon:LoadMaterial(equipData.Icon)
        end
    end
    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(id,false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end

function CLuaScheduleInfoWnd:SetTaskLevel()
    if not self.m_TaskTemplate then return end
    self.levelLabel.text = SafeStringFormat3( LocalString.GetString("%d级"),CLuaScheduleMgr:GetTaskCheckLevel(self.m_TaskTemplate) )
end

function CLuaScheduleInfoWnd:OnGetServerOpenTime( day, context )
    self:SetTaskLevel()
end
function CLuaScheduleInfoWnd:OnDestroy()
    CLuaScheduleMgr.selectedWeeklyScheduleInfo=nil
    CLuaScheduleMgr.selectedScheduleInfo=nil
    CLuaScheduleInfoWnd.s_UseCustomInfo = false
end
