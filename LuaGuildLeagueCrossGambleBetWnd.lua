local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"

LuaGuildLeagueCrossGambleBetWnd = class()
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_ServerNameLabel", "ServerNameLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_OddsLabel", "OddsLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_DetailButton", "DetailButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_IncreaseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_MoneyCtrl", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_LimitLabel", "LimitLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_ConfirmButton", "ConfirmButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_BetScoreCostLabel", "BetScoreCostLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_BetScoreOwnLabel", "BetScoreOwnLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleBetWnd, "m_TipLabel", "TipLabel", UILabel)

function LuaGuildLeagueCrossGambleBetWnd:Awake()
    UIEventListener.Get(self.m_DetailButton).onClick = DelegateFactory.VoidDelegate(function() self:OnDetailButtonClick() end)
    UIEventListener.Get(self.m_ConfirmButton).onClick = DelegateFactory.VoidDelegate(function() self:OnConfirmButtonClick() end)
    self.m_TipLabel.text = g_MessageMgr:FormatMessage("GLC_Gamble_Bet_Description", GuildLeagueCross_Setting.GetData().DaiBiToSilver)
end

function LuaGuildLeagueCrossGambleBetWnd:Init()
    local info = LuaGuildLeagueCrossMgr.m_GambleBetInfo
    local isRankGamble = LuaGuildLeagueCrossMgr:IsRankGamble(info.gambleType)
    self.m_DetailButton:SetActive(not isRankGamble)
    self.m_ServerNameLabel.text = isRankGamble and LuaGuildLeagueCrossMgr:GetRankGambleChoiceName(info.serverId) or info.serverName
    self.m_OddsLabel.text = SafeStringFormat3(LocalString.GetString("赔率%.2f倍"), info.odds-info.odds%0.01)

    local price = GuildLeagueCross_Setting.GetData().PerbetSilverNum
    local min = 1
    local max = LuaGuildLeagueCrossMgr:GetRemainingPortionForGamble(info.gambleType, LuaGuildLeagueCrossMgr.m_GambleInfo) / price
    
    self.m_IncreaseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
        self:OnNumChanged(value)
    end)
    self.m_IncreaseAndDecreaseButton:SetMinMax(1,max,1)
    self.m_IncreaseAndDecreaseButton:SetValue(1,true)
    self.m_LimitLabel.text = SafeStringFormat3(LocalString.GetString("该场竞猜剩余可投入%d份"), max)
    self:UpdateBetScore()
end

function LuaGuildLeagueCrossGambleBetWnd:OnNumChanged(value)
    local val = self.m_IncreaseAndDecreaseButton:GetValue()
    local daibiNum = self:GetScoreNum()
    self.m_MoneyCtrl:SetCost(GuildLeagueCross_Setting.GetData().PerbetSilverNum * val - math.min(val, daibiNum) * GuildLeagueCross_Setting.GetData().DaiBiToSilver)
    self:UpdateBetScore()
end

function LuaGuildLeagueCrossGambleBetWnd:OnDetailButtonClick()
    local info = LuaGuildLeagueCrossMgr.m_GambleBetInfo
    LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(info.serverId, info.serverName, EnumQueryGLCServerGuildInfoContext.eGamble)
end

function LuaGuildLeagueCrossGambleBetWnd:OnConfirmButtonClick()
    local info = LuaGuildLeagueCrossMgr.m_GambleBetInfo
    --复用serverid
    LuaGuildLeagueCrossMgr:RequestBetGuildLeagueCrossGamble(info.gambleType, info.serverId, self.m_IncreaseAndDecreaseButton:GetValue())
    CUIManager.CloseUI("GuildLeagueCrossGambleBetWnd")
    -- 服务器还缺乏下注后刷新的机制, 暂时采用主动请求的方式来刷新, 这种客户端的方式存在风险，就是上一条requestbet的rpc可能没执行完全就开始处理下面这条请求，返回的结果就不是最新的
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossGambleInfo()
end

function LuaGuildLeagueCrossGambleBetWnd:GetScoreNum()
    local player = CClientMainPlayer.Inst
    if player then
        return tonumber(player.PlayProp:GetTempPlayStringData(EnumTempPlayDataKey_lua.eGuildLeagueCrossGambleDaiBi)) or 0
    else
        return 0
    end
end

function LuaGuildLeagueCrossGambleBetWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
end

function LuaGuildLeagueCrossGambleBetWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
end

function LuaGuildLeagueCrossGambleBetWnd:OnUpdateTempPlayDataWithKey(args)
    self:UpdateBetScore()
end

function LuaGuildLeagueCrossGambleBetWnd:UpdateBetScore()
    local maxNum = self.m_IncreaseAndDecreaseButton:GetValue()
    local daibiNum = self:GetScoreNum()
    self.m_BetScoreCostLabel.text = tostring(math.min(maxNum, daibiNum))
    self.m_BetScoreOwnLabel.text = tostring(daibiNum)
end
