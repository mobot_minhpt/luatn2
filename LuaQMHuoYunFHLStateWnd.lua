local UISprite = import "UISprite"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local DelegateFactory = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local CUIManager = import "L10.UI.CUIManager"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local Ease = import "DG.Tweening.Ease"

CLuaQMHuoYunFHLStateWnd = class()

RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_ExpandBtn")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_ZhiYeIcon")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_TypeLabel")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_Slider")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_TransSlider")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_SliderLabel")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_SliderFx")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_CanFillLabel")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_StartValue")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_EndValue")
RegistClassMember(CLuaQMHuoYunFHLStateWnd, "m_SliderTweener")

function CLuaQMHuoYunFHLStateWnd:Awake()
    self.m_ExpandBtn = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    UIEventListener.Get(self.m_ExpandBtn).onClick = DelegateFactory.VoidDelegate( function(p) 
        self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    self.m_StartValue = 0
    self.m_Duration = -1
    self.m_EndValue = 0

    self:InitComponents()
end

function CLuaQMHuoYunFHLStateWnd:InitComponents()
    self.m_ZhiYeIcon = self.transform:Find("Anchor/Top/Line1/ZhiYeIcon"):GetComponent(typeof(UISprite))
    self.m_TypeLabel = self.transform:Find("Anchor/Top/Line1/Label2"):GetComponent(typeof(UILabel))
    self.m_Slider    = self.transform:Find("Anchor/Top/Line2/Slider"):GetComponent(typeof(UISlider))
    self.m_TransSlider  = self.transform:Find("Anchor/Top/Line2/TransSlider"):GetComponent(typeof(UISlider))
    self.m_SliderLabel  = self.transform:Find("Anchor/Top/Line2/Slider/Label"):GetComponent(typeof(UILabel))
    self.m_SliderFx     = self.transform:Find("Anchor/Top/Line2/Slider/Foreground/FxOffset/Fx"):GetComponent(typeof(CUIFx))
    self.m_CanFillLabel = self.transform:Find("Anchor/Top/Line3/Label"):GetComponent(typeof(UILabel))
end

function CLuaQMHuoYunFHLStateWnd:Init()
    local classId = EnumToInt(CClientMainPlayer.Inst.Class)
    self.m_ZhiYeIcon.spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
    self.m_TypeLabel.text = "(      "..WuYi2020_FengHuoLingClass.GetData(classId).TypeNameDescription ..")"
    self.m_CanFillLabel.text = g_MessageMgr:FormatMessage("WuYi2020_QuanMinHuoYun_FHL_EnergyCanFill", 0)

    self.m_Slider.value = 0
    self.m_SliderLabel.text = ""

    if CLuaQMHuoYunMgr.lastFHLState and #CLuaQMHuoYunMgr.lastFHLState ~= 0 then
        self:UpdateState(CLuaQMHuoYunMgr.lastFHLState[1], CLuaQMHuoYunMgr.lastFHLState[2], CLuaQMHuoYunMgr.lastFHLState[3], CLuaQMHuoYunMgr.lastFHLState[4])
    end
end

function CLuaQMHuoYunFHLStateWnd:UpdateState(class, value, maxvalue, canFillNum)
    self.m_ZhiYeIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class))
    self.m_TypeLabel.text = "(      "..WuYi2020_FengHuoLingClass.GetData(class).TypeNameDescription ..")"
    self.m_SliderLabel.text = math.floor(value) .. "/" .. maxvalue
    self.m_CanFillLabel.text = g_MessageMgr:FormatMessage("WuYi2020_QuanMinHuoYun_FHL_EnergyCanFill", canFillNum)--"当前能量可以灌注碎片{0}个"

    self.m_StartValue = self.m_Slider.value
    self.m_EndValue = value / maxvalue

    if self.m_SliderTweener then
        LuaTweenUtils.Kill(self.m_SliderTweener,false)
        self.m_SliderFx:DestroyFx()
        self.m_SliderTweener = nil
    end

    if self.m_EndValue > self.m_StartValue then
        self.m_SliderFx:LoadFx("fx/ui/prefab/UI_xuetiao.prefab")
        local duration = (self.m_EndValue - self.m_StartValue + 1) * (6.18/3)
        self.m_SliderTweener = LuaTweenUtils.TweenFloat(self.m_StartValue, self.m_EndValue, duration, function ( val )
            self.m_Slider.value = val
        end)
        LuaTweenUtils.SetEase(self.m_SliderTweener, Ease.OutCubic)
    else
        self.m_TransSlider.value = self.m_Slider.value
        self.m_Slider.value = value/maxvalue
        self.m_TransSlider.gameObject:SetActive(true)
        RegisterTickOnce(function ()
            self.m_TransSlider.gameObject:SetActive(false)
        end, 1000)
    end
end

function CLuaQMHuoYunFHLStateWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncFengHuoLingPlayValue", self, "UpdateState")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function CLuaQMHuoYunFHLStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncFengHuoLingPlayValue", self, "UpdateState")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIManager.TopAndRightTipWnd)
    end

    CLuaQMHuoYunMgr.reliveWndId = nil
end

function CLuaQMHuoYunFHLStateWnd:OnDestroy()
    if self.m_SliderTweener then
        LuaTweenUtils.Kill(self.m_SliderTweener,false)
    end

    CLuaQMHuoYunMgr.lastFHLState = nil
end

function CLuaQMHuoYunFHLStateWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end