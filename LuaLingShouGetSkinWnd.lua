local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Constants = import "L10.Game.Constants"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local QnTableView=import "L10.UI.QnTableView"
local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"


CLuaLingShouGetSkinWnd = class()
RegistClassMember(CLuaLingShouGetSkinWnd,"gridView")
RegistClassMember(CLuaLingShouGetSkinWnd,"textureLoader")
RegistClassMember(CLuaLingShouGetSkinWnd,"closeBtn")
RegistClassMember(CLuaLingShouGetSkinWnd,"getSkinBtn")
RegistClassMember(CLuaLingShouGetSkinWnd,"descLabel")

function CLuaLingShouGetSkinWnd:Awake()
    self.gridView = self.transform:Find("SortTable"):GetComponent(typeof(QnTableView))
    self.textureLoader = self.transform:Find("GameObject/Texture"):GetComponent(typeof(CLingShouModelTextureLoader))
    self.getSkinBtn = self.transform:Find("GameObject/GetSkinBtn").gameObject
    self.descLabel = self.transform:Find("GameObject/DescLabel"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.getSkinBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:GetSkin(go) end)

end

function CLuaLingShouGetSkinWnd:Init()
    local getNumFunc=function() 
        local list = CLingShouMgr.Inst:GetLingShouOverviewList()
        return list.Count
    end
    local initItemFunc=function(item,index) 
        local list = CLingShouMgr.Inst:GetLingShouOverviewList()
        item:Init(list[index])
    end
    self.gridView.m_DataSource = DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.gridView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:UpdateLingShou( row ) 
    end)

    CLingShouMgr.Inst:RequestLingShouList()

end

function CLuaLingShouGetSkinWnd:GetSkin( go) 
    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou

    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil

    if not selectedLingShou or selectedLingShou=="" then
        g_MessageMgr:ShowMessage("LINGSHOU_DONT_HAVE")
    elseif CLingShouMgr.Inst:IsOnBattle(selectedLingShou) then
        g_MessageMgr:ShowMessage("LINGSHOU_CHUZHAN")
    elseif bindPartenerLingShouId == selectedLingShou then
        g_MessageMgr:ShowMessage("LingShou_JieBan_HuaRong_Forbid")
    else
        --判断一下是否是珍贵灵兽
        local details = CLingShouMgr.Inst:GetLingShouDetails(selectedLingShou)
        if details == nil then
            g_MessageMgr:ShowMessage("LINGSHOU_DONT_HAVE")
        else
            --不是再刘麒麟处打开
            if not CLingShouMgr.Inst.fromNPC then
                if CLuaLingShouMgr.IsSpecial(details.data) or CLuaLingShouMgr.IsShenShou(details.data) then
                    g_MessageMgr:ShowMessage("LINGSHOU_LIANHUA_FORBIDEN")
                else
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LINGSHOU_HUARONG_CONFIRM"), 
                        DelegateFactory.Action(function() self:Request() end), nil, nil, nil, false)
                end
            else
                --LINGSHOU_HUARONG_VALUABLE_LINGSHOU
                if CLuaLingShouMgr.IsSpecial(details.data) or CLuaLingShouMgr.IsShenShou(details.data) then
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LINGSHOU_HUARONG_VALUABLE_LINGSHOU"), 
                        DelegateFactory.Action(function() self:Request() end), nil, nil, nil, false)
                else
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LINGSHOU_HUARONG_CONFIRM"), 
                        DelegateFactory.Action(function() self:Request() end), nil, nil, nil, false)
                end
            end
        end
    end
end
function CLuaLingShouGetSkinWnd:Request( )
    local lingshouId = CLingShouMgr.Inst.selectedLingShou
    local futiId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.FuTiLingShouId or nil
    if futiId == lingshouId then
        g_MessageMgr:ShowMessage("HuaLing_LingShou_CanNot_Do_This")
        return
    end

    if CLuaLingShouMgr.m_GetSkin_SelectedPos >= 0 then
        local find = CItemMgr.Inst:GetPackageItemAtPos(CLuaLingShouMgr.m_GetSkin_SelectedPos)
        if find ~= nil and find.TemplateId == Constants.HuaRongDanID then
            Gac2Gas.GetLingShouPixiang(lingshouId, EnumItemPlace_lua.Bag, CLuaLingShouMgr.m_GetSkin_SelectedPos, find.Id)
        else
            local pos
            local itemId
            local default
            default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, Constants.HuaRongDanID)
            if default then
                Gac2Gas.GetLingShouPixiang(lingshouId, EnumItemPlace_lua.Bag, pos, itemId)
            else
                g_MessageMgr:ShowMessage("PLAYER_NOT_HAVE_HRD")
            end
        end
    end
end

function CLuaLingShouGetSkinWnd:UpdateLingShou( row) 
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    if row < list.Count then
        CLingShouMgr.Inst.selectedLingShou = list[row].id

        local lingShouId = list[row].id

        CLingShouMgr.Inst:RequestLingShouDetails(lingShouId)
    else
        CLingShouMgr.Inst.selectedLingShou = ""
    end
end
function CLuaLingShouGetSkinWnd:OnEnable( )
    g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "GetLingShouDetails")
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
end
function CLuaLingShouGetSkinWnd:SendItem( args) 
	local id = args[0]
    if id == CLingShouMgr.Inst.selectedLingShou then
        CLingShouMgr.Inst:ClearLingShouData()
        CLingShouMgr.Inst:RequestLingShouList()

        self.textureLoader:InitNull()
        self.descLabel.text = ""
    end
end
function CLuaLingShouGetSkinWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "GetLingShouDetails")
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
end
function CLuaLingShouGetSkinWnd:OnDestroy( )
    CLingShouMgr.Inst:ClearLingShouData()
    CLingShouMgr.Inst.fromNPC = false
    CLingShouMgr.Inst.isExchangeFurniture = false
    CLuaLingShouMgr.m_GetSkin_SelectedPos = 0
end
function CLuaLingShouGetSkinWnd:GetAllLingShouOverview( )
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    --初始化列表
    self.gridView:ReloadData(false, false)
    self.gridView:SetSelectRow(CLingShouMgr.Inst:GetBattleIndex(), true)
end
function CLuaLingShouGetSkinWnd:GetLingShouDetails( args ) 
    local lingShouId=args[0]
    local details=args[1]
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        if details ~= nil then
            self.textureLoader:Init(details)
            local name = details.evolveGradeDesc .. CLingShouBaseMgr.GetLingShouName(details.data.TemplateId)
            if details.isMorphed then
                local templateId = details.data.Props.TemplateIdForSkin
                local evolveGrade = details.data.Props.EvolveGradeForSkin

                local keys = {}
                QuanMinPK_Stage.ForeachKey(function(k)
                    table.insert( keys,k )
                end)
                local desc=""

                local key = 0
                for i,v in ipairs(keys) do
                    local designData=QuanMinPK_Stage.GetData(v)
                    for tp, color, weaponColor, lingshouId, grade in string.gmatch(designData.Color2PiXiang, "(%d+),(%d+),(%d+),(%d+),(%d+);?") do
                        lingshouId=tonumber(lingshouId)
                        if lingshouId==templateId then
                            key=v
                            break
                        end
                    end
                end
    
                if key==2 then
                    desc=LocalString.GetString("本初")
                elseif key==3 then
                    desc=LocalString.GetString("一变")
                elseif key==4 then
                    desc=LocalString.GetString("二变")
                elseif key==5 then
                    desc=LocalString.GetString("三变")
                end
                name = desc .. CLingShouMgr.GetLingShouName(templateId)
            end
            self.descLabel.text = g_MessageMgr:FormatMessage("LINGSHOU_YIRONG_TIP", name)
        else
            self.textureLoader:InitNull()
            self.descLabel.text = ""
        end
    end
end

return CLuaLingShouGetSkinWnd
