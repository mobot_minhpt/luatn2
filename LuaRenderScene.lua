local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CGas2Gac = import "L10.Game.Gas2Gac"
local Shader = import "UnityEngine.Shader"
local Lua = import "L10.Engine.Lua"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local Vector3 = import "UnityEngine.Vector3"
local NGUIText = import "NGUIText"
local CIceMgr = import "L10.Engine.Scene.CIceMgr"

--[[CGas2Gac.m_NeedDynamicSkyboxUpdate = function (gas2gac)
    Lua.s_IsHook = false
    CGas2Gac.NeedDynamicSkyboxUpdate = true
    local inst = CClientHouseMgr.Inst 
    if inst:IsPlayerInYard() and inst.mCurExtraInfo then
        if inst.mCurExtraInfo.SkyBoxKind then
            CLuaHouseMgr.ChangeSkyBox(inst.mCurExtraInfo.SkyBoxKind)
            if inst.mCurExtraInfo.SkyBoxKind ~= 0 and inst.mCurExtraInfo.SkyBoxKind ~= 6 then
                -- 如果已经设置了天空盒，则不进行黄昏天空盒的处理（添加对应的fx)
                CGas2Gac.NeedDynamicSkyboxUpdate = false
            end
        end
    end
end--]]

CGas2Gac.m_hookOnDynamicSkyboxUpdate = function (gac2gas)
    local inst = CClientHouseMgr.Inst

    local needSunSetBox = true

    if inst:IsPlayerInYard() and inst.mCurExtraInfo then
        if inst.mCurExtraInfo.SkyBoxKind then
            CLuaHouseMgr.ChangeSkyBox(inst.mCurExtraInfo.SkyBoxKind)
            if inst.mCurExtraInfo.SkyBoxKind ~= 0 and inst.mCurExtraInfo.SkyBoxKind ~= 6 then
                -- 如果已经设置了天空盒，则不进行黄昏天空盒的处理（添加对应的fx)
                needSunSetBox = false
            end
        end
    end

    if needSunSetBox and inst:IsPlayerInHouse() and inst:IsPlayerInYard() then
        if CClientMainPlayer.Inst then
            local fx = CEffectMgr.Inst:AddObjectFX(CGas2Gac.DynamicSkyboxFxId,CClientMainPlayer.Inst.RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
            CClientMainPlayer.Inst.RO:AddFX(CGas2Gac.DynamicSkyboxFxId, fx, -1)
        end
    else
        if CClientMainPlayer.Inst then
            CClientMainPlayer.Inst.RO:RemoveFX(CGas2Gac.DynamicSkyboxFxId)
        end
    end
end

LuaRenderSceneMgr = {}
--@region 当铺场景处理

EDangPuLightStage = {
    eGuiTai = 1,
    eChuang = 2,
    eMianJu = 3,
    eAll = 4,
}

EDangPuLightStageName = {
    "_guitai",
    "_chuang",
    "_mianju",
    "",
}

local Main = import "L10.Engine.Main"
function LuaRenderSceneMgr.ChangeDangPuLightPu(stage)
    if Main.Inst and CRenderScene.Inst then
        CRenderScene.Inst:ClearDayAndNightParams(true)
        local partName = EDangPuLightStageName[stage]
        if partName then
            Main.Inst:StartCoroutine(CRenderScene.Inst:SetDayAndNightParams(false, 0, partName))
        end

        if stage == EDangPuLightStage.eGuiTai then
            LuaRenderSceneMgr.ShowAndHide(CRenderScene.Inst.m_FX, false)
            LuaRenderSceneMgr.ShowAndHideByName("Models/guitai_zifaguang", true)
            Shader.SetGlobalFloat("_WeatherDarkness", 0.15)
        elseif stage == EDangPuLightStage.eChuang then
            LuaRenderSceneMgr.ShowAndHide(CRenderScene.Inst.m_FX, false)
            LuaRenderSceneMgr.ShowAndHideByName("Models/Models_NOClip/jz_dangpu_008_001", true)
            Shader.SetGlobalFloat("_WeatherDarkness", 0.3)
        elseif stage == EDangPuLightStage.eMianJu then
            LuaRenderSceneMgr.ShowAndHide(CRenderScene.Inst.m_FX, false)
            LuaRenderSceneMgr.ShowAndHideByName("Models/Models_NOClip/jz_dangpu_008_001", true)
            LuaRenderSceneMgr.ShowAndHideByName("OtherObjects/FX/dangpu_zhuguang_jitai", true)
            LuaRenderSceneMgr.ShowAndHideByName("OtherObjects/FX/dangpu_tianchuanguang", true)
            LuaRenderSceneMgr.ShowAndHideByName("OtherObjects/FX/dangpu_luziyan (1)", true)
            LuaRenderSceneMgr.ShowAndHideByName("OtherObjects/FX/dangpu_luziyan", true)
            Shader.SetGlobalFloat("_WeatherDarkness", 0.3)
        elseif stage == EDangPuLightStage.eAll then
            LuaRenderSceneMgr.ShowAndHide(CRenderScene.Inst.m_FX, true)
            Shader.SetGlobalFloat("_WeatherDarkness", 1)
        end
    end
    
end


-- 当铺场景全黑的效果
function LuaRenderSceneMgr.DangPuDark()
    -- 隐藏所有特效
    LuaRenderSceneMgr.ShowAndHide(CRenderScene.Inst.m_FX, false)
    LuaRenderSceneMgr.ShowAndHideByName("Models/guitai_zifaguang", false)
    LuaRenderSceneMgr.ShowAndHideByName("Models/Models_NOClip/jz_dangpu_008_001", false)
    Shader.SetGlobalFloat("_WeatherDarkness", 0)
end

function LuaRenderSceneMgr.ShowAndHideByName(rootName, show)
    if CRenderScene.Inst then
        local root = CRenderScene.Inst.transform:Find(rootName)
        LuaRenderSceneMgr.ShowAndHide(root, show)
    end
end


function LuaRenderSceneMgr.ShowAndHide(root, show)
    if not root then return end

    for i = 0, root.transform.childCount-1 do
        local child = root.transform:GetChild(i)
        child.gameObject:SetActive(show)
    end
end

function Gas2Gac.ChangeDangPuLightStage(stage)
    -- 选判断是不是在dangpu场景中
    if CRenderScene.Inst then
        if StringStartWith(CRenderScene.Inst.m_NowSceneName, "dangpu") then
            if stage == 0 then
                LuaRenderSceneMgr.DangPuDark()
            else
                LuaRenderSceneMgr.ChangeDangPuLightPu(stage)
            end
        end
    end
end

--@endregion


--@region 金城池场景处理

function Gas2Gac.SetJinChengChiFrozen()
    CIceMgr.Inst:ShowIce()
end

function Gas2Gac.SetJinChengChiIceMelt(gridPosX, gridPosY)
    CIceMgr.Inst:IceMelt(gridPosX, gridPosY)
end

--@endregion

--@region 雾效

--@fogStart: 雾效开始距离
--@fogClarity: 雾效浓度
--@fogClarityMaximum: 雾效最大值
--@heightFogColor:  高度雾颜色
--@fogColor: 雾效颜色
function LuaRenderSceneMgr.UpdateFogInfo(fogStart, fogClarity, fogClarityMaximum, heightFogColor, fogColor)
    local _heightFogColor = NGUIText.ParseColor24(heightFogColor, 0)
    local _fogColor =  NGUIText.ParseColor24(fogColor, 0)
    local fogModel = CRenderScene.Inst.m_RenderData.m_FogModel
    fogModel:CopyFrom(fogModel.m_EnableFog, fogModel.m_EnableFogDarken, fogModel.m_HeightFogStart, fogModel.m_HeightFogEnd, fogModel.m_HeightFogColorFalloff, 
                        fogModel.m_HeightFogColorOffset, fogStart, fogClarity, fogModel.m_HeightMinimum, fogModel.m_FogFalloff, fogClarityMaximum, _heightFogColor, _fogColor, fogModel.m_SunScatterIntensity, fogModel.m_SunScatterColor)

    CRenderScene.Inst.mAppliedFogStartDist = fogStart
    CRenderScene.Inst.mAppliedFogColor = _fogColor

    CRenderScene.Inst.m_RenderData:SetFog()
end

function Gas2Gac.UpdateSceneFogSetting(fogStart, fogClarity, fogClarityMaximum, heightFogColor, fogColor)
    LuaRenderSceneMgr.UpdateFogInfo(fogStart, fogClarity, fogClarityMaximum, heightFogColor, fogColor)
end

function LuaRenderSceneMgr.UpdateFogInfoComplete(enableFog, enableFogDarken, heightFogStart, heightFogEnd, heightFogColorFalloff, heightFogColorOffset, fogStart, fogClarity, heightMinimum, fogFalloff, fogClarityMaximum, heightFogColor, fogColor)
    local _heightFogColor = NGUIText.ParseColor24(heightFogColor, 0)
    local _fogColor =  NGUIText.ParseColor24(fogColor, 0)
    local fogModel = CRenderScene.Inst.m_RenderData.m_FogModel
    fogModel:CopyFrom(enableFog, enableFogDarken, heightFogStart, heightFogEnd, heightFogColorFalloff, heightFogColorOffset, fogStart, fogClarity, 
                        heightMinimum, fogFalloff, fogClarityMaximum, _heightFogColor, _fogColor, fogModel.m_SunScatterIntensity, fogModel.m_SunScatterColor)

    CRenderScene.Inst.mAppliedFogStartDist = fogStart
    CRenderScene.Inst.mAppliedFogColor = _fogColor

    CRenderScene.Inst.m_RenderData:SetFog()
end

function Gas2Gac.UpdateSceneFogSettingComplete(enableFog, enableFogDarken, heightFogStart, heightFogEnd, heightFogColorFalloff, heightFogColorOffset, fogStart, fogClarity, heightMinimum, fogFalloff, fogClarityMaximum, heightFogColor, fogColor)
    LuaRenderSceneMgr.UpdateFogInfoComplete(enableFog, enableFogDarken, heightFogStart, heightFogEnd, heightFogColorFalloff, heightFogColorOffset, fogStart, fogClarity, heightMinimum, fogFalloff, fogClarityMaximum, heightFogColor, fogColor)
end

--@endregion

--@region ScenePostEffect

local CPostEffect = import "L10.Engine.CPostEffect"
local EventManager = import "EventManager"
local EnumEventType		= import "EnumEventType"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local EnumHeatWaveMode = import "L10.Engine.PostProcessing.EnumHeatWaveMode"

function LuaRenderSceneMgr.ShowScenePostEffect(effectType, isStart)
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        if effectType == 1 then -- 水波纹
            postEffectCtrl:EnableWaterWaveMat(false)
        elseif effectType == 2 then -- 前世镜
            postEffectCtrl:DisableWaterWaveMat(true)
            EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.JinZiFx})
        elseif effectType == 3 then -- 空气扭曲效果
            postEffectCtrl.Instance:SetHeatWaveMode(EnumHeatWaveMode.HeatwaveEffect ,isStart)
        end
    else
        if effectType == 1 then -- 水波纹
            CPostEffect.EnableWaterWaveMat(false)
        elseif effectType == 2 then -- 前世镜
            CPostEffect.DisableWaterWaveMat(true)
            EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.JinZiFx})
        elseif effectType == 3 then -- 空气扭曲效果
            CPostEffectMgr.Instance.HeatwaveEffectEnable = isStart
        end
    end
end

function Gas2Gac.ShowScenePostEffect(effectType, isStart)
    LuaRenderSceneMgr.ShowScenePostEffect(effectType, isStart)
end
LuaRenderSceneMgr.m_SetFogDensityTweener = nil
function LuaRenderSceneMgr.SetVolumeCloudFogDensity(fogDensity, time)
    local param = (CRenderScene.Inst or {}).m_VolumeCloudProfile
    if param == nil then
        return
    end
    if LuaRenderSceneMgr.m_SetFogDensityTweener then
        LuaTweenUtils.Kill(LuaRenderSceneMgr.m_SetFogDensityTweener, false)
        LuaRenderSceneMgr.m_SetFogDensityTweener = nil
    end
    LuaRenderSceneMgr.m_SetFogDensityTweener = LuaTweenUtils.TweenFloat(param.fogDensity, fogDensity, time, function(val)
        local profile = (CRenderScene.Inst or {}).m_VolumeCloudProfile
        if profile == nil then
            LuaTweenUtils.Kill(LuaRenderSceneMgr.m_SetFogDensityTweener, false)
            LuaRenderSceneMgr.m_SetFogDensityTweener = nil
            return 
        end
        profile.fogDensity = val
        if CPostProcessingMgr.s_NewPostProcessingOpen then
            CPostProcessingMgr.Instance.MainController:SetVolumeCloudParmeter(profile)
        end
    end)
end
--@endregion

