local SoundManager=import "SoundManager"
local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local Ease            = import "DG.Tweening.Ease"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local UILabel = import "UILabel"
local CIMMgr=import "L10.Game.CIMMgr"
local CJingLingMgr=import "L10.Game.CJingLingMgr"
local CGroupIMMgr=import "L10.Game.CGroupIMMgr"

local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local GameObject = import "UnityEngine.GameObject"
local EChatPanel=import "L10.Game.EChatPanel"
local CSocialWndMgr=import "L10.UI.CSocialWndMgr"
local CChatLinkMgr = import "CChatLinkMgr"

local DelegateFactory  = import "DelegateFactory"

LuaShengXiaoCardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengXiaoCardWnd, "ShengXiaoInfo", "ShengXiaoInfo", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "LeaveButton", "LeaveButton", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "TuoGuanButton", "TuoGuanButton", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "ChatButton", "ChatButton", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "CardTemplate", "CardTemplate", GameObject)
-- RegistChildComponent(LuaShengXiaoCardWnd, "SealCardTemplate", "SealCardTemplate", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "Guess", "Guess", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "GuessInfoLabel", "GuessInfoLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardWnd, "GuessCard", "GuessCard", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "StatusLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardWnd, "CancleTuoGuanButton", GameObject)
RegistChildComponent(LuaShengXiaoCardWnd, "VoiceButton", GameObject)
-- RegistChildComponent(LuaShengXiaoCardWnd, "Fx", CUIFx)
RegistChildComponent(LuaShengXiaoCardWnd, "InteractFx1", CUIFx)
RegistChildComponent(LuaShengXiaoCardWnd, "InteractFx2", CUIFx)
RegistChildComponent(LuaShengXiaoCardWnd, "InteractFx3", CUIFx)
RegistChildComponent(LuaShengXiaoCardWnd, "OverlayFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaShengXiaoCardWnd,"m_PlayerViews")
RegistClassMember(LuaShengXiaoCardWnd,"m_Button1")
RegistClassMember(LuaShengXiaoCardWnd,"m_Button1Label")
RegistClassMember(LuaShengXiaoCardWnd,"m_Alert")
RegistClassMember(LuaShengXiaoCardWnd,"m_ShengXiaoInfoTableView")
RegistClassMember(LuaShengXiaoCardWnd,"m_ShengXiaoPlayed")
RegistClassMember(LuaShengXiaoCardWnd,"m_VoiceSprite")
RegistClassMember(LuaShengXiaoCardWnd,"m_IsGuaJi")
-- RegistClassMember(LuaShengXiaoCardWnd,"m_GuessCardCmp")
RegistClassMember(LuaShengXiaoCardWnd,"m_Ticks")

function LuaShengXiaoCardWnd:Awake()
    self.m_Ticks = {}
    self.m_IsGuaJi = false
    self.CardTemplate:SetActive(false)
    local label = self.transform:Find("Anchor/TopLeft/Label"):GetComponent(typeof(UILabel))
    if LuaShengXiaoCardMgr.m_Context == 2 then
        label.text = LocalString.GetString("休闲模式")
    else
        label.text = nil
        -- if LuaShengXiaoCardMgr.m_IsReportRank then
        --     label.text = LocalString.GetString("积分赛")
        -- else
            -- label.text = LocalString.GetString("匹配模式")
        -- end
    end

    -- self.SealCardTemplate:SetActive(false)
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.TuoGuanButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTuoGuanButtonClick()
	end)


	
	UIEventListener.Get(self.ChatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)


    --@endregion EventBind end
    self.ShengXiaoInfo:SetActive(false)
    self.Guess:SetActive(false)
    -- self.ChooseShengXiao:SetActive(false)

    self.m_Button1 = self.transform:Find("Anchor/Buttons/Button1").gameObject
    self.m_Button1:SetActive(false)
    self.m_Button1Label = self.m_Button1.transform:Find("Label"):GetComponent(typeof(UILabel))

    self.m_Alert=FindChild(self.ChatButton.transform,"Alert"):GetComponent(typeof(UISprite))
    self:OnIMShouldShowAlertValueChange(nil)

    self.GuessInfoLabel.gameObject:SetActive(false)
    self.GuessInfoLabel.text = nil
    self.GuessCard:SetActive(false)
    self.StatusLabel.text = nil

    self.CancleTuoGuanButton:SetActive(false)
    UIEventListener.Get(self.CancleTuoGuanButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.ShengXiaoCard_StopGuaji()
    end)

    self.m_VoiceSprite = self.VoiceButton.transform:Find("Sprite"):GetComponent(typeof(UISprite))
    UIEventListener.Get(self.VoiceButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local isInStream = CCMiniAPIMgr.Inst:IsInPlayCCStream()
        print("isInStream",isInStream)
        if isInStream then
            CCMiniAPIMgr.Inst:StopCCMiniCapture()
            CCCChatMgr.Inst:LeaveRemoteCCChannel(EnumCCMiniStreamType.ePlayCC)
        else
            CCMiniAPIMgr.Inst:StartCCMiniCapture(EnumCCMiniStreamType.ePlayCC, nil)
            CCCChatMgr.Inst:JoinRemoteCCChannel(EnumCCMiniStreamType.ePlayCC, nil, "")
        end
    end)


    self.m_ShengXiaoInfoTableView = self.ShengXiaoInfo.transform:Find("TableView"):GetComponent(typeof(QnTableView))

    self.m_ShengXiaoPlayed = {}
    self.m_ShengXiaoInfoTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return 6
        end,
        function(item,row)
            local tf = item.transform
            local label = tf:Find("Label"):GetComponent(typeof(UILabel))
            local card = self.m_ShengXiaoPlayed[row+1]
            if card then
                if ShengXiaoCard_ShengXiao.Exists(card) then
                    local design = ShengXiaoCard_ShengXiao.GetData(card)
                    label.text = design.Name
                    label.color = NGUIText.ParseColor24(design.Color, 1)
                else
                    label.text=tostring(card)
                end
            else
                label.text = "?"
            end
        end)
end

function LuaShengXiaoCardWnd:RefreshGameMode()
    if LuaShengXiaoCardMgr.m_Context == 1 then
        local label = self.transform:Find("Anchor/TopLeft/Label"):GetComponent(typeof(UILabel))
        label.text = LocalString.GetString("匹配模式")
        if LuaShengXiaoCardMgr.m_PlayerProfiles then
            for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
                if v.Index==LuaShengXiaoCardMgr.m_PlayerIndex then
                    if v.GameInfo and v.GameInfo.Report then
                        label.text = CChatLinkMgr.TranslateToNGUIText(LocalString.GetString("匹配模式（#R积分#n）"))
                    end
                    break
                end
            end
        end
    end
end

function LuaShengXiaoCardWnd:OnIMShouldShowAlertValueChange(args)
    local immgrInst=CIMMgr.Inst
    self.m_Alert.enabled=immgrInst.ShowIMAlert_ContainSystemMsg 
                        or CJingLingMgr.Inst.ExistsUnreadMsgs 
                        or immgrInst.ShouldShowFriendRequestAlert
                        or CGroupIMMgr.Inst.ShowGroupIMAlert_NotContainIgnore
end
function LuaShengXiaoCardWnd:Init()
    print("Init")
    self.m_PlayerViews={}
    for i=1,3 do
        -- local playerView=CLuaDouDiZhuPlayerView:new()
        local node=FindChild(self.transform,"Player"..tostring(i))
        local script = node.gameObject:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script.m_LuaSelf:InitView(i)
        -- playerView:Init(node,i)
        table.insert( self.m_PlayerViews, script.m_LuaSelf)
    end
    self:InitStatus()

    self:RefreshVoiceSprite()
end

function LuaShengXiaoCardWnd:InitStatus()
    print("m_PlayerIndex",LuaShengXiaoCardMgr.m_PlayerIndex)
    if LuaShengXiaoCardMgr.m_PlayerProfiles then
        self:OnShengXiaoCard_SyncPlayState(LuaShengXiaoCardMgr.m_State,
            LuaShengXiaoCardMgr.m_Round,
            LuaShengXiaoCardMgr.m_SubRound,
            LuaShengXiaoCardMgr.m_LoserId)
    end

end

--@region UIEvent

function LuaShengXiaoCardWnd:OnLeaveButtonClick()
    local msg = g_MessageMgr:FormatMessage("ShengXiaoCard_LeavePlay")
    g_MessageMgr:ShowOkCancelMessage(msg, function()
        CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardWnd)
        Gac2Gas.RequestLeavePlay()
    end, nil, nil, nil, false)
end

function LuaShengXiaoCardWnd:OnTipButtonClick()
    -- g_MessageMgr:ShowMessage("ShengXiaoCard_Tip")
    LuaShengXiaoCardMgr.ShowTipWnd()
end

function LuaShengXiaoCardWnd:OnTuoGuanButtonClick()
    Gac2Gas.ShengXiaoCard_StartGuaji()
end


function LuaShengXiaoCardWnd:OnChatButtonClick()
    if CUIManager.IsLoaded(CUIResources.SocialWnd) then
        CUIManager.CloseUI(CUIResources.SocialWnd)
    else
        CSocialWndMgr.ShowChatWnd(EChatPanel.Current)
    end
end


--@endregion UIEvent

function LuaShengXiaoCardWnd:OnEnable()
    g_ScriptEvent:AddListener("IMShouldShowAlertValueChange", self, "OnIMShouldShowAlertValueChange")

    g_ScriptEvent:AddListener("ShengXiaoCard_SyncPlayState", self, "OnShengXiaoCard_SyncPlayState")
    g_ScriptEvent:AddListener("ShengXiaoCard_SyncPrepare", self, "OnShengXiaoCard_SyncPrepare")
    g_ScriptEvent:AddListener("ShengXiaoCard_SyncLeave", self, "OnShengXiaoCard_SyncLeave")
    g_ScriptEvent:AddListener("ShengXiaoCard_SyncJoin", self, "OnShengXiaoCard_SyncJoin")
    
    g_ScriptEvent:AddListener("ShengXiaoCard_SyncInteract", self, "OnShengXiaoCard_SyncInteract")
    g_ScriptEvent:AddListener("ShengXiaoCard_SyncGuaji", self, "OnShengXiaoCard_SyncGuaji")
    g_ScriptEvent:AddListener("OnJoinCCStream", self, "OnJoinCCStream")
    g_ScriptEvent:AddListener("OnQuitCCStream", self, "OnQuitCCStream")
end
function LuaShengXiaoCardWnd:OnDisable()
    g_ScriptEvent:RemoveListener("IMShouldShowAlertValueChange", self, "OnIMShouldShowAlertValueChange")

    g_ScriptEvent:RemoveListener("ShengXiaoCard_SyncPlayState", self, "OnShengXiaoCard_SyncPlayState")
    g_ScriptEvent:RemoveListener("ShengXiaoCard_SyncPrepare", self, "OnShengXiaoCard_SyncPrepare")
    g_ScriptEvent:RemoveListener("ShengXiaoCard_SyncLeave", self, "OnShengXiaoCard_SyncLeave")
    g_ScriptEvent:RemoveListener("ShengXiaoCard_SyncJoin", self, "OnShengXiaoCard_SyncJoin")
    g_ScriptEvent:RemoveListener("ShengXiaoCard_SyncInteract", self, "OnShengXiaoCard_SyncInteract")
    g_ScriptEvent:RemoveListener("ShengXiaoCard_SyncGuaji", self, "OnShengXiaoCard_SyncGuaji")

    g_ScriptEvent:RemoveListener("OnJoinCCStream", self, "OnJoinCCStream")
    g_ScriptEvent:RemoveListener("OnQuitCCStream", self, "OnQuitCCStream")
end
function LuaShengXiaoCardWnd:OnJoinCCStream(...)
    self:RefreshVoiceSprite()
    --刷新
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        local viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
        self.m_PlayerViews[viewChairId]:RefreshVoiceButton()
    end
end
function LuaShengXiaoCardWnd:OnQuitCCStream(...)
    self:RefreshVoiceSprite()
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        local viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
        self.m_PlayerViews[viewChairId]:OnQuitCCStream(...)
        self.m_PlayerViews[viewChairId]:RefreshVoiceButton()
    end

end
function LuaShengXiaoCardWnd:RefreshVoiceSprite()
    local isInStream = CCMiniAPIMgr.Inst:IsInPlayCCStream()
    self.m_VoiceSprite.spriteName = isInStream and "clubhouse_bimai" or "clubhouse_kaimai"
end
function LuaShengXiaoCardWnd:OnShengXiaoCard_SyncGuaji(playerId, Guaji)
    -- print("OnShengXiaoCard_SyncGuaji",playerId, Guaji)
    local index = LuaShengXiaoCardMgr.m_PlayerProfiles[playerId].Index
    local viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(index)
    if viewChairId==1 then
        self.m_IsGuaJi = Guaji
        self.CancleTuoGuanButton:SetActive(Guaji)
    else
        self.m_PlayerViews[viewChairId]:OnShengXiaoCard_SyncGuaji(Guaji)
    end

end

function LuaShengXiaoCardWnd:SetMessage(msgKey,...)
    self.GuessInfoLabel.gameObject:SetActive(true)
    self.GuessInfoLabel.text = g_MessageMgr:FormatMessage(msgKey,...)
end

function LuaShengXiaoCardWnd:GetGameNames()
    local chupaiName = nil
    local guessName = nil
    local passName = nil
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        if v.GameInfo then
            if v.GameInfo.Role == EnumShengXiaoCardRole.Show then
                chupaiName = v.Name
            elseif v.GameInfo.Role == EnumShengXiaoCardRole.Guess then
                guessName = v.Name
            elseif v.GameInfo.Role == EnumShengXiaoCardRole.Pass then
                passName = v.Name
            end
        end
    end
    return chupaiName,guessName,passName
end
function LuaShengXiaoCardWnd:GetGuessViewIndex()
    local guessIndex = 0
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        if v.GameInfo.Role == EnumShengXiaoCardRole.Guess then
            guessIndex = v.Index
            break
        end
    end
    local viewIndex = 0
    if guessIndex>0 then
        viewIndex = LuaShengXiaoCardMgr.SwitchViewChairId(guessIndex)
    end
    return viewIndex
end
function LuaShengXiaoCardWnd:GetChupaiViewIndex()

end

function LuaShengXiaoCardWnd:SetGameMessage(state)
    local myRole = 0
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        if v.Index==LuaShengXiaoCardMgr.m_PlayerIndex then
            if v.GameInfo then
                myRole = v.GameInfo.Role
            end
            break
        end
    end
    if myRole==0 then
        return
    end

    local chupaiName,guessName,passName = self:GetGameNames()
    local realCardName,displayCardName = nil,nil
    if LuaShengXiaoCardMgr.m_ShowInfo then
        realCardName,displayCardName = LuaShengXiaoCardMgr.m_ShowInfo.RealCardName,LuaShengXiaoCardMgr.m_ShowInfo.DisplayCardName
    end
    local yesStr = LocalString.GetString("相信")
    local noStr = LocalString.GetString("不相信")


    if state == EnumShengXiaoCardState.WaitShow then
        if myRole == EnumShengXiaoCardRole.Show then--我是出牌者
            self:SetMessage("ShengXiaoCard_WaitShow_ChuPaiZhe",guessName)
        elseif myRole == EnumShengXiaoCardRole.Guess then--我是猜牌者
            self:SetMessage("ShengXiaoCard_WaitShow_CaiPaiZhe",chupaiName)
        elseif myRole == EnumShengXiaoCardRole.Watch then--我是观牌者
            self:SetMessage("ShengXiaoCard_WaitShow_GuanPaiZhe",chupaiName,guessName)
        end
    elseif state == EnumShengXiaoCardState.PassShow then
        if myRole == EnumShengXiaoCardRole.Show then--我是出牌者
            self:SetMessage("ShengXiaoCard_PassShow_ChuPaiZhe",passName,guessName)
        elseif myRole == EnumShengXiaoCardRole.Guess then--我是猜牌者
            self:SetMessage("ShengXiaoCard_PassShow_CaiPaiZhe",passName)
        elseif myRole == EnumShengXiaoCardRole.Watch then--我是观牌者
        elseif myRole == EnumShengXiaoCardRole.Pass then--我是观牌者
            self:SetMessage("ShengXiaoCard_PassShow_ChuanPaiZhe",chupaiName,realCardName,displayCardName)
        end
    elseif state == EnumShengXiaoCardState.WaitGuess then
        if myRole == EnumShengXiaoCardRole.Show then--我是出牌者
            self:SetMessage("ShengXiaoCard_WaitGuess_ChuPaiZhe",realCardName,displayCardName,guessName)
        elseif myRole == EnumShengXiaoCardRole.Guess then--我是猜牌者
            self:SetMessage("ShengXiaoCard_WaitGuess_CaiPaiZhe",chupaiName,displayCardName)
        elseif myRole == EnumShengXiaoCardRole.Watch then--我是观牌者
            self:SetMessage("ShengXiaoCard_WaitGuess_GuanPaiZhe",chupaiName,displayCardName,guessName)
        end
    elseif state == EnumShengXiaoCardState.PassGuess then
        if myRole == EnumShengXiaoCardRole.Show then--我是出牌者
            self:SetMessage("ShengXiaoCard_PassGuess_ChuPaiZhe",passName,realCardName,displayCardName,guessName)
        elseif myRole == EnumShengXiaoCardRole.Guess then--我是猜牌者
            self:SetMessage("ShengXiaoCard_PassGuess_CaiPaiZhe",passName,displayCardName)
        elseif myRole == EnumShengXiaoCardRole.Watch then--我是观牌者
        elseif myRole == EnumShengXiaoCardRole.Pass then
            self:SetMessage("ShengXiaoCard_PassGuess_ChuanPaiZhe",realCardName,displayCardName,guessName)
        end
    elseif state == EnumShengXiaoCardState.WaitEnd then
        if LuaShengXiaoCardMgr.m_GuessInfo then
            local yesOrNo = LuaShengXiaoCardMgr.m_GuessInfo.YesOrNo
            if myRole == EnumShengXiaoCardRole.Show or--我是出牌者
                myRole == EnumShengXiaoCardRole.Watch or--我是观牌者
                myRole == EnumShengXiaoCardRole.Pass then
                self:SetMessage("ShengXiaoCard_WaitEnd_ChuPaiZhe",guessName,yesOrNo>0 and yesStr or noStr)
            elseif myRole == EnumShengXiaoCardRole.Guess then--我是猜牌者
                self:SetMessage("ShengXiaoCard_WaitEnd_CaiPaiZhe",yesOrNo>0 and yesStr or noStr)
            end
        end

    elseif state == EnumShengXiaoCardState.RoundEnd then
        if LuaShengXiaoCardMgr.m_GuessInfo then
            local yesOrNo = LuaShengXiaoCardMgr.m_GuessInfo.YesOrNo
            local realShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.Card.ShengXiao
            local displayShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.DisplayId
            --猜对
            if (realShengXiao==displayShengXiao and yesOrNo>0) or (realShengXiao~=displayShengXiao and yesOrNo==0) then
                if myRole == EnumShengXiaoCardRole.Show then--我是出牌者
                    if not passName then
                        self:SetMessage("ShengXiaoCard_RoundEnd_Right_ChuPaiZhe",guessName,yesOrNo>0 and yesStr or noStr,realCardName)
                    else
                        self:SetMessage("ShengXiaoCard_RoundEnd_Right_PassLose_ChuPaiZhe",guessName,yesOrNo>0 and yesStr or noStr,passName,realCardName)
                    end
                elseif myRole == EnumShengXiaoCardRole.Guess then--我是猜牌者
                    if not passName then
                        self:SetMessage("ShengXiaoCard_RoundEnd_Right_CaiPaiZhe",realCardName,yesOrNo>0 and yesStr or noStr,chupaiName)
                    else
                        self:SetMessage("ShengXiaoCard_RoundEnd_Right_PassLose_CaiPaiZhe",realCardName,passName)
                    end
                elseif myRole == EnumShengXiaoCardRole.Watch then--我是观牌者
                    if not passName then
                        self:SetMessage("ShengXiaoCard_RoundEnd_Right_GuanPaiZhe",realCardName,guessName,yesOrNo>0 and yesStr or noStr,chupaiName)
                    end
                elseif myRole == EnumShengXiaoCardRole.Pass then--我是传牌者
                    if passName then--有人传牌了
                        self:SetMessage("ShengXiaoCard_RoundEnd_Right_PassLose_ChuanPaiZhe",guessName,yesOrNo>0 and yesStr or noStr,realCardName)
                    end
                end
            else
                if myRole == EnumShengXiaoCardRole.Show then--我是出牌者
                    self:SetMessage("ShengXiaoCard_RoundEnd_Wrong_ChuPaiZhe",guessName,yesOrNo>0 and yesStr or noStr,realCardName)
                elseif myRole == EnumShengXiaoCardRole.Guess then--我是猜牌者
                    self:SetMessage("ShengXiaoCard_RoundEnd_Wrong_CaiPaiZhe",realCardName,yesOrNo>0 and yesStr or noStr,chupaiName)
                elseif myRole == EnumShengXiaoCardRole.Watch then--我是观牌者
                    self:SetMessage("ShengXiaoCard_RoundEnd_Wrong_GuanPaiZhe",realCardName,guessName,yesOrNo>0 and yesStr or noStr,chupaiName)
                elseif myRole == EnumShengXiaoCardRole.Pass then--我是传牌者
                    if passName then--有人传牌了
                        self:SetMessage("ShengXiaoCard_RoundEnd_Wrong_ChuanPaiZhe",guessName,yesOrNo>0 and yesStr or noStr,realCardName)
                    end
                end
            end
        end
    end
end

function LuaShengXiaoCardWnd:OnShengXiaoCard_SyncPlayState(state,Round, SubRound, LoserId)
    self:RefreshGameMode()

    self.m_Button1:SetActive(false)
    CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardChooseWnd)
    CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardConfirmWnd)
    self.GuessInfoLabel.gameObject:SetActive(false)

    --取消倒计时
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        local viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
        self.m_PlayerViews[viewChairId]:CancelCountdown()
    end
    

    if state>=EnumShengXiaoCardState.Start then
        self.ShengXiaoInfo:SetActive(true)
        self.m_ShengXiaoPlayed = {}
        for k,v in pairs(LuaShengXiaoCardMgr.m_PlayedCards) do
            table.insert( self.m_ShengXiaoPlayed, k)
        end
        table.sort(self.m_ShengXiaoPlayed,function(a,b)return a<b end)
        self.m_ShengXiaoInfoTableView:ReloadData(false,false)
    else
        self.ShengXiaoInfo:SetActive(false)
    end


    self:SetGameMessage(state)
    if state == EnumShengXiaoCardState.Idle then
    elseif state == EnumShengXiaoCardState.Prepare then
        --挂机状态取消
        self.m_IsGuaJi = false
        self.CancleTuoGuanButton:SetActive(false)

        self.Guess:SetActive(false)

        self.m_Button1:SetActive(true)
        self.m_Button1Label.text = LocalString.GetString("准备")
        UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.ShengXiaoCard_Prepare()
        end)

        local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
        local profile = LuaShengXiaoCardMgr.m_PlayerProfiles[myId]
        if profile and profile.Prepared then
            self.m_Button1:SetActive(false)
        end

        for i=1,3 do
            self.m_PlayerViews[i]:InitProfile(nil)
        end

    elseif state == EnumShengXiaoCardState.Start then
        self.StatusLabel.text = nil
        LuaShengXiaoCardMgr.TryTriggerGuide(1)
        if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide1) then
            --第二局
            LuaShengXiaoCardMgr.TryTriggerGuide(8)
        end
    elseif state == EnumShengXiaoCardState.Reward then
        print("state reward")
        for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
            if k==LuaShengXiaoCardMgr.m_LoserId then
                local viewIndex = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
                self.m_PlayerViews[viewIndex]:ShowLoseEffect()
                break
            end
        end
    elseif state == EnumShengXiaoCardState.End then
        -- RegisterTickOnce(function()
        --     CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardWnd)
        -- end,1000)

    elseif state == EnumShengXiaoCardState.RoundStart then
        --当玩家第一次游戏开始2秒后
        
    elseif state == EnumShengXiaoCardState.WaitShow or state == EnumShengXiaoCardState.PassShow then
        --等待出牌

        --看看是不是轮到我了
        local chupaiIndex = 0
        local passIndex = 0
        for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
            if v.GameInfo.Role == EnumShengXiaoCardRole.Show then
                chupaiIndex = v.Index
            elseif v.GameInfo.Role == EnumShengXiaoCardRole.Pass then
                passIndex = v.Index
            end
        end
        if state == EnumShengXiaoCardState.WaitShow and chupaiIndex>0 then
            local viewIndex = LuaShengXiaoCardMgr.SwitchViewChairId(chupaiIndex)
            if viewIndex==1 and not self.m_IsGuaJi then
                LuaShengXiaoCardMgr.TryTriggerGuide(3)
                self.m_Button1:SetActive(true)
                self.m_Button1Label.text = LocalString.GetString("出牌")
                UIEventListener.Get(self.m_Button1).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:PlayCard()
                end)
            else
                self.m_Button1:SetActive(false)
            end
            self.m_PlayerViews[viewIndex]:StartCountdown(LuaShengXiaoCardMgr.m_ExpiredTime)
        elseif state == EnumShengXiaoCardState.PassShow and passIndex>0 then
            local viewIndex = LuaShengXiaoCardMgr.SwitchViewChairId(passIndex)
            if viewIndex==1 and not self.m_IsGuaJi then
                local showInfo = LuaShengXiaoCardMgr.m_ShowInfo
                if showInfo then
                    LuaShengXiaoCardChooseWnd.s_ShengXiao = showInfo.DisplayId
                end
                CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardChooseWnd)
            else
                self.m_Button1:SetActive(false)
            end
            self.m_PlayerViews[viewIndex]:StartCountdown(LuaShengXiaoCardMgr.m_ExpiredTime)
        end

        self.Guess:SetActive(true)
        if state == EnumShengXiaoCardState.WaitShow then
            for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
                if v.Index==LuaShengXiaoCardMgr.m_PlayerIndex then
                    if v.GameInfo.Role == EnumShengXiaoCardRole.Show then--我是出牌者
                        self.GuessCard:SetActive(false) 
                    elseif v.GameInfo.Role == EnumShengXiaoCardRole.Guess then--我是猜牌者
                        self:SetGuessCard(0,"showback")
                    elseif v.GameInfo.Role == EnumShengXiaoCardRole.Watch then--我是观牌者
                        self:SetGuessCard(0,"showback")
                    elseif v.GameInfo.Role == EnumShengXiaoCardRole.Pass then--我是传牌者
                        --没有传牌者
                        self.GuessCard:SetActive(false) 
                    end
                end
            end
        elseif state == EnumShengXiaoCardState.PassShow then
            local showInfo = LuaShengXiaoCardMgr.m_ShowInfo
            if showInfo then
                local realId = showInfo.Card.ShengXiao
                local displayId = showInfo.DisplayId

                for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
                    if v.Index==LuaShengXiaoCardMgr.m_PlayerIndex then
                        if v.GameInfo.Role == EnumShengXiaoCardRole.Show then--我是出牌者
                            self:SetGuessCard(realId,"showwithfog")
                        elseif v.GameInfo.Role == EnumShengXiaoCardRole.Guess then--我是猜牌者
                            self:SetGuessCard(realId,"showback")
                        elseif v.GameInfo.Role == EnumShengXiaoCardRole.Watch then--我是观牌者
                            self.GuessCard:SetActive(false) 
                            --没有观牌者
                        elseif v.GameInfo.Role == EnumShengXiaoCardRole.Pass then--我是传牌者
                            self:SetGuessCard(realId,"showwithfog")
                        end
                    end
                end
            end
        -- else
        --     self.GuessCard:SetActive(false) 
        end
        -- self.Guess:SetActive(true)
    elseif state == EnumShengXiaoCardState.WaitGuess or state == EnumShengXiaoCardState.PassGuess then
        --等待猜牌
        local guessViewIndex = LuaShengXiaoCardWnd:GetGuessViewIndex()
        if guessViewIndex>0 then
            if guessViewIndex==1 and not self.m_IsGuaJi then
                LuaShengXiaoCardMgr.TryTriggerGuide(2)
                CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardConfirmWnd)
            end
            --倒计时
            self.m_PlayerViews[guessViewIndex]:StartCountdown(LuaShengXiaoCardMgr.m_ExpiredTime)
        end

        self.Guess:SetActive(true)
        local showInfo = LuaShengXiaoCardMgr.m_ShowInfo
        if showInfo then--有出牌信息
            local realId = showInfo.Card.ShengXiao
            local displayId = showInfo.DisplayId
            for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
                if v.Index==LuaShengXiaoCardMgr.m_PlayerIndex then
                    if v.GameInfo.Role == EnumShengXiaoCardRole.Show then--我是出牌者
                        self:SetGuessCard(realId,"showwithfog")
                    elseif v.GameInfo.Role == EnumShengXiaoCardRole.Guess then--我是猜牌者
                        self:SetGuessCard(displayId,"hide")
                    elseif v.GameInfo.Role == EnumShengXiaoCardRole.Watch then--我是观牌者
                        self:SetGuessCard(displayId,"hide")
                    elseif v.GameInfo.Role == EnumShengXiaoCardRole.Pass then
                        self:SetGuessCard(realId,"showwithfog")
                    end
                    break
                end
            end
        end

    -- elseif state == EnumShengXiaoCardState.PassShow then
        --过牌
    -- elseif state == EnumShengXiaoCardState.PassGuess then
        --出牌
    elseif state == EnumShengXiaoCardState.WaitEnd then
        CCenterCountdownWnd.count = 3
        CCenterCountdownWnd.InitStartTime()
        CUIManager.ShowUI(CUIResources.CenterCountdownWnd)

    elseif state == EnumShengXiaoCardState.RoundEnd then
        local chupaiName,guessName,passName = self:GetGameNames()
        if LuaShengXiaoCardMgr.m_GuessInfo then
            local yesOrNo = LuaShengXiaoCardMgr.m_GuessInfo.YesOrNo
            local realShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.Card.ShengXiao
            local displayShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.DisplayId

            self.Guess:SetActive(true)

            for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
                if v.Index==LuaShengXiaoCardMgr.m_PlayerIndex then
                    self:SetGuessCard(realShengXiao,"showresult",v.GameInfo.Role)
                end
            end

            local guessViewIndex = LuaShengXiaoCardWnd:GetGuessViewIndex()

            --猜对
            if self.m_Ticks["showend"] then 
                UnRegisterTick(self.m_Ticks["showend"]) 
                self.m_Ticks["showend"]=nil 
            end
            if (realShengXiao==displayShengXiao and yesOrNo>0) or (realShengXiao~=displayShengXiao and yesOrNo==0) then
                self.m_Ticks["showend"] = RegisterTickOnce(function()
                    self.m_Ticks["showend"] = nil
                    self.m_PlayerViews[guessViewIndex]:ShowRoundEndEffect(true)
                end,1000)
            else
                self.m_Ticks["showend"] = RegisterTickOnce(function()
                    self.m_Ticks["showend"] = nil
                    self.m_PlayerViews[guessViewIndex]:ShowRoundEndEffect(false)
                end,1000)
            end
            --印记牌飞行
            self:FlyGuessCard()
        end
    end

    local t = {}
    for i=1,3 do
        t[i] = false
    end
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        local viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
        t[viewChairId] = true
        self.m_PlayerViews[viewChairId]:InitProfile(v)
        self.m_PlayerViews[viewChairId]:RefreshVoiceButton()
    end

    for i,v in ipairs(t) do
        if not v then
            self.m_PlayerViews[i]:InitProfile(nil)
        end
    end

end
function LuaShengXiaoCardWnd:OnShengXiaoCard_SyncPrepare(playerId, bPrepare)
    local profile = LuaShengXiaoCardMgr.m_PlayerProfiles[playerId]
    if profile then
        local viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(profile.Index)

        local playerView = self.m_PlayerViews[viewChairId]
        playerView:OnShengXiaoCard_SyncPrepare(bPrepare)
        if playerView.m_ViewIndex==1 then
            self.m_Button1:SetActive(false)
            self.StatusLabel.text = LocalString.GetString("准备完毕，等待其他玩家准备")
        end
    end
end
function LuaShengXiaoCardWnd:OnShengXiaoCard_SyncLeave(playerId)

end
function LuaShengXiaoCardWnd:OnShengXiaoCard_SyncJoin(playerId, playerProfile_U)

end

function LuaShengXiaoCardWnd:FlyGuessCard()
    if self.m_Ticks["flycard"] then UnRegisterTick(self.m_Ticks["flycard"]) self.m_Ticks["flycard"]=nil end
    self.m_Ticks["flycard"] = RegisterTickOnce(function()
        self.m_Ticks["flycard"] = nil
        if not LuaShengXiaoCardMgr.m_GuessInfo then return end
        if not LuaShengXiaoCardMgr.m_ShowInfo then return end

        local yesOrNo = LuaShengXiaoCardMgr.m_GuessInfo.YesOrNo
        local realShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.Card.ShengXiao
        local displayShengXiao = LuaShengXiaoCardMgr.m_ShowInfo.DisplayId
        local targetPlayerId = 0
        --猜对
        if (realShengXiao==displayShengXiao and yesOrNo>0) or (realShengXiao~=displayShengXiao and yesOrNo==0) then
            --飞到出牌人
            targetPlayerId = LuaShengXiaoCardMgr.m_ShowInfo.Actor
        else
            --飞到猜牌人
            targetPlayerId = LuaShengXiaoCardMgr.m_GuessInfo.Actor
        end
        local from = nil
        local to = nil
        local fxs = { self.InteractFx1,self.InteractFx2,self.InteractFx3 }
        for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
            if k==targetPlayerId then
                local viewIndex = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
                to = fxs[viewIndex]
            end
        end

        local tf = self.GuessCard.transform
        local node = tf:Find("Node")

        node.localPosition = Vector3(0,0,0)
        local toPos = node.parent:InverseTransformPoint(to.transform.position)

        --位移
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPosition(node,toPos.x,toPos.y,toPos.z,0.8),Ease.InOutSine)
        --缩放
        LuaTweenUtils.TweenScaleTo(node,Vector3(0.2,0.2,0.2),0.8)
        node.localEulerAngles = Vector3(0,0,0)
        --旋转
        LuaTweenUtils.TweenVector3(Vector3(0,0,0),Vector3(0,0,720),0.8,function(z)
            node.localEulerAngles = z
        end)
        LuaTweenUtils.TweenAlpha(node,1,0.3,0.8)
    end,2000)
end

-- local tick = nil
-- local tick2 = nil
function LuaShengXiaoCardWnd:SetGuessCard(shengxiao,mode,role)
    -- print("SetGuessCard",mode)
    self.GuessCard:SetActive(true)
    local tf = self.GuessCard.transform
    LuaShengXiaoCard.SetName(tf,shengxiao)
    local back = tf:Find("Node/Back").gameObject
    local node = tf:Find("Node").gameObject
    local mark = tf:Find("Node/Mark").gameObject
    node.transform.localScale = Vector3(1,1,1)
    node.transform.localPosition = Vector3(0,0,0)
    node.transform.localEulerAngles = Vector3(0,0,0)
    LuaTweenUtils.TweenAlpha(node.transform,1,1,0.01)

    back.transform.localScale = Vector3(1,1,1)
    node.transform.localScale = Vector3(1,1,1)


    node:SetActive(true)
    local fx = tf:Find("Fx"):GetComponent(typeof(CUIFx))
    fx.gameObject:GetComponent(typeof(UIWidget)).alpha=0

    -- if tick then UnRegisterTick(tick) tick=nil end
    -- if tick2 then UnRegisterTick(tick2) tick2=nil end
    fx:DestroyFx()
    LuaTweenUtils.DOKill(back.transform,false)

    if mode=="hide" then
        fx:LoadFx("Fx/UI/Prefab/UI_hwcb_miwu001.prefab")
        back:SetActive(true)
        LuaTweenUtils.TweenAlpha(back.transform,1,0.5,1)
    elseif mode=="show" then
        back:SetActive(false)
    elseif mode=="showwithfog" then
        fx:LoadFx("Fx/UI/Prefab/UI_hwcb_miwu001.prefab")
        back:SetActive(false)

    elseif mode=="showback" then
        back:SetActive(true)
        back:GetComponent(typeof(UIWidget)).alpha=1

    elseif mode=="play" then
        back:SetActive(false)
        LuaShengXiaoCard.CardAppear(node.transform)
    elseif mode=="showresult" then
        if role==EnumShengXiaoCardRole.Show or role==EnumShengXiaoCardRole.Pass then
        else
            back:SetActive(true)
            back:GetComponent(typeof(UIWidget)).alpha=1

            LuaTweenUtils.TweenScaleXY(back.transform,0,1,0.3,Ease.InCirc)

            mark:SetActive(true)
            mark.transform.localScale = Vector3(0,1,1)
            LuaTweenUtils.SetDelay(LuaTweenUtils.TweenScaleXY(mark.transform,1,1,0.3,Ease.OutCirc),0.3)
        end
    end

end
-- local tick5 = nil
function LuaShengXiaoCardWnd:OnShengXiaoCard_SyncInteract(srcId, targetId, InteractType)
    local fxs = { self.InteractFx1,self.InteractFx2,self.InteractFx3 }
    local from = nil
    local to = nil
    local viewChairId = 0
    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        if k==srcId then
            viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
            from = fxs[viewChairId]
        elseif k==targetId then
            viewChairId = LuaShengXiaoCardMgr.SwitchViewChairId(v.Index)
            to = fxs[viewChairId]
        end
    end
    if InteractType>2 and viewChairId>0 then
        local playerView = self.m_PlayerViews[viewChairId]
        playerView:ShowExpression(InteractType)
        return
    end

    self.OverlayFx:DestroyFx()
    if from and to then
        if InteractType==0 then
            self.OverlayFx:LoadFx("Fx/UI/Prefab/UI_hwcb_caoxie_trail.prefab")
            SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.shoesthrow, Vector3(0,0,0),nil,0)
        elseif InteractType==1 then
            self.OverlayFx:LoadFx("Fx/UI/Prefab/UI_hwcb_xueqiu_trail.prefab")
            SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.snowthrow, Vector3(0,0,0),nil,0)
        elseif InteractType==2 then
            self.OverlayFx:LoadFx("Fx/UI/Prefab/UI_hwcb_xianhua_trail.prefab")
            SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.rosethrow, Vector3(0,0,0),nil,0)
        end
        local fromPos = self.OverlayFx.transform.parent:InverseTransformPoint(from.transform.position)
        local toPos = self.OverlayFx.transform.parent:InverseTransformPoint(to.transform.position)
        self.OverlayFx.transform.localPosition = fromPos
        
        to:DestroyFx()

        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPosition(self.OverlayFx.transform,toPos.x,toPos.y,toPos.z,1),Ease.InOutSine)
        if self.m_Ticks["interact"] then UnRegisterTick(self.m_Ticks["interact"]) self.m_Ticks["interact"]=nil end
        self.m_Ticks["interact"] = RegisterTickOnce(function()
            self.m_Ticks["interact"] = nil
            self.OverlayFx:DestroyFx()
            if InteractType==0 then
                to:LoadFx("Fx/UI/Prefab/UI_hwcb_caoxie_attack.prefab")
                SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.shoeshit, Vector3(0,0,0),nil,0)
            elseif InteractType==1 then
                to:LoadFx("Fx/UI/Prefab/UI_hwcb_xueqiu_attack.prefab")
                SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.snowhit, Vector3(0,0,0),nil,0)
            elseif InteractType==2 then
                to:LoadFx("Fx/UI/Prefab/UI_hwcb_xianhua_attack.prefab")
                SoundManager.Inst:PlayOneShot(LuaShengXiaoCardMgr.Sounds.rosehit, Vector3(0,0,0),nil,0)
            end
        end,1000)
    end
end

-- local tick3 = nil
function LuaShengXiaoCardWnd:PlayCard()
    --先看有没有选牌
    local playerView = self.m_PlayerViews[1]
    local idx,card = playerView:GetSelectedCard()
    if card then
        self.m_Button1:SetActive(false)
        playerView:PlayCard(idx,card)

        self:SetGuessCard(card.m_ShengXiao,"play")

        if self.m_Ticks["playcard"] then UnRegisterTick(self.m_Ticks["playcard"]) self.m_Ticks["playcard"]=nil end
        self.m_Ticks["playcard"] = RegisterTickOnce(function()
            LuaShengXiaoCardMgr.TryTriggerGuide(4)
            LuaShengXiaoCardChooseWnd.s_ShengXiao = card.m_ShengXiao
            CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardChooseWnd)
            self.m_Ticks.playcard = nil
        end,500)
    else
        --没选牌
        g_MessageMgr:ShowMessage("ShengXiaoCard_NoPick")
    end
end


function LuaShengXiaoCardWnd:OnDestroy()
    for k,v in pairs(self.m_Ticks) do
        UnRegisterTick(v)
    end
    self.m_Ticks = {}
end
