-- Auto Generated!!
local CDuoHunMgr = import "L10.Game.CDuoHunMgr"
local CFoeSelectPlayerTemplate = import "L10.UI.CFoeSelectPlayerTemplate"
local Color = import "UnityEngine.Color"
CFoeSelectPlayerTemplate.m_Init_CS2LuaHook = function (this, name, playerId) 
    this.playerId = playerId
    this.playerName = name
    this.playerNameLabel.color = Color.gray
    this.isGhost = false
    this.playerNameLabel.text = this.playerName
    this.hunpoSprite.enabled = false
    CDuoHunMgr.Inst.isQueryPlayerGhostInFoeSelectWnd = true
    Gac2Gas.QueryPlayerIsGhost(playerId)
end
CFoeSelectPlayerTemplate.m_OnUpdatePlayerInfo_CS2LuaHook = function (this, isGhost, playerId) 
    if playerId == this.playerId then
        this.playerNameLabel.text = this.playerName
        this.playerNameLabel.color = Color.white
        this.isGhost = isGhost
        this.hunpoSprite.enabled = isGhost
    end
end
