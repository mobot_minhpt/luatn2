
LuaAutoBaptizeNecessaryConditionMgr = {}
--洗炼附加目标。
LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList = {-1,-1}
LuaAutoBaptizeNecessaryConditionMgr.m_submitNecessaryList = {-1,-1}
LuaAutoBaptizeNecessaryConditionMgr.m_canGetWordCondsList = {}
function LuaAutoBaptizeNecessaryConditionMgr:AddNecessaryCondition(index,condition)
    LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList[index] = condition
end

function LuaAutoBaptizeNecessaryConditionMgr:RemoveNecessaryCondition(index)
    LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList[index] = -1
end

function LuaAutoBaptizeNecessaryConditionMgr:ClearNecessaryCondition()
    LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList = {-1,-1}
end

-- function LuaAutoBaptizeNecessaryConditionMgr:InitPreConditionList()
--     local defaultCountList
-- end



-- function LuaAutoBaptizeNecessaryConditionMgr:GetSubmitConfirmInfo()
--     local function GetWordCount(wordId)
--         local count = 0
--         for i,condition in ipairs(LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList) do
--             if condition == wordId then
--                 count = count+1
--             end
--         end
--         return count
--     end

--     local info = ""
--     local itemList = {}
--     for i,word in ipairs(LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList) do
--         if word ~= -1 then
--             local wordDesc = Word_Word.GetData(word * 100 + 1).WordDescription
--             local wordCount = GetWordCount(word)
--             if info ~= "" then
--                 info = info..LocalString.GetString("#n、#Y")
--             end
--             info = info..SafeStringFormat3(LocalString.GetString("%d#n条#Y%s"), wordCount, wordDesc)
--             table.insert(itemList,SafeStringFormat3(LocalString.GetString("[FFC800]%d[-]条[FFC800]%s[-]"), wordCount, wordDesc))
--             if(wordCount==2) then           
--                 break
--             end 
--         end
--     end
--     return info,itemList
-- end

function LuaAutoBaptizeNecessaryConditionMgr:CheckChooseNeccessary()
    for i,word in ipairs(LuaAutoBaptizeNecessaryConditionMgr.m_necessaryConditionList) do
        if word ~= -1 then
            return true
        end
    end
    return false
end

