local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local GameObject = import "UnityEngine.GameObject"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local QnTableItem = import "L10.UI.QnTableItem"

LuaWuLiangRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangRankWnd, "TabView", "TabView", QnAdvanceGridView)
RegistChildComponent(LuaWuLiangRankWnd, "ContentView", "ContentView", QnAdvanceGridView)
RegistChildComponent(LuaWuLiangRankWnd, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaWuLiangRankWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaWuLiangRankWnd, "VoidLabel", "VoidLabel", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end


function LuaWuLiangRankWnd:Init()
    self:InitTab()
    self:InitRank()

    -- 设置点击状态 发起请求
    if CClientMainPlayer.Inst then
        local clz = EnumToInt(CClientMainPlayer.Inst.Class)
        print(clz-1)
        self.TabView:ScrollToRow(clz-1)
        self.TabView:SetSelectRow(clz-1, true)
    end
end

function LuaWuLiangRankWnd:InitTab()
    -- 十二个职业
    self.ClassList = {}
    for i = 1, 12 do
        table.insert(self.ClassList, i)
    end
    
    self.TabView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.ClassList
    end, function(item, index)
        local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), index+1))
    end)

    -- 设置选中状态
    self.TabView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 请求
        local rankType = row + LuaWuLiangMgr.seasonRankType + 1
        Gac2Gas.SeasonRank_QueryRank(rankType, 1001)
    end)
    self.TabView:ReloadData(true,false)
end

function LuaWuLiangRankWnd:InitRank()
    -- 排名
    self.RankList = {}
    self.ContentView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.RankList
    end, function(item, index)
        local data = self.RankList[index+1]
        self:InitRankItem(item, data, index)
    end)

    -- 设置选中状态
    self.ContentView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 显示玩家信息
        local data = self.RankList[row+1]
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, 
            EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)
end

-- Key:Score playerId name level updateTime overdueTime
function LuaWuLiangRankWnd:InitRankItem(item, data, index, isSelf)
	local playerNameLabel = item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local passCountLabel = item.transform:Find("PassCountLabel"):GetComponent(typeof(UILabel))

    local rankSp = item.transform:Find("RankImage"):GetComponent(typeof(UISprite))
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))

    playerNameLabel.text = data.name
    levelLabel.text = data.Level
    passCountLabel.text = data.Score

    local qnitem = item.transform:GetComponent(typeof(QnTableItem))
    if qnitem then
        qnitem.enableHighlightedSprite = false
    end

    local rank = index + 1

    local rankImgList = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}

    local bg = item.transform:GetComponent(typeof(UISprite))

    if isSelf then
        bg.spriteName = "common_bg_mission_background_highlight"
    elseif (tonumber(index) % 2) == 0 then
		bg.spriteName = "common_bg_mission_background_n"
	else
		bg.spriteName = "common_bg_mission_background_s"
	end

    rankLabel.gameObject:SetActive(true)
    if rank <= 3 and rank >= 1 then
        rankLabel.text = ""
        rankSp.gameObject:SetActive(true)
        rankSp.spriteName = rankImgList[rank]
    else
        if rank == 0 then
            rankLabel.text = LocalString.GetString("未上榜")
        else
            rankLabel.text = tostring(rank)
        end
        rankSp.gameObject:SetActive(false)
    end
end

-- Key:Score Difficulty1 Difficulty2 Difficulty3 playerId name updateTime overdueTime
function LuaWuLiangRankWnd:TransformRankData(selfDataU, rankDataU)
    local selfRankData = {}
    local rankDataList = {}

    local selfData = MsgPackImpl.unpack(selfDataU)
    if CommonDefs.IsDic(selfData) then
        CommonDefs.DictIterate(selfData, DelegateFactory.Action_object_object(function (___key, ___value)
            selfRankData[tostring(___key)]=___value
        end))
    end

    local selfId = selfRankData.playerId
    local selfRank = 0

    local rankData = MsgPackImpl.unpack(rankDataU)
    if rankData.Count>0 then
        for i=1, rankData.Count do
            local data = rankData[i-1]
            local t = {}
            if CommonDefs.IsDic(data) then
                CommonDefs.DictIterate(data, DelegateFactory.Action_object_object(function (___key, ___value) 
                    t[tostring(___key)] = ___value
                end))
            end

            if t.playerId and t.playerId==selfId then
                selfRank = i
            end

            if i <= 20 then
                table.insert(rankDataList, t)
            end
        end
    end

    selfRankData.rank = selfRank
    return selfRankData, rankDataList
end

function LuaWuLiangRankWnd:OnData(rankType, seasonId, selfDataU, rankDataU)
    self.MyData, self.RankList = self:TransformRankData(selfDataU, rankDataU)

    if self.MyData.Score == nil then
        self.MyData.name = CClientMainPlayer.Inst.Name
        self.MyData.playerId = CClientMainPlayer.Inst.Id
        self.MyData.level = ""
        self.MyData.Score = ""
        self.MyData.UsedTime = ""
        self.MyData.rank = 0
    end
    self:InitRankItem(self.MainPlayerInfo, self.MyData, self.MyData.rank-1, true)

    local empty = (#self.RankList == 0)
    self.VoidLabel:SetActive(empty)

    self.ContentView:ReloadData(true,false)
end

function LuaWuLiangRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnData")
end

function LuaWuLiangRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnData")
end

--@region UIEvent

--@endregion UIEvent

