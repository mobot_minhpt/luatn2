local CWeatherMgr = import "L10.Game.CWeatherMgr"

LuaRefineMonsterLingliDescWnd = class()
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_LingGen")
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_LianGuai")
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_ZongMen")
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_XiePai")
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_WuXing")
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_IsOwnerIcon")
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_RuleBtn")
RegistClassMember(LuaRefineMonsterLingliDescWnd,"m_Weather")

function LuaRefineMonsterLingliDescWnd:Init()
    self.m_LingGen = self.transform:Find("Anchor/Current/Grid/1/Label"):GetComponent(typeof(UILabel))
    self.m_LianGuai = self.transform:Find("Anchor/Current/Grid/2/Label"):GetComponent(typeof(UILabel))
    self.m_ZongMen = self.transform:Find("Anchor/Current/Grid/3/Label"):GetComponent(typeof(UILabel))
    self.m_XiePai = self.transform:Find("Anchor/Current/Grid/4/Label"):GetComponent(typeof(UILabel))
    self.m_WuXing = self.transform:Find("Anchor/Current/Grid/5/Label"):GetComponent(typeof(UILabel))
    self.m_Weather = self.transform:Find("Anchor/Current/Grid/6/Label"):GetComponent(typeof(UILabel))
    self.m_LingFu = self.transform:Find("Anchor/Current/Grid/7/Label"):GetComponent(typeof(UILabel))
    self.m_IsOwnerIcon = self.transform:Find("Anchor/Current/Grid/2/OwnerIcon").gameObject
    self.m_RuleBtn = self.transform:Find("Anchor/RuleBtn").gameObject

    UIEventListener.Get(self.m_RuleBtn).onClick = DelegateFactory.VoidDelegate(
                                                          function(p)
            g_MessageMgr:ShowMessage("LianHua_FaZhen_Lingli_Introduction")
        end)

    self:InitStatus()

    if not LuaZongMenMgr.m_OpenNewSystem then
        local lingfu = self.transform:Find("Anchor/Current/Grid/7").gameObject
        lingfu:SetActive(false)

        local grid = self.transform:Find("Anchor/Current/Grid"):GetComponent(typeof(UIGrid))
        grid:Reposition()
    end
end

function LuaRefineMonsterLingliDescWnd:DealNum(n)
    local n100 = math.floor((n+0.0001)*100)
    if n100 % 100 ==0 then
        return SafeStringFormat3("%d", n100/100)
    elseif n100 %10 == 0 then
        return SafeStringFormat3("%0.1f", n100/100)
    else
        return SafeStringFormat3("%0.2f", n100/100)
    end
end

function LuaRefineMonsterLingliDescWnd:DealTime(n)
    n = math.floor(n)

    local day = n/(60*60*24)
    local hour =  n/(60*60)%24
    local minute = (n/60)%60

    if day>0 then
        return SafeStringFormat3("02d:%02d:%02d", day, hour >= 0 and hour or 0, minute >= 0 and minute or 0)
    else    
        return SafeStringFormat3("%02d:%02d", hour >= 0 and hour or 0, minute >= 0 and minute or 0)
    end
end

function LuaRefineMonsterLingliDescWnd:InitStatus()
    if LuaZongMenMgr.m_RefineMonsterStatusTable == nil or LuaZongMenMgr.m_LingLiParams ==nil then
        CUIManager.CloseUI(CLuaUIResources.RefineMonsterLingliDescWnd)
    end

    -- 还没有数据
    if LuaZongMenMgr.m_LingLiParams[1] == nil then
        self.m_LingGen.text = "-"
        self.m_WuXing.text = "-"
        self.m_ZongMen.text = "-"
        self.m_XiePai.text = "-"
        self.m_LianGuai.text = "-"
        self.m_Weather.text = "-"
        self.m_LingFu.text = "-"
        return
    end

    -- 0是没被抓
    if LuaZongMenMgr.m_RefineMonsterStatusTable["beizhuoFactor"] ~= 0 or LuaZongMenMgr.m_RefineMonsterStatusTable["damageFactor"] ~=1 then
        self.m_LingGen.text = SafeStringFormat3(LocalString.GetString("[FF5050]%s灵力/分钟"), self:DealNum(LuaZongMenMgr.m_LingLiParams[1]))
    else
        self.m_LingGen.text = SafeStringFormat3(LocalString.GetString("%s灵力/分钟"),  self:DealNum(LuaZongMenMgr.m_LingLiParams[1]))
    end

    if LuaZongMenMgr.m_RefineMonsterStatusTable["isOwner"] == 1 then
        self.m_LianGuai.text = SafeStringFormat3(LocalString.GetString("[00FF00]%s灵力/分钟"), self:DealNum(LuaZongMenMgr.m_LingLiParams[2]))
    else
        self.m_LianGuai.text = SafeStringFormat3(LocalString.GetString("%s灵力/分钟"), self:DealNum(LuaZongMenMgr.m_LingLiParams[2]))
    end

    self.m_ZongMen.text = SafeStringFormat3(LocalString.GetString("%s"), self:DealNum(LuaZongMenMgr.m_LingLiParams[3]))
    self.m_XiePai.text = SafeStringFormat3(LocalString.GetString("%s/分钟"), self:DealNum(LuaZongMenMgr.m_LingLiParams[4]))
    self.m_WuXing.text = tostring(LuaZongMenMgr.m_RefineMonsterStatusTable["wuxingFactor"])
    self.m_IsOwnerIcon:SetActive(LuaZongMenMgr.m_RefineMonsterStatusTable["isOwner"] == 1)

    -- 天气
    local weatherFactor = LuaZongMenMgr.m_RefineMonsterStatusTable["weatherFactor"]
    local weatherText = tostring(weatherFactor)
    local currentWeathers = ((CWeatherMgr.Inst or {}).m_CurrentWeather or {}).Weathers
    if currentWeathers ~= nil and currentWeathers.Length > 0 then
        local currentWeather = currentWeathers[0]
        local data = SoulCore_ZongPaiWeather.GetData(EnumToInt(currentWeather))
        if data then
            weatherText = SafeStringFormat3(LocalString.GetString("%s（%s）"), weatherText, data.Name)
        end
    end
    if weatherFactor <0 then
        weatherText = "[FF5050]" .. weatherText
    elseif weatherFactor >0 then
        weatherText = "[00FF00]" .. weatherText
    end
    self.m_Weather.text = weatherText

    -- 没有怪
    if LuaZongMenMgr.m_RefineMonsterStatusTable["templateId"] == nil or LuaZongMenMgr.m_RefineMonsterStatusTable["templateId"] == 0 then
        self.m_LianGuai.text = "-"
        self.m_IsOwnerIcon:SetActive(false)
    end

    -- 没有灵核
    if not LuaZongMenMgr.m_RefineMonsterStatusTable["isInputSoulCore"] then
        self.m_LingGen.text = "-"
        self.m_WuXing.text = "-"
        self.m_ZongMen.text = "-"
        self.m_XiePai.text = "-"
    end

    -- 灵符
    local lingfuDesc = "-"
    local id = LuaZongMenMgr.m_RefineMonsterStatusTable["lingfuFactor"]
    local lingfu = Menpai_LingfuLastTime.GetData(id).Factor
    lingfuDesc = tostring(self:DealNum(lingfu))
    
    -- 灵符
    self.m_LingFu.text = lingfuDesc
end