local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaYuanDan2023XinYuanBiDaWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2023XinYuanBiDaWnd, "TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaYuanDan2023XinYuanBiDaWnd, "Gift", "Gift", GameObject)
RegistChildComponent(LuaYuanDan2023XinYuanBiDaWnd, "ItemTexture", "ItemTexture", CUITexture)
RegistChildComponent(LuaYuanDan2023XinYuanBiDaWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaYuanDan2023XinYuanBiDaWnd, "Table", "Table", GameObject)
RegistChildComponent(LuaYuanDan2023XinYuanBiDaWnd, "yuandan2023_xinyuanitemtemplate", "yuandan2023_xinyuanitemtemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaYuanDan2023XinYuanBiDaWnd, "m_XinYuanList")
RegistClassMember(LuaYuanDan2023XinYuanBiDaWnd, "m_XinYuanState")
RegistClassMember(LuaYuanDan2023XinYuanBiDaWnd, "m_Select")
function LuaYuanDan2023XinYuanBiDaWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
    self.yuandan2023_xinyuanitemtemplate.gameObject:SetActive(false)
    
end

function LuaYuanDan2023XinYuanBiDaWnd:Init()
    self.Table.gameObject:SetActive(false)
    self.Gift.gameObject:SetActive(false)
    self.yuandan2023_xinyuanitemtemplate.gameObject:SetActive(false)
    self.m_XinYuanList = {}
    self.m_XinYuanState = {}
    Gac2Gas.QueryXinYuanBiDaStatusAll()

    -- self:InitXinYuan()
    -- self:InitAward()
end

function LuaYuanDan2023XinYuanBiDaWnd:SyncXinYuanStatus(XinYuanStateTable)
    self.m_XinYuanState = XinYuanStateTable
    self:InitXinYuan()
    self:InitAward()
end
function LuaYuanDan2023XinYuanBiDaWnd:InitXinYuan()
    self.Table.gameObject:SetActive(true)
    Extensions.RemoveAllChildren(self.Table.transform)
    self.m_Select = 0
    local childCount = YuanDan2023_XinYuan.GetDataCount()
    for i = 0,childCount - 1 do
        local obj = CUICommonDef.AddChild(self.Table.gameObject,self.yuandan2023_xinyuanitemtemplate.gameObject)
        self.m_XinYuanList[i + 1] = obj
        self:InitOneXinYuanItem(i + 1,obj)
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
end
-- 初始化奖励信息
function LuaYuanDan2023XinYuanBiDaWnd:InitAward()
    self.Gift.gameObject:SetActive(true)
    local AwardItemId = YuanDan2023_XinYuanBiDa.GetData().FinalAward
    local ItemData = Item_Item.GetData(AwardItemId)
    self:UpdateAwardTimes()
    if not ItemData then return end
    --self.ItemTexture:LoadMaterial(ItemData.Icon)
    CommonDefs.AddOnClickListener(
        self.ItemTexture.gameObject,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(AwardItemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

function LuaYuanDan2023XinYuanBiDaWnd:UpdateAwardTimes()
    local fullTimes = self.Table.transform.childCount
    local CompleteNum = 0
    for k,v in pairs(self.m_XinYuanState) do
        if v == EnumXinYuanState.Complete then
            CompleteNum = CompleteNum + 1
        end
    end
    self.Label.text = g_MessageMgr:FormatMessage("YuanDan2023_XinYuanBiDa_AwardTip",CompleteNum,fullTimes)
end
-- 初始化心愿信息
function LuaYuanDan2023XinYuanBiDaWnd:InitOneXinYuanItem(index,obj)
    if not obj then return end
    local data = YuanDan2023_XinYuan.GetData(index)
    if not data then obj.gameObject:SetActive(false) return end
    obj.gameObject:SetActive(true)
    obj.transform:Find("Deng/Top/Select").gameObject:SetActive(false)
    obj.transform:Find("Deng/Top/Bottom/Select").gameObject:SetActive(false)
    self:UpdateXinYuanState(index)    -- 更新状态
    local childNode = {"Highlight","UnLock","Deng"}
    for i=1,#childNode do
        local nameLabel = obj.transform:Find(String.Format("Deng/Top/Bottom/{0}/XinYuanNameLabel",childNode[i])):GetComponent(typeof(UILabel))
        nameLabel.text = LocalString.StrH2V(data.Name,false)
    end
    UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClickXinYuanItem(index)
	end)
end
-- 更新心愿的显示状态
function LuaYuanDan2023XinYuanBiDaWnd:UpdateXinYuanState(index)
    local obj = self.m_XinYuanList[index]
    local state = self.m_XinYuanState[index]
    if not obj then return end
    obj.transform:Find("Deng/Top/Deng").gameObject:SetActive(not state or state == EnumXinYuanState.Unlock)
    obj.transform:Find("Deng/Top/Highlight").gameObject:SetActive(state and state == EnumXinYuanState.Complete)
    obj.transform:Find("Deng/Top/UnLock").gameObject:SetActive(state and state == EnumXinYuanState.Receive)
    obj.transform:Find("Deng/Top/Bottom/Deng").gameObject:SetActive(not state or state == EnumXinYuanState.Unlock)
    obj.transform:Find("Deng/Top/Bottom/Highlight").gameObject:SetActive(state and state == EnumXinYuanState.Complete)
    obj.transform:Find("Deng/Top/Bottom/UnLock").gameObject:SetActive(state and state == EnumXinYuanState.Receive)
end
-- 心愿点击事件
function LuaYuanDan2023XinYuanBiDaWnd:OnClickXinYuanItem(index)
    local beforeSelected = self.m_XinYuanList[self.m_Select]
    if beforeSelected then
        beforeSelected.transform:Find("Deng/Top/Select").gameObject:SetActive(false)
        beforeSelected.transform:Find("Deng/Top/Bottom/Select").gameObject:SetActive(false)
    end
    self.m_Select = index
    local nowSelect = self.m_XinYuanList[index]
    nowSelect.transform:Find("Deng/Top/Select").gameObject:SetActive(true)
    nowSelect.transform:Find("Deng/Top/Bottom/Select").gameObject:SetActive(true)
    local data = YuanDan2023_XinYuan.GetData(index)
    local BtnText = YuanDan2023_XinYuanBiDa.GetData().XinYuanUnlockBtnText
    local state = self.m_XinYuanState[index]
    local ReplaceMesg = ""

    if state == EnumXinYuanState.Complete then
        ReplaceMesg = YuanDan2023_XinYuanBiDa.GetData().XinYuanCompleteDesc
    elseif state == EnumXinYuanState.Receive then
        ReplaceMesg = YuanDan2023_XinYuanBiDa.GetData().XinYuanProcessingDesc
    end
    local mesg = g_MessageMgr:FormatMessage(data.Describe,ReplaceMesg)
    if not mesg then return end
    if state == EnumXinYuanState.Unlock then
        LuaMessageTipMgr:ShowMessage(mesg,BtnText,function(...)
            -- 请求RPC
            Gac2Gas.RequestAcceptXinYuanBiDaWish(index)
            nowSelect.transform:Find("Deng/Top/Select").gameObject:SetActive(false)
            nowSelect.transform:Find("Deng/Top/Bottom/Select").gameObject:SetActive(false)
            self.m_Select = 0
        end)
    else
        CMessageTipMgr.Inst:Display(MessageMgr.Inst:GetMessageIdByName(data.Describe),mesg)
    end
end

function LuaYuanDan2023XinYuanBiDaWnd:UpdateXinYuan(id, state)
    self.m_XinYuanState[id] = state
    self:UpdateXinYuanState(id)
    self:UpdateAwardTimes()
end

--@region UIEvent
function LuaYuanDan2023XinYuanBiDaWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("YuanDan2023_XinYuanBiDa_Rule")
end
--@endregion UIEvent

function LuaYuanDan2023XinYuanBiDaWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateYuanDan2023XinYuanBiDaState",self,"SyncXinYuanStatus")
    g_ScriptEvent:AddListener("UpdateYuanDan2023OneXinYuanState",self,"UpdateXinYuan")
end

function LuaYuanDan2023XinYuanBiDaWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateYuanDan2023XinYuanBiDaState",self,"SyncXinYuanStatus")
    g_ScriptEvent:RemoveListener("UpdateYuanDan2023OneXinYuanState",self,"UpdateXinYuan")
end
