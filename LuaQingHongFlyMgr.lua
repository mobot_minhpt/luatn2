LuaQingHongFlyMgr = {}

LuaQingHongFlyMgr.m_FlyProgress = 0

--每次进副本第一次打开界面触发一次
LuaQingHongFlyMgr.m_TriggerGuide2=false
LuaQingHongFlyMgr.m_ShowBlink = false
LuaQingHongFlyMgr.m_FlyFail = false
function LuaQingHongFlyMgr.OnMainPlayerCreated()
	LuaQingHongFlyMgr.m_TriggerGuide2=false
end
function LuaQingHongFlyMgr.OnMainPlayerDestroyed()
	LuaQingHongFlyMgr.m_ShowBlink = false
end

function LuaQingHongFlyMgr.UpdateFlyProgress(progress)
	LuaQingHongFlyMgr.m_FlyProgress = progress
	if not CUIManager.IsLoaded(CLuaUIResources.QingHongFlyWnd) then
		CUIManager.ShowUI(CLuaUIResources.QingHongFlyWnd)
	else
		g_ScriptEvent:BroadcastInLua("UpdateQingHongFlyProgress")
	end
end

function LuaQingHongFlyMgr.OnLeave_QingHongFly()
	CUIManager.CloseUI(CLuaUIResources.QingHongFlyWnd)
end

function Gas2Gac.SyncQingHongFlyProgress(progress)
	LuaQingHongFlyMgr.UpdateFlyProgress(progress)
end

-- 飞行技能是否闪烁
function Gas2Gac.SyncQingHongFlySkillStatus(blink)
	LuaQingHongFlyMgr.m_ShowBlink = blink
	CLuaGuideMgr.TriggerQingHongFlySkillGuide(blink)
	g_ScriptEvent:BroadcastInLua("SyncQingHongFlySkillStatus", blink)
end

function Gas2Gac.OnPlayerQingHongFlyFail()
	LuaQingHongFlyMgr.m_TriggerGuide2=false
	LuaQingHongFlyMgr.m_FlyFail = true
	CLuaGuideMgr.TriggerQingHongFlySkillGuide(true)
end
