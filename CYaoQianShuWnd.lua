local CYaoQianShuWnd = import "L10.UI.CYaoQianShuWnd"
local CYaoYiYaoMgr = import "L10.Game.CYaoYiYaoMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"

CYaoQianShuWnd.m_hookInit = function(this) 
    local icon = this.transform:Find("Anchor/Icon")
    local bg = this.transform:Find("Anchor/Wnd_Bg_Box"):GetComponent(typeof(UIWidget))
    icon:Find("PC").gameObject:SetActive(CommonDefs.Is_PC_PLATFORM())
    icon:Find("Mobile").gameObject:SetActive(CommonDefs.IsInMobileDevice())

    this.textLabel.text = CommonDefs.IsInMobileDevice() and CYaoYiYaoMgr.Inst.m_DisplayText or CYaoYiYaoMgr.Inst.m_DisplayText_PC or CYaoYiYaoMgr.Inst.m_DisplayText
    this.textLabel:UpdateNGUIText()
    NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(NGUIText.StripSymbols(this.textLabel.text))
    if size.x > this.textLabel.width then 
        bg.height = 220
    else
        bg.height = 190
    end
    
    if this.cancelCountDownTick then
        invoke(this.cancelCountDownTick)
        this.cancelCountDownTick = nil
    end
    if CYaoYiYaoMgr.Inst.m_LastTimeBySecond > 0 then
        this.cancelCountDownTick = CTickMgr.Register(DelegateFactory.Action(function() 
            this:Close()
        end), CYaoYiYaoMgr.Inst.m_LastTimeBySecond * 1000, ETickType.Once)
    end
end
