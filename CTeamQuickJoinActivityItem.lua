-- Auto Generated!!
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamQuickJoinActivityItem = import "L10.UI.CTeamQuickJoinActivityItem"
local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
CTeamQuickJoinActivityItem.m_Init_CS2LuaHook = function (this, text, activityId, activityTypeId) 
    this.activityId = activityId
    this.activityTypeId = activityTypeId
    this.button.Text = text
    this.icon.enabled = false
    this:OnTeamQuickJoinMatchingTargetChanged()
end
CTeamQuickJoinActivityItem.m_OnTeamQuickJoinMatchingTargetChanged_CS2LuaHook = function (this) 
    if not CTeamMatchMgr.Inst.IsTeamMatching then
        this.icon.enabled = false
    else
        if this.activityId > 0 then
            this.icon.enabled = (CTeamMatchMgr.Inst.TeamQuickJoinActivityId == this.activityId)
        else
            local activity = TeamMatch_Activities.GetData(CTeamMatchMgr.Inst.TeamQuickJoinActivityId)
            this.icon.enabled = (activity ~= nil and this.activityTypeId > 0 and activity.Type == this.activityTypeId)
        end
    end
end
