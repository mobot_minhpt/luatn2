local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local QnTableItem = import "L10.UI.QnTableItem"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType2 = import "L10.UI.CTooltip+AlignType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local UISprite = import "UISprite"
local UILabel = import "UILabel"

CLuaTerrifyItemUseWnd = class()

RegistClassMember(CLuaTerrifyItemUseWnd, "m_TableView")
RegistClassMember(CLuaTerrifyItemUseWnd, "m_DataList")

function CLuaTerrifyItemUseWnd:Awake()
    self:InitComponents()
end

function CLuaTerrifyItemUseWnd:InitComponents()
    self.m_TableView = self.transform:Find("Anchor/TerrifyItems"):GetComponent(typeof(QnTableView))
end

function CLuaTerrifyItemUseWnd:Init()
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function ()
            return #self.m_DataList
        end,
        function (item, index)
            self:InitItem(item, self.m_DataList[index + 1])
        end
    )

    self:RefreshData()
end

function CLuaTerrifyItemUseWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "RefreshData")
    g_ScriptEvent:AddListener("SetItemAt", self, "RefreshData")
end

function CLuaTerrifyItemUseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "RefreshData")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshData")
end

function CLuaTerrifyItemUseWnd:InitDataList()
    self.m_DataList = {}
    
    Halloween2020_TerrifyProp.ForeachKey(function (key)
        local templateId = key
        local bagPos = 0
        local itemId = nil
        local default
        default, bagPos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, templateId)

        if default == true then
            local item = CItemMgr.Inst:GetById(itemId)
            if item ~= nil then
                local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
                if pos > 0 then
                    local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, pos)
                    local precious = CItemMgr.Inst:GetById(id)
                    local amount = precious.Amount

                    local info = {}
                    info.pos = pos
                    info.templateId = templateId
                    info.amount = amount
                    info.item = item
                    table.insert(self.m_DataList, info)
                end
            end
        else
            local item = CItemMgr.Inst:GetItemTemplate(templateId)

            local info = {}
            info.pos = 0
            info.templateId = templateId
            info.amount = 0
            info.item = item
            table.insert(self.m_DataList, info)
        end
    end)
end

function CLuaTerrifyItemUseWnd:RefreshData()
    self:InitDataList()
    self.m_TableView:ReloadData(true, false)
end

function CLuaTerrifyItemUseWnd:InitItem(item, info)
    local tableItem     = item.transform:GetComponent(typeof(QnTableItem))
    local longPressBtn  = item.transform:GetComponent(typeof(UILongPressButton))
    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local checkBox      = item.transform:Find("ClearupCheckbox"):GetComponent(typeof(QnCheckBox))
    local icon          = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local BindSprite    = item.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
    local AmountLabel   = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    local TextLabel     = item.transform:Find("TextLabel"):GetComponent(typeof(UILabel))
    local disableSprite = item.transform:Find("DisableSprite"):GetComponent(typeof(UISprite))

    icon.gameObject:SetActive(true)
    icon:LoadMaterial(info.item.Icon)
    checkBox.gameObject:SetActive(false)
    BindSprite.gameObject:SetActive(false)
    AmountLabel.gameObject:SetActive(true)

    if info.amount > 0 then
        if info.amount == 1 then
            AmountLabel.text = ""
        else
            AmountLabel.text = info.amount
        end
        disableSprite.gameObject:SetActive(false)
        TextLabel.gameObject:SetActive(false)

        longPressBtn.OnClickDelegate = DelegateFactory.Action(function ()
            --使用
            Gac2Gas.RequestUseItem(EnumItemPlace.Bag, info.pos, info.item.Id, "")
        end)
    else
        AmountLabel.text = ""
        disableSprite.gameObject:SetActive(true)
        TextLabel.gameObject:SetActive(true)

        longPressBtn.OnClickDelegate = DelegateFactory.Action(function ()
            --获取
            CItemAccessListMgr.Inst:ShowItemAccessInfo(info.templateId, false, item.transform, AlignType2.Left)
        end)
    end

    longPressBtn.OnLongPressDelegate = DelegateFactory.Action(function ()
        CItemInfoMgr.ShowLinkItemTemplateInfo(info.templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end
