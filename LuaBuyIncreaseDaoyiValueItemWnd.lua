local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaBuyIncreaseDaoyiValueItemWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBuyIncreaseDaoyiValueItemWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaBuyIncreaseDaoyiValueItemWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaBuyIncreaseDaoyiValueItemWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaBuyIncreaseDaoyiValueItemWnd, "OwnNumLabel", "OwnNumLabel", UILabel)
RegistChildComponent(LuaBuyIncreaseDaoyiValueItemWnd, "IconTexture", "IconTexture", GameObject)

--@endregion RegistChildComponent end

function LuaBuyIncreaseDaoyiValueItemWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


	
	UIEventListener.Get(self.IconTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIconTextureClick()
	end)


    --@endregion EventBind end
end

function LuaBuyIncreaseDaoyiValueItemWnd:Init()
	local maxCount = LuaInviteNpcMgr.RemainExchangeIncreaseDaoyiValueItemCount
	self.QnIncreseAndDecreaseButton:SetMinMax(1, maxCount > 0 and maxCount or 1, 1)
	self.OwnNumLabel.text = SectInviteNpc_Setting.GetData().AddDaoyiItemCost
	self.ReadMeLabel.text = SafeStringFormat3(LocalString.GetString("本周可兑换%s"), maxCount)
end

--@region UIEvent

function LuaBuyIncreaseDaoyiValueItemWnd:OnOkButtonClick()
	Gac2Gas.RequestExchangeLiuHeYeJing(self.QnIncreseAndDecreaseButton:GetValue())
end

function LuaBuyIncreaseDaoyiValueItemWnd:OnIconTextureClick()
	local itemId = SectInviteNpc_Setting.GetData().AddDaoyiItemID
	CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
end


--@endregion UIEvent

