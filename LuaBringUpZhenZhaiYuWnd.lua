local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local QnButton = import "L10.UI.QnButton"

LuaBringUpZhenZhaiYuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "Fish", "Fish", GameObject)
RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "ShuXingLabel", "ShuXingLabel", UILabel)
RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "ShuXingLimitLabel", "ShuXingLimitLabel", UILabel)
RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "Fabao", "Fabao", GameObject)
RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "FabaoIcon", "FabaoIcon", CUITexture)
RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "AddSprite", "AddSprite", GameObject)
RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "BringUpBtn", "BringUpBtn", GameObject)
RegistChildComponent(LuaBringUpZhenZhaiYuWnd, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "m_SelectFabaoStringId")
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "m_FishWordId")
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "m_FabaoNameLabel")
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "m_FabaoBorder")
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "m_ChengGongLvLabel")
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "m_ResultLabel")
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "m_SelectFabaoBagPos")
RegistClassMember(LuaBringUpZhenZhaiYuWnd, "HintLabel")

function LuaBringUpZhenZhaiYuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BringUpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBringUpBtnClick()
	end)


	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
	self.m_FabaoNameLabel = self.FabaoIcon.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.m_FabaoBorder = self.FabaoIcon.transform:Find("Border"):GetComponent(typeof(UISprite))
	self.m_ChengGongLvLabel = self.FabaoIcon.transform:Find("ChengGongLvLabel"):GetComponent(typeof(UILabel))
	self.m_ResultLabel = self.FabaoIcon.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))
	self.HintLabel = self.AddSprite.transform:Find("HintLabel"):GetComponent(typeof(UILabel))
end

function LuaBringUpZhenZhaiYuWnd:OnEnable()
    g_ScriptEvent:AddListener("SelectedGongFongTalisman",self,"OnSelectedGongFongTalisman")
	g_ScriptEvent:AddListener("ImproveZhengzhaiFishResult",self,"OnImproveZhengzhaiFishResult")
end

function LuaBringUpZhenZhaiYuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SelectedGongFongTalisman",self,"OnSelectedGongFongTalisman")
	g_ScriptEvent:RemoveListener("ImproveZhengzhaiFishResult",self,"OnImproveZhengzhaiFishResult")
end

function LuaBringUpZhenZhaiYuWnd:Init()
	--fish
	local data = Item_Item.GetData(LuaZhenZhaiYuMgr.m_FeedFishItemId)
	if not data then return end
	local fishIcon = self.Fish.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local fishNameLabel = self.Fish.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local fishLevelLabel = self.Fish.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local border = self.Fish.transform:Find("Border"):GetComponent(typeof(UISprite))
	fishIcon:LoadMaterial(data.Icon)
	fishNameLabel.text = data.Name
	local fishData = HouseFish_AllFishes.GetData(LuaZhenZhaiYuMgr.m_FeedFishItemId)
	fishLevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),fishData.Level)
	border.spriteName = CUICommonDef.GetItemCellBorder(data.NameColor)

	UIEventListener.Get(self.Fabao).onClick = DelegateFactory.VoidDelegate(function (go)
		LuaZhenZhaiYuMgr.OpenChooseFabaoWnd(LuaZhenZhaiYuMgr.m_FeedFishItemId)
	end)

	--init label
	local info = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo
	self.m_FishWordId = info.wordId
	local fishWordMaxLevel = HouseFish_Setting.GetData().FishWordMaxLevel
	if info.wordId and info.wordId ~= 0 and Word_Word.GetData(info.wordId) then
		self.ShuXingLabel.text = Word_Word.GetData(info.wordId).Description
		local maxWordLevel = fishWordMaxLevel[fishData.Level-1]
		local maxWord = math.floor(info.wordId/100)*100 + maxWordLevel
		self.ShuXingLimitLabel.text = Word_Word.GetData(maxWord).Description

		if maxWordLevel <= info.wordId%100 then
			self:InitHintLabel(true)
		else
			self:InitHintLabel()
		end
	else
		self.ShuXingLabel.text = SafeStringFormat3(LocalString.GetString("暂无"))
		self.ShuXingLimitLabel.text = SafeStringFormat3(LocalString.GetString("暂无"))
		self:InitHintLabel()
	end
end

function LuaBringUpZhenZhaiYuWnd:InitFabao()
	self:InitHintLabel()

	if self.m_SelectFabaoStringId then
		local citem = CItemMgr.Inst:GetById(self.m_SelectFabaoStringId)
		--local data = EquipmentTemplate_Equip.GetData(self.m_SelectFabaoStringId)
		if citem then
			self.FabaoIcon:LoadMaterial(citem.Icon)
			self.m_FabaoNameLabel.text = citem.Name
			self.m_FabaoBorder.spriteName = CUICommonDef.GetItemCellBorder(citem.Equip.QualityType)
		end
	end
	self:RefreshResultLabel()
end

function LuaBringUpZhenZhaiYuWnd:InitHintLabel(isUptoLimit)
	if isUptoLimit then
		self.FabaoIcon.gameObject:SetActive(false)
		self.AddSprite:SetActive(true)
	else
		self.FabaoIcon.gameObject:SetActive(self.m_SelectFabaoStringId ~= nil)
		self.AddSprite:SetActive(self.m_SelectFabaoStringId == nil)
	end

	local cbtn = self.BringUpBtn.transform:GetComponent(typeof(QnButton))
	if isUptoLimit then
		self.HintLabel.text = LocalString.GetString("已满级,无需继续供奉")
		cbtn.Enabled = false
		UIEventListener.Get(self.Fabao).onClick = nil
	else
		if self.m_FishWordId and self.m_FishWordId ~= 0 then
			self.HintLabel.text = LocalString.GetString("供奉与佑宅福鱼属性相同的法宝")
		else
			local str = SafeStringFormat3(LocalString.GetString("供奉法宝赋予佑宅福鱼属性#r(每只佑宅福鱼只能有一种属性)"))
			self.HintLabel.text = CChatLinkMgr.TranslateToNGUIText(str,false)
		end
		cbtn.Enabled = true
	end
end

function LuaBringUpZhenZhaiYuWnd:RefreshResultLabel()
	if self.m_SelectFabaoStringId == nil then
		self.m_ChengGongLvLabel.text = nil
		self.m_ResultLabel.text = nil
		return
	end

	local citem = CItemMgr.Inst:GetById(self.m_SelectFabaoStringId)
	if not citem then return end

	local fabaoLevel = citem.Grade
	local wordLevel = self.m_FishWordId % 100

	local formulaId = HouseFish_Setting.GetData().ZhengzhaiFishImproveFormula
	local formula = AllFormulas.Action_Formula[formulaId].Formula
	local result
    if formula ~= nil then
        result = formula(nil, nil, {fabaoLevel,wordLevel})
		local chenggonglv = result[1] * 100
		local addLevel = result[2]

		self.m_ChengGongLvLabel.text = SafeStringFormat3("%d%%",chenggonglv)
		local newWordId
		if self.m_FishWordId and self.m_FishWordId ~= 0 then
			newWordId = self.m_FishWordId + addLevel
			local desc = Word_Word.GetData(newWordId).Description
			local olddesc = Word_Word.GetData(self.m_FishWordId).Description
			local lcPos = string.find(desc,"+")
			local attstring=""
			local newValue = 0
			local oldValue = 0
			if lcPos then
				attstring = string.sub(desc,1,lcPos-1)
				newValue = string.sub(desc,lcPos+1)
				newValue = tonumber(newValue)
				oldValue = string.sub(olddesc,lcPos+1)
				oldValue = tonumber(oldValue)
			end
			self.m_ResultLabel.text = SafeStringFormat3("%s+%d",attstring,newValue-oldValue)
		else
			local fabaoType = EquipmentTemplate_Equip.GetData(citem.TemplateId).TalismanType
			local data = Talisman_SuitConsist.GetData(fabaoType)
			local suit = Talisman_Suit.GetData(data.ActivateSuits[0].SuitId)

			do
				local i = 0 local cnt = suit.Words.Length
				while i < cnt do
					local wordId = suit.Words[i]
					local cls = math.floor(wordId/100)
					local fishWordCls = HouseFish_WordMatch.GetData(cls).FishWord
					newWordId = fishWordCls * 100 + addLevel	
					i = i + 1
				end
			end
			self.m_ResultLabel.text = Word_Word.GetData(newWordId).Description
		end
		-- self.m_ResultLabel.text = Word_Word.GetData(newWordId).Description
		return
    end
	self.m_ChengGongLvLabel.text = nil
	self.m_ResultLabel.text = nil
end

--@region UIEvent

function LuaBringUpZhenZhaiYuWnd:OnBringUpBtnClick()
	local fish = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo--.itemId
	local fishId = fish.fishmapId--fish.itemId
	local fishWord = fish.wordId

	local itemId = self.m_SelectFabaoStringId
	local pos = self.m_SelectFabaoBagPos
	local place = EnumItemPlace.Bag
	if not itemId or not pos then
		return
	end
	if not fishWord or fishWord== 0 then
		-- print("RequestFeedZhengzhaiFish",fishId, place, pos, itemId)
		Gac2Gas.RequestFeedZhengzhaiFish(fishId, place, pos, itemId)--"IIIs", 1000, nil, "lua"}, -- fishId, place, pos, itemId
	else
		-- print("RequestImproveZhengzhaiFish",fishId, place, pos, itemId)
		Gac2Gas.RequestImproveZhengzhaiFish(fishId, place, pos, itemId)  --这个是给鱼的词条升级
	end
end

function LuaBringUpZhenZhaiYuWnd:OnSelectedGongFongTalisman(talismanId,bagPos)
	self.m_SelectFabaoStringId = talismanId
	self.m_SelectFabaoBagPos = bagPos
	self:InitFabao()
end

function LuaBringUpZhenZhaiYuWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("Feed_Zhenzhaiyu_With_Talisman_Tip")
end

function LuaBringUpZhenZhaiYuWnd:OnImproveZhengzhaiFishResult(fishId,isSucceed)
	CUIManager.CloseUI(CLuaUIResources.BringUpZhenZhaiYuWnd)
end

--@endregion UIEvent

