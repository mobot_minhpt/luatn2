return
{
	{ "RequestEnterGame", "ss" },
	{ "UpdateGatewayInfo", "sI" },
	{ "RequestReconnectGame", "sss" },
	{ "RequestDisconnectDone", "I" },
	{ "UpdatePlayerReconnectTicketDone", "ss" },
	{ "TeleportRequestEnterGame", "ds" },
	{ "ReportGatewayException", "ds" },
	{ "RequestCastSkill", "IIII" },
	{ "RequestSetTarget", "I" },
	{ "RequestReborn", "I" },
	{ "RequestPickItem", "I" },
	{ "RequestUseItem", "IIss" },
	{ "RequestBatchUseItem", "IIsIs" },
	{ "RequestEquipItem", "IIs" },
	{ "QueryItemInfo", "s" },
	{ "GlobalQueryItemInfo", "s" },
	{ "RequestInteractWithNpc", "II" },
	{ "SelectDialogChoice", "bI" },
	{ "EndConversation", "" },
	{ "RequestAutoMode", "I" },
	{ "RequestCancelAutoMode", "I" },
	{ "RequestRunServerTestScript", "s" },
	{ "RequestDiscardItem", "IIs" },
	{ "RequestPick", "I" },
	{ "RequestTemple", "I" },
	{ "SelectNpcDialogChoice", "IIII" },
	{ "RequestTeleportByNpcDialog", "IisI" },
	{ "UseBaolingdan", "sIIb" },
	{ "ConfirmBangHuiShangPiaoDone", "" },
	{ "RequestRename", "s" },
	{ "SendOtherClientInfo", "bbs" },
	{ "SendHuaBiConfirmInfo", "b" },
	{ "ConfirmHuaBiUseSilver", "" },
	{ "BuyMallItem", "iII" },
	{ "SellMarketItem", "II" },
	{ "RequestMarketItems", "" },
	{ "RequestMarketItem", "I" },
	{ "RequestMoneyInfo", "" },
	{ "RequestPlayerItemLimit", "i" },
	{ "RequestExchangeJade2YuanBao", "I" },
	{ "PreSellMarketItem", "II" },
	{ "RequestAutoShangJiaItem", "" },
	{ "GiftMallItem", "diIIs" },
	{ "QueryMonthPresentLimit", "" },
	{ "QueryShopActivityInfo", "I" },
	{ "QueryAvailableShopActivityIds", "" },
	{ "RequestGetShopActivityGift", "II" },
	{ "RequestCheckOrder", "sssssssssIssb", 1000 },
	{ "RequestQueryOrder", "sssssssssIss" },
	{ "SendQueryOrderSubReceiptDataBegin", "" },
	{ "SendQueryOrderSubReceiptData", "s" },
	{ "RequestGetFirstChargeGift", "" },
	{ "RequestGetAdditionalFirstChargeGift", "" },
	{ "RequestGetVipLevelGift", "I" },
	{ "RequestGetMonthCardReturn", "" },
	{ "SendRemoveCheckedOrderDone", "ss" },
	{ "LogFailedOrderInfo", "sssss", 1000 },
	{ "LogEnterBackgroundInPurchaseAction", "s", 1000 },
	{ "PlayGameVideoDone", "s" },
	{ "ApplyCreateGuild", "ss" },
	{ "ApplyCreateGuildEnd", "ss" },
	{ "RequestGetGuildInfo", "s" },
	{ "RequestOperationInGuild", "dsss" },
	{ "RequestAlterGuildMission", "ds" },
	{ "RequestAlterGuildMissionEnd", "ds" },
	{ "QueryPlayerGuildInfo", "i" },
	{ "RequestInvitePlayerAsGuildMember", "d" },
	{ "ConfirmInvitedAsGuildMember", "d" },
	{ "RequestMergeGuildTo", "d" },
	{ "RejectMergeGuild", "d" },
	{ "AcceptMergeGuild", "d" },
	{ "RequestSetGuildRights", "u" },
	{ "GC_RequestOpenAllGuildInfoWnd", "" },
	{ "GC_RequestOpenChallengeGuildWnd", "d" },
	{ "GC_RequestChallenge", "di" },
	{ "GC_RejectChallenge", "d" },
	{ "GC_AcceptChallenge", "d" },
	{ "GC_RequestOpenFightDetailWnd", "" },
	{ "RequestUpdateServerTime", "" },
	{ "ConfirmTeleportPublicCopy", "Is" },
	{ "RequestTeleportPublicCopy", "" },
	{ "RequestMapFullName", "" },
	{ "RequestPushInfoDone", "s" },
	{ "RequestPushInfoDoneEnd", "s" },
	{ "SetPushNotification", "Ib" },
	{ "SendJingLingLogInfo", "ii", 1000 },
	{ "TaskRequestCrossMap", "IIiiss", 1000 },
	{ "RequestGiveUpTask", "I" },
	{ "RequestSyncTaskTimeCheck", "" },
	{ "QueryPlayerInfo", "d" },
	{ "QueryLingShouInfo", "dsi" },
	{ "QueryAllPlayerLingShou", "di" },
	{ "QueryMapiInfo", "ds" },
	{ "QueryAllPlayerMapi", "d" },
	{ "QueryBabyInfo", "ds" },
	{ "QueryRank", "I" },
	{ "RequestCreateTeam", "" },
	{ "RequestDelTeamMember", "d" },
	{ "RequestTeamDepart", "" },
	{ "RequestJoinTeam", "d" },
	{ "InvitePlayerJoinTeam", "d", 200 },
	{ "RefuseInvitePlayerJoinTeam", "d" },
	{ "AcceptInvitePlayerJoinTeam", "d" },
	{ "LeaderAcceptJoinTeam", "dds" },
	{ "LeaderRefuseJoinTeam", "dds" },
	{ "SetTeamLeader", "d" },
	{ "RequestTeamFollow", "" },
	{ "CancelTeamFollow", "" },
	{ "QueryMyTeamLeaderPos", "" },
	{ "ClearTeamJoinRequest", "d" },
	{ "CheckIsPlayerInTeam", "d" },
	{ "GetPlayersNearby", "i" },
	{ "SummonTeammate", "", 1000 },
	{ "QueryTeamSize", "d" },
	{ "RejectTeamFollow", "" },
	{ "SetAutoAcceptTeamFollowRequest", "b" },
	{ "GetTeamMachingActivity", "" },
	{ "QueryTeamsInSameScene", "II" },
	{ "RequestChangeLeader", "" },
	{ "AgreeChangeLeader", "d" },
	{ "RejectChangeLeader", "d" },
	{ "SummonTeammateById", "d" },
	{ "PlayerRequestStartGuaJi", "" },
	{ "PlayerRequestStopGuaJi", "" },
	{ "SyncGuaJiState", "u" },
	{ "GuaJiSwitchToBackground", "b" },
	{ "SkillReliveSelf", "" },
	{ "BuffReliveSelf", "" },
	{ "RequestRelivePartner_v2", "" },
	{ "SetStatusWithSync", "ii" },
	{ "RequestTakeOffEquip", "IIs" },
	{ "RequestUpgradeNormalSkill", "Iu" },
	{ "RequestUpgradeTianFuSkill", "u" },
	{ "RequestSaveSkill2Template", "Isuuu" },
	{ "RequestSwitchSkillTemplate", "I" },
	{ "ChangeCurrentActiveSkill", "II" },
	{ "ChangeCurrentActiveSkill2", "II" },
	{ "RequestSuggestSkill", "I" },
	{ "SetActiveSkillForAI", "IuuI" },
	{ "SwitchActiveSkillSuite", "I" },
	{ "RequestLearnNew110Skill", "I" },
	{ "RequestSetUseDoubleJoystickSkill", "Ib" },
	{ "AddFriend", "ds" },
	{ "DelFriend", "d" },
	{ "AddBlacklist", "d" },
	{ "DelBlacklist", "d" },
	{ "DelEnemy", "d" },
	{ "DelRecent", "d" },
	{ "DelInteract", "d" },
	{ "AddFriendSpecialId", "d" },
	{ "DelFriendSpecialId", "d" },
	{ "QueryFriendliness", "d" },
	{ "QueryFriendlinessTbl", "" },
	{ "DelFriendRequest", "d" },
	{ "ClearFriendRequest", "" },
	{ "SetFriendRequestlistRead", "" },
	{ "AdjustFriendGroup", "di" },
	{ "AdjustFriendGroupWithMultiPlayer", "ui" },
	{ "SetFriendGroupName", "is" },
	{ "QueryFriendsNoGuild", "", 1000 },
	{ "DelMultiFriend", "uuuu" },
	{ "SendIMMsg", "dbbss", 300 },
	{ "SearchOnlinePlayerById", "d" },
	{ "SearchOnlinePlayerByName", "s" },
	{ "SendChatMessage", "Isbbbsssss", 1000, "CHAT_TOO_FREQUENT" },
	{ "FetchIMMessage", "d" },
	{ "UseLaba", "IsIIbuss" },
	{ "BroadcastVoiceTranslation", "Issdss", 500 },
	{ "LogVoiceTranslationOnIM", "dsdss", 500 },
	{ "BroadcastIMMessagesToFriends", "ubbss", 500 },
	{ "SetBroadcastSetting", "i" },
	{ "QueryOnlineReverseFriends", "I" },
	{ "VoiceControlTestLog", "IIss" },
	{ "QueryMyGmId", "" },
	{ "RequestForbidGuildSpeak", "d" },
	{ "RequestEnableGuildSpeak", "d" },
	{ "QueryGuildForbidSpeak", "" },
	{ "SetBroadcastSettingByGroup", "ii" },
	{ "AtPlayerInChannel", "Isuss" },
	{ "HandleMail", "ss" },
	{ "RequestDelMail", "s" },
	{ "GetMailBaseReward", "I" },
	{ "QueryMailContent", "I" },
	{ "RequestSyncMailContainer", "" },
	{ "SetMailRead", "si" },
	{ "SetAnnoucementMailRead", "I" },
	{ "RequestBatchDelMailBegin", "" },
	{ "RequestBatchDelMail", "s" },
	{ "RequestBatchDelMailEnd", "" },
	{ "RequestCanIntensifyEquipment", "iiiibbb" },
	{ "RequestIntensifyEquipment", "iiiibbb" },
	{ "GetEquipGradeByIntensifyLevel", "iisi" },
	{ "RequestIntensifySwitch", "IIsIIs" },
	{ "RequestSetPlayerPKMode", "I" },
	{ "RequestSetPKProtect", "II" },
	{ "RequestPersonalChallenge", "d" },
	{ "RequestCheckUseCangBaoTu", "IIs" },
	{ "QueryOpenedGumuInfo", "" },
	{ "RequestEnterGumu", "I" },
	{ "RequestJoinQifuShoucai", "I" },
	{ "RequestAllLingShouOverview", "i" },
	{ "RequestLingShouDetails", "si" },
	{ "RequestSummonLingShou", "s" },
	{ "RequestSummonLingShouBaby", "s" },
	{ "RequestPackLingShou", "" },
	{ "RequestPackLingShouBaby", "s" },
	{ "RequestFeedLingShou", "siis" },
	{ "RequestWashLingShou", "siisb" },
	{ "RequestLastLingShouWashResult", "s" },
	{ "RequestImproveLingShouWuxing", "siisb" },
	{ "LingShouRequestLearnNewSkill", "sIIsIb" },
	{ "RequestUpgradeLingShouSkill", "sIIIsb" },
	{ "RequestLingShouLianhua", "s" },
	{ "AddNewLingShouMaxNumber", "" },
	{ "RequestRenameLingShou", "ss" },
	{ "AcceptWashedLingShou", "s" },
	{ "GetLingShouPixiang", "sIIs" },
	{ "SetLingShouPixiang", "sIIs" },
	{ "PlayerSelectLingShou", "IIsI" },
	{ "RequestSelectLingShou", "IIs" },
	{ "RequestSetAssistLingShou", "s" },
	{ "RequestSetSkillSlotLockState", "sI" },
	{ "ConfirmClearLingShouPixiang", "" },
	{ "LingShouYuanShengLianHua", "IIs" },
	{ "RequestJiuZhuanQianKun", "IIsIsIIs" },
	{ "RequestYiXingHuanDou", "IIsIsIsi" },
	{ "RequestWashLingShouBaby", "siisb" },
	{ "AccpetLastLingShouWashBabyResult", "s" },
	{ "RequestFangShengLingShouBaby", "s" },
	{ "RequestLingShouBabyUseExpItem", "sIIs" },
	{ "RequestLingShouBabyTakeParentExp", "s" },
	{ "PermitLingShouMarry", "" },
	{ "RefuseLingShouMarry", "" },
	{ "RequestUpgradeBabyLingShouSkill", "sIb" },
	{ "RequestLingShouHeBaZi", "sds" },
	{ "RequestEngageWithTargetLingShou", "sds" },
	{ "AcceptLingShouEngageRequest", "sds" },
	{ "RequestBroadcastBaZiInvitation", "sIs" },
	{ "RequestSwitchFuTiLingShou", "s" },
	{ "RequestInteractMenuData", "di" },
	{ "QueryInteractMenuData", "di" },
	{ "GetAppearancePropById", "dI" },
	{ "SendConfirmMessageResult", "sI" },
	{ "SendGamePlayConfirmMessageResult", "sIu" },
	{ "RequestBaptizeEquipmentProperty", "iisbb" },
	{ "RequestBaptizeEquipmentWord", "iisbb" },
	{ "BaptizeWordResultConfirm", "iisb" },
	{ "RequestAutoBaptizeWord", "iisssssbb" },
	{ "TaskSubmitItem", "dIii" },
	{ "RequestLevelUp", "" },
	{ "AcceptEnterJuQing", "" },
	{ "RejectEnterJuQing", "" },
	{ "QueryJuQingWaitInfo", "" },
	{ "SetHpDrugShortcut", "I" },
	{ "BuyNpcShopItem", "dIIi" },
	{ "RequestAccuse", "dsIssdssssssssIsdddd", 1000 },
	{ "RequestStartTrade", "dI" },
	{ "ConfirmStartTrade", "dI" },
	{ "RefuseStartTrade", "d" },
	{ "MoveSilverToTradeShelf", "d" },
	{ "MoveItemToTradeShelf", "iisI" },
	{ "MoveItemToTradeBag", "is" },
	{ "LockTradeShelf", "" },
	{ "UnlockTradeShelf", "" },
	{ "ConfirmTrade", "b" },
	{ "CancelTrade", "" },
	{ "QueryTradeItem", "s" },
	{ "RequestStartGift", "d" },
	{ "ConfirmStartGift", "d" },
	{ "RequestGiftLimit", "" },
	{ "GetNeedGiftLimit", "dIsi" },
	{ "RequestGiftLimitOnShelf", "" },
	{ "QueryLifeSkillNeedInfo", "" },
	{ "RequestUseLifeSkill", "IIi" },
	{ "UpgradeLifeSkill", "I", 300 },
	{ "MakeHoleOnEquipment", "iis" },
	{ "RequestCanInlayStoneOnEquipment", "iisiisi" },
	{ "InlayStoneOnEquipment", "iisiisi" },
	{ "RemoveStoneOnEquipment", "iisii" },
	{ "RequestCompositeStone", "iis" },
	{ "SetGameSettingInfo", "Is" },
	{ "RequestSignUpGnjc", "I" },
	{ "RequestCancelSignUpGnjc", "I" },
	{ "QueryGnjcSignUpInfo", "" },
	{ "RequestGetDoubleScore", "" },
	{ "RequestExchangeYuanBao", "I" },
	{ "QueryGnjcPlayInfo", "" },
	{ "RequestSignUpGnjcFromGuanNingXunLianWin", "I" },
	{ "RequestEnterGuanNingXunLian", "" },
	{ "RequestCrossGnjcPlayerServerName", "" },
	{ "QueryLastGuanNingAwardInfo", "" },
	{ "RequestCompositeWuShanShi", "iisii" },
	{ "QueryDayActivityInfo", "" },
	{ "QueryWeeklyInfo", "" },
	{ "PackageOrder", "i" },
	{ "SetAutoPickItem", "s" },
	{ "SetAutoPickEquip", "s" },
	{ "SetAutoPickTalisman", "s" },
	{ "SetAutoAttackOnSelect", "I" },
	{ "RequestLeavePlay", "" },
	{ "QueryJuQingCount", "" },
	{ "RequestEnterJuQing", "Ib" },
	{ "QueryWuHuoQiQinCount", "" },
	{ "GetOnlineFriendsNotInTeam", "i" },
	{ "RequestPetDetails", "I" },
	{ "SetPetBehaviorType", "I" },
	{ "RequestKillAllMyPets", "" },
	{ "RequestStartQieCuo", "d" },
	{ "RequestRepairEquip", "II" },
	{ "AutoRequestRepairEquip", "II" },
	{ "QuerySceneInfo", "I" },
	{ "LevelTeleport", "I" },
	{ "QueryMMapPosInfo", "" },
	{ "PlayerRequestSubmitTaskWithoutSubmitNPC", "I" },
	{ "TaskTrackToGuildCity", "II" },
	{ "TrackToGuildCity", "" },
	{ "RequestTeleportToSameTemplateScene", "s" },
	{ "RequestLogClientHardwareInfo", "ssII" },
	{ "HeartBeat", "dId" },
	{ "QueryQianDaoInfo", "" },
	{ "SignUpForQianDao", "" },
	{ "SignUpForQianDaoWithExtraTimes", "" },
	{ "QueryTeamMemberBuff", "I" },
	{ "QueryTargetBuff", "I", 300 },
	{ "OnClientGuideEvent", "i" },
	{ "QueryPlayerCapacity", "d" },
	{ "RequestShowFishAction", "III" },
	{ "OpenPlayerShopWindow", "" },
	{ "RequestOpenPlayerShop", "s", 100 },
	{ "MaintainPlayerShop", "" },
	{ "PlayerShopOnShelf", "dIIII" },
	{ "PlayerShopOffShelf", "dI" },
	{ "AddPlayerShopFund", "dI" },
	{ "BuyItemFromPlayerShop", "dIsII" },
	{ "ChangePlayerShopDesc", "ds" },
	{ "ChangePlayerShopItemPrice", "dII" },
	{ "ChangePlayerShopName", "ds" },
	{ "GetPlayerShopFund", "dd" },
	{ "QueryCounterInfo", "d" },
	{ "QueryPlayerShopList", "II" },
	{ "QueryTradeRecords", "dI" },
	{ "RequestAddPlayerShopCounterPos", "dI" },
	{ "CancelPlayerShopBankrupt", "dII" },
	{ "CollectPlayerShopItem", "dIs" },
	{ "CancelCollectPlayerShopItem", "dIs" },
	{ "QueryPlayerShopSearchedItems", "IIIIbI" },
	{ "QueryPlayerShopItemCollectedCount", "dIs" },
	{ "QueryPlayerShopItemCollections", "b" },
	{ "QueryPlayerShopOnShelfPrice", "I" },
	{ "QueryPlayerShopRecommendPrice", "I" },
	{ "QueryPlayerShopOnShelfNum", "uuubIb" },
	{ "QueryPlayerShopFrozenFund", "" },
	{ "QueryPlayerShopPubliclyItems", "IIb", 1000 },
	{ "RequestQuickBuyItemFromPlayerShop", "IIb" },
	{ "QueryPlayerShopSearchedByWords", "uIIIIIIb" },
	{ "SavePlayershopTemplateFocus", "u" },
	{ "RequestChallengeMPTZ", "db" },
	{ "QueryChallengeRecord", "" },
	{ "MPTZQueryPlayerTarget", "" },
	{ "RequestSpeedupMPTZCooldown", "" },
	{ "QueryMPTZRankInfo", "iI" },
	{ "SetMPTZAISkill", "II" },
	{ "UpdateFreightBoxStatus", "diis", 500 },
	{ "GetCurrentFreightItems", "d" },
	{ "GetNextFreightItems", "" },
	{ "QueryPlayerName", "di" },
	{ "RequestBroadcastFreightHelp", "i", 1000 },
	{ "QueryGuildFreightGiveHelpCount", "" },
	{ "BeginTaskExecute", "I" },
	{ "RequestLogClientCutSceneInfo", "sds" },
	{ "OpenShop", "I" },
	{ "QueryTaskHuoLiTimes", "" },
	{ "AskForTeamMatching", "iiib", 1000 },
	{ "AskForLeaveMatching", "", 1000 },
	{ "GetMatchingTeamInfos", "iiib" },
	{ "BroadcastAddTeamRequestMsg", "isiiibb", 5000 },
	{ "RequestJoinMatchingTeam", "i" },
	{ "ShakeTree", "I" },
	{ "RequestIncBagSize", "I" },
	{ "RequestCanUpgradeEquipStone", "iisiii" },
	{ "RequestUpgradeEquipStone", "iisiii" },
	{ "RequestCanRemoveStoneOnEquipment", "iisii" },
	{ "RequestJoinXiangShi", "" },
	{ "SubmitXiangShiAnswer", "I" },
	{ "SubmitHuiShiAnswer", "II" },
	{ "SendKeJuDanMu", "sb" },
	{ "RequestWatchKeJu", "" },
	{ "RequestStopWatchKeJu", "" },
	{ "SubmitDianShiAnswer", "II" },
	{ "SubmitDianShiGamblingPlayerId", "u" },
	{ "Practice", "I", 300 },
	{ "PracticeToNextLevel", "I", 300 },
	{ "TryPracticeToNextLevel", "I" },
	{ "QueryGuildPracticeAvailable", "" },
	{ "SetGuildPracticeAvailable", "s" },
	{ "RequestOpenRepertory", "" },
	{ "CloseRepertoryWnd", "" },
	{ "OpenNewRepertory", "" },
	{ "MoveItemToRepertory", "IsI" },
	{ "MoveItemToBag", "Is" },
	{ "RepertoryReorder", "" },
	{ "RequestIncRiderExtraBagSize", "I" },
	{ "RequestGetOnlineTimeAward", "I" },
	{ "QueryOnlineTimeAward", "" },
	{ "RequestGetDaysAward", "I" },
	{ "RequestGetLevelUpAward", "I" },
	{ "RequestSubmitEquipGetMFBegin", "" },
	{ "RequestSubmitEquipGetMF", "IIs" },
	{ "RequestSubmitEquipGetMFEnd", "" },
	{ "RequestCompositeJinGangZuan", "iisii" },
	{ "ReportGuideProcess", "Ii" },
	{ "RequestSetTitle", "II" },
	{ "RequestCheckTitle", "" },
	{ "PlayerGetAchievementAward", "Is" },
	{ "QueryOtherPlayerAchievementInfo", "dI", 1000 },
	{ "QueryGuildLeagueInfo", "" },
	{ "QueryGuildLeagueMainInfo", "" },
	{ "QueryGuildLeagueViceInfo", "" },
	{ "QueryGLLevelInfo", "I" },
	{ "QueryGLMatchInfo", "I" },
	{ "RequestDisassembleEquip", "IIsbb" },
	{ "RequestChouBing", "IIsb" },
	{ "RequestResetWord", "IIs" },
	{ "RequestBindItem", "IIs" },
	{ "RequestSplitItem", "IIsI" },
	{ "QueryZhouMoActivityCHJPInfo", "" },
	{ "RequestSubmitZhouMoActivityCHJP", "I" },
	{ "RequestEnterZhouMoActivity", "I", 1000 },
	{ "QueryZhouMoActivityRankInfo", "", 2000 },
	{ "QueryZhouMoActivityBossInfo", "" },
	{ "PutOnSuitFx", "i" },
	{ "HideGemGroupFx", "b" },
	{ "RequestTakeOnFashion", "d" },
	{ "RequestTakeOffFashion", "d" },
	{ "RequestDiscardFashion", "d" },
	{ "RequestCheckFashion", "" },
	{ "RequestSetFashionHide", "II" },
	{ "RequestCompositeTaben", "IIb" },
	{ "RequestRenewMoreFashionBegin", "" },
	{ "RequestRenewMoreFashion", "dI" },
	{ "RequestRenewMoreFashionEnd", "" },
	{ "RequestSetFashionCustomColor", "dddd" },
	{ "RequestRecycleItem", "IIs" },
	{ "RequestBuyBackItem", "IIs" },
	{ "RequestBatchRecycleItemBegin", "" },
	{ "RequestBatchRecycleItem", "I" },
	{ "RequestBatchRecycleItemEnd", "" },
	{ "MoveSilverFromRepertoryToBag", "d" },
	{ "RequestFixLostSoulEquip", "IIs" },
	{ "DuanWuDZZ_RequestRankList", "s", nil, nil, "lua" },
	{ "SubmitMaintenanceZoneQuestionBegin", "I", 1000 },
	{ "SubmitMaintenanceZoneQuestion", "Is" },
	{ "SubmitGameSettingQuestionEnd", "IssssI", 1000 },
	{ "SubmitAppealQuestionEnd", "Isssss", 1000 },
	{ "EvaluateAnswer", "sIIIs", 1000 },
	{ "GetAnswerByQuestionId", "s", 1000 },
	{ "GetDetailByQuestionId", "s", 1000 },
	{ "GetQuestionsByUrs", "", 1000 },
	{ "CheckIfMaintenanceZoneAvailable", "" },
	{ "GetMaintenanceZoneTags", "I" },
	{ "OpenTianShuUseJade", "" },
	{ "GaoChangApply", "" },
	{ "GaoChangEnterStage1", "" },
	{ "GaoChangQueryApplyInfo", "" },
	{ "RequestReplaceWuZuan", "IIs" },
	{ "OpenYiZhangShanHeTu", "" },
	{ "OpenYiZhangShanHeTuV2", "IIs" },
	{ "RequestPreCheckYiZhangV2", "IIs" },
	{ "YiZhangV2RequestCrossMap", "IIsIiis", 1000 },
	{ "RequestResetTalismanBasicWord", "IIsb" },
	{ "ConfirmResetTalismanBasicWord", "IIsb" },
	{ "RequestResetTalismanExtraWord", "IIsb" },
	{ "ConfirmResetTalismanExtraWord", "IIsb" },
	{ "RequestHeChengTalismanBegin", "" },
	{ "RequestHeChengTalismanPutIn", "I" },
	{ "RequestHeChengTalismanEnd", "" },
	{ "RequestMakeXianJiaTalisman", "II" },
	{ "RequestUpgradeXianJiaBegin", "" },
	{ "RequestUpgradeXianJiaPutIn", "IIb" },
	{ "RequestUpgradeXianJiaEnd", "" },
	{ "ExchangeGiftCode", "s", 1000 },
	{ "ShareTaskDone", "i" },
	{ "CheckPaoShangVehicle", "" },
	{ "BindMobileOpenWnd", "" },
	{ "BindMobileDisbind", "s" },
	{ "BindMobileRequestCaptcha_Bind", "s" },
	{ "BindMobileRequestCaptcha_Renew", "" },
	{ "BindMobileVerifyCaptcha", "s" },
	{ "QueryHolidayQianDaoInfo", "I" },
	{ "RequestHolidayQianDao", "I" },
	{ "ConfirmPayBaoShiJin", "I" },
	{ "ConfirmBaoShiJinAction", "I" },
	{ "ConfirmCleanHistoryPunishTimes", "I" },
	{ "QuerySealInfo", "" },
	{ "SubmitInvitationCode", "d", 2000 },
	{ "QueryInvitationRankInfo", "", 1000 },
	{ "RequestInvitationInfo", "" },
	{ "QueryInvitedPlayerInfo", "", 1000 },
	{ "RequestInvitationAward", "I" },
	{ "SendModuleListBegin", "" },
	{ "SendModuleList", "s" },
	{ "SendModuleListEnd", "s" },
	{ "SendFilteredModuleListBegin", "" },
	{ "SendFilteredModuleList", "s" },
	{ "SendFilteredModuleListEnd", "s" },
	{ "SendProcessListBegin", "" },
	{ "SendProcessList", "s" },
	{ "SendProcessListEnd", "s" },
	{ "SendFilteredProcessListBegin", "" },
	{ "SendFilteredProcessList", "s" },
	{ "SendFilteredProcessListEnd", "s" },
	{ "SendTouchPosHistory", "u" },
	{ "ReportClientTime", "I" },
	{ "ReportClientVersion", "iiii" },
	{ "RequestCheatDetectVerify", "bs" },
	{ "SendCheatInfoBegin", "I" },
	{ "SendCheatInfo", "Is" },
	{ "SendCheatInfoEnd", "Is" },
	{ "SaveGuidePhase", "ii" },
	{ "SaveGuideRecord", "i" },
	{ "SaveOpenEntry", "i" },
	{ "SaveGuideRecordExt", "i" },
	{ "PutOnWing", "i" },
	{ "RequestQnRechargeReturn", "I" },
	{ "CheckQnRechargeReturn", "" },
	{ "ConfirmQnRechargeReturn", "b" },
	{ "RequestEquipPropertyChangedWithTempWords", "IIs" },
	{ "RequestGetHolidayOnlineTimeAward", "I" },
	{ "QueryHolidayOnlineTimeInfo", "" },
	{ "RequestExtraHolidayQianDao", "I" },
	{ "RequestBuyPRAGift", "I" },
	{ "RequesetCanQianZhuanShan", "IIs" },
	{ "ConfirmQianZhuanShan", "IIs" },
	{ "RequestAddPopularity", "ds" },
	{ "AddPresent", "i" },
	{ "SendFlower", "dsIid" },
	{ "ReportPersonalSpace", "sss", 1000 },
	{ "RetryLoginToPersonalSpace", "" },
	{ "SetHidePreviousName", "b", 3000 },
	{ "QueryPictureToken", "I" },
	{ "RequestAddSavePic", "s" },
	{ "PostShouTuMessage", "s" },
	{ "NeverRemindBaiShiOnLevelUp", "" },
	{ "NeedNotBaiShi", "" },
	{ "SearchShiFu", "s" },
	{ "RequestBaiShiToPlayer", "d" },
	{ "RequestBeShiTuWithTeamMember", "bb" },
	{ "RefuseBeTuDiOfPlayer", "d" },
	{ "AcceptBeTuDiOfPlayer", "dbb" },
	{ "BreakShiTuRelationship", "bd" },
	{ "PlayerRefuseEngage", "dsss" },
	{ "PlayerAcceptEngage", "dss" },
	{ "RequestOpenCeBaZiWnd", "" },
	{ "RequestCeBaZi", "Iiiii" },
	{ "RequestCancelCeBaZi", "" },
	{ "RequestEnterFriendWeddingScene", "s" },
	{ "BroadcastWeddingDeclareMsg", "s" },
	{ "OpenWeddingDinner", "i" },
	{ "FireLuxuryFireworks", "" },
	{ "PlayerRequestEngage", "b" },
	{ "RequestSendSimpleCandy", "" },
	{ "RequestSendLuxuryCandy", "" },
	{ "QueryWeddingCertNames", "dd" },
	{ "FireSimpleFireworks", "" },
	{ "RequestParadeHangzhou", "" },
	{ "RequestParadeJinling", "" },
	{ "ConfirmRequestCancelEngage", "" },
	{ "ConfirmSendInvitationToAllFriend", "" },
	{ "ConfirmSendInvitationToGuild", "" },
	{ "RequestStartWedding", "" },
	{ "ConfirmRequestPlayerDivorce", "" },
	{ "ConfirmPlayerRequestForceDivorce", "" },
	{ "ConfirmPlayerRequestCancelDivorce", "" },
	{ "ConfirmPlayerDoDivorceTogather", "" },
	{ "RequestApplyQiangqin", "" },
	{ "VoteQiangqinBiaoBai", "d" },
	{ "SaveQiangqinBiaoBai", "s" },
	{ "QiangqinForceTeleportToWeddingScene", "s" },
	{ "QueryIfPlayerStartQiangqinApply", "" },
	{ "QueryIfShowHideQiangqinPlayer", "" },
	{ "VoteGlobalQiangqinBiaoBai", "d" },
	{ "QueryWeddingFireworksRecord", "" },
	{ "ConfirmChuJiaHuaYuan", "s" },
	{ "ConfirmRansomChuJiaPlayerPay", "d" },
	{ "ConfirmVisitPlayerPay", "d" },
	{ "QueryFriendRansomChuJiaResult", "dI" },
	{ "RequestChuJiaDaZuo", "I" },
	{ "RequestChuJiaBaiFo", "I" },
	{ "ChuJiaHuaYuanAgree", "d" },
	{ "OnChuJiaHuanYuanRefuse", "d" },
	{ "NotSinglePlayerRequestChuJia", "" },
	{ "RequestSignUpForBiWu", "" },
	{ "RequestEnterBiWuPrepareScene", "" },
	{ "QueryBiWuStageMatchInfo", "" },
	{ "QueryBiWuStageWatchInfo", "I" },
	{ "RequestWatchBiWu", "II" },
	{ "QueryBiWuSignUpInfo", "" },
	{ "QueryBiWuTeamRoundData", "" },
	{ "UpdateBiWuSelectPlayerIdList", "Iu" },
	{ "RequestOpenBiWuSelectWnd", "" },
	{ "QueryBiWuScore", "" },
	{ "QueryBiWuChampionInfo", "I" },
	{ "RequestSubmitPurpleIceExchangeMF", "i" },
	{ "RequestSubmitRedIceExchangeMF", "i" },
	{ "PlayerCanUsePresentItem", "ii" },
	{ "RequestUsePresentItem", "iiis" },
	{ "RequestGetPresentItemFromLink", "dddIb" },
	{ "RequestCanGetPresentItemFromLink", "dddI" },
	{ "SelectTalismanAppearance", "iii" },
	{ "GetDuoBaoPlayStatus", "" },
	{ "RequestCurrentRoundDuoBaoInfo", "I" },
	{ "RequestDuoBaoOverview", "" },
	{ "RequestMyDuoBaoInfos", "" },
	{ "GetDuoBaoResult", "II" },
	{ "RequestDuoBaoInvest", "II" },
	{ "RequestMyDuoBaoReward", "" },
	{ "RequestRecentHongBaoInfo", "I", 2000 },
	{ "RequestSendHongBao", "IIsIIsI", 1000 },
	{ "RequestSnatchHongBao", "sIs", 1000 },
	{ "RequestHongBaoDetails", "s", 1000 },
	{ "RequestHongBaoHistoryRecord", "", 1000 },
	{ "RequestRemainHongBaoSendTimes", "I" },
	{ "RequestChangeGender", "I" },
	{ "QueryPresentItemRecieverName", "s" },
	{ "ConfirmLevelUp", "" },
	{ "QueryGuildPreLeagueTaskPersonalPoint", "" },
	{ "PackageOrderConfirm", "ibb" },
	{ "RepertoryReorderConfirm", "Ibb", 1000 },
	{ "RequestOpenDanMu", "" },
	{ "RequestCloseDanMu", "" },
	{ "SendDanMu", "sb" },
	{ "RequestAllZuoQiOverview", "" },
	{ "RequestRideOnZuoQi", "s" },
	{ "RequestRideOffZuoQi", "s" },
	{ "RequestRenewZuoQi", "sI" },
	{ "RequestSetDefaultZuoQi", "s" },
	{ "RequestDiscardZuoQi", "sb" },
	{ "RequestInvitePassenger", "I" },
	{ "GetInVehicleAsPassenger", "I" },
	{ "RefuseGetInVehicleAsPassenger", "I" },
	{ "RequestRenewMoreZuoQiBegin", "" },
	{ "RequestRenewMoreZuoQi", "sI" },
	{ "RequestRenewMoreZuoQiEnd", "" },
	{ "SetGuildLeagueFlag", "III", 1000 },
	{ "GetHuiLiuReward", "II" },
	{ "QueryHuiLiuReward", "" },
	{ "GetInvitedHuiLiuPlayerInfo", "" },
	{ "InvitedPlayerHuiLiu", "d" },
	{ "RequestLoveFlowerInfo", "I" },
	{ "RequestUseCommonFlowerSeed", "I" },
	{ "RequestUseLoveFlowerSeed", "I" },
	{ "ShakeLoveFlower", "I" },
	{ "RequestLoveFlowerTearAward", "I" },
	{ "GetDynamicActivityInfo", "I", 1000 },
	{ "GetDynamicActivitySimpleInfo", "" },
	{ "RequestGoDynamicActivitySite", "IIs" },
	{ "RequestRecordClientLog", "su" },
	{ "RequestStartShiTuQingXYLX", "" },
	{ "RequestStopShiTuQingXYLX", "" },
	{ "ShiTuQingXYLXAnswer", "di" },
	{ "RequestKouDaoStat", "I" },
	{ "RequestResetKouDaoStat", "I" },
	{ "RequestBroadcastKouDaoStat", "I" },
	{ "RequestKouDaoSceneInfo", "" },
	{ "CheckSetKouDaoEliteRight", "" },
	{ "RequestSendLoveLetter", "dub" },
	{ "RequestGetTopLoveLetter", "" },
	{ "RequestGetPlayerSendLoveLetter", "d" },
	{ "RequestGetPlayerReceivedLoveLetter", "ds" },
	{ "PraiseLoveLetter", "s" },
	{ "QueryLoveLetterSenderInfo", "d" },
	{ "QueryLoveLetterReceiverInfo", "d" },
	{ "RequestBroadLoveLetter", "s" },
	{ "SubmitQueQiaoPoemAnswer", "s" },
	{ "CompleteQueQiaoBaoYiBao", "" },
	{ "SetSwitchTargetSkipPetAndLingShou", "i" },
	{ "GetExpRainAward", "" },
	{ "RequestRelocateHouseResponse", "ss" },
	{ "SendClientInfoBegin", "ss" },
	{ "SendClientInfo", "sss" },
	{ "SendClientInfoEnd", "ss" },
	{ "IgnorePlayUpdateAlert", "" },
	{ "WriteJinLanPu", "di" },
	{ "UpdateJieBaiTitleWhenJieBai", "dss" },
	{ "ConfirmSessionJieBaiTitle", "ds" },
	{ "SignForJieBai", "d" },
	{ "RequestCloseJinLanPu", "" },
	{ "SetJiebaiCommonTitle", "s" },
	{ "SetJiebaiPersonalTitle", "s" },
	{ "RequsetCancelJieBai", "" },
	{ "RequestEnterChuangGuan", "I" },
	{ "RequestChuangGuanOverview", "I" },
	{ "RequestChuangGuanSceneInfo", "" },
	{ "RequestChuangGuanRank", "" },
	{ "StartBangHuaFirstStage", "" },
	{ "StartBangHuaSecondStage", "" },
	{ "EnterBangHuaSecondStageMultiPlay", "I" },
	{ "EnterBangHuaSecondStageSinglePlay", "" },
	{ "EnterBangHuaThirdStagePlay", "" },
	{ "QueryBangHuaCurStageStatus", "" },
	{ "ConfirmEnterBangHuaSecondStageSingle", "" },
	{ "QueryBangHuaFightData", "I" },
	{ "QueryBangHuaScenePlayerInfo", "" },
	{ "BroadcastBangHuaPlayerInfoInGuild", "I" },
	{ "RequestClearBangHuaFightData", "I" },
	{ "QueryBangHuaSecondSingleInfo", "" },
	{ "RequestExchangeAntiCheck", "" },
	{ "RequestExchangeAnti", "I", 2000 },
	{ "RequestSubAntiPointBegin", "", 2000 },
	{ "RequestSubAntiPoint", "II" },
	{ "RequestSubAntiPointEnd", "" },
	{ "RequestDistributeAntiPoint", "" },
	{ "RequestAntiExchangeTipsInfo", "" },
	{ "RequestExchangeAntiCheck2", "" },
	{ "RequestExchangeAnti2", "I", 2000 },
	{ "RequestAddOrSubAntiPointBegin", "", 2000 },
	{ "RequestAddOrSubAntiPoint", "Ii" },
	{ "RequestAddOrSubAntiPointEnd", "" },
	{ "RequestAntiExchangeTipsInfo2", "" },
	{ "SendQueryOrderSubReceiptSignBegin", "" },
	{ "SendQueryOrderSubReceiptSign", "s" },
	{ "JewelHeCheng", "IIbi" },
	{ "SetYiShiAddHpContinuously", "b" },
	{ "RequestContactGuildManager", "d" },
	{ "RequestChangeHairColor", "sIII" },
	{ "QueryDuoHunFanPlayerName", "sd" },
	{ "QueryHunPoPlayerName", "sd" },
	{ "UseQianLiYan", "iidbbs" },
	{ "SetDuoHunFan", "iid" },
	{ "PlayerRequestZhaoHun", "ii" },
	{ "CanZhaoHun", "" },
	{ "RequestEnterYouMingJie", "" },
	{ "QueryCuJuSignUpInfo", "" },
	{ "RequestSignUpCuJuPractice", "" },
	{ "CancelSignUpCuJuPractice", "" },
	{ "RefuseJoinCuJuPractice", "" },
	{ "RequestSignUpForCuJuGame", "" },
	{ "RequestEnterCuJuPrepareScene", "" },
	{ "ConfirmStartCuJuQieCuo", "" },
	{ "TrySetCuJuPosition", "I" },
	{ "QueryCuJuGameMatchInfo", "" },
	{ "QueryCuJuPlayInfo", "" },
	{ "RequestWatchCuJuPlay", "I" },
	{ "SubmitGuoQingXianLiDaTiAnswer", "I" },
	{ "QueryCampusCodeInfo", "", 1000 },
	{ "SubmitCampusCode", "I", 1000 },
	{ "RequestActiveTalismanOnBody", "i" },
	{ "RequestTeamDamageInfo", "", 1000 },
	{ "RequestEnterQianShiPlay", "i" },
	{ "RequestPutPlayFurnitureAtPos", "IIiii" },
	{ "RequestPackPlayFurniture", "I" },
	{ "UpdateShouldShowCapacityUpAlert", "bd" },
	{ "UpdateShouldShowAchievementAlert", "bd" },
	{ "RequestSetHelmetHide", "I" },
	{ "RequestPaimaiBet", "sII" },
	{ "RequestPaimaiOnShelf", "IIII" },
	{ "GetWorldPaiMaiOnShelfGoods", "IIII" },
	{ "GetGuildPaiMaiOnShelfGoods", "IIII" },
	{ "GetMyPaiMaiInvestGoods", "IIII" },
	{ "GetPaiMaiGoodsByTemplateId", "IIIII" },
	{ "GetMyPaimaiOnShelfGoods", "" },
	{ "GetGuildPaimaiHistory", "" },
	{ "RequestPaimaiOffShelf", "s" },
	{ "GetPaiMaiBonusValue", "" },
	{ "TakePaiMaiBonus", "d" },
	{ "ExchangePaiMaiBonus", "d" },
	{ "GetPaiMaiFrozenDetail", "" },
	{ "RequestTakePaiMaiUnFrozenJade", "" },
	{ "GetPaiMaiFrozenInfo", "" },
	{ "GetWolrdOnShelfCount", "" },
	{ "GetClassfiedOnShelfData", "" },
	{ "ZhaoHunMTWCConfirm", "" },
	{ "RandomCanAcceptQingYuanPlayers", "", 300 },
	{ "TalkWithYangYang", "ss" },
	{ "PlayerTryVote", "b" },
	{ "ChaiJieFaBao", "ii" },
	{ "SetNickName", "ds" },
	{ "SetLeaving", "b" },
	{ "SetTempLeaving", "b" },
	{ "SetAutoReply", "b" },
	{ "SetAutoReplyMsg", "s" },
	{ "RequestDoTransfer", "I" },
	{ "RequestExchangeNewJueJiItem", "IIIII" },
	{ "SetExpression", "i" },
	{ "SetExpressionTxt", "i" },
	{ "SetSticker", "i" },
	{ "SetProfileFrame", "i" },
	{ "QueryValidExpression", "" },
	{ "QueryValidExpressionTxt", "" },
	{ "QueryValidSticker", "" },
	{ "QueryValidProfileFrame", "" },
	{ "UnlockExpressionTxt", "i" },
	{ "UnlockSticker", "i" },
	{ "WashTalismanSuit", "iIb" },
	{ "RequestChaiJieFaBao", "ii" },
	{ "CreateFurnitureFromRepository", "Is" },
	{ "AddFurniture", "IIIdIIbsu" },
	{ "MoveFurniture", "IIdddIb" },
	{ "ExchangeFurniture", "IIIIds" },
	{ "ConvertItem2RepositoryFurniture", "IIs" },
	{ "ScaleFurniture", "II" },
	{ "ShiftFurniture", "Iddd" },
	{ "RequestPackAllFurniture", "" },
	{ "ScalePenzaiHuamu", "II" },
	{ "RotatePenzaiHuamu", "II" },
	{ "RequestProduceFurniture", "IIs" },
	{ "RequestUseFurniture", "II" },
	{ "ConvertItem2WeixiuResource", "Is" },
	{ "ConvertRepaireItem2WeixiuResource", "I" },
	{ "RequestWeixiuFurniture", "s" },
	{ "RequestOneKeyWeixiu", "II" },
	{ "QueryQishuFurniture", "" },
	{ "QueryHouseData", "" },
	{ "RequestBuyHouse", "" },
	{ "QueryAllCommunity", "" },
	{ "QueryCommunityData", "d" },
	{ "QueryHouseIntroByCommunityPos", "dI" },
	{ "QueryHouseIntroByHouseId", "s" },
	{ "RequestEnterHouseScene", "s" },
	{ "RequestEnterHouseSceneFromRank", "s" },
	{ "RequestEnterHome", "" },
	{ "RequestEnterHouseRoom", "" },
	{ "RequestEnterXiangfang", "" },
	{ "RequestConvertHouseToContract", "" },
	{ "RequestConvertContractToHouse", "II" },
	{ "RequestCollectHouse", "s" },
	{ "RequestRemoveCollectedHouse", "s" },
	{ "RequestRelocateHouse", "dI" },
	{ "PlayerRequestCompositeHuamu", "III" },
	{ "RequestDiqiLastOwnerName", "sd" },
	{ "OpenCJBModelMakeWnd", "" },
	{ "QueryRoomerPrivalegeInfo", "d" },
	{ "RequestSetRoomerPrivalege", "du" },
	{ "QueryOffLinePlayerHouse", "d" },
	{ "RequestCombineZhulongshi", "IIII" },
	{ "RequestUpdateBedBeiziStatus", "Ib" },
	{ "RequestGetClosestoolProduction", "I" },
	{ "RequestClosestoolProduce", "I" },
	{ "RequestGetHouseNewsReward", "I" },
	{ "RequestExchangeFurnitureWithRepaireMeterialBegin", "" },
	{ "RequestExchangeFurnitureWithRepaireMeterial", "s" },
	{ "RequestExchangeFurnitureWithRepaireMeterialEnd", "" },
	{ "RequestChangeShineiOpen", "b" },
	{ "PutMapiToMajiu", "s" },
	{ "TakeBackMapiFromMajiu", "s" },
	{ "RequestFightWithPuppet", "I" },
	{ "RequestStopFightWithPuppet", "I" },
	{ "RequestRemoveCPPuppet", "I" },
	{ "RequestRemoveFriendPuppet", "I" },
	{ "QueryFriendsFitPuppetRequirement", "" },
	{ "PlayerRequestAddFriendPuppet", "d" },
	{ "PuppetPlayerQueryHouseList", "" },
	{ "RemoveSelfPuppetAtHouse", "s" },
	{ "RequestViewPuppetDetailInfo", "I" },
	{ "SaveFriendPuppetGossip", "IIs" },
	{ "RequestCreatePuppet", "I" },
	{ "RequestPackPuppet", "I" },
	{ "RequestMovePuppet", "Iiii" },
	{ "RequestShowExpressionWithPuppet", "I" },
	{ "RenameHousePuppet", "Is" },
	{ "QueryPuppetFightHistory", "I" },
	{ "SetPuppetIsPatrol", "Ib" },
	{ "SwitchHousePuppetDifficulty", "II" },
	{ "RequestAddFashionForPuppet", "IIIs" },
	{ "RequestDressUpPuppet", "Id" },
	{ "RequestTakeOffPuppetFashion", "Id" },
	{ "RequestRenewPuppetFashion", "Iuu" },
	{ "PlayerRequestAddTabenForPuppet", "IIIsb" },
	{ "PlayerRequestSetPuppetHideFashion", "IIIII" },
	{ "PlayerRequestSetFashionColor", "Idddd" },
	{ "RequestRemovePuppetFashion", "Id" },
	{ "RequestAddPuppetMaxFashionNum", "IIIs" },
	{ "RequestUpgradeHouseBuilding", "I" },
	{ "RequestCancelHouseProgress", "I" },
	{ "RequestHouseProgressReduceTime", "IIsIb" },
	{ "RequestAddExtraProgress", "IIsIb" },
	{ "RequestPlantCrop", "II" },
	{ "RequestHarvestCrop", "I" },
	{ "RequestStealCrop", "I" },
	{ "RequestRemoveCrop", "I" },
	{ "RequestReviveCrop", "I" },
	{ "RequestExchangeCropProduct", "II" },
	{ "RequestChangeBuildingAppearance", "II" },
	{ "RequestMakeCJBModel1", "dI" },
	{ "RequestMakeCJBModel_Width", "dddddddddddd" },
	{ "RequestMakeCJBModel_Color", "IIIIIIIIIIII" },
	{ "RequestMakeCJBModel_Texture", "IIIIIIIIIIII" },
	{ "RequestMakeCJBModel_TextureRestore", "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh" },
	{ "RequestMakeCJBModel2", "ssd" },
	{ "QueryLastSavedCJBMakeProgress", "" },
	{ "RequestSaveCJBMakeProgress1", "IdI" },
	{ "RequestSaveCJBMakeProgress_Width", "dddddddddddd" },
	{ "RequestSaveCJBMakeProgress_Color", "IIIIIIIIIIII" },
	{ "RequestSaveCJBMakeProgress_Texture", "IIIIIIIIIIII" },
	{ "RequestSaveCJBMakeProgress_TextureRestore", "hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh" },
	{ "RequestSaveCJBMakeProgress2", "s" },
	{ "EndCJBMakeProgress", "Is" },
	{ "PlayerRequestCompositeChuanjiabaoItem", "IIsIIs" },
	{ "PlayerRequestMakeZhushabi", "IIb", 500 },
	{ "RequestJiandingZhushabi", "IIs" },
	{ "RequestPray", "", 2000 },
	{ "RequestAcceptLastHousePrayResult", "", 2000 },
	{ "MoveChuanjiabaoToHouse", "IIsI" },
	{ "QueryMyHouseLingqi", "", 500 },
	{ "QueryChuanjiabaoInHouse", "" },
	{ "SetActiveChuanjiabaoSuite", "I" },
	{ "MoveChuanjiabaoFromHouseToBag", "IIs" },
	{ "RequestChuanjiabaoArtistName", "ddd" },
	{ "RequestZhushabiArtistName", "dd" },
	{ "RequestChuanjiabaoModelArtistName", "d" },
	{ "RequestEnterContract", "IIs" },
	{ "QueryPlayerPrayInfo", "" },
	{ "RequestCalculateShapeStar", "Iud" },
	{ "RequestCalculateShangseStar", "Iuu" },
	{ "RequestNameChuanjiabaoModel", "sss" },
	{ "RequestRmChuanjiabaoModelTexture", "I" },
	{ "QueryTongzhuNum", "" },
	{ "RequestInviteRoomer", "d" },
	{ "ConfirmInviteRoomer", "d" },
	{ "RejectInviteRoomer", "d" },
	{ "AcceptRoomerInvite", "d" },
	{ "RejectRoomerInvite", "d" },
	{ "RequestRoomerLeave", "d" },
	{ "RoomerRequestLeave", "" },
	{ "QueryRoomerNames", "u" },
	{ "QueryCJBMakeIsRoomer", "" },
	{ "RequestRoomerAssignableTitleInfo", "" },
	{ "QueryFriendsFitTongzhuRequirement", "" },
	{ "OwnerSetHouseTitleInfo", "u" },
	{ "NameMyHouse", "ss" },
	{ "VarifyHouseTitleForHouseNameSuccess", "s" },
	{ "VarifyHouseTitleForHouseNameSuccessEnd", "" },
	{ "VarifyHouseTitlesForHouseTitleSuccess", "s" },
	{ "VarifyHouseTitlesForHouseTitleEnd", "" },
	{ "RequestOpenNameMyHouse", "" },
	{ "RequestResetChuanjiabaoMakeProgress", "" },
	{ "QueryPlayerCurrentPrayWords", "I" },
	{ "RequestChaijieZhushabiDetails", "IIs" },
	{ "RequestChaijieZhushabi", "IIs" },
	{ "RequestChaijieChuanjiabao", "IIs" },
	{ "RequestMakeNewModel", "I" },
	{ "BeginRepaireChuanjiabao", "sI" },
	{ "QueryPlayerNextTimePrayInfo", "" },
	{ "NameMyXiangfang", "s" },
	{ "ConfirmConvertHouseToMingyuan", "sII" },
	{ "PlayerRequestLoadHouseLayoutCheck", "I" },
	{ "PlayerRequestLoadHouseLayout", "ub" },
	{ "RequestExchangeLingshouToFurniture", "s" },
	{ "FeedLingshouFurniture", "II" },
	{ "SetLingshouFurnitureProperty", "IIss" },
	{ "ReviveLingshouFurniture", "I" },
	{ "ConfirmAddFreeMaintainFeeDay", "IsII" },
	{ "ConfirmSetFurnitureLimitStatus", "sII" },
	{ "QueryHasRegisteredChusai", "" },
	{ "UpdateHouseCompetionChusaiUploadTimes", "Is" },
	{ "RequestEnterHouseCompetitionYard", "d" },
	{ "RequestEnterHouseCompetitionRoom", "" },
	{ "RequestEnterHouseCompetitionXiangfang", "" },
	{ "RequestOpenHouseCompetitionWnd", "" },
	{ "RequestVictoryPlayerHouseList", "" },
	{ "PlayerRequestInteractWithMapi", "IId" },
	{ "RequestShouhuLingshou", "ss" },
	{ "RequestStopShouhuLingshou", "ss" },
	{ "RequestPutOnZuoqiEquip", "ssI" },
	{ "RequestPutBackZuoqiEquip", "sIs" },
	{ "QueryYumaInfo", "" },
	{ "QueryYumaFriends", "" },
	{ "InviteYumaWithFriend", "d" },
	{ "AgreeYumaWithFriend", "d" },
	{ "DenyYumaWithFriend", "d" },
	{ "CancelYuma", "" },
	{ "ConfirmStartYumaWithFriend", "d" },
	{ "RequestYumaSelf", "ss" },
	{ "QueryMapiDetail", "s" },
	{ "RequestGenerateLingfuItem", "sII" },
	{ "RequestGenerateLingfuItemFast", "II" },
	{ "RequestReclaimLingfuItem", "s" },
	{ "RequestOneKeyReclaimLingfuItems", "u" },
	{ "PlayerStartYumaChooseMapi", "s" },
	{ "YumaPlayerChooseRightMapi", "ds" },
	{ "QueryAllMapiDetail", "" },
	{ "QueryAllMapiAlert", "" },
	{ "RequestUpgradeLingfuLevel", "" },
	{ "RequestRandomAddMapiFurniture", "s" },
	{ "RequestWashLingfuProperty", "sIb" },
	{ "RequestReplaceLingfuProperty", "sIs" },
	{ "RequestQianghuaLingfu", "sbs" },
	{ "RequestGetMapiBaby", "I" },
	{ "RequestAddMapiExpWithItem", "ssI" },
	{ "RequestAddMapiLifeWithItem", "ssI" },
	{ "RequestRenameMapi", "ss" },
	{ "QueryMapiPinfen", "s" },
	{ "RequestFixMapiLingfu", "ss" },
	{ "ConfirmPlayerResetXiuweiPrice", "" },
	{ "ConfirmPlayerResetXiulianPrice", "" },
	{ "RequestCallZhaoQin", "d", 1000 },
	{ "RequestJoinZhaoQin", "d" },
	{ "GetZhaoQinCallerInfo", "" },
	{ "GetOneZhaoQinCallerInfo", "d" },
	{ "GetZhaoQinGuanZhanInfo", "" },
	{ "RequestZhaoQinGuanZhan", "d" },
	{ "GetZhaoQinItemTargetName", "sII" },
	{ "RequestPlayerFamilyTree", "ds" },
	{ "QueryCanTransferHolesSourceEquip", "" },
	{ "CanSelectTransferHoleSourceEquip", "I" },
	{ "QueryCanTransferHolesTargetEquip", "I" },
	{ "CanSelectTransferHoleTargetEquip", "II" },
	{ "ConfirmTransferEquipHoleTarget", "II" },
	{ "QueryCanTransferXianJiaFaBaoList", "" },
	{ "ConfirmTransferXianJiaFaBao", "u" },
	{ "GetGuildBuildBonus", "" },
	{ "GetSelfBuildBonus", "" },
	{ "RequestMultipleExpInfo", "" },
	{ "RequestReceiveFreeDoubleExp", "" },
	{ "RequestDongJieMultipleExp", "I" },
	{ "RequestJieDongMultipleExp", "I" },
	{ "RequestBuyMultipleExpItem", "I" },
	{ "JoinCcChannelByCid", "is" },
	{ "QueryZhuBoList", "" },
	{ "ClearTalismanSuitUserWordConfirm", "i" },
	{ "JoinTeamRemoteChannel", "s" },
	{ "JoinOrgRemoteChannel", "s" },
	{ "JoinCcRemoteChannel", "s" },
	{ "SetGuildCommander", "d" },
	{ "ResetGuildCommander", "" },
	{ "QueryRemoteChannelEId", "dI" },
	{ "LeaveTeamRemoteChannel", "" },
	{ "LeaveOrgRemoteChannel", "" },
	{ "LeaveCcRemoteChannel", "" },
	{ "SwitchCcChannel", "s" },
	{ "SetTeamSpeakStatus", "I" },
	{ "CcFollow", "I" },
	{ "CcUnfollow", "I" },
	{ "TryRandomCcChannelIfNotInAnyChannel", "s" },
	{ "JoinCcChannelByCcid", "Is" },
	{ "RecommendZhuBoInTeamChannel", "Is" },
	{ "RecommendZhuBoInOrgChannel", "Is" },
	{ "InviteListenGuildCommand", "", 3000 },
	{ "JoinTeamGroupRemoteChannel", "is" },
	{ "InviteListenTeamGroupCommand", "", 3000 },
	{ "SetTeamGroupCommander", "di" },
	{ "ResetTeamGroupCommander", "i" },
	{ "LeaveTeamGroupRemoteChannel", "" },
	{ "CreateDouHunChlgTeam", "s" },
	{ "VoteDouHunChlgTeam", "s" },
	{ "RequestJoinDouHunChlgTeam", "s" },
	{ "RequestModifyDouHunTeamDeclaration", "s" },
	{ "QueryDouHunChlgTeamApplyInfo", "" },
	{ "QueryDouHunChlgTeamInfoById", "s" },
	{ "AcceptDouHunTeamApplicant", "d" },
	{ "RequestLeaveDouHunChlgTeam", "" },
	{ "KickDouHunChlgTeamMember", "d" },
	{ "CancelApplyDouHunChlgTeam", "s" },
	{ "RefuseDouHunTeamApplicant", "d" },
	{ "ClearDouHunChlgTeamApplicant", "" },
	{ "QueryDouHunChlgTeamPosInfo", "" },
	{ "ChlgTeamLeaderSetMemberPos", "dI" },
	{ "ChlgTeamLeaderCancelMemberPos", "dI" },
	{ "QueryPlayerDouHunChlgTeamInfo", "" },
	{ "RequestEnterDouHunCrossPrepareScene", "" },
	{ "QueryDouHunTrainValue", "" },
	{ "SubmitDouHunTranItem", "u" },
	{ "QueryDouHunTrainEffectByType", "I" },
	{ "SetDouHunTrainJingLiFuTiEffect", "" },
	{ "SetDouHunTrainQiLiFuTiEffect", "s" },
	{ "SetDouHunTrainShenLiFuTiEffect", "u" },
	{ "QueryLocalDouHunPlayWatchInfo", "" },
	{ "RequestWatchLocalDouHun", "I" },
	{ "OpenCrossDouHunWatchWnd", "" },
	{ "RequestWatchCrossDouHun", "II" },
	{ "QueryCorssDouHunCurScoreInfo", "" },
	{ "QueryCorssDouHunHistoryScoreInfo", "" },
	{ "QueryCorssDouHunGroupScoreInfo", "I" },
	{ "QueryCrossDouHunHistoryWatchInfo", "III" },
	{ "QueryDouHunPlayInfo", "" },
	{ "QueryDouHunGroupRoundMember", "" },
	{ "requestSubstituteDouHunGroupMember", "d" },
	{ "QueryCrossDouHunWatchInfo", "II" },
	{ "RequestSetGambleChoice", "Iu" },
	{ "CheckAutoUseBaoLingDan", "" },
	{ "ConfirmAutoUseBaoLingDan", "" },
	{ "ConfirmAutoUseYXJWJ", "" },
	{ "GCO_RequestOpenOccupationDetailWnd", "" },
	{ "AddPlayerLbsInfo", "dd" },
	{ "DeleteLbsInfo", "" },
	{ "QueryLbsInfo", "ddd" },
	{ "QueryPlayerLbsDetails", "d" },
	{ "QueryPlayerLastLbsInfo", "" },
	{ "OpenGongJianJiaYuanWin", "" },
	{ "GongJianJiaYuanSubmitItem", "I" },
	{ "GongJianJiaYuanReward", "I" },
	{ "RequestRecentDaoJuLiBaoInfo", "", 500 },
	{ "RequestSendDaoJuLiBao", "IIsI", 1000 },
	{ "RequestSnatchDaoJuLiBao", "sI", 1000 },
	{ "RequestDaoJuLiBaoDetails", "s", 1000 },
	{ "RequestDaoJuLiBaoHistoryRecord", "", 1000 },
	{ "RequestRemainDaoJuLiBaoSendTimes", "I" },
	{ "QueryDaoJuLiBaoOpenState", "" },
	{ "CheckMyRights", "sI" },
	{ "QueryOnlineMembersInMyGuild", "I", 1000 },
	{ "QueryPlayerIsGhost", "d" },
	{ "ReOrderItemBag", "I" },
	{ "QueryMembersJieBaiPersonalTitle", "s" },
	{ "AnswerTaskQuestion", "IIb" },
	{ "RequestDeleteRole", "" },
	{ "SecondRequestDeleteRole", "" },
	{ "RequestSendDelRoleCapt", "I" },
	{ "RequestVerifyDelRoleCapt", "s" },
	{ "RequestCancleDelRole", "" },
	{ "MoveAllItemsToItem", "I" },
	{ "TakeAllItemsOutFormItem", "I" },
	{ "MatchStaticJingLingFilter", "ssss" },
	{ "MatchDynamicJingLingFilter", "Isss" },
	{ "RequestExpressionAction", "I" },
	{ "BuyAndSetDuoHunFan", "d" },
	{ "RequestDoVote", "II" },
	{ "RequestSendFlowerForVote", "III" },
	{ "RequestEnterYangYangHome", "" },
	{ "GetScreenVoteInfo", "" },
	{ "RequestEnterYangYangRoom", "" },
	{ "RequestCompleteYaoQian", "I" },
	{ "RequestSubmitQiFuXiang", "I" },
	{ "RequestCreateGroupIM", "s", 3000 },
	{ "InvitePlayerJoinGroupIM", "dds" },
	{ "AcceptInvitePlayerJoinGroupIM", "d" },
	{ "RefuseInvitePlayerJoinGroupIM", "d" },
	{ "LeaveGroupIM", "d" },
	{ "DepartGroupIM", "d" },
	{ "SendGroupIMMsg", "dbbss" },
	{ "ChangeGroupOwner", "dd" },
	{ "ChangeGroupName", "ds" },
	{ "ChangeGroupAnnouncement", "ds" },
	{ "SetNeedApprove", "db" },
	{ "KickPlayerFromGroup", "dd" },
	{ "AcceptGroupIMApprove", "dd" },
	{ "RefuseGroupIMApprove", "dd" },
	{ "BroadcastVoiceTranslationInGroupIM", "dsdss", 500 },
	{ "SetGroupIMIgnoreNewMsg", "db" },
	{ "RequestChangeShareLBSLocationFlag", "I" },
	{ "RequestPutInItemBag", "II" },
	{ "RequestMoveItemOutFromItemBag", "III" },
	{ "RequestResetAppearanceAndSkill", "" },
	{ "RequestAcceptNextHJTask", "" },
	{ "RequestLingShouBattleRank", "" },
	{ "RequestHmtShareInfo", "" },
	{ "ReportHmtShareSuccess", "" },
	{ "RequestOpenLocalDouHunWatchWnd", "" },
	{ "RequestSetShopActivityViewed", "I" },
	{ "RequestChallengePGQY", "" },
	{ "RequestPGQYStat", "I" },
	{ "RequestResetPGQYStat", "I" },
	{ "RequestBroadcastPGQYStat", "I" },
	{ "RequestFixChuanjiabaoItem", "IIs" },
	{ "ConfirmFixChuanjiabaoItem", "IIs" },
	{ "RequestEnterShouXiDaDiZiPrepareScene", "" },
	{ "QueryShouXiDaDiZiBiWuOverView", "I" },
	{ "QueryShouXiDaDiZiBiWuStageDetails", "II" },
	{ "RequestWatchShouXiDaDiZiBiWu", "d" },
	{ "RequestGuessRiddleType1", "I" },
	{ "RequestGuessRiddleType2", "s" },
	{ "RequestEquipPresentName", "ds" },
	{ "RequestLogJingLingFenXiangInfo", "dssI" },
	{ "PlayerChunJiePray", "s" },
	{ "RequestKaiJiang", "II" },
	{ "PlayerInputChunJieJiangPinInfo", "IIsss" },
	{ "AcceptEnterChuangGuan", "", 1000 },
	{ "RefuseEnterChuangGuan", "", 1000 },
	{ "QueryChuangGuanWaitInfo", "" },
	{ "SelectValentineAnswer", "II" },
	{ "RequestBroadcastValentineHelp", "", 300000, "VALENTINE_HELP_CD" },
	{ "ExchangeValentineRewardItem", "" },
	{ "TryGiveHelpValentine", "dI" },
	{ "QueryValentineFlowerData", "" },
	{ "PreCheckGiveHelpValentine", "dIIsdd" },
	{ "QueryValentineGetHelpCount", "" },
	{ "QianLiYanTrackCheckDest", "s" },
	{ "RequestSignUpPlayByKey", "I", 1000 },
	{ "RequestCancelSignUpPlayByKey", "I", 1000 },
	{ "RequsetGetSignUpInfoByKey", "I", 1000 },
	{ "RequestCallUpOnePlayer", "di" },
	{ "PlayerBeCalledUpInGuildLeagueDone", "id", 1000 },
	{ "RequestOpenGuildCallUpWnd", "", 1000 },
	{ "RequestSendPersonalHongBao", "dIs" },
	{ "QueryPersonalHongBaoOpenState", "" },
	{ "QueryTeamFightData", "" },
	{ "ResetTeamFightData", "" },
	{ "RequestEnterQMJJPrepareScene", "" },
	{ "QueryQMJJStageMatchInfo", "" },
	{ "QueryQMJJStageWatchInfo", "I" },
	{ "RequestWatchQMJJ", "II" },
	{ "UpdateQMJJSelectPlayerIdList", "Iu" },
	{ "QueryQMJJTeamRoundData", "" },
	{ "RequestOpenQMJJSelectWnd", "" },
	{ "ReportGuestBindInfo", "is" },
	{ "BuyMallItemWithReason", "iIIs" },
	{ "RequestOpenSecondaryPassword", "", 1000 },
	{ "RequestSetSecondaryPassword", "s", 1000 },
	{ "RequestUpdateSecondaryPassword", "ss", 1000 },
	{ "RequestClearSecondaryPassword", "", 1000 },
	{ "RequestCloseSecondaryPassword", "s", 1000 },
	{ "RequestVerifySecondaryPassword", "s", 1000 },
	{ "RequestSecondaryPasswordCaptcha", "I", 1000 },
	{ "VerifySecondaryPasswordCaptcha", "s", 1000 },
	{ "RequestSecondaryPasswordProtectContent", "", 1000 },
	{ "UpdateSecondaryPasswordProtectContent", "u", 1000 },
	{ "RequestDeleteQuDaoRole", "" },
	{ "SecondRequestDeleteQuDaoRole", "" },
	{ "VerifyDeleteRoleId", "dI" },
	{ "RequestManulEndKouDao", "" },
	{ "QingQiuQueryScoreRank", "" },
	{ "QingQiuQueryFightDataRank", "" },
	{ "QingQiuRequestEnterPlayPart2", "" },
	{ "QingQiuQueryPlayState", "" },
	{ "QingQiuQueryNpcAndMonsterLocation", "" },
	{ "PlayerTryCertificationDone", "sssss" },
	{ "SendMonoAssemblyInfoBegin", "ds" },
	{ "SendMonoAssemblyInfoList", "s" },
	{ "SendMonoAssemblyInfoEnd", "s" },
	{ "CcTestLog", "s" },
	{ "QueryDivinationLuckyTaskInfo", "s" },
	{ "ExchangeTaoHuaBaoXiaRewardItem", "" },
	{ "RequestBroadcastTaoHuaBaoXiaHelp", "" },
	{ "TryGiveHelpTaoHuaBaoXia", "dI" },
	{ "QueryTaoHuaBaoXiaFlowerData", "" },
	{ "PreCheckGiveHelpTaoHuaBaoXia", "dIIsdd" },
	{ "QueryTaoHuaBaoXiaGetHelpCount", "" },
	{ "TaoHuaBaoXiaLog", "s" },
	{ "VerifyGetCinemaTicketMobile", "s", 500 },
	{ "FinishTaskWipeGlass", "Ib" },
	{ "CollectPlayerVoice", "b" },
	{ "RequestOpenVoiceQuestionWnd", "I" },
	{ "AnswerTaskVoiceQuestion", "IIb" },
	{ "StopEmbrace", "" },
	{ "RequestEmbracePlayerAnswer", "dIbs" },
	{ "QueryDuoMaoMaoPlayInfo", "" },
	{ "FinishMakeGesture", "II" },
	{ "RequestMakeGesture", "I" },
	{ "OpenJingLingWnd", "" },
	{ "EvalJingLingQuestion", "" },
	{ "ReleaseExpertQuestion", "Is" },
	{ "PushExpertAnswerBegin", "", 1000 },
	{ "PushExpertAnswer", "s" },
	{ "PushExpertAnswerEnd", "d" },
	{ "AdoptExpertAnswer", "ddI" },
	{ "DeleteExpertQuestion", "d" },
	{ "GetJingLingExpertRank", "" },
	{ "GetJingLingReadPointInfo", "" },
	{ "SetPaiMaiRemindWay", "I", 1000 },
	{ "RequestOpenSeparateWaterWnd", "I" },
	{ "RequestOperateWater", "Ii" },
	{ "RequestCheckOrderAsPresent", "dsssssssssIssb", 1000 },
	{ "NotifyFailedOrderInfo", "sss" },
	{ "SubmitItemAwardConfirm", "I" },
	{ "OpenMultipleQuestionsWnd", "II" },
	{ "AnswerMutipleQuestions", "II" },
	{ "RequestTransformInDuoMaoMao", "I" },
	{ "RequestExchangeQnPoint2Jade", "I" },
	{ "SetDefaultExchangeQnPoint2Jade", "i" },
	{ "AcceptZhuJueJuQingTask", "I" },
	{ "DiceBeginBet", "" },
	{ "DiceBeginDice", "" },
	{ "TaskFinishPlayCG", "I" },
	{ "RequestExchangeItems", "II" },
	{ "RequestAllocExtraGongXun", "bu" },
	{ "GetAllocExtraGongXunStatus", "" },
	{ "RequestInputZhouNianQingMenPaioInfo", "IIsss" },
	{ "FinishTaskPuzzle", "I" },
	{ "QueryMonthCardRedPoint", "s" },
	{ "RequestCompleteSongReorder", "sIIII" },
	{ "RequestCompositeDaMingPai", "s" },
	{ "QueryPGQYWaitInfo", "" },
	{ "UploadInfoToQos", "sss" },
	{ "RemoveQos", "s" },
	{ "QueryKeJuReviewInfo", "" },
	{ "RequestStartKeJuReview", "" },
	{ "RequestReviewKeJuQuestion", "I", 5000 },
	{ "RequestReviewKeJuDanMu", "I" },
	{ "TowerRequestPlayPlayerInfoList", "s" },
	{ "TowerDefenseAddTower", "IIdddIs" },
	{ "TowerDefenseRemoveTower", "Is" },
	{ "TowerDefenseUpgradeTower", "Is" },
	{ "RequestBiDongPlayerAnswer", "dI" },
	{ "RequestQiuHunPlayerAnswer", "db" },
	{ "SetExpressionAppearance", "iI" },
	{ "RequestCheckExpressionAppearance", "" },
	{ "ReportGyroCheck", "i" },
	{ "OpenCrossDouHunMatchRecordWnd", "" },
	{ "RequestZhouNianQingShareAward", "I" },
	{ "XiaLvPkPlayerSignUpPlay", "" },
	{ "XiaLvPkPlayerCancelSignUpPlay", "b" },
	{ "XiaLvPkRequestWatchPk", "II" },
	{ "XiaLvPkQueryStageMatchInfo", "" },
	{ "XiaLvPkQueryStageWatchInfo", "I" },
	{ "XiaLvPkQueryForceRoundData", "" },
	{ "XiaLvPkSubmitWenShiAnswer", "II" },
	{ "XiaLvPkLearnTempSkill", "II" },
	{ "XiaLvPkQueryTempSkill", "" },
	{ "CrossXiaLvPkRequestWatchPk", "II" },
	{ "CrossXiaLvPkQueryStageMatchInfo", "" },
	{ "CrossXiaLvPkQueryStageWatchInfo", "I" },
	{ "CrossXiaLvPkQueryForceRoundData", "" },
	{ "CrossXiaLvPkSubmitWenShiAnswer", "II" },
	{ "CrossXiaLvPkLearnTempSkill", "II" },
	{ "CrossXiaLvPkQueryTempSkill", "" },
	{ "RequestRegisterQmpkPersonalInfo", "", 1000 },
	{ "RequestTeleportToOtherQmpkServer", "I", 1000 },
	{ "RequestSetQmpkEquipPropertyAndWord", "IIsI", 1000 },
	{ "RequestSetQmpkEquipGem", "IIsI", 1000 },
	{ "RequestSetQmpkTalismanBasicWord", "IIsI", 1000 },
	{ "RequestSetQmpkTalismanExtraWord", "IIsI", 1000 },
	{ "RequestSetQmpkTalismanWenShi", "IIss", 1000 },
	{ "RequestReplaceQmpkRedTalisman", "IIsI", 1000 },
	{ "RequestReplaceQmpkXiaJiaTalismanSuit", "Is", 1000 },
	{ "RequestOperateQmpkLingShou", "sIIsIII", 1000 },
	{ "RequestSetQmpkPrayWordGroup", "III", 1000 },
	{ "RequestSelfQmpkPersonalInfo", "", 200 },
	{ "RequestUpdateQmpkPersonalInfo", "IIIIs", 1000 },
	{ "RequestQueryQmpkPlayerList", "iiiii", 3000 },
	{ "RequestQueryQmpkPlayerDetails", "d", 1000 },
	{ "RequestSelfQmpkZhanDuiInfo", "", 1000 },
	{ "RequestCreateQmpkZhanDui", "ss", 5000 },
	{ "RequestLeaveQmpkZhanDui", "", 1000 },
	{ "RequestRemoveQmpkZhanDuiMember", "d", 1000 },
	{ "RequestJoinQmpkZhanDui", "d", 200 },
	{ "RequestQmpkZhanDuiApplyList", "I", 1000 },
	{ "AcceptApplyPlayerJoinQmpkZhanDui", "d", 500 },
	{ "RefuseApplyPlayerJoinQmpkZhanDui", "d", 500 },
	{ "RequestClearQmpkZhanDuiApplyList", "", 5000 },
	{ "RequestSetQmpkChuZhanInfo", "IIIII", 1000 },
	{ "RequestQmpkBiWuChuZhanInfo", "", 1000 },
	{ "AdjustQmpkBiWuChuZhanInfo", "IIIII", 1000 },
	{ "RequestQmpkQieCuoTeamList", "I", 1000 },
	{ "QueryQmpkPlayerPrayWordGroup", "I", 1000 },
	{ "RequestGetQmpkItem", "I", 500 },
	{ "RequestSingUpQmpkQieCuo", "", 2000 },
	{ "RequestQmpkQieCuoWithOtherTeam", "d", 2000 },
	{ "RequestQueryQmpkZhanDuiList", "I", 1000 },
	{ "QueryQmpkZhanDuiInfoByMemberId", "d", 1000 },
	{ "RequestUpdateQmpkZhanDuiTitleAndKouHao", "ss", 2000 },
	{ "RequestCancelSingUpQmpkQieCuo", "", 2000 },
	{ "RequestSingUpQmpkFreeMatch", "", 2000 },
	{ "RequestCancelJoinQmpkZhanDui", "d", 500 },
	{ "QueryQmpkZhanDuiInfoByZhanDuiId", "d", 500 },
	{ "QueryQmpkQieCuoTeamByMemberId", "d", 1000 },
	{ "QueryQmpkBiWuZhanKuang", "", 1000 },
	{ "RequestCancelSingUpQmpkFreeMatch", "", 1000 },
	{ "RequestWatchQmpkBiWu", "Idd", 2000 },
	{ "QueryQmpkWatchList", "", 2000 },
	{ "SetQmpkZhanDuiRecruitInfo", "IIs", 2000 },
	{ "SearchQmpkZhanDuiByClassAndZhiHui", "III" },
	{ "BroadcastQmpkZhanDuiRecruitInfo", "IIs", 2000 },
	{ "QueryQmpkAllMemberFightData", "", 500 },
	{ "QueryQmpkTaoTaiSaiOverView", "I", 1000 },
	{ "RequestQmpkTaoTaiSaiWatchList", "I", 1000 },
	{ "RequestWatchQmpkTaoTaoSai", "II", 1000 },
	{ "RequestQmpkJingCaiVote", "dIId", 500 },
	{ "QueryQmpkZongJueSaiOverView", "I", 500 },
	{ "RequestQmpkZongJueSaiWatchList", "I", 1000 },
	{ "RequestWatchQmpkZongJueSai", "I", 1000 },
	{ "RequestQmpkJingCaiData", "d", 500 },
	{ "QueryQmpkLingShouLingFuIndex", "I", 500 },
	{ "RequestQmpkMeiRiYiZhanInfo", "", 1000 },
	{ "RequestSignUpQmpkMeiRiYiZhan", "", 1000 },
	{ "RequestCancelSignUpQmpkMeiRiYiZhan", "", 1000 },
	{ "RequestTransferQmpkZhanDuiLeader", "d", 5000 },
	{ "RequestQmpkJingCaiRecord", "", 500 },
	{ "RequestQmpkJingCaiList", "I", 500 },
	{ "QueryQmpkDetailFightData", "I" },
	{ "RequestQmpkShiLiZhiXingInfo", "I" },
	{ "RequestEnterGameVideoScene", "Is" },
	{ "LNT_ClientRequestEnter", "ds" },
	{ "LNT_RequestEnterInGatewayDone", "dis" },
	{ "LNT_RequestReconnectGame", "sssI" },
	{ "RequestEnterJXYSPlay", "I" },
	{ "QueryTeamGroupTeamMemberInfo", "" },
	{ "QueryTeamGroupApplicantInfo", "" },
	{ "RequestCreateTeamGroup", "" },
	{ "RequestDisbandTeamGroup", "" },
	{ "InvitePlayerJoinTeamGroup", "d" },
	{ "RequestJoinTeamGroup", "d" },
	{ "RequestJoinGroupByQuickMatch", "s" },
	{ "RequestAcceptTeamGroupApplicant", "d" },
	{ "RequestDelTeamGroupApplicant", "d" },
	{ "RequestLeaveTeamGroup", "" },
	{ "RequestExchangeTeamGroupMember", "dd" },
	{ "RequestMoveTeamGroupMember", "dI" },
	{ "RequestExchangeTeamInTeamGroup", "IIII" },
	{ "RequestSetTeamGroupLeaderId", "d" },
	{ "RequestSetTeamGroupManager", "d" },
	{ "RequestCancelTeamGroupManager", "d" },
	{ "RequestSetTeamLeaderInGroup", "d" },
	{ "RequestStartTeamGroupFollow", "", 1000 },
	{ "RequestCancelTeamGroupFollow", "" },
	{ "RequestTeamGroupConfirm", "" },
	{ "RequestKickTeamGroupMember", "d" },
	{ "RequestClearTeamGroupApplicant", "" },
	{ "QueryTeamGroupTeamHpInfo", "I" },
	{ "RequestOpenTeamGroupWnd", "" },
	{ "QueryTeamGroupMembers", "i" },
	{ "RequestAddTeamRecruit", "IIIsuIb", 1000 },
	{ "QueryMyTeamRecruit", "" },
	{ "CancelMyTeamRecruit", "" },
	{ "RequestQueryTeamRecruit", "IIsbbb" },
	{ "LimitRpc_ClientRequestOperation", "dis" },
	{ "QueryCanReceiveFoodDetectItemPlayers", "" },
	{ "RequestSendFoodDetectItem", "d" },
	{ "AcceptReceiveFoodDetectItem", "d" },
	{ "RefuseReceiveFoodDetectItem", "d" },
	{ "ConfirmGetFoodDetectItemFromPlayer", "d" },
	{ "AcceptGiveFoodDetectItem", "d" },
	{ "RefuseGiveFoodDetectItem", "d" },
	{ "RequestDetectFood", "ddIIs" },
	{ "SetCurrentDetectFoodDrug", "I" },
	{ "RequestPlaceFoodOnMap", "ddIIs" },
	{ "ConfirmAddNewFashion", "sII" },
	{ "ConfirmAddNewZuoQi", "sII" },
	{ "TalismanFaBaoItemExchange", "I" },
	{ "ReportLeaveMessage", "I" },
	{ "QueryGotSpecialFood", "" },
	{ "ConfirmAddExtraProduct", "IIsdI" },
	{ "QueryGuanNingHistoryWeekData", "" },
	{ "FinishTaskWipeMuBei", "I" },
	{ "RequestSetEnemyGuild", "I" },
	{ "RequestCancelEnemyGuild", "I" },
	{ "QueryCurrentHuoGuoMeterial", "" },
	{ "SubmitHuoGuoMeterial", "I" },
	{ "QueryRankJieBaiInfo", "s" },
	{ "MPTYSRequestSignUp", "b" },
	{ "MPTYSRequestRankInfo", "" },
	{ "MPTYSFinishSelectHorse", "s" },
	{ "MPTYSCancelSignUp", "" },
	{ "MPTYSRequestGuildOperation", "i" },
	{ "MPTYSRequestReserveGroup", "ii" },
	{ "MPTYSRequestScheduleInfo", "i" },
	{ "MPTYSRequestCheer", "id" },
	{ "TryConfirmBaptizeWord", "iisb" },
	{ "TryConfirmResetTalismanBasicWord", "IIs" },
	{ "TryConfirmResetTalismanExtraWord", "IIs" },
	{ "PlayerFinishShakeDevice", "" },
	{ "RequestShowSendBangHuaHongBao", "" },
	{ "RequestSendBangHuaHongBao", "s" },
	{ "OpenContinuousQuestionsWnd", "I" },
	{ "AnswerContinuousQuestions", "II" },
	{ "ConfirmBindRoleForAppHelper", "s" },
	{ "TryBindRoleForAppHelper", "" },
	{ "TaskBeginCooking", "I" },
	{ "CookingPlayAddMeterial", "II" },
	{ "RequestSetFamilyTreeSetting", "II" },
	{ "QueryFriendGuanNingRankData", "" },
	{ "RequestShootBirds", "I" },
	{ "FinishShootBirdsOnce", "II" },
	{ "RequestModifyGuildStatue", "u" },
	{ "CarnivalLotteryWithTimes", "I" },
	{ "ResendExpertQuestion", "dI" },
	{ "AcceptZouYueForPlayer", "d" },
	{ "RefuseZouYueForPlayer", "d" },
	{ "AcceptConcert", "dI" },
	{ "RefuseConcert", "dI" },
	{ "RequestJXYSRemainInfo", "" },
	{ "ApplyKeJuZhuBo", "" },
	{ "RequestOpenYinMengXiangWnd", "I" },
	{ "YinMengXiangAddMeterial", "II" },
	{ "FinishShakeYinMengXiang", "I" },
	{ "JoinKeJuDianTai", "s" },
	{ "LeaveKeJuDianTai", "" },
	{ "RequestTeleportToLastStageInChaoDuPlay", "" },
	{ "QueryWeddingDayGiftInfo", "" },
	{ "RequestWeddingDayGift", "I" },
	{ "BroadcastWeddingDayDeclareMsg", "s" },
	{ "RequestSculptureWeddingRing", "sIIssII" },
	{ "LoveQingQiuAnswerQuestion", "s" },
	{ "GetGMHuiLiuJinagLi", "" },
	{ "RequestRollPointInChaoDuPlay", "I" },
	{ "RequestTaBenWeddingRing", "sIIsII" },
	{ "SelfClsQueryCanTransferHolesSourceEquip", "" },
	{ "SelfClsCanSelectTransferHoleSourceEquip", "I" },
	{ "SelfClsQueryCanTransferHolesTargetEquip", "I" },
	{ "SelfClsCanSelectTransferHoleTargetEquip", "II" },
	{ "SelfClsConfirmTransferEquipHoleTarget", "II" },
	{ "RequestEnterHuLuWa", "I" },
	{ "QueryTeamConfirmWaitInfo", "" },
	{ "ShowSubtitleDone", "I" },
	{ "NationalDayParadeQueryRank", "" },
	{ "QueryYBZDPlayInfo", "" },
	{ "RequestOpenVerifyBindPhoneWnd", "" },
	{ "GetVerifyCommonDeviceCode", "" },
	{ "RequestVerifyCommonDevice", "s", 1000 },
	{ "FastAddWardRobeNumLimit", "" },
	{ "FastAddZuoQiNumLimit", "" },
	{ "RequestSendYueGongQiFu", "dIIss" },
	{ "QueryYueGongQiFuById", "s" },
	{ "UpdateExpressionAlert", "s" },
	{ "EvaluateExpertAnswer", "did" },
	{ "RequestLogCorrectJingLing", "sss", 1000 },
	{ "RequestLBSAward", "dd" },
	{ "RequestTransformInHulu", "I" },
	{ "QueryHuLuRank", "" },
	{ "ApplyColorSystemSkill", "I" },
	{ "CancelColorSystemSkill", "I" },
	{ "PlayerCanUsePresentItemToFriend", "iid" },
	{ "RequestUsePresentItemToFriend", "iiisd" },
	{ "CloseSecretShop", "I" },
	{ "OpenSecretShop", "I" },
	{ "RequestBuySecretShopItem", "II" },
	{ "ExtendMapiShopWnd", "" },
	{ "RequestGetHuLuDrop", "II" },
	{ "RequestGetHuLuSceneOverView", "" },
	{ "SetMonitorWords", "is" },
	{ "RequestFangShengGuGu", "" },
	{ "LLW_RequestLiaoluowanBoardWarship", "dIs" },
	{ "LLW_RequestEnterPlayConfirmMsg", "ds" },
	{ "LLW_RequestLand", "ds" },
	{ "LLWT_RequestGuideIndex", "ds" },
	{ "Xingguan_PlayerRequestLightUp1Star", "dis" },
	{ "Xingguan_PlayerRequestLightDownStars", "dis" },
	{ "Xingguan_PlayerRequestSaveTemplate", "dis" },
	{ "Xingguan_PlayerRequestLoadTemplate", "dis" },
	{ "Xingguan_PlayerRequestUnlockTemplate", "dis" },
	{ "Xingguan_PlayerRequestStoreExpForXingmangLevelUp", "ds" },
	{ "Xingguan_PlayerRequestClearStars", "ds" },
	{ "LianLianKanSelectOneChessman", "ii" },
	{ "LianLianKanRequestMatchTwoChessman", "iiii" },
	{ "LianLianKanRequestResetMap", "" },
	{ "RequestSingInHuLuSummon", "d" },
	{ "RequestDelWeddingTaBenOnRing", "sII" },
	{ "QueryLianLianKanFinalOverView", "" },
	{ "QueryLianLianKanFinalRoundDetails", "" },
	{ "RequestEnterLianLianKanPrePlay", "" },
	{ "DelWeddingRingTaBenSculptureContent", "sII" },
	{ "SubmitDouHunTrainItem", "sIII" },
	{ "RequestChuHaiTanBaoResult", "IIs" },
	{ "RequestChuHaiTanBaoItem", "IIIs" },
	{ "RequestHandDraw", "II" },
	{ "FinishHandDraw", "II" },
	{ "QueryHeChengGuiCost", "IIs" },
	{ "RequestCanHeChengGui", "IIsIIs" },
	{ "RequestHeChengGui", "IIsIIs" },
	{ "RequestCanSwitchEquipWordSet", "IIs" },
	{ "RequestSwitchEquipWordSet", "IIs" },
	{ "RequestRelationPlayerIds", "" },
	{ "QueryFeiShengWaitInfo", "" },
	{ "OpenDuoMaoMaoSignUpWnd", "" },
	{ "RequestPGQYScoreDouble", "Id" },
	{ "RequestCommonPinTu", "I" },
	{ "FinishCommonPinTu", "II" },
	{ "RequestQianKunDaiAntiScores", "" },
	{ "RequestRefineQianKunDai", "IIsI" },
	{ "RequestQianKunDaiRefineDone", "" },
	{ "RequestHeChengItem", "Iu" },
	{ "RequestHeChengJiNengShu", "Iu" },
	{ "RequestHeChengWenShi", "Iu" },
	{ "RequestAdjustedEquipInfo", "s" },
	{ "RequestSwitchFeiShengAppearanceXainFanStatus", "I" },
	{ "RequestXianFanPropertyCompare", "" },
	{ "RequestOpenDouHunRewardWnd", "" },
	{ "RequestDouHunRewardPlayer", "dI" },
	{ "RequestDouHunServerRewardInfo", "I" },
	{ "RequestDouHunRewardRankInfo", "" },
	{ "AcceptJoinTeamGroupInvite", "db" },
	{ "RequestRefineTalismanTripod", "u" },
	{ "TianJiangFuDanGiveUpHatching", "" },
	{ "SearchShiFuV2", "sub" },
	{ "PostShouTuMessageV2", "sub" },
	{ "RequestOpenBaGuaLuPos", "i" },
	{ "RequestBaGuaLuZhuRuLingqi", "iIb" },
	{ "RequestBaGuaLuZhuRuInfo", "i" },
	{ "RequestOpenBaGuaLu", "" },
	{ "RequestBaGuaLuHeChengScore", "u" },
	{ "RequestEnableDouHunFuTi", "I" },
	{ "QueryEquipForFeiShengQianKunDai", "I" },
	{ "RequestOpenFeiShengQianKunDaiWnd", "" },
	{ "RequestCheckBindAppHelper", "I" },
	{ "RequestGetZhuanZhiReturn", "" },
	{ "ShareZhuanZhiReturn", "" },
	{ "RequestSignUpHongLvZuoZhan", "" },
	{ "RequestCancelSignUpHongLvZuoZhan", "" },
	{ "JoinNewYearConcertRemoteChannel", "s" },
	{ "LeaveNewYearConcertRemoteChannel", "" },
	{ "NewYearConcertSendDanMu", "sb" },
	{ "NewYearConcertQiangMai", "" },
	{ "NewYearConcertLottery", "" },
	{ "NewYearConcertQueryPlayState", "" },
	{ "NewYearConcertQueryScoreRank", "" },
	{ "NewYearConcertSendFlower", "dsIi" },
	{ "RequestQianDaoAward", "I" },
	{ "RequestQMQDSignUpInfo", "" },
	{ "RequestSignUpQMQD", "" },
	{ "RequestCancelSignUpQMQD", "" },
	{ "RequestQMQDPlayInfo", "" },
	{ "RequestQMQDPlayerSoulOut", "" },
	{ "RequestSignUpXueQiu", "b" },
	{ "RequestCancelSignUpXueQiu", "" },
	{ "QueryTeamSignUpXueQiuWaitInfo", "" },
	{ "RequestRemoveXueQiuSkillItem", "iIb" },
	{ "QuerySignUpXueQiuStatus", "" },
	{ "QueryXueQiuPlayInfo", "" },
	{ "QueryXueQiuPlayLocationInfo", "" },
	{ "RequestSignUpXueQiuDouble", "b" },
	{ "RequestCancelSignUpXueQiuDouble", "" },
	{ "QueryTeamSignUpXueQiuDoubleWaitInfo", "" },
	{ "RequestRemoveXueQiuDoubleSkillItem", "iIb" },
	{ "QuerySignUpXueQiuDoubleStatus", "" },
	{ "QueryXueQiuDoublePlayInfo", "" },
	{ "QueryXueQiuDoublePlayLocationInfo", "" },
	{ "QueryXueQiuSeasonRankData", "", nil, nil, "lua" },
	{ "PlayerSubmitTaskWithSelectedItem", "III" },
	{ "QueryHongLvZuoZhanSignUpInfo", "" },
	{ "GetWorldPaiMaiPublicityGoods", "IIII" },
	{ "RequestUseMysticRoseSeed", "IIsds" },
	{ "RequestCollectMysticRose", "I" },
	{ "RequestSubmitNianShouPuzzle", "II" },
	{ "RequestStartNianShouPinDuoDuo", "" },
	{ "RequestOpenFaBaoDingHeChengWnd", "" },
	{ "RequestSignUpNSDZZ", "" },
	{ "RequestNSDZZBattleInfo", "" },
	{ "RequestCancelSignUpNSDZZ", "" },
	{ "RequestNSDZZNPCPosInfo", "" },
	{ "RequestNSDZZTeamInfo", "" },
	{ "PlayerInputContactInfoByUseItem", "IIsss" },
	{ "RequestRideOffBalloon", "I" },
	{ "RequestVoiceLovePlayStatus", "" },
	{ "RequestTeleportToVoiceLoveScene", "bsii" },
	{ "RequestGuessRiddle", "IIs" },
	{ "RequestGetHuYiTianFenXiangItem", "" },
	{ "HandleLbsSimpleMsg", "ss" },
	{ "ConfirmFeiShengRule", "" },
	{ "OpenSIMCardApplyURL", "" },
	{ "RequestTeleportToXinYiClub", "" },
	{ "RequestBbsShareToken", "I" },
	{ "RequestContinueGuanNingChallengeTask", "b" },
	{ "SendGuanNingChallengeTaskChoice", "I" },
	{ "QueryGuanNingChallengeTaskChoice", "" },
	{ "QueryNosUploadToken", "I", 10000 },
	{ "RequestBbsShareDone", "" },
	{ "RequestTriggerBaptizeBbsShare", "b" },
	{ "RequestSetDunPaiHide", "I" },
	{ "ShareQianLiYanPlayerPos", "si" },
	{ "RequestQueryJXYSBossPos", "" },
	{ "SendSystemIMMsg", "s" },
	{ "RequestDouDiZhuReady", "b", nil, nil, "lua" },
	{ "RequestQiangDiZhu", "b", nil, nil, "lua" },
	{ "RequestChuPai", "u", nil, nil, "lua" },
	{ "RequestDouDiZhuTuoGuan", "b", nil, nil, "lua" },
	{ "QueryGuildDouDiZhuFightRank", "", nil, nil, "lua" },
	{ "RequestLeaveDouDiZhu", "", nil, nil, "lua" },
	{ "RequestSendDouDiZhuShortMessage", "i", nil, nil, "lua" },
	{ "RequestResetDouDiZhuScore", "", nil, nil, "lua" },
	{ "QueryGuildDouDiZhuChampionRecord", "", nil, nil, "lua" },
	{ "DouDiZhuSendDanMu", "sb", nil, nil, "lua" },
	{ "RequestWatchPlayerDouDiZhu", "d", nil, nil, "lua" },
	{ "RequestSignUpGuildDouDiZhu", "", nil, nil, "lua" },
	{ "RequestVoteGuildDouDiZhu", "d", nil, nil, "lua" },
	{ "RequestCancelVoteGuildDouDiZhu", "d", nil, nil, "lua" },
	{ "ExtendTianQiShopWnd", "" },
	{ "MapiRequestLearnNewSkill", "sis" },
	{ "RequestQuitZhiYuanPlay", "" },
	{ "SendZhiYuanInstruction", "ddd", 100 },
	{ "ZhiYuanHitOther", "d", 1000 },
	{ "ZhiYuanHitDrop", "s", 100 },
	{ "RequestJoinZhiYuan", "IdI" },
	{ "ForceSendZhiYuanInstruction", "ddd" },
	{ "RequestYuanYuRankData", "" },
	{ "CrystalRequestEnterPrepare", "" },
	{ "CrystalRequestChangeAuraMode", "II" },
	{ "CrystalSearchCrystalAuraMode", "I" },
	{ "NeverRemindTrackToDoShiMen", "" },
	{ "GuildPartyRequestCommitFragment", "II" },
	{ "GuildPartyRequestOpenPlay", "" },
	{ "QueryHasGuanNingChallengeTaskChoice", "" },
	{ "RequestUpgradeEquipWordCost", "IIs" },
	{ "RequestUpgradeEquipWord", "IIs" },
	{ "RequestViewedMessageIdx", "" },
	{ "RequestChuiLianEquipWordCost", "IIsI" },
	{ "RequestChuiLianEquipWord", "IIsI" },
	{ "IdentifySkillRequestWashSkill", "si" },
	{ "IdentifySkillRequestWashSkillCost", "s" },
	{ "IdentifySkillRequestApplyTempSkill", "sb" },
	{ "IdentifySkillRequestExtendSkill", "sIb" },
	{ "IdentifySkillRequestExchangeSkill", "ssb" },
	{ "IdentifySkillSetDisable", "sb" },
	{ "IdentifySkillUpdateEquipScore", "s" },
	{ "IdentifySkillUpdateOtherPlayerEquipScore", "ds", 1000 },
	{ "RequestSetNotAllowExpressionAction", "sI", nil, nil, "lua" },
	{ "RequestOpenUnlockWithKeyWnd", "I" },
	{ "FinishUnlockWithKeyTask", "Ib" },
	{ "RequestInvitePlayerWithHuiLiuDan", "dIIs" },
	{ "QueryHuiLiuDanInvitePlayers", "" },
	{ "RequestMoveItemOutFromItemBagForOnShelf", "III" },
	{ "ZhouNianShopQueryPackage", "" },
	{ "ZhouNianShopBuyPackage", "I" },
	{ "ZhouNianShopStatusQuery", "" },
	{ "ZhouNianShopAlertStatusQuery", "" },
	{ "FinishLightingTask", "I" },
	{ "FinishLianXingXingTask", "I" },
	{ "CcChatQueryPlayerToken", "b", 3000 },
	{ "RecordJingLingCustomInfo", "Isssss", 1000 },
	{ "RequestOpenLightingWnd", "I" },
	{ "CheckWorldCupJingCaiAwardReddot", "" },
	{ "QueryWorldCupTodayJingCaiInfo", "" },
	{ "RequestWorldCupTodayJingCaiVote", "IsI" },
	{ "QueryWorldCupTodayJingCaiRecord", "" },
	{ "RequestReceiveWorldCupJingCaiAward", "" },
	{ "QueryWorldCupTodayJingCaiDetail", "I" },
	{ "QueryWorldCupHomeTeamPlayInfo", "" },
	{ "RequestSetWorldCupHomeTeam", "I" },
	{ "RequestReceiveWorldCupHomeTeamAward", "" },
	{ "QueryWorldCupAgendaInfo", "" },
	{ "CancelCollectPlayerShopItemBegin", "" },
	{ "CancelCollectPlayerShopItemEnd", "" },
	{ "QueryMyBQPForSubmit", "" },
	{ "SubmitBingQiPuEquip", "sIIsIb" },
	{ "QueryBQPEquipDetails", "s" },
	{ "QueryBQPRank", "II" },
	{ "PraiseBQPInSubmitStage", "sI" },
	{ "VoteBQPInVoteStage", "sI" },
	{ "RequestBQPRecommendEquip", "" },
	{ "ShareBQPToWorld", "s", nil, nil, "lua" },
	{ "QuerySharedBQPInfo", "s", nil, nil, "lua" },
	{ "QueryBQPStage", "", nil, nil, "lua" },
	{ "QuerySponsorRank", "", nil, nil, "lua" },
	{ "BQPInvestForSponsorRank", "sd", nil, nil, "lua" },
	{ "QueryBQPPreciousRank", "", nil, nil, "lua" },
	{ "BQPPreciousRankSubmit", "sI", nil, nil, "lua" },
	{ "QueryBQPHistory", "IIb", nil, nil, "lua" },
	{ "TryBingQiPuEquip", "s", nil, nil, "lua" },
	{ "BingQiPuOffShelf", "s", nil, nil, "lua" },
	{ "QueryBingQiPuHistoryDetail", "Is", nil, nil, "lua" },
	{ "QueryBingQiPuMisc", "Iu", nil, nil, "lua" },
	{ "RequestShareLanRuoSiToWorld", "" },
	{ "FinishBlowBubble", "dIIs" },
	{ "RequestPokeSuperBubble", "" },
	{ "RequestSignUpZhuoJi", "", nil, nil, "lua" },
	{ "RequestCancelSignUpZhuoJi", "", nil, nil, "lua" },
	{ "QueryZhuoJiPlayInfo", "", nil, nil, "lua" },
	{ "RequestSignUpForCrossCuJu", "", nil, nil, "lua" },
	{ "QueryCuJuWeekRank", "", nil, nil, "lua" },
	{ "QueryCuJuTotalRank", "", nil, nil, "lua" },
	{ "QueryCuJuCrossSignUpInfo", "", nil, nil, "lua" },
	{ "RequestUseCuJuLottery", "sII", nil, nil, "lua" },
	{ "HandkerchiefFlyFinished", "", nil, nil, "lua" },
	{ "FinishDuLiNiangPicTask", "I", nil, nil, "lua" },
	{ "RequestSBCSFinishInfo", "", nil, nil, "lua" },
	{ "DoSayDialogEnd", "I", 100 },
	{ "RequestCancelSignUpForCrossCuJu", "", nil, nil, "lua" },
	{ "RequestMakeShenBing", "IIsIIs", nil, nil, "lua" },
	{ "RequestTrainShenBing", "IIsu", nil, nil, "lua" },
	{ "RequestBreakShenBingTrainLevel", "IIsIIs", nil, nil, "lua" },
	{ "RequestRecastShenBing", "IIsb", nil, nil, "lua" },
	{ "RequestRestoreShenBing", "IIsb", nil, nil, "lua" },
	{ "RequestExchangeQianYuan", "Iu", nil, nil, "lua" },
	{ "QueryJXYSPlayerDieCount", "I" },
	{ "JiXiangWuRequestTrain", "IIsI", nil, nil, "lua" },
	{ "JiXiangWuRequestHuaRong", "IIs", nil, nil, "lua" },
	{ "JiXiangWuRequestSetColorAndWeaponColor", "IIsII", nil, nil, "lua" },
	{ "RequestEnterShenBing", "I", nil, nil, "lua" },
	{ "RequestOpenTianQiShop", "" },
	{ "QueryWuCaiShaBingSignUpStatus", "", nil, nil, "lua" },
	{ "RequestSignUpWuCaiShaBing", "", nil, nil, "lua" },
	{ "RequestCancelSignUpWuCaiShaBing", "", nil, nil, "lua" },
	{ "RequestSubmitMaterial", "", nil, nil, "lua" },
	{ "RequestQueryWCSBBattleInfo", "", nil, nil, "lua" },
	{ "RequestStealMaterial", "I", nil, nil, "lua" },
	{ "RequestDouHunPraiseServerList", "", nil, nil, "lua" },
	{ "RequestDouHunPraiseServerInfo", "s", nil, nil, "lua" },
	{ "RequestDouHunPraiseRank", "", nil, nil, "lua" },
	{ "RequestDouHunPraisePlayer", "d", nil, nil, "lua" },
	{ "QueryCloseFriendList", "" },
	{ "QueryCloseFriendInfo", "" },
	{ "InviteCloseFriendHuiLiu", "ds", 5000 },
	{ "ReqeustPackChampionStatue", "I", nil, nil, "lua" },
	{ "ReqeustMoveChampionStatue", "IIII", nil, nil, "lua" },
	{ "RequestOpenChangeChampionStatueWnd", "I", nil, nil, "lua" },
	{ "ReqeustChangeChampionStatue", "IId", nil, nil, "lua" },
	{ "ReqeustCheckBQPComment", "", nil, nil, "lua" },
	{ "QueryMergeBattleOverview", "", nil, nil, "lua" },
	{ "QueryMergeBattleInviteHuiLiuInfo", "", nil, nil, "lua" },
	{ "QueryMergeBattleBindCodePlayerName", "d", nil, nil, "lua" },
	{ "QueryMergeBattleHuiLiuBindStatus", "", nil, nil, "lua" },
	{ "MergeBattleHuiLiuPlayerRequestBind", "d", nil, nil, "lua" },
	{ "QueryMergeBattlePersonalEvents", "", nil, nil, "lua" },
	{ "QueryMergeBattlePersonalScores", "", nil, nil, "lua" },
	{ "RequestConfirmStamp", "I", nil, nil, "lua" },
	{ "RequestUseFreeStamp", "IsII", nil, nil, "lua" },
	{ "RequestSmallPrizeDraw", "I", nil, nil, "lua" },
	{ "RequestBigPrizeDraw", "", nil, nil, "lua" },
	{ "QueryCarnivalTicketStock", "", nil, nil, "lua" },
	{ "RequestBuyCarnivalTicket", "I", nil, nil, "lua" },
	{ "CBG_RequestGetConditionsInfo", "dss", nil, nil, "lua" },
	{ "CBG_RequestRegister", "ds", nil, nil, "lua" },
	{ "CBG_RequestOnShelfSmsCode", "ds", nil, nil, "lua" },
	{ "CBG_RequestGameOnShelf", "dIsIs", nil, nil, "lua" },
	{ "CBG_RequestOnShelf", "dIs", nil, nil, "lua" },
	{ "CBG_RequestAllGameOnShelfInfos", "ds", nil, nil, "lua" },
	{ "CBG_RequestGameOffShelf", "ds", nil, nil, "lua" },
	{ "CrossSXDDZChallenged_Accept", "", nil, nil, "lua" },
	{ "QueryBattleStartEnterConfirm_Accept", "", nil, nil, "lua" },
	{ "CrossSXDDZChallenged_Escape", "", nil, nil, "lua" },
	{ "QueryBattleStartEnterConfirm_Escape", "", nil, nil, "lua" },
	{ "ApplyWatchCrossSXDDZ", "I", nil, nil, "lua" },
	{ "ApplyChallengeCrossSXDDZChampion", "I", nil, nil, "lua" },
	{ "CrossSXDDZQueryCurrentChallengeInfo", "I", nil, nil, "lua" },
	{ "RequestExchangeYuanBaoWithBaoShi", "II", nil, nil, "lua" },
	{ "RequestEnterCrossSXDDZPrepareScene", "", nil, nil, "lua" },
	{ "JiXiangWuRequestSyncWxProgress", "IIss", nil, nil, "lua" },
	{ "SendPVPCommand", "I", 1000, nil, "lua" },
	{ "RequestSyncGuiMenGuanInfo", "", nil, nil, "lua" },
	{ "GuiMenGuanRequestTrackToBoss", "s", nil, nil, "lua" },
	{ "NotifyGameAudioOpen", "" },
	{ "ProduceShenBingRanLiaoByMaterial", "II", nil, nil, "lua" },
	{ "ProduceShenBingRanLiaoByRanLiao", "III", nil, nil, "lua" },
	{ "RequestShenBingRanSe", "IIsuII", nil, nil, "lua" },
	{ "ConfirmBindRoleForDaShen", "ss", nil, nil, "lua" },
	{ "TryBindRoleForDaShen", "", nil, nil, "lua" },
	{ "TryUnBindRoleForDaShen", "", nil, nil, "lua" },
	{ "RequestOpenScreenBroken", "I" },
	{ "FinishOpenScreenBroken", "I", nil, nil, "lua" },
	{ "RequestResetShenBingRanSe", "IIs", nil, nil, "lua" },
	{ "RequestSubmitServerSubmitItem", "IIII", nil, nil, "lua" },
	{ "JiXiangWuRequestTrainOneKey", "IIs", nil, nil, "lua" },
	{ "CreateCityUnit", "sI", nil, nil, "lua" },
	{ "BuildCityUnit", "sIIiii", nil, nil, "lua" },
	{ "MoveCityUnit", "sIIiii", nil, nil, "lua" },
	{ "PackUpCityUnit", "sII", nil, nil, "lua" },
	{ "UpgradeCityUnit", "sIII", nil, nil, "lua" },
	{ "RequestUseCityUnit", "sII", nil, nil, "lua" },
	{ "RequestOneKeyPackAllUnit", "s", nil, nil, "lua" },
	{ "RequestUseCityWarLayoutTemplateCheck", "sI", nil, nil, "lua" },
	{ "RequestLoadCityWarLayoutBegin", "s" },
	{ "RequestLoadCityWarLayout", "su" },
	{ "RequestLoadCityWarLayoutEnd", "s" },
	{ "RequestBatchUpgradeCityUnit", "sIbI", nil, nil, "lua" },
	{ "RequestRebornInOwnCity", "" },
	{ "ConfirmOccupyHuFu", "dIIsb", nil, nil, "lua" },
	{ "QueryTerritoryMapsOverview", "", nil, nil, "lua" },
	{ "QueryTerritoryMapDetail", "I", nil, nil, "lua" },
	{ "QueryTerritoryCityTips", "II", nil, nil, "lua" },
	{ "QueryMyCityData", "I", nil, nil, "lua" },
	{ "QueryMirrorWarMapInfo", "", nil, nil, "lua" },
	{ "DeclareMirrorWar", "d", 3000, nil, "lua" },
	{ "QueryMirrorWarSummary", "", nil, nil, "lua" },
	{ "QueryMirrorWarPlayInfo", "I", nil, nil, "lua" },
	{ "BuildMirrorWarRebornPoint", "iii", nil, nil, "lua" },
	{ "DestroyMirrorWarRebornPoint", "I", nil, nil, "lua" },
	{ "RequestRebornInMirrorWarRebornPoint", "I", nil, nil, "lua" },
	{ "QueryGuildCityWarMaterial", "", nil, nil, "lua" },
	{ "RequestLeaveCityWarTerritory", "", nil, nil, "lua" },
	{ "RequestBackToOwnCity", "", nil, nil, "lua" },
	{ "RequestAlterCityMission", "ds", nil, nil, "lua" },
	{ "RequestAlterCityMissionEnd", "ds", nil, nil, "lua" },
	{ "RequestEnterMirrorWarPlay", "I", nil, nil, "lua" },
	{ "QueryMirrorWarWatchInfo", "", nil, nil, "lua" },
	{ "RequestWatchMirrorWar", "I", nil, nil, "lua" },
	{ "QueryCityWarHistory", "", nil, nil, "lua" },
	{ "QueryCityWarHistoryPlaySummary", "I", nil, nil, "lua" },
	{ "ExchangeCityWarGuildZiCai", "IIsI", nil, nil, "lua" },
	{ "QueryCityWarServerList", "", nil, nil, "lua" },
	{ "QueryCityWarServerCityList", "I", nil, nil, "lua" },
	{ "QueryMirrorWarPlayNum", "", nil, nil, "lua" },
	{ "QueryCityWarOccupyOpen", "", nil, nil, "lua" },
	{ "QueryCityWarGuildToCityHash", "", nil, nil, "lua" },
	{ "QueryCityFlag", "dd", nil, nil, "lua" },
	{ "QueryCityInfo", "dd", nil, nil, "lua" },
	{ "RequestHelpDeliveryBaby", "d" },
	{ "RequestLoadBabyFromPlayer", "I" },
	{ "RequestInspectInfo", "" },
	{ "RequestRestoreBaby", "s" },
	{ "RequestRenameBaby", "ss" },
	{ "RequestPlanBaby", "sub", nil, nil, "lua" },
	{ "RequestReceiveOnePlanAward", "sd" },
	{ "RequestReceiveAllPlanAward", "s" },
	{ "RequestRefreshBabyPlan", "s" },
	{ "FinishSelectDialog", "u" },
	{ "RequestAcceptTaskFromBaby", "sI" },
	{ "RequestReceiveQiYuAward", "sI" },
	{ "RequestSummonBaby", "s" },
	{ "RequestPackBaby", "s" },
	{ "RequestActiveBabyFashionColor", "sI" },
	{ "RequestAddBabyFashion", "sI" },
	{ "RequestRemoveBabyFashion", "sI" },
	{ "RequestActiveBabyExpression", "sI" },
	{ "RequestSaveBabyFashionInfo", "sIIII" },
	{ "RequestOpenYangYuWnd", "" },
	{ "QueryBabyNpcInteract", "II" },
	{ "RequestSetBabyFollow", "I" },
	{ "RequestSetBabyHaveFun", "I" },
	{ "RequestAcceptQiYuTaskFromBaby", "sI" },
	{ "QueryBabyParentsName", "s" },
	{ "RequestLoadBabyFromHouse", "I" },
	{ "QueryHouseBabyList", "sd" },
	{ "RequestReturnBaby", "s" },
	{ "RequestFeedingBabyNpc", "II" },
	{ "SendChatMessageToBaby", "sIbss" },
	{ "QueryBabyFollowStatus", "s" },
	{ "RequestOpenBabyChatWnd", "I" },
	{ "QueryBabyGradeAndExp", "I" },
	{ "QueryBabyNpcInfo", "" },
	{ "RequestLoadBabyFromOrphan", "s" },
	{ "RequestActiveBabyQiChang", "sb", nil, nil, "lua" },
	{ "RequestSetShowQiChangId", "sI" },
	{ "RequestChangeBabyStatus", "sI" },
	{ "RequestPlantAndHarvestInHouse", "s" },
	{ "RequestDriveVehicle", "I" },
	{ "RequestTakeOnCombatVehicle", "I" },
	{ "RequestTakeOffCombatVehicle", "" },
	{ "RequestRecycleTaskItem", "Is" },
	{ "RequestSignUpGQJC", "", nil, nil, "lua" },
	{ "RequestCancelSignUpGQJC", "", nil, nil, "lua" },
	{ "RequestPlayInfoGQJC", "", nil, nil, "lua" },
	{ "RequestSelectTempGQJC", "III", nil, nil, "lua" },
	{ "RequestJingLingExpertInfo", "d" },
	{ "ErSheRequestOpenWnd", "" },
	{ "ErSheLeave", "" },
	{ "ErSheQueryOuyuRole", "" },
	{ "ErSheRequestStartJuQing", "I" },
	{ "ErSheReportFinishedJuQingBegin", "II" },
	{ "ErSheReportFinishedJuQingPath", "II" },
	{ "ErSheReportFinishedJuQingEnd", "" },
	{ "ErSheQueryZhaoHuanRoleList", "" },
	{ "ErSheRequestZhaoHuanRole", "I" },
	{ "ErSheRequestTouchRole", "II" },
	{ "ErSheQueryGiftList", "" },
	{ "ErSheRequestBuyGift", "II" },
	{ "ErSheQueryFinishedJuQing", "" },
	{ "ErSheGetJuQingHistory", "I" },
	{ "QueryCityInfoForCityWarZhanLong", "", nil, nil, "lua" },
	{ "RequestSetCityWarZhanLongEnemyGuildId", "d", nil, nil, "lua" },
	{ "RequestGrabMaterialForCityWarZhanLong", "IsII", nil, nil, "lua" },
	{ "RequestSetFlagContentForCityWarZhanLong", "sIs", nil, nil, "lua" },
	{ "RequestPlaceFlagForCityWarZhanLong", "sI", nil, nil, "lua" },
	{ "RequestPracticeLianShenSkill", "II", 200, nil, "lua" },
	{ "QueryCityWarPlayOpenStatus", "", nil, nil, "lua" },
	{ "RequestTeleportToTerritoryByBigMap", "", nil, nil, "lua" },
	{ "RequestSetBiaoCheMap", "II", nil, nil, "lua" },
	{ "RequestResetBiaoCheMap", "II", nil, nil, "lua" },
	{ "QuerySetBiaoChePower", "", nil, nil, "lua" },
	{ "QueryBiaoChePosOnMap", "", nil, nil, "lua" },
	{ "QueryGuildAltarInfo", "", nil, nil, "lua" },
	{ "RequestTrackToSelfBiaoChe", "", nil, nil, "lua" },
	{ "RequestGiveUpYaYunTask", "", nil, nil, "lua" },
	{ "RequestTrackToGuildBiaoChe", "I", nil, nil, "lua" },
	{ "ConfirmFindYaYunBiaoChe", "", nil, nil, "lua" },
	{ "QuerySubmitResToBiaoChe", "I", nil, nil, "lua" },
	{ "QueryGuildSilveForBiaoChe", "II", nil, nil, "lua" },
	{ "RearingTestSubmitAnswer", "II" },
	{ "RearingTestStopTest", "" },
	{ "RequestUseBombForCityWarZhanLong", "sI" },
	{ "RequestRepairWallForCityWarZhanLong", "sI" },
	{ "RequestDamageQiXieForCityWarZhanLong", "sI" },
	{ "RequestRepairQiXieForCityWarZhanLong", "sI" },
	{ "RequestOpenGrabWndForCityWarZhanLong", "sI" },
	{ "RequestSetShenBingEffectHide", "I" },
	{ "LianLianKan2018SelectOneChessman", "ii", nil, nil, "lua" },
	{ "LianLianKan2018CancelSelectOneChessman", "ii", nil, nil, "lua" },
	{ "LianLianKan2018RequestMatchTwoChessman", "iiii", nil, nil, "lua" },
	{ "RequestEnterLianLianKan2018Play", "", nil, nil, "lua" },
	{ "LianLianKanPickAnswer", "i", nil, nil, "lua" },
	{ "QueryLianLianKan2018Rank", "", nil, nil, "lua" },
	{ "LianLianKanRequestResetChess", "", nil, nil, "lua" },
	{ "RequestLianLianKanBonusPool", "", nil, nil, "lua" },
	{ "OpenCityWarZiCaiShop", "", nil, nil, "lua" },
	{ "BuyCityWarZiCaiShopItem", "I", nil, nil, "lua" },
	{ "QueryCityWarGuildZiCaiInfo", "", nil, nil, "lua" },
	{ "SubmitCityWarGuildZiCai", "I", nil, nil, "lua" },
	{ "BaoWeiNanGuaConfirmEnter", "", nil, nil, "lua" },
	{ "SHTYSetRingPos", "ii", 1000 },
	{ "SetPropertyAppearanceSettingInfo", "II" },
	{ "QueryAllZhongCaoCount", "", nil, nil, "lua" },
	{ "RequestZhongCao", "ii", nil, nil, "lua" },
	{ "RequestActivatedAltarCount", "", nil, nil, "lua" },
	{ "PlayerCaptureScreenInGame", "" },
	{ "FinishHuaZhuZiPicTask", "I" },
	{ "QueryYanYunResNpcCount", "" },
	{ "QuerySetGuildBiaoCheCostAll", "I" },
	{ "RequestApplyManYueJiuForBaby", "s" },
	{ "ResponseCheckAcceleration", "d", nil, nil, "lua" },
	{ "RequestSetMemberExtraName", "ds", nil, nil, "lua" },
	{ "BabyFundRequestShowEntrance", "" },
	{ "BabyFundRequestBuyFund", "" },
	{ "BabyFundRequestRecvPackage", "I" },
	{ "QueryChristmasTreeInfo", "", nil, nil, "lua" },
	{ "RaiseChristmasTree", "III", nil, nil, "lua" },
	{ "GetGiftFromChristmasTree", "I", nil, nil, "lua" },
	{ "RequestEditCard", "sIIdsssd" },
	{ "RequestPutCardIntoTree", "IsII" },
	{ "RequestExchangeCardToYinPiao", "I" },
	{ "RequestBabyDoExpression", "III", nil, nil, "lua" },
	{ "QueryBabyUnLockExpressionGroupInfo", "I" },
	{ "QueryNeteaseMemberWelfareInfo", "", nil, nil, "lua" },
	{ "RequestGetNeteaseMemberLevelAward", "", nil, nil, "lua" },
	{ "RequestGetNeteaseMemberConsumeFirstLvAward", "", nil, nil, "lua" },
	{ "RequestGetNeteaseMemberConsumeSecondLvAward", "", nil, nil, "lua" },
	{ "RequestSendChistmasGiftToHouse", "I", nil, nil, "lua" },
	{ "RequestUseNeteaseMemberQualification", "ss" },
	{ "RequestSummonMonsterForCityWarZhanLong", "sI" },
	{ "RequestSummonSpyNpcForCityWarZhanLong", "sI" },
	{ "NewYearTaskBookRefreshTask", "sI", nil, nil, "lua" },
	{ "NewYearTaskBookFinishTask", "sI", nil, nil, "lua" },
	{ "NewYearTaskBookRecvReward", "sI", nil, nil, "lua" },
	{ "NewYearLuckyDrawRequestDraw", "sII", nil, nil, "lua" },
	{ "RequestLockLingShouPartnerSkillPos", "sII", nil, nil, "lua" },
	{ "RequestLingShouLearnPartnerSkill", "Iss", nil, nil, "lua" },
	{ "RequestUpgradeLingShouPartnerSkill", "sII", nil, nil, "lua" },
	{ "RequestExchangePartnerSkillBook", "IsI", nil, nil, "lua" },
	{ "RequestBindPartner_LingShou", "s", nil, nil, "lua" },
	{ "RequestCancelBindPartner_LingShou", "s", nil, nil, "lua" },
	{ "RequestImporveLingShouBabyPartnerAffinity", "sIs", nil, nil, "lua" },
	{ "RequestLingShouLearnPartnerSkillBySkillCls", "sd", nil, nil, "lua" },
	{ "QueryMonsterSiegeFightData", "I", nil, nil, "lua" },
	{ "ResetMonsterSiegeFightData", "I", nil, nil, "lua" },
	{ "BroadcastMonsterSiegeFightData", "I", nil, nil, "lua" },
	{ "QueryMonsterSiegfeCopyMemberInfo", "", nil, nil, "lua" },
	{ "RequestEnableWinterSceneAtHome", "uuuu" },
	{ "RequestEnableWinterFunitureAtHome", "I" },
	{ "RequestSwitchFurnitureWinterAppearance", "I" },
	{ "RequestSwitchFurnitureWinterAppearanceByTemplateId", "iI", 500 },
	{ "RequestSwitchWinterSceneAtHome", "" },
	{ "QuerySmartGMTokenAndUrl", "s" },
	{ "QXJTCheckReward", "" },
	{ "RequestSaveYingLingExtraSkillTemplate", "IIuuu" },
	{ "RequestYingLingTransform", "I" },
	{ "ReportBianLunResult", "b", nil, nil, "lua" },
	{ "ReportCompleteQiQiaoBan", "", nil, nil, "lua" },
	{ "SetJieBanQiChang", "sI", nil, nil, "lua" },
	{ "RequestAdmonishBaby", "sIbb", nil, nil, "lua" },
	{ "QueryBabyIdByEngineId", "I", nil, nil, "lua" },
	{ "QueryBabyOwnerPlayerId", "II", nil, nil, "lua" },
	{ "PlayerRequestBlow", "", nil, nil, "lua" },
	{ "QueryXueQiuSeasonRank", "II", nil, nil, "lua" },
	{ "RequestSendNeteaseMemberTokenBegin", "", nil, nil, "lua" },
	{ "RequestSendNeteaseMemberToken", "s", nil, nil, "lua" },
	{ "MoveAllPigCoinToPot", "", nil, nil, "lua" },
	{ "ApplyPigCoinPotReward", "I", nil, nil, "lua" },
	{ "RequestFireFireworks", "I", nil, nil, "lua" },
	{ "Request2019FireworkGift", "", nil, nil, "lua" },
	{ "RequestExchangeHonorCoin", "", nil, nil, "lua" },
	{ "RequestExchangeHonorCoinMeterial", "I", nil, nil, "lua" },
	{ "ConfirmCommitRiddle", "II", nil, nil, "lua" },
	{ "RequestStartMengHuaLu", "", nil, nil, "lua" },
	{ "RequestOpenMengHuaLuGrid", "I", nil, nil, "lua" },
	{ "RequestPickMengHuaLuItem", "I", nil, nil, "lua" },
	{ "RequestMengHuaLuAttack", "I", nil, nil, "lua" },
	{ "RequestUseMengHuaLuSkill", "II", nil, nil, "lua" },
	{ "RequestMengHuaLuBuyItem", "II", nil, nil, "lua" },
	{ "RequestMengHuaLuSetQiChang", "u", nil, nil, "lua" },
	{ "ErSheRequestSaveArPlayDataBegin", "s", nil, nil, "lua" },
	{ "ErSheRequestSaveArPlayData", "ss", nil, nil, "lua" },
	{ "ErSheRequestSaveArPlayDataEnd", "s", nil, nil, "lua" },
	{ "ErSheRequestLoadArPlayData", "s", nil, nil, "lua" },
	{ "RequestSendLuckyMoneyToBaby", "II", nil, nil, "lua" },
	{ "QueryBabyLuckyMoneyHistory", "I", nil, nil, "lua" },
	{ "RequestLingShouScoreInfo", "", nil, nil, "lua" },
	{ "QueryMengHuaLuQiChang", "", nil, nil, "lua" },
	{ "QueryMengHuaLuSkill", "", nil, nil, "lua" },
	{ "RequestStartMengHuaLuWithChoice", "b", nil, nil, "lua" },
	{ "RequestValentineQYSJList", "", nil, nil, "lua" },
	{ "RequestValentineQYSJInfo", "d", nil, nil, "lua" },
	{ "RequestAcceptValentineQYSJTask", "d", nil, nil, "lua" },
	{ "ValentineQYSJWritePromiseAndSubmit", "ds", nil, nil, "lua" },
	{ "RequestValentineQSYYInfo", "", nil, nil, "lua" },
	{ "SendLongIMMsgForGM", "dbbssss", 300, nil, "lua" },
	{ "SayDialogEnd", "III", nil, nil, "lua" },
	{ "QueryAudienceAction", "d", nil, nil, "lua" },
	{ "StarBiwuSetAudienceLocation", "dddd", nil, nil, "lua" },
	{ "StarBiwuSetAudienceAction", "dsIII", nil, nil, "lua" },
	{ "StarBiwuSetAudienceViewGossip", "b", nil, nil, "lua" },
	{ "StarBiwuSetAudienceViewVictory", "b", nil, nil, "lua" },
	{ "QueryStarBiwuPaimaiOverview", "", nil, nil, "lua" },
	{ "RequestStarBiwuPaimaiBet", "Idb", 1000, nil, "lua" },
	{ "CcUnfollowVer2", "ub", nil, nil, "lua" },
	{ "RequestAbandonCity", "", nil, nil, "lua" },
	{ "QueryFollwingBiaoChePosOnMMap", "", nil, nil, "lua" },
	{ "RequestSculptureEquip", "sIIsII", nil, nil, "lua" },
	{ "RequestSummonGuildMemberToCity", "", nil, nil, "lua" },
	{ "AcceptSummonBackToOwnCity", "", nil, nil, "lua" },
	{ "AcceptSummonTrackToGuildBiaoChe", "I", nil, nil, "lua" },
	{ "HouseCompetitionCheckReward", "", nil, nil, "lua" },
	{ "RequestExchangeLingShouPartnerSkill", "ss", nil, nil, "lua" },
	{ "StarBiwuAudienceRelease", "d", nil, nil, "lua" },
	{ "StarBiwuAudiencePack", "d", nil, nil, "lua" },
	{ "StarBiwuRenameTeam", "s", nil, nil, "lua" },
	{ "StarBiwuInviteAudience", "dIsII", nil, nil, "lua" },
	{ "GetOnlineFriendsNotInTeamGroup", "i", nil, nil, "lua" },
	{ "RequestQuickAddBabyTili", "sb", nil, nil, "lua" },
	{ "RequestQingMingNiangJiu", "", nil, nil, "lua" },
	{ "FinishTaskPintuPuzzle", "I", nil, nil, "lua" },
	{ "QueryCityMonsterFightParam", "II", nil, nil, "lua" },
	{ "QueryMirrorWarWatchEnable", "", nil, nil, "lua" },
	{ "RequestCanReferenceStoneOnEquipment", "iisiIi", nil, nil, "lua" },
	{ "ReferenceStoneOnEquipment", "iisiIi", nil, nil, "lua" },
	{ "RequestCanSwitchEquipHoleSet", "iis", nil, nil, "lua" },
	{ "RequestSwitchEquipHoleSet", "iis", nil, nil, "lua" },
	{ "QueryTianMenShanBattleFightData", "I", nil, nil, "lua" },
	{ "BroadcastTianMenShanBattleFightData", "I", nil, nil, "lua" },
	{ "QueryTianMenShanBattleCopyMemberInfo", "", nil, nil, "lua" },
	{ "FinishTaskPainting", "I", nil, nil, "lua" },
	{ "QuerySmartGMToken", "", nil, nil, "lua" },
	{ "RequestOpenXianZhiWnd", "", nil, nil, "lua" },
	{ "RequestLevelUpXianZhi", "b", nil, nil, "lua" },
	{ "RequestPromoteXianZhi", "b", nil, nil, "lua" },
	{ "RequestExtendXianZhi", "b", nil, nil, "lua" },
	{ "RequestSignUpXianZhi", "I", nil, nil, "lua" },
	{ "QueryXianZhiSignUpInfo", "", nil, nil, "lua" },
	{ "RequestActiveXianZhiSkill", "", nil, nil, "lua" },
	{ "RequestCancelXianZhiSkill", "", nil, nil, "lua" },
	{ "SetActiveSkillForRobot", "IuubbII", nil, nil, "lua" },
	{ "RequestDiskPlayerData", "I", nil, nil, "lua" },
	{ "DiskPlayerRecordDiskItem", "bIIsss", nil, nil, "lua" },
	{ "DiskPlayerAddRecordedDiskItem", "IIIs", nil, nil, "lua" },
	{ "DiskPlayerRemoveRecordItem", "I", nil, nil, "lua" },
	{ "DiskPlayerPlay", "Ib", nil, nil, "lua" },
	{ "DiskPlayerSettingWhenPlayVoice", "IIIb", nil, nil, "lua" },
	{ "QuerySarBiwuChampion", "d", nil, nil, "lua" },
	{ "SendAntiCheatCheckInfo", "Isssss", nil, nil, "lua" },
	{ "RequestEnterSummonMirrorWarMineVehicle", "", nil, nil, "lua" },
	{ "RequestSummonMirrorWarMineVehicle", "I", nil, nil, "lua" },
	{ "RequestQuickPlantAndHarvest", "", nil, nil, "lua" },
	{ "FinishXianZhiPictureTask", "II", nil, nil, "lua" },
	{ "CakeCuttingSignUpPlay", "", 300, nil, "lua" },
	{ "CakeCuttingCancelSignUp", "", 300, nil, "lua" },
	{ "CakeCuttingCheckInMatching", "", nil, nil, "lua" },
	{ "CakeCuttingPlaySetDirection", "ii", nil, nil, "lua" },
	{ "CakeCuttingPlaySpeedUp", "", nil, nil, "lua" },
	{ "CakeCuttingQueryRank", "", nil, nil, "lua" },
	{ "RequestModifyStarBiwuShowStatue", "IIIu", nil, nil, "lua" },
	{ "PlayerQueryTranslationFromChannel", "Iss", nil, nil, "lua" },
	{ "OnQteSuccess", "I", nil, nil, "lua" },
	{ "OnQteFailed", "I", nil, nil, "lua" },
	{ "OnGameVideoQteSuccess", "s", nil, nil, "lua" },
	{ "OnGameVideoQteFailed", "s", nil, nil, "lua" },
	{ "ZNQLotteryAddTicket", "IIIII", nil, nil, "lua" },
	{ "ZNQLotteryGetReward", "", 3000, nil, "lua" },
	{ "ZNQLotteryQueryLottery", "", nil, nil, "lua" },
	{ "ZNQLotteryQueryPrizePool", "", nil, nil, "lua" },
	{ "BroadcastVoiceTranslationOnIM", "dsdss", 300, nil, "lua" },
	{ "BroadcastVoiceTranslationToGroup", "usdss", 300, nil, "lua" },
	{ "QueryVoiceTranslation", "ss", nil, nil, "lua" },
	{ "QueryDiskItemCreatorName", "d", nil, nil, "lua" },
	{ "RequestSelectStarBiwuShowStatue", "I", nil, nil, "lua" },
	{ "RequestEnterWaKuangPlay", "", nil, nil, "lua" },
	{ "RequestDropKuangShiFromPackage", "iI", nil, nil, "lua" },
	{ "RequestUpgradeWaKuangSkillAndPackage", "I", nil, nil, "lua" },
	{ "RequestHouseFurnitureLimitStatus", "ss", nil, nil, "lua" },
	{ "RequestBuyNpcHaoGanDuZengSongNum", "II", nil, nil, "lua" },
	{ "RequestZengSongGiftToNpc", "III", nil, nil, "lua" },
	{ "ReadNpcLetter", "II", nil, nil, "lua" },
	{ "RequestSendLetterToNpc", "III", nil, nil, "lua" },
	{ "OperateLingShouBabyAIForCamera", "I", nil, nil, "lua" },
	{ "QueryLingShouBabyInfoForCamara", "", nil, nil, "lua" },
	{ "RequestUnlockAttrGroup", "i", nil, nil, "lua" },
	{ "RequestEquipInfoInAttrGroup", "sii", nil, nil, "lua" },
	{ "RequestCanSwitchAttrGroup", "i", nil, nil, "lua" },
	{ "RequestSwitchAttrGroup", "i", nil, nil, "lua" },
	{ "RequestSetNameInAttrGroup", "is", nil, nil, "lua" },
	{ "RequestSetSkillTemplateInAttrGroup", "ii", nil, nil, "lua" },
	{ "RequestSetTalismanSetInAttrGroup", "ii", nil, nil, "lua" },
	{ "RequestSetEquipWordSetIdxInAttrGroup", "iisi", nil, nil, "lua" },
	{ "RequestSetEquipHoleSetIdxInAttrGroup", "iisi", nil, nil, "lua" },
	{ "RequestSetBattleLingShouInAttrGroup", "is", nil, nil, "lua" },
	{ "RequestSetAssistLingShouInAttrGroup", "is", nil, nil, "lua" },
	{ "RequestSetBindedPartnerLingShouInAttrGroup", "is", nil, nil, "lua" },
	{ "UpdateWorldEvent2019ViewedMessageIdx", "", nil, nil, "lua" },
	{ "QueryLieFengRepairProgressInfo", "", nil, nil, "lua" },
	{ "RequestReceiveQiYiHuaAward", "I", nil, nil, "lua" },
	{ "RequestLogJingLingSpendTime", "IIss", nil, nil, "lua" },
	{ "TeamAppointmentPlayerQueryReward", "", nil, nil, "lua" },
	{ "TeamAppointmentPlayerGetReward", "I", nil, nil, "lua" },
	{ "TeamAppointmentCheckHasReward", "", nil, nil, "lua" },
	{ "RequestGetRdcShareReward", "", nil, nil, "lua" },
	{ "RequestGetRdcWholeSetReward", "", nil, nil, "lua" },
	{ "RequestRdxExchangeCard", "II", nil, nil, "lua" },
	{ "RequestRdxComposeCard", "I", nil, nil, "lua" },
	{ "RequestGetRfcCardSet", "", nil, nil, "lua" },
	{ "LiuYiFamilyTimeRefreshTask", "sI", nil, nil, "lua" },
	{ "LiuYiFamilyTimeFinishTask", "sI", nil, nil, "lua" },
	{ "LiuYiFamilyTimeRecvReward", "sI", nil, nil, "lua" },
	{ "SaveWakeupBed", "I", nil, nil, "lua" },
	{ "RequestDecomposeOneRdcCard", "I", nil, nil, "lua" },
	{ "RequestBatchDecomposeRdcCard", "", nil, nil, "lua" },
	{ "LeaderRmStarBiwuAudienceFromTeam", "d", nil, nil, "lua" },
	{ "LeaderInviteToStarBiwuCombatTeam", "d", nil, nil, "lua" },
	{ "ConfirmInviteToStarBiwuCombatTeam", "db", nil, nil, "lua" },
	{ "StarBiwuTransferCombatTeam", "d", nil, nil, "lua" },
	{ "LogVoiceTranslationOnIM_v2", "ddsdss", 500, nil, "lua" },
	{ "QueryInvitedPlayerIds", "", nil, nil, "lua" },
	{ "RequestInviteWatchFengXian", "u", nil, nil, "lua" },
	{ "RequestWatchFengXian", "dI", nil, nil, "lua" },
	{ "QueryFengXianPlayerAppearance", "d", nil, nil, "lua" },
	{ "OnQueryVoiceTranslation", "", nil, nil, "lua" },
	{ "RequestXianZhiPuInfo", "", nil, nil, "lua" },
	{ "RequestOperateXianZhi", "dI", nil, nil, "lua" },
	{ "RequestCombianWuSeSiAndBangle", "dsds", nil, nil, "lua" },
	{ "RequestCreateStarBiwuZhanDui", "ss", 5000, nil, "lua" },
	{ "RequestSelfStarBiwuPersonalInfo", "", 200, nil, "lua" },
	{ "RequestQueryStarBiwuZhanDuiList", "I", 1000, nil, "lua" },
	{ "QueryStarBiwuZhanDuiInfoByMemberId", "d", 1000, nil, "lua" },
	{ "QueryStarBiwuZhanDuiInfoByZhanDuiId", "d", 500, nil, "lua" },
	{ "RequestJoinStarBiwuZhanDui", "ds", 200, nil, "lua" },
	{ "RequestStarBiwuZhanDuiApplyList", "I", 1000, nil, "lua" },
	{ "AcceptApplyPlayerJoinStarBiwuZhanDui", "d", 500, nil, "lua" },
	{ "RefuseApplyPlayerJoinStarBiwuZhanDui", "d", 500, nil, "lua" },
	{ "RequestClearStarBiwuZhanDuiApplyList", "", 5000, nil, "lua" },
	{ "RequestEnterZongZi2019Play", "", 500, nil, "lua" },
	{ "RequestStarBiwuQieCuoTeamList", "I", 1000, nil, "lua" },
	{ "RequestSingUpStarBiwuQieCuo", "", 2000, nil, "lua" },
	{ "RequestCancelSingUpStarBiwuQieCuo", "", 2000, nil, "lua" },
	{ "RequestStarBiwuQieCuoWithOtherTeam", "d", 2000, nil, "lua" },
	{ "QueryStarBiwuQieCuoTeamByMemberId", "d", 2000, nil, "lua" },
	{ "QueryClientBinaryIndentity", "", 2000, nil, "lua" },
	{ "ReportClientBinaryCheckResult", "bs", 1000, nil, "lua" },
	{ "QueryLiangHaoRenewInfo", "dI", 500, nil, "lua" },
	{ "RequestRenewLiangHao", "dsII", 500, nil, "lua" },
	{ "RequestGiveUpLiangHao", "", 500, nil, "lua" },
	{ "RequestSetLiangHaoPrivilege", "II", 500, nil, "lua" },
	{ "QueryLiangHaoSellInfo", "s", 1000, nil, "lua" },
	{ "RequestBuyLiangHao", "II", 1000, nil, "lua" },
	{ "QueryLiangHaoJingPaiInfo", "I", 1000, nil, "lua" },
	{ "RequestJingPaiLiangHao", "II", 1000, nil, "lua" },
	{ "QueryMyLiangHaoJingPaiInfo", "", 1000, nil, "lua" },
	{ "RequestSelfStarBiwuZhanDuiInfo", "", 1000, nil, "lua" },
	{ "RequestLeaveStarBiwuZhanDui", "", 1000, nil, "lua" },
	{ "RequestCancelJoinStarBiwuZhanDui", "d", 500, nil, "lua" },
	{ "RequestExchangeLiangHaoItem", "sII", 500, nil, "lua" },
	{ "QueryPengPengCheSignUpInfo", "", nil, nil, "lua" },
	{ "RequestSignUpPengPengChe", "", nil, nil, "lua" },
	{ "RequestCancelSignUpPengPengChe", "", nil, nil, "lua" },
	{ "RequestPengPengCheBattleInfo", "", nil, nil, "lua" },
	{ "RequestRemovePengPengCheSkillItem", "iIb", nil, nil, "lua" },
	{ "IntiveOtherJoinStarBiwuZhanDui", "d", nil, nil, "lua" },
	{ "StarBiwuZhanDuiConfirmBeInvite", "d", nil, nil, "lua" },
	{ "RequestSetStarBiwuChuZhanInfo", "IIIIII", 1000, nil, "lua" },
	{ "AdjustStarBiwuBiWuChuZhanInfo", "IIIIII", 1000, nil, "lua" },
	{ "RequestUpdateStarBiwuZhanDuiTitleAndKouHao", "ss", 1000, nil, "lua" },
	{ "TaskRequestDoSayDialog", "I", nil, nil, "lua" },
	{ "FinishClientTaskEvent", "Isu", nil, nil, "lua" },
	{ "RequestCheckRdcCard", "I", nil, nil, "lua" },
	{ "RequestOpenEventTask", "Iss", nil, nil, "lua" },
	{ "FinishEventTask", "Is", nil, nil, "lua" },
	{ "RequestDrawHanZi", "I", nil, nil, "lua" },
	{ "FinishDrawHanZi", "II", nil, nil, "lua" },
	{ "QueryStarBiwuAllMemberFightData", "", nil, nil, "lua" },
	{ "QueryStarBiWuZhanKuang", "", nil, nil, "lua" },
	{ "QueryStarBiwuDetailFightData", "I", nil, nil, "lua" },
	{ "RequestAccuseGuild", "dss", nil, nil, "lua" },
	{ "RequestUseItemWithTaskId", "IIssI", nil, nil, "lua" },
	{ "QueryShenBingRestoreReturn", "IIs", nil, nil, "lua" },
	{ "ResuestSetDaShenLogToken", "s", nil, nil, "lua" },
	{ "RequestRemoveStarBiwuZhanDuiMember", "d", nil, nil, "lua" },
	{ "RequestStarBiWuChuZhanInfo", "", nil, nil, "lua" },
	{ "RequestUseHuiLiuItemWithChoice", "sIII", nil, nil, "lua" },
	{ "RequestLoginZhouBianMallShop", "", nil, nil, "lua" },
	{ "RequestTeleportToLiuYiPark", "", nil, nil, "lua" },
	{ "QueryStarBiwuXiaoZuSaiOverview", "II", nil, nil, "lua" },
	{ "RequestWatchStarBiwuXiaoZuSai", "ddII", nil, nil, "lua" },
	{ "RequestStarBiwuZongJueSaiWatchList", "I", 1000, nil, "lua" },
	{ "QueryStarBiwuZongJueSaiOverView", "I", 1000, nil, "lua" },
	{ "RequestStartUnintermittentHell", "", nil, nil, "lua" },
	{ "RequestExchangeAllAVGCurrency", "", nil, nil, "lua" },
	{ "SaveSceneryPhotoAlbumUrl", "Is", 5000, nil, "lua" },
	{ "RequestAllSceneryPhotoAlbumUrl", "", 5000, nil, "lua" },
	{ "RequestExchangeFaBaoSpecialFx", "I", 1000, nil, "lua" },
	{ "RequestOpenMedicineBox", "", 1000, nil, "lua" },
	{ "OneKeyCollectItemToMedicineBox", "", 2000, nil, "lua" },
	{ "SetMedicineBoxAutoCollect", "I", 2000, nil, "lua" },
	{ "OneKeyUseMedicineBoxItem", "s", 5000, nil, "lua" },
	{ "SaveMedicineBoxChoices", "s", 1000, nil, "lua" },
	{ "QuerySmartGMTokenAndUrlWitchContext", "sss", 1000, nil, "lua" },
	{ "TaskRequestPlayGameVideo", "I", nil, nil, "lua" },
	{ "RequestSetQmpkEquipIdentifySkill", "IIsI", 1000, nil, "lua" },
	{ "RequestOperateQmpkLingShouV2", "sIIsIIIs", 1000, nil, "lua" },
	{ "RequestStartQiXiLiuXingYu", "dIsss", nil, nil, "lua" },
	{ "FinishEventTaskWithConditionId", "Iss", nil, nil, "lua" },
	{ "PlayCGDone", "s" },
	{ "QueryLiangHaoSurplusYuanBaoValue", "Is", nil, nil, "lua" },
	{ "RequestInteractWithSceneItem", "II", 1000, nil, "lua" },
	{ "ConsumeJewelPlaySceneFx", "sIIII", nil, nil, "lua" },
	{ "RequestWatchStarBiwuJiFenSai", "Idd", nil, nil, "lua" },
	{ "QueryStarBiwuWatchList", "", nil, nil, "lua" },
	{ "FinishUITrigger", "I", nil, nil, "lua" },
	{ "OpenQmpkHandBook", "", nil, nil, "lua" },
	{ "ReceiveQmpkHandBookAward", "I", 500, nil, "lua" },
	{ "RequestSubmitZhongQiuNpcFriendlinessItem", "IIIs", nil, nil, "lua" },
	{ "RequestStartYueShenTemple", "", nil, nil, "lua" },
	{ "QueryCommunityGhostData", "" },
	{ "RequestAcceptGanEnZhiYuTask", "", nil, nil, "lua" },
	{ "RequestCalcLiangHaoPrice", "Is", 5000, nil, "lua" },
	{ "RequestBuyDingZhiLiangHao", "IsIII", 2000, nil, "lua" },
	{ "QueryLiangHaoDingZhiInfo", "I", 1000, nil, "lua" },
	{ "AuctionLiangHaoDingZhiItem", "II", 1000, nil, "lua" },
	{ "QueryMyLiangHaoDingZhiApplyInfo", "", 1000, nil, "lua" },
	{ "QueryPlayerShopSearchZhuangShiWuByColor", "IIIII", nil, nil, "lua" },
	{ "NeedChangePlayerName", "s", nil, nil, "lua" },
	{ "HaoGanDuTaskQiuQianComplete", "IsI", nil, nil, "lua" },
	{ "FailHandDraw", "II", nil, nil, "lua" },
	{ "TakeOutItemFromMedicineBox", "I", nil, nil, "lua" },
	{ "RequestTeleportToTerritoryByMapId", "I", nil, nil, "lua" },
	{ "RequestWashChuanjiabao", "IIsb", nil, nil, "lua" },
	{ "RequestWashChuanjiabaoInHouse", "sb", nil, nil, "lua" },
	{ "RequestImproveChuanjiabaoStar", "IIsI", nil, nil, "lua" },
	{ "RequestImproveChuanjiabaoStarInHouse", "sI", nil, nil, "lua" },
	{ "RequestAcceptChuanjiabaoWashResult", "IIs", nil, nil, "lua" },
	{ "RequestAcceptChuanjiabaoWashResultInHouse", "s", nil, nil, "lua" },
	{ "CnvLotteryAddTicket", "IIII", nil, nil, "lua" },
	{ "CnvLotteryGetReward", "", 3000, nil, "lua" },
	{ "CnvLotteryQueryLottery", "", nil, nil, "lua" },
	{ "CnvLotteryQueryPrizePool", "", nil, nil, "lua" },
	{ "QXJTQueryRegistered", "", nil, nil, "lua" },
	{ "QXJTUploadImage", "Is", nil, nil, "lua" },
	{ "RequestNpcHaoGanDuSpecialGiftInfo", "I", nil, nil, "lua" },
	{ "RequestZengSongSpecialGiftToNpc", "III", nil, nil, "lua" },
	{ "RequestPlayerName", "d", nil, nil, "lua" },
	{ "OpenNeteaseMemberMall", "", nil, nil, "lua" },
	{ "RequestUseNeteaseMemberRight", "ssI", nil, nil, "lua" },
	{ "RequestNeteaseMemberHolidayGift", "I", nil, nil, "lua" },
	{ "NpcFightBangQiang", "I", nil, nil, "lua" },
	{ "QueryFuliWndStatus", "", nil, nil, "lua" },
	{ "RequestZhongQiuNpcFriendlinessTask", "", nil, nil, "lua" },
	{ "RequestKeJuDanMuRecord", "", nil, nil, "lua" },
	{ "RequestZhongQiuNpcFriendlinessData", "", nil, nil, "lua" },
	{ "RequestSummonGuildMemberToMirrorWarPlay", "", nil, nil, "lua" },
	{ "AcceptSummonToMirrorWarPlay", "I", nil, nil, "lua" },
	{ "NationalDayTXZRSignUpPlay", "", nil, nil, "lua" },
	{ "NationalDayTXZRCancelSignUp", "", nil, nil, "lua" },
	{ "NationalDayTXZRCheckInMatching", "", nil, nil, "lua" },
	{ "RequestWatchStarBiwuZongJueSai", "I", nil, nil, "lua" },
	{ "RequestSetStarBiwuPublishInfo", "I", nil, nil, "lua" },
	{ "RequestFillGuildDiningTableGrid", "IiI", nil, nil, "lua" },
	{ "RequestDiningOnGuildDiningTable", "i", 3000, nil, "lua" },
	{ "RequestGroupPhotoWithOther", "dI", 2000, nil, "lua" },
	{ "RequestFinishShanGuiTask", "I", 5000, nil, "lua" },
	{ "RotateFurniture", "IIII", nil, nil, "lua" },
	{ "GetAccountQYCode", "", nil, nil, "lua" },
	{ "RequestCheckLink", "ssuu", nil, nil, "lua" },
	{ "RequestBuyOneSimpleItemSlot", "", nil, nil, "lua" },
	{ "MoveNormalItemToSimpleItems", "Is", nil, nil, "lua" },
	{ "MoveAllNormalItemToSimpleItems", "", nil, nil, "lua" },
	{ "MoveSimpleItemToNormalItem", "III", nil, nil, "lua" },
	{ "SimpleItemRepoReorder", "", nil, nil, "lua" },
	{ "RequestOpenBindRepoWnd", "", nil, nil, "lua" },
	{ "StarBiwuQueryCurrentJingCaiItem", "", nil, nil, "lua" },
	{ "StarBiwuQueryJingCaiRecord", "", nil, nil, "lua" },
	{ "StarBiwuQueryJingCaiRecordDetail", "IdI", nil, nil, "lua" },
	{ "StarBiwuRequestJingCaiBet", "Idu", nil, nil, "lua" },
	{ "StarBiwuRequestGetJingCaiReward", "", nil, nil, "lua" },
	{ "RequestFinishDiYuZhuXianTask", "I", nil, nil, "lua" },
	{ "MarkTreasureBoxInUnintermittentHell", "II", nil, nil, "lua" },
	{ "RequestReceiveQiChangActiveReward", "sI", nil, nil, "lua" },
	{ "SendNoneForegroundWndInfo", "suss", nil, nil, "lua" },
	{ "QueryPlayerFamilyTreeTags", "ds", 1000, nil, "lua" },
	{ "QueryPlayerFamilyTreeTagDetail", "ds", 1000, nil, "lua" },
	{ "TryTagPlayerFamilyTree", "dssIb", 3000, nil, "lua" },
	{ "FavorPlayerFamilyTreeTag", "dsb", 1000, nil, "lua" },
	{ "MapEditorSetObjectPos", "Iddd", 0, nil, "lua" },
	{ "MapEditorGenerateNpc", "I", 0, nil, "lua" },
	{ "MapEditorDeleteObj", "I", 0, nil, "lua" },
	{ "MapEditorExportNpcData", "", 0, nil, "lua" },
	{ "MapEditorQueryRecord", "", 0, nil, "lua" },
	{ "MapEditorSelectRecord", "i", 0, nil, "lua" },
	{ "MapEditorResetRecord", "", 0, nil, "lua" },
	{ "QueryHalloweenTaskProgress", "", 1000, nil, "lua" },
	{ "RequestGetDyePlayReward", "", 1000, nil, "lua" },
	{ "RequestAccTaskByHalloweenIdx", "I", 1000, nil, "lua" },
	{ "RequestHalloweenCardOrder", "", 1000, nil, "lua" },
	{ "RequestFinishHalloweenCard", "II", 1000, nil, "lua" },
	{ "RequestFinishTouchJade", "sd", 1000, nil, "lua" },
	{ "QueryRanSeJiBossStatus", "", 1000, nil, "lua" },
	{ "QueryBaoTuanZuoZhanApplyInfo", "", 1000, nil, "lua" },
	{ "RequestApplyBaoTuanZuoZhan", "", 1000, nil, "lua" },
	{ "QueryJoinSinglesDayLotteryInfo", "", 1000, nil, "lua" },
	{ "QuerySinglesDayLotteryResult", "I", 1000, nil, "lua" },
	{ "AnswerAuthenticationQuestion", "I", nil, nil, "lua" },
	{ "EndAuthentication", "", nil, nil, "lua" },
	{ "RequestEnterQiLinDongPlay", "", 3000, nil, "lua" },
	{ "CostJadeEnterQiLinDongPlay", "I", 3000, nil, "lua" },
	{ "ReplaceQiLinDongTempSkill", "II", 1000, nil, "lua" },
	{ "RequestOpenQiLinDongPlayShop", "", 1000, nil, "lua" },
	{ "RequestBuyQiLinDongPlayShopItem", "I", 1000, nil, "lua" },
	{ "CancelApplyBaoTuanZuoZhan", "", 1000, nil, "lua" },
	{ "RequestSingUpSanXingBattle", "I", 5000, nil, "lua" },
	{ "RequestCancelSignUpSanXingBattle", "I", 5000, nil, "lua" },
	{ "QuerySanXingBattlePlayInfo", "", 2000, nil, "lua" },
	{ "RequestSanXingBattleGateTeleport", "", 1000, nil, "lua" },
	{ "FinishClientTaskEventWithConditionId", "IsuI", nil, nil, "lua" },
	{ "RequestPickBatchItem", "s", nil, nil, "lua" },
	{ "RequestDeclareMirrorWarInfo", "dsII", 3000, nil, "lua" },
	{ "FinishAutoBaptizeWord", "iisssssbb" },
	{ "FinishLongPressScreen", "s", nil, nil, "lua" },
	{ "SendCustomRpc", "su", nil, nil, "lua" },
	{ "RequestShiFuScoreLevel", "", nil, nil, "lua" },
	{ "RequestRepairCityUnitInfo", "sII", 2000, nil, "lua" },
	{ "RepairCityUnit", "sII", nil, nil, "lua" },
	{ "RequestRepairAllCityUnitInfo", "s", 2000, nil, "lua" },
	{ "RepairAllCityUnit", "s", nil, nil, "lua" },
	{ "RequestOpenAddPlayerFamilyTreeTagWnd", "d", 3000, nil, "lua" },
	{ "RequestAutoAcceptTask", "I", 2000, nil, "lua" },
	{ "RequestSignUpChristmasGiftBattle", "I", 5000, nil, "lua" },
	{ "ReceiveChristmasZhuangBanAward", "", 1000, nil, "lua" },
	{ "QueryMirrorWarPriorityMatchGroup", "d", nil, nil, "lua" },
	{ "CancelMirrorWar", "d", 3000, nil, "lua" },
	{ "QuerySmartGMTokenAndHost", "s" },
	{ "QueryChristmasGiftBattlePlayInfo", "", 1000, nil, "lua" },
	{ "RequestUseChuJiaMuYu", "I" },
	{ "QueryTerritoryOccupyAwardList", "", 1000, nil, "lua" },
	{ "QueryTerritoryOccupyAward", "Ii", nil, nil, "lua" },
	{ "RequestTerritoryOccupyAward", "Ii", nil, nil, "lua" },
	{ "RequestAwardSchoolAchievement", "I", nil, nil, "lua" },
	{ "ConfirmChangePlayerName", "s", nil, nil, "lua" },
	{ "OneKeyBuyMallItemBegin", "", 5000, nil, "lua" },
	{ "OneKeyBuyMallItem", "I", nil, nil, "lua" },
	{ "OneKeyBuyMallItemEnd", "", nil, nil, "lua" },
	{ "RequestYuanDanGuildVoteInfo", "", nil, nil, "lua" },
	{ "RequestYuanDanGuildVoteQuestion", "I", nil, nil, "lua" },
	{ "RequestYuanDanGuildVoteCreate", "ssu", nil, nil, "lua" },
	{ "RequestYuanDanGuildDoVote", "Id", nil, nil, "lua" },
	{ "RequestYuanDanGuildVoteThankSpeech", "Is", nil, nil, "lua" },
	{ "RequestSetSanXingDefaultRelivePos", "I", nil, nil, "lua" },
	{ "RequestGuildPlayerIdAndName", "", nil, nil, "lua" },
	{ "QuerySanXingBattleFreeMatchInfo", "", nil, nil, "lua" },
	{ "QueryWorkManualOverview", "", 1000, nil, "lua" },
	{ "RequestGetManualReward", "I", nil, nil, "lua" },
	{ "RequestBuyHanJiaManual", "Is", nil, nil, "lua" },
	{ "RequestGetHanJiaSubTaskReward", "I", nil, nil, "lua" },
	{ "RequestOpenHanJiaBottle", "", nil, nil, "lua" },
	{ "RequestSubmitSnowball", "II", nil, nil, "lua" },
	{ "RequestDecorateSnowman", "IsI", nil, nil, "lua" },
	{ "RequestOpenHanJiaManualWnd", "", nil, nil, "lua" },
	{ "RequestOpenMenPaiHuTongSkillReplaceWnd", "", nil, nil, "lua" },
	{ "RequestReplaceMenPaiHuTongSkill", "II", nil, nil, "lua" },
	{ "RequestNewbieSchoolWish", "s", nil, nil, "lua" },
	{ "RequestSelectNewbieSchoolCourse", "II", nil, nil, "lua" },
	{ "RequestOpenSelectNewbieSchoolWnd", "I", nil, nil, "lua" },
	{ "JinShaJingWaterHeightChange", "I" },
	{ "RequestYuanDanGuildVoteShare", "I", nil, nil, "lua" },
	{ "GenerateHouseChristmasPicker", "I", 500, nil, "lua" },
	{ "RequestSignUpQingYiXiangTongPlay", "", nil, nil, "lua" },
	{ "CancelQingYiXiangTongPlaySignUp", "", nil, nil, "lua" },
	{ "QueryQingYiXiangTongPlaySignUp", "", nil, nil, "lua" },
	{ "QuerySingletonShopShelf", "", nil, nil, "lua" },
	{ "ReqeustBuySingletonShopGood", "II", nil, nil, "lua" },
	{ "RequestOpenHuiGuiJieBanWnd", "", nil, nil, "lua" },
	{ "RequestLiuShiFriends", "", nil, nil, "lua" },
	{ "RequestInviteFriend", "d", nil, nil, "lua" },
	{ "RequestPlayerInviters", "", nil, nil, "lua" },
	{ "RequestNotifyInviter", "d", nil, nil, "lua" },
	{ "RequestRemoveJieBanPlayer", "", nil, nil, "lua" },
	{ "QueryChunJie2020SeriesTasksRewardStatus", "", nil, nil, "lua" },
	{ "RequestGetChunJie2020SeriesTasksReward", "", nil, nil, "lua" },
	{ "QuerySchoolTaskOpenStatus", "", nil, nil, "lua" },
	{ "QueryMenPaiHuTongSkillInfo", "I", nil, nil, "lua" },
	{ "RequestUnLockXiaoYaoExpression", "" },
	{ "QuerySanXingBattleFreeMatchPlayerNum", "", nil, nil, "lua" },
	{ "RequestShowPromoCodeCheckOrderWnd", "sss" },
	{ "RequestSetFurnitureColor", "Iiii", nil, nil, "lua" },
	{ "CrossDouDiZhuOpenSignUpWnd", "", nil, nil, "lua" },
	{ "CrossDouDiZhuRequestSignUp", "", nil, nil, "lua" },
	{ "CrossDouDiZhuRequestCancelSignUp", "", nil, nil, "lua" },
	{ "CrossDouDiZhuQuerySignUpStatus", "", nil, nil, "lua" },
	{ "CrossDouDiZhuQueryHaiXuanRank", "", nil, nil, "lua" },
	{ "CrossDouDiZhuQueryJueSaiVoteList", "", nil, nil, "lua" },
	{ "CrossDouDiZhuRequestJueSaiVote", "d", nil, nil, "lua" },
	{ "CrossDouDiZhuRequestEnterJueSaiScene", "", nil, nil, "lua" },
	{ "CrossDouDiZhuRequestShowPlayInfo", "", nil, nil, "lua" },
	{ "CrossDouDiZhuRequestWatchPlayer", "d", nil, nil, "lua" },
	{ "SetPlayerInterestedItems", "u", nil, nil, "lua" },
	{ "YuanXiaoLanternSendLantern", "dsIIs", nil, nil, "lua" },
	{ "DuiDuiLeEnterGame", "", nil, nil, "lua" },
	{ "DuiDuiLeGetLastGameReward", "", nil, nil, "lua" },
	{ "DuiDuiLeOpenSignUpWnd", "", nil, nil, "lua" },
	{ "QueryPlayerXiaoYaoExpressionUnLockInfo", "d", nil, nil, "lua" },
	{ "RequestQYXTBattleInfo", "", nil, nil, "lua" },
	{ "RequestQYXTProgressInfo", "", nil, nil, "lua" },
	{ "RequestSendJueJiDisplayMail", "s", nil, nil, "lua" },
	{ "RequestResetFurnitureColor", "I", nil, nil, "lua" },
	{ "RequestRepairAllBodyEquip", "b", nil, nil, "lua" },
	{ "QueryStarBiwuZongJueSaiMatchInfo", "", nil, nil, "lua" },
	{ "CheckStarBiwuInJingCai", "", nil, nil, "lua" },
	{ "StarBiwuQueryJueSaiJingCaiItem", "I", nil, nil, "lua" },
	{ "RequestLogPCloginExtraInfo", "sssss", nil, nil, "lua" },
	{ "ReuqestLogJingLingQuickView", "iIIds", nil, nil, "lua" },
	{ "QingMing2020PvpSignUpPlay", "", nil, nil, "lua" },
	{ "QingMing2020PvpCheckInMatching", "", nil, nil, "lua" },
	{ "QingMing2020PvpCancelSignUp", "", nil, nil, "lua" },
	{ "QingMing2020PvpQueryRank", "", nil, nil, "lua" },
	{ "QingMing2020PvpQueryPlayInfo", "", nil, nil, "lua" },
	{ "QingMing2020PveEnterPlay", "", nil, nil, "lua" },
	{ "QingMing2020PveRequestReward", "I", nil, nil, "lua" },
	{ "QingMing2020CollectionReward", "", nil, nil, "lua" },
	{ "RanFaJiColorSystemChooseComplete", "sIII", nil, nil, "lua" },
	{ "ExchangeRanFaJiPeiFang", "I", nil, nil, "lua" },
	{ "TakeOnAdvancedHairColor", "I", nil, nil, "lua" },
	{ "TakeOffAdvancedHairColor", "I", nil, nil, "lua" },
	{ "DiscardAdvancedHairColor", "I", nil, nil, "lua" },
	{ "RequestSyncRanFaJiPlayData", "I", nil, nil, "lua" },
	{ "QueryPuJiaCunQiuQianInfo", "I", nil, nil, "lua" },
	{ "RequestPushPuJiaCunQiuQian", "I", nil, nil, "lua" },
	{ "HeChengRanFaJi", "I", nil, nil, "lua" },
	{ "QueryAutoBaptizeTargetWordCondList", "IIs", nil, nil, "lua" },
	{ "QueryTempPlayDataInUD", "I", nil, nil, "lua" },
	{ "SendNeteaseMemberGouKaTokenBegin", "", nil, nil, "lua" },
	{ "SendNeteaseMemberGouKaToken", "s", nil, nil, "lua" },
	{ "QueryNeteaseMemberGouKaInfo", "sss", nil, nil, "lua" },
	{ "InvitePlayerRemoteDouDiZhu", "dI", nil, nil, "lua" },
	{ "AcceptJoinRemoteDouDiZhu", "sIIdI", nil, nil, "lua" },
	{ "RefuseJoinRemoteDouDiZhu", "sIIdI", nil, nil, "lua" },
	{ "SendRemoteDouDiZhuLinkMessage", "i", 1000, nil, "lua" },
	{ "ApplyJoinRemoteDouDiZhu", "sII", nil, nil, "lua" },
	{ "QueryApplyJoinRemoteDouDiZhuPlayerList", "", nil, nil, "lua" },
	{ "ClearApplyJoinRemoteDouDiZhuPlayerList", "", nil, nil, "lua" },
	{ "ApproveJoinRemoteDouDiZhu", "d", nil, nil, "lua" },
	{ "DisapproveJoinRemoteDouDiZhu", "d", nil, nil, "lua" },
	{ "KickPlayerRemoteDouDiZhu", "d", nil, nil, "lua" },
	{ "QueryInvitedRemoteDouDiZhuPlayerList", "", nil, nil, "lua" },
	{ "DanZhuQuerySignUpStatus", "", nil, nil, "lua" },
	{ "DanZhuRequestSignUp", "I", nil, nil, "lua" },
	{ "DanZhuRequestCancelSignUp", "", nil, nil, "lua" },
	{ "DanZhuSelectDanZhuTypeDone", "I", nil, nil, "lua" },
	{ "QueryShiTuCultivateInfo", "Id", nil, nil, "lua" },
	{ "ShiTuCultivateShiFuAddItem", "dIIiis", nil, nil, "lua" },
	{ "ShiTuWeekTaskShiFuRefreshTask", "dI", nil, nil, "lua" },
	{ "ShiTuWeekTaskGetReward", "dI", nil, nil, "lua" },
	{ "ShiTuDailyTaskGetReward", "dI", nil, nil, "lua" },
	{ "ShiTuDailyTaskTuDiHandleTask", "dIb", nil, nil, "lua" },
	{ "ShiTuCultivateShiFuChooseTudi", "d", nil, nil, "lua" },
	{ "RequestCreateWoodPile", "IIII", nil, nil, "lua" },
	{ "RequestPackWoodPile", "I", nil, nil, "lua" },
	{ "RequestSwitchWoodPileMonsterIdx", "II", nil, nil, "lua" },
	{ "RequestMoveWoodPile", "IIII", nil, nil, "lua" },
	{ "RequestFightWithWoodPile", "I", nil, nil, "lua" },
	{ "RequestStopFightWithPile", "I", nil, nil, "lua" },
	{ "RequestPileFightProp", "I", nil, nil, "lua" },
	{ "RequestEmptyPileFightStatics", "I", nil, nil, "lua" },
	{ "DiYuShiKongAddBloodToFaQi", "", nil, nil, "lua" },
	{ "Request2020CombianWuSeSiAndBangle", "dsds", nil, nil, "lua" },
	{ "QueryQuanMinHuoYunInfo", "", nil, nil, "lua" },
	{ "SetQuanMinHuoYunAssistClass", "iiI", nil, nil, "lua" },
	{ "RequestGetQuanMinHuoYunFullAward", "i", nil, nil, "lua" },
	{ "RequestQuanMinHuoYunHeChengYaoFang", "u", nil, nil, "lua" },
	{ "RequestSubmitTongXinJingChip", "iiIIb", nil, nil, "lua" },
	{ "RequestRebornInFengHuoLing", "", nil, nil, "lua" },
	{ "RequestApplyChiJi", "", nil, nil, "lua" },
	{ "CancelApplyChiJi", "", nil, nil, "lua" },
	{ "RequestPickChiJiItemFromScene", "I" },
	{ "RequestPickPlayerChiJiBagItem", "Idd" },
	{ "RequestSplitChiJiItem", "IdI", nil, nil, "lua" },
	{ "RequestDropChiJiItem", "Id", nil, nil, "lua" },
	{ "RequestUseChiJiItem", "Id", nil, nil, "lua" },
	{ "RequestMergeChiJiBag", "", 1000, nil, "lua" },
	{ "QueryChiJiApplyInfo", "", nil, nil, "lua" },
	{ "RequestOpenChiJiBag", "", nil, nil, "lua" },
	{ "RequestSavePictureInfo", "IIIu", nil, nil, "lua" },
	{ "RequestLoadPictureInfo", "III", nil, nil, "lua" },
	{ "StartWatchChiJiTeamMember", "d", nil, nil, "lua" },
	{ "StopWatchChiJiTeamMember", "", nil, nil, "lua" },
	{ "RequestSetActiveTempSkill", "II", nil, nil, "lua" },
	{ "RequestEnterButterflyCrisisPlay", "I", nil, nil, "lua" },
	{ "ButterflyPlayerReadNpc", "I", nil, nil, "lua" },
	{ "ButterflyUnlockMemorySegment", "II", nil, nil, "lua" },
	{ "ButterflyGetUIStatus", "", nil, nil, "lua" },
	{ "ButterflyPlayerCollectReward", "I", nil, nil, "lua" },
	{ "RequestSyncChiJiTeamMemberHpInfo", "", nil, nil, "lua" },
	{ "RequestRMBGiftPrivilege", "I", nil, nil, "lua" },
	{ "RequestRecentHongBaoInfoV2", "I", 2000, nil, "lua" },
	{ "RequestGetTaskItem", "I", nil, nil, "lua" },
	{ "BuyProductWithTokenMoneyAsPresent", "dsssIIIs", 1000 },
	{ "BuyProductWithTokenMoney", "sssIIIs", 1000 },
	{ "HouseCompetitionTrySelectCard", "I", nil, nil, "lua" },
	{ "HouseCompetitionTryExtraCardReward", "I", nil, nil, "lua" },
	{ "RequestStopStarBiwuFight", "", nil, nil, "lua" },
	{ "ConfirmStopStarBiwuFight", "", nil, nil, "lua" },
	{ "RefuseStopStarBiwuFight", "", nil, nil, "lua" },
	{ "CcCaptureLog", "IIs" },
	{ "TestGatewayRttStart", "db", nil, nil, "lua" },
	{ "RecordForbidSearchOnlinePlayerByName", "s" },
	{ "DieKePossess", "I", nil, nil, "lua" },
	{ "RequestLevelUpFaBaoSpecialFx", "I", 1000, nil, "lua" },
	{ "RequestLevelDownFaBaoSpecialFx", "I", 1000, nil, "lua" },
	{ "RequestReturnFaBaoSpecialFx", "I", 1000, nil, "lua" },
	{ "ButterflyPickNpc", "II", nil, nil, "lua" },
	{ "RequestBuyHouseSkyBox", "I", nil, nil, "lua" },
	{ "RequestSwitchHouseSkyBox", "I", nil, nil, "lua" },
	{ "UniShowRealNameDialogResult", "i" },
	{ "BindMobileDisbind_Lua", "s", nil, nil, "lua" },
	{ "BindMobileRequestCaptcha_Bind_Lua", "ss", nil, nil, "lua" },
	{ "BindMobileRequestCaptcha_Renew_Lua", "", nil, nil, "lua" },
	{ "BindMobileVerifyCaptcha_Lua", "sss", nil, nil, "lua" },
	{ "GetVerifyCommonDeviceCode_Lua", "", nil, nil, "lua" },
	{ "RequestVerifyCommonDevice_Lua", "s", 1000, nil, "lua" },
	{ "RequestChangeApplicationPause", "b", nil, nil, "lua" },
	{ "HouseScreenshotReport", "ss", nil, nil, "lua" },
	{ "CreateAndAddFurniture", "Iddddbsudddd", nil, nil, "lua" },
	{ "BuyQmpkAdvancedZhanLing", "", 2000, nil, "lua" },
	{ "DetectAccountTransferCondition", "", nil, nil, "lua" },
	{ "QueryAccoutBindMobleInfo", "", nil, nil, "lua" },
	{ "XYLPTryReward", "I", nil, nil, "lua" },
	{ "XYLPQueryThreads", "", nil, nil, "lua" },
	{ "YNZJFindNpc", "I", nil, nil, "lua" },
	{ "YNZJQueryRank", "", nil, nil, "lua" },
	{ "YNZJQueryTransform", "I", nil, nil, "lua" },
	{ "ConfirmAccountTransfer", "", nil, nil, "lua" },
	{ "QueryWoodPileFightHistory", "I", nil, nil, "lua" },
	{ "DieKeForceUnPossess", "d" },
	{ "DieKeForceUnPossessAll", "" },
	{ "RequestCrossMap", "IiisIu", 1000 },
	{ "GlobalSpokesmanRequestPawn", "u", nil, nil, "lua" },
	{ "RequestGlobalSpokesmanShowExpression", "I", nil, nil, "lua" },
	{ "GlobalSpokesmanGetAvailablePawn", "", nil, nil, "lua" },
	{ "DonateToSpokesmanHouse", "I", nil, nil, "lua" },
	{ "RequestDonateToSpokesmanAward", "I", nil, nil, "lua" },
	{ "RequestEnterSpokesmanHouse", "", nil, nil, "lua" },
	{ "OpenDonateSpokesmanHouseWnd", "", nil, nil, "lua" },
	{ "RJPMQueryRank", "", nil, nil, "lua" },
	{ "RequestSpokesmanTCGData", "", nil, nil, "lua" },
	{ "SpokesmanTCGComposeCard", "bb", nil, nil, "lua" },
	{ "SpokesmanTCGReadCard", "I", nil, nil, "lua" },
	{ "SpokesmanTCGDecomposeCard", "II", nil, nil, "lua" },
	{ "SpokesmanTCGBatchDecomposeCard", "", nil, nil, "lua" },
	{ "SpokesmanTCGRetrieveCard", "II", nil, nil, "lua" },
	{ "SpokesmanTCGShareDone", "I", nil, nil, "lua" },
	{ "QuerySpokesmanContractSign", "b", nil, nil, "lua" },
	{ "RequestSignSpokesmanContract", "", nil, nil, "lua" },
	{ "QuerySpokesmanContractDetail", "", nil, nil, "lua" },
	{ "RequestFinishContractAward", "i", nil, nil, "lua" },
	{ "RequestFinishAllSpokesmanContractAward", "", nil, nil, "lua" },
	{ "RequestBuyJiaNianHua2020Ticket", "I", 5000, nil, "lua" },
	{ "QueryScoreShopGoodsRestStock", "II", nil, nil, "lua" },
	{ "QueryDieKeVipTicketSellInfo", "", nil, nil, "lua" },
	{ "QueryHuiLiuFund", "", nil, nil, "lua" },
	{ "GetHuiLiuFundTaskAward", "I", nil, nil, "lua" },
	{ "IgnoreHuiLiuFundAlert", "", nil, nil, "lua" },
	{ "QueryMyRequestBuyOrder", "", nil, nil, "lua" },
	{ "QueryRequestBuyOrderCountByType", "I", nil, nil, "lua" },
	{ "QueryRequestBuyOrderList", "II", nil, nil, "lua" },
	{ "QueryRequestBuyItemPrice", "I", nil, nil, "lua" },
	{ "CreateRequestBuyOrder", "IIIb", nil, nil, "lua" },
	{ "CancelRequestBuyOrder", "d", nil, nil, "lua" },
	{ "RefreshRequestBuyOrder", "d", nil, nil, "lua" },
	{ "SellItemToRequestBuyOrder", "dIsI", nil, nil, "lua" },
	{ "OpenRequestBuyWindow", "I", nil, nil, "lua" },
	{ "QueryRequestBuyOrderCount", "", nil, nil, "lua" },
	{ "QueryAttachJingCaiButton", "", nil, nil, "lua" },
	{ "QueryClassTransferFund", "", nil, nil, "lua" },
	{ "RequestBuyClassTransferFund", "", nil, nil, "lua" },
	{ "GetClassTransferFundTaskAward", "I", nil, nil, "lua" },
	{ "IgnoreClassTransferFundAlert", "", nil, nil, "lua" },
	{ "SubmitJiaNianHua2020PersonalInfo", "ss", nil, nil, "lua" },
	{ "ResponseAppUpgradeNotify", "sss", nil, nil, "lua" },
	{ "RequestTaskOpenTimeCheck", "I", nil, nil, "lua" },
	{ "CheckSpokesmanContractAward", "", nil, nil, "lua" },
	{ "RequestQiangXiangZiSignUpInfo", "", nil, nil, "lua" },
	{ "RequestSignUpQiangXiangZi", "", nil, nil, "lua" },
	{ "RequestCancelSignUpQiangXiangZi", "", nil, nil, "lua" },
	{ "RequestQiangXiangZiOpenOneChest", "", nil, nil, "lua" },
	{ "RequestQiangXiangZiOpenAllChest", "", nil, nil, "lua" },
	{ "RequestEnterHouseCompetitionTemplateShow", "I", nil, nil, "lua" },
	{ "RequestEnterJiFuFeedFish", "", nil, nil, "lua" },
	{ "RequestZhongCao2020", "II", nil, nil, "lua" },
	{ "QueryAllZhongCaoCount2020", "", nil, nil, "lua" },
	{ "QueryDouble11VouchersInfo", "", nil, nil, "lua" },
	{ "RequestUseDouble11Voucher", "", nil, nil, "lua" },
	{ "SignUpHalloween2020ArrestPlay", "", nil, nil, "lua" },
	{ "CancelSignUpHalloween2020ArrestPlay", "", nil, nil, "lua" },
	{ "QueryHalloween2020ArrestPlayMatchInfo", "", nil, nil, "lua" },
	{ "QueryHalloween2020ArrestPlayInfo", "", nil, nil, "lua" },
	{ "QueryHalloweenTerrifyAndDanShiInfo", "", nil, nil, "lua" },
	{ "QueryHouseMemberAtHomeInfo", "d", nil, nil, "lua" },
	{ "RequestEquipItemReminded", "IIs" },
	{ "SetAutoPickExtra", "s", nil, nil, "lua" },
	{ "QueryDouble11LotteryResult", "", nil, nil, "lua" },
	{ "QueryZhaoHuiFriendList", "", nil, nil, "lua" },
	{ "QueryAlreadyZhaoHuiFriendList", "", nil, nil, "lua" },
	{ "RequestZhaoHuiInviteFriend", "d", nil, nil, "lua" },
	{ "QueryZhaoHuiTaskInfo", "d", nil, nil, "lua" },
	{ "RequestGetZhaoHuiTaskReward", "dI", nil, nil, "lua" },
	{ "QueryHuiGuiInviterInfo", "", nil, nil, "lua" },
	{ "RequestHuiGuiInviteBind", "d", nil, nil, "lua" },
	{ "RequestGetHuiGuiTaskReward", "dI", nil, nil, "lua" },
	{ "RequestOpenHuiGuiShop", "", nil, nil, "lua" },
	{ "RequestHuiLiuBindWithMe", "d", nil, nil, "lua" },
	{ "QueryShowNewZhaoHuiBtn", "", nil, nil, "lua" },
	{ "RequestAcceptBindWithInvitee", "ds", nil, nil, "lua" },
	{ "RequestSignUpDuoMaoMao", "", 1000, nil, "lua" },
	{ "RequestCancelSignUpDuoMaoMao", "", 1000, nil, "lua" },
	{ "Wish_AddWish", "IIIIss", 1000, nil, "lua" },
	{ "Wish_DelWish", "s", 1000, nil, "lua" },
	{ "Wish_AddHelp", "dssIIIIss", 1000, nil, "lua" },
	{ "Wish_QueryWishLimit", "", nil, nil, "lua" },
	{ "LuaQueryMonthPresentLimit", "I", nil, nil, "lua" },
	{ "QueryPlayReport", "s", nil, nil, "lua" },
	{ "QueryAnQiIslandPlayInfo", "", nil, nil, "lua" },
	{ "RequestSyncRepertoryForMallPreview", "", 1000, nil, "lua" },
	{ "RequestLearnNew125Skill", "I", 1000, nil, "lua" },
	{ "QueryTianChengData", "", nil, nil, "lua" },
	{ "RequestGetTianChengLevelReward", "I", nil, nil, "lua" },
	{ "RequestGetTianChengTaskReward", "I", nil, nil, "lua" },
	{ "RequestBuyTianChengLevel", "I", nil, nil, "lua" },
	{ "RequestWatchTangYuan2021", "", nil, nil, "lua" },
	{ "RequestSignTangYuan2021", "", 1000, nil, "lua" },
	{ "RequestCancelSignTangYuan2021", "", 1000, nil, "lua" },
	{ "ResetZhuJueJuQingTasksForReview", "", 1000, nil, "lua" },
	{ "QueryTodayTangYuan2021Score", "", nil, nil, "lua" },
	{ "QueryPlayerIsMatchingTangYuan2021", "", 1000, nil, "lua" },
	{ "YanHuaLiBaoScreenshotReport", "ss", 5000, nil, "lua" },
	{ "RequestPlayYanHuaLiBao", "ssss", 10000, nil, "lua" },
	{ "RequestSignUpHanJiaAdventure", "", nil, nil, "lua" },
	{ "RequestCancelSignUpHanJiaAdventure", "", nil, nil, "lua" },
	{ "OpenHanJiaAdventureSignUpWnd", "", nil, nil, "lua" },
	{ "PlayerRequestAutoLayoutCheck", "", nil, nil, "lua" },
	{ "PlayerRequestAutoHouseLayout", "ub", nil, nil, "lua" },
	{ "RequestExchange125SkillWithBangGong", "", nil, nil, "lua" },
	{ "SelectRewardsByUseItem", "uIIs", nil, nil, "lua" },
	{ "SignupCareFireworkMatch", "", 1000, nil, "lua" },
	{ "CareFireworkCancelSignUp", "", 1000, nil, "lua" },
	{ "CareFireworkCheckMatch", "", 1000, nil, "lua" },
	{ "QueryPlayerShopSearchedItemsV2", "IIIIbI" },
	{ "MapEditorMonsterCastSkill", "III", nil, nil, "lua" },
	{ "MapEditorGenerateMonster", "IIiiII", nil, nil, "lua" },
	{ "MapEditorKillMonster", "", nil, nil, "lua" },
	{ "RequestCreateSect", "IIIIsIIsdd", 1000, nil, "lua" },
	{ "GetSectXinWuInfo", "", 1000, nil, "lua" },
	{ "RequestTrackToSectEntrance", "", 1000, nil, "lua" },
	{ "QuerySectInWorldMap", "d", nil, nil, "lua" },
	{ "QueryValentineDaySendFlowerRank", "", 1000, nil, "lua" },
	{ "GetJianJiaCangCangFlowerSeed", "", 1000, nil, "lua" },
	{ "RequestJianJiaCangCangFlowerOperate", "Is", 1000, nil, "lua" },
	{ "QueryJianJiaCangCangPlayInfo", "", 1000, nil, "lua" },
	{ "PlayerRequestBuyOffWorldPass", "Is", nil, nil, "lua" },
	{ "PlayerCollectOffWorldPassReward", "ii", nil, nil, "lua" },
	{ "PlayerCollectOffWorldWeekReward", "", nil, nil, "lua" },
	{ "GetOffWorldPlayInfo", "", nil, nil, "lua" },
	{ "SoulCoreUpgrade", "", 500, nil, "lua" },
	{ "TeleportToJianJiaCangCangScene", "", 5000, nil, "lua" },
	{ "RequestInviteTudiGoinSect", "u", 5000, nil, "lua" },
	{ "QueryTudiSectAndMingGe", "u", 5000, nil, "lua" },
	{ "RequestJianDingMingGe", "", 1000, nil, "lua" },
	{ "QueryPlayerMingGe", "", nil, nil, "lua" },
	{ "QueryToJoinSectInfo", "d", 1000, nil, "lua" },
	{ "RequestJoinSect", "dbdb", 1000, nil, "lua" },
	{ "RequestQuitSect", "d", 1000, nil, "lua" },
	{ "QueryLastInviteSectTime", "u", 1000, nil, "lua" },
	{ "QuerySectInfo", "dsd", 200, nil, "lua" },
	{ "QueryMySectTitleInfo", "d", 1000, nil, "lua" },
	{ "RequestBatchChangeSectOffice", "duu", 1000, nil, "lua" },
	{ "RequestSetSectRights", "du", nil, nil, "lua" },
	{ "RequestResignSectOffice", "d", nil, nil, "lua" },
	{ "RequestBatchSendSectMessage", "dsbb", nil, nil, "lua" },
	{ "RequestSetSectOffice", "ddI", nil, nil, "lua" },
	{ "RequestKickSectMember", "dd", nil, nil, "lua" },
	{ "RequestSetSectMemberSpeakStatus", "ddI", nil, nil, "lua" },
	{ "RequestSetSectShowTitle", "dI", nil, nil, "lua" },
	{ "RequestSetSectHideFromIdSearch", "db", nil, nil, "lua" },
	{ "QueryPlayerSectSpeakStatus", "dd", nil, nil, "lua" },
	{ "QueryCondenseLeftTime", "", nil, nil, "lua" },
	{ "RequestSubmitCondenseItem", "IIs", nil, nil, "lua" },
	{ "RequestPauseCondense", "", nil, nil, "lua" },
	{ "RequestResumeCondense", "", nil, nil, "lua" },
	{ "RequestFinishSoulcoreCarving", "", nil, nil, "lua" },
	{ "QuerySectEvilValue", "d", nil, nil, "lua" },
	{ "RequestSubmitSectEvilYaoGuai", "dIIs", nil, nil, "lua" },
	{ "ReportGatewayOnBan", "", nil, nil, "lua" },
	{ "QueryYiAiZhiMingSendFlowerRank", "", 2000, nil, "lua" },
	{ "EnterShuangshiyi2020Gameplay", "", nil, nil, "lua" },
	{ "EnterDiYuShiKongGameplay", "", nil, nil, "lua" },
	{ "RequestSetWallOrFloorPaperType", "III", nil, nil, "lua" },
	{ "RequestBuyWallOrFloorPaperType", "I", nil, nil, "lua" },
	{ "GetOffWorldRedDotInfo", "", nil, nil, "lua" },
	{ "RequestCatchYaoGuai", "I", nil, nil, "lua" },
	{ "RequestIncYaoGuaiBag", "I", nil, nil, "lua" },
	{ "RequestDeleteYaoGuai", "u", nil, nil, "lua" },
	{ "RequestYaoGuaiTuJianData", "", nil, nil, "lua" },
	{ "RequestGetYaoGuaiTuJianReward", "I", nil, nil, "lua" },
	{ "RequestGetYaoGuaiTuJianFullQualityReward", "I", nil, nil, "lua" },
	{ "QuerySectFaZhenCurrentInfo", "", 500, nil, "lua" },
	{ "RequestPutSoulCoreIntoFaZhen", "", 1000, nil, "lua" },
	{ "RequestPutMonsterIntoFaZhen", "s", 1000, nil, "lua" },
	{ "QueryFaZhenLianHuaMonsterQueueInfo", "", 1000, nil, "lua" },
	{ "RequestRemoveFaZhenLianHuaMonster", "Is", 1000, nil, "lua" },
	{ "QuerySectPersonalInputItemInfo", "", 1000, nil, "lua" },
	{ "QuerySectTopInputItemInfo", "", 1000, nil, "lua" },
	{ "RequestPutItemIntoFaZhen", "IIs", 1000, nil, "lua" },
	{ "QueryLianHuaItemSubmitHistroy", "", 1000, nil, "lua" },
	{ "SendCheckOrderRmbPackageItemId", "I", nil, nil, "lua" },
	{ "RequestContactSectManager", "d", nil, nil, "lua" },
	{ "AntiProfession_Transfer", "II", 1000, nil, "lua" },
	{ "AntiProfession_UpgradeSkill", "I", 1000, nil, "lua" },
	{ "AntiProfession_UnlockSlot", "I", 1000, nil, "lua" },
	{ "AntiProfession_BreakLimit", "I", 1000, nil, "lua" },
	{ "AntiProfession_GenItem", "II", 1000, nil, "lua" },
	{ "CBG_RequestChannelLoginToken", "" },
	{ "ForbidChatLog", "sdIss" },
	{ "RefuseStartGift", "d" },
	{ "RequestUnlockOffWorldBloodShop", "", nil, nil, "lua" },
	{ "GetOffWorldBloodShopInfo", "", nil, nil, "lua" },
	{ "RequestRefreshBloodShopItem", "b", nil, nil, "lua" },
	{ "RequestBuyBloodShopItem", "IIb", nil, nil, "lua" },
	{ "RequestSetSectFaZhenAvoidWuXing", "dd", nil, nil, "lua" },
	{ "QuerySectFaZhenAvoidWuXingInfo", "d", nil, nil, "lua" },
	{ "FinishChuShiLianHuaGuideTask", "", nil, nil, "lua" },
	{ "RequestUseKunXianSuo", "dsII", 5000, nil, "lua" },
	{ "RequestUseKunXianSuoCheck", "d", 1000, nil, "lua" },
	{ "TaskTrackToSectScene", "Id" },
	{ "QuerySectHasTitleMember", "dI", nil, nil, "lua" },
	{ "QueryEnemySectOverview", "d", nil, nil, "lua" },
	{ "QueryEnemySectCaptured", "d", nil, nil, "lua" },
	{ "RequestTrackEnemySect", "d", nil, nil, "lua" },
	{ "QuerySectEntranceByMapId", "u", nil, nil, "lua" },
	{ "QuerySectTaskDestSceneId", "Id" },
	{ "RequestOpenCoulCoreGainWnd", "d", nil, nil, "lua" },
	{ "RequestEnterSelfSectScene", "d", 1000, nil, "lua" },
	{ "QueryXinFuLotteryResult", "", nil, nil, "lua" },
	{ "RequestLearnShiMenMiShuSkill", "", nil, nil, "lua" },
	{ "UpgradeHpSkill", "", nil, nil, "lua" },
	{ "SMSW_RequestSignUp", "", 500, nil, "lua" },
	{ "SMSW_QueryRank", "", 500, nil, "lua" },
	{ "SMSW_QueryPlayInfo", "", 100, nil, "lua" },
	{ "ErHaPlayerEmbraceNpcLimitDist", "II", nil, nil, "lua" },
	{ "QueryDynamicShopShangJiaItem", "", 1000, nil, "lua" },
	{ "QueryAllSectNpcData", "", nil, nil, "lua" },
	{ "QueryMySectInviteNpcData", "", nil, nil, "lua" },
	{ "QuerySectInvitingNpcData", "I", nil, nil, "lua" },
	{ "RequestInviteNpcJoinSect", "I", nil, nil, "lua" },
	{ "RequestNpcLeaveSect", "I", nil, nil, "lua" },
	{ "RequestAcceptSectNpcFriendTask", "I", nil, nil, "lua" },
	{ "RequestMySectNpcId", "", nil, nil, "lua" },
	{ "RequestGetFengHuaLuItemAward", "I", nil, nil, "lua" },
	{ "QueryQingMing2021TuJianInfo", "", nil, nil, "lua" },
	{ "RequestEnableQingMing2021TuJianGrid", "I", nil, nil, "lua" },
	{ "RequestGetQingMing2021TuJianAward", "I", nil, nil, "lua" },
	{ "CaiDieSignUp", "", nil, nil, "lua" },
	{ "CaiDieTaskFinish", "I", nil, nil, "lua" },
	{ "RequestChangeSectWuXing", "I", 2000, nil, "lua" },
	{ "RequestEditPool", "I", 200, nil, "lua" },
	{ "RequestCancelEditPool", "", 200, nil, "lua" },
	{ "RequestSetPoolBegin", "", 200, nil, "lua" },
	{ "RequestSetPool", "uuuu", nil, nil, "lua" },
	{ "RequestDelPool", "uuuu", nil, nil, "lua" },
	{ "RequestSetPoolEnd", "", nil, nil, "lua" },
	{ "RequestSetWarmPool", "uuuu", nil, nil, "lua" },
	{ "RequestDelWarmPool", "uuuu", nil, nil, "lua" },
	{ "RequestPressQiaoqiaoban", "I", 200, nil, "lua" },
	{ "SubmitSelfEditedOffWorldFashion", "IIs", 1000, nil, "lua" },
	{ "RequestRealOffWorldFashion", "", 1000, nil, "lua" },
	{ "SendSectInviteImMessage", "dubsI", 1000, nil, "lua" },
	{ "QueryCrossGnjcGroupInfo", "", 1000, nil, "lua" },
	{ "QueryCrossGnjcServerInfo", "I", 1000, nil, "lua" },
	{ "QuerySectName", "d", nil, nil, "lua" },
	{ "AntiProfession_QueryTransferTime", "", nil, nil, "lua" },
	{ "EnterJiaoShanGamePlay", "", nil, nil, "lua" },
	{ "RequestGetFengHuaLuStageAward", "", nil, nil, "lua" },
	{ "RequestFengHuaLuXuYuan", "I", nil, nil, "lua" },
	{ "RequestMusicBoxState", "", nil, nil, "lua" },
	{ "RequestSwitchMusicBoxState", "II", nil, nil, "lua" },
	{ "RequestMusicBoxPlayList", "", nil, nil, "lua" },
	{ "PlayBigPictureEnd", "", nil, nil, "lua" },
	{ "RequestWuYi2021FindMindData", "", nil, nil, "lua" },
	{ "WuYi2021FindSingleMind", "I", nil, nil, "lua" },
	{ "WuYi2021FindAllMind", "", nil, nil, "lua" },
	{ "LogCanNotHandleOrderInfo", "ssssss", nil, nil, "lua" },
	{ "RequestLearnGuanTianXiangSkill", "I", 100, nil, "lua" },
	{ "QuerySectFutureWeather", "", 1000, nil, "lua" },
	{ "RequestErHaTCGData", "", nil, nil, "lua" },
	{ "ErHaTCGComposeCard", "b", 500, nil, "lua" },
	{ "ErHaTCGReadCard", "I", nil, nil, "lua" },
	{ "ErHaTCGDecomposeCard", "II", nil, nil, "lua" },
	{ "ErHaTCGBatchDecomposeCard", "", nil, nil, "lua" },
	{ "ErHaTCGShareDone", "I", nil, nil, "lua" },
	{ "ErHaTCGBuyHaiTang", "bI", nil, nil, "lua" },
	{ "ErHaTCGAcceptCardTask", "I", nil, nil, "lua" },
	{ "ErHaTCGAcceptScoreTask", "", nil, nil, "lua" },
	{ "ErHaTCGComposeHiddenCard", "I", nil, nil, "lua" },
	{ "ErHaTCGDecomposeCardPiece", "II", nil, nil, "lua" },
	{ "ErHaTCGReviewMainTask", "I", nil, nil, "lua" },
	{ "ErHaTCGReviewCardTask", "I", nil, nil, "lua" },
	{ "RequestReceiveLianHuaLingLiAward", "", nil, nil, "lua" },
	{ "SendCheckOrderDataBegin_GAS3", "", nil, nil, "lua" },
	{ "SendCheckOrderData_GAS3", "s", nil, nil, "lua" },
	{ "SendCheckOrderDataEnd_GAS3", "", nil, nil, "lua" },
	{ "TongQingYouLi_QueryInfo", "", nil, nil, "lua" },
	{ "TongQingYouLi_GetReward", "" },
	{ "TongQingYouLi_Share", "", nil, nil, "lua" },
	{ "TongQingYouLi_Appointment", "", nil, nil, "lua" },
	{ "HangZhouWaterHeightChange", "I" },
	{ "RequestEnterShengChenGangPlay", "", nil, nil, "lua" },
	{ "ZNQZhiBo_SendDanMu", "ss", 1000, "CHAT_TOO_FREQUENT", "lua" },
	{ "ZNQZhiBo_Appointment", "", nil, nil, "lua" },
	{ "ZNQZhiBo_RequestTeleport", "", nil, nil, "lua" },
	{ "RequestOpenXiepaiExtraReward", "d", 5000, nil, "lua" },
	{ "QueryXiepaiExtraRewardSwitch", "d", nil, nil, "lua" },
	{ "SaiMaJumpResult", "I", nil, nil, "lua" },
	{ "ChangeOffWorldFashionHolding", "", nil, nil, "lua" },
	{ "QueryPersonalSpaceBackGroundList", "", nil, nil, "lua" },
	{ "RequestUsePersonalSpaceBackGround", "I", nil, nil, "lua" },
	{ "ClubHouse_QueryRoomList", "dssIs", 200, nil, "lua" },
	{ "ClubHouse_CreateRoom", "ssIs", 1000, nil, "lua" },
	{ "ClubHouse_LeaveRoom", "b", 1000, nil, "lua" },
	{ "ClubHouse_EnterRoom", "dsb", 1000, nil, "lua" },
	{ "ClubHouse_ChangeStatus", "dsb", 1000, nil, "lua" },
	{ "ClubHouse_ClearStatusAll", "s", 1000, nil, "lua" },
	{ "ClubHouse_ForbidStatusAll", "sb", 1000, nil, "lua" },
	{ "ClubHouse_SetMic", "d", 1000, nil, "lua" },
	{ "ClubHouse_UnsetMic", "d", 1000, nil, "lua" },
	{ "ClubHouse_Kick", "d", 1000, nil, "lua" },
	{ "ClubHouse_DestroyRoom", "", 1000, nil, "lua" },
	{ "ClubHouse_ChangeOwner", "d", 1000, nil, "lua" },
	{ "ClubHouse_QueryMemberByStatus", "ss", 200, nil, "lua" },
	{ "ClubHouse_QueryAllListenersInRoom", "", 1000, nil, "lua" },
	{ "ClubHouse_QueryMyRoomId", "s", 500, nil, "lua" },
	{ "QuerySpokesmanSectRankExtra", "", nil, nil, "lua" },
	{ "RequestSignSpokesmanSect", "", nil, nil, "lua" },
	{ "QuerySpokesmanSectSignStatus", "", nil, nil, "lua" },
	{ "RequestCancleSignSpokesmanSect", "", nil, nil, "lua" },
	{ "ShiMenMiShu_UpdateConsumeType", "I", nil, nil, "lua" },
	{ "RequestPerformGnjcZhanYi", "", nil, nil, "lua" },
	{ "SubmitSilverToRepairTongTianTa", "I", nil, nil, "lua" },
	{ "CandyPlaySignUp", "", nil, nil, "lua" },
	{ "CandyPlayCancelSignUp", "", nil, nil, "lua" },
	{ "CandyPlayCheckInMatching", "", nil, nil, "lua" },
	{ "RequestBuySpecialBuildingType", "I", nil, nil, "lua" },
	{ "DoFashionTransform", "b", nil, nil, "lua" },
	{ "WanPiLingShouTaskFail", "I", nil, nil, "lua" },
	{ "CandySelectBabyType", "I", nil, nil, "lua" },
	{ "AntiProfession_RandomChangeScore", "II", 1000, nil, "lua" },
	{ "RequestApplySectLeader", "d", 1000, nil, "lua" },
	{ "RequestSignUpLongZhouPlay", "", nil, nil, "lua" },
	{ "RequestCancelSignUpLongZhouPlay", "", nil, nil, "lua" },
	{ "OpenLongZhouPlaySignUpWnd", "", nil, nil, "lua" },
	{ "ConfirmTransferEquipStone", "iisiis", nil, nil, "lua" },
	{ "QuerySectAlterNameInfo", "d", nil, nil, "lua" },
	{ "RequestAlterSectName", "ds", nil, nil, "lua" },
	{ "RequestAlterSectSkybox", "ddu", nil, nil, "lua" },
	{ "RequestPutMonsterAsItemIntoFaZhen", "s", nil, nil, "lua" },
	{ "RequestStartCondense", "", nil, nil, "lua" },
	{ "RequestJueDou", "d", nil, nil, "lua" },
	{ "QueryAllSectMemberId", "d", nil, nil, "lua" },
	{ "QuerySectInfoFromStone", "d", nil, nil, "lua" },
	{ "RequestUnLockFuXiDance", "", 1000, nil, "lua" },
	{ "RequestPlayFuXiDance", "d", 1000, nil, "lua" },
	{ "FuXiDanceUploadPreCheck", "", 1000, nil, "lua" },
	{ "SubJadeForFuXiDanceUpload", "", 1000, nil, "lua" },
	{ "FindNewFuXiDanceMotionId", "d", nil, nil, "lua" },
	{ "FuXiDanceMotionStatusChange", "IdI", nil, nil, "lua" },
	{ "CollectFuXiDanceMotion", "d", 100, nil, "lua" },
	{ "ReNameFuXiDanceMotion", "Ids", 1000, nil, "lua" },
	{ "DeleteFuXiDanceMotion", "Id", 1000, nil, "lua" },
	{ "ShareFuXiDanceMotion", "dIs", 1000, nil, "lua" },
	{ "MarkClientCareExpression", "dd", 1000, nil, "lua" },
	{ "TeamRequestEnterQueQiaoXianQu", "", nil, nil, "lua" },
	{ "QueryQueQiaoXianQuSignUpInfo", "", nil, nil, "lua" },
	{ "RequestSignUpQueQiaoXianQu", "", nil, nil, "lua" },
	{ "RequestCancelSignUpQueQiaoXianQu", "", nil, nil, "lua" },
	{ "QueryQueQiaoXianQuPartnerInfo", "", nil, nil, "lua" },
	{ "QueQiaoXianQuAnswerQuestion", "s", nil, nil, "lua" },
	{ "RequestEnterYanHuaPlay", "Iid", 1000, nil, "lua" },
	{ "RequestInvitePlayerToYanHuaPlay", "d", 1000, nil, "lua" },
	{ "RequestInviteOnlineFriendsToYanHuaPlay", "", 1000, nil, "lua" },
	{ "RequestInviteGuildMembersToYanHuaPlay", "", 1000, nil, "lua" },
	{ "RequestInviteSectMembersToYanHuaPlay", "", 1000, nil, "lua" },
	{ "RequestJoinYanHuaPlayInvited", "I", nil, nil, "lua" },
	{ "RequestStartFireWorkInYanHuaPlay", "i", nil, nil, "lua" },
	{ "TravestyRole_CheckSignUp", "", 1000, nil, "lua" },
	{ "TravestyRole_SignUpPlay", "I", 1000, nil, "lua" },
	{ "TravestyRole_CancelSignUp", "", 1000, nil, "lua" },
	{ "ChangeFishingButtonPressStatus", "id", nil, nil, "lua" },
	{ "MoveFishBasketToBag", "III", 10, nil, "lua" },
	{ "MoveAllFishToBag", "I", 100, nil, "lua" },
	{ "BasketReorder", "II", 100, nil, "lua" },
	{ "MoveFishFromBasketToHouseRepo", "I", 100, nil, "lua" },
	{ "RequestExchangeHouseRepoFishToScore", "I", 100, nil, "lua" },
	{ "GetbackZhengzhaiFish", "I", 1000, nil, "lua" },
	{ "PlaceZhengzhaiFish", "I", 1000, nil, "lua" },
	{ "PlaceZhengzhaiFishItem", "IIs", 1000, nil, "lua" },
	{ "AttachZhengzhaiFishToFabao", "II", 1000, nil, "lua" },
	{ "SetFishingFood", "I", 200, nil, "lua" },
	{ "RequestExchangeFishToScore", "III", 5, nil, "lua" },
	{ "RequestFeedZhengzhaiFish", "IIIs", 1000, nil, "lua" },
	{ "RequestImproveZhengzhaiFish", "IIIs", 1000, nil, "lua" },
	{ "RequestFixZhengzhaiFishDuration", "II", 1000, nil, "lua" },
	{ "RequestFixZhengzhaiFishDurationInBag", "IIsI", 1000, nil, "lua" },
	{ "RequestExchangeRepoFurnitureToRepairMeterial", "II", nil, nil, "lua" },
	{ "RequestUpgradeHouseFishingSkill", "I", nil, nil, "lua" },
	{ "RequestOlympicBossInfo", "", nil, nil, "lua" },
	{ "TrackToOlympicBossEntranceNpc", "I", nil, nil, "lua" },
	{ "RequestSeaFishingPlayInfo", "", nil, nil, "lua" },
	{ "RequestEnterSeaFishingPlay", "Ib", nil, nil, "lua" },
	{ "RequestRenewExpressionAppearance", "iII", nil, nil, "lua" },
	{ "RequestEnterShuiGuoPaiDui", "", nil, nil, "lua" },
	{ "RequestUnity2018UpgradeInfo", "", nil, nil, "lua" },
	{ "RequestUnity2018UpgradeAward", "", nil, nil, "lua" },
	{ "SetTianQiSpecialItemSkipConfirm", "u", nil, nil, "lua" },
	{ "RequestSetSectEnableAd", "db", nil, nil, "lua" },
	{ "QueryChangeSectJoinStdPreInfo", "d", nil, nil, "lua" },
	{ "RequestConfirmChangeSectJoinStd", "dIIsdd", 1000, nil, "lua" },
	{ "RequestPutAdvancedItemIntoFaZhen", "IIs", 1000, nil, "lua" },
	{ "RequestPutAdvancedMonsterAsItemIntoFaZhen", "s", 1000, nil, "lua" },
	{ "QuerySectVoteNpcVoteId", "I", 1000, nil, "lua" },
	{ "UseFuXiLaba", "IuIIb", nil, nil, "lua" },
	{ "RequestAddHousePoolGrid", "I", nil, nil, "lua" },
	{ "RequestStartFishing", "", nil, nil, "lua" },
	{ "RequestCancelFishing", "", nil, nil, "lua" },
	{ "RequestApplyNewFishAward", "I", nil, nil, "lua" },
	{ "RequestHarvestFish", "", 1000, nil, "lua" },
	{ "RequestClearZhengzhaiFishEffectInBag", "IsIs", 1000, nil, "lua" },
	{ "RequestClearZhengzhaiFishEffect", "IsI", 1000, nil, "lua" },
	{ "RequestExchangeZhangzhaiFishEffect", "IsIIsIIIsI", 1000, nil, "lua" },
	{ "ZuiMengLu_TryReward", "I", nil, nil, "lua" },
	{ "ZuiMengLu_ReadThread", "I", nil, nil, "lua" },
	{ "ZuiMengLu_TraceThread", "I", nil, nil, "lua" },
	{ "ZYJ2021HMLZ_CheckSignUp", "", 1000, nil, "lua" },
	{ "ZYJ2021HMLZ_SignUpPlay", "", 1000, nil, "lua" },
	{ "ZYJ2021HMLZ_CancelSignUp", "", 1000, nil, "lua" },
	{ "RequestOlympicGambleData", "", nil, nil, "lua" },
	{ "DoOlympicGamble", "III", nil, nil, "lua" },
	{ "RequestUseCangHuaZhu", "IIss", 50, nil, "lua" },
	{ "SetAddFriendLvThreashold", "I", 1000, nil, "lua" },
	{ "SetIgnoreAddFriendRequest", "b", 1000, nil, "lua" },
	{ "QueryJianNianHuaTicketSellInfo", "", nil, nil, "lua" },
	{ "OpenJiaNianHuaTicketSellWnd", "", nil, nil, "lua" },
	{ "DragFishingTagInSpecialFishingPlay", "id", nil, nil, "lua" },
	{ "YeSheng_FindThreads", "I", nil, nil, "lua" },
	{ "BindMobileRequestCaptchaDisbind", "", nil, nil, "lua" },
	{ "SendJueDouInviteResult", "db", nil, nil, "lua" },
	{ "CollectRepairToolInSeaFishingPlay", "I", nil, nil, "lua" },
	{ "SendQieCuoInviteResult", "dI", nil, nil, "lua" },
	{ "RequestTempChangeSectWuxing", "dIIs", nil, nil, "lua" },
	{ "SetSecretLove", "ds", 300, nil, "lua" },
	{ "CancelSecretLove", "d", 300, nil, "lua" },
	{ "RequestOpenExchangeLiuHeYeJingWnd", "", nil, nil, "lua" },
	{ "RequestExchangeLiuHeYeJing", "I", nil, nil, "lua" },
	{ "RequestEnterFakeSectScene", "d", 1000, nil, "lua" },
	{ "OfficerReleaseLaoFangBeiZhuoPlayers", "dud", nil, nil, "lua" },
	{ "RefreshSeaFishingDecorationShop", "Id", nil, nil, "lua" },
	{ "ViewZhengWuLuItem", "I", nil, nil, "lua" },
	{ "RequestHarvestFishInSpecialFishingPlay", "", 1000, nil, "lua" },
	{ "PlayerRequestExchangeZhuangShiWu", "II", nil, nil, "lua" },
	{ "QueryAllGuildsCreationAlert", "", nil, nil, "lua" },
	{ "SetRecommendIgnoreBigData", "b", nil, nil, "lua" },
	{ "QueryZhaiXingOpenStatus", "", nil, nil, "lua" },
	{ "RequestEnterZhaiXingPlay", "I", nil, nil, "lua" },
	{ "RequestZhaiXingSuccess", "I", nil, nil, "lua" },
	{ "RequestZhaiXingPassComponent", "I", nil, nil, "lua" },
	{ "OpenLuoChaHaiShiSignUpWnd", "", nil, nil, "lua" },
	{ "RequestSignUpLuoChaHaiShi", "", nil, nil, "lua" },
	{ "RequestCancelSignUpLuoChaHaiShi", "", nil, nil, "lua" },
	{ "InteractWithRoadBarrier", "I", nil, nil, "lua" },
	{ "FortCommitPick", "", nil, nil, "lua" },
	{ "ResurrectPlayer", "I", nil, nil, "lua" },
	{ "PlayerSelectSkill", "I", nil, nil, "lua" },
	{ "RequestLeaveScrollTaskMode", "I", nil, nil, "lua" },
	{ "ShowScrollTaskSubTitleDone", "I", nil, nil, "lua" },
	{ "RequestSaveCombData", "IIII", nil, nil, "lua" },
	{ "ScrollTaskGameEventDone", "Is", nil, nil, "lua" },
	{ "ReportMusicRunClick", "III", nil, nil, "lua" },
	{ "WipeScreenTearsDone", "I", nil, nil, "lua" },
	{ "ClickScreenLightningDone", "I", nil, nil, "lua" },
	{ "ScrollTaskCaptureScreenDone", "s", nil, nil, "lua" },
	{ "RequestArenaInfo", "", 200, nil, "lua" },
	{ "RequestSignupArena", "I", 200, nil, "lua" },
	{ "RequestCancelSignupArena", "", 200, nil, "lua" },
	{ "RequestArenaClassRecord", "", 200, nil, "lua" },
	{ "RequestOpenArenaScoreShop", "", 200, nil, "lua" },
	{ "RequestArenaRankInfo", "III", 200, nil, "lua" },
	{ "RequestGetWeeklyArenaScoreReward", "", nil, nil, "lua" },
	{ "RequestBuyFromArenaScoreShop", "I", nil, nil, "lua" },
	{ "RequestRefreshArenaShopManually", "", nil, nil, "lua" },
	{ "YinXianWan_QueryFurnitureInfo", "I", nil, nil, "lua" },
	{ "TianJiangBaoXiang_OpenWnd", "s", 500, nil, "lua" },
	{ "TianJiangBaoXiang_SelectDoor", "Ib", 500, nil, "lua" },
	{ "TianJiangBaoXiang_SelectReward", "I", 500, nil, "lua" },
	{ "TianJiangBaoXiang_RefreshReward", "", 2000, nil, "lua" },
	{ "TianJiangBaoXiang_SelectBoss", "", 500, nil, "lua" },
	{ "RequestEnterYouHuaChePlay", "", 1000, nil, "lua" },
	{ "RequestQueryHuaCheMapPos", "", 1000, nil, "lua" },
	{ "RequestQueryTopFisher", "", 1000, nil, "lua" },
	{ "OpenJieLiangYuanSignUpWnd", "", nil, nil, "lua" },
	{ "TeamRequestEnterJieLiangYuan", "", nil, nil, "lua" },
	{ "RequestSignUpJieLiangYuan", "", nil, nil, "lua" },
	{ "RequestCancelSignUpJieLiangYuan", "", nil, nil, "lua" },
	{ "JieLiangYuanRequestVoteNeiGui", "db", nil, nil, "lua" },
	{ "JieLiangYuanRequestGetAward", "b", nil, nil, "lua" },
	{ "Double11DSG_QueryTask", "b", nil, nil, "lua" },
	{ "Double11DSG_AcceptTask", "I", nil, nil, "lua" },
	{ "Double11KYD_EnterPlay", "", nil, nil, "lua" },
	{ "RequestEnterHousePoolScene", "s", 500, nil, "lua" },
	{ "RequestFangShengZhengzhaiFish", "IIs", 100, nil, "lua" },
	{ "ChaijieFishingPole", "IIs", 100, nil, "lua" },
	{ "RequestFangShengZhengzhaiFishInBasket", "I", 100, nil, "lua" },
	{ "UniAIDetectRefresh", "su", nil, nil, "lua" },
	{ "RequestAcceptNpcChatEvent", "II", nil, nil, "lua" },
	{ "RequestFinishNpcChatEvent", "II", nil, nil, "lua" },
	{ "RequestFinishNpcChatGroup", "II", nil, nil, "lua" },
	{ "RequestPrepareSendSectItemRedPack", "", nil, nil, "lua" },
	{ "RequestSendSectItemRedPack", "su", nil, nil, "lua" },
	{ "RequestSectItemRedPackOverview", "", 1000, nil, "lua" },
	{ "RequestSectItemRedPackDetail", "I", 1000, nil, "lua" },
	{ "RequestWatchSectItemRedPackLottery", "I", nil, nil, "lua" },
	{ "RequestLeaveWatchSectItemRedPackLottery", "", nil, nil, "lua" },
	{ "RequestSectItemRedPackStartMyLottery", "I", nil, nil, "lua" },
	{ "ConfirmJoinSectItemPackLottery", "dIb", nil, nil, "lua" },
	{ "RequestSectItemRedPackHistory", "", nil, nil, "lua" },
	{ "XinShengHuaJi_AddPicture", "u", nil, nil, "lua" },
	{ "XinShengHuaJi_DelPicture", "I", nil, nil, "lua" },
	{ "XinShengHuaJi_QueryPictures", "", nil, nil, "lua" },
	{ "XinShengHuaJi_AddTaskPic", "Iu", nil, nil, "lua" },
	{ "XinShengHuaJi_ReadPicture", "I", nil, nil, "lua" },
	{ "XinShengHuaJi_NotifyFlowChart", "s", nil, nil, "lua" },
	{ "XinShengHuaJi_AddRankPic", "us", nil, nil, "lua" },
	{ "XinShengHuaJi_QueryRankPic", "d", nil, nil, "lua" },
	{ "XinShengHuaJi_Vote", "dI", nil, nil, "lua" },
	{ "RuMengJi_Unlockable", "I", nil, nil, "lua" },
	{ "RuMengJi_Unlock", "I", nil, nil, "lua" },
	{ "RuMengJi_QueryState", "", nil, nil, "lua" },
	{ "SendFishForFinishingGuide", "", nil, nil, "lua" },
	{ "Watch_FlashInScene", "dd", nil, nil, "lua" },
	{ "Watch_QueryPlayData", "Iss", nil, nil, "lua" },
	{ "WashPermanentProp", "III", nil, nil, "lua" },
	{ "RequestBuyHouseGrassGrid", "II", nil, nil, "lua" },
	{ "RequestSetGrassType", "uuuu", 50, nil, "lua" },
	{ "RequestSetGroundType", "uuuu", 50, nil, "lua" },
	{ "RequestBindHouseGroundBrush", "II", 50, nil, "lua" },
	{ "RequestEditYardGround", "", 50, nil, "lua" },
	{ "RequestEndEditYardGround", "", 50, nil, "lua" },
	{ "RequestGLCExchangeGuildPos", "I", 500, nil, "lua" },
	{ "CancelGLCExchangeGuildPos", "I", 500, nil, "lua" },
	{ "ConfirmGLCExchangeGuildPos", "II", 500, nil, "lua" },
	{ "RefuseExchangeGuildLeagueCrossGuildPos", "I", 500, nil, "lua" },
	{ "QueryForOpenGLCWnd", "", 500, nil, "lua" },
	{ "QueryGLCJoinCrossGuildInfo", "", 500, nil, "lua" },
	{ "QueryGLCGroupScoreInfo", "", 500, nil, "lua" },
	{ "QueryGLCMatchInfo", "b", 500, nil, "lua" },
	{ "QueryGLCServerGuildInfo", "II", 500, nil, "lua" },
	{ "QueryGLCServerMatchInfo", "I", 500, nil, "lua" },
	{ "RequestGetSectNpcInviteReward", "I", nil, nil, "lua" },
	{ "ConfirmSendWeddingInvitation", "usI", nil, nil, "lua" },
	{ "RequestSkipNewWeddingPreGame", "", nil, nil, "lua" },
	{ "RequestStartNewWeddingPreGame", "", nil, nil, "lua" },
	{ "RequestChoseNewWeddingScene", "I", nil, nil, "lua" },
	{ "RequestCancelNewWeddingScene", "", nil, nil, "lua" },
	{ "QueryNewWeddingPartner", "", nil, nil, "lua" },
	{ "RequestPayNewWeddingSceneTicket", "dbsIIId", nil, nil, "lua" },
	{ "RequestSetNewWeddingAnswer", "II", nil, nil, "lua" },
	{ "ResumeNewWeddingConversation", "", nil, nil, "lua" },
	{ "StartNewWeddingConversation", "", nil, nil, "lua" },
	{ "RequestConfirmNewWeddingChoice", "I", nil, nil, "lua" },
	{ "ReplyConfirmNewWeddingChoice", "dIb", nil, nil, "lua" },
	{ "RequestSelectNewWeddingChoice", "I", nil, nil, "lua" },
	{ "SendNewWeddingCloseConversationWnd", "", nil, nil, "lua" },
	{ "CheckEnterWeddingByNewInvitation", "sI", nil, nil, "lua" },
	{ "QueryChoseNewWeddingScene", "b", nil, nil, "lua" },
	{ "RequestEnterWeddingByNewInvitation", "sII", nil, nil, "lua" },
	{ "RequestPostponeNewWeddingPreGame", "", nil, nil, "lua" },
	{ "RequestSignupWeddingPregame", "", nil, nil, "lua" },
	{ "GetYuanDan2022XinYuanRewards", "i", 100, nil, "lua" },
	{ "OpenNpcChatWnd", "I", 1000, nil, "lua" },
	{ "Xingguan_PlayerRequestSwitchTemplate", "dis", nil, nil, "lua" },
	{ "Xingguan_PlayerRequestGetEffectOverview", "ds", nil, nil, "lua" },
	{ "SubmitItemToGuildLeagueTrain", "IsIIII", nil, nil, "lua" },
	{ "QueryGuildLeagueTrainRank", "", nil, nil, "lua" },
	{ "RequestAcceptGuildLeagueBuffTask", "I", nil, nil, "lua" },
	{ "RequestSignUpYuanDan2022FuDan", "", nil, nil, "lua" },
	{ "RequestCancelSignUpYuanDan2022FuDan", "", nil, nil, "lua" },
	{ "RequestQeuryPlayerYuanDan2022FuDanSignUpInfo", "d", nil, nil, "lua" },
	{ "ChangeDirInFixedDirMoving", "dd", nil, nil, "lua" },
	{ "RequestPutChristmasCard", "sII", 500, nil, "lua" },
	{ "RequestGetCardGift", "II", 500, nil, "lua" },
	{ "QueryGuildChristmas2021Card", "", 100, nil, "lua" },
	{ "RequestLightStar", "I", 100, nil, "lua" },
	{ "RequestDreamWishAward", "I", 100, nil, "lua" },
	{ "RequestSignXingyu2021", "b", 100, nil, "lua" },
	{ "RequestCancelSignXingyu2021", "", 100, nil, "lua" },
	{ "QueryPlayerIsMatchingXingyu2021", "", 100, nil, "lua" },
	{ "QueryXingYuRank", "", 200, nil, "lua" },
	{ "QueryOtherChristmasCardInGuild", "II", 200, nil, "lua" },
	{ "RequestEnterDreamWishPlay", "I", 200, nil, "lua" },
	{ "RequestSubmitSchoolRebuildMeterial", "IIIsI", 50, nil, "lua" },
	{ "QuerySchoolRebuildingProgress", "", 100, nil, "lua" },
	{ "ShengXiaoCard_JokePlay", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_CheckInMatching", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_SignUpPlay", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_CancelSignUp", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_Prepare", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_CancelPrepare", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_Start", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_Show", "II", 1000, nil, "lua" },
	{ "ShengXiaoCard_Guess", "I", 1000, nil, "lua" },
	{ "ShengXiaoCard_Pass", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_PassShow", "I", 1000, nil, "lua" },
	{ "ShengXiaoCard_PassGuess", "I", 1000, nil, "lua" },
	{ "ShengXiaoCard_JoinRemoteChannel", "s", 1000, nil, "lua" },
	{ "ShengXiaoCard_LeaveRemoteChannel", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_StartGuaji", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_StopGuaji", "", 1000, nil, "lua" },
	{ "ShengXiaoCard_Interact", "dI", 1000, nil, "lua" },
	{ "RequestWatchGuildLeagueCrossPlay", "II", 500, nil, "lua" },
	{ "BQPQuerySubmitted", "", 100, nil, "lua" },
	{ "BQPRewardByJade", "sd", nil, nil, "lua" },
	{ "BQPPraise", "sIb", nil, nil, "lua" },
	{ "BQPQueryTopTenRank", "III", 100, nil, "lua" },
	{ "BQPQueryPreciousRank", "", 100, nil, "lua" },
	{ "BQPQueryRecommendEquip", "bI", 100, nil, "lua" },
	{ "BQPSubmitEquipPrecheck", "sIsb", 100, nil, "lua" },
	{ "BQPSubmitEquip", "sIsb", 100, nil, "lua" },
	{ "BQPQueryPraiseLeftTimes", "", 100, nil, "lua" },
	{ "SeasonRank_QueryRank", "II", 200, nil, "lua" },
	{ "ShangShiCookbookData", "", nil, nil, "lua" },
	{ "ShangShiMyFoodIngredient", "", nil, nil, "lua" },
	{ "ShangShiOpenIngredientPack", "I", nil, nil, "lua" },
	{ "ShangShiStartCooking", "Iu", nil, nil, "lua" },
	{ "ShangShiRetrieveIngredient", "II", nil, nil, "lua" },
	{ "ShangShiShareCooking", "I", nil, nil, "lua" },
	{ "ShangShiRemoveIngredientNewFlag", "I", nil, nil, "lua" },
	{ "RequestImproveLingShouExProps", "sIIs", 500, nil, "lua" },
	{ "RequestImproveLingShouExChengzhang", "sIIs", 500, nil, "lua" },
	{ "QueryTianShuoData", "", nil, nil, "lua" },
	{ "RequestGetTianShuoLevelReward", "I", nil, nil, "lua" },
	{ "RequestGetTianShuoTaskReward", "I", nil, nil, "lua" },
	{ "RequestBuyTianShuoLevel", "I", nil, nil, "lua" },
	{ "HLPY_StartRescue", "", nil, nil, "lua" },
	{ "HLPY_StopRescue", "", nil, nil, "lua" },
	{ "OpenXueJingKuangHuanSignUpWnd", "", nil, nil, "lua" },
	{ "RequestSignUpXueJingKuangHuan", "", nil, nil, "lua" },
	{ "RequestCancelSignUpXueJingKuangHuan", "", nil, nil, "lua" },
	{ "ChangeDirInXueJingKuangHuanMoving", "dd", nil, nil, "lua" },
	{ "RequestNewWeddingDeclare", "s", nil, nil, "lua" },
	{ "RequestNewWeddingBaiTang", "I", nil, nil, "lua" },
	{ "NewWeddingRequestOpenJiuXi", "bd", nil, nil, "lua" },
	{ "NewWeddingRequestFillInDisk", "III", nil, nil, "lua" },
	{ "NewWeddingRequestEnjoyFood", "I", nil, nil, "lua" },
	{ "XZNW_RequestReward", "I", 100, nil, "lua" },
	{ "XZNW_QueryReward", "", 100, nil, "lua" },
	{ "XZNW_OpenCardWnd", "", 100, nil, "lua" },
	{ "XZNW_EditCardContent", "ds", 100, nil, "lua" },
	{ "XZNW_PlayerSendCard", "", 100, nil, "lua" },
	{ "QueryGLCGuildPosInfo", "", 500, nil, "lua" },
	{ "GlobalMatch_RequestSignUp", "I", 500, nil, "lua" },
	{ "GlobalMatch_RequestSignUpWithExtra", "Iu", 500, nil, "lua" },
	{ "GlobalMatch_RequestCancelSignUp", "I", 500, nil, "lua" },
	{ "GlobalMatch_RequestCheckSignUp", "Id", 500, nil, "lua" },
	{ "BQPQueryShareCost", "s", 500, nil, "lua" },
	{ "WHCB_QueryReward", "", 500, nil, "lua" },
	{ "Curling_ThrowBall", "II", 500, nil, "lua" },
	{ "Curling_QueryRank", "s", 500, nil, "lua" },
	{ "NewWeddingRequestSkipXiuQiu", "", nil, nil, "lua" },
	{ "NewWeddingRequestStartXiuQiu", "", nil, nil, "lua" },
	{ "NewWeddingRequestXiuQiuDate", "", nil, nil, "lua" },
	{ "NewWeddingRequestXiuQiuDiss", "", nil, nil, "lua" },
	{ "NewWeddingRequestXiuQiuSupport", "", nil, nil, "lua" },
	{ "NewWeddingRequestSkipYiXianQian", "", nil, nil, "lua" },
	{ "NewWeddingRequestStartYiXianQian", "db", nil, nil, "lua" },
	{ "NewWeddingRequestSignupYiXianQian", "", nil, nil, "lua" },
	{ "NewWeddingRequestSkipNaoDongFang", "", nil, nil, "lua" },
	{ "NewWeddingRequestStartNaoDongFang", "db", nil, nil, "lua" },
	{ "NewWeddingRequestEnterNaoDongFang", "", nil, nil, "lua" },
	{ "RequestPostponeNewWeddingYiXianQian", "", nil, nil, "lua" },
	{ "RequestPostponeNewWeddingNaoDongFang", "", nil, nil, "lua" },
	{ "NewWeddingRequestEndWedding", "", nil, nil, "lua" },
	{ "RequestOpenYuanXiaoRiddle2022Wnd", "", nil, nil, "lua" },
	{ "RequestCloseYuanXiaoRiddle2022Wnd", "", nil, nil, "lua" },
	{ "SendRiddle2022DanMu", "sb", nil, nil, "lua" },
	{ "SendRiddle2022AskHelpDanMu", "I", nil, nil, "lua" },
	{ "SendRiddle2022GuessedDanMu", "I", nil, nil, "lua" },
	{ "RequestGuessRiddle2022Question", "Isb", nil, nil, "lua" },
	{ "RequestOpenRiddle2022Lantern", "I", nil, nil, "lua" },
	{ "RequestOpenFireworkPartyWnd", "", nil, nil, "lua" },
	{ "RequestSubmitItemsToFireworkParty", "", nil, nil, "lua" },
	{ "RequestSetOffConfessFirework", "ds", nil, nil, "lua" },
	{ "RequestHeChengMeiGuiHuaTaBen", "sII", nil, nil, "lua" },
	{ "RequestGenerateDriftBottle", "sIIbdsssdb", nil, nil, "lua" },
	{ "RequestViewDriftBottleData", "sII", nil, nil, "lua" },
	{ "RequestSetPackagePinnedItemType", "bI", nil, nil, "lua" },
	{ "NewWeddingRequestNotEndWedding", "", nil, nil, "lua" },
	{ "NewWeddingRequestSendCandy", "I", nil, nil, "lua" },
	{ "NewWeddingRequestSnatchCandy", "s", nil, nil, "lua" },
	{ "NewWeddingRequestBuyMallItem", "III", nil, nil, "lua" },
	{ "NewWeddingQueryGuestSide", "", nil, nil, "lua" },
	{ "RequestSendNewWeddingCurrentChoice", "I", nil, nil, "lua" },
	{ "XinShengHuaJi_RequestUnlock", "I", nil, nil, "lua" },
	{ "RequestTerritoryWarMapOverview", "", nil, nil, "lua" },
	{ "RequestTerritoryWarMapDetail", "", 200, nil, "lua" },
	{ "RequestPickTerritoryWarBornRegion", "I", nil, nil, "lua" },
	{ "RequestTerritoryWarTeleport", "II", nil, nil, "lua" },
	{ "RequestCancelTerritoryWarTeleport", "", nil, nil, "lua" },
	{ "RequestSwitchTerritoryWarPlayScene", "I", nil, nil, "lua" },
	{ "RequsetTerritoryWarAvoidWar", "", nil, nil, "lua" },
	{ "RequestTerritoryWarSetFavoriteGrid", "Ib", nil, nil, "lua" },
	{ "RequestTerritoryWarGiveupGrid", "I", nil, nil, "lua" },
	{ "RequestTerritoryWarMigrateCity", "I", nil, nil, "lua" },
	{ "RequestTerritoryWarMakeAlliance", "d", nil, nil, "lua" },
	{ "RequestTerritoryWarBreakAlliance", "d", nil, nil, "lua" },
	{ "RequestTerritoryWarLiberate", "", nil, nil, "lua" },
	{ "TerritoryWarTeleportToGuildLeader", "", nil, nil, "lua" },
	{ "RequestTerritoryWarScoreData", "", nil, nil, "lua" },
	{ "RequestTerritoryWarRelationData", "", nil, nil, "lua" },
	{ "RequestTerritoryWarScenePlayerCount", "", nil, nil, "lua" },
	{ "RequsetTerritoryWarRankInfo", "I", nil, nil, "lua" },
	{ "RequestTerritoryWarFightDetail", "", nil, nil, "lua" },
	{ "ScreenCapturePersonalSpaceShareFinished", "", nil, nil, "lua" },
	{ "NewWeddingRequestSetFirework", "bIb", nil, nil, "lua" },
	{ "RequestBroadcastInviteWine", "sIs", nil, nil, "lua" },
	{ "LiftUpPlayer", "I", nil, nil, "lua" },
	{ "RequestEnterMeiXiangLou", "bI", nil, nil, "lua" },
	{ "SongPlayPressButtom", "III", nil, nil, "lua" },
	{ "RequestGetSongGoodAward", "I", nil, nil, "lua" },
	{ "RequestAccuseSect", "dss", nil, nil, "lua" },
	{ "OpenDouHunCrossGambleWnd", "", nil, nil, "lua" },
	{ "QueryDouHunCrossGambleInfo", "I", nil, nil, "lua" },
	{ "RequestBetDouHunCrossGamble", "IsI", nil, nil, "lua" },
	{ "RequestDoubleBetDouHunCrossGamble", "I", nil, nil, "lua" },
	{ "RequestGetDouHunCrossGambleReward", "I", nil, nil, "lua" },
	{ "QueryDouHunCrossServerGroupInfo", "s", nil, nil, "lua" },
	{ "RequestSetSkillAppearance", "I", nil, nil, "lua" },
	{ "QueryDouHunTrainRank", "I", nil, nil, "lua" },
	{ "OpenDouHunSubmitItemWnd", "", nil, nil, "lua" },
	{ "QueryDouHunStage", "", nil, nil, "lua" },
	{ "QueryDouHunCrossChampionGroupInfo", "I", nil, nil, "lua" },
	{ "ReportFilterShiled", "ss", nil, nil, "lua" },
	{ "EnterQingMing2022PVEPlay", "", nil, nil, "lua" },
	{ "QingMing2022PVEOpenAnswerWnd", "", nil, nil, "lua" },
	{ "QingMing2022PVECancelAnswer", "", nil, nil, "lua" },
	{ "QingMing2022PVEAnswerQuestion", "d", nil, nil, "lua" },
	{ "QingMing2022SingleSelectBuff", "d", nil, nil, "lua" },
	{ "RequestGuildForeignAidInfo", "I", nil, nil, "lua" },
	{ "RequestGuildForeignAidApplyInfo", "", nil, nil, "lua" },
	{ "GuildForeignAidInvitePlayer", "d", nil, nil, "lua" },
	{ "GuildForeignAidAcceptPlayer", "d", nil, nil, "lua" },
	{ "GuildForeignAidRefusePlayer", "d", nil, nil, "lua" },
	{ "GuildForeignAidDeletePlayer", "dI", nil, nil, "lua" },
	{ "GuildForeignAidRefuseAll", "", nil, nil, "lua" },
	{ "RequestGuildForeignAidPersonalInfo", "I", nil, nil, "lua" },
	{ "RequestGuildForeignAidInviteInfo", "I", nil, nil, "lua" },
	{ "GuildForeignAidApply", "ds", nil, nil, "lua" },
	{ "GuildForeignAidCancelApply", "d", nil, nil, "lua" },
	{ "ZhuErDanParkour_DialogEnd", "", nil, nil, "lua" },
	{ "ZhuErDanParkour_EnterPlay", "" },
	{ "BaiSheBianRen_EnterPlay", "I" },
	{ "GuildJuDianQuerySceneOverview", "", nil, nil, "lua" },
	{ "GuildJuDianRequestBackToOwnServer", "", nil, nil, "lua" },
	{ "GuildJuDianRequestGoToOccupation", "", nil, nil, "lua" },
	{ "GuildJuDianRequestTeleportToPosition", "IIII", nil, nil, "lua" },
	{ "GuildJuDianRequestTagPosition", "IIII", nil, nil, "lua" },
	{ "GuildJuDianQueryMapSceneInfo", "II", nil, nil, "lua" },
	{ "GuildJuDianQueryPlayerCountByMapId", "I", nil, nil, "lua" },
	{ "GuildJuDianQueryGlobalTopGuildRank", "", nil, nil, "lua" },
	{ "GuildJuDianQueryGuildMemberDetail", "", nil, nil, "lua" },
	{ "GuildJuDianQueryBattleFieldRank", "", nil, nil, "lua" },
	{ "GuildJuDianRequestApplyBuff", "dd", nil, nil, "lua" },
	{ "GuildJuDianQueryJoinQualification", "", nil, nil, "lua" },
	{ "GuildJuDianQuerySetQualification", "", nil, nil, "lua" },
	{ "GuildJuDianRequestKickoutPlayer", "d", nil, nil, "lua" },
	{ "GuildJuDianQueryGuildFlagAndJuDian", "d", nil, nil, "lua" },
	{ "StartTradeSimulation", "I", nil, nil, "lua" },
	{ "TSBuyItem", "II", nil, nil, "lua" },
	{ "TSSellItem", "II", nil, nil, "lua" },
	{ "TSRepoLevelChange", "I", nil, nil, "lua" },
	{ "MoveToNextCity", "I", nil, nil, "lua" },
	{ "EndTradeSimulation", "", nil, nil, "lua" },
	{ "PutMoneyInBank", "I", nil, nil, "lua" },
	{ "GetMoneyFromBank", "I", nil, nil, "lua" },
	{ "TradeSimulationReturnMoney", "I", nil, nil, "lua" },
	{ "TradeSimulationBuyHp", "I", nil, nil, "lua" },
	{ "TradeSimulateFinishToday", "", nil, nil, "lua" },
	{ "TradeSimulateBuyPegionTick", "I", nil, nil, "lua" },
	{ "PegionTickAward", "", nil, nil, "lua" },
	{ "StartTradeSimulationScenario", "", nil, nil, "lua" },
	{ "TradeSimulationPingMao", "", nil, nil, "lua" },
	{ "GetTicketForChangeChannel", "", 500, nil, "lua" },
	{ "GaoChangCrossApply", "", nil, nil, "lua" },
	{ "GaoChangCrossEnterStage1", "", nil, nil, "lua" },
	{ "GaoChangCrossQueryApplyInfo", "", nil, nil, "lua" },
	{ "RequestArenaCrossServerRankInfo", "II", 200, nil, "lua" },
	{ "QueryWuYiManYouSanJieTaskInfo", "", nil, nil, "lua" },
	{ "RequestAcceptManYouSanJieTask", "I", nil, nil, "lua" },
	{ "StarBiwuRequestZhuWei", "I", nil, nil, "lua" },
	{ "XinBaiProgress_Query", "", nil, nil, "lua" },
	{ "XinBaiProgress_QueryReward", "", nil, nil, "lua" },
	{ "XinBaiProgress_TryReward", "I", nil, nil, "lua" },
	{ "SetMonitorChannel", "u", nil, nil, "lua" },
	{ "RequestReliveDaGongHunPo", "d", nil, nil, "lua" },
	{ "GuildTerritoryWarRequestTeamMemberTeleport", "", nil, nil, "lua" },
	{ "RequestEnterRuYiHole", "", nil, nil, "lua" },
	{ "QueryRuYiHuluwaUnlockData", "", nil, nil, "lua" },
	{ "QueryHuluwaTransformData", "", nil, nil, "lua" },
	{ "QueryHuluwaActivityAwardTimes", "", nil, nil, "lua" },
	{ "QueryHuluwaAdventureWndInfo", "", nil, nil, "lua" },
	{ "RequestEnterHuluwaAdventure", "I", nil, nil, "lua" },
	{ "RequestRollHuluwaAdventure", "", nil, nil, "lua" },
	{ "RequestHuluwaAdventureTransform", "I", nil, nil, "lua" },
	{ "QueryHuluwaBuffLevel", "I", nil, nil, "lua" },
	{ "RequestHuluwaResumeTransform", "", nil, nil, "lua" },
	{ "QueryLianDanLuAlertInfo", "", nil, nil, "lua" },
	{ "QueryLianDanLuPlayData", "", nil, nil, "lua" },
	{ "RequestOpenLianDanLuTaskWnd", "", nil, nil, "lua" },
	{ "RequestGetLianDanLuReward", "II", 200, nil, "lua" },
	{ "QueryPlayerCanJoinGuildTerritoryWar", "", nil, nil, "lua" },
	{ "RequestSummonGuildLeagueMainPlayBoss", "I", nil, nil, "lua" },
	{ "ShenYao_QuerySceneCountTbl", "", 200, nil, "lua" },
	{ "ShenYao_QueryTeamPass", "II", 200, nil, "lua" },
	{ "ShenYao_RequestOpenPersonalNpcWnd", "I", 200, nil, "lua" },
	{ "ShenYao_CreatePersonalNpc", "III", 200, nil, "lua" },
	{ "ShenYao_QueryPersonalNpcInfo", "", 200, nil, "lua" },
	{ "ShenYao_QueryUnlockLevel", "", 200, nil, "lua" },
	{ "ShenYao_QueryNpcEngineId", "I", 200, nil, "lua" },
	{ "ShenYao_QueryRemainTimes", "", 200, nil, "lua" },
	{ "ShenYao_SetRejectLevel", "I", 200, nil, "lua" },
	{ "ShenYao_QueryRejectLevel", "", 200, nil, "lua" },
	{ "ShenYao_CreatePersonalNpcByExchangeTicket", "IsI", 200, nil, "lua" },
	{ "RequestEnterNavalWarCabinPlay", "", nil, nil, "lua" },
	{ "RequestFireNavalWarCannon", "IddI", 200, nil, "lua" },
	{ "RequestChangeNavalWarShipSeat", "I", 1000, nil, "lua" },
	{ "YaoBan_Unlock", "I", nil, nil, "lua" },
	{ "YaoBan_RandomUnlock", "", nil, nil, "lua" },
	{ "YaoBan_Enable", "I", nil, nil, "lua" },
	{ "YaoBan_Disable", "", nil, nil, "lua" },
	{ "YaoBan_OpenWnd", "", nil, nil, "lua" },
	{ "GuanYaoJian_Unlock", "I", nil, nil, "lua" },
	{ "GuanYaoJian_OpenWnd", "", nil, nil, "lua" },
	{ "GuanYaoJian_Reward", "", nil, nil, "lua" },
	{ "QWD_SelectDifficulty", "III", nil, nil, "lua" },
	{ "QWD_SelectBuff", "I", nil, nil, "lua" },
	{ "OpenGuildLeagueTrainWnd", "", nil, nil, "lua" },
	{ "OpenGuildLeagueBuffTaskWnd", "", nil, nil, "lua" },
	{ "QueryShanYeMiZongInfo", "", nil, nil, "lua" },
	{ "QueryShanYeMiZongGambleInfo", "I", nil, nil, "lua" },
	{ "ShanYeMiZongRequestGetXianSuoReward", "I", nil, nil, "lua" },
	{ "ShanYeMiZongRequestGetChapterReward", "I", nil, nil, "lua" },
	{ "ShanYeMiZongRequestFinalRewardInfo", "", nil, nil, "lua" },
	{ "ShanYeMiZongRequestGetFinalReward", "", nil, nil, "lua" },
	{ "ShanYeMiZongRequestGamble", "II", nil, nil, "lua" },
	{ "ShanYeMiZongRequestGetExtraTaskReward", "", nil, nil, "lua" },
	{ "RequestBeginnerGuideTaskReward", "I", nil, nil, "lua" },
	{ "RequestBeginnerGuideFinalReward", "I", nil, nil, "lua" },
	{ "ShuiManJinShan_LeaderRequestSignUp", "II", nil, nil, "lua" },
	{ "ShuiManJinShan_MemberConfirmSignUp", "I", nil, nil, "lua" },
	{ "ShuiManJinShan_QueryTeamSignUp", "", nil, nil, "lua" },
	{ "ShuiManJinShan_CheckPlayOpenMatch", "s", nil, nil, "lua" },
	{ "RequestSetGuildForeignAidType", "dI", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossGambleInfo", "", nil, nil, "lua" },
	{ "RequestBetGuildLeagueCrossGamble", "III", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossGambleRecord", "", nil, nil, "lua" },
	{ "RequestGuildLeagueCrossGambleReward", "", nil, nil, "lua" },
	{ "RequestRemoveFestivalReddot", "I", nil, nil, "lua" },
	{ "XinShengHuaJi_RequestShareRankPic", "d", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossExchangePosRecord", "", nil, nil, "lua" },
	{ "RequestHuanHunShanZhuangPlayTimes", "", nil, nil, "lua" },
	{ "RequestEnterHuanHunShanZhuangPlay", "b", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossServerMatchRecord", "I", nil, nil, "lua" },
	{ "RequestGnjcDianZanData", "II", nil, nil, "lua" },
	{ "RequestDianZanGnjcOtherPlayer", "IId", nil, nil, "lua" },
	{ "RequestCancelDianZanGnjcOtherPlayer", "IId", nil, nil, "lua" },
	{ "ReportGnjcZhiHuiEvaluateResult", "III", nil, nil, "lua" },
	{ "RequestGnjcFastReborn", "", 2000, nil, "lua" },
	{ "RequestGnjcZhiHuiSignUpData", "", nil, nil, "lua" },
	{ "RequestSignUpGnjcZhiHui", "", nil, nil, "lua" },
	{ "RequestCancelSignUpGnjcZhiHui", "", nil, nil, "lua" },
	{ "RequestPerformGnjcZhiHuiFaZhen", "", nil, nil, "lua" },
	{ "RequestSetQmpkAntiProfessionData", "s", nil, nil, "lua" },
	{ "RequestSetQmpkSoulCoreWuXing", "I", nil, nil, "lua" },
	{ "RequestSetQmpkFishWordGroup", "IIII", nil, nil, "lua" },
	{ "QueryCurrentSeasonStarBiwuChampion", "", nil, nil, "lua" },
	{ "RequestRunRollerCoaster", "II", nil, nil, "lua" },
	{ "RequestContinueRollerCoaster", "", nil, nil, "lua" },
	{ "RequestEndRollerCoaster", "II", nil, nil, "lua" },
	{ "RequestRollerCoasterTeleport2Neighbour", "b", nil, nil, "lua" },
	{ "RequestStopRollerCoaster", "b", nil, nil, "lua" },
	{ "RequestResumeRollerCoaster", "", nil, nil, "lua" },
	{ "RequestSpeedUpRollerCoaster", "", 500, nil, "lua" },
	{ "SyncRollerCoasterProgress", "IIId", 50, nil, "lua" },
	{ "LeaveRollerCoasterFurniture", "", nil, nil, "lua" },
	{ "RequestBuyRollerCoasterFurniture", "III", nil, nil, "lua" },
	{ "FKPP_RequestOpenPopoWnd", "", nil, nil, "lua" },
	{ "StartTeamConfirm", "", 200, nil, "lua" },
	{ "TeamConfirm", "b", 200, nil, "lua" },
	{ "OnPlayerPostPersonalSpace", "", nil, nil, "lua" },
	{ "JoinAIDRemoteChannel", "s", nil, nil, "lua" },
	{ "LeaveAIDRemoteChannel", "", nil, nil, "lua" },
	{ "RequestActiveSYMZ", "", nil, nil, "lua" },
	{ "RequestHMTBindReward", "", nil, nil, "lua" },
	{ "QueryQmpkCreateRoleInfo", "", 5000, nil, "lua" },
	{ "RequestModifyJiXiangWuType", "IIsI", 1000, nil, "lua" },
	{ "HuanHunShanZhuangPlayerFindZhengYan", "II", nil, nil, "lua" },
	{ "QueryQmpkZongJueSaiOverView2", "", 500, nil, "lua" },
	{ "RequestSetBackPendantHide", "I", nil, nil, "lua" },
	{ "RequestHasShiTuShiMenReward", "", nil, nil, "lua" },
	{ "RequestShiTuShiMenInfo", "b", nil, nil, "lua" },
	{ "RequestSetShiTuShiMenName", "s", nil, nil, "lua" },
	{ "RequestShiTuShiMenNews", "b", nil, nil, "lua" },
	{ "RequestRecommendShiFu", "b", nil, nil, "lua" },
	{ "RequestRecommendTuDi", "b", nil, nil, "lua" },
	{ "WuLiangShenJing_OpenElevatorWnd", "", 200, nil, "lua" },
	{ "WuLiangShenJing_EnterPrepare", "I", 200, nil, "lua" },
	{ "WuLiangShenJing_EnterPlay", "I", 200, nil, "lua" },
	{ "WuLiangShenJing_QueryHuoBanData", "", 200, nil, "lua" },
	{ "WuLiangShenJing_UpgradeSkill", "II", 200, nil, "lua" },
	{ "WuLiangShenJing_UpgradeInherit", "I", 200, nil, "lua" },
	{ "WuLiangShenJing_ResetHuoBanSkill", "II", 200, nil, "lua" },
	{ "WuLiangShenJing_ResetHuoBanInherit", "I", 200, nil, "lua" },
	{ "WuLiangShenJing_SetChuZhan", "u", 200, nil, "lua" },
	{ "WuLiangShenJing_QueryRecentPass", "I", 200, nil, "lua" },
	{ "WuLiangShenJing_QueryHuoBanProp", "Ib", 200, nil, "lua" },
	{ "SFEQ_StartPlay", "", nil, nil, "lua" },
	{ "SFEQ_Script_RequestCloseWnd", "", nil, nil, "lua" },
	{ "SFEQ_ClickOption", "I", nil, nil, "lua" },
	{ "SFEQ_WPFL_RequestCloseWnd", "", nil, nil, "lua" },
	{ "SFEQ_WPFL_ClickOrReleaseItem", "I", nil, nil, "lua" },
	{ "SFEQ_WPFL_PutItemOnNPC", "II", nil, nil, "lua" },
	{ "SubmitQmpkCertificationInfo", "ss", 5000, nil, "lua" },
	{ "GhostTeleportRequestEnterGame", "ddsIII" },
	{ "StopReadGacNetworkSuccess", "IsI" },
	{ "GhostTeleportLeaveGas", "d" },
	{ "InviteJoinTeamFromGroup", "d", 500, nil, "lua" },
	{ "QueryQmpkPlayerInfo", "d", nil, nil, "lua" },
	{ "FinishShootBirdsInPlay", "", 500, nil, "lua" },
	{ "InterruptShootBirdsInPlay", "", 500, nil, "lua" },
	{ "RequestWuZiQiReady", "Ib", 500, nil, "lua" },
	{ "InvitePlayerJoinWuZiQi", "Id", nil, nil, "lua" },
	{ "AcceptInviteJoinWuZiQi", "Id", nil, nil, "lua" },
	{ "QueryOnlineShiMenPlayerForWuZiQi", "d", 500, nil, "lua" },
	{ "InvitePlayerWatchWuZiQi", "Id", nil, nil, "lua" },
	{ "AcceptWatchWuZiQiInvite", "Id", nil, nil, "lua" },
	{ "LeaveWuZiQi", "I", 1000, nil, "lua" },
	{ "RequestSetWuZiQiGrid", "II", 1000, nil, "lua" },
	{ "RequestResetWuZiQiGrid", "IdI", 1000, nil, "lua" },
	{ "RequestMarkWuZiQiGrid", "II", 1000, nil, "lua" },
	{ "RequestChatInWuZiQi", "Is", 1000, nil, "lua" },
	{ "RequestShiMenQiuXuePuInfo", "b", nil, nil, "lua" },
	{ "RequestShiMenHanZhangJiInfo", "b", nil, nil, "lua" },
	{ "ModifyChuShiJiYu", "dI", nil, nil, "lua" },
	{ "RequestChuShiGiftInfo", "d", nil, nil, "lua" },
	{ "SendChuShiGift", "dII", nil, nil, "lua" },
	{ "SyncGuZhengKeyStrokes", "Iu", 800, nil, "lua" },
	{ "RequestLeaveGuZheng", "I", 1000, nil, "lua" },
	{ "RequestUnlockGuZhengSheet", "I", nil, nil, "lua" },
	{ "RequestPlayGuZhengSheet", "II", 1000, nil, "lua" },
	{ "YYS_InvitePlayer", "", 1000, nil, "lua" },
	{ "YYS_RejectInvitation", "", 1000, nil, "lua" },
	{ "YYS_AcceptInvitation", "", 1000, nil, "lua" },
	{ "YYS_RequestOpenTab", "", 1000, nil, "lua" },
	{ "YYS_ClaimReward", "II", 1000, nil, "lua" },
	{ "QueryHasShanYeMiZongReward", "", nil, nil, "lua" },
	{ "YYS_QuerySeasonRank", "", 1000, nil, "lua" },
	{ "SyncPhysicsMove", "u", nil, nil, "lua" },
	{ "RequestPhysicsServerTime", "d" },
	{ "RequestPhysicsStartTime", "" },
	{ "QueryCompositeExtraEquipWordCost", "IIsIIIIIIb", nil, nil, "lua" },
	{ "RequestCanCompositeExtraEquipWord", "IIsIIsIIIIIIb", nil, nil, "lua" },
	{ "RequestCompositeExtraEquipWord", "IIsIIsIIIIIIb", nil, nil, "lua" },
	{ "CompositeExtraEquipWordResultConfirm", "IIsb", nil, nil, "lua" },
	{ "TryConfirmCompositeExtraEquipWord", "IIs", nil, nil, "lua" },
	{ "QueryDisassembleExtraEquipCostAndItems", "IIs", nil, nil, "lua" },
	{ "RequestDisassembleExtraEquip", "IIs", nil, nil, "lua" },
	{ "RequestCanForgeExtraEquip", "IIsu", nil, nil, "lua" },
	{ "RequestForgeExtraEquip", "IIsu", nil, nil, "lua" },
	{ "RequestCanForgeBreakExtraEquip", "IIs", nil, nil, "lua" },
	{ "RequestForgeBreakExtraEquip", "IIs", nil, nil, "lua" },
	{ "QueryForgeResetExtraEquipCostAndItems", "IIs", nil, nil, "lua" },
	{ "RequestForgeResetExtraEquip", "IIs", nil, nil, "lua" },
	{ "ShiftBackPendant", "iii", nil, nil, "lua" },
	{ "SaveBackPendantPos2Template", "Iiii", nil, nil, "lua" },
	{ "SwitchBackPendantPosTemplate", "I", nil, nil, "lua" },
	{ "RequestEnterShuiGuoPaiDuiForShiTu", "", nil, nil, "lua" },
	{ "XinBaiAchv_ShareToChannel", "II", 1000, nil, "lua" },
	{ "XinBaiAchv_QueryAchievementInfo", "", 1000, nil, "lua" },
	{ "XinBaiAchv_RequestAllClearReward", "", 1000, nil, "lua" },
	{ "OnSendMengDao", "ds", 1000, nil, "lua" },
	{ "SFEQ_QueryPlayData", "", 1000, nil, "lua" },
	{ "RequestTerritoryWarAllianceApplyInfo", "", 300, nil, "lua" },
	{ "TerritoryWarRefuseAllAllianceApplication", "", nil, nil, "lua" },
	{ "RequestOpenTerritoryWarPickBornRegionWnd", "", nil, nil, "lua" },
	{ "RequestTerritoryWarCanDrawCommand", "I", nil, nil, "lua" },
	{ "RequestTerritoryWarDoDrawCommand", "", nil, nil, "lua" },
	{ "QiXi2022_QueryActivityInfo", "", nil, nil, "lua" },
	{ "XinBaiAchv_OnClickAchievementInfo", "I", 500, nil, "lua" },
	{ "RequestGTWRelatedPlayMonsterSceneInfo", "", nil, nil, "lua" },
	{ "CanOpenGTWRelatedPlayContributeWnd", "", nil, nil, "lua" },
	{ "RequestOpenGTWRelatedPlayContributeWnd", "b", nil, nil, "lua" },
	{ "RemoveShinningHouseFishItem", "I", nil, nil, "lua" },
	{ "QueryQmpkHandbookReceiveAwardInfo", "", nil, nil, "lua" },
	{ "RequestFixBeginnerGuideDay2Finish", "I", nil, nil, "lua" },
	{ "SendDouHunWenDaResult", "IbI", nil, nil, "lua" },
	{ "OpenDouHunWenDaWnd", "", nil, nil, "lua" },
	{ "QueryDouHunCrossWaiKaRank", "I", nil, nil, "lua" },
	{ "GuildJuDianRequestTeleportToDailyScene", "III", nil, nil, "lua" },
	{ "GuildJuDianQueryRankInfo", "", nil, nil, "lua" },
	{ "GuildJuDianCanOpenContributionWnd", "", nil, nil, "lua" },
	{ "GuildJuDianQueryContributionWeekData", "", nil, nil, "lua" },
	{ "GuildJuDianQueryContributionSeasonData", "", nil, nil, "lua" },
	{ "GuildJuDianTeleportToLeaderScene", "", 2000, nil, "lua" },
	{ "GuildJuDianTeleportToGuideScene", "", 2000, nil, "lua" },
	{ "GuildJuDianSubmitLingqi", "I", nil, nil, "lua" },
	{ "GuildJuDianRequestDropFlag", "", nil, nil, "lua" },
	{ "GuildJuDianTeleportToTagScene", "I", nil, nil, "lua" },
	{ "JYDSY_RequestMoveToGrid", "II", nil, nil, "lua" },
	{ "JYDSY_RequestPutBarrier", "bII", nil, nil, "lua" },
	{ "QueryDouHunGroupAppearanceInfo", "ud", nil, nil, "lua" },
	{ "RequestBaiShiToMultiPlayers", "u", nil, nil, "lua" },
	{ "RequestShouTuToMultiPlayers", "u", nil, nil, "lua" },
	{ "RequestMyShouTuQuestionInfo", "", nil, nil, "lua" },
	{ "RequestMyBaiShiQuestionInfo", "", nil, nil, "lua" },
	{ "RequestSetGuildForeignAidApplyStatus", "b", nil, nil, "lua" },
	{ "RequestStartShiTuDuLingTask", "", nil, nil, "lua" },
	{ "SyncShiTuDuLingTaskInfo", "dIiI", nil, nil, "lua" },
	{ "RequestFinishShiTuDuLingTask", "", nil, nil, "lua" },
	{ "RequestEnterGTWGuidePlay", "", nil, nil, "lua" },
	{ "RequestPrepareSendShiTuGift", "d", nil, nil, "lua" },
	{ "RequestSetShiTuGiftWish", "bsII", nil, nil, "lua" },
	{ "RequestSendShiTuGift", "dbsII", nil, nil, "lua" },
	{ "RequestSetShifuPingJia", "I", nil, nil, "lua" },
	{ "TryExchangeGTWRelatedPlayCityMonsterHp", "", nil, nil, "lua" },
	{ "ExchangeGTWRelatedPlayCityMonsterHp", "I", nil, nil, "lua" },
	{ "RequestCreateShiTuGroupIm", "", nil, nil, "lua" },
	{ "RequestShifuAllTudiInfo", "I", nil, nil, "lua" },
	{ "RequestExchangeShiTuBaiShiTieToChuShiTie", "IIs", nil, nil, "lua" },
	{ "QuerySmartGMTokenRaw", "", nil, nil, "lua" },
	{ "QueryUXDynamicShopShangJiaGiftItems", "", nil, nil, "lua" },
	{ "BuyUXDynamicGiftItems", "Is", nil, nil, "lua" },
	{ "ClickUXDynamicGiftIcon", "Is", nil, nil, "lua" },
	{ "QueryUXDynamicMallHomeItems", "", nil, nil, "lua" },
	{ "DisplayUXDynamicMallHomeItems", "s", nil, nil, "lua" },
	{ "ClickUXDynamicMallHomeItem", "sI", nil, nil, "lua" },
	{ "BuyUXDynamicMallHomeItem", "sII", nil, nil, "lua" },
	{ "RequestInviteTudiJoinShiTuTeam", "I", nil, nil, "lua" },
	{ "Double11MQHX_BroadCastAction", "Iu", nil, nil, "lua" },
	{ "Double11SZTB_EnterGame", "", nil, nil, "lua" },
	{ "Double11SZTB_PushCart", "d", nil, nil, "lua" },
	{ "GJZL_RequestEnterPlay", "", 1000, nil, "lua" },
	{ "QueryDouHunJoinCrossGroupInfo", "II", nil, nil, "lua" },
	{ "QueryJinLuHunYuanZhanSkillInfo", "", nil, nil, "lua" },
	{ "ChooseJinLuHunYuanZhanSkill", "u", nil, nil, "lua" },
	{ "EnterXiangYaoYuQiuSiPlay", "I", nil, nil, "lua" },
	{ "QueryXiangYaoYuQiuSiRank", "", nil, nil, "lua" },
	{ "GJZL_SelectPosition", "I", 1000, nil, "lua" },
	{ "RequestSetCuJuPlace", "I", 1000, nil, "lua" },
	{ "QuerySectMission_Ite", "d", nil, nil, "lua" },
	{ "RequestSetSectMission_Ite", "ds", nil, nil, "lua" },
	{ "RequestAcceptSectApply_Ite", "d", nil, nil, "lua" },
	{ "RequestCleanSectApply_Ite", "d", nil, nil, "lua" },
	{ "RequestApplySect_Ite", "d", nil, nil, "lua" },
	{ "RequestApplyAllSect_Ite", "", nil, nil, "lua" },
	{ "QueryZhuoYaoUnlockInfo_Ite", "", nil, nil, "lua" },
	{ "QueryYaoguaiCatchInfo_Ite", "I", nil, nil, "lua" },
	{ "RequestGetHalloween2022HongBaoCover", "", nil, nil, "lua" },
	{ "RequestSignUpTrafficLight", "", nil, nil, "lua" },
	{ "QueryPlayerIsMatchingTrafficLight", "", nil, nil, "lua" },
	{ "QueryPlayerTrafficLightAward", "", 500, nil, "lua" },
	{ "RequestCancelSignTrafficLight", "", nil, nil, "lua" },
	{ "RequestStartProtectFire", "I", nil, nil, "lua" },
	{ "SetCrtSelectedHongBaoType", "I", nil, nil, "lua" },
	{ "OnClickFestivalReddot", "I", 1000, nil, "lua" },
	{ "ShiTuWenDaoShiFuAddStageItem", "dIIiis", nil, nil, "lua" },
	{ "ShiTuWenDaoGetTaskReward", "dII", nil, nil, "lua" },
	{ "WorldCupJingCai_RequestReceiveHomeTeamAward", "I", 1000, nil, "lua" },
	{ "WorldCupJingCai_RequestReceiveJingCaiAward", "I", 1000, nil, "lua" },
	{ "WorldCupJingCai_RequestUpdateJingCaiData", "", 1000, nil, "lua" },
	{ "WorldCupJingCai_QueryTaoTaiInfo", "", 1000, nil, "lua" },
	{ "TryJoinShiMenGroupIM", "", nil, nil, "lua" },
	{ "TrySkipTeamGameVideo", "s" },
	{ "QueryJinLuHunYuanZhanHotSkill", "", nil, nil, "lua" },
	{ "QueryJinLuHunYuanZhanOpenStatus", "", nil, nil, "lua" },
	{ "QueryStarBiwuStatusForActivity", "b", 100, nil, "lua" },
	{ "QueryBiWuCrossMatchInfo", "I", nil, nil, "lua" },
	{ "QueryBiWuCrossTeamInfo", "I", nil, nil, "lua" },
	{ "RequestWatchBiWuCross", "I", nil, nil, "lua" },
	{ "RequestEnterBiWuCrossPreapareScene", "", nil, nil, "lua" },
	{ "RequestSetBiWuCrossHero", "d", nil, nil, "lua" },
	{ "RequestCatchYaoGuai_Ite", "II", nil, nil, "lua" },
	{ "RequestRandomWuXing_Ite", "d", nil, nil, "lua" },
	{ "QuerySectFuWenRank_Ite", "d", 1000, nil, "lua" },
	{ "QuerySectBoomRank_Ite", "d", 1000, nil, "lua" },
	{ "RequestEnterSectLingfuPlay_Ite", "d", 1000, nil, "lua" },
	{ "RequestJoinSectXiuxingPlay_Ite", "d", 1000, nil, "lua" },
	{ "RequestSectXiuxingPlayStatus_Ite", "d", nil, nil, "lua" },
	{ "RequestChangeSectStone_Ite", "d", nil, nil, "lua" },
	{ "QueryBiWuCrossGroupTeamInfo", "I", nil, nil, "lua" },
	{ "RequestGiveUpWuZiQi", "I", nil, nil, "lua" },
	{ "RestartWuZiQi", "Iu", nil, nil, "lua" },
	{ "EnterShuiGuoPaiDuiForShiTuTask", "", nil, nil, "lua" },
	{ "QueryStarBiwuZhanduiRenqiRank", "", 1000, nil, "lua" },
	{ "QueryStarBiwuZhanduiDetailRank", "d", 100, nil, "lua" },
	{ "RequestStarBiwuSendGift", "IId", nil, nil, "lua" },
	{ "BingTianShiLianEnterPlay", "I", nil, nil, "lua" },
	{ "BingTianShiLianRequestData", "", nil, nil, "lua" },
	{ "ConfirmCommitRiddle2023", "II", nil, nil, "lua" },
	{ "RequestGuessRiddle2023", "IIs", nil, nil, "lua" },
	{ "RequestCCSpeak", "ss", nil, nil, "lua" },
	{ "Spring2023SLTT_EnterGame", "b", nil, nil, "lua" },
	{ "Spring2023ZJNH_AcceptTask", "I", nil, nil, "lua" },
	{ "Spring2023ZJNH_QueryTasks", "", nil, nil, "lua" },
	{ "QueryXinYuanBiDaStatus", "I", nil, nil, "lua" },
	{ "QueryXinYuanBiDaStatusAll", "", nil, nil, "lua" },
	{ "RequestAcceptXinYuanBiDaWish", "I", nil, nil, "lua" },
	{ "EnterYuanDanQiYuanPlay", "", nil, nil, "lua" },
	{ "QueryYuanDanQiYuanAwardTimes", "", nil, nil, "lua" },
	{ "TurkeyMatchSnowballHitPlayer", "d", nil, nil, "lua" },
	{ "QueryTurkeyMatchRankData", "", 500, nil, "lua" },
	{ "QueryPlayerTurkeyMatchInterfaceData", "", 500, nil, "lua" },
	{ "QueryCanStartChristmasKindnessGuildPlay", "", 500, nil, "lua" },
	{ "StartChristmasKindnessGuildPlay", "", 500, nil, "lua" },
	{ "GaoChangJiDouCrossQueryApplyInfo", "", nil, nil, "lua" },
	{ "GaoChangJiDouCrossApply", "", nil, nil, "lua" },
	{ "GaoChangJiDouCrossEnterStage1", "", nil, nil, "lua" },
	{ "JoinRemoteChannel", "Iss" },
	{ "LeaveRemoteChannel", "I" },
	{ "RequestAutoFinishSectTask", "I", nil, nil, "lua" },
	{ "RequestYaoGuaiRedAlert", "", nil, nil, "lua" },
	{ "FinishPutToFazhenTask", "", nil, nil, "lua" },
	{ "PDFY_SelectDifficulty", "III", 2000, nil, "lua" },
	{ "PDFY_RequestMonsterReward", "II", 1000, nil, "lua" },
	{ "PDFY_RequestMonsterAllReward", "", 1000, nil, "lua" },
	{ "PDFY_RequestShuffleBuff", "I", nil, nil, "lua" },
	{ "PDFY_RequestReplaceAllBuff", "", 1000, nil, "lua" },
	{ "PDFY_RequestLevelUpSkill", "I", nil, nil, "lua" },
	{ "PDFY_RequestEquipSkill", "I", 1000, nil, "lua" },
	{ "PDFY_RequestResetSkill", "", 1000, nil, "lua" },
	{ "PDFY_SetFinishGuiding", "", 1000, nil, "lua" },
	{ "PDFY_RequestOpenMainWnd", "", 1000, nil, "lua" },
	{ "PDFY_SelectBlessing", "III", 1000, nil, "lua" },
	{ "RequestXuePoLiXianData", "", nil, nil, "lua" },
	{ "XuePoLiXianUnlock", "I", nil, nil, "lua" },
	{ "XuePoLiXianLevelUp", "I", nil, nil, "lua" },
	{ "XuePoLiXianReset", "I", nil, nil, "lua" },
	{ "XuePoLiXianSetSnowmanType", "I", nil, nil, "lua" },
	{ "XuePoLiXianEnterSinglePlay", "I", nil, nil, "lua" },
	{ "XuePoLiXianRequestTeamPrepare", "I", nil, nil, "lua" },
	{ "XuePoLiXianUpdatePrepareStatus", "b", nil, nil, "lua" },
	{ "XuePoLiXianRejectPrepare", "", nil, nil, "lua" },
	{ "PDFY_QueryTempSkill", "", 500, nil, "lua" },
	{ "QueryNaoYuanXiaoTodayScore", "", 1000, nil, "lua" },
	{ "QueryNaoYuanXiaoRemainRewardTimes", "", 1000, nil, "lua" },
	{ "QuerySpriteGameYWToken", "s", 200, nil, "lua" },
	{ "QueryXuePoLiXianBattleInfo", "", nil, nil, "lua" },
	{ "QueryXianKeLaiPlayData", "", nil, nil, "lua" },
	{ "RequestOpenXianKeLaiWnd", "", nil, nil, "lua" },
	{ "RequestOpenXianKeLaiTaskWnd", "", nil, nil, "lua" },
	{ "RequestGetXianKeLaiReward", "II", nil, nil, "lua" },
	{ "RequestBuyXianKeLaiLevel", "I", nil, nil, "lua" },
	{ "RequestResetXianKeLaiLevel", "", nil, nil, "lua" },
	{ "RequestHanHuaMiBaoData", "", nil, nil, "lua" },
	{ "LotteryHanHuaMiBao", "I", nil, nil, "lua" },
	{ "RequestValentineYWQSInfo", "", nil, nil, "lua" },
	{ "RequestSignUpValentineYWQS", "", nil, nil, "lua" },
	{ "ReplyValentineYWQSInvitation", "db", nil, nil, "lua" },
	{ "AcceptValentineYWQSExpressionTask", "I", nil, nil, "lua" },
	{ "AcceptValentineYWQSDateTask", "I", nil, nil, "lua" },
	{ "RequestValentineYWQSRankInfo", "", nil, nil, "lua" },
	{ "ShareValentineYWQSToMengDao", "", nil, nil, "lua" },
	{ "VoteToValentineYWQSCouple", "dI", nil, nil, "lua" },
	{ "ValentineYWQSShowDoExpression", "I", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossAidEvaluteInfo", "", nil, nil, "lua" },
	{ "RequestSetSectUseEnemyForTask", "db", nil, nil, "lua" },
	{ "QuerySectRedAlert", "d", nil, nil, "lua" },
	{ "RequestValentineLGTMPlayInfo", "", nil, nil, "lua" },
	{ "SignUpValentineLGTMGameplay", "b", nil, nil, "lua" },
	{ "GuildJuDianQueryPlayerCountAll", "", 1000, nil, "lua" },
	{ "GuildJuDianQueryQueryMapLingQuanPos", "d", 1000, nil, "lua" },
	{ "GuildJuDianSubmitLingQuanInFight", "d", 1000, nil, "lua" },
	{ "GuildJuDianQueryLingQiSubmitInfoNew", "", 1000, nil, "lua" },
	{ "GuildJuDianQueryGuildGongFengData", "", nil, nil, "lua" },
	{ "GuildJuDianSubmitGongFengMaterial", "IIII", 1000, nil, "lua" },
	{ "RequestGTWSubmitLingQiInfo", "", 300, nil, "lua" },
	{ "ExchangeGTWRelatedPlayFXY", "", nil, nil, "lua" },
	{ "ExchangeGTWRelatedPlayScore", "I", nil, nil, "lua" },
	{ "ExchangeGTWRelatedPlayBuff", "I", nil, nil, "lua" },
	{ "RequestGTWAltarInfo", "", 300, nil, "lua" },
	{ "SubmitGTWAltarResourceItem", "", nil, nil, "lua" },
	{ "SetGTWGuildEnemy", "db", nil, nil, "lua" },
	{ "RequestGTWZhanLongTaskInfo", "", 300, nil, "lua" },
	{ "AcceptGTWZhanLongTask", "Ib", nil, nil, "lua" },
	{ "RequestRenewZuoQiByItem", "IIss", 1000, nil, "lua" },
	{ "PlayerRequestCompareOnlineProp", "ds", 1000, nil, "lua" },
	{ "PlayerRequestCompareRankProp", "ds", 1000, nil, "lua" },
	{ "PlayerRequestQueryPropComposition", "s", 1000, nil, "lua" },
	{ "PlayerRequestSharePropComposition", "sI", 1000, nil, "lua" },
	{ "PlayerCancelSharePropComposition", "s", 1000, nil, "lua" },
	{ "PlayerQueryPropShareRecord", "s", 1000, nil, "lua" },
	{ "PlayerQueryPropRankData", "s", 1000, nil, "lua" },
	{ "GuildJuDianAcceptZhanLongTask", "Ib", nil, nil, "lua" },
	{ "PlayerQueryExtraSkill", "I", 1000, nil, "lua" },
	{ "QueryGTWZhanLongShop", "", 300, nil, "lua" },
	{ "BuyGTWZhanLongShopItem", "I", nil, nil, "lua" },
	{ "GaoChangJiDouQueryAward", "", nil, nil, "lua" },
	{ "QueryOnlinePlayerSharedProp", "d", nil, nil, "lua" },
	{ "Spring2023TTSY_SitDown", "I", nil, nil, "lua" },
	{ "QueryGuildJuDianZhanLongShop", "", 300, nil, "lua" },
	{ "BuyGuildJuDianZhanLongShopItem", "I", nil, nil, "lua" },
	{ "GuildJuDianQueryZhanLongTaskInfo", "", nil, nil, "lua" },
	{ "RequestCanRecreateExtraEquip", "IIsbb", nil, nil, "lua" },
	{ "RequestRecreateExtraEquip", "IIsbb", nil, nil, "lua" },
	{ "RecreateExtraEquipResultConfirm", "IIsbI", nil, nil, "lua" },
	{ "TryConfirmRecreateExtraEquipWord", "IIsI", nil, nil, "lua" },
	{ "QueryRecreateExtraEquipCost", "IIsb", nil, nil, "lua" },
	{ "ClubHouse_TryReEnterRoom", "", 500, nil, "lua" },
	{ "RequestMergeOverlapItemToRepo", "", 1000, nil, "lua" },
	{ "RequestGTWChallengeMemberInfo", "b", 500, nil, "lua" },
	{ "RequestSetGTWChallengeElite", "db", nil, nil, "lua" },
	{ "RequestClearAllGTWChallengeElite", "", nil, nil, "lua" },
	{ "RequestGTWChallengeBattleFieldInfo", "", nil, nil, "lua" },
	{ "RequestEnterGTWChallengePlay", "II", nil, nil, "lua" },
	{ "RequestGTWChallengeFightDetailInfo", "I", nil, nil, "lua" },
	{ "RequestBroadcastGTWChallengeFightDetail", "I", nil, nil, "lua" },
	{ "SendClientDownloadStatData_Begin", "" },
	{ "SendClientDownloadStatData", "s" },
	{ "SendClientDownloadStatData_End", "" },
	{ "RequestClearGTWChallengeFightDetail", "", nil, nil, "lua" },
	{ "Anniv2023FSC_InvitePlayer", "d", 1000, nil, "lua" },
	{ "Anniv2023FSC_RejectInvitation", "d", 1000, nil, "lua" },
	{ "Anniv2023FSC_AcceptInvitation", "d", 1000, nil, "lua" },
	{ "Anniv2023FSC_QueryRankData", "", 1000, nil, "lua" },
	{ "Anniv2023FSC_RequestSetReady", "", 1000, nil, "lua" },
	{ "Anniv2023FSC_RequestReplaceCard", "I", 1000, nil, "lua" },
	{ "Anniv2023FSC_RequestPlayCard", "II", 1000, nil, "lua" },
	{ "Anniv2023FSC_RequestReplicate", "I", 1000, nil, "lua" },
	{ "Anniv2023FSC_RequestInterrupt", "I", 1000, nil, "lua" },
	{ "Anniv2023FSC_RequestLeaveGame", "", 1000, nil, "lua" },
	{ "Anniv2023FSC_IncGuidingStage", "", 1000, nil, "lua" },
	{ "RequestHuiCaiShanData", "", 100, nil, "lua" },
	{ "SetHuiCaiShanResult", "u", 100, nil, "lua" },
	{ "RequestYiRenSiFindCluesData", "", 100, nil, "lua" },
	{ "YiRenSiReadClues", "I", 100, nil, "lua" },
	{ "OpenDaFuWengSignUpWnd", "", 100, nil, "lua" },
	{ "RequestSignUpDaFuWeng", "", 100, nil, "lua" },
	{ "RequestCancelSignUpDaFuWeng", "", 100, nil, "lua" },
	{ "Anniv2023FSC_QueryPlayWndData", "", 1000, nil, "lua" },
	{ "Anniv2023_QueryFestivalWndData", "", 1000, nil, "lua" },
	{ "Anniv2023FSC_QueryCollectedCards", "I", 1000, nil, "lua" },
	{ "GuildJuDianTaskTeleportToPrepare", "", 1000, nil, "lua" },
	{ "GTWChallengeTeleportToTeamLeader", "", 100, nil, "lua" },
	{ "GTWChallengeAskMemberTeleport", "", 100, nil, "lua" },
	{ "EnterQingMing2023PVP", "", 1000, nil, "lua" },
	{ "QueryTeamRecruitByIdAndTime", "dd", 1000, nil, "lua" },
	{ "DaFuWengFinishCurStage", "I", 1000, nil, "lua" },
	{ "DaFuWengThrowDice", "", 1000, nil, "lua" },
	{ "DaFuWengUseCard", "II", 1000, nil, "lua" },
	{ "DaFuWengGoodsOperate", "bI", 100, nil, "lua" },
	{ "DaFuWengLandTrade", "b", 1000, nil, "lua" },
	{ "DaFuWengPawn", "bbb", 1000, nil, "lua" },
	{ "RequestGuildJuDianChallengeMemberInfo", "b", 1000, nil, "lua" },
	{ "NanDuLordAcceptSchedule", "II", 1000, nil, "lua" },
	{ "NanDuLordSelectScheduleChoice", "IIII", 1000, nil, "lua" },
	{ "LiYuZhangDaiFoodStartCooking", "Iuu", 1000, nil, "lua" },
	{ "EnterQingMing2023PvePlay", "I", 1000, nil, "lua" },
	{ "QueryQingMing2023PveRank", "", 1000, nil, "lua" },
	{ "DaFuWengShuYuanAnswer", "I", 1000, nil, "lua" },
	{ "QueryNewArenaInfo", "", 200, nil, "lua" },
	{ "QueryPlayerArenaDetails", "d", 200, nil, "lua" },
	{ "QueryArenaOpenInfo", "", 200, nil, "lua" },
	{ "QueryNewArenaRankInfo", "IIbI", 200, nil, "lua" },
	{ "QueryArenaTaskInfo", "I", 200, nil, "lua" },
	{ "RequestArenaSetActiveWeekTask", "Ib", nil, nil, "lua" },
	{ "RequestGetArenaWeekTaskReward", "II", nil, nil, "lua" },
	{ "QueryArenaPassportInfo", "", 200, nil, "lua" },
	{ "RequestBuyArenaPassport", "I", nil, nil, "lua" },
	{ "RequestGetArenaPassportLevelReward", "Ib", nil, nil, "lua" },
	{ "RequestBuyArenaPassportLevel", "I", nil, nil, "lua" },
	{ "QueryArenaPassportGiftInfo", "", 200, nil, "lua" },
	{ "QueryArenaPassportRankInfo", "I", 200, nil, "lua" },
	{ "RequestSetGuildJuDianChallengeElite", "db", 200, nil, "lua" },
	{ "RequestClearAllGuildJuDianChallengeElite", "", 1000, nil, "lua" },
	{ "NanDuLordRepayDebt", "I", 1000, nil, "lua" },
	{ "QueryRecommendQiChang", "s", 1000, nil, "lua" },
	{ "RequestSetTargetQiChang", "sI", 1000, nil, "lua" },
	{ "QueryTargetQiChang", "s", 200, nil, "lua" },
	{ "ClientLogCount", "II", nil, nil, "lua" },
	{ "DaFuWengRandomEventOpenTreasure", "", 1000, nil, "lua" },
	{ "DaFuWengFoYuanYaoQian", "", 1000, nil, "lua" },
	{ "PDFY_SwitchShowNextLevelSkillEffect", "b", 200, nil, "lua" },
	{ "Anniv2023_RequestMakeTaben", "I", 1000, nil, "lua" },
	{ "QueryQingMing2023PveInfo", "", 1000, nil, "lua" },
	{ "ReuqestNanDuJuBaoPenReward", "I", 1000, nil, "lua" },
	{ "WuChengEnFangShengFinish", "", 1000, nil, "lua" },
	{ "DaFuWengRandomEventBeggarSelect", "I", 1000, nil, "lua" },
	{ "DuanWu2023PVPVECompareScore", "d", 1000, nil, "lua" },
	{ "DuanWu2023PVPVEBreakBase", "", 1000, nil, "lua" },
	{ "ClubHouse_BroadcastMessageInRoom", "bbss", 1000, "CHAT_TOO_FREQUENT", "lua" },
	{ "PlayerSharedPropGuideInChat", "s", 1000, nil, "lua" },
	{ "UnlockNanDuLordFashion", "I", 200, nil, "lua" },
	{ "UnlockNanDuLordZuoQi", "I", 200, nil, "lua" },
	{ "TakeOnNanDuLordFashion", "I", 1000, nil, "lua" },
	{ "TakeOffNanDuLordFashion", "I", 1000, nil, "lua" },
	{ "RideOnNanDuLordZuoQi", "I", 1000, nil, "lua" },
	{ "RideOffNanDuLordZuoQi", "", 1000, nil, "lua" },
	{ "NanDuLordTakeOffAllFashionAndZuoQi", "", 1000, nil, "lua" },
	{ "DuanWu2023PVPVESeleteTempSkill", "I", 1000, nil, "lua" },
	{ "NanDuLordRequestBecomeLord", "", 1000, nil, "lua" },
	{ "NanDuLordRequestSetLordStatue", "u", 1000, nil, "lua" },
	{ "DaFuWengBulletinBoardReceiveTask", "I", 1000, nil, "lua" },
	{ "RequestBuyNanDuLordMoney", "I", 1000, nil, "lua" },
	{ "EnterEndlessChallengePlay", "", 1000, nil, "lua" },
	{ "ClubHouse_GivePresent", "dII", 200, nil, "lua" },
	{ "ClubHouse_QueryRank", "I", 200, nil, "lua" },
	{ "NanDuTaskBargains", "II", 1000, nil, "lua" },
	{ "RequestDaFuWengPlayData", "", 1000, nil, "lua" },
	{ "RequestGetDaFuWengReward", "II", 1000, nil, "lua" },
	{ "RequestBuyDaFuWengLevel", "I", 1000, nil, "lua" },
	{ "LiYuZhangDaiFoodEnterCopy", "I", 1000, nil, "lua" },
	{ "RequestUnlockStoreRoomByItem", "", 500, nil, "lua" },
	{ "RequestUnlockStoreRoomBySilver", "", 500, nil, "lua" },
	{ "RequestUnlockStoreRoomByJade", "", 500, nil, "lua" },
	{ "MoveItemFromStoreRoom2Bag", "Is", 50, nil, "lua" },
	{ "MoveItemFromBag2StoreRoom", "Is", 50, nil, "lua" },
	{ "StarBiwuQueryLeiGuTimes", "", 100, nil, "lua" },
	{ "StarBiwuRequestByLeiGuTimes", "Id", 100, nil, "lua" },
	{ "Anniv2023PSZJ_RequestMakeConfidant", "", 1000, nil, "lua" },
	{ "Anniv2023PSZJ_RequestDelConfidant", "", 1000, nil, "lua" },
	{ "Anniv2023PSZJ_AttachTabenToEquip", "IsIs", 1000, nil, "lua" },
	{ "Anniv2023PSZJ_RequestCraveName", "IIs", 1000, nil, "lua" },
	{ "ClubHouse_RandomPresent", "dI", 1000, nil, "lua" },
	{ "QueryPlayerCompetitionHonorRank", "b", nil, nil, "lua" },
	{ "QueryCompetitionHonorPlayerInfo", "d", nil, nil, "lua" },
	{ "FenYongZhenXianChooseSkill", "I", nil, nil, "lua" },
	{ "NanDuPaperAddPaper", "I", 1000, nil, "lua" },
	{ "PlayerQueryCrossPropRankData", "s", 1000, nil, "lua" },
	{ "PlayerRequestCompareCrossRankProp", "dsI", 1000, nil, "lua" },
	{ "YiRenSiPlayAniClues", "u", 1000, nil, "lua" },
	{ "Anniv2023FSC_QuerySpecialCards", "", 1000, nil, "lua" },
	{ "QingMing2023PvpQueryFriendRank", "", 1000, nil, "lua" },
	{ "QingMing2023PvpQueryTopRank", "", 1000, nil, "lua" },
	{ "DaFuWengSetPlayerPos", "ii", 100, nil, "lua" },
	{ "RequestLiuRuShiHuanZhuang", "II", nil, nil, "lua" },
	{ "QueryCompetitionHonorChapionGroup", "IIi", nil, nil, "lua" },
	{ "RequestBuyDaFuWengVoicePackage", "I", 1000, nil, "lua" },
	{ "RequestUseDaFuWengVoicePackage", "I", 1000, nil, "lua" },
	{ "TangGuoGuiEnterGame", "", nil, nil, "lua" },
	{ "PlayerQueryTangGuoGuiData", "", nil, nil, "lua" },
	{ "DaoDanXiaoGuiEnterGame", "", nil, nil, "lua" },
	{ "PlayerQueryDaoDanXiaoGuiData", "", nil, nil, "lua" },
	{ "RequestReorderStoreRoom", "", 1000, nil, "lua" },
	{ "XinXiangShiChengQueryLotteryInfo", "", nil, nil, "lua" },
	{ "XinXiangShiChengSelectItems", "u", 1000, nil, "lua" },
	{ "XinXiangShiChengCommonLottery", "", 1000, nil, "lua" },
	{ "XinXiangShiChengAdvancedLottery", "b", 1000, nil, "lua" },
	{ "XinXiangShiChengSendCoin", "d", 200, nil, "lua" },
	{ "XinXiangShiChengExchangeGold", "III", 200, nil, "lua" },
	{ "Anniv2023PSZJ_SetFamilyTreeFlag", "I", 500, nil, "lua" },
	{ "RequestSetTianQiAutoBuy", "II", 500, nil, "lua" },
	{ "DaFuWengLandLvUp", "b", 1000, nil, "lua" },
	{ "MuKeShuiYinFinish", "I", 1000, nil, "lua" },
	{ "CloseQiFuResultWnd", "", 100, nil, "lua" },
	{ "XinXiangShiChengQueryCoinAmount", "", 200, nil, "lua" },
	{ "QueryTeamSelectConfirmWaitInfo", "", 100, nil, "lua" },
	{ "RequestBuyZhanLingLevel", "I", 1000, nil, "lua" },
	{ "JZLYRequestUseFreeStamp", "I", 500, nil, "lua" },
	{ "JZLYQueryStampProgress", "", 500, nil, "lua" },
	{ "MoveItemFromRepoToSimpleRepo", "Is", 1000, nil, "lua" },
	{ "QueryQianDaoInfo_New", "", 1000, nil, "lua" },
	{ "RequestQianDao_New", "", 1000, nil, "lua" },
	{ "RequestBuQianDao_New", "", 1000, nil, "lua" },
	{ "MoveItemFromStoreRoom2Repo", "Is", 50, nil, "lua" },
	{ "MoveItemFromStoreRoom2SimpleRepo", "Is", 50, nil, "lua" },
	{ "MoveSimpleItemFromSimpleRepoToStoreRoom", "III", 50, nil, "lua" },
	{ "MoveItemFromRepo2StoreRoom", "Is", 50, nil, "lua" },
	{ "RequestSubmitStoreRoomTaskItem", "II", 100, nil, "lua" },
	{ "RequestArenaTalentInfo", "", 200, nil, "lua" },
	{ "RequestLevelupArenaTalent", "I", nil, nil, "lua" },
	{ "RequestSetCustomFacialData", "IIIIIuuuuuu", 1000, nil, "lua" },
	{ "RequestGetBigMonthCardDailyGift", "I", 1000, nil, "lua" },
	{ "RequestSetBigMonthCardDefaultItemId", "I", 1000, nil, "lua" },
	{ "VoiceMsgRecovery", "Iss", 100 },
	{ "RequestPaimaiBetSystemItem", "III", 1000, nil, "lua" },
	{ "QueryPaimaiSystemOnShelfItem", "IIII", 200, nil, "lua" },
	{ "QueryPaimaiSystemItemByPrice", "Id", 200, nil, "lua" },
	{ "QueryPaimaiMySystemItems", "", 1000, nil, "lua" },
	{ "NanDuLordGiveUpSchedule", "I", 1000, nil, "lua" },
	{ "DaFuWengSetGuaJi", "b", 1000, nil, "lua" },
	{ "AsianGames2023RequestQuestions", "", 1000, nil, "lua" },
	{ "AsianGames2023QuestionSubmitAnswer", "II", 1000, nil, "lua" },
	{ "PropGuideIsOpenCrossServer", "", nil, nil, "lua" },
	{ "RequestLogMallView", "ss", nil, nil, "lua" },
	{ "QueryCustomFacialData", "II", 1000, nil, "lua" },
	{ "CanTransformZKWeapon", "IIs", 1000, nil, "lua" },
	{ "ConfirmTransformZKWeapon", "IIs", 1000, nil, "lua" },
	{ "QueryStarBiwuReShenSaiZK_QD", "", 1000, nil, "lua" },
	{ "QueryStarBiwuZhengShiSaiZK_QD", "", 1000, nil, "lua" },
	{ "RequestEnterStarBiwuPrepare_QD", "", 1000, nil, "lua" },
	{ "RequestWatchStarBiwuZhengShiSai_QD", "I", 1000, nil, "lua" },
	{ "RequestOpenStoreRoomFromRepoWnd", "", 1000, nil, "lua" },
	{ "RequestOpenRepoFromStoreRoomWnd", "", 1000, nil, "lua" },
	{ "RequestGetPassReward", "III", 1000, nil, "lua" },
	{ "RequestSubmitPassTask", "II", 1000, nil, "lua" },
	{ "RequestBuyPassLevel", "II", 1000, nil, "lua" },
	{ "RequestBuyPassVipByJade", "II", 1000, nil, "lua" },
	{ "RequestGetTeleportPublicMapInfo", "I", 1000, nil, "lua" },
	{ "OpenGuildLeagueAppointHeroWnd", "", 1000, nil, "lua" },
	{ "RequestAppointGuildLeagueHero", "db", nil, nil, "lua" },
	{ "RequestGetGuildLeagueCrossTrainLevelReward", "II", nil, nil, "lua" },
	{ "NavalPvpAcceptSpecialEvent", "IIIb", 1000, nil, "lua" },
	{ "QueryGuildLeagueCrossTrainRankRewardInfo", "", 1000, nil, "lua" },
	{ "StarBiwuQueryJingCaiHistory", "I", 1000, nil, "lua" },
	{ "RequestQueryStarBiwuZhanDuiListQD", "I", 1000, nil, "lua" },
	{ "RequestSelfStarBiwuZhanDuiInfoQD", "", 1000, nil, "lua" },
	{ "QueryGuildLeagueCrossMatchBuffInfo", "", 1000, nil, "lua" },
	{ "GetNavalShipNames", "", 1000, nil, "lua" },
	{ "GaoChangJiDouCrossCancelApply", "", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossZhanLingRank", "", nil, nil, "lua" },
	{ "RequestEnterNavalPreTrainPlay", "", 1000, nil, "lua" },
	{ "RequestSetSubHandWeaponTabenHide", "I", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossRewardPoolInfo", "I", nil, nil, "lua" },
	{ "QueryGuildLeagueCrossZhanLingExtInfo", "", nil, nil, "lua" },
	{ "XianHaiTongXingLeaveTeam", "db", nil, nil, "lua" },
	{ "XianHaiTongXingFinishTask", "I", nil, nil, "lua" },
	{ "XianHaiTongXingGetReward", "I", nil, nil, "lua" },
	{ "XianHaiTongXingPlayerQueryTeamInfo", "", nil, nil, "lua" },
	{ "RequestReturnToNavalSeaPlay", "", 1000, nil, "lua" },
	{ "RequestCreateStarBiwuZhanDui_QD", "ss", 1000, nil, "lua" },
	{ "QueryStarBiwuZhanDuiInfoByMemberId_QD", "d", 1000, nil, "lua" },
	{ "AsianGames2023UnlockHuiJuan", "", 1000, nil, "lua" },
	{ "AsianGames2023GetHuiJuanUnlockAward", "I", 1000, nil, "lua" },
	{ "AsianGames2023GetHuiJuanCollectAward", "I", 1000, nil, "lua" },
	{ "RequestSendPassVipByJade", "IId", 1000, nil, "lua" },
	{ "GaoChangCancelApply", "", nil, nil, "lua" },
	{ "QueryQmpkBiWuZhanDuiMemberForBanPick", "I", nil, nil, "lua" },
	{ "QueryQmpkBiWuZhanDuiInfo", "I", nil, nil, "lua" },
	{ "SetQmpkBiWuPickMemberInfo", "Is", nil, nil, "lua" },
	{ "ConfirmQmpkBiWuPickResult", "I", nil, nil, "lua" },
	{ "SetQmpkBiWuBanMemberId", "Id", nil, nil, "lua" },
	{ "ConfirmQmpkBiWuBanResult", "I", nil, nil, "lua" },
	{ "NavalOnEnterSelectSeat", "I", 1000, nil, "lua" },
	{ "QueryQmpkActivityPageMeedInfo", "", nil, nil, "lua" },
	{ "ReceiveQmpkSignUpNumAward", "I", nil, nil, "lua" },
	{ "RequestEnterWTSS", "b", 1000, nil, "lua" },
	{ "Carnival2023MonsterEnterGame", "", 100, nil, "lua" },
	{ "Carnival2023RequestPlayData", "", 100, nil, "lua" },
	{ "Carnival2023RequestUpgradeSkill", "III", 100, nil, "lua" },
	{ "QueryPlayerAppearance", "ds", 500, nil, "lua" },
	{ "QueryJingYuanPlayInfo", "", nil, nil, "lua" },
	{ "Carnival2023BossEnterGame", "", nil, nil, "lua" },
	{ "QueryCarnival2023Rank", "", 1000, nil, "lua" },
	{ "RequestPlayerOpenWndForPass", "s", 1000, nil, "lua" },
	{ "QueryCompetitionSeasonData", "", 1000, nil, "lua" },
	{ "RequestCastNavalTempSkill", "Idd", 1000, nil, "lua" },
	{ "QueryGuildPinTuData", "", nil, nil, "lua" },
	{ "RequestEnterGuildPinTu", "I", nil, nil, "lua" },
	{ "RequestLeaveGuildPinTu", "", nil, nil, "lua" },
	{ "RequestGetGuildPinTuChip", "I", nil, nil, "lua" },
	{ "RequestReturnGuildPinTuChip", "I", nil, nil, "lua" },
	{ "RequestUseGuildPinTuChip", "III", 1000, nil, "lua" },
	{ "RequestAnswerGuildPinTuQuestion", "Is", 1000, nil, "lua" },
	{ "AskForHelpGuildPinTuQuestion", "I", 1000, nil, "lua" },
	{ "QueryGuildPinTuPlayerData", "", 1000, nil, "lua" },
	{ "RemindUseGuildPinTuChip", "", 1000, nil, "lua" },
	{ "SetFurnitureExpireNotifyChecked", "", 1000, nil, "lua" },
	{ "ReportLuoXiaFengSwimStatus", "I" },
	{ "AsianGames2023RequestGambleBet", "III", 1000, nil, "lua" },
	{ "AsianGames2023QueryGambleInfo", "I", 1000, nil, "lua" },
	{ "AsianGames2023QueryGambleStatus", "", 1000, nil, "lua" },
	{ "AsianGames2023RequestGetGambleReward", "I", 1000, nil, "lua" },
	{ "QueryGuoQing2023Info", "", 1000, nil, "lua" },
	{ "RequestNavalSailorFishing", "b", 1000, nil, "lua" },
	{ "QMKS_RequestPutSkillBooksToRongLu", "u", 1000, nil, "lua" },
	{ "QMKS_RequestKaiShuReward", "I", 100, nil, "lua" },
	{ "RequestUnlockFacialStyle", "I", 100, nil, "lua" },
	{ "FashionLotteryQueryTicketPrice", "I", nil, nil, "lua" },
	{ "FashionLotteryBuyTicket", "I", nil, nil, "lua" },
	{ "FashionLotteryRequestDraw", "I", 1000, nil, "lua" },
	{ "FashionLotteryDisassemble", "IIsI", 1000, nil, "lua" },
	{ "FashionLotteryExchangeFashion", "II", 1000, nil, "lua" },
	{ "RequestTakeOnFashion_Permanent", "I", 500, nil, "lua" },
	{ "RequestTakeOffFashion_Permanent", "I", 500, nil, "lua" },
	{ "DoFashionTransform_Permanent", "b", 500, nil, "lua" },
	{ "RequestDiscardFashion_Permanent", "I", 500, nil, "lua" },
	{ "RequestSetFashionHide_Permanent", "II", 100, nil, "lua" },
	{ "RequestCompositeTaben_Permanent", "IIb", 500, nil, "lua" },
	{ "FastAddWardRobeNumLimit_Permanent", "", 500, nil, "lua" },
	{ "RequestRenewFashion_Permanent", "II", 500, nil, "lua" },
	{ "RequestUnLockFashion_Permanent", "I", 500, nil, "lua" },
	{ "RequestPresentFashion_Permanent", "Id", 500, nil, "lua" },
	{ "RequestModifyFashionDaPeiName_Permanent", "Is", 1000, nil, "lua" },
	{ "RequestSaveFashionDaPeiContent_Permanent", "I", 1000, nil, "lua" },
	{ "RequestUseTeamMemberBg", "I", 1000, nil, "lua" },
	{ "RequestCollectTeamMemberBg", "I", 1000, nil, "lua" },
	{ "RequestUseTeamLeaderIcon", "I", 1000, nil, "lua" },
	{ "RequestCollectTeamLeaderIcon", "I", 1000, nil, "lua" },
	{ "RequestCanQuickForgeExtraEquip", "IIsuI", 1000, nil, "lua" },
	{ "RequestQuickForgeExtraEquip", "IIsuI", 1000, nil, "lua" },
	{ "OnFinishJuQingOperaPlay", "Ib", 1000, nil, "lua" },
	{ "OnFinishJuQingMaskPlay", "I", 1000, nil, "lua" },
	{ "UpdataCarnivalCollectTaskInfo", "", 1000, nil, "lua" },
	{ "CheckFashionDissamble", "IIs", nil, nil, "lua" },
	{ "RequestUpgradeCustomFurnitureTemplate", "I", 1000, nil, "lua" },
	{ "BeginApplyCustomFurnitureTemplate", "II", 1000, nil, "lua" },
	{ "EndApplyCustomFurnitureTemplate", "I", 1000, nil, "lua" },
	{ "RequestBuyCustomFurnitureTemplateSlot", "", 1000, nil, "lua" },
	{ "ZhongYuanPuDu_NotifyFlowChart", "s", 1000, nil, "lua" },
	{ "Double11MQLDBroadFlyAction", "III", nil, nil, "lua" },
	{ "RequestDownloadImRecord", "s", 1000, nil, "lua" },
	{ "RequestUploadImRecord", "s", 500, nil, "lua" },
	{ "UploadImRecordSuccess", "ss", nil, nil, "lua" },
	{ "RequestGTWZhanShenTaskInfo", "", nil, nil, "lua" },
	{ "RequestAcceptGTWZhanShenTask", "I", nil, nil, "lua" },
	{ "RequestUseFashionDaPei_Permanent", "I", 2000, nil, "lua" },
	{ "YaoYeManJuan_Synthesize", "I", nil, nil, "lua" },
	{ "CheckFashionLotteryEnsuranceInfo", "", nil, nil, "lua" },
	{ "CheckFashionLotteryDiscountInfo", "", nil, nil, "lua" },
	{ "RequestManualEndHengYuDangKou", "", nil, nil, "lua" },
	{ "RequestHengYuDangKouStat", "II", nil, nil, "lua" },
	{ "HengYuDangKouResetDifficulty", "I", nil, nil, "lua" },
	{ "HengYuDangKouSelectTeamForBoss2", "I", nil, nil, "lua" },
	{ "HengYuDangKouBoss2MemberConfirm", "", nil, nil, "lua" },
	{ "HengYuDangKouApplyBoss2Leader", "", nil, nil, "lua" },
	{ "HengYuDangKouOpenBox", "", nil, nil, "lua" },
	{ "ClubHouse_ChangePassWord", "s", 200, nil, "lua" },
	{ "ClubHouse_QueryPassWord", "s", 200, nil, "lua" },
	{ "Double11BYDLYSubmitTreasure", "", 1000, nil, "lua" },
	{ "RequestUploadFacialPic", "s", 1000 },
	{ "RequestReviewFacialPic", "sss", 1000 },
	{ "RequestUnlockLXFMap", "I", 3000, nil, "lua" },
	{ "EnterDouble11BYDLYPlay", "I", 1000, nil, "lua" },
	{ "RequestOpenHengYuDangKouInfo", "", nil, nil, "lua" },
	{ "RequestTakeOnFashionSuit_Permanent", "I", 1000, nil, "lua" },
	{ "SetCanSeeLingShouAndPetInGamePlay", "II", 1000, nil, "lua" },
	{ "RequestBeginnerGuideBatchTaskReward", "", 3000, nil, "lua" },
	{ "RequestEvaluateFashionSuit_Permanent", "IIIIIIIs", 2000, nil, "lua" },
	{ "RequestTakeOffFashionSuit_Permanent", "I", 1000, nil, "lua" },
	{ "Request2023Double11RechargeRewardById", "I", 1000, nil, "lua" },
	{ "RequestSet2023Double11ReplacementItems", "u", 1000, nil, "lua" },
	{ "RequestSet2023Double11MustGetItem", "I", 1000, nil, "lua" },
	{ "Request2023Double11RechargeLotteryByTimes", "I", 1000, nil, "lua" },
	{ "RefuseInvitedAsGuildMember", "d", nil, nil, "lua" },
	{ "QueryLuoChaHaiShi2023Info", "", 500, nil, "lua" },
	{ "RequestChangeToCbgTradeItem", "IIs", 1000, nil, "lua" },
	{ "RequestChangeToNoneCbgTradeItem", "IIs", 1000, nil, "lua" },
	{ "Request2023Double11SetRechargeLotteryIsSkipAnimation", "I", 200, nil, "lua" },
	{ "RequestFacialAccuse", "IdsIIssssdssd", 200 },
	{ "RequestEnterLuoChaBossPlay", "", 500, nil, "lua" },
	{ "QueryGuildPinTuProgress", "", nil, nil, "lua" },
	{ "RequestGamePlayPaiMaiBet", "sIdI", 200, nil, "lua" },
	{ "QueryGamePlayPaiMaiOnShelfCount", "IIu", nil, nil, "lua" },
	{ "QueryGamePlayPaiMaiOnShelfItem", "IIIIII", 200, nil, "lua" },
	{ "QueryGamePlayPaiMaiItemByPrice", "sId", 200, nil, "lua" },
	{ "OpenHengYuDangKou", "II", 1000, nil, "lua" },
	{ "QueryQieXianGuideScenePlayerCount", "", 300, nil, "lua" },
	{ "ConfirmUnLockFashionByItem_Permanent", "IsII", 1000, nil, "lua" },
	{ "QueryGamePlayPaiMaiMyItems", "I", 500, nil, "lua" },
	{ "Request2023Double11ExteriorDiscountCoupons", "I", 1000, nil, "lua" },
	{ "RequestCompositing2023Double11Coupons", "II", 1000, nil, "lua" },
	{ "Request2023Double11BuyExteriorByDiscountCoupons", "dI", 1000, nil, "lua" },
	{ "QueryGamePlayPaiMaiHistory", "IIII", 500, nil, "lua" },
	{ "Christmas2023CarftCard", "III", 100, nil, "lua" },
	{ "Christmas2023GetCardGift", "I", 100, nil, "lua" },
	{ "RequestBroadcastHengYuDangKouStat", "II", 1000, nil, "lua" },
	{ "RequestPlayerCreditScoreData", "d", nil, nil, "lua" },
	{ "RequestPublishToStarBiwuFreeMember", "b", 100, nil, "lua" },
	{ "RequestInviteStarBiwuFreeMember", "d", 100, nil, "lua" },
	{ "StarBiwuFreeMemberRequestAcceptBeInvite", "d", 100, nil, "lua" },
	{ "StarBiwuFreeMemberRequestRefuseBeInvite", "d", 100, nil, "lua" },
	{ "QueryStarBiwuJifensaiMatchS13", "III", 100, nil, "lua" },
	{ "QueryStarBiwuJifensaiMainRankS13", "I", 100, nil, "lua" },
	{ "QueryStarBiwuJifensaiRoundRankS13", "II", 0, nil, "lua" },
	{ "QueryStarBiwuTaotaisaiMatchS13", "I", 200, nil, "lua" },
	{ "QueryStarBiwuFreememberS13", "IIII", 200, nil, "lua" },
	{ "QueryStarBiwuMyInviver", "", 200, nil, "lua" },
	{ "BingXuePlay2023EnterGame", "", 200, nil, "lua" },
	{ "RequestSignUpCandyDelivery", "d", nil, nil, "lua" },
	{ "RequestCancelSignCandyDelivery", "", nil, nil, "lua" },
	{ "QueryPlayerIsMatchingCandyDelivery", "", nil, nil, "lua" },
	{ "QueryServerOpenPassedDay", "I", nil, nil, "lua" },
	{ "Christmas2023RequestGuaGuaKa", "b", 100, nil, "lua" },
	{ "FashionLotteryDiassembleUniTicket", "IIs", 1000, nil, "lua" },
	{ "BingXuePlay2023ChoseCircleId", "I", 100, nil, "lua" },
	{ "ExchangeBabyKouDaiLiBao", "", 1000, nil, "lua" },
	{ "HengYuDangKouApplyForFight", "", 1000, nil, "lua" },
	{ "QueryHengYuDangKouTeamMemberInfo", "", 1000, nil, "lua" },
	{ "QueryHengYuDangKouTeamMemberHpInfo", "", 500, nil, "lua" },
	{ "FinishSkillTraining", "I", 100, nil, "lua" },
	{ "QueryStarBiwuJifesanSaiGroupS13", "", 200, nil, "lua" },
	{ "RequestStarBiwuCleanMyInviverList", "", 1000, nil, "lua" },
	{ "RequestSwitchZuoQiPassengerMode", "sI", 500, nil, "lua" },
	{ "Christmas2023GetGuaGuaKaBaoDiAward", "", 1000, nil, "lua" },
	{ "JoinDoubleDragon", "", 500, nil, "lua" },
	{ "DoubleDragonExit", "", 500, nil, "lua" },
	{ "DoubleDragonUseGouZi", "d", 500, nil, "lua" },
	{ "DoubleDragoGetLongzhu", "i", 500, nil, "lua" },
	{ "QueryStarBiwuActivityStatusS13", "", 1000, nil, "lua" },
	{ "QueryStarBiwuTaotaisaiGroupS13", "", 200, nil, "lua" },
	{ "DoubleDragonPickPos", "i", 200, nil, "lua" },
	{ "DoubleDragonGouziBack", "", 100, nil, "lua" },
	{ "RequestLeaveArenaChatChannel", "", 100, nil, "lua" },
	{ "FinishTextBubblePlay", "I", 1000, nil, "lua" },
	{ "QuerySongQiongShen2024Info", "", 200, nil, "lua" },
	{ "RequestEnterSongQiongShenPlay", "b", 200, nil, "lua" },
	{ "ChooseSongQiongShenReward", "I", 200, nil, "lua" },
	{ "RequestChangeCandyDeliveryJob", "dd", nil, nil, "lua" },
	{ "ArenaAddFavToTarget", "d", 100, nil, "lua" },
	{ "QueryPlayerForWuZiQi", "IIu", 100, nil, "lua" },
	{ "QueryStarBiwuJingCaiWenDa", "", 100, nil, "lua" },
	{ "RequestStarBiwuJingCaiWenDaSelect", "II", 100, nil, "lua" },
	{ "HanJia2024InvertedWorld_QueryPlayOpenInfo", "", 1000, nil, "lua" },
	{ "HanJia2024InvertedWorld_RequestEnterPlay", "", 1000, nil, "lua" },
	{ "HanJia2024InvertedWorld_RequestClaimMissionReward", "I", 1000, nil, "lua" },
	{ "HanJia2024InvertedWorld_RequestResetEntrustMission", "I", 1000, nil, "lua" },
	{ "HanJia2024InvertedWorld_RequestExchangePassProgress", "", 100, nil, "lua" },
	{ "ApplyShuangLongQiangZhu", "", 100, nil, "lua" },
	{ "CancelApplyShuangLongQiangZhu", "", 100, nil, "lua" },
	{ "QueryShuangLongQiangZhuApply", "", 100, nil, "lua" },
	{ "SelectRoleInShuangLongQiangZhu", "I", 100, nil, "lua" },
	{ "SyncShuangLongXiZhuEvent", "IIu", 100, nil, "lua" },
	{ "SelectRewardInShuangLongQiangZhu", "I", 100, nil, "lua" },
	{ "SkillTrainingYingLingTransform", "I", 100, nil, "lua" },
	{ "GuideManualClearReddot", "I", 100, nil, "lua" },
	{ "GuideManualAddSearchHistory", "I", 100, nil, "lua" },
	{ "HanJia2024FurySnowman_RequestEnterPlay", "b", 1000, nil, "lua" },
	{ "OnFinishJuQingLearnKungFuPlay", "", 1000, nil, "lua" },
	{ "FailSkillTraining", "I", 0, nil, "lua" },
	{ "RequestDiscardExpressionAppearance", "II", 0, nil, "lua" },
	{ "DiscardProfileFrame", "I", 0, nil, "lua" },
	{ "RequestHengYuDangKouPlayer", "", 1000, nil, "lua" },
	{ "EnterXinNianChuangGuanPlay", "", 1000, nil, "lua" },
	{ "RequestGetExtraEqruipMarketingScoreAward", "I", 1000, nil, "lua" },
	{ "QueryStarBiwuStarScore", "", 1000, nil, "lua" },
	{ "QueryStarBiwuGroupIdx", "", 1000, nil, "lua" },
	{ "RequestEnterHZXJPlay", "", 1000, nil, "lua" },
	{ "RequestPickHZXJTaoHua", "I", 1000, nil, "lua" },
	{ "HuntRabbitSetRingPos", "ii", 1000, nil, "lua" },
	{ "RequestRideOffSpecialZuoQi", "", 1000 },
	{ "QuerySpecialConcernCandidatePlayers", "", 1000, nil, "lua" },
	{ "QueryVNClientAward", "", 100, nil, "lua" },
	{ "RequestVNClientAward", "s", 100, nil, "lua" },
	{ "RequestVNClientFollow", "s", 100, nil, "lua" },
	{ "QueryDayOnlineTime", "", 100, nil, "lua" },
}
