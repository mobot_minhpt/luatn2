
LuaSetGlobalIfNil("g_LuaUtil",
{
	m_strModuleName = "LuaUtil";
}
);

--local strJSON = g_LuaUtil:TableToJSON(tPack, "", "", false);
--local strJSON = g_LuaUtil:TableToJSON(tPack, "", "\t", true);
function g_LuaUtil:TableToJSON(t, strTotalIdent, strIdentChar, bNewLine)
	
	if(t == nil) then return "nil"; end
	
	local strNewLine = "";
	if(bNewLine == true) then strNewLine = "\n"; end
	
	local strJSON = "";
	strJSON = strJSON .. strTotalIdent .. "{ " .. strNewLine;
	
	for k,v in pairs(t) do
		-- str key
		local strKey = k;
		if(type(strKey) ~= "string") then strKey = "" .. tostring(k) .. ""; end
		
		-- str val
		local strVal = tostring(v);
		local strType = type(v);
		if(strType == "nil" or strType == "boolean" or strType == "number") then
			strVal = tostring(v);
		elseif(strType == "table") then
			strVal = strNewLine .. g_LuaUtil:TableToJSON(v, strTotalIdent .. strIdentChar, strIdentChar, bNewLine);
		else -- userdata/function/thread 
			strVal = "\"" .. tostring(v) .. "\"";
		end
		
		strJSON = strJSON .. strTotalIdent .. strIdentChar .. strKey .. " = " .. strVal .. ", " .. strNewLine;
	end
	
	strJSON = strJSON .. strTotalIdent .. "}";
	
	return strJSON;
end

function g_LuaUtil:DumpTable(t)
	if(type(t) ~= "table") then return; end
	
	print("\nDumpTable: " .. tostring(t) .. "");
	for k,v in pairs(t) do
		print(" pairs: [" .. tostring(k) .. "]=[" .. tostring(v) .. "]");
		if(type(v) == "table") then
			self:DumpTable(v);
		end
	end
	print("EndDump: " .. tostring(t) .. "\n");
end

function g_LuaUtil:ClearTable(t)
	if(type(t) ~= "table") then return; end

	for k,v in pairs(t) do
		-- if(type(v) == "table") then
		-- 	self:ClearTable(v);
		-- else
			-- print(" pairs: [" .. tostring(k) .. "]=[" .. tostring(v) .. "]");
			t[k] = nil;
		-- end
	end

end
-- get a lua table key count include int/string keys
function g_LuaUtil:GetTableSize(t)
	local iSize = 0;
	if(t == nil) then return 0; end
	
	for k,v in pairs(t) do
		iSize = iSize + 1;
	end
	return iSize;
end

-- return a string s hex format: abc => "61 62 63"
function g_LuaUtil:GetStringHex(s)

	local tmpTable = {};
	local strHex = "";
	for i = 1, string.len(s), 1 do
		local strByteHex = string.format("%02X", string.byte(s, i));
		tmpTable[i] = strByteHex;
		
	end
	strHex = table.concat(tmpTable,' '); 
	return strHex;
end

-- 将 lua 的字符串转成数组返回给C# 
function g_LuaUtil:GetByteArrayFromString(s)
	local tArray = {};
	for i = 1, string.len(s), 1 do
		tArray[i] = string.byte(s, i);
	end
	return tArray;
end

-- 将 lua number table 转成 string
function g_LuaUtil:GetStringFromByteArray(t)
	local strBuf = "";
	if(type(t) ~= "table") then return strBuf; end
	
	local tmpTable = {};
	for i = 1, #t, 1 do 
		--local iByte = tonumber(t[i]);
		tmpTable[i] = string.char(t[i])
		--strBuf = strBuf .. string.char(iByte);
	end
	local result = table.concat(tmpTable);
	return result;
end


function g_LuaUtil:DeepCopy(tOrgTable)
    local strOrgType = type(tOrgTable);
    local tCopy = {};
    if strOrgType == 'table' then
        tCopy = {};
        for orig_key, orig_value in next, tOrgTable, nil do
            tCopy[ g_LuaUtil:DeepCopy(orig_key) ] = g_LuaUtil:DeepCopy(orig_value);
        end
        setmetatable(tCopy, g_LuaUtil:DeepCopy(getmetatable(tOrgTable)));
    else -- number, string, boolean, etc
        tCopy = tOrgTable;
    end
    return tCopy;
end


--字符串分隔，返回空字符
function g_LuaUtil:StrSplit(str,split)
	local lcSubStrTab = {}  
    while true do  
        local lcPos = string.find(str,split)  
        if not lcPos then  
            lcSubStrTab[#lcSubStrTab+1] =  str      
            break  
        end  
        local lcSubStr  = string.sub(str,1,lcPos-1)  
        lcSubStrTab[#lcSubStrTab+1] = lcSubStr  
        str = string.sub(str,lcPos+1,#str)  
    end  
    return lcSubStrTab  
end

--[[
    @desc: 字符分割（如果分割符在末尾，不会出现异常的空白数据）
    author:{author}
    time:2021-01-15 11:51:24
    --@str:
	--@split: 
    @return:
]]
function g_LuaUtil:StrSplitAdv(str,split)
	local lcSubStrTab = {}  
    while true do  
		local lcPos = string.find(str,split)  
        if not lcPos then  
            lcSubStrTab[#lcSubStrTab+1] =  str      
            break  
		end 
        local lcSubStr  = string.sub(str,1,lcPos-1)  
		lcSubStrTab[#lcSubStrTab+1] = lcSubStr  
		if lcPos == #str then
			break
		end
		str = string.sub(str,lcPos+1,#str)  
    end  
    return lcSubStrTab  
end

--字符串分隔，返回空字符
function g_LuaUtil:StrSplitChinese(str,split)
	local lcSubStrTab = {}  
    while true do  
        local lcPos,lcPos2 = string.find(str,split)
        if not lcPos then  
            lcSubStrTab[#lcSubStrTab+1] =  str      
            break  
        end
        local lcSubStr  = string.sub(str,1,lcPos-1)  
        lcSubStrTab[#lcSubStrTab+1] = lcSubStr  
        str = string.sub(str,lcPos2+1,#str) 
    end  
    return lcSubStrTab  
end

-- 乱序一个数组
function g_LuaUtil:RandomShuffleArray(tArray)
	if(tArray == nil or type(tArray) ~= "table") then return; end
	
	if(#tArray <= 1.1) then return; end

	for i = #tArray, 2, -1 do
		local r = math.random(1, i - 1);
		
		local t = tArray[r];
		tArray[r] = tArray[i];
		tArray[i] = t;
	end
end
