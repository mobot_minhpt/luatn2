LuaHaoYiXingMgr = {}
LuaHaoYiXingMgr.m_PreviewWndPreviewCardID = 0
LuaHaoYiXingMgr.m_HaoYiXingCardsComposeNum = 0
LuaHaoYiXingMgr.m_CardsComposeResultList = {}
LuaHaoYiXingMgr.m_CardsComposeResultRewardsList = {}
LuaHaoYiXingMgr.m_FreeTimes = 0
LuaHaoYiXingMgr.m_HaitangCount = 0
LuaHaoYiXingMgr.m_HaitangPieceCount = 0
LuaHaoYiXingMgr.m_CurMainTaskIdx = 0
LuaHaoYiXingMgr.m_TotalScore = 0
LuaHaoYiXingMgr.m_ComposeHiddenCardTimes = 0
LuaHaoYiXingMgr.m_NeedSelectHiddenCardTab = false
LuaHaoYiXingMgr.m_canAcceptMainTask = false
LuaHaoYiXingMgr.m_CurrentDoingMainTaskIdx = 0
LuaHaoYiXingMgr.m_IsPlayOpen = false

function LuaHaoYiXingMgr:GetComposeHiddenCardTimes()
    if not self.m_IsPlayOpen then return end
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HaoYiXingCard_HaveChanceToExchange",self.m_ComposeHiddenCardTimes),DelegateFactory.Action(function()
        self.m_NeedSelectHiddenCardTab = true
        CUIManager.CloseUI(CLuaUIResources.HaoYiXingCardsWnd)
        CUIManager.ShowUI(CLuaUIResources.HaoYiXingCardsWnd)
    end),nil,LocalString.GetString("前往"),LocalString.GetString("取消"),false)
end

function Gas2Gac.SyncErHaTCGData(haitangCount, haitangPieceCount, curHuoLi, freeTimes, score, curMainTaskIdx, hiddenComposeCount, dataUD, canAcceptMainTask, currentDoingMainTaskIdx)
    -- 海棠花数量，海棠花花瓣数量，当前活力，免费次数，总积分，当前SpokesmanTCG_ErHaTCGScore表中的ID

    local list = MsgPackImpl.unpack(dataUD)
    local data = {}
    data.list = {}
    data.totalScore = score
    LuaHaoYiXingMgr.m_TotalScore = score
    LuaHaoYiXingMgr.m_ComposeHiddenCardTimes = hiddenComposeCount
    LuaHaoYiXingMgr.m_canAcceptMainTask = canAcceptMainTask
    LuaHaoYiXingMgr.m_CurrentDoingMainTaskIdx = currentDoingMainTaskIdx
    LuaHaoYiXingMgr.m_HaitangCount,LuaHaoYiXingMgr.m_HaitangPieceCount,LuaHaoYiXingMgr.m_FreeTimes,LuaHaoYiXingMgr.m_CurMainTaskIdx = haitangCount, haitangPieceCount, freeTimes,curMainTaskIdx
    local index2IdSet = {}
    SpokesmanTCG_ErHaTCG.ForeachKey(function (key)
        table.insert(data.list,{id = key,isNew = false,num = 0,pieceCount = 0,taskFinished = false, isCollected = true})
        local id = key
        index2IdSet[id] = #data.list
    end)
    for i = 0, list.Count - 1, 6 do
        local cardId = tonumber(list[i])
        local redundantCount = tonumber(list[i+1]) -- 多余数量
        local isNew = list[i+2]        -- 是否新获得
        local pieceCount = tonumber(list[i+3])     -- 碎片数
        local taskStatus = tonumber(list[i+4])   -- 0是未接任务  1是接了任务未完成  2是接了任务已完成
        local isCollected = list[i+5]   -- 是否已合成成功, 只有碎片型卡片有可能是false
        local index = index2IdSet[cardId]
        if index then
            local t = data.list[index]
            if t then 
                t.isNew = isNew
                t.pieceCount = pieceCount
                t.taskStatus = taskStatus
                t.isCollected = isCollected
                t.num = redundantCount
                t.owner = t.isCollected and true
            end
        end
    end
    g_ScriptEvent:BroadcastInLua("SyncErHaTCGData",data)
end

function Gas2Gac.SyncErHaTCGComposeResult(hiddenComposeCount, cardUD, itemUD)
    LuaHaoYiXingMgr.m_ComposeHiddenCardTimes = hiddenComposeCount
    local list = MsgPackImpl.unpack(cardUD)
    LuaHaoYiXingMgr.m_CardsComposeResultList = {}
    for i = 0, list.Count - 1, 2 do
        local cardId = list[i]
        local isNew = list[i+1]
        table.insert(LuaHaoYiXingMgr.m_CardsComposeResultList, {id = cardId, isnew = isNew})
    end

    LuaHaoYiXingMgr.m_CardsComposeResultRewardsList = {}
    local list2 = MsgPackImpl.unpack(itemUD)
    for i = 0, list2.Count - 1, 2 do
        local itemTemplateId = tonumber(list2[i])
        local count = tonumber(list2[i+1])
        table.insert(LuaHaoYiXingMgr.m_CardsComposeResultRewardsList, {id = itemTemplateId, count = count})
    end
    CUIManager.ShowUI(CLuaUIResources.HaoYiXingCardsComposeResultWnd)
    g_ScriptEvent:BroadcastInLua("OnSyncErHaTCGComposeResult")
    Gac2Gas.RequestErHaTCGData()
end

function Gas2Gac.ErHaTCGAcceptCardTaskSucc(cardId)
    g_ScriptEvent:BroadcastInLua("OnErHaTCGAcceptCardTaskSucc", tonumber(cardId))
end

function Gas2Gac.ErHaTCGAcceptScoreTaskSucc(newTaskIdx)
    Gac2Gas.RequestErHaTCGData()
end

function Gas2Gac.ErHaTCGBuyHaiTangSucc(newHaiTangCount)
    LuaHaoYiXingMgr.m_HaitangCount = newHaiTangCount
    g_ScriptEvent:BroadcastInLua("OnErHaTCGBuyHaiTangSucc")
end

function Gas2Gac.ErHaTCGShareDoneRet(hiddenComposeCount)
    LuaHaoYiXingMgr.m_ComposeHiddenCardTimes = hiddenComposeCount
    LuaHaoYiXingMgr:GetComposeHiddenCardTimes()
end
