local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
LuaToolBtnMgr = {}
LuaToolBtnMgr.BtnName = nil
LuaToolBtnMgr.callBack = nil
LuaToolBtnMgr.target = nil
-- 通用按钮，目前只支持显示在上方
function LuaToolBtnMgr:ShowToolBtn(BtnName,callBack,target)
	LuaToolBtnMgr.BtnName = BtnName
	LuaToolBtnMgr.callBack = callBack
	LuaToolBtnMgr.target = target
	CUIManager.ShowUI(CLuaUIResources.ToolBtnWnd)
end

LuaToolBtnWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaToolBtnWnd, "Btn", "Btn", CButton)
RegistChildComponent(LuaToolBtnWnd, "Label", "Label", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaToolBtnWnd, "targetSize")
RegistClassMember(LuaToolBtnWnd, "targetCenterPos")
RegistClassMember(LuaToolBtnWnd, "m_wnd")
function LuaToolBtnWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)
	self.m_wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    --@endregion EventBind end
end

function LuaToolBtnWnd:Init()
	self:GetPosAchor()
	self:AdjustPosition()
end
function LuaToolBtnWnd:Update()
	self.m_wnd:ClickThroughToClose()
end
function LuaToolBtnWnd:GetPosAchor()
	local target = LuaToolBtnMgr.target
	if not target then
		self.targetSize = Vector2(0,0)
		self.targetCenterPos = Vector3(0,0,0)
    else
		local b = NGUIMath.CalculateRelativeWidgetBounds(target)
		self.targetSize = Vector2(b.size.x, b.size.y)
		local pos = target.transform:TransformPoint(b.center)
		self.targetCenterPos = Vector3(pos.x, pos.y, pos.z)
	end
end

function LuaToolBtnWnd:AdjustPosition()
	if LuaToolBtnMgr.BtnName ~= nil then self.Label.text = LuaToolBtnMgr.BtnName end
	self.Btn.gameObject:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
	local localPos = self.Btn.transform:InverseTransformPoint(self.targetCenterPos)
	local selfBound = NGUIMath.CalculateRelativeWidgetBounds(self.Btn.transform)
	local pos = Vector3(localPos.x,localPos.y + self.targetSize.y * 0.5 + selfBound.size.y * 0.5,0)
	self.Btn.transform.localPosition = pos
end
function LuaToolBtnWnd:GetGuideGo(methodName)
    if methodName == "GetButton" then
        return self.Btn.gameObject
    end
end

--@region UIEvent

function LuaToolBtnWnd:OnBtnClick()
	if LuaToolBtnMgr.callBack then
		LuaToolBtnMgr.callBack()
	end
	CUIManager.CloseUI(CLuaUIResources.ToolBtnWnd)
end

--@endregion UIEvent
function LuaToolBtnWnd:OnDisable()
	LuaToolBtnMgr.BtnName = nil
	LuaToolBtnMgr.callBack = nil
	LuaToolBtnMgr.target = nil
end
