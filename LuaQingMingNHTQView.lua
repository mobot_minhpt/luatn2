local GameObject = import "UnityEngine.GameObject"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"

LuaQingMingNHTQView = class()

RegistChildComponent(LuaQingMingNHTQView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaQingMingNHTQView, "TaskInfo", "TaskInfo", GameObject)
RegistChildComponent(LuaQingMingNHTQView, "LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaQingMingNHTQView, "InviteBtn", "InviteBtn", GameObject)
RegistChildComponent(LuaQingMingNHTQView, "bg", "bg", GameObject)
RegistChildComponent(LuaQingMingNHTQView, "TaskDes1", "TaskDes1", UILabel)

function LuaQingMingNHTQView:Awake()
end

function LuaQingMingNHTQView:OnEnable()
	g_ScriptEvent:AddListener("UpdateQingMingCGYJMonsterInfo", self, "UpdateMonsterInfo")
end

function LuaQingMingNHTQView:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateQingMingCGYJMonsterInfo", self, "UpdateMonsterInfo")
end


function LuaQingMingNHTQView:Init()
  UIEventListener.Get(self.LeaveBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    CGamePlayMgr.Inst:LeavePlay()
  end)
  UIEventListener.Get(self.InviteBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    g_MessageMgr:ShowMessage('QingMingNHTQTip')
  end)
	LuaQingMing2021Mgr.MonsterStatus = nil
  self:InitPhaText()
end

function LuaQingMingNHTQView:UpdateMonsterInfo()
  if LuaQingMing2021Mgr.MonsterStatus then
    local pha1Text = g_MessageMgr:FormatMessage("QingMingNHTQText")
    self.TaskDes1.text = pha1Text
    for i,v in ipairs(LuaQingMing2021Mgr.MonsterStatus) do
      local monsterId = LuaQingMing2021Mgr.MonsterStatus[i][1]
      local monsterNum = LuaQingMing2021Mgr.MonsterStatus[i][2]
      local monsterMaxNum = LuaQingMing2021Mgr.MonsterStatus[i][3]
      local mData = Monster_Monster.GetData(monsterId)
      local str = mData.Name
      if monsterNum >= monsterMaxNum then
        str = '\n'.. str .. '[c][00FF00](' .. monsterNum .. '/' .. monsterMaxNum .. ')[-][c]'
      else
        str = '\n'..str .. '[c][FF0000](' .. monsterNum .. '/' .. monsterMaxNum .. ')[-][c]'
      end
      self.TaskDes1.text = self.TaskDes1.text .. str
    end
		--LuaQingMing2021Mgr.MonsterStatus = nil
  end
end

function LuaQingMingNHTQView:InitPhaText()
  local sceneName = CScene.MainScene.SceneName
  local pha1Text = g_MessageMgr:FormatMessage("QingMingNHTQText")

  local showText = '[c][FFC800]' .. sceneName .. '[-][c]'
  self.TaskName.text = showText

  self.TaskDes1.text = pha1Text
  self:UpdateMonsterInfo()
end

--@region UIEvent

--@endregion
