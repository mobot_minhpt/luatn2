local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local QnButton = import "L10.UI.QnButton"
local Transform = import "UnityEngine.Transform"
local UILabel = import "UILabel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"

LuaTurkeyMatchSignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTurkeyMatchSignWnd, "TopTeam", "TopTeam", GameObject)
RegistChildComponent(LuaTurkeyMatchSignWnd, "MemberGrid", "MemberGrid", UIGrid)
RegistChildComponent(LuaTurkeyMatchSignWnd, "RankBtn", "RankBtn", QnButton)
RegistChildComponent(LuaTurkeyMatchSignWnd, "TopRewards", "TopRewards", Transform)
RegistChildComponent(LuaTurkeyMatchSignWnd, "TimeLab", "TimeLab", UILabel)
RegistChildComponent(LuaTurkeyMatchSignWnd, "LimitLab", "LimitLab", UILabel)
RegistChildComponent(LuaTurkeyMatchSignWnd, "TypeLab", "TypeLab", UILabel)
RegistChildComponent(LuaTurkeyMatchSignWnd, "IntroLab", "IntroLab", UILabel)
RegistChildComponent(LuaTurkeyMatchSignWnd, "JoinLeftTimes", "JoinLeftTimes", UILabel)
RegistChildComponent(LuaTurkeyMatchSignWnd, "KillLeftTimes", "KillLeftTimes", UILabel)
RegistChildComponent(LuaTurkeyMatchSignWnd, "TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaTurkeyMatchSignWnd, "SignBtn", "SignBtn", QnButton)
RegistChildComponent(LuaTurkeyMatchSignWnd, "CancelBtn", "CancelBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaTurkeyMatchSignWnd, "m_Reward")
RegistClassMember(LuaTurkeyMatchSignWnd, "m_GameplayId")
RegistClassMember(LuaTurkeyMatchSignWnd, "m_RedColor")
RegistClassMember(LuaTurkeyMatchSignWnd, "m_GreenColor")

function LuaTurkeyMatchSignWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_Reward = self.transform:Find("Anchor/Right/GamePlayInfo/DailyReward/Rewards")
    self.m_GameplayId = Christmas2022_Setting.GetData().GamePlayId

    self.RankBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        CUIManager.ShowUI(CLuaUIResources.TurkeyMatchRankWnd)
    end)
    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        g_MessageMgr:ShowMessage("Christmas2022_TurkeyMatch_Rules")
    end)
    self.SignBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        Gac2Gas.GlobalMatch_RequestSignUp(self.m_GameplayId)
    end)
    self.CancelBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        Gac2Gas.GlobalMatch_RequestCancelSignUp(self.m_GameplayId)
    end)

    for i = 0, 2 do 
        local item      = self.TopRewards:GetChild(i)
        local btn       = item:GetComponent(typeof(QnButton))
        local itemId    = Christmas2022_Setting.GetData().AppearanceAwardItemId[i]

        btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
        end)
    end

    for i = 0, 2 do 
        local item      = self.m_Reward:GetChild(i)
        local icon      = item:Find("Icon"):GetComponent(typeof(CUITexture))
        local btn       = item:GetComponent(typeof(QnButton))
        local itemId    = Christmas2022_Setting.GetData().AwardItemId[i]

        local itemData = Item_Item.GetData(itemId)
        icon:LoadMaterial(itemData.Icon)
        btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
        end)
    end

    self.TimeLab.text = g_MessageMgr:FormatMessage("Christmas2022_TurkeyMatch_Time")
    self.LimitLab.text = SafeStringFormat3(LocalString.GetString("%d级"), Christmas2022_Setting.GetData().LevelLimit)
    self.TypeLab.text = Christmas2022_Setting.GetData().TaskPattern
    self.IntroLab.text = g_MessageMgr:FormatMessage("Christmas2022_TurkeyMatch_Desc")
    self.TopTeam:SetActive(false)
    self.SignBtn.gameObject:SetActive(true)
    self.CancelBtn.gameObject:SetActive(false)
    self.m_RedColor = NGUIText.ParseColor24("FF8073", 0)
    self.m_GreenColor = NGUIText.ParseColor24("6EFC71", 0)
end

function LuaTurkeyMatchSignWnd:Start()
    self:OnQueryPlayerTurkeyMatchInterfaceData_Result(0, 0, {})
    Gac2Gas.QueryPlayerTurkeyMatchInterfaceData()
    Gac2Gas.GlobalMatch_RequestCheckSignUp(self.m_GameplayId, CClientMainPlayer.Inst.Id)
end

function LuaTurkeyMatchSignWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryPlayerTurkeyMatchInterfaceData_Result", self, "OnQueryPlayerTurkeyMatchInterfaceData_Result")
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaTurkeyMatchSignWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryPlayerTurkeyMatchInterfaceData_Result", self, "OnQueryPlayerTurkeyMatchInterfaceData_Result")
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end
--@region UIEvent

--@endregion UIEvent

function LuaTurkeyMatchSignWnd:RefreshState(isMatching)
    self.SignBtn.gameObject:SetActive(not isMatching)
    self.CancelBtn.gameObject:SetActive(isMatching)
end

function LuaTurkeyMatchSignWnd:OnQueryPlayerTurkeyMatchInterfaceData_Result(dailyAwardTimes, firstKillAwardTimes, firstTeamInfos)
    local joinLeftTimes = Christmas2022_Setting.GetData().TurkeyMatchDailyAwardLimit - dailyAwardTimes
    local killLeftTimes = Christmas2022_Setting.GetData().TurkeyMatchFirstkillAwardLimit - firstKillAwardTimes

    self.JoinLeftTimes.text = SafeStringFormat3(LocalString.GetString("剩%d次"), joinLeftTimes)
    self.KillLeftTimes.text = SafeStringFormat3(LocalString.GetString("剩%d次"), killLeftTimes)
    
    self.JoinLeftTimes.color  = joinLeftTimes > 0 and self.m_GreenColor or self.m_RedColor
    self.KillLeftTimes.color  = killLeftTimes > 0 and self.m_GreenColor or self.m_RedColor

    if firstTeamInfos then
        self:UpdateFirstTeamInfo(firstTeamInfos)
    end
end

function LuaTurkeyMatchSignWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if (CClientMainPlayer.Inst or {}).Id == playerId and playId == self.m_GameplayId then
        self:RefreshState(isInMatching)
    end
end

function LuaTurkeyMatchSignWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == self.m_GameplayId then
        self:RefreshState(success)
    end
end

function LuaTurkeyMatchSignWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId == self.m_GameplayId then
       self:RefreshState(not success)
    end
end

function LuaTurkeyMatchSignWnd:UpdateFirstTeamInfo(firstTeamInfos)
    self.TopTeam:SetActive(#firstTeamInfos > 0)
    for i = 0, 2 do 
        local info      = firstTeamInfos[i+1]
        local item      = self.MemberGrid.transform:GetChild(i)
        item.gameObject:SetActive(info)
        if info then
            local icon      = item:GetComponent(typeof(CUITexture))
            local nameLab   = item:Find("PlayerName"):GetComponent(typeof(UILabel))
            local btn       = item:GetComponent(typeof(QnButton))
            local class     = info.class
            local gender    = info.gender
            local playerId  = info.playerId
            local name      = info.name
            local portraitName = CUICommonDef.GetPortraitName(class, gender, -1)
            icon:LoadNPCPortrait(portraitName,false)
            nameLab.text = name
            nameLab.color = playerId == CClientMainPlayer.Inst.Id and LuaColorUtils.GreenColor or LuaColorUtils.WhiteColor
            btn.OnClick = DelegateFactory.Action_QnButton(function(btn)
                CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, item.position, AlignType.Right)
            end)
        end
    end
end