local CXianjiaUpgradeDetailView=import "L10.UI.CXianjiaUpgradeDetailView"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local EnumTalismanTabWnd=import "L10.UI.EnumTalismanTabWnd"
CLuaXianjiaUpgradeWindow = class()
RegistClassMember(CLuaXianjiaUpgradeWindow,"itemList")
RegistClassMember(CLuaXianjiaUpgradeWindow,"detailView")

function CLuaXianjiaUpgradeWindow:Awake()
    local script = self.transform:Find("TaslimanList"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.itemList = script.m_LuaSelf
    self.detailView = self.transform:Find("UpgradeDetailView"):GetComponent(typeof(CXianjiaUpgradeDetailView))
end


function CLuaXianjiaUpgradeWindow:Init()
    self.itemList.OnTalismanSelect = function(info)
        self.detailView:OnXianjiaSelect(info)
   end
   self.itemList:Init(EnumTalismanTabWnd.upgrade)
end