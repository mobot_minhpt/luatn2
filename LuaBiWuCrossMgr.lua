local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CPos = import "L10.Engine.CPos"
local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"

LuaBiWuCrossMgr = {}

LuaBiWuCrossMgr.TeamInfo = nil
LuaBiWuCrossMgr.AllTeamInfos = nil
LuaBiWuCrossMgr.BattleInfo = nil --对战信息
LuaBiWuCrossMgr.TeamSelectIndex = 0
LuaBiWuCrossMgr.TeamSelectType = 0
LuaBiWuCrossMgr.TeamSelectLeftTime = 0
LuaBiWuCrossMgr.Mapper = {
    {0, 0},
    {0, 0},
    {0, 2},
    {0, 1},
    {0, 4},
    {0, 3},
    {0, 0},
    {0, 0},
    {5, 8},
    {6, 7},
    {9, 10}
}

function LuaBiWuCrossMgr:OnRenderSceneInit()
    if CScene.MainScene and CScene.MainScene.SceneTemplateId ~= 51102980 then
        LuaBiWuCrossMgr.ClearEfts(false)
        LuaBiWuCrossMgr.RemoveListener()
    end
end

function LuaBiWuCrossMgr:OnGasDisconnect()
    LuaBiWuCrossMgr.ClearEfts(false)
    LuaBiWuCrossMgr.RemoveListener()
end

function LuaBiWuCrossMgr.RemoveListener()
    g_ScriptEvent:RemoveListener("RenderSceneInit", LuaBiWuCrossMgr, "OnRenderSceneInit")
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaBiWuCrossMgr, "OnGasDisconnect")
end

function LuaBiWuCrossMgr.AddListener()
    LuaBiWuCrossMgr.RemoveListener()
    g_ScriptEvent:AddListener("RenderSceneInit", LuaBiWuCrossMgr, "OnRenderSceneInit")
    g_ScriptEvent:AddListener("GasDisconnect", LuaBiWuCrossMgr, "OnGasDisconnect")
end

LuaBiWuCrossMgr.Efts = nil
function LuaBiWuCrossMgr.ProcessShowEffect(ret_U, isstart)
    local ret = g_MessagePack.unpack(ret_U)
    for i = 1, #ret do
        local retdata = ret[i]
        local x1 = retdata[1]
        local y1 = retdata[2]
        local x2 = retdata[3]
        local y2 = retdata[4]
        LuaBiWuCrossMgr.ShowEft(x1, y1, x2, y2, isstart)
    end
end
LuaBiWuCrossMgr.Eftids = {
    {88804231, 88804233},
    {88804232, 88804234}
}
function LuaBiWuCrossMgr.ShowEft(x1, y1, x2, y2, isstart)
    if LuaBiWuCrossMgr.Efts then
        for i = 1, #LuaBiWuCrossMgr.Efts do
            local eftdata = LuaBiWuCrossMgr.Efts[i]
            if eftdata[2] == x1 and eftdata[3] == y1 and eftdata[4] == x2 and eftdata[5] == y2 and eftdata[6] == isstart then
                return --已经存在特效
            end
        end
    end

    if isstart then
        LuaBiWuCrossMgr.ShowEft(x1, y1, x2, y2, false)
    end

    local areaeftid = 0
    local pos1 = Utility.GridPos2WorldPos(x1, y1)
    local pos2 = Utility.GridPos2WorldPos(x2, y2)

    local absx = math.abs(x1 - x2)
    local absy = math.abs(y1 - y2)
    local dir = 0
    local eftids = LuaBiWuCrossMgr.Eftids
    if absy < absx then
        dir = 0
        if absy > 2 then
            areaeftid = isstart and eftids[2][1] or eftids[2][2]
        else
            areaeftid = isstart and eftids[1][1] or eftids[1][2]
        end
    else
        dir = 90
        if absx > 2 then
            areaeftid = isstart and eftids[2][1] or eftids[2][2]
        else
            areaeftid = isstart and eftids[1][1] or eftids[1][2]
        end
    end

    local pos = CommonDefs.op_Multiply_Vector3_Single(CommonDefs.op_Addition_Vector3_Vector3(pos1, pos2), 0.5)

    local fx = CEffectMgr.Inst:AddWorldPositionFX(areaeftid, pos, dir, 0, 1.0, -1, EnumWarnFXType.None, nil, 0, 0, nil)
    if not LuaBiWuCrossMgr.Efts then
        LuaBiWuCrossMgr.Efts = {}
        LuaBiWuCrossMgr.AddListener()
    end
    table.insert(LuaBiWuCrossMgr.Efts, {fx, x1, y1, x2, y2, isstart})
end

function LuaBiWuCrossMgr.ClearEfts(bDestroy)
    if bDestroy and LuaBiWuCrossMgr.Efts then
        for i = 1, #LuaBiWuCrossMgr.Efts do
            local fx = LuaBiWuCrossMgr.Efts[i][1]
            fx:Destroy()
        end
    end

    LuaBiWuCrossMgr.Efts = nil
end

function LuaBiWuCrossMgr.ProcessOpenHeroSelectWnd(memList_U, startTime, leftTime)
    local ret = g_MessagePack.unpack(memList_U)
    LuaBiWuCrossMgr.TeamInfo = {}
    for i = 1, #ret do
        local retdata = ret[i]
        local memdata = {}
        memdata.MemberId = retdata[1]
        memdata.Name = retdata[2]
        memdata.Job = retdata[3]
        memdata.Gender = retdata[4]
        memdata.Grade = retdata[5]
        memdata.XianFanStatus = retdata[6] or 0
        memdata.XiuWeiGrade = retdata[7] or 0
        memdata.XiuLianGrade = retdata[8] or 0
        memdata.ZhuangPing = retdata[9] or 0
        table.insert(LuaBiWuCrossMgr.TeamInfo, memdata)
    end

    LuaBiWuCrossMgr.TeamSelectLeftTime = leftTime
    LuaBiWuCrossMgr.TeamSelectIndex = 0
    LuaBiWuCrossMgr.TeamSelectType = 1
    CUIManager.ShowUI(CLuaUIResources.BiWuCrossTeamWnd)
end

function LuaBiWuCrossMgr.ProcessBiWuCrossGroupTeamInfoResult(ret_U)
    local ret = g_MessagePack.unpack(ret_U)
    LuaBiWuCrossMgr.AllTeamInfos = {}
    for i = 1, #ret do
        local retdata = ret[i]
        local data = {}
        data.TeamIndex = retdata[1]
        data.TeamName = LocalString.TranslateAndFormatText(retdata[2])
        data.ServerName = retdata[3]
        data.Rank = retdata[4]
        data.ZhuangPing = retdata[5]
        table.insert(LuaBiWuCrossMgr.AllTeamInfos, data)
    end
    g_ScriptEvent:BroadcastInLua("OnGetBiWuCrossTeamsInfo")
end

--[[
    @desc: 处理RPC得到的赛程数据
    author:Codee
    time:2022-09-02 10:04:29
    --@ret_U: 
    @return:
]]
function LuaBiWuCrossMgr.ProcessBiWuCrossMatchInfoResult(ret_U)
    local ret = g_MessagePack.unpack(ret_U)
    LuaBiWuCrossMgr.BattleInfo = {}
    for i = 1, 11 do
        local data = {}
        if i <= #ret then
            local retdata = ret[i]
            data.Matchindex = retdata[1]
            data.Round = retdata[2]
            data.Index = retdata[3]
            data.TeamIndex1 = retdata[4]
            data.TeamIndex2 = retdata[5]
            data.TeamName1 = LocalString.TranslateAndFormatText(retdata[6])
            data.TeamName2 = LocalString.TranslateAndFormatText(retdata[7])
            data.WinTeamIndex = retdata[8]
            data.CanWatch = retdata[9] or false
        else
            data.Matchindex = 0
            data.Round = 0
            data.Index = 0
            data.TeamIndex1 = -1
            data.TeamIndex2 = -1
            data.TeamName1 = ""
            data.TeamName2 = ""
            data.WinTeamIndex = 0
            data.CanWatch = false
        end

        local fmt = LocalString.GetString("第%s场胜者")
        local empty = LocalString.GetString("轮空")

        if data.TeamIndex1 <= 0 then --要么是轮空，要么是等待晋级
            local cindex = LuaBiWuCrossMgr.Mapper[i][1]
            if cindex > 0 then --等待晋级
                local cdata = LuaBiWuCrossMgr.BattleInfo[cindex]
                if cdata.WinTeamIndex > 0 then --有晋级数据
                    data.TeamIndex1 = cdata.WinTeamIndex
                    data.TeamName1 = cdata.WinTeamIndex == cdata.TeamIndex1 and cdata.TeamName1 or cdata.TeamName2
                else
                    data.TeamName1 = SafeStringFormat3(fmt, cindex)
                end
            else
                data.TeamName1 = empty
            end
        end

        if data.TeamIndex2 <= 0 then --要么是轮空，要么是等待晋级
            local cindex = LuaBiWuCrossMgr.Mapper[i][2]
            if cindex > 0 then --等待晋级
                local cdata = LuaBiWuCrossMgr.BattleInfo[cindex]
                if cdata.WinTeamIndex > 0 then --有晋级数据
                    data.TeamIndex2 = cdata.WinTeamIndex
                    data.TeamName2 = cdata.WinTeamIndex == cdata.TeamIndex1 and cdata.TeamName1 or cdata.TeamName2
                else
                    data.TeamName2 = SafeStringFormat3(fmt, cindex)
                end
            else
                data.TeamName2 = empty
            end
        end

        table.insert(LuaBiWuCrossMgr.BattleInfo, data)
    end

    g_ScriptEvent:BroadcastInLua("OnGetBiWuCrossMatchInfo")
end

function LuaBiWuCrossMgr.ProcessBiWuCrossTeamInfoResult(ret_U)
    local ret = g_MessagePack.unpack(ret_U)

    LuaBiWuCrossMgr.TeamInfo = {}
    for i = 1, #ret do
        local retdata = ret[i]
        local memdata = {}
        memdata.MemberId = retdata[1]
        memdata.Name = retdata[2]
        memdata.Job = retdata[3]
        memdata.Gender = retdata[4]
        memdata.Grade = retdata[5]
        memdata.XiuWeiGrade = retdata[6] or 0
        memdata.XiuLianGrade = retdata[7] or 0
        memdata.XianFanStatus = retdata[8] or 0
        memdata.ZhuangPing = retdata[9] or 0
        table.insert(LuaBiWuCrossMgr.TeamInfo, memdata)
    end
    CUIManager.ShowUI(CLuaUIResources.BiWuCrossTeamWnd)
end

function LuaBiWuCrossMgr.ShowInfoWnd()
    CUIManager.ShowUI(CLuaUIResources.BiWuCrossInfoWnd)
end

function LuaBiWuCrossMgr.ShowWatchWnd()
    CUIManager.ShowUI(CLuaUIResources.BiWuCrossWatchWnd)
end

--[[
    @desc: 打开队伍信息界面
    author:Codee
    time:2022-09-02 09:54:16
    --@type: 0-队伍信息 1-英雄选择
	--@teamindex: 
    @return:
]]
function LuaBiWuCrossMgr.ShowTeamWnd(type, teamindex)
    LuaBiWuCrossMgr.TeamSelectIndex = teamindex
    LuaBiWuCrossMgr.TeamSelectType = type
    LuaBiWuCrossMgr.ReqTeamInfo(LuaBiWuCrossMgr.TeamSelectIndex)
end

function LuaBiWuCrossMgr.ReqTeamInfo(teamIndex)
    Gac2Gas.QueryBiWuCrossTeamInfo(teamIndex)
end

--[[
    @desc: 请求赛程数据 
    author:Codee
    time:2022-09-02 09:49:55
    --@group: 组别
    @return:
]]
function LuaBiWuCrossMgr.ReqInfoData(group)
    Gac2Gas.QueryBiWuCrossMatchInfo(group)
end

function LuaBiWuCrossMgr.ReqGroupTeams(group)
    Gac2Gas.QueryBiWuCrossGroupTeamInfo(group)
end

--[[
    @desc: 请求观战跨服比武
    author:Codee
    time:2022-09-02 09:50:51
    --@matchIndex: 对战索引
    @return:
]]
function LuaBiWuCrossMgr.ReqEnterWatch(matchIndex)
    Gac2Gas.RequestWatchBiWuCross(matchIndex)
end

--[[
    @desc: 请求进入比赛
    author:Codee
    time:2022-08-03 10:34:24
    @return:
]]
function LuaBiWuCrossMgr.ReqEnterBattle()
    Gac2Gas.RequestEnterBiWuCrossPreapareScene()
end

--[[
    @desc: 队长选定英雄
    author:Codee
    time:2022-08-03 10:35:03
    @return:
]]
function LuaBiWuCrossMgr.ReqSelectAsHero(heroid)
    Gac2Gas.RequestSetBiWuCrossHero(heroid)
end
