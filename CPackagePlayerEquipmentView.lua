-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPackagePlayerEquipmentView = import "L10.UI.CPackagePlayerEquipmentView"
local EquipIntensify_Suit = import "L10.Game.EquipIntensify_Suit"
local NGUIText = import "NGUIText"
local UIEventListener = import "UIEventListener"
local CUITexture = import "L10.UI.CUITexture"
local Animation = import "UnityEngine.Animation"
local CClientObject = import "L10.Game.CClientObject"

LuaPackagePlayerEquipmentView = {}
LuaPackagePlayerEquipmentView.m_csthis = nil -- CS对象
-- 显示强化图标
function LuaPackagePlayerEquipmentView:UpdateIntensify(this)
    local intesifySuitId = CClientMainPlayer.Inst.ItemProp.IntesifySuitId
    local intesifySuitFxIndex = CClientMainPlayer.Inst.ItemProp.IntesifySuitFxIndex
    if intesifySuitId > 0 then
        local find = EquipIntensify_Suit.GetData(intesifySuitId)
        this.intensifyValueLabel.text = tostring(find.IntensifyLevel)
        if System.String.IsNullOrEmpty(find.SuitTextColor) then
            this.intensifyValueLabel.color = Color.white
        else
            this.intensifyValueLabel.color = NGUIText.ParseColor24(find.SuitTextColor, 0)
        end
    end

    local additiontex = this.additionBtn:GetComponent(typeof(CUITexture))
    local tex = this.intensifyTexture
    if tex == nil then
        return
    end
    additiontex:LoadMaterial("")
    tex.texture.enabled = true
    if intesifySuitId == 9 then
        tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_01.mat")
    elseif intesifySuitId == 10 then
        tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_02.mat")
    elseif intesifySuitId == 11 then -- 升龙套
        tex.texture.enabled = false
        if intesifySuitFxIndex == 0 then
            additiontex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_03.mat")
        else
            additiontex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition_04.mat")
        end
    else
        tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_equipmentaddition.mat")
    end
end
-- 显示锻造图标
function LuaPackagePlayerEquipmentView:UpdateForge(this)
    local intesifySuitId = CClientMainPlayer.Inst.ItemProp.IntesifySuitId
    local intesifySuitFxIndex = CClientMainPlayer.Inst.ItemProp.IntesifySuitFxIndex
    local forgeSuitId = CClientMainPlayer.Inst.ItemProp.ExtraEquipForgeSuitId
    local find = ExtraEquip_Suit.GetData(forgeSuitId)
    if forgeSuitId > 0 then
        this.forgeValueLabel.text = tostring(find.ForgeLevel)
        if System.String.IsNullOrEmpty(find.SuitTextColor) then
            this.forgeValueLabel.color = Color.white
        else
            this.forgeValueLabel.color = NGUIText.ParseColor24(find.SuitTextColor, 0)
        end
    end

    local tex = this.forgeTexture
    if tex == nil then
        return
    end
    tex.texture.enabled = true
    if intesifySuitId == 11 then -- 升龙套
        tex.texture.enabled = false
    else
        if not find or System.String.IsNullOrEmpty(find.SuitIcon) then
            tex:LoadMaterial("UI/Texture/Transparent/Material/packagewnd_icon_duanzao_01.mat")
        else
            tex:LoadMaterial(find.SuitIcon)
        end
    end
end

CPackagePlayerEquipmentView.m_UpdateIntensifySuitLevel_CS2LuaHook = function (this) 
    --添加按钮
    local switchButton = this.transform:Find("SwitchButton").gameObject
    UIEventListener.Get(switchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if not IsAttrGroupOpen() then
            g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
            return
        end
		local basicProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp
		local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
		if not basicProp or not playProp then
			return
		end
		if basicProp.XiuWeiGrade < AttrGroup_Group.GetData(1).XiuWeiGrade and playProp.AttrGroupUnlock == 0 then
            g_MessageMgr:ShowMessage("SXQH_Fail_NotEnough_XiuWei")
        else
            CUIManager.ShowUI(CLuaUIResources.PropertySwitchWnd)
        end
    end)

    local m_Animation = this.additionBtn:GetComponent(typeof(Animation))
    local m_Forge = this.additionBtn.transform:Find("Forge").gameObject
    local m_Intensify = this.additionBtn.transform:Find("Inensify").gameObject
    local m_Mark = this.additionBtn.transform:Find("Mark_ani").gameObject
    if CLuaEquipmentSuitInfoMgr:IsEquipmentNewSuitEnable() then
        m_Animation.enabled = true
        this.intensifyValueLabel.text = ""
        this.forgeValueLabel.text = ""
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp ~= nil then
            LuaPackagePlayerEquipmentView:UpdateIntensify(this)
            LuaPackagePlayerEquipmentView:UpdateForge(this)
        end
    else
        m_Animation.enabled = false
        m_Forge:SetActive(false)
        m_Intensify:SetActive(true)
        m_Mark:SetActive(false)
        local m_Label = m_Intensify.transform:Find("Label")
        local vec = m_Label.localPosition
        m_Label.localPosition = Vector3(this.intensifyValueLabel.transform.localPosition.x, vec.y, vec.z)
        this.intensifyValueLabel.text = ""
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp ~= nil then
            LuaPackagePlayerEquipmentView:UpdateIntensify(this)
        end
    end
end

CPackagePlayerEquipmentView.m_hookOnEnable = function(this)
    this:Init()
    EventManager.AddListenerInternal(EnumEventType.EquipmentSuitUpdate, MakeDelegateFromCSFunction(this.OnEquipmentSuitUpdate, MakeGenericClass(Action1, CClientObject), this))
    LuaPackagePlayerEquipmentView:OnEnable(this)
end

CPackagePlayerEquipmentView.m_hookOnDisable = function(this)
    EventManager.RemoveListenerInternal(EnumEventType.EquipmentSuitUpdate, MakeDelegateFromCSFunction(this.OnEquipmentSuitUpdate, MakeGenericClass(Action1, CClientObject), this))
    LuaPackagePlayerEquipmentView:OnDisable(this)
end

function LuaPackagePlayerEquipmentView:OnExtraEquipForgeSuitUpdate(obj)
    if not self.m_csthis then return end
    self.m_csthis:OnEquipmentSuitUpdate(obj)
end

function LuaPackagePlayerEquipmentView:OnEnable(this)
    self.m_csthis = this
    g_ScriptEvent:AddListener("ExtraEquipForgeSuitUpdate", self, "OnExtraEquipForgeSuitUpdate")
end

function LuaPackagePlayerEquipmentView:OnDisable(this)
    self.m_csthis = nil
    g_ScriptEvent:RemoveListener("ExtraEquipForgeSuitUpdate", self, "OnExtraEquipForgeSuitUpdate")
end