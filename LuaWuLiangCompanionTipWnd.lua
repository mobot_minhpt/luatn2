local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaWuLiangCompanionTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaWuLiangCompanionTipWnd, "TitleLabel", "TitleLabel", GameObject)
RegistChildComponent(LuaWuLiangCompanionTipWnd, "SkillTable", "SkillTable", GameObject)
RegistChildComponent(LuaWuLiangCompanionTipWnd, "SkillCountLabel", "SkillCountLabel", GameObject)
RegistChildComponent(LuaWuLiangCompanionTipWnd, "AttributeCountLabel", "AttributeCountLabel", GameObject)
RegistChildComponent(LuaWuLiangCompanionTipWnd, "AttributeTable", "AttributeTable", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangCompanionTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaWuLiangCompanionTipWnd:Init()
    -- { MonsterId, Id, Heal, Dps, Hp, UnderDamage, Level, Tid, InheritLevel, Attr, SkillLevelTbl}
    local data = LuaWuLiangMgr.huobanTipData

    local skillList = {}
    -- 技能培养等级
    local sumLevel = 0
    if data.SkillLevelTbl then
        for id, level in pairs(data.SkillLevelTbl) do
            local skill = {}
            skill.id = id
            skill.level = level
            sumLevel  = level + sumLevel
            table.insert(skillList, skill)
        end
    end
    self.SkillCountLabel:GetComponent(typeof(UILabel)).text = tostring(sumLevel)

    local inheritLevel = data.InheritLevel or 0
    local levelData = XinBaiLianDong_WuLiangInherit.GetData(inheritLevel)

    local huoBanDesData = XinBaiLianDong_WuLiangHuoBan.GetData(data.MonsterId)

    self.TitleLabel:GetComponent(typeof(UILabel)).text = huoBanDesData.Name
	-- 加成比例
    self.AttributeCountLabel:GetComponent(typeof(UILabel)).text = tostring(math.floor(levelData.PropertyCoefficient*100+huoBanDesData.PropertyCoefficient*100 + 0.01).."%")
    
    -- 显示属性
    --LuaWuLiangMgr:ShowAttr(self.AttributeTable, data.InheritLevel, data.MonsterId)
    LuaWuLiangMgr:ShowAttrNew(self.AttributeTable, data.MonsterId, data.Attr)

    -- 显示技能信息
    LuaWuLiangMgr:ShowSkill(self.SkillTable, data.MonsterId, data.SkillLevelTbl, nil, LuaWuLiangMgr.notFeishengCheck)
end

function LuaWuLiangCompanionTipWnd:OnDisable()
    LuaWuLiangMgr.notFeishengCheck = false
end
--@region UIEvent

--@endregion UIEvent

