local CTooltip          = import "L10.UI.CTooltip"
local CCurrentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UIRoot            = import "UIRoot"

LuaOwnMoneyListWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaOwnMoneyListWnd, "template")
RegistClassMember(LuaOwnMoneyListWnd, "grid")
RegistClassMember(LuaOwnMoneyListWnd, "bg")

RegistClassMember(LuaOwnMoneyListWnd, "skillPrivateSilverTipButton")

function LuaOwnMoneyListWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self.template = self.transform:Find("Bg/Template").gameObject
    self.template:SetActive(false)
    self.grid = self.transform:Find("Bg/Grid"):GetComponent(typeof(UIGrid))
    self.bg = self.transform:Find("Bg"):GetComponent(typeof(UIBasicSprite))
end

function LuaOwnMoneyListWnd:Init()
    local info = LuaOwnMoneyListMgr.info
    Extensions.RemoveAllChildren(self.grid.transform)
    for _, moneyType in ipairs(info.moneyTypeList) do
        local child = NGUITools.AddChild(self.grid.gameObject, self.template)
        child:SetActive(true)

        child.transform:GetComponent(typeof(CCurrentMoneyCtrl)):SetType(moneyType, 0, false)
        local tipButton = child.transform:Find("TipButton").gameObject
        if moneyType == EnumMoneyType_lua.SkillPrivateSilver then
            self.skillPrivateSilverTipButton = tipButton
            tipButton:SetActive(true)
            UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function (go)
                g_MessageMgr:ShowMessage("SKILL_PRIVATE_SILVER_TIP")
            end)
        else
            tipButton:SetActive(false)
        end
    end
    self.grid:Reposition()

    local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.grid.transform)
    self.bg.height = bounds.size.y + 40
    self:Layout()
end

function LuaOwnMoneyListWnd:Layout()
    local info = LuaOwnMoneyListMgr.info
    if not info.targetGo then return end

    local bounds = NGUIMath.CalculateRelativeWidgetBounds(info.targetGo.transform)
    local localTargetPos = self.bg.transform.parent:InverseTransformPoint(info.targetGo.transform.position)
    local x = 0
    local y = 0
    if info.alignType == CTooltip.AlignType.Top then --需要拓展
        x = localTargetPos.x
        y = localTargetPos.y + bounds.size.y / 2 + self.bg.height / 2
    end
    local scale = UIRoot.GetPixelSizeAdjustment(self.bg.gameObject)
    local mainScreenSize = CUIManager.MainScreenSize
    local virtualScreenWidth = mainScreenSize.x * scale
    local virtualScreenHeight = mainScreenSize.y * scale
    if UIRoot.EnableIPhoneXAdaptation then
        virtualScreenWidth = virtualScreenWidth - UIRoot.IPhoneXWidthMargin * 2
    end

    --左右不超出屏幕范围
    if x < (- virtualScreenWidth * 0.5 + self.bg.width * 0.5) then
        x = - virtualScreenWidth * 0.5 + self.bg.width * 0.5 + 25
    end
    if x > (virtualScreenWidth * 0.5 - self.bg.width * 0.5) then
        x = virtualScreenWidth * 0.5 - self.bg.width * 0.5 - 25
    end
    --上下不超出屏幕范围
    if y < (- virtualScreenHeight * 0.5 + self.bg.height * 0.5) then
        y = - virtualScreenHeight * 0.5 + self.bg.height * 0.5 + 25
    end
    if y > (virtualScreenHeight * 0.5 - self.bg.height * 0.5) then
        y = virtualScreenHeight * 0.5 - self.bg.height * 0.5 - 25
    end

    LuaUtils.SetLocalPosition(self.bg.transform, x, y, 0)
end

function LuaOwnMoneyListWnd:OnDestroy()
    if LuaOwnMoneyListMgr.info.closeCallback then
        LuaOwnMoneyListMgr.info.closeCallback()
    end
end

function LuaOwnMoneyListWnd:GetGuideGo(methodName)
    if methodName == "GetSkillPrivateSilverTipButton" then
        return self.skillPrivateSilverTipButton
    end
end

--@region UIEvent

--@endregion UIEvent


LuaOwnMoneyListMgr = {}

function LuaOwnMoneyListMgr:ShowOwnMoneyListWnd(moneyTypeList, targetGo, alignType, closeCallback)
    self.info = {}
    self.info.moneyTypeList = moneyTypeList
    self.info.targetGo = targetGo
    self.info.alignType = alignType
    self.info.closeCallback = closeCallback
    CUIManager.ShowUI(CLuaUIResources.OwnMoneyListWnd)
end

