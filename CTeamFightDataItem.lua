-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CTeamFightDataItem = import "L10.UI.CTeamFightDataItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Profession = import "L10.Game.Profession"

CTeamFightDataItem.m_UpdateItem_CS2LuaHook = function (this, data) 
    this.m_RankLabel.text = tostring(data.m_Rank)
    this.m_ProfessionSprite.spriteName = Profession.GetIcon(data.m_Class)
    this.m_NameLabel.text = data.m_Name

    this.m_DamageLabel.text = CUICommonDef.ConvertNumberOverMillion(data.m_Damage)
    this.m_TimeLabel.text = System.String.Format("{0}:{1}:{2}", math.floor(data.m_Time / 3600), math.floor(data.m_Time % 3600 / 60), data.m_Time % 60)
    this.m_PercentageLabel.text = System.String.Format("{0:0.0}%", data.m_Percentage * 100)
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == data.m_Id then
        this.m_PercentageLabel.color = Color.green this.m_TimeLabel.color = this.m_PercentageLabel.color this.m_DamageLabel.color = this.m_TimeLabel.color this.m_NameLabel.color = this.m_DamageLabel.color this.m_RankLabel.color = this.m_NameLabel.color
    else
        this.m_PercentageLabel.color = Color.white this.m_TimeLabel.color = this.m_PercentageLabel.color this.m_DamageLabel.color = this.m_TimeLabel.color this.m_NameLabel.color = this.m_DamageLabel.color this.m_RankLabel.color = this.m_NameLabel.color
    end
end
