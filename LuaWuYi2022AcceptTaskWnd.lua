local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"

LuaWuYi2022AcceptTaskWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "FinishedLabel", "FinishedLabel", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "Template5", "Template5", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "Template4", "Template4", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "Template1", "Template1", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "Template2", "Template2", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "Template3", "Template3", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "TopView", "TopView", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "BottomView", "BottomView", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "DesLabel1", "DesLabel1", UILabel)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "DesLabel2", "DesLabel2", UILabel)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "TaskLabel1", "TaskLabel1", UILabel)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "TaskLabel2", "TaskLabel2", UILabel)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "TaskLabel3", "TaskLabel3", UILabel)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "CloseTopViewButton", "CloseTopViewButton", GameObject)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "ButtonFx", "ButtonFx", CUIFx)
RegistChildComponent(LuaWuYi2022AcceptTaskWnd, "TopSprite", "TopSprite", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2022AcceptTaskWnd, "m_TemplateList")
RegistClassMember(LuaWuYi2022AcceptTaskWnd, "m_TaskLabelList")
RegistClassMember(LuaWuYi2022AcceptTaskWnd, "m_TaskDesData")
RegistClassMember(LuaWuYi2022AcceptTaskWnd, "m_Index")
function LuaWuYi2022AcceptTaskWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


	
	UIEventListener.Get(self.CloseTopViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseTopViewButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2022AcceptTaskWnd:Init()
	self.TopView.gameObject:SetActive(false)
	self.m_TemplateList = { self.Template1, self.Template2, self.Template3, self.Template4, self.Template5}
	self.m_TaskLabelList = { self.TaskLabel1, self.TaskLabel2, self.TaskLabel3}
	self.m_TaskDesData = WuYi_Setting.GetData().ManYouSanJieTaskDes
	self:OnQueryWuYiManYouSanJieTaskInfoResult()
	self.ButtonFx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
	Gac2Gas.QueryWuYiManYouSanJieTaskInfo()
	CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.WuYi2022AcceptTaskWnd)
end

function LuaWuYi2022AcceptTaskWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryWuYiManYouSanJieTaskInfoResult", self, "OnQueryWuYiManYouSanJieTaskInfoResult")
end

function LuaWuYi2022AcceptTaskWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryWuYiManYouSanJieTaskInfoResult", self, "OnQueryWuYiManYouSanJieTaskInfoResult")
end

function LuaWuYi2022AcceptTaskWnd:OnQueryWuYiManYouSanJieTaskInfoResult()
	self.Fx:LoadFx("fx/ui/prefab/UI_sanjiemanyou_zuobiao.prefab")
	for i = 1,#self.m_TemplateList do
		local j = i
		local btn = self.m_TemplateList[i]
		local data = self.m_TaskDesData[i - 1]
		self:InitTemplate(btn, j, data)
	end
	local fxIndex = 0
	local tempFinished = false
	for i = 1,#self.m_TemplateList do
		local taskIdList = WuYi_Setting.GetData().ManYouSanJieTask[i - 1]
		local taskId = tonumber(taskIdList[2])
		local stateInfo = LuaWuYi2022Mgr.m_YouSanJieTaskStateList[taskId]
		local btn = self.m_TemplateList[i]
		if stateInfo then
			if fxIndex == 0 or tempFinished then
				fxIndex = i
				self.Fx.transform.position = btn.transform:Find("Fx").position
			end
		end
		tempFinished = stateInfo and stateInfo.bFinished
	end
	self.Fx.gameObject:SetActive(fxIndex > 0)
end

function LuaWuYi2022AcceptTaskWnd:InitTemplate(obj, index, data)
	local title, des1, des2 = data[0], data[1], data[2]
	local taskIdList = WuYi_Setting.GetData().ManYouSanJieTask[index - 1]
	local taskId = tonumber(taskIdList[2])
	local stateInfo = LuaWuYi2022Mgr.m_YouSanJieTaskStateList[taskId]
	local haved = stateInfo 
	local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
	local lockSprite = obj.transform:Find("LockSprite")
	local finishedSprite = obj.transform:Find("FinishedSprite")
	label.text = title
	lockSprite.gameObject:SetActive(not haved)
	label.gameObject:SetActive(haved)
	finishedSprite.gameObject:SetActive(stateInfo and stateInfo.bFinished )
	UIEventListener.Get(obj.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if haved then
			self:OnSelectTask(index)
		else
			g_MessageMgr:ShowMessage("WuYi2022Task_UnLock")
		end
		if CGuideMgr.Inst:IsInPhase(EnumGuideKey.WuYi2022AcceptTaskWnd) then
			CGuideMgr.Inst:EndCurrentPhase()
		end
	end)
end

--@region UIEvent

function LuaWuYi2022AcceptTaskWnd:OnButtonClick()
	if self.m_Index then
		local lastIndex = self.m_Index - 1
		if lastIndex >= 1 then
			local taskIdList = WuYi_Setting.GetData().ManYouSanJieTask[lastIndex - 1]
			local taskId = tonumber(taskIdList[2])
			local stateInfo = LuaWuYi2022Mgr.m_YouSanJieTaskStateList[taskId]
			if not stateInfo.bFinished then
				g_MessageMgr:ShowMessage("WuYi2022_LastTask_NotFinished")
				return 
			end
		end
		Gac2Gas.RequestAcceptManYouSanJieTask(self.m_Index)
		CUIManager.CloseUI(CLuaUIResources.WuYi2022AcceptTaskWnd)
	end
end

function LuaWuYi2022AcceptTaskWnd:OnSelectTask(index)
	self.m_Index = index
	local data = self.m_TaskDesData[index - 1]
	local title, des1, des2 = data[0], data[1], data[2]
	local taskIdList = WuYi_Setting.GetData().ManYouSanJieTask[index - 1]
	self.TopView.gameObject:SetActive(true)
	self.TitleLabel.text = title
	self.DesLabel1.text = des1
	self.DesLabel2.text = des2
	local finished = true
	for i = 1,#self.m_TaskLabelList do
		local label = self.m_TaskLabelList[i]
		local taskId = taskIdList[i - 1]
		local taskData = Task_Task.GetData(taskId)
		label.text = taskData and taskData.Display or ""
		local stateInfo = LuaWuYi2022Mgr.m_YouSanJieTaskStateList[taskId]
		label.transform:Find("FinishedSprite").gameObject:SetActive(stateInfo and stateInfo.bFinished)
		finished = finished and (stateInfo and stateInfo.bFinished)
	end
	self.Button.gameObject:SetActive(not finished)
	self.FinishedLabel.gameObject:SetActive(finished)

	local b = NGUIMath.CalculateRelativeWidgetBounds(self.ButtonFx.transform)
	LuaTweenUtils.DOKill(self.ButtonFx.transform, false)
	local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
	self.ButtonFx.transform.localPosition = waypoints[0]
	LuaTweenUtils.DOLuaLocalPath(self.ButtonFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)

	self.TopSprite:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/mysj_task_%d.mat",index))
end

function LuaWuYi2022AcceptTaskWnd:OnCloseTopViewButtonClick()
	self.TopView.gameObject:SetActive(false)
	LuaTweenUtils.DOKill(self.ButtonFx.transform, false)
end


--@endregion UIEvent
function LuaWuYi2022AcceptTaskWnd:GetGuideGo(methodName)
	if methodName == "GetFirstTaskBtn" then
		return self.Template1.transform:Find("btn").gameObject
	end
	return nil
end
