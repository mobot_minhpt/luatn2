local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QualityColor = import "L10.Game.QualityColor"
local CItem = import "L10.Game.CItem"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaFashionLotterySplitWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFashionLotterySplitWnd, "ItemCell1", GameObject)
RegistChildComponent(LuaFashionLotterySplitWnd, "ItemCell2", GameObject)
RegistChildComponent(LuaFashionLotterySplitWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaFashionLotterySplitWnd, "ConfirmBtn", GameObject)
RegistChildComponent(LuaFashionLotterySplitWnd, "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)

RegistClassMember(LuaFashionLotterySplitWnd, "m_CountLabel_Item1")
RegistClassMember(LuaFashionLotterySplitWnd, "m_CountLabel_Item2")

RegistClassMember(LuaFashionLotterySplitWnd, "m_ConfirmMsgInfo")
RegistClassMember(LuaFashionLotterySplitWnd, "m_PerPrice")
--@endregion RegistChildComponent end

function LuaFashionLotterySplitWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.RuleBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
    UIEventListener.Get(self.ConfirmBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnConfirmBtnClick()
    end)
    self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:OnValueChanged(val)
    end)
end

function LuaFashionLotterySplitWnd:Init()
    self.m_ConfirmMsgInfo = {}
    local commonItem = CItemMgr.Inst:GetById(LuaFashionLotteryMgr.m_DissambleItemId)
    if not commonItem then return end
    self:InitSplitItem(commonItem.TemplateId)
    self:InitSplitTarget(commonItem.TemplateId)
    self.QnIncreseAndDecreaseButton:SetMinMax(1, commonItem.Amount, 1)
    self.QnIncreseAndDecreaseButton:SetValue(1)
    self:OnValueChanged(1)
end


function LuaFashionLotterySplitWnd:InitSplitItem(templateId)
    local itemData = CItemMgr.Inst:GetItemTemplate(templateId)
    if itemData then
        local nameLabel = self.ItemCell1.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        self.m_CountLabel_Item1 = self.ItemCell1.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
        nameLabel.color = QualityColor.GetRGBValue(CItem.GetQualityType(templateId)) 
        nameLabel.text = itemData.Name
        self.ItemCell1:GetComponent(typeof(CQnReturnAwardTemplate)):Init(itemData, 1)
        self.m_ConfirmMsgInfo.fashionName = itemData.Name
    end
end

function LuaFashionLotterySplitWnd:InitSplitTarget(templateId)
    local nameLabel = self.ItemCell2.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.m_CountLabel_Item2 = self.ItemCell2.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    local icon = self.ItemCell2.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local checkInfo = LuaFashionLotteryMgr.m_DisassembleList[templateId]
    if checkInfo and checkInfo.type == 1 then   -- 拆分道具为万能券
        self:SplitToJade(icon, checkInfo.prize)
    else
        local disassembleData = FashionLottery_Disassemble.GetData(templateId)
        if disassembleData then
            if disassembleData.Jade > 0 then
                self:SplitToJade(icon, disassembleData.Jade)
            elseif disassembleData.TicketNum > 0 then
                self:SplitToTicket(icon, disassembleData.TicketNum, disassembleData.ActivityId)
            else
                self:SplitToPoint(icon, disassembleData.Points)
            end
        end
    end
    nameLabel.text = self.m_ConfirmMsgInfo.targetName
end

function LuaFashionLotterySplitWnd:SplitToTicket(icon, price, activityId)
    local activityData = FashionLottery_Activity.GetData(activityId)
    local uniTicketId = activityData.UniTicketId
    local itemData = CItemMgr.Inst:GetItemTemplate(uniTicketId)
    self.ItemCell2:GetComponent(typeof(CQnReturnAwardTemplate)):Init(itemData, 1)
    self.m_PerPrice = price
    self.m_ConfirmMsgInfo.targetName = itemData.Name
end

function LuaFashionLotterySplitWnd:SplitToPoint(icon, price)
    self.m_PerPrice = price
    local disassemblePointsInfo = FashionLottery_Settings.GetData().DisassemblePointsInfo
    self.m_ConfirmMsgInfo.targetName = disassemblePointsInfo[0]
    icon:LoadMaterial(disassemblePointsInfo[1])
end

function LuaFashionLotterySplitWnd:SplitToJade(icon, price)
    self.m_PerPrice = price
    local disassembleJadeInfo = FashionLottery_Settings.GetData().DisassembleJadeInfo
    self.m_ConfirmMsgInfo.targetName = disassembleJadeInfo[0]
    icon:LoadMaterial(disassembleJadeInfo[1])
end

function LuaFashionLotterySplitWnd:OnValueChanged(val)
    self.m_ConfirmMsgInfo.count = val * self.m_PerPrice
    self.m_CountLabel_Item1.text = SafeStringFormat3("×%d", val)
    self.m_CountLabel_Item2.text = SafeStringFormat3("×%d", self.m_ConfirmMsgInfo.count)
end

--@region UIEvent
function LuaFashionLotterySplitWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("FashionLottery_Split_Rule_Tip")
end

function LuaFashionLotterySplitWnd:OnConfirmBtnClick()
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, LuaFashionLotteryMgr.m_DissambleItemId)
    if pos ~= 0 then
        local splitCount = self.QnIncreseAndDecreaseButton:GetValue()
        local msg = g_MessageMgr:FormatMessage("FashionLottery_Split_Confirm", splitCount, self.m_ConfirmMsgInfo.fashionName, self.m_ConfirmMsgInfo.count, self.m_ConfirmMsgInfo.targetName)
        g_MessageMgr:ShowOkCancelMessage(msg, function()
            Gac2Gas.FashionLotteryDisassemble(EnumItemPlace.Bag, pos, LuaFashionLotteryMgr.m_DissambleItemId, splitCount)
            CUIManager.CloseUI(CLuaUIResources.FashionLotterySplitWnd)
        end, nil, nil, nil, false)
    else
        CUIManager.CloseUI(CLuaUIResources.FashionLotterySplitWnd)
    end
end
--@endregion UIEvent

