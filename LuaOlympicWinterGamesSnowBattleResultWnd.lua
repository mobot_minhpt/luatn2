local CUITexture = import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"

LuaOlympicWinterGamesSnowBattleResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "ShopButton", "ShopButton", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "NoneRewardView", "NoneRewardView", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "Win", "Win", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "Draw", "Draw", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "Lose", "Lose", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "RewardView", "RewardView", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "ScoreLabel2", "ScoreLabel2", UILabel)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "Item", "Item", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleResultWnd, "IconTexture", "IconTexture", CUITexture)

--@endregion RegistChildComponent end

function LuaOlympicWinterGamesSnowBattleResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShopButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShopButtonClick()
	end)


	
	UIEventListener.Get(self.Item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnItemClick()
	end)


    --@endregion EventBind end
end

function LuaOlympicWinterGamesSnowBattleResultWnd:Init()
	self.ScoreLabel.text = LuaOlympicGamesMgr.m_SnowballFightResultScore
	self.ScoreLabel2.text = LuaOlympicGamesMgr.m_SnowballFightResultScore
	self.Win.gameObject:SetActive(LuaOlympicGamesMgr.m_SnowballFightPlayResult == EnumSnowballFightPlayResult.Win)
	self.Lose.gameObject:SetActive(LuaOlympicGamesMgr.m_SnowballFightPlayResult == EnumSnowballFightPlayResult.Lose)
	self.Draw.gameObject:SetActive(LuaOlympicGamesMgr.m_SnowballFightPlayResult == EnumSnowballFightPlayResult.Tie)
	local itemData = Item_Item.GetData(LuaOlympicGamesMgr.m_SnowballFightRewardItemId)
	if itemData then
		self.IconTexture:LoadMaterial(itemData.Icon)
	end
	self.RewardView.gameObject:SetActive(itemData)
	self.NoneRewardView.gameObject:SetActive(not itemData)
end

--@region UIEvent

function LuaOlympicWinterGamesSnowBattleResultWnd:OnShopButtonClick()
	CLuaNPCShopInfoMgr.ShowScoreShopById(WinterOlympic_SnowballFightSetting.GetData().ShopId)
end

function LuaOlympicWinterGamesSnowBattleResultWnd:OnItemClick()
	CItemInfoMgr.ShowLinkItemTemplateInfo(LuaOlympicGamesMgr.m_SnowballFightRewardItemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
end

--@endregion UIEvent

