local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CIMMgr = import "L10.Game.CIMMgr"
local String = import "System.String"
local CUITexture = import "L10.UI.CUITexture"
local PlayerSettings = import "L10.Game.PlayerSettings"
local TweenAlpha = import "TweenAlpha"

LuaVoiceAchievementItem = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaVoiceAchievementItem, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaVoiceAchievementItem, "ProfileFrame", "ProfileFrame", CUITexture)
RegistChildComponent(LuaVoiceAchievementItem, "ExpressionText", "ExpressionText", CUITexture)
RegistChildComponent(LuaVoiceAchievementItem, "SenderNameLabel", "SenderNameLabel", UILabel)
RegistChildComponent(LuaVoiceAchievementItem, "MsgLabel", "MsgLabel", UILabel)
RegistChildComponent(LuaVoiceAchievementItem, "ChatBubble", "ChatBubble", UISprite)
RegistChildComponent(LuaVoiceAchievementItem, "ChannelNameLabel", "ChannelNameLabel", UILabel)
RegistChildComponent(LuaVoiceAchievementItem, "AudioBtn", "AudioBtn", GameObject)
RegistChildComponent(LuaVoiceAchievementItem, "AudioSprite", "AudioSprite", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaVoiceAchievementItem,"m_AudioTween")
function LuaVoiceAchievementItem:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.MsgLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnLabelClick()
    end)
    self.m_AudioTween = self.AudioSprite.transform:GetComponent(typeof(TweenAlpha))
end

function LuaVoiceAchievementItem:Init()

end

function LuaVoiceAchievementItem:InitItem(data,isOpp)
    --我解锁了语音成就:<link button=姐夫祝词·修为,VoiceAchievement,4>
    local id = data.voiceAchievementId
    self.MsgLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]",NGUIText.EncodeColor24(self.MsgLabel.color),data.text)
    self.MsgLabel:ResizeCollider()
    if isOpp then
        self.ChannelNameLabel.text = SafeStringFormat3("[%s]%s",CChatHelper.GetChannelColor24(data.channel), data.channelName)
        local name = data.senderName
        if CIMMgr.Inst:LiangHaoEnabled() and not String.IsNullOrEmpty(data.LiangHaoIcon) then
            name = SafeStringFormat3("%s%s",data.senderName, data.LiangHaoIcon)
        end
        self.SenderNameLabel.text = name
    end
    self.Portrait:LoadNPCPortrait(data.senderPortrait)
    self.ExpressionText:LoadMaterial(data.expressionTxtPath)
    self.ChatBubble.spriteName = isOpp and "chat_background_1" or "chat_background_3"
    self.AudioSprite:SetActive(true)
    self.m_AudioTween.enabled = false
    --local id = 
    UIEventListener.Get(self.AudioBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaVoiceAchievementMgr:OnAudioBtnClick(id)
        --self.AudioSprite:SetActive(true)
        self.m_AudioTween.enabled = true
        if self.m_AudioTick then
            UnRegisterTick(self.m_AudioTick)
            self.m_AudioTick = nil
        end
        self.m_AudioTick = RegisterTickOnce(function()
            --self.AudioSprite:SetActive(false)   
            self.m_AudioTween.enabled = false
            self.m_AudioTween.value = 1
            --self.AudioSprite.alpha = 1
        end,3*1000)
    end)
    UIEventListener.Get(self.AudioBtn).onSelect = DelegateFactory.BoolDelegate(function(btn, isselected)
        if not isselected then
            if self.m_AudioTick then
                UnRegisterTick(self.m_AudioTick)
                self.m_AudioTick = nil
            end
            self.m_AudioTween.enabled = false
            self.m_AudioTween.value = 1
        end 
    end)
end

function LuaVoiceAchievementItem:OnDisable()
    if self.m_AudioTick then
        UnRegisterTick(self.m_AudioTick)
        self.m_AudioTick = nil
    end
end

function LuaVoiceAchievementItem:OnLabelClick()
    local url = self.MsgLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end




--@region UIEvent

--@endregion UIEvent

