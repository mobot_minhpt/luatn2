require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UITable = import "UITable"
local UIWidget = import "UIWidget"
local UILabel = import "UILabel"
local NGUIMath = import "NGUIMath"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local EnumPropertyAppearanceSettingBit = import "L10.Game.CPropertyAppearance+EnumPropertyAppearanceSettingBit"
local EnumClass = import "L10.Game.EnumClass"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CFashionMgr = import "L10.Game.CFashionMgr"

LuaPlayerAppearanceSettingWnd = class()
LuaPlayerAppearanceSettingWnd.Path = "ui/lianghao/LuaPlayerAppearanceSettingWnd"

RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_Background")

RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_CheckboxTemplate")
RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_CheckboxTable")

RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_LiangHaoRoot")
RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_MainPlayerIDLabel")
RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_MainPlayerLiangHaoIDLabel")
RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_MainPlayerDescLabel")
RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_CopyIDButton")
RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_SetLiangHaoButton")
RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_BuyLiangHaoButton")

RegistClassMember(LuaPlayerAppearanceSettingWnd, "m_enableBeiShi")

function LuaPlayerAppearanceSettingWnd:Init()

	self.m_enableBeiShi = false

	self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UIWidget))
	self.m_CheckboxTemplate = self.transform:Find("Anchor/Background/Checkbox").gameObject
	self.m_CheckboxTable = self.transform:Find("Anchor/Background/Table"):GetComponent(typeof(UITable))
	self.m_LiangHaoRoot = self.transform:Find("Anchor/Background/LiangHao"):GetComponent(typeof(UIWidget))
	self.m_MainPlayerIDLabel = self.m_LiangHaoRoot.transform:Find("PlayerID/PlayerIDLabel"):GetComponent(typeof(UILabel))
	self.m_MainPlayerLiangHaoIDLabel = self.m_LiangHaoRoot.transform:Find("PlayerID/LiangHaoIDLabel"):GetComponent(typeof(UILabel))
	self.m_MainPlayerDescLabel = self.m_LiangHaoRoot.transform:Find("PlayerID/DescLabel"):GetComponent(typeof(UILabel))
	self.m_CopyIDButton = self.m_LiangHaoRoot.transform:Find("Table/CopyIDButton").gameObject
	self.m_SetLiangHaoButton = self.m_LiangHaoRoot.transform:Find("Table/SetLiangHaoButton").gameObject
	self.m_BuyLiangHaoButton = self.m_LiangHaoRoot.transform:Find("Table/BuyLiangHaoButton").gameObject

	self.m_CheckboxTemplate:SetActive(false)

	CommonDefs.AddOnClickListener(self.m_CopyIDButton, DelegateFactory.Action_GameObject(function(go) self:OnCopyIDButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SetLiangHaoButton, DelegateFactory.Action_GameObject(function(go) self:OnSetLiangHaoButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_BuyLiangHaoButton, DelegateFactory.Action_GameObject(function(go) self:OnBuyLiangHaoButtonClick() end), false)

	self:InitCheckboxAndLiangHao()
end


function LuaPlayerAppearanceSettingWnd:InitCheckboxAndLiangHao()
	local mainplayer = CClientMainPlayer.Inst
	self.m_MainPlayerIDLabel.text = mainplayer and tostring(mainplayer.Id) or ""

	local profileInfo = mainplayer and mainplayer.BasicProp.ProfileInfo
	if profileInfo and CServerTimeMgr.Inst.timeStamp<profileInfo.LiangHaoExpiredTime then
		self.m_MainPlayerLiangHaoIDLabel.text = tostring(profileInfo.LiangHaoId)
		self.m_MainPlayerIDLabel.gameObject:SetActive(false)
		self.m_MainPlayerLiangHaoIDLabel.gameObject:SetActive(true)
		self.m_MainPlayerDescLabel.text = SafeStringFormat3(LocalString.GetString("(原:%s)"), self.m_MainPlayerIDLabel.text)
	else
		self.m_MainPlayerIDLabel.gameObject:SetActive(true)
		self.m_MainPlayerLiangHaoIDLabel.gameObject:SetActive(false)
		self.m_MainPlayerDescLabel.text = LocalString.GetString("无靓号")
	end

	self:InitAppearanceSetting()
	self.m_CheckboxTable:Reposition()
	local tableheight = NGUIMath.CalculateRelativeWidgetBounds(self.m_CheckboxTable.transform).size.y

	local total = tableheight
	if LuaPlayerAppearanceSettingWnd:LiangHaoSettingVisible() then
		self.m_LiangHaoRoot.gameObject:SetActive(true)
		self.m_LiangHaoRoot.transform.localPosition = Vector3(0, - tableheight, 0)
		total = total + self.m_LiangHaoRoot.height
	else
		self.m_LiangHaoRoot.gameObject:SetActive(false)
	end

	self.m_Background.height = total + 30

	local targetCenter = CUICommonDef.WorldSpace2NGUISpace(LuaLiangHaoMgr.TargetWorldCenter)
	--靠近target右侧底部对齐
	local pos = Vector3(targetCenter.x + LuaLiangHaoMgr.TargetSize.x * 0.5 + self.m_Background.width * 0.5, targetCenter.y + self.m_Background.height, 0)
	self.m_Background.transform.localPosition = pos
end

function LuaPlayerAppearanceSettingWnd:InitAppearanceSetting()
	if not CClientMainPlayer.Inst then
		self:Close()
		return
	end
	local fanshenRootObj = self.m_CheckboxTable.transform:Find("FanShenRoot").gameObject
	local xianShenRootObj = self.m_CheckboxTable.transform:Find("XianShenRoot").gameObject
	local bXianshen = CClientMainPlayer.Inst.AppearanceProp.FeiShengAppearanceXianFanStatus > 0
	fanshenRootObj:SetActive(not bXianshen)
	xianShenRootObj:SetActive(bXianshen)
	local bShowShieldOption = LuaLiangHaoMgr.CanHoldDunpaiClass()

	local rootobj = bXianshen and xianShenRootObj or fanshenRootObj
	-- 通用的盾牌设置
	local shieldObj = rootobj.transform:Find("Shield").gameObject
	shieldObj:SetActive(bShowShieldOption)
	if bShowShieldOption then
		local shieldCheckBox = shieldObj:GetComponent(typeof(QnCheckBox))
		shieldCheckBox.OnValueChanged = DelegateFactory.Action_bool(
			function (checked) 
				LuaLiangHaoMgr.OnDunPaiCheckStatusChanged(checked) 
			end)
		shieldCheckBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideDunPai == 0, false)
	end

	local havemask = false
	if CClientMainPlayer.Inst then
		havemask = CommonDefs.IsZhanKuang(CClientMainPlayer.Inst.Class)
	end

	local maskobj = rootobj.transform:Find("Mask").gameObject
	maskobj:SetActive(havemask)
	if havemask then
		local maskCheckBox = maskobj:GetComponent(typeof(QnCheckBox))
		maskCheckBox.OnValueChanged = DelegateFactory.Action_bool(
			function (checked) 
				CFashionMgr.Inst:RequestSetZhanKuangMaskHide(checked)
			end)
		maskCheckBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideZhanKuangMask == 0, false)
	end

	local beishibtn = nil
	if bXianshen then

		-- 神兵背饰
		beishibtn = xianShenRootObj.transform:Find("Pendant/BeiShiBtn")

		-- 仙身设置
		self:InitXianShenAppearanceHelmetSetting(xianShenRootObj)
		self:InitXianShenAppearanceClothesSetting(xianShenRootObj)

		local shenbin = xianShenRootObj.transform:Find("Pendant/ShenBin").gameObject
		local havefx = self:HaveShenBingBackClothFx()
		shenbin:SetActive(havefx)
		if havefx then
			local backClothFxCheckBox = shenbin:GetComponent(typeof(QnCheckBox))
			backClothFxCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
				Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingBackOrnament), checked and 0 or 1)
		 	end)
			backClothFxCheckBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideShenBingBackOrnament == 0, true)
		end

		-- 装备背饰
		local backPendantCheckBox = xianShenRootObj.transform:Find("Pendant/BackPendant"):GetComponent(typeof(QnCheckBox))
		backPendantCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
			self.m_enableBeiShi = checked
			self:RequestSetBackPendant(checked)
		end)
		self.m_enableBeiShi = CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0
		backPendantCheckBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0, true)
	else
		-- 凡身设置
		local helmetCheckBox = fanshenRootObj.transform:Find("Helmet"):GetComponent(typeof(QnCheckBox))
		helmetCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (checked) LuaLiangHaoMgr.OnTouKuiCheckStatusChanged(not checked) end)
		helmetCheckBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect ~= 0, false)

		beishibtn = fanshenRootObj.transform:Find("BackPendant/BeiShiBtn")
		-- 设置背饰
		local backPendantCheckBox = fanshenRootObj.transform:Find("BackPendant"):GetComponent(typeof(QnCheckBox))
		backPendantCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (checked)
			self.m_enableBeiShi = checked
			self:RequestSetBackPendant(checked)
		end)
		self.m_enableBeiShi = CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0
		backPendantCheckBox:SetSelected(CClientMainPlayer.Inst.AppearanceProp.HideBackPendant == 0, true)
	end

	CommonDefs.AddOnClickListener(
		beishibtn.gameObject, 
		DelegateFactory.Action_GameObject(
			function(go) 
				self:OnBeiShiBtnClick() 
			end
		), 
		false
	)

	-- 调整
	fanshenRootObj:GetComponent(typeof(UITable)):Reposition()
	xianShenRootObj:GetComponent(typeof(UITable)):Reposition()
end

function LuaPlayerAppearanceSettingWnd:OnBeiShiBtnClick()
	if not CClientMainPlayer.Inst then return end
	local bXianshen = CClientMainPlayer.Inst.AppearanceProp.FeiShengAppearanceXianFanStatus > 0
	local bsitem = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Body, 13)
	if not bsitem then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("尚未穿戴装备背饰，无法调整背饰位置"))
	else
		if not self.m_enableBeiShi then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请先勾选显示装备背饰再调整位置"))
		else
			LuaAppearancePreviewMgr.ShowBeiShiSetting()
			CUIManager.CloseUI(CLuaUIResources.PlayerAppearanceSettingWnd)
		end
	end 
end



function LuaPlayerAppearanceSettingWnd:HaveShenBingBackClothFx()
	local data = Initialization_Init.GetData(EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.Gender))
	if not data then
		return false
	end
	return data.ShenBingBeiShiFx > 0
end

function LuaPlayerAppearanceSettingWnd:RequestSetBackPendant(checked)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Class == EnumClass.YanShi then
		if checked then
			g_MessageMgr:ShowMessage("YanShi_Set_BackPendant_Show")
		else
			g_MessageMgr:ShowMessage("YanShi_Set_BackPendant_Hide")
		end
	end
	Gac2Gas.RequestSetBackPendantHide(checked and 0 or 1)
end

function LuaPlayerAppearanceSettingWnd:InitXianShenAppearanceHelmetSetting(xianShenRootObj)
	local helmetCheckBoxGroup = xianShenRootObj.transform:Find("Helmet"):GetComponent(typeof(QnCheckBoxGroup))
	local helmetLabel = helmetCheckBoxGroup.transform:Find("HelmetLabel"):GetComponent(typeof(UILabel))
	helmetCheckBoxGroup:InitWithOptions(Table2Array({LocalString.GetString("仙身"), LocalString.GetString("神兵"), LocalString.GetString("150鬼装"), LocalString.GetString("初始发型")}, MakeArrayClass(String)), false, false)

	local helmetIndex
	if CClientMainPlayer.Inst.AppearanceProp.HideHelmetEffect > 0 then
		helmetIndex = 3
	elseif CClientMainPlayer.Inst.AppearanceProp.HideShenBingHeadwear == 0 then
		helmetIndex = 1
	elseif CClientMainPlayer.Inst.AppearanceProp.Hide150GhostHeadwear == 0 then
		helmetIndex = 2
	else
		helmetIndex = 0
	end
	helmetCheckBoxGroup.m_CheckBoxes[helmetIndex].Selected = true
	helmetCheckBoxGroup:SetSelect(helmetIndex, true)
	if helmetIndex == 3 then
		helmetLabel.text = LocalString.GetString("头盔外观（当前不显示头盔）")
	end

	helmetCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(cb, index)
		helmetLabel.text = index == 3 and LocalString.GetString("头盔外观（当前不显示头盔）") or LocalString.GetString("头盔外观")

		-- Show Shenbing
		if index == 1 then
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingHeadwear), 0)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostHeadwear), 0)
			g_MessageMgr:ShowMessage("SHENBING_SETTING_NOTICE")
			Gac2Gas.RequestSetHelmetHide(0)
		elseif index == 2 then
			-- Show 150 Ghost
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingHeadwear), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostHeadwear), 0)
			g_MessageMgr:ShowMessage("150GHOST_SETTING_NOTICE")
			Gac2Gas.RequestSetHelmetHide(0)
		elseif index == 0 then
			-- Show Normal
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingHeadwear), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostHeadwear), 1)
			Gac2Gas.RequestSetHelmetHide(0)
		else
			-- index == 3
			-- Hide helmet
			Gac2Gas.RequestSetHelmetHide(1)
		end
	end)
end

function LuaPlayerAppearanceSettingWnd:InitXianShenAppearanceClothesSetting(xianShenRootObj)
	local clothCheckBoxGroup = xianShenRootObj.transform:Find("Clothes"):GetComponent(typeof(QnCheckBoxGroup))
	--local helmetLabel = helmetCheckBoxGroup.transform:Find("HelmetLabel"):GetComponent(typeof(UILabel))
	clothCheckBoxGroup.gameObject:SetActive(true)
	clothCheckBoxGroup:InitWithOptions(Table2Array({LocalString.GetString("神兵"), LocalString.GetString("150鬼装")}, MakeArrayClass(String)), false, true)

	local helmetIndex
	if CClientMainPlayer.Inst.AppearanceProp.HideShenBingClothes == 0 then
		helmetIndex = 0
	elseif CClientMainPlayer.Inst.AppearanceProp.Hide150GhostClothes == 0 then
		helmetIndex = 1
	else
		helmetIndex = -1
	end
	if helmetIndex >= 0 then
		clothCheckBoxGroup.m_CheckBoxes[helmetIndex].Selected = true
		clothCheckBoxGroup:SetSelect(helmetIndex, true)
	else
		for i = 0, 1 do
			clothCheckBoxGroup:SetSelect(i, false)
		end
	end

	clothCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(cb, index)
		if index == 0 then
			-- Show Shenbing
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingClothes), 0)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostClothes), 0)
			g_MessageMgr:ShowMessage("SHENBING_SETTING_NOTICE")
		elseif index == 1 then
			-- Show 150 Ghost
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingClothes), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostClothes), 0)
			g_MessageMgr:ShowMessage("150GHOST_SETTING_NOTICE")
		else
			-- Show Normal
			-- index == -1
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingClothes), 1)
			Gac2Gas.SetPropertyAppearanceSettingInfo(EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostClothes), 1)
		end
	end)
end

function LuaPlayerAppearanceSettingWnd:LiangHaoSettingVisible()
	--方法在iOS提审服会用到，改动时请留意
	return true
end

function LuaPlayerAppearanceSettingWnd:OnCopyIDButtonClick()
	if self.m_MainPlayerIDLabel.gameObject.activeSelf then
		CUICommonDef.clipboardText = self.m_MainPlayerIDLabel.text
		g_MessageMgr:ShowMessage("Copy_Success_Tips")
	elseif self.m_MainPlayerLiangHaoIDLabel.gameObject.activeSelf then
		CUICommonDef.clipboardText = self.m_MainPlayerLiangHaoIDLabel.text
		g_MessageMgr:ShowMessage("Copy_Success_Tips")
	end
end

function LuaPlayerAppearanceSettingWnd:OnSetLiangHaoButtonClick()

	local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.ProfileInfo.LiangHaoId
	if id and id > 0 then
		LuaLiangHaoMgr.ShowLiangHaoSettingWnd()
	else
		g_MessageMgr:ShowMessage("EnterLianghaoConfigFail_NoLianghao")
	end
end

function LuaPlayerAppearanceSettingWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaPlayerAppearanceSettingWnd:OnBuyLiangHaoButtonClick()
	LuaLiangHaoMgr.ShowLiangHaoPurchaseWnd()
	--靓号购买界面背景太淡了，交互说要打开时关闭背后的窗口
	self:Close()
	CUIManager.CloseUI("MainPlayerWnd")
end

function LuaPlayerAppearanceSettingWnd:GetGuideGo(methodName)
	if methodName == "GetBeiShiBtn" then
		if not CClientMainPlayer.Inst then
			return nil
		end
		local bXianshen = CClientMainPlayer.Inst.AppearanceProp.FeiShengAppearanceXianFanStatus > 0
		if bXianshen then
			return self.transform:Find("Anchor/Background/Table/XianShenRoot/Pendant/BeiShiBtn").gameObject
		else
			return self.transform:Find("Anchor/Background/Table/FanShenRoot/BackPendant/BeiShiBtn").gameObject
		end
	end
end
