local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UIStaticBlur = import "L10.UI.UIStaticBlur"
local CTeamMgr     = import "L10.Game.CTeamMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Animation = import "UnityEngine.Animation"
local CJingLingMgr = import "L10.Game.CJingLingMgr"

LuaShuJiaHaiZhanSignUpWnd = class()

RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "RewardTemplate")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "RewardGrid")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "RewardInfo")

RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "Anchor")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "LvLb")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "TypeLb")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "TimeLb")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "DescLb")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "RuleBtn")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "FightBtn")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "StaticBlur")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "StaticBlurTex")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "Normal")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "Hard")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "Anim")

RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "m_Type")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "m_GameplayId")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "m_CurMode")
RegistClassMember(LuaShuJiaHaiZhanSignUpWnd, "m_ChangeModeTick")

function LuaShuJiaHaiZhanSignUpWnd:Awake()
    self.Anchor = self.transform:Find("Anchor")
    self.RewardTemplate = self.transform:Find("Anchor/InfoView/BasicInfo/Rewards/Item").gameObject
    self.RewardTemplate:SetActive(false)
    self.RewardGrid = self.transform:Find("Anchor/InfoView/BasicInfo/Rewards/Grid"):GetComponent(typeof(UIGrid))
    self.RewardInfo = self.transform:Find("Anchor/InfoView/BasicInfo/Rewards/Info"):GetComponent(typeof(UILabel))

    self.LvLb = self.transform:Find("Anchor/InfoView/BasicInfo/Need/Info"):GetComponent(typeof(UILabel))
    self.TypeLb = self.transform:Find("Anchor/InfoView/BasicInfo/Type/Info"):GetComponent(typeof(UILabel))
    self.TimeLb = self.transform:Find("Anchor/InfoView/BasicInfo/Time/Info"):GetComponent(typeof(UILabel))
    self.DescLb = self.transform:Find("Anchor/InfoView/BasicInfo/Info"):GetComponent(typeof(UILabel))
    self.RuleBtn = self.transform:Find("Anchor/InfoView/RuleButton").gameObject
    self.FightBtn = self.transform:Find("Anchor/InfoView/FightButton").gameObject
    self.StaticBlur = self.transform:Find("StaticBlur"):GetComponent(typeof(UIStaticBlur))
    self.StaticBlurTex = self.transform:Find("StaticBlur"):GetComponent(typeof(UITexture))
    self.Normal = self.transform:Find("Anchor/InfoView/Mode/Normal"):GetComponent(typeof(QnSelectableButton))
    self.Hard = self.transform:Find("Anchor/InfoView/Mode/Hard"):GetComponent(typeof(QnSelectableButton))

    self.Anim = self.transform:GetComponent(typeof(Animation))
end

function LuaShuJiaHaiZhanSignUpWnd:OnEnable()
    self.StaticBlur:InitStaticBlur(self.StaticBlurTex)

    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
end

function LuaShuJiaHaiZhanSignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    UnRegisterTick(self.m_ChangeModeTick)
    self.m_ChangeModeTick = nil
end

function LuaShuJiaHaiZhanSignUpWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if (CClientMainPlayer.Inst or {}).Id == playerId and playId == self.m_GameplayId then
        -- 报名后正在匹配中置灰
        CUICommonDef.SetActive(self.FightBtn, not isInMatching, true)
    end
end

function LuaShuJiaHaiZhanSignUpWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == self.m_GameplayId and success then
        -- 报名成功后置灰
        CUICommonDef.SetActive(self.FightBtn, false, true)
    end
end

function LuaShuJiaHaiZhanSignUpWnd:ParseRewards(rewardStr)
    local rewards = {}
    for reward in string.gmatch(rewardStr, "([^;]+)") do
        table.insert(rewards, tonumber(reward))
    end
    return rewards
end

function LuaShuJiaHaiZhanSignUpWnd:SetModeBtn(mode)
    self.Normal:SetSelected(mode == 1)
    self.Hard:SetSelected(mode == 2)
    self.Normal.transform:Find("Label"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(mode == 1 and "ffffff" or "e0f9ff", 0)
    self.Hard.transform:Find("Label"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(mode == 1 and "d4c7f8" or "8759aa", 0)
end

function LuaShuJiaHaiZhanSignUpWnd:Init1(anim) --初探仙海
    LuaUtils.SetLocalPositionX(self.Anchor, -165)
    self.m_GameplayId = NavalWar_Setting.GetData().TrainingSeaPlayId
    self.Normal.gameObject:SetActive(false)
    self.Hard.gameObject:SetActive(false)

    local rewardItems = self:ParseRewards(ShuJia2023_Setting.GetData("HaiZhanGuideRewardItem").Value)
    local curTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPersistPlayDataWithKey(EnumPersistPlayDataKey_lua.eNavalWarTrainingReward) and 1 or 0
    Extensions.RemoveAllChildren(self.RewardGrid.transform)
    for _, id in ipairs(rewardItems) do
        local go = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardTemplate)
        go:SetActive(true)
        self:InitOneItem(go, id, curTimes > 0)
    end
    self.RewardGrid:Reposition()
    self.RewardInfo.text = LocalString.GetString("首次通关奖励")
    self.LvLb.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(ShuJia2023_Setting.GetData("HaiZhanGuideLv").Value))
    self.TypeLb.text = ShuJia2023_Setting.GetData("HaiZhanGuideType").Value
    self.TimeLb.text = ShuJia2023_Setting.GetData("HaiZhanGuideTime").Value
    self.DescLb.text = ShuJia2023_Setting.GetData("HaiZhanGuideTaskDesc").Value
    self.FightBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("前往挑战")
    CUICommonDef.SetActive(self.FightBtn, true, true)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        --g_MessageMgr:ShowMessage("ShuJia2023_HaiZhanGuide_Rule")
        CJingLingMgr.Inst:ShowJingLingWnd(LocalString.GetString("初探仙海"), "o_push", true, false, nil, false)
    end)
    UIEventListener.Get(self.FightBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestEnterNavalPreTrainPlay()
    end)
end

function LuaShuJiaHaiZhanSignUpWnd:Init2(anim) --缥缈海途
    LuaUtils.SetLocalPositionX(self.Anchor, -205)
    self.m_GameplayId = NavalWar_Setting.GetData().PveSeaPlayId
    self.Normal.gameObject:SetActive(true)
    self.Hard.gameObject:SetActive(true)
    local rewardItems = self:ParseRewards(ShuJia2023_Setting.GetData("HaiZhanPveRewardItem").Value)
    local hardRewardItems = self:ParseRewards(ShuJia2023_Setting.GetData("HaiZhanPveHardRewardItem").Value)
    local curTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eNavalWarPveEasyRewardTimes) or 0
    local curHardTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eNavalWarPveHardRewardTimes) or 0
    local limitTimes = NavalWar_Setting.GetData().PveRewardWeekLimit

    local changeMode = function(mode) 
        self:SetModeBtn(mode)
        self.m_CurMode = mode
        Extensions.RemoveAllChildren(self.RewardGrid.transform)
        local items = mode == 1 and rewardItems or hardRewardItems
        local times = mode == 1 and curTimes or curHardTimes
        for _, id in ipairs(items) do
            local go = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardTemplate)
            go:SetActive(true)
            local hasGot
            if mode == 1 then
                hasGot = times >= limitTimes
            else
                hasGot = times > 0
            end
            self:InitOneItem(go, id, hasGot)
        end
        self.RewardGrid:Reposition()
        self.RewardInfo.text = mode == 1 and SafeStringFormat3(LocalString.GetString("本周奖励次数 %d/%d"), curTimes, limitTimes) or LocalString.GetString("首次通关奖励")
    end
    changeMode(1)
    UIEventListener.Get(self.Normal.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_CurMode == 1 or anim:IsPlaying("shujia2023mainwnd_qiehuan") then return end
        self.Anim:Play("shujiahaizhansignupwnd_qie2")
        UnRegisterTick(self.m_ChangeModeTick)
        self.m_ChangeModeTick = RegisterTickOnce(function()
            changeMode(1)
        end, 66)
    end)
    UIEventListener.Get(self.Hard.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_CurMode == 2 or anim:IsPlaying("shujia2023mainwnd_qiehuan") then return end
        self.Anim:Play("shujiahaizhansignupwnd_qie1")
        UnRegisterTick(self.m_ChangeModeTick)
        self.m_ChangeModeTick = RegisterTickOnce(function()
            changeMode(2)
        end, 66)
    end)

    self.LvLb.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(ShuJia2023_Setting.GetData("HaiZhanPveLv").Value))
    self.TypeLb.text = ShuJia2023_Setting.GetData("HaiZhanPveType").Value
    self.TimeLb.text = ShuJia2023_Setting.GetData("HaiZhanPveTime").Value
    self.DescLb.text = ShuJia2023_Setting.GetData("HaiZhanPveTaskDesc").Value
    self.FightBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("带队挑战")
    CUICommonDef.SetActive(self.FightBtn, true, true)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        --g_MessageMgr:ShowMessage("ShuJia2023_HaiZhanPve_Rule")
        CJingLingMgr.Inst:ShowJingLingWnd(LocalString.GetString("缥缈海途"), "o_push", true, false, nil, false)
    end)
    local playId = NavalWar_Setting.GetData().PveSeaPlayId
    if CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(playId, CClientMainPlayer.Inst.Id)
    end
    UIEventListener.Get(self.FightBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.GlobalMatch_RequestSignUpWithExtra(playId, g_MessagePack.pack({IsHard = (self.m_CurMode == 2)})) --1:Normal 2:Hard
    end)
end

function LuaShuJiaHaiZhanSignUpWnd:Init3(anim) --烬火神树
    LuaUtils.SetLocalPositionX(self.Anchor, -205)
    self.Normal.gameObject:SetActive(true)
    self.Hard.gameObject:SetActive(true)
    local rewardItems = self:ParseRewards(ShuJia2023_Setting.GetData("FireTreeRewardItem").Value)
    local hardRewardItems = self:ParseRewards(ShuJia2023_Setting.GetData("FireTreeHardRewardItem").Value)
    local curTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWTSSEasyTimes) or 0
    local curHardTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWTSSHardTimes) or 0
    local limitTimes = NavalWar_Setting.GetData().WTSSWeekLimit

    local changeMode = function(mode) 
        self:SetModeBtn(mode)
        self.m_GameplayId = mode == 1 and NavalWar_Setting.GetData().WTSSEasyGameplayId or NavalWar_Setting.GetData().WTSSHardGameplayId
        self.m_CurMode = mode
        Extensions.RemoveAllChildren(self.RewardGrid.transform)
        local items = mode == 1 and rewardItems or hardRewardItems
        local times = mode == 1 and curTimes or curHardTimes
        local hasGot
        if mode == 1 then
            hasGot = times >= limitTimes
        else
            hasGot = times > 0
        end
        for _, id in ipairs(items) do
            local go = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardTemplate)
            go:SetActive(true)
            self:InitOneItem(go, id, hasGot)
        end
        self.RewardGrid:Reposition()
        self.RewardInfo.text = mode == 1 and SafeStringFormat3(LocalString.GetString("本周奖励次数 %d/%d"), curTimes, limitTimes) or LocalString.GetString("首次通关奖励")
    end
    changeMode(1)
    UIEventListener.Get(self.Normal.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_CurMode == 1 or anim:IsPlaying("shujia2023mainwnd_qiehuan3") then return end
        self.Anim:Play("shujiahaizhansignupwnd_qie4")
        UnRegisterTick(self.m_ChangeModeTick)
        self.m_ChangeModeTick = RegisterTickOnce(function()
            changeMode(1)
        end, 66)
    end)
    UIEventListener.Get(self.Hard.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_CurMode == 2 or anim:IsPlaying("shujia2023mainwnd_qiehuan3") then return end
        self.Anim:Play("shujiahaizhansignupwnd_qie3")
        UnRegisterTick(self.m_ChangeModeTick)
        self.m_ChangeModeTick = RegisterTickOnce(function()
            changeMode(2)
        end, 66)
    end)

    self.LvLb.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(ShuJia2023_Setting.GetData("FireTreeLv").Value))
    self.TypeLb.text = ShuJia2023_Setting.GetData("FireTreeType").Value
    self.TimeLb.text = ShuJia2023_Setting.GetData("FireTreeTime").Value
    self.DescLb.text = ShuJia2023_Setting.GetData("FireTreeTaskDesc").Value
    self.FightBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("带队挑战")
    CUICommonDef.SetActive(self.FightBtn, true, true)
    UIEventListener.Get(self.FightBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestEnterWTSS(self.m_CurMode == 2 and true or false)
    end)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        --g_MessageMgr:ShowMessage("ShuJia2023_FireTree_Rule")
        CJingLingMgr.Inst:ShowJingLingWnd(LocalString.GetString("烬火神树"), "o_push", true, false, nil, false)
    end)
end

function LuaShuJiaHaiZhanSignUpWnd:Init(type, anim)
    self.m_Type = type
    self["Init"..type](self, anim)
    --self.Anim:Play("shujiahaizhansignupwnd_show")
end

function LuaShuJiaHaiZhanSignUpWnd:InitOneItem(curItem, itemId, hasGot)
    curItem.transform:Find("Get").gameObject:SetActive(hasGot)
    local data = Item_Item.GetData(itemId)
    if not data then return end
    curItem.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end)
end