local CScene=import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CChatLinkMgr=import "CChatLinkMgr"
local UICamera = import "UICamera"

LuaQiXi2021View = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2021View, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaQiXi2021View, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaQiXi2021View, "LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaQiXi2021View, "TaskDesc", "TaskDesc", UILabel)

--@endregion RegistChildComponent end

function LuaQiXi2021View:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick(go)
	end)

    --@endregion EventBind end

    CommonDefs.AddOnClickListener(self.TaskDesc.gameObject, DelegateFactory.Action_GameObject(function(go) 
        self:OnLabelClick(self.TaskDesc) 
    end), false)
end

function LuaQiXi2021View:OnLabelClick(label)
	local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function LuaQiXi2021View:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("SyncYanHuaPlayInfo",self,"SyncYanHuaPlayInfo")
end

function LuaQiXi2021View:OnDisable( )
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("SyncYanHuaPlayInfo",self,"SyncYanHuaPlayInfo")
end

function LuaQiXi2021View:Init()
    self.TaskName.text = LocalString.GetString("七夕烟花")
    self.m_timestamp = 0
    self.m_status = 0
    local yanhuadata = CLuaQiXiMgr.YanHuaData
    if yanhuadata then
        if yanhuadata.timeStamp then
            self.m_timestamp = yanhuadata.timeStamp
        end
        if yanhuadata.status then
            self.m_status = yanhuadata.status
        end
    end
end

function LuaQiXi2021View:SyncYanHuaPlayInfo()
    local yanhuadata = CLuaQiXiMgr.YanHuaData
    self.m_timestamp  = yanhuadata.timeStamp
    self.m_status = yanhuadata.status
end

function LuaQiXi2021View:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            local time = self:GetRemainTimeText(CScene.MainScene.ShowTime)
            self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("剩余时间：%s"), time)
        else
            self.CountDownLabel.text = nil
        end
    else
        self.CountDownLabel.text = nil
    end

    local msg = ""
    if self.m_status == 1 then
        local stamp = self.m_timestamp - CServerTimeMgr.Inst.timeStamp
        if stamp < 0 then
            stamp = 0
        end
        local str = SafeStringFormat3("%02d:%02d", math.floor(stamp/60),stamp%60)
        msg = g_MessageMgr:FormatMessage("2021QiXi_Firework_DIYFirework",str)
    elseif self.m_status == 2 then
        local index = CLuaQiXiMgr.YanHuaData.chapterIndex
        local count = CLuaQiXiMgr.YanHuaData.chapterCount
        msg = g_MessageMgr:FormatMessage("2021QiXi_Firework_DIYFirework2",index,count)
    elseif self.m_status == 3 then
        msg = g_MessageMgr:FormatMessage("2021QiXi_Firework_DIYFirework3")
    end
    self.TaskDesc.text = msg
end

function LuaQiXi2021View:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

--@region UIEvent

function LuaQiXi2021View:OnLeaveBtnClick(go)
    local msg = g_MessageMgr:FormatMessage("2021QiXi_Firework_LeaveGameplay")
    local okfunc = function()
        Gac2Gas.RequestLeavePlay()
    end
    g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
end


--@endregion UIEvent

