require("common/common_include")

local UIGrid = import "UIGrid"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local BabyMgr = import "L10.Game.BabyMgr"
local Baby_Plan = import "L10.Game.Baby_Plan"
local Baby_Setting = import "L10.Game.Baby_Setting"
local CButton = import "L10.UI.CButton"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UILongPressButton = import "L10.UI.UILongPressButton"
local Baby_YouEr = import "L10.Game.Baby_YouEr"
--local Baby_ShaoNian = import "L10.Game.Baby_ShaoNian"
local NGUIText = import "NGUIText"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaBabySchedulePlanList = class()

RegistClassMember(LuaBabySchedulePlanList, "Anchor")
RegistClassMember(LuaBabySchedulePlanList, "BG")
RegistClassMember(LuaBabySchedulePlanList, "PlanGrid")
RegistClassMember(LuaBabySchedulePlanList, "PlanTemplate")
RegistClassMember(LuaBabySchedulePlanList, "MoneyCtrl")
RegistClassMember(LuaBabySchedulePlanList, "LeftRefreshTimeLabel")
RegistClassMember(LuaBabySchedulePlanList, "RefreshButton")

-- UI迭代添加内容
RegistChildComponent(LuaBabySchedulePlanList, "BabyNameLabel", UILabel)
RegistChildComponent(LuaBabySchedulePlanList, "BabyPropertyView", GameObject)
RegistChildComponent(LuaBabySchedulePlanList, "PropertyGrid", UIGrid)
RegistChildComponent(LuaBabySchedulePlanList, "PropertyTemplate", GameObject)
RegistChildComponent(LuaBabySchedulePlanList, "BabyPropertyView_ShaoNian", GameObject)
RegistChildComponent(LuaBabySchedulePlanList, "PropertyGrid_ShaoNian", UIGrid)
RegistChildComponent(LuaBabySchedulePlanList, "PropertyTemplate_ShaoNian", GameObject)
RegistChildComponent(LuaBabySchedulePlanList, "BabyPlanDetail", CCommonLuaScript)

RegistClassMember(LuaBabySchedulePlanList, "SelectedBaby")
RegistClassMember(LuaBabySchedulePlanList, "PlanableData")
RegistClassMember(LuaBabySchedulePlanList, "SelectedPlanIndex")
RegistClassMember(LuaBabySchedulePlanList, "PlanItemList")
RegistClassMember(LuaBabySchedulePlanList, "MaxPlanCount")
RegistClassMember(LuaBabySchedulePlanList, "LeftRefreshTimes")

--Baby2023
RegistClassMember(LuaBabySchedulePlanList, "HightestPropertyScore")
RegistClassMember(LuaBabySchedulePlanList, "TopPropertyGo")
RegistClassMember(LuaBabySchedulePlanList, "mTargetQiChang")
RegistClassMember(LuaBabySchedulePlanList, "mTargetQiChangData")

function LuaBabySchedulePlanList:Init()
	self:InitClassMembers()
	self:InitValues()
	self:InitTargetQichang()
end

function LuaBabySchedulePlanList:InitClassMembers()
	self.Anchor = self.transform:Find("Anchor").transform
	self.PlanGrid = self.transform:Find("Anchor/Plans/PlanGrid"):GetComponent(typeof(UIGrid))
	self.PlanTemplate = self.transform:Find("Anchor/Plans/PlanTemplate").gameObject
	self.PlanTemplate:SetActive(false)
	self.MoneyCtrl = self.transform:Find("Anchor/MoneyCtrl"):GetComponent(typeof(CCurentMoneyCtrl))
	self.MoneyCtrl.gameObject:SetActive(false)
	self.LeftRefreshTimeLabel = self.transform:Find("Anchor/LeftRefreshTimeLabel"):GetComponent(typeof(UILabel))
	self.LeftRefreshTimeLabel.text = ""
	self.RefreshButton = self.transform:Find("Anchor/RefreshButton"):GetComponent(typeof(CButton))
	self.BabyPropertyView:SetActive(false)
	self.PropertyTemplate:SetActive(false)
	self.BabyPropertyView_ShaoNian:SetActive(false)
	self.PropertyTemplate_ShaoNian:SetActive(false)
end

function LuaBabySchedulePlanList:InitValues()

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then
		CUIManager.CloseUI(CLuaUIResources.BabySchedulePlanList)
		return
	end

	self.SelectedBaby = selectedBaby
	local setting = Baby_Setting.GetData()
	self.PlanableData = {}
	self.PlanItemList = {}
	self.SelectedPlanIndex = -1
	self.MaxPlanCount = 6
	self.LeftRefreshTimes = 0
	self.BabyPlanDetail.gameObject:SetActive(false)

	local planableData = self.SelectedBaby.Props.ScheduleData.PlanableData
	if not planableData then return end

	CommonDefs.DictIterate(planableData, DelegateFactory.Action_object_object(function(key, val)
		local planId = val.PlanId
		local isUsed = val.Used == 1
		table.insert(self.PlanableData, {
			planId = planId,
			isUsed = isUsed,
			isSelected = self:IsSelected(planId),
			})
	end))

	local usedTime = self.SelectedBaby.Props.ScheduleData.RefreshTimes
	self.LeftRefreshTimes = setting.FreeRefreshCount - usedTime

	self:UpdatePlans()

	self.MoneyCtrl:SetType(4, 0, true) -- 灵玉
	self.MoneyCtrl:SetCost(setting.JadeRefreshCost)

	self:UpdateRefresh()
	self:UpdateBabyProperties()

	local onRefreshButtonClicked = function (go)
		self:OnRefreshButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.RefreshButton.gameObject, DelegateFactory.Action_GameObject(onRefreshButtonClicked), false)

	--[[if LuaBabyMgr.m_AddTargetPos then
		local localPos = self.Anchor.parent:InverseTransformPoint(LuaBabyMgr.m_AddTargetPos)
		self.Anchor.localPosition = Vector3(localPos.x, 0, 0)
	end--]]
end

function LuaBabySchedulePlanList:InitTargetQichang()
	self.mTargetQiChang = 0
	local selectedBabyId = BabyMgr.Inst.SelectedBabyId

	if not selectedBabyId then return end
	Gac2Gas.QueryTargetQiChang(selectedBabyId)
end

function LuaBabySchedulePlanList:IsSelected(planId)
	if not LuaBabyMgr.m_SelectedPlan then return false end

	for k, v in pairs(LuaBabyMgr.m_SelectedPlan) do
		if v == planId then
			return true
		end
	end
	return false
end

function LuaBabySchedulePlanList:GetSelectedPlanCount()
	if not LuaBabyMgr.m_SelectedPlan then return 0 end

	local planCount = 0
 	for k, v in pairs(LuaBabyMgr.m_SelectedPlan) do
 		if v then
 			planCount = planCount + 1
 		end
 	end
 	return planCount
end

-- 有紫色的日程没有派遣
function LuaBabySchedulePlanList:HasPurpleScheduleNotPlan()

	for i = 1, #self.PlanableData do
		if not self.PlanableData[i].isUsed then
			local plan = Baby_Plan.GetData(self.PlanableData[i].planId)
			if plan.Color == 4 then
				return true
			end
		end
	end
	return false
end

function LuaBabySchedulePlanList:UpdatePlans()

	CUICommonDef.ClearTransform(self.PlanGrid.transform)

	-- 最多只能有6个Plan，不然界面会出错
	for i = 1, self.MaxPlanCount do
		if self.PlanableData[i] then
			local go = NGUITools.AddChild(self.PlanGrid.gameObject, self.PlanTemplate)
    		self:InitPlanTemplate(go, self.PlanableData[i], i)
			go:SetActive(true)
			table.insert(self.PlanItemList, go)
		end
	end

	self.PlanGrid:Reposition()
end

function LuaBabySchedulePlanList:UpdateRefresh()
	if self.LeftRefreshTimes > 0 then
		self.LeftRefreshTimeLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]今日剩余免费刷新次数[-] %d"), self.LeftRefreshTimes)
		self.MoneyCtrl.gameObject:SetActive(false)
	else
		self.MoneyCtrl.gameObject:SetActive(true)
		self.LeftRefreshTimeLabel.text = ""
	end
end

function LuaBabySchedulePlanList:InitPlanTemplate(go, info, index)
	local BG = go:GetComponent(typeof(UISprite))
	local PlanLabel = go.transform:Find("PlanLabel"):GetComponent(typeof(UILabel))
	local NeedTiLiLabel = go.transform:Find("NeedTiLiLabel"):GetComponent(typeof(UILabel))
	local BGButton = go:GetComponent(typeof(CButton))
	local LongPressButton = go:GetComponent(typeof(UILongPressButton))
	local ExpResultLabel = go.transform:Find("ExpResultLabel"):GetComponent(typeof(UILabel))
	local PropResultLabel = go.transform:Find("PropResultLabel"):GetComponent(typeof(UILabel))

	local plan = Baby_Plan.GetData(info.planId)
	if not plan then return end

	PlanLabel.text = SafeStringFormat3("[%s]%s-%s[-]", self:GetColor(plan.Color), plan.Name, plan.Description)
	NeedTiLiLabel.text = plan.TiLiCost
	ExpResultLabel.text = SafeStringFormat3(LocalString.GetString("经验+%s"), tostring(plan.Exp))
	if plan.ShowPropDelta == 0 then
		PropResultLabel.text = LocalString.GetString("随机属性")
	else
		PropResultLabel.text = LuaBabyMgr.GetBabyPlanGainStr(plan)
	end
	

	if info.isUsed or info.isSelected then
		BG.alpha = 0.6

		BGButton.enableAnimation = false
		LongPressButton.OnLongPressDelegate = DelegateFactory.Action(function ()
			
		end)
		LongPressButton.OnClickDelegate = DelegateFactory.Action(function ()
			
		end)
		LongPressButton.OnLongPressEndDelegate = DelegateFactory.Action(function ()
			
		end)
	else
		BG.alpha = 1
		
		BGButton.enableAnimation = true

		LongPressButton.OnLongPressDelegate = DelegateFactory.Action(function ()
			self.SelectedPlanIndex = index
			self:UpdateSelections()
			self:UpdatePlanDetailInfo(go, info.planId)
		end)
		LongPressButton.OnClickDelegate = DelegateFactory.Action(function ()
			self.SelectedPlanIndex = index
			self:UpdateSelections()
			self:OnPlanClicked(go, info.planId)
		end)
		LongPressButton.OnLongPressEndDelegate = DelegateFactory.Action(function ()
			
		end)
	end
end

function LuaBabySchedulePlanList:UpdatePlanDetailInfo(go, planId)
	LuaBabyMgr.m_SelectedDetailPlanId = planId
	--LuaBabyMgr.ShowBabySchedulePlanDetail(go.transform)
	self:ShowBabySchedulePlanDetail()
end

function LuaBabySchedulePlanList:UpdateSelections()
	for i = 1, #self.PlanItemList do
		self:SetPlanItemSelected(self.PlanItemList[i], i == self.SelectedPlanIndex)
	end
end

function LuaBabySchedulePlanList:SetPlanItemSelected(item, isSelected)
	local BG = item:GetComponent(typeof(UISprite))
	if isSelected then
		BG.spriteName = "common_btn_07_highlight"
	else
		BG.spriteName = "common_bg_mission_background_s"
	end
end

function LuaBabySchedulePlanList:ShowBabySchedulePlanDetail()
	-- 修正位置
	LuaBabyMgr.m_SelectedDetailPlanPos = nil
	self.BabyPlanDetail.gameObject:SetActive(true)
	self.BabyPlanDetail:Init()
end

function LuaBabySchedulePlanList:OnRefreshButtonClicked(go)
	-- 免费刷新
	if self.LeftRefreshTimes > 0 then
		if self.SelectedBaby then
			-- 如果有已经待派遣的任务，或者还有紫色的任务没有派遣，则二次提示
			if self:GetSelectedPlanCount() > 0 or self:HasPurpleScheduleNotPlan() then
				local msg = g_MessageMgr:FormatMessage("REFRESH_PLAN_CONFIRM")
				MessageWndManager.ShowOKCancelMessage(msg,	DelegateFactory.Action(function()
					Gac2Gas.RequestRefreshBabyPlan(self.SelectedBaby.Id)
				end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
			else
				Gac2Gas.RequestRefreshBabyPlan(self.SelectedBaby.Id)
			end
		end
	else
		if self.MoneyCtrl.moneyEnough then
			if self.SelectedBaby then
				-- 如果有已经待派遣的任务，或者还有紫色的任务没有派遣，则二次提示
				if self:GetSelectedPlanCount() > 0 or self:HasPurpleScheduleNotPlan() then
					local msg = g_MessageMgr:FormatMessage("REFRESH_PLAN_CONFIRM")
					MessageWndManager.ShowOKCancelMessage(msg,	DelegateFactory.Action(function()
						Gac2Gas.RequestRefreshBabyPlan(self.SelectedBaby.Id)
					end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
				else
					Gac2Gas.RequestRefreshBabyPlan(self.SelectedBaby.Id)
				end
			end
		else 
			g_MessageMgr:ShowMessage("NOT_ENOUGH_MONEY_TO_REFRESH_PLAN")
		end
	end
	
end

function LuaBabySchedulePlanList:GetColor(index)
	if index == 1 then
		return "FFFE91"
	elseif index == 2 then
		return "519FFF"
	elseif index == 3 then
		return "FF5050"
	elseif index == 4 then
		return "C000C0"
	end
	return "FFFFFF"
end

function LuaBabySchedulePlanList:OnPlanClicked(go, planId)

	-- 如果是紫色plan，则只能归属抚养人进行派遣
	local plan = Baby_Plan.GetData(planId)

	if plan.Color == 4 and self.SelectedBaby.OwnerId ~= CClientMainPlayer.Inst.Id then
		MessageMgr.Inst:ShowMessage("YANGYU_SCHEDULE_ONLY_OWNER_CAN_USE_PURPLE_PLAN", {})
		return
	else
		g_ScriptEvent:BroadcastInLua("OnSelectBabySchedulePlan", planId)
		CUIManager.CloseUI(CLuaUIResources.BabySchedulePlanList)
	end
	
end

-- 显示宝宝属性
function LuaBabySchedulePlanList:UpdateBabyProperties()
	self.BabyPropertyView:SetActive(false)
	self.BabyPropertyView_ShaoNian:SetActive(false)

	self.BabyNameLabel.text = self.SelectedBaby.Name
	if self.SelectedBaby.Status == EnumBabyStatus.eChild then
		self.BabyPropertyView:SetActive(true)
		self:UpdateBabyProperty()
	elseif self.SelectedBaby.Status == EnumBabyStatus.eYoung then
		self.BabyPropertyView_ShaoNian:SetActive(true)
		self:UpdateBabyProperty_ShaoNian()
	end
end

function LuaBabySchedulePlanList:UpdateBabyProperty()
	CUICommonDef.ClearTransform(self.PropertyGrid.transform)

	if self.SelectedBaby.Status == EnumBabyStatus.eBorn then
		return
	end

	Baby_YouEr.ForeachKey(DelegateFactory.Action_object(function (key)
        local data = Baby_YouEr.GetData(key)
        local go = NGUITools.AddChild(self.PropertyGrid.gameObject, self.PropertyTemplate)
        self:InitBabyPropertyItem(go, key, data)
        go:SetActive(true)
    end))

    self.PropertyGrid:Reposition()
end

function LuaBabySchedulePlanList:InitBabyPropertyItem(go, key, data)
	local PropertyLabel = go.transform:Find("PropertyLabel"):GetComponent(typeof(UILabel))
	local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
	local BG = go.transform:GetComponent(typeof(CUITexture))

	PropertyLabel.text = data.Name
	BG:LoadMaterial(LuaBabyMgr.GetPropBG(key))
	if self.SelectedBaby.Status == EnumBabyStatus.eChild then
		local value = 0
		if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.ChildProp, key) then
			value = self.SelectedBaby.Props.ChildProp[key]
		end
		ValueLabel.text = tostring(value)
	elseif self.SelectedBaby.Status == EnumBabyStatus.eYoung then
		local needValue = {}
		Baby_ShaoNian.ForeachKey(function (id)
			local shaoNian = Baby_ShaoNian.GetData(id)
			if shaoNian.YouErName == key then
				table.insert(needValue, id)
			end
		end)

		local totalValue = 0
		for i = 1, #needValue do
			if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, needValue[i]) then
				totalValue = totalValue + self.SelectedBaby.Props.YoungProp[needValue[i]]
			end
		end
		ValueLabel.text = tostring(totalValue)
	end
end

function LuaBabySchedulePlanList:InitBabyPropertyItem_ShaoNian(go, key, data)
	local MainPropertyLabel = go.transform:Find("MainPropertyLabel"):GetComponent(typeof(UILabel))
	local SubPropertyLabel1 = go.transform:Find("SubPropertyLabel1"):GetComponent(typeof(UILabel))
	local SubPropertyLabel2 = go.transform:Find("SubPropertyLabel2"):GetComponent(typeof(UILabel))
	local BG = go.transform:GetComponent(typeof(CUITexture))
	local RectBG = go.transform:Find("RectBG"):GetComponent(typeof(UISprite))
	local Line = go.transform:Find("Line"):GetComponent(typeof(UISprite))
	local NodeTexture = go.transform:Find("NodeTexture"):GetComponent(typeof(UITexture))
	--baby2023
	local TopTag = go.transform:Find("TopTag")
	local BelowStandardTag1 = go.transform:Find("BelowStandardTag1")
	local BelowStandardTag2 = go.transform:Find("BelowStandardTag2")

	MainPropertyLabel.text = data.Name
	BG:LoadMaterial(LuaBabyMgr.GetPropBG(key))

	local subKey1 = key * 2 - 1
	local subKey2 = key * 2
	local score = 0

	local subData1 = Baby_ShaoNian.GetData(subKey1)
	if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, subData1.ID) then

		local color = self:GetPropertyTextColor(key)

		if BelowStandardTag1 and BelowStandardTag2 then--是否是迭代后界面
			if self.mTargetQiChang == 0 or self.mTargetQiChang == nil then
				BelowStandardTag1.gameObject:SetActive(false)
			else
				local request = self:GetQiChangRequestPropByIndex(subData1.ID, self.mTargetQiChangData)
				local own = self.SelectedBaby.Props.YoungProp[subData1.ID]
				if own < request then
					BelowStandardTag1.gameObject:SetActive(true)
				else
					if request == 0 then
						color = self:GetPropertyTextColorNotTarget(key)
					end
					BelowStandardTag1.gameObject:SetActive(false)
				end
			end
		end
		SubPropertyLabel1.text = SafeStringFormat3("[%s]%s %s[-]", color, subData1.Name, tostring(self.SelectedBaby.Props.YoungProp[subData1.ID]))
		score = score + self.SelectedBaby.Props.YoungProp[subData1.ID]
	else
		SubPropertyLabel1.text = ""
	end
	
	local subData2 = Baby_ShaoNian.GetData(subKey2)
	if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, subData2.ID) then

		local color = self:GetPropertyTextColor(key)

		if BelowStandardTag1 and BelowStandardTag2 then--是否是迭代后界面
			if self.mTargetQiChang == 0 or self.mTargetQiChang == nil then
				BelowStandardTag2.gameObject:SetActive(false)
			else
				local request = self:GetQiChangRequestPropByIndex(subData2.ID, self.mTargetQiChangData)
				local own = self.SelectedBaby.Props.YoungProp[subData2.ID]
				if own < request then
					BelowStandardTag2.gameObject:SetActive(true)
				else
					if request == 0 then
						color = self:GetPropertyTextColorNotTarget(key)
					end
					BelowStandardTag2.gameObject:SetActive(false)
				end
			end
		end
		SubPropertyLabel2.text = SafeStringFormat3("[%s]%s %s[-]", color, subData2.Name, tostring(self.SelectedBaby.Props.YoungProp[subData2.ID]))
		score = score + self.SelectedBaby.Props.YoungProp[subData2.ID]
	else
		SubPropertyLabel2.text = ""
	end

	Line.color = NGUIText.ParseColor24(self:GetPropertyTextColor(key), 0)
	Line.alpha = 0.7
	RectBG.color = NGUIText.ParseColor24(self:GetBGColor(key), 0)
	RectBG.alpha = 0.5
	NodeTexture.color = NGUIText.ParseColor24(self:GetPropertyTextColor(key), 0)

	--养育迭代2023
	if score >= self.HightestPropertyScore then
		self.TopPropertyGo = go
		self.HightestPropertyScore = score
	end

	if TopTag then
		TopTag.gameObject:SetActive(false)
	end
end

function LuaBabySchedulePlanList:UpdateBabyProperty_ShaoNian()
	CUICommonDef.ClearTransform(self.PropertyGrid_ShaoNian.transform)

	if self.SelectedBaby.Status == EnumBabyStatus.eBorn then
		return
	end

	self.HightestPropertyScore = 0
	self.TopPropertyGo = nil
	Baby_YouEr.ForeachKey(DelegateFactory.Action_object(function (key)
        local data = Baby_YouEr.GetData(key)
        local go = NGUITools.AddChild(self.PropertyGrid_ShaoNian.gameObject, self.PropertyTemplate_ShaoNian)
        self:InitBabyPropertyItem_ShaoNian(go, key, data)
        go:SetActive(true)
    end))
	if self.TopPropertyGo then
		local topTag = self.TopPropertyGo.transform:Find("TopTag")
		if topTag then
			topTag.gameObject:SetActive(true)
		end
	end

	self.PropertyGrid_ShaoNian:Reposition()
end

function LuaBabySchedulePlanList:GetQiChangRequestPropByIndex(index, qichang)
	if index == 1 then
		return qichang.Strategy
	elseif index == 2 then
		return qichang.Taoism
	elseif index == 3 then
		return qichang.Martial
	elseif index == 4 then
		return qichang.Military
	elseif index == 5 then
		return qichang.Poetry
	elseif index == 6 then
		return qichang.Yayi
	elseif index == 7 then
		return qichang.Heaven
	elseif index == 8 then
		return qichang.Process
	elseif index == 9 then
		return qichang.Virtue
	elseif index == 10 then
		return qichang.Ambiguity
	elseif index == 11 then
		return qichang.Tyrannical
	elseif index == 12 then
		return qichang.Evil
	end
	return ""
end

function LuaBabySchedulePlanList:GetBGColor(key)
	if key == 1 then
		return "665496"
	elseif key == 2 then
		return "99864d"
	elseif key == 3 then
		return "4990a6"
	elseif key == 4 then
		return "50a778"
	elseif key == 5 then
		return "9948a7"
	elseif key == 6 then
		return "9d494d"
	end
	return "FFFFFF"
end

function LuaBabySchedulePlanList:GetPropertyTextColor(key)
	if key == 1 then
		return "ffa2ff"
	elseif key == 2 then
		return "f2f292"
	elseif key == 3 then
		return "99dcfe"
	elseif key == 4 then
		return "B6FFDB"
	elseif key == 5 then
		return "ffb2ff"
	elseif key == 6 then
		return "ff9393"
	end
	return "FFFFFF"
end

-- 不为目标属性时的颜色
function LuaBabySchedulePlanList:GetPropertyTextColorNotTarget(key)
	if key == 1 then
		return "B06BB3"
	elseif key == 2 then
		return "B3B363"
	elseif key == 3 then
		return "5BABD4"
	elseif key == 4 then
		return "5DCB94"
	elseif key == 5 then
		return "B27AB2"
	elseif key == 6 then
		return "B76F6F"
	end
	return "FFFFFF"
end

-- end 显示宝宝属性

function LuaBabySchedulePlanList:OnSyncTargetQiChangInfo(babyId, targetQiChangId, recommendQiChangId)
	print("OnSyncTargetQiChangInfo",babyId, targetQiChangId, recommendQiChangId)
	self.mTargetQiChang = targetQiChangId
	self.mTargetQiChangData = Baby_QiChang.GetData(targetQiChangId)
	self:UpdateBabyProperty_ShaoNian()
end

function LuaBabySchedulePlanList:OnEnable()
	g_ScriptEvent:AddListener("RefreshBabyPlanSuccess", self, "InitValues")
	--2023
	g_ScriptEvent:AddListener("SyncTargetQiChangInfo", self, "OnSyncTargetQiChangInfo")
end

function LuaBabySchedulePlanList:OnDisable()
	g_ScriptEvent:RemoveListener("RefreshBabyPlanSuccess", self, "InitValues")
	--2023
	g_ScriptEvent:RemoveListener("SyncTargetQiChangInfo", self, "OnSyncTargetQiChangInfo")
end

return LuaBabySchedulePlanList
