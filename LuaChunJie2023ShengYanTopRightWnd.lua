local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaChunJie2023ShengYanTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023ShengYanTopRightWnd, "rightBtn", GameObject)
RegistChildComponent(LuaChunJie2023ShengYanTopRightWnd, "Bg", GameObject)
RegistChildComponent(LuaChunJie2023ShengYanTopRightWnd, "tipNodePlayer", GameObject)

RegistClassMember(LuaChunJie2023ShengYanTopRightWnd, "rankTipNodes")
--@endregion RegistChildComponent end

function LuaChunJie2023ShengYanTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.rankTipNodes = {}
    for i = 1, 4 do
        local tipnode = self.transform:Find("Anchor/tipPanel/node/tipNode" .. tostring(i)).gameObject
        table.insert(self.rankTipNodes, tipnode)
    end

    UIEventListener.Get(self.rightBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)
    UIEventListener.Get(self.Bg).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankListClick()
	end)
end

function LuaChunJie2023ShengYanTopRightWnd:Init()
    self:SyncTTSYPlayState()
end

function LuaChunJie2023ShengYanTopRightWnd:SyncTTSYPlayState()
    self:UpdateTipNodeInfo(self.tipNodePlayer, LuaChunJie2023Mgr.TTSY_MainPlayerRankInfo)
    -- 前四名情况
    for i = 1, #self.rankTipNodes do
        if LuaChunJie2023Mgr.TTSY_RankTbl[i] then
            self.rankTipNodes[i].gameObject:SetActive(true)
            self:UpdateTipNodeInfo(self.rankTipNodes[i], LuaChunJie2023Mgr.TTSY_RankTbl[i])
        else
            self.rankTipNodes[i].gameObject:SetActive(false)
        end
    end
end

function LuaChunJie2023ShengYanTopRightWnd:UpdateTipNodeInfo(tipNode, info)
    if info == nil or tipNode == nil then return end
    
    tipNode.transform:Find("text"):GetComponent(typeof(UILabel)).text = info.Name
    tipNode.transform:Find("num"):GetComponent(typeof(UILabel)).text = info.Score

    -- 显示排名
    local rankImgList = {g_sprites.Common_Huangguan_01, g_sprites.Common_Huangguan_02, g_sprites.Common_Huangguan_03}
    local node = tipNode.transform:Find("node"):GetComponent(typeof(UISprite))
    local label = tipNode.transform:Find("Label"):GetComponent(typeof(UILabel))
    if info.Rank <= 3 then
        node.gameObject:SetActive(true)
        label.gameObject:SetActive(false)
        node.spriteName = rankImgList[info.Rank]
    else
        node.gameObject:SetActive(false)
        label.gameObject:SetActive(true)
        label.text = tostring(info.Rank)
    end
end

--@region UIEvent
function LuaChunJie2023ShengYanTopRightWnd:OnRightBtnClick()
    self.rightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
function LuaChunJie2023ShengYanTopRightWnd:OnHideTopAndRightTipWnd()
    self.rightBtn.transform.localEulerAngles = Vector3(0, 0, -180)
end
function LuaChunJie2023ShengYanTopRightWnd:OnRankListClick()
    CUIManager.ShowUI(CLuaUIResources.ChunJie2023ShengYanRankWnd)
end
--@endregion UIEvent

function LuaChunJie2023ShengYanTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncTTSYPlayState",self,"SyncTTSYPlayState")
end

function LuaChunJie2023ShengYanTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncTTSYPlayState",self,"SyncTTSYPlayState")
end