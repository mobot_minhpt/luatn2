require("common/common_include")

local CUITexture = import "L10.UI.CUITexture"
local BabyMgr = import "L10.Game.BabyMgr"

LuaBabyFashionChooseWnd = class()

RegistChildComponent(LuaBabyFashionChooseWnd, "Left", GameObject)
RegistChildComponent(LuaBabyFashionChooseWnd, "LeftPortrait", CUITexture)
RegistChildComponent(LuaBabyFashionChooseWnd, "LeftLevelLabel", UILabel)
RegistChildComponent(LuaBabyFashionChooseWnd, "LeftNameLabel", UILabel)
RegistChildComponent(LuaBabyFashionChooseWnd, "LeftLight", GameObject)

RegistChildComponent(LuaBabyFashionChooseWnd, "Right", GameObject)
RegistChildComponent(LuaBabyFashionChooseWnd, "RightPortrait", CUITexture)
RegistChildComponent(LuaBabyFashionChooseWnd, "RightLevelLabel", UILabel)
RegistChildComponent(LuaBabyFashionChooseWnd, "RightNameLabel", UILabel)
RegistChildComponent(LuaBabyFashionChooseWnd, "RightLight", GameObject)

RegistClassMember(LuaBabyFashionChooseWnd, "LeftBaby")
RegistClassMember(LuaBabyFashionChooseWnd, "RightBaby")

function LuaBabyFashionChooseWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyFashionChooseWnd:InitClassMembers()
	self.LeftLight:SetActive(false)
	self.RightLight:SetActive(false)
end

function LuaBabyFashionChooseWnd:InitValues()
	if not LuaBabyMgr.m_PreviewFashionBabyTable or #LuaBabyMgr.m_PreviewFashionBabyTable <= 1 then
		CUIManager.CloseUI(CLuaUIResources.BabyFashionChooseWnd)
		return
	end
	self.LeftBaby = LuaBabyMgr.m_PreviewFashionBabyTable[1]
	self.RightBaby = LuaBabyMgr.m_PreviewFashionBabyTable[2]
	LuaBabyMgr.m_PreviewFashionBabyTable = nil

	local portraint1 = BabyMgr.Inst:GetBabyPortraitById(self.LeftBaby.Id)
	self.LeftPortrait:LoadNPCPortrait(portraint1, false)
	self.LeftLevelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(self.LeftBaby.Grade))
	self.LeftNameLabel.text = self.LeftBaby.Name

	local portraint2 = BabyMgr.Inst:GetBabyPortraitById(self.RightBaby.Id)
	self.RightPortrait:LoadNPCPortrait(portraint2, false)
	self.RightLevelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(self.RightBaby.Grade))
	self.RightNameLabel.text = self.RightBaby.Name

	CommonDefs.AddOnClickListener(self.Left, DelegateFactory.Action_GameObject(function (go)
		self:OnLeftBabyClicked(go)
	end), false)

	CommonDefs.AddOnClickListener(self.Right, DelegateFactory.Action_GameObject(function (go)
		self:OnRightBabyClicked(go)
	end), false)
end

function LuaBabyFashionChooseWnd:OnLeftBabyClicked(go)
	self.LeftLight:SetActive(true)
	self.RightLight:SetActive(false)

	LuaBabyMgr.m_PreviewFashionBaby = self.LeftBaby
	CUIManager.ShowUI(CLuaUIResources.ShopMallBabyFashionPreviewWnd)
	CUIManager.CloseUI(CLuaUIResources.BabyFashionChooseWnd)
end

function LuaBabyFashionChooseWnd:OnRightBabyClicked(go)
	self.RightLight:SetActive(true)
	self.LeftLight:SetActive(false)

	LuaBabyMgr.m_PreviewFashionBaby = self.RightBaby
	CUIManager.ShowUI(CLuaUIResources.ShopMallBabyFashionPreviewWnd)
	CUIManager.CloseUI(CLuaUIResources.BabyFashionChooseWnd)
end

function LuaBabyFashionChooseWnd:OnEnable()
	--g_ScriptEvent:AddListener("UpdateBabyGradeAndExp", self, "UpdateBabyGradeAndExp")
end

function LuaBabyFashionChooseWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("UpdateBabyGradeAndExp", self, "UpdateBabyGradeAndExp")
end

return LuaBabyFashionChooseWnd