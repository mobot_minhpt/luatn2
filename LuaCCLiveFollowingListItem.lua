require("common/common_include")

local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CButton = import "L10.UI.CButton"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local Constants = import "L10.Game.Constants"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"

LuaCCLiveFollowingListItem = class()
RegistClassMember(LuaCCLiveFollowingListItem,"m_ZhuboPortrait")
RegistClassMember(LuaCCLiveFollowingListItem,"m_ZhuboNameLabel")
RegistClassMember(LuaCCLiveFollowingListItem,"m_ZhuboTagLabel")
RegistClassMember(LuaCCLiveFollowingListItem,"m_FollowingButton")
RegistClassMember(LuaCCLiveFollowingListItem,"m_FollowingSprite")
RegistClassMember(LuaCCLiveFollowingListItem,"m_LeaveIcon")
RegistClassMember(LuaCCLiveFollowingListItem,"m_CCID")
RegistClassMember(LuaCCLiveFollowingListItem,"m_ZhuBoInfo")
RegistClassMember(LuaCCLiveFollowingListItem,"m_Tex")


function LuaCCLiveFollowingListItem:InitByZhuboInfo(info)
	self.m_ZhuboPortrait = self.transform:Find("Head/Portrait"):GetComponent(typeof(UITexture))
	self.m_ZhuboNameLabel = self.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	self.m_ZhuboTagLabel = self.transform:Find("TagLabel"):GetComponent(typeof(UILabel))
	self.m_FollowingButton = self.transform:Find("FollowingBtn"):GetComponent(typeof(CButton))
	self.m_FollowingSprite = self.transform:Find("FollowingBtn/Sprite"):GetComponent(typeof(UISprite))
	self.m_LeaveIcon = self.transform:Find("Head/LeaveIcon").gameObject

    self.m_ZhuBoInfo = info
	self.m_CCID = info and info.ccid or 0
	if self.m_Tex == nil then 
        self.m_Tex = Texture2D(128, 128, TextureFormat.RGBA32, false)
    end
	self.m_ZhuboPortrait.mainTexture = nil
    local station = CCCChatMgr.Inst:GetStationByCCID(self.m_CCID) -- 有可能不存在
    if info then
        self.m_ZhuboNameLabel.text = info.nickname
        self.m_LeaveIcon:SetActive(station and not station.liveInfo.isLive)
        self.m_ZhuboTagLabel.text = info.anchor_tag

        local bytes = CCCChatMgr.Inst:GetCCPortraitImage(info.purl)
        if bytes then
            CommonDefs.LoadImage(self.m_Tex, bytes)
            self.m_ZhuboPortrait.mainTexture = self.m_Tex
        else
            CCCChatMgr.Inst:DownloadCCPortraitImage(info.purl)
        end
    else
        self.m_ZhuboNameLabel.text = nil
        self.m_ZhuboTagLabel.text = nil
        self.m_LeaveIcon:SetActive(false)
        self.m_ZhuboPortrait.mainTexture = nil
    end
    self:OnCCFollowingUpdate()
    CUICommonDef.SetGrey(self.m_ZhuboPortrait.gameObject, not (station and not station.liveInfo.isLive))

    CommonDefs.AddOnClickListener(self.m_FollowingButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnFollowingButtonClick() end), false)
end

function LuaCCLiveFollowingListItem:OnFollowingButtonClick()
    LuaCCChatMgr.DoChangeCCFollowing(self.m_CCID)
end

function LuaCCLiveFollowingListItem:OnEnable()
	g_ScriptEvent:AddListener("OnCCFollowingUpdate", self, "OnCCFollowingUpdate")
	g_ScriptEvent:AddListener("OnZhuboPortraitReady", self, "OnZhuboPortraitReady")
end

function LuaCCLiveFollowingListItem:OnDisable()
	g_ScriptEvent:RemoveListener("OnCCFollowingUpdate", self, "OnCCFollowingUpdate")
	g_ScriptEvent:RemoveListener("OnZhuboPortraitReady", self, "OnZhuboPortraitReady")
end

function LuaCCLiveFollowingListItem:OnCCFollowingUpdate(args)
	if not self.m_FollowingButton then return end
    local isFollowing = CCCChatMgr.Inst:IsCCFollowing(self.m_CCID)
    self.m_FollowingButton.Text = self.m_ZhuBoInfo and tostring(self.m_ZhuBoInfo.followed) or ""
    self.m_FollowingSprite.spriteName = isFollowing and Constants.CCFollowingIcon or Constants.CCUnFollowingIcon
end

function LuaCCLiveFollowingListItem:OnZhuboPortraitReady(args)
	if not self.m_ZhuboPortrait then return end
	local url = args and args[0] or nil
    if self.m_ZhuBoInfo ~= nil and self.m_ZhuBoInfo.purl == url then
        local bytes = CCCChatMgr.Inst:GetCCPortraitImage(url)
        if bytes then
            CommonDefs.LoadImage(self.m_Tex, bytes)
            self.m_ZhuboPortrait.mainTexture = self.m_Tex
        end
    end
end

function LuaCCLiveFollowingListItem:OnDestroy()
	if not self.m_Tex then
		self.m_Tex = nil
	end
end
