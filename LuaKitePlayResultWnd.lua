require("3rdParty/ScriptEvent")
require("common/common_include")

local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIRes = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIManager = import "L10.UI.CUIManager"
local CLoginMgr = import "L10.Game.CLoginMgr"

CLuaKitePlayResultWnd=class()
RegistClassMember(CLuaKitePlayResultWnd,"CloseBtn")
RegistClassMember(CLuaKitePlayResultWnd,"ShareBtn")
RegistClassMember(CLuaKitePlayResultWnd,"RankBtn")
RegistClassMember(CLuaKitePlayResultWnd,"PlayerInfoNode")
RegistClassMember(CLuaKitePlayResultWnd,"RotateNode")
RegistClassMember(CLuaKitePlayResultWnd,"ScoreLabel")
RegistClassMember(CLuaKitePlayResultWnd,"TimeLabel")

function CLuaKitePlayResultWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.KitePlayResultWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onShareClick = function(go)
		CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.ShareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

	local onRankClick = function(go)
	end

	CommonDefs.AddOnClickListener(self.RankBtn,DelegateFactory.Action_GameObject(onRankClick),false)

	self.ScoreLabel.text = LuaKitePlayMgr.ResultScore
	local min = math.floor(LuaKitePlayMgr.ResultTime / 60)
	local sec = math.ceil(LuaKitePlayMgr.ResultTime - min * 60)
	if sec < 10 then
		sec = '0' .. sec
	end
	self.TimeLabel.text = min .. ':' .. sec
	if CClientMainPlayer.Inst then
		self.PlayerInfoNode:SetActive(true)
		self.PlayerInfoNode.transform:Find("name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
	  local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
	  self.PlayerInfoNode.transform:Find("server"):GetComponent(typeof(UILabel)).text = myGameServer.name
	  self.PlayerInfoNode.transform:Find("ID"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Id
	  self.PlayerInfoNode.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender))
	  self.PlayerInfoNode.transform:Find("icon/lv"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Level
	else
		self.PlayerInfoNode:SetActive(false)
	end

end

return CLuaKitePlayResultWnd
