-- Auto Generated!!
local CKouDaoSituationRankListItem = import "L10.UI.CKouDaoSituationRankListItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuSituationDetailView = import "L10.UI.CQingQiuSituationDetailView"
local EnumClass = import "L10.Game.EnumClass"
local EnumStatType = import "L10.Game.EnumStatType"
CQingQiuSituationDetailView.m_Init_CS2LuaHook = function (this, type) 

    this.curType = type
    this.tableView:Clear()
    this.tableView:LoadData(0, false)
end
CQingQiuSituationDetailView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    if index < 0 and index >= CQingQiuMgr.Inst.m_FightData.Count then
        return nil
    end
    local cellIdentifier = "RankItemCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.rankItemTemplate, cellIdentifier)
    end
    local info = CQingQiuMgr.Inst.m_FightData[index]
    if this.curType == EnumStatType.eDps then
        CommonDefs.GetComponent_GameObject_Type(cell, typeof(CKouDaoSituationRankListItem)):Init(index + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.m_PlayerClazz), info.m_PlayerName, info.m_PlayerDamage, CQingQiuMgr.Inst.m_TotalDamage > 0 and (info.m_PlayerDamage / CQingQiuMgr.Inst.m_TotalDamage) or 0)
    elseif this.curType == EnumStatType.eHeal then
        CommonDefs.GetComponent_GameObject_Type(cell, typeof(CKouDaoSituationRankListItem)):Init(index + 1, CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.m_PlayerClazz), info.m_PlayerName, info.m_PlayerHeal, CQingQiuMgr.Inst.m_TotalHeal > 0 and (info.m_PlayerHeal / CQingQiuMgr.Inst.m_TotalHeal) or 0)
    end

    return cell
end
