EFlowEvent =
{   
    Start = 1,
    Tick = 2,
    CustomEvent = 3,
}

EFlowResult =
{
    Failure = 0,
    Success = 1,
}

FlowEventArgsCount =
{   
    0,  --Start
    0,  --Tick
    4,  --CustomEvent
}

EFlowClass =
{   
    Global = 1,
    ClientAINpc = 2,
    Position = 3,
}

FlowObjectClass =
{   
    [1] = {"Global", { }},
    [2] = {"ClientAINpc", { }},
    [3] = {"Position", { }},
}

FlowOutputPrefix = "Flowchart_" --解析生成的lua文件的前缀，如Flowchart_AI,Flowchart_Gameplay"
