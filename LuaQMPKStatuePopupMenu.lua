require("3rdParty/ScriptEvent")
require("common/common_include")

local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CMainCamera=import "L10.Engine.CMainCamera"
local LuaGameObject=import "LuaGameObject"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"

LuaQMPKStatuePopupMenu=class()

RegistClassMember(LuaQMPKStatuePopupMenu,"m_Button1") 	-- 目前隐藏
RegistClassMember(LuaQMPKStatuePopupMenu,"m_Button2")	-- 目前隐藏
RegistClassMember(LuaQMPKStatuePopupMenu,"m_Button3")	-- 雕像界面
RegistClassMember(LuaQMPKStatuePopupMenu,"m_RotateButton")	-- 旋转按钮
RegistClassMember(LuaQMPKStatuePopupMenu,"m_DeleteButton")  -- 回收按钮

RegistClassMember(LuaQMPKStatuePopupMenu,"m_CachedPos")
RegistClassMember(LuaQMPKStatuePopupMenu,"m_Anchor")
RegistClassMember(LuaQMPKStatuePopupMenu,"m_LastPos")

function LuaQMPKStatuePopupMenu:Init()

    local engineId =  CQuanMinPKMgr.Inst.SelectedStatueEngineId
    local statue = CClientObjectMgr.Inst:GetObject(engineId)
    if not engineId or not statue then
        CUIManager.CloseUI(CUIResources.QMPKStatuePopupMenu)
        return
    end
    CQuanMinPKMgr.Inst.CurrentStatue=statue
    self:InitClassMember()

end

function LuaQMPKStatuePopupMenu:InitClassMember()

	self.m_Anchor=LuaGameObject.GetChildNoGC(self.transform,"Anchor").gameObject
    self.m_CachedPos=Vector3(0,0,0)
    self.m_Button1=self.transform:Find("Anchor/table/Button1").gameObject
    self.m_Button2=self.transform:Find("Anchor/table/Button2").gameObject
    self.m_Button3=self.transform:Find("Anchor/table/Button3").gameObject
    self.m_RotateButton=self.transform:Find("Anchor/table/RotateButton").gameObject
    self.m_DeleteButton=self.transform:Find("Anchor/table/DeleteButton").gameObject

    local onBuildStatue = function (go)
		self:OnBuildStatue(go)
	end
	CommonDefs.AddOnClickListener(self.m_Button3,DelegateFactory.Action_GameObject(onBuildStatue),false)

	local onRotate = function (go)
		self:OnRotateStatue(go)
	end
	CommonDefs.AddOnClickListener(self.m_RotateButton,DelegateFactory.Action_GameObject(onRotate),false)

	local onDelete = function (go)
		self:OnDeleteStatue(go)
	end
	CommonDefs.AddOnClickListener(self.m_DeleteButton,DelegateFactory.Action_GameObject(onDelete),false)
end

-- 打开雕像界面
function LuaQMPKStatuePopupMenu:OnBuildStatue(go)
	Gac2Gas.RequestOpenChangeChampionStatueWnd(CQuanMinPKMgr.Inst.SelectedStatueEngineId)
end

-- 旋转雕像
function LuaQMPKStatuePopupMenu:OnRotateStatue(go)
	-- body
	local engineId = CQuanMinPKMgr.Inst.SelectedStatueEngineId
	local x = CQuanMinPKMgr.Inst.CurrentStatue.WorldPos.x
	local y = CQuanMinPKMgr.Inst.CurrentStatue.WorldPos.z
	local dir = CQuanMinPKMgr.Inst.CurrentStatue.Dir
	dir = (dir + 90) % 360
	Gac2Gas.ReqeustMoveChampionStatue(CQuanMinPKMgr.Inst.SelectedStatueEngineId, x, y, tonumber(dir))
end

-- 收回雕像
function LuaQMPKStatuePopupMenu:OnDeleteStatue(go)
	Gac2Gas.ReqeustPackChampionStatue(CQuanMinPKMgr.Inst.SelectedStatueEngineId)
end


function LuaQMPKStatuePopupMenu:OnEnable()
	--g_ScriptEvent:AddListener("RequestExchangeQianYuanSuccess", self, "Init")
end

function LuaQMPKStatuePopupMenu:OnDisable()
	--g_ScriptEvent:RemoveListener("RequestExchangeQianYuanSuccess", self, "Init")
end

function LuaQMPKStatuePopupMenu:Update()
    local statue = CQuanMinPKMgr.Inst.CurrentStatue
    if not statue then return end
    local RO = nil
    if statue then
        RO = statue.RO
    end
    if not RO then return end
    
    local pos = RO.TopAnchorPos
    local rootPos = RO.Position
    --计算
    --pos = Vector3((rootPos.x+pos.x)/2,(rootPos.y+pos.y)/2,(rootPos.z+pos.z)/2)
    local screenPos = CMainCamera.Main:WorldToScreenPoint(pos)
    if self.m_CachedPos.x == screenPos.x and self.m_CachedPos.y==screenPos.y then
        return
    end
    screenPos.z = 0
    self.m_CachedPos = screenPos
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    self.m_Anchor.transform.position = nguiWorldPos
end


return LuaQMPKStatuePopupMenu