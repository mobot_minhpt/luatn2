local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CGuideBubbleSizeCtl = import "L10.UI.CGuideBubbleSizeCtl"
local Physics = import "UnityEngine.Physics"
local LayerDefine = import "L10.Engine.LayerDefine"
local CRenderObject = import "L10.Engine.CRenderObject"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CMainCamera = import "L10.Engine.CMainCamera"

LuaPetCatWnd = class()

RegistChildComponent(LuaPetCatWnd,"m_PetCatRoot1","PetCatRoot1", UIWidget)
RegistChildComponent(LuaPetCatWnd,"m_PetCatRoot2","PetCatRoot2", UIWidget)

RegistClassMember(LuaPetCatWnd, "m_Bubble")
RegistClassMember(LuaPetCatWnd, "m_OnSwipeDelegate")
RegistClassMember(LuaPetCatWnd, "m_Area")
RegistClassMember(LuaPetCatWnd, "m_PetCatTick")

function LuaPetCatWnd:OnEnable()
    self:InitCamera()
    g_ScriptEvent:AddListener("ClientObjDestroy", self, "OnClientObjDestroy")
end

function LuaPetCatWnd:OnDisable()
    self:CancelPetCatTick()
    self:EndMoveCamera()
    g_ScriptEvent:RemoveListener("ClientObjDestroy", self, "OnClientObjDestroy")
end

function LuaPetCatWnd:OnClientObjDestroy(args)
    CUIManager.CloseUI(CLuaUIResources.PetCatWnd)
end

function LuaPetCatWnd:Init()
    self.m_PetCatRoot1.gameObject:SetActive(false)
    self.m_PetCatRoot2.gameObject:SetActive(false)
    self.m_Area = (CLuaTaskMgr.m_PetCat_Type == 1) and self.m_PetCatRoot1 or self.m_PetCatRoot2
    UIEventListener.Get(self.m_Area.gameObject).onDragStart = LuaUtils.VoidDelegate(function(go)
        self:OnDragStart()
    end)
    UIEventListener.Get(self.m_Area.gameObject).onDragEnd = LuaUtils.VoidDelegate(function(go)
        self:OnDragEnd()
    end)
    self.m_TargetTemplateId = ZhuJueJuQing_Setting.GetData().PetCatData_CatNpcTemplateID
    self.m_Bubble = self.m_Area.transform:Find("Bubble"):GetComponent(typeof(CGuideBubbleSizeCtl))
    local msg = g_MessageMgr:FormatMessage((CLuaTaskMgr.m_PetCat_Type == 1) and "Pet_Cat_Cheek" or "Pet_BackOfCat")
    self.m_Bubble:Init(msg)
end

function LuaPetCatWnd:InitCamera()
    if CameraFollow.Inst then
        local cameraData = ZhuJueJuQing_Setting.GetData().PetCatData_Camera
        CameraFollow.Inst:BeginAICamera(Vector3(0, 0, 0), Vector3(0, 0, 0), 3600, false)
        local index = CLuaTaskMgr.m_PetCat_Type * 6 - 6
        CameraFollow.Inst:AICameraMoveTo(
                Vector3(cameraData[index], cameraData[index + 1], cameraData[index + 2]),
                Vector3(cameraData[index + 3], cameraData[index + 4], cameraData[index + 5]),1)
        RegisterTick(function ()
            if self.m_PetCatRoot1 then
                self.m_PetCatRoot1.gameObject:SetActive(CLuaTaskMgr.m_PetCat_Type == 1)
            end
            if self.m_PetCatRoot2 then
                self.m_PetCatRoot2.gameObject:SetActive(CLuaTaskMgr.m_PetCat_Type == 2)
            end
        end,1500)
    end
end

function LuaPetCatWnd:EndMoveCamera()
    CameraFollow.Inst:EndAICamera()
end

function LuaPetCatWnd:OnDragStart()
    local screenPos = Vector3(Input.mousePosition.x,Input.mousePosition.y,1)
    if not self:CheckPetCat(screenPos) then return end
    self.m_PetCatTick = RegisterTickOnce(function()
        self:EndPetCat()
    end,1000)
end

function LuaPetCatWnd:OnDragEnd()
    if self.m_PetCatTick then self:CancelPetCatTick() end
end

function LuaPetCatWnd:EndPetCat()
    self:CancelPetCatTick()
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_PetCatWnd_TaskId,(CLuaTaskMgr.m_PetCat_Type == 1) and "PetCat1" or "PetCat2")
    CUIManager.CloseUI(CLuaUIResources.PetCatWnd)
end

function LuaPetCatWnd:CancelPetCatTick()
    if self.m_PetCatTick then
        UnRegisterTick(self.m_PetCatTick)
        self.m_PetCatTick = nil
    end
end

function LuaPetCatWnd:CheckPetCat(screenPos)
    local mainCamera = CMainCamera.Main
    if not mainCamera then return end
    local ray = mainCamera:ScreenPointToRay(screenPos)
    local dist = mainCamera.farClipPlane - mainCamera.nearClipPlane
    local hits = Physics.RaycastAll(ray, dist, bit.lshift(1,LayerDefine.NPC))
    for i = 0, hits.Length - 1 do
        local collider = hits[i].collider.gameObject
        local p = collider.transform.parent
        if p then
            local ro = p.gameObject:GetComponent(typeof(CRenderObject))
            if ro and CClientObjectMgr.Inst then
                local co = CClientObjectMgr.Inst:GetObject(ro)
                if co then
                    local npc = TypeAs(CClientObjectMgr.Inst:GetObject(co.EngineId), typeof(CClientNpc))
                    if self.m_TargetTemplateId == npc.TemplateId then
                        return true
                    end
                end
            end
        end
    end
end