require("3rdParty/ScriptEvent")
require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CRealNameCheckMgr = import "L10.Game.CRealNameCheckMgr"
local UIPanel = import "UIPanel"

CLuaRealNamePreWnd =class()

function CLuaRealNamePreWnd:ctor()
end

function CLuaRealNamePreWnd:Init( tf )
	self.transform:GetComponent(typeof(UIPanel)).depth = 100000

	local g = LuaGameObject.GetChildNoGC(self.transform,"EnsureBtn").gameObject
	UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
		CUIManager.CloseUI(CUIResources.RealNamePreWnd)
		CUIManager.ShowUI(CUIResources.RealNameCheckWnd)
	end)

	if CRealNameCheckMgr.EnableRealNameCheck then
		LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject:SetActive(false)
	else
		local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
		g:SetActive(true)
		UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
			CUIManager.CloseUI(CUIResources.RealNamePreWnd)
		end)
	end
	
	
end

return CLuaRealNamePreWnd