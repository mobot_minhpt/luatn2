local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIRoot = import "UIRoot"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIFx = import "L10.UI.CUIFx"
local LocalString = import "LocalString"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local CHorseRaceMgr = import "L10.Game.CHorseRaceMgr"
local Ease = import "DG.Tweening.Ease"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"

LuaNewHorseRaceBaseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaNewHorseRaceBaseWnd, "m_ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaNewHorseRaceBaseWnd, "m_TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaNewHorseRaceBaseWnd, "m_RankLabel", "RankLabel", UILabel)
RegistChildComponent(LuaNewHorseRaceBaseWnd, "m_Profress", "Progress", UISlider)
RegistChildComponent(LuaNewHorseRaceBaseWnd, "m_SpeedLab", "SpeedLab", UILabel)
RegistChildComponent(LuaNewHorseRaceBaseWnd, "m_Grid", "Grid", GameObject)
RegistChildComponent(LuaNewHorseRaceBaseWnd, "m_Slider", "Slider", UISlider)

RegistClassMember(LuaNewHorseRaceBaseWnd, "m_FxStartGo")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_FxEndGo")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_YellowIcons")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_RedIcons")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_JumpState")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_JumpFxRoot")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_JumpFx")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_JumpResultFx")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_StartTime")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_Tick")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_InArea")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_ThumbFx")

RegistClassMember(LuaNewHorseRaceBaseWnd, "m_CurColor")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_SpeedGather")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_SpeedUpEndTime")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_JumpEndTime")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_JumpTime")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_AccelerateDuration")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_MaxAcceleration")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_Tweener")

RegistClassMember(LuaNewHorseRaceBaseWnd, "m_PrefectTex")

RegistClassMember(LuaNewHorseRaceBaseWnd, "m_SkillBg")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_PrefectJumpLab")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_NormalJumpTex")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_PrefectJumpTex")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_StartPrecent")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_Size")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_ResultFxTick")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_PerfectCenter")

RegistClassMember(LuaNewHorseRaceBaseWnd, "m_AccelerateYellowFxId")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_AccelerateRedFxId")
RegistClassMember(LuaNewHorseRaceBaseWnd, "m_AccelerateFailFxId")
--@endregion RegistChildComponent end

function LuaNewHorseRaceBaseWnd:Awake()
    self:InitComponents()
    self.m_AccelerateYellowFxId = MaPiTengYunSai_SettingV2.GetData("AccelerateYellowFxId").Value
    self.m_AccelerateRedFxId = MaPiTengYunSai_SettingV2.GetData("AccelerateRedFxId").Value
    self.m_AccelerateFailFxId = MaPiTengYunSai_SettingV2.GetData("AccelerateFailFxId").Value

    local quality = 0
    local naiLi = 0
    if not System.String.IsNullOrEmpty(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId) then
        local zuoQiData = CZuoQiMgr.Inst:GetZuoQiById(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId)
        if zuoQiData then
			quality = zuoQiData.mapiInfo.Quality
            naiLi = zuoQiData.mapiInfo.Naili
		end
    end
    self.m_JumpTime = 3
    self.m_AccelerateDuration = self:GetAccelerateDuration(naiLi)
    self.m_MaxAcceleration = self:GetMaxAcceleration(quality)

    self.m_YellowIcons = {}
    self.m_RedIcons = {}
    for i = 1, self.m_MaxAcceleration do
        local transform = self.m_Grid.transform:GetChild(i-1)
        transform.gameObject:SetActive(true)
        self.m_YellowIcons[i] = transform:Find("Yellow")
        self.m_RedIcons[i] = transform:Find("Red")
    end

    for i = self.m_MaxAcceleration + 1, 11 do
        self.m_Grid.transform:GetChild(i-1).gameObject:SetActive(false)
    end
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaNewHorseRaceBaseWnd:Init()
    self.m_JumpState:SetActive(false)

    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    self.m_StartTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CHorseRaceMgr.Inst.playStartTime)
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    self.m_Tick = RegisterTick(function()
        self:UpdateState()
    end, 100)
    self.m_TimeLabel.text = SafeStringFormat3("00:00")
    self.m_RankLabel.text = LocalString.GetString("")
    self.m_ThumbFx:LoadFx("fx/ui/prefab/UI_saima_benpao.prefab")
    self.m_SpeedLab.text = SafeStringFormat3(LocalString.GetString("%s米/秒"), SafeStringFormat3("%.1f", 0))
    self.m_Profress.value = 0
    self.m_Slider.value = 0
    self.m_SpeedGather = 0
    self:UpdateSpeedIcon(0)
    self:UpdateCurrentRank()
    self:IPhoneXAdaptation()

    if LuaNewHorseRaceMgr.m_CurColor ~= nil then 
        self.m_CurColor = LuaNewHorseRaceMgr.m_CurColor
        self.m_SpeedGather = LuaNewHorseRaceMgr.m_SpeedGather
        self.m_SpeedUpEndTime = LuaNewHorseRaceMgr.m_SpeedUpEndTime
        self:UpdateSpeedIcon(self.m_SpeedGather, self.m_CurColor)
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaNewHorseRaceBaseWnd:InitComponents()
    self.m_JumpState = self.transform:Find("TopRight/Center/JumpState").gameObject
    self.m_JumpFx    = self.transform:Find("TopRight/Center/JumpState/FxRoot/Fx"):GetComponent(typeof(CUIFx))
    self.m_FxStartGo = self.transform:Find("TopRight/Center/JumpState/Start").gameObject
    self.m_FxEndGo   = self.transform:Find("TopRight/Center/JumpState/End").gameObject
    self.m_PrefectTex= self.transform:Find("TopRight/Center/JumpState/State2"):GetComponent(typeof(UITexture))
    self.m_ThumbFx   = self.transform:Find("TopRight/Left/Progress/Thumb/Fx"):GetComponent(typeof(CUIFx))

    self.m_SkillBg        = self.transform:Find("TopRight/Center/HightLightSkillBg").gameObject
    self.m_PrefectJumpLab = self.transform:Find("TopRight/Center/JumpState/Label"):GetComponent(typeof(UILabel))
    self.m_NormalJumpTex   = self.transform:Find("TopRight/Center/JumpState/State2"):GetComponent(typeof(UITexture))
    self.m_PrefectJumpTex  = self.transform:Find("TopRight/Center/JumpState/Prefect"):GetComponent(typeof(UITexture))
    self.m_JumpResultFx     = self.transform:Find("TopRight/Center/JumpState/FxRoot/ResultFx"):GetComponent(typeof(CUIFx))
    self.m_JumpFxRoot       = self.transform:Find("TopRight/Center/JumpState/FxRoot").gameObject
end

function LuaNewHorseRaceBaseWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("PlayerEnterSaiMaJumpRegion", self, "OnPlayerEnterSaiMaJumpRegion")
    g_ScriptEvent:AddListener("PlayerLeaveSaiMaJumpRegion", self, "OnPlayerLeaveSaiMaJumpRegion")
    g_ScriptEvent:AddListener("SyncSaiMaAccelerateInfo", self, "OnSyncSaiMaAccelerateInfo")
    g_ScriptEvent:AddListener("PlayerUseSaiMaJumpSkill", self, "OnPlayerUseSaiMaJumpSkill")
    g_ScriptEvent:AddListener("RunwayMgrEnterNextRegion", self, "OnRunwayMgrEnterNextRegion")
    g_ScriptEvent:AddListener("OnSyncHorseRaceRankAndProgress", self, "UpdateCurrentRank")
    --CameraFollow.Inst.CurMode = CameraMode.E3D

    --CameraFollow.m_MinRotationEnable = true
    CRenderScene.Inst.EnableChunkDynamicHide = false
    self:AdjustCameraDir()
end

function LuaNewHorseRaceBaseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("PlayerEnterSaiMaJumpRegion", self, "OnPlayerEnterSaiMaJumpRegion")
    g_ScriptEvent:RemoveListener("PlayerLeaveSaiMaJumpRegion", self, "OnPlayerLeaveSaiMaJumpRegion")
    g_ScriptEvent:RemoveListener("SyncSaiMaAccelerateInfo", self, "OnSyncSaiMaAccelerateInfo")
    g_ScriptEvent:RemoveListener("PlayerUseSaiMaJumpSkill", self, "OnPlayerUseSaiMaJumpSkill")
    g_ScriptEvent:RemoveListener("RunwayMgrEnterNextRegion", self, "OnRunwayMgrEnterNextRegion")
    g_ScriptEvent:RemoveListener("OnSyncHorseRaceRankAndProgress", self, "UpdateCurrentRank")

    --CameraFollow.m_MinRotationEnable = false
    --CameraFollow.Inst.CurMode = CameraMode.E3DPlus
end

function LuaNewHorseRaceBaseWnd:OnRunwayMgrEnterNextRegion(regionId)
    local angle = -(CLuaRunwayMgr:GetRegionDir(regionId)) - 90
    self:AdjustCameraDir(angle)
end

function LuaNewHorseRaceBaseWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaNewHorseRaceBaseWnd:OnPlayerEnterSaiMaJumpRegion(start, size, duration)
    self.m_InArea = true

    self.m_SkillBg.transform.position = CSkillButtonBoardWnd.Instance.additionalButtonInfos[0].transform.position
    self.m_StartPrecent = start / 100
    self.m_Size = size / 100
    self:AdjustPrefectArea()
    self.m_JumpTime = duration
    self.m_JumpState:SetActive(true)
    self.m_JumpFx.OnLoadFxFinish = DelegateFactory.Action(function()
        self.m_JumpFxRoot.transform.position = self.m_FxStartGo.transform.position
        LuaTweenUtils.Kill(self.m_Tweener,false)
        self.m_Tweener = CommonDefs.SetEase_Tweener(LuaTweenUtils.DOMove(self.m_JumpFxRoot.transform, self.m_FxEndGo.transform.position, self.m_JumpTime, false), Ease.Linear)
        CommonDefs.OnComplete_Tweener(self.m_Tweener, DelegateFactory.TweenCallback(
            function()
                self.m_JumpState:SetActive(false)
            end))
    end)
    self.m_JumpResultFx:DestroyFx()
    self.m_JumpFx:LoadFx("fx/ui/prefab/UI_saima_huoyan.prefab")
end

function LuaNewHorseRaceBaseWnd:AdjustPrefectArea()
    local totalXOffset = self.m_FxEndGo.transform.localPosition.x - self.m_FxStartGo.transform.localPosition.x
    local XStart = self.m_FxStartGo.transform.localPosition.x + self.m_StartPrecent * totalXOffset
    self.m_NormalJumpTex.width = self.m_Size * totalXOffset
    self.m_PerfectCenter = (self.m_StartPrecent * totalXOffset + self.m_NormalJumpTex.width / 2 ) / totalXOffset

    Extensions.SetLocalPositionX(self.m_PrefectJumpLab.transform, XStart + self.m_NormalJumpTex.width / 2)
    Extensions.SetLocalPositionX(self.m_PrefectJumpTex.transform, XStart + self.m_NormalJumpTex.width / 2)
    Extensions.SetLocalPositionX(self.m_NormalJumpTex.transform, XStart)
end

function LuaNewHorseRaceBaseWnd:OnPlayerLeaveSaiMaJumpRegion(engineId, result, bSkill, forwardSpeed)
    if CClientMainPlayer.Inst.EngineId == engineId then
        self.m_InArea = false
        self.m_JumpState:SetActive(false)
        if result == EnumMPTYSJump.ePerfect then
            EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"fx/ui/prefab/UI_wanmeiyiyue.prefab"})
        end
    end
end

function LuaNewHorseRaceBaseWnd:OnSyncSaiMaAccelerateInfo(hitType, colour, duration, timeCount)
-- @param hitType
--  0:加速结束
-- 	1:未在额外加速状态下撞击加速点
-- 	2:在额外加速状态下撞进正确颜色加速点
-- 	3:在额外加速状态下撞击错误颜色加速点
    
    if hitType == 0 then
        duration = 0
    elseif hitType == 1 or hitType == 2 then
        local RO = CClientMainPlayer.Inst.RO
        if colour == 1 then
            local fx = CEffectMgr.Inst:AddObjectFX(self.m_AccelerateYellowFxId,RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
            RO:AddFX("AccelerateYellow", fx)
        else
            local fx = CEffectMgr.Inst:AddObjectFX(self.m_AccelerateRedFxId,RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
            RO:AddFX("AccelerateRed", fx)
        end
    elseif hitType == 3 then
        duration = 0
        local RO = CClientMainPlayer.Inst.RO
        local fx = CEffectMgr.Inst:AddObjectFX(self.m_AccelerateFailFxId,RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        RO:AddFX("AccelerateFail", fx)
    elseif hitType == 4 then

    end

    self:UpdateSpeedIcon(timeCount, colour)
    self.m_CurColor = colour
    self.m_SpeedGather = timeCount
    self.m_SpeedUpEndTime = CServerTimeMgr.Inst:GetZone8Time():AddSeconds(duration)
    self:UpdateState()
end

function LuaNewHorseRaceBaseWnd:OnPlayerUseSaiMaJumpSkill()
    if self.m_InArea == true then

        local percent = (self.m_JumpFxRoot.transform.localPosition.x - self.m_FxStartGo.transform.localPosition.x) / (self.m_FxEndGo.transform.localPosition.x - self.m_FxStartGo.transform.localPosition.x)
        local progress = percent
        
        local jumpState = EnumMPTYSJump.eLow
        if progress < self.m_StartPrecent then
            jumpState = EnumMPTYSJump.eLow
        elseif progress < self.m_StartPrecent + self.m_Size then
            local offset = math.abs(self.m_PerfectCenter - progress)
            if offset * 100 <= 2.5 then
                jumpState = EnumMPTYSJump.ePerfect
            else 
                jumpState = EnumMPTYSJump.eNormal
            end
        elseif progress < 1 then
            jumpState = EnumMPTYSJump.eHigh
        end

        LuaTweenUtils.Kill(self.m_Tweener,false)
        self:DoResultAnim(jumpState == EnumMPTYSJump.ePerfect)
        Gac2Gas.SaiMaJumpResult(jumpState)
    -- else
    --     g_MessageMgr:ShowMessage("NMPTYS_UseSkill_InCorrect_Tip")
    end
end

function LuaNewHorseRaceBaseWnd:UpdateState()
    local speed = 0
    if CClientMainPlayer.Inst ~= nil then
        speed = CClientMainPlayer.Inst.MaxSpeed
    end
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local delta = CommonDefs.op_Subtraction_DateTime_DateTime(now, self.m_StartTime)
    self.m_TimeLabel.text = SafeStringFormat3("%.2d:%.2d", delta.Minutes, delta.Seconds)

    if self.m_SpeedUpEndTime ~= nil then
        local speedRestTime = CommonDefs.op_Subtraction_DateTime_DateTime(self.m_SpeedUpEndTime, now).TotalSeconds
        self.m_Slider.value = speedRestTime / self.m_AccelerateDuration
    else 
        self.m_Slider.value = 0
    end

    self.m_SpeedLab.text = SafeStringFormat3(LocalString.GetString("%s米/秒"), SafeStringFormat3("%.1f", speed))

    if self.m_JumpEndTime ~= nil then
        local jumpRestTime = CommonDefs.op_Subtraction_DateTime_DateTime(self.m_JumpEndTime, now).TotalSeconds
        local progress = 1 - jumpRestTime / self.m_JumpTime
        if self.m_StartPrecent <= progress and progress <= self.m_StartPrecent + self.m_Size then
            self.m_SkillBg:SetActive(true)
        else
            self.m_SkillBg:SetActive(false)
        end
    else 
        self.m_SkillBg:SetActive(false)
    end
end

function LuaNewHorseRaceBaseWnd:UpdateSpeedIcon(timeCount, color)
    for i = 1, self.m_MaxAcceleration do
        local yellowIcon = self.m_YellowIcons[i]
        local redIcon = self.m_RedIcons[i]
        
        local animIcon = color == 1 and yellowIcon or redIcon
        if not animIcon.gameObject.activeSelf and timeCount >= i then
            self:DoAni(animIcon)
        end

        yellowIcon.gameObject:SetActive(timeCount >= i and color == 1)
        redIcon.gameObject:SetActive(timeCount >= i and color == 2)
    end
end

function LuaNewHorseRaceBaseWnd:UpdateCurrentRank()
    if CHorseRaceMgr.Inst.currentRank > 0 then
        self.m_RankLabel.text = CHorseRaceMgr.Inst.currentRank
    end
    if CHorseRaceMgr.Inst.progress > 0 then
        self.m_Profress.value = CHorseRaceMgr.Inst.progress / 100
    end
end

function LuaNewHorseRaceBaseWnd:OnExpandButtonClick()
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaNewHorseRaceBaseWnd:DoAni(foreground)
    LuaTweenUtils.TweenAlpha(foreground, 0.3, 1, 0.5)
    LuaTweenUtils.TweenScale(foreground, Vector3(2,2,2), Vector3(1,1,1), 0.5)
end

function LuaNewHorseRaceBaseWnd:IPhoneXAdaptation()
    if UIRoot.EnableIPhoneXAdaptation then
        local w = self.transform:Find("TopRight/Bottom"):GetComponent(typeof(UIWidget))
        if w == nil then
            return
        end
        w.bottomAnchor.absolute = 144 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
        w.topAnchor.absolute = 244 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
        w:ResetAndUpdateAnchors()
    end
end

function LuaNewHorseRaceBaseWnd:AdjustCameraDir(angle)
    LuaNewHorseRaceMgr:AdjustCameraDir(angle)
end

function LuaNewHorseRaceBaseWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)

    UnRegisterTick(self.m_ResultFxTick)

    CameraFollow.Inst.MaxSpHeight = 0
    LuaNewHorseRaceMgr.m_CurColor = nil
    LuaNewHorseRaceMgr.m_SpeedGather = nil
    LuaNewHorseRaceMgr.m_SpeedUpEndTime = nil
end

function LuaNewHorseRaceBaseWnd:DoResultAnim(success)
    self.m_JumpResultFx:DestroyFx()
    self.m_JumpResultFx:LoadFx(success and CLuaUIFxPaths.QTEConsecutiveClickFx or CLuaUIFxPaths.QTESingleClickFx)
    if self.m_ResultFxTick ~= nil then
        UnRegisterTick(self.m_ResultFxTick)
        self.m_ResultFxTick = nil
    end
    self.m_ResultFxTick = RegisterTickOnce(function()
        self.m_JumpState:SetActive(false)
    end, 1000)
end


function LuaNewHorseRaceBaseWnd:GetAccelerateDuration(naiLi)
    local result = 0
    local formulaId = tonumber(MaPiTengYunSai_SettingV2.GetData("AccelerateDurationFormulaId").Value)
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        result = formula(nil, nil, {naiLi})
    end
    return result
end

function LuaNewHorseRaceBaseWnd:GetMaxAcceleration(quality)
    local result = 0
    local formulaId =  tonumber(MaPiTengYunSai_SettingV2.GetData("MaxAccelerationFormulaId").Value)
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        result = formula(nil, nil, {quality})
    end
    return result
end

