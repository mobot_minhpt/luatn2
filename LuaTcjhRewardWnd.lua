--@region import
local GameObject                = import "UnityEngine.GameObject"
local Vector3                   = import "UnityEngine.Vector3"
local CUIManager                = import "L10.UI.CUIManager"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"

local QnTableView               = import "L10.UI.QnTableView"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"

--@endregion

CLuaTcjhRewardWnd = class()

--RegistChildComponent

RegistChildComponent(CLuaTcjhRewardWnd, "QnTableView1",		    QnTableView)
RegistChildComponent(CLuaTcjhRewardWnd, "QnTableView2",		    QnTableView)

RegistChildComponent(CLuaTcjhRewardWnd, "BuyBtn",		        GameObject)
RegistChildComponent(CLuaTcjhRewardWnd, "CloseBtn",		        GameObject)
RegistChildComponent(CLuaTcjhRewardWnd, "DesLabel",		        UILabel)

--@endregion

RegistClassMember(CLuaTcjhRewardWnd, "SelectedItem")


function CLuaTcjhRewardWnd:Awake()
    self.SelectedItem = nil
    UIEventListener.Get(self.BuyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyBtnClick()
    end)

    UIEventListener.Get(self.CloseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCloseBtnClick()
    end)
end

function CLuaTcjhRewardWnd:Init()
    self.DesLabel.text = LuaHanJiaMgr.TcjhRewardData.Des

    local btntext = LuaGameObject.GetChildNoGC(self.BuyBtn.transform,"Label").label
    btntext.text = LuaHanJiaMgr.TcjhRewardData.BtnName

    self:FillTable1()

    local showtable2 = self:FillTable2()
    if showtable2 == false then
        local pos = self.QnTableView1.transform.localPosition
        pos.y = 0
        self.QnTableView1.transform.localPosition = pos

        self.BuyBtn:SetActive(false)

        local bpos = self.CloseBtn.transform.localPosition
        bpos.x = 0
        self.CloseBtn.transform.localPosition = bpos
    end
end

function CLuaTcjhRewardWnd:FillTable1()
    local len = #LuaHanJiaMgr.TcjhRewardData.Items
    local initfunc = function (item,index)
        local data = LuaHanJiaMgr.TcjhRewardData.Items[index+1]
        self:FillItemCell(item,data,data.Count)
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView1:ReloadData(true, true)
end

function CLuaTcjhRewardWnd:FillTable2()

    local items = LuaHanJiaMgr.TcjhRewardData.LockItems
    if items == nil then 
        self.QnTableView2.gameObject:SetActive(false)
        return false 
    end --高级酒壶，不需要显示第二行

    local len = #items
    local initfunc = function (item,index)
        local itemdata = items[index+1].Data
        local itemcount = items[index+1].Count
        self:FillItemCell(item,itemdata,itemcount)
    end
    self.QnTableView2.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.QnTableView2:ReloadData(true, true)
    return true
end

function CLuaTcjhRewardWnd:FillItemCell(item,data,count)
    if item == nil then return end

    local cell = item.transform
    local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
    local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
    local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label

    local itemid = data.ItemID
    local isBind = data.IsBind
    
    local itemcfg = Item_Item.GetData(itemid)
    isBind = isBind or itemcfg.Lock == 1

    if itemcfg then
        icon:LoadMaterial(itemcfg.Icon)
    end

    bindgo:SetActive(tonumber(isBind) == 1)

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end 
    end)

    if tonumber(count) <= 1 then
        countlb.text = ""
    else
        countlb.text = count
    end
    cell.localPosition = Vector3.zero
    cell.gameObject:SetActive(true)

end

function CLuaTcjhRewardWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

--@region UIEvent

function CLuaTcjhRewardWnd:OnBuyBtnClick()
    if LuaHanJiaMgr.TcjhRewardData.BtnAction then
        LuaHanJiaMgr.TcjhRewardData.BtnAction()
    end
end

function CLuaTcjhRewardWnd:OnCloseBtnClick()
    if LuaHanJiaMgr.TcjhRewardData.CloseAction then
        LuaHanJiaMgr.TcjhRewardData.CloseAction()
    end
    CUIManager.CloseUI(CLuaUIResources.TcjhRewardWnd)
end

--@endregion
