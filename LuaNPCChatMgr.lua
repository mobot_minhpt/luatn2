local ClientMainPlayer = import"L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"
local EnumNPCChatType = import "L10.Game.EnumNPCChatType"
local EnumNPCChatMsgLocation = import "L10.Game.EnumNPCChatMsgLocation"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local ShareMgr = import "ShareMgr"
if LuaNPCChatMgr then
    LuaNPCChatMgr:DelListener()
end
LuaNPCChatMgr = {}

EnumNPCChatEventType = {
    RedPackage = 1, -- 奖励红包
    Gift = 2,       -- 奖励道具
    Task = 3,       -- 接取任务
}
EnumNPCChatStatus = {
    FreeChat = 0,   -- 自由聊天状态
    WaitForNPC = 1, -- 等待NPC输入
    WaitForPlayer = 2,  -- 等待玩家选择
    WaitForFinishEvent = 3, -- 等待玩家领取奖励
}

LuaNPCChatMgr.TaskID = nil

LuaNPCChatMgr.IsOpenNPCChatView = false     -- 在打开NPC聊天界面的情况下处理NPCChatPanel的效果,关闭NPC聊天界面时不做处理
LuaNPCChatMgr.IsChattingNPCID = 0           -- 正在聊天的NPCID
LuaNPCChatMgr.OpenNPCGiftWndData = nil      -- 奖励事件相关数据，用于打开NPCGiftWnd界面

LuaNPCChatMgr.ChatTick = nil                -- 模仿NPC输入的Tick，延迟发送消息
LuaNPCChatMgr.MsgIdInTick = 0               -- Tick延迟发送的消息ID
LuaNPCChatMgr.ConsumeTime = 0               -- Tick发送延迟时间

LuaNPCChatMgr.Id2ChatStatus = nil            -- key: NPCID value: 当前ChatStatus
LuaNPCChatMgr.Id2MessageNeedFinish = nil     -- Key：NPCID value: 当前需要完成的MsgID 
LuaNPCChatMgr.Id2ChatNum = nil               -- Key: NPCID value: 所有等待玩家进行的MsgID/EventID/GroupID (ChatID) 的数量
LuaNPCChatMgr.Id2ChatNeedFinish = nil        -- Key：NPCID value: 所有等待玩家进行的MsgID/EventID/GroupID (用ChatID表示三种消息ID) 
LuaNPCChatMgr.HasLoadData = false            -- 是否初始化过数据

LuaNPCChatMgr.EventId2Time = nil             -- key : EventID value : Event完成的时间戳
LuaNPCChatMgr.HasLoadEventTimeData = false   -- 是否加载本地文件信息(event事件和事件完成时间)

LuaNPCChatMgr.NPCChatEventId2TimeFileName = "NPCChatEventTimeTemplateFileData"   -- 本地文件名(event事件和事件完成时间)
LuaNPCChatMgr.MsgFormat = 'NPCChat://{"MsgID":%d,"type":%d,"Content":"%s","IsFirstOrEnd":%d}'
-- Json格式示例：NPCChat://{"MsgID":30001,"type":1,"Content":"XXXXXXXX","IsFirstOrEnd":1}

function LuaNPCChatMgr:DelListener()
    g_ScriptEvent:RemoveListener("SetNpcChatEventFinished",LuaNPCChatMgr,"OnSetNpcChatEventFinished")
    g_ScriptEvent:RemoveListener("MainPlayerCreated",LuaNPCChatMgr,"OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaNPCChatMgr, "OnGasDisconnect")  
end
function LuaNPCChatMgr:AddListener()
    g_ScriptEvent:AddListener("SetNpcChatEventFinished",LuaNPCChatMgr,"OnSetNpcChatEventFinished")
    g_ScriptEvent:AddListener("MainPlayerCreated",LuaNPCChatMgr,"OnMainPlayerCreated")
    g_ScriptEvent:AddListener("GasDisconnect", LuaNPCChatMgr, "OnGasDisconnect")  
end

function LuaNPCChatMgr:EnableNpcChat()
    return true
end

LuaNPCChatMgr:AddListener()
--[[
    key : NPCID  Value: Dic -> key : 未完成的eventID或GroupID value: doubleData : 接受事件或Group的时间
]]--
function LuaNPCChatMgr:OnMainPlayerCreated()
    self:InitData(LuaNPCChatMgr.DelayReset)
end

function LuaNPCChatMgr:InitData(force)
    if not self:EnableNpcChat() then return end
    --CIMMgr.Inst:GetLastIMMsgById()
    if LuaNPCChatMgr.HasLoadData and force == false then return end
    LuaNPCChatMgr.Id2ChatStatus = {}
    LuaNPCChatMgr.Id2ChatNum = {}
    LuaNPCChatMgr.Id2MessageNeedFinish = {}
    LuaNPCChatMgr.Id2ChatNeedFinish = {}
    local RelashionShip = CClientMainPlayer.Inst.RelationshipProp
    if RelashionShip == nil then return end
    local fset = RelashionShip.NpcFriendSet
    if fset == nil then return end
    
    NpcChat_ChatNpc.Foreach(function(key,npcCfg) 
        local isFriend = fset:GetBit(npcCfg.SN)
        if not isFriend then return end
        -- 有这个NPC好友
        local res,data = RelashionShip.OnGoingNpcChatInfo.Data.mapData.Data:TryGetValue(key)
        if res and data.Data.mapData.Data.Keys.Count > 0 then -- 说明有未完成的event或Group
            if not CIMMgr.Inst:IsLoaded(key) then -- 读一下历史消息
                CIMMgr.Inst:LoadMsgs(key)
            end
            
            local LastMsg = CIMMgr.Inst:GetLastIMMsgById(key)  -- 读一下历史记录中的最后一条消息
            local LastMsgInfo = LastMsg and self:ParseMsgFormat(LastMsg.Content) or nil
            local MsgID = LastMsgInfo and LastMsgInfo.MsgID or 0
            local EventID = MsgID ~= 0 and LastMsgInfo.Type == EnumToInt(EnumNPCChatType.Event) and tonumber(LastMsgInfo.Content) or 0
            
            local chatmsgdata = NpcChat_ChatMsg.GetData(MsgID)
            local GroupID = MsgID ~= 0 and chatmsgdata.GroupID or 0 
            
            CommonDefs.DictIterate(data.Data.mapData.Data,DelegateFactory.Action_object_object(function (___key, ___value) 
                if ___key == MsgID or ___key == EventID or ___key == GroupID then    -- 说明最后一条消息属于进行中的状态，会优先处理历史记录中最后一条消息的后续
                else
                    self:PushCurChatInfo(key,___key)        -- 将其他进行中Chat存放在 Id2ChatNeedFinish 中
                end
            end))

            -- 优先处理历史记录中最后一条消息及相关后续
            if MsgID ~= 0 then
                if EventID ~= 0 then -- 最后一条消息是待完成的Event，不发消息，更新一下状态
                    local data = NpcChat_ChatEvent.GetData(EventID) 
                    local eset = RelashionShip.FinishNpcChatEventSet
                    if eset and data then
                       local bbit = eset:GetBit(data.SN)
                        if not bbit then
                            self:UpdateNPCChatStatus(EnumNPCChatStatus.WaitForFinishEvent,key,EventID)
                            CLuaIMMgr.SetID2InteractMsgInfo(key,true)
                            return
                        end
                    end
                end
                
                --最后一条消息可能是玩家的发送信息
                if LastMsg and LastMsg.SenderId ~= key then
                    self:ParseMsg(MsgID,key)
                    return
                end

                -- 最后一条消息是Msg, 检查Msg是否有后续，如果有：发一下该Msg的后续
                local nextmsgid = chatmsgdata.NextMsgID
                self:CheckNextMsg(nextmsgid,LastMsgInfo.IsFirstOrEndMsg,GroupID,key)
            else    
                self:PopAndSendChatInfo(key)
            end
        end
    end)
    LuaNPCChatMgr.HasLoadData = true
end

LuaNPCChatMgr.DelayReset = false
function LuaNPCChatMgr:OnGasDisconnect()
    --if LuaNPCChatMgr.ChatTick then
    LuaNPCChatMgr.DelayReset = true
    --else
    --    self:ResetData()
    --end
end

-- 重置数据
function LuaNPCChatMgr:ResetData()
    LuaNPCChatMgr.IsOpenNPCChatView = false     
    LuaNPCChatMgr.IsChattingNPCID = 0           
    LuaNPCChatMgr.OpenNPCGiftWndData = nil      
    LuaNPCChatMgr.ChatTick = nil               
    LuaNPCChatMgr.MsgIdInTick = 0         
    LuaNPCChatMgr.ConsumeTime = 0  
    
    LuaNPCChatMgr.Id2ChatStatus = nil        
    LuaNPCChatMgr.Id2ChatNum = nil                
    LuaNPCChatMgr.Id2MessageNeedFinish = nil  
    LuaNPCChatMgr.Id2ChatNeedFinish = nil   
    LuaNPCChatMgr.HasLoadData = false

    LuaNPCChatMgr.EventId2Time = nil
    LuaNPCChatMgr.HasLoadEventTimeData = false
end
-- 添加一个待完成的Chat信息
function LuaNPCChatMgr:PushCurChatInfo(NPCID,ChatID)
    if not LuaNPCChatMgr.Id2ChatNeedFinish[NPCID] then
        LuaNPCChatMgr.Id2ChatNum[NPCID] = 0
        LuaNPCChatMgr.Id2ChatNeedFinish[NPCID] = {}
    end
    LuaNPCChatMgr.Id2ChatNum[NPCID] = LuaNPCChatMgr.Id2ChatNum[NPCID] + 1
    LuaNPCChatMgr.Id2ChatNeedFinish[NPCID][LuaNPCChatMgr.Id2ChatNum[NPCID]] = ChatID
end
-- 取一个待完成的Chat信息
function LuaNPCChatMgr:PopCurChatInfo(NPCID)
    if not LuaNPCChatMgr.Id2ChatNeedFinish[NPCID] then return -1 end
    if LuaNPCChatMgr.Id2ChatNum[NPCID] <= 0 then return -1 end
    local ChatId = LuaNPCChatMgr.Id2ChatNeedFinish[NPCID][LuaNPCChatMgr.Id2ChatNum[NPCID]]
    LuaNPCChatMgr.Id2ChatNeedFinish[NPCID][LuaNPCChatMgr.Id2ChatNum[NPCID]] = 0
    LuaNPCChatMgr.Id2ChatNum[NPCID] = LuaNPCChatMgr.Id2ChatNum[NPCID] - 1
    return ChatId
end
-- 取一个待完成的Chat消息并且发送消息
function LuaNPCChatMgr:PopAndSendChatInfo(NPCID)
    local MainPlayer = CClientMainPlayer.Inst
    if not MainPlayer then return end
    while true do
        local ChatID = self:PopCurChatInfo(NPCID)
        if ChatID < 0 then return end
        if ChatID > 3000000 then    -- 发Msg
            self:ProcessSendPlayerChatMsg(NPCID, ChatID)
            return
        elseif ChatID > 2000000 then    -- 发Group
            if not MainPlayer.RelationshipProp.FinishNpcChatGroupSet:GetBit(NpcChat_ChatGroup.GetData(ChatID).SN) then
                self:ProcessAcceptNPCChatGroup(NPCID, ChatID)
                return
            end
        elseif  ChatID > 1000000 then  -- 发Event
            if not MainPlayer.RelationshipProp.FinishNpcChatEventSet:GetBit(NpcChat_ChatEvent.GetData(ChatID).SN) then
                self:ProcessSetNpcChatEventAccepted(NPCID, ChatID)
                return 
            end
        end
    end
end
-- 解析下MsgFormat内容
function LuaNPCChatMgr:ParseMsgFormat(str)
    if cs_string.IsNullOrEmpty(str) then return nil end
    local MsgID, Type, Content, IsFirstOrEnd = string.match(str,'NPCChat://{"MsgID":(%d+),"type":(%d+),"Content":"([^%s]-)","IsFirstOrEnd":(%d+)}')
    if MsgID then
        return {MsgID = tonumber(MsgID), Type = tonumber(Type), Content = Content, IsFirstOrEnd = tonumber(IsFirstOrEnd)}
    else return nil 
    end
end
--------------事件完成时间本地存盘----------------
-- 读事件完成时间
function LuaNPCChatMgr:GetEventTime(EventID)
    local MainPlayer = CClientMainPlayer.Inst
    if not MainPlayer then return nil end 
    if not LuaNPCChatMgr.HasLoadEventTimeData then
        local json = CPlayerDataMgr.Inst:LoadPlayerData(LuaNPCChatMgr.NPCChatEventId2TimeFileName)
        if json ~= nil and json~="" then
            LuaNPCChatMgr.EventId2Time = luaJson.json2lua(json)
        end
        if not LuaNPCChatMgr.EventId2Time then
            LuaNPCChatMgr.EventId2Time = {}
        end
        LuaNPCChatMgr.HasLoadEventTimeData = true
    end
    return LuaNPCChatMgr.EventId2Time[tostring(EventID)] or LuaNPCChatMgr.EventId2Time[EventID]
end
-- 设事件完成时间
function LuaNPCChatMgr:SetEventTime(EventID,TimeStamp)
    local MainPlayer = CClientMainPlayer.Inst
    if not MainPlayer then return nil end 
    if not LuaNPCChatMgr.HasLoadEventTimeData then
        local json = CPlayerDataMgr.Inst:LoadPlayerData(LuaNPCChatMgr.NPCChatEventId2TimeFileName)
        if json ~= nil and json~="" then
            LuaNPCChatMgr.EventId2Time = luaJson.json2lua(json)
        end
        if not LuaNPCChatMgr.EventId2Time then
            LuaNPCChatMgr.EventId2Time = {}
        end
        LuaNPCChatMgr.HasLoadEventTimeData = true
    end
    LuaNPCChatMgr.EventId2Time[EventID] = TimeStamp
    local json = luaJson.table2json(LuaNPCChatMgr.EventId2Time)
    CPlayerDataMgr.Inst:SavePlayerData(LuaNPCChatMgr.NPCChatEventId2TimeFileName,json)
end

--[[
    @desc: 读ChatGroup表，读对话第一个消息
    调用ParseMsg，根据消息ID解析消息
    author:Codee
    time:2021-10-13 14:03:40
    --@NPCID:
	--@GroupID: 
    @return:
]]
function LuaNPCChatMgr:ProcessAcceptNPCChatGroup(NPCID,GroupID)
    local GroupInfo = NpcChat_ChatGroup.GetData(GroupID)
    if not GroupInfo then return end
    local FirstChatID = GroupInfo.FirstChatID
    if FirstChatID == 0 then return end
    self:ParseMsg(FirstChatID,NPCID)
end

function LuaNPCChatMgr:ProcessSetNpcChatEventAccepted(NPCID,eventId)
    local EventInfo = NpcChat_ChatEvent.GetData(eventId)
    local MsgId = EventInfo.MsgID
    local MsgInfo = NpcChat_ChatMsg.GetData(MsgId)
    if not MsgId then return end
    local IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.Default)
    if MsgInfo.IsFirstOrEndMsg == 2 then
        IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.End)
    elseif MsgInfo.IsFirstOrEndMsg == 1 then
        IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.First)
    end
    local NPCInfo = NPC_NPC.GetData(NPCID)
    if not NPCInfo then return end
    if CClientMainPlayer.Inst then
        local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,MsgId, EnumToInt(EnumNPCChatType.Event),tostring(eventId),IsFirstOrEndMsg)
        CIMMgr.Inst:RecvMsg(ClientMainPlayer.Inst.Id, NPCInfo.ID, 0, NPCInfo.Name, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), msg, CServerTimeMgr.Inst.timeStamp)
        self:UpdateNPCChatStatus(EnumNPCChatStatus.WaitForFinishEvent,NPCInfo.ID,eventId)
        CLuaIMMgr.SetID2InteractMsgInfo(NPCInfo.ID,true)
    end
end

function LuaNPCChatMgr:ProcessSendPlayerChatMsg(NPCID,NPCChatMsgID)
    local MsgInfo = NpcChat_ChatMsg.GetData(NPCChatMsgID)
    if not MsgInfo then return end
    self:ParseMsg(MsgInfo.ID,NPCID)
end

--@region RPC消息

function LuaNPCChatMgr:ProcessChatMsg(npcTemplateId, npcChatId)
    if not self:EnableNpcChat() then return end
    self:InitData(false)

    local st = LuaNPCChatMgr.Id2ChatStatus[npcTemplateId]
    if st == EnumNPCChatStatus.FreeChat or st == nil then
        self:DoEvent(npcTemplateId, npcChatId)
    else
        self:PushCurChatInfo(npcTemplateId,npcChatId)
    end
end


function LuaNPCChatMgr:DoEvent(npcTemplateId, npcChatId)
	if npcChatId > 3000000 then	--  3000001之后 消息ID->Msg  
		self:ProcessSendPlayerChatMsg(npcTemplateId, npcChatId)
	elseif npcChatId > 2000000 then	-- 2000001 - 3000000 对话组ID Group
		self:ProcessAcceptNPCChatGroup(npcTemplateId,npcChatId)
	else 	-- 2000000 之前 事件Event
		self:ProcessSetNpcChatEventAccepted(npcTemplateId,npcChatId)
	end
end

--@endregion

-- 读ChatMsg表，根据消息内容Content解析
--[[解析内容： 
    消息ID
    消息类型 -> 调用对应的方法
    消息内容 -> 根据不同的消息类型有不同的参数  
    所属对话组ID    -> 1. 消息分割符的显示会用到 2. 判断对话组结束时用到
    后续消息ID  -> 用于消息发送后能继续下一步
    是否该组的首个或末尾消息内容 -> 用于显示对话组分割符 ->需要在cs下解析
    发送方NPCID和NPCName    -> 处理一个Group组(一个Group组的消息都来自同一个NPC)中的NPC消息时，直接传入需要用到的NPCID和NPCName, 避免每次处理消息时都读表
    发送需要花费的时间 -> 用于显示等待对方输入的效果
]]
function LuaNPCChatMgr:ParseMsg(MsgID,NPCID)
    if NPCID <= 0 then return end
    self:UpdateNPCChatStatus(EnumNPCChatStatus.WaitForNPC,NPCID,MsgID)
    self:WaitForNPC(MsgID,NPCID)
end

function LuaNPCChatMgr:WaitForNPC(MsgID,NPCID)
    local NPCInfo = NPC_NPC.GetData(NPCID)
    local MsgInfo = NpcChat_ChatMsg.GetData(MsgID)
    if not MsgInfo or not NPCInfo then return end
    local NPCName = NPCInfo.Name
    local functionName,parameters = string.match(MsgInfo.Content,"(%w+)%(([^\n]-)%)")
    local func = LuaNPCChatMgr[functionName]
    if not func then return end
    local GroupID = MsgInfo.GroupID
    local ConsumeTime = MsgInfo.ConsumeTime
    local NextMsgID = MsgInfo.NextMsgID
    local IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.Default)
    if MsgInfo.IsFirstOrEndMsg == 2 then
        IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.End)
    elseif MsgInfo.IsFirstOrEndMsg == 1 then
        IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.First)
    end
    local args = {MsgID = MsgID, Content = parameters, GroupID = GroupID, NextMsgID = NextMsgID, IsFirstOrEndMsg = IsFirstOrEndMsg, NPCID = NPCID, NPCName = NPCName}
    
    if LuaNPCChatMgr.IsOpenNPCChatView and LuaNPCChatMgr.IsChattingNPCID == NPCID then
        LuaNPCChatMgr.MsgIdInTick = MsgID
        LuaNPCChatMgr.ConsumeTime = ConsumeTime
        if LuaNPCChatMgr.ConsumeTime ~= 0 then
            LuaNPCChatMgr.ChatTick = RegisterTickOnce(function() func(self,args) end,LuaNPCChatMgr.ConsumeTime * 1000)
        else
            func(self,args)
        end
    else
        func(self,args)
    end
end

--[[
    根据参数构造msg，msg内容应包括：消息ID, 消息类型，消息内容，是否该组的首个或末尾消息内容   -> 用于显示分割
    调用CIMMgr.Inst.RecvMsg(recverId, senderId, engineId, senderName, senderClass, senderGender, message, sendTime)  发消息
    根据情况设置并更新聊天状态 Id2ChatStatus  
    检查后续消息ID，如果有：调用ParaeMsg处理下一条Msg 如果无:检查是否一组Group结束->如果是:发送RPC;如果不是:不做处理
]]

-- 发文本消息
function LuaNPCChatMgr:SendText(args)
    self:ClearTick()
    local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,args.MsgID,EnumToInt(EnumNPCChatType.Text),args.Content,args.IsFirstOrEndMsg)
    if CClientMainPlayer.Inst then
        CIMMgr.Inst:RecvMsg(ClientMainPlayer.Inst.Id, args.NPCID, 0, args.NPCName, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), msg, CServerTimeMgr.Inst.timeStamp)
        self:CheckNextMsg(args.NextMsgID,args.IsFirstOrEndMsg,args.GroupID,args.NPCID)
    end
end

-- 发图片消息
-- 采用的最终方式是复用个人空间的图片展示方法 （PersonalSpace）
-- pic://,fullpic=XXX,type=pic,1500,719,False   --图片消息的数据格式。XXX是图片的pid
-- <img url=https://ok.166.net/gameyw-misc/opd/squash/20200319/165616-dt68zm0feh.jpg w=1190 h=128>  -- 精灵的富文本图片显示方式，有些复杂暂不考虑
function LuaNPCChatMgr:SendPic(args)
    self:ClearTick()
    local url = ShareMgr.GetWebImagePath(args.Content)
    local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,args.MsgID,EnumToInt(EnumNPCChatType.Pic),url,args.IsFirstOrEndMsg)
    if CClientMainPlayer.Inst then
        CIMMgr.Inst:RecvMsg(ClientMainPlayer.Inst.Id, args.NPCID, 0, args.NPCName, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), msg, CServerTimeMgr.Inst.timeStamp)
        self:CheckNextMsg(args.NextMsgID,args.IsFirstOrEndMsg,args.GroupID,args.NPCID)
    end
end
-- 发视频消息
function LuaNPCChatMgr:SendVideo(args)
    self:ClearTick()
    local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,args.MsgID,EnumToInt(EnumNPCChatType.Video),args.Content,args.IsFirstOrEndMsg)
    if CClientMainPlayer.Inst then
        CIMMgr.Inst:RecvMsg(ClientMainPlayer.Inst.Id, args.NPCID, 0, args.NPCName, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), msg, CServerTimeMgr.Inst.timeStamp)
        self:CheckNextMsg(args.NextMsgID,args.IsFirstOrEndMsg,args.GroupID,args.NPCID)  
    end
end
-- 发语音消息（语音消息的路径(配音资源路径)示例：event:/L10/L10_vocal/PuSongLing_New/PuSongLing_New_21）
-- 参数格式： (audio=路径,duartion=4(uint类型),text=文本内容)
function LuaNPCChatMgr:SendVoice(args)
    self:ClearTick()
    local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,args.MsgID,EnumToInt(EnumNPCChatType.Voice),args.Content,args.IsFirstOrEndMsg)
    if CClientMainPlayer.Inst then
        CIMMgr.Inst:RecvMsg(ClientMainPlayer.Inst.Id, args.NPCID, 0, args.NPCName, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), msg, CServerTimeMgr.Inst.timeStamp)
        self:CheckNextMsg(args.NextMsgID,args.IsFirstOrEndMsg,args.GroupID,args.NPCID)
    end
end

-- 当一个Group处理到一个Event时
--[[
    -- 已经接收过 
        1. -> 发送消息
        1.1 -> 还未完成过 -> 暂停并等待玩家完成，更新状态
        1.2 -> 已经完成过 -> 执行后续消息
    -- 还未接收过
        2. -> 发送RPC, RPC返回时调用LuaNPCChatMgr:OnSetNpcChatEventAccepted方法，方法中执行上述1. 及 1.1 步骤
]]
-- tonumber(args.Content) = eventID
function LuaNPCChatMgr:SendEvent(args)
    self:ClearTick()
    local eventID = tonumber(args.Content)
    if CClientMainPlayer.Inst then
        local RelationshipProp = CClientMainPlayer.Inst.RelationshipProp
        local res,data = RelationshipProp.OnGoingNpcChatInfo.Data.mapData.Data:TryGetValue(args.NPCID)
        local isAcceptEvent = res and data.Data.mapData.Data:ContainsKey(eventID) 
        local isFinishEvent = RelationshipProp.FinishNpcChatEventSet:GetBit(NpcChat_ChatEvent.GetData(eventID).SN)
        if not isAcceptEvent and not isFinishEvent then
            self:UpdateNPCChatStatus(EnumNPCChatStatus.FreeChat,args.NPCID,eventID)
            Gac2Gas.RequestAcceptNpcChatEvent(args.NPCID,eventID)
            return
        else
            local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,args.MsgID,EnumToInt(EnumNPCChatType.Event),args.Content,args.IsFirstOrEndMsg)  
            CIMMgr.Inst:RecvMsg(ClientMainPlayer.Inst.Id, args.NPCID, 0, args.NPCName, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), msg, CServerTimeMgr.Inst.timeStamp)
            if not isFinishEvent then
                self:UpdateNPCChatStatus(EnumNPCChatStatus.WaitForFinishEvent,args.NPCID,eventID)
                CLuaIMMgr.SetID2InteractMsgInfo(args.NPCID,true)
            else
                self:CheckNextMsg(args.NextMsgID,args.IsFirstOrEndMsg,args.GroupID,args.NPCID)
            end
        end
    end
end

-- 需要玩家选择则不需要发送消息,只更新状态
function LuaNPCChatMgr:ShowOptions(args)
    self:ClearTick()
    self:UpdateNPCChatStatus(EnumNPCChatStatus.WaitForPlayer,args.NPCID,args.MsgID)
    CLuaIMMgr.SetID2InteractMsgInfo(args.NPCID,true)
end

---------------------聊天状态------------------
-- NPC聊天状态变化
--[[
    处理：
    遇到NPC交互消息(奖励事件:需要领取，选项：需要玩家选择),记录当前NPC交互信息的类型,记录当前需要交互的信息ID(MsgID),如果是奖励事件(event),ID为EventID
    如果当前玩家打开对应NPC聊天界面:Broadcast("UpdateNPCChatStatus"),view界面根据上述记录的信息切换显示
]]
function LuaNPCChatMgr:UpdateNPCChatStatus(status,NPCID,MsgID)
    LuaNPCChatMgr.Id2ChatStatus[NPCID] = status
    LuaNPCChatMgr.Id2MessageNeedFinish[NPCID] = MsgID
    if LuaNPCChatMgr.IsOpenNPCChatView and LuaNPCChatMgr.IsChattingNPCID == NPCID then
        g_ScriptEvent:BroadcastInLua("UpdateNPCChatStatus")
    end
end

-- 切换聊天面板发生切换时
--[[
    处理：
    记录新的聊天面板数据：是否聊天对象为NPC，聊天对象为NPC时的NPCID
    切换->如果当前ChatTick不为空,说明：NPC正在输入中 -> UnRegisterTick 并立即处理后续的聊天信息
]]
function LuaNPCChatMgr:OnChangeFriendChatView(IsOpenNPCFriendChatView, oppositeID, oppositeName)
    if IsOpenNPCFriendChatView then
        LuaNPCChatMgr:ReleaseAlert(oppositeID)
        CLuaIMMgr.SetID2InteractMsgInfo(oppositeID,false)
    end
    if LuaNPCChatMgr.IsOpenNPCChatView ~= IsOpenNPCFriendChatView or LuaNPCChatMgr.IsChattingNPCID ~= oppositeID then
        if LuaNPCChatMgr.ChatTick ~= nil then
            UnRegisterTick(LuaNPCChatMgr.ChatTick)
            LuaNPCChatMgr.ChatTick = nil
            LuaNPCChatMgr.MsgIdInTick = 0
            LuaNPCChatMgr.ConsumeTime = 0
        end
    end
    if IsOpenNPCFriendChatView then
        LuaNPCChatMgr.IsOpenNPCChatView = IsOpenNPCFriendChatView
        LuaNPCChatMgr.IsChattingNPCID = oppositeID
    else
        LuaNPCChatMgr.IsOpenNPCChatView = false
        LuaNPCChatMgr.IsChattingNPCID = 0
    end
end
---------------------事件完成------------------
-- 事件完成时调用，事件完成时检查并继续播放后续Msg,服务器消息
function LuaNPCChatMgr:OnSetNpcChatEventFinished(NPCID, EventID)
    if not self:EnableNpcChat() then return end
    self:InitData(false)
    local EventInfo = NpcChat_ChatEvent.GetData(EventID)
    local MsgInfo = NpcChat_ChatMsg.GetData(EventInfo.MsgID) 
    CLuaIMMgr.SetID2InteractMsgInfo(NPCID,false)
    if MsgInfo then
        local IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.Default)
        if MsgInfo.IsFirstOrEndMsg == 2 then
            IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.End)
        elseif MsgInfo.IsFirstOrEndMsg == 1 then
            IsFirstOrEndMsg = EnumToInt(EnumNPCChatMsgLocation.First)
        end
        self:CheckNextMsg(MsgInfo.NextMsgID, IsFirstOrEndMsg, MsgInfo.GroupID, NPCID)
    end
end

-- 选项完成时调用, MsgID: 选项消息的ID ; NextMsgID: 根据玩家选择结果需要执行的后续消息ID
function LuaNPCChatMgr:OnOptionsFinished(NPCID,MsgID,NextMsgID)
    CLuaIMMgr.SetID2InteractMsgInfo(NPCID,false)
    local GroupID = NpcChat_ChatMsg.GetData(MsgID).GroupID
    if NextMsgID ~= 0 then
        self:ParseMsg(NextMsgID,NPCID)
    elseif NextMsgID == 0 and GroupID ~= 0 then
        Gac2Gas.RequestFinishNpcChatGroup(NPCID,GroupID)
    end
end

-- 检查后续消息
--[[
    处理：  如果有后续消息-> 调用ParseMsg方法处理后续消息
            如果没有后续消息 -> 当前消息是该group组的最后一条消息: 发送rpc通知服务器；否则不做处理。 更新状态 -> 检查Id2ChatNeedFinish并发消息
]]
function LuaNPCChatMgr:CheckNextMsg(NextMsgID,IsFirstOrEndMsg,GroupID,NPCID)
    if NextMsgID ~= 0 and IsFirstOrEndMsg ~= EnumToInt(EnumNPCChatMsgLocation.End) then
        self:ParseMsg(NextMsgID,NPCID)
        return
    elseif IsFirstOrEndMsg == EnumToInt(EnumNPCChatMsgLocation.End) and GroupID ~= 0 then
        if GroupID ~= 0 then
            Gac2Gas.RequestFinishNpcChatGroup(NPCID,GroupID)
        end
    end
    self:UpdateNPCChatStatus(EnumNPCChatStatus.FreeChat,NPCID,NextMsgID)
    self:PopAndSendChatInfo(NPCID) 
end

-- 添加好友，好友关系建立提醒
function LuaNPCChatMgr:OnAddFriend(NPCID)
    local NPCInfo = NPC_NPC.GetData(NPCID)
    if not NPCInfo then return end
    local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,0,EnumToInt(EnumNPCChatType.AddFriendTip),NPCInfo.Name,EnumToInt(EnumNPCChatMsgLocation.Default))  
    if CClientMainPlayer.Inst then
        CIMMgr.Inst:RecvMsg(ClientMainPlayer.Inst.Id, NPCID, 0, NPCInfo.Name, EnumToInt(EnumClass.Undefined), EnumToInt(EnumGender.Undefined), msg, CServerTimeMgr.Inst.timeStamp)
    end
end

-- 清Tick
function LuaNPCChatMgr:ClearTick()
    if LuaNPCChatMgr.ChatTick ~= nil then
        UnRegisterTick(LuaNPCChatMgr.ChatTick)
        LuaNPCChatMgr.ChatTick = nil
    end
    LuaNPCChatMgr.MsgIdInTick = 0
    LuaNPCChatMgr.ConsumeTime = 0
end

-- 打开NPCGiftWnd
function LuaNPCChatMgr:OpenNPCGiftWnd(Type,Args,NPCID,EventID)
    LuaNPCChatMgr.OpenNPCGiftWndData = {Type = Type, Args = Args, NPCID = NPCID, EventID = EventID}
    CUIManager.ShowUI(CLuaUIResources.NPCGiftWnd)
end

function LuaNPCChatMgr:IsChatNpc(id)
    if id and id<uint.MaxValue then
        return NpcChat_ChatNpc.Exists(id)
    else
        return false
    end
end

function LuaNPCChatMgr:GetResponseStr(npcid)
    local status = LuaNPCChatMgr.Id2ChatStatus[npcid]

    if status == EnumNPCChatStatus.WaitForPlayer then
        return true,LocalString.GetString("待回复")
    end
    if status == EnumNPCChatStatus.WaitForFinishEvent then
        return true,LocalString.GetString("待领取")
    end
    return false,""
end

LuaNPCChatMgr.CacheAlert = nil
function LuaNPCChatMgr:NeedAlert(pid)
    if LuaNPCChatMgr.CacheAlert == nil then 
        LuaNPCChatMgr.CacheAlert = {}
    end
    if LuaNPCChatMgr.CacheAlert[pid] then
        return false
    end
    return true
end

function LuaNPCChatMgr:ReleaseAlert(pid)
    if LuaNPCChatMgr.CacheAlert == nil then 
        LuaNPCChatMgr.CacheAlert = {}
    end
    LuaNPCChatMgr.CacheAlert[pid] = true
end
