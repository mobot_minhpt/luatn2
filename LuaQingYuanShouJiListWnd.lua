require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local Vector4 = import "UnityEngine.Vector4"

CLuaQingYuanShouJiListWnd = class()
RegistClassMember(CLuaQingYuanShouJiListWnd,"m_closeBtn")

RegistClassMember(CLuaQingYuanShouJiListWnd,"m_TableViewDataSource")
RegistClassMember(CLuaQingYuanShouJiListWnd,"m_TableView")
RegistClassMember(CLuaQingYuanShouJiListWnd,"m_ScrollView")
RegistClassMember(CLuaQingYuanShouJiListWnd,"m_Grid")

function CLuaQingYuanShouJiListWnd:Awake()
    
end

function CLuaQingYuanShouJiListWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaQingYuanShouJiListWnd:Init()
	self.m_closeBtn = self.transform:Find("CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
	self.m_ScrollView = self.transform:Find("TableView/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_Grid = self.transform:Find("TableView/ScrollView/Grid"):GetComponent(typeof(UIGrid))

	self.m_TableViewDataSource=DefaultTableViewDataSource.Create(function() 
			return self:NumberOfItems()  
		end, function(item, index)
			self:InitItem(item, index)
		end)

	self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    --adjust scrollview size
    local totalNum = (LuaValentine2019Mgr.ShouJiTbl and #LuaValentine2019Mgr.ShouJiTbl or 0)
    local newWidth = math.min(self.m_Grid.cellWidth * totalNum, 1600)
    local v = self.m_ScrollView.panel.baseClipRegion
    self.m_ScrollView.panel.baseClipRegion = Vector4(v.x, v.y, newWidth, v.w)

    self.m_TableView:ReloadData(true,false)
end

function CLuaQingYuanShouJiListWnd:NumberOfItems()
	return LuaValentine2019Mgr.ShouJiTbl and #LuaValentine2019Mgr.ShouJiTbl or 0
end

function CLuaQingYuanShouJiListWnd:InitItem(item,index)
	local data = LuaValentine2019Mgr.ShouJiTbl and LuaValentine2019Mgr.ShouJiTbl[index+1]
	if data == nil then return end
	local playerNameLabel = item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	if playerNameLabel then
		playerNameLabel.text = LuaValentine2019Mgr.GetWrapText(data.playerName)
	end

end

function CLuaQingYuanShouJiListWnd:OnSelectAtRow(row)
	self:Close()
	local data = LuaValentine2019Mgr.ShouJiTbl and LuaValentine2019Mgr.ShouJiTbl[row+1]
	if data == nil then return end
	LuaValentine2019Mgr.RequestValentineQYSJInfo(data.playerId)
end
