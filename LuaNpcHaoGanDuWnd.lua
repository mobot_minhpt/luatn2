local MessageWndManager = import "L10.UI.MessageWndManager"
local CEffectMgr=import "L10.Game.CEffectMgr"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local Vector3=import "UnityEngine.Vector3"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local UISlider=import "UISlider"
local Animator = import "UnityEngine.Animator"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local Screen=import "UnityEngine.Screen"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CUITexture=import "L10.UI.CUITexture"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView = import "L10.UI.QnTableView"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UIPanel=import "UIPanel"
local CItemMgr=import "L10.Game.CItemMgr"
local UIGrid=import "UIGrid"
local QnTabView=import "L10.UI.QnTabView"
local SoundManager=import "SoundManager"
local CUIFx=import "L10.UI.CUIFx"
local CClientNpc = import "L10.Game.CClientNpc"
local NPC_NPC = import "L10.Game.NPC_NPC"

CLuaNpcHaoGanDuWnd=class()
RegistClassMember(CLuaNpcHaoGanDuWnd,"m_NpcHeadList")
RegistClassMember(CLuaNpcHaoGanDuWnd,"m_NpcIds")
RegistClassMember(CLuaNpcHaoGanDuWnd,"m_NpcHeadOriginLocalPosition")
RegistClassMember(CLuaNpcHaoGanDuWnd,"m_NpcData")

RegistClassMember(CLuaNpcHaoGanDuWnd,"m_GiftList")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_ModelTexture")
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_RO")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_LetterLookup")
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_StoryLookup")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_TableView")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_Rotation")
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_ContentBg")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_SoundPlayingTick")
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_SoundPlayingEvent")
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_AniSoundPlayingEvent")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_IsPlaying")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_Notices")--红点提示
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_SendMailFxTick")--红点提示

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_TabView")--红点提示
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_TipButton")--红点提示
RegistClassMember(CLuaNpcHaoGanDuWnd, "m_LikeItems")

RegistClassMember(CLuaNpcHaoGanDuWnd, "m_ChangFxTick")


CLuaNpcHaoGanDuWnd.m_NpcId = 0
function CLuaNpcHaoGanDuWnd:Awake()
    self.m_ChangFxTick=nil
    self.m_ContentBg = self.transform:Find("Content/bg").gameObject
    self.m_Rotation = 180
    self:InitNpcData()

    self.m_LetterLookup = {}
    self.m_StoryLookup = {}
    NpcHaoGanDu_Letter.ForeachKey(function(k)
        local data = NpcHaoGanDu_Letter.GetData(k)
        if not self.m_LetterLookup[data.NpcId] then
            self.m_LetterLookup[data.NpcId] = {}
        end
        self.m_LetterLookup[data.NpcId][data.LetterId] = data
    end)

    NpcHaoGanDu_Xiaozhuan.ForeachKey(function(k)
        local data = NpcHaoGanDu_Xiaozhuan.GetData(k)
        if not self.m_StoryLookup[data.NpcId] then
            self.m_StoryLookup[data.NpcId] = {}
        end
        self.m_StoryLookup[data.NpcId][data.XiaozhuanId] = data
    end)

    self.m_TipButton = self.transform:Find("TipButton").gameObject
    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("NpcHaoGanDu_Tips")
    end)

    Gac2Gas.RequestNpcHaoGanDuSpecialGiftInfo(20021697)
    Gac2Gas.RequestNpcHaoGanDuSpecialGiftInfo(20021698)
    Gac2Gas.RequestNpcHaoGanDuSpecialGiftInfo(20021699)

    self.m_LikeItems = {}
    self.m_LikeItems[20021697] = {}--蒲松龄
    self.m_LikeItems[20021698] = {}--阿初
    self.m_LikeItems[20021699] = {}--哮天犬
    local setting = NpcHaoGanDu_Setting.GetData()
    for i=1,setting.PuSongLingLike.Length do
        local id = setting.PuSongLingLike[i-1]
        self.m_LikeItems[20021697][id] = true
    end
    for i=1,setting.AchuLike.Length do
        local id = setting.AchuLike[i-1]
        self.m_LikeItems[20021698][id] = true
    end
    for i=1,setting.XiaoTianQuanLike.Length do
        local id = setting.XiaoTianQuanLike[i-1]
        self.m_LikeItems[20021699][id] = true
    end
end

function CLuaNpcHaoGanDuWnd:InitNpcData()
    self.m_Notices = {}
    self.m_NpcData = {}
    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    if playProp then
        CommonDefs.DictIterate(playProp.NpcHaoGanDuData, DelegateFactory.Action_object_object(function (___key, ___value) 
            self.m_NpcData[tonumber(___key)] = ___value
        end))
        for k,v in pairs(self.m_NpcData) do
            CLuaNpcHaoGanDuMgr.ModifyNpcData(v)
        end
    end
end
function CLuaNpcHaoGanDuWnd:DoTitleAni()
    -- print("DoTitleAni")
    local tf = self.transform:Find("Content/HaoGan")
    local animator2 = tf:Find("ChengHao/ChengHao_2"):GetComponent(typeof(Animator))
    local ani2 = tf:Find("ChengHao/ChengHao_2/ChengHao_2"):GetComponent(typeof(CUITexture))
    -- ani1:Clear()
    ani2:Clear()

    local shuimoFx = tf:Find("Fx/ShuiMo"):GetComponent(typeof(CUIFx))
    shuimoFx:DestroyFx()
    shuimoFx:LoadFx("Fx/UI/Prefab/UI_NpcHaoGanDuWnd_ShuiMo_01.prefab")

    local level = 1
    local npcData = self.m_NpcData[CLuaNpcHaoGanDuWnd.m_NpcId] 
    if npcData then
        level = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(npcData.HaoGanDu)
    end
    local levelData = NpcHaoGanDu_Level.GetData(level)
    if levelData then
        ani2:LoadMaterial(levelData.Title)
        animator2:Play('NpcHaoGanDuWnd_ChengHao_2',0,0)
    else
        ani2:Clear()
    end
end

function CLuaNpcHaoGanDuWnd:ShowTitle()
    local tf = self.transform:Find("Content/HaoGan")
    local ani2 = tf:Find("ChengHao/ChengHao_2/ChengHao_2"):GetComponent(typeof(CUITexture))
    local animator2 = tf:Find("ChengHao/ChengHao_2"):GetComponent(typeof(Animator))

    animator2:Play('NpcHaoGanDuWnd_ChengHao_2',0,4)

    local shuimoFx = tf:Find("Fx/ShuiMo"):GetComponent(typeof(CUIFx))
    shuimoFx:DestroyFx()

    local level = 1
    local npcData = self.m_NpcData[CLuaNpcHaoGanDuWnd.m_NpcId] 
    if npcData then
        level = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(npcData.HaoGanDu)
    end
    local levelData = NpcHaoGanDu_Level.GetData(level)
    if levelData then
        ani2:LoadMaterial(levelData.Title)
    end
end

function CLuaNpcHaoGanDuWnd:Init()
    
    self.m_TabView = self.transform:Find("Tab"):GetComponent(typeof(QnTabView))
    self.m_TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function(btn,index)
        --隐藏特效
        local fx = btn.transform:Find("Fx").gameObject
        fx:SetActive(false)

        if index==0 then
            --称号动画
            -- self:DoTitleAni()
            self:ShowTitle()

            self.m_ContentBg:SetActive(false)
        else
            self.m_ContentBg:SetActive(true)
        end

        for i=1,self.m_TabView.m_TabButtons.Length do
            local selected = self.m_TabView.m_TabButtons[i-1].transform:Find("selected").gameObject
            selected:SetActive(self.m_TabView.m_TabButtons[i-1]==btn)
        end

        if index==2 then
            self:InitGift(CLuaNpcHaoGanDuWnd.m_NpcId)
            -- reposition一下就行
        
        elseif index==4 then--小传
            local tf = self.transform:Find("Content/Story")
            local grid = tf:Find("ScrollView/Grid").gameObject
            grid:GetComponent(typeof(UITable)):Reposition()
        end

    end)

    self.m_NpcHeadList = {}
    self.m_NpcIds = {}
    self.m_NpcHeadOriginLocalPosition = {}

    local npcList = FindChild(self.transform,"NpcList")
    local portraitTemplate = npcList:Find("Scrollview/PortraitTemplate").gameObject
    portraitTemplate:SetActive(false)
    local tf = npcList:Find("Scrollview/Table")
    Extensions.RemoveAllChildren(tf)

    NpcHaoGanDu_NPC.Foreach(function(k,v)
        local go = NGUITools.AddChild(tf.gameObject,portraitTemplate)
        go:SetActive(true)
        table.insert( self.m_NpcHeadList,go )
        table.insert( self.m_NpcIds, k)
        local data = NPC_NPC.GetData(k)
        local icon = go.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local texture = go.transform:Find("Texture"):GetComponent(typeof(CUITexture))

        go.transform:Find("Bg").gameObject:SetActive(true)
        go.transform:Find("Bg2").gameObject:SetActive(false)

        local grade = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(self.m_NpcData[k] and self.m_NpcData[k].HaoGanDu or 0)
        local path,width,height = CLuaNpcHaoGanDuMgr.GetBiAnHuaTextureInfo(grade)
        texture:LoadMaterial(path)
        local uitexture = texture:GetComponent(typeof(UITexture))
        uitexture.width = width
        uitexture.height = height



        icon:LoadNPCPortrait(data.Portrait)
        -- table.insert( self.m_NpcIds,k )
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnClickHead(g)
        end)
    end)

    -- self:InitNpc(self.m_NpcIds[1])
    self:OnClickHead(tf:GetChild(0).gameObject)

    tf:GetComponent(typeof(UIGrid)):Reposition()
    for i=1,tf.childCount do
        table.insert(self.m_NpcHeadOriginLocalPosition, tf:GetChild(i-1).localPosition)
    end
    local panel = npcList:Find("Scrollview"):GetComponent(typeof(UIPanel))

    panel.onClipMove = LuaUtils.OnClippingMoved(function(p)
        self:OnClipMove(p)
    end)
    self:OnClipMove(panel)

    self.m_TabView:ChangeTo(0)
    self:DoTitleAni()

end
function CLuaNpcHaoGanDuWnd:OnClickHead(g)
    for i,v in ipairs(self.m_NpcHeadList) do
        if v==g then
            v.transform:Find("Bg").gameObject:SetActive(false)
            v.transform:Find("Bg2").gameObject:SetActive(true)
            self:InitNpc(self.m_NpcIds[i])
        else
            v.transform:Find("Bg").gameObject:SetActive(true)
            v.transform:Find("Bg2").gameObject:SetActive(false)
        end
    end
    self:ShowTitle()
end

function CLuaNpcHaoGanDuWnd:InitNpc(id)
    self:CleanNpcAni()
    if self.m_SendMailFxTick then
        UnRegisterTick(self.m_SendMailFxTick)
        self.m_SendMailFxTick = nil
    end


    self.m_IsPlaying = false
    CLuaNpcHaoGanDuWnd.m_NpcId = id
    self:InitDoc(id)

    self:InitHaoGanDu(id)

    local tf = self.transform:Find("Content/HaoGan")
    -- local haoganduLabel = tf:Find("HaoGanDuLabel"):GetComponent(typeof(UILabel))
    -- local levelLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local nameLabel = self.transform:Find("Content/NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = self:GetName(id)

    -- if self.m_NpcData[id] then
    --     haoganduLabel.text = tostring(self.m_NpcData[id].HaoGanDu)
    --     levelLabel.text = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(self.m_NpcData[id].HaoGanDu)
    -- else
    --     haoganduLabel.text = "0"
    --     levelLabel.text = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(0)
    -- end


    self:InitGift(id)
    self:InitVoice(id)


    self.m_ModelTexture = FindChild(self.transform,"ModelTexture"):GetComponent(typeof(CUITexture))
    UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
        self.m_Rotation = self.m_Rotation - delta.x/Screen.width*360

        CUIManager.SetModelRotation("__NpcPreview__", self.m_Rotation)
    end)
    UIEventListener.Get(self.m_ModelTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsPlaying then return end
        if self.m_RO then
            local level=self.m_NpcData[id] and CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(self.m_NpcData[id].HaoGanDu) or 1
            local design = NpcHaoGanDu_Level.GetData(level)
            if design then
                local anisetting = nil
                if id==20021697 then
                    anisetting= design.PuSongLing
                elseif id==20021698 then
                    anisetting= design.Achu
                elseif id==20021699 then
                    anisetting= design.XiaoTian
                end
                if anisetting then
                    local ratios = design.Ratios
                    local all=0
                    for i=1,ratios.Length do
                        all = all+ratios[i-1]
                    end
                    local index = 0
                    local r = math.random( 1,all )
                    for i=1,ratios.Length do
                        r = r - ratios[i-1]
                        if r<=0 then
                            index = i-1
                            break
                        end
                    end

                    self.m_IsPlaying = true
                    self:DoAni(anisetting[index*2])

                    self:HideVoicePlayingEffect()
                    if self.m_SoundPlayingEvent then
                        SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
                        self.m_SoundPlayingEvent = nil
                    end
                    self.m_AniSoundPlayingEvent = SoundManager.Inst:PlayOneShot(anisetting[index*2+1],Vector3.zero,nil,0)
                end

            end
        end
        -- end
    end)
    self.m_RO = nil


    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        local data = NPC_NPC.GetData(id)
        ro:LoadMain(CClientNpc.GetNPCPrefabPath(data),nil,false,false)
        self.m_RO = ro
        self.m_RO.Scale = 1
    end)
    self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__NpcPreview__", self.m_ModelTextureLoader,180,0.05,-1,4.66,false,true,1,true)

    self:InitMail(id)
    self:InitStory(id)
end

function CLuaNpcHaoGanDuWnd:OnDestroy()
    if self.m_ChangFxTick then
        UnRegisterTick(self.m_ChangFxTick)
        self.m_ChangFxTick=nil
    end
    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__NpcPreview__")

    --停止声音
    if self.m_AniSoundPlayingEvent then
        SoundManager.Inst:StopSound(self.m_AniSoundPlayingEvent)
        self.m_AniSoundPlayingEvent=nil
    end
    if self.m_SoundPlayingEvent then
        SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
        self.m_SoundPlayingEvent = nil
    end

    --中断tick
    if self.m_SoundPlayingTick then
        UnRegisterTick(self.m_SoundPlayingTick)
        self.m_SoundPlayingTick = nil
    end

    if self.m_SendMailFxTick then
        UnRegisterTick(self.m_SendMailFxTick)
        self.m_SendMailFxTick = nil
    end
end

function CLuaNpcHaoGanDuWnd:OnClipMove(panel)
    local corners = panel.worldCorners
    local panelCenter = CommonDefs.op_Multiply_Vector3_Single((CommonDefs.op_Addition_Vector3_Vector3(corners[2], corners[0])), 0.5)
    local tableTf = panel.transform:Find("Table")
    local localCenter = tableTf:InverseTransformPoint(panelCenter)
    local localHeight = math.abs(tableTf:InverseTransformPoint(corners[2]).y - tableTf:InverseTransformPoint(corners[0]).y)/2

    for i,child in ipairs(self.m_NpcHeadList) do
        local coeff = math.abs(self.m_NpcHeadOriginLocalPosition[i].y - localCenter.y) / localHeight
        LuaUtils.SetLocalPositionX(child.transform,(-coeff)*coeff*200)
    end
end

function CLuaNpcHaoGanDuWnd:InitGift(id)
     local tf = self.transform:Find("Content/Gift")

    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = nil
    local valueLabel = tf:Find("ValueLabel"):GetComponent(typeof(UILabel))
    valueLabel.text = nil
    local label1 = tf:Find("Label1").gameObject
    label1:SetActive(false)

    local zengsongButton = tf:Find("IncreaseButton").gameObject
    UIEventListener.Get(zengsongButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.NpcHaoGanDuBuyGiftCountWnd)
    end)

    local zengsongLabel = tf:Find("ZengSongLabel"):GetComponent(typeof(UILabel))
    local setting = NpcHaoGanDu_Setting.GetData()
    if self.m_NpcData[id] then
        local leftTime = setting.FreeZengSongNum + self.m_NpcData[id].BuyNum - self.m_NpcData[id].ZengSongNum
        zengsongLabel.text = tostring(leftTime)
    else
        zengsongLabel.text = tostring(setting.FreeZengSongNum)
    end

    local itemProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp or nil

    self.m_GiftList = {}
    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i=1,bagSize do
    	local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
    	local item = CItemMgr.Inst:GetById(itemId)
        if item then
            if NpcHaoGanDu_Gift.Exists(item.TemplateId) then
                table.insert( self.m_GiftList,itemId )
            end
    	end
    end
    local empty = tf:Find("empty").gameObject
    empty:SetActive(#self.m_GiftList==0)


    local tableView = tf:Find("ScrollView"):GetComponent(typeof(QnTableView))
    self.m_TableView = tableView
    tableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_GiftList
        end,
        function(item,index)
            local itemId = self.m_GiftList[index+1]
            local commonItem = CItemMgr.Inst:GetById(itemId)
            local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
            local bindSprite = item.transform:Find("BindSprite").gameObject
            local star = item.transform:Find("StarSprite").gameObject

            if commonItem.IsItem and commonItem.Item.Count>1  then
                numLabel.text = tostring(commonItem.Item.Count) 
                numLabel.gameObject:SetActive(true)
            else
                numLabel.text = nil
                numLabel.gameObject:SetActive(false)
            end

            bindSprite:SetActive(commonItem.IsBinded)

            star:SetActive(self.m_LikeItems[id][commonItem.TemplateId] or false)

            -- numLabel.text = tostring(lookup[id])
            local icon = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
            -- local itemData = Item_Item.GetData(commonItem.TemplateId)
            -- if itemData then
                icon:LoadMaterial(commonItem.Icon)
            -- end
        end)
        
    tableView.OnSelectAtRow = DelegateFactory.Action_int(function(idx)
        local itemId = self.m_GiftList[idx+1]
        local item = CItemMgr.Inst:GetById(itemId)
        if item then
            label1:SetActive(true)
            nameLabel.text = item.Name
            local giftData = NpcHaoGanDu_Gift.GetData(item.TemplateId)
            local multiple = 1
            if id == 20021697 then
                multiple = giftData.PuSongLing>0 and giftData.PuSongLing or 1
            elseif id == 20021698 then
                multiple = giftData.Achu>0 and giftData.Achu or 1
            elseif id == 20021699 then
                multiple = giftData.XiaoTianQuan>0 and giftData.XiaoTianQuan or 1
            end
            valueLabel.text = math.floor(giftData.Basic * multiple)
        else
            label1:SetActive(false)
            nameLabel.text = nil
            valueLabel.text = nil
        end
    end)

    tableView:ReloadData(false, false)

    local giftButton = tf:Find("GiftButton").gameObject
    UIEventListener.Get(giftButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local row = tableView.currentSelectRow
        if row<0 then
            g_MessageMgr:ShowMessage("NpcHaoGanDu_No_Gift")
            return
        end
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_GiftList[row+1])
        if itemInfo then
            local commonItem = CItemMgr.Inst:GetById(itemInfo.itemId)
            local isLike = self.m_LikeItems[CLuaNpcHaoGanDuWnd.m_NpcId][commonItem.TemplateId]
            if commonItem.IsBinded then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NpcHaoGanDu_Gift_IsBind"), DelegateFactory.Action(function () 
                    if isLike then
                        self:DoAni("happy01")
                    end
                    Gac2Gas.RequestZengSongGiftToNpc(CLuaNpcHaoGanDuWnd.m_NpcId,1,itemInfo.pos)
                end), nil, nil, nil, false)
            else
                if isLike then
                    self:DoAni("happy01")
                end
                Gac2Gas.RequestZengSongGiftToNpc(CLuaNpcHaoGanDuWnd.m_NpcId,1,itemInfo.pos)
            end
        end
    end)

    self:InitSpecialGift(id)
end

function CLuaNpcHaoGanDuWnd:InitSpecialGift(npcId)
    local tf = self.transform:Find("Content/Gift")

    local specialGiftButton = tf:Find("SpecialGiftButton").gameObject
    UIEventListener.Get(specialGiftButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.NpcHaoGanDuGiftWnd)
    end)

    local label = specialGiftButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    local count = 0
    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    if playProp then
        local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,CLuaNpcHaoGanDuWnd.m_NpcId)
        if ret then--
            CommonDefs.DictIterate(data.SpecialGiftData, DelegateFactory.Action_object_object(function (___key, ___value)
                if ___value>0 then
                    count=count+1
                end
            end))
        end
    end
    label.text = SafeStringFormat3( "%d/%d",count,3 )
    
    local fx = specialGiftButton.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    if count<3 then
        fx:DestroyFx()
        fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        local path={Vector3(27,100,0),Vector3(27,-100,0),Vector3(-27,-100,0),Vector3(-27,100,0),Vector3(27,100,0)}
        local array = Table2Array(path, MakeArrayClass(Vector3))
		LuaTweenUtils.DOKill(fx.FxRoot, true)
        LuaUtils.SetLocalPosition(fx.FxRoot,27,100,0)
        LuaTweenUtils.DODefaultLocalPath(fx.FxRoot,array,2)
    else
        fx:DestroyFx()
    end
end

function CLuaNpcHaoGanDuWnd:InitDoc(id)
    local tf = self.transform:Find("Content/Doc")
    local contentTf = tf:Find("Content")
    local data = NpcHaoGanDu_NPC.GetData(id)
    if data then
        contentTf:GetChild(0):GetComponent(typeof(UILabel)).text = tostring(data.Height)
        contentTf:GetChild(1):GetComponent(typeof(UILabel)).text = tostring(data.Age)
        contentTf:GetChild(2):GetComponent(typeof(UILabel)).text = data.Character
        contentTf:GetChild(3):GetComponent(typeof(UILabel)).text = tostring(data.Weight)
        contentTf:GetChild(4):GetComponent(typeof(UILabel)).text = data.BirthDay
        contentTf:GetChild(5):GetComponent(typeof(UILabel)).text = tostring(data.Curator)
        local descLabel = tf:Find("ScrollView/DescLabel"):GetComponent(typeof(UILabel))
        descLabel.text = self:GetIntroduction(id)--data.Introduction
        NGUITools.UpdateWidgetCollider(descLabel.gameObject,true)
    end
end

function CLuaNpcHaoGanDuWnd:GetName(npcId)
    local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or nil
    local haveTask = function(taskId)
        if taskId==0 then return true end
        if taskProp then
            return taskProp:IsMainPlayerTaskFinished(taskId)
        else
            return false
        end
    end

    local ret = NpcHaoGanDu_NPC.GetData(npcId).Name
    NpcHaoGanDu_Introduction.Foreach(function(k,v)
        if v.NpcId==npcId and haveTask(v.TaskID) then
            ret = v.NpcName
        end
    end)
    return ret
end

function CLuaNpcHaoGanDuWnd:GetIntroduction(npcId)
    local ret = NpcHaoGanDu_NPC.GetData(npcId).Introduction
    local taskProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.TaskProp or nil
    local haveTask = function(taskId)
        if taskId==0 then return true end
        if taskProp then
            return taskProp:IsMainPlayerTaskFinished(taskId)
        else
            return false
        end
    end

    NpcHaoGanDu_Introduction.Foreach(function(k,v)
        if v.NpcId==npcId and haveTask(v.TaskID) then
            ret = v.Introduction
        end
    end)
    return ret
end

function CLuaNpcHaoGanDuWnd:HideVoicePlayingEffect()
    local tf = self.transform:Find("Content/Voice")
    local grid = tf:Find("ScrollView/Grid")
    for i=1,grid.childCount do
        local child=grid:GetChild(i-1)
        child:Find("Playing").gameObject:SetActive(false)
    end
end
function CLuaNpcHaoGanDuWnd:OnClickPlayVoiceButton(v,g)
    local parent = g.transform.parent.parent
    local index = g.transform.parent:GetSiblingIndex()

    local cancle = false
    for i=1,parent.childCount do
        if i-1==index then
            local effectNode = parent:GetChild(i-1):Find("Playing").gameObject
            if not effectNode.activeSelf then
                effectNode:SetActive(true)
            else
                effectNode:SetActive(false)
                cancle=true
            end
        else
            local effectNode = parent:GetChild(i-1):Find("Playing").gameObject
            effectNode:SetActive(false)
        end
    end
    --播放完毕，停止播放
    if self.m_SoundPlayingEvent then
        SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
        self.m_SoundPlayingEvent=nil
    end
    if self.m_SoundPlayingTick then
        UnRegisterTick(self.m_SoundPlayingTick)
        self.m_SoundPlayingTick = nil
    end

    if cancle then
        self:SetVoiceDesc(nil)
    else
        local callback = function()
            g.transform.parent:Find("Playing").gameObject:SetActive(false)
            self:SetVoiceDesc(nil)

            if self.m_SoundPlayingEvent then
                SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
                self.m_SoundPlayingEvent=nil
            end

            self.m_SoundPlayingTick = nil
        end

        self:SetVoiceDesc(v.Details)

        self.m_SoundPlayingTick = RegisterTickOnce(callback,v.Duration*1000)

        if self.m_AniSoundPlayingEvent then
            SoundManager.Inst:StopSound(self.m_AniSoundPlayingEvent)
            self.m_AniSoundPlayingEvent = nil
        end

        self.m_SoundPlayingEvent = SoundManager.Inst:PlayOneShot(v.Path,Vector3.zero,nil,0)
        self:DoAni(v.Animation)
    end
end

function CLuaNpcHaoGanDuWnd:DoChangeAni()
    self.m_IsPlaying = true
    self.m_RO.AniEndCallback = nil
    self.m_RO:LoadMain("Character/NPC/lnpc512/Prefab/lnpc512_01.prefab",nil,false,false)--变成狗
    self.m_RO.Scale = 0.5
    self.m_RO:DoAni("change01",false,0,1,0.2,true,1)
    self.m_RO.AniEndCallback = DelegateFactory.Action(function()
        self.m_RO.AniEndCallback = nil

        local fx = CEffectMgr.Inst:AddObjectFX(88800219, self.m_RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        self.m_RO:AddFX("ChangeFX", fx)

        local data = NPC_NPC.GetData(CLuaNpcHaoGanDuWnd.m_NpcId)
        self.m_RO:LoadMain(CClientNpc.GetNPCPrefabPath(data),nil,false,false)
        self.m_RO.Scale = 1
        self.m_RO:DoAni("stand01", true, 0, 1.0, 0.2, true, 1.0)
        self.m_RO.AniEndCallback = nil

        self.m_IsPlaying = false
    end)
end
--
function CLuaNpcHaoGanDuWnd:DoAni(ani)
    if self.m_ChangFxTick then
        UnRegisterTick(self.m_ChangFxTick)
        self.m_ChangFxTick=nil
    end
    if self.m_RO then
        self.m_IsPlaying = true
        if ani=="change01" and CLuaNpcHaoGanDuWnd.m_NpcId==20021699 then
            self.m_RO.AniEndCallback = nil
            self.m_RO:DoAni(ani,false,0,1,0.2,true,1)

            self.m_ChangFxTick = RegisterTickOnce(function()
                local fx = CEffectMgr.Inst:AddObjectFX(88800219, self.m_RO,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
                self.m_RO:AddFX("ChangeFX", fx)
                self.m_ChangFxTick=nil
            end,2000)

            self.m_RO.AniEndCallback = DelegateFactory.Action(function()
                self:DoChangeAni()
            end)
        else
            self.m_RO:DoAni(ani,false,0,1,0.2,true,1)
            self.m_RO.AniEndCallback = DelegateFactory.Action(function()
                self.m_RO.AniEndCallback = nil
                self.m_RO:DoAni("stand01", true, 0, 1.0, 0.2, true, 1.0)
                self.m_IsPlaying = false
            end)
        end
    end
end

function CLuaNpcHaoGanDuWnd:InitVoice(id)
    local list={}
    local keys = {}
    NpcHaoGanDu_Voice.Foreach(function(k,v)
        if v.NpcId == id then
            table.insert( keys,k )
        end
    end)
    for i,v in ipairs(keys) do
        table.insert( list,NpcHaoGanDu_Voice.GetData(v) )
    end

    local myGrade = 1
    if self.m_NpcData[id] then
        myGrade = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(self.m_NpcData[id].HaoGanDu)
    end

    local tf = self.transform:Find("Content/Voice")

    local item = tf:Find("ScrollView/Item").gameObject
    item:SetActive(false)
    local grid = tf:Find("ScrollView/Grid").gameObject
    Extensions.RemoveAllChildren(grid.transform)

    for i,v in ipairs(list) do
        local go = NGUITools.AddChild(grid,item)
        go:SetActive(true)
        go:GetComponent(typeof(UITexture)).enabled = i%2==1

        local label1=go.transform:Find("Label1"):GetComponent(typeof(UILabel))
        label1.text = v.Description
        local label2=go.transform:Find("Label2"):GetComponent(typeof(UILabel))
        label2.text = v.Title
        local playButton = go.transform:Find("PlayButton").gameObject

        UIEventListener.Get(playButton).onClick=DelegateFactory.VoidDelegate(function(g)
            self:OnClickPlayVoiceButton(v,g)
        end)
        local playing = go.transform:Find("Playing").gameObject
        playing:SetActive(false)

        if v.Grade>myGrade then
            --锁住
            label2.text = LocalString.GetString("（未解锁）")--"????"
            playButton:SetActive(false)
            go.transform:Find("Lock").gameObject:SetActive(true)
            go:GetComponent(typeof(UIWidget)).alpha = 0.5
        end
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()
end
function CLuaNpcHaoGanDuWnd:SetVoiceDesc(text,callback)
    local descLabel = self.transform:Find("Overlay/VoiceDescLabel"):GetComponent(typeof(UILabel))
    if text and text~="" then
        descLabel.text=text
        descLabel.gameObject:SetActive(true)
        UIEventListener.Get(descLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            -- if callback then
            --     callback()
            -- end
            descLabel.gameObject:SetActive(false)
        end)
    else
        descLabel.gameObject:SetActive(false)
        -- UIEventListener.Get(descLabel.gameObject).onClick = nil
    end
end

function CLuaNpcHaoGanDuWnd:ShowSendMailFx()
    if self.m_SendMailFxTick then
        UnRegisterTick(self.m_SendMailFxTick)
        self.m_SendMailFxTick = nil
    end

    local fx = self.transform:Find("Overlay/Fx"):GetComponent(typeof(CUIFx))
    fx:DestroyFx()
    fx:LoadFx("Fx/UI/Prefab/UI_haogandujixin.prefab")

    self.m_SendMailFxTick = RegisterTickOnce(function()
        fx:DestroyFx()
        self.m_SendMailFxTick = nil
    end,4000)
end

--发送邮件
function CLuaNpcHaoGanDuWnd:SendMail(npcId,i)
    --打开邮件
    local letterDesign = self.m_LetterLookup[npcId][i]
    CLuaLetterDisplayMgr.SendLetter(letterDesign.Content,function()
        Gac2Gas.RequestSendLetterToNpc(npcId,i,0)
        self:ShowSendMailFx()
    end)
end

function CLuaNpcHaoGanDuWnd:OnClickSendMailButton(go,npcId)
    local popupList={}
    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,npcId)
    if ret then
        local letterFlag = data.LetterFlag
        local miscFlag = data.MiscFlag
        for i=1,30 do
            local letterDesign = self.m_LetterLookup[npcId][i]
            if letterDesign and miscFlag:GetBit(i) and not letterFlag:GetBit(i) then
                if letterDesign.Type==LuaEnumNpcHaoGanDuLetterType.ePlayerSend2Npc then
                    local item = PopupMenuItemData(letterDesign.Title,DelegateFactory.Action_int(function(index) 
                                    self:SendMail(npcId,i)
                                end),false, nil, EnumPopupMenuItemStyle.Light)
                    table.insert( popupList,item )
                end
            end
        end
    end
    if #popupList>0 then
        local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, CPopupMenuInfoMgr.AlignType.Top)
    else
        g_MessageMgr:ShowMessage("NpcHaoGanDu_No_Mail_Send")
    end
end

function CLuaNpcHaoGanDuWnd:InitMail(id)
    local tf = self.transform:Find("Content/Mail")

    local empty = tf:Find("empty").gameObject
    empty:SetActive(true)

    local sendMailBtn = tf:Find("EditButton").gameObject
    UIEventListener.Get(sendMailBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickSendMailButton(go,id)
    end)

    local item = FindChild(tf,"Item").gameObject
    item:SetActive(false)
    local grid = FindChild(tf,"Grid").gameObject
    Extensions.RemoveAllChildren(grid.transform)

    local tabView = tf:Find("TabBar"):GetComponent(typeof(QnTabView))
    tabView.OnSelect =  DelegateFactory.Action_QnTabButton_int(function (btn, index) 
        Extensions.RemoveAllChildren(grid.transform)

        local buttons = tabView.m_TabButtons
        for i=1,buttons.Length do
            local bg = buttons[i-1].transform:Find("tab"):GetComponent(typeof(CUITexture))
            if buttons[i-1]==btn then
                bg:LoadMaterial("UI/Texture/Transparent/Material/haogandu_shuxin_xuanzhong.mat")
            else
                bg:LoadMaterial("UI/Texture/Transparent/Material/haogandu_shuxin_zhengchang.mat")
            end
        end
    end)

    local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
    if playProp then
        local ret,data = CommonDefs.DictTryGet_LuaCall(playProp.NpcHaoGanDuData,id)
        if ret then
            local letterFlag = data.LetterFlag--value为1的信件展示给玩家，包括npc发给玩家的信和玩家发给NPC的信
            local miscFlag = data.MiscFlag--对NPC发给玩家的信来说为1表示玩家已经读过了；对玩家发给NPC的信来说为1表示玩家可以发送此信
            local receiveMails = {}
            local sendMails = {}
            for i=1,30 do
                local letterDesign = self.m_LetterLookup[id][i]
                if letterDesign and letterFlag:GetBit(i) then
                    if letterDesign.Type==LuaEnumNpcHaoGanDuLetterType.eNpcSend2Player or letterDesign.Type==LuaEnumNpcHaoGanDuLetterType.eNpcReplyPlayer then
                        table.insert( receiveMails,i )
                    else
                        table.insert( sendMails,i )
                    end
                end
            end
            table.sort( receiveMails )
            table.sort( sendMails )

            tabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (btn, index) 
                Extensions.RemoveAllChildren(grid.transform)

                local buttons = tabView.m_TabButtons
                for i=1,buttons.Length do
                    local bg = buttons[i-1].transform:Find("tab"):GetComponent(typeof(CUITexture))
                    if buttons[i-1]==btn then
                        bg:LoadMaterial("UI/Texture/Transparent/Material/haogandu_shuxin_xuanzhong.mat")
                    else
                        bg:LoadMaterial("UI/Texture/Transparent/Material/haogandu_shuxin_zhengchang.mat")
                    end
                end

                if index==0 then
                    if #receiveMails==0 then
                        empty:SetActive(true)
                    else
                        empty:SetActive(false)
                    end
                    for i,v in ipairs(receiveMails) do
                        local letterDesign = self.m_LetterLookup[id][v]
                        -- if letterDesign then
                        local go = NGUITools.AddChild(grid,item)
                        go:SetActive(true) 
                        go:GetComponent(typeof(UITexture)).enabled = i%2==0 and false or true
                        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
                        label.text = letterDesign.Title
                        local btn = go.transform:Find("MailButton").gameObject
                        local btn2 = go.transform:Find("MailButton2").gameObject
    
                        --已读过的不需要回信
                        -- if not miscFlag:GetBit(v) and letterDesign.CanReply>0 then

                        local choices = nil
                        local choiceNames = nil
                        local choiceContents = nil
                        --看需不需要回复
                        if letterDesign.ReplyChoices then
                            choices = {}
                            choiceNames = {}
                            choiceContents = {}
                            for i=1,letterDesign.ReplyChoices.Length do
                                local letterId = letterDesign.ReplyChoices[i-1]
                                table.insert( choices,letterId )
                                table.insert( choiceNames, self.m_LetterLookup[id][letterId].Summary )
                                table.insert( choiceContents, self.m_LetterLookup[id][letterId].Content )

                                --如果已经回复过了 就不需要回复了
                                if letterFlag:GetBit(letterId) then
                                    choices={}
                                    choiceNames = {}
                                    choiceContents = {}
                                    break
                                end
                            end
                        end
                        local haveRead = miscFlag:GetBit(v) 
                        local onClick = function(go)
                            btn:SetActive(true)
                            btn2:SetActive(false)
                            Gac2Gas.ReadNpcLetter(id,v)--读信
                            if choices then
                                CLuaLetterDisplayMgr.ShowLetterDisplayWnd(letterDesign.Content,choiceNames,choiceContents,function(idx)
                                    local replyLetterId = choices[idx]
                                    self:ShowSendMailFx()
                                    Gac2Gas.RequestSendLetterToNpc(id,replyLetterId,v)
                                end,haveRead)
                            else
                                CLuaLetterDisplayMgr.ShowLetterDisplayWnd(letterDesign.Content,nil,nil,nil,true)
                            end
                        end

                        --是否读过
                        if not haveRead then--没读过
                            btn:SetActive(false)
                            UIEventListener.Get(btn2).onClick = DelegateFactory.VoidDelegate(onClick)
                        else--读过了
                            btn2:SetActive(false)
                            UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(onClick)
                        end
                    end
                else
                    if #sendMails==0 then
                        empty:SetActive(true)
                    else
                        empty:SetActive(false)
                    end
                    for i,v in ipairs(sendMails) do
                        local letterDesign = self.m_LetterLookup[id][v]
                        local go = NGUITools.AddChild(grid,item)
                        go:SetActive(true)
                        go:GetComponent(typeof(UITexture)).enabled = i%2==0 and false or true
                        
                        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
                        label.text = letterDesign.Title
                        local btn = go.transform:Find("MailButton").gameObject
                        local btn2 = go.transform:Find("MailButton2").gameObject
                        btn2:SetActive(false)

                        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                            CLuaLetterDisplayMgr.ShowLetterDisplayWnd(letterDesign.Content,nil,nil,nil,true)
                        end)
                    end
                end
                grid:GetComponent(typeof(UIGrid)):Reposition()
            end)
            tabView:ChangeTo(tabView.CurrentSelectTab>0 and tabView.CurrentSelectTab or 0)
        end
    end
end

function CLuaNpcHaoGanDuWnd:InitStory(id)
    local tf = self.transform:Find("Content/Story")
    local item = tf:Find("ScrollView/Item").gameObject
    item:SetActive(false)

    
    local myHaoGanDu = 0
    if self.m_NpcData[id] then
        myHaoGanDu = self.m_NpcData[id].HaoGanDu
    end
    tf:Find("ScrollView"):GetComponent(typeof(UIScrollView)):ResetPosition()
    local grid = tf:Find("ScrollView/Grid").gameObject
    Extensions.RemoveAllChildren(grid.transform)

    for i,v in ipairs(self.m_StoryLookup[id]) do
        local go = NGUITools.AddChild(grid,item)
        go:SetActive(true)

        local label = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
        label.text = v.Title
        local label2 = go.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
        label2.text = v.Content
        -- label2:UpdateNGUIText()
        local split = go.transform:Find("Split").gameObject
        local bg = go:GetComponent(typeof(UITexture))


        local btn = go.transform:Find("Change").gameObject

        local lock = go.transform:Find("Lock").gameObject
        if v.HaoGanDu > myHaoGanDu then
            --锁定
            lock:SetActive(true)
            btn:SetActive(false)
            split:SetActive(false)
            label2.gameObject:SetActive(false)
            bg.height = 90
            local ctexture = bg:GetComponent(typeof(CUITexture))
            ctexture:LoadMaterial("UI/Texture/Transparent/Material/haogandu_xiaozhuan_suo.mat")

            label.text=LocalString.GetString("（未解锁）")--"????"
        else
            local b = NGUIMath.CalculateRelativeWidgetBounds(label2.transform,true)
            bg.height = 90 + math.floor(b.size.y) + 20
            -- bg:ResizeCollider()
            NGUITools.UpdateWidgetCollider(bg.gameObject,true)
            --
            lock:SetActive(false)
            btn:SetActive(true)
            split:SetActive(true)
            label2.gameObject:SetActive(true)

            UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(g)
                local parent = g.transform.parent
                local split = parent:Find("Split").gameObject
                local contentLabel = parent:Find("ContentLabel").gameObject
                local bg = parent:GetComponent(typeof(UITexture))
                
                local label = g:GetComponent(typeof(UILabel))
                if label.text == LocalString.GetString("收起") then
                    label.text = LocalString.GetString("展开")
                    split:SetActive(false)
                    contentLabel:SetActive(false)
                    bg.height = 90
                else
                    label.text = LocalString.GetString("收起")
                    split:SetActive(true)
                    contentLabel:SetActive(true)
                    local b = NGUIMath.CalculateRelativeWidgetBounds(contentLabel.transform)
                    bg.height = 90 + math.floor(b.size.y) + 20
                end
    
                grid:GetComponent(typeof(UITable)):Reposition()
            end)
        end
    end
    grid:GetComponent(typeof(UITable)):Reposition()
end

function CLuaNpcHaoGanDuWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("SyncNpcHaoGanDuInfo", self, "OnSyncNpcHaoGanDuInfo")
    g_ScriptEvent:AddListener("SyncNpcHaoGanDuSpecialGiftInfo",self,"OnSyncNpcHaoGanDuSpecialGiftInfo")
    
end
function CLuaNpcHaoGanDuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("SyncNpcHaoGanDuInfo", self, "OnSyncNpcHaoGanDuInfo")
    g_ScriptEvent:RemoveListener("SyncNpcHaoGanDuSpecialGiftInfo",self,"OnSyncNpcHaoGanDuSpecialGiftInfo")
end

function CLuaNpcHaoGanDuWnd:CleanNpcAni()
    if self.m_AniSoundPlayingEvent then
        SoundManager.Inst:StopSound(self.m_AniSoundPlayingEvent)
        self.m_AniSoundPlayingEvent=nil
    end
    self:HideVoicePlayingEffect()
    
    if self.m_SoundPlayingEvent then
        SoundManager.Inst:StopSound(self.m_SoundPlayingEvent)
        self.m_SoundPlayingEvent=nil
    end
    if self.m_SoundPlayingTick then
        UnRegisterTick(self.m_SoundPlayingTick)
        self.m_SoundPlayingTick = nil
    end
end

function CLuaNpcHaoGanDuWnd:OnSyncNpcHaoGanDuSpecialGiftInfo(npcId,newGift)
    if CLuaNpcHaoGanDuWnd.m_NpcId==npcId then
        self:InitSpecialGift(npcId)
        if newGift then
            --停掉动画
            -- self:CleanNpcAni()
            --播放happy动作
            -- self:DoAni("happy01")
        end
    end
end

function CLuaNpcHaoGanDuWnd:OnSyncNpcHaoGanDuInfo(npcId,upgrade,reason,addHaoGanDu,newVoice,newStory)
    self:InitNpcData()
    if npcId == CLuaNpcHaoGanDuWnd.m_NpcId then
        --更新赠礼数量
        local tf = self.transform:Find("Content/Gift")
        local zengsongLabel = tf:Find("ZengSongLabel"):GetComponent(typeof(UILabel))
        local setting = NpcHaoGanDu_Setting.GetData()
        if self.m_NpcData[npcId] then
            local leftTime = setting.FreeZengSongNum+self.m_NpcData[npcId].BuyNum - self.m_NpcData[npcId].ZengSongNum
            zengsongLabel.text = tostring(leftTime)
        else
            zengsongLabel.text = tostring(setting.FreeZengSongNum)
        end

        --更新好感度
        self:InitHaoGanDu(npcId)
        --更新描述文字
        self:InitDoc(npcId)

        --升级
        -- if upgrade then
        -- end

        if reason==LuaEnumSyncNpcHaoGanDuInfoReason.eReadLetter 
            or reason == LuaEnumSyncNpcHaoGanDuInfoReason.eSendLetter
            or reason == LuaEnumSyncNpcHaoGanDuInfoReason.eReceiveLetter
            or reason == LuaEnumSyncNpcHaoGanDuInfoReason.eNewLetterCanSend then
            --读信
            self:InitMail(npcId)
        end
        if addHaoGanDu then
            --刷新邮件逻辑
            self:InitMail(npcId)

            --提升特效
            local fx = self.m_ModelTexture.transform:Find("TiSheng"):GetComponent(typeof(CUIFx))
            fx:DestroyFx()
            fx:LoadFx("Fx/UI/Prefab/UI_HaoGanDu_TiSheng_01.prefab")
        end

        if newVoice then
            self:InitVoice(npcId)
            self:PlayNoticeFx(4)
        end
        if newStory then
            self:InitStory(npcId)
            self:PlayNoticeFx(5)
        end
        if reason == LuaEnumSyncNpcHaoGanDuInfoReason.eReceiveLetter or reason == LuaEnumSyncNpcHaoGanDuInfoReason.eNewLetterCanSend then
            self:PlayNoticeFx(6)
        end

    end
end
function CLuaNpcHaoGanDuWnd:PlayNoticeFx(idx)
    -- local tabView = self.transform:Find("Tab"):GetComponent(typeof(QnTabView))
    --如果在当前tab下就不用提醒了
    if self.m_TabView.CurrentSelectTab==idx-1 then
        return
    end
    local fx = self.m_TabView.m_TabButtons[idx-1].transform:Find("Fx"):GetComponent(typeof(CUIFx))
    fx:DestroyFx()
    fx:LoadFx("Fx/UI/Prefab/UI_haogandutixing.prefab")
end

function CLuaNpcHaoGanDuWnd:InitHaoGanDu(npcId)


    local tf = self.transform:Find("Content/Gift")
    local levelLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local haoganduLabel = tf:Find("HaoGanDuLabel"):GetComponent(typeof(UILabel))

    local setting = NpcHaoGanDu_Setting.GetData()
    local field = setting.HaoGanDuGrade
    
    local haogandu=0
    if self.m_NpcData[npcId] then
        haogandu = self.m_NpcData[npcId].HaoGanDu
        levelLabel.text = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(haogandu)
    else
        haogandu=0
        levelLabel.text = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(0)
    end
    local rangeUp = 999999
    local rangeDown=0
    for i=1,field.Length do
        local low = field[i-1][1]
        local high = field[i-1][2]
        if haogandu>=low and haogandu<=high then
            rangeUp=high
            rangeDown = low
            break
        elseif haogandu>high then
            rangeUp = high
            rangeDown = low
        end
    end
    haoganduLabel.text = SafeStringFormat3("%d/%d",haogandu,rangeUp)
    local slider=tf:Find("HaoGanDuSlider"):GetComponent(typeof(UISlider))
    slider.value = (haogandu-rangeDown)/(rangeUp-rangeDown)


    local tf = self.transform:Find("Content/HaoGan")
    local haoganduLabel = tf:Find("HaoGanDuLabel"):GetComponent(typeof(UILabel))
    local levelLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))

    if self.m_NpcData[npcId] then
        -- haoganduLabel.text = tostring(self.m_NpcData[npcId].HaoGanDu)
        levelLabel.text = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(self.m_NpcData[npcId].HaoGanDu)
    else
        -- haoganduLabel.text = "0"
        levelLabel.text = CLuaNpcHaoGanDuMgr.GetHaoGanDuLevel(0)
    end
    haoganduLabel.text = SafeStringFormat3("%d/%d",haogandu,rangeUp)
end

function CLuaNpcHaoGanDuWnd:OnSendItem(args)
    local itemId = args[0]
    if not(itemId==nil or itemId=="") then
        for i,v in ipairs(self.m_GiftList) do
            if v==itemId then
                local item = self.m_TableView:GetItemAtRow(i-1)
                local commonItem = CItemMgr.Inst:GetById(itemId)
                local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
                if commonItem.IsItem then
                    numLabel.text = commonItem.Item.Count>1 and tostring(commonItem.Item.Count) or nil
                end
                break
            end
        end
    end
end

function CLuaNpcHaoGanDuWnd:OnSetItemAt(args)
    local place, position, oldItemId, newItemId=args[0],args[1],args[2],args[3]
    if place == EnumItemPlace.Bag then
        for i,v in ipairs(self.m_GiftList) do
            if v==oldItemId then
                self:InitGift(CLuaNpcHaoGanDuWnd.m_NpcId)
                break
            end
        end
    end
end

function CLuaNpcHaoGanDuWnd:GetGuideGo(methodName)
    if methodName=="GetAChuHead" then
        return self.m_NpcHeadList[2]
    elseif methodName == "GetDocTab" then
        return self.m_TabView.m_TabButtons[1].gameObject
    elseif methodName == "GetGiftTab" then
        return self.m_TabView.m_TabButtons[2].gameObject
    elseif methodName=="GetTipButton" then
        return self.m_TipButton

    end
end
