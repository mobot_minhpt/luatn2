-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CHuanLingResult = import "L10.UI.CHuanLingResult"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local Item_Item = import "L10.Game.Item_Item"
local TweenPosition = import "TweenPosition"
CHuanLingResult.m_Init_CS2LuaHook = function (this, lingfuId, quality, pos) 

    this.m_LingfuId = lingfuId
    local itemInfo = CItemMgr.Inst:GetById(lingfuId)
    local item = Item_Item.GetData(itemInfo.TemplateId)
    if item ~= nil then
        this.itemTexture:LoadMaterial(item.Icon)
    end
    this.qualitySprite.spriteName = CZuoQiMgr.GetLingFuItemCellBorder(quality)
    local xiantiaoName = System.String.Format(this.m_HuanLingXianTiaoFx, this.fxLevel[quality - 1])
    local xingxingName = System.String.Format(this.m_HuanLingXingXingFx, this.fxLevel[quality - 1])
    this.xiantiaoFx:LoadFx(xiantiaoName)
    this.xingxingFx:LoadFx(xingxingName)

    local tweener = CommonDefs.GetComponent_Component_Type(this, typeof(TweenPosition))
    tweener:PlayForward()
end
CHuanLingResult.m_OnResultClicked_CS2LuaHook = function (this, go) 
    local item = CItemMgr.Inst:GetById(this.m_LingfuId)
    if item ~= nil then
        CItemInfoMgr.ShowLinkItemInfo(item, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
    end
end
