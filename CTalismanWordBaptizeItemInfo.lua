-- Auto Generated!!
local Boolean = import "System.Boolean"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTalismanWordBaptizeItemInfo = import "L10.UI.CTalismanWordBaptizeItemInfo"
local CTalismanWordBaptizeMgr = import "L10.UI.CTalismanWordBaptizeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EventManager = import "EventManager"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local IdPartition = import "L10.Game.IdPartition"
local Item_Item = import "L10.Game.Item_Item"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local String = import "System.String"
--local Talisman_ResetWord = import "L10.Game.Talisman_ResetWord"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector4 = import "UnityEngine.Vector4"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanWordBaptizeItemInfo.m_Init_CS2LuaHook = function (this) 

    this.talismanIcon.material = nil
    this.talismanQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    if this.talismanNameLabel ~= nil then
        this.talismanNameLabel.text = ""
    end
    this.talismanBindSprite.spriteName = nil
    this.itemGo:SetActive(false)
    this.jadeCheckBox.Selected = false
    this.jadeGo:SetActive(false)
    this.countAvailable = false
    this.acquireGo:SetActive(false)
    this.yinliangCtrl:SetCost(0)
    if CTalismanWordBaptizeMgr.selectTalisman == nil then
        return
    end
    this.talisman = CItemMgr.Inst:GetById(CTalismanWordBaptizeMgr.selectTalisman.itemId)
    if this.talisman ~= nil and this.talisman.IsEquip and IdPartition.IdIsTalisman(this.talisman.TemplateId) then
        this.jadeCheckBox.OnValueChanged = MakeDelegateFromCSFunction(this.OnCheckBoxChanged, MakeGenericClass(Action1, Boolean), this)
        this.talismanIcon:LoadMaterial(this.talisman.Icon)
        this.talismanQualitySprite.spriteName = CUICommonDef.GetItemCellBorder(this.talisman.Equip.QualityType)
        if this.talismanNameLabel ~= nil then
            this.talismanNameLabel.text = this.talisman.Name
            this.talismanNameLabel.color = this.talisman.Equip.DisplayColor
        end
        this.talismanBindSprite.spriteName = this.talisman.BindOrEquipCornerMark
        Talisman_ResetWord.Foreach(function (index, data) 
            if data.Color == EnumToInt(this.talisman.Equip.QualityType) or (bit.bor(data.Color, EnumQualityType_lua.Dark)) == EnumToInt(this.talisman.Equip.QualityType) then
                if data.TCRange[0] <= this.talisman.Equip.NeedLevel and data.TCRange[1] >= this.talisman.Equip.NeedLevel then
                    this.itemTemplateId = CTalismanWordBaptizeMgr.baseWordBaptize and data.ResetBasicCost[0] or data.ResetExtraCost[0]
                    this.needCount = CTalismanWordBaptizeMgr.baseWordBaptize and data.ResetBasicCost[1] or data.ResetExtraCost[1]
                    if not this.talisman.IsBinded then
                        this.yinliangCtrl:SetCost(CTalismanWordBaptizeMgr.baseWordBaptize and data.ResetBasicCostYinLiang or data.ResetExtraCostYinLiang)
                    else
                        this.yinliangCtrl:SetCost(0)
                    end
                end
            end
        end)
        local costItem = Item_Item.GetData(this.itemTemplateId)
        if costItem ~= nil then
            this.itemIcon:LoadMaterial(costItem.Icon)
            this.itemNameLabel.text = costItem.Name
            this.itemNameLabel.color = GameSetting_Common_Wapper.Inst:GetColor(costItem.NameColor)
            this:UpdateCostItem()
        end
    end
end
CTalismanWordBaptizeItemInfo.m_UpdateCostItem_CS2LuaHook = function (this) 

    if CTalismanWordBaptizeMgr.selectTalisman ~= nil then
        this.talisman = CItemMgr.Inst:GetById(CTalismanWordBaptizeMgr.selectTalisman.itemId)
    end
    if this.talisman ~= nil and this.talismanNameLabel ~= nil then
        this.talismanNameLabel.text = this.talisman.Name
        this.talismanNameLabel.color = this.talisman.Equip.DisplayColor
    end
    local bindCount, notbindCount
    bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(this.itemTemplateId)
    this.countAvailable = bindCount + notbindCount >= this.needCount
    if bindCount + notbindCount < this.needCount and this.jadeCheckBox.Selected then
        this.itemGo:SetActive(false)
        this.jadeGo:SetActive(true)
        local data = Mall_LingYuMall.GetData(this.itemTemplateId)
        this.lingyuCtrl:SetType(data.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
        this.lingyuCtrl:SetCost(data.Jade * (this.needCount - bindCount - notbindCount))
    else
        this.itemGo:SetActive(true)
        this.jadeGo:SetActive(false)
        if bindCount + notbindCount < this.needCount then
            this.itemAmount.text = System.String.Format("[c][FF0000]{0}[-]/{1}", bindCount + notbindCount, this.needCount)
            this.acquireGo:SetActive(true)
        else
            this.itemAmount.text = System.String.Format("{0}/{1}", bindCount + notbindCount, this.needCount)
            this.acquireGo:SetActive(false)
        end
    end
end
CTalismanWordBaptizeItemInfo.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.talismanQualitySprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.talismanQualitySprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnTalismanClick, VoidDelegate, this), true)
    UIEventListener.Get(this.itemGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.itemGo).onClick, MakeDelegateFromCSFunction(this.OnCostItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireClick, VoidDelegate, this), true)

    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CTalismanWordBaptizeItemInfo.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.talismanQualitySprite.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.talismanQualitySprite.gameObject).onClick, MakeDelegateFromCSFunction(this.OnTalismanClick, VoidDelegate, this), false)
    UIEventListener.Get(this.itemGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.itemGo).onClick, MakeDelegateFromCSFunction(this.OnCostItemClick, VoidDelegate, this), false)
    UIEventListener.Get(this.acquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.acquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireClick, VoidDelegate, this), false)

    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CTalismanWordBaptizeItemInfo.m_UpdateFx_CS2LuaHook = function (this, show) 
    if show then
        if this.fxRoot ~= nil then
            this.fxRoot:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
            local gap = 5
            if this.talismanQualitySprite ~= nil then
                this:DoAni(Vector4(- this.talismanQualitySprite.width * 0.5 + gap, this.talismanQualitySprite.height * 0.5 - gap, this.talismanQualitySprite.width - gap * 2, this.talismanQualitySprite.height - gap * 2), false)
            end
        end
    else
        if this.fxRoot ~= nil then
            this.fxRoot:DestroyFx()
        end
    end
end
