require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local Extensions = import "Extensions"
local CItemMgr = import "L10.Game.CItemMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MsgPackImpl = import "MsgPackImpl"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"
local NGUIText = import "NGUIText"

CLuaQingYuanShouJiMemoirsWnd = class()
RegistClassMember(CLuaQingYuanShouJiMemoirsWnd,"m_closeBtn")
RegistClassMember(CLuaQingYuanShouJiMemoirsWnd,"m_TitleLabel")
RegistClassMember(CLuaQingYuanShouJiMemoirsWnd,"m_ScrollView")
RegistClassMember(CLuaQingYuanShouJiMemoirsWnd,"m_Table")
RegistClassMember(CLuaQingYuanShouJiMemoirsWnd,"m_Template")

function CLuaQingYuanShouJiMemoirsWnd:Awake()
    
end

function CLuaQingYuanShouJiMemoirsWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaQingYuanShouJiMemoirsWnd:Init()
	self.m_closeBtn = self.transform:Find("Background/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_TitleLabel = self.transform:Find("Left/Label"):GetComponent(typeof(UILabel))
	self.m_ScrollView = self.transform:Find("Right/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_Table = self.transform:Find("Right/ScrollView/Table"):GetComponent(typeof(UITable))
	self.m_Template = self.transform:Find("Right/ScrollView/Item").gameObject

	self.m_Template:SetActive(false)

	Extensions.RemoveAllChildren(self.m_Table.transform)

	local playerPromise = nil
	local partnerId = 0
	local partnerName = nil
	local partnerPromise = nil
	local taskTbl = {}

	local item = CItemMgr.Inst:GetById(LuaValentine2019Mgr.MemoirsItemId)

    local raw = item and item.IsItem and item.Item.ExtraVarData.Data
    if raw ~= nil and item.TemplateId == Valentine2019_Setting.GetData().QYSJItemFinish then
        local list = MsgPackImpl.unpack(raw)
        if list and list.Count==22 then
        	playerPromise = tostring(list[0])
        	partnerId = tonumber(list[1])
        	partnerName = tostring(list[2])
        	partnerPromise = tostring(list[3])
        	for i=4,list.Count-1,2 do
        		local data = {}
        		data.taskId = tonumber(list[i])
    			data.finishTime = tonumber(list[i+1])
    			table.insert(taskTbl, data)
        	end
        end
	end
		
	self:InitContent(playerPromise, partnerId, partnerName, partnerPromise, taskTbl)
end

function CLuaQingYuanShouJiMemoirsWnd:InitContent(playerPromise, partnerId, partnerName, partnerPromise, taskTbl)

	local instance = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
	instance:SetActive(true)
	local dateLabel = instance.transform:Find("DateLabel"):GetComponent(typeof(UILabel))
	local contentLabel = instance.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
	dateLabel.text = LocalString.GetString("誓言")
	contentLabel.text = SafeStringFormat3(LocalString.GetString("我的誓言:%s\nTA的誓言:%s"), playerPromise or "", partnerPromise or "")

	for _,data in pairs(taskTbl) do
		local instance = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
		instance:SetActive(true)
		local dateLabel = instance.transform:Find("DateLabel"):GetComponent(typeof(UILabel))
		local contentLabel = instance.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))

		local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(data.finishTime)
		local timeInfo = ToStringWrap(dt, "yyyy-MM-dd")
		dateLabel.text = timeInfo
		local designdata = Valentine2019_QYSJTask.GetData(data.taskId)
		local content = designdata and designdata.Doc or ""
		local text = g_MessageMgr:Format(content, CChatLinkMgr.TranslateToNGUIText(SafeStringFormat3("<link player=%s,%s>", partnerId, partnerName or " ")))
		contentLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(contentLabel.color), text)
		CommonDefs.AddOnClickListener(contentLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnContentLabelClick(contentLabel) end), false)
	end

	self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("与%s在一起的情人节回忆"), partnerName or "")

	self.m_Table:Reposition()
	self.m_ScrollView:ResetPosition()
end

function CLuaQingYuanShouJiMemoirsWnd:OnContentLabelClick(label)
	local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end
