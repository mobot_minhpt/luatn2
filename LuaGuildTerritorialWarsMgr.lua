local CCommonProgressInfoMgr = import "L10.UI.CCommonProgressInfoMgr"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CTeamMgr=import "L10.Game.CTeamMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"

LuaGuildTerritorialWarsMgr = {}
LuaGuildTerritorialWarsMgr.m_IsShowBigMap = true
LuaGuildTerritorialWarsMgr.m_ServerGroupDecided = false

function LuaGuildTerritorialWarsMgr:ShowSmallMap()
    self.m_IsShowBigMap = false
    if self:IsChallengePlayOpen() then
        Gac2Gas.RequestGTWChallengeBattleFieldInfo()
        return
    end
    Gac2Gas.RequestTerritoryWarMapOverview()
end

function LuaGuildTerritorialWarsMgr:IsInPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        return CommonDefs.HashSetContains(GuildTerritoryWar_Setting.GetData().GamePlayId, typeof(UInt32), gamePlayId)  
	end
    return false
end

function LuaGuildTerritorialWarsMgr:UpdateTeamMemberExtStringForGameplay(gameplayId, memberId, content, extraInfo)
    if CScene.MainScene then
        if CommonDefs.HashSetContains(GuildTerritoryWar_Setting.GetData().GamePlayId, typeof(UInt32), gameplayId) then
            CTeamMgr.Inst:UpdateTeamMemberExtString(memberId, content)
        end 
	end
end

function LuaGuildTerritorialWarsMgr:ShowTopRightWnd(wnd)
    LuaTopAndRightMenuWndMgr:ShowTopRightWnd(wnd, CLuaUIResources.GuildTerritorialWarsTopRightWnd, function ()
        return self:IsInPlay() or self:IsInGuide()
    end)
end

function LuaGuildTerritorialWarsMgr:IsPlayOpen()
    return self.m_TerritoryWarStatus > 0
end

LuaGuildTerritorialWarsMgr.m_TerritoryWarStatus = 0
function LuaGuildTerritorialWarsMgr:SyncTerritoryWarStatus(seasonId, status)
    self.m_TerritoryWarStatus = status
    g_ScriptEvent:BroadcastInLua("UpdateTaskPlayViewVisibility")
end

LuaGuildTerritorialWarsMgr.m_PickedRegion = 0
LuaGuildTerritorialWarsMgr.m_RegionGuildInfo = {}
function LuaGuildTerritorialWarsMgr:SendTerritoryWarPickBornRegionInfo(pickedRegion, regionGuildCount)
    local list = MsgPackImpl.unpack(regionGuildCount)
    if not list then return end
    self.m_RegionGuildInfo = {}
    GuildTerritoryWar_Region.Foreach(function (region, data)
		self.m_RegionGuildInfo[region] = 0
	end)
    for i = 0, list.Count-1, 2 do
        local region = list[i]
        local guildCount = list[i+1]
        self.m_RegionGuildInfo[region] = guildCount
    end
    self.m_PickedRegion = pickedRegion
    if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsOccupyWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarPickBornRegionInfo")
    else
        CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsOccupyWnd)
    end
end

LuaGuildTerritorialWarsMgr.m_GridTipData = nil
LuaGuildTerritorialWarsMgr.m_GridTipPos = nil
function LuaGuildTerritorialWarsMgr:ShowMapGridTip(data, btn)
    if self.m_ObserveType == 0 and not self:IsInGuide() then return end
    self.m_GridTipData = data
    self.m_GridTipPos = btn.transform:Find("TopRightRoot").position
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsMapTipWnd)
end

function LuaGuildTerritorialWarsMgr:ShowTeleportProgress(duration)
    LuaCommonProgressWndMgr.m_ShowCloseBtn = true
    LuaCommonProgressWndMgr.m_OnCloseButtonClickAction = function ()
        Gac2Gas.RequestCancelTerritoryWarTeleport()
    end
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        mainplayer:AddTeleportFx()
    end
    duration = duration / 1000
    CCommonProgressInfoMgr.ShowCommonProgressWnd(LocalString.GetString("传送中..."), duration, CCommonProgressInfoMgr.ProgressType.TeleportCasting)
end

LuaGuildTerritorialWarsMgr.m_GuildInfo = {}
LuaGuildTerritorialWarsMgr.m_ObserveType = 0
LuaGuildTerritorialWarsMgr.m_GridId2Level = {}
LuaGuildTerritorialWarsMgr.m_GuildId = 0
LuaGuildTerritorialWarsMgr.m_CurrentWeek = 0
function LuaGuildTerritorialWarsMgr:SendTerritoryWarMapOverview(myGuildId, observeType, currentWeek, guildList, randGrids)
    -- observeType:0 不在比赛服,不实时刷新且需要屏蔽一些按钮 1 在比赛服,需要定时刷新
    self.m_ObserveType = observeType
    self.m_GuildId = myGuildId
    self.m_CurrentWeek = currentWeek
    local list = guildList and MsgPackImpl.unpack(guildList)
    self.m_GuildInfo = {}
    local sortedGuildIdList = {}
    for i = 0, list.Count-1, 5 do
        local guildId = list[i]
        local guildName = list[i+1]
        local allianceGuildId = list[i+2] -- 同盟帮会ID
        -- local slaveGuildId = list[i+3] -- 俘虏了的帮会ID
        local masterGuildId = list[i+3] -- 被俘虏的帮会ID
        local serverName = list[i+4]
        self.m_GuildInfo[guildId] = {guildId = guildId, guildName = guildName, allianceGuildId = allianceGuildId, masterGuildId = masterGuildId, serverName = serverName}
        table.insert(sortedGuildIdList, guildId)
    end
    if #sortedGuildIdList > 0 then
        table.sort(sortedGuildIdList, function (a, b)
            return a < b
        end)
    end
    for i = 1, #sortedGuildIdList do
        local guildId = sortedGuildIdList[i]
        self.m_GuildInfo[guildId].colorIndex = i
    end
    list = randGrids and MsgPackImpl.unpack(randGrids)
    if not list then return end
    local gridId2Level = {}
    for i = 0, list.Count-1, 2 do
        local gridId = list[i]
        local level = list[i+1]
        gridId2Level[gridId] = level
    end
    self.m_GridId2Level = gridId2Level
    if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsMapWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarMap_GridId2Level",gridId2Level)
    else
        CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsMapWnd)
    end
end

function LuaGuildTerritorialWarsMgr:GetGridColor(gridId)
    local gridInfo = self.m_GridInfo[gridId]
    local guildInfo = self.m_GuildInfo[gridInfo.belongGuildId]
    if guildInfo and guildInfo.colorIndex then
        local color = GuildTerritoryWar_Setting.GetData().DiffPartyColor[guildInfo.colorIndex - 1]
        return NGUIText.ParseColor24(color, 0)
    end
    return nil
end

function LuaGuildTerritorialWarsMgr:GetGridFlagColor(gridId)
    local gridInfo = self.m_GridInfo[gridId]
    local guildInfo = self.m_GuildInfo[gridInfo.belongGuildId]
    if guildInfo and guildInfo.colorIndex then
        local color = GuildTerritoryWar_Setting.GetData().DiffFlagColor[guildInfo.colorIndex - 1]
        return NGUIText.ParseColor24(color, 0)
    end
    return nil
end

LuaGuildTerritorialWarsMgr.m_BattleStartTime = 0
LuaGuildTerritorialWarsMgr.m_PlayEndTime = 0
LuaGuildTerritorialWarsMgr.m_CurPosGridId = 0
LuaGuildTerritorialWarsMgr.m_MyGuildGridCount = 0
LuaGuildTerritorialWarsMgr.m_FavGridIdInfo = {}
LuaGuildTerritorialWarsMgr.m_CityInfo = {}
LuaGuildTerritorialWarsMgr.m_GridInfo = {}
function LuaGuildTerritorialWarsMgr:SendTerritoryWarMapDetail(myGuildId, battleStartTime, finishTime, curPosGridId, myGuildGridCount, favoriteGrids, grids, hasMonsterGrids)
    self.m_PlayEndTime,self.m_CurPosGridId,self.m_MyGuildGridCount,self.m_BattleStartTime = finishTime, curPosGridId, myGuildGridCount,battleStartTime
    self.m_GuildId = myGuildId
    local list = favoriteGrids and MsgPackImpl.unpack(favoriteGrids)
    self.m_FavGridIdInfo = {}
    for i = 0, list.Count-1 do
        local favGridId = list[i]
        self.m_FavGridIdInfo[favGridId] = true
    end
    self.m_CityInfo = {}
    self.m_GuildGridInfo = {}
    self.m_GridInfo = {}
    GuildTerritoryWar_Map.Foreach(function (key, data)
		self.m_GridInfo[key] = {belongGuildId = 0,status = 0,stausParam = 0}
	end)
    list = grids and MsgPackImpl.unpack(grids)
    for i = 0, list.Count-1, 5 do
        local gridId = list[i]
        local belongGuildId = list[i+1] -- 归属帮会ID
        local isCity = list[i+2] -- 是否帮会主城
        local status = list[i+3] -- 格子状态 0 默认 1 战斗中 2 放弃中 3 保护期 4 免战中 5 申请免战中
        local stausParam = list[i+4] -- 格子状态参数 status是2,3,4,5的时候 这个是状态结束时间
        self.m_GridInfo[gridId] = {belongGuildId = belongGuildId,isCity = isCity,status = status,stausParam = stausParam}
        if isCity then
            self.m_CityInfo[belongGuildId] = gridId
        end
    end
    self.m_MapMonsterSceneInfo = {}
    local list = hasMonsterGrids and g_MessagePack.unpack(hasMonsterGrids) or {}
    for i = 1, #list, 2 do
        local gridId = list[i]
        local monsterCount = list[i+1]
        self.m_MapMonsterSceneInfo[gridId] = monsterCount
    end
    if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsMapWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarMapDetail")
    else
        Gac2Gas.RequestTerritoryWarMapDetail()
    end
end

LuaGuildTerritorialWarsMgr.m_SeasonId = 0
LuaGuildTerritorialWarsMgr.m_WeekCount = 0
LuaGuildTerritorialWarsMgr.m_BattleLevel = 0
LuaGuildTerritorialWarsMgr.m_RoundFinishTime = 0
LuaGuildTerritorialWarsMgr.m_SceneIdx = 0
LuaGuildTerritorialWarsMgr.m_CurGridId = 0
LuaGuildTerritorialWarsMgr.m_CurGridBelongGuildId = 0
LuaGuildTerritorialWarsMgr.m_CurGridBelongForce = 0
LuaGuildTerritorialWarsMgr.m_CurGridOccupyNpcEngineId = 0
LuaGuildTerritorialWarsMgr.m_PrepareFinishTime = 0
LuaGuildTerritorialWarsMgr.m_MonsterCount = 0
LuaGuildTerritorialWarsMgr.m_PickCount = 0
function LuaGuildTerritorialWarsMgr:SendTerritoryWarPlayInfo(myGuildId,seasonId, weekCount, battleLevel, prepareFinishTime, roundFinishTime, gridId, sceneIdx, isFighting, belongGuildId, belongForce, occupyNpcEngineId)
    self.m_SeasonId = seasonId
    self.m_GuildId = myGuildId
    self.m_WeekCount = weekCount
    self.m_BattleLevel = battleLevel
    self.m_RoundFinishTime = roundFinishTime
    self.m_SceneIdx = sceneIdx
    self.m_CurGridId = gridId
    self:SetAtmosphere()
    self.m_PrepareFinishTime = prepareFinishTime
    g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarPlayInfo")
    self:ShowGuildTerritorialWarsPlayView()
    self.m_CurGridBelongGuildId = belongGuildId
    self.m_CurGridBelongForce = belongForce
    self.m_CurGridOccupyNpcEngineId = occupyNpcEngineId
    self:ShowOccupyFx()
end

function LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayInfo(myGuildId, seasonId, weekCount, battleLevel, gridId, sceneIdx, belongGuildId, belongForce, monsterCount, pickCount)
    self.m_SeasonId = seasonId
    self.m_GuildId = myGuildId
    self.m_WeekCount = weekCount
    self.m_BattleLevel = battleLevel
    self.m_SceneIdx = sceneIdx
    self.m_CurGridId = gridId
    self:SetAtmosphere()
    self.m_CurGridBelongGuildId = belongGuildId
    self.m_CurGridBelongForce = belongForce
    self.m_MonsterCount = monsterCount
    self.m_PickCount = pickCount
    self:ShowGuildTerritorialWarsPlayView()
end

function LuaGuildTerritorialWarsMgr:SetAtmosphere()
    local gridId = self.m_CurGridId
	local mapData = GuildTerritoryWar_Map.GetData(gridId)
	if mapData then
		local regionData = GuildTerritoryWar_Region.GetData(mapData.Region)
        if not regionData then
            return
        end
		CRenderScene.Inst:SetAtmosphere(regionData.atmosphere)
	end
end

function LuaGuildTerritorialWarsMgr:ShowOccupyFx()
    local obj = CClientObjectMgr.Inst:GetObject(LuaGuildTerritorialWarsMgr.m_CurGridOccupyNpcEngineId)
    if not obj then return end
    local npc = TypeAs(obj, typeof(CClientNpc))
    if not npc then return end
    local guildId = self.m_GuildId
    local occupyFx = GuildTerritoryWar_Setting.GetData().OccupyFx
    local fxId = occupyFx[2]
    if LuaGuildTerritorialWarsMgr.m_CurGridBelongGuildId ~= 0 then
        local isMyGuild = guildId == LuaGuildTerritorialWarsMgr.m_CurGridBelongGuildId
        local isFriend = self.m_GuildInfo[guildId] and (LuaGuildTerritorialWarsMgr.m_CurGridBelongGuildId == self.m_GuildInfo[guildId].allianceGuildId)
        fxId = (isMyGuild or isFriend) and occupyFx[1] or occupyFx[0]
    end
    for i = 0, 2 do
        local otherFxId = occupyFx[i]
        if otherFxId ~= fxId and npc.RO:ContainFx(otherFxId) then
            npc.RO:RemoveFX(otherFxId)
        end
    end
    if not npc.RO:ContainFx(fxId) then
        local fx = CEffectMgr.Inst:AddObjectFX(fxId, npc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        npc.RO:AddFX(fxId, fx, -1)
    end
end

LuaGuildTerritorialWarsMgr.m_GuildRank = 0
function LuaGuildTerritorialWarsMgr:SyncTerritoryWarGuildCurrentRank(rank)
    self.m_GuildRank = rank
    self:ShowGuildTerritorialWarsPlayView()
end

function LuaGuildTerritorialWarsMgr:ShowGuildTerritorialWarsPlayView()
    if self:IsChallengePlayOpen() then
        self:ShowChallengePlayView()
        return
    end
    if self:IsPeripheryPlay() then
        self:ShowGuildTerritorialWarsPeripheryPlayView()
        return
    end
    if not self:IsInPlay() then
        return
    end
    if self.m_BattleLevel < 1 then return end
    local totalSeconds = self.m_RoundFinishTime - CServerTimeMgr.Inst.timeStamp
    if self.m_PrepareFinishTime > CServerTimeMgr.Inst.timeStamp then
        totalSeconds = self.m_PrepareFinishTime - CServerTimeMgr.Inst.timeStamp
    end
    totalSeconds = math.max(0,totalSeconds)
    local timeStr = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    local grounpName = GuildTerritoryWar_Setting.GetData().GuildGroupingNames[self.m_BattleLevel - 1]
    local guildRank = (self.m_GuildRank == 0) and LocalString.GetString("暂无") or self.m_GuildRank..LocalString.GetString(" (非实时)")
    local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsView",self.m_SeasonId, self.m_WeekCount, grounpName,guildRank,timeStr)
    if self.m_PrepareFinishTime > CServerTimeMgr.Inst.timeStamp then
        msg = string.gsub(msg, LocalString.GetString("剩余时间"), LocalString.GetString("准备开战"))
    end
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate", msg, true, true, LocalString.GetString("规则"), LocalString.GetString("战况"),function(go)
        LuaGuildTerritorialWarsMgr:ShowCommonImageRuleWnd(true)
    end,function(go)
        Gac2Gas.RequestTerritoryWarScoreData()
    end)
    self:ShowCommonImageRuleWnd()
end


function LuaGuildTerritorialWarsMgr:ShowChallengePlayView()
    local bossNum = self.m_CurrentBossIdx 
    local levelIndexStr = Extensions.ConvertToChineseString(bossNum)
    local data = GuildTerritoryWar_ChallengeBoss.GetData(bossNum)
    if not data then
        return
    end
    local monsterId = self:IsEliteChallengePlayOpen() and data.EliteMonsterId or data.NormalMonsterId
    local monsterData = Monster_Monster.GetData(monsterId)
    if not monsterData then
        return
    end
    local monsterIsKill = self.m_IsCurrentBossDie and 1 or 0
    local msg = g_MessageMgr:FormatMessage("GuildTerritorialChallengePlayView",levelIndexStr,monsterData.Name,monsterIsKill,"%s")
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate", msg, true, true, LocalString.GetString("离开"),LocalString.GetString("战况"),nil,function(go)
        --CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsChallengeSituatuionWnd)
        LuaBattleSituationMgr.OpenBattleSituationWnd(EnumSituationType.eGuildTerritorialWarsChallenge)
    end)
end

function LuaGuildTerritorialWarsMgr:ShowGuildTerritorialWarsPeripheryPlayView()
    if not CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsPeripheryAlertWnd) then
        CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsPeripheryAlertWnd)
    end
    if self.m_BattleLevel == 0 then return end
    local grounpName = GuildTerritoryWar_Setting.GetData().GuildGroupingNames[self.m_BattleLevel - 1]
    local monsterNum = LuaGuildTerritorialWarsMgr:GetMonsterCount(self.m_CurGridId)
    local mapData = GuildTerritoryWar_Map.GetData(self.m_CurGridId)
    if mapData then
        local regionData = GuildTerritoryWar_Region.GetData(mapData.Region)
        if regionData then
            local text2 = SafeStringFormat3(LocalString.GetString("(%s,%s,%s)"), regionData.Name, mapData.PosX, mapData.PosY)
            local msg = g_MessageMgr:FormatMessage("GuildTerritorialDailyWarsView",self.m_SeasonId, self.m_WeekCount, grounpName,monsterNum,text2)
            g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate", msg, true, false, LocalString.GetString("离开"),nil,nil,nil)
        end
    end
end

function LuaGuildTerritorialWarsMgr:ShowCommonImageRuleWnd(forceShow)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local curDay = now.Day
    local lastShowDay = PlayerPrefs.GetInt("GuildTerritorialWars_ShowCommonImageRuleWnd_Day")
    if lastShowDay == curDay and not forceShow then return end
    PlayerPrefs.SetInt("GuildTerritorialWars_ShowCommonImageRuleWnd_Day",curDay)
    local imagePaths = {
        "UI/Texture/NonTransparent/Material/guildterritorialwars_bg_02.mat",
        "UI/Texture/NonTransparent/Material/guildterritorialwars_bg_01.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("GuildTerritorialWars_Rule_1"),
        g_MessageMgr:FormatMessage("GuildTerritorialWars_Rule_2"),
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

LuaGuildTerritorialWarsMgr.m_ProgressList = {}
LuaGuildTerritorialWarsMgr.m_PlayerNumList = {}
function LuaGuildTerritorialWarsMgr:SyncTerritoryWarPlayProgress(progressUd)
    local list = progressUd and MsgPackImpl.unpack(progressUd)
    self.m_ProgressList = {}
    for i = 0, list.Count-1, 2 do
        local idx = list[i] -- 0:总进度 1:分线1 2:分线2
        local progress = list[i+1] -- 进度
        self.m_ProgressList[idx] = progress
    end
    g_ScriptEvent:BroadcastInLua("OnSyncTerritoryWarPlayProgress")
end

LuaGuildTerritorialWarsMgr.m_CurScoreInfo = {}
LuaGuildTerritorialWarsMgr.m_GuildScoreInfo = {}
LuaGuildTerritorialWarsMgr.m_ScoreEventInfo = {}
LuaGuildTerritorialWarsMgr.m_NotOpenSituationWnd = false
function LuaGuildTerritorialWarsMgr:SendTerritoryWarScoreInfo(myGuildId, currentWeek, currentScore, gridCount, scoreSpeed, toMasterSpeed, fromSlaveSpeed, rankInfo, eventInfo)
    self.m_CurScoreInfo = {myGuildId = myGuildId, currentScore = currentScore, gridCount = gridCount, scoreSpeed = scoreSpeed, toMasterSpeed = MsgPackImpl.unpack(toMasterSpeed), fromSlaveSpeed = MsgPackImpl.unpack(fromSlaveSpeed)}
    self.m_GuildScoreInfo = {}
    self.m_WeekCount = currentWeek
    local list = rankInfo and MsgPackImpl.unpack(rankInfo)
    for i = 0, list.Count-1, 9 do
        local guildId = list[i]
        local guildName = list[i+1]
        local serverName = list[i+2]
        local score = list[i+3]
        local gridCount = list[i+4]
        local guildDeleted = list[i+5]
        local isMainCityOwner = list[i+6]
        local isSlave = list[i+7]
        local memberCount = list[i+8]
        table.insert(self.m_GuildScoreInfo, {
            isMainCityOwner = isMainCityOwner, isSlave = isSlave, guildId = guildId,
            guildName = guildName, serverName = serverName, score = score, 
            occupyNum = gridCount, guildDeleted = guildDeleted, memberCount =  memberCount
        })
    end
    self.m_ScoreEventInfo = {}
    GuildTerritoryWar_ScoreEvent.ForeachKey(function (key)
        local designData = GuildTerritoryWar_ScoreEvent.GetData(key)
        self.m_ScoreEventInfo[key] = {progress = 0, isFinished = false, designData = designData}
    end)
    list = eventInfo and MsgPackImpl.unpack(eventInfo)
    for i = 0, list.Count-1,3 do
        local eventId = list[i]
        local progress = list[i+1]
        local isFinished = list[i+2]
        self.m_ScoreEventInfo[eventId].progress = progress
        self.m_ScoreEventInfo[eventId].isFinished = isFinished
    end
    if self.m_NotOpenSituationWnd then
        self.m_NotOpenSituationWnd = false
        g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarScoreInfo")
        return
    end
    CUIManager.CloseUI(CLuaUIResources.GuildTerritorialWarsSituationWnd)
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsSituationWnd)
end

function LuaGuildTerritorialWarsMgr:SendTerritoryWarRelationInfo(myGuildId, hasAlliance, relationInfo)
    local relationInfoList = {}
    local list = relationInfo and MsgPackImpl.unpack(relationInfo)
    for i = 0, list.Count-1, 11 do
        local guildId = list[i]
        local guildName = list[i+1]
        local serverId = list[i+2]
        local serverName = list[i+3]
        local leaderId = list[i+4]
        local leaderName = list[i+5]
        local equipScore = list[i+6]
        local ownGridCount = list[i+7]
        local allianceCount = list[i+8]
        local relationType = list[i+9] -- 0中立 1盟友 2我被他俘虏 3他被我俘虏 4等待结盟回应 5敌对
        local guildDeleted = list[i+10]

        local isMyAlliance = relationType == 1
        local isMaster = relationType == 2
        local isSlave = relationType == 3
        local isWait = relationType == 4
        local isDiDui = relationType == 5
        table.insert(relationInfoList,{
            guildId = guildId, guildName = guildName, serverName = serverName, leaderId = leaderId, 
            leaderName = leaderName, equipScore = equipScore, ownGridCount = ownGridCount, allianceCount = allianceCount, 
            relationType = relationType, serverId = serverId,isMyAlliance = isMyAlliance,isMaster = isMaster,
            isSlave = isSlave,isWait = isWait,guildDeleted = guildDeleted, isDiDui = isDiDui
        })
    end
    g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarRelationInfo",myGuildId, hasAlliance, relationInfoList)
end

function LuaGuildTerritorialWarsMgr:SendTerritoryWarScenePlayerCount(scenePlayerCount)
    local list = scenePlayerCount and MsgPackImpl.unpack(scenePlayerCount)
    self.m_PlayerNumList = {}
    for i = 0, list.Count-1, 2 do
        local sceneIdx = list[i]
        local playerCount = list[i+1]
        self.m_PlayerNumList[sceneIdx] = playerCount
    end
    g_ScriptEvent:BroadcastInLua("OnSyncTerritoryWarPlayProgress")
end

LuaGuildTerritorialWarsMgr.m_RankInfo = {}
LuaGuildTerritorialWarsMgr.m_RankWndDefaultIndex = 0
function LuaGuildTerritorialWarsMgr:SendTerritoryWarRankInfo(playerGuildId, playerBattleLevel, battleLevel, rankInfo)
    self.m_RankWndDefaultIndex = playerBattleLevel
    local list = rankInfo and MsgPackImpl.unpack(rankInfo)
    self.m_RankInfo = {}
    for i = 0, list.Count-1, 8 do
        local rank = list[i]
        local guildId = list[i+1]
        local guildName = list[i+2]
        local serverName = list[i+3]
        local equipScore = list[i+4]
        local occupyCount = list[i+5]
        local score = list[i+6]
        local guildDeleted = list[i+7]
        table.insert(self.m_RankInfo,{
            rank = rank, guildId = guildId, guildName = guildName, serverName = serverName, 
            equipScore = equipScore, occupyCount = occupyCount, score = score,guildDeleted = guildDeleted
        })
    end
    if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsRankWnd) then
        g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarRankInfo",playerGuildId)
        return
    end
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsRankWnd)
end

LuaGuildTerritorialWarsMgr.m_FightDetailInfo = {}
function LuaGuildTerritorialWarsMgr:SendTerritoryWarFightDetail(detail)
    self.m_FightDetailInfo = {}
    local list = detail and MsgPackImpl.unpack(detail)
    for i= 0, list.Count-1, 14 do
        local playerId = list[i]
        local playerName = list[i+1]
        local class = list[i+2]
        local isForeignAid = list[i+3]  -- 是否外援
        local kill = list[i+4]          -- 击杀
        local beKill = list[i+5]        -- 被杀
        local damage = list[i+6]        -- 伤害
        local ce = list[i+7]            -- 爆尸
        local relive = list[i+8]        -- 复活
        local ctrl = list[i+9]          -- 控制
        local rmCtrl = list[i+10]       -- 解控
        local damageTaken = list[i+11]  -- 承伤
        local heal = list[i+12]         -- 治疗
        local gender = list[i+13]       -- 性别
        table.insert(self.m_FightDetailInfo,{
            playerId = playerId, playerName = playerName, class = class, isForeignAid = isForeignAid,
            kill = kill, beKill = beKill, damage = damage, ce = ce,relive = relive,ctrl = ctrl,
            rmCtrl = rmCtrl,damageTaken = damageTaken,heal = heal, gender = gender
        })
    end
    g_ScriptEvent:BroadcastInLua("OnSendTerritoryWarFightDetail")
end

function LuaGuildTerritorialWarsMgr:IsMyArea(gridId)
    local guildId = self.m_GuildId
    local gridInfo = self.m_GridInfo[gridId]
    return gridInfo and gridInfo.belongGuildId == guildId
end

function LuaGuildTerritorialWarsMgr:IsFriendArea(gridId)
    local guildId = self.m_GuildId
    local gridInfo = self.m_GridInfo[gridId]
    local guildInfo = self.m_GuildInfo[guildId]
    return guildInfo and gridInfo and guildInfo.allianceGuildId == gridInfo.belongGuildId and (gridInfo.belongGuildId > 0)
end

function LuaGuildTerritorialWarsMgr:IsEnemyArea(gridId)
    local gridInfo = self.m_GridInfo[gridId]
    local isMyArea = self:IsMyArea(gridId)
	local isFriendArea = self:IsFriendArea(gridId)
    return not isMyArea and not isFriendArea and gridInfo and (gridInfo.belongGuildId > 0)
end

function LuaGuildTerritorialWarsMgr:IsCaptiveArea(gridId)
    local gridInfo = self.m_GridInfo[(gridId)]
    local guildInfo = self.m_GuildInfo[gridInfo and gridInfo.belongGuildId or 0]
    return guildInfo and guildInfo.masterGuildId > 0
end

function LuaGuildTerritorialWarsMgr:GetMyCaptiveArea(gridId)
    local gridInfo = self.m_GridInfo[(gridId)]
    local guildInfo = self.m_GuildInfo[gridInfo and gridInfo.belongGuildId or 0]
    local guildID = guildInfo and guildInfo.masterGuildId or 0
    return guildID == self.m_GuildId
end

function LuaGuildTerritorialWarsMgr:RequestGuildTerritoryWarMemberTeleport(leaderId, leaderName, gridId, sceneIdx)
    local data = GuildTerritoryWar_Map.GetData(gridId)
    if not data then return end
    local region = data.Region
	local regionData = GuildTerritoryWar_Region.GetData(region)
    if not regionData then return end
    local msg = g_MessageMgr:FormatMessage("GuildTerritoryWarMemberTeleport_Confirm", leaderName, regionData.Name, data.PosX, data.PosY)
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        Gac2Gas.TerritoryWarTeleportToGuildLeader()
    end),nil,LocalString.GetString("前往"),LocalString.GetString("取消"),false)
end

LuaGuildTerritorialWarsMgr.m_AllianceApplyInfo ={}
function LuaGuildTerritorialWarsMgr:SendTerritoryWarAllianceApplyInfo(applyInfo)
    local list = g_MessagePack.unpack(applyInfo)
    self.m_AllianceApplyInfo ={}
    for i = 1, #list, 11 do
        local guildId = list[i]
        local guildName = list[i+1]
        local serverId = list[i+2]
        local serverName = list[i+3]
        local leaderId = list[i+4]
        local leaderName = list[i+5]
        local equipScore = list[i+6]
        local ownGridCount = list[i+7]
        local guildDeleted = list[i+8]
        local applyTime = list[i+9]
        local score = list[i+10]
        table.insert(self.m_AllianceApplyInfo,{
            guildId = guildId, guildName = guildName, serverId = serverId, serverName = serverName,
            leaderId = leaderId, leaderName = leaderName, equipScore = equipScore, ownGridCount = ownGridCount,
            guildDeleted = guildDeleted, applyTime = applyTime,score = score
        })
    end
    table.sort(self.m_AllianceApplyInfo,function (a,b)
        return a.applyTime < b.applyTime
    end)
    g_ScriptEvent:BroadcastInLua("SendTerritoryWarAllianceApplyInfo")
end

function LuaGuildTerritorialWarsMgr:SendTerritoryWarCanDrawCommand(requestType, can)
    g_ScriptEvent:BroadcastInLua("SendTerritoryWarCanDrawCommand",requestType, can)
end

LuaGuildTerritorialWarsMgr.m_DrawColorIndex = 0
LuaGuildTerritorialWarsMgr.m_MonsterCount = 0
LuaGuildTerritorialWarsMgr.m_MonsterSceneInfo = {}
LuaGuildTerritorialWarsMgr.m_MapMonsterSceneInfo = {}
LuaGuildTerritorialWarsMgr.m_CanOpenContributeWnd = false
LuaGuildTerritorialWarsMgr.m_WeeklyContributeInfo = {}
LuaGuildTerritorialWarsMgr.m_QuarterlyContributeInfo = {}

function LuaGuildTerritorialWarsMgr:IsPeripheryPlay()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        return CommonDefs.HashSetContains(GuildTerritoryWar_RelatedPlaySetting.GetData().RelatedGamePlayId, typeof(UInt32), gamePlayId)  
	end
    return false
end

LuaGuildTerritorialWarsMgr.m_IsInGuide = false
LuaGuildTerritorialWarsMgr.m_GuideGridId = 0
function LuaGuildTerritorialWarsMgr:IsInGuide()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        return self.m_IsInGuide or CommonDefs.HashSetContains(GuildTerritoryWar_RelatedPlaySetting.GetData().GuildGameplayId, typeof(UInt32), gamePlayId)  
	end
    return self.m_IsInGuide
end

function LuaGuildTerritorialWarsMgr:ShowGuideMapWnd()
    self.m_IsInGuide = true
    self.m_GridInfo = {}
    self.m_CurrentWeek = 2
    self.m_GuildId = 1
    GuildTerritoryWar_Map.Foreach(function (key, data)
        self.m_GridInfo[key] = {belongGuildId = 0,status = 0,stausParam = 0,isCity =false}
    end)
    self.m_GuideGridId = 56
    self.m_GridInfo[self.m_GuideGridId] = {belongGuildId = self.m_GuildId,status = 0,stausParam = 0,isCity = true}
    self.m_GuildInfo[self.m_GuildId] = {guildId = self.m_GuildId, guildName = LocalString.GetString("我的帮会") , allianceGuildId = 0, masterGuildId = 0, serverName = LocalString.GetString("我的服务器"), colorIndex = 1}
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsMapWnd)
    CGuideMgr.Inst:StartGuide(EnumGuideKey.GuildTerritorialWarGuideMap,0)
end

function LuaGuildTerritorialWarsMgr:GetMonsterCount(gridId)
    return self.m_MapMonsterSceneInfo[gridId] and self.m_MapMonsterSceneInfo[gridId] or 0
end

function LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayMonsterCount(monsterCount)
    self.m_MonsterCount = monsterCount
    if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsPeripheryAlertWnd) then
        g_ScriptEvent:BroadcastInLua("SendGTWRelatedPlayMonsterCount")
    else
        CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsPeripheryAlertWnd)
    end
end

function LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayMonsterSceneInfo(sceneInfo)
    self.m_MonsterSceneInfo = {}
    local list = sceneInfo and g_MessagePack.unpack(sceneInfo) or {}
    for i = 1, #list, 2 do
        local gridId = list[i]
        local monsterCount = list[i+1]
        if monsterCount > 0 then
            table.insert(self.m_MonsterSceneInfo, {gridId = gridId,monsterCount = monsterCount})
        end
    end
    table.sort(self.m_MonsterSceneInfo,function (a, b)
        local data1 = GuildTerritoryWar_Map.GetData(a.gridId)
        local data2 = GuildTerritoryWar_Map.GetData(b.gridId)
        if data1.Level ~= data2.Level then
            return data1.Level > data2.Level
        elseif a.monsterCount == b.monsterCount then
            return a.gridId < b.gridId
        else
            return a.monsterCount > b.monsterCount
        end
    end)
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsPeripheryMonsterWnd)
end

function LuaGuildTerritorialWarsMgr:SendCanOpenGTWRelatedPlayContributeWnd(canOpen)
    self.m_CanOpenContributeWnd = canOpen
    g_ScriptEvent:BroadcastInLua("SendCanOpenGTWRelatedPlayContributeWnd")
end

function LuaGuildTerritorialWarsMgr:SendGTWRelatedPlayContributeInfo(isTotal, contributeInfo)
    local list = contributeInfo and g_MessagePack.unpack(contributeInfo) or {}
    local contributeInfo = {}
    for i = 1, #list, 10 do
        local playerId = list[i]
        local playerName = list[i+1]
        local level = list[i+2]
        local class = list[i+3]
        local hasFeiSheng = list[i+4]
        local isForeignAid = list[i+5]
        local killCount = list[i+6]
        local lingqi = list[i+7]
        local playTime = list[i+8]
        local pingjia = list[i+9]
        table.insert(contributeInfo,{
            playerId = playerId, playerName = playerName, level = level, class = class,
            hasFeiSheng = hasFeiSheng, isForeignAid = isForeignAid, killCount = killCount, lingqi = lingqi,
            playTime = playTime, pingjia = pingjia
        })
    end
    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    table.sort(contributeInfo,function (a, b)
        if a.playerId == playerId then
            return true
        elseif b.playerId == playerId then
            return false
        elseif a.lingqi == b.lingqi then
            return a.playerId < b.playerId
        else
            return a.lingqi > b.lingqi
        end
    end)
    if isTotal then
        self.m_QuarterlyContributeInfo = contributeInfo
    else
        self.m_WeeklyContributeInfo = contributeInfo
    end
    g_ScriptEvent:BroadcastInLua("SendGTWRelatedPlayContributeInfo", isTotal)
end

LuaGuildTerritorialWarsMgr.m_PlayWeeklyResultInfo = {}
function LuaGuildTerritorialWarsMgr:SendGuildTerritoryWarPlayWeeklyResult(seasonId, weekPassed, guildId, guildName, rank, guildScore, gridCount, slaveCount, playerScore, contribution, silver, exp, mingwang, itemUd)
    self.m_PlayWeeklyResultInfo = {
        seasonId = seasonId, weekPassed = weekPassed, guildId = guildId, guildName = guildName,
        rank = rank, guildScore = guildScore, gridCount = gridCount, slaveCount = slaveCount,
        playerScore = playerScore, contribution = contribution, silver = silver, exp = exp,
        mingwang = mingwang, itemInfoArr = g_MessagePack.unpack(itemUd)
    }
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsResultWnd)
end

function LuaGuildTerritorialWarsMgr:SendPlayerShareCommandPicResult(isSucc)
    g_ScriptEvent:BroadcastInLua("SendPlayerShareCommandPicResult", isSucc)
end

LuaGuildTerritorialWarsMgr.EnableGuide_GuildTerritorialWarPeripheryPlay2 = false

function LuaGuildTerritorialWarsMgr:TeleportToMyCity()
    -- local belongGuildId = self.m_GuildId
    -- local gridId = self.m_CityInfo[belongGuildId]
    Gac2Gas.RequestTerritoryWarTeleport(0, 0)
end

function LuaGuildTerritorialWarsMgr:IsChallengePlayOpen()
    return self:IsEliteChallengePlayOpen() or self:IsRegularChallengePlayOpen()
end

function LuaGuildTerritorialWarsMgr:IsEliteChallengePlayOpen()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        return CommonDefs.HashSetContains(GuildTerritoryWar_RelatedPlaySetting.GetData().EliteWarGameplayId, typeof(UInt32), gamePlayId)  
	end
    return false
end

function LuaGuildTerritorialWarsMgr:IsRegularChallengePlayOpen()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        return CommonDefs.HashSetContains(GuildTerritoryWar_RelatedPlaySetting.GetData().RegularWarGameplayId, typeof(UInt32), gamePlayId)  
	end
    return false
end


LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo = {}
function LuaGuildTerritorialWarsMgr:SendGTWSubmitLingQiInfo(currentHaveLingQi, todayExchangeFXYCount, levelInfo)
    print("SendGTWSubmitLingQiInfo", currentHaveLingQi, todayExchangeFXYCount) -- 当前拥有灵气数, 今日已兑换凤血玉数
    levelInfo = levelInfo and g_MessagePack.unpack(levelInfo) or {}
    self.m_SubmitLingQiInfo = {currentHaveLingQi = currentHaveLingQi, todayExchangeFXYCount = todayExchangeFXYCount, levelInfo = levelInfo}
    local scoreInfo = levelInfo.scoreInfo
    local scoreLv = scoreInfo.scoreLv -- 当前等级
    local scoreExp = scoreInfo.scoreExp -- 当前经验
    local scoreTotal = scoreInfo.total -- 可获得总数
    local scoreCurrent = scoreInfo.scoreCurrent -- 当前已获得数
    local canExchangeScore = scoreInfo.canExchangeScore -- 能否兑换
    print("SendGTWSubmitLingQiInfo exp", scoreLv, scoreExp, scoreTotal, scoreCurrent, canExchangeScore)

    local hpInfo = levelInfo.hpInfo
    local hpLv = hpInfo.hpLv
    local hpExp = hpInfo.hpExp
    local hpTotal = hpInfo.total
    local hpCurrent = hpInfo.hpCurrent
    local canExchangeHp = hpInfo.canExchangeHp
    print("SendGTWSubmitLingQiInfo hp", hpLv, hpExp, hpTotal, hpCurrent, canExchangeHp)

    local buffInfo = levelInfo.buffInfo
    local buffLv = buffInfo.buffLv
    local buffExp = buffInfo.buffExp
    local buffTotal = buffInfo.total
    local buffCurrent = buffInfo.buffCurrent
    local canExchangeBuff = buffInfo.canExchangeBuff
    print("SendGTWSubmitLingQiInfo buff", buffLv, buffExp, buffTotal, buffCurrent, canExchangeBuff)
    g_ScriptEvent:BroadcastInLua("SendGTWSubmitLingQiInfo", currentHaveLingQi, todayExchangeFXYCount, levelInfo)
end

LuaGuildTerritorialWarsMgr.m_AltarInfo = {}
function LuaGuildTerritorialWarsMgr:SendGTWAltarInfo(altarInfo)
    print("SendGTWAltarInfo")
    altarInfo = g_MessagePack.unpack(altarInfo)
    self.m_AltarInfo = {}
    local activedAltarInfo = altarInfo.activedAltarInfo
    self.m_AltarInfo.activedAltarInfo = {}
    for _, idx in ipairs(activedAltarInfo or {}) do
        -- idx 1 火 2 木 3 冰 4 风
        print("SendGTWAltarInfo actived", idx)
        self.m_AltarInfo.activedAltarInfo[idx] = true
    end

    local altarResourceInfo = altarInfo.altarResourceInfo
    self.m_AltarInfo.altarResourceInfo = {}
    for idx = 1, 4 do
        -- idx 1 火 2 木 3 冰 4 风
        local resourceCount = altarResourceInfo[idx] or 0 -- 已供奉的资源数
        print("SendGTWAltarInfo resourceCount", idx, resourceCount)
        self.m_AltarInfo.altarResourceInfo[idx] = resourceCount
    end

    local haveResourceCount = altarInfo.haveResourceCount
    self.m_AltarInfo.haveResourceCount = {}
    for idx = 1, 4 do
        -- idx 1 火 2 木 3 冰 4 风
        local hasResCount = haveResourceCount[idx] or 0 -- 玩家当前拥有的资源数
        print("SendGTWAltarInfo hasResCount", hasResCount)
        self.m_AltarInfo.haveResourceCount[idx] = hasResCount
    end
    g_ScriptEvent:BroadcastInLua("SendGTWAltarInfo", altarInfo)
end

LuaGuildTerritorialWarsMgr.m_ZhanLongTaskInfo = {}
function LuaGuildTerritorialWarsMgr:SendGTWZhanLongTaskInfo(todayTaskCount, taskInfo)
    print("SendGTWZhanLongTaskInfo", todayTaskCount) -- 今日已完成任务数
    taskInfo = g_MessagePack.unpack(taskInfo)
    for idx, info in pairs(taskInfo) do
        local taskId = info.taskId
        local state = info.state -- 1 已完成, 2 正在进行
        print("SendGTWZhanLongTaskInfo task", idx, taskId, state)
    end
    self.m_ZhanLongTaskInfo = {todayTaskCount = todayTaskCount, taskInfo = taskInfo}
    g_ScriptEvent:BroadcastInLua("SendGTWZhanLongTaskInfo", taskInfo)
end

LuaGuildTerritorialWarsMgr.m_ZhanLongShopInfo = {}
function LuaGuildTerritorialWarsMgr:SendGTWZhanLongShopInfo(hasScore, hasPermission, shopInfo)
    print("SendGTWZhanLongShopInfo", hasScore, hasPermission)
    shopInfo = shopInfo and g_MessagePack.unpack(shopInfo)
    -- for itemId, goodsInfo in pairs(shopInfo) do
    --     for itemId, goodsInfo in pairs(shopInfo) do
    --         local needScore = goodsInfo.needScore
    --         local remainCount = goodsInfo.remainCount
    --         print("SendGTWZhanLongShopInfo Goods:", itemId, needScore, remainCount)
    --     end
    -- end
    self.m_ZhanLongShopInfo = {hasScore = hasScore, hasPermission = hasPermission, shopInfo = shopInfo}
    if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsZhanLongShopWnd) then
        g_ScriptEvent:BroadcastInLua("SendGTWZhanLongShopInfo", hasScore, hasPermission, shopInfo)
        return
    end
    CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsZhanLongShopWnd)
end

LuaGuildTerritorialWarsMgr.m_ChallengeMemberInfo = {}
LuaGuildTerritorialWarsMgr.m_IsChallenging = false --棋局挑战是否开启
LuaGuildTerritorialWarsMgr.m_FilterOnlineAndJingYing = false --是否只显示在线和精英
function LuaGuildTerritorialWarsMgr:SendGTWChallengeMemberInfo(onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging, memberInfo)
    print("SendGTWChallengeMemberInfo", onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, isChallenging)
    memberInfo = g_MessagePack.unpack(memberInfo)
    local list = {}
    for i = 1, #memberInfo, 13  do
        local playerId = memberInfo[i]
        local playername = memberInfo[i + 1]
        local level = memberInfo[i + 2]
        local feisheng = memberInfo[i + 3]
        local xiuwei = memberInfo[i + 4]
        local xiulian = memberInfo[i + 5]
        local equipScore = memberInfo[i + 6]
        local playtime = memberInfo[i + 7]
        local score = memberInfo[i + 8]
        local isElite = memberInfo[i + 9]
        -- local isForeignAid = memberInfo[i + 10] -- 这个字段没用，复用成是否入场
        local isForeignAid = false
        local isInPlay = memberInfo[i + 10] -- 是否入场
        local isOnline = memberInfo[i + 11]
        local class = memberInfo[i + 12]
        table.insert(list, {
            playerId = playerId, playername = playername, level  = level, feisheng = feisheng, xiuwei = xiuwei, xiulian = xiulian,
            equipScore = equipScore, playtime = playtime, score = score, isElite = isElite, isForeignAid = isForeignAid, isOnline = isOnline, class = class,
            isInPlay = isInPlay
        })
        print("SendGTWChallengeMemberInfo member", playerId, playername, level, feisheng, xiuwei, xiulian, equipScore, playtime, score, isElite, isForeignAid, isInPlay)
    end
    -- if not hasPermission then
    --     return
    -- end
    self.m_IsChallenging = isChallenging
    self.m_CanOpenContributeWnd = true
    self.m_ChallengeMemberInfo = {onlineMemberCount = onlineMemberCount, totalMemberCount = totalMemberCount, onlineEliteCount = onlineEliteCount, totalEliteCount = totalEliteCount, hasPermission = hasPermission, list = list}
    if CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsJingYingSettingWnd) then
        g_ScriptEvent:BroadcastInLua("SendGTWChallengeMemberInfo", onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount, hasPermission, list)
    else
        CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsJingYingSettingWnd)
    end
end

function LuaGuildTerritorialWarsMgr:SendSetGTWChallengeEliteResult(targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
    self.m_ChallengeMemberInfo.onlineMemberCount = onlineMemberCount
    self.m_ChallengeMemberInfo.totalMemberCount = totalMemberCount
    self.m_ChallengeMemberInfo.onlineEliteCount = onlineEliteCount
    self.m_ChallengeMemberInfo.totalEliteCount = totalEliteCount
    for i = 1, #self.m_ChallengeMemberInfo.list do
		if self.m_ChallengeMemberInfo.list[i].playerId == targetPlayerId then
			self.m_ChallengeMemberInfo.list[i].isElite = isElite
		end
	end
    g_ScriptEvent:BroadcastInLua("SendSetGTWChallengeEliteResult", targetPlayerId, isElite, onlineMemberCount, totalMemberCount, onlineEliteCount, totalEliteCount)
end

LuaGuildTerritorialWarsMgr.m_ChallengeBattleFieldInfo = {}
function LuaGuildTerritorialWarsMgr:SendGTWChallengeBattleFieldInfo(myGuildId, challengeIdx,curbossIdx,  remainTime, bossInfo, passGuildInfo)
    print("SendGTWChallengeBattleFieldInfo", myGuildId, challengeIdx, remainTime)
    bossInfo = g_MessagePack.unpack(bossInfo)
    local gridInfo = {}
    local battingBossIdx = 0
    for bossIdx, info in pairs(bossInfo) do
        local gridId = info.gridId
        local defeated = info.defeated
        local state = defeated and 1 or 3
        local isCurrent = info.isCurrent
        if isCurrent then
            battingBossIdx = bossIdx
            state = defeated and 1 or 2
        end
        gridInfo[gridId] = {bossIdx = bossIdx, state = state}
        print("SendGTWChallengeBattleFieldInfo bossInfo", bossIdx, gridId, defeated)
    end

    passGuildInfo = g_MessagePack.unpack(passGuildInfo)
    for guildId, info in pairs(passGuildInfo) do
        local guildName = info.guildName
        local passTime = info.passTime
        print("SendGTWChallengeBattleFieldInfo passGuildInfo", guildId, guildName, passTime)
    end
    print(myGuildId, remainTime)
    self.m_ChallengeBattleFieldInfo = {curbossIdx = curbossIdx, battingBossIdx = battingBossIdx, myGuildId = myGuildId, challengeIdx = challengeIdx,remainTime = remainTime, bossInfo = bossInfo, passGuildInfo = passGuildInfo, gridInfo = gridInfo}
    if not CUIManager.IsLoaded(CLuaUIResources.GuildTerritorialWarsMapWnd) then
        CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsMapWnd)
    end
    g_ScriptEvent:BroadcastInLua("SendGTWChallengeBattleFieldInfo", myGuildId, remainTime, bossInfo, passGuildInfo)
end

LuaGuildTerritorialWarsMgr.m_CurrentBossIdx = 0

LuaGuildTerritorialWarsMgr.m_ChallengeFightDetailInfo = {}
function LuaGuildTerritorialWarsMgr:SendGTWChallengeFightDetailInfo(detailType, detail, playerCount, statTotal)
    print("SendGTWChallengeFightDetailInfo", detailType, playerCount, statTotal)
    detail = g_MessagePack.unpack(detail)
    local detailInfo = {}
    for _, info in ipairs(detail) do
        local playerId = info[1]
        local playerName = info[2]
        local playerClass = info[3]
        local playerStat = info[4]
        table.insert(detailInfo, {playerId = playerId, playerName = playerName, playerClass = playerClass, playerStat = playerStat})
        print("SendGTWChallengeFightDetailInfo detail", playerId, playerName, playerClass, playerStat)
    end
    table.sort(detailInfo, function(a,b)
        if a.playerStat == b.playerStat then
            return a.playerId < b.playerId
        end
        return a.playerStat > b.playerStat
    end)
    local players = {}
    for i,v in ipairs(detailInfo) do
        players[i-1] = v
    end
    players.Count = #players
    self.m_ChallengeFightDetailInfo = {statType = detailType,playerCount = playerCount,statTotal = statTotal,players = players}
    g_ScriptEvent:BroadcastInLua("SendGTWChallengeFightDetailInfo",detailType, detailInfo , playerCount, statTotal)
end

LuaGuildTerritorialWarsMgr.m_ChallengeIdx = 0
LuaGuildTerritorialWarsMgr.m_MaxBossIdx = 0
LuaGuildTerritorialWarsMgr.m_IsCurrentBossDie = false
function LuaGuildTerritorialWarsMgr:SendGTWChallengePlayInfo(challengeIdx, currentBossIdx, maxBossIdx, isCurrentBossDie)
    print("SendGTWChallengePlayInfo", challengeIdx, currentBossIdx, maxBossIdx, isCurrentBossDie)
    self.m_CurrentBossIdx = currentBossIdx
    self.m_ChallengeIdx = challengeIdx
    self.m_MaxBossIdx = maxBossIdx
    self.m_IsCurrentBossDie = isCurrentBossDie
    g_ScriptEvent:BroadcastInLua("SendGTWChallengePlayInfo")
end

LuaGuildTerritorialWarsMgr.m_ZhanShenAcceptedTaskId = 0
LuaGuildTerritorialWarsMgr.m_ZhanShenProgress = 0
LuaGuildTerritorialWarsMgr.m_ZhanShenIsFinish = false
function LuaGuildTerritorialWarsMgr:SyncGTWZhanShenTaskInfo(acceptedTaskId, progress, isFinish) --接了任务之后才会发的
    self.m_ZhanShenAcceptedTaskId = acceptedTaskId
    self.m_ZhanShenProgress = progress
    self.m_ZhanShenIsFinish = isFinish
    g_ScriptEvent:BroadcastInLua("SyncGTWZhanShenTaskInfo", acceptedTaskId, progress, isFinish)
end

LuaGuildTerritorialWarsMgr.m_ZhanShenTaskIds = {}
function LuaGuildTerritorialWarsMgr:SendGTWZhanShenTaskChooseData(acceptedTaskId, taskIds)
    self.m_ZhanShenAcceptedTaskId = acceptedTaskId
    self.m_ZhanShenTaskIds = taskIds and g_MessagePack.unpack(taskIds) or {}
    table.sort(self.m_ZhanShenTaskIds, function(a, b)
        return GuildTerritoryWar_ZhanShenTask.GetData(a).TaskLevel < GuildTerritoryWar_ZhanShenTask.GetData(b).TaskLevel
    end)
    g_ScriptEvent:BroadcastInLua("SendGTWZhanShenTaskChooseData", acceptedTaskId, self.m_ZhanShenTaskIds)
end
