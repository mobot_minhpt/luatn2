local UIRoot=import "UIRoot"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local NativeTools = import "L10.Engine.NativeTools"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local Setting = import "L10.Engine.Setting"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaServerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaServerWnd, "AgeTipLeft", "AgeTipLeft", GameObject)
RegistChildComponent(LuaServerWnd, "AgeTipRight", "AgeTipRight", GameObject)

--@endregion RegistChildComponent end

function LuaServerWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AgeTipLeft.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAgeTipLeftClick()
	end)


	
	UIEventListener.Get(self.AgeTipRight.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAgeTipRightClick()
	end)


    --@endregion EventBind end

    if not CommonDefs.IS_CN_CLIENT then
        self.AgeTipRight:SetActive(false)
        self.AgeTipLeft:SetActive(false)
        return
    end
    if CommonDefs.Is_UNITY_STANDALONE_WIN() or NativeTools.IsAllChannelApk() then
        self.AgeTipRight:SetActive(false)
        self.AgeTipLeft:SetActive(true)
    else
        self.AgeTipRight:SetActive(true)
        self.AgeTipLeft:SetActive(false)

        if UIRoot.EnableIPhoneXAdaptation then
            Extensions.SetLocalPositionX(self.AgeTipRight.transform, -81 + UIRoot.VirtualIPhoneXWidthMargin)
        end
    end
end

function LuaServerWnd:OnEnable()
    local bgm = SoundManager.Inst.m_BGMusicName
    if bgm  or (not bgm and CRenderScene.Inst and CRenderScene.Inst.m_NowSceneName == Setting.sNewDengLu_Level)then
        if bgm and bgm  ~= SoundManager.s_LoginMusic then
            SoundManager.Inst:StopBGMusic()
        end
        if CRenderScene.Inst and CRenderScene.Inst.m_NowSceneName ~= Setting.sNewDengLu_Level then
            return
        end
        if Setting.sDengLu_Level ~= Setting.sNewDengLu_Level then
            return
        end
        if PlayerSettings.MusicEnabled then
            self.m_FakeBgmSound = SoundManager.Inst:PlaySound(SoundManager.s_LoginMusic,Vector3.zero,SoundManager.Inst.bgMusicVolume,nil,0)
        end
    end
    
end

function LuaServerWnd:OnDisable()
    if self.m_FakeBgmSound then
        SoundManager.Inst:StopSound(self.m_FakeBgmSound)
        self.m_FakeBgmSound = nil
    end
end

function LuaServerWnd:Init()
end

--@region UIEvent

function LuaServerWnd:OnAgeTipLeftClick()
    CUIManager.ShowUI(CLuaUIResources.Login_Age)
end

function LuaServerWnd:OnAgeTipRightClick()
    CUIManager.ShowUI(CLuaUIResources.Login_Age)
end


--@endregion UIEvent

