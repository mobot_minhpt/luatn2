local QnButton=import "L10.UI.QnButton"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"


CLuaQMPKMatchRecordWnd = class()
RegistClassMember(CLuaQMPKMatchRecordWnd,"m_WatchBtn")
RegistClassMember(CLuaQMPKMatchRecordWnd,"m_AdvGridView")
RegistClassMember(CLuaQMPKMatchRecordWnd,"m_CurrentRow")
RegistClassMember(CLuaQMPKMatchRecordWnd,"m_DataSource")

function CLuaQMPKMatchRecordWnd:Awake()
    self.m_WatchBtn = self.transform:Find("ShowArea/ViewButton"):GetComponent(typeof(QnButton))
    self.m_AdvGridView = self.transform:Find("ShowArea/QnAdvView"):GetComponent(typeof(QnTableView))
    self.m_CurrentRow = -1
end

function CLuaQMPKMatchRecordWnd:OnEnable( )
    UIEventListener.Get(self.m_WatchBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnWatchBtnClicked(go) end)
end

function CLuaQMPKMatchRecordWnd:Init( )
    self.m_WatchBtn.Enabled = false

    local getNumFunc=function() return #CLuaQMPKMgr.m_MatchRecordList end
    local initItemFunc=function(item,index) 
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end

        self:InitItem(item,index,CLuaQMPKMgr.m_MatchRecordList[index+1])
    end

    self.m_DataSource=DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.m_AdvGridView.m_DataSource=self.m_DataSource
    self.m_AdvGridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self.m_CurrentRow = row
        if CLuaQMPKMgr.m_MatchRecordList[self.m_CurrentRow+1].m_Win < 0 
            and CLuaQMPKMgr.m_MatchRecordList[self.m_CurrentRow+1].m_IsMatching then
            self.m_WatchBtn.Enabled = true
        else
            self.m_WatchBtn.Enabled = false
        end
    end)
    self.m_AdvGridView:ReloadData(true,false)

    local refreshButton=FindChild(self.transform,"RefreshButton").gameObject
    UIEventListener.Get(refreshButton).onClick = DelegateFactory.VoidDelegate(function(go) 
        Gac2Gas.QueryQmpkWatchList()
    end)
end
function CLuaQMPKMatchRecordWnd:OnWatchBtnClicked( go) 
    Gac2Gas.RequestWatchQmpkBiWu(3,--EnumToInt(CQuanMinPKMgr.Inst.m_MatchStage), 
        CLuaQMPKMgr.m_MatchRecordList[self.m_CurrentRow+1].m_Zhandui1, 
        CLuaQMPKMgr.m_MatchRecordList[self.m_CurrentRow+1].m_Zhandui2)
end

function CLuaQMPKMatchRecordWnd:InitItem(item,index,record)
    local transform=item.transform
    local m_NameLabel={}
    m_NameLabel[0]=FindChild(transform,"NameLabel1"):GetComponent(typeof(UILabel))
    m_NameLabel[1]=FindChild(transform,"NameLabel2"):GetComponent(typeof(UILabel))
    local m_ResultSprite={}
    m_ResultSprite[0]=FindChild(transform,"ResultSprite1"):GetComponent(typeof(UISprite))
    m_ResultSprite[1]=FindChild(transform,"ResultSprite2"):GetComponent(typeof(UISprite))


    local m_CannotWatchObj = transform:Find("CannotWatchMark").gameObject
    local m_MatchingLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    m_NameLabel[0].text = record.m_TeamName1
    m_NameLabel[1].text = record.m_TeamName2
    if record.m_Win >= 0 then
        m_CannotWatchObj:SetActive(true)
        m_MatchingLabel.text = LocalString.GetString("VS")
        m_ResultSprite[0].gameObject:SetActive(true)
        m_ResultSprite[1].gameObject:SetActive(true)

        local default
        if record.m_Win == record.m_Zhandui1 then
            default = "common_fight_result_win"
        else
            default = "common_fight_result_lose"
        end
        m_ResultSprite[0].spriteName = default

        local extern
        if record.m_Win == record.m_Zhandui2 then
            extern = "common_fight_result_win"
        else
            extern = "common_fight_result_lose"
        end
        m_ResultSprite[1].spriteName = extern
    else
        if record.m_IsMatching then
            m_MatchingLabel.text = LocalString.GetString("进行中")
            m_ResultSprite[0].gameObject:SetActive(false)
            m_ResultSprite[1].gameObject:SetActive(false)
            m_CannotWatchObj:SetActive(false)
        else
            m_MatchingLabel.text = LocalString.GetString("准备中")
            m_ResultSprite[0].gameObject:SetActive(false)
            m_ResultSprite[1].gameObject:SetActive(false)
            m_CannotWatchObj:SetActive(true)
        end
    end
end
return CLuaQMPKMatchRecordWnd
