local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local PackageItemInfo = import "L10.UI.CItemInfoMgr+PackageItemInfo"
local NGUIMath = import "NGUIMath"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local IdPartition = import "L10.Game.IdPartition"
local Extensions = import "Extensions"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CCommonItem = import "L10.Game.CCommonItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local BodyItemInfo = import "L10.UI.CItemInfoMgr+BodyItemInfo"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"


CLuaEquipInfoWnd = class()
RegistClassMember(CLuaEquipInfoWnd,"table")
RegistClassMember(CLuaEquipInfoWnd,"tipTemplate")
RegistClassMember(CLuaEquipInfoWnd,"baseItemInfo")
RegistClassMember(CLuaEquipInfoWnd,"equipTips")
RegistClassMember(CLuaEquipInfoWnd,"ExtraPropertyState")
RegistClassMember(CLuaEquipInfoWnd,"IsExtraPropertyFrameVisible")
RegistClassMember(CLuaEquipInfoWnd,"m_TipsCount")
RegistClassMember(CLuaEquipInfoWnd,"NeedAdjustHeight")
RegistClassMember(CLuaEquipInfoWnd,"m_Wnd")
CLuaEquipInfoWnd.Instance = nil
CLuaEquipInfoWnd.FromAttrGroup = false--属性切换

function CLuaEquipInfoWnd:Awake()
    self.table = self.transform:Find("Table"):GetComponent(typeof(UITable))
    self.tipTemplate = self.transform:Find("EquipTip").gameObject
    self.baseItemInfo = nil
    self.equipTips = {}
    self.IsExtraPropertyFrameVisible = false
    self.m_TipsCount = 0
    self.NeedAdjustHeight = true

    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

if CLuaEquipInfoWnd.Instance ~= nil then
    -- L10.CLogMgr.LogError("more than one instance running!" .. ToStringWrap(self:GetType()))
end
CLuaEquipInfoWnd.Instance = self
self.tipTemplate:SetActive(false)
end


function CLuaEquipInfoWnd:OnDestroy( )
    CLuaEquipInfoWnd.FromAttrGroup = false
    CLuaEquipInfoWnd.Instance = nil
    CItemInfoMgr.NeedAdjustHeight = true
end
function CLuaEquipInfoWnd:Init( )
    self.baseItemInfo = nil
    self.m_TipsCount = 0
    local default = CItemInfoMgr.showType
    if default == CItemInfoMgr.ShowType.Package then
        local item = CItemMgr.Inst:GetById(CItemInfoMgr.packageItemInfo.itemId)
        if item ~= nil then
            self.baseItemInfo = CItemInfoMgr.packageItemInfo
            self:Init0(item)
        end
    elseif default == CItemInfoMgr.ShowType.Body then
        local item = CItemMgr.Inst:GetById(CItemInfoMgr.bodyItemInfo.itemId)
        if item ~= nil then
            self.baseItemInfo = CItemInfoMgr.bodyItemInfo
            self:Init0(item)
        end
    elseif default == CItemInfoMgr.ShowType.Link then
        local item = CItemInfoMgr.linkItemInfo.item
        if item ~= nil then
            self.baseItemInfo = CItemInfoMgr.linkItemInfo
            self:Init0(item)
        end
        local template = CItemMgr.Inst:GetEquipTemplate(CItemInfoMgr.linkItemInfo.templateId)
        if template ~= nil then
            self.baseItemInfo = CItemInfoMgr.linkItemInfo
            self:Init1(template, false, false)
        end
    elseif default == CItemInfoMgr.ShowType.XianjiaPreview then
        local template = CItemMgr.Inst:GetEquipTemplate(CItemInfoMgr.linkItemInfo.templateId)
        if template ~= nil then
            self.baseItemInfo = CItemInfoMgr.linkItemInfo
            self:Init1(template, true, false)
        end
    elseif default == CItemInfoMgr.ShowType.QianKunDai then
        local item = CItemInfoMgr.linkItemInfo.item
        if item ~= nil then
            self.baseItemInfo = CItemInfoMgr.linkItemInfo
            self:Init0(item)
        end
    end
end
function CLuaEquipInfoWnd:UpdateDisplay( )
    if self.baseItemInfo ~= nil then
        local item = nil
        if TypeIs(self.baseItemInfo, typeof(LinkItemInfo)) then
            local linkInfo = TypeAs(self.baseItemInfo, typeof(LinkItemInfo))
            if linkInfo.item ~= nil then
                --尝试查询一次是否是自己的装备，如果是，则刷新数据
                local newItem = CItemMgr.Inst:GetById(linkInfo.item.Id)
                if newItem ~= nil then
                    linkInfo.item = newItem
                end
                item = linkInfo.item
            end
        else
            item = CItemMgr.Inst:GetById(self.baseItemInfo.itemId)
        end
        if item ~= nil then
            self:Init0(item)
        end
    end
end
function CLuaEquipInfoWnd:OnEquipTipLayoutWnd( )
    if self.baseItemInfo ~= nil then
        self.table:Reposition()
        self:AdjustPosition(self.baseItemInfo.alignType, self.baseItemInfo.targetCenterPos, self.baseItemInfo.targetSize)
    end
end
function CLuaEquipInfoWnd:Update( )
    self.m_Wnd:ClickThroughToClose()
end

-- init0是具体装备
function CLuaEquipInfoWnd:Init0( item) 
    self.equipTips = {}
    Extensions.RemoveAllChildren(self.table.transform)

    if not item.IsEquip then
        self.m_Wnd:Close()
        return
    end

    local commonItems = CreateFromClass(MakeGenericClass(List, CCommonItem))
    CommonDefs.ListAdd(commonItems, typeof(CCommonItem), item)

    if TypeIs(self.baseItemInfo, typeof(PackageItemInfo)) 
    or (TypeIs(self.baseItemInfo, typeof(LinkItemInfo)) and (TypeAs(self.baseItemInfo, typeof(LinkItemInfo))).needCompare) then
        --TODO 检查一下装备格子
        local bodyItemIds = CreateFromClass(MakeGenericClass(List, String))
        local equipment = EquipmentTemplate_Equip.GetData(item.TemplateId)
        if equipment ~= nil then
            if CClientMainPlayer.Inst ~= nil then
                local itemProp = CClientMainPlayer.Inst.ItemProp
                local itemId = nil
                if equipment.Type == EnumToInt(EnumBodyPosition.Ring) or equipment.Type == EnumToInt(EnumBodyPosition.Bracelet) then
                    itemId = itemProp:GetItemAt(EnumItemPlace.Body, (equipment.Type) + 3)
                    if not System.String.IsNullOrEmpty(itemId) then
                        CommonDefs.ListAdd(bodyItemIds, typeof(String), itemId)
                    end
                end
                local type = equipment.Type
                if IdPartition.IdIsTalisman(equipment.ID) then
                    type = type + CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanCurIndex * 8
                end

                if item.Equip.SubHand > 0 then
                    itemId = itemProp:GetItemAt(EnumItemPlace.Body, 2)--副手在盾上
                else
                    itemId = itemProp:GetItemAt(EnumItemPlace.Body, type)
                end
                if not System.String.IsNullOrEmpty(itemId) then
                    CommonDefs.ListAdd(bodyItemIds, typeof(String), itemId)
                end

                do
                    local i = 0
                    while i < bodyItemIds.Count do
                        local bodyItem = CItemMgr.Inst:GetById(bodyItemIds[i])
                        if bodyItem ~= nil and bodyItem.IsEquip then
                            CommonDefs.ListAdd(commonItems, typeof(CCommonItem), bodyItem)
                        end
                        i = i + 1
                    end
                end
            end
        end
    elseif TypeIs(self.baseItemInfo, typeof(BodyItemInfo)) then
        --nothing to do
    end

    self.m_TipsCount = commonItems.Count

    for i = self.IsExtraPropertyFrameVisible and 0 or commonItems.Count - 1, 0, - 1 do
        local instance = NGUITools.AddChild(self.table.gameObject, self.tipTemplate)
        instance:SetActive(true)
        local script = instance:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        local equipTip = script.m_LuaSelf
        if i == 0 then
            if commonItems.Count > 1 then
                local items = {}--CreateFromClass(MakeArrayClass(CEquipment), commonItems.Count-1)
                for i=1,commonItems.Count-1 do
                    -- items[i-1] = commonItems[i]
                    table.insert( items,commonItems[i] )
                end
                equipTip:Init0(commonItems[i], self.baseItemInfo, items)
            else
                equipTip:Init0(commonItems[i], self.baseItemInfo, nil)
            end
        else
            equipTip:Init0(commonItems[i], nil, {commonItems[0]})
        end
        table.insert( self.equipTips, equipTip )
    end

    self.table:Reposition()

    local maxTop = 0
    for i,v in ipairs(self.equipTips) do
        local top = v:GetTopOffset()
        if top > maxTop then
            maxTop = top
        end
    end
    for i,v in ipairs(self.equipTips) do
        v:SetTopOffset(maxTop)
    end

    self:AdjustPosition(self.baseItemInfo.alignType, self.baseItemInfo.targetCenterPos, self.baseItemInfo.targetSize)
end

function CLuaEquipInfoWnd:Init1(template, xianjiaPreview, otherPlayerFaBao) 
    self.equipTips = {}
    Extensions.RemoveAllChildren(self.table.transform)

    if template == nil then
        self.m_Wnd:Close()
        return
    end

    self.m_TipsCount = 1

    local instance = NGUITools.AddChild(self.table.gameObject, self.tipTemplate)
    instance:SetActive(true)
    local script = instance:GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    local equipTip = script.m_LuaSelf
    
    if not xianjiaPreview then
        if Talisman_XianJia.GetData(template.ID) ~= nil then
            equipTip:InitXianjiaPreview(template, false)
        else
            equipTip:Init1(template, self.baseItemInfo)
        end
    else
        equipTip:InitXianjiaPreview(template, true)
    end

    -- 处理炼化物品的额外信息
    self:AddLianHuaItemInfo(template.ID, equipTip)

    table.insert( self.equipTips,equipTip )
    self.table:Reposition()

    local maxTop = 0
    for i,v in ipairs(self.equipTips) do
        local top = v:GetTopOffset()
        if top > maxTop then
            maxTop = top
        end
    end
    for i,v in ipairs(self.equipTips) do
        v:SetTopOffset(maxTop)
    end

    self:AdjustPosition(self.baseItemInfo.alignType, self.baseItemInfo.targetCenterPos, self.baseItemInfo.targetSize)
end

function CLuaEquipInfoWnd:AddLianHuaItemInfo(templateId, equipTip)
    local data = LianHua_Item.GetData(templateId)
    if data~=nil and LuaZongMenMgr.m_RefineMonsterItemLabels ~=nil then
        local quality = data.Quality
        for i=1,#LuaZongMenMgr.m_RefineMonsterItemLabels do
            equipTip:AddTitle(LuaZongMenMgr.m_RefineMonsterItemLabels[i], Color.white)
        end
        equipTip:LayoutWnd()
    end
    LuaZongMenMgr.m_RefineMonsterItemLabels = nil
end

function CLuaEquipInfoWnd:OnSetItemAt( args) 
    local place, position, oldItemId, newItemId=args[0],args[1],args[2],args[3]
    --check place
    repeat
        local default = place
        if default == EnumItemPlace.Body then
            if not (TypeIs(self.baseItemInfo, typeof(BodyItemInfo))) then
                return
            end
            break
        elseif default == EnumItemPlace.Bag or default == EnumItemPlace.Task then
            if not (TypeIs(self.baseItemInfo, typeof(PackageItemInfo))) then
                return
            end
            break
        end
    until 1
    local pos = self.baseItemInfo.position
    --check position
    if pos ~= position then
        return
    end


    local itemProp = CClientMainPlayer.Inst.ItemProp
    local id = itemProp:GetItemAt(place, position)
    --这个位置上的物品 不是原来的物品，关掉
    if id ~= self.baseItemInfo.itemId then
        self.m_Wnd:Close()
        return
    end

    -- bingo，更新下
    local item = CItemMgr.Inst:GetById(id)
    if item ~= nil then
        self:Init0(item)
        return
    else
        self.m_Wnd:Close()
    end
end
function CLuaEquipInfoWnd:OnEquipDurationUpdate( args) 
    local place, position=args[0],args[1]
    if CClientMainPlayer.Inst ~= nil then
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(place, position)
        if not(id==nil or id=="") then
            for i,v in ipairs(self.equipTips) do
                if v.ItemId == id then
                    self:UpdateDisplay()
                    break
                end
            end
        end
    end
end
function CLuaEquipInfoWnd:OnSendItem( args) 
    local itemId = args[0]
    if not(itemId==nil or itemId=="") then
        for i,v in ipairs(self.equipTips) do
            if v.ItemId==itemId then
                self:UpdateDisplay()
                break
            end
        end
    end
end
function CLuaEquipInfoWnd:OnQueryEquipGiverNameReturn(args) 
    local giverId, itemId = args[0],args[1]
    for i,v in ipairs(self.equipTips) do
        if v.ItemId==itemId then
            self:UpdateDisplay()
            break
        end
    end

end
function CLuaEquipInfoWnd:AdjustPosition( type, worldPos, targetSize) 
    if self.IsExtraPropertyFrameVisible then
        self.table.transform.localPosition = Vector3.zero
        return
    end

    local selfBounds
    repeat
        local default = type
        if default == CItemInfoMgr.AlignType.Default then
            self.table.transform.localPosition = Vector3.zero
            break
        elseif default == CItemInfoMgr.AlignType.Left then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(self.table.transform.parent:InverseTransformPoint(worldPos).x - targetSize.x * 0.5 - selfBounds.size.x * 0.5, 0, 0)
            break
        elseif default == CItemInfoMgr.AlignType.Right then
            selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform)
            self.table.transform.localPosition = Vector3(self.table.transform.parent:InverseTransformPoint(worldPos).x + targetSize.x * 0.5 + selfBounds.size.x * 0.5, 0, 0)
            break
        elseif default == CItemInfoMgr.AlignType.ScreenLeft then
            do
                if #self.equipTips > 1 then
                    self.table.transform.localPosition = Vector3.zero
                else
                    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
                    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
                    self.table.transform.localPosition = Vector3(- virtualScreenWidth * 0.25, 0, 0)
                end
            end
            break
        elseif default == CItemInfoMgr.AlignType.ScreenRight then
            do
                if #self.equipTips > 1 then
                    self.table.transform.localPosition = Vector3.zero
                else
                    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
                    local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
                    self.table.transform.localPosition = Vector3(virtualScreenWidth * 0.25, 0, 0)
                end
            end
            break
        end
    until 1
end

function CLuaEquipInfoWnd:GetGuideGo( methodName) 
    if methodName == "GetEquipButton" then
        if #self.equipTips > 0 then
            return self.equipTips[#self.equipTips]:GetEquipButton()
        end
    elseif methodName == "GetTransferButton" then
        if #self.equipTips > 0 then
            return self.equipTips[#self.equipTips]:GetTransferButton()
        end
    end
end

function CLuaEquipInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:AddListener("MainPlayerEquipDurationUpdate",self,"OnEquipDurationUpdate")
    g_ScriptEvent:AddListener("OnQueryEquipGiverNameReturn",self,"OnQueryEquipGiverNameReturn")
    g_ScriptEvent:AddListener("OnEquipTipLayoutWnd", self, "OnEquipTipLayoutWnd")
end
function CLuaEquipInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    g_ScriptEvent:RemoveListener("MainPlayerEquipDurationUpdate",self,"OnEquipDurationUpdate")
    g_ScriptEvent:RemoveListener("OnQueryEquipGiverNameReturn",self,"OnQueryEquipGiverNameReturn")
    g_ScriptEvent:RemoveListener("OnEquipTipLayoutWnd", self, "OnEquipTipLayoutWnd")
end
