local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTabView = import "L10.UI.QnTabView"
local QnTableView = import "L10.UI.QnTableView"
local QnTableItem=import "L10.UI.QnTableItem"

LuaJuDianBattleRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaJuDianBattleRankWnd, "TopTab", "TopTab", QnTabView)
RegistChildComponent(LuaJuDianBattleRankWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end

function LuaJuDianBattleRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaJuDianBattleRankWnd:Init()
    -- 打开的rpc是这个
    --Gac2Gas.GuildJuDianQueryRankInfo()

    self.m_RankData = LuaJuDianBattleMgr.RankData

    -- 刷新一下tableview
    self.TopTab.OnSelect = DelegateFactory.Action_QnTabButton_int(function (btn, index)
        self.TableView:ReloadData(true,false)
    end)


    local myGuildId = LuaJuDianBattleMgr:GetSelfGuildId()

    for i = 1, 7 do
        self.TopTab.transform:Find(tostring(i)).gameObject:SetActive(false)
    end

    -- 戊己庚西
    local nameList = {LocalString.GetString("甲组"),LocalString.GetString("乙组"),LocalString.GetString("丙组"),
        LocalString.GetString("丁组"),LocalString.GetString("戊组"), LocalString.GetString("己组"), LocalString.GetString("庚组")}

    -- 确定默认选中
    -- table.insert(rankTbl[battleFieldId], {guildId, guildName, serverName, equipScore, killCount, occupyScore})
    local selectIndex = 1
    for i, list in pairs(self.m_RankData) do
        local item = self.TopTab.transform:Find(tostring(i))
        item.gameObject:SetActive(true)
        local label = item:Find("Label"):GetComponent(typeof(UILabel))

        if list.IsCrossServer then
            label.text = SafeStringFormat3("%s%s", nameList[i], LocalString.GetString("(跨服)"))
        else
            label.text = nameList[i]
        end

        for j, data in pairs(list) do
            if type(data)== "table" and data[1] == myGuildId then
                selectIndex = i
            end
        end
    end

    -- 设置table
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return self:CurrentCount()
    end, function(item, index)
        index = index + 1
        self:InitItem(item, index)
    end)

    self.TopTab:ChangeTo(selectIndex-1)
end

function LuaJuDianBattleRankWnd:CurrentCount()
    local index = self.TopTab.CurrentSelectTab
    if self.m_RankData and self.m_RankData[index + 1] then
        return #self.m_RankData[index + 1]
    end

    return 0
end

function LuaJuDianBattleRankWnd:InitItem(item, index)
    local tabIndex = self.TopTab.CurrentSelectTab + 1
    local list = self.m_RankData[tabIndex]

    local data = list[index]

    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = item.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    local guildNameLabel = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    local serverNameLabel = item.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
    local equipLabel = item.transform:Find("EquipLabel"):GetComponent(typeof(UILabel))
    local killLabel = item.transform:Find("KillLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    local bg = item.transform:GetComponent(typeof(QnTableItem))
    if index %2 == 0 then
        bg.m_NormalSpirte = "common_bg_mission_background_n"
    else
        bg.m_NormalSpirte = "common_bg_mission_background_s"
    end

    local rankSp = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}
    if index > 3 then
        rankLabel.text = tostring(index)
        rankImage.gameObject:SetActive(false)
    else
        rankLabel.text = ""
        rankImage.gameObject:SetActive(true)
        rankImage.spriteName = rankSp[index]
    end

    -- table.insert(rankTbl[battleFieldId], {guildId, guildName, serverName, equipScore, killCount, occupyScore})
    guildNameLabel.text = data[2]
    serverNameLabel.text = data[3]
    equipLabel.text = data[4]
    killLabel.text = data[5]
    scoreLabel.text = data[6]
end

--@region UIEvent

--@endregion UIEvent

