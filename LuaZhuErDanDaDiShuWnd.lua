local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local GameObject = import "UnityEngine.GameObject"
local UISlider=import "UISlider"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Object = import "System.Object"

LuaZhuErDanDaDiShuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "Holes", "Holes", GameObject)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "Fail", "Fail", GameObject)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "AllScoreLabel", "AllScoreLabel", UILabel)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "ResultFx", "ResultFx", CUIFx)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "Progress", "Progress", UISlider)
RegistChildComponent(LuaZhuErDanDaDiShuWnd, "TipLabel", "TipLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_Items")

RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_TotalTime")
RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_PassScore")
RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_GameScore")

RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_CountDownTick")
RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_TimeCounter")
RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_StartTime")
RegistClassMember(LuaZhuErDanDaDiShuWnd, "m_IsFinished")


function LuaZhuErDanDaDiShuWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
	self.Success:SetActive(false)
	self.Fail:SetActive(false)

	self.m_StartTime=0
	self.TipLabel.text = g_MessageMgr:FormatMessage("ZhuErDan_DaDiShu_Tip")
end

function LuaZhuErDanDaDiShuWnd:Init()
    self.m_Items = {}
    for i = 1, self.Holes.transform.childCount do
        local item = self.Holes.transform:GetChild(i-1):GetChild(0)
        local script = item:GetComponent(typeof(CCommonLuaScript))
        script:Init({})
        local t = script.m_LuaSelf
        table.insert(self.m_Items,t)
    end
	local setting = ZhuJueJuQing_ZhuErDanSetting.GetData()
	self.m_TotalTime = setting.FallingToyTotalTime
	self.m_PassScore = setting.FallingToyPassScore
	self.AllScoreLabel.text = "/"..tostring(self.m_PassScore)

	self.Progress.value = 1

	self.m_StartTime=Time.time
	self.m_IsFinished = false
    self:StartGame()
end
function LuaZhuErDanDaDiShuWnd:DestroyCountDownTick()
	self.m_TimeCounter = 0
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end

end
function LuaZhuErDanDaDiShuWnd:StartGame()
	self.m_GameScore = 0

	-- 倒计时
	self:DestroyCountDownTick()
	self.m_CountDownTick = RegisterTick(function ()
		self.m_TimeCounter = self.m_TimeCounter + 1
		local leftTime = self.m_TotalTime - self.m_TimeCounter
		
		if leftTime < 0 then
			self:EndGame()
			self:DestroyCountDownTick()
		else
			self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("倒计时%d秒"),leftTime)
		end
	end, 1000)

	self.Success:SetActive(false)
	self.Fail:SetActive(false)
end

--@region UIEvent

--@endregion UIEvent

function LuaZhuErDanDaDiShuWnd:Update()
	if self.m_IsFinished then return end

    if self.m_StartTime>0 then
		local v = 1-(Time.time-self.m_StartTime)/self.m_TotalTime
		if v>=0 then
			self.Progress.value = v
		end
	end
end


function LuaZhuErDanDaDiShuWnd:OnEnable()
	g_ScriptEvent:AddListener("ZhuErDan_DaDiShu_AddScore",self,"OnAddScore")
end
function LuaZhuErDanDaDiShuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ZhuErDan_DaDiShu_AddScore",self,"OnAddScore")
end
function LuaZhuErDanDaDiShuWnd:OnAddScore(score)
	self.m_GameScore = self.m_GameScore+score
	self.ScoreLabel.text = tostring(self.m_GameScore)
	if self.m_GameScore >= self.m_PassScore then
		self:EndGame()
	end
end

function LuaZhuErDanDaDiShuWnd:EndGame()
	self.m_IsFinished = true

	if self.m_GameScore >= self.m_PassScore then
		self.Success:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_pengpengche_shengli.prefab")
		local empty = CreateFromClass(MakeGenericClass(List, Object))
		empty = MsgPackImpl.pack(empty)
		Gac2Gas.FinishClientTaskEvent(22120608,"ZhuErDan_DaDiShu",empty)

		g_ScriptEvent:BroadcastInLua("ZhuErDan_DaDiShu_Finish",true)

	else
		self.Fail:SetActive(true)
		self.ResultFx:LoadFx("Fx/UI/Prefab/UI_pengpengche_shibai.prefab")
		g_ScriptEvent:BroadcastInLua("ZhuErDan_JieDianXin_Finish",false)
	end
	RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.ZhuErDanDaDiShuWnd)
	end, 5000)
end

function LuaZhuErDanDaDiShuWnd:OnDestroy()
    self:DestroyCountDownTick()
end