require("common/common_include")
local Random = import "UnityEngine.Random"
local Object = import "UnityEngine.Object"
local Extensions = import "Extensions"
local Time = import "UnityEngine.Time"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"

CLuaLaternEffect = class()
RegistClassMember(CLuaLaternEffect, "m_LifeTime")
RegistClassMember(CLuaLaternEffect, "m_RiseSpeed")

function CLuaLaternEffect:OnEnable( ... )
	self.m_LifeTime = 10
	self.m_RiseSpeed = Random.Range(100, 200)
	Object.Destroy(self.gameObject, self.m_LifeTime)
	UIEventListener.Get(self.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:SnatchHongbao()
	end)
end

function CLuaLaternEffect:Update( ... )
	local newy = self.transform.localPosition.y + self.m_RiseSpeed * Time.deltaTime
	Extensions.SetLocalPositionY(self.transform, newy)
end

function CLuaLaternEffect:SnatchHongbao( ... )
	EventManager.BroadcastInternalForLua(EnumEventType.RequestSnatchSystemHongBao, {})
end

return CLuaLaternEffect