local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local Convert=import "System.Convert"
local CTooltip = import "L10.UI.CTooltip"
local MessageMgr = import "L10.Game.MessageMgr"
local CClientFormula = import "L10.Game.CClientFormula"
LuaPlayerDetailPropertyView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPlayerDetailPropertyView, "Item", "Item", GameObject)
RegistChildComponent(LuaPlayerDetailPropertyView, "Table", "Table", UITable)

--@endregion RegistChildComponent end
RegistClassMember(LuaPlayerDetailPropertyView, "m_PropertyList")
RegistClassMember(LuaPlayerDetailPropertyView, "m_PropertyData")
RegistClassMember(LuaPlayerDetailPropertyView, "m_PropertyValue")
RegistClassMember(LuaPlayerDetailPropertyView, "m_OriColor")
RegistClassMember(LuaPlayerDetailPropertyView, "m_ChangeColor")
RegistClassMember(LuaPlayerDetailPropertyView, "m_ClickTick")
RegistClassMember(LuaPlayerDetailPropertyView, "m_WordsColor")
RegistClassMember(LuaPlayerDetailPropertyView, "m_normalPropertyValueLabelWidth")
RegistClassMember(LuaPlayerDetailPropertyView, "m_mfPropertyValueLabelWidth")
RegistClassMember(LuaPlayerDetailPropertyView, "m_OnMainplayerFightPropUpdate")
function LuaPlayerDetailPropertyView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_normalPropertyValueLabelWidth = 160
    self.m_mfPropertyValueLabelWidth = 500
    self.m_PropertyList = {}
    self.m_PropertyValue = {}
    self.m_ChangeColor = "fffe91"
    self.m_OriColor = self.Item.transform:Find("KeyValuePair/NameLabel"):GetComponent(typeof(UILabel)).color
    self.m_WordsColor = CreateFromClass(MakeGenericClass(List, UInt32))
    self.m_ClickTick = nil
    self.m_OnMainplayerFightPropUpdate = DelegateFactory.Action(function()
        self:UpdateValue() 
    end)

end

function LuaPlayerDetailPropertyView:Init()
    self.Item.gameObject:SetActive(false)
    self:InitTable() 
    self:UpdateValue()   
end
function LuaPlayerDetailPropertyView:InitTable()
    
    Extensions.RemoveAllChildren(self.Table.transform)
    PropGuide_PropertyDetail.Foreach(function(k,v)
        local Section = CUICommonDef.AddChild(self.Table.gameObject,self.Item)
        Section:SetActive(true)
        Section.transform:Find("Header/NameLabel"):GetComponent(typeof(UILabel)).text = v.TitleName
        self:InitSection(Section,v.Content)
    end)
    self.Table:Reposition()
    self.gameObject.transform:Find("Scroll View"):GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
end
function LuaPlayerDetailPropertyView:InitSection(SectionGo,PropertyList)
    if not SectionGo or not PropertyList then return end
    SectionGo.transform:Find("KeyValuePair").gameObject:SetActive(false)
    local kvPair = self.Item.transform:Find("KeyValuePair").gameObject
    local Grid = SectionGo.transform:Find("Grid")
    Extensions.RemoveAllChildren(Grid.transform)
    for i = 0,PropertyList.Length - 1 do
        local PropertyGo = CUICommonDef.AddChild(Grid.gameObject,kvPair)
        self:InitOneProperty(PropertyGo,PropertyList[i])
    end
    Grid:GetComponent(typeof(UIGrid)):Reposition()
    local bg = SectionGo.transform:GetComponent(typeof(UISprite))
    local bound = NGUIMath.CalculateRelativeWidgetBounds(bg.transform,Grid)
    bg.height = math.abs(bound.min.y) + 20
end
function LuaPlayerDetailPropertyView:InitOneProperty(propertyGo,propID)
    if not propertyGo then return end
    local Namego = propertyGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local ValueGo = propertyGo.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local data = PropGuide_Property.GetData(propID) 
    if not data then return end
    self.m_PropertyList[propID] = propertyGo
    self.m_PropertyValue[propID] = {go = ValueGo, fightPropId = data.FightPropID, name = data.Name}

    Namego.text = data.Name
    UIEventListener.Get(Namego.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        -- 属性详析按钮
        if LuaPlayerPropertyMgr.PropertyGuide and data.ShowBtn == 1 then
            self:ShowBtn(go,propID)
        else
	        self:ShowMessage(go,data.Desc)
        end
        -- 关联属性变色
        if LuaPlayerPropertyMgr.PropertyGuide and data.RelateProperty then
            self:SetWorldColor(propID)
            -- 遍历RelateProperty
            for i = 0,data.RelateProperty.Length - 1 do
                self:SetWorldColor(data.RelateProperty[i])
            end
            -- 起Tick
            self:ClearTick()
            self.m_ClickTick = RegisterTickOnce(function()
                self:ClearTick()
                self:RevertColors()
            end,PropGuide_Setting.GetData().RelatePropertyTime * 1000)
        end
	end)
end

function LuaPlayerDetailPropertyView:UpdateValue()
    if not CClientMainPlayer.Inst then return end
    for k,v in pairs(self.m_PropertyValue) do
        local type = CommonDefs.ConvertIntToEnum(typeof(EPlayerFightProp), v.fightPropId)
        local label = v.go
        if label.width ~= self.m_normalPropertyValueLabelWidth then label.width = self.m_normalPropertyValueLabelWidth end
        if v.name == LocalString.GetString("当前经验") then
            local expVal = CClientMainPlayer.Inst.PlayProp.Exp
            --label.text = CUICommonDef.GetShortExpStr(CClientMainPlayer.Inst.PlayProp.Exp)
            if expVal < 10000000000 then
                label.text =  CUICommonDef.GetShortExpStr(expVal)
            else
                label.text = SafeStringFormat3(LocalString.GetString("%.2f亿"), expVal / 100000000)
            end
        elseif v.name == LocalString.GetString("当前气血") then
            local curHp = CClientMainPlayer.Inst.Hp
            label.text = tostring(math.max(1,math.floor(curHp)))
        elseif v.name == LocalString.GetString("当前气血上限") then
            local curHpFull = CClientMainPlayer.Inst.CurrentHpFull
            label.text = tostring(math.max(1,math.floor(curHpFull)))
        elseif v.name == LocalString.GetString("升级经验") then
            local val = CClientMainPlayer.Inst:GetMaxExp()
            if val == System.UInt64.MaxValue then
                label.text = LocalString.GetString("等级已满")
            else
                if val < 10000000000 then
                    label.text =  CUICommonDef.GetShortExpStr(val)
                else
                    label.text = SafeStringFormat3(LocalString.GetString("%.2f亿"), val / 100000000)
                end
            end
        elseif v.name == LocalString.GetString("修为") then
            label.text = tostring(NumberTruncate(CClientMainPlayer.Inst.BasicProp.XiuWeiGrade, 1))
        elseif v.name == LocalString.GetString("修炼") then
            label.text = tostring(NumberTruncate(CClientMainPlayer.Inst.BasicProp.XiuLian, 1))
        elseif v.name == LocalString.GetString("活力") then
            label.text = tostring(CClientMainPlayer.Inst.PlayProp.HuoLi)
        elseif v.name == LocalString.GetString("PK值") then
            label.text = tostring(math.max(0,CClientMainPlayer.Inst.FightProp.PkValue))
        elseif v.fightPropId and v.fightPropId > 0 then
            local val = CClientMainPlayer.Inst.FightProp:GetParam(type)
            if type == EPlayerFightProp.mSpeed or type == EPlayerFightProp.pSpeed then    -- 攻速
                if val < 1E-06 then val = 1 end
                label.text =  System.String.Format("{0:F2}",val)
            elseif type == EPlayerFightProp.pFatalDamage or type == EPlayerFightProp.mFatalDamage
            or type == EPlayerFightProp.AntipFatalDamage or type == EPlayerFightProp.AntimFatalDamage then
                label.text =  System.String.Format("{0}%",tostring(NumberTruncate(val, 3) * 100))
            elseif type == EPlayerFightProp.MF then
                label.width = self.m_mfPropertyValueLabelWidth
                if val < 1E-06 then
                    label.text = System.String.Format(LocalString.GetString("{0} (幸运值有助于提升红紫鬼装爆率)"), CClientMainPlayer.Inst.StrMF)
                else
                    local formula = CommonDefs.GetFormulaObject(200, typeof(Formula2))
                    local displayVal = (formula ~= nil and math.floor((CClientFormula.Inst:Formula_200(val) * 100)) or 0)
                    label.text =  System.String.Format(LocalString.GetString("{0} (红紫鬼装爆率提升[c][00ff00]{1}%[-][/c])"), CClientMainPlayer.Inst.StrMF, displayVal)
                end
            elseif v.name == LocalString.GetString("物理减伤比例") or v.name == LocalString.GetString("法术减伤比例") then
                val = -1 * val
                if math.abs(val) < System.Double.Epsilon then
                    label.text = LocalString.GetString("0")
                else
                    label.text =  System.String.Format("{0:F1}%", val * 100)
                end
            else
                label.text = tostring((math.floor(val)))
            end
        else
            label.text = LocalString.GetString("无数据")
        end
    end
end
function LuaPlayerDetailPropertyView:Update()
    if Input.GetMouseButtonDown(0) then    
        self:ClearTick()
        self:RevertColors()
    end
end

function LuaPlayerDetailPropertyView:RevertColors()
    for i = self.m_WordsColor.Count - 1 , 0, -1 do
        self:RevertWordColor(self.m_WordsColor[i])
    end
end
function LuaPlayerDetailPropertyView:ShowBtn(go,propertyID)
    local showWnd = function()
        LuaPlayerPropertyMgr:ShowPlayerPropertyDetailWnd(propertyID)
    end
    LuaToolBtnMgr:ShowToolBtn(PropGuide_Setting.GetData().PropertyGuideName,showWnd,go.transform)
end
function LuaPlayerDetailPropertyView:ShowMessage(go,MessageName)
    CTooltip.Show(MessageMgr.Inst:FormatMessage(MessageName,{}), go.transform, CTooltip.AlignType.Top);
end
function LuaPlayerDetailPropertyView:SetWorldColor(propID)
    local go = self.m_PropertyList[propID]
    if not go then return end
    if self.m_WordsColor:Contains(propID) then return end
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24(self.m_ChangeColor, 0)
    self.m_WordsColor:Add(propID)
end
function LuaPlayerDetailPropertyView:RevertWordColor(propID)
    local go = self.m_PropertyList[propID]
    if not go then return end
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).color = self.m_OriColor
    if self.m_WordsColor:Contains(propID) then
        self.m_WordsColor:Remove(propID)
    end
end

function LuaPlayerDetailPropertyView:ClearTick()
    if self.m_ClickTick ~= nil then
        UnRegisterTick(self.m_ClickTick)
        self.m_ClickTick = nil
    end
end

function LuaPlayerDetailPropertyView:OnEnable()
    EventManager.AddListenerInternal(EnumEventType.MainplayerFightPropUpdate, self.m_OnMainplayerFightPropUpdate)
    EventManager.AddListenerInternal(EnumEventType.MainPlayerPlayPropUpdate, self.m_OnMainplayerFightPropUpdate)
    EventManager.AddListenerInternal(EnumEventType.MainPlayerBasicPropUpdate, self.m_OnMainplayerFightPropUpdate)
end

function LuaPlayerDetailPropertyView:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.MainplayerFightPropUpdate, self.m_OnMainplayerFightPropUpdate)
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerPlayPropUpdate, self.m_OnMainplayerFightPropUpdate)
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerBasicPropUpdate, self.m_OnMainplayerFightPropUpdate)
    self:ClearTick()
end
--@region UIEvent

--@endregion UIEvent

