local CommonDefs = import "L10.Game.CommonDefs"
local CUITexture = import "L10.UI.CUITexture"
local Camera = import "UnityEngine.Camera"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local Vector3 = import "UnityEngine.Vector3"
local Quaternion = import "UnityEngine.Quaternion"
local LayerDefine = import "L10.Engine.LayerDefine"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local CUIFx = import "L10.UI.CUIFx"
local UITexture = import "UITexture"

LuaFxAndModelPreview = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFxAndModelPreview, "PreviewTexture", "PreviewTexture", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaFxAndModelPreview, "m_RenderTexture")
RegistClassMember(LuaFxAndModelPreview, "m_IdentifierTf")
function LuaFxAndModelPreview:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaFxAndModelPreview:Init(identifier,loader,modelRotation,localScale,rootPosX, rootPosY, rootPosZ,isHighRes,parentTf)
    local fxModelRoot
    if self.m_IdentifierTf then
        self.m_IdentifierTf = self.transform:Find(identifier)
        self.m_RenderTexture = self.m_IdentifierTf:GetComponent(typeof(Camera)).targetTexture
        fxModelRoot = self.m_IdentifierTf:Find("FxModelRoot")
    else
        local go = GameObject(identifier)
        local camera = CommonDefs.AddComponent_GameObject_Type(go, typeof(Camera))
        camera.clearFlags = CameraClearFlags.SolidColor
        -- camera.backgroundColor = Color(0, 0, 0, 0)
        camera.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D
        camera.orthographic = true
        -- camera.fieldOfView = 30
        camera.farClipPlane = 10
        camera.nearClipPlane = -10
        camera.depth = 2
        camera.transform.parent = self.m_IdentifierTf
        --camera.transform.localRotation = Quaternion.Euler(23, 120, 0)
        camera.orthographicSize = 1

        local _modelRoot = GameObject("FxModelRoot")
        _modelRoot.transform.parent = go.transform
        _modelRoot.transform.localRotation = Quaternion.Euler(0, modelRotation, 0)
        _modelRoot.transform.localScale = Vector3(localScale,localScale,localScale)
        _modelRoot.transform.localPosition = Vector3(rootPosX, rootPosY, rootPosZ)
        fxModelRoot = _modelRoot

        local size = 512
        if isHighRes then
            size = 1024
        end

        local rt = RenderTexture(size,size,24,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
        rt.wrapMode = TextureWrapMode.Clamp
        rt.filterMode = FilterMode.Bilinear
        rt.anisoLevel = isHighRes and 2 or 0
        rt.name = camera.name
        camera.targetTexture = rt

        self.m_RenderTexture = rt
        local cuifx = CommonDefs.AddComponent_GameObject_Type(fxModelRoot, typeof(CUIFx))
        local uitexture = CommonDefs.AddComponent_GameObject_Type(fxModelRoot, typeof(UITexture))
        self.m_IdentifierTf = go.transform
        self.m_IdentifierTf.position = Vector3(7,-4,0)
    end
    local fx = fxModelRoot.transform:GetComponent(typeof(CUIFx))

    if loader then
        loader(fx)
    end

    self.PreviewTexture.mainTexture = self.m_RenderTexture
    self.m_IdentifierTf.parent = parentTf
    self.m_IdentifierTf.localScale = Vector3.one
    self.m_IdentifierTf.rotation = Quaternion.Euler(0, 90, 0)
end

--@region UIEvent

--@endregion UIEvent

