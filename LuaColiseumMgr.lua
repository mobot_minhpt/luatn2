local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScene=import "L10.Game.CScene"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"

LuaColiseumMgr = {}
LuaColiseumMgr.m_CanGetWeeklyReward = false
LuaColiseumMgr.m_Score = 0 --当前分数
LuaColiseumMgr.m_MaxRankScore = 0 --最大战斗积分
LuaColiseumMgr.m_ArenaPlayScore = 0 --商店积分
LuaColiseumMgr.m_WeeklyBattleTimes = 0 --本周战斗次数
LuaColiseumMgr.m_WeeklyWinTimes  = 0 --本周胜利次数
LuaColiseumMgr.m_WeeklyScoreRewardState = 0 --本周奖励0:不能领取 1:可以领取 2:已领完
LuaColiseumMgr.m_IsMatching = false -- 是否匹配中
LuaColiseumMgr.m_MatchType = 0 --匹配类型 1:1v1 2:2v2 其他同理
LuaColiseumMgr.m_BattleData = {} --matchType => data
LuaColiseumMgr.m_RecordsTipWndData = {} --class => data
LuaColiseumMgr.m_RankWndType = 1 --排行榜匹配类型
LuaColiseumMgr.m_RankData = {}
LuaColiseumMgr.m_RankWndPage = 0
LuaColiseumMgr.m_RankWndMyScore = 0
LuaColiseumMgr.m_RankWndMyEquipScore = 0
LuaColiseumMgr.m_RankWndMyRank = 0
LuaColiseumMgr.m_RankWndMaxPage = 0
LuaColiseumMgr.m_LastGameType = 1
LuaColiseumMgr.m_ScoreBeforeBattle = 0
LuaColiseumMgr.m_ScoreAfterBattle = 0
LuaColiseumMgr.m_ArenaPlayScoreBeforeBattle = 0
LuaColiseumMgr.m_ArenaPlayScoreAfterBattle = 0
LuaColiseumMgr.m_ArenaPlayForce1ResultData = {}
LuaColiseumMgr.m_ArenaPlayForce2ResultData = {}
LuaColiseumMgr.m_ArenaPlayForce1WinType = 0
LuaColiseumMgr.m_ArenaPlayForce2WinType = 0
LuaColiseumMgr.m_ArenaPlayMyResultData = {}
LuaColiseumMgr.m_WeeklyGetShopScore = 0
LuaColiseumMgr.m_WeeklyGetShopScoreLimit = 0
function LuaColiseumMgr:SendArenaInfo(myMaxRankScore, myArenaPlayScore, weeklyBattleTimes, weeklyWinTimes, weeklyScoreRewardState, matching, matchType, weeklyGetShopScore, weeklyGetShopScoreLimit, BattleRecord)
    self.m_MaxRankScore = myMaxRankScore
    self.m_ArenaPlayScore = myArenaPlayScore
    self.m_WeeklyBattleTimes = weeklyBattleTimes
    self.m_WeeklyWinTimes = weeklyWinTimes 
    self.m_WeeklyScoreRewardState = weeklyScoreRewardState
    self.m_IsMatching =  matching 
    self.m_MatchType = matchType 
    self.m_WeeklyGetShopScore = weeklyGetShopScore
    self.m_WeeklyGetShopScoreLimit = weeklyGetShopScoreLimit
    self.m_BattleData = {}
    local list = MsgPackImpl.unpack(BattleRecord)
    for i = 0, list.Count-1, 6 do
        local rankType = list[i] -- 1:1v1, 2:2v2 其他同理
        local myRankScore = list[i+1] -- 我的对战积分
        local myRank = list[i+2] -- 我的排名
        local winTimes = list[i+3] -- 胜场数
        local lostTimes = list[i+4] -- 负场数
        local winCombo = list[i+5] -- 连胜数
        self.m_BattleData[rankType] = {myRankScore = myRankScore,myRank = myRank,winTimes = winTimes, lostTimes = lostTimes, winCombo = winCombo}
    end
    g_ScriptEvent:BroadcastInLua("OnSendArenaInfo")
end

function LuaColiseumMgr:SyncArenaSignupState(matching, matchType)
    -- matching 是否匹配中
    -- mathType 匹配类型 1:1v1, 2:2v2 其他同理 没在匹配中时为0
    self.m_IsMatching = matching
    self.m_MatchType = matchType
    g_ScriptEvent:BroadcastInLua("OnSendArenaInfo")
end

function LuaColiseumMgr:SyncWeeklyArenaScoreRewardState(weeklyBattleTimes, weeklyWinTimes, rewardState, currentShopScore)
    -- weeklyBattleTimes 本周战斗次数
    -- weeklyWinTimes 本周胜利次数
    -- rewardState 0:不能领取 1:可以领取 2:已领完
    -- currentShopScore 当前商店积分
    self.m_WeeklyBattleTimes = weeklyBattleTimes
    self.m_WeeklyWinTimes = weeklyWinTimes 
    self.m_WeeklyScoreRewardState = rewardState
    self.m_ArenaPlayScore = currentShopScore
    g_ScriptEvent:BroadcastInLua("OnSendArenaInfo")
end

function LuaColiseumMgr:SendArenaClassRecord(classRecord)
    self.m_RecordsTipWndData = {}
    local list = MsgPackImpl.unpack(classRecord)
    for i = 0, list.Count-1, 3 do
        local class = list[i] -- 对方职业
        local winTimes = list[i+1] -- 我的胜场数
        local lostTimes = list[i+2] -- 我的负场数
        self.m_RecordsTipWndData[class] = {winTimes = winTimes, lostTimes = lostTimes}
    end
    g_ScriptEvent:BroadcastInLua("OnSendArenaClassRecord")
end

LuaColiseumMgr.m_SeasonId = 0
function LuaColiseumMgr:SendArenaRankInfo(seasonId, rankType, page, totalPage, myScore, myEquipScore, myRank, rankInfo)
    self.m_RankWndPage = page
    self.m_RankWndMaxPage = math.max(1,totalPage)
    self.m_RankWndMyScore = myScore
    self.m_RankWndMyEquipScore = myEquipScore
    self.m_RankWndMyRank = myRank
    self.m_SeasonId = seasonId
    self.m_RankData = {}
    local list = MsgPackImpl.unpack(rankInfo)
    for i = 0, list.Count-1, 9 do
        local rank = list[i]
        local playerId = list[i+1]
        local playerName = list[i+2]
        local playerLevel = list[i+3]
        local playerScore = list[i+4]
        local playerEquipScore = list[i+5]
        local hasFeiSheng = list[i+6]
        local playerClass = list[i+7]
        local serverName = list[i+8] -- 服务器名
        if rank ~= 0 then
            table.insert(self.m_RankData,{serverName = serverName,rank = rank, playerId = playerId, playerName = playerName, playerLevel = playerLevel, playerScore = playerScore,playerEquipScore = playerEquipScore,hasFeiSheng = hasFeiSheng, playerClass = playerClass})
        end
    end
    table.sort(self.m_RankData,function (a, b)
        return a.rank < b.rank
    end)
    g_ScriptEvent:BroadcastInLua("OnSendArenaRankInfo")
end

function LuaColiseumMgr:SendArenaCrossServerRankInfo(seasonId, levelType, myScore, myEquipScore, myRank, rankInfo)
    self.m_SeasonId = seasonId
    self.m_RankWndMyRank = myRank
    self.m_RankWndMyScore = myScore
    self.m_RankWndMyEquipScore = myEquipScore
    self.m_RankData = {}
    local list = MsgPackImpl.unpack(rankInfo)
    for i = 0, list.Count-1, 9 do
        local rank = list[i]
        local playerId = list[i+1]
        local playerName = list[i+2]
        local playerLevel = list[i+3]
        local playerScore = list[i+4]
        local playerEquipScore = list[i+5]
        local hasFeiSheng = list[i+6]
        local playerClass = list[i+7]
        local serverName = list[i+8] -- 服务器名
        if rank ~= 0 then
            table.insert(self.m_RankData,{serverName = serverName,rank = rank, playerId = playerId, playerName = playerName, playerLevel = playerLevel, playerScore = playerScore,playerEquipScore = playerEquipScore,hasFeiSheng = hasFeiSheng, playerClass = playerClass})
        end
    end
    table.sort(self.m_RankData,function (a, b)
        return a.rank < b.rank
    end)
    g_ScriptEvent:BroadcastInLua("OnSendArenaRankInfo")
end

function LuaColiseumMgr:ShowEndGameWnd(lastGameType, shopScoreBeforeBattle, shopScoreAfterBattle, scoreBeforeBattle, scoreAfterBattle)
    self.m_LastGameType = lastGameType
    self.m_ArenaPlayScoreBeforeBattle = shopScoreBeforeBattle
    self.m_ArenaPlayScoreAfterBattle = shopScoreAfterBattle
    self.m_ScoreBeforeBattle = scoreBeforeBattle
    self.m_ScoreAfterBattle = scoreAfterBattle
    CUIManager.ShowUI(CLuaUIResources.ColiseumEndGameWnd)
end

function LuaColiseumMgr:GetProcessValueAndDanSpritesNum(score)
	local setting = Arena_Setting.GetData()

	--段位
	local dan = self:GetDan(score)
	local danInfoData = Arena_DanInfo.GetData(dan)
	local subScoreInterval = score - math.max(danInfoData.Score, setting.SubDanStartRankScore)

	--小段位点数
    local showDanSpritesNum = math.ceil(subScoreInterval / (setting.SubDanInterval))
    if showDanSpritesNum > 0 and showDanSpritesNum < 5 then
        local upValue = math.floor(subScoreInterval / (setting.SubDanInterval))
        if upValue == showDanSpritesNum then
            showDanSpritesNum = showDanSpritesNum + 1
        end
    end
    showDanSpritesNum = math.min(showDanSpritesNum, 5)
	--进度条数值
	local processValue = subScoreInterval - (math.max(showDanSpritesNum - 1,0)) * setting.SubDanInterval
    processValue = math.min(math.max(0,processValue),100)
	return processValue, showDanSpritesNum, dan
end

function LuaColiseumMgr:GetDan(score)
	local dan = 5
    Arena_DanInfo.Foreach(function (key,data)
        if score < data.Score then
            dan = math.min(dan,key - 1)
        end
    end)
	return dan
end

function LuaColiseumMgr:IsInPlay()
    local gamePlayId = 0
    if CScene.MainScene then
        gamePlayId = CScene.MainScene.GamePlayDesignId
    end
    return CommonDefs.HashSetContains(Arena_Setting.GetData().ArenaGameplayId, typeof(UInt32), gamePlayId)
end

function LuaColiseumMgr:ShowTopRightWnd(wnd)
    local isInPlay = self:IsInPlay()
    if isInPlay then
        wnd.contentNode:SetActive(false)
        CUIManager.ShowUI(CUIResources.FightingSpiritDamageWnd)
        CUIManager.ShowUI(CLuaUIResources.CommonKillInfoTopWnd)
    else
        if 51101092 ~= LuaTopAndRightMenuWndMgr.m_GamePlayId then
            CUIManager.CloseUI(CUIResources.FightingSpiritDamageWnd)
        end
    end
end

function LuaColiseumMgr:SendArenaPlayResult(matchType, winType, changeRankScore, newRankScore, addShopScore, newShopScore, winComboTimes, playerData)
    -- winType 0胜利 1失败 2平局
    -- changeRankScore 有正负
    self.m_ArenaPlayForce1ResultData = {}
    self.m_ArenaPlayForce2ResultData = {}
    self.m_ArenaPlayMyResultData = {}
    local list = MsgPackImpl.unpack(playerData)
    for i = 0, list.Count-1, 12 do
        local playerId = list[i]
        local playerName = list[i+1]
        local class = list[i+2]
        local playerWinType = list[i+3] -- 0胜利 1失败 2平局
        local kill = list[i+4]
        local die = list[i+5]
        local ctrl = list[i+6]
        local rmCtrl = list[i+7]
        local relive = list[i+8]
        local damage = list[i+9]
        local heal = list[i+10]
        local force = list[i+11]
        local t = {playerId = playerId,name = playerName,class = class,killNum = kill,dieTimes = die,controllNum = ctrl,
        decontrollNum = rmCtrl,resurgenceTimes = relive,damage = damage,curativeDose = heal,playerWinType = playerWinType}
        table.insert(force == 0 and self.m_ArenaPlayForce1ResultData or self.m_ArenaPlayForce2ResultData, t)
        if force == 0 then
            self.m_ArenaPlayForce1WinType = playerWinType
        else
            self.m_ArenaPlayForce2WinType = playerWinType
        end
        if CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id then
            self.m_ArenaPlayMyResultData = t
        end
       
    end
    self.m_ArenaPlayMyResultData.winningStreakNum = winComboTimes
    LuaColiseumMgr:ShowEndGameWnd(matchType, newShopScore - addShopScore, newShopScore, newRankScore - changeRankScore, newRankScore)
end

function LuaColiseumMgr:SendArenaScoreShopData(shopScore, nextAutoRefreshTime, goodsLimitData, goodsData)
    local limitGoods = goodsLimitData and MsgPackImpl.unpack(goodsLimitData)
    local limitGoodsArray = {}
    for i = 0, limitGoods.Count -1, 2 do
        local goodsId = limitGoods[i]
        local remainCount = limitGoods[i+1]
        table.insert(limitGoodsArray,{goodsId = goodsId,remainCount = remainCount,shopData = Arena_Shop.GetData(goodsId)})
    end

    local goods = goodsData and MsgPackImpl.unpack(goodsData)
    local goodsArray = {}
    for i = 0, goods.Count - 1, 2 do
        local goodsId = goods[i]
        local state = goods[i+1] -- 0可以买 1已购买 2已售罄
        table.insert(goodsArray,{goodsId = goodsId,state = state,shopData = Arena_Shop.GetData(goodsId)})
    end
    g_ScriptEvent:BroadcastInLua("OnSendArenaScoreShopData", shopScore, nextAutoRefreshTime, limitGoodsArray, goodsArray)
end

function LuaColiseumMgr:IsInCurrentSeason()
    local id = LuaArenaMgr:GetCurrentSeasonId()
    local seasonInfo = LuaArenaMgr:GetSeasonInfoClass().GetData(id)
    local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(seasonInfo.StartTime)
    local endTime =  CServerTimeMgr.Inst:GetTimeStampByStr(seasonInfo.EndTime)
    local nowTimeStamp = CServerTimeMgr.Inst.timeStamp
    return nowTimeStamp > startTime and nowTimeStamp < endTime
end

function LuaColiseumMgr:GetCurrentSeasonText()
    local id = LuaArenaMgr:GetCurrentSeasonId()
    local seasonInfo = LuaArenaMgr:GetSeasonInfoClass().GetData(id)
    local startTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst:GetTimeStampByStr(seasonInfo.StartTime))
    local endTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst:GetTimeStampByStr(seasonInfo.EndTime))
    return SafeStringFormat3("%s~%s",startTime:ToString("yyyy.MM.dd"),endTime:ToString("yyyy.MM.dd"))
end

LuaColiseumMgr.m_TeamInfo = {}
function LuaColiseumMgr:SendArenaBattleTeamInfo(teamInfo)
    print("SendArenaBattleTeamInfo")
    local teamInfoList = MsgPackImpl.unpack(teamInfo)
    self.m_TeamInfo = {}
    for i = 0, teamInfoList.Count - 1, 8 do
        local force = teamInfoList[i]
        local playerId = teamInfoList[i+1]
        local playerName = teamInfoList[i+2]
        local level = teamInfoList[i+3]
        local gender = teamInfoList[i+4]
        local class = teamInfoList[i+5]
        local hasFeiSheng = teamInfoList[i+6]
        local isLeader = teamInfoList[i+7]
        print("SendArenaBattleTeamInfo", force, playerId, playerName, level, gender, class, hasFeiSheng, isLeader)
        if not self.m_TeamInfo[force] then
            self.m_TeamInfo[force] = {}
        end
        table.insert(self.m_TeamInfo[force],{force = force, playerId = playerId, playerName = playerName, level = level, gender = gender, class = class, hasFeiSheng = hasFeiSheng, isLeader = isLeader})
    end
    CUIManager.ShowUI(CLuaUIResources.ColiseumShowTeamWnd)
end

function LuaColiseumMgr:GetLevelName()
    local stage = CBiWuDaHuiMgr.Inst:GetMyStage()
    local nameArr = {
        LocalString.GetString("新锐组"),
        LocalString.GetString("英武组"),
        LocalString.GetString("神勇组"),
        LocalString.GetString("天罡组"),
        LocalString.GetString("天元组")
    }
    stage = stage == 0 and 1 or stage
    return nameArr[stage]
end