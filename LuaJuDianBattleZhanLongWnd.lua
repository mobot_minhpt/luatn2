local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"
local Animation = import "UnityEngine.Animation"

LuaJuDianBattleZhanLongWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleZhanLongWnd, "TaskCountLabel", UILabel)
RegistChildComponent(LuaJuDianBattleZhanLongWnd, "TipButton", GameObject)
RegistChildComponent(LuaJuDianBattleZhanLongWnd, "TaskTable", UIGrid)
RegistChildComponent(LuaJuDianBattleZhanLongWnd, "TaskTemplate", GameObject)

RegistClassMember(LuaJuDianBattleZhanLongWnd, "m_ZhanLongTimesLimitPerDay")
RegistClassMember(LuaJuDianBattleZhanLongWnd, "m_ZhanLongDisplayCount")
RegistClassMember(LuaJuDianBattleZhanLongWnd, "m_TaskShowStatusList")
RegistClassMember(LuaJuDianBattleZhanLongWnd, "m_TaskObjList")
--@endregion RegistChildComponent end

function LuaJuDianBattleZhanLongWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)
end

function LuaJuDianBattleZhanLongWnd:Init()
    self.TaskTemplate.gameObject:SetActive(false)
    local setData = GuildOccupationWar_DailySetting.GetData()
    self.m_ZhanLongTimesLimitPerDay = setData.ZhanLongTimesLimitPerDay
    self.m_ZhanLongDisplayCount = setData.ZhanLongDisplayCount
    self.TaskCountLabel.text = "0/0"
    self:InitTaskTable()
    Gac2Gas.GuildJuDianQueryZhanLongTaskInfo()
end

function LuaJuDianBattleZhanLongWnd:InitTaskTable()
    self.m_TaskObjList = {}
    self.m_TaskShowStatusList = {}
    Extensions.RemoveAllChildren(self.TaskTable.transform)
    for i = 1, self.m_ZhanLongDisplayCount do
        local obj = NGUITools.AddChild(self.TaskTable.gameObject, self.TaskTemplate.gameObject)
        obj:SetActive(true)
        obj.transform:Find("TheBack").gameObject:SetActive(true)
        obj.transform:Find("TheFront").gameObject:SetActive(false)
        self.m_TaskShowStatusList[i] = true
        self.m_TaskObjList[i] = obj
    end
    self.TaskTable:Reposition()
end

function LuaJuDianBattleZhanLongWnd:UpdateTaskTable(myTaskData, fullTask, hasTask)
    for i = 1, self.m_ZhanLongDisplayCount do
        local obj = self.m_TaskObjList[i]
        local backView = obj.transform:Find("TheBack")
        local frontView = obj.transform:Find("TheFront")
        local anim = obj:GetComponent(typeof(Animation))
        obj:SetActive(true)
        if myTaskData[i] then
            if not self.m_TaskShowStatusList[i] then
                self.m_TaskShowStatusList[i] = true
                anim:Play("guildterritorialwarszhanlongwnd_qiehuan")
            end
            backView.gameObject:SetActive(false)
            frontView.gameObject:SetActive(true)
            local taskData = GuildOccupationWar_ZhanLong.GetData(myTaskData[i].taskId)
            frontView:Find("TaskNameLabel"):GetComponent(typeof(UILabel)).text = taskData.Name
            frontView:Find("xinxi/TaskInfo/TaskDescriptionLabel"):GetComponent(typeof(UILabel)).text = taskData.Description
            frontView:Find("xinxi/TaskInfo/FinishedLabel").gameObject:SetActive(myTaskData[i].status == 1)
            frontView:Find("xinxi/TaskInfo/OngoingLabel").gameObject:SetActive(myTaskData[i].status == 0)
            self:InitTaskReward(frontView, taskData.Rewards)
            UIEventListener.Get(frontView.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnTaskFrontClick(taskData.ActivityId)
            end)
        else
            self.m_TaskShowStatusList[i] = false
            backView.gameObject:SetActive(true)
            frontView.gameObject:SetActive(false)
            if fullTask then backView:GetComponent(typeof(UIPanel)).alpha = 0.3 end
            UIEventListener.Get(backView.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnTaskBackClick(i, fullTask, hasTask)
            end)
        end
    end
end

function LuaJuDianBattleZhanLongWnd:InitTaskReward(frontView, rewardList)
    local itemTable = frontView:Find("xinxi/ItemTable"):GetComponent(typeof(UITable))
    local itemTemplate = frontView:Find("xinxi/ItemTemplate").gameObject
    itemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(itemTable.transform)
    for i = 0, rewardList.Length-1, 2 do
        local go = NGUITools.AddChild(itemTable.gameObject, itemTemplate)
        go:GetComponent(typeof(CQnReturnAwardTemplate)):Init(CItemMgr.Inst:GetItemTemplate(rewardList[i]), rewardList[i+1])
        go:SetActive(true)
    end
    itemTable:Reposition()
end

function LuaJuDianBattleZhanLongWnd:OnSendZhanLongTaskInfo(data)
    -- 今日剩余完成任务次数
    local todayCount = self.m_ZhanLongTimesLimitPerDay - data[1]
    local countColor =  todayCount == 0 and "[FF0000]" or "[FFFFFF]"
    local countStr = SafeStringFormat3("%s%d[FFFFFF]/%d", countColor, todayCount, self.m_ZhanLongTimesLimitPerDay)
    self.TaskCountLabel.text = countStr
    -- 任务数据整理
    local myTaskData = {}
    local hasTask = false
	for i=2, #data, 3 do  -- 玩家已接的任务，3个一组，分别是序号，taskId，任务状态 0已接，1已完成
        local taskInfo = {}
        local index = data[i]
        taskInfo.taskId = data[i+1]
        taskInfo.status = data[i+2]
        if taskInfo.status == 0 then hasTask =  true end
        myTaskData[index] = taskInfo
	end
    self:UpdateTaskTable(myTaskData, todayCount == 0, hasTask)
end

--@region UIEvent
function LuaJuDianBattleZhanLongWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("GUILD_JUDIAN_ZHANLONG_INTERFACE_TIPS")
end

function LuaJuDianBattleZhanLongWnd:OnTaskFrontClick(scheduleId)
    CLuaScheduleMgr:ShowScheduleInfo(scheduleId)
end

function LuaJuDianBattleZhanLongWnd:OnTaskBackClick(index, fullTask, hasTask)
    if fullTask then
        g_MessageMgr:ShowMessage("GUILD_JUDIAN_ZHANLONG_FULL_TASK")
    elseif hasTask then
        g_MessageMgr:ShowMessage("GUILD_JUDIAN_ZHANLONG_HAS_TASK")
    else
        Gac2Gas.GuildJuDianAcceptZhanLongTask(index, false)
    end
end

--@endregion UIEvent
function LuaJuDianBattleZhanLongWnd:OnEnable()
	g_ScriptEvent:AddListener("GuildJuDianSendZhanLongTaskInfo", self, "OnSendZhanLongTaskInfo")
end

function LuaJuDianBattleZhanLongWnd:OnDisable()
	g_ScriptEvent:RemoveListener("GuildJuDianSendZhanLongTaskInfo", self, "OnSendZhanLongTaskInfo")
end