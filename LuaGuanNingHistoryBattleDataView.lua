require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
-- local EnumGuanNingWeekDataKey=import "L10.Game.EnumGuanNingWeekDataKey"
local CSwitchMgr=import "L10.Engine.CSwitchMgr"
--历史数据
CLuaGuanNingHistoryBattleDataView=class()
RegistClassMember(CLuaGuanNingHistoryBattleDataView,"m_Grid")
RegistClassMember(CLuaGuanNingHistoryBattleDataView,"m_TabView")

RegistClassMember(CLuaGuanNingHistoryBattleDataView,"m_ScoreLableTable")
RegistClassMember(CLuaGuanNingHistoryBattleDataView,"m_ScoreLableTable1")
RegistClassMember(CLuaGuanNingHistoryBattleDataView,"m_ScoreLableTable2")
RegistClassMember(CLuaGuanNingHistoryBattleDataView,"m_DescLableTable")


function CLuaGuanNingHistoryBattleDataView:Init(tf)
    if not tf then return end
    local g = LuaGameObject.GetChildNoGC(tf,"ShareButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUICommonDef.CaptureScreenAndShare()
    end)
        
    --渠道服 隐藏
    if CLuaGuanNingMgr.IsOnlyShare2PersonalSpace() then
        LuaGameObject.GetChildNoGC(tf,"ShareButton").gameObject:SetActive(false)
    end

    if not CSwitchMgr.EnableGuanNingShare then
        LuaGameObject.GetChildNoGC(tf,"ShareButton").gameObject:SetActive(false)
    end

    self.m_TabView=LuaGameObject.GetChildNoGC(tf,"TabView").qnTabView
    self.m_TabView.OnSelect=DelegateFactory.Action_QnTabButton_int(function(btn,index) self:UpdateUI() end)
    self.m_ScoreLableTable={}
    self.m_DescLableTable = {}
    local scoreLabelsTf=LuaGameObject.GetChildNoGC(tf,"ScoreLabels").transform
    for i=1,9 do
        local child = scoreLabelsTf:Find(tostring(i))
        self.m_ScoreLableTable[i]=child:GetComponent(typeof(UILabel))
        self.m_ScoreLableTable[i].transform:Find("UpSprite").gameObject:SetActive(false)
        self.m_DescLableTable[i]=child:Find("Label"):GetComponent(typeof(UILabel))
    end
    self.m_ScoreLableTable1={}
    scoreLabelsTf=LuaGameObject.GetChildNoGC(tf,"ScoreLabels1").transform
    for i=1,2 do
        self.m_ScoreLableTable1[i]=LuaGameObject.GetChildNoGC(scoreLabelsTf,tostring(i)).label
    end
    self.m_ScoreLableTable2={}
    scoreLabelsTf=LuaGameObject.GetChildNoGC(tf,"ScoreLabels2").transform
    for i=1,5 do
        self.m_ScoreLableTable2[i]=scoreLabelsTf:Find(tostring(i)):GetComponent(typeof(UILabel))
    end

    self:InitTime(tf)

    if CLuaGuanNingMgr.historyMonthData then 
        self:UpdateUI()
    end
end
function CLuaGuanNingHistoryBattleDataView:InitTime(tf)
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"TimeLabel").label
    local time = CServerTimeMgr.Inst:GetZone8Time()
    time=CUICommonDef.GetMondayInThisWeek(time)
    if timeLabel then timeLabel.text=SafeStringFormat3( LocalString.GetString("自%s/%02d/%02d起"),time.Year,time.Month,time.Day ) end

    if not CLuaGuanNingMgr.lastBattleData then return end
    local lastBattleData=CLuaGuanNingMgr.lastBattleData
    time = CServerTimeMgr.ConvertTimeStampToZone8Time(lastBattleData.timeStamp)
    time=CUICommonDef.GetMondayInThisWeek(time)
    if timeLabel then timeLabel.text=SafeStringFormat3( LocalString.GetString("自%s/%02d/%02d起"),time.Year,time.Month,time.Day ) end
end

function CLuaGuanNingHistoryBattleDataView:UpdateUI()
    local historyMonthData=CLuaGuanNingMgr.historyMonthData
    -- print(tostring(historyMonthData))
    if not historyMonthData then return end
    local enumGuanNingWeekDataKeyForScoreLableList = {}
    if self.m_TabView.CurrentSelectTab==0 then--最佳
        --得分
        self:InitCommonLabel(self.m_ScoreLableTable1[1], EnumGuanNingWeekDataKey.eMaxPlayScore)
        --连斩
        self:InitCommonLabel(self.m_ScoreLableTable1[2], EnumGuanNingWeekDataKey.eMaxPlayLianShaNum)
        self.m_DescLableTable[1].text=LocalString.GetString("最高击杀")
        self.m_DescLableTable[2].text=LocalString.GetString("最多阵亡")
        self.m_DescLableTable[3].text=LocalString.GetString("最高助攻")
        self.m_DescLableTable[4].text=LocalString.GetString("最高复活")
        self.m_DescLableTable[5].text=LocalString.GetString("最多获赞")
        enumGuanNingWeekDataKeyForScoreLableList = {
            EnumGuanNingWeekDataKey.eMaxPlayKillNum,EnumGuanNingWeekDataKey.eMaxPlayDieNum,EnumGuanNingWeekDataKey.eMaxPlayHelpNum,
            EnumGuanNingWeekDataKey.eMaxPlayReliveNum,EnumGuanNingWeekDataKey.eMaxGetDianZanNum,
            EnumGuanNingWeekDataKey.eMaxPlayOccupyHighLandNum,EnumGuanNingWeekDataKey.eMaxPlayOccupyForestNum,
            EnumGuanNingWeekDataKey.eMaxPlayOccupyCaveNum,EnumGuanNingWeekDataKey.eMaxPlaySubmitFlagNum
        }
    elseif self.m_TabView.CurrentSelectTab==1 then
        --总得分
        local a = historyMonthData[EnumGuanNingWeekDataKey.eTotalScore]
        self:InitCommonLabel(self.m_ScoreLableTable2[3], EnumGuanNingWeekDataKey.eTotalScore)
        --场均分
        local c = historyMonthData[EnumGuanNingWeekDataKey.eJoinNum]
        if c==0 then
            self.m_ScoreLableTable2[1].text="0"
        else
            self.m_ScoreLableTable2[1].text=tostring(math.floor(a/c))
        end
        self.m_ScoreLableTable2[1].transform:Find("UpSprite").gameObject:SetActive(false)
        --胜场数
        self:InitCommonLabel(self.m_ScoreLableTable2[2], EnumGuanNingWeekDataKey.eVictoryNum)
        --当前连胜
        self:InitCommonLabel(self.m_ScoreLableTable2[4], EnumGuanNingWeekDataKey.eLianShengNum)
        --点赞次数
        self:InitCommonLabel(self.m_ScoreLableTable2[5], EnumGuanNingWeekDataKey.eTotalSendDianZanNum)

        self.m_DescLableTable[1].text=LocalString.GetString("累计击杀")
        self.m_DescLableTable[2].text=LocalString.GetString("累计阵亡")
        self.m_DescLableTable[3].text=LocalString.GetString("累计助攻")
        self.m_DescLableTable[4].text=LocalString.GetString("累计复活")
        self.m_DescLableTable[5].text=LocalString.GetString("累计获赞")
        enumGuanNingWeekDataKeyForScoreLableList = {
            EnumGuanNingWeekDataKey.eTotalKillNum,EnumGuanNingWeekDataKey.eTotalDieNum,EnumGuanNingWeekDataKey.eTotalHelpNum,
            EnumGuanNingWeekDataKey.eTotalReliveNum,EnumGuanNingWeekDataKey.eTotalGetDianZanNum,
            EnumGuanNingWeekDataKey.eTotalOccupyHighLandNum,EnumGuanNingWeekDataKey.eTotalOccupyForestNum,
            EnumGuanNingWeekDataKey.eTotalOccupyCaveNum,EnumGuanNingWeekDataKey.eTotalSubmitFlagNum
        }
    end

    for i,k in pairs(enumGuanNingWeekDataKeyForScoreLableList) do
        if k then
            self:InitCommonLabel(self.m_ScoreLableTable[i], k)
        end
    end
end
function CLuaGuanNingHistoryBattleDataView:InitCommonLabel(label, k)
    if k then
        label.text = CLuaGuanNingMgr.historyMonthData[k] or 0
        label.transform:Find("UpSprite").gameObject:SetActive(CLuaGuanNingMgr.m_BreakRecords.weekData and CLuaGuanNingMgr.m_BreakRecords.weekData[k])
    end
end
function CLuaGuanNingHistoryBattleDataView:InitData()
    local historyMonthData=CLuaGuanNingMgr.historyMonthData
    if not historyMonthData then return end

    self:UpdateUI()
end
