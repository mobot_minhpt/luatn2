-- Auto Generated!!
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMapiDetailInfoWnd = import "L10.UI.CMapiDetailInfoWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local String = import "System.String"
local UInt32 = import "System.UInt32"
CMapiDetailInfoWnd.m_InitNull_CS2LuaHook = function (this) 
    this.emotionSprite.gameObject:SetActive(false)
    this.wordLabel.gameObject:SetActive(false)
    this.wordBgSprite.gameObject:SetActive(false)
    this.qinmiduLabel.gameObject:SetActive(false)
    this.nameLabel.gameObject:SetActive(false)
    this.lvLabel.gameObject:SetActive(false)
    this.fangmuzhongLabel.gameObject:SetActive(false)
end
CMapiDetailInfoWnd.m_Init_String_Int32_String_Double_String_UInt32_Boolean_CS2LuaHook = function (this, zuoqiId, emotion, word, qinmidu, name, level, bFangmuzhong) 
    this.emotionSprite.gameObject:SetActive(true)
    this.wordLabel.gameObject:SetActive(true)
    this.wordBgSprite.gameObject:SetActive(true)
    this.qinmiduLabel.gameObject:SetActive(true)
    this.nameLabel.gameObject:SetActive(true)
    this.lvLabel.gameObject:SetActive(true)


    this.zqid = zuoqiId
    if emotion <= EnumMapiQingxu_lua.eHappy and emotion >= EnumMapiQingxu_lua.eAngry then
        this.emotionSprite.spriteName = CMapiDetailInfoWnd.EmotionSpriteName[emotion]
    end
    this.wordLabel.text = word
    this.qinmiduLabel.text = System.String.Format(LocalString.GetString("亲密度: {0}"), math.floor(qinmidu))
    this.nameLabel.text = name
    this.lvLabel.text = System.String.Format("Lv.{0}", level)
    this.fangmuzhongLabel.gameObject:SetActive(bFangmuzhong)

    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(zuoqiId)
    if zqdata ~= nil and zqdata.mapiInfo ~= nil then
        this.loader:Init(zqdata.mapiInfo, nil, nil, -135, nil)
    else
        Gac2Gas.QueryMapiDetail(this.zqid)
    end
end
CMapiDetailInfoWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListener(EnumEventType.OnMapiFangmuUpdate, MakeDelegateFromCSFunction(this.OnMapiFangmuUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.OnUpdateMapiQingxu, MakeDelegateFromCSFunction(this.OnUpdateMapiQingxu, MakeGenericClass(Action3, String, UInt32, UInt32), this))
end
CMapiDetailInfoWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.OnMapiFangmuUpdate, MakeDelegateFromCSFunction(this.OnMapiFangmuUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnSyncMapiInfo, MakeDelegateFromCSFunction(this.OnSyncMapiInfo, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnUpdateMapiQingxu, MakeDelegateFromCSFunction(this.OnUpdateMapiQingxu, MakeGenericClass(Action3, String, UInt32, UInt32), this))
end
CMapiDetailInfoWnd.m_OnUpdateMapiQingxu_CS2LuaHook = function (this, zuoqiId, engineId, qingxuzhi) 
    if zuoqiId == this.zqid then
        local emotion = qingxuzhi
        if emotion <= EnumMapiQingxu_lua.eHappy and emotion >= EnumMapiQingxu_lua.eAngry then
            this.emotionSprite.gameObject:SetActive(true)
            this.emotionSprite.spriteName = CMapiDetailInfoWnd.EmotionSpriteName[emotion]
        end
    end
end
CMapiDetailInfoWnd.m_OnSyncMapiInfo_CS2LuaHook = function (this, zuoqiId) 
    if zuoqiId ~= this.zqid then
        return
    end
    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(this.zqid)
    if zqdata ~= nil and zqdata.mapiInfo ~= nil then
        this.qinmiduLabel.text = System.String.Format(LocalString.GetString("亲密度: {0}"), math.floor(zqdata.mapiInfo.Qinmidu))
        this.lvLabel.text = System.String.Format("Lv.{0}", zqdata.mapiInfo.Level)
        this.nameLabel.text = zqdata.mapiInfo.Name

        this.loader:Init(zqdata.mapiInfo, nil, nil, -135, nil)
    end
end
CMapiDetailInfoWnd.m_OnMapiFangmuUpdate_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil or this.zqid == nil then
        return
    end

    local furnitureInfo = CClientHouseMgr.Inst.mFurnitureProp
    if furnitureInfo == nil then
        return
    end

    local bUnderFangmu = false
    CommonDefs.DictIterate(furnitureInfo.MapiAppearanceInfo, DelegateFactory.Action_object_object(function (___key, ___value) 
        local v = {}
        v.Key = ___key
        v.Value = ___value
        if v.Value.ZuoqiId == this.zqid then
            bUnderFangmu = true
        end
    end))

    this.fangmuzhongLabel.gameObject:SetActive(bUnderFangmu)
end
