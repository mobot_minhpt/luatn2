-- Auto Generated!!
local CGuildEnemyMgr = import "L10.Game.CGuildEnemyMgr"
local CGuildSetEnemyConfirmWnd = import "L10.UI.CGuildSetEnemyConfirmWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildSetEnemyConfirmWnd.m_Init_CS2LuaHook = function (this) 
    this.descLabel.text = g_MessageMgr:FormatMessage("SET_ENEMY_GUILD_CONFIRM")
    this.ownSilverLabel.text = tostring(CGuildEnemyMgr.Inst.OwnGuildSilver)
    this.costSilverLabel.text = tostring(CGuildEnemyMgr.Inst.CostGuildSilver)
end
CGuildSetEnemyConfirmWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.okBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okBtn).onClick, MakeDelegateFromCSFunction(this.OnOKButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.cancelBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelBtn).onClick, MakeDelegateFromCSFunction(this.OnCancelButtonClick, VoidDelegate, this), true)
end
