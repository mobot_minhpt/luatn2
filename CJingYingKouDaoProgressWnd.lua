-- Auto Generated!!
local CJingYingKouDaoProgressWnd = import "L10.UI.CJingYingKouDaoProgressWnd"
local CKouDaoMgr = import "L10.Game.CKouDaoMgr"
local Vector3 = import "UnityEngine.Vector3"
CJingYingKouDaoProgressWnd.m_Init_CS2LuaHook = function (this) 
    this.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
    this.TotalTime = CKouDaoMgr.Inst.JingYingBossKillTime
    CKouDaoMgr.Inst.JingYingBossKillTime = 0
    this:showCountDown()
    this:OnKouDaoKillCountUpdate()
end
