-- Auto Generated!!
local CTaskSubmitItem = import "L10.UI.CTaskSubmitItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumQualityType = import "L10.Game.EnumQualityType"
CTaskSubmitItem.m_Init_CS2LuaHook = function (this, item, count, place) 
    this.iconTexture.mainTexture = nil
    this.iconTexture.material = nil
    this.amountLabel.text = nil
    if System.String.IsNullOrEmpty(item.Id) then
        return
    end
    this.ItemId = item.Id
    this.iconTexture:LoadMaterial(item.Icon)
    this.amountLabel.text = tostring(count)
    this.Place = place
    this.disableSprite.enabled = not item.MainPlayerIsFit

    if item.IsItem then
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    else
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
    end
end
