local DelegateFactory = import "DelegateFactory"
local CBaZiMgr        = import "L10.Game.CBaZiMgr"
local CBaziCoupleInfo = import "L10.UI.CBaziCoupleInfo"

LuaWeddingBaZiWnd = class()

--@region RegistChildComponent: Dont Modify Manually
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingBaZiWnd, "commentTable")
RegistClassMember(LuaWeddingBaZiWnd, "commentLabel")
RegistClassMember(LuaWeddingBaZiWnd, "commentLabel2")
RegistClassMember(LuaWeddingBaZiWnd, "assessLabel")
RegistClassMember(LuaWeddingBaZiWnd, "maleInfo")
RegistClassMember(LuaWeddingBaZiWnd, "femaleInfo")
RegistClassMember(LuaWeddingBaZiWnd, "moreButton")

RegistClassMember(LuaWeddingBaZiWnd, "result")
RegistClassMember(LuaWeddingBaZiWnd, "panCi")

function LuaWeddingBaZiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWeddingBaZiWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.commentTable = anchor:Find("Result"):Find("DescTable"):GetComponent(typeof(UITable))
    self.commentLabel = self.commentTable.transform:Find("Desc"):GetComponent(typeof(UILabel))
    self.commentLabel2 = self.commentTable.transform:Find("Desc2"):GetComponent(typeof(UILabel))
    self.assessLabel = anchor:Find("Result"):Find("Level"):Find("Label"):GetComponent(typeof(UILabel))
    self.maleInfo = anchor:Find("CoupleInfo"):Find("Male"):GetComponent(typeof(CBaziCoupleInfo))
    self.femaleInfo = anchor:Find("CoupleInfo"):Find("Female"):GetComponent(typeof(CBaziCoupleInfo))
    self.moreButton = anchor:Find("MoreButton").gameObject
end

-- 初始化按键响应
function LuaWeddingBaZiWnd:InitEventListener()
	UIEventListener.Get(self.moreButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMoreButtonClick()
	end)
end


function LuaWeddingBaZiWnd:Init()
    self.maleInfo:Init()
    self.femaleInfo:Init()
    self:MatchBaZiResult()
    self:InitMoreInfo()
end

function LuaWeddingBaZiWnd:MatchBaZiResult()
    local matchResult = CBaZiMgr.Inst:MatchBaziByYMDH(self.maleInfo.BirthYear, self.maleInfo.BirthMonth, self.maleInfo.BirthDay, self.maleInfo.BirthHour,
        self.femaleInfo.BirthYear, self.femaleInfo.BirthMonth, self.femaleInfo.BirthDay, self.femaleInfo.BirthHour)
    if not matchResult then return end

    self.result = BaZi_Result.GetData(matchResult.id)
    if not self.result then return end

    self.assessLabel.text = self.result.Assess
    self.commentLabel.text = self.result.Comment1
    self.commentLabel2.text = self.result.Comment2
    self.commentLabel2.gameObject:SetActive(not System.String.IsNullOrEmpty(self.result.Comment2))
    self.commentTable:Reposition()
end

function LuaWeddingBaZiWnd:InitMoreInfo()
    local str = SafeStringFormat3(LocalString.GetString("男方八字%s，女方八字%s\n"), self.maleInfo.BaziQiangRuo, self.femaleInfo.BaziQiangRuo)
	str = str .. SafeStringFormat3(LocalString.GetString("全局观之，此两人乃%s等姻缘。判词云：%s%s\n解曰：%s"),
        self.result.Assess, self.result.Comment1, self.result.Comment2, self.result.Explain)
    LuaWeddingIterationMgr.baZiStr = str
end

--@region UIEvent

function LuaWeddingBaZiWnd:OnMoreButtonClick()
    if not self.result then return end
    CUIManager.ShowUI(CLuaUIResources.WeddingBaZiExtraWnd)
end

--@endregion UIEvent

