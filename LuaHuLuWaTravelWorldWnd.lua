local UILabel = import "UILabel"

local CUIFx = import "L10.UI.CUIFx"

local CHuLuWaPathData = import "CHuLuWaPathData"

local UITexture = import "UITexture"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Camera = import "UnityEngine.Camera"
local RenderTexture = import "UnityEngine.RenderTexture"
local CShakeDice = import "L10.UI.CShakeDice"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local LuaTweenUtils = import "LuaTweenUtils"
local LayerDefine = import "L10.Engine.LayerDefine"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local CUITexture = import "L10.UI.CUITexture"
local Color = import "UnityEngine.Color"
local QnButton = import "L10.UI.QnButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaHuLuWaTravelWorldWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "ShakeDice", "ShakeDice", GameObject)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "Camera", "Camera", Camera)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "TouziModel", "TouziModel", GameObject)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "TouZiBtn", "TouZiBtn", UITexture)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "UIFx", "UIFx", CUIFx)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "PlayerIcon", "PlayerIcon", CUITexture)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "PlayerNode", "PlayerNode", GameObject)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "TouZiFx", "TouZiFx", CUIFx)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "TouZi", "TouZi", GameObject)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "PlayerFx", "PlayerFx", CUIFx)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "BuyTouZiLabel", "BuyTouZiLabel", UILabel)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "ResetTouZiLabel", "ResetTouZiLabel", UILabel)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "TouZiCountLabel", "TouZiCountLabel", UILabel)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaHuLuWaTravelWorldWnd, "HuLuWaName", "HuLuWaName", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHuLuWaTravelWorldWnd,"m_StartIdx")
RegistClassMember(LuaHuLuWaTravelWorldWnd,"m_EndIdx")
RegistClassMember(LuaHuLuWaTravelWorldWnd,"m_CurIdx")
RegistClassMember(LuaHuLuWaTravelWorldWnd,"m_IsTraveling")
RegistClassMember(LuaHuLuWaTravelWorldWnd,"m_HighLightFx")
RegistClassMember(LuaHuLuWaTravelWorldWnd,"m_TouZiFxTime")

function LuaHuLuWaTravelWorldWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.PlayerIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPlayerNodeClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
    UIEventListener.Get(self.TouZiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTouZiBtnClick()
	end)
    self.m_HighLightFx = {}
    self.m_TouZiFxTime = 1
end

function LuaHuLuWaTravelWorldWnd:Init()
    self.TouziModel.layer = LayerDefine.ModelForNGUI_3D 
    local rt = RenderTexture(512, 512, 24, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default)
    self.Camera.targetTexture = rt
    self.m_ShakeDiceAni = self.Camera:GetComponent(typeof(CShakeDice))
    self.TouZiBtn.mainTexture = rt

    --Init
    self.UIFx:LoadFx("fx/ui/prefab/UI_sanjie_saoguang.prefab")
    self:InitCurPosition()
    self:RevertFxPosition()
    self:InitMapNodes()
    self:InitPlayerIcon()
    self:InitTouziCountAndReset()

    --guide
    CLuaGuideMgr.TryHuLuWaAdventureGuide()
end

function LuaHuLuWaTravelWorldWnd:InitTouziCountAndReset()
    --到过某地，但没有进入副本
    local curPos = LuaHuLuWa2022Mgr.m_CurTravelPosId
    if not LuaHuLuWa2022Mgr.m_IsVisited and LuaHuLuWa2022Mgr.m_VisitedPosDict[curPos] then
        CUICommonDef.SetActive(self.TouZiBtn.gameObject,false,true)
        self.TouZiBtn.transform:GetComponent(typeof(UITexture)).color = Color.gray
        LuaHuLuWa2022Mgr.OpenTravelEventTip(curPos)
    else
        CUICommonDef.SetActive(self.TouZiBtn.gameObject,true,true)
        self.TouZiBtn.transform:GetComponent(typeof(UITexture)).color = Color.white
    end

    self.TouZiCountLabel.text = SafeStringFormat3("x%d",LuaHuLuWa2022Mgr.m_LeftTravelTimes)
    if LuaHuLuWa2022Mgr.m_LeftTravelTimes > 0 then
        --显示重置时间
        self.ResetTouZiLabel.gameObject:SetActive(true)
        self.BuyTouZiLabel.gameObject:SetActive(false)
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local resetTime = CreateFromClass(DateTime, now.Year, now.Month, now.Day, 23, 59, 59)
        local hour = now.Hour
        
        local temp = CommonDefs.op_Subtraction_DateTime_DateTime(resetTime,now)
        local lastHour = temp.Hours
        local lastMins = temp.Minutes
        local lastSeconds = temp.Seconds
        if lastHour == 0 then
            self.ResetTouZiLabel.text = SafeStringFormat3(LocalString.GetString("%d分钟后重置"),lastMins+1)
        else
            self.ResetTouZiLabel.text = SafeStringFormat3(LocalString.GetString("%d小时后重置"),lastHour+1)
        end
    else
        --显示跳转购买
        self.ResetTouZiLabel.gameObject:SetActive(false)
        self.BuyTouZiLabel.gameObject:SetActive(true)
    end
end
function LuaHuLuWaTravelWorldWnd:InitMapNodes()
    for i=1,27,1 do
        local item = self.transform:Find(SafeStringFormat3("MapView/ActivityNode%d",i))
        local stateIcon = item:Find("TravelState")
        local boxTexture = item:Find("BoxTexture")
        if stateIcon then
            stateIcon = stateIcon:GetComponent(typeof(CUITexture))
            local data = HuluBrothers_Adventure.GetData(i)
            local iconName = "wenhao"
            --是否到访过
            if LuaHuLuWa2022Mgr.m_VisitedPosDict[i] then
                iconName = data.Type
            end
            local iconPath = SafeStringFormat3("UI/Texture/Transparent/Material/huluwatravelworldwnd_bg_%s.mat",iconName)
            stateIcon:LoadMaterial(iconPath)
        end
        if boxTexture then
            boxTexture = boxTexture:GetComponent(typeof(CUITexture))
            local iconName = "baoxiang_close"
            if LuaHuLuWa2022Mgr.m_VisitedPosDict[i] then
                iconName = "baoxiang_open"
            end
            local iconPath = SafeStringFormat3("UI/Texture/Transparent/Material/huluwatravelworldwnd_bg_%s.mat",iconName)
            boxTexture:LoadMaterial(iconPath)
        end
    end
end

function LuaHuLuWaTravelWorldWnd:InitPlayerIcon()
    local myHuluwaIdx = LuaHuLuWa2022Mgr.m_TravelingHuluwaId

    local data = HuluBrothers_Transform.GetData(myHuluwaIdx)
    local portrait = data.Portrait
    portrait = SafeStringFormat3("UI/Texture/Portrait/Material/%s.mat",portrait)

    self.PlayerIcon:LoadMaterial(portrait)
    self.CountDownLabel.text = nil
    local monsterId = data.MonsterID
    local monster = Monster_Monster.GetData(monsterId)
    self.HuLuWaName.text = monster.Name
end
function LuaHuLuWaTravelWorldWnd:InitPlayerCountDown(val)
    self.PlayerIcon:LoadMaterial(nil)
    self.CountDownLabel.text = val
end

function LuaHuLuWaTravelWorldWnd:RevertFxPosition()
    if self.m_HighLightFx and next(self.m_HighLightFx) then
        for _,fx in ipairs(self.m_HighLightFx) do 
            fx:DestroyFx()
            LuaTweenUtils.DOKill(fx.transform, false)
        end
    end

    for i=1,27,1 do 
        local pathData = self.transform:Find(SafeStringFormat3("PathTexture/Path%d",i)):GetComponent(typeof(CHuLuWaPathData))
        local fx = pathData.transform:Find("Fx"):GetComponent(typeof(CUIFx))
        fx:DestroyFx()
        fx.transform.localPosition = pathData.mPathPointList[0]
    end
end

function LuaHuLuWaTravelWorldWnd:InitCurPosition()
    local idx = LuaHuLuWa2022Mgr.m_CurTravelPosId
    self.m_StartIdx = idx
    self.m_CurIdx = idx

    local str = SafeStringFormat3("PathTexture/Path%d",idx)
    local path = self.transform:Find(str)
    if path then
        path = path:GetComponent(typeof(CHuLuWaPathData))
        local firstPos = path.mPathPointList[0]
        self.PlayerNode.transform.localPosition = firstPos
    end
end

function LuaHuLuWaTravelWorldWnd:OnEnable()
    --SyncHuluwaAdventureWndInfo
    g_ScriptEvent:AddListener("SyncHuluwaAdventureWndInfo", self, "OnSyncHuluwaAdventureWndInfo")
    g_ScriptEvent:AddListener("SyncRollHuluwaAdventure", self, "OnSyncRollHuluwaAdventure")
    g_ScriptEvent:AddListener("CloseTravelEventPopWnd", self, "OnCloseTravelEventPopWnd")
    g_ScriptEvent:AddListener("InitHuluwaAdventureTransform", self, "OnInitHuluwaAdventureTransform")
    g_ScriptEvent:AddListener("SyncHuluwaAdventureVisitStatusChange", self, "OnSyncHuluwaAdventureVisitStatusChange")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
end

function LuaHuLuWaTravelWorldWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHuluwaAdventureWndInfo", self, "OnSyncHuluwaAdventureWndInfo")
    g_ScriptEvent:RemoveListener("SyncRollHuluwaAdventure", self, "OnSyncRollHuluwaAdventure")
    g_ScriptEvent:RemoveListener("CloseTravelEventPopWnd", self, "OnCloseTravelEventPopWnd")
    g_ScriptEvent:RemoveListener("InitHuluwaAdventureTransform", self, "OnInitHuluwaAdventureTransform")
    g_ScriptEvent:RemoveListener("SyncHuluwaAdventureVisitStatusChange", self, "OnSyncHuluwaAdventureVisitStatusChange")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    if self.m_ShakingTick then
        UnRegisterTick(self.m_ShakingTick)
        self.m_ShakingTick = nil
    end
    if self.m_IsTravelingTick then
        UnRegisterTick(self.m_IsTravelingTick)
        self.m_IsTravelingTick = nil
    end
    if self.m_WaitTouZiFxTick then
        UnRegisterTick(self.m_WaitTouZiFxTick)
    end
    LuaHuLuWa2022Mgr.m_TravelTipId = nil
    CUIManager.CloseUI(CLuaUIResources.HuLuWaTravelEventPopWnd)
end
function LuaHuLuWaTravelWorldWnd:OnSendItem( args)
    Gac2Gas.QueryHuluwaAdventureWndInfo()
end
function LuaHuLuWaTravelWorldWnd:OnSyncHuluwaAdventureWndInfo()
    if not LuaHuLuWa2022Mgr.m_TravelingHuluwaId or LuaHuLuWa2022Mgr.m_TravelingHuluwaId == 0 then
        self:OnPlayerNodeClick()
    end
    self:InitCurPosition()
    self:RevertFxPosition()
    self:InitMapNodes()
    self:InitPlayerIcon()
    self:InitTouziCountAndReset()
end

function LuaHuLuWaTravelWorldWnd:OnInitHuluwaAdventureTransform()
    self:InitPlayerIcon()
end
--@region UIEvent

function LuaHuLuWaTravelWorldWnd:OnTouZiBtnClick()
    if self.m_ShakingTick or self.m_IsTravelingTick then
        return
    end
    if LuaHuLuWa2022Mgr.m_LeftTravelTimes <= 0 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HULUWA_INVALID_ROLL_TIMES"), 
            DelegateFactory.Action(function ()
                --CShopMallMgr.ShowLinyuShoppingMall(21051636)
                CLuaNPCShopInfoMgr.ShowScoreShopById(34000086)
            end),
            nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        return
    end

    Gac2Gas.RequestRollHuluwaAdventure()
end

function LuaHuLuWaTravelWorldWnd:OnSyncRollHuluwaAdventure(randVal, leftTimes)
    --print("OnSyncRollHuluwaAdventure",randVal, leftTimes)
    LuaHuLuWa2022Mgr.m_LeftTravelTimes = leftTimes
    self:InitTouziCountAndReset()
    self.m_EndIdx = (self.m_StartIdx + randVal - 1)

    self.m_CurPath = self.transform:Find(SafeStringFormat3("PathTexture/Path%d",self.m_CurIdx)):GetComponent(typeof(CHuLuWaPathData))
    
    local sidx,eidx
    if self.m_EndIdx <= 27 then
        sidx = self.m_StartIdx
        eidx = self.m_EndIdx
    else
        sidx = self.m_StartIdx
        eidx = 27
    end

    --特效路径
    local TravelPathFx = function()
        local list = CreateFromClass(MakeGenericClass(List, Vector3))
        local fx
        for i=sidx,eidx,1 do
            local pathData = self.transform:Find(SafeStringFormat3("PathTexture/Path%d",i)):GetComponent(typeof(CHuLuWaPathData))
            local pointList = pathData.mPathPointList
            if list.Count == 0 then
                fx = pathData.transform:Find("Fx"):GetComponent(typeof(CUIFx)) 
            end
            for l=0,pointList.Count-1,1 do
                CommonDefs.ListAdd(list, typeof(Vector3), pointList[l])
            end     
        end
        if eidx ~= self.m_EndIdx then
            for i=1,self.m_EndIdx-27,1 do
                local pathData = self.transform:Find(SafeStringFormat3("PathTexture/Path%d",i)):GetComponent(typeof(CHuLuWaPathData))
                local pointList = pathData.mPathPointList
                for l=0,pointList.Count-1,1 do
                    CommonDefs.ListAdd(list, typeof(Vector3), pointList[l])
                end     
            end
        end
        fx.OnLoadFxFinish = DelegateFactory.Action(function()           
            local pc = CommonDefs.GetComponentInChildren_GameObject_Type(fx.transform.gameObject, typeof(ParticleSystem))
            pc:Play()
        end)
        fx:LoadFx("fx/ui/prefab/UI_sanjie_saoguang.prefab")
        local tweener = LuaTweenUtils.DOLuaLocalPathOnce(fx.transform,CommonDefs.ListToArray(list),1,PathType.Linear, PathMode.Ignore, 0.1,Ease.InOutCubic)
        table.insert(self.m_HighLightFx,fx)
    end

    self.m_IsTraveling = true
    if self.m_CurPath.mPathPointList and self.m_CurPath.mPathPointList.Count > 0 then
        self.m_ShakeDiceAni.Inst:ShakeDice(randVal, randVal, randVal)
        self.m_IsShaking = true
        self.m_NextPassIndex = 0

        if not self.m_ShakingTick then
            self.m_ShakingTick = RegisterTickOnce(function()
                self.TouZiFx.OnLoadFxFinish = DelegateFactory.Action(function()            
                    local pc = CommonDefs.GetComponentInChildren_GameObject_Type(self.TouZiFx.transform.gameObject, typeof(ParticleSystem))
                    pc:Play()
                end)
                self.TouZiFx.transform.position = self.TouZiBtn.transform.position
                self.TouZiFx:LoadFx("fx/ui/prefab/UI_sanjie_shaizi.prefab")

                local path={self.TouZi.transform.localPosition,self.PlayerNode.transform.localPosition}
                local array = Table2Array(path, MakeArrayClass(Vector3))               
                local touzitweener = LuaTweenUtils.DOLuaLocalPathOnce(self.TouZiFx.transform,array,self.m_TouZiFxTime,PathType.Linear, PathMode.Ignore, self.m_TouZiFxTime,Ease.Linear)
                CommonDefs.OnComplete_Tweener(touzitweener, DelegateFactory.TweenCallback(function()
                    self.PlayerFx:LoadFx("fx/ui/prefab/UI_sanjie_baodian.prefab")
                    self:InitPlayerCountDown(randVal)
                    self.m_WaitTouZiFxTick = RegisterTickOnce(function()
                        self:InitBtnState(false)
                        self.m_IsShaking = false
                        TravelPathFx()--
                        self:OnTraveling()
                        self.m_ShakingTick = nil
                    end,1000)
                    
                end))
            end,1000)
        end
    end
end

function LuaHuLuWaTravelWorldWnd:OnTraveling()
    if not self.m_IsTravelingTick then
        self.m_IsTravelingTick = RegisterTick(function()
            if self.m_NextPassIndex < self.m_CurPath.mPathPointList.Count then
                local vec3 = self.m_CurPath.mPathPointList[self.m_NextPassIndex]
                self.PlayerNode.transform.localPosition = vec3
                self.m_NextPassIndex = self.m_NextPassIndex + 1
            else
                self.m_CurIdx = self.m_CurIdx + 1
                self:InitPlayerCountDown(self.m_EndIdx+1-self.m_CurIdx)
                if self.m_CurIdx <= self.m_EndIdx then
                    --套圈了
                    if self.m_CurIdx > 27 then
                        self.m_CurIdx = self.m_CurIdx % 27
                        self.m_EndIdx = self.m_EndIdx % 27
                    end
                    self.m_NextPassIndex = 0
                    self.m_CurPath = self.transform:Find(SafeStringFormat3("PathTexture/Path%d",self.m_CurIdx)):GetComponent(typeof(CHuLuWaPathData))
                else
                    if self.m_IsTravelingTick then
                        UnRegisterTick(self.m_IsTravelingTick)
                        self.m_IsTravelingTick = nil
                    end
                    self.m_StartIdx = self.m_CurIdx
                    self:RevertFxPosition()
                    self.m_IsTraveling = false
                    --重回起点
                    if self.m_EndIdx == 27 then self.m_EndIdx = 0 end

                    LuaHuLuWa2022Mgr.m_VisitedPosDict[self.m_EndIdx+1] = true
                    self:InitMapNodes()
                    self:InitPlayerIcon()
                    LuaHuLuWa2022Mgr.OpenTravelEventTip(self.m_EndIdx+1)
                end                           
            end
        end,50)
    end
end

function LuaHuLuWaTravelWorldWnd:InitBtnState(isEnable)
    local btn = self.TouZiBtn.transform:GetComponent(typeof(QnButton))
    btn.Enabled = isEnable
    self.TouZiBtn.color = isEnable and Color.white or Color.grey
end
function LuaHuLuWaTravelWorldWnd:OnTipBtnClick()
    g_MessageMgr:ShowMessage("Huluwa_Travel_Tip")
end

function LuaHuLuWaTravelWorldWnd:OnPlayerNodeClick()
    if self.m_IsTraveling then 
        return 
    end
    local type = LuaHuLuWa2022Mgr.EnumPreviewType.eTravel
    LuaHuLuWa2022Mgr.OpenHuLuWaChoosePreviewWnd(type)
end

function LuaHuLuWaTravelWorldWnd:OnCloseTravelEventPopWnd(eventId)
    Gac2Gas.RequestEnterHuluwaAdventure(eventId)
    --如果不用进副本 则不用关闭界面
    if HuluBrothers_Adventure.GetData(eventId).GameplayId and HuluBrothers_Adventure.GetData(eventId).GameplayId ~= 0 then 
        CUIManager.CloseUI(CLuaUIResources.HuLuWaTravelWorldWnd)
        CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
    end
end
function LuaHuLuWaTravelWorldWnd:OnCloseButtonClick()
    if self.m_IsTraveling then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CloseWnd_InTraveling_Confirm"), 
            DelegateFactory.Action(function ()
                CUIManager.CloseUI(CLuaUIResources.HuLuWaTravelWorldWnd)
            end),
            nil,
            LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        CUIManager.CloseUI(CLuaUIResources.HuLuWaTravelWorldWnd)
    end
end


--@endregion UIEvent

function LuaHuLuWaTravelWorldWnd:OnSyncHuluwaAdventureVisitStatusChange(bVisited)
    LuaHuLuWa2022Mgr.m_IsVisited = bVisited
    LuaHuLuWa2022Mgr.m_VisitedPosDict[self.m_CurIdx] = bVisited
    self:InitMapNodes()
    self:InitTouziCountAndReset()
end

--引导
function LuaHuLuWaTravelWorldWnd:GetGuideGo(methodName)
    if methodName == "GetPlayerNode" then
        return self.PlayerIcon.gameObject
    end
    if methodName == "GetTouZiBtn" then
        return self.TouZiBtn.gameObject
    end
    return nil
end