local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIInput = import "UIInput"
local DefaultChatInputListener = import "L10.UI.DefaultChatInputListener"
local CChatInputLink = import "CChatInputLink"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Time = import "UnityEngine.Time"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local Constants = import "L10.Game.Constants"
local CQnSymbolParser = import "CQnSymbolParser"
local EventDelegate = import "EventDelegate"

LuaSendPlotDiscussionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaSendPlotDiscussionWnd, "SubmitButton", "SubmitButton", GameObject)
RegistChildComponent(LuaSendPlotDiscussionWnd, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaSendPlotDiscussionWnd, "StatusText", "StatusText", UIInput)
RegistChildComponent(LuaSendPlotDiscussionWnd, "EmotionButton", "EmotionButton", GameObject)
RegistChildComponent(LuaSendPlotDiscussionWnd, "StatusLabel", "StatusLabel", UILabel)
RegistChildComponent(LuaSendPlotDiscussionWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaSendPlotDiscussionWnd, "Table", "Table", UITable)
--@endregion RegistChildComponent end
RegistClassMember(LuaSendPlotDiscussionWnd,"m_DefaultChatInputListener")
RegistClassMember(LuaSendPlotDiscussionWnd,"m_ExistingLinks")
RegistClassMember(LuaSendPlotDiscussionWnd,"m_InputHeight")

function LuaSendPlotDiscussionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitButtonClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)


	
	UIEventListener.Get(self.EmotionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEmotionButtonClick()
	end)


    --@endregion EventBind end
end

function LuaSendPlotDiscussionWnd:Init()
	self:InitDefaultChatInputListener()
	self.m_InputHeight = 340
	EventDelegate.Add(self.StatusText.onChange, DelegateFactory.Callback(function() self:OnInputValueChanged() end))
	self.Table:Reposition()
	self.ScrollView:ResetPosition()
end

--@region UIEvent
function LuaSendPlotDiscussionWnd:OnInputValueChanged()
	local totalString = CommonDefs.StringLength(self.StatusText.value)
    self.StatusLabel.text = SafeStringFormat3(LocalString.GetString('内容最多800个字，还剩[FFFF00]%s[-][c]个字'),800-totalString)
	if self.StatusText.label.height > self.m_InputHeight then
		self.ScrollView.contentPivot = UIWidget.Pivot.Bottom
	  else
		self.ScrollView.contentPivot = UIWidget.Pivot.Top
	  end
	self.Table:Reposition()
	self.ScrollView:ResetPosition()
end

function LuaSendPlotDiscussionWnd:OnSubmitButtonClick()
	local sendText = self:GetSendMsg()
	if sendText == nil then return end
	LuaPlotDiscussionMgr:RequestAddCardComment(LuaPlotDiscussionMgr.m_CardId, sendText)
	CUIManager.CloseUI(CLuaUIResources.SendPlotDiscussionWnd)
end

function LuaSendPlotDiscussionWnd:OnCancelButtonClick()
	CUIManager.CloseUI(CLuaUIResources.SendPlotDiscussionWnd)
end

function LuaSendPlotDiscussionWnd:OnEmotionButtonClick()
	CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, self.EmotionButton.gameObject, EChatPanel.Undefined, 0, 0)
end

--@endregion UIEvent

function LuaSendPlotDiscussionWnd:GetSendMsg()
	local input = self.StatusText
	local sendInfo = input.value
	if System.String.IsNullOrEmpty(sendInfo) then
        g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
        return nil
    end
	input.value = CQnSymbolParser.FilterExceededEmoticons(StringTrim(sendInfo), Constants.MaxEmotionNum)
	local msg = input.value
	if self.m_LastSendTime and Time.realtimeSinceStartup - self.m_LastSendTime < 1 then
        return nil
    end
	self.m_LastSendTime = Time.realtimeSinceStartup
	local sendText = CChatInputLink.Encapsulate(msg, self.m_ExistingLinks)
	sendText = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(sendText, true)
	return sendText
end

function LuaSendPlotDiscussionWnd:InitDefaultChatInputListener()
	self.m_ExistingLinks =  CreateFromClass(MakeGenericClass(Dictionary, Int32, CChatInputLink))
	self.m_DefaultChatInputListener = DefaultChatInputListener()
	self.m_DefaultChatInputListener:SetOnInputEmoticonFunc(function (prefix)
		self:OnInputEmoticon(prefix)
	end)
	self.m_DefaultChatInputListener:SetOnInputItemFunc(function (item)
		self:OnInputItemLink(item)
	end)
	self.m_DefaultChatInputListener:SetOnInputBabyFunc(function (babyId, babyName)
		self:OnInputBabyLink(babyId, babyName)
	end)
	self.m_DefaultChatInputListener:SetOnLingShouFunc(function (lingshouId, lingshouName)
		self:OnInputLingShouLink(lingshouId, lingshouName)
	end)
	self.m_DefaultChatInputListener:SetOnInputWeddingItemFunc(function (item)
		self:OnInputWeddingItemLink(item)
	end)
	self.m_DefaultChatInputListener:SetOnInputAchievementFunc(function (achievementId)
		self:OnInputAchievement(achievementId)
	end)
end

function LuaSendPlotDiscussionWnd:OnInputEmoticon(prefix)
	local input = self.StatusText
	self.StatusText.label.EnableEmotion = false
	input.text = SafeStringFormat3("%s#%s",input.text, prefix) 
end

function LuaSendPlotDiscussionWnd:OnInputItemLink(item)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendItemLink(input.value, item, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaSendPlotDiscussionWnd:OnInputBabyLink(babyId, babyName)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendBabyLink(input.value, babyId, babyName, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaSendPlotDiscussionWnd:OnInputLingShouLink(lingshouId, lingshouName)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendLingShouLink(input.value, lingshouId, lingshouName, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaSendPlotDiscussionWnd:OnInputWeddingItemLink(item)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendWeddingItemLink(input.value, item, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end

function LuaSendPlotDiscussionWnd:OnInputAchievement(achievementId)
	local text = nil
	local input = self.StatusText
	text = CChatInputLink.AppendAchievementLink(input.value, achievementId, self.m_ExistingLinks)
	self.StatusText.label.EnableEmotion = false
	input.text = text
end
