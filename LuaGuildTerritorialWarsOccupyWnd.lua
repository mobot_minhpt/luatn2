local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local CGuildMgr = import "L10.Game.CGuildMgr"

LuaGuildTerritorialWarsOccupyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "TopReadMeLabel", "TopReadMeLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "CurTerritorialNameLabel", "CurTerritorialNameLabel", CUITexture)
RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "CurTerritorialGuildsNumLabel", "CurTerritorialGuildsNumLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "CurTerritorialDescLabel", "CurTerritorialDescLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "CenterViewButton", "CenterViewButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "OccupyButton", "OccupyButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsOccupyWnd, "TipButton", "TipButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsOccupyWnd,"m_TabIndex")

function LuaGuildTerritorialWarsOccupyWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.CenterViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCenterViewButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.OccupyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOccupyButtonClick()
	end)
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsOccupyWnd:Init()
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelectTab(btn,index)
	end)
	self:OnSendTerritoryWarPickBornRegionInfo()
end

function LuaGuildTerritorialWarsOccupyWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSendTerritoryWarPickBornRegionInfo", self, "OnSendTerritoryWarPickBornRegionInfo")
end

function LuaGuildTerritorialWarsOccupyWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSendTerritoryWarPickBornRegionInfo", self, "OnSendTerritoryWarPickBornRegionInfo")
end

function LuaGuildTerritorialWarsOccupyWnd:OnSendTerritoryWarPickBornRegionInfo()
	local data = GuildTerritoryWar_Region.GetData(LuaGuildTerritorialWarsMgr.m_PickedRegion)
	self.TopReadMeLabel.text = not data and LocalString.GetString("请选择一块区域作为帮会落脚处") or 
	SafeStringFormat3(LocalString.GetString("已选择%s作为帮会落脚点"),data.Name)
	GuildTerritoryWar_Region.Foreach(function (region, data)
		local go = self.QnRadioBox.m_RadioButtons[region - 1]
		self:InitRegionGuildItem(go, data)
	end)
	self.OccupyButton.Enabled = data == nil
	self.QnRadioBox:ChangeTo(LuaGuildTerritorialWarsMgr.m_PickedRegion == 0 and 0 or LuaGuildTerritorialWarsMgr.m_PickedRegion - 1, true)
end

function LuaGuildTerritorialWarsOccupyWnd:InitRegionGuildItem(go, data)
	if not go then return end
	local guidNum = LuaGuildTerritorialWarsMgr.m_RegionGuildInfo[data.Region]

	local signSprite = go.transform:Find("NameLabel/SignSprite"):GetComponent(typeof(UISprite))
	local guildNumLabel = go.transform:Find("GuildNumLabel"):GetComponent(typeof(UILabel))
	local ownBangPai = go.transform:Find("GuildNumLabel/OwnBangPai").gameObject
	local otherBangPai = go.transform:Find("GuildNumLabel/OtherBangPai").gameObject

	ownBangPai.gameObject:SetActive(guidNum > 0 and LuaGuildTerritorialWarsMgr.m_PickedRegion == data.Region)
	otherBangPai.gameObject:SetActive(guidNum > 0 and LuaGuildTerritorialWarsMgr.m_PickedRegion ~= data.Region)
	guildNumLabel.text = guidNum
	guildNumLabel.gameObject:SetActive(guidNum > 0)
	signSprite.color = NGUIText.ParseColor24(data.Region == 7 and "B2B2B2" or (guidNum >= 2 and "FF0000" or "00FF00"), 0)
end

--@region UIEvent

function LuaGuildTerritorialWarsOccupyWnd:OnCenterViewButtonClick()
	g_MessageMgr:ShowMessage("GuildTerritorialWarsViewOccupyWnd_ClickCenter")
end

function LuaGuildTerritorialWarsOccupyWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("GuildTerritorialWarsViewOccupyWnd_ReadMe")
end

function LuaGuildTerritorialWarsOccupyWnd:OnOccupyButtonClick()
	local data = GuildTerritoryWar_Region.GetData(self.m_TabIndex + 1)
	if not CClientMainPlayer.Inst then return end
	if not CClientMainPlayer.Inst:IsInGuild() then return end
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("RequestPickTerritoryWarBornRegion_Confirm",CGuildMgr.Inst.m_GuildName,data.Name),DelegateFactory.Action(function()
		Gac2Gas.RequestPickTerritoryWarBornRegion(self.m_TabIndex + 1)
	end),nil,LocalString.GetString("我想好了"),LocalString.GetString("再想一下"),false)
end
--@endregion UIEvent

function LuaGuildTerritorialWarsOccupyWnd:OnSelectTab(btn,index)
	self.m_TabIndex = index

	local region = index + 1
	local guidNum = LuaGuildTerritorialWarsMgr.m_RegionGuildInfo[region]
	local data = GuildTerritoryWar_Region.GetData(region)
	local matArray = {"haotian","youtian","xuantian","biantian","cangtian","yantian"}

	self.CurTerritorialNameLabel:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/guildterritorialwarsoccupywnd_%s.mat",matArray[region]))
	self.CurTerritorialGuildsNumLabel.text = guidNum 
	self.CurTerritorialDescLabel.text = data.Description
end
