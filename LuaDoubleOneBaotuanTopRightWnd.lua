local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local LocalString = import "LocalString"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

LuaDoubleOneBaotuanTopRightWnd = class()
RegistChildComponent(LuaDoubleOneBaotuanTopRightWnd,"rightBtn", GameObject)
RegistChildComponent(LuaDoubleOneBaotuanTopRightWnd,"text1", UILabel)
RegistChildComponent(LuaDoubleOneBaotuanTopRightWnd,"text2", UILabel)

--RegistClassMember(LuaDoubleOneBaotuanTopRightWnd, "speItemTable")

function LuaDoubleOneBaotuanTopRightWnd:Close()
	--CUIManager.CloseUI(CLuaUIResources.)
end

function LuaDoubleOneBaotuanTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("DoubleOneBaotuanRoundRefresh", self, "InitData")
end

function LuaDoubleOneBaotuanTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("DoubleOneBaotuanRoundRefresh", self, "InitData")
end

function LuaDoubleOneBaotuanTopRightWnd:Init()
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

	LuaDoubleOne2019Mgr.BaotuanCurRound = nil
	self:InitData()
end

function LuaDoubleOneBaotuanTopRightWnd:InitData()
	if LuaDoubleOne2019Mgr.BaotuanCurRound and LuaDoubleOne2019Mgr.BaotuanTotalRound then
		self.text1.text = SafeStringFormat3(LocalString.GetString('第%s轮'),LuaDoubleOne2019Mgr.BaotuanCurRound)
		self.text2.text = SafeStringFormat3(LocalString.GetString('共%s轮'),LuaDoubleOne2019Mgr.BaotuanTotalRound)
	else
		self.text1.text = ''
		self.text2.text = ''
	end
end

return LuaDoubleOneBaotuanTopRightWnd
