require("3rdParty/ScriptEvent")
require("common/common_include")

local CUIFx = import "L10.UI.CUIFx"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CWelfareMgr = import "L10.UI.CWelfareMgr"

LuaMainWelfareWnd = class()

-- 子界面
RegistChildComponent(LuaMainWelfareWnd, "MonthCardReturnWindow", GameObject)
RegistChildComponent(LuaMainWelfareWnd, "DoubleExpWindow", GameObject)

RegistChildComponent(LuaMainWelfareWnd, "MonthCardTab", GameObject)
RegistChildComponent(LuaMainWelfareWnd, "DoubleExpTab", GameObject)

RegistClassMember(LuaMainWelfareWnd, "TabList")
RegistClassMember(LuaMainWelfareWnd, "SubWndList")
RegistClassMember(LuaMainWelfareWnd, "monthCardAlert")
RegistClassMember(LuaMainWelfareWnd, "doubleExpAlert")
RegistClassMember(LuaMainWelfareWnd, "showCount")
RegistClassMember(LuaMainWelfareWnd, "monthCardEmptyAlert")
RegistClassMember(LuaMainWelfareWnd, "monthCardEmptyAlertDone")

function LuaMainWelfareWnd:Init()
end

function LuaMainWelfareWnd:InitData()
	self.TabList = {}
	table.insert(self.TabList, self.MonthCardTab)
	table.insert(self.TabList, self.DoubleExpTab)

	self.SubWndList = {}
	table.insert(self.SubWndList, self.MonthCardReturnWindow)
	table.insert(self.SubWndList, self.DoubleExpWindow)

	CommonDefs.AddOnClickListener(self.MonthCardTab, DelegateFactory.Action_GameObject(function (go)
		self:OnTabClicked(1)
	end), false)
	CommonDefs.AddOnClickListener(self.DoubleExpTab, DelegateFactory.Action_GameObject(function (go)
		self:OnTabClicked(2)
	end), false)

	self.monthCardAlert = self.MonthCardTab.transform:Find('Sprite').gameObject
	self.doubleExpAlert = self.DoubleExpTab.transform:Find('Sprite').gameObject

	self.monthCardAlert:SetActive(false)

	self:UpdateAlert()

	self:CheckMonthCardStatus()
end

function LuaMainWelfareWnd:CheckMonthCardStatus()
	local monthCardInfo = CClientMainPlayer.Inst.ItemProp.MonthCard
	if monthCardInfo and monthCardInfo.BuyTime ~= 0 then
		local leftDay = CChargeWnd.MonthCardLeftDay(monthCardInfo)
		if leftDay <= 0 then
			Gac2Gas.QueryMonthCardRedPoint('redPoint')
		end
	end
end

function LuaMainWelfareWnd:HideAllTabs()
	self.MonthCardReturnWindow:SetActive(false)
	self.DoubleExpWindow:SetActive(false)
end


function LuaMainWelfareWnd:OnTabClicked(index)
	for i = 1, #self.TabList do
		-- set selected
		self:SetTabSelected(self.TabList[i], i == index, i)
		-- show wnd
		self.SubWndList[i].gameObject:SetActive(i == index)
	end
end

function LuaMainWelfareWnd:SetTabSelected(go, selected, index)
	local texture = go.transform:Find("Texture").gameObject
	texture:SetActive(selected)
	local fx = go.transform:Find("FxRoot"):GetComponent(typeof(CUIFx))
	if fx then
		if selected then
			if index == 1 and self.monthCardEmptyAlert then
				self.monthCardAlert:SetActive(false)
				if not self.monthCardEmptyAlertDone then
					self.monthCardEmptyAlertDone = true
				end
			end
			fx:LoadFx(SafeStringFormat3("Fx/UI/Prefab/UI_fulijiemian_0%s.prefab", tostring(index)))
			fx.gameObject:SetActive(true)
		else
			fx.gameObject:SetActive(false)
		end
	end
end

function LuaMainWelfareWnd:ShowMonthCard()
	self.MonthCardReturnWindow:SetActive(true)
end

function LuaMainWelfareWnd:ShowDoubleExp()
	self.DoubleExpWindow:SetActive(true)
end

function LuaMainWelfareWnd:SwitchToMonthCard()
	if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsCrossServerPlayer then
		return
	end
end

function LuaMainWelfareWnd:UpdateAlert()
	local monthSign = false
	--local monthLeftAlertSign = false
	local doubleExpSign = false

	local monthCardInfo = CClientMainPlayer.Inst.ItemProp.MonthCard
	if monthCardInfo and monthCardInfo.BuyTime ~= 0 then
		local leftDay = CChargeWnd.MonthCardLeftDay(monthCardInfo)
		if leftDay > 0 then
			if not CPropertyItem.IsSamePeriod(monthCardInfo.LastReturnTime, "d") or CWelfareMgr.ShowMonthCardAlert() then
				self.monthCardAlert:SetActive(true)
				monthSign = true
			end
		end
	end
	--self.monthCardEmptyAlert = monthLeftAlertSign


	if not CSigninMgr.Inst:HasGetFreeDoubleExp() then
		self.doubleExpAlert:SetActive(true)
		doubleExpSign = true
	else
		self.doubleExpAlert:SetActive(false)
	end

	if LuaWelfareMgr.m_MainWelfareWndShowMonth then
		monthSign = true
		LuaWelfareMgr.m_MainWelfareWndShowMonth = false
	end

	if not self.showCount then
		self.showCount = 0
	end
	if self.showCount == 0 then
		self.showCount = 1
		if doubleExpSign and monthSign then
			self:OnTabClicked(1)
		elseif not doubleExpSign and monthSign then
			self:OnTabClicked(1)
		elseif doubleExpSign and not monthSign then
			self:OnTabClicked(2)
		else
			if CClientMainPlayer.Inst.MaxLevel < MultipleExp_GameSetting.GetData().Receive_Grade then
				self:OnTabClicked(1)
			else
				self:OnTabClicked(2)
			end
		end
	end
end

function LuaMainWelfareWnd:UpdateCardEmptyAlert(flag)
	if flag == 1 then
		self.monthCardEmptyAlert = true
		self.monthCardAlert:SetActive(true)
	end
end

function LuaMainWelfareWnd:OnEnable()
	--self:HideAllTabs()

	--self:ShowMonthCard()
	--self:ShowDoubleExp()
	g_ScriptEvent:AddListener("OnQueryDoubleExpInfoReturn", self, "UpdateAlert")
	g_ScriptEvent:AddListener("OnMonthCardAlertStatusChange", self, "UpdateAlert")
	g_ScriptEvent:AddListener("OnMonthCardRedPointQuery", self, "UpdateCardEmptyAlert")

	--self:OnTabClicked(1)
	self:InitData()
end

function LuaMainWelfareWnd:OnDisable()
	-- body
	g_ScriptEvent:RemoveListener("OnQueryDoubleExpInfoReturn", self, "UpdateAlert")
	g_ScriptEvent:RemoveListener("OnMonthCardAlertStatusChange", self, "UpdateAlert")
	g_ScriptEvent:RemoveListener("OnMonthCardRedPointQuery", self, "UpdateCardEmptyAlert")
end

function Gas2Gac.QueryMonthCardRedPointDone(flag, extra)
  g_ScriptEvent:BroadcastInLua("OnMonthCardRedPointQuery",flag)
end

return LuaMainWelfareWnd
