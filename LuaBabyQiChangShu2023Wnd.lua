require("common/common_include")

local UIGrid = import "UIGrid"
local Baby_Setting = import "L10.Game.Baby_Setting"
--local Baby_QiChang = import "L10.Game.Baby_QiChang"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local QnButton = import "L10.UI.QnButton"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CUIFx = import "L10.UI.CUIFx"
local CChatLinkMgr = import "CChatLinkMgr"
local CButton = import "L10.UI.CButton"

LuaBabyQiChangShu2023Wnd = class()

RegistChildComponent(LuaBabyQiChangShu2023Wnd, "TypeScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "TypeGrid", UIGrid)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "QiChangTypePrefab", GameObject)

RegistChildComponent(LuaBabyQiChangShu2023Wnd, "QiChangTypeLabel", UILabel)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "ActiveAllTypeQiChang", UILabel)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "GainButton", CButton)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "GainFx", CUIFx)

RegistChildComponent(LuaBabyQiChangShu2023Wnd, "YiJianDingLabel", UILabel)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "TableView", QnTableView)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "QiChangScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "QiChangGrid", UIGrid)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "QiChangItem", GameObject)
--2023
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "FindCurrentBtn", GameObject)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "FindJieBanBtn", GameObject)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "FindTargetBtn", GameObject)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "FindRecormmendBtn", GameObject)
RegistChildComponent(LuaBabyQiChangShu2023Wnd, "TipBtn", GameObject)

RegistClassMember(LuaBabyQiChangShu2023Wnd, "SelectedBaby")
RegistClassMember(LuaBabyQiChangShu2023Wnd, "TypeItems")
RegistClassMember(LuaBabyQiChangShu2023Wnd, "SelectedQiChangTypeIndex")
RegistClassMember(LuaBabyQiChangShu2023Wnd, "SelectedQiChangId")
RegistClassMember(LuaBabyQiChangShu2023Wnd, "TableViewDataSource")
RegistClassMember(LuaBabyQiChangShu2023Wnd, "SelectedTypeQiChangs")
RegistClassMember(LuaBabyQiChangShu2023Wnd, "mTargetQiChangId")
RegistClassMember(LuaBabyQiChangShu2023Wnd, "mRecormmendQiChangId")

function LuaBabyQiChangShu2023Wnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyQiChangShu2023Wnd:InitClassMembers()
	if not LuaBabyMgr.m_ChosenBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyQiChangShuWnd)
		return
	end

	self.SelectedBaby = LuaBabyMgr.m_ChosenBaby
	self.SelectedQiChangTypeIndex = -1
	self.SelectedQiChangId = 0
	self.SelectedTypeQiChangs = {}
	self.mTargetQiChangId = LuaBabyMgr.mTargetQiChangId

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.SelectedTypeQiChangs
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

	UIEventListener.Get(self.FindCurrentBtn).onClick = DelegateFactory.VoidDelegate(function()
		self:OnFindCurrentBtnClick()
    end)
	UIEventListener.Get(self.FindJieBanBtn).onClick = DelegateFactory.VoidDelegate(function()
		self:OnFindJieBanBtnClick()
    end)
	UIEventListener.Get(self.FindTargetBtn).onClick = DelegateFactory.VoidDelegate(function()
		self:OnFindTargetBtnClick()
    end)
	UIEventListener.Get(self.FindRecormmendBtn).onClick = DelegateFactory.VoidDelegate(function()
		self:OnFindRecormmendBtnClick()
    end)
	UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function()
		self:OnTipBtnClick()
    end)
end

function LuaBabyQiChangShu2023Wnd:InitValues()
	self.QiChangTypePrefab:SetActive(false)
	self.QiChangItem:SetActive(false)

	self:InitQiChangeTypes()
	
	--if #self.TypeItems > 0 and self.TypeItems[1]:GetComponent(typeof(QnButton)) then
	--	local selectableBtn = self.TypeItems[1]:GetComponent(typeof(QnButton))
	--	selectableBtn:On_Click(self.TypeItems[1])
	--	self:OnQiChangTypeClicked(self.TypeItems[1], 0)
	--end
	-- 选中当前气场所在的气场品质，若无当前气场则选择第一个
	local data = Baby_QiChang.GetData(self.SelectedBaby.Props.QiChangData.CurrentQiChangId)
	if data then
		local index = data.Quality - 1
		self:SetQiChangTypeSelect(index)
	else
		self:SetQiChangTypeSelect(0)
	end 
	
	self.mRecormmendQiChangId = 0
	local babyId = self.SelectedBaby.Id
	Gac2Gas.QueryTargetQiChang(babyId)

	
end

function LuaBabyQiChangShu2023Wnd:InitQiChangeTypes()
	CUICommonDef.ClearTransform(self.TypeGrid.transform)
	self.TypeItems = {}

	local setting = Baby_Setting.GetData()
	local qiChangTypes = setting.NewQiChangTypeName
	for i = 0, qiChangTypes.Length-1 do
		local go = NGUITools.AddChild(self.TypeGrid.gameObject, self.QiChangTypePrefab)
		self:InitQiChangType(go, qiChangTypes[i], i)
		go:SetActive(true)
		table.insert(self.TypeItems, go)
	end

	self.TypeGrid:Reposition()
end

function LuaBabyQiChangShu2023Wnd:InitQiChangType(go, name, index)
	local Label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	local Alert = go.transform:Find("Alert").gameObject
	Label.text = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangShuColor(index), name)

	-- 显示红点
	Alert:SetActive(LuaBabyMgr.HasNewQiChangRewardForType(index+1, self.SelectedBaby))
	local onTypeClicked = function (go)
		self:OnQiChangTypeClicked(go, index)
	end
	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onTypeClicked), false)
end

-- index 从0开始
function LuaBabyQiChangShu2023Wnd:OnQiChangTypeClicked(go, index)
	self.SelectedQiChangTypeIndex = index

	for i = 1, #self.TypeItems do
		local normalBG = self.TypeItems[i].transform:Find("BG").gameObject
		local selectedBG = self.TypeItems[i].transform:Find("BG_S").gameObject
		if self.TypeItems[i] ~= go then
			normalBG:SetActive(true)
			selectedBG:SetActive(false)
		else
			normalBG:SetActive(false)
			selectedBG:SetActive(true)
		end
	end

	local setting = Baby_Setting.GetData()
	local qiChangTypes = setting.NewQiChangTypeName
	self.QiChangTypeLabel.text = SafeStringFormat3(LocalString.GetString("[%s]%s气场[-]"), LuaBabyMgr.GetQiChangShuColor(index), qiChangTypes[index])
	

	local qichangReward = Baby_QiChangReward.GetData(index+1)
	if not qichangReward then
		self.ActiveAllTypeQiChang.gameObject:SetActive(false)
	else
		self.ActiveAllTypeQiChang.gameObject:SetActive(true)
		local itemTId = qichangReward.Reward[0]
		local itemLink =  CChatLinkMgr.ChatItemLink.GenerateLink(itemTId, nil)
		self.ActiveAllTypeQiChang.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]鉴定出全部%s气场奖励[-]%s"), qiChangTypes[index], itemLink.displayTag)

		CommonDefs.AddOnClickListener(self.ActiveAllTypeQiChang.gameObject, DelegateFactory.Action_GameObject(function (go)
			local url = self.ActiveAllTypeQiChang:GetUrlAtPosition(UICamera.lastWorldPosition)
    		if url then
        		CChatLinkMgr.ProcessLinkClick(url, nil)
    		end
		end), false)
	end


	self.SelectedQiChangId = 0

	local totalQiChangCounter = 0
	local activeQiChangCounter = 0
	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
	local currentQiChangId = self.SelectedBaby.Props.QiChangData.CurrentQiChangId

	local jiebanQiChangId = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId
	local isOwnBaby = self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id -- 只有专属宝宝才能结伴气场

	self.SelectedTypeQiChangs = {}
	local isAllActived = true

	Baby_QiChang.ForeachKey(function (key)
        local qichang = Baby_QiChang.GetData(key)
        if qichang.Quality-1 == index then

        	local isActive = CommonDefs.DictContains_LuaCall(activedQiChang, key)
        	local isCurrent = key == currentQiChangId
        	local quality = index
        	local isSetJieBan = (jiebanQiChangId == key) and isOwnBaby
			local isUnlocked = self:IsQiChangUnlocked(key)
        	table.insert(self.SelectedTypeQiChangs, {key = key, 
        		isActive = isActive,
        		isCurrent = isCurrent,
        		quality = quality,
        		isSetJieBan = isSetJieBan,
				isUnlocked = isUnlocked})

        	totalQiChangCounter = totalQiChangCounter + 1
        	if isActive then
        		activeQiChangCounter = activeQiChangCounter + 1
        	end
        	isAllActived = isAllActived and isActive
        end
    end)

	-- todo 领取显示
	if qichangReward then
		local activedReward = self.SelectedBaby.Props.QiChangData.ActivedRewardStatus
		if not activedReward or not CommonDefs.DictContains_LuaCall(activedReward, index+1) then
			-- 没有领取过
			if isAllActived then
				self.GainFx:LoadFx("Fx/UI/Prefab/UI_huangdonglibao.prefab")
				self.GainButton.gameObject:SetActive(true)
				self.GainButton.Enabled = true
				CommonDefs.AddOnClickListener(self.GainButton.gameObject, DelegateFactory.Action_GameObject(function (go)
					Gac2Gas.RequestReceiveQiChangActiveReward(self.SelectedBaby.Id, index+1)
				end), false)
			else
				self.GainFx:DestroyFx()
				self.GainButton.gameObject:SetActive(false)
			end
		else
			-- 领取过了
			self.GainFx:DestroyFx()
			self.GainButton.gameObject:SetActive(true)
			self.GainButton.Enabled = false
		end
	end

	-- 加一个排序
	local function compare(a, b)
		if a.isCurrent and not b.isCurrent then
			return true
		elseif not a.isCurrent and b.isCurrent then
			return false
		else
			if a.isSetJieBan and not b.isSetJieBan then
				return true
			elseif not a.isSetJieBan and b.isSetJieBan then
				return false
			else
				if a.isActive and not b.isActive then
					return true
				elseif not a.isActive and b.isActive then
					return false
				else
					if a.isUnlocked and not b.isUnlocked then
						return true
					elseif not a.isUnlocked and b.isUnlocked then
						return false
					else
						return a.key < b.key
					end
				end
			end
			
		end
	end
	table.sort(self.SelectedTypeQiChangs, compare)

	self.TableView:ReloadData(true, true)
	
	self.YiJianDingLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]已鉴定[-]  %d/%d"), activeQiChangCounter, totalQiChangCounter)
end

function LuaBabyQiChangShu2023Wnd:IsQiChangUnlocked(qichangId)
	return LuaBabyMgr.IsQiChangUnlocked(qichangId,self.SelectedBaby)
	--[[local isSatisfied = true
	local qichang = Baby_QiChang.GetData(qichangId)
	local preQichangId = qichang.PreQichang
    local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
    -- step 1 前置气场
    if preQichangId ~= 0 then
        local preQiChang = Baby_QiChang.GetData(preQichangId)
        if preQiChang then
            -- 检查前置气场是否鉴定
            if not CommonDefs.DictContains_LuaCall(activedQiChang, preQiChang.ID) then
				isSatisfied = false
				return false
            end
        end     
    end

    -- Step 2 获得奇遇
    local qiyuDict = self.SelectedBaby.Props.ScheduleData.QiYuData
    if qichang.QiyuGet ~= 0 then
        local qiyu = Baby_QiYu.GetData(qichang.QiyuGet)
        if qiyu then
            if not CommonDefs.DictContains_LuaCall(qiyuDict, qiyu.ID) then
				isSatisfied = false
				return false
            end
        end
    end

    -- Step 3 属性要求, 有12个
    for i = 1, 12 do
        if LuaBabyMgr.GetQiChangRequestPropByIndex(i, qichang) > 0 then
            local request = LuaBabyMgr.GetQiChangRequestPropByIndex(i, qichang)
            local own = 0
            if self.SelectedBaby.Props.YoungProp and CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, i) then
                own = self.SelectedBaby.Props.YoungProp[i]
            end
            local shaoNian = Baby_ShaoNian.GetData(i)
            local youEr = Baby_YouEr.GetData(shaoNian.YouErName)
            if shaoNian and youEr then          
                if own < request then
					isSatisfied = false
					return false
                end
            end
        end
    end

    -- Step 4 属性最高
    if qichang.TopAttribute ~= 0 and qichang.Quality >= 5 then
        local youer = Baby_YouEr.GetData(qichang.TopAttribute)

        if youer and self.SelectedBaby.Props.YoungProp then
            local highest = 0

            if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, qichang.TopAttribute) then
                highest = LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, qichang.TopAttribute)
            end
            local isHighest = true

            if self.SelectedBaby.Props.YoungProp and self.SelectedBaby.Props.YoungProp.Count >= 12 then
                for i = 1, 6, 1 do
                    if qichang.TopAttribute ~=i and LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, i) > highest then
                        isHighest = false
                    end
                end
            end
            if not isHighest then
				isSatisfied = false
				return false
            end
        end
    end
	return isSatisfied--]]
end

function LuaBabyQiChangShu2023Wnd:InitItem(item, index)

	local info = self.SelectedTypeQiChangs[index+1]
	if not info then return nil end

	local key = info.key
	local isCurrent = info.isCurrent
	local isActive = info.isActive
	local quality = info.quality
	local isSetJieBan = info.isSetJieBan
	local isUnlocked = info.isUnlocked

	local BG = item.transform:Find("Bg"):GetComponent(typeof(UITexture))
	local QiChangNameLabel = item.transform:Find("Bg/QiChangNameLabel"):GetComponent(typeof(UILabel))
	local Lock = item.transform:Find("Bg/Lock").gameObject
	local TagGrid = item.transform:Find("TagGrid"):GetComponent(typeof(UIGrid))
	CUICommonDef.ClearTransform(TagGrid.transform)
	local Current = item.transform:Find("Current").gameObject
	Current:SetActive(false)
	local JieBan = item.transform:Find("JieBan").gameObject
	JieBan:SetActive(false)
	local Target = item.transform:Find("Target").gameObject
	Target:SetActive(false)
	local Recormmend = item.transform:Find("Recormmend").gameObject
	Recormmend:SetActive(false)
	local DaiJianDing = item.transform:Find("DaiJianDing").gameObject
	local Reward = item.transform:Find("Reward").gameObject
	Reward:SetActive(false)
	local IconTexture = item.transform:Find("Reward/IconTexture"):GetComponent(typeof(CUITexture))
	IconTexture:Clear()
	local DisableSprite = item.transform:Find("Reward/DisableSprite").gameObject

	local qichang = Baby_QiChang.GetData(key)
	local name = qichang.NameM
	local qiChangReward = qichang.RewardM
	if self.SelectedBaby.Gender == 1 then
		name = qichang.NameF
		qiChangReward = qichang.RewardF
	end
	local cname = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangShuColor(quality), name)
	QiChangNameLabel.text = LocalString.StrH2V(cname,false)
	if not isActive then
		BG.alpha = 0.5
		QiChangNameLabel.alpha = 0.5
	else
		BG.alpha = 1
		QiChangNameLabel.alpha = 1
	end
	
	Reward:SetActive(false)
	if qiChangReward and qiChangReward.Length > 0 then
		Reward:SetActive(true)

		local itemTId = qiChangReward[0]
		local item = Item_Item.GetData(itemTId)
    	if item then
        	IconTexture:LoadMaterial(item.Icon)
        	CommonDefs.AddOnClickListener(Reward, DelegateFactory.Action_GameObject(function (go)
            	CItemInfoMgr.ShowLinkItemTemplateInfo(itemTId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        	end), false)
		end
		DisableSprite:SetActive(not isActive)
	else
		local qiChangSkill = nil
		Baby_Skill.ForeachKey((function (skillId)
			local data  = Baby_Skill.GetData(skillId)
			if data.ExQiChang == key and self:IsSkillOpen(skillId) then
				qiChangSkill = data
			end
		end))
		if qiChangSkill then
			Reward:SetActive(true)
			IconTexture:LoadMaterial(qiChangSkill.Icon)
			CommonDefs.AddOnClickListener(Reward, DelegateFactory.Action_GameObject(function (go)
            	LuaBabyMgr.ShowBabySkillTip(qiChangSkill.ID)
        	end), false)
		end
		DisableSprite:SetActive(not isActive)
	end
	

	if isCurrent then
		local go = NGUITools.AddChild(TagGrid.gameObject, Current)
		go:SetActive(true)
	end

	if isSetJieBan then
		local go = NGUITools.AddChild(TagGrid.gameObject, JieBan)
		go:SetActive(true)
	end

	--IsTaregt
	if key == self.mTargetQiChangId then
		local go = NGUITools.AddChild(TagGrid.gameObject, Target)
		go:SetActive(true)
	end
	--isRecormmend
	if key == self.mRecormmendQiChangId then
		local go = NGUITools.AddChild(TagGrid.gameObject, Recormmend)
		go:SetActive(true)
	end

	--鉴定和解锁
	if not isActive and isUnlocked then
		Lock:SetActive(false)
		DaiJianDing:SetActive(key ~= self.mTargetQiChangId and key ~= self.mRecormmendQiChangId)
	elseif not isActive and not isUnlocked then
		Lock:SetActive(true)
		DaiJianDing:SetActive(false)
	elseif isActive then
		Lock:SetActive(false)
		DaiJianDing:SetActive(false)
	end
	TagGrid:Reposition()

end

function LuaBabyQiChangShu2023Wnd:OnSelectAtRow(row)
	local info = self.SelectedTypeQiChangs[row+1]
	if not info then return end
	self.SelectedQiChangId = info.key
	LuaBabyMgr.OpenQiChangTip(info.key)
end

function LuaBabyQiChangShu2023Wnd:OnQiChangItemClicked(go, key)
	self.SelectedQiChangId = key
	LuaBabyMgr.OpenQiChangTip(key)
end

function LuaBabyQiChangShu2023Wnd:OnSetButtonClicked(go)
	if self.SelectedQiChangId == 0 then
		g_MessageMgr:ShowMessage("BABY_NO_SELECT_QICHANG")
		return
	else
		if self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id then
			Gac2Gas.SetJieBanQiChang(self.SelectedBaby.Id , self.SelectedQiChangId)
		else
			g_MessageMgr:ShowMessage("CANNOT_SET_JIEBAN")
		end
		
	end
end

function LuaBabyQiChangShu2023Wnd:SetJieBanQiChangSuccess(bSuccess, babyId, qiChangId)
	--if CClientMainPlayer.Inst then
	--	if #self.TypeItems >= self.SelectedQiChangTypeIndex+1 and self.TypeItems[self.SelectedQiChangTypeIndex+1]:GetComponent(typeof(QnButton)) then
	--		local selectableBtn = self.TypeItems[self.SelectedQiChangTypeIndex+1]:GetComponent(typeof(QnButton))
	--		selectableBtn:On_Click(self.TypeItems[self.SelectedQiChangTypeIndex+1])
	--		self:OnQiChangTypeClicked(self.TypeItems[self.SelectedQiChangTypeIndex+1], self.SelectedQiChangTypeIndex)
	--	end
	--end
	if bSuccess and babyId == self.SelectedBaby.Id then
		self:FindAndGotoQiChangBtn(qiChangId)
	end
end

function LuaBabyQiChangShu2023Wnd:ReceiveQiChangActiveRewardSuccess(babyId)
	if babyId == self.SelectedBaby.Id then
		self.GainFx:DestroyFx()
		self.GainButton.Enabled = false
	end

	-- 更新左边一列的红点
	for i = 1, #self.TypeItems do
		local Alert = self.TypeItems[i].transform:Find("Alert").gameObject
		Alert:SetActive(LuaBabyMgr.HasNewQiChangRewardForType(i, self.SelectedBaby))
	end
end

--Click Event
function LuaBabyQiChangShu2023Wnd:OnFindCurrentBtnClick()
	local currentQiChangId = self.SelectedBaby.Props.QiChangData.CurrentQiChangId
	if currentQiChangId == 0 or currentQiChangId == nil then
		g_MessageMgr:ShowMessage("BABY_NO_CURRENT_QICHANG")
		return
	end
	self:FindAndGotoQiChangBtn(currentQiChangId)
end
function LuaBabyQiChangShu2023Wnd:OnFindJieBanBtnClick()
	local jiebanQiChangId = CClientMainPlayer.Inst.PlayProp.JieBanQiChangId
	if jiebanQiChangId == 0 then
		g_MessageMgr:ShowMessage("BABY_NO_JIEBAN_QICHANG")
		return
	end
	local isOwnBaby = self.SelectedBaby.OwnerId == CClientMainPlayer.Inst.Id
	if not isOwnBaby then
		g_MessageMgr:ShowMessage("YANGYU_SET_JIEBAN_QICHANG_NOT_OWN_BABY")
		return
	end
	self:FindAndGotoQiChangBtn(jiebanQiChangId)	
end
function LuaBabyQiChangShu2023Wnd:OnFindTargetBtnClick()
	if self.mTargetQiChangId == 0 then
		g_MessageMgr:ShowMessage("BABY_NO_TARGET_QICHANG")
		return
	end
	self:FindAndGotoQiChangBtn(self.mTargetQiChangId)
end
function LuaBabyQiChangShu2023Wnd:OnFindRecormmendBtnClick()
	local babyId = self.SelectedBaby.Id
	--生成推荐气场
	Gac2Gas.QueryRecommendQiChang(babyId)
end
function LuaBabyQiChangShu2023Wnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("BABY_QICHANGSHUWND2023_TIP")
end

function LuaBabyQiChangShu2023Wnd:FindAndGotoQiChangBtn(qichangId)
	local data = Baby_QiChang.GetData(qichangId)
	if data then
		local index = data.Quality - 1
		self:SetQiChangTypeSelect(index)
		local row = -1
		for idx,info in ipairs(self.SelectedTypeQiChangs) do
			if info.key == qichangId then
				row = idx - 1
			end
		end

		if row >= 0 then
			self.TableView:SetSelectRow(row, true)
		end

		if row > 9 then
			self.ScrollToRowTick = RegisterTickOnce(function()
				self.TableView:ScrollToRow(row)
			end, 300)
		end
	end

	-- 取消目标气场
	if qichangId == 0 then
		local data = Baby_QiChang.GetData(self.SelectedQiChangId)
		if data then
			--local index = data.Quality - 1
			--self:SetQiChangTypeSelect(index)
			
			local row = -1
			for idx,info in ipairs(self.SelectedTypeQiChangs) do
				if info.key == self.SelectedQiChangId then
					row = idx - 1
				end
			end

			if row >= 0 then
				self.TableView:SetSelectRow(row, false)
			end

			self.SelectedQiChangId = 0
		end
	end
	end

function LuaBabyQiChangShu2023Wnd:SetQiChangTypeSelect(index)
	if #self.TypeItems > 0 and self.TypeItems[index+1]:GetComponent(typeof(QnButton)) then
		local selectableBtn = self.TypeItems[index+1]:GetComponent(typeof(QnButton))
		selectableBtn:On_Click(self.TypeItems[index+1])
		self:OnQiChangTypeClicked(self.TypeItems[index+1], index)
	end
end

function LuaBabyQiChangShu2023Wnd:OnSyncTargetQiChangInfo(babyId, targetQiChangId, recommendQiChangId)
	if babyId == self.SelectedBaby.Id then
		self.mTargetQiChangId = targetQiChangId
		--self:FindAndGotoQiChangBtn(targetQiChangId)
		--if #self.TypeItems >= self.SelectedQiChangTypeIndex+1 and self.TypeItems[self.SelectedQiChangTypeIndex+1]:GetComponent(typeof(QnButton)) then
		--	local selectableBtn = self.TypeItems[self.SelectedQiChangTypeIndex+1]:GetComponent(typeof(QnButton))
		--	selectableBtn:On_Click(self.TypeItems[self.SelectedQiChangTypeIndex+1])
		--	self:OnQiChangTypeClicked(self.TypeItems[self.SelectedQiChangTypeIndex+1], self.SelectedQiChangTypeIndex,false)
		--end
	end
end

function LuaBabyQiChangShu2023Wnd:OnSyncSetTargetQiChangRes(babyId, qiChangId)
	if babyId == self.SelectedBaby.Id then
		self.mTargetQiChangId = qiChangId
		if qiChangId == 0 then --取消目标气场
			self:FindAndGotoQiChangBtn(self.SelectedQiChangId)
			return
		end
		self:FindAndGotoQiChangBtn(qiChangId)
	end
end

function LuaBabyQiChangShu2023Wnd:OnSyncRecommendQiChang(babyId,qichangId)
	if babyId == self.SelectedBaby.Id then
		self.mRecormmendQiChangId = qichangId
		self:FindAndGotoQiChangBtn(qichangId)
	end
end

function LuaBabyQiChangShu2023Wnd:OnEnable()
	g_ScriptEvent:AddListener("SetJieBanBabyQiChangRes", self, "SetJieBanQiChangSuccess")
	g_ScriptEvent:AddListener("ReceiveQiChangActiveRewardSuccess", self, "ReceiveQiChangActiveRewardSuccess")
	g_ScriptEvent:AddListener("SyncTargetQiChangInfo", self, "OnSyncTargetQiChangInfo")
	g_ScriptEvent:AddListener("SyncRecommendQiChang", self, "OnSyncRecommendQiChang")
	g_ScriptEvent:AddListener("SyncSetTargetQiChangRes", self, "OnSyncSetTargetQiChangRes")
	g_ScriptEvent:AddListener("FindAndGotoQiChang", self, "FindAndGotoQiChangBtn")
end

function LuaBabyQiChangShu2023Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("SetJieBanBabyQiChangRes", self, "SetJieBanQiChangSuccess")
	g_ScriptEvent:RemoveListener("ReceiveQiChangActiveRewardSuccess", self, "ReceiveQiChangActiveRewardSuccess")
	g_ScriptEvent:RemoveListener("SyncTargetQiChangInfo", self, "OnSyncTargetQiChangInfo")
	g_ScriptEvent:RemoveListener("SyncRecommendQiChang", self, "OnSyncRecommendQiChang")
	g_ScriptEvent:RemoveListener("SyncSetTargetQiChangRes", self, "OnSyncSetTargetQiChangRes")
	g_ScriptEvent:RemoveListener("FindAndGotoQiChang", self, "FindAndGotoQiChangBtn")
	LuaBabyMgr.mRecormmendQiChangId = 0
	if self.ScrollToRow then UnRegisterTick(self.ScrollToRow) self.ScrollToRow = nil end
end

function LuaBabyQiChangShu2023Wnd:IsSkillOpen(skillId)
	if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
		-- hmt和vn版本屏蔽宝宝说话相关技能和气场相关奖励
		if skillId == 2 or skillId == 4 or skillId == 5 then
			return false
		end
			return true
	else 
		return true
	end
end

function LuaBabyQiChangShu2023Wnd:GetGuideGo(methodName)
	if methodName=="RecommendQiChangButton" then
		return self.FindRecormmendBtn
	elseif methodName=="RuleButton" then
		return self.TipBtn
	end
	return nil
end

return LuaBabyQiChangShu2023Wnd
