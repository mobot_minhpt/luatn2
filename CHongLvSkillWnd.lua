-- Auto Generated!!
local CHongLvSkillWnd = import "L10.UI.CHongLvSkillWnd"
local CHongLvTemple = import "L10.UI.CHongLvTemple"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local ShengDan_HongLvZuoZhan = import "L10.Game.ShengDan_HongLvZuoZhan"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CHongLvSkillWnd.m_Init_CS2LuaHook = function (this) 

    this.applyButton.Visible = not CHongLvSkillWnd.s_DisableSignUp
    this.gameOutline.text = g_MessageMgr:FormatMessage("CHRISTMAS_HONGLV_NOTICE")
    local data = ShengDan_HongLvZuoZhan.GetData()
    --初始化技能
    if data ~= nil and data.SceneSkill ~= nil then
        do
            local i = 0
            while i < data.SceneSkill.Length and i < this.skills.Length do
                this.skills[i]:Init(data.SceneSkill[i])
                i = i + 1
            end
        end
    end
    --初始化神坛
    this.m_TempleTemplate.gameObject:SetActive(false)
    if data ~= nil and data.TempleList ~= nil then
        do
            local i = 0
            while i < data.TempleList.Length do
                local go = CommonDefs.Object_Instantiate(this.m_TempleTemplate.gameObject)
                go:SetActive(true)
                go.transform.parent = this.m_Table.transform
                go.transform.localScale = Vector3.one
                local temple = CommonDefs.GetComponent_GameObject_Type(go, typeof(CHongLvTemple))
                temple:Init(data.TempleList[i])
                i = i + 1
            end
        end
        this.m_Table:Reposition()
    end

    this.dismissHintLabel.text = LocalString.GetString("未报名")

    UIEventListener.Get(this.moreInfo).onClick = MakeDelegateFromCSFunction(this.OnMoreInfoClicked, VoidDelegate, this)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    this.applyButton.OnClick = MakeDelegateFromCSFunction(this.OnSignUp, MakeGenericClass(Action1, QnButton), this)
    --查询报名信息
    Gac2Gas.QueryHongLvZuoZhanSignUpInfo()
end
