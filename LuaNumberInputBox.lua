local NGUIText = import "NGUIText"
local QnButton=import "L10.UI.QnButton"
local QnNumberInput=import "L10.UI.QnNumberInput"

CLuaNumberInputBox = class()
RegistClassMember(CLuaNumberInputBox,"textLabel")
RegistClassMember(CLuaNumberInputBox,"okButton")
RegistClassMember(CLuaNumberInputBox,"cancelButton")

RegistClassMember(CLuaNumberInputBox,"m_IncreaseButton")
RegistClassMember(CLuaNumberInputBox,"m_DecreaseButton")
RegistClassMember(CLuaNumberInputBox,"m_ValueInput")
RegistClassMember(CLuaNumberInputBox,"m_CurrentValue")
RegistClassMember(CLuaNumberInputBox,"m_MaxValue")
RegistClassMember(CLuaNumberInputBox,"m_MinValue")
RegistClassMember(CLuaNumberInputBox,"m_Step")


RegistClassMember(CLuaNumberInputBox,"maxContentWidth")
RegistClassMember(CLuaNumberInputBox,"m_MaxValueLabel")
RegistClassMember(CLuaNumberInputBox,"content")
RegistClassMember(CLuaNumberInputBox,"countDown")
RegistClassMember(CLuaNumberInputBox,"m_CountDownTick")

function CLuaNumberInputBox:Awake()
    self.m_MaxValue=99
    self.m_MinValue=1
    self.m_Step = 1
    self.m_CurrentValue = 0

    self.textLabel = self.transform:Find("Anchor/TextLabel"):GetComponent(typeof(UILabel))
    self.okButton = self.transform:Find("Anchor/OKButton").gameObject
    self.cancelButton = self.transform:Find("Anchor/CancelButton").gameObject

    self.m_IncreaseButton = self.transform:Find("Anchor/QnIncreseAndDecreaseButton/IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_DecreaseButton = self.transform:Find("Anchor/QnIncreseAndDecreaseButton/DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_ValueInput = self.transform:Find("Anchor/QnIncreseAndDecreaseButton/QnNumberInput"):GetComponent(typeof(QnNumberInput))

    self.maxContentWidth = 692
    self.m_MaxValueLabel = self.transform:Find("Anchor/Label (1)"):GetComponent(typeof(UILabel))
    self.content = ""
    self.countDown = -1
    self.m_CountDownTick = nil


    UIEventListener.Get(self.okButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if CLuaNumberInputMgr.OnCompleteInput then
            CLuaNumberInputMgr.OnCompleteInput(tonumber(self.m_ValueInput.Text))
        end
        CUIManager.CloseUI(CIndirectUIResources.NumberInputBox)
    end)
    UIEventListener.Get(self.cancelButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CIndirectUIResources.NumberInputBox)
    end)

    self.m_IncreaseButton.OnClick = DelegateFactory.Action_QnButton(function(button)
        if self.m_CurrentValue >= self.m_MaxValue then
            g_MessageMgr:ShowMessage("REACHED_MAXLIMIT")
            return
        end
        self.m_CurrentValue = self.m_CurrentValue + self.m_Step
        if self.m_CurrentValue > self.m_MaxValue then
            g_MessageMgr:ShowMessage("REACHED_MAXLIMIT")
            self.m_CurrentValue = self.m_MaxValue
        end
        self.m_ValueInput:ForceSetText(tostring(self.m_CurrentValue), false)
        self:UpdateButtonStatus()
    end)

    self.m_DecreaseButton.OnClick = DelegateFactory.Action_QnButton(function(button)
        if self.m_CurrentValue <= self.m_MinValue then
            return
        end
        if self.m_CurrentValue <= self.m_Step then
            self.m_CurrentValue = self.m_MinValue
        else
            self.m_CurrentValue = self.m_CurrentValue - self.m_Step
            if self.m_CurrentValue < self.m_MinValue then
                self.m_CurrentValue = self.m_MinValue
            end
        end
        self.m_ValueInput:ForceSetText(tostring(self.m_CurrentValue), false)
        self:UpdateButtonStatus()
    end)

    self.m_ValueInput.OnValueChanged = DelegateFactory.Action_string(function(v)
        if v and v~="" then
            local val = tonumber(v)
            if val>self.m_MaxValue then
                g_MessageMgr:ShowMessage("REACHED_MAXLIMIT")
                val = self.m_MaxValue
            end
            self:SetValueForInternal(val,true)
        end
    end)
    self.m_ValueInput.OnMaxValueButtonClick = DelegateFactory.Action(function()
        self:SetValueForInternal(self.m_MaxValue)
    end)
    self.m_ValueInput.OnKeyboardClosed = DelegateFactory.Action(function()
        if self.m_CurrentValue < self.m_MinValue then
            self.m_CurrentValue = self.m_MinValue
        end
        if self.m_CurrentValue > self.m_MaxValue then
            self.m_CurrentValue = self.m_MaxValue
        end
        self.m_ValueInput:ForceSetText(tostring(self.m_CurrentValue), false)
        self:UpdateButtonStatus()
    end)

end

function CLuaNumberInputBox:SetValueForInternal(value, invokeCallBack) 
    self.m_CurrentValue = value
    self.m_ValueInput:ForceSetText(tostring(self.m_CurrentValue), false)

    self:UpdateButtonStatus()
end

function CLuaNumberInputBox:UpdateButtonStatus()
    self.m_DecreaseButton.Enabled = not(self.m_CurrentValue<=self.m_MinValue)
    self.m_IncreaseButton.Enabled = not(self.m_CurrentValue>=self.m_MaxValue)

    if self.m_MinValue>=self.m_MaxValue then
        self.m_ValueInput.Editable = false
    end
end

function CLuaNumberInputBox:SetMinMax( minv, maxv, step)
    self.m_MinValue = minv
    self.m_MaxValue = maxv
    self.m_Step = step
    local dirty = false
    if self.m_CurrentValue < minv then
        self.m_CurrentValue = minv
        dirty = true
    elseif self.m_CurrentValue > maxv then
        self.m_CurrentValue = maxv
        dirty = true
    end
    if dirty then
        self.m_ValueInput:ForceSetText(tostring(self.m_CurrentValue), false)
    end
    self:UpdateButtonStatus()
end
function CLuaNumberInputBox:SetValue(value, invokeCallBack)
    if value < self.m_MinValue then
        value = self.m_MinValue
    end
    if value > self.m_MaxValue then
        value = self.m_MaxValue
    end
    self.m_CurrentValue = value
    self.m_ValueInput:ForceSetText(tostring(self.m_CurrentValue), false)

    self:UpdateButtonStatus()
end

function CLuaNumberInputBox:Init( )
    self.content = CLuaNumberInputMgr.Text
    self.textLabel.text = self.content
    self.textLabel:UpdateNGUIText()
    NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(self.content)
    self.textLabel.width = size.x < self.maxContentWidth and math.ceil(size.x) or self.maxContentWidth
    --必须向上取整，不然会出现错误的文本折断
    Extensions.SetLocalPositionX(self.textLabel.transform, - self.textLabel.localSize.x * 0.5)
    self.m_MaxValueLabel.text = tostring(CLuaNumberInputMgr.MaxVal)
    self:SetMinMax(CLuaNumberInputMgr.MinVal, CLuaNumberInputMgr.MaxVal, 1)
    self:SetValue(CLuaNumberInputMgr.DefaultVal, true)


    self.countDown = CLuaNumberInputMgr.CountDown
    if self.countDown and self.countDown > 0 then
        self:DestroyTick()
        self.m_CountDownTick = RegisterTick(function () 
            self.textLabel.text =SafeStringFormat3(LocalString.GetString("%s\n[00ff00]剩余关闭时间%d秒[-]"), self.content, self.countDown)
            self.countDown = self.countDown - 1
            if self.countDown < 0 then
                CUIManager.CloseUI(CIndirectUIResources.NumberInputBox)
                self:DestroyTick()
            end
        end, 1000)
    end
end
function CLuaNumberInputBox:DestroyTick( )
    if self.m_CountDownTick ~= nil then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick = nil
    end
end


CLuaNumberInputMgr = {}

CLuaNumberInputMgr.MinVal = 1
CLuaNumberInputMgr.MaxVal = 1
CLuaNumberInputMgr.DefaultVal =1
CLuaNumberInputMgr.OnCompleteInput = 1
CLuaNumberInputMgr.Title = nil
CLuaNumberInputMgr.Text = nil
CLuaNumberInputMgr.CountDown = -1

function CLuaNumberInputMgr.ShowNumInputBox(minVal, maxVal, defalutVal, onCompleteInput, text, countdown) 
    CLuaNumberInputMgr.MinVal = minVal
    CLuaNumberInputMgr.MaxVal = maxVal
    CLuaNumberInputMgr.OnCompleteInput = onCompleteInput
    CLuaNumberInputMgr.Title = nil
    CLuaNumberInputMgr.Text = text
    CLuaNumberInputMgr.CountDown = countdown
    CLuaNumberInputMgr.DefaultVal = defalutVal
    CUIManager.ShowUI(CIndirectUIResources.NumberInputBox)
end
