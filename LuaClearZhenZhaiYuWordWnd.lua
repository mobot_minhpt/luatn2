local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaClearZhenZhaiYuWordWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "Fish", "Fish", GameObject)
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "AddSprite", "AddSprite", GameObject)
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "FishIcon", "FishIcon", CUITexture)
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "WordLabel", "WordLabel", UILabel)
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "FishBorder", "FishBorder", UISprite)
RegistChildComponent(LuaClearZhenZhaiYuWordWnd, "ClearBtn", "ClearBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaClearZhenZhaiYuWordWnd, "m_SelectedFish")

function LuaClearZhenZhaiYuWordWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Fish.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFishClick()
	end)


	
	UIEventListener.Get(self.ClearBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearBtnClick()
	end)


    --@endregion EventBind end
end

function LuaClearZhenZhaiYuWordWnd:OnEnable()
	g_ScriptEvent:AddListener("SelectFishForClearing", self, "OnSelectFishForClearing")
end

function LuaClearZhenZhaiYuWordWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SelectFishForClearing", self, "OnSelectFishForClearing")
end

function LuaClearZhenZhaiYuWordWnd:Init()
	self.FishIcon.gameObject:SetActive(self.m_SelectedFish)
	self.AddSprite.gameObject:SetActive(not self.m_SelectedFish)
end

function LuaClearZhenZhaiYuWordWnd:OnSelectFishForClearing(fish)
	self.m_SelectedFish = fish
	self.FishIcon.gameObject:SetActive(self.m_SelectedFish)
	self.AddSprite.gameObject:SetActive(not self.m_SelectedFish)

	if fish then
		local itemId = fish.itemId
		local data = Item_Item.GetData(itemId)
		if  data then
			self.FishIcon:LoadMaterial(data.Icon)
			self.NameLabel.text = data.Name
			self.FishBorder.spriteName = CUICommonDef.GetItemCellBorder(data.NameColor)
		end
		local wordId = fish.wordId
		if wordId and wordId ~= 0 then
			self.WordLabel.text = Word_Word.GetData(wordId).Description
		else
			self.WordLabel.text = nil
		end

		local fishData = HouseFish_AllFishes.GetData(itemId)
		if fishData then
			self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),fishData.Level)
		end
	end

end

--@region UIEvent

function LuaClearZhenZhaiYuWordWnd:OnFishClick()
	LuaZhenZhaiYuMgr.OpenFishCreelForClearing()
end

function LuaClearZhenZhaiYuWordWnd:OnClearBtnClick()
	if not self.m_SelectedFish then
		return
	end
	local fish = self.m_SelectedFish
	local fishname = Item_Item.GetData(fish.itemId)
	if not fishname then 
		return
	end
	fishname = fishname.Name
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Clear_Zhenzhaiyu_Word_Comfirm",fishname), 
        DelegateFactory.Action(function ()			           
            local fishmapId = fish.fishmapId
			local consumeItemPos = LuaZhenZhaiYuMgr.m_ClearConsumeItemPos
    		local consumeItemId = LuaZhenZhaiYuMgr.m_ClearConsumeItemId
			if fishmapId then
				Gac2Gas.RequestClearZhengzhaiFishEffect(consumeItemPos, consumeItemId, fishmapId)--consumeItemPos, consumeItemId, fishId
			else
				Gac2Gas.RequestClearZhengzhaiFishEffectInBag(consumeItemPos, consumeItemId,fish.bagPos,fish.itemId)--consumeItemPos, consumeItemId, fishItemPos, fishItemId
			end
			CUIManager.CloseUI(CLuaUIResources.ClearZhenZhaiYuWordWnd)
        end),
    nil,
    LocalString.GetString("前往"), LocalString.GetString("取消"), false)

end


--@endregion UIEvent

