require("3rdParty/ScriptEvent")
require("common/common_include")
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local LuaGameObject=import "LuaGameObject"

local Gac2Gas=import "L10.Game.Gac2Gas"
CLuaTowerDefenseStatusWnd=class()
RegistClassMember(CLuaTowerDefenseStatusWnd,"m_ItemTemplate")
RegistClassMember(CLuaTowerDefenseStatusWnd,"m_Grid")
-- RegistClassMember(CLuaTowerDefenseStatusWnd,"m_ClostBtn")--关闭

function CLuaTowerDefenseStatusWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI("TowerDefenseStatusWnd") 
    end)

    self.m_ItemTemplate=LuaGameObject.GetChildNoGC(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_Grid=LuaGameObject.GetChildNoGC(self.transform,"Grid").gameObject
        
    --请求数据
    Gac2Gas.TowerRequestPlayPlayerInfoList(" ")
end

function CLuaTowerDefenseStatusWnd:OnEnable()
     g_ScriptEvent:AddListener("TowerDefenseSyncPlayPlayerInfoList", self, "OnTowerDefenseSyncPlayPlayerInfoList")
end

function CLuaTowerDefenseStatusWnd:OnDisable()
     g_ScriptEvent:RemoveListener("TowerDefenseSyncPlayPlayerInfoList", self, "OnTowerDefenseSyncPlayPlayerInfoList")
    -- g_ScriptEvent:RemoveListener("GetLingShouOtherDetails", self, "OnGetLingShouOtherDetails")

end

function CLuaTowerDefenseStatusWnd:OnTowerDefenseSyncPlayPlayerInfoList(args)
    local myId=0
    if CClientMainPlayer.Inst then
        myId=CClientMainPlayer.Inst.Id
    end

    CUICommonDef.ClearTransform(self.m_Grid.transform)
    local gridCmp=LuaGameObject.GetChildNoGC(self.transform,"Grid").grid
    local array=args[0]
    local count=array.Length/4

    local tempT={}
    for i=0,count-1 do
        table.insert(tempT,{array[i*4+0],array[i*4+1] or " ",array[i*4+2],array[i*4+3]})
    end
    local function compare( a, b ) 
        if a[3]<b[3] then
            return false
        elseif a[3]>b[3] then
            return true
        elseif a[4]>b[4] then
            return true
        elseif a[4]<b[4] then
            return false
        else
            return a[1]<b[1]
        end
    end
    table.sort( tempT, compare ) 
    for i=1,#tempT do
        local playerId=tempT[i][1]
        local playerName=tempT[i][2]
        local score=tempT[i][3]
        local gold=tempT[i][4]

        local go=NGUITools.AddChild(self.m_Grid,self.m_ItemTemplate)
        go:SetActive(true)

        local nameLabel=LuaGameObject.GetChildNoGC(go.transform,"NameLabel").label
        local valueLabel=LuaGameObject.GetChildNoGC(go.transform,"ValueLabel").label
        local scoreLabel=LuaGameObject.GetChildNoGC(go.transform,"ScoreLabel").label

        if myId==playerId then
            nameLabel.text=SafeStringFormat3("[c][009e04]%s[-][/c]",playerName)
            valueLabel.text=SafeStringFormat3("[c][009e04]%d[-][/c]",gold)--tostring(gold)
            scoreLabel.text=SafeStringFormat3("[c][009e04]%d[-][/c]",score)--tostring(score)
        else
            nameLabel.text=SafeStringFormat3("[c][112C4C]%s[-][/c]",playerName)
            valueLabel.text=SafeStringFormat3("[c][112C4C]%d[-][/c]",gold)--tostring(gold)
            scoreLabel.text=SafeStringFormat3("[c][112C4C]%d[-][/c]",score)--tostring(score)
        end
        if i%2==0 then
            LuaGameObject(go.transform).sprite.spriteName="common_textbg_02_dark"
        else
            LuaGameObject(go.transform).sprite.spriteName="common_textbg_02_light"
        end
    end
    gridCmp:Reposition()
end
function CLuaTowerDefenseStatusWnd:OnDestroy()
    -- collectgarbage("collect")
end
return CLuaTowerDefenseStatusWnd