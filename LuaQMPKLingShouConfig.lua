local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local CUITexture=import "L10.UI.CUITexture"
local UICamera = import "UICamera"
local TouchPhase = import "UnityEngine.TouchPhase"
local SoundManager = import "SoundManager"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local LingShouOverview = import "L10.Game.CLingShouBaseMgr+LingShouOverview"
local Input = import "UnityEngine.Input"
local Extensions = import "Extensions"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CLingShouSkillSlot = import "L10.UI.CLingShouSkillSlot"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouItem = import "L10.UI.CLingShouItem"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local Application = import "UnityEngine.Application"
local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"
local CQMPKTitleTemplate=import "L10.UI.CQMPKTitleTemplate"
local QnRadioBox=import "L10.UI.QnRadioBox"
local LingShouDetails=import "L10.Game.CLingShouBaseMgr+LingShouDetails"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

CLuaQMPKLingShouConfig = class()
RegistClassMember(CLuaQMPKLingShouConfig,"m_ModelLoader")
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingShouSkills")
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingShouSkillSlot")--在lua端重新实现灵兽技能栏功能，弃用CLingShouSkills.cs及CLingShouSkillSlot.cs
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingShouSkillSlotArray")
RegistClassMember(CLuaQMPKLingShouConfig, "m_LingShouSkillOutlineArray")
RegistClassMember(CLuaQMPKLingShouConfig,"m_ConfirmBtn")
RegistClassMember(CLuaQMPKLingShouConfig,"m_AllSkillsGrid")
RegistClassMember(CLuaQMPKLingShouConfig,"m_SkillObj")
RegistClassMember(CLuaQMPKLingShouConfig,"m_MovingItemObj")
RegistClassMember(CLuaQMPKLingShouConfig,"m_MovingSkill")
RegistClassMember(CLuaQMPKLingShouConfig,"m_NatureChooseObj")
RegistClassMember(CLuaQMPKLingShouConfig,"m_TemplateChooseObj")
RegistClassMember(CLuaQMPKLingShouConfig,"m_ChoosenPetRoot")
RegistClassMember(CLuaQMPKLingShouConfig,"m_ChoosenPetGrid")
RegistClassMember(CLuaQMPKLingShouConfig,"m_PetTemplateObj")
RegistClassMember(CLuaQMPKLingShouConfig,"m_ChoosenBg")
RegistClassMember(CLuaQMPKLingShouConfig,"m_SkillTypeRadioBox")
RegistClassMember(CLuaQMPKLingShouConfig,"m_TitleLabel")
RegistClassMember(CLuaQMPKLingShouConfig,"m_NatureStrList")
RegistClassMember(CLuaQMPKLingShouConfig,"m_PetSkillList")
RegistClassMember(CLuaQMPKLingShouConfig,"m_IsInited")
RegistClassMember(CLuaQMPKLingShouConfig,"m_ChoosenPetList")
RegistClassMember(CLuaQMPKLingShouConfig,"m_ChoosenPetItems")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentSkillType")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentLingShouId")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentLingShouTemplateId")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentLingShouNature")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentLingShouType")
RegistClassMember(CLuaQMPKLingShouConfig,"m_HasSkillIdList")
RegistClassMember(CLuaQMPKLingShouConfig,"m_AllSkillObjs")
RegistClassMember(CLuaQMPKLingShouConfig,"m_AllSkillIds")
RegistClassMember(CLuaQMPKLingShouConfig,"dragging")
RegistClassMember(CLuaQMPKLingShouConfig,"fingerIndex")
RegistClassMember(CLuaQMPKLingShouConfig,"srcIndex")
RegistClassMember(CLuaQMPKLingShouConfig,"lastPos")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentDragSkillId")

RegistClassMember(CLuaQMPKLingShouConfig,"m_RadioBox")
RegistClassMember(CLuaQMPKLingShouConfig,"m_SkillRoot")
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingfuRoot")
RegistClassMember(CLuaQMPKLingShouConfig,"m_Skill2GoLookup")
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingfuWordChoiceTbl")

--结伴技能
RegistClassMember(CLuaQMPKLingShouConfig,"m_MateSkillTypeRadioBox")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentMateSkillType")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentRadioBoxState")
RegistClassMember(CLuaQMPKLingShouConfig,"m_HasMateSkillIdList")

RegistClassMember(CLuaQMPKLingShouConfig,"m_LingfuIndexTbl")
RegistClassMember(CLuaQMPKLingShouConfig,"m_CurrentLingfuTitleIndex")
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingfuTitleTbl")

--他人配置
RegistClassMember(CLuaQMPKLingShouConfig,"m_TipLab")
RegistClassMember(CLuaQMPKLingShouConfig,"m_ConfirmBtnLab")
RegistClassMember(CLuaQMPKLingShouConfig,"m_IsOtherPlayer")
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingShouDetailsList")
RegistClassMember(CLuaQMPKLingShouConfig,"m_IgnoreNextGetDetail")
RegistClassMember(CLuaQMPKLingShouConfig,"m_HasChange")
RegistClassMember(CLuaQMPKLingShouConfig,"m_LingShouTbl")
function CLuaQMPKLingShouConfig:Awake()
    self.m_IsOtherPlayer = CLuaQMPKMgr.s_IsOtherPlayer

    self.m_ModelLoader = self.transform:Find("Main/Texture"):GetComponent(typeof(CLingShouModelTextureLoader))
    self.m_LingShouSkills = self.transform:Find("Skill/Slots"):GetComponent(typeof(UIGrid))
    self.m_LingShouSkillSlot = self.m_LingShouSkills.transform:Find("1").gameObject
    self.m_ConfirmBtn = self.transform:Find("ConfirmBtn").gameObject
    self.m_AllSkillsGrid = self.transform:Find("Skill/AllSkill/Bg/Slots"):GetComponent(typeof(UIGrid))
    -- self.m_SkillObj = self.transform:Find("Skill/Slots/1").gameObject
    self.m_MovingItemObj = self.transform:Find("Skill/MovingItem").gameObject
    self.m_MovingSkill = self.transform:Find("Skill/MovingItem/1"):GetComponent(typeof(CLingShouSkillSlot))
    self.m_NatureChooseObj = self.transform:Find("Main/Nature"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_TemplateChooseObj = self.transform:Find("Main/Template"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_ChoosenPetRoot = self.transform:Find("LingShouSelector").gameObject
    self.m_ChoosenPetGrid = self.transform:Find("LingShouSelector/ScrollView/Table"):GetComponent(typeof(UIGrid))
    self.m_PetTemplateObj = self.transform:Find("LingShouTable/Pool/LingShouItem").gameObject
    self.m_ChoosenBg = self.transform:Find("LingShouSelector/Bg"):GetComponent(typeof(UISprite))
    self.m_SkillTypeRadioBox = self.transform:Find("Skill/TypeRadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_TitleLabel = self.transform:Find("Main/Label (1)"):GetComponent(typeof(UILabel))
    self.m_LingShouTbl = self.transform:Find("LingShouTable"):GetComponent(typeof(CCommonLuaScript))

    --结伴技能
    self.m_MateSkillTypeRadioBox = self.transform:Find("Skill/MateTypeRadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_CurrentRadioBoxState = 0

    self.m_SkillRoot=FindChild(self.transform,"Skill").gameObject
    self.m_SkillObj=FindChild(self.m_SkillRoot.transform,"SkillTemplate").gameObject
    self.m_SkillObj:SetActive(false)
    self.m_LingfuRoot=FindChild(self.transform,"Lingfu").gameObject
    self.m_RadioBox = self.transform:Find("RadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_RadioBox.OnSelect =DelegateFactory.Action_QnButton_int(function(btn,index)
        self.m_CurrentRadioBoxState = index --0代表出战技能，1代表结伴技能
        if index==0 then
            self.m_SkillRoot:SetActive(true)
            self.m_SkillTypeRadioBox.gameObject:SetActive(true)
            self.m_MateSkillTypeRadioBox.gameObject:SetActive(false)
            self.m_LingfuRoot:SetActive(false)
            self:ResetAllSkills()
            self:ResetLingShouSkills()
        elseif index==1 then
            self.m_SkillRoot:SetActive(true)
            self.m_SkillTypeRadioBox.gameObject:SetActive(false)
            self.m_MateSkillTypeRadioBox.gameObject:SetActive(true)
            self.m_LingfuRoot:SetActive(false)
            self:ResetAllSkills()
            self:ResetLingShouSkills()
        else
            self.m_SkillRoot:SetActive(false)
            --self.m_MateSkillRoot:SetActive(false)
            self.m_LingfuRoot:SetActive(true)
            --无奈
            local tableTf=FindChild(self.m_LingfuRoot.transform,"Table")
            for i=1,tableTf.childCount do
                tableTf:GetChild(i-1):Find("Words"):GetComponent(typeof(UITable)):Reposition()
            end
            tableTf:GetComponent(typeof(UITable)):Reposition()
        end
    end)

    self.m_TipLab = self.transform:Find("Skill/Label"):GetComponent(typeof(UILabel))
    self.m_ConfirmBtnLab = self.transform:Find("ConfirmBtn/Label"):GetComponent(typeof(UILabel))

    self.m_NatureStrList = {}
    table.insert( self.m_NatureStrList, LocalString.GetString("英勇"))
    table.insert( self.m_NatureStrList, LocalString.GetString("聪慧"))
    table.insert( self.m_NatureStrList, LocalString.GetString("倔强"))
    table.insert( self.m_NatureStrList, LocalString.GetString("谨慎"))
    table.insert( self.m_NatureStrList, LocalString.GetString("忠心"))

    --self.m_PetSkillList = ?
    self.m_IsInited = false
    --self.m_ChoosenPetList = ?
    --self.m_ChoosenPetItems = ?
    --self.m_CurrentSkillType = ?
    --self.m_CurrentLingShouId = ?
    --self.m_CurrentLingShouTemplateId = ?
    --self.m_CurrentLingShouNature = ?
    --self.m_CurrentLingShouType = ?
    --self.m_HasSkillIdList = ?
    --self.m_AllSkillObjs = ?
    --self.m_AllSkillIds = ?
    self.dragging = false
    self.fingerIndex = -1
    self.srcIndex = -1
    --self.lastPos = ?
    self.m_CurrentDragSkillId = 0
    self.m_HasChange = false
end

-- Auto Generated!!


function CLuaQMPKLingShouConfig:OnEnable( )
    g_ScriptEvent:AddListener("SelectLingShouAtRow", self, "OnSelectLingShouAtRow")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "OnGetLingShouDetails")
    g_ScriptEvent:AddListener("SelectOtherPlayerLingShouAtRow", self, "OnSelectOtherPlayerLingShouAtRow")
    g_ScriptEvent:AddListener("QMPKLingShouLearn", self, "OnQMPKLingShouLearn")
    if self.m_LingShouTbl and self.m_LingShouTbl.m_LuaSelf then
        self.m_LingShouTbl.m_LuaSelf.lingShouTable:ReloadData(false, false)
        self.m_LingShouTbl.m_LuaSelf.lingShouTable:SetSelectRow(0, true)
    end
    if self.m_IsInited then
        return
    end
    self:InitLingShouSkillSlots()
    self.m_IsInited = true
    self:Init()
    self.m_IgnoreNextGetDetail = false

end
function CLuaQMPKLingShouConfig:OnDisable()
    g_ScriptEvent:RemoveListener("SelectLingShouAtRow", self, "OnSelectLingShouAtRow")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "OnGetLingShouDetails")
    g_ScriptEvent:RemoveListener("SelectOtherPlayerLingShouAtRow", self, "OnSelectOtherPlayerLingShouAtRow")
    g_ScriptEvent:RemoveListener("QMPKLingShouLearn", self, "OnQMPKLingShouLearn")
end

function CLuaQMPKLingShouConfig:OnDestroy()

    self:DestroyLingShouSkillSlots()
end

function CLuaQMPKLingShouConfig:OnSelectLingShouAtRow(row)
    -- local row=args[0]
    local endFunc = function() self:SelectLingShouAtRow(row) end
    self:OnPageCloseOrChange(endFunc)
    
end
function CLuaQMPKLingShouConfig:OnSelectOtherPlayerLingShouAtRow(row)
    self:SelectOtherPlayerLingShouAtRow(row)
end
function CLuaQMPKLingShouConfig:OnGetLingShouDetails(args)
    if self.m_IgnoreNextGetDetail then
        self.m_IgnoreNextGetDetail = false
        return
    end
    local id=args[0]
    local details=args[1]
    self:GetLingShouDetails(id, details)
end

function CLuaQMPKLingShouConfig:Init( )
    self.m_ChoosenPetList ={}-- CreateFromClass(MakeGenericClass(List, CQMPKPet))
    self.m_ChoosenPetItems ={}-- CreateFromClass(MakeGenericClass(List, GameObject))
    self.m_CurrentSkillType = 0
    self.m_HasSkillIdList ={}-- CreateFromClass(MakeGenericClass(List, UInt32))
    self.m_AllSkillObjs ={}-- CreateFromClass(MakeGenericClass(List, GameObject))
    self.m_AllSkillIds ={}-- CreateFromClass(MakeGenericClass(List, CQMPKPetSkill))
    self.fingerIndex = - 1
    self.srcIndex = - 1
    --结伴技能
    self.m_CurrentMateSkillType = 4
    self.m_HasMateSkillIdList = {}

    UIEventListener.Get(self.m_ConfirmBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_IsOtherPlayer then
            CUIManager.ShowUI(CLuaUIResources.QMPKLingShouLearnWnd)
        else
            self:OnConfirmBtnClick(go,nil)
        end
    end)
    UIEventListener.Get(self.m_NatureChooseObj.gameObject).onClick =DelegateFactory.VoidDelegate(function(go)
        self:OnNatureClick(go)
    end)
    UIEventListener.Get(self.m_TemplateChooseObj.gameObject).onClick =DelegateFactory.VoidDelegate(function(go)
        self:OnTemplateClick(go)
    end)
    self.m_SkillTypeRadioBox.OnSelect =DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSkillTypeSelected(btn, index)
    end)
    self.m_MateSkillTypeRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSkillTypeSelected(btn, index + 4)
    end)

    self.m_PetSkillList ={}
    do
        local keys ={}
        QuanMinPK_LingShouSkill.ForeachKey(function (key)
            table.insert( keys,key )
        end)
        for i,v in ipairs(keys) do
            local data = QuanMinPK_LingShouSkill.GetData(v)
            local skillType = data.SkillType		--低4类为出战技能,高4类为结伴技能
            if not self.m_PetSkillList[skillType] then
                self.m_PetSkillList[skillType] = {}
            end
            table.insert( self.m_PetSkillList[skillType], {
                m_SkillId = v,
                m_Level = data.Level,
                m_Type = data.Type,
                m_Category = skillType
            } )
        end
    end

    do
        local keys ={}
        QuanMinPK_LingShou.ForeachKey(function (key)
            table.insert( keys,key )
        end)

        for i,v in ipairs(keys) do
            local data = QuanMinPK_LingShou.GetData(v)
            table.insert( self.m_ChoosenPetList,{
                m_TemplateId = data.Template,
                m_Level = data.Level,
                m_Name = data.InitName,
                m_EvolveGrade = data.EvolveGrade,
                m_Type = data.Type} )
        end
    end


    for i,v in ipairs(self.m_ChoosenPetList) do
        local overview = CreateFromClass(LingShouOverview)
        overview.templateId = v.m_TemplateId
        overview.name = v.m_Name
        overview.level = v.m_Level
        overview.evolveGrade = v.m_EvolveGrade
        local go = NGUITools.AddChild(self.m_ChoosenPetGrid.gameObject, self.m_PetTemplateObj)
        go:SetActive(true)
        local item = go:GetComponent(typeof(CLingShouItem))
        item:Init(overview)

        table.insert( self.m_ChoosenPetItems,go )
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnTemplateSelected(go)
        end)
    end

    self.m_ChoosenPetGrid:Reposition()
    self.m_ChoosenPetRoot:SetActive(false)
    if self.m_IsOtherPlayer then
        self:InitOtherPlayerConfig()
    end
end
function CLuaQMPKLingShouConfig:OnSkillTypeSelected( btn, index)
    if(index < 4)then
        self.m_CurrentSkillType = index
    else
        self.m_CurrentMateSkillType = index
    end
    self:ResetAllSkills()
end
function CLuaQMPKLingShouConfig:OnNatureClick( go)
    if self.m_IsOtherPlayer then
        return 
    end

    local popupList ={}

    for i,v in ipairs(self.m_NatureStrList) do
        local data = PopupMenuItemData(v,
            DelegateFactory.Action_int(function(index)
                self:OnNatureSelected(index)
            end),
            false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( popupList,data )
    end

    self.m_NatureChooseObj:Expand(true)
    CPopupMenuInfoMgr.ShowPopupMenu(
        Table2Array(popupList,MakeArrayClass(PopupMenuItemData)),
        go.transform,
        CPopupMenuInfoMgr.AlignType.Bottom,
        1,
        DelegateFactory.Action(function () self.m_NatureChooseObj:Expand(false) end),
        600,
        true,
        296)
end
function CLuaQMPKLingShouConfig:OnNatureSelected( index)
    self.m_NatureChooseObj:Init(self.m_NatureStrList[index+1])
    if self.m_CurrentLingShouNature ~= index + 1 then self.m_HasChanged = true end  -- 灵兽性格
    self.m_CurrentLingShouNature = index + 1
end
function CLuaQMPKLingShouConfig:OnTemplateClick( go)
    if self.m_IsOtherPlayer then
        return
    end

    self.m_TemplateChooseObj:Expand(true)
    self.m_ChoosenPetRoot:SetActive(true)
end
function CLuaQMPKLingShouConfig:OnTemplateSelected( go)

    for i,v in ipairs(self.m_ChoosenPetItems) do
        if v==go then
            self.m_TemplateChooseObj:Init(self.m_ChoosenPetList[i].m_Name)
            if self.m_CurrentLingShouTemplateId ~= self.m_ChoosenPetList[i].m_TemplateId then self.m_HasChanged = true end  -- 灵兽类型
            self.m_CurrentLingShouTemplateId = self.m_ChoosenPetList[i].m_TemplateId
            break
        end
    end

    self.m_TemplateChooseObj:Expand(false)
    self.m_ChoosenPetRoot:SetActive(false)

    local evolveGrade = 1
    for i,v in ipairs(self.m_ChoosenPetList) do
        if v.m_TemplateId==self.m_CurrentLingShouTemplateId then
            self.m_CurrentLingShouType = self.m_ChoosenPetList[i].m_Type
            evolveGrade = self.m_ChoosenPetList[i].m_EvolveGrade
            break
        end
    end

    self:CheckAndReplaceLingShouSkill()
    self:ResetAllSkills()
    self.m_TitleLabel.text = CLingShouBaseMgr.GetLingShouTypeTempalteDesc(self.m_CurrentLingShouTemplateId)
    self.m_ModelLoader:Init(self.m_CurrentLingShouTemplateId, 0, evolveGrade)
end
function CLuaQMPKLingShouConfig:CheckAndReplaceLingShouSkill( )

    for i,v in ipairs(self.m_HasSkillIdList) do
        local skillCls= math.floor(v / 100)
        local data = QuanMinPK_LingShouSkill.GetData(skillCls)
        if not data or not data.Type or data.OpposeSkill<=0 then
        else
            local has=false
            for j=1,data.Type.Length do
                if data.Type[j-1]==self.m_CurrentLingShouType then
                    has=true
                    break
                end
            end
            if not has then
                self.m_HasSkillIdList[i] = data.OpposeSkill * 100 + QuanMinPK_LingShouSkill.GetData(data.OpposeSkill).Level
            end
        end
    end

    for i,v in ipairs(self.m_HasMateSkillIdList) do
        local skillCls= math.floor(v / 100)
        local data = QuanMinPK_LingShouSkill.GetData(skillCls)
        if not data or not data.Type or data.OpposeSkill<=0 then
        else
            local has=false
            for j=1,data.Type.Length do
                if data.Type[j-1]==self.m_CurrentLingShouType then
                    has=true
                    break
                end
            end
            if not has then
                self.m_HasMateSkillIdList[i] = data.OpposeSkill * 100 + QuanMinPK_LingShouSkill.GetData(data.OpposeSkill).Level
            end
        end
    end
end
function CLuaQMPKLingShouConfig:SelectLingShouAtRow( row)
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    if row < list.Count then
        self.m_CurrentLingShouId = list[row].id
        self.m_CurrentLingShouTemplateId = list[row].templateId
        do
            local i = 0 local cnt = #self.m_ChoosenPetList
            while i < cnt do
                if self.m_ChoosenPetList[i+1].m_TemplateId == self.m_CurrentLingShouTemplateId then
                    self.m_CurrentLingShouType = self.m_ChoosenPetList[i+1].m_Type
                    break
                end
                i = i + 1
            end
        end
        self.m_CurrentLingShouNature = EnumToInt(list[row].nature)
        self.m_NatureChooseObj:Init(self.m_NatureStrList[self.m_CurrentLingShouNature])
        Gac2Gas.RequestLingShouDetails(self.m_CurrentLingShouId, 0)
    else
        self.m_CurrentLingShouId = nil
    end
end
function CLuaQMPKLingShouConfig:SelectOtherPlayerLingShouAtRow(row)
    local playerInfo = CLuaQMPKMgr.s_PlayerInfo
    local lingShouInfos = playerInfo.LingShou
    if row < #lingShouInfos then
        local lingShouInfo = lingShouInfos[row+1]
        self.m_CurrentLingShouId = lingShouInfo.Id
        self.m_CurrentLingShouTemplateId = lingShouInfo.TemplateId
        do
            local i = 0 local cnt = #self.m_ChoosenPetList
            while i < cnt do
                if self.m_ChoosenPetList[i+1].m_TemplateId == self.m_CurrentLingShouTemplateId then
                    self.m_CurrentLingShouType = self.m_ChoosenPetList[i+1].m_Type
                    break
                end
                i = i + 1
            end
        end
        self.m_CurrentLingShouNature = EnumToInt(lingShouInfo.Props.Nature)
        self.m_NatureChooseObj:Init(self.m_NatureStrList[self.m_CurrentLingShouNature])
        self:GetOtherLingShouDetails(self.m_CurrentLingShouId, lingShouInfo)
    else
        self.m_CurrentLingShouId = nil
    end
end
function CLuaQMPKLingShouConfig:GetLingShouDetails( id, details)
    if id ~= self.m_CurrentLingShouId then
        return
    end
    local pet = nil
    do
        local i = 0
        while i < #self.m_ChoosenPetList do
            if self.m_ChoosenPetList[i+1].m_TemplateId == details.data.TemplateId then
                pet = self.m_ChoosenPetList[i+1]
                break
            end
            i = i + 1
        end
    end

    if pet ~= nil then
        self.m_TemplateChooseObj:Init(pet.m_Name)
    end
    self.m_TitleLabel.text = CLingShouBaseMgr.GetLingShouTypeTempalteDesc(details.data.TemplateId)
    self.m_ModelLoader:Init(details)

    --灵兽自有技能初始化
    do
        local list = details.data.Props.SkillListForSave
        self.m_HasSkillIdList={}
        for i=1,list.Length do
            table.insert( self.m_HasSkillIdList,list[i-1] )
        end
    end
    do
        self.m_HasMateSkillIdList = {}
        local list = details.data.Props.PartnerInfo.PartnerNormalSkills
        table.insert(self.m_HasMateSkillIdList,0)
        for i = 1, 5, 1 do  --5个普通结伴技能槽
            table.insert(self.m_HasMateSkillIdList,list[i])
        end
        list = details.data.Props.PartnerInfo.PartnerProfessionalSkills
        for i = 1, 3, 1 do  --3个职业结伴技能槽
            table.insert(self.m_HasMateSkillIdList,list[i] )
        end
        table.insert(self.m_HasMateSkillIdList,0)
    end
    self:ResetLingShouSkills()

    self:ResetAllSkills()

    self:InitLingfu(id,details)
    self:RefreshLearnStatus()
end
function CLuaQMPKLingShouConfig:GetOtherLingShouDetails(lingshouId, lingshou)
    local fightProp = CLingShouMgr.Inst:GetLingShouFightPropByLingShou(lingshou)
    local details = CLuaLingShouOtherMgr.AddLingShouDetails(lingshouId,lingshou,fightProp)
    self:GetLingShouDetails(lingshouId, details)
end
function CLuaQMPKLingShouConfig.AddLingShouDetails(lingshouId,pData,pFightProperty)
    if self.m_LingShouDetailsList == nil then
        self.m_LingShouDetailsList = {}
    end
    local details = self.m_LingShouDetailsList[lingshouId]
    if details then
        details.data = pData
        details.fightProperty = pFightProperty
    else
        details = LingShouDetails()
        details.id = lingshouId
        details.data = pData
        details.fightProperty = pFightProperty
        self.m_LingShouDetailsList[lingshouId] = details
    end
    return details
end

function CLuaQMPKLingShouConfig:ResetLingShouSkills()
    if(self.m_CurrentRadioBoxState == 0)then
        --self.m_LingShouSkills:InitSlots(Table2Array(self.m_HasSkillIdList,MakeArrayClass(UInt32)))
        self:RefreshLingShouSkillSlots(self.m_HasSkillIdList)
    elseif(self.m_CurrentRadioBoxState == 1)then
        --self.m_LingShouSkills:InitSlots(Table2Array(self.m_HasMateSkillIdList,MakeArrayClass(UInt32)))
        self:RefreshLingShouSkillSlots(self.m_HasMateSkillIdList)
    end
end

function CLuaQMPKLingShouConfig:ResetAllSkills( )
    if self.m_CurrentLingShouType == 0 then
        return
    end
    local _skill_type_index = self.m_CurrentSkillType
    if(self.m_CurrentRadioBoxState == 1)then -- 当前为结伴技能状态
        _skill_type_index = self.m_CurrentMateSkillType
    end
    Extensions.RemoveAllChildren(self.m_AllSkillsGrid.transform)
    self.m_AllSkillIds={}
    self.m_AllSkillObjs={}

    self.m_Skill2GoLookup={}
    do
        local i = 0 local cnt = #self.m_PetSkillList[_skill_type_index + 1]
        while i < cnt do
            local goon=true
            local contain=false
            local petSkillInfo=self.m_PetSkillList[_skill_type_index + 1][i + 1]
            if  petSkillInfo.m_Type then
                local array= petSkillInfo.m_Type
                for j=1,array.Length do
                    if array[j-1]==self.m_CurrentLingShouType then
                        contain=true
                        break
                    end
                end
                goon=contain and true or false
            end

            if goon then
                local go = NGUITools.AddChild(self.m_AllSkillsGrid.gameObject, self.m_SkillObj)
                go:SetActive(true)
                table.insert( self.m_AllSkillObjs,go )

                UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function(go) self:OnDragIconStart(go) end)

                local skillId = petSkillInfo.m_SkillId * 100 + petSkillInfo.m_Level
                self:InitSkillSlot(go.transform,skillId)
                table.insert( self.m_AllSkillIds, petSkillInfo)

                self.m_Skill2GoLookup[skillId]=go
            end

            i = i + 1
        end
    end
    self.m_AllSkillsGrid:Reposition()
    self:RefreshLearnStatus()
end

function CLuaQMPKLingShouConfig:InitSkillSlot(transform,skillId)
    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if skillId>0 then
            CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil)
        end
    end)
    local data=Skill_AllSkills.GetData(skillId)
    if not data then return end
    local icon=transform:Find("Icon"):GetComponent(typeof(CUITexture))
    icon:LoadSkillIcon(data.SkillIcon)
    local levelLabel=transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    levelLabel.text="Lv."..tostring(data.Level)

    local learn=transform:Find("Learn").gameObject
    learn:SetActive(false)
    for i,v in ipairs(self.m_HasSkillIdList) do
        if v==skillId then
            learn:SetActive(true)
            break
        end
    end
end

function CLuaQMPKLingShouConfig:OnDragIconStart( go)
    if self.m_IsOtherPlayer then
        return
    end

    do
        local i = 0
        while i < #self.m_AllSkillObjs do
            if self.m_AllSkillObjs[i+1].gameObject == go then
                self.srcIndex = i
                self.m_MovingItemObj:SetActive(true)

                -- clear selected flag
                for k, v in pairs(self.m_LingShouSkillOutlineArray) do
                  v:SetActive(false)
                end

                self.m_CurrentDragSkillId = self.m_AllSkillIds[i+1].m_SkillId * 100 + self.m_AllSkillIds[i+1].m_Level
                self.m_MovingSkill:Init(self.m_CurrentDragSkillId)
                if self.m_AllSkillIds[i+1].m_Category == 3 then
                  self.m_LingShouSkillOutlineArray[1]:SetActive(true)
                elseif self.m_AllSkillIds[i+1].m_Category == 4 then
                  self.m_LingShouSkillOutlineArray[2]:SetActive(true)
                elseif self.m_AllSkillIds[i+1].m_Category == 7 then
                  self.m_LingShouSkillOutlineArray[6]:SetActive(true)
                elseif self.m_AllSkillIds[i+1].m_Category == 8 then
                  self.m_LingShouSkillOutlineArray[7]:SetActive(true)
                  self.m_LingShouSkillOutlineArray[8]:SetActive(true)
                end
                self.dragging = true
                self.lastPos = Input.mousePosition

                if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
                    self.fingerIndex = Input.GetTouch(0).fingerId
                    local pos=Input.GetTouch(0).position
                    self.lastPos = Vector3(pos.x,pos.y,0)
                end
                break
            end
            i = i + 1
        end
    end
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.Skill_Switch, Vector3.zero, nil, 0)
end
function CLuaQMPKLingShouConfig:Update( )
    if self.m_IsOtherPlayer then
        return
    end

    if Input.GetMouseButtonUp(0) then
        if UICamera.lastHit.collider == nil or self.m_TemplateChooseObj.gameObject ~= UICamera.lastHit.collider.gameObject then
            local corners = self.m_ChoosenBg.worldCorners
            local lastPos = UICamera.lastWorldPosition
            if lastPos.x < corners[0].x or lastPos.x > corners[2].x or lastPos.y < corners[0].y or lastPos.y > corners[2].y then
                self.m_TemplateChooseObj:Expand(false)
                self.m_ChoosenPetRoot:SetActive(false)
            end
        end
    end

    if not self.dragging then
        return
    end

    if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
        do
            local i = 0
            while i < Input.touchCount do
                local touch = Input.GetTouch(i)
                if touch.fingerId == self.fingerIndex then
                    if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                        if touch.phase ~= TouchPhase.Stationary then
                            local pos=touch.position
                            pos=Vector3(pos.x,pos.y,0)
                            self.m_MovingItemObj.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(pos)
                            self.lastPos = pos
                        end
                        return
                    else
                        break
                    end
                end
                i = i + 1
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.m_MovingItemObj.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.lastPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end

    self.m_MovingItemObj:SetActive(false)
    -- clear selected flag
    for k, v in pairs(self.m_LingShouSkillOutlineArray) do
      v:SetActive(false)
    end
    self:ProcessHoveredObject(CUICommonDef.SelectedUIWithRacast)

    self.dragging = false
    self.fingerIndex = - 1
    self.srcIndex = - 1
end
function CLuaQMPKLingShouConfig:ProcessHoveredObject( go)
    if(self.m_CurrentRadioBoxState == 0)then --出战技能状态
        local i = 0 local cnt = #self.m_LingShouSkillSlotArray
        while i < cnt do
            if go == self.m_LingShouSkillSlotArray[i + 1].gameObject then
                if (i == 0 and self.m_CurrentSkillType ~= 2) or (i == 1 and self.m_CurrentSkillType ~= 3) or (i ~= 0 and i ~= 1 and self.m_CurrentSkillType ~= 0 and self.m_CurrentSkillType ~= 1) then
                    g_MessageMgr:ShowMessage("QMPK_Change_LingShou_Skill_Type_Incorrect")
                    return
                end

                for i=1,#self.m_HasSkillIdList do
                    if self.m_HasSkillIdList[i]==self.m_CurrentDragSkillId then
                        g_MessageMgr:ShowMessage("QMPK_Already_Has_Skill")
                        return
                    end
                end

                if self.m_HasSkillIdList[i + 2] ~= self.m_CurrentDragSkillId then self.m_HasChanged = true end
                self.m_HasSkillIdList[i + 2] = self.m_CurrentDragSkillId
                --self.m_LingShouSkills:InitSlots(Table2Array(self.m_HasSkillIdList,MakeArrayClass(UInt32)))
                self:RefreshLingShouSkillSlots(self.m_HasSkillIdList)
                self:RefreshLearnStatus()
                break
            end
            i = i + 1
        end
    elseif(self.m_CurrentRadioBoxState == 1)then --结伴技能状态
        local i = 0 local cnt = #self.m_LingShouSkillSlotArray
        while i < cnt do
            if go == self.m_LingShouSkillSlotArray[i + 1].gameObject then
                for i=1,#self.m_HasMateSkillIdList do
                    if self.m_HasMateSkillIdList[i]==self.m_CurrentDragSkillId then
                        g_MessageMgr:ShowMessage("QMPK_Already_Has_Skill")
                        return
                    end
                end
                if ((self.m_CurrentMateSkillType < 6 and i > 4) or (self.m_CurrentMateSkillType == 6 and i ~= 5) or (self.m_CurrentMateSkillType == 7 and i < 6)) then
                    g_MessageMgr:ShowMessage("QMPK_Change_LingShou_Skill_Type_Incorrect")
                    return
                end
                if self.m_HasMateSkillIdList[i + 2] ~= self.m_CurrentDragSkillId then self.m_HasChanged = true end
                self.m_HasMateSkillIdList[i + 2] = self.m_CurrentDragSkillId
                --self.m_LingShouSkills:InitSlots(Table2Array(self.m_HasMateSkillIdList,MakeArrayClass(UInt32)))
                self:RefreshLingShouSkillSlots(self.m_HasMateSkillIdList)
                self:RefreshLearnStatus()
                break
            end
            i = i + 1
        end
    end
end

function CLuaQMPKLingShouConfig:RefreshLearnStatus()
    if not self.m_Skill2GoLookup then return end

    if(self.m_CurrentRadioBoxState == 0)then --出战技能状态
        local lookup={}
        for i,v in ipairs(self.m_HasSkillIdList) do
            lookup[v]=true
        end
        for k,v in pairs(self.m_Skill2GoLookup) do
            local learn=v.transform:Find("Learn").gameObject
            if lookup[k] then
                learn:SetActive(true)
            else
                learn:SetActive(false)
            end
        end
    elseif(self.m_CurrentRadioBoxState == 1)then
        local lookup={}
        for i,v in ipairs(self.m_HasMateSkillIdList) do
            lookup[v]=true
        end
        for k,v in pairs(self.m_Skill2GoLookup) do
            local learn=v.transform:Find("Learn").gameObject
            if lookup[k] then
                learn:SetActive(true)
            else
                learn:SetActive(false)
            end
        end
    end
end

function CLuaQMPKLingShouConfig:OnConfirmBtnClick( go,endFunc)
    local raw = ""
    for i = 1, 8 do
        raw=raw..tostring(math.floor(self.m_HasSkillIdList[i+1] / 100))..";"
    end
    local raw2 = ""
    local has_empty = false
    local has_bubble = false
    for i = 1, 8, 1 do
        if(self.m_HasMateSkillIdList[i+1] ~= 0)then -- 0为非法ID
            raw2 = raw2..tostring(math.floor(self.m_HasMateSkillIdList[i+1] / 100))..";"
            if(has_empty)then
                has_bubble = true
            end
        else
            has_empty = true
        end
    end
    if(has_bubble)then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("尚有未配置的结伴技能"))
        return
    end

    local xiuxiAction
    local typeStr
    if CLingShouMgr.Inst:IsOnAssist(self.m_CurrentLingShouId) then
        typeStr = LocalString.GetString("助战")
        xiuxiAction = function()
            Gac2Gas.RequestSetAssistLingShou("")
        end
    elseif CLingShouMgr.Inst:IsOnJieBan(self.m_CurrentLingShouId) then
        typeStr = LocalString.GetString("结伴")
        xiuxiAction = function()
            Gac2Gas.RequestCancelBindPartner_LingShou(self.m_CurrentLingShouId)
        end
    elseif CLingShouMgr.Inst:IsOnBattle(self.m_CurrentLingShouId) then
        typeStr = LocalString.GetString("出战")
        xiuxiAction = function()
            Gac2Gas.RequestPackLingShou()
        end
    end
    if xiuxiAction then
        local msg = g_MessageMgr:FormatMessage("QMPK_LingShou_Config_XiuXi", typeStr)
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function ()
            self.m_IgnoreNextGetDetail = true
            xiuxiAction()
            Gac2Gas.RequestOperateQmpkLingShouV2(self.m_CurrentLingShouId, self.m_CurrentLingShouTemplateId, self.m_CurrentLingShouNature, raw,
            self.m_LingfuIndexTbl[1],self.m_LingfuIndexTbl[2],self.m_LingfuIndexTbl[3], raw2)
            if endFunc then endFunc() end
            end),DelegateFactory.Action(function() if endFunc then endFunc() end end),LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        Gac2Gas.RequestOperateQmpkLingShouV2(self.m_CurrentLingShouId, self.m_CurrentLingShouTemplateId, self.m_CurrentLingShouNature, raw,
        self.m_LingfuIndexTbl[1],self.m_LingfuIndexTbl[2],self.m_LingfuIndexTbl[3], raw2)
        if endFunc then endFunc() end
    end
    self.m_HasChanged = false
end

function CLuaQMPKLingShouConfig:OnQMPKLingShouLearn(lingshouId)
    local raw = ""
    for i = 1, 8 do
        raw=raw..tostring(math.floor(self.m_HasSkillIdList[i+1] / 100))..";"
    end
    local raw2 = ""
    local has_empty = false
    local has_bubble = false
    for i = 1, 8, 1 do
        if(self.m_HasMateSkillIdList[i+1] ~= 0)then -- 0为非法ID
            raw2 = raw2..tostring(math.floor(self.m_HasMateSkillIdList[i+1] / 100))..";"
            if(has_empty)then
                has_bubble = true
            end
        else
            has_empty = true
        end
    end

    local xiuxiAction
    local typeStr
    if CLingShouMgr.Inst:IsOnAssist(lingshouId) then
        typeStr = LocalString.GetString("助战")
        xiuxiAction = function()
            Gac2Gas.RequestSetAssistLingShou("")
        end
    elseif CLingShouMgr.Inst:IsOnJieBan(lingshouId) then
        typeStr = LocalString.GetString("结伴")
        xiuxiAction = function()
            Gac2Gas.RequestCancelBindPartner_LingShou(lingshouId)
        end
    elseif CLingShouMgr.Inst:IsOnBattle(lingshouId) then
        typeStr = LocalString.GetString("出战")
        xiuxiAction = function()
            Gac2Gas.RequestPackLingShou()
        end
    end
    if xiuxiAction then
        local msg = g_MessageMgr:FormatMessage("QMPK_LingShou_Config_XiuXi_Learn", typeStr)
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function ()
            xiuxiAction()
            Gac2Gas.RequestOperateQmpkLingShouV2(lingshouId, self.m_CurrentLingShouTemplateId, self.m_CurrentLingShouNature, raw,
            self.m_LingfuIndexTbl[1],self.m_LingfuIndexTbl[2],self.m_LingfuIndexTbl[3], raw2)
            end),nil,LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        Gac2Gas.RequestOperateQmpkLingShouV2(lingshouId, self.m_CurrentLingShouTemplateId, self.m_CurrentLingShouNature, raw,
            self.m_LingfuIndexTbl[1],self.m_LingfuIndexTbl[2],self.m_LingfuIndexTbl[3], raw2)
    end
end

function CLuaQMPKLingShouConfig:InitLingShouSkillSlots()
    self.m_LingShouSkillSlotArray = {}
    self.m_LingShouSkillOutlineArray = {}
    self.m_LingShouSkillSlot:SetActive(false)
    for i = 1, 8, 1 do
        local new_slot = NGUITools.AddChild(self.m_LingShouSkills.gameObject, self.m_LingShouSkillSlot)
        new_slot:SetActive(true)
        table.insert(self.m_LingShouSkillSlotArray, new_slot)
        table.insert(self.m_LingShouSkillOutlineArray, new_slot.transform:Find("HasSkill/Outline").gameObject)
        self.m_LingShouSkillOutlineArray[i]:SetActive(false)
    end
    self.m_LingShouSkills:Reposition()
end

function CLuaQMPKLingShouConfig:DestroyLingShouSkillSlots()
    if(self.m_LingShouSkillSlotArray)then
        for k, v in ipairs(self.m_LingShouSkillSlotArray) do
            GameObject.Destroy(v)
        end
    end
    self.m_LingShouSkillSlotArray = nil
    self.m_LingShouSkillOutlineArray = nil
end

function CLuaQMPKLingShouConfig:RefreshLingShouSkillSlots(_skill_table) -- 目前skilltable的第一项和最后一项为无效项，需跳过
    for k, v in ipairs(self.m_LingShouSkillSlotArray) do
        self:InitLingShouSkillSlot(v.transform, _skill_table[k + 1])
    end
end

function CLuaQMPKLingShouConfig:InitLingShouSkillSlot(_transform,skillId)
    UIEventListener.Get(_transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if skillId>0 then
            CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil)
        end
        for k, v in pairs(self.m_LingShouSkillSlotArray) do
          self.m_LingShouSkillOutlineArray[k]:SetActive(v.transform == _transform)
        end
    end)
    local data=Skill_AllSkills.GetData(skillId)
    local icon=_transform:Find("HasSkill/Texture"):GetComponent(typeof(CUITexture))
    local levelLabel=_transform:Find("HasSkill/Sprite/LevelLabel"):GetComponent(typeof(UILabel))
    local levelRoot = _transform:Find("HasSkill/Sprite").gameObject
    if not data then
        icon.gameObject:SetActive(false)
        levelRoot:SetActive(false)
        return
    end
    icon.gameObject:SetActive(true)
    levelRoot:SetActive(true)
    icon:LoadSkillIcon(data.SkillIcon)
    levelLabel.text="Lv."..tostring(data.Level)
end


function CLuaQMPKLingShouConfig:InitLingfu(id, details)
    local dic=details.data.Props.QmpkLingFuIndex

    local t={}
    CommonDefs.DictIterate(dic,DelegateFactory.Action_object_object(function (p1, p2) 
        local key=tonumber(p1)
        local val=tonumber(p2)
        t[key]=val
    end))
    self.m_LingfuIndexTbl=t

    self.m_LingfuTitleTbl={}
    -- self.m_LingfuDescTbl={}

    local parent=FindChild(self.m_LingfuRoot.transform,"Table")
    CUICommonDef.ClearTransform(parent)
    local titleTemplate=FindChild(self.m_LingfuRoot.transform,"TitleTemplate").gameObject
    local wordTemplate=FindChild(self.m_LingfuRoot.transform,"WordTemplate").gameObject
    titleTemplate:SetActive(false)
    wordTemplate:SetActive(false)


    local lingshouData= QuanMinPK_LingShou.GetData(self.m_CurrentLingShouTemplateId)
    for k,v in pairs(self.m_LingfuIndexTbl) do
        local go=NGUITools.AddChild(parent.gameObject,titleTemplate)
        go:SetActive(true)
        table.insert( self.m_LingfuTitleTbl,go )

        UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(go) self:OnLingfuTitleClick(go) end)

        local wordIdArray=0
        local name=""
        if k==1 then
            wordIdArray=lingshouData.GeneralLingFu[v-1]
            name=lingshouData.GeneralLingFuName[v-1]
        elseif k==2 then
            wordIdArray=lingshouData.SpecialLingFu[v-1]
            name=lingshouData.SpecialLingFuName[v-1]
        elseif k==3 then
            wordIdArray=lingshouData.SuitLingFu[v-1]
            name=lingshouData.SuitLingFuName[v-1]
        end

        go:GetComponent(typeof(CQMPKTitleTemplate)):Init(name)
        local type=k==1 and LocalString.GetString("普通(三件)") or k==2 and LocalString.GetString("特殊(三件)") or k==3 and LocalString.GetString("套装(四联/六联)")
        FindChild(go.transform,"TypeLabel"):GetComponent(typeof(UILabel)).text=type

        local wordsTable=FindChild(go.transform,"Words")
        for i=1,wordIdArray.Length do
            local item=NGUITools.AddChild(wordsTable.gameObject,wordTemplate)
            item:SetActive(true)
            local word = Word_Word.GetData(wordIdArray[i-1])
            FindChild(item.transform,"Label"):GetComponent(typeof(UILabel)).text=word.Description
        end
        wordsTable:GetComponent(typeof(UITable)):Reposition()
    end
    
    parent:GetComponent(typeof(UITable)):Reposition()

end

function CLuaQMPKLingShouConfig:OnLingfuTitleClick( go) 
    if self.m_IsOtherPlayer then
        return
    end
    self.m_CurrentLingfuTitleIndex=0
    for i,v in ipairs(self.m_LingfuTitleTbl) do
        if go==v then
            self.m_CurrentLingfuTitleIndex=i
            break
        end
    end

    self.m_LingfuWordChoiceTbl={}
    local lingshouData= QuanMinPK_LingShou.GetData(self.m_CurrentLingShouTemplateId)
    if lingshouData then
        local popupList={}
        local ids=nil
        local names=nil
        if self.m_CurrentLingfuTitleIndex==1 then
            ids=lingshouData.GeneralLingFu
            names=lingshouData.GeneralLingFuName
        elseif self.m_CurrentLingfuTitleIndex==2 then
            ids=lingshouData.SpecialLingFu
            names=lingshouData.SpecialLingFuName
        elseif self.m_CurrentLingfuTitleIndex==3 then
            ids=lingshouData.SuitLingFu
            names=lingshouData.SuitLingFuName
        end

        local curIndex=self.m_LingfuIndexTbl[self.m_CurrentLingfuTitleIndex]
        for i=1,ids.Length do
            if curIndex~=i then--选过的不能再选了
                table.insert( self.m_LingfuWordChoiceTbl,i )
                local data = PopupMenuItemData(names[i-1], DelegateFactory.Action_int(function(index) self:OnLingfuTitleSelected(index) end),false, nil, EnumPopupMenuItemStyle.Light)
                table.insert( popupList,data )
            end
        end

        local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform:Find("Place"), CPopupMenuInfoMgr.AlignType.Bottom, #popupList >= 8 and 2 or 1, nil, 600, true, 296)
    end

end
function CLuaQMPKLingShouConfig:OnLingfuTitleSelected( index) 
    local choiceIndex=self.m_LingfuWordChoiceTbl[index+1]
    if self.m_CurrentLingfuTitleIndex>0 and choiceIndex>0 then
        if self.m_LingfuIndexTbl[self.m_CurrentLingfuTitleIndex] ~= choiceIndex then self.m_HasChanged = true end
        --更新索引
        self.m_LingfuIndexTbl[self.m_CurrentLingfuTitleIndex]=choiceIndex

        --刷新界面
        local wordIdArray=0
        local name=""
        local lingshouData= QuanMinPK_LingShou.GetData(self.m_CurrentLingShouTemplateId)
        if self.m_CurrentLingfuTitleIndex==1 then
            wordIdArray=lingshouData.GeneralLingFu[choiceIndex-1]
            name=lingshouData.GeneralLingFuName[choiceIndex-1]
        elseif self.m_CurrentLingfuTitleIndex==2 then
            wordIdArray=lingshouData.SpecialLingFu[choiceIndex-1]
            name=lingshouData.SpecialLingFuName[choiceIndex-1]
        elseif self.m_CurrentLingfuTitleIndex==3 then
            wordIdArray=lingshouData.SuitLingFu[choiceIndex-1]
            name=lingshouData.SuitLingFuName[choiceIndex-1]
        end

        
        local titleGo=self.m_LingfuTitleTbl[self.m_CurrentLingfuTitleIndex]
        titleGo:GetComponent(typeof(CQMPKTitleTemplate)):Init(name)

        local wordsTable=FindChild(titleGo.transform,"Words")
        CUICommonDef.ClearTransform(wordsTable)

        local wordTemplate=FindChild(self.m_LingfuRoot.transform,"WordTemplate").gameObject

        for i=1,wordIdArray.Length do
            local item=NGUITools.AddChild(wordsTable.gameObject,wordTemplate)
            item:SetActive(true)
            local word = Word_Word.GetData(wordIdArray[i-1])
            FindChild(item.transform,"Label"):GetComponent(typeof(UILabel)).text=word.Description
        end
        wordsTable:GetComponent(typeof(UITable)):Reposition()
        
        local parent=FindChild(self.m_LingfuRoot.transform,"Table")
        parent:GetComponent(typeof(UITable)):Reposition()
        
        -- local desc=""
        -- for i=1,wordIdArray.Length do
        --     local word = Word_Word.GetData(wordIdArray[i-1])
        --     desc=desc..word.Description.."\n"
        -- end
        -- self.m_LingfuDescTbl[self.m_CurrentLingfuTitleIndex]:GetComponent(typeof(UILabel)).text=desc

        -- local jewelData=Jewel_Jewel.GetData(jewelId)
        -- self.m_WenShiTitleTbl[self.m_CurrentWenShiTitleIndex]:GetComponent(typeof(CQMPKTitleTemplate)):Init(jewelData.JewelName)

        -- local word = Word_Word.GetData(jewelData.onFaBao)
        -- self.m_WenShiDescTbl[self.m_CurrentWenShiTitleIndex]:GetComponent(typeof(UILabel)).text=word.Description
    end

end

function CLuaQMPKLingShouConfig:InitOtherPlayerConfig() 
    self.m_NatureChooseObj:SetArrowVisible(false)
    self.m_TemplateChooseObj:SetArrowVisible(false)

    self.m_TipLab.gameObject:SetActive(false)
    self.m_ConfirmBtnLab.text = LocalString.GetString("学习此灵兽")
end

function CLuaQMPKLingShouConfig:OnPageCloseOrChange(finalFunc)
    if self.m_HasChanged then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("QMPK_ConfigSave_Confirm"), function()
            self:OnConfirmBtnClick(self.m_ConfirmBtn,finalFunc)
        end, function()
            if finalFunc then finalFunc() end
        end, nil, nil, false)
        self.m_HasChanged = false
        return true
    else
        if finalFunc then finalFunc() end
    end
    return false
end