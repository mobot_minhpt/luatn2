-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipBaptizeJadeCost = import "L10.UI.CEquipBaptizeJadeCost"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Money = import "L10.Game.Money"
local UIEventListener = import "UIEventListener"
CEquipBaptizeJadeCost.m_Awake_CS2LuaHook = function (this) 
    this.addBtn:SetActive(false)
    UIEventListener.Get(this.addBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CShopMallMgr.ShowChargeWnd()
    end)
end
CEquipBaptizeJadeCost.m_OnMoneyUpdated_CS2LuaHook = function (this) 
    this.mCurrentJade = 0
    if CClientMainPlayer.Inst ~= nil then
        if CShopMallMgr.CanUseBindJadeBuy(this.mTemplateId) then
            this.mCurrentJade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
            if this.jadeBindSprite ~= nil then
                this.jadeBindSprite.spriteName = Money.GetIconName(EnumMoneyType.LingYu_OnlyBind, EnumPlayScoreKey.NONE)
                this.jadeBindSprite.gameObject:SetActive(CClientMainPlayer.Inst.BindJade > 0)
            end
        else
            this.mCurrentJade = CClientMainPlayer.Inst.Jade
            if this.jadeBindSprite ~= nil then
                this.jadeBindSprite.gameObject:SetActive(false)
            end
        end
    end
    this.jadeLabel.text = tostring(this.mCurrentJade)
end
