local CPhysicsTimeMgr=import "L10.Engine.CPhysicsTimeMgr"
local AMPlayer=import "L10.Game.CutScene.AMPlayer"
local WeatherManager=import "CTT3.Weather.WeatherManager"
local CCinemachineCtrl=import "L10.Game.CCinemachineCtrl"
local CCinemachineMgr=import "L10.Game.CCinemachineMgr"
local EnumObjectType = import "L10.Game.EnumObjectType"
local CDynamicWorldPositionFX=import "L10.Game.CDynamicWorldPositionFX"
local CItemMgr=import "L10.Game.CItemMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CBlowDetectMgr = import "L10.Game.CBlowDetectMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local DelegateFactory = import "DelegateFactory"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local EventManager = import "EventManager"
local CTalismanUserWordCls = import "L10.Game.CTalismanUserWordCls"
local CPlayerHousePrayWordsType = import "L10.Game.CPlayerHousePrayWordsType"
local CCommonItem = import "L10.Game.CCommonItem"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CFeiShengMgr=import "L10.Game.CFeiShengMgr"
local CScene = import "L10.Game.CScene"
local GameObject = import "UnityEngine.GameObject"
local CSystemInfoMgr = import "L10.Engine.CSystemInfoMgr"
local CFakeBullet=import "L10.Game.CFakeBullet"
local EKickOutType = import "L10.Engine.EKickOutType"
local Animator = import "UnityEngine.Animator"
local LuaTweenUtils = import "LuaTweenUtils"
local WWW = import "UnityEngine.WWW"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CCameraParam = import "L10.Engine.CameraControl.CCameraParam"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local CClientNpc = import "L10.Game.CClientNpc"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local EChuanjiabaoType = import "L10.Game.EChuanjiabaoType"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemInfoMgr  = import "L10.UI.CItemInfoMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local Clipboard = import "L10.Engine.Clipboard"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CPlayerInfo = import "CPlayerInfoMgr+CPlayerInfo"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CProfileObject = import "L10.Game.CProfileObject"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CPropertySkill = import "L10.Game.CPropertySkill"
local CZhaoHunData = import "L10.Game.CZhaoHunData"
local EnumClass = import "L10.Game.EnumClass"
local EnumEventType = import "EnumEventType"
local EnumGender = import "L10.Game.EnumGender"
local CCppBuff=import "L10.Game.CCppBuff"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local CLightMgr = import "L10.Engine.CLightMgr"
local CClientFurniture = import "L10.Game.CClientFurniture"
local Renderer = import "UnityEngine.Renderer"
local Material = import "UnityEngine.Material"
local NewPostEffectMgr = import "L10.Engine.NewPostEffectMgr"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local EnumActionState = import "L10.Game.EnumActionState"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local SoundManager = import "SoundManager"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CCommonProgressInfoMgr = import "L10.UI.CCommonProgressInfoMgr"
local Object = import "System.Object"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CGuildMgr = import "L10.Game.CGuildMgr"
local DateTime = import "System.DateTime"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local CWeatherMgr = import "L10.Game.CWeatherMgr"
local CClientPick = import "L10.Game.CClientPick"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"

function Gas2Gac.SetIsTreatedAsPlayerForPlay(name, bset)
	if bset then
		g_ActivedIsTreatedAsPlayerName = name
	elseif g_ActivedIsTreatedAsPlayerName == name then
		g_ActivedIsTreatedAsPlayerName = nil
	end
end

g_IsTreatedAsPlayer = {}
g_ActivedIsTreatedAsPlayerName = nil
CClientMainPlayer.m_IsTreatedAsPlayer = function(obj)
	if g_ActivedIsTreatedAsPlayerName and g_IsTreatedAsPlayer[g_ActivedIsTreatedAsPlayerName] then
		return g_IsTreatedAsPlayer[g_ActivedIsTreatedAsPlayerName](obj)
	end
end

g_IsTreatedAsPlayer["GuoQingJiaoChang"] = function(obj)

	if obj and obj.ObjectType == EnumObjectType.Monster then
		return true, true
	end
end

function Gas2Gac.BaoWeiNanGuaSyncPlayInfo(playInfoU, eventInfoU)
	local playInfo = MsgPackImpl.unpack(playInfoU)
	local eventInfo = MsgPackImpl.unpack(eventInfoU)

	local round, killed, all, remainTime, percent = playInfo[0], playInfo[1], playInfo[2], playInfo[3], playInfo[4]


	local eventTable = {}
	for i = 0, eventInfo.Count - 1 do

		eventTable[eventInfo[i]] = true
	end
	LuaBaoWeiNanGuaMgr.percent = percent
	LuaBaoWeiNanGuaMgr.killed = killed
	LuaBaoWeiNanGuaMgr.all = all
	LuaBaoWeiNanGuaMgr.eventTable = eventTable
	g_ScriptEvent:BroadcastInLua("OnBaoWeiNanGuaSyncPlayInfo")
end

function Gas2Gac.BaoWeiNanGuaResult(bWin, infoU)

	LuaBaoWeiNanGuaMgr.result = bWin
	if bWin then
		local infoList = MsgPackImpl.unpack(infoU)
		local hpPercent, itemId = infoList[0], infoList[1]

		LuaBaoWeiNanGuaMgr.resultPercent = hpPercent
		LuaBaoWeiNanGuaMgr.resultItem = itemId
	else

	end
	CUIManager.ShowUI("BaoWeiNanGuaResultWnd")
end

g_UpdateCheckAccelerationIdTick = nil
function Gas2Gac.CheckAcceleration(checkId, checkDuration)

	if g_UpdateCheckAccelerationIdTick then
		UnRegisterTick(g_UpdateCheckAccelerationIdTick)
		g_UpdateCheckAccelerationIdTick = nil
	end

	g_UpdateCheckAccelerationIdTick = RegisterTickOnce(function()
		Gac2Gas.ResponseCheckAcceleration(checkId)
		g_UpdateCheckAccelerationIdTick=nil
	end, checkDuration * 1000)
end

function Gas2Gac.NewYearGuildQuizOpenNotifyWnd(guildId, msg, remainTime)
	local theGuildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
	if theGuildId > 0 and guildId == theGuildId then
		LuaNewYear2019Mgr.GuildQuizMsg = msg
		LuaNewYear2019Mgr.GuildQuizTime = remainTime
		CUIManager.ShowUI("NewYear2019TaskTipWnd")
	end
end

function Gas2Gac.NewYearChallengeSyncScoreInfo(infoU)
	local infoList = MsgPackImpl.unpack(infoU)
	if infoList.Count < 5 then return end
	local show, score, teamScore, rate, prob = infoList[0], infoList[1], infoList[2], infoList[3], infoList[4]

	LuaNewYear2019Mgr.NewYearGameShowIndex = show
	LuaNewYear2019Mgr.NewYearGameScore = score
	LuaNewYear2019Mgr.NewYearGameAttack = rate
	LuaNewYear2019Mgr.NewYearGameTeamScore = teamScore
	LuaNewYear2019Mgr.NewYearGamePer = prob
	g_ScriptEvent:BroadcastInLua("NewYearGameRefresh")
end

function Gas2Gac.DeletePlayerAchievement(achievementId)
	if not CClientMainPlayer.Inst then return end
	CClientMainPlayer.Inst.PlayProp.Achievement:RmAchievement(achievementId)
	EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerAchievementProgress,{achievementId})
end

function Gas2Gac.SetFogValue(value)
	if CRenderScene.Inst then
		local startDist = CRenderScene.Inst.m_RenderData.fogStartDist
		CRenderScene.Inst:OverrideFogStart((1.3 - value) * startDist, false)
	end
end

function Gas2Gac.NotifyPlayerStartBlow(duration)
	CBlowDetectMgr.Inst:StartDetect(duration, DelegateFactory.Action_bool(
		function(result)
			if result then
				Gac2Gas.PlayerRequestBlow()
			else
				g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("检测吹气超时，请重试！"))
			end
		end
	), false)
end

local CPos = import "L10.Engine.CPos"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

local fireworks2019 = {}
function Gas2Gac.StopFire2019Fireworks()
	for k,v in pairs(fireworks2019) do
		v:Destroy()
	end
end
function Gas2Gac.StartFire2019Fireworks(kind)
	local x,y = math.floor(65.7*64),math.floor(58.4*64)
	local pos = CPos(x,y)
	if kind==1 then
		if fireworks2019[1] then fireworks2019[1]:Destroy() end
		fireworks2019[1] = CEffectMgr.Inst:AddWorldPositionFX(88800913, pos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
	elseif kind==2 then
		if fireworks2019[2] then fireworks2019[2]:Destroy() end
		fireworks2019[2] =CEffectMgr.Inst:AddWorldPositionFX(88800914, pos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
	elseif kind==3 then
		if fireworks2019[3] then fireworks2019[3]:Destroy() end
		fireworks2019[3] = CEffectMgr.Inst:AddWorldPositionFX(88800915, pos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
	end
end

function Gas2Gac.Sync2019HonorCoinInfo(honorCoinCount, honorExchageIdx, honorMeterialExchangeSet)
	if not CClientMainPlayer.Inst then return end
	CClientMainPlayer.Inst.PlayProp.HonorCoinCount = honorCoinCount
	CClientMainPlayer.Inst.PlayProp.HonorExchangeIdx = honorExchageIdx
	CClientMainPlayer.Inst.PlayProp.HonorMeterialRewardSet = honorMeterialExchangeSet
	g_ScriptEvent:BroadcastInLua("Sync2019HonorCoinInfo",honorCoinCount, honorExchageIdx,honorMeterialExchangeSet)
end
local CLingShou=import "L10.Game.CLingShou"
function Gas2Gac.RequestLingShouScoreInfo_Result(lingshouId, lingshouUD, score, bindLingShouId, bindLingshouUD, bindScore)

	local lingshou1 = CLingShou()
	lingshou1:LoadFromString(lingshouUD)
	local lingshou2 = CLingShou()
	lingshou2:LoadFromString(bindLingshouUD)
	g_ScriptEvent:BroadcastInLua("RequestLingShouScoreInfo_Result",lingshouId,score,lingshou1, bindLingShouId,bindScore,lingshou2)
end

function Gas2Gac.OpenActivityUrl(activityName)
	CWebBrowserMgr.Inst:OpenActivityUrl(activityName)
end

function Gas2Gac.SyncStarBiwuViewVictory()
end


function Gas2Gac.HouseCompetitionCheckRewardResult(bCheckResult)
end

function Gas2Gac.UpdateMessagePushStatus(pushId, bOpen)
	local Application = import "UnityEngine.Application"
	local RuntimePlatform = import "UnityEngine.RuntimePlatform"
	local Main = import "L10.Engine.Main"
	if Application.platform ~= RuntimePlatform.IPhonePlayer or Main.Inst.EngineVersion > 366576 or Main.Inst.EngineVersion < 0 then
		local CLocalPushMgr = import "L10.Game.CLocalPushMgr"
		if bOpen == false then
			CLocalPushMgr.AddExcludedPushId(pushId)
		else
			CLocalPushMgr.RemoveExcludedPushId(pushId)
		end
	end
end

function Gas2Gac.HideNpcByWeather(engineId, bHide)
	local npc = TypeAs(CClientObjectMgr.Inst:GetObject(engineId), typeof(CClientNpc))
	if not npc then return end
	npc.Visible = not bHide
end


function Gas2Gac.GetOnlineFriendsNotInTeamGroup(retData_U, context)
end

-- 智能客服的url, QuerySmartGMToken 对应的返回函数
function Gas2Gac.SyncSmartGMToken(bSuccess, refer, code, message)
	-- bSuccess 为 true 的时候,refer有效,code,message不用管
	-- bSuccess 为false的时候,refer无效,code,message就是原始返回
	if bSuccess then
		CWebBrowserMgr.Inst:OpenUrl(refer)
	end
end

-- 智能客服的token, QuerySmartGMTokenRaw 对应的返回函数
function Gas2Gac.SyncSmartGMTokenRaw(bSuccess, token, code, message)
	-- bSuccess 为 true 的时候,token有效,code,message不用管
	-- bSuccess 为false的时候,token无效,code,message就是原始返回
	if bSuccess then
		g_ScriptEvent:BroadcastInLua("OnMoBaiBangShare",token)
	end
end

function Gas2Gac.SendSmartGMTokenAndUrlWitchContext(bSuccess, token, code, message, question, context)
	-- bSuccess 为 true 的时候,token有效,code,message不用管
	-- bSuccess 为false的时候,token无效,code,message就是原始返回
	if bSuccess then
		CWebBrowserMgr.Inst:OpenUrl(token.."&q="..WWW.EscapeURL(question or ""))
	else
	end
end

function Gas2Gac.SendQueryPlayerInfoResult_Online_End(playerId, intensifySuitId, intensifySuitFxIndex, talismanIndex, talismanUseWordBytes, isActiveXianJiaSuitUseWord, housePrayWordsUD)
	local talismanUserWordCls = CTalismanUserWordCls()
	if talismanUseWordBytes ~= nil and talismanUseWordBytes.Length > 0 then
		talismanUserWordCls:LoadFromString(talismanUseWordBytes)
	end

	local list = MsgPackImpl.unpack(housePrayWordsUD)
	local buffObj = CCppBuff()
	buffObj:LoadFromString(list[0])


	local words = CPlayerHousePrayWordsType()
	if list[1] and list[1].Length > 0 then
		words:LoadFromString(list[1])
	end

	g_ScriptEvent:BroadcastInLua("OnQueryPlayerIntensifySuitInfoDoneLua", playerId, intensifySuitId, intensifySuitFxIndex, talismanIndex, talismanUserWordCls, isActiveXianJiaSuitUseWord)

	EventManager.BroadcastInternalForLua(EnumEventType.OnQueryPlayerIntensifySuitInfoDone, {playerId, intensifySuitId, talismanIndex, talismanUserWordCls, isActiveXianJiaSuitUseWord})
	g_ScriptEvent:BroadcastInLua("OnQueryPlayerHousePrayWordsDone",playerId,words,buffObj)
end

function Gas2Gac.SendQueryPlayerInfoResult_Offline(playerId, playerName, playerGrade, playerClass, title, xiuwei, guildId, guildName, teamName, achievement, hateness, friendness, equipscore, gender, skillProp, appearProp, xiulian, zhaoHunInfo, expression, expressionTxt, sticker, serverName, xianfanStatus, profileInfo_U, yingLingState, mingGe, feishengLevel, xianshenLevel, sectId, sectName)
    if CPlayerInfoMgr.PlayerId == playerId then
        local skill = CPropertySkill()
        skill:LoadFromString(skillProp)
        local appearance = CPropertyAppearance()
		appearance:LoadFromString(appearProp, CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerClass), CommonDefs.ConvertIntToEnum(typeof(EnumGender), gender))

        local zhaohunData = CZhaoHunData()
		zhaohunData:LoadFromString(zhaoHunInfo)

        local profileInfo = CProfileObject()
        if profileInfo_U ~= nil then
            profileInfo:LoadFromString(profileInfo_U)
		end

        CPlayerInfoMgr.PlayerInfo = CreateFromClass(CPlayerInfo, math.floor(playerId), playerName, playerClass, gender, title, playerGrade, xiuwei, xiulian, guildId, guildName,  sectId, sectName, teamName, achievement, hateness, friendness, equipscore, expression, expressionTxt, sticker, profileInfo, skill, appearance, zhaohunData, false, nil, nil, serverName, xianfanStatus > 0, mingGe, feishengLevel, xianshenLevel)
		CPlayerInfoMgr.PlayerInfo.xianfanStatus = xianfanStatus

        EventManager.BroadcastInternalForLua(EnumEventType.OnQueryPlayerBasicInfoDone, {math.floor(playerId), false})
    end
	EventManager.Broadcast(EnumEventType.OnQueryPlayerHunPoInfoResultOffLine)

    local words = CPlayerHousePrayWordsType()
    words.StartTime = System.UInt32.MaxValue
	-- EventManager.BroadcastInternalForLua(EnumEventType.OnQueryPlayerHousePrayWordsDone, {math.floor(playerId), words})

	g_ScriptEvent:BroadcastInLua("OnQueryPlayerHousePrayWordsDone",playerId,words)
end

function Gas2Gac.SendQueryPlayerInfoResult_NotExist(playerId)
    local words = CPlayerHousePrayWordsType()
    words.StartTime = System.UInt32.MaxValue
    -- EventManager.BroadcastInternalForLua(EnumEventType.OnQueryPlayerHousePrayWordsDone, {math.floor(playerId), words})
	g_ScriptEvent:BroadcastInLua("OnQueryPlayerHousePrayWordsDone",playerId,words)
end

function Gas2Gac.ShowOnceFlagEffect(flagTemplateId, x, y, z)
end


function Gas2Gac.ReplyEquipInfoInAttrGroup(itemId, wordSetIdx, holeSetIdx, gemGroupId, feiShengAdjust, adjustEquipGrade, adjustGemGroupId, adjustIntensifyLevel, adjustForgeLevel, adjustWordUD, equipStr)
	local item = CreateFromClass(CCommonItem, false, equipStr)
	item.Equip.GemGroupId = gemGroupId
	item.Equip.ActiveWordSetIdx = wordSetIdx
	item.Equip.ActiveHoleSetIdx = holeSetIdx
	-- print(wordSetIdx,holeSetIdx)

	if feiShengAdjust then
		local adjustWordList = MsgPackImpl.unpack(adjustWordUD)
		if adjustWordList and adjustWordList.Count == 5 then
			local fixedWords = adjustWordList[0]
			if fixedWords then
				CFeiShengMgr.Inst.m_BasicWords = CreateFromClass(MakeGenericClass(List, UInt32))
				for i = 0, fixedWords.Count - 1 do
					-- print("fixedWords", fixedWords[i])
					CommonDefs.ListAdd(CFeiShengMgr.Inst.m_BasicWords, typeof(UInt32), math.floor(tonumber(fixedWords[i] or 0)))
				end
			end

			local extraWords = adjustWordList[1]
			if extraWords then
				CFeiShengMgr.Inst.m_ExtraWords = CreateFromClass(MakeGenericClass(List, UInt32))
				for i = 0, extraWords.Count - 1 do
					-- print("extraWords", extraWords[i])
					CommonDefs.ListAdd(CFeiShengMgr.Inst.m_ExtraWords, typeof(UInt32), math.floor(tonumber(extraWords[i] or 0)))
				end
			end

			local jewelItems = adjustWordList[2]
			if jewelItems then
				CFeiShengMgr.Inst.m_JewelItems = CreateFromClass(MakeGenericClass(List, UInt32))
				for i = 0, jewelItems.Count - 1 do
					-- print("jewelItems", jewelItems[i])
					CommonDefs.ListAdd(CFeiShengMgr.Inst.m_JewelItems, typeof(UInt32), math.floor(tonumber(jewelItems[i] or 0)))
				end
			end

			-- 服务器不发基础属性调整系数，客户端自己会算
			--[[
			local adjustFactors = adjustWordList[3]
			if adjustFactors then
				CFeiShengMgr.Inst.m_AdjustFactors = CreateFromClass(MakeGenericClass(Dictionary, String, Double))
				CommonDefs.DictIterate(adjustFactors, DelegateFactory.Action_object_object(function (___key, ___value)
					-- print("adjustFactors", tostring(___key), tostring(___value))
					CommonDefs.DictAdd(CFeiShengMgr.Inst.m_AdjustFactors, typeof(String), ___key, typeof(Double), tonumber(___value))
				end))
			end
			--]]

			local shenbingWords = adjustWordList[4]
			if shenbingWords then
				CFeiShengMgr.Inst.m_ShenBingWords = CreateFromClass(MakeGenericClass(List, UInt32))
				for i = 0, shenbingWords.Count - 1 do
					-- print("shenbingWords", shenbingWords[i])
					CommonDefs.ListAdd(CFeiShengMgr.Inst.m_ShenBingWords, typeof(UInt32), math.floor(tonumber(shenbingWords[i] or 0)))
				end
			end
		end

		item.Equip.IsFeiShengAdjusted = 1
		item.Equip.AdjustedFeiShengEquipGrade = adjustEquipGrade
		item.Equip.AdjustedFeiShengIntensifyLevel = adjustIntensifyLevel
		item.Equip.AdjustedFeiShengForgeLevel = adjustForgeLevel
		item.Equip.AdjustedFeiShengGemGroupId = adjustGemGroupId

		CommonDefs.DictClear(item.Equip.AdjustedFeiShengFixedWords)

		local idx = 1
		CommonDefs.ListIterate(CFeiShengMgr.Inst.m_BasicWords, DelegateFactory.Action_object(function (___value)
			local v = ___value
			CommonDefs.DictAdd(item.Equip.AdjustedFeiShengFixedWords, typeof(Byte), idx, typeof(UInt32), v)
			idx = idx + 1
		end))

		CommonDefs.DictClear(item.Equip.AdjustedFeiShengExtraWords)
		idx = 1
		CommonDefs.ListIterate(CFeiShengMgr.Inst.m_ExtraWords, DelegateFactory.Action_object(function (___value)
			local v = ___value
			CommonDefs.DictAdd(item.Equip.AdjustedFeiShengExtraWords, typeof(Byte), idx, typeof(UInt32), v)
			idx = idx + 1
		end))

		CommonDefs.DictClear(item.Equip.AdjustedFeiShengJewelItems)
		idx = 1
		CommonDefs.ListIterate(CFeiShengMgr.Inst.m_JewelItems, DelegateFactory.Action_object(function (___value)
			local v = ___value
			CommonDefs.DictAdd(item.Equip.AdjustedFeiShengJewelItems, typeof(Byte), idx, typeof(UInt32), v)
			idx = idx + 1
		end))
		CommonDefs.DictClear(item.Equip.AdjustedShenBingWords)
		idx = 1

		CommonDefs.ListIterate(CFeiShengMgr.Inst.m_ShenBingWords, DelegateFactory.Action_object(function (___value)
			local v = ___value
			local default = idx
			idx = default + 1
			CommonDefs.DictAdd(item.Equip.AdjustedShenBingWords, typeof(Byte), default, typeof(UInt32), v)
		end))
	end
	g_ScriptEvent:BroadcastInLua("ReplyEquipInfoInAttrGroup", itemId, wordSetIdx, holeSetIdx, gemGroupId, feiShengAdjust,item)
end

function Gas2Gac.UpdateAttrGroupUnlock(v)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.PlayProp.AttrGroupUnlock = v
	end
	g_ScriptEvent:BroadcastInLua("UpdateAttrGroupUnlock")
end

function Gas2Gac.UpdateAttrGroupAt(groupIdx, eItem, groupUD)
	local group = CAttrGroup()
	group:LoadFromString(groupUD)

	if CClientMainPlayer.Inst then
		CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,groupIdx,group)
	end
	g_ScriptEvent:BroadcastInLua("UpdateAttrGroupAt", groupIdx,eItem,group)
end

function Gas2Gac.RequestCanSwitchAttrGroupReturn(groupIdx, canSwitch)
	-- print("RequestCanSwitchAttrGroupReturn", groupIdx, canSwitch)
end

function Gas2Gac.SwitchAttrGroupSuccess(groupIdx)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.PlayProp.ActiveAttrGroupIdx = groupIdx
	end
	g_ScriptEvent:BroadcastInLua("SwitchAttrGroupSuccess", groupIdx)
end

function Gas2Gac.AddBulletFxWithSpeed(srcEngineId, dstEngineId, fxId, speed)
    local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
	local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)
	if srcObj and dstObj then
		local fx = CEffectMgr.Inst:AddBulletFX(
			fxId,
			srcObj.RO,
			dstObj.RO,
			0,speed,-1,nil)
		srcObj.RO:UnRefFX(fx)
	end
end

function Gas2Gac.CreateFakeBullet(BulletTemplateId, SourceX, SourceY, DestX, DestY,speed)
	CFakeBullet.CreateFakeBullet(BulletTemplateId, SourceX/64, SourceY/64, DestX/64, DestY/64,speed*30/64)
end

function Gas2Gac.SyncWakeupBedSuccess(WakeupBedRoom, WakeupBedTemplateId, WakeupBedX, WakeupBedY, status)
    if not CClientMainPlayer.Inst then return end
	local basicProp = CClientMainPlayer.Inst.BasicProp

	basicProp.WakeupBedRoom = WakeupBedRoom
	basicProp.WakeupBedTemplateId = WakeupBedTemplateId
	basicProp.WakeupBedX = WakeupBedX
	basicProp.WakeupBedY = WakeupBedY
	if status then
		g_MessageMgr:ShowMessage("House_RenChuang_Success")
	else
		g_MessageMgr:ShowMessage("House_RenChuang_Cancel_Success")
	end
end

function Gas2Gac.PlayAddLinkFx(srcEngineId, dstEngineId, fxId, duration)
	local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
	local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)
	if srcObj and srcObj.RO and dstObj and dstObj.RO and duration > 0 then
		local fx = srcObj.RO:GetLinkFxById(fxId, dstObj.RO)
		if fx then
			fx:Destroy()
		end
		local fx = CEffectMgr.Inst:AddLinkFX(fxId, srcObj.RO, dstObj.RO, 0, 1, -1)
		if fx then
			RegisterTickOnce(function()
				local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
				local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)
				if srcObj and srcObj.RO and dstObj and dstObj.RO then 
					local fx = srcObj.RO:GetLinkFxById(fxId, dstObj.RO)
					if fx then
						fx:Destroy()
					end
				end
			end, duration)
		end
	end
end

function Gas2Gac.PlayRemoveLinkFx(srcEngineId, dstEngineId, fxId)
    print("PlayRemoveLinkFx", srcEngineId, dstEngineId, fxId)
    local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
	local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)
	if srcObj and srcObj.RO and dstObj and dstObj.RO then 
		local fx = srcObj.RO:GetLinkFxById(fxId, dstObj.RO)
		if fx then
		    fx:Destroy()
		end
	end
end

function Gas2Gac.UpdateTianFuSkillTemplate(templateIdx, tianfuSkills_u)
	if not CClientMainPlayer.Inst then return end

	local tianfuSkills = MsgPackImpl.unpack(tianfuSkills_u)
	if not tianfuSkills then return end

	local skillProp = CClientMainPlayer.Inst.SkillProp
	local baseIdx = (templateIdx - 1) * 5
	for i = 1, 5 do
		skillProp.TianFuSkillTemplate[baseIdx + i] = tianfuSkills.Count >= i and tianfuSkills[i-1] or SkillButtonSkillType_lua.NoSkill
	end
	EventManager.BroadcastInternalForLua(EnumEventType.MainPlayerSkillPropUpdate, {})
end

-------------------
---BEGIN dll检测---
-------------------

function Gas2Gac.TriggerClientBinaryCheatCheck()

	--仅限Win平台
	if Application.platform ~= RuntimePlatform.WindowsPlayer then
		--非win直接通过
		Gac2Gas.ReportClientBinaryCheckResult(true, "")
		return
	end
	Gac2Gas.QueryClientBinaryIndentity()
end

function Gas2Gac.SyncClientBinaryIndentity(dataUD)

	--仅限Win平台
	if Application.platform ~= RuntimePlatform.WindowsPlayer then
		return
	end

	local dict = MsgPackImpl.unpack(dataUD)
	if not dict then return end

	local bPass = true
	local sResult = ""
	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key,val)
		--key for path
		--val for hash
		if bPass then
			local filePath = Application.dataPath..key
			local result = CSystemInfoMgr.Inst:ComputeFileMD5(filePath)
			if result~=nil and result~="" and val~=result then
				bPass = false
				sResult = filePath
			end
		end

	end))
	Gac2Gas.ReportClientBinaryCheckResult(bPass, sResult)
end
-------------------
----END dll检测----
-------------------

function Gas2Gac.ZhuoXiaoGuiBegin()
end

function Gas2Gac.ZhuoXiaoGuiEnd()
end

function Gas2Gac.SelectXiaoGuiSuccess(engineId, x, y, targetEngineId, targetX, targetY)
	CEffectMgr.Inst:AddFakeBulletFX(88801085, x/64, y/64, targetX/64, targetY/64,10,EnumWarnFXType.None,0,1,-1)
end

function Gas2Gac.LinearRushBegin(skillId, engineId, speed, destX, destY, destZ)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj ~= nil then
		local skillCls = math.floor(skillId / 100)
		-- 幻形引路
		if skillCls == 909007 then
			obj.RO:SetKickOutStatus(true, EKickOutType.YanShi_HXYL, -1, -1, -20)
		elseif skillCls == 913002 then --战狂狰兽·跃
			obj.RO:SetKickOutStatus(true, EKickOutType.ZhanKuang_ZSY, -1, -1, -20)
		elseif CommonDefs.HashSetContains(GameSetting_Client.GetData().LinearRushSkillCls,typeof(UInt32),skillCls) then
			local duraion = 1
			if speed > 0 then duraion = math.max(1/speed *0.5 + 0.5, 0.7) end
			obj.RO:SetKickOutStatus(true, EKickOutType.Line, duraion, -1, -20)
		end
	end
end

function Gas2Gac.LinearRushEnd(skillId, engineId)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj ~= nil then
		local skillCls = math.floor(skillId / 100)
		-- 幻形引路
		if skillCls ==  909007 then
			obj.RO:SetKickOutStatus(false, EKickOutType.YanShi_HXYL, -1, -1, -20)
		elseif skillCls == 913002 then --战狂狰兽·跃
			obj.RO:SetKickOutStatus(false, EKickOutType.ZhanKuang_ZSY, -1, -1, -20)
		elseif CommonDefs.HashSetContains(GameSetting_Client.GetData().LinearRushSkillCls,typeof(UInt32),skillCls) then
			obj.RO:SetKickOutStatus(false, EKickOutType.Line, -1, -1, -20)
		end
	end
end

function Gas2Gac.LinearPounceBegin(skillId, engineId, speed, AttackerEngineId, srcX, srcY, srcZ, destX, destY, destZ)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj then
		local skillCls = math.floor(skillId / 100)
		if skillCls == 950187 then
			local duration = 1
			if speed > 0 then duration = math.max(1/speed *0.5 + 0.5, 0.7) end
			local objX, objY = obj.Pos.x, obj.Pos.y
			local srcDiffX, srcDiffY = destX - srcX, destY - srcY
			local objDiffX, objDiffY = objX - srcX, objY - srcY
			local srcDist = math.sqrt(srcDiffX * srcDiffX + srcDiffY * srcDiffY)
			local objDist = math.sqrt(objDiffX * objDiffX + objDiffY * objDiffY)
			local delayTime = (objDist / srcDist) * duration
			obj.RO:SetKickOutStatus(true, EKickOutType.Line, duration - delayTime, delayTime, -20)
		end
	end
end

function Gas2Gac.LinearPounceEnd(skillId, engineId)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj then
		local skillCls = math.floor(skillId / 100)
		if skillCls == 950187 then
			obj.RO:SetKickOutStatus(false, EKickOutType.Line, -1, -1, -20)
		end
	end
end

-- 领取可选择回流奖励成功
function Gas2Gac.UseHuiLiuItemWithChoiceSuccess()

end

function Gas2Gac.SyncHellJieTuoGuoInfo(num, totalNum, currentLevel, infoU, bossInfoU)
	local list = MsgPackImpl.unpack(infoU)
	local t = {}
	local msg = "[ff2525]"..LocalString.GetString("被囚禁").."[-]"
	for i=1,list.Count,4 do
		local new_data = {
			playerId = list[i - 1],
			isHaveBuff = list[i],
			x = list[i+1],
			y = list[i+2],
		}
		local str = new_data.isHaveBuff and msg or ""
		CTeamMgr.Inst:UpdateTeamMemberExtState(new_data.playerId,str)
		table.insert( t, new_data)
	end

	local boss_dic = {}
	local bosslist = MsgPackImpl.unpack(bossInfoU)
	for i =1, bosslist.Count, 3 do
		local templateId, _x, _y =bosslist[i-1], bosslist[i], bosslist[i+1]

		if(boss_dic[templateId] == nil)then
			boss_dic[templateId] = {}
		else

		end
		table.insert(boss_dic[templateId], {x = _x, y = _y})
	end
	--目前第一次同步信息会在管理器初始化之前发送，此时管理器尚未开始监听，该同步信息会被遗漏，应提前保存
	LuaWuJianDiYuMgr:OnSyncHellJieTuoGuoNum(num,totalNum, currentLevel, t, boss_dic)
	--g_ScriptEvent:BroadcastInLua("SyncHellJieTuoGuoNum",num,totalNum,t)
end

function Gas2Gac.MoveSceneObject(path, x, y, z, time)
	--if CScene.MainScene.SceneTemplateId ~= sceneTemplateId then
	--	return
	--end

	local obj = GameObject.Find(path)
	if obj then
		local animator = obj:GetComponent(typeof(Animator));
        if animator then
            animator:Play("shuttle01");
		end
		LuaTweenUtils.TweenPosition(obj.transform,x,y,z,time)
	end
end

-- 法宝外观列表 key = (lv-1) * 100 + grade * 10 + count
function Gas2Gac.UpdateTalismanAppearanceList(appearanceListUD)
	if CClientMainPlayer.Inst == nil then
		return
	end
	CommonDefs.ListClear(CClientMainPlayer.Inst.TalismanAppearanceList)
	local appearanceList = MsgPackImpl.unpack(appearanceListUD)
	if appearanceList then
		for i = 0, appearanceList.Count - 1 do
			CommonDefs.ListAdd(CClientMainPlayer.Inst.TalismanAppearanceList, typeof(Int32), appearanceList[i])
		end
	end
	g_ScriptEvent:BroadcastInLua("TalismanFxListUpdate")
end


local CLoadingWnd = import "L10.UI.CLoadingWnd"
function Gas2Gac.SetNextLoadingPicToBlack(subTitleId)
	CLoadingWnd.Inst:SetNextToBlackTitle(subTitleId)
end

function Gas2Gac.SyncDayAndNightChangeInfo(templateId, startTime, stopTime, currentDegree, finalDegree, updateNow)
	-- 如果策划表填了永夜字段的话, 那么这类场景是不会有Rpc来更新昼夜信息的, 服务器来保证这一点
	if CScene.MainScene and CScene.MainScene.SceneTemplateId == templateId then
		-- 为0表示不会变化, 目前变化的值只有0和1
		if startTime == 0 and stopTime == 0 then
			CScene.MainScene.IsNightScene = (finalDegree > 0.5)
			g_ScriptEvent:BroadcastInLua("OnSyncDayAndNightChangeInfo",CScene.MainScene.IsNightScene)
			if updateNow == 1 then
				if CScene.MainScene.IsNightScene then
					L10.Engine.Scene.CRenderScene.Inst:Change2Night()
				else
					L10.Engine.Scene.CRenderScene.Inst:Change2Day()
				end
			end
		else
			-- 不为0, 那么需要根据时间进行动态变换
        	CScene.MainScene.StartTime = startTime
        	CScene.MainScene.StopTime = stopTime
        	CScene.MainScene.CurrentDegree = currentDegree
        	CScene.MainScene.FinalDegree = finalDegree
		end
	end
end

function Gas2Gac.EnterHellLevel(level)
	LuaWuJianDiYuMgr:OnEnterHellLevel(level)
end

function Gas2Gac.HideMainPlayer(bHide)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.Visible = not bHide
	end
end

function Gas2Gac.EndCameraMoveImmediately()
	if CameraFollow.Inst.IsAICamera then
		CameraFollow.Inst:RestoreCamera()
	end
end


function Gas2Gac.SyncYueShenTempleInfo(num, totalNum, currentLevel, infoU, bossInfo)

	local list = MsgPackImpl.unpack(infoU)
	local meminfos = {}
	local msg = "[ff0000]"..LocalString.GetString("被囚禁").."[-]"
    for i=1,list.Count,4 do
        local data = {
			playerId = list[i - 1],
			isHaveBuff = list[i],
			x = list[i+1],
			y = list[i+2],
		}
		table.insert(meminfos,data)
		local str = data.isHaveBuff and msg or ""
		CTeamMgr.Inst:UpdateTeamMemberExtState(data.playerId,str)
	end

    local enemydata = {}
	local list2 = MsgPackImpl.unpack(bossInfo)
	for i=1,list2.Count,3 do
        local data = {bossTemplateId = list2[i-1],x = list2[i],y = list2[i+1]}
        table.insert(enemydata,data)
	end
	g_ScriptEvent:BroadcastInLua("OnSyncYueShenTempleInfo",num,totalNum,meminfos,currentLevel)--右上角更新队员的新和月灵石数量
    g_ScriptEvent:BroadcastInLua("SyncExtPos",meminfos,enemydata)--小地图更新boss位置和玩家的位置

	if num == totalNum then
		CUIManager.ShowUI("ZQYueLingShiHeChengWnd")
	end
end

function Gas2Gac.SyncZhongQiuNpcFriendlinessInfo(npcData, currentNpcId)

	local list = MsgPackImpl.unpack(npcData)
	g_ScriptEvent:BroadcastInLua("OnSyncZQFriendliness",list,currentNpcId)
end

function Gas2Gac.EnterYueShenLevel(currentLevel)
	CLuaZQYueShenDianTopRightWnd.Level = currentLevel
end

-- 禁言状态.收到这个RPC之后需要把收到的数值存到 PlayProp 中,参数意义如下:
-- 永久禁言 -> forbidType : EnumSpeakForbidType.Disable(1), expiredTime: -1
-- 一般禁言 -> forbidType : EnumSpeakForbidType.Disable(1), expiredTime: timestamp
-- 静默禁言 -> forbidType : EnumSpeakForbidType.Silent(2), expiredTime: timestamp
-- 没有被禁言 -> forbidType : EnumSpeakForbidType.NotForbid(0)
function Gas2Gac.SyncSpeakSettingStatus(forbidType, expiredTime)

	if not CClientMainPlayer.Inst then return end
	CClientMainPlayer.Inst.PlayProp.ForbidSpeakType = forbidType
	CClientMainPlayer.Inst.PlayProp.ForbidSpeakExpiredTime = expiredTime
end


function Gas2Gac.CheckPlayerName()
	local name = CClientMainPlayer.Inst and CClientMainPlayer.Inst.RealName
	if not name then return end
	if not CWordFilterMgr.Inst:CheckName(name) then
		Gac2Gas.NeedChangePlayerName("auto")
	end
end

function Gas2Gac.OpenHaoGanDuTaskQiuQianWnd(taskId, eventName)
	CLuaNpcHaoGanDuZhanBuWnd.m_TaskID = taskId
	CLuaNpcHaoGanDuZhanBuWnd.m_EventName = eventName
	CUIManager.ShowUI(CLuaUIResources.NpcHaoGanDuZhanBuWnd)
end

function Gas2Gac.SendChuanjiabaoWashResult(itemId, newStar, wordIdU)
	local wordIdList = MsgPackImpl.unpack(wordIdU)
	local t = {}
	for i=1,wordIdList.Count do
		table.insert( t,wordIdList[i-1] )
	end
	local commonItem = CItemMgr.Inst:GetById(itemId)
	if commonItem then
		g_ScriptEvent:BroadcastInLua("SendChuanjiabaoWashResult",itemId,newStar,t)
	end
end


function Gas2Gac.SendPlayerName(playerId,name)
	if LuaDanMuMgr.Recording then
		LuaDanMuMgr:OnReceivePlayerName(playerId,name)--这种写法不好
	end
	g_ScriptEvent:BroadcastInLua("SendPlayerName",playerId,name)
end

function Gas2Gac.PlayerGetPickInHell(engineId)
	g_ScriptEvent:BroadcastInLua("PlayerCollectJieTuoGuo", engineId)
end

function Gas2Gac.ZhongQiuNpcTaskResult(npcId)
	g_ScriptEvent:BroadcastInLua("OnGetZQNPCSelect",npcId)
end

function Gas2Gac.PlayerGetPickInYueShenTemple(engineId, progress)
	-- print("PlayerGetPickInYueShenTemple", engineId, progress)
	g_ScriptEvent:BroadcastInLua("PlayerGetPickInYueShenTemple",engineId,progress)
end

-- 历史弹幕请求三个返回
function Gas2Gac.SendDanMuRecordBegin(key)
	LuaDanMuMgr:OnStartRecieveDanMuRecord(key)
end

function Gas2Gac.SendDanMuRecord(key, danMu_U)
	LuaDanMuMgr:OnReceiveDanMuRecord(key, danMu_U)
end

function Gas2Gac.SendDanMuRecordEnd(key)
	LuaDanMuMgr:OnReceiveDanMuRecordFinish(key)
end

function Gas2Gac.ClearLianShenSkillData()
	if not CClientMainPlayer.Inst then return end

	CClientMainPlayer.Inst.SkillProp.LianShenSkillLevel = CreateFromClass(MakeGenericClass(Dictionary, UInt32, byte))
	CClientMainPlayer.Inst.SkillProp.LianShenPracticeNum = CreateFromClass(MakeGenericClass(Dictionary, UInt32, UInt16))
end


function Gas2Gac.SendHousePrayResult(rengeList, digeList, tiangeList, score, panci, borrowLifeWordId, propMulti)
    CClientChuanJiaBaoMgr.Inst.newPrayResult = panci
    CClientChuanJiaBaoMgr.Inst.newScore = score
    CClientChuanJiaBaoMgr.Inst.newBorrowLifeId = borrowLifeWordId
    CommonDefs.DictClear(CClientChuanJiaBaoMgr.Inst.newPrayWordDic)
    CommonDefs.DictAdd(CClientChuanJiaBaoMgr.Inst.newPrayWordDic, typeof(EChuanjiabaoType), EChuanjiabaoType.TianGe, typeof(MakeArrayClass(UInt32)), CClientChuanJiaBaoMgr.Inst:GetList(tiangeList))
    CommonDefs.DictAdd(CClientChuanJiaBaoMgr.Inst.newPrayWordDic, typeof(EChuanjiabaoType), EChuanjiabaoType.DiGe, typeof(MakeArrayClass(UInt32)), CClientChuanJiaBaoMgr.Inst:GetList(digeList))
	CommonDefs.DictAdd(CClientChuanJiaBaoMgr.Inst.newPrayWordDic, typeof(EChuanjiabaoType), EChuanjiabaoType.RenGe, typeof(MakeArrayClass(UInt32)), CClientChuanJiaBaoMgr.Inst:GetList(rengeList))


    EventManager.Broadcast(EnumEventType.OnSendHousePrayResult)
end
function Gas2Gac.QueryPlayerNextTimePrayInfoResult(qishu, prob, wordLvDiff, borrowLifeProb, propMulti)
	g_MessageMgr:ShowMessage("JIAYUAN_QIFU_TIP", SafeStringFormat3("%d",qishu), SafeStringFormat3("%0.3f",prob), SafeStringFormat3("%d",wordLvDiff), SafeStringFormat3("%0.3f",borrowLifeProb) )
end

function Gas2Gac.QueryMyGmIdResult(gmId)
	if not CClientMainPlayer.Inst then return end
	CWebBrowserMgr.Inst:OpenUrl('https://research-game.163.com/htmls/jume3t/paper.html?uid='..CClientMainPlayer.Inst.Id)
end

function Gas2Gac.LoadScenePhotoFilter(bOpen, color, density)
	local ColorUtility = import "UnityEngine.ColorUtility"
	if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        if bOpen then
			local ret, colorNum = ColorUtility.TryParseHtmlString(color)
			postEffectCtrl:SetScenePhotoFilter(colorNum, density)
		else
			postEffectCtrl:RemoveScenePhotoFilter()
		end
	else
		if bOpen then
			local ret, colorNum = ColorUtility.TryParseHtmlString(color)
			CPostEffectMgr.Instance:LoadScenePhotoFilter(colorNum, density)
		else
			CPostEffectMgr.Instance:RemoveScenePhotoFilter()
		end
	end
end

--[[
    @desc: AI用 山鬼刷花接口
    author:CodeGize
    time:2019-07-31 14:56:31
    --@bShow: true-显示；false-隐藏
	--@posX: 中心点X
	--@posY: 中心点Y
	--@speed: 扩散速度
    @return:
]]
function Gas2Gac.ShowSceneObjectInCopyMap(bShow,posX,posY,speed)
	CRenderScene.Inst:SetFlowerCircle(bShow,posX,posY,speed)
end

function Gas2Gac.UpdateAllBindRepo(simpleItem_U)
	CClientMainPlayer.Inst.ItemProp.SimpleItems:LoadFromString(simpleItem_U)
	g_ScriptEvent:BroadcastInLua("OnUpdateAllBindRepo")
end

function Gas2Gac.RequestOpenBindRepoWndResult(isRiderOk, usableSize, riderAllowOpenSize)

end


function Gas2Gac.OpenUrlWithoutBuildIn(url)
	CommonDefs.OpenURL(url)
end

function Gas2Gac.SyncHellTreasureBoxPos(boxInfo_U)

	local info = MsgPackImpl.unpack(boxInfo_U)
	local pin_table = {}
	for i = 0, (info.Count - 1), 3 do
		table.insert(pin_table, {playerId = info[i], x = info[i+1], y = info[i+2]})
	end
	LuaWuJianDiYuMgr:OnSyncPinPoints(pin_table)
end

-- 打开使用某个任务道具的弹窗
function Gas2Gac.SyncUseTaskItemTip(templateId)
	CLuaItemMgr.ShowFastUse(templateId,EnumItemPlace.Task)
end

-- 关闭使用某个任务道具的弹窗
function Gas2Gac.SyncDestroyUseTaskItemTip()
	CLuaItemMgr.CloseFastUse()
end

function Gas2Gac.MapEditorOpenSearchNpcWnd()
	if not CLuaMapEditMgr then return end
	if not CLuaMapEditMgr:CheckInMapEditorPlay() then return end
	CUIManager.ShowUI(CLuaUIResources.MapEditSearchWnd)
end

function Gas2Gac.MapEditorExportNpcDataResult(str)
	if not CLuaMapEditMgr then return end
	Clipboard.SetClipboradText(str)
	g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("NPC数据已复制到剪贴板"))
end

function Gas2Gac.MapEditorQueryRecordResult(recordResult_U)
	if not CLuaMapEditMgr then return end
	if not CLuaMapEditMgr:CheckInMapEditorPlay() then return end

	CLuaMapEditMgr:OnQueryRecordResult(recordResult_U)
end

function Gas2Gac.MapEditorCloseLoadWnd()
	if not CLuaMapEditMgr then return end
	if not CLuaMapEditMgr:CheckInMapEditorPlay() then return end

	CUIManager.CloseUI(CLuaUIResources.MapEditLoadWnd)
end

function Gas2Gac.MapEditorOpenSearchMonsterWnd()
	if not CLuaMapEditMgr then return end
	if not CLuaMapEditMgr:CheckInMapEditorPlay() then return end
	CUIManager.ShowUI(CLuaUIResources.MapEditMonsterWnd)
end

LuaAuthenData = {}

function Gas2Gac.SendAuthenticationQuestion(playId, currentIndex, correctCount, questionInfoUD)
	-- questionInfoUD => {question, answer1, answer2, answer3, answer4}
	local list = MsgPackImpl.unpack(questionInfoUD)
	local authData = Authentication_Authentication.GetData(playId)
	if not authData then
		return
	end

	LuaAuthenData.m_Title = authData.Title
	LuaAuthenData.m_Question = list[0]
	LuaAuthenData.m_CurrentQuestionIndex = currentIndex
	LuaAuthenData.m_CorrectQuestionCount = correctCount
	LuaAuthenData.m_TotalQuestionCount = authData.TotalQuestionCount
	LuaAuthenData.m_TotalScore = correctCount * authData.Score
	LuaAuthenData.m_PassScore = authData.SuccessThreshold * authData.Score
	LuaAuthenData.m_AnswerList = {list[1],list[2],list[3],list[4]}
	LuaAuthenData.m_QuestionCountDown = authData.CountdownDuration

	if CUIManager.IsLoaded("DoubleOneExamWnd") then
		g_ScriptEvent:BroadcastInLua("AuthenInfoUpdate")
	else
		CUIManager.ShowUI("DoubleOneExamWnd")
	end
end

function Gas2Gac.SendAuthenticationCorrectChoice(correctChoice)
	LuaAuthenData.m_RightAnswerIndex = correctChoice
	g_ScriptEvent:BroadcastInLua("AuthenAnswerUpdate")
end

function Gas2Gac.FinishAuthentication(playId, correctCount)
	local authData = Authentication_Authentication.GetData(playId)
	if not authData then
		return
	end
	LuaAuthenData.m_ResultScore = correctCount * authData.Score
	LuaAuthenData.m_PassScore = authData.SuccessThreshold * authData.Score

	CUIManager.CloseUI("DoubleOneExamWnd")
	CUIManager.ShowUI("DoubleOneExamResultWnd")
end

-- 关闭倒计时窗口
function Gas2Gac.DestroyPlayCountWnd()
	CUIManager.CloseUI(CUIResources.CenterCountdownWnd)
end

local FMOD_StudioSystem = import "FMOD_StudioSystem"
local Vector3 = import "UnityEngine.Vector3"
-- 播放声音
function Gas2Gac.PlaySound(path)
	FMOD_StudioSystem.instance:PlayOneShot(path,Vector3.zero)
end

------BEGIN 物品掉落优化 先暂时放到这里
local CFakeDropItemObject = import "L10.Game.CFakeDropItemObject"
local CClientDropItemMgr = import "L10.Game.CClientDropItemMgr"
function Gas2Gac.ShowBatchMonsterDropItem(x, y, z, itemData_U)
	local data = MsgPackImpl.unpack(itemData_U)
	if not data or data.Count==0 then return end
	--[[
	x,y为pixel pos
	服务器定义的数据结构
	local appearData = {
				item:GetId(), templateId, isDropping, dropCharacterEngineId,
				color, wordCount, count, straightDrop,
				originalHeight, isFake, displayColorId, hole,
				isNiTian, isPrecious, pickable, name,
				cannotBeAutoPick}

	测试发现，批量掉落多件物品时，第一个的dropCharacterEngineId为nil，这会导致unpack出来的结果不正常
	]]


	local itemId = tostring(data[0])
	local templateId = tonumber(data[1])
	local isDropping = data[2] and data[2] or false
	local dropCharacterEngineId = tonumber(data[3])
	local color = tonumber(data[4])
	local wordCount = tonumber(data[5])
	local count = tonumber(data[6])
	local straightDrop = data[7] and data[7] or false
	local originalHeight = tonumber(data[8])
	local isFake = data[9] and data[9] or false
	local displayColorId = tonumber(data[10])
	local hole = tonumber(data[11])
	local isNiTian = data[12] and data[12] or false
	local isPrecious = data[13] and data[13] or false
	local pickable = data[14] and data[14] or false
	local name = tostring(data[15])
	local cannotBeAutoPick = data[16] and data[16] or false

	local itemObj = CFakeDropItemObject.CreateCFakeDropItemObject(itemId, x, y, z)
	if itemObj then
		itemObj:ShowItemObject(itemId, templateId, isDropping, dropCharacterEngineId, color,
			wordCount, count, straightDrop, originalHeight, isFake,
			displayColorId, hole, isNiTian, isPrecious, pickable, name, cannotBeAutoPick)
	end
end

function Gas2Gac.PickBatchItemFailed(itemId)
	local itemObj = CClientDropItemMgr.Inst:GetObject(itemId)
    if itemObj then
    	itemObj:Drop()--拾取失败时，播放掉落效果
    end
end

function Gas2Gac.AutoPickBatchItemSuccess(itemId)
	local itemObj = CClientDropItemMgr.Inst:GetObject(itemId)
    if itemObj then
    	itemObj:AddPickFx()--拾取成功时，播放拾起效果
    end
end

function Gas2Gac.BatchItemAutoDestroy(itemId)
	local itemObj = CClientDropItemMgr.Inst:GetObject(itemId)
    if itemObj then
    	itemObj:Destroy()
    end
end
------END 物品掉落优化


function Gas2Gac.SendCustomRpc(type, contentUD)

end

local CTrackMgr = import 'L10.Game.CTrackMgr'
-- 停止寻路
function Gas2Gac.TryStopTrack()
  CTrackMgr.Inst:Stop()
  CTrackMgr.Inst:CleanCrossMapState()

  if CClientMainPlayer.Inst then
    CClientMainPlayer.Inst:StopMove()
  end
end

-- 智能客服的token, QuerySmartGMTokenAndHost 对应的返回函数
function Gas2Gac.SyncSmartGMTokenAndHost(host, token, expire, refer)
	CWebBrowserMgr.Inst:OpenUrl(refer)
end

function Gas2Gac.EnterOrLeaveGameVideoScene(bEnter)
	local CMachineLearningCacheMgr = import "L10.Game.CMachineLearningCacheMgr"
	local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
	local playerid = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	CMachineLearningCacheMgr.SetGameVideoFlag(bEnter)
	CMachineLearningCacheMgr.UploadAndGetResult(CMachineLearningCacheMgr.s_UploadGameInfoUrl, "{\"PlayerId\":"..playerid..",\""..(bEnter and "begin" or "done").."\":1}")
end


function Gas2Gac.QueryPlayerCurrentPrayWordsResult(engineId, buffUD, wordsList)
    if wordsList.Length > 0 then
        local info = CPlayerHousePrayWordsType()
        info:LoadFromString(wordsList)
        if info == nil then
            return
        end
        local buff = CCppBuff()
        buff:LoadFromString(buffUD)
        g_ScriptEvent:BroadcastInLua("OnQueryOtherPlayerPrayInfoResult",engineId,info,buff)
    end
end
function Gas2Gac.RequestConfirmChangePlayerName(token)
	Gac2Gas.ConfirmChangePlayerName(token)
end

function Gas2Gac.ItemUseCheckBagSpaceFail(itemId, context)
	if context == "ItemQuickUse" then
		CItemInfoMgr.ShowQuickUseItem(itemId)
	end
end

function Gas2Gac.ClearPlayerTitleInPlay()
	CGamePlayMgr.Inst:ClearPlayerTitleInPlay()
end

function Gas2Gac.SyncPlayerTitleInPlay(playerId, titleName, color)
	titleName = SafeStringFormat3("[%s]%s",color,titleName)
	CGamePlayMgr.Inst:SyncPlayerTitleInPlay(playerId,titleName)
end

function Gas2Gac.SyncTieChunLianPlayScore(score)
	g_ScriptEvent:BroadcastInLua("SyncTieChunLianPlayScore",score)
end

function Gas2Gac.SyncTieChunLianPlayResult(score)
	CLuaTieChunLianResultWnd.m_Score = score
	CUIManager.ShowUI(CLuaUIResources.TieChunLianResultWnd)
end

function Gas2Gac.StartTrackToDynamicNpc(npcTemplateId, sceneTemplateId, x, y, z)
	CTrackMgr.Inst:FindDynamicNpc("", sceneTemplateId, npcTemplateId, x, y, z)
end

function Gas2Gac.EnterEnjoySceneryMode(rzy_U, cameraLocalPositionTable_U)
	local rzy = MsgPackImpl.unpack(rzy_U)
	local cameraLocalPositionTable = MsgPackImpl.unpack(cameraLocalPositionTable_U)
	if not rzy or not cameraLocalPositionTable then
		return
	end

	if CClientMainPlayer.Inst then
		local zuoqiId = CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId
		if zuoqiId and zuoqiId~="" then
			Gac2Gas.RequestRideOffZuoQi(zuoqiId)
		end

		CLuaScreenCaptureWnd.bInScenePhoto = true
		CLuaScreenCaptureWnd.ScenePhotoId = 0

		CLuaScreenCaptureMgr.CustomPhotoCameraPos = {
			rzy = Vector3(rzy[0], rzy[1], rzy[2]),
			pos = {
				cameraLocalPositionTable[0],
				cameraLocalPositionTable[1],
				cameraLocalPositionTable[2],
				cameraLocalPositionTable[3],
				cameraLocalPositionTable[4],
				cameraLocalPositionTable[5]
			}
		}
		CUIManager.ShowUI(CUIResources.ScreenCaptureWnd)
	end
end

function Gas2Gac.SendChunJie2020SeriesTasksRewardStatus(canGetReward, rewardGot)
	g_ScriptEvent:BroadcastInLua("SendChunJie2020SeriesTasksRewardStatus", canGetReward, rewardGot)
end

function Gas2Gac.GetChunJie2020SeriesTasksRewardSuccess()
	g_ScriptEvent:BroadcastInLua("GetChunJie2020SeriesTasksRewardSuccess")
end

function Gas2Gac.SyncGameVideoTargetPos(px, py)
	local CMachineLearningCacheMgr = import "L10.Game.CMachineLearningCacheMgr"
	CMachineLearningCacheMgr.SetGameVideoTargetPos(px, py)
end

function Gas2Gac.SyncSingletonShopOnshelfStatus(onShelfUd)
	g_ScriptEvent:BroadcastInLua("SyncSingletonShopOnshelfStatus", onShelfUd)
end

function Gas2Gac.SyncSingletonShopBuyResult(bSuccess, onShelfUd)
	g_ScriptEvent:BroadcastInLua("SyncSingletonShopBuyResult", bSuccess, onShelfUd)
end

function Gas2Gac.SetPlayerInterestedItemsSuccess(itemsUD)
end

function Gas2Gac.ReplyPlayerXiaoYaoExpressionUnLockInfo(playerId, bUnLock)
end

function Gas2Gac.AddSceneSyncGroupPlayerId(syncGroupId, playerId)

end

function Gas2Gac.DelSceneSyncGroupPlayerId(syncGroupId, playerId)

end

function Gas2Gac.AddObjSyncGroupPlayerId(syncGroupId, playerId)

end

function Gas2Gac.DelObjSyncGroupPlayerId(syncGroupId, playerId)

end

local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local HousePopupMenuItemData=import "L10.UI.HousePopupMenuItemData"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
-- 查询秋千状态
function Gas2Gac.QueryPuJiaCunQiuQianInfoResult(furnitureId, userId)
	if userId > 0 then
		if userId ~= CClientMainPlayer.Inst.Id then
			local fur = CClientFurnitureMgr.inst:GetFurniture(furnitureId)
			if fur then
				local itemActionPairs = {} --CLuaHouseMgr.GetPopupMenuItemData(this)
				table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
					Gac2Gas.RequestPushPuJiaCunQiuQian(furnitureId)
				end), 'UI/Texture/Transparent/Material/housepopupmenu_tuiqiuqian.mat', true, true, -1))
				if #itemActionPairs > 0 then
					CHousePopupMgr.ShowHousePopupMenu(Table2Array(itemActionPairs,MakeArrayClass(HousePopupMenuItemData)), fur.RO:GetSlotTransform("TopAnchor", false))
				end
			end
		else
			local fur = CClientFurnitureMgr.inst:GetFurniture(furnitureId)
			if fur then
				local itemActionPairs = {} --CLuaHouseMgr.GetPopupMenuItemData(this)
				table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
					g_MessageMgr:ShowMessage('PJCQiuQian_Push_Self')
				end), 'UI/Texture/Transparent/Material/housepopupmenu_tuiqiuqian.mat', true, true, -1))
				if #itemActionPairs > 0 then
					CHousePopupMgr.ShowHousePopupMenu(Table2Array(itemActionPairs,MakeArrayClass(HousePopupMenuItemData)), fur.RO:GetSlotTransform("TopAnchor", false))
				end
			end
		end
	else
		local fur = CClientFurnitureMgr.inst:GetFurniture(furnitureId)
		if fur then
			local itemActionPairs = {} --CLuaHouseMgr.GetPopupMenuItemData(this)
			local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
			table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
				fur:RequestUseFurniture(1, zswdata)
			end), 'UI/Texture/Transparent/Material/housepopupmenu_shiyong.mat', true, true, -1))
--			table.insert( itemActionPairs,  HousePopupMenuItemData("", DelegateFactory.Action_int(function (index)
--				this:RequestUseFurniture(2, zswdata)
--			end), 'UI/Texture/Transparent/Material/housepopupmenu_tuiqiuqian.mat', true, true, -1))

			if #itemActionPairs > 0 then
				CHousePopupMgr.ShowHousePopupMenu(Table2Array(itemActionPairs,MakeArrayClass(HousePopupMenuItemData)), fur.RO:GetSlotTransform("TopAnchor", false))
			end
		end
	end
end

--
function Gas2Gac.PushPuJiaCunQiuQianSuccess(furnitureId, userId)
	local fur = CClientFurnitureMgr.inst:GetFurniture(furnitureId)
	if fur then
		fur.RO:DoAni("stand03", true, 0, 1.0, 0.15, true, 1.0)
	end
end


function Gas2Gac.OnRushEnd(x, y)
	local pos = CPos(x, y)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst:SetPos(pos)
	end
end



local CoreScene = import "L10.Engine.Scene.CoreScene"
function Gas2Gac.LuaSetMainScene(metaSceneName)
	if not metaSceneName or metaSceneName == "" then return end
	local sceneName = metaSceneName:sub(metaSceneName:find("/[^/]*$") + 1)
	if not sceneName or sceneName == "" then return end
	CoreScene.s_IsPreloadScene = true
	CoreScene.RemoteSetMainScene(sceneName)
	LuaNetworkMgr:ResetDelayTime()
end


function Gas2Gac.OpenActivityWithBuiltInExplorer(activityName)
	if activityName=="JiaYuan2022Index" or string.find(activityName,"JiaYuan2022Index@") then
		CLuaWebBrowserMgr.OpenHouseActivityUrl(activityName)
		return
	end
	CWebBrowserMgr.Inst:OpenActivityUrl(activityName)
end

local NativeTools = import "L10.Engine.NativeTools"
local PlayerSettings = import "L10.Game.PlayerSettings"
local MessageWndManager = import "L10.UI.MessageWndManager"
function Gas2Gac.CheckEarphone(ud)

	local dict = MsgPackImpl.unpack(ud)
	if not dict or not dict["NotSupportTip"] or not dict["EarphoneWithoutSoundTip"] then return end

	--PC和老版本没有登记权限的安卓不支持检测耳机接口,这些设备上直接调用会一直返回false
	local isSupport = true
	if Application.platform == RuntimePlatform.WindowsPlayer or
	(Application.platform == RuntimePlatform.Android and
	not NativeTools.HasUserAuthorizedPermission(NativeTools.PermissionNames.MODIFY_AUDIO_SETTINGS)) then
		isSupport = false
	end

	--不支持和没带耳机都显示一下原有tip
	if not isSupport or not NativeTools.IsWearingEarphone() then
		g_MessageMgr:ShowMessage(dict["NotSupportTip"])
		return
	end

	--带了耳机没开声音
	if not PlayerSettings.MusicEnabled or not PlayerSettings.SoundEnabled then
		local msg = g_MessageMgr:FormatMessage(dict["EarphoneWithoutSoundTip"])
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
			PlayerSettings.MusicEnabled = true
			PlayerSettings.SoundEnabled = true
		end), nil, nil, nil, false)
	end
end

function Gas2Gac.PlayerOverSeaFlagChange(flag)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp then
		CClientMainPlayer.Inst.BasicProp.ClientData.Extra.OverSeaPlayerFlag = flag
	end
end


function Gas2Gac.PlayerUpdateDayHuoLi(newDayHuoLi, newExpireTime)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
		CClientMainPlayer.Inst.PlayProp.DayHuoLi = newDayHuoLi
		CClientMainPlayer.Inst.PlayProp.DayHuoLiExpireTime = newExpireTime
	end

	g_ScriptEvent:BroadcastInLua("PlayerUpdateDayHuoLi")
end

function Gas2Gac.QueryWoodPileFightHistory_Result(pileType, fightStatics)
	local data = CWoodPileFightStatic()
	data:LoadFromString(fightStatics)
	g_ScriptEvent:BroadcastInLua("QueryWoodPileFightHistory_Result",pileType,data)
end

function Gas2Gac.HideSceneAllJiaJu()
	local dic = CClientFurnitureMgr.Inst.FurnitureList
	CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, fur)
		if fur.RO then
			fur.RO.Visible = false
		end
	end))
end

function Gas2Gac.OpenDonateSpokesmanHouseWndResult(isShowBtn)
	-- 1:show, 0: hide
	g_ScriptEvent:BroadcastInLua("OnRequestSpokesmanHouseBtnStatus",isShowBtn)
end

function Gas2Gac.ShowSceneAllJiaJu()
	local dic = CClientFurnitureMgr.Inst.FurnitureList
	CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, fur)
		if fur.RO then
			fur.RO.Visible = true
		end
	end))
end

function Gas2Gac.SyncSpokesmanOperatorId(designid,fansid,fake)
	CLuaSpokesmanHomeMgr.SyncSpokesmanOperatorId(designid,fansid,fake)
end

function Gas2Gac.StartFireWorkChapter(chapterId)
	CLuaQiXiMgr.ProcessFireworkChapter(chapterId,0)
end

function Gas2Gac.StartFireWorkChapterInCopy(chapterId, beginTime)
	CLuaQiXiMgr.ProcessFireworkChapter(chapterId,beginTime)
end

-- 2020七夕流星雨
function Gas2Gac.SyncQiXiLiuXingYu(kindUd)
	local kindList = MsgPackImpl.unpack(kindUd)
	if kindList.Count == 0 then
		Gas2Gac.StopQiXiLiuXingYu()
		return
	end
	for i = 0, kindList.Count - 1 do
		Gas2Gac.StartQiXiLiuXingYu(kindList[i])
	end
end

function Gas2Gac.ReplyScoreShopGoodsRestStock(shopId, itemTemplateId, stock)
	g_ScriptEvent:BroadcastInLua("OnReplyScoreShopGoodsRestStock",shopId, itemTemplateId, stock)
end

-- 绑定光源
function Gas2Gac.AttachLightToCharacter(engineId)
	CClientMainPlayer.Inst.SpotLightEnable = false
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj == nil then return end

	obj.SpotLightEnable = true
end

-- AI调节副本光照,0表示变亮
function Gas2Gac.SyncAdjustSceneLight(value)
	CLightMgr.Inst:SetEnvBrightness(1-value)
end

-- 显示七夕副本的天灯血条
function Gas2Gac.ShowQiXiTianDengHpBar(engineId, hp, hpFull)
	CLuaQiXiMgr.ShowQiXiTianDengHpBar(engineId, hp, hpFull)
end

function Gas2Gac.RequestTaskOpenTimeCheckResult(taskId, result, openTime)
	CLuaTaskMgr.OnRequestTaskOpenTimeCheckResult(taskId, result, openTime)
end

-- 清空购物车玩法数据统计
-- @param 积分, 各个颜色怪物数量对应表{key=colour, value=count}
-- @param 怪物位置, 如果是怪物死亡,table才会有值, {x, y, colour}, 其余为nil
-- @param 波数
function Gas2Gac.ClearTrolleyStatics(credit, monsterCountTbl, monsterInfo, round)
	local monsterCount = MsgPackImpl.unpack(monsterCountTbl)
	local monsterDeath = MsgPackImpl.unpack(monsterInfo)
	CLuaShuangshiyi2020Mgr:OnClearTrolleyStatics(credit, monsterCount, monsterDeath, round)
end

-- 清空购物车玩法结果
-- @param 积分, 奖品物品id
function Gas2Gac.ClearTrolleyResult(credit, rewardItemId)
	CLuaShuangshiyi2020Mgr:OnClearTrolleyResult(credit, rewardItemId)
end

-- 玩家种草的返回信息
-- @param 时装物品, 数量
function Gas2Gac.PlayerZhongCaoRet2020(mallItemId, count)
	g_ScriptEvent:BroadcastInLua("PlayerZhongCaoRet2020",mallItemId,count)
end

-- 查询所有种草信息
-- @param 当前进度, 数据
function Gas2Gac.QueryAllZhongCaoCountRet2020(progress, data)
	local t = {}
	local dic = MsgPackImpl.unpack(data)
	if dic then
        CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (k, v)
            t[tonumber(k)]=tonumber(v)
        end))
        g_ScriptEvent:BroadcastInLua("QueryAllZhongCaoCountRet2020",progress,t)
    end
end

-- 查询礼包与福利券兑换有关的返回信息
-- @param 礼包购买情况 	为数组,索引从小到大对应礼包金额从大到小
-- @param 福利券使用情况 为数组,索引从小到大对应礼包金额从大到小
function Gas2Gac.QueryDouble11VouchersInfoRet(packageInfo, voucherInfo)
	local pList = MsgPackImpl.unpack(packageInfo)
	local vList = MsgPackImpl.unpack(voucherInfo)
	CLuaShuangshiyi2020Mgr:OnQueryDouble11VouchersInfoRet(pList, vList)
end
-- 查询中奖信息返回信息
-- @param 状态
-- EnumDouble11LotteryStatus = { eNone = 0, eParticipating = 1, ePrepareing = 2, eProcessing = 3, ePublicity = 4, }
-- 客户端只能接到 0 2 4
-- @param 中奖信息 	[prize] => {{id=id, name=name, serverId=serverId, serverName=serverName, class=class}, ...}
function Gas2Gac.SendDouble11LotteryResult(status, data)
	if status == 4 then
		local lotteryData = MsgPackImpl.unpack(data)
		g_ScriptEvent:BroadcastInLua("SendDouble11LotteryResult",status,lotteryData)
	elseif status == 0 or status == 2 then
		g_ScriptEvent:BroadcastInLua("SendDouble11LotteryResult",status)
	end
end

-- 中奖玩家提示
-- @param 奖项类型
-- @param 期间累计消费灵玉
-- @param 返还灵玉数额
function Gas2Gac.PlayerHitDouble11RebateReward(prize, payJade, returnJade)
	CLuaShuangshiyi2020Mgr:RewardFirstShow(prize, payJade, returnJade)
end

function Gas2Gac.Halloween2020ArrestPlaySignUpResult(bSuccess)
	CLuaHalloween2020Mgr.Halloween2020ArrestPlaySignUpResult(bSuccess)
end

function Gas2Gac.Halloween2020ArrestPlayCancelSignUpResult(bSuccess)
	CLuaHalloween2020Mgr.Halloween2020ArrestPlayCancelSignUpResult(bSuccess)
end

function Gas2Gac.QueryHalloween2020ArrestPlayMatchInfoResult(bInMatching)
	CLuaHalloween2020Mgr.QueryHalloween2020ArrestPlayMatchInfoResult(bInMatching)
end

function Gas2Gac.QueryHalloween2020ArrestPlayInfoResult(winForce, attackData, defendData)
	CLuaHalloween2020Mgr.QueryHalloween2020ArrestPlayInfoResult(winForce, attackData, defendData)
end

function Gas2Gac.EnterHalloween2020ArrestPlay(force)
	CLuaHalloween2020Mgr.EnterHalloween2020ArrestPlay(force)
end

function Gas2Gac.ReplyHalloweenTerrifyAndDanShiInfo(terrifyCount, maxTerrifyCount, level, param1, param2)
	CLuaHalloween2020Mgr.ReplyHalloweenTerrifyAndDanShiInfo(terrifyCount, maxTerrifyCount, level, param1, param2)
end

function Gas2Gac.QueryHouseMemberAtHomeInfoResult(communityId, dataUD)
	CLuaHouseMgr.QueryHouseMemberAtHomeInfoResult(communityId, dataUD)
end

function Gas2Gac.SyncHalloween2020ArrestPlayInfo(restCount, totalCount, playData)
	CLuaHalloween2020Mgr.SyncHalloween2020ArrestPlayInfo(restCount, totalCount, playData)
end

-- 同步个人分数信息
-- @param sourceInfoUD 分数信息
-- {结果, 势力, 得分, 职业排名, 是否为本场最高分, 是否本势力最高分
-- 击杀数, 死亡数, 助攻数, 复活数,  最大连杀, 采集弹药数, 击杀boss数
-- 本场前最高得分, 本场前最高击杀数, 本场前最高死亡数, 本场前最高助攻数, 本场前最高复活数}
-- @param maxInfoUD 同阵营最大值信息 数组 对应在EnumGuanNingWeekDataKey
function Gas2Gac.UpdateAnQiIslandPlayerPersonalData(sourceInfoUD, maxInfoUD)
	CLuaAnQiDaoMgr.UpdateAnQiIslandPlayerPersonalData(sourceInfoUD, maxInfoUD)
end

-- 副本信息 防守->海沧 进攻->大福
-- @param cannonInfoUD 火炮信息 {防守方弹药总数, 防守方当前弹药, 防守方血量, 进攻方弹药总数, 进攻方当前弹药, 进攻方血量}
-- @param defendDataUD 防守方玩家信息 {id1, name1, class1, (kill+dead)score1, reloadScore1, ... }
-- @param attackDataUD 进攻方玩家信息 {id1, name1, class1, (kill+dead)score1, reloadScore1, ... }
-- @param isFinish 是否已进入结束状态
-- @param doubleTimes 玩家剩余双倍经验次数
function Gas2Gac.SendAnQiIslandPlayInfoResult(cannonInfoUD, defendDataUD, attackDataUD, isFinish, doubleTimes)
	CLuaAnQiDaoMgr.SendAnQiIslandPlayInfoResult(cannonInfoUD, defendDataUD, attackDataUD, isFinish, doubleTimes)
end

-- 本场最佳数据
-- @param maxDataInfo
-- {[EnumCommonForce.eDefend] = {[k1] = {playerIdTbl = {id1, ...}, value = 0}, [k2] = {...}},
--  [EnumCommonForce.eAttack] = {...}}
-- k1, k2, ... 参考EnumGuanNingWeekDataKey 27-31 36 37
function Gas2Gac.SendAnQiIslandMaxDataInfo(maxDataInfo)
	CLuaAnQiDaoMgr.SendAnQiIslandMaxDataInfo(maxDataInfo)
end

-- 玩家得分
-- @param score 		得分
-- @param exp 			经验
-- @param freeSilver 	银票
function Gas2Gac.UpdatePlayerAnQiIslandScore(score, exp, freeSilver)
	CLuaAnQiDaoMgr.UpdatePlayerAnQiIslandScore(score, exp, freeSilver)
end

-- 火炮发射弹药
-- @param force 势力
-- @param cnt 	发射次数
function Gas2Gac.AnQiIslandFireCannon(force, cnt)
	CLuaAnQiDaoMgr.AnQiIslandFireCannon(force, cnt)
end

-- 同步火炮信息
-- @param defInfo {防守方血量, 防守方弹药总数, 防守方当前弹药}
-- @param attInfo {进攻方血量, 进攻方弹药总数, 进攻方当前弹药}
function Gas2Gac.AnQiIslandSyncCannonInfo(defInfo, attInfo)
	CLuaAnQiDaoMgr.AnQiIslandSyncCannonInfo(defInfo, attInfo)
end

function Gas2Gac.ShowAnQiIslandResultWnd()
	CLuaAnQiDaoMgr.ShowAnQiIslandResultWnd()
end

-- @param fightTime 战斗开始后的时间
function Gas2Gac.SendAnQiIslandCurrTime(fightTime)
	CLuaAnQiDaoMgr.SendAnQiIslandCurrTime(fightTime)
end

-- @param pickInfo {eid1, x1, y1, eid2, x2, y2, ...}
-- @param bossInfo {eid1, x1, y1, eid2, x2, y2, ...}
function Gas2Gac.AnQiIslandSyncCannonBallAndBossPos(pickInfo, bossInfo)
	CLuaAnQiDaoMgr.AnQiIslandSyncCannonBallAndBossPos(pickInfo, bossInfo)
end

function Gas2Gac.ShowYuanDanHappyNewYearShareWnd()
	CUIManager.ShowUI(CLuaUIResources.YuanDanShareWnd)
end

-- xueRenNpcEngineId 雪人Npc
-- curStage为0时 也就是未开始时 ShengDan_SnowBall无数据 用雪人默认大小和名字
-- showFx是否播放模型缩放比例变化的特效
function Gas2Gac.SyncShengDanXueRenXueQiuNum(xueRenNpcEngineId, curStage, showFx)
	LuaChristmasMgr.SyncShengDanXueRenXueQiuNum(xueRenNpcEngineId, curStage, showFx)
end

function Gas2Gac.TangYuan2021PlayerEnd(duration, todayScore,score, rank)
	LuaYuanXiao2021Mgr:TangYuan2021PlayerEnd(duration, todayScore, score, rank)
end

function Gas2Gac.UpdateTangYuan2021PlayInfo(playInfo)
	local data = MsgPackImpl.unpack(playInfo)
	LuaYuanXiao2021Mgr:UpdateTangYuan2021PlayInfo(data)
end

-- 根据RO的Scale添加相同Scale的Fx
-- 仅仅在Fx表里没填Scale生效
function Gas2Gac.AddFxWithROScale(engineId, fxId)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	local adjust = 1
	-- 特殊处理一下Scale
	if fxId == 88801879 then
		adjust = 0.7
	end
	if obj ~= nil then
		obj.DelayExecuteQueue:AppendCommand(DelegateFactory.Action_uint(function (id)
			local fx = CEffectMgr.Inst:AddObjectFX(fxId, obj.RO, 0, obj.RO.Scale * adjust, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
			obj.RO:AddFX(fxId, fx, -1)
		end))
	end
end

-- xueQiuNum 雪人抓了多少雪球
function Gas2Gac.SyncShengDanCatchXueQiuNum(xueQiuNum)
	LuaChristmasMgr.SyncShengDanCatchXueQiuNum(xueQiuNum)
end


-- 玩家采集弹药后提示客户端放特效
-- @param pickEngineId 		采集物engineId
-- @param playerId 			玩家Id
function Gas2Gac.AnQiIslandPickCannonBall(pickEngineId, playerId)
	CLuaAnQiDaoMgr.AnQiIslandPickCannonBall(pickEngineId, playerId)
end

-- 玩家击杀boss后提示客户端放特效
-- @param force 			击杀方势力
function Gas2Gac.AnQiIslandBossKilled(force)
	CLuaAnQiDaoMgr.AnQiIslandBossKilled(force)
end

function Gas2Gac.PlayerRequestAutoLayoutCheckSuccess()
	CLuaClientFurnitureMgr.AutoPlaceFurniture()
end

function Gas2Gac.QueryTodayTangYuan2021Score_Result(todayScore)
	LuaYuanXiao2021Mgr:TodayTangYuanScore(todayScore)
end

function Gas2Gac.QueryPlayerIsMatchingTangYuan2021_Result(isMatching)
	g_ScriptEvent:BroadcastInLua("PlayerIsMatchingTangYuan2021_Result", isMatching)
end

function Gas2Gac.StartFlick(engineId, aniType)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj == nil then return end

	local aniData = Animation_FlickAni.GetData(aniType)
	if not aniData then return end

	obj.FlickingAniType = aniType
end

function Gas2Gac.AddSoulcoreLinkFxAndAniWithoutDuration(srcEngineId, dstEngineId, fxId)
    local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
	local dstObj = CClientObjectMgr.Inst:GetObject(dstEngineId)
	if srcObj and dstObj then
		srcObj:ShowActionState(EnumActionState.Casting, {0})
		local fx = CEffectMgr.Inst:AddLinkFX(fxId, srcObj.RO, dstObj.RO, 0, 1, -1)
	end
end

function Gas2Gac.RmSoulcoreLinkFxAndAni(srcEngineId, fxId)
    local srcObj = CClientObjectMgr.Inst:GetObject(srcEngineId)
    if srcObj then
		srcObj:ShowActionState(EnumActionState.Idle, {})
		local fx = srcObj.RO:GetFxById(fxId)
		if fx and fx.m_ROList.Length == 2 then
			local dstRO = fx.m_ROList[1]
			srcObj.RO:UnRefFX(fx)
			dstRO:UnRefFX(fx)
			fx:Destroy()
		end
    end
end

function Gas2Gac.SyncCondenseLeftTime(bPause, endTimestamp, leftTime, speed, phase, totalTime)
	LuaZongMenMgr:SyncCondenseLeftTime(bPause, endTimestamp, leftTime, speed, phase, totalTime)
end

function Gas2Gac.ReplySubmitCondenseItem(bOk, endTimestamp)
	LuaZongMenMgr:ReplySubmitCondenseItem(bOk, endTimestamp)
end

function Gas2Gac.SyncCondenseWeatherChange(speed, endTimestamp)
	LuaZongMenMgr:SyncCondenseWeatherChange(speed, endTimestamp)
end

function Gas2Gac.ReplyPauseCondense(bOk, endTimestamp, leftTime)
	LuaZongMenMgr:ReplyPauseCondense(bOk, endTimestamp, leftTime)
end

function Gas2Gac.ReplyResumeCondense(bOk, endTimestamp)
	LuaZongMenMgr:ReplyResumeCondense(bOk, endTimestamp)
end

function Gas2Gac.SyncFinishCarving()
	LuaZongMenMgr:SyncFinishCarving()
end

function Gas2Gac.SyncFinishCondense()
	LuaZongMenMgr:SyncFinishCondense()
end

function Gas2Gac.UpdateWallOrFloorPaperType(roomType, placeType, paperId)
	CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
	if not CClientHouseMgr.Inst.mCurFurnitureProp2 then return end
	if roomType == EnumHouseRoomType.Room then
		if placeType == EnumHousePaperPlaceType.Wall then
			CClientHouseMgr.Inst.mCurFurnitureProp2.RoomWallType = paperId
		elseif placeType == EnumHousePaperPlaceType.Floor then
			CClientHouseMgr.Inst.mCurFurnitureProp2.RoomFloorType = paperId
		end
	elseif roomType == EnumHouseRoomType.Xiangfang then
		if placeType == EnumHousePaperPlaceType.Wall then
			CClientHouseMgr.Inst.mCurFurnitureProp2.XiangfangWallType = paperId
		elseif placeType == EnumHousePaperPlaceType.Floor then
			CClientHouseMgr.Inst.mCurFurnitureProp2.XiangfangFloorType = paperId
		end
	end
	if placeType == EnumHousePaperPlaceType.Wall then
		CLuaHouseMgr.SetWallPaper()
	elseif placeType == EnumHousePaperPlaceType.Floor then
		CLuaHouseMgr.SetFloorPaper()
	end
	g_ScriptEvent:BroadcastInLua("UpdateCurrentWallPaper")
end

-- 临时积分 积分商店会用 添加以下Enum 第一个字段从1开始
-- EnumTempPlayScoreKey = {

-- 	Max = 64,
-- }
local CPlayScoreInfo=import "L10.Game.CPlayScoreInfo"
function Gas2Gac.SyncTempPlayScore(key, score, expiredTime)
	if not CClientMainPlayer then return end

	local info = CPlayScoreInfo()
	info.ExpiredTime = expiredTime
	info.Score = score
	CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayScore,key,info)
	g_ScriptEvent:BroadcastInLua("SyncTempPlayScore", key,score,expiredTime)
end

-- 查询到的邪派值, bSuccess 如果是false,那么查询的宗门不存在
-- evilValue 邪派值
-- bEvil 是否为邪派
function Gas2Gac.SyncSectEvilValue(bSuccess, sectId, evilValue, bEvil)
	LuaZongMenMgr:SyncSectEvilValue(bSuccess, sectId, evilValue, bEvil)
end

--bIIIIIIIdIIdddd
function Gas2Gac.QuerySectFaZhenCurrentInfoResult(isInputSoulCore, templateId, restTime, currentLingLi, lingliLimit,
        myWuXing, fazhenWuXing, nextRefreshTime, linggen, quality, isOwner, menpaiAddFactor, xiepaiAddFactor, wuxingFactor, bInputItem, beizhuoFactor, weatherFactor, damageFactor, lingfuCount, lingfuFactor, lingfuRemainTime, baoyangFactor, baoyangRemainTime)
	local ret = {}
	ret["isInputSoulCore"] = isInputSoulCore
	ret["templateId"] = templateId
	ret["restTime"] = restTime
	ret["currentLingLi"] = currentLingLi
	ret["lingliLimit"] = lingliLimit
	ret["myWuXing"] = myWuXing
	ret["fazhenWuXing"] = fazhenWuXing
	ret["nextRefreshTime"] = nextRefreshTime
	ret["linggen"] = linggen
	ret["quality"] = quality
	ret["isOwner"] = isOwner
	ret["menpaiAddFactor"] = menpaiAddFactor
	ret["xiepaiAddFactor"] = xiepaiAddFactor
	ret["wuxingFactor"] = wuxingFactor
	ret["bInputItem"] = bInputItem
	ret["beizhuoFactor"] = beizhuoFactor
	ret["weatherFactor"] = weatherFactor
	ret["damageFactor"] = damageFactor
	ret["lingfuFactor"] = lingfuFactor
	ret["lingfuCount"] = lingfuCount
	ret["lingfuRemainTime"] = lingfuRemainTime
	ret["baoyangFactor"] = baoyangFactor
	ret["baoyangRemainTime"] = baoyangRemainTime
	g_ScriptEvent:BroadcastInLua("QuerySectFaZhenCurrentInfoResult", ret)
end

function Gas2Gac.PutSoulCoreIntoFaZhenSuccess()
	g_ScriptEvent:BroadcastInLua("PutSoulCoreIntoFaZhenSuccess")
end

function Gas2Gac.PutMonsterIntoFaZhenSuccess()
	g_ScriptEvent:BroadcastInLua("PutMonsterIntoFaZhenSuccess")
end

function Gas2Gac.QueryFaZhenLianHuaMonsterQueueInfoResult(dataUD)
	local ret = MsgPackImpl.unpack(dataUD)
	g_ScriptEvent:BroadcastInLua("QueryFaZhenLianHuaMonsterQueueInfoResult", ret)
end

function Gas2Gac.RemoveFaZhenLianHuaMonsterSuccess(index, id)
	g_ScriptEvent:BroadcastInLua("RemoveFaZhenLianHuaMonsterSuccess", index, id)
end

function Gas2Gac.QuerySectPersonalInputItemInfoResult(nextRefreshTime, dataUD, advancedItemDataUD, submitTypeDataUD)
	local data = MsgPackImpl.unpack(dataUD)
	local advancaData = MsgPackImpl.unpack(advancedItemDataUD)
	local typeData = MsgPackImpl.unpack(submitTypeDataUD)
	g_ScriptEvent:BroadcastInLua("QuerySectPersonalInputItemInfoResult", nextRefreshTime, data, advancaData, typeData)
end

function Gas2Gac.QuerySectTopInputItemInfoResult(nextRefreshTime, dataUD)
	local data = MsgPackImpl.unpack(dataUD)
	g_ScriptEvent:BroadcastInLua("QuerySectTopInputItemInfoResult", nextRefreshTime, data)
end

function Gas2Gac.PutItemIntoFaZhenSuccess(templateId)
	g_ScriptEvent:BroadcastInLua("PutItemIntoFaZhenSuccess", templateId)
end

function Gas2Gac.QueryLianHuaItemSubmitHistroyResult(dataUD)
	local data = MsgPackImpl.unpack(dataUD)
	g_ScriptEvent:BroadcastInLua("QueryLianHuaItemSubmitHistroyResult", data)
end

function Gas2Gac.ShowHongBaoCanSnatchIcon(channelType)
	EventManager.BroadcastInternalForLua(EnumEventType.PlayerSendHongBao, {channelType, 1})
end

function Gas2Gac.SyncSectFaZhenPeopleInfo(sectId, playerId1, engineId1, time1, playerId2, engineId2, time2, isXiePai, wuxing)
	LuaZongMenMgr:SyncLianRenStatus(sectId, playerId1, engineId1, time1, playerId2, engineId2, time2, isXiePai, wuxing)
end

function Gas2Gac.SyncSectFaZhenMonsterInfo(sectId, templateId, costTime, beginTime, isXiePai, wuxing, lingheAppearanceData)
	LuaZongMenMgr:SyncLianYaoStatus(sectId, templateId, costTime, beginTime, isXiePai)
	LuaZongMenMgr.m_CurrentSectId = sectId
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
		LuaZongMenMgr:StartLianHuaFaZhenBtnTick(templateId~=0)
	end
	local list = MsgPackImpl.unpack(lingheAppearanceData)
	LuaZongMenMgr:SyncLingHeStatus(list)
end

function Gas2Gac.ReleaseFaZhenBeiZhuoPlayer(index, playerId)
	LuaZongMenMgr:ReleaseFaZhenBeiZhuoPlayer(index, playerId)
end

function Gas2Gac.NewHouseWallFloorPaperUnlocked(houseId, paperType, isInDuration,oodTime)
	CLuaHouseMgr.NewHouseWallFloorPaperUnlocked(houseId, paperType, isInDuration,oodTime)
end

-- 提交妖怪更改邪派值, bSuccess 如果是false,那么本次操作没有成功
-- evilValue 邪派值
-- bEvil 是否为邪派
function Gas2Gac.ReplySubmitSectEvilYaoGuai(bSuccess, sectId, evilValue, bEvil)
	LuaZongMenMgr:ReplySubmitSectEvilYaoGuai(bSuccess, sectId, evilValue, bEvil)
end

function Gas2Gac.RequestUseKunXianSuoResult(playerId, targetId, result)
	g_ScriptEvent:BroadcastInLua("RequestUseKunXianSuoResult", playerId, targetId, result)
end

function Gas2Gac.RequestUseKunXianSuoCheckResult(targetId, result)
	g_ScriptEvent:BroadcastInLua("RequestUseKunXianSuoCheckResult", targetId, result)
end

function Gas2Gac.ShowSceneItemHighlight(sceneId, itempath, time)
	local sectScene = {16101788, 16101789, 16101790, 16101791, 16101792, 16101793}
	sectScene[16101788] = 1
	sectScene[16101789] = 1
	sectScene[16101790] = 1
	sectScene[16101791] = 1
	sectScene[16101792] = 1
	sectScene[16101793] = 1

	if sceneId ~= CScene.MainScene.SceneTemplateId then
		if sectScene[sceneId] == nil or sectScene[CScene.MainScene.SceneTemplateId] == nil then
			return
		end
	end

	local outlineMat = CreateFromClass(Material, CClientFurniture.s_OutlinedCjbEffectMaterial)
    local color = Color.white
    color.a = 0.5
    outlineMat:SetColor("_OutlineColor", color)

    local obj = CRenderScene.Inst and CRenderScene.Inst.transform:Find(itempath)
	if obj then
		-- 设置闪光
		local renderers = CommonDefs.GetComponentsInChildren_Component_Type(obj, typeof(Renderer))
		for i=0, renderers.Length-1 do
			local renderer = renderers[i]
            renderer.materials = Table2Array({renderer.materials[0], outlineMat}, MakeArrayClass(Material))
		end
		
		local pos = obj.transform.position
		-- 当前的场景相机
		local rotate = CameraFollow.Inst.rotateObj
		CameraFollow.Inst:BeginAICamera(rotate.position, rotate.eulerAngles, 3600, false)
		-- 计算新的相机位置
		local ea = rotate.eulerAngles
		local d = CommonDefs.op_Multiply_Vector3_Single(rotate.forward, -15)
		local pos = CommonDefs.op_Addition_Vector3_Vector3(pos, d)
		CameraFollow.Inst:AICameraMoveTo(pos, ea, time / 3)

		RegisterTickOnce(function()
			CameraFollow.Inst:EndAICamera()
			if CRenderScene.Inst then
				local obj = CRenderScene.Inst.transform:Find(itempath)
				if obj~=nil then
					local renderers = CommonDefs.GetComponentsInChildren_Component_Type(obj, typeof(Renderer))
					for i=0, renderers.Length-1 do
						local renderer = renderers[i]
						renderer.materials = Table2Array({renderer.materials[0]}, MakeArrayClass(Material))
					end
				end
			end
		end, time*1000)
	end 
end

function Gas2Gac.PlayLianHuaZhuoRenFx(playerEngineId, attackerEngineId)
	LuaZongMenMgr:AddZhuoRenFx(playerEngineId, attackerEngineId)
end

function Gas2Gac.ShowEyeCloseEffect(durationTime, blinkType, blinkTotalTime)
	NewPostEffectMgr.SetEyeCloseOnCamera(CUIManager.instance.MainCamera.gameObject, durationTime, blinkType, blinkTotalTime, true)
end

function Gas2Gac.PlayerPressQiaoqiaoban(fid, playerId)
	g_ScriptEvent:BroadcastInLua("PlayerPressQiaoqiaoban", fid, playerId)
end

function Gas2Gac.ReplyCrossGnjcGroupInfoQueryResult(group, dataUD, count)

	local ids = {}
	local names = {}
	local list = MsgPackImpl.unpack(dataUD)
	for i = 0, list.Count - 1, 2 do
		-- print(list[i], list[i+1])
		local id = list[i]
		if id~=group then
			table.insert( ids,id )--server group id
			table.insert( names,list[i+1] )--server group name
		end
	end
	g_ScriptEvent:BroadcastInLua("ReplyCrossGnjcGroupInfoQueryResult", group,count,ids,names)
end

function Gas2Gac.ReplyCrossGnjcServerInfoQueryResult(group, dataUD)

	local t = {}
	local list = MsgPackImpl.unpack(dataUD)
	for i = 0, list.Count - 1, 2 do
		-- print(list[i], list[i+1])
		-- table.insert( t,list[i] )--server id
		table.insert( t,list[i+1] )--server name
	end
	g_ScriptEvent:BroadcastInLua("ReplyCrossGnjcServerInfoQueryResult", group,t)
end

function Gas2Gac.UpdateHousePool(houseId, poolud, warmpoolud)
	if CUIManager.IsLoaded(CLuaUIResources.PoolEditWnd) then
		LuaPoolMgr.OnUpdateHousePool(houseId, poolud, warmpoolud,false)
	else
		LuaPoolMgr.OnUpdateHousePool(houseId, poolud, warmpoolud,true)
	end
end

-- 可以和策划约定流程图里传duration=-1为常开
function Gas2Gac.PlayBigPicture(path, duration)
	CZhuJueJuQingMgr.Instance.PicTexturePath = path;
	CZhuJueJuQingMgr.Instance.PicDuduration = duration;
	CUIManager.ShowUI(CLuaUIResources.BigJuqingPicWnd)
end

-- 镖车血量 坐标X 坐标Y 任务进程 镖车EngineId
function Gas2Gac.SyncShengChenGangBiaoCheInfo(hpPercent, posX, posY, playProgress, biaoCheEngineId)

	LuaZhouNianQing2021Mgr.UpdateShengChenGangPlayInfo( hpPercent ,posX , posY, playProgress, biaoCheEngineId)
end

-- 三个参数分别是 宗门id，是否开启，剩余时间
function Gas2Gac.SyncXiepaiExtraRewardSwitch(sectId, bOpen, leftTime, expCost)

	LuaZongMenMgr:SyncXiepaiExtraRewardSwitch(sectId, bOpen, leftTime, expCost)
end

-- @param hitType	0:加速结束
-- 					1:未在额外加速状态下撞击加速点
-- 					2:在额外加速状态下撞进正确颜色加速点
-- 					3:在额外加速状态下撞击错误颜色加速点
-- 					4:完美跳跃恢复满加速时间
-- @param colour 	当前撞击点颜色
-- @param duration 	当前加速持续时间
function Gas2Gac.SyncSaiMaAccelerateInfo(hitType, colour, duration, timeCount)
	LuaNewHorseRaceMgr:SyncSaiMaAccelerateInfo(hitType, colour, duration, timeCount)
end

function Gas2Gac.PlayerUseSaiMaJumpSkill()
	g_ScriptEvent:BroadcastInLua("PlayerUseSaiMaJumpSkill")
end

-- @param start 	完美区域起点百分比
-- @param size 		完美区域大小
-- @param duration 	总时长(s)
function Gas2Gac.PlayerEnterSaiMaJumpRegion(start, size, duration)
	g_ScriptEvent:BroadcastInLua("PlayerEnterSaiMaJumpRegion", start, size, duration)
end

-- @param engineId 引擎对象id
-- @param result EnumMPTYSJump
-- @param bSkill 是否在跳跃准备区内使用过技能
-- @param forwardSpeed 前向速度
function Gas2Gac.PlayerLeaveSaiMaJumpRegion(engineId, result, bSkill, forwardSpeed)
	LuaNewHorseRaceMgr:PlayerLeaveSaiMaJumpRegion(engineId, result, bSkill, forwardSpeed)
end

-- 代言人宗门
-- isSigned 是否报名
-- effectCnt 当前排行榜有几个符合条件
function Gas2Gac.SyncSpokesmanSectRankExtra(isSigned, effectCnt)
	g_ScriptEvent:BroadcastInLua("SyncSpokesmanSectRankExtra", isSigned, effectCnt)
end

-- 查询是否报名的返回 
function Gas2Gac.SyncSpokesmanSectSignStatus(isSigned)
	g_ScriptEvent:BroadcastInLua("SyncSpokesmanSectSignStatus", isSigned)
end

-- 请求报名/取消报名的返回
function Gas2Gac.ReplySignSpokesmanSect(isSigned)
	g_ScriptEvent:BroadcastInLua("SyncSpokesmanSectSignStatus", isSigned)
end

-- 临时改变外观
function Gas2Gac.UseTempTaBen(position, fashionId)
	if position == 1 then
		CClientMainPlayer.Inst:ChangeWeapon(fashionId)
	end
end

function Gas2Gac.CloseCaiDieWaitUI()
	CUIManager.CloseUI("MessageBox")
end

function Gas2Gac.CandyPlaySignUpResult(bSuccess)
	g_ScriptEvent:BroadcastInLua("CandyPlayCheckInMatchingResult", bSuccess)
end

function Gas2Gac.CandyPlayCancelSignUpResult(bSuccess)
	g_ScriptEvent:BroadcastInLua("CandyPlayCheckInMatchingResult", not bSuccess)
end

function Gas2Gac.CandyPlayCheckInMatchingResult(bInMatching)
	g_ScriptEvent:BroadcastInLua("CandyPlayCheckInMatchingResult", bInMatching)
end

-- @param stage 3(第一大轮前), 5(第二大轮前), 7(第三大轮前)
function Gas2Gac.CandyPlayStageFx(stage)
	LuaLiuYi2021TangGuoMgr:AddTangGuoWuFx()
	if stage == 3 then
		g_MessageMgr:ShowMessage("LiuYi_2021_TangGuo_Stage_Tip_1")
	elseif stage == 5 then
		g_MessageMgr:ShowMessage("LiuYi_2021_TangGuo_Stage_Tip_2")
	elseif stage == 7 then
		g_MessageMgr:ShowMessage("LiuYi_2021_TangGuo_Stage_Tip_3")
	end
end

-- @param countdown 倒计时时间
function Gas2Gac.CandyPlayOpenSelectWnd(countdown)
	LuaLiuYi2021TangGuoMgr:ShowTipAndChoose(countdown)
end

function Gas2Gac.CandyPlayerScoreChange(deltaScore)
	if CClientMainPlayer.Inst then
		local c = Color.red
		if deltaScore > 0 then
			c = Color.green
		end
		CClientMainPlayer.Inst.RO:ShowScoreTextWithSize(deltaScore, c, LiuYi2021_Setting.GetData().TangGuoScoreTextSize)
	end
end

-- @param playInfoU 玩法数据
-- {playerid1, score1, playername1, rank1, playerid2, ...}
function Gas2Gac.CandyPlaySyncPlayInfo(playInfoU)
	LuaLiuYi2021TangGuoMgr:SetSceneMode()
	local info = MsgPackImpl.unpack(playInfoU)
	g_ScriptEvent:BroadcastInLua("CandyPlaySyncPlayInfo", info)
end

-- @param babyType 	玩家选择的角色类型
-- @param rank 		排名
-- @param score 	得分
function Gas2Gac.CandyPlayResult(babyType, rank, score)
	LuaLiuYi2021TangGuoMgr:ShowResultWnd(babyType, rank, score)
end

function Gas2Gac.SyncMySectName(sectName)
	-- 客户端接入时可删除本行输出

	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.SectName = sectName
	end
end

function Gas2Gac.EnableSpecialBuilding(houseId, buildingType)
	--更新specialbuilding数据
	if not CClientHouseMgr.Inst then return end
	if not CClientMainPlayer.Inst then return end
	if houseId~=CClientMainPlayer.Inst.ItemProp.HouseId then return end
	
    local speicalBuilding = CClientHouseMgr.Inst.mFurnitureProp2.SpecialBuilding
	speicalBuilding:SetBit(buildingType,true)

	g_ScriptEvent:BroadcastInLua("EnableSpecialBuilding", houseId,buildingType)
end

-- 触发敌对宗门的引导
function Gas2Gac.SyncTriggerEnemySectGuide()

	if not COpenEntryMgr.Inst:GetGuideRecord(141) then
		COpenEntryMgr.Inst:SaveGuidePhase(141, 0)
		CGuideMgr.Inst:StartGuide(141, 0)
	end
end

-- 赛马迭代版本, 玩家进入倒计时阶段 -> 关闭选马界面
function Gas2Gac.OnSaiMaStartCountDown()
	LuaNewHorseRaceMgr:OnSaiMaStartCountDown()
end

-- 赛马迭代版本, 比赛开始特效
function Gas2Gac.MPTYSShowStartPlayFx()
	LuaNewHorseRaceMgr:MPTYSShowStartPlayFx()
end

function Gas2Gac.SyncPlayerHpInfoToWatcher(engineId, curHp, curHpFull, hpFull, ignoreId_U)
	local playerDict = MsgPackImpl.unpack(ignoreId_U)
	-- 自己是否在玩法内
	LuaFlagDuelMgr:TryTriggerFlagDuel(playerDict)

	CHeadInfoWnd.Instance:SetFlagDuelState(engineId, playerDict)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj and CHeadInfoWnd.Instance:NeedShowFlagDuelyHP(obj)>=0 then
		obj.Hp = curHp
		obj.HpFull = hpFull
		obj.CurrentHpFull = curHpFull
		EventManager.BroadcastInternalForLua(EnumEventType.HpUpdate, {engineId})
	end
end

-- @param bEnable true 为激活功能, false 为关闭功能
function Gas2Gac.EnableMainPlayerFootSteps(bEnable)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.FootStepSoundEnable = bEnable
	end
end

function Gas2Gac.SyncSaiMaSpeed(engineId, adjSpeed, mulSpeed)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if not obj then return end

    if obj == CClientMainPlayer.Inst then
        -- CClientMainPlayer.Inst.DelayExecuteQueue:AppendCommand(DelegateFactory.Action_uint(function (id)
			CClientMainPlayer.Inst.FightProp:SetParam(EPlayerFightProp.AdjSpeed, adjSpeed)
			CClientMainPlayer.Inst.FightProp:SetParam(EPlayerFightProp.MulSpeed, mulSpeed)

			obj.EngineObject:SetSpeed(obj.MaxPixelSpeed)

            EventManager.BroadcastInternalForLua(EnumEventType.MainplayerFightPropUpdate, {})
		-- end))
    else
        obj.EngineObject:SetSpeed(obj.MaxPixelSpeed)
    end
end

-- @param cameraHeightOffset
-- @param cameraZ
-- @param cameraY
function Gas2Gac.EnableFirstPersonPerspective(cameraHeightOffset, cameraZ, cameraY)
	if CameraFollow.Inst == nil or CClientMainPlayer.Inst == nil then
        return
    end

	--CameraFollow.m_MinRotationEnable = true
	CameraFollow.Inst.m_FPPEnable = true
	local vec3 = CameraFollow.Inst.targetRZY
    vec3.x = 180 + CClientMainPlayer.Inst.RO.Direction
    vec3.y = cameraZ
    vec3.z = cameraY
    CClientMainPlayer.Inst.RO.Visible = false
	CRenderScene.Inst.EnableChunkDynamicHide = false
    CameraFollow.Inst.MaxSpHeight = cameraHeightOffset
    CameraFollow.Inst.targetRZY = vec3
	CameraFollow.Inst.RZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function Gas2Gac.DisableFirstPersonPerspective()
	if CameraFollow.Inst == nil or CClientMainPlayer.Inst == nil then
        return
    end
	
	--CameraFollow.m_MinRotationEnable = false
	CameraFollow.Inst.m_FPPEnable = false
	local vec3 = CameraFollow.Inst.targetRZY
    vec3.x = 237.5
    vec3.y = 12
    vec3.z = 9.5
	
	CClientMainPlayer.Inst.RO.Visible = true
	CRenderScene.Inst.EnableChunkDynamicHide = true
    CameraFollow.Inst.MaxSpHeight = 0
    CameraFollow.Inst.targetRZY = vec3
	CameraFollow.Inst.RZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function Gas2Gac.EnableCameraDirectionBind(cameraZ, cameraY)
	if CameraFollow.Inst == nil or CClientMainPlayer.Inst == nil then
        return
    end
	CameraFollow.Inst.m_CameraBindEnable = true
	local vec3 = CameraFollow.Inst.targetRZY
	CClientMainPlayer.Inst.RO.Direction = vec3.x - 180
	vec3.y = cameraZ
	vec3.z = cameraY
	CameraFollow.Inst.targetRZY = vec3
    CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

function Gas2Gac.DisableCameraDirectionBind()
	if CameraFollow.Inst == nil then
        return
    end
	CameraFollow.Inst.m_CameraBindEnable = false
end

-- @param bInstant 是否瞬间切换
function Gas2Gac.SetCameraRYD(rotation, yAngle, distToPlayer, bInstant)
	if CameraFollow.Inst == nil or CClientMainPlayer.Inst == nil then
        return
    end

	if CameraFollow.Inst.CurMode == CameraMode.E3DPlus then
		CameraFollow.Inst.CurCameraParam.CurRotation = rotation

		yAngle = math.min(math.max(yAngle, CCameraParam.MIN_Y_ANGLE), CCameraParam.MAX_Y_ANGLE)
		CameraFollow.Inst.CurCameraParam.CurYAngle = yAngle

		if distToPlayer > 0 then
			distToPlayer = math.min(math.max(distToPlayer, CCameraParam.MIN_DIST_TO_PLAYER), CCameraParam.MAX_DIST_TO_PLAYER)
			CameraFollow.Inst.CurCameraParam.CurDistToPlayer = distToPlayer
		end

		NormalCamera.Inst:SetCurrentRZY(not bInstant)
	end
end

function Gas2Gac.UpdateHouseFishInfo(fishInfo_u)
	LuaSeaFishingMgr.OnUpdateHouseFishInfo(fishInfo_u)
end

function Gas2Gac.FishingHarvestSuccess(fishItemTempId, weight)
	LuaSeaFishingMgr.OnFishingHarvestSuccess(fishItemTempId, weight)
	g_ScriptEvent:BroadcastInLua("FishingHarvestSuccess",fishItemTempId, weight)
end

function Gas2Gac.UpdateFishingFightState(tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
	--tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time
	g_ScriptEvent:BroadcastInLua("UpdateFishingFightState",tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
end

function Gas2Gac.HouseFishingFightFailed()
	LuaSeaFishingMgr.OnHouseFishingFightFailed()
	g_ScriptEvent:BroadcastInLua("HouseFishingFightFailed")
	if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
		CSkillButtonBoardWnd.Show(true)
	end
end

function Gas2Gac.BeginFishingFight(tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
	LuaSeaFishingMgr.OnBeginFishingFight()
	g_ScriptEvent:BroadcastInLua("BeginFishingFight", tagPos, tagSpeed, greenPos, greenSpeed, greenLen, percent, time)
end

function Gas2Gac.FishingHarvestBegin(fishItemTempId)
	LuaSeaFishingMgr.OnFishingHarvestBegin(fishItemTempId)
end

function Gas2Gac.UpdateFishFood(fishFoodItemTempId)
	LuaSeaFishingMgr.UpdateFishFood(fishFoodItemTempId)
end

function Gas2Gac.SetCurrentFishingWaitDuration(startTime, waitDuration)
	LuaSeaFishingMgr.m_CurGameplayId = CScene.MainScene.GamePlayDesignId
	if LuaSeaFishingMgr.m_CurGameplayId == HaiDiaoFuBen_Settings.GetData().SpecialGameplayId then
		CSkillButtonBoardWnd.Hide(true)
		CUIManager.ShowUI(CUIResources.HouseFishingWnd)
	end
	LuaSeaFishingMgr.OnSetCurrentFishingWaitDuration(startTime, waitDuration)
end

function Gas2Gac.SyncShuiGuoPaiDuiScore(score)
	LuaFruitPartyMgr:SyncShuiGuoPaiDuiScore(score)
end

function Gas2Gac.SyncShuiGuoPaiDuiLianJi(lianJiCount)
	g_ScriptEvent:BroadcastInLua("SyncShuiGuoPaiDuiLianJi", lianJiCount)
end

-- hasGotReward_U  rewardInfo_U  格式均为 {[itemId] = rewardType,}
function Gas2Gac.ShowShuiGuoPaiDuiResultWnd(maxLianJi, score, rewardInfo_U, hasGotReward_U)
	local rewardInfo = MsgPackImpl.unpack(rewardInfo_U)
	local hasGotReward = MsgPackImpl.unpack(hasGotReward_U)
	local rewards = {}
	local gotRewards = {}
	if rewardInfo then
		for i = 0, rewardInfo.Count - 1 do
			rewards[i + 1] = rewardInfo[i]
		end
	end
	if hasGotReward then
		for i = 0, hasGotReward.Count - 1 do
			gotRewards[i + 1] = hasGotReward[i]
		end
	end

	LuaFruitPartyMgr:ShowShuiGuoPaiDuiResultWnd(maxLianJi, score, rewards, gotRewards)
end

function Gas2Gac.SetFishPoleSuccess(playerId, fishPoleType)
	LuaSeaFishingMgr.OnSetFishPoleSuccess(fishPoleType)
end
function Gas2Gac.ShowLingLiRain()
	LuaZongMenMgr:ShowLingLiRain()
end

function Gas2Gac.ShutdownLingLiRain()
	LuaZongMenMgr:ShutdownLingLiRain()
end

function Gas2Gac.SyncSectStdRulesChangeDone(sectId, joinStdUd, rulesUd)
end

function Gas2Gac.SyncSectShowLianhuaData(Ud)
	-- UD 解开后是 engineId 的 list
	local list = MsgPackImpl.unpack(Ud)
	LuaZongMenMgr:SyncXuanMeiStatus(list)
end

-- Gac2Gs.QuerySectVoteNpcVoteId(engineId)
function Gas2Gac.SyncSectVoteNpcVoteId(engineId, voteId)
	CWebBrowserMgr.Inst:OpenUrl(SafeStringFormat3("https://qnm.163.com/2021/mrt/mh/#/rank-detail?stageType=fusai&id=%d", voteId))
end

function Gas2Gac.UpdateHousePoolGridNum(houseId, poolgridnum)
	local setting = House_Setting.GetData()
	CClientHouseMgr.Inst.mCurFurnitureProp2.PoolGridNum = poolgridnum
	CUIManager.CloseUI(CLuaUIResources.BuyPoolComfirmWnd)
	g_ScriptEvent:BroadcastInLua("UpdateHousePoolGridNum",houseId, poolgridnum+setting.BasePoolGridNum)
	g_MessageMgr:ShowMessage("Update_PoolNum_To_Number",poolgridnum+setting.BasePoolGridNum)
end

function Gas2Gac.SwitchFishingPannel(bIsOpen)
	LuaSeaFishingMgr.OnSwitchFishingPannel(bIsOpen)
end

function Gas2Gac.MarkFishAwarded(fishIdx)
	g_ScriptEvent:BroadcastInLua("MarkFishAwarded",fishIdx)
end

function Gas2Gac.UpdateFishBasket(basketUD)
	LuaSeaFishingMgr.OnUpdateFishBasket(basketUD)
end

--function Gas2Gac.UpdateFishBasket(pos, fishItemTempId, count)
--	print("UpdateFishBasket",pos, fishItemTempId, count)
--	LuaSeaFishingMgr.OnUpdateFishBasket(pos, fishItemTempId, count)
--end
--
function Gas2Gac.PlayerFirstEnterFishingHouse()
end

function Gas2Gac.UpdateZhengzhaiFishInfo(fishId, fishUD)
	if not  CClientMainPlayer.Inst then return end
    local playerId = CClientMainPlayer.Inst.Id
	local fishInfoType = CClientHouseMgr.Inst.mCurFurnitureProp2.FishInfo
    local dic = fishInfoType.ZhengzhaiFish
	if CommonDefs.DictContains_LuaCall(dic, playerId) then
		local zhengzhaiFishInfoType = CommonDefs.DictGetValue_LuaCall(dic, playerId)
        local zhenzhaiFishMap = zhengzhaiFishInfoType.AllFishes
		local zhenzhaiyu = CZhengzhaiFishType()
		
		if zhenzhaiyu:LoadFromString(fishUD) then
			zhenzhaiFishMap[fishId] = zhenzhaiyu
		end
	end
	
	g_ScriptEvent:BroadcastInLua("UpdateZhengzhaiFishInfo",fishId, fishUD)
end

function Gas2Gac.ImproveZhengzhaiFishSuccess(fishId)
	g_ScriptEvent:BroadcastInLua("ImproveZhengzhaiFishResult",fishId,true)
end

function Gas2Gac.ImproveZhengzhaiFishFailed(fishId)
	g_ScriptEvent:BroadcastInLua("ImproveZhengzhaiFishResult",fishId,false)
end

function Gas2Gac.UpdateLingShouMaxLevel(lingshouId, maxLv,maxXiuwei)
	g_ScriptEvent:BroadcastInLua("UpdateLingShouMaxLevel",lingshouId, maxLv,maxXiuwei)
end

-- teamInfo_U {playerId, class, grade, xianfanStatus, equipScore}
-- equipScore 0 表示玩家不在附近
function Gas2Gac.SendJueDouInviteToPlayer(msg, challengerId, bTeam, teamInfo_U)

end

function Gas2Gac.SendQieCuoInviteToPlayer(msg, challengerId, bTeam, teamInfo_U)
	LuaFlagDuelMgr:SendQieCuoInviteToPlayer(msg, challengerId, bTeam, teamInfo_U)
end

function Gas2Gac.SetBgMusicVolume(value, restore)
	SoundManager.Inst:ChangeBgMusicVolume("Flowchart", value, restore)
end

function Gas2Gac.RequestEditPoolSuccess()
	LuaPoolMgr.OpenPoolEditer()
end

-- 开始宗派任务寻路
function Gas2Gac.TrackToDoSectSchoolTask(taskId)
	if not CClientMainPlayer.Inst then return end
    local taskProp = CClientMainPlayer.Inst.TaskProp
    local task = CommonDefs.DictGetValue(taskProp.CurrentTasks, typeof(UInt32), taskId)
    local taskDesignData = task.TaskTemplate
    local func = CLuaTaskListItem.m_ProcessTaskCallbackMap[taskDesignData.GamePlay]
    if func then
        return func(task)
    end
end

-- 进入场景之后开始同步，断线重连也会同步
-- stage: 对应梦境的主题,目前取值范围是 1,2
-- components_U: 场景中剩余的组件,unpack之后是一个table
-- {
--	id1 = {类型, x, y, z}, 
--	id2 = {类型, x, y, z}, 
-- }
-- 	类型目前取值1,2，分别代表星星和云彩
function Gas2Gac.SyncZhaiXingComponent(stage, components_U)
	--print("SyncZhaiXingComponent", stage)
	local dict = MsgPackImpl.unpack(components_U)
	local components = {}
	if dict then
		CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key,val)
			--print(key, val[0], val[1], val[2], val[3])
			table.insert(components, {key = key, type = val[0], x = val[1], y = val[2], z = val[3]})
		end))
	end
	LuaZhaiXingMgr:SyncZhaiXingComponent(stage, components)
end

-- 开启了哪几个主题
-- Gac2Gas.QueryZhaiXingOpenStatus 的返回,开启状态
-- 解开后是个list, 对应梦境的主题
function Gas2Gac.ReplyZhaiXingOpenStatus(openStatus)
	local list = MsgPackImpl.unpack(openStatus)
	local status = {}

	for i = 0, list.Count - 1 do
		--print(list[i])
		table.insert(status, list[i])
	end

	LuaZhaiXingMgr:ReplyZhaiXing(status)
end

-- Gac2Gas.RequestZhaiXingSuccess 的返回。请求摘星成功,碰到障碍也要发，不然断线进入场景之后这个障碍还在
function Gas2Gac.ReplyZhaiXingSuccess(index)
	--print("ReplyZhaiXingSuccess", index)
	LuaZhaiXingMgr:ReplyZhaiXingSuccess(index)
end

-- Gac2Gas.ReplyZhaiXingPassComponent 的返回。请求摘星经过障碍
function Gas2Gac.ReplyZhaiXingPassComponent(index)
	--print("ReplyZhaiXingPassComponent", index)
	LuaZhaiXingMgr:ReplyZhaiXingPassComponent(index)
end

-- 摘星加速
function Gas2Gac.SyncZhaiXingSpeedUp(index)
	--print("SyncZhaiXingSpeedUp", index)
	LuaZhaiXingMgr:SyncZhaiXingSpeedUp(index)
end

-- 摘星同步分数(断线重连)
function Gas2Gac.SyncZhaiXingScore(score, fightEndTime)
	--print("SyncZhaiXingScore", score, fightEndTime)
	LuaZhaiXingMgr:SyncZhaiXingScore(score, fightEndTime)
end

-- 摘星结算
function Gas2Gac.SyncZhaiXingSummary(score, timeUse, historyBest)
	--print("SyncZhaiXingSummary", score, timeUse, historyBest)
	LuaZhaiXingMgr:SyncZhaiXingSummary(score, timeUse, historyBest)
end

-- 同步罗刹海市
function Gas2Gac.SyncLuoChaPlayHp(playerId, playHp, playHpFull)
	-- 主角与目标血量同步
	-- 1代表连续血量
	LuaGamePlayHpMgr:AddHpInfo(playerId, playHp, playHpFull, false, 1, true)
end

-- 打开报名窗口
function Gas2Gac.OpenLuoChaHaiShiSignUpWnd(bSignUp, failedRewardTimes, playerSuccessRewardTimes, luoChaSuccessRewardTimes)
	LuaLuoCha2021Mgr:ShowSignUpWnd(bSignUp, failedRewardTimes, playerSuccessRewardTimes, luoChaSuccessRewardTimes)
end

-- 同步报名结果
function Gas2Gac.SyncLuoChaHaiShiSignUpResult(bSignUp)
	g_ScriptEvent:BroadcastInLua("SyncLuoChaHaiShiSignUpResult", bSignUp)
end

-- 同步罗刹玩家等级信息
function Gas2Gac.SyncLuoChaAttackerLevelInfo(level, power, upgradeNeedPower, learnedSkillCount, skillCount)
	LuaLuoCha2021Mgr.m_LuoChaPower = power
	LuaLuoCha2021Mgr.m_Level = level
	LuaLuoCha2021Mgr.m_LuoChaUpgradeNeedPower = upgradeNeedPower
	LuaLuoCha2021Mgr.m_LearnSkillCount = skillCount - learnedSkillCount
	g_ScriptEvent:BroadcastInLua("SyncLuoChaAttackerLevelInfo", level, power, upgradeNeedPower, learnedSkillCount, skillCount)
end

-- 更新炮台物资
function Gas2Gac.UpdateLuoChaFortPickCount(fortPickCount)
	LuaLuoCha2021Mgr:CheckFortFx(fortPickCount)
	LuaLuoCha2021Mgr.m_SumPickCount = fortPickCount
	g_ScriptEvent:BroadcastInLua("UpdateLuoChaFortPickCount", fortPickCount)
end

-- 同步存活人类玩家数
function Gas2Gac.SyncLuoChaPlayerAliveCount(aliveCount)
	LuaLuoCha2021Mgr.m_RenLeiAliveCount = aliveCount
	g_ScriptEvent:BroadcastInLua("SyncLuoChaPlayerAliveCount", aliveCount)
end

-- 同步人类玩家物资信息
function Gas2Gac.SyncLuoChaDefenderPickInfo(data)
	if data ~= nil then
		local pickInfo = MsgPackImpl.unpack(data)
		for i=0, pickInfo.Count -1 do
			local info = pickInfo[i]
			local id = info[0]
			local count = info[1]
			-- 同步到队伍信息
			if CGamePlayMgr.Inst then
				CommonDefs.DictSet(CGamePlayMgr.Inst.GamePlayItems, typeof(UInt64), id, typeof(float), count)
			end
			EventManager.BroadcastInternalForLua(EnumEventType.OnGameplayItemUpdate,{id, 51102271, count})
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == id then
				LuaLuoCha2021Mgr.m_RenLeiCurrentPickCount = count
				g_ScriptEvent:BroadcastInLua("SyncLuoChaDefenderPickInfo", count)
			end
		end
	end
end

-- 显示罗刹玩家结果
function Gas2Gac.ShowLuoChaAttackerResult(bWin, time, killCount, totalCount, skillInfo, playerName)
	LuaLuoCha2021Mgr:ShowResultWnd(true, bWin, time, killCount, MsgPackImpl.unpack(skillInfo), totalCount, playerName)
end

-- 显示人类玩家结果
function Gas2Gac.ShowLuoChaDefenderResult(bWin, time, fortPickCount, playerInfo)
	LuaLuoCha2021Mgr:ShowResultWnd(false, bWin, time, fortPickCount, MsgPackImpl.unpack(playerInfo))
end

-- 同步人类玩家等级信息
function Gas2Gac.SyncLuoChaDefenderLevelInfo(level, learnedSkillCount, skillCount, maxPickCount, upgradeNeedPick)
	LuaLuoCha2021Mgr.m_Level = level
	LuaLuoCha2021Mgr.m_RenLeiMaxPickCount = maxPickCount
	LuaLuoCha2021Mgr.m_RenLeiUpgradeNeedPickCount = upgradeNeedPick
	LuaLuoCha2021Mgr.m_LearnSkillCount = skillCount - learnedSkillCount
	g_ScriptEvent:BroadcastInLua("SyncLuoChaDefenderLevelInfo", level, learnedSkillCount, skillCount, maxPickCount, upgradeNeedPick)
end

local CFurnitureProp2=import "L10.Game.CFurnitureProp2"
function Gas2Gac.SyncYangyangHousePool(furnitureProp2ud)
	local furniture2 = CFurnitureProp2()
    furniture2:LoadFromString(furnitureProp2ud)

    CClientFurnitureMgr.Inst.m_Pool = furniture2.Pool
    CClientFurnitureMgr.Inst.m_WarmPool = furniture2.WarmPool
end

-- 同步罗刹可学习技能信息
function Gas2Gac.SyncLuoChaCanLearnSkillInfo(skillInfo)
	if skillInfo ~= nil then
		local info = MsgPackImpl.unpack(skillInfo)
		LuaLuoCha2021Mgr:CacheLearnSkillInfo(info)
		g_ScriptEvent:BroadcastInLua("SyncLuoChaCanLearnSkillInfo", info)
	end
end

-- 躲猫猫当前场景是否名园
function Gas2Gac.SetDuoMaoMaoMingYuanInfo(bMingYuan)
	CClientFurnitureMgr.Inst.m_IsMingYuan = bMingYuan
end

function Gas2Gac.RequestQueryTopFisher_Result(topfisher_ud)
	LuaSeaFishingMgr.OnRequestQueryTopFisher_Result(topfisher_ud)
end

function Gas2Gac.ShowCastingWnd(srcId, castingTime)
	if CClientMainPlayer.Inst ~= nil then
		CClientMainPlayer.Inst:OnShowActionStateCasting(srcId, castingTime)
	end
end

function Gas2Gac.AddNpcFriend(npcTemplateId, sn)
	--print("AddNpcFriend",npcTemplateId, sn)
	local MainPlayer = CClientMainPlayer.Inst
	if MainPlayer then
        MainPlayer.RelationshipProp.NpcFriendSet:SetBit(sn,true)
    end
	LuaNPCChatMgr:OnAddFriend(npcTemplateId)
end

function Gas2Gac.DelNpcFriend(npcTemplateId, sn)
	--print("DelNpcFriend",npcTemplateId, sn)
	local MainPlayer = CClientMainPlayer.Inst
	if MainPlayer then
        MainPlayer.RelationshipProp.NpcFriendSet:SetBit(sn,false)
    end
end

-- function Gas2Gac.SetNpcChatEventAccepted(eventId,sn)
-- 	--print("SetNpcChatEventAccepted", eventId, sn)
-- 	local MainPlayer = CClientMainPlayer.Inst
--     if MainPlayer then
--         MainPlayer.RelationshipProp.AcceptNpcChatEventSet:SetBit(sn,true)
--     end
-- 	LuaNPCChatMgr:OnSetNpcChatEventAccepted(eventId)
-- end
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
function Gas2Gac.SetNpcChatEventFinished(npcTemplateId, eventId, sn)
	--print("SetNpcChatEventFinished",npcTemplateId ,eventId, sn)
	local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        MainPlayer.RelationshipProp.FinishNpcChatEventSet:SetBit(sn,true)
    end
	LuaNPCChatMgr:SetEventTime(eventId,CServerTimeMgr.Inst.timeStamp)
	g_ScriptEvent:BroadcastInLua("SetNpcChatEventFinished",npcTemplateId, eventId)
end

-- function Gas2Gac.SendNpcChatGroupId(npcTemplateId, npcChatGroupId)
-- 	--print("SendNpcChatGroupId",npcTemplateId,npcChatGroupId)
-- 	LuaNPCChatMgr:OnAcceptNPCChatGroup(npcChatGroupId, npcTemplateId)
-- end

function Gas2Gac.SendNpcChatMsgId(npcTemplateId, npcChatId)
	LuaNPCChatMgr:ProcessChatMsg(npcTemplateId, npcChatId)
end

function Gas2Gac.SetNpcChatGroupFinished(npcTemplateId, groupId, sn)
	--print("SetNpcChatGroupFinished",npcTemplateId, groupId, sn)
	local MainPlayer = CClientMainPlayer.Inst
	if MainPlayer then
        MainPlayer.RelationshipProp.FinishNpcChatGroupSet:SetBit(sn,true)
	end
end

function Gas2Gac.SyncHouseGrassInfo(houseId, grassInfoUD)
	LuaHouseTerrainMgr.SyncHouseGrassInfo(houseId, grassInfoUD)
end

function Gas2Gac.SyncSendWeddingInvitationConfirm(price, invitationType)
	local moneyType = EnumMoneyType.YinLiang
	local tipMessage
	if invitationType == 4 then
		tipMessage = g_MessageMgr:FormatMessage("QINGJIAN_WORLD_NEED_LINGYU")
		moneyType = EnumMoneyType.LingYu
	elseif invitationType == 3 then
		tipMessage = g_MessageMgr:FormatMessage("QINGJIAN_NEED_YINLIANG")
	elseif invitationType == 1 then
		tipMessage = g_MessageMgr:FormatMessage("QINGJIAN_DUILD_NEED_YINLIANG")
	elseif invitationType == 2 then
		tipMessage = g_MessageMgr:FormatMessage("QINGJIAN_SECT_NEED_YINLIANG")
	end
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(moneyType, tipMessage, price, DelegateFactory.Action(function ()
		Gac2Gas.ConfirmSendWeddingInvitation(invitationType)
	end), nil, false, true, EnumPlayScoreKey.NONE, false)

	-- QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.Score, msg, cost, okAction, nil, false, true, EnumPlayScoreKey.YuMa, false)
end

-- 查询本服参加跨服帮会信息
function Gas2Gac.QueryGLCGuildPosInfoResult(ownGuilId, exchangeRequest_U, guildInfo_U)
	LuaGuildLeagueCrossMgr:QueryGLCGuildPosInfoResult(ownGuilId, exchangeRequest_U, guildInfo_U)
end

-- 交换位置请求 成功发送
function Gas2Gac.RequestGLCExchangeGuildPosSuccess(destGuildId)
	LuaGuildLeagueCrossMgr:RequestGLCExchangeGuildPosSuccess(destGuildId)
end

-- 更新收到的交换位置请求
function Gas2Gac.UpdateGLCExchangePosInfo(exchangeRequest_U)

end

-- 打开活动界面所需信息
function Gas2Gac.QueryForOpenGLCWndResult(info_U, bTrainOpen, betInfo_U)
	LuaGuildLeagueCrossMgr:QueryForOpenGLCWndResult(info_U, bTrainOpen, betInfo_U)
end

-- 查询本服参加跨服联赛帮会的信息
function Gas2Gac.QueryGLCJoinCrossGuildInfoResult(guildInfo_U)
	LuaGuildLeagueCrossMgr:QueryGLCJoinCrossGuildInfoResult(guildInfo_U)
end

-- 查询小组赛积分信息
function Gas2Gac.QueryGLCGroupScoreInfoResult(scoreInfo_U)
	LuaGuildLeagueCrossMgr:QueryGLCGroupScoreInfoResult(scoreInfo_U)
end

-- 小组赛对阵信息
function Gas2Gac.QueryGLCGroupMatchInfoResult(belongGroup, info_U)
	LuaGuildLeagueCrossMgr:QueryGLCGroupMatchInfoResult(belongGroup, info_U)
end

-- 排位赛对阵信息
function Gas2Gac.QueryGLCRankMatchInfoResult(info_U)
	LuaGuildLeagueCrossMgr:QueryGLCRankMatchInfoResult(info_U)
end

-- 查询指定服务器帮会信息 界面上的点击操作的返回
function Gas2Gac.QueryGLCServerGuildInfoResult(info_U, context)
	LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfoResult(info_U, context)
end

-- 查询服务器之间比赛信息
function Gas2Gac.QueryGLCServerMatchInfoResult(info_U, buffInfo_U)
	LuaGuildLeagueCrossMgr:QueryGLCServerMatchInfoResult(info_U, buffInfo_U)
end

-- 场景是否可以选择(新郎是否有足够的钱) status 为1表示可以选
function Gas2Gac.SyncQueryChoseNewWeddingSceneResult(bReview, teamLeaderId, discount, sceneInfoUd)
	LuaWeddingIterationMgr:SyncChooseSceneResult(bReview, teamLeaderId, sceneInfoUd)
end

function Gas2Gac.SyncNewWeddingSceneInfo(guestCount, leftTime, groomId, groomName, brideId, brideName)
	LuaWeddingIterationMgr:SyncSceneInfo(guestCount, leftTime, groomId, groomName, brideId, brideName)
end

-- stage 当前处于玩法哪个阶段，对应服务端枚举值 EnumNewWeddingPlayStage,因为玩法开发中，这个枚举值还在增加中
-- extraInfoUd 不同阶段的其他上下文信息
function Gas2Gac.SyncNewWeddingStageInfo(stage, extraInfoUd)
	LuaWeddingIterationMgr:SyncStageInfo(stage, extraInfoUd)
end

function Gas2Gac.SyncNewWeddingPartner(playerId, name, gender, cls)
	LuaWeddingIterationMgr:SyncPartner(playerId, name, gender, cls)
end

function Gas2Gac.SyncChoseNewWeddingScene(leaderId, playerId1, choice1, bSameWithLast1, playerId2, choice2, bSameWithLast2)
	LuaWeddingIterationMgr:SyncChooseScene(leaderId, playerId1, choice1, bSameWithLast1, playerId2, choice2, bSameWithLast2)
end

function Gas2Gac.SyncHouseGrassGridNum(houseId, grassType, count)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.HouseId == houseId then
		if not CClientHouseMgr.Inst.mFurnitureProp3 then return end
		local maxNum = CClientHouseMgr.Inst.mFurnitureProp3.GrassContainer.MaxGridNum
		maxNum[grassType] = count
		g_ScriptEvent:BroadcastInLua("SyncHouseGrassGridNum",grassType, count)
	end
end

function Gas2Gac.SyncHouseGroundInfo(houseId, groundChgUD)
	LuaHouseTerrainMgr.SyncHouseGroundInfo(houseId, groundChgUD)
end

function Gas2Gac.OpenNpcChatWndSuccess(npcTemplateId)
	if LuaNPCChatMgr.TaskID then
		local empty = CreateFromClass(MakeGenericClass(List, Object))
		empty = MsgPackImpl.pack(empty)
		Gac2Gas.FinishClientTaskEventWithConditionId(LuaNPCChatMgr.TaskID , "OpenNpcChatWnd", empty, npcTemplateId)
	end
end

-- 将婚前游戏中答题者当前选项同步给假新娘
function Gas2Gac.SyncNewWeddingCurrentChoice(senderId, choiceIdx)
	LuaWeddingIterationMgr:SyncCurrentChoice(senderId, choiceIdx)
end

-- 被提问的人反馈当前选项是否正确给提问者-信息同步给提问者，因为回答问题的可能是个NPC，所以这里用engineId
function Gas2Gac.SyncConfirmNewWeddingChoice(senderEngineId, choiceIdx, boolean_Right)
	LuaWeddingIterationMgr:SyncConfirmChoice(senderEngineId, choiceIdx, boolean_Right)
end

-- 被提问当前选项是否正确
function Gas2Gac.SyncNewWeddingChoiceConfirm(senderId, choiceIdx)
	LuaWeddingIterationMgr:SyncChoiceConfirm(senderId, choiceIdx)
end

-- 同步提问者关闭答题界面
function Gas2Gac.SyncNewWeddingCloseConversationWnd(senderId)
	LuaWeddingIterationMgr:SyncCloseConversationWnd(senderId)
end

-- 倒计时结束时间
function Gas2Gac.SyncChoseNewWeddingSceneCountDown(endTimestamp)
	LuaWeddingIterationMgr:SyncChooseSceneCountDown(endTimestamp)
end


-- 摘星玩法开始
function Gas2Gac.SyncZhaiXingPlayStart(score)
	LuaZhaiXingMgr:SyncZhaiXingPlayStart(score)
end

function Gas2Gac.SubmitItemToGuildLeagueTrainSuccess(ret_u)
	LuaGuildLeagueCrossMgr:SubmitItemToGuildLeagueTrainSuccess(ret_u)
end

function Gas2Gac.QueryGuildLeagueTrainRankResult(rankInfo_U, guildName, totalScore)
	LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainRankResult(rankInfo_U, guildName, totalScore)
end
function Gas2Gac.EnableHouseGroundBrushType(houseId,brushType)
	if CClientHouseMgr.Inst.mCurFurnitureProp3 then
		CClientHouseMgr.Inst.mCurFurnitureProp3.GroundContainer.EnabledBrush:SetBit(brushType,true)
	end

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.HouseId == houseId then
		if CClientHouseMgr.Inst.mFurnitureProp3 then
			CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.EnabledBrush:SetBit(brushType,true)
		end
	end
	g_ScriptEvent:BroadcastInLua("EnableHouseGroundBrushType",brushType)
end

function Gas2Gac.BindHouseGroundBrushType(houseId,brushPos, brushType)
	if CClientHouseMgr.Inst.mCurFurnitureProp3 then
		CClientHouseMgr.Inst.mCurFurnitureProp3.GroundContainer.BindedBrush[brushPos] = brushType
	end
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.HouseId == houseId then
		if CClientHouseMgr.Inst.mFurnitureProp3 then
			CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.BindedBrush[brushPos] = brushType
		end
	end
	CRenderScene.Inst.TerrainFast:SetRuntimeTexture(brushPos-1,brushType)
	if brushType==0 then
		CClientHouseMgr.Inst:ClearGroundChannelData(brushPos-1)
		LuaHouseTerrainMgr:SaveGround()
	end
	--设置完之后要刷新地表，blenddata会根据贴图来设置
	CClientHouseMgr.Inst:RefreshAllGround()
	g_ScriptEvent:BroadcastInLua("BindHouseGroundBrushType",brushPos, brushType)
end

function Gas2Gac.CheckEnterWeddingByNewInvitationResult(sceneId, invitationType, bCanEnter, bNeedTicket)
	LuaWeddingIterationMgr:CheckEnterWeddingByNewInvitationResult(sceneId, invitationType, bCanEnter)
end

-- 婚礼副本中有人点了跳过婚前仪式
function Gas2Gac.SyncPartnerSkipNewWeddingPreGame(senderId)
	LuaWeddingIterationMgr:SyncPartnerSkipPreGame(senderId)
end



--同步元旦福蛋报名状态
function Gas2Gac.SyncPlayerYuanDan2022FuDanSignUpResult(isMatching)
	LuaYuanDan2022Mgr:SyncPlayerYuanDan2022FuDanSignUpResult(isMatching)
end

-- 查询某个玩家元旦福蛋的报名状态返回
function Gas2Gac.QueryPlayerYuanDan2022FuDanSignUpInfoResult(playerId, isInMatching)
	LuaYuanDan2022Mgr:QueryPlayerYuanDan2022FuDanSignUpInfoResult(playerId, isInMatching)
end

-- 领取心愿奖励成功
function Gas2Gac.RequestGetYuanDanXinYuanRewardsSuccess(index)
	g_ScriptEvent:BroadcastInLua("OnGetYuanDanXinYuanRewardsSuccess",index)
end

-- 福蛋玩法结算数据
-- 数据形如下面的list，一个玩家的数据有10个元素
-- { playerid, name, class, winType, eggs, interrupt, force, gender, picker, fighter}
-- winType 是比赛结果，枚举值 EnumPlayResult
function Gas2Gac.SendYuanDan2022FuDanPlayResult(i, playResultUD)
	LuaYuanDan2022Mgr:SendYuanDan2022FuDanPlayResult(i, playResultUD)
end

-- 玩家开始进入孵蛋状态，播进度条
function Gas2Gac.YuanDan2022FuDanStartFuDan(duraion)
	CCommonProgressInfoMgr.ShowCommonProgressWnd(LocalString.GetString("孵蛋中"), duraion, CCommonProgressInfoMgr.ProgressType.CommonCasting)
end

-- 玩家停止孵蛋，关闭进度条
function Gas2Gac.YuanDan2022FuDanStopFuDan()
	CCommonProgressInfoMgr.CloseCommonProgressWndByType(CCommonProgressInfoMgr.ProgressType.CommonCasting)
end

function Gas2Gac.XingYuPlayerEnterStarRange(idx)
	LuaChristmas2021Mgr.Wait2SecondsOpenGuanXingWnd(idx)
end


function Gas2Gac.XingYuPlayerLeaveStarRange(idx)
	LuaChristmas2021Mgr.XingYuPlayerLeaveStarRange(idx)
end

-- 同步福蛋玩法的战况，数据形如下面的list，一个玩家的数据有6个元素
-- { playerid, name, class, eggs, interrupt, force,  playerid, name, class, eggs, interrupt, force }
-- name 可能是空字符串 ,classs 可能是0，此时说明玩家可能尚未进入副本
-- stage 第几阶段，第2阶段是赛点
-- 每秒同步一次，可以监听这个rpc来显示任务栏的文本
function Gas2Gac.SyncYuanDan2022PlayInfo(stage, playInfoUD)
	--print("SyncYuanDan2022PlayInfo",stage)
	LuaYuanDan2022Mgr:SyncYuanDan2022PlayInfo(stage, playInfoUD)
end

function Gas2Gac.QueryGuildChristmas2021Card_Result(allcard_ud)
	print("QueryGuildChristmas2021Card_Result")
    -- LuaChristmas2021Mgr.SyncGuildChristmas2021CardResult(allcard_ud)
	LuaChristmas2023Mgr:SyncGuildChristmas2021CardResult(allcard_ud) -- 2023圣诞节雪花絮语复用2021
end

function Gas2Gac.InitGuildLeagueWatchHeadWndInfo(matchIndex, info_U)
	LuaGuildLeagueCrossMgr:InitGuildLeagueWatchHeadWndInfo(matchIndex, info_U)
end

function Gas2Gac.EndEditYardGroundSuccess()
end

function Gas2Gac.SyncMonsterAlwaysShowNameBoard(engineId, isShow) -- 1 or 0
	LuaClientObject:SyncMonsterAlwaysShowNameBoard(engineId, isShow)
end

function Gas2Gac.SyncGuildLeagueTowerCountToWatcher(defendTowerNum, attackTowerNum)
	LuaGuildLeagueCrossMgr:SyncGuildLeagueTowerCountToWatcher(defendTowerNum, attackTowerNum)
end

function Gas2Gac.SyncGuildLeagueBossHpInfoToWatcher(info_U)
	LuaGuildLeagueCrossMgr:SyncGuildLeagueBossHpInfoToWatcher(info_U)
end

local EnumTempPlayDataKey = import "L10.Game.EnumTempPlayDataKey"
function Gas2Gac.QuerySchoolRebuildingProgress_Result(progressUD, progressPerDayUD)
	local progressList = MsgPackImpl.unpack(progressUD)
	local progressPerDayList = {0,0,0,0,0}
	if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumToInt(EnumTempPlayDataKey.eSchoolRebuildingPerDay)) then
		local data = CClientMainPlayer.Inst.PlayProp.TempPlayData[EnumToInt(EnumTempPlayDataKey.eSchoolRebuildingPerDay)]
		if data.ExpiredTime > CServerTimeMgr.Inst.timeStamp then
			local list = MsgPackImpl.unpack(data.Data.Data)
			for i = 1, 5 do
				if list.Count >= i then
					progressPerDayList[i] = list[i - 1]
				end
			end
		end
	end
	local list1,list2 = {0,0,0,0,0},{}
	for i = 1,5 do
		if progressList and i <= progressList.Count then
			list1[i] = progressList[i - 1]
		end
		list2[i]  = progressPerDayList[i]
	end
	g_ScriptEvent:BroadcastInLua("OnQuerySchoolRebuildingProgressResult",list1,list2)
end

function Gas2Gac.XingYuRankResult(idx, duration)
	LuaChristmas2021Mgr.OpenSDXYSucceedWnd(idx,duration)
end

function Gas2Gac.QueryXingYuRank_Result(playerId_ud, name_ud, duration_ud)
	if not CClientMainPlayer.Inst then return end

	local unpack = MsgPackImpl.unpack
	local idList, nameList, durationList = unpack(playerId_ud), unpack(name_ud), unpack(duration_ud)
	LuaChristmas2021Mgr.PlayerRankTable = {}
	LuaChristmas2021Mgr.MyXingYuRankInfo = nil
	
	local myId = CClientMainPlayer.Inst.Id 
	for i=0,idList.Count-1,1 do
		local info = {}
		info.rank = i+1
		info.playerId = idList[i]
		info.playerName = nameList[i]
		info.duration = durationList[i]
		if info.playerId == myId then
			LuaChristmas2021Mgr.MyXingYuRankInfo = info
		else
			table.insert(LuaChristmas2021Mgr.PlayerRankTable,info)
		end
	end

	LuaChristmas2021Mgr.ISXingYunMatching = false
	if CUIManager.IsLoaded(CLuaUIResources.SDXYSignAndRankWnd) then
		g_ScriptEvent:BroadcastInLua("SDXYQueryRankResult")
	else
		LuaChristmas2021Mgr.IsNeedOpenSDXYRankTab = true
		CUIManager.ShowUI(CLuaUIResources.SDXYSignAndRankWnd)
	end
end

function Gas2Gac.QueryPlayerIsMatchingXingyu2021_Result(isMatching)
	--g_ScriptEvent:BroadcastInLua("RefreshSDXYMatchSate",isMatching)
	LuaChristmas2021Mgr.ISXingYunMatching = isMatching
	CUIManager.ShowUI(CLuaUIResources.SDXYSignAndRankWnd)
end

-- 性别,对应男方还是女方宾客
function Gas2Gac.ShowNewWeddingPreGameRule(genderSide)
	LuaWeddingIterationMgr:ShowPreGameRule(genderSide)
end

function Gas2Gac.SetSceneDecorationId(decorationId)
	if CScene.MainScene then
		CScene.MainScene.DecorationId = decorationId
	end
end

function Gas2Gac.SyncXingYuLightedStars(starList_ud)
	local list = MsgPackImpl.unpack(starList_ud)
	LuaChristmas2021Mgr.m_LastXingYuLightedStarsList = list
	g_ScriptEvent:BroadcastInLua("SyncXingYuLightedStars",list)
end

function Gas2Gac.XingyuLightStar(starIdx)
end

function Gas2Gac.GetChrismas2021CardGiftSuccess(cardId, giftItemTempId)
	g_ScriptEvent:BroadcastInLua("GetChrismas2021CardGiftSuccess",cardId, giftItemTempId)
end

function Gas2Gac.PutChristmasCardSuccess(guildId, cardId)
	g_ScriptEvent:BroadcastInLua("GiveWishSucceed",guildId,cardId)
end

function Gas2Gac.QueryOtherChristmasCardInGuild_Failed()
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Query_Card_Failed_Ask_Query_Guild"), DelegateFactory.Action(function () 
		LuaChristmas2021Mgr.OpenGuildWishesWnd()
	end), nil, nil, nil, false)
end

function Gas2Gac.QueryOtherChristmasCardInGuild_Result(card_ud)
	if card_ud then
		local list = MsgPackImpl.unpack(card_ud)
		if list then
			local card = {}
            card.Id = list[0]
            card.GuildId = list[1]
            card.PlayerId = list[2]
            card.Msg = list[3]
            card.Gifts = list[4]
            card.Sended = list[5]
            card.SendPlayerId = list[6]
			card.PlayerName = list[7]
			LuaChristmas2021Mgr:OpenWishDetailWnd(card)
		end
	end
end

function Gas2Gac.SubmitSchoolRebuildMeterial_Success()
	g_ScriptEvent:BroadcastInLua("OnSubmitSchoolRebuildMeterialSuccess")
end

function Gas2Gac.RequestEditYardGroundSuccess()
	LuaHouseTerrainMgr.BeginEditTerrain()
end

function Gas2Gac.UpdateLingShouExItemUsedTime(playerId, lingshouId, exChengzhang, exProps)
	g_ScriptEvent:BroadcastInLua("UpdateLingShouExItemUsedTime",playerId, lingshouId, exChengzhang, exProps)
end

function Gas2Gac.QueryGuildLeaguePlayerPosResult(posInfo_U)
	LuaGuildLeagueCrossMgr:QueryGuildLeaguePlayerPosResult(posInfo_U)
end

function Gas2Gac.ShowNewWeddingBaiTangButton(nextProgress)
	LuaWeddingIterationMgr:ShowBaiTangButton(nextProgress)
end

-- 婚礼获取到结婚证
function Gas2Gac.NotifyPlayerGetWeddingCert(certItemId)
	LuaWeddingIterationMgr:NotifyPlayerGetWeddingCert(certItemId)
end

function Gas2Gac.NewWeddingSyncFillInDiskProgress(furnitureId, itemId, progress)
	LuaWeddingIterationMgr:SyncFillInDiskProgress(furnitureId, itemId, progress)
end

function Gas2Gac.NewWeddingSyncXiuQiuMessage(msgId)
	LuaWeddingIterationMgr:SyncXiuQiuMessage(msgId)
end

function Gas2Gac.NewWeddingHideNpc(engineId)
	LuaWeddingIterationMgr:HideNpc(engineId)
end

function Gas2Gac.NewWeddingSyncXiuQiuMovement(fromEngineId, toEngineId)
	LuaWeddingIterationMgr:SyncXiuQiuMovement(fromEngineId, toEngineId)
end

function Gas2Gac.NewWeddingSyncEnjoyFoodResult(furnitureId, leftTimes, bEnjoyed)
	LuaWeddingIterationMgr:SyncEnjoyFoodResult(furnitureId, leftTimes, bEnjoyed)
end

function Gas2Gac.NewWeddingStartPlayCountDown(count, time)
end

function Gas2Gac.NewWeddingShowPlayStartFx()
	LuaWeddingIterationMgr:ShowPlayStartFx()
end

function Gas2Gac.NewWeddingShowPlayEndFx()
	LuaWeddingIterationMgr:ShowPlayEndFx()
end

function Gas2Gac.NewWeddingSyncYiXianQianSelfMatchInfo(targetId, targetName, round, roundLeftTime, bSigned, bCutOff)
	LuaWeddingIterationMgr:SyncYiXianQianSelfMatchInfo(targetId, targetName, round, roundLeftTime, bSigned, bCutOff)
end

function Gas2Gac.NewWeddingSyncYiXianQianSceneMatchInfo(matchUd)
	LuaWeddingIterationMgr:SyncYiXianQianSceneMatchInfo(matchUd)
end

function Gas2Gac.SyncPartnerSkipNewWeddingYiXianQian(partnerId)
	LuaWeddingIterationMgr:SyncPartnerSkipYiXianQian(partnerId)
end

function Gas2Gac.SyncPartnerSkipNewWeddingNaoDongFang(partnerId)
	LuaWeddingIterationMgr:SyncPartnerSkipNaoDongFang(partnerId)
end

function Gas2Gac.SyncPartnerEndNewWedding(partnerId)
	LuaWeddingIterationMgr:SyncPartnerEndWedding(partnerId)
end

function Gas2Gac.NewWeddingSyncSendCandy(candyId)
	LuaWeddingIterationMgr:SyncSendCandy(candyId)
end

-- 红色法宝套装词条是否被飞升调整了
function Gas2Gac.RedFabaoSuitBeChanged(changed)
	--TODO 
	--print("RedFabaoSuitBeChanged", changed)
end

function Gas2Gac.NewWeddingSyncGuestSide(guestSide)
	LuaWeddingIterationMgr:SyncGuestSide(guestSide)
end

function Gas2Gac.NewWeddingSyncBaiTangEndFx(groomId, brideId, stage)
	LuaWeddingIterationMgr:SyncBaiTangEndFx(stage)
end

-- 婚前仪式答对数目,变化时同步
function Gas2Gac.NewWeddingSyncPregameRightAnswer(rightCount)
	LuaWeddingIterationMgr:SyncPregameRightAnswer(rightCount)
end

-- 场景中宾客数目,变化时同步
function Gas2Gac.NewWeddingSyncGuestCount(guestCount)
	LuaWeddingIterationMgr:SyncGuestCount(guestCount)
end

-- 报名参加婚礼小玩法的结果
function Gas2Gac.NewWeddingSyncSignupDone(stage, bOk)
	LuaWeddingIterationMgr:SyncSignupDone(stage, bOk)
end

-- 婚礼开始游行 (旧婚礼RPC)
function Gas2Gac.GroomStartParade()
	LuaWeddingIterationMgr:GroomStartParade()
end

-- 婚礼结束游行 (旧婚礼RPC)
function Gas2Gac.GroomStopParade()
	LuaWeddingIterationMgr:GroomStopParade()
end

-- 请求求婚 (旧婚礼RPC)
function Gas2Gac.PlayerRequestEngage()
	LuaWeddingIterationMgr:PlayerRequestEngage()
end

-- 收到求婚 (旧婚礼RPC)
function Gas2Gac.PlayerRequestEngageMe(playerId, playerName, itemName)
	LuaWeddingIterationMgr:PlayerRequestEngageMe(playerId, playerName, itemName)
end

-- 请求开始测八字 (旧婚礼RPC)
function Gas2Gac.PlayerRequestOpenCeBaZiWnd()
	LuaWeddingIterationMgr:PlayerRequestOpenCeBaZiWnd()
end

-- 开启测八字窗口 (旧婚礼RPC)
function Gas2Gac.OpenCeBaZiWnd()
	LuaWeddingIterationMgr:OpenCeBaZiWnd()
end

-- 获得八字结果 (旧婚礼RPC)
function Gas2Gac.SendBaZiResult(itemId)
	LuaWeddingIterationMgr:SendBaZiResult(itemId)
end

-- 更新纪念日礼物信息(旧婚礼RPC)
function Gas2Gac.UpdateWeddingDayGiftInfo(honeyMoonGiftGot, honeyMoonGiftTime, hundredDayGiftGot, hundredDayGiftTime, anniversaryGiftGot, anniversaryGiftTime)
	LuaWeddingIterationMgr:UpdateWeddingDayGiftInfo(honeyMoonGiftGot, honeyMoonGiftTime, hundredDayGiftGot, hundredDayGiftTime, anniversaryGiftGot, anniversaryGiftTime)
end

-- 千千结宣言界面(旧婚礼RPC)
function Gas2Gac.OpenWeddingDayDeclareWnd(groomName, brideName)
	LuaWeddingIterationMgr:ShowWeddingDayPromiseWnd(groomName, brideName)
end

-- 测八字界面(旧婚礼RPC)
function Gas2Gac.CancelCeBaZi()
	LuaWeddingIterationMgr:CancelCeBaZi()
end

function Gas2Gac.SyncNewWeddingSkybox(skyboxId)
	LuaWeddingIterationMgr:SyncSkybox(skyboxId)
end

function Gas2Gac.GaoChangInviteToApply()
    MessageWndManager.ShowOKCancelMessageWithTimelimitAndPriority(
            g_MessageMgr:FormatMessage("GAOCHANG_OPEN_INFORM"), 300, 0,
            DelegateFactory.Action(function()
                Gac2Gas.GaoChangQueryApplyInfo() end),nil,
            nil,nil,false)
end

function Gas2Gac.NewWeddingSyncSayContent(engineId, sayId)
	LuaWeddingIterationMgr:SyncSayContent(engineId, sayId)
end

function Gas2Gac.SyncNewWeddingSignupYiXianQianDone()
end

function Gas2Gac.SyncNewWeddingNDFSceneInfo(groomId, brideId)
	LuaWeddingIterationMgr:SyncNDFSceneInfo(groomId, brideId)
end

function Gas2Gac.NewWeddingSyncYiXianQianSceneCutOffInfo(playerId, matchPlayerId)
	LuaWeddingIterationMgr:SyncYiXianQianSceneCutOffInfo(playerId, matchPlayerId)
end

-- 外卡赛1-5 主赛事6-11
function Gas2Gac.QueryDouHunCrossGambleInfoResult(gambleType, bOpenDoubleBet, gambleInfo_U, betInfo_U)
	print("QueryDouHunCrossGambleInfoResult", gambleType)
	local gambleInfo = g_MessagePack.unpack(gambleInfo_U)
	-- betinfo会返回所有gambleType的info
	local betInfoUnpack = g_MessagePack.unpack(betInfo_U)

	-- 个人竞猜数据
	local betData = {}
	for i=1, 11 do
		local data = {}
		data.hasGamble = false
		data.choice = 0
		data.silver = 0
		data.hasGotReward = false
		data.hasDouble = false
		data.betInProcess = true
		data.rewardSilver = 0
		table.insert(betData, data)
	end

	for i= 1, 11 do
		local info = betInfoUnpack[i]
		if info then
			local data = betData[i]
			data.hasGamble = true
			data.choice = info[1]
			data.silver = info[2]
			if data.silver == 0 then
				data.hasGamble = false
			end
			data.hasGotReward = info[3]
			data.hasDouble = info[4]
			data.betInProcess = info[5]
			data.rewardSilver = info[6]
		end
	end

	-- 总体竞猜数据
	local gambleData = {}
	gambleData.result = gambleInfo["result"]
	gambleData.bSetResult = gambleInfo["bSetResult"]
	gambleData.totalSilver = gambleInfo["totalSilver"]
	gambleData.closeTime = gambleInfo["closeTime"]
	gambleData.finishTime = gambleInfo["finishTime"]
	--print("result", gambleData.result, gambleData.bSetResult, "totalSilver", gambleData.totalSilver, "silver",betData[gambleType].silver)

	-- 主赛事有8个choice
	local choiceInfo = gambleInfo["choiceInfo"]
	local choiceSilver = gambleInfo["choiceSilver"]
	
	-- 每种押注的金额 用来计算可以获得多少钱
	-- 外卡赛 0000 - 1111
	-- 主赛事 1-8
	local betInfo = gambleInfo["betInfo"]

	gambleData.betInfo = betInfo

	if gambleType > 5 then
		-- 主赛事有八支队伍
		gambleData.serverData = {}
		-- 队伍信息
		local total = 0
		for i=1,8 do
			local serverData = {}
			serverData.Id = choiceInfo[i][1]
			total = total + choiceSilver[serverData.Id] or 0
		end

		for i=1,8 do
			local serverData = {}
			serverData.Id = choiceInfo[i][1]
			serverData.ServerName = choiceInfo[i][2]
			serverData.ServerSilver = choiceSilver[serverData.Id] or 0
			serverData.Percent = (choiceSilver[serverData.Id] or 0) /math.max(total, 1)
			gambleData.serverData[i] = serverData
			gambleData.serverData[serverData.Id] = serverData
			----print("serverData.Id", serverData.Id)
		end
		g_ScriptEvent:BroadcastInLua("QueryDouHunCrossGambleInfoResult", gambleType, bOpenDoubleBet, gambleData, betData)
	end
end

function Gas2Gac.OpenDouHunCrossGambleWnd(gambleStage)
	CLuaFightingSpiritMgr:TryToOpenGambleWnd(gambleStage)
end

function Gas2Gac.BetDouHunCrossGambleFailed(gambleType, choice, silver)
	print("BetDouHunCrossGambleFailed", gambleType, choice, silver)
	g_ScriptEvent:BroadcastInLua("BetDouHunCrossGambleFailed", gambleType, choice, silver)
end

function Gas2Gac.BetDouHunCrossGambleSuccess(gambleType, choice, silver)
	--print("BetDouHunCrossGambleSuccess", gambleType, choice, silver)
	g_ScriptEvent:BroadcastInLua("BetDouHunCrossGambleSuccess", gambleType, choice, silver)
end

function Gas2Gac.DoubleBetDouHunCrossGambleFailed(gambleType, choice, silver)
	--print("DoubleBetDouHunCrossGambleFailed", gambleType, choice, silver)
	g_ScriptEvent:BroadcastInLua("DoubleBetDouHunCrossGambleFailed", gambleType, choice, silver)
end

function Gas2Gac.DoubleBetDouHunCrossGambleSuccess(gambleType, choice, silver)
	--print("DoubleBetDouHunCrossGambleSuccess", gambleType, choice, silver)
	g_ScriptEvent:BroadcastInLua("DoubleBetDouHunCrossGambleSuccess", gambleType, choice, silver)
end

function Gas2Gac.RequestGetDouHunCrossGambleRewardSuccess(gambleType, silver)
	--print("RequestGetDouHunCrossGambleRewardSuccess", gambleType, silver)
	g_ScriptEvent:BroadcastInLua("RequestGetDouHunCrossGambleRewardSuccess", gambleType, silver)
end

function Gas2Gac.QueryDouHunCrossServerGroupInfoResult(data_U)
	local data = MsgPackImpl.unpack(data_U)
	local groupId = data[0]
	local declaration = data[1]
	local memberInfo = data[2]
	local leaderID = data[3]
	--print("QueryDouHunCrossServerGroupInfoResult", groupId, declaration, memberInfo, leaderID)
	local teamInfo = CFightingSpiritMgr.Instance:UnpackTeamInfo(data)
	CLuaFightingSpiritMgr.OpenOtherTeam(teamInfo)
end

function Gas2Gac.AddSkillAppearance(appearId, expiredTime)
	if CClientMainPlayer.Inst then
		local appInfo = CClientMainPlayer.Inst.PlayProp.SkillAppearInfo
		if appInfo then
			if CommonDefs.DictContains(appInfo, typeof(UInt32), appearId) then
				appInfo[appearId] = expiredTime
			else
				CommonDefs.DictAdd(appInfo, typeof(UInt32), appearId, typeof(UInt32), expiredTime)
    		end
		end

		CLuaFightingSpiritMgr.m_SkillAppearancePerview = true
        LuaAppearancePreviewMgr:ShowSkillAppearanceSubview()
	end
end

function Gas2Gac.SetSkillAppearanceSuccess(appearId, expiredTime)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.AppearanceProp.SkillAppearId = appearId
		CClientMainPlayer.Inst.AppearanceProp.SkillAppearExpiredTime = expiredTime
	end
	g_ScriptEvent:BroadcastInLua("SetSkillAppearanceSuccess", appearId, expiredTime)
end

function Gas2Gac.QueryDouHunTrainRankResult(trainType, rankData_U)
	--print("QueryDouHunTrainRankResult")
	local list = MsgPackImpl.unpack(rankData_U)
	local rankList = {}

	local mainPlayerInfo = {}
	mainPlayerInfo.rank = nil
	mainPlayerInfo.name = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerName()
	mainPlayerInfo.count = 0

	local data = DouHunTan_Setting.GetData()
	local maxValueList = {data.MaxJingLi, data.MaxQiLi, data.MaxShenLi}

	local myServerId = 0
	if CClientMainPlayer.Inst then
		myServerId = CClientMainPlayer.Inst:GetMyServerId()
	end
	for i=0, list.Count-1 do
		local data = list[i]
		local rankData = {}
		rankData.rank = i+1
		rankData.serverId = data[0]
		rankData.name = data[1]
		rankData.count = data[2] / maxValueList[trainType]
		if rankData.serverId == myServerId then
			mainPlayerInfo.rank = i+1
			mainPlayerInfo.count = rankData.count
			mainPlayerInfo.name = rankData.name
		end
		table.insert(rankList, rankData)
	end
	g_ScriptEvent:BroadcastInLua("QueryDouHunTrainRankResult", trainType, rankList, mainPlayerInfo)
end


function Gas2Gac.QueryDouHunStageResult(localStage, crossStage, crossLevel, extraInfo)
	local stage = 0
	if localStage == 1 then
		-- 报名
		stage = 1
	elseif localStage == 2 then
		-- 投票
		stage = 2
	elseif localStage >= 3 and localStage <= 5 then
		-- 选拨
		stage = 3
	elseif crossStage >= 101 and crossStage <= 103 then
		-- 外卡赛
		stage = 5
	elseif crossStage == 104 or crossStage == 105 then
		-- 主赛事
		stage = 6
	elseif localStage >=6 and localStage <= 8 then
		-- 培养
		stage = 4
	else
		-- 冠军
		stage = 7
	end

	g_ScriptEvent:BroadcastInLua("QueryDouHunStageResult", stage, crossLevel, localStage, g_MessagePack.unpack(extraInfo))
end

local CGroupMemberInfo = import "L10.Game.CGroupMemberInfo"
function Gas2Gac.QueryDouHunCrossChampionGroupInfoResult(info_U)
	local list = MsgPackImpl.unpack(info_U)

	local memberInfoList = list[3]
	local level = list[0]
	local servername = list[1]
	local leaderId = list[2]
	local dataList = {}

	for i=0, memberInfoList.Count -1 do
		local memberInfo = memberInfoList[i]
		local data = CreateFromClass(CGroupMemberInfo)
		data:LoadFromString(memberInfo)
		table.insert(dataList, data)
	end

	g_ScriptEvent:BroadcastInLua("QueryDouHunCrossChampionGroupInfoResult", level, servername, leaderId, dataList)
end

-- 婚前仪式，对方选择了开启
function Gas2Gac.SyncPartnerStartNewWeddingPreGame(senderId)
	LuaWeddingIterationMgr:SyncPartnerStartPreGame(senderId)
end

function Gas2Gac.OpenQingMing2022PVEQuestionWnd(questionId, timeout)
	LuaQingMing2022Mgr:OpenYHPZQuestionWnd(questionId, timeout)
end

function Gas2Gac.CloseQingMing2022PVEQuestionWnd()
	LuaQingMing2022Mgr:CloseYHPZQuestionWnd()
end

function Gas2Gac.QingMing2022PVESyncScoreInPlay(score)
	LuaQingMing2022Mgr:SyncYHPZScoreInPlay(score)
end

function Gas2Gac.QingMing2022PVEPlayResult(score, todayMaxScore, newrecord)
	LuaQingMing2022Mgr:ShowYHPZResultWnd(score, todayMaxScore, newrecord)
end

function Gas2Gac.QingMing2022SingleOpenBuffSelectionWnd(tempBuffList, tempSkillId, timeout)
	LuaQingMing2022Mgr:OpenHJSMWnd(tempBuffList, tempSkillId, timeout)
end

EnumJuDianBattleMapState = {
	NotTaken = 1,
	Taken = 2,
	Supply = 3,
	NotChosen = 4,
	Monster = 5,
	LingQuan = 6,
}

function Gas2Gac.GuildJuDianReplySceneOverview(battleFieldId, currentStage, statusEndTime, notTakenSceneListUd, takenSceneListUd, supplySceneListUd, tagSceneListUd)
	local notTakenSceneList = MsgPackImpl.unpack(notTakenSceneListUd)
	local takenSceneList = MsgPackImpl.unpack(takenSceneListUd)
	local supplySceneList = MsgPackImpl.unpack(supplySceneListUd)
	local tagSceneList = MsgPackImpl.unpack(tagSceneListUd)

	local mapList = {}
	for i = 0, notTakenSceneList.Count -1 do
		local mapId = notTakenSceneList[i]
		local data = {}
		data.state = EnumJuDianBattleMapState.NotTaken
		mapList[mapId] = data
	end
	
	for i = 0, takenSceneList.Count -1, 3 do
		local mapId = takenSceneList[i]
		local guildId = takenSceneList[i+1]
		local guildName = takenSceneList[i+2]

		local data = {}
		data.state = EnumJuDianBattleMapState.Taken
		data.guildId = guildId
		data.guildName = guildName
		mapList[mapId] = data
	end
	
	for i = 0, supplySceneList.Count -1, 3 do
		local mapId = supplySceneList[i]
		local bForeCast = supplySceneList[i+1]
		local nexRefreshTime = supplySceneList[i+2]

		local data = {}
		data.state = EnumJuDianBattleMapState.Supply
		data.bShow = not bForeCast -- 是否为预警,true表示即将刷出来,false表示已经刷出来了
		data.supplyEndTime = nexRefreshTime
		mapList[mapId] = data
	end

	for i = 0, tagSceneList.Count -1, 1 do
		local mapId = tagSceneList[i]
		local data = {}
		if mapList[mapId] then
			data = mapList[mapId]
			data.tag = true
		end
	end
	g_ScriptEvent:BroadcastInLua("GuildJuDianReplySceneOverview", battleFieldId, currentStage, statusEndTime, mapList)
end

-- 某分线的场景地图 包含 据点旗、据点、无名神像等信息
function Gas2Gac.GuildJuDianSyncMapSceneInfo(mapId, sceneIdx, sceneId, tagInfoUd, takenFlagInfoUd, notTakenFlagInfoUd, takenJuDianUd, notTakenJuDianUd, supplyNpcInfoUd, teleportCdRemain)
	print("teleportCdRemain", teleportCdRemain)
	-- 场景上的标记点
	local tagInfo = MsgPackImpl.unpack(tagInfoUd)
	local tag = nil
	if tagInfo and tagInfo.Count == 2 then
		tag = {}
		tag.x = tagInfo[0]
		tag.y = tagInfo[1]
	end
	
	-- 已夺旗帜 可能是本帮或者外帮
	local takenFlagInfo = MsgPackImpl.unpack(takenFlagInfoUd)
	local flagList = {}

	for i = 0, takenFlagInfo.Count -1, 7 do
		local flagData = {}
		flagData.flagId = takenFlagInfo[i]
		flagData.x = takenFlagInfo[i+1]
		flagData.y = takenFlagInfo[i+2]
		flagData.guildId = takenFlagInfo[i+3]
		flagData.guildName = takenFlagInfo[i+4]
		flagData.playerId = takenFlagInfo[i+5]
		flagData.playerName = takenFlagInfo[i+6]

		if flagData.guildId == 0 then
			flagData.hasTaken = false
		else
			flagData.hasTaken = true
		end
		table.insert(flagList, flagData)
	end
	
	-- 未夺旗帜
	local notTakenFlagInfo = MsgPackImpl.unpack(notTakenFlagInfoUd)
	for i = 0, notTakenFlagInfo.Count -1, 3 do
		local flagData = {}
		flagData.flagId = notTakenFlagInfo[i]
		flagData.x = notTakenFlagInfo[i+1]
		flagData.y = notTakenFlagInfo[i+2]
		flagData.hasTaken = false
		table.insert(flagList, flagData)
	end
	
	-- 已夺据点
	local takenJuDian = MsgPackImpl.unpack(takenJuDianUd)
	local juDianList = {}
	for i = 0, takenJuDian.Count -1, 5 do
		local juDianData = {}
		juDianData.judianId = takenJuDian[i]
		juDianData.x = takenJuDian[i+1]
		juDianData.y = takenJuDian[i+2]
		juDianData.guildId = takenJuDian[i+3]
		juDianData.guildName = takenJuDian[i+4]
		juDianData.hasTaken = true
		table.insert(juDianList, juDianData)
	end

	-- 未夺据点
	local notTakenJuDian = MsgPackImpl.unpack(notTakenJuDianUd)
	for i = 0, notTakenJuDian.Count -1, 5 do
		local juDianData = {}
		juDianData.judianId = notTakenJuDian[i]
		juDianData.x = notTakenJuDian[i+1]
		juDianData.y = notTakenJuDian[i+2]
		juDianData.guildId = notTakenJuDian[i+3]
		juDianData.guildName = notTakenJuDian[i+4]
		juDianData.hasTaken = false
		table.insert(juDianList, juDianData)
	end

	local supplyNpcInfo = MsgPackImpl.unpack(supplyNpcInfoUd)
	local npcList = {}
	for i = 0, supplyNpcInfo.Count -1, 5 do
		local bForecast = supplyNpcInfo[i+1]

		local npcData = {}
		npcData.judianIdx = supplyNpcInfo[i] -- npcId
		npcData.bShow = not bForecast
		npcData.nextRefreshTime = supplyNpcInfo[i+2]
		npcData.x = supplyNpcInfo[i+3]
		npcData.y = supplyNpcInfo[i+4]
		
		table.insert(npcList, npcData)
	end

	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncMapSceneInfo", mapId, sceneIdx, sceneId, tag, flagList, juDianList, npcList, teleportCdRemain)
end

-- 每个分线的人数
-- 自己所处分线
function Gas2Gac.GuildJuDianSyncPlayerCountByMapId(mapId, currentSceneIdx, playerCountUd)
	local playerCountInfo = MsgPackImpl.unpack(playerCountUd)
	local serverDataList = {}
	for i = 0, playerCountInfo.Count -1, 2 do
		local data = {}
		data.sceneIdx = playerCountInfo[i]
		data.playerCount = playerCountInfo[i+1]
		serverDataList[data.sceneIdx] = data
	end
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncPlayerCountByMapId", mapId, currentSceneIdx, serverDataList)
end

-- TODO 同步所有场景的人数，包括战斗场景和师门
function Gas2Gac.GuildJuDianSyncMapPlayerCountAll(playerCountListUd)
	local playerCountList = g_MessagePack.unpack(playerCountListUd)
	if not playerCountList then return end
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncMapPlayerCountAll", playerCountList)
end

-- TODO 同步师门场景的灵气数量
function Gas2Gac.GuildJuDianSyncMapLingQuanCount(lingQiCountListUd)
	local lingQuanCountList = g_MessagePack.unpack(lingQiCountListUd)
	if not lingQuanCountList then return end
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncMapLingQuanCount", lingQuanCountList)
end

-- TODO 同步师门场景的灵气位置
function Gas2Gac.GuildJuDianQueryMapLingQuanPosResult(nextRefreshTime, lingQiPosListUd)
	local lingQuanPosList = g_MessagePack.unpack(lingQiPosListUd)
	if not lingQuanPosList then return end
	g_ScriptEvent:BroadcastInLua("GuildJuDianQueryMapLingQuanPosResult", nextRefreshTime, lingQuanPosList)
end

-- TODO 备战提交灵气界面信息
function Gas2Gac.GuildJuDianSendLingQiSubmitInfoNew(lingQiInfoU)
	local lingQiInfo = g_MessagePack.unpack(lingQiInfoU)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSendLingQiSubmitInfoNew", lingQiInfo)
end

function Gas2Gac.GuildJuDianQueryGuildGongFengDataResult(dataU)
	local data = g_MessagePack.unpack(dataU)
	g_ScriptEvent:BroadcastInLua("GuildJuDianQueryGuildGongFengDataResult", data)
end

function Gas2Gac.GuildJuDianSendZhanLongTaskInfo(taskInfoU)
	local data = g_MessagePack.unpack(taskInfoU)
	if #data < 1 then return end
	g_ScriptEvent:BroadcastInLua("GuildJuDianSendZhanLongTaskInfo", data)
end

-- 前四/自己帮会信息
function Gas2Gac.GuildJuDianSyncTopFourGuildRank(topGuildRankUd)
	local topGuildRankInfo = MsgPackImpl.unpack(topGuildRankUd)
	local rankData = {}
	for i = 0, topGuildRankInfo.Count -1, 4 do
		local data = {}
		data.rankPos = topGuildRankInfo[i]
		data.guildId = topGuildRankInfo[i+1]
		data.guildName = topGuildRankInfo[i+2]
		data.score = topGuildRankInfo[i+3]
		table.insert(rankData, data)
	end
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncTopFourGuildRank", rankData)
end

-- 玩家不在占领地图，收到的前四帮会排名数据
-- 帮会排名与积分
function Gas2Gac.GuildJuDianSyncGlobalTopRank(topGuildRankUd, selfRankUd)
	local rankData = {}
	local guildLookup = {}

	local topGuildRankInfo = MsgPackImpl.unpack(topGuildRankUd)
	for i = 0, topGuildRankInfo.Count -1, 4 do
		local data = {}
		data.rankPos = topGuildRankInfo[i]
		data.guildId = topGuildRankInfo[i+1]
		data.guildName = topGuildRankInfo[i+2]
		data.score = topGuildRankInfo[i+3]

		guildLookup[data.guildId] = true
		table.insert(rankData, data)
	end
	
	local rankInfo = MsgPackImpl.unpack(selfRankUd)
	for i = 0, rankInfo.Count -1, 8 do
		local data = {}
		data.rankPos = rankInfo[i]
		data.guildId = rankInfo[i+1]
		data.guildName = rankInfo[i+2]
		data.score = rankInfo[i+3]

		local seasonData = {}
		seasonData.season = rankInfo[i+4]
		seasonData.week = rankInfo[i+5]
		seasonData.battleFieldId = rankInfo[i+6]
		seasonData.playStage = rankInfo[i+7]
		seasonData.rankPos = data.rankPos
		LuaJuDianBattleMgr.SeasonData = seasonData

		if not guildLookup[data.guildId] then
			table.insert(rankData, data)
		end
	end

	LuaJuDianBattleMgr.GuildRankData = rankData
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncGlobalTopRank", rankData)
end

-- 玩家在占领地图，收到的占领进度数据
-- 每个据点的占领信息 和 旗帜信息
function Gas2Gac.GuildJuDianSyncCurrentSceneProgress(ud, flagProgressUd)
	local list = MsgPackImpl.unpack(ud)
	-- 用index作为据点信息的key
	local judianData = {}
	for i = 0, list.Count -1, 3 do
		local data = {}
		data.judianIdx = list[i]
		data.takenGuildId = list[i+1]
		data.takenGuildName = list[i+2]
		judianData[data.judianIdx] =  data
	end

	local list2 = MsgPackImpl.unpack(flagProgressUd)
	local flagData = {}
	if list2 then
		flagData.takenGuildId = list2[0]
		flagData.takenGuildName = list2[1]
	end

	LuaJuDianBattleMgr.SceneFlagData = flagData
	LuaJuDianBattleMgr.SceneJuDianData = judianData

	-- 显示当前场景的据点和flag信息
	LuaJuDianBattleMgr:SetPlayingShowMode(EnumJuDianPlayingShowMode.JuDianOverview)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncCurrentSceneProgress", judianData, flagData)
end

-- 玩家在占领地图
-- 只有在圈子中的玩家才会收到这个,收到这个才认为在据点中
-- 断线重连/进出圈子/圈子中第一名变化/圈子中本帮会人数变化 会收到这个RPC
function Gas2Gac.GuildJuDianSyncCurrentScenePlayerCountRank(ud, selfGuildUd)
	local list = MsgPackImpl.unpack(ud)
	local rankData = {}
	local guildLookup = {}

	-- 只有1条/0条数据，当前据点的帮会人数第一名
	-- LuaJuDianBattlePlayingWnd:IsInJuDian 应该结合这个来写,收到这个RPC之后，第一名有1条数据触发 in, 0条数据触发out
	local isInJuDian = false
	for i = 0, list.Count -1, 4 do
		local data = {}
		data.judianEngineId = list[i]
		data.guildId = list[i+1]
		data.guildName = list[i+2]
		data.peopleCount = list[i+3]
		isInJuDian = true
		
		guildLookup[data.guildId] = true
		table.insert(rankData, data)
	end

	-- 判断是否在据点内
	local isInJuDian = (list.Count>0)
	LuaJuDianBattleMgr.IsInsideJuDian = isInJuDian


	-- 自己帮会在当前据点的人数
	list = MsgPackImpl.unpack(selfGuildUd)
	for i = 0, list.Count -1, 4 do
		local data = {}
		data.judianEngineId = list[i]
		data.guildId = list[i+1]
		data.guildName = list[i+2]
		data.peopleCount = list[i+3]
		if not guildLookup[data.guildId] then
			table.insert(rankData, data)
		end
	end
	LuaJuDianBattleMgr.JuDianPeopleData = rankData
	LuaJuDianBattleMgr:SetPlayingShowMode(EnumJuDianPlayingShowMode.JuDianDetail)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncCurrentScenePlayerCountRank", rankData)
end

-- 显示据点进度
function Gas2Gac.GuildJuDianSyncCurrentSceneJuDianProgress(judianUd)
	local list2 = MsgPackImpl.unpack(judianUd)

	local judianLookup = {}
	local judianData = {}
	for i = 0, list2.Count -1, 6 do
		local data = {}
		data.judianIdx = list2[i]
		data.takenGuildId = list2[i+1]
		data.progress = list2[i+2]
		data.x = list2[i+3]
		data.y = list2[i+4]
		data.engineId = list2[i+5]
		judianLookup[data.judianIdx] = true
		table.insert(judianData, data)
	end

	if LuaJuDianBattleMgr.JuDianProgressData then
		for i=1, #LuaJuDianBattleMgr.JuDianProgressData do
			local data = LuaJuDianBattleMgr.JuDianProgressData[i]
			if not judianLookup[data.judianIdx] then
				table.insert(judianData, data)
			end
		end
	end

	LuaJuDianBattleMgr:SetPlayingShowMode(EnumJuDianPlayingShowMode.JuDianDetail)
	LuaJuDianBattleMgr.JuDianProgressData = judianData
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncCurrentSceneJuDianProgress", judianData)
end

-- 玩家不在战斗分线，收到的信息
function Gas2Gac.GuildJuDianSyncNoneFightSceneIdx()
	--print("GuildJuDianSyncNoneFightSceneIdx")
	LuaJuDianBattleMgr:SetPlayingShowMode(EnumJuDianPlayingShowMode.Nothing)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncNoneFightSceneIdx")
end

EnumGuildJuDianMemberDetail = {
	Kill = 1,
	Death = 2,
	Dps = 3,
	Baoshi = 4,
	Reborn = 5,
	Control = 6,
	AntiControl = 7,
	Hurt = 8,
	Heal = 9,
	LingQi = 10,
}

function Gas2Gac.GuildJuDianSyncGuildMemberDetail(memberDetailUd)
	-- 用 CGuildJuDianInfoRpc 数据结构的 LoadFromString 方法解析数据
	-- 可能帮会很大，那样的话就会收到多个这样的RPC,每一条RPC中包含多个玩家数据
	local dataList = {}

	local memberDetail = CGuildJuDianInfoRpc()
	memberDetail:LoadFromString(memberDetailUd)

	if memberDetail.Infos then
		CommonDefs.DictIterate(memberDetail.Infos, DelegateFactory.Action_object_object(function(key,v)
			local data = {}
			data.PlayerId = v.PlayerId
			data.Name = v.Name
			data.Class = v.Class
			data[EnumGuildJuDianMemberDetail.Kill] = math.floor(v.KillCount)
			data[EnumGuildJuDianMemberDetail.Death] = math.floor(v.DieCount)
			data[EnumGuildJuDianMemberDetail.Dps] = math.floor(v.Dps)
			data[EnumGuildJuDianMemberDetail.Baoshi] = math.floor(v.BaoShiCount)
			data[EnumGuildJuDianMemberDetail.Reborn] = math.floor(v.RebornCount)
			data[EnumGuildJuDianMemberDetail.Control] = math.floor(v.ControlCount)
			data[EnumGuildJuDianMemberDetail.AntiControl] = math.floor(v.FreeCount)
			data[EnumGuildJuDianMemberDetail.Hurt] = math.floor(v.UnderDamage)
			data[EnumGuildJuDianMemberDetail.Heal] = math.floor(v.Heal)
			data[EnumGuildJuDianMemberDetail.LingQi] = math.floor(v.LingQuanCount)
			print("lingQuanCount=", v.LingQuanCount)	-- TODO

			table.insert(dataList, data)
		end))
	end
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncGuildMemberDetail", dataList)
end

-- 战况中查询帮会成员详情
-- score 占领分
-- rank 占领排名
-- totalCount 占领排名的分母
-- fightLevel 战意等级
-- killCount 击杀数
-- playerCount 场内人数
-- activeBuffUd 神像增益
function Gas2Gac.GuildJuDianSyncGuildMemberDetailEnd(score, rank, totalCount, fightLevel, killCount, playerCount, activeBuffUd)
	local list1 = MsgPackImpl.unpack(activeBuffUd)
	local buffList = {}

	local buffLookUp = {}
	for i = 0, list1.Count -1, 2 do
		local data = {}
		data.BuffId = list1[i]
		data.EndTime = list1[i+1]
		data.Enable = true
		buffLookUp[data.BuffId] = true
		table.insert(buffList, data)
	end

	local npcBuffData = GuildOccupationWar_Setting.GetData().StatueNPCBuffID
	for i=0, npcBuffData.Length-2, 2 do
		local id = npcBuffData[i]
		if not buffLookUp[id] then
			local data = {}
			data.BuffId = id
			data.Enable = false
			table.insert(buffList, data)
		end
	end
	
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncGuildMemberDetailEnd", score, rank, totalCount, fightLevel, killCount, playerCount, buffList)
end

EnumGuildJuDianTopType = {
	Kill = 1,
	Baoshi = 2,
	Control = 3,
	Heal = 4,
}

-- 战场排行榜
-- bShowFx 是否播放X名特效
function Gas2Gac.GuildJuDianSyncBattleFieldRank(bShowFx, topPlayersUd, rankUd)
	local list1 = MsgPackImpl.unpack(topPlayersUd)
	local topList = {}
	for i = 0, list1.Count -1, 7 do
		local data = {}
		data.topType = list1[i] -- 1 击杀 2 暴尸 3控制 4治疗
		data.topPlayerId = list1[i+1]
		data.topPlayerName = list1[i+2]
		data.topValue = math.floor(list1[i+3])
		data.topGuildName = list1[i+4]
		data.class = list1[i+5]
		data.gender = list1[i+6]
		topList[data.topType] = data
	end

	local list2 = MsgPackImpl.unpack(rankUd)
	local guildList = {}
	for i = 0, list2.Count -1, 10 do
		local data = {}
		data.rankPos = list2[i]
		data.guildId = list2[i+1]
		data.guildName = list2[i+2]
		data.serverId = list2[i+3]
		data.serverName = list2[i+4]
		data.playerCount = list2[i+5]
		data.kill = list2[i+6]
		data.score = list2[i+7]
		data.flagCount = list2[i+8]
		data.juDianCount = list2[i+9]
		table.insert(guildList, data)
	end

	if bShowFx then
		LuaJuDianBattleMgr:ShowResultWnd(topList, guildList)
	end
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncBattleFieldRank", bShowFx, topList, guildList)
end

function Gas2Gac.SyncSongPlayPressButton(playerId, buttonIdx, perform, score)
	--print("SyncSongPlayPressButton",playerId, buttonIdx, perform, score)
	LuaMeiXiangLouMgr.SyncSongPlayPressButton(playerId, buttonIdx, perform, score)
end

function Gas2Gac.StartSongPlay(songId)
	LuaMeiXiangLouMgr.StartSongPlay(songId)
end

function Gas2Gac.SongBeatArrived(songId, rhythmIdx, rhythmId)
	LuaMeiXiangLouMgr.SongBeatArrived(songId, rhythmIdx, rhythmId)
end

function Gas2Gac.SongPlayTeamResult(totalScore, leaderId,playerId1,name1,class1,gender1, score1, beatNum1, playerId2,name2,class2,gender2, score2, beatNum2)
	--g_ScriptEvent:BroadcastInLua("EndSongPlay")
	LuaMeiXiangLouMgr.SongPlayTeamResult(totalScore, leaderId,playerId1,name1,class1,gender1, score1, beatNum1, playerId2,name2,class2,gender2, score2, beatNum2)
end

function Gas2Gac.SongPlaySingResult(playerId, score, perfect, good, ok, miss,maxCombo)
	--g_ScriptEvent:BroadcastInLua("EndSongPlay")
	LuaMeiXiangLouMgr.SongPlaySingResult(playerId, score, perfect, good, ok, miss,maxCombo)
end

function Gas2Gac.UpdateTradeSimulationData(isFinish, tradeSimulationUD)
	LuaBusinessMgr:UpdateTradeSimulationData(isFinish, tradeSimulationUD)
end

function Gas2Gac.GetTicketForChangeChannelResult(result, urlWithTicket)
	print("GetTicketForChangeChannelResult", result, urlWithTicket)
	if result == 1 then
		CWebBrowserMgr.Inst:OpenUrl(urlWithTicket)
	end
end

function Gas2Gac.GaoChangCrossApplySuccess()
	LuaGaoChangCrossMgr:GaoChangCrossApplySuccess()
end

function Gas2Gac.GaoChangCrossInviteToStage1()
	LuaGaoChangCrossMgr:GaoChangCrossInviteToStage1()
end

function Gas2Gac.GaoChangCrossInviteToApply()
	LuaGaoChangCrossMgr:GaoChangCrossInviteToApply()
end

function Gas2Gac.StartHouseFishingGuide(fishTempId)
	g_ScriptEvent:BroadcastInLua("StartHouseFishingGuide",fishTempId)
end

function Gas2Gac.GaoChangCrossQueryApplyInfoResult(bApplied)
	LuaGaoChangCrossMgr:GaoChangCrossQueryApplyInfoResult(bApplied)
end

-- 未到时间的task不发
-- { {{taskId, bHave, bFinished}, {taskId, bHave, bFinished},},   }
function Gas2Gac.QueryWuYiManYouSanJieTaskInfoResult(taskInfo_U)
	LuaWuYi2022Mgr:QueryWuYiManYouSanJieTaskInfoResult(taskInfo_U)
end

function Gas2Gac.SyncStarBiwuChampionInScene(zhanduiId, zhanduiName)
	CLuaStarBiwuMgr.championZhanduiName = zhanduiName
    CUIManager.ShowUI(CLuaUIResources.StarBiwuChampionWnd)
end

-- EnumPlayResult
-- 进入boss阶段时 要把祭坛相关信息界面去掉
function Gas2Gac.EnterDaGongHunBossStage(playResult)
	LuaWuYi2022Mgr:EnterDaGongHunBossStage(playResult)
end

function Gas2Gac.SyncDaGongHunJiTanNum(jiTanNum)
	LuaWuYi2022Mgr:SyncDaGongHunJiTanNum(jiTanNum)
end

-- 据点战同步帮会名
-- 可能会增量同步。例：一开始场景中只有A和B两个玩家，都属于G1帮会，这个接口会同步G1帮会信息。后来C进入场景，属于G2帮会，此时只会同步G2帮会信息给场景中所有玩家
function Gas2Gac.GuildJuDianSyncGuildName(nameUd)
	local list = MsgPackImpl.unpack(nameUd)
	if not list then return end
	
	local step = 2

	local guildNameDict = CGuildMgr.Inst.m_GuildID2NameLookup
	local updateGuildList = {}

	for i = 0, list.Count - 1, step do
		local guildId = list[i]
		local guildName = list[i+1]

		if CommonDefs.DictContains(guildNameDict, typeof(UInt32), guildId) then
			local old = guildNameDict[guildId]
			if old ~= guildName then
				guildNameDict[guildId] = guildName
				updateGuildList[guildId] = true
			end
		else
			CommonDefs.DictAdd(guildNameDict, typeof(UInt32), guildId, typeof(String), guildName)
			updateGuildList[guildId] = true
		end
	end

	-- 刷新需要更新的帮会名
	CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
		if TypeIs(obj, typeof(CClientMainPlayer)) or TypeIs(obj, typeof(CClientOtherPlayer)) then
			if obj and obj.BasicProp and updateGuildList[obj.BasicProp.GuildId] then
				EventManager.BroadcastInternalForLua(EnumEventType.UpdateGuildName,{obj.EngineId})
			end
		end
	end))
end

function Gas2Gac.SyncHuluBrotherSummary(selfUseTime, playerId, playerName, bestUseTime)
	local isRuYiDong = LuaHuLuWa2022Mgr.IsInRuYiDongPlay()
	LuaHuLuWa2022Mgr.OpenRuYiDongResultWnd(isRuYiDong,selfUseTime, playerId, playerName, bestUseTime)
end

function Gas2Gac.SyncHuluBrotherBossProgress(killCount)
	LuaHuLuWa2022Mgr.m_KillCount = killCount
	g_ScriptEvent:BroadcastInLua("SyncHuluBrotherBossProgress",killCount)
end

-- {idx, idx2, ...}
function Gas2Gac.QueryRuYiHuluwaUnlockDataResult(unlockDataUd)
	local list = MsgPackImpl.unpack(unlockDataUd)
	if not list then return end
	
	local step = 1

	LuaHuLuWa2022Mgr.m_QiaoDuoRuYiUnLockDic = {}

	for i = 0, list.Count - 1, step do
		LuaHuLuWa2022Mgr.m_QiaoDuoRuYiUnLockDic[tonumber(list[i])] = true
	end

	g_ScriptEvent:BroadcastInLua("QueryRuYiHuluwaUnlockDataResult")
end

-- {idx, playerId, playerName, ...}
function Gas2Gac.QueryHuluwaTransformDataResult(transformDataUd)
	local list = MsgPackImpl.unpack(transformDataUd)
	if not list then return end
	
	local step = 3
	local idx, playerId, playerName

	LuaHuLuWa2022Mgr.m_HuLuWaChooseData = {}
	for i = 0, list.Count - 1, step do
		local t = {}
		t.idx = list[i]
		t.playerId = list[i+1]
		t.playerName = list[i+2]
		if t.idx and t.idx > 0 then
			LuaHuLuWa2022Mgr.m_HuLuWaChooseData[t.idx] = t
		end
	end
	g_ScriptEvent:BroadcastInLua("QueryHuluwaTransformDataResult")
end

function Gas2Gac.SendLianDanLuAlertInfo(p1, p2, p3, vip1, vip2, bAlert)
	LuaHuLuWa2022Mgr.m_SchedulePageAlert = bAlert
	LuaHuLuWa2022Mgr.m_ZhanLingP1 = p1
	LuaHuLuWa2022Mgr.m_ZhanLingP2 = p2
    LuaHuLuWa2022Mgr.m_ZhanLingP3 = p3
	LuaHuLuWa2022Mgr.m_IsVip1 = vip1 == 1
	LuaHuLuWa2022Mgr.m_IsVip2 = vip2 == 1
	g_ScriptEvent:BroadcastInLua("SendLianDanLuAlertInfo",p1, p2, p3, vip1, vip2, bAlert)
end

function Gas2Gac.SyncLianDanLuPlayData(p1, p2, p3, vip1, vip2, bAlert, receiveDataUD)
	--print("==SyncLianDanLuPlayData==", p1, p2, p3, vip1, vip2, bAlert)
	LuaHuLuWa2022Mgr.SyncLianDanLuPlayData(p1, p2,p3, vip1, vip2, bAlert, receiveDataUD)
end

function Gas2Gac.UnLockLianDanLuVipSuccess(vip)
	--print("==UnLockLianDanLuVipSuccess==", vip)
	g_ScriptEvent:BroadcastInLua("UnLockLianDanLuVipSuccess", vip)
end

function Gas2Gac.SendLianDanLuTaskData(taskDataUD)
	print("==SendLianDanLuTaskData==")
	LuaHuLuWa2022Mgr.SendLianDanLuTaskData(taskDataUD)
end

function Gas2Gac.ReceiveLianDanLuRewardSuccess(level, index)
	print("==ReceiveLianDanLuRewardSuccess==", level, index)
	g_ScriptEvent:BroadcastInLua("ReceiveLianDanLuRewardSuccess", level, index)
end

function Gas2Gac.GuildJuDianSyncJoinQualification(bCanJoin)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncJoinQualification", bCanJoin)
end

function Gas2Gac.GuildJuDianSyncSetQualification(bCanTag, bCanKickoutPlayer)
	print("GuildJuDianSyncSetQualification", bCanTag, bCanKickoutPlayer)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncSetQualification", bCanTag, bCanKickoutPlayer)
end

function Gas2Gac.SyncHuluwaActivityAwardTimes(ud)
	local list = MsgPackImpl.unpack(ud)
	-- list的5项目前分别对应游历三界,勇闯如意洞和巧夺如意的次数 + 如意洞是否开启， 葫芦娃是否开启
    if list and list.Count >= 5 then
        LuaHuLuWa2022Mgr.m_IsRuYiServerSwitchOn = list[3]
        LuaHuLuWa2022Mgr.m_IsHuLuWaServerSwitchOn = list[4]
    end
    --2022.5.5正式投放后忽略最后两个参数
    local beginDate = CreateFromClass(DateTime, 2022, 5, 5, 0, 0, 0)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    if DateTime.Compare(now, beginDate) >= 0 then
        LuaHuLuWa2022Mgr.m_IsRuYiServerSwitchOn = false
        LuaHuLuWa2022Mgr.m_IsHuLuWaServerSwitchOn = false
    end
	g_ScriptEvent:BroadcastInLua("SyncHuluwaActivityAwardTimes", list)
end

function Gas2Gac.SyncRollHuluwaAdventure(value, leftTimes)
	LuaHuLuWa2022Mgr.m_LeftTravelTimes = leftTimes
	g_ScriptEvent:BroadcastInLua("SyncRollHuluwaAdventure",value, leftTimes)
end

function Gas2Gac.SyncHuluwaAdventureWndInfo(huluwaId, currentPosId, leftTravelTimes, visitedListUd, bVisited)
	local list = MsgPackImpl.unpack(visitedListUd)
	
	-- list里面是 Brothers_Adventure 这张表的id
	LuaHuLuWa2022Mgr.m_TravelingHuluwaId = huluwaId
	LuaHuLuWa2022Mgr.m_CurTravelPosId = currentPosId
	LuaHuLuWa2022Mgr.m_LeftTravelTimes = leftTravelTimes
	LuaHuLuWa2022Mgr.m_VisitedPosDict = {}
	LuaHuLuWa2022Mgr.m_IsVisited = bVisited
	if list then
		for i=0,list.Count-1,1 do
			LuaHuLuWa2022Mgr.m_VisitedPosDict[tonumber(list[i])] = true
		end
	end
	--是否已打开界面
	if CUIManager.IsLoaded(CLuaUIResources.HuLuWaTravelWorldWnd) then
		g_ScriptEvent:BroadcastInLua("SyncHuluwaAdventureWndInfo")
	else
		CUIManager.ShowUI(CLuaUIResources.HuLuWaTravelWorldWnd)
	end
end

function Gas2Gac.QWD_ShowNextFloorBuff(candidateBuffData, selectedBuffsData)
	-- candidateBuffData list{ dict{ buffId, value list{...} } }
	--print("QWD_ShowNextFloorBuff", buff1, buff2, buff3)
	LuaDuanWu2022Mgr:OpenBuffWnd(candidateBuffData, selectedBuffsData)
end

function Gas2Gac.QWD_SyncPlayResult(difficulty1, difficulty2, difficulty3, floor, usedTime, score, bBreakRecord)
	print("QWD_SyncPlayResult", difficulty1, difficulty2, difficulty3, floor, usedTime, score, bBreakRecord)
	LuaDuanWu2022Mgr:OpenResultWnd(difficulty1, difficulty2, difficulty3, floor, usedTime, score, bBreakRecord)
end

function Gas2Gac.SyncHuluwaBuffLevel(buffId, level)
	g_ScriptEvent:BroadcastInLua("SyncHuluwaBuffLevel",buffId, level)
end

EnumJuDianBattleMapItem = {
	Flag = 0,
	JuDian1 = 1,
	JuDian2 = 2,
	JuDian3 = 3,
}

function Gas2Gac.GuildJuDianSyncGuildFlagAndJuDian(guildId, takenFlagInfoUd, takenJuDianInfoUd)
	local takenFlagList = MsgPackImpl.unpack(takenFlagInfoUd)

	local mapDataList = {}
	if takenFlagList then
		for i = 0, takenFlagList.Count - 1, 1 do
			local mapId = takenFlagList[i] -- 据点旗只有第一分线有,所以知道mapid就可以了
			local data = {}
			table.insert(data, EnumJuDianBattleMapItem.Flag)
			mapDataList[mapId*100 + 1] = data
		end
	end
	
	local takenJuDianList = MsgPackImpl.unpack(takenJuDianInfoUd)
	
	if takenJuDianList then
		for i = 0, takenJuDianList.Count - 1, 1 do
			-- list中的每一项都是一个单独的list，包含了一个map所有分线的据点占领信息，第一项是mapid，后面全是分线数
			local mapInfo = takenJuDianList[i]
			local mapInfoLen = mapInfo.Count

			local mapId = mapInfo[0]
			for j = 1, mapInfoLen - 1, 2 do
				local sceneIdx = mapInfo[j] -- 分线数
				local judianIdx = mapInfo[j+1] -- 据点id

				local data = mapDataList[mapId*100 + sceneIdx]
				if data == nil then
					data = {}
					mapDataList[mapId*100 + sceneIdx] = data
				end
				table.insert(data, judianIdx)
			end
		end
	end

	g_ScriptEvent:BroadcastInLua("GuildJuDianSyncGuildFlagAndJuDian", mapDataList)
end

-- 同步 beginnerGuideInfo
-- @param beginnerGuideInfoU 序列化后的 CBeginnerGuide, 需要 LoadFromString
function Gas2Gac.SyncBeginnerGuideInfo(beginnerGuideInfoU)
	LuaQianYingChuWenMgr:SyncBeginnerGuideInfo(beginnerGuideInfoU)
end

-- 玩法开放给玩家, 可以进行引导
function Gas2Gac.OpenBeginnerGuideInfoToPlayer()
	LuaQianYingChuWenMgr:OpenBeginnerGuideInfoToPlayer()
end

-- 玩家接取新手引导开启任务, 此时就把登录奖励关了
function Gas2Gac.CloseLoginDaysAward()
	LuaQianYingChuWenMgr:CloseLoginDaysAward()
end

-- 领取任务奖励成功
-- Gas2Gac.SyncBeginnerGuideInfo 会在这个之前就发下去, 所以这里可以不用更新数据
-- @param taskId			领取奖励的任务Id
-- @param postTaskId		后续任务Id, 方便刷界面, 其实也可以读表, 如果是0的话那这个目标就结束了
-- @param todayAllFinish	是否本日任务都完成了, 都完成了可以界面上显示获得碎片
function Gas2Gac.RequestBeginnerGuideTaskRewardSuccess(taskId, postTaskId, todayAllFinish)
	LuaQianYingChuWenMgr:RequestBeginnerGuideTaskRewardSuccess(taskId, postTaskId, todayAllFinish)
end

-- 一键领取任务奖励成功
-- Gas2Gac.SyncBeginnerGuideInfo 会在这个之前就发下去, 所以这里可以不用更新数据
-- @param tasks				领取了哪些任务的奖励 { taskId, taskId, ... }
-- @param todayAllFinish	本次领取导致哪些天的任务都完成了, { x天, x天, ... }
function Gas2Gac.RequestBeginnerGuideBatchTaskRewardSuccess(tasks, todayAllFinishT)
	LuaQianYingChuWenMgr:RequestBeginnerGuideBatchTaskRewardSuccess(tasks, todayAllFinishT)
end

-- 领取最终奖励成功
-- Gas2Gac.SyncBeginnerGuideInfo 会在这个之前就发下去, 所以这里可以不用更新数据
-- 所有相关数据都清空了, 只有一个 Stage 变为了EnumBeginnerGuideStage.eFinish
-- @param rewardId			1: 领取的第一个奖励, 2: 领取的第二个奖励
function Gas2Gac.RequestBeginnerGuideFinalRewardSuccess(rewardId)
	LuaQianYingChuWenMgr:RequestBeginnerGuideFinalRewardSuccess(rewardId)
end

-- 弹出葫芦娃游历三界的窗口
function Gas2Gac.SyncHuluwaAdventurePopWnd()
	LuaHuLuWa2022Mgr.OpenTravelWorldWnd()
end

function Gas2Gac.SyncHuluwaAdventureVisitStatusChange(bVisited)
	g_ScriptEvent:BroadcastInLua("SyncHuluwaAdventureVisitStatusChange",bVisited)
end

-- 据点战战意等级变化
function Gas2Gac.GuildJuDianSyncFightLevelChange(newLevel)
	LuaJuDianBattleMgr:ShowZhanYiLevelUp(newLevel)
end

function Gas2Gac.SyncHuluwaBossPosition(templateId, x, y)
	LuaHuLuWa2022Mgr.BossPosInfo = {
		templateId = templateId, 
		x=x, 
		y=y
	}
	g_ScriptEvent:BroadcastInLua("SyncHuluwaBossPosition",templateId, x, y)
end

-- 海战船舱内副本同步船只血量rpc
function Gas2Gac.SyncNavalWarCabinShipHp(hp, currHpFull, HpFull)
	-- print("SyncNavalWarCabinShipHp", hp, currHpFull, HpFull)
	LuaHaiZhanMgr.SyncNavalWarCabinShipHp(hp, currHpFull, HpFull)
end

-- 海战同步玩家船血量虚条变化rpc
function Gas2Gac.SyncNavalWarShipVirtualHpChange(totalDeltaHp, endTime, duration)
	LuaHaiZhanMgr.SyncNavalWarShipVirtualHpChange(totalDeltaHp, endTime, duration)
end

-- @param cdInfo = {lastFireTime, cdDuration}
-- in milliseconds
function Gas2Gac.SyncNavalWarCannonCD(seatId, cdInfoUd)
	LuaHaiZhanMgr.SyncNavalWarCannonCD(seatId, cdInfoUd)
end

-- @param materialsUd = {
--		[1] = { [triggerId] = cnt, ... }
--		[2] = boardCnt,
--		[3] = { [skillId] = cnt, ... }
-- }
function Gas2Gac.SyncNavalWarMaterials(materialsUd)
	--print("SyncNavalWarMaterials", materialsUd)
	LuaHaiZhanMgr.SyncNavalWarMaterials(materialsUd)
end

function Gas2Gac.SyncNavalSpecialMonsterHp(engineId, hp, currentHpFull, hpFull)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        obj.Hp,obj.CurrentHpFull,obj.HpFull = hp,currentHpFull,hpFull
	end
	LuaHaiZhanMgr.SyncNavalSpecialMonsterHp(engineId, hp, currentHpFull, hpFull)
end

-- @param seatInfoU = {
--		[place] = { id = playerId, status = ?, gender = ?, class = ?, name = ?, }, ...
-- }
-- EnumNavalPlaySeatStatus = {
--     Sea = 1,
--     Cabin = 2,
--     Leave = 0,
-- }
function Gas2Gac.SyncNavalWarSeatInfo(seatInfoU)
	-- print("SyncNavalWarSeatInfo", seatInfoU)
	LuaHaiZhanMgr.SyncNavalWarSeatInfo(seatInfoU)
end

function Gas2Gac.SyncNavalWarCabinFire(fireNum)
	-- print("SyncNavalWarCabinFire", fireNum)
	LuaHaiZhanMgr.SyncNavalWarCabinFire(fireNum)
end

function Gas2Gac.SyncNavalWarStage(stage)
	-- print("SyncNavalWarStage", stage)
	LuaHaiZhanMgr.SyncNavalWarStage(stage)
end

function Gas2Gac.NavalwarShipDriverLeaveRiding(frameId, poId)
	-- print("NavalwarShipDriverLeaveRiding", frameId, poId)
	LuaHaiZhanMgr.NavalwarShipDriverLeaveRiding(frameId, poId)
end

function Gas2Gac.NavalWarFireSkyShotGun(attackerId, skillId, x, y, delay1, delay2, offsetsUd)
	-- print("NavalWarFireSkyShotGun", attackerId, skillId, x, y, delay1, delay2, offsetsUd)
	LuaHaiZhanMgr.NavalWarFireSkyShotGun(attackerId, skillId, x, y, delay1, delay2, offsetsUd)
end

-- @param infoUd: { status, startTime or nil, duration or nil }
function Gas2Gac.NavalShipStatusChange(shipId, infoUd)
	LuaHaiZhanMgr.NavalShipStatusChange(shipId, infoUd)
end

-- @param infoUd: { [shipId] -> { status, startTime or nil, duration or nil } }
function Gas2Gac.NavalSyncShipStatus(infoUd)
	LuaHaiZhanMgr.NavalSyncShipStatus(infoUd)
end

-- @param goldShipInfoU: { { shipId, name, x, y }, { shipId, name, x, y }, ... }
-- @param huntingShipInfoU: { shipId, name, x, y }
function Gas2Gac.QueryNavalPvpShipPosResult(bossGenTime, goldShipInfoU, huntingShipInfoU, resourceU)
	-- print("QueryNavalPvpShipPosResult")
	LuaHaiZhanMgr.OnQueryNavalPvpShipPosResult(bossGenTime,goldShipInfoU, huntingShipInfoU,resourceU)
end

function Gas2Gac.OpenNavalOnEnterSelectSeatWnd(startTime, duration)
	LuaHaiZhanPosSelectionWnd.s_StartTime = startTime
	LuaHaiZhanPosSelectionWnd.s_Duration = duration
	CUIManager.ShowUI(CLuaUIResources.HaiZhanPosSelectionWnd)
end

-- @param x, y, dir: 船只位置和朝向
-- @param cannonFloor: 第几层炮台
-- @param launchInfoUd: { { delay, id, vx, vy, offset, i }, { delay, id, vx, vy, offset, i }, ... }
--						其中, offset 表示相对于船中心点的偏移量, vx, vy 表示炮弹的速度, i 第几个炮口
function Gas2Gac.NavalWarDoCreateShipCannon(frameId, poId, triggerId, x, y, dir, cannonFloor, launchInfoUd)
	LuaHaiZhanMgr.NavalWarDoCreateShipCannon(frameId, poId, triggerId, x, y, dir, cannonFloor, launchInfoUd)
end

function Gas2Gac.NavalWarShowImpMessage(msgId, argsU)
	g_ScriptEvent:BroadcastInLua("NavalWarShowImpMessage", msgId, g_MessagePack.unpack(argsU))
end

-- 水手打捞成功后给返回消息
-- @param playerId 水手玩家id
-- @param deltaTblUd = { { mType, id, count }, ... }
function Gas2Gac.RequestNavalSailorFishingSuccess(playerId, deltaTblUd)
	LuaHaiZhanMgr.RequestNavalSailorFishingSuccess(playerId, deltaTblUd)
end

function Gas2Gac.SetGuildForeignAidTypeSuccess(targetPlayerId, aidType)
	LuaGuildExternalAidMgr:SetGuildForeignAidTypeSuccess(targetPlayerId, aidType)
end

function Gas2Gac.SendGnjcEvaluateZhiHuiAlert(playerId, playerName, playerClass, playerGender, showTime)
    print("SendGnjcEvaluateZhiHuiAlert", playerId, playerName, playerClass, playerGender, showTime)
	CLuaGuanNingMgr:RegistEvaluateZhiHuiAlert(playerId, playerName, playerClass, playerGender, showTime)
end

function Gas2Gac.SendGnjcDianZanData(playId, serverGroupId, allData, myData)
    print("SendGnjcDianZanData", playId, serverGroupId)
	local praisedCnt = 0
    -- 有点赞数据的玩家
    local allList = allData.Length == 0 and {} or g_MessagePack.unpack(allData)
    for i = 1, #allList, 3 do
        local playerId = allList[i]
        local force = allList[i+1]
        local num = allList[i+2]
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
			praisedCnt = num
		end
        print("=====allList=====", playerId, force, num)
    end
    -- 我的点赞数据
    local myList = myData.Length == 0 and {} or g_MessagePack.unpack(myData)
	--CLuaGuanNingMgr.m_ThumbUpCnt = #myList
    for i = 1, #myList, 1 do
        local playerId = myList[i]
        print("=====myList=====", playerId)
    end    
	g_ScriptEvent:BroadcastInLua("SendGnjcDianZanData", playId, serverGroupId, praisedCnt, myList)
end

function Gas2Gac.SendGnjcZhiHuiSignUpData(dataUD)
    print("SendGnjcZhiHuiSignUpData")
    local list = dataUD.Length == 0 and {} or g_MessagePack.unpack(dataUD)
	g_ScriptEvent:BroadcastInLua("SendGnjcZhiHuiSignUpData", list)
end

function Gas2Gac.SyncGnjcZhiHuiInfo(bOpen, attackZhiHuiPlayerId, defendZhiHuiPlayerId, status)
    print("SyncGnjcZhiHuiInfo", bOpen, attackZhiHuiPlayerId, defendZhiHuiPlayerId, status)
	CLuaGuanNingMgr.m_Status = status
	CLuaGuanNingMgr.m_CommanderOpen = bOpen
	CLuaGuanNingMgr.m_HuXiaoCommanderId = defendZhiHuiPlayerId or 0
	CLuaGuanNingMgr.m_LongYinCommanderId = attackZhiHuiPlayerId or 0
	if CLuaGuanNingMgr.m_Status == EnumGuanNingPlayStatus.eBeiZhan then
		-- 太早了可能会被第一次进关宁的引导给顶掉，稍微延迟一下
		if bOpen then
			RegisterTickOnce(function()
				if CGuanNingMgr.Inst.inGnjc then
					CLuaGuideMgr.TryTriggerGuanNingCommanderSignUpGuide()
				end
			end, 2000)
		end
	elseif CLuaGuanNingMgr.m_Status == EnumGuanNingPlayStatus.eStart then
		CUIManager.CloseUI(CLuaUIResources.GuanNingSignalWnd)
		CLuaGuideMgr.TryTriggerGuanNingCommanderGuide(bOpen and CLuaGuanNingMgr:IsCommander())
	end
	g_ScriptEvent:BroadcastInLua("SyncGnjcZhiHuiInfo", bOpen, attackZhiHuiPlayerId, defendZhiHuiPlayerId, status)
end

function Gas2Gac.SyncGnjcZhiHuiFaZhenProgress(currentProgress, totalProgress)
    --print("SyncGnjcZhiHuiFaZhenProgress", currentProgress, totalProgress)
	g_ScriptEvent:BroadcastInLua("SyncGnjcZhiHuiFaZhenProgress", currentProgress, totalProgress)
end

function Gas2Gac.DianZanGnjcOtherPlayerSuccess(playId, serverGroupId, targetId)
	print("DianZanGnjcOtherPlayerSuccess", playId, serverGroupId, targetId)
	g_ScriptEvent:BroadcastInLua("DianZanGnjcOtherPlayerSuccess", playId, serverGroupId, targetId)
end

function Gas2Gac.CancelDianZanGnjcOtherPlayerSuccess(playId, serverGroupId, targetId)
	print("CancelDianZanGnjcOtherPlayerSuccess", playId, serverGroupId, targetId)
	g_ScriptEvent:BroadcastInLua("CancelDianZanGnjcOtherPlayerSuccess", playId, serverGroupId, targetId)
end

function Gas2Gac.SyncGnjcZhiHuiFaZhenPosition(x1, y1, x2, y2)
	--print("SyncGnjcZhiHuiFaZhenPosition", x1, y1, x2, y2)
	g_ScriptEvent:BroadcastInLua("SyncGnjcZhiHuiFaZhenPosition", x1, y1, x2, y2)
	--wyh 先放这
	EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
end

function Gas2Gac.StartGnjcZhiHuiGuide()
	print("StartGnjcZhiHuiGuide")
end

function Gas2Gac.SyncGnjcTotalScoreInfo(attackScore, defendScore)
	--print("==SyncGnjcTotalScoreInfo==", attackScore, defendScore)
	g_ScriptEvent:BroadcastInLua("SyncGnjcTotalScoreInfo", attackScore, defendScore)
end

function Gas2Gac.SendGnjcRecommendDianZanInfo(playId, servergroupId, data_U)
	print("==SendGnjcRecommendDianZanInfo==")
	local info = {}
	local data = g_MessagePack.unpack(data_U)
	for i=1, #data, 5 do
		info[#info + 1] = {
			playerId = data[i],
			rank = data[i+1],
			name = data[i+2],
			class = data[i+3],
			gender = data[i+4],
		}
	end
	CLuaGuanNingMgr.m_PlayId = playId
    CLuaGuanNingMgr.m_ServerGroupId = servergroupId
	g_ScriptEvent:BroadcastInLua("SendGnjcRecommendDianZanInfo", info)
end

function Gas2Gac.SyncStarBiwuCurrentSeasonChampionInfo(currentSeason, leanderId, teamName, ud)
	local rpcInfo = CStarBiwuChampionTeamInfo_Rpc()
	rpcInfo:LoadFromString(ud)
	CLuaStarBiwuMgr:SyncStarBiwuCurrentSeasonChampionInfo(currentSeason, leanderId, teamName, rpcInfo)
	-- print("SyncStarBiwuCurrentSeasonChampionInfo")
	-- CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
	-- 	print(key, "AttackerZhanduiId", v.MemberId)
	-- 	print(key, "DefenderZhanduiId", v.MemberName)
	-- 	print(key, "AttackerScore", v.MemberClass)
	-- 	print(key, "DefenderScore", v.MemberGender)
	-- end))
end

-- 过山车发车
-- @param playerId		司机的Id
-- @param cabinFId		花车的家具Id
--						非0时是家具Id, (在花车所在的家园内, 与花车交互后发车)
--						为0代表在别人家(在上一个家园发车, 到新的家园后可以继续玩的情况)
-- @param cabinTId		花车的家具模板Id
-- @param stationFId	发车站家具Id
-- @param playerIdsUd	乘客与司机信息 Dictionary { playerId1 -> slotIdx1, playerId2 -> slotIdx2, ... }
function Gas2Gac.RequestRollerCoasterRunSuccess(playerId, cabinFId, cabinTId, stationFId, playerIdsUd)
	LuaHouseRollerCoasterMgr.BeginPlay(playerId, cabinFId, cabinTId, stationFId)
end

-- 对应玩家离开过山车
function Gas2Gac.RunRollerCoasterEnd(playerId)
	LuaHouseRollerCoasterMgr.RunRollerCoasterEnd(playerId)
end

-- 坐着过山车进入其他家园后, 服务器发给司机
-- 司机检查能不能继续发车, 能继续发车
-- @param fTid			花车的家具模板 (不一定有用 先发着)
-- @param playerIdsUd	乘客与司机信息 (不一定有用 先发着)
function Gas2Gac.RequestCheckContinueRollerCoaster(fTid, playerIdsUd)
	print("RequestCheckContinueRollerCoaster", fTid)
end

-- 过山车在本场景运行到站台后自动停下来 fid ~= 0 and bAuto
--		继续发车 走正常发车流程: Gac2Gas_RequestRunRollerCoaster ...
-- 过山车在其他人家运行到站台后自动停下来 fid == 0 and bAuto
--		继续发车用 Gac2Gas_RequestResumeRollerCoaster
-- 过山车运行中, 玩家手动请求停车 not bAuto
--		继续发车用 Gac2Gas_RequestResumeRollerCoaster
-- @param playerId		请求人Id
-- @param driverId		司机Id
-- @param fid			花车的家具Id, 为0代表在别人家
-- @param fTid			花车的家具模板Id
function Gas2Gac.RequestRollerCoasterStopSuccess(playerId, driverId, fid, fTid, bAuto)
	LuaHouseRollerCoasterMgr.OnStop(playerId, driverId, fid, fTid, bAuto)
end

-- 过山车运行中, 玩家手动请求停车后, 司机继续发车
-- @param playerId		司机Id
-- @param fid			花车的家具Id, 为0代表在别人家
-- @param fTid			花车的家具模板Id
function Gas2Gac.RequestRollerCoasterResumeSuccess(playerId, fid, fTid)
	LuaHouseRollerCoasterMgr.OnResume(playerId, fid, fTid)
end

-- 过山车运行中, 请求加速
-- @param playerId		请求人Id
-- @param driverId		司机Id
-- @param fid			花车的家具Id, 为0代表在别人家
-- @param fTid			花车的家具模板Id
function Gas2Gac.RequestRollerCoasterSpeedUpSuccess(playerId, driverId, fid, fTid)
	g_ScriptEvent:BroadcastInLua("HouseRollerCoasterAccelerate",fid)
end

-- 过山车运行过程中同步进度
-- @param driverId		司机Id
-- @param fid			花车的家具Id, 为0代表在别人家
function Gas2Gac.SyncRollerCoasterProgress(driverId, fid, pathIndex, segmentPercent, speed)
	LuaHouseRollerCoasterMgr.SyncRollerCoasterProgress(driverId, fid, pathIndex, segmentPercent, speed)
end

-- 进入场景同步过山车进度
-- @param infoUd		List:
-- { driverId, fId or 0, fTid, stationFId, isStop, pathIndex, segmentPercent, speed, ...... }
-- 其中 fId为0 是在其他家园, 取不到家具
-- isStop 为 1 代表当前是停车状态, 为 0 为运行状态
-- 多个过山车在跑就有多组
function Gas2Gac.BatchSyncRollerCoasterProgress(infoUd)
	LuaHouseRollerCoasterMgr.BatchSyncRollerCoasterProgress(infoUd)
end

function IsOpenGhostScene()
	return false
end


--TODO: optimize
BSId2EngineId = {}
CurrentTeleportNewEngineIds = {}

function Gas2Gac.UpdateGhostObjectMap(bsId, engineId)
	BSId2EngineId[bsId] = engineId
end

EnableGhostPrint = false

function GhostPrint(...)
	if not EnableGhostPrint then return end
	print(...)
end


CEngineObjectMgr = import "L10.Engine.CEngineObjectMgr"
CDirectorHandler = import "L10.Game.CDirectorHandler"

function GhostTeleportReplaceEngineId(bsId, engineId)
	if not IsOpenGhostScene() then return end

	CurrentTeleportNewEngineIds[engineId] = true

	local oldEngineId = BSId2EngineId[bsId]
	if not oldEngineId then 
		BSId2EngineId[bsId] = engineId
		return false 
	end

	local obj = CClientObjectMgr.Inst:GetObject(oldEngineId)

	local newObj = CClientObjectMgr.Inst:GetObject(engineId)
	BSId2EngineId[bsId] = engineId
	if oldEngineId == engineId then return false end
	if obj and newObj then
		obj:SwapEngineObject(newObj)
		--Time = import "UnityEngine.Time"
		--print(Time.realtimeSinceStartup, "CopyPosition:", newObj.EngineObject.Pos, obj.EngineObject.Pos)
		obj.EngineObject:SyncMoveInfoFromAnotherObj(newObj.EngineObject)
		--print(Time.realtimeSinceStartup, "AfterCopyPosition:", newObj.EngineObject.Pos, obj.EngineObject.Pos)
		--local obj1 = CClientObjectMgr.Inst:GetObject(engineId) 
		--local obj2 = CClientObjectMgr.Inst:GetObject(oldEngineId)
		local oldEngine = CEngineObjectMgr.Inst:GetObject(oldEngineId)
		CoreScene.MainCoreScene:RemoveObjFrGrid(oldEngine)
		CoreScene.MainCoreScene.Handler:OnDestroyObject(oldEngine)
		oldEngine:Destroy()
		if TypeIs(obj, typeof(CClientOtherPlayer)) then
			CClientPlayerMgr.Inst:AddPlayer(obj)
		end
		return true
	end
	return false
end

function Gas2Gac.GhostTeleportReplaceEngineId(bsId, engineId)
	--GhostTeleportReplaceEngineId(bsId, engineId)
end

function Gas2Gac.GhostPlayerBeginEnterScene(sceneId, idsUD)
	CScene.MainScene.SceneId = sceneId

	local ids = idsUD and MsgPackImpl.unpack(idsUD)
	if not ids then return end
	local idMap = {}
	--local idstr = ""
	for i = 0, ids.Count-1 do
		idMap[ids[i]] = true
		local engineObj = CEngineObjectMgr.Inst:GetObject(ids[i])
		if engineObj then
			CoreScene.MainCoreScene:RemoveObjFrGrid(engineObj)
			CoreScene.MainCoreScene.Handler:OnDestroyObject(engineObj)
			engineObj:Destroy()
			--idstr = idstr..","..tostring(id)
		end
	end
	--print("DelObjBeforeEnterScene:", idstr)
end

local NewObjCount = 0

function Gas2Gac.GhostTeleportReplaceMainPlayerEngineId(engineId)
	if not IsOpenGhostScene() then return end

	CurrentTeleportNewEngineIds = {}
	CurrentTeleportNewEngineIds[engineId] = true
	NewObjCount = 0

	local engineObj = CEngineObjectMgr.Inst:GetObject(engineId)
	--print("Replace MainPlayer:", engineId, engineObj, CClientMainPlayer.Inst)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst:ReplaceEngineObject(engineObj)
	end
end

local CoreScene = import "L10.Engine.Scene.CoreScene"
function Gas2Gac.GhostPlayerTeleportEnd()
	if not IsOpenGhostScene() then return end

	if not next(CurrentTeleportNewEngineIds) then return end

	local replaceObjCount = 0
	--local delObjs = {}
	--CommonDefs.DictIterate(CClientObjectMgr.Inst.m_Id2Object, DelegateFactory.Action_object_object(function (key, value)
	--	local id = tonumber(key)
	--	if not CurrentTeleportNewEngineIds[id] then
	--		delObjs[value] = 1
	--	else
	--		replaceObjCount = replaceObjCount + 1
	--	end
	--end))
	local delobjCount = 0
	--for obj, _ in pairs(delObjs) do
	--	obj:Destroy()
	--	delobjCount = delobjCount + 1
	--end

	local replaceEngineCount = 0
	local delEngines = {}
	CommonDefs.DictIterate(CEngineObjectMgr.Inst.m_Objects, DelegateFactory.Action_object_object(function (key, value)
		local id = tonumber(key)
		if not CurrentTeleportNewEngineIds[id] then
			delEngines[value] = id
		else
			replaceEngineCount = replaceEngineCount + 1
		end
	end))
	local delEngineCount = 0
	--local idstr = ""
	for obj, id in pairs(delEngines) do
		CoreScene.MainCoreScene:RemoveObjFrGrid(obj)
		CoreScene.MainCoreScene.Handler:OnDestroyObject(obj)
		obj:Destroy()
		delEngineCount = delEngineCount + 1
		--idstr = idstr..","..tostring(id)
	end

	--print("DestroyObj:", idstr)

	print("TeleportSummary:", NewObjCount, replaceObjCount, replaceEngineCount, delobjCount, delEngineCount)
	CurrentTeleportNewEngineIds = {}
end


function Gas2Gac.GhostTeleportShowFakePlayerToPlayer(bsId, engineId, name, gender, objClass, state, dir, appearanceData, level, titileId, titleName, statusData)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowFakePlayerToPlayer(engineId, name, gender, objClass, state, dir, appearanceData, level, titileId, titleName, statusData)
end

function Gas2Gac.GhostTeleportShowFlagToPlayer(bsId,engineId,templateId,actionState,ownerEngineId,statusData,dir, pixelPosX, pixelPosY, pixelPosZ, maxLifeTime)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	local obj = CClientObjectMgr.Inst:GetObject(oldEngineId)
	if obj and TypeIs(obj, typeof(CClientFlag)) then
		L10.Game.Gas2Gac.m_Instance:ShowFlagToPlayer(engineId,templateId,actionState,ownerEngineId,statusData,dir)
	else
		LuaSimpleFlagMgr:CreateLuaFlag(engineId, templateId, ownerEngineId, pixelPosX, pixelPosY, pixelPosZ, maxLifeTime)
	end
end

function Gas2Gac.GhostTeleportShowBulletToPlayer(bsId,engineId,templateId,actionState,ownerEngineId,statusData)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowBulletToPlayer(engineId,templateId,actionState,ownerEngineId,statusData)
end
function Gas2Gac.GhostTeleportShowItemToPlayer(bsId,engineId,templateId,isDropping,dropObjectEngineId,equipColor,wordCount,count,straightDrop,originalHeight,isFake,displayColor,holeNum,isNiTian,isPrecious,pickAble,specialDisplayName,bCannotAutoPick)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowItemToPlayer(engineId,templateId,isDropping,dropObjectEngineId,equipColor,wordCount,count,straightDrop,originalHeight,isFake,displayColor,holeNum,isNiTian,isPrecious,pickAble,specialDisplayName,bCannotAutoPick)
end
function Gas2Gac.GhostTeleportShowLingShouToPlayer(bsId,engineId,templateId,id,actionState,dir,name,ownerEngineId,hp,currentHpFull,hpFull,pixiangTemplateId,quality,evolveGrade,pixiangEvolveGrade,statusData,resId,marryInfo,babyInfo,extraBabyTempId1,extraBabyTempId2,extraBabyTempId3)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowLingShouToPlayer(engineId,templateId,id,actionState,dir,name,ownerEngineId,hp,currentHpFull,hpFull,pixiangTemplateId,quality,evolveGrade,pixiangEvolveGrade,statusData,resId,marryInfo,babyInfo,extraBabyTempId1,extraBabyTempId2,extraBabyTempId3)
end
function Gas2Gac.GhostTeleportShowMonsterToPlayer(bsId,engineId,templateId,actionState,dir,appearanceData,resourceId,level,benefitCharacterId,ownerPlayerId,name,statusData,isFake,fakeAppearData,extraData,specialTitleId,specialTitleStr,zuoqiTemplateId,driverId,passengerIdList)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowMonsterToPlayer(engineId,templateId,actionState,dir,appearanceData,resourceId,level,benefitCharacterId,ownerPlayerId,name,statusData,isFake,fakeAppearData,extraData,specialTitleId,specialTitleStr,zuoqiTemplateId,driverId,passengerIdList)
end
function Gas2Gac.GhostTeleportShowNpcToPlayer(bsId,engineId,templateId,actionState,dir,name,title,headFx,fxId,ownerPlayerId,isFake,fakeAppearData,statusData,portraitName,zuoQiTemplateId,passengerIdList,EmbracePlayerId,level,driverId)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowNpcToPlayer(engineId,templateId,actionState,dir,name,title,headFx,fxId,ownerPlayerId,isFake,fakeAppearData,statusData,portraitName,zuoQiTemplateId,passengerIdList,EmbracePlayerId,level,driverId)
end
function Gas2Gac.GhostTeleportShowPetToPlayer(bsId,engineId,templateId,actionState,dir,ownerEngineId,ownerIsFemale,hp,currentHpFull,hpFull,ownerName,isShadow,appearProp,Class,Gender,titleId,titleName,statusData)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowPetToPlayer(engineId,templateId,actionState,dir,ownerEngineId,ownerIsFemale,hp,currentHpFull,hpFull,ownerName,isShadow,appearProp,Class,Gender,titleId,titleName,statusData)
end
function Gas2Gac.GhostTeleportShowOtherPlayerToPlayer(bsId,engineId,name,gender,objClass,actionState,dir,appearanceData,level,titileId,titleName,statusData,serverGroupId,hasFeiSheng)
	EnableGhostPrint = true
	print(bsId,engineId,name,gender,objClass,actionState)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then 
		EnableGhostPrint = false
		return 
	end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowOtherPlayerToPlayer(engineId,name,gender,objClass,actionState,dir,appearanceData,level,titileId,titleName,statusData,serverGroupId,hasFeiSheng)
	EnableGhostPrint = false
end
function Gas2Gac.GhostTeleportShowPickToPlayer(bsId,engineId,templateId,dir,actionState,name,resourceId)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowPickToPlayer(engineId,templateId,dir,actionState,name,resourceId)
end
function Gas2Gac.GhostTeleportShowTempleToPlayer(bsId,engineId,templateId,dir,actionState)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowTempleToPlayer(engineId,templateId,dir,actionState)
end
function Gas2Gac.GhostTeleportShowPlatformToPlayer(bsId,engineId,templateId,actionState,dir,name)
	CurrentTeleportNewEngineIds[engineId] = true
	if bsId~=-1 and GhostTeleportReplaceEngineId(bsId, engineId) then return end
	NewObjCount = NewObjCount + 1
	L10.Game.Gas2Gac.m_Instance:ShowPlatformToPlayer(engineId,templateId,actionState,dir,name)
end

function Gas2Gac.GhostDoTeleport(pipeId, playerId, ip, port)
end

function Gas2Gac.StopReadGacNetwork(...)
end

--@region 中元节相关RPC

-- 同步所有玩家所扮演的角色
-- role: 1 ~ 10
-- @param dataU			List: {playerId, role, playerId, role, ...}
function Gas2Gac.UpdatePlayerGSNZRoleInfo(dataU)
	ZhongYuanJie2022Mgr.UpdatePlayerGSNZRoleInfo(dataU)
end

-- @param value			int, 同步当前古树的进度
function Gas2Gac.SyncGSNZPlayInfo(value)
	ZhongYuanJie2022Mgr.SyncGSNZPlayInfo(value)
end

-- @param winForce		胜利方势力 EnumCommonForce.eDefend / eAttack
-- @param force			玩家的势力
-- @param rewardTime	今日已经领取奖励次数
-- @param playerInfosUd	玩家信息 List: { id, name, class, role(扮演角色), killnum, picknum }
function Gas2Gac.ShowGSNZResultWnd(winForce, force, rewardTime, playerInfosUd)
	ZhongYuanJie2022Mgr.ShowGSNZResultWnd(winForce, force, rewardTime, playerInfosUd)
end

-- @param playerId		操作玩家Id
-- @param furnitureId	古筝家具Id
-- @param dataUd		Gac2Gas_SyncGuZhengKeyStrokes 同步过来的数据原封不动转发
function Gas2Gac.SyncGuZhengKeyStrokes(playerId, furnitureId, dataUd)
	ZhongYuanJie2022Mgr.PlayYaoZheng(playerId,furnitureId,dataUd)
end

-- @param id			乐谱id
function Gas2Gac.GuZhengSheetUnlockSuccess(id)
	ZhongYuanJie2022Mgr.GuZhengSheetUnlockSuccess(id)
end

-- @param playerId		操作玩家Id
-- @param fId			古筝家具实例Id
-- @param sheetId		乐谱id
-- @param startTime		演奏开始的服务器时间,
--						为0代表离开视野或者玩家离开琴导致演奏提前结束
function Gas2Gac.SyncGuZhengPlaySheet(playerId, fid, sheetId, startTime)
	ZhongYuanJie2022Mgr.ProcessGuZhengPlayDoc(playerId, fid, sheetId, startTime)
end

--@endregion

function Gas2Gac.SyncPhysicsMove(playerId, ud)
	LuaHaiZhanMgr.SyncNavalMove(playerId, ud)
end

function Gas2Gac.SyncPhysicsServerTime(serverTime, clientTime)
	LuaHaiZhanMgr.SyncPhysicsServerTime(serverTime,clientTime)
end

function Gas2Gac.SyncPhysicsStartTime(startTime, currTime)
	LuaHaiZhanMgr.SyncPhysicsStartTime(startTime, currTime)
end

-- physicsObjId: >0 playerId; <0 engineId
function Gas2Gac.CreatePhysicsObject(frameId, physicsObjId, controllerType, extraUd)
	-- print("CreatePhysicsObject", frameId, physicsObjId, controllerType)
	LuaHaiZhanMgr.CreatePhysicsObject(frameId,physicsObjId, controllerType, extraUd)
end

function Gas2Gac.OnHitByPhysicsTrigger(frameId,objId, triggerId, templateId)
	-- print("OnHitByPhysicsTrigger", frameId,objId, triggerId, templateId)
	LuaHaiZhanMgr.OnHitByPhysicsTrigger(frameId,objId, triggerId, templateId)
end

-- lifeCycle, x, y, dir, ownerid, vx, vy, extraSync or nil
-- templateId == 1: extraSync = cannonPos
function Gas2Gac.CreatePhysicsTrigger(frameId, id, templateId, dataUd)
	-- print("CreatePhysicsTrigger", frameId, id, templateId)
	LuaHaiZhanMgr.CreatePhysicsTrigger(frameId, id, templateId, dataUd)
end

function Gas2Gac.DestroyPhysicsObjectOrTrigger(frameId, isObject, pId)
	-- print("DestroyPhysicsObjectOrTrigger", frameId, isObject, pId)
end

-- templateId 为策划填写 用于标明这次缠绕表现的Id,具体哪个表待商量, 目前填特效ID
-- countDownInfo { 起始时间(帧), 持续时间(帧), }
function Gas2Gac.FreezePhysicsObject(frameId, id, templateId, countDownInfo)
	-- print("FreezePhysicsObject", frameId, id, templateId)
	LuaHaiZhanMgr.FreezePhysicsObject(frameId, id, templateId,countDownInfo)
end

function Gas2Gac.UnFreezePhysicsObject(frameId, id)
	-- print("UnFreezePhysicsObject", frameId, id)
	LuaHaiZhanMgr.UnFreezePhysicsObject(frameId, id)
end

function Gas2Gac.AddPhysicsEffector(frameId, id, triggerId, dataU)
	-- print("AddPhysicsEffector", frameId, id, triggerId,dataU)
	LuaHaiZhanMgr.AddPhysicsEffector(frameId, id, triggerId,dataU)
end

function Gas2Gac.RmPhysicsEffector(frameId, id, triggerId)
	-- print("RmPhysicsEffector", frameId, id, triggerId)
	LuaHaiZhanMgr.RmPhysicsEffector(frameId, id, triggerId)
end

function Gas2Gac.ApplyPhysicsAISteering(frameId, id, steeringInfo)
	-- print("ApplyPhysicsAISteering", frameId, id)
	LuaHaiZhanMgr.ApplyPhysicsAISteering(frameId, id, steeringInfo)
end

function Gas2Gac.SyncPhysicsSnapshot(frameId, dataUd)
	--重新对时
	local localFrame = CPhysicsTimeMgr.Inst:GetGuessServerFrameId()
	if localFrame>frameId+40 then
		CPhysicsTimeMgr.Inst.minRtt = 0
	end

	local info = g_MessagePack.unpack(dataUd)
	LuaHaiZhanMgr.SyncPhysicsSnapshot(frameId,info)
end

function Gas2Gac.SyncPhysicsSelfSnapshot(frameId, dataUd)
	local info = g_MessagePack.unpack(dataUd)
	LuaHaiZhanMgr.SyncPhysicsSnapshot(frameId,{info})
end

-- { { poId, { cmd1, cmd2, ... } }, ... }
function Gas2Gac.SyncPhysicsCmdQSnapshot(frameId, dataUd)
	local info = g_MessagePack.unpack(dataUd)
	LuaHaiZhanMgr.SyncPhysicsCmdQSnapshot(frameId,info)
end

function Gas2Gac.TeleportPhysicsObjectInScene(frameId, poId, x, y, dir)
	LuaHaiZhanMgr.TeleportPhysicsObjectInScene(frameId, poId, x, y, dir)
end

function Gas2Gac.ApplyPhysicsWorldForce(frameId, poId, x, y)
	LuaHaiZhanMgr.ApplyPhysicsWorldForce(frameId, poId, x, y)
end

function Gas2Gac.UpdatePhysicsMulSpeed(frameId, poId, mulSpeed)
	LuaHaiZhanMgr.UpdatePhysicsMulSpeed(frameId, poId, mulSpeed)
end

function Gas2Gac.SyncPhysicsCmdCD(cdUd)
	print("SyncPhysicsCmdCD")
	LuaHaiZhanMgr.SyncPhysicsCmdCD(cdUd)
end

function Gas2Gac.SendSkillEventToBuffFx(engineId, mainSkillId, buffId, eventId, targetPixelX, targetPixelY, extraUd)
	-- print("SendSkillEventToBuffFx", engineId, mainSkillId, buffId, eventId, targetPixelX, targetPixelY)
	local co = CClientObjectMgr.Inst:GetObject(engineId)
	if not co then return end

	local extra = g_MessagePack.unpack(extraUd)
	local time = extra.Time or 1
	local delay = extra.DelayTime or 0

	local buffData = Buff_Buff.GetData(buffId)
	local fxList = buffData.FX
	if fxList then
		for i = 1, fxList.Length do
			-- local fxKey = math.floor(buffId/10)+i-1
			-- local fx0 =co.RO:GetFx(fxKey)
			local fx = co.RO:GetFxById(fxList[i-1])
			if fx then
				if TypeIs(fx.m_FX, typeof(CDynamicWorldPositionFX)) then
					local x,z = targetPixelX/64,targetPixelY/64
					local height = CRenderScene.Inst:SampleLogicHeight(x,z)
					-- kickout的时候先将船绑定到触手上。
					-- 技能实现看起来有问题。
					-- 这个特效跟buff关联，但是有可能之前的buff没有移除，但是逻辑上又调用了抛船。
					-- 这个时候特效没有机会初始化位置
					fx.m_FX.m_IsFollowStatus = true;
					fx.m_FX:TryAttach();
					fx.m_FX:SetKickOut(Vector3(x,height,z),EKickOutType.Parabola,time,delay,-8,270,2)
				end
			end
		end
	end
end

function Gas2Gac.MarkNewShinningHouseFishItem(itemTempId)
	--print("MarkNewShinningHouseFishItem",itemTempId)
	LuaSeaFishingMgr:OnRefreshShinning(itemTempId,true)
	g_ScriptEvent:BroadcastInLua("MarkNewShinningHouseFishItem",itemTempId)
end

function Gas2Gac.RemoveShinningHouseFishItem(itemTempId)
	--print("remove shinning!",itemTempId)
	LuaSeaFishingMgr:OnRefreshShinning(itemTempId,false)
	g_ScriptEvent:BroadcastInLua("RemoveShinningHouseFishItem",itemTempId)
end

function Gas2Gac.GuildJuDianReplyDailySceneOverview(monsterNpcDataUD, otherDataUD)
	print("GuildJuDianReplyDailySceneOverview")
	local data = g_MessagePack.unpack(monsterNpcDataUD)

	LuaJuDianBattleMgr:SyncDailySceneData(data)
end

function Gas2Gac.RequestFixBeginnerGuideDay2FinishSuccess(day)
	LuaQianYingChuWenMgr:RequestFixBeginnerGuideDay2FinishSuccess(day)
end

function Gas2Gac.GuildJuDianReplyRankInfo(centerRankData, localRankData)
	print("GuildJuDianReplyRankInfo")
	local centerData = g_MessagePack.unpack(centerRankData)
	local localData = g_MessagePack.unpack(localRankData)
	LuaJuDianBattleMgr:OpenRankWnd(centerData, localData)
end

function Gas2Gac.GuildJuDianSendCanOpenContributionWnd(result)
	print("===GuildJuDianSendCanOpenContributionWnd===", result)
	LuaJuDianBattleMgr.CanOpenContributionWnd = result
end

function Gas2Gac.GuildJuDianSendContributionWeekData(dataUD)
	print("GuildJuDianSendContributionWeekData")
	-- list, 9个数据为一组，分别是  playerId playerName, class, level, xianfanStatus, 击杀数，提交数，棋局时间，战斗评价
	local data = g_MessagePack.unpack(dataUD)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSendContributionWeekData", data)
end

function Gas2Gac.GuildJuDianSendContributionSeasonData(dataUD)
	print("GuildJuDianSendContributionSeasonData")

	-- list, 9个数据为一组，分别是  playerId playerName, class, level, xianfanStatus, 击杀数，提交数，棋局时间，战斗评价
	local data = g_MessagePack.unpack(dataUD)
	g_ScriptEvent:BroadcastInLua("GuildJuDianSendContributionSeasonData", data)
end

function Gas2Gac.GuildJuDianShowAwardWnd(season, week, guildRank, guildName, guildScore, totoalKillCount, eval, guildContri, mingwang, exp, freeSilver)
	print("==GuildJuDianShowAwardWnd=")
	print(season, week, guildRank, guildName)
	print(guildScore, totoalKillCount, eval)
	print(guildContri, mingwang, exp, freeSilver)
	local data = {}
	data.season = season
	data.week = week
	data.guildRank = guildRank
	data.guildName = guildName
	data.guildScore = guildScore
	data.totoalKillCount = totoalKillCount
	data.eval = eval
	data.guildContri = guildContri
	data.mingwang = mingwang
	data.exp = exp
	data.freeSilver = freeSilver

	LuaJuDianBattleMgr.ResultData = data
	if LuaJuDianBattleMgr:IsInGameplay() or LuaJuDianBattleMgr:IsInJuDian() then 
		CUIManager.ShowUI(CLuaUIResources.JuDianBattleResultWnd)
	end
end

function Gas2Gac.GuildJuDianOpenGuideMapWnd()
	print("GuildJuDianOpenGuideMapWnd")
	LuaJuDianBattleMgr:OpenWorldMap()
end

function Gas2Gac.SendJinLuHunYuanZhanChoosableSkillInfo(data_U)
	--print("SendJinLuHunYuanZhanChoosableSkillInfo")
	local data = g_MessagePack.unpack(data_U)
	LuaGuoQingPvPMgr.ChoosableSkillInfoData = data
	CUIManager.ShowUI(CLuaUIResources.GuoQingPvPSkillSelectWnd)
	--for profession, v in pairs(data) do
		-- v 是一个列表，有7个skillId，前5个普通，后2个绝技
	--end
end

function Gas2Gac.SyncJinLuHunYuanZhanTeamChooseResult(data_U)
	--print("SyncJinLuHunYuanZhanTeamChooseResult")
	local data = g_MessagePack.unpack(data_U)
	LuaGuoQingPvPMgr.TeamSkillInfoData = data
	g_ScriptEvent:BroadcastInLua("OnJinLuHunYuanZhanTeamChooseResult")
	-- 12个数组为1组
	-- playerId, name, class, gender, force
	-- 7个skillId， 注意有的可能为0
end

function Gas2Gac.SyncJinLuHunYuanZhanAllFinalSkillInfo(reason, data_U)
	--print("SyncJinLuHunYuanZhanAllFinalSkillInfo")
	-- reason是一个字符串，表示为什么下发这个rpc
	-- 为startplay时， 说明是开打后，服务器主动推送的
	-- 为clientrequest时，说明是玩家主动请求的
		-- 12个数组为1组
		-- playerId, name, class, gender, force
		-- 7个skillId， 注意有的可能为0
	local data = g_MessagePack.unpack(data_U)
	LuaGuoQingPvPMgr:InitAllSkillData(reason,data)
	-- 结构跟上面那个rpc是一样的，只是包含了场景内所有玩家的数据
end

function Gas2Gac.SendJinLuHunYuanZhanPlayResult(winForce, allSkillInfo_U, evaluateInfo_U)
	-- allSkillInfo_U 这个跟SyncJinLuHunYuanZhanAllFinalSkillInfo中的数据是一致的
	local SkillInfoData = g_MessagePack.unpack(allSkillInfo_U)
	local data = g_MessagePack.unpack(evaluateInfo_U)
	print("SendJinLuHunYuanZhanPlayResult")
	LuaGuoQingPvPMgr:InitPlayResult(winForce,SkillInfoData,data)
	

end


function Gas2Gac.SendXiangYaoYuQiuSiRank(selfFinishTime, data_U)
	--print("==SendXiangYaoYuQiuSiRank==", selfFinishTime)
	local data = g_MessagePack.unpack(data_U)

	LuaGuoQingPvEMgr:InitRankData(selfFinishTime,data)
end

function Gas2Gac.GuildJuDianOpenLingqiSubmitWnd(count)
	LuaJuDianBattleMgr:OpenLingqiSubmitWnd(count)
end

function Gas2Gac.Halloween2022SyncAllowToUseHongBaoCover(usedJade, isAllowToUseCover)
	LuaHalloween2022Mgr.Halloween2022SyncAllowToUseHongBaoCover(usedJade, isAllowToUseCover)
end

function Gas2Gac.PlayPumpkinHeadFire(engineId, fireFxId, duration)
	LuaHalloween2022Mgr.PlayPumpkinHeadFire(engineId, fireFxId,duration)
end

function Gas2Gac.XiangYaoYuQiuSiPlaySuccess(costTime, times, mode)
	LuaGuoQingPvEMgr.times = times;
	LuaGuoQingPvEMgr.costTime = costTime
	LuaGuoQingPvEMgr.mode = mode
	CUIManager.ShowUI(CLuaUIResources.GuoqingPvEResultWnd)
	print("==XiangYaoYuQiuSiPlaySuccess==", costTime, times, mode)
end

function Gas2Gac.ReplyJinLuHunYuanZhanHotSkill(hotSkillData_U)
	local data = g_MessagePack.unpack(hotSkillData_U)

	g_ScriptEvent:BroadcastInLua("ReplyJinLuHunYuanZhanHotSkill",data)
	
end

function Gas2Gac.EnterJinLuHunYuanZhanDieStatus(playerId, engineId)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if not obj then return end

	if TypeIs(obj, typeof(CClientOtherPlayer)) then
		CPUBGMgr.Inst:OnEnterChiJiPreDie(obj)
	end
end

function Gas2Gac.ReplyJinLuHunYuanZhanOpenStatus(status)
	g_ScriptEvent:BroadcastInLua("ReplyJinLuHunYuanZhanOpenStatus",status)
	print("ReplyJinLuHunYuanZhanOpenStatus,,", status)
end

function Gas2Gac.SyncJinLuHunYuanZhanKillInfo(attackData_U, defendData_U)
	print("SyncJinLuHunYuanZhanKillInfo")
	local attackdata = g_MessagePack.unpack(attackData_U)
	local defenddata = g_MessagePack.unpack(defendData_U) 
	LuaGuoQingPvPMgr.attackdata = attackdata
	LuaGuoQingPvPMgr.defenddata = defenddata
	CUIManager.ShowUI(CLuaUIResources.GuoQingPvPPlayingWnd)

	--g_ScriptEvent:BroadcastInLua("SyncJinLuHunYuanZhanKillInfo",attackdata,defenddata)
	--for i=1, #data, 4 do
	--	print("name ", data[i+1])
	--	print("class ", data[i+2])
	--	print("kill ", data[i+3])
	--end
end

function Gas2Gac.GuildCustomBuildPreTaskSyncStatus(status)
	LuaGuildCustomBuildMgr.OnSyncPreTaskStatus(status)
end

function Gas2Gac.GuildCustomBuildPreTaskSyncPlayInfo(playInfoUd)
	--print("GuildCustomBuildPreTaskSyncPlayInfo",playInfoUd)
	LuaGuildCustomBuildMgr.OnGuildCustomBuildPreTaskSyncPlayInfo(playInfoUd)
end

function Gas2Gac.SendGnjcCounterattackNpcInfo(index, force, gridX, gridY)
	print("SendGnjcCounterattackNpcInfo",index, force, gridX, gridY)
	g_ScriptEvent:BroadcastInLua("SendGnjcCounterattackNpcInfo", index, force, gridX, gridY)
end

function Gas2Gac.CanPostShouTuMessage(isWaiMen)
	LuaShiTuMgr:CanPostShouTuMessage(isWaiMen)
end

function Gas2Gac.PlayerCanSearchShiFu(isWaiMen)
	LuaShiTuMgr:PlayerCanSearchShiFu(isWaiMen)
end

function Gas2Gac.UpdateExpressionExpireInfo(group, expiredTime)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
		print("======UpdateExpressionExpireInfo======", group, expiredTime)
		CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.PlayProp.ExpressionExpireInfo, group, expiredTime)
	end
end

function Gas2Gac.SyncNpcCountDownProgress(engineId, bDisplay, leftTime, totalTime)
	print("SyncNpcCountDownProgress",engineId, bDisplay, leftTime, totalTime)
	g_ScriptEvent:BroadcastInLua("SyncNpcCountDownProgress", engineId, bDisplay, leftTime, totalTime)
end

function Gas2Gac.TriggerSectGetYaoguaiGuide()
	CGuideMgr.Inst:StartGuide(EnumGuideKey.ZhuoYaoHuaFuResult, 0)
end

function Gas2Gac.TriggerSectPutYaoguaiToFazhenGuide()
end

-- Gac2Gas.RequestYaoGuaiRedAlert 查询红点
function Gas2Gac.SyncYaoGuaiRedAlert(bShow)
	g_ScriptEvent:BroadcastInLua("SyncYaoGuaiRedAlert",bShow)
end

function Gas2Gac.SceneBrightnessTransition(duration, brightness)
	if CWeatherMgr.Inst then
		CWeatherMgr.Inst:CustomSetDarkness(duration, brightness)
	end
end

-- 采集物变身
function Gas2Gac.SyncPickResourceIdChange(engineId, resourceId)
	local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
	if clientObj and TypeIs(clientObj, typeof(CClientPick)) then
		local pick = TypeAs(clientObj, typeof(CClientPick))
		if pick.RO then
			pick.RO:RemoveFX("SelfFx")
		end
		pick.ResourceId = resourceId
		pick:LoadResource()
	end
end

function Gas2Gac.SyncXiuxingBoomBornPostion(x, y)
	LuaZongMenMgr:SyncXiuxingBoomBornPostion(x, y)
end

function Gas2Gac.RequestSetSectUseEnemyForTaskResult(sectId, bUse)
end

function Gas2Gac.OpenRenewZuoQiByItemWnd(place, pos, itemId, dataUD, days)
	LuaVehicleRenewalMgr:OpenRenewZuoQiByItemWnd(place, pos, itemId, dataUD, days)
end

function Gas2Gac.SyncSectXiuxingReliveTime(nextTime)
	LuaZongMenMgr:SyncSectXiuxingReliveTime(nextTime)
end

function Gas2Gac.SendGuildJuDianZhanLongShopInfo(hasScore, hasPermission, shopInfoU)
	local shopInfo = g_MessagePack.unpack(shopInfoU)
	LuaJuDianBattleMgr:SendGuildJuDianZhanLongShopInfo(hasScore, hasPermission, shopInfo)
end

--#region 队伍招募
function Gas2Gac.QueryTeamRecruitResult(recruitListUD, currentPageNum, pageNum)
    LuaTeamRecruitMgr:QueryTeamRecruitResult(recruitListUD, currentPageNum, pageNum)
end

function Gas2Gac.OpenTeamRecruitWnd()
    LuaTeamRecruitMgr:OpenTeamRecruitWnd()
end

function Gas2Gac.QueryOwnTeamRecruitResult(recruitListUD)
    LuaTeamRecruitMgr:QueryOwnTeamRecruitResult(recruitListUD)
end

function Gas2Gac.QueryTeamRecruitByIdAndTimeResult(recruitListUD)
	LuaTeamRecruitMgr:QueryTeamRecruitByIdAndTimeResult(recruitListUD)
end

function Gas2Gac.TeamRecruitAddSuccess(recruitListUD)
	LuaTeamRecruitMgr:TeamRecruitAddSuccess(recruitListUD)
end

function Gas2Gac.TeamRecruitCancelSuccess()
	LuaTeamRecruitMgr:TeamRecruitCancelSuccess()
end
--#endregion 队伍招募

function Gas2Gac.NanDuLordAcceptScheduleSuccess(period, scheduleId, scheduleType)
	LuaYiRenSiMgr:NanDuLordAcceptScheduleSuccess(period, scheduleId, scheduleType)
end

function Gas2Gac.NanDuLordSelectScheduleChoiceSuccess(period, scheduleId, question, choice)
	g_ScriptEvent:BroadcastInLua("NanDuLordSelectScheduleChoiceSuccess")
end

function Gas2Gac.NanDuLordOpenWnd(wndType)
	LuaYiRenSiMgr:NanDuLordOpenWnd(wndType)	
end

function Gas2Gac.NanDuLordFinishScheduleEvent(period)
	print("NanDuLordFinishScheduleEvent", period)
end

-- @param details = [deltaMoney, deltaSubMoney, fameId, deltaFame, MEFame, MEDelta,
--					 period, scheduleId, choice, attr1, deltaAttr1, attr2, deltaAttr2, ...]
-- 只有选择选项类型的日程中choice才非0, 如果只是显示用应该也用不到
-- MEFame, MEDelta 是指互斥的声望变化 MEDelta为正数, 表示互斥声望的扣除
function Gas2Gac.NanDuLordShowAttrChange(details)
	LuaYiRenSiMgr:NanDuLordShowAttrChange(details)
end

-- 需要在这里弹消息或者别的手段显示属性变更
-- 因为参数比较多并且都是读表固定死的, 客户端直接弹比较好
-- 如 成功解锁词条<%s>, %s势力声望提升%s, 金钱提升%s
-- @param designId	NanDuFanHua_JuBaoPen表id
function Gas2Gac.UnlockNanDuJuBaoPenItem(designId)
	LuaYiRenSiMgr:UnlockNanDuJuBaoPenItem(designId)
end

function Gas2Gac.GetNanDuJuBaoPenRewardSuccess(fameId)
	LuaYiRenSiMgr:GetNanDuJuBaoPenRewardSuccess(fameId)
end

function Gas2Gac.NanDuLordBecomeLordSuccess()
	LuaYiRenSiMgr:NanDuLordBecomeLordSuccess()
end

function Gas2Gac.NanDuLordRequestSetLordStatueSuccess()
	print("NanDuLordRequestSetLordStatueSuccess")
end

function Gas2Gac.NanDuTaskBargainsSuccess(bargainId, price)
	g_ScriptEvent:BroadcastInLua("NanDuTaskBargainsSuccess", bargainId, price)
end

function Gas2Gac.CloseSelectConfirmUI()
	CUIManager.CloseUI(CLuaUIResources.HaiZhanPosSelectionWnd)
end
-- @param resultUd	{ { playerId = bConfirm, ... }, { seatId = playerId, ... } }
function Gas2Gac.SyncTeamSelectConfirmInfo(bForceOpen, startTime, waitTimeout, gameplayId, resultUd)
	g_ScriptEvent:BroadcastInLua("SyncTeamSelectConfirmInfo", resultUd)
end

function Gas2Gac.SyncNoTeamSelectConfirmInfo()
	print("SyncNoTeamSelectConfirmInfo")
end

-- nowTime: 	现在的时间，采用服务器推送的值是方便命令改时间测试
-- todayTimes:	今日签到次数，0表示未签到
-- totalTimes：	累计连续签到次数
-- nextBuQianTime：	可补签的日期（表示最近可补签的那天，补签后会重新计算），为0表示没有可补签的
-- usedBuQianTimes：本月已经补签的次数
-- totalBuQianTimes：本月拥有的可补签次数
-- x/7 这个x是用totalTimes%7得来，如果余数为0，若当天已签到，则显示7/7
function Gas2Gac.SendQianDaoInfo_New(nowTime, todayTimes, totalTimes, nextBuQianTime, usedBuQianTimes, totalBuQianTimes)
	LuaWelfareMgr:SendQianDaoInfo_New(nowTime, todayTimes, totalTimes, nextBuQianTime, usedBuQianTimes, totalBuQianTimes)
end

function Gas2Gac.ShowQianDaoWnd_New()
	LuaWelfareMgr:ShowQianDaoWnd_New()
end

function Gas2Gac.ShowQianDaoRefreshFlag_New()
	LuaWelfareMgr:ShowQianDaoRefreshFlag_New()
end

function Gas2Gac.OpenNewQianDao()
	LuaWelfareMgr:OpenNewQianDao()
end

function Gas2Gac.UpdatePlayerProfileFrameInfo(frameId, expiredTime)
	if not CExpressionMgr.Inst then
		return
	end

	CommonDefs.ListIterate(CExpressionMgr.Inst.validProfileFrameList, DelegateFactory.Action_object(function (___value) 
		local data = ___value
		if data[0] == frameId then
			data[1] = expiredTime
		end
	end))
end

function Gas2Gac.SyncRepertoryProp(repoUD)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.RepertoryProp:LoadFromString(repoUD)
	end
end


-- 查询 我的竞拍 的结果
-- Gac2Gas.QueryPaimaiMySystemItems
-- 收到这个之后，接下来开始循环收到 SendPaimaiMySystemItems
function Gas2Gac.SendPaimaiMySystemItemsBegin()
	LuaAuctionMgr:SendPaimaiMySystemItemsBegin()
end

-- 玩家可能会投入很多组，这个RPC每次返回一部分, 直到收到 SendPaimaiMySystemItemsEnd 代表接收完毕
function Gas2Gac.SendPaimaiMySystemItems(infoRpcUd)
	LuaAuctionMgr:SendPaimaiMySystemItems(infoRpcUd)
end

function Gas2Gac.SendPaimaiMySystemItemsEnd()
	LuaAuctionMgr:SendPaimaiMySystemItemsEnd()
end

-- 根据价格查询库存
-- Gac2Gas.QueryPaimaiSystemItemByPrice
function Gas2Gac.SendPaimaiSystemItemByPrice(itemTemplateId, oneShootPrice, currentlowestPrice, takeoffPrice, price, hasCount)
	LuaAuctionMgr:SendPaimaiSystemItemByPrice(itemTemplateId, oneShootPrice, currentlowestPrice, takeoffPrice, price, hasCount)
end

-- Gac2Gas.QueryPaimaiSystemOnShelfItem -> 
-- 如果每页展示8个，那么第一页的RPC参数为
-- beginIndex = 1, endIndex = 8, pageNum = 1, itemTemplateId = 模板Id
-- validTotalCount: 总数(用以计算总页数)
-- pageNum: 当前第几页
-- itemTemplateId: 物品模板Id
-- currentLowestPrice: 当前最低价格
function Gas2Gac.SendSystemOnShelfItem(validTotalCount, infoRpcUd, pageNum, itemTemplateId, currentLowestPrice)
	LuaAuctionMgr:SendSystemOnShelfItem(validTotalCount, infoRpcUd, pageNum, itemTemplateId, currentLowestPrice)
end

-- Gac2Gas.RequestPaimaiBetSystemItem ->
-- 请求购买的结果, bOk = true 表示成功
function Gas2Gac.PlayerRequestBetSystemResult(bOk, itemTemplateId, price, targetType, betCount)
	LuaAuctionMgr:PlayerRequestBetSystemResult(bOk, itemTemplateId, price, targetType, betCount)
end


function Gas2Gac.HideYanjia()
	if CClientMainPlayer.Inst then
		--用这个接口, 假装进入安全区, 收起偃甲
		CClientMainPlayer.Inst:OnEnterSafeRegion(true)
	end
end

function Gas2Gac.SetWeatherSystemProfile(profile)
	WeatherManager.ApplyProfileByName(profile)
end

function Gas2Gac.SetCinemachineXYZ(xRotation, yAngle, zoom)
	local go = CCinemachineMgr.GetActiveCamera()
	if go then
		local ctrl = go:GetComponent(typeof(CCinemachineCtrl))
		if ctrl then ctrl:SetCinemachineXYZ(xRotation,yAngle,zoom) end
	end
end

-- @param reason 0: 未知, 1: 追猎, 2: 死亡丢失, 3: 宝箱
function Gas2Gac.NavalPvpGoldChange(shipId, delta, reason, allGoldU)
	LuaHaiZhanMgr.NavalPvpGoldChange(shipId, delta, reason, allGoldU)
end

function Gas2Gac.NavalPvpExpChange(shipId, delta, allExpU)
	LuaHaiZhanMgr.NavalPvpExpChange(shipId, delta, allExpU)
end

function Gas2Gac.NavalPvpShowSpecialEvent(posIndex, npcEngineId, eventId)
	LuaHaiZhanMgr.NavalPvpShowSpecialEvent(posIndex, npcEngineId, eventId)
end

function Gas2Gac.NavalPvpPlayEnd(rank, killShip, killMonster, packInfo)
	print("NavalPvpPlayEnd", rank, killShip, killMonster, packInfo)
	LuaHaiZhanMgr.NavalPvpPlayEnd(rank, killShip, killMonster, packInfo)
end

-- @param playerId 请求方玩家Id
function Gas2Gac.NavalPvpAcceptSpecialEvent(eventId, reasonId, bAccept)
	LuaHaiZhanMgr.NavalPvpAcceptSpecialEvent(eventId, reasonId, bAccept)
end

-- @param huntingInfoU: local info = {
--     attShipId = shipId,
--     defShipId = targetShipId,
--     startTime = os.time(),
--     attName = attName,
--     defName = defName,
--     stage = 1,
--     eventId = eventId,
-- }
-- 如果 info 为空，表示结束狩猎
function Gas2Gac.NavalPvpSyncHuntInfo(huntingInfoU)
	LuaHaiZhanMgr.NavalPvpSyncHuntInfo(huntingInfoU)
end

function Gas2Gac.GetNavalShipNamesReturn(shipId2NameU)
	-- print("GetNavalShipNamesReturn", shipId2NameU)
	LuaHaiZhanMgr.GetNavalShipNamesReturn(shipId2NameU)
end

function Gas2Gac.SyncNavalWarShipId2EngineId(shipId, shipId2EngineIdU)
	LuaHaiZhanMgr.SyncNavalWarShipId2EngineId(shipId, shipId2EngineIdU)
end

-- 参数参考 StarBiwuSendJingCaiRecordDetail
function Gas2Gac.StarBiwuSendJingCaiHistory(jingcaiIdxsUd, rightChoiceUd, jingcaiStage, targetDate, phaseBetPool, choice2BetValueUd, showResultUd, matchTblUd, maxBetValue)
	-- local jingcaiItemList = MsgPackImpl.unpack(jingcaiIdxsUd)
  	-- if not jingcaiItemList then return end -- 这里包含了4个竞猜项

  	-- local selfChoiceList = MsgPackImpl.unpack(selfChoiceTblUd)
  	-- if not selfChoiceList then return end -- 这里[0-3]分别是自己的选择

  	local rightChoiceList = MsgPackImpl.unpack(rightChoiceUd)
  	if not rightChoiceList then return end -- 这里[0-3]分别是正确的选择

  	-- targetDate -> 这是竞猜比赛日,来源就是策划配表中的比赛日
  	local matchInfoList = g_MessagePack.unpack(matchTblUd)
  	-- 如果对阵表已经出来的话，这里[0-1]分别是对阵的两个队伍名,对阵信息尚未出现的话,是空字符串
	
	local resultList = g_MessagePack.unpack(showResultUd)
	if not resultList then return end

	g_ScriptEvent:BroadcastInLua("StarBiwuSendJingCaiHistory",jingcaiIdxsUd,rightChoiceList, jingcaiStage, targetDate, phaseBetPool, MsgPackImpl.unpack(choice2BetValueUd),resultList,matchInfoList,maxBetValue)
end

function Gas2Gac.ModifyFashionDaPeiNameSuccess_Permanent(index, name)
	LuaAppearancePreviewMgr:ModifyFashionDaPeiNameSuccess_Permanent(index, name)
end

function Gas2Gac.SaveFashionDaPeiContentSuccess_Permanent(index, headId, bodyId, weaponId, backPendantId)
	LuaAppearancePreviewMgr:SaveFashionDaPeiContentSuccess_Permanent(index, headId, bodyId, weaponId, backPendantId)
end

function Gas2Gac.UnLockFashionSuccess_Permanent(fashionId)
	LuaAppearancePreviewMgr:UnLockFashionSuccess_Permanent(fashionId)
end

function Gas2Gac.SetWardRobeTabenNumLimit_Permanent(num)
	LuaAppearancePreviewMgr:SetWardRobeTabenNumLimit_Permanent(num)
end

function Gas2Gac.CompositeTabenSuccess_Permanent()
	LuaAppearancePreviewMgr:CompositeTabenSuccess_Permanent()
	LuaSEASdkMgr:AppsflyerNotify("thoitrang_chetao", SafeStringFormat3("{}"))
end

function Gas2Gac.SetFashionHideSuccess_Permanent(pos, value)
	LuaAppearancePreviewMgr:SetFashionHideSuccess_Permanent(pos, value)
end

function Gas2Gac.UnLockTeamMemberBgSuccess(id)
	LuaAppearancePreviewMgr:UnLockTeamMemberBgSuccess(id)
end

function Gas2Gac.UseTeamMemberBgSuccess(id)
	LuaAppearancePreviewMgr:UseTeamMemberBgSuccess(id)
end

function Gas2Gac.UnLockTeamLeaderIconSuccess(id)
	LuaAppearancePreviewMgr:UnLockTeamLeaderIconSuccess(id)
end

function Gas2Gac.UseTeamLeaderIconSuccess(id)
	LuaAppearancePreviewMgr:UseTeamLeaderIconSuccess(id)
end

function Gas2Gac.SyncIgnoreCheckText(channelId, text, mode, expireTime)
	LuaChatMgr:SyncIgnoreCheckText(channelId, text, mode, expireTime)
end

-- 全服拍卖发生了推迟，如果此时玩家正在新寇岛拍卖界面，则需要刷新页面，因为拍卖结束时间发生了变化
-- 如果玩家不处于新寇岛拍卖界面，则不需要处理这个RPC
function Gas2Gac.SyncSystemPaimaiDelay(playId)
	LuaAuctionMgr:SyncSystemPaimaiDelay(playId)
end

function Gas2Gac.SyncWardrobeProperty(wardrobeProp, reason)
	LuaAppearancePreviewMgr:SyncWardrobeProperty(wardrobeProp, reason)
end

function Gas2Gac.OpenPermanentFashionSwitch()
	print("OpenPermanentFashionSwitch")
	LuaAppearancePreviewMgr:EnableNewAppearanceWnd()
end

function Gas2Gac.EvaluateFashionSuitSuccess_Permanent(suitId)
    LuaAppearancePreviewMgr:EvaluateFashionSuitSuccess_Permanent(suitId)
end

function Gas2Gac.RequestChangeToCbgTradeItemResult(itemId, bSuccess)
    LuaAppearancePreviewMgr:RequestChangeToCbgTradeItemResult(itemId, bSuccess)
end

function Gas2Gac.RequestChangeToNoneCbgTradeItemResult(itemId, bSuccess)
    LuaAppearancePreviewMgr:RequestChangeToNoneCbgTradeItemResult(itemId, bSuccess)
end

function Gas2Gac.SyncFashionability(value)
	LuaAppearancePreviewMgr:SyncFashionability(value)
end

function Gas2Gac.UnLockFashionByItemConfirm_Permanent(fashionId, progress, itemId, place, pos)
	LuaAppearancePreviewMgr:UnLockFashionByItemConfirm_Permanent(fashionId, progress, itemId, place, pos)
end

function Gas2Gac.TakeOffFashion_Permanent(fashionId)
	LuaAppearancePreviewMgr:TakeOffFashion_Permanent(fashionId)
end

function Gas2Gac.TakeOffFashionSuit_Permanent(suitId)
	LuaAppearancePreviewMgr:TakeOffFashionSuit_Permanent(suitId)
end

--升级时提示可以找师傅
function Gas2Gac.RemindBaiShiOnLevelUp()
	CLuaGuideMgr.RemindBaiShiOnLevelUp()
end

function Gas2Gac.SendJuQingFail()
	CLuaGuideMgr.SendJuQingFail()
end
function Gas2Gac.PlayGameVideo(videoId)
	-- 先设置成所有动画播放完都有一个回调
	AMPlayer.Inst:Play(videoId, nil, DelegateFactory.Action_string(function(id)
		Gac2Gas.PlayGameVideoDone(id)
	end))
end


function Gas2Gac.AddFashionSuccess_Permanent(fashionId, isExist)
	LuaAppearancePreviewMgr:AddFashionSuccess_Permanent(fashionId, isExist)
end

function Gas2Gac.UnLockFashionAlert_Permanent(fashionId)
	LuaAppearancePreviewMgr:UnLockFashionAlert_Permanent(fashionId)
end

function Gas2Gac.ReplyPublishToStarBiwuFreeMember(bPublish)
	CLuaStarBiwuMgr.m_PublishInfoToFreePlayer = bPublish
	g_ScriptEvent:BroadcastInLua("OnPublishStarBiWuInfoToFreePlayerStatusChanged")
end

function Gas2Gac.ReplyInviteStarBiwuFreeMember(targetId)
end

function Gas2Gac.ReplyStarBiwuFreeMemberAcceptBeInvite(zhanduiId)
	CUIManager.CloseUI(CLuaUIResources.StarBiWuZhanDuiInvitedWnd)
end

function Gas2Gac.ReplyStarBiwuFreeMemberRefuseBeInvite(zhanduiId)
end

function Gas2Gac.SyncStarBiwuJifensaiMainRankS13(group, selfGroup, selfZhanduiId, ud)
end

function Gas2Gac.SyncStarBiwuJifensaiRoundRankS13(group, round, selfGroup, selfZhanduiId, ud)
end

function Gas2Gac.SyncStarBiwuTaotaisaiMatchS13(group, selfGroup, selfZhanduiId, ud, currentMatchPlayIdx)
	local rpcInfo = CStarBiwuTaotaisaiMatch_Rpc()
	rpcInfo:LoadFromString(ud)
	-- print("SyncStarBiwuTaotaisaiMatchS13", currentMatchPlayIdx)
	CLuaStarBiwuMgr:OnSyncStarBiwuTaiTaiSaiMatchS13(group, selfGroup, selfZhanduiId, rpcInfo, currentMatchPlayIdx)
	-- CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
	-- 	print(key, "AttackerZhanduiId", v.AttackerZhanduiId)
	-- 	print(key, "AttackerZhanDuiName", v.AttackerZhanDuiName)
	-- 	print(key, "DefenderZhanduiId", v.DefenderZhanduiId)
	-- 	print(key, "DefenderZhanDuiName", v.DefenderZhanDuiName)
	-- 	print(key, "AttackerScore", v.AttackerScore)
	-- 	print(key, "DefenderScore", v.DefenderScore)
	-- 	print(key, "WinnerZhanduiId", v.WinnerZhanduiId)
	-- 	print(key, "PlayStatus", v.PlayStatus) -- 0 未开始 1 正在打 2结果出来了
	-- end))
end

function Gas2Gac.SyncStarBiwuFreememberS13(profession, sortMethed, memberInfoUd, page, count, bPublishSelfInfo, totalPageCount)
	CLuaStarBiwuMgr:OnSyncStarBiwuFreememberS13(profession, sortMethed, memberInfoUd, page, count, bPublishSelfInfo, totalPageCount)
	-- local list = MsgPackImpl.unpack(memberInfoUd)
	-- if not list then return end
	-- for i = 0, list.Count - 1 do
	-- 	local info = list[i]
	-- 	local memberId = info[0]
	-- 	local playerName = info[1]
	-- 	local fromServerName = info[2]
	-- 	local fakeAppearance = CreateFromClass(CPropertyAppearance)
	-- 	fakeAppearance:LoadFromString(info[3], EnumClass.Undefined, EnumGender.Undefined)
	-- 	local capacity = info[4]
	-- 	local starScore = info[5]
	-- 	local profession = info[6]
	-- 	local gender = info[7]
	-- 	local grade = info[8]
		
	-- 	print(i, "fakeAppearance", memberId, starScore, profession, gender, grade)
	-- end
end

function Gas2Gac.SyncStarBiwuMyInviverBegin()
	CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList = {}
end

function Gas2Gac.SyncStarBiwuMyInviver(ud)
	local rpcInfo = CStarBiwuMyInvite_Rpc()
	rpcInfo:LoadFromString(ud)
	CLuaStarBiwuMgr:OnSyncStarBiwuMyInviver(rpcInfo)
	-- CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
	-- 	print(key, "ZhanduiId", v.ZhanduiId)
	-- 	print(key, "ZhanDuiName", v.ZhanDuiName)
	-- 	print(key, "Capacity", v.Capacity) -- 战力
	-- 	print(key, "StarScore", v.StarScore) -- 明星值
	-- 	print(key, "InviterId", v.InviterId) -- 邀请者id
	-- 	print(key, "InviterName", v.InviterName) -- 邀请者昵称
	-- 	print(key, "AcceptFlag", v.AcceptFlag) -- 0尚未同意 1已经同意

	-- 	CommonDefs.DictIterate(v.ZhanDuiMembers, DelegateFactory.Action_object_object(function(key_2, v_2)
	-- 		print(key, "ZhanDuiMembers", v_2.MemberId, v_2.MemberClass)
	-- 	end))
		
	-- end))
end

function Gas2Gac.SyncStarBiwuMyInviverEnd(ud)
	g_ScriptEvent:BroadcastInLua("ReplyStarBiwuZhanDuiInvitedList")
end

function Gas2Gac.SendServerOpenPassedDay(serverOpenPassedDays, context)
	--print("SendServerOpenPassedDay", serverOpenPassedDays, context)
	LuaLoginMgr:SetServerOpenTime(serverOpenPassedDays, context)
end

function Gas2Gac.ShowMessageAfterTakeOnFashion_Permanent(position, fashionId, bHide)
	LuaAppearancePreviewMgr:ShowMessageAfterTakeOnFashion_Permanent(position, fashionId, bHide)
end

-- 第13届明星赛活动页状态
-- signUpStatus:
-- 1: 尚未进入这一届的报名阶段
-- 2: 进入报名阶段但是没有报名的权限
-- 3: 进入报名阶段但有报名的权限
-- 4: 已经截止报名
-- bSignUp: true 已报名 false 未报名
-- bHasFight: 是否存在比赛(观战), true 存在
function Gas2Gac.SyncStarBiwuActivityStatusS13(signUpStatus, bSignUp, bHasFight)
	CLuaStarBiwuMgr:OnSyncStarBiwuActivityStatusS13(signUpStatus, bSignUp, bHasFight)
end

-- 第13届明星赛战队名
function Gas2Gac.SyncStarBiwuZhanduiNameBegin()
	CLuaStarBiwuMgr.m_ZhuanDuiId2ZhuanDuiName = {}
end

function Gas2Gac.SyncStarBiwuZhanduiName(ud)
	local rpcInfo = CStarBiwuJifensaiZhanduiName_Rpc()
	rpcInfo:LoadFromString(ud)
	CLuaStarBiwuMgr:OnSyncJiFenSaiZhuanDuiId2Name(rpcInfo)
	-- CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
	-- 	print(key, v)
	-- end))
end

function Gas2Gac.SyncStarBiwuZhanduiNameEnd()
end


-- 第13届明星赛积分赛排行信息
function Gas2Gac.SyncStarBiwuJifensaiRankS13Begin()
	CLuaStarBiwuMgr.m_JiFenSaiRankInfo = {}
end

function Gas2Gac.SyncStarBiwuJifensaiRankS13(ud)
	local rpcInfo = CStarBiwuJifensaiRank_Rpc()
	rpcInfo:LoadFromString(ud)
	CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiRankS13(rpcInfo)
	-- CommonDefs.DictIterate(rpcInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
	-- 	print(key, "ZhanduiId", v.ZhanduiId)
	-- 	print(key, "MainRank", v.MainRank.WinCount, v.MainRank.LoseCount, v.MainRank.WinTime)
		
	-- 	print(key, "RoundRank")
		
	-- 	-- key_3 : 轮次
	-- 	CommonDefs.DictIterate(v.RoundRank, DelegateFactory.Action_object_object(function(key_3, v_3)
	-- 		print(key_3, v_3.WinFlag, v_3.MatchZhanduiId)
	-- 	end))
	-- end))
end

function Gas2Gac.SyncStarBiwuJifensaiRankS13End(group, selfGroup, selfZhanduiId)
	CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiRankS13End(group, selfGroup, selfZhanduiId)
end

-- 第13届明星赛积分赛对阵信息
function Gas2Gac.SyncStarBiwuJifensaiMatchS13Begin()
	CLuaStarBiwuMgr.m_JiFenSaiMatchInfo = {}
end

function Gas2Gac.SyncStarBiwuJifensaiMatchS13(ud)
	local matchInfo = CStarBiwuJifensaiMatch_Rpc()
	matchInfo:LoadFromString(ud)
	CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiMatchS13(matchInfo)
	-- print("round", matchInfo.Round)
	
	-- CommonDefs.DictIterate(matchInfo.Infos, DelegateFactory.Action_object_object(function(key, v)
	-- 	print(key, "AttackerZhanduiId", v.AttackerZhanduiId)
	-- 	print(key, "DefenderZhanduiId", v.DefenderZhanduiId)
	-- 	print(key, "AttackerScore", v.AttackerScore)
	-- 	print(key, "DefenderScore", v.DefenderScore)
	-- 	print(key, "WinnerZhanduiId", v.WinnerZhanduiId)
	-- 	print(key, "PlayStatus", v.PlayStatus) -- 0 未开始 1 正在打 2结果出来了
	-- end))
end

function Gas2Gac.SyncStarBiwuJifensaiMatchS13End(day, group, selfGroup, selfZhanduiId, bMatchGenerated)
	CLuaStarBiwuMgr:OnSyncStarBiwuJifensaiMatchS13End(day, group, selfGroup, selfZhanduiId, bMatchGenerated)
end

-- Gac2Gas.QueryStarBiwuJifesanSaiGroupS13
function Gas2Gac.SyncStarBiwuJifesanSaiGroupS13(selfGroup, groupCount, selfZhanduiId)
	CLuaStarBiwuMgr:OnSyncStarBiwuJifesanSaiGroupS13(selfGroup, groupCount, selfZhanduiId)
end

-- 淘汰赛分组情况 Gac2Gas.QueryStarBiwuTaotaisaiGroupS13()
function Gas2Gac.SyncStarBiwuTaotaisaiGroupS13(selfGroup, groupCount, selfZhanduiId)
	CLuaStarBiwuMgr:OnSyncStarBiwuTaotaiSaiGroupS13(selfGroup, groupCount, selfZhanduiId)
end

-- 竞猜问答
function Gas2Gac.SyncStarBiwuJingCaiWenDa(ud)
	local info = CStarBiwuJingCaiWenDa_Rpc()
	info:LoadFromString(ud)
	CLuaStarBiwuMgr:GetQuestionInfo(info)
 end

function Gas2Gac.SyncGuideManualData(guideManualDataUd)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.PlayProp.GuideManualData:LoadFromString(guideManualDataUd)
		g_ScriptEvent:BroadcastInLua("SyncGuideManualData")
	end
end

function Gas2Gac.DiscardExpressionAppearance(group, appearance)
	LuaAppearancePreviewMgr:DiscardExpressionAppearance(group, appearance)
end

-- 同步桃花血量，进入视野或更新血量时都会同步
function Gas2Gac.SyncHZXJTaoHuaHp(engineId, hp, currentHpFull, hpFull)
	print("Gas2Gac.SyncHZXJTaoHuaHp", engineId, hp, currentHpFull, hpFull)
end

-- 同步桃花被拾取状态，用于显示headinfo
-- @param isPicking 是否正在被拾取, 变化状态时会同步
-- @param pickTime 拾取时间, 拾取时每秒同步，总时长读表
function Gas2Gac.SyncHZXJTaoHuaPickStatus(engineId, isPicking, pickTime)
	print("Gas2Gac.SyncHZXJTaoHuaPickStatus", engineId, isPicking, pickTime)
end

-- 同步桃花是否要显示被打烂的模型状态
-- @param isRotten true: 要显示被打烂的模型
function Gas2Gac.SyncHZXJTaoHuaStatus(engineId, isRotten)
	print("Gas2Gac.SyncHZXJTaoHuaStatus", engineId, isRotten)
end

-- 更新副本中采集到的桃花瓣的数量
-- @param pretty 桃花瓣数量
-- @param rotten 烂桃花数量
function Gas2Gac.SyncHZXJTaoHuaResult(pretty, rotten)
	print("Gas2Gac.SyncHZXJTaoHuaResult", pretty, rotten)
end

-- 通知客户端拾取桃花瓣的结果
-- @param isRotten 是否是烂桃花
function Gas2Gac.RequestPickHZXJTaoHuaDone(playerId, taoHuaEngineId, isRotten)
	print("Gas2Gac.RequestPickHZXJTaoHuaDone", playerId, taoHuaEngineId, isRotten)
end

-- 玩法结算界面
-- @param bWin 是否胜利
-- @param pretty 桃花瓣数量
-- @param rotten 烂桃花数量
-- @param process 加世界进度
-- @param playerInfo { 玩家1Id, 玩家1名字, 玩家1性别, 玩家2Id, 玩家2名字, 玩家2性别 }
function Gas2Gac.SyncHZXJResult(bWin, pretty, rotten, process, name1, name2)
	print("Gas2Gac.SyncHZXJResult", pretty, rotten, process, name1, name2)
end

function Gas2Gac.HuntRabbitSyncStatusToPlayersInPlay(playStatus, enterStatusTime, leftTime, extraData)
	--print("Gas2Gac.HuntRabbitSyncStatusToPlayersInPlay", playStatus, enterStatusTime, leftTime, extraData)
	LuaZhuJueJuQingMgr:UpdateHuntRabbitPlayStatus(playStatus, enterStatusTime, leftTime, extraData)
end

function Gas2Gac.HuntRabbitReplyRingPosResult(bSuccess, x, y)
	--print("Gas2Gac.HuntRabbitReplyRingPosResult", bSuccess, x, y)
	LuaZhuJueJuQingMgr:HuntRabbitReplyRingPosResult(bSuccess, x, y)
end

function Gas2Gac.QueryDayOnlineTimeResult(onlineTime, loginTimeStamp)
	LuaSEASdkMgr:OnSyncDayOnlineTimeResult(onlineTime, loginTimeStamp)
end 