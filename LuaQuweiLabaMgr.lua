
LuaQuweiLabaMgr = class()
LuaQuweiLabaMgr.m_PlayerId = nil
LuaQuweiLabaMgr.m_PlayerName = nil
LuaQuweiLabaMgr.m_Type = nil

function LuaQuweiLabaMgr:ShowLaBaWnd(playerId, playerName)
    self.m_PlayerId = playerId
    self.m_PlayerName = playerName
    CUIManager.ShowUI(CLuaUIResources.QuweiLabaWnd)
end
