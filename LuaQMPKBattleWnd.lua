local CServerTimeMgr=import "L10.Game.CServerTimeMgr"

CLuaQMPKBattleWnd = class()
RegistClassMember(CLuaQMPKBattleWnd,"m_Button")
RegistClassMember(CLuaQMPKBattleWnd,"m_SelectedSprite")

function CLuaQMPKBattleWnd:Init()
    self.m_Button = {}
    self.m_Button[1]=FindChild(self.transform,"Matching").gameObject
    self.m_Button[2]=FindChild(self.transform,"Challenge").gameObject
    self.m_Button[3]=FindChild(self.transform,"Button3").gameObject

    -- 8.9-8.12
    --8.14-8.17
    --8.26
    --文字颜色调整
    local label=self.m_Button[3].transform:Find("Label"):GetComponent(typeof(UILabel))


    local open = CLuaQMPKMgr.IsMeiRiYiZhanOpen()
    -- if month==7 and day>=26 and day<=31 then
        -- open=true
    -- if month==8 and ((day>=9 and day<=12) or (day>=14 and day<=17) or (day==26)) then
    --     open=true
    -- end
    local lock = self.m_Button[3].transform:Find("Lock").gameObject
    local icon = self.m_Button[3].transform:Find("GameObject"):GetComponent(typeof(UITexture))
    if open then
        label.color = Color(0,1,65/256)--绿色
        icon.alpha = 1
        lock.gameObject:SetActive(false)
    else
        label.color = Color(1,88/256,54/256)--红色
        icon.alpha = 0.5
        lock.gameObject:SetActive(true)
    end

    for i,v in ipairs(self.m_Button) do
        UIEventListener.Get(v).onClick =DelegateFactory.VoidDelegate(function(go) 
            self:OnButtonClicked(go) 
        end)
    end
end

function CLuaQMPKBattleWnd:OnButtonClicked( go) 
    for i,v in ipairs(self.m_Button) do
        if go == v then
            if i == 1 then
                Gac2Gas.RequestSingUpQmpkFreeMatch()
            elseif i == 2 then
                CUIManager.ShowUI(CLuaUIResources.QMPKQieCuoWnd)
            elseif i==3 then
                CUIManager.ShowUI(CLuaUIResources.QMPKMeiRiYiZhanWnd)
            end
        end
    end
    CUIManager.CloseUI(CLuaUIResources.QMPKBattleWnd)
end

return CLuaQMPKBattleWnd
