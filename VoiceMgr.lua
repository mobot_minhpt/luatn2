local CVoiceMgr = import "L10.Game.CVoiceMgr"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local VoicePlayInfo = import "L10.Game.VoicePlayInfo"
local CUIManager = import "L10.UI.CUIManager"

LuaVoiceMgr = class()

LuaVoiceMgr.JingLingVoiceBaseURL = "https://hi-163-qnm.nosdn.127.net/jlaudio/"

--从游戏服查询频道翻译
function LuaVoiceMgr.PlayerQueryTranslationFromChannel(channelId, channelName, voiceId)
    Gac2Gas.PlayerQueryTranslationFromChannel(channelId or 0, channelName or "", voiceId or "")
end
--将IM翻译发送给游戏服
function LuaVoiceMgr.BroadcastVoiceTranslationOnIM(recvId, voiceId, duration, msg1, msg2)
    Gac2Gas.BroadcastVoiceTranslationOnIM(recvId, voiceId, duration, msg1, msg2)
end
function LuaVoiceMgr.BroadcastVoiceTranslationToGroup(recvIds, voiceId, duration, msg1, msg2)
    Gac2Gas.BroadcastVoiceTranslationToGroup(recvIds, voiceId, duration, msg1, msg2)
end
--从游戏服查询语音翻译,仅提供给IM使用
function LuaVoiceMgr.QueryVoiceTranslationOnIM(voiceId)
	Gac2Gas.QueryVoiceTranslation(voiceId,EnumQueryVoiceTranslationContext.OnIM)
end
--从游戏服查询语音翻译,context定义为EnumQueryVoiceTranslationContext
function LuaVoiceMgr.QueryVoiceTranslation(voiceId, context)
	Gac2Gas.QueryVoiceTranslation(voiceId, context)
end

function LuaVoiceMgr.PlayJingLingVoice(voiceName)
    if voiceName and voiceName~= "" then
        local info = VoicePlayInfo(voiceName, EnumVoicePlatform.PersonalSpace, LuaVoiceMgr.JingLingVoiceBaseURL..voiceName)
        CUIManager.PlayVoice(info)
    end
end

function LuaVoiceMgr.OnQueryVoiceTranslation()
    Gac2Gas.OnQueryVoiceTranslation()
end

function LuaVoiceMgr.LogVoiceTranslationOnIM(senderId, receiverId, voiceId, voiceLength, msg1, msg2)
    Gac2Gas.LogVoiceTranslationOnIM_v2(senderId, receiverId, voiceId, voiceLength, msg1, msg2)
end

---------------
--Gas2Gac RPC--
---------------

function Gas2Gac.PlayerQueryTranslationResult(voiceId, success, translation, context)
    -- Gac2Gas.PlayerQueryTranslationFromChannel 和 Gac2Gas.QueryVoiceTranslationOnIM RPC的服务器回应均为该RPC
    if success then
        CVoiceMgr.Inst:AddVoiceText(voiceId, translation)
    end
end

function Gas2Gac.BroadcastVoiceTranslationOnIM(voiceId, voiceLength, translation)
	CVoiceMgr.Inst:AddVoiceText(voiceId, translation)
end