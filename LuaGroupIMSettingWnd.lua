local UIToggle = import "UIToggle"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"
local EventDelegate = import "EventDelegate"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CInputWndMgr = import "L10.UI.CInputWndMgr"
local CGroupIMSettingItem = import "L10.UI.CGroupIMSettingItem"
local CIMMgr = import "L10.Game.CIMMgr"
local UILabelOverflow = import "UILabel+Overflow"
local AnchorPoint = import "UIRect+AnchorPoint"

LuaGroupIMSettingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGroupIMSettingWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaGroupIMSettingWnd, "RemoveMemberButton", "RemoveMemberButton", GameObject)
RegistChildComponent(LuaGroupIMSettingWnd, "ChangeGroupNameButton", "ChangeGroupNameButton", GameObject)
RegistChildComponent(LuaGroupIMSettingWnd, "ChangeMottoBtn", "ChangeMottoBtn", GameObject)
RegistChildComponent(LuaGroupIMSettingWnd, "ShiTuTeamButton", "ShiTuTeamButton", GameObject)
RegistChildComponent(LuaGroupIMSettingWnd, "MemberCountLabel", "MemberCountLabel", UILabel)
RegistChildComponent(LuaGroupIMSettingWnd, "GroupNameLabel", "GroupNameLabel", UILabel)
RegistChildComponent(LuaGroupIMSettingWnd, "MottoLabel", "MottoLabel", UILabel)
RegistChildComponent(LuaGroupIMSettingWnd, "NeedApproveToggle", "NeedApproveToggle", UIToggle)
RegistChildComponent(LuaGroupIMSettingWnd, "AlertToggle", "AlertToggle", UIToggle)
RegistChildComponent(LuaGroupIMSettingWnd, "GuildAimBg", "GuildAimBg", UIWidget)
--@endregion RegistChildComponent end
RegistClassMember(LuaGroupIMSettingWnd, "m_List")
RegistClassMember(LuaGroupIMSettingWnd, "m_IsShiTuView")
RegistClassMember(LuaGroupIMSettingWnd, "m_IsShiFu")
RegistClassMember(LuaGroupIMSettingWnd, "m_TudiIndex")

function LuaGroupIMSettingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RemoveMemberButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRemoveMemberButtonClick()
	end)


	
	UIEventListener.Get(self.ChangeGroupNameButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeGroupNameButtonClick()
	end)


	
	UIEventListener.Get(self.ChangeMottoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeMottoBtnClick()
	end)


    UIEventListener.Get(self.ShiTuTeamButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShiTuTeamButtonClick()
	end)
    --@endregion EventBind end
end

function LuaGroupIMSettingWnd:Init()
    self.m_IsShiTuView = LuaGroupIMMgr:IsShiTuIMId(CGroupIMMgr.m_SettingWndGroupIMId)
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function() 
            return (self.m_List and #self.m_List or 0) + (self.m_IsShiTuView and 0 or 1)
        end,
        function(view, row)
            return self:ItemAt(view, row)
        end)
    EventDelegate.Add(self.NeedApproveToggle.onChange,DelegateFactory.Callback(function () 
        Gac2Gas.SetNeedApprove(CGroupIMMgr.m_SettingWndGroupIMId, self.NeedApproveToggle.value)
    end))
    EventDelegate.Add(self.AlertToggle.onChange,DelegateFactory.Callback(function () 
        self:SetGroupIMIgnoreNewMsg()
    end))
    local isOwer = self:IsOwner()
    self.ShiTuTeamButton.gameObject:SetActive(self.m_IsShiTuView)
    self.NeedApproveToggle.gameObject:SetActive(isOwer)
    self.ChangeGroupNameButton.gameObject:SetActive(isOwer)
    self.ChangeMottoBtn.gameObject:SetActive(isOwer)
    self.RemoveMemberButton.gameObject:SetActive(isOwer and not self.m_IsShiTuView)
    self.AlertToggle.gameObject:SetActive(true)
    local value = false
    if CClientMainPlayer.Inst and CommonDefs.DictContains(CClientMainPlayer.Inst.RelationshipProp.JoinedGroupIM, typeof(ulong), CGroupIMMgr.m_SettingWndGroupIMId) and
    CClientMainPlayer.Inst.RelationshipProp.JoinedGroupIM[CGroupIMMgr.m_SettingWndGroupIMId].IgnoreNewMsg == 0 then
        value = true
    end
    self.AlertToggle.value = value
    self:UpdateInfo()
end

function LuaGroupIMSettingWnd:ItemAt(view, row)
    if row == 0 and not self.m_IsShiTuView then
        local item = view:GetFromPool(0)
        return item
    else
        local item = TypeAs(view:GetFromPool(1), typeof(CGroupIMSettingItem))
        local index = self.m_IsShiTuView and row + 1 or row
        item:Init(self.m_List[index])
        item.toggle.gameObject:SetActive(not self.m_IsShiTuView)
        local titleArr = self.m_IsShiFu and 
        {LocalString.GetString("大徒弟"),LocalString.GetString("二徒弟"),LocalString.GetString("三徒弟"),LocalString.GetString("四徒弟")} or 
        {LocalString.GetString("大师兄"),LocalString.GetString("二师兄"),LocalString.GetString("三师兄"),LocalString.GetString("四师弟")}
        if self.m_IsShiTuView and row ~= 0 then
            local title = titleArr[row]
            if self.m_TudiIndex then
                local allTudiInfo = LuaShiTuMgr.m_ShifuAllTudiInfo.allTudiInfo
                local tudiInfo = allTudiInfo[self.m_List[index].BasicInfo.ID]
                local isWaiMen = tudiInfo.isWaiMen
                if isWaiMen then
                    title = LocalString.GetString("外门弟子")
                end
                if row > self.m_TudiIndex then
                    title = string.gsub(title, LocalString.GetString("师兄"), LocalString.GetString("师弟"))
                elseif row == self.m_TudiIndex then
                    title = LocalString.GetString("我")
                end
                if self.m_List[index].BasicInfo.Gender == 1 then
                    title = string.gsub(title, LocalString.GetString("师兄"), LocalString.GetString("师姐"))
                    title = string.gsub(title, LocalString.GetString("师弟"), LocalString.GetString("师妹"))
                end
                if row ~= self.m_TudiIndex then
                    local isFriend = CIMMgr.Inst:IsMyFriend(self.m_List[index].BasicInfo.ID)
                    if not isFriend then
                        title = title .. LocalString.GetString("(非好友)")
                    end
                end
            elseif self.m_IsShiFu then
                local t = CClientMainPlayer.Inst.RelationshipProp.TuDi[self.m_List[index].BasicInfo.ID]
                local isWaiMen = t and (t.Extra:GetBit(2)) or false
                if isWaiMen then
                    title = LocalString.GetString("外门弟子")
                end
            end
            item.groupLabel.text = title
            item.groupLabel.overflowMethod = UILabelOverflow.ResizeFreely
            item.groupLabel.rightAnchor = AnchorPoint(400)
        elseif self.m_IsShiTuView and row == 0 then
            item.groupLabel.text = LocalString.GetString("师父")
        end
        return item
    end 
end

function LuaGroupIMSettingWnd:UpdateInfo()
    local info = CGroupIMMgr.Inst:GetGroupIM(CGroupIMMgr.m_SettingWndGroupIMId)
    if info then
        self.GroupNameLabel.text = info.GroupName.StringData
        self.MottoLabel.text = info.GroupAnnouncement.StringData
        self.MemberCountLabel.text = SafeStringFormat3("%d/%d", info.Members.Keys.Count, CGroupIMMgr.GetMaxGroupMemberNum(info.Type)) --IL2CPP_SMALLCODE info.Members.Keys.Length, CGroupIMMgr.MaxGroupMemberNum
        self.m_List = {}
        CommonDefs.DictIterate(info.Members, DelegateFactory.Action_object_object(function(key,val)
            table.insert(self.m_List, val)
        end))
        self.m_IsShiFu = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == info.OwnerId
        self:SortShiTuViewList(info)
    
        if self.m_IsShiTuView then
            self.ChangeGroupNameButton.gameObject:SetActive(self.m_IsShiFu)
            self.ChangeMottoBtn.gameObject:SetActive(self.m_IsShiFu)
            self.NeedApproveToggle.gameObject:SetActive(false)
        end
        self.GuildAimBg.height = self.m_IsShiTuView and 728 or 628
        self.NeedApproveToggle.value = info.NeedApprove > 0
    end
    self.TableView:ReloadData()
end

function LuaGroupIMSettingWnd:SortShiTuViewList(info)
    if self.m_IsShiTuView  then
        if not self.m_IsShiFu then
            if LuaShiTuMgr.m_ShifuAllTudiInfo.shifuId ~= info.OwnerId then
                Gac2Gas.RequestShifuAllTudiInfo(0)
            else
                local allTudiInfo = LuaShiTuMgr.m_ShifuAllTudiInfo.allTudiInfo
                if allTudiInfo then
                    table.sort(self.m_List,function (a,b )
                        if a.BasicInfo.ID == info.OwnerId then
                            return true
                        elseif b.BasicInfo.ID == info.OwnerId then
                            return false
                        end
                        local t1 = allTudiInfo[a.BasicInfo.ID]
                        local t2 = allTudiInfo[b.BasicInfo.ID]
                        local isWaiMen1 = t1.isWaiMen
                        local isWaiMen2 = t2.isWaiMen
                        if isWaiMen1 and not isWaiMen2 then
                            return false
                        elseif isWaiMen2 and not isWaiMen1 then
                            return true
                        end
                        if t1 and t2 then
                            return t1.tudiTime < t2.tudiTime
                        end
                        return a.BasicInfo.ID < b.BasicInfo.ID
                    end)
                end
            end
            for i = 2,#self.m_List do
                if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == self.m_List[i].BasicInfo.ID then
                    self.m_TudiIndex = i - 1
                end
            end
        else
            if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RelationshipProp.TuDi then
                table.sort(self.m_List,function (a,b )
                    if a.BasicInfo.ID == info.OwnerId then
                        return true
                    elseif b.BasicInfo.ID == info.OwnerId then
                        return false
                    end
                    local t1 = CClientMainPlayer.Inst.RelationshipProp.TuDi[a.BasicInfo.ID]
                    local t2 = CClientMainPlayer.Inst.RelationshipProp.TuDi[b.BasicInfo.ID]
                    local isWaiMen1 = t1 and (t1.Extra:GetBit(2)) or false
                    local isWaiMen2 = t2 and (t2.Extra:GetBit(2)) or false
                    if isWaiMen1 and not isWaiMen2 then
                        return false
                    elseif isWaiMen2 and not isWaiMen1 then
                        return true
                    end
                    if t1 and t2 then
                        return t1.Time < t2.Time
                    end
                    return a.BasicInfo.ID < b.BasicInfo.ID
                end)
            end
        end
    end
end

function LuaGroupIMSettingWnd:IsOwner()
    local info = CGroupIMMgr.Inst:GetGroupIM(CGroupIMMgr.m_SettingWndGroupIMId)
    if info and CClientMainPlayer.Inst then
        return CClientMainPlayer.Inst.Id == info.OwnerId
    end
    return false
end

function LuaGroupIMSettingWnd:OnEnable()
    g_ScriptEvent:AddListener("GroupIMSetGroupName", self, "OnGroupIMSetGroupName")
    g_ScriptEvent:AddListener("GroupIMSetGroupAnnouncement", self, "OnGroupIMSetGroupAnnouncement")
    g_ScriptEvent:AddListener("PlayerLeaveGroupIM", self, "OnPlayerLeaveGroupIM")
    g_ScriptEvent:AddListener("MainPlayerLeaveGroupIM", self, "OnMainPlayerLeaveGroupIM")
    g_ScriptEvent:AddListener("PlayerJoinGroupIM", self, "OnPlayerJoinGroupIM")
    g_ScriptEvent:AddListener("OnSendShifuAllTudiInfo", self, "OnSendShifuAllTudiInfo")
end

function LuaGroupIMSettingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GroupIMSetGroupName", self, "OnGroupIMSetGroupName")
    g_ScriptEvent:RemoveListener("GroupIMSetGroupAnnouncement", self, "OnGroupIMSetGroupAnnouncement")
    g_ScriptEvent:RemoveListener("PlayerLeaveGroupIM", self, "OnPlayerLeaveGroupIM")
    g_ScriptEvent:RemoveListener("MainPlayerLeaveGroupIM", self, "OnMainPlayerLeaveGroupIM")
    g_ScriptEvent:RemoveListener("PlayerJoinGroupIM", self, "OnPlayerJoinGroupIM")
    g_ScriptEvent:RemoveListener("OnSendShifuAllTudiInfo", self, "OnSendShifuAllTudiInfo")
end

function LuaGroupIMSettingWnd:OnSendShifuAllTudiInfo()
    local info = CGroupIMMgr.Inst:GetGroupIM(CGroupIMMgr.m_SettingWndGroupIMId)
    if info and LuaShiTuMgr.m_ShifuAllTudiInfo.shifuId == info.OwnerId then
        self:UpdateInfo()
    end
end

function LuaGroupIMSettingWnd:OnGroupIMSetGroupName(data)
    local serialNum, name = data[0],data[1]
    if serialNum == CGroupIMMgr.m_SettingWndGroupIMId then
        self.GroupNameLabel.text = name
        g_MessageMgr:ShowMessage("CHANGE_SUCCESS")
    end
end

function LuaGroupIMSettingWnd:OnGroupIMSetGroupAnnouncement(data)
    local serialNum, str = data[0],data[1]
    if serialNum == CGroupIMMgr.m_SettingWndGroupIMId then
        self.MottoLabel.text = str
        g_MessageMgr:ShowMessage("ESTABLISHES_SUCCESS")
    end
end

function LuaGroupIMSettingWnd:OnPlayerLeaveGroupIM(data)
    local serialNum, playerId = data[0],data[1]
    if not CClientMainPlayer.Inst then
        return
    end
    if CClientMainPlayer.Inst.Id == playerId then
        CUIManager.CloseUI(CUIResources.GroupIMSettingWnd)
    else
        self:UpdateInfo()
    end
end

function LuaGroupIMSettingWnd:OnMainPlayerLeaveGroupIM(data)
    local serialNum = data[0]
    CUIManager.CloseUI(CUIResources.GroupIMSettingWnd)
end

function LuaGroupIMSettingWnd:OnPlayerJoinGroupIM(data)
    local serialNum, playerId = data[0],data[1]
    if serialNum == CGroupIMMgr.m_SettingWndGroupIMId then
        self:UpdateInfo()
    end
end

--@region UIEvent

function LuaGroupIMSettingWnd:OnRemoveMemberButtonClick()
    local isOwer = self:IsOwner()
    if not isOwer then
        g_MessageMgr:ShowMessage("NO_PERMISSION")
        return
    end
    if not CClientMainPlayer.Inst then 
        return
    end
    local count = 0
    local id = CClientMainPlayer.Inst.Id
    for i= 0, self.TableView.Count - 1 do
        local cmp =  TypeAs(self.TableView:GetItemAtRow(i), typeof(CGroupIMSettingItem))
        if cmp then
            if id ~= cmp.PlayerId and cmp.IsChosen then
                count = count + 1
            end
        end
    end
    if count == 0 then
        g_MessageMgr:ShowMessage("CHECK_NO_ONE")
        return
    end
    local msg = g_MessageMgr:FormatMessage("SURE_TO_CHECK")
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		self:ConfirmRemoveMembers()
	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaGroupIMSettingWnd:ConfirmRemoveMembers()
    if not CClientMainPlayer.Inst then return end
    local id = CClientMainPlayer.Inst.Id
    for i = 0, self.TableView.Count - 1 do
        local cmp =  TypeAs(self.TableView:GetItemAtRow(i), typeof(CGroupIMSettingItem))
        if cmp then
            if id ~= cmp.PlayerId and cmp.IsChosen then
                Gac2Gas.KickPlayerFromGroup(CGroupIMMgr.m_SettingWndGroupIMId, cmp.PlayerId)
            end
        end
    end
end

function LuaGroupIMSettingWnd:OnChangeGroupNameButtonClick()
    CInputBoxMgr.ShowInputBox(LocalString.GetString("群组名称"),DelegateFactory.Action_string(function (str) 
        self:ChangeGroupName(str)
    end), CGroupIMMgr.MaxGroupNameLen*2)
end

function LuaGroupIMSettingWnd:ChangeGroupName(str)
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(str)
    if not ret.msg or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("Name_Violation")
        return
    end
    Gac2Gas.ChangeGroupName(CGroupIMMgr.m_SettingWndGroupIMId, str)
end

function LuaGroupIMSettingWnd:OnChangeMottoBtnClick()
    local msg = ""
    local info = CGroupIMMgr.Inst:GetGroupIM(CGroupIMMgr.m_SettingWndGroupIMId)
    if info then
        msg = info.GroupAnnouncement.StringData
    end
    CInputWndMgr.ShowInputWnd(DelegateFactory.Action_string(function (str) 
        self:ChangeGroupAnnouncement(str)
    end),CGroupIMMgr.MaxAnnouncementLen * 2,msg,nil,LocalString.GetString("群公告"),LocalString.GetString("保存修改"),
    DelegateFactory.Action_string(function (str) 
        local warning = g_MessageMgr:FormatMessage("CHANGE_NOTHING")
        MessageWndManager.ShowOKCancelMessage(warning,DelegateFactory.Action(function()
            self:ChangeGroupAnnouncement(str)
            CUIManager.CloseUI(CIndirectUIResources.InputWnd)
        end),DelegateFactory.Action(function()
            CUIManager.CloseUI(CIndirectUIResources.InputWnd)
        end),LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    end))
end

function LuaGroupIMSettingWnd:ChangeGroupAnnouncement(str)
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(str)
    if not ret.msg or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end
    Gac2Gas.ChangeGroupAnnouncement(CGroupIMMgr.m_SettingWndGroupIMId, str)
end

function LuaGroupIMSettingWnd:SetGroupIMIgnoreNewMsg()
    if not CClientMainPlayer.Inst then return end
    local value = false
    if CommonDefs.DictContains(CClientMainPlayer.Inst.RelationshipProp.JoinedGroupIM, typeof(ulong), CGroupIMMgr.m_SettingWndGroupIMId) and
    CClientMainPlayer.Inst.RelationshipProp.JoinedGroupIM[CGroupIMMgr.m_SettingWndGroupIMId].IgnoreNewMsg == 0 then
        value = true
    end
    if value ~= self.AlertToggle.value then
        Gac2Gas.SetGroupIMIgnoreNewMsg(CGroupIMMgr.m_SettingWndGroupIMId, not self.AlertToggle.value)
    end
end

function LuaGroupIMSettingWnd:OnShiTuTeamButtonClick()
    Gac2Gas.InviteJoinTeamFromGroup(CGroupIMMgr.m_SettingWndGroupIMId)
end
--@endregion UIEvent

