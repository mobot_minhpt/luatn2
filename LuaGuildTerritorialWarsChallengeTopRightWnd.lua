local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"

LuaGuildTerritorialWarsChallengeTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsChallengeTopRightWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsChallengeTopRightWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsChallengeTopRightWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsChallengeTopRightWnd, "MapBtn", "MapBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsChallengeTopRightWnd, "m_Tick")

function LuaGuildTerritorialWarsChallengeTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    UIEventListener.Get(self.MapBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMapBtnClick()
	end)
end

function LuaGuildTerritorialWarsChallengeTopRightWnd:Init()
    self.Template.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Grid.transform)
    for i = 1, 5 do
        local item = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
        self:InitTemplate(item,i )
    end
    self.Grid:Reposition()
    self:CancelTick()
    self.m_Tick = RegisterTick(function()
		LuaGuildTerritorialWarsMgr:ShowChallengePlayView()
	end,500)
end

function LuaGuildTerritorialWarsChallengeTopRightWnd:InitTemplate(item, index)
    item.gameObject:SetActive(true)
    local texture = item.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local finished = item.transform:Find("Finished")
    local locked = item.transform:Find("Locked")
    local battling = item.transform:Find("Battling")

    local isKill = index < LuaGuildTerritorialWarsMgr.m_MaxBossIdx
    local isBattle = index == LuaGuildTerritorialWarsMgr.m_MaxBossIdx
    local islock = index > LuaGuildTerritorialWarsMgr.m_MaxBossIdx

    locked.gameObject:SetActive(islock)
    finished.gameObject:SetActive(isKill)
    Extensions.SetLocalRotationZ(texture.transform, isKill and -1 or 0)
    texture.color = islock and Color.black or Color.white
    texture.alpha = isKill and 0.6 or (islock and 0.8 or 1)
    local data = GuildTerritoryWar_ChallengeBoss.GetData(index)
    local monsterId = LuaGuildTerritorialWarsMgr:IsEliteChallengePlayOpen() and data.EliteMonsterId or data.NormalMonsterId
    local monsterData = Monster_Monster.GetData(monsterId)
    texture:LoadNPCPortrait(monsterData and monsterData.HeadIcon or nil)
    battling.gameObject:SetActive(isBattle)
end

function LuaGuildTerritorialWarsChallengeTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SendGTWChallengePlayInfo", self, "OnSendGTWChallengePlayInfo")
end

function LuaGuildTerritorialWarsChallengeTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SendGTWChallengePlayInfo", self, "OnSendGTWChallengePlayInfo")
    self:CancelTick()
end

function LuaGuildTerritorialWarsChallengeTopRightWnd:OnSendGTWChallengePlayInfo()
    self:Init()
end

function LuaGuildTerritorialWarsChallengeTopRightWnd:CancelTick()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end

function LuaGuildTerritorialWarsChallengeTopRightWnd:OnHideTopAndRightTipWnd( )
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end


--@region UIEvent
function LuaGuildTerritorialWarsChallengeTopRightWnd:OnMapBtnClick()
    Gac2Gas.RequestGTWChallengeBattleFieldInfo()
end
--@endregion UIEvent

