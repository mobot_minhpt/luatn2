local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CScheduleMgr=import "L10.Game.CScheduleMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CMessageTipMgr    = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaShengXiaoCardEntryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengXiaoCardEntryWnd, "MatchButton1", "MatchButton1", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "MatchButton2", "MatchButton2", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "Buttons", "Buttons", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "IsMatching", "IsMatching", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "CancelMatchButton", "CancelMatchButton", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "RuleAlert", "RuleAlert", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "GetReward", "GetReward", GameObject)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaShengXiaoCardEntryWnd, "Reward", "Reward", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaShengXiaoCardEntryWnd, "m_PlayData")

function LuaShengXiaoCardEntryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.MatchButton1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMatchButton1Click()
	end)

	UIEventListener.Get(self.MatchButton2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMatchButton2Click()
	end)

	UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleButtonClick()
	end)

	UIEventListener.Get(self.RankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

	UIEventListener.Get(self.CancelMatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelMatchButtonClick()
	end)


    --@endregion EventBind end

    local tf = self.transform:Find("Rule")
    local table = tf:Find("TableBody/Table").gameObject
	Extensions.RemoveAllChildren(table.transform)
    local tempalte = tf:Find("ParagraphTemplate").gameObject
    tempalte:SetActive(false)


    local times = ShengXiaoCard_Setting.GetData().ReportTimesLimit-CScheduleMgr.Inst:GetScheduleTimeFromPlayProp(219)
    local msg =	g_MessageMgr:FormatMessage("ShengXiaoCard_Rule",times)
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info then
        for i = 0,info.paragraphs.Count - 1 do
            local paragraphGo = CUICommonDef.AddChild(table.gameObject, tempalte)
            paragraphGo:SetActive(true)
            paragraphGo:GetComponent(typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
        end
        -- tf:Find("TableBody"):GetComponent(typeof(UIScrollView)):ResetPosition()
        table:GetComponent(typeof(UITable)):Reposition()
    end
    local timeStr = ShengXiaoCard_Season.GetData(1001).SeasonTime
    local a,b,c,d = string.match(timeStr,"(%d+).(%d+)-(%d+).(%d+)" )
    self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("%s年%s月%s日至%s月%s日"),"2022",a,b,c,d)
    UIEventListener.Get(self.Reward).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 21041266
        CItemInfoMgr.ShowLinkItemTemplateInfo(21041266)
    end)


    Gac2Gas.WHCB_QueryReward()
    self.GetReward:SetActive(false)

    if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_RuleAlert) then
        self.RuleAlert:SetActive(true)
    else
        self.RuleAlert:SetActive(false)
    end

end

function LuaShengXiaoCardEntryWnd:Init()
    self.Buttons:SetActive(true)
    self.IsMatching:SetActive(false)
    Gac2Gas.ShengXiaoCard_CheckInMatching()
end

--@region UIEvent

function LuaShengXiaoCardEntryWnd:OnMatchButton1Click()
    Gac2Gas.ShengXiaoCard_JokePlay()
end

function LuaShengXiaoCardEntryWnd:OnMatchButton2Click()
    Gac2Gas.ShengXiaoCard_SignUpPlay()
end

function LuaShengXiaoCardEntryWnd:OnRuleButtonClick()
    LuaShengXiaoCardMgr.ShowTipWnd()

    if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_RuleAlert) then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_RuleAlert)
        self.RuleAlert:SetActive(false)
    end
end

function LuaShengXiaoCardEntryWnd:OnRankButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardRankWnd)
end


function LuaShengXiaoCardEntryWnd:OnCancelMatchButtonClick()
    Gac2Gas.ShengXiaoCard_CancelSignUp()
end


--@endregion UIEvent

function LuaShengXiaoCardEntryWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("WHCB_QueryRewardResult", self, "OnWHCB_QueryRewardResult")
    
end
function LuaShengXiaoCardEntryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:RemoveListener("WHCB_QueryRewardResult", self, "OnWHCB_QueryRewardResult")
end

function LuaShengXiaoCardEntryWnd:OnWHCB_QueryRewardResult(data_U)
    self.m_PlayData = {}
    local dict = MsgPackImpl.unpack(data_U)
    CommonDefs.DictIterate(dict,DelegateFactory.Action_object_object(function (___key, ___value) 
        -- print(___key,___value)
        --Win Lose Escape Reward
        self.m_PlayData[tostring(___key)]=tonumber(___value)
    end))
    if self.m_PlayData["Reward"] and self.m_PlayData["Reward"]>0 then
        self.GetReward:SetActive(true)
    end

end

function LuaShengXiaoCardEntryWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId==LuaShengXiaoCardMgr.PlayID then
        if isInMatching then
            self.Buttons:SetActive(false)
            self.IsMatching:SetActive(true)
        else
            self.Buttons:SetActive(true)
            self.IsMatching:SetActive(false)
        end
    end
end
function LuaShengXiaoCardEntryWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId==LuaShengXiaoCardMgr.PlayID and success==true then
        self.Buttons:SetActive(false)
        self.IsMatching:SetActive(true)
    end
end
function LuaShengXiaoCardEntryWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId==LuaShengXiaoCardMgr.PlayID and success==true then
        self.Buttons:SetActive(true)
        self.IsMatching:SetActive(false)
    end
end
