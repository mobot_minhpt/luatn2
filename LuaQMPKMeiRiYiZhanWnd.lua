local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession=import "L10.Game.Profession"
local CChatLinkMgr=import "CChatLinkMgr"
local UICamera=import "UICamera"
local CUITexture=import "L10.UI.CUITexture"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

CLuaQMPKMeiRiYiZhanWnd=class()
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_MyRankLabel")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_MyNameLabel")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_MyCountLabel")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_MyPlayerId")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_MyClassSprite")


RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_RankDataSource")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_RankTableView")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_RankList")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_IsMatching")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_MatchButton")

RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_LogItem")

RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_Awards")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_Sliders")
RegistClassMember(CLuaQMPKMeiRiYiZhanWnd,"m_Dots")


function CLuaQMPKMeiRiYiZhanWnd:SetLianShengCount(count)
    --进度都为0
    for i,v in ipairs(self.m_Sliders) do
        v.value=0
    end

    local counts={3,5,10,15,20}
    for i,v in ipairs(counts) do
        if count>= v then
            if self.m_Sliders[i] then
                local val=(count-v)/(counts[i+1]-v)

                self.m_Sliders[i].value=val
            end
            self.m_Awards[i]:LoadMaterial("UI/Texture/Transparent/Material/shanhetuwnd_box_open.mat",true)

            self.m_Dots[i].spriteName="qinghua_icon_3"
        else
            self.m_Dots[i].spriteName="qinghua_icon_2"
            self.m_Awards[i]:LoadMaterial("UI/Texture/Transparent/Material/shanhetuwnd_box_close.mat",false)
        end
    end

    self.m_MyCountLabel.text=tostring(count)
end
function CLuaQMPKMeiRiYiZhanWnd:Init()
    Gac2Gas.RequestQmpkMeiRiYiZhanInfo()

    local parent=FindChild(self.transform,"LianSheng")
    self.m_Awards={}
    local tf=FindChild(parent,"Awards")
    for i=1,5 do
        self.m_Awards[i]=tf:GetChild(i-1):GetComponent(typeof(CUITexture))
    end
    self.m_Sliders={}
    tf=FindChild(parent,"Sliders")
    for i=1,4 do
        self.m_Sliders[i]=tf:GetChild(i-1):GetComponent(typeof(UISlider))
    end
    self.m_Dots={}
    tf=FindChild(parent,"Dots")
    for i=1,5 do
        self.m_Dots[i]=tf:GetChild(i-1):GetComponent(typeof(UISprite))
    end




    local rankTf=FindChild(self.transform,"Rank")
    self.m_MyRankLabel=FindChild(self.transform,"MyRankLabel"):GetComponent(typeof(UILabel))
    self.m_MyNameLabel=FindChild(self.transform,"MyNameLabel"):GetComponent(typeof(UILabel))
    self.m_MyCountLabel=FindChild(self.transform,"MyTimeLabel"):GetComponent(typeof(UILabel))
    self.m_MyClassSprite=FindChild(self.transform,"MyClassSprite"):GetComponent(typeof(UISprite))
    self.m_MyCountLabel.text= LocalString.GetString("—")
    self.m_MyRankLabel.text= LocalString.GetString("—")

    
    if CClientMainPlayer.Inst then
        self.m_MyPlayerId=CClientMainPlayer.Inst.Id
        self.m_MyNameLabel.text=CClientMainPlayer.Inst.Name
        self.m_MyClassSprite.spriteName=Profession.GetIcon(CClientMainPlayer.Inst.Class)
    end
    self.m_MatchButton=FindChild(self.transform,"MatchButton").gameObject
    UIEventListener.Get(self.m_MatchButton).onClick=DelegateFactory.VoidDelegate(function(go)
        --按钮文字更换
        if self.m_IsMatching then
            Gac2Gas.RequestCancelSignUpQmpkMeiRiYiZhan()
        else
            Gac2Gas.RequestSignUpQmpkMeiRiYiZhan()
        end
    end)

    UIEventListener.Get(FindChild(self.transform,"TipButton").gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("QMPK_MeiRiYiZhan_Tip")
    end)

    local logView=FindChild(self.transform,"LogView")
    self.m_LogItem=FindChild(logView,"LogItem").gameObject
    self.m_LogItem:SetActive(false)

    --MeiRiYiZhanItemTips	     21030282,21030283,21030284,21030285,21030286
    self:InitAwards()

    local isOpen = CLuaQMPKMgr.IsMeiRiYiZhanOpen()
    local matchTimeLabel = self.transform:Find("Anchor/BottomView/Label"):GetComponent(typeof(UILabel))
    local btnScript = self.m_MatchButton:GetComponent(typeof(UISprite))
    if isOpen then
        matchTimeLabel.color = Color(0,1,65/256)--绿色
        btnScript.spriteName = "common_button_orange_normal"
    else
        matchTimeLabel.color = Color(1,88/256,54/256)--红色
        btnScript.spriteName = "common_button_darkblue_3_n"
    end
end
function CLuaQMPKMeiRiYiZhanWnd:InitAwards()
    local MeiRiYiZhanItemTips={}--{21030816,21030817,21030818,21030819,21030820}

    local tips = QuanMinPK_Setting.GetData().MeiRiYiZhanItemTips
    for i=1,tips.Length do
        table.insert( MeiRiYiZhanItemTips, tips[i-1] )
    end
    -- local MeiRiYiZhanItemTips={21030816,21030817,21030818,21030819,21030820}
    local tf=FindChild(self.transform,"LianSheng")
    local parent=FindChild(tf,"Awards")
    for i=1,5 do
        local child=parent:GetChild(i-1).gameObject
        UIEventListener.Get(child).onClick=DelegateFactory.VoidDelegate(function(go)
            -- MeiRiYiZhanItemTips[i]
            CItemInfoMgr.ShowLinkItemTemplateInfo(MeiRiYiZhanItemTips[i], false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        end)
    end
end


function CLuaQMPKMeiRiYiZhanWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyQmpkMeiRiYiZhanInfo", self, "OnReplyQmpkMeiRiYiZhanInfo")
    g_ScriptEvent:AddListener("JoinQmpkMeiRiYiZhan", self, "OnJoinQmpkMeiRiYiZhan")
    g_ScriptEvent:AddListener("QuitQmpkMeiRiYiZhan", self, "OnQuitQmpkMeiRiYiZhan")

    
end
function CLuaQMPKMeiRiYiZhanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyQmpkMeiRiYiZhanInfo", self, "OnReplyQmpkMeiRiYiZhanInfo")
    g_ScriptEvent:RemoveListener("JoinQmpkMeiRiYiZhan", self, "OnJoinQmpkMeiRiYiZhan")
    g_ScriptEvent:RemoveListener("QuitQmpkMeiRiYiZhan", self, "OnQuitQmpkMeiRiYiZhan")
end

function CLuaQMPKMeiRiYiZhanWnd:OnJoinQmpkMeiRiYiZhan()
    local label=FindChild(self.m_MatchButton.transform,"Label"):GetComponent(typeof(UILabel))
    label.text=LocalString.GetString("取消匹配")
    self.m_IsMatching=true
end
function CLuaQMPKMeiRiYiZhanWnd:OnQuitQmpkMeiRiYiZhan()
    local label=FindChild(self.m_MatchButton.transform,"Label"):GetComponent(typeof(UILabel))
    label.text=LocalString.GetString("开始匹配")
    self.m_IsMatching=false
end

function CLuaQMPKMeiRiYiZhanWnd:OnReplyQmpkMeiRiYiZhanInfo(lianshengCount,rankInfo,dynamicInfo,isMatching)
    self.m_IsMatching=isMatching>0
    local label=FindChild(self.m_MatchButton.transform,"Label"):GetComponent(typeof(UILabel))
    if isMatching>0 then
        label.text=LocalString.GetString("取消匹配")
    else
        label.text=LocalString.GetString("开始匹配")
    end

    FindChild(self.transform,"LianShengLabel"):GetComponent(typeof(UILabel)).text=tostring(lianshengCount)
    self:SetLianShengCount(lianshengCount)

    self.m_RankList=rankInfo
    local tableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitRankItem(item,index)
    end

    local dataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    tableView.m_DataSource=dataSource
    tableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    dataSource.count=#self.m_RankList
    for i,v in ipairs(self.m_RankList) do
        if v.playerId==self.m_MyPlayerId then
            self.m_MyNameLabel.text=v.name
            self.m_MyCountLabel.text=tostring(v.count)
            self.m_MyRankLabel.text=tostring(i)
            self.m_MyClassSprite=Profession.GetIconByNumber(v.class)
        end
    end
    tableView:ReloadData(true,false)

    local parent=self.m_LogItem.transform.parent:Find("Grid")
    for i,v in ipairs(dynamicInfo) do
        local go=NGUITools.AddChild(parent.gameObject,self.m_LogItem)
        go:SetActive(true)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text=CChatLinkMgr.TranslateToNGUIText(v.msg,false)
        UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(g)
            self:OnClickLog(g)
        end)
    end
    parent:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaQMPKMeiRiYiZhanWnd:OnClickLog(g)
    local label=g.transform:Find("Label"):GetComponent(typeof(UILabel))
    local url=label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url then
        CChatLinkMgr.ProcessLinkClick(url,nil)
    end
end


function CLuaQMPKMeiRiYiZhanWnd:InitRankItem(item,index)
    local tf=item.transform
    local rankLabel=FindChild(tf,"RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=FindChild(tf,"RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local countLabel=FindChild(tf,"CountLabel"):GetComponent(typeof(UILabel))

    local classSprite=FindChild(tf,"ClassSprite"):GetComponent(typeof(UISprite))
    
    local info=self.m_RankList[index+1]
    countLabel.text=tostring(info.count)
    nameLabel.text=info.name
    classSprite.spriteName=Profession.GetIconByNumber(info.class)

    local rank=index+1
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    if info.playerId==self.m_MyPlayerId then
        self.m_MyNameLabel.text=info.name
        self.m_MyCountLabel.text=tostring(info.count)
        self.m_MyRankLabel.text=tostring(rank)
        self.m_MyClassSprite=Profession.GetIconByNumber(info.class)
    end
end
function CLuaQMPKMeiRiYiZhanWnd:OnSelectAtRow(row)
    local data=self.m_RankList[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end

function CLuaQMPKMeiRiYiZhanWnd:OnDestroy()
    --关掉界面就取消吧
    -- Gac2Gas.RequestCancelSignUpQmpkMeiRiYiZhan()
end
return CLuaQMPKMeiRiYiZhanWnd