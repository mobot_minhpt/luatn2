LuaPinchFaceBtn = class()

function LuaPinchFaceBtn:Awake()
    CommonDefs.AddOnPressListener(self.gameObject, DelegateFactory.Action_GameObject_bool(function (go, isPressed)
        if isPressed then
            g_ScriptEvent:BroadcastInLua("OnShowPinchFaceClickFx")
        end
    end),true)
end
