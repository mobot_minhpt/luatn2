local QnButton=import "L10.UI.QnButton"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CCommonSelector = import "L10.UI.CCommonSelector"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local QnTabView = import "L10.UI.QnTabView"
EnumStarBiwuFightXZSStatus = {
	-- 左边和右边相对客户端界面而言,索引小的是左边
	eNotBegin 				= 0, -- 尚未开始
	eNoResultYet 			= 1, -- 结果还没出来,还在打
	eLeftWinByFight 		= 2, -- 双方没有弃权左边打赢
	eRightWinByFight 		= 3, -- 双方没有弃权右边打赢
	eEvenBothQuit 			= 4, -- 双方弃权导致打平
	eLeftWinByEnemyQuit 	= 5, -- 右边弃权导致左边赢
	eRightWinByEnemyQuit 	= 6, -- 左边弃权导致右边赢
}
EnumStarBiwuFightXZSStage = {
	-- 控制客户端小组赛页面标题的显示
	eBeforeMatchDay 		= 0, -- 比赛日之前
	eMatchDayBeforeRound 	= 1, -- 比赛日,轮次开始之前
	eRound1NoResultYet 		= 2, -- 第一轮刚开始,结果还没出来
	eRounding				= 3, -- 轮次正在进行中,第一轮结果已经刷出来了
	eRoundEnd				= 4, -- 所有轮次结束
}
CLuaStarBiwuGroupMatchWnd = class()
CLuaStarBiwuGroupMatchWnd.Path = "ui/starbiwu/LuaStarBiwuGroupMatchWnd"
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_GroupAdvGridView")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_RecordAdvGridView")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_GroupDataTable")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_RecordDataTable")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_GroupNameTable")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_WatchBtn")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_RecordSelector")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_CurrentRecordGroup")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_CurrentRecordRow")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_TipLabel")
RegistClassMember(CLuaStarBiwuGroupMatchWnd, "m_CurrentTabIndex")

function CLuaStarBiwuGroupMatchWnd:Awake()
  self.m_GroupNameTable = {LocalString.GetString("甲"), LocalString.GetString("乙"), LocalString.GetString("丙"), LocalString.GetString("丁")}
	self.m_CurrentRecordGroup = 1
  Gac2Gas.QueryStarBiwuXiaoZuSaiOverview(1, 1)
end

function CLuaStarBiwuGroupMatchWnd:OnEnable()
  self.m_GroupAdvGridView = self.transform:Find("RankRoot/ContentArea/AdvView"):GetComponent(typeof(QnAdvanceGridView))
  self.m_RecordAdvGridView = self.transform:Find("WatchRoot/QnAdvView"):GetComponent(typeof(QnAdvanceGridView))
  self.m_GroupAdvGridView.transform:Find("Pool/TeamDetailItem").gameObject:SetActive(false)
  self.m_WatchBtn = self.transform:Find("WatchRoot/ViewButton"):GetComponent(typeof(QnButton))
	UIEventListener.Get(self.m_WatchBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
		self:OnWatchBtnClicked(go)
	end)
  self.m_RecordSelector = self.transform:Find("WatchRoot/TopArea/Selector"):GetComponent(typeof(CCommonSelector))
	self.m_TipLabel = self.transform:Find("RankRoot/TopArea/TipLabel"):GetComponent(typeof(UILabel))
	self.m_CurrentTabIndex = 0
	self.transform:Find("Wnd_Bg_Primary_Tab/Tabs"):GetComponent(typeof(QnTabView)).OnSelect = DelegateFactory.Action_QnTabButton_int(function(tab, index)
		self.m_CurrentTabIndex = index
		if index == 1 then
			self:InitRecord()
		end
	end)
  UIEventListener.Get(self.transform:Find("WatchRoot/RefreshButton").gameObject).onClick = LuaUtils.VoidDelegate(function(go)
      Gac2Gas.QueryStarBiwuXiaoZuSaiOverview(2, self.m_CurrentRecordGroup)
  end)
	UIEventListener.Get(self.transform:Find("RankRoot/TopArea/HintButton").gameObject).onClick = LuaUtils.VoidDelegate(function(go)
		g_MessageMgr:ShowMessage("StarBiwu_XiaoZuSai_Rule")
	end)
  UIEventListener.Get(self.transform:Find("WatchRoot/OpenDataAnalysisButton").gameObject).onClick = LuaUtils.VoidDelegate(function(go)
		self:OpenDataAnalysisWnd()
	end)

  g_ScriptEvent:AddListener("ReplyStarBiwuXiaoZuSaiSaiOverView", self, "ReplyStarBiwuXiaoZuSaiSaiOverView")
end

function CLuaStarBiwuGroupMatchWnd:OpenDataAnalysisWnd()
  local data = self.m_RecordDataTable[self.m_CurrentRecordGroup][self.m_CurrentRecordRow]
  if data then --TODO 一场结束的比赛
      --CLuaStarBiwuMgr:OpenStarBiwuDataAnalysisWnd(data.aliasName)
  end
end

function CLuaStarBiwuGroupMatchWnd:OnDisable()
  g_ScriptEvent:RemoveListener("ReplyStarBiwuXiaoZuSaiSaiOverView", self, "ReplyStarBiwuXiaoZuSaiSaiOverView")
end

function CLuaStarBiwuGroupMatchWnd:GetTipString(queryType, group, round, xiaozusaiStage)
	if queryType == 1 then
		if xiaozusaiStage <= EnumStarBiwuFightXZSStage.eBeforeMatchDay then
			return g_MessageMgr:FormatMessage("StarBiwu_XiaoZuSai_Msg1")
		else
			if group <= 0 then
				return g_MessageMgr:FormatMessage("StarBiwu_XiaoZuSai_Msg2")
			else
				if xiaozusaiStage < EnumStarBiwuFightXZSStage.eRoundEnd then
					return g_MessageMgr:FormatMessage("StarBiwu_XiaoZuSai_Msg"..(group * 2 + 1))
				else
					return g_MessageMgr:FormatMessage("StarBiwu_XiaoZuSai_Msg"..(group * 2 + 2))
				end
			end
		end
	end
	return ""
end

function CLuaStarBiwuGroupMatchWnd:ReplyStarBiwuXiaoZuSaiSaiOverView(queryType, group, round, xiaozusaiStage, queryGroup, group1Ud, group2Ud, groud3Ud, group4Ud, extraUd)
  local groupUDTbl = {group1Ud, group2Ud, groud3Ud, group4Ud}
  if queryType == 1 then
		self.m_TipLabel.text = self:GetTipString(queryType, group, round, xiaozusaiStage)
    self.m_GroupDataTable = {}
    for i = 1, 4 do
      local tbl = {}
      local data = MsgPackImpl.unpack(groupUDTbl[i])
      if data then
        for j = 0, data.Count - 1, 7 do
          local cleanScore = data[j + 6]
          table.insert(tbl, {CleanScore = cleanScore,CurrentRank = data[j + 1], Name = data[j + 2], JiFenRank = data[j + 3], CurrentWinCount = data[j + 5], ZhanduiId = data[j], RetainFlag = data[j + 4]})
        end
      end
			table.insert(self.m_GroupDataTable, tbl)
    end
    self:InitRank(group, xiaozusaiStage)
		self.m_CurrentRecordGroup = group
    Gac2Gas.QueryStarBiwuXiaoZuSaiOverview(2, group)
  elseif queryType == 2 then
    self.m_RecordDataTable = {{}, {}, {}, {}}
    for i = 1, 4 do
      local tbl = {}
      local data = MsgPackImpl.unpack(groupUDTbl[i])
      if data then
        local index = 1
        for j = 0, data.Count - 1, 6 do
					local curRound = math.ceil(index / 2)
          local tbl = {ZhanduiId1 = data[j], ZhanduiId2 = data[j + 1], ZhanduiName1 = data[j + 2], ZhanduiName2 = data[j + 3], Status = data[j + 4], Round = curRound, TimeStamp = data[j + 5]}
          table.insert(self.m_RecordDataTable[i], tbl)
          index = index + 1
        end
        table.sort(self.m_RecordDataTable[i],function(a,b)
              return a.TimeStamp < b.TimeStamp
          end )
      end
    end
		if self.m_CurrentTabIndex == 1 then
    	self:InitRecord()
		end
  end
end

function CLuaStarBiwuGroupMatchWnd:Init()
end

function CLuaStarBiwuGroupMatchWnd:InitRank(group, xiaozusaiStage)
  local getNumFunc = function() return #self.m_GroupDataTable end
  local initItemFunc = function(item, index)
    item.transform:Find("TitleArea/RankLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[c][fcff00]%s组[-][/c]排行"), self.m_GroupNameTable[index + 1])
    for i = 1, 4 do
      local itemRootTrans = item.transform:Find("Item"..i)
			local zhanduiTbl = self.m_GroupDataTable[index + 1][i]
			itemRootTrans.gameObject:SetActive(zhanduiTbl)
			if zhanduiTbl then
	      local transTbl = {"RankLabel", "NameLabel", "JifenRankLabel", "GroupWinCountLabel"}
	      local valueTbl = {zhanduiTbl.CurrentRank, zhanduiTbl.Name, zhanduiTbl.CleanScore, zhanduiTbl.CurrentWinCount}
	      for j = 1, #transTbl do
					local label = itemRootTrans:Find(transTbl[j]):GetComponent(typeof(UILabel))
					label.text = valueTbl[j]
					if j == 3 and zhanduiTbl.RetainFlag > 0 then
						label.text = LocalString.GetString("保级")
					end
					if j == 1 then
						if zhanduiTbl.CurrentRank == 0 then
							label.text = "-"
						elseif zhanduiTbl.CurrentRank == 1 or zhanduiTbl.CurrentRank == 2 then
							if index + 1 < group or (index + 1 == group and xiaozusaiStage >= EnumStarBiwuFightXZSStage.eRoundEnd) then
								label.text = LocalString.GetString("晋级")
							end
						end
					end
          if j == 2 then
            UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
              CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, zhanduiTbl.ZhanduiId, 1)
            end)
          end
	      end
			end
    end
  end

  self.m_GroupAdvGridView.m_DataSource = DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
  self.m_GroupAdvGridView:ReloadData(true,false)
end

function CLuaStarBiwuGroupMatchWnd:InitRecord()
  self.m_WatchBtn.Enabled = false
  local menuTbl = {LocalString.GetString("甲组"), LocalString.GetString("乙组"), LocalString.GetString("丙组"), LocalString.GetString("丁组")}

  self.m_RecordSelector:Init(Table2Class(menuTbl, MakeGenericClass(List, cs_string)), DelegateFactory.Action_int(function(index)
      self.m_RecordSelector:SetName(menuTbl[index + 1])
      self.m_CurrentRecordGroup = index + 1
			Gac2Gas.QueryStarBiwuXiaoZuSaiOverview(2, self.m_CurrentRecordGroup)
      --self.m_RecordAdvGridView:ReloadData(true, false)
  end))
  if not self.m_RecordDataTable then
    self.m_RecordDataTable = {{}, {}, {}, {}}
  end
  local getNumFunc = function()
    local currentRecordDataTbl = self.m_RecordDataTable[self.m_CurrentRecordGroup]
    if not currentRecordDataTbl then return 0 end
    return #currentRecordDataTbl
  end
  local initItemFunc = function(item, index)
      if index % 2 == 1 then
          item:SetBackgroundTexture("common_bg_mission_background_s")
      else
          item:SetBackgroundTexture("common_bg_mission_background_n")
      end

      self:InitItem(item, index)
  end
  self.m_RecordAdvGridView.m_DataSource = DefaultTableViewDataSource.Create(getNumFunc, initItemFunc)
  self.m_RecordAdvGridView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
      self.m_CurrentRecordRow = row + 1
      if self.m_RecordDataTable[self.m_CurrentRecordGroup][self.m_CurrentRecordRow].Status == EnumStarBiwuFightXZSStatus.eNoResultYet then
          self.m_WatchBtn.Enabled = true
      else
          self.m_WatchBtn.Enabled = false
      end
  end)
  
  if menuTbl[self.m_CurrentRecordGroup] == nil then
    self.m_CurrentRecordGroup = 1
    self.m_RecordSelector:OnItemSelected(0)
  else
    self.m_RecordSelector:SetName(menuTbl[self.m_CurrentRecordGroup])
  end
	self.m_RecordAdvGridView:ReloadData(true, false)
end

function CLuaStarBiwuGroupMatchWnd:OnWatchBtnClicked( go)
  local data = self.m_RecordDataTable[self.m_CurrentRecordGroup][self.m_CurrentRecordRow]
  Gac2Gas.RequestWatchStarBiwuXiaoZuSai(data.ZhanduiId1, data.ZhanduiId2, self.m_CurrentRecordGroup, data.Round)
end

function CLuaStarBiwuGroupMatchWnd:InitItem(item, index)
    local trans = item.transform
    local data = self.m_RecordDataTable[self.m_CurrentRecordGroup][index + 1]
    FindChild(trans,"NameLabel1"):GetComponent(typeof(UILabel)).text = data.ZhanduiName1
    FindChild(trans,"NameLabel2"):GetComponent(typeof(UILabel)).text = data.ZhanduiName2
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(data.TimeStamp)
    trans:Find("GroupLabel"):GetComponent(typeof(UILabel)).text = ToStringWrap(time, "MM.dd HH:mm")
    local m_ResultSprite={}
    local resultSprite1 = FindChild(trans, "ResultSprite1"):GetComponent(typeof(UISprite))
    local resultSprite2 = FindChild(trans, "ResultSprite2"):GetComponent(typeof(UISprite))
		local m_CannotWatchObj = trans:Find("CannotWatchMark").gameObject
    UIEventListener.Get(item.transform:Find("TeamItem1/NameLabel1").gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
      CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, data.ZhanduiId1, 1)
    end)
    UIEventListener.Get(item.transform:Find("TeamItem2/NameLabel2").gameObject).onClick = DelegateFactory.VoidDelegate(function(go) 
        CLuaStarBiwuMgr:ShowZhanDuiMemberWnd(0, data.ZhanduiId2, 1)
    end)

    local cannotWatchObj = trans:Find("CannotWatchMark").gameObject
    local matchingLabel = trans:Find("Label"):GetComponent(typeof(UILabel))
    if data.Status == EnumStarBiwuFightXZSStatus.eNotBegin then
      matchingLabel.gameObject:SetActive(true)
      matchingLabel.text = LocalString.GetString("准备中")
      m_CannotWatchObj:SetActive(true)
      resultSprite1.gameObject:SetActive(false)
      resultSprite2.gameObject:SetActive(false)
    elseif data.Status == EnumStarBiwuFightXZSStatus.eNoResultYet then
      matchingLabel.gameObject:SetActive(true)
      matchingLabel.text = LocalString.GetString("进行中")
      m_CannotWatchObj:SetActive(false)
      resultSprite1.gameObject:SetActive(false)
      resultSprite2.gameObject:SetActive(false)
    else
      matchingLabel.gameObject:SetActive(false)
      m_CannotWatchObj:SetActive(true)
      resultSprite1.gameObject:SetActive(true)
      resultSprite2.gameObject:SetActive(true)
      local spritename1
      local spritename2
      if data.Status == EnumStarBiwuFightXZSStatus.eEvenBothQuit then
        spritename1 = "common_fight_result_tie"
        spritename2 = spritename1
      elseif data.Status == EnumStarBiwuFightXZSStatus.eLeftWinByFight or data.Status == EnumStarBiwuFightXZSStatus.eLeftWinByEnemyQuit then
        spritename1 = "common_fight_result_win"
        spritename2 = "common_fight_result_lose"
      else
        spritename1 = "common_fight_result_lose"
        spritename2 = "common_fight_result_win"
      end
      resultSprite1.spriteName = spritename1
      resultSprite2.spriteName = spritename2
    end
end
