local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local BoxCollider = import "UnityEngine.BoxCollider"
local CChatLinkMgr = import "CChatLinkMgr"
local Ease = import "DG.Tweening.Ease"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaCHChooseGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
-- 主播列表
RegistChildComponent(LuaCHChooseGiftWnd, "BroadcasterGrid", UIGrid)     
RegistChildComponent(LuaCHChooseGiftWnd, "BroadcasterTemplate", GameObject)
-- 礼物列表
RegistChildComponent(LuaCHChooseGiftWnd, "GiftGrid", UIGrid)            
RegistChildComponent(LuaCHChooseGiftWnd, "ItemCell", GameObject)
-- 礼物信息
RegistChildComponent(LuaCHChooseGiftWnd, "GiftNameLabel", UILabel)          
RegistChildComponent(LuaCHChooseGiftWnd, "HotValueLabel", UILabel)
RegistChildComponent(LuaCHChooseGiftWnd, "GiftDescLabel", UILabel)
-- 消耗
RegistChildComponent(LuaCHChooseGiftWnd, "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)          
RegistChildComponent(LuaCHChooseGiftWnd, "QnCostAndOwnMoney", CCurentMoneyCtrl)
-- 按钮
RegistChildComponent(LuaCHChooseGiftWnd, "RuleBtn", GameObject)    
RegistChildComponent(LuaCHChooseGiftWnd, "SubmitBtn", CButton)    
RegistChildComponent(LuaCHChooseGiftWnd, "ComboBtn", GameObject)    
RegistClassMember(LuaCHChooseGiftWnd, "m_ComboLabel")
RegistClassMember(LuaCHChooseGiftWnd, "m_ComboXTexture")
RegistClassMember(LuaCHChooseGiftWnd, "m_ComboCountDown")

RegistClassMember(LuaCHChooseGiftWnd, "m_ComboCountDownTick")
RegistClassMember(LuaCHChooseGiftWnd, "m_SelectGiftId")
RegistClassMember(LuaCHChooseGiftWnd, "m_GiftPrice")
RegistClassMember(LuaCHChooseGiftWnd, "m_GiftCount")
RegistClassMember(LuaCHChooseGiftWnd, "m_SelectBroadcasterAccId")
RegistClassMember(LuaCHChooseGiftWnd, "m_SelectBroadcasterPlayerId")
RegistClassMember(LuaCHChooseGiftWnd, "m_BroadcasterTbl")
RegistClassMember(LuaCHChooseGiftWnd, "m_TotalTickTime")
RegistClassMember(LuaCHChooseGiftWnd, "m_TickBeginTime")
RegistClassMember(LuaCHChooseGiftWnd, "m_TickEndTime")
RegistClassMember(LuaCHChooseGiftWnd, "m_NeedToShowCheck")
RegistClassMember(LuaCHChooseGiftWnd, "m_GiftPriceToCheck")
RegistClassMember(LuaCHChooseGiftWnd, "m_PreSendRPCTime")
RegistClassMember(LuaCHChooseGiftWnd, "m_SendRPCTimeOffset")

RegistClassMember(LuaCHChooseGiftWnd, "m_ComboCount")
RegistClassMember(LuaCHChooseGiftWnd, "m_Time")
--@endregion RegistChildComponent end

function LuaCHChooseGiftWnd:Awake()
    self.m_ComboLabel = self.ComboBtn.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    self.m_ComboXTexture = self.ComboBtn.transform:Find("CountLabel/Texture"):GetComponent(typeof(UITexture))
    self.m_ComboCountDown = self.ComboBtn.transform:Find("Progress"):GetComponent(typeof(UITexture))
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(val)
        self:OnValueChanged(val)
    end)
    UIEventListener.Get(self.SubmitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSubmitBtnClick()
    end)
    UIEventListener.Get(self.ComboBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSubmitBtnClick()
    end)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
end

function LuaCHChooseGiftWnd:Init()
    self.m_PreSendRPCTime = 0
    self.m_SendRPCTimeOffset = 0.2
    self.m_SelectBroadcasterAccId = nil
    self.m_SelectBroadcasterPlayerId = nil
    local setData = GameplayItem_Setting.GetData()
    self.m_TotalTickTime = setData.ClubHouse_ComboDuration
    self.m_GiftPriceToCheck = setData.ClubHouse_GiftPrice_Alert
    self.m_ComboCount = 0
    self:InitBroadcasterGrid()
    self:InitGiftGrid()
    self:UpdateSubmitBtn(false)
end

function LuaCHChooseGiftWnd:OnBroadcasterListUpdate()
    if self.m_SelectBroadcasterAccId then
        local member = nil
        if self.m_SelectBroadcasterAccId == LuaClubHouseMgr.m_CurRoomInfo.Owner.AccId then
            member = LuaClubHouseMgr.m_CurRoomInfo.Owner
        elseif LuaClubHouseMgr.m_CurRoomInfo.MicTbl[self.m_SelectBroadcasterAccId] then
            member = LuaClubHouseMgr.m_CurRoomInfo.Members[self.m_SelectBroadcasterAccId]
        end
        self.m_SelectBroadcasterAccId = member and member.AccId or nil
        self.m_SelectBroadcasterPlayerId = member and member.PlayerId or nil
    end
    self:InitBroadcasterGrid()
end

function LuaCHChooseGiftWnd:InitBroadcasterGrid()
    self.BroadcasterTemplate:SetActive(false)
    self.m_BroadcasterTbl = {}
    Extensions.RemoveAllChildren(self.BroadcasterGrid.transform)
    local broadcasters = LuaClubHouseMgr:GetBroadcasters()
    for i = 1, #broadcasters do
        local go = NGUITools.AddChild(self.BroadcasterGrid.gameObject, self.BroadcasterTemplate)
        table.insert(self.m_BroadcasterTbl, {item = go, accId = broadcasters[i].AccId, playerId = broadcasters[i].PlayerId})
        go:SetActive(true)
        self:InitOneBroadcaster(go, broadcasters[i])
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnBroadcasterClick(i)
        end)
    end
    self.BroadcasterGrid:Reposition()
    self:UpdateSubmitBtn(false)
end

function LuaCHChooseGiftWnd:InitOneBroadcaster(item, member)
    local portrait = item.transform:GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local ownerIcon = item.transform:Find("Owner").gameObject
    local selectedSprite = item.transform:Find("SelectedSprite").gameObject
    portrait:LoadNPCPortrait(member.Portrait, false)
    LuaClubHouseMgr:SetMengDaoProfile(portrait, member.PlayerId, "CHChooseGiftWnd")
    self:WrapText(nameLabel, member.PlayerName, member.IsMe)
    ownerIcon:SetActive(member.AccId == LuaClubHouseMgr:GetRoomOwner().AccId)
    item.transform:Find("SelectedSprite").gameObject:SetActive(false)
    
    if self.m_SelectBroadcasterAccId then
        portrait.alpha = self.m_SelectBroadcasterAccId == member.AccId and 1 or 0.5
        selectedSprite:SetActive(self.m_SelectBroadcasterAccId == member.AccId)
    else
        portrait.alpha = 1
        selectedSprite:SetActive(false)
    end
end

function LuaCHChooseGiftWnd:InitGiftGrid()
    self.ItemCell:SetActive(false)
    Extensions.RemoveAllChildren(self.GiftGrid.transform)
    for i = 1, GameplayItem_ClubHousePresent.GetDataCount() do
        local go = NGUITools.AddChild(self.GiftGrid.gameObject, self.ItemCell)
        go.transform:Find("SelectedSprite").gameObject:SetActive(false)
        go:SetActive(true)
        local data = GameplayItem_ClubHousePresent.GetData(i)
        go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
        local qualityTypePath = SafeStringFormat3("UI/Texture/Transparent/Material/clubhouse_daojukuang_%s.mat", data.QualityType)
        go.transform:Find("QualityTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(qualityTypePath)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnGiftItemClick(i)
        end)
    end
    self.GiftGrid:Reposition()
    self:OnGiftItemClick(1)
end

function LuaCHChooseGiftWnd:UpdateGiftInfo()
    local data = GameplayItem_ClubHousePresent.GetData(self.m_SelectGiftId)
    -- 礼物信息
    self.GiftNameLabel.text = data.Name
    self.HotValueLabel.text = tostring(data.Heat)
    self.GiftDescLabel.text = CChatLinkMgr.TranslateToNGUIText(data.Description)
    -- 礼物售价
    self.m_NeedToShowCheck = data.Jade ~= 0
    self.m_GiftPrice = data.Silver == 0 and data.Jade or data.Silver
    self.QnCostAndOwnMoney:SetType(data.Silver == 0 and EnumMoneyType.LingYu or EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    self.QnIncreseAndDecreaseButton:SetValue(1, true)
end

function LuaCHChooseGiftWnd:UpdateSubmitBtn(newCombo)
    if self.m_ComboCountDownTick then
        UnRegisterTick(self.m_ComboCountDownTick)
    end
    local remainTime = self:GetComboRemainTime()
    if LuaClubHouseMgr.m_GivePresentInfo and LuaClubHouseMgr.m_GivePresentInfo.DstAccId == self.m_SelectBroadcasterAccId
       and self.m_SelectGiftId == LuaClubHouseMgr.m_GivePresentInfo.PresentId and remainTime > 0 then
        self.SubmitBtn.gameObject:SetActive(false)
        self.ComboBtn:SetActive(true)
        
        if LuaClubHouseMgr.m_GivePresentInfo.isNewCombo then self.m_ComboCount = 0 end
        self.m_Time = newCombo and LuaClubHouseMgr.m_GivePresentInfo.Combo - self.m_ComboCount or 0
        self.m_ComboCount = LuaClubHouseMgr.m_GivePresentInfo.Combo
        self.m_Time = self.m_Time > 3 and 3 or self.m_Time
        if self.m_Time > 0 then 
            self:ShowComboTween()
        else
            self.m_ComboLabel.text = tostring(self.m_ComboCount > 9999 and 9999 or self.m_ComboCount)
            self.m_ComboXTexture:ResetAndUpdateAnchors()
        end
        
        self.m_ComboCountDown.fillAmount = remainTime / self.m_TotalTickTime
        self.m_ComboCountDownTick = RegisterTick(function ()
            local remainTime = self:GetComboRemainTime()
            if remainTime > 0 then
                self.m_ComboCountDown.fillAmount = remainTime / self.m_TotalTickTime
            else
                self:UpdateSubmitBtn(false)
            end
        end, 30)
    else
        if remainTime <= 0 then self.m_ComboCount = 0 end
        self.SubmitBtn.gameObject:SetActive(true)
        self.ComboBtn:SetActive(false)
        local posPre = self.SubmitBtn.transform.localPosition
        posPre.z = self.m_SelectBroadcasterAccId and 1 or -1
        self.SubmitBtn.transform.localPosition = posPre
        self.SubmitBtn.Text = self.m_SelectBroadcasterAccId and LocalString.GetString("送出礼物") or LocalString.GetString("未选择佳宾")
        self.SubmitBtn:GetComponent(typeof(BoxCollider)).enabled = self.m_SelectBroadcasterAccId
    end
end

function LuaCHChooseGiftWnd:ShowComboTween()
    LuaTweenUtils.DOKill(self.m_ComboLabel.transform, false)
    if self.m_Time > 0 then
        self.m_Time = self.m_Time - 1
        local comboCount = self.m_ComboCount - self.m_Time
        self.m_ComboLabel.text = tostring(comboCount > 9999 and 9999 or comboCount)
        self.m_ComboXTexture:ResetAndUpdateAnchors()
        local tween = LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.m_ComboLabel.transform, Vector3(4, 4, 4), Vector3(1, 1, 1), 0.5), Ease.OutExpo)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.m_ComboLabel, 0, 1, 0.5), Ease.OutExpo)
        LuaTweenUtils.OnComplete(tween, function()
            self:ShowComboTween()
        end)
    end
end

function LuaCHChooseGiftWnd:GetComboRemainTime()
    if LuaClubHouseMgr.m_GivePresentInfo and LuaClubHouseMgr.m_GivePresentInfo.UpdateTime then
        return self.m_TotalTickTime - (CServerTimeMgr.Inst.timeStamp - LuaClubHouseMgr.m_GivePresentInfo.UpdateTime)
    else
        return 0
    end
end

function LuaCHChooseGiftWnd:OnClubHouseNotifyGivePresent()
    self:UpdateSubmitBtn(true)
end

function LuaCHChooseGiftWnd:WrapText(label, originText, isMe)
    --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
    label.text = originText
    local fit = true
    local outerText = ""
    fit, outerText = label:Wrap(originText)
    if not fit then
        outerText = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
        label.text = isMe and "[c][00ff60]"..outerText.."[-][/c]" or outerText
    elseif isMe then
        label.text = "[c][00ff60]"..originText.."[-][/c]"
    end
end

--@region UIEvent
function LuaCHChooseGiftWnd:OnGiftItemClick(giftId)
    if self.m_SelectGiftId then
        self.GiftGrid.transform:GetChild(self.m_SelectGiftId - 1):Find("SelectedSprite").gameObject:SetActive(false)
    end
    self.m_SelectGiftId = giftId
    self.GiftGrid.transform:GetChild(self.m_SelectGiftId - 1):Find("SelectedSprite").gameObject:SetActive(true)
    self:UpdateGiftInfo()
    self:UpdateSubmitBtn(false)
end

function LuaCHChooseGiftWnd:OnBroadcasterClick(index)
    self.m_SelectBroadcasterAccId = self.m_BroadcasterTbl[index].accId
    self.m_SelectBroadcasterPlayerId = self.m_BroadcasterTbl[index].playerId
    for i = 0, self.BroadcasterGrid.transform.childCount - 1 do
        local item = self.BroadcasterGrid.transform:GetChild(i)
        item:GetComponent(typeof(UITexture)).alpha = i + 1 == index and 1 or 0.5
        item.transform:Find("SelectedSprite").gameObject:SetActive(i + 1 == index)
    end
    self:UpdateSubmitBtn(false)
end

function LuaCHChooseGiftWnd:OnValueChanged(val)
    self.m_GiftCount = val
    self.QnCostAndOwnMoney:SetCost(val * self.m_GiftPrice)
end

function LuaCHChooseGiftWnd:OnSubmitBtnClick()
    -- 客户端提前拦截给自己送礼
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == self.m_SelectBroadcasterPlayerId then
        g_MessageMgr:ShowMessage("CLUBHOUSE_CANNOT_DO_ACTION_TO_SELF")
        return
    end
    if self.m_PreSendRPCTime and CServerTimeMgr.Inst.timeStamp - self.m_PreSendRPCTime < self.m_SendRPCTimeOffset then return end
    if self.m_SelectBroadcasterAccId then
        local cost = self.QnCostAndOwnMoney:GetCost()
        if self.m_NeedToShowCheck and self.m_GiftPriceToCheck <= cost then
            local msg = g_MessageMgr:FormatMessage("ClubHouse_GiftPrice_Notice_Message", tostring(cost))
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                self.m_PreSendRPCTime = CServerTimeMgr.Inst.timeStamp
                Gac2Gas.ClubHouse_GivePresent(self.m_SelectBroadcasterAccId, self.m_SelectGiftId, self.m_GiftCount)
            end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
        else
            self.m_PreSendRPCTime = CServerTimeMgr.Inst.timeStamp
            Gac2Gas.ClubHouse_GivePresent(self.m_SelectBroadcasterAccId, self.m_SelectGiftId, self.m_GiftCount)
        end
    else
        g_MessageMgr:ShowMessage("CLUBHOUSE_NEED_CHOOSENGUEST")
    end
end

function LuaCHChooseGiftWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("CLUBHOUSE_TIPS_GIFT")
end

--@endregion UIEvent

function LuaCHChooseGiftWnd:OnEnable()
    g_ScriptEvent:AddListener("ClubHouse_Notify_GivePresent_Me", self, "OnClubHouseNotifyGivePresent")
    g_ScriptEvent:AddListener("ClubHouse_RoomContentView_Broadcaster_Update", self, "OnBroadcasterListUpdate")
end

function LuaCHChooseGiftWnd:OnDisable()
    if self.m_ComboCountDownTick then
        UnRegisterTick(self.m_ComboCountDownTick)
    end
    if self.m_ComboLabel then
        LuaTweenUtils.DOKill(self.m_ComboLabel.transform, false)
    end
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_GivePresent_Me", self, "OnClubHouseNotifyGivePresent")
    g_ScriptEvent:RemoveListener("ClubHouse_RoomContentView_Broadcaster_Update", self, "OnBroadcasterListUpdate")
end