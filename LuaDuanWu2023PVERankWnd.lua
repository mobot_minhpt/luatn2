local QnAdvanceGridView      = import "L10.UI.QnAdvanceGridView"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CPlayerInfoMgr         = import "CPlayerInfoMgr"
local CRankData              = import "L10.UI.CRankData"
local EnumPlayerInfoContext  = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel             = import "L10.Game.EChatPanel"
local AlignType              = import "CPlayerInfoMgr+AlignType"
local Profession             = import "L10.Game.Profession"
local CServerTimeMgr         = import "L10.Game.CServerTimeMgr"
local Item_Item              = import "L10.Game.Item_Item"

LuaDuanWu2023PVERankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2023PVERankWnd, "returnAward1", "ReturnAward1", CQnReturnAwardTemplate)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "returnAward2", "ReturnAward2", CQnReturnAwardTemplate)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "tableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "myRank_RankNum", "Rank", UILabel)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "myRank_Name", "Name", UILabel)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "myRank_KillBossNum", "KillBossNum", UILabel)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "myRank_Class", "Class", UISprite)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "myRank_Guild", "Guild", UILabel)
RegistChildComponent(LuaDuanWu2023PVERankWnd, "leftTime", "LeftTime", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaDuanWu2023PVERankWnd, "rankList")
RegistClassMember(LuaDuanWu2023PVERankWnd, "rankSpriteTbl")

function LuaDuanWu2023PVERankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaDuanWu2023PVERankWnd:OnEnable()
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaDuanWu2023PVERankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaDuanWu2023PVERankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    self.myRank_RankNum.text = myInfo.Rank > 0 and myInfo.Rank or LocalString.GetString("未上榜")
    self.myRank_KillBossNum.text = myInfo.Value
    self.myRank_Guild.text = myInfo.Guild_Name

    self.rankList = {}
    for i = 1, CRankData.Inst.RankList.Count do
        table.insert(self.rankList, CRankData.Inst.RankList[i - 1])
    end
    self.tableView:ReloadData(true, false)
end

function LuaDuanWu2023PVERankWnd:Init()
    local setting = Duanwu2023_EndlessChallengeSetting.GetData()
    self.myRank_Name.text = CClientMainPlayer.Inst.Name
    self.myRank_Class.spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)
    self.myRank_Guild.text = ""
    self.myRank_RankNum.text = "-"
    self.myRank_KillBossNum.text = "-"

    self:InitTableView()
    Gac2Gas.QueryRank(setting.RankId)

    local minute, hour, day, month, year = string.match(setting.RankRewardCronStr, "(%d+) (%d+) (%d+) (%d+) .+ (%d+)")
    local timeStr = SafeStringFormat3("%d-%d-%d %d:%d", year, month, day, hour, minute)
    local diff = CServerTimeMgr.Inst:GetTimeStampByStr(timeStr) - CServerTimeMgr.Inst.timeStamp
    local day = 0
    if diff > 0 then
        day = math.ceil(diff / 3600 / 24)
    end
    self.leftTime.text = SafeStringFormat3(LocalString.GetString("%d天后根据排名发放奖励"), day)

    local rewardRank = {}
	for rank in string.gmatch(setting.RankRewardInfos, "(%d+)[%d,%s]*") do
		table.insert(rewardRank, tonumber(rank))
	end
    local rewardItemId = {}
    for itemId in string.gmatch(setting.RankRewardItemId, "(%d+)") do
		table.insert(rewardItemId, tonumber(itemId))
	end
    self.returnAward1:Init(Item_Item.GetData(rewardItemId[1]), 0)
    self.returnAward1.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("前%d"), rewardRank[1])
    self.returnAward2:Init(Item_Item.GetData(rewardItemId[2]), 0)
    self.returnAward2.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("前%d"), rewardRank[2])
end

function LuaDuanWu2023PVERankWnd:InitTableView()
    self.rankList = {}
    self.rankSpriteTbl = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    self.tableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.rankList
        end,
        function(item, index)
            self:InitItem(item, index)
        end
    )
    self.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.rankList[row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)
end

function LuaDuanWu2023PVERankWnd:InitItem(item, index)
    item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
    local tf = item.transform

    local name = tf:Find("Name"):GetComponent(typeof(UILabel))
    local rankNum = tf:Find("Rank"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("Rank/Sprite"):GetComponent(typeof(UISprite))
    local killBossNum = tf:Find("KillBossNum"):GetComponent(typeof(UILabel))
    local guild = tf:Find("Guild"):GetComponent(typeof(UILabel))
    local class = tf:Find("Class"):GetComponent(typeof(UISprite))

    local info = self.rankList[index + 1]
    local rank = info.Rank

    rankNum.text = ""
    rankSprite.spriteName = ""
    if rank >= 1 and rank <= 3 then
        rankSprite.spriteName = self.rankSpriteTbl[rank]
    else
        rankNum.text = rank
    end

    name.text = info.Name
    guild.text = info.Guild_Name
    killBossNum.text = info.Value
    class.spriteName = Profession.GetIcon(info.Job)
end

--@region UIEvent

--@endregion UIEvent
