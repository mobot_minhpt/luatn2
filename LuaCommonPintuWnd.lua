require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local Vector3 = import "UnityEngine.Vector3"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local UIRoot = import "UIRoot"
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
local Rect = import "UnityEngine.Rect"
local ZhuJueJuQing_CommonPinTu = import "L10.Game.ZhuJueJuQing_CommonPinTu"

CLuaCommonPintuWnd=class()
RegistClassMember(CLuaCommonPintuWnd,"ShardParentDict")
RegistClassMember(CLuaCommonPintuWnd,"ShardGrid")
RegistClassMember(CLuaCommonPintuWnd,"shardPrefab")
RegistClassMember(CLuaCommonPintuWnd,"TargetParent")
RegistClassMember(CLuaCommonPintuWnd,"HintLabel")
RegistClassMember(CLuaCommonPintuWnd,"CloseBtn")
RegistClassMember(CLuaCommonPintuWnd,"Finished")

function CLuaCommonPintuWnd:Awake()
	self.Finished = false
	self.ShardParentDict={}
	self:showShards()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.CommonPintuWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
end
function CLuaCommonPintuWnd:showShards()
	local trans = self.ShardGrid.transform
	local dict = {}
	for i =1, trans.childCount do
		local go = self:generateShard(i)
		dict[i] = go
	end
	local rlist = CUICommonDef.GetRandomList(trans.childCount)
	for i =1, trans.childCount do
		local go = dict[rlist[i-1]+1]
		local parent = trans:GetChild(i-1)
		go.transform.parent = parent
		go.transform.localPosition = Vector3(0,0,0)
		go.transform.localScale = Vector3(0.75,0.75,1)
		self.ShardParentDict[go] = parent
	end
end
function CLuaCommonPintuWnd:generateShard(index)
	local parent = self.shardPrefab.transform.parent.gameObject
	local go = CUICommonDef.AddChild(parent,self.shardPrefab)
	go.name = tostring(index)
	go:SetActive(true)
	local cuitexture = go:GetComponent(typeof(CUITexture))
	local callback = function(flag)
		local uiTex= go:GetComponent(typeof(UITexture))
		local y= 2 - math.modf((index-1)/3)
		local x= (index-1)%3
		local rect = Rect(0.33333*x,0.33333*y,0.33333,0.333333)
		uiTex.uvRect = rect;
	end
	local data = ZhuJueJuQing_CommonPinTu.GetData(CZhuJueJuQingMgr.Instance.PintuID)
	cuitexture:LoadMaterial(data.Text,false,DelegateFactory.Action_bool(callback))
	
	local onDrag = function(go,delta)
		if self.Finished then 
			return 
		end
		local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject);
		local trans = go.transform
		local pos = trans.localPosition
		pos.x = pos.x + delta.x*scale
		pos.y = pos.y + delta.y*scale
		trans.localPosition = pos
	end
	local onPress = function(go,flag)
		if self.Finished then 
			return 
			
		end
		if not flag then
			local currentPos = UICamera.currentTouch.pos
			local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
			local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
			for i =0,hits.Length -1 do
				if hits[i].collider.gameObject.transform.parent == self.TargetParent.transform  then
					go.transform.parent = hits[i].collider.gameObject.transform
					go.transform.localPosition = Vector3.zero
					self:judgeFinish()
					self.HintLabel:SetActive(false)
					return
				end
			end
			go.transform.parent = self.ShardParentDict[go].transform
			go.transform.localPosition = Vector3.zero
			go.transform.localScale = Vector3(0.75,0.75,1)
			self:judgeEmpty()
		else
			go.transform.localScale = Vector3.one
		end
	end
	CommonDefs.AddOnDragListener(go,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnPressListener(go,DelegateFactory.Action_GameObject_bool(onPress),false)	
	return go
end
function CLuaCommonPintuWnd:judgeEmpty()
	local notempty = false
	for i = 0,self.TargetParent.transform.childCount-1 do
		local trans = self.TargetParent.transform:GetChild(i)
		if trans.childCount > 0 then
			notempty = true
			break
		end
	end
	self.HintLabel:SetActive(not notempty)

end

function CLuaCommonPintuWnd:judgeFinish()
	for i = 0,self.TargetParent.transform.childCount-1 do
		local trans = self.TargetParent.transform:GetChild(i)
		if trans.childCount == 0 then
			return 
		end
		if trans:GetChild(0).name ~= trans.name then
			return 
		end
	end
	self.Finished=true
	Gac2Gas.FinishCommonPinTu(CZhuJueJuQingMgr.Instance.PintuTaskID,CZhuJueJuQingMgr.Instance.PintuID)
end

return CLuaCommonPintuWnd











