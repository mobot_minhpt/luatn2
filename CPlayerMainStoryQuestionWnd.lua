-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CButton = import "L10.UI.CButton"
local CPlayerMainStoryMgr = import "L10.Game.CPlayerMainStoryMgr"
local CPlayerMainStoryQuestionWnd = import "L10.UI.CPlayerMainStoryQuestionWnd"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local L10 = import "L10"
local Question_Task = import "L10.Game.Question_Task"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerMainStoryQuestionWnd.m_ShowAllNode_CS2LuaHook = function (this) 
    this.backBtn:SetActive(true)
    this.anchorNode:SetActive(true)
    this:InitPanelInfo()

    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)
end
CPlayerMainStoryQuestionWnd.m_InitPanelInfo_CS2LuaHook = function (this) 
    this.tipNode:SetActive(true)
    this.rightNode:SetActive(false)
    this.wrongNode:SetActive(false)

    if CPlayerMainStoryMgr.Inst.questionType > 0 then
        this.rightLabelNode.text = this.OtherRightString
    else
        this.rightLabelNode.text = this.DefaultRightString
    end

    local questionInfo = Question_Task.GetData(CPlayerMainStoryMgr.Inst.questionId)
    if questionInfo ~= nil then
        CommonDefs.GetComponent_GameObject_Type(this.sendBtn, typeof(CButton)).Enabled = true
        if CPlayerMainStoryMgr.Inst.total > 1 then
            this.infoWithNumNode:SetActive(true)
            this.infoNode:SetActive(false)
            this.nowProcessSlider.value = (CPlayerMainStoryMgr.Inst.index) / CPlayerMainStoryMgr.Inst.total
            this.nowProcessLabel.text = (tostring(CPlayerMainStoryMgr.Inst.index) .. "/") .. CPlayerMainStoryMgr.Inst.total
            this.numQuestionLabel.text = g_MessageMgr:Format(questionInfo.Question)
        else
            this.infoWithNumNode:SetActive(false)
            this.infoNode:SetActive(true)
            this.questionLabel.text = g_MessageMgr:Format(questionInfo.Question)
        end
    end

    this:CloseAciton()

    if CommonDefs.IS_VN_CLIENT then
        this.voiceBtn:SetActive(false)
        this.tipNode:SetActive(false)
    end
end

CPlayerMainStoryQuestionWnd.m_OnEnable_CS2LuaHook = function (this) 

    UIEventListener.Get(this.voiceBtn).onPress = MakeDelegateFromCSFunction(this.OnVoiceButtonPress, BoolDelegate, this)
    UIEventListener.Get(this.sendBtn).onClick = MakeDelegateFromCSFunction(this.OnSendBtnClick, VoidDelegate, this)
    EventManager.AddListenerInternal(EnumEventType.PlayerMainStoryInput, MakeDelegateFromCSFunction(this.OnVoiceBack, MakeGenericClass(Action1, String), this))
end
CPlayerMainStoryQuestionWnd.m_CloseAciton_CS2LuaHook = function (this) 
    if this.cancel ~= nil then
        invoke(this.cancel)
        this.cancel = nil
    end
end
CPlayerMainStoryQuestionWnd.m_OnDisable_CS2LuaHook = function (this) 

    UIEventListener.Get(this.voiceBtn).onPress = nil
    UIEventListener.Get(this.sendBtn).onClick = nil
    EventManager.RemoveListenerInternal(EnumEventType.PlayerMainStoryInput, MakeDelegateFromCSFunction(this.OnVoiceBack, MakeGenericClass(Action1, String), this))
    this:CloseAciton()
end
CPlayerMainStoryQuestionWnd.m_OnVoiceButtonPress_CS2LuaHook = function (this, go, pressed) 

    if pressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.PlayerMainStory, EChatPanel.Undefined, go, EnumVoicePlatform.Default, false)
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
    end
end
CPlayerMainStoryQuestionWnd.m_OnSendBtnClick_CS2LuaHook = function (this, go) 

    if System.String.IsNullOrEmpty(StringTrim(this.questionInput.value)) then
        return
    end

    local questionInfo = Question_Task.GetData(CPlayerMainStoryMgr.Inst.questionId)
    if questionInfo ~= nil then
        local answer = StringTrim(this.questionInput.value)
        if (string.find(answer, questionInfo.Right, 1, true) ~= nil) then
            this.tipNode:SetActive(false)
            this.rightNode:SetActive(true)
            this.wrongNode:SetActive(false)

            Gac2Gas.AnswerTaskVoiceQuestion(CPlayerMainStoryMgr.Inst.taskId, CPlayerMainStoryMgr.Inst.questionId, true)
            CommonDefs.GetComponent_GameObject_Type(this.sendBtn, typeof(CButton)).Enabled = false
            this:CloseAciton()
            this.questionInput.value = ""
            this.cancel = L10.Engine.CTickMgr.Register(MakeDelegateFromCSFunction(this.InitPanelInfo, Action0, this), 1000, ETickType.Once)
        else
            this.tipNode:SetActive(false)
            this.rightNode:SetActive(false)
            this.wrongNode:SetActive(true)

            CommonDefs.GetComponent_Component_Type(this.wrongNode.transform:Find("answer"), typeof(UILabel)).text = questionInfo.Right
            Gac2Gas.AnswerTaskVoiceQuestion(CPlayerMainStoryMgr.Inst.taskId, CPlayerMainStoryMgr.Inst.questionId, false)
            CommonDefs.GetComponent_GameObject_Type(this.sendBtn, typeof(CButton)).Enabled = false
            this:CloseAciton()
            this.questionInput.value = ""
            this.cancel = L10.Engine.CTickMgr.Register(MakeDelegateFromCSFunction(this.InitPanelInfo, Action0, this), 3000, ETickType.Once)
        end
    end
end
