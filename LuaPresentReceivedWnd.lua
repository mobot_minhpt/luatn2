require("common/common_include")
require("ui/present/LuaPresentReceivedItem")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CPresentMgr = import "L10.Game.CPresentMgr"
local GameplayItem_PresentItem = import "L10.Game.GameplayItem_PresentItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Item_Item = import "L10.Game.Item_Item"


LuaPresentReceivedWnd = class()

RegistClassMember(LuaPresentReceivedWnd,"closeBtn")
RegistClassMember(LuaPresentReceivedWnd,"playerInfoLabel")
RegistClassMember(LuaPresentReceivedWnd,"scrollView")
RegistClassMember(LuaPresentReceivedWnd,"grid")
RegistClassMember(LuaPresentReceivedWnd,"itemTemplate")
RegistClassMember(LuaPresentReceivedWnd,"receiveBtn")
RegistClassMember(LuaPresentReceivedWnd,"remainTimeLabel")

function LuaPresentReceivedWnd:Awake()

end

function LuaPresentReceivedWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaPresentReceivedWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_2/CloseButton").gameObject
	self.playerInfoLabel = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))
	self.scrollView = self.transform:Find("Anchor/Items/ItemsScrollView"):GetComponent(typeof(UIScrollView))
	self.grid = self.transform:Find("Anchor/Items/ItemsScrollView/Grid"):GetComponent(typeof(UIGrid))
	self.itemTemplate = self.transform:Find("Anchor/Items/ItemsScrollView/ItemCell").gameObject
	self.receiveBtn = self.transform:Find("Anchor/ReceiveBtn").gameObject
	self.remainTimeLabel = self.transform:Find("Anchor/RemainTimeLabel"):GetComponent(typeof(UILabel))
	
	self.itemTemplate:SetActive(false)
	
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.receiveBtn, DelegateFactory.Action_GameObject(function(go) self:OnReceiveButtonClick(go) end), false)
	
	self.playerInfoLabel.text = SafeStringFormat3(LocalString.GetString("好友 %s 向你送来了:"), CPresentMgr.Inst.RecvPresentInfo.senderName)
    self.remainTimeLabel.text = nil
    CUICommonDef.ClearTransform(self.grid.transform)
    local data = GameplayItem_PresentItem.GetData(CPresentMgr.Inst.RecvPresentInfo.templateId)
	if data then
		local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(CPresentMgr.Inst.RecvPresentInfo.sendTime + data.LinkDuration * 60 * 60)
		local span = dt:Subtract(CServerTimeMgr.Inst:GetZone8Time())
		if span.TotalSeconds < 0 then
			self.remainTimeLabel.text = LocalString.GetString("已过期")
		else
			local totalSeconds = math.floor(span.TotalSeconds)
			local hours = math.floor(totalSeconds / 3600)
			local minutes = math.floor((totalSeconds % 3600) / 60)
			local seconds = math.floor(totalSeconds % 60)
			self.remainTimeLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d小时%d分%d秒领取有效"), hours, minutes, seconds)
		end
		for i=0,data.GetItemId.Length-1 do
			local templateId = data.GetItemId[i][0]
			if Item_Item.Exists(templateId) then
				local go = CUICommonDef.AddChild(self.grid.gameObject, self.itemTemplate)
				go:SetActive(true)
				local item = LuaPresentReceivedItem:new()
				item:Init(go, templateId, 1)
			end
		end
		self.grid:Reposition()
		self.scrollView:ResetPosition()
	else
		self:Close()
	end
end

function LuaPresentReceivedWnd:OnReceiveButtonClick(go)
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Receive_Gift_Send_Mail_Confirm_Message"),
		DelegateFactory.Action(function()
			self:RequestGetPresent(true)
		end),
		DelegateFactory.Action(function()
			self:RequestGetPresent(false)
		end), nil, nil, false)
end

function LuaPresentReceivedWnd:RequestGetPresent(allowSendMail)
	Gac2Gas.RequestGetPresentItemFromLink(CPresentMgr.Inst.RecvPresentInfo.receiverId,
            CPresentMgr.Inst.RecvPresentInfo.senderId,
            CPresentMgr.Inst.RecvPresentInfo.sendTime,
            CPresentMgr.Inst.RecvPresentInfo.templateId,
            allowSendMail)
	self:Close()
end

return LuaPresentReceivedWnd

