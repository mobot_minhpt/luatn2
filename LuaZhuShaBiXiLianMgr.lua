local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemChooseMgr=import "L10.UI.CItemChooseMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CChuanjiabaoWordType=import "L10.Game.CChuanjiabaoWordType"
local CEquipment=import "L10.Game.CEquipment"

CLuaZhuShaBiXiLianMgr = {}

CLuaZhuShaBiXiLianMgr.m_CountConditions = {}
CLuaZhuShaBiXiLianMgr.m_WordConditions = {}

function CLuaZhuShaBiXiLianMgr.InitCondition()
    CLuaZhuShaBiXiLianMgr.m_CountConditions = {}
    CLuaZhuShaBiXiLianMgr.m_WordConditions = {}

    CLuaZhuShaBiXiLianMgr.AddCountOption(6,true)
    CLuaZhuShaBiXiLianMgr.AddCountOption(7,true)
    CLuaZhuShaBiXiLianMgr.AddCountOption(8,true)

    local commonItem = CItemMgr.Inst:GetById(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    local wordInfo = nil

    if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
        if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
            wordInfo = CreateFromClass(CChuanjiabaoWordType)
            wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
        end
    else
        wordInfo = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
    end

    if wordInfo then
        if wordInfo.Type == EChuanjiabaoType_lua.RenGe then
            if CClientMainPlayer.Inst then
                local data = ChuanjiabaoModel_RenGeWordOption.GetData(wordInfo.TypeParam)
                if data then
                    for i=1,data.Word.Length do
                        CLuaZhuShaBiXiLianMgr.AddWordOption(data.Word[i-1],false)
                    end
                end
            end
        elseif wordInfo.Type == EChuanjiabaoType_lua.TianGe then
            local data = ChuanjiabaoModel_Setting.GetData().TianGeWordOption
            for i=1,data.Length do
                CLuaZhuShaBiXiLianMgr.AddWordOption(data[i-1],false)
            end
        elseif wordInfo.Type == EChuanjiabaoType_lua.DiGe then
            local data = ChuanjiabaoModel_Setting.GetData().DiGeWordOption
            for i=1,data.Length do
                CLuaZhuShaBiXiLianMgr.AddWordOption(data[i-1],false)
            end
        end
    end
end
function CLuaZhuShaBiXiLianMgr.IsGoodResult(lastWashResult,preStar,preWordCount)
    local words = lastWashResult.Words
    local wordCount = words.Count
    if wordCount>=6 then
        return true
    end
    local newStar = lastWashResult.Star
    if newStar>preStar and wordCount>= preWordCount then
        return true
    end
    if newStar>=preStar and wordCount> preWordCount then
        return true
    end
    return false
end

function CLuaZhuShaBiXiLianMgr.CheckCondition(lastWashResult)
    local words = lastWashResult.Words
    local wordCount = words.Count
    for k,v in pairs(CLuaZhuShaBiXiLianMgr.m_CountConditions) do
        if v then
            if wordCount>=k then
                return true
            end
        end
    end

    local result = false
    CommonDefs.DictIterate(words,DelegateFactory.Action_object_object(function (k, v)
        local word = math.floor(k/100)
        if CLuaZhuShaBiXiLianMgr.m_WordConditions[word] then
            result = true
        else
            local data = __Word_Word_Template.GetData(word)
            if data and CLuaZhuShaBiXiLianMgr.m_WordConditions[data.WordClass] then
                result = true
            end
        end
    end))
    return result
end


function CLuaZhuShaBiXiLianMgr.AddCountOption(count,v)
    CLuaZhuShaBiXiLianMgr.m_CountConditions[count] = v
end

function CLuaZhuShaBiXiLianMgr.AddWordOption(word,v)
    CLuaZhuShaBiXiLianMgr.m_WordConditions[word] = v
end

function CLuaZhuShaBiXiLianMgr.GetWordClassDescription(wordClass)
    local wordId = CEquipment.GetWordIdPrefixByWordClass(wordClass)
    local data = __Word_Word_Template.GetData(wordId)
    if data then
        return data.WordDescription
    else
        return nil
    end
end

function CLuaZhuShaBiXiLianMgr.CanXiLian(wordInfo)
    if not wordInfo then return false end
    if wordInfo.Star>=6 then
        return true
    else
        return false
    end
end
function CLuaZhuShaBiXiLianMgr.GetWashBaseProp(lv,star,numOfWord)
    local formul = AllFormulas.Action_Formula[ChuanjiabaoModel_Setting.GetData().WashBaseFormulaId].Formula
    return formul(nil,nil,{lv,star,numOfWord})
end
function CLuaZhuShaBiXiLianMgr.GetBorrowLifeProp(star,numOfWord)
    local prop = 0
    local borrowlifeData = House_BorrowLifeProb.GetData(star)
    if numOfWord==1 then
        prop = borrowlifeData.WordNum1
    elseif numOfWord==2 then
        prop = borrowlifeData.WordNum2
    elseif numOfWord==3 then
        prop = borrowlifeData.WordNum3
    elseif numOfWord==4 then
        prop = borrowlifeData.WordNum4
    elseif numOfWord==5 then
        prop = borrowlifeData.WordNum5
    elseif numOfWord==6 then
        prop = borrowlifeData.WordNum6
    elseif numOfWord==7 then
        prop = borrowlifeData.WordNum7
    elseif numOfWord==8 then
        prop = borrowlifeData.WordNum8
    end
    return prop
end

function CLuaZhuShaBiXiLianMgr.ShowZhuShaBiShengXingWnd()
    local t = {}
    local itemProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp or nil

    local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i=1,bagSize do
        local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if commonItem and commonItem.IsItem and commonItem.IsBinded then
            local type = commonItem.Item.Type
            local wordInfo = nil
            if type==EnumItemType_lua.EvaluatedZhushabi then--EnumItemType.EvaluatedZhushabi
                if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                    wordInfo = CreateFromClass(CChuanjiabaoWordType)
                    wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
                end
                if wordInfo and wordInfo.Star==6 or wordInfo.Star==7 then
                    table.insert( t, itemId )
                end
            end
        end
    end
	local stringList =  CreateFromClass(MakeGenericClass(List,cs_string))
	for i,v in ipairs(t) do
		CommonDefs.ListAdd(stringList,typeof(cs_string),v)
	end
    CItemChooseMgr.Inst.ItemIds=stringList
    CItemChooseMgr.Inst.OnChoose=DelegateFactory.Action_string(function(itemId)
        local commonItem = CItemMgr.Inst:GetById(itemId)
        if commonItem then
            local wordInfo = CreateFromClass(CChuanjiabaoWordType)
            wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
            local star =  wordInfo.Star--wordInfo.WashedStar and wordInfo.WashedStar or wordInfo.Star

            local name = Item_Item.GetData(commonItem.TemplateId).Name
            local lv = 0
            if string.find(name,LocalString.GetString("1级")) then
                lv=1
            elseif string.find(name,LocalString.GetString("2级")) then
                lv=2
            elseif string.find(name,LocalString.GetString("3级")) then
                lv=3
            end
            local costNum=0
            local targetStar=0
            local starData = ChuanjiabaoModel_ImproveStar.GetData(lv)
            if starData then
                if star==6 then
                    costNum=starData.Star6
                    targetStar=7
                elseif star==7 then
                    costNum=starData.Star7
                    targetStar=8
                end
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZhuShaBi_ShengXing_Confirm",costNum,targetStar), DelegateFactory.Action(function () 
                    local itemInfo = CItemMgr.Inst:GetItemInfo(itemId)
                    if itemInfo then
                        Gac2Gas.RequestImproveChuanjiabaoStar(EnumToInt(itemInfo.place),itemInfo.pos,itemId)--place, pos, itemId
                    end
                end), nil, nil, nil, false)
            end
        end
	end)
    CItemChooseMgr.Inst.Title = LocalString.GetString("请选择要升星的朱砂笔")
    CItemChooseMgr.Inst.ButtonText = LocalString.GetString("升级星级")
    CUIManager.ShowUI(CIndirectUIResources.ItemChooseWnd)
end

