
local DefaultTableViewDataSource    = import "L10.UI.DefaultTableViewDataSource"
local QnTableView                   = import "L10.UI.QnTableView"
local UILabel                       = import "UILabel"
local BoxCollider                   = import "UnityEngine.BoxCollider"
local Color                         = import "UnityEngine.Color"
local MessageWndManager             = import "L10.UI.MessageWndManager"

LuaGuildVoteDetailWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaGuildVoteDetailWnd,         "DesLable",         UILabel)
RegistChildComponent(LuaGuildVoteDetailWnd,         "AdvView",          QnTableView)
RegistChildComponent(LuaGuildVoteDetailWnd,         "VoteBtn",          GameObject)
RegistChildComponent(LuaGuildVoteDetailWnd,         "ThanksBtn",        GameObject)
RegistChildComponent(LuaGuildVoteDetailWnd,         "VoteLable",        GameObject)
RegistChildComponent(LuaGuildVoteDetailWnd,         "ShareBtn",         GameObject)

---------RegistClassMember-------   
RegistClassMember(LuaGuildVoteDetailWnd,            "VotePlayerID")
RegistClassMember(LuaGuildVoteDetailWnd,            "SelectID")
RegistClassMember(LuaGuildVoteDetailWnd,            "IsLock")

function LuaGuildVoteDetailWnd:Init()
    self.IsLock = luaGuildVoteMgr.IsVoted
    self:InitBtn()
    self:RefreshDesLable()
    self:RefresBottomState()
    self:InitAdvView()
end

function LuaGuildVoteDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("SendYuanDanGuildDoVoteSucc", self, "SendYuanDanGuildDoVoteSucc")
end

function LuaGuildVoteDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendYuanDanGuildDoVoteSucc", self, "SendYuanDanGuildDoVoteSucc")
    luaGuildVoteMgr:ClearDetailData()
end

function LuaGuildVoteDetailWnd:InitAdvView()
    self.AdvView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return #luaGuildVoteMgr.PlayerOptionTable
    end, function(item, index)
        self:InitItem(item, index)
    end)

    self.AdvView.OnSelectAtRow = DelegateFactory.Action_int(function(row)

    end)
    self.AdvView:ReloadData(true,false)
    if luaGuildVoteMgr.IsVoted or luaGuildVoteMgr.IsFinish then
        for i=1,#luaGuildVoteMgr.PlayerOptionTable do
            if luaGuildVoteMgr.PlayerOptionTable[i].playerId == luaGuildVoteMgr.VotePlayerId then
                self.AdvView:SetSelectRow(i-1,true)
            end
        end
        self:LockSelect()
    end
end

function LuaGuildVoteDetailWnd:InitItem(item,index)
    item.name = index
    local PlayNameLable = item.transform:Find("PlayNameLable"):GetComponent(typeof(UILabel))
    local CountLable = item.transform:Find("CountLable"):GetComponent(typeof(UILabel))
    local IsWin = item.transform:Find("IsWin").gameObject
    local Tip = item.transform:Find("Tip").gameObject
    PlayNameLable.text = luaGuildVoteMgr.PlayerOptionTable[index+1].playerName

    CountLable.text = luaGuildVoteMgr.PlayerOptionTable[index+1].voteCount
    CountLable.gameObject:SetActive(luaGuildVoteMgr.IsFinish)
    Tip:SetActive(luaGuildVoteMgr.PlayerOptionTable[index+1].playerId == luaGuildVoteMgr.VotePlayerId)
    IsWin:SetActive(luaGuildVoteMgr.PlayerOptionTable[index+1].playerId == luaGuildVoteMgr.WinPlayerId)

    if luaGuildVoteMgr.PlayerOptionTable[index+1].playerId == luaGuildVoteMgr.WinPlayerId then
        PlayNameLable.color = Color.yellow
    else
        PlayNameLable.color = Color.white
    end
end

function LuaGuildVoteDetailWnd:RefreshDesLable()
    if luaGuildVoteMgr.IsFinish then
        self.DesLable.text = luaGuildVoteMgr.QuestionContent..LocalString.GetString("[c][00FF00]结果已揭晓[-][/c]")
    else
        self.DesLable.text = luaGuildVoteMgr.QuestionContent
    end
end

function LuaGuildVoteDetailWnd:RefresBottomState()
    self.ShareBtn:SetActive(luaGuildVoteMgr.IsVoted)
    self.ThanksBtn:SetActive(luaGuildVoteMgr.IsFinish)
    self.VoteBtn:SetActive((not luaGuildVoteMgr.IsFinish) and (not luaGuildVoteMgr.IsVoted))
    self.VoteLable:SetActive((not luaGuildVoteMgr.IsFinish) and luaGuildVoteMgr.IsVoted)
end

function LuaGuildVoteDetailWnd:RefreshItem()
    self.AdvView:Clear()
    self.AdvView:ReloadData(true,false)
end

function LuaGuildVoteDetailWnd:SendYuanDanGuildDoVoteSucc()
    self:RefreshItem()
    self:LockSelect()
    self.AdvView:SetSelectRow(self.SelectID,true)
    self:RefresBottomState()
end

function LuaGuildVoteDetailWnd:LockSelect()
    for i=0,#luaGuildVoteMgr.PlayerOptionTable - 1 do
        self.AdvView.m_ItemList[i].transform:GetComponent(typeof(BoxCollider)).enabled = false
    end
end

function LuaGuildVoteDetailWnd:InitBtn()
    UIEventListener.Get(self.VoteBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        self.SelectID = self.AdvView.currentSelectRow

        if self.SelectID >= 0 then
            self.VotePlayerID = luaGuildVoteMgr.PlayerOptionTable[self.SelectID+1].playerId
            Gac2Gas.RequestYuanDanGuildDoVote(luaGuildVoteMgr.QuestionId,self.VotePlayerID)
        else
            g_MessageMgr:ShowMessage("Guild_Vote_No_VoteID")
        end
    end)
    UIEventListener.Get(self.ThanksBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        if luaGuildVoteMgr.WinPlayerId == CClientMainPlayer.Inst.Id then
            CUIManager.ShowUI(CLuaUIResources.GuildVoteThanksWnd)
        else
            g_MessageMgr:ShowMessage("Guild_Vote_Only_Winner")
        end
    end)
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        local msg = g_MessageMgr:FormatMessage("Guild_Vote_Confirm_Share")
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function ()
            Gac2Gas.RequestYuanDanGuildVoteShare(luaGuildVoteMgr.QuestionId)
        end), nil, nil, nil, false)
    end)
end
