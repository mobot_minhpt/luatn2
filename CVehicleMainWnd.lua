-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CVehicleMainWnd = import "L10.UI.CVehicleMainWnd"
local CVehicleWnd = import "L10.UI.CVehicleWnd"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
CVehicleMainWnd.m_Init_CS2LuaHook = function (this) 

    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)
    this.tabBar:ChangeTab(CZuoQiMgr.TabIndex, false)

    if not CZuoQiMgr.IsMapiEnable() then
        for i = 1, 3 do
            this.tabBar:GetTabGoByIndex(i):SetActive(false)
        end
    end
end
CVehicleMainWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    CZuoQiMgr.TabIndex = index

    this.zuoqiView.gameObject:SetActive(index == 0)
    this.mapiView.gameObject:SetActive(index == 1)
    this.lingfuView.gameObject:SetActive(index == 2)
    this.yumaView.gameObject:SetActive(index == 3)

    if index == 0 then
        this.zuoqiView:Init()
        this.titleLabel.text = LocalString.GetString("坐骑")
    elseif index == 1 then
        this.mapiView:Init()
        this.titleLabel.text = LocalString.GetString("马匹")
    elseif index == 2 then
        this.lingfuView:Init()
        this.titleLabel.text = LocalString.GetString("灵符")
    elseif index == 3 then
        this.yumaView:Init()
        this.titleLabel.text = LocalString.GetString("育马")
    end
end
CVehicleMainWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    this:Close()
    CZuoQiMgr.Inst.curSelectedZuoqiId = nil
    CZuoQiMgr.Inst.curSelectedZuoqiTid = 0
    CZuoQiMgr.TabIndex = 0

    --判断是不是在引导
    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(33) and L10.Game.Guide.CGuideMgr.Inst:IsInSubPhase(3) then
        --如果已经上马了，则跳到下一步
        if CClientMainPlayer.Inst ~= nil then
            if not System.String.IsNullOrEmpty(CClientMainPlayer.Inst.BasicProp.CurrentZuoQiId) then
                --有坐骑了
                --进入下一阶段
                L10.Game.Guide.CGuideMgr.Inst:NextSubPhase()
            end
        end
    end
end
CVehicleMainWnd.m_GetDesc_CS2LuaHook = function (this) 
    local rtn = nil
    local cmp = CommonDefs.GetComponentInChildren_Component_Type(this, typeof(QnModelPreviewer))
    if cmp ~= nil then
        rtn = cmp:GetDesc()
    end
    if rtn ~= nil then
        if not this.triggered then
            this.triggered = false
            this:StartCoroutine(this:DelayTrigger())
        end
    end
    return rtn
end
CVehicleMainWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "GetMountButton" then
        local wnd = CommonDefs.GetComponentInChildren_Component_Type(this, typeof(CVehicleWnd))
        if wnd ~= nil then
            return wnd:GetMountButton()
        end
    elseif methodName == "GetDesc" then
        return this:GetDesc()
        --QnModelPreviewer cmp = GetComponentInChildren<QnModelPreviewer>();
        --if (cmp!=null)
        --{

        --    return cmp.GetDesc();
        --}
    elseif methodName == "GetCloseButton" then
        return this.closeButton
    end
    --找不到 结束引导
    L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
    return nil
end
