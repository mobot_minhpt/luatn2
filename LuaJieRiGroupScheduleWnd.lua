local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UISprite = import "UISprite"

LuaJieRiGroupScheduleWnd = class()
LuaJieRiGroupScheduleWnd.s_OpenFromScheduleWnd = false

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaJieRiGroupScheduleWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaJieRiGroupScheduleWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaJieRiGroupScheduleWnd, "Background", "Background", UISprite)
--@endregion RegistChildComponent end
RegistClassMember(LuaJieRiGroupScheduleWnd,"m_GroupId2AlertSpriteMap")


function LuaJieRiGroupScheduleWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    if LuaJieRiGroupScheduleWnd.s_OpenFromScheduleWnd then
        LuaJieRiGroupScheduleWnd.s_OpenFromScheduleWnd = false
        if not CUIManager.IsLoaded(CLuaUIResources.ScheduleWnd) then return end
        local items = CLuaScheduleMgr:GetTodayFestivals()
        local widget = self.Background.transform.parent:GetComponent(typeof(UIWidget))
        --widget:SetAnchor()
        widget.leftAnchor:Set(0.5, -135)
        widget.rightAnchor:Set(0.5, -100)
        widget.bottomAnchor:Set(0, 500)
        widget.topAnchor:Set(0, 180 * (items and #items - 1 or 0))
        --local go = CUIManager.instance:GetGameObject(CLuaUIResources.ScheduleWnd)
        --if go then
        --    local jieRiBtn = FindChild(go.transform,"JieRiButton")
        --    local pos = self.transform:InverseTransformPoint(jieRiBtn.position)    
        --end
    end
end

function LuaJieRiGroupScheduleWnd:Init()
    if not CClientMainPlayer.Inst then
        CUIManager.CloseUI(CLuaUIResources.JieRiGroupScheduleWnd)
        return
    end
    self.m_GroupId2AlertSpriteMap = {}
    self.Template.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.Grid.transform)
    local items = CLuaScheduleMgr:GetTodayFestivals()
    for i = 1,#items do
        if not self.m_GroupId2AlertSpriteMap[items[i].ID] then
            local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
            obj.gameObject:SetActive(true)
            self:InitItem(obj, items[i])
        end
    end
    self.Grid:Reposition()
    self.Background.height = self.Background.height + (#items - 1) * self.Grid.cellHeight
    self:UpdateAlertSprites(true)
end

function LuaJieRiGroupScheduleWnd:InitItem(obj, data)
    local label = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
    local tex = obj.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local alert = obj.transform:Find("Alert").gameObject
    label.text = data.Name
    self.m_GroupId2AlertSpriteMap[data.ID] = alert
    if not System.String.IsNullOrEmpty(data.MiniPicture) then
        tex:LoadMaterial(data.MiniPicture)
    end
    UIEventListener.Get(obj.gameObject).onClick = LuaUtils.VoidDelegate(function()
        CLuaScheduleMgr.OnJieRiGroupScheduleClick(data)
    end)
end

function LuaJieRiGroupScheduleWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateActivityRedDot",self,"UpdateAlertSprites")
end

function LuaJieRiGroupScheduleWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot",self,"UpdateAlertSprites")
end

function LuaJieRiGroupScheduleWnd:UpdateAlertSprites(hasChanged)
    if not hasChanged then return end
    for groupId, alert in pairs(self.m_GroupId2AlertSpriteMap) do
        alert.gameObject:SetActive(false)
    end
    local items = CLuaScheduleMgr:GetTodayFestivals()
    for i = 1,#items do
        local alert = self.m_GroupId2AlertSpriteMap[items[i].ID]
        local showFx = --[[CLuaScheduleMgr:IsNeedShowJieRiButtonFx(items[i]) or --]]LuaActivityRedDotMgr:IsRedDot(items[i].ID, LuaEnumRedDotType.JieRi)
        alert.gameObject:SetActive(showFx)     
    end
end

--@region UIEvent
--@endregion UIEvent