local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CPos = import "L10.Engine.CPos"
local CMainCamera = import "L10.Engine.CMainCamera"
local NormalCamera=import "L10.Engine.CameraControl.NormalCamera"
LuaDaFuWongPlayLandTopWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongPlayLandTopWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaDaFuWongPlayLandTopWnd, "DaFuWongLandTopTip", "DaFuWongLandTopTip", GameObject)
RegistChildComponent(LuaDaFuWongPlayLandTopWnd, "DaFuWongHospitalTopTip", "DaFuWongHospitalTopTip", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongPlayLandTopWnd, "m_LandTopView")
RegistClassMember(LuaDaFuWongPlayLandTopWnd, "m_HospitalView")
RegistClassMember(LuaDaFuWongPlayLandTopWnd, "m_IsShow")
RegistClassMember(LuaDaFuWongPlayLandTopWnd, "m_InitPos")
RegistClassMember(LuaDaFuWongPlayLandTopWnd, "m_Content")
function LuaDaFuWongPlayLandTopWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.DaFuWongLandTopTip.gameObject:SetActive(false)
    self.DaFuWongHospitalTopTip.gameObject:SetActive(false)
    self.m_IsShow = false
    self.m_InitPos = nil
    self.m_Content = self.transform:Find("Content")
end

-- function LuaDaFuWongPlayLandTopWnd:Update()
--     if not self.m_IsShow or not self.m_InitPos then return end
--     -- --local viewPos = CMainCamera.Main:WorldToViewportPoint(Vector3(0,0,0))
-- 	-- local isInView = true --viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1
-- 	local screenPos = CMainCamera.Main:WorldToScreenPoint(self.m_InitPos) --or Vector3(0,3000,0)
-- 	screenPos.z = 0
-- 	local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
-- 	self.m_Content.transform.position = worldPos
-- end

function LuaDaFuWongPlayLandTopWnd:Init()
    Extensions.RemoveAllChildren(self.Content.transform)
    self.m_LandTopView = {}
    self.m_HospitalView = {}
    DaFuWeng_Land.Foreach(function(k,v)
        --if v.Type == 2 then
        local go = CUICommonDef.AddChild(self.Content.gameObject,self.DaFuWongLandTopTip)
        go.gameObject:SetActive(true)
        self.m_LandTopView[k] = {}
        self.m_LandTopView[k].go = go
        self.m_LandTopView[k].data = v
        self:InitTopView(k)
        if LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[k] then
            self:UpdateTopView(k)
        end
        if k == LuaDaFuWongMgr.SpecialId[1].landId or k == LuaDaFuWongMgr.SpecialId[2].landId then
            local go = CUICommonDef.AddChild(self.Content.gameObject,self.DaFuWongHospitalTopTip)
            go.gameObject:SetActive(true)
            self.m_HospitalView[k] = {}
            self.m_HospitalView[k].go = go
            self.m_HospitalView[k].data = v
            self:InitTopView(k)
            if LuaDaFuWongMgr.CurHospitalInfo then
                self:UpdateTopView(k)
            end
        end
    end)
    CUIManager.SetUIVisibility(CLuaUIResources.DaFuWongPlayLandTopWnd, LuaDaFuWongMgr.CurStage and LuaDaFuWongMgr.CurStage ~= EnumDaFuWengStage.GameStart and LuaDaFuWongMgr.CurStage ~= EnumDaFuWengStage.GameEnd, "Dafuweng")
end

function LuaDaFuWongPlayLandTopWnd:InitTopView(id)
    if id == LuaDaFuWongMgr.SpecialId[1].landId and self.m_HospitalView[id] then
        local view = self.m_HospitalView[id].go
        local data = self.m_HospitalView[id].data
        view:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(data)
        return 
    end
    if id == LuaDaFuWongMgr.SpecialId[2].landId and self.m_HospitalView[id] then
        local view = self.m_HospitalView[id].go
        local data = self.m_HospitalView[id].data
        view:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(data)
        return 
    end
    local view = self.m_LandTopView[id].go
    local data = self.m_LandTopView[id].data
    view:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init(data)
end
function LuaDaFuWongPlayLandTopWnd:UpdateTopView(id)
    if id == LuaDaFuWongMgr.SpecialId[1].landId and self.m_HospitalView[id] then
        local hospitalData = LuaDaFuWongMgr.CurHospitalInfo[id]
        local luascript = self.m_HospitalView[id].go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
        luascript:UpdateData(hospitalData)
    end
    if id == LuaDaFuWongMgr.SpecialId[2].landId and self.m_HospitalView[id] then
        local hospitalData = LuaDaFuWongMgr.CurHospitalInfo[id]
        local luascript = self.m_HospitalView[id].go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
        luascript:UpdateData(hospitalData)
    end
    if not self.m_LandTopView[id] then return end
    local data = LuaDaFuWongMgr.LandInfo and LuaDaFuWongMgr.LandInfo[id]
    local luascript = self.m_LandTopView[id].go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    
    if data then
        if luascript.m_Owner ~= data.Owner then
            luascript:SwitchOwner(data)
            return
        end
    end
    luascript:UpdateData(data)
end

function LuaDaFuWongPlayLandTopWnd:OnStageUpdate(CurStage)
	CUIManager.SetUIVisibility(CLuaUIResources.DaFuWongPlayLandTopWnd, CurStage ~= EnumDaFuWengStage.GameStart and CurStage ~= EnumDaFuWengStage.GameEnd, "Dafuweng")
end
function LuaDaFuWongPlayLandTopWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWongLandInfoUpdate",self,"UpdateTopView")
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongPlayLandTopWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWongLandInfoUpdate",self,"UpdateTopView")
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    self.m_InitPos = nil
end
--@region UIEvent

--@endregion UIEvent

