local UISprite = import "UISprite"

local CUIFx = import "L10.UI.CUIFx"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"

LuaShiTuRatingTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuRatingTipWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaShiTuRatingTipWnd, "DescriptionLabel", "DescriptionLabel", UILabel)
RegistChildComponent(LuaShiTuRatingTipWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaShiTuRatingTipWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaShiTuRatingTipWnd, "GradeSprite", "GradeSprite", UISprite)

--@endregion RegistChildComponent end

function LuaShiTuRatingTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaShiTuRatingTipWnd:Init()
    local data = ShiTu_MainLevel.GetData(LuaShiTuMgr.m_FamilyTreeShiMenInfo.shimenLevel)
    self.Icon:LoadMaterial(data.TreeIcon)
    if not String.IsNullOrEmpty(data.JiaPuFx) then
        self.Fx:LoadFx(data.JiaPuFx..".prefab")
    end
    self.GradeSprite.spriteName = SafeStringFormat3("guildwnd_grade_0%d",LuaShiTuMgr.m_FamilyTreeShiMenInfo.shimenLevel)
    self.TitleLabel.text = data.Title
    local colorArr = {"7eeb81","ddffc7","f9f197","ffafaf","eec6ff"}
    self.TitleLabel.color = NGUIText.ParseColor24(colorArr[LuaShiTuMgr.m_FamilyTreeShiMenInfo.shimenLevel], 0)
    self.DescriptionLabel.text = data.TitleDescription
end

--@region UIEvent

--@endregion UIEvent

