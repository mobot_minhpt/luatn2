local CUITexture=import "L10.UI.CUITexture"
local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"


CLuaGuildMainWndActivityWnd = class()
RegistClassMember(CLuaGuildMainWndActivityWnd,"m_TableView")
RegistClassMember(CLuaGuildMainWndActivityWnd,"dataList")
RegistClassMember(CLuaGuildMainWndActivityWnd,"m_GLCActivityName")--跨服联赛活动名
RegistClassMember(CLuaGuildMainWndActivityWnd,"m_GLCIsOpen")--跨服联赛是否开启
RegistClassMember(CLuaGuildMainWndActivityWnd,"m_HengYuDangKouAID")--横屿荡寇活动id
-- 当前服务器是否开启横屿荡寇活动
function CLuaGuildMainWndActivityWnd:IsHengYuDangKouEnable()
    return LuaHengYuDangKouMgr:IsHengYuDangKouEnable()
end
function CLuaGuildMainWndActivityWnd:Start()
    self.dataList={}
    self.m_GLCActivityName = LocalString.GetString("跨服帮会联赛")
    self.m_GLCIsOpen = LuaGuildLeagueCrossMgr:IsOpenedOnMyServer()
    self.m_HengYuDangKouAID = HengYuDangKou_Setting.GetData().TrophyTabSchedule
    Guild_GuildActivity.ForeachKey(function (key) 
        local val = Guild_GuildActivity.GetData(key)
        if val.ActivityID == self.m_HengYuDangKouAID then
            if val.IsActive > 0 and self:IsHengYuDangKouEnable() then
                table.insert(self.dataList, val)
            end
        elseif val.IsActive > 0 then
            if val.Name ~= self.m_GLCActivityName or self.m_GLCIsOpen then
                table.insert( self.dataList,val )
            end
        end
    end)

    self.m_TableView = self.transform:Find("QnTableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.dataList
        end,
        function(item, index)
            self:InitItem(item.transform,index)
        end)
    self.m_TableView:ReloadData(true, false)

end
function CLuaGuildMainWndActivityWnd:InitItem(transform, row)
    local data=self.dataList[row+1]
    local Id, name, desp, icon, buttonName = data.ID, data.Name, data.Description, data.Icon, data.ButtonName
    local m_TitleLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local m_DespLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    local m_Texture = transform:Find("ItemCell"):GetComponent(typeof(CUITexture))
    local m_Button = transform:Find("Btn").gameObject
    local m_Button2 = transform:Find("Btn2").gameObject
    local tipButton = transform:Find("TipButton").gameObject
    local timeLabel = transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    if data.ActivityTime and data.ActivityTime~="" then
        timeLabel.text = "("..data.ActivityTime..")"
    end
    if data.ActivityID>0 then
        tipButton:SetActive(true)
        UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(g)
            CLuaScheduleMgr.selectedScheduleInfo={
                activityId = data.ActivityID,
                taskId = Task_Schedule.GetData(data.ActivityID).TaskID[0],

                TotalTimes = 0,--不显示次数
                DailyTimes = 0,--不显示活力
                FinishedTimes = 0,
                
                Icon = data.Icon,
                ActivityTime = data.ActivityTime or "",
            }
            CLuaScheduleInfoWnd.s_UseCustomInfo = true
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
        end)
    else
        tipButton:SetActive(false)
    end

    -- m_CurrentActivity = CommonDefs.ConvertIntToEnum(typeof(EGuildActivity), Id)
    m_TitleLabel.text = name
    m_DespLabel.text = desp
    m_Texture:LoadMaterial(icon)
    -- m_Button.Text = buttonName
    m_Button.transform:Find("Label"):GetComponent(typeof(UILabel)).text=buttonName


    if Id == 4 then --EGuildActivity.GuildLeague
        UIEventListener.Get(m_Button).onClick = DelegateFactory.VoidDelegate(function(go)
            CUIManager.ShowUI(CUIResources.GuildLeagueResultLookUpWnd)
        end)
        if self.m_GLCIsOpen then
            --跨服帮赛期间，本服帮赛的描述进行修改
            m_DespLabel.text = g_MessageMgr:FormatMessage("GLC_LOCAL_GUILD_LEAGUE_ACTIVITY_DESC")
        end
    elseif Id == 7 then --EGuildActivity.GuildChallenge
        UIEventListener.Get(m_Button).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.GC_RequestOpenAllGuildInfoWnd()
        end)
    elseif name == self.m_GLCActivityName then
        UIEventListener.Get(m_Button).onClick = DelegateFactory.VoidDelegate(function(go)
            LuaGuildLeagueCrossMgr:ShowMatchInfoWnd()
        end)
    else
        UIEventListener.Get(m_Button).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "BackToGuildCity", "", "")
        end)
    end
    -- 如果是横屿荡寇，显示战利品
    if data.ActivityID == self.m_HengYuDangKouAID then
        -- 显示战利品按钮，提示按钮左移
        tipButton.transform.localPosition = Vector3(882, 0, 0)
        m_Button2.transform.localPosition = Vector3(1058, 0, 0)
        m_Button2:SetActive(true)
        -- 缩小文本框避免覆盖
        m_DespLabel.width = 650
        -- 点击查看战利品
        UIEventListener.Get(m_Button2).onClick = DelegateFactory.VoidDelegate(function(g)
            CUIManager.ShowUI(CLuaUIResources.HengYuDangKouZhanLiPinWnd)
        end)
    else
        m_Button2:SetActive(false)
    end
end
