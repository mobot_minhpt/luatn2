local Object = import "System.Object"
local UIWidget          = import "UIWidget"
local UICamera          = import "UICamera"
local Vector3           = import "UnityEngine.Vector3"
local Physics           = import "UnityEngine.Physics"

LuaPlantTreeWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaPlantTreeWnd,   "LongPressBtn",             GameObject)     --  长按按钮

RegistChildComponent(LuaPlantTreeWnd,   "TreeRoot_1",               UIWidget)       --  树节点
RegistChildComponent(LuaPlantTreeWnd,   "TreeRoot_2",               UIWidget)
RegistChildComponent(LuaPlantTreeWnd,   "TreeRoot_3",               UIWidget)
RegistChildComponent(LuaPlantTreeWnd,   "TreeRoot_4",               UIWidget)
RegistChildComponent(LuaPlantTreeWnd,   "TreeRoot_5",               UIWidget)
RegistChildComponent(LuaPlantTreeWnd,   "TreeRoot_5",               UIWidget)

RegistChildComponent(LuaPlantTreeWnd,   "leaf2_1",                  UIWidget)       --  叶子节点
RegistChildComponent(LuaPlantTreeWnd,   "leaf3_1",                  UIWidget)
RegistChildComponent(LuaPlantTreeWnd,   "leaf3_2",                  UIWidget)

RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect2_1",           GameObject)     --  特效节点

RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect3_1",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect3_2",           GameObject)

RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect4_1",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect4_2",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect4_3",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect4_4",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect4_5",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect4_6",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "ClickEffect4_7",           GameObject)

RegistChildComponent(LuaPlantTreeWnd,   "CompleteEffect",           GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "TouchBg",                  GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "Tips",                     GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "Finger_1",                   GameObject)
RegistChildComponent(LuaPlantTreeWnd,   "Finger_2",                   GameObject)

---------RegistClassMember-------
RegistClassMember(LuaPlantTreeWnd,      "m_TimeTick")

RegistClassMember(LuaPlantTreeWnd,      "m_FadeInTime")
RegistClassMember(LuaPlantTreeWnd,      "m_AppearTime") 

RegistClassMember(LuaPlantTreeWnd,      "m_ClickedNum")     --  用来记录当前阶段已点击的特效数量
RegistClassMember(LuaPlantTreeWnd,      "m_EffectNum")      --  当前接段需要点击的特效数量

RegistClassMember(LuaPlantTreeWnd,      "m_ClickEffect4Table")

LuaPlantTreeWnd.taskId = nil

function LuaPlantTreeWnd:Awake()
    self.m_FadeInTime = 2   --  渐现所需时间
    self.m_AppearTime = 1   --  新树出现时间间隔

    self.ClickEffect2_1:SetActive(false)
    self.ClickEffect3_1:SetActive(false)
    self.ClickEffect3_2:SetActive(false)
    self.CompleteEffect:SetActive(false)
    self.leaf2_1.alpha = 0
    self.leaf3_1.alpha = 0
    self.leaf3_2.alpha = 0

    self.m_EffectNum = 0
    self.m_ClickedNum = 0

    self.m_ClickEffect4Table = {self.ClickEffect4_1,self.ClickEffect4_2,self.ClickEffect4_3,self.ClickEffect4_4,self.ClickEffect4_5,self.ClickEffect4_6,self.ClickEffect4_7}

    for i=1,#self.m_ClickEffect4Table do
        self.m_ClickEffect4Table[i]:SetActive(false)
    end
    self.Finger_1:SetActive(false)
    self.Finger_2:SetActive(false)
    self.TouchBg:SetActive(false)

    UIEventListener.Get(self.LongPressBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPress(go, isPressed)
    end)
end 

function LuaPlantTreeWnd:OnDisable()
    UnRegisterTick(self.m_TimeTick)
end

function LuaPlantTreeWnd:OnPress(go,isPressed)
    UnRegisterTick(self.m_TimeTick)
    if isPressed then
        self.m_TimeTick = RegisterTickOnce(function ()
            self:ShowTreeRoot1()
        end, self.m_AppearTime * 1000)
    end
end

function LuaPlantTreeWnd:ShowTreeRoot1()        --  显示小树苗  第一阶段
    UnRegisterTick(self.m_TimeTick)
    self.Tips:SetActive(false)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_1, 0,1, self.m_FadeInTime,function() 
        self.m_TimeTick = RegisterTickOnce(function ()
            self:ShowTreeRoot2()
        end, self.m_AppearTime * 1000)
    end)
end

function LuaPlantTreeWnd:ShowTreeRoot2()        --  显示小树  第二阶段  /一个树枝
    UnRegisterTick(self.m_TimeTick)
    self.m_EffectNum = 1
    self.m_ClickedNum = 0
    LuaTweenUtils.TweenAlpha(self.TreeRoot_1, 1,0, self.m_FadeInTime,function() end)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_2, 0,1, self.m_FadeInTime,function() 
        self.Finger_1:SetActive(true)
        self.ClickEffect2_1:SetActive(true)
    end)

    UIEventListener.Get(self.ClickEffect2_1).onClick = DelegateFactory.VoidDelegate(function(p)
        self.Finger_1:SetActive(false)
        self:EffectBtnClick(self.ClickEffect2_1,self.leaf2_1,3)
    end)
end

function LuaPlantTreeWnd:ShowTreeRoot3()        --  显示小树  第三阶段 /两个树枝
    self.m_EffectNum = 2
    self.m_ClickedNum = 0
    UnRegisterTick(self.m_TimeTick)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_2, 1,0, self.m_FadeInTime,function() 

    end)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_3, 0,1, self.m_FadeInTime,function() 
        self.ClickEffect3_1:SetActive(true)
        self.ClickEffect3_2:SetActive(true)
    end)
    UIEventListener.Get(self.ClickEffect3_1).onClick = DelegateFactory.VoidDelegate(function(p)
        self:EffectBtnClick(self.ClickEffect3_1,self.leaf3_1,4)
    end)
    UIEventListener.Get(self.ClickEffect3_2).onClick = DelegateFactory.VoidDelegate(function(p)
        self:EffectBtnClick(self.ClickEffect3_2,self.leaf3_2,4)
    end)
end

function LuaPlantTreeWnd:ShowTreeRoot4()    --  显示大树 第四阶段   /七个特效
    self.m_EffectNum = 7
    self.m_ClickedNum = 0
    UnRegisterTick(self.m_TimeTick)
    self.TouchBg:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_3, 1,0, self.m_FadeInTime,function() 

    end)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_4, 0,1, self.m_FadeInTime,function() 
        for i=1,#self.m_ClickEffect4Table do
            self.m_ClickEffect4Table[i]:SetActive(true)
        end
        self.Finger_2:SetActive(true)
    end)
    
    UIEventListener.Get(self.TouchBg).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnTouch()
    end)
    UIEventListener.Get(self.TouchBg).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
        self:OnTouch()
    end)
end

function LuaPlantTreeWnd:ShowTreeRoot5()        --  显示金色大树 第五阶段   /落叶特效
    UnRegisterTick(self.m_TimeTick)
    self.CompleteEffect:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_4, 1,0, self.m_FadeInTime,function() 

    end)
    LuaTweenUtils.TweenAlpha(self.TreeRoot_5, 0,1, self.m_FadeInTime,function() 
        self.m_TimeTick = RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.PlantTreeWnd)
        end, 5 * 1000)
    end)
end

function LuaPlantTreeWnd:OnTouch()
    local currentPos = UICamera.currentTouch.pos
    local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
    local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
    
    local HitNum = 0
    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        if (string.find(collider.name, "ClickEffect4_", 1, true) ~= nil) then
            collider:SetActive(false)
            if self.Finger_2.activeSelf then
                self.Finger_2:SetActive(false)
            end
            if self:CheckOver() then
                self.m_TimeTick = RegisterTickOnce(function ()
                    self:ShowTreeRoot5()
                    local empty = CreateFromClass(MakeGenericClass(List, Object))
                    Gac2Gas.FinishClientTaskEvent(LuaPlantTreeWnd.taskId,"PlantTree",MsgPackImpl.pack(empty))
                end, self.m_AppearTime * 1000)
            end
        end
    end
end

function LuaPlantTreeWnd:EffectBtnClick(ClickEffect,leaf,stage)     --  点击枝干特效回调，stage 表示接下来要展示的阶段
    ClickEffect:SetActive(false)
    LuaTweenUtils.TweenAlpha(leaf, 0,1, self.m_FadeInTime,function()
        if self:CheckOver() then
            self.m_TimeTick = RegisterTickOnce(function ()
                if stage == 3 then
                    self:ShowTreeRoot3()
                elseif  stage == 4 then
                    self:ShowTreeRoot4()
                end
            end, self.m_AppearTime * 1000)
        end
    end)
end

function LuaPlantTreeWnd:CheckOver()        --  检测特效是否点完了
    self.m_ClickedNum = self.m_ClickedNum + 1
    if self.m_ClickedNum >= self.m_EffectNum then
        return true
    end
    return false
end