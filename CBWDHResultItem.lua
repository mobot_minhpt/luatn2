-- Auto Generated!!
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHResultItem = import "L10.UI.CBWDHResultItem"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local EnumClass = import "L10.Game.EnumClass"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
CBWDHResultItem.m_Init_CS2LuaHook = function (this, round, leftData, rightData, winTeamId, leftTeamId, rightTeamId) 
    --没有获胜队伍，说明还没有比赛
    --没有获胜队伍，说明还没有比赛
    if winTeamId == 0 then
        this.resultSprite1.enabled = false
        this.resultSprite2.enabled = false
        this.teamLeaderSprite1.enabled = false
        this.teamleaderSprite2.enabled = false

        this.skillMarkSprite1.enabled = false
        this.skillMarkSprite2.enabled = false
    else
        this.resultSprite1.enabled = true
        this.resultSprite2.enabled = true
        --左侧队伍胜利
        if winTeamId == leftTeamId then
            this.resultSprite1.spriteName = Constants.WinSprite
            this.resultSprite2.spriteName = Constants.LoseSprite
        elseif winTeamId == rightTeamId then
            this.resultSprite1.spriteName = Constants.LoseSprite
            this.resultSprite2.spriteName = Constants.WinSprite
        end
    end

    this.roundLabel.text = System.String.Format(LocalString.GetString("第{0}场"), round)

    local isTeamRound = CBiWuDaHuiMgr.IsTeamRound(round)
    if not isTeamRound then
        this.teamLeaderSprite1.enabled = false
        this.teamleaderSprite2.enabled = false

        if leftData ~= nil then
            this.skillMarkSprite1.enabled = true
            if leftData.cls == 0 then
                this.skillMarkSprite1.spriteName = ""
            else
                this.skillMarkSprite1.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), leftData.cls))
            end
        else
            this.skillMarkSprite1.enabled = false
        end

        if rightData ~= nil then
            this.skillMarkSprite2.enabled = true
            if rightData.cls == 0 then
                this.skillMarkSprite2.spriteName = ""
            else
                this.skillMarkSprite2.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), rightData.cls))
            end
        else
            this.skillMarkSprite2.enabled = false
        end
    else
        this.teamLeaderSprite1.enabled = true
        this.teamleaderSprite2.enabled = true
        this.skillMarkSprite1.enabled = false
        this.skillMarkSprite2.enabled = false
    end

    if isTeamRound then
        this.battleTypeSprite.spriteName = CBWDHResultItem.TeamSpriteName
        this.battleTypeSprite:MakePixelPerfect()
    else
        this.battleTypeSprite.spriteName = CBWDHResultItem.PersonalSpriteName
        this.battleTypeSprite:MakePixelPerfect()
    end

    if leftData ~= nil then
        this.dpsLabel1.text = tostring(leftData.dpsNum)
        this.killNumLabel1.text = tostring(leftData.killNum)
        this.nameLabel1.text = leftData.playerName
        this.nameLabel1.color = Color.white
    else
        this.dpsLabel1.text = ""
        this.killNumLabel1.text = ""
        this.nameLabel1.text = LocalString.GetString("<空缺>")
        this.nameLabel1.color = Color.gray

        this.teamLeaderSprite1.enabled = false
    end

    if rightData ~= nil then
        this.dpsLabel2.text = tostring(rightData.dpsNum)
        this.killNumLabel2.text = tostring(rightData.killNum)
        this.nameLabel2.text = rightData.playerName
        this.nameLabel2.color = Color.white
    else
        this.dpsLabel2.text = ""
        this.killNumLabel2.text = ""
        this.nameLabel2.text = LocalString.GetString("<空缺>")
        this.nameLabel2.color = Color.gray

        this.teamleaderSprite2.enabled = false
    end
end
