-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTitleDetailView = import "L10.UI.CTitleDetailView"
local CTitleMgr = import "L10.Game.CTitleMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local Title_Title = import "L10.Game.Title_Title"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTitleDetailView.m_Init_CS2LuaHook = function (this, sectionIdx, rowIdx) 

    if CClientMainPlayer.Inst == nil then
        return
    end
    if sectionIdx >= 0 and sectionIdx < CTitleMgr.Inst.SectionList.Count and rowIdx >= 0 and rowIdx < CTitleMgr.Inst.SectionList[sectionIdx].RowList.Count then
        this.SectionIndex = sectionIdx
        this.RowIndex = rowIdx
        local titleBaseInfo = CTitleMgr.Inst.SectionList[sectionIdx].RowList[rowIdx]

        this.title = Title_Title.GetData(titleBaseInfo.TitleID)
        local hasOwnTitle = CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.Titles, typeof(UInt32), this.title.ID)

        --称号预览
        if this.title.IsDynamicName == 0 then
            this.previewNameLabel.text = titleBaseInfo.TitleName
        elseif CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.DynamicTitleNames, typeof(UInt32), this.title.ID) then
            this.previewNameLabel.text = LocalString.TranslateAndFormatText( CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.DynamicTitleNames, typeof(UInt32), this.title.ID) )
        end
        this.previewNameLabel.color = CTitleMgr.Inst:GetTitleColor(this.title)
        this.displayTitleButton:SetActive(hasOwnTitle)

        -- 称号图标
        local originWidth = 462
        if not System.String.IsNullOrEmpty(this.title.SmallIcon) then
            this.previewNameLabel.width = originWidth - 70
            this.previewNameLabel.text = "    " .. this.previewNameLabel.text

            this.previewTitleIcon.gameObject:SetActive(true)
            this.previewTitleIcon.gameObject.transform.localPosition = Vector3(20- this.previewNameLabel.printedSize.x/2, 0, 0)
            this.previewTitleIcon.spriteName = this.title.SmallIcon
        else
            this.previewTitleIcon.gameObject:SetActive(false)
            this.previewNameLabel.width = originWidth
        end

        --是否拥有称号
        local default
        if CClientMainPlayer.Inst.PlayProp.ShowTitleId == this.title.ID then
            default = LocalString.GetString("取消佩戴")
        else
            default = LocalString.GetString("佩戴称号")
        end
        this.displayTitleButtonLabel.text = default
        this.putonTag:SetActive(CClientMainPlayer.Inst.PlayProp.ShowTitleId == this.title.ID)
        if hasOwnTitle then
            if CClientMainPlayer.Inst.PlayProp.ShowTitleId == this.title.ID then
                UIEventListener.Get(this.displayTitleButton).onClick = MakeDelegateFromCSFunction(this.OnCancelDisplay, VoidDelegate, this)
            else
                UIEventListener.Get(this.displayTitleButton).onClick = MakeDelegateFromCSFunction(this.OnDisplayButtonClick, VoidDelegate, this)
            end
        end

        --称号描述
        if this.title.AvailableTime == 0 then
            this.dateLabel.text = LocalString.GetString("永久称号")
        else
            if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.Titles, typeof(UInt32), this.title.ID) then
                local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.Titles, typeof(UInt32), this.title.ID) + this.title.AvailableTime * 3600)
                this.dateLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("有效期至：{0}-{1}-{2} {3}:{4}:{5}"), {
                    this:FormatTime(endTime.Year), 
                    this:FormatTime(endTime.Month), 
                    this:FormatTime(endTime.Day), 
                    this:FormatTime(endTime.Hour), 
                    this:FormatTime(endTime.Minute), 
                    this:FormatTime(endTime.Second)
                })
            else
                if this.title.AvailableTime < 24 then
                    this.dateLabel.text = System.String.Format(LocalString.GetString("有效期：{0}小时"), this.title.AvailableTime)
                else
                    this.dateLabel.text = System.String.Format(LocalString.GetString("有效期：{0}天"), math.floor(math.floor(this.title.AvailableTime / 24)))
                end
            end
        end
        this.titleDesLabel.text = CChatLinkMgr.TranslateToNGUIText(this.title.titleDesc, false)
        this.titleDesBackground.height = - math.floor(this.titleDesLabel.transform.localPosition.y) + this.titleDesLabel.height + 38

        --属性描述
        this.propertyDesLabel.text = this.title.AttrDesc
        this.equipPropertyButton:SetActive(hasOwnTitle and this.title.AttrDesc ~= LocalString.GetString("无"))
        --是否拥有称号
        local extern
        if CClientMainPlayer.Inst.PlayProp.AttrTitleId == this.title.ID then
            extern = LocalString.GetString("取消激活")
        else
            extern = LocalString.GetString("激活属性")
        end
        this.equipPropertyButton:SetActive(this.title.AttrDesc ~= LocalString.GetString("无属性加成") and this.title.AttrDesc ~= nil)

        this.equipPropertyButtonLabel.text = extern
        this.activateTag:SetActive(CClientMainPlayer.Inst.PlayProp.AttrTitleId == this.title.ID)
        this.propertyDesBackground.height = - math.floor(this.propertyDesLabel.transform.localPosition.y) + this.propertyDesLabel.height + 38
        if hasOwnTitle then
            if CClientMainPlayer.Inst.PlayProp.AttrTitleId == this.title.ID then
                UIEventListener.Get(this.equipPropertyButton).onClick = MakeDelegateFromCSFunction(this.OnCancelPropertyEquip, VoidDelegate, this)
            else
                UIEventListener.Get(this.equipPropertyButton).onClick = MakeDelegateFromCSFunction(this.OnPropertyEquipButtonClick, VoidDelegate, this)
            end
        end

        --获得方式
        this.aquireLabel.text = CChatLinkMgr.TranslateToNGUIText(this.title.ObtainDesc, false)
        this.aquireBackground.height = - math.floor(this.aquireLabel.transform.localPosition.y) + this.aquireLabel.height + 38

        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
CTitleDetailView.m_OnDisplayButtonClick_CS2LuaHook = function (this, go) 

    --佩戴称号
    Gac2Gas.RequestSetTitle(this.title.ID, CClientMainPlayer.Inst.PlayProp.AttrTitleId)
    if CGuanNingMgr.Inst.inGnjc or CLuaAnQiDaoMgr.IsInPlay() then
        g_MessageMgr:ShowMessage("Title_Changed_In_Guanning")
    end
end
