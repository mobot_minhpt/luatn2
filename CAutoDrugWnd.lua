-- Auto Generated!!
local AutoDrugWndMgr = import "L10.UI.AutoDrugWndMgr"
local CAutoDrugWnd = import "L10.UI.CAutoDrugWnd"
local CGameSettingMgr = import "L10.Game.CGameSettingMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local GameObject = import "UnityEngine.GameObject"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CAutoDrugWnd.m_Init_CS2LuaHook = function (this)
    --throw new System.NotImplementedException();
    --throw new System.NotImplementedException();
    --if CGameSettingMgr.Inst:GetAutoHealInfo() == nil or CGameSettingMgr.Inst:GetAutoHealInfo().Length ~= 7 then
    if CGameSettingMgr.Inst:GetAutoHealInfo() == nil or CGameSettingMgr.Inst:GetAutoHealInfo().Length < 6 then
        CGameSettingMgr.Inst:SetAutoHealInfo("1,50,0,50,0,1")
        this.AutoDrugMidWnd:InitButton()
    end

    this.AutoDrugMidWnd:InitFood()

    if AutoDrugWndMgr.CurrentHpFood ~= nil or AutoDrugWndMgr.CurrentMpFood ~= nil then
        this.AutoDrugMidWnd:SetFood(AutoDrugWndMgr.CurrentHpFood, AutoDrugWndMgr.CurrentMpFood)
    end

    this.AutoDrugMidWnd:InitFx()
end
CAutoDrugWnd.m_OnEnable_CS2LuaHook = function (this)
    this.AutoDrugMidWnd.OpenDrugItemList = CommonDefs.CombineListner_Action_GameObject(this.AutoDrugMidWnd.OpenDrugItemList, MakeDelegateFromCSFunction(this.OpenDrugItemList, MakeGenericClass(Action1, GameObject), this), true)
    if this.CloseBtn ~= nil then
        UIEventListener.Get(this.CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CloseBtn).onClick, MakeDelegateFromCSFunction(this.onCloseBtnClick, VoidDelegate, this), true)
    end
    AutoDrugWndMgr.SetAutoDrugFinished = CommonDefs.CombineListner_Action_Item_Item_Item_Item(AutoDrugWndMgr.SetAutoDrugFinished, MakeDelegateFromCSFunction(this.SetFood, MakeGenericClass(Action2, Item_Item, Item_Item), this), true)
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CAutoDrugWnd.m_OnDisable_CS2LuaHook = function (this)

    this.AutoDrugMidWnd.OpenDrugItemList = CommonDefs.CombineListner_Action_GameObject(this.AutoDrugMidWnd.OpenDrugItemList, MakeDelegateFromCSFunction(this.OpenDrugItemList, MakeGenericClass(Action1, GameObject), this), false)
    AutoDrugWndMgr.SetAutoDrugFinished = CommonDefs.CombineListner_Action_Item_Item_Item_Item(AutoDrugWndMgr.SetAutoDrugFinished, MakeDelegateFromCSFunction(this.SetFood, MakeGenericClass(Action2, Item_Item, Item_Item), this), false)
    if this.CloseBtn ~= nil then
        UIEventListener.Get(this.CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CloseBtn).onClick, MakeDelegateFromCSFunction(this.onCloseBtnClick, VoidDelegate, this), false)
    end

    this:SetAutoHeal()
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CAutoDrugWnd.m_onCloseBtnClick_CS2LuaHook = function (this, go)
    this:Close()
    this:SetAutoHeal()
end
CAutoDrugWnd.m_SetAutoHeal_CS2LuaHook = function (this)
    local AutoHealstring = {}
    if this.AutoDrugMidWnd.isHpAutoOn then
        AutoHealstring[0+1] = "1"
        EventManager.BroadcastInternalForLua(EnumEventType.OnAutoDrugStateChange, {true, true})
    else
        AutoHealstring[0+1] = "0"
        EventManager.BroadcastInternalForLua(EnumEventType.OnAutoDrugStateChange, {true, false})
    end
    if this.AutoDrugMidWnd:GetHpThreshold() ~= nil then
        AutoHealstring[1+1] = this.AutoDrugMidWnd:GetHpThreshold()
    else
        AutoHealstring[1+1] = "50"
    end
    if this.AutoDrugMidWnd:GetMpThreshold() ~= nil then
        AutoHealstring[3+1] = this.AutoDrugMidWnd:GetMpThreshold()
    else
        AutoHealstring[3+1] = "50"
    end

    if AutoDrugWndMgr.CurrentHpFood ~= nil then
        AutoHealstring[2+1] = tostring(AutoDrugWndMgr.CurrentHpFood.ID)
    else
        AutoHealstring[2+1] = "0"
    end
    if AutoDrugWndMgr.CurrentMpFood ~= nil then
        AutoHealstring[4+1] = tostring(AutoDrugWndMgr.CurrentMpFood.ID)
    else
        AutoHealstring[4+1] = "0"
    end
    if this.AutoDrugMidWnd.isMpAutoOn then
        AutoHealstring[5+1] = "1"
        EventManager.BroadcastInternalForLua(EnumEventType.OnAutoDrugStateChange, {false, true})
    else
        AutoHealstring[5+1] = "0"
        EventManager.BroadcastInternalForLua(EnumEventType.OnAutoDrugStateChange, {false, false})
    end
    if this.AutoDrugMidWnd.isAutoAddDrugOn then
        AutoHealstring[6+1] = '1'
    else
        AutoHealstring[6+1] = '0'
    end
    CGameSettingMgr.Inst:SetAutoHealInfo(table.concat( AutoHealstring, ","))
    local temp = CGameSettingMgr.Inst:GetAutoHealInfo()
end
CAutoDrugWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName)
    if methodName == "GetHpItemButton" then
        return this:GetHpItemButton()
    elseif methodName == "GetMpItemButton" then
        return this:GetMpItemButton()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end

AutoDrugWndMgr.m_SetCurrentFood_CS2LuaHook = function (hpTemplateId, mpTemplateId)
    if hpTemplateId ~= 0 then
        local ItemUnit = CItemMgr.Inst:GetItemTemplate(hpTemplateId)
        AutoDrugWndMgr.CurrentHpFood = ItemUnit
    else
        AutoDrugWndMgr.CurrentHpFood = nil
    end
    if mpTemplateId ~= 0 then
        local ItemUnit = CItemMgr.Inst:GetItemTemplate(mpTemplateId)
        AutoDrugWndMgr.CurrentMpFood = ItemUnit
    else
        AutoDrugWndMgr.CurrentMpFood = nil
    end
    if AutoDrugWndMgr.SetAutoDrugFinished ~= nil then
        GenericDelegateInvoke(AutoDrugWndMgr.SetAutoDrugFinished, Table2ArrayWithCount({AutoDrugWndMgr.CurrentHpFood, AutoDrugWndMgr.CurrentMpFood}, 2, MakeArrayClass(Object)))
    end
end
