
LuaCommonItemChooseMgr = {}
LuaCommonItemChooseMgr.m_TitleStr = nil
LuaCommonItemChooseMgr.m_BtnStr = nil
LuaCommonItemChooseMgr.m_DescStr = nil
-- 没有可提交物品时显示的字符串
LuaCommonItemChooseMgr.m_VoidStr = nil
--ItemSelectFunc：
--用法：筛选需要显示的物品，默认行为为显示包裹里所有物品
--参数：itemId:string
--返回值：true显示该物品，false不显示
LuaCommonItemChooseMgr.m_ItemSelectFunc = nil
--ItemSortFunc:
--用法：排序显示的物品，默认顺序参照玩家包裹
--参数：itemId1:string, itemId2:string
LuaCommonItemChooseMgr.m_ItemSort = nil
--ItemInitFunc:
--用法：初始化物品cell的显示，默认行为参照玩家包裹，该函数会可以在默认行为之上对cell做更改
--参数：itemId:string, itemCell:CPackageItemCell
LuaCommonItemChooseMgr.m_ItemInit = nil
--ItemClickFunc:
--用法：点击物品时用来根据物品信息更新窗口的描述信息，默认不更新
--参数：descLabel:UILabel, selectedItems:lua—table[itemid, itemid2](itemId为string类型，单选模式时只有一个itemid)
LuaCommonItemChooseMgr.m_ItemClickFunc = nil
--CommitFunc:
--用法：点击提交按钮的行为，无默认行为
--参数：selectedItems:lua—table[itemid, itemid2](itemId为string类型，单选模式时只有一个itemid)
LuaCommonItemChooseMgr.m_CommitFunc = nil
--EnableMultiSelect:
--用法：true为多选模式，false为单选模式
LuaCommonItemChooseMgr.m_EnableMultiSelect = nil
--AccessItemTemplateIds
--用法：放入需要显示的item的templateId
LuaCommonItemChooseMgr.m_AccessItemTemplateIds = {}
--用来显示按钮
LuaCommonItemChooseMgr.m_TipMsg = nil

function LuaCommonItemChooseMgr:ShowItemChooseWnd(titleStr, btnStr, descStr, enableMultiSelect, itemSelectFunc, sortFunc, initFunc, commitFunc, clickFunc, voidStr, accessItemTemplateIds, tipMsg)
    self.m_TitleStr = titleStr
    self.m_BtnStr = btnStr
    self.m_DescStr = descStr
    self.m_EnableMultiSelect = enableMultiSelect
    self.m_ItemInit = initFunc
    self.m_ItemSort = sortFunc
    self.m_ItemSelectFunc = itemSelectFunc
    self.m_CommitFunc = commitFunc
    self.m_ItemClickFunc = clickFunc
    self.m_VoidStr = voidStr
    self.m_AccessItemTemplateIds = accessItemTemplateIds ~= nil and accessItemTemplateIds or {}
    self.m_TipMsg = tipMsg
    CUIManager.ShowUI(CLuaUIResources.CommonItemChooseWnd)
end

-- 关闭窗口时调用
function LuaCommonItemChooseMgr:Reset()
    self.m_TitleStr = nil
    self.m_BtnStr = nil
    self.m_DescStr = nil
    self.m_CommitFunc = nil
    self.m_ClickFunc = nil
    self.m_ItemSelectFunc = nil
    self.m_AccessItemTemplateIds = {}
    self.m_TipMsg = nil
end
