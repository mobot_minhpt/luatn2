local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITable = import "UITable"
local QnCheckBox = import "L10.UI.QnCheckBox"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local Animation = import "UnityEngine.Animation"
local PlayerSettings = import "L10.Game.PlayerSettings"
local SoundManager = import "SoundManager"

LuaShuangshiyi2023DiscountWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "ActivityTime", "ActivityTime", UILabel)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "DiscountTips", "DiscountTips", UILabel)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "CouponsList", "CouponsList", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "CouponsTable", "CouponsTable", UITable)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "Money", "Money", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "Money1", "Money1", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "Money2", "Money2", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "Money3", "Money3", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "Money4", "Money4", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "MyCouponsButton", "MyCouponsButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "OneDrawButton", "OneDrawButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "FiveDrawButton", "FiveDrawButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "SkipAnimation", "SkipAnimation", QnCheckBox)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountWnd, "CouponsGrid", "CouponsGrid", GameObject)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023DiscountWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaShuangshiyi2023DiscountWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023DiscountWnd:OnEnable()
    g_ScriptEvent:AddListener("Show2023Double11DiscountCouponsResult", self, "OnShowDrawResult")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "RefreshVariableUI")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "RefreshVariableUI")

end

function  LuaShuangshiyi2023DiscountWnd:OnDisable()
    g_ScriptEvent:RemoveListener("Show2023Double11DiscountCouponsResult", self, "OnShowDrawResult")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "RefreshVariableUI")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "RefreshVariableUI")

    if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
end

function LuaShuangshiyi2023DiscountWnd:OnShowDrawResult(itemList)
    if PlayerSettings.SoundEnabled then
        local sound = "event:/L10/L10_UI/2023ShiZhuangChouJiang/2023ShiZhuangChouJiang_6s"
        if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
        self.m_LotteryResultSound = SoundManager.Inst:PlayOneShot(sound, Vector3.zero, nil, 0)
    end
    
    if self.aniComp then
        self.isOneDraw = #itemList == 1
        if self.isOneDraw then
            self.aniComp:Play("shuangshiyi2023discountwnd_chouka01")
        else
            self.aniComp:Play("shuangshiyi2023discountwnd_chouka10")
        end
    end
    
    local couponTypeIconList = {}
    local rawData = Double11_DiscountCouponsSetting.GetData().CouponsTypeIcon
    local rawDataList = g_LuaUtil:StrSplit(rawData, ";")
    for i = 1, #rawDataList do
        if rawDataList[i] ~= "" then
            table.insert(couponTypeIconList, rawDataList[i])
        end
    end

    if #itemList == 1 then
        --激活第三个
        local templateItem = self.CouponsGrid.transform:Find("CouponsItem" .. 3)
        local couponData = Double11_ExteriorDiscountCoupons.GetData(itemList[1])
        local couponType = couponData.Type
        local name = couponData.Name
        local quality = couponData.Quality
        local typeIcon = couponTypeIconList[couponType]

        local textureComp = templateItem.transform:Find("Card"):GetComponent(typeof(CUITexture))
        local icon = self.bigCouponsList[quality].icon
        textureComp:LoadMaterial(icon)
        templateItem.transform:Find("Name"):GetComponent(typeof(UILabel)).text = name
        templateItem.transform:Find("TypeIcon/san").gameObject:SetActive(couponType == 1)
        templateItem.transform:Find("TypeIcon/zuoji").gameObject:SetActive(couponType == 2)
        templateItem.transform:Find("TypeIcon/beishi").gameObject:SetActive(couponType == 3)
        templateItem.transform:Find("TypeIcon/san"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
        templateItem.transform:Find("TypeIcon/zuoji"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
        templateItem.transform:Find("TypeIcon/beishi"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
        
        templateItem.transform:Find("Quality5Vfx").gameObject:SetActive(quality == 5)
        templateItem.transform:Find("Quality6Vfx").gameObject:SetActive(quality == 6)
    else
        for i = 1, 5 do
            local templateItem = self.CouponsGrid.transform:Find("CouponsItem" .. i)
            local couponData = Double11_ExteriorDiscountCoupons.GetData(itemList[i])
            local couponType = couponData.Type
            local name = couponData.Name
            local quality = couponData.Quality
            local typeIcon = couponTypeIconList[couponType]

            local textureComp = templateItem.transform:Find("Card"):GetComponent(typeof(CUITexture))
            local icon = self.bigCouponsList[quality].icon
            textureComp:LoadMaterial(icon)
            templateItem.transform:Find("Name"):GetComponent(typeof(UILabel)).text = name
            templateItem.transform:Find("TypeIcon/san").gameObject:SetActive(couponType == 1)
            templateItem.transform:Find("TypeIcon/zuoji").gameObject:SetActive(couponType == 2)
            templateItem.transform:Find("TypeIcon/beishi").gameObject:SetActive(couponType == 3)
            templateItem.transform:Find("TypeIcon/san"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
            templateItem.transform:Find("TypeIcon/zuoji"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
            templateItem.transform:Find("TypeIcon/beishi"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]

            templateItem.transform:Find("Quality5Vfx").gameObject:SetActive(quality == 5)
            templateItem.transform:Find("Quality6Vfx").gameObject:SetActive(quality == 6)
        end
    end
end


function LuaShuangshiyi2023DiscountWnd:InitWndData()
    self.isOneDraw = false
    
    self.couponsList = {}
    local rawCouponsData = Double11_DiscountCouponsSetting.GetData().SmallIconList
    local couponsList = g_LuaUtil:StrSplit(rawCouponsData, ";")
    for i = 1, #couponsList do
        if couponsList[i] ~= "" then
            local couponsData = g_LuaUtil:StrSplit(couponsList[i], ",")
            local coupons = {}
            coupons.name = couponsData[1]
            coupons.icon = couponsData[2]
            table.insert(self.couponsList, coupons)
        end
    end

    self.bigCouponsList = {}
    rawCouponsData = Double11_DiscountCouponsSetting.GetData().BigIconList
    couponsList = g_LuaUtil:StrSplit(rawCouponsData, ";")
    for i = 1, #couponsList do
        if couponsList[i] ~= "" then
            local couponsData = g_LuaUtil:StrSplit(couponsList[i], ",")
            local coupons = {}
            coupons.name = couponsData[1]
            coupons.icon = couponsData[2]
            table.insert(self.bigCouponsList, coupons)
        end
    end

    self.drawCostData = {}
    local rawDrawCostData = Double11_DiscountCouponsSetting.GetData().SingleDrawCost
    local costList = g_LuaUtil:StrSplit(rawDrawCostData, ";")
    for i = 1, #costList do
        if costList[i] ~= "" then
            local costData = g_LuaUtil:StrSplit(costList[i], ",")
            local cost = {}
            for moneyIndex = 1, 4 do
                cost[moneyIndex] = tonumber(costData[moneyIndex])
            end
            table.insert(self.drawCostData, cost)
        end
    end
    
    self.aniComp = self.transform:GetComponent(typeof(Animation))
end 

function LuaShuangshiyi2023DiscountWnd:RefreshConstUI()
    self.ActivityTime.text = g_MessageMgr:FormatMessage("Double11_2023Discount_ActivityTime")
    self.DiscountTips.text = g_MessageMgr:FormatMessage("Double11_2023Discount_DiscountTips")
    
    for i = 1, #self.couponsList do
        local conponsData = self.couponsList[i]
        local templateItem = self.CouponsTable.transform:Find("Card0" .. i)
        local textureComp = templateItem.transform:GetComponent(typeof(CUITexture))
        textureComp:LoadMaterial(conponsData.icon)
        templateItem.transform:Find("Name"):GetComponent(typeof(UILabel)).text = conponsData.name
    end
end

function LuaShuangshiyi2023DiscountWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eDouble11CouponsSingleDrawTimes then
        self:RefreshVariableUI()
    end
end

function LuaShuangshiyi2023DiscountWnd:RefreshVariableUI()
    --refresh money
    if CClientMainPlayer.Inst then
        self.Money1.transform:GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.FreeSilver
        self.Money2.transform:GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Silver
        self.Money3.transform:GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.YuanBao
        self.Money4.transform:GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
        self.Money4.transform:Find("BindLingyuSprite").gameObject:SetActive(CClientMainPlayer.Inst.BindJade > 0)
    end
    self.FiveDrawButton.transform:Find("Consume/ConsumeNumber"):GetComponent(typeof(UILabel)).text = Double11_DiscountCouponsSetting.GetData().FiveTimesDrawCost
    
    --refresh one draw cost
    self.oneDrawTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11CouponsSingleDrawTimes) or 0
    local freeDrawTimes = 0
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        freeDrawTimes = list[3] and list[3] or 0
    end
    if self.oneDrawTimes == 0 or freeDrawTimes > 0 then
        --免费
        self.OneDrawButton.transform:Find("Free").gameObject:SetActive(true)
        local drawText = freeDrawTimes + (self.oneDrawTimes == 0 and 1 or 0)
        self.OneDrawButton.transform:Find("Free"):GetComponent(typeof(UILabel)).text = LocalString.GetString("免费次数：") .. drawText .. LocalString.GetString("次")
        self.OneDrawButton.transform:Find("Consume").gameObject:SetActive(false)
    elseif self.oneDrawTimes == 1 then
        --银票
        self.OneDrawButton.transform:Find("Free").gameObject:SetActive(false)
        self.OneDrawButton.transform:Find("Consume").gameObject:SetActive(true)
        self.OneDrawButton.transform:Find("Consume/Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_yinpiao"
        self.OneDrawButton.transform:Find("Consume/ConsumeNumber"):GetComponent(typeof(UILabel)).text = self.drawCostData[2][self.oneDrawTimes]

    elseif self.oneDrawTimes == 2 then
        --银两
        self.OneDrawButton.transform:Find("Free").gameObject:SetActive(false)
        self.OneDrawButton.transform:Find("Consume").gameObject:SetActive(true)
        self.OneDrawButton.transform:Find("Consume/Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_yinliang"
        self.OneDrawButton.transform:Find("Consume/ConsumeNumber"):GetComponent(typeof(UILabel)).text = self.drawCostData[3][self.oneDrawTimes]
    elseif self.oneDrawTimes == 3 then
        --元宝
        self.OneDrawButton.transform:Find("Free").gameObject:SetActive(false)
        self.OneDrawButton.transform:Find("Consume").gameObject:SetActive(true)
        self.OneDrawButton.transform:Find("Consume/Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_yuanbao"
        self.OneDrawButton.transform:Find("Consume/ConsumeNumber"):GetComponent(typeof(UILabel)).text = self.drawCostData[4][self.oneDrawTimes]
    else    
        --灵玉
        self.OneDrawButton.transform:Find("Free").gameObject:SetActive(false)
        self.OneDrawButton.transform:Find("Consume").gameObject:SetActive(true)
        self.OneDrawButton.transform:Find("Consume/Sprite"):GetComponent(typeof(UISprite)).spriteName = "packagewnd_lingyu"
        self.OneDrawButton.transform:Find("Consume/ConsumeNumber"):GetComponent(typeof(UILabel)).text = self.drawCostData[5][4]
    end

    --refresh SkipAnimation
    self.skipAnimationValue = 0
    local list = {}
    local json = CPlayerDataMgr.Inst:LoadPlayerData("shuangshiyi2023_defaultSetting")
    if json~=nil and json~="" then
        list = luaJson.json2lua(json)
    end
    if list then
        for key,v in pairs(list) do
            if key == "shuangshiyi2023_discount_skip_animation" then
                self.skipAnimationValue = v
            end
        end
    end
    self.SkipAnimation:SetSelected(self.skipAnimationValue == 1, false)
end

function LuaShuangshiyi2023DiscountWnd:InitUIEvent()
    UIEventListener.Get(self.Money2.transform:Find("AddBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
    end)

    UIEventListener.Get(self.Money3.transform:Find("AddBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CChargeWnd.OpenWndGettingYuanbao()
    end)

    UIEventListener.Get(self.Money4.transform:Find("AddBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CShopMallMgr.ShowChargeWnd()
    end)
    
    UIEventListener.Get(self.MyCouponsButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023DiscountPackageWnd)
    end)
    
    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_2023Discount_Rule")
    end)

    UIEventListener.Get(self.FiveDrawButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if CClientMainPlayer.Inst then
            if CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade >= Double11_DiscountCouponsSetting.GetData().FiveTimesDrawCost then
                Gac2Gas.Request2023Double11ExteriorDiscountCoupons(5)
            else
                local message = g_MessageMgr:FormatMessage("Double11_2023Discount_LingYu_NotEnough")
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                    CShopMallMgr.ShowChargeWnd()
                end), nil)
            end
        end
    end)

    UIEventListener.Get(self.OneDrawButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickOneDrawButton()
    end)
    

    self.SkipAnimation.OnValueChanged = DelegateFactory.Action_bool(function(value)
        local tbl = {shuangshiyi2023_discount_skip_animation = value and 1 or 0}
        local json = luaJson.table2json(tbl)
        CPlayerDataMgr.Inst:SavePlayerData("shuangshiyi2023_defaultSetting",json)
    end)
    
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function(_)
        if self.isOneDraw then
            self.aniComp:Play("shuangshiyi2023discountwnd_chouka01_fanhui")
        else
            self.aniComp:Play("shuangshiyi2023discountwnd_chouka10_fanhui")
        end
        if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
        g_MessageMgr:ShowMessage("ZheKou_ShouNA_Success")
    end)

    for i = 1, 5 do
        local templateItem = self.CouponsGrid.transform:Find("CouponsItem" .. i)
        UIEventListener.Get(templateItem.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
            g_MessageMgr:ShowMessage("Double11_2023Discount_Item_Tip")
        end)
    end
end

function LuaShuangshiyi2023DiscountWnd:OnClickOneDrawButton()
    self.oneDrawTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11CouponsSingleDrawTimes) or 0
    local freeDrawTimes = 0
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        freeDrawTimes = list[3] and list[3] or 0
    end
    if self.oneDrawTimes == 0 or freeDrawTimes > 0 then
        --免费
        Gac2Gas.Request2023Double11ExteriorDiscountCoupons(1)
    elseif self.oneDrawTimes == 1 then
        --银票
        local cost = self.drawCostData[2][self.oneDrawTimes]
        if CClientMainPlayer.Inst.FreeSilver >= cost then
            --银票够
            Gac2Gas.Request2023Double11ExteriorDiscountCoupons(1)
        else
            if CClientMainPlayer.Inst.FreeSilver + CClientMainPlayer.Inst.Silver >= cost then
                --银票+银两够
                local message = g_MessageMgr:FormatMessage("Double11_2023Discount_YinPiao_NotEnough", cost-CClientMainPlayer.Inst.FreeSilver)
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                    Gac2Gas.Request2023Double11ExteriorDiscountCoupons(1)
                end), nil)
            else
                --银票+银两都不够
                local message = g_MessageMgr:FormatMessage("Double11_2023Discount_YinPiao_YinLiang_NotEnough")
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                    CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
                end), nil)
            end
        end
        
    elseif self.oneDrawTimes == 2 then
        --银两
        local cost = self.drawCostData[3][self.oneDrawTimes]
        if CClientMainPlayer.Inst.Silver >= cost then
            Gac2Gas.Request2023Double11ExteriorDiscountCoupons(1)
        else
            local message = g_MessageMgr:FormatMessage("Double11_2023Discount_YinLiang_NotEnough")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
            end), nil)
        end
    elseif self.oneDrawTimes == 3 then
        --元宝
        local cost = self.drawCostData[4][self.oneDrawTimes]
        if CClientMainPlayer.Inst.YuanBao >= cost then
            Gac2Gas.Request2023Double11ExteriorDiscountCoupons(1)
        else
            local message = g_MessageMgr:FormatMessage("Double11_2023Discount_YuanBao_NotEnough")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                CChargeWnd.OpenWndGettingYuanbao()
            end), nil)
        end
        
    else
        --灵玉
        local cost = self.drawCostData[5][4]
        if CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade >= cost then
            Gac2Gas.Request2023Double11ExteriorDiscountCoupons(1)
        else
            local message = g_MessageMgr:FormatMessage("Double11_2023Discount_LingYu_NotEnough")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
                CShopMallMgr.ShowChargeWnd()
            end), nil)
        end
    end
end
    
