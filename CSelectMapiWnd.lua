-- Auto Generated!!
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CMapiFanyuWnd = import "L10.UI.CMapiFanyuWnd"
local CMapiView = import "L10.UI.CMapiView"
local CommonDefs = import "L10.Game.CommonDefs"
local CSelectMapiItem = import "L10.UI.CSelectMapiItem"
local CSelectMapiWnd = import "L10.UI.CSelectMapiWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local MapiSelectWndType = import "L10.UI.MapiSelectWndType"
local NGUITools = import "NGUITools"
local String = import "System.String"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"

CSelectMapiWnd.m_Init_CS2LuaHook = function (this) 

    this.itemTemplate:SetActive(false)
    this.lingshouItemTemplate:SetActive(false)
    this.expItemTemplate:SetActive(false)
    this.headerLabel.gameObject:SetActive(false)
    this.btn.gameObject:SetActive(false)
    this.shortBgGo:SetActive(false)
    this.longBgGo:SetActive(false)
    this.lingshouBgGo:SetActive(false)
    this.expItemBgGO:SetActive(false)
    this.btn:SetActive(false)
    this.centerLabel.gameObject:SetActive(false)
    CSelectMapiWnd.sCurSelectedLingshou[0] = ""
    CSelectMapiWnd.sCurSelectedLingshou[1] = ""

    this.fangmuItemTemplate:SetActive(false)
    this.topHeaderLabel.gameObject:SetActive(false)
    this.fangmuBgGo:SetActive(false)

    this.lifeItemTemplate:SetActive(false)

    if CSelectMapiWnd.sType == MapiSelectWndType.eFanyuSelectMapi then
        this:InitSelectMapi()
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eShouhuSelectLingshou then
        this:InitSelectLingshou()
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectExpItem then
        this:InitSelectExpItem()
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectFangmu then
        this:InitSelectFangmu()
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectMajiu then
        this:InitMajiu()
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectLifeItem then
        this:InitSelectLifeItem()
    end
end
CSelectMapiWnd.m_InitSelectFangmu_CS2LuaHook = function (this) 
    this.topHeaderLabel.gameObject:SetActive(true)
    this.fangmuBgGo:SetActive(true)
    this.btn:SetActive(true)
    this.headerLabel.text = LocalString.GetString("房客的马匹仅能放牧一段时间")
    CommonDefs.GetComponentInChildren_GameObject_Type(this.btn, typeof(UILabel)).text = LocalString.GetString("确定")

    CommonDefs.ListClear(CSelectMapiWnd.sCurFangmuedMapiList)
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            local zqdata = CZuoQiMgr.Inst.zuoqis[i]
            if CZuoQiMgr.IsMapiTemplateId(zqdata.templateId) and zqdata.mapiInfo ~= nil then
                local ItemUnit = NGUITools.AddChild(this.scrollView.gameObject, this.fangmuItemTemplate)
                ItemUnit:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)):InitFangmuMapi(zqdata)
            end
            i = i + 1
        end
    end
end
CSelectMapiWnd.m_InitMajiu_CS2LuaHook = function (this) 
    this.expItemBgGO:SetActive(true)
    this.headerLabel.gameObject:SetActive(true)
    this.headerLabel.text = LocalString.GetString("请选择要放入马厩的马匹")

    local action = DelegateFactory.Action_string(function (id) 
        this:SelectMajiuMapi(id)
    end)

    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            if CZuoQiMgr.IsMapiTemplateId(CZuoQiMgr.Inst.zuoqis[i].templateId) and not CZuoQiMgr.Inst.zuoqis[i].isMajiuMapi then
                local ItemUnit = NGUITools.AddChild(this.scrollView.gameObject, this.itemTemplate)
                ItemUnit:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)):InitMapi(CZuoQiMgr.Inst.zuoqis[i], action)
            end
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CSelectMapiWnd.m_InitSelectExpItem_CS2LuaHook = function (this) 
    this.expItemBgGO:SetActive(true)
    this.headerLabel.gameObject:SetActive(true)
    this.headerLabel.text = LocalString.GetString("点击可使用，长按可连续使用")

    CommonDefs.ListClear(CSelectMapiWnd.sExpItemDic)
    local expItemList = ZuoQi_Setting.GetData().MapiExpItemList

    Extensions.RemoveAllChildren(this.grid.transform)

    do
        local i = 0
        while i < expItemList.Length do
            local itemtid = expItemList[i]
            local expAmount = expItemList[i + 1]

            local ItemUnit = NGUITools.AddChild(this.scrollView.gameObject, this.expItemTemplate)
            ItemUnit:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)):InitExpItem(itemtid, expAmount)

            CommonDefs.ListAdd(CSelectMapiWnd.sExpItemDic, typeof(CSelectMapiItem), CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)))
            i = i + 2
        end
    end

    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CSelectMapiWnd.m_InitSelectLingshou_CS2LuaHook = function (this) 
    this.lingshouBgGo:SetActive(true)
    this.btn:SetActive(true)

    this.headerLabel.text = LocalString.GetString("每只马匹最多可守护两只灵兽")
    CommonDefs.GetComponentInChildren_GameObject_Type(this.btn, typeof(UILabel)).text = LocalString.GetString("确定")
    this.headerLabel.gameObject:SetActive(true)

    local curZuoQi = nil
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            if CZuoQiMgr.Inst.zuoqis[i].id == CMapiView.sCurZuoqiId then
                curZuoQi = CZuoQiMgr.Inst.zuoqis[i]
                break
            end
            i = i + 1
        end
    end
    if curZuoQi == nil or not CZuoQiMgr.IsMapiTemplateId(curZuoQi.templateId) or curZuoQi.mapiInfo == nil then
        this:Close()
        return
    end

    CLingShouMgr.Inst:ClearLingShouData()
    CLingShouMgr.Inst:RequestLingShouList()
end
CSelectMapiWnd.m_OnSelectLingshouBtnClicked_CS2LuaHook = function (this) 
    local curZuoQi = nil
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            if CZuoQiMgr.Inst.zuoqis[i].id == CMapiView.sCurZuoqiId then
                curZuoQi = CZuoQiMgr.Inst.zuoqis[i]
                break
            end
            i = i + 1
        end
    end
    if curZuoQi == nil or curZuoQi.mapiInfo == nil then
        return
    end

    local mapiInfo = curZuoQi.mapiInfo

    -- 先找出需要取消守护的灵兽
    do
        local i = 1
        while i < mapiInfo.ShouhuLingshou.Length do
            local lingshouId = mapiInfo.ShouhuLingshou[i]
            if not System.String.IsNullOrEmpty(lingshouId) and not (CSelectMapiWnd.sCurSelectedLingshou[0] == lingshouId) and not (CSelectMapiWnd.sCurSelectedLingshou[1] == lingshouId) then
                Gac2Gas.RequestStopShouhuLingshou(curZuoQi.id, lingshouId)
            end
            i = i + 1
        end
    end
    -- 再找出需要开始守护的灵兽
    do
        local i = 0
        while i < CSelectMapiWnd.sCurSelectedLingshou.Length do
            local lingshouId = CSelectMapiWnd.sCurSelectedLingshou[i]
            if not System.String.IsNullOrEmpty(lingshouId) and not (mapiInfo.ShouhuLingshou[1] == lingshouId) and not (mapiInfo.ShouhuLingshou[2] == lingshouId) then
                Gac2Gas.RequestShouhuLingshou(curZuoQi.id, lingshouId)
            end
            i = i + 1
        end
    end
end
CSelectMapiWnd.m_OnGetAllLingShouOtherOverview_CS2LuaHook = function (this) 
    if CSelectMapiWnd.sType ~= MapiSelectWndType.eShouhuSelectLingshou then
        return
    end

    Extensions.RemoveAllChildren(this.grid.transform)

    local idx = 0
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    do
        local i = 0
        while i < list.Count do
            local lingshou = list[i]

            local shouhuedByCurMapi = false
            local shouhuedByOtherMapi = false
            do
                local k = 0
                while k < CZuoQiMgr.Inst.zuoqis.Count do
                    local zqdata = CZuoQiMgr.Inst.zuoqis[k]
                    if CZuoQiMgr.IsMapiTemplateId(zqdata.templateId) and zqdata.mapiInfo ~= nil then
                        local mapiInfo = zqdata.mapiInfo
                        do
                            local j = 1
                            while j < mapiInfo.ShouhuLingshou.Length do
                                if mapiInfo.ShouhuLingshou[j] == lingshou.id then
                                    if zqdata.id == CMapiView.sCurZuoqiId then
                                        shouhuedByCurMapi = true
                                        if idx < 2 then
                                            CSelectMapiWnd.sCurSelectedLingshou[idx] = lingshou.id
                                            idx = idx + 1
                                        end
                                    else
                                        shouhuedByOtherMapi = true
                                    end
                                    break
                                end
                                j = j + 1
                            end
                        end
                    end
                    k = k + 1
                end
            end

            local ItemUnit = NGUITools.AddChild(this.scrollView.gameObject, this.lingshouItemTemplate)
            ItemUnit:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)):InitLingshou(lingshou, shouhuedByCurMapi, shouhuedByOtherMapi)
            i = i + 1
        end
    end

    this.grid:Reposition()
    this.scrollView:ResetPosition()

    if list.Count == 0 then
        this.centerLabel.gameObject:SetActive(true)
        this.centerLabel.text = LocalString.GetString("你当前没有灵兽哦")
    end
end
CSelectMapiWnd.m_InitSelectMapi_CS2LuaHook = function (this) 
    local action = DelegateFactory.Action_string(function (id) 
        this:SelectMapi(id)
    end)

    this.shortBgGo:SetActive(not CSelectMapiWnd.SBShowInviteFriend)
    this.longBgGo:SetActive(CSelectMapiWnd.SBShowInviteFriend)
    this.btn:SetActive(CSelectMapiWnd.SBShowInviteFriend)

    if CSelectMapiWnd.SBShowInviteFriend then
        CommonDefs.GetComponentInChildren_GameObject_Type(this.btn, typeof(UILabel)).text = LocalString.GetString("好友的马匹")
    end


    Extensions.RemoveAllChildren(this.grid.transform)
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            local zqdata = CZuoQiMgr.Inst.zuoqis[i]

            if not (CMapiFanyuWnd.sLeftZuoQi ~= nil and CMapiFanyuWnd.sLeftZuoQi.Id == zqdata.id) then
                if CZuoQiMgr.IsMapiTemplateId(zqdata.templateId) then
                    local ItemUnit = NGUITools.AddChild(this.scrollView.gameObject, this.itemTemplate)
                    ItemUnit:SetActive(true)
                    CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)):InitMapi(zqdata, action)
                end
            end
            i = i + 1
        end
    end

    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CSelectMapiWnd.m_SelectMajiuMapi_CS2LuaHook = function (this, zqid) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(zqid) then
        return
    end

    Gac2Gas.PutMapiToMajiu(zqid)

    CUIManager.CloseUI(CUIResources.SelectMapiWnd)
end
CSelectMapiWnd.m_SelectMapi_CS2LuaHook = function (this, zqid) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    if not CZuoQiMgr.Inst:CheckMapiNotInMajiu(zqid) then
        return
    end

    local bIsStarter = (CClientMainPlayer.Inst.Id == CMapiFanyuWnd.sStarterPlayerId)
    local bIsRightPart = (not bIsStarter) or CSelectMapiWnd.SBShowInviteFriend

    if bIsStarter and not bIsRightPart then
        Gac2Gas.PlayerStartYumaChooseMapi(zqid)
    else
        Gac2Gas.YumaPlayerChooseRightMapi(CMapiFanyuWnd.sStarterPlayerId, zqid)
    end

    CUIManager.CloseUI(CUIResources.SelectMapiWnd)
end
CSelectMapiWnd.m_OnBtnClick_CS2LuaHook = function (this, go) 
    if CSelectMapiWnd.sType == MapiSelectWndType.eFanyuSelectMapi then
        CUIManager.ShowUI(CUIResources.InviteFanyuWnd)
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eShouhuSelectLingshou then
        this:OnSelectLingshouBtnClicked()
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectExpItem then
    elseif CSelectMapiWnd.sType == MapiSelectWndType.eSelectFangmu then
        this:OnFangmuMapiBtnClicked()
    end
    CUIManager.CloseUI(CUIResources.SelectMapiWnd)
end
CSelectMapiWnd.m_OnFangmuMapiBtnClicked_CS2LuaHook = function (this) 
    local furniture = CClientHouseMgr.Inst.mFurnitureProp
    if furniture == nil or CClientMainPlayer.Inst == nil then
        return
    end

    -- 先找出需要取消放牧的
    local cancelMapiList = CreateFromClass(MakeGenericClass(Dictionary, UInt32, String))
    CommonDefs.DictIterate(furniture.MapiAppearanceInfo, DelegateFactory.Action_object_object(function (___key, ___value) 
        local v = {}
        v.Key = ___key
        v.Value = ___value
        if v.Value.OwnerId == CClientMainPlayer.Inst.Id and not CommonDefs.ListContains(CSelectMapiWnd.sCurFangmuedMapiList, typeof(String), v.Value.ZuoqiId) then
            CommonDefs.DictAdd(cancelMapiList, typeof(UInt32), v.Key, typeof(String), v.Value.ZuoqiId)
        end
    end))
    CommonDefs.DictIterate(cancelMapiList, DelegateFactory.Action_object_object(function (___key, ___value) 
        local v = {}
        v.Key = ___key
        v.Value = ___value
        Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eFurniture, v.Key, EnumFurniturePlace_lua.eRepository, 0, CClientMainPlayer.Inst.Id, "")
    end))

    -- 再找出需要开始放牧的马匹
    do
        local i = 0
        while i < CSelectMapiWnd.sCurFangmuedMapiList.Count do
            local zuoqiId = CSelectMapiWnd.sCurFangmuedMapiList[i]
            local bFound = false
            CommonDefs.DictIterate(furniture.MapiAppearanceInfo, DelegateFactory.Action_object_object(function (___key, ___value) 
                local v = {}
                v.Key = ___key
                v.Value = ___value
                if v.Value.ZuoqiId == zuoqiId then
                    bFound = true
                end
            end))
            if not bFound then
                if CZuoQiMgr.Inst:CheckMapiNotInMajiu(zuoqiId) then
                    Gac2Gas.RequestRandomAddMapiFurniture(zuoqiId)
                end
            end
            i = i + 1
        end
    end
end
CSelectMapiWnd.m_InitSelectLifeItem_CS2LuaHook = function (this) 
    this.expItemBgGO:SetActive(true)
    this.headerLabel.gameObject:SetActive(true)
    this.headerLabel.text = LocalString.GetString("点击可使用，长按可连续使用")

    CommonDefs.ListClear(CSelectMapiWnd.sLifeItemDic)
    Extensions.RemoveAllChildren(this.grid.transform)

    local curZuoQi = nil
    do
        local i = 0
        while i < CZuoQiMgr.Inst.zuoqis.Count do
            if CZuoQiMgr.Inst.zuoqis[i].id == CMapiView.sCurZuoqiId then
                curZuoQi = CZuoQiMgr.Inst.zuoqis[i]
                break
            end
            i = i + 1
        end
    end
    if curZuoQi == nil or curZuoQi.mapiInfo == nil then
        return
    end

    local mapiInfo = curZuoQi.mapiInfo

    local mapiLifeItemTbl = ZuoQi_Setting.GetData().MapiLifeItemId
    for i = 0, mapiLifeItemTbl.Length - 1, 3 do
    	local mapiQuality = mapiLifeItemTbl[i]
    	if mapiQuality == mapiInfo.Quality then
    		local lifeItemTid = mapiLifeItemTbl[i+1]
    		local lifeAmount = math.floor(mapiLifeItemTbl[i+2]/86400)
    		local ItemUnit = NGUITools.AddChild(this.scrollView.gameObject, this.lifeItemTemplate)
            ItemUnit:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)):InitLifeItem(lifeItemTid, lifeAmount)

            CommonDefs.ListAdd(CSelectMapiWnd.sLifeItemDic, typeof(CSelectMapiItem), CommonDefs.GetComponent_GameObject_Type(ItemUnit, typeof(CSelectMapiItem)))
    	end
    end

    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
