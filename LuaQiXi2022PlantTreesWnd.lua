local QnTableView = import "L10.UI.QnTableView"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CClientObject = import "L10.Game.CClientObject"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Quaternion = import "UnityEngine.Quaternion"

LuaQiXi2022PlantTreesWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RulesView", "RulesView", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "TaskView", "TaskView", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RankView", "RankView", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "ModelTexture", "ModelTexture", UITexture)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "Bubble", "Bubble", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "InviteButton", "InviteButton", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "BubbleLabel", "BubbleLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RulesViewButton", "RulesViewButton", CButton)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RulesViewScrollView", "RulesViewScrollView", UIScrollView)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RulesViewParagraphTemplate", "RulesViewParagraphTemplate", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RulesViewItemViewTemplate", "RulesViewItemViewTemplate", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RulesViewTable", "RulesViewTable", UITable)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "TaskViewNameLabel", "TaskViewNameLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "TaskViewValLabel", "TaskViewValLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "TaskViewTabs", "TaskViewTabs", UITabBar)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "TaskViewScrollView", "TaskViewScrollView", UIScrollView)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "TaskViewGrid", "TaskViewGrid", UIGrid)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "TaskViewTemplate", "TaskViewTemplate", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RankViewTabs", "RankViewTabs", UITabBar)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RankViewHeader1", "RankViewHeader1", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RankViewHeader2", "RankViewHeader2", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RankViewTableView", "RankViewTableView", QnTableView)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "RankViewNoneLabel", "RankViewNoneLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "Fx1", "Fx1", CUIFx)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "Fx2", "Fx2", CUIFx)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "Fx3", "Fx3", CUIFx)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "AfterBg", "AfterBg", GameObject)
RegistChildComponent(LuaQiXi2022PlantTreesWnd, "BeforeBg", "BeforeBg", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_PlayerId2PlayerNameDict")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_Identifier")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_MainRO")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_PlayerRO1")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_PlayerRO2")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_RankData")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_RankViewIndex")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_TaskViewIndex")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_TaskId2Obj")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_PartnerData")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_JieyuanData")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_DailyTaskData")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_Rank")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_Tick")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_TabIndex")
RegistClassMember(LuaQiXi2022PlantTreesWnd,"m_IsEndModelAni")

function LuaQiXi2022PlantTreesWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.InviteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInviteButtonClick()
	end)


	
	UIEventListener.Get(self.RulesViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRulesViewButtonClick()
	end)


    --@endregion EventBind end
end

function LuaQiXi2022PlantTreesWnd:Init()
    Gac2Gas.YYS_RequestOpenTab()
    
    self.AfterBg.gameObject:SetActive(false)
    self.BeforeBg.gameObject:SetActive(true)
    self:InitTabs()
    self:InitRulesView()
    self:InitRankView()
end

function LuaQiXi2022PlantTreesWnd:InitTabs()
    self:GetTempPlayData()
    self.Tabs:Init(true)
    self.TaskViewTabs:Init(true)
    self.TaskViewTabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTaskViewTabsTabChange(index)
	end)
    self.TaskViewTabs:ChangeTab(0, false) 
    local showRedDot = self:CheckTaskViewRedDot()
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)
    self.Tabs:ChangeTab((self.m_PartnerData and showRedDot) and 1 or 0, false)
    self.RankViewTabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnRankViewTabsTabChange(index)
	end)
    self.RankViewTabs:ChangeTab(0, false) 
end

function LuaQiXi2022PlantTreesWnd:CheckTaskViewRedDot()
    local showRedDot1,showRedDot2 = false,false
    local havePartner = false

    if LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData then
        local tempPlayData = LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData
        havePartner = (tempPlayData[1] ~= nil)
    end

    if self.m_DailyTaskData then
        QiXi2022_DailyTask.ForeachKey(function (k)
            local arr = self.m_DailyTaskData[k]
            if arr then
                local progress, status = arr[1], arr[2]
                showRedDot1 = showRedDot1 or (status == 1)
            end
        end)
    end

    if self.m_JieyuanData then
        QiXi2022_DisposableTask.ForeachKey(function (k)
            local status = self.m_JieyuanData[k]
            if status then
                showRedDot2 = showRedDot2 or (status == 1)
            end
        end)
    end
    if self.TaskViewTabs.tabButtons then
        for i = 0, self.TaskViewTabs.tabButtons.Length - 1 do
            local btn = self.TaskViewTabs.tabButtons[i]
            btn.transform:Find("AlertSprite").gameObject:SetActive(havePartner and ((showRedDot1 and i == 1) or (showRedDot2 and i == 0)))
        end
        for i = 0, self.Tabs.tabButtons.Length - 1 do
            self.Tabs.tabButtons[i].transform:Find("Alert").gameObject:SetActive(false)
        end
    end
    local showRedDot = (showRedDot1 or showRedDot2) and havePartner
    self.Tabs.tabButtons[1].transform:Find("Alert").gameObject:SetActive(showRedDot)
    return showRedDot
end

function LuaQiXi2022PlantTreesWnd:GetTempPlayData()
    if LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData then
        local tempPlayData = LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData
        
        local jieyuanData = tempPlayData[4]
        -- if jieyuanData then
        --     for taskId, status in pairs(jieyuanData) do --0未完成/1已完成未领取/2已领取
        --         print(taskId, status)
        --     end 
        -- end
        self.m_JieyuanData = jieyuanData
    end

    if LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndDailyTaskData then
        local tempPlayData = LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndDailyTaskData
        -- for taskId, arr in pairs(tempPlayData) do
        --     local progress, status = arr[1], arr[2]
        --     --print(taskId, progress, status)
        -- end
        self.m_DailyTaskData = tempPlayData
    end
end

function LuaQiXi2022PlantTreesWnd:InitModelView()
    self.BubbleLabel.text = g_MessageMgr:FormatMessage("QiXi2022PlantTreesWnd_Bubble")
    self.m_Identifier = "_QiXi2022PlantTrees_Preview_"
    if self.ModelTexture.mainTexture and self.m_MainRO then
        self:LoadModels(self.m_MainRO)
        return
    end
    local modelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_MainRO = ro
        self:LoadModels(self.m_MainRO)
    end)
    CUIManager.DestroyModelTexture(self.m_Identifier)
    self.ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_Identifier, modelTextureLoader,180,0.1,-1,8.58,false,true,1,true,false)
end

function LuaQiXi2022PlantTreesWnd:LoadModels(ro)
    if self.m_PlayerRO1 then
        self.m_PlayerRO1:Destroy()
        self.m_PlayerRO1 = nil
    end
    if self.m_PlayerRO2 then
        self.m_PlayerRO2:Destroy()
        self.m_PlayerRO2 = nil
    end
    self.m_PlayerRO1 = CRenderObject.CreateRenderObject(ro.gameObject,"RenderObject",false)
    self.m_PlayerRO1.NeedUpdateAABB = true
    self.m_PlayerRO1.Layer = LayerDefine.ModelForNGUI_3D
    self.m_PlayerRO2 = CRenderObject.CreateRenderObject(ro.gameObject,"RenderObject",false)
    self.m_PlayerRO2.NeedUpdateAABB = true
    self.m_PlayerRO2.Layer = LayerDefine.ModelForNGUI_3D
    if not CClientMainPlayer.Inst then
        return  
    end
    self:CancelTick()
    if self.m_PartnerData then
        if not self.m_IsEndModelAni then
            self:ShowTwoPlayerAni()
        else
            RegisterTickOnce(function ()
                if self.Fx3 then
                    self.Fx3:LoadFx("fx/ui/prefab/UI_QX2022_YinYuanShu_GuoChang.prefab")
                    self.RulesView.gameObject:SetActive(false)
                    self.TaskView.gameObject:SetActive(false)
                    self.RankView.gameObject:SetActive(false)
                    self.ModelTexture.gameObject:SetActive(false)
                end
            end,2500)
            self.m_Tick = RegisterTickOnce(function ()
                self:ShowTwoPlayerAni()
            end,3000)
        end  
    else
        self:ShowOnePlayerAni() 
    end
    self.m_IsEndModelAni = true 
end

function LuaQiXi2022PlantTreesWnd:ShowOnePlayerAni()
    if self.m_PlayerRO1 == nil then
        return
    end
    local fashionHeadId = QiXi2022_Setting.GetData().HeadID
    local fashionBodyId = QiXi2022_Setting.GetData().BodyID

    local fakeAppearance = CreateFromClass(CPropertyAppearance)
    fakeAppearance.FeiShengAppearanceXianFanStatus = 0   
    fakeAppearance.HeadFashionId = fashionHeadId
    fakeAppearance.BodyFashionId = fashionBodyId
    fakeAppearance.m_Gender = CClientMainPlayer.Inst.Gender
    fakeAppearance.m_Class = CClientMainPlayer.Inst.Class
    self.m_PlayerRO1.transform.localPosition = Vector3(-1.63, -1.05, -1.66)  
    self.m_PlayerRO1.transform.localRotation = Quaternion.Euler(0, 153.58, 0)  
    CClientMainPlayer.LoadResource(self.m_PlayerRO1, fakeAppearance, true, 1, 0, true, 0, false, 0, false, nil, false, false)
    CClientObject.ShowExpressionAction(self.m_PlayerRO1, 47000077, EnumToInt(fakeAppearance.Gender))

    self.AfterBg.gameObject:SetActive(false)
    self.BeforeBg.gameObject:SetActive(true)
end

function LuaQiXi2022PlantTreesWnd:ShowTwoPlayerAni()
    if self.m_PlayerRO1 == nil or self.m_PlayerRO2 == nil then
        return
    end
    self.m_PlayerRO1.transform.localPosition = Vector3(-1.51, -1.05, -1.66)  
    self.m_PlayerRO1.transform.localRotation = Quaternion.Euler(0, 30.3, 0)  
    self.m_PlayerRO2.transform.localPosition = Vector3(-0.49, -0.99, -1.66)  
    self.m_PlayerRO2.transform.localRotation = Quaternion.Euler(0, -18, 0)  
    self.RulesView.gameObject:SetActive(self.m_TabIndex == 0)
    self.TaskView.gameObject:SetActive(self.m_TabIndex == 1)
    self.RankView.gameObject:SetActive(self.m_TabIndex == 2)
    self.AfterBg.gameObject:SetActive(true)
    self.BeforeBg.gameObject:SetActive(false)
    self.ModelTexture.gameObject:SetActive(true)
    self.Fx1:LoadFx("fx/ui/prefab/UI_QX2022_YinYuanShu.prefab")
    self.Fx2:LoadFx("fx/ui/prefab/UI_QX2022_YinYuanShu_ShuYe.prefab")

    local fashionHeadId = QiXi2022_Setting.GetData().HeadID
    local fashionBodyId = QiXi2022_Setting.GetData().BodyID
    local fakeAppearance = CreateFromClass(CPropertyAppearance)
    fakeAppearance.FeiShengAppearanceXianFanStatus = 0   
    fakeAppearance.HeadFashionId = fashionHeadId
    fakeAppearance.BodyFashionId = fashionBodyId
    fakeAppearance.m_Gender = CClientMainPlayer.Inst.Gender
    fakeAppearance.m_Class = CClientMainPlayer.Inst.Class
    local gender1 = EnumToInt(fakeAppearance.Gender)
    CClientMainPlayer.LoadResource(self.m_PlayerRO1, fakeAppearance, true, 1, 0, true, 0, false, 0, false, nil, false, false)
    
    if self.m_PartnerData and self.m_PartnerData.gender and self.m_PartnerData.class then
        fakeAppearance.m_Gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), self.m_PartnerData.gender)
        fakeAppearance.m_Class =  CommonDefs.ConvertIntToEnum(typeof(EnumClass), self.m_PartnerData.class) 
    end
    CClientMainPlayer.LoadResource(self.m_PlayerRO2, fakeAppearance, true, 1, 0, true, 0, false, 0, false, nil, false, false)
   
    local expressionDict = {}
    expressionDict[EnumToInt(EnumGender.Male)] = 47000151
    expressionDict[EnumToInt(EnumGender.Female)] = 47000152
    local gender2 = EnumToInt(fakeAppearance.Gender)
    CClientObject.ShowExpressionAction(self.m_PlayerRO1, expressionDict[gender1], gender1)
    if gender1 == EnumToInt(EnumGender.Female) then
        local fx = CEffectMgr.Inst:AddObjectFX(88800303,self.m_PlayerRO1,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        self.m_PlayerRO1:AddFX("-", fx)
    end
    CClientObject.ShowExpressionAction(self.m_PlayerRO2, expressionDict[gender2], gender2)
    if gender2 == EnumToInt(EnumGender.Female) then
        local fx = CEffectMgr.Inst:AddObjectFX(88800303,self.m_PlayerRO2,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        self.m_PlayerRO2:AddFX("-", fx)
    end
end

function LuaQiXi2022PlantTreesWnd:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaQiXi2022PlantTreesWnd:InitRulesView()
    self.RulesViewButton.Text = self.m_PartnerData and LocalString.GetString("前往商城买花") or LocalString.GetString("邀请好友种树")
    self.RulesViewParagraphTemplate.gameObject:SetActive(false)
    self.RulesViewItemViewTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.RulesViewTable.transform)
    local msg = g_MessageMgr:FormatMessage("QiXi2022PlantTreesWn_Rule1")
	self:InitParagraph(msg)
    
    local itemsObj = CUICommonDef.AddChild(self.RulesViewTable.gameObject, self.RulesViewItemViewTemplate.gameObject)
    itemsObj:SetActive(true)
    local itemTemplate = itemsObj.transform:Find("ItemTemplate")
    local itemsGrid = itemsObj.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    Extensions.RemoveAllChildren(itemsGrid.transform)
    itemTemplate.gameObject:SetActive(false)
    
    for i = 0, QiXi2022_Setting.GetData().Flowers.Length - 1 do
        local arr = QiXi2022_Setting.GetData().Flowers[i]
        local itemId, val = arr[0], arr[1]
        local itemData = Item_Item.GetData(itemId)

        local go = CUICommonDef.AddChild(itemsGrid.gameObject, itemTemplate.gameObject)
        go.gameObject:SetActive(true)
        local itemCell = go.transform:Find("ItemCell")
        UIEventListener.Get(itemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
        end)
        local iconTexture = itemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        iconTexture:LoadMaterial(itemData.Icon)
        local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local numLabel = go.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = itemData.Name
        numLabel.text = SafeStringFormat3(" +%d",val) 
    end

    itemsGrid:Reposition()
    msg = g_MessageMgr:FormatMessage("QiXi2022PlantTreesWn_Rule2")
	self:InitParagraph(msg)
    self.RulesViewTable:Reposition()
    self.RulesViewScrollView:ResetPosition()
end

function LuaQiXi2022PlantTreesWnd:InitParagraph(msg)
    if System.String.IsNullOrEmpty(msg) then
        return
    end
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = CUICommonDef.AddChild(self.RulesViewTable.gameObject, self.RulesViewParagraphTemplate.gameObject)
        paragraphGo:SetActive(true)
        local label = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)).textLabel
        label.text = "[0C0C0C]" ..info.paragraphs[i]
        label.color = Color.white
        --Init(info.paragraphs[i], 202116351)
    end
end

function LuaQiXi2022PlantTreesWnd:InitTaskView()
    self.TaskViewTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.TaskViewGrid.transform)
    self.m_TaskId2Obj = {}
    if self.m_TaskViewIndex == 0 then
        QiXi2022_DailyTask.ForeachKey(function (k)
            local obj = NGUITools.AddChild(self.TaskViewGrid.gameObject, self.TaskViewTemplate.gameObject)
            self:InitTaskViewItem(obj,QiXi2022_DailyTask.GetData(k))
            self.m_TaskId2Obj[k] = obj
        end)
    else
        QiXi2022_DisposableTask.ForeachKey(function (k)
            local obj = NGUITools.AddChild(self.TaskViewGrid.gameObject, self.TaskViewTemplate.gameObject)
            self:InitTaskViewItem(obj,QiXi2022_DisposableTask.GetData(k))
            self.m_TaskId2Obj[k] = obj
        end)
    end
    self.TaskViewGrid:Reposition()
    self.TaskViewScrollView:ResetPosition()
end

function LuaQiXi2022PlantTreesWnd:InitTaskViewItem(go, designData)

    local finishButton = go.transform:Find("FinishButton").gameObject
    local rewardButton = go.transform:Find("RewardButton").gameObject
    local itemCell = go.transform:Find("ItemCell").gameObject
    local iconTexture = go.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
    local scoreNumLabel = go.transform:Find("Score/ScoreNumLabel"):GetComponent(typeof(UILabel))

    UIEventListener.Get(finishButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_TaskViewIndex == 0 then
            CUIManager.ShowUI("ScheduleWnd")
        else
            g_MessageMgr:ShowMessage(designData.MessageKey)
        end
	end)
    UIEventListener.Get(rewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    Gac2Gas.YYS_ClaimReward(self.m_TaskViewIndex + 1, designData.TaskId)
	end)
    UIEventListener.Get(itemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(designData.Award)
	end)
    local itemData = Item_Item.GetData(designData.Award)
    iconTexture:LoadMaterial(itemData.Icon)
    scoreNumLabel.text = designData.Yinyuanzhi
    go.gameObject:SetActive(true)
    local progress, status = 0,0

    if self.m_TaskViewIndex == 0 and self.m_DailyTaskData then
        local arr = self.m_DailyTaskData[designData.TaskId]
        if arr then
            progress, status = arr[1], arr[2]
        end
    elseif self.m_TaskViewIndex == 1 and self.m_JieyuanData and self.m_JieyuanData[designData.TaskId] then
        status = self.m_JieyuanData[designData.TaskId]
    end

    self:UpdateTaskViewItem(go, designData,status,progress)
end

function LuaQiXi2022PlantTreesWnd:UpdateTaskViewItem(go, designData,status,progress)
    local desLabel = go.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
    local finishButton = go.transform:Find("FinishButton").gameObject
    local rewardButton = go.transform:Find("RewardButton").gameObject
    local acceptedSprite = go.transform:Find("AcceptedSprite").gameObject
    local scoreAcceptedSprite = go.transform:Find("ScoreAccepted").gameObject
    local mark = go.transform:Find("Mark").gameObject
    local rewardInfoLabel = go.transform:Find("RewardInfoLabel"):GetComponent(typeof(UILabel))

    finishButton.gameObject:SetActive(status == 0) 
    rewardButton.gameObject:SetActive(status == 1)
    local havePartner = false
    if LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData then
        local tempPlayData = LuaQiXi2022Mgr.m_QiXi2022PlantTreesWndPlayData
        havePartner = (tempPlayData[1] ~= nil)
    end
    rewardButton.transform:Find("Alert").gameObject:SetActive(havePartner)
    acceptedSprite.gameObject:SetActive(status == 2)
    scoreAcceptedSprite.gameObject:SetActive(status == 2)
    mark.gameObject:SetActive(status == 2)
    rewardInfoLabel.gameObject:SetActive(status == 2)
    local msg = SafeStringFormat3("[2D2D2D]"..designData.TaskDescription, progress)
    desLabel.text = CUICommonDef.TranslateToNGUIText(msg)
    desLabel.color = Color.white
end

function LuaQiXi2022PlantTreesWnd:OnSyncPlayData()
    --数据有更新 刷新一下页面
    self:GetTempPlayData()
    if self.m_TaskViewIndex == 0 then
        QiXi2022_DailyTask.ForeachKey(function (k)
            local obj = nil
            if self.m_TaskId2Obj[k] then
                obj = self.m_TaskId2Obj[k]
            else
                obj = NGUITools.AddChild(self.TaskViewGrid.gameObject, self.TaskViewTemplate.gameObject)
            end
            if obj and self.m_DailyTaskData then
                local designData = QiXi2022_DailyTask.GetData(k)
                local progress, status = 0,0
                local arr = self.m_DailyTaskData[designData.TaskId]
                if arr then
                    progress, status = arr[1], arr[2]
                end
                self:UpdateTaskViewItem(obj, designData, status, progress)
            end
        end)
    else
        QiXi2022_DisposableTask.ForeachKey(function (k)
            local obj = nil
            if self.m_TaskId2Obj[k] then
                obj = self.m_TaskId2Obj[k]
            else
                obj = NGUITools.AddChild(self.TaskViewGrid.gameObject, self.TaskViewTemplate.gameObject)
            end
            if obj and self.m_JieyuanData and self.m_JieyuanData[k]  then
                local progress, status = 0, 0
                status = self.m_JieyuanData[k]
                local designData = QiXi2022_DisposableTask.GetData(k)
                self:UpdateTaskViewItem(obj, designData,status, progress)
            end
        end)
    end
    self:CheckTaskViewRedDot()
end

function LuaQiXi2022PlantTreesWnd:OnUpdateMissionProgress(kind, id, value, bFinish, bClaimReward)
    if kind == (self.m_TaskViewIndex + 1) then
        local go = self.m_TaskId2Obj[id]
        if not go then return end
        local designData = kind == 1 and QiXi2022_DailyTask.GetData(id) or QiXi2022_DisposableTask.GetData(id)
        local status = 0
        if bFinish then
            status = 1
        end
        if bClaimReward then
            status = 2
        end
        if self.m_TaskViewIndex == 0 and self.m_DailyTaskData then
            if self.m_DailyTaskData[designData.TaskId] then
                self.m_DailyTaskData[designData.TaskId][1] = value
                self.m_DailyTaskData[designData.TaskId][2] = status
            end
        elseif self.m_TaskViewIndex == 1 and self.m_JieyuanData and self.m_JieyuanData[designData.TaskId] then
            self.m_JieyuanData[designData.TaskId] = status
        end
        self:UpdateTaskViewItem(go, designData,status,value)
        self:CheckTaskViewRedDot()
    end
end

function LuaQiXi2022PlantTreesWnd:InitRankView()
    self.TaskViewNameLabel.text = LocalString.GetString("暂无")
    self.TaskViewValLabel.text = 0
    self.RankViewTableView.m_DataSource = DefaultTableViewDataSource.Create2(function() 
        return math.min(51,self.m_RankData and #self.m_RankData or 0)
    end, function(view,row)
        local item = view:GetFromPool(self.m_RankViewIndex)
        if self.m_RankViewIndex == 0 then
            self:InitRankViewItem1(item, row)
        else
            self:InitRankViewItem2(item, row + 1)
        end
        return item
    end)
    self.RankViewTableView:ReloadData(true, true)
end

function LuaQiXi2022PlantTreesWnd:InitRankViewItem1(item, index)
    local mySprite = item.transform:Find("MySprite")
    local highlight = item.transform:Find("Highlight")
    local noneLabel = item.transform:Find("NoneLabel")
    local mainRoot = item.transform:Find("Main")
    local rankLabel = mainRoot.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local valLabel = mainRoot.transform:Find("ValLabel"):GetComponent(typeof(UILabel))
    local item1 = mainRoot.transform:Find("Item1")
    local item1IconTexture = mainRoot.transform:Find("Item1/IconTexture"):GetComponent(typeof(CUITexture))
    local item2 = mainRoot.transform:Find("Item2")
    
    local item2IconTexture = mainRoot.transform:Find("Item2/IconTexture"):GetComponent(typeof(CUITexture))
    local nameLabel1 = mainRoot.transform:Find("NameLabel1"):GetComponent(typeof(UILabel))
    local nameLabel2 = mainRoot.transform:Find("NameLabel2"):GetComponent(typeof(UILabel))
    local rankSprite = mainRoot.transform:Find("RankSprite"):GetComponent(typeof(UISprite))

    local data = self.m_RankData[index + 1]
    highlight.gameObject:SetActive(index == 0)
    noneLabel.gameObject:SetActive(data.isNone)
    mainRoot.gameObject:SetActive(not data.isNone)
    rankLabel.text = LocalString.GetString("未上榜")
    mySprite.gameObject:SetActive(false)
    if not data.isNone then
        mySprite.gameObject:SetActive(index == 0)
        rankLabel.text = ""
        if index == 1 then
            rankSprite.spriteName="Rank_No.1"
        elseif index == 2 then
            rankSprite.spriteName="Rank_No.2png"
        elseif index == 3 then
            rankSprite.spriteName="Rank_No.3png"
        elseif index == 0 then
            if self.m_Rank then
                if self.m_Rank == 1 then
                    rankSprite.spriteName="Rank_No.1"
                elseif self.m_Rank == 2 then
                    rankSprite.spriteName="Rank_No.2png"
                elseif self.m_Rank == 3 then
                    rankSprite.spriteName="Rank_No.3png"
                elseif self.m_Rank > 0 and self.m_Rank < 101 then
                    rankLabel.text = self.m_Rank
                    rankSprite.gameObject:SetActive(false)
				else
					rankLabel.text = LocalString.GetString("未上榜")
					rankSprite.gameObject:SetActive(false)
                end
            end
        else
            rankLabel.text = (index > 0 and index < 51) and index or LocalString.GetString("未上榜")
            rankSprite.gameObject:SetActive(false)
        end
        local val1, mainPlayerId1, class1, gender1 = data.playerData1.loveValue, data.playerData1.playerId, data.playerData1.class, data.playerData1.gender
        valLabel.text = val1
        if val1 == 0 then
            rankLabel.text = LocalString.GetString("未上榜")
            rankSprite.gameObject:SetActive(false)
            item.gameObject:SetActive(index == 0)
        end
        item1IconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(class1, gender1, -1), false)
        nameLabel1.text = data.playerData1.name
        nameLabel1.color = NGUIText.ParseColor24((CClientMainPlayer.Inst and data.playerData1.playerId == CClientMainPlayer.Inst.Id) and "05B005" or "2D2D2D",0)
        local val2, mainPlayerId2, class2, gender2 = data.playerData2.loveValue, data.playerData2.playerId, data.playerData2.class, data.playerData2.gender
        item2IconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(class2, gender2, -1), false)
        nameLabel2.text = data.playerData2.name
        nameLabel2.color = NGUIText.ParseColor24((CClientMainPlayer.Inst and data.playerData2.playerId == CClientMainPlayer.Inst.Id) and "05B005" or "2D2D2D",0)
        UIEventListener.Get(item1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerData1.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end)
        UIEventListener.Get(item2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerData2.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end)
    end
end

function LuaQiXi2022PlantTreesWnd:OnSeasonRank_QueryRankResult(rankType, seasonId, selfData, rankData)
    self.m_Rank = 0
    self.m_RankReward = false
    self.m_RankData = {}
    if rankType == EnumSeasonRankType.eQiXi2022YYS then
        local rawData = g_MessagePack.unpack(selfData)
        if rawData.playerId == nil then
            --空数据
            table.insert(self.m_RankData,{isNone = true})
        else
            --rawData3是主玩家, rawData4是对象玩家
            local rawData3 = {}
            rawData3["playerId"] = rawData.playerId
            rawData3["name"] = rawData.name
            rawData3["updateTime"] = rawData.updateTime
            rawData3["overdueTime"] = rawData.overdueTime
            rawData3["class"] = rawData.data[2]
            rawData3["gender"] = rawData.data[3]
            rawData3["loveValue"] = rawData.data[1]
            
            local rawData4 = {}
            rawData4["playerId"] = rawData.data[4]
            rawData4["name"] = rawData.data[5]
            rawData4["updateTime"] = rawData.updateTime
            rawData4["overdueTime"] = rawData.overdueTime
            rawData4["class"] = rawData.data[6]
            rawData4["gender"] = rawData.data[7]
            rawData4["loveValue"] = rawData.data[1]
            if CClientMainPlayer.Inst then
                if rawData3.playerId == CClientMainPlayer.Inst.Id then
                    self.m_PartnerData = {class = rawData4["class"], gender = rawData4["gender"]}
                    self:OnGetPartnerData()
                    self.TaskViewNameLabel.text = rawData4.name
                    self.TaskViewValLabel.text = rawData4["loveValue"]
                elseif rawData4.playerId == CClientMainPlayer.Inst.Id then
                    self.m_PartnerData = {class = rawData3["class"], gender = rawData3["gender"]}
                    self:OnGetPartnerData()
                    self.TaskViewNameLabel.text = rawData3.name
                    self.TaskViewValLabel.text = rawData3["loveValue"]
                end
            end
            table.insert(self.m_RankData,{playerData1 = rawData3, playerData2 = rawData4})
        end

        local rawData2 = g_MessagePack.unpack(rankData)

        for i = 1, #rawData2 do
            --local rawData3, rawData4 = rawData2[i], rawData2[i].data -- playerId,name,updateTime,overdueTime,data
            local rawData3 = {}
            rawData3["playerId"] = rawData2[i].playerId
            rawData3["name"] = rawData2[i].name
            rawData3["updateTime"] = rawData2[i].updateTime
            rawData3["overdueTime"] = rawData2[i].overdueTime
            rawData3["class"] = rawData2[i].data[2]
            rawData3["gender"] = rawData2[i].data[3]
            rawData3["loveValue"] = rawData2[i].data[1]

            local rawData4 = {}
            rawData4["playerId"] = rawData2[i].data[4]
            rawData4["name"] = rawData2[i].data[5]
            rawData4["updateTime"] = rawData2[i].updateTime
            rawData4["overdueTime"] = rawData2[i].overdueTime
            rawData4["class"] = rawData2[i].data[6]
            rawData4["gender"] = rawData2[i].data[7]
            rawData4["loveValue"] = rawData2[i].data[1]
            
            if CClientMainPlayer.Inst and (rawData3.playerId == CClientMainPlayer.Inst.Id or rawData4.playerId == CClientMainPlayer.Inst.Id) then
                self.m_Rank = #self.m_RankData

                if rawData4.loveValue > 0 then
                    self.m_RankReward = true
                end
            end
            table.insert(self.m_RankData,{playerData1 = rawData3, playerData2 = rawData4})
        end

    end
    self.RankViewNoneLabel.gameObject:SetActive(#self.m_RankData == 0)
    self.RankViewTableView:ReloadData(true, true)
    self.RankViewTableView.m_ScrollView:ResetPosition()
    if not self.m_IsEndModelAni then
        local showRedDot = self:CheckTaskViewRedDot()
        self.Tabs:ChangeTab((self.m_PartnerData and showRedDot) and 1 or 0, false)
        self:InitModelView()
    end
end

function LuaQiXi2022PlantTreesWnd:OnReportRankDataSuccess(rankType, seasonId, data_U)
    if rankType == EnumSeasonRankType.eQiXi2022YYS then
        local t = g_MessagePack.unpack(data_U)
        self.TaskViewValLabel.text = t.totalYYValue
    end
end

function LuaQiXi2022PlantTreesWnd:OnGetPartnerData()
    self.Bubble.gameObject:SetActive(self.m_TabIndex ~= 0 and not self.m_PartnerData)
    self.RulesViewButton.Text = self.m_PartnerData and LocalString.GetString("前往商城买花") or LocalString.GetString("邀请好友种树")
end

function LuaQiXi2022PlantTreesWnd:OnPlantTreeSuccess()
    self.m_IsEndModelAni = false
    Gac2Gas.YYS_QuerySeasonRank()
end

function LuaQiXi2022PlantTreesWnd:InitRankViewItem2(item, index)
    local titleLabel = item.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local mtable = item.transform:Find("Table"):GetComponent(typeof(UITable))
    local itemTemplate = item.transform:Find("ItemTemplate").gameObject
    local mySprite = item.transform:Find("MySprite")
    itemTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(mtable.transform)

    local data = self.m_RankData[index]
    titleLabel.text = data.AwardDescription
    rankLabel.text = data.Paiming
    local nextData = self.m_RankData[index + 1]
    if nextData and self.m_Rank then
        mySprite.gameObject:SetActive(data.Top <= self.m_Rank and self.m_Rank < nextData.Top and self.m_RankReward) 
    elseif self.m_Rank then
        mySprite.gameObject:SetActive(self.m_Rank >= data.Top and self.m_RankReward) 
    end

    for i = 0, data.Award.Length - 1 do
        local itemId = data.Award[i]
        local obj = NGUITools.AddChild(mtable.gameObject, itemTemplate.gameObject)
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
        end)
        local itemData = Item_Item.GetData(itemId)
        local icon = obj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        icon:LoadMaterial(itemData.Icon)
        obj.gameObject:SetActive(true)
    end
    
    mtable:Reposition()
end

function LuaQiXi2022PlantTreesWnd:OnEnable()
    g_ScriptEvent:AddListener("YYS_SyncPlayData", self, "OnSyncPlayData")
    g_ScriptEvent:AddListener("YYS_UpdateMissionProgress", self, "OnUpdateMissionProgress")
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
    g_ScriptEvent:AddListener("YYS_PlantTreeSuccess", self, "OnPlantTreeSuccess")
    g_ScriptEvent:AddListener("SeasonRank_ReportRankDataSuccess", self, "OnReportRankDataSuccess")
end

function LuaQiXi2022PlantTreesWnd:OnDisable()
    self:CancelTick()
    g_ScriptEvent:RemoveListener("YYS_SyncPlayData", self, "OnSyncPlayData")
    g_ScriptEvent:RemoveListener("YYS_UpdateMissionProgress", self, "OnUpdateMissionProgress")
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
    g_ScriptEvent:RemoveListener("YYS_PlantTreeSuccess", self, "OnPlantTreeSuccess")
    g_ScriptEvent:RemoveListener("SeasonRank_ReportRankDataSuccess", self, "OnReportRankDataSuccess")
end

function LuaQiXi2022PlantTreesWnd:OnDestroy()
    if self.m_PlayerRO1 then
        self.m_PlayerRO1:Destroy()
        self.m_PlayerRO1 = nil
    end
    if self.m_PlayerRO2 then
        self.m_PlayerRO2:Destroy()
        self.m_PlayerRO2 = nil
    end
    CUIManager.DestroyModelTexture(self.m_Identifier)
end

--@region UIEvent
function LuaQiXi2022PlantTreesWnd:OnTabsTabChange(index)
    self.m_TabIndex = index
    self.RulesView.gameObject:SetActive(index == 0)
    self.TaskView.gameObject:SetActive(index == 1)
    self.RankView.gameObject:SetActive(index == 2)
    if index == 2 then
        self.RankViewTabs:ChangeTab(0, false)
    end

    self.Bubble.gameObject:SetActive(not self.m_PartnerData)
    for i = 0, self.Tabs.tabButtons.Length - 1 do
        local btn = self.Tabs.tabButtons[i]
        local tex = btn.transform:GetComponent(typeof(CUITexture))
        tex:LoadMaterial(index == i and "UI/Texture/Transparent/Material/qixi2022planttreewnd_xuanzhong.mat" or "UI/Texture/Transparent/Material/qixi2022planttreewnd_weixuan.mat")
        tex.uiTexture.width = index == i and 196 or 152
        tex.uiTexture.height = index == i and 196 or 146
        btn.label.color = NGUIText.ParseColor24(index == i and "3B2C22" or "F9C9C8",0)
    end
    self.Tabs.transform:GetComponent(typeof(UITable)):Reposition()
end

function LuaQiXi2022PlantTreesWnd:OnInviteButtonClick()
    Gac2Gas.YYS_InvitePlayer()
end

function LuaQiXi2022PlantTreesWnd:OnRulesViewButtonClick()
    if not self.m_PartnerData then
        self:OnInviteButtonClick()
    else
        CShopMallMgr.ShowLinyuShoppingMall(QiXi2022_Setting.GetData().BuyYiai)
    end
end

function LuaQiXi2022PlantTreesWnd:OnTaskViewTabsTabChange(index)
    self.m_TaskViewIndex = index
    self:InitTaskView()
    for i = 0, self.TaskViewTabs.tabButtons.Length - 1 do
        local btn = self.TaskViewTabs.tabButtons[i]
        btn.transform:Find("Selected").gameObject:SetActive(index == i)
        btn.transform:Find("Unselected").gameObject:SetActive(index ~= i)
    end
end

function LuaQiXi2022PlantTreesWnd:OnRankViewTabsTabChange(index)
    self.m_RankViewIndex = index
    self.RankViewHeader1.gameObject:SetActive(index == 0)
    self.RankViewHeader2.gameObject:SetActive(index == 1)
    self.m_RankData = {}
    if index == 1 then
        QiXi2022_ListAward.Foreach(function (k,v)
            table.insert(self.m_RankData,QiXi2022_ListAward.GetData(k))
        end)
        self.RankViewNoneLabel.gameObject:SetActive(false)
    else
        self.RankViewNoneLabel.gameObject:SetActive(true)
        self.RankViewTableView.m_ScrollView:ResetPosition()
        Gac2Gas.YYS_QuerySeasonRank()
    end
    self.RankViewTableView:ReloadData(true, true)
    for i = 0, self.RankViewTabs.tabButtons.Length - 1 do
        local btn = self.RankViewTabs.tabButtons[i]
        btn.transform:Find("Selected").gameObject:SetActive(index == i)
        btn.transform:Find("Unselected").gameObject:SetActive(index ~= i)
    end
end
--@endregion UIEvent

