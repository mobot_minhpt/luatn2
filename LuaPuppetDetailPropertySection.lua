-- local UILabel = import "UILabel"
-- local UIEventListener = import "UIEventListener"
-- local Object = import "System.Object"
-- local NGUITools = import "NGUITools"
-- local NGUIMath = import "NGUIMath"
-- local MessageMgr = import "L10.Game.MessageMgr"
-- local Mathf = import "UnityEngine.Mathf"
-- local LocalString = import "LocalString"
-- local Extensions = import "Extensions"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
-- local DelegateFactory = import "DelegateFactory"
local CTooltip = import "L10.UI.CTooltip"
-- local CPuppetDetailPropertySection = import "L10.UI.CPuppetDetailPropertySection"
-- local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientFormula = import "L10.Game.CClientFormula"


CLuaPuppetDetailPropertySection = class()
RegistClassMember(CLuaPuppetDetailPropertySection,"sectionNameLabel")
RegistClassMember(CLuaPuppetDetailPropertySection,"sectionGrid")
RegistClassMember(CLuaPuppetDetailPropertySection,"propertyTemplate")
RegistClassMember(CLuaPuppetDetailPropertySection,"bg")
RegistClassMember(CLuaPuppetDetailPropertySection,"splitLine")
RegistClassMember(CLuaPuppetDetailPropertySection,"normalPropertyValueLabelWidth")
RegistClassMember(CLuaPuppetDetailPropertySection,"mfPropertyValueLabelWidth")
RegistClassMember(CLuaPuppetDetailPropertySection,"propertyNameList")
RegistClassMember(CLuaPuppetDetailPropertySection,"propertyValueList")
RegistClassMember(CLuaPuppetDetailPropertySection,"propertyPairs")
RegistClassMember(CLuaPuppetDetailPropertySection,"fightProp")
RegistClassMember(CLuaPuppetDetailPropertySection,"cpFightProp")

function CLuaPuppetDetailPropertySection:Awake()
    self.sectionNameLabel = self.transform:Find("Header/NameLabel"):GetComponent(typeof(UILabel))
    self.sectionGrid = self.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.propertyTemplate = self.transform:Find("KeyValuePair").gameObject
    self.bg = self.transform:GetComponent(typeof(UIWidget))
    self.splitLine = self.transform:Find("Split"):GetComponent(typeof(UISprite))
    self.normalPropertyValueLabelWidth = 160
    self.mfPropertyValueLabelWidth = 500
    self.propertyNameList = nil
    self.propertyValueList = nil
    self.propertyPairs = nil
    self.fightProp = nil
    self.cpFightProp = nil

end
function CLuaPuppetDetailPropertySection:Start()
    self.propertyTemplate:SetActive(false)
end

function CLuaPuppetDetailPropertySection:Init( sectionName, propertyPairs, _fightProp, _cpfightProp) 
    if _fightProp == nil and _cpfightProp == nil then
        return
    end

    self.fightProp = _fightProp
    self.cpFightProp = _cpfightProp
    self.propertyPairs = propertyPairs
    self.sectionNameLabel.text = sectionName
    self.propertyNameList = {}
    self.propertyValueList = {}
    Extensions.RemoveAllChildren(self.sectionGrid.transform)


    for i,v in ipairs(self.propertyPairs) do
        local itemGo = NGUITools.AddChild(self.sectionGrid.gameObject, self.propertyTemplate)
            itemGo:SetActive(true)
            local nameLabel = CommonDefs.GetComponent_Component_Type(itemGo.transform:Find("NameLabel"), typeof(UILabel))
            nameLabel.text = v[1]
            -- CommonDefs.ListAdd(self.propertyNameList, typeof(UILabel), nameLabel)
            table.insert( self.propertyNameList,nameLabel )
            local displayData = v
            UIEventListener.Get(nameLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
                self:OnPropertyNameClick(go, displayData)
            end)
            local valueLabel = CommonDefs.GetComponent_Component_Type(itemGo.transform:Find("ValueLabel"), typeof(UILabel))
            -- CommonDefs.ListAdd(self.propertyValueList, typeof(UILabel), valueLabel)
            table.insert( self.propertyValueList,valueLabel )
    end

    self.sectionGrid:Reposition()
    self:UpdateValue()

    local b = NGUIMath.CalculateRelativeWidgetBounds(self.bg.transform, self.sectionGrid.transform)
    self.bg.height = math.floor(math.abs(b.min.y)) + 20
    self.splitLine:ResetAndUpdateAnchors()
end
function CLuaPuppetDetailPropertySection:OnPropertyNameClick( go, v) 
    if v[3]==nil or v[3]=="" then
        return
    end
    CTooltip.Show(g_MessageMgr:FormatMessage(v[3]), go.transform, CTooltip.AlignType.Top)
end
function CLuaPuppetDetailPropertySection:UpdateValue( )

    if CClientMainPlayer.Inst == nil then
        return
    end

    for i,v in ipairs(self.propertyPairs) do
        local properyName = v[1]
        local type = v[2]
        local label = self.propertyValueList[i]
        if label.width ~= self.normalPropertyValueLabelWidth then
            label.width = self.normalPropertyValueLabelWidth
        end
        if EnumToInt(type) > 0 then
            local val = 0
            if self.fightProp ~= nil then
                val = self.fightProp:GetParam(type)
            elseif self.cpFightProp ~= nil then
                val = self.cpFightProp:GetParam(type)
            end

            if type == EPlayerFightProp.mSpeed or type == EPlayerFightProp.pSpeed then
                if val < 1E-06 then
                    val = 1
                end
                --攻速默认就是1！
                label.text = System.String.Format("{0:F2}", val)
            elseif type == EPlayerFightProp.pFatalDamage or type == EPlayerFightProp.mFatalDamage or type == EPlayerFightProp.AntipFatalDamage or type == EPlayerFightProp.AntimFatalDamage then
                label.text = System.String.Format("{0}%", tostring((NumberTruncate(val, 3) * 100)))
            elseif type == EPlayerFightProp.MF then
                label.width = self.mfPropertyValueLabelWidth
                if val < 1E-06 then
                    label.text = System.String.Format(LocalString.GetString("{0} (幸运值有助于提升红紫鬼装爆率)"), CClientMainPlayer.Inst.StrMF)
                else
                    local formula = CommonDefs.GetFormulaObject(200, typeof(Formula2))
                    local displayVal = (formula ~= nil and math.floor((CClientFormula.Inst:Formula_200(val) * 100)) or 0)
                    label.text = System.String.Format(LocalString.GetString("{0} (红紫鬼装爆率提升[c][00ff00]{1}%[-][/c])"), CClientMainPlayer.Inst.StrMF, displayVal)
                end
            elseif properyName == LocalString.GetString("物理减伤比例") or properyName == LocalString.GetString("法术减伤比例") then
                --百分比，保留小数点后一位，向下取整
                val = - val
                --这地方坑爹的是负数
                if val < System.Double.Epsilon then
                    label.text = "0"
                else
                    label.text = System.String.Format("{0}%", tostring((NumberTruncate(val, 3) * 100)))
                end
            else
                --原始数值直接显示！！
                label.text = tostring((math.floor(val)))
            end
        else
            label.text = LocalString.GetString("无数据")
        end
    end
 
end

