local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local IdPartition = import "L10.Game.IdPartition"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

LuaChooseFabao2GongFengWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaChooseFabao2GongFengWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaChooseFabao2GongFengWnd, "XiaoGuoLabel", "XiaoGuoLabel", UILabel)
RegistChildComponent(LuaChooseFabao2GongFengWnd, "ChengGongLvLabel", "ChengGongLvLabel", UILabel)
RegistChildComponent(LuaChooseFabao2GongFengWnd, "AddBtn", "AddBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaChooseFabao2GongFengWnd,"m_TalismanInBagList")
RegistClassMember(LuaChooseFabao2GongFengWnd,"m_FishWordId")
RegistClassMember(LuaChooseFabao2GongFengWnd,"m_SelectStringId")
RegistClassMember(LuaChooseFabao2GongFengWnd,"m_SelectBagPos")
RegistClassMember(LuaChooseFabao2GongFengWnd,"m_TalismanInBagPos")
RegistClassMember(LuaChooseFabao2GongFengWnd,"m_SelectRow")

function LuaChooseFabao2GongFengWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.AddBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddBtnClick()
	end)

    --@endregion EventBind end
	self.m_TalismanInBagList = {}
	LuaZhenZhaiYuMgr.InitFish2TalismanWords()
end

function LuaChooseFabao2GongFengWnd:Init()
	self.m_TalismanInBagPos = {}
	self.m_TalismanInBagList = {}
	self.m_SelectId = nil
	self.m_FishWordId = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.wordId
	local count = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
	for i=1,count,1 do
		local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
		if itemId then
			local commonItem = CItemMgr.Inst:GetById(itemId)
			if commonItem ~= nil and commonItem.IsEquip and IdPartition.IdIsTalisman(commonItem.TemplateId) and not IdPartition.IdIsXianJiaTalisman(commonItem.TemplateId) then
				table.insert(self.m_TalismanInBagList,commonItem)
				table.insert(self.m_TalismanInBagPos,i)
			end
		end
	end

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_TalismanInBagList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectTalismanAt(row)
		end)

	self.TableView:ReloadData(true,true)
	self.m_SelectRow = -1
	self:RefreshResultLabel()
	--self.TableView:SetSelectRow(0,true)

end

function LuaChooseFabao2GongFengWnd:InitItem(item,row)
	local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local countLabel = item.transform:Find("Count"):GetComponent(typeof(UILabel))
	countLabel.gameObject:SetActive(false) 
	local border = item.transform:Find("Border"):GetComponent(typeof(UISprite))

	local citem = self.m_TalismanInBagList[row + 1]
	local templateId = citem.TemplateId

	if self.m_FishWordId and self.m_FishWordId ~= 0 then
		local cls = math.floor(self.m_FishWordId/100)
		local fabaoType = EquipmentTemplate_Equip.GetData(templateId).TalismanType
		local data = Talisman_SuitConsist.GetData(fabaoType)
		local suit = Talisman_Suit.GetData(data.ActivateSuits[0].SuitId)
		local talismanWords = LuaZhenZhaiYuMgr.m_Fish2TalismanWords[cls]
		local findSameWord = false
		do
			local i = 0 local cnt = suit.Words.Length
			while i < cnt do
				local wordId = suit.Words[i]
				local wordCls = math.floor(wordId/100)
				for j=1,#talismanWords,1 do
					if talismanWords[j] == wordCls then
						findSameWord = true
						break
					end
				end
				if findSameWord then
					break
				end			
				i = i + 1
			end
		end
		if not findSameWord then
			item.gameObject:SetActive(false)
			return
		end
	end
	local fabaoGrade = EquipmentTemplate_Equip.GetData(templateId).Grade
	if self.m_FishWordId%100 <= 4 and fabaoGrade==90 then
		item.gameObject:SetActive(false)
	else
		item.gameObject:SetActive(true)
	end
	
	if citem then
		icon:LoadMaterial(citem.Icon)
		border.spriteName = CUICommonDef.GetItemCellBorder(citem.Equip.QualityType)
	end
end

function LuaChooseFabao2GongFengWnd:OnSelectTalismanAt(row)
	local citem = self.m_TalismanInBagList[row + 1]
	local bagPos = self.m_TalismanInBagPos[row + 1]
	
	self.XiaoGuoLabel.text = nil
	self.ChengGongLvLabel.text = nil
	self.m_SelectStringId = citem.Id
	self.m_SelectBagPos = bagPos
	self.m_SelectRow = row

	CItemInfoMgr.ShowLinkItemInfo(self.m_SelectStringId, false, nil, AlignType.Left, 0, 0, 0, 0)
	self:RefreshResultLabel()
end

function LuaChooseFabao2GongFengWnd:RefreshResultLabel()
	local citem = self.m_TalismanInBagList[self.m_SelectRow + 1]
	if not citem then
		self.ChengGongLvLabel.text = nil
		self.XiaoGuoLabel.text = nil 
		return 
	end

	local fabaoLevel = citem.Grade
	local wordLevel = self.m_FishWordId % 100

	local formulaId = HouseFish_Setting.GetData().ZhengzhaiFishImproveFormula
	local formula = AllFormulas.Action_Formula[formulaId].Formula
	local result
    if formula ~= nil then
        result = formula(nil, nil, {fabaoLevel,wordLevel})
		local chenggonglv = result[1] * 100
		local addLevel = result[2]

		self.ChengGongLvLabel.text = SafeStringFormat3("%d%%",chenggonglv)
		local newWordId
		if self.m_FishWordId and self.m_FishWordId ~= 0 then
			newWordId = self.m_FishWordId + addLevel
			local desc = Word_Word.GetData(newWordId).Description
			local olddesc = Word_Word.GetData(self.m_FishWordId).Description
			local lcPos = string.find(desc,"+")
			local attstring=""
			local newValue = 0
			local oldValue = 0
			if lcPos then
				attstring = string.sub(desc,1,lcPos-1)
				newValue = string.sub(desc,lcPos+1)
				newValue = tonumber(newValue)
				oldValue = string.sub(olddesc,lcPos+1)
				oldValue = tonumber(oldValue)
			end
			self.XiaoGuoLabel.text = SafeStringFormat3("%s+%d",attstring,newValue-oldValue)
		else
			local fabaoType = EquipmentTemplate_Equip.GetData(citem.TemplateId).TalismanType
			local data = Talisman_SuitConsist.GetData(fabaoType)
			local suit = Talisman_Suit.GetData(data.ActivateSuits[0].SuitId)

			do
				local i = 0 local cnt = suit.Words.Length
				while i < cnt do
					local wordId = suit.Words[i]
					local cls = math.floor(wordId/100)
					local fishWordCls = HouseFish_WordMatch.GetData(cls).FishWord
					newWordId = fishWordCls * 100 + addLevel		
					i = i + 1
				end
			end
			self.XiaoGuoLabel.text = Word_Word.GetData(newWordId).Description
		end
		-- self.XiaoGuoLabel.text = Word_Word.GetData(newWordId).Description
	else
		self.ChengGongLvLabel.text = nil
		self.XiaoGuoLabel.text = nil
    end

end

--@region UIEvent

function LuaChooseFabao2GongFengWnd:OnAddBtnClick()
	LuaZhenZhaiYuMgr.m_SelectGongFengTalismanStringId = self.m_SelectStringId
	g_ScriptEvent:BroadcastInLua("SelectedGongFongTalisman",self.m_SelectStringId,self.m_SelectBagPos)
	CUIManager.CloseUI(CLuaUIResources.ChooseFabao2GongFengWnd)
end

--@endregion UIEvent

