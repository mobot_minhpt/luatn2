require("3rdParty/ScriptEvent")
require("common/common_include")

local UISprite = import "UISprite"
local UIWidget = import "UIWidget"
local UITexture = import "UITexture"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CQuizMgr = import "L10.Game.CQuizMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local Mathf = import "UnityEngine.Mathf"
local Vector3 = import "UnityEngine.Vector3"
local Color = import "UnityEngine.Color"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIRes = import "L10.UI.CUIResources"
local UIStaticBlur = import "L10.UI.UIStaticBlur"
local Animation = import "UnityEngine.Animation"
local Physics = import "UnityEngine.Physics"


CLuaDramaEntranceWnd=class()
RegistClassMember(CLuaDramaEntranceWnd,"FocusIndex")
RegistClassMember(CLuaDramaEntranceWnd,"nowAngle")
RegistClassMember(CLuaDramaEntranceWnd,"dramaCount")
RegistClassMember(CLuaDramaEntranceWnd,"entrancetable")
RegistClassMember(CLuaDramaEntranceWnd,"dtable")

RegistClassMember(CLuaDramaEntranceWnd,"Chapters")
RegistClassMember(CLuaDramaEntranceWnd,"CharacterName")
RegistClassMember(CLuaDramaEntranceWnd,"CharacterTexture")
RegistClassMember(CLuaDramaEntranceWnd,"CloseButton")
RegistClassMember(CLuaDramaEntranceWnd,"ReviewButton")
RegistClassMember(CLuaDramaEntranceWnd,"TitleLabel")
RegistClassMember(CLuaDramaEntranceWnd,"InProgressHint")
RegistClassMember(CLuaDramaEntranceWnd,"CompleteHint")
RegistClassMember(CLuaDramaEntranceWnd,"InnerCloseButton")
RegistClassMember(CLuaDramaEntranceWnd,"DesLabel")
RegistClassMember(CLuaDramaEntranceWnd,"CommitButton")
RegistClassMember(CLuaDramaEntranceWnd,"ViewPanel")
RegistClassMember(CLuaDramaEntranceWnd,"ViewPanelAnim")
RegistClassMember(CLuaDramaEntranceWnd,"ReviewChoose")
RegistClassMember(CLuaDramaEntranceWnd,"ResetTaskButton")
RegistClassMember(CLuaDramaEntranceWnd,"ViewCGButton")
RegistClassMember(CLuaDramaEntranceWnd,"StaticBlur")


function CLuaDramaEntranceWnd:isQuestionAnswered(index)
	return CQuizMgr.Inst:GetMultipleQuizQuestionState(index)
end

function CLuaDramaEntranceWnd:Awake()
	self.Chapters = {}
	for i = 1, 3 do
		table.insert(self.Chapters, self.transform:Find("Chapter"..i).gameObject)
	end
	self.CharacterName = self.transform:Find("CharacterArea/Name"):GetComponent(typeof(UILabel))
	self.CharacterTexture = self.transform:Find("CharacterArea/Portrait"):GetComponent(typeof(CUITexture))
	self.CloseButton = self.transform:Find("CloseButton").gameObject
	self.ReviewButton = self.transform:Find("ReviewButton").gameObject
	self.TitleLabel = self.transform:Find("Panel/Mask/Bg/Title"):GetComponent(typeof(UILabel))
	self.InProgressHint = self.transform:Find("Panel/Mask/Button/InProgressHint").gameObject
	self.CompleteHint = self.transform:Find("Panel/Mask/Button/CompleteHint").gameObject
	self.InnerCloseButton = self.transform:Find("Panel/Mask/CloseButton").gameObject
	self.DesLabel = self.transform:Find("Panel/Mask/DesLabel"):GetComponent(typeof(UILabel))
	self.CommitButton = self.transform:Find("Panel/Mask/Button/CommitButton").gameObject
	self.ViewPanel = self.transform:Find("Panel/Mask").gameObject
	self.ViewPanelAnim = self.ViewPanel:GetComponent(typeof(Animation))
	self.ReviewChoose = self.transform:Find("ReviewChoose").gameObject
	self.ResetTaskButton = self.transform:Find("ReviewChoose/ResetTaskButton").gameObject
	self.ViewCGButton = self.transform:Find("ReviewChoose/ViewCGButton").gameObject
	self.StaticBlur = self.transform:Find("Panel/StaticBlur"):GetComponent(typeof(UIStaticBlur))
	self.StaticBlur.gameObject:SetActive(false)

	CommonDefs.AddOnClickListener(self.InnerCloseButton, DelegateFactory.Action_GameObject(function(go) 
		self.StaticBlur.gameObject:SetActive(false)
		self.ViewPanelAnim:Play("juqingentrancewnd_popups_close")
	end), false)

	CommonDefs.AddOnClickListener(self.CloseButton, DelegateFactory.Action_GameObject(function(go) CUIManager.CloseUI(CUIRes.DramaEntranceWnd) end), false)

	CommonDefs.AddOnClickListener(self.ResetTaskButton, DelegateFactory.Action_GameObject(function(go)
		self.ReviewChoose:SetActive(false)
		local openDramaCount = CZhuJueJuQingMgr.Instance:GetSelfJuqing().OpenTaskID.Length
		if not CZhuJueJuQingMgr.Instance:IsChapterFinished(openDramaCount) then 
			g_MessageMgr:ShowMessage("RESET_TASK_FOR_REVIEW_NOT_ALL_FINISHED")
			return
		end
		local message = g_MessageMgr:FormatMessage("ZHUJUEJUQING_RESET_CORFIRM")
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
			Gac2Gas.ResetZhuJueJuQingTasksForReview()
		end), nil, nil, nil, false)
	end), false)

	CommonDefs.AddOnClickListener(self.ViewCGButton, DelegateFactory.Action_GameObject(function(go)
		self.ReviewChoose:SetActive(false)
		local message = g_MessageMgr:FormatMessage("ZHUJUEJUQING_PLAYCG_CORFIRM")
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
			CZhuJueJuQingMgr.Instance:PlaySelfJuqingCG()
		end), nil, nil, nil, false)
	end), false)

	CommonDefs.AddOnClickListener(self.ReviewButton, DelegateFactory.Action_GameObject(function(go)
		if LuaZhuJueJuQingMgr.m_IsTaskReviewOpen then
			if self.ReviewChoose.activeSelf then
				self.ReviewChoose:SetActive(false)
			else
				self.ReviewChoose:SetActive(true)
			end
		else
			CZhuJueJuQingMgr.Instance:PlaySelfJuqingCG()
		end
	end), false)

	CommonDefs.AddOnClickListener(self.ReviewChoose.transform:Find("RaycastCheck").gameObject, DelegateFactory.Action_GameObject(function(go)
		self.ReviewChoose:SetActive(false)
	end), false)
end

function CLuaDramaEntranceWnd:OnEnable()
	g_ScriptEvent:AddListener("ReviewTask", self, "Init")
end

function CLuaDramaEntranceWnd:OnDisable()
	g_ScriptEvent:RemoveListener("ReviewTask", self, "Init")
end

function CLuaDramaEntranceWnd:Update()
	if self.StaticBlur.gameObject.activeSelf and not self.ViewPanelAnim:IsPlaying("juqingentrancewnd_popups_close") and not self.ViewPanelAnim:IsPlaying("juqingentrancewnd_popups_show") and Input.GetMouseButtonUp(0) then
		local cam = UICamera.currentCamera
		local hits = Physics.RaycastAll(cam:ScreenPointToRay(Input.mousePosition), cam.farClipPlane, cam.cullingMask)
		if hits then
			for i = 0, hits.Length - 1 do
				if hits[i].collider.transform.name ~= "Mask" and NGUITools.IsChild(self.ViewPanel.transform, hits[i].collider.transform) then
					return
				end
			end
			self.StaticBlur.gameObject:SetActive(false)
			self.ViewPanelAnim:Play("juqingentrancewnd_popups_close")
		end
	end
end

function CLuaDramaEntranceWnd:Init()
	LuaUtils.SetLocalScale(self.transform:Find("_BgMask_"), 2, 2, 1)
	
	self.StaticBlur.gameObject:SetActive(false)
	self.ViewPanel:SetActive(false)
	self.ReviewChoose:SetActive(false)

	self.dramaCount = CZhuJueJuQingMgr.Instance:GetSelfJuqing().TaskID.Length
	
	local name = CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender)
    self.CharacterTexture:LoadNPCPortrait(name, false)
	self.CharacterName.text = CZhuJueJuQingMgr.Instance:GetSelfJuqing().Name

	self.ReviewButton:SetActive(CZhuJueJuQingMgr.Instance:IsChapterFinished(1))
	
	self:initEntrances()

	if CommonDefs.IS_VN_CLIENT and LocalString.isTH then
		LuaUtils.SetLocalPosition(self.DesLabel.transform, -372, 222, 0)
		self.DesLabel.width = 914
		self.DesLabel.height = 270
	end
end

function CLuaDramaEntranceWnd:OnDestroy()
	
end

function CLuaDramaEntranceWnd:initEntrances()
	local onclick = function(go)
		if self.ReviewChoose.activeSelf then
			self.ReviewChoose:SetActive(false)
		end
		for i = 1, self.dramaCount do
			if go.transform.parent.parent.gameObject == self.Chapters[i] then 
				self:onEntranceClick(i)
				return
			end
		end
	end

	for i = 1, self.dramaCount do
		local entrance = self:instantiateEntrance(i)
		local qnBtn = entrance:GetComponent(typeof(QnButton))
		UIEventListener.Get(entrance).onClick = DelegateFactory.VoidDelegate(function (go)
			onclick(go)
		end)
		UIEventListener.Get(entrance).onPress = DelegateFactory.BoolDelegate(function(go, isPressed)
			if isPressed then
				self.StaticBlur:InitStaticBlur(self.StaticBlur:GetComponent(typeof(UITexture)))
			end
			qnBtn:On_Press(go, isPressed)
		end)
	end
end

function CLuaDramaEntranceWnd:instantiateEntrance(index)
	local title = self.Chapters[index].transform:Find("Title")
	local complete = title:Find("Complete").gameObject
	local inProgress = title:Find("InProgress").gameObject
	local locked = title:Find("Locked").gameObject
	local btnTex = self.Chapters[index].transform:Find("Btn"..index.."/Btn"..index.."_color"):GetComponent(typeof(UITexture))
	local lockedTex = btnTex.transform.parent:Find("Locked").gameObject
	local glow = btnTex.transform:Find("vfx_btn0"..index.."_glow")
	local hint = glow and glow:Find("btn_hint").gameObject
	local deco = CommonDefs.GetComponentsInChildren_Component_Type(btnTex, typeof(UITexture))
	local statusBg = title:Find("Bg_1").gameObject

	complete:SetActive(false)
	inProgress:SetActive(false)
	locked:SetActive(false)
	statusBg:SetActive(true)
	btnTex.alpha = 1
	title.transform.localScale = Vector3.one
	statusBg.transform.localScale = Vector3.one
	locked.transform.localScale = Vector3.one
	complete.transform.localScale = Vector3.one
	inProgress.transform.localScale = Vector3.one

	local data = Task_Task.GetData(CZhuJueJuQingMgr.Instance:GetSelfJuqing().TaskID[index - 1][1])
	if not CZhuJueJuQingMgr.Instance:IsChapterUnLocked(index) or not data or data.Status ~= 0 then
		--CUICommonDef.SetActive(title.gameObject, false)
		--CUICommonDef.SetActive(btnTex.gameObject, false)
		locked:SetActive(true)
		lockedTex:SetActive(true)
		if glow then glow.gameObject:SetActive(false) end
		for i = 1, deco.Length - 1 do deco[i].color = NGUIText.ParseColor24("89829D", 0) end 
		title.transform.localScale = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, 0.9)
		statusBg.transform.localScale = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, 1.11)
		locked.transform.localScale = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, 1.11)
	else
		--CUICommonDef.SetActive(title.gameObject, true)
		--CUICommonDef.SetActive(btnTex.gameObject, true)
		lockedTex:SetActive(false)
		if glow then glow.gameObject:SetActive(true) end
		for i = 1, deco.Length - 1 do deco[i].color = Color.white end
		if CZhuJueJuQingMgr.Instance:IsChapterFinished(index) then 
			complete:SetActive(true)
			hint:SetActive(false)
		elseif CZhuJueJuQingMgr.Instance:IsChapterDoing(index) then
			inProgress:SetActive(true)
			hint:SetActive(true)
		else
			statusBg:SetActive(false)
			hint:SetActive(true)
		end
	end
	return btnTex.gameObject
end

function CLuaDramaEntranceWnd:onEntranceClick(index)
	local data = Task_Task.GetData(CZhuJueJuQingMgr.Instance:GetSelfJuqing().TaskID[index - 1][1])
	if data and data.Status == 0 then
		if CZhuJueJuQingMgr.Instance:IsChapterUnLocked(index) then
			self.ViewPanel:SetActive(true)
			self.ViewPanelAnim:Play("juqingentrancewnd_popups_show")
			self.StaticBlur.gameObject:SetActive(true)
			
			self.TitleLabel.text = CZhuJueJuQingMgr.Instance:IndexToName(index)
			self.DesLabel.text = CZhuJueJuQingMgr.Instance:GetSelfJuqing().TaskDescription[index-1]
			
			self.CommitButton:SetActive(false)
			self.InProgressHint:SetActive(false)
			self.CompleteHint:SetActive(false)
			if not CZhuJueJuQingMgr.Instance:IsChapterAlreadyGet(index) then
				self.CommitButton:SetActive(true)
			else
				if CZhuJueJuQingMgr.Instance:IsChapterFinished(index) then
					self.CompleteHint:SetActive(true)
				else
					self.InProgressHint:SetActive(true)
				end
			end
	
			local func = function(go)
				CZhuJueJuQingMgr.Instance:TryAcceptTask(index)
			end
			CommonDefs.AddOnClickListener(self.CommitButton, DelegateFactory.Action_GameObject(func), false)
		else
			g_MessageMgr:ShowCustomMsg(LocalString.GetString("完成上一章节所有主角剧情任务，方可开启该章节任务"))
		end
	else
		g_MessageMgr:ShowCustomMsg(LocalString.GetString("该章节暂未开启，开启时间请关注游戏公告"))
	end
end

return CLuaDramaEntranceWnd








