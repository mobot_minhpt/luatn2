local UISimpleTableView = import "L10.UI.UISimpleTableView"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
LuaDaFuWongJiaoyiHangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "BuyTableBody", "BuyTableBody", UISimpleTableView)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "SellTableBody", "SellTableBody", UISimpleTableView)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "BuyTemplate", "BuyTemplate", GameObject)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "SellTemplate", "SellTemplate", GameObject)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "EventTip", "EventTip", UILabel)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "OwnMoney", "OwnMoney", GameObject)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "Count", "Count", UILabel)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "CloseButton", "CloseButton", CButton)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaDaFuWongJiaoyiHangWnd, "CanNotBuyLabel", "CanNotBuyLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_WndTick")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_CountDown")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_BuyTableDataSource")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_SellTableDataSource")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_ShopData")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_MineData")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_Sound")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_CurStage")
RegistClassMember(LuaDaFuWongJiaoyiHangWnd, "m_CanOperate")
function LuaDaFuWongJiaoyiHangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
    self.m_CountDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundCard).Time
    self.m_BuyTableDataSource = nil
    self.m_SellTableDataSource = nil
    self.m_ShopData = nil
    self.m_MineData = nil
    self.BuyTemplate.gameObject:SetActive(false)
    self.SellTemplate.gameObject:SetActive(false)
    self.m_Sound = nil
    self.m_CurStage = nil
    self.m_CanOperate = false
end

function LuaDaFuWongJiaoyiHangWnd:Init()
    self.m_CurStage = LuaDaFuWongMgr.CurStage
    self.CanNotBuyLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_JiaoYiHang_Cannot_Operate")
    self:StartCoundDownTick()
    self:InitData()
    self:InitTableView()
    self:UpdateMoney()
    self:ShowEventTip()
    if not self.m_Sound then
        self.m_Sound = SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_JiaoYiHang", Vector3.zero, nil, 0)
    end
end

function LuaDaFuWongJiaoyiHangWnd:StartCoundDownTick()
    if self.m_CurStage == EnumDaFuWengStage.RoundCard and LuaDaFuWongMgr.IsMyRound then
        self.m_CanOperate = true
        self.CanNotBuyLabel.gameObject:SetActive(false)
        self.CountDownLabel.gameObject:SetActive(true)
        local countDown = self.m_CountDown
        local endFunc = function() end
        local durationFunc = function(time) if self.CountDownLabel then self.CountDownLabel.text = time end end
        LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.ShangHui,countDown,durationFunc,endFunc)
    else
        self.m_CanOperate = false
        self.CanNotBuyLabel.gameObject:SetActive(true)
        self.CountDownLabel.gameObject:SetActive(false)
    end

end
function LuaDaFuWongJiaoyiHangWnd:InitData()
    self.m_ShopData = {}
    self.m_MineData = {}
    if LuaDaFuWongMgr.CurShopData and LuaDaFuWongMgr.PlayerInfo then
        local shopData = {}
        local mineData = {}
        if CClientMainPlayer.Inst then
            local myData = LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id]
            for k,v in pairs(myData.GoodsInfo) do
                table.insert(mineData,{v.goodsId,v.price,k})
            end
            DaFuWeng_Goods.Foreach(function(k,v)
                if bit.band(myData.BuyMask ,bit.lshift(1, k - 1)) == 0 then
                    table.insert(shopData,{k,LuaDaFuWongMgr.CurShopData[k].price,LuaDaFuWongMgr.CurShopData[k].changeValue})
                end
            end)
        end
        self.m_ShopData = shopData
        self.m_MineData = mineData
    end
    local curMoney = 0
    if CClientMainPlayer.Inst and LuaDaFuWongMgr.PlayerInfo then curMoney = LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id].Money end
    table.sort(self.m_ShopData,function(a,b)
        local res = false
        if a[2] <= curMoney and b[2] > curMoney then
            return true
        elseif a[2] > curMoney and b[2] <= curMoney then
            return false
        else
            return  a[1] < b[1]
        end
    end)
end
function LuaDaFuWongJiaoyiHangWnd:InitTableView()
    if not self.m_BuyTableDataSource then
        self.m_BuyTableDataSource = DefaultUISimpleTableViewDataSource.Create(
            function()
                return self:NumberOfBuyRows()
            end,
            function(index)
                return self:BuyCellForRowAtIndex(index)
            end
        )
    end
    if not self.m_SellTableDataSource then
        self.m_SellTableDataSource = DefaultUISimpleTableViewDataSource.Create(
            function()
                return self:NumberOfSellRows()
            end,
            function(index)
                return self:SellCellForRowAtIndex(index)
            end
        )
    end

    self.BuyTableBody:Clear()
    self.BuyTableBody.dataSource = self.m_BuyTableDataSource
    self.BuyTableBody:LoadData(0, true)

    self.SellTableBody:Clear()
    self.SellTableBody.dataSource = self.m_SellTableDataSource
    self.SellTableBody:LoadData(0, true)
end

function LuaDaFuWongJiaoyiHangWnd:NumberOfBuyRows()
    return #self.m_ShopData
end

function LuaDaFuWongJiaoyiHangWnd:NumberOfSellRows()
    return #self.m_MineData
end

function LuaDaFuWongJiaoyiHangWnd:BuyCellForRowAtIndex(index)
    local data = self.m_ShopData[index + 1]
    local itemData = DaFuWeng_Goods.GetData(data[1])
    local ItemView = self.BuyTableBody:DequeueReusableCellWithIdentifier("ItemCell")
    if ItemView == nil then
        ItemView = self.BuyTableBody:AllocNewCellWithIdentifier(self.BuyTemplate,"ItemCell")
    end
    local curMoney = 0
    if CClientMainPlayer.Inst and LuaDaFuWongMgr.PlayerInfo then 
        curMoney = LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id] and LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id].Money 
    end
    local curPrice = data[2]
    ItemView.gameObject:SetActive(true)
    ItemView.transform:Find("Table/Name/NameLabel"):GetComponent(typeof(UILabel)).text = itemData.Name
    ItemView.transform:Find("Table/Name/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
    ItemView.transform:Find("Table/Price/PriceLabel"):GetComponent(typeof(UILabel)).text = data[2]
    local noLabel = ItemView.transform:Find("Table/UpOrDown/NoLabel").gameObject
    local UpLabel = ItemView.transform:Find("Table/UpOrDown/UpLabel"):GetComponent(typeof(UILabel))
    local Sprite = ItemView.transform:Find("Table/UpOrDown/Sprite"):GetComponent(typeof(UISprite))
    noLabel.gameObject:SetActive(math.floor(math.abs(data[3] * 100)) == 0)
    UpLabel.gameObject:SetActive(math.floor(math.abs(data[3] * 100)) ~= 0)
    Sprite.gameObject:SetActive(math.floor(math.abs(data[3] * 100)) ~= 0)    
    if data[3] < 0 and math.floor(math.abs(data[3] * 100)) ~= 0 then
        local precent = math.abs(data[3])
        Sprite.spriteName = "common_arrow_07_green"
        Sprite.color = NGUIText.ParseColor24("0CAD0C",0)
        UpLabel.text = math.floor(precent * 100) .. "%"
        UpLabel.color = NGUIText.ParseColor24("0CAD0C",0)
    elseif data[3] > 0 and math.floor(math.abs(data[3] * 100)) ~= 0 then
        local precent = math.abs(data[3])
        Sprite.spriteName = "common_arrow_07_red"
        Sprite.color = NGUIText.ParseColor24("ba2727",0)
        UpLabel.text = math.floor(precent * 100) .. "%"
        UpLabel.color = NGUIText.ParseColor24("ba2727",0)
    end
    local BuyBtn = ItemView.transform:Find("Table/Buy/BuyBtn")
    local noBuyLabel = ItemView.transform:Find("Table/Buy/NotBuyLabel")
    BuyBtn.gameObject:SetActive(curMoney >= curPrice)
    noBuyLabel.gameObject:SetActive(curMoney < curPrice)
    BuyBtn.gameObject:GetComponent(typeof(CButton)).Enabled = self.m_CanOperate
    if self.m_CanOperate then 
        BuyBtn.gameObject:GetComponent(typeof(UIWidget)).alpha = 1 
    else
        BuyBtn.gameObject:GetComponent(typeof(UIWidget)).alpha = 0.5
    end
    UIEventListener.Get(BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick(index)
	end)
    ItemView.transform:Find("Table"):GetComponent(typeof(UITable)):Reposition()
    return ItemView
end

function LuaDaFuWongJiaoyiHangWnd:SellCellForRowAtIndex(index)
    local data = self.m_MineData[index + 1]
    local itemData = DaFuWeng_Goods.GetData(data[1])
    local oriPrice = LuaDaFuWongMgr:GetGoodsCurPriceWithId(data[1]).price
    local ItemView = self.SellTableBody:DequeueReusableCellWithIdentifier("ItemCell")
    if ItemView == nil then
        ItemView = self.SellTableBody:AllocNewCellWithIdentifier(self.SellTemplate,"ItemCell")
    end
    ItemView.gameObject:SetActive(true)
    ItemView.transform:Find("Table/Name/NameLabel"):GetComponent(typeof(UILabel)).text = itemData.Name
    ItemView.transform:Find("Table/Name/Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
    ItemView.transform:Find("Table/Price/PriceLabel"):GetComponent(typeof(UILabel)).text = data[2]
    ItemView.transform:Find("Table/Price/redDot").gameObject:SetActive(data[2] < oriPrice)
    local SellBtn = ItemView.transform:Find("Table/Sell/SellBtn")
    SellBtn.gameObject:GetComponent(typeof(CButton)).Enabled = self.m_CanOperate
    if self.m_CanOperate then 
        SellBtn.gameObject:GetComponent(typeof(UIWidget)).alpha = 1 
    else
        SellBtn.gameObject:GetComponent(typeof(UIWidget)).alpha = 0.5
    end
    UIEventListener.Get(SellBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSellBtnClick(index)
	end)
    ItemView.transform:Find("Table"):GetComponent(typeof(UITable)):Reposition()
    
    return ItemView
end


function LuaDaFuWongJiaoyiHangWnd:ShowEventTip()
    if not LuaDaFuWongMgr.EventInfo then self.EventTip.gameObject:SetActive(false)  return end
    local eventInfo = LuaDaFuWongMgr.EventInfo
    local eventData = DaFuWeng_ShangHuiEvent.GetData(eventInfo[2])
    local colors = {"ff7272","00ff60"}
    local upOrdownValue = {LocalString.GetString("上涨"),LocalString.GetString("下降")}
    if not eventData then self.EventTip.gameObject:SetActive(false) return end

    --if eventInfo.Data.Type == 1 then
        local attach = eventData.AttachValue
        local attachName = DaFuWeng_Goods.GetData(attach).Name
        local upOrdown = eventData.ChangeValue > 0
        local index = 1 
        if not upOrdown then index = 2 end
        self.EventTip.text = g_MessageMgr:FormatMessage("DaFuWeng_JiaoYiHang_WorldEventTip",attachName,colors[index],upOrdownValue[index])
    -- elseif eventInfo.Data.Type == 2 then
    --     local attachName = LocalString.GetString("所有货物")
    --     local upOrdown = eventInfo.Data.ChangeValue > 1
    --     local index = 1 
    --     if not upOrdown then index = 2 end
    --     self.EventTip.text = g_MessageMgr:FormatMessage("DaFuWeng_JiaoYiHang_WorldEventTip",attachName,colors[index],upOrdownValue[index])
    -- else
    --     self.EventTip.gameObject:SetActive(false)
    -- end
end

function LuaDaFuWongJiaoyiHangWnd:UpdateView(curStage)
    self.m_CurStage = curStage
    self:StartCoundDownTick()
    self:InitData()
    self.BuyTableBody:LoadData(0, true)
    self.SellTableBody:LoadData(0, true)
    self:UpdateMoney()
    self:ShowEventTip()
end

function LuaDaFuWongJiaoyiHangWnd:UpdateMoney()
    local curMoney = 0
    if CClientMainPlayer.Inst and LuaDaFuWongMgr.PlayerInfo then 
        curMoney = LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id] and LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id].Money 
    end
    self.OwnMoney.transform:Find("Money"):GetComponent(typeof(UILabel)).text = curMoney
end

function LuaDaFuWongJiaoyiHangWnd:OnBuyBtnClick(index)
    if not self.m_CanOperate then return end
    local data = self.m_ShopData[index + 1]
    local itemData = DaFuWeng_Goods.GetData(data[1])
    local curPrice = data[2]
    local curMoney = 0
    if CClientMainPlayer.Inst and LuaDaFuWongMgr.PlayerInfo then 
        curMoney = LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id] and LuaDaFuWongMgr.PlayerInfo[CClientMainPlayer.Inst.Id].Money 
    end
    local name = itemData.Name
    local leftMoney = curMoney - curPrice
    --g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("DaFuWeng_BuyGoods_Confirm", curPrice,name,leftMoney), function()
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Buy", Vector3.zero, nil, 0)
        Gac2Gas.DaFuWengGoodsOperate(true,data[1])
    --end, nil, nil, nil, false)
end
function LuaDaFuWongJiaoyiHangWnd:GetGuideGo(methodName)
	if methodName == "GetCloseBtn" then
        return self.CloseButton.gameObject
	end
end
function LuaDaFuWongJiaoyiHangWnd:OnSellBtnClick(index)
    if not self.m_CanOperate then return end
    local data = self.m_MineData[index + 1]
    local itemData = DaFuWeng_Goods.GetData(data[1])
    local name = itemData.Name
    local curPrice = LuaDaFuWongMgr:GetGoodsCurPriceWithId(data[1]).price
    --g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("DaFuWeng_SellGoods_Confirm", curPrice,name), function()
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_Sell", Vector3.zero, nil, 0)
        Gac2Gas.DaFuWengGoodsOperate(false,data[3])
    --end, nil, nil, nil, false)
end
function LuaDaFuWongJiaoyiHangWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self, "UpdateView")
end
function LuaDaFuWongJiaoyiHangWnd:OnDisable()
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.ShangHui)
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self, "UpdateView")
    if self.m_Sound then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound=nil
    end
end
--@region UIEvent

function LuaDaFuWongJiaoyiHangWnd:OnCloseButtonClick()
    -- 通知服务器玩家交易结束
    --Gac2Gas.DaFuWengFinishCurStage(EnumDaFuWengStage.RoundGridShangHui)
    CUIManager.CloseUI(CLuaUIResources.DaFuWongJiaoyiHangWnd)
end


--@endregion UIEvent

