-- Auto Generated!!
local Boolean = import "System.Boolean"
local CAssistantEvaluateWnd = import "L10.UI.CAssistantEvaluateWnd"
local CButton = import "L10.UI.CButton"
local CChatLinkMgr = import "CChatLinkMgr"
local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local ContactAssistantDataMgr = import "L10.Game.ContactAssistantDataMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local Kefuzhuanqu_PingJia = import "L10.Game.Kefuzhuanqu_PingJia"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CAssistantEvaluateWnd.m_Awake_CS2LuaHook = function (this) 
    if CAssistantEvaluateWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CAssistantEvaluateWnd.Instance = this
end
CAssistantEvaluateWnd.m_Init_CS2LuaHook = function (this) 
    local count = Kefuzhuanqu_PingJia.GetDataCount()
    do
        local i = 1
        while i <= count do
            local data = Kefuzhuanqu_PingJia.GetData(i)
            if data ~= nil then
                CommonDefs.DictSet(this.desDic1, typeof(UInt32), data.Star, typeof(String), data.Content)
                CommonDefs.DictSet(this.desDic2, typeof(UInt32), data.Star, typeof(String), data.Description)
            end
            i = i + 1
        end
    end

    this.templateNode:SetActive(false)
    --UIEventListener.Get(this.starArray[this.starArray.Length-1]).onClick(this.starArray[this.starArray.Length-1]);
    this:InitStarPanel(0)
end
CAssistantEvaluateWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.submitBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.submitBtn).onClick, MakeDelegateFromCSFunction(this.SubmitEvaluate, VoidDelegate, this), true)
    CommonDefs.EnumerableIterate(this.starArray, DelegateFactory.Action_object(function (___value) 
        local starBtn = ___value
        UIEventListener.Get(starBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(starBtn).onClick, MakeDelegateFromCSFunction(this.OnStarClick, VoidDelegate, this), true)
    end))
    EventManager.AddListenerInternal(EnumEventType.EvaluateContactAssistantSign, MakeDelegateFromCSFunction(this.InitEvaluateScroll, MakeGenericClass(Action1, UInt32), this))
end
CAssistantEvaluateWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.submitBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.submitBtn).onClick, MakeDelegateFromCSFunction(this.SubmitEvaluate, VoidDelegate, this), false)

    CommonDefs.EnumerableIterate(this.starArray, DelegateFactory.Action_object(function (___value) 
        local starBtn = ___value
        UIEventListener.Get(starBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(starBtn).onClick, MakeDelegateFromCSFunction(this.OnStarClick, VoidDelegate, this), false)
    end))
    EventManager.RemoveListenerInternal(EnumEventType.EvaluateContactAssistantSign, MakeDelegateFromCSFunction(this.InitEvaluateScroll, MakeGenericClass(Action1, UInt32), this))
    CommonDefs.DictClear(ContactAssistantDataMgr.Inst.evaluateSignDic)
end
CAssistantEvaluateWnd.m_InitEvaluateScroll_CS2LuaHook = function (this, star) 
    if not CommonDefs.DictContains(ContactAssistantDataMgr.Inst.evaluateSignDic, typeof(UInt32), star) then
        return
    end

    CommonDefs.DictClear(this.saveChooseSign)
    Extensions.RemoveAllChildren(this.grid.transform)
    this.maxChooseCount = 0

    local evaluateDic = CommonDefs.DictGetValue(ContactAssistantDataMgr.Inst.evaluateSignDic, typeof(UInt32), star)

    local count = evaluateDic.Count
    this.maxChooseCount = count
    do
        local i = 1
        while i <= count do
            if CommonDefs.DictContains(evaluateDic, typeof(Int32), i) then
                local chooseNode = NGUITools.AddChild(this.grid.gameObject, this.templateNode)
                chooseNode:SetActive(true)
                CommonDefs.GetComponent_Component_Type(chooseNode.transform:Find("text"), typeof(UILabel)).text = CommonDefs.DictGetValue(evaluateDic, typeof(Int32), i)
                local index = i
                UIEventListener.Get(chooseNode).onClick = DelegateFactory.VoidDelegate(function (p) 
                    this:EvaluateSignChoose(index, chooseNode)
                end)
            end
            i = i + 1
        end
    end

    this.grid:Reposition()
    this.scrollview:ResetPosition()
end
CAssistantEvaluateWnd.m_EvaluateSignChoose_CS2LuaHook = function (this, index, btn) 
    local sign = btn.transform:Find("clicksign").gameObject
    if sign.activeSelf then
        sign:SetActive(false)
        CommonDefs.DictSet(this.saveChooseSign, typeof(Int32), index, typeof(Boolean), false)
    else
        sign:SetActive(true)
        CommonDefs.DictSet(this.saveChooseSign, typeof(Int32), index, typeof(Boolean), true)
    end
end
CAssistantEvaluateWnd.m_InitStarPanel_CS2LuaHook = function (this, index) 
    this.feeling = index

    if index > 0 then
        this.InfoNode:SetActive(true)
        this.InitNode:SetActive(false)
        CommonDefs.GetComponent_GameObject_Type(this.submitBtn, typeof(CButton)).Enabled = true
    else
        this.InfoNode:SetActive(false)
        this.InitNode:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.submitBtn, typeof(CButton)).Enabled = false
    end


    do
        local i = 0
        while i < this.starArray.Length do
            if i < index then
                this.starArray[i].transform:Find("star").gameObject:SetActive(true)
            else
                this.starArray[i].transform:Find("star").gameObject:SetActive(false)
            end
            i = i + 1
        end
    end

    if CommonDefs.DictContains(ContactAssistantDataMgr.Inst.evaluateSignDic, typeof(UInt32), index) then
        this:InitEvaluateScroll(index)
    else
        Gac2Gas.GetMaintenanceZoneTags(index)
    end

    this.desLabel1.text = ""
    this.desLabel2.text = ""

    if CommonDefs.DictContains(this.desDic1, typeof(UInt32), index) then
        this.desLabel1.text = CChatLinkMgr.TranslateToNGUIText(CommonDefs.DictGetValue(this.desDic1, typeof(UInt32), index), false)
    end

    if CommonDefs.DictContains(this.desDic2, typeof(UInt32), index) then
        this.desLabel2.text = CChatLinkMgr.TranslateToNGUIText(CommonDefs.DictGetValue(this.desDic2, typeof(UInt32), index), false)
    end
end
CAssistantEvaluateWnd.m_GetSignString_CS2LuaHook = function (this) 
    local result = "|"
    do
        local i = 1
        while i <= this.maxChooseCount do
            if CommonDefs.DictContains(this.saveChooseSign, typeof(Int32), i) and CommonDefs.DictGetValue(this.saveChooseSign, typeof(Int32), i) then
                result = (result .. tostring(i)) .. "|"
            end
            i = i + 1
        end
    end

    result = (result .. ":") .. this.otherWord.value

    return result
end
CAssistantEvaluateWnd.m_CheckSubmitStatus_CS2LuaHook = function (this) 
    if this.feeling < 5 then
        if this.saveChooseSign.Count > 0 then
            do
                local __itr_is_triger_return = nil
                local __itr_result = nil
                CommonDefs.DictIterateWithRet(this.saveChooseSign, DelegateFactory.DictIterFunc(function (___key, ___value) 
                    local v = {}
                    v.Key = ___key
                    v.Value = ___value
                    if v.Value then
                        __itr_is_triger_return = true
                        __itr_result = true
                        return true
                    end
                end))
                if __itr_is_triger_return == true then
                    return __itr_result
                end
            end
            g_MessageMgr:ShowMessage("CHOOSE_ONE_OPTION_ALERT")
            return false
        else
            g_MessageMgr:ShowMessage("CHOOSE_ONE_OPTION_ALERT")
            return false
        end
    else
        return true
    end
end
CAssistantEvaluateWnd.m_SubmitEvaluate_CS2LuaHook = function (this, go) 
    if this:CheckSubmitStatus() then
        this.aid = CContactAssistantMgr.Inst.clickAnswerId
        if CContactAssistantMgr.Inst.clickChatAnswerUUid > 0 then
            CIMMgr.Inst:DelMsgAndRefresh(CContactAssistantMgr.Inst.clickChatAnswerUUid)
            CContactAssistantMgr.Inst.clickChatAnswerUUid = 0
        end
        Gac2Gas.EvaluateAnswer(this.aid, 0, 0, this.feeling, this:GetSignString())
        this:Close()
    end
end
