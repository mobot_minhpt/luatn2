-- local VoidDelegate = import "UIEventListener+VoidDelegate"
-- local UIEventListener = import "UIEventListener"
-- local Gac2Gas = import "L10.Game.Gac2Gas"
-- local CGuanNingChoiceWnd = import "L10.UI.CGuanNingChoiceWnd"


CLuaGuanNingChoiceWnd = class()
RegistClassMember(CLuaGuanNingChoiceWnd,"oldPlayerBtn")
RegistClassMember(CLuaGuanNingChoiceWnd,"newPlayerBtn")

function CLuaGuanNingChoiceWnd:Awake()
    self.oldPlayerBtn = self.transform:Find("Anchor/OldPlayerBtn").gameObject
    self.newPlayerBtn = self.transform:Find("Anchor/NewPlayerBtn").gameObject

    UIEventListener.Get(self.oldPlayerBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GuanNing_EnterChoice_OldPlayer_Confirm"), function()
            Gac2Gas.RequestSignUpGnjcFromGuanNingXunLianWin(CLuaGuanNingMgr.m_XunLianPlayIndex)
            CUIManager.CloseUI(CLuaUIResources.GuanNingChoiceWnd)
        end, nil, LocalString.GetString("确认"), LocalString.GetString("取消"), false)
    end)
    UIEventListener.Get(self.newPlayerBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestEnterGuanNingXunLian()
        CUIManager.CloseUI(CLuaUIResources.GuanNingChoiceWnd)
    end)
end

return CLuaGuanNingChoiceWnd
