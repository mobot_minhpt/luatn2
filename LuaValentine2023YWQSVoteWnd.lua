local DelegateFactory        = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemMgr               = import "L10.Game.CItemMgr"
local EnumItemPlace          = import "L10.Game.EnumItemPlace"
local CItemInfoMgr           = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr     = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType      = import "L10.UI.CTooltip+AlignType"

LuaValentine2023YWQSVoteWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaValentine2023YWQSVoteWnd, "qnAddSubAndInput")
RegistClassMember(LuaValentine2023YWQSVoteWnd, "count")
RegistClassMember(LuaValentine2023YWQSVoteWnd, "disableSprite")

function LuaValentine2023YWQSVoteWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local content = self.transform:Find("Content")
    self.qnAddSubAndInput = content:Find("AddSubAndInput"):GetComponent(typeof(QnAddSubAndInputButton))
    self.qnAddSubAndInput:SetValue(1, true)

    local icon = content:Find("Icon"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(Item_Item.GetData(Valentine2023_YWQS_Setting.GetData().VoteItemId).Icon)
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnItemClick(go)
	end)

    self.count = content:Find("Icon/Count"):GetComponent(typeof(UILabel))
    self.disableSprite = content:Find("Icon/DisableSprite").gameObject
    self:UpdateCount()

    UIEventListener.Get(content:Find("OKButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)

    UIEventListener.Get(content:Find("CancelButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)
end

function LuaValentine2023YWQSVoteWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaValentine2023YWQSVoteWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end

function LuaValentine2023YWQSVoteWnd:OnSendItem(args)
    local item = args and args[0] and CItemMgr.Inst:GetById(args[0]) or nil
    if item and item.TemplateId == Valentine2023_YWQS_Setting.GetData().VoteItemId then
        self:UpdateCount()
    end
end

function LuaValentine2023YWQSVoteWnd:OnSetItemAt(args)
    self:UpdateCount()
end

function LuaValentine2023YWQSVoteWnd:UpdateCount()
    local cnt = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, Valentine2023_YWQS_Setting.GetData().VoteItemId)
    self.count.text = cnt
    self.disableSprite:SetActive(cnt == 0)
end

function LuaValentine2023YWQSVoteWnd:Init()
end

--@region UIEvent

function LuaValentine2023YWQSVoteWnd:OnItemClick(go)
    local itemId = Valentine2023_YWQS_Setting.GetData().VoteItemId
    local cnt = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
    if cnt > 0 then
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
    else
        CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, go.transform, CTooltipAlignType.Right)
    end
end

function LuaValentine2023YWQSVoteWnd:OnOKButtonClick()
    Gac2Gas.VoteToValentineYWQSCouple(LuaValentine2023Mgr.YWQSInfo.votePlayerId, self.qnAddSubAndInput:GetValue())
end

function LuaValentine2023YWQSVoteWnd:OnCancelButtonClick()
    CUIManager.CloseUI(CLuaUIResources.Valentine2023YWQSVoteWnd)
end

--@endregion UIEvent
