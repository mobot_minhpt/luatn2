local DelegateFactory       = import "DelegateFactory"
local Profession            = import "L10.Game.Profession"
local CPlayerInfoMgr        = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel            = import "L10.Game.EChatPanel"
local AlignType             = import "CPlayerInfoMgr+AlignType"
local CChatHelper           = import "L10.UI.CChatHelper"
local CServerTimeMgr        = import "L10.Game.CServerTimeMgr"

LuaTeamRecruitDetailWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaTeamRecruitDetailWnd, "target")
RegistClassMember(LuaTeamRecruitDetailWnd, "level")
RegistClassMember(LuaTeamRecruitDetailWnd, "recruitTable")
RegistClassMember(LuaTeamRecruitDetailWnd, "msg")
RegistClassMember(LuaTeamRecruitDetailWnd, "leftTime")

RegistClassMember(LuaTeamRecruitDetailWnd, "isMyRecruit")
RegistClassMember(LuaTeamRecruitDetailWnd, "recruitInfo")

function LuaTeamRecruitDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local anchor = self.transform:Find("Anchor")
    self.target = anchor:Find("Target/Label"):GetComponent(typeof(UILabel))
    self.level = anchor:Find("Level/Label"):GetComponent(typeof(UILabel))
    self.recruitTable = anchor:Find("Recruit/Table")
    self.msg = anchor:Find("Message/Label"):GetComponent(typeof(UILabel))
    self.leftTime = anchor:Find("LeftTime"):GetComponent(typeof(UILabel))

    if LuaTeamRecruitMgr.dataForDetailWnd.id ~= CClientMainPlayer.Inst.Id then
        Gac2Gas.ClientLogCount(EnumClientLogCountKey.TeamRecruit_Check, 1)
    end
end

function LuaTeamRecruitDetailWnd:SetState(stateRoot, isOk)
    if self.isMyRecruit then
        stateRoot:Find("Ok").gameObject:SetActive(false)
        stateRoot:Find("CanChat").gameObject:SetActive(false)
        stateRoot:Find("CantChat").gameObject:SetActive(false)
    else
        stateRoot:Find("Ok").gameObject:SetActive(isOk)
        stateRoot:Find("CanChat").gameObject:SetActive(not isOk and not self.recruitInfo.check)
        stateRoot:Find("CantChat").gameObject:SetActive(not isOk and self.recruitInfo.check)
    end
end

function LuaTeamRecruitDetailWnd:InitRecruitInfo()
    self.recruitInfo = LuaTeamRecruitMgr.dataForDetailWnd
    self.isMyRecruit = CClientMainPlayer.Inst.Id == self.recruitInfo.id
end

function LuaTeamRecruitDetailWnd:InitPlayerInfo()
    local playerInfo = self.transform:Find("Anchor/PlayerInfo")
    local icon = playerInfo:Find("Icon"):GetComponent(typeof(CUITexture))
    local name = playerInfo:Find("Name"):GetComponent(typeof(UILabel))

    icon:LoadPortrait(CUICommonDef.GetPortraitName(self.recruitInfo.class, self.recruitInfo.gender, -1), true)
    name.text = self.recruitInfo.name

    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CPlayerInfoMgr.ShowPlayerPopupMenu(self.recruitInfo.id, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
    end)
end

function LuaTeamRecruitDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("TeamRecruitSelfInfo", self, "OnTeamRecruitSelfInfo")
end

function LuaTeamRecruitDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("TeamRecruitSelfInfo", self, "OnTeamRecruitSelfInfo")
end

function LuaTeamRecruitDetailWnd:OnTeamRecruitSelfInfo()
    if self.isMyRecruit then
        self.recruitInfo = LuaTeamRecruitMgr.myData
        self:UpdateRecruitInfo()
        self:UpdateLeftTime()
    end
end

function LuaTeamRecruitDetailWnd:UpdateRecruitInfo()
    self.target.text = TeamRecruit_Type.GetData(self.recruitInfo.target).Name
    self.level.text = SafeStringFormat3("%d-%d", self.recruitInfo.gardeStart, self.recruitInfo.gardeEnd)
    self.msg.text = self.recruitInfo.msg

    local typeList = self.recruitInfo.typeList
    for i = 1, 4 do
        local child = self.recruitTable:GetChild(i - 1)
        if typeList[i] then
            child.gameObject:SetActive(true)
            local typeLabel = child:Find("Type"):GetComponent(typeof(UILabel))
            local classSprite = child:Find("Class"):GetComponent(typeof(UISprite))
            local equipScoreLabel = child:Find("EquipScore"):GetComponent(typeof(UILabel))

            local typeStr = TeamRecruit_Feature.GetData(typeList[i][1]).Name
            if typeList[i][2] > 0 then
                typeLabel.text = SafeStringFormat3("%s-", typeStr)
                classSprite.gameObject:SetActive(true)
                classSprite.spriteName = Profession.GetIconByNumber(typeList[i][2])
                classSprite:ResetAndUpdateAnchors()
            else
                typeLabel.text = typeStr
                classSprite.gameObject:SetActive(false)
            end
            equipScoreLabel.text = SafeStringFormat3(LocalString.GetString("装评≥ %d"), math.max(typeList[i][3], 0))
        else
            child.gameObject:SetActive(false)
        end
    end
end

function LuaTeamRecruitDetailWnd:UpdateLeftTime()
    local passSecond = math.ceil(CServerTimeMgr.Inst.timeStamp - self.recruitInfo.startTime)
    if self.recruitInfo.duration <= passSecond then
        self.leftTime.text = SafeStringFormat3(LocalString.GetString("招募已失效"))
        return
    end

    if self.isMyRecruit then
        local leftSecond = math.max(self.recruitInfo.duration - passSecond, 0)
        local hour = math.floor(leftSecond / 3600)
        local min = math.floor(leftSecond % 3600 / 60)

        local str = ""
        if hour > 0 then
            if min > 0 then
                str = SafeStringFormat3(LocalString.GetString("%d小时%d分钟"), hour, min)
            else
                str = SafeStringFormat3(LocalString.GetString("%d小时"), hour)
            end
        else
            str = SafeStringFormat3(LocalString.GetString("%d分钟"), math.max(min, 1))
        end

        self.leftTime.text = SafeStringFormat3(LocalString.GetString("本次招募剩余有效期%s"), str)
    else
        local hour = math.floor(passSecond / 3600)
        local min = math.floor(passSecond % 3600 / 60)
        local second = math.floor(passSecond % 60)
        local str = ""
        if hour > 0 then
            if min > 0 then
                str = SafeStringFormat3(LocalString.GetString("%d小时%d分钟"), hour, min)
            else
                str = SafeStringFormat3(LocalString.GetString("%d小时"), hour)
            end
        elseif min > 0 then
            str = SafeStringFormat3(LocalString.GetString("%d分钟"), min)
        else
            str = SafeStringFormat3(LocalString.GetString("%d秒"), second)
        end

        self.leftTime.text = SafeStringFormat3(LocalString.GetString("此招募发布于%s之前"), str)
    end
end

function LuaTeamRecruitDetailWnd:Init()
    self:InitRecruitInfo()
    local title = self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel))
    title.text = self.isMyRecruit and LocalString.GetString("我的招募") or LocalString.GetString("招募详情")

    self:InitPlayerInfo()
    self:UpdateRecruitInfo()

    local isLevelOk = false
    local isFeatureOk = false
    if not self.isMyRecruit then
        local myLevel = CClientMainPlayer.Inst.Level
        isLevelOk = myLevel >= self.recruitInfo.gardeStart and myLevel <= self.recruitInfo.gardeEnd

        local typeList = self.recruitInfo.typeList
        local myClass = EnumToInt(CClientMainPlayer.Inst.Class)
        for i = 1, #typeList do
            local child = self.recruitTable:GetChild(i - 1)
            local indexLabel = child:Find("Index"):GetComponent(typeof(UILabel))
            local typeLabel = child:Find("Type"):GetComponent(typeof(UILabel))
            local classTrans = child:Find("Class")
            local equipScoreLabel = child:Find("EquipScore"):GetComponent(typeof(UILabel))

            local isTypeOk = false
            local classArray = TeamRecruit_Feature.GetData(typeList[i][1]).Class
            for j = 0, classArray.Length - 1 do
                if classArray[j] == myClass then
                    isTypeOk = true
                    break
                end
            end

            local isClassOk = false
            if isTypeOk then
                isClassOk = typeList[i][2] <= 0 and true or (typeList[i][2] == myClass)
            end

            local isEquipScoreOk = false
            if isTypeOk then
                isEquipScoreOk = CClientMainPlayer.Inst.EquipScore >= typeList[i][3]
            end

            local isAllOk = isTypeOk and isClassOk and isEquipScoreOk
            indexLabel.color = NGUIText.ParseColor24(isAllOk and "B3F8FF" or "777777", 0)
            typeLabel.color = NGUIText.ParseColor24(isAllOk and "FFFFFF" or "B3B3B3", 0)
            LuaUtils.SetLocalPositionZ(classTrans, isAllOk and 0 or -1)
            equipScoreLabel.color = NGUIText.ParseColor24(isAllOk and "FFFFFF" or "B3B3B3", 0)

            if isAllOk then isFeatureOk = true end
        end
    end

    local anchor = self.transform:Find("Anchor")
    self:SetState(anchor:Find("Target/State"), true)
    self:SetState(anchor:Find("Message/State"), true)
    self:SetState(anchor:Find("Level/State"), isLevelOk)
    self:SetState(anchor:Find("Recruit/State"), isFeatureOk)

    local stopButton = anchor:Find("Bottom/StopButton").gameObject
    local resendButton = anchor:Find("Bottom/ResendButton").gameObject
    local chatButton = anchor:Find("Bottom/ChatButton").gameObject
    local cantChat = anchor:Find("Bottom/CantChat").gameObject

    stopButton:SetActive(self.isMyRecruit)
    resendButton:SetActive(self.isMyRecruit)
    chatButton:SetActive(not self.isMyRecruit and ((isLevelOk and isFeatureOk) or not self.recruitInfo.check))
    cantChat:SetActive(not self.isMyRecruit and (not isLevelOk or not isFeatureOk) and self.recruitInfo.check)

    UIEventListener.Get(stopButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStopButtonClick()
	end)

    UIEventListener.Get(resendButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnResendButtonClick()
	end)

    UIEventListener.Get(chatButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)
    self:UpdateLeftTime()
end

function LuaTeamRecruitDetailWnd:OnDestroy()
    LuaTeamRecruitMgr.dataForDetailWnd = {}
end

--@region UIEvent

function LuaTeamRecruitDetailWnd:OnStopButtonClick()
    Gac2Gas.CancelMyTeamRecruit()
    CUIManager.CloseUI(CLuaUIResources.TeamRecruitDetailWnd)
end

function LuaTeamRecruitDetailWnd:OnResendButtonClick()
    CUIManager.ShowUI(CLuaUIResources.TeamRecruitPostWnd)
end

function LuaTeamRecruitDetailWnd:OnChatButtonClick()
    if CServerTimeMgr.Inst.timeStamp >= self.recruitInfo.startTime + self.recruitInfo.duration then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("无法私聊，该招募已过期"))
        return
    end

    Gac2Gas.ClientLogCount(EnumClientLogCountKey.TeamRecruit_Contact, 1)
    CChatHelper.ShowFriendWnd(self.recruitInfo.id, self.recruitInfo.name)
    local target = TeamRecruit_Type.GetData(self.recruitInfo.target).Name
    local msg = g_MessageMgr:FormatMessage("TEAM_RECRUIT_CHAT_MESSAGE", target, self.recruitInfo.id, self.recruitInfo.startTime)
    CChatHelper.SendMsgWithFilterOption(EChatPanel.Friend, msg, self.recruitInfo.id, true)
    CUIManager.CloseUI(CLuaUIResources.TeamRecruitDetailWnd)
end

--@endregion UIEvent
