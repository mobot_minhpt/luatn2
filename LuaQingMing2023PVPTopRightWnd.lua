local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CScene=import "L10.Game.CScene"

LuaQingMing2023PVPTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQingMing2023PVPTopRightWnd, "rightBtn", GameObject)
RegistChildComponent(LuaQingMing2023PVPTopRightWnd, "tipNodePlayer", GameObject)
RegistChildComponent(LuaQingMing2023PVPTopRightWnd, "tipNode1", GameObject)
RegistChildComponent(LuaQingMing2023PVPTopRightWnd, "tipNode2", GameObject)
RegistChildComponent(LuaQingMing2023PVPTopRightWnd, "tipNode3", GameObject)
RegistChildComponent(LuaQingMing2023PVPTopRightWnd, "RemainTimeLabel", UILabel)

RegistClassMember(LuaQingMing2023PVPTopRightWnd, "m_PlayerIdList")
--@endregion RegistChildComponent end

function LuaQingMing2023PVPTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.rightBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)
end

function LuaQingMing2023PVPTopRightWnd:Init()
    self:QingMing2023SyncPlayRankInfo()
    self:QingMing2023SyncSubPlayInfo()
end

function LuaQingMing2023PVPTopRightWnd:QingMing2023SyncPlayRankInfo()
    self.m_PlayerIdList = {}
    -- 玩家自身
    self:UpdateRankInfo(self.tipNodePlayer, LuaQingMing2023Mgr.m_PVP_MainPlayerRank)
    -- 前三
    local infoGoList = {self.tipNode1, self.tipNode2, self.tipNode3}
    for i = 1, #infoGoList do
        infoGoList[i]:SetActive(LuaQingMing2023Mgr.m_PVP_RankInfo[i])
        if LuaQingMing2023Mgr.m_PVP_RankInfo[i] then
            self:UpdateRankInfo(infoGoList[i], LuaQingMing2023Mgr.m_PVP_RankInfo[i])
        end
    end
    self:QingMing2023SyncSubPlayInfo()
end

function LuaQingMing2023PVPTopRightWnd:UpdateRankInfo(InfoGo, data)
    if data == nil then return end
    local rankImgList = {"headinfownd_jiashang_01", "headinfownd_jiashang_02", "headinfownd_jiashang_03"}
    local rankImg = InfoGo.transform:Find("node")
    local rankLabel = InfoGo.transform:Find("Label")
    if rankImg then rankImg.gameObject:SetActive(data.Rank > 0 and data.Rank <= 3) end
    if rankLabel then rankLabel.gameObject:SetActive(data.Rank <= 0 or data.Rank > 3) end
    if data.Rank > 0 and data.Rank <= 3 then
        rankImg:GetComponent(typeof(UISprite)).spriteName = rankImgList[data.Rank]
    else
        rankLabel:GetComponent(typeof(UILabel)).text = tostring(data.Rank)
    end
    InfoGo.transform:Find("text"):GetComponent(typeof(UILabel)).text = data.PlayerName
    InfoGo.transform:Find("score"):GetComponent(typeof(UILabel)).text = data.Score
    table.insert(self.m_PlayerIdList, data.PlayerId)
end

function LuaQingMing2023PVPTopRightWnd:QingMing2023SyncSubPlayInfo()
    local infoGoList = {self.tipNodePlayer, self.tipNode1, self.tipNode2, self.tipNode3}
    for i = 1, #self.m_PlayerIdList do
        local data = LuaQingMing2023Mgr.m_PVP_CurrentLevelData[self.m_PlayerIdList[i]]
        local hpInfo = infoGoList[i].transform:Find("hp")
        local itemTbl = {}
        itemTbl[4] = infoGoList[i].transform:Find("key")
        itemTbl[6] = infoGoList[i].transform:Find("lvliu")
        itemTbl[3] = infoGoList[i].transform:Find("baoxiang")
        hpInfo.gameObject:SetActive(false)
        for i, item in pairs(itemTbl) do
            item.gameObject:SetActive(false)
        end

        if data ~= nil and LuaQingMing2023Mgr.m_PVP_CurrentLevelId ~= nil and LuaQingMing2023Mgr.m_PVP_CurrentLevelId ~= 0 then
            local item = itemTbl[LuaQingMing2023Mgr.m_PVP_CurrentLevelId] and itemTbl[LuaQingMing2023Mgr.m_PVP_CurrentLevelId] or hpInfo
            
            if LuaQingMing2023Mgr.m_PVP_CurrentLevelId == 4 then
                -- 抢钥匙
                item.gameObject:SetActive(data.Score > 0)
            elseif LuaQingMing2023Mgr.m_PVP_CurrentLevelId == 3 or LuaQingMing2023Mgr.m_PVP_CurrentLevelId == 6 then
                -- 绿柳/宝箱
                item.gameObject:SetActive(true)
                item:GetComponent(typeof(UILabel)).text = tostring(data.Score)
            else
                -- 其他小游戏血量
                item.gameObject:SetActive(true)
                item:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("血量%d%%"), data.Score)
            end
        end
    end
end

function LuaQingMing2023PVPTopRightWnd:OnHideTopAndRightTipWnd()
    self.rightBtn.transform.localEulerAngles = Vector3(0, 0, -180)
end

function LuaQingMing2023PVPTopRightWnd:OnRemainTimeUpdate()
    local mainScene = CScene.MainScene
    if mainScene then
        self.RemainTimeLabel.text = LuaGamePlayMgr:GetRemainTimeText(mainScene.ShowTime)
    end
end

--@region UIEvent
function LuaQingMing2023PVPTopRightWnd:OnRightBtnClick()
    self.rightBtn.transform.localEulerAngles = Vector3(0, 0, 0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end
--@endregion UIEvent

function LuaQingMing2023PVPTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("QingMing2023SyncPlayRankInfo", self, "QingMing2023SyncPlayRankInfo")
    g_ScriptEvent:AddListener("QingMing2023SyncSubPlayInfo", self, "QingMing2023SyncSubPlayInfo")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
end

function LuaQingMing2023PVPTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd",self,"OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("QingMing2023SyncPlayRankInfo", self, "QingMing2023SyncPlayRankInfo")
    g_ScriptEvent:RemoveListener("QingMing2023SyncSubPlayInfo", self, "QingMing2023SyncSubPlayInfo")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
end
