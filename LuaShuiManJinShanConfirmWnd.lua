local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CButton = import "L10.UI.CButton"
local Transform = import "UnityEngine.Transform"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color = import "UnityEngine.Color"
local TweenAlpha = import "TweenAlpha"
local Animator = import "UnityEngine.Animator"
local UISoundEvents = import "SoundManager+UISoundEvents"

LuaShuiManJinShanConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShuiManJinShanConfirmWnd, "Force1Bg", "Force1Bg", GameObject)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "Force2Bg", "Force2Bg", GameObject)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "TeamState", "TeamState", GameObject)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "MemberTable", "MemberTable", QnTableView)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "Timer", "Timer", UISlider)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "TimerLab", "TimerLab", UILabel)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "ForceName", "ForceName", UILabel)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "Desc", "Desc", UILabel)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "SkillTable", "SkillTable", QnTableView)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "SkillName", "SkillName", UILabel)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "SkillDesc", "SkillDesc", UILabel)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "MatchBtn", "MatchBtn", QnButton)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "ConfirmBtn", "ConfirmBtn", QnButton)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "MatchingTip", "MatchingTip", UILabel)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "ConfirmedTip", "ConfirmedTip", UILabel)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "ConfirmAnchor", "ConfirmAnchor", Transform)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "EnterAnchor", "EnterAnchor", Transform)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaShuiManJinShanConfirmWnd, "LeftChanceLab", "LeftChanceLab", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaShuiManJinShanConfirmWnd, "ForceName2")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_LeftForce")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_RightForce")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_Title1")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_Title2")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SliderFore")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_GameplayId")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_IsSolo")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_CurForce")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_IsMatching")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SelectedBuffIdx")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_ConfirmDuration")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_ForceName")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_BuffList")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_MemberList")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_MemberConfirmList")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_PassTimes")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SelectedBuffIdx")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_BlueCol")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_YellowCol")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SliderCol1")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SliderCol2")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_CurForceCol")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_RedCol")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_Tick")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_IsConfirmed")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_ExpiredTime")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_IsConfirmState")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_CloseBtn")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_Animator")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_Offset")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SkillNameBg")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_TimerFlash")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_BlueCol1")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_YellowCol1")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SliderSpr1")
RegistClassMember(LuaShuiManJinShanConfirmWnd, "m_SliderSpr2")

function LuaShuiManJinShanConfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.ForceName2     = self.ConfirmAnchor.transform:Find("Offset/Force/ForceName2"):GetComponent(typeof(UILabel))
    self.m_LeftForce    = self.transform:Find("LeftForce").gameObject
    self.m_RightForce   = self.transform:Find("RightForce").gameObject
    self.m_Title1       = self.Force1Bg.transform:Find("SoloTitle").gameObject
    self.m_Title2       = self.Force2Bg.transform:Find("SoloTitle").gameObject
    self.m_SliderFore   = self.Timer.transform:Find("Foreground"):GetComponent(typeof(UISprite))
    self.m_CloseBtn     = self.transform:Find("CloseButton").gameObject
    self.m_SkillNameBg  = self.SkillName.transform:Find("NameBg"):GetComponent(typeof(UISprite))
    self.m_TimerFlash   = self.m_SliderFore:GetComponent(typeof(TweenAlpha))
    self.m_Animator     = self.transform:GetComponent(typeof(Animator))
    self.m_Offset       = self.ConfirmAnchor.transform:Find("Offset").transform

    UIEventListener.Get(self.m_LeftForce.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if not LuaShuiManJinShanMgr.m_IsConfirmState then
            self:SelectForce(1001)
        end
	end)

    UIEventListener.Get(self.m_RightForce.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if not LuaShuiManJinShanMgr.m_IsConfirmState then
            self:SelectForce(1002)
        end
	end)

    self.MatchBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnMatchBtnClick(btn)
    end)

    self.ConfirmBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnConfirmBtnClick(btn)
    end)

    UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("SHUIMANJINSHAN_ENTERWND_TIP")
    end)

    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        if not LuaShuiManJinShanMgr.m_IsConfirmState then
            CUIManager.CloseUI(CLuaUIResources.ShuiManJinShanConfirmWnd)
            return
        end

        if not self.m_IsSolo then
            Gac2Gas.ShuiManJinShan_MemberConfirmSignUp(0)
            return
        end

        if self.m_IsMatching or self.m_IsConfirmed then
            CUIManager.CloseUI(CLuaUIResources.ShuiManJinShanConfirmWnd)
            return
        end
        
        LuaShuiManJinShanMgr.m_IsConfirmState = false
        self:Init()
    end)

    self.SkillTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_BuffList
        end,
        function(item, index)
            self:InitItem1(item, index + 1, self.m_BuffList[index + 1])
        end
    )

    self.MemberTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_MemberList
        end,
        function(item, index)
            self:InitItem2(item, self.m_MemberList[index + 1])
        end
    )

    self.SkillTable.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        if self.m_IsMatching or self.m_IsConfirmed then
            return
        end

        self:SelectSkill(row + 1)
        self:RefreshState()
    end)

    self.m_RedCol       = NGUIText.ParseColor24("FF0000", 0)
    self.m_BlueCol      = NGUIText.ParseColor24("92C0FB", 0)
    self.m_YellowCol    = NGUIText.ParseColor24("FFE5AA", 0)
    self.m_BlueCol1     = NGUIText.ParseColor24("AACEFF", 0)
    self.m_YellowCol1   = NGUIText.ParseColor24("FFC94E", 0)
    self.m_SliderCol1   = NGUIText.ParseColor24("D3E4FF", 0)
    self.m_SliderCol2   = NGUIText.ParseColor24("FFFFFF", 0)
    self.m_SliderSpr1   = "common_hpandmp_mp"
    self.m_SliderSpr2   = "common_hpandmp_max"
end

function LuaShuiManJinShanConfirmWnd:Init()
    if LuaShuiManJinShanMgr.m_IsConfirmState then
        self:InitConfirm()
    else
        self:InitEnter()
    end
end

function LuaShuiManJinShanConfirmWnd:InitEnter()
    local pass = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eShuiManJinShanPassThisWeek)
    local total = XinBaiLianDong_Setting.GetData().ShuiManJinShan_WeekTimesLimit
    local remain = total - pass
    self.LeftChanceLab.text = remain
    self.m_Animator:Play("shuimanjinshanenterwnd_show", 0, 0)
end

function LuaShuiManJinShanConfirmWnd:InitConfirm()
    self.m_GameplayId = LuaShuiManJinShanMgr.m_GameplayId
    self.m_IsSolo = LuaShuiManJinShanMgr.m_IsSolo
    self.m_CurForce = LuaShuiManJinShanMgr.m_CurForce
    self.m_ConfirmDuration = XinBaiLianDong_Setting.GetData().ShuiManJinShan_ConfirmDuration
    self.m_PassTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua["eShuiManJinShanPassTimes_" .. tostring(self.m_CurForce)])

    if self.m_IsSolo then
        self.m_IsMatching = LuaShuiManJinShanMgr.m_IsMatching
        self.m_SelectedBuffIdx = (LuaShuiManJinShanMgr.m_SoloInfo or {}).BuffIdx or 1
        SoundManager.Inst:PlayOneShot(UISoundEvents.WndOpen, Vector3(0,0,0),nil,0)
    else
        self.m_IsConfirmed = LuaShuiManJinShanMgr.m_IsConfirmed
        self.m_ExpiredTime = LuaShuiManJinShanMgr.m_GroupInfo.ExpiredTime
        self.m_MemberConfirmList = LuaShuiManJinShanMgr.m_GroupInfo.Confirm or {}
        self.m_SelectedBuffIdx = LuaShuiManJinShanMgr.m_GroupInfo.Confirm[(CClientMainPlayer.Inst or {}).Id or 0] or 1
        self.m_MemberList = LuaShuiManJinShanMgr.m_GroupInfo.Members or {}
    end

    self.TeamState:SetActive(not self.m_IsSolo)
    Extensions.SetLocalPositionY(self.m_Offset, self.m_IsSolo and 80 or 0)

    self:SwitchForce(self.m_CurForce)
    self.SkillTable:ReloadData(true, false)
    self:SelectSkill(self.m_SelectedBuffIdx)
    self:RefreshState()

    if not self.m_IsSolo then
        self.MemberTable:ReloadData(true, false)
        UnRegisterTick(self.m_Tick)
        self:OnTick()
        self.m_Tick = RegisterTick(function()
            self:OnTick()
        end, 1000)
    end
end

function LuaShuiManJinShanConfirmWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("ShuiManJinShan_MemberConfirmSignUp", self, "OnShuiManJinShan_MemberConfirmSignUp")
end

function LuaShuiManJinShanConfirmWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:RemoveListener("ShuiManJinShan_MemberConfirmSignUp", self, "OnShuiManJinShan_MemberConfirmSignUp")
end

function LuaShuiManJinShanConfirmWnd:OnDestroy()
    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end

    LuaShuiManJinShanMgr:Clear()
end

function LuaShuiManJinShanConfirmWnd:OnTick()
    local remainTime = self.m_ExpiredTime - CServerTimeMgr.Inst.timeStamp
    remainTime = remainTime >= 0 and remainTime or 0

    self.TimerLab.text = SafeStringFormat3(LocalString.GetString("%d秒"), remainTime)
    local progress = remainTime / self.m_ConfirmDuration 
    self.Timer.value = progress

    local oriCol = self.m_CurForce == 1001 and self.m_SliderCol1 or self.m_SliderCol2
    if progress <= 0.2 then
        self.TimerLab.color = self.m_RedCol
        self.m_SliderFore.color = self.m_RedCol
        self.m_TimerFlash.enabled = true
        self.m_TimerFlash.alpha = 1
    else
        self.TimerLab.color = oriCol
        self.m_SliderFore.color = Color.white
        self.m_TimerFlash.enabled = false
    end
    if remainTime == 0 then
        CUIManager.CloseUI(CLuaUIResources.ShuiManJinShanConfirmWnd)
    end
end

function LuaShuiManJinShanConfirmWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == self.m_GameplayId then
        self.m_IsMatching = success
        LuaShuiManJinShanMgr.m_IsMatching = self.m_IsMatching
        self:RefreshState()
    end
end

function LuaShuiManJinShanConfirmWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId == self.m_GameplayId then
        self.m_IsMatching = not success
        LuaShuiManJinShanMgr.m_IsMatching = self.m_IsMatching
        self:RefreshState()
    end
end

function LuaShuiManJinShanConfirmWnd:OnShuiManJinShan_MemberConfirmSignUp(memberId, buffIdx)
    if buffIdx == 0 then
        CUIManager.CloseUI(CLuaUIResources.ShuiManJinShanConfirmWnd)
        return
    end
    self.m_MemberConfirmList[memberId] = buffIdx
    self.MemberTable:ReloadData(true, false)

    if CClientMainPlayer.Inst and memberId == CClientMainPlayer.Inst.Id then
        self.m_IsConfirmed = buffIdx
        self.ConfirmBtn.gameObject:SetActive(not self.m_IsConfirmed)
        self.ConfirmedTip.gameObject:SetActive(self.m_IsConfirmed)
    end
end
--@region UIEvent

--@endregion UIEvent

function LuaShuiManJinShanConfirmWnd:SelectForce(force)
    LuaShuiManJinShanMgr.m_CurForce = force
    if LuaShuiManJinShanMgr.m_IsSolo then
        LuaShuiManJinShanMgr.m_IsConfirmState = true 
        self:Init()
    else
        Gac2Gas.ShuiManJinShan_LeaderRequestSignUp(force, LuaShuiManJinShanMgr.m_SignUpDifficulty)
    end
end


function LuaShuiManJinShanConfirmWnd:InitItem1(item, index, info)
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local selected  = item.transform:Find("Selected"):GetComponent(typeof(UITexture))
    local selected2 = item.transform:Find("Selected_Hard"):GetComponent(typeof(UITexture))
    local lock      = item.transform:Find("Lock").gameObject
    local mask      = item.transform:Find("Mask"):GetComponent(typeof(UITexture))
    local isUnlock  = self.m_PassTimes + 1 >= index 
    local isPermit  = info.permitMode == LuaShuiManJinShanMgr.m_CurDifficulty or LuaShuiManJinShanMgr.m_IsSolo
    local isSelected= index == self.m_SelectedBuffIdx

    local buffData = Buff_Buff.GetData(info.buffId)
    if buffData then
        icon:LoadMaterial(buffData.Icon)
    end

    if isUnlock and isPermit then
        mask.alpha = isSelected and 0 or 0.3
    else
        mask.alpha = 0.8
    end

    lock:SetActive(not (isUnlock and isPermit))

    if info.permitMode == 2 then
        selected.gameObject:SetActive(false)
        selected2.gameObject:SetActive(isSelected)
    else
        selected.gameObject:SetActive(isSelected)
        selected2.gameObject:SetActive(false)
    end
end

function LuaShuiManJinShanConfirmWnd:InitItem2(item, info)
    local playerIcon    = item:GetComponent(typeof(CUITexture))
    local skillIcon     = item.transform:Find("Skill"):GetComponent(typeof(CUITexture))
    local marker        = item.transform:Find("Marker").gameObject
    local name          = item.transform:Find("PlayerName"):GetComponent(typeof(UILabel))

    local portraitName = CUICommonDef.GetPortraitName(info[3], info[4], -1)
	playerIcon:LoadNPCPortrait(portraitName,false)

    name.text = string.sub(info[2], 1, 3 * 4)
    local buffIdx = self.m_MemberConfirmList[info[1]]
    marker:SetActive(buffIdx)
    skillIcon.gameObject:SetActive(buffIdx)
    if buffIdx then
        local buffData = Buff_Buff.GetData(self.m_BuffList[buffIdx].buffId)
        if buffData then
            skillIcon:LoadMaterial(buffData.Icon)
        end
    end
end

function LuaShuiManJinShanConfirmWnd:OnMatchBtnClick()
    Gac2Gas.GlobalMatch_RequestSignUpWithExtra(self.m_GameplayId, g_MessagePack.pack({Force = self.m_CurForce, BuffIdx = self.m_SelectedBuffIdx}))
end

function LuaShuiManJinShanConfirmWnd:OnConfirmBtnClick()
    Gac2Gas.ShuiManJinShan_MemberConfirmSignUp(self.m_SelectedBuffIdx)
end

function LuaShuiManJinShanConfirmWnd:OnCancelBtnClick()
    Gac2Gas.GlobalMatch_RequestCancelSignUp(self.m_GameplayId)
end

function LuaShuiManJinShanConfirmWnd:SelectSkill(buffIdx)
    self.m_SelectedBuffIdx = buffIdx
    local buffData = Buff_Buff.GetData(self.m_BuffList[self.m_SelectedBuffIdx].buffId)
    local isUnlock = self.m_PassTimes + 1 >= self.m_SelectedBuffIdx
    local isPermit = self.m_BuffList[self.m_SelectedBuffIdx].permitMode == LuaShuiManJinShanMgr.m_CurDifficulty or LuaShuiManJinShanMgr.m_IsSolo
    local isForbid
    local leftTime = self.m_SelectedBuffIdx - self.m_PassTimes - 1
    if buffData then
        self.SkillName.text = buffData.Name
        if isUnlock and isPermit then
            self.SkillDesc.text = CUICommonDef.TranslateToNGUIText(buffData.Display)
        elseif not isPermit then 
            if LuaShuiManJinShanMgr.m_CurDifficulty == 1 then
                self.SkillDesc.text = g_MessageMgr:FormatMessage("SHUIMANJINSHAN_CONFIRMWND_MODEFORBID1")
            else
                self.SkillDesc.text = g_MessageMgr:FormatMessage("SHUIMANJINSHAN_CONFIRMWND_MODEFORBID2")
            end
        elseif not isUnlock then
            self.SkillDesc.text = SafeStringFormat3(LocalString.GetString("再通关%d次以解锁此支援效果"), leftTime)
        end
    end
end

function LuaShuiManJinShanConfirmWnd:SwitchForce(force)
    self.m_CurForce = force
    local data = XinBaiLianDong_ShuiManJinShan.GetData(force)
    if data then
        self.m_ForceName = data.Name
        self.m_BuffList = {}
        for i = 0, data.BuffList.Length - 1 do
            table.insert(self.m_BuffList, {buffId = data.BuffList[i], permitMode = data.Difficulty[i]})
        end
    end
    -- self.Force1Bg:SetActive(force == 1001)
    -- self.Force2Bg:SetActive(force == 1002)
    self.m_CurForceCol = force == 1001 and self.m_BlueCol or self.m_YellowCol
    self.ForceName.gameObject:SetActive(force == 1001)
    self.ForceName2.gameObject:SetActive(force == 1002)
    self.ForceName.text = self.m_ForceName
    self.ForceName2.text = self.m_ForceName
    self.SkillName.color = self.m_CurForceCol
    self.m_SkillNameBg.color = force == 1001 and self.m_BlueCol1 or self.m_YellowCol1

    self.m_SliderFore.spriteName = force == 1001 and self.m_SliderSpr1 or self.m_SliderSpr2
    self.m_SliderFore.color = force == 1001 and self.m_SliderCol1 or self.m_SliderCol2
    -- Extensions.SetLocalPositionX(self.ConfirmAnchor, force == 1002 and -848 or 0)

    self.m_Animator:Play(force == 1002 and "shuimanjinshanenterwnd_fahai" or "shuimanjinshanenterwnd_baishexiaoqing", 0, 0)
end

function LuaShuiManJinShanConfirmWnd:RefreshState()
    if self.m_IsSolo then
        self.CancelBtn.gameObject:SetActive(self.m_IsMatching)
        self.MatchBtn.gameObject:SetActive(not self.m_IsMatching)
        self.MatchingTip.gameObject:SetActive(self.m_IsMatching)
        self.ConfirmBtn.gameObject:SetActive(false)
        self.ConfirmedTip.gameObject:SetActive(false)
    else 
        self.CancelBtn.gameObject:SetActive(false)
        self.MatchBtn.gameObject:SetActive(false)
        self.MatchingTip.gameObject:SetActive(false)
        self.ConfirmBtn.gameObject:SetActive(not self.m_IsConfirmed)
        self.ConfirmedTip.gameObject:SetActive(self.m_IsConfirmed)
    end

    local isUnlock = self.m_PassTimes + 1 >= self.m_SelectedBuffIdx
    local isPermit = self.m_BuffList[self.m_SelectedBuffIdx].permitMode == LuaShuiManJinShanMgr.m_CurDifficulty or LuaShuiManJinShanMgr.m_IsSolo
    self.MatchBtn.Enabled = isUnlock and isPermit
    self.ConfirmBtn.Enabled = isUnlock and isPermit

    self.SkillTable:ReloadData(true, false)
end