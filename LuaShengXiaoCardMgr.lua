local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
if not _G["LuaShengXiaoCardMgr"] then
    LuaShengXiaoCardMgr = {}
end
LuaShengXiaoCardMgr.PlayID = 51102344

EnumShengXiaoCardState = {
	Idle = 101,
	Prepare = 102,

	Start = 111,
	Reward = 112,
	End = 113,

	RoundStart = 121,
	RoundEnd = 122,

	WaitShow = 131,
	WaitGuess = 132,
	PassShow = 133,
	PassGuess = 134,
	WaitEnd = 135,
}
EnumShengXiaoCardRole = {
	None = 0,	-- 无角色
	Show = 1,	-- 出牌者
	Guess = 2,	-- 猜牌者
	Watch = 3,	-- 观牌者
	Pass = 4,	-- 过牌者
}

LuaShengXiaoCardMgr.Sounds = {
    rosehit = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Rose_Hit",
    rosethrow = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Rose_Throw",
    shoeshit = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Shoes_Hit",
    shoesthrow = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Shoes_Throw",
    snowhit = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Snow_Hit",
    snowthrow = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Snow_Throw",
    right = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Right",
    wrong = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Wrong",
    danger = "event:/L10/L10_UI/ShengXiaoDuiJue/UI_Danger",
    countdown = "event:/L10/L10_UI/2022ChunJie/UI_ChunJie_CountDown",
}

function LuaShengXiaoCardMgr.ShowTipWnd()
    local imagePaths = {
        "UI/Texture/NonTransparent/Material/shengxiaocard_yindao_02.mat",
        "UI/Texture/NonTransparent/Material/shengxiaocard_yindao_01.mat",
        "UI/Texture/NonTransparent/Material/shengxiaocard_yindao_03.mat",
        "UI/Texture/NonTransparent/Material/shengxiaocard_yindao_05.mat",
        "UI/Texture/NonTransparent/Material/shengxiaocard_yindao_04.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("ShengXiaoCard_Rule_1"),
        g_MessageMgr:FormatMessage("ShengXiaoCard_Rule_2"),
        g_MessageMgr:FormatMessage("ShengXiaoCard_Rule_3"),
        g_MessageMgr:FormatMessage("ShengXiaoCard_Rule_4"),
        g_MessageMgr:FormatMessage("ShengXiaoCard_Rule_5"),
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

function LuaShengXiaoCardMgr.TryTriggerGuide(id)
    if id==1 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide1) then
            RegisterTickOnce(function()
                CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(1).BatID)
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide1)
            end,2000)
        end
    elseif id==2 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide2) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide2)
            CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(2).BatID)
        end
    elseif id==3 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide3) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide3)
            CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(3).BatID)
        end
    elseif id==4 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide4) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide4)
            CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(4).BatID)
        end
    elseif id==5 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide5) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide5)
            CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(5).BatID)
        end
    elseif id==6 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide6) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide6)
            CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(6).BatID)
        end
    elseif id==7 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide7) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide7)
            CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(7).BatID)
        end
    elseif id==8 then
        if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide8) then
            COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.ShengXiaoCard_Guide8)
            RegisterTickOnce(function()
                CLuaBatDialogWnd.ShowWnd(ShengXiaoCard_Guide.GetData(8).BatID)
            end,2000)
        end
    end
end

function LuaShengXiaoCardMgr.SwitchViewChairId(chairId)--3
	local chairCount=3
	local dt=chairId-LuaShengXiaoCardMgr.m_PlayerIndex
	if dt<0 then 
		dt=dt+chairCount 
	end
	local ret= dt%chairCount
	return ret+1
end
function LuaShengXiaoCardMgr.SwitchChairId(viewChairId)--3
	local chairCount=3
	local dt=viewChairId-LuaShengXiaoCardMgr.SwitchViewChairId(CLuaDouDiZhuMgr.m_PlayerIndex)
	if dt<0 then 
		dt=dt+chairCount 
	end
	local ret= dt%chairCount
	return ret+1
end
LuaShengXiaoCardMgr.m_State = 0
-- LuaShengXiaoCardMgr.m_PlayerGameInfos = nil
LuaShengXiaoCardMgr.m_PlayerIndex = 0
--Actor Owner DisplayId Card
LuaShengXiaoCardMgr.m_ShowInfo = nil
LuaShengXiaoCardMgr.m_GuessInfo = nil
LuaShengXiaoCardMgr.m_PlayerProfiles = nil
LuaShengXiaoCardMgr.m_PlayedCards = nil
--当前选中的牌
LuaShengXiaoCardMgr.m_SelectedCardId = nil

LuaShengXiaoCardMgr.m_Round = 0
LuaShengXiaoCardMgr.m_SubRound = 0
LuaShengXiaoCardMgr.m_LoserId = 0
LuaShengXiaoCardMgr.m_ExpiredTime = 0

LuaShengXiaoCardMgr.m_GuessIndex = 0
LuaShengXiaoCardMgr.m_Context = 0
LuaShengXiaoCardMgr.m_IsReportRank = false


--ShengXiaoCard_SyncPlayState
function LuaShengXiaoCardMgr.SyncPlayState(state, PlayerInfos_U, Round, SubRound, LoserId, ShowInfo_U, GuessInfo_U, PlayerIn2Profile_U, context, PreparePlayers_U,expiredTime,isReportRank)
    -- print("LuaShengXiaoCardMgr.SyncPlayState",state,  Round, SubRound, LoserId,context,isReportRank)
    LuaShengXiaoCardMgr.m_GuessIndex = 0

    LuaShengXiaoCardMgr.m_Context = context
    LuaShengXiaoCardMgr.m_IsReportRank = isReportRank

    LuaShengXiaoCardMgr.m_Round = Round
    LuaShengXiaoCardMgr.m_SubRound = SubRound
    LuaShengXiaoCardMgr.m_LoserId = LoserId
    LuaShengXiaoCardMgr.m_ExpiredTime = expiredTime
    LuaShengXiaoCardMgr.m_PlayedCards = { [3] = true}--肯定有老虎

    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    LuaShengXiaoCardMgr.m_PlayerProfiles = {}
    if PlayerIn2Profile_U and PlayerIn2Profile_U.Length>0 then
        local playerInGame = MsgPackImpl.unpack(PlayerIn2Profile_U)
        if playerInGame then
            CommonDefs.DictIterate(playerInGame,DelegateFactory.Action_object_object(function (___key, ___value) 
                local profile = {}
                CommonDefs.DictIterate(___value,DelegateFactory.Action_object_object(function (k, v) 
                    --Index Class Name Gender
                    profile[tostring(k)] = v
                end))
                profile.Id = tonumber(___key)
                if profile.Id==myId then
                    LuaShengXiaoCardMgr.m_PlayerIndex = profile.Index
                end
                LuaShengXiaoCardMgr.m_PlayerProfiles[profile.Id] = profile
            end))
        end
    end

    if PlayerInfos_U and PlayerInfos_U.Length > 0 then
        local playerInfo = MsgPackImpl.unpack(PlayerInfos_U)
        if playerInfo then
            if CommonDefs.IsDic(playerInfo) then
                CommonDefs.DictIterate(playerInfo,DelegateFactory.Action_object_object(function (___key, ___value) 
                    local gameInfo = {}
                    local playerId = tonumber(___key)
                    CommonDefs.DictIterate(___value,DelegateFactory.Action_object_object(function (k, v) 
                        --Role Seals Hands Guaji Escape
                        -- print(k,v)
                        if k=="Hands" then
                            gameInfo.Hands = {}
                            CommonDefs.DictIterate(v,DelegateFactory.Action_object_object(function (k2, v2) 
                                -- print("handcard2",k2,v2)
                                local cardInfo = {}
                                CommonDefs.DictIterate(v2,DelegateFactory.Action_object_object(function (k3, v3) 
                                    -- print("handcard3",k3,v3)
                                    --Id ShengXiao
                                    cardInfo[k3] = v3
                                    if k3=="ShengXiao" then
                                        if playerId==myId then
                                            LuaShengXiaoCardMgr.m_PlayedCards[tonumber(v3)] = true
                                        end
                                    end
                                end))
                                table.insert( gameInfo.Hands,cardInfo )
                            end))
                            table.sort( gameInfo.Hands,function(a,b)
                                if a.ShengXiao>b.ShengXiao then
                                    return false
                                elseif a.ShengXiao<b.ShengXiao then
                                    return true
                                else
                                    return a.Id<b.Id
                                end
                            end )
                        elseif k=="Seals" then
                            if CommonDefs.IsDic(v) then
                                gameInfo.Seals = {}
                                CommonDefs.DictIterate(v,DelegateFactory.Action_object_object(function (k2, v2) 
                                    local cardId = tonumber(k2)
                                    gameInfo.Seals[cardId] = tonumber(v2)
                                    LuaShengXiaoCardMgr.m_PlayedCards[cardId] = true
                                end))
                            else
                                gameInfo.Seals = {}
                                for i=1,v.Count do
                                    -- print("list seal",i,v[i-1])
                                    gameInfo.Seals[i] = tonumber(v[i-1])
                                end
                            end
                        else
                            gameInfo[k] = v
                        end
                    end))
                    if LuaShengXiaoCardMgr.m_PlayerProfiles[playerId] then
                        LuaShengXiaoCardMgr.m_PlayerProfiles[playerId].GameInfo = gameInfo
                    end
                end))
            end
        end
    end
    LuaShengXiaoCardMgr.m_ShowInfo = nil
    if ShowInfo_U and ShowInfo_U.Length > 0 then
        LuaShengXiaoCardMgr.m_ShowInfo = {}
        local showInfo = MsgPackImpl.unpack(ShowInfo_U)
        if showInfo then
            CommonDefs.DictIterate(showInfo,DelegateFactory.Action_object_object(function (___key, ___value) 
                --Actor Owner DisplayId Card
                if ___key=="Card" then
                    local card = {}
                    CommonDefs.DictIterate(___value,DelegateFactory.Action_object_object(function (k, v) 
                        --Id ShengXiao
                        card[k] = v
                    end))
                    LuaShengXiaoCardMgr.m_ShowInfo.Card = card

                else
                    LuaShengXiaoCardMgr.m_ShowInfo[___key] = ___value
                end
            end))
            LuaShengXiaoCardMgr.m_ShowInfo.RealCardName = ""
            LuaShengXiaoCardMgr.m_ShowInfo.DisplayCardName = ""
            if LuaShengXiaoCardMgr.m_ShowInfo.Card.ShengXiao then
                local design = ShengXiaoCard_ShengXiao.GetData(LuaShengXiaoCardMgr.m_ShowInfo.Card.ShengXiao)
                if design then
                    LuaShengXiaoCardMgr.m_ShowInfo.RealCardName = design.Name
                end
            end
            if LuaShengXiaoCardMgr.m_ShowInfo.DisplayId then
                local design = ShengXiaoCard_ShengXiao.GetData(LuaShengXiaoCardMgr.m_ShowInfo.DisplayId)
                if design then
                    LuaShengXiaoCardMgr.m_ShowInfo.DisplayCardName = design.Name
                end
            end
        end
    end

    LuaShengXiaoCardMgr.m_GuessInfo = nil
    if GuessInfo_U and GuessInfo_U.Length>0 then
        LuaShengXiaoCardMgr.m_GuessInfo = {}
        local guessInfo = MsgPackImpl.unpack(GuessInfo_U)
        if guessInfo then
            CommonDefs.DictIterate(guessInfo,DelegateFactory.Action_object_object(function (___key, ___value) 
                -- print("guess",___key,___value)
                --YesOrNo Actor
                LuaShengXiaoCardMgr.m_GuessInfo[___key] = ___value
            end))
        end
    end



    if PreparePlayers_U and PreparePlayers_U.Length>0 then
        local dic = MsgPackImpl.unpack(PreparePlayers_U)
        if dic then
            if CommonDefs.IsDic(dic) then
                CommonDefs.DictIterate(dic,DelegateFactory.Action_object_object(function (___key, ___value) 
                    local id = tonumber(___key)
                    if LuaShengXiaoCardMgr.m_PlayerProfiles[id] then
                        LuaShengXiaoCardMgr.m_PlayerProfiles[id].Prepared = ___value
                    end
                end))
            end
        end
    end

    LuaShengXiaoCardMgr.m_State = state

    for k,v in pairs(LuaShengXiaoCardMgr.m_PlayerProfiles) do
        if v.GameInfo then
            if v.GameInfo.Role == EnumShengXiaoCardRole.Guess then
                LuaShengXiaoCardMgr.m_GuessIndex = v.Index--猜牌者
                break
            end
        end
    end

    if not CUIManager.IsLoaded(CLuaUIResources.ShengXiaoCardWnd) then
		CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardWnd)
    else
        g_ScriptEvent:BroadcastInLua("ShengXiaoCard_SyncPlayState",state,Round, SubRound, LoserId)
	end
    
    if state ==EnumShengXiaoCardState.Reward then
        LuaShengXiaoCardResultWnd.s_PlayerInfos = LuaShengXiaoCardMgr.m_PlayerProfiles
        RegisterTickOnce(function()
		    CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardResultWnd)
        end,3000)
    end
end