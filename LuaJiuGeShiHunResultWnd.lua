local AlignType	= import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
LuaJiuGeShiHunResultWnd = class()

RegistChildComponent(LuaJiuGeShiHunResultWnd, "m_ItemTexture","ItemTexture", CUITexture)
RegistChildComponent(LuaJiuGeShiHunResultWnd, "m_Item","Item", GameObject)
RegistChildComponent(LuaJiuGeShiHunResultWnd, "m_Word1","Word1", GameObject)
RegistChildComponent(LuaJiuGeShiHunResultWnd, "m_Word2","Word2", GameObject)
RegistChildComponent(LuaJiuGeShiHunResultWnd, "m_Word3","Word3", GameObject)
RegistChildComponent(LuaJiuGeShiHunResultWnd, "m_Bg","Bg", GameObject)
RegistChildComponent(LuaJiuGeShiHunResultWnd, "m_LoseTexture","LoseTexture", GameObject)

function LuaJiuGeShiHunResultWnd:Init()
    local item = Item_Item.GetData(DuanWu2020Mgr.m_rewardItemId)
    if item and DuanWu2020Mgr.m_JiuGeShiHunResult then
        self.m_ItemTexture:LoadMaterial(item.Icon)
        UIEventListener.Get(self.m_Item).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(DuanWu2020Mgr.m_rewardItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    self.m_LoseTexture:SetActive(not DuanWu2020Mgr.m_JiuGeShiHunResult)
    self.m_Bg:SetActive(DuanWu2020Mgr.m_JiuGeShiHunResult)
    self.m_Item:SetActive(DuanWu2020Mgr.m_JiuGeShiHunResult)
    self.m_Word1:SetActive(DuanWu2020Mgr.m_JiuGeShiHunResult and (DuanWu2020Mgr.m_hpPercent > 70))
    self.m_Word2:SetActive(DuanWu2020Mgr.m_JiuGeShiHunResult and (DuanWu2020Mgr.m_hpPercent >= 30) and (DuanWu2020Mgr.m_hpPercent <= 70))
    self.m_Word3:SetActive(DuanWu2020Mgr.m_JiuGeShiHunResult and (DuanWu2020Mgr.m_hpPercent < 30))
end