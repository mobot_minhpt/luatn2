local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Extensions = import "Extensions"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"
local UIScrollView = import "UIScrollView"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"

CLuaGemGroupInfoWnd = class()
RegistClassMember(CLuaGemGroupInfoWnd,"m_Wnd")

RegistClassMember(CLuaGemGroupInfoWnd,"scrollView")
RegistClassMember(CLuaGemGroupInfoWnd,"table")
RegistClassMember(CLuaGemGroupInfoWnd,"levelTemplate")
RegistClassMember(CLuaGemGroupInfoWnd,"levelList")

RegistClassMember(CLuaGemGroupInfoWnd,"detailView")

CLuaGemGroupInfoWnd.GemGroupId = 0
function CLuaGemGroupInfoWnd:Awake()
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))


    local script = self.transform:Find("DetailView"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.detailView = script.m_LuaSelf

    local transform = self.transform:Find("MasterView")
    self.scrollView = transform:Find("Scrollview"):GetComponent(typeof(UIScrollView))
    self.table = transform:Find("Scrollview/Table"):GetComponent(typeof(UITable))
    self.levelTemplate = transform:Find("Scrollview/LevelTemplate").gameObject
    self.levelList = {}
end

function CLuaGemGroupInfoWnd:Init()
    self:InitMaterView()
end

function CLuaGemGroupInfoWnd:InitMaterView( )

    Extensions.RemoveAllChildren(self.table.transform)
    self.levelTemplate:SetActive(false)
    self.levelList = {}
    for i = 0, 9 do
        local instance = NGUITools.AddChild(self.table.gameObject, self.levelTemplate)
        instance:SetActive(true)
        CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UILabel)).text = System.String.Format(LocalString.GetString("{0}级"), i + 1)
        table.insert( self.levelList,instance )
        UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnLevelClicked(go)
        end)
        CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnSelectableButton)):SetSelected(false, false)
    end
    self.table:Reposition()
    self.scrollView:ResetPosition()
    self:OnLevelClicked(self.levelList[0+1])
end
function CLuaGemGroupInfoWnd:OnLevelClicked( go) 
    for i,v in ipairs(self.levelList) do
        v:GetComponent(typeof(QnSelectableButton)):SetSelected(go == v, false)
        if go==v then
            self.detailView:Init(CLuaGemGroupInfoWnd.GemGroupId + i-1)
        end
    end
end

function CLuaGemGroupInfoWnd:Update()
    if self.m_Wnd then
        self.m_Wnd:ClickThroughToClose()
    end
end



