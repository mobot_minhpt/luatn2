local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Vector3 = import "UnityEngine.Vector3"
local UIRoot = import "UIRoot"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaChildrenDayPlayOperateWnd = class()
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"templateNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"detailNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"pos1", GameObject)
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"pos2", GameObject)
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"pos3", GameObject)
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"pos4", GameObject)
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"chatBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayOperateWnd,"anchor", GameObject)

RegistClassMember(LuaChildrenDayPlayOperateWnd, "saveSignNode")
RegistClassMember(LuaChildrenDayPlayOperateWnd, "skillNodeArray")

function LuaChildrenDayPlayOperateWnd:Close()
	--CUIManager.CloseUI(CLuaUIResources.)
end

function LuaChildrenDayPlayOperateWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateChildrenDayPlaySkillInfo", self, "UpdateSkillInfo")

	if UIRoot.EnableIPhoneXAdaptation then
		local w = CommonDefs.GetComponent_Component_Type(self.anchor.transform, typeof(UIWidget))
		w.bottomAnchor.absolute = 59 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
		w.topAnchor.absolute = 87 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
	end
end

function LuaChildrenDayPlayOperateWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateChildrenDayPlaySkillInfo", self, "UpdateSkillInfo")
end

function LuaChildrenDayPlayOperateWnd:UpdateSkillMove()
	if LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_operation == 1 then
		local moveTime = 1
		LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_operation = 0
		if self.skillNodeArray then
			self.skillNodeArray[LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_pos]:SetActive(false)
		end
		local screenPos = CMainCamera.Main:WorldToScreenPoint(Vector3(LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_x,CClientMainPlayer.Inst.RO.Position.y,LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_y))
		screenPos.z = 0

		local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
		CCommonUIFxWnd.Instance:PlayLineAni(nguiWorldPos,self.skillNodeArray[LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_pos].transform.position,moveTime,DelegateFactory.Action(function (p)
			if self.skillNodeArray then
				self.skillNodeArray[LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_pos]:SetActive(true)
			end
		end))
	end
end

function LuaChildrenDayPlayOperateWnd:UpdateSkillInfo()
	self:ClearShowInfo()

	if not LuaChildrenDay2019Mgr.PengPengCheSkillInfo or not LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_list then
		return
	end

	self:UpdateSkillMove()

	for i,v in ipairs(self.skillNodeArray) do
	 local skillId = LuaChildrenDay2019Mgr.PengPengCheSkillInfo.skill_list[i]
	 self:SetNodeInfo(self.skillNodeArray[i],skillId,i)
	end
end

function LuaChildrenDayPlayOperateWnd:ClearShowInfo()
	self.detailNode:SetActive(false)
	if self.saveSignNode ~= nil then
		self.saveSignNode:SetActive(false)
		self.saveSignNode = nil
	end
	if self.skillNodeArray then
		for i,v in pairs(self.skillNodeArray) do
			v:SetActive(true)
		end
	end
end
function LuaChildrenDayPlayOperateWnd:SetDetailInfo(skill,pos,skillId)
    local detailText = CommonDefs.GetComponent_Component_Type(self.detailNode.transform:Find("Label"), typeof(UILabel))
    local removeBtn = self.detailNode.transform:Find("removeBtn").gameObject
    local learnBtn = self.detailNode.transform:Find("learnBtn").gameObject

    detailText.text = skill.Display

    UIEventListener.Get(removeBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        Gac2Gas.RequestRemovePengPengCheSkillItem(pos, skillId, false)
    end)
    UIEventListener.Get(learnBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        Gac2Gas.RequestRemovePengPengCheSkillItem(pos, skillId, true)
    end)
end

function LuaChildrenDayPlayOperateWnd:OpenDetailPanel(sign,skill,pos,skillId)
    if sign.activeSelf then
        sign:SetActive(false)
        self.saveSignNode = nil
        self.detailNode:SetActive(false)
    else
        if self.saveSignNode then
            self.saveSignNode:SetActive(false)
        end

        self.saveSignNode = sign
        sign:SetActive(true)
        self.detailNode:SetActive(true)
        self:SetDetailInfo(skill, pos, skillId)
    end
end

function LuaChildrenDayPlayOperateWnd:SetNodeInfo(go,skillId,pos)
	local iconTexture = CommonDefs.GetComponent_Component_Type(go.transform:Find("SkillIcon"), typeof(CUITexture))
	local skillLabel = CommonDefs.GetComponent_Component_Type(go.transform:Find("name"), typeof(UILabel))
	local selectSprite = go.transform:Find("SelectedSprite").gameObject

	iconTexture.mainTexture = nil
	iconTexture.material = nil
	skillLabel.text = ""

	if skillId > 0 then
		local skill = Skill_AllSkills.GetData(skillId)
		if skill ~= nil then
			iconTexture:LoadSkillIcon(skill.SkillIcon)
			skillLabel.text = skill.Name
		end
		--go.GetComponent<BoxCollider>().enabled = true;
		UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (p)
			self:OpenDetailPanel(selectSprite, skill, pos, skillId)
		end)
	else
		UIEventListener.Get(go).onClick = nil
		--go.GetComponent<BoxCollider>().enabled = false;
	end
end

function LuaChildrenDayPlayOperateWnd:Update()
	if Input.GetMouseButtonDown(0) then
		if not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and not CUICommonDef.IsChild) then
		end
	end
	if Input.GetMouseButtonDown(0) then
		if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
		not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
		CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
			self:ClearShowInfo()
		end
	end
end

function LuaChildrenDayPlayOperateWnd:Init()
	self.templateNode:SetActive(false)
	self.skillNodeArray = {self.pos1,self.pos2,self.pos3,self.pos4}

	UIEventListener.Get(self.chatBtn).onClick = DelegateFactory.VoidDelegate(function (p)
		CSocialWndMgr.ShowChatWnd()
	end)
end

return LuaChildrenDayPlayOperateWnd
