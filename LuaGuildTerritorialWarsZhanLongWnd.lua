local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType	= import "L10.UI.CItemInfoMgr+AlignType"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"

LuaGuildTerritorialWarsZhanLongWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsZhanLongWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsZhanLongWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsZhanLongWnd, "ReceiveAwardButton", "ReceiveAwardButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsZhanLongWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaGuildTerritorialWarsZhanLongWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsZhanLongWnd,"m_TemplateList")
RegistClassMember(LuaGuildTerritorialWarsZhanLongWnd,"m_TaskIdList")
RegistClassMember(LuaGuildTerritorialWarsZhanLongWnd,"m_TaskId2State")
RegistClassMember(LuaGuildTerritorialWarsZhanLongWnd,"m_ShowFrontNum")

function LuaGuildTerritorialWarsZhanLongWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.ReceiveAwardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReceiveAwardButtonClick()
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsZhanLongWnd:Init()
	self.Template.gameObject:SetActive(false)
	self.TopLabel.text = ""
	Gac2Gas.RequestGTWZhanLongTaskInfo()
end

function LuaGuildTerritorialWarsZhanLongWnd:InitTemplate(template, idx, taskId)
	local theBack = template.transform:Find("TheBack"):GetComponent(typeof(UIPanel))
	local theFront = template.transform:Find("TheFront")
	local diDui = theFront.transform:Find("DiDui_Bg")
	local taskNameLabel = theFront.transform:Find("TaskNameLabel"):GetComponent(typeof(UILabel))
	local taskDescriptionLabel = theFront.transform:Find("xinxi/TaskDescriptionLabel"):GetComponent(typeof(UILabel))
	local finishedLabel = theFront.transform:Find("xinxi/FinishedLabel")
	local ongoingLabel = theFront.transform:Find("xinxi/OngoingLabel")
	local itemTable = theFront.transform:Find("xinxi/ItemTable"):GetComponent(typeof(UITable))
	local itemTemplate = theFront.transform:Find("xinxi/ItemTemplate")

	local designData = GuildTerritoryWar_TDQJ_ZhanLongTask.GetData(taskId)
	local isfinished = (taskId ~= 0) and (self.m_TaskId2State[taskId] == 1)
	local isDiDui = designData and designData.HasEnemy > 0
	print(taskId,self.m_TaskId2State[taskId])
	local showFront = self.m_TaskId2State[taskId] ~= nil

	theBack.gameObject:SetActive(not showFront) 
	if showFront then
		print(template.transform:GetComponent(typeof(Animation)))
		template.transform:GetComponent(typeof(Animation)):Play()
	end
	theFront.gameObject:SetActive(showFront) 
	theBack.alpha = self.m_ShowFrontNum == 3 and 0.3 or 1
	diDui.gameObject:SetActive(isDiDui)
	taskNameLabel.text = designData and designData.Name or ""
	taskDescriptionLabel.text = designData and designData.Description or ""
	finishedLabel.gameObject:SetActive(isfinished)
	ongoingLabel.gameObject:SetActive(not isfinished)
	itemTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(itemTable.transform)

	if designData then
		for i = 0, designData.Reward.Length - 1 do
			local itemId =  designData.Reward[i][0]
			local itemData = Item_Item.GetData(itemId)
			local obj = NGUITools.AddChild(itemTable.gameObject, itemTemplate.gameObject)
			obj:SetActive(true)
			local iconTexture = obj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
			iconTexture:LoadMaterial(itemData.Icon)
			local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
			numLabel.text = designData.Reward[i][1]
			UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end)
		end
	end
	itemTable:Reposition()

	UIEventListener.Get(theBack.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCardBackClick(template, idx, taskId)
	end)
	UIEventListener.Get(theFront.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCardFrontClick(template, designData.ActivityId)
	end)
end

function LuaGuildTerritorialWarsZhanLongWnd:OnEnable()
	g_ScriptEvent:AddListener("SendGTWZhanLongTaskInfo", self, "OnSendGTWZhanLongTaskInfo")
end

function LuaGuildTerritorialWarsZhanLongWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendGTWZhanLongTaskInfo", self, "OnSendGTWZhanLongTaskInfo")
end

function LuaGuildTerritorialWarsZhanLongWnd:OnSendGTWZhanLongTaskInfo(taskInfo)
	local finishedTaskNum = GuildTerritoryWar_RelatedPlaySetting.GetData().DailyZhanLongTaskCountLimit - LuaGuildTerritorialWarsMgr.m_ZhanLongTaskInfo.todayTaskCount 
	self.TopLabel.text = SafeStringFormat3(LocalString.GetString("今日剩余完成任务次数%s%d/3"),finishedTaskNum == 0 and "[FF0000]" or "[FFFFFF]",finishedTaskNum )
	if self.m_TemplateList and self.m_TaskId2State then
		self.m_ShowFrontNum = 0
		for idx, info in pairs(LuaGuildTerritorialWarsMgr.m_ZhanLongTaskInfo.taskInfo) do
			local taskId = info.taskId
			local state = info.state -- 1 已完成, 2 正在进行
			self.m_TaskIdList[idx] = taskId
			print(idx, taskId,state)
			if state == 1 then
				self.m_ShowFrontNum = self.m_ShowFrontNum + 1
			end
			if self.m_TaskId2State[taskId] ~= state then
				local obj = self.m_TemplateList[idx]
				self.m_TaskId2State[taskId] = state
				self:InitTemplate(obj, idx, self.m_TaskIdList[idx] )
			end
		end
		return
	end
	self.m_ShowFrontNum = 0
	Extensions.RemoveAllChildren(self.Grid.transform)
	self.m_TemplateList = {}
	self.m_TaskIdList = {0,0,0,0,0} 
	self.m_TaskId2State = {}
	for idx, info in pairs(LuaGuildTerritorialWarsMgr.m_ZhanLongTaskInfo.taskInfo) do
        local taskId = info.taskId
        local state = info.state -- 1 已完成, 2 正在进行
        self.m_TaskIdList[idx] = taskId
		if state == 1 then
			self.m_ShowFrontNum = self.m_ShowFrontNum + 1
		end
		self.m_TaskId2State[taskId] = state
    end
	for i = 1,5 do
		local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
		self:InitTemplate(obj, i, self.m_TaskIdList[i] )
		obj:SetActive(true)
		table.insert(self.m_TemplateList, obj)
	end
	self.Grid:Reposition()
end
--@region UIEvent

function LuaGuildTerritorialWarsZhanLongWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("GuildTerritorialWarsZhanLongWnd_ReadMe")
end

function LuaGuildTerritorialWarsZhanLongWnd:OnReceiveAwardButtonClick()
	Gac2Gas.AcceptGTWZhanLongTask(0, true)
end

function LuaGuildTerritorialWarsZhanLongWnd:OnCardBackClick(template, idx, taskId)
	-- if self.m_ShowFrontNum >= 3 then
	-- 	return
	-- end
	-- self.m_TaskId2State[taskId] = 2
	-- self.m_ShowFrontNum = self.m_ShowFrontNum + 1
	-- for i ,template in pairs(self.m_TemplateList) do
	-- 	local _taskId = self.m_TaskIdList[i]
	-- 	self:InitTemplate(template,_taskId)
	-- end
	Gac2Gas.AcceptGTWZhanLongTask(idx, false)
end

function LuaGuildTerritorialWarsZhanLongWnd:OnCardFrontClick(obj, activityId)
	CLuaScheduleMgr:ShowScheduleInfo(activityId)
end
--@endregion UIEvent

