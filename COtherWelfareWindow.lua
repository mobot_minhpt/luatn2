local CommonDefs = import "L10.Game.CommonDefs"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CJiFenWebMgr = import "L10.Game.CJiFenWebMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

LuaOtherWelfareWnd = class()

RegistClassMember(LuaOtherWelfareWnd, "m_ScrollView")
RegistClassMember(LuaOtherWelfareWnd, "m_Table")
RegistClassMember(LuaOtherWelfareWnd, "m_BonusTemplate")

RegistClassMember(LuaOtherWelfareWnd, "m_Datas")

function LuaOtherWelfareWnd:Awake()
    self.m_ScrollView = self.transform:Find("BonusScrollview"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("BonusScrollview/Table"):GetComponent(typeof(UITable))
    self.m_BonusTemplate = self.transform:Find("BonusScrollview/BonusTemplate").gameObject
    self.m_Datas = nil
    self.m_BonusTemplate:SetActive(false)
end

function LuaOtherWelfareWnd:InitContent()

    local n = self.m_Table.transform.childCount
    for i=0,n-1 do
        self.m_Table.transform:GetChild(i).gameObject:SetActive(false)
    end
    self:LoadData()
    
    local index = 0
    for __, data in pairs(self.m_Datas) do
        local go = nil
        if index<n then
            go = self.m_Table.transform:GetChild(index).gameObject
        else
            go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_BonusTemplate)
        end
        index = index+1
        go:SetActive(true)
        go:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitContent(data.Key)
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaOtherWelfareWnd:LoadData()
    self.m_Datas = {}
    KaiFuActivity_Others.Foreach(function (key, data)
        if LuaHuiLiuMgr.CheckNewHuiLiuOpen() and key == 'ZhaoHui' then
        elseif not LuaHuiLiuMgr.CheckNewHuiLiuOpen() and key == 'ZhaoHuiNew' then
        else
          local show = self:CheckValid(key)
          if show then
              table.insert(self.m_Datas, {Key = key, Index = data.Index})
          end
        end
    end)
    table.sort(self.m_Datas, function(data1, data2) --策划表需保证Index不相等
        return data1.Index<data2.Index 
    end)
end

function LuaOtherWelfareWnd:CheckValid(key)
    if CommonDefs.IS_HMT_CLIENT then
        if key == 'GiftExchange' then
            return CSwitchMgr.EnableGiftExchange
        elseif key == "Invitation" then
            return true
        elseif key == "ZhaoHuiNew" then
            return true
        elseif key == "BindPhone" and not CommonDefs.Is_PC_PLATFORM() then
            return true
        else
            return false
        end
    else
        if key == 'BindPhone' then
            return CSigninMgr.Inst:CheckValidPlatformByPhoneClient(key)
        elseif key == 'GiftExchange' then
            if CommonDefs.IS_CN_CLIENT then
                return true
            elseif CommonDefs.IS_VN_CLIENT then
                return false
            else
                return true --CSigninMgr.Inst:CheckValidPlatformByPlayConfigAndPhoneClient(key)
            end
        elseif key == 'QiYuJLB' then
            return CJiFenWebMgr.Inst.m_CanAccess and CSigninMgr.Inst:CheckValidPlatformByPhoneClient(key)
        elseif key == 'BindAssist' or key == 'BindDaShen' then --大神和助手一直都是一个判断处理,之后如有分开处理的需求注意修改！
            if CommonDefs.IS_CN_CLIENT then
                return LuaWelfareMgr.EnableDaShen()
            else
                return false
            end
        elseif key == 'TeamReverse' then
            local playId = 81
            local checkNum = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(playId)
            if checkNum > 0 then
                return true
            else
                return false
            end
        elseif key == "Unity2018GengXin" then
            return LuaWelfareMgr:CanShowUnity2018UpgradeAlert()
        elseif key == "ZhaoHuiNew" then
            if CClientMainPlayer.Inst.Level < 50 then
                return false
            else
                return true
            end
        elseif key == "WeChatWelfare" then
            return self:GetWeCharWelfareNeedShow()
        elseif key == "AWuWeChatwelfare" then
            return LuaJingLingMgr.m_OpenWeChatWelfare and CSigninMgr.Inst:CheckValidPlatformByPhoneClient(key)
        else
            return CSigninMgr.Inst:CheckValidPlatformByPlayerConfig(key)
        end
    end
end

function LuaOtherWelfareWnd:OnEnable()
    self:InitContent()
    g_ScriptEvent:AddListener("BindMobileVeryfiCaptchaSuccess", self, "OnBindPhoneSuccess")
    g_ScriptEvent:AddListener("JiFenWebInfoResult", self, "OnJiFenWebInfoResult")
    g_ScriptEvent:AddListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
end

function LuaOtherWelfareWnd:OnDisable()
    g_ScriptEvent:RemoveListener("BindMobileVeryfiCaptchaSuccess", self, "OnBindPhoneSuccess")
    g_ScriptEvent:RemoveListener("JiFenWebInfoResult", self, "OnJiFenWebInfoResult")
    g_ScriptEvent:RemoveListener("OnRefresbUnity2018UpgradeInfo", self, "OnRefresbUnity2018UpgradeInfo")
end

function LuaOtherWelfareWnd:OnBindPhoneSuccess(args)
    self:InitContent()
end

function LuaOtherWelfareWnd:OnJiFenWebInfoResult(args)
    self:InitContent()
end

function LuaOtherWelfareWnd:OnRefresbUnity2018UpgradeInfo()
    self:InitContent()
end

function LuaOtherWelfareWnd:GetWeCharWelfareNeedShow()
    --VN屏蔽微信福利
    if CommonDefs.IS_VN_CLIENT then
        return false
    end
    
    if LuaJingLingMgr:CanOpenWeChatWelFare() and CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level <= 14 and CClientMainPlayer.Inst.ItemProp.Vip.Level >= 1 then
        return true
    end
    return false
end
