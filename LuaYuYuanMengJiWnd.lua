local QnTabView = import "L10.UI.QnTabView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UIPanel = import "UIPanel"
local UILabel = import "UILabel"
local Object = import "System.Object"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"

LuaYuYuanMengJiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuYuanMengJiWnd, "LeftTable", "LeftTable", QnTabView)
RegistChildComponent(LuaYuYuanMengJiWnd, "NPC", "NPC", CUITexture)
RegistChildComponent(LuaYuYuanMengJiWnd, "ArchiveContent", "ArchiveContent", UILabel)
RegistChildComponent(LuaYuYuanMengJiWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaYuYuanMengJiWnd, "Info2", "Info2", GameObject)
RegistChildComponent(LuaYuYuanMengJiWnd, "PassedTex", "PassedTex", GameObject)
RegistChildComponent(LuaYuYuanMengJiWnd, "InfoText", "InfoText", UILabel)
RegistChildComponent(LuaYuYuanMengJiWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaYuYuanMengJiWnd, "ArchiveEnding", "ArchiveEnding", UIPanel)
RegistChildComponent(LuaYuYuanMengJiWnd, "RunFx", "RunFx", GameObject)
RegistChildComponent(LuaYuYuanMengJiWnd, "EndText", "EndText", UILabel)
RegistChildComponent(LuaYuYuanMengJiWnd, "Title2", "Title2", UILabel)
RegistChildComponent(LuaYuYuanMengJiWnd, "NoArchive", "NoArchive", UILabel)
RegistChildComponent(LuaYuYuanMengJiWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaYuYuanMengJiWnd, "m_datas")
RegistClassMember(LuaYuYuanMengJiWnd, "m_beReadCount")
RegistClassMember(LuaYuYuanMengJiWnd, "m_isHEState")
RegistClassMember(LuaYuYuanMengJiWnd, "m_selectIndex")
RegistClassMember(LuaYuYuanMengJiWnd, "m_tweener")

function LuaYuYuanMengJiWnd:Awake()
    self.m_beReadCount = 0
    self.m_selectIndex = 0

    self.m_isHEState = false
    local taskid = ZhongYuanJie_Setting2022.GetData().YYMJ_BE_TASKID
    local taskProp = CClientMainPlayer.Inst.TaskProp
    if taskProp:IsMainPlayerTaskFinished(taskid) then
        self.m_isHEState = true
    end

    self.m_datas = {}
    ZhongYuanJie_YuYuanMengJi.Foreach(
        function(id, data)
            local tdata = {
                GroupId = data.GroupId,
                Portrait = data.Portrait,
                Name = data.Name,
                Title = data.Title,
                Content = data.Content,
                Ending = data.Ending,
                NeedFx = not self.m_isHEState,
                HEState = self:GetHEState(data.TaskId),
                TaskId = data.TaskId
            }

            table.insert(self.m_datas, tdata)
        end
    )

    UIEventListener.Get(self.InfoText.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnInfoTextClick()
        end
    )

    --@region EventBind: Dont Modify Manually!

    self.LeftTable.OnSelect =
        DelegateFactory.Action_QnTabButton_int(
        function(btn, index)
            self:OnLeftTableSelected(btn, index)
        end
    )

    UIEventListener.Get(self.TipButton.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnTipButtonClick()
        end
    )

    UIEventListener.Get(self.CloseButton.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnCloseButtonClick()
        end
    )

    --@endregion EventBind end
end

function LuaYuYuanMengJiWnd:Init()
    local ct = #self.m_datas
    for i = 1, ct do
        self:InitGroup(i)
    end
    self:RefreshTip()

    local cond = ZhongYuanJie2022Mgr.YYMJCondID
    if cond > 0 then
        self.LeftTable:ChangeTo(cond - 1)
    end
end

function LuaYuYuanMengJiWnd:RefreshTip()
    self.Info2:SetActive(not self.m_isHEState)
    self.InfoText.gameObject:SetActive(self.m_isHEState)
    if not self.m_isHEState then
        local ct = #self.m_datas
        local intotext = FindChildWithType(self.Info2.transform, "InfoText", typeof(UILabel))
        intotext.text = self.m_beReadCount .. "/" .. ct
        intotext.color = self.m_beReadCount == ct and Color(0, 1, 96 / 255, 1) or Color(1, 80 / 255, 80 / 255, 1)
    else
        self.InfoText.text = g_MessageMgr:FormatMessage("YYMJ_TIP")
    end
end

function LuaYuYuanMengJiWnd:GetHEState(taskid)
    if not self.m_isHEState then --BE阶段
        return 0
    else --HE阶段
        if ZhongYuanJie2022Mgr.YYMJTaskID == taskid then --即将完成任务
            return 2
        end

        local taskProp = CClientMainPlayer.Inst.TaskProp --已经完成任务
        if taskProp:IsMainPlayerTaskFinished(taskid) then
            return 3
        end

        return 1 --未完成任务
    end
end

function LuaYuYuanMengJiWnd:InitGroup(index)
    if self.m_datas == nil then
        return
    end
    local data = self.m_datas[index]
    local root = FindChild(self.LeftTable.transform, "Group" .. index)

    local icon = FindChildWithType(root, "tex", typeof(CUITexture))
    icon:LoadNPCPortrait(data.Portrait, data.HEState == 1)

    local bar = FindChildWithType(root, "bar", typeof(GameObject))
    bar:SetActive(data.HEState >= 2) --HE完成状态

    local highlight = FindChildWithType(root, "highlight", typeof(GameObject))
    highlight:SetActive(self.m_selectIndex == index)

    local fx = FindChildWithType(root, "Alert", typeof(CUIFx))
    if data.NeedFx then
        fx.gameObject:SetActive(true)
        LuaTweenUtils.DOKill(fx.transform, false)
        fx.transform.localPosition = Vector3(0, 0, 0)
        local b = NGUIMath.CalculateRelativeWidgetBounds(root)
        local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
        fx.transform.localPosition = waypoints[0]
        fx:LoadFx("fx/ui/prefab/UI_kuang_blue02.prefab")
        LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, -1, Ease.Linear)
    else
        fx.gameObject:SetActive(false)
    end
end

function LuaYuYuanMengJiWnd:RefeshContent(index)
    if self.m_datas == nil then
        return
    end
    local data = self.m_datas[index]
    self:RefreshNPC(data)
    self:RefreshDesc(data)
    self.PassedTex:SetActive(data.HEState >= 2) --HE完成状态

    self.NoArchive.gameObject:SetActive(data.HEState == 1) --HE未完成
    if data.HEState == 1 then
        local fmt = LocalString.GetString("完成任务%s可改写结局")
        local taskdata = Task_Task.GetData(data.TaskId)
        local taskname = taskdata == nil and "" or taskdata.Display
        self.NoArchive.text = g_MessageMgr:Format(fmt, taskname)
    end

    self.EndText.gameObject:SetActive(data.HEState >= 2)
    self.RunFx:SetActive(data.HEState == 2)
    if data.HEState >= 2 then
        self.EndText.text = data.Ending
        local y = self.ArchiveEnding.clipOffset.y
        if data.HEState == 2 then
            self.m_tweener = LuaTweenUtils.TweenFloat(
                -1200,
                0,
                1.5,
                function(val)
                    if self.ArchiveEnding then
                        self.ArchiveEnding.clipOffset = Vector2(val, y)
                    end
                end
            )
        else
            self.ArchiveEnding.clipOffset = Vector2(0, y)
        end
    end
end

function LuaYuYuanMengJiWnd:RefreshNPC(data)
    local icon = self.NPC:LoadNPCPortrait(data.Portrait, data.HEState == 1) --HE未完成

    local label = FindChildWithType(self.NPC.transform, "Name", typeof(UILabel))
    label.text = data.Name
end

function LuaYuYuanMengJiWnd:RefreshDesc(data)
    self.ArchiveContent.text = g_MessageMgr:Format(data.Content)
    self.Title.text = data.Title
    self.Title2.enabled = false
    self.Title2.enabled = true
    self.Title2.text = g_MessageMgr:Format(LocalString.GetString("关于%s"), data.Name)
end

function LuaYuYuanMengJiWnd:OnDisable()
    if self.m_tweener then
        LuaTweenUtils.Kill(self.m_tweener, false)
    end
end

--@region UIEvent

function LuaYuYuanMengJiWnd:OnInfoTextClick()
    local url = self.InfoText:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function LuaYuYuanMengJiWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("YYMJ_RULE")
end

function LuaYuYuanMengJiWnd:OnLeftTableSelected(btn, index)
    local lastindex = self.m_selectIndex
    self.m_selectIndex = index + 1
    if lastindex > 0 then
        self:InitGroup(lastindex)
    end
    self:InitGroup(self.m_selectIndex)

    if not self.m_isHEState then
        local data = self.m_datas[self.m_selectIndex]
        if data.NeedFx then
            data.NeedFx = false
            self.m_beReadCount = self.m_beReadCount + 1
            self:RefreshTip()
        end
    end
    self:RefeshContent(self.m_selectIndex)
end

function LuaYuYuanMengJiWnd:OnCloseButtonClick()
    local empty = CreateFromClass(MakeGenericClass(List, Object))
    empty = MsgPackImpl.pack(empty)
    if not self.m_isHEState then
        if self.m_beReadCount == #self.m_datas then
            local taskid = ZhongYuanJie_Setting2022.GetData().YYMJ_BE_TASKID
            Gac2Gas.FinishClientTaskEventWithConditionId(taskid, "YuYuanMengJi", empty, 0) --完成任务
            CUIManager.CloseUI(CLuaUIResources.YuYuanMengJiWnd)
        else
            local msg = g_MessageMgr:FormatMessage("YYMJ_BETASK_INTERRUPT")
            local okfunc = function()
                CUIManager.CloseUI(CLuaUIResources.YuYuanMengJiWnd)
            end
            g_MessageMgr:ShowOkCancelMessage(msg, okfunc, nil, nil, nil, false)
        end
    else
        local taskid = ZhongYuanJie2022Mgr.YYMJTaskID
        local condid = ZhongYuanJie2022Mgr.YYMJCondID
        Gac2Gas.FinishClientTaskEventWithConditionId(taskid, "YuYuanMengJi", empty, condid)
        CUIManager.CloseUI(CLuaUIResources.YuYuanMengJiWnd)
    end
    ZhongYuanJie2022Mgr.YYMJTaskID = 0
    ZhongYuanJie2022Mgr.YYMJCondID = 0
end

--@endregion UIEvent
