local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaGuoQingPvEMgr = {}

--RegistClassMember(LuaGuoQingPvEMgr,"RankData")
--RegistClassMember(LuaGuoQingPvEMgr,"SelfFinishTime")

function LuaGuoQingPvEMgr:InitRankData(selfFinishTime,data)
    self.RankData = {}
	self.SelfFinishTime = selfFinishTime
	for i=1, #data, 4 do
		local x = {}
		x["rank"] = data[i]
		x["playerId"] = data[i+1]
		x["playerName"] = data[i+2]
		x["finishTime"] = data[i+3]
		table.insert(self.RankData,x)
		-- 4个数据为1组
		-- rank, playerId, playerName, finishTime (单位：秒)
	end--
	CUIManager.ShowUI(CLuaUIResources.GuoQingPvERankWnd)
end