-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQingQiuExchangeWnd = import "L10.UI.CQingQiuExchangeWnd"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local String = import "System.String"
local TaoHuaBaoXia_Flowers = import "L10.Game.TaoHuaBaoXia_Flowers"
local TaoHuaBaoXia_GameSetting = import "L10.Game.TaoHuaBaoXia_GameSetting"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
CQingQiuExchangeWnd.m_Init_CS2LuaHook = function (this) 
    Gac2Gas.QueryTaoHuaBaoXiaFlowerData()
    Gac2Gas.QueryTaoHuaBaoXiaGetHelpCount()
    local rewardItemId = TaoHuaBaoXia_GameSetting.GetData().RewardItemID
    local rewardData = Item_Item.GetData(rewardItemId)
    if rewardData == nil then
        return
    end
    --m_RewardTex.LoadMaterial(rewardData.Icon);
    this.m_RewardName = rewardData.Name
    UIEventListener.Get(this.m_RewardTex.gameObject).onClick = DelegateFactory.VoidDelegate(function (obj) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(rewardItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
    local index = 0
    TaoHuaBaoXia_Flowers.ForeachKey(DelegateFactory.Action_object(function (key) 
        CommonDefs.ListAdd(this.m_FlowerItem, typeof(UInt32), key)
        local data = Item_Item.GetData(key)
        if data ~= nil then
            this.m_FlowerTex[index]:LoadMaterial(data.Icon)
            UIEventListener.Get(this.m_FlowerTex[index].gameObject).onClick = DelegateFactory.VoidDelegate(function (obj) 
                CItemInfoMgr.ShowLinkItemTemplateInfo(key, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
            this.m_FlowerLabel[index].text = data.Name
        end
        index = index + 1
    end))
end
CQingQiuExchangeWnd.m_SetValentineFlowerData_CS2LuaHook = function (this, dict) 
    local maxReward = Int32.MaxValue
    do
        local i = 0 local cnt = this.m_FlowerItem.Count local num
        while i < cnt do
            if dict ~= nil and CommonDefs.DictContains(dict, typeof(String), tostring(this.m_FlowerItem[i])) then
                num = math.floor(tonumber(CommonDefs.DictGetValue(dict, typeof(String), tostring(this.m_FlowerItem[i])) or 0))
            else
                num = 0
            end
            maxReward = math.min(num, maxReward)
            if num <= 0 then
                this.m_CountLabel[i].text = "[FF0000]0[-]/1"
            else
                this.m_CountLabel[i].text = num .. "/1"
            end
            i = i + 1
        end
    end

    if maxReward <= 0 then
        this.m_RewardLabel.text = this.m_RewardName .. "[FF0000]x0[-]"
        this.m_ExchangeBtn.Enabled = false
        --m_RewardFxTex.SetActive(false);
        Extensions.SetLocalPositionZ(this.m_RewardTex.transform, - 1)
    else
        this.m_ExchangeBtn.Enabled = true
        this.m_RewardLabel.text = (this.m_RewardName .. "x") .. maxReward
        --m_RewardFxTex.SetActive(true);
        Extensions.SetLocalPositionZ(this.m_RewardTex.transform, 0)
    end
end
CQingQiuExchangeWnd.m_SetValentineGetHelpCount_CS2LuaHook = function (this, cnt) 
    this.m_HelpLabel.text = ((LocalString.GetString("求助 ") .. cnt) .. "/") .. CQingQiuExchangeWnd.s_TotalHelpCount
    if cnt >= CQingQiuExchangeWnd.s_TotalHelpCount then
        this.m_HelpBtn.Enabled = false
    else
        this.m_HelpBtn.Enabled = true
    end
end
