local Input = import "UnityEngine.Input"
local CUITexture = import "L10.UI.CUITexture"
local CSpeakingBubble = import "L10.UI.CSpeakingBubble"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CCommonDanMuCtrl = import "L10.UI.CCommonDanMuCtrl"
local EnumSpeakingBubbleType=import "L10.Game.EnumSpeakingBubbleType"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local UIBasicSpriteFlip = import " UIBasicSprite+Flip"
local CBaseWnd = import "L10.UI.CBaseWnd"
local EChatPanel=import "L10.Game.EChatPanel"
local CSocialWndMgr=import "L10.UI.CSocialWndMgr"
local CAsyncLoadTexture = import "L10.UI.CAsyncLoadTexture"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Time = import "UnityEngine.Time"

LuaShiTuWuZiQiWnd=class()
RegistClassMember(LuaShiTuWuZiQiWnd,"m_CloseButton")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_OwnerPortrait")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OwnerNameLabel")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OwnerNoPlayerIcon")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OwnerReadyIcon")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OwnerSpeakingBubble")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OwnerChessmanLabel")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_OppPortrait")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OppNameLabel")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OppNoPlayerIcon")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OppReadyIcon")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OppSpeakingBubble")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_OppChessmanLabel")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_PastTimeLabel")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_RoundNumLabel")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_BeReadyButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_CancelReadyButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_GiveUpButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResetButton")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_ChessBoardRoot")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ChessBoardBg")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ChessmanTemplate")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_PlaceButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_MarkButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_MarkIndicator")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_QuickChatButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ChatButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_WatcherListBg")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_WatcherScrollView")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_WatcherScrollIndicator")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_WatcherTable")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_WatcherTemplate")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_WatcherExpandButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_WatcherExpandButtonArrow")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_DisableDanMuCheckBox")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_DanMuCtrl")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResultView")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResultWinIcon")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResultLoseIcon")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResultTieIcon")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResultLeaveButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResultContinueButton")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ResultContinueForNormalButton")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_CurSelectedGridIdx")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_GridIdxTbl")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_ShowWatcherList")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_MaxWatcherListBgHeight")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_MinWatcherListBgHeight")

RegistClassMember(LuaShiTuWuZiQiWnd,"m_PastTimeTick")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_MarkIndicatorTick")
RegistClassMember(LuaShiTuWuZiQiWnd,"m_LastResetTime") --上次悔棋时间
RegistClassMember(LuaShiTuWuZiQiWnd,"m_CELL_NUM_PER_LINE")


function LuaShiTuWuZiQiWnd:Awake()
    self.m_CloseButton = self.transform:Find("CloseButton").gameObject
    local ownerRoot = self.transform:Find("Anchor/Players/Owner")
    local oppRoot = self.transform:Find("Anchor/Players/Opp")
    self.m_OwnerPortrait = ownerRoot:Find("Portrait"):GetComponent(typeof(CUITexture))
    self.m_OwnerNameLabel = ownerRoot:Find("Portrait/NameLabel"):GetComponent(typeof(UILabel))
    self.m_OwnerNoPlayerIcon = ownerRoot:Find("Portrait/NoPlayerIcon").gameObject
    self.m_OwnerReadyIcon = ownerRoot:Find("Portrait/ReadyIcon").gameObject
    self.m_OwnerSpeakingBubble = ownerRoot:Find("SpeakingBubble"):GetComponent(typeof(CSpeakingBubble))
    self.m_OwnerChessmanLabel = ownerRoot:Find("ChessmanIcon/Label"):GetComponent(typeof(UILabel))
    self.m_OppPortrait = oppRoot:Find("Portrait"):GetComponent(typeof(CUITexture))
    self.m_OppNameLabel = oppRoot:Find("Portrait/NameLabel"):GetComponent(typeof(UILabel))
    self.m_OppNoPlayerIcon = oppRoot:Find("Portrait/NoPlayerIcon").gameObject
    self.m_OppReadyIcon = oppRoot:Find("Portrait/ReadyIcon").gameObject
    self.m_OppSpeakingBubble = oppRoot:Find("SpeakingBubble"):GetComponent(typeof(CSpeakingBubble))
    self.m_OppChessmanLabel = oppRoot:Find("ChessmanIcon/Label"):GetComponent(typeof(UILabel))

    self.m_PastTimeLabel = self.transform:Find("Anchor/RoundInfo/PastTimeLabel"):GetComponent(typeof(UILabel))
    self.m_RoundNumLabel = self.transform:Find("Anchor/RoundInfo/RoundNumLabel"):GetComponent(typeof(UILabel))

    self.m_ChessBoardRoot = self.transform:Find("Anchor/ChessBoard")
    self.m_ChessBoardBg = self.transform:Find("Anchor/ChessBoard/CoordBg"):GetComponent(typeof(UIWidget))
    self.m_ChessmanTemplate = self.transform:Find("Anchor/ChessBoard/ChessmanTemplate").gameObject
    self.m_PlaceButton = self.transform:Find("Anchor/ChessBoard/PlaceButton").gameObject
    self.m_MarkButton = self.transform:Find("Anchor/ChessBoard/MarkButton").gameObject
    self.m_MarkIndicator = self.transform:Find("Anchor/ChessBoard/MarkIndicator").gameObject
    self.m_ChessmanTemplate:SetActive(false)
    self.m_PlaceButton:SetActive(false)
    self.m_MarkButton:SetActive(false)
    self.m_MarkIndicator:SetActive(false)

    self.m_BeReadyButton = self.transform:Find("Anchor/BeReadyButton").gameObject
    self.m_CancelReadyButton = self.transform:Find("Anchor/CancelReadyButton").gameObject
    self.m_GiveUpButton = self.transform:Find("Anchor/GiveUpButton").gameObject
    self.m_ResetButton = self.transform:Find("Anchor/ResetButton").gameObject

    self.m_QuickChatButton = self.transform:Find("BottomRight/QuickChatButton").gameObject
    self.m_ChatButton = self.transform:Find("BottomRight/ChatButton").gameObject
    local watcherRoot =  self.transform:Find("BottomRight/WatcherList")
    self.m_WatcherListBg = watcherRoot:GetComponent(typeof(UIWidget))
    self.m_WatcherScrollView =  watcherRoot:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_WatcherScrollIndicator = watcherRoot:Find("ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    self.m_WatcherTable =  watcherRoot:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_WatcherTemplate =  watcherRoot:Find("ScrollView/WatcherTemplate").gameObject
    self.m_WatcherExpandButton = watcherRoot:Find("ExpandButton").gameObject
    self.m_WatcherInviteButton = watcherRoot:Find("InviteButton").gameObject
    self.m_WatcherExpandButtonArrow = watcherRoot:Find("ExpandButton/Arrow"):GetComponent(typeof(UISprite))
    self.m_DisableDanMuCheckBox =  watcherRoot:Find("DisableDanMuCheckBox"):GetComponent(typeof(QnCheckBox))
    self.m_DisableDanMuCheckBox.Selected = false
    self.m_WatcherTemplate:SetActive(false)
    self.m_MaxWatcherListBgHeight = self.m_WatcherListBg.height
    self.m_MinWatcherListBgHeight = 10
    self.m_DanMuCtrl = self.transform:Find("DanMu"):GetComponent(typeof(CCommonDanMuCtrl))

    self.m_ResultView = self.transform:Find("Result").gameObject
    self.m_ResultWinIcon = self.transform:Find("Result/Win").gameObject
    self.m_ResultLoseIcon = self.transform:Find("Result/Lose").gameObject
    self.m_ResultTieIcon = self.transform:Find("Result/Tie").gameObject
    self.m_ResultLeaveButton = self.transform:Find("Result/LeaveButton").gameObject
    self.m_ResultContinueButton = self.transform:Find("Result/ContinueButton").gameObject
    self.m_ResultContinueForNormalButton = self.transform:Find("Result/ContinueForNormalButton").gameObject

    self.m_ResultView:SetActive(false)

    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function() self:OnCloseButtonClick() end)
    UIEventListener.Get(self.m_OppPortrait.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnOppPortraitClick() end)
    UIEventListener.Get(self.m_ChessBoardBg.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnChessBoardClick() end)
    UIEventListener.Get(self.m_PlaceButton.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnPlaceButtonClick() end)
    UIEventListener.Get(self.m_MarkButton.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnMarkButtonClick() end)


    UIEventListener.Get(self.m_BeReadyButton).onClick = DelegateFactory.VoidDelegate(function() self:OnBeReadyButtonClick() end)
    UIEventListener.Get(self.m_CancelReadyButton).onClick = DelegateFactory.VoidDelegate(function() self:OnCancelReadyButtonClick() end)
    UIEventListener.Get(self.m_GiveUpButton).onClick = DelegateFactory.VoidDelegate(function() self:OnGiveUpButtonClick() end)
    UIEventListener.Get(self.m_ResetButton).onClick = DelegateFactory.VoidDelegate(function() self:OnResetButtonClick() end) 
    UIEventListener.Get(self.m_QuickChatButton).onClick = DelegateFactory.VoidDelegate(function() self:OnQuickChatButtonClick() end) 
    UIEventListener.Get(self.m_ChatButton).onClick = DelegateFactory.VoidDelegate(function() self:OnChatButtonClick() end) 
    UIEventListener.Get(self.m_WatcherExpandButton).onClick = DelegateFactory.VoidDelegate(function() self:OnWatcherExpandButtonClick() end)
    UIEventListener.Get(self.m_WatcherInviteButton).onClick = DelegateFactory.VoidDelegate(function() self:OnWatcherInviteButtonClick() end)
    UIEventListener.Get(self.m_ResultLeaveButton).onClick = DelegateFactory.VoidDelegate(function() self:Close() end)
    UIEventListener.Get(self.m_ResultContinueButton).onClick = DelegateFactory.VoidDelegate(function() self:OnContinueButtonClick() end)
    UIEventListener.Get(self.m_ResultContinueForNormalButton).onClick = DelegateFactory.VoidDelegate(function() self:OnContinueForNormalButtonClick() end)

    self.m_GridIdxTbl = {}
    self.m_CurSelectedGridIdx = -1

    self.m_CELL_NUM_PER_LINE = 14
    self.m_ShowWatcherList = true
    self.m_LastResetTime = 0
end

function LuaShiTuWuZiQiWnd:Init()
    --LuaShiTuMgr:WZQ_GenFakeWuZiQiInfo()
    self:UpdateOwnerInfo()
    self:UpdateOpponentInfo()
    self:UpdateReadyInfo()
    self:UpdateRoundInfo()
    self:UpdateOperationInfo()
    self:UpdateChessboardInfo()
    self:UpdateWatcherListInfo()
    self:StartPastTimeTick()
    self:UpdateResultInfo()
    self:StartCheckRoundEndTick()
end

function LuaShiTuWuZiQiWnd:OnEnable()
    g_ScriptEvent:AddListener("OnShiTuWuZiQiPlayerInfoUpdate", self, "OnShiTuWuZiQiPlayerInfoUpdate")
    g_ScriptEvent:AddListener("OnShiTuWuZiQiPlayStarted", self, "OnShiTuWuZiQiPlayStarted")
    g_ScriptEvent:AddListener("OnShiTuWuZiQiGridInfoUpdate", self, "OnShiTuWuZiQiGridInfoUpdate")
    g_ScriptEvent:AddListener("OnRecvShiTuWuZiQiChatMsg", self, "OnRecvShiTuWuZiQiChatMsg")
    g_ScriptEvent:AddListener("OnShiTuWuZiQiWatcherInfoUpdate", self, "OnShiTuWuZiQiWatcherInfoUpdate")
    g_ScriptEvent:AddListener("OnRecvShiTuWuZiQiMarkGridInfo", self, "OnRecvShiTuWuZiQiMarkGridInfo")
    g_ScriptEvent:AddListener("OnShiTuWuZiQiReturnResult", self, "OnShiTuWuZiQiReturnResult")
end

function LuaShiTuWuZiQiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnShiTuWuZiQiPlayerInfoUpdate", self, "OnShiTuWuZiQiPlayerInfoUpdate")
    g_ScriptEvent:RemoveListener("OnShiTuWuZiQiPlayStarted", self, "OnShiTuWuZiQiPlayStarted")
    g_ScriptEvent:RemoveListener("OnShiTuWuZiQiGridInfoUpdate", self, "OnShiTuWuZiQiGridInfoUpdate")
    g_ScriptEvent:RemoveListener("OnRecvShiTuWuZiQiChatMsg", self, "OnRecvShiTuWuZiQiChatMsg")
    g_ScriptEvent:RemoveListener("OnShiTuWuZiQiWatcherInfoUpdate", self, "OnShiTuWuZiQiWatcherInfoUpdate")
    g_ScriptEvent:RemoveListener("OnRecvShiTuWuZiQiMarkGridInfo", self, "OnRecvShiTuWuZiQiMarkGridInfo")
    g_ScriptEvent:RemoveListener("OnShiTuWuZiQiReturnResult", self, "OnShiTuWuZiQiReturnResult")
end

function LuaShiTuWuZiQiWnd:OnDestroy()
    LuaShiTuMgr:LeaveWuZiQi()
    self:StopPastTimeTick()
    self:StopMarkIndicatorTick()
    self:StopCheckRoundEndTick()
end

function LuaShiTuWuZiQiWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaShiTuWuZiQiWnd:OnShiTuWuZiQiPlayerInfoUpdate()
    self:UpdateOwnerInfo()
    self:UpdateOpponentInfo()
    self:UpdateReadyInfo()
    self:UpdateOperationInfo()
end

function LuaShiTuWuZiQiWnd:OnShiTuWuZiQiPlayStarted()
    self.m_LastResetTime = 0
    self:UpdateReadyInfo()
    self:UpdateOperationInfo()
end

function LuaShiTuWuZiQiWnd:OnShiTuWuZiQiGridInfoUpdate()
    self:UpdateChessboardInfo()
    self:UpdateOperationInfo()
end

function LuaShiTuWuZiQiWnd:OnRecvShiTuWuZiQiChatMsg(playerId, msg)
    if LuaShiTuMgr:WZQ_IsWatcher(playerId) then
        if not self.m_DisableDanMuCheckBox.Selected then
            self.m_DanMuCtrl:AddBullet(msg)
        end
    elseif LuaShiTuMgr:WZQ_IsOwner(playerId) then
        self.m_OwnerSpeakingBubble:Init(msg, EnumSpeakingBubbleType.Default, nil, 5)
    elseif LuaShiTuMgr:WZQ_IsOpp(playerId) then
        self.m_OppSpeakingBubble:Init(msg, EnumSpeakingBubbleType.Default, nil, 5)
    end
end

function LuaShiTuWuZiQiWnd:OnRecvShiTuWuZiQiMarkGridInfo(gridIdx, playerName)
    if self.m_DisableDanMuCheckBox.Selected then
        return
    end
    self.m_MarkIndicator:SetActive(true)
    self.m_MarkIndicator.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s指棋"), playerName)
    local posX, posY = self:GetPos(gridIdx)
    self.m_MarkIndicator.transform.localPosition = Vector3(posX, posY, 0)
    self:StartMarkIndicatorTick()
end

function LuaShiTuWuZiQiWnd:OnShiTuWuZiQiReturnResult(winPlayerId, resultGridInfo)
    self:UpdateResultInfo()
    -- TODO 闪烁棋子
end

function LuaShiTuWuZiQiWnd:UpdateOwnerInfo()
    local ownerId, ownerName, ownerClass, ownerGender =LuaShiTuMgr:WZQ_GetOwnerInfo()
    local ownerRoot = self.transform:Find("Anchor/Players/Owner")
    ownerRoot.gameObject:SetActive(ownerId and ownerId>0)
    self:UpdatePlayerInfo(ownerId, ownerName, ownerClass, ownerGender, self.m_OwnerPortrait, self.m_OwnerNameLabel, self.m_OwnerNoPlayerIcon)
end

function LuaShiTuWuZiQiWnd:UpdateOpponentInfo()
    local oppId, oppName, oppClass, oppGender =LuaShiTuMgr:WZQ_GetOppInfo()
    local oppRoot = self.transform:Find("Anchor/Players/Opp")
    if LuaShiTuMgr:WZQ_IsShiTuTask() then -- refs #182016 师徒轻易春秋任务屏蔽邀请的节点，其他情况下允许邀请
        oppRoot.gameObject:SetActive(oppId and oppId>0)
    end
    self:UpdatePlayerInfo(oppId, oppName, oppClass, oppGender, self.m_OppPortrait, self.m_OppNameLabel, self.m_OppNoPlayerIcon)
end

function LuaShiTuWuZiQiWnd:UpdatePlayerInfo(playerId, name, class, gender, portraitTexture, nameLabel, noPlayerIcon)
    portraitTexture:LoadPortrait(CUICommonDef.GetPortraitName(class,gender, -1), false)
    nameLabel.text = name
    noPlayerIcon:SetActive(not( playerId and playerId>0))
end

function LuaShiTuWuZiQiWnd:UpdateReadyInfo()
    local playStarted = LuaShiTuMgr:WZQ_IsPlayStarted()
    self.m_OwnerReadyIcon:SetActive(not playStarted and LuaShiTuMgr:WZQ_IsOwnerReady())
    self.m_OppReadyIcon:SetActive(not playStarted and LuaShiTuMgr:WZQ_IsOppReady())
end

function LuaShiTuWuZiQiWnd:UpdateRoundInfo(playInfo)
    local pastSeconds = LuaShiTuMgr:WZQ_IsPlayStarted() and LuaShiTuMgr:WZQ_GetPastTimeSeconds() or 0
    self.m_PastTimeLabel.text = SafeStringFormat3("%02d:%02d", math.floor(pastSeconds / 60), pastSeconds % 60)
    self.m_RoundNumLabel.text = tostring(LuaShiTuMgr:WZQ_GetPastRoundNum())
    local lastStepPlayerId = LuaShiTuMgr:WZQ_GetLastStepPlayerId()
    local lastStepTime = LuaShiTuMgr:WZQ_GetLastStepTime()
    if LuaShiTuMgr:WZQ_IsPlayStarted() then
        if LuaShiTuMgr:WZQ_GetOwnerId() == lastStepPlayerId then
            self.m_OwnerChessmanLabel.gameObject:SetActive(false)
            self.m_OppChessmanLabel.gameObject:SetActive(true)
            self.m_OppChessmanLabel.text = tostring(math.ceil(LuaShiTuMgr:WZQ_GetLeftSetGridTime()))
        else
            self.m_OwnerChessmanLabel.gameObject:SetActive(true)
            self.m_OppChessmanLabel.gameObject:SetActive(false)
            self.m_OwnerChessmanLabel.text = tostring(math.ceil(LuaShiTuMgr:WZQ_GetLeftSetGridTime()))
        end
    else
        self.m_OwnerChessmanLabel.gameObject:SetActive(false)
        self.m_OppChessmanLabel.gameObject:SetActive(false)
    end
end

function LuaShiTuWuZiQiWnd:UpdateOperationInfo()
    local playStarted =  LuaShiTuMgr:WZQ_IsPlayStarted()
    local playerIsOwner = LuaShiTuMgr:WZQ_MainPlayerIsOwner()
    local playerIsOpp = LuaShiTuMgr:WZQ_MainPlayerIsOpp()
    local isReady = (playerIsOwner and LuaShiTuMgr:WZQ_IsOwnerReady()) or (playerIsOpp and LuaShiTuMgr:WZQ_IsOppReady())
    self.m_BeReadyButton:SetActive(not playStarted and not isReady and not LuaShiTuMgr:WZQ_MainPlayerIsWatcher())
    self.m_CancelReadyButton:SetActive(not playStarted and isReady)
    self.m_GiveUpButton:SetActive(playStarted and (playerIsOwner or playerIsOpp))
    self.m_ResetButton:SetActive(playStarted and (playerIsOwner or playerIsOpp) and
        LuaShiTuMgr:WZQ_GetPastRoundNum()>0 and LuaShiTuMgr:WZQ_GetLastStepGridIndex()>0 and not LuaShiTuMgr:WZQ_CanSetGrid())
end

function LuaShiTuWuZiQiWnd:UpdateChessboardInfo()
    local gridIdxTbl, lastGridIdx = LuaShiTuMgr:WZQ_GetGridIdxInfo()
    for i, data in pairs(self.m_GridIdxTbl) do
        data.gridIdx = 0
        data.go:SetActive(false)
    end
    local i = 1
    local n = #self.m_GridIdxTbl
    if lastGridIdx and lastGridIdx>=0 then
        for gridIdx, gridColor in pairs(gridIdxTbl) do
            if EnumWuZiQiGridColor.eNone ~= gridColor then
                local child = nil
                if i<n then
                    child = self.m_GridIdxTbl[i].go
                    self.m_GridIdxTbl[i].gridIdx = gridIdx
                else
                    child = CUICommonDef.AddChild(self.m_ChessBoardRoot.gameObject, self.m_ChessmanTemplate)
                    table.insert(self.m_GridIdxTbl, {gridIdx = gridIdx, go = child})
                end
                i = i+1
                child:SetActive(true)
                local posX, posY = self:GetPos(gridIdx)
                child.transform.localPosition = Vector3(posX, posY, 0)
                local alert = child.transform:Find("Alert").gameObject
                local highlight = child.transform:Find("Highlight").gameObject
                alert:SetActive(gridIdx == lastGridIdx)
                highlight:SetActive(false)
                child:GetComponent(typeof(CAsyncLoadTexture)):ShowTexture((gridColor == EnumWuZiQiGridColor.eBlack) and 0 or 1) -- 0黑色 1白色
        
            end
        end
    end
end

function LuaShiTuWuZiQiWnd:UpdateResultInfo()
    self.m_ResultView:SetActive(false)

    if not LuaShiTuMgr:WZQ_MainPlayerIsOpp() and not LuaShiTuMgr:WZQ_MainPlayerIsOwner() then
        return
    end
    if LuaShiTuMgr:WZQ_IsPlayEnded() then
        self.m_ResultWinIcon:SetActive(false)
        self.m_ResultLoseIcon:SetActive(false)
        self.m_ResultTieIcon:SetActive(false)
        local winPlayerId = LuaShiTuMgr:WZQ_GetWinPlayerId()
        if winPlayerId == 1 then --平局
            self.m_ResultView:SetActive(true)
            self.m_ResultTieIcon:SetActive(true)
        elseif winPlayerId>0 then
            self.m_ResultView:SetActive(true)
            if LuaShiTuMgr:WZQ_MainPlayerIsOwner() and LuaShiTuMgr:WZQ_GetOwnerId() == winPlayerId or
            LuaShiTuMgr:WZQ_MainPlayerIsOpp() and LuaShiTuMgr:WZQ_GetOppId() == winPlayerId then
                self.m_ResultWinIcon:SetActive(true)
            else
                --失败
                self.m_ResultLoseIcon:SetActive(true)
            end
        else
            --中途退场, 这种情况在WZQ_IsPlayEnded()中进行了排除
        end

        -- 师徒五子棋显示离开和再来一局按钮，非师徒五子棋只显示一个确定按钮（确定的行为同再来一局）
        self.m_ResultLeaveButton:SetActive(LuaShiTuMgr:WZQ_IsShiTuTask())
        self.m_ResultContinueButton:SetActive(LuaShiTuMgr:WZQ_IsShiTuTask())
        self.m_ResultContinueForNormalButton:SetActive(not LuaShiTuMgr:WZQ_IsShiTuTask())
    end
end

------ 观众

function LuaShiTuWuZiQiWnd:UpdateWatcherListInfo()
    self.m_WatcherExpandButtonArrow.flip = self.m_ShowWatcherList and UIBasicSpriteFlip.Nothing or UIBasicSpriteFlip.Vertically
    local n = self.m_WatcherTable.transform.childCount
    for i=0,n-1 do
        self.m_WatcherTable.transform:GetChild(i).gameObject:SetActive(false)
    end

    if self.m_ShowWatcherList then
        local watchers = LuaShiTuMgr:WZQ_GetWatcherList()
        local index = 0
        for __, watcherInfo in pairs(watchers) do
            local go = nil
            if index<n then
                go = self.m_WatcherTable.transform:GetChild(index).gameObject
            else
                go = CUICommonDef.AddChild(self.m_WatcherTable.gameObject, self.m_WatcherTemplate)
            end
            index = index+1
            go:SetActive(true)
            local id, name, gender, class = watcherInfo[1], watcherInfo[2], watcherInfo[3], watcherInfo[4]
            go:GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(class,gender, -1), false)
            go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = name
        end
        self.m_WatcherTable:Reposition()
    end


    local contentHeight = NGUIMath.CalculateRelativeWidgetBounds(self.m_WatcherTable.transform).size.y + self.m_WatcherTable.padding.y * 2
    local contentPadding = math.abs(self.m_WatcherScrollView.panel.topAnchor.absolute) + math.abs(self.m_WatcherScrollView.panel.bottomAnchor.absolute) + self.m_WatcherScrollView.panel.clipSoftness.y * 2 + 1
    local displayWndHeight =  math.min(math.max(contentHeight + contentPadding, self.m_MinWatcherListBgHeight), self.m_MaxWatcherListBgHeight) 
    self.m_WatcherListBg.height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.m_WatcherListBg.transform, typeof(UIRect), true)
    for i=0, rects.Length-1 do
        if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
            rects[i]:ResetAndUpdateAnchors()
        end
    end
    self.m_WatcherScrollView:ResetPosition()
    self.m_WatcherScrollIndicator:Layout()
end

function LuaShiTuWuZiQiWnd:OnWatcherExpandButtonClick()
    self.m_ShowWatcherList = not self.m_ShowWatcherList
    self:UpdateWatcherListInfo()
end

function LuaShiTuWuZiQiWnd:OnWatcherInviteButtonClick()
    LuaShiTuMgr:QueryPlayerForWuZiQi(true, self.m_WatcherInviteButton, false)
end

function LuaShiTuWuZiQiWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaShiTuWuZiQiWnd:OnContinueButtonClick()
    LuaShiTuMgr:RestartWuZiQi()
    self.m_ResultView:SetActive(false)
end

function LuaShiTuWuZiQiWnd:OnContinueForNormalButtonClick()
    LuaShiTuMgr:RestartWuZiQi()
    self.m_ResultView:SetActive(false)
end

function LuaShiTuWuZiQiWnd:OnShiTuWuZiQiWatcherInfoUpdate()
    self:UpdateWatcherListInfo()
end
------

function LuaShiTuWuZiQiWnd:StartPastTimeTick()
    if self.m_PastTimeTick then
        self:StopPastTimeTick()
    end
    self.m_PastTimeTick = RegisterTick(function()
        self:UpdateRoundInfo()
    end, 1000)
end

function LuaShiTuWuZiQiWnd:StopPastTimeTick()
    if self.m_PastTimeTick then
        UnRegisterTick(self.m_PastTimeTick)
        self.m_PastTimeTick = nil
    end
end

function LuaShiTuWuZiQiWnd:StartMarkIndicatorTick()
    if self.m_MarkIndicatorTick then
        self:StopMarkIndicatorTick()
    end
    self.m_MarkIndicatorTick = RegisterTickOnce(function()
        self:StopMarkIndicatorTick()
        self.m_MarkIndicator:SetActive(false)
    end, 1000)
end

function LuaShiTuWuZiQiWnd:StopMarkIndicatorTick()
    if self.m_MarkIndicatorTick then
        UnRegisterTick(self.m_MarkIndicatorTick)
        self.m_MarkIndicatorTick = nil
    end
end

function LuaShiTuWuZiQiWnd:StartCheckRoundEndTick()
    if self.m_CheckRoundEndTick then
        self:StopCheckRoundEndTick()
    end
    self.m_CheckRoundEndTick = RegisterTick(function()
        self:CheckRoundEnd()
    end, 1000)
end

function LuaShiTuWuZiQiWnd:StopCheckRoundEndTick()
    if self.m_CheckRoundEndTick then
        UnRegisterTick(self.m_CheckRoundEndTick)
        self.m_CheckRoundEndTick = nil
    end
end

function LuaShiTuWuZiQiWnd:CheckRoundEnd()
    if LuaShiTuMgr:CheckRoundEnd() then
        local gridIdx = nil
        if self.m_PlaceButton.activeSelf and self:CheckPlaceEmpty(self.m_CurSelectedGridIdx) and LuaShiTuMgr:WZQ_CanSetGrid() then
            gridIdx = self.m_CurSelectedGridIdx
        end
        if not gridIdx or gridIdx<1 then
            gridIdx = LuaShiTuMgr:GetRandomSetGridIndex()
        end
        if gridIdx then
            LuaShiTuMgr:RequestSetWuZiQiGrid(gridIdx)
            self.m_PlaceButton:SetActive(false)
        end
    end
end

function LuaShiTuWuZiQiWnd:OnCloseButtonClick()
    if LuaShiTuMgr:WZQ_IsPlayStarted() then
        local msg = LuaShiTuMgr:WZQ_IsShiTuTask() and "WuZiQi_Leave_Confirm" or "WuZiQi_Leave_Confirm_Normal"
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage(msg), function ()
            self:Close()
        end, nil, nil, nil, false)
    else
        self:Close()
    end
end

function LuaShiTuWuZiQiWnd:OnOppPortraitClick()
    if not LuaShiTuMgr:WZQ_GetOppId() or LuaShiTuMgr:WZQ_GetOppId()==0 then
        LuaShiTuMgr:QueryPlayerForWuZiQi(false, self.m_OppPortrait.gameObject, true)
    end
end

function LuaShiTuWuZiQiWnd:OnBeReadyButtonClick()
    LuaShiTuMgr:RequestWuZiQiReady(true)
end

function LuaShiTuWuZiQiWnd:OnCancelReadyButtonClick()
    LuaShiTuMgr:RequestWuZiQiReady(false)
end

function LuaShiTuWuZiQiWnd:OnGiveUpButtonClick()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("WuZiQi_RenShu_Confirm"), function ()
        LuaShiTuMgr:RequestGiveUpWuZiQi()
    end, nil, nil, nil, false)
end

function LuaShiTuWuZiQiWnd:OnResetButtonClick()
    --悔棋
    if self.m_LastResetTime and Time.realtimeSinceStartup - self.m_LastResetTime<60 then
        g_MessageMgr:ShowMessage("WuZiQi_Reset_CoolDown_Tip", tostring(math.ceil(self.m_LastResetTime + 60 - Time.realtimeSinceStartup)))
        return
    end
    if LuaShiTuMgr:WZQ_GetResetGridLeftTime()<=0 then
        g_MessageMgr:ShowMessage("ShiTu_WuZiQi_Reset_Timeout")
        return
    end
    self.m_LastResetTime = Time.realtimeSinceStartup
    LuaShiTuMgr:RequestResetWuZiQiGrid()
end

function LuaShiTuWuZiQiWnd:OnQuickChatButtonClick()
    CLuaDouDiZhuMgr:ShowQuickChatWnd(self.m_QuickChatButton.transform.position, "shituwuziqi")
end

function LuaShiTuWuZiQiWnd:OnChatButtonClick()
    if CUIManager.IsLoaded(CUIResources.SocialWnd) then
        CUIManager.CloseUI(CUIResources.SocialWnd)
    else
        CSocialWndMgr.ShowChatWnd(EChatPanel.Current)
    end
end

function LuaShiTuWuZiQiWnd:CheckPlaceEmpty(gridIdx)
    for __, data in pairs(self.m_GridIdxTbl) do
        if data.gridIdx == gridIdx then
            return not data.go.activeSelf
        end
    end
    return true
end

function LuaShiTuWuZiQiWnd:OnChessBoardClick()
    if not LuaShiTuMgr:WZQ_IsPlayStarted() then
        g_MessageMgr:ShowMessage("WuZiQi_NotBegin")
        return
    end
    local nearestRow,nearestColumn,posX,posY = self:GetGridIdx()
    self.m_CurSelectedGridIdx = nearestRow*(self.m_CELL_NUM_PER_LINE + 1) + nearestColumn + 1
    if nearestRow and nearestColumn then
        if not self:CheckPlaceEmpty(self.m_CurSelectedGridIdx) then
            g_MessageMgr:ShowMessage("ShiTu_WuZiQi_Already_Exist_QiZi_In_Current_Place")
            return
        end

        if LuaShiTuMgr:WZQ_CanSetGrid() then--对于当前下棋者，落子
            self.m_PlaceButton:SetActive(true)
            self.m_PlaceButton.transform.localPosition = Vector3(posX, posY, 0)  

        elseif LuaShiTuMgr:WZQ_CanMarkGrid() then--对于旁观者，指棋
            self.m_MarkButton:SetActive(true)
            self.m_MarkButton.transform.localPosition = Vector3(posX, posY, 0) 
            
        else
            g_MessageMgr:ShowMessage("ShiTu_WuZiQi_Need_Wait_Your_Turn")
        end
    end
end

function LuaShiTuWuZiQiWnd:Update()
    if Input.GetMouseButtonDown(0) then
        if CUICommonDef.IsOverUI and UICamera.lastHit.collider then
            
            if self.m_PlaceButton.activeSelf and not CUICommonDef.IsChild(self.m_PlaceButton.transform, UICamera.lastHit.collider.transform) then
                self.m_PlaceButton:SetActive(false)
            end
            if self.m_MarkButton.activeSelf and not CUICommonDef.IsChild(self.m_MarkButton.transform, UICamera.lastHit.collider.transform) then
                self.m_MarkButton:SetActive(false)
            end
        end
    end
end

function LuaShiTuWuZiQiWnd:OnPlaceButtonClick()
    if self.m_CurSelectedGridIdx>=1 and self:CheckPlaceEmpty(self.m_CurSelectedGridIdx) then
        LuaShiTuMgr:RequestSetWuZiQiGrid(self.m_CurSelectedGridIdx)
    end
    self.m_PlaceButton:SetActive(false)
end

function LuaShiTuWuZiQiWnd:OnMarkButtonClick()
    if self.m_CurSelectedGridIdx>=1 and self:CheckPlaceEmpty(self.m_CurSelectedGridIdx) then
        LuaShiTuMgr:RequestMarkWuZiQiGrid(self.m_CurSelectedGridIdx)
    end
    self.m_MarkButton:SetActive(false)
end

function LuaShiTuWuZiQiWnd:GetGridIdx()
    local screenPos = Vector3(Input.mousePosition.x, Input.mousePosition.y, 0)
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    nguiWorldPos.z = 0
    local localPos = self.m_ChessBoardRoot.transform:InverseTransformPoint(nguiWorldPos)
    local width = self.m_ChessBoardBg.width * self.m_ChessBoardBg.transform.localScale.x
    local height = self.m_ChessBoardBg.height * self.m_ChessBoardBg.transform.localScale.y
    local cellWidth = width/self.m_CELL_NUM_PER_LINE
    local cellHeight = height/self.m_CELL_NUM_PER_LINE

    localPos.x = math.min(math.max(localPos.x, -width*0.5), width*0.5)
    localPos.y = math.min(math.max(localPos.y, -height*0.5), height*0.5)

    local nearestColumn = math.floor((localPos.x + width*0.5)/cellWidth + 0.5)
    local nearestRow = self.m_CELL_NUM_PER_LINE - math.floor((localPos.y + height*0.5)/cellHeight + 0.5)
    local posX = nearestColumn * cellWidth - width * 0.5
    local posY = height * 0.5 - nearestRow * cellHeight
    return nearestRow, nearestColumn, posX, posY
end
--左上角为序号1
function LuaShiTuWuZiQiWnd:GetPos(gridIdx)
    gridIdx = gridIdx -1 --改为从0开始编号
    local row = math.floor(gridIdx / (self.m_CELL_NUM_PER_LINE + 1))
    local column = gridIdx % (self.m_CELL_NUM_PER_LINE + 1)
    local width = self.m_ChessBoardBg.width * self.m_ChessBoardBg.transform.localScale.x
    local height = self.m_ChessBoardBg.height * self.m_ChessBoardBg.transform.localScale.y
    local cellWidth = width/self.m_CELL_NUM_PER_LINE
    local cellHeight = height/self.m_CELL_NUM_PER_LINE
    local posX = column * cellWidth - width * 0.5
    local posY = height * 0.5 - row * cellHeight
    return posX, posY
end
