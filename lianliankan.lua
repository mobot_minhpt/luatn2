--参考http://blog.csdn.net/u013149325/article/details/17359087
CLianLianKanRound = class()
RegistClassMember(CLianLianKanRound, "m_Grid")
RegistClassMember(CLianLianKanRound, "m_Lines")
RegistClassMember(CLianLianKanRound, "m_Row")
RegistClassMember(CLianLianKanRound, "m_Column")
RegistClassMember(CLianLianKanRound, "m_Matrix")
RegistClassMember(CLianLianKanRound, "m_Templates")
RegistClassMember(CLianLianKanRound, "m_PairNum")
RegistClassMember(CLianLianKanRound, "m_RandomNum")

--这里的x其实是行坐标，y是列坐标
function CLianLianKanRound:Ctor()
    --self:Init(Lua_LianLianKan_Setting["Matrix"].Value,Lua_LianLianKan_Setting["EffectTemplate"].Value)
end

function CLianLianKanRound:Init(matrixStr,templateStr)
    self.m_RandomNum=100
    local raw = matrixStr

    self.m_Matrix={}
    for v1 in string.gmatch( raw,"([%d%p]+);?" ) do
        local t={}
        table.insert( self.m_Matrix,t)
        for v2 in string.gmatch( v1,"(%d+),?" ) do
            table.insert(t,tonumber(v2))
        end
    end
    self.m_Row=table.getn(self.m_Matrix)
    if self.m_Row>0 then
        self.m_Column=table.getn(self.m_Matrix[1])
    end


    self.m_PairNum=0
    for i1,v1 in ipairs(self.m_Matrix) do
        for i2,v2 in ipairs(v1) do
            if v2>0 then
                self.m_PairNum=self.m_PairNum+1
            end
        end
    end
    --一共有多少对
    self.m_PairNum=self.m_PairNum/2
    -- print("pair num",self.m_PairNum)

    self.m_Grid={}
    for i=1,self.m_Row do--多少行
        self.m_Grid[i] = self.m_Grid[i] or {}
        for j=1,self.m_Column do--多少列
            local val=0
            self.m_Grid[i][j]={x=i,y=j,type=val,visible=val>0,}
        end
    end
    self.m_Lines={}

    self.m_Templates={}
    for id,num in string.gmatch(templateStr,"(%d+),(%d+);?" ) do
        -- table.insert(self.m_Templates,tonumber(v))
        self.m_Templates[tonumber(id)]=tonumber(num)
    end

end

-- function CLianLianKanRound:SetRandomNum(p)

-- end
-- function CLianLianKanRound:Init(row,column)
--     self.m_Grid={}
--     for i=1,row do--多少行
--         self.m_Grid[i] = self.m_Grid[i] or {}
--         for j=1,column do--多少列
--             local val=0
--             self.m_Grid[i][j]={x=i,y=j,type=val,visible=val>0,}
--         end
--     end
--     self.m_Row=row
--     self.m_Column=column
--     self.m_Lines={}
-- end

function CLianLianKanRound:Remove(x,y)
    local t=self.m_Grid[x][y]
    t.type=0
    t.visible=false
end
function CLianLianKanRound:RemoveAll()
    for i=1,self.m_Row do
        for j=1,self.m_Column do
            local item=self.m_Grid[i][j]
            item.type=0
            item.visible=false
        end
    end
end
function CLianLianKanRound:Add(x,y,type)
    local t=self.m_Grid[x][y]
    t.type=type
    t.visible=type>0
end

function CLianLianKanRound:GetLines()
    return self.m_Lines
end

function CLianLianKanRound:GetPath(startX,startY,endX,endY)
    if not self:CanMatch(startX,startY,endX,endY) then
        return nil
    end

    local path={}
    --确定第一个点
    table.insert( path,{startX,startY} )

    local lines={}
    for i,v in ipairs(self.m_Lines) do
        table.insert( lines,v )
    end
    local count=0
    while #lines>0 do
        local lines2={}
        local lastPoint=path[#path]
        --确定下一个点
        for i,v in ipairs(lines) do
            if lastPoint[1]==v[1] and lastPoint[2]==v[2] then
                table.insert( path,{v[3],v[4]} )
            elseif lastPoint[1]==v[3] and lastPoint[2]==v[4] then
                table.insert( path,{v[1],v[2]} )
            else
                table.insert( lines2,v )
            end
        end
        lines=lines2
        count=count+1
        if count>=10 then 
	        break
        end
    end
    return path
end

function CLianLianKanRound:Debug()
    for i,v in ipairs(self.m_Grid) do
        local line=""
        for j,item in ipairs(v) do
            line=line.." "..item.type
        end
    end
end



function CLianLianKanRound:MatchDirect(a,b)
    if not (a.x==b.x or a.y==b.y) then
        return false
    end
    -- print("MatchDirect",a.x,a.y,b.x,b.y)
    --a b的横坐标相同时
    local yMatch=true
    if a.x==b.x then
        if a.y>b.y then
            if b.y+1==a.y then--相邻
                return true
            else
                for i=b.y+1,a.y-1 do
                    if self.m_Grid[a.x][i].visible==true then
                        yMatch=false
                    end
                end
            end
        end
        if b.y>a.y then
            if a.y+1==b.y then--相邻
                return true
            else
                for i=a.y+1,b.y-1 do
                    if self.m_Grid[a.x][i].visible==true then
                        yMatch=false
                    end
                end
            end
        end
    end
    local xMatch=true
    if a.y==b.y then
        if a.x>b.x then
            if a.x==b.x+1 then
                return true
            else
                for i=b.x+1,a.x-1 do
                    if self.m_Grid[i][a.y].visible==true then
                        xMatch=false
                    end
                end
            end
        end
        if b.x>a.x then
            if b.x==a.x+1 then
                return true
            else
                for i=a.x+1,b.x-1 do
                    if self.m_Grid[i][a.y].visible==true then
                        xMatch=false
                    end
                end
            end
        end
    end
    return xMatch and yMatch
end

function CLianLianKanRound:MatchOneCorner(a,b)
    local node=self.m_Grid[a.x][b.y]
    if node.visible==false and self:MatchDirect(a,node) and self:MatchDirect(node,b) then
        table.insert( self.m_Lines,{a.x,a.y,node.x,node.y} )
        table.insert( self.m_Lines,{node.x,node.y,b.x,b.y} )
        return true
    end
    node=self.m_Grid[b.x][a.y]
    if node.visible==false and self:MatchDirect(a,node) and self:MatchDirect(node,b) then
        table.insert( self.m_Lines,{a.x,a.y,node.x,node.y} )
        table.insert( self.m_Lines,{node.x,node.y,b.x,b.y} )
        return true
    end
    return false
end

function CLianLianKanRound:MatchTwoCorner(a,b)
    local i,j
    j=a.y
    if a.x>1 then
        for i=a.x-1,1,-1 do
            local node=self.m_Grid[i][j]
            if node.visible==true then 
                break
            elseif self:MatchOneCorner(b,node) then
                table.insert( self.m_Lines,{a.x,a.y,node.x,node.y} )
                return true
            end
        end
    end
    j=a.y
    for i=a.x+1,self.m_Row do
        local node=self.m_Grid[i][j]
        if node.visible==true then
            break
        elseif self:MatchOneCorner(b,node) then
            table.insert( self.m_Lines,{a.x,a.y,node.x,node.y} )
            return true
        end
    end
    i=a.x
    if a.y>1 then
        for j=a.y-1,1,-1 do
            local node=self.m_Grid[i][j]
            if node.visible==true then
                break
            elseif self:MatchOneCorner(b,node) then
                table.insert( self.m_Lines,{a.x,a.y,node.x,node.y} )
                return true
            end
        end
    end
    i=a.x
    for j=a.y+1,self.m_Column do
        local node=self.m_Grid[i][j]
        if node.visible==true then
            break
        elseif self:MatchOneCorner(b,node) then
            table.insert( self.m_Lines,{a.x,a.y,node.x,node.y} )
            return true
        end
    end
    return false
end

function CLianLianKanRound:Match(a,b)
	if not a or not b then return false end
	if a.type <= 0 or b.type <= 0 then return false end

    self.m_Lines={}
    if a.type~=b.type then
        return false
    end

    if self:MatchDirect(a,b) then
        table.insert( self.m_Lines,{a.x,a.y,b.x,b.y} )
        return true
    elseif self:MatchOneCorner(a,b) then
        return true
    elseif self:MatchTwoCorner(a,b) then
        return true
    end
    return false
end

function CLianLianKanRound:CanMatch(x1,y1,x2,y2)
    local a=self.m_Grid[x1] and self.m_Grid[x1][y1]
    local b=self.m_Grid[x2] and self.m_Grid[x2][y2]

    return self:Match(a,b)
end

function CLianLianKanRound:HaveSolution()
    --比赛结束了
    if self:IsRoundDone() then
        return true
    end
    for i=1,self.m_Row do--多少行
        for j=1,self.m_Column do--多少列
            local item=self.m_Grid[i][j]
            if item.visible==true then
                --查看是否有解
                if self:LookForSolution(i,j) then
                    return true
                end
            end
        end
    end
    return false
end
function CLianLianKanRound:LookForSolution(rowIndex,columnIndex)
    local a=self.m_Grid[rowIndex][columnIndex]
    for i=rowIndex,self.m_Row do
        if i==rowIndex then--自己这一行 
            for j=columnIndex+1,self.m_Column do
                local b=self.m_Grid[i][j]
                if b.visible==true then
                    if self:Match(a,b) then
                        return true
                    end
                end
            end
        else--下一行
            for j=1,self.m_Column do
                local b=self.m_Grid[i][j]
                if b.visible==true then
                    if self:Match(a,b) then
                        return true
                    end
                end
            end
        end
    end
    return false
end

--获取棋盘上所有数据
function CLianLianKanRound:GetAllGrid()
    local ret={}
    for i=1,self.m_Row do
        for j=1,self.m_Column do
                local type=self.m_Grid[i][j].type
                table.insert( ret,type )
        end
    end
    -- local debugStr=""
    -- for i,v in ipairs(ret) do
    --     debugStr=debugStr.." "..v
    -- end
    -- print(debugStr)
    return ret
end

--服务器调用 客户端不需要
--用模板随机一下棋盘
function CLianLianKanRound:GeneraterMap()
    local P1=100--随机排序的参数，对难度影响不大
    local P2=self.m_RandomNum--随机打乱的参数，会影响地图的随机性，数值越大越乱，会影响难度
    -- print("random num",P2)
    --TODO
    local keys={}
    -- self.m_Templates
    for k,v in pairs(self.m_Templates) do
        for i=1,v do
            table.insert( keys,k )
            table.insert( keys,k )
        end
    end


    local index=1
    --摆放到地图上
    for i=1,self.m_Row do
        for j=1,self.m_Column do
            if self.m_Matrix[i][j]>0 then
                self.m_Grid[i][j]={x=i,y=j,type=keys[index] or 0,visible=(keys[index] or 0)>0}
                index=index+1
            end
        end
    end
    --开始随机地图 随机100次
    for i=1,P2 do
        local rx1=math.random(1,self.m_Row)
        local ry1=math.random(1,self.m_Column)
        local rx2=math.random(1,self.m_Row)
        local ry2=math.random(1,self.m_Column)
        if rx1==rx2 and ry1==ry2 then
        else
            if self.m_Matrix[rx1][ry1]>0 and self.m_Matrix[rx2][ry2]>0 then
                local t1=self.m_Grid[rx1][ry1]
                local t2=self.m_Grid[rx2][ry2]
                local type1=t1.type
                local type2=t2.type

                t1.type=type2
                t1.visible=t1.type>0

                t2.type=type1
                t2.visible=t2.type>0
            end
        end
    end
end

--服务器调用 客户端不需要
--随机一下现存地图
function CLianLianKanRound:RandomMap()
    --TODO
    --开始随机地图 随机100次
    local P=100
    for i=1,P do
        local rx1=math.random(1,self.m_Row)
        local ry1=math.random(1,self.m_Column)
        local rx2=math.random(1,self.m_Row)
        local ry2=math.random(1,self.m_Column)
        if rx1==rx2 and ry1==ry2 then
        else
            if self.m_Matrix[rx1][ry1]>0 and self.m_Matrix[rx2][ry2]>0 then

                local t1=self.m_Grid[rx1][ry1]
                local t2=self.m_Grid[rx2][ry2]
                local type1=t1.type
                local type2=t2.type

                t1.type=type2
                t1.visible=t1.type>0
                t2.type=type1
                t2.visible=t2.type>0
            end
        end
    end
end

--棋盘是否消除完毕
function CLianLianKanRound:IsRoundDone()
    local done = true
    for i=1,self.m_Row do
        for j=1,self.m_Column do
            if self.m_Matrix[i][j] > 0 and self.m_Grid[i][j].type >0 then
                done = false
                break
            end
        end
        if not done then
            break
        end
    end
    return done
end

function CLianLianKanRound:GetLeftNum()
    local num=0
    for i=1,self.m_Row do
        for j=1,self.m_Column do
            if self.m_Matrix[i][j] > 0 and self.m_Grid[i][j].type >0 then
                num=num+1
            end
        end
    end
    return num
end

function CLianLianKanRound:GetTemplateId(x,y)
    if self.m_Matrix[x][y]>0 and self.m_Grid[x][y] then
        return self.m_Grid[x][y].type or 0
    end
    return 0
end


return CLianLianKanRound
