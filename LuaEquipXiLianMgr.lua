local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"

CLuaEquipXiLianMgr = {}

--高级洗炼
--默认无数量要求
CLuaEquipXiLianMgr.m_countOption = 0
--条件一/条件二
CLuaEquipXiLianMgr.m_OtherOptions = {{},{}}
CLuaEquipXiLianMgr.m_OtherOptionStates = {false,false}--是否选择该条件


function CLuaEquipXiLianMgr.ClearOption()
    CLuaEquipXiLianMgr.m_countOption = 0
    CLuaEquipXiLianMgr.m_OtherOptions = {{},{}}
    CLuaEquipXiLianMgr.m_OtherOptionStates = {false,false}
end
function CLuaEquipXiLianMgr.IsAdvanceXiLianHaveChoice()
    if CLuaEquipXiLianMgr.m_countOption>0 then
        return true
    end

    for i,idTbl in ipairs(CLuaEquipXiLianMgr.m_OtherOptions) do
        if CLuaEquipXiLianMgr.m_OtherOptionStates[i] then
            if next(idTbl) then
                return true
            end
        end
    end
    return false
end


function CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList, extraWordList, isAdvance) 
    local inst = CEquipmentBaptizeMgr.Inst
       local ok = false
        local wordCount = 0
        do
            local i = 0
            while i < fixedWordList.Count do
                if fixedWordList[i] > 0 then
                    wordCount = wordCount + 1
                end
                i = i + 1
            end
        end
        do
            local i = 0
            while i < extraWordList.Count do
                if extraWordList[i] > 0 then
                    wordCount = wordCount + 1
                end
                i = i + 1
            end
        end
        if isAdvance then
            -- if wordCount>CLuaEquipXiLianMgr.m_countOption then
            --     ok = true
            -- end
        else
            --数量满足条件
            CommonDefs.ListIterate(inst.countConditionList, DelegateFactory.Action_object(function (___value) 
                local item = ___value
                if item <= wordCount then
                    ok = true
                end
            end))
        end

        if not ok then
            local fuzzyResult = {}
            local extraWordTbl = {}
            CommonDefs.ListIterate(extraWordList, DelegateFactory.Action_object(function (___value) 
                local item = ___value
                if item > 0 then
                    local fuzzy = (math.floor(item / 100))
                    if fuzzyResult[fuzzy] then
                        fuzzyResult[fuzzy] = fuzzyResult[fuzzy] + 1
                    else
                        fuzzyResult[fuzzy] = 1
                    end
                    extraWordTbl[item] = true
                end
            end))

            if isAdvance then
                --高级洗炼判断
                --先判断数量
                if wordCount>=CLuaEquipXiLianMgr.m_countOption then
                    local t = {}
                    for i,idTbl in ipairs(CLuaEquipXiLianMgr.m_OtherOptions) do
                        if CLuaEquipXiLianMgr.m_OtherOptionStates[i] then
                            local clone = {}
                            for k,v in pairs(idTbl) do
                                if v then
									local design = Word_AutoXiLianWord.GetData(k)
									local wordList = design and design.WordList
									if wordList then
										for j = 0, wordList.Length - 1 do
											clone[wordList[j]] = true
										end
									end
                                end
                            end
                            if next(clone) then
                                table.insert( t, clone)
                            end
                        end
                    end
                    if #t==0 then
                        --没有选任何条件，则必定满足条件
                        ok = true
                    elseif #t==1 then
                        for wordId,v in pairs(t[1]) do
                            if fuzzyResult[wordId] then
                                ok = true
                                break
                            end
                        end
                    elseif #t==2 then
                        local records = {}
                        for k,v in pairs(fuzzyResult) do
                            records[k] = 0
                        end
                        --为了看看词条是否达到条件，满足条件1，对应填1，满足条件2，对应填2，同时满足的话，就是3了
                        --最终看是否有2条满足条件1和2，就看最后满足的数量了
                        for i,words in ipairs(t) do
                            for wordId,v in pairs(words) do
                                if records[wordId] then
                                    records[wordId] = records[wordId]+i
                                end
                            end
                        end
                        --
                        local count1,count2,count3 = 0,0,0

                        for k,v in pairs(records) do
                            if v==1 then
                                count1 = 1--只满足条件1
                            elseif v==2 then
                                count2 = 1--只满足条件2
                            elseif v==3 then
                                if fuzzyResult[k]>1 then
                                    --说明洗出了多条相同的词条
                                    ok = true
                                    break
                                else
                                    --同时满足条件1和条件2，比如条件1是A/B/C，条件2是B/C/D，洗出B/C
                                    count3 = count3 + 1
                                end
                            end
                        end
                        --达到条件
                        if not ok then
                            if count1+count2+count3>1 then
                                ok = true
                            end
                        end
                    end
                end
            else
                CommonDefs.ListIterate(inst.wordConditionList, DelegateFactory.Action_object(function (___value) 
                    local item = ___value
                    if fuzzyResult[item] then
                        ok = true
                    end
                end))
                if not ok then
                    --检查特殊词条，-8
                    CommonDefs.ListIterate(inst.specialWordConditionList, DelegateFactory.Action_object(function (___value) 
                        local item = ___value
                        if extraWordTbl[item] then
                            ok = true
                        end
                    end))
                end
                if not ok then
                    if inst.checkScore and inst.postScore > inst.preScore then
                        ok = true
                    end
                end
            end
        end

    return ok
end



function CLuaEquipXiLianMgr.GetAutoBaptizeParam4() 
    local builder = {}
    local inst = CEquipmentBaptizeMgr.Inst
    do
        local i = 0
        while i < inst.countConditionList.Count do
            table.insert( builder, tostring(inst.countConditionList[i] ))
            i = i + 1
        end
    end
    return table.concat( builder, ",")
end
function CLuaEquipXiLianMgr.GetAutoBaptizeParam5() 
    local builder = {}
    local inst = CEquipmentBaptizeMgr.Inst
    do
        local i = 0
        while i < inst.wordConditionList.Count do
            table.insert( builder, tostring(inst.wordConditionList[i] ))
            i = i + 1
        end
    end
    return table.concat( builder, ",")
end

function CLuaEquipXiLianMgr.GetAdvanceXiLianCountParam()
    return tostring(CLuaEquipXiLianMgr.m_countOption)
end
function CLuaEquipXiLianMgr.GetAutoBaptizeParam6() 
    if CLuaEquipXiLianMgr.m_OtherOptionStates[1] then
        local builder = {}
        for k,v in pairs(CLuaEquipXiLianMgr.m_OtherOptions[1]) do
            if v then
                table.insert( builder, tostring(k))
            end
        end
        return table.concat( builder, ",")
    end
    return ""
end
function CLuaEquipXiLianMgr.GetAutoBaptizeParam7() 
    if CLuaEquipXiLianMgr.m_OtherOptionStates[2] then
        local builder = {}
        for k,v in pairs(CLuaEquipXiLianMgr.m_OtherOptions[2]) do
            if v then
                table.insert( builder, tostring(k))
            end
        end
        return table.concat( builder, ",")
    end
    return ""
end


function CLuaEquipXiLianMgr.InitCondition() 
    local inst = CEquipmentBaptizeMgr.Inst
    CommonDefs.ListClear(inst.countConditionList)
    CommonDefs.ListClear(inst.wordConditionList)
    CommonDefs.ListClear(inst.necessaryConditionList)

    local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize

    local equipTemplate = CEquipmentProcessMgr.Inst.BaptizingEquipmentTemplate
    if equipTemplate ~= nil then
        local countOption = EquipBaptize_WordCountOption.GetData(EnumToInt(equipData.Color))
        if countOption ~= nil then
            if countOption.WordCount ~= nil and countOption.WordCount.Length > 0 then
                do
                    local i = 0
                    while i < countOption.WordCount.Length do
                        CommonDefs.ListAdd(inst.countConditionList, typeof(Int32), countOption.WordCount[i])
                        i = i + 1
                    end
                end
            end
        end
    end
    local option = CEquipmentProcessMgr.Inst:GetWordOption(equipTemplate.Type, equipTemplate.SubType)
    if option ~= nil then
        if option.Word ~= nil and option.Word.Length > 0 then
            do
                local i = 0
                while i < option.Word.Length do
                    CommonDefs.ListAdd(inst.wordConditionList, typeof(UInt32), option.Word[i])
                    i = i + 1
                end
            end
        end
    end
    --137鬼装
    inst:TryAdd137GhostCondition(equipInfo.itemId)

    inst.checkScore = false
end
