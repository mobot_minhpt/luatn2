local CScene = import "L10.Game.CScene"
local CPos = import "L10.Engine.CPos"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaYiRenSiMgr = {}
LuaYiRenSiMgr.LordTempPlayDataKey = 380
LuaYiRenSiMgr.LordSchedulePlayDataKey = 381
LuaYiRenSiMgr.AddLoadSceneFallBackListener = false
LuaYiRenSiMgr.LordFameName = {
    [1] = LocalString.GetString("乌衣巷"),
    [2] = LocalString.GetString("十六楼"),
    [3] = LocalString.GetString("十里秦淮"),
    [4] = LocalString.GetString("商市长街"),
    [5] = LocalString.GetString("三法司")
}

LuaYiRenSiMgr.LordPropertyName = {
    [7] = LocalString.GetString("道德"),
    [8] = LocalString.GetString("智慧"),
    [9] = LocalString.GetString("体质"),
    [10] = LocalString.GetString("心情"),
    [11] = LocalString.GetString("手艺"),
    [12] = LocalString.GetString("仪仗")
}

LuaYiRenSiMgr.LordPropertyKey = {
    ["moneyIndex"] = 1,
    ["WuYiXiangFameIndex"] = 2,
    ["ShiLiuLouFameIndex"] = 3,
    ["ShiLiQingHuaiFameIndex"] = 4,
    ["ShangShiJieKouFameIndex"] = 5,
    ["SanFaSiFameIndex"] = 6,
    ["DaoDeIndex"] = 7,
    ["ZhiHuiIndex"] = 8,
    ["TiZhiIndex"] = 9,
    ["XinQingIndex"] = 10,
    ["ShouYiIndex"] = 11,
    ["YiZhangIndex"] = 12,
    ["subMoneyIndex"] = 13,
    ["isLordIndex"] = 14,
}

function LuaYiRenSiMgr:OnPlayerLogin()
    LuaYiRenSiMgr.lastPlayTransportDateTime = nil
    LuaYiRenSiMgr.showBlueLineFx = false
    --在大富翁有每日任务未完成时, 是否亮主界面节日按钮上的红点
    LuaYiRenSiMgr.showDaFuWenRedPoint = true
    LuaYiRenSiMgr.isActivityAlertFirstSyncDaFuWen = true
    if not LuaYiRenSiMgr.AddLoadSceneFallBackListener then
        g_ScriptEvent:AddListener("StartLoadSceneFallBack", self, "StartLoadScene")
        LuaYiRenSiMgr.AddLoadSceneFallBackListener = true
    end
end

function LuaYiRenSiMgr:LoadingSceneFinished()
    if CScene.MainScene == nil then return end
    local mapid = CScene.MainScene.SceneTemplateId
    if mapid == 16000499 then
        g_MessageMgr:ShowMessage("NANDULORD_ENTER_NANDU_NOFASHION")
    end
end

function LuaYiRenSiMgr:StartLoadScene(args)
    --if not LuaYiRenSiMgr.AddLoadSceneFinishListener then
    --    g_ScriptEvent:AddListener("LoadingSceneFinished", self, "LoadingSceneFinished")
    --    LuaYiRenSiMgr.AddLoadSceneFinishListener = true
    --end
    --
    --local levelName = args[0]
    --if levelName ~= "dafuwen01" then
    --    return
    --end
    --
    --local playAni = false
    --if LuaYiRenSiMgr.lastPlayTransportDateTime then
    --    local now = CServerTimeMgr.Inst:GetZone8Time()
    --    local lastRecord = LuaYiRenSiMgr.lastPlayTransportDateTime
    --    if now.Year ~= lastRecord.Year or now.Month ~= lastRecord.Month or now.Day ~= lastRecord.Day then
    --        playAni = true
    --        LuaYiRenSiMgr.lastPlayTransportDateTime = CServerTimeMgr.Inst:GetZone8Time()
    --    end
    --else    
    --    playAni = true
    --    LuaYiRenSiMgr.lastPlayTransportDateTime = CServerTimeMgr.Inst:GetZone8Time()
    --end
    --
    --if playAni then
    --    self:OpenScrollWnd(2)
    --end
end

function LuaYiRenSiMgr:OpenMakeFanWithData(fanData, duration)
    local unpackData = g_MessagePack.unpack_use_float(fanData)
    LuaMakeFanWnd.fanData = unpackData
    LuaMakeFanWnd.duration = duration * 1000
    CUIManager.ShowUI(CLuaUIResources.MakeFanWnd)
end

function LuaYiRenSiMgr:SyncHuiCaiShanData(ret_U)
    g_ScriptEvent:BroadcastInLua("ShowFan")
end

function LuaYiRenSiMgr:SyncYiRenSiFindCluesData(ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    LuaYiRenSiMgr.ReadClues = unpackData.Read
    LuaYiRenSiMgr.FindClues = unpackData.Find
    LuaYiRenSiMgr.PlayAnimationClues = unpackData.PlayAni
    g_ScriptEvent:BroadcastInLua("SyncYiRenSiFindCluesData")
end

function LuaYiRenSiMgr:OpenYiRenSiClue(clueId, ret_U)
    local unpackData = g_MessagePack.unpack(ret_U)
    LuaYiRenSiMgr.ReadClues = unpackData.Read
    LuaYiRenSiMgr.FindClues = unpackData.Find
    LuaYiRenSiMgr.PlayAnimationClues = unpackData.PlayAni

    local cluesTable = {}
    NanDuFanHua_YiRenSiClues.Foreach(function (k, v)
        if cluesTable[v.CluesType] then
            table.insert(cluesTable[v.CluesType].IdList, v.Id)
            table.insert(cluesTable[v.CluesType].DescribeList, v.Describe)
        else
            local srcName = v.CluesName
            local e = string.find(srcName,'%(')
            if e then
                srcName = string.sub(srcName,1,e-1)
            end

            local tbl = {IdList = {v.Id}, Name = srcName, CluesMaterial = v.CluesMaterial, DescribeList = {v.Describe}}
            cluesTable[v.CluesType] = tbl
        end
    end)

    LuaCluesDetailWnd.OpenClueId = clueId
    CUIManager.ShowUI(CLuaUIResources.CluesDetailWnd)
end

function LuaYiRenSiMgr:LiYuZhangDaiFoodCookingResult(cookbookId, isSucc)
    g_ScriptEvent:BroadcastInLua("SyncNanDuFanHuaCookResult", cookbookId, isSucc)
end

function LuaYiRenSiMgr:OpenPaper(paperIdList, firstOpenPaperId)
    LuaNanDuPaperWnd.paperIdList = paperIdList
    LuaNanDuPaperWnd.firstOpenPaperId = firstOpenPaperId
    CUIManager.ShowUI(CLuaUIResources.NanDuPaperWnd)
end

function LuaYiRenSiMgr:OnMainPlayerCreated()
    self.effectIndex = 1
    g_ScriptEvent:AddListener("NanDuFanHua_FindFood_DrawArrowEffect", LuaYiRenSiMgr, "OnNanDuFanHuaDrawEffect")
    g_ScriptEvent:AddListener("NanDuFanHua_FindFood_DrawArrowEffectList", LuaYiRenSiMgr, "OnNanDuFanHuaDrawEffectList")
end

function LuaYiRenSiMgr:OnMainPlayerDestroyed()
    g_ScriptEvent:RemoveListener("NanDuFanHua_FindFood_DrawArrowEffect", LuaYiRenSiMgr, "OnNanDuFanHuaDrawEffect")
    g_ScriptEvent:RemoveListener("NanDuFanHua_FindFood_DrawArrowEffectList", LuaYiRenSiMgr, "OnNanDuFanHuaDrawEffectList")
end

function LuaYiRenSiMgr:OnNanDuFanHuaDrawEffectList(positonsData)
    -- x1, y1, x2, y2, ...
    local positions = g_MessagePack.unpack(positonsData)
    for i = 1, #positions, 2 do
        self:OnNanDuFanHuaDrawEffect(positions[i], positions[i+1])
    end
end

function LuaYiRenSiMgr:OpenScrollWnd(animationType)
    if animationType == 1 then
        LuaNanDuScrollWnd.animationName = "nanduscrollwnd_show"
        LuaNanDuScrollWnd.duration = 10000
        CUIManager.ShowUI(CLuaUIResources.NanDuScrollWnd)        
    elseif animationType == 2 then
        LuaNanDuScrollWnd.animationName = "nanduscrollwnd_chuansuo"
        LuaNanDuScrollWnd.duration = 6000
        CUIManager.ShowUI(CLuaUIResources.NanDuScrollWnd)
    end
end

function LuaYiRenSiMgr:OnNanDuFanHuaDrawEffect(x, y)
    if not CScene.MainScene then
        return false
    end
    
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local target = {}
    LiYuZhangDaiFood_Ingredients.Foreach(function(_, v)
        if v.CopyId == gamePlayId then
            local subStrList = g_LuaUtil:StrSplit(v.CopyTargetPosition, ",")
            for i = 1, #subStrList do
                table.insert(target, tonumber(subStrList[i]))
            end
        end
    end)
    
    local angle = math.atan2(target[2] - y, target[1] - x) * Rad2Deg
    local trackpos = L10.Engine.Utility.GridPos2PixelPos(CreateFromClass(CPos, x, y))
    
    LuaGamePlayMgr:SetMiniMapMarkInfo(self.effectIndex, {
        res = "UI/Texture/FestivalActivity/Festival_YiRenSi/Material/common_arrow_03.mat", -- Monster
        x = x,
        y = y,
        size = {
            {width = 32, height = 32}, -- 小地图
            {width = 36, height = 36}, -- 大地图
        },
        rotZ = angle + 60,
    }, true)
    self.effectIndex = self.effectIndex + 1
    
    local worldVfx = CEffectMgr.Inst:AddWorldPositionFX(88600020, trackpos, angle, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
    RegisterTickOnce(function()
        if worldVfx then
            worldVfx:Destroy()
        end
    end,5000)
end

function LuaYiRenSiMgr:OpenBargain(bargainId)
    LuaNanDuFanHuaBargainWnd.bargainId = bargainId
    CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaBargainWnd)
end

function LuaYiRenSiMgr:OpenMuke(mukeStateid)
    LuaNanDuFanHuaMuKeWnd.m_CurPhase = mukeStateid
    CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaMuKeWnd)
end

function LuaYiRenSiMgr:CheckWorldMapEntrance()
    local isFinishTask = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(NanDuFanHua_LordSetting.GetData().NanDuMapTask)
    local isOverTime = false
    local delTime = NanDuFanHua_LordSetting.GetData().NanDuMapDelTime
    local dd = {}
    for d in string.gmatch(delTime, "%d+") do
        table.insert(dd, tonumber(d))
    end

    local now = CServerTimeMgr.Inst:GetZone8Time()
    local delTimeDate = CreateFromClass(DateTime, dd[1], dd[2], dd[3], dd[4] or 0, dd[5] or 0, dd[6] or 0)
    isOverTime = DateTime.Compare(delTimeDate, now) < 0
    return isFinishTask and (not isOverTime)
end

function LuaYiRenSiMgr:NanDuLordShowAttrChange(details)
    local unpackData = g_MessagePack.unpack(details)
    local attributeList = {}

    local deltaMoney = unpackData[1]
    if deltaMoney ~= 0 then
        local packMessage = self:PackSingeDetial(LocalString.GetString("通宝"), deltaMoney)
        table.insert(attributeList, packMessage)
    end
    local deltaSubMoney = unpackData[2]
    if deltaSubMoney ~= 0 then
        local packMessage = self:PackSingeDetial(LocalString.GetString("负债"), deltaSubMoney)
        table.insert(attributeList, packMessage)
    end
    local fameId, deltaFame = unpackData[3], unpackData[4]
    if deltaFame ~= 0 then
        local name = LuaYiRenSiMgr.LordFameName[fameId]
        local packMessage = self:PackSingeDetial(name, deltaFame)
        table.insert(attributeList, packMessage)
    end
    local meFameId, meDeltaFame = unpackData[5], unpackData[6]
    if meDeltaFame ~= 0 then
        local name = LuaYiRenSiMgr.LordFameName[meFameId]
        local packMessage = self:PackSingeDetial(name, -meDeltaFame)
        table.insert(attributeList, packMessage)
    end
    for i = 12, #unpackData, 2 do
        local propertyName = LuaYiRenSiMgr.LordPropertyName[unpackData[i] + 6]
        local propertyValue = unpackData[i+1]
        if propertyValue ~= 0 then
            local packMessage = self:PackSingeDetial(propertyName, propertyValue)
            table.insert(attributeList, packMessage)
        end
    end
    
    LuaNanDuFanHuaLordChangePropertyWnd.m_showLive2D = unpackData[11]
    LuaNanDuFanHuaLordChangePropertyWnd.m_attributeList = attributeList
    CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordChangePropertyWnd)
    g_ScriptEvent:BroadcastInLua("UpdateNanDuFanHuaLordProperty")
end

function LuaYiRenSiMgr:PackSingeDetial(name, value)
    local packMessage = ""
    if value > 0 then
        packMessage = LocalString.GetString(name) .. "#G+" .. value
    else
        packMessage = LocalString.GetString(name) .. "#R" .. value
    end
    return packMessage
end

function LuaYiRenSiMgr:NanDuLordOpenWnd(wndType)
    if wndType == 2 then
        CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaRepaymentWnd)
    end
end

function LuaYiRenSiMgr:UnlockNanDuJuBaoPenItem(designId)
    LuaNanDuFanHuaLordCiTiaoDetailWnd.citiaoIndex = designId
    CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordCiTiaoDetailWnd)
    g_ScriptEvent:BroadcastInLua("UnlockNanDuJuBaoPenItem", designId)
end

function LuaYiRenSiMgr:GetNanDuJuBaoPenRewardSuccess(fameId)
    g_ScriptEvent:BroadcastInLua("GetNanDuJuBaoPenRewardSuccess", fameId)
end

function LuaYiRenSiMgr:ShowActivityAlert()
    return false
end

function LuaYiRenSiMgr:OpenScroll(type)
    if type == 1 then
        LuaNanDuScrollWnd.animationName = "nanduscrollwnd_chuansuo"
        LuaNanDuScrollWnd.duration = 7000
        CUIManager.ShowUI(CLuaUIResources.NanDuScrollWnd)
    elseif type == 2 then
        LuaNanDuScrollWnd.animationName = "nanduscrollwnd_show"
        LuaNanDuScrollWnd.duration = 10000
        CUIManager.ShowUI(CLuaUIResources.NanDuScrollWnd)
    end
end

function LuaYiRenSiMgr:CalculateLordProsperity(fameName)
    local ciTiaoData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            ciTiaoData = data[4]
        end
    end
    
    local unlock = 0
    local all = 0
    local unlockCiTiao = ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[fameName]-1] and ciTiaoData[LuaYiRenSiMgr.LordPropertyKey[fameName]-1] or 0
    --为了兼顾线上的数据, 已解锁的成就忽略status的限制, 未解锁的成就依然被status限制
    NanDuFanHua_JuBaoPen.Foreach(function(key, data)
        if data.Fame == LuaYiRenSiMgr.LordPropertyKey[fameName]-1 then
            local cfg = NanDuFanHua_JuBaoPen.GetData(key)
            local curCiTiaoUnlock = bit.band(unlockCiTiao, bit.lshift(1, cfg.Idx - 1)) > 0
            if curCiTiaoUnlock then
                all = all + 1
                unlock = unlock + 1
            else
                if cfg.Status ~= 3 then
                    all = all + 1
                end 
            end
        end
    end)
    return unlock, all
end

function LuaYiRenSiMgr:NanDuLordAcceptScheduleSuccess(period, scheduleId, scheduleType)
    g_ScriptEvent:BroadcastInLua("NanDuFanHua_Lord_AddSchedule")
    if scheduleType == 1 then
        CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaLordMainWnd)
        local scheduleData = {}
        if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey) then
            local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordSchedulePlayDataKey)
            if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                local data = g_MessagePack.unpack(playData.Data.Data)
                scheduleData = data
            end
        end
        local scheduleInfo = scheduleData[period]
        if scheduleInfo then
            local taskId = scheduleInfo[6] and scheduleInfo[6] or 0
            if taskId ~= 0 then
                local taskName = Task_Task.GetData(taskId).Display
                g_MessageMgr:ShowMessage("NANDUFANHUA_LORD_GETTASK", taskName)
            end
        end
    end
end

function LuaYiRenSiMgr:PrintTable(node)
    -- to make output beautiful
    local function tab(amt)
        local str = ""
        for i=1,amt do
            str = str .. "\t"
        end
        return str
    end

    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. tab(depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. tab(depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. tab(depth) .. key .. " = '"..tostring(v).."'"
                end

                if (cur_index == size) then
                    output_str = output_str .. "\n" .. tab(depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. tab(depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. tab(depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    --print(output_str)
    return output_str
end

function LuaYiRenSiMgr:NanDuLordBecomeLordSuccess()
    g_MessageMgr:ShowMessage("NANDUFANHUA_LORD_BECOMELORD")
    g_ScriptEvent:BroadcastInLua("NanDuLordBecomeLordSuccess")
    
    local fxIdstring = NanDuFanHua_LordSetting.GetData().BecomeLordUIFX
    local fxLastDuration = NanDuFanHua_LordSetting.GetData().BecomeLordUIFXLast
    local fxIdList = g_LuaUtil:StrSplit(fxIdstring,";")
    for i = 1, #fxIdList-1 do
        local fxId = tonumber(fxIdList[i])
        CCommonUIFxWnd.AddUIFx(fxId, fxLastDuration, false, false)
    end
end 