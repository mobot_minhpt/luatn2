local EnumItemPlace = import "L10.Game.EnumItemPlace"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnTableView = import "L10.UI.QnTableView"
local UIProgressBar = import "UIProgressBar"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaZhuoYaoCaptureView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuoYaoCaptureView, "Portrait1", "Portrait1", CUITexture)
RegistChildComponent(LuaZhuoYaoCaptureView, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "LianHuaTimeLabel", "LianHuaTimeLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "GoToCatchWayButton", "GoToCatchWayButton", GameObject)
RegistChildComponent(LuaZhuoYaoCaptureView, "CatchWayLabel", "CatchWayLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "HideNumOfLessQnCheckBox", "HideNumOfLessQnCheckBox", QnCheckBox)
RegistChildComponent(LuaZhuoYaoCaptureView, "HideHighLevelQnCheckBox", "HideHighLevelQnCheckBox", QnCheckBox)
RegistChildComponent(LuaZhuoYaoCaptureView, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaZhuoYaoCaptureView, "Progress", "Progress", UIProgressBar)
RegistChildComponent(LuaZhuoYaoCaptureView, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "GradeLabel", "GradeLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaZhuoYaoCaptureView, "CatchButton", "CatchButton", GameObject)
RegistChildComponent(LuaZhuoYaoCaptureView, "RadioBox", "RadioBox", QnRadioBox)
RegistChildComponent(LuaZhuoYaoCaptureView, "RightTopView", "RightTopView", GameObject)
RegistChildComponent(LuaZhuoYaoCaptureView, "RightBottomView", "RightBottomView", GameObject)
RegistChildComponent(LuaZhuoYaoCaptureView, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "LimitLabel", "LimitLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "GradeLimitLabel", "GradeLimitLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "CaptureTimesLabel", "CaptureTimesLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "PlayerLevelLimitLabel", "PlayerLevelLimitLabel", UILabel)
RegistChildComponent(LuaZhuoYaoCaptureView, "NoneSelectLabel", "NoneSelectLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhuoYaoCaptureView, "m_YaoGuaiList")
RegistClassMember(LuaZhuoYaoCaptureView, "m_SelectRow")
RegistClassMember(LuaZhuoYaoCaptureView, "m_IsHideNumOfLess")
RegistClassMember(LuaZhuoYaoCaptureView, "m_IsHideHighLevel")
RegistClassMember(LuaZhuoYaoCaptureView, "m_FuLongPieceItemId")
RegistClassMember(LuaZhuoYaoCaptureView, "m_CatchItemId")
RegistClassMember(LuaZhuoYaoCaptureView, "m_CatchRateItemId")
RegistClassMember(LuaZhuoYaoCaptureView, "m_ExtraRate")
RegistClassMember(LuaZhuoYaoCaptureView, "m_PlayerCatchMinLevelFormula")
RegistClassMember(LuaZhuoYaoCaptureView, "m_RadioBoxIndex")
RegistClassMember(LuaZhuoYaoCaptureView, "m_GuideTaskId")
RegistClassMember(LuaZhuoYaoCaptureView, "m_GuideItemId")
RegistClassMember(LuaZhuoYaoCaptureView, "m_GuideTuJianId")
RegistClassMember(LuaZhuoYaoCaptureView, "m_UseGuideItem")
function LuaZhuoYaoCaptureView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.GoToCatchWayButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGoToCatchWayButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.CatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCatchButtonClick()
	end)


    --@endregion EventBind end
	self.HideNumOfLessQnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected )
        self:OnHideNumOfLessQnCheckBoxClick(selected)
    end)

	self.HideHighLevelQnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (selected )
        self:OnHideHighLevelQnCheckBoxClick(selected)
    end)
end

function LuaZhuoYaoCaptureView:Start()
	self.LimitLabel.text = ""
	self.NoneSelectLabel.text = g_MessageMgr:FormatMessage("ZhuoYaoCaptureView_NoneSelectLabel")
	self.NoneSelectLabel.gameObject:SetActive(true)
	self.RightTopView.gameObject:SetActive(false)
	self.RightBottomView.gameObject:SetActive(false)
	self.HideNumOfLessQnCheckBox.Selected = false
	self.HideHighLevelQnCheckBox.Selected = false
	local formulaId = tonumber(ZhuoYao_Setting.GetData("PlayerCatchMinLevel").Value) 
	self.m_PlayerCatchMinLevelFormula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula
	self:InitCatchItemInfo()
	self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_YaoGuaiList and #self.m_YaoGuaiList or 0
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
	self.QnTableView:ReloadData(true, false)
	self.RadioBox.OnSelect= DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnRadioBoxSelect(index)
	end)
	self.RadioBox:ChangeTo(0, true)
	self:OnSyncZhuoYaoLevelAndExp(LuaZhuoYaoMgr.m_Level, LuaZhuoYaoMgr.m_Exp)
	Gac2Gas.QueryZhuoYaoUnlockInfo_Ite()
end

function LuaZhuoYaoCaptureView:InitCatchItemInfo()
	self.m_FuLongPieceItemId, self.m_CatchItemId = LuaZhuoYaoMgr:GetFuLongPieceItemAndCatchItem()
	local catchRateItemIdStr = ZhuoYao_Setting.GetData("CatchRateItemId").Value
	local arr = g_LuaUtil:StrSplit(catchRateItemIdStr,";")
	arr = g_LuaUtil:StrSplit(arr[1],",")
	local rate = 0
	self.m_CatchRateItemId, rate = tonumber(arr[1]),tonumber(arr[2])
	self.m_ExtraRate = math.floor(rate * 100) 
	self.m_GuideTaskId, self.m_GuideItemId, self.m_GuideTuJianId = 0,21021377,100095 -- tonumber(guideSetting[1]), tonumber(guideSetting[1]), tonumber(guideSetting[1])
end

function LuaZhuoYaoCaptureView:ItemAt(item,index)
	item.gameObject.name = tostring(index)
    local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local mark = item.transform:Find("Mark").gameObject
    local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))

	local tujianData = self.m_YaoGuaiList[index + 1]
    if not tujianData then
        return 
    end

	local icon = tujianData.Icon
    local border = item.transform:Find("Border"):GetComponent(typeof(UISprite))
    --border.spriteName = ""
	border.gameObject:SetActive(false)
	mark:SetActive(self:CheckLimitLevel(tujianData) or self:CheckNumOfLess(tujianData)) 

	levelLabel.text = tujianData.Grade
    portrait:LoadNPCPortrait(icon)
end

function LuaZhuoYaoCaptureView:OnSelectAtRow(row)
	self.m_SelectRow = row + 1
    local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
	self.RightTopView.gameObject:SetActive(tujianData ~= nil)
	self.RightBottomView.gameObject:SetActive(tujianData ~= nil)
	self.NoneSelectLabel.gameObject:SetActive(tujianData == nil)
	if not tujianData then
        return 
    end

	local headIcon = tujianData.Icon
	self.Portrait1:LoadNPCPortrait(headIcon)
	self.LevelLabel.text = tujianData.Grade
    self.NameLabel.text = tujianData.Name
	self.LianHuaTimeLabel.text = math.floor(tujianData.Time / 3600) .. LocalString.GetString("小时")
	local maxGrade = math.max(1,tujianData.Grade - 2)
	local isGradeLimit = LuaZhuoYaoMgr.m_Level < maxGrade
	self.GradeLimitLabel.text = SafeStringFormat3("%s%d[ffffff]/%d",isGradeLimit and "[FF0000]" or "[ffffff]", LuaZhuoYaoMgr.m_Level, maxGrade)
	local arr = LuaZhuoYaoMgr.m_YaoGuaiCacthInfoIte[tujianData.ID] 
	self.CaptureTimesLabel.text = SafeStringFormat3("%d/%d", 0, 0)
	if arr and #arr == 2 then
		self.CaptureTimesLabel.text = SafeStringFormat3("%d/%d", arr[1], arr[2])
	end
	Gac2Gas.QueryYaoguaiCatchInfo_Ite(tujianData.ID)
	local myLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
	
	local limitLevel = self.m_PlayerCatchMinLevelFormula and self.m_PlayerCatchMinLevelFormula(nil, nil, {tujianData.Fixinglevel}) or 0
	if limitLevel <= 0 then
		self.PlayerLevelLimitLabel.text = LocalString.GetString("无限制")
	else
		local isPlayerLevelLimit = myLevel < limitLevel
		self.PlayerLevelLimitLabel.text = SafeStringFormat3("%s%d[ffffff]/%d",isPlayerLevelLimit and "[FF0000]" or "[ffffff]", myLevel, limitLevel)
	end
	local getPathdata = ZhuoYao_GetPathInfo.GetData(tujianData.GetPath)
    if getPathdata then
        self.CatchWayLabel.text = SafeStringFormat3(LocalString.GetString("次数获取:%s"),getPathdata.PathName) 
    end

	self:InitRadioBoxAndLimitLabel(tujianData)

	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) and (CGuideMgr.Inst:IsInSubPhase(4) or CGuideMgr.Inst:IsInSubPhase(5)) then
		local item = self.QnTableView:GetItemAtRow(row)
		local guiJiangItem = self:GetGuiJiangItem()
		if row ~= guiJiangItem then
			CGuideMgr.Inst:StartGuide(EnumGuideKey.NewZhuoYaoCaptureView, 3)
		end
	end
end

function LuaZhuoYaoCaptureView:InitRadioBoxAndLimitLabel(tujianData)
	local mlimitLevel, numOfLess = self:CheckLimitLevel(tujianData) , self:CheckNumOfLess(tujianData)
	local myLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
	local limitLevel = self.m_PlayerCatchMinLevelFormula and self.m_PlayerCatchMinLevelFormula(nil, nil, {tujianData.Fixinglevel}) or 0
	local isPlayerLevelLimit = limitLevel > 0 and myLevel < limitLevel
	self:InitRightBottomView(tujianData)
	self.m_UseGuideItem = false
	if tujianData and tujianData.ID == self.m_GuideTuJianId then
		local hasItemCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Task, self.m_GuideItemId)
		self.m_UseGuideItem = hasItemCount > 0
		if self.m_UseGuideItem then
			local btn1, btn2, btn3 = self.RadioBox.m_RadioButtons[0],self.RadioBox.m_RadioButtons[1],self.RadioBox.m_RadioButtons[2]
			btn2.gameObject:SetActive(false)
			btn3.gameObject:SetActive(false)
			local item1 = btn1.transform:Find("Item")
			self:InitCatchItem(item1, self.m_GuideItemId)		
			btn1.transform:Find("CatchRateLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[ffffff]%d%%",100)
		end
	end
	self.RadioBox.gameObject:SetActive((not mlimitLevel and not numOfLess) or self.m_UseGuideItem)
	self.CatchButton.gameObject:SetActive((not mlimitLevel and not numOfLess) or self.m_UseGuideItem)
	self.LimitLabel.gameObject:SetActive((mlimitLevel or numOfLess) and not self.m_UseGuideItem)
	local arr = LuaZhuoYaoMgr.m_YaoGuaiCacthInfoIte[tujianData.ID] 
	if arr and #arr == 2 and arr[1] >= tonumber(ZhuoYao_Setting.GetData("UnlockPerMonsterPerWeek").Value) then
		self.LimitLabel.text = g_MessageMgr:FormatMessage("ZHUOYAO_NOTICE_HAVEGOTTENTHAN10")
	elseif isPlayerLevelLimit then
		self.LimitLabel.text = g_MessageMgr:FormatMessage("ZhuoYaoCaptureView_Limit",LocalString.GetString("玩家等级"),LocalString.GetString("玩家等级"))
	elseif mlimitLevel then
		self.LimitLabel.text = g_MessageMgr:FormatMessage("ZhuoYaoCaptureView_Limit",LocalString.GetString("捉妖等级"),LocalString.GetString("捉妖等级"))
	elseif numOfLess then
		self.LimitLabel.text = g_MessageMgr:FormatMessage("ZhuoYaoCaptureView_Limit",LocalString.GetString("解锁次数"),LocalString.GetString("解锁次数"))
	end
end

function LuaZhuoYaoCaptureView:InitRightBottomView(tujianData)
	local btn1, btn2, btn3 = self.RadioBox.m_RadioButtons[0],self.RadioBox.m_RadioButtons[1],self.RadioBox.m_RadioButtons[2]
	local item1, item2, item3, item4 =  btn1.transform:Find("Item"),btn2.transform:Find("Item1"),btn2.transform:Find("Item2"),btn3.transform:Find("Item")
	local catchRate = LuaZhuoYaoMgr:GetCatchRate(tujianData) 
	btn1.gameObject:SetActive(true)
	btn2.gameObject:SetActive(true)
	btn3.gameObject:SetActive(true)
	self:InitCatchItem(item1, self.m_CatchItemId)
	self:InitCatchItem(item2, self.m_CatchItemId)
	self:InitCatchItem(item3, self.m_CatchRateItemId)
	self:InitCatchItem(item4, self.m_FuLongPieceItemId)
	
	btn1.transform:Find("CatchRateLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[ffffff]%d%%",catchRate * 100)
	btn2.transform:Find("CatchRateLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[ffffff]%d%%([03B836]+%d%%[ffffff])",catchRate* 100,self.m_ExtraRate)
	btn3.transform:Find("CatchRateLabel"):GetComponent(typeof(UILabel)).text = "[FFB74C]100%"
end

function LuaZhuoYaoCaptureView:InitCatchItem(item, itemId)
	local icon = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local itemGetLabel = item.transform:Find("ItemGetLabel")

	local itemData = Item_Item.GetData(itemId)
	local itemCount = CItemMgr.Inst:GetItemCount(itemId)
	if itemId == self.m_GuideItemId then
		itemCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Task, self.m_GuideItemId)
	end
	icon:LoadMaterial(itemData.Icon)
	numLabel.text = SafeStringFormat3("%s%d[ffffff]/1",itemCount == 0 and "[ff0000]" or "[ffffff]",itemCount) 
	nameLabel.text = itemData.Name
	itemGetLabel.gameObject:SetActive(itemCount == 0)

	UIEventListener.Get(itemGetLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView)then
			return
		end
	    CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, item.transform, CTooltipAlignType.Left)
	end)

	UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView)then
			return
		end
		CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
	end)
end

function LuaZhuoYaoCaptureView:OnEnable()
    g_ScriptEvent:AddListener("OnSyncZhuoYaoLevelAndExp", self, "OnSyncZhuoYaoLevelAndExp")
	g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:AddListener("OnSyncYaoGuaiCacthInfo_Ite", self, "OnSyncYaoGuaiCacthInfo_Ite")
	g_ScriptEvent:AddListener("OnSyncZhuoYaoUnlockInfo_Ite", self, "OnSyncZhuoYaoUnlockInfo_Ite")
end

function LuaZhuoYaoCaptureView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncZhuoYaoLevelAndExp", self, "OnSyncZhuoYaoLevelAndExp")
	g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:RemoveListener("OnSyncYaoGuaiCacthInfo_Ite", self, "OnSyncYaoGuaiCacthInfo_Ite")
	g_ScriptEvent:RemoveListener("OnSyncZhuoYaoUnlockInfo_Ite", self, "OnSyncZhuoYaoUnlockInfo_Ite")
end

function LuaZhuoYaoCaptureView:OnSyncYaoGuaiCacthInfo_Ite(yaoguaiTempId, catchTimes, unlockTimes)
	local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
	local id = yaoguaiTempId
	if tujianData.ID == id then
		self.CaptureTimesLabel.text = SafeStringFormat3("%d/%d", catchTimes, unlockTimes)
		self:InitRadioBoxAndLimitLabel(tujianData)
	end
	local item = self.QnTableView:GetItemAtRow(self.m_SelectRow - 1) 
	if item then
		self:ItemAt(item,self.m_SelectRow - 1)
	end
end

function LuaZhuoYaoCaptureView:OnSyncZhuoYaoUnlockInfo_Ite(level, exp)
	self:InitYaoGuaiList()
end

function LuaZhuoYaoCaptureView:InitYaoGuaiList()
	self.m_YaoGuaiList = {}
	ZhuoYao_TuJian.ForeachKey(function (k)
		local data = ZhuoYao_TuJian.GetData(k)
		if data.Status == 0 then
			local hide = false
			if self.m_IsHideHighLevel and self:CheckLimitLevel(data) then
				hide = true
			end
			if self.m_IsHideNumOfLess and self:CheckNumOfLess(data) then
				hide = true
			end
			if not hide then
				table.insert(self.m_YaoGuaiList, data)
			end
		end
	end)
	table.sort(self.m_YaoGuaiList,function (a,b)
		if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) then
			if self.m_GuideTuJianId == a.ID then
				return true
			elseif self.m_GuideTuJianId == b.ID then
				return false
			end
		end
		local canCatch1 = not (self:CheckLimitLevel(a) or self:CheckNumOfLess(a))
		local canCatch2 = not (self:CheckLimitLevel(b) or self:CheckNumOfLess(b))
		if canCatch1 and not canCatch2 then
			return true
		elseif not canCatch1 and canCatch2 then
			return false
		end
		return a.Grade > b.Grade
	end)
	self.NoneSelectLabel.gameObject:SetActive(true)
	self.RightTopView.gameObject:SetActive(false)
	self.RightBottomView.gameObject:SetActive(false)
	self.QnTableView:ReloadData(true, false)
	self.QnTableView:SetSelectRow(0, true)
end

function LuaZhuoYaoCaptureView:CheckLimitLevel(tujianData)
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) and self.m_GuideTuJianId == tujianData.ID then
		return false
	end
	local maxGrade = math.max(1,tujianData.Grade - 2)
	local res = LuaZhuoYaoMgr.m_Level < maxGrade
	--res = res or LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_OtherSet[tujianData.ID]
	local myLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
	local limitLevel = self.m_PlayerCatchMinLevelFormula and self.m_PlayerCatchMinLevelFormula(nil, nil, {tujianData.Fixinglevel}) or 0
	if limitLevel <= 0 then
		return res
	else
		local isPlayerLevelLimit = myLevel < limitLevel
		res = res or isPlayerLevelLimit
		return res
	end
end

function LuaZhuoYaoCaptureView:CheckNumOfLess(tujianData)
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) and self.m_GuideTuJianId == tujianData.ID then
		return false
	end
	local arr = LuaZhuoYaoMgr.m_YaoGuaiCacthInfoIte[tujianData.ID] 
	if arr and #arr == 2 then
		return (arr[1] >= arr[2]) 
	end
	return LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_NoTimesSet[tujianData.ID]
end

function LuaZhuoYaoCaptureView:SendItem(args)
    local item = CItemMgr.Inst:GetById(args[0])
	if item and item.IsItem then
        self:RefreshCatchItemNumLabel(item.TemplateId)
    end
end

function LuaZhuoYaoCaptureView:SetItemAt(args)
    local  place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    local item = CItemMgr.Inst:GetById( newItemId)
    if item and item.IsItem then
        self:RefreshCatchItemNumLabel(item.TemplateId)
    end
end

function LuaZhuoYaoCaptureView:RefreshCatchItemNumLabel(templateId)
	if self.m_CatchItemId == templateId then
		local btn1, btn2 = self.RadioBox.m_RadioButtons[0],self.RadioBox.m_RadioButtons[1]
		local item1, item2 =  btn1.transform:Find("Item"),btn2.transform:Find("Item1")
		self:InitCatchItem(item1, self.m_CatchItemId)
		self:InitCatchItem(item2, self.m_CatchItemId)
	elseif self.m_CatchRateItemId == templateId then
		local btn2 = self.RadioBox.m_RadioButtons[1]
		local item3 =  btn2.transform:Find("Item2")
		self:InitCatchItem(item3, self.m_CatchRateItemId)
	elseif self.m_FuLongPieceItemId == templateId then
		local btn3 = self.RadioBox.m_RadioButtons[2]
		local item4 =  btn3.transform:Find("Item")
		self:InitCatchItem(item4, self.m_FuLongPieceItemId)
	end
end

function LuaZhuoYaoCaptureView:OnSyncZhuoYaoLevelAndExp(level, exp)
    self.Progress.value = exp / ZhuoYao_Exp.GetData(level).ExpFull
    self.GradeLabel.text = level
    self.ProgressLabel.text = SafeStringFormat3("%d/%d",exp,ZhuoYao_Exp.GetData(level).ExpFull)
end

--@region UIEvent

function LuaZhuoYaoCaptureView:OnGoToCatchWayButtonClick()
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView)then
		return
	end
	local tujianData = self.m_YaoGuaiList[self.m_SelectRow]

    if tujianData then
		local arr = LuaZhuoYaoMgr.m_YaoGuaiCacthInfoIte[tujianData.ID] 
		if #arr == 2 and arr[1] >= tonumber(ZhuoYao_Setting.GetData("UnlockPerMonsterPerWeek").Value) then
			g_MessageMgr:ShowMessage("ZhuoYao_GoToCatchWay_NumOfLess")
			return
		end
        local getPath = tujianData.GetPath
        LuaZhuoYaoMgr:DoGetpathAction(getPath)
    end
end

function LuaZhuoYaoCaptureView:OnTipButtonClick()
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) then
		return
	end
	g_MessageMgr:ShowMessage("ZhuoYao_Capture_ReadMe")
end

function LuaZhuoYaoCaptureView:OnCatchButtonClick()
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) and (not CGuideMgr.Inst:IsInSubPhase(5)) then
		return
	end
	local isUseTaskItem = false
	local tujianData = self.m_YaoGuaiList[self.m_SelectRow]
	if tujianData and tujianData.ID == self.m_GuideTuJianId then
		local hasItemCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Task,self.m_GuideItemId)
		isUseTaskItem = hasItemCount > 0
	end
	if not isUseTaskItem then
		local itemCount1 = CItemMgr.Inst:GetItemCount(self.m_CatchItemId)
		local itemCount2 = CItemMgr.Inst:GetItemCount(self.m_CatchRateItemId)
		local itemCount3 = CItemMgr.Inst:GetItemCount(self.m_FuLongPieceItemId)
		if self.m_RadioBoxIndex == 0 and itemCount1 == 0 then
			g_MessageMgr:ShowMessage("None_CatchItem_ToCatch")
			return
		elseif self.m_RadioBoxIndex == 1 and (itemCount1 == 0 or itemCount2 == 0) then
			g_MessageMgr:ShowMessage("None_CatchItem_ToCatch")
			return
		elseif self.m_RadioBoxIndex == 2 and itemCount3 == 0 then
			g_MessageMgr:ShowMessage("None_CatchItem_ToCatch")
			return
		end
	end
	if tujianData then
		LuaZhuoYaoMgr.m_ZhuoYaoHuaFuWnd_YaoguaiTempId = tujianData.ID
		LuaZhuoYaoMgr.m_ZhuoYaoHuaFuWnd_ZhuoyaoType = self.m_RadioBoxIndex + 1
		CUIManager.ShowUI(CLuaUIResources.ZhuoYaoHuaFuWnd)
	end
end

function LuaZhuoYaoCaptureView:OnHideNumOfLessQnCheckBoxClick(selected)
	self.m_IsHideNumOfLess = selected
	self:InitYaoGuaiList()
end

function LuaZhuoYaoCaptureView:OnHideHighLevelQnCheckBoxClick(selected)
	self.m_IsHideHighLevel = selected
	self:InitYaoGuaiList()
end

function LuaZhuoYaoCaptureView:OnRadioBoxSelect(index)
	self.m_RadioBoxIndex = index
	if CGuideMgr.Inst:IsInPhase(EnumGuideKey.NewZhuoYaoCaptureView) and CGuideMgr.Inst:IsInSubPhase(5) then
		if self.m_RadioBoxIndex ~= 0 then
			CGuideMgr.Inst:StartGuide(EnumGuideKey.NewZhuoYaoCaptureView, 4)
		end
	end
end
--@endregion UIEvent

--@region Guide
function LuaZhuoYaoCaptureView:GetGuiJiangItem()
	local aimZhuoYaoYaoId = tonumber(ZhuoYao_Setting.GetData("GuideSpecialMonster").Value) 
	local itemIndex = 0
	if self.m_YaoGuaiList then
		for i = 1, #self.m_YaoGuaiList do
			local data = self.m_YaoGuaiList[i]
			if data.ID == aimZhuoYaoYaoId then
				itemIndex = i
			end
		end
	end
	if itemIndex > 0 then
		local item = self.QnTableView:GetItemAtRow(itemIndex - 1)
		return item.gameObject
	end
	return nil
end

function LuaZhuoYaoCaptureView:GetCatchItem1()
	local btn = self.RadioBox.m_RadioButtons[0]
	return btn.gameObject
end
--@endregion Guide