local LuaGameObject=import "LuaGameObject"
local CUIManager = import "L10.UI.CUIManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local NGUIText = import "NGUIText"

CLuaFightingSpiritFavorWnd = class()

RegistClassMember(CLuaFightingSpiritFavorWnd, "m_TableView")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_TableViewDataSource")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_PageButton")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_CurrentPage")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_RankButton")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_SearchButton")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_SearchInput")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_SearchValue")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_SearchIndex")

RegistClassMember(CLuaFightingSpiritFavorWnd, "m_ShowList")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_TotalPage")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_InitPage")
RegistClassMember(CLuaFightingSpiritFavorWnd, "m_TeamsPerPage")

function CLuaFightingSpiritFavorWnd:Init()
	self.m_InitPage = 1
	self.m_TeamsPerPage = 15
	self.m_TotalPage = 1
	self.m_ShowList = {}

	self.m_TableView = LuaGameObject.GetChildNoGC(self.transform, "TableView").tableView
	self.m_PageButton = LuaGameObject.GetChildNoGC(self.transform, "QnIncreseAndDecreaseButton").qnAddSubAndInputButton
	self.m_RankButton = LuaGameObject.GetChildNoGC(self.transform, "RankButton").qnButton
	self.m_SearchButton = LuaGameObject.GetChildNoGC(self.transform, "SearchButton").qnButton
	self.m_SearchInput = LuaGameObject.GetChildNoGC(self.transform, "SearchInput").input

	local function initItem(item, index)
		self:InitItem(item, index)
	end
	self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(#self.m_ShowList, initItem)
	self.m_TableView.m_DataSource = self.m_TableViewDataSource
	self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
	end)
	self.m_TableView:ReloadData(true, false)

	self.m_CurrentPage = self.m_InitPage
	self.m_PageButton.ImmediateCallBackWhenInput = false
	self.m_PageButton:SetMinMax(1, self.m_TotalPage, 1)
	self.m_PageButton:SetValue(self.m_CurrentPage, false)
	self.m_PageButton:OverrideText(SafeStringFormat3("%d/%d", self.m_CurrentPage, self.m_TotalPage))
	self.m_PageButton.onValueChanged = DelegateFactory.Action_uint(function(page)
		self.m_CurrentPage = page
		self:FreshPage()
	end)

	self.m_RankButton.OnClick = DelegateFactory.Action_QnButton(function(button)
		CUIManager.ShowUI(CLuaUIResources.FightingSpiritFavorRankWnd)
	end)

	self.m_SearchButton.OnClick = DelegateFactory.Action_QnButton(function(button)
		self:DoSearch()	
	end)

	self:OnTeamListReady()
end

function CLuaFightingSpiritFavorWnd:OnEnable()
	g_ScriptEvent:AddListener("RequestDouHunPraiseServerListResult", self, "OnTeamListReady")
	g_ScriptEvent:AddListener("RequestDouHunPraisePlayerSuccess", self, "OnFavorPlayerSuccess")
end

function CLuaFightingSpiritFavorWnd:OnDisable()
	g_ScriptEvent:RemoveListener("RequestDouHunPraiseServerListResult", self, "OnTeamListReady")
	g_ScriptEvent:RemoveListener("RequestDouHunPraisePlayerSuccess", self, "OnFavorPlayerSuccess")
end

function CLuaFightingSpiritFavorWnd:OnFavorPlayerSuccess(targetPlayerId, targetServerId)
	for _, data in ipairs(CLuaFightingSpiritMgr.m_FavorTeamList) do
		if data.groupId == targetServerId then
			data.favorCount = data.favorCount + 1
			data.bFavor = true
		end
	end
	self:FreshPage()
end

function CLuaFightingSpiritFavorWnd:OnTeamListReady()
	local teamList = CLuaFightingSpiritMgr.m_FavorTeamList
	self.m_TotalPage = math.max(1, math.ceil(#teamList / self.m_TeamsPerPage))
	self.m_CurrentPage = math.max(1, math.min(self.m_CurrentPage, self.m_TotalPage))
	self.m_SearchValue = nil
	self.m_SearchIndex = nil
	
	self.m_PageButton:SetMinMax(1, self.m_TotalPage, 1)
	self.m_PageButton:SetValue(self.m_CurrentPage, false)
	self:FreshPage()
end

function CLuaFightingSpiritFavorWnd:FreshPage()
	self.m_CurrentPage = math.max(1, math.min(self.m_CurrentPage, self.m_TotalPage))
	self.m_PageButton:OverrideText(SafeStringFormat3("%d/%d", self.m_CurrentPage, self.m_TotalPage))
	-- 刷新当前页
	local teamList = CLuaFightingSpiritMgr.m_FavorTeamList
	local startIndex = (self.m_CurrentPage - 1) * self.m_TeamsPerPage + 1
	local endIndex = math.min(startIndex + self.m_TeamsPerPage - 1, #teamList)
	self.m_ShowList = {}
	for i = startIndex, endIndex do
		table.insert(self.m_ShowList, teamList[i])
	end
	self.m_TableViewDataSource.count = #self.m_ShowList
	self.m_TableView:ReloadData(true, false)
end

function CLuaFightingSpiritFavorWnd:DoSearch()
	if cs_string.IsNullOrEmpty(self.m_SearchInput.value) then
		self.m_SearchIndex = nil
		self.m_SearchValue = nil
		self:FreshPage()
		return
	else
		-- 不同搜索内容，从头开始搜
		if not self.m_SearchValue or self.m_SearchValue ~= self.m_SearchInput.value then
			self.m_SearchIndex = nil
		end
		self.m_SearchValue = self.m_SearchInput.value
	end

	-- 从当前结果往后搜，处理重复结果
	local teamList = CLuaFightingSpiritMgr.m_FavorTeamList
	local startIndex = self.m_SearchIndex and self.m_SearchIndex + 1 or 1
	for i = startIndex, startIndex + #teamList - 1 do
		local index = (i - 1) % #teamList + 1

		local serverInfo = CFightingSpiritMgr.Instance:GetServerInfoFromID(teamList[index].serverId)
		local serverName = serverInfo and serverInfo.ServerName or ""
		local temp = string.match(serverName, self.m_SearchValue)
		if temp and temp ~= "" then
			self.m_SearchIndex = index
			local page = math.max(1, math.ceil(self.m_SearchIndex / self.m_TeamsPerPage))
			self.m_CurrentPage = page
			self.m_PageButton:SetValue(self.m_CurrentPage, false)
			self:FreshPage()
			break
		end
	end
end

function CLuaFightingSpiritFavorWnd:InitItem(item, index)
	local tf = item.transform
	local favorSprite = LuaGameObject.GetChildNoGC(tf, "FavorSprite").sprite
	local countLabel = LuaGameObject.GetChildNoGC(tf, "CountLabel").label
	local teamNameLabel = LuaGameObject.GetChildNoGC(tf, "TeamLabel").label
	local serverNameLabel = LuaGameObject.GetChildNoGC(tf, "ServerLabel").label
	local favorButton = LuaGameObject.GetChildNoGC(tf, "FavorButton").qnButton
	local searchSprite = LuaGameObject.GetChildNoGC(tf, "SearchSprite").sprite
	local lightSpriteGO = LuaGameObject.GetChildNoGC(tf, "BackSpriteLight").gameObject

	local info = self.m_ShowList[index + 1]
	if info.bFavor then
		lightSpriteGO:SetActive(true)
		favorSprite.spriteName = "personalspacewnd_heart_2"
	else
		lightSpriteGO:SetActive(false)
		favorSprite.spriteName = "personalspacewnd_heart_1"
	end

	if CClientMainPlayer.Inst and info.serverId == CClientMainPlayer.Inst:GetMyServerId() then
		serverNameLabel.color = Color.green
	else
		serverNameLabel.color = NGUIText.ParseColor24("A4FFFF", 0)
	end
	if info.bIsTeamMember then
		teamNameLabel.color = Color.green
	else
		teamNameLabel.color = Color.white
	end

	countLabel.text = tostring(info.favorCount) or ""
	local serverInfo = CFightingSpiritMgr.Instance:GetServerInfoFromID(info.serverId)

	serverNameLabel.text = ""

	local serverName = serverInfo and serverInfo.ServerName or "" 

	if info.groupName == 1 then
		serverNameLabel.text = SafeStringFormat3(LocalString.GetString("%s·[c][fb3ffd]乾[/c]"), serverName)
	elseif info.groupName == 2 then
		serverNameLabel.text =  SafeStringFormat3(LocalString.GetString("%s·[c][edb743]坤[/c]"), serverName)
	end
	teamNameLabel.text = SafeStringFormat3(LocalString.GetString("%s的队"),info.leaderName)

	favorButton.OnClick = DelegateFactory.Action_QnButton(function(button)
		CLuaFightingSpiritMgr.m_FavorServerId = info.serverId
		CLuaFightingSpiritMgr.m_FavorInfo = info
		CLuaFightingSpiritMgr.m_FavorGroupId = info.groupId
		print("CLuaFightingSpiritMgr.m_FavorServerId", CLuaFightingSpiritMgr.m_FavorServerId)
		CUIManager.ShowUI(CLuaUIResources.FightingSpiritFavorTeamWnd)
	end)

	local teamList = CLuaFightingSpiritMgr.m_FavorTeamList
	if self.m_SearchIndex and teamList[self.m_SearchIndex] and teamList[self.m_SearchIndex].serverId == info.serverId then
		searchSprite.gameObject:SetActive(true)
	else
		searchSprite.gameObject:SetActive(false)
	end
end

function CLuaFightingSpiritFavorWnd:OnSelectAtRow(row)
end

return CLuaFightingSpiritFavorWnd
