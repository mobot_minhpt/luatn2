local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Animation = import "UnityEngine.Animation"

LuaGuoQing2023MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQing2023MainWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaGuoQing2023MainWnd, "JingYuanBtn", GameObject)
RegistChildComponent(LuaGuoQing2023MainWnd, "ProtectHZBtn", GameObject)
RegistChildComponent(LuaGuoQing2023MainWnd, "MallBtn", GameObject)
RegistChildComponent(LuaGuoQing2023MainWnd, "DailyRewardBtn", GameObject)
RegistChildComponent(LuaGuoQing2023MainWnd, "JingYanRainBtn", GameObject)
RegistChildComponent(LuaGuoQing2023MainWnd, "SignBtn", GameObject)
RegistChildComponent(LuaGuoQing2023MainWnd, "LiQuanBtn", GameObject)

RegistChildComponent(LuaGuoQing2023MainWnd, "GuoQing2023MainView", GameObject)
RegistChildComponent(LuaGuoQing2023MainWnd, "GuoQing2023JingYuanEnterView", CCommonLuaScript)
RegistChildComponent(LuaGuoQing2023MainWnd, "BackButton", GameObject)

RegistClassMember(LuaGuoQing2023MainWnd, "m_QianDaoTabName")
RegistClassMember(LuaGuoQing2023MainWnd, "m_JingYanRainActivityID")
RegistClassMember(LuaGuoQing2023MainWnd, "m_DailyRewardActivityID")
RegistClassMember(LuaGuoQing2023MainWnd, "m_ProtectHangZhouActivityID")
RegistClassMember(LuaGuoQing2023MainWnd, "m_LiQuanActivityID")
RegistClassMember(LuaGuoQing2023MainWnd, "m_QianDaoActivityID")
RegistClassMember(LuaGuoQing2023MainWnd, "m_Animation")

RegistClassMember(LuaGuoQing2023MainWnd, "isMainView")
--@endregion RegistChildComponent end

function LuaGuoQing2023MainWnd:Awake()
    self.m_Animation = self.gameObject:GetComponent(typeof(Animation))
    UIEventListener.Get(self.MallBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnMallBtnClick()
    end)
    UIEventListener.Get(self.DailyRewardBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDailyRewardBtnClick()
    end)
    UIEventListener.Get(self.BackButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnBackButtonClick()
    end)
    UIEventListener.Get(self.LiQuanBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnLiQuanBtnClick()
    end)
end

function LuaGuoQing2023MainWnd:Init()
    self.isMainView = true
    self:ShowMainView()
    LuaActivityRedDotMgr:OnRedDotClicked(75)
end

function LuaGuoQing2023MainWnd:ShowMainView()
    self.GuoQing2023MainView.gameObject:SetActive(true)
    self.GuoQing2023JingYuanEnterView.gameObject:SetActive(false)

    local setData = GuoQing2023_Setting.GetData()
    local jieRiData = Task_JieRiGroup.GetData(setData.JieRiGroupId)
    self.TimeLabel.text = jieRiData.ActivityTime
    self:InitJingYanRainBtn(setData.JingYanRainTimeList)
    -- self:InitSignBtn()
    
    local info = LuaGuoQing2023Mgr.m_GuoQing2023Info
    self:PlayOpenAnim(info.jyOpenMatch)
    self:InitJingYuanBtn(info.jyOpen, info.jyOpenMatch, info.jyPlayEnd, info.jyTimes)
    self:InitProtectHZBtn(info.hzOpen, info.hzPlayEnd, info.hzTimes, info.hzNextTime)

    self.m_QianDaoTabName = setData.QianDaoTabName
    self.m_JingYanRainActivityID = setData.JingYanRainActivityID
    self.m_DailyRewardActivityID = setData.DailyRewardActivityID
    self.m_ProtectHangZhouActivityID = setData.ProtectHangZhouActivityID
    self.m_LiQuanActivityID = setData.LiQuanActivityID
    self.m_QianDaoActivityID = setData.QianDaoActivityID
end

function LuaGuoQing2023MainWnd:PlayOpenAnim(jyOpenMatch)
    if jyOpenMatch then
        self:PlayAnimation("guoqing2023mainwnd_on_show", 1)
    else
        self:PlayAnimation("guoqing2023mainwnd_off_show", 1)
    end
end

function LuaGuoQing2023MainWnd:InitJingYuanBtn(jyOpen, jyOpenMatch, jyPlayEnd, jyTimes)
    local alert = self.JingYuanBtn.transform:Find("Alert").gameObject
    if LuaActivityRedDotMgr:IsRedDot(74) then
        alert:SetActive(true)
        LuaActivityRedDotMgr:OnRedDotClicked(74)
    else
        alert:SetActive(false)
    end  
    local openTip = self.JingYuanBtn.transform:Find("OpenTip")
    openTip.gameObject:SetActive(jyOpenMatch)
    local endLabel = self.JingYuanBtn.transform:Find("EndLabel"):GetComponent(typeof(UILabel))
    local timeLabel = self.JingYuanBtn.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local giftLabel = self.JingYuanBtn.transform:Find("GiftLabel"):GetComponent(typeof(UILabel))
    local fullLabel = self.JingYuanBtn.transform:Find("FullLabel"):GetComponent(typeof(UILabel))
    giftLabel.text = SafeStringFormat3(LocalString.GetString("%d份奖励待获取"), jyTimes)
    endLabel.gameObject:SetActive(jyPlayEnd)
    giftLabel.gameObject:SetActive(not jyPlayEnd and jyTimes ~= 0)
    fullLabel.gameObject:SetActive(not jyPlayEnd and jyTimes == 0)
    local timeStr, isOpen = LuaGuoQing2023Mgr:CheckJingYuanOpen()
    timeLabel.text = SafeStringFormat3(LocalString.GetString("每天%s开放"), timeStr)
    if jyPlayEnd then
        CUICommonDef.SetActive(self.JingYuanBtn, false, true)
        timeLabel.gameObject:SetActive(false)
    else
        UIEventListener.Get(self.JingYuanBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnJingYuanBtnClick()
        end)
    end
end

function LuaGuoQing2023MainWnd:InitProtectHZBtn(hzOpen, hzPlayEnd, hzTimes, hzNextTime)
    local timeLabel = self.ProtectHZBtn.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local countLabel = self.ProtectHZBtn.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    local endLabel = self.ProtectHZBtn.transform:Find("EndLabel"):GetComponent(typeof(UILabel))
    timeLabel.gameObject:SetActive(not hzPlayEnd)
    countLabel.gameObject:SetActive(not hzPlayEnd)
    endLabel.gameObject:SetActive(hzPlayEnd)
    countLabel.text = SafeStringFormat3(LocalString.GetString("剩余挑战机会%d"), hzTimes)
    timeLabel.text = SafeStringFormat3(LocalString.GetString("%s刷新下一波"), hzNextTime)
    if hzPlayEnd then
        CUICommonDef.SetActive(self.ProtectHZBtn, false, true)
    else
        UIEventListener.Get(self.ProtectHZBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnProtectHZBtnClick()
        end)
    end
end

function LuaGuoQing2023MainWnd:InitJingYanRainBtn(timeStr)
    local timeLabel1 = self.JingYanRainBtn.transform:Find("TimeLabel1"):GetComponent(typeof(UILabel))
    local timeLabel2 = self.JingYanRainBtn.transform:Find("TimeLabel2"):GetComponent(typeof(UILabel))
    timeLabel1.text = timeStr[0]
    timeLabel2.text = timeStr[1]
    local hour1,min1 = string.match(timeStr[0],"(%d+):(%d+)")
    local hour2,min2 = string.match(timeStr[1],"(%d+):(%d+)")
    local timeStamp1 = CServerTimeMgr.Inst:GetTodayTimeStampByTime(hour1,min1,0)
    local timeStamp2 = CServerTimeMgr.Inst:GetTodayTimeStampByTime(hour2,min2,0)
    local nowTime = CServerTimeMgr.Inst.timeStamp
    timeLabel1.alpha = timeStamp1 > nowTime and 1 or 0.35
    timeLabel2.alpha = timeStamp2 > nowTime and 1 or 0.35

    UIEventListener.Get(self.JingYanRainBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnJingYanRainBtnClick()
    end)
end

function LuaGuoQing2023MainWnd:InitSignBtn()
    local alert = self.SignBtn.transform:Find("Alert").gameObject
    local holiday = CSigninMgr.Inst.holidayInfo
	local showAlert = holiday and holiday.showSignin and not holiday.hasSignGift and holiday.todayInfo and holiday.todayInfo.Open == 1
    alert:SetActive(showAlert)
    UIEventListener.Get(self.SignBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSignBtnClick()
    end)
end

function LuaGuoQing2023MainWnd:PlayAnimation(clipName, speed)
    local clip = self.m_Animation:get_Item(clipName)
    clip.speed = speed
    clip.time = speed > 0 and 0 or clip.length
    self.m_Animation:Play(clipName)
end

--@region UIEvent
function LuaGuoQing2023MainWnd:OnJingYuanBtnClick()
    self.isMainView = false
    self:PlayAnimation("guoqing2023mainwnd_jingyuanenterview_show", 1)
    self.JingYuanBtn.transform:Find("Alert").gameObject:SetActive(false)
end

function LuaGuoQing2023MainWnd:OnProtectHZBtnClick()
    CLuaScheduleMgr:ShowScheduleInfo(self.m_ProtectHangZhouActivityID)
end

function LuaGuoQing2023MainWnd:OnJingYanRainBtnClick()
    CLuaScheduleMgr.ShowJieRiScheduleInfo(self.m_JingYanRainActivityID, "ZHONGYUANJIE2022_GUSHUNINGZHU_NOTOPEN_TEST")
end

function LuaGuoQing2023MainWnd:OnMallBtnClick()
    CShopMallMgr.ShowLingyuLiBaoShop()
end

function LuaGuoQing2023MainWnd:OnDailyRewardBtnClick()
    CLuaScheduleMgr:ShowScheduleInfo(self.m_DailyRewardActivityID)
end

function LuaGuoQing2023MainWnd:OnSignBtnClick()
    local scheduleInfo = CLuaScheduleMgr:GetScheduleInfo(self.m_QianDaoActivityID)
    if scheduleInfo then
        self.SignBtn.transform:Find("Alert").gameObject:SetActive(false)
        CWelfareMgr.OpenWelfareWnd(self.m_QianDaoTabName)
    else
        CLuaScheduleMgr.ShowJieRiScheduleInfo(self.m_QianDaoActivityID, "ZHONGYUANJIE2022_GUSHUNINGZHU_NOTOPEN_TEST")
    end
end

function LuaGuoQing2023MainWnd:OnLiQuanBtnClick()
    CLuaScheduleMgr:ShowScheduleInfo(self.m_LiQuanActivityID)
end

function LuaGuoQing2023MainWnd:OnBackButtonClick()
    if not self.isMainView then
        self.isMainView = true
        self:PlayAnimation("guoqing2023mainwnd_jingyuanenterview_show", -1)
    end
end
--@endregion UIEvent