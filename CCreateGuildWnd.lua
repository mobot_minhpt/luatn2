-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CCreateGuildWnd = import "L10.UI.CCreateGuildWnd"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Guild_Setting = import "L10.Game.Guild_Setting"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local String = import "System.String"
CCreateGuildWnd.m_OnEnable_CS2LuaHook = function (this) 
    local message = g_MessageMgr:FormatMessage("Guild_Build_Info")
    this.m_CreateInstructionLabel:SetContent(CChatLinkMgr.TranslateToNGUIText(message, false))

    EventManager.AddListenerInternal(EnumEventType.RequestOperationInGuildSucceed, MakeDelegateFromCSFunction(this.OnRequestOperationInGuildSucceed, MakeGenericClass(Action2, String, String), this))

    local cost = Guild_Setting.GetData().guild_build_money
    this.moneyCtrl:SetCost(cost)
end
CCreateGuildWnd.m_OnRequestOperationInGuildSucceed_CS2LuaHook = function (this, operationType, paramStr) 
    if (("CreateGuild") == operationType) then
        CUIManager.CloseUI(CUIResources.GuildWnd)
    end
end
CCreateGuildWnd.m_OnCreateButtonClick_CS2LuaHook = function (this, button) 
    local guildName = this.m_GuildNameInput.Text
    local guildMisson = this.m_GuildAimInput.Text

    guildName = Extensions.RemoveBlank(guildName)
    if Extensions.IsNumeric(guildName) then
        g_MessageMgr:ShowMessage("Guild_Name_Numeric")
        return
    end
    if CommonDefs.StringLength(guildName) == 0 or (string.find(guildName, LocalString.GetString("请输入帮会名称"), 1, true) ~= nil) then
        g_MessageMgr:ShowMessage("GUILD_INPUT_NEED_NAME")
        return
    end

    if CommonDefs.IsSeaMultiLang() then
        local len = CommonDefs.StringLength(guildName)
        -- 帮会名STRING(40)
        if len < Constants.SeaMinCharLimitForGuildName or len > Constants.SeaMaxCharLimitForGuildName then
            g_MessageMgr:ShowMessage("SEA_GUILD_INPUT_NAME_LENGTH", Constants.SeaMinCharLimitForGuildName, Constants.SeaMaxCharLimitForGuildName)
            return
        end
    else
        local len = CUICommonDef.GetStrByteLength(guildName)
        if len < 4 or len > 10 then
            g_MessageMgr:ShowMessage("GUILD_INPUT_NAME_LENGTH")
            return
        end
    end

    if not CWordFilterMgr.Inst:CheckName(guildName) then
        g_MessageMgr:ShowMessage("Guild_Name_Violation")
        return
    end

    len = CUICommonDef.GetStrByteLength(guildMisson)
    if (LocalString.GetString("请输入帮会宗旨") == guildMisson) or len < 2 or len > 200 then
        g_MessageMgr:ShowMessage("Guild_Input_Mission_length")
        return
    end

    if not CWordFilterMgr.Inst:CheckGuildMotto(guildMisson) then
        g_MessageMgr:ShowMessage("Guild_Motto_Violation")
        return
    end

    local cost = Guild_Setting.GetData().guild_build_money

    local responseNum = Guild_Setting.GetData().response_build_number

    local datas = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, cost, 24, responseNum)
    local message = g_MessageMgr:FormatMessage("GUILD_CREATION_CONFIRM", CommonDefs.ListToArray(datas))
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        CLogMgr.Log("length:" .. CommonDefs.StringLength(guildMisson))
        if CommonDefs.StringLength(guildMisson) > 50 then
            local guildMission1 = CommonDefs.StringSubstring2(guildMisson, 0, 50)
            local guildMission2 = CommonDefs.StringSubstring1(guildMisson, 50)
            Gac2Gas.ApplyCreateGuild(guildName, guildMission1)
            Gac2Gas.ApplyCreateGuildEnd(guildName, guildMission2)
        else
            Gac2Gas.ApplyCreateGuildEnd(guildName, guildMisson)
        end
    end), nil, nil, nil, false)
end
