local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local UIRoot = import "UIRoot"
local Screen=import "UnityEngine.Screen"
local Mathf = import "UnityEngine.Mathf"
local TweenPosition = import "TweenPosition"
local TweenAlpha = import "TweenAlpha"
local TweenScale = import "TweenScale"
local CommonDefs = import "L10.Game.CommonDefs"

LuaButterflyCrisisStoryWnd = class()

RegistChildComponent(LuaButterflyCrisisStoryWnd, "Background", UISprite)
RegistChildComponent(LuaButterflyCrisisStoryWnd, "ContentLabel", UILabel)
RegistChildComponent(LuaButterflyCrisisStoryWnd, "ContentScrollView",  CUIRestrictScrollView)

RegistClassMember(LuaButterflyCrisisStoryWnd, "m_Wnd")
RegistClassMember(LuaButterflyCrisisStoryWnd, "PositionTween")
RegistClassMember(LuaButterflyCrisisStoryWnd, "IsPositionTweenFinished")
RegistClassMember(LuaButterflyCrisisStoryWnd, "AlphaTween")
RegistClassMember(LuaButterflyCrisisStoryWnd, "IsAlphaTweenFinished")
RegistClassMember(LuaButterflyCrisisStoryWnd, "ScaleTween")
RegistClassMember(LuaButterflyCrisisStoryWnd, "IsScaleTweenFinished")

function LuaButterflyCrisisStoryWnd:Awake()
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    self.PositionTween = self.transform:GetComponent(typeof(TweenPosition))
    self.PositionTween.enabled = false
    self.AlphaTween = self.transform:GetComponent(typeof(TweenAlpha))
    self.ScaleTween = self.transform:GetComponent(typeof(TweenScale))

    self.IsPositionTweenFinished = false
    self.IsAlphaTweenFinished = false
    self.IsScaleTweenFinished = false
    
end

function LuaButterflyCrisisStoryWnd:Init()
    self.ContentLabel.text = LuaButterflyCrisisMgr.m_Story
    self:Layout()

    CommonDefs.AddEventDelegate(self.PositionTween.onFinished, DelegateFactory.Action(function ()
        self:PositionTweenFinish()
    end))
    
    CommonDefs.AddEventDelegate(self.AlphaTween.onFinished, DelegateFactory.Action(function ()
        self:AlphaTweenFinish()
    end))

    CommonDefs.AddEventDelegate(self.ScaleTween.onFinished, DelegateFactory.Action(function ()
        self:ScaleTweenFinish()
    end))

    LuaUtils.SetPosition(self.PositionTween.transform, LuaButterflyCrisisMgr.m_StoryWorldPos.x, LuaButterflyCrisisMgr.m_StoryWorldPos.y, 0)
    self.PositionTween.from = self.PositionTween.transform.localPosition
    self.PositionTween:Play()
    self.AlphaTween:Play()
    self.ScaleTween:Play()
end

function LuaButterflyCrisisStoryWnd:PositionTweenFinish()
    self.IsAlphaTweenFinished = true
end

function LuaButterflyCrisisStoryWnd:AlphaTweenFinish()
    self.IsPositionTweenFinished = true
end

function LuaButterflyCrisisStoryWnd:ScaleTweenFinish()
    self.IsScaleTweenFinished = true
end

function LuaButterflyCrisisStoryWnd:Layout()
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ContentScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ContentScrollView.panel.bottomAnchor.absolute)
    local height = Mathf.Min(self.ContentLabel.height, 660)
    
    local totalHeight = height + contentTopPadding + contentBottomPadding + (self.ContentScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 80, virtualScreenHeight - 100)
    self.Background:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.ContentScrollView.panel:ResetAndUpdateAnchors()
    self.ContentScrollView:ResetPosition()

end
function LuaButterflyCrisisStoryWnd:Update()
    if self.IsPositionTweenFinished and self.IsAlphaTweenFinished and self.IsScaleTweenFinished then
        self.m_Wnd:ClickThroughToClose()
    end
end

return LuaButterflyCrisisStoryWnd
