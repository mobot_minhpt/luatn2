local CScene=import "L10.Game.CScene"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaChristmas2021Mgr = {}

function LuaChristmas2021Mgr.IsInXingYuPlay()
    if not CScene.MainScene then return false end

    local gamePlayId = CScene.MainScene.GamePlayDesignId
    if gamePlayId == Christmas2021_XingYu.GetData().GamePlayId then
        return true
    end
    return false
end

function LuaChristmas2021Mgr.IsInMJSYPlay()
    if not CScene.MainScene then return false end

    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local isIn = false
    Christmas2021_WishInDream.Foreach(function(key,data)
        if data.GamePlayId == gamePlayId then
            isIn = true
        end
    end)
    return isIn
end

----------------------------------温暖心意-------------------------------------
LuaChristmas2021Mgr.WishType = {
    eMakeWish = 0,
    eDetailWish = 1,
    eWishSucceed = 2,
}

function LuaChristmas2021Mgr:OpenMakeWishWnd()
    LuaChristmas2021Mgr.SingleWishWndType = LuaChristmas2021Mgr.WishType.eMakeWish
    CUIManager.ShowUI(CLuaUIResources.WNXYGiveWishWnd)
end

function LuaChristmas2021Mgr:OpenWishDetailWnd(cardInfo)
    LuaChristmas2021Mgr.SingleWishWndType = LuaChristmas2021Mgr.WishType.eDetailWish
    LuaChristmas2021Mgr.m_CurShowingCardInfo = cardInfo
    CUIManager.ShowUI(CLuaUIResources.WNXYGiveWishWnd)
end

function LuaChristmas2021Mgr:OpenWishSucceedWnd()
    LuaChristmas2021Mgr.SingleWishWndType = LuaChristmas2021Mgr.WishType.eWishSucceed
    CUIManager.ShowUI(CLuaUIResources.WNXYGiveWishWnd)
end

function LuaChristmas2021Mgr:OpenAddGiftWnd(itemId)
    LuaChristmas2021Mgr.CurGiftItemId = itemId
    CUIManager.ShowUI(CLuaUIResources.WNXYAddGiftWnd)
end

function LuaChristmas2021Mgr.OpenGuildWishesWnd()
    LuaChristmas2021Mgr.GuildCardNum = 0
    LuaChristmas2021Mgr.GetPlayerInfoCardNum = 0

    Gac2Gas.QueryGuildChristmas2021Card()
end

-----------------------------------圣诞星语--------------------------------------------
LuaChristmas2021Mgr.StartSetIndex = nil
function LuaChristmas2021Mgr:OpenGuanXingWnd(idx)
    LuaChristmas2021Mgr.StartSetIndex = idx
    CUIManager.ShowUI(CLuaUIResources.SDXYGuanXingWnd)
end

LuaChristmas2021Mgr.WaitOpenGuanXingWndTick = nil
-- 等两秒打开观星界面
function LuaChristmas2021Mgr.Wait2SecondsOpenGuanXingWnd(idx)
    if not CClientMainPlayer.Inst then return end
    local pos = CClientMainPlayer.Inst.RO.Position
    local pickDelegate = DelegateFactory.VoidDelegate(function (p)
        LuaChristmas2021Mgr:OpenGuanXingWnd(idx)
    end)
    LuaChristmas2021Mgr.ShowPickUpWnd(LocalString.GetString("观星"),pos,pickDelegate)
end
-- 吟唱过程中离开观星圈 不打开观星界面
function LuaChristmas2021Mgr.XingYuPlayerLeaveStarRange(idx)
    LuaChristmas2021Mgr.HidePickUpWnd()
end

--星象 是否允许上下旋转
LuaChristmas2021Mgr.s_SwipeUpAndDown = true
--星象 是否允许将正反两面都设置为观星成功
LuaChristmas2021Mgr.s_DoubleFaceSucceed = true

--报名界面
function LuaChristmas2021Mgr.OpenXingYuSignUpWnd()
    Gac2Gas.QueryPlayerIsMatchingXingyu2021()
end
--排行榜界面
function LuaChristmas2021Mgr.OpenXingYuRankWnd()
    Gac2Gas.QueryXingYuRank()
end

function LuaChristmas2021Mgr.OpenSDXYSucceedWnd(rank,duration)
    LuaChristmas2021Mgr.MySDXYRank = rank
    LuaChristmas2021Mgr.MyPassDuration = duration
    CUIManager.ShowUI(CLuaUIResources.SDXYSucceedWnd)
end

function LuaChristmas2021Mgr.GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3(LocalString.GetString("%d小时%d分%d秒"), math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)   
    else
        return SafeStringFormat3(LocalString.GetString("%d分%d秒"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

LuaChristmas2021Mgr.PlayerId2MengDaoInfo = {}
function LuaChristmas2021Mgr.SyncGuildChristmas2021CardResult(allcard_ud)
    LuaChristmas2021Mgr.GuildWishList = {}

    if allcard_ud and allcard_ud.Length > 0 then 
        local list = MsgPackImpl.unpack(allcard_ud)
        if list then 
            LuaChristmas2021Mgr.GuildCardNum = list.Count
            for i=0,list.Count-1,1 do
                local info = list[list.Count - 1 - i]
                local card = {}
                card.Id = info[0]
                card.GuildId = info[1]
                card.PlayerId = info[2]
                card.Msg = info[3]
                card.Gifts = info[4]
                card.Sended = info[5]
                card.SendPlayerId = info[6]
                card.PlayerName = info[7]
                table.insert(LuaChristmas2021Mgr.GuildWishList,card)
            end
        end
    end

    if CUIManager.IsLoaded(CLuaUIResources.WNXYGuildWishesWnd) then
		g_ScriptEvent:BroadcastInLua("SyncGuildChristmas2021CardResult")
	else
		CUIManager.ShowUI(CLuaUIResources.WNXYGuildWishesWnd)
	end
end

function LuaChristmas2021Mgr.ShowPickUpWnd(btntext,pos,pickDelegate)
    LuaSeaFishingMgr.m_CollectionPos = pos
    LuaSeaFishingMgr.m_PickUpWndDelegate = pickDelegate
    LuaSeaFishingMgr.m_PickBtnText = btntext
    if not CUIManager.IsLoaded(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd) then
        CUIManager.ShowUI(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd)
    end
end

function LuaChristmas2021Mgr.HidePickUpWnd()
    if CUIManager.IsLoaded(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd) then
        CUIManager.CloseUI(CLuaUIResources.SpecialHaiDiaoPlayPickUpWnd)
    end
end

function LuaChristmas2021Mgr.OnEnterXingYuPlay()
    if LuaChristmas2021Mgr.IsInXingYuPlay() then
        g_MessageMgr:ShowMessage("2021ChristmasSDXY_RULE")
    end
end

-------------------------------梦境识愿------------------------
--LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId] = {线索idx1,线索idx2...线索idxn}
--LuaChristmas2021Mgr.m_NPC2CorrectIdx[npcId] = 推荐线索Idx
--LuaChristmas2021Mgr.m_GamePlayId2Npc[gameplayid] = npcid
function LuaChristmas2021Mgr.OpenMJSYPlayWnd()
    if not LuaChristmas2021Mgr.m_NPCXianSuoTbl or not next(LuaChristmas2021Mgr.m_NPCXianSuoTbl) then
        LuaChristmas2021Mgr.m_NPCXianSuoTbl = {}
        LuaChristmas2021Mgr.m_NPC2CorrectIdx = {}
        Christmas2021_WishInDream.Foreach(function(Idx,data)
            local npcId = data.NpcTempId
            if not LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId] then
                LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId] = {}
            end
            local xiansuos = LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId]
            table.insert(xiansuos,Idx)

            if data.CorrectItem == 1 then
                LuaChristmas2021Mgr.m_NPC2CorrectIdx[npcId] = Idx
            end
        end)
    end
    CUIManager.ShowUI(CLuaUIResources.MJSYPlayWnd)
end

function LuaChristmas2021Mgr.GetNpcIdByGameMjPlayId(gamePlayId)
    if not LuaChristmas2021Mgr.m_GamePlayId2Npc or not next(LuaChristmas2021Mgr.m_GamePlayId2Npc) then
        LuaChristmas2021Mgr.m_GamePlayId2Npc = {}
        Christmas2021_WishInDream.Foreach(function(Idx,data)
            local npcId = data.NpcTempId
            local playId = data.GamePlayId
            LuaChristmas2021Mgr.m_GamePlayId2Npc[playId] = npcId
        end)
    end
    return LuaChristmas2021Mgr.m_GamePlayId2Npc[gamePlayId]
end

function LuaChristmas2021Mgr.GetXianSuoTblByNpcId(nid)
    if not LuaChristmas2021Mgr.m_NPCXianSuoTbl or not next(LuaChristmas2021Mgr.m_NPCXianSuoTbl) then
        LuaChristmas2021Mgr.m_NPCXianSuoTbl = {}
        LuaChristmas2021Mgr.m_NPC2CorrectIdx = {}
        Christmas2021_WishInDream.Foreach(function(Idx,data)
            local npcId = data.NpcTempId
            if not LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId] then
                LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId] = {}
            end
            local xiansuos = LuaChristmas2021Mgr.m_NPCXianSuoTbl[npcId]
            table.insert(xiansuos,Idx)

            if data.CorrectItem == 1 then
                LuaChristmas2021Mgr.m_NPC2CorrectIdx[npcId] = Idx
            end
        end)
    end
    return LuaChristmas2021Mgr.m_NPCXianSuoTbl[nid]
end

function LuaChristmas2021Mgr:IsDreamWishActive(wishIdx)
	local v = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eDreamWish)
	return v and bit.band(v,bit.lshift(1,wishIdx)) > 0
end

LuaChristmas2021Mgr.m_XianSuoIdxForTip = nil
function LuaChristmas2021Mgr.OpenXianSuoTipInfoWnd(idx)
    LuaChristmas2021Mgr.m_XianSuoIdxForTip = idx
    CUIManager.ShowUI(CLuaUIResources.MJSYInfoWnd)
end
-------------------------------Question and Answer---------------------------
LuaChristmas2021Mgr.m_QuestionId = nil 
LuaChristmas2021Mgr.m_CorrectDelegate = nil 
LuaChristmas2021Mgr.m_MistakeDelegate = nil
LuaChristmas2021Mgr.m_QAWndTitle = ""
function LuaChristmas2021Mgr.OpenQuestionWnd(title,questionId,correctDelegate,mistakeDelegate)
    LuaChristmas2021Mgr.m_QAWndTitle = title
    LuaChristmas2021Mgr.m_QuestionId = questionId
    LuaChristmas2021Mgr.m_CorrectDelegate = correctDelegate
    LuaChristmas2021Mgr.m_MistakeDelegate = mistakeDelegate
    CUIManager.ShowUI(CLuaUIResources.QuestionAndAnswerWnd)
end

function LuaChristmas2021Mgr.EndPlay()
    LuaChristmas2021Mgr.m_LastXingYuLightedStarsList = nil
end
