function invoke(action,...)
	if ENABLE_XLUA then
		return action(...)
	else
		return action:Invoke(...)
	end
end

function import(class_name)
	if ENABLE_XLUA then
		local t = CS
		for name, point in string.gmatch(class_name, "([^.+]+)([%.%+]?)") do
			if t[name] then
				if point == "" then
					return t[name]
				end
			else
				break
			end
			t = t[name]
		end
		error(string.format("import class not found '%s'", class_name))
		return nil
	end
	local t = _G
	for name, point in string.gmatch(class_name, "([^.]+)(%.?)") do
		if t[name] then
			if point == "" then
				return t[name]
			end
		else
			break
		end
		t = t[name]
	end

	local class = luanet.import_type(class_name)
	if not class then
		error(string.format("import class not found '%s'", class_name))
		return
	end

	local t = _G
	for name, point in string.gmatch(class_name, "([^.]+)(%.?)") do
		if point == "" then
			if not t[name] then
				t[name] = class
			end
			break
		else
			if not t[name] then
				t[name] = {}
			end
			t = t[name]
		end
	end
	return class
end

if ENABLE_XLUA then
	xlua.load_assembly("mscorlib")
	xlua.load_assembly("System")
	xlua.load_assembly("System.Core")
	xlua.load_assembly("UnityEngine")
	xlua.load_assembly("Assembly-CSharp-firstpass")
	xlua.load_assembly("Assembly-CSharp")
	xlua.load_assembly("DOTween")
	xlua.load_assembly("zxing.unity")
else
	luanet.load_assembly("mscorlib")
	luanet.load_assembly("System")
	luanet.load_assembly("System.Core")
	luanet.load_assembly("UnityEngine")
	luanet.load_assembly("Assembly-CSharp-firstpass")
	luanet.load_assembly("Assembly-CSharp")
	luanet.load_assembly("DOTween")
	luanet.load_assembly("zxing.unity")

	typeof = luanet.ctype
end
-- import basic types
Boolean = import "System.Boolean"
Byte = import "System.Byte"
SByte = import "System.SByte"
Char = import "System.Char"
UInt16 = import "System.UInt16"
Int16 = import "System.Int16"
UInt32 = import "System.UInt32"
Int32 = import "System.Int32"
UInt64 = import "System.UInt64"
Int64 = import "System.Int64"
Single = import "System.Single"
Double = import "System.Double"
String = import "System.String"

bool = Boolean
byte = Byte
sbyte = SByte
char = Char
ushort = UInt16
short = Int16
uint = UInt32
int = Int32
ulong = UInt64
long = Int64
float = Single
double = Double
cs_string = String

TypeOfBoolean = typeof (Boolean)
TypeOfByte = typeof (Byte)
TypeOfSByte = typeof (SByte)
TypeOfChar = typeof (Char)
TypeOfUInt16 = typeof (UInt16)
TypeOfInt16 = typeof (Int16)
TypeOfUInt32 = typeof (UInt32)
TypeOfInt32 = typeof (Int32)
TypeOfUInt64 = typeof (UInt64)
TypeOfInt64 = typeof (Int64)
TypeOfSingle = typeof (Single)
TypeOfDouble = typeof (Double)
TypeOfString = typeof (String)

-- import basic container
EventManager = import "EventManager"
Array = import "System.Array"
Action0 = import "System.Action"

if not ENABLE_XLUA then
	List = import "System.Collections.Generic.List`1"
	HashSet = import "System.Collections.Generic.HashSet`1"
	Dictionary = import "System.Collections.Generic.Dictionary`2"

	Action1 = import "System.Action`1"
	Action2 = import "System.Action`2"
	Action3 = import "System.Action`3"
	Action4 = import "System.Action`4"
	Action5 = import "Action`5"
	Action6 = import "Action`6"
	Comparison = import "System.Comparison`1"
	Func1 = import "System.Func`1"
	Func2 = import "System.Func`2"
	Func3 = import "System.Func`3"
	Converter = import "System.Converter`2"
	Formula2 = import "L10.Game.Formula`2"
	Formula3 = import "L10.Game.Formula`3"
	Formula4 = import "L10.Game.Formula`4"

	TypeOfList = typeof (List)
	TypeOfHashSet = typeof (HashSet)
	TypeOfDictionary = typeof (Dictionary)
	TypeOfArray = typeof (Array)

	MakeGenericClass = luanet.make_generic_class
	MakeArrayClass = luanet.make_array_class
	CreateFromClass = luanet.create_from_class
	GetBaseClass = luanet.base_class
	GetElemClass = luanet.elem_class
	MakeDelegateFromCSFunction = function(...)
		return luanet.make_delegate_from_csfunction(...)
	end
	EnumFromInt = luanet.enum
	function EnumToInt(enumObj)
		if (type(enumObj) == "number") then
			return enumObj
		else
			return luanet.enumToInt(enumObj)
		end
	end
else
	List = {"System.Collections.Generic.List"}
	HashSet = {"System.Collections.Generic.HashSet"}
	Dictionary = {"System.Collections.Generic.Dictionary"}

	Action1 = {"System.Action"}
	Action2 = {"System.Action"}
	Action3 = {"System.Action"}
	Action4 = {"System.Action"}
	Action5 = {"Action"}
	Action6 = {"Action"}
	Comparison = {"System.Comparison"}
	Func1 = {"System.Func"}
	Func2 = {"System.Func"}
	Func3 = {"System.Func"}

	function MakeGenericClass(class,...)
		if class==List then
			return xlua.import_generic_type(class[1],...)
		elseif class==Dictionary then
			return xlua.import_generic_type(class[1],...)
		elseif class==HashSet then
			return xlua.import_generic_type(class[1],...)
		elseif class==Action1 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Action2 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Action3 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Action4 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Action5 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Action6 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Comparison then
			return xlua.import_generic_type(class[1],...)
		elseif class==Func1 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Func2 then
			return xlua.import_generic_type(class[1],...)
		elseif class==Func3 then
			return xlua.import_generic_type(class[1],...)
		else
			print("error!",class,type(class),...)
			print(debug.traceback("Stack trace"))
		end
	end

	MakeArrayClass = xlua.make_array_class
	CreateFromClass = xlua.create_from_class
	GetBaseClass = xlua.base_class
	GetElemClass = xlua.elem_class

	MakeDelegateFromCSFunction = function(func,...)
		local _,cs_func_idx = debug.getupvalue(func,3)--function bool idx
		return xlua.make_delegate_from_csfunction(cs_func_idx,...)
	end
	function EnumFromInt(EnumClass,i)
		return EnumClass.__CastFrom(i)
	end

	function EnumToInt(enumObj)
		if (type(enumObj) == "number") then
			return enumObj
		else
			return CS.System.Convert.ToInt32(enumObj)
		end
	end

	Formula2 = GetBaseClass(xlua.import_generic_type("L10.Game.Formula",CS.System.Int32,CS.System.Int32))

	-- Formula3 = import "L10.Game.Formula`3"
	-- Formula4 = import "L10.Game.Formula`4"
	-- Formula10 = import "L10.Game.Formula`10"

	TypeOfList = typeof (GetBaseClass(MakeGenericClass(List, Int32)))
	TypeOfHashSet = typeof (GetBaseClass(MakeGenericClass(HashSet, Int32)))
	TypeOfDictionary = typeof (GetBaseClass(MakeGenericClass(Dictionary, Int32, Int32)))
	TypeOfArray = typeof (CS.System.Array)
end

	-- help function
function Table2Class(tbl, class)
	local isTable = type(tbl) == "table"
	local cls = GetBaseClass(class)
	if not cls then
		return tbl
	end
	local t = typeof (cls)
	if t == TypeOfList then
		if isTable then
			return Table2List(tbl, class)
		end
	elseif t == TypeOfHashSet then
		if isTable then
			return Table2HashSet(tbl, class)
		end
	elseif t == TypeOfDictionary then
		if isTable then
			return Table2Dictionary(tbl, class)
		end
	elseif t == TypeOfArray then
		if isTable then
			return Table2Array(tbl, class)
		end
	else
		return tbl
	end
end

function Table2ArrayArray(tbl, class)
	local count = #tbl
	local ret = CreateFromClass(class, count)
	local elemType = GetElemClass(class)
	for i = 1, count do
		ret[i - 1] = Table2Array(tbl[i], elemType)
	end
	return ret
end

function Table2Array(tbl, class)
	local count = #tbl
	local ret = CreateFromClass(class, count)
	local elemType = GetElemClass(class)
	for i = 1, count do
		ret[i - 1] = Table2Class(tbl[i], elemType)
	end
	return ret
end

function Table2ArrayWithCount(tbl, count, class)
	local ret = CreateFromClass(class, count)
	local elemType = GetElemClass(class)
	for i = 1, count do
		if tbl[i] ~= nil then
			ret[i - 1] = Table2Class(tbl[i], elemType)
		else
			ret[i - 1] = nil
		end
	end
	return ret
end

function Table2List(tbl, class)
	local ret = CreateFromClass(class)
	local elemType = typeof(GetElemClass(class))
	for _, v in ipairs(tbl) do
		CommonDefs.ListAdd(ret,elemType,v)
	end
	return ret
end

function Table2Dictionary(tbl, class)
	local ret = CreateFromClass(class)
	local keyType = typeof(GetElemClass(class, 0))
	local valueType = typeof(GetElemClass(class, 1))
	for k, v in pairs(tbl) do
		CommonDefs.DictAdd(ret, keyType, k, valueType, v)
	end
	return ret
end
--refs #315278 by hzzhangzhixiang 似乎被误删掉了，加回来
function Table2HashSet(tbl, class)
	local ret = CreateFromClass(class)
	local elemType = typeof(GetElemClass(class))
	for _, v in ipairs(tbl) do
		CommonDefs.HashSetAdd(ret,elemType,v)
	end
	return ret
end

require("common/common_include")

require "rpc"

-- 补充一个List到lua Table的转换，某些复杂的情况下需要用到（自动翻译那边多一点）
function List2Table(listObject)
	if listObject == nil then
		return nil
	end
	local t = {}
	for i = 0, listObject.Count-1 do
		table.insert(t, listObject[i])
	end
	return t
end
function Array2Table(array)
	if array == nil then
		return nil
	end
	local t = {}
	for i = 0, array.Length-1 do
		table.insert(t, array[i])
	end
	return t
end

function List2Params(listObject)
	local t = {}
	if listObject == nil then
		return nil
	end
	for i = 0, listObject.Count-1 do
		table.insert(t, listObject[i])
	end
	return unpack(t)
end

function ToStringWrap(obj, ...)
	if (type(obj) == "userdata") then
		return obj:ToString(...)
	else
		return tostring(obj)
	end
end

function StringAt(s, i)
	return CommonDefs.StringAt(s, i-1)
end

function NumberComplexToString(n, ...)
	return CommonDefs.NumberComplexToString(n, ...)
end

function StringStartWith(str, prefix)
	if str==nil or prefix==nil then return false end
	return string.sub(str, 1, #prefix) == prefix
end

function round2(num, digitNum)
	local multiply = 10^digitNum
	return math.floor(num*multiply + 0.5)/multiply
end

math.lerp = function(a, b, r)
	return a*(1-r) + b*r
end

math.inverse_lerp = function (a, b, r)
	return a*r + b*(1-r)
end

function DictElePosIncDec(dict, idxType, index, valType, delta)
	local v = CommonDefs.DictGetValue(dict, idxType, index)
	CommonDefs.DictSet(dict, idxType, index, valType, v+delta)
	return v
end

function DictElePreIncDec(dict, idxType, index, valType, delta)
	local v = CommonDefs.DictGetValue(dict, idxType, index)
	CommonDefs.DictSet(dict, idxType, index, valType, v+delta)
	return CommonDefs.DictGetValue(dict, idxType, index)
end

function InitializeDict(dict, keyType, valueType, ...)
	local args = {...}
	for i = 1, #args, 2 do
		CommonDefs.DictAdd(dict, typeof(keyType), args[i], typeof(valueType), args[i+1])
	end
	return dict
end

function InitializeList(list, valueType, ...)
	local args = {...}
	for i = 1, #args do
		CommonDefs.ListAdd(list, typeof(valueType), args[i])
	end
	return list
end

function InitializeListWithArray(list, valueType, array)
	for i = 1, array.Length do
		CommonDefs.ListAdd(list, typeof(valueType), array[i-1])
	end

	return list
end

function InitializeListWithCollection(list, valueType, collection)
	CommonDefs.ListAddRange(list, collection)
	return list
end

function NewStringBuilderWraper(StringBuilder, ...)
	local args = {...}
	if #args == 0 then
		return CreateFromClass(StringBuilder)
	elseif #args == 1 then
		if type(args[1]) == "string" then
			return CommonDefs.StringBuilder_String(args[1])
		else
			return CommonDefs.StringBuilder_int(args[1])
		end
	elseif #args == 2 then
		if type(args[1])=="string" and type(args[2])=="number" then
			return CommonDefs.StringBuilder_String_int(args[1], args[2])
		else
			return CommonDefs.StringBuilder_int_int(args[1], args[2])
		end
	else
		return CreateFromClass(StringBuilder, ...)
	end
end

function UnityEngine_Random(v1, v2)
	if (math.floor(v1) == v1 and math.floor(v2) == v2) then
		return math.random(1, v2-v1) + v1-1	-- 左闭右开
	else
		return math.random(0, 1) * (v2-v1) + v1  -- 浮点数闭区间
	end
end

function StringTrim(s)
	return s:match("^%s*(.-)%s*$")
end

function math_sign(v)
	if v == 0 then
		return 0
	else
		return v/math.abs(v)
	end
end

GenericDelegateInvoke = CommonDefs.GenericDelegateInvoke

GetType = CommonDefs.GetType

TypeIs = CommonDefs.TypeIs

function MultiDimArrayGet(obj, ...)
	local func = CommonDefs.MultiDimArrayGet
	local indexs = Table2Array({...}, MakeArrayClass(Int32))
	return func(obj, indexs)
end

function MultiDimArraySet(obj, ...)
	local func = CommonDefs.MultiDimArraySet
	local indexs = {}
	local args = {...}
	for i = 1, #args-1 do
		table.insert(indexs, args[i])
	end
	local val = args[#args]
	local i = Table2Array(indexs, MakeArrayClass(Int32))
	func(obj, i, val)
end

function TypeAs(object, objType)
	if (object == nil) then return nil end
	if GetType(object).IsPrimitive then
		return CommonDefs.TypeAs(object, objType)
	elseif CommonDefs.TypeIs(object, objType) then
		return object
	else
		return nil
	end
end

function NumberCompareTo(n1, n2)
	if (n1 == n2) then
		return 0
	elseif (n1 > n2) then
		return 1
	else
		return -1
	end
end

PI = 3.14159265359
Deg2Rad = 0.0174533
Rad2Deg = 57.2958

function NumberTruncate(n, digits)
	local mul = 10 ^ digits
	return math.floor(n*mul)/mul
end

function ArrayCopyTo(array1, array2, index)
	local length = array1.Length
	for i = 0, length-1 do
		array2[index + i] = array1[i]
	end
end

function CollectionCopyTo(collection1, array2, index)
	CommonDefs.CollectionCopyTo(collection1, array2, index)
end

require "arkscript"
require "CopyClassFunction"
require "gaccommondef"
require "bazimgr"
require "clientfurniturescene"

local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local Action = import "System.Action"
local DelegateFactory = import "DelegateFactory"

--[[
    @desc:注册一个循环的Tick
    author:{author}
    time:2020-07-14 11:24:46
    --@f:回调函数
	--@interval:时间间隔
    @return:Tick
]]
function RegisterTick(f, interval)
	interval = math.floor(interval)
	if interval <= 0 then
		return
	end
	local delegate = DelegateFactory.Action(f)
	return CTickMgr.Register(delegate, interval, ETickType.Loop)
end
--[[
    @desc:注册一个一次性的Tick 
    author:{author}
    time:2020-07-14 11:25:53
    --@f:回调函数
	--@interval:延迟的时间
    @return:Tick
]]
function RegisterTickOnce(f, interval)
	interval = math.floor(interval)
	if interval <= 0 then
		return
	end
	local delegate = DelegateFactory.Action(f)
	return CTickMgr.Register(delegate, interval, ETickType.Once)
end

--[[
    @desc:去除Tick
    author:{author}
    time:2020-07-14 11:26:42
    --@tick:要去除的TIck
    @return:
]]
function UnRegisterTick(tick)
	if tick then
		if ENABLE_XLUA then
			tick()
		else
			tick:Invoke()
		end
	end
end

--[[
    @desc:注册一个循环执行一定时间的Tick，执行完成后去除注册
    author:{author}
    time:2020-07-14 11:28:08
    --@f:回调函数
	--@interval:时间间隔
	--@last:持续时间
    @return:Tick
]]
function RegisterTickWithDuration(f, interval, last)
	interval = math.floor(interval)
	if interval <= 0 then
		return
	end
	local count = math.floor(last / interval)
	if count <= 0 then
		return
	end

	local tick
	tick = RegisterTick(function()
		f()
		count = count - 1
		if count <= 0 then
			UnRegisterTick(tick)
		end
	end, interval)
	return tick
end

function Debug(...)
	local args = {...}
	local str = ""
	for i=1,#args do
		local v = "nil"
		if args[i] then
			v = tostring(args[i])
		end
		str = str..v.." \t "
	end
	print(debug.traceback(str))
end

----------------------------------------------
-- 如果是 xKey 变量是 nil 就设置全局变量的值
function LuaSetGlobalIfNil(xKey, xValue)
	if(xKey == nil) then error("LuaSetGlobal: key is nil"); return; end
	if(type(xKey) ~= "string") then error("LuaSetGlobal: key is not string, type: " .. type(xKey)); return; end

	if(xValue == nil) then error("LuaSetGlobal: value is nil, use LuaDelGlobal(k)"); return; end

	local lOldValue = rawget(_G, xKey);
	if(lOldValue == nil) then
		rawset(_G, xKey, xValue);
	end
end

function LuaSetGlobal(xKey, xValue)
	if(xKey == nil) then error("LuaSetGlobal: key is nil"); return; end
	if(type(xKey) ~= "string") then error("LuaSetGlobal: key is not string, type: " .. type(xKey)); return; end

	if(xValue == nil) then error("LuaSetGlobal: value is nil, use LuaDelGlobal(k)"); return; end
	rawset(_G, xKey, xValue);
end

-- 全局变量取值函数(右值)
function LuaGetGlobal(xKey)
	if(xKey == nil) then error("LuaGetGlobal: key is nil"); return; end
	if(type(xKey) ~= "string") then error("LuaGetGlobal: key is not string, type: " .. type(xKey)); return; end

	local xValue = rawget(_G, xKey);
	return xValue;
end

-- 全局变量取值函数(右值)
function LuaDelGlobal(xKey)
	if(xKey == nil) then error("LuaDelGlobal: key is nil"); return; end
	if(type(xKey) ~= "string") then error("LuaDelGlobal: key is not string, type: " .. type(xKey)); return; end

	rawset(_G, xKey, nil);
end

-- 全局,获取模块并实例化该模块
function LuaRequireModule(moduleName)
	if(moduleName == nil or moduleName == "") then
		error("LuaRequireModule: moduleName is nil");
		return
	end
	local className = require(moduleName)
	if className == nil then
		error("LuaRequireModule: module return value is nil",moduleName)
		return
	end
	return className:new(nil)
end

-- -- json
-- LuaSetGlobalIfNil("g_JSON", require("3rdParty/json/JSON"));

if not FASTMAIN_FOR_PINCHFACE then
	require ("3rdParty/LuaUtil"); -- 事件子模块
	-- 事件监听
	require ("3rdParty/ScriptEvent"); -- 事件子模块
	require("msgpack")

	require("design/Task_IdMap")
	AllFormulas = {}
	require("design/AllFormulas/Formula_Formula")
	require("design/AllFormulas/Formula_Rank")
	require("design/AllFormulas/Formula_ScenesItem_ItemChoice")

	function GetFormula(id)
		if AllFormulas.Action_Formula[id] then
			return AllFormulas.Action_Formula[id].Formula
		else
			return nil
		end
	end
	require("game/DesignTableList")
	require "ui/uimodule"
	BeginSample("AllUIModules")	
	require ("ui/AllUIModules") -- UI子模块
	EndSample()
	BeginSample("all")
	require("auto_transform/all")
	EndSample()
	require("message")
	BeginSample("AllGameModules")
	require("game/AllGameModules")
	EndSample()
else
	-- 伏羲云游戏捏脸
	require ("3rdParty/LuaUtil"); -- 事件子模块
	-- 事件监听
	require ("3rdParty/ScriptEvent"); -- 事件子模块
	require("msgpack")

	require("game/DesignTableList")
	require "ui/uimodule"

	BeginSample("AllUIModules")	
	__ui_cache = {
		LuaCommandMgr = "game/common/LuaCommandMgr",
		CLuaCharacterCreationCameraCtrl = "ui/login/LuaCharacterCreationCameraCtrl",
		LuaPinchFaceMgr = "ui/pinchface/LuaPinchFaceMgr",
		LuaPinchFaceWnd = "ui/pinchface/LuaPinchFaceWnd",
		LuaPinchFacePreselectionView = "ui/pinchface/LuaPinchFacePreselectionView",
		LuaPinchFaceMainView = "ui/pinchface/LuaPinchFaceMainView",
		LuaPinchFaceSliderControlView = "ui/pinchface/LuaPinchFaceSliderControlView",
		LuaPinchFacePreviewView = "ui/pinchface/LuaPinchFacePreviewView",
		LuaPinchFaceDisplayView = "ui/pinchface/LuaPinchFaceDisplayView",
		LuaOneDimenSlider = "ui/pinchface/LuaOneDimenSlider",
		LuaTwoDimenSlider = "ui/pinchface/LuaTwoDimenSlider",
		LuaCustomColorSlider = "ui/pinchface/LuaCustomColorSlider",
		LuaPinchFaceModifyWnd = "ui/pinchface/LuaPinchFaceModifyWnd",
		LuaPinchFaceShareWnd = "ui/pinchface/LuaPinchFaceShareWnd",
		LuaPinchFaceTimeCtrlMgr = "ui/pinchface/LuaPinchFaceTimeCtrlMgr",
		CLuaRanFaMgr = "ui/ranfa/LuaRanFaMgr",
		LuaCloudGameFaceMgr = "ui/pinchface/LuaCloudGameFaceMgr",
		LuaPinchFaceFuxiWnd = "ui/pinchface/LuaPinchFaceFuxiWnd",
		LuaCameraMgr = "ui/common/LuaCameraWnd",
		LuaMiniVolumeSettingsMgr = "ui/LuaMiniVolumeSettingsWnd",
		LuaPinchFaceBtn = "ui/pinchface/LuaPinchFaceBtn",
		LuaBeforeEnterDengZhongShiJieWnd = "ui/chuangjue2023/LuaBeforeEnterDengZhongShiJieWnd",
		LuaCharacterCreation2023Wnd = "ui/chuangjue2023/LuaCharacterCreation2023Wnd",
		LuaCharacterCreation2023Mgr = "ui/chuangjue2023/LuaCharacterCreation2023Mgr",
	}

	AllFormulas = {}
	require("design/AllFormulas/Formula_Formula")
	require("design/AllFormulas/Formula_Rank")
	require("design/AllFormulas/Formula_ScenesItem_ItemChoice")
	function GetFormula(id)
		if AllFormulas.Action_Formula[id] then
			return AllFormulas.Action_Formula[id].Formula
		else
			return nil
		end
	end
	require "DesignBinaryExt"
	local __design_data = require "DesignBinaryInc"
	--5.2.4全部require
	for k,v in pairs(__ui_cache) do
		local ok = xpcall(function() require(v) end, function(err)
			print(SafeStringFormat3("<color=red>error:%s, file:%s</color>", err, v))
		end)
	end
	__ui_cache = nil

	setmetatable(_G, { __index = function(t,key)
		local v = rawget(_G,key)
		if v then return v end

		if __design_data[key] then
			local v = __design_data[key]
			if v==1 then
				rawset(_G,key,create_sheet_class(key))
			elseif v==2 then
				rawset(_G,key,create_setting_class(key))
			elseif v==3 then
				rawset(_G,key,ext_create_sheet_class(key))
			elseif v==4 then
				rawset(_G,key,create_sheet_class_with_lazyfields(key))
			elseif v==5 then
				rawset(_G,key,create_setting_class_with_lazyfields(key))
			elseif v==6 then
				rawset(_G,key,ext_create_sheet_class_with_lazyfields(key))
			end
			__design_data[key]=nil
			return rawget(_G,key)
		end
		return nil
	end })

	EndSample()
	BeginSample("all")
	require ("auto_transform/defines")
	EndSample()
	require("message")
	BeginSample("AllGameModules")
	require("game/AllGameModules")
	EndSample()
end

--require("common/doudizhu/doudizhu")--斗地主

function FindChild(tf,name)
	return CUICommonDef.GetChild(tf,name)
end

function FindChildWithType(trans,path,type)
	return CUICommonDef.GetChildWithType(trans,path,type)
end

function ShowUI(name)
	if CLuaUIResources[name] then
		CUIManager.ShowUI(CLuaUIResources[name])
	end
end
function CloseUI(name)
	if CLuaUIResources[name] then
		CUIManager.CloseUI(CLuaUIResources[name])
	end
end

require "DesignBinaryInc"

DeepCopyAllClasses()

