local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaZhouNianQingPassportUnlockWnd = class()

RegistClassMember(LuaZhouNianQingPassportUnlockWnd, "Template1")
RegistClassMember(LuaZhouNianQingPassportUnlockWnd, "Template2")
RegistClassMember(LuaZhouNianQingPassportUnlockWnd, "AwardTemplate")


function LuaZhouNianQingPassportUnlockWnd:Awake()
    self.Template1 = self.transform:Find("Anchor/Template1").gameObject
    self.Template2 = self.transform:Find("Anchor/Template2").gameObject
    self.AwardTemplate = self.Template2.transform:Find("AwardTemplate").gameObject
    self.Template1:SetActive(false)
    self.Template2:SetActive(false)
    self.AwardTemplate:SetActive(false)
end

function LuaZhouNianQingPassportUnlockWnd:Init()
    local setting = ZhanLing_Setting.GetData()

    for i = 1, 2 do
        local vip = self.transform:Find("Anchor/Passport"..i)
        vip:Find("Price"):GetComponent(typeof(UILabel)).text = i == 1 and LocalString.GetString("原价128元") or LocalString.GetString("原价168元")
        vip:Find("UnlockButton/Label"):GetComponent(typeof(UILabel)).text = setting.VIPCost[i-1]..LocalString.GetString("元解锁")
        if i == 2 then
            vip:Find("UnlockButton/ExtSprite/ExtLabel"):GetComponent(typeof(UILabel)).text = "+"..math.floor((setting.Vip2AddRate-1)*100).."%"..LocalString.GetString("任务经验获得")
        end
        local btn = vip:Find("UnlockButton").gameObject
        if LuaZhouNianQing2023Mgr["m_IsVip"..i] then 
            CUICommonDef.SetActive(btn, false, true)
            btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已解锁")
        else
            UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                LuaZhouNianQing2023Mgr:BuyVip(i)
            end)
        end
        
        local uitable = vip:Find("ScrollView/InfoTable"):GetComponent(typeof(UITable))
        Extensions.RemoveAllChildren(uitable.transform)
        local obj = NGUITools.AddChild(uitable.gameObject, self.Template1)
        obj:SetActive(true)
        obj.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/FestivalActivity/Festival_ZhouNianQing/ZhouNianQing2023/Material/zhounianqingpassportwnd_icon_"..(i+1)..".mat")
        obj.transform:Find("Desc1/Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("解锁")..(i==1 and LocalString.GetString("首夏清和灯") or LocalString.GetString("四季同辉灯"))
        obj.transform:Find("Desc2/Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("额外获取%s灯芒"), i == 1 and setting.Vip1AddValue or setting.Vip2AddValue)

        local grid
        if i == 2 then
            obj = NGUITools.AddChild(uitable.gameObject, self.Template2)
            obj:SetActive(true)
            grid = obj.transform:Find("Grid"):GetComponent(typeof(UIGrid))
            Extensions.RemoveAllChildren(grid.transform)
            obj.transform:Find("Title1").gameObject:SetActive(false)
            obj.transform:Find("Title2").gameObject:SetActive(true)
            local exPrize = setting.Vip2ExtraPrize
            for j = 0, exPrize.Length - 1 do
                local prize = exPrize[j]
                local itemid = tonumber(prize[0])
                local num = tonumber(prize[1])
                local award = NGUITools.AddChild(grid.gameObject, self.AwardTemplate)
                award:SetActive(true)
                local icon = award.transform:Find("Icon"):GetComponent(typeof(CUITexture))
                local countLabel = award.transform:Find("Amount"):GetComponent(typeof(UILabel))
                local qualitySprite = award.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
                local fx = award.transform:Find("Fx"):GetComponent(typeof(CUIFx))
                local data = Item_Item.GetData(itemid)
                icon:LoadMaterial(data.Icon)
                countLabel.text = num > 1 and num or nil
                qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(data, nil, false)
    
                UIEventListener.Get(award).onClick = DelegateFactory.VoidDelegate(function (go)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
                end)
            end
            grid:Reposition()
        end

        obj = NGUITools.AddChild(uitable.gameObject, self.Template2)
        obj:SetActive(true)
        grid = obj.transform:Find("Grid"):GetComponent(typeof(UIGrid))
        Extensions.RemoveAllChildren(grid.transform)
        local vipPrize = setting["Vip"..i.."Prize"]
        for j = 0, vipPrize.Length - 1 do
            local prize = vipPrize[j]
            local itemid = tonumber(prize[0])
            local num = tonumber(prize[1])
            local award = NGUITools.AddChild(grid.gameObject, self.AwardTemplate)
            award:SetActive(true)
            local icon = award.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            local countLabel = award.transform:Find("Amount"):GetComponent(typeof(UILabel))
            local qualitySprite = award.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
            local fx = award.transform:Find("Fx"):GetComponent(typeof(CUIFx))
            local data = Item_Item.GetData(itemid)
            icon:LoadMaterial(data.Icon)
            countLabel.text = num > 1 and num or nil
            qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(data, nil, false)

            UIEventListener.Get(award).onClick = DelegateFactory.VoidDelegate(function (go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        end
    end
end

