-- Auto Generated!!
local CGuildEnemyListItem = import "L10.UI.CGuildEnemyListItem"
local Constants = import "L10.Game.Constants"
local CButton = import "L10.UI.CButton"
CGuildEnemyListItem.m_Init_CS2LuaHook = function (this, info, index) 
    this.guildId = info.guildId
    local default
    if index % 2 == 0 then
        default = Constants.EvenBgSprite
    else
        default = Constants.OddBgSpirite
    end
    this.bgBtn:SetBackgroundSprite(default)
    this.idLabel.text = tostring(info.guildId)
    this.nameLabel.text = info.guildName
    this.numLabel.text = System.String.Format("{0}/{1}", info.memberNum, info.maxMemberNum)
    this.leaderLabel.text = info.leaderName
    this.levelLabel.text = tostring(info.guildLevel)
    this:UpdateRelationship()

    local bg = this.transform:GetComponent(typeof(UISprite))
    local btn = this.transform:GetComponent(typeof(CButton))

    if index %2 == 0 then
        bg.spriteName = "common_bg_mission_background_n"
        btn.normalSprite = "common_bg_mission_background_n"
    else
        bg.spriteName = "common_bg_mission_background_s"
        btn.normalSprite = "common_bg_mission_background_s"
    end
end
