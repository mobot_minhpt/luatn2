require("common/common_include")

local CItemMgr = import "L10.Game.CItemMgr"
local CUIManager = import "L10.UI.CUIManager"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CSkillAcquisitionMgr = import "L10.UI.CSkillAcquisitionMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaEquipIdentifySkillMgr = {}

LuaEquipIdentifySkillMgr.m_InitialEquipId = nil
LuaEquipIdentifySkillMgr.m_InitialWndIndex = 0

LuaEquipIdentifySkillMgr.ShowIdentifySkillWnd = function(itemId)
	LuaEquipIdentifySkillMgr.m_InitialEquipId = itemId
	LuaEquipIdentifySkillMgr.m_InitialWndIndex = 0
	--检查是否为合法装备
	local item = CItemMgr.Inst:GetById(itemId)
	if item and item.IsEquip and item.Equip.Identifiable > 0 then
        if item.Equip.SubHand > 0 then
            g_MessageMgr:ShowMessage("FUSHOU_WEAPON_BAN_JIANDING_SKILL")
        else
		    CUIManager.ShowUI(CLuaUIResources.EquipIdentifySkillWnd)
        end
	end
end

LuaEquipIdentifySkillMgr.ShowExtendSkillWnd = function(itemId)
	LuaEquipIdentifySkillMgr.m_InitialEquipId = itemId
	LuaEquipIdentifySkillMgr.m_InitialWndIndex = 2
	--检查是否为合法装备
	local item = CItemMgr.Inst:GetById(itemId)
	if item and item.IsEquip and item.Equip.Identifiable > 0 then
		 CUIManager.ShowUI(CLuaUIResources.EquipIdentifySkillWnd)
	end
end

LuaEquipIdentifySkillMgr.GetCanExchangeEquipments = function()
    -- 蓝装及以上品质
    -- 已绑定
    -- 不可鉴定，或者
    -- 可鉴定并且已经鉴定过
	local result = {}
	local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return result end
    -- body
    local bodySize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Body)
    for i=1,bodySize do
    	local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Body, i)
    	local item = CItemMgr.Inst:GetById(itemId)
    	if item and item.IsEquip and item.IsBinded and item.Equip.IsBlueOrBetterEquipExceptTalisman and
            (not item.Equip.IsIdentifiable or Skill_AllSkills.Exists(item.Equip.FixedIdentifySkillId)) then

            if not item.Equip.IsIdentifiable then
                local template  = EquipmentTemplate_Equip.GetData(item.TemplateId)
                if CItemMgr.Inst:IsIdentifiableBodyPosition(template.Type) then
                    table.insert(result, itemId)
                end
            else
    		  table.insert(result, itemId)
            end
    	end
    end
	-- bag
    local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i=1,bagSize do
    	local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
    	local item = CItemMgr.Inst:GetById(itemId)
    	if item and item.IsEquip and item.IsBinded and item.Equip.IsBlueOrBetterEquipExceptTalisman and
            (not item.Equip.IsIdentifiable or Skill_AllSkills.Exists(item.Equip.FixedIdentifySkillId)) then
    		
            if not item.Equip.IsIdentifiable then
                local template  = EquipmentTemplate_Equip.GetData(item.TemplateId)
                if CItemMgr.Inst:IsIdentifiableBodyPosition(template.Type) then
                    table.insert(result, itemId)
                end
            else
              table.insert(result, itemId)
            end
    	end
    end
    
    return result
end

LuaEquipIdentifySkillMgr.GetCanExtendEquipments = function()
    local result = {}
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return result end
    -- body
    local bodySize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Body)
    for i=1,bodySize do
        local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Body, i)
        local item = CItemMgr.Inst:GetById(itemId)
        if item and item.IsEquip and item.IsBinded and Skill_AllSkills.Exists(item.Equip.FixedIdentifySkillId) and item.Equip.SubHand <= 0 then
            table.insert(result, itemId)
        end
    end
    -- bag
    local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i=1,bagSize do
        local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        local item = CItemMgr.Inst:GetById(itemId)
        if item and item.IsEquip and item.IsBinded and Skill_AllSkills.Exists(item.Equip.FixedIdentifySkillId) and item.Equip.SubHand <= 0 then
            table.insert(result, itemId)
        end
    end
    
    return result
end

LuaEquipIdentifySkillMgr.costItemData = nil
function LuaEquipIdentifySkillMgr:GetCostItemData()
    return self.costItemData
end
function LuaEquipIdentifySkillMgr:IdentifySkillWashCost(equipId, list)
    local t = {}
    local identifySkillData = IdentifySkill_Settings.GetData()
    for i=0,list.Count-1 do
        if list[i].Count == 4 then
            --washItemType, needMoney, itemTemplateId, num
            if list[i][2] == identifySkillData.JianDingTianShu then
                for j = 0,identifySkillData.LvJianDingTianShu.Length - 1 do
                    table.insert(t, j + 1 , {washItemType = 2 + j, needMoney = list[i][1], itemTemplateId = identifySkillData.LvJianDingTianShu[j], num = list[i][3]})
                end
                table.insert(t, identifySkillData.LvJianDingTianShu.Length + 1,{washItemType = list[i][0], needMoney = list[i][1], itemTemplateId = list[i][2], num = list[i][3]})
            else
                table.insert(t, {washItemType = list[i][0], needMoney = list[i][1], itemTemplateId = list[i][2], num = list[i][3]})
            end
        end
    end
    self.costItemData = t
    g_ScriptEvent:BroadcastInLua("IdentifySkillWashCost", equipId, t)
end

function LuaEquipIdentifySkillMgr.IsEquipIdentifyCostItem(equipId)
    local identifySkillData = IdentifySkill_Settings.GetData()
    for j = 0,identifySkillData.LvJianDingTianShu.Length - 1 do
        if identifySkillData.LvJianDingTianShu[j] == equipId then
            return true
        end
    end
    return false
end

LuaEquipIdentifySkillMgr.ItemChooseWnd_OnSelectCallback = nil
LuaEquipIdentifySkillMgr.ItemChooseWnd_EquipNeedLevel = nil
LuaEquipIdentifySkillMgr.ItemChooseWnd_OnCloseCallback = nil
LuaEquipIdentifySkillMgr.ItemChooseWnd_SelectableCostItem = nil
function LuaEquipIdentifySkillMgr:ShowItemChooseWnd(onSelectCallback, equipNeedLevel, onCloseCallback, selectableCostItem)
    self.ItemChooseWnd_OnSelectCallback = onSelectCallback
    self.ItemChooseWnd_EquipNeedLevel = equipNeedLevel
    self.ItemChooseWnd_OnCloseCallback = onCloseCallback
    self.ItemChooseWnd_SelectableCostItem = selectableCostItem
    CUIManager.ShowUI(CLuaUIResources.EquipIdentifySkillItemChooseWnd)
end
function LuaEquipIdentifySkillMgr:OnItemChooseWndDisable()
    if self.ItemChooseWnd_OnCloseCallback then
        self.ItemChooseWnd_OnCloseCallback()
        self.ItemChooseWnd_OnCloseCallback = nil
    end
end
function LuaEquipIdentifySkillMgr:GetItemChooseWnd_EquipNeedLevel()
    return self.ItemChooseWnd_EquipNeedLevel
end
function LuaEquipIdentifySkillMgr:OnItemChooseWndSelectCallback(selectableCostItem, type)
    if self.ItemChooseWnd_OnSelectCallback then
        self.ItemChooseWnd_OnSelectCallback(selectableCostItem, type)
    end
end
function LuaEquipIdentifySkillMgr:GetItemChooseWndSelectableCostItem()
    return self.ItemChooseWnd_SelectableCostItem
end
-------

-- Gas2Gac

function Gas2Gac.IdentifySkillWashCost(equipId, ret_U)
    local list = ret_U and MsgPackImpl.unpack(ret_U)
    if not list then return end
    LuaEquipIdentifySkillMgr:IdentifySkillWashCost(equipId, list)
end

function Gas2Gac.IdentifySkillExtendCost(equipId, extendTimes, needMoney, itemTemplateId, num)
     g_ScriptEvent:BroadcastInLua("IdentifySkillExtendCost", equipId, extendTimes, needMoney, itemTemplateId, num)
end

function Gas2Gac.IdentifySkillExchangeCost(equip1Id, equip2Id, needJade, itemTemplateId1, num1, itemTemplateId2, num2)
    g_ScriptEvent:BroadcastInLua("IdentifySkillExchangeCost", equip1Id, equip2Id, needJade, itemTemplateId1, num1, itemTemplateId2, num2)
end

function Gas2Gac.IdentifySkillApplyFeeback(equipId, ret_U)
    local list = ret_U and MsgPackImpl.unpack(ret_U)
    if not list then return end
    --startTime, endTime, tempIdentifySkill, feebackItemTemplateId, feebackItemNum
    local commonItem = CItemMgr.Inst:GetById(equipId)
    local equip = commonItem and commonItem.Equip
    if not equip then return end

    local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(list[1])
    local tmpSkill = list[2] and Skill_AllSkills.GetData(list[2])
    local feedbackItemNum = list[4]
    if not tmpSkill or not feedbackItemNum then return end

    local msg = g_MessageMgr:FormatMessage("Equip_ReIdentify_Giveup_ExpiryDate", tmpSkill.Name, endTime:ToString("yyyy-MM-dd"), feedbackItemNum)
    MessageWndManager.ShowOKCancelMessage(msg, 
        DelegateFactory.Action(function() Gac2Gas.IdentifySkillRequestApplyTempSkill(equipId, false) end),
        nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function Gas2Gac.IdentifySkillRequestWashSkillResult(success, equipId, washItemType, curSkillChanged, tmpSkillChanged, oldTmpSkillId, newTmpSkillId)
    if not success then
        return
    end
    g_ScriptEvent:BroadcastInLua("IdentifySkillRequestWashSkillSuccess", equipId, washItemType, curSkillChanged, tmpSkillChanged, oldTmpSkillId, newTmpSkillId)
end

function Gas2Gac.IdentifySkillRequestApplyTempSkillResult(success, equipId)
    if not success then
        return
    end
    g_ScriptEvent:BroadcastInLua("IdentifySkillRequestApplyTempSkillSuccess", equipId)
end

function Gas2Gac.IdentifySkillRequestExtendSkillResult(success, equipId, times)
    if not success then
        return
    end
    g_ScriptEvent:BroadcastInLua("IdentifySkillRequestExtendSuccess", equipId)
end

function Gas2Gac.IdentifySkillRequestExchangeSkillResult(success, itemId1, itemId2)
    if not success then
        return
    end
    g_ScriptEvent:BroadcastInLua("IdentifySkillRequestExchangeSuccess", itemId1, itemId2)
end

function Gas2Gac.IdentifySkillSetDisableResult(equipId, success, itemId, bSkillDisable)
    if not success then
        return
    end
end

function Gas2Gac.ShowSkillAquisitionWnd(skillId)
    local list = CreateFromClass(MakeGenericClass(List, UInt32))
    CommonDefs.ListAdd(list, typeof(UInt32), skillId)
    CSkillAcquisitionMgr.ShowSkillAquisitionWnd(list)
end