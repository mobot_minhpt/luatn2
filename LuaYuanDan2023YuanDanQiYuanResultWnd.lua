local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animator = import "UnityEngine.Animator"
LuaYuanDan2023YuanDanQiYuanResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanResultWnd, "Win", "Win", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanResultWnd, "Fail", "Fail", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanResultWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanResultWnd, "ItemCellTemplate", "ItemCellTemplate", GameObject)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanResultWnd, "RwTipLb1", "RwTipLb1", UILabel)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanResultWnd, "FightBtn", "FightBtn", QnButton)
RegistChildComponent(LuaYuanDan2023YuanDanQiYuanResultWnd, "RwTipLb2", "RwTipLb2", UILabel)

--@endregion RegistChildComponent end

function LuaYuanDan2023YuanDanQiYuanResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.FightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFightBtnClick()
	end)


    --@endregion EventBind end
end

function LuaYuanDan2023YuanDanQiYuanResultWnd:Init()
	self.ItemCellTemplate.gameObject:SetActive(false)
	local isWin = LuaYuanDan2023Mgr.YuanDanQiYuanResultInfo.isWin
	local isReward = LuaYuanDan2023Mgr.YuanDanQiYuanResultInfo.isReward
	local anim = self.gameObject:GetComponent(typeof(Animator))
	if isWin then
		anim:Play("conmmon_resultwnd_half_win_1")
	else
		anim:Play("conmmon_resultwnd_half_loss_1")
	end
	self.Win.gameObject:SetActive(isWin)
	self.Fail.gameObject:SetActive(not isWin)
	if isWin then
		self.Grid.gameObject:SetActive(isReward)
		self.RwTipLb1.gameObject:SetActive(isReward)
		self.RwTipLb2.gameObject:SetActive(not isReward)
		if isReward then
			self:ShowAwards()
		end
	end
end
function LuaYuanDan2023YuanDanQiYuanResultWnd:ShowAwards()
	local awardList = YuanDan2023_YuanDanQiYuan.GetData().AwardItemList
	Extensions.RemoveAllChildren(self.Grid.transform)
	if awardList.Length > 0 then
		for i = 0, awardList.Length - 1 do
			local go = CUICommonDef.AddChild(self.Grid.gameObject, self.ItemCellTemplate)
			go.gameObject:SetActive(true)
			self:InitOneItem(go,awardList[i])
		end
	end
	self.Grid:Reposition()
end
function LuaYuanDan2023YuanDanQiYuanResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
	curItem:SetActive(true)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

--@region UIEvent

function LuaYuanDan2023YuanDanQiYuanResultWnd:OnFightBtnClick()
	CUIManager.CloseUI(CLuaUIResources.YuanDan2023YuanDanQiYuanResultWnd)
	CUIManager.ShowUI(CLuaUIResources.YuanDan2023YuanDanQiYuanEnterWnd)
end


--@endregion UIEvent

