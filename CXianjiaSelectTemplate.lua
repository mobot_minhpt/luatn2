-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CXianjiaSelectTemplate = import "L10.UI.CXianjiaSelectTemplate"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumQualityType = import "L10.Game.EnumQualityType"
local Object = import "System.Object"
local QualityColor = import "L10.Game.QualityColor"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CXianjiaSelectTemplate.m_Init_CS2LuaHook = function (this, talisman) 

    this.templateId = talisman.ID
    this.iconTexture:LoadMaterial(talisman.Icon)
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), talisman.Color))
    this.nameLabel.text = talisman.Name
    this.nameLabel.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), talisman.Color))
    local bindCount, notbindCount
    bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(talisman.ID)
    local pos
    local itemId
    local default
    default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Body, talisman.ID)
    if default then
        bindCount = bindCount + 1
    end
    if bindCount + notbindCount == 0 then
        this.amountLabel.text = "0"
        this.amountLabel.color = Color.red
    else
        this.amountLabel.text = tostring((bindCount + notbindCount))
        this.amountLabel.color = Color.white
    end
    UIEventListener.Get(this.iconTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.OnTalismanClick, VoidDelegate, this)
end
CXianjiaSelectTemplate.m_OnTalismanClick_CS2LuaHook = function (this, go) 

    if this.templateId > 0 then
        CItemInfoMgr.ShowLinkItemTemplateInfo(this.templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        if this.OnIconClicked ~= nil then
            GenericDelegateInvoke(this.OnIconClicked, Table2ArrayWithCount({this.gameObject}, 1, MakeArrayClass(Object)))
        end
    end
end
