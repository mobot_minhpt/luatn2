
local CClientMonster = import "L10.Game.CClientMonster"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Helloween_Setting = import "L10.Game.Halloween_Setting"
local UISlider=import "UISlider"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"

LuaCrazyDyeBossStateWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaCrazyDyeBossStateWnd,       "Slider",            UISlider)

---------RegistClassMember-------
RegistClassMember(LuaCrazyDyeBossStateWnd,          "MonsterEngineId")
RegistClassMember(LuaCrazyDyeBossStateWnd,          "RanSeBossId")

RegistClassMember(LuaCrazyDyeBossStateWnd,          "BossHpPercent")    --真实血量
RegistClassMember(LuaCrazyDyeBossStateWnd,          "SliderShow")       --显示位置
RegistClassMember(LuaCrazyDyeBossStateWnd,          "SliderTarget")     --目标位置
RegistClassMember(LuaCrazyDyeBossStateWnd,          "HpParam")          --插值参数
RegistClassMember(LuaCrazyDyeBossStateWnd,          "CountDownTick")    --Boss变身计时器

RegistClassMember(LuaCrazyDyeBossStateWnd,          "HpMax")            --当前阶段血量上限
RegistClassMember(LuaCrazyDyeBossStateWnd,          "HpMin")            --当前阶段血量下限

function LuaCrazyDyeBossStateWnd:Awake()
    self.RanSeBossId = Helloween_Setting.GetData().RanSeBossId
    self.SliderShow = 0.5       --  默认显示在中间吧
    self.HpParam = 0.03
    Gac2Gas.QueryRanSeJiBossStatus()
    
    self.CountDownTick = RegisterTick(function ()
        luaCrazyDyeMgr.IsChange = (CServerTimeMgr.Inst.timeStamp < luaCrazyDyeMgr.ChangeEndTime)
    end, 1000)
end

function LuaCrazyDyeBossStateWnd:OnEnable()
    g_ScriptEvent:AddListener("HpUpdate", self, "OnHpUpdate")
    g_ScriptEvent:AddListener("SendRanSeJiBossStatus", self, "RefreshBossState")
end

function LuaCrazyDyeBossStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HpUpdate", self, "OnHpUpdate")
    g_ScriptEvent:RemoveListener("SendRanSeJiBossStatus", self, "RefreshBossState")
    UnRegisterTick(self.CountDownTick)
end

function LuaCrazyDyeBossStateWnd:Update()
    if self.SliderTarget and math.abs(self.SliderTarget-self.SliderShow) > 0.01 then 
        self.SliderShow = math.lerp(self.SliderShow,self.SliderTarget,self.HpParam)
        self.Slider.value = self.SliderShow
    end
end

function LuaCrazyDyeBossStateWnd:OnHpUpdate(args)           --boss进如视野后开始更新滑动条
    self.MonsterEngineId = args[0]
    local obj = CClientObjectMgr.Inst:GetObject(self.MonsterEngineId)
    if obj and TypeIs(obj, typeof(CClientMonster)) then
        if obj.TemplateId == self.RanSeBossId then          --检测是染色剂boss则刷新滑动条
            self:RefreshBossState(obj.Hp,obj.HpFull)
        end
    end
end

function LuaCrazyDyeBossStateWnd:RefreshBossState(Hp,HpFull)
    self.BossHpPercent = Hp/HpFull
    luaCrazyDyeMgr.IsChange = (CServerTimeMgr.Inst.timeStamp < luaCrazyDyeMgr.ChangeEndTime)
    self:RefreshColor()
    if not luaCrazyDyeMgr.IsChange then
        if luaCrazyDyeMgr.BossColor == 1 then           --  红
            self.SliderTarget = 1 - (self.HpMax - self.BossHpPercent) / (self.HpMax - self.HpMin) * 0.3
        elseif luaCrazyDyeMgr.BossColor == 2 then       --  蓝
            self.SliderTarget = (self.HpMax - self.BossHpPercent) / (self.HpMax - self.HpMin) * 0.3
        end
    else
        self.SliderTarget = 0.5
    end
end

function LuaCrazyDyeBossStateWnd:RefreshColor()
    for k,v in pairs(luaCrazyDyeMgr.ColorData) do
        if self.BossHpPercent <= v.threshold[1] then
            luaCrazyDyeMgr.BossColor = v.color
            self.HpMax = v.threshold[1]
            self.HpMin = v.threshold[2]
        end
    end
end
