local LuaGameObject=import "LuaGameObject"

local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CHideAndSeekSkillItem = import "L10.UI.CHideAndSeekSkillItem"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local NGUITools = import "NGUITools"
local LiuYi_ZhuoJiSetting = import "L10.Game.LiuYi_ZhuoJiSetting"
local MessageMgr = import "L10.Game.MessageMgr"
local Color = import "UnityEngine.Color"
local Extensions = import "Extensions"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"

CLuaZhuojiSingUpWnd = class()
RegistClassMember(CLuaZhuojiSingUpWnd, "m_PipeiLabel")
RegistClassMember(CLuaZhuojiSingUpWnd, "m_PipeiSprite")
RegistClassMember(CLuaZhuojiSingUpWnd, "m_PipeiingLabel")

RegistClassMember(CLuaZhuojiSingUpWnd, "m_DescLabel")

RegistClassMember(CLuaZhuojiSingUpWnd, "m_SkillBtnList")

RegistClassMember(CLuaZhuojiSingUpWnd, "m_RuleTemplate")
RegistClassMember(CLuaZhuojiSingUpWnd, "m_RuleTbl")
RegistClassMember(CLuaZhuojiSingUpWnd, "m_ScrollView")

RegistClassMember(CLuaZhuojiSingUpWnd, "m_RewardRemainLabel")

RegistClassMember(CLuaZhuojiSingUpWnd, "m_Skills")

function CLuaZhuojiSingUpWnd:Init()
	local anchor = LuaGameObject.GetChildNoGC(self.transform, "Anchor").transform
	local bg = LuaGameObject.GetChildNoGC(anchor, "bg").transform

	local headView = LuaGameObject.GetChildNoGC(bg, "HeadView").transform
	self.m_DescLabel = LuaGameObject.GetChildNoGC(headView, "Outline").label
	local str = MessageMgr.Inst:FormatMessage("Liuyi_Zhuoji_Desc_Simple",{})
	self.m_DescLabel.text = str

	local scrollView = LuaGameObject.GetChildNoGC(bg, "ContentScrollView").gameObject
	self.m_ScrollView = CommonDefs.GetComponent_GameObject_Type(scrollView, typeof(CUIRestrictScrollView))
	self.m_RuleTbl = LuaGameObject.GetChildNoGC(scrollView.transform, "Table").table
	Extensions.RemoveAllChildren(self.m_RuleTbl.transform)

	local templates = LuaGameObject.GetChildNoGC(bg, "Templates").transform
	self.m_RuleTemplate = LuaGameObject.GetChildNoGC(templates, "ParagraphTemplate").gameObject
	local ruleMsg = MessageMgr.Inst:FormatMessage("Liuyi_Zhuoji_Rule_Msg",{})
	for ruleStr in string.gmatch(ruleMsg or "1;2;", "([^;]+)") do
		local unit = NGUITools.AddChild(self.m_RuleTbl.gameObject, self.m_RuleTemplate)
		unit:SetActive(true)
		local paragraph = CommonDefs.GetComponent_GameObject_Type(unit, typeof(CTipParagraphItem))
		ruleStr = string.gsub(ruleStr, "\n", "")
		paragraph:Init(ruleStr, 0xffffffff)
	end
	self.m_RuleTbl:Reposition()
	self.m_ScrollView:ResetPosition()
	self.m_RuleTemplate:SetActive(false)

	self.m_PipeiingLabel = LuaGameObject.GetChildNoGC(bg, "pipeiing").label

	local pipeiBtnLuaGO = LuaGameObject.GetChildNoGC(bg, "startgame")
	UIEventListener.Get(pipeiBtnLuaGO.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		self:OnSignUpBtnClicked(go)
	end)
	self.m_PipeiSprite = pipeiBtnLuaGO.sprite
	self.m_PipeiLabel = LuaGameObject.GetChildNoGC(self.m_PipeiSprite.transform, "Label").label

	local setting = LiuYi_ZhuoJiSetting.GetData()
	local tempSkills = setting.TempSkills 
	local releaseChickenSkill = setting.ReleaseChickenSkill
	local showSkills = {}
	for i = 1, tempSkills.Length do
		local skillId = tempSkills[i-1]
		if skillId ~= releaseChickenSkill then
			table.insert(showSkills, skillId)
		end
	end

	local skills = LuaGameObject.GetChildNoGC(bg, "Skills").gameObject
	self.m_SkillBtnList = {}
	self.m_Skills = {}
	local i = 1
	for _, skillId in pairs(showSkills) do
		if i <= 4 then 
			local name = SafeStringFormat3("%dHideSkill", i)
			local luaGO = LuaGameObject.GetChildNoGC(skills.transform, name).gameObject
			local btn = CommonDefs.GetComponent_GameObject_Type(luaGO, typeof(CHideAndSeekSkillItem))
			self.m_SkillBtnList[i] = btn
			self.m_Skills[i] = skillId

			btn.gameObject:SetActive(true)
			btn:Init(skillId)
			UIEventListener.Get(luaGO).onClick = DelegateFactory.VoidDelegate(function(go)
				self:OnSkillBtnClicked(go)
			end)
		end
		i = i + 1
	end

	self.m_RewardRemainLabel = LuaGameObject.GetChildNoGC(bg, "rewardremain").label

	self:ShowPipeiBtn()
	self:ShowRemainTimesLabel()
end

function CLuaZhuojiSingUpWnd:ShowPipeiBtn()
	self.m_PipeiingLabel.gameObject:SetActive(CLuaLiuYiMgr.IsSignUp)
	if CLuaLiuYiMgr.IsSignUp then
		self.m_PipeiLabel.text = LocalString.GetString("取消匹配")
		self.m_PipeiSprite.spriteName = "common_button_darkblue_3_n"
	else
		self.m_PipeiLabel.text = LocalString.GetString("开始匹配")
		self.m_PipeiSprite.spriteName = "common_button_orange_normal"
	end
end

function CLuaZhuojiSingUpWnd:ShowRemainTimesLabel()
	self.m_RewardRemainLabel.text = CLuaLiuYiMgr.RewardRemainTimes .. ""
	if CLuaLiuYiMgr.RewardRemainTimes > 0 then
		self.m_RewardRemainLabel.color = Color.green
	else
		self.m_RewardRemainLabel.color = Color.red
	end
end

function CLuaZhuojiSingUpWnd:OnEnable()
    g_ScriptEvent:AddListener("LiuyiZhuoji_SendZhuoJiSignUpInfo", self, "OnLiuyiZhuoji_SendZhuoJiSignUpInfo")
end

function CLuaZhuojiSingUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("LiuyiZhuoji_SendZhuoJiSignUpInfo", self, "OnLiuyiZhuoji_SendZhuoJiSignUpInfo")
end

function CLuaZhuojiSingUpWnd:OnLiuyiZhuoji_SendZhuoJiSignUpInfo()
	self:ShowPipeiBtn()
	self:ShowRemainTimesLabel()
end

function CLuaZhuojiSingUpWnd:OnSkillBtnClicked(go)
	for i = 1, 4 do
		if self.m_SkillBtnList[i].gameObject == go then
			CSkillInfoMgr.ShowSkillInfoWnd(self.m_Skills[i], self.m_SkillBtnList[i].transform.position, EnumSkillInfoContext.Default, true, 0, 0, nil)
			break
		end
	end
end

function CLuaZhuojiSingUpWnd:OnSignUpBtnClicked(go)
	if CLuaLiuYiMgr.IsSignUp then
		Gac2Gas.RequestCancelSignUpZhuoJi()
	else
		Gac2Gas.RequestSignUpZhuoJi()
	end
end

function CLuaZhuojiSingUpWnd:OnDestroy()
	
end
return CLuaZhuojiSingUpWnd