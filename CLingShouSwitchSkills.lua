-- Auto Generated!!
local CLingShouSwitchSkillMgr = import "L10.Game.CLingShouSwitchSkillMgr"
local CLingShouSwitchSkills = import "L10.UI.CLingShouSwitchSkills"
local CLingShouSwitchSkillSlot = import "L10.UI.CLingShouSwitchSkillSlot"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Vector3 = import "UnityEngine.Vector3"
CLingShouSwitchSkills.m_Init_CS2LuaHook = function (this) 
    this.inited = true
    this.slots = CreateFromClass(MakeGenericClass(List, CLingShouSwitchSkillSlot))
    CommonDefs.ListAdd(this.slots, typeof(CLingShouSwitchSkillSlot), this.first)

    local count = 0
    if CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        --显示两个
        count = 2
    else
        count = 6
    end
    do
        local i = 0
        while i < count - 1 do
            local go = TypeAs(CommonDefs.Object_Instantiate(this.first.gameObject), typeof(GameObject))
            go.transform.parent = this.grid.transform
            go.transform.localScale = Vector3.one
            local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CLingShouSwitchSkillSlot))
            CommonDefs.ListAdd(this.slots, typeof(CLingShouSwitchSkillSlot), cmp)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this:InitNull()
end
CLingShouSwitchSkills.m_Awake_CS2LuaHook = function (this) 
    if not this.inited then
        this:Init()
    end
end
CLingShouSwitchSkills.m_InitSlots_CS2LuaHook = function (this, skillList) 
    if not this.inited then
        this:Init()
    end
    if CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        for i = 1, 2 do
            if i - 1 < this.slots.Count then
                this.slots[i - 1]:Init(skillList[i])
            end
        end
    else
        for i = 3, 8 do
            if i - 3 < this.slots.Count then
                this.slots[i - 3]:Init(skillList[i])
            end
        end
    end
end
CLingShouSwitchSkills.m_InitNull_CS2LuaHook = function (this) 
    if not this.inited then
        this:Init()
    end
    CommonDefs.ListIterate(this.slots, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        item:Init(0)
    end))
end
