local BoxCollider = import "UnityEngine.BoxCollider"
local UITexture = import "UITexture"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local CUIFx = import "L10.UI.CUIFx"

LuaScrollTaskDragGameTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaScrollTaskDragGameTaskView, "GameDesc", "GameDesc", UILabel)
RegistChildComponent(LuaScrollTaskDragGameTaskView, "Target", "Target", BoxCollider)
RegistChildComponent(LuaScrollTaskDragGameTaskView, "ItemContainer", "ItemContainer", UITexture)
RegistChildComponent(LuaScrollTaskDragGameTaskView, "ItemOffset", "ItemOffset", GameObject)
RegistChildComponent(LuaScrollTaskDragGameTaskView, "Item", "Item", CUITexture)

--@endregion RegistChildComponent end
--RegistClassMember(LuaScrollTaskDragGameTaskView, "m_IsDraging")
RegistClassMember(LuaScrollTaskDragGameTaskView, "m_DragOffset")
RegistClassMember(LuaScrollTaskDragGameTaskView, "m_Callback")
RegistClassMember(LuaScrollTaskDragGameTaskView, "m_Tick")
RegistClassMember(LuaScrollTaskDragGameTaskView, "m_SuccessFx")
RegistClassMember(LuaScrollTaskDragGameTaskView, "m_FinishDelay")
RegistClassMember(LuaScrollTaskDragGameTaskView, "m_SuccessFxPath")

function LuaScrollTaskDragGameTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_SuccessFx = self.transform:Find("TargetOffset/SuccessFx"):GetComponent(typeof(CUIFx))
    self.TargetOffset = self.transform:Find("TargetOffset")
end

function LuaScrollTaskDragGameTaskView:Start()
    CommonDefs.AddOnDragListener(self.ItemOffset.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    
    UIEventListener.Get(self.ItemOffset.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
        self:OnDragStart(g)
    end)

    UIEventListener.Get(self.ItemOffset.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
        self:OnDragEnd(g)
    end)
end

function LuaScrollTaskDragGameTaskView:InitGame(gameId, taskId, callback)
    self:LoadDesignData(gameId)

    self.m_DragOffset = Vector3.zero
    self.ItemOffset:SetActive(true)
    self.GameDesc.gameObject:SetActive(true)
    self.Target.gameObject:SetActive(true)
    self.Target.transform.localPosition = Vector3.zero
    self.m_SuccessFx:DestroyFx()
    self.ItemOffset.transform.localPosition = Vector3.zero
    Extensions.SetLocalRotationZ(self.ItemOffset.transform, 0)

    self.m_IsPlaying = true
    self.m_Callback = callback
end

function LuaScrollTaskDragGameTaskView:LoadDesignData(gameId)
    local data = ScrollTask_DragGame.GetData(gameId)
    if data == nil then
        return
    end
    self.GameDesc.text = data.Desc
    local position = g_LuaUtil:StrSplit(data.TargetPos,",")
    self.TargetOffset.localPosition = Vector3(tonumber(position[1]), tonumber(position[2]), 0)
    self.Item:LoadMaterial(data.ItemTexPath)
    self.m_SuccessFxPath = data.SuccessFx
end

--@region UIEvent

--@endregion UIEvent


function LuaScrollTaskDragGameTaskView:OnScreenDrag(go, delta)
    if self.m_IsPlaying == false then
        return
    end
    local currentPos = UICamera.currentTouch.pos
    self.ItemOffset.transform.position = CommonDefs.op_Addition_Vector3_Vector3(UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0)), self.m_DragOffset)
end

function LuaScrollTaskDragGameTaskView:OnDragStart(g)
    if self.m_IsPlaying == false then
        return
    end
    local currentPos = UICamera.currentTouch.pos
    self.m_DragOffset = CommonDefs.op_Subtraction_Vector3_Vector3(self.ItemOffset.transform.position, UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0)))
end

function LuaScrollTaskDragGameTaskView:OnDragEnd(g)
    if self.m_IsPlaying == false then
        return
    end
    local currentPos = UICamera.currentTouch.pos
    local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
    local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

    for i =0,hits.Length - 1 do
        if hits[i].collider == self.Target and self.m_IsPlaying then
            self.m_IsPlaying = false
            self:OnSuccess()
            return
        end
    end

    self.ItemOffset.transform.localPosition = Vector3.zero
end

function LuaScrollTaskDragGameTaskView:OnSuccess()
    self.Target.transform.localPosition = Vector3(187, 71, 0)
    self.ItemOffset.transform.position = self.Target.transform.position
    Extensions.SetLocalRotationZ(self.ItemOffset.transform, 30)
    self.GameDesc.gameObject:SetActive(false)
    self.Target.gameObject:SetActive(false)

    if self.m_SuccessFxPath then
        self.m_SuccessFx:LoadFx(self.m_SuccessFxPath)
    end

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:OnGameFinish()
    end,self.m_FinishDelay or 3000)
end

function LuaScrollTaskDragGameTaskView:OnGameFinish()
    self.m_SuccessFx:DestroyFx()

    if self.m_Callback then
        self.m_Callback()
    end
end

function LuaScrollTaskDragGameTaskView:OnDestroy()
    UnRegisterTick(self.m_Tick)
end