local CButton = import "L10.UI.CButton"
local Animation = import "UnityEngine.Animation"
local UITable = import "UITable"
local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnButton = import "L10.UI.QnButton"
local GameObject = import "UnityEngine.GameObject"

LuaDaFuWongResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongResultWnd, "ShareButton", "ShareButton", QnButton)
RegistChildComponent(LuaDaFuWongResultWnd, "Result", "Result", GameObject)
RegistChildComponent(LuaDaFuWongResultWnd, "RewardTemplate", "RewardTemplate", GameObject)
RegistChildComponent(LuaDaFuWongResultWnd, "RewardTable", "RewardTable", UITable)
RegistChildComponent(LuaDaFuWongResultWnd, "rank", "rank", GameObject)
RegistChildComponent(LuaDaFuWongResultWnd, "HideWhenShare", "HideWhenShare", GameObject)
RegistChildComponent(LuaDaFuWongResultWnd, "ZhanBaoBtn", "ZhanBaoBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongResultWnd, "m_MaxPerform")
RegistClassMember(LuaDaFuWongResultWnd, "m_Ani")
function LuaDaFuWongResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


	
	UIEventListener.Get(self.ZhanBaoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhanBaoBtnClick()
	end)


    --@endregion EventBind end
	self.m_MaxPerform = DaFuWeng_Setting.GetData().ResultMaxPerformNum
	self.m_Ani = self.transform:GetComponent(typeof(Animation))
end

function LuaDaFuWongResultWnd:Init()
	self.RewardTemplate:SetActive(false)
	self:InitRankAndResult()
	self:InitRewardInfo()
	self:InitPerformInfo()
	local result = LuaDaFuWongMgr.PlayResult 
	if LuaDaFuWongMgr.ZhanBaoOpen and result and result.report and #result.report > 0 then
		self.ZhanBaoBtn.gameObject:SetActive(true)
	else
		self.ZhanBaoBtn.gameObject:SetActive(false)
	end
end
function LuaDaFuWongResultWnd:InitRankAndResult()
	local result = LuaDaFuWongMgr.PlayResult
	if not result then return end
	-- rank 
	for i = 0,self.rank.transform.childCount - 1 do
		self.rank.transform:GetChild(i).gameObject:SetActive(result.rank - 1 == i)
	end
	-- 胜利失败
	self.Result.transform:Find("Win").gameObject:SetActive(result.result)
	self.Result.transform:Find("Fail").gameObject:SetActive(not result.result)
	local VoiceList = LuaDaFuWongMgr.EventIdToVoicePath
	local voicePackageId = result.reward and result.reward.voicePackageId or 1
	local stage = result.result and (1000 + EnumDaFuWengStage.GameEnd) or 0
	if result.result then
		self.m_Ani:Play("dafuwongresultwnd_sl")
		-- if CClientMainPlayer.Inst then
		-- 	LuaDaFuWongMgr:AddVoiceToPlay(1000 + EnumDaFuWengStage.GameEnd,CClientMainPlayer.Inst.Id)
		-- end
	else
		self.m_Ani:Play("dafuwongresultwnd_sb")
	end
	if VoiceList and VoiceList[stage] and VoiceList[stage][voicePackageId] then
		local path = VoiceList[stage][voicePackageId][1]
		SoundManager.Inst:StartDialogSound(path)
	else
		local count = DaFuWeng_VoiceList.GetDataCount()
		for i = 1, count do
			local data = DaFuWeng_VoiceList.GetData(i)
			if data.Stage == stage and data.PackageID == voicePackageId then
				SoundManager.Inst:StartDialogSound(data.VoicePath)
				break
			end
		end
	end
end

function LuaDaFuWongResultWnd:InitRewardInfo()
	--Extensions.RemoveAllChildren(self.RewardTable.transform)
	local result = LuaDaFuWongMgr.PlayResult
	if not result then self.RewardTable.gameObject:SetActive(false) return end
	local reward1 = self.RewardTable.transform:GetChild(0)
	local reward2 = self.RewardTable.transform:GetChild(1)
	reward1.transform:Find("Name"):GetComponent(typeof(UILabel)).text = DaFuWeng_Setting.GetData().TongXingZhengExpName
	reward1.transform:Find("Score"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("+%d",result.reward.progress)
	reward2.transform:Find("Name"):GetComponent(typeof(UILabel)).text = DaFuWeng_Setting.GetData().ExpName
	reward2.transform:Find("Score"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("+%d",result.reward.exp)
end
function LuaDaFuWongResultWnd:InitPerformInfo()
	local result = LuaDaFuWongMgr.PlayResult
	if not result then return end
	local root = nil
	if result.result then root = self.Result.transform:Find("Win/WndBg_win/dafuwongresultwnd_shengli_taizi/Preform").gameObject
	else root = self.Result.transform:Find("Fail/WndBg_fail/dafuwongresultwnd_taotai_taizi/Preform").gameObject end
	for i = 0,root.transform.childCount - 1 do
		root.transform:GetChild(i).gameObject:SetActive(false)
	end
	local performInfo = {}
	local performNum = 0
	for k,v in pairs(result.playData) do
		if performNum >= self.m_MaxPerform then break end
		if v[3] > 0 then
			performNum = performNum + 1
			local preformInfo = DaFuWeng_ResultPerform.GetData(v[1])
			local go = root.transform:GetChild(performNum - 1)
			go.gameObject:SetActive(true)
			go.transform:Find("title"):GetComponent(typeof(UILabel)).text = preformInfo.TitleName
			go.transform:Find("title/des"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(preformInfo.Describe,v[2]) 
		end
	end
end
--@region UIEvent

function LuaDaFuWongResultWnd:OnShareButtonClick()
	CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.DaFuWongResultWnd, self.HideWhenShare)
end

function LuaDaFuWongResultWnd:OnZhanBaoBtnClick()
	CUIManager.ShowUI(CLuaUIResources.DaFuWongZhanBaoWnd)
end


--@endregion UIEvent

