local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UITable = import "UITable"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaLiuYi2023BossTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuYi2023BossTaskView, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaLiuYi2023BossTaskView, "StatesRoot", "StatesRoot", UITable)
RegistChildComponent(LuaLiuYi2023BossTaskView, "DescLab", "DescLab", UILabel)
RegistChildComponent(LuaLiuYi2023BossTaskView, "HpSlider", "HpSlider", UISlider)
RegistChildComponent(LuaLiuYi2023BossTaskView, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaLiuYi2023BossTaskView, "LeaveButton", "LeaveButton", CButton)
RegistChildComponent(LuaLiuYi2023BossTaskView, "RuleButton", "RuleButton", CButton)
RegistChildComponent(LuaLiuYi2023BossTaskView, "SliderLab", "SliderLab", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2023BossTaskView, "m_Tick")

function LuaLiuYi2023BossTaskView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    UIEventListener.Get(self.LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)

    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleButtonClick()
	end)

    self.DescLab.text = ""
end

function LuaLiuYi2023BossTaskView:Init()
    self:UpdateInfo()
    self:Refresh(LuaLiuYi2023Mgr.m_BossNpcLeftHp or 100)
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 1000)
end

--@region UIEvent

--@endregion UIEvent

function LuaLiuYi2023BossTaskView:OnEnable()
    g_ScriptEvent:AddListener("UpdateLiuYi2023BossStageInfo",self,"OnUpdateLiuYi2023BossStageInfo")
    g_ScriptEvent:AddListener("SyncTangGuoGuiNpcHp", self, "OnSyncTangGuoGuiNpcHp")
end

function LuaLiuYi2023BossTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateLiuYi2023BossStageInfo",self,"OnUpdateLiuYi2023BossStageInfo")
    g_ScriptEvent:RemoveListener("SyncTangGuoGuiNpcHp", self, "OnSyncTangGuoGuiNpcHp")
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaLiuYi2023BossTaskView:OnSyncTangGuoGuiNpcHp(NpcLeftHp)
    self:Refresh(NpcLeftHp)
end

function LuaLiuYi2023BossTaskView:OnUpdateLiuYi2023BossStageInfo()
	self:UpdateInfo()
end

function LuaLiuYi2023BossTaskView:UpdateInfo()
	self.TaskName.text = LuaLiuYi2023Mgr.m_BossTaskName
    self.DescLab.text = CUICommonDef.TranslateToNGUIText(LuaLiuYi2023Mgr.m_BossDesc)
    self.StatesRoot:Reposition()
end

function LuaLiuYi2023BossTaskView:OnLeaveButtonClick()
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaLiuYi2023BossTaskView:OnRuleButtonClick()
    g_MessageMgr:ShowMessage("LIUYI2023_TANGGUOGUI_RULE")
end

function LuaLiuYi2023BossTaskView:Refresh(progress)
    self.HpSlider.value = progress * 0.01
    self.SliderLab.text = SafeStringFormat3("%.f%%", progress)
end

function LuaLiuYi2023BossTaskView:OnTick()
    if CScene.MainScene ~= nil then
        if CScene.MainScene.ShowTimeCountDown then
            self.CountDownLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.CountDownLabel.text = nil
        end
    else
        self.CountDownLabel.text = nil
    end
end

function LuaLiuYi2023BossTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return SafeStringFormat3(LocalString.GetString("剩余时间： %s"), time)
end

function LuaLiuYi2023BossTaskView:OnDestroy()
    LuaLiuYi2023Mgr.m_BossTaskName = ""
    LuaLiuYi2023Mgr.m_BossDesc = ""
end