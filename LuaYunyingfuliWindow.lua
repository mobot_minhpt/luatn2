local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local GameObject = import "UnityEngine.GameObject"
local Vector3 = import "UnityEngine.Vector3"

LuaYunyingfuliWindow=class()
RegistChildComponent(LuaYunyingfuliWindow,"btn1", GameObject)
RegistChildComponent(LuaYunyingfuliWindow,"btn2", GameObject)
RegistChildComponent(LuaYunyingfuliWindow,"btn3", GameObject)
RegistChildComponent(LuaYunyingfuliWindow,"node1", GameObject)
RegistChildComponent(LuaYunyingfuliWindow,"node2", GameObject)
RegistChildComponent(LuaYunyingfuliWindow,"node3", GameObject)

function LuaYunyingfuliWindow:OnEnable()
  self:InitData()
end

function LuaYunyingfuliWindow:InitData()
  self.btn1:SetActive(false)
  self.btn2:SetActive(false)
  self.btn3:SetActive(false)
  self.node1:SetActive(false)
  self.node2:SetActive(false)
  self.node3:SetActive(false)
  local btnTable = {self.btn1,self.btn2,self.btn3}
  local nodeTable = {self.node1,self.node2,self.node3}
  if LuaYunyingfuliMgr.fuliData then
    local count = #LuaYunyingfuliMgr.fuliData
    local fNode = nodeTable[count]
    if fNode then
      fNode:SetActive(true)
      for i,v in pairs(LuaYunyingfuliMgr.fuliData) do
        local fBtnNode = fNode.transform:Find(i)
        local btnNode = btnTable[v]
        if fBtnNode and btnNode then
          btnNode:SetActive(true)
          btnNode.transform.parent = fBtnNode
          btnNode.transform.localPosition = Vector3.zero
          if v == 1 then
            local onClick = function(go)
              CLuaShopMallMgr:OpenZhouBianMallShop()
            end
            CommonDefs.AddOnClickListener(btnNode,DelegateFactory.Action_GameObject(onClick),false)
          elseif v == 2 then
            local onClick = function(go)
              CUIManager.ShowUI("CCLiveListWnd")
            end
            CommonDefs.AddOnClickListener(btnNode,DelegateFactory.Action_GameObject(onClick),false)
          elseif v == 3 then
            local onClick = function(go)
              LuaJingLingMgr.OpenSDKJingLing()
            end
            CommonDefs.AddOnClickListener(btnNode,DelegateFactory.Action_GameObject(onClick),false)
          end
        end
      end
    end
  end
end

return LuaYunyingfuliWindow
