-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CTeamFightData = import "L10.UI.CTeamFightData"
local CTeamFightDataItem = import "L10.UI.CTeamFightDataItem"
local CTeamFightDataWnd = import "L10.UI.CTeamFightDataWnd"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTeamFightDataWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.ReplyTeamFightData, MakeDelegateFromCSFunction(this.ReplyTeamFightData, MakeGenericClass(Action1, MakeGenericClass(List, CTeamFightData)), this))
    UIEventListener.Get(this.m_CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseBtn).onClick, MakeDelegateFromCSFunction(this.CloseWnd, VoidDelegate, this), true)
    UIEventListener.Get(this.m_ClearBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ClearBtn).onClick, MakeDelegateFromCSFunction(this.ClearData, VoidDelegate, this), true)
    this.m_ClearBtn:SetActive(false)
    this.m_PlayerItemObj:SetActive(false)
end
CTeamFightDataWnd.m_ReplyTeamFightData_CS2LuaHook = function (this, data) 
    Extensions.RemoveAllChildren(this.m_Grid.transform)
    do
        local i = 0 local cnt = data.Count
        while i < cnt do
            local obj = NGUITools.AddChild(this.m_Grid.gameObject, this.m_PlayerItemObj)
            obj:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CTeamFightDataItem))
            if item ~= nil then
                item:UpdateItem(data[i])
            end
            i = i + 1
        end
    end
    this.m_Grid:Reposition()
end
