CLuaForcesMgr = class()


local EnumObjectType = import "L10.Game.EnumObjectType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientFakePlayer = import "L10.Game.CClientFakePlayer"
local CGameVideoPlayer = import "L10.Game.CGameVideoPlayer"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CLingshouFurnitureProp = import "L10.Game.CLingshouFurnitureProp"
local CClientMonster = import "L10.Game.CClientMonster"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"

CLuaForcesMgr.IsEnemyForPlay_House_Old= function (character1, character2) 
    if character1 == nil or character2 == nil or not CClientHouseMgr.Inst:IsPlayerInHouse() then
        return false, false
    end

    local owner1 = character1
    if owner1:GetOwner() ~= nil then
        owner1 = owner1:GetOwner()
    end
    local owner2 = character2
    if owner2:GetOwner() ~= nil then
        owner2 = owner2:GetOwner()
    end
    local playerId = 0
    local puppetEngineId = 0
    if (owner1.ObjectType == EnumObjectType.Monster or owner1.ObjectType == EnumObjectType.FakePlayer) and owner2.ObjectType == EnumObjectType.Player then
        puppetEngineId = owner1.EngineId
        if TypeIs(owner2, typeof(CClientOtherPlayer)) then
            playerId = owner2.PlayerId
        elseif TypeIs(owner2, typeof(CClientMainPlayer)) then
            playerId = owner2.Id
        end
    elseif (owner2.ObjectType == EnumObjectType.Monster or owner2.ObjectType == EnumObjectType.FakePlayer) and owner1.ObjectType == EnumObjectType.Player then
        puppetEngineId = owner2.EngineId
        if TypeIs(owner1, typeof(CClientOtherPlayer)) then
            playerId = owner1.PlayerId
        elseif TypeIs(owner1, typeof(CClientMainPlayer)) then
            playerId = owner1.Id
        end
    end
    if CHousePuppetMgr.Inst:IsPuppet(owner1.EngineId) or CHousePuppetMgr.Inst:IsPuppet(owner2.EngineId) then
        local targetPlayerId = 0
        local __try_get_result
         __try_get_result, targetPlayerId = CommonDefs.DictTryGet(CClientFurnitureMgr.Inst.PuppetFightInfo, typeof(UInt32), puppetEngineId, typeof(UInt64))
        return true, targetPlayerId == playerId
    end

    if CClientHouseMgr.Inst.mCurBasicProp == nil or CClientHouseMgr.Inst.mCurFurnitureProp == nil then
        return false, false
    end

    local engineId1 = character1.EngineId
    local engineId2 = character2.EngineId
    local fur = nil
    local otherObj = nil
    local fur1 = CClientFurnitureMgr.Inst:GetFurnitureByEngineId(engineId1)
    local fur2 = CClientFurnitureMgr.Inst:GetFurnitureByEngineId(engineId2)
    if fur1 ~= nil and fur2 == nil then
        fur = fur1
        otherObj = character2
    elseif fur1 == nil and fur2 ~= nil then
        fur = fur2
        otherObj = character1
    elseif fur1 ~= nil and fur2 ~= nil then
        return true, false
    end
    if otherObj == nil or fur == nil then
        return false, false
    end
    local prop = nil

    local __try_get_result
    __try_get_result, prop = CommonDefs.DictTryGet(CClientHouseMgr.Inst.mCurFurnitureProp.LingshouInfo, typeof(UInt32), fur.ID, typeof(CLingshouFurnitureProp))
    if not __try_get_result then
        return false, false
    end
    local player = otherObj
    if otherObj.ObjectType == EnumObjectType.LingShou or otherObj.ObjectType == EnumObjectType.Pet or otherObj.ObjectType == EnumObjectType.Flag then
        player = otherObj:GetOwner()
    end

    local houseid = CClientHouseMgr.Inst.mCurBasicProp.Id
    local ownHouseId = ""
    if TypeIs(player, typeof(CClientMainPlayer)) then
        ownHouseId = player.ItemProp.HouseId
    end
    if houseid == ownHouseId then
        return true, false
    end
    if prop ~= nil and prop.IsDead > 0 then
        return true, false
    end
    return true, true
end



function CLuaForcesMgr.IsEnemyForPlay_House(obj1, obj2)
	local character1, character2 = obj1, obj2
	if (not character1 or not character2) then 
		return false, false 
	end

	local owner1 = character1;
	if (owner1:GetOwner() ~= nil) then owner1 = owner1:GetOwner() end
	local owner2 = character2
	if (owner2:GetOwner() ~= nil) then owner2 = owner2:GetOwner() end
	local playerId = 0
	local pileEngineId = 0
	local pileTempId = 0
	if (owner1.ObjectType == EnumObjectType.Monster and owner2.ObjectType == EnumObjectType.Player and TypeIs(owner2, typeof(CClientMainPlayer))) then
		pileEngineId = owner1.EngineId
		playerId = owner2.Id
		pileTempId = owner1.TemplateId
	elseif (owner1.ObjectType == EnumObjectType.Player and TypeIs(owner1, typeof(CClientMainPlayer)) and owner2.ObjectType == EnumObjectType.Monster) then
		pileEngineId = owner2.EngineId
		playerId = owner1.Id
		pileTempId = owner2.TemplateId
	end

	local dict = CClientFurnitureMgr.Inst.WoodPileClientObjectList
	local pileId = dict and CommonDefs.DictGetValue_LuaCall(dict,pileEngineId)
	if pileId then
		local monsterForce = Monster_Monster.GetData(pileTempId).Force
		local targetId = CLuaHouseWoodPileMgr.m_FightStatus and CLuaHouseWoodPileMgr.m_FightStatus[pileEngineId]
		if (targetId) then
			if monsterForce == 4120010 then
				return true, playerId == targetId
			elseif monsterForce == 4120020 then
				return true, playerId ~= targetId
			end
		else
			if monsterForce == 4120010 then
				return true, false
			elseif monsterForce == 4120020 then
				return true, false	-- 这里有点tricky
			end
		end
	end


	return CLuaForcesMgr.IsEnemyForPlay_House_Old(obj1, obj2)
end


CCastSkillCheckFuncs = {}


function CCastSkillCheckFuncs.CastSkillCheckInPlay_House(skillId)
	local target = CClientMainPlayer.Inst.Target
	if (not target) then return true end
	local playerId = 0
	local pileEngineId = 0
	local pileTempId = 0
	if (target.ObjectType == EnumObjectType.Monster) then
		pileEngineId = target.EngineId
		playerId = CClientMainPlayer.Inst.Id
		pileTempId = target.TemplateId
	end

	local dict = CClientFurnitureMgr.Inst.WoodPileClientObjectList
	local pileId = dict and CommonDefs.DictGetValue_LuaCall(dict,pileEngineId)
	if pileId then
		local monsterForce = Monster_Monster.GetData(pileTempId).Force
		local targetId = CLuaHouseWoodPileMgr.m_FightStatus and CLuaHouseWoodPileMgr.m_FightStatus[pileEngineId]
		if not targetId and monsterForce == 4120020 then
			return false
		end
	end

	return true
end

function CCastSkillCheckFuncs.CastSkillCheckInPlay_TangYuan2022(skillId)
	LuaTangYuan2022Mgr:InitGraySkillIdDict()
	local skillCls = math.floor(skillId / 100)
	if LuaTangYuan2022Mgr.GraySkillIdDict[skillCls] then
		return false
	end

	return true
end

function CCastSkillCheckFuncs.CastSkillCheckInPlay_JinLuHunYuanZhan(skillId)
    local skillCls = math.floor(skillId / 100)

    if not CSkillMgr.Inst:IsTempSkill(skillCls) then
        return false
    end
    
    return true
end

function CCastSkillCheckFuncs.CastSkillCheckInPlay_DuanWu2023PVPVE(skillId)
    return LuaDuanWu2023Mgr:CanCastSkillInPVPVE(skillId)
end

function CCastSkillCheckFuncs.CastSkillCheckInPlay_Double11MQLD(skillId)
    return LuaShuangshiyi2023Mgr:CanCastSkillInMengQuanLuanDou(skillId)
end

function CCastSkillCheckFuncs.CastSkillCheckInPlay_CandyDelivery(skillId)
    return LuaHalloween2023Mgr:CastSkillCheckInCandyDelivery(skillId)
end

CSkillSetCondCheckFuncs = {}

function CLuaForcesMgr.IsEnemyForPlay_AnQiIsland(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local force1 = 0
    local force2 = 0
    local bFind1 = false
    local bFind2 = false
    local playerId1 = 0
    local playerId2 = 0
    local player1 = character1
    local player2 = character2

    if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
        player1 = character1:GetOwner()
    elseif character1.ObjectType == EnumObjectType.GVLingShou or character1.ObjectType == EnumObjectType.GVPet or character1.ObjectType == EnumObjectType.GVFlag then
        player1 = character1:GetOwner()
    end
    if player1 ~= nil then
        if TypeIs(player1, typeof(CClientMainPlayer)) then
            playerId1 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player1, typeof(CClientFakePlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
            playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
        end
        if playerId1 > 0 then
            bFind1, force1 = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, playerId1)
        end
    end

    if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
        player2 = character2:GetOwner()
    elseif character2.ObjectType == EnumObjectType.GVLingShou or character2.ObjectType == EnumObjectType.GVPet or character2.ObjectType == EnumObjectType.GVFlag then
        player2 = character2:GetOwner()
    end

    if player2 ~= nil then
        if TypeIs(player2, typeof(CClientMainPlayer)) then
            playerId2 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player2, typeof(CClientFakePlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
            playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
        end

        if playerId2 > 0 and playerId1 ~= playerId2 then
            bFind2, force2 = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, playerId2)
        end
    end

    if bFind1 and bFind2 then
        return true, force1 ~= force2
    else
        return false, false
    end
end

function CLuaForcesMgr.IsEnemyForPlay_Sect(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end
 
    local player1 = character1
    local player2 = character2
    if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
        player1 = character1:GetOwner()
    end

    if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
        player2 = character2:GetOwner()
    end

    if player1 == player2 then
        return true, false
    end

    if TypeIs(player1, typeof(CClientMainPlayer)) and TypeIs(player2, typeof(CClientMonster)) then
        local obj1 = TypeAs(player1, typeof(CClientMainPlayer))
        local obj2 = TypeAs(player2, typeof(CClientMonster))
        if obj2.OwnerGuildId == 0 then return false, false end
        return true, obj1.BasicProp.SectId ~= obj2.OwnerGuildId
    elseif TypeIs(player1, typeof(CClientMonster)) and TypeIs(player2, typeof(CClientMainPlayer)) then
        local obj1 = TypeAs(player1, typeof(CClientMonster))
        local obj2 = TypeAs(player2, typeof(CClientMainPlayer))
        if obj1.OwnerGuildId == 0 then return false, false end
        return true, obj2.BasicProp.SectId ~= obj1.OwnerGuildId
    else
        return false, false
    end
end


function CLuaForcesMgr.IsEnemyForPlay_CaiDie(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local playerId1 = -1
    local playerId2 = -1
    local player1 = character1
    local player2 = character2

    if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
        player1 = character1:GetOwner()
    elseif character1.ObjectType == EnumObjectType.GVLingShou or character1.ObjectType == EnumObjectType.GVPet or character1.ObjectType == EnumObjectType.GVFlag then
        player1 = character1:GetOwner()
    end
    if player1 ~= nil then
        if TypeIs(player1, typeof(CClientMainPlayer)) then
            playerId1 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player1, typeof(CClientFakePlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
            playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
        end
    end

    if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
        player2 = character2:GetOwner()
    elseif character2.ObjectType == EnumObjectType.GVLingShou or character2.ObjectType == EnumObjectType.GVPet or character2.ObjectType == EnumObjectType.GVFlag then
        player2 = character2:GetOwner()
    end
    if player2 ~= nil then
        if TypeIs(player2, typeof(CClientMainPlayer)) then
            playerId2 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player2, typeof(CClientFakePlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
            playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
        end
    end

    if playerId1 ~= -1 and playerId2 ~= -1 then
        if LuaOffWorldPassMgr.cursedPlayerId == 0 then
            return true, false
        end
        if playerId1 == playerId2 then
            return true, false
        elseif playerId1 == LuaOffWorldPassMgr.cursedPlayerId or playerId2 == LuaOffWorldPassMgr.cursedPlayerId then
            return true, true
        else
            return true, false
        end
    end
    return false, false
end

--- 泛用检测，会检测玩家与玩家、玩家与怪物、怪物与怪物之间的敌对关系
function CLuaForcesMgr.IsEnemyForPlay_Common(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local force1
    local force2

    local monsterForceDict = CForcesMgr.Inst.m_MonsterForceInfo
    local playerForceDict  = CForcesMgr.Inst.m_PlayForceInfo

    if character1.ObjectType == EnumObjectType.Monster then
        force1 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character1.EngineId)
        if not force1 then
            return true, true
        end
    end

    if character2.ObjectType == EnumObjectType.Monster then
        force2 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character2.EngineId)
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    end

    local player1 = character1
    local player2 = character2
    local playerId1 = 0
    local playerId2 = 0

    if not force1 then
        if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
            player1 = character1:GetOwner()
        end
        if player1 then
            if TypeIs(player1, typeof(CClientMainPlayer)) then
                playerId1 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player1, typeof(CClientFakePlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
                playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId1 > 0 then
                force1 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId1)
            end
        end
        if not force1 then
            return true, true
        end
    end

    if not force2 then
        if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
            player2 = character2:GetOwner()
        end
        if player2 then
            if TypeIs(player2, typeof(CClientMainPlayer)) then
                playerId2 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player2, typeof(CClientFakePlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
                playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId2 > 0 then
                force2 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId2)
            end
        end
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    else
        return false, false
    end
end

function CLuaForcesMgr.IsEnemyForPlay_GuanNingNewMode(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local force1
    local force2

    local monsterForceDict = CForcesMgr.Inst.m_MonsterForceInfo
    local playerForceDict  = CForcesMgr.Inst.m_PlayForceInfo

    if character1.ObjectType == EnumObjectType.Monster then
        force1 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character1.EngineId)
        if not force1 then
            return true, true
        end
    end

    if character2.ObjectType == EnumObjectType.Monster then
        force2 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character2.EngineId)
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    end

    local player1 = character1
    local player2 = character2
    local playerId1 = 0
    local playerId2 = 0

    if not force1 then
        if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
            player1 = character1:GetOwner()
        end
        if player1 then
            if TypeIs(player1, typeof(CClientMainPlayer)) then
                playerId1 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player1, typeof(CClientFakePlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
                playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId1 > 0 then
                force1 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId1)
            end
        end
        if not force1 then
            return true, true
        end
    end

    if not force2 then
        if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
            player2 = character2:GetOwner()
        end
        if player2 then
            if TypeIs(player2, typeof(CClientMainPlayer)) then
                playerId2 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player2, typeof(CClientFakePlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
                playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId2 > 0 then
                force2 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId2)
            end
        end
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    else
        return false, false
    end
end

function CLuaForcesMgr.IsEnemyForPlay_TravestyRole(character1,character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local playerId1 = -1
    local playerId2 = -1
    local player1 = character1
    local player2 = character2

    if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
        player1 = character1:GetOwner()
    elseif character1.ObjectType == EnumObjectType.GVLingShou or character1.ObjectType == EnumObjectType.GVPet or character1.ObjectType == EnumObjectType.GVFlag then
        player1 = character1:GetOwner()
    end
    if player1 ~= nil then
        if TypeIs(player1, typeof(CClientMainPlayer)) then
            playerId1 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player1, typeof(CClientFakePlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
            playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
        end
    end

    if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
        player2 = character2:GetOwner()
    elseif character2.ObjectType == EnumObjectType.GVLingShou or character2.ObjectType == EnumObjectType.GVPet or character2.ObjectType == EnumObjectType.GVFlag then
        player2 = character2:GetOwner()
    end
    if player2 ~= nil then
        if TypeIs(player2, typeof(CClientMainPlayer)) then
            playerId2 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player2, typeof(CClientFakePlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
            playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
        end
    end

    if playerId1 ~= -1 and playerId2 ~= -1 then
        if playerId1 == playerId2 then 
            return true, false
        end
        return true, LuaShuJia2021Mgr.CheckIsSameRole(playerId1,playerId2)
    end
    return false, false
end

function CLuaForcesMgr.IsEnemyForPlay_Double11MQHX(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    return true, character1 ~= character2
end

function CLuaForcesMgr.IsEnemyForPlay_Spring2023TTSY(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    return true, character1 ~= character2
end
function CLuaForcesMgr.IsEnemyForPlay_DaFuWeng(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    return true, false
end
function CLuaForcesMgr.IsEnemyForPlay_LuoCha(character1,character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local playerId1 = -1
    local playerId2 = -1
    local player1 = character1
    local player2 = character2

    if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
        player1 = character1:GetOwner()
    elseif character1.ObjectType == EnumObjectType.GVLingShou or character1.ObjectType == EnumObjectType.GVPet or character1.ObjectType == EnumObjectType.GVFlag then
        player1 = character1:GetOwner()
    end

    if player1 ~= nil then
        if TypeIs(player1, typeof(CClientMainPlayer)) then
            playerId1 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player1, typeof(CClientFakePlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
            playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
        end
    end

    if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
        player2 = character2:GetOwner()
    elseif character2.ObjectType == EnumObjectType.GVLingShou or character2.ObjectType == EnumObjectType.GVPet or character2.ObjectType == EnumObjectType.GVFlag then
        player2 = character2:GetOwner()
    end
    if player2 ~= nil then
        if TypeIs(player2, typeof(CClientMainPlayer)) then
            playerId2 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player2, typeof(CClientFakePlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
            playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
        end
    end

    if playerId1 ~= -1 and playerId2 ~= -1 then
        if playerId1 == playerId2 then
            return true, false
        end
        local isEnemy = (CForcesMgr.Inst:GetPlayerForce(playerId1) ~= CForcesMgr.Inst:GetPlayerForce(playerId2))
        return true, isEnemy
    end
    return false, false
end


function CLuaForcesMgr.IsEnemyForPlay_YuanDan2022FuDan(character1,character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local force1
    local force2

    local monsterForceDict = CForcesMgr.Inst.m_MonsterForceInfo
    local playerForceDict  = CForcesMgr.Inst.m_PlayForceInfo

    if character1.ObjectType == EnumObjectType.Monster then
        force1 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character1.EngineId)
        if not force1 then
            return true, true
        end
    end

    if character2.ObjectType == EnumObjectType.Monster then
        force2 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character2.EngineId)
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    end

    local player1 = character1
    local player2 = character2
    local playerId1 = 0
    local playerId2 = 0

    if not force1 then
        if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
            player1 = character1:GetOwner()
        end
        if player1 then
            if TypeIs(player1, typeof(CClientMainPlayer)) then
                playerId1 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player1, typeof(CClientFakePlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
                playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId1 > 0 then
                force1 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId1)
            end
        end
        if not force1 then
            return true, true
        end
    end

    if not force2 then
        if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
            player2 = character2:GetOwner()
        end
        if player2 then
            if TypeIs(player2, typeof(CClientMainPlayer)) then
                playerId2 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player2, typeof(CClientFakePlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
                playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId2 > 0 then
                force2 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId2)
            end
        end
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    else
        return false, false
    end
end


function CLuaForcesMgr.IsEnemyForPlay_SnowballFight(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local force1 = 0
    local force2 = 0
    local bFind1 = false
    local bFind2 = false
    local playerId1 = 0
    local playerId2 = 0
    local player1 = character1
    local player2 = character2

    if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
        player1 = character1:GetOwner()
    elseif character1.ObjectType == EnumObjectType.GVLingShou or character1.ObjectType == EnumObjectType.GVPet or character1.ObjectType == EnumObjectType.GVFlag then
        player1 = character1:GetOwner()
    end
    if player1 ~= nil then
        if TypeIs(player1, typeof(CClientMainPlayer)) then
            playerId1 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player1, typeof(CClientFakePlayer)) then
            playerId1 = TypeAs(player1, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
            playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
        end
        if playerId1 > 0 then
            bFind1, force1 = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, playerId1)
        end
    end

    if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
        player2 = character2:GetOwner()
    elseif character2.ObjectType == EnumObjectType.GVLingShou or character2.ObjectType == EnumObjectType.GVPet or character2.ObjectType == EnumObjectType.GVFlag then
        player2 = character2:GetOwner()
    end

    if player2 ~= nil then
        if TypeIs(player2, typeof(CClientMainPlayer)) then
            playerId2 = CClientMainPlayer.Inst.Id
        elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientOtherPlayer)).PlayerId
        elseif TypeIs(player2, typeof(CClientFakePlayer)) then
            playerId2 = TypeAs(player2, typeof(CClientFakePlayer)).PlayerId
        elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
            playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
        end

        if playerId2 > 0 and playerId1 ~= playerId2 then
            bFind2, force2 = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, playerId2)
        end
    end

    if bFind1 and bFind2 then
        return true, force1 ~= force2
    else
        return false, false
    end
end

function CLuaForcesMgr.IsEnemyForPlay_GuildTerritoryWar(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local force1
    local force2

    local monsterForceDict = CForcesMgr.Inst.m_MonsterForceInfo
    local playerForceDict  = CForcesMgr.Inst.m_PlayForceInfo

    if character1.ObjectType == EnumObjectType.Monster then
        force1 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character1.EngineId)
        if not force1 then
            return true, true
        end
    end

    if character2.ObjectType == EnumObjectType.Monster then
        force2 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character2.EngineId)
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    end

    local player1 = character1
    local player2 = character2
    local playerId1 = 0
    local playerId2 = 0

    if not force1 then
        if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
            player1 = character1:GetOwner()
        end
        if player1 then
            if TypeIs(player1, typeof(CClientMainPlayer)) then
                playerId1 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player1, typeof(CClientFakePlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
                playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId1 > 0 then
                force1 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId1)
            end
        end
        if not force1 then
            return true, true
        end
    end

    if not force2 then
        if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
            player2 = character2:GetOwner()
        end
        if player2 then
            if TypeIs(player2, typeof(CClientMainPlayer)) then
                playerId2 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player2, typeof(CClientFakePlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
                playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId2 > 0 then
                force2 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId2)
            end
        end
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    else
        return false, false
    end
end

-- 据点战 根据帮会id判断 是否是敌人
function CLuaForcesMgr.IsEnemyForPlay_GuildJuDian(character1, character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    if character1.ObjectType == EnumObjectType.Monster or character2.ObjectType == EnumObjectType.Monster then
        return true, true
    end

    local player1 = character1
    local player2 = character2

    if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
        player1 = character1:GetOwner()
    end
    if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
        player2 = character2:GetOwner()
    end

    if player1 and player2 and player1.ObjectType == EnumObjectType.Player and player2.ObjectType == EnumObjectType.Player then
        local guildId1 = player1.BasicProp.GuildId
        local guildId2 = player2.BasicProp.GuildId
        return true, guildId1 ~= guildId2
    end

    return false, false
end

function CCastSkillCheckFuncs.CastSkillCheckInPlay_QingMing2022(skillId)
	local skillCls = math.floor(skillId / 100)

    if not CSkillMgr.Inst:IsTempSkill(skillCls) then
        return false
    end
	return true
end

function CCastSkillCheckFuncs.CastSkillCheckInPlay_ChristmasKindnessGuildPlay(skillId)
	local skillCls = math.floor(skillId / 100)

    if not CSkillMgr.Inst:IsTempSkill(skillCls) then
        return false
    end
	return true
end

function CLuaForcesMgr.IsEnemyForPlay_GuShuNingZhu(character1,character2)
    if character1 == nil or character2 == nil then
        return false, false
    end

    local force1
    local force2

    local monsterForceDict = CForcesMgr.Inst.m_MonsterForceInfo
    local playerForceDict  = CForcesMgr.Inst.m_PlayForceInfo

    if character1.ObjectType == EnumObjectType.Monster then
        force1 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character1.EngineId)
        if not force1 then
            return true, true
        end
    end

    if character2.ObjectType == EnumObjectType.Monster then
        force2 = CommonDefs.DictGetValue_LuaCall(monsterForceDict, character2.EngineId)
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    end

    local player1 = character1
    local player2 = character2
    local playerId1 = 0
    local playerId2 = 0

    if not force1 then
        if character1.ObjectType == EnumObjectType.LingShou or character1.ObjectType == EnumObjectType.Pet or character1.ObjectType == EnumObjectType.Flag then
            player1 = character1:GetOwner()
        end
        if player1 then
            if TypeIs(player1, typeof(CClientMainPlayer)) then
                playerId1 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player1, typeof(CClientOtherPlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player1, typeof(CClientFakePlayer)) then
                playerId1 = (TypeAs(player1, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player1, typeof(CGameVideoPlayer)) then
                playerId1 = TypeAs(player1, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId1 > 0 then
                force1 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId1)
            end
        end
        if not force1 then
            return true, true
        end
    end

    if not force2 then
        if character2.ObjectType == EnumObjectType.LingShou or character2.ObjectType == EnumObjectType.Pet or character2.ObjectType == EnumObjectType.Flag then
            player2 = character2:GetOwner()
        end
        if player2 then
            if TypeIs(player2, typeof(CClientMainPlayer)) then
                playerId2 = CClientMainPlayer.Inst.Id
            elseif TypeIs(player2, typeof(CClientOtherPlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientOtherPlayer))).PlayerId
            elseif TypeIs(player2, typeof(CClientFakePlayer)) then
                playerId2 = (TypeAs(player2, typeof(CClientFakePlayer))).PlayerId
            elseif TypeIs(player2, typeof(CGameVideoPlayer)) then
                playerId2 = TypeAs(player2, typeof(CGameVideoPlayer)).PlayerId
            end

            if playerId2 > 0 then
                force2 = CommonDefs.DictGetValue_LuaCall(playerForceDict, playerId2)
            end
        end
        if not force2 then
            return true, true
        end
    end

    if force1 and force2 then
        return true, force1 ~= force2
    else
        return false, false
    end
end

function CLuaForcesMgr.IsEnemyForPlay_TurkeyMatch(character1, character2)
    return CLuaForcesMgr.IsEnemyForPlay_Common(character1, character2)
end

function CLuaForcesMgr.IsEnemyForPlay_Double11MQLD(character1, character2)
    return CLuaForcesMgr.IsEnemyForPlay_Common(character1, character2)
end