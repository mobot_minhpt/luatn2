local Input = import "UnityEngine.Input"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Profession = import "L10.Game.Profession"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"

CLuaCrossDouDiZhuRankWnd = class()

RegistClassMember(CLuaCrossDouDiZhuRankWnd,"m_JiangJinLabel")
RegistClassMember(CLuaCrossDouDiZhuRankWnd,"m_TableView")
RegistClassMember(CLuaCrossDouDiZhuRankWnd,"m_RankList")
RegistClassMember(CLuaCrossDouDiZhuRankWnd,"m_JiangJinLabel")
RegistClassMember(CLuaCrossDouDiZhuRankWnd,"m_IsGM")

CLuaCrossDouDiZhuRankWnd.m_IsAllRank = true--总排名

function CLuaCrossDouDiZhuRankWnd:Init()

    local titleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    titleLabel.text = CLuaCrossDouDiZhuRankWnd.m_IsAllRank and LocalString.GetString("积分排行") or LocalString.GetString("本场积分排行")

    self.m_JiangJinLabel = self.transform:Find("JiangJinLabel"):GetComponent(typeof(UILabel))
    self.m_JiangJinLabel.text = "0"

    self.m_RankList = {}

    self:InitMainInfo(nil)

    self.m_TableView=self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item,index+1,self.m_RankList[index+1])
        end)

    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        local data=self.m_RankList[row+1]
        if data then
            if not self.m_IsGM  then--非GM
                CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
            else
                local list={
                    PopupMenuItemData(LocalString.GetString("观战"), DelegateFactory.Action_int(function (index) 
                        Gac2Gas.CrossDouDiZhuRequestWatchPlayer(data.playerId)
                    end), false, nil)
                }
                local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
                CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(list,MakeArrayClass(PopupMenuItemData)), pos, CPopupMenuInfoMgr.AlignType.Right)
            end
        end
    end)
    if CLuaCrossDouDiZhuRankWnd.m_IsAllRank then
        local scoreLabel = self.transform:Find("Header/HeadLabel4"):GetComponent(typeof(UILabel))
        scoreLabel.text = LocalString.GetString("牌神积分")
        Gac2Gas.CrossDouDiZhuQueryHaiXuanRank()
    else
        Gac2Gas.CrossDouDiZhuRequestShowPlayInfo()
    end

end

function CLuaCrossDouDiZhuRankWnd:InitMainInfo(rank,cls,name,serverName,score,time,applyTime)
    local tf = self.transform:Find("MainPlayerInfo")
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local serverLabel = tf:Find("ServerLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local timeLabel = tf:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local applyTimeLabel = tf:Find("ApplyTimeLabel"):GetComponent(typeof(UILabel))
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))

    rankLabel.text = rank and tostring(rank) or LocalString.GetString("未上榜")
    nameLabel.text = name and name or "—"
    serverLabel.text = serverName and serverName or "—"
    scoreLabel.text = score and tostring(score) or "—"
    timeLabel.text = time and self:FormatTime(time) or "—"
    clsSprite.spriteName = cls and Profession.GetIconByNumber(cls) or ""

    if applyTime then
    local date = CServerTimeMgr.ConvertTimeStampToZone8Time(applyTime)
    applyTimeLabel.text = SafeStringFormat3("%d-%d %02d:%02d", date.Month,date.Day,date.Hour,date.Minute)
    else
        applyTimeLabel.text = "—"
    end
    -- applyTimeLabel.text = applyTime and tostring(applyTime) or "—"
end

function CLuaCrossDouDiZhuRankWnd:InitItem( item,index,info )
    local tf = item.transform
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local serverLabel = tf:Find("ServerLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local timeLabel = tf:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local applyTimeLabel = tf:Find("ApplyTimeLabel"):GetComponent(typeof(UILabel))
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))

    rankLabel.text = tostring(index)
    nameLabel.text = info.Name
    serverLabel.text = info.ServerName
    if CLuaCrossDouDiZhuRankWnd.m_IsAllRank then
        scoreLabel.text = info.HaiXuanScore
        timeLabel.text = self:FormatTime(info.HaiXuanUsedTime)
        clsSprite.spriteName = Profession.GetIconByNumber(info.Class)

    else
        scoreLabel.text = info.Score
        timeLabel.text = self:FormatTime(info.UsedTime)
    end
        clsSprite.spriteName = Profession.GetIconByNumber(info.Class)
        local date = CServerTimeMgr.ConvertTimeStampToZone8Time(info.FirstSignUpTime)
    applyTimeLabel.text = SafeStringFormat3("%d-%d %02d:%02d", date.Month,date.Day,date.Hour,date.Minute)
end

function CLuaCrossDouDiZhuRankWnd:FormatTime(time)
    local minute=math.floor(time/60)
    local second=time%60
    return SafeStringFormat3("%02d:%02d",minute,second)
end
function CLuaCrossDouDiZhuRankWnd:OnEnable()
    g_ScriptEvent:AddListener("CrossDouDiZhuQueryHaiXuanRankResult", self, "OnCrossDouDiZhuQueryHaiXuanRankResult")
    g_ScriptEvent:AddListener("CrossDouDiZhuRequestShowPlayInfoResult", self, "OnCrossDouDiZhuRequestShowPlayInfoResult")
end

function CLuaCrossDouDiZhuRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("CrossDouDiZhuQueryHaiXuanRankResult", self, "OnCrossDouDiZhuQueryHaiXuanRankResult")
    g_ScriptEvent:RemoveListener("CrossDouDiZhuRequestShowPlayInfoResult", self, "OnCrossDouDiZhuRequestShowPlayInfoResult")
end


function CLuaCrossDouDiZhuRankWnd:OnCrossDouDiZhuRequestShowPlayInfoResult(rank,silverPool,bGM)
    self.m_IsGM = bGM
    local jiangjin = CLuaCrossDouDiZhuMgr.GetJiangJin(silverPool)
    self.m_JiangJinLabel.text = tostring(jiangjin[1])

    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    for i,v in ipairs(rank) do
        if v.playerId==playerId then
            self:InitMainInfo(i,v.Class,v.Name,v.ServerName,v.Score,v.UsedTime,v.FirstSignUpTime)
            break
        end
    end

    self.m_RankList = rank
    self.m_TableView:ReloadData(true,false)
    
end

function CLuaCrossDouDiZhuRankWnd:OnCrossDouDiZhuQueryHaiXuanRankResult(t,silverPool,v)
    local jiangjin = CLuaCrossDouDiZhuMgr.GetJiangJin(silverPool)
    self.m_JiangJinLabel.text = tostring(jiangjin[1])

    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

    local rank = nil
    for i,info in ipairs(t) do
        if info.playerId==playerId then
            rank = i
            break
        end
    end
    self:InitMainInfo(rank,v.Class,v.Name,v.ServerName,v.HaiXuanScore,v.HaiXuanUsedTime,v.FirstSignUpTime)

    self.m_RankList = t
    self.m_TableView:ReloadData(true,false)
end

function CLuaCrossDouDiZhuRankWnd:OnDestroy()
    CLuaCrossDouDiZhuRankWnd.m_IsAllRank = true
end
