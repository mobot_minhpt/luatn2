local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UILabel = import "UILabel"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaWuYi2023XXSCExchangeGoldWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023XXSCExchangeGoldWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaWuYi2023XXSCExchangeGoldWnd, "ExchangeButton", "ExchangeButton", GameObject)
RegistChildComponent(LuaWuYi2023XXSCExchangeGoldWnd, "OwnInfoLabel", "OwnInfoLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCExchangeGoldWnd, "CostInfoLabel", "CostInfoLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCExchangeGoldWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCommonLuaScript)
RegistChildComponent(LuaWuYi2023XXSCExchangeGoldWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaWuYi2023XXSCExchangeGoldWnd, "TopLabel", "TopLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023XXSCExchangeGoldWnd,"m_TabIndex")

function LuaWuYi2023XXSCExchangeGoldWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExchangeButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023XXSCExchangeGoldWnd:Init()
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnTabChange(go, index) end)
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (v)
        self:OnValueChanged(v)
    end)
	self.QnIncreseAndDecreaseButton:SetMinMax(1, 999, 1)
	self.TabBar:ChangeTab(LuaWuYi2023Mgr.m_IsExchangeGold and 0 or 1, false)
end

function LuaWuYi2023XXSCExchangeGoldWnd:SetCost()
	local num = self.QnIncreseAndDecreaseButton:GetValue()
	local jade2Gold = WuYi2023_XinXiangShiCheng.GetData().Jade2Gold
	local ExchangeRatio = WuYi2023_XinXiangShiCheng.GetData().ExchangeRatio
	self.QnCostAndOwnMoney:SetCost((self.m_TabIndex == 0 and jade2Gold or ExchangeRatio) * num) 
end

function LuaWuYi2023XXSCExchangeGoldWnd:OnDisable()
	LuaWuYi2023Mgr.m_IsExchangeGold = false
end
--@region UIEvent

function LuaWuYi2023XXSCExchangeGoldWnd:OnExchangeButtonClick()
	if not self.QnCostAndOwnMoney:IsEnough() then
		if self.m_TabIndex == 0 then
			local txt = g_MessageMgr:FormatMessage("XinXiangShiCheng_JadeNotEnough")
			MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                
            end), DelegateFactory.Action(function ()
                CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCExchangeGoldWnd)
            end), LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		else
			local txt = g_MessageMgr:FormatMessage("XinXiangShiCheng_YuanBaoNotEnough")
			MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                
            end), DelegateFactory.Action(function ()
                CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCExchangeGoldWnd)
            end), LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		end
		return
	end
	Gac2Gas.XinXiangShiChengExchangeGold(self.QnIncreseAndDecreaseButton:GetValue(),self.m_TabIndex + 1, self.m_TabIndex == 0 and 1 or 3)
	CUIManager.CloseUI(CLuaUIResources.WuYi2023XXSCExchangeGoldWnd)
end

function LuaWuYi2023XXSCExchangeGoldWnd:OnTabChange(go, index)
	local jade2Gold = WuYi2023_XinXiangShiCheng.GetData().Jade2Gold
	local ExchangeRatio = WuYi2023_XinXiangShiCheng.GetData().ExchangeRatio
	local exchangeTimesPerDay = WuYi2023_XinXiangShiCheng.GetData().ExchangeTimeLimit
	local v = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXinXiangShiChengExchangeTimes) or 0

	self.m_TabIndex = index
	self.QnIncreseAndDecreaseButton:SetMinMax(1, math.max(1,self.m_TabIndex == 0 and 999 or (exchangeTimesPerDay - v)), 1)
	self.TopLabel.color = Color.white
	self.OwnInfoLabel.text = index == 0 and LocalString.GetString("拥有灵玉") or LocalString.GetString("拥有元宝")
	self.CostInfoLabel.text = index == 0 and LocalString.GetString("消耗灵玉") or LocalString.GetString("消耗元宝")
	self.TopLabel.text = index == 0 and SafeStringFormat3(LocalString.GetString("[acf8ff]%d灵玉可兑换1金印"), jade2Gold) or
		SafeStringFormat3(LocalString.GetString("[acf8ff]%d元宝可兑换1铜印(每日可兑换[fffe91]%d/%d[acf8ff]次)"), ExchangeRatio, exchangeTimesPerDay - v,exchangeTimesPerDay) 
	local bindLingYu = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BindJade or 0
	self.QnCostAndOwnMoney:SetType(index == 0 and (bindLingYu > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu) or EnumMoneyType.YuanBao, EnumPlayScoreKey.NONE, false)
	self:SetCost() 
	local title = index == 0 and LocalString.GetString("金印兑换") or LocalString.GetString("铜印兑换")
	self.transform:Find("Wnd_Bg_Secondary_2/TitleLabel"):GetComponent(typeof(UILabel)).text = title
	self.transform:Find("Anchor/QnIncreseAndDecreaseButton/Label2"):GetComponent(typeof(UILabel)).text = title
	self.ExchangeButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = title
end

function LuaWuYi2023XXSCExchangeGoldWnd:OnValueChanged(val)
	self:SetCost()
end
--@endregion UIEvent

