-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpace_CircleGenerateType = import "L10.Game.CPersonalSpace_CircleGenerateType"
local CPersonalSpaceHotMomentsView = import "L10.UI.CPersonalSpaceHotMomentsView"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local L10 = import "L10"
local Object = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UITexture = import "UITexture"
CPersonalSpaceHotMomentsView.m_UpdateHotMomentsScrollView_CS2LuaHook = function (this, data, generateType, savePos)
    if data.data.list == nil or (data.data.list ~= nil and data.data.list.Length < CPersonalSpaceHotMomentsView.MaxEveryGetNum) then
        this.statusListScrollViewIndicator.enableShowNode = false
    end

    if data.code == 0 then
        if data.data.list ~= nil and data.data.list.Length > 0 then
            CPersonalSpaceWnd.Instance:updateMomentCache(data.data.list, false, generateType, this.gameObject, false)
        end
    end
    if generateType == CPersonalSpace_CircleGenerateType.HotMoments then
        CPersonalSpaceWnd.Instance:InitMomentCircleList(this.statusListScrollView, this.statusListPanel, this.statusListTable, generateType, CPersonalSpaceMgr.Inst.hotMomentList, this.sendStatusPanel_emptyNode, savePos, 0)
    elseif generateType == CPersonalSpace_CircleGenerateType.AllHotMoments then
        CPersonalSpaceWnd.Instance:InitMomentCircleList(this.statusListScrollView, this.statusListPanel, this.statusListTable, generateType, CPersonalSpaceMgr.Inst.allhotMomentList, this.sendStatusPanel_emptyNode, savePos, 0)
    end
end
CPersonalSpaceHotMomentsView.m_SetShowMoreBtn_CS2LuaHook = function (this, showMoreBtn, genType, targetid)
    --UIEventListener.Get(showMoreBtn).onClick = p =>
    --{
    --    showMoreBtn.SetActive(false);
    --    int lastId = 0;

    --    if (genType == CPersonalSpace_CircleGenerateType.HotMoments)
    --    {
    --        if (CPersonalSpaceMgr.Inst.hotMomentList.Count > 0)
    --        {
    --            CPersonalSpace_MomentCacheData lastData = CPersonalSpaceMgr.Inst.hotMomentList[CPersonalSpaceMgr.Inst.hotMomentList.Count - 1];
    --            lastId = lastData.data.hot_index;
    --        }
    --        CPersonalSpaceMgr.Inst.GetHotMoments(NowHotChoosePageNum, delegate(CPersonalSpace_GetMoments_Ret data)
    --        {
    --            if (!this || this == null)
    --                return;
    --            if (data.data.list != null)
    --            {
    --                CPersonalSpaceMgr.Inst.updateMomentCacheList(CPersonalSpaceMgr.Inst.hotMomentDic, CPersonalSpaceMgr.Inst.hotMomentList, poolNode); // save the old node for next show use, this can speed up the init
    --                for (int i = 0; i < data.data.list.Length; i++)
    --                {
    --                    data.data.list[i].type = genType;
    --                }
    --                UpdateHotMomentsScrollView(data, genType,true);
    --            }
    --            else
    --            {
    --                statusListScrollViewIndicator.enableShowNode = false;
    --                //TODO show no more
    --            }
    --        }, null);
    --    }
    --    else if (genType == CPersonalSpace_CircleGenerateType.AllHotMoments)
    --    {
    --        if (CPersonalSpaceMgr.Inst.allhotMomentList.Count > 0)
    --        {
    --            CPersonalSpace_MomentCacheData lastData = CPersonalSpaceMgr.Inst.allhotMomentList[CPersonalSpaceMgr.Inst.allhotMomentList.Count - 1];
    --            lastId = lastData.data.hot_index;
    --        }
    --        CPersonalSpaceMgr.Inst.GetAllHotMoments(NowAllHotChoosePageNum, delegate(CPersonalSpace_GetMoments_Ret data)
    --        {
    --            if (!this || this == null)
    --                return;
    --            if (data.data.list != null)
    --            {
    --                CPersonalSpaceMgr.Inst.updateMomentCacheList(CPersonalSpaceMgr.Inst.allhotMomentDic, CPersonalSpaceMgr.Inst.allhotMomentList, poolNode); // save the old node for next show use, this can speed up the init
    --                for (int i = 0; i < data.data.list.Length; i++)
    --                {
    --                    data.data.list[i].type = genType;
    --                }
    --                UpdateHotMomentsScrollView(data, genType, true);
    --            }
    --            else
    --            {
    --                statusListScrollViewIndicator.enableShowNode = false;
    --                //TODO show no more
    --            }
    --        }, null);
    --    }


    --};
end
CPersonalSpaceHotMomentsView.m_UpdateHotList_CS2LuaHook = function (this, page)
    if page > 0 then
        page = page - 1
    end

    CommonDefs.ListClear(CPersonalSpaceMgr.Inst.hotMomentList)
    CommonDefs.DictClear(CPersonalSpaceMgr.Inst.hotMomentDic)
    Extensions.RemoveAllChildren(this.statusListTable.transform)

    this.NowHotChoosePageNum = page
    CPersonalSpaceMgr.Inst:GetHotMoments(this.NowHotChoosePageNum, DelegateFactory.Action_CPersonalSpace_GetMoments_Ret(function (data)
        if not this or this == nil then
            return
        end
        do
            local i = 0
            while i < data.data.list.Length do
                data.data.list[i].type = CPersonalSpace_CircleGenerateType.HotMoments
                i = i + 1
            end
        end
        this.statusListScrollViewIndicator.enableShowNode = true
        this:UpdateHotMomentsScrollView(data, CPersonalSpace_CircleGenerateType.HotMoments, false)
    end), nil)
end
CPersonalSpaceHotMomentsView.m_UpdateAllHotList_CS2LuaHook = function (this, page)
    if page > 0 then
        page = page - 1
    end

    CommonDefs.ListClear(CPersonalSpaceMgr.Inst.allhotMomentList)
    CommonDefs.DictClear(CPersonalSpaceMgr.Inst.allhotMomentDic)
    Extensions.RemoveAllChildren(this.statusListTable.transform)
    this.NowAllHotChoosePageNum = page
    CPersonalSpaceMgr.Inst:GetAllHotMoments(this.NowAllHotChoosePageNum, DelegateFactory.Action_CPersonalSpace_GetMoments_Ret(function (data)
        if not this or this == nil then
            return
        end
        do
            local i = 0
            while i < data.data.list.Length do
                data.data.list[i].type = CPersonalSpace_CircleGenerateType.AllHotMoments
                i = i + 1
            end
        end
        this.statusListScrollViewIndicator.enableShowNode = true
        this:UpdateHotMomentsScrollView(data, CPersonalSpace_CircleGenerateType.AllHotMoments, false)
    end), nil)
end
CPersonalSpaceHotMomentsView.m_InitHotMoments_CS2LuaHook = function (this)
    this.sendStatusPanel_emptyNode:SetActive(true)
    CommonDefs.GetComponent_Component_Type(this.sendStatusPanel_emptyNode.transform:Find("Label"), typeof(UILabel)).text = CPersonalSpaceHotMomentsView.HotEmptyString
    CPersonalSpaceMgr.Inst.NowScrollType = CPersonalSpace_CircleGenerateType.HotMoments

    this:UpdateHotList(0)
    this.qaButton.onIncAndDecButtonClicked = MakeDelegateFromCSFunction(this.UpdateHotList, MakeGenericClass(Action1, UInt32), this)
    this.qaButton.onKeyBoardClosed = MakeDelegateFromCSFunction(this.UpdateHotList, MakeGenericClass(Action1, UInt32), this)
    this.qaButton:SetMinMax(1, 20, 1)
    this.qaButton:SetValue(1, true)
end
CPersonalSpaceHotMomentsView.m_InitHotMomentsAll_CS2LuaHook = function (this)
    this.sendStatusPanel_emptyNode:SetActive(true)
    CommonDefs.GetComponent_Component_Type(this.sendStatusPanel_emptyNode.transform:Find("Label"), typeof(UILabel)).text = CPersonalSpaceHotMomentsView.HotEmptyString
    CPersonalSpaceMgr.Inst.NowScrollType = CPersonalSpace_CircleGenerateType.AllHotMoments

    this:UpdateAllHotList(0)
    this.qaButton.onIncAndDecButtonClicked = MakeDelegateFromCSFunction(this.UpdateAllHotList, MakeGenericClass(Action1, UInt32), this)
    this.qaButton.onKeyBoardClosed = MakeDelegateFromCSFunction(this.UpdateAllHotList, MakeGenericClass(Action1, UInt32), this)
    this.qaButton:SetMinMax(1, 20, 1)
    this.qaButton:SetValue(1, true)
    --GameObject showMoreBtn = statusListScrollViewIndicator.transform.FindChild("ShowNode").gameObject;
    --SetShowMoreBtn(showMoreBtn, CPersonalSpace_CircleGenerateType.AllHotMoments, 0);

    local hotTalkBtn = this.transform:Find("OpArea/hotTalk").gameObject
    if LuaPersonalSpaceMgrReal.EnableHotTalk then
      hotTalkBtn:SetActive(true)
      UIEventListener.Get(hotTalkBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.PersonalSpaceHotTalkWnd)
      end)
    else
      hotTalkBtn:SetActive(false)
    end
end
CPersonalSpaceHotMomentsView.m_HongRenCallBack_CS2LuaHook = function (this, code, dataString)
    local showInfoArray = Table2ArrayWithCount({this.hongRenPanel.transform:Find("Info/InfoItem1").gameObject, this.hongRenPanel.transform:Find("Info/InfoItem2").gameObject, this.hongRenPanel.transform:Find("Info/InfoItem3").gameObject, this.hongRenPanel.transform:Find("Info/InfoItem4").gameObject}, 4, MakeArrayClass(GameObject))
    if code then
        local retList = TypeAs(L10.Game.Utils.Json.Deserialize(dataString), typeof(MakeGenericClass(List, Object)))
        do
            local i = 0
            while i < retList.Count do
                local dic = TypeAs(retList[i], typeof(MakeGenericClass(Dictionary, String, Object)))
                if i < showInfoArray.Length then
                    local node = showInfoArray[i]
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("read/num"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(dic, typeof(String), "views"))
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("favor/num"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(dic, typeof(String), "zan"))
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = ToStringWrap(CommonDefs.DictGetValue(dic, typeof(String), "name"))

                    local utex = CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(UITexture))
                    if utex == nil then
                        return
                    end

                    local photoUrl = System.String.Format("{0}?imageView&thumbnail={1}x{2}", ToStringWrap(CommonDefs.DictGetValue(dic, typeof(String), "pic")), utex.width, utex.height)
                    --string photoUrl = dic["pic"].ToString();
                    CPersonalSpaceMgr.DownLoadPic(photoUrl, utex, utex.height, utex.width, nil, true, false)
                    local clickUrl = ToStringWrap(CommonDefs.DictGetValue(dic, typeof(String), "url"))
                    local bgBtn = node.transform:Find("bg").gameObject
                    UIEventListener.Get(bgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                        CWebBrowserMgr.Inst:OpenUrl(clickUrl)
                    end)
                end
                i = i + 1
            end
        end
    else
    end
end
CPersonalSpaceHotMomentsView.m_OnTabChange_CS2LuaHook = function (this, go, index)
    if index == 0 then
        this.hotPanel:SetActive(true)
        this.hongRenPanel:SetActive(false)
        this:InitHotMomentsAll()
    elseif index == 1 then
        this.hotPanel:SetActive(true)
        this.hongRenPanel:SetActive(false)
        this:InitHotMoments()
    elseif index == 2 then
        this.hotPanel:SetActive(false)
        this.hongRenPanel:SetActive(true)
        this:InitHongRenInfo()
    end
end
