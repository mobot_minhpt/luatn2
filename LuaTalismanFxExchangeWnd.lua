local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DelegateFactory = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CUITexture = import "L10.UI.CUITexture"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CButton = import "L10.UI.CButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CChatLinkMgr = import "CChatLinkMgr"
local UILabel = import "UILabel"
local UIVerticalLabel = import "UIVerticalLabel"
local CUICommonDef = import "L10.UI.CUICommonDef"
local NGUIMath = import "NGUIMath"
local UInt32 = import "System.UInt32"
local Item_Item = import "L10.Game.Item_Item"
local CGuideBubbleDisplay = import "L10.UI.CGuideBubbleDisplay"
local CUIFx = import "L10.UI.CUIFx"

CLuaTalismanFxExchangeWnd = class()

CLuaTalismanFxExchangeWnd.s_TalismanFxId = 0

RegistChildComponent(CLuaTalismanFxExchangeWnd, "TableView", QnTableView)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ModelPreviewer", QnModelPreviewer)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ItemName", UILabel)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ExchangeButton", CButton)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ExchangeLabel", UILabel)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "Cost", Transform)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ItemCost", Transform)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ContributionCost", CCurentMoneyCtrl)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "HouseResCost", UILabel)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ItemDesc", UIVerticalLabel)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "talismanTab", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "YueLiCost", CCurentMoneyCtrl)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "chargeNode", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "returnNode", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "notOpenNode", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "YueliCost1", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "YueliCost2", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "guideNode", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ItemLabelDesc", GameObject)
RegistChildComponent(CLuaTalismanFxExchangeWnd, "ExtraItemCost", Transform)

RegistClassMember(CLuaTalismanFxExchangeWnd, "m_ItemTable")
RegistClassMember(CLuaTalismanFxExchangeWnd, "m_SelectIndex")
RegistClassMember(CLuaTalismanFxExchangeWnd, "m_SelectFxId")
RegistClassMember(CLuaTalismanFxExchangeWnd, "m_ItemEnough")

function CLuaTalismanFxExchangeWnd:Awake()
	--self.m_SelectFxId = CLuaTalismanFxExchangeWnd.s_TalismanFxId

	self.m_RealSelectFxId = CLuaTalismanFxExchangeWnd.s_TalismanFxId
	local data = ItemGet_TalismanFX.GetData(self.m_RealSelectFxId)
	if data then
		local dType = data.TalismanID % 100

		ItemGet_TalismanFX.Foreach(function (key, data)
			if data.TalismanID == dType  then
				self.m_SelectFxId = key
			end
		end)
	end

	self.talismanTab:SetActive(false)
	self.ItemLabelDesc:SetActive(false)
end


function CLuaTalismanFxExchangeWnd:Init()
	self.m_ItemTable = {}

	local itemTbl = {}
	local gotItemTbl = {}
	ItemGet_TalismanFX.Foreach(function(k, v)
		if v.TalismanID < 100 then
			local got = LuaTalismanMgr:CheckContain(v.TalismanID)
			local fxOrderDesign = Talisman_FXOrder.GetData(k)
			local fxOrder = fxOrderDesign and fxOrderDesign.Order or 0
			if got then
				table.insert(gotItemTbl, {id = k, got = true, order = fxOrder})
			else
				table.insert(itemTbl, {id = k, got = false, order = fxOrder})
			end
		end
	end)

	table.sort(itemTbl, function(a, b) return a.order < b.order end)
	table.sort(gotItemTbl, function(a, b) return a.order < b.order end)
	for _, item in ipairs(itemTbl) do
		table.insert(self.m_ItemTable, item)
	end
	for _, item in ipairs(gotItemTbl) do
		table.insert(self.m_ItemTable, item)
	end

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
		function() return #self.m_ItemTable end,
		function(item, row) self:InitItem(item, row) end
	)
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self.m_SelectIndex = row + 1
		self.m_SelectFxId = self.m_ItemTable[row + 1].id
		self:InitDetails()
	end)

	if self.m_SelectFxId and self.m_SelectFxId > 0 then
		for i = 1, #self.m_ItemTable do
			if self.m_ItemTable[i].id == self.m_SelectFxId then
				self.m_SelectIndex = i
				self.TableView:SetSelectRow(i - 1, true)
				break
			end
		end
	else
		self.m_SelectIndex = 1
		self.TableView:SetSelectRow(0, true)
	end
	self.TableView:ReloadData(false, false)

	self:CheckGuide()
end

function CLuaTalismanFxExchangeWnd:InitItem(item, row)
	local tf = item.transform

	local id = self.m_ItemTable[row+1].id
	local got = self.m_ItemTable[row+1].got
	local data = ItemGet_TalismanFX.GetData(id)

	-- icon
	local iconTex = tf:Find("Icon"):GetComponent(typeof(CUITexture))
	iconTex:LoadMaterial(data.Icon)

	-- lock
	local lockGo = tf:Find("Icon/LockMask").gameObject
	lockGo:SetActive(not got)

	-- name
	local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
	nameLabel.text = CChatLinkMgr.TranslateToNGUIText(data.Name, false)

	-- stage
	local stageLabel = tf:Find("StageLabel"):GetComponent(typeof(UILabel))
	stageLabel.text = SafeStringFormat3(LocalString.GetString("%s阶仙家"), data.Stage)
end

function CLuaTalismanFxExchangeWnd:CheckNotOpen(data)
	local lockData = Talisman_XianJiaAppearanceLevel.GetData(data.TalismanID)
	if lockData then
		local lockGroup = lockData.LockGroup
		local lockNum = 0
    if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eUnLockSpecialFaBaoFxLevelUp) then
			lockNum = tonumber(CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockSpecialFaBaoFxLevelUp].StringData)
		end
		local lockTable = {}
		if lockNum then
			while lockNum > 0 do
				local r = lockNum % 2
				lockNum = math.floor(lockNum/2)
				table.insert(lockTable,r)
			end
		end
		if lockTable[lockGroup] and lockTable[lockGroup] == 1 then
			return false
		end
	end
	return true
end

function CLuaTalismanFxExchangeWnd:CheckItemState(data)
	local level = math.floor(data.TalismanID / 100)
	local tType = data.TalismanID % 100
	if level <= 0 then
		return 3
	else
		if self:CheckNotOpen(data) then
			return 1
		else
			local formerId = (level-1) * 100 + tType
			local got = LuaTalismanMgr:CheckContain(formerId)
			if got then
				return 3
			else
				return 2,level
			end
		end
	end
end

function CLuaTalismanFxExchangeWnd:OnDescLabelClick(label)
	local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end

function CLuaTalismanFxExchangeWnd:InitExtraBar(selectItem)
	local tData = Talisman_XianJiaAppearanceLevel.GetData(selectItem.TalismanID)
	if tData then
		if tData.FixWordCount > 0 then
			self.ItemLabelDesc:SetActive(true)
			self.ItemLabelDesc.transform:Find('text'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('乾坤位法宝基础属性词条数+%s'),tData.FixWordCount)
		else
			self.ItemLabelDesc:SetActive(false)
		end
	end
end

function CLuaTalismanFxExchangeWnd:InitItemNode(node,costItemId,costItemCount,itemTemplate)
	local iconTex = node:Find("Icon"):GetComponent(typeof(CUITexture))
	iconTex:LoadMaterial(itemTemplate.Icon)

	UIEventListener.Get(node.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		local actionSource = nil
		if not self.m_ItemEnough then
			actionSource = DefaultItemActionDataSource.Create(1, {function(...)
				CItemAccessListMgr.Inst:ShowItemAccessInfo(costItemId, false, self.transform, AlignType1.Right)
			end}, {LocalString.GetString("获取途径")})
		end
		CItemInfoMgr.ShowLinkItemTemplateInfo(costItemId, false, actionSource, AlignType.Default, 0, 0, 0, 0)
	end)

	local countUpdate = node:GetComponent(typeof(CItemCountUpdate))
	local getNode = node:Find("GetNode").gameObject
	countUpdate.templateId = costItemId
	countUpdate.format = SafeStringFormat3("{0}/%d", costItemCount)
	countUpdate.onChange = DelegateFactory.Action_int(function(val)
		if val < costItemCount then
			self.m_ItemEnough = false
			countUpdate.format = SafeStringFormat3("[ff0000]{0}[-]/%d", costItemCount)
			getNode:SetActive(true)
		else
			self.m_ItemEnough = true
			countUpdate.format = SafeStringFormat3("{0}/%d", costItemCount)
			getNode:SetActive(false)
		end
	end)
	countUpdate:UpdateCount()
end

function CLuaTalismanFxExchangeWnd:InitSingleDetails(selectItem)
	self.Cost.gameObject:SetActive(false)

	--local selectItem = self.m_ItemTable[self.m_SelectIndex]
	local data = ItemGet_TalismanFX.GetData(selectItem.id)
	self.ModelPreviewer:PreviewMainPlayer(
	CClientMainPlayer.Inst.AppearanceProp.HideGemFxToOtherPlayer,
	CClientMainPlayer.Inst.AppearanceProp.IntesifySuitId,
	CClientMainPlayer.Inst.AppearanceProp.Wing,
	CClientMainPlayer.Inst.AppearanceProp.HideWing,
	data.TalismanID,
	0, 180, nil)

	local level = math.ceil(data.TalismanID/100)
	local name = SafeStringFormat3(LocalString.GetString('%s·%s级'),data.Name,level)
	local nameLength = string.len(name)
	local height = 12 * nameLength + 126
	self.ItemName.transform:Find('BackSprite'):GetComponent(typeof(UISprite)).width = height

	self.ItemName.text = CUICommonDef.GetVerticalLayoutText(name, false)
	self.ItemDesc.fontColor = NGUIMath.HexToColor(0x6b8eb3ff)
	self.ItemDesc.text = data.Desc

	self:InitExtraBar(data)

	if selectItem.got then
		self.chargeNode:SetActive(true)
		self.returnNode:SetActive(false)
		self.notOpenNode:SetActive(false)
		self.ExchangeButton.Enabled = false
		self.ExchangeLabel.text = LocalString.GetString("已拥有")
	else
		local checkState,level = self:CheckItemState(data)
		if checkState == 1 then
			self.chargeNode:SetActive(false)
			self.returnNode:SetActive(false)
			self.notOpenNode:SetActive(true)
			local showString = g_MessageMgr:FormatMessage('TalismanUnlockTip')
			local descLabel = self.notOpenNode.transform:Find('Cost/Label'):GetComponent(typeof(UILabel))
			descLabel.text = showString
			CommonDefs.AddOnClickListener(descLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDescLabelClick(descLabel) end), false)
		elseif checkState == 2 and level then
			self.chargeNode:SetActive(false)
			self.returnNode:SetActive(false)
			self.notOpenNode:SetActive(true)
			local showString = SafeStringFormat3(LocalString.GetString('需逐级解锁，请先解锁[c][FFE345]%s·%s级[-][/c]外观'),data.Name,level)
			self.notOpenNode.transform:Find('Cost/Label'):GetComponent(typeof(UILabel)).text = showString
		elseif checkState == 3 then -- if can charge
			self.chargeNode:SetActive(true)
			self.returnNode:SetActive(false)
			self.notOpenNode:SetActive(false)

			self.ExchangeButton.Enabled = true
			self.ExchangeLabel.text = LocalString.GetString("兑换外观")

			local level = math.floor(data.TalismanID / 100)
			local showString = SafeStringFormat3(LocalString.GetString('是否兑换[c][FFE345]%s·%s级[-][/c]外观？'),data.Name,level+1)
			if level > 0 then
				UIEventListener.Get(self.ExchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
					MessageWndManager.ShowOKCancelMessage(showString, DelegateFactory.Action(function ()
						Gac2Gas.RequestLevelUpFaBaoSpecialFx(selectItem.id)
					end), nil, nil, nil, false)
				end)
			else
				UIEventListener.Get(self.ExchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
					MessageWndManager.ShowOKCancelMessage(showString, DelegateFactory.Action(function ()
						Gac2Gas.RequestExchangeFaBaoSpecialFx(selectItem.id)
					end), nil, nil, nil, false)
				end)
			end

			self.Cost.gameObject:SetActive(true)
			self.ItemCost.gameObject:SetActive(false)
			self.ContributionCost.gameObject:SetActive(false)
			self.HouseResCost.gameObject:SetActive(false)
			self.YueliCost1:SetActive(false)
			self.YueliCost2:SetActive(false)
			self.ExtraItemCost.gameObject:SetActive(false)
			if data.Item then
				self.ItemCost.gameObject:SetActive(true)
				local costItemId = data.Item[0]
				local costItemCount = data.Item[1]
				local itemTemplate = costItemId and Item_Item.GetData(costItemId)
				if costItemId and costItemCount and itemTemplate then
					self:InitItemNode(self.ItemCost,costItemId,costItemCount,itemTemplate)

					if data.Contribution > 0 then
						self.ContributionCost.gameObject:SetActive(true)
						self.ContributionCost:SetCost(data.Contribution)
					elseif data.HouseRes > 0 then
						self.HouseResCost.gameObject:SetActive(true)
						self.HouseResCost.text = SafeStringFormat3(LocalString.GetString("及[c][FFE345]%s万[-][/c]家园资材"), math.floor(data.HouseRes / 10000))
					elseif data.YueLi > 0 then
						self.YueliCost1:SetActive(true)
						self.YueliCost1.transform:Find('Cost'):GetComponent(typeof(UILabel)).text = data.YueLi
						local val = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.YueLi]
						self.YueliCost1.transform:Find('Own'):GetComponent(typeof(UILabel)).text = val
					elseif data.Item.Length >= 4 then
						local extraCostItemId = data.Item[2]
						local extraCostItemCount = data.Item[3]
						local extraItemTemplate = extraCostItemId and Item_Item.GetData(extraCostItemId)
						self.ExtraItemCost.gameObject:SetActive(true)
						self:InitItemNode(self.ExtraItemCost,extraCostItemId,extraCostItemCount,extraItemTemplate)
					end
				end
			elseif data.YueLi > 0 then
				self.YueliCost2:SetActive(true)
				self.YueliCost2.transform:Find('Cost'):GetComponent(typeof(UILabel)).text = data.YueLi
				local val = CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.YueLi]
				self.YueliCost2.transform:Find('Own'):GetComponent(typeof(UILabel)).text = val
			end
		end
	end
end

function CLuaTalismanFxExchangeWnd:InitTalismanSinTab(node,tFxId,tId,last,defaultClick)
  local lightNode = node:Find('light').gameObject
  local clickNode = node:Find('bg').gameObject
	local labelNode = node:Find('Label').gameObject
	local bar = node:Find('slider')
	local barBg = node:Find('sliderBg')
	if barBg then
		barBg.gameObject:SetActive(true)
	end
	lightNode:SetActive(false)

	local got = LuaTalismanMgr:CheckContain(tId)
	if got then
		clickNode.transform.localPosition = Vector3(clickNode.transform.localPosition.x,clickNode.transform.localPosition.y,0)
		lightNode.transform.localPosition = Vector3(lightNode.transform.localPosition.x,lightNode.transform.localPosition.y,0)
		labelNode.transform.localPosition = Vector3(labelNode.transform.localPosition.x,labelNode.transform.localPosition.y,0)
		if bar then
			bar.gameObject:SetActive(true)
		end
	else
		clickNode.transform.localPosition = Vector3(clickNode.transform.localPosition.x,clickNode.transform.localPosition.y,-1)
		lightNode.transform.localPosition = Vector3(lightNode.transform.localPosition.x,lightNode.transform.localPosition.y,-1)
		labelNode.transform.localPosition = Vector3(labelNode.transform.localPosition.x,labelNode.transform.localPosition.y,-1)
		if bar then
			bar.gameObject:SetActive(false)
		end
	end

	local click = function(go)
		if self.lightNode then
			self.lightNode:SetActive(false)
		end
		self.lightNode = lightNode
		self.lightNode:SetActive(true)
		local selectData = {}
		selectData.id = tFxId
		selectData.got = got
		self:InitSingleDetails(selectData)
	end

	if defaultClick then
		if self.m_RealSelectFxId == tFxId then
			click()
			self.m_RealSelectFxId = nil
		end
	elseif not self.clickOnce then
		if not got then
			click()
			self.clickOnce = true
		elseif last == 0 then
			click()
		end
	end

	UIEventListener.Get(clickNode).onClick = DelegateFactory.VoidDelegate(click)

	if last == 0 then
		if bar then
			bar.gameObject:SetActive(false)
		end
		if barBg then
			barBg.gameObject:SetActive(false)
		end
	end
end

function CLuaTalismanFxExchangeWnd:InitDetails()
	local selectItem = self.m_ItemTable[self.m_SelectIndex]
	local tData = ItemGet_TalismanFX.GetData(selectItem.id)

  local talismanType = tData.TalismanID % 100
  local talismanTable = {}
  ItemGet_TalismanFX.Foreach(function (key, data)
    local tType = data.TalismanID % 100
    if tType == talismanType then
      table.insert(talismanTable,{key,data.TalismanID})
    end
  end)

	local tCount = #talismanTable
  if tCount <= 1 then
    self.talismanTab:SetActive(false)
		self:InitSingleDetails()
    return
  end

	self.talismanTab:SetActive(true)
  table.sort(talismanTable,function(a,b)
    return a[2] < b[2]
  end)

	local defaultClick = nil
	if self.m_RealSelectFxId and self.m_RealSelectFxId > 0 then
		defaultClick = true
	end
	self.clickOnce = nil

	self.tabNodeTable = {}
  local talismanNodeTable = {self.talismanTab.transform:Find('node1'),self.talismanTab.transform:Find('node2'),self.talismanTab.transform:Find('node3'),self.talismanTab.transform:Find('node4'),self.talismanTab.transform:Find('node5')}
  for i,v in pairs(talismanNodeTable) do
    if talismanTable[i] then
      v.gameObject:SetActive(true)
      self:InitTalismanSinTab(v,talismanTable[i][1],talismanTable[i][2],tCount-i,defaultClick)
			table.insert(self.tabNodeTable,{talismanTable[i][2],v})
    else
      v.gameObject:SetActive(false)
    end
  end
end

function CLuaTalismanFxExchangeWnd:OnEnable()
	g_ScriptEvent:AddListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
	g_ScriptEvent:AddListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
	g_ScriptEvent:AddListener("TalismanFxUnlock", self, "OnTalismanFxUnlock")
end

function CLuaTalismanFxExchangeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
	g_ScriptEvent:RemoveListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
	g_ScriptEvent:RemoveListener("TalismanFxUnlock", self, "OnTalismanFxUnlock")
end

function CLuaTalismanFxExchangeWnd:OnTalismanFxUnlock(tId)
	if self.tabNodeTable then
		for i,v in pairs(self.tabNodeTable) do
			if v[1] == tId then
				v[2].transform:Find('fxNode'):GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_fabaojiesuo.prefab')
			end
		end
	end
end

function CLuaTalismanFxExchangeWnd:OnTalismanFxUpdate(args)
	local obj = args[0]
	if TypeIs(obj, typeof(CClientMainPlayer)) then
		self:Init()
	end
end

function CLuaTalismanFxExchangeWnd:OnTalismanFxListUpdate()
	self:Init()
end

function CLuaTalismanFxExchangeWnd:CheckGuide()
	if CLuaGuideMgr.TryTriggerTalismanExchageGuide() then
		self:InitGuide()
	end
end

function CLuaTalismanFxExchangeWnd:InitGuide()
	self.guideNode:SetActive(true)

	local bubble = self.guideNode.transform:Find('bubble'):GetComponent(typeof(CGuideBubbleDisplay))
  bubble.gameObject:SetActive(true)
  bubble:Init(LocalString.GetString('点击查看不同等级对应的法宝外观效果'))

	local btn = self.guideNode.transform:Find('btn').gameObject
	local onClick = function(go)
		self.guideNode:SetActive(false)
	end
	CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(onClick),false)
end
