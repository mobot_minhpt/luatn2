-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local UICamera = import "UICamera"
CTipParagraphItem.m_OnLabelClick_CS2LuaHook = function (this, go) 
    local url = this.textLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end
