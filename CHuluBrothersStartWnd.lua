-- Auto Generated!!
local CHuluBrothersStartWnd = import "L10.UI.CHuluBrothersStartWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
--local HuluBrothers_Reward = import "L10.Game.HuluBrothers_Reward"
local Int32 = import "System.Int32"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CHuluBrothersStartWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseObj).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)
    UIEventListener.Get(this.m_TipObj).onClick = MakeDelegateFromCSFunction(this.ShowTip, VoidDelegate, this)
    UIEventListener.Get(this.m_EasyBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.GoEasyLevel, VoidDelegate, this)
    UIEventListener.Get(this.m_HardBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.GoHardLevel, VoidDelegate, this)
    UIEventListener.Get(this.m_ScoreRankObj).onClick = MakeDelegateFromCSFunction(this.ShowRankWnd, VoidDelegate, this)
    EventManager.AddListenerInternal(EnumEventType.HuluBrothersOverview, MakeDelegateFromCSFunction(this.ShowBossIcon, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))
    do
        local i = 0 local cnt = this.m_BossObj.Length
        while i < cnt do
            this.m_BossObj[i]:SetActive(false)
            i = i + 1
        end
    end
end
CHuluBrothersStartWnd.m_ShowBossIcon_CS2LuaHook = function (this, bossIds) 
    do
        local i = 0 local cnt = bossIds.Count
        while i < cnt do
            local id = math.floor(tonumber(bossIds[i] or 0))
            if this.m_BossId2Index ~= nil and CommonDefs.DictContains(this.m_BossId2Index, typeof(UInt32), id) then
                this.m_BossObj[CommonDefs.DictGetValue(this.m_BossId2Index, typeof(UInt32), id)]:SetActive(true)
            end
            i = i + 1
        end
    end
end
CHuluBrothersStartWnd.m_Init_CS2LuaHook = function (this) 
    this.m_TimeLabel.text = g_MessageMgr:FormatMessage("Huluwa_OpenTime")
    Gac2Gas.RequestGetHuLuSceneOverView()
    if nil == this.m_BossId2Index then
        this.m_BossId2Index = CreateFromClass(MakeGenericClass(Dictionary, UInt32, Int32))
        HuluBrothers_Reward.Foreach(function (key, data) 
            if not CommonDefs.DictContains(this.m_BossId2Index, typeof(UInt32), data.NormalMonsterId) then
                CommonDefs.DictAdd(this.m_BossId2Index, typeof(UInt32), data.NormalMonsterId, typeof(Int32), key - 1)
            end
            if not CommonDefs.DictContains(this.m_BossId2Index, typeof(UInt32), data.HardMonsterId) then
                CommonDefs.DictAdd(this.m_BossId2Index, typeof(UInt32), data.HardMonsterId, typeof(Int32), key + 3)
            end
        end)
    end
end
