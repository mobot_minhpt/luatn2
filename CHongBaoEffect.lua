-- Auto Generated!!
local CHongBaoEffect = import "L10.UI.CHongBaoEffect"
local Object = import "UnityEngine.Object"
CHongBaoEffect.m_Start_CS2LuaHook = function (this) 
    this.m_Trans = this.transform
    this.m_DropSpeed = UnityEngine_Random(this.m_DropSpeed - 100, this.m_DropSpeed + 100)
    this.m_RotateSpeed = UnityEngine_Random(this.m_RotateSpeed - 100, this.m_RotateSpeed + 100)
    Object.Destroy(this.gameObject, this.m_LifeTime)
end
