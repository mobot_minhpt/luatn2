local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local CGuildMgr=import "L10.Game.CGuildMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CIndirectUIResources=import "L10.UI.CIndirectUIResources"
local Vector3 = import "UnityEngine.Vector3"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local NGUITools = import "NGUITools"
local NGUIText = import "NGUIText"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local Constants = import "L10.Game.Constants"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CUITexture=import "L10.UI.CUITexture"
local CButton=import "L10.UI.CButton"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

CLuaFriendPopupMenu = class()
RegistClassMember(CLuaFriendPopupMenu,"basicInfoRoot")
RegistClassMember(CLuaFriendPopupMenu,"background")
RegistClassMember(CLuaFriendPopupMenu,"texture")
RegistClassMember(CLuaFriendPopupMenu,"profileFrame")
RegistClassMember(CLuaFriendPopupMenu,"expressionTxt")
RegistClassMember(CLuaFriendPopupMenu,"nameLabel")
RegistClassMember(CLuaFriendPopupMenu,"levelLabel")
RegistClassMember(CLuaFriendPopupMenu,"teamInfoLabel")
RegistClassMember(CLuaFriendPopupMenu,"xiuweiLabel")
RegistClassMember(CLuaFriendPopupMenu,"buttonTemplate")
RegistClassMember(CLuaFriendPopupMenu,"imageButtonTemplate")
RegistClassMember(CLuaFriendPopupMenu,"grid")
RegistClassMember(CLuaFriendPopupMenu,"modifyNickNameBtn")
RegistClassMember(CLuaFriendPopupMenu,"buttons")
RegistClassMember(CLuaFriendPopupMenu,"DefaultButtonBgSpriteName")
RegistClassMember(CLuaFriendPopupMenu,"OrangeButtonBgSpriteName")

RegistClassMember(CLuaFriendPopupMenu,"m_Wnd")


function CLuaFriendPopupMenu:Awake()
    self.basicInfoRoot = self.transform:Find("Anchor/Background/BasicInfo").gameObject
    self.background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))
    self.texture = self.transform:Find("Anchor/Background/BasicInfo/Border/Portrait"):GetComponent(typeof(CUITexture))
    self.profileFrame = self.transform:Find("Anchor/Background/BasicInfo/Border/ProfileFrame"):GetComponent(typeof(CUITexture))
    self.expressionTxt = self.transform:Find("Anchor/Background/BasicInfo/Border/ExpressionTxt"):GetComponent(typeof(CUITexture))
    self.nameLabel = self.transform:Find("Anchor/Background/BasicInfo/NameLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("Anchor/Background/BasicInfo/LevelLabel"):GetComponent(typeof(UILabel))
    self.teamInfoLabel = self.transform:Find("Anchor/Background/BasicInfo/TeamInfoLabel"):GetComponent(typeof(UILabel))
    self.xiuweiLabel = self.transform:Find("Anchor/Background/BasicInfo/XiuweiLabel"):GetComponent(typeof(UILabel))
    self.buttonTemplate = self.transform:Find("Anchor/TemplateButton1").gameObject
    self.imageButtonTemplate = self.transform:Find("Anchor/TemplateButton2").gameObject
    self.grid = self.transform:Find("Anchor/Background/ButtonsGrid"):GetComponent(typeof(UIGrid))
    self.modifyNickNameBtn = self.transform:Find("Anchor/Background/BasicInfo/ModifyNicknameBtn").gameObject
    self.playerFlagSprite = self.transform:Find("Anchor/Background/BasicInfo/CountryFlag"):GetComponent(typeof(UISprite))
    
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.buttons = {}
    self.DefaultButtonBgSpriteName = "common_btn_01_blue"
    self.OrangeButtonBgSpriteName = "common_btn_01_yellow"

end

function CLuaFriendPopupMenu:Init( )
    local playerId = CPlayerInfoMgr.PopupMenuInfo.playerId
    local showModifyBtn = (CPlayerInfoMgr.PopupMenuInfo.context == EnumPlayerInfoContext.FriendInFriendWnd) and CIMMgr.Inst:IsMyFriend(playerId)

    if CPlayerInfoMgr.PopupMenuInfo.context == EnumPlayerInfoContext.GuildMember and CGuildMgr.Inst.m_GuildMemberInfo then
        --权限 帮主：全部显示 副帮主：帮主不能显示
        local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0

        local info = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), myId)
        local info2 = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.Infos, typeof(UInt64), playerId)
        if info and info2 then


            if info.Title==1 then
                showModifyBtn=true
            elseif info.Title==2 and info2.Title~=1 then
                showModifyBtn=true
            else
                showModifyBtn=false
            end
        end
    end

    self.modifyNickNameBtn:SetActive(showModifyBtn)
    self:LoadItems()
end
function CLuaFriendPopupMenu:Start( )

    self.buttonTemplate:SetActive(false)
    self.imageButtonTemplate:SetActive(false)

    UIEventListener.Get(self.modifyNickNameBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnModifyNickNameButtonClick(go) end)
end

function CLuaFriendPopupMenu:InitItem(transform,text,iconName)
    local label = FindChild(transform,"Label"):GetComponent(typeof(UILabel))

    local tf = FindChild(transform,"Sprite")
    local sprite  = tf and tf:GetComponent(typeof(UISprite)) or nil

    label.text=text


    if sprite then
        if iconName and iconName~="" then
            sprite.enabled=true
            sprite.spriteName = iconName
        else
            sprite.enabled=false
        end
    end

end
function CLuaFriendPopupMenu:LoadItems( )
    local popupMenuInfo = CPlayerInfoMgr.PopupMenuInfo
    if popupMenuInfo.itemActionPairs == nil or popupMenuInfo.itemActionPairs.Count == 0 then
        CUIManager.CloseUI(CIndirectUIResources.FriendPopupMenu)
        return
    end

    self.texture:LoadNPCPortrait(popupMenuInfo.portraitName, false)
    if self.profileFrame ~= nil and CExpressionMgr.EnableProfileFrame then
        self.profileFrame:LoadMaterial(CUICommonDef.GetProfileFramePath(popupMenuInfo.profileFrame))
    end
    self.expressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(popupMenuInfo.expressionTxt))

    local name = popupMenuInfo.displayName
    --local lianghaoIcon = CUICommonDef.GetLiangHaoIcon(popupMenuInfo.profileInfo, false, true)
    --if LuaLiangHaoMgr.LiangHaoEnabled() and lianghaoIcon~=nil then
    --   name = name..lianghaoIcon
    --end
    local flagSpriteName = LuaSEASdkMgr:GetFlagSpriteName(popupMenuInfo.profileInfo.CountryFlag)
    self.playerFlagSprite.spriteName = flagSpriteName
    
    self.nameLabel.text = name
    local default
    if popupMenuInfo.isInXianShenStatus then
        default = Constants.ColorOfFeiSheng
    else
        default = NGUIText.EncodeColor24(self.levelLabel.color)
    end

    if popupMenuInfo.context == EnumPlayerInfoContext.ChatRoom and not LuaClubHouseMgr:IsMyGame(CommonDefs.DictGetValue_LuaCall(popupMenuInfo.extraInfo, "AppName")) then
        local accid = tonumber(CommonDefs.DictGetValue_LuaCall(popupMenuInfo.extraInfo, "AccId"))
        local member = LuaClubHouseMgr:GetMember(accid)
        self.nameLabel.text = member.PlayerName
        self.levelLabel.text = ""
        self.teamInfoLabel.text = SafeStringFormat3(LocalString.GetString("来自: %s"), LuaClubHouseMgr:GetAppDisplayName(CommonDefs.DictGetValue_LuaCall(popupMenuInfo.extraInfo, "AppName")))
        self.xiuweiLabel.text = ""
    else
        self.levelLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", default, popupMenuInfo.level)
        self.teamInfoLabel.text = LocalString.GetString("队伍:-/-")
        Gac2Gas.QueryTeamSize(popupMenuInfo.playerId)
        self.xiuweiLabel.text = System.String.Format(LocalString.GetString("修为:{0:F1}"), NumberTruncate(popupMenuInfo.xiuweiGrade, 1))
    end

    Extensions.RemoveAllChildren(self.grid.transform)
    self.buttons={}
    local callback=DelegateFactory.VoidDelegate(function(go)
        self:OnMenuItemClick(go)
    end)
    do
        local i = 0
        while i < popupMenuInfo.itemActionPairs.Count do
            local pair = popupMenuInfo.itemActionPairs[i]
            local extern
            if not System.String.IsNullOrEmpty(pair.imageName) then
                extern = self.imageButtonTemplate
            else
                extern = self.buttonTemplate
            end
            local instance = NGUITools.AddChild(self.grid.gameObject, extern)
            instance:SetActive(true)
            local text = pair.text
            local bgSpriteName = ""
            repeat
                local ref = pair.style
                if ref == CPlayerInfoMgr.EnumMenuItemStyle.Orange then
                    bgSpriteName = self.OrangeButtonBgSpriteName
                    break
                else
                    bgSpriteName = self.DefaultButtonBgSpriteName
                    break
                end
            until 1
            local imageName = pair.imageName
            self:InitItem(instance.transform,text,  imageName)
            local cmp = instance:GetComponent(typeof(CButton))
            cmp:SetBackgroundSprite(bgSpriteName)
            UIEventListener.Get(instance).onClick=callback

            table.insert( self.buttons, instance )
            if pair.action == nil then
                cmp.Enabled=false
            end
            i = i + 1
        end
    end
    self.grid:Reposition()
    self.background.height = math.floor(NGUIMath.CalculateRelativeWidgetBounds(self.grid.transform).size.y) + math.floor(NGUIMath.CalculateRelativeWidgetBounds(self.basicInfoRoot.transform).size.y) + 50

    local localPos = self.background.transform.parent:InverseTransformPoint(popupMenuInfo.worldPos)

    repeat
        local out = popupMenuInfo.alignType
        if out == CPlayerInfoMgr.AlignType.Default then
            self.background.transform.localPosition = Vector3(localPos.x,localPos.y+self.background.height * 0.5,localPos.z)
            
            break
        elseif out == CPlayerInfoMgr.AlignType.Left then
            self.background.transform.localPosition = Vector3(localPos.x- self.background.width * 0.5,localPos.y+self.background.height * 0.5,localPos.z)
            
            break
        elseif out == CPlayerInfoMgr.AlignType.Right then
            self.background.transform.localPosition = Vector3(localPos.x+self.background.width * 0.5,localPos.y+ self.background.height * 0.5,localPos.z)
            
            break
        elseif out == CPlayerInfoMgr.AlignType.Bottom then
            self.background.transform.localPosition = localPos
            break
        elseif out == CPlayerInfoMgr.AlignType.Top then
            self.background.transform.localPosition = Vector3(localPos.x, localPos.y+self.background.height, localPos.z)
            break
        else
            self.background.transform.localPosition = Vector3(localPos.x, localPos.y+self.background.height*0.5, localPos.z)
            break
        end
    until 1

    local centerX = self.background.transform.localPosition.x
    local centerY = self.background.transform.localPosition.y - self.background.height * 0.5
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale
    local needAdjustLocation = false
    --左右不超出屏幕范围
    if centerX - self.background.width * 0.5 < - virtualScreenWidth * 0.5 then
        centerX = - virtualScreenWidth * 0.5 + self.background.width * 0.5
        needAdjustLocation = true
    end
    if centerX + self.background.width * 0.5 > (virtualScreenWidth * 0.5) then
        centerX = virtualScreenWidth * 0.5 - self.background.width * 0.5
        needAdjustLocation = true
    end
    --上下不超出屏幕范围
    if centerY + self.background.height * 0.5 > virtualScreenHeight * 0.5 then
        centerY = virtualScreenHeight * 0.5 - self.background.height * 0.5
        needAdjustLocation = true
    elseif centerY - self.background.height * 0.5 < - virtualScreenHeight * 0.5 then
        centerY = - virtualScreenHeight * 0.5 + self.background.height * 0.5
        needAdjustLocation = true
    end

    if needAdjustLocation then
        self.background.transform.localPosition = Vector3(centerX, centerY + self.background.height * 0.5, 0)
    end

    -- 茶室特殊设置
    if popupMenuInfo.context == EnumPlayerInfoContext.ChatRoom then
        self:ClubHouseSet()
    end
end
function CLuaFriendPopupMenu:OnModifyNickNameButtonClick( go) 
    local playerId = CPlayerInfoMgr.PopupMenuInfo.playerId
    if CPlayerInfoMgr.PopupMenuInfo.context == EnumPlayerInfoContext.GuildMember then
        local text = g_MessageMgr:FormatMessage("Friend_Modify_Nick_Name_Text", CPlayerInfoMgr.PopupMenuInfo.displayName, Constants.FriendNickNameLength * 2, Constants.FriendNickNameLength)
		if CommonDefs.IsSeaMultiLang() and (LocalString.language == "en" or LocalString.language == "ina") then
			text = g_MessageMgr:FormatMessage("SEA_Friend_Modify_Nick_Name_Text", CPlayerInfoMgr.PopupMenuInfo.displayName, Constants.FriendNickNameLength * 2)
		end
        local extraName = CLuaGuildMgr.m_ExtraNames[playerId] or nil
        CInputBoxMgr.ShowInputBox(text, DelegateFactory.Action_string(function (value) 
            if value == nil then
                value = ""
            end
            value = string.gsub(StringTrim(value), " ", "")
            if not CWordFilterMgr.Inst:CheckName(value) then
                g_MessageMgr:ShowMessage("Name_Violation")
                return
            end
            --是否是帮会内的备注名称修改
            Gac2Gas.RequestSetMemberExtraName(playerId,value)
        end), Constants.FriendNickNameLength * 2, true, extraName, extraName and extraName or LocalString.GetString("点此输入(当前无备注名称)"))
    else
        local data = CIMMgr.Inst:GetBasicInfo(playerId)
        local default
        if data ~= nil then
            default = data.NickName.StringData
        else
            default = ""
        end
        local nickName = default
        local text = g_MessageMgr:FormatMessage("Friend_Modify_Nick_Name_Text", CPlayerInfoMgr.PopupMenuInfo.displayName, Constants.FriendNickNameLength * 2, Constants.FriendNickNameLength)
		if CommonDefs.IsSeaMultiLang() and (LocalString.language == "en" or LocalString.language == "ina") then
			text = g_MessageMgr:FormatMessage("SEA_Friend_Modify_Nick_Name_Text", CPlayerInfoMgr.PopupMenuInfo.displayName, Constants.FriendNickNameLength * 2)
		end
        local extern
        if not nickName or nickName=="" then
            extern = nil
        else
            extern = nickName
        end
        CInputBoxMgr.ShowInputBox(text, DelegateFactory.Action_string(function (value) 
            if value == nil then
                value = ""
            end
            value = string.gsub(StringTrim(value), " ", "")
            if not CWordFilterMgr.Inst:CheckName(value) then
                g_MessageMgr:ShowMessage("Name_Violation")
                return
            end
            Gac2Gas.SetNickName(playerId, value)
        end), Constants.FriendNickNameLength * 2, true, extern, LocalString.GetString("点此输入(当前无备注名称)"))
    end
end

function CLuaFriendPopupMenu:OnMenuItemClick( go) 
    CUIManager.CloseUI(CIndirectUIResources.FriendPopupMenu)
    do
        local i = 0
        while i < #self.buttons do
            if self.buttons[i+1]:Equals(go) then
                local info = CPlayerInfoMgr.PopupMenuInfo
                -- GenericDelegateInvoke(info.itemActionPairs[i].action, Table2ArrayWithCount({info.playerId, info.displayName}, 2, MakeArrayClass(Object)))
                -- info.itemActionPairs[i].action(info.playerId, info.displayName)
                info.itemActionPairs[i]:CallAction(info.playerId, info.displayName)
                break
            end
            i = i + 1
        end
    end
end

function CLuaFriendPopupMenu:OnEnable()
	g_ScriptEvent:AddListener("QueryTeamSizeResult", self, "OnQueryTeamSizeResult")
end

function CLuaFriendPopupMenu:OnDisable()
	g_ScriptEvent:RemoveListener("QueryTeamSizeResult", self, "OnQueryTeamSizeResult")
end
function CLuaFriendPopupMenu:OnQueryTeamSizeResult(playerId, teamSize)
    if CPlayerInfoMgr.PopupMenuInfo.playerId ~= playerId then
        return
    end
    if teamSize > 0 then
        self.teamInfoLabel.text = SafeStringFormat3(LocalString.GetString("队伍:%d/5"), teamSize)
    else
        self.teamInfoLabel.text = LocalString.GetString("队伍:-/-")
    end
end
function CLuaFriendPopupMenu:Update()
    self.m_Wnd:ClickThroughToClose()
end

-- 茶室的一些特殊处理
function CLuaFriendPopupMenu:ClubHouseSet()
    local popupMenuInfo = CPlayerInfoMgr.PopupMenuInfo
    -- 茶室中查看使用梦岛头像
    CPersonalSpaceMgr.Inst:GetUserProfile(popupMenuInfo.playerId, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (data)
        if data.code == 0 then
            local _data = data.data
            if not System.String.IsNullOrEmpty(_data.photo) then
                self.texture:Clear()
                self.expressionTxt:Clear()
                CPersonalSpaceMgr.Inst:DownLoadPortraitPic(_data.photo, self.texture.texture, nil)
            end
        end
    end), nil)
end