require("common/common_include")
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local UIProgressBar = import "UIProgressBar"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local PlayerSettings = import "L10.Game.PlayerSettings"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local QnProgressBar = import "L10.UI.QnProgressBar"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CButton = import "L10.UI.CButton"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType3 = import "CPlayerInfoMgr+AlignType"

LuaBattleSituationWnd = class()

RegistClassMember(LuaBattleSituationWnd, "m_closeBtn")
RegistClassMember(LuaBattleSituationWnd, "m_broadCastBtn")
RegistClassMember(LuaBattleSituationWnd, "m_clearBtn")
RegistClassMember(LuaBattleSituationWnd, "m_titleLabel")
RegistClassMember(LuaBattleSituationWnd, "m_hideAllyCheckbox")

RegistClassMember(LuaBattleSituationWnd, "m_TabViewTable")
RegistClassMember(LuaBattleSituationWnd, "m_TabTemplate")

RegistClassMember(LuaBattleSituationWnd, "m_DetailSimpleTableView")
RegistClassMember(LuaBattleSituationWnd, "m_RankItemTemplate")
RegistClassMember(LuaBattleSituationWnd, "m_PlayerItemTemplate")
RegistClassMember(LuaBattleSituationWnd, "m_BangHuaHeroTemplate")

RegistClassMember(LuaBattleSituationWnd, "curTabStat")
RegistClassMember(LuaBattleSituationWnd, "TabList")
RegistClassMember(LuaBattleSituationWnd, "tableDataSource")
RegistClassMember(LuaBattleSituationWnd, "curSelectedTab")

function LuaBattleSituationWnd:Awake()
    self.m_closeBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    self.m_broadCastBtn = self.transform:Find("Anchor/BtnGrid/BroadcastBtn").gameObject
    self.m_clearBtn = self.transform:Find("Anchor/BtnGrid/ClearBtn").gameObject
    self.m_titleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_hideAllyCheckbox = self.transform:Find("Anchor/BtnGrid/Checkbox"):GetComponent(typeof(QnCheckBox))
    self.m_Checkbox2 = self.transform:Find("Anchor/BtnGrid/Checkbox (1)"):GetComponent(typeof(QnCheckBox))

    self.m_BtnGrid = self.transform:Find("Anchor/BtnGrid"):GetComponent(typeof(UIGrid))

    self.m_TabViewTable = self.transform:Find("Anchor/TabView/Scroll View/Table").gameObject
    self.m_TabTemplate = self.transform:Find("Anchor/TabView/Scroll View/Pool/ItemCell").gameObject

    self.m_DetailSimpleTableView = self.transform:Find("Anchor/DetailView"):GetComponent(typeof(UISimpleTableView))
    self.m_RankItemTemplate = self.transform:Find("Anchor/DetailView/Scroll View/Pool/RankItemCell").gameObject
    self.m_PlayerItemTemplate = self.transform:Find("Anchor/DetailView/Scroll View/Pool/PlayerItemCell").gameObject
    self.m_BangHuaHeroTemplate = self.transform:Find("Anchor/DetailView/Scroll View/Pool/BangHuaHeroCell").gameObject

    self.curTabStat = EnumSituationWndTab.Undefined
    self.TabList = LuaBattleSituationMgr.GetStatTabList()
    self.curSelectedTab = nil

    self.m_RankItemTemplate:SetActive(false)
    self.m_PlayerItemTemplate:SetActive(false)
    self.m_BangHuaHeroTemplate:SetActive(false)
end

function LuaBattleSituationWnd:Init()
    self:InitMainWnd()
    self:InitTabsView()
    self:InitDetailView()

    --打开第一个Tab
    self:OnTabSelected(self.curSelectedTab, self.TabList[1])

    self.m_BtnGrid:Reposition()
end

function LuaBattleSituationWnd:Start()
    CommonDefs.AddOnClickListener(
        self.m_closeBtn,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnCloseButtonClick(go)
            end
        ),
        false
    )

    CommonDefs.AddOnClickListener(
        self.m_broadCastBtn,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnBroadcastButtonClick(go)
            end
        ),
        false
    )

    CommonDefs.AddOnClickListener(
        self.m_clearBtn,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnClearButtonClick(go)
            end
        ),
        false
    )

    self.m_hideAllyCheckbox.OnValueChanged =
        DelegateFactory.Action_bool(
        function(val)
            self:OnHideAllyValueChanged(val)
        end
    )

    self.m_Checkbox2.OnValueChanged =
        DelegateFactory.Action_bool(
        function(val)
            self:OnCheckbox2Changed(val)
        end
    )
end

function LuaBattleSituationWnd:InitMainWnd()
    if LuaBattleSituationMgr.situationType == EnumSituationType.eBangHua then
        self.m_titleLabel.text = LocalString.GetString("帮花战况")
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao then
        self.m_titleLabel.text = LocalString.GetString("寇岛战况")
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.ePGQY then
        self.m_titleLabel.text = LocalString.GetString("盘古清阳战况")
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eTianMenShan then
        self.m_titleLabel.text = LocalString.GetString("天门山战况")
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eGuildTerritorialWarsChallenge then
        self.m_titleLabel.text = LocalString.GetString("棋局挑战战况")
    elseif LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        self.m_titleLabel.text = LocalString.GetString("战况")
    end

    self.m_clearBtn:SetActive(
        (LuaBattleSituationMgr.situationType ~= EnumSituationType.eKoudao and
            LuaBattleSituationMgr.situationType ~= EnumSituationType.eTianMenShan and
            LuaBattleSituationMgr.situationType ~= EnumSituationType.eHengYuDangKou and
            LuaBattleSituationMgr.situationType ~= EnumSituationType.eGuildTerritorialWarsChallenge) or
            LuaBattleSituationMgr.EnableClearButtonInKouDao
    )

    self.m_hideAllyCheckbox.gameObject:SetActive(LuaBattleSituationMgr.situationType == EnumSituationType.eKoudao or 
        LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou)
    if self.m_hideAllyCheckbox.gameObject.activeSelf then
        self.m_hideAllyCheckbox:SetSelected(PlayerSettings.HideNoneEnemyInKouDao, true)
    end

    self.m_Checkbox2.gameObject:SetActive(LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou)
    if self.m_Checkbox2.gameObject.activeSelf then
        if LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
            self.m_Checkbox2:SetSelected(LuaHengYuDangKouMgr.m_ShowCurBattle, true)
        end
    end
end

function LuaBattleSituationWnd:InitTabsView()
    self.m_TabTemplate:SetActive(false)

    Extensions.RemoveAllChildren(self.m_TabViewTable.transform)

    for i, v in ipairs(self.TabList) do
        local curTab = UICommonDef.AddChild(self.m_TabViewTable, self.m_TabTemplate)
        curTab:SetActive(true)
        curTab.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LuaBattleSituationMgr.GetNameByTabStat(v)
        CommonDefs.AddOnClickListener(
            curTab,
            DelegateFactory.Action_GameObject(
                function(go)
                    self:OnTabSelected(go, v)
                end
            ),
            false
        )
        self.curSelectedTab = self.curSelectedTab or curTab -- 初始状态下当前选中的是第一个tab
        curTab:GetComponent(typeof(CButton)).Selected = false
    end

    self.m_TabViewTable:GetComponent(typeof(UITable)):Reposition()
end

function LuaBattleSituationWnd:InitDetailView()
    if not self.tableDataSource then
        self.tableDataSource =
            DefaultUISimpleTableViewDataSource.Create(
            function()
                return self:NumberOfRows()
            end,
            function(index)
                return self:CellForRowAtIndex(index)
            end
        )
    end

    self.m_DetailSimpleTableView:Clear()
    self.m_DetailSimpleTableView.dataSource = self.tableDataSource
end

-- eDps, eHeal, eSuffer -> stats
-- eGuildHero
-- Undefined
function LuaBattleSituationWnd:NumberOfRows()
    if self.curTabStat == EnumSituationWndTab.eGuildHero then
        return LuaBattleSituationMgr.GetGuildQueenHeroInfo().Count
    elseif self.curTabStat == EnumSituationWndTab.Undefined then
        return LuaBattleSituationMgr.GetPlayersInCopy().Count
    else
        return LuaBattleSituationMgr.GetStatInfo().playerCount
    end
end

function LuaBattleSituationWnd:CellForRowAtIndex(index)
    if self.curTabStat == EnumSituationWndTab.eGuildHero then
        local guildQueenHeroInfo = LuaBattleSituationMgr.GetGuildQueenHeroInfo()

        if index < 0 or index >= guildQueenHeroInfo.Count then
            return nil
        end

        local cellIdentifier = "GuildHeroItemCell"
        local cell =
            self.m_DetailSimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier) or
            self.m_DetailSimpleTableView:AllocNewCellWithIdentifier(self.m_BangHuaHeroTemplate, cellIdentifier)

        local info = guildQueenHeroInfo[index]
        self:InitBangHuaHeroRankItem(cell, index + 1, info.Cls, info.PlayerName, info.Stage, info.Percent)

        return cell
    elseif self.curTabStat == EnumSituationWndTab.Undefined then
        local playersInCopyInfo = LuaBattleSituationMgr.GetPlayersInCopy()
        if index < 0 or index >= playersInCopyInfo.Count then
            return nil
        end

        local cellIdentifier = "PlayerItemCell"
        local cell =
            self.m_DetailSimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier) or
            self.m_DetailSimpleTableView:AllocNewCellWithIdentifier(self.m_PlayerItemTemplate, cellIdentifier)

        local info = playersInCopyInfo[index]
        self:InitPlayerItem(cell, info)

        return cell
    else
        local statInfo = LuaBattleSituationMgr.GetStatInfo().players

        if index < 0 or index >= LuaBattleSituationMgr.GetStatInfo().playerCount then
            return nil
        end

        local cellIdentifier = "RankItemCell"
        local cell =
            self.m_DetailSimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier) or
            self.m_DetailSimpleTableView:AllocNewCellWithIdentifier(self.m_RankItemTemplate, cellIdentifier)

        local info = statInfo[index]

        local statTotal = LuaBattleSituationMgr.GetStatInfo().statTotal
        local percentage = statTotal > 0 and info.playerStat * 1.0 / statTotal or 0

        self:InitRankItem(
            cell,
            index + 1,
            CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.playerClass),
            info.playerName,
            info.playerStat,
            percentage
        )

        return cell
    end
end

function LuaBattleSituationWnd:InitBangHuaHeroRankItem(go, rank, cls, playerName, stage, percentage)
    local rankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local iconSprite = go.transform:Find("ProfessionIcon"):GetComponent(typeof(UISprite))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local progressBars = {}
    for i = 1, 5 do -- 帮花这里五个阶段，先写死在这里
        table.insert(
            progressBars,
            go.transform:Find("Table/QnProgressBar" .. tostring(i)):GetComponent(typeof(QnProgressBar))
        )
    end

    rankLabel.text = tostring(rank)
    iconSprite.spriteName = Profession.GetIcon(cls)
    nameLabel.text = playerName

    for i = 1, #progressBars do
        if i < stage then
            progressBars[i]:SetProgress(1)
        elseif i == stage then
            progressBars[i]:SetProgress(percentage)
        else
            progressBars[i]:SetProgress(0)
        end
        progressBars[i]:SetText("")
    end
end

function LuaBattleSituationWnd:InitPlayerItem(go, info)
    local iconSprite = go.transform:Find("ProfessionIcon"):GetComponent(typeof(UISprite))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = go.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local teamInfoLabel = go.transform:Find("PercentageLabel"):GetComponent(typeof(UILabel))

    iconSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.playerClass))
    nameLabel.text = info.playerName
    levelLabel.text = "lv." .. tostring(info.playerGrade)
    teamInfoLabel.text = info.teamExists and LocalString.GetString("已组队") or LocalString.GetString("未组队")

    local id = info.playerId
    if id then
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(
        function()
            CPlayerInfoMgr.ShowPlayerPopupMenu(id, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, 
                CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType3.Default)
        end
    )
    end
end

function LuaBattleSituationWnd:InitRankItem(go, rank, cls, playerName, value, percentage)
    local rankLabel = go.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local iconSprite = go.transform:Find("ProfessionIcon"):GetComponent(typeof(UISprite))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    local percentageLabel = go.transform:Find("PercentageLabel"):GetComponent(typeof(UILabel))
    local progressBar = go.transform:Find("ProgressBar"):GetComponent(typeof(UIProgressBar))

    rankLabel.text = tostring(rank)
    iconSprite.spriteName = Profession.GetIcon(cls)
    nameLabel.text = playerName
    valueLabel.text = tostring(value)

    if LuaBattleSituationMgr.situationType ~= EnumSituationType.eHengYuDangKou then
        percentageLabel.text = tostring(math.floor(percentage * 100 + 0.5)) .. "%"
    else
        percentageLabel.text = tostring(math.floor((percentage * 1000) + 0.5) * 0.1 .. "%")
    end
    
    progressBar.value = percentage
end

function LuaBattleSituationWnd:UpdateDetailView(newStat)
    self.curTabStat = newStat
    self.m_DetailSimpleTableView:Clear()
    if self.curTabStat == EnumSituationWndTab.eGuildHero then
        LuaBattleSituationMgr.RequestBangHuaHeroInfo()
    elseif self.curTabStat == EnumSituationWndTab.Undefined then
        LuaBattleSituationMgr.RequestSituationSceneInfo()
    else
        LuaBattleSituationMgr.RequestSituationStat(self.curTabStat)
    end
end

function LuaBattleSituationWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRequestKouDaoStatInfoFinished", self, "OnRequestKouDaoStatInfoFinished")
    g_ScriptEvent:AddListener("OnRequestBangHuaStatInfoFinished", self, "OnRequestKouDaoStatInfoFinished")
    g_ScriptEvent:AddListener("OnRequestKouDaoSceneInfoFinished", self, "OnRequestKouDaoSceneInfoFinished")
    g_ScriptEvent:AddListener("OnQueryBangHuaScenePlayerInfoResult", self, "OnRequestKouDaoSceneInfoFinished")
    g_ScriptEvent:AddListener("OnRequestTianMenShanSceneInfoFinished", self, "OnRequestKouDaoSceneInfoFinished")
    g_ScriptEvent:AddListener("SendHengYuDangKouStat", self, "SendHengYuDangKouStat")

    g_ScriptEvent:AddListener("OnRequestResetStatInfoFinished", self, "OnRequestResetStatInfoFinished")
    g_ScriptEvent:AddListener("OnRequestTianMenShanStatInfoFinished", self, "OnRequestTianMenShanStatInfoFinished")
    g_ScriptEvent:AddListener("SendGTWChallengeFightDetailInfo", self, "OnSendGTWChallengeFightDetailInfo")
    
end

function LuaBattleSituationWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRequestKouDaoStatInfoFinished", self, "OnRequestKouDaoStatInfoFinished")
    g_ScriptEvent:RemoveListener("OnRequestBangHuaStatInfoFinished", self, "OnRequestKouDaoStatInfoFinished")
    g_ScriptEvent:RemoveListener("OnRequestKouDaoSceneInfoFinished", self, "OnRequestKouDaoSceneInfoFinished")
    g_ScriptEvent:RemoveListener("OnQueryBangHuaScenePlayerInfoResult", self, "OnRequestKouDaoSceneInfoFinished")
    g_ScriptEvent:RemoveListener("OnRequestTianMenShanSceneInfoFinished", self, "OnRequestKouDaoSceneInfoFinished")
    g_ScriptEvent:RemoveListener("SendHengYuDangKouStat", self, "SendHengYuDangKouStat")

    g_ScriptEvent:RemoveListener("OnRequestResetStatInfoFinished", self, "OnRequestResetStatInfoFinished")
    g_ScriptEvent:RemoveListener("OnRequestTianMenShanStatInfoFinished", self, "OnRequestTianMenShanStatInfoFinished")
    g_ScriptEvent:RemoveListener("SendGTWChallengeFightDetailInfo", self, "OnSendGTWChallengeFightDetailInfo")
end

function LuaBattleSituationWnd:OnDestroy()
end

--CallBacks
function LuaBattleSituationWnd:OnCloseButtonClick(go)
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaBattleSituationWnd:OnBroadcastButtonClick(go)
    LuaBattleSituationMgr.RequestBroadCastSituationStat(self.curTabStat)
end

function LuaBattleSituationWnd:OnClearButtonClick(go)
    LuaBattleSituationMgr.RequestResetSituationStat(self.curTabStat)
end

function LuaBattleSituationWnd:OnHideAllyValueChanged(val)
    PlayerSettings.HideNoneEnemyInKouDao = val
end

function LuaBattleSituationWnd:OnCheckbox2Changed(val)
    if LuaBattleSituationMgr.situationType == EnumSituationType.eHengYuDangKou then
        LuaHengYuDangKouMgr.m_ShowCurBattle = val
        if self.curTabStat ~= EnumSituationWndTab.Undefined then
            self:UpdateDetailView(self.curTabStat)
        end
    end
end

function LuaBattleSituationWnd:OnTabSelected(go, type)
    if not go then
        return
    end
    if self.curSelectedTab then
        self.curSelectedTab:GetComponent(typeof(CButton)).Selected = false
    end
    go:GetComponent(typeof(CButton)).Selected = true
    self.curSelectedTab = go
    self:UpdateDetailView(type)
end

function LuaBattleSituationWnd:OnRequestKouDaoStatInfoFinished(args)
    local tabStat = args[0]
    if self.curTabStat == tabStat then
        self.m_DetailSimpleTableView:LoadData(0, false)
    end
end

function LuaBattleSituationWnd:OnRequestTianMenShanStatInfoFinished(tabStat)
    if self.curTabStat == tabStat then
        self.m_DetailSimpleTableView:LoadData(0, false)
    end
end

function LuaBattleSituationWnd:OnRequestKouDaoSceneInfoFinished()
    if self.curTabStat == EnumSituationWndTab.Undefined then
        self.m_DetailSimpleTableView:LoadData(0, false)
    end
end

function LuaBattleSituationWnd:SendHengYuDangKouStat()
    self.m_DetailSimpleTableView:LoadData(0, false)
end

function LuaBattleSituationWnd:OnRequestResetStatInfoFinished(args)
    local tabStat, success = args[0], args[1]
    if success and self.curTabStat ~= EnumSituationWndTab.Undefined and self.curTabStat == tabStat then
        self.m_DetailSimpleTableView:Clear()
    end
end

function LuaBattleSituationWnd:OnSendGTWChallengeFightDetailInfo(detailType, detailInfo , playerCount, statTotal)
    print(detailType , self.curTabStat)
    if detailType == self.curTabStat then
        self.m_DetailSimpleTableView:LoadData(0, false)
    end
end
