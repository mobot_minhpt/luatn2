-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFriendRequestListView = import "L10.UI.CFriendRequestListView"
local CFriendSearchListItem = import "L10.UI.CFriendSearchListItem"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local FriendRequestItemData = import "FriendRequestItemData"
local UInt64 = import "System.UInt64"
CFriendRequestListView.m_NumberOfRows_CS2LuaHook = function (this) 
    return this.allData.Count
end
CFriendRequestListView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    if index < 0 and index >= this.allData.Count then
        return nil
    end
    local cellIdentifier = "FriendRequestListItemCell"
    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.friendItemTemplate, cellIdentifier)
    end

    local item = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CFriendSearchListItem))
    local data = this.allData[index]
    item:Init(data.isOnline, data.playerId, data.name, data.portraitName, data.expressionTxt, data.profileFrame, data.level, CIMMgr.Inst:IsMyFriend(data.playerId), data.isInXianShenStatus)
    item.OnAddFriendDelegate = MakeDelegateFromCSFunction(this.OnAddFriendButtonClick, MakeGenericClass(Action1, UInt64), this)
    return cell
end
CFriendRequestListView.m_OnEnable_CS2LuaHook = function (this) 

    this:LoadRequestList()

    EventManager.AddListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddFriendRequest, MakeDelegateFromCSFunction(this.OnAddFriendRequest, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_DelFriendRequest, MakeDelegateFromCSFunction(this.OnDeleteFriendRequest, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.MainPlayerRelationship_ClearFriendRequest, MakeDelegateFromCSFunction(this.OnClearFriendRequest, Action0, this))
end
CFriendRequestListView.m_OnDisable_CS2LuaHook = function (this) 

    EventManager.RemoveListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddFriendRequest, MakeDelegateFromCSFunction(this.OnAddFriendRequest, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_DelFriendRequest, MakeDelegateFromCSFunction(this.OnDeleteFriendRequest, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.MainPlayerRelationship_ClearFriendRequest, MakeDelegateFromCSFunction(this.OnClearFriendRequest, Action0, this))
end
CFriendRequestListView.m_LoadRequestList_CS2LuaHook = function (this) 

    this.tableView:Clear()
    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    CommonDefs.ListClear(this.allData)

    if CClientMainPlayer.Inst ~= nil then
        local list = CIMMgr.Inst.FriendRequestList
        do
            local i = 0
            while i < list.Count do
                local playerId = list[i]
                CommonDefs.ListAdd(this.allData, typeof(FriendRequestItemData), CreateFromClass(FriendRequestItemData, CIMMgr.Inst:GetBasicInfo(playerId)))
                i = i + 1
            end
        end
        this.tableView:LoadData(0, false)
        CIMMgr.Inst:SetFriendRequestRead()
    end
end
