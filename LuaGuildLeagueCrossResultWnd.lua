local DelegateFactory  = import "DelegateFactory"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonSelector = import "L10.UI.CCommonSelector"
local CButton = import "L10.UI.CButton"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"

LuaGuildLeagueCrossResultWnd = class()
RegistClassMember(LuaGuildLeagueCrossResultWnd, "m_TitleLabel")
RegistClassMember(LuaGuildLeagueCrossResultWnd, "m_Header")
RegistClassMember(LuaGuildLeagueCrossResultWnd, "m_Template")
RegistClassMember(LuaGuildLeagueCrossResultWnd, "m_ScrollView")
RegistClassMember(LuaGuildLeagueCrossResultWnd, "m_Table")
RegistClassMember(LuaGuildLeagueCrossResultWnd, "m_SelectorLabel")
RegistClassMember(LuaGuildLeagueCrossResultWnd, "m_Selector")

function LuaGuildLeagueCrossResultWnd:Awake()
    self.m_TitleLabel = self.transform:Find("Anchor/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_Header = self.transform:Find("Anchor/Header")
    self.m_Template = self.transform:Find("Anchor/ScrollView/Item").gameObject
    self.m_ScrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_SelectorLabel = self.transform:Find("Anchor/Label").gameObject
    self.m_Selector = self.transform:Find("Anchor/Selector"):GetComponent(typeof(CCommonSelector))
    self.m_Template:SetActive(false)
    self.m_SelectorLabel:SetActive(CommonDefs.IS_CN_CLIENT)
    self.m_Selector.gameObject:SetActive(CommonDefs.IS_CN_CLIENT)
end

function LuaGuildLeagueCrossResultWnd:Init()
    local NThMatch = LuaGuildLeagueCrossMgr.m_ScheduleInfo and LuaGuildLeagueCrossMgr.m_ScheduleInfo.levelRankHistoryIndex or GuildLeagueCross_Setting.GetData().NThMatch 
    self.m_TitleLabel.text = SafeStringFormat3(LocalString.GetString("第%s届跨服帮会联赛"), Extensions.ConvertToChineseString(NThMatch))
    local levelRankInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo and LuaGuildLeagueCrossMgr.m_ScheduleInfo.levelRankInfo or {} 
    local levelNames = GuildLeagueCross_Setting.GetData().CrossLevelName
    local titles = {}
    local defaultSelectIndex = -1
    local myServerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() or 0
    for i=1,#levelRankInfo do
        if i<=levelNames.Length then
            table.insert(titles, levelNames[i-1])
        else
            table.insert(titles,  SafeStringFormat3(LocalString.GetString("分区%d"), i))
        end
        if defaultSelectIndex<0 then
            for j=1,#levelRankInfo[i] do
                if levelRankInfo[i][j].serverId == myServerId then
                    defaultSelectIndex = i
                    break
                end
            end
        end
    end
    if defaultSelectIndex<0 then
        defaultSelectIndex = 1
    end
    local names = Table2List(titles, MakeGenericClass(List,cs_string))

    self.m_Selector:Init(names, DelegateFactory.Action_int(function ( index )
		self.m_Selector:SetName(names[index])
        self:LoadData(index+1, levelRankInfo[index+1])
    end))
    self.m_Selector:SetPopupAlignType(CPopupMenuInfoMgrAlignType.Top)
    if #levelRankInfo>0 then
        self.m_Selector:SetName(names[defaultSelectIndex-1])
        self:LoadData(defaultSelectIndex, levelRankInfo[defaultSelectIndex])
    end
end

function LuaGuildLeagueCrossResultWnd:LoadData(level, rankInfo)
    self:InitHeader(level, rankInfo)
    self:InitTable(level, rankInfo, 4)
end

function LuaGuildLeagueCrossResultWnd:InitHeader(level, rankInfo)
    for i=1,3 do
        local child = self.m_Header:GetChild(i-1)
        child:Find("NameLabel"):GetComponent(typeof(UILabel)).text = rankInfo[i].serverName
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnServerItemClick(child.gameObject, level, rankInfo[i].serverId, rankInfo[i].serverName) end)
    end
end

function LuaGuildLeagueCrossResultWnd:InitTable(level, rankInfo, startIndex)
    local n = self.m_Table.transform.childCount
    for i=0,n-1 do
        self.m_Table.transform:GetChild(i).gameObject:SetActive(false)
    end
    local idx = 0
    for i=startIndex,#rankInfo do
        local child = nil
        if idx < n then
            child = self.m_Table.transform:GetChild(idx).gameObject
        else
            child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
        end
        idx = idx + 1
        child:SetActive(true)
        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
        local isMyServer = LuaGuildLeagueCrossMgr:IsMyServer(rankInfo[i].serverId)
        child.transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("第%s名"), Extensions.ConvertToChineseString(rankInfo[i].index))
        child.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%s%s", isMyServer and "[c][79ff7c]" or "", rankInfo[i].serverName)
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnServerItemClick(child.gameObject, level, rankInfo[i].serverId, rankInfo[i].serverName) end)
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaGuildLeagueCrossResultWnd:OnServerItemClick(go, level, serverId, serverName)
    LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(serverId, serverName)
end

