local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaHuLuWaTravelEventPopWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHuLuWaTravelEventPopWnd, "HasItemView", "HasItemView", GameObject)
RegistChildComponent(LuaHuLuWaTravelEventPopWnd, "NoItemView", "NoItemView", GameObject)
RegistChildComponent(LuaHuLuWaTravelEventPopWnd, "ItemIcon", "ItemIcon", CUITexture)

--@endregion RegistChildComponent end

function LuaHuLuWaTravelEventPopWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaHuLuWaTravelEventPopWnd:Init()
    local id = LuaHuLuWa2022Mgr.m_TravelTipId
    local data = HuluBrothers_Adventure.GetData(id)

    if data then
        local iconName = data.Type == "baoxiang" and "baoxiang_close" or data.Type
        local iconpath = SafeStringFormat3("UI/Texture/Transparent/Material/huluwatravelworldwnd_bg_%s.mat",iconName)
        local hasItem = data.Award ~= 0

        local showView
        if hasItem then
            showView = self.HasItemView.transform
            self.HasItemView:SetActive(true)
            self.NoItemView:SetActive(false)
            local iconPath = Item_Item.GetData(data.Award).Icon
            self.ItemIcon:LoadMaterial(iconPath)
        else
            showView = self.NoItemView.transform
            self.HasItemView:SetActive(false)
            self.NoItemView:SetActive(true)
        end
        local nameLabel = showView:Find("EventName"):GetComponent(typeof(UILabel))
        local desc = showView:Find("EventDesc"):GetComponent(typeof(UILabel))
        nameLabel.text = data.Name
        
        desc.text = data.Desc
        local iconTexture = showView:Find("EventIcon")
        if iconTexture then
            iconTexture = showView:Find("EventIcon"):GetComponent(typeof(CUITexture))
            iconTexture:LoadMaterial(iconpath)
        end
    end
end

function LuaHuLuWaTravelEventPopWnd:OnDisable()
    if not LuaHuLuWa2022Mgr.m_TravelTipId then
        return
    end
    g_ScriptEvent:BroadcastInLua("CloseTravelEventPopWnd", LuaHuLuWa2022Mgr.m_TravelTipId)
end

--@region UIEvent

--@endregion UIEvent

