LuaBuffInfoWndMgr = {}

LuaBuffInfoWndMgr.m_BuffTemplateId = 0
LuaBuffInfoWndMgr.m_isEnable = true
LuaBuffInfoWndMgr.m_ExtraInfoList = nil

function LuaBuffInfoWndMgr:ShowWnd(buffTemplateId, isEnable, extraInfoList)
    self.m_BuffTemplateId = buffTemplateId
    self.m_isEnable = isEnable
    self.m_ExtraInfoList = extraInfoList

    CUIManager.ShowUI(CLuaUIResources.BuffInfoWnd)
end
