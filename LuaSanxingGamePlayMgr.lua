local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaSanxingGamePlayMgr = {}

function LuaSanxingGamePlayMgr.IsInPlay()
	if CScene.MainScene then
		local gamePlayId = CScene.MainScene.GamePlayDesignId
		if gamePlayId == SanXingBattle_Setting.GetData().GamePlayId then
			return true
		end
	end
	return false
end

function LuaSanxingGamePlayMgr.InitPhaText(labelNode)
	local sceneName = CScene.MainScene.SceneName

end

function LuaSanxingGamePlayMgr.ClearData()
	LuaSanxingGamePlayMgr.InGuidePha1 = nil
	LuaSanxingGamePlayMgr.InGuidePha2 = nil
	LuaSanxingGamePlayMgr.InGuidePha1_2 = nil
	LuaSanxingGamePlayMgr.InGuidePha2_2 = nil
	LuaSanxingGamePlayMgr.currentScore = nil
	LuaSanxingGamePlayMgr.totalScore = nil
	LuaSanxingGamePlayMgr.sanxingStage = nil
	LuaSanxingGamePlayMgr.sanxingForce = nil
	LuaSanxingGamePlayMgr.FlagPos = nil
	LuaSanxingGamePlayMgr.BossPos = nil
	LuaSanxingGamePlayMgr.matchTotalTime = nil
	LuaSanxingGamePlayMgr.EndScore = nil
	LuaSanxingGamePlayMgr.EndSkillId = nil
	LuaSanxingGamePlayMgr.EndItemId = nil
	LuaSanxingGamePlayMgr.EndRank = nil
	LuaSanxingGamePlayMgr.EndRankNum = nil
end

function LuaSanxingGamePlayMgr.OnMainPlayerCreated()
	if CScene.MainScene then
		local gamePlayId = CScene.MainScene.GamePlayDesignId
		if gamePlayId == SanXingBattle_Setting.GetData().GamePlayId then
			LuaSanxingGamePlayMgr.Inited = true
			CLuaMiniMap:InitSanxingFlag()
			g_ScriptEvent:AddListener("SanxingFlagUpdate", CLuaMiniMap, "UpdateSanxingFlagPos")
		end
	end
end

function LuaSanxingGamePlayMgr.OnMainPlayerDestroyed()
	if LuaSanxingGamePlayMgr.Inited then
		LuaSanxingGamePlayMgr.Inited = false
		CLuaMiniMap:ClearSanxingFlag()
    g_ScriptEvent:RemoveListener("SanxingFlagUpdate", CLuaMiniMap, "UpdateSanxingFlagPos")
	end
end

function Gas2Gac.QuerySanXingBattelePlayInfoResult(winforce, jinshaScore, attackData, defendData)
	LuaSanxingGamePlayMgr.winForce = winforce
	LuaSanxingGamePlayMgr.jinshaScore = jinshaScore
	LuaSanxingGamePlayMgr.attackDataTable = {}
	LuaSanxingGamePlayMgr.defendDataTable = {}
	local attackList = MsgPackImpl.unpack(attackData)
	for i=0, attackList.Count-1, 6 do
		local playerId = tonumber(attackList[i])
		local playerName = attackList[i+1]
		local clazz = tonumber(attackList[i+2])
		local score = tonumber(attackList[i+3])
		local flagScore = tonumber(attackList[i+4])
		local serverName = attackList[i+5]
		table.insert(LuaSanxingGamePlayMgr.attackDataTable,{playerId,playerName,clazz,score,flagScore,serverName})
	end

	local defendList = MsgPackImpl.unpack(defendData)
	for i=0, defendList.Count-1, 6 do
		local playerId = tonumber(defendList[i])
		local playerName = defendList[i+1]
		local clazz = tonumber(defendList[i+2])
		local score = tonumber(defendList[i+3])
		local flagScore = tonumber(defendList[i+4])
		local serverName = defendList[i+5]
		table.insert(LuaSanxingGamePlayMgr.defendDataTable,{playerId,playerName,clazz,score,flagScore,serverName})
	end

	if CUIManager.IsLoaded(CLuaUIResources.SanxingResultWnd) then
		g_ScriptEvent:BroadcastInLua("SanxingResultUpdate")
	else
		CUIManager.ShowUI(CLuaUIResources.SanxingResultWnd)
	end
end

function Gas2Gac.ShowSanXingBattleGateInteractiveMenu(x, y)
	MessageWndManager.ShowOKCancelMessage(LocalString.GetString("是否通过城门？"), DelegateFactory.Action(function ()
		Gac2Gas.RequestSanXingBattleGateTeleport()
	end), DelegateFactory.Action(function ()
	end), nil, nil, false)
end

function Gas2Gac.StartSanXingBattleGuide(force)
	LuaSanxingGamePlayMgr.guildForce = force
	if force == 0 then
		CLuaGuideMgr.TryTriggerSanxingGuide1()
	elseif force == 1 then
		CLuaGuideMgr.TryTriggerSanxingGuide1_1()
	end
end

function Gas2Gac.JoinSanXingBattFreeMatch(estimatedTime, elo)
	LuaSanxingGamePlayMgr.matchTime = CServerTimeMgr.Inst.timeStamp
	LuaSanxingGamePlayMgr.matchTotalTime = estimatedTime

	--
	CUIManager.ShowUI(CLuaUIResources.SanxingPipeiWnd)
	--if CUIManager.IsLoaded(CLuaUIResources.SanxingShowWnd) then
		--CUIManager.CloseUI(CLuaUIResources.SanxingShowWnd)
	--end
end

function Gas2Gac.QuitSanXingBattleFreeMatch()
end

function Gas2Gac.OpenSanXingBattleFreeMatchWnd(bFreeMatch, beginTime, estimatedTime, elo)
	LuaSanxingGamePlayMgr.matchStatus = bFreeMatch
	LuaSanxingGamePlayMgr.matchTime = beginTime
	LuaSanxingGamePlayMgr.matchTotalTime = estimatedTime
	LuaSanxingGamePlayMgr.matchScore = elo

	if bFreeMatch == true then
		CUIManager.ShowUI(CLuaUIResources.SanxingPipeiWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.SanxingShowWnd)
	end
end

function Gas2Gac.SyncSanXingBattleCurrentStage(stage, force)
	LuaSanxingGamePlayMgr.sanxingStage = stage
	LuaSanxingGamePlayMgr.sanxingForce = force

	g_ScriptEvent:BroadcastInLua("SanxingStageUpdate")
end

function Gas2Gac.SyncSanXingBattleJinShaScore(currentScore, totalScore)
	LuaSanxingGamePlayMgr.currentScore = currentScore
	LuaSanxingGamePlayMgr.totalScore = totalScore
	g_ScriptEvent:BroadcastInLua("SanxingScoreUpdate")
end

function Gas2Gac.SyncSanXingFlagPosition(data)
	local list = MsgPackImpl.unpack(data)
	if list then
		for i=0, list.Count-1, 3 do
			local templateId = tonumber(list[i])
			local posX = tonumber(list[i+1]) + 3
			local posY = tonumber(list[i+2]) + 2
			if not LuaSanxingGamePlayMgr.FlagPos then
				LuaSanxingGamePlayMgr.FlagPos = {}
			end
			LuaSanxingGamePlayMgr.FlagPos[templateId] = {posX,posY}
		end
		g_ScriptEvent:BroadcastInLua("SanxingFlagUpdate")
	end
end

function Gas2Gac.SyncSanXingBossPosition(force, templateId, x, y)
	if not LuaSanxingGamePlayMgr.BossPos then
		LuaSanxingGamePlayMgr.BossPos = {}
	end
	LuaSanxingGamePlayMgr.BossPos = {x,y}
	g_ScriptEvent:BroadcastInLua("SanxingBossUpdate")
end

function Gas2Gac.OpenSanXingBattleReliveWnd(force, dieTime)
	LuaSanxingGamePlayMgr.DieEndTime = dieTime

	CUIManager.ShowUI(CLuaUIResources.SanxingRebornWnd)
end

function Gas2Gac.SetSanXingDefaultRelivePosSuccess(force, index)
end

function Gas2Gac.CloseSanXingBattleReliveWnd()
end

function Gas2Gac.PlayerEnterSanXingBattleCopy(force)
	LuaSanxingGamePlayMgr.ClearData()
end

function Gas2Gac.ShowSanXingBattlePlayFx(fxPath)
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {fxPath})
end

function Gas2Gac.SendSanXingBattleAwardInfo(score, skillId, itemId, rank, awardNum)
	LuaSanxingGamePlayMgr.EndScore = score
	LuaSanxingGamePlayMgr.EndSkillId = skillId
	LuaSanxingGamePlayMgr.EndItemId = itemId
	LuaSanxingGamePlayMgr.EndRank = rank
	LuaSanxingGamePlayMgr.EndRankNum = awardNum
end

function Gas2Gac.OpenMenPaiHuTongSkillReplaceWnd(replaceCount, totalCount, skillData)
	LuaSanxingGamePlayMgr.SkillReplaceCount = replaceCount
	LuaSanxingGamePlayMgr.SkillTotalCount = totalCount
	LuaSanxingGamePlayMgr.SkillTable = {}

	local list = MsgPackImpl.unpack(skillData)
	if list then
		for i=0, list.Count-1, 2 do
			-- 分别是技能id、过期时间
			local skillId = tonumber(list[i])
			local timeStamp = tonumber(list[i+1])
			table.insert(LuaSanxingGamePlayMgr.SkillTable,{skillId,timeStamp})
		end
	end

	if CUIManager.IsLoaded(CLuaUIResources.SanxingChangeSkillWnd) then
		g_ScriptEvent:BroadcastInLua("SanxingBattleSkillUpdate")
	else
		CUIManager.ShowUI(CLuaUIResources.SanxingChangeSkillWnd)
	end
end

function Gas2Gac.ReplyMenPaiHuTongSkillInfo(engineId, data)
	local skillTable = {}
	local list = MsgPackImpl.unpack(data)
	if list then
		for i=0, list.Count-1, 1 do
			local skillName = list[i]
			table.insert(skillTable,skillName)
		end
		g_ScriptEvent:BroadcastInLua("SanxingMenPaiHuTongUpdate", engineId, skillTable)
	end
end

function Gas2Gac.ReplySanXingBattleFreeMatchPlayerNum(totalNum, copyPlayerNum)
	LuaSanxingGamePlayMgr.pipeiNum = totalNum
	LuaSanxingGamePlayMgr.pipeiTotalNum = copyPlayerNum
	g_ScriptEvent:BroadcastInLua("SanxingBattlePipeiUpdate")
end
