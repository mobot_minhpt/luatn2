-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLogMgr = import "L10.CLogMgr"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CTeamMember = import "L10.Game.CTeamMember"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamWnd = import "L10.UI.Team.CTeamWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local L10 = import "L10"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

CTeamWnd.m_Start_CS2LuaHook = function (this) 
    --tabBar.ChangeTab(0);
    --Refresh();

    --主动刷新一次
    --tabBar.ChangeTab(0);
    --Refresh();

    --主动刷新一次
    EventManager.Broadcast(EnumEventType.TeamInfoChange)

    if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.TeamRecruitInTaskWndGuide) then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.TeamRecruitInTaskWndGuide)
    elseif not COpenEntryMgr.Inst:GetGuideRecord(189) and CTeamMgr.Inst:MainPlayerIsTeamLeader() then
		COpenEntryMgr.Inst:SaveGuidePhase(189, 0)
		CGuideMgr.Inst:StartGuide(189, 0)
    else
        if CGuideMgr.Inst:IsInPhase(189) then
            CGuideMgr.Inst:EndCurrentPhase()
        end
	end
end
CTeamWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CUIManager.CloseUI(CUIResources.TeamWnd)
    end)
    UIEventListener.Get(this.TitleLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
        if CTeamGroupMgr.Instance:InTeamGroup() then
            CUIManager.CloseUI(CUIResources.TeamWnd)
            Gac2Gas.RequestOpenTeamGroupWnd()
        end
    end)
    this.tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) 
        this:UpdateTab(index)
    end)

    this:UpdateTab(0)
end
CTeamWnd.m_UpdateTab_CS2LuaHook = function (this, tabIndex) 
    repeat
        local default = tabIndex
        if default == 0 then
            do
                this.infoTabContent.gameObject:SetActive(true)
                this.applyTabContent.gameObject:SetActive(false)
            end
            break
        elseif default == 1 then
            do
                this.infoTabContent.gameObject:SetActive(false)
                this.applyTabContent.gameObject:SetActive(true)
            end
            break
        end
    until 1
end
CTeamWnd.m_OnEnable_CS2LuaHook = function (this) 
    --CTeamModel.Inst.refreshTeamAction += RefreshTeam;
    --CTeamModel.Inst.refreshMemberInfoAction += RefreshMemberInfo;

    --CTeamModel.Inst.refreshTeamAction += RefreshTeam;
    --CTeamModel.Inst.refreshMemberInfoAction += RefreshMemberInfo;

    EventManager.AddListener(EnumEventType.TeamInfoChange, MakeDelegateFromCSFunction(this.RefreshTeam, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.TeamMemberInfoChange, MakeDelegateFromCSFunction(this.RefreshMemberInfo, MakeGenericClass(Action1, UInt64), this))


    --如果有队伍
    if CTeamMgr.Inst:TeamExists() and CTeamMgr.Inst.HasNewApplicant then
        this.tabBar:ChangeTab(1, false)
        --直接切换到申请界面
    else
        this.tabBar:ChangeTab(0, false)
    end

    this:Refresh()

    this:OnUpdateTeamApplyAlertStatus()

    EventManager.AddListener(EnumEventType.UpdateTeamApplyAlertStatus, MakeDelegateFromCSFunction(this.OnUpdateTeamApplyAlertStatus, Action0, this))
end
CTeamWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.TeamInfoChange, MakeDelegateFromCSFunction(this.RefreshTeam, Action0, this))
    --CTeamModel.Inst.refreshTeamAction -= RefreshTeam;
    --CTeamModel.Inst.refreshMemberInfoAction -= RefreshMemberInfo;
    EventManager.RemoveListener(EnumEventType.UpdateTeamApplyAlertStatus, MakeDelegateFromCSFunction(this.OnUpdateTeamApplyAlertStatus, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.TeamMemberInfoChange, MakeDelegateFromCSFunction(this.RefreshMemberInfo, MakeGenericClass(Action1, UInt64), this))
    LuaTeamMemberItem.Identify2Rotation = nil
end
CTeamWnd.m_RefreshTeam_CS2LuaHook = function (this) 
    local members = CreateFromClass(MakeGenericClass(List, CTeamMember))
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        members = CTeamMgr.Inst.Members
    end

    this:Refresh()
    this.infoTabContent:RefreshTeam(members)
    this.applyTabContent:RefreshApplyList()
end
CTeamWnd.m_RefreshMemberInfo_CS2LuaHook = function (this, playerId) 
    local find = CTeamMgr.Inst:GetMemberById(playerId)
    if find ~= nil then
        this:Refresh()

        this.infoTabContent:RefreshMember(find)
        this.applyTabContent:RefreshMember(find)
    end
end
CTeamWnd.m_Refresh_CS2LuaHook = function (this) 
    --如果没有队伍
    --如果没有队伍
    if CTeamMgr.Inst:TeamExists() then
        if IsCrossServerTeamInfoDisplayOpen() then
            this.TitleLabel.text = SafeStringFormat3(LocalString.GetString("队伍[acf8ff](%s)[-]"), GetCrossServerTeamInfoDisplayText(CTeamMgr.Inst.TeamId))
        else
            this.TitleLabel.text = LocalString.GetString("队伍")
        end
        this.haveTeam:SetActive(true)
    else
        this.TitleLabel.text = LocalString.GetString("队伍")
        this.haveTeam:SetActive(false)
    end

    -- 切换团队
    local changeViewBtn = this.transform:Find("Anchor/TeamInfoTabContent/changeViewBtn").gameObject
    if CTeamGroupMgr.Instance:InTeamGroup() then
        changeViewBtn:SetActive(true)
        UIEventListener.Get(changeViewBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
            CUIManager.CloseUI(CUIResources.TeamWnd)
            CUIManager.ShowUI(CUIResources.TeamGroupWnd)
        end)
    else
        changeViewBtn:SetActive(false)
    end

    this.infoTabContent:RefreshTeamMatchVisuality()
end
CTeamWnd.m_GetTargetButton_CS2LuaHook = function (this) 
    if CTeamMgr.Inst:TeamExists() then
        --如果有队伍就中断引导
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
        return nil
    end
    return this.infoTabContent:GetTargetButton()
end
CTeamWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 
    if methodName == "GetTargetButton" then
        return this:GetTargetButton()
    elseif methodName == "GetMoreOptBtn" then
        return this.infoTabContent.moreOptBtn
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end
