require("3rdParty/ScriptEvent")
require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local GameObject = import "UnityEngine.GameObject"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"

LuaNewYear2019TaskTipWnd=class()
RegistChildComponent(LuaNewYear2019TaskTipWnd,"msgLabel", UILabel)
RegistChildComponent(LuaNewYear2019TaskTipWnd,"timeLabel", UILabel)
RegistChildComponent(LuaNewYear2019TaskTipWnd,"jumpBtn", GameObject)

RegistClassMember(LuaNewYear2019TaskTipWnd, "m_Time")
RegistClassMember(LuaNewYear2019TaskTipWnd, "m_Begin")

function LuaNewYear2019TaskTipWnd:SetTime()
  local min = math.floor(self.m_Time / 60)
  local sec = self.m_Time - min * 60
  if min < 10 then
    min = '0' .. min
  end
  sec = math.floor(sec)
  if sec < 10 then
    sec = '0' .. sec
  end
  self.timeLabel.text = min .. ':' .. sec
end

function LuaNewYear2019TaskTipWnd:Update()
    if self.m_Begin then
      self.m_Time = self.m_Time - Time.deltaTime
      if self.m_Time <= 0 then
        CUIManager.CloseUI("NewYear2019TaskTipWnd")
      else
        self:SetTime()
      end
    end
end

function LuaNewYear2019TaskTipWnd:GoToGuildTalk()
  CSocialWndMgr.ShowChatWnd(EChatPanel.Guild)
  CUIManager.CloseUI("NewYear2019TaskTipWnd")
end

function LuaNewYear2019TaskTipWnd:Init()
  CommonDefs.AddOnClickListener(self.jumpBtn, DelegateFactory.Action_GameObject(function(go) self:GoToGuildTalk()  end), false)
  self.msgLabel.text = LuaNewYear2019Mgr.GuildQuizMsg
  self.m_Time = LuaNewYear2019Mgr.GuildQuizTime
  self.m_Begin = true
  self:SetTime()
end

function LuaNewYear2019TaskTipWnd:OnDestroy()

end

return LuaNewYear2019TaskTipWnd
