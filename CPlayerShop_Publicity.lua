-- Auto Generated!!
local Boolean = import "System.Boolean"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_Publicity = import "L10.UI.CPlayerShop_Publicity"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopTemplateItem = import "L10.UI.CPlayerShopTemplateItem"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local ItemTemplateInfo = import "L10.UI.ItemTemplateInfo"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local UInt32 = import "System.UInt32"
CPlayerShop_Publicity.m_Init_CS2LuaHook = function (this)
    this.m_BuySectionData = CPlayerShopMgr.Inst:GetPublicityGroupNames()
    CommonDefs.DictClear(this.m_BuySubSectionData)
    do
        local i = 0
        while i < this.m_BuySectionData.Count do
            local subSectionNames = CPlayerShopMgr.Inst:GetPublicityItemSubTypeNames(this.m_BuySectionData[i])
            CommonDefs.DictAdd(this.m_BuySubSectionData, typeof(String), this.m_BuySectionData[i], typeof(MakeGenericClass(List, String)), subSectionNames)
            i = i + 1
        end
    end
    this.m_BuySectionTableView:Init(this.m_BuySectionData, this.m_BuySubSectionData, true)
    local billButton = this.transform:Find("BillButton")
    if billButton then
        UIEventListener.Get(billButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CPlayerShopMgr.ShowPlayerShopBillWnd()
        end)
    end
end
CPlayerShop_Publicity.m_OnDisable_CS2LuaHook = function (this) 
    this.m_BuySectionTableView.OnBuySectionSelect = CommonDefs.CombineListner_Action_int_int(this.m_BuySectionTableView.OnBuySectionSelect, MakeDelegateFromCSFunction(this.OnBuySectionSelect, MakeGenericClass(Action2, Int32, Int32), this), false)
    this.m_ShopItemTemplateTable.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_ShopItemTemplateTable.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectTemplateItem, MakeGenericClass(Action1, Int32), this), false)
    CYuanbaoMarketMgr.ClearPlayerShopData()

    this.m_EquipSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_EquipSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_TalismanSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_TalismanSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_ZhushabiSearch.OnSearchCallback = CommonDefs.CombineListner_Action_uint_SearchOption_bool(this.m_ZhushabiSearch.OnSearchCallback, MakeDelegateFromCSFunction(this.OnSearchCallBack, MakeGenericClass(Action3, UInt32, SearchOption, Boolean), this), false)
    this.m_EquipSearch.OnSearchWordsCallback = CommonDefs.CombineListner_Action_string_uint_uint_uint_SearchOption(this.m_EquipSearch.OnSearchWordsCallback, MakeDelegateFromCSFunction(this.OnSearchWordsCallBack, MakeGenericClass(Action5, String, UInt32, UInt32, UInt32, SearchOption), this), false)
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopOnShelfNumResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfNumResult, MakeGenericClass(Action2, MakeGenericClass(List, UInt32), Boolean), this))
end
CPlayerShop_Publicity.m_OnBuySectionSelect_CS2LuaHook = function (this, section, subsection)
    if section == 0 then
        this:GotoAllPublicity()
    elseif section == 1 then
        this:GotoPlayerShopFocus()
    elseif section == 2 then
        this:GotoEquipSearch(SearchOption.Valueable)
    elseif section == 3 then
        this:GotoTalismanSearch()
    else
        local typeName = this.m_BuySectionData[section]
        local subTypeName = CommonDefs.DictGetValue(this.m_BuySubSectionData, typeof(String), typeName)[subsection]

        if typeName == CPlayerShop_Publicity.ZhushabiTypeName and subTypeName == CPlayerShop_Publicity.ZhushabiSubtypeName then
            this:GotoZhushabiSearch()
            return
        end

        this:GotoTemplateTable(typeName, subTypeName)
    end
end
CPlayerShop_Publicity.m_OnSearchWordsCallBack_CS2LuaHook = function (this, wordIdStr, gradeRange, type, subtype, option) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(true)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo:Init2(wordIdStr, gradeRange, type, subtype, option, true, false)
end
CPlayerShop_Publicity.m_GotoShopItemInfo_CS2LuaHook = function (this, templateId, option) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(true)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo:Init(templateId, option, true, false)
end
CPlayerShop_Publicity.m_GotoTemplateTable_CS2LuaHook = function (this, typeName, subTypeName)
    this.m_ShopItemTemplateTable.gameObject:SetActive(true)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    local Ids = CPlayerShopMgr.Inst:GetItemTemplatesInPublicityGroup(typeName, subTypeName)
    CommonDefs.ListClear(this.m_CurrentTemplateIds)
    local data = CreateFromClass(MakeGenericClass(List, Object))
    local data2 = CreateFromClass(MakeGenericClass(List, Object))
    local data3 = CreateFromClass(MakeGenericClass(List, Object))
    do
        local i = 0
        while i < Ids.Count do
            CommonDefs.ListAdd(this.m_CurrentTemplateIds, typeof(ItemTemplateInfo), CreateFromClass(ItemTemplateInfo, Ids[i], false, false))
            if i < 50 then 
                CommonDefs.ListAdd(data, typeof(UInt32), Ids[i])
            elseif i < 100 then
                CommonDefs.ListAdd(data2, typeof(UInt32), Ids[i])
			else
                CommonDefs.ListAdd(data3, typeof(UInt32), Ids[i])
            end
            i = i + 1
        end
    end
    Gac2Gas.QueryPlayerShopOnShelfNum(MsgPackImpl.pack(data), MsgPackImpl.pack(data2), MsgPackImpl.pack(data3), true, SearchOption_lua.All, false)
    this.m_ShopItemTemplateTable:ReloadData(true, false)
end
CPlayerShop_Publicity.m_GotoEquipSearch_CS2LuaHook = function (this, option) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(true)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_EquipSearch:Init(true, option)
end
CPlayerShop_Publicity.m_GotoTalismanSearch_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(true)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_TalismanSearch:Init(true)
end
CPlayerShop_Publicity.m_GotoZhushabiSearch_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(true)
    this.m_ZhushabiSearch:Init(true, SearchOption.Valueable)
end
CPlayerShop_Publicity.m_GotoPlayerShopFocus_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(false)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(true)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus:Init(true)
end
CPlayerShop_Publicity.m_GotoAllPublicity_CS2LuaHook = function (this) 
    this.m_ShopItemTemplateTable.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo.gameObject:SetActive(true)
    this.m_EquipSearch.gameObject:SetActive(false)
    this.m_TalismanSearch.gameObject:SetActive(false)
    this.m_PlayerShopFocus.gameObject:SetActive(false)
    this.m_ZhushabiSearch.gameObject:SetActive(false)
    this.m_PlayerShopItemInfo:Init(0, SearchOption.AllPublicity, true, true)
end
CPlayerShop_Publicity.m_OnQueryPlayerShopOnShelfNumResult_CS2LuaHook = function (this, selledCounts, publicity)
    if not publicity then
        return
    end
    do
        local i = 0
        while i < selledCounts.Count do
            local id = selledCounts[i]
            local count = selledCounts[i + 1]
            do
                local j = 0
                while j < this.m_CurrentTemplateIds.Count do
                    if this.m_CurrentTemplateIds[j].TemplateId == id then
                        this.m_CurrentTemplateIds[j].SellCount = count
                        local item = TypeAs(this.m_ShopItemTemplateTable:GetItemAtRow(j), typeof(CPlayerShopTemplateItem))
                        if item ~= nil then
                            item:UpdateData(this.m_CurrentTemplateIds[j].TemplateId, this.m_CurrentTemplateIds[j].SellCount, this.m_CurrentTemplateIds[j].IsFocus, false, false)
                        end
                        break
                    end
                    j = j + 1
                end
            end
            i = i + 2
        end
    end
end
