local UILabel = import "UILabel"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local EPropStatus = import "L10.Game.EPropStatus"

LuaZongMenPracticeReliveWnd = class()

RegistClassMember(LuaZongMenPracticeReliveWnd,"m_ReliveLabel")
RegistClassMember(LuaZongMenPracticeReliveWnd,"m_StartTime")
RegistClassMember(LuaZongMenPracticeReliveWnd,"m_Duration")
RegistClassMember(LuaZongMenPracticeReliveWnd,"m_CloseWhenAlive")

function LuaZongMenPracticeReliveWnd:Awake()
    self.m_StartTime = -1
    self.m_Duration = LuaZongMenMgr.m_NextTime - CServerTimeMgr.Inst.timeStamp
    self.m_ReliveLabel = self.transform:Find("Anchor/ContentRoot/CountDownLabel"):GetComponent(typeof(UILabel))
    self.m_CloseWhenAlive = true
end

function LuaZongMenPracticeReliveWnd:Init()
    self.m_StartTime = CServerTimeMgr.Inst.timeStamp
end

function LuaZongMenPracticeReliveWnd:Update()
    if self.m_CloseWhenAlive and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("SectBind")) == 0 then
        self:CountdownOver()
    end
    if self.m_Duration <= 0 then
        self:CountdownOver()
    end
    if CServerTimeMgr.Inst.timeStamp - self.m_StartTime <= self.m_Duration then
        local seconds = math.floor(self.m_Duration - (CServerTimeMgr.Inst.timeStamp - self.m_StartTime) + 0.5)
        seconds = seconds < 0 and 0 or seconds
        self.m_ReliveLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), seconds)
    else
        self:CountdownOver()
    end
end

function LuaZongMenPracticeReliveWnd:CountdownOver()
    CUIManager.CloseUI(CLuaUIResources.ZongMenPracticeReliveWnd)
end