local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animation = import "UnityEngine.Animation"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"

LuaNanDuFanHuaOverallWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "ShangShiJieKouEntrance", "ShangShiJieKouEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "SanFaSiEntrance", "SanFaSiEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "ShiLiuLouEntrance", "ShiLiuLouEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "WuYiXiangEntrance", "WuYiXiangEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "ShiLiQingHuaiEntrance", "ShiLiQingHuaiEntrance", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "UpLevelButton", "UpLevelButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "LordRewardItem1", "LordRewardItem1", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "LordRewardItem2", "LordRewardItem2", GameObject)
RegistChildComponent(LuaNanDuFanHuaOverallWnd, "LordRewardItem3", "LordRewardItem3", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaOverallWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.entranceList = {self.WuYiXiangEntrance, self.ShiLiuLouEntrance, self.ShiLiQingHuaiEntrance, self.ShangShiJieKouEntrance, self.SanFaSiEntrance}
    self:ShowConstUI()
    self:InitUIEvent()

    g_ScriptEvent:AddListener("NanDuLordBecomeLordSuccess", self, "RefreshUI")
end

function LuaNanDuFanHuaOverallWnd:Init()

end

function LuaNanDuFanHuaOverallWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("NanDuLordBecomeLordSuccess", self, "RefreshUI")
end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaOverallWnd:ShowConstUI()
    local rewardString = NanDuFanHua_LordSetting.GetData().ShowLordRewardItemId
    local showVideoRewardItem = NanDuFanHua_LordSetting.GetData().ShowVideoRewardItem
    local itemList = {self.LordRewardItem1, self.LordRewardItem2, self.LordRewardItem3}
    
    local rewardList = g_LuaUtil:StrSplit(rewardString,";")
    for i = 1, #rewardList do
        local itemId = tonumber(rewardList[i])
        local clickShowVideo = false
        if itemId == showVideoRewardItem then
            clickShowVideo = true
        end

        local itemGo = itemList[i]
        if itemGo then
            local texture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
            local ItemData = Item_Item.GetData(itemId)
            if ItemData then texture:LoadMaterial(ItemData.Icon) end
            if clickShowVideo then
                UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function (_)
                    local previewUrl = NanDuFanHua_LordSetting.GetData().ShowVideoRewardUrl
                    if not CCPlayerCtrl.IsSupportted() then
                        g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
                        return
                    end
                    AMPlayer.Inst:PlayCG(previewUrl, nil, 1)
                    g_MessageMgr:ShowMessage('NANDULORD_REWARD_VIDEO')
                end)
            else
                UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function (_)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
                end)
            end
        end
    end
end

function LuaNanDuFanHuaOverallWnd:OnEnable()
    self.lordData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.lordData = data[1]
        end
    end
    self:RefreshUI()
end

function LuaNanDuFanHuaOverallWnd:OnDisable()
end

function LuaNanDuFanHuaOverallWnd:InitUIEvent()
    local juBaoPenNameList = {"WuYiXiangFameIndex", "ShiLiuLouFameIndex", "ShiLiQingHuaiFameIndex", "ShangShiJieKouFameIndex", "SanFaSiFameIndex"}
    local fameName = { "WuYiXiangFameIndex", "ShiLiuLouFameIndex", "ShiLiQingHuaiFameIndex", "ShangShiJieKouFameIndex", "SanFaSiFameIndex"}

    for i = 1, #self.entranceList do
        UIEventListener.Get(self.entranceList[i].transform:Find("EntranceButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            self:ShowSearchButtonAndHideOther(i)        
        end)

        UIEventListener.Get(self.entranceList[i].transform:Find("SearchButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            local overallFameName = fameName[i]
            for i = 1, #juBaoPenNameList do
                if juBaoPenNameList[i] == overallFameName then
                    g_ScriptEvent:BroadcastInLua("NanDuFanHua_OpenJuBaoPenAndHideOverall", i)
                    break
                end
            end
        end)
    end

    UIEventListener.Get(self.UpLevelButton).onClick = DelegateFactory.VoidDelegate(function (p)
        local aniComp = self.UpLevelButton.transform:Find("vfx_chengweilingzhu"):GetComponent(typeof(Animation))
        aniComp:Play("nandufanhua_moshui_close")
        RegisterTickOnce(function()
            if not CommonDefs.IsNull(self.gameObject) then
                CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordVertifyWnd)
                aniComp:Stop()
                aniComp:Play("nandufanhua_moshui_backshow")
            end
        end, 200)
    end)
end 

function LuaNanDuFanHuaOverallWnd:ShowSearchButtonAndHideOther(showIndex)
    for i = 1, #self.entranceList do
        self.entranceList[i].transform:Find("SearchButton").gameObject:SetActive(i == showIndex)
        self.entranceList[i].transform:Find("Border").gameObject:SetActive(i == showIndex)
    end
end

function LuaNanDuFanHuaOverallWnd:RefreshUI()
    --local moneyCnt = self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
    --self.MoneyLabel.text = moneyCnt
    local fameName = { "WuYiXiangFameIndex", "ShiLiuLouFameIndex", "ShiLiQingHuaiFameIndex", "ShangShiJieKouFameIndex", "SanFaSiFameIndex"}

    for i = 1, #self.entranceList do
        local unlock, all = LuaYiRenSiMgr:CalculateLordProsperity(fameName[i])
        self.entranceList[i].transform:Find("EntranceButton/Value"):GetComponent(typeof(UILabel)).text = LocalString.GetString("繁荣度 ") .. math.floor(unlock/all*100) .. "%"
        self.entranceList[i].transform:Find("EntranceButton"):GetComponent(typeof(UILabel)).text = LuaYiRenSiMgr.LordFameName[i]
    end
    
    local isLord = self.lordData[LuaYiRenSiMgr.LordPropertyKey.isLordIndex] and self.lordData[LuaYiRenSiMgr.LordPropertyKey.isLordIndex] == 1 or false
    self.UpLevelButton:SetActive(not isLord)
end 
