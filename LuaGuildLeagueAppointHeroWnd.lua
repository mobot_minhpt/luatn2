local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Constants = import "L10.Game.Constants"
local NGUIText = import "NGUIText"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"

LuaGuildLeagueAppointHeroWnd = class()
RegistChildComponent(LuaGuildLeagueAppointHeroWnd,"m_InfoButton", "InfoButton", GameObject)
RegistChildComponent(LuaGuildLeagueAppointHeroWnd,"m_AppointButton", "AppointButton", GameObject)
RegistChildComponent(LuaGuildLeagueAppointHeroWnd,"m_ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaGuildLeagueAppointHeroWnd,"m_Table", "Table", UITable)

function LuaGuildLeagueAppointHeroWnd:Awake()
	self.m_ItemTemplate:SetActive(false)

    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnInfoButtonClick() end)
    UIEventListener.Get(self.m_AppointButton).onClick = DelegateFactory.VoidDelegate(function() self:OnAppointButtonClick() end)
end

function LuaGuildLeagueAppointHeroWnd:Init()
	self:InitTable(LuaGuildLeagueMgr.m_CurrentHeroInfo or {})
	self.m_AppointButton:SetActive(LuaGuildLeagueMgr.m_HasAppointHeroRight or false)
end

function LuaGuildLeagueAppointHeroWnd:InitTable(infoTbl)
	Extensions.RemoveAllChildren(self.m_Table.transform)
	for i=1,5 do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
        child:SetActive(true)
		local portrait = child.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local nameLabel = child.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local levelLabel = child.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
        local noHeroLabel = child.transform:Find("NoHeroLabel"):GetComponent(typeof(UILabel))
        if i<=#infoTbl then
			local info = infoTbl[i]
			portrait:LoadPortrait(CUICommonDef.GetPortraitName(info.class, info.gender, -1), false)
			UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnPlayerClick(info.id, info.name, portrait.gameObject) end)
			nameLabel.text = info.name
			levelLabel.text = SafeStringFormat3(LocalString.GetString("lv.%d"), math.floor(info.level))
			if info.isInXianShenStatus then
				levelLabel.color = NGUIText.ParseColor24(Constants.ColorOfFeiSheng, 0)
			end
			noHeroLabel.gameObject:SetActive(false)
		else
			portrait.gameObject:SetActive(false)
			nameLabel.gameObject:SetActive(false)
			levelLabel.gameObject:SetActive(false)
			noHeroLabel.gameObject:SetActive(true)
		end
	end
    self.m_Table:Reposition()
end

function LuaGuildLeagueAppointHeroWnd:OnInfoButtonClick()
	g_MessageMgr:ShowMessage("Guild_League_Hero_Description") --所有玩家都能看，主要介绍英雄的作用机制
end

function LuaGuildLeagueAppointHeroWnd:OnAppointButtonClick()
	LuaGuildLeagueMgr:RequestOpenGuildLeagueAppointHeroDetailWnd()
end

function LuaGuildLeagueAppointHeroWnd:OnPlayerClick(playerId, playerName, go)
	CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, go.transform.position, AlignType.Default)
end
