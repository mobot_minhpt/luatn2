local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local Texture2D = import "UnityEngine.Texture2D"
local CLoginMgr=import "L10.Game.CLoginMgr"
local ShareMgr = import "ShareMgr"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local Object = import "System.Object"
local MsgPackImpl = import "MsgPackImpl"
local EShareType = import "L10.UI.EShareType"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local QnButton=import "L10.UI.QnButton"
local UIScrollView=import "UIScrollView"
local Main=import "L10.Engine.Main"
local CBaseWnd=import "L10.UI.CBaseWnd"
local File = import "System.IO.File"
local CHTTPForm = import "L10.Game.CHTTPForm"
local ClientHttp = import "L10.Game.ClientHttp"
local HTTPHelper = import "L10.Game.HTTPHelper"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CWebBrowserMgr            = import "L10.Game.CWebBrowserMgr"
local CommonDefs                = import "L10.Game.CommonDefs"
local BoxCollider = import "UnityEngine.BoxCollider"
local GameObject = import "UnityEngine.GameObject"
local NativeTools = import "L10.Engine.NativeTools"
local EChatPanel = import "L10.Game.EChatPanel"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local PicData = import "L10.Game.PicData"
local CChatHelper = import "L10.UI.CChatHelper"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"

CLuaShareBox = class()
CLuaShareBox.Path = "ui/share/LuaShareBox"
RegistClassMember(CLuaShareBox,"m_Grid")
RegistClassMember(CLuaShareBox,"m_TipLabel")
RegistClassMember(CLuaShareBox,"m_CloseButton")
RegistClassMember(CLuaShareBox,"m_PersonalSpaceButton")
RegistClassMember(CLuaShareBox,"m_WeixinButton")
RegistClassMember(CLuaShareBox,"m_WeiboButton")
RegistClassMember(CLuaShareBox,"m_WeixinPengyouQuanButton")
RegistClassMember(CLuaShareBox,"m_YixinButton")
RegistClassMember(CLuaShareBox,"m_YixinPengyouQuanButton")
RegistClassMember(CLuaShareBox,"m_GodlikeButton")
RegistClassMember(CLuaShareBox,"m_GodlikePengyouQuanButton")
RegistClassMember(CLuaShareBox,"m_FaceBookButton")
RegistClassMember(CLuaShareBox,"m_KakaoButton")
RegistClassMember(CLuaShareBox,"m_Texture")
RegistClassMember(CLuaShareBox, "m_ScenePhotoButton")

-- RegistClassMember(CLuaShareBox,"m_UploadToHouseCompetitionBtn")
-- RegistClassMember(CLuaShareBox,"m_UploadToHouseCompetitionLabel")

RegistChildComponent(CLuaShareBox,"m_UploadToHouseCompetitionLabel",UILabel)
RegistChildComponent(CLuaShareBox,"m_UploadToHouseCompetitionBtn",GameObject)
RegistChildComponent(CLuaShareBox,"UploadToQixiBtn",GameObject)

function CLuaShareBox:Awake()
    self.m_Grid = FindChild(self.transform,"Grid"):GetComponent(typeof(UIGrid))
    self.m_TipLabel = FindChild(self.transform,"TipLabel"):GetComponent(typeof(UILabel))
    self.m_CloseButton = FindChild(self.transform,"CloseButton").gameObject
    local tf = self.transform:Find("Anchor/Container/Scroll View/Grid/PersonalSpaceButton")
    if tf then
        self.m_PersonalSpaceButton = tf:GetComponent(typeof(QnButton))
    end
    self.m_WeixinButton = FindChild(self.transform,"Wexin"):GetComponent(typeof(QnButton))
    self.m_WeiboButton = FindChild(self.transform,"WeiBo"):GetComponent(typeof(QnButton))
    self.m_WeixinPengyouQuanButton = FindChild(self.transform,"WeixinPengyouQuan"):GetComponent(typeof(QnButton))
    self.m_YixinButton = FindChild(self.transform,"YiXin"):GetComponent(typeof(QnButton))
    self.m_YixinPengyouQuanButton = FindChild(self.transform,"YiXinPengyouQuan"):GetComponent(typeof(QnButton))

    self.m_YixinButton.gameObject:SetActive(false)
    self.m_YixinPengyouQuanButton.gameObject:SetActive(false)

    self.m_GodlikeButton = FindChild(self.transform,"GodLike"):GetComponent(typeof(QnButton))
    self.m_GodlikePengyouQuanButton = FindChild(self.transform,"GodLikePengYouQUan"):GetComponent(typeof(QnButton))
    self.m_FaceBookButton = FindChild(self.transform,"Facebook"):GetComponent(typeof(QnButton))
    self.m_KakaoButton = FindChild(self.transform,"Kakao"):GetComponent(typeof(QnButton))

    -- 风景相册
    local photoBtnTrans = FindChild(self.transform,"ScenePhotoButton")
    if photoBtnTrans then
      self.m_ScenePhotoButton = photoBtnTrans:GetComponent(typeof(QnButton))
      self.m_ScenePhotoButton.gameObject:SetActive(CLuaScreenCaptureWnd.bInScenePhoto and CLuaScreenCaptureWnd.ScenePhotoId > 0)
      self.m_ScenePhotoButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickScenePhotoButton(btn) end)
    end

    local tf= self.transform:Find("Anchor/Sprite/Texture")
    if tf then
        self.m_Texture = tf:GetComponent(typeof(UITexture))
    else
        tf = self.transform:Find("Rot/Texture")
        if tf then
            self.m_Texture = tf:GetComponent(typeof(UITexture))
        end
    end

    self.m_WeixinButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickWeixinButton(btn) end)
    self.m_WeiboButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickWeiboButton(btn) end)
    self.m_WeixinPengyouQuanButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickWeixinPengyouQuanButton(btn) end)
    self.m_GodlikeButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickGodlikeButton(btn) end)
    self.m_GodlikePengyouQuanButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickGodlikePengyouQuanButton(btn) end)
    if CommonDefs.IS_KR_CLIENT then
        self.m_FaceBookButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickFacebook(btn) end)
        self.m_KakaoButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickKakao(btn) end)
    end
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function(g) self:OnClickCloseButton(g) end)
    if self.m_PersonalSpaceButton then
        self.m_PersonalSpaceButton.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickPersonalSpaceButton(btn) end)
    end
    self:InitChannels()
    self:InitAntiAlasing()
end




function CLuaShareBox:OnMoBaiBangShare(token)
    LuaShareMoBaiBangMgr.token = token
end


function CLuaShareBox:InitChannels()
    local guildChannel = FindChild(self.transform,"GuildChannel")
    if guildChannel then
        local btn = guildChannel:GetComponent(typeof(QnButton))
        btn.gameObject:SetActive(ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage or ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage2)
        btn.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickChannelBtn(btn, EChatPanel.Guild) end)
    end
    local teamChannel = FindChild(self.transform,"TeamChannel")
    if teamChannel then
        local btn = teamChannel:GetComponent(typeof(QnButton))
        btn.gameObject:SetActive((ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage or ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage2) and CTeamMgr.Inst:TeamExists())
        btn.OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnClickChannelBtn(btn, EChatPanel.Team) end)
    end
    if ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage or ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage2 then
        self.m_WeixinButton.gameObject:SetActive(false)
        self.m_WeixinPengyouQuanButton.gameObject:SetActive(false)
        self.m_WeiboButton.gameObject:SetActive(false)
        self.m_GodlikeButton.gameObject:SetActive(false)
        self.m_GodlikePengyouQuanButton.gameObject:SetActive(false)
        self.m_FaceBookButton.gameObject:SetActive(false)
        self.m_KakaoButton.gameObject:SetActive(false)
    end
end

function CLuaShareBox:OnClickChannelBtn(btn, channel)
    local data = File.ReadAllBytes(ShareBoxMgr.ImagePath)
    LoadPicMgr.UploadPic(CPersonalSpaceMgr.Upload_PicType, data, DelegateFactory.Action_string(function (url) 
        LoadPicMgr.SaveUploadPic(CPersonalSpaceMgr.Upload_PicType, url, LoadPicMgr.NORSuffix, DelegateFactory.Action_CPersonalSpace_BaseRet(function (ret) 
            if ret.code == 0 then
                local pdata = CreateFromClass(PicData)
                pdata.small = false
                pdata.width = 256
                pdata.height = 256
                local message = LoadPicMgr.GetPicLink(ret.msg, pdata)
                CChatHelper.SendMsg(channel , message, 0)
                CSocialWndMgr.ShowChatWnd(channel)
             end
        end), nil)
    end))
end

function CLuaShareBox:SetButtonEnable(btn,active)
    if not btn then return end
    btn.gameObject:SetActive(active)

end
function CLuaShareBox:OnSendHouseCompetitionData()
    if not self.m_UploadToHouseCompetitionBtn then return end

    CUICommonDef.SetActive(self.m_UploadToHouseCompetitionBtn, (CLuaHouseCompetitionMgr.sMaxUploadTimes - CHouseCompetitionMgr.Inst:GetUploadTimes()) > 0)
    self.m_UploadToHouseCompetitionLabel.gameObject:SetActive(true)
end

function CLuaShareBox:Init()
    self.m_TipLabel.text = g_MessageMgr:FormatMessage("FenXiang_WeiBo_One")
    if CommonDefs.IS_KR_CLIENT then--韩服只有kaokao和facebook
        self:SetButtonEnable(self.m_PersonalSpaceButton,false)
        self.m_WeixinButton.gameObject:SetActive(false)
        self.m_WeixinPengyouQuanButton.gameObject:SetActive(false)
        self.m_WeiboButton.gameObject:SetActive(false)
        self.m_GodlikeButton.gameObject:SetActive(false)
        self.m_GodlikePengyouQuanButton.gameObject:SetActive(false)

        if Application.platform == RuntimePlatform.Android then
            --韩服android端只有GP包需要facebook(其实韩服只有GP包)
            self.m_FaceBookButton.gameObject:SetActive(SdkU3d.getAppChannel():Contains("google"))
        else
            --韩服ios 需要facebook
            self.m_FaceBookButton.gameObject:SetActive(true)
        end

        self.m_KakaoButton.gameObject:SetActive(true)
    elseif CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then--hmt有梦岛，其他都没有
        self:SetButtonEnable(self.m_PersonalSpaceButton,ShareBoxMgr.ShareType ~= EShareType.ShareChatPanelImage)
        self.m_WeixinButton.gameObject:SetActive(false)
        self.m_WeixinPengyouQuanButton.gameObject:SetActive(false)
        self.m_WeiboButton.gameObject:SetActive(false)
        self.m_GodlikeButton.gameObject:SetActive(false)
        self.m_GodlikePengyouQuanButton.gameObject:SetActive(false)
        self.m_FaceBookButton.gameObject:SetActive(false)
        self.m_KakaoButton.gameObject:SetActive(false)
    else--国服
        if Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor or NativeTools.IsAllChannelApk() then
            self:SetButtonEnable(self.m_PersonalSpaceButton,ShareBoxMgr.ShareType ~= EShareType.ShareChatPanelImage)
            self.m_WeixinButton.gameObject:SetActive(false)
            self.m_WeixinPengyouQuanButton.gameObject:SetActive(false)
            self.m_WeiboButton.gameObject:SetActive(false)
            self.m_GodlikeButton.gameObject:SetActive(false)
            self.m_GodlikePengyouQuanButton.gameObject:SetActive(false)
            self.m_FaceBookButton.gameObject:SetActive(false)
            self.m_KakaoButton.gameObject:SetActive(false)
        else
            self:SetButtonEnable(self.m_PersonalSpaceButton,true);
            --安智、内涵段子、今日头条、金立 37wan、华为
            if CLuaShareMgr.IsOpenShare() then
                self.m_WeixinButton.Enabled = ShareMgr.IsPlatformInstalled(ShareMgr.Platform_Weixin)
                self.m_WeixinPengyouQuanButton.Enabled = ShareMgr.IsPlatformInstalled(ShareMgr.Platform_Weixin)
                self.m_WeiboButton.Enabled = ShareMgr.IsPlatformInstalled(ShareMgr.Platform_Weibo)
                self.m_GodlikeButton.Enabled = ShareMgr.IsPlatformInstalled(ShareMgr.Platform_GodLike)
                self.m_GodlikePengyouQuanButton.Enabled = ShareMgr.IsPlatformInstalled(ShareMgr.Platform_GodLike)
                self.m_GodlikeButton.gameObject:SetActive(not CLuaShareMgr.m_IsHuaWei and ShareBoxMgr.ShareType ~= EShareType.ShareChatPanelImage and ShareBoxMgr.ShareType ~= EShareType.ShareChatPanelImage2)
                self.m_GodlikePengyouQuanButton.gameObject:SetActive(not CLuaShareMgr.m_IsHuaWei and ShareBoxMgr.ShareType ~= EShareType.ShareChatPanelImage and ShareBoxMgr.ShareType ~= EShareType.ShareChatPanelImage2)
            else
                self.m_WeixinButton.gameObject:SetActive(false)
                self.m_WeixinPengyouQuanButton.gameObject:SetActive(false)
                self.m_WeiboButton.gameObject:SetActive(false)
                self.m_GodlikeButton.gameObject:SetActive(false)
                self.m_GodlikePengyouQuanButton.gameObject:SetActive(false)
            end
            self.m_FaceBookButton.gameObject:SetActive(false)
            self.m_KakaoButton.gameObject:SetActive(false)
        end
        if Application.platform == RuntimePlatform.Android then
            self.m_TipLabel.text = g_MessageMgr:FormatMessage("FenXiang_WeiBo_Two")
            self.m_WeiboButton.gameObject:SetActive(false)
            --  //低于该版本时，网易大神不可用
            if (Main.Inst.EngineVersion < 352490) then
                self.m_GodlikeButton.gameObject:SetActive(false)
                self.m_GodlikePengyouQuanButton.gameObject:SetActive(false)
            end
        end
    end

    if CommonDefs.IS_KR_CLIENT then
        self.m_TipLabel.gameObject:SetActive(not (self.m_KakaoButton.gameObject.activeSelf or self.m_FaceBookButton.gameObject.activeSelf))
	elseif CommonDefs.IS_VN_CLIENT then
		self.m_TipLabel.gameObject:SetActive(false)
    else
        self.m_TipLabel.gameObject:SetActive(not (self.m_WeixinButton.gameObject.activeSelf
                                              or self.m_WeixinPengyouQuanButton.gameObject.activeSelf
                                              or self.m_WeixinButton.gameObject.activeSelf
                                              or self.m_GodlikeButton.gameObject.activeSelf
                                              or self.m_GodlikePengyouQuanButton.gameObject.activeSelf
                                              or ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage
                                              or ShareBoxMgr.ShareType == EShareType.ShareChatPanelImage2
                                              or (self.m_PersonalSpaceButton and self.m_PersonalSpaceButton.gameObject.activeSelf)))
    end

    Gac2Gas.QueryPlayerGuildInfo(1)

    if self.m_Texture and ShareBoxMgr.ImagePath and ShareBoxMgr.ImagePath~="" and File.Exists(ShareBoxMgr.ImagePath) then
        local tex = nil
        if (ShareBoxMgr.ShareType == EShareType.CJBShareImage) then
            tex = Texture2D(512, 512)
            self.m_Texture.width = 290
            self.m_Texture.height = 290
        else
            tex = Texture2D(1024, 576)
        end
        local data = File.ReadAllBytes(ShareBoxMgr.ImagePath)
        CommonDefs.LoadImage(tex, data)
        self.m_Texture.mainTexture = tex

        if ShareBoxMgr.ShareType == EShareType.ShareTranspareImage and tex then
            self.transform:Find("Anchor/Sprite"):GetComponent(typeof(UISprite)).enabled = false
            self.m_Texture.height = 800
            local width = 800 / tex.height * tex.width
            if width > 2000 then
                width = 2000
            end
            self.m_Texture.width = width
            self.m_Texture:SetAnchor(nil, 0, 0, 0, 0)

            local closeBtn = self.transform:Find("CloseButton").gameObject
            if closeBtn then
                closeBtn:SetActive(true)
                closeBtn.transform.localPosition = Vector3(width/2, 460,0)
            end
            
            local boxCollider = self.m_Texture.transform:GetComponent(typeof(BoxCollider))
            if boxCollider then
                boxCollider.enabled = true
            end

            local event = self.m_Texture.transform:GetComponent(typeof(UIEventListener))
            if event then
                -- 重新设置点击事件 以免报错
                event.onClick = DelegateFactory.VoidDelegate(function()
                    
                end)
            end
        end
    end

    -- //老版本渠道包逻辑处理
    if not CommonDefs.Is_PC_PLATFORM() then
        local isNetease = CLoginMgr.Inst:IsNetEaseOfficialLogin()
        if (not isNetease and Main.Inst.EngineVersion < 293680) then
            self.m_TipLabel.text = g_MessageMgr:FormatMessage("CHANNEL_SHARE_OLD_VERSION_TIP")
            self.m_TipLabel.gameObject:SetActive(true)
            self.m_Grid.gameObject:SetActive(false)
        end
    end

    if self.m_UploadToHouseCompetitionBtn then
        self.m_UploadToHouseCompetitionBtn:SetActive(false)
    end

    if self.m_UploadToHouseCompetitionLabel then
        self.m_UploadToHouseCompetitionLabel.gameObject:SetActive(false)
    end

    if CLuaShareMgr.m_ShareHouseCompetition then
        self:CheckShowHouseCompetition()
    end

    if self.UploadToQixiBtn then
        self.UploadToQixiBtn:SetActive(false)
    end
    if self.UploadToQixiBtn and not CLuaShareMgr.m_ShareHouseCompetition then
        self.UploadToQixiBtn:SetActive(false)
        local isqixi = CLuaShareMgr.IsQixiShareModuleEnable()

        if isqixi then
            CLuaShareMgr.RequireQixiData()
        end
    end

    self.m_Grid:Reposition()
    local scrollView = self.m_Grid.transform.parent:GetComponent(typeof(UIScrollView))
    if scrollView then
        scrollView:ResetPosition()
    end
    if ShareBoxMgr.ShareType == EShareType.MoBaiBang then
        Gac2Gas.QuerySmartGMTokenRaw()
    end
    

end

--@region 七夕活动

--[[
    @desc: 获得七夕数据的回调
    author:CodeGize
    time:2019-06-24 15:50:46
    @return:
]]
function CLuaShareBox:OnGetQixiData()
    if CLuaShareMgr.QixiData.State==-1 then --服务器未开启
        self.UploadToQixiBtn:SetActive(false)
        self.m_UploadToHouseCompetitionLabel.gameObject:SetActive(false)
    else
        self.UploadToQixiBtn:SetActive(true)
        local UploadToQixiBtnClick = function(go)
            self:OnUploadToQixiBtnClick()
        end
        CommonDefs.AddOnClickListener(self.UploadToQixiBtn,DelegateFactory.Action_GameObject(UploadToQixiBtnClick), false)

        self.m_UploadToHouseCompetitionLabel.gameObject:SetActive(true)
        local str = ""
        if CLuaShareMgr.QixiData.State == 0 then
            str = LocalString.GetString("星光印记——七夕截图大赛开启啦，请点击参与截图大赛前往报名")
        else
            str = SafeStringFormat3(LocalString.GetString("上传您的截图参与星光印记，今日剩余可上传[c][FF9A00FF]%d/%d[-][/c]张"),
            CLuaShareMgr.QixiData.Count,CLuaShareMgr.QixiUploadCountMax)
        end
        self.m_UploadToHouseCompetitionLabel.text = str
    end
    self.m_Grid:Reposition()
end

--[[
    @desc: 点击七夕按钮事件
    author:CodeGize
    time:2019-06-24 15:59:05
    @return:
]]
function CLuaShareBox:OnUploadToQixiBtnClick()
    if CLuaShareMgr.QixiData == nil or CLuaShareMgr.QixiData.State == 0 then
        CWebBrowserMgr.Inst:OpenActivityUrl("qnm2019QXJT")
        return
    end

    local tex = self.m_Texture.mainTexture
    if tex ~= nil then
        local data = CommonDefs.EncodeToJPG(tex)

        local onFinished = DelegateFactory.Action_string(function (url)

            if url ==nil then
                url = ""
            end
            CLuaShareMgr.UpLoadQixiCompleted(url)
        end)
        CPersonalSpaceMgr.Inst:UploadPic("screenshot", data, onFinished)
    end
    -- self:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

--@endregion

--检查一下家园设计大赛
function CLuaShareBox:CheckShowHouseCompetition()
    if not self.m_UploadToHouseCompetitionBtn then return end

    UIEventListener.Get(self.m_UploadToHouseCompetitionBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickUploadToHouseCompetitionBtn(go)
    end)
    CLuaHouseCompetitionMgr.CheckMirrorServer()
    local bInOwnHouseAndHouseCompetition=CLuaHouseCompetitionMgr.CanShareHouseCompetition()
    self.m_UploadToHouseCompetitionBtn:SetActive(bInOwnHouseAndHouseCompetition)
    self.m_UploadToHouseCompetitionLabel.text = LocalString.GetString("可以将截图上传至家园设计大赛，至多保存最近的20张截图。")

    CUICommonDef.SetActive(self.m_UploadToHouseCompetitionBtn, (CLuaHouseCompetitionMgr.sMaxUploadTimes - CHouseCompetitionMgr.Inst:GetUploadTimes()) > 0,true)

    self.m_UploadToHouseCompetitionLabel.gameObject:SetActive(bInOwnHouseAndHouseCompetition)

    if not CHouseCompetitionMgr.Inst.RegisteredChusai then
        Gac2Gas.QueryHasRegisteredChusai()
    end
end

function CLuaShareBox:OnEnable()
    g_ScriptEvent:AddListener("OnMoBaiBangShare", self, "OnMoBaiBangShare")
	g_ScriptEvent:AddListener("QueryPlayerGuildInfoDone", self, "OnQueryPlayerGuildInfoDone")
    g_ScriptEvent:AddListener("OnSendHouseCompetitionData",self,"OnSendHouseCompetitionData")
    g_ScriptEvent:AddListener("OnQixiDataRecived",self,"OnGetQixiData")
end
function CLuaShareBox:OnDisable()
    g_ScriptEvent:RemoveListener("OnMoBaiBangShare", self, "OnMoBaiBangShare")
	g_ScriptEvent:RemoveListener("QueryPlayerGuildInfoDone", self, "OnQueryPlayerGuildInfoDone")
    g_ScriptEvent:RemoveListener("OnSendHouseCompetitionData",self,"OnSendHouseCompetitionData")
    g_ScriptEvent:RemoveListener("OnQixiDataRecived",self,"OnGetQixiData")
end

function CLuaShareBox:OnDestroy()
    if self.m_Texture and self.m_Texture.mainTexture then
        UnityEngine.Object.Destroy(self.m_Texture.mainTexture)
    end
    --reset
    CLuaShareMgr.m_ShareHouseCompetition = false
end

function CLuaShareBox:OnClickUploadToHouseCompetitionBtn(go)
    local tex = self.m_Texture.mainTexture
    if tex ~= nil then
        if tex.width<=10 or tex.height<=10 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING",LocalString.GetString("上传图片失败"))
        else
            local data = CommonDefs.EncodeToJPG(tex)
            local onFinished = DelegateFactory.Action_string(function (url)
                CHouseCompetitionMgr.Inst:SetUploadTimes(CHouseCompetitionMgr.Inst:GetUploadTimes() + 1)
                Gac2Gas.UpdateHouseCompetionChusaiUploadTimes(CHouseCompetitionMgr.Inst:GetUploadTimes(), url)
            end)
            CPersonalSpaceMgr.Inst:UploadPic(CPersonalSpaceMgr.Upload_HouseCompetitionPicType, data, onFinished)
        end
    end
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
function CLuaShareBox:OnQueryPlayerGuildInfoDone( args )
    local reason, guildInfo=args[0],args[1]
    if guildInfo ~= nil and not System.String.IsNullOrEmpty(guildInfo.Name) then
        ShareMgr.GuildName = guildInfo.Name
    else
        ShareMgr.GuildName = LocalString.GetString("无")
    end
end
function CLuaShareBox:OnClickCloseButton( obj)
    if ShareBoxMgr.ShareType == EShareType.TicketShare then
        CQingQiuMgr.Inst:UseTicketItem()
    end
    -- self:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
function CLuaShareBox:OnClickPersonalSpaceButton( button)
    if not CPersonalSpaceMgr.Inst:OpenPersonalSpaceCheck(false) then
        g_MessageMgr:ShowMessage("PERSONAL_SPACE_NOT_OPEN")
        return
    end

    ShareMgr.ShareLocalImage2PersonalSpace(ShareBoxMgr.ImagePath, DelegateFactory.Action(function ()
        if CClientMainPlayer.Inst and LuaVoiceAchievementMgr.s_IsOpen then
            local playerId = CClientMainPlayer.Inst.Id
            --Gac2Gas.OnSendMengDao(playerId,ShareBoxMgr.ImagePath)
        end
        g_ScriptEvent:BroadcastInLua("PersonalSpaceShareFinished")
        CUIManager.CloseUI(CUIResources.Share2PersonalSpaceWnd)
        g_MessageMgr:ShowMessage("SHARE_SUCCESS")
        local imgaeName = ShareBoxMgr.ImagePath
        if string.find(imgaeName,"ChristmasWindowShoot") then
            Gac2Gas.ReceiveChristmasZhuangBanAward()
        end
		if CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) then
			Gac2Gas.ScreenCapturePersonalSpaceShareFinished()
		end
    end))
end

function CLuaShareBox:OnClickWeixinButton(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_WEIXIN_FRIEND)
end
function CLuaShareBox:OnClickWeiboButton(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_WEIBO)
end
function CLuaShareBox:OnClickWeixinPengyouQuanButton(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_WEIXIN_TIMELINE)
end
function CLuaShareBox:OnClickYixinButton(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_YIXIN_FRIEND)
end
function CLuaShareBox:OnClickYixinPengyouQuanButton(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_YIXIN_TIMELINE)
end

function CLuaShareBox:OnClickFacebook(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_FACEBOOK)
end
function CLuaShareBox:OnClickKakao(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_KAKAO)
end

function CLuaShareBox:OnClickGodlikeButton(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_GODLIKE_FRIEND)
end
function CLuaShareBox:OnClickGodlikePengyouQuanButton(button)
    self:DoShare(ShareMgr.NT_SHARE_TYPE_GODLIKE_TIMELINE)
end

function CLuaShareBox:OnClickScenePhotoButton(button)
  local data = File.ReadAllBytes(ShareBoxMgr.ImagePath)
  CPersonalSpaceMgr.Inst:UploadPic(CPersonalSpaceMgr.Upload_HouseSharePicType, data, DelegateFactory.Action_string(function (url)
    if CLuaScreenCaptureWnd.bInScenePhoto then
      Gac2Gas.SaveSceneryPhotoAlbumUrl(CLuaScreenCaptureWnd.ScenePhotoId, url)
      g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("该照片已保存至风景相册"))
    end
  end))
end

function CLuaShareBox:DoShare(channel)
    if ShareBoxMgr.ShareType == EShareType.Invite then
        ShareMgr.ShareInviteUrl(channel)
    elseif ShareBoxMgr.ShareType == EShareType.Trade then
        ShareMgr.ShareTradeUrl(channel, ShareBoxMgr.BuyerName, ShareBoxMgr.ItemName, ShareBoxMgr.Price)
    elseif ShareBoxMgr.ShareType == EShareType.Yangmao then
        ShareMgr.ShareYanmaoUrl(channel)
    elseif ShareBoxMgr.ShareType == EShareType.Marry then
        ShareMgr.ShareMarryUrl(channel, ShareBoxMgr.marryOtherId, ShareBoxMgr.marryOtherName, ShareBoxMgr.marryOtherClass, ShareBoxMgr.marryOtherGender)
    elseif ShareBoxMgr.ShareType == EShareType.MonthZhangDan then
        ShareMgr.ShareMonthZhangDan(channel)
    elseif ShareBoxMgr.ShareType == EShareType.MoBaiBang then
        LuaShareMoBaiBangMgr.Share(channel,LuaShareMoBaiBangMgr.token)
    elseif ShareBoxMgr.ShareType == EShareType.ShareImage then
        ShareMgr.ShareImage(channel, ShareBoxMgr.ImagePath)
    elseif ShareBoxMgr.ShareType == EShareType.CJBShareImage then
        ShareMgr.ShareCJBUrl(channel, ShareBoxMgr.ImagePath)
    elseif ShareBoxMgr.ShareType == EShareType.ChunjieGift then
        ShareMgr.ShareChunjieGift(channel, ShareBoxMgr.ChunjieGiftID);
    elseif ShareBoxMgr.ShareType == EShareType.HuiLiu then
        ShareMgr.ShareHuiLiuUrl(channel)
    elseif ShareBoxMgr.ShareType == EShareType.QingQiuQiYu then
        ShareMgr.ShareQingQiuQiYuUrl(channel, ShareBoxMgr.s_QingQiuQiYuRank);
    elseif ShareBoxMgr.ShareType == EShareType.AnniversaryPic then
        ShareMgr.ShareImage(channel, ShareBoxMgr.ImagePath)
    elseif ShareBoxMgr.ShareType == EShareType.AnniversaryUrl then
        ShareMgr.ShareAnniversaryUrl(channel)
    elseif ShareBoxMgr.ShareType == EShareType.JiaoNiLiao then
        ShareMgr.ShareJiaoNiLiaoUrl(channel)
    elseif ShareBoxMgr.ShareType == EShareType.HousePicShare then
        ShareMgr.ShareHousePicUrl(channel)
    elseif ShareBoxMgr.ShareType == EShareType.TicketShare then
        ShareMgr.ShareImage(channel, ShareBoxMgr.ImagePath)
    elseif ShareBoxMgr.ShareType == EShareType.ZhuanzhiUrl then
        ShareMgr.ShareImage(channel, ShareBoxMgr.ImagePath)
    elseif ShareBoxMgr.ShareType == EShareType.ZhuShouShare then
        ShareMgr.ShareZhuShou(channel)
    elseif ShareBoxMgr.ShareType == EShareType.ChunJieShare then
        ShareMgr.ShareChunJie(channel)
    elseif ShareBoxMgr.ShareType == EShareType.PreciousItemShare then
        ShareMgr.ShareImage(channel, ShareBoxMgr.ImagePath)
    elseif ShareBoxMgr.ShareType == EShareType.EquipBaptizeShare then
        ShareMgr.ShareImage(channel, ShareBoxMgr.ImagePath)
    elseif ShareBoxMgr.ShareType == EShareType.JiXiangWuHatchShare then
        ShareMgr.ShareJiXiangWuHatchUrl(channel, ShareBoxMgr.JiXiangWuShareItemId)
    elseif ShareBoxMgr.ShareType == EShareType.CommonUrl then
        ShareMgr.ShareCommonUrl(channel, ShareBoxMgr.CommonShareUrl, ShareBoxMgr.CommonShareTitle, ShareBoxMgr.CommonShareText, ShareBoxMgr.CommonShareThumb)
    elseif ShareBoxMgr.ShareType == EShareType.ShareTranspareImage then
        ShareMgr.ShareImage(channel, ShareBoxMgr.ImagePath)
    end
    -- //打开分享页面，记个log
    self:LogToServer(ShareBoxMgr.ShareType, channel)
end


function CLuaShareBox:LogToServer( type,channel)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
    CommonDefs.DictAdd(dict, typeof(String), "share_point", typeof(Object), tostring((EnumToInt(type) + 1)))
    CommonDefs.DictAdd(dict, typeof(String), "click", typeof(Object), "1")
    CommonDefs.DictAdd(dict, typeof(String), "share_channel", typeof(Object), self:getShareChannelName(channel))
    Gac2Gas.RequestRecordClientLog("NgShare", MsgPackImpl.pack(dict))
end
function CLuaShareBox:getShareChannelName( channel )
    if channel == ShareMgr.NT_SHARE_TYPE_WEIBO then
        return "weibo"
    end
    if channel == ShareMgr.NT_SHARE_TYPE_WEIXIN_FRIEND then
        return "weixin"
    end
    if channel == ShareMgr.NT_SHARE_TYPE_WEIXIN_TIMELINE then
        return "weixinfriend"
    end
    if channel == ShareMgr.NT_SHARE_TYPE_YIXIN_FRIEND then
        return "yixin"
    end
    if channel == ShareMgr.NT_SHARE_TYPE_YIXIN_TIMELINE then
        return "yixinfriend"
    end
    return "unknown"
end

-- Anti-Alasing
RegistClassMember(CLuaShareBox, "m_OriginalImagePath")
RegistClassMember(CLuaShareBox, "m_AntiAlasingImagePath")
RegistClassMember(CLuaShareBox, "m_IndicatorObj")
RegistClassMember(CLuaShareBox, "m_bInAntiAlasingMode")
RegistClassMember(CLuaShareBox, "m_bInRequest")
CLuaShareBox.AntiAlasingUrl = "http://dl.fuxi.netease.com:39595/anti_aliasing"
CLuaShareBox.EnableAntiAlasing = false
if CommonDefs.IS_CN_CLIENT then
    CLuaShareBox.EnableAntiAlasing = true
end

function CLuaShareBox:InitAntiAlasing()
    self.m_bInAntiAlasingMode = false
    self.m_bInRequest = false
    local antialasingTrans = self.transform:Find("Anchor/AntiAlasingSetting")
    if not antialasingTrans then return end
    local isInScreencapture = CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd)
    antialasingTrans.gameObject:SetActive(CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) and ShareBoxMgr.ShareType == EShareType.ShareImage and CLuaShareBox.EnableAntiAlasing)
    if not antialasingTrans.gameObject.activeSelf then return end
    self.m_IndicatorObj = antialasingTrans:Find("Indicator").gameObject
    self.m_IndicatorObj:SetActive(false)
    self.m_OriginalImagePath = ShareBoxMgr.ImagePath
    local checkbox = antialasingTrans:Find("QnCheckbox"):GetComponent(typeof(QnCheckBox))
    checkbox:SetSelected(false, true)
    checkbox.OnValueChanged = DelegateFactory.Action_bool(function(v)
        self:ChangeTexture(v)
    end)
end

function CLuaShareBox:ChangeTexture(bAntiAlasing)
    self.m_bInAntiAlasingMode = bAntiAlasing
    local imagePath = self.m_OriginalImagePath
    if bAntiAlasing then
        if not self.m_AntiAlasingImagePath then
            self.m_IndicatorObj:SetActive(true)
            self.m_Texture.mainTexture = nil
            if self.m_bInRequest then return end
            local form = CHTTPForm()
            form:AddBinaryData("img", File.ReadAllBytes(self.m_OriginalImagePath), nil)
            local request = ClientHttp(true, CLuaShareBox.AntiAlasingUrl, form, nil)
            HTTPHelper.GetResponse(request, DelegateFactory.Action_bool_string(function(ret, filePath)
                if ret and self.m_IndicatorObj then
                    self.m_IndicatorObj:SetActive(false)
                    self.m_AntiAlasingImagePath = filePath
                    if self.m_bInAntiAlasingMode then
                        self:LoadImage(filePath)
                    end
                end
            end), true)
            return
        else
            imagePath = self.m_AntiAlasingImagePath
        end
    else
        self.m_IndicatorObj:SetActive(false)
    end
    self:LoadImage(imagePath)
end

function CLuaShareBox:LoadImage(imagePath)
    if self.m_Texture and imagePath and File.Exists(imagePath) then
        local tex = Texture2D(1024, 576)
        local data = File.ReadAllBytes(imagePath)
        CommonDefs.LoadImage(tex, data)
        self.m_Texture.mainTexture = tex
        ShareBoxMgr.ImagePath = imagePath
    end
end
-- Anti-Alasing End
