local CRenderScene=import "L10.Engine.Scene.CRenderScene"
local UITexture = import "UITexture"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local EShadowType = import "L10.Engine.EShadowType"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local Screen = import "UnityEngine.Screen"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"
local RenderTexture = import "UnityEngine.RenderTexture"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local CSharpResourceLoader = import "L10.Game.CSharpResourceLoader"
local Quaternion = import "UnityEngine.Quaternion"
local CWorldPositionFX = import "L10.Game.CWorldPositionFX"
local CFX = import "L10.Game.CEffectMgr+CFX"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
local EPostCameraType = import "L10.Engine.PostProcessing.EPostCameraType"
local CWater = import "L10.Engine.CWater"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local Setting = import "L10.Engine.Setting"
local Shader = import "UnityEngine.Shader"
local ShaderLodParams = import "L10.Game.ShaderLodParams"
local DepthTextureMode = import "UnityEngine.DepthTextureMode"
local CCharacterRenderMgr = import "L10.Engine.CCharacterRenderMgr"
local CPinchFaceHubMgr = import "L10.Game.CPinchFaceHubMgr"
local StreamingController = import "UnityEngine.StreamingController"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CMainCamera = import "L10.Engine.CMainCamera"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local Vector4 = import "UnityEngine.Vector4"
local EnumPostEffectType = import "L10.Engine.PostProcessing.EnumPostEffectType"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CLoginMgr = import "L10.Game.CLoginMgr"
local Rect = import "UnityEngine.Rect"
local CFacialMgr = import "L10.Game.CFacialMgr"
local CPinchFaceWork = import "L10.Game.CPinchFaceHubMgr+CPinchFaceWork"

LuaPinchFaceWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceWnd, "PinchFacePreselectionView", "PinchFacePreselectionView", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "ShowMainViewButtonPos", "ShowMainViewButtonPos", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "ShowPreviewViewBtnPos", "ShowPreviewViewBtnPos", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "ShowDisplayViewBtnPos", "ShowDisplayViewBtnPos", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "BackServerWndButton", "BackServerWndButton", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "PinchFaceMainView", "PinchFaceMainView", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "PinchFacePreviewView", "PinchFacePreviewView", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "BackMainViewButton", "BackMainViewButton", CButton)
RegistChildComponent(LuaPinchFaceWnd, "PinchFaceDisplayView", "PinchFaceDisplayView", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "BackPreviewViewButton", "BackPreviewViewButton", CButton)
RegistChildComponent(LuaPinchFaceWnd, "BackPreselectionViewButton", "BackPreselectionViewButton", CButton)
RegistChildComponent(LuaPinchFaceWnd, "MainPreviewTexture", "MainPreviewTexture", UITexture)
RegistChildComponent(LuaPinchFaceWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "FxRoot", "FxRoot", GameObject)
RegistChildComponent(LuaPinchFaceWnd, "BtnClickFx", "BtnClickFx", CUIFx)
RegistChildComponent(LuaPinchFaceWnd, "BlurTex", "BlurTex", UITexture)
RegistChildComponent(LuaPinchFaceWnd, "RainbowFx", "RainbowFx", CUIFx)
--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFaceWnd, "m_ModelRoot")
RegistClassMember(LuaPinchFaceWnd, "m_EnvironmentFx")
RegistClassMember(LuaPinchFaceWnd, "m_BgColorFx")
RegistClassMember(LuaPinchFaceWnd, "m_SceneRoot")
RegistClassMember(LuaPinchFaceWnd, "m_SwipeGestureListener")
RegistClassMember(LuaPinchFaceWnd, "m_Target")
RegistClassMember(LuaPinchFaceWnd, "m_TextureName")
RegistClassMember(LuaPinchFaceWnd, "m_TextureLoader")
RegistClassMember(LuaPinchFaceWnd, "m_Camera")
RegistClassMember(LuaPinchFaceWnd, "m_UpdateCameraRectTick")
RegistClassMember(LuaPinchFaceWnd, "m_EffectController")
RegistClassMember(LuaPinchFaceWnd, "m_CCinemachineCtrlMgr")
RegistClassMember(LuaPinchFaceWnd, "m_IKTarget")
RegistClassMember(LuaPinchFaceWnd, "m_StarFx")
RegistClassMember(LuaPinchFaceWnd, "m_CacheOfflineDataTick")
RegistClassMember(LuaPinchFaceWnd, "m_CheckIsOperatingTick")
RegistClassMember(LuaPinchFaceWnd, "m_HideUIGroupExcepts")
RegistClassMember(LuaPinchFaceWnd, "m_NotHidePopUIExcepts")
RegistClassMember(LuaPinchFaceWnd, "m_PostProcessProfile")
RegistClassMember(LuaPinchFaceWnd, "m_Sound")

function LuaPinchFaceWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BackServerWndButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackServerWndButtonClick()
	end)

	
	UIEventListener.Get(self.BackMainViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackMainViewButtonClick()
	end)


	UIEventListener.Get(self.BackPreviewViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackPreviewViewButtonClick()
	end)


	
	UIEventListener.Get(self.BackPreselectionViewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackPreselectionViewButtonClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
	UIEventListener.Get(self.MainPreviewTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
		self:OnSwipe(delta)
	end)

	UIEventListener.Get(self.MainPreviewTexture.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(go)
		self:OnMainPreviewTextureDragEnd()
	end)

	UIEventListener.Get(self.MainPreviewTexture.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
		self:OnMainPreviewTextureClick()
	end)

	self:InitCommonBtns()
	CUIManager.CloseUI("LoginQueueWnd")
end

function LuaPinchFaceWnd:InitCommonBtns()
	local btn = self.ShowMainViewButtonPos.transform:Find("CommonButton")
	UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowMainViewButtonClick()
	end)
	btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("精细调整")

	btn = self.ShowPreviewViewBtnPos.transform:Find("CommonButton")
	UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowPreviewViewBtnClick()
	end)
	btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("进入预览")

	btn = self.ShowDisplayViewBtnPos.transform:Find("CommonButton")
	UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowDisplayViewBtnClick()
	end)
	btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("下一步")
	
end

function LuaPinchFaceWnd:Start()
	self:OnOpenOrCloseWinSocialWnd()
end

function LuaPinchFaceWnd:Init()
	CUIManager.CloseUI(CUIResources.DialogWnd)
	CUIManager.CloseUI(CUIResources.CharacterCreation2023Wnd)
	CFuxiFaceDNAMgr.Inst:ClearFuxiLogState()
	CUIManager.ShowUI("MiddleNoticeCenter")
	self.BackServerWndButton.gameObject:SetActive(LuaPinchFaceMgr.m_IsInCreateMode)
	self.CloseButton.gameObject:SetActive(LuaPinchFaceMgr.m_IsInModifyMode or LuaPinchFaceMgr.m_IsOffline)
	self.BtnClickFx.gameObject:SetActive(false)
	local hasCacheData = LuaPinchFaceMgr.m_CacheCustomFacialData ~= nil
	self:CreateModel()
	self.PinchFacePreselectionView.gameObject:SetActive(false)
	self.PinchFaceMainView.gameObject:SetActive(hasCacheData)
	self.PinchFacePreviewView.gameObject:SetActive(false)
	self.PinchFaceDisplayView.gameObject:SetActive(false)
	LuaPinchFaceMgr.m_PreselectionViewTabIndex = 0
	self.PinchFacePreselectionView.gameObject:SetActive(not hasCacheData)
	LuaPinchFaceMgr.m_IsFaceChanged = hasCacheData and not LuaPinchFaceMgr.m_HasCustomFacialData
	LuaPinchFaceMgr.m_CacheCustomFacialData = nil
	self:CancelCacheOfflineDataTick()
	LuaPinchFaceMgr.m_CanCacheOfflineData = true
	self.m_CacheOfflineDataTick = RegisterTick(function()
		LuaPinchFaceMgr.m_CanCacheOfflineData = true
	end,PinchFace_Setting.GetData().CacheOfflineDataInterval * 1000)
	if LuaPinchFaceMgr.m_IsInCreateMode then
		LuaPinchFaceTimeCtrlMgr:OnOpenWnd()
	end
    self:HandleAutoLoginByCmdLineArgs()
end

function LuaPinchFaceWnd:HandleAutoLoginByCmdLineArgs()
    if CLoginMgr.Inst:NeedAutoLoginByCmdLineArgs() then
        self:OnShowDisplayViewBtnClick()
    end
end

function LuaPinchFaceWnd:CreateModel()
	self.m_TextureName = "__PinchFaceModel__"
	local transform = GameObject.Find(self.m_TextureName)
	if transform then
		GameObject.Destroy(transform.gameObject)
	end
	self.m_ModelRoot = GameObject(self.m_TextureName).transform
	GameObject.DontDestroyOnLoad(self.m_ModelRoot.gameObject)
	self.m_ModelRoot.localPosition = Vector3(0, -1000, 0)
	self.m_EnvironmentFx = GameObject("EnvironmentFx").transform
	self.m_EnvironmentFx.parent = self.m_ModelRoot
	self.m_EnvironmentFx.transform.localPosition = Vector3.zero
	self.m_BgColorFx = GameObject("BgColorFx").transform
	self.m_BgColorFx.parent = self.m_ModelRoot
	self.m_BgColorFx.transform.localPosition = Vector3.zero
	LuaPinchFaceMgr.m_RO = CRenderObject.CreateRenderObject(self.m_ModelRoot.gameObject, "RenderObject")
	LuaPinchFaceMgr.m_RO.IsPinchFaceImportant = true
	self:LoadResource(LuaPinchFaceMgr.m_RO)

	local obj = GameObject("PinchFaceWndCCinemachineCtrl")
	obj.transform.parent = self.m_ModelRoot.transform
	self.m_Camera = CommonDefs.AddCameraComponent(obj)
	local ctrl = obj:GetComponent(typeof(StreamingController))
	ctrl.streamingMipmapBias = -7

	self:InitCamera(self.m_Camera)
	self:InitCinemachine(obj)

	self:InitSceneRoot()

	self.m_IKTarget = GameObject("iktarget").transform
	self.m_IKTarget.parent = obj.transform

	local x, y = LuaPinchFaceMgr:GetIKOffsetXY()
	self.m_IKTarget.localPosition = Vector3(x, y,-20)
end

function LuaPinchFaceWnd:InitSceneRoot()
	CSharpResourceLoader.Inst:LoadGameObject("Fx/UI/Prefab/PinchFaceScene.prefab", DelegateFactory.Action_GameObject(function(obj)
		local go = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity)
		local bgroot = go.transform:Find("BGRoot")
		for i = 0, bgroot.transform.childCount - 1 do
			local child = bgroot.transform:GetChild(i)
			child.gameObject:SetActive(false)
		end
		go.transform.parent = self.m_ModelRoot.transform
		go.transform.localPosition = Vector3.zero
		go.transform.localScale = Vector3.one
		go.transform.localEulerAngles = Vector3(0, -180, 0)
		NGUITools.SetLayer(go.gameObject, LayerDefine.WaterReflection)
		self.m_SceneRoot = go.transform
		self:ResetDefaultColor()
	end))
end


function LuaPinchFaceWnd:InitBlurTexture()
	if not CLuaPlayerSettings.SupportHighQuality() then return end
	if not self.BlurTex or not self.m_PostProcessProfile then return end

	local rect = Vector4.zero
	if LuaPinchFaceMgr.m_IsEnableBlurTex then
		local corners = self.BlurTex.worldCorners
		local bottomLeft = CUIManager.UIMainCamera:WorldToViewportPoint(corners[0])
		local topRight = CUIManager.UIMainCamera:WorldToViewportPoint(corners[2])
		rect = Vector4(bottomLeft.x,bottomLeft.y,topRight.x,topRight.y)
	end
	local setting = self.m_PostProcessProfile:GetSetting(EnumPostEffectType.PartialBlur)
	if setting then
		setting.enabled:Override(LuaPinchFaceMgr.m_IsEnableBlurTex)
		setting.rect:Override(rect)
	end
end

function LuaPinchFaceWnd:LoadResource(ro)
	ro.transform.localPosition = Vector3(0, 0, 0) 
	ro.transform.localEulerAngles = Vector3(0, -180, 0)
	ro.Layer = LayerDefine.ModelForNGUI_3D
	ro.ShadowType = EShadowType.None
	ro.CanBeJob = false
	if CClientMainPlayer.s_UseHighAccuracy and CommonDefs.IsAndroidPlatform() then
		ro.UseHighAccuracy = true
	end
	LuaPinchFaceMgr.m_PreselectionIndex = LuaPinchFaceMgr.m_InitialPreselectionIndex
	local hasCacheData = LuaPinchFaceMgr.m_CacheCustomFacialData ~= nil
	if not hasCacheData then
		LuaPinchFaceMgr.m_HairId = LuaPinchFaceMgr:GetPreselectionHairId(LuaPinchFaceMgr.m_PreselectionIndex)
	end
	LuaPinchFaceMgr:SetFashion(LuaPinchFaceMgr.m_FashionData, EnumPinchFaceAniState.PreviewStand01, not LuaPinchFaceMgr.m_IsInModifyMode, LuaPinchFaceMgr.m_CacheCustomFacialData)
end

function LuaPinchFaceWnd:InitCamera(camera)
	LuaPinchFaceMgr.m_Camera = camera
	camera.clearFlags = CameraClearFlags.Depth
	camera.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.WaterReflection
	camera.orthographic = false
	camera.fieldOfView = 15
	camera.farClipPlane = 30
	camera.nearClipPlane = 0.1
	camera.depthTextureMode = DepthTextureMode.Depth
	camera.depth = 0
	camera.backgroundColor = Color(0, 0, 0, 0)
	camera.allowHDR = true
	camera.clearFlags = CameraClearFlags.SolidColor
	camera.clearFlags = CameraClearFlags.Depth
	self.m_EffectController = CPostProcessingMgr.Instance:Registercamera(camera, EPostCameraType.UI)
	self.m_EffectController:AddPostProcessingProfile("assets/res/fx/postprocessingeffect/Profile/pinchfaceSceneProfile.asset",  DelegateFactory.Action_ScriptableObject(function (so)
		self.m_PostProcessProfile = so
		self:InitBlurTexture()
	end))
end

function LuaPinchFaceWnd:InitCinemachine(obj)
	if not LuaPinchFaceMgr.m_RO then return end
	self.m_CCinemachineCtrlMgr = obj:AddComponent(typeof(CPinchFaceCinemachineCtrlMgr))
	local data = LuaPinchFaceMgr:GetInitializationData()
	self.m_CCinemachineCtrlMgr:SetFollowObj(LuaPinchFaceMgr.m_RO.transform)
	if data then
		self.m_CCinemachineCtrlMgr:SetNewCamParams(0, data.CameraState1)
		self.m_CCinemachineCtrlMgr:SetNewCamParams(1, data.CameraState2)
		self.m_CCinemachineCtrlMgr:SetNewCamParams(2, data.CameraState3)
		self.m_CCinemachineCtrlMgr:SetNewCamParams(3, data.CameraState4)
		self.m_CCinemachineCtrlMgr:SetNewCamParams(4, data.CameraState5)
		self.m_CCinemachineCtrlMgr:SetNewCamParams(5, data.CameraState6)
		self.m_CCinemachineCtrlMgr:UpdateEyeAnchor(data.EyeOffset[3], data.EyeOffset[4], data.EyeOffset[5], data.EyeOffset[0], data.EyeOffset[1], data.EyeOffset[2])
	end
	self.m_CCinemachineCtrlMgr:ShowCam(1)
end

function LuaPinchFaceWnd:OnDestroy()
	if self.m_ModelRoot then
		GameObject.Destroy(self.m_ModelRoot.gameObject)
		self.m_ModelRoot = nil
	end
	if self.MainPreviewTexture.mainTexture then
		RenderTexture.ReleaseTemporary(self.MainPreviewTexture.mainTexture)
		self.MainPreviewTexture.mainTexture = nil
	end
	if LuaPinchFaceMgr.m_IsOffline then
        LuaPinchFaceMgr.m_IsOffline = false
        CUIManager.ShowUIGroup(CUIManager.EUIModuleGroup.EServerWndUI)
		LuaPinchFaceTimeCtrlMgr:OnBackToLoginScene()
		LuaLoginMgr:HideQueueStatusTopNotice()
    end
	self:CancelCacheOfflineDataTick()
	if self.m_StarFx then
		self.m_StarFx:Destroy()
	end
	LuaPinchFaceMgr:OnDestroy()
end

function LuaPinchFaceWnd:CancelUpdateCameraRectTick()
	if self.m_UpdateCameraRectTick then
		UnRegisterTick(self.m_UpdateCameraRectTick)
		self.m_UpdateCameraRectTick = nil
	end
end

function LuaPinchFaceWnd:CancelCacheOfflineDataTick()
	if self.m_CacheOfflineDataTick then
		UnRegisterTick(self.m_CacheOfflineDataTick)
		self.m_CacheOfflineDataTick = nil
	end
end

function LuaPinchFaceWnd:CancelCheckPlayerOperatingTick()
	if self.m_CheckIsOperatingTick then
		UnRegisterTick(self.m_CheckIsOperatingTick)
		self.m_CheckIsOperatingTick = nil
	end
end

function LuaPinchFaceWnd:ResetDefaultColor()
	local colorData =  PinchFace_BgColor.GetData(1)
	if LuaPinchFaceMgr.m_BgColorData then
		colorData = LuaPinchFaceMgr.m_BgColorData
	end
	LuaPinchFaceMgr.m_BgColorData = colorData
	self:OnSetPinchFaceCameraBgColor(colorData)
end

--@region UIEvent

function LuaPinchFaceWnd:OnShowMainViewButtonClick()
	LuaPinchFaceMgr:CachePreselectionPartDatas()
	self.PinchFacePreselectionView.gameObject:SetActive(false)
	self.PinchFaceMainView.gameObject:SetActive(true)
	if LuaPinchFaceMgr.m_RO then
		LuaPinchFaceMgr.m_RO:Preview("stand01", 0)
	end
end

function LuaPinchFaceWnd:OnBackServerWndButtonClick()
	if LuaPinchFaceMgr.m_IsInCreateMode then
		local data = LuaPinchFaceMgr:LoadOfflineDataCache()
		if data then
			local msg = g_MessageMgr:FormatMessage("ClearFacialDataCacheConfirm")
			MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                LuaPinchFaceMgr:ClearOfflineData()
				self:BackServerWnd()
            end),DelegateFactory.Action(function()
            end),nil,nil,false)
			return
		end
	end
	self:BackServerWnd()
end

function LuaPinchFaceWnd:BackServerWnd()
	if LuaPinchFaceMgr.m_IsOffline then
		LuaCharacterCreation2023Mgr:ShowLoginUI()
	end

	if not LuaPinchFaceMgr.m_IsOffline then
		LuaCharacterCreation2023Mgr:OpenCreatingOrExitingWnd(EnumToInt(LuaPinchFaceMgr.m_CurEnumClass),true)
	else
		CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
	end
end

function LuaPinchFaceWnd:OnShowPreviewViewBtnClick()
	CFuxiFaceDNAMgr.Inst:TryReportApplyLog("RandomApply")
	LuaPinchFaceMgr:SetBlurTexVisible(false)
	LuaPinchFaceMgr:SetIKLookAtTo(true)
	self.PinchFaceMainView.gameObject:SetActive(false)
	self.PinchFacePreviewView.gameObject:SetActive(true)
end

function LuaPinchFaceWnd:OnShowDisplayViewBtnClick()
	LuaPinchFaceMgr:SetIKLookAtTo(false)
	self.PinchFacePreviewView.gameObject:SetActive(false)
	self.PinchFaceDisplayView.gameObject:SetActive(true)
	if LuaPinchFaceMgr.m_IsEnableAutoSaveData then
		LuaPinchFaceMgr:SaveData()
	end
	g_ScriptEvent:BroadcastInLua("OnShowPinchFaceDisplayView_PlayMovie")
	if CommonDefs.IS_VN_CLIENT then
		LuaSEASdkMgr:AppsflyerNotify("Generate_Face", SafeStringFormat3("{}"))
		LuaSEASdkMgr:FaceBookTracking("FB_Generate_Face", "")
	end
end

function LuaPinchFaceWnd:OnBackMainViewButtonClick()
	LuaPinchFaceMgr:SetBlurTexVisible(false)
	
	--重置肤色和发色
	LuaPinchFaceMgr:SetSkinColor(0)
	local resetAdvancedHairColorIndex = 0
	if LuaPinchFaceMgr.m_IsInModifyMode and CClientMainPlayer.Inst then
		local appearanceProp = CClientMainPlayer.Inst.AppearanceProp
		resetAdvancedHairColorIndex = appearanceProp.AdvancedHairColorId
	end
	LuaPinchFaceMgr:SetAdvancedHairColorIndex(resetAdvancedHairColorIndex)

	self.PinchFaceMainView.gameObject:SetActive(true)
	self.PinchFacePreviewView.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.m_EnvironmentFx)
end

function LuaPinchFaceWnd:OnBackPreviewViewButtonClick()
	self.PinchFacePreviewView.gameObject:SetActive(true)
	self.PinchFaceDisplayView.gameObject:SetActive(false)
end

function LuaPinchFaceWnd:OnBackPreselectionViewButtonClick()
	self:BackPreselectionView()
end

function LuaPinchFaceWnd:BackPreselectionView()
	LuaPinchFaceMgr.m_PreselectionViewTabIndex = 1
	self.PinchFacePreselectionView.gameObject:SetActive(true)
	self.PinchFaceMainView.gameObject:SetActive(false)
end

function LuaPinchFaceWnd:OnCloseButtonClick()
	if LuaPinchFaceMgr:IsFacialDataChanged() then
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("PinchFaceWndCloseButtonClickConfirm"), DelegateFactory.Action(function ()
			CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
			LuaPinchFaceMgr:ClearOfflineData()
        end), nil, nil, nil, false)
	else
		CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
		LuaPinchFaceMgr:ClearOfflineData()
	end
end

--@endregion UIEvent

function LuaPinchFaceWnd:Update()
	if Input.GetMouseButtonDown(0) then
		LuaPinchFaceMgr.m_IsPlayerOperating = true
		self:CancelCheckPlayerOperatingTick()
		self.m_CheckIsOperatingTick = RegisterTickOnce(function()
			self:CancelCheckPlayerOperatingTick()
			LuaPinchFaceMgr.m_IsPlayerOperating = false
		end, LuaPinchFaceTimeCtrlMgr.m_CheckIsOperatingInterval * 1000)
	end
end

function LuaPinchFaceWnd:OnEnable()
	CTT3.Weather.WeatherManager.ApplyTimelineProfileByName("")
	CTT3.Weather.WeatherManager.Inst:SetNewWeatherSystemEnable(false)
	CMainCamera.Inst:SetCameraEnableStatus(false, "use_another_camera", false)
	local excepts = {"MiddleNoticeCenter", "TopNoticeCenter"}
	local List_String = MakeGenericClass(List,cs_string)
	self.m_HideUIGroupExcepts = Table2List(excepts, List_String)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, self.m_HideUIGroupExcepts, false, true)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, self.m_HideUIGroupExcepts, false, true)
	CUIManager.HideUIByChangeLayer(CUIResources.CharacterCreation2023Wnd,false,true)
	local excepts2 = {"PinchFaceWnd"}
	self.m_NotHidePopUIExcepts = Table2List(excepts2, List_String)
	CUIManager.SetPopViewsVisiable(false, self.m_NotHidePopUIExcepts,nil)
	local cameraObj = GameObject.Find("SceneRoot/Camera")
	if cameraObj then
		cameraObj:SetActive(false)
	end
	GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.PinchFaceWnd)
	CPinchFaceHubMgr.Inst:RefreshToken()
	CUIManager.ModelLightEnabled = false
	g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
	g_ScriptEvent:AddListener("AppFocusChange", self, "OnApplicationFocus")
	g_ScriptEvent:AddListener("OnScreenChange", self, "OnScreenChange")
	g_ScriptEvent:AddListener("OnShowPinchFaceClickFx", self, "OnShowPinchFaceClickFx")
	g_ScriptEvent:AddListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
	g_ScriptEvent:AddListener("OnSetPinchFaceCameraBgColor", self, "OnSetPinchFaceCameraBgColor")
	g_ScriptEvent:AddListener("OnSetPinchFaceEnvironmentFx", self, "OnSetPinchFaceEnvironmentFx")
	g_ScriptEvent:AddListener("OnSetPinchFacePinchEnabled", self, "OnSetPinchFacePinchEnabled")
	g_ScriptEvent:AddListener("OnSetPinchFaceBlurTexVisible", self, "OnSetPinchFaceBlurTexVisible")
	g_ScriptEvent:AddListener("SendQueueStatus", self, "OnSendQueueStatus")
	g_ScriptEvent:AddListener("OpenOrCloseWinSocialWnd", self, "OnOpenOrCloseWinSocialWnd")
	g_ScriptEvent:AddListener("OnMiniVolumeSettingsChanged", self, "OnMiniVolumeSettingsChanged")
	local bgmevent = "event:/L10/L10_music/MUS_login"
	if LuaPinchFaceMgr.m_IsInModifyMode then
		SoundManager.Inst:StopBGMusic()
	end
	SoundManager.s_BlockBgMusic = true
	self.m_Music = SoundManager.Inst:PlaySound(bgmevent,Vector3.zero, 1, nil, 0)
	self:OnMiniVolumeSettingsChanged()
	if CommonDefs.IsIOSPlatform() then
        --CommonDefs.SetAutoRotateEnabled(false, "pinchfacewnd")
    end
	CLuaPlayerSettings.SetShaderLod()
	self:AddCodeScan()
end

function LuaPinchFaceWnd:OnDisable()
	if CRenderScene.Inst and CRenderScene.Inst.m_NowSceneName == Setting.sNewDengLu_Level then
        CTT3.Weather.WeatherManager.Inst:SetNewWeatherSystemEnable(true)
		CTT3.Weather.WeatherManager.Inst:LoadNewWeatherSceneAsset(Setting.sNewDengLu_Level)
	end
	CMainCamera.Inst:SetCameraEnableStatus(true, "use_another_camera", false)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EGameUI, self.m_HideUIGroupExcepts, false, true)
	CUIManager.ShowUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, self.m_HideUIGroupExcepts, false, true)
	CUIManager.SetPopViewsVisiable(true, self.m_NotHidePopUIExcepts,nil)
	if CRenderScene.Inst then
		local cameraObj = CRenderScene.Inst.transform:Find("Camera")
		if cameraObj then
			cameraObj.gameObject:SetActive(true)
		end
	end
	CameraFollow.Inst:ResetToDefault()
	CPinchFaceHubMgr.Inst:Clear()
	CUIManager.CloseUI(CLuaUIResources.PinchFaceHubWnd)
	CUIManager.ModelLightEnabled = true
	g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
	g_ScriptEvent:RemoveListener("AppFocusChange", self, "OnApplicationFocus")
	g_ScriptEvent:RemoveListener("OnScreenChange", self, "OnScreenChange")
	g_ScriptEvent:RemoveListener("OnShowPinchFaceClickFx", self, "OnShowPinchFaceClickFx")
	g_ScriptEvent:RemoveListener("OnSetPinchFaceIKLookAtTo", self, "OnSetPinchFaceIKLookAtTo")
	g_ScriptEvent:RemoveListener("OnSetPinchFaceCameraBgColor", self, "OnSetPinchFaceCameraBgColor")
	g_ScriptEvent:RemoveListener("OnSetPinchFaceEnvironmentFx", self, "OnSetPinchFaceEnvironmentFx")
	g_ScriptEvent:RemoveListener("OnSetPinchFacePinchEnabled", self, "OnSetPinchFacePinchEnabled")
	g_ScriptEvent:RemoveListener("OnSetPinchFaceBlurTexVisible", self, "OnSetPinchFaceBlurTexVisible")
	g_ScriptEvent:RemoveListener("SendQueueStatus", self, "OnSendQueueStatus")
	g_ScriptEvent:RemoveListener("OpenOrCloseWinSocialWnd", self, "OnOpenOrCloseWinSocialWnd")
	g_ScriptEvent:RemoveListener("OnMiniVolumeSettingsChanged", self, "OnMiniVolumeSettingsChanged")
	self:CancelUpdateCameraRectTick()
	self:CancelCheckPlayerOperatingTick()
	if self.m_EffectController then
		self.m_EffectController:SetRainDropEffect(0,false)
	end
	if Shader.globalMaximumLOD == 800 then
		Shader.globalMaximumLOD = ShaderLodParams.SHADER_LOD_HIGH_LEVEL
	end
	CCharacterRenderMgr.Inst:RejectCharacterModel("PinchFaceWnd")
	SoundManager.Inst:StopSound(self.m_Music)
	SoundManager.s_BlockBgMusic = false
	if LuaPinchFaceMgr.m_IsInModifyMode then
		SoundManager.Inst:StartBGMusic()
	end
	if CommonDefs.IsIOSPlatform() then
        --CommonDefs.SetAutoRotateEnabled(true, "pinchfacewnd")
    end
	self:ClearCodeScan()
end

function LuaPinchFaceWnd:AddCodeScan()
	if self.m_OnNtQRCodeScannerMessageDelegate == nil then
		self.m_OnNtQRCodeScannerMessageDelegate = DelegateFactory.Action_string(function (data)
			self:OnScannerMessage(data)
		end)
		EventManager.AddListenerInternal(EnumEventType.OnScannerMessage, self.m_OnNtQRCodeScannerMessageDelegate)
	end

    if self.m_OnScannerMessageDelegate == nil then
        self.m_OnScannerMessageDelegate = DelegateFactory.Action_string(function (data)
            self:OnScannerMessage(data)
        end)
        CommonDefs.CombineCodeScannerOnMessage(self.m_OnScannerMessageDelegate, true)
    end

    if self.m_CombineCodeScannerOnEventDelegate == nil then
        self.m_CombineCodeScannerOnEventDelegate = DelegateFactory.Action_string(function (data)
            self:OnScannerEvent(data)
        end)
        CommonDefs.CombineCodeScannerOnEvent(self.m_CombineCodeScannerOnEventDelegate,  true)
    end

    if self.m_CombineCodeScannerOnDecoderMessageDelegate == nil then
        self.m_CombineCodeScannerOnDecoderMessageDelegate = DelegateFactory.Action_string(function (data)
            self:OnDecoderMessage(data)
        end)
        CommonDefs.CombineCodeScannerOnDecoderMessage(self.m_CombineCodeScannerOnDecoderMessageDelegate,  true)
    end
end

function LuaPinchFaceWnd:OnScannerMessage(data)
    if cs_string.IsNullOrEmpty(data) then
		g_MessageMgr:ShowMessage("PinchFace_ImportData_Failed")
        return
    end
	local m = CPinchFaceHubMgr.s_Regex:Match(data)
	local shareId = ""
	if m.Length > 1 then
		shareId = m.Groups[1].Value
	end
	if not cs_string.IsNullOrEmpty(shareId) then
		CPinchFaceHubMgr.Inst:GetByShareId(shareId, DelegateFactory.Action_CPinchFaceHub_ShareWork_Ret(function(ret)
			local data = ret.data
			if ret.code == 0 then
				local work = CPinchFaceWork(ret.data)
				work:RequestPinchFaceData(DelegateFactory.Action_bytes(function (bytes)
					CFacialMgr.CurFacialSaveData = CFacialMgr.ParseHubData(bytes)
					local data = CFacialMgr.CurFacialSaveData
        			LuaPinchFaceMgr:OnImportData(data, true)
				end))
			else
				g_MessageMgr:ShowMessage("PinchFace_ImportData_Failed")
			end
		end
		))
	else
		g_MessageMgr:ShowMessage("PinchFace_ImportData_Failed")
	end
end

function LuaPinchFaceWnd:OnScannerEvent(data)

end

function LuaPinchFaceWnd:OnDecoderMessage(data)

end

function LuaPinchFaceWnd:ClearCodeScan()
	if self.m_OnNtQRCodeScannerMessageDelegate then
		EventManager.RemoveListenerInternal(EnumEventType.OnScannerMessage, self.m_OnNtQRCodeScannerMessageDelegate)
		self.m_OnNtQRCodeScannerMessageDelegate = nil
	end

    if self.m_OnScannerMessageDelegate then
        CommonDefs.CombineCodeScannerOnMessage(self.m_OnScannerMessageDelegate, false)
        self.m_OnScannerMessageDelegate = nil
    end
    if self.m_CombineCodeScannerOnEventDelegate then
        CommonDefs.CombineCodeScannerOnEvent(self.m_CombineCodeScannerOnEventDelegate, false)
        self.m_CombineCodeScannerOnEventDelegate = nil
    end
    if self.m_CombineCodeScannerOnDecoderMessageDelegate then
        CommonDefs.CombineCodeScannerOnDecoderMessage(self.m_CombineCodeScannerOnDecoderMessageDelegate, false)
        self.m_CombineCodeScannerOnDecoderMessageDelegate = nil
    end
end

function LuaPinchFaceWnd:OnMiniVolumeSettingsChanged()
	if self.m_Music then
        SoundManager.Inst:UpdateVolume(self.m_Music, PlayerSettings.MusicEnabled and PlayerSettings.VolumeSetting or 0)
    end
end

function LuaPinchFaceWnd:OnOpenOrCloseWinSocialWnd()
	if CommonDefs.IsPCGameMode() then
		local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
		if self.m_Camera then
			if self.IsWinSocialWndOpened ~= CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
				self.IsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
				if CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened then
					local rect = Rect(0, 0, 1-Constants.WinSocialWndRatio, 1)
					self.m_Camera.rect = rect
				else
					local rect = Rect(0, 0, 1, 1)
					self.m_Camera.rect = rect
				end
			end
		end
	end
end

function LuaPinchFaceWnd:OnMainPlayerCreated()
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, self.m_HideUIGroupExcepts, false, true)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, self.m_HideUIGroupExcepts, false, true)
	CUIManager.HideUIByChangeLayer(CUIResources.CharacterCreation2023Wnd,false,true)
	CUIManager.SetPopViewsVisiable(false, self.m_NotHidePopUIExcepts,nil)
	CTT3.Weather.WeatherManager.ApplyTimelineProfileByName("")
	CTT3.Weather.WeatherManager.Inst:SetNewWeatherSystemEnable(false)
	self:OnSetPinchFaceCameraBgColor(LuaPinchFaceMgr.m_BgColorData)
end

function LuaPinchFaceWnd:OnShowPinchFaceClickFx()
	self.BtnClickFx.gameObject:SetActive(false)
	local wrorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
	local localPos = self.BtnClickFx.transform.parent:InverseTransformPoint(wrorldPos)
	self.BtnClickFx.transform.localPosition = localPos
	self.BtnClickFx:LoadFx("fx/Vfx/Prefab/Dynamic/vfx_nielian_click.prefab")
	self.BtnClickFx.gameObject:SetActive(true)
end

function LuaPinchFaceWnd:OnApplicationFocus(args)
	local focusStatus = args[0]
	if CommonDefs.IsIOSPlatform() then
		self.MainPreviewTexture.gameObject:SetActive(focusStatus) 
	end
end

function LuaPinchFaceWnd:OnSetPinchFaceBlurTexVisible(visible)
	self:InitBlurTexture()
end

function LuaPinchFaceWnd:OnSetPinchFacePinchEnabled(enabled)
	if self.m_CCinemachineCtrlMgr then
		self.m_CCinemachineCtrlMgr:SetPinchEnabled(enabled)
	end
end

function LuaPinchFaceWnd:OnSetPinchFaceEnvironmentFx(data)
	if self.m_EnvironmentFx then
		Extensions.RemoveAllChildren(self.m_EnvironmentFx)
		if data.Fx then
			CSharpResourceLoader.Inst:LoadGameObject(data.Fx, DelegateFactory.Action_GameObject(function(obj)
				if obj then
					local go = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity)
					go.transform.parent = self.m_ModelRoot.transform
					go.transform.localPosition = Vector3.zero
					go.transform.localScale = Vector3.one
					NGUITools.SetLayer(go.gameObject, LayerDefine.Effect_3D)
					go.transform.parent = self.m_EnvironmentFx
					local wf = go.transform:GetComponent(typeof(CWorldPositionFX))
					wf:SetCanSkipUnimportant(false)
					local fx = CFX()
					wf:Init(fx, self.m_ModelRoot.transform.position, 0, 0, 1, 1, nil, nil, nil)
					go.transform.localEulerAngles = Vector3(0, 180, 0)
				end
			end))
		end
		if self.m_EffectController then
			self.m_EffectController:SetRainDropEffect(data.RainDropEffectParameter and data.RainDropEffectParameter or 0, data.RainDropEffectParameter and data.RainDropEffectParameter > 0 or false)
		end
	end
end

function LuaPinchFaceWnd:OnSetPinchFaceCameraBgColor(data)
	local bgroot = self.m_SceneRoot.transform:Find("BGRoot")
	if bgroot then
		local childCount = bgroot.transform.childCount
		for i = 0, childCount - 1 do
			local child = bgroot.transform:GetChild(i)
			if child then
				local isCurBackground = data.BackgroundName == child.name
				child.gameObject:SetActive(isCurBackground)
				if isCurBackground then
					NGUITools.SetLayer(child.gameObject, LayerDefine.WaterReflection)
					local water = CommonDefs.GetComponentInChildren_GameObject_Type(child.gameObject, typeof(CWater))
					if water then
						water.currentCamera = self.m_Camera
						if water.reflectionCamera then
							GameObject.Destroy(water.reflectionCamera.gameObject)
							water.reflectionCamera = nil
						end
						water:StartUp()
					end
				end
			end
		end
	end
	if self.m_BgColorFx then
		Extensions.RemoveAllChildren(self.m_BgColorFx)
		if self.m_StarFx then
			self.m_StarFx:Destroy()
		end
		if data.FxType == 2 then
			local pos = self.m_BgColorFx.transform.position
			local fxId = PinchFace_Setting.GetData().Background_StarFxId
			self.m_StarFx = CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
		end
	end
	self.RainbowFx:DestroyFx()
	if data.FxType == 1 then
		local id = PinchFace_Setting.GetData().Background_RainbowFxId
		local fx = Fx_UI.GetData(id)
		self.RainbowFx:LoadFx(fx.Prefab)
	end
	CCharacterRenderMgr.Inst:ApplyCharacterModel("PinchFaceWnd",data.BackgroundName)
end

function LuaPinchFaceWnd:OnScreenChange()
	self:InitBlurTexture()
end

function LuaPinchFaceWnd:OnSwipe(delta)
	local deltaVal= - delta.x / Screen.width * 360
	self:RotateRoleModel(deltaVal)
end

function LuaPinchFaceWnd:RotateRoleModel(degree)
	if not LuaPinchFaceMgr.m_RO or not LuaPinchFaceMgr.m_IsEnableRotateRole then return end
    local t = LuaPinchFaceMgr.m_RO.transform
    if t and t.transform.gameObject.activeSelf then
		local newEulerAngleY = (t.transform.localEulerAngles.y + degree + 180) % 360 - 180
		if newEulerAngleY ~= t.transform.localEulerAngles.y then
			LuaPinchFaceMgr:RotateRoleModel(newEulerAngleY)
		end

		
		if LuaPinchFaceMgr.m_IsEnableIK and self.m_Target then
			local canik = self:CheckCameraPosForIK()
			if not canik then
				LuaPinchFaceMgr.m_RO:SetLookAtIKTarget(nil, true, false)
				self.m_Target = nil
			end
		end
    end
end

function LuaPinchFaceWnd:OnSetPinchFaceIKLookAtTo()
	if not LuaPinchFaceMgr.m_RO then return end
	if not LuaPinchFaceMgr.m_IsEnableIK then
		LuaPinchFaceMgr.m_RO:SetLookAtIKTarget(nil, true, false)
		return
	end
	local t = LuaPinchFaceMgr.m_IsUseIKLookAt and self.m_IKTarget or nil
	if not self:CheckCameraPosForIK() then
		t = nil
	end
	if LuaPinchFaceMgr.m_IsUseIKLookAt and LuaPinchFaceMgr.m_RO.m_AnimationFast and LuaPinchFaceMgr.m_RO.m_AnimationFast.m_IK and LuaPinchFaceMgr.m_RO.m_AnimationFast.m_IK.solver and LuaPinchFaceMgr.m_RO.m_AnimationFast.m_IK.solver.target == t then
		return
	end
	LuaPinchFaceMgr.m_RO:SetLookAtIKTarget(t, true, false)
	self.m_Target = t
end

function LuaPinchFaceWnd:OnMainPreviewTextureDragEnd()
	self:OnSetPinchFaceIKLookAtTo()
end

function LuaPinchFaceWnd:CheckCameraPosForIK()
	if not LuaPinchFaceMgr.m_IsUseIKLookAt or nil == LuaPinchFaceMgr.m_RO then
		return false
	end
	local t = LuaPinchFaceMgr.m_RO.gameObject
    if t ~= nil and t.gameObject.activeSelf then
		local yAngle = t.transform.localEulerAngles.y
		local cameraObj = self.m_Camera
		local targetDir = CommonDefs.op_Subtraction_Vector3_Vector3(cameraObj.transform.position, LuaPinchFaceMgr.m_RO.transform.position)
		local conbined = (Vector3.Lerp(LuaPinchFaceMgr.m_RO:GetSlotTransform("Bip001 Pelvis").up, LuaPinchFaceMgr.m_RO.transform.forward, NormalCamera.m_lerpFactor)).normalized
		local pTocAngle = Vector3.Angle(targetDir, conbined)
		return pTocAngle <= 65 and (yAngle >= 90 and yAngle <= 270)
    end
	return false
end

function LuaPinchFaceWnd:OnMainPreviewTextureClick()
end

function LuaPinchFaceWnd:OnWillClose()
    if LuaPinchFaceMgr.m_IsInCreateMode then
		LuaPinchFaceTimeCtrlMgr:OnCloseWnd()
	end
end

function LuaPinchFaceWnd:OnSendQueueStatus()
	LuaLoginMgr:ShowQueueStatusTopNotice()
end