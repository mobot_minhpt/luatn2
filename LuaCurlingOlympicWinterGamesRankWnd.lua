local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local QnRadioBox = import "L10.UI.QnRadioBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Constants = import "L10.Game.Constants"

LuaCurlingOlympicWinterGamesRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCurlingOlympicWinterGamesRankWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaCurlingOlympicWinterGamesRankWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end

function LuaCurlingOlympicWinterGamesRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaCurlingOlympicWinterGamesRankWnd:Init()
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.Template.gameObject:SetActive(false)
    self:OnCurlingQueryRankResult()
    Gac2Gas.Curling_QueryRank("")
end

function LuaCurlingOlympicWinterGamesRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnCurlingQueryRankResult", self, "OnCurlingQueryRankResult")
end

function LuaCurlingOlympicWinterGamesRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnCurlingQueryRankResult", self, "OnCurlingQueryRankResult")
end

function LuaCurlingOlympicWinterGamesRankWnd:OnCurlingQueryRankResult()
    Extensions.RemoveAllChildren(self.Grid.transform)
    local radioBox = self.Grid.gameObject:GetComponent(typeof(QnRadioBox))
    radioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #LuaOlympicGamesMgr.m_RankResult)
    for i = 1,#LuaOlympicGamesMgr.m_RankResult do
        local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template.gameObject)
        self:InitItem(obj, i)
        radioBox.m_RadioButtons[i - 1] = obj:GetComponent(typeof(QnSelectableButton))
        radioBox.m_RadioButtons[i - 1]:SetSelected(false, false)
        radioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(radioBox.On_Click, MakeGenericClass(Action1, QnButton), radioBox)
    end
    self.Grid:Reposition()
end
--@region UIEvent

--@endregion UIEvent

function LuaCurlingOlympicWinterGamesRankWnd:InitItem(obj,i)
    obj.gameObject:SetActive(true)
    local t = LuaOlympicGamesMgr.m_RankResult[i]
    local bg = obj.transform:Find("Bg"):GetComponent(typeof(UISprite))
    local rankLabel = obj.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = obj.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local distLabel1 = obj.transform:Find("DistLabel1"):GetComponent(typeof(UILabel))
    local distLabel2 = obj.transform:Find("DistLabel2"):GetComponent(typeof(UILabel))
    local distLabel3 = obj.transform:Find("DistLabel3"):GetComponent(typeof(UILabel))
    local distLabelArray = {distLabel1,distLabel2,distLabel3}
    local myObject = obj.transform:Find("My")

    bg.spriteName = i % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
    nameLabel.text = t.Name
    for j = 1, 3 do
        local distLabel = distLabelArray[j]
        distLabel.text = LuaOlympicGamesMgr.m_CurlingGameRound > j and LocalString.GetString("无效") or LocalString.GetString("暂无")
        if t.Rounds then
            if t.Rounds and t.Rounds[j] and t.Rounds[j].Dist then
                if t.Rounds[j].Dist >= 0 then
                    distLabel.text = SafeStringFormat3(LocalString.GetString("%.1f米"),t.Rounds[j].Dist / 64)
                else
                    distLabel.text = LocalString.GetString("无效")
                end
            end
        end
    end
    rankImage.spriteName = ""
    if i == 1 then
        rankImage.spriteName = Constants.RankFirstSpriteName
    elseif i == 2 then
        rankImage.spriteName = Constants.RankSecondSpriteName
    elseif i == 3 then
        rankImage.spriteName = Constants.RankThirdSpriteName
    end
    rankLabel.text = i <= 3 and "" or i
    myObject.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == t.Id)
end
