local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIInput = import "UIInput"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Object = import "System.Object"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local AlignType3 = import "L10.UI.CTooltip+AlignType"

LuaZongMenHongBaoSetWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongMenHongBaoSetWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "SortBtn", "SortBtn", QnSelectableButton)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "SubmitButton", "SubmitButton", GameObject)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "RemainMoneyLabel", "RemainMoneyLabel", UILabel)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "RemainTimesLabel", "RemainTimesLabel", UILabel)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "StatusText", "StatusText", UIInput)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "ItemsView", "ItemsView", GameObject)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "ClearButton", "ClearButton", GameObject)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "SortSprite", "SortSprite", GameObject)
RegistChildComponent(LuaZongMenHongBaoSetWnd, "Blank", "Blank", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_PresetData")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_ItemList")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_ItemPosList")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_Cost")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_MaxCount")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_MinCount")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_ItemCount")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_RemainJadeLimit")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_RemainSendTimes")
RegistClassMember(LuaZongMenHongBaoSetWnd, "m_RadioBox")
function LuaZongMenHongBaoSetWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.SortBtn.OnButtonSelected = DelegateFactory.Action_bool(function (selected)
	    self:OnSortBtnSelected(selected)
	end)

	UIEventListener.Get(self.SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitButtonClick()
	end)

	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)

	UIEventListener.Get(self.ClearButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearButtonClick()
	end)

	UIEventListener.Get(self.Blank.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	end)
    --@endregion EventBind end
end

function LuaZongMenHongBaoSetWnd:Init()
	LuaZongMenMgr.m_CurSelectHongBaoItemIndex = 1
	self.m_ItemList = {}
	self.m_ItemPosList = {}
	self.m_Cost = 0
	self.m_MaxCount = 12
	self.m_MinCount = 4
	self.RemainMoneyLabel.text = ""
	self.RemainTimesLabel.text = ""
	self.QnCostAndOwnMoney:SetCost(self.m_Cost)
	self.ItemTemplate.gameObject:SetActive(false)
	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(v)
		self:OnNumChanged(v)
    end)
	self.QnIncreseAndDecreaseButton.onKeyBoardClosed = DelegateFactory.Action_uint(function(v)
		CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
    end)
	self.QnIncreseAndDecreaseButton.onIncAndDecButtonClicked = DelegateFactory.Action_uint(function(v)
		CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
    end)
	self.QnIncreseAndDecreaseButton:SetMinMax(self.m_MinCount, self.m_MaxCount, 1)
	self.QnIncreseAndDecreaseButton:SetValue(self.m_MinCount, true)
	self.QnIncreseAndDecreaseButton:SetNumberInputAlignType(AlignType3.Bottom)
	self:InitSortButton()
	self.QnCostAndOwnMoney:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	Gac2Gas.RequestPrepareSendSectItemRedPack()
end

function LuaZongMenHongBaoSetWnd:InitSortButton()
	self.m_PresetData = {}
	SectItemRedPack_Preset.ForeachKey(function (key)
		self.m_PresetData[key] = SectItemRedPack_Preset.GetData(key)
	end)
end

function LuaZongMenHongBaoSetWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncPrepareSendSectItemRedPackInfo", self, "OnSyncPrepareSendSectItemRedPackInfo")
	g_ScriptEvent:AddListener("SetSectHongBaoItem", self, "OnSetSectHongBaoItem")
end

function LuaZongMenHongBaoSetWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncPrepareSendSectItemRedPackInfo", self, "OnSyncPrepareSendSectItemRedPackInfo")
	g_ScriptEvent:RemoveListener("SetSectHongBaoItem", self, "OnSetSectHongBaoItem")
	CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
end

function LuaZongMenHongBaoSetWnd:OnSetSectHongBaoItem(isDelete)
	self.m_ItemList = {}
	self.m_Cost = 0	
	if isDelete then
		self.m_ItemCount = self.m_ItemCount - 1
	end
	for i = 1, self.m_MaxCount do
		local data = LuaZongMenMgr.m_HongBaoItemList[i]
		if data then
			self.m_Cost = self.m_Cost + data.price
		end
		self.m_ItemList[i] = data
	end
	self.QnCostAndOwnMoney:SetCost(self.m_Cost)
	self.QnIncreseAndDecreaseButton:SetValue(self.m_ItemCount, true)
end

function LuaZongMenHongBaoSetWnd:OnSyncPrepareSendSectItemRedPackInfo(remainJadeLimit, remainSendTimes,lastItemList,dynamicOnShelfItemsSet)
	self.RemainMoneyLabel.text = SafeStringFormat3(LocalString.GetString("剩余灵玉赠送额度%d"),remainJadeLimit)
	self.RemainTimesLabel.text = SafeStringFormat3(LocalString.GetString("今日还可以发%d次"),remainSendTimes)
	self.m_RemainJadeLimit = remainJadeLimit
	self.m_RemainSendTimes = remainSendTimes
	self.m_ItemList = {}
	self.m_Cost = 0
	for index, data in pairs(lastItemList) do
		local itemId = data.itemId
		local num = data.num
		local mallData = Mall_LingYuMall.GetData(itemId)
		local t = {itemId = itemId, num = num, price = 0}
		if mallData then
			t.price = mallData.Jade * num
			self.m_Cost = self.m_Cost + mallData.Jade * num
		end
		table.insert(self.m_ItemList,t)
	end
	self.QnCostAndOwnMoney:SetCost(self.m_Cost)
	self.QnIncreseAndDecreaseButton:SetValue(#lastItemList ~= 0 and #lastItemList or 4, true)
end
--@region UIEvent

function LuaZongMenHongBaoSetWnd:OnSortBtnSelected(selected)
	CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	Extensions.SetLocalRotationZ(self.SortSprite.transform, selected and 180 or 0)
	if not selected then return end
	local t = {}
	local item = PopupMenuItemData(LocalString.GetString("自定义"),DelegateFactory.Action_int(function (idx)
		self.SortBtn.Text = LocalString.GetString("自定义")
		self:OnChooseSortClicked(0)
	end),false,nil)
	table.insert(t, item)
	for index,data in pairs(self.m_PresetData) do
        item = PopupMenuItemData(data.Name,DelegateFactory.Action_int(function (idx)
            self.SortBtn.Text = data.Name
            self:OnChooseSortClicked(index)
        end),false,nil)
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.SortBtn.m_Label.transform, AlignType.Left,1,DelegateFactory.Action(function()
        Extensions.SetLocalRotationZ(self.SortSprite.transform, 0)
    end),600,true,400)
end

function LuaZongMenHongBaoSetWnd:OnChooseSortClicked(index)
	CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	local data = self.m_PresetData[index]
	self.m_ItemList = {}
	self.m_Cost = 0
	if data then
		for i = 0,data.Items.Length - 1 do
			local itemData = data.Items[i]
			if itemData.Length == 2 then
				local itemId = itemData[0]
				local num = itemData[1]
				local mallData = Mall_LingYuMall.GetData(itemId)
				local t = {itemId = itemId, num = num, price = 0}
				if mallData then
					t.price = mallData.Jade * num
					self.m_Cost = self.m_Cost + mallData.Jade * num
				end
				table.insert(self.m_ItemList,t)
			end
		end
	end
	self.QnCostAndOwnMoney:SetCost(self.m_Cost)
	self.QnIncreseAndDecreaseButton:SetValue(data and data.Items.Length or self.m_MinCount, true)
end

function LuaZongMenHongBaoSetWnd:OnSubmitButtonClick()
	CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	local str = self.StatusText.value
	if System.String.IsNullOrEmpty(str) then
        str = LocalString.GetString("恭喜发财")
    end
	for i = 1, self.m_ItemCount do
		local data = self.m_ItemList[i]
		if not data then
			g_MessageMgr:ShowMessage("ZongMenHongBaoSetWnd_Need_FillItem")
			return
		end
	end
	local text = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(str, true)
	if text == nil then return end
	if not self.QnCostAndOwnMoney.moneyEnough and CClientMainPlayer.Inst  then
		local qnPoint = CClientMainPlayer.Inst.QnPoint
		local txt = g_MessageMgr:FormatMessage("NotEnough_Jade", qnPoint)
		MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
			CShopMallMgr.ShowChargeWnd()
		end), nil, LocalString.GetString("充值"), LocalString.GetString("取消"), false)
		return
	end
	
	if self.m_RemainJadeLimit and self.m_RemainJadeLimit < self.m_Cost then
		g_MessageMgr:ShowMessage("ZongMenHongBaoSetWnd_RemainJadeLimit_NotEnough")
        return
	end
	if self.m_RemainSendTimes and self.m_RemainSendTimes < 1 then
		g_MessageMgr:ShowMessage("ZongMenHongBaoSetWnd_RemainSendTimes_NotEnough")
        return
	end
	if self.m_Cost > 10000 or self.m_Cost < 100 then
		g_MessageMgr:ShowMessage("ZongMenHongBaoSetWnd_Cost_Error")
        return
	end
	local msg = g_MessageMgr:FormatMessage("ZongMenHongBaoSetWnd_SendHongBao_Confirm",self.m_Cost)
	local list = CreateFromClass(MakeGenericClass(List, Object))
    for i = 1, self.m_ItemCount do
		local data = self.m_ItemList[i]
		if data then
			CommonDefs.ListAdd_LuaCall(list, data.itemId)
			CommonDefs.ListAdd_LuaCall(list, data.num)
		end
	end
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		Gac2Gas.RequestSendSectItemRedPack(text,MsgPackImpl.pack(list))
		CUIManager.CloseUI(CLuaUIResources.ZongMenHongBaoSetWnd)
	end),nil,nil,nil,false)
end

function LuaZongMenHongBaoSetWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ZongMenHongBaoSetWnd_ReadMe")
end

function LuaZongMenHongBaoSetWnd:OnNumChanged(v)
	if v < self.m_MinCount then
		return
	end
	self.m_ItemCount = v
	self.m_ItemPosList = {}
	local data = SectItemRedPack_Position.GetData(v)
	if data then
		for i = 0,data.ItemsPos.Length - 1 do
			local itemPosData = data.ItemsPos[i]
			if itemPosData.Length == 2 then
				table.insert(self.m_ItemPosList,{x = itemPosData[0], y = itemPosData[1]})
			end
		end
	end
	Extensions.RemoveAllChildren(self.ItemsView.transform)
	for i = 1, v do
		local obj = NGUITools.AddChild(self.ItemsView.gameObject, self.ItemTemplate.gameObject)
		self:InitItem(obj,i)
	end
	self.m_RadioBox = self.ItemsView.transform:GetComponent(typeof(QnRadioBox))
	if not self.m_RadioBox then
		self.m_RadioBox = self.ItemsView.gameObject:AddComponent(typeof(QnRadioBox))
	end
	self.m_RadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), self.ItemsView.transform.childCount)
	for i = 0, self.ItemsView.transform.childCount - 1 do
        local go = self.ItemsView.transform:GetChild(i).gameObject
        self.m_RadioBox.m_RadioButtons[i] = go:GetComponent(typeof(QnSelectableButton))
        self.m_RadioBox.m_RadioButtons[i].OnClick = MakeDelegateFromCSFunction(self.m_RadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_RadioBox)
    end
	self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnBtnSelected(btn,index)
    end)
    self.m_RadioBox:ChangeTo(LuaZongMenMgr.m_CurSelectHongBaoItemIndex - 1, false)
	self.m_Cost = 0
	for i = 1, self.m_MaxCount do
		data = self.m_ItemList[i]
		if data and i <= v then
			self.m_Cost = self.m_Cost + data.price
		elseif data and i > v then
			self.m_ItemList[i] = nil
		end
	end
	self.QnCostAndOwnMoney:SetCost(self.m_Cost)
end

function LuaZongMenHongBaoSetWnd:OnClearButtonClick()
	self.m_ItemList = {}
	self.m_Cost = 0
	for i = 1, self.m_ItemCount do 
		self.m_ItemList[i] = nil
	end
	self.QnIncreseAndDecreaseButton:SetValue(self.m_ItemCount, true)
	self.QnCostAndOwnMoney:SetCost(self.m_Cost)
	CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
end
--@endregion UIEvent

function LuaZongMenHongBaoSetWnd:InitItem(obj, index)
	obj.gameObject:SetActive(true)
	local data = self.m_ItemList[index]
	local itemId = data and data.itemId or 0
	local posData = self.m_ItemPosList[index]
	
	local itemData = Item_Item.GetData(itemId)

	local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local item = obj.transform:Find("ItemCell")
	local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local addSprite = item.transform:Find("AddSprite")
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	if posData then
		obj.transform.localPosition = Vector3(posData.x, posData.y, 0)
	end
	nameLabel.text = itemData and itemData.Name or LocalString.GetString("添加道具")
	numLabel.text = data and data.num or ""
	if itemData then
		iconTexture:LoadMaterial(itemData.Icon)
		qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemId))
	end
	addSprite.gameObject:SetActive(not itemData)
	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    if itemData then
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
		end
	end)
	UIEventListener.Get(addSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaZongMenMgr.m_HongBaoItemList = self.m_ItemList
		LuaZongMenMgr.m_HongBaoItemCount = self.m_ItemCount
		LuaZongMenMgr.m_CurSelectHongBaoItemIndex = index
		CUIManager.ShowUI(CLuaUIResources.SectHongBaoItemSelectWnd)
		CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	end)
end

function LuaZongMenHongBaoSetWnd:OnBtnSelected(btn,index)
	LuaZongMenMgr.m_HongBaoItemList = self.m_ItemList
	LuaZongMenMgr.m_HongBaoItemCount = self.m_ItemCount
	LuaZongMenMgr.m_CurSelectHongBaoItemPos = btn.transform.position
	local item = LuaZongMenMgr.m_HongBaoItemList[index + 1]
	LuaZongMenMgr.m_CurSelectHongBaoItemIndex = index + 1
	if item then
		CUIManager.CloseUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
		CUIManager.ShowUI(CLuaUIResources.ChangeZongMenHongBaoItemWnd)
	end
end
