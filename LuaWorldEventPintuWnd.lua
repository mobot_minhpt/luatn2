require("common/common_include")

local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"
local CUIFx = import "L10.UI.CUIFx"
local UITexture = import "UITexture"
local Vector3 = import "UnityEngine.Vector3"
local Screen = import "UnityEngine.Screen"
local ShiJieShiJian_KaiSuo = import "L10.Game.ShiJieShiJian_KaiSuo"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local Physics = import "UnityEngine.Physics"
local MessageWndManager = import "L10.UI.MessageWndManager"
local TweenPosition = import "TweenPosition"
local TweenAlpha = import "TweenAlpha"
local Time = import "UnityEngine.Time"

LuaWorldEventPintuWnd = class()

RegistClassMember(LuaWorldEventPintuWnd,"CloseBtn")
RegistClassMember(LuaWorldEventPintuWnd,"Template")
RegistClassMember(LuaWorldEventPintuWnd,"PlaceNode")
RegistClassMember(LuaWorldEventPintuWnd,"DragNode")
RegistClassMember(LuaWorldEventPintuWnd,"Bg")
RegistClassMember(LuaWorldEventPintuWnd,"TimeLabel")
RegistClassMember(LuaWorldEventPintuWnd,"LeftNode")
RegistClassMember(LuaWorldEventPintuWnd,"RightNode")
RegistClassMember(LuaWorldEventPintuWnd,"FxNode")
RegistClassMember(LuaWorldEventPintuWnd,"TipNode")

function LuaWorldEventPintuWnd:InitPlaceNode()

end

local dataTable = {}
local chooseTable = {}

local moveNode = nil
local chooseIndex = 0
local restTime = -1000

function LuaWorldEventPintuWnd:StartDrag()

end

function LuaWorldEventPintuWnd:EndDrag()
  if moveNode then
    moveNode.localPosition = Vector3.zero
  end
  moveNode = nil
  chooseIndex = 0
end

function LuaWorldEventPintuWnd:SecondAni()
  self.PlaceNode:SetActive(false)
  self.TimeLabel.parent.gameObject:SetActive(false)
  self.TipNode:SetActive(false)
  self.DragNode:SetActive(false)
  self.FxNode:GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_yaoshikaisuo02.prefab")

  RegisterTickWithDuration(function ()
    local tween1 = TweenPosition.Begin(self.LeftNode,0.35,Vector3(-1000,self.LeftNode.transform.localPosition.y,0))
    TweenAlpha.Begin(self.LeftNode,0.35,0)
    local tween2 = TweenPosition.Begin(self.RightNode,0.35,Vector3(1000,self.RightNode.transform.localPosition.y,0))
    TweenAlpha.Begin(self.RightNode,0.35,0)
    local tweenFinish = function()
      Gac2Gas.FinishUnlockWithKeyTask(LuaWorldEventMgr.PintuTaskId,true)
      CUIManager.CloseUI(CUIRes.WorldEventPintuWnd)
    end

    CommonDefs.AddEventDelegate(tween2.onFinished, DelegateFactory.Action(tweenFinish))
  end,500,500)
end

function LuaWorldEventPintuWnd:StartEndAni()
  local count = self.PlaceNode.transform.childCount
  for i=1,count do
    local node = self.PlaceNode.transform:Find(i)
    local tween = TweenPosition.Begin(node:GetChild(0).gameObject,0.35,Vector3.zero)
    TweenAlpha.Begin(node.gameObject,0.35,0)
    if i == 1 then
      local tweenFinish = function()
        self:SecondAni()
      end

      CommonDefs.AddEventDelegate(tween.onFinished, DelegateFactory.Action(tweenFinish))
    end
  end
end

function LuaWorldEventPintuWnd:JudgePut(putNode,dragNode)
  local desNum = tonumber(putNode.name)
  if not desNum then
    return
  end

  local targetNum = tonumber(dragNode.name)
  if not targetNum then
    return
  end

  if desNum * 100 ~= targetNum then
    return
  end

  dragNode:SetActive(false)
  local lockSprite = putNode.transform:Find("Sprite")
  if lockSprite then
    lockSprite:GetComponent(typeof(UITexture)).color = Color.green
    local fxNode = putNode.transform:Find("FxNode")
    local fxNodeCUIFx = fxNode:GetComponent(typeof(CUIFx))
    fxNodeCUIFx:LoadFx("fx/ui/prefab/UI_yaoshikaisuo01.prefab")
  end
  if #chooseTable <= 1 then
    chooseTable = {}
    self:StartEndAni()
  else
    for i,v in pairs(chooseTable) do
      if v == desNum then
        table.remove(chooseTable,i)
      end
    end
  end
end


function LuaWorldEventPintuWnd:GenerateDragNode(dragNode)
  local onDrag = function(go,delta)
    if not moveNode then
      moveNode = go.transform:Find("node")
    end
    local _trans = moveNode
    local currentPos = UICamera.currentTouch.pos
    _trans.position = UICamera.currentCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))
  end

  local onPress = function(go,flag)
    if flag then
      local startNum = tonumber(go.transform.name)
      if startNum then
        chooseIndex = startNum
        self:StartDrag()
      end
    else
      local currentPos = UICamera.currentTouch.pos
      local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
      local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

      for i =0,hits.Length - 1 do
        if hits[i].collider.gameObject.transform.parent then
          self:JudgePut(hits[i].collider.gameObject,go)
        end
      end
      self:EndDrag()
    end
  end
  CommonDefs.AddOnDragListener(dragNode,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
  CommonDefs.AddOnPressListener(dragNode,DelegateFactory.Action_GameObject_bool(onPress),false)
end

function LuaWorldEventPintuWnd:InitDragNode()
  dataTable = {}
  Extensions.RemoveAllChildren(self.DragNode.transform)
  local count = ShiJieShiJian_KaiSuo.GetDataCount()
  for i=1,count do
    local data = ShiJieShiJian_KaiSuo.GetData(i)
    if data then
      table.insert(dataTable,{data.KeyID,data.KeyIcon,data.LockIcon})
      local go = CUICommonDef.AddChild(self.DragNode,self.Template)
      go:SetActive(true)
      go.transform.localPosition = Vector3(LuaWorldEventMgr.PintuDis * ((count - 1)/ 2 + (i - count)),0,0)
      go.transform:Find("node"):GetComponent(typeof(CUITexture)):LoadMaterial(data.KeyIcon,false,nil)
      go.name = data.KeyID * 100
      self:GenerateDragNode(go)
    end
  end
end

function LuaWorldEventPintuWnd:InitPutNode()
  local count = ShiJieShiJian_KaiSuo.GetDataCount()
  local numTable = {}
  for i=1,count do
    local data = ShiJieShiJian_KaiSuo.GetData(i)
    if data and data.status == 1 then
      table.insert(numTable,i)
    end
  end
  chooseTable = {}
  for i=1,5 do
    local chooseNum = table.remove(numTable,math.random(1, #numTable))
    local data = ShiJieShiJian_KaiSuo.GetData(chooseNum)
    table.insert(chooseTable,chooseNum)
    local putNode = self.PlaceNode.transform:Find(tostring(i))
    if putNode then
      local node = putNode.transform:Find("node")
      node.name = tostring(chooseNum)
      --node:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = data.LockIcon
      local cuitexture = node:Find("Sprite").gameObject:AddComponent(typeof(CUITexture))
      cuitexture:LoadMaterial("ui/texture/transparent/material/"..data.LockIcon..".mat")
    end
  end
end

local TimeLabelComponent = nil

function LuaWorldEventPintuWnd:Update()
  if not TimeLabelComponent then
    return
  end

  if restTime > 0 then
    restTime = restTime - Time.deltaTime
    TimeLabelComponent.text = math.floor(restTime)
  else
    TimeLabelComponent.text = "0"
    TimeLabelComponent = nil
  end
end

function LuaWorldEventPintuWnd:Init()
  moveNode = nil
  chooseIndex = 0

	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2",LocalString.GetString("确认退出吗?")), DelegateFactory.Action(function ()
      Gac2Gas.FinishUnlockWithKeyTask(LuaWorldEventMgr.PintuTaskId,false)
      CUIManager.CloseUI(CUIRes.WorldEventPintuWnd)
    end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
  if 1920 / Screen.width > 1080 / Screen.height then
    self.Bg.width = 1920--Screen.width
    self.Bg.height = Screen.height * 1920 / Screen.width--Screen.height
  else
    self.Bg.width = Screen.width * 1080 / Screen.height--Screen.width
    self.Bg.height = 1080--Screen.height
  end

  self.Template:SetActive(false)
  self:InitPutNode()
  self:InitDragNode()
  self:InitPlaceNode()

  restTime = LuaWorldEventMgr.PintuTime
  TimeLabelComponent = self.TimeLabel:GetComponent(typeof(UILabel))
end

return LuaWorldEventPintuWnd
