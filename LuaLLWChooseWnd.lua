require("common/common_include")
--require("design/GamePlay/LiaoLuoWan")
require("3rdParty/ScriptEvent")
local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Color = import "UnityEngine.Color"
local LocalString = import "LocalString"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLiaoLuoWanMgr = import "L10.Game.CLiaoLuoWanMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"

CLuaLLWChooseWnd=class()
RegistClassMember(CLuaLLWChooseWnd, "m_PortraitList")
RegistClassMember(CLuaLLWChooseWnd, "m_NameList")
RegistClassMember(CLuaLLWChooseWnd, "m_SelectedSprite")
RegistClassMember(CLuaLLWChooseWnd, "m_ItemList")
RegistClassMember(CLuaLLWChooseWnd, "m_NameOriginalColor")
RegistClassMember(CLuaLLWChooseWnd, "m_SelectedPos")

function CLuaLLWChooseWnd:Init()
    self.m_SelectedPos = -1
    local g = LuaGameObject.GetChildNoGC(self.transform, "Confirm").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        -- TODO: 提示
        if self.m_SelectedPos >= 0 then
            if CClientMainPlayer.Inst then
                Gac2Gas.LLW_RequestLiaoluowanBoardWarship(CClientMainPlayer.Inst.Id, self.m_SelectedPos, " ")
            end
        end
    end)
    self.m_ItemList = {}
    self.m_SelectedSprite = {}
    self.m_NameList = {}
    self.m_PortraitList = {}
    local index2SeatPos = {0, 2, 3, 4, 1}
    for i = 0, 4 do
        local item = LuaGameObject.GetChildNoGC(self.transform, "Position"..tostring(i)).gameObject
        table.insert(self.m_ItemList, item)
        local selectedObj = LuaGameObject.GetChildNoGC(item.transform, "Selected").gameObject
        UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(obj)
            for i, val in pairs(self.m_ItemList) do
                if obj == val then
                    self.m_SelectedPos = index2SeatPos[i]
                    self.m_SelectedSprite[i]:SetActive(true)
                else
                    self.m_SelectedSprite[i]:SetActive(false)
                end
            end
        end)
        selectedObj:SetActive(false)
        table.insert(self.m_SelectedSprite, selectedObj)
        table.insert(self.m_NameList, LuaGameObject.GetChildNoGC(item.transform, "Name").label)
        if i == 0 then
            self.m_NameOriginalColor = self.m_NameList[1].color
        end
        self.m_NameList[i + 1].text = LocalString.GetString("空缺")
        self.m_NameList[i + 1].color = Color.gray
        table.insert(self.m_PortraitList, LuaGameObject.GetChildNoGC(item.transform, "Portrait").cTexture)
        LuaGameObject.GetChildNoGC(item.transform, "Type").label.text = LiaoLuoWan_VehiclePosition.GetData(i + 1).Name
        LuaGameObject.GetChildNoGC(item.transform, "Function").label.text = LiaoLuoWan_VehiclePosition.GetData(i + 1).Function

        -- initialize skill
        local skillId = {}
        local index = 1
        for id in string.gmatch(LiaoLuoWan_VehiclePosition.GetData(i + 1).Skill, "([^;]+)") do
            skillId[index] = id
            index = index + 1
        end
        local skillCnt = i == 0 and 3 or 2
        for j = 1, skillCnt do
            local skillObj = LuaGameObject.GetChildNoGC(item.transform, "Skill"..tostring(j)).gameObject
            local id = skillId[j]
            UIEventListener.Get(skillObj).onClick = DelegateFactory.VoidDelegate(function(go)
                CSkillInfoMgr.ShowSkillInfoWnd(id, skillObj.transform.position, EnumSkillInfoContext.Default, true, 0, 0, nil)
            end)
            local skill = LuaGameObject.GetChildNoGC(skillObj.transform, "Texture").cTexture
            skill:LoadSkillIcon(Skill_AllSkills.GetData(skillId[j]).SkillIcon)
        end
    end
    self:SyncSeatInfo()
end

function CLuaLLWChooseWnd:OnEnable()
    g_ScriptEvent:AddListener("LLWSyncSeatInfo", self, "SyncSeatInfo")
end

function CLuaLLWChooseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("LLWSyncSeatInfo", self, "SyncSeatInfo")
end

function CLuaLLWChooseWnd:InitSeatInfo(seatIndex, seatInfo)
    if seatInfo.m_Id <= 0 then
            self.m_NameList[seatIndex].text = LocalString.GetString("空缺")
            self.m_NameList[seatIndex].color = Color.gray
            self.m_PortraitList[seatIndex]:Clear()
            return
    end
    self.m_NameList[seatIndex].text = seatInfo.m_Name
    if CClientMainPlayer.Inst and seatInfo.m_Id == CClientMainPlayer.Inst.Id then
        self.m_NameList[seatIndex].color = Color.green
    else
        self.m_NameList[seatIndex].color = self.m_NameOriginalColor
    end
    self.m_PortraitList[seatIndex]:LoadNPCPortrait(CUICommonDef.GetPortraitName(seatInfo.m_Class, seatInfo.m_Gender, -1), false)
end

function CLuaLLWChooseWnd:SyncSeatInfo()
    local index2SeatPos = {1, 5, 2, 3, 4}
    if CLiaoLuoWanMgr.Inst.m_SeatInfo then
        for i = 0, 4 do
            self:InitSeatInfo(index2SeatPos[i + 1], CLiaoLuoWanMgr.Inst.m_SeatInfo[i])
        end
    end
end

return CLuaLLWChooseWnd
