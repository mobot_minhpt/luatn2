--import
local GameObject			= import "UnityEngine.GameObject"
local CChargeGuideCtrl      = import "L10.UI.CChargeGuideCtrl"
local QnRadioBox            = import "L10.UI.QnRadioBox"
local QnButton              = import "L10.UI.QnButton"

local Object                = import "System.Object"
local CommonDefs            = import "L10.Game.CommonDefs"
local CWebBrowserMgr        = import "L10.Game.CWebBrowserMgr"
local UIEventListener       = import "UIEventListener"
local DelegateFactory       = import "DelegateFactory"

--define
CLuaChargeGuideWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaChargeGuideWnd, "m_CloseButton",		GameObject)
RegistChildComponent(CLuaChargeGuideWnd, "m_CardCtrl",		    CChargeGuideCtrl)
RegistChildComponent(CLuaChargeGuideWnd, "m_ACtrl",		        CChargeGuideCtrl)
RegistChildComponent(CLuaChargeGuideWnd, "m_WCtrl",		        CChargeGuideCtrl)
RegistChildComponent(CLuaChargeGuideWnd, "m_RadioBox",		    QnRadioBox)
RegistChildComponent(CLuaChargeGuideWnd, "m_TipButton",		    QnButton)

--RegistClassMember
RegistClassMember(CLuaChargeGuideWnd, "m_ChargeGuideLogType")
RegistClassMember(CLuaChargeGuideWnd,"m_Wnd")
--@region flow function

function CLuaChargeGuideWnd:Init()
    self.m_ChargeGuideLogType = 0
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    UIEventListener.Get(self.m_CloseButton).onClick = LuaUtils.VoidDelegate(function()
        self:OnClose()
    end);
    self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectRadioBox(index)
    end)

    UIEventListener.Get(self.m_TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickTipButton()
    end)
    
    if CommonDefs.IS_VN_CLIENT or CommonDefs.IS_HMT_CLIENT then
        self.m_RadioBox:SetVisibleGroup(Table2ArrayWithCount({0}, 1, MakeArrayClass(System.Int32)), false)
        self.m_TipButton.transform.parent.gameObject:SetActive(false);
    end
end

function CLuaChargeGuideWnd:OnEnable()
end

function CLuaChargeGuideWnd:OnDisable()
end

--@endregion

function CLuaChargeGuideWnd:OnClickTipButton()
    CWebBrowserMgr.Inst:OpenUrl("https://m.ds.163.com/article/6228729fc3e1e5000101627f/");
    self:Log2Server(4);
end

function CLuaChargeGuideWnd:OnSelectRadioBox(index)
    self.m_ChargeGuideLogType = index;
    self.m_CardCtrl.gameObject:SetActive(index == 0)
    self.m_ACtrl.gameObject:SetActive(index == 1)
    self.m_WCtrl.gameObject:SetActive(index == 2)
end

function CLuaChargeGuideWnd:OnClose()
    self:Log2Server(self.m_ChargeGuideLogType)
    self.m_Wnd:Close()
end

function CLuaChargeGuideWnd:Log2Server(type)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
    CommonDefs.DictAdd(dict, typeof(String), "chage_guide_type", typeof(Object), type)
    Gac2Gas.RequestRecordClientLog("Charge_Guide",MsgPackImpl.pack(dict))
end