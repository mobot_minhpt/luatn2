local NativeTools = import "L10.Engine.NativeTools"

LuaCameraMgr = class()

LuaCameraMgr.m_Enable = true
LuaCameraMgr.m_DefaultCamIndex = 0
LuaCameraMgr.m_TargetFPS = 60
LuaCameraMgr.m_ResolutionCoef = 1
LuaCameraMgr.m_TopTipMessage = ""
LuaCameraMgr.m_CenterTipName = ""
LuaCameraMgr.m_NeedSave = false
LuaCameraMgr.m_Callback = nil

function LuaCameraMgr:OpenCameraWnd(defaultCamIndex, targetFPS, resolutionCoef, topTipMessage, centerTipName, needSave, callback)
    if NativeTools.IsSupportRequestPermission() then
        if not NativeTools.HasUserAuthorizedPermission(NativeTools.PermissionNames.CAMERA) then
            NativeTools.RequestUserPermissionOrPromptIfDenied(NativeTools.PermissionNames.CAMERA, nil);
            return
        end
    end

    self.m_DefaultCamIndex = defaultCamIndex
    self.m_TargetFPS = targetFPS
    self.m_ResolutionCoef = resolutionCoef
    self.m_TopTipMessage = topTipMessage
    self.m_CenterTipName = centerTipName
    self.m_NeedSave = needSave
    self.m_Callback = callback
    CUIManager.ShowUI(CLuaUIResources.CameraWnd)
end

function LuaCameraMgr:IsCameraValided()
    return self.m_Enable and self:IsPlatformSupported()
end

function LuaCameraMgr:IsPlatformSupported()
    return CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform() 
end