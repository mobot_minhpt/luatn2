function CCityReplaceHelper:Ctor(serverType, replaceFunctionTbl, postActionFunctionTbl)
    self.m_ServerType = serverType

    self.m_ReplaceFunctionTbl = replaceFunctionTbl
    if not self.m_ReplaceFunctionTbl then
        self:LogError("CityReplace.NoReplaceFunctionTbl")
    end

    self.m_PostActionFunctionTbl = postActionFunctionTbl
    if not self.m_PostActionFunctionTbl then
        self:LogError("CityReplace.NoPostActionTbl")
    end

    self.m_BackupStateTbl = {}

    self.m_FieldReplaceValueFormatter = {
        Task_Task = {
            BeginNPC = tonumber,
            SubmitNPC = tonumber,
            Status = tonumber,
            ShowTimeoutTips = tonumber,
        },
        PublicMap_PublicMap = {
            View3D = tonumber,
        }
    }
end

function CCityReplaceHelper:Start()
    self:BackupAll()

    self:ReplaceAll()

    self:RunPostActions()
end

function CCityReplaceHelper:Stop()
    self:ResotreAll()

    self:RunPostActions(true)
end

--#region 加载替换项

function CCityReplaceHelper:ForeachInDesignTable(table, func)
    if self.m_ServerType == "gac" then
        table.Foreach(func)
    else
        for k, v in pairs(table or {}) do
            func(k, v)
        end
    end
end

function CCityReplaceHelper:LoadAllReplaces()
    self.m_AllReplaces = {}

    self:ForeachInDesignTable(CityReplaceHelper_MiscReplace, function(k, v)
        self:LoadMiscReplacesById(k, v)
    end)

    self:ForeachInDesignTable(CityReplaceHelper_PublicmapReplace, function(k, v)
        self:LoadMapReplaceById(k, v)
    end)

    self:ForeachInDesignTable(CityReplaceHelper_TaskReplace, function(k, v)
        self:LoadTaskReplaceById(k, v)
    end)

    self:ForeachInDesignTable(CityReplaceHelper_MailContentReplace, function(k, v)
        self:LoadMailContentReplaceById(k, v)
    end)
end

function CCityReplaceHelper:LoadMiscReplacesById(id, designData)
    if not (id and designData) then return end

    local data = {
        TableName = designData.TableName,
        Key = designData.Key,
        Status = designData.ReplaceStatus,
        FieldInfo = {},
    }
    
    local fieldInfo = {
        FieldName = designData.FieldName, ReplaceValue = designData.ReplaceValue
    }

    if self.m_FieldReplaceValueFormatter[data.TableName] and self.m_FieldReplaceValueFormatter[data.TableName][data.FieldName] then
        fieldInfo.ReplaceValue = self.m_FieldReplaceValueFormatter[data.TableName][data.FieldName](fieldInfo.ReplaceValue)
    end

    table.insert(data.FieldInfo, fieldInfo)

    self:DoAddReplace(id, data)
end

function CCityReplaceHelper:LoadTaskReplaceById(id, designData)
    if not (id and designData) then return end

    local data = {
        TableName = "Task_Task",
        Key = designData.ID,
        Status = designData.ReplaceStatus,
        FieldInfo = {},
    }

    for k, v in pairs(designData) do
        if k ~= "ID" and k ~= "ReplaceStatus" and v and v ~= "" then
            local fieldInfo = {
                FieldName = k,
                ReplaceValue = v,
            }
            if self.m_FieldReplaceValueFormatter[data.TableName] and self.m_FieldReplaceValueFormatter[data.TableName][k] then
                fieldInfo.ReplaceValue = self.m_FieldReplaceValueFormatter[data.TableName][k](fieldInfo.ReplaceValue)
            end
            table.insert(data.FieldInfo, fieldInfo)
        end
    end

    self:DoAddReplace(id, data)
end

function CCityReplaceHelper:LoadMapReplaceById(id, designData)
    if not (id and designData) then return end

    local data = {
        TableName = "PublicMap_PublicMap",
        Key = designData.ID,
        Status = designData.ReplaceStatus,
        FieldInfo = {},
    }

    for k, v in pairs(designData) do
        if k ~= "ID" and k ~= "ReplaceStatus" and v and v ~= "" then
            local fieldInfo = {
                FieldName = k,
                ReplaceValue = v,
            }
            if self.m_FieldReplaceValueFormatter[data.TableName] and self.m_FieldReplaceValueFormatter[data.TableName][k] then
                fieldInfo.ReplaceValue = self.m_FieldReplaceValueFormatter[data.TableName][k](fieldInfo.ReplaceValue)
            end
            table.insert(data.FieldInfo, fieldInfo)
        end
    end

    self:DoAddReplace(id, data)
end

function CCityReplaceHelper:LoadMailContentReplaceById(id, designData)
    if not (id and designData) then return end

    local data = {
        TableName = designData.TableName,
        Key = designData.Key,
        Status = designData.ReplaceStatus,
        FieldInfo = {},
    }
    
    local fieldInfo = {
        FieldName = designData.FieldName, ReplaceValue = designData.ReplaceValue
    }

    if self.m_FieldReplaceValueFormatter[data.TableName] and self.m_FieldReplaceValueFormatter[data.TableName][data.FieldName] then
        fieldInfo.ReplaceValue = self.m_FieldReplaceValueFormatter[data.TableName][data.FieldName](fieldInfo.ReplaceValue)
    end

    table.insert(data.FieldInfo, fieldInfo)

    self:DoAddReplace(id, data)
end

function CCityReplaceHelper:DoAddReplace(id, data)
    if self.m_AllReplaces[id] then
        self:LogMessage(string.format("CityReplace.DuplicateReplace %s", id))
    end
    self.m_AllReplaces[id] = data
end


function CCityReplaceHelper:GetFieldIdentity(tablename, field)
    return table.concat({tablename, field}, "_")
end

function CCityReplaceHelper:GetFieldReplaceType(fid)
    if CityReplaceHelperFieldDefine[fid] and CityReplaceHelperFieldDefine[fid]["ReplaceFunctions"] then
        return CityReplaceHelperFieldDefine[fid]["ReplaceFunctions"][self.m_ServerType]
    end

    if not CityReplaceHelperFieldDefine[fid] then
        self:LogError(string.format("CityReplace.NotSupport %s.", fid))
    end
end

function CCityReplaceHelper:ShouldReplaceField(fid)
    return self:GetFieldReplaceType(fid) ~= nil
end

function CCityReplaceHelper:GetFieldPostActions(fid)
    if CityReplaceHelperFieldDefine[fid] and CityReplaceHelperFieldDefine[fid]["PostActions"] then
        return CityReplaceHelperFieldDefine[fid]["PostActions"][self.m_ServerType]
    end
end

function CCityReplaceHelper:GetClientName(tablename, fieldInfo)
    local fid = self:GetFieldIdentity(tablename, fieldInfo.FieldName)
    if CityReplaceHelperFieldDefine[fid] and CityReplaceHelperFieldDefine[fid].ClientAliasName then
        return CityReplaceHelperFieldDefine[fid].ClientAliasName
    else
        return fieldInfo.FieldName
    end
end

--#endregion

--#region 备份相关字段的原始状态

function CCityReplaceHelper:BackupAll()

    -- 客户端不作策划数据备份，使用getrawdata来获取原始的策划数据值。
    if self.m_ServerType == "gac" then return end

    for id, _ in pairs(self.m_AllReplaces) do
        SAFE_CALL(self.BackupById, self, id)
    end
end

-- 注意备份函数的浅拷贝问题，如果替换函数直接修改，还原的时候会失效。
-- 现在的策划表Patch之后再取数据的话拿到的都是同一份数据。
function CCityReplaceHelper:BackupById(id)

    -- 客户端不作策划数据备份，使用getrawdata来获取原始的策划数据值。
    if self.m_ServerType == "gac" then return end

    local designData = self.m_AllReplaces[id]
    if not (id and designData) then return end

    local tablename, key = designData.TableName, designData.Key
    for _, fieldInfo in pairs(designData.FieldInfo) do
        local fid = self:GetFieldIdentity(tablename, fieldInfo.FieldName)
        if self:ShouldReplaceField(fid) and not self:GetBackupState(tablename, key, fieldInfo.FieldName) then
            local func = self:GetBackupFunction(fid)
            if func then
                local state = SAFE_CALL(func, self, id, tablename, key, fieldInfo)
                self:SaveBackupState(tablename, key, fieldInfo.FieldName, state)
            else
                self:LogError(string.format("CityReplace.NoBackupFunc %s", fid))
            end
        end
    end
end

function CCityReplaceHelper:SaveBackupState(tablename, key, field, data)
    -- 客户端不作策划数据备份，使用getrawdata来获取原始的策划数据值。
    if self.m_ServerType == "gac" then return end

    local name = table.concat({tablename, key or "", field}, "_")
    self.m_BackupStateTbl[name] = data
end

function CCityReplaceHelper:GetBackupState(tablename, key, field)

    -- 客户端不作策划数据备份，使用getrawdata来获取原始的策划数据值。
    if self.m_ServerType == "gac" then
        self:LogError("GetBackupState was not suppirted on gac")
        return
    end

    local name = table.concat({tablename, key or "", field}, "_")
    return self.m_BackupStateTbl[name]
end

--#endregion

--#region 替换字段
function CCityReplaceHelper:ReplaceAll()
    for id, info in pairs(self.m_AllReplaces) do
        SAFE_CALL(self.ReplaceById, self, id)
    end
end

function CCityReplaceHelper:ReplaceById(id)
    local designData = self.m_AllReplaces[id]
    if not (id and designData) then return end

    -- 判断一下开关
    if designData.Status ~= 0 then return end

    local tablename, key = designData.TableName, designData.Key
    for _, fieldInfo in pairs(designData.FieldInfo) do
        local fid = self:GetFieldIdentity(tablename, fieldInfo.FieldName)
        if  self:ShouldReplaceField(fid) then
            local func = self:GetReplaceFunction(fid)
            if func then
                SAFE_CALL(func, self, id, tablename, key, fieldInfo)
            else
                self:LogError(string.format("CityReplace.NoReplaceFunc %s", fid))
            end
        end
    end
end
--#endregion

--#region PostActions

function CCityReplaceHelper:RunPostActions(bIsRestore)
    local postActionTable = {}
    for id, designData in pairs(self.m_AllReplaces) do
        if designData.Status == 0 then
            local tablename, key = designData.TableName, designData.Key
            for _, fieldInfo in pairs(designData.FieldInfo) do
                local fid = self:GetFieldIdentity(tablename, fieldInfo.FieldName)
                local actions = self:GetFieldPostActions(fid)
                for _, actionName in pairs(actions or {}) do
                    postActionTable[actionName] = postActionTable[actionName] or {}
                    local infoClone = {FieldName = fieldInfo.FieldName, ReplaceValue = fieldInfo.ReplaceValue}
                    if bIsRestore then
                        if self.m_ServerType == "gac" then
                            infoClone.ReplaceValue = self.m_ReplaceFunctionTbl.GetOriginalData(tablename, key, fieldInfo.FieldName)
                        else
                            infoClone.ReplaceValue = self:GetBackupState(tablename, key, fieldInfo.FieldName)
                        end
                    end
                    table.insert(postActionTable[actionName], {id, tablename, key, infoClone})
                end
            end
        end
    end

    self:RunPostActionTbl(postActionTable)
end

function CCityReplaceHelper:DoPostActionByName(actionname, replaces)
    local func = self.m_PostActionFunctionTbl[actionname]
    if func then
        func(replaces)
        self:LogMessage(string.format("CityReplace.PostActionDone %s", actionname))
    else
        self:LogError("CityReplace.NoPostAction " .. actionname)
    end
end

function CCityReplaceHelper:RunPostActionById(id, bIsRestore)
    local designData = self.m_AllReplaces[id]
    if not (id and designData) then return end

    -- 判断一下开关
    if designData.Status ~= 0 then return end

    local postActionTable = {}

    local tablename, key = designData.TableName, designData.Key
    for _, fieldInfo in pairs(designData.FieldInfo) do
        local fid = self:GetFieldIdentity(tablename, fieldInfo.FieldName)
        local actions = self:GetFieldPostActions(fid)
        for _, actionName in pairs(actions or {}) do
            postActionTable[actionName] = postActionTable[actionName] or {}
            local infoClone = {FieldName = fieldInfo.FieldName, ReplaceValue = fieldInfo.ReplaceValue}
            if bIsRestore then
                if self.m_ServerType == "gac" then
                    infoClone.ReplaceValue = self.m_ReplaceFunctionTbl.GetOriginalData(tablename, key, fieldInfo.FieldName)
                else
                    infoClone.ReplaceValue = self:GetBackupState(tablename, key, fieldInfo.FieldName)
                end
            end
            table.insert(postActionTable[actionName], {id, tablename, key, infoClone})
        end
    end

    self:RunPostActionTbl(postActionTable)
end

function CCityReplaceHelper:RunPostActionTbl(postActionTable)
    local sortedActionList = {}
    for actionName, _ in pairs(postActionTable) do
        table.insert(sortedActionList, actionName)
    end

    table.sort(sortedActionList, function(a, b)
        local av = CityReplaceHelperPostActionOrder[a] or 0
        local bv = CityReplaceHelperPostActionOrder[b] or 0
        return av < bv
    end)

    for _, actionName in pairs(sortedActionList) do
        SAFE_CALL(self.DoPostActionByName, self, actionName, postActionTable[actionName])
    end
end

--#endregion

--#region 恢复字段

function CCityReplaceHelper:ResotreAll()
    for id, _ in pairs(self.m_AllReplaces) do
        SAFE_CALL(self.RestoreById, self, id)
    end
end

function CCityReplaceHelper:RestoreById(id)
    local designData = self.m_AllReplaces[id]
    if not (id and designData) then return end

    local tablename, key = designData.TableName, designData.Key
    for _, fieldInfo in pairs(designData.FieldInfo) do
        local fid = self:GetFieldIdentity(tablename, fieldInfo.FieldName)
        if self:ShouldReplaceField(fid) then
            local func = self:GetRestoreFunction(fid)
            if func then
                SAFE_CALL(func, self, id, tablename, key, fieldInfo)
            else
                self:LogError(string.format("CityReplace.NoRestoreFunc %s", fid))
            end
        end
    end
end

--#endregion

--#region 使用到的处理函数

function CCityReplaceHelper:GetSpecialFunctionSuffix(fid)
    local replaceType = self:GetFieldReplaceType(fid)

    if replaceType == EnumCityReplaceHelperReplaceType.eSpecialReplace then
        return fid
    end
    
    return replaceType
end

function CCityReplaceHelper:GetBackupFunction(fid)
    local suffixName = self:GetSpecialFunctionSuffix(fid)
    local funcName = table.concat({"Backup", suffixName}, "_")
    return self.m_ReplaceFunctionTbl[funcName]
end

function CCityReplaceHelper:GetReplaceFunction(fid)
    local suffixName = self:GetSpecialFunctionSuffix(fid)
    local funcName = table.concat({"Replace", suffixName}, "_")
    return self.m_ReplaceFunctionTbl[funcName]
end

function CCityReplaceHelper:GetRestoreFunction(fid)
    local suffixName = self:GetSpecialFunctionSuffix(fid)
    local funcName = table.concat({"Restore", suffixName}, "_")
    return self.m_ReplaceFunctionTbl[funcName]
end


--#endregion

--#region 工具函数

function CCityReplaceHelper:LogMessage(str)
    if self.m_ServerType == "gac" then
        local log = import "L10.CLogMgr"
        log.Log(str)
        return
    end
    LOG_INFO(OTHER, "%s", str)
end

function CCityReplaceHelper:LogError(str)
    if self.m_ServerType == "gac" then
        local log = import "L10.CLogMgr"
        log.LogError(str)
        return
    end
    LOG_ERR(OTHER, "%s", str)
end

--#endregion

function CCityReplaceHelper:StartSingleById(id, bNotRunPost)
    if not self.m_AllReplaces[id] then return end

    self:BackupById(id)
    self:ReplaceById(id)
    if not bNotRunPost then
        self:RunPostActionById(id)
    end
end

function CCityReplaceHelper:StopSingleById(id, bNotRunPost)
    if not self.m_AllReplaces[id] then return end

    self:RestoreById(id)
    if not bNotRunPost then
        self:RunPostActionById(id, true)
    end
end
