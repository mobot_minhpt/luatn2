require("common/common_include")

local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local NGUITools = import "NGUITools"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture = import "L10.UI.CUITexture"
local CScene = import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Utility = import "L10.Engine.Utility"
local UISprite = import "UISprite"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CMapPathDrawer = import "L10.UI.CMapPathDrawer"
local CTrackMgr = import "L10.Game.CTrackMgr"
local Input = import "UnityEngine.Input"
local CPos = import "L10.Engine.CPos"
local CMiniMapRoutingWnd = import "L10.UI.CMiniMapRoutingWnd"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local AlignType = import "L10.UI.CTooltip.AlignType"
local WndType = import "L10.UI.WndType"
local NGUIMath = import "NGUIMath"
local NGUIText = import "NGUIText"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"

local CCityWarMgr = import "L10.Game.CCityWarMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CUIResources = import "L10.UI.CUIResources"
local Color = import "UnityEngine.Color"

local LuaTweenUtils = import "LuaTweenUtils"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Extensions = import "Extensions"
local Ease = import "DG.Tweening.Ease"

CLuaCityWarCopySecondaryMapWnd = class()
CLuaCityWarCopySecondaryMapWnd.Path = "ui/citywar/LuaCityWarCopySecondaryMapWnd"

RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SecondMainPlayerTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_OtherPlayerTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_RelivePointTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SecondMapRootObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_LeftMapId")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_RightMapId")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_UIMapWidth")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_PathDrawer")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SecondDestTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_InfoButtonObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_LeftMapBgObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_RightMapBgObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ConstructBtn")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_LastSelectedNpc")

RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_LeftBottomMapPos")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ActualMapWidth")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdUIMapWidth")

-- Three-Level Map
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SecondRootObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdRootObj")
-- For Tween Animation
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SecondWidget")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdWidget")

RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_bThirdMap")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SwitchBtnSprite")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdCityId2ObjTable")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdMapRootObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThumbnailTrans")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdMainPlayerTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdDestTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdEnemyPlayerTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdPathDrawer")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SecondGateTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdGateTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdRelivePointTemplateObj")

RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_DragStartTime")

function CLuaCityWarCopySecondaryMapWnd:Init()
  if #CLuaCityWarMgr.WarForceInfo < 2 then
    CUIManager.CloseUI(CLuaUIResources.CityWarCopySecondaryMapWnd)
    CUIManager.CloseUI(CUIResources.CityWarCopySecondaryMapWnd)
    return
  end
end

function CLuaCityWarCopySecondaryMapWnd:Awake( ... )
    if #CLuaCityWarMgr.WarForceInfo < 2 then return end

    if CScene.MainScene then
	    local resNameTable = {[1]="kfzcshamo", [2]="kfzclvdi", [3]="kfzcxue", [4]="kfzczhaoze", [0]="kfzcjydt"}
        local resName = ""
        if CLuaCityWarMgr:IsInWatchScene() then
            local index = CScene.MainScene.SceneTemplateId - 16000200
            if index == 5 then index = 0 end
            resName = resNameTable[index]
        elseif CCityWarMgr.Inst:IsInMirrorWarScene() then
            resName = resNameTable[CityWar_Map.GetData(CScene.MainScene.SceneTemplateId - 100526).Zone]
        end
    	self.transform:Find("BG"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/NonTransparent/Material/"..resName..".mat")
    end

    local a, b = string.match(CityWar_Setting.GetData().MapAnglePos, "(%d+),%d+;(%d+),%d+;")
    self.m_LeftBottomMapPos = a
    self.m_ActualMapWidth = b - a
    self.m_ThirdUIMapWidth = tonumber(CityWar_Setting.GetData().ThirdMapSize)


    Gac2Gas.QueryMMapPosInfo()

    self.m_ConstructBtn = self.transform:Find("BG/ConstructButton").gameObject
    self.m_SecondMainPlayerTemplateObj = self.transform:Find("BG/Second/Root/MainPlayer").gameObject
    self.m_OtherPlayerTemplateObj = self.transform:Find("BG/OtherPlayer").gameObject
    self.m_RelivePointTemplateObj = self.transform:Find("BG/RelivePoint").gameObject
    self.m_SecondMainPlayerTemplateObj:SetActive(false)
    self.m_OtherPlayerTemplateObj:SetActive(false)
    self.m_RelivePointTemplateObj:SetActive(false)
    self.m_SecondMapRootObj = self.transform:Find("BG/Second/Root").gameObject
    self.m_PathDrawer = self.m_SecondMapRootObj.transform:Find("MapPathDrawer"):GetComponent(typeof(CMapPathDrawer))
    self.m_SecondDestTemplateObj = self.transform:Find("BG/Second/Root/Dest").gameObject
    self.m_SecondDestTemplateObj:SetActive(false)
    --self.m_UIMapWidth = self.transform:Find("BG/FieldBg"):GetComponent(typeof(UISprite)).height

    UIEventListener.Get(self.m_ConstructBtn).onClick = LuaUtils.VoidDelegate(function ( ... )

      if CLuaCityWarMgr:IsInWatchScene() then
  			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("观战中无法进行此操作"))
  			return
  		end

		CCityWarMgr.Inst.PlaceMode = true

		CameraFollow.Inst:SetMaximumPinchIn(false,false)

		local excepts = {"MiddleNoticeCenter","GuideWnd", "PlaceCityUnitWnd"}
		local List_String = MakeGenericClass(List,cs_string)
		CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
		CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)
		CUIManager.ShowUI(CLuaUIResources.PlaceCityUnitWnd)

		local bIsInCopyScene = CCityWarMgr.Inst:IsInCityWarCopyScene()
		if bIsInCopyScene then
			Gac2Gas.CheckMyRights("RebornPoint", 0)
		else
			Gac2Gas.CheckMyRights("CityBuild", 0)
		end

		CUIManager.CloseUI(CLuaUIResources.CityWarCopySecondaryMapWnd)
		CUIManager.CloseUI(CUIResources.CityWarCopySecondaryMapWnd)
	end)
	UIEventListener.Get(self.transform:Find("ButtonRoot/PrimaryMapBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CLuaCityWarMgr:OpenPrimaryMap()
	end)
	UIEventListener.Get(self.transform:Find("ButtonRoot/SecondaryMapBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		if CLuaCityWarMgr:IsInWatchScene() then return end
		if CCityWarMgr.Inst:IsInMirrorWarScene() then
	        CLuaCityWarMgr:OpenSecondaryMap(CScene.MainScene.SceneTemplateId - 100526)
    	end
	end)
	UIEventListener.Get(self.transform:Find("ButtonRoot/BackCityBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestBackToOwnCity()
	end)

	UIEventListener.Get(self.transform:Find("BG/FieldBg").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBgClick(...)
	end)

--[[	self.m_LeftMapBgObj = self.transform:Find("BG/FieldBg/Bg").gameObject
	UIEventListener.Get(self.m_LeftMapBgObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBgClick(true)
	end)
	self.m_RightMapBgObj = self.transform:Find("BG/FieldBg/Bg1").gameObject
	UIEventListener.Get(self.m_RightMapBgObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:OnBgClick(false)
	end)--]]

	self:InitTitle()
	self:InitCity()
	self:InitMainPlayer()
	self:InitButtons()
	self:InitNpcInfo()
	self:InitPlaySwitch()

	self:InitThirdMap()
	self:InitThirdMapMainPlayer()

	self:InitGate()
	self:InitRelivePoint()

  self:InitVehicle()
end

function CLuaCityWarCopySecondaryMapWnd:InitPlaySwitch( ... )
	local num = CLuaCityWarMgr:GetCityWarCopyNum(CLuaCityWarMgr.WarForceInfo[1].MapId, CLuaCityWarMgr.WarForceInfo[2].MapId)
	local rootObj = self.transform:Find("BG/PlayRoot").gameObject
	if num <= 1 then rootObj:SetActive(false) return end
	local transNameTbl = {"First", "Second", "Third"}
	local indexNameTbl = {LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三")}
	for i = 1, #transNameTbl do
		local obj = rootObj.transform:Find(transNameTbl[i]).gameObject
		obj:SetActive(i <= num)
		if i <= num then
			obj.transform:Find("CurSceneFlag").gameObject:SetActive(i == CLuaCityWarMgr.WarIndex)
			obj.transform:Find("OtherSceneFlag").gameObject:SetActive(i ~= CLuaCityWarMgr.WarIndex)
			UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function ( ... )
				g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("是否确定立即传送至战场")..indexNameTbl[i], function ( ... )
          if CLuaCityWarMgr:IsInWatchScene() then
            Gac2Gas.RequestWatchMirrorWar(CLuaCityWarMgr.WatchPlayIds[i])
          else
            Gac2Gas.RequestEnterMirrorWarPlay(i)
          end
					end, nil, nil, nil, false)
			end)
		end
	end
end

function CLuaCityWarCopySecondaryMapWnd:InitCity( ... )
  self.m_CityId2PositonTable = {}

	local cityTemplateObj = self.transform:Find("BG/Second/City").gameObject
	cityTemplateObj:SetActive(false)
    --Resize city template
    local width = CLuaCityWarSecondaryMapWnd.CityWidth / self.m_ActualMapWidth * 1000
    local bg1 = cityTemplateObj:GetComponent(typeof(UISprite))
    local bg2 = cityTemplateObj.transform:Find("bg"):GetComponent(typeof(UISprite))
    bg1.width = width
    bg1.height = width
    bg2.width = width
    bg2.height = width

    local sprite1 = cityTemplateObj.transform:Find("MajorFlag/ForceSprite2"):GetComponent(typeof(UISprite))
    sprite1.width = width
    sprite1.height = width
    width = width + 35
    local sprite2 = cityTemplateObj.transform:Find("MajorFlag/ForceSprite1"):GetComponent(typeof(UISprite))
    sprite2.width = width
    sprite2.height = width

    local lefttop = Vector3(-width / 2, width / 2, 0)
	local fightingTrans = cityTemplateObj.transform:Find("MajorFlag/MyGuildFlag")
	fightingTrans.localPosition = lefttop

	--for i = 1, 2 do
		local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
		local mapTemplateId = CLuaCityWarMgr.WarForceInfo[1].MapId
		CityWar_MapCity.Foreach(function(k, v)
			if v.MapID == 16000159 then
				local x, y, w, l = string.match(v.Rect, "(%d+),(%d+),(%d+),(%d+)")
				local obj = NGUITools.AddChild(self.m_SecondMapRootObj, cityTemplateObj)
				obj:SetActive(true)

				local isMajorCity = k == 1004 or k == 1006
				obj.transform:Find("MajorFlag").gameObject:SetActive(isMajorCity)
				if isMajorCity then
					local colorCode = k == 1004 and "6592FF" or "D96153"
					for i = 1, 2 do
						obj.transform:Find("MajorFlag/ForceSprite"..i):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24(colorCode, 0)
					end
					obj.transform:Find("MajorFlag/MyGuildFlag").gameObject:SetActive((not CLuaCityWarMgr:IsInWatchScene()) and ((k == 1004 and myForce == 1) or (k == 1006 and myForce == 0)))
				end
				obj.transform.localPosition = self:GetSecondMapUIPos(x + (w - x) / 2, y + (l - y) / 2)
				obj.transform:Find("ComponentFlag").gameObject:SetActive(k < 1004 or k > 1006)

				self.m_CityId2PositonTable[k] = obj.transform.position
			end
		end)
	--end

  self:InitSecondLine()
end

function CLuaCityWarCopySecondaryMapWnd:InitSecondLine()
  local lineObj = self.transform:Find("BG/Line").gameObject
  lineObj:SetActive(false)

  if CLuaCityWarMgr:IsInWatchScene() then return end
  lineObj:GetComponent(typeof(UISprite)).width = 300

  local sourceCityTbl = {1003, 1005, 1009}
  local rotationTbl = {270, 0, 90}
  local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
  if myForce == 0 then
    sourceCityTbl = {1001, 1005, 1007}
    rotationTbl = {270, 180, 90}
  end
  for i = 1, 3 do
    local obj = NGUITools.AddChild(self.m_SecondMapRootObj, lineObj)
    obj:SetActive(true)
    obj.transform.position = self.m_CityId2PositonTable[sourceCityTbl[i]]
    Extensions.SetLocalRotationZ(obj.transform, rotationTbl[i])
  end
end

function CLuaCityWarCopySecondaryMapWnd:InitGate( ... )
	local gateTemplateObj = self.transform:Find("BG/Second/Gate").gameObject
	gateTemplateObj:SetActive(false)

  if CLuaCityWarMgr:IsInWatchScene() then return end

	if not CLuaCityWarMgr:IsOwnCurrentCity() then return end
    CommonDefs.DictIterate(CCityWarMgr.Inst.UnitList, DelegateFactory.Action_object_object(function (___key, ___value)
		local unitData = ___value
        if CityWar_Unit.GetData(unitData.TemplateId).Type == 3 then
        	local gateObj = NGUITools.AddChild(self.m_SecondMapRootObj, gateTemplateObj)
        	gateObj:SetActive(true)
        	gateObj.transform.localPosition = self:GetSecondMapUIPos(unitData.ServerPos.x, unitData.ServerPos.z)
        	Extensions.SetLocalRotationZ(gateObj.transform, unitData.ServerRotateY + 90)

        	-- Three-Level Map
        	local thirdObj = NGUITools.AddChild(self.m_ThirdMapRootObj, self.m_ThirdGateTemplateObj)
        	thirdObj:SetActive(true)
        	thirdObj.transform.localPosition = self:GetThirdMapUIPos(unitData.ServerPos.x, unitData.ServerPos.z)
        	Extensions.SetLocalRotationZ(thirdObj.transform, unitData.ServerRotateY + 90)
        end
	end))
end

function CLuaCityWarCopySecondaryMapWnd:InitButtons( ... )
	self.m_InfoButtonObj = self.transform:Find("ButtonRoot/InfoButton").gameObject
	UIEventListener.Get(self.m_InfoButtonObj).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnInfoBtnClick(go)
	end)
	self:OnInfoBtnClick(nil)
	UIEventListener.Get(self.transform:Find("ButtonRoot/RoutingButton").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CUIManager.ShowUI(CUIResources.MiniMapRoutingWnd)
		CMiniMapRoutingWnd.RouteTo = DelegateFactory.Func_int_int_bool(function (x, y)
			self:Track(CPos(x, y))
			return true
		end)
	end)
end

function CLuaCityWarCopySecondaryMapWnd:OnInfoBtnClick(go)
	local contentColor = 4294967295
    --白色
    if CScene.MainScene ~= nil then
        local data = PublicMap_PublicMap.GetData(CScene.MainScene.SceneTemplateId)
        local tips = CreateFromClass(MakeGenericClass(List, String))
        if data ~= nil then
            if data.AllowPK then
                if CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst) then
                    contentColor = 4294967295
                    CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
                else
                    CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("可进行PK战斗"))
                    contentColor = 4278190335
                    --红色
                    if data.EnablePK then
                        CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("恶意杀人增加PK值"))
                        local default
                        if data.IsDeathPunish then
                            default = LocalString.GetString("死亡有惩罚")
                        else
                            default = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(tips, typeof(String), default)
                    else
                        CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("恶意杀人不增加PK值"))
                        local extern
                        if data.IsDeathPunish then
                            extern = LocalString.GetString("死亡有惩罚")
                        else
                            extern = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(tips, typeof(String), extern)
                    end
                    local ref
                    if data.CanDuoHun then
                        ref = LocalString.GetString("可夺魂")
                    else
                        ref = LocalString.GetString("不可夺魂")
                    end
                    CommonDefs.ListAdd(tips, typeof(String), ref)
                end
            else
                contentColor = 4294967295
                --白色
                CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
            end
        end
        if (data.DecPK and not CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst)) or CScene.MainScene.SceneTemplateId == 16100021 then
            CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("在此区域活动可减少PK值"))
        else
            CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("此区域活动无法减少PK值"))
        end
        CommonDefs.GetComponent_Component_Type(self.m_InfoButtonObj.transform:GetChild(0), typeof(UISprite)).color = NGUIMath.HexToColor(contentColor)
        --只有点击真正点击调用的才弹出窗口
        if go ~= nil then
            CMessageTipMgr.Inst:Display(WndType.Tooltip, true, LocalString.GetString("当前场景须知"), tips, 400, go.transform, AlignType.Right, contentColor)
        end
    end
end

function CLuaCityWarCopySecondaryMapWnd:InitTitle( ... )
	local mapId1, mapId2 = CLuaCityWarMgr.WarForceInfo[1].MapId, CLuaCityWarMgr.WarForceInfo[2].MapId
	--self.m_LeftMapId = (mapId1 == CityWar_Map.GetData(mapId2).WestMap or mapId1 == CityWar_Map.GetData(mapId2).NorthMap) and mapId1 or mapId2
	local map2 = CityWar_Map.GetData(mapId2)
	if mapId1 == map2.WestMap or mapId1 == map2.NorthMap then
		self.m_LeftMapId, self.m_RightMapId = mapId1, mapId2
	else
		self.m_LeftMapId, self.m_RightMapId = mapId2, mapId1
	end

	local titleTrans = self.transform:Find("BG/Title")
	local guildName1Label = titleTrans:Find("GuildName1"):GetComponent(typeof(UILabel))
	guildName1Label.text = self.m_LeftMapId == mapId1 and CLuaCityWarMgr.WarForceInfo[1].GuildName or CLuaCityWarMgr.WarForceInfo[2].GuildName
	local guildName2Label = titleTrans:Find("GuildName2"):GetComponent(typeof(UILabel))
	guildName2Label.text = self.m_LeftMapId == mapId2 and CLuaCityWarMgr.WarForceInfo[1].GuildName or CLuaCityWarMgr.WarForceInfo[2].GuildName

	local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
	local isLeft = (self.m_LeftMapId == mapId1 and CLuaCityWarMgr.WarForceInfo[1].Force == myForce) or (self.m_LeftMapId == mapId2 and CLuaCityWarMgr.WarForceInfo[2].Force == myForce)
	--titleTrans:Find("MyGuildFlag1").gameObject:SetActive(isLeft)
	--titleTrans:Find("MyGuildFlag2").gameObject:SetActive(not isLeft)
	guildName1Label.color = CLuaCityWarMgr:IsInWatchScene() and Color.white or (isLeft and Color.green or Color.white)
	guildName2Label.color = CLuaCityWarMgr:IsInWatchScene() and Color.white or (isLeft and Color.white or Color.green)

	--local colorCodeTbl = {[1] = "f5c1705a", [2] = "6bdc455a", [3] = "61eeff5a", [4] = "a5b3ed5a", [0] = "febb3b5a"}
	--local objTbl = {self.m_LeftMapBgObj, self.m_RightMapBgObj}
	--local zoneTbl = {CityWar_Map.GetData(self.m_LeftMapId).Zone, CityWar_Map.GetData(self.m_RightMapId).Zone}
	--for i = 1, #objTbl do
	--	objTbl[i]:GetComponent(typeof(UISprite)).color = NGUIText.ParseColor32(colorCodeTbl[zoneTbl[i]], 0)
	--end
end

function CLuaCityWarCopySecondaryMapWnd:GetSecondMapUIPos(x, y)
	return Vector3((x - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * 1000, (y - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * 1000, 0)
end

function CLuaCityWarCopySecondaryMapWnd:InitRelivePoint( ... )
  if CLuaCityWarMgr:IsInWatchScene() then return end

	for _,v in ipairs(CLuaCityWarMgr.WarRebornPointInfo) do
		local obj = NGUITools.AddChild(self.m_SecondMapRootObj, self.m_RelivePointTemplateObj)
		obj:SetActive(true)
		obj.transform.localPosition = self:GetSecondMapUIPos(v.Posx, v.Posy)
		UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function (go)
			self:OnRelivePointClick(go, v.EngineId)
		end)


		--Three-Level Map
		obj = NGUITools.AddChild(self.m_ThirdMapRootObj, self.m_ThirdRelivePointTemplateObj)
		obj:SetActive(true)
		obj.transform.localPosition = self:GetThirdMapUIPos(v.Posx, v.Posy)
		UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function (go)
			self:OnRelivePointClick(go, v.EngineId)
		end)
	end
end

function CLuaCityWarCopySecondaryMapWnd:UpdateMapPath(playerMapPos)
	if Input.anyKey or (not CLuaCityWarMgr:CanTrack()) then
 		self.m_PathDrawer:ClearCurrentPath()
 		self.m_SecondDestTemplateObj:SetActive(false)
		return
	end

	local movePath = CClientMainPlayer.Inst.EngineObject.CurrentMovePath
    local wayPoint = CClientMainPlayer.Inst.EngineObject.WayPoint
    if movePath ~= nil then
        local listPos = CreateFromClass(MakeGenericClass(List, Vector3))
        do
            local i = wayPoint
            while i < movePath.PathSize do
                local tmp1 = i
                local tmp2 = 0
                local default
                default, tmp1, tmp2 = movePath:GetPixelPosAt(tmp1, tmp2)
                local pixelPos = default
                local gridPos = Utility.PixelPos2GridPos(pixelPos)
                local mapPos = self:GetSecondMapUIPos(gridPos.x, gridPos.y)
                CommonDefs.ListAdd(listPos, typeof(Vector3), mapPos)
                i = i + 1
            end
        end
 		self.m_SecondDestTemplateObj:SetActive(true)
        self.m_SecondDestTemplateObj.transform.localPosition = CLuaCityWarMgr.CopySceneDestinationPos
        CommonDefs.ListAdd(listPos, typeof(Vector3), CLuaCityWarMgr.CopySceneDestinationPos)
        self.m_PathDrawer:UpdatePlayerPath(listPos, playerMapPos)
 	else
 		self.m_SecondDestTemplateObj:SetActive(false)
    end
end

function CLuaCityWarCopySecondaryMapWnd:InitMainPlayer( ... )
	if CClientMainPlayer.Inst then
		local obj = self.m_SecondMainPlayerTemplateObj
		obj:SetActive(true)
		local gridPos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
		local playerMapPos = self:GetSecondMapUIPos(gridPos.x, gridPos.y)
		obj.transform.localPosition = playerMapPos
		local x, y = CClientMainPlayer.Inst.RO.transform.forward.x, CClientMainPlayer.Inst.RO.transform.forward.z

		local angle = math.atan2(y, x) * 180 / math.pi
		obj.transform.localEulerAngles = Vector3(0, 0, angle)
		self:UpdateMapPath(playerMapPos)
	end
end

function CLuaCityWarCopySecondaryMapWnd:InitNpcInfo()
	local markTemplateObj = self.transform:Find("BG/MarkTemplate").gameObject
	markTemplateObj:SetActive(false)

	local teleportTemplateObj = self.transform:Find("BG/TeleportTemplate").gameObject
	teleportTemplateObj:SetActive(false)
end

function CLuaCityWarCopySecondaryMapWnd:OnBgClick( ... )
	local worldPos = CUIManager.instance.MainCamera:ScreenToWorldPoint(Input.mousePosition)
	local deltaPos = self.m_SecondMapRootObj.transform:InverseTransformPoint(worldPos)
	local gridPos = CPos(deltaPos.x * self.m_ActualMapWidth / 1000 + self.m_LeftBottomMapPos, deltaPos.y * self.m_ActualMapWidth / 1000 + self.m_LeftBottomMapPos)
	self:Track(gridPos)
end


function CLuaCityWarCopySecondaryMapWnd:Track(gridPos)
	if not CLuaCityWarMgr:CanTrack() then return end
	if not CScene.MainScene then return end

	local pixelPos = Utility.GridPos2PixelPos(gridPos)
	CTrackMgr.Inst:Track(nil, CScene.MainScene.SceneTemplateId, pixelPos, Utility.Grid2Pixel(2.0), nil, nil, nil, nil, true)
	self.m_SecondDestTemplateObj:SetActive(true)
	CLuaCityWarMgr.CopySceneDestinationPos = self:GetSecondMapUIPos(gridPos.x, gridPos.y)
	self.m_SecondDestTemplateObj.transform.localPosition = CLuaCityWarMgr.CopySceneDestinationPos

	--Three-Level Map
	self.m_ThirdDestTemplateObj:SetActive(true)
	self.m_ThirdDestTemplateObj.transform.localPosition = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
end

function CLuaCityWarCopySecondaryMapWnd:OnRelivePointClick(go, engineId)
	Gac2Gas.RequestRebornInMirrorWarRebornPoint(engineId)
	CUIManager.CloseUI(CLuaUIResources.CityWarCopySecondaryMapWnd)
	CUIManager.CloseUI(CUIResources.CityWarCopySecondaryMapWnd)
end

function CLuaCityWarCopySecondaryMapWnd:UpdateObjPosInfo(argv)
  if CLuaCityWarMgr:IsInWatchScene() then return end

	local playerList = argv[0]
	local mainplayerEngineId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.EngineId or 0
	for i = 0, playerList.Count - 1, 6 do
		if playerList[i + 1] ~= mainplayerEngineId then
			local obj = NGUITools.AddChild(self.m_SecondMapRootObj, self.m_OtherPlayerTemplateObj)
			obj:SetActive(true)
			obj.transform.localPosition = self:GetSecondMapUIPos(playerList[i + 2], playerList[i + 3])

			--Three-Level Map
			local objj = NGUITools.AddChild(self.m_ThirdMapRootObj, self.m_OtherPlayerTemplateObj)
			objj:SetActive(true)
			objj.transform.localPosition = self:GetThirdMapUIPos(playerList[i + 2], playerList[i + 3])
		end
	end
end

function CLuaCityWarCopySecondaryMapWnd:MainPlayerMoveStepped( ... )
	self:InitMainPlayer()
	-- Three-Level Map
	self:InitThirdMapMainPlayer()
end

function CLuaCityWarCopySecondaryMapWnd:MainPlayerMoveEnded( ... )
	self.m_PathDrawer:ClearCurrentPath()
	self.m_SecondDestTemplateObj:SetActive(false)
	-- Three-Level Map
	self.m_ThirdPathDrawer:ClearCurrentPath()
 	self.m_ThirdDestTemplateObj:SetActive(false)
end

function CLuaCityWarCopySecondaryMapWnd:UpdateMirrorWarForceInfo( ... )
	self:InitPlaySwitch()
end

function CLuaCityWarCopySecondaryMapWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("UpdateObjPosInfo", self, "UpdateObjPosInfo")
	g_ScriptEvent:AddListener("MainPlayerMoveStepped", self, "MainPlayerMoveStepped")
	g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
	g_ScriptEvent:AddListener("UpdateMirrorWarForceInfo", self, "UpdateMirrorWarForceInfo")
  g_ScriptEvent:AddListener("UpdateMirrorWarForceMineAndVehicleInfo", self, "UpdateMirrorWarForceMineAndVehicleInfo")
  g_ScriptEvent:AddListener("EnterSummonMirrorWarMineVehicle", self, "EnterSummonMirrorWarMineVehicle")
  g_ScriptEvent:AddListener("SummonMirrorWarMineVehicleSuccess", self, "SummonMirrorWarMineVehicleSuccess")
	--g_ScriptEvent:AddListener("QueryTerritoryMapDetailResult", self, "QueryTerritoryMapDetailResult")
	--g_ScriptEvent:AddListener("QueryCityInfoResult", self, "QueryCityInfoResult")
end

function CLuaCityWarCopySecondaryMapWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("UpdateObjPosInfo", self, "UpdateObjPosInfo")
	g_ScriptEvent:RemoveListener("MainPlayerMoveStepped", self, "MainPlayerMoveStepped")
	g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "MainPlayerMoveEnded")
	g_ScriptEvent:RemoveListener("UpdateMirrorWarForceInfo", self, "UpdateMirrorWarForceInfo")
  g_ScriptEvent:RemoveListener("UpdateMirrorWarForceMineAndVehicleInfo", self, "UpdateMirrorWarForceMineAndVehicleInfo")
  g_ScriptEvent:RemoveListener("EnterSummonMirrorWarMineVehicle", self, "EnterSummonMirrorWarMineVehicle")
  g_ScriptEvent:RemoveListener("SummonMirrorWarMineVehicleSuccess", self, "SummonMirrorWarMineVehicleSuccess")
	--g_ScriptEvent:RemoveListener("QueryTerritoryMapDetailResult", self, "QueryTerritoryMapDetailResult")
	--g_ScriptEvent:RemoveListener("QueryCityInfoResult", self, "QueryCityInfoResult")
end


---------- Three-Level Map Begin
function CLuaCityWarCopySecondaryMapWnd:InitThirdMap( ... )
    self.m_ThirdMainPlayerTemplateObj = self.transform:Find("BG/Third/Map/Root/MainPlayer").gameObject
    self.m_ThirdDestTemplateObj = self.transform:Find("BG/Third/Map/Root/Dest").gameObject
    self.m_ThirdEnemyPlayerTemplateObj = self.transform:Find("BG/Third/Map/EnemyPlayer").gameObject
    self.m_ThirdDestTemplateObj:SetActive(false)
    self.m_ThirdMainPlayerTemplateObj:SetActive(false)
    self.m_ThirdEnemyPlayerTemplateObj:SetActive(false)
    self.m_ThirdPathDrawer = self.transform:Find("BG/Third/Map/Root/MapPathDrawer"):GetComponent(typeof(CMapPathDrawer))
    self.m_ThirdGateTemplateObj = self.transform:Find("BG/Third/Map/Gate").gameObject
    self.m_ThirdGateTemplateObj:SetActive(false)

	self.m_SecondRootObj = self.transform:Find("BG/Second").gameObject
	self.m_ThirdRootObj = self.transform:Find("BG/Third").gameObject
	self.m_SecondRootObj:SetActive(true)
	self.m_ThirdRootObj:SetActive(true)
	self.m_SecondWidget = self.m_SecondRootObj:GetComponent(typeof(UIWidget))
	self.m_ThirdWidget = self.m_ThirdRootObj:GetComponent(typeof(UIWidget))

	self.m_SwitchBtnSprite = self.transform:Find("BG/SwitchMapBtn"):GetComponent(typeof(UISprite))
	local transTbl = {"LeftArrow", "RightArrow", "UpArrow", "DownArrow"}
	local moveDeltaTbl = {{1, 0}, {-1, 0}, {0, -1}, {0, 1}}
	for i = 1, #transTbl do
		UIEventListener.Get(self.m_ThirdRootObj.transform:Find("Map/Operation/"..transTbl[i]).gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
			self:ThirdMapMove(moveDeltaTbl[i][1] * CLuaCityWarSecondaryMapWnd.ArrowMoveDelta, moveDeltaTbl[i][2] * CLuaCityWarSecondaryMapWnd.ArrowMoveDelta, CLuaCityWarSecondaryMapWnd.ArrowMoveTime)
		end)
	end

	local touchObj = self.m_ThirdRootObj.transform:Find("Map/TouchBg").gameObject
	UIEventListener.Get(touchObj).onDragStart = LuaUtils.VoidDelegate(function ( ... )
		self.m_DragStartTime = Time.realtimeSinceStartup
	end)
	UIEventListener.Get(touchObj).onDrag = LuaUtils.VectorDelegate(function (go, delta)
		if Time.realtimeSinceStartup - self.m_DragStartTime < CLuaCityWarSecondaryMapWnd.DragMinTime then return end
		self:ThirdMapMove(delta.x, delta.y, 0)
	end)
	UIEventListener.Get(self.m_ThirdRootObj.transform:Find("Map/TouchBg").gameObject).onClick = LuaUtils.VoidDelegate(function (...)
		self:ThirdMapClick()
	end)
	self.m_ThumbnailTrans = self.m_ThirdRootObj.transform:Find("Map/Thumbnail/CityRoot/Scope")

	self.m_bThirdMap = false
	self.m_SwitchBtnSprite.spriteName = self.m_bThirdMap and "common_icon_suoxiao" or "common_icon_fangda"

	UIEventListener.Get(self.m_SwitchBtnSprite.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:SwitchMap(not self.m_bThirdMap, true)
	end)
	self:SwitchMap(PlayerPrefs.GetInt(CLuaCityWarSecondaryMapWnd.PlayerPrefsKey, 0) > 0, false)

	UIEventListener.Get(self.transform:Find("BG/Third/LocateMeBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:LocateMe(CLuaCityWarSecondaryMapWnd.LocateMoveTime)
	end)

	local cityTemplateObj = self.transform:Find("BG/Third/Map/City").gameObject
	cityTemplateObj:SetActive(false)
    --Resize city template
	local width = CLuaCityWarSecondaryMapWnd.CityWidth / self.m_ActualMapWidth * self.m_ThirdUIMapWidth
    local bg1 = cityTemplateObj:GetComponent(typeof(UISprite))
    local bg2 = cityTemplateObj.transform:Find("Sprite"):GetComponent(typeof(UISprite))
    bg1.width = width
    bg1.height = width
    bg2.width = width
    bg2.height = width

    local sprite1 = cityTemplateObj.transform:Find("MajorFlag/ForceSprite1"):GetComponent(typeof(UISprite))
    sprite1.width = width
    sprite1.height = width
    width = width + 35
    local sprite2 = cityTemplateObj.transform:Find("MajorFlag/ForceSprite2"):GetComponent(typeof(UISprite))
    sprite2.width = width
    sprite2.height = width

    local lefttop = Vector3(-width / 2, width / 2, 0)
	local fightingTrans = cityTemplateObj.transform:Find("MajorFlag/MyGuildFlag")
	fightingTrans.localPosition = lefttop

	self.m_ThirdMapRootObj = self.transform:Find("BG/Third/Map/Root").gameObject

	self.m_ThirdRelivePointTemplateObj = self.transform:Find("BG/Third/Map/RelivePoint").gameObject
	self.m_ThirdRelivePointTemplateObj:SetActive(false)

	local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
	CityWar_MapCity.Foreach(function(k, v)
		if v.MapID == 16000159 then
			local x, y, w, l = string.match(v.Rect, "(%d+),(%d+),(%d+),(%d+)")
			local obj = NGUITools.AddChild(self.m_ThirdMapRootObj, cityTemplateObj)
			obj:SetActive(true)
			local isMajorCity = k == 1004 or k == 1006
			obj.transform:Find("MajorFlag").gameObject:SetActive(isMajorCity)
			if isMajorCity then
				local colorCode = k == 1004 and "6592FF" or "D96153"
				for i = 1, 2 do
					obj.transform:Find("MajorFlag/ForceSprite"..i):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24(colorCode, 0)
				end
				obj.transform:Find("MajorFlag/MyGuildFlag").gameObject:SetActive((k == 1004 and myForce == 1) or (k == 1006 and myForce == 0))
			end
			obj.transform.localPosition = self:GetThirdMapUIPos(x + (w - x) / 2, y + (l - y) / 2)
			obj.transform:Find("ComponentFlag").gameObject:SetActive(k < 1004 or k > 1006)
		end
	end)
	self:LocateMe(0)
end

function CLuaCityWarCopySecondaryMapWnd:TweenAlpha(widget, startVal, endVal, time)
	local tweener = LuaTweenUtils.TweenFloat(startVal, endVal, time, function ( val )
		widget.alpha = val
	end)
	LuaTweenUtils.SetEase(tweener, Ease.InOutQuint)
end

function CLuaCityWarCopySecondaryMapWnd:TweenScale(trans, startVal, endVal, time)
	local tweener = LuaTweenUtils.TweenScale(trans, startVal, endVal, time)
	LuaTweenUtils.SetEase(tweener, Ease.InOutQuint)
end

function CLuaCityWarCopySecondaryMapWnd:SwitchMap(bThirdMap, bAnimation)
	self.m_bThirdMap = bThirdMap
	local time = bAnimation and CLuaCityWarSecondaryMapWnd.WidgetTweenTime or 0

	self.m_ThirdRootObj:SetActive(bThirdMap)
	self.m_SecondRootObj:SetActive(not bThirdMap)

	if bThirdMap then
		self:TweenAlpha(self.m_ThirdWidget, 0, 1, time)
		self:TweenScale(self.m_ThirdWidget.transform, Vector3(0.8, 0.8, 1), Vector3(1, 1, 1), time)

		self:TweenAlpha(self.m_SecondWidget, 1, 0, time)
		self:TweenScale(self.m_SecondWidget.transform, Vector3(1, 1, 1), Vector3(1.2, 1.2, 1), time)
	else
		self:TweenAlpha(self.m_SecondWidget, 0, 1, time)
		self:TweenScale(self.m_SecondWidget.transform, Vector3(1.2, 1.2, 1), Vector3(1, 1, 1), time)

		self:TweenAlpha(self.m_ThirdWidget, 1, 0, time)
		self:TweenScale(self.m_ThirdWidget.transform, Vector3(1, 1, 1), Vector3(0.8, 0.8, 1), time)
	end

	self.m_SwitchBtnSprite.spriteName = bThirdMap and "common_icon_suoxiao" or "common_icon_fangda"
	PlayerPrefs.SetInt(CLuaCityWarSecondaryMapWnd.PlayerPrefsKey, bThirdMap and 1 or 0)
end

function CLuaCityWarCopySecondaryMapWnd:ThirdMapMove(deltaX, deltaY, time)
	local endX = self.m_ThirdMapRootObj.transform.localPosition.x + deltaX
	local endY = self.m_ThirdMapRootObj.transform.localPosition.y + deltaY
	endX = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, endX))
	endY = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, endY))
	LuaTweenUtils.TweenPosition(self.m_ThirdMapRootObj.transform, endX, endY, 0, time)
	self:UpdateThumbnail(endX, endY)
end

function CLuaCityWarCopySecondaryMapWnd:UpdateThumbnail(x, y)
	local xx = math.abs(x) * 0.04
	local yy = math.abs(y) * 0.04
	self.m_ThumbnailTrans.localPosition = Vector3(xx, yy, 0)
end

function CLuaCityWarCopySecondaryMapWnd:InitThirdMapMainPlayer( ... )
	if CClientMainPlayer.Inst then
		self.m_ThirdMainPlayerTemplateObj:SetActive(true)
		local gridPos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
		local playerMapPos = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
		self.m_ThirdMainPlayerTemplateObj.transform.localPosition = playerMapPos
		local x, y = CClientMainPlayer.Inst.RO.transform.forward.x, CClientMainPlayer.Inst.RO.transform.forward.z

		local angle = math.atan2(y, x) * 180 / math.pi
		self.m_ThirdMainPlayerTemplateObj.transform.localEulerAngles = Vector3(0, 0, angle)
		self:UpdateThirdMapPath(playerMapPos)
	end
end

function CLuaCityWarCopySecondaryMapWnd:UpdateThirdMapPath(playerMapPos)
	local anyKey = CommonDefs.DEVICE_PLATFORM == "pc" and Input.anyKey or false
	if anyKey or (not CLuaCityWarMgr:CanTrack()) then
 		self.m_ThirdPathDrawer:ClearCurrentPath()
 		self.m_ThirdDestTemplateObj:SetActive(false)
		return
	end
	local movePath = CClientMainPlayer.Inst.EngineObject.CurrentMovePath
    local wayPoint = CClientMainPlayer.Inst.EngineObject.WayPoint
    if movePath ~= nil then
        local listPos = CreateFromClass(MakeGenericClass(List, Vector3))
        do
            local i = wayPoint
            while i < movePath.PathSize do
                local tmp1 = i
                local tmp2 = 0
                local default
                default, tmp1, tmp2 = movePath:GetPixelPosAt(tmp1, tmp2)
                local pixelPos = default
                local gridPos = Utility.PixelPos2GridPos(pixelPos)
                local mapPos = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
                CommonDefs.ListAdd(listPos, typeof(Vector3), mapPos)
                i = i + 1
            end
        end
        local endPos = Utility.PixelPos2GridPos(CTrackMgr.Inst:GetCurSceneEndPos())
        self.m_ThirdDestTemplateObj:SetActive(true)
        local endPoss = self:GetThirdMapUIPos(endPos.x, endPos.y)
		self.m_ThirdDestTemplateObj.transform.localPosition = endPoss
        CommonDefs.ListAdd(listPos, typeof(Vector3), endPoss)
        self.m_ThirdPathDrawer:UpdatePlayerPath(listPos, playerMapPos)
 	else
 		self.m_ThirdDestTemplateObj:SetActive(false)
    end
end

function CLuaCityWarCopySecondaryMapWnd:LocateMe(time)
	local gridPos = CPos(400, 400)
	if CClientMainPlayer.Inst then
		gridPos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
	end
	local uiPos = self:GetThirdMapUIPos(gridPos.x, gridPos.y)
	local x = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, -1 * uiPos.x))
	local y = math.max(-self.m_ThirdUIMapWidth + 500, math.min(-500, -1 * uiPos.y))
	--self.m_ThirdMapRootObj.transform.localPosition = Vector3(x, y, 0)
	LuaTweenUtils.TweenPosition(self.m_ThirdMapRootObj.transform, x, y, 0, time)
	self:UpdateThumbnail(x, y)
end

function CLuaCityWarCopySecondaryMapWnd:ThirdMapClick()
	local worldPos = CUIManager.instance.MainCamera:ScreenToWorldPoint(Input.mousePosition)
	local deltaPos = self.m_ThirdMapRootObj.transform:InverseTransformPoint(worldPos)
	local gridPos = CPos(deltaPos.x * self.m_ActualMapWidth / self.m_ThirdUIMapWidth + self.m_LeftBottomMapPos, deltaPos.y * self.m_ActualMapWidth / self.m_ThirdUIMapWidth + self.m_LeftBottomMapPos)
	self:Track(gridPos)
end

function CLuaCityWarCopySecondaryMapWnd:QueryEnemyPlayerPosResult( dataUD )
	if not CScene.MainScene or CScene.MainScene.SceneTemplateId ~= CLuaCityWarMgr.CurrentMapId then return end

	local playerList = MsgPackImpl.unpack(dataUD)
	for i = 0, playerList.Count - 1, 4 do
		local obj = NGUITools.AddChild(self.m_ThirdMapRootObj, self.m_ThirdEnemyPlayerTemplateObj)
		obj:SetActive(true)
		obj.transform.localPosition = self:GetThirdMapUIPos(playerList[i + 2], playerList[i + 3])
	end
end

function CLuaCityWarCopySecondaryMapWnd:GetThirdMapUIPos(x, y)
	return Vector3((x - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * self.m_ThirdUIMapWidth, (y - self.m_LeftBottomMapPos) / self.m_ActualMapWidth * self.m_ThirdUIMapWidth, 0)
end
---------- Three-Level Map End


-- Mine and Vehicle
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SummonVehicleRootObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_LeftVehicleLabel")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ComponentNumLabel")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ExtraLeftVehicleLabel")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_CityId2PositonTable")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_CurrentSelectedCityObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_VehicleTemplateObj")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_SecondVehicleRootTrans")
RegistClassMember(CLuaCityWarCopySecondaryMapWnd, "m_ThirdVehicleRootTrans")

function CLuaCityWarCopySecondaryMapWnd:SetCitySelected(obj, bSelected)
  if self.m_CurrentSelectedCityObj then
    self.m_CurrentSelectedCityObj.transform:Find("Selected").gameObject:SetActive(false)
  end
  obj.transform:Find("Selected").gameObject:SetActive(bSelected)
  self.m_CurrentSelectedCityObj = obj
end

function CLuaCityWarCopySecondaryMapWnd:InitVehicle()
  self.m_VehicleTemplateObj = self.transform:Find("BG/VehicleTemplate").gameObject
  self.m_VehicleTemplateObj:SetActive(false)
  self.m_SecondVehicleRootTrans = self.m_SecondMapRootObj.transform:Find("VehicleRoot")
  self.m_ThirdVehicleRootTrans = self.m_ThirdMapRootObj.transform:Find("VehicleRoot")
  self.m_SummonVehicleRootObj = self.transform:Find("BG/AddCarRoot").gameObject
  self.m_SummonVehicleRootObj:SetActive(false)
  UIEventListener.Get(self.m_SummonVehicleRootObj.transform:Find("BackButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
    self.m_SummonVehicleRootObj:SetActive(false)
  end)
  self.m_ExtraLeftVehicleLabel = self.m_SummonVehicleRootObj.transform:Find("LeftNumLabel"):GetComponent(typeof(UILabel))

  UIEventListener.Get(self.transform:Find("BG/CallButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
    Gac2Gas.RequestEnterSummonMirrorWarMineVehicle()
  end)
  self.m_LeftVehicleLabel = self.transform:Find("BG/LeftCarRoot/Label"):GetComponent(typeof(UILabel))
  self.m_ComponentNumLabel = self.transform:Find("BG/ComponentRoot/Label"):GetComponent(typeof(UILabel))

  -- Init vehicle path and position
  local cityTemplateObj = self.m_SummonVehicleRootObj.transform:Find("City").gameObject
  cityTemplateObj:SetActive(false)
  local myForce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
  local posTbl = myForce == 1 and {1003, 1005, 1009} or {1001, 1005, 1007}
  for k, v in pairs(posTbl) do
    local obj = NGUITools.AddChild(self.m_SummonVehicleRootObj, cityTemplateObj)
    obj:SetActive(true)
    obj.transform.position = self.m_CityId2PositonTable[v]
    UIEventListener.Get(obj).onClick = LuaUtils.VoidDelegate(function()
      self:SetCitySelected(obj, true)
      self:OnSummonCityClick(k)
    end)
    self:SetCitySelected(obj, false)
  end
end

function CLuaCityWarCopySecondaryMapWnd:OnSummonCityClick(index)
  g_MessageMgr:ShowOkCancelMessage(LocalString.GetString("确定在该位置召唤攻城车吗？"), function()
    Gac2Gas.RequestSummonMirrorWarMineVehicle(index)
  end, nil, nil, nil, false)
end

function CLuaCityWarCopySecondaryMapWnd:UpdateMirrorWarForceMineAndVehicleInfo(mineNum, leftVehicleNum, data_U)
  self.m_ComponentNumLabel.text = LocalString.GetString("攻城车组件:")..mineNum
  self.m_LeftVehicleLabel.text = SafeStringFormat3(LocalString.GetString("可召唤攻城车:%d[c][ffffff](%d组件1辆)[-]"), leftVehicleNum, CityWar_MirrorWar.GetData().SummonMineVehicleCost)
  self.m_ExtraLeftVehicleLabel.text = LocalString.GetString("剩余召唤数量:")..leftVehicleNum

  Extensions.RemoveAllChildren(self.m_SecondVehicleRootTrans)
  Extensions.RemoveAllChildren(self.m_ThirdVehicleRootTrans)
	local list = MsgPackImpl.unpack(data_U)
	if not list then return end

	for i = 0, list.Count - 1, 6 do
		local engineId = list[i]
		local templateId = list[i + 1]
		local hp = list[i + 2]
		local hpFull = list[i + 3]
		local x = list[i + 4]
		local y = list[i + 5]

    local secondObj = NGUITools.AddChild(self.m_SecondVehicleRootTrans.gameObject, self.m_VehicleTemplateObj)
    secondObj:SetActive(true)
    secondObj.transform.localPosition = self:GetSecondMapUIPos(x, y)

    local thirdObj = NGUITools.AddChild(self.m_ThirdVehicleRootTrans.gameObject, self.m_VehicleTemplateObj)
    thirdObj:SetActive(true)
    thirdObj.transform.localPosition = self:GetThirdMapUIPos(x, y)
	end
end

function CLuaCityWarCopySecondaryMapWnd:EnterSummonMirrorWarMineVehicle()
  if self.m_bThirdMap then
    self:SwitchMap(not self.m_bThirdMap, true)
  end
  self.m_SummonVehicleRootObj:SetActive(true)
end

function CLuaCityWarCopySecondaryMapWnd:SummonMirrorWarMineVehicleSuccess(posIndex)
end
-- Mine and Vehicle End
