local CTrackMgr=import "L10.Game.CTrackMgr"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local QnButton=import "L10.UI.QnButton"
local CGuildMgr=import "L10.Game.CGuildMgr"
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"

CLuaGuildMainWndBenifitWnd = class()
RegistClassMember(CLuaGuildMainWndBenifitWnd,"m_TableView")
RegistClassMember(CLuaGuildMainWndBenifitWnd,"dataList")
RegistClassMember(CLuaGuildMainWndBenifitWnd,"sbDisableGuildBonusInCrossServer")
RegistClassMember(CLuaGuildMainWndBenifitWnd,"sbDisableGuildGongxunCrossServer")


function CLuaGuildMainWndBenifitWnd:Start( )
    self.sbDisableGuildBonusInCrossServer=true
    self.sbDisableGuildGongxunCrossServer=true
    self.dataList={}
    Guild_GuildBenifit.ForeachKey(function (key) 
        local val = Guild_GuildBenifit.GetData(key)
        if val.IsActive > 0 then
            if val.ID == 6 then
                if CAuctionMgr.IsOpen() then
                    table.insert( self.dataList,val )
                end
            else
                table.insert( self.dataList,val )
            end
        end
    end)

    self.m_TableView = self.transform:Find("QnTableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.dataList
        end,
        function(item, index)
            self:InitItem(item.transform,index)
        end)

    self.m_TableView:ReloadData(true, false)
end

function CLuaGuildMainWndBenifitWnd:OnEnable()
    g_ScriptEvent:AddListener("SendPaiMaiBonus", self, "OnSendPaiMaiBonus")
    g_ScriptEvent:AddListener("SyncSelfBuildBonus", self, "OnSyncSelfBuildBonus")

end
function CLuaGuildMainWndBenifitWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendPaiMaiBonus", self, "OnSendPaiMaiBonus")
    g_ScriptEvent:RemoveListener("SyncSelfBuildBonus", self, "OnSyncSelfBuildBonus")

end
function CLuaGuildMainWndBenifitWnd:OnSendPaiMaiBonus(bonus)
    for i,v in ipairs(self.dataList) do
        if v.ID == 6 then
            local tf=self.m_TableView:GetItemAtRow(i-1).transform
            local m_DespLabel = tf:Find("Label"):GetComponent(typeof(UILabel))
            m_DespLabel.text = System.String.Format(v.Description, bonus)
            break
        end
    end
end

function CLuaGuildMainWndBenifitWnd:OnSyncSelfBuildBonus(bonus, isThisWeekTaken, playStatus)
    for i,v in ipairs(self.dataList) do
        if v.ID == 3 then
            local tf=self.m_TableView:GetItemAtRow(i-1).transform
            self:InitBuildBonus(tf,bonus, isThisWeekTaken, playStatus)
            break
        end
    end
end
function CLuaGuildMainWndBenifitWnd:InitBuildBonus(tf,bonus, isThisWeekTaken, playStatus)
    local data = nil
    for i,v in ipairs(self.dataList) do
        if v.ID == 3 then
            data = v
            break
        end
    end
    local descLabel = tf:Find("Label"):GetComponent(typeof(UILabel))
    local message = g_MessageMgr:Format(data.Description, bonus)
    if isThisWeekTaken and playStatus~=2 then
        descLabel.text = SafeStringFormat3(LocalString.GetString("%s(已领取)"), message)
    else
        descLabel.text = message
    end
    local dot = tf:Find("Btn/Alert").gameObject
    dot:SetActive(CLuaRedDotForGuild.BonusAvaliable)
end

function CLuaGuildMainWndBenifitWnd:InitItem(transform,row)
    local data=self.dataList[row+1]
    local Id, name, desp, icon, buttonName = data.ID, data.Name, data.Description, data.Icon, data.ButtonName

    local m_TitleLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local m_DespLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    local m_Texture = transform:Find("ItemCell"):GetComponent(typeof(CUITexture))
    local m_Button = transform:Find("Btn"):GetComponent(typeof(QnButton))
    local m_Dot = transform:Find("Btn/Alert").gameObject
    m_Dot:SetActive(false)

    m_TitleLabel.text = name
    if Id == 5 then--EGuildBenifit.GuildGongXun
        m_DespLabel.text = g_MessageMgr:Format(desp, CClientMainPlayer.Inst == nil and 0 or CClientMainPlayer.Inst.PlayProp.GuildData.GuildGongXun)
    elseif Id == 6 then--EGuildBenifit.GuildAuctionBonus
        Gac2Gas.GetPaiMaiBonusValue()
        m_DespLabel.text = ""
    elseif Id == 3 then--EGuildBenifit.GuildBonus
        --初始化
        self:InitBuildBonus(transform, CLuaRedDotForGuild.Bonus, CLuaRedDotForGuild.IsThisWeekTaken, CLuaRedDotForGuild.Status)
    elseif Id == 2 then--EGuildBenifit.GuildLianShen
        m_DespLabel.text = g_MessageMgr:Format(desp, CClientMainPlayer.Inst == nil and 0 or CClientMainPlayer.Inst.PlayProp.MingWangData.Score)
    else
        m_DespLabel.text = desp
    end

    m_Texture:LoadMaterial(icon)
    m_Button.Text = buttonName


    if Id == 1 then--EGuildBenifit.GuildXiulian
        m_Button.OnClick = DelegateFactory.Action_QnButton(function(btn)
            CGuildMgr.Inst:ShowGuildTrainWnd()
        end)
    elseif Id == 2 then--EGuildBenifit.GuildLianShen
        m_Button.OnClick = DelegateFactory.Action_QnButton(function(btn)
            CUIManager.ShowUI(CLuaUIResources.LianShenWnd)
        end)
    elseif Id == 3 then--EGuildBenifit.GuildBonus
        m_Button.OnClick = DelegateFactory.Action_QnButton(function(btn)
            if self.sbDisableGuildBonusInCrossServer and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer then
                g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
                return
            end
            CTrackMgr.Inst:TrackToGuildNpc(20001168,0,0)
            CUIManager.CloseUI(CUIResources.GuildMainWnd)
        end)
    elseif Id == 5 then--EGuildBenifit.GuildGongXun
        m_Button.OnClick = DelegateFactory.Action_QnButton(function(btn)
            if self.sbDisableGuildGongxunCrossServer and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsCrossServerPlayer then
                g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
                return
            end
            CTrackMgr.Inst:TrackToGuildNpc(20001168,0,0)
            CUIManager.CloseUI(CUIResources.GuildMainWnd)
        end)
    elseif Id == 6 then--EGuildBenifit.GuildAuctionBonus
        m_Button.OnClick = DelegateFactory.Action_QnButton(function(btn)
            CAuctionMgr.Inst:ShowGuildBonusButtons(btn)
        end)
    elseif Id == 7 then--EGuildBenifit.GuildLifeSkill
        m_Button.OnClick = DelegateFactory.Action_QnButton(function(btn)
            CSkillInfoMgr.ShowLivingSkillWnd()
        end)
    else
        m_Button.OnClick = DelegateFactory.Action_QnButton(function(btn)
            g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        end)
    end

end

