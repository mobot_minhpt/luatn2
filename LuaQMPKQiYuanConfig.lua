local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUITexture=import "L10.UI.CUITexture"
local CQMPKTitleTemplate=import "L10.UI.CQMPKTitleTemplate"
local CQMPKWordsListTemplate = import "L10.UI.CQMPKWordsListTemplate"
local QnTabView = import "L10.UI.QnTabView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local IdPartition = import "L10.Game.IdPartition"
local CItemMgr = import "L10.Game.CItemMgr"
local CTalismanMgr = import "L10.Game.CTalismanMgr"

CLuaQMPKQiYuanConfig = class()
RegistClassMember(CLuaQMPKQiYuanConfig,"m_TypeObj")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_FishTypeObj")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_TypeTexture")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_SelectedBg")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_SelectedFishBg")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_CurrentTypeTex")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_CurrentNameLabel")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_Title")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_Words")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_ConfirmObj")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_IsInited")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_Type2WordDict")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_TypeIndex")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_FishTypeIndex")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_CurrentType")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_CurrentFishType")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_TabView")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_ChuanJiaBaoView")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_ZhenZhaiYuView")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_FabaoAttachedFishs")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_FishWords")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_CurTab")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_Fabao1")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_Fabao2")
--他人配置
RegistClassMember(CLuaQMPKQiYuanConfig,"m_IsOtherConfig")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_TipLab")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_ChuanJiaBaoTipLab")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_ZhenZhaiYuTipLab")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_ConfirmBtnLab")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_EffectFishWordInfo")
RegistClassMember(CLuaQMPKQiYuanConfig,"m_HasChange")
function CLuaQMPKQiYuanConfig:Awake()
    self.m_TypeObj = {}
    self.m_TypeObj[1]=FindChild(self.transform,"Di").gameObject
    self.m_TypeObj[2]=FindChild(self.transform,"Tian").gameObject
    self.m_TypeObj[3]=FindChild(self.transform,"Ren").gameObject

    self.m_TypeTexture = {}
    self.m_TypeTexture[1]=self.m_TypeObj[1]:GetComponent(typeof(CUITexture))
    self.m_TypeTexture[2]=self.m_TypeObj[2]:GetComponent(typeof(CUITexture))
    self.m_TypeTexture[3]=self.m_TypeObj[3]:GetComponent(typeof(CUITexture))

    self.m_SelectedBg = {}
    self.m_SelectedBg[1]=FindChild(self.m_TypeObj[1].transform,"Background").gameObject
    self.m_SelectedBg[2]=FindChild(self.m_TypeObj[2].transform,"Background").gameObject
    self.m_SelectedBg[3]=FindChild(self.m_TypeObj[3].transform,"Background").gameObject

    self.m_FishTypeObj = {}
    self.m_FishTypeObj[1] = self.transform:Find("ZhenZhaiYu/Fabao1/Fish1").gameObject
    self.m_FishTypeObj[2] = self.transform:Find("ZhenZhaiYu/Fabao1/Fish2").gameObject
    self.m_FishTypeObj[3] = self.transform:Find("ZhenZhaiYu/Fabao2/Fish1").gameObject
    self.m_FishTypeObj[4] = self.transform:Find("ZhenZhaiYu/Fabao2/Fish2").gameObject

    self.m_SelectedFishBg = {}
    self.m_SelectedFishBg[1] = FindChild(self.m_FishTypeObj[1].transform, "Selected").gameObject
    self.m_SelectedFishBg[2] = FindChild(self.m_FishTypeObj[2].transform, "Selected").gameObject
    self.m_SelectedFishBg[3] = FindChild(self.m_FishTypeObj[3].transform, "Selected").gameObject
    self.m_SelectedFishBg[4] = FindChild(self.m_FishTypeObj[4].transform, "Selected").gameObject

    self.m_Fabao1 = self.transform:Find("ZhenZhaiYu/Fabao1").gameObject
    self.m_Fabao2 = self.transform:Find("ZhenZhaiYu/Fabao2").gameObject

    self.m_CurrentTypeTex = self.transform:Find("Current"):GetComponent(typeof(CUITexture))
    self.m_CurrentNameLabel = self.transform:Find("Current/CurrentName"):GetComponent(typeof(UILabel))
    self.m_Title = self.transform:Find("Current/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_Words = self.transform:Find("Current/WordTemplate"):GetComponent(typeof(CQMPKWordsListTemplate))
    self.m_ConfirmObj = self.transform:Find("ConfimBtn").gameObject
--self.m_IsInited = ?
    self.m_TypeIndex ={0,0,0}
    self.m_FishTypeIndex = {0, 0, 0, 0}
--self.m_CurrentType = ?
    self.m_FabaoAttachedFishs = {{0,0},{0,0}}

    self.m_ChuanJiaBaoView  = self.transform:Find("ChuanJiaBao").gameObject
    self.m_ZhenZhaiYuView   = self.transform:Find("ZhenZhaiYu").gameObject
    self.m_TabView = self.transform:Find("SwitchTab"):GetComponent(typeof(QnTabView))
    self.m_TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
        self:OnChangeView(index)
    end)

    self.m_TipLab = self.transform:Find("Tip"):GetComponent(typeof(UILabel))
    self.m_ChuanJiaBaoTipLab = self.transform:Find("ChuanjiaBaoAlertLab"):GetComponent(typeof(UILabel))
    self.m_ZhenZhaiYuTipLab = self.transform:Find("ZhenZhaiYuAlertLab"):GetComponent(typeof(UILabel))
    self.m_ConfirmBtnLab = self.transform:Find("ConfimBtn/Label"):GetComponent(typeof(UILabel))

    self.m_IsOtherConfig = CLuaQMPKMgr.s_IsOtherPlayer
    self.m_HasChange = false
end

function CLuaQMPKQiYuanConfig:OnEnable( )
    g_ScriptEvent:AddListener("QMPKPrayBuffWordResult", self, "OnQMPKPrayBuffWordResult")
    if self.m_IsInited then
        if self.m_CurTab == 0 then
            self:OnTypeSelected(self.m_TypeObj[1])
        elseif self.m_CurTab == 1 then
            self:OnFishTypeSelected(self.m_FishTypeObj[1])
        end
        return
    end
    self.m_IsInited = true
    self:Init()
end

function CLuaQMPKQiYuanConfig:OnDisable()
    g_ScriptEvent:RemoveListener("QMPKPrayBuffWordResult", self, "OnQMPKPrayBuffWordResult")
end

function  CLuaQMPKQiYuanConfig:OnQMPKPrayBuffWordResult( engineId, di, tian, ren, fishTbl )
    self:OnPrayBuffResult(engineId, di, tian, ren, fishTbl)
end
function CLuaQMPKQiYuanConfig:Init( )
    for i,v in ipairs(self.m_TypeObj) do
        UIEventListener.Get(self.m_TypeObj[i]).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnTypeSelected(go)
        end)
    end

    UIEventListener.Get(self.m_Title.gameObject).onClick =DelegateFactory.VoidDelegate(function(go)
        if self.m_IsOtherConfig then
            return
        end
        self:OnTitleClick(go)
    end)
    UIEventListener.Get(self.m_ConfirmObj).onClick = DelegateFactory.VoidDelegate(function ( go )
        self:OnConfirmBtnClick(go)
    end)

    if CClientMainPlayer.Inst == nil then
        return
    end
    local class = self.m_IsOtherConfig and CLuaQMPKMgr.s_PlayerCls or EnumToInt(CClientMainPlayer.Inst.Class)
    self.m_Type2WordDict ={}

    do
        local keys ={}
        QuanMinPK_Pray.ForeachKey(function (key) 
            table.insert( keys,key )
        end)
        for i,v in ipairs(keys) do
            local word = QuanMinPK_Pray.GetData(v)
            if word.Class == class then
                local prayWord={ m_Description = word.Description,m_Id = v,m_Words = word.Words }
                if not self.m_Type2WordDict[word.Type] then
                    self.m_Type2WordDict[word.Type]={}
                end
                if word.IsDefault > 0 then
                    table.insert( self.m_Type2WordDict[word.Type],1,prayWord )
                else
                    table.insert( self.m_Type2WordDict[word.Type],prayWord )
                end
            end
        end
    end

    for i,v in ipairs(self.m_TypeTexture) do
        v:LoadMaterial(CQuanMinPKMgr.Inst.m_ChuanJiaBaoIcon[i-1])
    end


    self.m_FishWords ={}
    do
        local keys ={}
        QuanMinPK_Fish.ForeachKey(function (key) 
            table.insert( keys,key )
        end)
        for i,v in ipairs(keys) do
            local word = QuanMinPK_Fish.GetData(v)
            if word.Class == class then
                for j = 0, word.Words.Length - 1 do
                    local fishWord = { m_Description = word.Description[j], m_Id = j, m_Words = Table2Array({word.Words[j]}, MakeArrayClass(uint))}
                    table.insert(self.m_FishWords, fishWord)
                end
            end
        end
    end

    for i,v in ipairs(self.m_TypeTexture) do
        v:LoadMaterial(CQuanMinPKMgr.Inst.m_ChuanJiaBaoIcon[i-1])
    end

    if self.m_IsOtherConfig then
        self:InitOtherPlayerConfig()
    else
        if CClientMainPlayer.Inst ~= nil then
            Gac2Gas.QueryQmpkPlayerPrayWordGroup(CClientMainPlayer.Inst.EngineId)
        end
    end

    self:OnChangeView(0)
end
function CLuaQMPKQiYuanConfig:OnPrayBuffResult( objId, di, tian, ren, fishTbl, effectFishWordInfo) 
    if not self.m_IsOtherConfig then
        if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.EngineId ~= objId then
            return
        end
    end

    local temp={tonumber(di),tonumber(tian),tonumber(ren)}
    for i=1,3 do
        for j=1,#self.m_Type2WordDict[i] do
            if self.m_Type2WordDict[i][j].m_Id==temp[i] then
                self.m_TypeIndex[i]=j-1
                break
            end
        end
    end

    self.m_FishTypeIndex = fishTbl
    self.m_EffectFishWordInfo = effectFishWordInfo

    if self.m_CurTab == 0 then
        self:OnTypeSelected(self.m_TypeObj[1])
    elseif self.m_CurTab == 1 then
        self:OnFishTypeSelected(self.m_FishTypeObj[1])
    end
end
function CLuaQMPKQiYuanConfig:OnTypeSelected( go) 

    for i,v in ipairs(self.m_TypeObj) do
        if v==go then
            self.m_CurrentNameLabel.text = CQuanMinPKMgr.Inst.m_ChuanJiaBaoName[i-1]
            self.m_CurrentType = i
            self:OnTitleSelected(self.m_TypeIndex[i])
            self.m_CurrentTypeTex:LoadMaterial(CQuanMinPKMgr.Inst.m_ChuanJiaBaoIcon[i-1])
            self.m_SelectedBg[i]:SetActive(true)
        else
            self.m_SelectedBg[i]:SetActive(false)
        end
    end

end
function CLuaQMPKQiYuanConfig:OnFishTypeSelected( go) 

    for i,v in ipairs(self.m_FishTypeObj) do
        if v==go then
            local fishId = QuanMinPK_Setting.GetData().FishID[i-1]
            local itemData = Item_Item.GetData(fishId)
            self.m_CurrentNameLabel.text = itemData.Name
            self.m_CurrentFishType = i
            self:OnFishTitleSelected(self.m_FishTypeIndex[i]-1)
            self.m_CurrentTypeTex:LoadMaterial(itemData.Icon)
            self.m_SelectedFishBg[i]:SetActive(true)
        else
            self.m_SelectedFishBg[i]:SetActive(false)
        end
    end

    self:InitZhenZhaiYu()
end
function CLuaQMPKQiYuanConfig:OnTitleClick( go) 
    local popupList ={}
    if self.m_CurTab == 0 then
        local list =self.m_Type2WordDict[self.m_CurrentType]
        for i,v in ipairs(list) do
            local data = PopupMenuItemData(v.m_Description,DelegateFactory.Action_int(function(index) self:OnTitleSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( popupList,data )
        end
    elseif self.m_CurTab == 1 then
        local list = self.m_FishWords
        for i,v in ipairs(list) do
            local data = PopupMenuItemData(v.m_Description,DelegateFactory.Action_int(function(index) self:OnFishTitleSelected(index) end), false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( popupList,data )
        end
    end
    CPopupMenuInfoMgr.ShowPopupMenu(
        Table2Array(popupList,MakeArrayClass(PopupMenuItemData)), 
        go.transform, 
        CPopupMenuInfoMgr.AlignType.Bottom, 
        #popupList >= 8 and 2 or 1, 
        nil, 
        600, 
        true, 
        296)
end
function CLuaQMPKQiYuanConfig:OnTitleSelected( index) 
    local list = self.m_Type2WordDict[self.m_CurrentType]
    self.m_Title:Init(list[index+1].m_Description)
    self.m_Title:Expand(true)
    self.m_Words:Init(list[index+1].m_Words)
    if self.m_TypeIndex[self.m_CurrentType] ~= index then self.m_HasChanged = true end
    self.m_TypeIndex[self.m_CurrentType] = index
end
function CLuaQMPKQiYuanConfig:OnFishTitleSelected(index) 
    local list = self.m_FishWords
    self.m_Title:Init(list[index+1].m_Description)
    self.m_Title:Expand(true)
    self.m_Words:Init(list[index+1].m_Words)
    if self.m_FishTypeIndex[self.m_CurrentFishType] ~= index + 1 then self.m_HasChanged = true end
    self.m_FishTypeIndex[self.m_CurrentFishType] = index + 1
    self:InitZhenZhaiYu()
end
function CLuaQMPKQiYuanConfig:OnConfirmBtnClick( go) 
    Gac2Gas.RequestSetQmpkPrayWordGroup(
        self.m_Type2WordDict[1][self.m_TypeIndex[1]+1].m_Id, 
        self.m_Type2WordDict[2][self.m_TypeIndex[2]+1].m_Id, 
        self.m_Type2WordDict[3][self.m_TypeIndex[3]+1].m_Id)

    if not self.m_IsOtherConfig then
        Gac2Gas.RequestSetQmpkFishWordGroup(
            self.m_FishTypeIndex[1], 
            self.m_FishTypeIndex[2], 
            self.m_FishTypeIndex[3],
            self.m_FishTypeIndex[4])
    end
    self.m_HasChanged = false
end
function CLuaQMPKQiYuanConfig:OnChangeView(index)
    self.m_CurTab = index
    self.m_ChuanJiaBaoView:SetActive(index == 0)
    self.m_ZhenZhaiYuView:SetActive(index == 1)

    if index == 0 then
        self:OnTypeSelected(self.m_TypeObj[1])
    elseif index == 1 then
        self:OnFishTypeSelected(self.m_FishTypeObj[1])
    end

    if self.m_IsOtherConfig then
        local notSameClass = CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) ~= CLuaQMPKMgr.s_PlayerCls
        self.m_ChuanJiaBaoTipLab.gameObject:SetActive(index == 0 and notSameClass)
        self.m_ConfirmObj:SetActive(index == 0 and (not notSameClass))
        self.m_ZhenZhaiYuTipLab.gameObject:SetActive(index == 1)
    end
end

function CLuaQMPKQiYuanConfig:InitZhenZhaiYu()
    local fishIds = QuanMinPK_Setting.GetData().FishID
    self.m_FabaoAttachedFishs[1] = {{itemId = fishIds[0], wordId = self.m_FishWords[self.m_FishTypeIndex[1]].m_Words[0] },
    {itemId = fishIds[1], wordId = self.m_FishWords[self.m_FishTypeIndex[2]].m_Words[0]}}
    self.m_FabaoAttachedFishs[2] = {{itemId = fishIds[2], wordId = self.m_FishWords[self.m_FishTypeIndex[3]].m_Words[0] },
    {itemId = fishIds[3], wordId = self.m_FishWords[self.m_FishTypeIndex[4]].m_Words[0]}}
    self:InitFaBaoView()
end

function CLuaQMPKQiYuanConfig:InitFaBaoView()
    if self.m_IsOtherConfig then
        self:InitOtherFaBao(1,self.m_Fabao1)
        self:InitOtherFaBao(2,self.m_Fabao2)
    else
        self:InitFaBao(1,self.m_Fabao1)
        self:InitFaBao(2,self.m_Fabao2)
    end
end

function CLuaQMPKQiYuanConfig:InitFaBao(suitIndex,item)
    local fabaoLabel = item.transform:Find("FabaoLabel"):GetComponent(typeof(UILabel))
    local suitLabel1 = item.transform:Find("SuitLabel1"):GetComponent(typeof(UILabel))
    local suitLabel2 = item.transform:Find("SuitLabel2"):GetComponent(typeof(UILabel))
    local fish1 = item.transform:Find("Fish1")
    local fish2 = item.transform:Find("Fish2")

    if not CClientMainPlayer.Inst then return end
    local activeIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.ActiveAttrGroupIdx or 1
    if activeIdx==0 then
        activeIdx=1
    end
    local v = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,activeIdx)
    local activeTalismanSet = 1
    if v then
        activeTalismanSet = v.TalismanSetIdx
    end

    if suitIndex == activeTalismanSet then
        fabaoLabel.text = SafeStringFormat3(LocalString.GetString("法宝栏%d (生效)"),suitIndex)
    else
        fabaoLabel.text = SafeStringFormat3(LocalString.GetString("法宝栏%d"),suitIndex)
    end

    --套装属性
    suitLabel1.text = ""
    suitLabel2.text = ""
    local suitWords = {}

    local startIdx = LuaEnumBodyPosition.TalismanQian
    local endIdx = LuaEnumBodyPosition.TalismanDui
    if suitIndex==2 then
        startIdx = LuaEnumBodyPosition.TalismanQian2
        endIdx = LuaEnumBodyPosition.TalismanDui2
    end
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local suitTables = {}
    local suitFeiShengAdjusted = {}
    for i=startIdx,endIdx do
        local itemId = itemProp:GetItemAt(EnumItemPlace.Body, i) or ""
        local talisman = CItemMgr.Inst:GetById(itemId)
        if talisman and IdPartition.IdIsTalisman(talisman.TemplateId) then
            local suitInfo = CTalismanMgr.Inst:GetNormalTalismanSuitInfo(talisman)
            if suitInfo then
                for i=0,suitInfo.SuitInfos.Count-1,1 do
                    local suit = suitInfo.SuitInfos[i]
                    local myTbl = {}
                    myTbl.info = suit
                    myTbl.isFeiShengAdjusted = talisman.Equip.IsFeiShengAdjusted
                    if suitTables[suit.SuitId] then
                        table.insert(suitTables[suit.SuitId],myTbl)                       
                    else
                        suitTables[suit.SuitId] = {}
                        table.insert(suitTables[suit.SuitId],myTbl)
                    end
                end
            end
        end
    end

    for suitId,mySuitTbls in pairs(suitTables) do--suitInfos
        local count = #mySuitTbls
        local suitInfos = mySuitTbls[1].info
        local isFeiShengAdjusted = mySuitTbls[1].isFeiShengAdjusted
        local needCount = suitInfos.NeedCount

        if count >= needCount then
            local suitData = Talisman_Suit.GetData(suitId)
            if suitData and suitData.IsXianJia ~= 1 then
                local needAdjusted = isFeiShengAdjusted > 0 and CLuaEquipTip.b_NeedShowFeiShengAdjusted
                if needAdjusted and suitData.FeiShengReplaceWords ~= nil then
                    table.insert(suitWords,suitData.FeiShengReplaceWords[0])
                else
                    table.insert(suitWords,suitData.Words[0])
                end
            end
        end
    end

    local label = suitLabel1
    for i=1,#suitWords,2 do
        if i == 3 then
            label = suitLabel2
        end    
        local word1 = suitWords[i]
        local word2 = suitWords[i+1]
        local str1 = ""
        local str2 =""
        if word1 then
            str1 = Word_Word.GetData(word1).Description
        end
        if word2 then
            str2 = Word_Word.GetData(word2).Description
        end
        if str1=="" and str2=="" then
            label.text = ""
        elseif str2 == "" then
            label.text = str1
        else
            label.text = SafeStringFormat2(LocalString.GetString("%s，%s"),str1, str2)
        end
    end

    --法宝栏attach的鱼
    for i, v in ipairs(self.m_FabaoAttachedFishs[suitIndex]) do
        if i==1 then
            self:InitFaBaoFish(suitIndex,fish1,v,suitWords,i)
        elseif i== 2 then
            self:InitFaBaoFish(suitIndex,fish2,v,suitWords,i)
        end
    end
end

function CLuaQMPKQiYuanConfig:InitFaBaoFish(suitIndex,fish,zhenzhaiInfo,suitWords,findex)
    local fishIcon = fish:Find("FishIcon"):GetComponent(typeof(CUITexture))
    local emptyNode = fish:Find("EmptyNode")
    local border = fish:Find("FishIcon/Border"):GetComponent(typeof(UISprite))
    local nameLabel = fish:Find("FishIcon/NameLabel"):GetComponent(typeof(UILabel))
    local shuXingLabel = fish:Find("FishIcon/ShuXingLabel"):GetComponent(typeof(UILabel))
    fishIcon.gameObject:SetActive(zhenzhaiInfo ~= nil)
    emptyNode.gameObject:SetActive(zhenzhaiInfo == nil)


    UIEventListener.Get(fish.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnFishTypeSelected(fish.gameObject)
    end)

    local itemData = Item_Item.GetData(zhenzhaiInfo.itemId)
    fishIcon:LoadMaterial(itemData.Icon)
    border.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    nameLabel.text = itemData.Name

    local isConflct = false
	local fishWord = zhenzhaiInfo.wordId
	if fishWord and fishWord ~= 0 then
		local fisgCls = math.floor(fishWord/100)
		local data = HouseFish_WordConflict.GetData(fisgCls)
		if data and suitWords then
			local conflicts = data.Conflict
			for i=0,conflicts.Length-1,1 do
				local cls = conflicts[i]
				for j=1,#suitWords,1 do
					if cls == math.floor(suitWords[j]/100) then
						isConflct = true
						break
					end
				end
				if isConflct then
					break
				end
			end
		end
        -- 是否和已关联的镇宅鱼属性相同
		if findex == 2 then
            local attachedFishs = self.m_FabaoAttachedFishs[suitIndex]
            if attachedFishs[1] and attachedFishs[2] then
                local fworld1 = attachedFishs[1].wordId
                local fworld2 = attachedFishs[2].wordId
                if math.floor(fworld1/100) == math.floor(fworld2/100) then
                    isConflct = true
                end
            end
        end

        if isConflct then
            shuXingLabel.text = LocalString.GetString("[c][ff0000]属性互斥[-][/c]")
        else
            shuXingLabel.text = Word_Word.GetData(fishWord).Description
        end
    else
        shuXingLabel.text = LocalString.GetString("暂无")
	end
end

function CLuaQMPKQiYuanConfig:InitOtherFaBao(suitIndex,item)
    local fabaoLabel = item.transform:Find("FabaoLabel"):GetComponent(typeof(UILabel))
    local suitLabel1 = item.transform:Find("SuitLabel1"):GetComponent(typeof(UILabel))
    local suitLabel2 = item.transform:Find("SuitLabel2"):GetComponent(typeof(UILabel))
    local fish1 = item.transform:Find("Fish1")
    local fish2 = item.transform:Find("Fish2")

    fabaoLabel.text = SafeStringFormat3(LocalString.GetString("法宝栏%d"),suitIndex)
    --套装属性
    suitLabel1.text = ""
    suitLabel2.text = ""
    local suitWords = self.m_EffectFishWordInfo

    local label = suitLabel1
    for i=1,#suitWords,2 do
        if i == 3 then
            label = suitLabel2
        end    
        local word1 = suitWords[i]
        local word2 = suitWords[i+1]
        local str1 = ""
        local str2 =""
        if word1 then
            str1 = Word_Word.GetData(word1).Description
        end
        if word2 then
            str2 = Word_Word.GetData(word2).Description
        end
        if str1=="" and str2=="" then
            label.text = ""
        elseif str2 == "" then
            label.text = str1
        else
            label.text = SafeStringFormat2(LocalString.GetString("%s，%s"),str1, str2)
        end
    end

    --法宝栏attach的鱼
    for i, v in ipairs(self.m_FabaoAttachedFishs[suitIndex]) do
        if i==1 then
            self:InitFaBaoFish(suitIndex,fish1,v,suitWords,i)
        elseif i== 2 then
            self:InitFaBaoFish(suitIndex,fish2,v,suitWords,i)
        end
    end
end

function CLuaQMPKQiYuanConfig:InitOtherFaBaoFish(suitIndex,fish,zhenzhaiInfo,suitWords,findex)
    local fishIcon = fish:Find("FishIcon"):GetComponent(typeof(CUITexture))
    local emptyNode = fish:Find("EmptyNode")
    local border = fish:Find("FishIcon/Border"):GetComponent(typeof(UISprite))
    local nameLabel = fish:Find("FishIcon/NameLabel"):GetComponent(typeof(UILabel))
    local shuXingLabel = fish:Find("FishIcon/ShuXingLabel"):GetComponent(typeof(UILabel))
    fishIcon.gameObject:SetActive(zhenzhaiInfo ~= nil)
    emptyNode.gameObject:SetActive(zhenzhaiInfo == nil)

    UIEventListener.Get(fish.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnFishTypeSelected(fish.gameObject)
    end)

    local itemData = Item_Item.GetData(zhenzhaiInfo.itemId)
    fishIcon:LoadMaterial(itemData.Icon)
    border.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    nameLabel.text = itemData.Name
	local fishWord = zhenzhaiInfo.wordId
	shuXingLabel.text = Word_Word.GetData(fishWord).Description
end

function CLuaQMPKQiYuanConfig:InitOtherPlayerConfig() 
    self.m_TipLab.gameObject:SetActive(false)
    self.m_ConfirmBtnLab.text = LocalString.GetString("学习此传家宝")

    local playerInfo = CLuaQMPKMgr.s_PlayerInfo
    local qifuWordInfo = playerInfo.QifuWordInfo
    local fishWordInfo = playerInfo.FishWordInfo
    local effectFishWordInfo = playerInfo.EffectFishWordInfo
    self:OnPrayBuffResult(CLuaQMPKMgr.s_PlayerId, qifuWordInfo[1], qifuWordInfo[2], qifuWordInfo[3], fishWordInfo, effectFishWordInfo)
end
function CLuaQMPKQiYuanConfig:OnPageCloseOrChange(finalFunc)
    if self.m_HasChanged then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("QMPK_ConfigSave_Confirm"), function()
            self:OnConfirmBtnClick(self.m_ConfirmBtn)
            if finalFunc then finalFunc() end
        end, function()
            if finalFunc then finalFunc() end
        end, nil, nil, false)
        self.m_HasChanged = false
        return true
    else
        if finalFunc then finalFunc() end
    end
    return false
end
return CLuaQMPKQiYuanConfig
