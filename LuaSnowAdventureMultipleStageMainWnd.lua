local UISprite = import "UISprite"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local Animation = import "UnityEngine.Animation"
local CTooltip = import "L10.UI.CTooltip"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"

LuaSnowAdventureMultipleStageMainWnd = class()
LuaSnowAdventureMultipleStageMainWnd.FirstOpenStageId = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "Rewards", "Rewards", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "RewardButton", "RewardButton", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "progressSprite", "progressSprite", UITexture)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "RewardName", "RewardName", UILabel)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "RewardTips", "RewardTips", UILabel)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "Route", "Route", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "AdventureName", "AdventureName", UILabel)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "RemainLabel", "RemainLabel", UILabel)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "ChangeSnowManButton", "ChangeSnowManButton", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "SnowManTexture", "SnowManTexture", CUITexture)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "SnowManProIcon", "SnowManProIcon", UISprite)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "SnowManLevel", "SnowManLevel", UILabel)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "SnowAdventureStageDetailWnd", "SnowAdventureStageDetailWnd", GameObject)
RegistChildComponent(LuaSnowAdventureMultipleStageMainWnd, "MultipleRoute", "MultipleRoute", GameObject)

--@endregion RegistChildComponent end

function LuaSnowAdventureMultipleStageMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    if CommonDefs.IS_VN_CLIENT then
        if LocalString.language == "vn" then
            self.transform:Find("AdventureName/snowadventuresinglestagemainwnd_logo_bg").localPosition = Vector3(-111, -36, 0)
        else
            self.transform:Find("AdventureName/snowadventuresinglestagemainwnd_logo_bg").localPosition = Vector3(-33, -36, 0)
        end
    end
    
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaSnowAdventureMultipleStageMainWnd:Init()
    Gac2Gas.RequestXuePoLiXianData()
end

--@region UIEvent

--@endregion UIEvent

function LuaSnowAdventureMultipleStageMainWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2023_SyncXuePoLiXianData", self, "OnSyncXuePoLiXianData")
    g_ScriptEvent:AddListener("SelectSnowAdventureStage", self, "OnSelectSnowAdventureStage")
    g_ScriptEvent:AddListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
    g_ScriptEvent:AddListener("CloseSnowAdventureStageDetailWnd", self, "OnCloseSnowAdventureStageDetailWnd")


    if LuaSnowAdventureMultipleStageMainWnd.FirstOpenStageId then
        LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId = nil
    end
end

function LuaSnowAdventureMultipleStageMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXuePoLiXianData", self, "OnSyncXuePoLiXianData")
    g_ScriptEvent:RemoveListener("SelectSnowAdventureStage", self, "OnSelectSnowAdventureStage")
    g_ScriptEvent:RemoveListener("HanJia2023_EnterPlayScene", self, "OnEnterScene")
    g_ScriptEvent:RemoveListener("CloseSnowAdventureStageDetailWnd", self, "OnCloseSnowAdventureStageDetailWnd")



    for i = 1, 4 do
        local len = #self.stageRouteScriptList[i]
        if len ~= 0 then
            for subIndex = 1, len do
                self.stageRouteScriptList[i][subIndex]:RemoveListener()
            end
        end
    end
end

function LuaSnowAdventureMultipleStageMainWnd:OnEnterScene()
    CUIManager.CloseUI(CLuaUIResources.SnowAdventureMultipleStageMainWnd)
end

function LuaSnowAdventureMultipleStageMainWnd:InitWndData()
    self.timeRewardItemId = HanJia2023_XuePoLiXianSetting.GetData().SuccessNumRewardItemId
    self.rewardTimePerRound = HanJia2023_XuePoLiXianSetting.GetData().SuccessNumNeed
    self.snowAdventureData = LuaHanJia2023Mgr.snowAdventureData

    self.stageDetailScript = LuaSnowAdventureStageDetailWnd:new()
    self.stageDetailScript:Init(self.SnowAdventureStageDetailWnd)

    self.stageRouteScriptList = {{}, {}, {}, {}}
    self.initEventOnce = false

    self.wndAnimation = self.transform:GetComponent(typeof(Animation))

end

function LuaSnowAdventureMultipleStageMainWnd:RefreshConstUI()
    self:InitOneItem(self.RewardButton, self.timeRewardItemId)
end

function LuaSnowAdventureMultipleStageMainWnd:RefreshVariableUI()
    if LuaHanJia2023Mgr.snowAdventureData == nil then
        return
    end
    self.snowAdventureData = LuaHanJia2023Mgr.snowAdventureData
    
    --刷新成功通关次数
    local realSuccessNum = self.snowAdventureData.successNum % self.rewardTimePerRound
    self.RewardTips.text =  g_MessageMgr:FormatMessage("SnowAdventure_GetRewardTips", realSuccessNum, self.rewardTimePerRound)
    self.progressSprite.fillAmount = realSuccessNum / self.rewardTimePerRound

    --刷新剩余参与次数
    self.RemainLabel.text = g_MessageMgr:FormatMessage("SnowAdventure_RemainPlayNum", self.snowAdventureData.playNum)

    --刷新雪人
    local snowmanType = self.snowAdventureData.lastSnowmanType
    local snowmanLevel = self.snowAdventureData.snowmanInfo[snowmanType]
    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)
    self.SnowManTexture:LoadMaterial(snowmanConfig.Pic)
    self.SnowManLevel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(snowmanLevel))
    self.SnowManProIcon.spriteName = Profession.GetIconByNumber(snowmanConfig.Career)
    
    self:RefreshRoute()
end

function LuaSnowAdventureMultipleStageMainWnd:RefreshRoute()
    local routeObj = self.MultipleRoute
    local routeId = 4
    local childCount = routeObj.transform.childCount
    for i = 1, childCount do
        local childRoot = routeObj.transform:Find(tostring(i))
        --这个点对应的stageIndex
        local stageId = routeId * 100 + i
        --当前路线已解锁的最大StageId
        local curRouteMaxUnlockStageId = self.snowAdventureData.unlockStageInfo[routeId]
        if curRouteMaxUnlockStageId == nil then
            curRouteMaxUnlockStageId = 0
        end
        --当前这个点 的前序点是否挑战过.. 多人模式里没有这个事情
        local preStagePass = true

        local stageScript = nil
        if #self.stageRouteScriptList[routeId] < i then
            stageScript = LuaSnowAdventureStageTemplate:new()
            stageScript:Init(childRoot)
            table.insert(self.stageRouteScriptList[routeId], stageScript)
        else
            stageScript = self.stageRouteScriptList[routeId][i]
        end
        
        stageScript:RefreshData(stageId, curRouteMaxUnlockStageId, preStagePass, 0)
    end
end

function LuaSnowAdventureMultipleStageMainWnd:InitUIEvent()
    if self.snowAdventureData == nil then
        return
    end
    self.initEventOnce = true
    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("SnowAdventure_MultiplePlayTips")
    end)

    UIEventListener.Get(self.ChangeSnowManButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        LuaSnowAdventureSnowManWnd.ShowCountdownPattern = false
        CUIManager.ShowUI(CLuaUIResources.SnowAdventureSnowManWnd)
    end)

    UIEventListener.Get(self.RemainLabel.transform:Find("PlusBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        local addTimeItemId = 21051948
        CItemAccessListMgr.Inst:ShowItemAccessInfo(addTimeItemId, false, self.RemainLabel.transform, CTooltip.AlignType.Right)
    end)
end

function LuaSnowAdventureMultipleStageMainWnd:OnSyncXuePoLiXianData()
    self:RefreshVariableUI()
    if self.initEventOnce == false then
        self:InitUIEvent()
    end
end

function LuaSnowAdventureMultipleStageMainWnd:OnSelectSnowAdventureStage(selectedStageId)
    -- open or refresh
    if (not self.SnowAdventureStageDetailWnd.activeInHierarchy) or (self.SnowAdventureStageDetailWnd.transform:GetComponent(typeof(UIPanel)).alpha == 0) then
        self.wndAnimation:Play("snowadventuremultiplestagemainwnd_qiehuan")
    end
    self.transform:Find("CloseButton").gameObject:SetActive(false)
    self.stageDetailScript:RefreshData(selectedStageId)
end

function LuaSnowAdventureMultipleStageMainWnd:OnCloseSnowAdventureStageDetailWnd()
    self.wndAnimation:Play("snowadventuremultiplestagemainwnd_qiehuanhui")
    self.transform:Find("CloseButton").gameObject:SetActive(true)
end

function LuaSnowAdventureMultipleStageMainWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end
