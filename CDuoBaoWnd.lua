-- Auto Generated!!
local CDuoBaoMgr = import "L10.UI.CDuoBaoMgr"
local CDuoBaoWnd = import "L10.UI.CDuoBaoWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Int32 = import "System.Int32"
local QnTabButton = import "L10.UI.QnTabButton"
local UIEventListener = import "UIEventListener"
CDuoBaoWnd.m_Init_CS2LuaHook = function (this) 

    CDuoBaoMgr.Inst:InitData()
    UIEventListener.Get(this.closeBtn).onClick = (DelegateFactory.VoidDelegate(function (p) 
        CUIManager.CloseUI(CUIResources.DuoBaoWnd)
    end))
    this.tabBar.OnSelect = MakeDelegateFromCSFunction(this.OnTabClick, MakeGenericClass(Action2, QnTabButton, Int32), this)
    this.tabBar:ChangeTo(0)
end
CDuoBaoWnd.m_OnTabClick_CS2LuaHook = function (this, button, index) 
    this.mTitle.text = string.gsub(button.Text, "\n", CommonDefs.IS_VN_CLIENT and " " or "")
    repeat
        local default = index
        if default == 0 then
            this.m_DuobaoingWnd:Init()
            break
        elseif default == 1 then
            this.m_MyDuobaoWnd:Init()
            break
        elseif default == 2 then
            this.m_DuobaoResultWnd:Init()
            break
        end
    until 1
end
