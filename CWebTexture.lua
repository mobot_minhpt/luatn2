-- Auto Generated!!
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CWebTexture = import "L10.UI.CWebTexture"
CWebTexture.m_Init_CS2LuaHook = function (this, playerId, cls, gender) 
    this.m_LocalTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, -1), false)
    this.m_LocalTexture.gameObject:SetActive(true)
    this.m_WebTexture.gameObject:SetActive(false)

    local postUrl = CPersonalSpaceMgr.BASE_URL .. "qnm/info/get_avatar"
    this:StartCoroutine(this:DownloadCoroutine(postUrl, playerId))
end
