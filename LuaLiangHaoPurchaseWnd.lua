require("common/common_include")

local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UITabBar = import "L10.UI.UITabBar"
local UIGrid = import "UIGrid"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local CTooltip=import "L10.UI.CTooltip"
local NGUIText = import "NGUIText"
local Color = import "UnityEngine.Color"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaLiangHaoPurchaseWnd = class()

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_ViewTabBar")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_FilterPurchaseTabBar")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_FilterAuctionTabBar")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_FilterCustomizeTabBar")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PurchaseAndAuctionRoot")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeRoot")
--Purchase and Auction
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PageGrid")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_LiangHaoItem")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PurchaseRoot")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_AuctionRoot")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PurchaseTipLabel")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PurchaseMoneyCtrl")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PurchaseButton")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_RemainTipLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_RemainTimeLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_AuctionButton")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_MyAuctionButton")

--Customize
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeHead")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeTitleLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeAvaliableNumLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizePlayerNumLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizePlayerNumTipLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizePageGrid")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizePageItem")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeTipLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeRemainTipLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeRemainTimeLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_RequestCustomizeButton")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_MyRequestCustomizeButton")

--Common
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_NextPageButton")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PrevPageButton")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_PageLabel")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_InfoButton")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_RefreshButton")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CurViewIdx")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CurPurchaseFilterIdx")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CurAuctionFilterIdx")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CurPageIdx")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_MaxLiangHaoPerPage")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_LiangHaoTbl")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_LiangHaoItems")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CurSelectedIndex")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_AuctionColor")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_NoneAuctionColor")

RegistClassMember(LuaLiangHaoPurchaseWnd, "m_MaxItemPerPageForCustomize")
RegistClassMember(LuaLiangHaoPurchaseWnd, "m_CustomizeDataTbl")

function LuaLiangHaoPurchaseWnd:Init()

	self.m_ViewTabBar = self.transform:Find("Anchor/ViewTabs"):GetComponent(typeof(UITabBar))
	self.m_FilterPurchaseTabBar = self.transform:Find("Anchor/FilterTabs1"):GetComponent(typeof(UITabBar))
	self.m_FilterAuctionTabBar = self.transform:Find("Anchor/FilterTabs2"):GetComponent(typeof(UITabBar))
	self.m_FilterCustomizeTabBar = self.transform:Find("Anchor/FilterTabsForCustomize"):GetComponent(typeof(UITabBar))

	self.m_PurchaseAndAuctionRoot = self.transform:Find("Anchor/PurchaseAndAuctionRoot")
	self.m_CustomizeRoot = self.transform:Find("Anchor/CustomizeRoot")


	self.m_PageGrid = self.m_PurchaseAndAuctionRoot:Find("ScrollView/PageItem/Grid"):GetComponent(typeof(UIGrid))
	self.m_LiangHaoItem = self.m_PurchaseAndAuctionRoot:Find("ScrollView/PageItem/Item").gameObject
	self.m_PurchaseRoot = self.m_PurchaseAndAuctionRoot:Find("Purchase").gameObject
	self.m_AuctionRoot = self.m_PurchaseAndAuctionRoot:Find("Auction").gameObject
	self.m_PurchaseTipLabel = self.m_PurchaseAndAuctionRoot:Find("Auction/PurchaseTipLabel"):GetComponent(typeof(UILabel))
	self.m_PurchaseMoneyCtrl = self.m_PurchaseAndAuctionRoot:Find("Purchase/MoneyCtrl"):GetComponent(typeof(CCurentMoneyCtrl))
	self.m_PurchaseButton = self.m_PurchaseAndAuctionRoot:Find("Purchase/PurchaseButton"):GetComponent(typeof(CButton))
	self.m_RemainTipLabel = self.m_PurchaseAndAuctionRoot:Find("Auction/RemainTime/Label"):GetComponent(typeof(UILabel))
	self.m_RemainTimeLabel = self.m_PurchaseAndAuctionRoot:Find("Auction/RemainTime/RemainTimeLabel"):GetComponent(typeof(UILabel))
	self.m_AuctionButton = self.m_PurchaseAndAuctionRoot:Find("Auction/AuctionButton"):GetComponent(typeof(CButton))
	self.m_MyAuctionButton = self.m_PurchaseAndAuctionRoot:Find("Auction/MyAuctionButton"):GetComponent(typeof(CButton))

	self.m_NextPageButton = self.transform:Find("Anchor/Navigation/NextBtn"):GetComponent(typeof(CButton))
	self.m_PrevPageButton = self.transform:Find("Anchor/Navigation/PreviousBtn"):GetComponent(typeof(CButton))
	self.m_PageLabel = self.transform:Find("Anchor/Navigation/PageLabel"):GetComponent(typeof(UILabel))self.m_InfoButton = self.transform:Find("Anchor/InfoButton").gameObject
	self.m_RefreshButton = self.transform:Find("Anchor/RefreshButton").gameObject

	self.m_CustomizeHead = self.m_CustomizeRoot:Find("HeadBg").gameObject
	self.m_CustomizeTitleLabel = self.m_CustomizeRoot:Find("HeadBg/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_CustomizeAvaliableNumLabel = self.m_CustomizeRoot:Find("HeadBg/AvaliableLabel/ValueLabel"):GetComponent(typeof(UILabel))
	self.m_CustomizePlayerNumLabel = self.m_CustomizeRoot:Find("HeadBg/PlayerLabel/ValueLabel"):GetComponent(typeof(UILabel))
	self.m_CustomizePlayerNumTipLabel = self.m_CustomizeRoot:Find("HeadBg/PlayerLabel/TipLabel"):GetComponent(typeof(UILabel))
	self.m_CustomizePageGrid = self.m_CustomizeRoot:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
	self.m_CustomizePageItem = self.m_CustomizeRoot:Find("ScrollView/Item").gameObject
	self.m_CustomizeTipLabel = self.m_CustomizeRoot:Find("CustomizeTipLabel"):GetComponent(typeof(UILabel))
	self.m_CustomizeRemainTipLabel = self.m_CustomizeRoot:Find("RemainTime/Label"):GetComponent(typeof(UILabel))
	self.m_CustomizeRemainTimeLabel = self.m_CustomizeRoot:Find("RemainTime/RemainTimeLabel"):GetComponent(typeof(UILabel))
	self.m_RequestCustomizeButton = self.m_CustomizeRoot:Find("RequestCustomizeButton"):GetComponent(typeof(CButton))
	self.m_MyRequestCustomizeButton = self.m_CustomizeRoot:Find("MyRequestCustomizeButton"):GetComponent(typeof(CButton))

	self.m_LiangHaoItem:SetActive(false)
	self.m_CustomizePageItem:SetActive(false)
	
	CommonDefs.AddOnClickListener(self.m_PrevPageButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnPrevPageButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_NextPageButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnNextPageButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_PageLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnPageLabelClick() end), false)
	CommonDefs.AddOnClickListener(self.m_PurchaseButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnPurchaseButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_AuctionButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnAuctionButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_MyAuctionButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnMyAuctionButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_RequestCustomizeButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnRequestCustomizeButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_MyRequestCustomizeButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnMyRequestCustomizeButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_InfoButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_RefreshButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnRefreshButtonClick() end), false)

	self.m_ViewTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnChangeView(go, index) end)
	self.m_FilterPurchaseTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnChangePurchaseFilter(go, index) end)
	self.m_FilterAuctionTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnChangeAuctionFilter(go, index) end)
	self.m_FilterCustomizeTabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnChangeCustomizeFilter(go, index) end)

	self.m_CurViewIdx = 0
	self.m_CurPurchaseFilterIdx = 0
	self.m_CurAuctionFilterIdx = 0
	self.m_CurPageIdx = 0
	self.m_CurSelectedIndex = nil

	self.m_AuctionColor = NGUIText.ParseColor24("8ff5ff", 0)
	self.m_NoneAuctionColor = Color.white

	self.m_MaxLiangHaoPerPage = 12
	self.m_LiangHaoTbl = {}
	self.m_LiangHaoItems = {}
	self.m_MaxItemPerPageForCustomize = 8
	self.m_FilterPurchaseTabBar:ChangeTab(self.m_CurPurchaseFilterIdx, true)
	self.m_FilterAuctionTabBar:ChangeTab(self.m_CurAuctionFilterIdx, true)
	self.m_FilterCustomizeTabBar:ChangeTab(0, true)
	self.m_ViewTabBar:ChangeTab(self.m_CurViewIdx, false)

	self.m_PurchaseTipLabel.text = ""
	self.m_RemainTipLabel.text = ""
	self.m_RemainTimeLabel.text = ""

	self.m_CustomizeHead:SetActive(false)
	self.m_CustomizeTipLabel.text = ""
	self.m_CustomizeTipLabel.text = ""
	self.m_CustomizeRemainTimeLabel.text = ""
end

function LuaLiangHaoPurchaseWnd:RegisterTick()
	self:CancelTick()
	self.m_UpdateTick = RegisterTick(function ( ... )
			self:UpdateRemainTime()
		end, 500)
	self:UpdateRemainTime()
end

function LuaLiangHaoPurchaseWnd:CancelTick()
	if self.m_UpdateTick then
		UnRegisterTick(self.m_UpdateTick)
		self.m_UpdateTick = nil
	end
end

function LuaLiangHaoPurchaseWnd:OnEnable()
	g_ScriptEvent:AddListener("OnLiangHaoPurchaseInfoReply", self, "OnLiangHaoPurchaseInfoReply")
	self:RegisterTick()
end

function LuaLiangHaoPurchaseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnLiangHaoPurchaseInfoReply", self, "OnLiangHaoPurchaseInfoReply")
	self:CancelTick()
end

function LuaLiangHaoPurchaseWnd:UpdateRemainTime()
	self:UpdateAuctionRemainTime()
	self:UpdateCustomizeRemainTime()
end

function LuaLiangHaoPurchaseWnd:UpdateAuctionRemainTime()
	if not self.m_RemainTimeLabel then return end
	if not self.m_AuctionRoot.activeSelf then return end

	local time = LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionExpireTime or 0
	local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(time)
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local timespan = endTime:Subtract(now)
	local totalSeconds = timespan.TotalSeconds
	if totalSeconds<=0 then
		if #self.m_LiangHaoTbl>0 then
			--如果还显示折数据，就清空一下，清空的逻辑在LoadData中
			--self:LoadData()
			--self:ShowPage()
			self.m_PurchaseTipLabel.text = ""
			self.m_RemainTipLabel.text = LocalString.GetString("今日已结束")
			self.m_RemainTimeLabel.text = ""
		else
			time = LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionNextStartTime or 0
			endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(time)
			timespan = endTime:Subtract(now)
			totalSeconds = math.max(0, timespan.TotalSeconds)
			local hour = math.floor(totalSeconds/3600)
			local min = math.floor(math.fmod(totalSeconds,3600)/60)
			local sec = math.floor(math.fmod(totalSeconds,60))
			self.m_RemainTipLabel.text = LocalString.GetString("距离开始")
			self.m_RemainTimeLabel.text = SafeStringFormat3("%d:%02d:%02d", hour, min, sec)

			if totalSeconds>0 then
				self.m_PurchaseTipLabel.text = LocalString.GetString("抢靓号活动尚未开始")
			else
				self.m_PurchaseTipLabel.text = LocalString.GetString("请刷新")
			end
		end
	else
		local hour = math.floor(totalSeconds/3600)
		local min = math.floor(math.fmod(totalSeconds,3600)/60)
		local sec = math.floor(math.fmod(totalSeconds,60))
		self.m_RemainTipLabel.text = LocalString.GetString("距离结束")
		self.m_RemainTimeLabel.text = SafeStringFormat3("%d:%02d:%02d", hour, min, sec)

		if #self.m_LiangHaoTbl==0 then
			self.m_PurchaseTipLabel.text = LocalString.GetString("本次投放的靓号被抢完了")
		else
			self.m_PurchaseTipLabel.text = ""
		end
	end
end

function LuaLiangHaoPurchaseWnd:UpdateCustomizeRemainTime()
	if not self.m_CustomizeRemainTimeLabel then return end
	if not self.m_CustomizeRoot.gameObject.activeSelf then return end

	local time = LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionExpireTime or 0
	local endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(time)
	local now = CServerTimeMgr.Inst:GetZone8Time()
	local timespan = endTime:Subtract(now)
	local totalSeconds = timespan.TotalSeconds
	local isAvaliable = false
	if totalSeconds<=0 then
		if #self.m_CustomizeDataTbl>0 then

			self.m_CustomizeTipLabel.text = ""
			self.m_CustomizeRemainTipLabel.text = LocalString.GetString("今日已结束")
			self.m_CustomizeRemainTimeLabel.text = ""
			isAvaliable = true --坤少说和竞拍保持一致，有数据就显示
		else
			time = LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionNextStartTime or 0
			endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(time)
			timespan = endTime:Subtract(now)
			totalSeconds = math.max(0, timespan.TotalSeconds)
			local hour = math.floor(totalSeconds/3600)
			local min = math.floor(math.fmod(totalSeconds,3600)/60)
			local sec = math.floor(math.fmod(totalSeconds,60))
			self.m_CustomizeRemainTipLabel.text = LocalString.GetString("距离开始")
			self.m_CustomizeRemainTimeLabel.text = SafeStringFormat3("%d:%02d:%02d", hour, min, sec)

			if totalSeconds>0 then
				self.m_CustomizeTipLabel.text = LocalString.GetString("定制靓号活动尚未开始")
			else
				self.m_CustomizeTipLabel.text = LocalString.GetString("请刷新")
			end
			isAvaliable = false
		end
	else
		local hour = math.floor(totalSeconds/3600)
		local min = math.floor(math.fmod(totalSeconds,3600)/60)
		local sec = math.floor(math.fmod(totalSeconds,60))
		self.m_CustomizeRemainTipLabel.text = LocalString.GetString("距离结束")
		self.m_CustomizeRemainTimeLabel.text = SafeStringFormat3("%d:%02d:%02d", hour, min, sec)

		self.m_CustomizeTipLabel.text = ""
		isAvaliable = true
	end

	self.m_CustomizeHead:SetActive(isAvaliable) --活动未开始的时候
end

function LuaLiangHaoPurchaseWnd:OnChangeView(go, index)

	self.m_PurchaseAndAuctionRoot.gameObject:SetActive(index==0 or index==1)
	self.m_CustomizeRoot.gameObject:SetActive(index==2)

	self.m_PurchaseRoot:SetActive(index==0)
	self.m_FilterPurchaseTabBar.gameObject:SetActive(index==0)
	self.m_AuctionRoot:SetActive(index==1)
	self.m_FilterAuctionTabBar.gameObject:SetActive(index==1)
	self.m_FilterCustomizeTabBar.gameObject:SetActive(index==2)
	self.m_CurViewIdx = index
	self.m_CurPageIdx = 0
	self:LoadDataAndShowPage()

	self:UpdateCustomizeRemainTime()--防止这个地方刷新不及时，主动调用一下
end

function LuaLiangHaoPurchaseWnd:OnChangePurchaseFilter(go, index)
	self.m_CurPurchaseFilterIdx = index
	self.m_CurPageIdx = 0
	self:LoadData()
	self:ShowPage()
end

function LuaLiangHaoPurchaseWnd:OnChangeAuctionFilter(go, index)
	self.m_CurAuctionFilterIdx = index
	self.m_CurPageIdx = 0
	self:LoadData()
	self:ShowPage()
end

function LuaLiangHaoPurchaseWnd:OnChangeCustomizeFilter(go, index)
	self.m_CurPageIdx = 0
	self:LoadDataForCustomize()
	self:ShowPageForCustomize()
end

function LuaLiangHaoPurchaseWnd:LoadDataAndShowPage()
	if self.m_CurViewIdx == 2 and self.m_CustomizeRoot.gameObject.activeSelf then
        self:LoadDataForCustomize()
		self:ShowPageForCustomize()
    else
    	self:LoadData()
		self:ShowPage()
    end
end

function LuaLiangHaoPurchaseWnd:OnLiangHaoPurchaseInfoReply(lianghaoPurchaseInfoReplyType)
	if lianghaoPurchaseInfoReplyType == EnumLiangHaoPurchaseInfoReplyType.ePurchase then
		self:LoadDataAndShowPage()
	elseif lianghaoPurchaseInfoReplyType == EnumLiangHaoPurchaseInfoReplyType.eAuction then
		self:LoadDataAndShowPage()
	elseif lianghaoPurchaseInfoReplyType == EnumLiangHaoPurchaseInfoReplyType.eCustomize then
		self:LoadDataAndShowPage()
	elseif lianghaoPurchaseInfoReplyType == EnumLiangHaoPurchaseInfoReplyType.eRefresh then
        self:LoadDataAndShowPage()
    end
end

function LuaLiangHaoPurchaseWnd:MatchDigitNumber(lianghaoId)
	if self.m_CurViewIdx == 0 then
		if not self.m_CurPurchaseFilterIdx or self.m_CurPurchaseFilterIdx == 0 then
			return 1000000<=lianghaoId and lianghaoId<=9999999 --7位
		else
			return 10000000<=lianghaoId and lianghaoId<=99999999 --8位
		end
	elseif self.m_CurViewIdx == 1 then
		if not self.m_CurAuctionFilterIdx or self.m_CurAuctionFilterIdx == 0 then
			return 10000<=lianghaoId and lianghaoId<=99999 --5位
		elseif self.m_CurAuctionFilterIdx == 1 then
			return 100000<=lianghaoId and lianghaoId<=999999 --6位
		elseif self.m_CurAuctionFilterIdx == 2 then
			return 1000000<=lianghaoId and lianghaoId<=9999999 --7位
		else
			return 10000000<=lianghaoId and lianghaoId<=99999999 --8位
		end
	end

	return false
end

function LuaLiangHaoPurchaseWnd:LoadData()
	local tbl = {}
	if self.m_CurViewIdx == 0 then
		tbl = LuaLiangHaoMgr.PurchaseLiangHaoInfo.purchaseTbl
	elseif self.m_CurViewIdx == 1 then
		--if LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionExpireTime>CServerTimeMgr.Inst.timeStamp then
			tbl = LuaLiangHaoMgr.PurchaseLiangHaoInfo.auctionTbl
		--end
	end

	self.m_LiangHaoTbl = {}

	for __,data in pairs(tbl) do
		if self:MatchDigitNumber(data.lianghaoId) then
			table.insert(self.m_LiangHaoTbl, data)
		end
	end
end

function LuaLiangHaoPurchaseWnd:LoadDataForCustomize()

	local idx = self.m_FilterCustomizeTabBar.SelectedIndex
	local digitNumber = 0
	if idx == 0 then digitNumber = 5
		elseif idx == 1 then digitNumber = 6
			elseif idx == 2 then digitNumber = 7
				elseif idx == 3 then digitNumber = 8 end

	local customizeTbl =  LuaLiangHaoMgr.PurchaseLiangHaoInfo.customizeTbl[digitNumber] or {}
	self.m_CustomizeDataTbl = customizeTbl.list or {}

	self.m_CustomizeTitleLabel.text = LuaLiangHaoMgr.GetCardTitleName(digitNumber)
	self.m_CustomizeAvaliableNumLabel.text = tostring(customizeTbl.avaliableNum or 0)
	self.m_CustomizePlayerNumLabel.text = tostring(customizeTbl.playerNum or 0)
	self.m_CustomizePlayerNumTipLabel.text = SafeStringFormat3(LocalString.GetString("(仅显示前%s名)"), tostring(customizeTbl.avaliableNum or 0))
end

function LuaLiangHaoPurchaseWnd:ShowPage()
	local pageCount = math.ceil(#self.m_LiangHaoTbl * 1 / self.m_MaxLiangHaoPerPage)
	if self.m_CurPageIdx >= 0 and self.m_CurPageIdx < pageCount then
        self.m_PageLabel.text = System.String.Format("{0}/{1}", self.m_CurPageIdx + 1, pageCount)
    else
        self.m_CurPageIdx = 0
        local index = self.m_CurPageIdx+1
        if pageCount==0 then index=0 end
        self.m_PageLabel.text = System.String.Format("{0}/{1}", index, pageCount)
    end

    self.m_PrevPageButton.Enabled = (pageCount > 1 and self.m_CurPageIdx > 0)
    self.m_NextPageButton.Enabled = (pageCount > 1 and self.m_CurPageIdx < pageCount - 1)

    self.m_CurSelectedIndex = nil
    if self.m_PurchaseRoot.activeSelf then
    	self.m_PurchaseMoneyCtrl:SetCost(0)
	elseif self.m_AuctionRoot.activeSelf then
		--do nothing
	end

	self.m_LiangHaoItems = {}
    Extensions.RemoveAllChildren(self.m_PageGrid.transform)

    local startIdx = self.m_CurPageIdx*self.m_MaxLiangHaoPerPage+1
    local endIdx = math.min(startIdx + self.m_MaxLiangHaoPerPage - 1, #self.m_LiangHaoTbl)

    for i = startIdx, endIdx do
    	local go = CUICommonDef.AddChild(self.m_PageGrid.gameObject, self.m_LiangHaoItem)
    	go:SetActive(true)
    	go.transform:Find("LiangHaoLabel"):GetComponent(typeof(UILabel)).text = tostring(self.m_LiangHaoTbl[i].lianghaoId)
    	local priceLabel = go.transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    	priceLabel.text = tostring(self.m_LiangHaoTbl[i].price)
    	if self.m_CurViewIdx == 1 and self.m_LiangHaoTbl[i].count and self.m_LiangHaoTbl[i].count>0 then
    		priceLabel.color = self.m_AuctionColor
    	else
    		priceLabel.color = self.m_NoneAuctionColor
    	end
    	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnCandidateLiangHaoClick(i) end), false)
    	table.insert(self.m_LiangHaoItems, {id=self.m_LiangHaoTbl[i].lianghaoId, gameObject=go})
	end

	self.m_PageGrid:Reposition()
end

function LuaLiangHaoPurchaseWnd:ShowPageForCustomize()

	local pageCount = math.ceil(#self.m_CustomizeDataTbl * 1 / self.m_MaxItemPerPageForCustomize)
	if self.m_CurPageIdx >= 0 and self.m_CurPageIdx < pageCount then
        self.m_PageLabel.text = System.String.Format("{0}/{1}", self.m_CurPageIdx + 1, pageCount)
    else
        self.m_CurPageIdx = 0
        local index = self.m_CurPageIdx+1
        if pageCount==0 then index=0 end
        self.m_PageLabel.text = System.String.Format("{0}/{1}", index, pageCount)
    end

    self.m_PrevPageButton.Enabled = (pageCount > 1 and self.m_CurPageIdx > 0)
    self.m_NextPageButton.Enabled = (pageCount > 1 and self.m_CurPageIdx < pageCount - 1)

    self.m_CurSelectedIndex = nil

    Extensions.RemoveAllChildren(self.m_CustomizePageGrid.transform)

    local startIdx = self.m_CurPageIdx*self.m_MaxItemPerPageForCustomize+1
    local endIdx = math.min(startIdx + self.m_MaxItemPerPageForCustomize - 1, #self.m_CustomizeDataTbl)

    for i = startIdx, endIdx do
    	local go = CUICommonDef.AddChild(self.m_CustomizePageGrid.gameObject, self.m_CustomizePageItem)
    	go:SetActive(true)
    	go.transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text = tostring(i)
    	local playerNameLabel = go.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    	playerNameLabel.text = self.m_CustomizeDataTbl[i].playerName
    	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == self.m_CustomizeDataTbl[i].playerId then
    		playerNameLabel.color = Color.green
    	else
    		playerNameLabel.color =  Color.white
    	end
    	go.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel)).text = self.m_CustomizeDataTbl[i].serverName
    	go.transform:Find("NoNameLabel").gameObject:SetActive(self.m_CustomizeDataTbl[i].playerId == 0)
    	local priceLabel = go.transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    	if self.m_CustomizeDataTbl[i].price>0 then
    		priceLabel.gameObject:SetActive(true)
    		priceLabel.text = tostring(self.m_CustomizeDataTbl[i].price)
    	else
    		priceLabel.gameObject:SetActive(false)
    		priceLabel.text = ""
    	end
	end

	self.m_CustomizePageGrid:Reposition()
end

function LuaLiangHaoPurchaseWnd:OnCandidateLiangHaoClick(index)
	self.m_CurSelectedIndex = index
	local price = self.m_LiangHaoTbl[self.m_CurSelectedIndex].price
	if self.m_PurchaseRoot.activeSelf then
		self.m_PurchaseMoneyCtrl:SetCost(price)
		self.m_PurchaseButton.Enabled = true
	elseif self.m_AuctionRoot.activeSelf then
		self.m_AuctionButton.Enabled = true
	end

	for __,data in pairs(self.m_LiangHaoItems) do
		if data.id == self.m_LiangHaoTbl[self.m_CurSelectedIndex].lianghaoId then
			data.gameObject:GetComponent(typeof(CButton)).Selected = true
		else
			data.gameObject:GetComponent(typeof(CButton)).Selected = false
		end
	end
end

--上一页
function LuaLiangHaoPurchaseWnd:OnPrevPageButtonClick()
	if self.m_CurPageIdx > 0 then
        self.m_CurPageIdx = self.m_CurPageIdx - 1
        if self.m_CustomizeRoot.gameObject.activeSelf then
        	self:ShowPageForCustomize()
        else
        	self:ShowPage()
        end
    end
end

--下一页
function LuaLiangHaoPurchaseWnd:OnNextPageButtonClick()
	local dataCount = 0
	local maxPerPage = 1
	if self.m_CustomizeRoot.gameObject.activeSelf then
		dataCount = #self.m_CustomizeDataTbl
		maxPerPage = self.m_MaxItemPerPageForCustomize
	else
		dataCount = #self.m_LiangHaoTbl
		maxPerPage = self.m_MaxLiangHaoPerPage
	end
	local pageCount = math.ceil(dataCount* 1 / maxPerPage)
    if self.m_CurPageIdx < pageCount - 1 then
        self.m_CurPageIdx = self.m_CurPageIdx + 1
        if self.m_CustomizeRoot.gameObject.activeSelf then
        	self:ShowPageForCustomize()
        else
        	self:ShowPage()
        end
    end
end

function LuaLiangHaoPurchaseWnd:OnPageLabelClick()
	local dataCount = 0
	local maxPerPage = 1
	if self.m_CustomizeRoot.gameObject.activeSelf then
		dataCount = #self.m_CustomizeDataTbl
		maxPerPage = self.m_MaxItemPerPageForCustomize
	else
		dataCount = #self.m_LiangHaoTbl
		maxPerPage = self.m_MaxLiangHaoPerPage
	end
	local pageCount = math.ceil(dataCount * 1 / maxPerPage)
	if pageCount<2 then return end -- 只有一页的情况不处理
	CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(1, pageCount, self.m_CurPageIdx + 1, 10, DelegateFactory.Action_int(function (val)
        self.m_PageLabel.text = tostring(val)
    end),
    DelegateFactory.Action_int(function (val)
        self:OnNumberKeyBoardInput(val)
    end), self.m_PageLabel, CTooltip.AlignType.Top, true)
end

function LuaLiangHaoPurchaseWnd:OnNumberKeyBoardInput(val)
	local dataCount = 0
	local maxPerPage = 1
	if self.m_CustomizeRoot.gameObject.activeSelf then
		dataCount = #self.m_CustomizeDataTbl
		maxPerPage = self.m_MaxItemPerPageForCustomize
	else
		dataCount = #self.m_LiangHaoTbl
		maxPerPage = self.m_MaxLiangHaoPerPage
	end

	local page=math.max(1,val)
    local totalPageCount = math.ceil(dataCount * 1 / maxPerPage)
    page=math.min(totalPageCount,page)
    self.m_CurPageIdx = page - 1 -- index 从 0 开始的
    if self.m_CustomizeRoot.gameObject.activeSelf then
    	self:ShowPageForCustomize()
    else
    	self:ShowPage()
    end
end

--购买
function LuaLiangHaoPurchaseWnd:OnPurchaseButtonClick()
	local data = self.m_CurSelectedIndex and self.m_LiangHaoTbl and self.m_LiangHaoTbl[self.m_CurSelectedIndex]
	if data then
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Lianghao_Buy_Confirm",data.price, data.lianghaoId), 
			DelegateFactory.Action(function() 
				LuaLiangHaoMgr.RequestBuyLiangHao(data.lianghaoId, data.price)
			end),nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	else
		g_MessageMgr:ShowMessage("LiangHao_BuyError_NotSelect")
	end
end

--竞拍
function LuaLiangHaoPurchaseWnd:OnAuctionButtonClick()
	local data = self.m_CurSelectedIndex and self.m_LiangHaoTbl and self.m_LiangHaoTbl[self.m_CurSelectedIndex]
	if data then
		LuaLiangHaoMgr.QueryLiangHaoAuctionInfo(data.lianghaoId)
	else
		g_MessageMgr:ShowMessage("LiangHao_BuyError_NotSelect")
	end
end

--我的竞拍
function LuaLiangHaoPurchaseWnd:OnMyAuctionButtonClick()
	LuaLiangHaoMgr.QueryMyLiangHaoAuctionInfo()
end

function LuaLiangHaoPurchaseWnd:OnInfoButtonClick()
	if self.m_CurViewIdx == 0 then
		g_MessageMgr:ShowMessage("Lianghao_Buy_ReadMe")
	elseif self.m_CurViewIdx == 1 then
		g_MessageMgr:ShowMessage("Lianghao_Auction_ReadMe")
	elseif self.m_CurViewIdx == 2 then
		g_MessageMgr:ShowMessage("Lianghao_Dingzhi_Readme")
	end
end

function LuaLiangHaoPurchaseWnd:OnRefreshButtonClick()
	LuaLiangHaoMgr.RefreshLiangHaoPurchaseInfo()
end

--------Begin 定制

function LuaLiangHaoPurchaseWnd:OnRequestCustomizeButtonClick()
	local idx = self.m_FilterCustomizeTabBar.SelectedIndex
	local digitNumber = 0
	if idx == 0 then digitNumber = 5
		elseif idx == 1 then digitNumber = 6
			elseif idx == 2 then digitNumber = 7
				elseif idx == 3 then digitNumber = 8 end
	LuaLiangHaoMgr.OpenAuctionCustomizeCardWnd(digitNumber)--不考虑当前是否有定制号，交给服务器判断
end

function LuaLiangHaoPurchaseWnd:OnMyRequestCustomizeButtonClick()
	LuaLiangHaoMgr.OpenMyAuctionCustomizeCardWnd()
end

--------End 定制


