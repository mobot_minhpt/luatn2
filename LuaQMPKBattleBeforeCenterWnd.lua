local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITexture = import "UITexture"
local UILabel = import "UILabel"
local UITable = import "UITable"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local Animation = import "UnityEngine.Animation"
LuaQMPKBattleBeforeCenterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "Prepare", "Prepare", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "CountDownBg", "CountDownBg", UITexture)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "CenterCountDownLabel", "CenterCountDownLabel", UILabel)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "TeamScore1", "TeamScore1", UILabel)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "TeamScore2", "TeamScore2", UILabel)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "ProgressTable", "ProgressTable", UITable)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "RuleTable", "RuleTable", UITable)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "RuleTemplate", "RuleTemplate", UILabel)
RegistChildComponent(LuaQMPKBattleBeforeCenterWnd, "CountDownLabel", "CountDownLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_CurRound")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_LeftIsAttack")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_ProgressList")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_CloseTick")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_CountDownAni")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_RedCountDown")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_LimitCountDown")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_CountDownTick")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_curTime")
RegistClassMember(LuaQMPKBattleBeforeCenterWnd, "m_prepareStageCurTime")
function LuaQMPKBattleBeforeCenterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_CurRound = 0
    self.m_LeftIsAttack = false
    self.m_ProgressList = nil
    self.m_CloseTick = nil
    self.m_CountDownAni = self.CenterCountDownLabel.transform:GetComponent(typeof(Animation))
    self.m_RedCountDown = self.CountDownBg.transform:Find("RedCountBg"):GetComponent(typeof(UITexture))
    self.m_LimitCountDown = QuanMinPK_Setting.GetData().ShowRedProgressLeftTime
    self.m_CountDownTick = nil
    self.m_prepareStageCurTime = 0
    self.m_curTime = 0
    self:Init()
end

function LuaQMPKBattleBeforeCenterWnd:Init()
    Extensions.RemoveAllChildren(self.RuleTable.transform)
    self.ProgressTable.gameObject:SetActive(false)
end

function LuaQMPKBattleBeforeCenterWnd:InitBanPickCenterInfo(leftIsAttack,score1,score2,round,curTime,endTime,maxTime,winForce)
    self.Prepare.gameObject:SetActive(true)
    self.CountDownBg.gameObject:SetActive(true)
    self.CenterCountDownLabel.gameObject:SetActive(false)
    self.m_RedCountDown.gameObject:SetActive(false)
    if round ~= self.m_CurRound then
        self.m_CurRound = round
        if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
        self:InitRule()
    end
    self.m_LeftIsAttack = leftIsAttack
    self:InitScore(score1,score2)
    self:UpdateTime(curTime,endTime,maxTime)
    self:InitProgress(round,winForce)
end

function LuaQMPKBattleBeforeCenterWnd:InitScore(score1,score2)
    self.TeamScore1.text = score1
    self.TeamScore2.text = score2
    if self.m_LeftIsAttack then
        self.TeamScore1.color = NGUIText.ParseColor24("7cb1e3", 0)
        self.TeamScore2.color = NGUIText.ParseColor24("ff7777", 0)
    else
        self.TeamScore1.color = NGUIText.ParseColor24("ff7777", 0)
        self.TeamScore2.color = NGUIText.ParseColor24("7cb1e3", 0)
    end
end

function LuaQMPKBattleBeforeCenterWnd:UpdateTime(curTime,endTime,maxTime)
    local leftTime = math.floor(endTime - curTime)
    self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("倒计时 %d"),leftTime)
    if not self.m_CountDownTick then
        self.m_curTime = leftTime * 1000
        self.m_CountDownTick = RegisterTick(function()
            self.CountDownBg.fillAmount = self.m_curTime / (maxTime * 1000)
            self.m_RedCountDown.fillAmount = self.m_curTime / (maxTime * 1000)
            self.m_curTime = math.max(0,self.m_curTime - 30) 
        end,30)
    elseif self.m_curTime < leftTime * 1000 then
        UnRegisterTick(self.m_CountDownTick)
        self.m_curTime = leftTime * 1000
        self.m_CountDownTick = RegisterTick(function()
            self.CountDownBg.fillAmount = self.m_curTime / (maxTime * 1000)
            self.m_RedCountDown.fillAmount = self.m_curTime / (maxTime * 1000)
            self.m_curTime = math.max(0,self.m_curTime - 30) 
        end,30)
    end
    -- self.CountDownBg.fillAmount = leftTime / maxTime
    -- self.m_RedCountDown.fillAmount = leftTime / maxTime
    self.m_RedCountDown.gameObject:SetActive(leftTime <= self.m_LimitCountDown )
    self.CountDownLabel.color = leftTime <= self.m_LimitCountDown and NGUIText.ParseColor24("F0645D", 0) or NGUIText.ParseColor24("FFDDA6", 0)
end

function LuaQMPKBattleBeforeCenterWnd:InitRule()
    self.RuleTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.RuleTable.transform)
    local RuleMessage = g_MessageMgr:FormatMessage("QMPK_BanPickRule"..self.m_CurRound)
    if not System.String.IsNullOrEmpty(RuleMessage) then
        local info = CMessageTipMgr.regex_Paragraph:Matches(RuleMessage)

        if not info then return end
        do 
            local i = 0
            while i < info.Count do
                local paragraphGo = CUICommonDef.AddChild(self.RuleTable.gameObject, self.RuleTemplate.gameObject)
                paragraphGo.gameObject:GetComponent(typeof(UILabel)).text = ToStringWrap(info[i].Groups[3])
                paragraphGo.gameObject:SetActive(true)
                i = i + 1
            end
        end
        self.RuleTable:Reposition()
    end
end

function LuaQMPKBattleBeforeCenterWnd:InitProgress(round,winForce)
    self.ProgressTable.gameObject:SetActive(true)
    if not self.m_ProgressList then
        self.m_ProgressList = {}
        local nameList = QuanMinPK_Setting.GetData().BattleStageName
        for i = 0,nameList.Length - 1 do
            
            local Node = self.ProgressTable.transform:Find("Node"..(i + 1))
            local Line = self.ProgressTable.transform:Find("Line"..(i + 1))
            if Node then
                local node = Node.transform:Find("Node")
                local hnode = Node.transform:Find("HighLightNode")
                local cnode = Node.transform:Find("ComplateNode")
                node.transform:Find("Label"):GetComponent(typeof(UILabel)).text = nameList[i][0]
                hnode.transform:Find("Label"):GetComponent(typeof(UILabel)).text = nameList[i][1]
                cnode.transform:Find("Label"):GetComponent(typeof(UILabel)).text = nameList[i][0]
                self.m_ProgressList[i + 1] = {}
                self.m_ProgressList[i + 1].round = i + 1
                self.m_ProgressList[i + 1].Node = node.gameObject
                self.m_ProgressList[i + 1].HighLightNode = hnode.gameObject
                self.m_ProgressList[i + 1].ComplateNode = cnode.gameObject
            end
            if Line then 
                self.m_ProgressList[i + 1].Line = Line.gameObject
            end
        end
    end

    for k,v in pairs(self.m_ProgressList) do
        local itemRound = v.round
        if itemRound < round then
            v.Node.gameObject:SetActive(false)
            v.HighLightNode.gameObject:SetActive(false)
            v.ComplateNode.gameObject:SetActive(true)
            if winForce[itemRound] == EnumCommonForce_lua.eAttack then
                v.ComplateNode:GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("486E94", 0)
            elseif winForce[itemRound] == EnumCommonForce_lua.eDefend then
                v.ComplateNode:GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("A65B5C", 0)
            end
            if v.Line then 
                v.Line:GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("FABA70", 0)
            end
        elseif itemRound == round then
            v.Node.gameObject:SetActive(false)
            v.HighLightNode.gameObject:SetActive(true)
            v.ComplateNode.gameObject:SetActive(false)
            if v.Line then 
                v.Line:GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("404A78", 0)
            end
        else
            v.Node.gameObject:SetActive(true)
            v.HighLightNode.gameObject:SetActive(false)
            v.ComplateNode.gameObject:SetActive(false)
            if v.Line then 
                v.Line:GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("404A78", 0)
            end
        end
    end
    self.ProgressTable:Reposition()
end
function LuaQMPKBattleBeforeCenterWnd:SetPrepareVisible(show)
    self.Prepare.gameObject:SetActive(show)
end
function LuaQMPKBattleBeforeCenterWnd:InitPrepareStatus(curTime,endTime)
    --self.Prepare.gameObject:SetActive(false)
    if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
    self.m_RedCountDown.gameObject:SetActive(false)
    self.CountDownBg.gameObject:SetActive(true)
    self.CenterCountDownLabel.gameObject:SetActive(true)
    local leftTime = math.floor(endTime - curTime)
    if leftTime > 0 then
        self.CenterCountDownLabel.text = leftTime
        if self.m_prepareStageCurTime ~= leftTime then
            self.m_CountDownAni:Stop()
            self.m_CountDownAni:Play("qmpkbattlebeforewnd_countdown_2")
        end
        self.m_prepareStageCurTime = leftTime
    end
    self.CountDownBg.fillAmount = 1
    if self.m_CloseTick then return end
    self.m_CloseTick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.QMPKBattleBeforeWnd)
    end, math.max(leftTime,0.1) * 1000)
end
--@region UIEvent

--@endregion UIEvent
function  LuaQMPKBattleBeforeCenterWnd:OnDisable()
    if self.m_CloseTick then UnRegisterTick(self.m_CloseTick) self.m_CloseTick = nil end
    if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
end
