local LuaUtils = import "LuaUtils"
local UIEventListener = import "UIEventListener"
local XueQiuDaZhan_Season = import "L10.Game.XueQiuDaZhan_Season"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local CChatLinkMgr = import "CChatLinkMgr"
local UIWidget = import "UIWidget"

LuaSnowBallHisWnd = class()
RegistChildComponent(LuaSnowBallHisWnd,"closeBtn", GameObject)
RegistChildComponent(LuaSnowBallHisWnd,"template", GameObject)
RegistChildComponent(LuaSnowBallHisWnd,"nodeTemplate", GameObject)
RegistChildComponent(LuaSnowBallHisWnd,"scrollView", UIScrollView)
RegistChildComponent(LuaSnowBallHisWnd,"table", UITable)
RegistChildComponent(LuaSnowBallHisWnd,"shareBtn", GameObject)

function LuaSnowBallHisWnd:GetTitleName(num)
  if not num then
    return ""
  end
  num = tonumber(num)
  if not num then
    return ""
  end
  local count = XueQiuDaZhan_Award.GetDataCount()
  for i=1,count do
    local data = XueQiuDaZhan_Award.GetData(i)
    if data then
      if num >= data.StartNum and num <= data.EndNum then
        return CChatLinkMgr.TranslateToNGUIText(data.Title,false)
      end
    end
  end
  return ""
end

function LuaSnowBallHisWnd:InitShowNodeData(node,index,ctype)
  local index = tostring(index)
  local ctype = tostring(ctype)
  if node then
    node.transform:Find('title'):GetComponent(typeof(UILabel)).text = ""
    node.transform:Find('kill'):GetComponent(typeof(UILabel)).text = "0"
    node.transform:Find('win'):GetComponent(typeof(UILabel)).text = "0"
    node.transform:Find('total'):GetComponent(typeof(UILabel)).text = "0"
    node.transform:Find('score'):GetComponent(typeof(UILabel)).text = "0"
  end
  if LuaSnowBallMgr.SceInfo then
    if CommonDefs.DictContains(LuaSnowBallMgr.SceInfo, typeof(String), ctype) then
      if CommonDefs.DictContains(LuaSnowBallMgr.SceInfo[ctype], typeof(String), index) then
        local data = LuaSnowBallMgr.SceInfo[ctype][index]
        if CommonDefs.DictContains(data, typeof(String), 'rank') then
          node.transform:Find('title'):GetComponent(typeof(UILabel)).text = self:GetTitleName(data.rank)
        else
          node.transform:Find('title'):GetComponent(typeof(UILabel)).text = ''
        end
        node.transform:Find('kill'):GetComponent(typeof(UILabel)).text = data.killNum
        node.transform:Find('win'):GetComponent(typeof(UILabel)).text = data.winNum
        node.transform:Find('total'):GetComponent(typeof(UILabel)).text = data.totalNum
        node.transform:Find('score'):GetComponent(typeof(UILabel)).text = data.matchScore
      end
    end
  end
end

function LuaSnowBallHisWnd:AddShowNode(index)
  local node = NGUITools.AddChild(self.table.gameObject,self.template)
  node:SetActive(true)
  local singleNode = node.transform:Find('single').gameObject
  local doubleNode = node.transform:Find('double').gameObject
  self:InitShowNodeData(singleNode,index,1001)
  self:InitShowNodeData(doubleNode,index,1002)
end
function LuaSnowBallHisWnd:InitData(showIndex)
  Extensions.RemoveAllChildren(self.table.transform)
  if LuaSnowBallMgr.SceInfo == nil then
  end
  local nodeName = {LocalString.GetString('第\n一\n赛\n季'),LocalString.GetString('第\n二\n赛\n季'),LocalString.GetString('第\n三\n赛\n季'),LocalString.GetString('第\n四\n赛\n季'),LocalString.GetString('第\n五\n赛\n季'),LocalString.GetString('第\n六\n赛\n季')}
  local seasonCount = XueQiuDaZhan_Season.GetDataCount()
  local nowTime = CServerTimeMgr.Inst.timeStamp
  local realShowInde = nil
  local totalCount = 0
  for i=1,seasonCount do
    local seasonInfo = XueQiuDaZhan_Season.GetData(1000+i)
    if seasonInfo and seasonInfo.Status == 0 then
      totalCount = totalCount + 1
      local index = seasonInfo.ID
      local node = NGUITools.AddChild(self.table.gameObject,self.nodeTemplate)
      node:SetActive(true)
      local btn = node.transform:Find('bg').gameObject
      if nowTime >= seasonInfo.RankStartTime and nowTime < seasonInfo.RankEndTime then
        node.transform:Find('lock').gameObject:SetActive(false)
        node.transform:Find('open').gameObject:SetActive(true)
        node.transform:Find('open/text'):GetComponent(typeof(UILabel)).text = nodeName[totalCount]

        UIEventListener.Get(btn).onClick = LuaUtils.VoidDelegate(function ( ... )
          self:InitData(index)
        end)
      elseif nowTime < seasonInfo.RankStartTime then
        node.transform:Find('lock').gameObject:SetActive(true)
        node.transform:Find('open').gameObject:SetActive(false)
        node.transform:Find('lock/text'):GetComponent(typeof(UILabel)).text = nodeName[totalCount]
      elseif nowTime > seasonInfo.RankEndTime then
        node.transform:Find('lock').gameObject:SetActive(false)
        node.transform:Find('open').gameObject:SetActive(true)
        node.transform:Find('open/text'):GetComponent(typeof(UILabel)).text = nodeName[totalCount]

        UIEventListener.Get(btn).onClick = LuaUtils.VoidDelegate(function ( ... )
          self:InitData(index)
        end)
      end

      if showIndex and showIndex == index then
        self:AddShowNode(index)
        realShowInde = totalCount
      elseif not showIndex then
        if nowTime >= seasonInfo.RankStartTime and nowTime < seasonInfo.RankEndTime then
          self:AddShowNode(index)
          realShowInde = totalCount
        end
      end
    end
  end

  if realShowInde and realShowInde > 3 then
    self.scrollView.contentPivot = UIWidget.Pivot.Right
  else
    self.scrollView.contentPivot = UIWidget.Pivot.Left
  end

  self.table:Reposition()
  self.scrollView:ResetPosition()
end

function LuaSnowBallHisWnd:Init()
	UIEventListener.Get(self.closeBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
    CUIManager.CloseUI(CLuaUIResources.SnowBallHisWnd)
	end)
  self.template:SetActive(false)
  self.nodeTemplate:SetActive(false)

	UIEventListener.Get(self.shareBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
		CUICommonDef.CaptureScreenAndShare()
	end)

  self:InitData()
end

function LuaSnowBallHisWnd:OnEnable( ... )
--	g_ScriptEvent:AddListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end

function LuaSnowBallHisWnd:OnDisable( ... )
--	g_ScriptEvent:RemoveListener("SyncNewYearPrizeDrawIndex", self, "SyncDrawIndex")
end

function LuaSnowBallHisWnd:OnDestroy( ... )
end

return LuaSnowBallHisWnd
