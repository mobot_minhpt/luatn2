-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemConsumeWndMgr = import "L10.UI.CItemConsumeWndMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingShouPurchaseWnd = import "L10.UI.CLingShouPurchaseWnd"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local MessageWndManager = import "L10.UI.MessageWndManager"
CLingShouPurchaseWnd.m_Init_CS2LuaHook = function (this) 
    this.templateId = CItemConsumeWndMgr.itemTemplateId
    local data = CItemMgr.Inst:GetItemTemplate(this.templateId)
    this.countUpdate.templateId = this.templateId
    this.countUpdate.format = ((CItemConsumeWndMgr.wndTitle .. "({0}/") .. tostring(CItemConsumeWndMgr.consumeAmount)) .. ")"
    --uint needNum = 1;
    --int lingShouSlotNum = CClientMainPlayer.Inst.FightProp.LingShouNumber;//现有的数量
    --int[] nums = LingShou_Setting.GetData().SheJiWuNum;
    --if (nums != null && nums.Length > lingShouSlotNum)
    --{
    --    //下一个需要的数量
    --    needNum = (uint)nums[lingShouSlotNum];
    --}

    this.countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        if val < CItemConsumeWndMgr.consumeAmount then
            this.countUpdate.format = ((CItemConsumeWndMgr.wndTitle .. "([ff0000]{0}[-]/") .. tostring(CItemConsumeWndMgr.consumeAmount)) .. ")"
        else
            this.countUpdate.format = ((CItemConsumeWndMgr.wndTitle .. "({0}/") .. tostring(CItemConsumeWndMgr.consumeAmount)) .. ")"
        end
        this:UpdateCost()
    end)
    this.countUpdate:UpdateCount()

    this:UpdateCost()
end
CLingShouPurchaseWnd.m_UpdateCost_CS2LuaHook = function (this) 
    local costJade = Mall_LingYuMall.GetData(this.templateId)
    if costJade ~= nil then
        local needNum = CItemConsumeWndMgr.consumeAmount - this.countUpdate.count
        needNum = needNum < 0 and 0 or needNum
        this.cost = costJade.Jade * needNum
        this.costLabel.text = tostring(this.cost)
    else
        this.cost = 0
        this.costLabel.text = "0"
    end
end
CLingShouPurchaseWnd.m_Purchase_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        this:Close()
        return
    end
    if CClientMainPlayer.Inst.Jade < this.cost then
        --灵玉不足
        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function () 
            --TODO:打开充值界面
            CShopMallMgr.ShowChargeWnd()
        end), nil, LocalString.GetString("充值"), nil, false)
    elseif CItemConsumeWndMgr.purchaseAction ~= nil then
        --TODO:购买
        --Gac2Gas.AddNewLingShouMaxNumberWithJade();
        invoke(CItemConsumeWndMgr.purchaseAction)
    end
    this:Close()
end
