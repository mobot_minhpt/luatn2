local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnButton = import "L10.UI.QnButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local UILabel = import "UILabel"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
LuaQMPHKUnlockZhanLingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPHKUnlockZhanLingWnd, "BuyBtn", "BuyBtn", QnButton)
RegistChildComponent(LuaQMPHKUnlockZhanLingWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaQMPHKUnlockZhanLingWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaQMPHKUnlockZhanLingWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPHKUnlockZhanLingWnd,"m_ShowItemList")
function LuaQMPHKUnlockZhanLingWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.BuyBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyBtnClick()
	end)


    --@endregion EventBind end
end

function LuaQMPHKUnlockZhanLingWnd:Init()
	self.QnCostAndOwnMoney:SetCost(QuanMinPK_Setting.GetData().UnLockAdvancedZhanLingLingYu)
	self:InitRewardList()
end

function LuaQMPHKUnlockZhanLingWnd:InitRewardList()
	local vipItem = {}
	self.m_ShowItemList = {}
	QuanMinPK_HandBook.Foreach(function (key,data)
		local itemId = data.Item2[0]
		local count = data.Item2[1]
		local isBind = data.Item2[2] + 1
		if not vipItem[itemId] then 
			vipItem[itemId] = {0,0,0,itemId}	-- 绑定数量，非绑定数量，权重,道具id
		end
		vipItem[itemId][isBind] = vipItem[itemId][isBind] + count
    end)

	local weightlist = QuanMinPK_Setting.GetData().HandBookVipItemWeight
	for i = 0,weightlist.Length - 1 do
		if vipItem[weightlist[i]] then
			vipItem[weightlist[i]][3] = weightlist.Length - i
		end
	end

	for k,v in pairs(vipItem) do
		local nobindCount = v[1]
		local bindCount = v[2]
		local weight = v[3]
		local itemId = v[4]
		if nobindCount > 0 then
			table.insert(self.m_ShowItemList,{itemId,nobindCount,weight,0})
		end
		if bindCount > 0 then
			table.insert(self.m_ShowItemList,{itemId,bindCount,weight,1})
		end
	end

	table.sort(self.m_ShowItemList,function(a,b)
		if a[3] == b[3] then
			if a[1] == b[1] then return a[2] < b[2]
			else return a[1] < b[1] end
		else
			return a[3] > b[3]
		end
	end)

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_ShowItemList
    end, function(item, index)
        self:InitItem(item, self.m_ShowItemList[index + 1])
    end)
	self.TableView:ReloadData(true, false)
end

function LuaQMPHKUnlockZhanLingWnd:InitItem(item,data)
	local icon = item.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	local qualitySprite = item.transform:Find("ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
	local preciousSprite = item.transform:Find("ItemCell/BindSprite"):GetComponent(typeof(UISprite))
	local count = item.transform:Find("ItemCell/AmountLabel"):GetComponent(typeof(UILabel))
	local itemData = Item_Item.GetData(data[1])
	count.text = data[2]
	icon:LoadMaterial(itemData.Icon)
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
	local isPrecious = itemData.ValuablesFloorPrice > 0 and itemData.PlayerShopPrice == 0
	local isBind = data[4] == 1
	if isPrecious then
		preciousSprite.gameObject:SetActive(true)
		preciousSprite.spriteName = Constants.ItemPreciousSpriteName
	elseif isBind then
		preciousSprite.gameObject:SetActive(true)
		preciousSprite.spriteName = Constants.ItemBindedSpriteName
	else
		preciousSprite.gameObject:SetActive(false)
	end
	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
		CItemInfoMgr.ShowLinkItemTemplateInfo(data[1], false, nil, AlignType.Default, 0, 0, 0, 0)
	end)
end

--@region UIEvent

function LuaQMPHKUnlockZhanLingWnd:OnBuyBtnClick()
	if self.QnCostAndOwnMoney.moneyEnough then
		Gac2Gas.BuyQmpkAdvancedZhanLing()
	else
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function ()
            CShopMallMgr.ShowChargeWnd()
        end, nil, LocalString.GetString("前往"), nil, false)
	end

end

--@endregion UIEvent

