-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CHouseMsgItemWnd = import "L10.UI.CHouseMsgItemWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UICamera = import "UICamera"

CHouseMsgItemWnd_Extends = {}
function CHouseMsgItemWnd_Extends.Format_STEAL_EXTEA_ITEM(msgName, msgParam)
	local targetMsgParam = CreateFromClass(MakeGenericClass(List, Object))
    targetMsgParam:Add(msgParam[0])
    targetMsgParam:Add(msgParam[1])
    local secondPart = SafeStringFormat2("%sX%s", msgParam[2], msgParam[3])
    if msgParam.Count > 4 then
        do
            local i = 4
            while (i + 1) < msgParam.Count do
                secondPart = SafeStringFormat2(LocalString.GetString("%s、%sX%s"), secondPart, msgParam[i], msgParam[i + 1])
                i = i + 2
            end
        end
    end
    targetMsgParam:Add(secondPart)
    return g_MessageMgr:FormatMessage(msgName, CommonDefs.ListToArray(targetMsgParam))

end

CHouseMsgItemWnd.m_SetMsg_CS2LuaHook = function (this, news) 
    this.mNews = news
    if news.MessageParam.Data ~= nil and news.MessageParam.Data.Length > 0 then
        local msgParam = TypeAs(MsgPackImpl.unpack(news.MessageParam.Data), typeof(MakeGenericClass(List, Object)))
        local msg = ""
        if news.Message == "Crop_Stolen1" then
            msg = this:Format_Crop_Stolen1(news.Message, msgParam)
		elseif news.Message == "STEAL_EXTEA_ITEM" then
            msg = CHouseMsgItemWnd_Extends.Format_STEAL_EXTEA_ITEM(news.Message, msgParam)
        else
            msg = g_MessageMgr:FormatMessage(news.Message, CommonDefs.ListToArray(msgParam))
        end
        this.msgLbl.text = msg
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(news.Time)
        this.timeLbl.text = System.String.Format("{0}-{1}-{2}", time.Year, time.Month, time.Day)

        this.rewardGO:SetActive(news.RewardId ~= 0)

        this.rewardFx:LoadFx(CUIFxPaths.HouseNewsRewardFx)
        this.rewardFx.gameObject:SetActive(news.RewardId ~= 255)
        CUICommonDef.SetActive(this.rewardGO, news.RewardId ~= 255, true)

        local trans = this.rewardGO.transform:Find("sprite")
        if trans ~= nil then
            trans.gameObject:SetActive(news.RewardId == 255)
        end
    else
        this.msgLbl.text = ""
        this.timeLbl.text = ""
        this.rewardGO:SetActive(false)
    end
end
CHouseMsgItemWnd.m_Format_Crop_Stolen1_CS2LuaHook = function (this, msgName, msgParam) 
    local targetMsgParma = CreateFromClass(MakeGenericClass(List, Object))
    CommonDefs.ListAdd(targetMsgParma, typeof(Object), msgParam[0])
    local secondPart = System.String.Format("{0}X{1}", msgParam[1], msgParam[2])
    if msgParam.Count > 3 then
        do
            local i = 3
            while (i + 1) < msgParam.Count do
                secondPart = System.String.Format(LocalString.GetString("{0}、{1}X{2}"), secondPart, msgParam[i], msgParam[i + 1])
                i = i + 2
            end
        end
    end
    CommonDefs.ListAdd(targetMsgParma, typeof(Object), secondPart)
    return g_MessageMgr:FormatMessage(msgName, CommonDefs.ListToArray(targetMsgParma))
end
CHouseMsgItemWnd.m_OnLabelClick_CS2LuaHook = function (this, go) 
    local url = this.msgLbl:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end
CHouseMsgItemWnd.m_OnRewardClick_CS2LuaHook = function (this, go) 
    local k = CClientHouseMgr.Inst:GetNewsKey(this.mNews)
    if k <= 0 then
        return
    end

    Gac2Gas.RequestGetHouseNewsReward(k)
end
