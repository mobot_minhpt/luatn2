-- Auto Generated!!
local CSecondPwdSetWnd = import "L10.UI.CSecondPwdSetWnd"
CSecondPwdSetWnd.m_OnClickCOnfirmButton_CS2LuaHook = function (this, go) 
    if this.input1.value ~= this.input2.value then
        g_MessageMgr:ShowMessage("Secondary_Password_Not_Match")
        return
    end
    if CommonDefs.StringLength(this.input1.value) ~= 6 then
        g_MessageMgr:ShowMessage("Password_Length_Not_Legal")
        return
    end
    Gac2Gas.RequestSetSecondaryPassword(this.input1.value)
    --this.Close();
end
