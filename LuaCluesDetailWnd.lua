local UIGrid = import "UIGrid"
local UITable = import "UITable"
local UILabel = import "UILabel"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaCluesDetailWnd = class()
LuaCluesDetailWnd.OpenClueId = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCluesDetailWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaCluesDetailWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaCluesDetailWnd, "DescribeLabel", "DescribeLabel", UILabel)
RegistChildComponent(LuaCluesDetailWnd, "BottomBtnsWidget", "BottomBtnsWidget", GameObject)
RegistChildComponent(LuaCluesDetailWnd, "Subtable", "Subtable", UIGrid)
RegistChildComponent(LuaCluesDetailWnd, "SubButtonTemplate", "SubButtonTemplate", GameObject)

--@endregion RegistChildComponent end

function LuaCluesDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    if LuaCluesDetailWnd.OpenClueId then
        self:InitWndData()
        self:RefreshConstUI()
        self:RefreshVariableUI()
    end
end

function LuaCluesDetailWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaCluesDetailWnd:InitWndData()
    self.prefixPath = ""
end

function LuaCluesDetailWnd:RefreshConstUI()
    local readClues = LuaYiRenSiMgr.ReadClues and LuaYiRenSiMgr.ReadClues or 0
    local findClues = LuaYiRenSiMgr.FindClues and LuaYiRenSiMgr.FindClues or 0
    self.SubButtonTemplate:SetActive(false)
    local cluesTable = {}
    NanDuFanHua_YiRenSiClues.Foreach(function (k, v)
        if cluesTable[v.CluesType] then
            table.insert(cluesTable[v.CluesType].IdList, v.Id)
            table.insert(cluesTable[v.CluesType].DescribeList, v.Describe)
        else
            local srcName = v.CluesName
            local e = string.find(srcName,'%(')
            if e then
                srcName = string.sub(srcName,1,e-1)
            end

            local tbl = {IdList = {v.Id}, Name = srcName, CluesMaterial = v.CluesMaterial, DescribeList = {v.Describe}}
            cluesTable[v.CluesType] = tbl
        end
    end)
    local clueType = NanDuFanHua_YiRenSiClues.GetData(LuaCluesDetailWnd.OpenClueId).CluesType
    self.cfg = cluesTable[clueType]
    
    Extensions.RemoveAllChildren(self.Subtable.transform)
    self.subButtonList = {}
    if #self.cfg.IdList > 1 then
        for subId = 1, #self.cfg.IdList do
            local subTrans = CommonDefs.Object_Instantiate(self.SubButtonTemplate.transform)
            --Load Material
            subTrans.gameObject:SetActive(true)
            subTrans:SetParent(self.Subtable.transform)
            subTrans.localScale = Vector3.one
            subTrans.localPosition = Vector3.zero
            local cluesId = self.cfg.IdList[subId]
            local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
            local isRead = (bit.band(readClues, bit.lshift(1, cluesId - 1)) ~= 0)
            subTrans.transform:Find("DigitLabel"):GetComponent(typeof(UILabel)).text = EnumChineseDigits.GetDigit(subId)
            subTrans.transform:Find("RedDot").gameObject:SetActive(isFind and (not isRead))
            subTrans.transform:Find("DigitLabel").gameObject:SetActive(isFind)
            subTrans.transform:Find("LockedSprite").gameObject:SetActive(not isFind)
            table.insert(self.subButtonList, subTrans.gameObject)

            UIEventListener.Get(subTrans.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
                if isFind then
                    if not isRead then
                        Gac2Gas.YiRenSiReadClues(cluesId)
                    end
                    self:OnClickSubButtons(subId)
                else
                    g_MessageMgr:ShowMessage('YIRENSI_NO_CLUE_YET')
                end    
            end)
        end
        self.Subtable:Reposition()
    end
    
    self.Title.text = CUICommonDef.TranslateToNGUIText(self.cfg.Name)
    
    local clueObj = self.ItemCell
    clueObj.transform:Find("Mask/Tex"):GetComponent(typeof(CUITexture)):LoadMaterial(self.prefixPath .. self.cfg.CluesMaterial)
    if #self.cfg.IdList > 1 then
        --特殊线索
        local clueListLen = #self.cfg.IdList
        local findNumber = 0
        for t = 1, clueListLen do
            local cluesId = self.cfg.IdList[t]
            local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
            local isRead = (bit.band(readClues, bit.lshift(1, cluesId - 1)) ~= 0)
            findNumber = findNumber + (isFind and 1 or 0)
            if isFind and cluesId == LuaCluesDetailWnd.OpenClueId then
                self:OnClickSubButtons(t)
                if not isRead then
                    Gac2Gas.YiRenSiReadClues(cluesId)
                end
            end
        end
        clueObj.transform:Find("Mask/Tex").gameObject:SetActive(findNumber > 0)
        clueObj.transform:Find("Panel/LockedSprite").gameObject:SetActive(findNumber < clueListLen)
        clueObj.transform:Find("Panel/LockMask").gameObject:SetActive(findNumber < clueListLen and findNumber > 0)
        clueObj.transform:Find("Panel/LockMask"):GetComponent(typeof(UITexture)).fillAmount = 1 - (findNumber/clueListLen)
    else
        --普通线索
        local cluesId = self.cfg.IdList[1]
        local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
        clueObj.transform:Find("Mask/Tex").gameObject:SetActive(isFind)
        clueObj.transform:Find("Panel/LockedSprite").gameObject:SetActive(not isFind)
        clueObj.transform:Find("Panel/LockMask").gameObject:SetActive(false)
        self.DescribeLabel.text = CUICommonDef.TranslateToNGUIText(self.cfg.DescribeList[1])
    end
end

function LuaCluesDetailWnd:RefreshVariableUI()

end


function LuaCluesDetailWnd:OnClickSubButtons(subId)
    for i = 1, #self.subButtonList do
        local go = self.subButtonList[i]
        go.transform:Find("IsSelected").gameObject:SetActive(i == subId)
    end
    self.DescribeLabel.text = CUICommonDef.TranslateToNGUIText(self.cfg.DescribeList[subId])
end

function LuaCluesDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncYiRenSiFindCluesData", self, "OnSyncYiRenSiFindCluesData")
end

function LuaCluesDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncYiRenSiFindCluesData", self, "OnSyncYiRenSiFindCluesData")
end

function LuaCluesDetailWnd:OnSyncYiRenSiFindCluesData()
    local readClues = LuaYiRenSiMgr.ReadClues and LuaYiRenSiMgr.ReadClues or 0
    local findClues = LuaYiRenSiMgr.FindClues and LuaYiRenSiMgr.FindClues or 0
    if #self.cfg.IdList > 1 then
        for subId = 1, #self.cfg.IdList do
            local subTrans = self.subButtonList[subId]
            local cluesId = self.cfg.IdList[subId]
            local isFind = (bit.band(findClues, bit.lshift(1, cluesId - 1)) ~= 0)
            local isRead = (bit.band(readClues, bit.lshift(1, cluesId - 1)) ~= 0)
            subTrans.transform:Find("RedDot").gameObject:SetActive(isFind and (not isRead))
        end
    end
end
