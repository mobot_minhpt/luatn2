local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
LuaSpringCouponsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSpringCouponsWnd, "Vehicle", "Vehicle", CUITexture)
RegistChildComponent(LuaSpringCouponsWnd, "PreviewBtn", "PreviewBtn", GameObject)
RegistChildComponent(LuaSpringCouponsWnd, "Hint", "Hint", UILabel)
RegistChildComponent(LuaSpringCouponsWnd, "QnTableView1", "QnTableView1", QnTableView)
RegistChildComponent(LuaSpringCouponsWnd, "JoinActivityButton", "JoinActivityButton", GameObject)
RegistChildComponent(LuaSpringCouponsWnd, "GoShopButton", "GoShopButton", GameObject)
RegistChildComponent(LuaSpringCouponsWnd, "ZuoqiName", "ZuoqiName", UILabel)
RegistChildComponent(LuaSpringCouponsWnd, "RewardTips", "RewardTips", UILabel)

--@endregion RegistChildComponent end

function LuaSpringCouponsWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:InitUIEvent()
    self:RefreshConstInfo()
end

function LuaSpringCouponsWnd:InitWndData()
    self.ZuoqiId = HanJia2023_SpringCouponsSetting.GetData().ZuoqiID
    self.vehicleId = ZuoQi_ZuoQi.GetData(self.ZuoqiId).ItemID
    self.itemRewards = HanJia2023_SpringCouponsSetting.GetData().ItemRewards
    self.itemCount = self.itemRewards.Length

    self.chunjieJieRiId = 22
    self.chunjieActivityId = 42030285

    self.hanjiaJieRiId = 27
    self.newLimitTimeActivityId = 42030283
end

function LuaSpringCouponsWnd:InitUIEvent()
    UIEventListener.Get(self.JoinActivityButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local isOpen = CScheduleMgr.Inst:IsCanJoinSchedule(self.chunjieActivityId, true)

        if isOpen then
            LuaChunJie2023Mgr:OpenChunJie2023MainWnd()
        else
            g_MessageMgr:ShowMessage("CHUNJIE2023_TIME_OVER")
        end
    end)

    self.GoShopButton:SetActive(false)
    UIEventListener.Get(self.GoShopButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local isOpen = CScheduleMgr.Inst:IsCanJoinSchedule(self.newLimitTimeActivityId, true)

        if isOpen then
            CShopMallMgr.SelectMallIndex = 0
            CShopMallMgr.SelectCategory = 6
            CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
        else
            g_MessageMgr:ShowMessage("CHUNJIE2023_SHOPMALL_TIME_OVER")
        end
    end)

    UIEventListener.Get(self.PreviewBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CZuoQiMgr.Inst:ShowVehiclePreview(self.ZuoqiId)
    end)
end

function LuaSpringCouponsWnd:RefreshConstInfo()
    self.ZuoqiName.text = g_MessageMgr:FormatMessage("CHUNJIE2023_ZUOQI_NAME")
    self.Hint.text = g_MessageMgr:FormatMessage("CHUNJIE2023_JIANGQUAN_DESC")
    self.RewardTips.text = g_MessageMgr:FormatMessage("CHUNJIE2023_JIANGQUAN_TIPS")

    local initfunc = function(item,index)
        self:FillItems(item,index)
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(self.itemCount, initfunc)
    self.QnTableView1:ReloadData(true, true)
end

function LuaSpringCouponsWnd:FillItems(item, index)--ok
    local cell = item.transform:Find("ItemCell")
    local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
    --local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
    local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label

    local itemid = self.itemRewards[index]
    local isBind = false
    local count = 1

    local itemcfg = Item_Item.GetData(itemid)
    if itemcfg then
        icon:LoadMaterial(itemcfg.Icon)
    end
    --bindgo:SetActive(tonumber(isBind) == 1)
    if tonumber(count) <= 1 then
        countlb.text = ""
    else
        countlb.text = count
    end

    cell.gameObject:SetActive(true)

    UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaSpringCouponsWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

