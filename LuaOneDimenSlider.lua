local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local UILongPressButton = import "L10.UI.UILongPressButton"

LuaOneDimenSlider = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOneDimenSlider, "Slider", "Slider", UISlider)
RegistChildComponent(LuaOneDimenSlider, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaOneDimenSlider, "NumLabel", "NumLabel", UILabel)
RegistChildComponent(LuaOneDimenSlider, "DecreaseButton", "DecreaseButton", QnButton)
RegistChildComponent(LuaOneDimenSlider, "IncreaseButton", "IncreaseButton", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaOneDimenSlider, "m_OnSliderChangeValueAction")
RegistClassMember(LuaOneDimenSlider, "m_RecordLastValueAction")
RegistClassMember(LuaOneDimenSlider, "m_DefaultOnChangeValueAction")
RegistClassMember(LuaOneDimenSlider, "m_NullOnChangeValueAction")
RegistClassMember(LuaOneDimenSlider, "m_MinValue")
RegistClassMember(LuaOneDimenSlider, "m_MaxValue")
RegistClassMember(LuaOneDimenSlider, "m_Value")
RegistClassMember(LuaOneDimenSlider, "m_LongPressTick")

function LuaOneDimenSlider:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDecreaseButtonClick()
	end)


	
	UIEventListener.Get(self.IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)


    --@endregion EventBind end
	self.Slider.onDragFinished = LuaUtils.OnDragFinished(function() 
        self:OnDragFinished()
    end)

	UIEventListener.Get(self.Slider.thumb.gameObject).onDragStart = DelegateFactory.VoidDelegate(function (go)
        self:OnDragStart()
    end)

	self.m_NullOnChangeValueAction = DelegateFactory.Action_float(function(v) end)
	self.m_DefaultOnChangeValueAction = DelegateFactory.Action_float(function(v)
		self:OnChangeValue(v, false)
    end)
	self.Slider.OnChangeValue = self.m_DefaultOnChangeValueAction

	local longPressDecreaseButton = self.DecreaseButton.gameObject:GetComponent(typeof(UILongPressButton))
	longPressDecreaseButton.OnLongPressDelegate = DelegateFactory.Action(function () 
		self:OnDecreaseButtonPress()
	end)
	longPressDecreaseButton.OnLongPressEndDelegate = DelegateFactory.Action(function () 
		self:OnDecreaseButtonPressEnd()
	end)

	local longPressIncreaseButton = self.IncreaseButton.gameObject:GetComponent(typeof(UILongPressButton))
	longPressIncreaseButton.OnLongPressDelegate = DelegateFactory.Action(function () 
		self:OnIncreaseButtonPress()
	end)
	longPressIncreaseButton.OnLongPressEndDelegate = DelegateFactory.Action(function () 
		self:OnIncreaseButtonPressEnd()
	end)
end

function LuaOneDimenSlider:Init(title, minValue, maxValue, onSliderChangeValueAction, recordLastValueAction)
	if title then
        self.TitleLabel.text = title
    end
	self.m_OnSliderChangeValueAction = onSliderChangeValueAction
	self.m_RecordLastValueAction = recordLastValueAction
	self.m_MinValue = math.floor(minValue)
	self.m_MaxValue = math.floor(maxValue)
end

function LuaOneDimenSlider:SetDefaultValue(defaultValue)
	self.Slider.OnChangeValue = self.m_NullOnChangeValueAction
	self.Slider.value = defaultValue
    self:OnChangeValue(defaultValue, true)
    self.Slider.OnChangeValue = self.m_DefaultOnChangeValueAction
end

function LuaOneDimenSlider:InitSliderWithNumVal(newVal, ingnoreCallback)
	self.Slider.mValue = (newVal - self.m_MinValue) / (self.m_MaxValue - self.m_MinValue)
	self.Slider:ForceUpdate()
	self:OnChangeValue(self.Slider.mValue, true)
	if self.m_OnSliderChangeValueAction and not ingnoreCallback then
		self.m_OnSliderChangeValueAction(math.min(1, math.max(-1, self.Slider.value)), true)
	end
end
--@region UIEvent

function LuaOneDimenSlider:OnDecreaseButtonClick()
	if not self.m_Value or not self.m_MinValue or not self.m_MaxValue then return end
	self:CancelLongPressTick()
	if self.m_RecordLastValueAction then
		local val = self.Slider.value
		self.m_RecordLastValueAction(val)
	end 
	local newVal = math.max(self.m_MinValue, self.m_Value - 1) 
	self:InitSliderWithNumVal(newVal)
end

function LuaOneDimenSlider:OnIncreaseButtonClick()
	if not self.m_Value or not self.m_MinValue or not self.m_MaxValue then return end
	self:CancelLongPressTick()
	if self.m_RecordLastValueAction then
		local val = self.Slider.value
		self.m_RecordLastValueAction(val)
	end 
	local newVal = math.min(self.m_MaxValue, self.m_Value + 1) 
	self:InitSliderWithNumVal(newVal)
end

function LuaOneDimenSlider:OnDragFinished()
	local isRecord = true
	if self.m_OnSliderChangeValueAction then
		self.m_OnSliderChangeValueAction(math.min(1, math.max(-1, self.Slider.value)), isRecord)
	end
end

function LuaOneDimenSlider:OnDragStart()
	if self.m_RecordLastValueAction then
		local val = self.Slider.value
		self.m_RecordLastValueAction(val)
	end 
end

function LuaOneDimenSlider:OnChangeValue(v, ingnoreCallback)
	self.m_Value = math.floor(math.lerp(self.m_MinValue, self.m_MaxValue, v) + 0.5)
	self.NumLabel.text = self.m_Value
	self.DecreaseButton.Enabled = self.m_Value ~= self.m_MinValue
	self.IncreaseButton.Enabled = self.m_Value ~= self.m_MaxValue
	if self.m_OnSliderChangeValueAction and not ingnoreCallback then
		self.m_OnSliderChangeValueAction(math.min(1, math.max(-1, v)))
	end 
end

function LuaOneDimenSlider:OnDecreaseButtonPress()
	if not self.m_Value or not self.m_MinValue or not self.m_MaxValue then return end
	if self.m_RecordLastValueAction then
		local val = self.Slider.value
		self.m_RecordLastValueAction(val)
	end 
	self:CancelLongPressTick()
	self.m_LongPressTick = RegisterTick(function ()
		local newVal = math.max(self.m_MinValue, self.m_Value - 1) 
		self:InitSliderWithNumVal(newVal, true)
		if newVal == self.m_MinValue then
			self:CancelLongPressTick()
		end
	end,100)
end

function LuaOneDimenSlider:OnDecreaseButtonPressEnd()
	self:CancelLongPressTick()
	if self.m_OnSliderChangeValueAction then
		self.m_OnSliderChangeValueAction(math.min(1, math.max(-1, self.Slider.value)), true)
	end
end

function LuaOneDimenSlider:OnIncreaseButtonPress()
	if not self.m_Value or not self.m_MinValue or not self.m_MaxValue then return end
	if self.m_RecordLastValueAction then
		local val = self.Slider.value
		self.m_RecordLastValueAction(val)
	end 
	self:CancelLongPressTick()
	self.m_LongPressTick = RegisterTick(function ()
		local newVal = math.min(self.m_MaxValue, self.m_Value + 1) 
		self:InitSliderWithNumVal(newVal, true)
		if newVal == self.m_MaxValue then
			self:CancelLongPressTick()
		end
	end,100)
end

function LuaOneDimenSlider:OnIncreaseButtonPressEnd()
	self:CancelLongPressTick()
	if self.m_OnSliderChangeValueAction then
		self.m_OnSliderChangeValueAction(math.min(1, math.max(-1, self.Slider.value)), true)
	end
end

--@endregion UIEvent

function LuaOneDimenSlider:OnDisable()
	self:CancelLongPressTick()
end

function LuaOneDimenSlider:CancelLongPressTick()
	if self.m_LongPressTick then
		UnRegisterTick(self.m_LongPressTick)
		self.m_LongPressTick = nil
	end
end
