local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local CUIResources = import "L10.UI.CUIResources"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local CAchievementMgr = import "L10.Game.CAchievementMgr"

LuaProfessionTransferWnd = class()

RegistClassMember(LuaProfessionTransferWnd,"m_SourcePortrait")
RegistClassMember(LuaProfessionTransferWnd,"m_TargetPortrait")
RegistClassMember(LuaProfessionTransferWnd,"m_SourceProfessionLabel")
RegistClassMember(LuaProfessionTransferWnd,"m_TargetProfessionLabel")
RegistClassMember(LuaProfessionTransferWnd,"m_ChoseTargetBtn")
RegistClassMember(LuaProfessionTransferWnd,"m_ChoseTargetSprite")
RegistClassMember(LuaProfessionTransferWnd,"m_TransferBtn")
RegistClassMember(LuaProfessionTransferWnd,"m_InfoBtn")

RegistClassMember(LuaProfessionTransferWnd,"m_TargetCls")

function LuaProfessionTransferWnd:InitComponents()
    self.m_SourcePortrait = self.transform:Find("Anchor/Source/Portrait"):GetComponent(typeof(CUITexture))
    self.m_TargetPortrait = self.transform:Find("Anchor/Target/Portrait"):GetComponent(typeof(CUITexture))
    self.m_SourceProfessionLabel = self.transform:Find("Anchor/Source/Profession"):GetComponent(typeof(UILabel))
    self.m_TargetProfessionLabel = self.transform:Find("Anchor/Target/Profession"):GetComponent(typeof(UILabel))
    self.m_ChoseTargetBtn = self.transform:Find("Anchor/Target/Portrait").gameObject
    self.m_ChoseTargetSprite = self.transform:Find("Anchor/Target/Portrait/ChoseTargetSprite").gameObject
    self.m_TransferBtn = self.transform:Find("Anchor/TransferBtn"):GetComponent(typeof(CButton))
    self.m_InfoBtn = self.transform:Find("Anchor/InfoBtn").gameObject
    self.m_TargetCls = EnumClass.Undefined
end

function LuaProfessionTransferWnd:Awake()
    self:InitComponents()
end

function LuaProfessionTransferWnd:Init()
    self.m_TargetCls = EnumClass.Undefined
    self.m_SourcePortrait:Clear()
    self.m_TargetPortrait:Clear()
    self.m_SourceProfessionLabel.text = nil
    self.m_TargetProfessionLabel.text = LocalString.GetString("点击加号添加")
    self.m_TransferBtn.Enabled = false
    self.m_ChoseTargetSprite:SetActive(true)
    self.m_TargetProfessionLabel.color = Color.gray
    if CClientMainPlayer.Inst ~= nil then
        self.m_SourcePortrait:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
        self.m_SourceProfessionLabel.text = Profession.GetFullName(CClientMainPlayer.Inst.Class)
    end
end

function LuaProfessionTransferWnd:Start()
    UIEventListener.Get(self.m_ChoseTargetBtn).onClick = DelegateFactory.VoidDelegate(function () self:OnChoseTargetButtonClick() end)
    UIEventListener.Get(self.m_TransferBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function () self:OnTransferButtonClick() end)
    UIEventListener.Get(self.m_InfoBtn).onClick = DelegateFactory.VoidDelegate(function () self:OnInfoButtonClick() end)
end

function LuaProfessionTransferWnd:OnEnable()
    g_ScriptEvent:AddListener("GetAchievementAwardSuccess",self,"GetAchievementAwardSuccess")
    g_ScriptEvent:AddListener("OnTargetProfessionSelected",self,"OnTargetProfessionSelected")
end

function LuaProfessionTransferWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetAchievementAwardSuccess",self,"GetAchievementAwardSuccess")
    g_ScriptEvent:RemoveListener("OnTargetProfessionSelected",self,"OnTargetProfessionSelected")
end

function LuaProfessionTransferWnd:OnChoseTargetButtonClick()
    CUIManager.ShowUI(CUIResources.ProfessionOptionsWnd)
end

function LuaProfessionTransferWnd:OnTransferButtonClick()
    --转职前如果有未领取奖励的成就，先领取，避免转职后职业不符无法领取
    if CAchievementMgr.Inst:CheckAchievementAvailable() then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ProfessionTransfer_Achievment_CONFIRM"), DelegateFactory.Action(function () 
            LuaAchievementMgr.GetAllAchievementsBeforeProfessionTransfer()
        end), nil, LocalString.GetString("领取"), nil, false)
        return
    end
    self:DoRequestTransfer()
end

function LuaProfessionTransferWnd:DoRequestTransfer()
    local message = g_MessageMgr:FormatMessage("ProfessionTransfer_CONFIRM", Profession.GetFullName(self.m_TargetCls))
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        Gac2Gas.RequestDoTransfer(EnumToInt(self.m_TargetCls))
        self:Close()
    end), nil, LocalString.GetString("转职"), nil, false)
end

function LuaProfessionTransferWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("ProfessionTransfer_Description2")
end

function LuaProfessionTransferWnd:GetAchievementAwardSuccess(args)
    local achievementId = args[0]
    local context = args[1]
    if context == "professiontransfer" and not CAchievementMgr.Inst:CheckAchievementAvailable() then
        self:DoRequestTransfer()
    end
end

function LuaProfessionTransferWnd:OnTargetProfessionSelected(args)
    local cls = args[0]
    if CClientMainPlayer.Inst ~= nil and cls ~= EnumClass.Undefined then
        self.m_TargetCls = cls
        self.m_ChoseTargetSprite:SetActive(false)
        self.m_TargetPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, CClientMainPlayer.Inst.Gender, -1), false)
        self.m_TargetProfessionLabel.text = Profession.GetFullName(cls)
        self.m_TargetProfessionLabel.color = Color.white
        self.m_TransferBtn.Enabled = true
    end
end

function LuaProfessionTransferWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
