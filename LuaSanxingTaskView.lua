require("common/common_include")

local CButton = import "L10.UI.CButton"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

LuaSanxingTaskView = class()

RegistChildComponent(LuaSanxingTaskView, "TaskName", UILabel)
RegistChildComponent(LuaSanxingTaskView, "TaskInfo", GameObject)
RegistChildComponent(LuaSanxingTaskView, "InviteBtn", CButton)
RegistChildComponent(LuaSanxingTaskView, "LeaveBtn", CButton)
RegistChildComponent(LuaSanxingTaskView, "bg", UISprite)
RegistChildComponent(LuaSanxingTaskView, "TaskDes1", UILabel)
RegistChildComponent(LuaSanxingTaskView, "TaskDes2", UILabel)
RegistChildComponent(LuaSanxingTaskView, "TaskDes3", UILabel)

function LuaSanxingTaskView:Awake()
  self.TaskName.text = nil
  --self.TaskInfo.text = nil
end

function LuaSanxingTaskView:Init()
  UIEventListener.Get(self.LeaveBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    CGamePlayMgr.Inst:LeavePlay()
  end)
  UIEventListener.Get(self.InviteBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    Gac2Gas.QuerySanXingBattlePlayInfo()
  end)

  self:InitPhaText()
end

function LuaSanxingTaskView:InitPhaText()
  local sceneName = CScene.MainScene.SceneName

  local pha1Text,pha2Text,pha3Text
  if LuaSanxingGamePlayMgr.sanxingForce == 0 then
    pha1Text,pha2Text,pha3Text =
    g_MessageMgr:FormatMessage("SanxingDefendPha1"),
    g_MessageMgr:FormatMessage("SanxingDefendPha2"),
    g_MessageMgr:FormatMessage("SanxingDefendPha3")
  elseif LuaSanxingGamePlayMgr.sanxingForce == 1 then
    pha1Text,pha2Text,pha3Text =
    g_MessageMgr:FormatMessage("SanxingAttackPha1"),
    g_MessageMgr:FormatMessage("SanxingAttackPha2"),
    g_MessageMgr:FormatMessage("SanxingAttackPha3")
  end
  if not pha1Text then
    return
  end

  local showText = '[c][FBC701]' .. sceneName .. '[-][c]'
  local textTable = {pha1Text,pha2Text,pha3Text}
  for i,v in ipairs(textTable) do
    if LuaSanxingGamePlayMgr.sanxingStage and LuaSanxingGamePlayMgr.sanxingStage == i then
      textTable[i] = '[c][DBDB58]' .. v .. '[-][c]'
    else
      textTable[i] = v
    end
  end

  self.TaskName.text = showText
  self.TaskDes1.text = textTable[1]
  self.TaskDes2.text = textTable[2]
  self.TaskDes3.text = textTable[3]

  --self.TaskDes1:ResetAndUpdateAnchors()
  --self.TaskDes2:ResetAndUpdateAnchors()
  --self.TaskDes3:ResetAndUpdateAnchors()
  --self.TaskInfo:ResetAndUpdateAnchors()
  --self.bg:ResetAndUpdateAnchors()
end

function LuaSanxingTaskView:GetRemainTimeText(totalSeconds)
  if CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("GodCoronationWait"))  == 1 or
  CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("SeeGodCoronationWait")) == 1 then
    totalSeconds = 600 - (3600 - totalSeconds)
  end
  if totalSeconds < 0 then
    totalSeconds = 0
  end
  if totalSeconds >= 3600 then
    return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
  else
    return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
  end
end

function LuaSanxingTaskView:OnEnable()
  g_ScriptEvent:AddListener("SanxingStageUpdate", self, "InitPhaText")
end

function LuaSanxingTaskView:OnDisable()
  g_ScriptEvent:RemoveListener("SanxingStageUpdate", self, "InitPhaText")
end

return LuaSanxingTaskView
