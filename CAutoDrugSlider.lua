-- Auto Generated!!
local CAutoDrugSlider = import "L10.UI.CAutoDrugSlider"
local CGameSettingMgr = import "L10.Game.CGameSettingMgr"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
CAutoDrugSlider.m_Init_CS2LuaHook = function (this, isHpSlider)
    this.m_UISlider.value = 0.5
    this.isHpSlider = isHpSlider
    this.AutoHealInfo = CGameSettingMgr.Inst:GetAutoHealInfo()
    if this.AutoHealInfo ~= nil and this.AutoHealInfo.Length >= 6 then
        local index = isHpSlider and 1 or 3
        this.m_UISlider.value = ((System.Int32.Parse(this.AutoHealInfo[index]))) / 100
        this.m_Threshold = this.AutoHealInfo[index]
    end
    this:SetText()
end
CAutoDrugSlider.m_SetText_CS2LuaHook = function (this)
    if this.isHpSlider then
        this.textLabel.text = System.String.Format(LocalString.GetString("[c][{0}]人物气血低于 [FFED5F]{1}[-] %时自动补充[-][/c]"), NGUIText.EncodeColor24(this.textLabel.color), this.m_Threshold)
    else
        this.textLabel.text = System.String.Format(LocalString.GetString("[c][{0}]人物法力低于 [FFED5F]{1}[-] %时自动补充[-][/c]"), NGUIText.EncodeColor24(this.textLabel.color), this.m_Threshold)
    end
end
