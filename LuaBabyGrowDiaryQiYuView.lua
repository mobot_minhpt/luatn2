require("common/common_include")

local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local ShareMgr=import "ShareMgr"
local BabyMgr = import "L10.Game.BabyMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CButton = import "L10.UI.CButton"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Main = import "L10.Engine.Main"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Texture = import "UnityEngine.Texture"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"

LuaBabyGrowDiaryQiYuView = class()

RegistClassMember(LuaBabyGrowDiaryQiYuView, "PageButton")
RegistClassMember(LuaBabyGrowDiaryQiYuView, "QiYuGrid")
RegistClassMember(LuaBabyGrowDiaryQiYuView, "QiYuTemplate")

RegistClassMember(LuaBabyGrowDiaryQiYuView, "QiYuInfos")
RegistClassMember(LuaBabyGrowDiaryQiYuView, "QiYuNumPerPage")
RegistClassMember(LuaBabyGrowDiaryQiYuView, "QiYuMaxPage")
RegistClassMember(LuaBabyGrowDiaryQiYuView, "CurrentPage")

RegistClassMember(LuaBabyGrowDiaryQiYuView, "SelectedBaby")

function LuaBabyGrowDiaryQiYuView:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyGrowDiaryQiYuView:InitClassMembers()
	self.PageButton = self.transform:Find("PageButton"):GetComponent(typeof(QnAddSubAndInputButton))
	self.QiYuGrid = self.transform:Find("QiYuGrid"):GetComponent(typeof(UIGrid))
	self.QiYuTemplate = self.transform:Find("QiYuTemplate").gameObject
	self.QiYuTemplate:SetActive(false)
	
end

function LuaBabyGrowDiaryQiYuView:InitValues()

	self.QiYuNumPerPage = 4
	self.CurrentPage = 1

    local onPageChange = function (value)
		self:OnPageChange(value)
	end
	self.PageButton:SetNumberInputAlignType(AlignType.Right)
	self.PageButton.onValueChanged  = DelegateFactory.Action_uint(onPageChange)
	self.PageButton.ImmediateCallBackWhenInput = false

	self:UpdateQiYus()
end

function LuaBabyGrowDiaryQiYuView:UpdateQiYus()

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then
		CUIManager.CloseUI(CLuaUIResources.BabyGrowDiaryWnd)
		return
	end
	self.SelectedBaby = selectedBaby

	self.QiYuInfos = {}
	local qiyuDict = self.SelectedBaby.Props.ScheduleData.QiYuData
	if not qiyuDict then return end

	local taskData = self.SelectedBaby.Props.TaskData
	if not taskData then return end

	local finishedTasks = taskData.FinishedTasks
	local acceptedTasks = taskData.AcceptedTasks

	Baby_QiYu.ForeachKey(function (key)
        local data = Baby_QiYu.GetData(key)

        local isUnlocked = CommonDefs.DictContains_LuaCall(qiyuDict, key)
        local isGifted = false
        if CommonDefs.DictContains_LuaCall(qiyuDict, key) then
        	local qiyuInfo = qiyuDict[key]
        	isGifted = qiyuInfo.ReceivedAward == 0
        end
        local picPath = data.MalePicPathShare
        if self.SelectedBaby.Gender == 1 then
        	picPath = data.FemalePicPathShare
    	end

    	local canAcceptTask = false
    	if data.TaskId ~= 0 and self.SelectedBaby.Grade >= data.GetLevel then
    		
    		if not CommonDefs.DictContains_LuaCall(finishedTasks, data.TaskId) and not CommonDefs.DictContains_LuaCall(acceptedTasks, data.TaskId) then
    			canAcceptTask = true
    		end

    		-- 有可接任务且已经完成后，才解锁
    		if CommonDefs.DictContains_LuaCall(finishedTasks, data.TaskId) then
    			isUnlocked = true
    		end
    	end


        table.insert(self.QiYuInfos, {
        	Id = data.ID,
        	PicPath = picPath,
        	Description = data.Description,
        	IsUnlocked =  isUnlocked, 
        	IsGifted = isGifted,
        	CanAcceptTask = canAcceptTask,
        	})
        
    end)

    self.QiYuMaxPage = math.ceil(#self.QiYuInfos / self.QiYuNumPerPage)
    self.PageButton:SetMinMax(1, self.QiYuMaxPage, 1)

    self.PageButton:SetValue(self.CurrentPage, true)
end

function LuaBabyGrowDiaryQiYuView:OnPageChange(value)
	local txt = SafeStringFormat3(LocalString.GetString("%s/%s"), tostring(value), tostring(self.QiYuMaxPage))
	self.PageButton:OverrideText(txt)
	self.CurrentPage = value

	CUICommonDef.ClearTransform(self.QiYuGrid.transform)

	local low = (value - 1) * self.QiYuNumPerPage + 1
	local high = (value - 1) * self.QiYuNumPerPage + 4
	if high > #self.QiYuInfos then
		high = #self.QiYuInfos
	end
	for i = low, high, 1 do
		local go = NGUITools.AddChild(self.QiYuGrid.gameObject, self.QiYuTemplate)
    	self:InitQiYuItem(go, self.QiYuInfos[i])
        go:SetActive(true)
	end

	self.QiYuGrid:Reposition()
end

function LuaBabyGrowDiaryQiYuView:InitQiYuItem(go, info)
	local UnlockLabel = go.transform:Find("UnlockLabel"):GetComponent(typeof(UILabel))
	UnlockLabel.text = ""
	local QiYuTexture = go.transform:Find("QiYuTexture"):GetComponent(typeof(CUITexture))
	QiYuTexture.gameObject:SetActive(true)
	QiYuTexture:Clear()
	local GiftedBorder = go.transform:Find("QiYuTexture/GiftedBorder").gameObject
	GiftedBorder:SetActive(false)
	local GiftFx = go.transform:Find("QiYuTexture/GiftFx"):GetComponent(typeof(CUIFx))
	GiftFx:DestroyFx()
	local GiftBorderFx = go.transform:Find("QiYuTexture/GiftBorderFx"):GetComponent(typeof(CUIFx))
	GiftBorderFx:DestroyFx()

	local button = go.transform:Find("BG"):GetComponent(typeof(CButton))
	button.Enabled = info.IsUnlocked
	local AcceptBtn = go.transform:Find("AcceptBtn").gameObject
	AcceptBtn:SetActive(false)

	if info.IsUnlocked then
		self:DownLoadPic(go, info.PicPath, QiYuTexture.texture)
		--QiYuTexture:LoadMaterial(info.PicPath)
	else
		UnlockLabel.text = info.Description
	end


	GiftedBorder:SetActive(info.IsGifted)
	if info.IsGifted then
		GiftFx:LoadFx(CUIFxPaths.BabyDiaryTiXing)
		GiftBorderFx:LoadFx(CUIFxPaths.BabyDiaryLiuGuang)
	end


	local onClickQiYuTexture = function (go)
		self:OnClickQiYuTexture(go, info)
	end

    CommonDefs.AddOnClickListener(button.gameObject, DelegateFactory.Action_GameObject(onClickQiYuTexture), false)

    AcceptBtn:SetActive(info.CanAcceptTask)
    local onAcceptBtnClicked = function (go)
    	self:OnAcceptBtnClicked(go, info)
    end
    CommonDefs.AddOnClickListener(AcceptBtn, DelegateFactory.Action_GameObject(onAcceptBtnClicked), false)

end

function LuaBabyGrowDiaryQiYuView:DownLoadPic(go, path, uiTexture)
	path = ShareMgr.GetWebImagePath(path)
	Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(path, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
		if not go then return end
        local _texture = CreateFromClass(Texture2D, 4, 4, TextureFormat.RGBA32, false)
        CommonDefs.LoadImage(_texture,bytes)
        local texture = TypeAs(_texture, typeof(Texture))
        uiTexture.mainTexture = texture
    end)))
end

function LuaBabyGrowDiaryQiYuView:OnClickQiYuTexture(go, info)
	LuaBabyMgr.m_SelectedQiYuId = info.Id
	CUIManager.ShowUI(CLuaUIResources.BabyQiYuShareWnd)
end

function LuaBabyGrowDiaryQiYuView:OnAcceptBtnClicked(go, info)
	local qiyu = Baby_QiYu.GetData(info.Id)
	if qiyu then
		Gac2Gas.RequestAcceptQiYuTaskFromBaby(self.SelectedBaby.Id , info.Id)
	end
end

function LuaBabyGrowDiaryQiYuView:OnEnable()
	g_ScriptEvent:AddListener("AcceptQiYuTaskFromBabySuccess", self, "UpdateQiYuData")
	g_ScriptEvent:AddListener("ReceiveQiYuAwardSuccess", self, "UpdateQiYuData")
end

function LuaBabyGrowDiaryQiYuView:OnDisable()
	g_ScriptEvent:RemoveListener("AcceptQiYuTaskFromBabySuccess", self, "UpdateQiYuData")
	g_ScriptEvent:RemoveListener("ReceiveQiYuAwardSuccess", self, "UpdateQiYuData")
end

function LuaBabyGrowDiaryQiYuView:UpdateQiYuData(babyId)
	if babyId == self.SelectedBaby.Id then
		self:UpdateQiYus()
	end
end

return LuaBabyGrowDiaryQiYuView