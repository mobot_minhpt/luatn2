local GameObject                = import "UnityEngine.GameObject"
local Vector3                   = import "UnityEngine.Vector3"
local Quaternion                = import "UnityEngine.Quaternion"
local CommonDefs                = import "L10.Game.CommonDefs"
local CPayMgr                   = import "L10.Game.CPayMgr"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CUITexture				= import "L10.UI.CUITexture"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local LuaGameObject             = import "LuaGameObject"
local UIGrid                    = import "UIGrid"

--天成酒壶购买高级酒壶界面
CLuaTcjhBuyAdvPassWnd = class()

RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "BuyAdvPassBtn1",   GameObject)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "BuyAdvPassBtn2",   GameObject)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "ItemCell",         GameObject)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "Grid1",            UIGrid)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "Grid21",           UIGrid)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "Grid22",           UIGrid)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "Title1PriceLabel", UILabel)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "Title2PriceLabel", UILabel)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "ItemIcon1",        CUITexture)
RegistChildComponent(CLuaTcjhBuyAdvPassWnd, "ItemIcon2",        CUITexture)

RegistClassMember(CLuaTcjhBuyAdvPassWnd,  "SelectedItem")

function CLuaTcjhBuyAdvPassWnd:Awake()
    self.SelectedItem = nil
    UIEventListener.Get(self.BuyAdvPassBtn1).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick(1)
    end)
    UIEventListener.Get(self.BuyAdvPassBtn2).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyAdvPassBtnClick(2)
    end)
end

function CLuaTcjhBuyAdvPassWnd:Init( )
    local setting = HanJia2021_TianChengSetting.GetData()

    self.Title1PriceLabel.text = setting.OriPrice1
    self.Title2PriceLabel.text = setting.OriPrice2

    local iconpath = "UI/Texture/Item_Other/Material/other_1234.mat"
    self.ItemIcon1:LoadMaterial(iconpath)
    self.ItemIcon2:LoadMaterial(iconpath)

    local buybtnlb1 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn1.transform,"Label").label
    buybtnlb1.text = setting.BuyPrice1

    local buybtnlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"Label").label
    local buybtnextlb2 = LuaGameObject.GetChildNoGC(self.BuyAdvPassBtn2.transform,"ExtLabel").label

    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({";"}, 1, MakeArrayClass(System.String))
    local btnstrs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(setting.BuyPrice2, splits, op)
    buybtnlb2.text = btnstrs[0]
    buybtnextlb2.text = btnstrs[1]
    local des1 = setting.VIP1Des
    local des2 = setting.VIP2Des

    local nmldesarray = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(des1, splits, op)
    local advdesarray = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(des2, splits, op)
    self:FillItems1(nmldesarray)
    self:FillItems2(advdesarray)
end

function CLuaTcjhBuyAdvPassWnd:FillItems(desarray,grid,isleft)
    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({","}, 1, MakeArrayClass(System.String))
    local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(desarray, splits, op)
    local len = strs.Length
    for i=0,len-1,2 do
        local go = GameObject.Instantiate(self.ItemCell, Vector3.zero, Quaternion.identity)
        go.transform.parent = grid.transform
        go.transform.localScale = Vector3.one
        self:FillItem(go,strs[i],strs[i+1],isleft)
        go:SetActive(true)
    end
    grid:Reposition()
end

function CLuaTcjhBuyAdvPassWnd:FillItems1(desarray)
    self:FillItems(desarray[0],self.Grid1,true)
end

function CLuaTcjhBuyAdvPassWnd:FillItems2(desarray)
    self:FillItems(desarray[0],self.Grid21,false)
    self:FillItems(desarray[1],self.Grid22,false)
end

function CLuaTcjhBuyAdvPassWnd:FillItem(item,itemid,itemcount,isleft)
    local itemcfg = Item_Item.GetData(itemid)
    local iconTex = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ctTxt = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    if itemcfg then
        iconTex:LoadMaterial(itemcfg.Icon)
    end

    if tonumber(itemcount) <= 1 then
        ctTxt.text = ""
    else
        ctTxt.text = itemcount
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local atype
        if isleft then
            atype = AlignType.ScreenRight
        else
            atype = AlignType.ScreenLeft
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, atype, 0, 0, 0, 0) 
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end
    end)
end

function CLuaTcjhBuyAdvPassWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

--@region UIEvent

function CLuaTcjhBuyAdvPassWnd:OnBuyAdvPassBtnClick(type)
    local setting = HanJia2021_TianChengSetting.GetData()
    if type ==1 then
        local msg = g_MessageMgr:FormatMessage("Tcjh_BuyAdvPass")
        local okfunc = function()
            CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(setting.Vip1PID), 0)
            CUIManager.CloseUI(CLuaUIResources.TcjhBuyAdvPassWnd)
        end
        g_MessageMgr:ShowOkCancelMessage(msg,okfunc,nil,nil,nil,false)
    else
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(setting.Vip2PID), 0)
        CUIManager.CloseUI(CLuaUIResources.TcjhBuyAdvPassWnd)
    end
end

--@endregion
