require("common/common_include")

local CRankData=import "L10.UI.CRankData"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CGuildMgr = import "L10.Game.CGuildMgr"

local UILabel=import "UILabel"
local QnTableView=import "L10.UI.QnTableView"

LuaMengHualuRankWnd=class()
-- 主玩家相关信息
RegistChildComponent(LuaMengHualuRankWnd, "MyRankLabel", UILabel)
RegistChildComponent(LuaMengHualuRankWnd, "MyNameLabel", UILabel)
RegistChildComponent(LuaMengHualuRankWnd, "MyGuildLabel", UILabel)
RegistChildComponent(LuaMengHualuRankWnd, "MyBabyLabel", UILabel)
RegistChildComponent(LuaMengHualuRankWnd, "MyQiChangLabel", UILabel)
RegistChildComponent(LuaMengHualuRankWnd, "MyScoreLabel", UILabel)

RegistChildComponent(LuaMengHualuRankWnd, "TableView", QnTableView)

RegistClassMember(LuaMengHualuRankWnd,"TableViewDataSource")
RegistClassMember(LuaMengHualuRankWnd,"RankList")


function LuaMengHualuRankWnd:Init()
    
    if CClientMainPlayer.Inst then
        self.MyNameLabel.text = CClientMainPlayer.Inst.Name
    end
    self.MyRankLabel.text = LocalString.GetString("未上榜")
    self.MyNameLabel.text = LocalString.GetString("—")

    if CClientMainPlayer.Inst:IsInGuild() then
        self.MyGuildLabel.text = CGuildMgr.Inst.m_GuildName
    else
        self.MyGuildLabel.text = LocalString.GetString("—")
    end
    self.MyBabyLabel.text = LocalString.GetString("—")
    self.MyQiChangLabel.text = LocalString.GetString("—")
    self.MyScoreLabel.text = LocalString.GetString("—")

   
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem(item,index)
    end
    
    self.TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.TableView.m_DataSource=self.TableViewDataSource
    self.TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    Gac2Gas.QueryRank(41000178)
end

function LuaMengHualuRankWnd:OnSelectAtRow(row)
    local data = self.RankList[row]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end

function LuaMengHualuRankWnd:InitItem(item,index)
    local tf = item.transform
    local RankLabel = tf:Find("Table/RankLabel"):GetComponent(typeof(UILabel))
    RankLabel.text = ""
    local RankImage = tf:Find("Table/RankLabel/RankImage"):GetComponent(typeof(UISprite))
    RankImage.spriteName = nil
    local NameLabel = tf:Find("Table/NameLabel"):GetComponent(typeof(UILabel))
    NameLabel.text = ""
    local GuildLabel = tf:Find("Table/GuildLabel"):GetComponent(typeof(UILabel))
    GuildLabel.text = ""
    local BabyLabel = tf:Find("Table/BabyLabel"):GetComponent(typeof(UILabel))
    BabyLabel.text = ""
    local QiChangLabel = tf:Find("Table/QiChangLabel"):GetComponent(typeof(UILabel))
    QiChangLabel.text = ""
    local ScoreLabel = tf:Find("Table/ScoreLabel"):GetComponent(typeof(UILabel))
    ScoreLabel.text = ""

    local info = self.RankList[index]
    if not info then return end

    local rank = info.Rank
    if rank == 1 then
        RankImage.spriteName = "Rank_No.1"
    elseif rank ==2 then
        RankImage.spriteName = "Rank_No.2png"
    elseif rank ==3 then
        RankImage.spriteName = "Rank_No.3png"
    else
        RankLabel.text=tostring(rank)
    end

    NameLabel.text = info.Name
    if System.String.IsNullOrEmpty(info.Guild_Name) then
        GuildLabel.text = LocalString.GetString("—")
    else
        GuildLabel.text = info.Guild_Name
    end
    BabyLabel.text = info.LingshouName
    QiChangLabel.text = info.LingshouId
    ScoreLabel.text = tostring(info.Value)

end

function LuaMengHualuRankWnd:OnRankDataReady()
    if CLuaRankData.m_CurRankId ~= 41000178 then return end
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    if myInfo.Rank > 0 then
        self.MyRankLabel.text = myInfo.Rank
    else
        self.MyRankLabel.text = LocalString.GetString("未上榜")
    end
    self.MyNameLabel.text= myInfo.Name
    if CClientMainPlayer.Inst then
        self.MyNameLabel.text= CClientMainPlayer.Inst.Name
    end

    if myInfo.Value > 0 then
        self.MyScoreLabel.text=tostring(myInfo.Value)
    else
        self.MyScoreLabel.text = LocalString.GetString("—")
    end

    self.MyBabyLabel.text = myInfo.LingshouName
    self.MyQiChangLabel.text = myInfo.LingshouId
    self.MyScoreLabel.text = tostring(myInfo.Value)

    if CClientMainPlayer.Inst:IsInGuild() then
        self.MyGuildLabel.text=CGuildMgr.Inst.m_GuildName
    else
        self.MyGuildLabel.text=LocalString.GetString("—")
    end
    self.RankList = CRankData.Inst.RankList

    self.TableViewDataSource.count = self.RankList.Count
    self.TableView:ReloadData(true,false)

end

function LuaMengHualuRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaMengHualuRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

return LuaMengHualuRankWnd
