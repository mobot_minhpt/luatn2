
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaAntiProfessionConvertWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_Icon", "Icon", CUITexture)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_Selectable", "Selectable", GameObject)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_Title", "Title", UILabel)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_GenNumLab", "GenNumLab", UILabel)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_DecreaseButton", "DecreaseButton", QnButton)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_NumInput", "NumInput", GameObject)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_IncreaseButton", "IncreaseButton", QnButton)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_InputLab", "InputLab", UILabel)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_MaxInputLab", "MaxInputLab", UILabel)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_WeekMaxNumLab", "Num", UILabel)
RegistChildComponent(LuaAntiProfessionConvertWnd, "m_CostNumLab", "NumLab", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaAntiProfessionConvertWnd, "m_InputNum")
RegistClassMember(LuaAntiProfessionConvertWnd, "m_InputNumMax")
RegistClassMember(LuaAntiProfessionConvertWnd, "m_IncreaseTick")
RegistClassMember(LuaAntiProfessionConvertWnd, "m_DecreaseTick")
RegistClassMember(LuaAntiProfessionConvertWnd, "m_Class")

function LuaAntiProfessionConvertWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDecreaseButtonClick()
	end)


	
	UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)


	
	UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInputLabClick()
	end)


	
	UIEventListener.Get(self.m_TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.m_OpBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpBtnClick()
	end)


    --@endregion EventBind end
end

function LuaAntiProfessionConvertWnd:Init()
	self.m_InputNum = 0
	self.m_InputNumMax = 0
	self.m_CostNumLab.text = ""
	self.m_GenNumLab.text = ""
	self.m_Selectable:SetActive(true)
	self.m_NumInput:SetActive(false)

	UIEventListener.Get(self.m_Icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIconClick()
	end)

	UIEventListener.Get(self.m_IncreaseButton.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, state)
        self:OnIncreaseButtonPress(go,state)
    end)

	UIEventListener.Get(self.m_DecreaseButton.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, state)
        self:OnDecreaseButtonPress(go,state)
    end)

	self:UnpdateSilver()

	self.m_QnCostAndOwnMoney:SetCost(0)
	self:RefreshMax()
end

--@region UIEvent
function LuaAntiProfessionConvertWnd:OnEnable()
	g_ScriptEvent:AddListener("AntiProfessionConvertChoose", self, "SetClass")
	g_ScriptEvent:AddListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
	g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "UnpdateSilver")
	g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaAntiProfessionConvertWnd:OnDisable()
	g_ScriptEvent:RemoveListener("AntiProfessionConvertChoose", self, "SetClass")
	g_ScriptEvent:RemoveListener("AntiProfessionUpdateScore", self, "OnAntiProfessionUpdateScore")
	g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "UnpdateSilver")
	g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaAntiProfessionConvertWnd:OnIconClick()
	CUIManager.ShowUI(CLuaUIResources.AntiProfessionConvertChooseWnd)
end

function LuaAntiProfessionConvertWnd:OnDecreaseButtonClick()
	self:SetInputNum(self.m_InputNum - 2)
end

function LuaAntiProfessionConvertWnd:OnIncreaseButtonClick()
	self:SetInputNum(self.m_InputNum + 2)
end

function LuaAntiProfessionConvertWnd:OnInputLabClick()
	CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val) 
		self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val) 
		if val % 2 ~= 0 then
			g_MessageMgr:ShowMessage("ANTIPROFESSION_CONVERTWND_NOT_EVEN")
		end
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaAntiProfessionConvertWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("ANTIPROFESSION_CONVERTWND_TIP")
end

function LuaAntiProfessionConvertWnd:OnOpBtnClick()
	if self.m_Class == nil then
		g_MessageMgr:ShowMessage("ANTIPROFESSION_CONVERTWND_NOSELECT")
		return
	end
	if self.m_InputNum == nil or self.m_InputNum == 0 then
		g_MessageMgr:ShowMessage("ANTIPROFESSION_CHANGE_FAILED_ZERO")
		return
	end
	local freeSilver = CClientMainPlayer.Inst.FreeSilver
    local silver = CClientMainPlayer.Inst.Silver
    local cost = self.m_QnCostAndOwnMoney.cost
    local freeEnough = freeSilver >= cost
    local totalEnough = silver + freeSilver >= cost
    if totalEnough and not freeEnough and freeSilver ~= 0 then
        local silverCost = cost - freeSilver
        MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("升级所需银票不足，是否消耗%d银两进行转换？"), silverCost), DelegateFactory.Action(function ()
            Gac2Gas.AntiProfession_RandomChangeScore(self.m_Class, self.m_InputNum)
        end), nil, nil, nil, false)
    else 
        Gac2Gas.AntiProfession_RandomChangeScore(self.m_Class, self.m_InputNum)
    end
end

function LuaAntiProfessionConvertWnd:OnDecreaseButtonPress(go,isPressed)
	if isPressed then
		if self.m_DecreaseTick == nil then
			self.m_DecreaseTick = RegisterTick(function()
				self:SetInputNum(self.m_InputNum - 2)
			end, 100)
		end
	else
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end
end

function LuaAntiProfessionConvertWnd:OnIncreaseButtonPress(go,isPressed)
	if isPressed then
		if self.m_IncreaseTick == nil then
			self.m_IncreaseTick = RegisterTick(function()
				self:SetInputNum(self.m_InputNum + 2)
			end, 100)
		end
	else
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end
end

--@endregion UIEvent

function LuaAntiProfessionConvertWnd:UnpdateSilver()
	if CClientMainPlayer.Inst == nil then
        return
    end

    local freeSilver = CClientMainPlayer.Inst.FreeSilver
	
    --优先消耗银票
    if freeSilver > 0 then
        self.m_QnCostAndOwnMoney:SetType(EnumMoneyType_lua.YinPiao, 0, false)
    else
        self.m_QnCostAndOwnMoney:SetType(EnumMoneyType_lua.YinLiang, 0, false)
    end
end

function LuaAntiProfessionConvertWnd:SetInputNum(num)
	if num < 0 then
		num = 0
	end

	if num > self.m_InputNumMax then
		num = self.m_InputNumMax
	end

	num = num - (num % 2)

	self.m_InputNum = num
	self.m_CostNumLab.text = num
	self.m_InputLab.text = num
	self.m_GenNumLab.text = self:CalGenScore(num)

	self.m_IncreaseButton.Enabled = self.m_InputNumMax - num >= 1
	self.m_DecreaseButton.Enabled = num >= 1
	if self.m_InputNumMax - num < 1 then
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end

	if num < 1 then
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end

	self.m_QnCostAndOwnMoney:SetCost(self:CalCostMoney(num))
end

function LuaAntiProfessionConvertWnd:SetClass(class)
	self.m_Class = class
	
    local itemId = SoulCore_Settings.GetData().AntiProfessionItemId[class - 1][1]
	local itemData = Item_Item.GetData(itemId)
    self.m_Title.text = itemData.Name
	self.m_Icon:LoadMaterial(LuaZongMenMgr:GetProfessionScoreIconPath(class))
	self.m_Selectable:SetActive(false)
	self.m_NumInput:SetActive(true)

	self:RefreshMax()
	self:SetInputNum(self.m_InputNum)
end

function LuaAntiProfessionConvertWnd:RefreshMax()
	local ownScore = 0
	if self.m_Class ~= nil then
		ownScore = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, self.m_Class) and CClientMainPlayer.Inst.SkillProp.AntiProfessionScore[self.m_Class] or 0
	end
	local maxThisWeek = self:CalWeekMax()
	local changeThisWeek = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eAntiProfessionChangeScoreNum)
	self.m_InputNumMax = math.min(ownScore, maxThisWeek - changeThisWeek)
	self.m_MaxInputLab.text = self.m_InputNumMax
	self.m_WeekMaxNumLab.text = SafeStringFormat3("%d/%d", changeThisWeek, maxThisWeek)
end

function LuaAntiProfessionConvertWnd:OnAntiProfessionUpdateScore(profession, score)
	if self.m_Class == profession then
		self:RefreshMax()
		self:SetInputNum(self.m_InputNum)
	end
end

function LuaAntiProfessionConvertWnd:OnUpdateTempPlayTimesWithKey(args)
	local key = args[0]
    if key == EnumPlayTimesKey_lua.eAntiProfessionChangeScoreNum then
        self:RefreshMax()
    end
end

function LuaAntiProfessionConvertWnd:CalCostMoney(score)
	local costMoney = 0
    local formulaId = SoulCore_Settings.GetData().AntiProfessionChangeScore_MoneyFormulaId
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        costMoney = formula(nil, nil, {score})
    end
    return costMoney
end

function LuaAntiProfessionConvertWnd:CalGenScore(score)
	local genScore = 0
    local formulaId = SoulCore_Settings.GetData().AntiProfessionChangeScore_DeltaFormulaId
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        genScore = formula(nil, nil, {score})
    end
    return genScore
end

function LuaAntiProfessionConvertWnd:CalWeekMax()
	local antiLevel = LuaZongMenMgr:CalMainPlayerAntiProfessionLevel(false)
	local soulCoreLevel = CClientMainPlayer.Inst.SkillProp.SoulCore.Level

	local weekMax = 0
    local formulaId = SoulCore_Settings.GetData().AntiProfessionChangeScore_LimitFormulaId
    local formula = AllFormulas.Action_Formula[formulaId].Formula
    if formula ~= nil then
        weekMax = formula(nil, nil, {soulCoreLevel, antiLevel})
    end
    return weekMax
end

function LuaAntiProfessionConvertWnd:OnDestroy()
	if self.m_DecreaseTick ~= nil then
		UnRegisterTick(self.m_DecreaseTick)
		self.m_DecreaseTick = nil
	end
	if self.m_IncreaseTick ~= nil then
		UnRegisterTick(self.m_IncreaseTick)
		self.m_IncreaseTick = nil
	end
end
