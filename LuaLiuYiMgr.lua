require("common/common_include")
local EPropStatus = import "L10.Game.EPropStatus"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CActionStateMgr = import "L10.Game.CActionStateMgr"
local CScene=import "L10.Game.CScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientMonster = import "L10.Game.CClientMonster"
local CSkillAcquisitionMgr = import "L10.UI.CSkillAcquisitionMgr"
local LiuYi_ZhuoJiSetting = import "L10.Game.LiuYi_ZhuoJiSetting"
local Monster_Monster = import "L10.Game.Monster_Monster"
local Time = import "UnityEngine.Time"
local Extensions = import "Extensions"
local CForcesMgr = import "L10.Game.CForcesMgr"
local Color = import "UnityEngine.Color"

CTickMgr = import "L10.Engine.CTickMgr"
ETickType = import "L10.Engine.ETickType"

CLuaLiuYiMgr = {}
CLuaLiuYiMgr.ItemId = nil
CLuaLiuYiMgr.ItemPos = nil
CLuaLiuYiMgr.ItemPlace = nil
CLuaLiuYiMgr.CanPoke = true
CLuaLiuYiMgr.Chickentick = nil

CLuaLiuYiMgr.RewardRemainTimes = 0
CLuaLiuYiMgr.IsSignUp = false
CLuaLiuYiMgr.ZhuojiResult = {}

CLuaLiuYiMgr.AttachedChicken = {}


function CLuaLiuYiMgr:Init()
	g_ScriptEvent:AddListener("PlayerShakeDevice", self, "PlayerShakeDevice")
	g_ScriptEvent:AddListener("ClientObjCreate", self, "ClientObjCreate")

	CLuaLiuYiMgr.ChickenState = {}
end

function CLuaLiuYiMgr:RegisterChickenTick()
	if CLuaLiuYiMgr.Chickentick then return end

	CLuaLiuYiMgr.Chickentick = CTickMgr.Register(DelegateFactory.Action(function ()
		CLuaLiuYiMgr:OnChickenTick()
	end), 33, ETickType.Loop)
end

function CLuaLiuYiMgr:InitDesignData()
	if not CLuaLiuYiMgr.InitedDesignData then
		local setting = LiuYi_ZhuoJiSetting.GetData()
		CLuaLiuYiMgr.ChickenShieldBuffId = setting.ChickenShieldBuffId

		local monsterId = setting.ChickenId
		local littleMonsterId = setting.LittleChickenId

		local monsterData = Monster_Monster.GetData(monsterId)
		local littleMonsterData = Monster_Monster.GetData(littleMonsterId)
		CLuaLiuYiMgr.ChickenId = monsterId
		CLuaLiuYiMgr.LittleChickenId = littleMonsterId
		CLuaLiuYiMgr.ChickenScale = monsterData.Scale
		CLuaLiuYiMgr.LittleChickenScale = littleMonsterData.Scale

		CLuaLiuYiMgr.InitedDesignData = true
	end
end

function CLuaLiuYiMgr:PlayerShakeDevice(args)
	if not CLuaLiuYiMgr.CanPoke then
		return
	end
	CLuaLiuYiMgr.CanPoke = false
	CTickMgr.Register(DelegateFactory.Action(function ()
		CLuaLiuYiMgr.CanPoke = true
	end), 1000, ETickType.Once)
	if CClientMainPlayer.Inst ~= nil and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("SuperBubble")) == 1 then
		Gac2Gas.RequestPokeSuperBubble()
	end
end

function CLuaLiuYiMgr:OnChickenTick()
	local gamePlayId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
    end
    if not(gamePlayId == 51100803 or gamePlayId == 51100804) then -- 儿童节捉鸡比赛
    	return
    end

    self:InitDesignData()

    local range = 0.2
    local luaFunc = function(obj)
    		if TypeAs(obj, typeof(CClientMonster)) then
    			if CommonDefs.DictContains(obj.BuffProp.Buffs, typeof(uint), CLuaLiuYiMgr.ChickenShieldBuffId) then
    				if not CLuaLiuYiMgr.ChickenState[obj.EngineId] then
    					CLuaLiuYiMgr.ChickenState[obj.EngineId] = {StartTime = Time.realtimeSinceStartup}
    					if obj.RO then
	    					local trans = nil
	    					trans = FindChild(obj.RO.transform, "lnpc250_01.skin(Clone)")
	    					if not trans then
	    						trans = FindChild(obj.RO.transform, "lnpc250_01.skin(Clone)")
	    					end 
	    					CLuaLiuYiMgr.ChickenState[obj.EngineId].Transform = trans
	    				end
    				end
    				if CLuaLiuYiMgr.ChickenState[obj.EngineId].Transform then
    					local y = (math.sin(Time.realtimeSinceStartup - CLuaLiuYiMgr.ChickenState[obj.EngineId].StartTime + math.pi * 3 / 2) + 2) * range
    					Extensions.SetLocalPositionY(CLuaLiuYiMgr.ChickenState[obj.EngineId].Transform, y)
    				end
    			else
    				CLuaLiuYiMgr.ChickenState[obj.EngineId] = nil
    			end
    		end
    	end
  	local action = DelegateFactory.Action_CClientObject(luaFunc)
  	CClientObjectMgr.Inst:ForeachObject(action)
end

function CLuaLiuYiMgr:ClientObjCreate(args)
	if not next(CLuaLiuYiMgr.AttachedChicken) then
		return
	end
    self:InitDesignData()

	local gamePlayId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
    end
    if not(gamePlayId == 51100803 or gamePlayId == 51100804) then -- 儿童节捉鸡比赛
    	return
    end

	local engineId = args[0]
	if not engineId then return end
	local playerId = nil
	local chickenEngineId = nil
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if not obj then return end
	if TypeAs(obj, typeof(CClientMainPlayer)) then
		playerId = obj.Id
	elseif TypeAs(obj, typeof(CClientOtherPlayer)) then
		playerId = obj.PlayerId
	elseif TypeAs(obj, typeof(CClientMonster)) then
		chickenEngineId = engineId
	end

	for k, v in pairs(CLuaLiuYiMgr.AttachedChicken) do
		if playerId and not chickenEngineId then
			if v == playerId then
				chickenEngineId = k
				break
			end
		elseif chickenEngineId and not playerId then
			if k == chickenEngineId then
				playerId = v
				break
			end
		end
	end
	if playerId and chickenEngineId then
		CLuaLiuYiMgr:AttachChickerRO(playerId, chickenEngineId)
	end
end

function CLuaLiuYiMgr:AttachChickerRO(playerId, engineId)
    self:InitDesignData()

	local character = CClientPlayerMgr.Inst:GetPlayer(playerId)
	if not (character and character.RO) then
		return
	end
	local chicken = CClientObjectMgr.Inst:GetObject(engineId)
	if not (chicken and chicken.RO) then
		return
	end

	local scale = 1.0
	if chicken.TemplateId == CLuaLiuYiMgr.ChickenId then
		scale = CLuaLiuYiMgr.ChickenScale
	elseif chicken.TemplateId == CLuaLiuYiMgr.LittleChickenId then
		scale = CLuaLiuYiMgr.LittleChickenScale
	end
	character.RO:AttachRO(chicken.RO, "Head", -0.2, -0.1, 0, 270, 90, 0, scale)

	chicken:AddChickenSpecialSoundAndFX()

	CActionStateMgr.Inst:RefreshMoveState(character)
end

function CLuaLiuYiMgr:DettachChickerRO( playerId )
    self:InitDesignData()

	local character = CClientPlayerMgr.Inst:GetPlayer(playerId)
	if not (character and character.RO) then
		return
	end

	local chickenEngineId = nil
	for k, v in pairs(CLuaLiuYiMgr.AttachedChicken) do
		if v == playerId then
			chickenEngineId = k
			break
		end
	end

	if not chickenEngineId then return end

	local chicken = CClientObjectMgr.Inst:GetObject(chickenEngineId)
	if not (chicken and chicken.RO) then
		return
	end

	local scale = 1.0
	if chicken.TemplateId == CLuaLiuYiMgr.ChickenId then
		scale = CLuaLiuYiMgr.ChickenScale
	elseif chicken.TemplateId == CLuaLiuYiMgr.LittleChickenId then
		scale = CLuaLiuYiMgr.LittleChickenScale
	end

	character.RO:DetachRO(chicken.RO, 0, 0, 0, 0, 0, 0, scale)
	CActionStateMgr.Inst:RefreshMoveState(character)
end

CLuaLiuYiMgr:Init()


function Gas2Gac.SyncNpcBubbleInfo(engineId, blowTime, scale)
	local CLiuYiMgr = import "L10.Game.CLiuYiMgr"
	CLiuYiMgr.Inst:SetBubbleData(engineId, blowTime, scale)
end

function Gas2Gac.SendZhuoJiSignUpInfo(isSignUp, forceOpen, rewardRemainTimes)
	CLuaLiuYiMgr.IsSignUp = isSignUp
	CLuaLiuYiMgr.RewardRemainTimes = rewardRemainTimes
	if forceOpen then
		CUIManager.ShowUI("ZhuojiSingUpWnd")
	end

	g_ScriptEvent:BroadcastInLua("LiuyiZhuoji_SendZhuoJiSignUpInfo")

	CLuaLiuYiMgr:RegisterChickenTick()
end

function Gas2Gac.QueryZhuoJiPlayInfoResult(resultTbl_U)
	local list = MsgPackImpl.unpack(resultTbl_U)
	if not list then return end

	CLuaLiuYiMgr.ZhuojiResult = {}
	for i = 0, list.Count - 1, 5 do
		local res = {}
		res.id = list[i]
		res.name = list[i + 1]
		res.rank = list[i + 2]
		local force = list[i + 3]
		res.force = force
		res.score = list[i + 4]

		CLuaLiuYiMgr.ZhuojiResult[force] = res
	end

	CUIManager.ShowUI("ZhuojiInfoWnd")
end

function Gas2Gac.UpdateZhuoJiForceScore(force, score)
	if not CLuaLiuYiMgr.ZhuojiResult[force] then
		CLuaLiuYiMgr.ZhuojiResult[force] = {}
	end
	local origScore = CLuaLiuYiMgr.ZhuojiResult[force].score or 0
	CLuaLiuYiMgr.ZhuojiResult[force].score = score

	if CClientMainPlayer.Inst and origScore ~= score then
		local myforce = CForcesMgr.Inst:GetPlayerForce(CClientMainPlayer.Inst.Id)
		if myforce > 0 and force == myforce then
			local RO = CClientMainPlayer.Inst.RO
			if RO then
				local c = Color.red
				if score - origScore > 0 then
					c = Color.green
				end
				RO:ShowScoreText(score - origScore, c)
			end
		end
	end
	if origScore ~= score then
		g_ScriptEvent:BroadcastInLua("LiuyiZhuoji_UpdateZhuoJiForceScore", force, origScore, score)
	end
end

function Gas2Gac.ShowZhuoJiPlayResult(force, score, rank)
	if not CLuaLiuYiMgr.ZhuojiResult[force] then
		CLuaLiuYiMgr.ZhuojiResult[force] = {}
	end
	CLuaLiuYiMgr.FinalResult = {}
	CLuaLiuYiMgr.FinalResult.score = score
	CLuaLiuYiMgr.FinalResult.rank = rank
	CLuaLiuYiMgr.FinalResult.force = force
	
	CUIManager.ShowUI("ZhuojiResultInfoWnd")
end

function Gas2Gac.PlayerCatchChicken(playerId, chickenEngineId)
	CLuaLiuYiMgr:RegisterChickenTick()
	CLuaLiuYiMgr.AttachedChicken[chickenEngineId] = playerId
	CLuaLiuYiMgr:AttachChickerRO(playerId, chickenEngineId)
end

function Gas2Gac.PlayerReleaseChicken(playerId)
	CLuaLiuYiMgr:DettachChickerRO(playerId)
	for k, v in pairs(CLuaLiuYiMgr.AttachedChicken) do
		if v == playerId then
			CLuaLiuYiMgr.AttachedChicken[k] = nil
		end
	end

end

function Gas2Gac.ShowZhuoJiSkillsTip(skillIdList_U)
	local list = MsgPackImpl.unpack(skillIdList_U)
	if not list then return end

	local tempSkillIds = CreateFromClass(MakeGenericClass(List, UInt32))
	for i = 0, list.Count - 1 do
		CommonDefs.ListAdd(tempSkillIds, typeof(UInt32), list[i])
	end
	CSkillAcquisitionMgr.ShowTempSkillsTip(tempSkillIds)
end
