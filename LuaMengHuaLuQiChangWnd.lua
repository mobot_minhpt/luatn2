require("common/common_include")

local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local MengHuaLu_QiChang = import "L10.Game.MengHuaLu_QiChang"
local QnCheckBox = import "L10.UI.QnCheckBox"
local Baby_QiChang = import "L10.Game.Baby_QiChang"
local MessageMgr = import "L10.Game.MessageMgr"
local MengHuaLu_Setting = import "L10.Game.MengHuaLu_Setting"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"

LuaMengHuaLuQiChangWnd = class()

RegistChildComponent(LuaMengHuaLuQiChangWnd, "QiChangTable", UITable)
RegistChildComponent(LuaMengHuaLuQiChangWnd, "QiChangTemplate", GameObject)
RegistChildComponent(LuaMengHuaLuQiChangWnd, "ContentScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaMengHuaLuQiChangWnd, "SettingBtn", CButton)
RegistChildComponent(LuaMengHuaLuQiChangWnd, "HintLabel", GameObject)
-- 花费
RegistChildComponent(LuaMengHuaLuQiChangWnd, "CostLabel", UILabel)
RegistChildComponent(LuaMengHuaLuQiChangWnd, "OwnLabel", UILabel)
RegistChildComponent(LuaMengHuaLuQiChangWnd, "AddMoneyBtn", GameObject)

RegistClassMember(LuaMengHuaLuQiChangWnd, "AllQiChangs")
RegistClassMember(LuaMengHuaLuQiChangWnd, "ActivedQiChangs")
RegistClassMember(LuaMengHuaLuQiChangWnd, "SelectedQiChangs") --list
RegistClassMember(LuaMengHuaLuQiChangWnd, "QiChangInfos")
RegistClassMember(LuaMengHuaLuQiChangWnd, "BabyInfo")

function LuaMengHuaLuQiChangWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaMengHuaLuQiChangWnd:InitClassMembers()
	self.BabyInfo = LuaMengHuaLuMgr.m_BabyInfo
	if not self.BabyInfo then 
		CUIManager.CloseUI(CLuaUIResources.MengHuaLuQiChangWnd)
		return
	end
	self.QiChangInfos = {}
	self.SelectedQiChangs = nil
	self.CostLabel.text = "0"
	self.OwnLabel.text = self.BabyInfo.Gold
	Gac2Gas.QueryMengHuaLuQiChang()
end

function LuaMengHuaLuQiChangWnd:InitValues()
	self.QiChangTemplate:SetActive(false)
	
	local onSettingBtnClicked = function (go)
		self:OnSettingBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.SettingBtn.gameObject, DelegateFactory.Action_GameObject(onSettingBtnClicked), false)

	local onAddMoneyBtnClicked = function (go)
		self:OnAddMoneyBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.AddMoneyBtn.gameObject, DelegateFactory.Action_GameObject(onAddMoneyBtnClicked), false)
end

function LuaMengHuaLuQiChangWnd:UpdateQiChangs(allQiChang, activeQiChang)
	self.AllQiChangs = allQiChang
	self.ActivedQiChangs = activeQiChang

	self.SelectedQiChangs = CreateFromClass(MakeGenericClass(List, UInt32))
	for i = 0, self.ActivedQiChangs.Count-1 do
		CommonDefs.ListAdd(self.SelectedQiChangs, typeof(UInt32), self.ActivedQiChangs[i])
	end

	self.QiChangInfos = {}
	Extensions.RemoveAllChildren(self.QiChangTable.transform)

	for i = 0, self.AllQiChangs.Count-1 do
		local data = MengHuaLu_QiChang.GetData(self.AllQiChangs[i])
		table.insert(self.QiChangInfos, data)
	end

	local compare = function (a, b)
		if self:IsQiChangActived(a.ID) and not self:IsQiChangActived(b.ID) then
			return true
		elseif not self:IsQiChangActived(a.ID) and self:IsQiChangActived(b.ID) then
			return false
		else
			local aQiChang = Baby_QiChang.GetData(a.QiChangID)
			local bQiChang = Baby_QiChang.GetData(b.QiChangID)
			return bQiChang.Quality < aQiChang.Quality
		end
	end
	table.sort(self.QiChangInfos, compare)

	self.SettingBtn.Enabled = #self.QiChangInfos ~= 0

	for i = 1, #self.QiChangInfos do
		local go = CUICommonDef.AddChild(self.QiChangTable.gameObject, self.QiChangTemplate)
		self:InitQiChangItem(go, self.QiChangInfos[i])
		go:SetActive(true)
	end

	self.QiChangTable:Reposition()
	self.ContentScrollView.panel:ResetAndUpdateAnchors()
	self.ContentScrollView:ResetPosition()

	self.HintLabel:SetActive(#self.QiChangInfos == 0)

end

function LuaMengHuaLuQiChangWnd:InitQiChangItem(go, qichang)
	local QiChangNameLabel = go.transform:Find("QiChangNameLabel"):GetComponent(typeof(UILabel))
	local QiChangDescLabel = go.transform:Find("QiChangDescLabel"):GetComponent(typeof(UILabel))
	local Current = go.transform:Find("Current").gameObject
	local QnCheckbox = go.transform:Find("QnCheckbox"):GetComponent(typeof(QnCheckBox))

	local qichangName = qichang.MaleName
	if self.BabyInfo.AppearanceData[1] == 1 then
		qichangName = qichang.FemaleName
	end

	local babyQiChang = Baby_QiChang.GetData(qichang.QiChangID)
	QiChangNameLabel.text = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(babyQiChang.Quality-1), qichangName)
	QiChangDescLabel.text = qichang.Description
	Current:SetActive(self:IsQiChangActived(qichang.ID))
	QnCheckbox:SetSelected(self:IsQiChangActived(qichang.ID), false)

	local onCheckBoxClicked = function (value)
		self:OnCheckBoxClicked(qichang.ID, value, QnCheckbox)
	end
	QnCheckbox.OnValueChanged = DelegateFactory.Action_bool(onCheckBoxClicked)
end

function LuaMengHuaLuQiChangWnd:IsQiChangActived(qichangId)
	for i = 0, self.ActivedQiChangs.Count-1 do
		if self.ActivedQiChangs[i] == qichangId then
			return true
		end
	end
	return false
end

function LuaMengHuaLuQiChangWnd:OnCheckBoxClicked(qichangId, value, checkbox)
	local setting = MengHuaLu_Setting.GetData()

	if value then
		if self.SelectedQiChangs.Count >= setting.MaxActiveQiChangNum then
			MessageMgr.Inst:ShowMessage("MENGHUALU_MAX_QICHANG", {})
			checkbox:SetSelected(false, false)
			return
		else
			self.SelectedQiChangs:Add(qichangId)
		end
	else
		self.SelectedQiChangs:Remove(qichangId)
	end

	self.CostLabel.text = tostring(setting.ModifyQiChangGold)
end

function LuaMengHuaLuQiChangWnd:OnSettingBtnClicked(go)
	-- 检查金额是否足够
	local setting = MengHuaLu_Setting.GetData()
	if self.BabyInfo.Gold < setting.ModifyQiChangGold then
		MessageMgr.Inst:ShowMessage("MENGHUALU_SET_QICHANG_MONEY_NOTENOUGH", {setting.ModifyQiChangGold})
		return
	end

	-- 二次确认
	local msg = g_MessageMgr:FormatMessage("MENGHUALU_SET_QICHANG_CONFIRM")
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
		local selectedList = CreateFromClass(MakeGenericClass(List, Object))
		for i = 0, self.SelectedQiChangs.Count-1 do
			CommonDefs.ListAdd(selectedList, typeof(UInt32), self.SelectedQiChangs[i])
		end
		Gac2Gas.RequestMengHuaLuSetQiChang(MsgPackImpl.pack(selectedList))
    end), nil, nil, nil, false)
	
end

function LuaMengHuaLuQiChangWnd:MengHuaLuSetQiChangSuccess(list)
	self:UpdateQiChangs(self.AllQiChangs, list)
end

function LuaMengHuaLuQiChangWnd:OnAddMoneyBtnClicked(go)
	MessageMgr.Inst:ShowMessage("MENGHUALU_ADD_MONEY", {})
end

function LuaMengHuaLuQiChangWnd:UpdateBabyGold(grid, gold)
	if grid == 0 then
		self.BabyInfo.Gold = gold
		self.OwnLabel.text = self.BabyInfo.Gold
	end
end

function LuaMengHuaLuQiChangWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryMengHuaLuQiChangResult", self, "UpdateQiChangs")
	g_ScriptEvent:AddListener("RequestMengHuaLuSetQiChangSuccess", self, "MengHuaLuSetQiChangSuccess")
	g_ScriptEvent:AddListener("SyncMengHuaLuCharacterGold", self, "UpdateBabyGold")
end

function LuaMengHuaLuQiChangWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryMengHuaLuQiChangResult", self, "UpdateQiChangs")
	g_ScriptEvent:RemoveListener("RequestMengHuaLuSetQiChangSuccess", self, "MengHuaLuSetQiChangSuccess")
	g_ScriptEvent:RemoveListener("SyncMengHuaLuCharacterGold", self, "UpdateBabyGold")
end

return LuaMengHuaLuQiChangWnd
