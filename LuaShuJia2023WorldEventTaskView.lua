local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaShuJia2023WorldEventTaskView = class()

RegistClassMember(LuaShuJia2023WorldEventTaskView, "Type1")
RegistClassMember(LuaShuJia2023WorldEventTaskView, "Type2")
RegistClassMember(LuaShuJia2023WorldEventTaskView, "Bg1")
RegistClassMember(LuaShuJia2023WorldEventTaskView, "Bg2")
RegistClassMember(LuaShuJia2023WorldEventTaskView, "Countdown")
RegistClassMember(LuaShuJia2023WorldEventTaskView, "ProgressBar")
RegistClassMember(LuaShuJia2023WorldEventTaskView, "Percent")
RegistClassMember(LuaShuJia2023WorldEventTaskView, "Hint")

RegistClassMember(LuaShuJia2023WorldEventTaskView, "m_Tick")

function LuaShuJia2023WorldEventTaskView:Awake()
    self.Type1 = self.transform:Find("Type1")
    self.Type2 = self.transform:Find("Type2")
    self.Countdown = self.transform:Find("Type1/Countdown"):GetComponent(typeof(UILabel))
    self.ProgressBar = self.transform:Find("Type2/Progress"):GetComponent(typeof(UIProgressBar))
    self.Percent = self.transform:Find("Type2/Progress/Percent"):GetComponent(typeof(UILabel))
    self.Hint = self.transform:Find("Type2/Progress/Hint"):GetComponent(typeof(UILabel))
    self.Bg1 = self.Type1:Find("Bg"):GetComponent(typeof(UITexture))
    self.Bg2 = self.Type2:Find("Bg"):GetComponent(typeof(UITexture))

    UIEventListener.Get(self.Bg1.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaShuJia2023Mgr:OpenWorldEventWnd()
    end)
end

function LuaShuJia2023WorldEventTaskView:OnEnable()
    self:Init()
    g_ScriptEvent:AddListener("ShuJia2023WorldEventsSyncStageTwoStatus", self, "Init")
end

function LuaShuJia2023WorldEventTaskView:OnDisable()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = nil
    g_ScriptEvent:RemoveListener("ShuJia2023WorldEventsSyncStageTwoStatus", self, "Init")
end

function LuaShuJia2023WorldEventTaskView:Init()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = nil

    local isInYXW = LuaShuJia2023Mgr:IsInYXW()
    self.Type1.gameObject:SetActive(not isInYXW)
    self.Type2.gameObject:SetActive(isInYXW)
    local widget = self.transform:GetComponent(typeof(UIWidget))
    if isInYXW then
        self:InitType2()
        widget.width, widget.height = self.Bg2.width, self.Bg2.height
    else
        self:InitType1()
        widget.width, widget.height = self.Bg1.width, self.Bg1.height
    end
end

function LuaShuJia2023WorldEventTaskView:InitType1()
    local time1 = tonumber(ShuJia2023_Setting.GetData("GenerateBossTime").Value)
    local time2 = tonumber(ShuJia2023_Setting.GetData("BossLiveTime").Value)
    local tickFunc = function()
        local cd = math.floor(LuaShuJia2023Mgr.m_StageTwoStartTime + time1 + time2 - CServerTimeMgr.Inst.timeStamp)
        if LuaShuJia2023Mgr.m_StageTwoStatus == 0 or cd < 0 then
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
            LuaShuJia2023Mgr.m_StageTwoStatus = 0
            g_ScriptEvent:BroadcastInLua("UpdateTaskPlayViewVisibility")
            return
        end
        self.Countdown.text = SafeStringFormat3(LocalString.GetString("%02d:%02d"), cd / 60, cd % 60)
    end
    tickFunc()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTick(function()
        tickFunc()
    end, 500)
end

function LuaShuJia2023WorldEventTaskView:InitType2()
    local time1 = tonumber(ShuJia2023_Setting.GetData("GenerateBossTime").Value)
    local time2 = tonumber(ShuJia2023_Setting.GetData("BossLiveTime").Value)
    local hint1, hint2 = string.match(ShuJia2023_Setting.GetData("WorldEventTaskStageHint").Value, "(.+);(.+)")
    local label = self.Type2:Find("Label"):GetComponent(typeof(UILabel))
    local tickFunc = function()
        local cd = math.floor(LuaShuJia2023Mgr.m_StageTwoStartTime + time1 + time2 - CServerTimeMgr.Inst.timeStamp)
        if LuaShuJia2023Mgr.m_StageTwoStatus == 0 or cd < 0 then
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
            LuaShuJia2023Mgr.m_StageTwoStatus = 0
            g_ScriptEvent:BroadcastInLua("UpdateTaskPlayViewVisibility")
            return
        end
        label.text = LocalString.GetString("隐仙湾遇袭")..SafeStringFormat3("(%d/%d)", LuaShuJia2023Mgr.m_StageTwoStatus, 2)
        self.Hint.text = LuaShuJia2023Mgr.m_StageTwoStatus == 1 and hint1 or hint2
        local percent
        if LuaShuJia2023Mgr.m_StageTwoStatus == 1 then
            percent = (CServerTimeMgr.Inst.timeStamp - LuaShuJia2023Mgr.m_StageTwoStartTime) / time1
        elseif LuaShuJia2023Mgr.m_StageTwoStatus == 2 then
            percent = (CServerTimeMgr.Inst.timeStamp - LuaShuJia2023Mgr.m_StageTwoStartTime - time1) / time2
        else
            percent = 1
        end
        percent = math.max(0, percent)
        if percent > 1 then 
            percent = 0
            LuaShuJia2023Mgr.m_StageTwoStatus = LuaShuJia2023Mgr.m_StageTwoStatus + 1
        end
        self.ProgressBar.value = percent
        self.Percent.text = tostring(math.ceil(percent*100)).."%"
    end
    tickFunc()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTick(function()
        tickFunc()
    end, 500)
end