require("common/common_include")

local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CUIFx = import "L10.UI.CUIFx"
local BoxCollider = import "UnityEngine.BoxCollider"

-- 用于处理仙职技能的消失和显示
LuaXianZhiSkillInfo = class()

RegistChildComponent(LuaXianZhiSkillInfo, "XianZhiButton", UISprite)
RegistChildComponent(LuaXianZhiSkillInfo, "ShowFx", CUIFx)
RegistChildComponent(LuaXianZhiSkillInfo, "ButtonBorder", UISprite)

RegistClassMember(LuaXianZhiSkillInfo, "IsShow")
RegistClassMember(LuaXianZhiSkillInfo, "UpdateCooldownFunc")

function LuaXianZhiSkillInfo:Init()
	self.ShowFx:DestroyFx()
	self.gameObject:SetActive(true)
	self.IsShow = self.ButtonBorder.alpha ~= 0
	self.transform:GetComponent(typeof(BoxCollider)).enabled = self.IsShow
	self:UpdateCooldown()
end

function LuaXianZhiSkillInfo:OnEnable()
	if not self.UpdateCooldownFunc then
		self.UpdateCooldownFunc = DelegateFactory.Action(function ()
			self:UpdateCooldown()
		end)
	end
	EventManager.AddListener(EnumEventType.UpdateCooldown, self.UpdateCooldownFunc)
end

function LuaXianZhiSkillInfo:OnDisable()
	EventManager.RemoveListener(EnumEventType.UpdateCooldown, self.UpdateCooldownFunc)
end

function LuaXianZhiSkillInfo:UpdateCooldown()
	local skillId = CLuaXianzhiMgr.GetXianZhiSkillByDelta()
	local remain = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(skillId)
	if self.IsShow and remain > 0 then
		self:FadeAway()
	elseif not self.IsShow and remain <= 0 then
		self:ShowUp()
	end
end

-- 进入CD时间，渐隐
function LuaXianZhiSkillInfo:FadeAway()
	self.transform:GetComponent(typeof(BoxCollider)).enabled = false
	LuaTweenUtils.TweenAlpha(self.ButtonBorder.transform, 1, 0, 1, function()
		g_ScriptEvent:BroadcastInLua("InitAdditionalSkill", self.transform.parent.parent.parent.parent:GetComponent(typeof(CSkillButtonBoardWnd)))
	end)
	self.IsShow = false
end

function LuaXianZhiSkillInfo:ShowUp()
	self.transform:GetComponent(typeof(BoxCollider)).enabled = true
	-- 增加一个显示的特效
	if self.ButtonBorder.alpha ~= 1 then
		self.ShowFx:DestroyFx()
		self.ShowFx:LoadFx("Fx/UI/Prefab/UI_jinengjihuo.prefab")
		LuaTweenUtils.TweenAlpha(self.ButtonBorder.transform, 0, 1, 1, function()
			g_ScriptEvent:BroadcastInLua("InitAdditionalSkill", self.transform.parent.parent.parent.parent:GetComponent(typeof(CSkillButtonBoardWnd)))
		end)
		self.IsShow = true
	end
end

return LuaXianZhiSkillInfo