local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Task_Task = import "L10.Game.Task_Task"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local ClientAction = import "L10.UI.ClientAction"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"

LuaProfessionTransferMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaProfessionTransferMainWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaProfessionTransferMainWnd, "NewProfessionJuQing", "NewProfessionJuQing", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "PlayPV", "PlayPV", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "PlayVideoButton", "PlayVideoButton", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "NewProfessionIntroduction", "NewProfessionIntroduction", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "ReadIntroductionButton", "ReadIntroductionButton", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "Bottombtns", "Bottombtns", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "GoTransferButton", "GoTransferButton", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "BattleButton", "BattleButton", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "TransferReturnButton", "TransferReturnButton", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "TransferFundButton", "TransferFundButton", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "firstChapterBtn", "firstChapterBtn", GameObject)
RegistChildComponent(LuaProfessionTransferMainWnd, "secondChapterBtn", "secondChapterBtn", GameObject)

--@endregion RegistChildComponent end

function LuaProfessionTransferMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaProfessionTransferMainWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaProfessionTransferMainWnd:InitWndData()
    self.dramaCount = 2
    self.newProfession = 13

    self.myProfession = 1
    if CClientMainPlayer.Inst then
        self.myProfession = EnumToInt(CClientMainPlayer.Inst.Class)
    end
    
    local exceptZhanKuangTaskIds = ProfessionTransfer_Setting.GetData().ExceptZhanKuangTaskIds
    local taskList = g_LuaUtil:StrSplit(exceptZhanKuangTaskIds,";")
    self.exceptZhanKuangTaskIds = {}
    for i = 1, #taskList do
        local subStrList = g_LuaUtil:StrSplit(taskList[i], ",")
        if subStrList[1] == "1" or subStrList[1] == "2" then
            table.insert(self.exceptZhanKuangTaskIds, {tonumber(subStrList[2]), tonumber(subStrList[3])})
        end
    end
end

function LuaProfessionTransferMainWnd:RefreshConstUI()
    --refresh juqing
    self.TimeLabel.text = g_MessageMgr:FormatMessage("PROFESSION_TRANSFER_MAIN_WND_OPEN_TIME")
    self:RefreshJuQingButton()
end 

function LuaProfessionTransferMainWnd:IsPlayerNewProfession()
    return self.myProfession == self.newProfession
end

function LuaProfessionTransferMainWnd:GetChapterState(chapterId)
    --1: 等级不足而锁 2: task在autoPatch里呈关闭状态而锁 3: 已领取 4: 已完成 5:可以领取 6:前置未完成等其他情况
    local taskID = nil
    if self:IsPlayerNewProfession() then
        --自己是战狂职业, 从zhujuejuqingmgr中判断
        local juqingData = CZhuJueJuQingMgr.Instance:GetSelfJuqing()
        taskID = juqingData.TaskID[chapterId - 1][1]
    else
        taskID = self.exceptZhanKuangTaskIds[chapterId][1]
    end
    local taskData = Task_Task.GetData(taskID)
    if taskData.Status == 3 then
        return 2
    end
    
    if chapterId > 1 and (not self:IsChapterFinished(chapterId - 1)) then
        return 6
    end
    
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level < taskData.Level then
        return 1
    end
    
    if self:IsChapterFinished(chapterId) then
        return 4
    end
    
    if self:IsChapterAlreadyGet(chapterId) then
        return 3
    end
    
    if self:IsChapterUnLocked(chapterId) then
        return 5
    end

    return 6
end

function LuaProfessionTransferMainWnd:IsChapterAlreadyGet(chapterId)
    if not CClientMainPlayer.Inst then
        return false
    end

    if self:IsPlayerNewProfession() then
        return CZhuJueJuQingMgr.Instance:IsChapterAlreadyGet(chapterId)
    end
    
    local taskID = self.exceptZhanKuangTaskIds[chapterId][1]
    local condition1 = false
    local condition2 = false
    for subTaskId = self.exceptZhanKuangTaskIds[chapterId][1], self.exceptZhanKuangTaskIds[chapterId][2] do
        condition1 = condition1 or CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(subTaskId)
        condition2 = condition2 or CommonDefs.DictContains(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(uint), taskID)
    end
    return condition1 or condition2
end

function LuaProfessionTransferMainWnd:IsChapterFinished(chapterId)
    if not CClientMainPlayer.Inst then
        return false
    end

    if self:IsPlayerNewProfession() then
        return CZhuJueJuQingMgr.Instance:IsChapterFinished(chapterId)
    end
    
    local taskID = self.exceptZhanKuangTaskIds[chapterId][2]
    return CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskID)
end

function LuaProfessionTransferMainWnd:IsChapterUnLocked(chapterId)
    if not CClientMainPlayer.Inst then
        return false
    end

    if self:IsPlayerNewProfession() then
        return CZhuJueJuQingMgr.Instance:IsChapterUnLocked(chapterId)
    end
    
    if chapterId > 1 then
        if not self:IsChapterFinished(chapterId-1) then
            return false
        end
    end
    return true
end

function LuaProfessionTransferMainWnd:RefreshJuQingButton()
    self.juqingButtonList = {self.firstChapterBtn, self.secondChapterBtn}
    for i = 1, self.dramaCount do
        local playChapterButton = self.juqingButtonList[i]
        if playChapterButton then
            local state = self:GetChapterState(i)
            if state == 1 or state == 2 or state == 6 then
                playChapterButton.transform:Find("Lock").gameObject:SetActive(true)
                playChapterButton.transform:Find("Normal").gameObject:SetActive(false)
                playChapterButton.transform:Find("IsAccepted").gameObject:SetActive(false)
                playChapterButton.transform:Find("IsFinished").gameObject:SetActive(false)
            elseif state == 3 then
                playChapterButton.transform:Find("Lock").gameObject:SetActive(false)
                playChapterButton.transform:Find("Normal").gameObject:SetActive(true)
                playChapterButton.transform:Find("IsAccepted").gameObject:SetActive(true)
                playChapterButton.transform:Find("IsFinished").gameObject:SetActive(false)
            elseif state == 4 then
                playChapterButton.transform:Find("Lock").gameObject:SetActive(false)
                playChapterButton.transform:Find("Normal").gameObject:SetActive(true)
                playChapterButton.transform:Find("IsAccepted").gameObject:SetActive(false)
                playChapterButton.transform:Find("IsFinished").gameObject:SetActive(true)
            elseif state == 5 then
                playChapterButton.transform:Find("Lock").gameObject:SetActive(false)
                playChapterButton.transform:Find("Normal").gameObject:SetActive(true)
                playChapterButton.transform:Find("IsAccepted").gameObject:SetActive(false)
                playChapterButton.transform:Find("IsFinished").gameObject:SetActive(false)
                
            end
        end
    end
end

function LuaProfessionTransferMainWnd:RefreshVariableUI()
    
end 

function LuaProfessionTransferMainWnd:InitUIEvent()
    UIEventListener.Get(self.TipButton).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("PROFESSION_TRANSFER_MAIN_WND_TIP")
    end)

    for i = 1, self.dramaCount do
        if self.juqingButtonList[i] then
            UIEventListener.Get(self.juqingButtonList[i]).onClick = DelegateFactory.VoidDelegate(function (_)
                self:OnClickPlayJuQing(i)
            end)
        end
    end
    
    local videoUrl = ProfessionTransfer_Setting.GetData().ZhanKuangVideos
    UIEventListener.Get(self.PlayVideoButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if not CCPlayerCtrl.IsSupportted() then
            g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
            return
        end
        AMPlayer.Inst:PlayCG(videoUrl, nil, 1)
    end)

    UIEventListener.Get(self.ReadIntroductionButton).onClick = DelegateFactory.VoidDelegate(function (_)
	    CUIManager.ShowUI(CLuaUIResources.ProfessionTransferZhanKuangIntroductionWnd)
    end)

    UIEventListener.Get(self.GoTransferButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local action = ProfessionTransfer_Setting.GetData().FindTransferNPC
        if action then
            CUIManager.CloseUI(CLuaUIResources.ProfessionTransferMainWnd)
            ClientAction.DoAction(action)
        end
    end)

    UIEventListener.Get(self.BattleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CWelfareMgr.OpenWelfareWnd(LocalString.GetString("开书比拼"))
    end)

    UIEventListener.Get(self.TransferReturnButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CWelfareMgr.OpenWelfareWnd(LocalString.GetString("转职返还"))
    end)

    UIEventListener.Get(self.TransferFundButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CWelfareMgr.OpenWelfareWnd(LocalString.GetString("转职基金"))
    end)
end 

function LuaProfessionTransferMainWnd:OnClickPlayJuQing(index)
    local state = self:GetChapterState(index)
    if state == 1 then
        g_MessageMgr:ShowMessage("ProfessionTransferMainWnd_LEVEL_NOT_ENOUGH")
    elseif state == 2 then
        g_MessageMgr:ShowMessage("ProfessionTransferMainWnd_JUQING_NOT_OPEN")
    elseif state == 3 then
        g_MessageMgr:ShowMessage("ProfessionTransferMainWnd_ACCEPTED_BEFORE")
    elseif state == 4 then
        g_MessageMgr:ShowMessage("ProfessionTransferMainWnd_FINISHED_BEFORE")
    elseif state == 5 then
        if self:IsPlayerNewProfession() then
            CUIManager.CloseUI(CLuaUIResources.ProfessionTransferMainWnd)
            CUIManager.ShowUI(CUIResources.DramaEntranceWnd)
        else
            g_MessageMgr:ShowMessage("ProfessionTransferMainWnd_NOT_NEW_PROFESSION")
            local action = ProfessionTransfer_Setting.GetData().ExceptZhanKuangFindTaskNpc
            if action then
                CUIManager.CloseUI(CLuaUIResources.ProfessionTransferMainWnd)
                ClientAction.DoAction(action)
            end
        end
    elseif state == 6 then
        g_MessageMgr:ShowMessage("ProfessionTransferMainWnd_FINISH_PREVIOUS_TASK")
    end
end 
