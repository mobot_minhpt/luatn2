require("3rdParty/ScriptEvent")
require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local Profession = import "L10.Game.Profession"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local MemberPosition = import "L10.Game.CTeamGroupMgr+EnumTeamGroupMemberPosition"
local Color = import "UnityEngine.Color"
local Vector3 = import "UnityEngine.Vector3"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Constants = import "L10.Game.Constants"
local NGUIText = import "NGUIText"
CLuaTeamGroupPlayerInfoItem = class()
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_playerID")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_NameLabel")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_LevelLabel")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_InfoRoot")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_EmptyRoot")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_ClazzIcon")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_LeaderMark")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_HealthProgressBar")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_gameObject")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_OnPress")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_OnDrag")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_Parent")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_GroupLeaderMark")
RegistClassMember(CLuaTeamGroupPlayerInfoItem,"m_HealthForeGround")
local normalColor = Color(1,0.196,0.196)
local managerColor = Color(0.78,0.45,0.176)
local NormalVector3
local DeathVector3
function CLuaTeamGroupPlayerInfoItem:ctor()

end
function CLuaTeamGroupPlayerInfoItem:AttachToGo(go)
	self.m_gameObject = go
	local trans = go.transform
	self.m_NameLabel = LuaGameObject.GetChildNoGC(trans,"NameLabel").label
	self.m_LevelLabel = LuaGameObject.GetChildNoGC(trans,"LevelLabel").label
	self.m_InfoRoot = LuaGameObject.GetChildNoGC(trans,"InfoRoot").gameObject
	self.m_EmptyRoot = LuaGameObject.GetChildNoGC(trans,"EmptyRoot").gameObject
	self.m_ClazzIcon = LuaGameObject.GetChildNoGC(trans,"ClazzIconSprite").sprite
	self.m_LeaderMark = LuaGameObject.GetChildNoGC(trans,"LeaderMark").gameObject
	self.m_HealthProgressBar = LuaGameObject.GetChildNoGC(trans,"HealthBGSprite").progressBar
	self.m_GroupLeaderMark = LuaGameObject.GetChildNoGC(trans,"GroupLeaderMark").gameObject
	self.m_HealthForeGround = LuaGameObject.GetChildNoGC(trans,"HealthForeGround").sprite
	NormalVector3 = self.m_ClazzIcon.transform.localPosition
	DeathVector3 = Vector3(NormalVector3.x,NormalVector3.y,-1)
	local OnPress = function(go,flag)
		if self.m_OnPress ~= nil then
			self.m_OnPress(self.m_Parent, go,flag)
		end
	end
	local OnDrag = function(go,delta)
		if self.m_OnDrag ~= nil then
			self.m_OnDrag(self.m_Parent,go,delta)
		end
	end
	
	CommonDefs.AddOnDragListener(self.m_gameObject,DelegateFactory.Action_GameObject_Vector2(OnDrag),false)
	CommonDefs.AddOnPressListener(self.m_gameObject,DelegateFactory.Action_GameObject_bool(OnPress),false)
end
function CLuaTeamGroupPlayerInfoItem:RefreshInfo(playerID)
	if playerID > 0 then 
		self.m_InfoRoot:SetActive(true)
		self.m_EmptyRoot:SetActive(false)
		local playerInfo = CTeamGroupMgr.Instance:GetMemberInfo(playerID)
		if playerInfo.HealthPercentage == 0 then 
			self.m_ClazzIcon.transform.localPosition = DeathVector3
		else
			self.m_ClazzIcon.transform.localPosition = NormalVector3
		end
		self.m_HealthProgressBar.value = playerInfo.HealthPercentage/100
		self.m_ClazzIcon.spriteName = Profession.GetIcon(playerInfo.Clazz)
		local pos = CTeamGroupMgr.Instance:GetPlayerPos(playerID)
		if pos == MemberPosition.Leader then 		
			self.m_LeaderMark:SetActive(false)
			self.m_GroupLeaderMark:SetActive(true)
		elseif CTeamGroupMgr.Instance:IsTeamLeader(playerID) then
			self.m_GroupLeaderMark:SetActive(false)
			self.m_LeaderMark:SetActive(true)
		else
			self.m_LeaderMark:SetActive(false)
			self.m_GroupLeaderMark:SetActive(false)
		end 
		if pos == MemberPosition.Normal then
			self.m_HealthForeGround.color = normalColor 
		else
			self.m_HealthForeGround.color = managerColor
		end
		if playerID == CClientMainPlayer.Inst.Id then
			self.m_NameLabel.color = Color.green
		else
			self.m_NameLabel.color = Color.white
		end
		self.m_NameLabel.text = playerInfo.Name
		local colorString = NGUIText.EncodeColor24(self.m_LevelLabel.color)
		if playerInfo.IsInXianShenStatus then
			colorString = Constants.ColorOfFeiSheng
		end
		self.m_LevelLabel.text = cs_string.Format("[c][{0}]lv.{1}[-][/c]", colorString, playerInfo.Level)
	else
		self.m_InfoRoot:SetActive(false)
		self.m_EmptyRoot:SetActive(true)
	end
end


return CLuaTeamGroupPlayerInfoItem	