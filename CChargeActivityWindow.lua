-- Auto Generated!!
local CChargeActivityAwardTemplate = import "L10.UI.CChargeActivityAwardTemplate"
local CChargeActivityWindow = import "L10.UI.CChargeActivityWindow"
local Charge_ShopActivity = import "L10.Game.Charge_ShopActivity"
--local Charge_ShopActivityDescription = import "L10.Game.Charge_ShopActivityDescription"
local CommonDefs = import "L10.Game.CommonDefs"
local CWelfareBonusMgr = import "L10.Game.CWelfareBonusMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local NGUITools = import "NGUITools"
local UInt32 = import "System.UInt32"
CChargeActivityWindow.m_Init_CS2LuaHook = function (this, ID) 
    this.ID = ID
    local shopAct = Charge_ShopActivity.GetData(ID)
    if shopAct == nil then
        return
    end
    this:InitSubTitle(shopAct.Daily, shopAct.Type)
    this.titleLabel.text = shopAct.Title
    this.descLabel.text = shopAct.Description
    Gac2Gas.QueryShopActivityInfo(ID)
end
CChargeActivityWindow.m_InitSubTitle_CS2LuaHook = function (this, daily, type) 
    Charge_ShopActivityDescription.Foreach(function (key, data) 
        if data.Daily == daily and data.Type == type then
            this.subLabel.text = data.Description
        end
    end)
end
CChargeActivityWindow.m_InitAwardInfo_CS2LuaHook = function (this, ID) 
    if ID ~= this.ID then
        return
    end
    Extensions.RemoveAllChildren(this.table.transform)
    this.awardTemplate:SetActive(false)
    local dic = CWelfareBonusMgr.Inst:GetAwardDetail(ID)
    if dic == nil then
        return
    end
    local index = 1
    CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, ___value) 
        local data = {}
        data.Key = ___key
        data.Value = ___value
        local instance = NGUITools.AddChild(this.table.gameObject, this.awardTemplate)
        instance:SetActive(true)
        local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CChargeActivityAwardTemplate))
        if template ~= nil then
            template:Init(ID, data.Value, data.Key, CWelfareBonusMgr.Inst.chargeActivityAccumulation, CommonDefs.ListContains(CWelfareBonusMgr.Inst.receivedAwardID, typeof(UInt32), index), index)
            index = index + 1
        end
    end))
    this.table:Reposition()
    this.scrollview:ResetPosition()
end
