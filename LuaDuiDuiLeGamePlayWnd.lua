local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"

LuaDuiDuiLeGamePlayWnd = class()

RegistClassMember(LuaDuiDuiLeGamePlayWnd, "m_JoystickEnabled")

function LuaDuiDuiLeGamePlayWnd:Init()
end

function LuaDuiDuiLeGamePlayWnd:OnEnable()
    g_ScriptEvent:AddListener("YuanXiao2020_UpdatePlayerInfos", self, "LoadPlayerInfosData")
end

function LuaDuiDuiLeGamePlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("YuanXiao2020_UpdatePlayerInfos", self, "LoadPlayerInfosData")
end

function LuaDuiDuiLeGamePlayWnd:LoadPlayerInfosData(data)
    local playerInfos = LuaYuanXiao2020Mgr:GetPlayerInfos()

    for i = 1,#playerInfos do
        local scale = playerInfos[i].Scale
        local playerId = playerInfos[i].ID
        local co = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if co then
            local ro = co.RO
            if ro then
                ro.Scale = scale
            end
        end
    end
end
