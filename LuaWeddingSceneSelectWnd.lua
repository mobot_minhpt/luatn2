local DelegateFactory   = import "DelegateFactory"
local UILabel           = import "UILabel"
local GameObject        = import "UnityEngine.GameObject"
local EnumGender        = import "L10.Game.EnumGender"
local BoxCollider       = import "UnityEngine.BoxCollider"
local CUICommonDef      = import "L10.UI.CUICommonDef"
local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"
local LuaTweenUtils     = import "LuaTweenUtils"
local Vector3           = import "UnityEngine.Vector3"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local NGUIText          = import "NGUIText"
local UIEventListener   = import "UIEventListener"
local CCurentMoneyCtrl  = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType     = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey  = import "L10.Game.EnumPlayScoreKey"
local CItemMgr          = import "L10.Game.CItemMgr"
local EnumItemPlace     = import "L10.Game.EnumItemPlace"
local TweenScale        = import "TweenScale"

LuaWeddingSceneSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaWeddingSceneSelectWnd, "UpLabel", "UpLabel", UILabel)
RegistChildComponent(LuaWeddingSceneSelectWnd, "Left", "Left", GameObject)
RegistChildComponent(LuaWeddingSceneSelectWnd, "Right", "Right", GameObject)
RegistChildComponent(LuaWeddingSceneSelectWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingSceneSelectWnd, "mainsTrans")
RegistClassMember(LuaWeddingSceneSelectWnd, "picsCTex")
RegistClassMember(LuaWeddingSceneSelectWnd, "hearts")
RegistClassMember(LuaWeddingSceneSelectWnd, "considersLabel")
RegistClassMember(LuaWeddingSceneSelectWnd, "unavailsLabel")
RegistClassMember(LuaWeddingSceneSelectWnd, "selectWndsGo")
RegistClassMember(LuaWeddingSceneSelectWnd, "namesLabel")
RegistClassMember(LuaWeddingSceneSelectWnd, "costsLabel")

RegistClassMember(LuaWeddingSceneSelectWnd, "scenesInfo")
RegistClassMember(LuaWeddingSceneSelectWnd, "mainsPos")
RegistClassMember(LuaWeddingSceneSelectWnd, "curSelect")
RegistClassMember(LuaWeddingSceneSelectWnd, "curPartnerSelect")
RegistClassMember(LuaWeddingSceneSelectWnd, "stage")
RegistClassMember(LuaWeddingSceneSelectWnd, "countDownNum")
RegistClassMember(LuaWeddingSceneSelectWnd, "confirmPaysGo")
RegistClassMember(LuaWeddingSceneSelectWnd, "payWndsGo")
RegistClassMember(LuaWeddingSceneSelectWnd, "heartsTrans")

RegistClassMember(LuaWeddingSceneSelectWnd, "words")
RegistClassMember(LuaWeddingSceneSelectWnd, "wordSelect")
RegistClassMember(LuaWeddingSceneSelectWnd, "partnerWordSelect")
RegistClassMember(LuaWeddingSceneSelectWnd, "endTimeStamp")
RegistClassMember(LuaWeddingSceneSelectWnd, "countDownTick")
RegistClassMember(LuaWeddingSceneSelectWnd, "countDownLabel")
RegistClassMember(LuaWeddingSceneSelectWnd, "countDownFx")

RegistClassMember(LuaWeddingSceneSelectWnd, "scaleTween")
RegistClassMember(LuaWeddingSceneSelectWnd, "scaleBig")
RegistClassMember(LuaWeddingSceneSelectWnd, "scaleSmall")

function LuaWeddingSceneSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

    --@endregion EventBind end

	self:InitUIComponents()
end

function LuaWeddingSceneSelectWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncChooseNewWeddingScene", self, "OnSyncChooseNewWeddingScene")
	g_ScriptEvent:AddListener("SyncChooseNewWeddingSceneCountDown", self, "OnSyncCountDown")
end

function LuaWeddingSceneSelectWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncChooseNewWeddingScene", self, "OnSyncChooseNewWeddingScene")
	g_ScriptEvent:RemoveListener("SyncChooseNewWeddingSceneCountDown", self, "OnSyncCountDown")
end

function LuaWeddingSceneSelectWnd:OnSyncChooseNewWeddingScene(tbl)
	local playerId = CClientMainPlayer.Inst.Id
	tbl[1].choice = self:MapId2SelectId(tbl[1].choice)
	tbl[2].choice = self:MapId2SelectId(tbl[2].choice)

	if tbl[1].choice == 0 and tbl[2].choice == 0 then
		g_MessageMgr:ShowMessage("WEDDING_PARTNER_LEAVE_SCENE_SELECT_WND")
		CUIManager.CloseUI(CLuaUIResources.WeddingSceneSelectWnd)
		return
	end

	for i = 1, 2 do
		if tbl[i].playerId ~= playerId then
			if tbl[i].choice ~= self.curPartnerSelect then
				self.curPartnerSelect = tbl[i].choice
				if self.stage == 1 then
					self:UpdateHalfHeart(true)
					self:UpdateConsider()
				else
					local str = CClientMainPlayer.Inst.Gender == EnumGender.Male and LocalString.GetString("新娘") or LocalString.GetString("新郎")
					g_MessageMgr:ShowMessage("WEDDING_PARTNER_WANT_CONSIDER_AGAIN", str)
					self:ChangeStage(1)
				end
			elseif tbl[i].sameWithLast == true then
				self:UpdateBubble(true)
			end
		end
	end
end

-- mapId转换为显示的序号
function LuaWeddingSceneSelectWnd:MapId2SelectId(mapId)
	local selectId = 0
	for i = 1, 2 do
		if self.scenesInfo[i].mapId == mapId then
			selectId = i
			break
		end
	end
	return selectId
end

function LuaWeddingSceneSelectWnd:OnSyncCountDown(endTimestamp)
	self.endTimeStamp = endTimestamp
	self:ChangeStage(2)
end

function LuaWeddingSceneSelectWnd:Init()
	self:InitPartnerInfo()
	self:InitClassMembers()

	self:InitScenesInfo()
	self:InitSelectWnd()
	self:ChangeStage(1)
end

function LuaWeddingSceneSelectWnd:InitPartnerInfo()
	Gac2Gas.QueryNewWeddingPartner()
end

-- 初始化UI组件
function LuaWeddingSceneSelectWnd:InitUIComponents()
	local leftTrans = self.Left.transform
	local rightTrans = self.Right.transform
	self.mainsTrans = {leftTrans, rightTrans}

	-- 记录位置
	local leftPos = leftTrans.localPosition
	local rightPos = rightTrans.localPosition
	self.mainsPos = {leftPos, rightPos}

	self.picsCTex = {}
	self.hearts = {}
	self.considersLabel = {}
	self.selectWndsGo = {}
	self.namesLabel = {}
	self.costsLabel = {}
	self.payWndsGo = {}
	self.confirmPaysGo = {}
	self.heartsTrans = {}
	self.unavailsLabel = {}

	for i = 1, 2 do
		self:InitHalfUIComponents(self.mainsTrans[i])
	end
end

-- 初始化一半组件
function LuaWeddingSceneSelectWnd:InitHalfUIComponents(parentTrans)
	local picCTex = parentTrans:Find("Picture"):GetComponent(typeof(CUITexture))
	table.insert(self.picsCTex, picCTex)
	local heartTrans = parentTrans:Find("Heart")
	table.insert(self.heartsTrans, heartTrans)

	local leftHeartTrans = heartTrans:Find("Left")
	local leftHeart = self:InitHalfHeart(leftHeartTrans)

	local rightHeartTrans = heartTrans:Find("Right")
	local rightHeart = self:InitHalfHeart(rightHeartTrans)
	table.insert(self.hearts, {leftHeart, rightHeart})

	local confirmPayGo = heartTrans:Find("ConfirmPay").gameObject
	table.insert(self.confirmPaysGo, confirmPayGo)

	local considerLabel = parentTrans:Find("Consider"):GetComponent(typeof(UILabel))
	table.insert(self.considersLabel, considerLabel)

	local selectWndTrans = parentTrans:Find("SelectWnd")
	local selectWndGo = selectWndTrans.gameObject
	selectWndGo:SetActive(false)
	table.insert(self.selectWndsGo, selectWndGo)

	local nameLabel = parentTrans:Find("Name"):GetComponent(typeof(UILabel))
	table.insert(self.namesLabel, nameLabel)

	local costLabel = selectWndTrans:Find("Cost"):GetComponent(typeof(UILabel))
	table.insert(self.costsLabel, costLabel)

	selectWndTrans:Find("CountDown").gameObject:SetActive(false)

	local unavailLabel = selectWndTrans:Find("Unavailable"):GetComponent(typeof(UILabel))
	table.insert(self.unavailsLabel, unavailLabel)

	local payWndTrans = parentTrans:Find("PayWnd")
	local payWndGo = payWndTrans.gameObject
	payWndGo:SetActive(false)
	table.insert(self.payWndsGo, payWndGo)
end

function LuaWeddingSceneSelectWnd:InitHalfHeart(parentTrans)
	local heart = {}
	local avatarCTex = parentTrans:Find("Avatar"):GetComponent(typeof(CUITexture))
	avatarCTex.gameObject:SetActive(false)
	heart.avatarCTex = avatarCTex

	local selectedGo = parentTrans:Find("Bg").gameObject
	selectedGo:SetActive(false)
	heart.selectedGo = selectedGo

	local wordLabel = parentTrans:Find("Word"):GetComponent(typeof(UILabel))
	wordLabel.gameObject:SetActive(false)
	heart.wordLabel = wordLabel

	return heart
end

-- 初始化成员变量
function LuaWeddingSceneSelectWnd:InitClassMembers()
	self.curSelect = 0
	self.curPartnerSelect = 0
	self.stage = 1

	self.scaleTween = 0.9
	self.scaleBig = 1.2
	self.scaleSmall = 0.9

	self.wordSelect = 0
	self.partnerWordSelect = 0

	local wordsArray = WeddingIteration_Setting.GetData().SceneSelectBubbleWord
	self.words = LuaWeddingIterationMgr:Array2Tbl(wordsArray)
end

-- 更新上面标签内容
function LuaWeddingSceneSelectWnd:UpdateUpLabel()
	if self.stage == 1 then
		self.UpLabel.text = LocalString.GetString("请共同选择一套婚礼场景")
		self.UpLabel.gameObject:SetActive(true)
	elseif self.stage == 2 then
		self.UpLabel.text = SafeStringFormat3(LocalString.GetString("保持当前选择并静待%d秒"), self.countDownNum)
		self.UpLabel.gameObject:SetActive(true)
	elseif self.stage == 3 then
		self.UpLabel.gameObject:SetActive(false)
	end
end

-- 初始化场景信息
function LuaWeddingSceneSelectWnd:InitScenesInfo()
    self.scenesInfo = {}
    for id = 1, 2 do
        local tbl = {}
        tbl.mapId = id
        local data = WeddingIteration_Skybox.GetData(id)
        tbl.cost = data.Cost
		tbl.reviewCost = data.ReviewCost
        tbl.name = data.Name
        tbl.preview = data.Preview
        table.insert(self.scenesInfo, tbl)
    end

    -- 按价格降序排列
    table.sort(self.scenesInfo, function(a, b)
        return a.cost > b.cost
    end)
end

function LuaWeddingSceneSelectWnd:InitSelectWnd()
	for id = 1, 2 do
		self:InitOneSelectWnd(id)
	end
end

-- 是不是队长
function LuaWeddingSceneSelectWnd:IsLeader()
	local playerId = CClientMainPlayer.Inst.Id
	local leaderId = LuaWeddingIterationMgr.sceneSelectInfo.leaderId
	return playerId == leaderId
end

function LuaWeddingSceneSelectWnd:InitOneSelectWnd(id)
	self.namesLabel[id].text = self.scenesInfo[id].name
	self.picsCTex[id]:LoadMaterial(self.scenesInfo[id].preview)

	-- 队长显示价格
	if self:IsLeader() then
		self.costsLabel[id].text = LuaWeddingIterationMgr.isReview and self.scenesInfo[id].reviewCost or self.scenesInfo[id].cost
		self.costsLabel[id].gameObject:SetActive(true)
	else
		self.costsLabel[id].gameObject:SetActive(false)
	end

	local mapId = self.scenesInfo[id].mapId
	local mapStatus = LuaWeddingIterationMgr.sceneSelectInfo.mapStatus

	local mainGo = self.mainsTrans[id].gameObject
	if mapStatus[mapId] == 1 then
		self.unavailsLabel[id].gameObject:SetActive(false)

		UIEventListener.Get(mainGo).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnAvailableSceneClick(id)
		end)

		local considerGo = self.considersLabel[id].gameObject
		UIEventListener.Get(considerGo).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnConsiderClick()
		end)
	else
		if not self:IsLeader() then
			self.unavailsLabel[id].text = LocalString.GetString("暂不可用")
		else
			self.unavailsLabel[id].text = LocalString.GetString("银两不足")
		end
		self.unavailsLabel[id].gameObject:SetActive(true)

		UIEventListener.Get(mainGo).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
			self:OnUnavailableScenePress(go, isPressed)
		end)
	end
end

-- 点击坚持己见或再考虑下
function LuaWeddingSceneSelectWnd:OnConsiderClick()
	if self.stage == 1 then
		self:UpdateBubble(false)
		Gac2Gas.RequestChoseNewWeddingScene(self.scenesInfo[self.curSelect].mapId)
	else
		Gac2Gas.RequestChoseNewWeddingScene(0)
		self.curSelect = 0
		self:ChangeStage(1)
	end
end

-- 更新选择提示框
function LuaWeddingSceneSelectWnd:UpdateHeart()
	local id = self.curSelect

	if self.stage == 1 then
		for i = 1, 2 do
			self.confirmPaysGo[i]:SetActive(false)
			local boxCollider = self.heartsTrans[i]:GetComponent(typeof(BoxCollider))
			boxCollider.enabled = false

			self.picsCTex[i].color = NGUIText.ParseColor24("FFFFFF", 0)
		end

		self:UpdateHalfHeart(true)
		self:UpdateHalfHeart(false)

	elseif self.stage == 2 then
		-- 不显示气泡
		for j = 1, 2 do
			self.hearts[id][j].wordLabel.gameObject:SetActive(false)
		end
	elseif self.stage == 3 then
		if self:IsLeader() then
			-- 更换为确认支付
			for j = 1, 2 do
				self.hearts[id][j].avatarCTex.gameObject:SetActive(false)
			end
			self.confirmPaysGo[id]:SetActive(true)
			local boxCollider = self.heartsTrans[id]:GetComponent(typeof(BoxCollider))
			boxCollider.enabled = true

			UIEventListener.Get(self.heartsTrans[id].gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				if self.stage ~= 3 then return end
				local bReview = LuaWeddingIterationMgr.isReview
				if not bReview then
					Gac2Gas.RequestPayNewWeddingSceneTicket(self.scenesInfo[id].cost, bReview, "", 1, 1, 1, 1.0)
				else
					local found, discount, itemId, pos = self:GetDiscountInfo()
					if found then
						Gac2Gas.RequestPayNewWeddingSceneTicket(math.ceil(self.scenesInfo[id].reviewCost * discount), bReview, itemId, EnumItemPlace_lua.Bag, pos, 1, discount)
					else
						Gac2Gas.RequestPayNewWeddingSceneTicket(self.scenesInfo[id].reviewCost, bReview, "", 0, 0, 0, 1.0)
					end
				end
			end)
		end

		self.picsCTex[id].color = NGUIText.ParseColor24("7F7F7F", 0)
	end
end

-- 获取打折券id、折扣、位置
function LuaWeddingSceneSelectWnd:GetDiscountInfo()
	local discountItemId = WeddingIteration_Setting.GetData().ReviewDiscountItemId
	local discount = WeddingIteration_Setting.GetData().ReviewDiscount

	local itemProp = CClientMainPlayer.Inst.ItemProp
	local bagSize =  itemProp:GetPlaceSize(EnumItemPlace.Bag)
	for i = 1, bagSize do
		local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
		local item = CItemMgr.Inst:GetById(itemId)
		if item and item.TemplateId == discountItemId and not item.Item:IsExpire() then
			return true, discount, itemId, i
		end
	end

	return false
end

-- 更新选择提示框的头像显示
function LuaWeddingSceneSelectWnd:UpdateHalfHeart(isPartner)
	-- 性别、职业和选择的场景
	local gender, class, id
	if isPartner == false then
		gender = CClientMainPlayer.Inst.Gender
		class = CClientMainPlayer.Inst.Class
		id = self.curSelect
	else
		if LuaWeddingIterationMgr.partnerInfo == nil then return end
		gender = LuaWeddingIterationMgr.partnerInfo.gender
		class = LuaWeddingIterationMgr.partnerInfo.class
		id = self.curPartnerSelect
	end

	if id <= 0 then
		for i = 1, 2 do
			self:SetHeartActive(gender, class, i, false)
		end
		return
	end

	self:SetHeartActive(gender, class, id, true)
	local otherId = (id == 1) and 2 or 1
	self:SetHeartActive(gender, class, otherId, false)
end

-- 设置头像显示和状态
function LuaWeddingSceneSelectWnd:SetHeartActive(gender, class, id, active)
	local heart = gender == EnumGender.Male and self.hearts[id][1] or self.hearts[id][2]

	if active then
		local avatarCTex = heart.avatarCTex

		local expression = 6 -- 害羞
		local mat = CUICommonDef.GetPortraitName(class, gender, expression)
		avatarCTex:LoadNPCPortrait(mat)
	end

	heart.selectedGo:SetActive(active)
	heart.avatarCTex.gameObject:SetActive(active)
	heart.wordLabel.gameObject:SetActive(false)
end

-- 更新气泡显示
function LuaWeddingSceneSelectWnd:UpdateBubble(isPartner)
	if self.stage ~= 1 then return end

	local gender, id, curWordId
	if isPartner == false then
		gender = CClientMainPlayer.Inst.Gender
		id = self.curSelect
		curWordId = self.wordSelect
	else
		if LuaWeddingIterationMgr.partnerInfo == nil then return end
		gender = LuaWeddingIterationMgr.partnerInfo.gender
		id = self.curPartnerSelect
		curWordId = self.partnerWordSelect
	end

	-- 随机一个气泡
	local wordNum = #self.words
	local newWordId = LuaWeddingIterationMgr:GetDifferentRandomNum(wordNum, curWordId)

	if isPartner == false then
		self.wordSelect = newWordId
	else
		self.partnerWordSelect = newWordId
	end

	local heart = gender == EnumGender.Male and self.hearts[id][1] or self.hearts[id][2]
	LuaWeddingIterationMgr:SetBubbleWord(heart.wordLabel, self.words[newWordId])
end

-- 改变阶段
function LuaWeddingSceneSelectWnd:ChangeStage(newStage)
	if newStage == 1 then
		if self.stage == 2 then
			self:ClearCountDownTick()
			if self.countDownLabel then
				self.countDownLabel.gameObject:SetActive(false)
				self.countDownFx:DestroyFx()
				self.countDownLabel = nil
			end
		end

		for id = 1, 2 do
			self.payWndsGo[id]:SetActive(false)

			local trans = self.mainsTrans[id]
			trans.localPosition = self.mainsPos[id]
			if self.curSelect <= 0 then
				trans.localScale = Vector3.one
			end
			trans.gameObject:SetActive(true)
			self.selectWndsGo[id]:SetActive(true)
		end

	elseif newStage == 2 then
		local id = self.curSelect
		local trans = self.mainsTrans[id]
		local pos = trans.localPosition
		pos.x = 0
		trans.localPosition = pos

		local otherId = (id == 1) and 2 or 1
		self.mainsTrans[otherId].gameObject:SetActive(false)
		self.countDownLabel = self.selectWndsGo[id].transform:Find("CountDown"):GetComponent(typeof(UILabel))
		self.countDownFx = self.selectWndsGo[id].transform:Find("CountDownFx"):GetComponent(typeof(CUIFx))
		self.countDownFx:LoadFx(g_UIFxPaths.WeddingDaoJiShiFxPath)
		self.countDownLabel.gameObject:SetActive(true)
		self:StartCountDown()
	elseif newStage == 3 then
		local id = self.curSelect
		self.selectWndsGo[id]:SetActive(false)
		-- 打开支付界面
		self:InitPayWnd()
		self.payWndsGo[id]:SetActive(true)
	end

	self.stage = newStage
	self:UpdateUpLabel()
	self:UpdateHeart()
	self:UpdateConsider()
end

-- 开始倒计时
function LuaWeddingSceneSelectWnd:StartCountDown()
	self:UpdateTimeOnce()
	self:ClearCountDownTick()
	self.countDownTick = RegisterTick(function()
		self:UpdateTimeOnce()
	end, 1000)
end

function LuaWeddingSceneSelectWnd:ClearCountDownTick()
	if self.countDownTick then
		UnRegisterTick(self.countDownTick)
		self.countDownTick = nil
	end
end

-- 更新一次时间
function LuaWeddingSceneSelectWnd:UpdateTimeOnce()
	self.countDownNum = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
	if self.countDownNum <= 0 then
		self:ClearCountDownTick()
		self.countDownLabel.gameObject:SetActive(false)
		self.countDownFx:DestroyFx()
		self.countDownLabel = nil
		self:ChangeStage(3)
		return
	end
	self:UpdateUpLabel()
	self.countDownLabel.text = self.countDownNum

	-- 收缩动效
	local tweenScale = self.countDownLabel.transform:GetComponent(typeof(TweenScale))
	tweenScale:ResetToBeginning()
	tweenScale:PlayForward()
end

-- 初始化支付界面
function LuaWeddingSceneSelectWnd:InitPayWnd()
	local id = self.curSelect
	local trans = self.payWndsGo[id].transform
	local tipLabel = trans:Find("Tip"):GetComponent(typeof(UILabel))
	local partnerName = LuaWeddingIterationMgr.partnerInfo.name

	local moneyCtrl = trans:Find("QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
	local discountGo = moneyCtrl.transform:Find("Discount").gameObject
	discountGo:SetActive(false)

	if self:IsLeader() then
		moneyCtrl.gameObject:SetActive(true)
		moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)

		if not LuaWeddingIterationMgr.isReview then
			moneyCtrl:SetCost(self.scenesInfo[id].cost)
		else
			local cost = self.scenesInfo[id].reviewCost
			local found, discount = self:GetDiscountInfo()
			if found then
				local discountLabel = discountGo.transform:Find("Label"):GetComponent(typeof(UILabel))
				discountLabel.text = SafeStringFormat3(LocalString.GetString("%d折"), discount * 10)
				discountGo:SetActive(true)
				moneyCtrl:SetCost(math.ceil(cost * discount))
			else
				moneyCtrl:SetCost(cost)
			end
		end

		local text = SafeStringFormat3(LocalString.GetString("将在此举办您与%s的婚礼"), partnerName)
		tipLabel.text = text
		tipLabel.transform.localPosition = Vector3(0, 75, 0)
	else
		moneyCtrl.gameObject:SetActive(false)

		local str = CClientMainPlayer.Inst.Gender == EnumGender.Male and LocalString.GetString("新娘") or LocalString.GetString("新郎")
		local text = SafeStringFormat3(LocalString.GetString("将在此举办您与%s的婚礼\n%s正在支付所需银两"), partnerName, str)
		tipLabel.text = text
		tipLabel.transform.localPosition = Vector3(0, 0, 0)
	end
end

-- 更新坚持己见或再考虑下
function LuaWeddingSceneSelectWnd:UpdateConsider()
	-- 有人未选择
	if self.curSelect <= 0 or self.curPartnerSelect <= 0 then
		for i = 1, 2 do
			self.considersLabel[i].gameObject:SetActive(false)
		end
		return
	end

	local id = self.curSelect
	local considerLabel = self.considersLabel[id]
	local considerGo = considerLabel.gameObject

	if self.stage == 1 then
		local otherId = (id == 1) and 2 or 1
		local otherConsiderLabel = self.considersLabel[otherId]
		local otherConsiderGo = otherConsiderLabel.gameObject
		otherConsiderGo:SetActive(false)

		if self.curSelect ~= self.curPartnerSelect then
			considerLabel.text = LocalString.GetString("坚持己见?")
			considerGo:SetActive(true)
		else
			considerGo:SetActive(false)
		end
	elseif self.stage == 2 then
		considerLabel.text = LocalString.GetString("再考虑下?")
		considerGo:SetActive(true)
	elseif self.stage == 3 then
		if self:IsLeader() then
			considerLabel.text = LocalString.GetString("再考虑下?")
			considerGo:SetActive(true)
		else
			considerGo:SetActive(false)
		end
	end
end

-- 关闭界面
function LuaWeddingSceneSelectWnd:OnDestroy()
	self:ClearCountDownTick()
	LuaWeddingIterationMgr.sceneSelectInfo = {}
end

--@region UIEvent

-- 点击不可用的场景，略微收缩后还原
function LuaWeddingSceneSelectWnd:OnUnavailableScenePress(go, isPressed)
	local trans = go.transform
	if isPressed then
		if self.curSelect > 0 then
			local scale = self.scaleTween * self.scaleSmall
			LuaTweenUtils.DOScale(trans, Vector3(scale, scale, 1), 0.2)
		else
			LuaTweenUtils.DOScale(trans, Vector3(self.scaleTween, self.scaleTween, 1), 0.2)
		end
	else
		if self.curSelect > 0 then
			LuaTweenUtils.DOScale(trans, Vector3(self.scaleSmall, self.scaleSmall, 1), 0.2)
		else
			LuaTweenUtils.DOScale(trans, Vector3.one, 0.2)
		end
	end
end

-- 点击可用的场景
function LuaWeddingSceneSelectWnd:OnAvailableSceneClick(id)
	if self.stage ~= 1 then return end
	if id == self.curSelect then return end

	-- 另一个场景Id
	local otherId = (id == 1) and 2 or 1

	-- 缩放
	self.mainsTrans[id].localScale = Vector3(self.scaleBig, self.scaleBig, 1)
	self.mainsTrans[otherId].localScale = Vector3(self.scaleSmall, self.scaleSmall, 1)

	self.curSelect = id
	self:UpdateHalfHeart(false)
	self:UpdateConsider()

	Gac2Gas.RequestChoseNewWeddingScene(self.scenesInfo[id].mapId)
end

function LuaWeddingSceneSelectWnd:OnCloseButtonClick()
	local partnerName = LuaWeddingIterationMgr.partnerInfo.name
	local message = g_MessageMgr:FormatMessage("WEDDING_CLOSE_SCENE_SELECT", partnerName)
	MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
		Gac2Gas.RequestCancelNewWeddingScene()
		CUIManager.CloseUI(CLuaUIResources.WeddingSceneSelectWnd)
	end), nil, nil, nil, false)
end

--@endregion UIEvent
