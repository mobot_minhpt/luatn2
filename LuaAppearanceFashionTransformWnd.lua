local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CUITexture = import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Item_Item = import "L10.Game.Item_Item"
local CButton = import "L10.UI.CButton"

LuaAppearanceFashionTransformWnd = class()


RegistChildComponent(LuaAppearanceFashionTransformWnd, "m_Label", "Label", UILabel)
RegistChildComponent(LuaAppearanceFashionTransformWnd, "m_ScrItem", "StandardItem", GameObject)
RegistChildComponent(LuaAppearanceFashionTransformWnd, "m_DstItem", "PremiumItem", GameObject)
RegistChildComponent(LuaAppearanceFashionTransformWnd, "m_MoneyCtrl", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaAppearanceFashionTransformWnd, "m_TransformButton", "TransformButton", CButton)
RegistChildComponent(LuaAppearanceFashionTransformWnd, "m_InfoButton", "InfoButton", GameObject)

RegistClassMember(LuaAppearanceFashionTransformWnd, "m_NeedTransferCBGItemId")
RegistClassMember(LuaAppearanceFashionTransformWnd, "m_SrcItemTemplateId")
RegistClassMember(LuaAppearanceFashionTransformWnd, "m_SrcItemName")
RegistClassMember(LuaAppearanceFashionTransformWnd, "m_DstItemTemplateId")
RegistClassMember(LuaAppearanceFashionTransformWnd, "m_DstItemName")
RegistClassMember(LuaAppearanceFashionTransformWnd, "m_Cost")
RegistClassMember(LuaAppearanceFashionTransformWnd, "m_SrcRulePos")
RegistClassMember(LuaAppearanceFashionTransformWnd, "m_DstRulePos")

function LuaAppearanceFashionTransformWnd:Awake()
    UIEventListener.Get(self.m_TransformButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnTransformButtonClick() end)
    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnInfoButtonClick() end)
end

function LuaAppearanceFashionTransformWnd:Init()
    self.m_NeedTransferCBGItemId = LuaAppearancePreviewMgr.m_NeedTransferCBGItemId
    local commonItem = CItemMgr.Inst:GetById(self.m_NeedTransferCBGItemId)
    local srcItem = Item_Item.GetData(commonItem.TemplateId)
    self.m_SrcItemTemplateId, self.m_SrcItemName = srcItem.ID, srcItem.Name
    local targetInfo = LuaAppearancePreviewMgr:GetCBGTargetInfo(srcItem.ID)
    self.m_DstItemTemplateId, self.m_Cost = targetInfo.targetId, targetInfo.cost
    local dstItem =  Item_Item.GetData(self.m_DstItemTemplateId)
    self.m_DstItemName = dstItem.Name

    self.m_Label.text = g_MessageMgr:FormatMessage("Appearance_Preview_CBG_Transform_Title", srcItem.Name, dstItem.Name)
    self:InitItem(self.m_ScrItem, srcItem)
    self:InitItem(self.m_DstItem, dstItem)
    self.m_MoneyCtrl:SetCost(self.m_Cost)

    if self.m_DstRulePos==nil then
        self.m_SrcRulePos = self.m_ScrItem.transform:Find("Table").transform.position
        self.m_DstRulePos = self.m_DstItem.transform:Find("Table").transform.position
    end
    if LuaAppearancePreviewMgr:IsCBGTradeItem(self.m_SrcItemTemplateId) then
        self.m_TransformButton.Text = LocalString.GetString("转天衣锦盒")
        self.m_ScrItem.transform:Find("Table").transform.position = self.m_DstRulePos
    self.m_DstItem.transform:Find("Table").transform.position = self.m_SrcRulePos
    else
        self.m_TransformButton.Text = LocalString.GetString("转藏宝锦盒")
        self.m_ScrItem.transform:Find("Table").transform.position = self.m_SrcRulePos
    self.m_DstItem.transform:Find("Table").transform.position = self.m_DstRulePos
    end
end

function LuaAppearanceFashionTransformWnd:InitItem(go, item_item)
    local icon = go.transform:GetComponent(typeof(CUITexture))
    local border = go.transform:Find("Border"):GetComponent(typeof(UISprite))
    icon:LoadMaterial(item_item.Icon)
    border.spriteName = CUICommonDef.GetItemCellBorder(item_item, nil, false)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go) CItemInfoMgr.ShowLinkItemTemplateInfo(item_item.ID) end)
end


function LuaAppearanceFashionTransformWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRequestChangeToCbgTradeItemResult",self,"OnRequestChangeToCbgTradeItemResult")
    g_ScriptEvent:AddListener("OnRequestChangeToNoneCbgTradeItemResult",self,"OnRequestChangeToNoneCbgTradeItemResult")
end

function LuaAppearanceFashionTransformWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRequestChangeToCbgTradeItemResult",self,"OnRequestChangeToCbgTradeItemResult")
    g_ScriptEvent:RemoveListener("OnRequestChangeToNoneCbgTradeItemResult",self,"OnRequestChangeToNoneCbgTradeItemResult")
end

function LuaAppearanceFashionTransformWnd:OnRequestChangeToCbgTradeItemResult(itemId, bSuccess)
    if self.m_NeedTransferCBGItemId == itemId and bSuccess then
        self:Close()
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_CBG_Transform_To_Trade_Item_Success", self.m_DstItemName), function()
            LuaAppearancePreviewMgr:OpenCBG()
        end, nil, nil, nil, false)
    end
end

function LuaAppearanceFashionTransformWnd:OnRequestChangeToNoneCbgTradeItemResult(itemId, bSuccess)
    if self.m_NeedTransferCBGItemId == itemId and bSuccess then
        self:Close()
        g_MessageMgr:ShowMessage("Appearance_Preview_CBG_Transform_To_None_Trade_Item_Success", self.m_DstItemName)
    end
end

function LuaAppearanceFashionTransformWnd:OnTransformButtonClick()
    if LuaAppearancePreviewMgr:IsCBGTradeItem(self.m_SrcItemTemplateId) then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_CBG_Transform_To_None_Trade_Item_Confirm", self.m_DstItemName, self.m_SrcItemName, self.m_Cost, self.m_SrcItemName, self.m_DstItemName), function()
            LuaAppearancePreviewMgr:RequestChangeToNoneCbgTradeItem(self.m_NeedTransferCBGItemId)
        end, nil, nil, nil, false)
        
    else
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Appearance_Preview_CBG_Transform_To_Trade_Item_Confirm", self.m_DstItemName, self.m_SrcItemName, self.m_Cost, self.m_SrcItemName, self.m_DstItemName), function()
            LuaAppearancePreviewMgr:RequestChangeToCbgTradeItem(self.m_NeedTransferCBGItemId)
        end, nil, nil, nil, false)
    end
end

function LuaAppearanceFashionTransformWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("Appearance_Preview_CBG_Transform_Tip")
end

function LuaAppearanceFashionTransformWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

