-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CCommonItem = import "L10.Game.CCommonItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPlayerTalismanInfoWnd = import "L10.UI.CPlayerTalismanInfoWnd"
local CPlayerTalismanItemCell = import "L10.UI.CPlayerTalismanItemCell"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CPropertyItem = import "L10.Game.CPropertyItem"
CPlayerTalismanInfoWnd.m_Init_CS2LuaHook = function (this) 

    local default
    if (CPlayerInfoMgr.PlayerInfo ~= nil) then
        default = CPlayerInfoMgr.PlayerInfo.name .. LocalString.GetString("的法宝")
    else
        default = ""
    end
    this.titleLabel.text = default
    Extensions.RemoveAllChildren(this.rotTable.transform)
    CommonDefs.ListClear(this.itemCells)
    local delta = 0
    if CPlayerInfoMgr.PlayerInfo.talismanIndex > 1 then
        delta = CPropertyItem.s_MultipleColnumTalismanEnabled and ((CPlayerInfoMgr.PlayerInfo.talismanIndex - 1) * 8) or 8
    end
    do
        local i = EnumBodyPosition_lua.TalismanQian + delta
        while i <= EnumBodyPosition_lua.TalismanDui + delta do
            local instance = CUICommonDef.AddChild(this.rotTable.gameObject, this.talismanTemplate)
            instance:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CPlayerTalismanItemCell))
            if CPlayerInfoMgr.PlayerInfo ~= nil and CommonDefs.DictContains(CPlayerInfoMgr.PlayerInfo.pos2equipDict, typeof(UInt32), i) then
                template:Init(i, CommonDefs.DictGetValue(CPlayerInfoMgr.PlayerInfo.pos2equipDict, typeof(UInt32), i))
            else
                template:Init(i, nil)
            end
            CommonDefs.ListAdd(this.itemCells, typeof(CPlayerTalismanItemCell), template)
            UIEventListener.Get(instance).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            i = i + 1
        end
    end
    this.rotTable:Reposition()
end
CPlayerTalismanInfoWnd.m_OnItemClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.itemCells.Count do
            if go == this.itemCells[i].gameObject then
                if CPlayerInfoMgr.PlayerInfo ~= nil and CommonDefs.DictContains(CPlayerInfoMgr.PlayerInfo.pos2equipDict, typeof(UInt32), this.itemCells[i].position) then
                    local equipment = CommonDefs.DictGetValue(CPlayerInfoMgr.PlayerInfo.pos2equipDict, typeof(UInt32), this.itemCells[i].position)
                    if equipment ~= nil then
                        local common = CreateFromClass(CCommonItem, equipment)
                        CItemInfoMgr.ShowOtherPlayerItemInfo(common, CPlayerInfoMgr.PlayerInfo, false, nil, AlignType.Default, 0, 0, 0, 0)
                    end
                    -- CItemInfoMgr.ShowLinkItemInfo(equipment.Id);
                end
                break
            end
            i = i + 1
        end
    end
end
