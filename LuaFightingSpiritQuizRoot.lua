local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local UITable = import "UITable"
local Random = import "UnityEngine.Random"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaFightingSpiritQuizRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritQuizRoot, "ChooseView", "ChooseView", QnTableView)
RegistChildComponent(LuaFightingSpiritQuizRoot, "QuestionLabel", "QuestionLabel", UILabel)
RegistChildComponent(LuaFightingSpiritQuizRoot, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaFightingSpiritQuizRoot, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaFightingSpiritQuizRoot, "ChooseTable", "ChooseTable", UITable)
RegistChildComponent(LuaFightingSpiritQuizRoot, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaFightingSpiritQuizRoot, "CountdownTimeLabel", "CountdownTimeLabel", UILabel)
RegistChildComponent(LuaFightingSpiritQuizRoot, "CountdownDescLabel", "CountdownDescLabel", UILabel)
RegistChildComponent(LuaFightingSpiritQuizRoot, "MoneyCountLabel", "MoneyCountLabel", UILabel)
RegistChildComponent(LuaFightingSpiritQuizRoot, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaFightingSpiritQuizRoot, "ShareButton", "ShareButton", GameObject)

--@endregion RegistChildComponent end

function LuaFightingSpiritQuizRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritQuizRoot:InitRoot()
    -- 奖池数据默认为0
    if self.m_Silver == nil then
        self.m_Silver = 0
    end

    self.MoneyCountLabel.text = self.m_Silver
    Gac2Gas.OpenDouHunWenDaWnd()

    -- DouHun_WenDa_Wnd_Desc
    -- 每日开放1题，答对可获得银两和经验奖励。
    -- 计算当前开启的数量
    local firstData = DouHunCross_WenDa.GetData(1)
    local date = firstData.Date

    local now = CServerTimeMgr.Inst:GetZone8Time()

    local year = math.floor(date/10000)
    local month = math.floor(math.floor(date%10000)/100)
    local day = math.floor(date%100)
    local startdate = CreateFromClass(DateTime, year, month, day, 0, 0, 0)


    -- 总的数量
    self.m_QuizTotalCount = DouHunCross_WenDa.GetDataCount()

    -- 已公布
    self.m_QuizCount = math.min(CServerTimeMgr.Inst:DayDiff(now, startdate) + 1, self.m_QuizTotalCount)

    -- 初始化随机数据
    self:InitQuizData()

    -- 初始化倒计时
    local hasStart, startDiff = CLuaFightingSpiritMgr:HasGambleOpen() 
    local hasEnd, endDiff = CLuaFightingSpiritMgr:HasGambleFinish()

    local timeDiff = nil
    if not hasStart then
        -- 还没开始
        self.CountdownDescLabel.text = LocalString.GetString("距竞猜开始")
        timeDiff = startDiff
    elseif not hasEnd then
        self.CountdownDescLabel.text = LocalString.GetString("距竞猜结束")
        timeDiff = endDiff
    else
        self.CountdownDescLabel.text = LocalString.GetString("竞猜已结束")
        self.CountdownTimeLabel.text = ""
    end

    if timeDiff then
        if timeDiff.Days >0 then
            self.CountdownTimeLabel.text = SafeStringFormat3(LocalString.GetString("%s天"), timeDiff.Days)
        elseif timeDiff.Hours > 0 then
            self.CountdownTimeLabel.text = SafeStringFormat3(LocalString.GetString("%s时"), timeDiff.Hours)
        else
            self.CountdownTimeLabel.text = SafeStringFormat3(LocalString.GetString("%s分"), timeDiff.Minutes)
        end
    end

    for i=1, 4 do
        local item = self.ChooseTable.transform:Find(tostring(i))

        local normal = item:Find("Normal").gameObject
        local highLight = item:Find("HighLight").gameObject
        local right = item:Find("Right").gameObject
        local wrong = item:Find("Wrong").gameObject
        
        normal:SetActive(false)
        highLight:SetActive(false)
        right:SetActive(false)
        wrong:SetActive(false)
    end
end

function LuaFightingSpiritQuizRoot:InitQuizData()
    -- 先生成好 4*3*2*1=24种可能顺序
    local randomList = {}
    for i=1, 4 do
        for j = 1,4 do
            if j ~= i then
                for k = 1, 4 do
                    if k ~=j and k~=i then
                        for m = 1, 4 do
                            if m~=i and m~=j and m~=k then
                                local list = {}
                                table.insert(list, i)
                                table.insert(list, j)
                                table.insert(list, k)
                                table.insert(list, m)

                                table.insert(randomList, list)
                            end
                        end
                    end
                end
            end
        end
    end

    self.m_QuizRandomList = {}
    -- 设置随机数种子
    local simpleRandom = math.floor(((CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0) % 23) + 1)

    for i=1, self.m_QuizCount do
        local quiz = {}
        local index = math.floor(math.floor(simpleRandom + i*i*2) % 23) + 1
        
        -- 1对应了random[1]的答案
        local randomInfo = randomList[index]
        
        for j =1, 4 do
            if randomInfo[j] == 1 then
                quiz.rightAnswer = j
            end
        end
        quiz.randomList = randomInfo

        table.insert(self.m_QuizRandomList, quiz)
    end
end

function LuaFightingSpiritQuizRoot:InitAnswer(quizIndex)
    local data = DouHunCross_WenDa.GetData(quizIndex)
    local mapList = {data.answer, data.wronganswer1, data.wronganswer2, data.wronganswer3, data.explain}


    self.QuestionLabel.text = data.content
    if not CLuaFightingSpiritMgr:IsMyServerGuanwang() then
        self.QuestionLabel.text = data.Qcontent
        mapList = {data.Qanswer, data.Qwronganswer1, data.Qwronganswer2, data.Qwronganswer3, data.Qexplain}
    end
    self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("题%s"), tostring(quizIndex))

    -- 随机到的可能性
    local quizRandom = self.m_QuizRandomList[quizIndex]
    local rightIndex = quizRandom.rightAnswer

    local hasAnswer = false
    if self.m_ChoiceInfo[quizIndex] then
        hasAnswer = true
    end
    local choose = self.m_ChoiceInfo[quizIndex] 

    -- 是否回答
    if hasAnswer then
        self.DescLabel.text = mapList[5]
        self.DescLabel.gameObject:SetActive(true)
        self.ShareButton:SetActive(false)
    else
        self.DescLabel.gameObject:SetActive(false)
        self.ShareButton:SetActive(true)
        UIEventListener.Get(self.ShareButton).onClick = DelegateFactory.VoidDelegate(function()
            self:ShowSharePopupMenu(quizIndex)
        end)
    end

    for i=1, 4 do
        local index = quizRandom.randomList[i]
        local content = mapList[index]

        local item = self.ChooseTable.transform:Find(tostring(i))
        local contentLabel = item:Find("ContentLabel"):GetComponent(typeof(UILabel))
        
        contentLabel.text = content

        local normal = item:Find("Normal").gameObject
        local highLight = item:Find("HighLight").gameObject
        local right = item:Find("Right").gameObject
        local wrong = item:Find("Wrong").gameObject

        normal:SetActive(false)
        highLight:SetActive(false)
        right:SetActive(false)
        wrong:SetActive(false)

        if hasAnswer then
            if i == rightIndex then
                right:SetActive(true)
            end

            if i == choose then
                highLight:SetActive(true)
                if i ~= rightIndex then
                    wrong:SetActive(true)
                end
            else
                normal:SetActive(true)
            end
        else
            normal:SetActive(true)
        end

        if not hasAnswer then
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                Gac2Gas.SendDouHunWenDaResult(quizIndex, i == rightIndex, i)
                self.m_ChoiceInfo[quizIndex] = i
                self.m_CurrentIndex = quizIndex
                self:RefreshShow()
            end)
        else
            UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            end)
        end
    end
end

function LuaFightingSpiritQuizRoot:OnData(data)
    -- {silver, choiceInfo}
    self.m_Silver = data[1]
    self.m_ChoiceInfo = data[2]

    -- 奖池
    self.MoneyCountLabel.text = self.m_Silver

    -- 初始化按钮
    self.ChooseView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return self.m_QuizCount
    end, function(item, index)
        self:InitChooseItem(item, index + 1)
    end)

    -- 设置点击事件
    self.ChooseView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 设置一个问答
        if row +1 > self.m_QuizCount then
            g_MessageMgr:ShowMessage("DouHun_WenDa_Question_Not_Open")
        elseif row ~= 0 and not self.m_ChoiceInfo[row] then
            self.ChooseView:SetSelectRow(self.m_CurrentIndex-1, true)
            g_MessageMgr:ShowMessage("DouHun_WenDa_Question_Cant_Answer") 
        else
            self:InitAnswer(row+1)
        end
    end)

    if not self.m_HasInit then
        self.m_CurrentIndex = self.m_QuizCount
        for i=1, self.m_QuizCount do
            if not self.m_ChoiceInfo[i] then
                self.m_CurrentIndex = i
                self:RefreshShow()
                self.m_HasInit = true
                return
            end
        end
    end

    self:RefreshShow()
    self.m_HasInit = true
end

function LuaFightingSpiritQuizRoot:RefreshShow()
    self.ChooseView:ReloadData(true,false)

    self.ChooseView:SetSelectRow(self.m_CurrentIndex-1, true)

    if self.m_QuizCount > self.m_CurrentIndex then
        local item = self.ChooseView:GetItemAtRow(self.m_CurrentIndex)
        local scrollView = self.ChooseView.transform:Find("ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
        if item then
            CUICommonDef.SetFullyVisible(item.gameObject, scrollView)
        end
    end

    local hasAnswerCount = 0
    local rightCount = 0
    for i=1, self.m_QuizCount do
        local quizRandom = self.m_QuizRandomList[i]
        local right = quizRandom.rightAnswer
        
        if self.m_ChoiceInfo[i] then
            hasAnswerCount = hasAnswerCount + 1

            if self.m_ChoiceInfo[i] == right then
                rightCount = rightCount + 1
            end
        end
    end

    self.BottomLabel.text = SafeStringFormat3(LocalString.GetString("已公布%s题 作答%s题 正确%s题"), self.m_QuizCount, hasAnswerCount, rightCount)
end

-- index已经从1开始了
function LuaFightingSpiritQuizRoot:InitChooseItem(item, index)
    local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
    local mark = item.transform:Find("Mark"):GetComponent(typeof(CUITexture))

    label.text = SafeStringFormat3(LocalString.GetString("题%s"), tostring(index))

    if self.m_ChoiceInfo[index] then
        -- 已做答
        mark:LoadMaterial("UI/Texture/Transparent/Material/bagualuwnd_button_shanggua.mat")
    else
        mark:LoadMaterial("UI/Texture/Transparent/Material/bagualuwnd_button_shanggua_none.mat")
    end
end

function LuaFightingSpiritQuizRoot:OnEnable()
    self.m_HasInit = false
    self:InitRoot()
    g_ScriptEvent:AddListener("FightingSpiritWenDa", self, "OnData")
end

function LuaFightingSpiritQuizRoot:OnDisable()
    g_ScriptEvent:RemoveListener("FightingSpiritWenDa", self, "OnData")
end

function LuaFightingSpiritQuizRoot:ShowSharePopupMenu(quizIndex)
    local popupList={}

    table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至世界"), DelegateFactory.Action_int(function(index) 
        self:ShareQuestion(quizIndex, EChatPanel.World)
    end),false, nil, EnumPopupMenuItemStyle.Default))

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至帮会"), DelegateFactory.Action_int(function(index) 
            self:ShareQuestion(quizIndex, EChatPanel.Guild)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    if CTeamMgr.Inst:TeamExists() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至队伍"), DelegateFactory.Action_int(function(index) 
            self:ShareQuestion(quizIndex, EChatPanel.Team)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end

    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right

    CPopupMenuInfoMgr.ShowPopupMenu(array, self.ShareButton.transform, side, 1, nil, 600, true, 220)
end

function LuaFightingSpiritQuizRoot:ShareQuestion(quizIndex, channel)
    local optionLabelArray = {"A.","B.","C.","D."}
    local optionText = ""

    -- 数据
    local data = DouHunCross_WenDa.GetData(quizIndex)
    local mapList = {data.answer, data.wronganswer1, data.wronganswer2, data.wronganswer3, data.explain}
    if not CLuaFightingSpiritMgr:IsMyServerGuanwang() then
        mapList = {data.Qanswer, data.Qwronganswer1, data.Qwronganswer2, data.Qwronganswer3, data.Qexplain}
    end
    local quizRandom = self.m_QuizRandomList[quizIndex]


    for i=1, 4 do
        local index = quizRandom.randomList[i]
        local content = mapList[index]
        optionText = SafeStringFormat3("%s%s%s ",optionText, optionLabelArray[i], content)
    end

    local link = g_MessageMgr:FormatMessage("KEJU_XIANGSHI_SHARE_QUESTION", data.content, optionText)
    if not CLuaFightingSpiritMgr:IsMyServerGuanwang() then
        link = g_MessageMgr:FormatMessage("KEJU_XIANGSHI_SHARE_QUESTION", data.Qcontent, optionText)
    end
    CChatHelper.SendMsgWithFilterOption(channel, link, 0, true)
    CSocialWndMgr.ShowChatWnd()
end


--@region UIEvent

function LuaFightingSpiritQuizRoot:OnTipBtnClick()
    g_MessageMgr:ShowMessage("DouHun_WenDa_Wnd_Desc")
end


--@endregion UIEvent

