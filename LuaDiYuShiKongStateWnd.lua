local DelegateFactory = import "DelegateFactory"
local Color = import "UnityEngine.Color"
local UISlider = import "UISlider"
local UILabel = import "UILabel"

CLuaDiYuShiKongStateWnd = class()

RegistClassMember(CLuaDiYuShiKongStateWnd, "m_RongXinSlider")
RegistClassMember(CLuaDiYuShiKongStateWnd, "m_FaQiSlider")
RegistClassMember(CLuaDiYuShiKongStateWnd, "m_XiaoGuiEscapeNum")
RegistClassMember(CLuaDiYuShiKongStateWnd, "m_ExpandBtn")

function CLuaDiYuShiKongStateWnd:Awake()
    self.m_ExpandBtn        = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    UIEventListener.Get(self.m_ExpandBtn).onClick = DelegateFactory.VoidDelegate( function(p) 
        self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    self:InitComponents()

end

function CLuaDiYuShiKongStateWnd:Init()
    self.m_XiaoGuiEscapeNum.text = "0"
end

function CLuaDiYuShiKongStateWnd:InitComponents()
    self.m_RongXinSlider    = self.transform:Find("Anchor/Top/Line1/Slider1"):GetComponent(typeof(UISlider))
    self.m_FaQiSlider       = self.transform:Find("Anchor/Top/Line2/Slider2"):GetComponent(typeof(UISlider))
    self.m_XiaoGuiEscapeNum = self.transform:Find("Anchor/Top/Line3/LabelNum"):GetComponent(typeof(UILabel))
end

function CLuaDiYuShiKongStateWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDiYuShiKongCopyInfo", self, "RefreshState")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function CLuaDiYuShiKongStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDiYuShiKongCopyInfo", self, "RefreshState")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIManager.TopAndRightTipWnd)
    end
end

function CLuaDiYuShiKongStateWnd:OnHideTopAndRightTipWnd()
    self.m_ExpandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end

function CLuaDiYuShiKongStateWnd:RefreshState(rongxinHp, rongxinHpFull, faqiHp, faqiHpFull, escapedXiaoGuiCount)
    if escapedXiaoGuiCount < 15 then
        self.m_XiaoGuiEscapeNum.color = Color.green
    end if escapedXiaoGuiCount < 20 then
        self.m_XiaoGuiEscapeNum.color = Color.yellow
    else
        self.m_XiaoGuiEscapeNum.color = Color.red
    end

    self.m_XiaoGuiEscapeNum.text = escapedXiaoGuiCount

    self.m_RongXinSlider.value = rongxinHpFull == 0 and 0 or rongxinHp / rongxinHpFull

    self.m_FaQiSlider.value  = faqiHpFull == 0 and 0 or faqiHp / faqiHpFull
end
