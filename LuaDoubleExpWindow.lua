local UInt32 = import "System.UInt32"
local UIEventListener = import "UIEventListener"
local String = import "System.String"
local Object = import "System.Object"
local MsgPackImpl = import "MsgPackImpl"
local Gac2Gas = import "L10.Game.Gac2Gas"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UITable = import "UITable"
local QnCheckBox = import "L10.UI.QnCheckBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CItemMgr = import "L10.Game.CItemMgr"

CLuaDoubleExpWindow = class()
RegistChildComponent(CLuaDoubleExpWindow,"doubleExp", GameObject)
RegistChildComponent(CLuaDoubleExpWindow,"multiExp", GameObject)
RegistChildComponent(CLuaDoubleExpWindow,"tipsButton", GameObject)
RegistChildComponent(CLuaDoubleExpWindow,"settingBtn", GameObject)
RegistChildComponent(CLuaDoubleExpWindow,"settingPanel", GameObject)
RegistChildComponent(CLuaDoubleExpWindow,"Table", UITable)
RegistChildComponent(CLuaDoubleExpWindow,"CheckBox1", QnCheckBox)
RegistChildComponent(CLuaDoubleExpWindow,"CheckBox2", QnCheckBox)

RegistClassMember(CLuaDoubleExpWindow, "DoubleBtn")
RegistClassMember(CLuaDoubleExpWindow, "TripleBtn")
RegistClassMember(CLuaDoubleExpWindow, "DoubleWnd")
RegistClassMember(CLuaDoubleExpWindow, "TripleWnd")

RegistClassMember(CLuaDoubleExpWindow, "CurBtn")

function CLuaDoubleExpWindow:GetTripleItemCount()
  local data = GameSetting_Client.GetData()
  local default = data.TrebleExperienceItemId
  local templateIds = default
  local totalCount = 0
  do
    local i = 0
    while i < templateIds.Length do
      totalCount = totalCount + CItemMgr.Inst:GetItemCount(templateIds[i])
      i = i + 1
    end
  end
  return totalCount
end
-- Auto Generated!!
function CLuaDoubleExpWindow:OnEnable( )
    UIEventListener.Get(self.tipsButton).onClick =  DelegateFactory.VoidDelegate(function(go) self:OnTipButtonClick(go) end)
    UIEventListener.Get(self.settingBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:InitSettingPanel(go) end)

    if CClientMainPlayer.Inst == nil then
        return
    end
    self.multiExp:SetActive(CClientMainPlayer.Inst.HasFeiSheng)
    self.doubleExp:SetActive(not CClientMainPlayer.Inst.HasFeiSheng)
    self.CheckBox1.OnValueChanged = DelegateFactory.Action_bool(function(check) self:OnUseTripleChanged(check) end)
    self.CheckBox2.OnValueChanged = DelegateFactory.Action_bool(function(check) self:OnUseDoubleChanged(check) end)
    --Gac2Gas.RequestMultipleExpInfo()

    self.DoubleBtn = self.transform:Find("Anchor/multiExp/DoubleBtn")
    self.TripleBtn = self.transform:Find("Anchor/multiExp/TripleBtn")
    self.DoubleWnd = self.transform:Find("Anchor/multiExp/Double")
    self.TripleWnd = self.transform:Find("Anchor/multiExp/Triple")

    if self.DoubleBtn then
        self.DoubleBtn:GetComponent(typeof(QnSelectableButton)).OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnTabClicked(btn.transform) end)
    end

    if self.TripleBtn then
        self.TripleBtn:GetComponent(typeof(QnSelectableButton)).OnClick = DelegateFactory.Action_QnButton(function(btn) self:OnTabClicked(btn.transform) end)
    end

    local getFreeDoubleExp = CPropertyItem.IsSamePeriod(CSigninMgr.Inst.doubleExpReceiveTime, "w")
    local tripleItemTId = MultipleExp_GameSetting.GetData().Triple_Item_TemplateId
    local haveTripleExpCount = self:GetTripleItemCount()

    if self.CurBtn then
        self:OnTabClicked(self.CurBtn)
    else
        local tripleBuff = CSigninMgr.Inst:IsPlayerUsingMultipleExp(MultipleExp_GameSetting.GetData().Triple_Show_BuffId)
        if not getFreeDoubleExp then
          if self.DoubleBtn then
            self:OnTabClicked(self.DoubleBtn)
          end
        elseif tripleBuff then
          if self.TripleBtn then
            self:OnTabClicked(self.TripleBtn)
          end
        else
          if self.DoubleBtn then
            self:OnTabClicked(self.DoubleBtn)
          end
        end
    end
end


function CLuaDoubleExpWindow:OnTabClicked(go)
    self.CurBtn = go

    local isDouble = go.gameObject == self.DoubleBtn.gameObject
    local isTriple = go.gameObject == self.TripleBtn.gameObject
    self.DoubleWnd.gameObject:SetActive(isDouble)
    self.TripleWnd.gameObject:SetActive(isTriple)

    self.DoubleBtn:GetComponent(typeof(QnSelectableButton)):SetSelected(isDouble, false)
    self.TripleBtn:GetComponent(typeof(QnSelectableButton)):SetSelected(isTriple, false)
end


function CLuaDoubleExpWindow:OnTipButtonClick(go)
    g_MessageMgr:ShowMessage("DoubleExp_Tip")
end

function CLuaDoubleExpWindow:OnUseTripleChanged(check)
    local arg = check and "1" or "0"
    Gac2Gas.SetGameSettingInfo(EnumGameSetting_lua.eTripleExpAutoUseForYiTiaoLong, arg)
    if check then
        self.CheckBox2:SetSelected(not check, false)
    end
end

function CLuaDoubleExpWindow:OnUseDoubleChanged(check)
    local arg = check and "1" or "0"
    Gac2Gas.SetGameSettingInfo(EnumGameSetting_lua.eMultipleExpAutoUseForYiTiaoLong, arg)
    if check then
        self.CheckBox1:SetSelected(not check, false)
    end
end


function CLuaDoubleExpWindow:InitSettingPanel( go)
    self.settingPanel:SetActive(true)

    local backBtn = self.settingPanel.transform:Find("darkbg").gameObject
    UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self.settingPanel:SetActive(false)
    end)

    self.CheckBox1.Visible = CClientMainPlayer.Inst.HasFeiSheng
    self.CheckBox2.Visible = true
    self.Table:Reposition()

    local gameSetting = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eCommonGameSetting)
    local settingDic = nil 
    if gameSetting then
        settingDic = TypeAs(MsgPackImpl.unpack(gameSetting.Data), typeof(MakeGenericClass(Dictionary, String, Object)))
    end

    --双倍
    local skey = tostring((EnumGameSetting_lua.eMultipleExpAutoUseForYiTiaoLong))
    if settingDic and CommonDefs.DictContains(settingDic, typeof(String), skey) then
        local key = CommonDefs.DictGetValue(settingDic, typeof(String), skey)
        local value =  (System.Int32.Parse(ToStringWrap(key)) == 1)
        self.CheckBox2:SetSelected(value, true)
        if value then
            self.CheckBox1:SetSelected(false, true)
        end
    else
        self.CheckBox2:SetSelected(false, true)
    end

    --三倍
    skey = tostring((EnumGameSetting_lua.eTripleExpAutoUseForYiTiaoLong))
    if settingDic and CommonDefs.DictContains(settingDic, typeof(String), skey) then
        local key = CommonDefs.DictGetValue(settingDic, typeof(String), skey)
        local value = (System.Int32.Parse(ToStringWrap(key)) == 1)
        self.CheckBox1:SetSelected(value, true)
        if value then
            self.CheckBox2:SetSelected(false, true)
        end
    else
        self.CheckBox1:SetSelected(false, true)
    end
end
