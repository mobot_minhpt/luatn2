require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIRes = import "L10.UI.CUIResources"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIManager = import "L10.UI.CUIManager"

CLuaKitePlayOpenWnd=class()
RegistClassMember(CLuaKitePlayOpenWnd,"CloseBtn")
RegistClassMember(CLuaKitePlayOpenWnd,"Choose1")
RegistClassMember(CLuaKitePlayOpenWnd,"Choose2")
RegistClassMember(CLuaKitePlayOpenWnd,"Choose3")
RegistClassMember(CLuaKitePlayOpenWnd,"EnterBtn")
RegistClassMember(CLuaKitePlayOpenWnd,"TemplateNode")
RegistClassMember(CLuaKitePlayOpenWnd,"ScrollViewNode")
RegistClassMember(CLuaKitePlayOpenWnd,"TableNode")

local nowChooseKiteTypeNode = nil
local nowChooseType = 0

function CLuaKitePlayOpenWnd:Init()
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.KitePlayOpenWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.TemplateNode:SetActive(false)
	self.Choose1.transform:Find("sign").gameObject:SetActive(false)
	self.Choose2.transform:Find("sign").gameObject:SetActive(false)
	self.Choose3.transform:Find("sign").gameObject:SetActive(false)

	local onClickChoose1 = function(go)
		LuaKitePlayMgr.ChooseKiteType = 1
		if nowChooseKiteTypeNode then
			nowChooseKiteTypeNode:SetActive(false)
		end
		nowChooseKiteTypeNode = go.transform:Find("sign").gameObject
		nowChooseKiteTypeNode:SetActive(true)
	end
	CommonDefs.AddOnClickListener(self.Choose1,DelegateFactory.Action_GameObject(onClickChoose1),false)
	local onClickChoose2 = function(go)
		LuaKitePlayMgr.ChooseKiteType = 2
		if nowChooseKiteTypeNode then
			nowChooseKiteTypeNode:SetActive(false)
		end
		nowChooseKiteTypeNode = go.transform:Find("sign").gameObject
		nowChooseKiteTypeNode:SetActive(true)
	end
	CommonDefs.AddOnClickListener(self.Choose2,DelegateFactory.Action_GameObject(onClickChoose2),false)
	local onClickChoose3 = function(go)
		LuaKitePlayMgr.ChooseKiteType = 3
		if nowChooseKiteTypeNode then
			nowChooseKiteTypeNode:SetActive(false)
		end
		nowChooseKiteTypeNode = go.transform:Find("sign").gameObject
		nowChooseKiteTypeNode:SetActive(true)
	end
	CommonDefs.AddOnClickListener(self.Choose3,DelegateFactory.Action_GameObject(onClickChoose3),false)
	local onEnterGame = function(go)
		if not CClientMainPlayer.Inst then
			return
		end
		Gac2Gas.RequestJoinZhiYuan(LuaKitePlayMgr.ChooseNPCEngineId, CClientMainPlayer.Inst.WorldPos.y,LuaKitePlayMgr.ChooseKiteType)
		CUIManager.CloseUI(CUIRes.KitePlayOpenWnd)
	end
	CommonDefs.AddOnClickListener(self.EnterBtn,DelegateFactory.Action_GameObject(onEnterGame),false)
end

return CLuaKitePlayOpenWnd
