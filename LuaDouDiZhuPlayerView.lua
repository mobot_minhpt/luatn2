local Object = import "System.Object"
local CUIFx=import "L10.UI.CUIFx"
local SoundManager=import "SoundManager"
local UISoundEvents=import "SoundManager+UISoundEvents"
local Vector3 = import "UnityEngine.Vector3"
local Ease=import "DG.Tweening.Ease"
local CSpeakingBubble=import "L10.UI.CSpeakingBubble"
local EnumSpeakingBubbleType=import "L10.Game.EnumSpeakingBubbleType"
local UISlider=import "UISlider"

local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"


CLuaDouDiZhuPlayerView=class()
RegistClassMember(CLuaDouDiZhuPlayerView,"m_Transform")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_NameLabel")

RegistClassMember(CLuaDouDiZhuPlayerView,"m_PortraitTexture")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_ScoreLabel")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_ViewIndex")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_Index")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_HandCardsTf")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_HandCards2")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_ShowCardsTf")

RegistClassMember(CLuaDouDiZhuPlayerView,"m_CardNumLabel")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_ReadyMark")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_DiZhuMark")
-- RegistClassMember(CLuaDouDiZhuPlayerView,"m_DiZhuMark2")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_TipLabel")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_StatusLabel")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_MsgLabel")

RegistClassMember(CLuaDouDiZhuPlayerView,"m_Tick")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_LeftTime")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_CountdownLabel")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_Slider")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_SliderTweener")

 RegistClassMember(CLuaDouDiZhuPlayerView,"m_SpeakingBubble")

--viewIndex==1
RegistClassMember(CLuaDouDiZhuPlayerView,"m_Button1")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_Button2")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_Button3")

RegistClassMember(CLuaDouDiZhuPlayerView,"m_CancleTuoGuanButton")

RegistClassMember(CLuaDouDiZhuPlayerView,"m_UIFx")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_JinbiFx")

RegistClassMember(CLuaDouDiZhuPlayerView,"m_NoticeLabel")

RegistClassMember(CLuaDouDiZhuPlayerView,"m_PlayerNode")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_NoPlayerNode")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_AddFriendButton")
RegistClassMember(CLuaDouDiZhuPlayerView,"m_InviteListButton")


function CLuaDouDiZhuPlayerView:ShowInvitePopupMenu(go)
    local popupList={}
    local data = PopupMenuItemData(LocalString.GetString("邀请好友"), DelegateFactory.Action_int(function(index) 
        CLuaRemoteDouDiZhuInviteWnd.s_Index = CLuaDouDiZhuMgr.SwitchChairId(self.m_ViewIndex)
        -- print(self.m_ViewIndex,CLuaRemoteDouDiZhuInviteWnd.s_Index)
        CUIManager.ShowUI(CLuaUIResources.RemoteDouDiZhuInviteWnd)
    end),false, nil, EnumPopupMenuItemStyle.Default)
    table.insert( popupList,data )
    local data2 = PopupMenuItemData(LocalString.GetString("世界频道邀请"), DelegateFactory.Action_int(function(index) 
        Gac2Gas.SendRemoteDouDiZhuLinkMessage(1)
    end),false, nil, EnumPopupMenuItemStyle.Default)
    table.insert( popupList,data2 )
    local data3 = PopupMenuItemData(LocalString.GetString("帮会频道邀请"), DelegateFactory.Action_int(function(index) 
        Gac2Gas.SendRemoteDouDiZhuLinkMessage(2)
    end),false, nil, EnumPopupMenuItemStyle.Default)
    table.insert( popupList,data3 )

    local data4 = PopupMenuItemData(LocalString.GetString("队伍频道邀请"), DelegateFactory.Action_int(function(index) 
        Gac2Gas.SendRemoteDouDiZhuLinkMessage(3)
    end),false, nil, EnumPopupMenuItemStyle.Default)
    table.insert( popupList,data4 )

    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right
    if self.m_ViewIndex==2 then
        side = CPopupMenuInfoMgr.AlignType.Left
    end
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, side, 1, nil, 600, true, 296)
end
function CLuaDouDiZhuPlayerView:ShowKickPopupMenu(go)
    local popupList={}
    local data = PopupMenuItemData(LocalString.GetString("踢出玩家"), DelegateFactory.Action_int(function(index) 
        local msg=g_MessageMgr:FormatMessage("RemoteDouDiZhu_Kickout_Confirm",{})
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(self.m_Index)
            if info then
                Gac2Gas.KickPlayerRemoteDouDiZhu(info.id)
            end
        end), nil, nil, nil, false)

    end),false, nil, EnumPopupMenuItemStyle.Default)
    table.insert( popupList,data )

    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right
    if self.m_ViewIndex==2 then
        side = CPopupMenuInfoMgr.AlignType.Left
    end
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, side, 1, nil, 600, true, 296)
end

function CLuaDouDiZhuPlayerView:Init(tf,viewIndex)
    self.m_Transform=tf
    self.m_ViewIndex=viewIndex

    local isOwn = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id==CLuaDouDiZhuMgr.m_OwnerId) or false

    if self.m_ViewIndex==1 then
    else
        if CLuaDouDiZhuMgr.m_IsRemote then
            self.m_PlayerNode = tf:Find("Player")
            self.m_NoPlayerNode = tf:Find("NoPlayer")
            if self.m_NoPlayerNode then
                self.m_AddFriendButton = self.m_NoPlayerNode:Find("InviteButton").gameObject
                if isOwn then
                    UIEventListener.Get(self.m_AddFriendButton).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:ShowInvitePopupMenu(go)
                    end)
                end
                self.m_InviteListButton = self.m_NoPlayerNode:Find("ApplyListButton").gameObject
                if isOwn then
                    UIEventListener.Get(self.m_InviteListButton).onClick = DelegateFactory.VoidDelegate(function(go)
                        CUIManager.ShowUI(CLuaUIResources.RemoteDouDiZhuApplyListWnd)
                    end)
                end
            end
            if self.m_PlayerNode then
                local kickButton = self.m_PlayerNode:Find("KickButton").gameObject
                if isOwn then
                    UIEventListener.Get(kickButton).onClick = DelegateFactory.VoidDelegate(function(go)
                        self:ShowKickPopupMenu(go)
                    end)
                end
            end
        end
    end


    -- self.m_HandCardsTbl={}

    self.m_LeftTime=0

    self.m_HandCardsTf=FindChild(tf,"HandCards")
    self.m_ShowCardsTf=FindChild(tf,"ShowCards")

    local fx = FindChild(tf,"Fx")
    if fx then
        self.m_UIFx=fx:GetComponent(typeof(CUIFx))
    end
    local jinbifx = FindChild(tf,"JinbiFx")
    if jinbifx then
        self.m_JinbiFx=jinbifx:GetComponent(typeof(CUIFx))
    end


    if self.m_ViewIndex==1 then
        CLuaDouDiZhuCardMgr.m_ShowCardsTf=self.m_ShowCardsTf
        CLuaDouDiZhuCardMgr.m_HandCardsTf=self.m_HandCardsTf
    end


    local cardNum=FindChild(tf,"CardNumLabel")
    if cardNum then
        self.m_CardNumLabel=cardNum:GetComponent(typeof(UILabel))
        self.m_CardNumLabel.text="0"
    end
    local handCards2=FindChild(tf,"HandCards2")
    if handCards2 then
        self.m_HandCards2=handCards2.gameObject
        self.m_HandCards2:SetActive(false)
    end
    -- self.m_PortraitTexture
    self.m_PortraitTexture=FindChild(tf,"PortraitTexture"):GetComponent(typeof(CUITexture))
    self.m_PortraitTexture:Clear()

    -- self.m_CardTemplate=FindChild(tf,"CardTemplate").gameObject
    -- self.m_CardTemplate:SetActive(false)

    local scorelabel =FindChild(tf,"ScoreLabel")
    if scorelabel then 
        self.m_ScoreLabel=FindChild(tf,"ScoreLabel"):GetComponent(typeof(UILabel))
        self.m_ScoreLabel.text=nil
    end

    local name=FindChild(tf,"NameLabel")
    if name then
        self.m_NameLabel=name:GetComponent(typeof(UILabel))
        self.m_NameLabel.text=nil
    end

    local readymark =FindChild(tf,"ReadyMark")
    if readymark then 
        self.m_ReadyMark=readymark.gameObject
        self.m_ReadyMark:SetActive(false)
    end
    self.m_DiZhuMark=FindChild(tf,"DiZhuMark").gameObject
    self.m_DiZhuMark:SetActive(false)

    if viewIndex==1 then
        local btn = FindChild(tf,"Button1")
        if btn then self.m_Button1=btn.gameObject self.m_Button1:SetActive(false) end
        btn = FindChild(tf,"Button2")
        if btn then self.m_Button2=btn.gameObject self.m_Button2:SetActive(false) end
        btn = FindChild(tf,"Button3")
        if btn then self.m_Button3=btn.gameObject self.m_Button3:SetActive(false) end
        -- self.m_Button2=FindChild(tf,"Button2").gameObject
        -- self.m_Button3=FindChild(tf,"Button3").gameObject
        -- self.m_Button1:SetActive(false)
        -- self.m_Button2:SetActive(false)
        -- self.m_Button3:SetActive(false)
        local label = FindChild(tf,"TipLabel")
        if label then self.m_TipLabel=label:GetComponent(typeof(UILabel)) self.m_TipLabel.text=nil end
        -- self.m_TipLabel=FindChild(tf,"TipLabel"):GetComponent(typeof(UILabel))
        -- self.m_TipLabel.text=nil
        label = FindChild(tf,"NoticeLabel")
        if label then self.m_NoticeLabel=label:GetComponent(typeof(UILabel)) self.m_NoticeLabel.text=nil end
        -- self.m_NoticeLabel=FindChild(tf,"NoticeLabel"):GetComponent(typeof(UILabel))
        -- self.m_NoticeLabel.text=nil

        btn = FindChild(tf,"CancleTuoGuanButton")
        if btn then 
            self.m_CancleTuoGuanButton=btn.gameObject 
            self.m_CancleTuoGuanButton:SetActive(false) 
            UIEventListener.Get(self.m_CancleTuoGuanButton).onClick=LuaUtils.VoidDelegate(function(go)
                Gac2Gas.RequestDouDiZhuTuoGuan(false)
            end)
        end

        -- self.m_CancleTuoGuanButton=FindChild(tf,"CancleTuoGuanButton").gameObject
        -- self.m_CancleTuoGuanButton:SetActive(false)

        local node = FindChild(tf,"EmptyRegion")
        if node then
            local emptyRegion=node.gameObject
            UIEventListener.Get(emptyRegion).onClick=LuaUtils.VoidDelegate(function(go)
                for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
                    if v then
                        k:TakeBack()
                    end
                end
            end)
        end
    else
        local node = FindChild(tf,"StatusLabel")
        if node then
            self.m_StatusLabel=node:GetComponent(typeof(UILabel))
            self.m_StatusLabel.text=nil
        end
    end

    local node = FindChild(tf,"MsgLabel")
    if node then
        self.m_MsgLabel=node:GetComponent(typeof(UILabel))
        self.m_MsgLabel.text=nil
    end

    local node = FindChild(tf,"CountdownLabel")
    if node then
        self.m_CountdownLabel=node:GetComponent(typeof(UILabel))
        self.m_CountdownLabel.text=nil
        self.m_CountdownLabel.gameObject:SetActive(false)
    end

    if self.m_ViewIndex==1 then
        local node = FindChild(tf,"Slider")
        if node then
            self.m_Slider=node:GetComponent(typeof(UISlider))
            self.m_Slider.gameObject:SetActive(false)
        end
    end


    if CLuaDouDiZhuMgr.m_IsRemote then
        if self.m_NoPlayerNode then self.m_NoPlayerNode.gameObject:SetActive(true) end
        if self.m_PlayerNode then self.m_PlayerNode.gameObject:SetActive(false) end
    else
        self.m_Transform.gameObject:SetActive(false)
    end
    self:OnApplyJoinRemoteDouDiZhuPlayerListAlert()

    local bubble = FindChild(self.m_Transform,"SpeakingBubble")
    if bubble then
        self.m_SpeakingBubble=bubble:GetComponent(typeof(CSpeakingBubble))
        self.m_SpeakingBubble.gameObject:SetActive(false)
    end
end

function CLuaDouDiZhuPlayerView:OnPlayerLeaveDouDiZhu()
    self:CancleTick()

    CUICommonDef.ClearTransform(self.m_HandCardsTf)
    CUICommonDef.ClearTransform(self.m_ShowCardsTf)

    self.m_PortraitTexture:Clear()

    if self.m_ScoreLabel then self.m_ScoreLabel.text=nil end
    
    if self.m_NameLabel then self.m_NameLabel.text=nil end

    if self.m_ReadyMark then self.m_ReadyMark:SetActive(false) end
    self.m_DiZhuMark:SetActive(false)

    if self.m_JinbiFx then self.m_JinbiFx:DestroyFx() end
    
    if self.m_ViewIndex==1 then
        if self.m_Button1 then self.m_Button1:SetActive(false) end
        if self.m_Button2 then self.m_Button2:SetActive(false) end
        if self.m_Button3 then self.m_Button3:SetActive(false) end

        if self.m_TipLabel then self.m_TipLabel.text=nil end
        if self.m_CancleTuoGuanButton then self.m_CancleTuoGuanButton:SetActive(false) end
        
    else
        if self.m_StatusLabel then self.m_StatusLabel.text=nil end

    end
    if self.m_MsgLabel then self.m_MsgLabel.text=nil end
    
    if CLuaDouDiZhuMgr.m_IsRemote then
        if self.m_NoPlayerNode then 
            self.m_NoPlayerNode.gameObject:SetActive(true) 
            local alert = self.m_InviteListButton.transform:Find("Alert").gameObject
            alert:SetActive(false)
        end
        if self.m_PlayerNode then self.m_PlayerNode.gameObject:SetActive(false) end
    else
        self.m_Transform.gameObject:SetActive(false)
    end

end

function CLuaDouDiZhuPlayerView:OnSyncDouDiZhuCardsInfo(info,bFaPai,bChuPai)
    if not info then return end
    self.m_Index=info.index
    --不一样，发送的角色没有明牌 且不是自己
    CUICommonDef.ClearTransform(self.m_HandCardsTf)
    CUICommonDef.ClearTransform(self.m_ShowCardsTf)

    --syncplayerinfo的时候 可能自己不是最后一个到，所以那个时候无法判断是不是allready
    if self.m_ViewIndex==1 then
        if CLuaDouDiZhuMgr.m_Play and CLuaDouDiZhuMgr.m_Play:IsAllReady() then
            if self.m_TipLabel then self.m_TipLabel.text=nil end
        end
    end

    if self.m_ViewIndex~=1 then
        --显示牌的数量
        if self.m_HandCards2 then
            if CLuaDouDiZhuMgr.m_Play and CLuaDouDiZhuMgr.m_Play:IsAllReady() then
                self.m_HandCards2:SetActive(true)
            else
                self.m_HandCards2:SetActive(false)
            end
        end
        if self.m_CardNumLabel then
            self.m_CardNumLabel.text=tostring(info.handCardsCount)
        end
    end

    --音效
    if self.m_ViewIndex==1 and bFaPai then
        SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.DouDiZhuFaPai, Vector3(0,0,0),nil,0)
    end
    
    if info.handCards then
        if #info.handCards~=info.handCardsCount then
            --判断一下是否是明牌
            
        else
            --清空
            -- self.m_HandCardsTbl={}
            local node=self.m_HandCardsTf.gameObject

            --反一下
            local tbl={}
            for i=#info.handCards,1,-1 do
                table.insert( tbl,info.handCards[i] )
            end

            local cardNum=#tbl

            CLuaDouDiZhuCardMgr.Init()

            for i,v in ipairs(tbl) do

                local card = NGUITools.AddChild(node,CLuaDouDiZhuMgr.m_CardTemplate)
                card:SetActive(true)

                if self.m_ViewIndex==1 then
                    local cardCmp=CLuaDouDiZhuCard:new()
                    cardCmp:Init(card.transform,v,i,cardNum,CLuaDouDiZhuMgr.IsCrossGM())
                    cardCmp:Register()
                    if bFaPai then
                        --做个展开动画
                        cardCmp:PlayFaPaiEffect()
                    end
                else
                    CLuaDouDiZhuMgr.SetCard(card.transform,v,i)
                end
            end
        end
    end

    local node=self.m_ShowCardsTf.gameObject

    local tbl={}
    for i=#info.showCards,1,-1 do
        table.insert( tbl,info.showCards[i] )
    end

    local cardNum=#tbl
    for i,v in ipairs(tbl) do
        local card = NGUITools.AddChild(node,CLuaDouDiZhuMgr.m_CardTemplate)
        card:SetActive(true)
        local tf=card.transform
        
        CLuaDouDiZhuMgr.SetCard(tf,v,i)
        local x,y=CLuaDouDiZhuCardMgr.GetCardLocalPosition(tf,self.m_ViewIndex,i,cardNum)
        if bChuPai and cardNum>5 then
            LuaTweenUtils.DOKill(tf,false)
            LuaUtils.SetLocalPosition(tf,0,y,0)
            LuaTweenUtils.TweenPositionX(tf,x,0.5)
        else
            LuaUtils.SetLocalPosition(tf,x,y,0)
        end
    end

    if CLuaDouDiZhuMgr.m_Play.m_ChuPaiIndex==self.m_Index then
        --当前出牌
        self:SetChuPaiStatus()
    end
end

function CLuaDouDiZhuPlayerView:OnPlayerDouDiZhuReady(ready)
    if self.m_ReadyMark then
        if CLuaDouDiZhuMgr.m_Play and  CLuaDouDiZhuMgr.m_Play:IsAllReady() then
            self.m_ReadyMark:SetActive(false)
        else
            self.m_ReadyMark:SetActive(ready)
        end
    end

    if self.m_ViewIndex==1 then
        if ready and self.m_TipLabel then
            if CLuaDouDiZhuMgr.m_Play and not CLuaDouDiZhuMgr.m_Play:IsAllReady() then
                if CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eCrossPlay then--跨服斗地主
                    self.m_TipLabel.text=LocalString.GetString("已准备，等待全部参赛选手进入下一局比赛...")
                else
                    self.m_TipLabel.text=LocalString.GetString("已准备，等待其他玩家准备...")
                end
            else
                self.m_TipLabel.text=nil
            end
            --准备按钮隐藏
            if self.m_Button2 then self.m_Button2:SetActive(false) end
        end
    end
end
function CLuaDouDiZhuPlayerView:OnSyncDouDiZhuPlayerInfo(info)
    if self.m_MsgLabel then
        self.m_MsgLabel.text=nil
    end
    if self.m_UIFx then
        self.m_UIFx:DestroyFx()--setactive的时候特效会再次播放 所以这里直接清理掉
    end
    if CLuaDouDiZhuMgr.m_IsRemote then
        if self.m_NoPlayerNode then self.m_NoPlayerNode.gameObject:SetActive(false) end
        if self.m_PlayerNode then self.m_PlayerNode.gameObject:SetActive(true) end
    else
        self.m_Transform.gameObject:SetActive(true)
    end

    self.m_Index=info.index

    local cls=info.class
    local portraitName=CUICommonDef.GetPortraitName(info.class, info.gender, -1)
    self.m_PortraitTexture:LoadNPCPortrait(portraitName)
    self:OnUpdateDouDiZhuPlayerScore(info.score)

    if self.m_NameLabel then
        self.m_NameLabel.text=info.name
    end
    
    --全部准备好 则不显示
    if self.m_ReadyMark then
        if CLuaDouDiZhuMgr.m_Play and  CLuaDouDiZhuMgr.m_Play:IsAllReady() then
            self.m_ReadyMark:SetActive(false)
        else
            self.m_ReadyMark:SetActive(info.ready)
        end
    end
    self.m_DiZhuMark:SetActive(info.isDiZhu)
    if self.m_JinbiFx then
        self.m_JinbiFx:DestroyFx()
        if info.isDiZhu then
            self.m_JinbiFx:LoadFx(CLuaUIFxPaths.DouDiZhuJinBi)
        end
    end

    if self.m_ViewIndex==1 then
        if info.ready and self.m_TipLabel then
            if CLuaDouDiZhuMgr.m_Play and not CLuaDouDiZhuMgr.m_Play:IsAllReady() then
                self.m_TipLabel.text=nil
            else
                if CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eCrossPlay then--跨服斗地主
                    self.m_TipLabel.text=LocalString.GetString("已准备，等待全部参赛选手进入下一局比赛...")
                else
                    self.m_TipLabel.text=LocalString.GetString("已准备，等待其他玩家准备...")
                end
            end
        end
    end

    self:OnPlayerDouDiZhuTuoGuan(info.tuoGuan)

    --手牌
    self:OnSyncDouDiZhuCardsInfo(info,false)


    --如果不是比赛 而且还没有开始 并且没有准备 要显示准备按钮
    if self.m_ViewIndex==1 then
        --先把所有按钮隐藏
        if self.m_Button1 then self.m_Button1:SetActive(false) end
        if self.m_Button2 then self.m_Button2:SetActive(false) end
        if self.m_Button3 then self.m_Button3:SetActive(false) end

        if CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eGuildPlay or
			CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eJiaYuanPlay or
			CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eRemoteJiaYuanPlay then--家园
            if CLuaDouDiZhuMgr.m_Play then
                if not CLuaDouDiZhuMgr.m_Play:IsAllReady() then
                    if not info.ready then
                        --显示准备按钮
                            self:SetButtonActive(self.m_Button2,true)
                            self:SetButtonText(self.m_Button2,LocalString.GetString("准备"))
                        -- end

                        if self.m_Button2 then
                            UIEventListener.Get(self.m_Button2).onClick=LuaUtils.VoidDelegate(function(go)
                                Gac2Gas.RequestDouDiZhuReady(true)
                            end)
                        end
                    end
                end
            end
        end
        if CLuaDouDiZhuMgr.m_Play and CLuaDouDiZhuMgr.m_Play:IsAllReady() then
            --都准备好了 比赛中，根据进度显示按钮
            if CLuaDouDiZhuMgr.m_Play.m_ChuPaiIndex==self.m_Index then
                self:SetChuPaiStatus()
                CUICommonDef.ClearTransform(self.m_ShowCardsTf)
                if self.m_MsgLabel then self.m_MsgLabel.text=nil end
                self:StartCountdown(CLuaDouDiZhuMgr.m_LeftTime)
            elseif CLuaDouDiZhuMgr.m_Play.m_QiangDiZhuIndex==self.m_Index then
                self:OnSyncQiangDiZhuProgress(CLuaDouDiZhuMgr.m_IsJiaoDiZhu,CLuaDouDiZhuMgr.m_LeftTime)
            end
        end
    else
        if CLuaDouDiZhuMgr.m_Play.m_ChuPaiIndex==self.m_Index then
            self:SetChuPaiStatus()
            CUICommonDef.ClearTransform(self.m_ShowCardsTf)
            if self.m_MsgLabel then
                self.m_MsgLabel.text=nil
            end
            self:StartCountdown(CLuaDouDiZhuMgr.m_LeftTime)
        elseif CLuaDouDiZhuMgr.m_Play.m_QiangDiZhuIndex==self.m_Index then
            self:OnSyncQiangDiZhuProgress(CLuaDouDiZhuMgr.m_IsJiaoDiZhu,CLuaDouDiZhuMgr.m_LeftTime)
        end
    end
end
function CLuaDouDiZhuPlayerView:OnPlayerDouDiZhuTuoGuan(bTuoGuan)
    if self.m_ViewIndex==1 then
        if bTuoGuan then
            -- self.m_CancleTuoGuanButton:SetActive(true)
            self:SetButtonActive(self.m_CancleTuoGuanButton,true)

            if self.m_Button1 then self.m_Button1:SetActive(false) end
            if self.m_Button2 then self.m_Button2:SetActive(false) end
            if self.m_Button3 then self.m_Button3:SetActive(false) end
            -- self.m_Button2:SetActive(false)
            -- self.m_Button3:SetActive(false)
        else
            if self.m_CancleTuoGuanButton then self.m_CancleTuoGuanButton:SetActive(false) end
            --如果轮到自己出牌 还要显示按钮
            if CLuaDouDiZhuMgr.m_Play and CLuaDouDiZhuMgr.m_Play.m_ChuPaiIndex==self.m_Index then
                self:SetChuPaiStatus()
            end
        end
    else
        if self.m_StatusLabel then
            if bTuoGuan then
                self.m_StatusLabel.text=LocalString.GetString("托管中...")
            else
                self.m_StatusLabel.text=nil
            end
        end   
    end
end

function CLuaDouDiZhuPlayerView:OnPlayerMingPai(info)
    CUICommonDef.ClearTransform(self.m_HandCardsTf)
    if info.mingPai then
        local node=self.m_HandCardsTf.gameObject
        
        local tbl={}
        for i=#info.handCards,1,-1 do
            table.insert( tbl,info.handCards[i] )
        end

        for i,v in ipairs(tbl) do
            local card = NGUITools.AddChild(node,CLuaDouDiZhuMgr.m_CardTemplate)
            card:SetActive(true)
            CLuaDouDiZhuMgr.SetCard(card.transform,v,i)

        end
        local grid=node:GetComponent(typeof(UIGrid))
        grid:Reposition()

        self:ShowMessage(LocalString.GetString("明牌"))
    end
end
function CLuaDouDiZhuPlayerView:RefreshButtons()
    
end
function CLuaDouDiZhuPlayerView:OnSyncQiangDiZhuProgress(bIsJiaoDiZhu, leftTime)
    self:ShowMessage(nil)
    
    if self.m_ViewIndex==1 then
        --看是否托管
        local bTuoGuan=false
        if CLuaDouDiZhuMgr.m_Play then
            local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(self.m_Index)
            bTuoGuan=info.tuoGuan
        end
        if not bTuoGuan then
            if self.m_Button1 then
                CUICommonDef.SetActive(self.m_Button1,true,true)
            end
            self:SetButtonActive(self.m_Button1,true)
            self:SetButtonActive(self.m_Button2,false)
            self:SetButtonActive(self.m_Button3,true)
            
            if bIsJiaoDiZhu then
                self:SetButtonText(self.m_Button1,LocalString.GetString("叫地主"))
                self:SetButtonText(self.m_Button3,LocalString.GetString("不叫"))
            else
                self:SetButtonText(self.m_Button1,LocalString.GetString("抢地主"))
                self:SetButtonText(self.m_Button3,LocalString.GetString("不抢"))
            end

            if self.m_Button1 then
                CommonDefs.AddOnClickListener(self.m_Button1,DelegateFactory.Action_GameObject(function(go)
                    --叫地主
                    Gac2Gas.RequestQiangDiZhu(true)
                end),false)
            end
            if self.m_Button3 then
                CommonDefs.AddOnClickListener(self.m_Button3,DelegateFactory.Action_GameObject(function(go)
                    --不叫
                    Gac2Gas.RequestQiangDiZhu(false)
                end),false)
            end
        end
    end

    --倒计时
    self:StartCountdown(leftTime)

end

function CLuaDouDiZhuPlayerView:StartCountdown(leftTime)
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
    end
    self.m_LeftTime=leftTime

    if self.m_LeftTime>0 and self.m_CountdownLabel then
        self.m_CountdownLabel.text = tostring(math.floor(self.m_LeftTime))
        self.m_CountdownLabel.gameObject:SetActive(true)

        self.m_Tick = RegisterTickWithDuration(function ()
            if not self:OnTick() then 
                UnRegisterTick(self.m_Tick)
                self.m_Tick=nil
            end
        end,1000,(leftTime)*1000)
        
        --小于5秒
        if self.m_LeftTime<=5 then
            --倒计时
            if self.m_Slider then
                self.m_Slider.gameObject:SetActive(true)
                if self.m_SliderTweener then
                    LuaTweenUtils.Kill(self.m_SliderTweener,false)--tweenfloat的这种不是依赖于transform的，必须采用这种方式kill
                    self.m_SliderTweener = nil
                end
                local val = self.m_LeftTime/5
                self.m_SliderTweener=LuaTweenUtils.TweenFloat(val,0,val*5,
                    function(val)
                        if self.m_Slider then
                            self.m_Slider.value=val
                            if val<0.05 then--最后时刻 隐藏
                                self.m_Slider.gameObject:SetActive(false)
                                LuaTweenUtils.Kill(self.m_SliderTweener,false)
                                self.m_SliderTweener = nil
                            end
                        end
                    end)
            end
        end
    end


end
function CLuaDouDiZhuPlayerView:OnTick()
	if self.m_CountdownLabel == nil then return false end

    self.m_LeftTime = self.m_LeftTime - 1
    if self.m_LeftTime==5 then
        --倒计时
        if self.m_Slider then
            self.m_Slider.gameObject:SetActive(true)
            -- LuaTweenUtils.DOKill(self.m_Slider.transform,false)
            if self.m_SliderTweener then
                LuaTweenUtils.Kill(self.m_SliderTweener,false)--tweenfloat的这种不是依赖于transform的，必须采用这种方式kill
                self.m_SliderTweener = nil
            end
            self.m_SliderTweener=LuaTweenUtils.TweenFloat(1,0,5,
                function(val)
                    if self.m_Slider then
                        self.m_Slider.value=val
                        if val<0.05 then--最后时刻 隐藏
                            self.m_Slider.gameObject:SetActive(false)
                            LuaTweenUtils.Kill(self.m_SliderTweener,false)
                            self.m_SliderTweener = nil
                        end
                    end
                end)
        end
    end
    if self.m_LeftTime>0 then
        self.m_CountdownLabel.text = tostring(math.floor(self.m_LeftTime))
        self.m_CountdownLabel.gameObject:SetActive(true)
        return true
    end
    self.m_CountdownLabel.text=nil
    self.m_CountdownLabel.gameObject:SetActive(false)
    return false
end
--清空消息
function CLuaDouDiZhuPlayerView:ClearMsg()
    if self.m_MsgLabel then
        self.m_MsgLabel.text=nil
    end
end
function CLuaDouDiZhuPlayerView:SetButtonActive(button,active)
    if button then
        if active and not CLuaDouDiZhuMgr.m_IsWatch then
            button:SetActive(true)
        else
            button:SetActive(false)
        end
    end
end
function CLuaDouDiZhuPlayerView:SetButtonText(button,text)
    if button then
        local label=FindChild(button.transform,"Label"):GetComponent(typeof(UILabel))
        if text==LocalString.GetString("准备")
            or text==LocalString.GetString("出牌") 
            or text==LocalString.GetString("取消托管")then

            button:GetComponent(typeof(UISprite)).spriteName="common_button_orange_normal"
                
        elseif text==LocalString.GetString("叫地主")
            or text==LocalString.GetString("不叫")
            or text==LocalString.GetString("抢地主")
            or text==LocalString.GetString("不抢")
            or text==LocalString.GetString("不出")
            or text==LocalString.GetString("提示") then
            button:GetComponent(typeof(UISprite)).spriteName="common_button_darkblue_3_n"
        end
        label.text=text
    end
end

function CLuaDouDiZhuPlayerView:SetChuPaiStatus()
    if self.m_ViewIndex==1 then
        --看是否托管
        local bTuoGuan=false
        if CLuaDouDiZhuMgr.m_Play then
            local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(self.m_Index)
            bTuoGuan=info.tuoGuan
        end

        if not bTuoGuan then
            if self.m_Button1 then CUICommonDef.SetActive(self.m_Button1,true,true) end

            --如果要不起的时候 只需要显示“不出”按钮
            local ret,tbl=CLuaDouDiZhuMgr.m_Play:GetHintChuPaiCards(self.m_Index)
            if ret then
                -- self.m_Button1:SetActive(true)
                self:SetButtonActive(self.m_Button1,true)
                self:SetButtonText(self.m_Button1,LocalString.GetString("不出"))
                
                if CLuaDouDiZhuMgr.m_Play and CLuaDouDiZhuMgr.m_Play:IsMustChuPai(self.m_Index) then--必须出牌
                    if self.m_Button1 then CUICommonDef.SetActive(self.m_Button1,false,true) end
                    --要清空显示区域
	                -- g_ScriptEvent:BroadcastInLua("DouDiZhu_ClearMsg")
                else
                    if self.m_Button1 then
                        UIEventListener.Get(self.m_Button1).onClick=LuaUtils.VoidDelegate(function(go)
                            self:RequestBuChu()
                        end)
                    end
                end

                self:SetButtonActive(self.m_Button2,true)
                self:SetButtonActive(self.m_Button3,true)
                self:SetButtonText(self.m_Button2,LocalString.GetString("提示"))
                
                self:SetButtonText(self.m_Button3,LocalString.GetString("出牌"))

                if self.m_Button2 then
                    UIEventListener.Get(self.m_Button2).onClick=LuaUtils.VoidDelegate(function(go)
                        self:HintChuPai()
                    end)
                end
                if self.m_Button3 then
                    UIEventListener.Get(self.m_Button3).onClick=LuaUtils.VoidDelegate(function(go)
                        self:RequestChuPai()
                    end)
                end
            else
                self:SetButtonActive(self.m_Button1,false)
                self:SetButtonActive(self.m_Button2,true)

                self:SetButtonText(self.m_Button2,LocalString.GetString("不出"))
                if not CLuaDouDiZhuMgr.m_IsWatch then
                    if self.m_NoticeLabel then
                        self.m_NoticeLabel.text=LocalString.GetString("你没有牌比上家大哦!")
                    end
                end
                if self.m_Button2 then
                    UIEventListener.Get(self.m_Button2).onClick=LuaUtils.VoidDelegate(function(go)
                        self:RequestBuChu()
                        if self.m_NoticeLabel then
                            self.m_NoticeLabel.text=nil
                        end
                    end)
                end
                self:SetButtonActive(self.m_Button3,false)
            end
        else
            self:SetButtonActive(self.m_Button1,false)
            self:SetButtonActive(self.m_Button2,false)
            self:SetButtonActive(self.m_Button3,false)
        end
    end
end

function CLuaDouDiZhuPlayerView:OnSyncChuPaiProgress(leftTime, maxCardIndex)
    if self.m_ViewIndex==1 then
        --如果是托管状态 不显示3个按钮
        self:SetChuPaiStatus()
    end

    -- --如果当前最大牌是我 并且轮到我出牌
    -- if maxCardIndex==self.m_Index then
    --     CUICommonDef.ClearTransform(self.m_ShowCardsTf)
    -- end

    --轮到我出牌的时候 就把牌隐藏
    CUICommonDef.ClearTransform(self.m_ShowCardsTf)
    --消息也隐藏
    if self.m_MsgLabel then self.m_MsgLabel.text=nil end

    self:StartCountdown(leftTime)
end

function CLuaDouDiZhuPlayerView:HintChuPai()
    if not CLuaDouDiZhuMgr.m_Play then return end
    if self.m_ViewIndex==1 then
        local ret,tbl=CLuaDouDiZhuMgr.m_Play:GetHintChuPaiCards(self.m_Index)
        if ret then
            local handCards={}
            -- for i,v in ipairs(self.m_HandCardsTbl) do
            --     handCards[v]=true
            --     v:TakeBack()
            -- end
            for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
                if v then
                    handCards[k]=true
                    k:TakeBack()
                end
            end
            
            for i,v in ipairs(tbl) do
                for k,m in pairs(handCards) do
                    if k and k.m_Card==v then
                        k:Pop()
                        handCards[k]=nil
                    end
                end
            end
        else
        end
    end
end
function CLuaDouDiZhuPlayerView:RequestBuChu()
    --都收回
    for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
        if v then
            k:TakeBack()
        end
    end

    local list = CreateFromClass(MakeGenericClass(List, Object))
    Gac2Gas.RequestChuPai(MsgPackImpl.pack(list))
end
function CLuaDouDiZhuPlayerView:RequestChuPai()
    local tbl={}
    local list = CreateFromClass(MakeGenericClass(List, Object))

    for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
        if k.m_Selected then
            table.insert( tbl,k.m_Card )
            -- CommonDefs.ListAdd(list, typeof(UInt32), k.m_Card)
        end
    end
    --排序
    table.sort( tbl )
    for i,v in ipairs(tbl) do
        CommonDefs.ListAdd(list, typeof(UInt32), v)
    end

    if #tbl==0 then
        --如果有牌的话 应该提示出牌
        local ret,tbl=CLuaDouDiZhuMgr.m_Play:GetHintChuPaiCards(self.m_Index)
        if ret then
            g_MessageMgr:ShowMessage("DOUDIZHU_MUST_CHUPAI", {})
        else
            Gac2Gas.RequestChuPai(MsgPackImpl.pack(list))
        end
    else
        --先检查一下 再提交
        if not CLuaDouDiZhuMgr.m_Play then return end
        local ret,msg=CLuaDouDiZhuMgr.m_Play:IsChuPaiValid(CLuaDouDiZhuMgr.m_PlayerIndex,tbl)
        if ret then
            Gac2Gas.RequestChuPai(MsgPackImpl.pack(list))
        else
            g_MessageMgr:ShowMessage(msg, {})
        end
    end
end

function CLuaDouDiZhuPlayerView:OnPlayerChuPai()
    --出牌声音
    SoundManager.Inst:PlayOneShot(UISoundEvents.WndOpen, Vector3(0,0,0),nil,0)
    
    local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(self.m_Index)
    
    if self.m_ViewIndex==1 then
        CLuaDouDiZhuCardMgr.ChuPai(info.handCards,info.showCards)
    else
        self:OnSyncDouDiZhuCardsInfo(info,false,true)
    end

    if #info.showCards==0 then
        self:ShowMessage(LocalString.GetString("不出"))
    end

    --取消倒计时
    self:CancleTick()

    if self.m_ViewIndex==1 then
        self:SetButtonActive(self.m_Button1,false)
        self:SetButtonActive(self.m_Button2,false)
        self:SetButtonActive(self.m_Button3,false)
        if self.m_NoticeLabel then
            self.m_NoticeLabel.text=nil--出玩牌之后消失
        end
    end

    --判断类型 做发牌特效
    if self.m_UIFx then
        if CLuaDouDiZhuMgr.m_Play then
            self.m_UIFx:DestroyFx()
            local type=CLuaDouDiZhuMgr.m_Play:ParseCardsType(info.showCards)
            if type==EnumDouDiZhuCardsType.eFeiJi
                or type==EnumDouDiZhuCardsType.eFeiJiDai1
                or type==EnumDouDiZhuCardsType.eFeiJiDai2
                or type==EnumDouDiZhuCardsType.e4FeiJi
                or type==EnumDouDiZhuCardsType.e4FeiJiDai2
                or type==EnumDouDiZhuCardsType.e4FeiJiDai2Dui then
                --飞机特效
                self.m_UIFx:LoadFx(CLuaUIFxPaths.DouDiZhuFeiJi)
                SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.DouDiZhuFeiJi, Vector3(0,0,0),nil,0)
            elseif type==EnumDouDiZhuCardsType.eZhaDan then
                --炸弹
                self.m_UIFx:LoadFx(CLuaUIFxPaths.DouDiZhuZhaDan)
                SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.DouDiZhuZhaDan, Vector3(0,0,0),nil,0)
            elseif type==EnumDouDiZhuCardsType.eHuoJian then
                --火箭
                self.m_UIFx:LoadFx(CLuaUIFxPaths.DouDiZhuHuoJian)
                SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.DouDiZhuHuoJian, Vector3(0,0,0),nil,0)
            end
        end
    end

end

function CLuaDouDiZhuPlayerView:OnDestroy()
    self:CancleTick()
end

function CLuaDouDiZhuPlayerView:ShowMessage(msg)
    if self.m_MsgLabel then 
        self.m_MsgLabel.text=msg
        if msg then
            --动画
            local tf=self.m_MsgLabel.transform
            LuaUtils.SetLocalScale(tf,2,2,2)
            LuaTweenUtils.TweenScaleXY(tf,1,1,0.2,Ease.OutQuad)
            LuaTweenUtils.TweenAlpha(tf,0,1,0.2)
        end
    end
end

function CLuaDouDiZhuPlayerView:CancleTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end
    if self.m_CountdownLabel then
        self.m_CountdownLabel.text=nil
        self.m_CountdownLabel.gameObject:SetActive(false)
    end

    if self.m_SliderTweener then
        LuaTweenUtils.Kill(self.m_SliderTweener,false)
        self.m_SliderTweener=nil
    end
    if self.m_Slider then
        self.m_Slider.gameObject:SetActive(false)
    end
end
function CLuaDouDiZhuPlayerView:OnDouDiZhuStartRound()
    if self.m_ViewIndex==1 then
        if self.m_TipLabel then 
            self.m_TipLabel.text=nil
        end
    end
    if self.m_ReadyMark then
        self.m_ReadyMark:SetActive(false)
    end
end
function CLuaDouDiZhuPlayerView:OnPlayerQiangDiZhu(status)
    --抢完地主之后 隐藏按钮
    if self.m_ViewIndex==1 then
        -- self.m_Button1:SetActive(false)
        self:SetButtonActive(self.m_Button1,false)
        -- self.m_Button2:SetActive(false)
        self:SetButtonActive(self.m_Button2,false)
        -- self.m_Button3:SetActive(false)
        self:SetButtonActive(self.m_Button3,false)
    end
    if status==EnumQiangDiZhuStatus.eJiaoDiZhu then
        self:ShowMessage(LocalString.GetString("叫地主"))
    elseif status==EnumQiangDiZhuStatus.eQiangDiZhu then
        self:ShowMessage(LocalString.GetString("抢地主"))
    end
    --
    self:CancleTick()
end

function CLuaDouDiZhuPlayerView:SetDiZhu()
    self.m_DiZhuMark:SetActive(true)
    -- self.m_DiZhuMark2:SetActive(true)
    if self.m_JinbiFx then
        self.m_JinbiFx:DestroyFx()
        self.m_JinbiFx:LoadFx(CLuaUIFxPaths.DouDiZhuJinBi)
    end
    self:DoDiZhuMaoAni()
end

function CLuaDouDiZhuPlayerView:OnDiZhuAddReserveCards(info)
    if self.m_ViewIndex==1 then
        --做动画
        CLuaDouDiZhuCardMgr.FaDiPai(info.handCards)
    else
        --手牌数量变化
        if self.m_HandCards2 then
            self.m_HandCards2:SetActive(true)
        end
        if self.m_CardNumLabel then
            self.m_CardNumLabel.text=tostring(info.handCardsCount)
        end
    end
end

function CLuaDouDiZhuPlayerView:DoDiZhuMaoAni()
    SoundManager.Inst:PlayOneShot(CLuaUISoundEvents.DouDiZhuDiZhuMao, Vector3(0,0,0),nil,0)
    -- print(self.m_Transform.parent)
    local initX=37
    local initY=215
    local worldPos=self.m_Transform.parent.parent:TransformPoint(Vector3(-31,0,0))
    local tf=self.m_DiZhuMark.transform
    local startPos=tf:InverseTransformPoint(worldPos)

    local tf=self.m_DiZhuMark.transform
    tf.localScale=Vector3(2,2,1)
    LuaTweenUtils.TweenScaleXY(tf,1,1,0.5,Ease.OutBack)
    LuaUtils.SetLocalRotationZ(tf,-30)
    LuaTweenUtils.TweenRotationZ(tf,0,0.5)

    local targetPos=Vector3(initX,initY,0)

    local midPos=Vector3((initX+startPos.x)/2,(initY+startPos.y+150)/2,0)

    local path={startPos,midPos,targetPos}
    local array = Table2Array(path, MakeArrayClass(Vector3))
    LuaUtils.SetLocalPosition(tf,startPos.x,startPos.y,0)
    LuaTweenUtils.DOLocalPathOnce(tf,array,0.5,Ease.OutQuart)
end

function CLuaDouDiZhuPlayerView:OnShowPlayerDouDiZhuShortMessage(id)
    if self.m_SpeakingBubble then
        local text=DouDiZhu_ShortMessage.GetData(id).Message
        self.m_SpeakingBubble:Init(text,EnumSpeakingBubbleType.Default,nil,5)
    end
end

function CLuaDouDiZhuPlayerView:OnResetDouDiZhuRound()
    --按钮状态要修改 牌要清空
    local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(self.m_Index)
    if info then
        self:OnSyncDouDiZhuPlayerInfo(info)
    end
end
function CLuaDouDiZhuPlayerView:OnUpdateDouDiZhuPlayerScore(score)
    if self.m_ScoreLabel then
        if CLuaDouDiZhuMgr.m_Type == EnumDouDiZhuType.eRemoteQiangXiangZi then
            self.m_ScoreLabel.text = LuaDouDiZhuBaoXiangMgr:GetScoreText(self.m_Index)
        else
            self.m_ScoreLabel.text = LocalString.GetString("积分").." "..tostring(score)
        end
    end
end
function CLuaDouDiZhuPlayerView:OnDouDiZhuEndRound(winnerIndex)
    if self.m_ViewIndex~=1 and self.m_Index~=winnerIndex then
        --显示底牌
        CUICommonDef.ClearTransform(self.m_ShowCardsTf)
        if self.m_MsgLabel then self.m_MsgLabel.text=nil end
        local info=CLuaDouDiZhuMgr.m_Play:GetIndexInfo(self.m_Index)
        if info then

            local tbl={}
            for i=#info.handCards,1,-1 do
                table.insert( tbl,info.handCards[i] )
            end
            local node=self.m_ShowCardsTf.gameObject
        
            local cardNum=#tbl
            -- print("cardNum",cardNum)
            for i,v in ipairs(tbl) do
                -- print(i,v)
                local card = NGUITools.AddChild(node,CLuaDouDiZhuMgr.m_CardTemplate)
                card:SetActive(true)
                local tf=card.transform
                
                CLuaDouDiZhuMgr.SetCard(tf,v,i)
                local x,y=CLuaDouDiZhuCardMgr.GetCardLocalPosition(tf,self.m_ViewIndex,i,cardNum)
                LuaUtils.SetLocalPosition(tf,x,y,0)
            end
        end
    end
end

function CLuaDouDiZhuPlayerView:OnApplyJoinRemoteDouDiZhuPlayerListAlert()
    if self.m_NoPlayerNode and self.m_NoPlayerNode.gameObject.activeSelf then
        local alert = self.m_InviteListButton.transform:Find("Alert").gameObject
        alert:SetActive(CLuaDouDiZhuMgr.m_ApplyJoinRemoteDouDiZhuPlayerListAlert or false)
    end
end