-- Auto Generated!!
local CCJBMgr = import "L10.Game.CCJBMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipment = import "L10.Game.CEquipment"
local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_Shelf = import "L10.UI.CPlayerShop_Shelf"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemFlag = import "L10.Game.EnumItemFlag"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local EnumQualityType = import "L10.Game.EnumQualityType"
CPlayerShop_Shelf.m_Init_CS2LuaHook = function (this)
    this.m_TipLabel.text = g_MessageMgr:FormatMessage("PLAYERSHOP_ADD_ITEM_DUE_INTERFACE_TIP")
    this.m_SearchResultTable.m_DataSource = this
    this.m_LeftTabLabel.text = System.String.Format(LocalString.GetString("剩余[ffff00]{0}[-]格"), CPlayerShopData.Main.MaxShelfCount - CPlayerShopData.Main.CurrentShelfCount)
    this.m_CachedData = CPlayerShopMgr.m_ShelfContext
    if this.m_CachedData ~= nil then
        local item = this.m_CachedData.Item
        if item ~= nil then
            this.m_CountInput:SetMinMax(1, this.m_CachedData.Count, 1)
            this.m_CountInput:SetValue(1, true)
            this.m_SingleItemPriceInput:SetMinMax(this.m_MinPrice, this.m_MaxPrice, 1)
            this.m_SingleItemPriceInput:SetValue(this.m_CachedData.Price, true)
            Gac2Gas.QueryPlayerShopOnShelfPrice(this.m_CachedData.PackagePlace)

            this.m_TotalPriceInput.text = tostring((this.m_CachedData.Count * this.m_CachedData.Price))

            this.m_NameLabel.text = item.Name
            this.m_LevelLabel.text = System.String.Format(LocalString.GetString("{0}级"), item.Grade)
            if item.IsItem then
                this.m_NameLabel.color = CItem.GetColor(item.TemplateId)
                this.m_ItemInfoLabel:SetContent(item.Item.Description)
            else
                this.m_NameLabel.color = CEquipment.GetColor(item.TemplateId)
                this.m_ItemInfoLabel:SetContent(item.Equip:GetDescription())
            end
            this.m_ItemTexture:LoadMaterial(item.Icon)
            this.m_TaxInput.text = tostring((CPlayerShopMgr.Inst:GetTax(this.m_CachedData.Price, this.m_CachedData.Count)))

            this.m_SearchResultTable.gameObject:SetActive(true)
            if item.IsEquip and item.Equip and (item.Equip.IsRedEquipment or item.Equip.IsBlueEquipment) then
                CPlayerShop_Shelf.TotalPriceLimit = tonumber(PlayerShop_Setting.GetData("MaxRedBluePreciousPrice").Value)
            end
            --如果是盗版装备,则只传盗版装备Id
            if item.IsEquip and item.Equip:FlagIsSet(EnumItemFlag.ISFAKE) then
                this:SearchOtherSells(CPlayerShopMgr.Inst:ToAliasTemplateId(item.TemplateId))
            else
                this:SearchOtherSells(item.TemplateId)
            end
        end
    end
end
CPlayerShop_Shelf.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopSearchedItemsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action4, MakeGenericClass(List, CPlayerShopItemData), UInt32, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.QueryPlayerShopOnShelfPriceResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfPriceResult, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.PlayerShopOnShelfSuccess, MakeDelegateFromCSFunction(this.OnPlayerShopOnShelfSuccess, MakeGenericClass(Action3, String, UInt32, UInt32), this))
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this), true)
    UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickItemTexture, VoidDelegate, this), true)
    this.m_SingleItemPriceInput.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_SingleItemPriceInput.onValueChanged, MakeDelegateFromCSFunction(this.OnInputValueChanged, MakeGenericClass(Action1, UInt32), this), true)
    this.m_CountInput.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_CountInput.onValueChanged, MakeDelegateFromCSFunction(this.OnInputValueChanged, MakeGenericClass(Action1, UInt32), this), true)
    this.m_SubmitButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SubmitButton.OnClick, MakeDelegateFromCSFunction(this.OnSubmit, MakeGenericClass(Action1, QnButton), this), true)
end
CPlayerShop_Shelf.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopSearchedItemsResult, MakeDelegateFromCSFunction(this.OnRecieveSearchResult, MakeGenericClass(Action4, MakeGenericClass(List, CPlayerShopItemData), UInt32, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.QueryPlayerShopOnShelfPriceResult, MakeDelegateFromCSFunction(this.OnQueryPlayerShopOnShelfPriceResult, MakeGenericClass(Action4, UInt32, UInt32, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.PlayerShopOnShelfSuccess, MakeDelegateFromCSFunction(this.OnPlayerShopOnShelfSuccess, MakeGenericClass(Action3, String, UInt32, UInt32), this))
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnClose, VoidDelegate, this), false)
    UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnClickItemTexture, VoidDelegate, this), false)
    this.m_SingleItemPriceInput.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_SingleItemPriceInput.onValueChanged, MakeDelegateFromCSFunction(this.OnInputValueChanged, MakeGenericClass(Action1, UInt32), this), false)
    this.m_CountInput.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_CountInput.onValueChanged, MakeDelegateFromCSFunction(this.OnInputValueChanged, MakeGenericClass(Action1, UInt32), this), false)
    this.m_SubmitButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SubmitButton.OnClick, MakeDelegateFromCSFunction(this.OnSubmit, MakeGenericClass(Action1, QnButton), this), false)
    CPlayerShop_Shelf.TotalPriceLimit = 1000000000
end
CPlayerShop_Shelf.m_SearchOtherSells_CS2LuaHook = function (this, templateId)
    --清空数据再查询
    CommonDefs.ListClear(this.m_SearchResultData)
    this.m_SearchResultTable:ReloadData(false, false)
    if this.m_CachedData.Item.IsPrecious then
        Gac2Gas.QueryPlayerShopSearchedItems(templateId, SearchOption_lua.Valueable, 1, 15, false, 0)
    else
        Gac2Gas.QueryPlayerShopSearchedItems(templateId, SearchOption_lua.All, 1, 15, false, 0)
    end
end
CPlayerShop_Shelf.m_OnRecieveSearchResult_CS2LuaHook = function (this, data, totalCount, indexStart, indexEnd)
    CommonDefs.ListClear(this.m_SearchResultData)
    --去掉自己当前上架的物品
    do
        local i = 0
        while i < data.Count do
            if data[i].OwnerId ~= CClientMainPlayer.Inst.Id then
                CommonDefs.ListAdd(this.m_SearchResultData, typeof(CPlayerShopItemData), data[i])
                if this.m_SearchResultData.Count >= 4 then
                    break
                end
            end
            i = i + 1
        end
    end
    this.m_SearchResultTable:ReloadData(true, false)
    this.m_NoShopItemLabel:SetActive(this.m_SearchResultData.Count == 0)
end
CPlayerShop_Shelf.m_OnQueryPlayerShopOnShelfPriceResult_CS2LuaHook = function (this, bagPos, basisPrice, minPrice, maxPrice)
    if this.m_CachedData.PackagePlace == bagPos then
        if basisPrice == 0 then
            MessageWndManager.ShowOKMessage(LocalString.GetString("不能上架此物品"), nil)
        else
            if maxPrice == System.UInt32.MaxValue then
                maxPrice = CPlayerShop_Shelf.TotalPriceLimit
            end
            if basisPrice == System.UInt32.MaxValue then
                basisPrice = CPlayerShop_Shelf.TotalPriceLimit
            end
            this.m_MaxPrice = maxPrice
            this.m_MinPrice = minPrice
            this.m_BasicPrice = basisPrice
            if this.m_MaxPrice == CPlayerShop_Shelf.TotalPriceLimit then
                this.m_SingleItemPriceInput:SetMinMax(0, maxPrice, 1)
                this.m_SingleItemPriceInput:SetValue(math.floor(math.max(this.m_CachedData.Price, this.m_BasicPrice)), true)
                this.m_SingleItemPriceInput:SetButtonType(QnAddSubAndInputButton.ButtonType.OnlyAllowInput)
                this.m_SingleItemPriceInput:UpdatePlaceHolder(System.String.Format(LocalString.GetString("最低价格{0}"), this.m_MinPrice))
                this.m_SingleItemPriceInput:OverrideText("")
                this.m_PriceTipLabel.gameObject:SetActive(false)
            else
                this.m_SingleItemPriceInput:SetButtonType(QnAddSubAndInputButton.ButtonType.OnlyAllowButtons)
                local step = math.floor(tonumber((this.m_MaxPrice - this.m_MinPrice) * CPlayerShopMgr.PriceParameters or 0))
                if step < 1 then
                    step = 1
                end
                this.m_SingleItemPriceInput:SetMinMax(this.m_MinPrice, this.m_MaxPrice, step)
                this.m_SingleItemPriceInput:SetValue(math.floor(math.max(this.m_CachedData.Price, this.m_BasicPrice)), true)
                this.m_PriceTipLabel.gameObject:SetActive(true)
                this:SetPriceTip()
            end
        end
    end
end
CPlayerShop_Shelf.m_OnInputValueChanged_CS2LuaHook = function (this, v)
    if this.m_CachedData ~= nil then
        local price = this.m_SingleItemPriceInput:GetValue()
        this:SetPriceTip()
        local count = this.m_CountInput:GetValue()
        local totalPrice = price * count
        if totalPrice > CPlayerShop_Shelf.TotalPriceLimit then
            this.m_TotalPriceInput.text = LocalString.GetString("超过10亿")
            this.m_TaxInput.text = tostring((CPlayerShopMgr.Inst:GetTax(price, count)))
        else
            this.m_TotalPriceInput.text = tostring(totalPrice)
            this.m_TaxInput.text = tostring((CPlayerShopMgr.Inst:GetTax(price, count)))
        end
    end
end
CPlayerShop_Shelf.m_OnReview_CS2LuaHook = function (this) 
    if this.m_CachedData ~= nil then
        local item = this.m_CachedData.Item

        if item ~= nil and item.IsItem then
            if item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao or item.Item.Type == EnumItemType_lua.Chuanjiabao then
                CCJBMgr.Inst:OpenTipShow(item.Id, false)
            end
        end
    end
end
CPlayerShop_Shelf.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local reviewAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("预览"), MakeDelegateFromCSFunction(this.OnReview, Action0, this))

    local item = CItemMgr.Inst:GetById(itemId)
    if item == nil then
        local _item = CPlayerShopMgr.Inst:GetItemById(itemId)
        if _item ~= nil then
            item = _item.Item
        end
    end

    if item ~= nil and item.IsItem then
        if item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao or item.Item.Type == EnumItemType_lua.Chuanjiabao then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), reviewAction)
        end
    end

    return actionPairs
end
CPlayerShop_Shelf.m_OnSubmit_CS2LuaHook = function (this, button)
    local price = this.m_SingleItemPriceInput:GetValue()
    if price < this.m_MinPrice then
        MessageWndManager.ShowOKMessage(LocalString.GetString("该物品限定了最低价格为：") .. this.m_MinPrice, nil)
        this.m_SingleItemPriceInput:SetValue(this.m_MinPrice, true)
        return
    elseif price > this.m_MaxPrice then
        MessageWndManager.ShowOKMessage(LocalString.GetString("该物品限定了最高价格为：") .. this.m_MaxPrice, nil)
        this.m_SingleItemPriceInput:SetValue(this.m_MaxPrice, true)
        return
    end
    local count = this.m_CountInput:GetValue()
    local totalPrice = price * count
    if totalPrice > CPlayerShop_Shelf.TotalPriceLimit then
        MessageWndManager.ShowOKMessage(LocalString.GetString("上架失败，一次上架的商品总价不能超过10亿！"), nil)
    elseif this.m_CachedData ~= nil then
        this.m_CachedData.Price = price
        this.m_CachedData.Count = count

        --上架参数：商店Id，包裹位置，上架位置，价格
        if this.m_CachedData.Item ~= nil then
            if CPlayerShopMgr.Inst:IsPreciousButNotGhostEquipOrJueJi(this.m_CachedData.Item) and this.m_CachedData.Price >= CPlayerShopMgr.s_CheckBigPrice then
                local msg = g_MessageMgr:FormatMessage("Sell_Big_Reconfirm")
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                    if CPlayerShop_Shelf.s_ShowConfirm and this.m_CachedData.Item.IsPrecious and this.m_CachedData.Price <= this.m_BasicPrice then
                        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("你在用最低价格上架珍品，请问是否上架？"), DelegateFactory.Action(function () 
                            CPlayerShopMgr.Inst:DoOnShelf(this.m_CachedData)
                        end), nil, nil, nil, false)
                    else
                        CPlayerShopMgr.Inst:DoOnShelf(this.m_CachedData)
                    end
                end), nil, nil, nil, false)
            else
                if CPlayerShop_Shelf.s_ShowConfirm and this.m_CachedData.Item.IsPrecious and this.m_CachedData.Price <= this.m_BasicPrice then
                    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("你在用最低价格上架珍品，请问是否上架？"), DelegateFactory.Action(function () 
                        CPlayerShopMgr.Inst:DoOnShelf(this.m_CachedData)
                    end), nil, nil, nil, false)
                else
                    CPlayerShopMgr.Inst:DoOnShelf(this.m_CachedData)
                end
            end
        end
    end
end
CPlayerShop_Shelf.m_SetPriceTip_CS2LuaHook = function (this) 
    if not this.m_PriceTipLabel.gameObject.activeSelf then
        return
    end
    local t = this:GetPricePercent(this.m_BasicPrice, this.m_SingleItemPriceInput:GetValue())
    this.m_PriceTipLabel.color = t > 100 and Color.red or Color.green
    if t > 100 then
        this.m_PriceTipLabel.text = System.String.Format(LocalString.GetString("推荐价格：+{0}%"), t - 100)
    else
        this.m_PriceTipLabel.text = System.String.Format(LocalString.GetString("推荐价格：{0}%"), t - 100)
    end
end
CPlayerShop_Shelf.m_GetPricePercent_CS2LuaHook = function (this, basicPrice, currentPrice) 
    local t = 0
    if basicPrice > 0 then
        t = math.floor(100 * currentPrice / basicPrice + 0.5)
    end
    return t
end
