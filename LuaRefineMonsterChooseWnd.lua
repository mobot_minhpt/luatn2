local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UILabel = import "UILabel"
local StringStringKeyValuePair = import "L10.Game.StringStringKeyValuePair"
local DelegateFactory = import "DelegateFactory"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LocalString = import "LocalString"
local CChatLinkMgr = import "CChatLinkMgr"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"

LuaRefineMonsterChooseWnd=class()
RegistClassMember(LuaRefineMonsterChooseWnd,"m_CommitBtn")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_VoidBtn")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_CommitBtnLabel")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_VoidLabel")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_TableView")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_TitleLabel")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_DescLabel")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_CloseBtn")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_LimitQuality")
--当前选中对象 一个Table
--单选时 永远只保存一个对象
RegistClassMember(LuaRefineMonsterChooseWnd,"m_CurrentMonsterData")
--展示的MonsterList
RegistClassMember(LuaRefineMonsterChooseWnd,"m_MonsterList")
RegistClassMember(LuaRefineMonsterChooseWnd,"m_CurrentMonsterQuality")
-- 引导结束时的Action
RegistClassMember(LuaRefineMonsterChooseWnd,"m_Action")

function LuaRefineMonsterChooseWnd:InitComponents()
    self.m_TableView = self.transform:Find("Anchor/Offset/TableView"):GetComponent(typeof(QnTableView))
    self.m_CommitBtn = self.transform:Find("Anchor/Offset/CommitBtn").gameObject
    self.m_CommitBtnLabel = self.transform:Find("Anchor/Offset/CommitBtn/Label"):GetComponent(typeof(UILabel))
    self.m_VoidLabel = self.transform:Find("Anchor/Offset/Bg/VoidLabel"):GetComponent(typeof(UILabel))
    self.m_VoidBtn = self.transform:Find("Anchor/Offset/Bg/VoidLabel/VoidBtn").gameObject

    self.m_TitleLabel = self.transform:Find("Anchor/Offset/Bg/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel = self.transform:Find("Anchor/Offset/DescLabel"):GetComponent(typeof(UILabel))
    self.m_CloseBtn = self.transform:Find("Anchor/Offset/CloseBtn").gameObject
end

function LuaRefineMonsterChooseWnd:Init()
    Gac2Gas.QueryFaZhenLianHuaMonsterQueueInfo()
    self:InitComponents()

    self.m_CurrentMonsterQuality = LuaZongMenMgr.m_RefineMonsterStatusTable["quality"]
    UIEventListener.Get(self.m_CommitBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnCommit()
    end)

    UIEventListener.Get(self.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnClose()
    end)

    UIEventListener.Get(self.m_VoidBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.CloseUI(CLuaUIResources.RefineMonsterChooseWnd)
        LuaZhuoYaoMgr.m_ZhuoYaoMainWndTab = 1
        CUIManager.ShowUI(CLuaUIResources.ZhuoYaoMainWnd)
    end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local monsterData = self.m_MonsterList[row+1]
        local tujianData = ZhuoYao_TuJian.GetData(monsterData.TemplateId)
        local labels = {}
        table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("怪物品质"), tostring(monsterData.Quality)))
        table.insert(labels, CreateFromClass(StringStringKeyValuePair, LocalString.GetString("炼化时间"), LuaZongMenMgr:ScondToString(tujianData.Time)))

        if not CGuideMgr.Inst:IsInPhase(213) then
            LuaZhuoYaoMgr:ShowTip(monsterData, labels)
        end
        local tableItem = self.m_TableView:GetItemAtRow(row)
        self.m_CurrentMonsterData = monsterData
        g_ScriptEvent:BroadcastInLua("RefineMonsterSelectMonsterToCompare", self.m_CurrentMonsterData)
    end)

    self.m_LimitQuality = 0


    if LuaZongMenMgr.m_OpenNewSystem then
        self.m_VoidLabel.text = LocalString.GetString("暂无可炼化的妖怪，请先捕捉一只本周已收录的妖怪")
        self.m_VoidBtn:SetActive(true)
    else
        local contentLink = SafeStringFormat3(LocalString.GetString("[ACF9FF]暂未捕捉到炼化怪，可使用[-][FFFF00]缚妖索[-][ACF9FF]捕捉哦[-][00FF00]<link button=点击获取,ItemGet,21021026>"))
        self.m_VoidLabel.text = CChatLinkMgr.TranslateToNGUIText(contentLink, false)
        self.m_VoidBtn:SetActive(false)
    end

    UIEventListener.Get(self.m_VoidLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)
    
    self:InitStatus()
end

function LuaRefineMonsterChooseWnd:InitStatus()
    self.m_MonsterList = {}
    self.m_CurrentMonsterData = nil

    for k,v in pairs(LuaZhuoYaoMgr.m_WarehouseYaoGuaiList) do
        table.insert(self.m_MonsterList, v)
    end

    if #self.m_MonsterList == 0 then
        self.m_VoidLabel.gameObject:SetActive(true)
        self.m_CommitBtn:SetActive(false)
        CUICommonDef.SetActive(self.m_CommitBtn, false, true)
    else
        self.m_VoidLabel.gameObject:SetActive(false)
        self.m_CommitBtn:SetActive(true)
        CUICommonDef.SetActive(self.m_CommitBtn, true, false)
    end

    -- 排序
    table.sort(self.m_MonsterList, function(monster1, monster2)
        if monster1.Quality ~= monster2.Quality then
            return monster1.Quality > monster2.Quality
        elseif monster1.Level ~= monster2.Level then
            return monster1.Level > monster2.Level
        elseif monster1.MonsterTempId ~= monster2.MonsterTempId then
            return monster1.MonsterTempId > monster2.MonsterTempId
        else
            return false
        end
    end)

    self.m_TableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_MonsterList
        end,
        
        function(item,index)
            self:InitItem(item.transform, index)
        end)

    self.m_TableView:ReloadData(true, false)
end

function LuaRefineMonsterChooseWnd:InitItem(transform, index)
    local iconTexture = transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local qualitySprite = transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local disableSprite = transform:Find("DisableSprite").gameObject

    local monsterData = self.m_MonsterList[index+1]
    local data = ZhuoYao_TuJian.GetData(monsterData.TemplateId)
    local quality = self.m_LimitQuality
    iconTexture:LoadNPCPortrait(data.Icon)
    local borderSpriteName = LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(data, monsterData.Quality)
    qualitySprite.spriteName = borderSpriteName
    if quality ~= 0 and quality > monsterData.Quality then
        disableSprite:SetActive(true)
    else
        disableSprite:SetActive(false)
    end
end

function LuaRefineMonsterChooseWnd:InitQueueInfo(infos)
    if infos.Count/7 == LianHua_Setting.GetData().LianHuaGuaiLimit then
        local minQuality = 0
        for i=0, infos.Count-7, 7 do
            if minQuality == 0 or infos[i+2] < minQuality then
                minQuality = infos[i+2]
            end
        end
        self.m_LimitQuality = minQuality
    else
        self.m_LimitQuality = 0
    end
    self.m_TableView:ReloadData(true,false)
end

function LuaRefineMonsterChooseWnd:OnCommit()
    if self.m_CurrentMonsterData ~= nil then
        local quality = LuaZongMenMgr.m_RefineMonsterStatusTable["quality"]
        Gac2Gas.RequestPutMonsterIntoFaZhen(self.m_CurrentMonsterData.Id)
    else
        g_MessageMgr:ShowMessage("Please_Choose_Fazhen_LianHuaGuai")
    end
end

-- 用于处理引导
function LuaRefineMonsterChooseWnd:GetGuideGo(methodName)
    if methodName == "GetTableViewGo" then
        return self.m_TableView.gameObject
    elseif methodName == "GetCloseBtn" then
        return self.m_CloseBtn
    elseif methodName == "GetCommitBtn" then
        if self.m_CommitBtn.activeSelf then
            return self.m_CommitBtn
        else
            return self.m_CloseBtn
        end
    elseif methodName == "GetItem" then
        local item = self.m_TableView:GetItemAtRow(0)
        if item then
            return item.gameObject
        else
            return self.m_TableView.gameObject
        end
    end
end

function LuaRefineMonsterChooseWnd:OnClose()
    CUIManager.CloseUI(CLuaUIResources.RefineMonsterChooseWnd)
end

function LuaRefineMonsterChooseWnd:OnEnable()
    g_ScriptEvent:AddListener("OnWarehouseYaoGuaiListUpdate", self, "InitStatus")
    g_ScriptEvent:AddListener("QueryFaZhenLianHuaMonsterQueueInfoResult", self, "InitQueueInfo")
end

function LuaRefineMonsterChooseWnd:OnDisable()
    LuaZhuoYaoMgr.m_ZhuoYaoMainWndTab = 0
    g_ScriptEvent:RemoveListener("OnWarehouseYaoGuaiListUpdate", self, "InitStatus")
    g_ScriptEvent:RemoveListener("QueryFaZhenLianHuaMonsterQueueInfoResult", self, "InitQueueInfo")
    g_ScriptEvent:BroadcastInLua("RefineMonsterSelectMonsterToCompare", nil)
end
