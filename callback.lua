local Gas2Gac=import "L10.Game.Gas2Gac"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local CGuildMgr=import "L10.Game.CGuildMgr"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CScene=import "L10.Game.CScene"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CUIActionMgr = import "L10.UI.CUIActionMgr"
local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
local EnumQMPKPlayStage=import "L10.Game.EnumQMPKPlayStage"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local CItemMgr = import "L10.Game.CItemMgr"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CMainCamera = import "L10.Engine.CMainCamera"
local Object = import "System.Object"
local MessageWndManager=import "L10.UI.MessageWndManager"
local Screen = import "UnityEngine.Screen"
local ScreenOrientation = import "UnityEngine.ScreenOrientation"
local Main = import "L10.Engine.Main"
local MsgPackImpl = import "MsgPackImpl"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local Object = import "System.Object"
local SystemInfo = import "UnityEngine.SystemInfo"
local CTaskListBoard = import "L10.UI.CTaskListBoard"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local Vector3 = import "UnityEngine.Vector3"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"

local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local NativeTools = import "L10.Engine.NativeTools"
local CClientObjectRoot=import "L10.Game.CClientObjectRoot"
local LayerDefine = import "L10.Engine.LayerDefine"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local CWeekendGameplayMgr = import "L10.Game.CWeekendGameplayMgr"
local CMenPaiChuangGuanMgr = import "L10.Game.CMenPaiChuangGuanMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CRankData = import "L10.UI.CRankData"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local Utility = import "L10.Engine.Utility"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local CPackageView=import "L10.UI.CPackageView"
local CConversationMgr = import "L10.Game.CConversationMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CChargeMgr = import "L10.Game.CChargeMgr"
local MouseInputHandler = import "L10.Engine.MouseInputHandler"

g_OpenWndCallback={}
g_OpenWndCallback["ZongMenWuXingNiuZhuanWnd"]=function(wndName,data)
	LuaZongMenMgr:RequestTempChangeSectWuxing(data)
end
g_OpenWndCallback["ZongMenChangeSkyboxWnd"]=function(wndName,data)
	LuaZongMenMgr.m_ChangeZongMenNameContextUd = data
	CUIManager.ShowUI(CLuaUIResources.ZongMenChangeSkyboxWnd)
end
g_OpenWndCallback["ZongMenRuleWnd"] = function(wnd,data)
	Gac2Gas.QuerySectInfoFromStone(LuaZongMenMgr.m_SyncCurrentSectId)
end
g_OpenWndCallback["RequestChangeSectStone"] = function(wnd,data)
	Gac2Gas.RequestChangeSectStone_Ite(CClientMainPlayer.Inst.BasicProp.SectId)
end
g_OpenWndCallback["DragonBoatGamePlayWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.DragonBoatGamePlayWnd)
end
g_OpenWndCallback["WorldCupWnd"]=function(wndName,data)
        CUIManager.ShowUI(CUIResources.WorldCupWnd)
end
g_OpenWndCallback["BlowBubblesWnd"]=function(wndName,data)
	local infosDict = MsgPackImpl.unpack(data)
	CommonDefs.DictIterate(infosDict, DelegateFactory.Action_object_object(function(key,val)
		if key == "ItemId" then
			CLuaLiuYiMgr.ItemId = CommonDefs.Convert_ToString(val)
		elseif key == "Pos" then
			CLuaLiuYiMgr.ItemPos = CommonDefs.Convert_ToUInt32(val)
		elseif key == "Place" then
			CLuaLiuYiMgr.ItemPlace = CommonDefs.Convert_ToUInt32(val)
		end
	end))
	if CUIManager.IsLoaded(CIndirectUIResources.ItemInfoWnd) then
		CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
	end
	if CUIManager.IsLoaded(CUIResources.PackageWnd) then
		CUIManager.CloseUI(CUIResources.PackageWnd)
	end
	CUIManager.ShowUI(CUIResources.BlowBubblesWnd)
end

g_OpenWndCallback["CommonPlayerListWnd2"] = function(wndname,data)
	local infosDict = MsgPackImpl.unpack(data)
	local list = MsgPackImpl.unpack(data)
	if list == nil or list.Count <= 0 then return end
	if list[0] == "QiXiInvite" then
		CLuaQiXiMgr.ShowInvitWnd()
	end
end

g_OpenWndCallback["DoubleOneBonusLotteryWnd"]=function(wndName,data)
  Gac2Gas.QueryJoinSinglesDayLotteryInfo()
end
g_OpenWndCallback["DoubleOneQLDShowWnd"]=function(wndName,data)
  Gac2Gas.RequestOpenQiLinDongPlayShop()
end
g_OpenWndCallback["YanChiXiaWorkWnd"]=function(wndName,data)
	Gac2Gas.RequestOpenHanJiaManualWnd()
end

g_OpenWndCallback["DuanWuDaZuoZhanRankWnd"]=function(wndName,data)
	CUIManager.ShowUI("DuanWuDaZuoZhanRankWnd")
end


g_OpenWndCallback["BQPDailyWnd"]=function(wndName,data)
	CUIManager.ShowUI(CUIResources.BingQiPuWnd)
end

g_OpenWndCallback["BQPCommitWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.BQPCommitEquipWnd)
end

g_OpenWndCallback["BQPResultWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.BQPResultWnd)
end

g_OpenWndCallback["ChildrenDayPlayShowWnd"]=function(wndName,data)
  Gac2Gas.QueryPengPengCheSignUpInfo()
end

g_OpenWndCallback["DanZhuMatchWnd"]=function(wndName,data)
  Gac2Gas.DanZhuQuerySignUpStatus()
end

g_OpenWndCallback["BaoZhuPlayWnd"]=function(wndName,data)
  LuaBaoZhuPlayMgr.OpenPlayWnd()
end

g_OpenWndCallback["QingMingCGYJMainWnd"]=function(wndName,data)
  Gac2Gas.QueryQingMing2021TuJianInfo()
end

g_OpenWndCallback["WorldEventVerShowWnd"]=function(wndName,data)
  if data then
    local dict = MsgPackImpl.unpack(data)
  	if not dict then return end
    local list = dict["args"]
    if not list then return end
    if list.Count >= 2 then
      LuaWorldEventMgr.HorShowTime = list[1]
    else
      LuaWorldEventMgr.HorShowTime = 0
    end
    LuaWorldEventMgr.VerShowPic = list[0]
  end
	CUIManager.ShowUI(CLuaUIResources.WorldEventVerShowWnd)
end

g_OpenWndCallback["WorldEventHorShowWnd"]=function(wndName,data)
  if data then
    local dict = MsgPackImpl.unpack(data)
  	if not dict then return end
    local list = dict["args"]
    if not list then return end
    LuaWorldEventMgr.HorShowPic = list[0]
  end
	CUIManager.ShowUI(CLuaUIResources.WorldEventHorShowWnd)
end

g_OpenWndCallback["WorldQiyishuWnd"]=function(wndName,data)
  CUIManager.ShowUI(CLuaUIResources.WorldQiyishuWnd)
end

g_OpenWndCallback["HuiGuiJieBanWnd"]=function(wndName,data)
	Gac2Gas.RequestOpenHuiGuiJieBanWnd()
end

g_OpenWndCallback["ShenbingZhuzaoWnd"] = function (wndName, data)
	CUIManager.ShowUI(CLuaUIResources.ShenBingZhuZaoWnd)
end

g_OpenWndCallback["ShenBingPeiYangWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ShenBingPeiYangWnd)
end

g_OpenWndCallback["ShenbingEnterWnd"] = function (wnd, data)
	Gac2Gas.RequestSBCSFinishInfo()
end
g_OpenWndCallback["CharacterCardWnd"] = function (wnd, data)
  Gac2Gas.RequestGetRfcCardSet()
end

g_OpenWndCallback["ShenBingExchangeQyWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ShenBingExchangeQyWnd)
end

g_OpenWndCallback["WuCaiShaBingEnterWnd"] = function(wnd, data)
	Gac2Gas.QueryWuCaiShaBingSignUpStatus()
end

g_OpenWndCallback["ShenBingRestoreWnd"] = function (wnd, data)
	LuaShenbingMgr:RestoreShenBing()
end

g_OpenWndCallback["MajiuWnd"] = function(wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list and list.Count > 0 then
		CZuoQiMgr.Inst.m_CurMajiuFurnitureId = tonumber(list[0])
		CUIManager.ShowUI(CLuaUIResources.MajiuWnd)
	end
end

g_OpenWndCallback["PlaceCityUnitWnd"] = function(wnd, data)
	local CCityWarMgr = import "L10.Game.CCityWarMgr"
	CCityWarMgr.Inst.PlaceMode = true

	local excepts = {"MiddleNoticeCenter", "PlaceCityUnitWnd"}
	local List_String = MakeGenericClass(List,cs_string)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EGameUI, Table2List(excepts, List_String), false, true)
	CUIManager.HideUIGroupByChangeLayer(EUIModuleGroup.EBgGameUI, Table2List(excepts, List_String), false, true)

	CUIManager.ShowUI(CLuaUIResources.PlaceCityUnitWnd)
end

g_OpenWndCallback["QMPKJingCaiWnd"] = function(wnd, data)
	local dict = MsgPackImpl.unpack(data)
	if not dict then return end
	local list = dict["args"]
	if list and list.Count==1 then
		local stage = tonumber(list[0])
		CLuaQMPKMgr.m_JingCaiPlayStage = stage
	end
	CUIManager.ShowUI(CLuaUIResources.QMPKJingCaiWnd)
end

g_OpenWndCallback["JiXiangWuTrainWnd"]=function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.QMPKJiXiangWuWnd)
end

g_OpenWndCallback["OpenChangeChampionStatueWnd"]=function(wnd,data)
	local list = MsgPackImpl.unpack(data)
	if list and list.Count > 2 then
		CLuaQMPKMgr.m_StatueFx = tonumber(list[0])
		CLuaQMPKMgr.m_StatueAniId = tonumber(list[1])
		CLuaQMPKMgr.m_StatuePreviewTime = tonumber(list[2])
	end
	CUIManager.ShowUI(CLuaUIResources.QMPKStatueBuildWnd)
end
--积分赛
g_OpenWndCallback["QMPKMatchRecordWnd"]=function(wnd,data)
	Gac2Gas.QueryQmpkWatchList()
end
--淘汰赛 总决赛
g_OpenWndCallback["QMPKTaoTaiSaiMatchRecordWnd"]=function(wnd,data)
    local dict = MsgPackImpl.unpack(data)
  	if not dict then return end
    local list = dict["args"]
	if list and list.Count==1 then
		local stage=tonumber(list[0])
		if stage==4 then
			CQuanMinPKMgr.Inst.m_WatchListWndType=EnumQMPKPlayStage.eTaoTaiSai
			CUIManager.ShowUI(CLuaUIResources.QMPKTaoTaiSaiMatchRecordWnd)
		elseif stage==5 then
			CQuanMinPKMgr.Inst.m_WatchListWndType=EnumQMPKPlayStage.eZongJueSai
			CUIManager.ShowUI(CLuaUIResources.QMPKTaoTaiSaiMatchRecordWnd)
		end
	end
end

g_OpenWndCallback["CarnivalCollectSubWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
end

g_OpenWndCallback["CarnivalStampBook"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CarnivalCollectWnd)
end

g_OpenWndCallback["CrossSXDDZWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CrossSXDDZWnd)
end

g_OpenWndCallback["CaiPiao2019Wnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CaiPiao2019Wnd)
end

g_OpenWndCallback["CarnivalCaiPiao2019Wnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CarnivalCaiPiao2019Wnd)
end


g_OpenWndCallback["QMPKStatuePopupMenu"]=function(wnd,data)
	local list = MsgPackImpl.unpack(data)
	if list and list.Count==1 then
		local engineId = tonumber(list[0])
		CQuanMinPKMgr.Inst.SelectedStatueEngineId = engineId
		CUIManager.ShowUI(CUIResources.QMPKStatuePopupMenu)
	end
end

g_OpenWndCallback["WelfareCarnivalTicketWnd"] = function (wnd, data)
	CWelfareMgr.OpenWelfareWnd(LocalString.GetString("倩女嘉年华"))
end

g_OpenWndCallback["HalloweenRescueWnd"] = function(wnd,data)
	CUIManager.ShowUI("HalloweenRescueWnd")
end

g_OpenWndCallback["ExchangeYuanBaoWithBaoShiWnd"] = function (wnd, data)
	local result = {}
	local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
	-- bag
    local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    local exist = false
    for i=1,bagSize do
    	local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
    	local item = CItemMgr.Inst:GetById(itemId)
    	if item and item.IsItem and item.IsBinded and item.Item.Type == EnumItemType_lua.Gem then
    		if CommonDefs.DictContains(GameSetting_Common_Wapper.BaoShiExchangeYuanBaoInfo, typeof(uint), item.TemplateId) then
    			exist = true
    			break
    		end
    	end
    end
    if not exist then
    	g_MessageMgr:ShowMessage("No_BaoShi_For_Exchange_YuanBao")
    else
		CUIManager.ShowUI(CLuaUIResources.ExchangeYuanBaoWithBaoShiWnd)
    end
end

g_OpenWndCallback["TreasureHouseWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.TreasureHouseWnd)
end

g_OpenWndCallback["GuildTerritorialWarGuideMap"] = function(wnd,data)
	LuaGuildTerritorialWarsMgr:ShowGuideMapWnd()
end
g_OpenWndCallback["GuildTerritorialWarsViewOccupyWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsViewOccupyWnd)
end
g_OpenWndCallback["GuildTerritorialWarsRankWnd"] = function(wnd,data)
	Gac2Gas.RequsetTerritoryWarRankInfo(1)
end
g_OpenWndCallback["GuildTerritorialWarsJieMengWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsJieMengWnd)
end
g_OpenWndCallback["GuildTerritorialWarsEnterWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsEnterWnd)
end
g_OpenWndCallback["GuildTerritorialWarsMapWnd"] = function(wnd,data)
	Gac2Gas.RequestTerritoryWarMapOverview()
end
g_OpenWndCallback["GuildTerritorialWarsSituationWnd"] = function(wnd,data)
	Gac2Gas.RequestTerritoryWarScoreData()
end
g_OpenWndCallback["GuildTerritorialWarsJingYingSettingWnd"] = function(wnd,data)
	Gac2Gas.RequestGTWChallengeMemberInfo(true)
end
g_OpenWndCallback["GuildExternalAidWnd"] = function(wnd,data)
	LuaGuildExternalAidMgr:ShowGuildExternalAidWnd()
end
g_OpenWndCallback["LingQiNumInputBox"] = function(wnd,data)
	Gac2Gas.TryExchangeGTWRelatedPlayCityMonsterHp()
end
g_OpenWndCallback["GuildExternalAidCardWnd"] = function(wnd,data)
	LuaGuildExternalAidMgr:ShowGuildExternalAidCardWnd()
end
g_OpenWndCallback["GuildTerritorialWarsBeiZhanWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsBeiZhanWnd)
end
g_OpenWndCallback["GuildTerritorialWarsZhanLongWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsZhanLongWnd)
end
g_OpenWndCallback["GuildTerritorialWarsZhanLongShopWnd"] = function(wnd,data)
	Gac2Gas.QueryGTWZhanLongShop()
end
g_OpenWndCallback["JuDianBattleJingYingSettingWnd"] = function(wnd,data)
	Gac2Gas.RequestGuildJuDianChallengeMemberInfo(true)
end
g_OpenWndCallback["guanningbattledatawnd"] = function(wnd,data)
    CLuaGuanNingMgr.ShowLastBattleData = false
	CLuaGuanNingMgr.m_BreakRecords = {}
	CUIManager.ShowUI(CUIResources.GuanNingBattleDataWnd)
end

g_OpenWndCallback["YuanDan2022XuYuanWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.YuanDan2022XuYuanWnd)
end

g_OpenWndCallback["YuanDan2022FuDanGamePlayWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YuanDan2022FuDanGamePlayWnd)
end

g_OpenWndCallback["ShenBingColorationWnd"] = function (wnd, data)
	if not LuaShenbingMgr:IsColorationOpen() then
		g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
		return
	end
	CUIManager.ShowUI(CLuaUIResources.ShenBingColorationWnd)
end

g_OpenWndCallback["BatDialogWnd"] = function (wnd,data)
	local list = MsgPackImpl.unpack(data)
	if list and list.Count >= 1 then
		if list.Count >= 2 then
			if list[1] == 1 then
				CLuaBatDialogWnd.PortraitName = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PortraitName or Constants.DefaultNPCPortrait
				CLuaBatDialogWnd.Name = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
			elseif list[1] == 2 then
				local member = CTeamMgr.Inst:GetMemberById(CTeamMgr.Inst.LeaderId)
				if member then
					CLuaBatDialogWnd.PortraitName = CUICommonDef.GetPortraitName(member.m_MemberClass, member.m_MemberGender, member.m_Expression) or Constants.DefaultNPCPortrait
					CLuaBatDialogWnd.Name = member.m_MemberName or ""
				else
					CLuaBatDialogWnd.PortraitName = Constants.DefaultNPCPortrait
					CLuaBatDialogWnd.Name = ""
				end
			else
				local member = CTeamMgr.Inst:GetMemberById(list[1])
				if member then
					CLuaBatDialogWnd.PortraitName = CUICommonDef.GetPortraitName(member.m_MemberClass, member.m_MemberGender, member.m_Expression) or Constants.DefaultNPCPortrait
					CLuaBatDialogWnd.Name = member.m_MemberName or ""
				else
					CLuaBatDialogWnd.PortraitName = Constants.DefaultNPCPortrait
					CLuaBatDialogWnd.Name = ""
				end
			end
		end
		CLuaBatDialogWnd.ShowWnd(list[0])
	end
end

g_OpenWndCallback["BaiGuiTuJian"] = function (wnd, data)
	if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.PlayProp then return end

	local list = MsgPackImpl.unpack(data)
	if list and list.Count == 1 then
		LuaShenbingMgr.m_CurrentTuJianId = list[0]
		CClientMainPlayer.Inst.PlayProp.BaiGuiTuJianData.TuJianProgress:SetBit(list[0], true)
	end
	CUIManager.ShowUI("BaiGuiTuJianWnd")
end
g_OpenWndCallback["ServerSubmitWnd"] = function(wnd,data)
	local list = MsgPackImpl.unpack(data)
	if list.Count==3 then
		local id = tonumber(list[0])
		local submitNum = tonumber(list[1])
		local totalNum = tonumber(list[2])
		CLuaServerSubmitWnd.s_Id = id
		CLuaServerSubmitWnd.s_SubmitNum = submitNum
		CLuaServerSubmitWnd.s_TotalNum = totalNum
		CUIManager.ShowUI(CLuaUIResources.ServerSubmitWnd)
	end
end

g_OpenWndCallback["FallingDownWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.FallingDownWnd)
end

g_OpenWndCallback["GuoQingJiaoChangSignUpWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list.Count == 1 then
		CLuaGuoQingJiaoChangMgr.ShowCommonRule(tonumber(list[0]))
	end
end

g_OpenWndCallback["CityConstructionRankWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CityConstructionRankWnd)
end

g_OpenWndCallback["GQJCRankWnd"] = function (wnd, data)
	CUIManager.ShowUI("GQJCRankWnd")
end

g_OpenWndCallback["GQJCStateWnd"] = function (wnd, data)
	CUIManager.ShowUI("GQJCStateWnd")
end

g_OpenWndCallback["PrenatalMessageWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.PrenatalMessageWnd)
end

g_OpenWndCallback["BabyNamingWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.BabyNamingWnd)
end

g_OpenWndCallback["CityWarGuildEnemyWnd"] = function ( ... )
	CUIManager.ShowUI(CLuaUIResources.CityWarGuildEnemyWnd)
end

g_OpenWndCallback["LianLianKanApplyWnd"] = function( wnd,data)
	CUIManager.ShowUI("LianLianKanApplyWnd")
end
g_OpenWndCallback["LianLianKanRankWnd"] = function( wnd,data)
	CUIManager.ShowUI("LianLianKanRankWnd")
end
g_OpenWndCallback["ZhongCaoFashionWnd"] = function( wnd,data)
	CUIManager.ShowUI("ZhongCaoFashionWnd")
end
g_OpenWndCallback["YanHuaLiBaoEditorWnd"] = function( wnd,data)
	--CUIManager.ShowUI(CLuaUIResources.YanHuaLiBaoEditorWnd)
	CLuaYanHuaEditorMgr.OpenYanHuaLiBaoEditor()
end
g_OpenWndCallback["HanHouTaoKouLingWnd"] = function( wnd,data)
	local list = MsgPackImpl.unpack(data)
	if list then
		local msg = tostring(list[0])
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			CUICommonDef.clipboardText = msg
		end),nil,LocalString.GetString("复制"),LocalString.GetString("取消"),false)
	end
end

g_OpenWndCallback["CityWarZiCaiShopWnd"] = function( wnd,data)
	CUIManager.ShowUI(CLuaUIResources.CityWarZiCaiShopWnd)
end

g_OpenWndCallback["CakeCuttingShowWnd"] = function( wnd,data)
	CUIManager.ShowUI(CLuaUIResources.CakeCuttingShowWnd)
end

g_OpenWndCallback["CakeCuttingRankWnd"] = function( wnd,data)
  Gac2Gas.CakeCuttingQueryRank()
end


g_OpenWndCallback["SubmitBiaoCheWnd"] = function(wnd,data)
	local list = MsgPackImpl.unpack(data)
	--local data = {npcEngineId, mapId, biaoCheStatus, biaoCheProgress, resourceId}
	if list.Count==6 then
		CLuaCityWarMgr.ShowBiaoCheSubmitWnd(tonumber(list[0]),tonumber(list[1]),tonumber(list[2]),tonumber(list[3]),tonumber(list[4]),tonumber(list[5]))
	end
end

g_OpenWndCallback["BabyWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.BabyWnd)
end


g_OpenWndCallback["CalendarARWnd"] = function(wnd,data)
	if Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor then
		g_MessageMgr:ShowMessage("AR_PC_NOT_SUPPORTED")
        return
    end
	if Main.Inst.EngineVersion > 0 and Main.Inst.EngineVersion <355934  then
		g_MessageMgr:ShowMessage("NEED_UPDATE_CLIENT")
        return
    end

	g_MessageMgr:ShowMessage("AR_NOTSUPPORTED")
	local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
	CommonDefs.DictAdd(dict, typeof(String), "AR_NOT_SUPPORTED", typeof(Object), SystemInfo.deviceModel)
	Gac2Gas2.RequestRecordClientLog("CALENDAR_AR", MsgPackImpl.pack(dict))
end

g_OpenWndCallback["ManYueJiuChooseBabyWnd"] = function(wnd, data)
	CLuaManYueJiuChooseBabyWnd.m_BabyInfo={}
	local list = MsgPackImpl.unpack(data)
	for i = 0, list.Count - 1, 6 do
		-- print(list[i], list[i+1])
		table.insert( CLuaManYueJiuChooseBabyWnd.m_BabyInfo,{
			babyId = tostring(list[i]),
			name = tostring(list[i+1]),
			status = tonumber(list[i+2]),
			grade = tonumber(list[i+3]),
			gender = tonumber(list[i+4]),
			hairColor = tonumber(list[i+5]),
		} )
	end
	CUIManager.ShowUI(CLuaUIResources.ManYueJiuChooseBabyWnd)
end

g_OpenWndCallback["CityWarMonsterSiegeSettingWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CityWarMonsterSiegeSettingWnd)
end

g_OpenWndCallback["ChristmasSendGiftWnd"] = function(wnd, data)
	CUIManager.ShowUI("ChristmasSendGiftWnd")
end

g_OpenWndCallback["ChristmasTreeBreedWnd"] = function(wnd, data)

	local dict = MsgPackImpl.unpack(data)
  	if not dict then return end
    local npcEngineId = tonumber(dict["npcEngineId"])
    LuaChristmasMgr.ShowChristmasTreeBreedWnd(npcEngineId)
end

g_OpenWndCallback["CityWarExchangeWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list.Count ~= 2 then return end
	CLuaCityWarMgr.CurExchangeCnt = list[0]
	CLuaCityWarMgr.MaxExchangeCnt = list[1]
	CUIManager.ShowUI(CLuaUIResources.CityWarExchangeWnd)
end

g_OpenWndCallback["DuanWu2022WuduEnterWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.DuanWu2022WuduEnterWnd)
end

g_OpenWndCallback["LiuYi2022PopoEnterWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.LiuYi2022PopoEnterWnd)
end

g_OpenWndCallback["ShuJia2022MainWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ShuJia2022MainWnd)
end

g_OpenWndCallback["ZhongQiu2022MainWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ZhongQiu2022MainWnd)
end

g_OpenWndCallback["JiaoYueDuoSuYiSignUpWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.JiaoYueDuoSuYiSignUpWnd)
end

g_OpenWndCallback["DuanWu2022MainWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.DuanWu2022MainWnd)
end

g_OpenWndCallback["CityWarGuildSearchWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CityWarGuildSearchWnd)
end

g_OpenWndCallback["ShiTuTrainingHandbookWnd"] = function (wnd, data)
	LuaShiTuTrainingHandbookMgr:OpenWnd(wnd, data)
end

g_OpenWndCallback["ShiTuMainWnd"] = function (wnd, data)
	LuaShiTuMgr:OpenShiTuMainWnd()
end

g_OpenWndCallback["ShouTuWaiMenDiZi"] = function (wnd, data)
	LuaShiTuMgr:ShouTuWaiMenDiZi()
end

g_OpenWndCallback["NewYearTaskBookWnd"] = function(wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list.Count ~= 5 then return end

	local itemId = list[0]
	local score = list[1]
  LuaNewYear2019Mgr.TaskItemId = itemId
  LuaNewYear2019Mgr.TaskScore = score


  LuaNewYear2019Mgr.TaskInfoTable = {}
	local taskInfo = list[2]
	for i = 0, taskInfo.Count - 1, 5 do
		local taskid = taskInfo[i]
		local level = taskInfo[i + 1]
		local value = taskInfo[i + 2]
		local finished = taskInfo[i + 3]
		local cansubmit = taskInfo[i + 4]

    table.insert(LuaNewYear2019Mgr.TaskInfoTable,{taskid, level, value, finished, cansubmit})
	end

  LuaNewYear2019Mgr.TaskRewardTable = {}
	local rewardInfo = list[3]
	for i = 0, rewardInfo.Count - 1, 3 do
		local rewardid = rewardInfo[i]
		local recved = rewardInfo[i + 1]
		local canRecv = rewardInfo[i + 2]

    table.insert(LuaNewYear2019Mgr.TaskRewardTable,{rewardid, recved, canRecv})
	end

	local showWnd = list[4] or CUIManager.IsLoaded("NewYear2019TaskBookWnd")
	if showWnd then
		CUIManager.ShowUI("NewYear2019TaskBookWnd")
	end
end

g_OpenWndCallback["NewYearLuckyDrawWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list.Count ~= 2 then return end
  LuaNewYear2019Mgr.PackItemId = list[0]
  LuaNewYear2019Mgr.SelectedItemId = list[1]
	CUIManager.ShowUI("NewYear2019DrawWnd")
end

g_OpenWndCallback["BianLunWnd"] = function (wnd, data)
  local list = MsgPackImpl.unpack(data)
  if list.Count ~= 1 then return end
  LuaDebateWithNpcMgr.TaskId = list[0]
  CUIManager.ShowUI(CLuaUIResources.DebateWithNpcWnd)
end

g_OpenWndCallback["QiQiaoBanWnd"] = function (wnd, data)
  CUIManager.ShowUI(CLuaUIResources.QiqiaobanWnd)
end

g_OpenWndCallback["SnowBallHisWnd"] = function (wnd, data)
  Gac2Gas.QueryXueQiuSeasonRankData()
end

g_OpenWndCallback["BabyQiChangJianDing"] = function (wnd, data)
	LuaBabyMgr.OpenQiChangJianDing()
end

g_OpenWndCallback["CityGuildRankWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CityGuildRankWnd)
end

g_OpenWndCallback["CityOccupyRankWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CityOccupyRankWnd)
end

g_OpenWndCallback["CityWarPrimaryMapWnd"] = function(wnd, data)
	CLuaCityWarMgr:OpenPrimaryMap()
end

g_OpenWndCallback["YeShengTuiLiWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YeShengTuiLiWnd)
end

g_OpenWndCallback["TianMenShanZhuLiSettingWnd"] = function (wnd,data)
	CUIManager.ShowUI(CLuaUIResources.TianMenShanZhuLiSettingWnd)
end

g_OpenWndCallback["ExchangeJieBanSkillItemWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ExchangeJieBanSkillItemWnd)
end
g_OpenWndCallback["ExchangeJieBanSkillItemWnd2"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ExchangeJieBanSkillItemWnd2)
end

g_OpenWndCallback["BabyXiLianWnd"] = function (wnd, data)
	local dict = MsgPackImpl.unpack(data)
	local list = dict["args"]
	if list and list.Count == 1 then
		local index = tonumber(list[0])
  		LuaBabyMgr.OpenBabyXiLian(index)
	end
end

g_OpenWndCallback["NewYear2019CunQianGuanWnd"] = function (wnd, data)
	CUIManager.ShowUI("NewYear2019CunQianGuanWnd")
end
g_OpenWndCallback["NewYear2019NianFireworkConfirmWnd"] = function (wnd, data)
	CUIManager.ShowUI("NewYear2019NianFireworkConfirmWnd")
end
g_OpenWndCallback["NewYear2019NianRewardExchangeWnd"] = function (wnd, data)
	CUIManager.ShowUI("NewYear2019NianRewardExchangeWnd")
end
g_OpenWndCallback["NewYear2019OpenFireworkWnd"] = function (wnd, data)
	local dict = MsgPackImpl.unpack(data)
	local list = dict["args"]
	if list and list.Count == 1 then
		local index = tonumber(list[0])
		local msg = nil
		if index==1 then
			msg = g_MessageMgr:FormatMessage("NewYear2019_Open_FireWork1")
		elseif index==2 then
			msg = g_MessageMgr:FormatMessage("NewYear2019_Open_FireWork2")
		elseif index==3 then
			msg = g_MessageMgr:FormatMessage("NewYear2019_Open_FireWork3")
		end
		MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			Gac2Gas.RequestFireFireworks(index)
		end),nil,nil,nil,false)
	end
end

g_OpenWndCallback["PreciousItemAlertWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list and list.Count > 0 then
		CLuaPreciousItemAlertWnd.ItemTemplateIds = {}
		for i=1,list.Count do
			table.insert( CLuaPreciousItemAlertWnd.ItemTemplateIds, tonumber(list[i-1]))
		end
		CUIManager.ShowUI(CLuaUIResources.PreciousItemAlertWnd)
	end
end

g_OpenWndCallback["MengHuaLuStart"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.MengHuaLuStartWnd)
end

g_OpenWndCallback["QiXiFireworkEntryWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QiXiFireworkEntryWnd)
end

g_OpenWndCallback["YuanXiaoRiddleCandidateWnd"] = function(wnd, data)
	local list = MsgPackImpl.unpack(data)
	if not (list and list.Count > 1) then return end

	local npcEngineId = list[0]
	local candidates = {}
	for i = 1, list.Count - 1 do
		table.insert(candidates, list[i])
	end

  LuaYuanxiaoMgr.npcEngineId = npcEngineId
  LuaYuanxiaoMgr.chooseTable = candidates
	--Gac2Gas.ConfirmCommitRiddle(npcEngineId, candidates[1])
  CUIManager.ShowUI("YuanxiaoChoosePuzzleWnd")
end

g_OpenWndCallback["QYSJMemoirsWnd"] = function (wnd, data)
	local infoDict = MsgPackImpl.unpack(data)

	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		LuaValentine2019Mgr.ShowQingYuanShouJiMemoirsWnd(itemId)
	end

end

g_OpenWndCallback["PreExistenceMarridgeAlbumWnd"] = function(wnd, data)
	if CClientMainPlayer.Inst then
		LuaValentine2019Mgr.ShowPreExistenceMarridgeAlbumWnd(true, nil, CClientMainPlayer.Inst.Id, CClientMainPlayer.Inst.BasicProp.Name)
	end
end

g_OpenWndCallback["XueQiuWnd"] = function(wnd, data)
  CUIManager.ShowUI(CUIResources.SnowBallShowWnd)
end

g_OpenWndCallback["TangYuanPlayEndWnd"] = function(wnd, data)
	local list = MsgPackImpl.unpack(data)
	if not (list and list.Count > 1) then return end

	local makeCount = list[0]
	local totalScore = list[1]
	LuaYuanxiaoMgr:ShowResultWnd(makeCount, totalScore)
end

g_OpenWndCallback["EquipKeZiWnd"] = function(wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local itemInfo=CItemMgr.Inst:GetItemInfo(g_PackageViewItemId)
	if not itemInfo then return end

	CLuaEquipKeZiWnd.m_TemplateId = itemInfo.templateId
	local args = raw["args"]
	local list = args[0]
	local t = {}
	for i=1,list.Count do
		t[tonumber(list[i-1])] =true
	end
	CLuaEquipKeZiWnd.m_FilterEquipTypes = t
	CUIManager.ShowUI(CLuaUIResources.EquipKeZiWnd)
end

g_OpenWndCallback["LingShouSwitchJieBanSkillWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.LingShouSwitchJieBanSkillWnd)
end

g_OpenWndCallback["StarBiwuRenameTeam"] = function(wnd, data)
  local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
  local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

  CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入新的战队名称"), DelegateFactory.Action_string(function (val)
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(val, true)
  	if (not ret.msg) or ret.shouldBeIgnore then return end
    Gac2Gas.StarBiwuRenameTeam(ret.msg)
  end), 20, true, nil, LocalString.GetString("请输入战队名（不超过五个字）"))
end

g_OpenWndCallback["StarBiWuInviteAudience"] = function(wnd, data)
  local argvs = MsgPackImpl.unpack(data)
  if not argvs or argvs.Count ~= 4 then return end
  local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
  CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入亲友团成员的角色ID"), DelegateFactory.Action_string(function (val)
    local playerId = tonumber(val)
    if playerId then
      Gac2Gas.StarBiwuInviteAudience(playerId, argvs[0], argvs[1], argvs[2], argvs[3])
    else
      g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请输入正确的角色ID"))
    end
  end), 20, true, nil, LocalString.GetString("请输入角色ID"))
end

g_OpenWndCallback["FengHuaLuWnd"]=function(wnd, data)
	LuaWorldEventMgr2021.ShowFengHuaLu()
end

g_OpenWndCallback["StarBiwuFinalWatchWnd"] = function(wnd, data)
	CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(1)
end

g_OpenWndCallback["StarBiwuZhanDuiSearchWnd"] = function(wnd, data)
	local argvs = MsgPackImpl.unpack(data)
	CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(0)
end

g_OpenWndCallback["StarBiwuSelfZhanDuiWnd"] = function(wnd, data)
	local argvs = MsgPackImpl.unpack(data)
	CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(1)
end

g_OpenWndCallback["StarBiwuAgendaWnd"] = function(wnd, data)
	local argvs = MsgPackImpl.unpack(data)
	CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(3)
end

g_OpenWndCallback["StarBiwuRewardWnd"] = function(wnd, data)
	local argvs = MsgPackImpl.unpack(data)
  	CUIManager.ShowUI(CLuaUIResources.StarBiwuRewardWnd)
end

g_OpenWndCallback["StarBiwuQieCuoWnd"] = function(wnd, data)
  CUIManager.ShowUI(CLuaUIResources.StarBiwuQieCuoWnd)
end

g_OpenWndCallback["StarBiwuChampionHistoryWnd"] = function(wnd, data)
  CUIManager.ShowUI(CLuaUIResources.StarBiwuChampionHistoryWnd)
end

g_OpenWndCallback["StarBiwuShowWatch"] = function(wnd, data)
  local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
  CBiWuDaHuiMgr.Inst.QMJJWatch = false;
  CUIManager.ShowUI(CUIResources.BWDHWatchWnd)
end

g_OpenWndCallback["StarBiwuXiaoZuSai"] = function(wnd, data)
  CUIManager.ShowUI(CLuaUIResources.StarBiwuGroupMatchWnd)
end

g_OpenWndCallback["StarBiwuZJSOverview"] = function(wnd, data)
  CUIManager.ShowUI(CLuaUIResources.StarBiwuTopFourWnd)
end

g_OpenWndCallback["StarBiwuFinalMatchRecordWnd"] = function(wnd, data)
  CUIManager.ShowUI(CLuaUIResources.StarBiwuFinalMatchRecordWnd)
end

g_OpenWndCallback["StarBiwuZhanDuiPlatformWnd"] = function(wnd, data)
	local dict = MsgPackImpl.unpack(data)
	local args = dict and dict.Count >= 1 and dict["args"] or nil
	if args and args.Count >= 1 then
		CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(tonumber(args[0]))
	else
		CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(0)
	end
end

g_OpenWndCallback["StarBiwuDetailInfoWnd"] = function(wnd, data)
	local dict = MsgPackImpl.unpack(data)
	local args = dict and dict.Count >= 1 and dict["args"] or nil
	if args and args.Count >= 1 then
		CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(tonumber(args[0]))
	else
		CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(0)
	end
end

g_OpenWndCallback["NpcFightingWnd"] = function(wnd, data)
	local tbl = MsgPackImpl.unpack(data)
	CLuaNpcFightingWnd.NpcId1 = tbl[0]
	CLuaNpcFightingWnd.NpcId2 = tbl[1]
    CLuaNpcFightingWnd.bNotShowButton = tbl[2] == 1
	CUIManager.ShowUI(CLuaUIResources.NpcFightingWnd)
end

g_OpenWndCallback["XianZhiWnd"] = function(wnd, data)
	CLuaXianzhiMgr.OpenXianZhiWnd()
end

g_OpenWndCallback["XianZhiPu"] = function(wnd, data)
	Gac2Gas.RequestXianZhiPuInfo()
end

--灵兽认亲
g_OpenWndCallback["LingShouRenQinWnd"] = function(wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if raw == nil then return end
	LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel = raw[0]
	CUIManager.ShowUI(CLuaUIResources.LingShouRenQinWnd)
end

--额外属性
g_OpenWndCallback["ExtraPropertyWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ExtraPropertyWnd)
end
g_OpenWndCallback["WashExtraPropertyWnd"] = function(wnd, data)
	LuaExtraPropertyMgr.m_WashExtraPropertyWndType = 1
	local raw = MsgPackImpl.unpack(data)
	if raw then 
		local args = raw['args']
		if args and args.Length == 1 then
			local param1 = tonumber(args[0])
			if param1 == 2 then
				LuaExtraPropertyMgr.m_WashExtraPropertyWndType = 2
			end
		end
	end
	CUIManager.ShowUI(CLuaUIResources.WashExtraPropertyWnd)
end

g_OpenWndCallback["BackBonusWnd"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if raw ==nil then return end
  local args = raw['args']
  local methodName = args[0]
  local param1 = args[1]
	if methodName=="show_wnd" then
    local itemId = tonumber(param1)
    LuaBackBonusMgr.ShowItemId = itemId
    CUIManager.ShowUI(CLuaUIResources.BackBonusWnd)
	end
end

g_OpenWndCallback["XianzhiChooseImageTaskWnd"] = function( wnd,data )
	local raw = MsgPackImpl.unpack(data)
	if raw ==nil then return end
	local args = raw["args"]
	if args == nil then return end
	CLuaXianzhiMgr.OpenXianzhiChooseImageTaskWnd(args[0])
end

g_OpenWndCallback["LiuYiFamilyTimeWnd"] = function(wnd, data)
  local list = MsgPackImpl.unpack(data)
  if list.Count ~= 6 then return end

  local itemId = list[0]
  local score = list[1]
  LuaChildrenDay2019Mgr.TaskItemId = itemId
  LuaChildrenDay2019Mgr.score = score

  LuaChildrenDay2019Mgr.TaskInfoTable = {}
  local taskInfo = list[2]
  for i = 0, taskInfo.Count - 1, 5 do
    local _table = {}
    _table.taskid = tonumber(taskInfo[i])
    _table.level = tonumber(taskInfo[i + 1])
    _table.value = tonumber(taskInfo[i + 2])
    _table.finished = tonumber(taskInfo[i + 3])
    _table.cansubmit = tonumber(taskInfo[i + 4])
    table.insert(LuaChildrenDay2019Mgr.TaskInfoTable,_table)
  end

  LuaChildrenDay2019Mgr.TaskRewardTable = {}
  local rewardInfo = list[3]
  for i = 0, rewardInfo.Count - 1, 3 do
    local _table = {}
    _table.rewardid = tonumber(rewardInfo[i])
    _table.recved = tonumber(rewardInfo[i + 1])
    _table.canRecv = tonumber(rewardInfo[i + 2])
    table.insert(LuaChildrenDay2019Mgr.TaskRewardTable,_table)
  end

  local appearanceInfo = list[5]
  local status = tonumber(appearanceInfo[0])
  if status == 0 then
    -- is npc
    LuaChildrenDay2019Mgr.ShowBabyInfo = nil
  else
    local gender = tonumber(appearanceInfo[1])
    local hairColor = tonumber(appearanceInfo[2])
    local skinColor = tonumber(appearanceInfo[3])
    local bodyWeight = tonumber(appearanceInfo[4])
    local hairStyle = tonumber(appearanceInfo[5])
    local fashionHeadId = tonumber(appearanceInfo[6])
    local fashionBodyId = tonumber(appearanceInfo[7])
    local colorId = tonumber(appearanceInfo[8])
    local wingId = tonumber(appearanceInfo[9])
    local qiChangId = tonumber(appearanceInfo[10])
    if not LuaChildrenDay2019Mgr.ShowBabyInfo then
      LuaChildrenDay2019Mgr.ShowBabyInfo = {}
    end
    LuaChildrenDay2019Mgr.ShowBabyInfo.status = status
    LuaChildrenDay2019Mgr.ShowBabyInfo.gender = gender
    LuaChildrenDay2019Mgr.ShowBabyInfo.hairColor = hairColor
    LuaChildrenDay2019Mgr.ShowBabyInfo.skinColor = skinColor
    LuaChildrenDay2019Mgr.ShowBabyInfo.hairstyle = hairStyle
    LuaChildrenDay2019Mgr.ShowBabyInfo.headId = fashionHeadId
    LuaChildrenDay2019Mgr.ShowBabyInfo.bodyId = fashionBodyId
    LuaChildrenDay2019Mgr.ShowBabyInfo.colorId = colorId
    LuaChildrenDay2019Mgr.ShowBabyInfo.wingId = wingId
    LuaChildrenDay2019Mgr.ShowBabyInfo.qiChangId = qiChangId

  end

  CUIManager.ShowUI(CLuaUIResources.ChildrenDay2019TaskBookWnd)
end

g_OpenWndCallback["WaKuangRankWnd"] = function (wnd, data)
	CUIManager.ShowUI("WaKuangRankWnd")
end

g_OpenWndCallback["FengXianDaHuiConfirm"] = function (wnd, data)
	CLuaXianzhiMgr.FengXianDaHuiConform()
end

g_OpenWndCallback["TingFengWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list.Count < 3 then return end

	local playerName = list[0]
	local xianzhiName = list[1]
	local skillName = list[2]
	CLuaXianzhiMgr.OpenTingFengWnd(playerName, xianzhiName, skillName)
end

g_OpenWndCallback["TriggerButtonGuideWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	if list.Count < 1 then return end

	local guideId = list[0]
	if not guideId then return end

	-- print(guideId)
	CGuideMgr.Inst:StartGuide(EnumGuideKey.KillDaWu, 0)
end

--端午五色丝合成界面
g_OpenWndCallback["DwWusesiHeChengWnd"]= function(wnd, data)
	CLuaDwMgr2019.ShowDwWusesiHeChengWnd()
end

--端午五色丝拓本界面
g_OpenWndCallback["DwWusesiTabenWnd"]= function(wnd, data)
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		CLuaDwMgr2019.ShowDwWusesiTabenWnd(itemId)
	end
end

g_OpenWndCallback["DwZongziRuleWnd"] = function(wnd, data)
	CLuaDwMgr2019.ShowDwZongziRuleWnd()
end

g_OpenWndCallback["FloweWithBloodEffectWnd"] = function(wnd, data)
	-- OpenWnd({"FloweWithBloodEffectWnd",taskId,subtitleId})
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	local subtitleId = tonumber(args[1])
	CLuaTaskMgr.ShowFlowerWithBloodEffectWnd(wnd, taskId, subtitleId)
end


g_OpenWndCallback["LeafWithMemoryWnd"] = function(wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	local fadeAlphaTime = tonumber(args[1])
	local colorId = tonumber(args[2])
	CLuaTaskMgr.ShowLeafWithMemoryWnd(wnd, taskId, fadeAlphaTime, colorId)
end

g_OpenWndCallback["DriveAwayGhostWnd"] = function(wnd, data)
	--OpenWnd({"DriveAwayGhostWnd",taskId,hitCount, totalSeconds})
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	local needCount = tonumber(args[1])
	local totalSeconds = tonumber(args[2])

	CLuaTaskMgr.ShowDriveAwayGhostWnd(wnd, taskId, needCount, totalSeconds)
end

g_OpenWndCallback["ShowPictureWnd"]=function(wndName,data)
    if data then
        local dict = MsgPackImpl.unpack(data)
        if not dict then return end
        local list = dict["args"]
				if list and list.Count==3 then
					CLuaShowPictureWnd.m_MatPath = tostring(list[0])
					CLuaShowPictureWnd.m_Width = tonumber(list[1])
					CLuaShowPictureWnd.m_Height = tonumber(list[2])
					CUIManager.ShowUI(CLuaUIResources.ShowPictureWnd)
				end
    end
end
g_OpenWndCallback["FlowerWitheredEffectWnd"] = function(wnd, data)
	--OpenWnd({"FlowerWitheredEffectWnd",taskId,pressDuration})
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	local pressDuration = tonumber(args[1])

	CLuaTaskMgr.ShowFlowerWitheredEffectWnd(wnd, taskId, pressDuration)
end

g_OpenWndCallback["FamilyportraitAddress"] = function(wnd, data)
	--OpenWnd({"FlowerWitheredEffectWnd",taskId,pressDuration})
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
  LuaCharacterCardMgr.ShareBigPicUrl = args[0]

  CUIManager.ShowUI(CLuaUIResources.CharacterCardDetailWnd)
end

g_OpenWndCallback["TalismanFxExchangeWnd"] = function(wnd, data)
	CLuaTalismanFxExchangeWnd.s_TalismanFxId = 0
	CUIManager.ShowUI(CLuaUIResources.TalismanFxExchangeWnd)
end

g_OpenWndCallback["TalismanFxReturnWnd"] = function(wnd, data)
  if LuaTalismanMgr:CheckTalismanReturnOpen() then
    CUIManager.ShowUI(CLuaUIResources.TalismanFxReturnWnd)
  else
    g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('没有满足条件的仙家法宝外观可以回退'))
  end
end

g_OpenWndCallback["BuffItemWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.BuffItemWnd)
end

g_OpenWndCallback["QiYuanWnd"] = function (wnd, data)
	CUIManager.ShowUI(CUIResources.QiYuanWnd)
end

g_OpenWndCallback["FeiShengRuleConfirmWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.FeiShengRuleConfirmWnd)
end

g_OpenWndCallback["CageEffectWnd"] = function(wnd,data)
	CUIManager.ShowUI("CageEffectWnd")
end

g_OpenWndCallback["QiXiLiuXingQiYuanWnd"] = function(wnd, data)
	CUIManager.ShowUI("QiXiLiuXingQiYuanWnd")
end

g_OpenWndCallback["ShowQuickUseItemInfo"] = function(wnd, data)
	local list = MsgPackImpl.unpack(data)
	if not list then
		return
	end

	local itemTemplateId = tonumber(list[0])
	local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, itemTemplateId)
	if not (default and pos and itemId) then
		return
	end

	CItemInfoMgr.taskTriggeredTaskItemId = itemId
	CItemInfoMgr.taskTriggeredTaskId = 0
	CItemInfoMgr.quickUseTemplateID = itemTemplateId
	CUIManager.ShowUI(CIndirectUIResources.ItemUsageWnd)
end

g_OpenWndCallback["ZQYueShenDianDescWnd"] = function (wnd,data)
	CUIManager.ShowUI(CLuaUIResources.ZQYueShenDianDescWnd)
end

g_OpenWndCallback["ZQYaoBanShangYueWnd"] = function (wnd,data)
	CUIManager.ShowUI(CLuaUIResources.ZQYaoBanShangYueWnd)
end

g_OpenWndCallback["FallingDownTreasure"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	LuaZhuJueJuQingMgr.OpenFallingDownTreasure(taskId)
end

g_OpenWndCallback["Lumbering"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	LuaZhuJueJuQingMgr.OpenLumberingWnd(taskId)
end

g_OpenWndCallback["BoilMedicine"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	LuaZhuJueJuQingMgr.OpenBoilMedicineWnd(taskId)
end

g_OpenWndCallback["BabyWndTab"] = function (wnd, data)
	local args =  MsgPackImpl.unpack(data)
	if not args or args.Count <= 0 then return end
	LuaBabyMgr.OpenBabyWnd(1, args[0])
end

g_OpenWndCallback["chasesteps"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	LuaZhuJueJuQingMgr:ShowFootStepPlayWnd(taskId)
end

g_OpenWndCallback["shanzhenmeiwei"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	LuaZhuJueJuQingMgr:ShowShaoKaoPlayWnd(taskId)
end

g_OpenWndCallback["zuanmuquhuo"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	LuaZhuJueJuQingMgr:ShowMakeFirePlayWnd(taskId)
end



g_OpenWndCallback["ZhuShaBiShengXingWnd"] = function(wnd,data)
	CLuaZhuShaBiXiLianMgr.ShowZhuShaBiShengXingWnd()
end

g_OpenWndCallback["NationalDayTXZREnterWnd"] = function(wnd,data)
	LuaNationalDayMgr.OpenTXZREnterWnd()
end

g_OpenWndCallback["CookBookWnd"] = function(wnd,data)
	local list = MsgPackImpl.unpack(data)
	if list and CommonDefs.IsDic(list) then
		if  CommonDefs.DictContains_LuaCall(list, "args") then
			list = list["args"]
			if list and list.Count >= 1 then
				LuaCookBookMgr.m_DefaultCookbookId = tonumber(list[0])
			end
		end
	else
		if list and list.Count >= 1 then
			LuaCookBookMgr.m_DefaultCookbookId = tonumber(list[0])
		end
	end
	Gac2Gas.ShangShiCookbookData()
end


g_OpenWndCallback["PuSongLingHandWriteEffect"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	if not args then return end
	local taskId = tonumber(args[0])
	LuaZhuJueJuQingMgr.OpenPuSongLingHandWriteEffect(taskId)
end

g_OpenWndCallback["OtherBabyInfoWnd"] = function(wnd, data)
	local args = MsgPackImpl.unpack(data)
	if not args then
		return
	end

	CLuaOtherBabyInfoWnd.m_PlayerId = tonumber(args[0])
	CLuaOtherBabyInfoWnd.m_BabyId = args[1]
	local babyInfoList = args[2]
	if babyInfoList then
		local showInfo = {}
		local showInfoList = babyInfoList[7]
		if showInfoList and showInfoList.Count > 0 then
			showInfo = {
				status = tonumber(showInfoList[0]),
				gender = tonumber(showInfoList[1]),
				name = showInfoList[2],
				hairColor = tonumber(showInfoList[3]),
				skinColor = tonumber(showInfoList[4]),
				bodyWeight = tonumber(showInfoList[5]),
				hairstyle = tonumber(showInfoList[6]),
				headId = tonumber(showInfoList[7]),
				bodyId = tonumber(showInfoList[8]),
				colorId = tonumber(showInfoList[9]),
				backId = tonumber(showInfoList[10]),
				grade = tonumber(showInfoList[11]),
				showQiChangId = tonumber(showInfoList[12]),
			}
		end

		local propInfo = {}
		local propInfoList = babyInfoList[8]
		if propInfoList and propInfoList.Count > 0 then
			for i = 1, propInfoList.Count, 2 do
				table.insert(propInfo, {
					propId = propInfoList[i - 1],
					propNum = propInfoList[i]
				})
			end
		end
		CLuaOtherBabyInfoWnd.m_BabyInfo = {
			tonumber(babyInfoList[0]),
			tonumber(babyInfoList[1]),
			tonumber(babyInfoList[2]),
			babyInfoList[3],
			tonumber(babyInfoList[4]),
			tonumber(babyInfoList[5]),
			tonumber(babyInfoList[6]),
			showInfo,
			propInfo,
		}
	end
	CUIManager.ShowUI(CLuaUIResources.OtherBabyInfoWnd)
end

g_OpenWndCallback["PlayFluteWnd"]=function(wndName,data)
	local args = MsgPackImpl.unpack(data)
	local fluteId = args and args[0]
	if not fluteId then return end

	LuaDiZiWnd.fluteId  = fluteId
	LuaDiZiWnd.taskId = 0
	CUIManager.ShowUI(CLuaUIResources.DiZiWnd)
end

g_OpenWndCallback["LongPressScreenWnd"] = function(wndName, data)
	local args = MsgPackImpl.unpack(data)
	if not (args and args.Count >= 4) then return end

	local key, x, y, tip = args[0], args[1], args[2], args[3]
	-- Gac2Gas.FinishLongPressScreen(key)
end

g_OpenWndCallback["PictureTransitionWnd"] = function(wndName, data)
	local args = MsgPackImpl.unpack(data)
	if not (args and args.Count >= 1) then return end

	local key = args[0]
end

g_OpenWndCallback["ChristmasDisplayWnd"] = function (wnd, data)
	local player = CClientMainPlayer.Inst
	if player then
		local level = player.Level
		if player.HasFeiSheng then
			level = player.XianShenLevel
		end
		if level >= 50 then
			CUIManager.ShowUI("ChristmasDisplayWnd")
		else
			g_MessageMgr:ShowMessage("BAGUALU_GRADELIMIT",50)
		end
	end
end

g_OpenWndCallback["HanJiaManualWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YanChiXiaWorkWnd)
end

g_OpenWndCallback["SubmitSnowballWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	local furnitureId, currentCount, maxCount, currentStage = list[0], list[1], list[2], list[3]
	HanJia2020Mgr:RequestInteractWithUnfinishedSnowMan(furnitureId, currentCount, maxCount, currentStage)
end

g_OpenWndCallback["DecorateSnowmanWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	local furnitureId = list[0]
	HanJia2020Mgr:RequestInteractWithfinishedSnowMan(furnitureId)
end

g_OpenWndCallback["SchoolContributionWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.SchoolContributionWnd)
end

--新手师门课业选择
g_OpenWndCallback["NewbieSchoolSelectWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	local taskId, stage, remainCredit, needCredit, eachCreditInfo = list[0], list[1], list[2], list[3], list[4]
	if eachCreditInfo.Count~=3 then return end
	local travelCredit, fightCredit, miscCredit = eachCreditInfo[0], eachCreditInfo[1], eachCreditInfo[2]
	LuaSchoolTaskMgr:ShowOptionWnd(taskId, stage, remainCredit, needCredit, travelCredit, fightCredit, miscCredit)
end
--新手师门课业成绩单
g_OpenWndCallback["NewbieSchoolReportWnd"] = function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	local daydiff, eachCreditInfo, wishContent = list[0], list[1], list[2]
	if eachCreditInfo.Count~=3 then return end
	local travelCredit, fightCredit, miscCredit = eachCreditInfo[0], eachCreditInfo[1], eachCreditInfo[2]
	LuaSchoolTaskMgr:ShowTranscriptWnd(daydiff, travelCredit, fightCredit, miscCredit, wishContent)
end
--新手师门许愿
g_OpenWndCallback["NewbieSchoolWishingWnd"] = function (wnd, data)
	LuaSchoolTaskMgr:ShowWishingWnd()
end

g_OpenWndCallback["CrossDouDiZhuApplyWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuApplyWnd)
end
g_OpenWndCallback["CrossDouDiZhuRankWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuRankWnd)
end
g_OpenWndCallback["CrossDouDiZhuVoteWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuVoteWnd)
end
g_OpenWndCallback["ChunJieSeriesTaskWnd"] = function (wnd, data)
	CLuaCrossDouDiZhuRankWnd.m_IsAllRank = true
	CUIManager.ShowUI(CLuaUIResources.ChunJieSeriesTaskWnd)
end

g_OpenWndCallback["QYXTSignUpWnd"] = function (wnd, data)
	local player = CClientMainPlayer.Inst
	local needGrade = Valentine2020_Setting.GetData().NeedGrade
	if player then
		local level = player.Level
		if player.HasFeiSheng then
			level = player.XianShenLevel
		end
		if level >= needGrade then
			CLuaQYXTMgr.ForceOpen = true
			Gac2Gas.QueryQingYiXiangTongPlaySignUp()
		else
			g_MessageMgr:ShowMessage("BAGUALU_GRADELIMIT",needGrade)
		end
	end
end

g_OpenWndCallback["QYXTReLiveWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QYXTReliveWnd)
end

g_OpenWndCallback["DiYuShiKongWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.DiYuShiKongWnd)
end

g_OpenWndCallback["YuanXiaoLanternSendWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2020QiYuanWnd)
end

g_OpenWndCallback["DuiDuiLeStartWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.DuiDuiLeStartWnd)
end

g_OpenWndCallback["TieChunLianRankWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.TieChunLianRankWnd)
end

g_OpenWndCallback["HuanJingMiZongWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QingMingSignRankWnd)
end

g_OpenWndCallback["JieZiXiaoChouWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QingMingJieZiXiaoChouWnd)
end

g_OpenWndCallback["JieZiSucceedWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QingMingJieZiSucceedWnd)
end

g_OpenWndCallback["QingMingFengWuCollectWnd"]=function(wndName,data)
	if data then
		local argvs = MsgPackImpl.unpack(data)
		if argvs and argvs.Count > 0 then
			CLuaQingMing2020Mgr.CollectTaskId = argvs[0]
		end
	end
	CUIManager.ShowUI(CLuaUIResources.QingMingFengWuZhiWnd)
end

g_OpenWndCallback["AccountTransferWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.AccountTransferWnd)
end

g_OpenWndCallback["XYLPWnd"] = function(wnd, data)
	ZhongYuanJie2020Mgr:OpenXYLPWnd()
end

g_OpenWndCallback["DesireMirrorWnd"] = function(wnd, data)
	ZhongYuanJie2020Mgr:OpenDesireMirrorWnd()
end

g_OpenWndCallback["StarBiwuEventReviewWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.StarBiwuEventReviewWnd)
end
--端午五色丝合成界面
g_OpenWndCallback["DwWusesiHeCheng2020Wnd"]= function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.DwWusesiHeCheng2020Wnd)
end

--端午五色丝拓本界面
g_OpenWndCallback["DwWusesiTaben2020Wnd"]= function(wnd, data)
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		CLuaDwWusesiTaben2020Wnd.OriID = itemId
    	CUIManager.ShowUI(CLuaUIResources.DwWusesiTaben2020Wnd)
	end
end

--通用4合一合成界面
g_OpenWndCallback["ItemExchange4To1Wnd"]= function(wnd, data)
	local list = MsgPackImpl.unpack(data)
	local list = list['args']
	if list and list.Count < 1 then return end
	g_ComposeMgr.m_ExchangeId = tonumber(list[0])
	if list.Count == 3 then
		g_ComposeMgr.m_ComposeLackOfItemMsg = list[1]
		g_ComposeMgr.m_ComposeSuccessMsg = list[2]
	end
	local data = Exchange_Exchange.GetData(g_ComposeMgr.m_ExchangeId)
	if data and data.ConsumeItems.Length == 4 then
		CUIManager.ShowUI(CLuaUIResources.ItemExchange4To1Wnd)
	end
end

-- 周年庆PVP
g_OpenWndCallback["PUBGStartWnd"]= function(wnd, data)
	Gac2Gas.QueryChiJiApplyInfo()
end

-- 白蝶风波
g_OpenWndCallback["ButterflyCrisisMainWnd"] = function (wnd, data)
	-- 默认打开第一个NPC
	Gac2Gas.ButterflyGetUIStatus()
end


--NPC选美
g_OpenWndCallback["NPCXuanMeiWnd"] = function(wnd, data)
	if data then
		local argvs = MsgPackImpl.unpack(data)
		if argvs and argvs.Count > 0 then
			CLuaNPCXuanMeiMgr.CandidateNpcIds = {}
			CLuaNPCXuanMeiMgr.CandidateId2Vote = {}
			CLuaNPCXuanMeiMgr.IsPickNpcFinish = argvs[0]
			CLuaNPCXuanMeiMgr.CurMaxVoteTimes = argvs[1]

			for i=2,argvs.Count-2,2 do
				local npcId = argvs[i]
				local vote = argvs[i+1]
				table.insert(CLuaNPCXuanMeiMgr.CandidateNpcIds,argvs[i])
				CLuaNPCXuanMeiMgr.CandidateId2Vote[npcId] = vote
			end
		end
		CUIManager.ShowUI(CLuaUIResources.NPCXuanMeiWnd)
	end
end

g_OpenWndCallback["HouseCompetitionFanPaiZiWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.HouseCompetitionFanPaiZiWnd)
end

--代言人
g_OpenWndCallback["SpokesmanExpressionWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.SpokesmanExpressionWnd)
end

g_OpenWndCallback["SpokesmanPawnshopWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.SpokesmanPawnshopWnd)
end

g_OpenWndCallback["SpokesmanPawnCertificateWnd"] = function (wnd, data)
	local raw = MsgPackImpl.unpack(data)
	if not raw then return end
	local args = raw["args"]
	local ItemId = args[0]
	if ItemId == nil  then return end
	CLuaSpokesmanMgr.currentItemId = ItemId
	CUIManager.ShowUI(CLuaUIResources.SpokesmanPawnCertificateWnd)
end

g_OpenWndCallback["SpokesmanHireContractWnd"] = function(wnd, data)
	CLuaSpokesmanMgr:OpenSpokesmanHireContractWnd()
end

g_OpenWndCallback["SpokesmanCardsWnd"] = function(wnd, data)
	Gac2Gas.RequestSpokesmanTCGData()
end

g_OpenWndCallback["MengGuiJieSignWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.MengGuiJieSignWnd)
end

g_OpenWndCallback["DaoDanTipEnterWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.DaoDanTipEnterWnd)
end

g_OpenWndCallback["TerrifyItemUseWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.TerrifyItemUseWnd)
end

g_OpenWndCallback["TerrifyItemUseEntryWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.TerrifyItemUseEntryWnd)
end

--新的手机认证
g_OpenWndCallback["CommonDeviceVerifyNew"] = function(wnd, data)
	if data then
		local argvs = MsgPackImpl.unpack(data)
		if argvs and argvs.Count > 0 then
			local _areaCode = argvs[0]
      		local _phoneNum = argvs[1]
      		--CRelatePhoneData.Inst.setInfo(_areaCode,_phoneNum, false,true);
      		LuaWelfareMgr:OpenRelatePhoneWnd(_areaCode, _phoneNum, false,true, false)
		end
	end
end

g_OpenWndCallback["CocoonBreak"] = function(wnd, data)

	local list = MsgPackImpl.unpack(data)
	if list and list.Count == 1 then
		local taskId = tonumber(list[0])
		if not CUIManager.IsLoaded(CLuaUIResources.CocoonBreakWnd) then
			CLuaTaskMgr.m_CocoonBreakWnd_TaskId  = taskId
			CUIManager.ShowUI(CLuaUIResources.CocoonBreakWnd)
		end
	end
end

g_OpenWndCallback["SpokesmanHouseZhongChou"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.SpokesmanHouseZhongChouWnd)
end

g_OpenWndCallback["Shuangshiyi2020Voucher"]=function(wndName,data)
	CLuaShuangshiyi2020Mgr.UseVoucher()
end

g_OpenWndCallback["Shuangshiyi2020ZhongCao"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020ZhongCaoWnd)
end

g_OpenWndCallback["Shuangshiyi2020ClearTrolley"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020ClearTrolleyEnterWnd)
end

g_OpenWndCallback["Shuangshiyi2020Lottery"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020LotteryWnd)
end

g_OpenWndCallback["ShuangShiYi2022MainWnd"] = function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2022MainWnd)
end

g_OpenWndCallback["MengQuanHengXingSignUpWnd"] = function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingSignUpWnd)
end

g_OpenWndCallback["ShenZhaiTanBaoSignUpWnd"] = function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.ShenZhaiTanBaoSignUpWnd)
end

g_OpenWndCallback["AnQiDaoTipWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.AnQiDaoTipWnd)
end

g_OpenWndCallback["YuanXiao2021QiYuanWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2021QiYuanWnd)
end

g_OpenWndCallback["YuanXiao2021TangYuanEnterWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2021TangYuanEnterWnd)
end

--天成酒壶界面
g_OpenWndCallback["TcjhMainWnd"]=function(wndName,data)
	Gac2Gas.QueryTianChengData()
end

g_OpenWndCallback["TsjzMainWnd"] = function(wndName,data)
	LuaHanJiaMgr.RequireTsjzData()
end

g_OpenWndCallback["TcjhRideSelectWnd"]=function(wndName,data)
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		if itemId == nil  then return end
		CLuaTcjhRideSelectWnd.OpenUI(itemId)
	end
end

g_OpenWndCallback["GiftSelectWnd"]=function(wndName,data)
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		if itemId == nil  then return end
		LuaGiftSelectWndMgr.Show(itemId)
	end
end

g_OpenWndCallback["PetAdventureStartWnd"]=function(wndName,data)
	Gac2Gas.OpenHanJiaAdventureSignUpWnd()
end

g_OpenWndCallback["XueJingKuangHuanStartWnd"]=function(wndName,data)
	LuaHanJiaMgr:OpenXueJingKuangHuanStartWnd()
end

g_OpenWndCallback["ZongMenMingGeJianDing"] = function(wnd, data)
	LuaZongMenMgr:OpenMingGeJianDingWnd()
end

g_OpenWndCallback["WuXingAvoidSetting"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.WuXingAvoidSettingWnd)
end

g_OpenWndCallback["ZongMenSearch"] = function(wnd, data)
	if LuaZongMenMgr.m_OpenNewSystem then
		CUIManager.ShowUI(CLuaUIResources.ZongMenNewSearchWnd)
		return
	end
	CUIManager.ShowUI(CLuaUIResources.ZongMenSearchWnd)
end

g_OpenWndCallback["SoulCoreCondenseWnd"] = function(wnd, data)
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "isInit") then
		local isInit = CommonDefs.DictGetValue_LuaCall(infoDict, "isInit")
		if isInit == 1 then
			LuaZongMenMgr:TryStartCondense()
			return
		end
	end
    CUIManager.ShowUI(CLuaUIResources.SoulCoreCondenseWnd)
end

g_OpenWndCallback["ZongMenJusticeContributeWnd"] = function(wnd, data)
    CUIManager.ShowUI(CLuaUIResources.ZongMenJusticeContributeWnd)
end

g_OpenWndCallback["ZongMenCreate"]=function(wndName,data)
	local list = MsgPackImpl.unpack(data)
	if list.Count > 0 then
		LuaZongMenMgr:OpenZongMenCreateIntroductionWnd(list[0])
	end
end

g_OpenWndCallback["MusicBox"]=function(wndName,data)
	CUIManager.CloseUI(CUIResources.PackageWnd)
	CLuaMusicBoxMgr:EnableMusicBox(true)
end

g_OpenWndCallback["ZhuoRenTipWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.ZhuoRenTipWnd)
end

g_OpenWndCallback["EnemyZongPaiWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.EnemyZongPaiWnd)
end

g_OpenWndCallback["GuardZongMen"] = function(wnd, data)
	LuaZongMenMgr:GoToGuardZongMen()
end

g_OpenWndCallback['OffWorldPlay1MiddleResultWnd'] = function(wnd, data)
	local infoList = MsgPackImpl.unpack(data)
  LuaOffWorldPassMgr.MiddleStageData = {infoList[0],infoList[1],infoList[2]}
  CUIManager.ShowUI(CLuaUIResources.OffWorldPlay1MiddleResultWnd)
end

g_OpenWndCallback["InviteNpcJoinZongMenWnd"]=function(wndName,data)
	LuaInviteNpcMgr.ShowInviteNpcWnd()
end

g_OpenWndCallback["TrainNpcFavorWnd"]=function(wndName,data)
	Gac2Gas.QueryMySectInviteNpcData()
end

g_OpenWndCallback["GuanNingGroupWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.GuanNingGroupWnd)
end

g_OpenWndCallback["ShengChenGangWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.ShengChenGangWnd)
end

g_OpenWndCallback["ZNQZhiBoYaoQingWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.ZNQZhiBoYaoQingWnd)
end

g_OpenWndCallback["TongQingYouLiWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.TongQingYouLiWnd)
end

g_OpenWndCallback["DaMoWangEnterWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.DaMoWangEnterWnd)
end

g_OpenWndCallback["Halloween2021GamePlayWnd"]=function(wndName,data)
	Gac2Gas.OpenJieLiangYuanSignUpWnd()
end

g_OpenWndCallback["DaMoWangResultWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.DaMoWangResultWnd)
end

g_OpenWndCallback["KunxiansuoUseWnd"]=function(wndName,data)
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") and CommonDefs.DictContains_LuaCall(infoDict, "Place")
	and CommonDefs.DictContains_LuaCall(infoDict, "Pos") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		local place = CommonDefs.DictGetValue_LuaCall(infoDict, "Place")
		local pos = CommonDefs.DictGetValue_LuaCall(infoDict, "Pos")
		LuaZongMenMgr:OpenKunxiansuoUseWnd(itemId, place, pos)
	end
end

g_OpenWndCallback["CommonButtonAtPosWnd"]=function(wndName,data)
	local infoList = MsgPackImpl.unpack(data)
	if infoList.Count >= 5 then
		LuaCommonButtonAtPosWndMgr:ShowButton(infoList[0],tonumber(infoList[1]), tonumber(infoList[2]), tonumber(infoList[3]), infoList[4])
	end
end

g_OpenWndCallback["HaoYiXingCardWnd"]=function(wndName,data)
	if not LuaHaoYiXingMgr.m_IsPlayOpen then return end
	CUIManager.ShowUI(CLuaUIResources.HaoYiXingCardsWnd)
end

g_OpenWndCallback["WuYi2021JianXinWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.WuYi2021JianXinWnd)
end

g_OpenWndCallback["LuoSpecialZongMenWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.LuoSpecialZongMenWnd)
end

g_OpenWndCallback["LiuYi2021TangGuoEnterWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.LiuYi2021TangGuoEnterWnd)
end

g_OpenWndCallback["RepairTongTianTaSubmitSilverWnd"]=function(wndName,data)
	local list = MsgPackImpl.unpack(data)
	if list and list.Count == 4 then
		local info = {}
		for i = 1, 4 do
			info[i] = list[i-1]
		end
		LuaZongMenMgr.m_RepairTongTianTaInfo = info
		if not CUIManager.IsLoaded(CLuaUIResources.RepairTongTianTaSubmitSilverWnd) then
			CUIManager.ShowUI(CLuaUIResources.RepairTongTianTaSubmitSilverWnd)
		else 
			g_ScriptEvent:BroadcastInLua("RefreshRepairTongTianTaSubmitSilverWnd")
		end
	end
end

g_OpenWndCallback["AntiProfessionSwapWnd"]=function(wndName,data)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.SkillProp.AntiProfessionSlot.CurSlotNum >= 2 then
		CUIManager.ShowUI(CLuaUIResources.AntiProfessionSwapWnd)
	else
		g_MessageMgr:ShowMessage("ANTIPROFESSION_SWAPVIEW_SLOT_NOTENOUGH")
	end
end

g_OpenWndCallback["AntiProfessionConvertWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.AntiProfessionConvertWnd)
end

g_OpenWndCallback["Unity2018UpgradeWnd"]=function(wndName,data)
	LuaWelfareMgr:OpenUnity2018UpgradeWnd()
end

g_OpenWndCallback["HaiDiaoFuBenSignUpWnd"] = function (wnd, data)
	LuaSeaFishingMgr.OpenSignHaiDiaoFuBenWnd()
end

g_OpenWndCallback["FruitPartySignWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.FruitPartySignWnd)
end

g_OpenWndCallback["ShiTuFruitPartySignWnd"] = function (wnd, data)
	LuaShiTuMgr:ShowShiTuFruitPartySignWnd()
end

g_OpenWndCallback["QueQiaoXianQuEnterWnd"] = function (wnd, data)
	--把旧版本的接口 也直接打开新的界面 2023七夕
	CUIManager.ShowUI(CLuaUIResources.QiXi2023QueQiaoXianQuEnterWnd)
end

g_OpenWndCallback["QiXi2023QueQiaoXianQuEnterWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QiXi2023QueQiaoXianQuEnterWnd)
end

g_OpenWndCallback["OffWorldPlay1ShopWnd"] = function (wnd, data)
	Gac2Gas.GetOffWorldBloodShopInfo()
end

g_OpenWndCallback["ScreenTransitionWnd"] = function (wnd,data)
	--OpenWnd("ScreenTransitionWnd",{fadeIn,duration,fadeOut,color})
	local Data = MsgPackImpl.unpack(data)
	if not Data then return end
	local list = nil
	if CommonDefs.IsDic(Data) then
		list = Data["args"]
	else
		list = Data
	end
	local info = {}
	if list and list.Count == 4 then
		info = {fadeIn = tonumber(list[0]),
				duration = tonumber(list[1]),
				fadeOut = tonumber(list[2]),
				color = tostring(list[3])}
		CLuaTaskMgr.ShowScreenTransition(info)
	end
end

g_OpenWndCallback["LingShouLevelLimitWnd"] = function(wnd,data)
	if IsLingShouLevelLimitOpen() then
		CUIManager.ShowUI(CLuaUIResources.LingShouLevelLimitWnd)
	end
end

g_OpenWndCallback["OlympicGamesGambleWnd"] = function(wnd,data)
	LuaOlympicGamesMgr:RequsetOlymmpicGambleWnd()
end
g_OpenWndCallback["OlympicWinterGamesSnowBattleEnterWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.OlympicWinterGamesSnowBattleEnterWnd)
end
g_OpenWndCallback["CurlingGameRule"] = function(wnd,data)
	LuaOlympicGamesMgr:ShowCurlingGameRule()
end

g_OpenWndCallback["SectPrisonListWnd"] = function(wnd,data)
	local list = MsgPackImpl.unpack(data)
	LuaZongMenMgr.m_SectPrisonListInfo = list
	if not CUIManager.IsLoaded(CLuaUIResources.SectPrisonListWnd) then
		CUIManager.ShowUI(CLuaUIResources.SectPrisonListWnd)
	else
		g_ScriptEvent:BroadcastInLua("SectPrisonListInfo")
	end
end

g_OpenWndCallback["ClearZhenZhaiYuWordWnd"]=function(wndName,data)
	if not CClientHouseMgr.Inst:IsInOwnHouse() then
		g_MessageMgr:ShowMessage("FISH_ITEM_CAN_ONLY_USE_IN_HOUSE")
		return
	end

	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") and CommonDefs.DictContains_LuaCall(infoDict, "Pos") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		local pos = CommonDefs.DictGetValue_LuaCall(infoDict, "Pos")
		LuaZhenZhaiYuMgr.OpenClearZhenZhaiYuWordWnd(pos,itemId)
	end
end

g_OpenWndCallback["InheritZhenZhaiYuWordWnd"]=function(wndName,data)
	if not CClientHouseMgr.Inst:IsInOwnHouse() then
		g_MessageMgr:ShowMessage("FISH_ITEM_CAN_ONLY_USE_IN_HOUSE")
		return
	end
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") and CommonDefs.DictContains_LuaCall(infoDict, "Pos") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		local pos = CommonDefs.DictGetValue_LuaCall(infoDict, "Pos")
		LuaZhenZhaiYuMgr.OpenInheritZhenZhaiYuWordWnd(pos,itemId)
	end
end

g_OpenWndCallback["LuoCha2021EnterWnd"]=function(wndName,data)
	Gac2Gas.OpenLuoChaHaiShiSignUpWnd()
end

g_OpenWndCallback["GuildLeagueBuffWnd"]=function(wndName,data)
	local list = MsgPackImpl.unpack(data)
	if list and list.Count==3 then
		local historyList = list[0]
		local defBuffLeftCount = tonumber(list[1])
		local attBuffLeftCount = tonumber(list[2])
		local tbl = {}
		for i=0,historyList.Count-1 do
			local data = historyList[i]
			table.insert(tbl, {id=i+1, time=tonumber(data[0]), playerId=tonumber(data[1]), playerName=tostring(data[2]), isDefBuff=(tonumber(data[3])==0)})
		end
		table.sort(tbl, function(a, b)
			if a.time~=b.time then
				return a.time>b.time
			else
				return a.id<b.id
			end
		end)
		LuaGuildLeagueMgr:ShowGuildLeagueAltarBuffHistoryWnd(tbl, defBuffLeftCount, attBuffLeftCount)
	end
end

g_OpenWndCallback["ZhaiXingEnterWnd"]=function(wndName,data)
	CUIManager.ShowUI(CLuaUIResources.ZhaiXingEnterWnd)
end

g_OpenWndCallback["Christmas2021XingYuSignWnd"] = function(wnd, data)
	LuaChristmas2021Mgr.OpenXingYuSignUpWnd()
end

g_OpenWndCallback["Christmas2021GiveWish"] = function(wnd, data)
	LuaChristmas2021Mgr:OpenMakeWishWnd()
end

g_OpenWndCallback["Christmas2021GuildWishes"] = function(wnd, data)
	LuaChristmas2021Mgr.OpenGuildWishesWnd()
end

g_OpenWndCallback["Christmas2021MengJingWnd"] = function(wnd, data)
	LuaChristmas2021Mgr.OpenMJSYPlayWnd()
end
--狂奔的汤圆
g_OpenWndCallback["RunningWildTangYuanSignWnd"] = function(wnd, data)
	LuaTangYuan2022Mgr.OpenSignWnd()
end
g_OpenWndCallback["RunningWildTangYuanRankWnd"] = function(wnd, data)
	LuaTangYuan2022Mgr.OpenRankWnd()
end
--告白烟花
g_OpenWndCallback["SendShowLoveFireWorkWnd"] = function(wnd, data)
	LuaFireWorkPartyMgr.OpenSendFireworkToFriendWnd()
end
--魅香楼
g_OpenWndCallback["MXLMusicPlayWnd"] = function(wnd, data)
	LuaMeiXiangLouMgr.OpenMusicPlaySignWnd()
end
--周年庆2022
g_OpenWndCallback["HuLuWaTransform4QiaoDuoRuYi"] = function(wnd, data)
	local type = LuaHuLuWa2022Mgr.EnumPreviewType.eQiaoDuoRuYi
	LuaHuLuWa2022Mgr.OpenHuLuWaChoosePreviewWnd(type)
end
g_OpenWndCallback["HuLuWaTransform4RuYiDong"] = function(wnd, data)
	local type = LuaHuLuWa2022Mgr.EnumPreviewType.eRuYiDong
	LuaHuLuWa2022Mgr.OpenHuLuWaChoosePreviewWnd(type)
end
--勇闯如意洞
g_OpenWndCallback["YongChuangRuYiDongSignWnd"] = function(wnd, data)
	local setting = HuluBrothers_Setting.GetData()
	local str = setting.HuluBrotherOpenWeekDay
	local isOpen = LuaHuLuWa2022Mgr.GetActivityState(str)
	if not isOpen and not LuaHuLuWa2022Mgr.m_IsHuLuWaServerSwitchOn then
        g_MessageMgr:ShowMessage("YongChuangRuYiDong_Sign_Tip")
        return
    end
    LuaHuLuWa2022Mgr.OpenRuYiDongSignWnd()
end
--巧夺如意
g_OpenWndCallback["QiaoDuoRuYiSignWnd"] = function(wnd, data)
	local setting = HuluBrothers_Setting.GetData()
	local str = setting.RuYiOpenWeekDay
	local isOpen = LuaHuLuWa2022Mgr.GetActivityState(str)
	local bHideBtn = false
    if not isOpen then
        bHideBtn = true
    else
        bHideBtn = false
    end
    LuaHuLuWa2022Mgr.OpenQiaoDuoRuYiSignWnd(bHideBtn)
end
--三界历险
g_OpenWndCallback["HuLuWaTravelAdventureWnd"] = function(wnd, data)
	local setting = HuluBrothers_Setting.GetData()
	local str = setting.AdventureOpenDay
    local isOpen = LuaHuLuWa2022Mgr.GetActivityState(str)
    if not isOpen then
        g_MessageMgr:ShowMessage("Huluwa_Travel_Tip")
        return
    end
    LuaHuLuWa2022Mgr.OpenTravelWorldWnd()
end
--结伴同游
g_OpenWndCallback["HuLuWaJieBanTongYou"] = function (wnd,data)
	local setting = HuluBrothers_Setting.GetData()
	local str = setting.HuiLiuOpenDay
    local isOpen = LuaHuLuWa2022Mgr.GetActivityState(str)
	if not isOpen then
        g_MessageMgr:ShowMessage("JBTY_Activity_Msg")
        return
    end
    LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage("JBTY_Activity_Msg"), LocalString.GetString("参加"), function ( ... )
        Gac2Gas.RequestOpenHuiGuiJieBanWnd()
    end)
end
--家园保卫
g_OpenWndCallback["HuLuWaJiaYuanBaoWei"] = function (wnd,data)
	local setting = HuluBrothers_Setting.GetData()
	local str = setting.HomeDefendOpenDay
    local isOpen = LuaHuLuWa2022Mgr.GetActivityState(str)
	if not isOpen then
        g_MessageMgr:ShowMessage("JYBW_Activity_Msg")
        return
    end
    LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage("JYBW_Activity_Msg"), LocalString.GetString("参加"), function ( ... )
        CClientHouseMgr.Inst:GoBackHome()
    end)
end
--魔性变身
g_OpenWndCallback["HuLuWaMoXingBianShen"] = function (wnd,data)
	local setting = HuluBrothers_Setting.GetData()
	local str = setting.TransformOpenDay
    local isOpen = LuaHuLuWa2022Mgr.GetActivityState(str)
	if not isOpen then
        g_MessageMgr:ShowMessage("MXBS_Activity_Msg")
        return
    end
    LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage("MXBS_Activity_Msg"), LocalString.GetString("参加"), function ( ... )
        local pixelPos = Utility.GridPos2PixelPos(CPos(30, 40))
        CTrackMgr.Inst:Track(nil, 16000002, pixelPos, 0, DelegateFactory.Action(function()
                
        end), nil, nil, nil, false)
    end)
end
--以爱之名-周年庆用
g_OpenWndCallback["HuLuWaYiAiZhiMing"] = function(wnd,data)
	local setting = HuluBrothers_Setting.GetData()
	local isOpen = false
	local flowerRankOpenDay = setting.FlowerRankOpenDay
    local year,month,day,hour,min,ehour,emin = string.match(flowerRankOpenDay, "(%d+)/(%d+)/(%d+),(%d+):(%d+)-(%d+):(%d+)")
    local sdate = CreateFromClass(DateTime, year, month, day, hour, min, 0)
    local edate = CreateFromClass(DateTime, year, month, day, ehour, emin, 0)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    if DateTime.Compare(now, sdate) >= 0 and DateTime.Compare(now, edate) <= 0 then
        isOpen = true
    end
	if not isOpen then
        g_MessageMgr:ShowMessage("YAZM_Activity_Msg")
        return
    end
    Gac2Gas.QueryYiAiZhiMingSendFlowerRank()
end
g_OpenWndCallback["PackageWnd"] = function (wnd, data)
	CUIManager.ShowUI(CUIResources.PackageWnd)
end
g_OpenWndCallback["GoBackHome"] = function (wnd, data)
	CClientHouseMgr.Inst:GoBackHome()
end


g_OpenWndCallback["YuanDan2022FuDanReadMeWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YuanDan2022FuDanReadMeWnd)
end

g_OpenWndCallback["SchoolRebuildingWnd"] = function(wnd, data)
	local dict = MsgPackImpl.unpack(data)
	if dict then
		local list = dict["args"]
		if list and list.Count == 1 then
			LuaSchoolRebuildingWnd.m_Index = tonumber(list[0])
		end
	end
	CUIManager.ShowUI(CLuaUIResources.SchoolRebuildingWnd)
end

g_OpenWndCallback["ShengXiaoCardEntryWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ShengXiaoCardEntryWnd)
end
g_OpenWndCallback["ShoppingMallWnd"] = function(wnd, data)
	local dict = MsgPackImpl.unpack(data)
	local list = dict and dict["args"]
	local methodName, param1
	if list then
		methodName = list.Count >= 1 and tostring(list[0]) or nil
		param1 = list.Count >= 2 and tonumber(list[1]) or nil
	end

	if methodName == "show_lingyumall" then
		CShopMallMgr.ShowLinyuShoppingMall(param1)
	elseif methodName == "show_yuanbaomall" then
		CShopMallMgr.ShowYuanbaoShoppingMall(param1)
	elseif methodName == "show_charge" then
		CShopMallMgr.ShowChargeWnd()
	elseif methodName == "buy_monthcard" then
		--CChargeWnd.BuyMonthCard()
		CChargeMgr.Inst:BuyMonthCardForMyself()
	elseif methodName == "buy_bigmonthcard" then
		LuaWelfareMgr.BuyBigMonthCardForMyself()
	else 
		CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
	end
end

g_OpenWndCallback["ChunJie2022HeKaWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.ChunJie2022HeKaWnd)

	local list = MsgPackImpl.unpack(data)
	for i=1,list.Count do
		print(i)
	end
end
g_OpenWndCallback["XunZhaoNianWeiWnd"] = function (wnd,data)
	CUIManager.ShowUI(CLuaUIResources.XunZhaoNianWeiWnd)
end

g_OpenWndCallback["FightingSpiritGambleWnd"] = function(wnd, data)
	local dict = MsgPackImpl.unpack(data)
	if not dict then return end
	local list = dict["args"]
	local index = 1
	if list and list.Count==1 then
		index = tonumber(list[0])
	end
	CLuaFightingSpiritMgr:OpenGambleWnd(index)
end

g_OpenWndCallback["BusinessMainWnd"] = function(wnd, data)
	LuaBusinessMgr:TryOpenBusinessWnd()
end

g_OpenWndCallback["BusinessRankWnd"] = function(wnd, data)
	CRankData.Inst.InitRankId = 41000254
	if IsOpenNewRankWnd() then
	    CUIManager.ShowUI(CLuaUIResources.NewRankWnd)
	else
		CUIManager.ShowUI(CUIResources.RankWnd)
	end
end

g_OpenWndCallback["GuildWarsEnterWnd"] = function(wnd, data)
	LuaJuDianBattleMgr.m_ShowTianDiEnterWnd = true
    LuaJuDianBattleMgr:OpenGuildWarsEnterWnd()
end

g_OpenWndCallback["JuDianBattleEnterWnd"] = function(wnd, data)
	LuaJuDianBattleMgr.m_ShowTianDiEnterWnd = false
    LuaJuDianBattleMgr:OpenGuildWarsEnterWnd()
end

g_OpenWndCallback["JuDianBattleInfoWnd"] = function(wnd, data)
    LuaJuDianBattleMgr:OpenInfoWnd(0)
end

g_OpenWndCallback["ShanYeMiZongMainWnd"] = function(wnd, data)
	LuaShanYeMiZongMainWnd.s_UnlockNewExtraTask = true
    CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongMainWnd)
end

g_OpenWndCallback["ShowLetterDisplayWnd"] = function(wnd, data)
	local content
	local list = MsgPackImpl.unpack(data)
	if list and CommonDefs.IsDic(list) then
		if  CommonDefs.DictContains_LuaCall(list, "args") then
			list = list["args"]
			if list and list.Count >= 1 then
				content = list[0]
			end
		end
	else
		if list and list.Count >= 1 then
			content = list[0]
		end
	end
	CLuaLetterDisplayMgr.ShowLetterDisplayWnd(content)
end

g_OpenWndCallback["WuLiangCompanionBtnWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.WuLiangCompanionBtnWnd)
end

g_OpenWndCallback["GuildPuzzleEnterWnd"] = function (wnd, data)
	Gac2Gas.QueryGuildPinTuData()
end

g_OpenWndCallback["ShenYaoExclusiveWnd"] = function(wnd, data)
	if not LuaXinBaiLianDongMgr.IsShenYaoOpen then
		g_MessageMgr:ShowMessage("SHENYAO_CREATE_NPC_FAILED_NOT_IN_TIME")
		return
	end
	local itemId
	local list = MsgPackImpl.unpack(data)
	if list and CommonDefs.IsDic(list) then
		if  CommonDefs.DictContains_LuaCall(list, "args") then
			list = list["args"]
			if list and list.Count >= 1 then
				itemId = list[0]
			end
		end
	else
		if list and list.Count >= 1 then
			itemId = list[0]
		end
	end
	itemId = tonumber(itemId)
	if itemId == XinBaiLianDong_Setting.GetData().ShenYao_ExchangeTicketItemId then
		local package = CUIManager.instance:GetGameObject(CUIResources.PackageWnd)
		if package then
			LuaXinBaiLianDongMgr:ShenYao_OpenSelectNpcWnd(package.transform:Find("Package/PackageView"):GetComponent(typeof(CPackageView)).SelectedItemId)
		end
	else
		Gac2Gas.ShenYao_RequestOpenPersonalNpcWnd(itemId)
	end
end

g_OpenWndCallback["HuanHunShanZhuangSignUpWnd"] = function(wnd, data)
    CUIManager.ShowUI(CLuaUIResources.HuanHunShanZhuangSignUpWnd)
end

-- 通用图文说明
g_OpenWndCallback["CommonTextImageRuleWnd"]=function (wnd, data)
	local dict = MsgPackImpl.unpack(data)

	if dict then
		local list = dict["args"]
		if list and list.Count==1 then
			local index = tonumber(list[0])
			LuaCommonTextImageRuleMgr:ShowWnd(index)
		end
	end
end

-- 通用图片说明
g_OpenWndCallback["CommonImageRuleWnd"]=function (wnd, data)
	local list = MsgPackImpl.unpack(data)
	local picRuleID
	if list and CommonDefs.IsDic(list) then
		if  CommonDefs.DictContains_LuaCall(list, "args") then
			list = list["args"]
			if list and list.Count >= 1 then
				picRuleID = tonumber(list[0])
			end
		end
	elseif type(list) == "number" then
		picRuleID = list
	else
		if list and list.Count >= 1 then
			picRuleID = tonumber(list[0])
		end
	end
	LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(picRuleID)
end

g_OpenWndCallback["HouseRollerCoasterExchangeWnd"] = function(wnd,data)
	if CClientHouseMgr.Inst:IsCurHouseRoomer() then
		g_MessageMgr:ShowMessage("ROOMER_CANNOT_BUY")
		return
	end
	if CClientHouseMgr.Inst:IsInOwnHouse() then
		CUIManager.ShowUI(CLuaUIResources.HouseRollerCoasterExchangeWnd)
	else
		g_MessageMgr:ShowMessage("OPERATION_NEED_IN_OWN_HOUSE")
	end
end

g_OpenWndCallback["JiaoYueDuoSuYiSignUpWnd"] = function(wnd,data)
	CUIManager.ShowUI(CLuaUIResources.JiaoYueDuoSuYiSignUpWnd)
end

g_OpenWndCallback["FightingSpiritWenDa"] = function(wnd,dataU)
	-- {silver, choiceInfo}
	local data = g_MessagePack.unpack(dataU)
	g_ScriptEvent:BroadcastInLua("FightingSpiritWenDa", data)
end

g_OpenWndCallback["ZongPaiLingFuRankWnd"] = function(wnd,dataU)
	CUIManager.ShowUI(CLuaUIResources.ZongPaiLingFuRankWnd)
end

g_OpenWndCallback["RankWnd"] = function(wnd, data)
	local dict = MsgPackImpl.unpack(data)
	if dict then
		local list = dict["args"]
		if list and list.Count == 1 then
			CRankData.Inst.InitRankId = list[0]
		end
	end
	if IsOpenNewRankWnd() then
	    CUIManager.ShowUI(CLuaUIResources.NewRankWnd)
	else
		CUIManager.ShowUI(CUIResources.RankWnd)
	end
end

g_OpenWndCallback["StoreRoomConstructWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.StoreRoomConstructWnd)
end

g_OpenWndCallback["HengYuDangKouTeamSelectWnd"] = function (wnd, data)
	LuaHengYuDangKouMgr:OpenTeamSelectWnd()
end
g_OpenWndCallback["HengYuDangKouOpenWnd"] = function (wnd, data)
	CUIManager.ShowUI(CLuaUIResources.HengYuDangKouOpenWnd)
end

Gas2Gac.m_OpenWndCallback=function(wndName,data)
	if g_OpenWndCallback[wndName] then
		g_OpenWndCallback[wndName](wndName,data)
	elseif CLuaUIResources[wndName] then
		CUIManager.ShowUI(CLuaUIResources[wndName])
	end
end

g_CloseWndCallback={}

Gas2Gac.m_CloseWndCallback=function(wndName,data)
	if g_CloseWndCallback[wndName] then
		g_CloseWndCallback[wndName](wndName,data)
	end
end

g_CloseWndCallback["WuLiangCompanionBtnWnd"] = function (wnd, data)
	CUIManager.CloseUI(CLuaUIResources.WuLiangCompanionBtnWnd)
end

g_CloseWndCallback["GuoQingJiaoChangSelectTempWnd"] = function (wnd, data)
	CUIManager.CloseUI("GQJCChooseRewardWnd")
end

g_CloseWndCallback["BabyNamingWnd"] = function (wnd, data)
	CUIManager.CloseUI(CLuaUIResources.BabyNamingWnd)
end

g_CloseWndCallback["MengHuaLu"] = function (wnd, data)
	CUIManager.CloseUI(CLuaUIResources.MengHuaLuWnd)
end

g_CloseWndCallback["MengHuaLuQiChangWnd"] = function (wnd, data)
	CUIManager.CloseUI(CLuaUIResources.MengHuaLuQiChangWnd)
end

g_CloseWndCallback["NpcFightingWnd"] = function (wnd, data)
	CUIManager.CloseUI(CLuaUIResources.NpcFightingWnd)
end

g_CloseWndCallback["XianZhiPromotionWnd"] = function (wnd, data)
	CUIManager.CloseUI(CLuaUIResources.XianZhiPromotionWnd)
end

g_CloseWndCallback["PackageWnd"] = function (wnd, data)
	if CUIManager.IsLoaded(CIndirectUIResources.ItemInfoWnd) then
		CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
	end
	if CUIManager.IsLoaded(CUIResources.PackageWnd) then
		CUIManager.CloseUI(CUIResources.PackageWnd)
	end
end

g_CloseWndCallback["CageEffectWnd"] = function(wnd,data)
	CUIManager.CloseUI("CageEffectWnd")
end

g_CloseWndCallback["RuYiDongTopRightWnd"] = function(wnd,data)
	CUIManager.CloseUI(CLuaUIResources.RuYiDongTopRightWnd)
end

g_CloseWndCallback["QiXiLiuXingQiYuanWnd"] = function (wnd, data)
	CUIManager.CloseUI("QiXiLiuXingQiYuanWnd")
end

g_CloseWndCallback["SanxingPipeiWnd"] = function(wnd,data)
    CUIManager.CloseUI(CLuaUIResources.SanxingPipeiWnd)
end

g_CloseWndCallback["SanxingRebornWnd"] = function(wnd,data)
    CUIManager.CloseUI(CLuaUIResources.SanxingRebornWnd)
end

g_CloseWndCallback["HouseWoodPileStatusWnd"] = function(wnd,data)
    CUIManager.CloseUI(CLuaUIResources.HouseWoodPileStatusWnd)
end

g_CloseWndCallback["MengGuiJiePiPeiWnd"] = function(wnd,data)
    CUIManager.CloseUI(CLuaUIResources.MengGuiJiePiPeiWnd)
end

g_CloseWndCallback["CommonButtonAtPosWnd"]=function(wndName,data)
	CUIManager.CloseUI(CLuaUIResources.CommonButtonAtPosWnd)
end

g_CloseWndCallback["NewWeddingDeclare"]=function(wndName,data)
	CUIManager.CloseUI(CLuaUIResources.WeddingPromiseWnd)
end

g_CloseWndCallback["WeddingSceneSelectWnd"] = function(wndName,data)
	CUIManager.CloseUI(CLuaUIResources.WeddingSceneSelectWnd)
end

g_CloseWndCallback["BusinessMainWnd"] = function(wndName,data)
	CUIManager.CloseUI(CLuaUIResources.BusinessMainWnd)
end

g_CloseWndCallback["VehicleBatchRenewalFeesWnd"] = function(wndName,data)
	CUIManager.CloseUI(CLuaUIResources.VehicleBatchRenewalFeesWnd)
end

g_CloseWndCallback["SkillBeginnerGuideWnd"] = function (wnd, data)
	CUIManager.CloseUI(CLuaUIResources.SkillBeginnerGuideWnd)
end

Gas2Gac.m_StartCreateMainPlayer=function()
	CClientFurnitureMgr.Inst.m_IsWinter=CLuaHouseMgr.IsWinter()
end

g_SceneTemplateId = 0
Gas2Gac.m_OnMainPlayerCreated=function()
    local gamePlayId=0
    local sceneTemplateId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
        sceneTemplateId=CScene.MainScene.SceneTemplateId
	end
	g_SceneTemplateId=sceneTemplateId
	local map = PublicMap_PublicMap.GetData(sceneTemplateId)
	local sceneName = map and map.Res or nil
	if gamePlayId==51100795 then--端午大作战
		--玩法逻辑在这里并不合适，玩法的一些数据可能在之前同步过来
		-- CLuaDuanWuDaZuoZhanMgr.Init()
	-- elseif --
	elseif gamePlayId==51100444 then--全民争霸赛
		CLuaQMPKMgr.OnEnterPlay()
  elseif gamePlayId==51101092 then -- 明星邀请赛
    CLuaStarBiwuMgr.OnEnterPlay()
	elseif gamePlayId==51100101 or gamePlayId==51100208 then--关宁
		CLuaGuanNingMgr.OnEnterPlay()
	elseif gamePlayId==51101263 then
		CLuaRescueChristmasTreeMgr.OnEnterPlay()
	elseif sceneName=="jinshajing" then--金沙镜
		CLuaSceneMgr.EnterJinShaJing()
	elseif sceneName=="zilishimen" then--zilishimen
		CLuaSceneMgr.EnterSect()
	elseif gamePlayId == 51101269 or gamePlayId == 51101346 then--模仿动物
		CGuideMgr.Inst:StartGuide(EnumGuideKey.LearnAnimal, 0)
	elseif gamePlayId == 51101501 then--清明解字消愁
		CLuaQingMing2020Mgr.OnEnterPvePlay()
	elseif gamePlayId == 51101516 then--清明幻境迷踪
		CLuaQingMing2020Mgr.OnEnterPvpPlay()
	elseif gamePlayId == 51101849 then--安期岛之战
		CLuaAnQiDaoMgr.OnEnterPlay()
	elseif LuaSeaFishingMgr.IsInPlay() then--海钓副本
		LuaSeaFishingMgr:AddListener()
	elseif gamePlayId == 51102436 then--巧夺如意
		LuaHuLuWa2022Mgr.InitRuYiAdditionalSkill()
		if CUIManager.IsLoaded(CLuaUIResources.ScheduleWnd) then
			CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
		end
	elseif gamePlayId == 51100606 or gamePlayId == 51100623 or gamePlayId == 51102571 then--勇闯如意洞
		LuaHuLuWa2022Mgr.m_KillCount = 0
		if CUIManager.IsLoaded(CLuaUIResources.ScheduleWnd) then
			CUIManager.CloseUI(CLuaUIResources.ScheduleWnd)
		end
	elseif gamePlayId == 51103256 or gamePlayId == 51103257 then --中元节2023中元普渡
		LuaZhongYuanJie2023Mgr:OnMainPlayerCreated()
	end
	LuaZuiMengLuMgr:OnMainPlayerCreated(gamePlayId)--2021罪梦录
	CLuaSceneMgr:EnterScene()
	LuaEyeCatchTheDogMgr.OnMainPlayerCreated()
	LuaChristmasMgr.OnMainPlayerCreated()
	CLuaCityWarMgr.OnMainPlayerCreated()
	LuaMengHuaLuMgr.OnMainPlayerCreated()
	CLuaPlayerCapacityMgr.OnMainPlayerCreated()
	LuaWelfareMgr.BindDaShenViaContentProvider()
	LuaWuJianDiYuMgr:OnMainPlayerCreated()
	LuaQingHongFlyMgr.OnMainPlayerCreated()
	g_LuaSchoolContributionMgr:OnMainPlayerCreated()
  LuaSanxingGamePlayMgr.OnMainPlayerCreated()
	LuaMailMgr:SendMailWhenCancelDirectShowJueJi("mainplayercreated")
	LuaLoginMgr.ModifyWindowTitleForPC("mainplayercreated")
	LuaNetworkMgr:OnMainPlayerCreated()
	LuaAccountTransferMgr:OnMainPlayerCreated()
	LuaHuiLiuMgr.OnMainPlayerCreated()
	LuaGamePlayMgr:OnMainPlayerCreated()
	LuaSeaFishingMgr.OnSwitchFishingPannel(false)
	CLuaTaskMgr.OnMainPlayerCreated()
	LuaChunJie2022Mgr.Enter()
	g_DeviceMgr.TryCheckMuMu()
	g_DeviceMgr.TryRemoveTempApkFile()
	LuaPlotDiscussionMgr:OnMainPlayerCreated()
	LuaTangYuan2022Mgr.OnEnterPlay()
	LuaHuanHunShanZhuangMgr:OnMainPlayerCreated()
	LuaVoiceAchievementMgr.AddListener()
	LuaZhongQiu2022Mgr:OnMainPlayerCreated()
	LuaActivityRedDotMgr:OnMainPlayerCreated()
	LuaHalloween2022Mgr:OnMainPlayerCreated()
	LuaZongMenMgr:OnMainPlayerCreated()
	LuaPengDaoMgr:OnMainPlayerCreated()
	LuaZhouNianQing2023Mgr:QueryPass()
	LuaCommonSideDialogMgr:OnMainPlayerCreated()
	CLuaZswTemplateMgr.ClearCustomTemplateDataCache()
	collectgarbage("collect")
end

CClientMainPlayer.m_OnMainPlayerDestroyed=function()
	g_SceneTemplateId = 0
	LuaGamePlayMgr:OnMainPlayerDestroyed()

	LuaWuJianDiYuMgr:OnMainPlayerDestroyed()
	LuaHuiLiuMgr.OnMainPlayerDestroyed()
	CLuaRescueChristmasTreeMgr.OnPlayEnd()
	CLuaQingMing2020Mgr.OnPlayEnd()
	CLuaSceneMgr:Leave()
	LuaSanxingGamePlayMgr.OnMainPlayerDestroyed()
	LuaLoginMgr.ModifyWindowTitleForPC("mainplayerdestroyed")
	CLuaZswTemplateMgr.OnPlayEnd()
	LuaZhuoYaoMgr:Leave()
	LuaZongMenMgr.m_SMSW_PlayState = EnumSMSWPlayState.Idle
	LuaZongMenMgr:ClearNiuZhuanWuXingResult()
	LuaOlympicGamesMgr:OnMainPlayerDestroyed()
	LuaHMLZMgr:Leave()
	LuaSeaFishingMgr:RemoveListener()
	CLuaHouseMgr.ClearYarkPick()
	LuaChristmas2021Mgr.EndPlay()
	LuaChunJie2022Mgr.Leave()
	CLuaGuanNingMgr.OnLeavePlay()
	CLuaQMPKMgr.OnLeavePlay()
	LuaTangYuan2022Mgr.LeaveScene()
	LuaHuLuWa2022Mgr.OnLeaveRuYiDong()
	LuaHuanHunShanZhuangMgr:OnMainPlayerDestroyed()
	LuaHouseRollerCoasterMgr.OnMainPlayerDestroyed()
	LuaQiXi2022Mgr:OnMainPlayerDestroyed()
	LuaXinBaiLianDongMgr:OnMainPlayerDestroyed()
	LuaZhongQiu2022Mgr:OnMainPlayerDestroyed()
	LuaActivityRedDotMgr:OnMainPlayerDestroyed()
	LuaZhongYuanJie2023Mgr:OnMainPlayerDestroyed()
	LuaQingHongFlyMgr.OnMainPlayerDestroyed()
end

Gas2Gac.m_ShowOtherPlayerToPlayer_Lua=function(engineId)
	if g_SceneTemplateId==16000197 then
		CLuaXuanZhuanMuMaMgr.TryBindPlayer(engineId)
	elseif g_SceneTemplateId == 16000006 then
		CLuaChengZhongMgr.TryBindPlayer(engineId)
	end
end

Gas2Gac.m_ShowNpcToPlayer_Lua=function(engineId)
	if g_SceneTemplateId==16000197 then
		CLuaXuanZhuanMuMaMgr.TryBindPlayer(engineId)
	end
end

g_TrackToTaskLocationEventCallback={}

-- 任务表可以调用的TaskEvent,表中需要传入事件名eventName和任务的taskid
CTaskMgr.m_TrackToTaskLocation = function(eventName,taskId, eventConds)
  if eventName == "DuLiNiangPic" then
    LuaShenbingMgr.DLNTaskId = taskId
		CUIManager.ShowUI(CLuaUIResources.ShenbingDuliniangWnd)
  elseif eventName == "HuaZhuZiPic" then
		CUIManager.ShowUI(CLuaUIResources.HuaZhuWnd)
  elseif eventName == "PuzzleCommon" then
  	CZhuJueJuQingMgr.Instance.MihanTaskID = taskId
  	CUIManager.ShowUI(CLuaUIResources.JuQingPinTuWnd)
  elseif eventName == "Painting" then
    LuaPaintPicMgr.ShowArgs = taskId
  	CUIManager.ShowUI(CLuaUIResources.PaintPicWnd)
  elseif eventName == "PaintingType2" then
    LuaPaintPicMgr.ShowArgs = taskId
  	CUIManager.ShowUI(CLuaUIResources.PaintPicWndType2)
  elseif eventName == "PaintingType3" then
    LuaPaintPicMgr.ShowArgs = taskId
  	CUIManager.ShowUI(CLuaUIResources.PaintPicWndType3)
  elseif eventName == "GongTangBianJie" then
    LuaZhuJueJuQingMgr.TaskId = taskId
  	CUIManager.ShowUI(CLuaUIResources.ChooseProofWnd)
  elseif eventName == "YuTuDaoYao" then
    LuaZhuJueJuQingMgr.TaskId = taskId
  	CUIManager.ShowUI(CLuaUIResources.RabbitMakeDrugWnd)
  elseif eventName == "RabbitJump" then
    LuaZhuJueJuQingMgr.TaskId = taskId
  	CUIManager.ShowUI(CLuaUIResources.RabbitJumpWnd)
  elseif eventName == "XianZhiPicture" then
		CLuaXianzhiMgr.OpenXianzhiChooseImageTaskWnd(taskId)
	elseif eventName == "BoLangGu1" then
		CLuaBoLangGuWnd.m_Key = "BoLangGu1"
		CUIManager.ShowUI(CLuaUIResources.BoLangGuWnd)
	elseif eventName == "BoLangGu2" then
		CLuaBoLangGuWnd.m_Key = "BoLangGu2"
		CUIManager.ShowUI(CLuaUIResources.BoLangGuWnd)
	elseif eventName == "BoLangGu3" then
		CLuaBoLangGuWnd.m_Key = "BoLangGu3"
		CUIManager.ShowUI(CLuaUIResources.BoLangGuWnd)
	elseif eventName == "BoLangGu4" then
		CLuaBoLangGuWnd.m_Key = "BoLangGu4"
		CUIManager.ShowUI(CLuaUIResources.BoLangGuWnd)
	elseif eventName == "BoLangGu5" then
		CLuaBoLangGuWnd.m_Key = "BoLangGu5"
		CUIManager.ShowUI(CLuaUIResources.BoLangGuWnd)
	elseif eventName == "BoLangGu7" then
		CLuaBoLangGuWnd.m_Key = "BoLangGu7"
		CUIManager.ShowUI(CLuaUIResources.BoLangGuWnd)
	elseif eventName == "JianBiHua1" then
		CLuaJianBiHuaWnd.m_Key = "JianBiHua1"
		CUIManager.ShowUI(CLuaUIResources.JianBiHuaWnd)
	elseif eventName == "JianBiHua2" then
		CLuaJianBiHuaWnd.m_Key = "JianBiHua2"
		CUIManager.ShowUI(CLuaUIResources.JianBiHuaWnd)
	elseif eventName == "JianBiHua3" then
		CLuaJianBiHuaWnd.m_Key = "JianBiHua3"
		CUIManager.ShowUI(CLuaUIResources.JianBiHuaWnd)
	elseif eventName == "JianBiHua4" then
		CLuaJianBiHuaWnd.m_Key = "JianBiHua4"
		CUIManager.ShowUI(CLuaUIResources.JianBiHuaWnd)
	elseif eventName == "DrawHanZi" then
		Gac2Gas.RequestDrawHanZi(taskId)
	elseif eventName == "HalloweenPinTu" then
		CUIManager.ShowUI("JigsawWnd")
	elseif eventName == "HalloweenTouchJade" then
		CUIManager.ShowUI("CleanLingYuWnd")
	elseif eventName == "xingxinghudie" then
    LuaWorldEventMgr.DrawLineTaskId = taskId
		CUIManager.ShowUI(CLuaUIResources.DrawLineWnd)
	elseif CLuaTaskMgr.IsOpenJuQingWndTaskEvent(eventName) then
		CLuaTaskMgr.RequestOpenEventTask(taskId, eventName)
	elseif eventName == "FinishSectInviteNpcGuide" then
		CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
			if TypeIs(obj, typeof(CClientNpc)) then
                if obj  and obj.TemplateId == 20023042 then
					CClientMainPlayer.Inst.Target = obj
					CClientMainPlayer.Inst:InteractWithNpc(obj.EngineId)
                    return
				end
			end
		end))
  	elseif eventName == "PickScrolls" then
    	CUIManager.ShowUI(CLuaUIResources.OffWorldPha1Wnd)
  	elseif eventName == "SelfEditedOffWorldFashion" then
    	CUIManager.ShowUI(CLuaUIResources.OffWorldPassChooseWnd)
	elseif eventName == "QuShuiLiuShang1" then
		LuaWuMenHuaShiMgr:OpenQuShuiLiuShangWnd(taskId)
	elseif eventName == "QuShuiLiuShang2" then
		LuaWuMenHuaShiMgr:OpenQuShuiLiuShangWnd(taskId)
	elseif eventName == "QuShuiLiuShang3" then
		LuaWuMenHuaShiMgr:OpenQuShuiLiuShangWnd(taskId)
	elseif eventName == "QuShuiLiuShang4" then
		LuaWuMenHuaShiMgr:OpenQuShuiLiuShangWnd(taskId)
	elseif eventName == "LZHuiWeiWuQiong" then
		CUIManager.ShowUI(CLuaUIResources.FanKanHuaCeWnd)
	elseif eventName == "BackToSelfSect" then
		CLuaMiniMapPopWnd.m_ShowWorldMap = true
        CUIManager.ShowUI(CUIResources.MiniMapPopWnd)
	elseif LuaWuMenHuaShiMgr:NeedOpenDerivativePlotWnd(eventName, taskId) then
		LuaWuMenHuaShiMgr:OpenXinShengHuaJiDerivativePlotWnd(taskId)
	else
		local subeventname,conditionId = string.match(eventName,"([^|]+)|(%d+)")
		if subeventname and conditionId and g_TrackToTaskLocationEventCallback[subeventname] then
			g_TrackToTaskLocationEventCallback[subeventname](taskId, eventConds,conditionId)
		elseif g_TrackToTaskLocationEventCallback[eventName] then
			g_TrackToTaskLocationEventCallback[eventName](taskId, eventConds,conditionId)
		end
	end
end

g_TrackToTaskLocationEventCallback["cameramove"] = function(taskId, eventConds,conditionId)
	local id = tonumber(conditionId)
	local data = id and Task_NpcAppear.GetData(id)
	if not data then return end
	local x,y,z,rx,ry,rz,maxDuration,moveDuraion,writeId = string.match(data.Value, "([0-9%.]+),([0-9%.]+),([0-9%.]+),([0-9%.]+),([0-9%.]+),([0-9%.]+),([0-9%.]+),([0-9%.]+),(%d+)")

    if x then
		if CameraFollow.Inst then
			CClientObjectRoot.Inst:HideMonster(true)
			CClientObjectRoot.Inst:HideMainPlayer(true)
			MouseInputHandler.Inst:HideWalkIndicator(true)
			CClientObjectRoot.Inst:SetLayerVisible(false, LayerDefine.OtherPlayer)
			CUIManager.HideUIByChangeLayer(CUIResources.HeadInfoWnd,false,false)
				--移动镜头前先尝试下坐骑
			CClientMainPlayer.Inst:GetOffVehicle()--客户端主动先下一下，不然等rpc回来就晚了，镜头会被修改不能还原
			CZuoQiMgr.Inst:TryRideOff()
    		CameraFollow.Inst:BeginAICamera(Vector3(0, 0, 0), Vector3(0, 0, 0), tonumber(maxDuration), false)
			CameraFollow.Inst:AICameraMoveTo(Vector3(tonumber(x), tonumber(y), tonumber(z)), Vector3(tonumber(rx), tonumber(ry), tonumber(rz)), tonumber(moveDuraion))
				
			RegisterTickOnce(function ()
				CClientObjectRoot.Inst:HideMonster(false)
				CClientObjectRoot.Inst:HideMainPlayer(false)
				MouseInputHandler.Inst:HideWalkIndicator(false)
				CClientObjectRoot.Inst:SetLayerVisible(true, LayerDefine.OtherPlayer)
				CUIManager.ShowUIByChangeLayer(CUIResources.HeadInfoWnd,false,false)
				Gac2Gas.FinishEventTaskWithConditionId(taskId, "cameramove", conditionId)
			end, tonumber(maxDuration) * 1000)
		end
    	CLuaHandWriteEffectMgr.ShowHandWriteEffect(tonumber(writeId), nil)
	end
end

g_TrackToTaskLocationEventCallback["opentaskdialog"] = function(taskId, eventConds)
	Gac2Gas.TaskRequestDoSayDialog(taskId)
end

g_TrackToTaskLocationEventCallback["watchgamevideo"] = function(taskId, eventConds)
	Gac2Gas.TaskRequestPlayGameVideo(taskId)
end

g_TrackToTaskLocationEventCallback["useitem"] = function(taskId, eventConds,conditionId)
	local place = EnumItemPlace.Bag
	local itemTemplateId = conditionId and tonumber(conditionId)
	if not itemTemplateId then return end

    if CItemMgr.Inst:IsTaskItem(itemTemplateId) then
		place = EnumItemPlace.Task
	end

	local flag, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(place,itemTemplateId)
	if not flag or not pos or not itemId then return end

	CItemInfoMgr.ShowTaskTriggeredItemInfo(itemId, taskId)
end

g_TrackToTaskLocationEventCallback["ShuJiaXunWuFinish"]=function(taskId, eventConds,conditionId)
	CLuaBookshelfSearchWnd.Open(taskId)
end

--g_TrackToTaskLocationEventCallback["ShanGuiGuQin"]=function(taskId, eventConds,conditionId)
--	CLuaGuqinWnd.Show(taskId,tonumber(conditionId))
--end

g_TrackToTaskLocationEventCallback["tanguqin"]=function(taskId, eventConds,conditionId)
	CLuaGuqinWnd.Show(taskId,tonumber(conditionId))
end

g_TrackToTaskLocationEventCallback["ShanGuiWuZiQi"]=function(taskId, eventConds,conditionId)
	CLuaWuZiQiWnd.Show(taskId)
end

g_TrackToTaskLocationEventCallback["PanZhuang1"]=function(taskId, eventConds, conditionId)
	local effectType = 1
	local drawTime = 1
	CLuaTaskMgr.ShowPanZhuangWnd(taskId, effectType, drawTime)
end

g_TrackToTaskLocationEventCallback["PanZhuang2"]=function(taskId, eventConds, conditionId)
	local effectType = 2
	local drawTime = 1
	CLuaTaskMgr.ShowPanZhuangWnd(taskId, effectType, drawTime)
end

g_TrackToTaskLocationEventCallback["PanZhuang3"]=function(taskId, eventConds, conditionId)
	local effectType = 3
	local drawTime = 1
	CLuaTaskMgr.ShowPanZhuangWnd(taskId, effectType, drawTime)
end

g_TrackToTaskLocationEventCallback["ObservingSoul"]=function(taskId, eventConds, conditionId)
	if not CUIManager.IsLoaded(CLuaUIResources.ObservingSoulWnd) then
		LuaObservingSoulMgr.m_TaskID = taskId
		CUIManager.ShowUI(CLuaUIResources.ObservingSoulWnd)
	end
end

g_TrackToTaskLocationEventCallback["CocoonBreak"]=function(taskId, eventConds, conditionId)
	if not CUIManager.IsLoaded(CLuaUIResources.CocoonBreakWnd) then
		CLuaTaskMgr.m_CocoonBreakWnd_TaskId  = taskId
		CUIManager.ShowUI(CLuaUIResources.CocoonBreakWnd)
	end
end

g_TrackToTaskLocationEventCallback["EyeCatchTheDog"]=function(taskId, eventConds, conditionId)
	local npcTemplateId = ZhuJueJuQing_Setting.GetData().EyeCatchTheDogWnd_DogNpcTemplateID
	CLuaTaskMgr.ShowEyeCatchTheDogWnd(taskId, npcTemplateId)
end

g_TrackToTaskLocationEventCallback["PetCat1"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.ShowPetCatWnd(1, taskId)
end

g_TrackToTaskLocationEventCallback["PetCat2"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.ShowPetCatWnd(2, taskId)
end

g_TrackToTaskLocationEventCallback["MakeClothes"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.m_MakeClothesWnd_TaskId = taskId
	CLuaTaskMgr.m_MakeClothesWnd_ConditionId = tonumber(conditionId)
	LuaMakeClothesMgr.m_WndType = CLuaTaskMgr.m_MakeClothesWnd_ConditionId
	if CLuaTaskMgr.m_MakeClothesWnd_ConditionId == 2 then
		CUIManager.ShowUI(CLuaUIResources.MakeJiaYiWnd)
		return
	end
	CUIManager.ShowUI(CLuaUIResources.MakeClothesWnd)
end

g_TrackToTaskLocationEventCallback["YiRenSiDrawFan"]=function(taskId, eventConds, conditionId)
	LuaMakeFanWnd.fanData = nil
	LuaMakeFanWnd.duration = nil
	CUIManager.ShowUI(CLuaUIResources.MakeFanWnd)
end

g_TrackToTaskLocationEventCallback["HeShengZiJinWnd"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.m_HeShengZiJinWnd_TaskId = taskId
	CUIManager.ShowUI(CLuaUIResources.HeShengZiJinWnd)
end


g_TrackToTaskLocationEventCallback["PourWine"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.ShowPourWineWnd(taskId)
end

g_TrackToTaskLocationEventCallback["CouncilWnd"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.m_CouncilWnd_TaskId = taskId
	CUIManager.ShowUI(CLuaUIResources.CouncilWnd)
end

g_TrackToTaskLocationEventCallback["DebitNote1"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.m_DebitNote_TaskId = taskId
	CLuaTaskMgr.m_DebitNoteState = 1
	CUIManager.ShowUI(CLuaUIResources.DebitNoteWnd)
end

g_TrackToTaskLocationEventCallback["DebitNote2"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.m_DebitNote_TaskId = taskId
	CLuaTaskMgr.m_DebitNoteState = 2
	CUIManager.ShowUI(CLuaUIResources.DebitNoteWnd)
end

g_TrackToTaskLocationEventCallback["LongPressChangeModel"]=function(taskId, eventConds, conditionId)
	LuaMakeBeautificationWndMgr:ShowMakeBeautificationWnd("LongPressChangeModel", taskId)
end

g_TrackToTaskLocationEventCallback["WipeScreenBloodFog"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.m_ScreenBloodFogWnd_TaskId = taskId
	CUIManager.ShowUI(CLuaUIResources.ScreenBloodFogWnd)
end

g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi1"]=function(taskId, eventConds,conditionId)
	CLuaKaoChengHuangWnd.Open(taskId)
end

g_TrackToTaskLocationEventCallback["TiaoXiangPlay"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.ShowTiaoXiangPlayWnd(taskId)
end

g_TrackToTaskLocationEventCallback["OpenJinNang"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.ShowOpenJinNangWnd(taskId)
end

g_TrackToTaskLocationEventCallback["SectNpcWanPiLingShou"]=function(taskId, eventConds, conditionId)
	CLuaTaskMgr.ShowWanPiLingShouWnd(taskId)
end

g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi2"]=g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi1"]
g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi3"]=g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi1"]
g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi4"]=g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi1"]
g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi5"]=g_TrackToTaskLocationEventCallback["DiYuChengHuangKaoShi1"]

g_TrackToTaskLocationEventCallback["PlayFlute"]=function(taskId, eventConds,conditionId)
	local fluteId = tonumber(conditionId)
	if not fluteId then return end

	LuaDiZiWnd.fluteId  = fluteId
	LuaDiZiWnd.taskId 	= taskId
	-- print("PlayFlute", fluteId)
	CUIManager.ShowUI(CLuaUIResources.DiZiWnd)
end

--打开NPCChat界面
g_TrackToTaskLocationEventCallback["OpenNpcChatWnd"]=function(taskId, eventConds,conditionId)
	local npcid = tonumber(conditionId)
	local npcdata = NPC_NPC.GetData(npcid)
	local npcname = ""
	if npcdata then 
		npcname = npcdata.Name
	end
	CChatHelper.ShowFriendWnd(npcid, npcname)
	LuaNPCChatMgr.TaskID = taskId
	Gac2Gas.OpenNpcChatWnd(npcid)
end

g_TrackToTaskLocationEventCallback["GameAudioOpen"]=function(taskId, eventConds,conditionId)
	g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("设置中打开音效才能继续任务哦！"))
end

g_TrackToTaskLocationEventCallback["WipeScreenLight"]=function(taskId, eventConds,conditionId)
	local wipeId = tonumber(conditionId)
	if not wipeId then return end

	LuaCleanScreenWnd.taskId = taskId
	LuaCleanScreenWnd.wipeId = wipeId
	CUIManager.ShowUI(CLuaUIResources.CleanScreenWnd)
end

g_TrackToTaskLocationEventCallback["LingShouRenQing"]=function(taskId, eventConds,conditionId)
	LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel = tonumber(conditionId)
	if not LuaInviteNpcMgr.m_LingShouRenQinDifficultyLevel then return end
	CUIManager.ShowUI(CLuaUIResources.LingShouRenQinWnd)
end

g_TrackToTaskLocationEventCallback["PlantTree"] = function(taskId, eventConds,conditionId)
	LuaPlantTreeWnd.taskId = taskId
	CUIManager.ShowUI(CLuaUIResources.PlantTreeWnd)
end

g_TrackToTaskLocationEventCallback["FireCage"] = function(taskId, eventConds,conditionId)
	LuaFireCageWnd.taskId = taskId
	CUIManager.ShowUI(CLuaUIResources.FireCageWnd)
end

g_TrackToTaskLocationEventCallback["DayToNight"] = function(taskId, eventConds,conditionId)
	LuaDayToNightWnd.taskId = taskId
	CUIManager.ShowUI(CLuaUIResources.DayToNightWnd)
end

g_TrackToTaskLocationEventCallback["LeafPic"] = function(taskId, eventConds,conditionId)
	LuaLeafPicWnd.taskId = taskId
	CUIManager.ShowUI(CLuaUIResources.LeafPicWnd)
end

g_TrackToTaskLocationEventCallback["TakePhoto"] = function(taskId, eventConds,conditionId)
	CLuaTaskMgr.TaskTriggerTakePhoto(taskId, "TakePhoto")
end

g_TrackToTaskLocationEventCallback["SelectNewbieSchoolCourse"] = function(taskId, eventConds,conditionId)
	Gac2Gas.RequestOpenSelectNewbieSchoolWnd(taskId)
end

g_TrackToTaskLocationEventCallback["FeedLingShouRenShenGuo"] = function(taskId, eventConds,conditionId)
	CLuaLingShouMgr.TryFeedLingShou()
end

g_TrackToTaskLocationEventCallback["skill125"] = function(taskId, eventConds,conditionId)

	local cls = CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) or 0
    local data = Exchange_Exchange125Skill.GetData(cls)
	if data == nil then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", "No Item In Exchange_Exchange125Skill for Class "..tostring(cls))
        return
    end
    if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(data.SkillID) then
        g_MessageMgr:ShowMessage("Exchange_125_Skill_Book_Skill_Already_Learned")
		return
	end
	local exchangeInfo = Exchange_Exchange.GetData(data.ExchangeID)
	local itemTemplateId = exchangeInfo.NewItems[0][0]
	if CItemMgr.Inst:GetItemCount(itemTemplateId)>0 then
		local ret, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, itemTemplateId)
		CItemInfoMgr.ShowQuickUseItem(itemId)
	else
		CUIManager.ShowUI("Exchange125SkillBookWnd")
	end
end

g_TrackToTaskLocationEventCallback["FinishJianDingMingGe"]=function(taskId, eventConds, conditionId)
	LuaZongMenMgr:OpenMingGeJianDingWnd()
end

g_TrackToTaskLocationEventCallback["OpenSoulCoreGainWnd"]=function(taskId, eventConds, conditionId)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0 then
		Gac2Gas.RequestOpenCoulCoreGainWnd(CClientMainPlayer.Inst.BasicProp.SectId)
	end
end

g_TrackToTaskLocationEventCallback["GuideAntiProLv"]=function(taskId, eventConds, conditionId)
	LuaZongMenMgr.m_SoulCoreWndTabIndex = 1
	CUIManager.ShowUI(CLuaUIResources.SoulCoreWnd)
end

g_TrackToTaskLocationEventCallback["GuideHpSkillLv"]=function(taskId, eventConds, conditionId)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0 then
		LuaZongMenMgr.m_MainWndTabIndex = 2
		LuaZongMenMgr.m_ZongMenSkillViewIndex = 2
		CUIManager.ShowUI(CLuaUIResources.ZongMenMainWnd)
	else 
		g_MessageMgr:ShowMessage("ZONGPAI_TASK_NO_SECT")
	end
end

g_TrackToTaskLocationEventCallback["LearnShiMenMiShu"]=function(taskId, eventConds, conditionId)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0 then
		LuaZongMenMgr.m_MainWndTabIndex = 2
		LuaZongMenMgr.m_ZongMenSkillViewIndex = 1
		CUIManager.ShowUI(CLuaUIResources.ZongMenMainWnd)
	else 
		g_MessageMgr:ShowMessage("ZONGPAI_TASK_NO_SECT")
	end
end

g_TrackToTaskLocationEventCallback["pianpianqiuyu"] = function(taskId, eventConds,conditionId)
	-- LuaWorldEventMgr.DrawMoveNodeFx = ""
	-- LuaWorldEventMgr.DrawFinishFx = "fx/ui/prefab/UI_yibihua_hudie02.prefab"
	LuaWorldEventMgr.DrawTaskId = taskId
	LuaWorldEventMgr.DrawEventName = "pianpianqiuyu"
	CUIManager.ShowUI(CLuaUIResources.DrawDripWnd)
end
-- 叶生推理玩法
g_TrackToTaskLocationEventCallback["yssxzr"]=function(taskId, eventConds, conditionId)
	CUIManager.ShowUI(CLuaUIResources.YeShengTuiLiWnd)
end

g_TrackToTaskLocationEventCallback["XZNW_SendCard"]=function(taskId, eventConds, conditionId)
	Gac2Gas.XZNW_OpenCardWnd()
end

g_TrackToTaskLocationEventCallback["ZhuErDan_JieDianXin"]=function(taskId, eventConds, conditionId)
	CUIManager.ShowUI(CLuaUIResources.ZhuErDanJieDianXinWnd)
end
g_TrackToTaskLocationEventCallback["ZhuErDan_ChuoPaoPao"]=function(taskId, eventConds, conditionId)
	CUIManager.ShowUI(CLuaUIResources.ZhuErDanChuoPaoPaoWnd)
end
g_TrackToTaskLocationEventCallback["ZhuErDan_DaDiShu"]=function(taskId, eventConds, conditionId)
	CUIManager.ShowUI(CLuaUIResources.ZhuErDanDaDiShuWnd)
end
g_TrackToTaskLocationEventCallback["ZhuErDan_GuanChaXinZang"]=function(taskId, eventConds, conditionId)
	CUIManager.ShowUI(CLuaUIResources.ZhuErDanGuanChaXinZangWnd)
end
g_TrackToTaskLocationEventCallback["TradeSimulationFinish"]=function(taskId, eventConds, conditionId)
	LuaBusinessMgr:TryOpenBusinessWnd()
end
g_TrackToTaskLocationEventCallback["TradeSimulationScenarioPassed"]=function(taskId, eventConds, conditionId)
	LuaBusinessMgr:TryOpenBusinessWnd()
end
g_TrackToTaskLocationEventCallback["ShuiManJinShan"]=function(taskId, eventConds, conditionId)
	LuaShuiManJinShanMgr:OpenGroupWnd(1)
end
g_TrackToTaskLocationEventCallback["PingMao"]=function(taskId, eventConds, conditionId)
	CUIManager.ShowUI(CLuaUIResources.EmployCatAgreementWnd)
end

g_TrackToTaskLocationEventCallback["YuYuanMengJi"] = function(taskId, eventConds, conditionId)
	ZhongYuanJie2022Mgr.OpenYuYuanMengJi(taskId, conditionId)
end

g_TrackToTaskLocationEventCallback["FinishDuLing"] = function(taskId, eventConds,conditionId)
	LuaShiTuMgr:RequestStartShiTuDuLingTask()
end

g_TrackToTaskLocationEventCallback["PengDaoDialogWnd"] = function(taskId, eventConds,conditionId)
	Gac2Gas.PDFY_RequestOpenMainWnd()
end

g_TrackToTaskLocationEventCallback["CatchYaoGuaiSucc"] = function(taskId, eventConds,conditionId)
	CGuideMgr.Inst:StartGuide(EnumGuideKey.NewZhuoYaoCaptureView, 0)
end

g_TrackToTaskLocationEventCallback["LiYuZhangDaiFoodCook"]=function(taskId, eventConds, conditionId)
	LuaNanDuFanHuaCookBookWnd.DefaultOpenTab = tonumber(conditionId) - 1
	CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaCookBookWnd)
end

g_TrackToTaskLocationEventCallback["NanDuFanHuaPaper"]=function(taskId, eventConds, conditionId)
	--验证之后删除下面的NanDuFanHuaPaper1-NanDuFanHuaPaper4
	LuaNanDuPaperWnd.paperIdList = {tonumber(conditionId)}
	LuaNanDuPaperWnd.firstOpenPaperId = tonumber(conditionId)
	CUIManager.ShowUI(CLuaUIResources.NanDuPaperWnd)
end

g_TrackToTaskLocationEventCallback["NanDuLordAcceptSchedule"] = function(taskId, eventConds,conditionId)
	local lordScheduleId = tonumber(conditionId)
	if lordScheduleId == 7 then
		CGuideMgr.Inst:StartGuide(EnumGuideKey.NanDuScheduleGuide, 0)
	else
		LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex = 2
		CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordMainWnd)
	end
end

g_TrackToTaskLocationEventCallback["NanDuFanHuaPaper1"] = function(taskId, eventConds,conditionId)
	LuaNanDuPaperWnd.paperIdList = {1}
	LuaNanDuPaperWnd.firstOpenPaperId = 1
	CUIManager.ShowUI(CLuaUIResources.NanDuPaperWnd)
end

g_TrackToTaskLocationEventCallback["NanDuFanHuaPaper2"] = function(taskId, eventConds,conditionId)
	LuaNanDuPaperWnd.paperIdList = {2}
	LuaNanDuPaperWnd.firstOpenPaperId = 2
	CUIManager.ShowUI(CLuaUIResources.NanDuPaperWnd)
end

g_TrackToTaskLocationEventCallback["NanDuFanHuaPaper3"] = function(taskId, eventConds,conditionId)
	LuaNanDuPaperWnd.paperIdList = {3}
	LuaNanDuPaperWnd.firstOpenPaperId = 3
	CUIManager.ShowUI(CLuaUIResources.NanDuPaperWnd)
end

g_TrackToTaskLocationEventCallback["NanDuFanHuaPaper4"] = function(taskId, eventConds,conditionId)
	LuaNanDuPaperWnd.paperIdList = {4}
	LuaNanDuPaperWnd.firstOpenPaperId = 4
	CUIManager.ShowUI(CLuaUIResources.NanDuPaperWnd)
end
g_TrackToTaskLocationEventCallback["DaFuWengPlay"] = function(taskId, eventConds,conditionId)
	CUIManager.ShowUI(CLuaUIResources.DaFuWongEnterWnd)
end
g_TrackToTaskLocationEventCallback["DaFuWengYaoQian"] = function(taskId, eventConds,conditionId)
	CUIManager.ShowUI(CLuaUIResources.DaFuWongEnterWnd)
end
g_TrackToTaskLocationEventCallback["TearCalendar"] = function(taskId, eventConds,conditionId)
	LuaLiuRuShiMgr:OpenTearCalendarWnd(taskId, conditionId)
end

g_TrackToTaskLocationEventCallback["LiuRuShi_JianShu"] = function(taskId, eventConds,conditionId)
	LuaLiuRuShiMgr:OpenJianShuWnd(taskId, conditionId)
end

g_TrackToTaskLocationEventCallback["LiuRuShi_Examination"] = function(taskId, eventConds,conditionId)
	LuaLiuRuShiMgr:OpenLiuRuShiDaTiWnd(taskId)
end

g_TrackToTaskLocationEventCallback["LiuRuShi_HuanZhuang"] = function(taskId, eventConds,conditionId)
	LuaLiuRuShiMgr:OpenLiuRuShiHuanZhuangWnd(taskId)
end

g_TrackToTaskLocationEventCallback["LiuRuShi_Gifting"] = function(taskId, eventConds,conditionId)
	LuaLiuRuShiMgr:OpenLiuRuShiGiftWnd(taskId, conditionId)
end

g_TrackToTaskLocationEventCallback["bargain"]=function(taskId, eventConds, conditionId)
	LuaYiRenSiMgr:OpenBargain(tonumber(conditionId))
end

g_TrackToTaskLocationEventCallback["MuKeShuiYin"]=function(taskId, eventConds, conditionId)
	LuaYiRenSiMgr:OpenMuke(tonumber(conditionId))
end

g_TrackToTaskLocationEventCallback["WuChengEnFangSheng"] = function(taskId, eventConds,conditionId)
	CUIManager.ShowUI(CLuaUIResources.FreeFishWnd)
end

g_TrackToTaskLocationEventCallback["StoreRoomUnlocked"] = function(taskId, eventConds,conditionId)
	LuaStoreRoomMgr:OpenConstructWnd()
end

-- 用于控制显示副本中的任务列表，默认返回false执行C#中的默认操作

g_TaskListBoardCustomViewTbl = {}
-- refs #169118 by wuyihang
g_TaskListBoardCustomViewTbl["HuanHunShanZhuangView"] = function() return LuaHuanHunShanZhuangMgr:IsInPlay() end
-- refs #170514 by wuyihang
g_TaskListBoardCustomViewTbl["ShenYaoTaskView"] = function() return LuaXinBaiLianDongMgr:IsInShenYaoPlay() end
-- refs #170514 by xieshiheng
g_TaskListBoardCustomViewTbl["WuLiangCompanionView"] = function() return LuaWuLiangMgr:IsInWuLiangBattleScene() end
-- refs #68925 by hzfengkaixuan
g_TaskListBoardCustomViewTbl["ChuangGuanStatView"] = function() return CMenPaiChuangGuanMgr.Inst.IsInGamePlay end
-- svn revision 217785 by hzlumengmeng
g_TaskListBoardCustomViewTbl["WeekendActivityStatusView"] = function() return CWeekendGameplayMgr.Inst.isPlayerInScene end
-- refs #92763 by hzzhangqing
g_TaskListBoardCustomViewTbl["XueqiuStatusView"] = function() return CSnowBallMgr.Inst:IsInSnowBallFight() end
-- refs #96474 by hzshengfang
g_TaskListBoardCustomViewTbl["GuanNingTaskView"] = function() return CGuanNingMgr.Inst.inGnjc end
-- refs #97459 by hzzhangqing
g_TaskListBoardCustomViewTbl["WeekendFightView"] = function() return CWeekendFightMgr.Inst.inFight end
-- refs #109781 by hzzhangqing
g_TaskListBoardCustomViewTbl["NewYearGameView"] = function() return CWeekendFightMgr.Inst.inNewYearGame end
-- refs #115254 by hzliuyujia
g_TaskListBoardCustomViewTbl["WaKuangTaskView"] = function() return LuaWuYiMgr.IsInWaKuang() end
-- refs #115719 by hzliuyujia
g_TaskListBoardCustomViewTbl["FengXianView"] = function() return CLuaXianzhiMgr.InFengXian() end
-- refs #116376 by chenguangzhong
g_TaskListBoardCustomViewTbl["ZongziView"] = function() return CLuaDwMgr2019.IsInZongziPlay() end
-- refs #123199 by hzzhangqing
g_TaskListBoardCustomViewTbl["SanXingView"] = function() return LuaSanxingGamePlayMgr.IsInPlay() end
-- refs #144136 by hzzhangqing
g_TaskListBoardCustomViewTbl["BaoZhuView"] = function() return LuaBaoZhuPlayMgr.IsInPlay() end
-- refs #146380 by hzzhangqing
g_TaskListBoardCustomViewTbl["QingMingNHTQView"] = function() return LuaQingMing2021Mgr.IsInPlay() end
-- refs #133979 #128624 by huangxinyu
g_TaskListBoardCustomViewTbl["QingYiXiangTongView"] = function() return CLuaQYXTMgr.IsInPlay() or CLuaQingMing2020Mgr.IsInPlay() or CLuaQingMing2020Mgr.IsInPvePlay() end
-- refs #135715 by hzliuyujia
g_TaskListBoardCustomViewTbl["ChiJiTaskView"] = function() return CPUBGMgr.Inst:IsInChiJi() end
-- refs #137333 by tongtiexin
g_TaskListBoardCustomViewTbl["MengGuiJieTaskView"] = function() return CLuaHalloween2020Mgr.IsInPlay() end
-- refs #143632 by xieshiheng
g_TaskListBoardCustomViewTbl["YuanXiao2021TangYuanBoardView"] = function() return LuaYuanXiao2021Mgr.IsInTangYuanPlay() end
--refs #135023 by linhao1
g_TaskListBoardCustomViewTbl["DuanWu2020TaskBoardView"] = function() return DuanWu2020Mgr:IsInPlay() end

g_TaskListBoardCustomViewTbl["OffWorldPlay2View"] = function() return LuaOffWorldPassMgr.IsInPlay2() end

g_TaskListBoardCustomViewTbl["ShiMenRuQinView"] = function() return LuaZongMenMgr:IsShiMenShouWeiFaZhenPlay() end
--refs #148612 by baixue
g_TaskListBoardCustomViewTbl["ShengChenGangView"] = function() return LuaZhouNianQing2021Mgr:IsInPlay() end
g_TaskListBoardCustomViewTbl["DuanWu2021TaskBoardView"] = function() return LuaDuanWu2021Mgr:IsInPlay() end
g_TaskListBoardCustomViewTbl["DisguiseBossView"] = function() return LuaShuJia2021Mgr.IsDaMoWangPlay() end
g_TaskListBoardCustomViewTbl["SeaFishingTaskView"] = function() return LuaSeaFishingMgr.IsInPlay() end
g_TaskListBoardCustomViewTbl["QiXi2021View"]= function() return CLuaQiXiMgr.IsInPlay2021() end
g_TaskListBoardCustomViewTbl["QueQiaoXianQuView"] = function() return LuaQiXi2021Mgr.IsQueQiaoXianQuPlay() end
g_TaskListBoardCustomViewTbl["FruitPartyView"] = function() return LuaFruitPartyMgr.IsInFruitParty() end
g_TaskListBoardCustomViewTbl["Halloween2021View"] = function() return LuaHalloween2021Mgr:IsInGamePlay() end
g_TaskListBoardCustomViewTbl["WeddingView"] = function() return LuaWeddingIterationMgr:IsInGamePlay() end
g_TaskListBoardCustomViewTbl["CommonCountDownView"] = function() return LuaCommonCountDownViewMgr:IsShow() end
g_TaskListBoardCustomViewTbl["OlympicWinterGamesSnowBattleView"] = function() return LuaOlympicGamesMgr:IsInOlympicWinterGamesSnowBattlePlay() end
g_TaskListBoardCustomViewTbl["Christmas2021TaskView"] = function() return LuaChristmas2021Mgr.IsInXingYuPlay() or LuaChristmas2021Mgr.IsInMJSYPlay() or LuaTangYuan2022Mgr.IsInPlay() or LuaHuLuWa2022Mgr.IsInQiaoDuoRuYiPlay() or LuaHuLuWa2022Mgr.IsInRuYiDongPlay() end
g_TaskListBoardCustomViewTbl["GuildTerritorialWarsView"] = function() return LuaGuildTerritorialWarsMgr:IsInPlay() end
g_TaskListBoardCustomViewTbl["ShuiManJinShanTaskView"] = function() return LuaShuiManJinShanMgr:IsInGameplay() end
g_TaskListBoardCustomViewTbl["ShiTuJianLaJiTaskView"] = function() return LuaShiTuMgr:IsInJianLaJiPlay() end
g_TaskListBoardCustomViewTbl["WorldCup2022GJZLView"] = function() return LuaWorldCup2022Mgr:IsInGamePlay() end
g_TaskListBoardCustomViewTbl["HalloweenTrafficTaskView"] = function() return LuaHalloween2022Mgr.IsInTrafficPlay() end
g_TaskListBoardCustomViewTbl["HalloweenShouHuGouHuoTaskView"] = function() return LuaHalloween2022Mgr.IsInFireDefencePlay() end
g_TaskListBoardCustomViewTbl["Double11MQHXTaskView"] = function() return LuaShuangshiyi2023Mgr.IsInMQLDPlay() end
g_TaskListBoardCustomViewTbl["ZongMenPracticeTaskView"] = function() return LuaZongMenMgr:IsInZongMenPracticePlay() end
g_TaskListBoardCustomViewTbl["CommonGamePlayTaskView"] = function() return LuaCommonGamePlayTaskViewMgr:IsInPlay() end

--动态加载窗口，请将新窗口存放于Assets/Res/UI/Prefab/TaskAndTeam/文件夹下，并将窗口名及开启条件填入g_TaskListBoardDynamicViewTbl
g_TaskListBoardDynamicViewTbl = {}
g_TaskListBoardDynamicViewTbl["ShiTuCompassTaskView"] = function() return LuaShiTuMgr:IsInZhaoMiJiPlay() end
g_TaskListBoardDynamicViewTbl["Valentine2023YWQSTaskView"] = function() return LuaValentine2023Mgr:IsInYWQSScene() end
g_TaskListBoardDynamicViewTbl["JuDianBattleCountDownView"] = function() return LuaJuDianBattleMgr:IsInJuDian() or LuaJuDianBattleMgr:IsInGameplay() or LuaJuDianBattleMgr:IsInDailyGameplay() end
g_TaskListBoardDynamicViewTbl["LiuYi2023BossTaskView"] = function() return LuaLiuYi2023Mgr:IsInBossPlay() end
g_TaskListBoardDynamicViewTbl["TreasureSeaTaskView"] = function() return LuaHaiZhanMgr:IsInPvpPlay() end

CTaskListBoard.m_HookTaskListVisible = function (wnd)

	--新增的定制小窗口，请填入g_TaskListBoardCustomViewTbl表中，方便统一处理
	--新增的定制小窗口，需统一挂载Lua脚本处理其中的逻辑，Lua脚本中可以按需决定是否添加Init方法
	
	--先把所有的新增view结点都隐藏，按条件开启
	----C#下已有结点
	wnd.taskListView.gameObject:SetActive(false)
	wnd.countdownView.gameObject:SetActive(false)
	----新增Lua结点
	for viewGoName, needShowFunc in pairs(g_TaskListBoardCustomViewTbl) do
		local viewTransform = wnd.transform:Find(viewGoName)
		if viewTransform then
			viewTransform.gameObject:SetActive(false)
		end
	end
	--动态加载窗口子节点隐藏
	wnd.m_DynamicChild.gameObject:SetActive(false)

	for viewGoName, needShowFunc in pairs(g_TaskListBoardCustomViewTbl) do
		if needShowFunc() then
			local viewTransform = wnd.transform:Find(viewGoName)
			viewTransform.gameObject:SetActive(true)
			local luaScript = CommonDefs.GetComponent_GameObject_Type(viewTransform.gameObject, typeof(CCommonLuaScript))
			if luaScript then
				luaScript:Init({})
			end
			return true
		end
	end
	for viewGoName, needShowFunc in pairs(g_TaskListBoardDynamicViewTbl) do
		if needShowFunc() then
			wnd.m_DynamicChild.gameObject:SetActive(true)
			wnd.m_DynamicChild:Init("UI/Prefab/TaskAndTeam/"..viewGoName..".prefab", {})
			return true
		end
	end
	-- 备注1 return true表示强行显示此方法内设置的view，否则会根据ShowCountdownView()来决定显示tasklistview还是通用的countdownview
	-- 备注2 如果仅想显示countdownview无需在当前方法进行处理，通过GamePlay策划表配表即可实现
	--特殊的逻辑可以在下面添加
	-- refs #106314 by hzzhangqing
	if CWeekendFightMgr.Inst.inBaoWeiNanGua then
		wnd.countdownView.gameObject:SetActive(true)
		return true
	end
	-- refs #133469 by hzzhangqing
	if LuaChildrenDay2020Mgr.InGamePlay() then
		wnd.countdownView.gameObject:SetActive(true)
	end
	
  	return false

end

g_UIActionShowCallback={}

CUIActionMgr.m_UIActionShowCallback = function (windowName, methodName, param1, param2, param3, env)
	if g_UIActionShowCallback[windowName] then
		g_UIActionShowCallback[windowName](windowName, methodName, param1, param2, param3)
	else
		if CLuaUIResources[windowName] then
			CUIManager.ShowUI(CLuaUIResources[windowName])
		end
	end
end

--客户端主动发起OpenWnd
g_UIActionShowCallback["YuYuanMengJi"] = function(windowName, methodName)
    if methodName == "show_wnd" then
        CUIManager.ShowUI(CLuaUIResources.YuYuanMengJiWnd)
    end
end

g_UIActionShowCallback["BQPDailyWnd"]=function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CUIResources.BingQiPuWnd)
end

g_UIActionShowCallback["BQPCommitWnd"]=function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.BQPCommitEquipWnd)
end

g_UIActionShowCallback["BQPResultWnd"]=function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.BQPResultWnd)
end


g_UIActionShowCallback["DoubleOneBonusLotteryWnd"]=function (windowName, methodName, param1, param2, param3)
  Gac2Gas.QueryJoinSinglesDayLotteryInfo()
end

g_UIActionShowCallback["DoubleOneQLDShowWnd"]=function (windowName, methodName, param1, param2, param3)
  Gac2Gas.RequestOpenQiLinDongPlayShop()
end

g_UIActionShowCallback["YanChiXiaWorkWnd"]=function (windowName, methodName, param1, param2, param3)
	Gac2Gas.RequestOpenHanJiaManualWnd()
end

g_UIActionShowCallback["schedulewnd"]=function (windowName, methodName, param1, param2, param3)
	if methodName == "show_wnd_type" then
		CLuaScheduleMgr.taskTabType = tonumber(param1)
		CUIManager.ShowUI(CLuaUIResources.ScheduleWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.ScheduleWnd)
	end
end

g_OpenWndCallback["DuanWu2021OffWorldPlay2TipWnd"]=function(wndName,data)
	LuaDuanWu2021Mgr:ShowOffWorldPlay2TipWnd()
end
g_UIActionShowCallback["WelfareCarnivalTicketWnd"] = function (windowName, methodName, param1, param2, param3)
	CWelfareMgr.OpenWelfareWnd(LocalString.GetString("倩女嘉年华"))
end

g_UIActionShowCallback["shenbingenterwnd"]=function (windowName, methodName, param1, param2, param3)
	-- if methodName=="show_wnd" then
		Gac2Gas.RequestSBCSFinishInfo()
	-- end
end

g_UIActionShowCallback["CharacterCardWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
    Gac2Gas.RequestGetRfcCardSet()
	end
end
g_UIActionShowCallback["MergeBattleWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.QueryMergeBattleOverview()
end


g_UIActionShowCallback["BaiGuiTuJian"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI("BaiGuiTuJianWnd")
end

g_UIActionShowCallback["npcshopwnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_gnjc_shop" then
		CLuaNPCShopInfoMgr.ShowScoreShop("GNJC")
	elseif methodName=="show_mptz_shop" then
		CLuaNPCShopInfoMgr.ShowScoreShop("MPTZ")
	elseif methodName=="show_qyjf_shop" then
		CLuaNPCShopInfoMgr.ShowScoreShop("QYJF")
	elseif methodName=="show_duobao_shop" then
		CLuaNPCShopInfoMgr.ShowScoreShop("DuoBao")
	elseif methodName=="show_gongxun_shop" then
		CLuaNPCShopInfoMgr.ShowScoreShop("GXZ")
	elseif methodName == "show_seafishing_shop" then
		CLuaNPCShopInfoMgr.ShowScoreShop("FishingScore")
	end
end

g_UIActionShowCallback["WelfareWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_chargeactivity" then--福利界面的消费返利
		CWelfareMgr.OpenWelfareWnd(LocalString.GetString("灵玉消费返利"))
	elseif methodName == "show_jierisignin" then -- 节日签到
		CWelfareMgr.OpenWelfareWnd(QianDao_Holiday.GetData(param1).ListTitle)
	elseif methodName == "show_bigmonthcard" then -- 开启大月卡
		LuaWelfareMgr.IsOpenBigMonthCard = true
		CUIManager.ShowUI(CUIResources.WelfareWnd)
	end
end
g_UIActionShowCallback["ZhongCaoFashionWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI("ZhongCaoFashionWnd")
	end
end

g_UIActionShowCallback["BaGuaLuWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		Gac2Gas.RequestOpenBaGuaLu()
	end
end

g_UIActionShowCallback["OpenShop"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		if param1 then
			CLuaNPCShopInfoMgr.ShowScoreShopById(tonumber(param1))
		end
	end
end

g_UIActionShowCallback["HanJia2023LotteryWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		if LuaHanJia2023Mgr.isXKLOpen then
			CUIManager.ShowUI(CLuaUIResources.HanJia2023LotteryWnd)
		end
	end
end

g_UIActionShowCallback["MengHuaLu"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.MengHuaLuStartWnd)
	end
end
g_UIActionShowCallback["GuardZongMenWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		LuaZongMenMgr:GoToGuardZongMen()
	end
end
g_UIActionShowCallback["WuJianDiYuEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.WuJianDiYuEnterWnd)
	end
end

g_UIActionShowCallback["OpenOffWorldPassMainWnd"] = function (windowName, methodName, param1, param2, param3)
  if methodName=="show_wnd" then
    Gac2Gas.GetOffWorldPlayInfo()
	if param1~=nil then
		LuaOffWorldPassMgr.OpenTabIndex = tonumber(param1)
	end
  end
end

g_UIActionShowCallback["WuLiangLevelWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.WuLiangShenJing_OpenElevatorWnd()
end

local CLingShouMgr=import "L10.Game.CLingShouMgr"
g_UIActionShowCallback["lingshouwnd"] = function (windowName, methodName, param1, param2, param3)
	if not CClientMainPlayer.Inst then return end
	if methodName=="show_shuxing" then
		CLuaLingShouMainWnd.tabIndex = 0
		CUIManager.ShowUI(CUIResources.LingShouMainWnd)
	elseif methodName=="show_xilian" then
		CLuaLingShouMainWnd.tabIndex = 1
		CUIManager.ShowUI(CUIResources.LingShouMainWnd)
	elseif methodName=="show_jineng" then
		CLuaLingShouMainWnd.tabIndex = 2
		CUIManager.ShowUI(CUIResources.LingShouMainWnd)
	elseif methodName=="show_xiuwu" then
		CLuaLingShouMainWnd.tabIndex = 3
		CUIManager.ShowUI(CUIResources.LingShouMainWnd)
	end
end

g_UIActionShowCallback["PreExistenceMarridgeAlbumWnd"] = function (windowName, methodName)
	if methodName == "show_wnd" then
		if CClientMainPlayer.Inst then
			LuaValentine2019Mgr.ShowPreExistenceMarridgeAlbumWnd(true, nil,CClientMainPlayer.Inst.Id,CClientMainPlayer.Inst.BasicProp.Name)
		end
	end
end

g_UIActionShowCallback["ShengChenGangWnd"]=function (windowName, methodName, param1, param2, param3)
	if methodName == "show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.ShengChenGangWnd)
	end
end

g_UIActionShowCallback["DaMoWangEnterWnd"]=function (windowName, methodName, param1, param2, param3)
	if methodName == "show_wnd" then
		if LuaShuJia2021Mgr.IsDaMoWangPlay() then g_MessageMgr:ShowMessage("ENTER_PLAY_NEED_IN_PUBLIC_MAP") return end
		CUIManager.ShowUI(CLuaUIResources.DaMoWangEnterWnd)
	end
end

g_UIActionShowCallback["DaMoWangResultWnd"]=function (windowName, methodName, param1, param2, param3)
	if methodName == "show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.DaMoWangResultWnd)
	end
end

local CRealNameCheckMgr = import "L10.Game.CRealNameCheckMgr"
g_UIActionShowCallback["OpenRealNameWndWithCloseBtn"] = function (windowName, methodName)
  if methodName == "show_wnd" then
    CRealNameCheckMgr.EnableRealNameCheck = false
    CRealNameCheckMgr.Inst:OpenCertification()
  end
end

g_UIActionShowCallback["JoinYaYunView"] = function (windowName, methodName)
    if methodName == "show_wnd" then
        Gac2Gas.QueryMyCityData(EnumQueryCityDataAction.eJoinYaYun)
    end
end

g_UIActionShowCallback["LiangHaoPurchaseWnd"] = function (windowName, methodName)
    if methodName == "show_wnd" then
        LuaLiangHaoMgr.ShowLiangHaoPurchaseWnd()
    end
end

g_UIActionShowCallback["ZhouBianMallShopWebView"] = function (windowName, methodName)
	if methodName == "show_wnd" then
		CLuaShopMallMgr:OpenZhouBianMallShop()
	end
end

g_UIActionShowCallback["CommunityWnd"] = function (windowName, methodName)
	if methodName == "show_wnd" then
		CUIManager.ShowUI(CUIResources.CommunityWnd)
	end
end

g_UIActionShowCallback["LuaStarBiwuJingCaiWnd"] = function (windowName, methodName)
	if methodName == "show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.StarBiwuJingCaiWnd)
	end
end

g_UIActionShowCallback["guildvoteWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName == "show_wnd" then
		if CClientMainPlayer.Inst.BasicProp.GuildId ~= 0  then		--	加入帮会才能打开投票界面
			CUIManager.ShowUI(CLuaUIResources.GuildVoteWnd)
		else
            g_MessageMgr:ShowMessage("Guild_Vote_No_Guild")
		end
	elseif methodName=="show_detailwnd" then
        Gac2Gas.RequestYuanDanGuildVoteQuestion(param1)
	end
end

g_UIActionShowCallback["SanxingShowWnd"] = function (windowName, methodName)
	if methodName == "show_wnd" then
    Gac2Gas.QuerySanXingBattleFreeMatchInfo()
	end
end

g_UIActionShowCallback["BaoZhuPlayWnd"]=function(windowName, methodName)
	if methodName == "show_wnd" then
    LuaBaoZhuPlayMgr.OpenPlayWnd()
	end
end

g_UIActionShowCallback["QingMingCGYJMainWnd"]=function(windowName, methodName)
	if methodName == "show_wnd" then
    Gac2Gas.QueryQingMing2021TuJianInfo()
	end
end

g_UIActionShowCallback["HuaYiGongShangWnd"]=function(windowName, methodName)
	if methodName == "show_wnd" then
		LuaWuMenHuaShiMgr:OpenHuaYiGongShangWnd()
	end
end

g_UIActionShowCallback["GuidelineWnd"]=function(windowName, methodName)
	CUIManager.ShowUI("GuidelineWnd")
end

g_UIActionShowCallback["MapiView"] = function(windowName, methodName, param1, param2)
	if methodName == "show_wnd" then
		local zuoqiId = param1
		if CZuoQiMgr.Inst:GetZuoQiById(zuoqiId) then
			CZuoQiMgr.Inst.curSelectedZuoqiId = zuoqiId
			CZuoQiMgr.Inst.curSelectedZuoqiTid = ZuoQi_Setting.GetData().MapiTemplateId
		end
		CZuoQiMgr.TabIndex = 1
		CUIManager.ShowUI(CUIResources.VehicleMainWnd)
	end
end

g_UIActionShowCallback["TcjhMainWnd"] = function(windowName, methodName, param1, param2)
	Gac2Gas.QueryTianChengData()
end

g_UIActionShowCallback["TsjzMainWnd"] = function(windowName, methodName, param1, param2)
	LuaHanJiaMgr.RequireTsjzData()
end

g_UIActionShowCallback["ZongMenCreate"] = function(windowName, methodName, param1, param2)
	LuaZongMenMgr:OpenZongMenCreateIntroductionWnd()
end

g_UIActionShowCallback["FengHuaLuWnd"] = function(windowName, methodName, param1, param2)
	if methodName == "show_FengHuaLuid" then
		LuaWorldEventMgr2021.ShowFengHuaLuID(param1)
	else
		LuaWorldEventMgr2021.ShowFengHuaLu()
	end
end

g_UIActionShowCallback["FruitPartySignWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.FruitPartySignWnd)
end

g_UIActionShowCallback["ShiTuFruitPartySignWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaShiTuMgr:ShowShiTuFruitPartySignWnd()
end


g_UIActionShowCallback["BusinessMainWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaBusinessMgr:TryOpenBusinessWnd()
end

g_UIActionShowCallback["BusinessRankWnd"] = function (windowName, methodName, param1, param2, param3)
	CRankData.Inst.InitRankId = 41000254
	if IsOpenNewRankWnd() then
	    CUIManager.ShowUI(CLuaUIResources.NewRankWnd)
	else
		CUIManager.ShowUI(CUIResources.RankWnd)
	end
end

g_UIActionShowCallback["assistantUrl"] = function (windowName, methodName, param1, param2)
  if not methodName then
    return
  end

  if not param1 or param1 == '' then
    return
  end


  Gac2Gas2.QuerySmartGMTokenAndHost(param1)
end

g_UIActionShowCallback["HuiGuiJieBanWnd"] = function(windowName, methodName)
	if methodName == "show_wnd" then
		Gac2Gas.RequestOpenHuiGuiJieBanWnd()
	end
end

g_UIActionShowCallback["CityWarAwardWnd"] = function(windowName, methodName)
	if methodName == "show_wnd" then
		Gac2Gas.QueryTerritoryOccupyAwardList()
	end
end

g_UIActionShowCallback["guildwnd"] = function(windowName, methodName)
	if methodName == "show_info" then--<link ui=帮会信息,guildwnd,show_info>
		CGuildMgr.Inst:ShowGuildMainWnd(0)
	elseif methodName == "show_member" then--<link ui=帮会成员,guildwnd,show_member>
		CGuildMgr.Inst:ShowGuildMainWnd(1)
	elseif methodName == "show_activity" then--<link ui=帮会活动,guildwnd,show_activity>
		CGuildMgr.Inst:ShowGuildMainWnd(2)
	elseif methodName == "show_benefit" then--<link ui=帮会福利,guildwnd,show_benefit>
		CGuildMgr.Inst:ShowGuildMainWnd(3)
	elseif methodName == "show_change_guild" then--<link ui=我要换帮,guildwnd,show_change_guild>
		CLuaGuildMgr.ShowChangeGuild()
	elseif methodName == "noguild" then--
		CUIManager.ShowUI(CUIResources.GuildWnd)
	end
end

g_UIActionShowCallback["CrossDouDiZhuApplyWnd"] = function(wnaName,methodName)
	if methodName == "show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuApplyWnd)
	end
end

g_UIActionShowCallback["CrossDouDiZhuRankWnd"] = function(wnaName,methodName)
	if methodName == "show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuRankWnd)
	end
end

g_UIActionShowCallback["ChunJieSeriesTaskWnd"] = function(wnaName,methodName)
	if methodName == "show_wnd" then
		CLuaCrossDouDiZhuRankWnd.m_IsAllRank = true
		CUIManager.ShowUI(CLuaUIResources.ChunJieSeriesTaskWnd)
	end
end
g_UIActionShowCallback["StarBiwuRewardWnd"] = function(wnaName,methodName)
	CUIManager.ShowUI(CLuaUIResources.StarBiwuRewardWnd)
end
g_UIActionShowCallback["StarBiwuAgendaWnd"] = function(wnaName,methodName)
	CLuaStarBiwuMgr:OpenStarBiWuZhanDuiWnd(3)
end

g_UIActionShowCallback["StarBiwuChampionHistoryWnd"] = function(wnaName,methodName)
	if methodName == "show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.StarBiwuChampionHistoryWnd)
	end
end

g_UIActionShowCallback["JieZiXiaoChouWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.QingMingJieZiXiaoChouWnd)
	end
end

g_UIActionShowCallback["HuanJingMiZongWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.QingMingSignRankWnd)
	end
end

g_UIActionShowCallback["ButterflyCrisisMainWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.ButterflyGetUIStatus()
end

g_UIActionShowCallback["SpokesmanHireContractWnd"]=function (windowName, methodName, param1, param2, param3)
	CLuaSpokesmanMgr:OpenSpokesmanHireContractWnd()
end

g_UIActionShowCallback["SpokesmanCardsWnd"]=function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.SpokesmanCardsWnd)
end

g_UIActionShowCallback["OpenNewWelfareWnd"]=function (windowName, methodName, param1, param2, param3)
  CUIManager.ShowUI(CUIResources.WelfareWnd)
end

g_UIActionShowCallback["SpokesmanHouseZhongChou"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.SpokesmanHouseZhongChouWnd)
end

g_UIActionShowCallback["GuildWarsEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaJuDianBattleMgr.m_ShowTianDiEnterWnd = true
	LuaJuDianBattleMgr:OpenGuildWarsEnterWnd()
end

g_UIActionShowCallback["JuDianBattleEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaJuDianBattleMgr.m_ShowTianDiEnterWnd = false
    LuaJuDianBattleMgr:OpenGuildWarsEnterWnd()
end

g_UIActionShowCallback["HaoYiXingCardWnd"] = function (windowName, methodName, param1, param2, param3)
	if not LuaHaoYiXingMgr.m_IsPlayOpen then return end
	CUIManager.ShowUI(CLuaUIResources.HaoYiXingCardsWnd)
end

--食谱烹饪
g_UIActionShowCallback["CookBookWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.ShangShiCookbookData()
end

g_UIActionShowCallback["SpokesmanPawnshopWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.SpokesmanPawnshopWnd)
end

g_UIActionShowCallback["XYLPWnd"] = function (windowName, methodName, param1, param2, param3)
	ZhongYuanJie2020Mgr:OpenXYLPWnd()
end

g_UIActionShowCallback["YNZJQueryRank"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.YNZJQueryRank()
end

g_UIActionShowCallback["Halloween2021GamePlayWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.OpenJieLiangYuanSignUpWnd()
end

g_UIActionShowCallback["CrossServerColiseumMainWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaColiseumMgr.m_LastGameType = 1
	CUIManager.ShowUI(CLuaUIResources.CrossServerColiseumMainWnd)
end

g_UIActionShowCallback["SpokesmanZhouBianWindow"] = function (windowName, methodName, param1, param2, param3)
	CWelfareMgr.OpenWelfareWnd(LocalString.GetString("萌物杂货铺"))
end

g_UIActionShowCallback["OpenCarnival2020Ticket"]=function (windowName, methodName, param1, param2, param3)
	CWelfareMgr.OpenWelfareWnd(LocalString.GetString("嘉年华门票"))
end

g_UIActionShowCallback["OpenCarnival2021Ticket"]=function (windowName, methodName, param1, param2, param3)
	CWelfareMgr.OpenWelfareWnd(LocalString.GetString("嘉年华门票"))
end

g_UIActionShowCallback["OpenCarnival2020Fuli"]=function (windowName, methodName, param1, param2, param3)
	CWelfareMgr.OpenWelfareWnd(LocalString.GetString("嘉年华福利"))
end

g_UIActionShowCallback["Shuangshiyi2020ZhongCao"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020ZhongCaoWnd)
end

g_UIActionShowCallback["Shuangshiyi2020Lottery"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020LotteryWnd)
end

g_UIActionShowCallback["Shuangshiyi2020Voucher"]=function (windowName, methodName, param1, param2, param3)
	CLuaShuangshiyi2020Mgr.UseVoucher()
end

g_UIActionShowCallback["Shuangshiyi2020ClearTrolley"]=function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2020ClearTrolleyEnterWnd)
end

g_UIActionShowCallback["ShuangShiYi2022MainWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2022MainWnd)
end

g_UIActionShowCallback["MengQuanHengXingSignUpWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.MengQuanHengXingSignUpWnd)
end

g_UIActionShowCallback["ShenZhaiTanBaoSignUpWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.ShenZhaiTanBaoSignUpWnd)
end

g_UIActionShowCallback["PetAdventureStartWnd"] = function (windowName, methodName, param1,param2, param3)
	if methodName == "show_wnd" then
		Gac2Gas.OpenHanJiaAdventureSignUpWnd()
	end
end

g_UIActionShowCallback["XueJingKuangHuanStartWnd"] = function (windowName, methodName, param1,param2, param3)
	if methodName == "show_wnd" then
		LuaHanJiaMgr:OpenXueJingKuangHuanStartWnd()
	end
end

g_UIActionShowCallback["GuildPuzzleEnterWnd"] = function (windowName, methodName, param1,param2, param3)
	Gac2Gas.QueryGuildPinTuData()
end

g_UIActionShowCallback["QingRenJie2021RankWnd"] = function(wnd, data)
	Gac2Gas.QueryYiAiZhiMingSendFlowerRank()
end
g_UIActionShowCallback["OlympicGamesGambleWnd"] = function(wnd, data)
	LuaOlympicGamesMgr:RequsetOlymmpicGambleWnd()
end
g_UIActionShowCallback["OlympicWinterGamesSnowBattleEnterWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.OlympicWinterGamesSnowBattleEnterWnd)
end

g_UIActionShowCallback["TrainNpcFavorWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		Gac2Gas.QueryMySectInviteNpcData()
	end
end

g_UIActionShowCallback["YuanXiaoRiddle2022Wnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.RequestOpenYuanXiaoRiddle2022Wnd()
end

g_UIActionShowCallback["QueQiaoXianQuEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	--把旧版本的接口 也直接打开新的界面 2023七夕
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.QiXi2023QueQiaoXianQuEnterWnd)
	end
end

g_UIActionShowCallback["QiXi2023QueQiaoXianQuEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.QiXi2023QueQiaoXianQuEnterWnd)
	end
end

g_UIActionShowCallback["LuoCha2021EnterWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.OpenLuoChaHaiShiSignUpWnd()
end

g_UIActionShowCallback["TianJiangBaoXiangWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.TianJiangBaoXiang_OpenWnd(tostring(EnumTianJiangBaoXiangDoor.None))
end

g_UIActionShowCallback["Christmas2021MengJingWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		LuaChristmas2021Mgr.OpenMJSYPlayWnd()
	end
end
g_UIActionShowCallback["MXLMusicPlayWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		LuaMeiXiangLouMgr.OpenMusicPlaySignWnd()
	end
end

g_UIActionShowCallback["ZongPaiLingFuRankWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.ZongPaiLingFuRankWnd)
end


g_OpenWndCallback["ZuiMengLuWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ZuiMengLuWnd)
end

g_OpenWndCallback["HMLZEnterWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.HMLZEnterWnd)
end

g_UIActionShowCallback["SchoolRebuildingWnd"] = function (windowName, methodName)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.SchoolRebuildingWnd)
	end
end

--2021圣诞
g_UIActionShowCallback["Christmas2021XingYuSignWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		LuaChristmas2021Mgr.OpenXingYuSignUpWnd()
	end
end

g_UIActionShowCallback["ZhaiXingEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.ZhaiXingEnterWnd)
	end
end

g_UIActionShowCallback["GuildLeagueCrossPosExchangeApplyWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		LuaGuildLeagueCrossMgr:QueryGLCGuildPosInfo() --服务器只提供了打开帮会位置互换界面，如果存在申请有红点提示
	end
end

--烟花众筹
g_UIActionShowCallback["FireWorkPartyZhongChouWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		LuaFireWorkPartyMgr.OpenZhongChouWnd()
	end
end

g_UIActionShowCallback["ShenYaoLeftTimeWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		CUIManager.ShowUI(CLuaUIResources.ShenYaoLeftTimeWnd)
	end
end

-- 跨服高昌
g_UIActionShowCallback["GaoChangCrossEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaGaoChangCrossMgr:QueryApplyInfo()
end

g_UIActionShowCallback["JuDianBattleInfoWnd"] = function(windowName, methodName, param1, param2, param3)
    LuaJuDianBattleMgr:OpenInfoWnd(0)
end

g_UIActionShowCallback["GuanYaoJianWnd"] = function(windowName, methodName, param1, param2, param3)
    Gac2Gas.GuanYaoJian_OpenWnd()
end

g_UIActionShowCallback["ShanYaoJieYuanWnd"] = function(windowName, methodName, param1, param2, param3)
    Gac2Gas.YaoBan_OpenWnd()
end

g_UIActionShowCallback["QianYingChuWenWnd"] = function(windowName, methodName, param1, param2, param3)
    LuaQianYingChuWenMgr:OpenQianYingChuWenWnd()
end

-- 通用图文说明
g_UIActionShowCallback["CommonTextImageRuleWnd"]=function (windowName, methodName, param1, param2, param3)
	if methodName == "show_wnd" then
		local picRuleId  = tonumber(param1)
		LuaCommonTextImageRuleMgr:ShowWnd(picRuleId)
	end
end

g_UIActionShowCallback["GuildExternalAidCardWnd"]=function (windowName, methodName, param1, param2, param3)
	LuaGuildExternalAidMgr:ShowGuildExternalAidCardWnd()
end

g_UIActionShowCallback["HmtBindWnd"]=function (windowName, methodName, param1, param2, param3)
	if CommonDefs.IsPCGameMode() then
		g_MessageMgr:ShowMessage("Bind_Only_On_The_Phone")
	else
		CUIManager.ShowUI(CLuaUIResources.HmtBindWnd)
	end
end

g_UIActionShowCallback["VoiceAchievementWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName=="show_wnd" then
		LuaVoiceAchievementMgr.OpenAchievementWnd()
	end
end

g_UIActionShowCallback["GSNZEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.GSNZEnterWnd)
end


g_UIActionShowCallback["GuildTerritoryWarTeleportToMyCity"] = function (windowName, methodName, param1, param2, param3)
	LuaGuildTerritorialWarsMgr:TeleportToMyCity()
end
g_UIActionShowCallback["GuildTerritorialWarsBeiZhanWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsBeiZhanWnd)
end
g_UIActionShowCallback["GuildTerritorialWarsZhanLongWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsZhanLongWnd)
end
g_UIActionShowCallback["GuildTerritorialWarsJingYingSettingWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.RequestGTWChallengeMemberInfo(true)
end
g_UIActionShowCallback["LuaJuDianBattleJingYingSettingWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.RequestGuildJuDianChallengeMemberInfo(true)
end
g_UIActionShowCallback["EquipmentProcessWnd"] = function (windowName, methodName, param1, param2, param3)
	CEquipmentProcessMgr.Show(0, 0)
end

g_UIActionShowCallback["SnowAdventureSnowManWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.RequestXuePoLiXianData()
	LuaSnowAdventureSnowManWnd.ShowCountdownPattern = false
	CUIManager.ShowUI(CLuaUIResources.SnowAdventureSnowManWnd)
end

g_UIActionShowCallback["TrackZongmen"] = function (windowName, methodName, param1, param2, param3)
	local zongMenCreated = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId ~= 0
	if zongMenCreated then
		if not LuaZongMenMgr:IsMySectScene() then
			if CClientMainPlayer.Inst then
				Gac2Gas.RequestEnterSelfSectScene(CClientMainPlayer.Inst.BasicProp.SectId)
			end
		else
			CTrackMgr.Inst:FindNPC(20005111, Constants.JinLing, 0, 0, nil, nil)
		end
	else
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你没有宗派，请先加入一个宗派！"))
	end
end

g_UIActionShowCallback["PackageWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CUIResources.PackageWnd)
end

g_UIActionShowCallback["PengDaoDialogWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.PDFY_RequestOpenMainWnd()
end

g_UIActionShowCallback["SnowAdventureSingleStageMainWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.RequestXuePoLiXianData()
	LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId = nil
	CUIManager.ShowUI(CLuaUIResources.SnowAdventureSingleStageMainWnd)
end

g_UIActionShowCallback["SnowAdventureMultipleStageMainWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.RequestXuePoLiXianData()
	LuaSnowAdventureMultipleStageMainWnd.FirstOpenStageId = nil
	CUIManager.ShowUI(CLuaUIResources.SnowAdventureMultipleStageMainWnd)
end

g_UIActionShowCallback["SnowManPVEPlayWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.BingTianShiLianRequestData()
	CUIManager.ShowUI(CLuaUIResources.SnowManPVEPlayWnd)
end

g_UIActionShowCallback["NanDuScrollWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName == "nanduscrollwnd_chuansuo" then
		LuaYiRenSiMgr:OpenScroll(1)
	elseif methodName == "nanduscrollwnd_show" then
		LuaYiRenSiMgr:OpenScroll(2)
	end
end

g_UIActionShowCallback["HengYuDangKouTeamSelectWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaHengYuDangKouMgr:OpenTeamSelectWnd()
end
g_UIActionShowCallback["HengYuDangKouOpenWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.HengYuDangKouOpenWnd)
end

g_OpenWndCallback["OpenNanDuLordWayWnd"] = function(wnd, data)
	LuaNanDuFanHuaLordMainWnd.OpenDefaultTabIndex = 2
	CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaLordMainWnd)
end

g_OpenWndCallback["NanDuScrollWnd"] = function(wnd, data)
	if data then
		local dict = MsgPackImpl.unpack(data)
		if not dict then return end
		local list = dict["args"]
		if list and list.Count==1 then
			local animationName = tostring(list[0])
			if animationName == "nanduscrollwnd_chuansuo" then
				LuaYiRenSiMgr:OpenScroll(1)
			elseif animationName == "nanduscrollwnd_show" then
				LuaYiRenSiMgr:OpenScroll(2)
			end
		end
	end
end

g_OpenWndCallback["ShowWorldMap"] = function(wnd, data)
	CLuaMiniMapPopWnd.m_ShowWorldMap = true--直接打开世界地图
	CUIManager.ShowUI(CUIResources.MiniMapPopWnd)
end

g_OpenWndCallback["NanDuFanHuaBargainWnd"] = function(wnd, data)
	if data then
		local dict = MsgPackImpl.unpack(data)
		if not dict then return end
		local list = dict["args"]
		if list then
			local bargainId = tonumber(list)
			LuaYiRenSiMgr:OpenBargain(bargainId)
		end
	end
end

g_OpenWndCallback["NanDuFanHuaCookBookWnd"] = function(wnd, data)
	if data then
		local dict = MsgPackImpl.unpack(data)
		if not dict then return end
		local list = dict["args"]
		if list then
			local bookId = tonumber(list)
			LuaNanDuFanHuaCookBookWnd.DefaultOpenTab = tonumber(bookId) - 1
			CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaCookBookWnd)
		end
	end
end

g_OpenWndCallback["FireWorkPartyZhongChouWnd"] = function(wnd, data)
	LuaFireWorkPartyMgr.OpenZhongChouWnd()
end

g_OpenWndCallback["ZhengWuLuWnd"] = function(wnd, data)
	LuaZhengWuLuMgr.OpenMainWnd()
end

g_OpenWndCallback["EntrustWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.EntrustWnd)
end

g_OpenWndCallback["KanYiDaoEnterWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.KanYiDaoEnterWnd)
end

g_OpenWndCallback["XinShengHuaJiDrawWnd"] = function(wnd, data)
	LuaWuMenHuaShiMgr:QueryOpenXinShengHuaJiDrawWnd(data)
end

g_OpenWndCallback["XinShengHuaJiPaintSetWnd"] = function(wnd, data)
	LuaWuMenHuaShiMgr:QueryOpenXinShengHuaJiPaintSetWnd()
end

g_OpenWndCallback["WeddingSceneSelectWnd"] = function(wnd, data)
	LuaWeddingIterationMgr:QueryOpenSceneSelectWnd(data)
end

g_OpenWndCallback["NewWeddingSetAnswer"] = function(wnd, data)
	LuaWeddingIterationMgr:OpenFakeBrideWnd("NewWeddingSetAnswer", data)
end

g_OpenWndCallback["NewWeddingConversation"] = function(wnd, data)
	LuaWeddingIterationMgr:OpenFakeBrideWnd("NewWeddingConversation", data)
end

g_OpenWndCallback["NewWeddingConversationFemale"] = function(wnd, data)
	LuaWeddingIterationMgr:OpenFakeBrideWnd("NewWeddingConversationFemale", data)
end

g_OpenWndCallback["WuMenHuaShiDrawLotsWnd"] = function(wnd, data)
	LuaWuMenHuaShiMgr:OpenDrawLotsWnd(data)
end

g_OpenWndCallback["GuildLeagueCrossMatchInfoWnd"] = function(wnd, data)
	LuaGuildLeagueCrossMgr:ShowMatchInfoWnd()
end

g_OpenWndCallback["GuildLeagueTrainWnd"] = function(wnd, data)
	LuaGuildLeagueCrossMgr:OpenGuildLeagueTrainWnd(data)
end

g_OpenWndCallback["GuildLeagueBuffTaskWnd"] = function(wnd, data)
	LuaGuildLeagueCrossMgr:OpenGuildLeagueBuffTaskWnd(data)
end

g_OpenWndCallback["GuildLeagueCrossTrainSubmitWnd"] = function(wnd, data)
	LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainInfo()
end

g_OpenWndCallback["GuildLeagueStateWnd"] = function(wnd, data)
	CUIManager.ShowUI(CUIResources.GuildLeagueBattleStateWnd)
end

g_OpenWndCallback["RuMengJiWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.RuMengJiWnd)
end

g_OpenWndCallback["WeddingFakeBrideWnd"] = function(wnd, data)
	LuaWeddingIterationMgr:StartNewWeddingConversation()
end

g_OpenWndCallback["XinShengHuaJiPictureShowWnd"] = function(wnd, data)
	LuaWuMenHuaShiMgr:ShowPictureWnd(data)
end

g_OpenWndCallback["NewWeddingDeclare"] = function(wnd, data)
	LuaWeddingIterationMgr:OpenPromiseWnd(data)
end

g_OpenWndCallback["NewWeddingFillInDiskWnd"] = function(wnd, data)
	LuaWeddingIterationMgr:OpenFeastWnd(data)
end

g_OpenWndCallback["WeddingFireworkSelectWnd"] = function(wnd, data)
	LuaWeddingIterationMgr:OpenFireworkSelectWnd()
end

g_OpenWndCallback["NewWeddingXiuQiuMsgWnd"] = function(wnd, data)
	LuaWeddingIterationMgr:OpenFlowerBallWnd(data)
end

g_OpenWndCallback["YuanXiaoRiddle2022Wnd"] = function(wnd, data)
	LuaYuanXiao2022Mgr:OpenWnd(data)
end

g_OpenWndCallback["WuYi2022AcceptTaskWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.WuYi2022AcceptTaskWnd)
end

g_OpenWndCallback["StarBiwuChampionWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.StarBiwuChampionWnd)
end

g_OpenWndCallback["YinHunPoZhiEnterWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YinHunPoZhiEnterWnd)
end

g_OpenWndCallback["HuaYiGongShangWnd"] = function(wnd, data)
	LuaWuMenHuaShiMgr:OpenHuaYiGongShangWnd()
end

g_OpenWndCallback["ShanYaoJieYuanWnd"] = function(wnd, data)
	LuaXinBaiLianDongMgr:ShowShanYaoJieYuanWnd(data)
end

g_OpenWndCallback["DouDiZhuBaoXiangApplyWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.DouDiZhuBaoXiangApplyWnd)
end

g_OpenWndCallback["GuanYaoJianWnd"] = function(wnd, data)
	LuaXinBaiLianDongMgr:ShowGuanYaoJianWnd(data)
end

g_OpenWndCallback["ShenYaoLeftTimeWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ShenYaoLeftTimeWnd)
end

g_OpenWndCallback["ShuiManJinShanSoloWnd"] = function(wnd, data)
	LuaShuiManJinShanMgr:OpenSoloWnd()
end

g_OpenWndCallback["ShuiManJinShanGroupWnd"] = function(wnd, data)
	local difficulty = 1
	local raw = MsgPackImpl.unpack(data)
    local args = raw["args"]
	if raw and raw.Count >= 1 then
		difficulty = tonumber(args[0]) 
	end
	LuaShuiManJinShanMgr:OpenGroupWnd(difficulty)
end

g_OpenWndCallback["QiXi2022EnterWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QiXi2022EnterWnd)
end

g_OpenWndCallback["QiXi2022ScheduleWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.QiXi2022ScheduleWnd)
end

g_OpenWndCallback["QiXi2022PlotWnd"] = function(wnd, data)
	local raw = MsgPackImpl.unpack(data)
    -- local args = raw["args"]
	if raw and raw.Count >= 2 then
		LuaQiXi2022Mgr.m_HasOpenQiXi2022PlotWnd = not raw[0]
		LuaQiXi2022Mgr.m_ClosePlotWndTime = tonumber(raw[1])
	end
	CUIManager.ShowUI(CLuaUIResources.QiXi2022PlotWnd)
end

g_OpenWndCallback["QiXi2022ReturnGoodsWnd"] = function(wnd, data)
	LuaQiXi2022Mgr:OpenQiXi2022ReturnGoodsWnd(data)
end

g_OpenWndCallback["ChuShiPosterWnd"] = function(wnd, data)
	LuaShiTuMgr:ShowChuShiPosterWndWithCreateAni(data)
end

g_OpenWndCallback["WuLiangLevelWnd"] = function(wnd, data)
	Gac2Gas.WuLiangShenJing_OpenElevatorWnd()
end

g_OpenWndCallback["WorldCup2022GJZLSelectWnd"]=function(wndName, data)
	LuaWorldCup2022Mgr:OpenGJZLSelectWnd(data)
end

g_OpenWndCallback["CuJuSelectWnd"]=function(wndName, data)
	LuaCuJuMgr:OpenCuJuSelectWnd(data)
end

--服务器RPC发起的OpenWnd
g_OpenWndCallback["YuYuanMengJi"] = function(wnd, data)
    CUIManager.ShowUI(CLuaUIResources.YuYuanMengJiWnd)
end

g_OpenWndCallback["YuanXiao2023AddRiddleWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	local npcEngineId
	if data and CommonDefs.IsDic(data) then
		if CommonDefs.DictContains_LuaCall(data, "args") then
			data = data["args"]
			if data and data.Count >= 1 then
				npcEngineId = tonumber(data[0])
			end
		end
	else
		if data and data.Count >= 1 then
			npcEngineId = tonumber(data[0])
		end
	end
	LuaYuanXiao2023AddRiddleWnd.s_npcEngineId = npcEngineId
	CConversationMgr.Inst:CloseDialog()
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2024AddRiddleWnd)
end

g_OpenWndCallback["YuanXiao2023GuessRiddleWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	local npcEngineId, riddleId, senderId, senderName
	if data and CommonDefs.IsDic(data) then
		if CommonDefs.DictContains_LuaCall(data, "args") then
			data = data["args"]
			if data and data.Count >= 1 then
				npcEngineId, riddleId, senderId, senderName = tonumber(data[0]), tonumber(data[1]), tonumber(data[2]), data[3]
			end
		end
	else
		if data and data.Count >= 1 then
			npcEngineId, riddleId, senderId, senderName = tonumber(data[0]), tonumber(data[1]), tonumber(data[2]), data[3]
		end
	end
	LuaYuanXiao2023GuessRiddleWnd.s_npcEngineId = npcEngineId
	LuaYuanXiao2023GuessRiddleWnd.s_riddleId = riddleId
	LuaYuanXiao2023GuessRiddleWnd.s_senderId = senderId
	LuaYuanXiao2023GuessRiddleWnd.s_senderName = senderName
	CConversationMgr.Inst:CloseDialog()
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2024GuessRiddleWnd)
end

g_OpenWndCallback["YuanXiao2023RiddleRankWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2023RiddleRankWnd)
end

g_OpenWndCallback["StartChristmasKindnessGuildPlayWnd"] = function(wnd, data)
	LuaChristmas2022Mgr:StartChristmasKindnessGuildPlay()
end

g_OpenWndCallback["YuanXiao2023QiYuanWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2023QiYuanWnd)
end

g_OpenWndCallback["PengDaoDialogWnd"] = function(wnd, data)
	Gac2Gas.PDFY_RequestOpenMainWnd()
end

g_OpenWndCallback["ZhuoYaoCaptureView"] = function(wnd, data)
	if LuaZongMenMgr.m_OpenNewSystem then
		LuaZhuoYaoMgr.m_ZhuoYaoMainWndTab = 1
	end
	if (CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.MingGe > 0) then
		CUIManager.ShowUI(CLuaUIResources.ZhuoYaoMainWnd)
	else
		g_MessageMgr:ShowMessage("Open_ZhuoYaoCaptureView_NoneMingGe")
	end
end

g_OpenWndCallback["LiuRuShiTaskFailWnd"] = function(wnd, data)
	LuaLiuRuShiMgr:TaskFail(data)
end

g_OpenWndCallback["WuYi2023XXSCMainWnd"] = function(wnd, data)
	Gac2Gas.XinXiangShiChengQueryLotteryInfo()
end

g_UIActionShowCallback["WuYi2023XXSCMainWnd"]=function (windowName, methodName)
	Gac2Gas.XinXiangShiChengQueryLotteryInfo()
end

g_OpenWndCallback["WuYi2023MainWnd"] = function(wnd, data)
	LuaCommonJieRiMgr.jieRiId = 29
	CUIManager.ShowUI(CLuaUIResources.WuYi2023MainWnd)
end

g_UIActionShowCallback["WuYi2023MainWnd"]=function (windowName, methodName)
	LuaCommonJieRiMgr.jieRiId = 29
	CUIManager.ShowUI(CLuaUIResources.WuYi2023MainWnd)
end

g_UIActionShowCallback["ZhuoYaoCaptureView"]=function (windowName, methodName)
	if LuaZongMenMgr.m_OpenNewSystem then
		LuaZhuoYaoMgr.m_ZhuoYaoMainWndTab = 1
	end
	CUIManager.ShowUI(CLuaUIResources.ZhuoYaoMainWnd)
end

g_UIActionShowCallback["ChunJie2023MainWnd"]=function (windowName, methodName)
	LuaChunJie2023Mgr:OpenChunJie2023MainWnd()
end

g_UIActionShowCallback["Valentine2023MainWnd"]=function (windowName, methodName, param1, param2, param3)
	LuaValentine2023Mgr:ShowValentine2023MainWnd()
end

g_UIActionShowCallback["Valentine2023YWQSWnd"]=function (windowName, methodName, param1, param2, param3)
	LuaValentine2023Mgr:ShowValentine2023YWQSWnd(false)
end

g_OpenWndCallback["Valentine2023MainWnd"] = function(wnd, data)
	LuaValentine2023Mgr:ShowValentine2023MainWnd()
end

g_OpenWndCallback["Valentine2023YWQSWnd"] = function(wnd, data)
	LuaValentine2023Mgr:ShowValentine2023YWQSWnd(true)
end

g_OpenWndCallback["Valentine2023YWQSVoteWnd"] = function(wnd, data)
	LuaValentine2023Mgr:ShowValentine2023YWQSVoteWnd(data)
end

g_UIActionShowCallback["YuanXiaoGaoChangEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.GaoChangQueryApplyInfo()
    Gac2Gas.GaoChangJiDouCrossQueryApplyInfo()
end

g_UIActionShowCallback["YuanXiao2023MainWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.YuanXiao2023MainWnd)
end

g_UIActionShowCallback["ShuJia2023MainWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.ShuJia2023MainWnd)
end

g_OpenWndCallback["HaiZhanChangePosWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.HaiZhanChangePosWnd)
end

g_OpenWndCallback["SkillBeginnerGuideWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) and CommonDefs.DictContains_LuaCall(data, "args") then
		data = data["args"]
	end
	if data and data.Count >= 1 then
		if CUIManager.IsLoaded(CLuaUIResources.SkillBeginnerGuideWnd) then
			g_ScriptEvent:BroadcastInLua("UpdateSkillGuideSequence", tonumber(data[0]))
		else
			LuaSkillBeginnerGuideWnd.castSkillSequenceId = tonumber(data[0])
			CUIManager.ShowUI(CLuaUIResources.SkillBeginnerGuideWnd)
		end
	end
end

g_OpenWndCallback["ShuJiaHaiZhanResultWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) then
		if CommonDefs.DictContains_LuaCall(data, "args") then
			data = data["args"]
			if data and data.Count >= 1 then
				LuaShuJiaHaiZhanResultWnd.s_Time = tonumber(data[0])
				LuaShuJiaHaiZhanResultWnd.s_GamePlayId = tonumber(data[1])
				LuaShuJiaHaiZhanResultWnd.s_IsHard = tonumber(data[2])
			end
		end
	else
		if data and data.Count >= 1 then
			LuaShuJiaHaiZhanResultWnd.s_Time = tonumber(data[0])
			LuaShuJiaHaiZhanResultWnd.s_GamePlayId = tonumber(data[1])
			LuaShuJiaHaiZhanResultWnd.s_IsHard = tonumber(data[2])
		end
	end

	if CTeamMgr.Inst:TeamExists() then
		CUIManager.ShowUI(CLuaUIResources.ShuJiaHaiZhanResultWnd)
	end
end

g_OpenWndCallback["HaiZhanPosSelectionWnd"] = function(wnd, data)
	local gameplayId
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) then
		if CommonDefs.DictContains_LuaCall(data, "args") then
			data = data["args"]
			if data and data.Count >= 1 then
				gameplayId = data[0]
			end
		end
	else
		if data and data.Count >= 1 then
			gameplayId = data[0]
		end
	end
	if gameplayId and gameplayId == NavalWar_Setting.GetData().TrainingSeaPlayId then
		Gac2Gas.GlobalMatch_RequestSignUp(gameplayId)
	else
		CUIManager.ShowUI(CLuaUIResources.HaiZhanPosSelectionWnd)
	end
end

g_OpenWndCallback["JuDianBattleZhanLongShopWnd"] = function(wnd,data)
	Gac2Gas.QueryGuildJuDianZhanLongShop()
end

g_UIActionShowCallback["MyTeamRecruitWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaTeamRecruitMgr:ShowMyTeamRecruitWnd(param1, param2)
end

g_UIActionShowCallback["PropertyRankWnd"] = function (windowName, methodName, param1, param2, param3)
	print("PropertyRankWnd",methodName,param1)
	LuaPlayerPropertyMgr:QueryPropRank(tonumber(param1))
end

g_OpenWndCallback["LiuYi2023MainWnd"] = function(wnd, data)
	LuaCommonJieRiMgr.jieRiId = 30
	CUIManager.ShowUI(CLuaUIResources.LiuYi2023MainWnd)
end

g_UIActionShowCallback["LiuYi2023MainWnd"]=function (windowName, methodName)
	LuaCommonJieRiMgr.jieRiId = 30
	CUIManager.ShowUI(CLuaUIResources.LiuYi2023MainWnd)
end

g_OpenWndCallback["QiXi2023MainWnd"] = function(wnd, data)
	LuaCommonJieRiMgr.jieRiId = 33
	CUIManager.ShowUI(CLuaUIResources.QiXi2023MainWnd)
end

g_UIActionShowCallback["QiXi2023MainWnd"]=function (windowName, methodName)
	LuaCommonJieRiMgr.jieRiId = 33
	CUIManager.ShowUI(CLuaUIResources.QiXi2023MainWnd)
end

g_OpenWndCallback["YaYunHui2023MainWnd"] = function(wnd, data)
	LuaCommonJieRiMgr.jieRiId = 38
	CUIManager.ShowUI(CLuaUIResources.YaYunHui2023MainWnd)
end

g_UIActionShowCallback["YaYunHui2023MainWnd"]=function (windowName, methodName)
	LuaCommonJieRiMgr.jieRiId = 38
	CUIManager.ShowUI(CLuaUIResources.YaYunHui2023MainWnd)
end

g_OpenWndCallback["ZhongQiu2023MainWnd"] = function(wnd, data)
	LuaCommonJieRiMgr.jieRiId = 37
	CUIManager.ShowUI(CLuaUIResources.ZhongQiu2023MainWnd)
end

g_UIActionShowCallback["ZhongQiu2023MainWnd"]=function (windowName, methodName)
	LuaCommonJieRiMgr.jieRiId = 37
	CUIManager.ShowUI(CLuaUIResources.ZhongQiu2023MainWnd)
end

g_OpenWndCallback["DuanWu2023PVPVESkillSelectWnd"] = function(wnd, data)
	LuaDuanWu2023Mgr:ShowPVPVESkillSelectWnd(data)
end

g_OpenWndCallback["QingMing2023MainWnd"] = function(wnd, data)
	LuaQingMing2023Mgr:ShowQingMing2023MainWnd()
end

g_UIActionShowCallback["QingMing2023MainWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaQingMing2023Mgr:ShowQingMing2023MainWnd()
end

g_OpenWndCallback["ZhouNianQingComposeTabenWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ZhouNianQingLookForKidComposeWnd)
end

g_OpenWndCallback["ZhouNianQingCarveTabenWnd"] = function(wnd, data)
	--[[local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, ZhouNianQing2023_Setting.GetData().TabenItemId)
	if not (default and pos and itemId) then
		return
	end
	if CClientMainPlayer.Inst then
		local ret, info = CommonDefs.DictTryGet_LuaCall(CClientMainPlayer.Inst.RelationshipProp.TempRelationData, 1)
		if not ret or info.ExpiredTime <= CServerTimeMgr.Inst.timeStamp then
			return 
		end
	else 
		return
	end--]]
	CUIManager.ShowUI(CLuaUIResources.ZhouNianQingCarveTabenWnd)
end

g_OpenWndCallback["ZhouNianQingTabenWnd"] = function(wnd, data)
	local infoDict = MsgPackImpl.unpack(data)
	if infoDict and CommonDefs.DictContains_LuaCall(infoDict, "ItemId") then
		local itemId = CommonDefs.DictGetValue_LuaCall(infoDict, "ItemId")
		CLuaDwWusesiTaben2020Wnd.OriID = itemId
		CLuaDwWusesiTaben2020Wnd.TitleStr = ZhouNianQing2023_PSZJSetting.GetData().PSZJ_Taben_Makeup
		CLuaDwWusesiTaben2020Wnd.WndTitleStr = LocalString.GetString("知己手镯拓本")
		CLuaDwWusesiTaben2020Wnd.ConfirmMsg = "SOULMATE_TABEN_CONFIRM"
		CLuaDwWusesiTaben2020Wnd.Gac2Gas = "Anniv2023PSZJ_AttachTabenToEquip"
		CUIManager.ShowUI(CLuaUIResources.DwWusesiTaben2020Wnd)
	end
end

g_OpenWndCallback["ZhouNianQingLifeFriendWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ZhouNianQingLifeFriendWnd)
end

g_OpenWndCallback["ZhouNianQingFSCRankWnd"] = function(wnd, data)
	Gac2Gas.Anniv2023FSC_QueryRankData()
end

g_OpenWndCallback["ShuJia2023WorldEventWnd"] = function(wnd, data)
	LuaShuJia2023Mgr:OpenWorldEventWnd()
end

g_UIActionShowCallback["ZhouNianQing2023MainWnd"] = function (windowName, methodName, param1, param2, param3)
	CUIManager.ShowUI(CLuaUIResources.ZhouNianQing2023MainWnd)
end

g_OpenWndCallback["LiuRuShiEndWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) and CommonDefs.DictContains_LuaCall(data, "args") then
		data = data["args"]
	end
	if data and data.Count >= 1 then
		LuaLiuRuShiMgr:ShowLiuRuShiEndWnd(tonumber(data[0]))
	end
end

g_UIActionShowCallback["StarBiwuReShenBattleStatusWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.QueryStarBiwuReShenSaiZK_QD()
end

g_OpenWndCallback["StarBiwuReShenBattleStatusWnd"] = function(wnd, data)
	Gac2Gas.QueryStarBiwuReShenSaiZK_QD()
end

g_UIActionShowCallback["StarBiwuQuDaoBattleStatusWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.QueryStarBiwuZhengShiSaiZK_QD()
end

g_OpenWndCallback["StarBiwuQuDaoBattleStatusWnd"] = function(wnd, data)
	Gac2Gas.QueryStarBiwuZhengShiSaiZK_QD()
end

local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
g_UIActionShowCallback["HongBaoWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName == "show_worldhongbao" then
		CHongBaoMgr.ShowHongBaoWnd(EnumHongbaoType.WorldHongbao)
	end
end

local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CHouseUIMgr = import "L10.UI.CHouseUIMgr"
local EHouseMainWndTab = import "L10.UI.EHouseMainWndTab"
g_UIActionShowCallback["HouseMainWnd"] = function (windowName, methodName, param1, param2, param3)
    if not CSwitchMgr.EnableHouse then
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end

    if CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse() then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("正在巫先生家园，无法查看自己的家园信息"))
        return
    end

    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.IsCrossServerPlayer then
            g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
            return
        end
        local houseid = CClientMainPlayer.Inst.ItemProp.HouseId
        if houseid ~= nil and houseid ~= "" then
            CHouseUIMgr.OpenHouseMainWnd(EHouseMainWndTab.EInfoTab)
        else
            local message = g_MessageMgr:FormatMessage("BUY_HOUSE_TIP")
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                CTrackMgr.Inst:FindNPC(Constants.JiayuanNpcId, Constants.HangZhouID, 89, 38, nil, nil)
            end), nil, LocalString.GetString("前往"), LocalString.GetString("取消"), false)
        end
    end
end

g_UIActionShowCallback["ShoppingMallWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName == "show_lingyumall" then
		CShopMallMgr.ShowLinyuShoppingMall(param1)
	elseif methodName == "buy_monthcard" then
		CChargeMgr.Inst:BuyMonthCardForMyself()
	elseif methodName == "buy_bigmonthcard" then
		LuaWelfareMgr.BuyBigMonthCardForMyself()
	end
end

local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
g_UIActionShowCallback["SkillWnd"] = function (windowName, methodName, param1, param2, param3)
	if methodName == "show_livingskill" then
		CSkillInfoMgr.ShowLivingSkillWnd()
	end
end

g_OpenWndCallback["PinchFaceModifyWnd"] = function(wnd, data)
	LuaPinchFaceMgr:OpenPinchFaceModifyWnd()
end

g_UIActionShowCallback["WuYi2023JZLYBaoLingDan"] = function (windowName, methodName, param1, param2, param3)
	if CClientMainPlayer.Inst ~= nil then
		local itemId = CClientMainPlayer.Inst.HasFeiSheng and 21004564 or 21000709
        CShopMallMgr.ShowLinyuShoppingMall(itemId)
    end
end

g_UIActionShowCallback["CommonJieRiMainWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaCommonJieRiMgr:ShowMainWnd(param1, param2)
end

g_OpenWndCallback["CompetitionHonorFameHallWnd"] = function(wnd, data)
	LuaCompetitionHonorMgr:OpenCompetitionHonorFameHallWnd()
end

g_UIActionShowCallback["CompetitionHonorFameHallWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaCompetitionHonorMgr:OpenCompetitionHonorFameHallWnd()
end

g_UIActionShowCallback["CommonPassportWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaCommonPassportMgr:ShowPassportWnd(tonumber(param1))
end

g_OpenWndCallback["ZhongYuanPuDuNoteWnd"] = function(wnd, data)
	local letter = LuaZhongYuanJie2023Mgr:GetLetterContent()
	CLuaLetterDisplayMgr.ShowLetterDisplayWnd(letter, nil,nil,nil,true)
end

g_OpenWndCallback["ZhongYuanPuDuHuaFuWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.ZYPDHuaFuWnd)
end

g_OpenWndCallback["FashionLotteryWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) and CommonDefs.DictContains_LuaCall(data, "args") then
		data = data["args"]
	end
	if data and data.Count >= 1 then
		LuaFashionLotteryMgr:OpenFashionLotteryWnd(data[0])
	end
end

g_UIActionShowCallback["FashionLotteryWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaFashionLotteryMgr:OpenFashionLotteryWnd(methodName)
end

g_OpenWndCallback["FashionLotterySplitWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) and CommonDefs.DictContains_LuaCall(data, "args") then
		data = data["args"]
	end
	if data and data.Count >= 2 then
		LuaFashionLotteryMgr:OpenFashionLotterySplitWnd(data[0], data[1])
	end
end

g_OpenWndCallback["FashionLotteryExchangeWnd"] = function(wnd, data)
	LuaFashionLotteryMgr:OpenFashionLotteryExchangeWnd()
end

g_UIActionShowCallback["FashionLotteryExchangeWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaFashionLotteryMgr:OpenFashionLotteryExchangeWnd()
end

g_UIActionShowCallback["LuoChaHaiShiEnterWnd"] = function (windowName, methodName, param1, param2, param3)
	Gac2Gas.QueryLuoChaHaiShi2023Info()
end

g_OpenWndCallback["GuoQing2023MainWnd"] = function(wnd, data)
	LuaGuoQing2023Mgr:OpenGuoQing2023MainWnd()
end

g_UIActionShowCallback["GuoQing2023MainWnd"] = function (windowName, methodName, param1, param2, param3)
	LuaGuoQing2023Mgr:OpenGuoQing2023MainWnd()
end

g_OpenWndCallback["Christmas2023XueHuaXuYuGetGiftWnd"] = function(wnd, data)
	LuaChristmas2023Mgr:QueryGift()
end

g_OpenWndCallback["Christmas2023GuaGuaCardWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) and CommonDefs.DictContains_LuaCall(data, "args") then
		data = data["args"]
	end
	local isPaid = false
	if data and CommonDefs.IsList(data) and data.Count >= 1 and tonumber(data[0]) > 0 then isPaid = true end
	LuaChristmas2023Mgr:OpenChristmas2023GuaGuaCardWnd(isPaid)
end

g_OpenWndCallback["Shuangshiyi2023MainWnd"] = function(wnd, data)
	LuaCommonJieRiMgr.jieRiId = 41
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023MainWnd)
end

g_UIActionShowCallback["Shuangshiyi2023LotteryWnd"] = function (windowName, methodName, param1, param2, param3)
	if not CScheduleMgr.Inst:IsCanJoinSchedule(42030322, true) then
		g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
		return
	end
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023LotteryWnd)
end

g_UIActionShowCallback["Shuangshiyi2023DiscountPackageWnd"] = function (windowName, methodName, param1, param2, param3)
	if not CScheduleMgr.Inst:IsCanJoinSchedule(42030324, true) then
		g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
		return
	end
	CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023DiscountPackageWnd)
end

g_OpenWndCallback["YuanDan2024BreakEggWnd"] = function(wnd, data)
	CUIManager.ShowUI(CLuaUIResources.YuanDan2024BreakEggWnd)
end

g_OpenWndCallback["Halloween2023OrderBattleRuleWnd"] = function(wnd, data)
	data = MsgPackImpl.unpack(data)
	if data and CommonDefs.IsDic(data) and CommonDefs.DictContains_LuaCall(data, "args") then
		data = data["args"]
	end
	if data and data.Count >= 3 then
		LuaHalloween2023Mgr:ShowHalloween2023OrderBattleRuleWnd(data[0], data[1], data[2])
	end
end

--#region 春节2024

g_OpenWndCallback["ShuangLongQiangZhuSelectWnd"] = function(wnd, data)
	LuaChunJie2024Mgr:ShowSLQZSelectWnd(data)
end

g_OpenWndCallback["ShuangLongQiangZhuWnd"] = function(wnd, data)
	LuaChunJie2024Mgr:ShowSLQZPlayWnd(data)
end

g_OpenWndCallback["ShuangLongQiangZhuRewardWnd"] = function(wnd, data)
	LuaChunJie2024Mgr:ShowSLQZRewardWnd(data)
end

g_OpenWndCallback["ShowSongQiongShenRewardWnd"] = function(wnd, data)
	LuaChunJie2024Mgr:ShowSQSRewardWnd(data)
end

--#endregion 春节2024

g_OpenWndCallback["YiRenSiDrawFan"] = function(wnd, data)
	LuaMakeFanWnd.fanData = nil
	LuaMakeFanWnd.duration = nil
	CUIManager.ShowUI(CLuaUIResources.MakeFanWnd)
end

g_UIActionShowCallback["YiRenSiDrawFan"] = function (windowName, methodName, param1, param2, param3)
	LuaMakeFanWnd.fanData = nil
	LuaMakeFanWnd.duration = nil
	CUIManager.ShowUI(CLuaUIResources.MakeFanWnd)
end
