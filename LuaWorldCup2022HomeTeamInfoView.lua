local DelegateFactory = import "DelegateFactory"
local CItemInfoMgr    = import "L10.UI.CItemInfoMgr"
local CUIFxPaths      = import "L10.UI.CUIFxPaths"
local Vector4         = import "UnityEngine.Vector4"

LuaWorldCup2022HomeTeamInfoView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "icon")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "name")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "state")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "groupInfo")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "groupInfo_WinDrawLose")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "groupInfo_Score")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "groupInfo_Rank")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "table")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "aChuWin")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "aChuSport")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "aChuFail")
RegistClassMember(LuaWorldCup2022HomeTeamInfoView, "aChuWinBig")

function LuaWorldCup2022HomeTeamInfoView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitAward()
end

function LuaWorldCup2022HomeTeamInfoView:InitUIComponents()
    self.icon = self.transform:Find("LeftTop/Icon"):GetComponent(typeof(CUITexture))
    self.name = self.transform:Find("LeftTop/Name"):GetComponent(typeof(UILabel))
    self.state = self.transform:Find("LeftTop/State"):GetComponent(typeof(UILabel))
    self.groupInfo = self.transform:Find("LeftTop/GroupInfo").gameObject
    self.groupInfo_WinDrawLose = self.transform:Find("LeftTop/GroupInfo/WinDrawLose"):GetComponent(typeof(UILabel))
    self.groupInfo_Score = self.transform:Find("LeftTop/GroupInfo/Score"):GetComponent(typeof(UILabel))
    self.groupInfo_Rank = self.transform:Find("LeftTop/GroupInfo/Rank"):GetComponent(typeof(UILabel))
    self.table = self.transform:Find("Panel/Table")

    self.aChuWin = self.transform:Find("Panel/AChu/win")
    self.aChuSport = self.transform:Find("Panel/AChu/sport")
    self.aChuFail = self.transform:Find("Panel/AChu/fail")
    self.aChuWinBig = self.transform:Find("vfx_achuwin")
end

-- 初始化奖励
function LuaWorldCup2022HomeTeamInfoView:InitAward()
    for i = 1, 6 do
        local award = self.table:GetChild(i - 1):Find("Award")
        local icon = award:Find("Icon"):GetComponent(typeof(CUITexture))
        local num = award:Find("Num"):GetComponent(typeof(UILabel))

        local itemId = WorldCup_HomeTeamReward.GetData(i).Reward
        icon:LoadMaterial(Item_Item.GetData(itemId).Icon)
        num.text = ""

        UIEventListener.Get(award.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnAwardClick(i, itemId)
        end)
    end
end

function LuaWorldCup2022HomeTeamInfoView:OnEnable()
    g_ScriptEvent:AddListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:AddListener("WorldCupJingCai_SendPeriodAndRoundData", self, "SendPeriodAndRoundData")
    g_ScriptEvent:AddListener("WorldCupJingCai_ReplyTaoTaiInfoResult", self, "ReplyTaoTaiInfoResult")
    self:UpdateInfo()
end

function LuaWorldCup2022HomeTeamInfoView:OnDisable()
    g_ScriptEvent:RemoveListener("WorldCupJingCai_UpdateAllJingCaiData", self, "UpdateAllJingCaiData")
    g_ScriptEvent:RemoveListener("WorldCupJingCai_SendPeriodAndRoundData", self, "SendPeriodAndRoundData")
    g_ScriptEvent:RemoveListener("WorldCupJingCai_ReplyTaoTaiInfoResult", self, "ReplyTaoTaiInfoResult")
end

function LuaWorldCup2022HomeTeamInfoView:UpdateAllJingCaiData()
    self:UpdateInfo()
end

function LuaWorldCup2022HomeTeamInfoView:SendPeriodAndRoundData()
    self:UpdateInfo()
end

function LuaWorldCup2022HomeTeamInfoView:ReplyTaoTaiInfoResult()
    self:UpdateInfo()
end

function LuaWorldCup2022HomeTeamInfoView:UpdateInfo()
    local teamId = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_HomeTeamId

    local data = WorldCup_TeamInfo.GetData(teamId)
    self.icon:LoadMaterial(data.Icon)
    self.name.text = data.Name

    local totaiStage = LuaWorldCup2022Mgr.lotteryInfo.taotaiStage
    local curStage = LuaWorldCup2022Mgr.lotteryInfo.curStage or 1
    local str1 = totaiStage > 0 and LocalString.GetString("止步：") or LocalString.GetString("当前：")
    local stageStr = (curStage > 0 and curStage <= 6) and WorldCup_HomeTeamReward.GetData(curStage).Stage or ""
    if curStage == 1 then
        self.groupInfo:SetActive(true)
        self.state.text = SafeStringFormat3(LocalString.GetString("%s%s（%s组）"), str1, stageStr, WorldCup_TeamInfo.GetData(teamId).Group)

        self.groupInfo_Rank.text = LuaWorldCup2022Mgr.lotteryInfo.groupRank
        local win, lose, draw, score = self:GetGroupScore()
        self.groupInfo_WinDrawLose.text = SafeStringFormat3(LocalString.GetString("%d胜%d平%d负"), win, draw, lose)
        self.groupInfo_Score.text = score
    else
        self.groupInfo:SetActive(false)
        self.state.text = curStage ~= 6 and SafeStringFormat3("%s%s", str1, stageStr) or LocalString.GetString("冠军")
    end

    self.aChuWin.gameObject:SetActive(false)
    self.aChuFail.gameObject:SetActive(false)
    self.aChuSport.gameObject:SetActive(false)
    self.aChuWinBig.gameObject:SetActive(false)
    for i = 1, 6 do
        local child = self.table:GetChild(i - 1)
        local normal = child:Find("Normal").gameObject
        local highlight = child:Find("Highlight").gameObject
        normal:SetActive(i ~= curStage)
        highlight:SetActive(i == curStage)

        if totaiStage > 0 and i > totaiStage then
            LuaUtils.SetLocalPositionZ(normal.transform, -1)
            LuaUtils.SetLocalPositionZ(highlight.transform, -1)
            LuaUtils.SetLocalPositionZ(child:Find("Award"), -1)
            LuaUtils.SetLocalPositionZ(child:Find("Type"), -1)
        else
            LuaUtils.SetLocalPositionZ(normal.transform, 0)
            LuaUtils.SetLocalPositionZ(highlight.transform, 0)
            LuaUtils.SetLocalPositionZ(child:Find("Award"), 0)
            LuaUtils.SetLocalPositionZ(child:Find("Type"), 0)
        end

        if i == curStage then
            if i == 6 then
                if LuaWorldCup2022Mgr.lotteryInfo.hasShowBigAChuWin then
                    self.aChuWin.gameObject:SetActive(true)
                else
                    self.aChuWinBig.gameObject:SetActive(true)
                    LuaWorldCup2022Mgr.lotteryInfo.hasShowBigAChuWin = true
                end
            else
                local childPos = child.localPosition
                if totaiStage == curStage then
                    self.aChuFail.gameObject:SetActive(true)
                    LuaUtils.SetLocalPosition(self.aChuFail, childPos.x + 147, 68 * i - 245, 0)
                else
                    self.aChuSport.gameObject:SetActive(true)
                    LuaUtils.SetLocalPosition(self.aChuSport, childPos.x + 122, 68 * i - 245, 0)
                end
            end
        end
    end
    self:UpdateAward()
end

-- 小组积分
function LuaWorldCup2022HomeTeamInfoView:GetGroupScore()
    if not LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData then
        return 0, 0, 0, 0
    end

    local matchRange = WorldCup_HomeTeamReward.GetData(1).MatchRange
    local teamId = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_HomeTeamId

    local win, lose, draw = 0, 0, 0
    for id, data in pairs(LuaWorldCup2022Mgr.lotteryInfo.allRoundResultData) do
        if id >= matchRange[0] and id <= matchRange[1] and data[6] >= 0 and data[7] >= 0 then
            if data[1] == teamId then
                if data[6] > data[7] then
                    win = win + 1
                elseif data[6] < data[7] then
                    lose = lose + 1
                else
                    draw = draw + 1
                end
            elseif data[2] == teamId then
                if data[6] < data[7] then
                    win = win + 1
                elseif data[6] > data[7] then
                    lose = lose + 1
                else
                    draw = draw + 1
                end
            end
        end
    end
    local score = win * 3 + draw * 1
    return win, lose, draw, score
end

function LuaWorldCup2022HomeTeamInfoView:UpdateAward()
    local stage = LuaWorldCup2022Mgr.lotteryInfo.curStage or 1
    local lastRoundId = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_LastRoundId

    for i = 1, 6 do
        local award = self.table:GetChild(i - 1):Find("Award")
        local finish = award:Find("Finish").gameObject
        local lock = award:Find("Lock").gameObject
        local fx = award:Find("Fx"):GetComponent(typeof(CUIFx))
        fx.gameObject:SetActive(false)
        finish:SetActive(false)
        lock:SetActive(false)

        if i > stage then
            lock:SetActive(true)
        elseif bit.band(lastRoundId, bit.lshift(1, i)) > 0 then
            finish:SetActive(true)
        else
            fx.gameObject:SetActive(true)
            fx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
            local icon = award:Find("Icon"):GetComponent(typeof(UITexture))
            local gap = 1
            CUIFx.DoAni(Vector4(- icon.width * 0.5 + gap, icon.height * 0.5 - gap,
                icon.width - gap * 2, icon.height - gap * 2), false, fx)
        end
    end
end

--@region UIEvent

function LuaWorldCup2022HomeTeamInfoView:OnAwardClick(i, itemId)
    local stage = LuaWorldCup2022Mgr.lotteryInfo.curStage or 1
    local lastRoundId = LuaWorldCup2022Mgr.lotteryInfo.jingCaiData.m_LastRoundId
    if i <= stage and bit.band(lastRoundId, bit.lshift(1, i)) == 0 then
        Gac2Gas.WorldCupJingCai_RequestReceiveHomeTeamAward(i)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
    end
end

--@endregion UIEvent
