local CClientFurniture=import "L10.Game.CClientFurniture"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CUICenterOnChild=import "L10.UI.CUICenterOnChild"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local LuaUtils=import "LuaUtils"
local CUITexture=import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
CLuaWinterHouseZswLookupWnd = class()
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_ModelTexture")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_ItemTemplate")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_SelectedIndex")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_ItemLookup")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_TemplateId")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_PriceLabel")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_CostSprite")
RegistClassMember(CLuaWinterHouseZswLookupWnd, "m_RadioBox")

CLuaWinterHouseZswLookupWnd.s_Index = 0
function CLuaWinterHouseZswLookupWnd:Init()
    self.m_ItemLookup={}

    self.m_PriceLabel = FindChild(self.transform,"PriceLabel"):GetComponent(typeof(UILabel))
    self.m_CostSprite = FindChild(self.transform,"CostSprite"):GetComponent(typeof(UISprite))

    self.m_ModelTexture = FindChild(self.transform,"Preview"):GetComponent(typeof(CUITexture))

    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)

    self.m_TemplateIds1={}--已解锁
    self.m_TemplateIds2={}--未解锁

    local unlocked = {}
    local prop = CClientHouseMgr.Inst.mCurFurnitureProp

    CommonDefs.DictIterate(prop.WinterFuniture,DelegateFactory.Action_object_object(function (k,v) 
        unlocked[k] = true
    end))
    WinterHouse_Zhuangshiwu.Foreach(function(k,v)
        if v.Currency==4 then
            unlocked[k] = true
        end
    end)
    --排序
    WinterHouse_Zhuangshiwu.ForeachKey(function(k)
        if unlocked[k] then
            table.insert( self.m_TemplateIds1,k )
        else
            table.insert( self.m_TemplateIds2,k )
        end
    end)
    
	local pool = self.transform:Find("pool"):GetComponent(typeof(CUIGameObjectPool))
    local grid = FindChild(self.transform,"Grid").gameObject

    self.m_RadioBox = self.transform:Find("RadioBox"):GetComponent(typeof(QnRadioBox))

    local centerOnChild = grid:GetComponent(typeof(CUICenterOnChild))
    centerOnChild.onCenter = LuaUtils.OnCenterCallback(function(go)
        for k,v in pairs(self.m_ItemLookup) do
            local nameLabel = k.transform:Find("Label"):GetComponent(typeof(UILabel))
            if k==go then
                nameLabel.color = Color(1,1,1,1)
            else
                nameLabel.color = Color(0,1,0,1)
            end
        end
        self:OnSelected(self.m_ItemLookup[go])
    end)
    local onclick = DelegateFactory.VoidDelegate(function(go)
        centerOnChild:CenterOnInstant(go.transform)
    end)

    local lockedGo = self.transform:Find("Locked").gameObject
    local unlockedGo = self.transform:Find("Unlocked").gameObject

    local go1 = unlockedGo.transform:Find("Button01").gameObject
    local go2 = unlockedGo.transform:Find("Button02").gameObject
    UIEventListener.Get(go1).onClick = DelegateFactory.VoidDelegate(function(g)
        Gac2Gas.RequestSwitchFurnitureWinterAppearanceByTemplateId(self.m_TemplateId,1)
    end)
    UIEventListener.Get(go2).onClick = DelegateFactory.VoidDelegate(function(g)
        Gac2Gas.RequestSwitchFurnitureWinterAppearanceByTemplateId(self.m_TemplateId,0)
    end)

    self.m_RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        if index==0 then
            unlockedGo:SetActive(true)
            lockedGo:SetActive(false)
        else
            unlockedGo:SetActive(false)
            lockedGo:SetActive(true)
        end
        self.m_ItemLookup={}
        local childCount = grid.transform.childCount
        if childCount> 0 then
            for i=childCount,1,-1 do
                local item = grid.transform:GetChild(i-1).gameObject
                pool:Recycle(item)
            end
        end
        local tbl = index==0 and self.m_TemplateIds1 or self.m_TemplateIds2
        if tbl then
            for i,v in ipairs(tbl) do
                local go = pool:GetFromPool(0)
                go.transform.parent = grid.transform
                go.transform.localScale = Vector3.one
                go:SetActive(true)

                local nameLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
                local zsw = Zhuangshiwu_Zhuangshiwu.GetData(v)
                nameLabel.text = zsw.Name

                self.m_ItemLookup[go] = i
                UIEventListener.Get(go).onClick = onclick
            end
        end
        if grid.transform.childCount>0 then
            grid:GetComponent(typeof(UIGrid)):Reposition()
            centerOnChild:CenterOnInstant(grid.transform:GetChild(0))
        else
            unlockedGo:SetActive(false)
            lockedGo:SetActive(false)
            self.m_ModelTexture.mainTexture=nil
            CUIManager.DestroyModelTexture("__FurniturePreview__")
        end
    end)
    self.m_RadioBox:ChangeTo(CLuaWinterHouseZswLookupWnd.s_Index,true)


    -- self:RefreshCost()


end

function CLuaWinterHouseZswLookupWnd:OnSelected(index)
    local templateId = self.m_RadioBox.CurrentSelectIndex == 0 and self.m_TemplateIds1[index] or self.m_TemplateIds2[index]
    if templateId~=self.m_TemplateId then
        self.m_TemplateId = templateId
        local wh = WinterHouse_Zhuangshiwu.GetData(templateId)
        self.m_PriceLabel.text=tostring(wh.Price)
        if wh.Currency==1 then
            self.m_CostSprite.spriteName = "packagewnd_yinliang"
        elseif wh.Currency==2 then
            self.m_CostSprite.spriteName = "packagewnd_lingyu"
        elseif wh.Currency==3 then
            self.m_CostSprite.spriteName = "packagewnd_yinpiao"
        else
            self.m_CostSprite.spriteName = nil
        end
        self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__FurniturePreview__", self.m_ModelTextureLoader,180,0.05,-1,4.66,false,true,1)
    end
end

function CLuaWinterHouseZswLookupWnd:LoadModel(ro)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_TemplateId)
    local resName = data.ResNameXue
    local resid = SafeStringFormat3("_%02d",data.ResIdXue)
    local prefabname = "Assets/Res/Character/Jiayuan/" .. data.ResNameXue .. "/Prefab/" .. data.ResNameXue .. resid .. ".prefab"
    if data.ProgramSnow>0 then
        resid = SafeStringFormat3("_%02d",data.ResId)
        prefabname = "Assets/Res/Character/Jiayuan/" .. data.ResName .. "/Prefab/" .. data.ResName .. resid .. ".prefab"
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
            CClientFurniture.SetSnowLevel(ro,true,self.m_TemplateId)
        end))
    end
    ro:LoadMain(prefabname)
    
    if data.FXXue>0 then
        local fx = CEffectMgr.Inst:AddObjectFX(data.FXXue, ro, 0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        ro:AddFX("SelfFx", fx)
    end

    local whdata = WinterHouse_Zhuangshiwu.GetData(self.m_TemplateId)
    ro.Scale = whdata.ModelScale
end

function CLuaWinterHouseZswLookupWnd:OnDestroy()
    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__FurniturePreview__")
end
