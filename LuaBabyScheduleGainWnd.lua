require("common/common_include")

local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Baby_Plan = import "L10.Game.Baby_Plan"
local BabyMgr = import "L10.Game.BabyMgr"
local Item_Item = import "L10.Game.Item_Item"

LuaBabyScheduleGainWnd = class()

RegistClassMember(LuaBabyScheduleGainWnd, "GainTable")
RegistClassMember(LuaBabyScheduleGainWnd, "GainTemplate")
RegistClassMember(LuaBabyScheduleGainWnd, "ContentScrollView")
RegistClassMember(LuaBabyScheduleGainWnd, "OneClickGainBtn")
RegistClassMember(LuaBabyScheduleGainWnd, "HintLabel")

RegistClassMember(LuaBabyScheduleGainWnd, "GainInfos")

function LuaBabyScheduleGainWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaBabyScheduleGainWnd:InitClassMembers()
	self.GainTable = self.transform:Find("Anchor/bg/ContentScrollView/GainTable"):GetComponent(typeof(UITable))
	self.GainTemplate = self.transform:Find("Anchor/bg/Templates/GainTemplate").gameObject
	self.GainTemplate:SetActive(false)
	self.ContentScrollView = self.transform:Find("Anchor/bg/ContentScrollView"):GetComponent(typeof(CUIRestrictScrollView))
	self.OneClickGainBtn = self.transform:Find("Anchor/Buttons/OneClickGainBtn"):GetComponent(typeof(CButton))
	self.HintLabel = self.transform:Find("Anchor/bg/HintLabel").gameObject
end

function LuaBabyScheduleGainWnd:InitValues()

	self.GainInfos = {}

	local onOneClickGainBtnClicked =  function (go)
		self:OnOneClickGainBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.OneClickGainBtn.gameObject, DelegateFactory.Action_GameObject(onOneClickGainBtnClicked), false)

	self:UpdateGains()

	LuaBabyMgr.SaveProp()
end

function LuaBabyScheduleGainWnd:UpdateGains()

	Extensions.RemoveAllChildren(self.GainTable.transform)

	local selectedBaby = BabyMgr.Inst:GetSelectedBaby()
	if not selectedBaby then return end

	local planRecord = selectedBaby.Props.ScheduleData.PlanRecordData
	if not planRecord then
		return
	end

	self.GainInfos = {}

	CommonDefs.DictIterate(planRecord, DelegateFactory.Action_object_object(function(key, val)
		local planId = val.PlanId
		local receivedAward = val.ReceivedAward
		local finishTime = val.FinishTime
		local itemAward = val.ItemAward
		local isYoundAward = val.IsYoungAward == 1
		table.insert(self.GainInfos, {
			PlanId = planId,
			ReceivedAward = receivedAward == 1,
			FinishTime = finishTime,
			ExtraAward = itemAward == 1,
			Index = key,
			IsYoungAward = isYoundAward,
			})
	end))

	-- 对GainInfos进行排序
	local function compareGain(a, b)
		return b.FinishTime < a.FinishTime
	end
	table.sort(self.GainInfos, compareGain)

	for i = 1, #self.GainInfos do
		local go = CUICommonDef.AddChild(self.GainTable.gameObject, self.GainTemplate)
		self:InitGainItem(go, self.GainInfos[i])
		go:SetActive(true)
	end

	self.GainTable:Reposition()
	self.ContentScrollView:ResetPosition()

	self.HintLabel:SetActive(#self.GainInfos == 0)

end

function LuaBabyScheduleGainWnd:InitGainItem(go, info)
	local TimeLabel = go.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
	TimeLabel.text = ""
	local GainLabel = go.transform:Find("GainLabel"):GetComponent(typeof(UILabel))
	GainLabel.text = ""
	local GainButton = go.transform:Find("GainButton"):GetComponent(typeof(CButton))
	GainButton.Enabled = false

	local babyPlan = Baby_Plan.GetData(info.PlanId)
	if not babyPlan then return end
	local planName = babyPlan.Name

	local descBack = babyPlan.DescriptionBack

	local gainStr = ""
	local status = BabyMgr.Inst:GetSelectedBabyStatus()

	-- 是否获得额外的奖励
	if info.ExtraAward then
		local item = Item_Item.GetData(babyPlan.ExtraItemId)
		if item then
			gainStr = SafeStringFormat3(LocalString.GetString("%s[ACF8FF]%s，[-]"), gainStr, item.Name)
		end
	end

	local showYoungDelta = false

	if not info.ReceivedAward then
		-- 如果还未领取，则根据自身状态显示
		showYoungDelta = status == EnumBabyStatus.eYoung
	else
		showYoungDelta = info.IsYoungAward
	end

	if not showYoungDelta then
		local count = babyPlan.ChildPropDelta.Length
		for i = 0, count-1 do
			local property = babyPlan.ChildPropDelta[i][0]
			local delta = babyPlan.ChildPropDelta[i][1]

			local addLabel = "+"
			if delta < 0 then
				addLabel = "-"
			end

			gainStr = SafeStringFormat3(LocalString.GetString("%s[ACF8FF]%s%s%d，[-]"), gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eChild, property), addLabel, math.abs(delta))
		end
		
	else
		local count = babyPlan.YoungPropDelta.Length
		for i = 0, count-1 do
			local property = babyPlan.YoungPropDelta[i][0]
			local delta = babyPlan.YoungPropDelta[i][1]

			local addLabel = "+"
			if delta < 0 then
				addLabel = "-"
			end

			gainStr = SafeStringFormat3(LocalString.GetString("%s[ACF8FF]%s%s%d，[-]"), gainStr, LuaBabyMgr.GetPropName(EnumBabyStatus.eYoung, property), addLabel, math.abs(delta))
			
		end
	end

	gainStr = SafeStringFormat3(LocalString.GetString("%s[ACF8FF]经验+%s[-]"), gainStr, babyPlan.Exp)

	TimeLabel.text = CServerTimeMgr.ConvertTimeStampToZone8Time(info.FinishTime):ToString("yyyy.MM.dd")
	GainLabel.text = SafeStringFormat3(LocalString.GetString("%s，%s"), descBack, gainStr) 
	GainButton.Enabled = not info.ReceivedAward

	local onGainButtonClicked = function (go)
		self:OnGainButtonClickeded(go, info.Index)
	end
	CommonDefs.AddOnClickListener(GainButton.gameObject, DelegateFactory.Action_GameObject(onGainButtonClicked), false)
end

function LuaBabyScheduleGainWnd:OnGainButtonClickeded(go, index)
	if not BabyMgr.Inst.SelectedBabyId then return end
	Gac2Gas.RequestReceiveOnePlanAward(BabyMgr.Inst.SelectedBabyId, index)
end

function LuaBabyScheduleGainWnd:GetColor(index)
	if index == 1 then
		return "FFFE91"
	elseif index == 2 then
		return "519FFF"
	elseif index == 3 then
		return "FF5050"
	elseif index == 4 then
		return "C000C0"
	end
	return "FFFFFF"
end

-- 一键获取
function LuaBabyScheduleGainWnd:OnOneClickGainBtnClicked(go)
	if not BabyMgr.Inst.SelectedBabyId then return end
	Gac2Gas.RequestReceiveAllPlanAward(BabyMgr.Inst.SelectedBabyId)
end

function LuaBabyScheduleGainWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBabyPlanAward", self, "UpdateGains")
end

function LuaBabyScheduleGainWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBabyPlanAward", self, "UpdateGains")
end

return LuaBabyScheduleGainWnd