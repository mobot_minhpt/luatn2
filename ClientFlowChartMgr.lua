require "design/Flowchart/FlowchartConstant"

local CClientAIMgr = import "L10.Game.CClientAIMgr"
-- require ("asyncflow")
CClientAIMgr.Inst:DoRequireAsyncflow()

require "ai/LuaClientAINpc"
require("design/Flowchart/" .. FlowOutputPrefix .. 'ClientAINpc')
fc_set = flowchart._set
fc_get = flowchart._get
function fc_set_t(idx, k, v) fc_get(idx)[k] = v return v end
fc_call = flowchart._call
fc_ret = flowchart._ret
fc_wait = flowchart._wait
fc_ep = flowchart._eventparam
fc_callev = flowchart._callevent
fc_stop = flowchart._stopchart
fc_stopnode = flowchart._stopnode

-- no use
fc_gotonode = flowchart._gotonode
fc_stopchart = flowchart.unload
fc_startchart = flowchart.load
fc_sendreward = flowchart.sendreward
fc_sendcustomval = flowchart.sendcustomval
fc_setnetmode = flowchart.setnetmode

-- in miliseconds
g_ClientAIInterval = 200
flowchart.setup(g_ClientAIInterval)

-- 转表定义
-- if FlowOutputPrefix and EFlowEvent then print('flowchartconstant.lua success', FlowOutputPrefix)
-- else print("please require flowchartconstant.lua") end

flowchart.import("ClientAINpc")

if Flowchart_ClientAINpc == nil then print("please require lua file for ai") end

g_ClientAITick = nil

-- 所有成功挂上流程图的对象的 uid -> true
-- 用来判断要不要停tick
g_ClientAIRefObj = {}

