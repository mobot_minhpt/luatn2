local CGamePlayMgr = import "L10.Game.CGamePlayMgr"

LuaQingMing2022Mgr = {}

------------------ 引魂破执 --------------------
LuaQingMing2022Mgr.YHPZInfo = {}
LuaQingMing2022Mgr.isYHPZEnd = false

-- 打开问题界面
function LuaQingMing2022Mgr:OpenYHPZQuestionWnd(questionId, timeout)
    self.YHPZInfo.questionId = questionId
    self.YHPZInfo.timeout = timeout
    CUIManager.ShowUI(CLuaUIResources.YinHunPoZhiQuestionWnd)
end

-- 关闭银魂破执问题界面
function LuaQingMing2022Mgr:CloseYHPZQuestionWnd()
    CUIManager.CloseUI(CLuaUIResources.YinHunPoZhiQuestionWnd)
end

-- 同步银魂破执分数
function LuaQingMing2022Mgr:SyncYHPZScoreInPlay(score)
    self.YHPZInfo.score = score
    g_ScriptEvent:BroadcastInLua("QingMing2022SyncYHPZScoreInPlay")
end

-- 打开结算界面
function LuaQingMing2022Mgr:ShowYHPZResultWnd(score, todayMaxScore, newrecord)
    if CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) then
        CUIManager.CloseUI(CUIResources.ScreenCaptureWnd)
    end

    self.YHPZInfo.score = score
    self.YHPZInfo.todayMaxScore = math.max(todayMaxScore, score)
    self.YHPZInfo.newrecord = newrecord
    self.isYHPZEnd = true
    CUIManager.ShowUI(CLuaUIResources.YinHunPoZhiResultWnd)
end

-- 离开场景
function LuaQingMing2022Mgr:LeaveYHPZScene()
    if self.isYHPZEnd then
        CGamePlayMgr.Inst:LeavePlay()
    else
        local msg = g_MessageMgr:FormatMessage("YINHUNPOZHI_LEAVE_SCENE_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            Gac2Gas.RequestLeavePlay()
        end), nil, nil, nil, false)
    end
end

-- array转成table
function LuaQingMing2022Mgr:Array2Tbl(array)
    if array == nil or array.Length == 0 then return nil end
    local tbl = {}
    for i = 0, array.Length - 1 do
        table.insert(tbl, array[i])
    end
    return tbl
end

-- 进入引魂破执玩法
function LuaQingMing2022Mgr:EnterYHPZ()
    self.isYHPZEnd = false
    self:ShowYHPZImageRuleWnd()
end

-- 打开图文引导
function LuaQingMing2022Mgr:ShowYHPZImageRuleWnd()
    local imagePaths = {
        "UI/Texture/NonTransparent/Material/yinhunpozhiwnd_01.mat",
        "UI/Texture/NonTransparent/Material/yinhunpozhiwnd_02.mat",
        "UI/Texture/NonTransparent/Material/yinhunpozhiwnd_03.mat",
        "UI/Texture/NonTransparent/Material/yinhunpozhiwnd_04.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("YINHUNPOZHI_RULE_1"),
        g_MessageMgr:FormatMessage("YINHUNPOZHI_RULE_2"),
        g_MessageMgr:FormatMessage("YINHUNPOZHI_RULE_3"),
        g_MessageMgr:FormatMessage("YINHUNPOZHI_RULE_4"),
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end


------------------ 幻境释梦 ----------------------
LuaQingMing2022Mgr.HJSMGainTbl = {}
LuaQingMing2022Mgr.HJSMTimeOut = 0

function LuaQingMing2022Mgr:OpenHJSMWnd(_buffList, _skillId, timeout)
    local buffList = {}
    CommonDefs.ListIterate(MsgPackImpl.unpack(_buffList), DelegateFactory.Action_object(function (value)
        table.insert(buffList, tonumber(value))
    end))

    self.HJSMGainTbl = {}
    for k, id in pairs(buffList) do
        table.insert(self.HJSMGainTbl, id)
    end

    table.insert(self.HJSMGainTbl, _skillId)
    self.HJSMTimeOut = timeout

    CUIManager.CloseUI(CLuaUIResources.HuanJingShiMengWnd)
    CUIManager.ShowUI(CLuaUIResources.HuanJingShiMengWnd)
end
