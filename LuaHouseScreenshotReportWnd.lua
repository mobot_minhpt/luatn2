local UILabel = import "UILabel"
local UITexture = import "UITexture"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local UIInput = import "UIInput"
local CMiniMap = import "L10.UI.CMiniMap"

CLuaHouseScreenshotReportWnd = class()

RegistChildComponent(CLuaHouseScreenshotReportWnd, "m_ReportBtn","ReportBtn", GameObject)
RegistChildComponent(CLuaHouseScreenshotReportWnd, "m_InputReason","StatusText", UIInput)
RegistChildComponent(CLuaHouseScreenshotReportWnd, "m_HomeNameLabel","HomeNameLabel", UILabel)
RegistChildComponent(CLuaHouseScreenshotReportWnd, "m_OwnerNameLabel","OwnerNameLabel", UILabel)
RegistChildComponent(CLuaHouseScreenshotReportWnd, "m_SreenShotTexture","SreenShotTexture", UITexture)
RegistClassMember(CLuaHouseScreenshotReportWnd,"m_ImageUrl")

function CLuaHouseScreenshotReportWnd:OnEnable()
    g_ScriptEvent:AddListener("SendPlayerName", self, "OnSendPlayerName") 
end

function CLuaHouseScreenshotReportWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendPlayerName", self, "OnSendPlayerName")
end

function CLuaHouseScreenshotReportWnd:Awake()
    UIEventListener.Get(self.m_ReportBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        self:UploadReportInfo()
    end)
    self.m_ImageUrl = ""
end

function CLuaHouseScreenshotReportWnd:Init()
    local curHouseBasicProp = CClientHouseMgr.Inst.mCurBasicProp
    local ownerId = curHouseBasicProp.OwnerId
    local ownerId2 = curHouseBasicProp.OwnerId2

    local sceneName = CMiniMap.Instance.sceneNameButton.Text
    if not sceneName or sceneName == "" then
        sceneName = ""
    end

    local owner1 = CClientPlayerMgr.Inst:GetPlayer(ownerId)
    local owner2 = CClientPlayerMgr.Inst:GetPlayer(ownerId2)

    self.m_HomeNameLabel.text = sceneName
    CUICommonDef.LoadLocalFile2Texture(self.m_SreenShotTexture,CLuaHouseScreenshotReportMgr.ImagePath,815,475)

    Gac2Gas.RequestPlayerName(ownerId)
end

function CLuaHouseScreenshotReportWnd:UploadReportInfo()
    local tex = self.m_SreenShotTexture.mainTexture
    if tex ~= nil then
        local data = CommonDefs.EncodeToJPG(tex)

        local onFinished = DelegateFactory.Action_string(function (url)
            if url ==nil then
                url = ""
            end
            self.m_ImageUrl = url
            local reportReason = self.m_InputReason.value == nil and "" or StringTrim(self.m_InputReason.value)

            Gac2Gas.HouseScreenshotReport(self.m_ImageUrl,reportReason)         
        end)
        CPersonalSpaceMgr.Inst:UploadPic("housereportscreenshot", data, onFinished)
    end
end

function CLuaHouseScreenshotReportWnd:OnSendPlayerName(playerId,playerName)
    self.m_OwnerNameLabel.text = playerName
end

--================
CLuaHouseScreenshotReportMgr = class()
CLuaHouseScreenshotReportMgr.ImagePath = nil

