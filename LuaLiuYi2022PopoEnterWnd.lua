local UILabel = import "UILabel"
local UITable = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

LuaLiuYi2022PopoEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "TextTable", "TextTable", UITable)
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "TextTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "BtnLabel", "BtnLabel", UILabel)
RegistChildComponent(LuaLiuYi2022PopoEnterWnd, "MatchingMark", "MatchingMark", GameObject)

--@endregion RegistChildComponent end

function LuaLiuYi2022PopoEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtnClick()
	end)


    --@endregion EventBind end
end

function LuaLiuYi2022PopoEnterWnd:Init()
	local setting = DuanWu_Setting.GetData()
	self.gameplayId = setting.BubbleGameplayId
	if CClientMainPlayer.Inst then
		Gac2Gas.GlobalMatch_RequestCheckSignUp(self.gameplayId , CClientMainPlayer.Inst.Id)
	end

	local maxCount = setting.BubbleMaxDailyScore
	local daily = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(LuaDuanWu2022Mgr.EnumDuanWuPlayTime.eDuanWu2022FKPP_Daily)

	self.ScoreLabel.text = daily .. "/" .. maxCount
	if daily >= maxCount then
		self.ScoreLabel.color = Color(0,1,96/255)
	else
		self.ScoreLabel.color = Color(172/255,248/255,1)
	end

	self:InitTextTable()
end

function LuaLiuYi2022PopoEnterWnd:InitTextTable()
    -- 配表时间
    local setting = DuanWu_Setting.GetData()
    self.TimeLabel.text = setting.BubbleOpenTimeDescription

    self.TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.TextTable.transform)
    local msg = g_MessageMgr:FormatMessage("LiuYi_2022_Bubble_Introduction")
    
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end
    
    self.TextTable:Reposition()
	self.ScrollView:ResetPosition()
end

function LuaLiuYi2022PopoEnterWnd:OnState(isMatching)
	self.isMatching = isMatching
	self.MatchingMark:SetActive(isMatching)

	local btnSp = self.EnterBtn.transform:GetComponent(typeof(UISprite))
	if not isMatching then
		self.BtnLabel.text = LocalString.GetString("报名参加")
		btnSp.spriteName = "common_btn_01_yellow"
	else
		self.BtnLabel.text = LocalString.GetString("取消报名")
		btnSp.spriteName = "common_btn_01_blue"
	end
end

function LuaLiuYi2022PopoEnterWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
	self:OnState(isInMatching)
end

function LuaLiuYi2022PopoEnterWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
	if success then
		self:OnState(true)
	end
end

function LuaLiuYi2022PopoEnterWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
	if success then
		self:OnState(false)
	end
end

function LuaLiuYi2022PopoEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaLiuYi2022PopoEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

--@region UIEvent

function LuaLiuYi2022PopoEnterWnd:OnEnterBtnClick()
	if self.isMatching then
		Gac2Gas.GlobalMatch_RequestCancelSignUp(self.gameplayId)
	else
		Gac2Gas.GlobalMatch_RequestSignUp(self.gameplayId)
	end
end


--@endregion UIEvent

