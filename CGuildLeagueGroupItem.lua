-- Auto Generated!!
local CGuildLeagueGroupItem = import "L10.UI.CGuildLeagueGroupItem"
local LocalString = import "LocalString"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local Extensions = import "Extensions"
CGuildLeagueGroupItem.m_Init_CS2LuaHook = function (this, group) 
    this.group = group
    if LuaGuildLeagueCrossMgr:IsOpenedOnMyServer() then
        this.nameLabel.text = SafeStringFormat3(LocalString.GetString("%s组"), Extensions.ConvertToChineseString(group))
    else
        this.nameLabel.text = LuaGuildLeagueMgr.GetLevelStr(group)
    end
end
