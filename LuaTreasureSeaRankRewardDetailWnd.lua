local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local UITabBar = import "L10.UI.UITabBar"
local Profession = import "L10.Game.Profession"

LuaTreasureSeaRankRewardDetailWnd = class()
LuaTreasureSeaRankRewardDetailWnd.s_Tab = 0 -- 0:Rank 1:Reward

RegistClassMember(LuaTreasureSeaRankRewardDetailWnd, "HintLabel")
RegistClassMember(LuaTreasureSeaRankRewardDetailWnd, "RewardView")
RegistClassMember(LuaTreasureSeaRankRewardDetailWnd, "RankView")
RegistClassMember(LuaTreasureSeaRankRewardDetailWnd, "RankTableView")
RegistClassMember(LuaTreasureSeaRankRewardDetailWnd, "TabBar")

RegistClassMember(LuaTreasureSeaRankRewardDetailWnd, "m_RankList")

function LuaTreasureSeaRankRewardDetailWnd:Awake()
    self.HintLabel = self.transform:Find("Top/HintLabel"):GetComponent(typeof(UILabel))
    self.RewardView = self.transform:Find("Anchor/RewardView").gameObject
    self.RankView = self.transform:Find("Anchor/RankView").gameObject
    self.RewardView:SetActive(false)
    self.RankView:SetActive(false)
    self.TabBar = self.transform:Find("Top/TabBar"):GetComponent(typeof(UITabBar))
    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index + 1)
    end)

    self.m_RankList = {}
    local TableView = self.RankView.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    local TableViewDataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item, index)
            self:InitItem(item, index, self.m_RankList[index + 1])
        end
    )
    TableView.m_DataSource = TableViewDataSource

    TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)  
        local data = self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
        end
    end)

    self.RankTableView = TableView
end

function LuaTreasureSeaRankRewardDetailWnd:OnTabChange(index)
    if index == 1 then
        self.RewardView:SetActive(false)
        Gac2Gas.QueryRank(NavalWar_Setting.GetData().PvpRankId)
    else
        self.RewardView:SetActive(true)
        self.RankView:SetActive(false)
        self:InitRewardView()
    end
end

function LuaTreasureSeaRankRewardDetailWnd:InitRewardView()
    local Grid = self.transform:Find("Anchor/RewardView/Grid"):GetComponent(typeof(UIGrid))
    local Template = self.transform:Find("Anchor/RewardView/RewardTemplate").gameObject
    Template:SetActive(false)

    Extensions.RemoveAllChildren(Grid.transform)
    local rankItems = NavalWar_Setting.GetData().PvpRankRewardItem
    local preRank = 0
    for i = 0, rankItems.Length - 1 do
        local go = NGUITools.AddChild(Grid.gameObject, Template)
        go:SetActive(true)
        local items = rankItems[i]
        go.transform:Find("RankLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("第%d-%d名"), preRank + 1, items[0]) 
        preRank = items[0]
        local grid = go.transform:Find("Grid"):GetComponent(typeof(UIGrid))
        local template = go.transform:Find("ItemTemplate").gameObject
        template:SetActive(false)
        for j = 1, items.Length - 1, 2 do
            local id = items[j]
            local num = items[j+1]
            go = NGUITools.AddChild(grid.gameObject, template)
            go:SetActive(true)
            self:InitOneItem(go, id, num)
        end
        grid:Reposition()
    end
    Grid:Reposition()
end

function LuaTreasureSeaRankRewardDetailWnd:InitRankView()
    self.RankView:SetActive(true)

    local myRankLabel = self.transform:Find("Anchor/RankView/MainPlayerInfo/RankLabel"):GetComponent(typeof(UILabel))
    local myRankSprite = myRankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    local myNameLabel = self.transform:Find("Anchor/RankView/MainPlayerInfo/NameLabel"):GetComponent(typeof(UILabel))
    local myJobSp = myNameLabel.transform:Find("JobSp"):GetComponent(typeof(UISprite))
    local myLvLabel = self.transform:Find("Anchor/RankView/MainPlayerInfo/LvLabel"):GetComponent(typeof(UILabel))
    local myGuildLabel = self.transform:Find("Anchor/RankView/MainPlayerInfo/GuildLabel"):GetComponent(typeof(UILabel))
    local myScoreLabel = self.transform:Find("Anchor/RankView/MainPlayerInfo/ScoreLabel"):GetComponent(typeof(UILabel))

    local myInfo = CRankData.Inst.MainPlayerRankInfo
    if myInfo.Rank > 0 then
        local rank = myInfo.Rank
        myRankLabel.text = ""
        myRankSprite.gameObject:SetActive(true)
        if rank == 1 then
            myRankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            myRankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            myRankSprite.spriteName = "Rank_No.3png"
        else
            myRankSprite.gameObject:SetActive(false)
            myRankLabel.text = tostring(rank)
        end
    else
        myRankLabel.text = LocalString.GetString("未上榜")
    end

    myNameLabel.text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or myInfo.Name
    myJobSp.spriteName = Profession.GetIcon(myInfo.Job)
    myLvLabel.text = myInfo.Level
    --myLvLabel.color = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng and NGUIText.ParseColor24("FE7900", 0) or Color.white
    myGuildLabel.text = System.String.IsNullOrEmpty(myInfo.Guild_Name) and LocalString.GetString("—") or myInfo.Guild_Name
    myScoreLabel.text = myInfo.Value > 0 and myInfo.Value or LocalString.GetString("—")

    self.m_RankList = {}
    for i = 1, CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i - 1])
    end

    self.RankView.transform:Find("EmptyTip").gameObject:SetActive(#self.m_RankList == 0)
    self.RankTableView:ReloadData(true, false)
end

function LuaTreasureSeaRankRewardDetailWnd:InitItem(item, index, info)
    item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankSprite = rankLabel.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local jobSp = nameLabel.transform:Find("JobSp"):GetComponent(typeof(UISprite))
    local lvLabel = item.transform:Find("LvLabel"):GetComponent(typeof(UILabel))
    local guildLabel = item.transform:Find("GuildLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    if info.Rank > 0 then
        local rank = info.Rank
        rankLabel.text = ""
        rankSprite.gameObject:SetActive(true)
        if rank == 1 then
            rankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            rankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            rankSprite.spriteName = "Rank_No.3png"
        else
            rankSprite.gameObject:SetActive(false)
            rankLabel.text = tostring(rank)
        end
    else
        rankLabel.text = LocalString.GetString("未上榜")
    end

    nameLabel.text =  info.Name
    jobSp.spriteName = Profession.GetIcon(info.Job)
    lvLabel.text = info.Level
    guildLabel.text = System.String.IsNullOrEmpty(info.Guild_Name) and LocalString.GetString("—") or info.Guild_Name
    scoreLabel.text = info.Value > 0 and info.Value or LocalString.GetString("—")
end

function LuaTreasureSeaRankRewardDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaTreasureSeaRankRewardDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaTreasureSeaRankRewardDetailWnd:OnRankDataReady()
    if CRankData.Inst.MainPlayerRankInfo and CRankData.Inst.MainPlayerRankInfo.rankId == NavalWar_Setting.GetData().PvpRankId then
        self:InitRankView()
    end
end

function LuaTreasureSeaRankRewardDetailWnd:Init()
    self.HintLabel.text = NavalWar_Setting.GetData().PvpRankRewardHint
    self.TabBar:ChangeTab(LuaTreasureSeaRankRewardDetailWnd.s_Tab)
end

function LuaTreasureSeaRankRewardDetailWnd:InitOneItem(curItem, itemID, count)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local countLabel = curItem.transform:Find("Count"):GetComponent(typeof(UILabel))
    local qualitySprite = curItem.transform:Find("Border"):GetComponent(typeof(UISprite))
    if count == 1 then countLabel.gameObject:SetActive(false) 
    else countLabel.text = count end
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemID))

    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end