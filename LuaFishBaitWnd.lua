

local QnTableView = import "L10.UI.QnTableView"
local CItem=import "L10.Game.CItem"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CItemMgr = import "L10.Game.CItemMgr"
local QnTableItem = import "L10.UI.QnTableItem"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local QnButtonState = import "L10.UI.QnButton+QnButtonState"

LuaFishBaitWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFishBaitWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaFishBaitWnd, "m_BaitsItemIdList")
RegistClassMember(LuaFishBaitWnd, "m_WeaponId")
RegistClassMember(LuaFishBaitWnd, "m_YuGanLevel")
RegistClassMember(LuaFishBaitWnd, "m_SkillLevel")
RegistClassMember(LuaFishBaitWnd, "m_SelectRow")
RegistClassMember(LuaFishBaitWnd, "m_SpecialHeMaFoodCount")
RegistClassMember(LuaFishBaitWnd, "m_SpecialHeMaFoodItem")
function LuaFishBaitWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    LuaSeaFishingMgr.m_BaitsItemIdList = {}
    local setting = HouseFish_Setting.GetData()
    local baitsList = setting.FishFoodTempId
    for i=0,baitsList.Length-1,1 do
        table.insert(LuaSeaFishingMgr.m_BaitsItemIdList,baitsList[i])
    end
    self.m_YuGanLevel = 0
    self.m_SkillLevel = 0
    self.m_SelectRow = 0
    self.m_SpecialHeMaFoodCount = 0
    self.m_SpecialHeMaFoodItem = setting.HeMaFishingFood

    local outOfDateStr = setting.HeMaEventOutOfDateTime
    local year, month, day, hour, min = string.match(outOfDateStr, "(%d+)-(%d+)-(%d+) (%d+):(%d+)") 
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local outOfDate = CreateFromClass(DateTime, year, month, day, hour, min, 0)

    if DateTime.Compare(now, outOfDate) >= 0 then--盒马鲜生海钓活动已结束
        self.m_SpecialHeMaFoodCount = 0
    else
        self.m_SpecialHeMaFoodCount = 1
    end
end

function LuaFishBaitWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateFishFood",self,"OnUpdateFishFood")
end

function LuaFishBaitWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateFishFood",self,"OnUpdateFishFood")
end

function LuaFishBaitWnd:Init()
    local cls = HouseFish_Setting.GetData().FishSkillId
	self.m_CurSkillId = CClientMainPlayer.Inst.SkillProp:GetSkillIdWithDeltaByCls(cls, CClientMainPlayer.Inst.Level)
	self.m_SkillLevel = CLuaDesignMgr.Skill_AllSkills_GetLevel(self.m_CurSkillId)

    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
    for i=1,equipListOnBody.Count,1 do
        local data = HouseFish_PoleType.GetDataBySubKey("TemplateID",equipListOnBody[i-1].templateId)
		if data then
			self.m_YuGanLevel = data.PoleLevel
		end
    end

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #LuaSeaFishingMgr.m_BaitsItemIdList + self.m_SpecialHeMaFoodCount
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.TableView:ReloadData(false, false)
    if CClientMainPlayer.Inst then
        local itemProp = CClientMainPlayer.Inst.ItemProp
        if itemProp then
            self.m_CurBait = itemProp.FishFood
        end
        if itemProp and itemProp.FishFood and itemProp.FishFood ~= 0 then
            local selectedId = itemProp.FishFood
            --是否是盒马鱼饵 
            if selectedId == self.m_SpecialHeMaFoodItem then
                if self.m_SpecialHeMaFoodCount > 0 then
                    self.m_SelectRow = 0
                    self.TableView:SetSelectRow(0,true)
                else
                    --活动结束
                end
            else
                local selectedIndex = 0
                for i=1,#LuaSeaFishingMgr.m_BaitsItemIdList,1 do
                    local itemid = LuaSeaFishingMgr.m_BaitsItemIdList[i]
                    if itemid == selectedId then
                        selectedIndex = i 
                        break
                    end
                end
                if selectedIndex ~= 0 then
                    self.m_SelectRow = selectedIndex - 1 + self.m_SpecialHeMaFoodCount
                    self.TableView:SetSelectRow(selectedIndex - 1 + self.m_SpecialHeMaFoodCount,true)
                end
            end   
        end
    end
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local item = self.TableView:GetItemAtRow(row)
        local pos = item.transform:Find("Item").transform.localPosition
        if pos.z < 0 then
            g_MessageMgr:ShowMessage("ChangePole_To_Unlock_HigherYuEr")
            self.TableView:SetSelectRow(row,false)
            local item2 = self.TableView:GetItemAtRow(self.m_SelectRow)
            item2:SetState(QnButtonState.Highlighted)
            return
        end

        self.m_SelectRow = row
        local selectedItemId 

        if row == 0 and self.m_SpecialHeMaFoodCount == 1 then
            selectedItemId = self.m_SpecialHeMaFoodItem
        else
            selectedItemId = LuaSeaFishingMgr.m_BaitsItemIdList[row + 1 - self.m_SpecialHeMaFoodCount]
        end
        
        local bindCount, notBindCount
        bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(selectedItemId)
        local count = bindCount + notBindCount
        if selectedItemId ~= self.m_CurBait then
            Gac2Gas.SetFishingFood(selectedItemId)
        else
            Gac2Gas.SetFishingFood(0)
            LuaSeaFishingMgr.UpdateFishFood(0)
        end
    end)
end

function LuaFishBaitWnd:InitItem(item,row)
    local longPressButton = item.transform:Find("Item/Icon"):GetComponent(typeof(UILongPressButton))
    local icon = item.transform:Find("Item/Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local countLabel = item.transform:Find("Item/Count"):GetComponent(typeof(UILabel))
    local mask = item.transform:Find("Item/Mask"):GetComponent(typeof(UILongPressButton))
    local button = item.transform:GetComponent(typeof(QnTableItem))
    local descLabel = item.transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    local itemId
    if row == 0 and self.m_SpecialHeMaFoodCount == 1 then
        itemId = self.m_SpecialHeMaFoodItem
    else
        itemId = LuaSeaFishingMgr.m_BaitsItemIdList[row + 1 - self.m_SpecialHeMaFoodCount]
    end

    local data = Item_Item.GetData(itemId)

    local function longPressCallback(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
    end

    if data then
        nameLabel.text = data.Name
        icon:LoadMaterial(data.Icon)
        
        local desc = CItem.GetItemDescription(itemId,true)
		local lcPos = string.find(desc,"#r")
		if lcPos then
			desc = string.sub(desc,lcPos+2)
		end
        descLabel.text = CChatLinkMgr.TranslateToNGUIText(desc,false)

        local bindCount, notBindCount
        bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
        local count = bindCount + notBindCount
        countLabel.text = count
        mask.gameObject:SetActive(count<=0)

        mask.OnClickDelegate = DelegateFactory.Action(function ()
            if count > 0 then return end
            if itemId == self.m_SpecialHeMaFoodItem then
                g_MessageMgr:ShowMessage("Please_GetHeMaFishFood_First")
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, true, nil, AlignType1.Right)
            end
        end)
        mask.OnLongPressDelegate = DelegateFactory.Action(function ()
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
        end)
        mask.OnLongPressEndDelegate = DelegateFactory.Action(function ()
            CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        end)

        longPressButton.OnLongPressDelegate = DelegateFactory.Action(function ()
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
        end)
        longPressButton.OnLongPressEndDelegate = DelegateFactory.Action(function ()
            CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        end)
    end

    local pos = item.transform:Find("Item").transform.localPosition
    if itemId == self.m_SpecialHeMaFoodItem then
        pos.z = 0
        item.transform:Find("Item").transform.localPosition = pos
        button.enabled = true
    else
        local yuerLevel = row + 1 - self.m_SpecialHeMaFoodCount
        
        local foodTempLv = HouseFish_Setting.GetData().FishFoodTempLv
        local needPlayerLevel = foodTempLv[yuerLevel-1]
        local needSkillLevel = 1
        if yuerLevel <= 1 then
            needSkillLevel = 1
        else
            needSkillLevel = HouseFish_Setting.GetData().FishFoodWeightMaxLv[yuerLevel-1-1] + 1
        end

        local player = CClientMainPlayer.Inst
        if not player then return end
        local level = player.Level
        if player.HasFeiSheng then
            level = player.XianShenLevel
        end
        if yuerLevel > self.m_YuGanLevel or needPlayerLevel > level or self.m_SkillLevel < needSkillLevel then   
            pos.z = -1
            item.transform:Find("Item").transform.localPosition = pos
            --button.Enabled = false
            mask.OnClickDelegate = nil
        else
            pos.z = 0
            item.transform:Find("Item").transform.localPosition = pos
            button.enabled = true
        end
    end    
end

function LuaFishBaitWnd:OnUpdateFishFood()
    if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.ItemProp then 
        return 
    end
	self.m_CurBait = CClientMainPlayer.Inst.ItemProp.FishFood
    if self.m_CurBait == 0 then
        self.TableView:SetSelectRow(self.m_SelectRow,false)
    end
end

--@region UIEvent

--@endregion UIEvent

