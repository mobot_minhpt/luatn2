-- Auto Generated!!
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShop_EquipSearch = import "L10.UI.CPlayerShop_EquipSearch"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local DelegateFactory = import "DelegateFactory"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EquipmentTemplate_Recommand = import "L10.Game.EquipmentTemplate_Recommand"
--local EquipmentTemplate_Type = import "L10.Game.EquipmentTemplate_Type"
local Int32 = import "System.Int32"
local ItemSearchParams = import "L10.UI.ItemSearchParams"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local SearchOption = import "L10.UI.SearchOption"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
CPlayerShop_EquipSearch.m_Init_CS2LuaHook = function (this, isPublicity, option)
    if option ~= CYuanbaoMarketMgr.SearchEquipValueable and CYuanbaoMarketMgr.Jump2SelectType then
        CYuanbaoMarketMgr.Jump2SelectType = false
    end
    this.m_SearchOption = option
    this.IsPublicity = isPublicity
    this.m_SearchButton.Text = LocalString.GetString("装备搜索")
    this:ReloadColorData()
    this:ReloadEquipData()
    --初始化过一次之后就不再使用推荐设置
    if this.m_UseRecommandSettings then
        this.m_UseRecommandSettings = false
        this.m_KangXingValueButton:SetMinMax(1, 12, 1)
        this:BindEvents()
        this.m_CheckBoxGroup:SetSelect(0, true)
    end
end
CPlayerShop_EquipSearch.m_BindEvents_CS2LuaHook = function (this) 
    this.m_LevelButton.OnClick = MakeDelegateFromCSFunction(this.OnLevelButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_TypeButton.OnClick = MakeDelegateFromCSFunction(this.OnTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_SubTypeButton.OnClick = MakeDelegateFromCSFunction(this.OnSubTypeButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_ColorButton.OnClick = MakeDelegateFromCSFunction(this.OnColorButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_EquipNameButton.OnClick = MakeDelegateFromCSFunction(this.OnEquipNameButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    this.m_SearchButton.OnClick = MakeDelegateFromCSFunction(this.OnSearchButtonOnClick, MakeGenericClass(Action1, QnButton), this)
    if this.m_FocusButton ~= nil then
        this.m_FocusButton.OnClick = MakeDelegateFromCSFunction(this.OnFocusButtonClick, MakeGenericClass(Action1, QnButton), this)
    end
    this.m_CheckBoxGroup.OnSelect = MakeDelegateFromCSFunction(this.OnCheckBoxGroupSelect, MakeGenericClass(Action2, QnCheckBox, Int32), this)
    this.m_KangXingWordButton.OnClick = MakeDelegateFromCSFunction(this.OnClickKangXingWordButton, MakeGenericClass(Action1, QnButton), this)
    this.m_KangXingTypeButton.OnClick = MakeDelegateFromCSFunction(this.OnClickKangXingTypeButton, MakeGenericClass(Action1, QnButton), this)
    this.m_KangXingValueButton.onKeyBoardClosed = MakeDelegateFromCSFunction(this.OnKeyBoardClosed, MakeGenericClass(Action1, UInt32), this)
end
CPlayerShop_EquipSearch.m_OnCheckBoxGroupSelect_CS2LuaHook = function (this, checkbox, index) 
    local tip = LocalString.GetString("[7F7F7F]点击选择[-]")
    if index == 0 then
        this:ReloadColorData()
        this:ReloadEquipData()
        this.m_KangXingWordButton.Text = tip
        this.m_KangXingValueButton:OverrideText(tip)
    else
        this:ReloadKangXingData()
        this.m_ColorButton.Text = tip
        this.m_EquipNameButton.Text = tip
        this.m_KangXingValueButton:SetValue(2, true)
        this:OnKeyBoardClosed(2)
    end
    this.m_IsWordSearch = (index == 1)
    if this.m_FocusButton ~= nil then
        this.m_FocusButton.Visible = not this.m_IsWordSearch
    end
    this.m_KangXingWordButton.Enabled = (index == 1)
    this.m_KangXingValueButton:SetInputEnabled(index == 1)
    this.m_KangXingTypeButton.Enabled = (index == 1)
    this.m_ColorButton.Enabled = (index == 0)
    this.m_EquipNameButton.Enabled = (index == 0)
end
CPlayerShop_EquipSearch.m_ReloadKangXingData_CS2LuaHook = function (this) 
    this.m_KangXingTypeActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetKangXingTypeNames(), MakeDelegateFromCSFunction(this.KangXingTypeAction, MakeGenericClass(Action1, Int32), this))
    if this.m_KangXingTypeActions.Count > 0 then
        this.SelectKangXingType = math.min(math.max(this.SelectKangXingType, 0), this.m_KangXingTypeActions.Count - 1)
        this.m_KangXingTypeButton.Text = this.m_KangXingTypeActions[this.SelectKangXingType].text
    end

    this.m_KangXingWordActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetKangXingWordsNames(), MakeDelegateFromCSFunction(this.KangXingWordAction, MakeGenericClass(Action1, Int32), this))
    if this.m_KangXingWordActions.Count > 0 then
        this.SelectKangXingWord = math.min(math.max(this.SelectKangXingWord, 0), this.m_KangXingWordActions.Count - 1)
        this.m_KangXingWordButton.Text = this.m_KangXingWordActions[this.SelectKangXingWord].text
    end
end
CPlayerShop_EquipSearch.m_OnClickKangXingWordButton_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_KangXingWordActions), button.transform, AlignType.Top, 2, nil, nil, DelegateFactory.Action(function () 
		SAFE_CALL(function() this.m_KangXingWordButton:SetTipStatus(false) end)
    end), 600,420)
end
CPlayerShop_EquipSearch.m_KangXingWordAction_CS2LuaHook = function (this, row) 
    if this.m_KangXingWordButton ~= nil and this.m_KangXingWordActions ~= nil and this.m_KangXingWordActions.Count > row then
        this.SelectKangXingWord = row
        this.m_KangXingWordButton.Text = this.m_KangXingWordActions[this.SelectKangXingWord].text
    end
end
CPlayerShop_EquipSearch.m_OnClickKangXingTypeButton_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_KangXingTypeActions), button.transform, AlignType.Top, 1, nil, nil, DelegateFactory.Action(function () 
		SAFE_CALL(function () this.m_KangXingTypeButton:SetTipStatus(false) end)
    end), 600,420)
end
CPlayerShop_EquipSearch.m_KangXingTypeAction_CS2LuaHook = function (this, row) 
    if this.m_KangXingTypeButton ~= nil and this.m_KangXingTypeActions ~= nil and this.m_KangXingTypeActions.Count > row then
        this.SelectKangXingType = row
        this.m_KangXingTypeButton.Text = this.m_KangXingTypeActions[this.SelectKangXingType].text
        this:OnKeyBoardClosed(this.m_KangXingValueButton:GetValue())
    end
end
CPlayerShop_EquipSearch.m_OnKeyBoardClosed_CS2LuaHook = function (this, value) 
    if this.SelectKangXingType == 0 then
        this.m_KangXingValueButton:OverrideText(">=" .. value)
    else
        this.m_KangXingValueButton:OverrideText((">=" .. value) .. "%")
    end
end
CPlayerShop_EquipSearch.m_ReloadEquipData_CS2LuaHook = function (this) 

    if this.SelectLevel >= 0 and this.SelectLevel < this.m_LevelActions.Count and this.SelectType >= 0 and this.SelectType < this.m_TypeActions.Count and this.SelectColor >= 0 and this.SelectColor < this.m_ColorActions.Count and this.SelectSubType >= 0 and this.SelectSubType < this.m_SubTypeActions.Count then
        local weapons = CPlayerShopMgr.Inst:SearchEquip(this.m_LevelActions[this.SelectLevel].text, this.m_TypeActions[this.SelectType].text, this.m_SubTypeActions[this.SelectSubType].text, this.SelectHandTypeName, this.m_ColorActions[this.SelectColor].text)
        CommonDefs.ListClear(this.m_SearchTemplateIds)
        CommonDefs.ListClear(this.m_SearchTemplateColors)
        CommonDefs.ListClear(this.m_SearchNames)

        CommonDefs.ListAddRange(this.m_SearchTemplateIds, weapons.Keys)
        CommonDefs.ListSort1(this.m_SearchTemplateIds, typeof(UInt32), DelegateFactory.Comparison_uint(function (a, b) 
            local data1 = EquipmentTemplate_Equip.GetData(a)
            if data1 == nil then
                data1 = EquipmentTemplate_Equip.GetData(CPlayerShopMgr.Inst:FromAliasTemplateId(a))
            end
            local data2 = EquipmentTemplate_Equip.GetData(b)
            if data2 == nil then
                data2 = EquipmentTemplate_Equip.GetData(CPlayerShopMgr.Inst:FromAliasTemplateId(b))
            end
            if data1 == nil or data2 == nil then
                return 0
            end
            if data1.Grade == data2.Grade then
                if data1.ID == data2.ID then
                    return NumberCompareTo(a, b)
                end
                return NumberCompareTo(data1.ID, data2.ID)
            end
            return NumberCompareTo(data1.Grade, data2.Grade)
        end))
        this.m_SearchTemplateColors = CPlayerShopMgr.Inst:GetColors(this.m_SearchTemplateIds)
        do
            local i = 0
            while i < this.m_SearchTemplateIds.Count do
                CommonDefs.ListAdd(this.m_SearchNames, typeof(String), CommonDefs.DictGetValue(weapons, typeof(UInt32), this.m_SearchTemplateIds[i]))
                i = i + 1
            end
        end

        this.m_EquipNameActions = CPlayerShopMgr.Inst:ConvertOptions(this.m_SearchNames, MakeDelegateFromCSFunction(this.EquipAction, MakeGenericClass(Action1, Int32), this))
        if this.m_EquipNameActions.Count > 0 then
            this.SelectEquip = 0
            if  CYuanbaoMarketMgr.Jump2SelectType then
                local templateId = CYuanbaoMarketMgr.SearchTemplateId
                templateId = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
                local equip = EquipmentTemplate_Equip.GetData(templateId)
                if equip then
                    local name = equip.Name
                    if templateId ~= CYuanbaoMarketMgr.SearchTemplateId then
                        name = equip.AliasName
                    end
                    for i = 0,this.m_EquipNameActions.Count - 1,1 do
                        if this.m_EquipNameActions[i].text == name then
                            this.SelectEquip = i
                        end
                    end    
                end
            end
            this.SelectEquip = math.min(math.max(this.SelectEquip, 0), this.m_EquipNameActions.Count - 1)
        
            if this.m_FocusButton ~= nil then
                local focus = CPlayerShopMgr.Inst:IsFocusTemplate(this.m_SearchTemplateIds[this.SelectEquip])
                local default
                if focus then
                    default = LocalString.GetString("取消关注")
                else
                    default = LocalString.GetString("关注此类")
                end
                this.m_FocusButton.Text = default
            end
            this.m_EquipNameButton.Text = this.m_EquipNameActions[this.SelectEquip].text

            CommonDefs.GetComponentInChildren_Component_Type(this.m_EquipNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[0]
        else
            this.m_EquipNameButton.Text = LocalString.GetString("空")
            MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何装备,请重新选择！"), nil)
        end
    else
        this.m_EquipNameButton.Text = LocalString.GetString("空")
        MessageWndManager.ShowOKMessage(LocalString.GetString("当前你选择的分类中没有任何装备,请重新选择！"), nil)
    end
end
CPlayerShop_EquipSearch.m_ReloadSubTypeData_CS2LuaHook = function (this) 

    local subTypeNames = CPlayerShopMgr.Inst:GetEquipSubTypeGroupNames(this.m_TypeActions[this.SelectType].text)
    this.m_SubTypeActions = CPlayerShopMgr.Inst:ConvertOptions(subTypeNames, MakeDelegateFromCSFunction(this.SubTypeAction, MakeGenericClass(Action1, Int32), this))

    if this.m_UseRecommandSettings then
        local recommandSubTypeName = this:GetRecommandSubTypeName(this.m_TypeActions[this.SelectType].text)
        local recommandIndex = - 1
        do
            local i = 0
            while i < this.m_SubTypeActions.Count do
                if this.m_SubTypeActions[i].text == recommandSubTypeName then
                    recommandIndex = i
                    break
                end
                i = i + 1
            end
        end
        if recommandIndex ~= - 1 then
            this.SelectSubType = recommandIndex
        end
    end
    if  CYuanbaoMarketMgr.Jump2SelectType then
        local templateId = CYuanbaoMarketMgr.SearchTemplateId
        templateId = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        if equip then
            local subTypeName = EquipmentTemplate_SubType.GetData(equip.Type * 100 + equip.SubType).Name
            for i = 0,this.m_SubTypeActions.Count - 1,1 do
                if this.m_SubTypeActions[i].text == subTypeName then
                    this.SelectSubType = i
                end
            end
        end
    end
    if this.m_SubTypeActions.Count > 0 then
        this.SelectSubType = math.min(math.max(this.SelectSubType, 0), this.m_SubTypeActions.Count - 1)
        this.m_SubTypeButton.Text = this.m_SubTypeActions[this.SelectSubType].text
    end
    this:ReloadHandTypeData()
end

CPlayerShop_EquipSearch.m_ReloadTypeData_CS2LuaHook = function (this) 
    this.m_TypeActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetEquipTypeGroupNames(), MakeDelegateFromCSFunction(this.TypeAction, MakeGenericClass(Action1, Int32), this))
    if this.m_TypeActions.Count > 0 then
        if  CYuanbaoMarketMgr.Jump2SelectType then
            local templateId = CYuanbaoMarketMgr.SearchTemplateId
            templateId = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
            local equip = EquipmentTemplate_Equip.GetData(templateId)
            if equip then
                local type = equip.Type
                if type < 17 then
                    local typeName = EquipmentTemplate_Type.GetData(type).Name
                    for i = 0,this.m_TypeActions.Count - 1,1 do
                        if this.m_TypeActions[i].text == typeName then
                            this.SelectType = i
                        end
                    end
                end
            end
        end
        this.SelectType = math.min(math.max(this.SelectType, 0), this.m_TypeActions.Count - 1)
        this.m_TypeButton.Text = this.m_TypeActions[this.SelectType].text
    end
    this:ReloadSubTypeData()
end
CPlayerShop_EquipSearch.m_ReloadColorData_CS2LuaHook = function (this) 
    this.m_ColorActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetEquipColorGroupNames(this.m_SearchOption == SearchOption.Valueable), MakeDelegateFromCSFunction(this.ColorAction, MakeGenericClass(Action1, Int32), this))
    --#101953 【优化】玩家商店珍品装备筛选，默认装备改取鬼装
    if not this.m_InitValueableSearch and this.m_SearchOption == SearchOption.Valueable then
        this.SelectColor = this.m_ColorActions.Count - 1
        this.m_InitValueableSearch = true
    end
    if this.m_UseRecommandSettings then
        local recommandColorName = this:GetRecommanColorName(this.m_TypeActions[this.SelectType].text)
        local recommandIndex = - 1
        do
            local i = 0
            while i < this.m_ColorActions.Count do
                if this.m_ColorActions[i].text == recommandColorName then
                    recommandIndex = i
                    break
                end
                i = i + 1
            end
        end
        if recommandIndex ~= - 1 then
            this.SelectColor = recommandIndex
        end
    end

    if  CYuanbaoMarketMgr.Jump2SelectType then
        this.m_ColorActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetEquipColorGroupNames(CYuanbaoMarketMgr.SearchEquipValueable == SearchOption.Valueable), MakeDelegateFromCSFunction(this.ColorAction, MakeGenericClass(Action1, Int32), this))
        local templateId = CYuanbaoMarketMgr.SearchTemplateId
        templateId = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        if equip then
            local colorName = EquipmentTemplate_Color.GetData(equip.Color).Name
            for i = 0,this.m_ColorActions.Count - 1,1 do
                if this.m_ColorActions[i].text == colorName then
                    this.SelectColor = i
                end
            end
        end
    end

    if this.m_ColorActions.Count > 0 then
        this.SelectColor = math.min(math.max(this.SelectColor, 0), this.m_ColorActions.Count - 1)
        this.m_ColorButton.Text = this.m_ColorActions[this.SelectColor].text
    end
end
CPlayerShop_EquipSearch.m_ReloadLevelData_CS2LuaHook = function (this) 

    this.m_LevelActions = CPlayerShopMgr.Inst:ConvertOptions(CPlayerShopMgr.Inst:GetEquipLevelGroupNames(), MakeDelegateFromCSFunction(this.LevelAction, MakeGenericClass(Action1, Int32), this))

    if this.m_UseRecommandSettings then
        if CClientMainPlayer.Inst ~= nil then
            if CClientMainPlayer.Inst.Level <= 69 then
                this.SelectLevel = 0
            elseif CClientMainPlayer.Inst.Level <= 89 then
                this.SelectLevel = 1
            elseif CClientMainPlayer.Inst.Level <= 109 then
                this.SelectLevel = 2
            elseif CClientMainPlayer.Inst.Level <= 129 then
                this.SelectLevel = 3
            else
                this.SelectLevel = 4
            end
        end
    end

    if  CYuanbaoMarketMgr.Jump2SelectType then
        local templateId = CYuanbaoMarketMgr.SearchTemplateId
        templateId = CPlayerShopMgr.Inst:FromAliasTemplateId(templateId)
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        if equip then
            local equipLevel = equip.Grade
            if equipLevel <= 69 then
                this.SelectLevel = 0
            elseif equipLevel <= 89 then
                this.SelectLevel = 1
            elseif equipLevel <= 109 then
                this.SelectLevel = 2
            elseif equipLevel <= 129 then
                this.SelectLevel = 3
            else
                this.SelectLevel = 4
            end
        end
    end

    if this.m_LevelActions.Count > 0 then
        this.SelectLevel = math.min(math.max(this.SelectLevel, 0), this.m_LevelActions.Count - 1)
        this.m_LevelButton.Text = this.m_LevelActions[this.SelectLevel].text
    end
end
CPlayerShop_EquipSearch.m_OnTypeButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_TypeActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_TypeActions.Count), nil, nil, DelegateFactory.Action(function () 
		SAFE_CALL(function() this.m_TypeButton:SetTipStatus(false) end)
    end), 600,420)
end
CPlayerShop_EquipSearch.m_OnSubTypeButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_SubTypeActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_SubTypeActions.Count), nil, nil, DelegateFactory.Action(function () 
		SAFE_CALL(function() this.m_SubTypeButton:SetTipStatus(false) end)
    end), 600,420)
end
CPlayerShop_EquipSearch.m_OnEquipNameButtonOnClick_CS2LuaHook = function (this, button) 
    local param = CreateFromClass(ItemSearchParams, this.m_SearchTemplateIds, this.IsPublicity, this.m_SearchOption)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_EquipNameActions), button.transform, AlignType.Top, this:GetColumnCount(this.m_EquipNameActions.Count), this.m_SearchTemplateColors, param, DelegateFactory.Action(function () 
		SAFE_CALL(function() this.m_EquipNameButton:SetTipStatus(false) end)
    end), 600,420)
end
CPlayerShop_EquipSearch.m_OnColorButtonOnClick_CS2LuaHook = function (this, button) 
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_ColorActions), button.transform, AlignType.Bottom, this:GetColumnCount(this.m_ColorActions.Count), nil, nil, DelegateFactory.Action(function () 
		SAFE_CALL(function() this.m_ColorButton:SetTipStatus(false) end)
    end), 600,420)
end
CPlayerShop_EquipSearch.m_OnSearchButtonOnClick_CS2LuaHook = function (this, button) 
    if this.m_IsWordSearch then
        local isPercent = (this.SelectKangXingType == 1)
        local wordName = this.m_KangXingWordActions[this.SelectKangXingWord].text
        local minValue = this.m_KangXingValueButton:GetValue()
        local gradeRange = this.m_Equip_GradeRange[this.SelectLevel]
        local type = CPlayerShopMgr.Inst:GetEquipTypeByName(this.m_TypeActions[this.SelectType].text)
        local subtype = CPlayerShopMgr.Inst:GetEquipSubTypeByName(type, this.m_SubTypeActions[this.SelectSubType].text)
        local wordIds = CPlayerShopMgr.Inst:SearchWords(wordName, isPercent, minValue)
        local wordIdstr = this:ConcatWordIds(wordIds)
        if this.OnSearchWordsCallback ~= nil then
            GenericDelegateInvoke(this.OnSearchWordsCallback, Table2ArrayWithCount({wordIdstr, gradeRange, type, subtype, this.m_SearchOption}, 5, MakeArrayClass(Object)))
        end
    else
        if this.SelectEquip < 0 or this.SelectEquip >= this.m_SearchTemplateIds.Count then
            return
        end
        local itemId = this.m_SearchTemplateIds[this.SelectEquip]
        GenericDelegateInvoke(this.OnSearchCallback, Table2ArrayWithCount({itemId, this.m_SearchOption, false}, 3, MakeArrayClass(Object)))
    end
end
CPlayerShop_EquipSearch.m_OnFocusButtonClick_CS2LuaHook = function (this, button) 
    if this.SelectEquip < 0 or this.SelectEquip >= this.m_SearchTemplateIds.Count then
        return
    end
    local itemId = this.m_SearchTemplateIds[this.SelectEquip]
    local allFocus = CPlayerShopMgr.Inst:GetAllFocusTemplate()
    if CommonDefs.HashSetContains(allFocus, typeof(UInt32), itemId) then
        CommonDefs.HashSetRemove(allFocus, typeof(UInt32), itemId)
        CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("取消关注成功"))
        this.m_FocusButton.Text = LocalString.GetString("关注此类")
    else
        CommonDefs.HashSetAdd(allFocus, typeof(UInt32), itemId)
        if allFocus.Count <= CPlayerShopMgr.s_FocusTemplateLimit then
            CPlayerShopMgr.Inst:SetAllFocusTemplate(allFocus)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注成功"))
            this.m_FocusButton.Text = LocalString.GetString("取消关注")
        else
            g_MessageMgr:ShowMessage("PLAYER_SHOP_MAX_COLLECTIONS")
        end
    end
end
CPlayerShop_EquipSearch.m_ConcatWordIds_CS2LuaHook = function (this, wordIds) 
    local builder = NewStringBuilderWraper(StringBuilder)
    if wordIds ~= nil then
        do
            local i = 0
            while i < wordIds.Count - 1 do
                CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, wordIds[i])
                builder:Append(",")
                i = i + 1
            end
        end
        if wordIds.Count >= 1 then
            CommonDefs.StringBuilder_Append_StringBuilder_UInt32(builder, wordIds[wordIds.Count - 1])
        end
    end
    return ToStringWrap(builder)
end
CPlayerShop_EquipSearch.m_LevelAction_CS2LuaHook = function (this, row) 

    if this.m_LevelButton ~= nil and this.m_LevelActions ~= nil and this.m_LevelActions.Count > row then
        this.SelectLevel = row
        this.m_LevelButton.Text = this.m_LevelActions[this.SelectLevel].text
        this:ReloadEquipData()
    end
end
CPlayerShop_EquipSearch.m_TypeAction_CS2LuaHook = function (this, row) 

    if this.m_TypeButton ~= nil and this.m_TypeActions ~= nil and this.m_TypeActions.Count > row then
        this.SelectType = row
        this.m_TypeButton.Text = this.m_TypeActions[this.SelectType].text
        this.m_UseRecommandSettings = true
        this:ReloadSubTypeData()
        this:ReloadEquipData()
    end
end
CPlayerShop_EquipSearch.m_SubTypeAction_CS2LuaHook = function (this, row) 

    if this.m_SubTypeButton ~= nil and this.m_SubTypeActions ~= nil and this.m_SubTypeActions.Count > row then
        this.SelectSubType = row
        this.m_SubTypeButton.Text = this.m_SubTypeActions[this.SelectSubType].text
        this:ReloadHandTypeData()
        this:ReloadEquipData()
    end
end
CPlayerShop_EquipSearch.m_ColorAction_CS2LuaHook = function (this, row) 

    if this.m_ColorButton ~= nil and this.m_ColorActions ~= nil and this.m_ColorActions.Count > row then
        this.SelectColor = row
        this.m_ColorButton.Text = this.m_ColorActions[this.SelectColor].text
        this:ReloadEquipData()
    end
end
CPlayerShop_EquipSearch.m_EquipAction_CS2LuaHook = function (this, row) 

    if this.m_EquipNameButton ~= nil and this.m_EquipNameActions ~= nil and this.m_EquipNameActions.Count > row then
        this.SelectEquip = row
        if this.m_FocusButton ~= nil then
            local focus = CPlayerShopMgr.Inst:IsFocusTemplate(this.m_SearchTemplateIds[this.SelectEquip])
            local default
            if focus then
                default = LocalString.GetString("取消关注")
            else
                default = LocalString.GetString("关注此类")
            end
            this.m_FocusButton.Text = default
        end
        this.m_EquipNameButton.Text = this.m_EquipNameActions[row].text
        CommonDefs.GetComponentInChildren_Component_Type(this.m_EquipNameButton, typeof(UILabel)).color = this.m_SearchTemplateColors[row]
    end
end
CPlayerShop_EquipSearch.m_GetColumnCount_CS2LuaHook = function (this, count) 
    local columns = 1
    if count >= 6 then
        columns = 2
    end
    return columns
end
CPlayerShop_EquipSearch.m_GetRecommandSubTypeName_CS2LuaHook = function (this, typeName) 
    local type = this:GetEquipTypeByName(typeName)
    local recommandKey = type
    if CClientMainPlayer.Inst ~= nil then
        recommandKey = recommandKey + (EnumToInt(CClientMainPlayer.Inst.Class) * 100)
    end
    local data = EquipmentTemplate_Recommand.GetData(recommandKey)
    if data ~= nil then
        return CPlayerShopMgr.Inst:GetEquipSubTypeName(type, data.SubType)
    end
    return ""
end
CPlayerShop_EquipSearch.m_GetRecommanColorName_CS2LuaHook = function (this, typeName) 
    local type = this:GetEquipTypeByName(typeName)
    local recommandKey = type
    if CClientMainPlayer.Inst ~= nil then
        recommandKey = recommandKey + (EnumToInt(CClientMainPlayer.Inst.Class) * 100)
    end
    local data = EquipmentTemplate_Recommand.GetData(recommandKey)
    if data ~= nil then
        return CPlayerShopMgr.Inst:GetEquipColorName(data.Color, true)
    end
    return ""
end
CPlayerShop_EquipSearch.m_GetEquipTypeByName_CS2LuaHook = function (this, name) 
    local ret = 1
    EquipmentTemplate_Type.Foreach(function (k, v) 
        if v.Name == name then
            ret = v.Type
        end
    end)
    return ret
end

-- #208604 易市搜索装备界面增加单、双手选择-客户端支持 【2023.10.26投放】10.12Build版本
CPlayerShop_EquipSearch.m_ReloadHandTypeData_CS2LuaHook = function (this)
    if this.m_HandTypeButtons.Count == 0 then -- 初始化
        for i = 0, this.m_HandType.transform.childCount - 1 do
            local button = this.m_HandType.transform:GetChild(i):GetComponent(typeof(QnCheckBox))
            CommonDefs.ListAdd(this.m_HandTypeButtons, typeof(QnCheckBox), button)
            button.OnClick = MakeDelegateFromCSFunction(this.HandTypeAction, MakeGenericClass(Action1, QnButton), this)
        end
    end

    this.SelectHandTypeName = ""
    local handTypeNames = CPlayerShopMgr.Inst:GetEquipHandTypeGroupNames(this.m_SubTypeActions[this.SelectSubType].text)
    if handTypeNames.Count > 0 then
        local tbl = {}
        for i = 0, handTypeNames.Count - 1,1 do
            tbl[handTypeNames[i]] = true
        end
        for i = 0, this.m_HandTypeButtons.Count - 1 do
            local btn = this.m_HandTypeButtons[i]
            btn.m_BackGround.alpha = tbl[btn.Text] and 1 or 0.5
            btn.transform:GetComponent(typeof(BoxCollider)).enabled = tbl[btn.Text]
            if this.SelectHandTypeName == "" and tbl[btn.Text] then
                this.SelectHandTypeName = btn.Text
            end
            btn:SetSelected(this.SelectHandTypeName == btn.Text, false)
        end
        this.m_HandType.gameObject:SetActive(true)
    else
        this.m_HandType.gameObject:SetActive(false)
    end
end
CPlayerShop_EquipSearch.m_HandTypeAction_CS2LuaHook = function (this, btn)
    this.SelectHandTypeName = btn.Text
    for i = 0, this.m_HandTypeButtons.Count - 1 do
        this.m_HandTypeButtons[i]:SetSelected(this.m_HandTypeButtons[i].Text == btn.Text, false)
    end
    this:ReloadEquipData()
end