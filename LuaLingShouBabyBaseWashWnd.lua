require("common/common_include")
require("ui/lingshou/wash/LuaLingShouBabyWashResultView")

local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local Mall_LingYuMall=import "L10.Game.Mall_LingYuMall"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType=import "L10.UI.CTooltip+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local MessageMgr=import "L10.Game.MessageMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

CLuaLingShouBabyBaseWashWnd=class()
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_RecommendSprite")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_View1")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_View2")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_BabyInfo")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_LingShouId")

RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_StartButton")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_RequestLabel")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_AcceptButton")

RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_MoneyCtrl")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_BiliuluIcon")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_BiliuluLabel")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_BiliuluCountUpdate")
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_BiliuluMask")

RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_Pause")--突然弹出其他界面会暂停
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_StartWaitTime")

RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_AutoCostToggle")--自动消耗灵玉
RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_JadeCost")

RegistClassMember(CLuaLingShouBabyBaseWashWnd,"m_IsFirstClick")


function CLuaLingShouBabyBaseWashWnd:AutoCostLingyu()
    return self.m_AutoCostToggle.value
end

function CLuaLingShouBabyBaseWashWnd:InitBiliulu()
    --碧柳露
    self.m_MoneyCtrl=LuaGameObject.GetChildNoGC(self.transform,"MoneyCtrl").moneyCtrl
    local mallData = Mall_LingYuMall.GetData(CLuaLingShouMgr.BiLiuLuId)
    self.m_JadeCost = mallData.Jade
    --判断是否可以使用绑定灵玉
    if mallData.CanUseBindJade > 0 then
        self.m_MoneyCtrl:SetType(EnumMoneyType.LingYu_WithBind,EnumPlayScoreKey.NONE, true)
    else
        self.m_MoneyCtrl:SetType(EnumMoneyType.LingYu,EnumPlayScoreKey.NONE, true)
    end
    self.m_MoneyCtrl:SetCost(self.m_JadeCost)

    self.m_BiliuluIcon=LuaGameObject.GetChildNoGC(self.transform,"BiliuluIcon").cTexture
    self.m_BiliuluLabel=LuaGameObject.GetChildNoGC(self.transform,"BiliuluLabel").label
    self.m_BiliuluMask=LuaGameObject.GetChildNoGC(self.transform,"BiliuluMask").gameObject
    --数量刷新
    self.m_BiliuluCountUpdate=LuaGameObject.GetChildNoGC(self.transform,"BiliuluCountUpdate").itemCountUpdate
    self.m_BiliuluCountUpdate.templateId=CLuaLingShouMgr.BiLiuLuId
    self.m_BiliuluCountUpdate.excludeExpireTime = true
    self.m_BiliuluCountUpdate.format="{0}/1"
    local function OnCountChange(val)
        if val==0 then
            self.m_BiliuluCountUpdate.format="[ff0000]{0}[-]/1"
            self.m_BiliuluMask:SetActive(true)
        else
            self.m_BiliuluCountUpdate.format="{0}/1"
            self.m_BiliuluMask:SetActive(false)
        end
    end
    self.m_BiliuluCountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    self.m_BiliuluCountUpdate:UpdateCount()

    local data = CItemMgr.Inst:GetItemTemplate(CLuaLingShouMgr.BiLiuLuId)
    if data then
        self.m_BiliuluIcon:LoadMaterial(data.Icon)
        self.m_BiliuluLabel.text=data.Name
    else
        self.m_BiliuluIcon:Clear()
        self.m_BiliuluLabel=" "
    end
    local g = LuaGameObject.GetChildNoGC(self.transform,"BiliuluCountUpdate").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_BiliuluCountUpdate.count==0 then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(CLuaLingShouMgr.BiLiuLuId, false, go.transform, AlignType.Right)
        end
    end)

    LuaGameObject.GetChildNoGC(self.transform,"JadeNode").gameObject:SetActive(false)
    self.m_AutoCostToggle=LuaGameObject.GetChildNoGC(self.transform,"AutoBuyToggle").toggle
    EventDelegate.Add(self.m_AutoCostToggle.onChange,DelegateFactory.Callback(function () 
        local val = self.m_AutoCostToggle.value
        LuaGameObject.GetChildNoGC(self.transform,"JadeNode").gameObject:SetActive(val)
    end))

end
function CLuaLingShouBabyBaseWashWnd:Init()
    self:InitBiliulu()

    self.m_AcceptButton=LuaGameObject.GetChildNoGC(self.transform,"AcceptButton").gameObject
    UIEventListener.Get(self.m_AcceptButton).onClick = DelegateFactory.VoidDelegate(function(go)
        --替换新属性
        CUICommonDef.SetActive(self.m_AcceptButton,false,true)
        Gac2Gas.AccpetLastLingShouWashBabyResult(self.m_LingShouId)
    end)
    
    self.m_StartButton=LuaGameObject.GetChildNoGC(self.transform,"StartButton").gameObject
    UIEventListener.Get(self.m_StartButton).onClick = DelegateFactory.VoidDelegate(function(go)
        --开始洗炼
        self:StartWash()
    end)
    self.m_RequestLabel=LuaGameObject.GetChildNoGC(self.transform,"RequestLabel").label
    self.m_RecommendSprite=LuaGameObject.GetChildNoGC(self.transform,"RecommendSprite").sprite
    self.m_RecommendSprite.gameObject:SetActive(false)

    self.m_View1=CLuaLingShouBabyWashResultView:new()
    local template=LuaGameObject.GetChildNoGC(self.transform,"View1").gameObject
    local go=NGUITools.AddChild(template.transform.parent.gameObject,template)
    go.transform.localPosition = Vector3(-154,-321,0)

    self.m_View1:Init(template.transform)
    self.m_View2=CLuaLingShouBabyWashResultView:new()
    self.m_View2:Init(go.transform)
    self.m_View2:SetTitle(LocalString.GetString("新属性"))
    
    self:InitBabyInfo()

    self.m_IsFirstClick=true
end
function CLuaLingShouBabyBaseWashWnd:StartWash()
	local function ok()
		if self.m_BabyInfo then
			local newQuality=self.m_BabyInfo.LastWashQuality
			local needWarning=false
			if newQuality>self.m_BabyInfo.Quality then
				needWarning=true
			end
			if not needWarning then
				if newQuality==5 or newQuality==6 then--判断品质是甲或者乙
					needWarning=true
				end
			end
			if needWarning then
				local content = MessageMgr.Inst:FormatMessage("LINGSHOU_XIBAOBAO_CONFIRM",{})
				MessageWndManager.ShowOKCancelMessage(content,
					DelegateFactory.Action(function ()
						self:RequestWash()
					end),nil,nil,nil,false)
			else
				self:RequestWash()
			end
		end
	end
	local needAlert=false
	if self.m_BabyInfo.Level<50 then
		needAlert=true
	end
	if needAlert then
		if self.m_IsFirstClick==false then
			needAlert=false
		end
	end
	if needAlert then
		MessageWndManager.ShowOKCancelMessage(
			MessageMgr.Inst:FormatMessage("CUSTOM_STRING2", {LocalString.GetString("当前灵兽宝宝等级不足50级，建议升级至50级后洗炼。你确定要继续洗炼吗？")}), 
			DelegateFactory.Action(ok), nil,nil,nil,false)
		self.m_IsFirstClick=false
	else
		ok()
	end
end

function CLuaLingShouBabyBaseWashWnd:InitBabyInfo()
    local lingShouId=CLingShouMgr.Inst.selectedLingShou
    self.m_LingShouId=lingShouId
    if lingShouId~=nil or lingShouId~="" then
        self.m_RecommendSprite.gameObject:SetActive(false)
        local details=CLingShouMgr.Inst:GetLingShouDetails(lingShouId)
        if details~=nil then
            local babyInfo = details.data.Props.Baby
            self.m_BabyInfo=babyInfo
            if babyInfo.BornTime>0 then
                self.m_View1:InitData(babyInfo)
                if babyInfo.LastWashQuality>0 then
                    self.m_View2:InitResult(babyInfo,babyInfo.LastWashQuality,babyInfo.LastWashSkillCls1,babyInfo.LastWashSkillCls2)
                    --有上次洗炼的结果
                    CUICommonDef.SetActive(self.m_AcceptButton,true,true)
                else
                    self.m_View2:InitData(nil)
                    CUICommonDef.SetActive(self.m_AcceptButton,false,true)
                end
                if babyInfo.LastWashQuality>babyInfo.Quality then
                    self.m_RecommendSprite.gameObject:SetActive(true)
                else
                    self.m_RecommendSprite.gameObject:SetActive(false)
                end
            else

            end
        end
    end
end


function CLuaLingShouBabyBaseWashWnd:OnEnable()
     g_ScriptEvent:AddListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
     g_ScriptEvent:AddListener("Guide_ChangeView", self, "OnChangeView")
     g_ScriptEvent:AddListener("UpdateLingShouBabyLastWashResult", self, "OnUpdateLingShouBabyLastWashResult")
end


function CLuaLingShouBabyBaseWashWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
    g_ScriptEvent:RemoveListener("Guide_ChangeView", self, "OnChangeView")
    g_ScriptEvent:RemoveListener("UpdateLingShouBabyLastWashResult", self, "OnUpdateLingShouBabyLastWashResult")
end
function CLuaLingShouBabyBaseWashWnd:OnChangeView(args)
    if not CClientMainPlayer.Inst then
        self.m_Pause=true
    end
    local top=CUIManager.instance:GetTopPopUI()
    if top then
        if top.Name=="LingShouBabyWashWnd" then
            self.m_Pause=false
        else
            self.m_Pause=true
        end
    end
end
function CLuaLingShouBabyBaseWashWnd:OnUpdateLingShouBabyInfo(args)
    local lingShouId=args[0]
    if self.m_LingShouId==lingShouId then
        self:InitBabyInfo()
    end
end

return CLuaLingShouBabyBaseWashWnd
