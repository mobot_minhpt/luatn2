require("common/common_include")


local GameObject  = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local MessageMgr = import "L10.Game.MessageMgr"
local UITabBar = import "L10.UI.UITabBar"

CLuaMergeBattleWnd=class()
RegistChildComponent(CLuaMergeBattleWnd, "m_OverviewBtn", CButton)
RegistChildComponent(CLuaMergeBattleWnd, "m_DataBtn", CButton)
RegistChildComponent(CLuaMergeBattleWnd, "m_PointsBtn", CButton)
RegistChildComponent(CLuaMergeBattleWnd, "m_RecallBtn", CButton)
RegistChildComponent(CLuaMergeBattleWnd, "m_BindBtn", CButton)
RegistChildComponent(CLuaMergeBattleWnd, "m_OverviewDetail",GameObject)
RegistChildComponent(CLuaMergeBattleWnd, "m_DataDetail",GameObject)
RegistChildComponent(CLuaMergeBattleWnd, "m_PointsDetail",GameObject)
RegistChildComponent(CLuaMergeBattleWnd, "m_RecallDetail",GameObject)
RegistChildComponent(CLuaMergeBattleWnd, "m_BindDetail",GameObject)
RegistChildComponent(CLuaMergeBattleWnd, "CloseButton", CButton)
RegistChildComponent(CLuaMergeBattleWnd, "MoreInfo", CButton)
RegistClassMember(CLuaMergeBattleWnd,"m_TabBar")
function CLuaMergeBattleWnd:Awake()
	
end

function CLuaMergeBattleWnd:Init()
	self.m_TabBar = self.transform:Find("MasterView/Scroll View/Table"):GetComponent(typeof(UITabBar))
	self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index)
    end)
	self.m_TabBar:ChangeTab(0, false)
	
	CommonDefs.AddOnClickListener(self.MoreInfo.gameObject, DelegateFactory.Action_GameObject(function (go)
		MessageMgr.Inst:ShowMessage("MergeBattleTIPs", {})
	end), false)
	
end

function CLuaMergeBattleWnd:OnTabChange(index)
	self.m_OverviewDetail:SetActive(index == 0)
	self.m_DataDetail:SetActive(index == 1)
	self.m_PointsDetail:SetActive(index == 2)
	self.m_RecallDetail:SetActive(index ==3)
	self.m_BindDetail:SetActive(index == 4)
end

function CLuaMergeBattleWnd:OnEnable()

end

function CLuaMergeBattleWnd:OnDisable()
	
end

