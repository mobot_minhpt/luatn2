local QnSelectableButton = import "L10.UI.QnSelectableButton"

local Extensions = import "Extensions"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local UIGrid=import "UIGrid"
local Item_Item=import "L10.Game.Item_Item"
local CUITexture=import "L10.UI.CUITexture"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

CLuaQMPKGetItemWnd = class()
RegistClassMember(CLuaQMPKGetItemWnd,"m_ItemTable")
RegistClassMember(CLuaQMPKGetItemWnd,"m_GetBtn")
RegistClassMember(CLuaQMPKGetItemWnd,"m_TypeTemplate")
RegistClassMember(CLuaQMPKGetItemWnd,"m_TypeGrid")
RegistClassMember(CLuaQMPKGetItemWnd,"m_TypeBtnList")
RegistClassMember(CLuaQMPKGetItemWnd,"m_AllItems")
RegistClassMember(CLuaQMPKGetItemWnd,"m_CurrentType")
RegistClassMember(CLuaQMPKGetItemWnd,"m_CurrentRow")
RegistClassMember(CLuaQMPKGetItemWnd,"m_GetItemType")

function CLuaQMPKGetItemWnd:Awake()
    self.m_ItemTable = self.transform:Find("ItemTableView"):GetComponent(typeof(QnTableView))
    self.m_GetBtn = self.transform:Find("Bottom/GetBtn").gameObject
    self.m_TypeTemplate = self.transform:Find("Left/Type").gameObject
    self.m_TypeTemplate:SetActive(false)
    self.m_TypeGrid = self.transform:Find("Left/Grid"):GetComponent(typeof(UIGrid))
self.m_TypeBtnList = {}
self.m_AllItems = {}
self.m_CurrentType = 0
self.m_CurrentRow = 0

self.m_GetItemType={
    LocalString.GetString("药品"),
    LocalString.GetString("食品"),
    LocalString.GetString("状态药"),
    LocalString.GetString("其他")
}

end

function CLuaQMPKGetItemWnd:OnEnable( )
    UIEventListener.Get(self.m_GetBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:GetItem(go) end)
end
function CLuaQMPKGetItemWnd:Init( )
    self.m_TypeBtnList ={}

    local typeCnt = #self.m_GetItemType
    self.m_AllItems = {}

    for i=1,typeCnt do
        table.insert( self.m_AllItems,{} )
    end

    local keys = {}
    QuanMinPK_GetItem.ForeachKey(function (key) 
        table.insert( keys,key )
    end)

    for i,v in ipairs(keys) do
        local data = QuanMinPK_GetItem.GetData(keys[i])
        local item={
            m_ItemId=data.ItemID,
            m_ItemName=data.Description,
        }
        table.insert( self.m_AllItems[data.Type],item )
    end

    self.m_TypeBtnList={}
    Extensions.RemoveAllChildren(self.m_TypeGrid.transform)
    do
        local i = 0
        while i < typeCnt do
            local go = NGUITools.AddChild(self.m_TypeGrid.gameObject, self.m_TypeTemplate)
            go:SetActive(true)
            table.insert( self.m_TypeBtnList,go:GetComponent(typeof(QnSelectableButton)) )
            go.transform:Find("Label"):GetComponent(typeof(UILabel)).text=self.m_GetItemType[i+1]
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go) self:OnTypeSelected(go) end)
            i = i + 1
        end
    end
    self.m_TypeGrid:Reposition()

    local getNumFunc=function() 
        if self.m_AllItems[self.m_CurrentType] then
            return #self.m_AllItems[self.m_CurrentType]
        end
        return 0
    end
    local initItemFunc=function(item,index) 
        self:InitItem(item,index)
    end

    self.m_ItemTable.m_DataSource=DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.m_ItemTable.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self.m_CurrentRow = row+1
    end)
    self:OnTypeSelected(self.m_TypeBtnList[1].gameObject)
    self.m_ItemTable:SetSelectRow(0, true)
end
function CLuaQMPKGetItemWnd:OnTypeSelected( go) 
    for i,v in ipairs(self.m_TypeBtnList) do
        if v.gameObject==go then
            self.m_CurrentType=i
            v:SetSelected(true,false)
        else
            v:SetSelected(false,false)
        end
    end
    self.m_ItemTable:ReloadData(false, false)
end

function CLuaQMPKGetItemWnd:GetItem( go) 
    Gac2Gas.RequestGetQmpkItem(self.m_AllItems[self.m_CurrentType][self.m_CurrentRow].m_ItemId)
end

function CLuaQMPKGetItemWnd:InitItem(item,row)
    local transform=item.transform
    if self.m_AllItems[self.m_CurrentType] ~= nil then
        local info=self.m_AllItems[self.m_CurrentType][row+1]
        local itemId=info.m_ItemId
        local desc=info.m_ItemName

        local data=Item_Item.GetData(itemId)
        if data then
            local icon=transform:Find("ItemCell"):GetComponent(typeof(CUITexture))
            icon:LoadMaterial(data.Icon)
            local nameLabel=transform:Find("name"):GetComponent(typeof(UILabel))
            local descLabel=transform:Find("Desc"):GetComponent(typeof(UILabel))
            nameLabel.text=data.Name
            descLabel.text=desc
            UIEventListener.Get(icon.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        end

    end
end

return CLuaQMPKGetItemWnd
