local UISlider = import "UISlider"
local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"

LuaHLPYHelpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHLPYHelpWnd, "HelpButton", "HelpButton", GameObject)
RegistChildComponent(LuaHLPYHelpWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaHLPYHelpWnd, "TextLabel", "TextLabel", UILabel)

--@endregion RegistChildComponent end

LuaHLPYHelpWnd.s_InRescueRadius = false
LuaHLPYHelpWnd.s_Progress = 0
LuaHLPYHelpWnd.s_YingJiu2022Status = false
LuaHLPYHelpWnd.s_RescueTargetEngineId = 0
function LuaHLPYHelpWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.HelpButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHelpButtonClick()
	end)


    --@endregion EventBind end
	self.HelpButton:SetActive(LuaHLPYHelpWnd.s_InRescueRadius)
	self.ProgressBar.value = LuaHLPYHelpWnd.s_Progress/100

	self.ProgressBar.gameObject:SetActive(false)
end

function LuaHLPYHelpWnd:Init()

end

--@region UIEvent

function LuaHLPYHelpWnd:OnHelpButtonClick()
	Gac2Gas.HLPY_StartRescue()
end


--@endregion UIEvent
function LuaHLPYHelpWnd:OnEnable()
    g_ScriptEvent:AddListener("HLPY_SyncRescueProgress", self, "OnHLPY_SyncRescueProgress")
    g_ScriptEvent:AddListener("HLPY_LeaveRescueRadius", self, "OnHLPY_LeaveRescueRadius")
    g_ScriptEvent:AddListener("HLPY_EnterRescueRadius", self, "OnHLPY_EnterRescueRadius")
    g_ScriptEvent:AddListener("OnEnter_YingJiu2022", self, "OnEnter_YingJiu2022")
    g_ScriptEvent:AddListener("OnLeave_YingJiu2022", self, "OnLeave_YingJiu2022")
	
end
function LuaHLPYHelpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HLPY_SyncRescueProgress", self, "OnHLPY_SyncRescueProgress")
    g_ScriptEvent:RemoveListener("HLPY_LeaveRescueRadius", self, "OnHLPY_LeaveRescueRadius")
    g_ScriptEvent:RemoveListener("HLPY_EnterRescueRadius", self, "OnHLPY_EnterRescueRadius")
	g_ScriptEvent:RemoveListener("OnEnter_YingJiu2022", self, "OnEnter_YingJiu2022")
    g_ScriptEvent:RemoveListener("OnLeave_YingJiu2022", self, "OnLeave_YingJiu2022")
end

function LuaHLPYHelpWnd:OnEnter_YingJiu2022()
	self.ProgressBar.gameObject:SetActive(LuaHLPYHelpWnd.s_YingJiu2022Status)
end
function LuaHLPYHelpWnd:OnLeave_YingJiu2022()
	self.ProgressBar.gameObject:SetActive(LuaHLPYHelpWnd.s_YingJiu2022Status)
end

function LuaHLPYHelpWnd:OnHLPY_SyncRescueProgress(progress,bExistsEnemy)
	self.ProgressBar.value = progress/100
	if bExistsEnemy then
		self.TextLabel.text = LocalString.GetString("附近有敌人，解救速度变慢。")
	else
		self.TextLabel.text = LocalString.GetString("解救中")
	end
	if progress>=100 then
		self.ProgressBar.gameObject:SetActive(false)
	end
end
function LuaHLPYHelpWnd:OnHLPY_LeaveRescueRadius()
	self.HelpButton:SetActive(false)
end
function LuaHLPYHelpWnd:OnHLPY_EnterRescueRadius()
	self.HelpButton:SetActive(true)
end
