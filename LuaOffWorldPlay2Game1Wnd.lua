

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Physics = import "UnityEngine.Physics"

LuaOffWorldPlay2Game1Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaOffWorldPlay2Game1Wnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay2Game1Wnd, "TipTextNode", "TipTextNode", UILabel)
RegistChildComponent(LuaOffWorldPlay2Game1Wnd, "play1", "play1", GameObject)
RegistChildComponent(LuaOffWorldPlay2Game1Wnd, "play2", "play2", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaOffWorldPlay2Game1Wnd, "guideFx")

function LuaOffWorldPlay2Game1Wnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

--@region UIEvent

--@endregion UIEvent


---------------------
function LuaOffWorldPlay2Game1Wnd:DoSucc()
	local fxTime = 2000
	if self.m_Tick then
    UnRegisterTick(self.m_Tick)
	end

	self.m_Tick = RegisterTickWithDuration(function ()
    Gac2Gas.CaiDieTaskFinish(LuaOffWorldPassMgr.Play2Game1Type)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPlay2Game1Wnd)
	end,fxTime,fxTime)

  if LuaOffWorldPassMgr.Play2Game1Type == 1 then
    self.play1.transform:Find('fxNode'):GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_caidiezhen_jianqiewancheng_xizi.prefab')
		self.play1.transform:Find('node').gameObject:SetActive(false)
  else
    self.play2.transform:Find('fxNode'):GetComponent(typeof(CUIFx)):LoadFx('fx/ui/prefab/UI_caidiezhen_jianqiewancheng_caidai.prefab')
		self.play2.transform:Find('node').gameObject:SetActive(false)
  end

end

function LuaOffWorldPlay2Game1Wnd:PlayMoveAni(index)
  local moveNode = self.moveNodeTable[index]
  local movePos = self.movePosTable[index]
  local moveTime = 500
  LuaTweenUtils.TweenPosition(moveNode.transform,movePos.x,movePos.y,movePos.z,moveTime/1000)

  if self.m_Tick then
    UnRegisterTick(self.m_Tick)
    self.m_Tick = nil
  end

  local totalNum = #self.moveNodeTable-1

  if LuaOffWorldPassMgr.Play2Game1Type == 1 then
    self.TipTextNode.text = SafeStringFormat3(LocalString.GetString('沿虚线剪碎喜字 %s/%s'),index,totalNum)
  else
    self.TipTextNode.text = SafeStringFormat3(LocalString.GetString('沿虚线剪碎礼带 %s/%s'),index,totalNum)
  end

  if index >= totalNum then
    self.m_Tick = RegisterTick(function()
      --succ
      self:DoSucc()
    end,moveTime)
  else
    self:EndGuide()
    self.m_Tick = RegisterTick(function()
      self:InitCutLine(index + 1)
    end,moveTime)
  end
end

function LuaOffWorldPlay2Game1Wnd:JudgeSlice(node1,node2,index,lineNode)
  if (node1.name == 'click1' and node2.name == 'click2') or (node1.name == 'click2' and node2.name == 'click1') then
    lineNode:SetActive(false)
    self:PlayMoveAni(index)
  end
end

function LuaOffWorldPlay2Game1Wnd:InitCutLine(index)
  local lineNode = nil
  for i,v in ipairs(self.cutLineTable) do
    if i == index then
      v:SetActive(true)
      lineNode = v
    else
      v:SetActive(false)
    end
  end

  --local

	local onDrag = function(go,delta)
		local currentPos = UICamera.currentTouch.pos

		local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
		local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

		for i =0,hits.Length - 1 do
			if hits[i].collider.gameObject.transform.parent then
				self:JudgeSlice(go,hits[i].collider.gameObject,index,lineNode)
			end
		end
	end

	CommonDefs.AddOnDragListener(lineNode.transform:Find('click1').gameObject,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(lineNode.transform:Find('click2').gameObject,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
end


function LuaOffWorldPlay2Game1Wnd:StartCut()
  self:InitCutLine(1)
end

function LuaOffWorldPlay2Game1Wnd:InitSliceNode(node)
  --
  local line = node.transform:Find('line').gameObject
  local _node = node.transform:Find('node').gameObject
  local cut = node.transform:Find('cut').gameObject

  self.movePosTable = {}
	local trans = cut.transform
	for i =1, trans.childCount do
		local moveNode = trans:GetChild(i-1)
    table.insert(self.movePosTable,moveNode.localPosition)
	end
  cut:SetActive(false)

  self.moveNodeTable = {}
	local trans = _node.transform
	for i =1, trans.childCount do
		local moveNode = trans:GetChild(i-1).gameObject
    table.insert(self.moveNodeTable,moveNode)
	end

  local totalNum = #self.moveNodeTable-1

  if LuaOffWorldPassMgr.Play2Game1Type == 1 then
    self.TipTextNode.text = SafeStringFormat3(LocalString.GetString('沿虚线剪碎喜字 %s/%s'),0,totalNum)
  else
    self.TipTextNode.text = SafeStringFormat3(LocalString.GetString('沿虚线剪碎礼带 %s/%s'),0,totalNum)
  end

  self.cutLineTable = {}
	local trans = line.transform
	for i =1, trans.childCount do
		local lineNode = trans:GetChild(i-1).gameObject
    lineNode:SetActive(false)
    table.insert(self.cutLineTable,lineNode)
	end

  self:StartCut()
end

function LuaOffWorldPlay2Game1Wnd:Init()
	local onCloseClick = function(go)
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2",LocalString.GetString("确认退出吗?")), DelegateFactory.Action(function ()
      CUIManager.CloseUI(CLuaUIResources.OffWorldPlay2Game1Wnd)
    end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	--
  if LuaOffWorldPassMgr.Play2Game1Type == 1 then
    self.play1:SetActive(true)
    self.play2:SetActive(false)
    self:InitSliceNode(self.play1)
  else
    self.play1:SetActive(false)
    self.play2:SetActive(true)
    self:InitSliceNode(self.play2)
  end

  self:InitGuide()
end

function LuaOffWorldPlay2Game1Wnd:InitGuide()
  if LuaOffWorldPassMgr.Play2Game1Type == 1 then
    self.guideFx = self.play1.transform:Find('guide/fxNode/guideFx'):GetComponent(typeof(CUIFx))
  else
    self.guideFx = self.play2.transform:Find('guide/fxNode/guideFx'):GetComponent(typeof(CUIFx))
  end
  self.guideFx.gameObject:SetActive(true)
  self.guideFx:LoadFx('fx/ui/prefab/UI_caidiezhen_jiandao.prefab')
end

function LuaOffWorldPlay2Game1Wnd:EndGuide()
  if self.guideFx then
    self.guideFx.gameObject:SetActive(false)
  end
end

function LuaOffWorldPlay2Game1Wnd:OnDestroy()
  if self.m_Tick then
    UnRegisterTick(self.m_Tick)
    self.m_Tick = nil
  end
end
