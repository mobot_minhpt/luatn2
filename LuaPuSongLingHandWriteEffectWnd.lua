local UIPanel = import "UIPanel"
local CUIFx = import "L10.UI.CUIFx"

LuaPuSongLingHandWriteEffectWnd = class()

RegistChildComponent(LuaPuSongLingHandWriteEffectWnd, "Anchor", UIPanel)
RegistChildComponent(LuaPuSongLingHandWriteEffectWnd, "TextFx", CUIFx)


RegistClassMember(LuaPuSongLingHandWriteEffectWnd, "m_ShowFxTick")
RegistClassMember(LuaPuSongLingHandWriteEffectWnd, "m_CloseWndTick")

function LuaPuSongLingHandWriteEffectWnd:Init()
	self.Anchor.alpha = 0
	self.TextFx:DestroyFx()

	self:DestroyShowFxTick()
	self.m_ShowFxTick = RegisterTickOnce(function()
		self.TextFx:LoadFx("Fx/UI/Prefab/UI_pusongling_xiezi.prefab")
    end, 1500)

	self.m_CloseWndTick = RegisterTickOnce(function ()
		Gac2Gas.FinishEventTask(LuaZhuJueJuQingMgr.m_PuSongLingHandWriteTaskId, "PuSongLingHandWriteEffect")
		CUIManager.CloseUI(CLuaUIResources.PuSongLingHandWriteEffectWnd)
	end, 9000)
end


function LuaPuSongLingHandWriteEffectWnd:OnDisable()
	self:DestroyShowFxTick()
end

function LuaPuSongLingHandWriteEffectWnd:DestroyShowFxTick()
	if self.m_ShowFxTick then
		UnRegisterTick(self.m_ShowFxTick)
		self.m_ShowFxTick = nil
	end
end

function LuaPuSongLingHandWriteEffectWnd:DestroyCloseWndTick()
	if self.m_CloseWndTick then
		UnRegisterTick(self.m_CloseWndTick)
		self.m_CloseWndTick = nil
	end
end

return LuaPuSongLingHandWriteEffectWnd