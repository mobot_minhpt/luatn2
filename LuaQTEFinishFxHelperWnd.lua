local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local UnityEngine = import "UnityEngine"
local CUIFx = import "L10.UI.CUIFx"
local SoundManager = import "SoundManager"
local PlayerSettings = import "L10.Game.PlayerSettings"

LuaQTEFinishFxHelperWnd = class()
RegistClassMember(LuaQTEFinishFxHelperWnd, "Root")
RegistClassMember(LuaQTEFinishFxHelperWnd, "SucFxTemplate")
RegistClassMember(LuaQTEFinishFxHelperWnd, "FailFxTemplate")

RegistClassMember(LuaQTEFinishFxHelperWnd, "ListLimit")
RegistClassMember(LuaQTEFinishFxHelperWnd, "FxTimeSpan")
RegistClassMember(LuaQTEFinishFxHelperWnd, "SucList")
RegistClassMember(LuaQTEFinishFxHelperWnd, "FailList") -- {go,  endTime}
RegistClassMember(LuaQTEFinishFxHelperWnd, "FxTemplate")

function LuaQTEFinishFxHelperWnd:Awake()
    self.Root = self.transform:Find("Root").gameObject
    self.FxTemplate = self.transform:Find("FxTemplate").gameObject

    self.ListLimit = 10
    self.FxTimeSpan = 1 -- 如果修改了特效要注意是否需要修改TimeSpan
end

function LuaQTEFinishFxHelperWnd:Start()
    self.FxTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.Root.transform)

    self.SucList = {}
    self.FailList = {}
end

function LuaQTEFinishFxHelperWnd:TriggerSucFx(position)
    local foundIndex = -1
    local earliestIndex = -1
    local earliest = #self.SucList > 0 and self.SucList[1].endTime
    local curTime = UnityEngine.Time.time

    for i,v in ipairs(self.SucList) do -- 找已经结束的特效
        if v.endTime <= curTime then
            foundIndex = i
            break
        end
        earliest = math.min(earliest, v.endTime)
        if v.endTime < earliest then
            earliest = v.endTime
            earliestIndex = i
        end
    end

    if foundIndex == -1 then
        if #self.SucList < self.ListLimit then --没超上限就新增一个
            local go = UICommonDef.AddChild(self.Root, self.FxTemplate)
            go:SetActive(true)
            table.insert(self.SucList,{go = go, endTime = -1})
            foundIndex = #self.SucList
        elseif earliestIndex ~= -1 then --满了就重置最先开始的特效
            foundIndex = earliestIndex
        end
    end

    if foundIndex ~= -1 then
        local go = self.SucList[foundIndex].go
        local Fx = go:GetComponent(typeof(CUIFx))

        go.transform.localPosition = position

        local sucessFx = LuaQTEMgr.CurrentQTEInfo.SuccessFx or CLuaUIFxPaths.QTESuccessFx
        if self.SucList[foundIndex].endTime == -1 then
            Fx:LoadFx(sucessFx)
            if PlayerSettings.SoundEnabled then
                SoundManager.Inst:PlaySound(SoundManager.UISoundEvents.MissionDone, Vector3.zero, 1.0, nil, 0)
            end
        else
            go:SetActive(false)
            Fx:LoadFx(sucessFx)
            if PlayerSettings.SoundEnabled then
                SoundManager.Inst:PlaySound(SoundManager.UISoundEvents.MissionDone, Vector3.zero, 1.0, nil, 0)
            end
        end

        self.SucList[foundIndex].endTime = curTime + self.FxTimeSpan
    end
end

function LuaQTEFinishFxHelperWnd:TriggerFailFx(position)
    local foundIndex = -1
    local earliestIndex = -1
    local earliest = #self.FailList > 0 and self.FailList[1].endTime
    local curTime = UnityEngine.Time.time

    for i,v in ipairs(self.FailList) do -- 找已经结束的特效
        if v.endTime <= curTime then
            foundIndex = i
            break
        end
        earliest = math.min(earliest, v.endTime)
        if v.endTime < earliest then
            earliest = v.endTime
            earliestIndex = i
        end
    end

    if foundIndex == -1 then
        if #self.FailList < self.ListLimit then --没超上限就新增一个
            local go = UICommonDef.AddChild(self.Root, self.FxTemplate)
            go:SetActive(true)
            table.insert(self.FailList,{go = go, endTime = -1})
            foundIndex = #self.FailList
        elseif earliestIndex ~= -1 then --满了就重置最先开始的特效
            foundIndex = earliestIndex
        end
    end

    if foundIndex ~= -1 then
        local go = self.FailList[foundIndex].go
        local Fx = go:GetComponent(typeof(CUIFx))

        go.transform.localPosition = position

        local failFx = LuaQTEMgr.CurrentQTEInfo.FailedFx or CLuaUIFxPaths.QTEFailFx
        if self.FailList[foundIndex].endTime == -1 then
            Fx:LoadFx(failFx)
        else
            go:SetActive(false)
            Fx:LoadFx(failFx)
        end

        self.FailList[foundIndex].endTime = curTime + self.FxTimeSpan
    end
end
