require("common/doudizhu/doudizhu")


EnumDouDiZhuType = {
	eUnknown = 0,
	eGuildFightPre = 1,
	eGuildFightFinal = 2,
	eGuildPlay = 3,
	eJiaYuanPlay = 4,
	eCrossPlay = 5,
	eRemoteJiaYuanPlay = 6,
	eRemoteQiangXiangZi = 7,
}

EnumGuildDouDiZhuFightStage = {
	eSignUp = 1,
	eConfirmPlayer = 2,
	ePrePlay = 3,
	eFinalPlay = 4,
	eEnd = 5,
}


CLuaDouDiZhuMgr={}
CLuaDouDiZhuMgr.s_GameData = nil

CLuaDouDiZhuMgr.m_OwnerId=0
CLuaDouDiZhuMgr.m_IsRemote=true
CLuaDouDiZhuMgr.m_PlayerIndex=0
CLuaDouDiZhuMgr.m_IsWatch=false
CLuaDouDiZhuMgr.m_Play=nil

CLuaDouDiZhuMgr.m_Type=0--帮会还是家园斗地主
CLuaDouDiZhuMgr.m_CardTemplate=nil
CLuaDouDiZhuMgr.m_RankDataList=nil
CLuaDouDiZhuMgr.m_FinalBroadcast=false
CLuaDouDiZhuMgr.m_MainPlayerRankData=nil
CLuaDouDiZhuMgr.m_DouDiZhuStage=0
CLuaDouDiZhuMgr.m_DouDiZhuRound=0
CLuaDouDiZhuMgr.m_ChampionRecord=nil

CLuaDouDiZhuMgr.m_Result=nil

--CLuaDouDiZhuMgr.m_CanChoose=false
--CLuaDouDiZhuMgr.m_Choosed=false
CLuaDouDiZhuMgr.m_CanSignUp=false
CLuaDouDiZhuMgr.m_SignUp=false
CLuaDouDiZhuMgr.m_ShowFight=false
CLuaDouDiZhuMgr.m_PraiseNum=0
CLuaDouDiZhuMgr.m_FightNum=0

CLuaDouDiZhuMgr.m_ChatClickWorldPos=nil
CLuaDouDiZhuMgr.m_QuickChatContext=nil

CLuaDouDiZhuMgr.m_ResetTick=nil
CLuaDouDiZhuMgr.m_ShowResultTick=nil

CLuaDouDiZhuMgr.m_Score=0
CLuaDouDiZhuMgr.m_Power=0
CLuaDouDiZhuMgr.m_DelayShowResult=3500
CLuaDouDiZhuMgr.m_Round=0
CLuaDouDiZhuMgr.m_RoundName=""

CLuaDouDiZhuMgr.m_IsJiaoDiZhu=false
CLuaDouDiZhuMgr.m_LeftTime=0
CLuaDouDiZhuMgr.m_Tick=nil


CLuaDouDiZhuMgr.m_WatchPlayers=nil

CLuaDouDiZhuMgr.m_ShowRoundLeftTime=false
CLuaDouDiZhuMgr.m_RoundLeftTime=0
CLuaDouDiZhuMgr.m_RoundTime=0

function CLuaDouDiZhuMgr.SwitchViewChairId(chairId)--3
	local chairCount=3
	local dt=chairId-CLuaDouDiZhuMgr.m_PlayerIndex
	if dt<0 then 
		dt=dt+chairCount 
	end
	local ret= dt%chairCount
	return ret+1
end
function CLuaDouDiZhuMgr.SwitchChairId(viewChairId)--3
	local chairCount=3
	local dt=viewChairId-CLuaDouDiZhuMgr.SwitchViewChairId(CLuaDouDiZhuMgr.m_PlayerIndex)
	if dt<0 then 
		dt=dt+chairCount 
	end
	local ret= dt%chairCount
	return ret+1
end



function CLuaDouDiZhuMgr.SetCard(tf,card,index,showback)
	local label=FindChild(tf,"Label"):GetComponent(typeof(UILabel))
	local sprite1=FindChild(tf,"Sprite"):GetComponent(typeof(UISprite))
	local sprite2=FindChild(tf,"Sprite2"):GetComponent(typeof(UISprite))
	local sprite3=FindChild(tf,"Sprite3"):GetComponent(typeof(UIWidget))
	local sprite4=FindChild(tf,"Sprite4"):GetComponent(typeof(UIWidget))
	local back=FindChild(tf,"Back"):GetComponent(typeof(UIWidget))

	local bg=FindChild(tf,"Node"):GetComponent(typeof(UISprite))

	bg.depth=index*2
	label.depth=index*2+1
	sprite1.depth=index*2+1
	sprite2.depth=index*2+1
	sprite3.depth=index*2+1
	sprite4.depth=index*2+1

	if card==0 or showback then
		label.gameObject:SetActive(false)
		sprite1.gameObject:SetActive(false)
		sprite2.gameObject:SetActive(false)
		sprite3.gameObject:SetActive(false)
		sprite4.gameObject:SetActive(false)
		back.depth = bg.depth+1
		bg.enabled = false
		back.gameObject:SetActive(true)
		-- bg.spriteName="doudizhu_paimian_02"
		return
	end
	back.gameObject:SetActive(false)
	bg.enabled = true

	local val=math.floor(card/100)
	local type=card%100

	local black=Color(0,0,0,1)
	local red=Color(1,0,0,1)


	if val==100 or val==200 then
		label.gameObject:SetActive(false)
		sprite1.gameObject:SetActive(false)
		sprite2.gameObject:SetActive(false)
		sprite3.gameObject:SetActive(true)
		sprite4.gameObject:SetActive(true)

		if val==100 then
			CUICommonDef.SetActive(sprite3.gameObject,false,true)
			sprite4.color=black
		else
			CUICommonDef.SetActive(sprite3.gameObject,true,true)
			sprite4.color=red
		end
	else
		label.gameObject:SetActive(true)
		sprite1.gameObject:SetActive(true)
		sprite2.gameObject:SetActive(true)
		sprite3.gameObject:SetActive(false)
		sprite4.gameObject:SetActive(false)

		if val==11 then
			label.text="J"
		elseif val==12 then
			label.text="Q"
		elseif val==13 then
			label.text="K"
		elseif val==14 then
			label.text="A"
		elseif val==20 then
			label.text="2"
		else
			label.text=tostring(val)
		end

		if type==1 or type==4 then
			label.color=black
		else
			label.color=red
		end
		if type==1 then
			--黑桃
			sprite1.spriteName="doudizhu_heitao"
			sprite2.spriteName="doudizhu_heitao"
			
		elseif type==2 then
			--红桃
			sprite1.spriteName="doudizhu_hongtao"
			sprite2.spriteName="doudizhu_hongtao"
			
		elseif type==3 then
			--方块
			sprite1.spriteName="doudizhu_fangpian"
			sprite2.spriteName="doudizhu_fangpian"
		elseif type==4 then
			--梅花
			sprite1.spriteName="doudizhu_meihua"
			sprite2.spriteName="doudizhu_meihua"
		end
	end
end

function CLuaDouDiZhuMgr.IsCrossGM()
	return CLuaDouDiZhuMgr.m_IsWatch and CLuaDouDiZhuMgr.m_Type==EnumDouDiZhuType.eCrossPlay
end



function CLuaDouDiZhuMgr.UpdateTick(leftTime)
	if CLuaDouDiZhuMgr.m_Tick then
		UnRegisterTick(CLuaDouDiZhuMgr.m_Tick)
		CLuaDouDiZhuMgr.m_Tick = nil
	end
	CLuaDouDiZhuMgr.m_LeftTime=leftTime

	CLuaDouDiZhuMgr.m_Tick = RegisterTickWithDuration(function ()
		CLuaDouDiZhuMgr.m_LeftTime = CLuaDouDiZhuMgr.m_LeftTime-1
		if CLuaDouDiZhuMgr.m_LeftTime<=0 then
			CLuaDouDiZhuMgr.m_LeftTime = 0
			UnRegisterTick(CLuaDouDiZhuMgr.m_Tick)
			CLuaDouDiZhuMgr.m_Tick=nil
		end
	end,1000,(leftTime)*1000)
end
function CLuaDouDiZhuMgr.CancleTick()
	if CLuaDouDiZhuMgr.m_Tick then
		UnRegisterTick(CLuaDouDiZhuMgr.m_Tick)
		CLuaDouDiZhuMgr.m_Tick = nil
	end
	CLuaDouDiZhuMgr.m_LeftTime=0
end


--等于nil表示没有赢家
CLuaDouDiZhuMgr.m_IsWinner = nil


CLuaDouDiZhuMgr.m_AutoContinueTick = nil
CLuaDouDiZhuMgr.m_AutoContinueLeftTime = 0

function CLuaDouDiZhuMgr.CancleAutoContinueCountdown()
	CLuaDouDiZhuMgr.m_Result = nil
	CLuaDouDiZhuMgr.m_AutoContinueLeftTime = 0
	if CLuaDouDiZhuMgr.m_AutoContinueTick then
		UnRegisterTick(CLuaDouDiZhuMgr.m_AutoContinueTick)
		CLuaDouDiZhuMgr.m_AutoContinueTick = nil
	end
end
function CLuaDouDiZhuMgr.StartContinueCountdown(leftTime)
	if CLuaDouDiZhuMgr.m_AutoContinueTick then
		UnRegisterTick(CLuaDouDiZhuMgr.m_AutoContinueTick)
		CLuaDouDiZhuMgr.m_AutoContinueTick = nil
	end
	CLuaDouDiZhuMgr.m_AutoContinueLeftTime=leftTime

	CLuaDouDiZhuMgr.m_AutoContinueTick = RegisterTickWithDuration(function ()
		CLuaDouDiZhuMgr.m_AutoContinueLeftTime = CLuaDouDiZhuMgr.m_AutoContinueLeftTime-1
		if CLuaDouDiZhuMgr.m_AutoContinueLeftTime<=0 then
			CLuaDouDiZhuMgr.m_AutoContinueLeftTime = 0
			UnRegisterTick(CLuaDouDiZhuMgr.m_AutoContinueTick)
			CLuaDouDiZhuMgr.m_AutoContinueTick=nil
		end
	end,1000,(leftTime)*1000)
end



local Ease=import "DG.Tweening.Ease"
local UIGrid=import "UIGrid"
CLuaDouDiZhuCardMgr={}
CLuaDouDiZhuCardMgr.m_SelectedCardList={}
CLuaDouDiZhuCardMgr.m_AllCards={}--dic
CLuaDouDiZhuCardMgr.m_CardsLookup={}--dic
CLuaDouDiZhuCardMgr.m_ShowCardsTf=nil
CLuaDouDiZhuCardMgr.m_HandCardsTf=nil

function CLuaDouDiZhuCardMgr.Init()
    CLuaDouDiZhuCardMgr.m_SelectedCardList={}
    CLuaDouDiZhuCardMgr.m_AllCards={}--dic
    CLuaDouDiZhuCardMgr.m_CardsLookup={}
end

--出牌动画
function CLuaDouDiZhuCardMgr.CardAppear(tf)
    LuaTweenUtils.DOKill(tf,false)
    tf.localScale=Vector3(2,2,1)
    LuaTweenUtils.TweenAlpha(tf,0,1,0.5)
    LuaTweenUtils.TweenScaleXY(tf,1,1,0.5,Ease.OutBack)
end
--viewIndex是2/3的时候，要考虑2排的情况
function CLuaDouDiZhuCardMgr.GetCardLocalPosition(tf,playerViewIndex,cardIndex,cardNum)
    -- local grid=tf.parent:GetComponent(typeof(UIGrid))

	-- local maxPerLine=grid.maxPerLine
	-- local x=cardIndex-1
	-- local y=0
	-- if maxPerLine~=0 then
	local	x=(cardIndex-1)%10
	local	y=math.floor((cardIndex-1)/10)
	-- end
	if playerViewIndex==2 then
		if cardIndex>10 then
			local temp=cardNum%10
			x=temp-1-(cardIndex-1)%10
		else
			x=math.min(cardNum,10)-cardIndex
		end
		-- print("pos",x,y,cardIndex,cardNum)
		return -70*x,-130*y
	else
		return 70*x,-130*y
	end
end


function CLuaDouDiZhuCardMgr.FaDiPai(handCards)
	local tempCards={}
	for i,v in ipairs(handCards) do
		tempCards[v]=false
	end

	local curHandCards={}
	for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
		table.insert( curHandCards,k )
		tempCards[k.m_Card]=true
	end

	--新增加的3张牌
	local addCards={}
	for k,v in pairs(tempCards) do
		if not v then
			table.insert(addCards,k)
		end
	end

	local newCards={}
	for i=#handCards,1,-1 do
		table.insert( newCards,handCards[i] )
	end
	local newCardsIndexLookup={}
	for i,v in ipairs(newCards) do
		newCardsIndexLookup[v]=i
	end
	local cardNum=#handCards
	for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
		-- table.insert( curHandCards,k )
		if v then
			k.m_Index=newCardsIndexLookup[k.m_Card]
			k.m_CardNum=cardNum
			k:MoveToNewPos()
			CLuaDouDiZhuMgr.SetCard(k.m_Transform,k.m_Card,k.m_Index,CLuaDouDiZhuMgr.IsCrossGM())
		end
	end

	--创建底牌 并创建到指定的位置
	local node=CLuaDouDiZhuCardMgr.m_HandCardsTf.gameObject
	for i,v in ipairs(addCards) do
		-- print("addcard",v)
		local card = NGUITools.AddChild(node,CLuaDouDiZhuMgr.m_CardTemplate)
		card:SetActive(true)
		local cardCmp=CLuaDouDiZhuCard:new()
		cardCmp:Init(card.transform,v,newCardsIndexLookup[v],cardNum,CLuaDouDiZhuMgr.IsCrossGM())
		cardCmp:Register()
		cardCmp:PlayGetDiPaiEffect()
	end
end

function CLuaDouDiZhuCardMgr.ChuPai(handCards,showCards)
    --确定哪些牌需要移动
    local cards1={}
    local cards2={}

    local showCardsDic={}
    for i,v in ipairs(showCards) do
        showCardsDic[v]=true
        -- print(v)
    end
    --node对应CLuaDouDiZhuCard
    for k,v in pairs(CLuaDouDiZhuCardMgr.m_AllCards) do
        -- print(k,v)
        if v then
            if showCardsDic[k.m_Card] then
                --如果在出的牌里面
                table.insert( cards2,k )
                CLuaDouDiZhuCardMgr.m_AllCards[k]=nil
            else
                table.insert( cards1,k )
            end
        end
    end

    table.sort(cards1,function(a,b)
        return a.m_Card>b.m_Card
    end)

    --更新序号
    CLuaDouDiZhuCardMgr.m_AllCards={}
    local cardNum=#cards1
    for i,v in ipairs(cards1) do
        v.m_Index=i
        v.m_CardNum=cardNum
        v:MoveToNewPos()
        -- v:Init(v.m_Transform,v.m_Card,v.m_Index,cardNum)
        CLuaDouDiZhuMgr.SetCard(v.m_Transform,v.m_Card,v.m_Index,CLuaDouDiZhuMgr.IsCrossGM())
        CLuaDouDiZhuCardMgr.m_AllCards[v]=true
    end

    for i,v in ipairs(cards2) do
        v:Disappear()
    end

	if CLuaDouDiZhuCardMgr.m_ShowCardsTf then
		CUICommonDef.ClearTransform(CLuaDouDiZhuCardMgr.m_ShowCardsTf)
        local node=CLuaDouDiZhuCardMgr.m_ShowCardsTf.gameObject

        local tbl={}
        for i=#showCards,1,-1 do
            table.insert( tbl,showCards[i] )
        end
		local cardTbl={}
        for i,v in ipairs(tbl) do
            local card = NGUITools.AddChild(node,CLuaDouDiZhuMgr.m_CardTemplate)
            card:SetActive(true)
			CLuaDouDiZhuMgr.SetCard(card.transform,v,i)
			table.insert( cardTbl,card )
		end
		local grid=node:GetComponent(typeof(UIGrid))
		grid:Reposition()
		for i,v in ipairs(cardTbl) do
            CLuaDouDiZhuCardMgr.CardAppear(v.transform)
		end
    end

end
CLuaDouDiZhuMgr.m_ApplyJoinRemoteDouDiZhuPlayerListAlert=false
function CLuaDouDiZhuMgr.UpdateRemoteDouDiZhuApplyAlert(state)
	CLuaDouDiZhuMgr.m_ApplyJoinRemoteDouDiZhuPlayerListAlert = state
	g_ScriptEvent:BroadcastInLua("ApplyJoinRemoteDouDiZhuPlayerListAlert")
end

--把斗地主快捷聊天抽出一个方法进行复用，根据策划的要求，师徒五子棋和斗地主公用一份快捷聊天数据，后面看情况是否要拆分
function CLuaDouDiZhuMgr:ShowQuickChatWnd(worldPos, context)
	self.m_ChatClickWorldPos = worldPos and worldPos or Vector3.zero
	self.m_QuickChatContext = context
	CUIManager.ShowUI(CUIResources.DouDiZhuQuickChatWnd)
end
--index下标从1开始
function CLuaDouDiZhuMgr:OnQuickMsgClick(index)
	if self.m_QuickChatContext==nil or self.m_QuickChatContext=="" then
		Gac2Gas.RequestSendDouDiZhuShortMessage(index)
	elseif self.m_QuickChatContext=="doudizhu" then
		Gac2Gas.RequestSendDouDiZhuShortMessage(index)
	elseif self.m_QuickChatContext=="shituwuziqi" then
		local msg=DouDiZhu_ShortMessage.GetData(index).Message
		LuaShiTuMgr:RequestChatInWuZiQi(msg)
	end
end
