local CUITexture = import "L10.UI.CUITexture"
local UIWidget=import "UIWidget"
-- 改写自LuaOffWorldPlay2TipWnd，基本逻辑保持不变，为了方便通用专门写了一个
LuaCommonImageRuleWnd =class()
RegistClassMember(LuaCommonImageRuleWnd,"m_ImageTemplate")
RegistClassMember(LuaCommonImageRuleWnd,"m_ImageRoot")
RegistClassMember(LuaCommonImageRuleWnd,"m_IndicatorTemplate")
RegistClassMember(LuaCommonImageRuleWnd,"m_IndicatorGrid")

RegistClassMember(LuaCommonImageRuleWnd,"m_PicList")
RegistClassMember(LuaCommonImageRuleWnd,"m_MessageList")
RegistClassMember(LuaCommonImageRuleWnd,"m_Angles")
RegistClassMember(LuaCommonImageRuleWnd,"m_DeltaDegree")
RegistClassMember(LuaCommonImageRuleWnd,"m_CurrentSelectedIndex")
RegistClassMember(LuaCommonImageRuleWnd,"m_BgMask")

function LuaCommonImageRuleWnd:Awake()
    self.m_ImageTemplate = self.transform:Find("Anchor/Template").gameObject
    self.m_ImageRoot = self.transform:Find("Anchor/ImageRoot")
    self.m_IndicatorTemplate = self.transform:Find("Anchor/IndicatorTemplate").gameObject
    self.m_IndicatorGrid = self.transform:Find("Anchor/IndicatorGrid"):GetComponent(typeof(UIGrid))
    
    self.m_ImageTemplate:SetActive(false)
    self.m_IndicatorTemplate:SetActive(false)
end

function LuaCommonImageRuleWnd:Init()
    local imageRuleInfo = LuaImageRuleMgr.m_ImageRuleInfo
    self.m_PicList = imageRuleInfo.imagePaths
    self.m_MessageList = imageRuleInfo.msgs

	Extensions.RemoveAllChildren(self.m_ImageRoot.transform)
    for i,v in ipairs(self.m_PicList) do
        local go = CUICommonDef.AddChild(self.m_ImageRoot.gameObject ,self.m_ImageTemplate)
        go:SetActive(true)
        local texture = go:GetComponent(typeof(CUITexture))
        texture:LoadMaterial(v)

        local ruleText = go.transform:Find("Border/Label"):GetComponent(typeof(UILabel))
        ruleText.text = self.m_MessageList[i]
        if System.String.IsNullOrEmpty(self.m_MessageList[i]) then
            ruleText.gameObject:SetActive(false)
        else
            ruleText.gameObject:SetActive(true)
        end

        UIEventListener.Get(go).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
            self:OnDrag(g,delta)
        end)
        UIEventListener.Get(go).onDragEnd = DelegateFactory.VoidDelegate(function(g)
            self:OnDragEnd(g)
        end)
        UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function(g)
            self:OnDragStart(g)
        end)
    end
    self.m_Angles={}
    self.m_DeltaDegree = math.floor(360/#self.m_PicList)
    for i,v in ipairs(self.m_PicList) do
        self.m_Angles[i] = (i-1)*self.m_DeltaDegree
    end

    self.m_CurrentSelectedIndex = 1
    self:UpdatePosition()

    Extensions.RemoveAllChildren(self.m_IndicatorGrid.transform)
    for i,v in ipairs(self.m_PicList) do
        local go = CUICommonDef.AddChild(self.m_IndicatorGrid.gameObject,self.m_IndicatorTemplate)
        go:SetActive(true)
    end
    self.m_IndicatorGrid:Reposition()

    self:UpdateIndicator(self.m_CurrentSelectedIndex)

    local lastArrow = self.transform:Find("Anchor/LastArrow").gameObject
    local nextArrow = self.transform:Find("Anchor/NextArrow").gameObject
    lastArrow:SetActive(#self.m_PicList > 1)
    nextArrow:SetActive(#self.m_PicList > 1)
    UIEventListener.Get(lastArrow).onClick = DelegateFactory.VoidDelegate(function(g)
      self:UpdatePage(-1)
    end)
    UIEventListener.Get(nextArrow).onClick = DelegateFactory.VoidDelegate(function(g)
      self:UpdatePage(1)
    end)

    self.m_BgMask = self.transform:Find("_BgMask_")
    if self.m_BgMask then
        self.m_BgMask = self.m_BgMask:GetComponent(typeof(UITexture))
        if LuaTangYuan2022Mgr.IsInPlay() then 
            self.m_BgMask.alpha = 0.5
            UIEventListener.Get(self.m_BgMask.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
                CUIManager.CloseUI(CLuaUIResources.CommonImageRuleWnd)
            end)
        else
            self.m_BgMask.alpha = 1
            UIEventListener.Get(self.m_BgMask.gameObject).onClick = nil
        end
    end
end


function LuaCommonImageRuleWnd:UpdateIndicator(index)
    for i=1,self.m_IndicatorGrid.transform.childCount do
        local sprite = self.m_IndicatorGrid.transform:GetChild(i-1):GetComponent(typeof(UISprite))
        sprite.color= i==index and Color.white or Color(0.3,0.5,0.8)
    end
end

function LuaCommonImageRuleWnd:OnDrag(go,delta)
    local moveDeg = 57.2958*math.asin(math.max(-1,math.min(delta.x/400,1)))
    for i,v in ipairs(self.m_Angles) do
        local angle = v + moveDeg
        self.m_Angles[i] = angle - math.floor(angle/360)*360
    end

    self:UpdatePosition()
end
function LuaCommonImageRuleWnd:OnDragStart(go)
    for i,v in ipairs(self.m_Angles) do
        local child = self.m_ImageRoot:GetChild(i-1)
        LuaTweenUtils.DOKill(child,false)
    end
end

function LuaCommonImageRuleWnd:UpdatePage(offset)
    self.m_CurrentSelectedIndex =self.m_CurrentSelectedIndex + offset

    if self.m_CurrentSelectedIndex<1 then
        self.m_CurrentSelectedIndex=self.m_CurrentSelectedIndex+#self.m_Angles
    end
    if self.m_CurrentSelectedIndex>#self.m_Angles then
        self.m_CurrentSelectedIndex=self.m_CurrentSelectedIndex-#self.m_Angles
    end

    for i,v in ipairs(self.m_Angles) do
        local degree = (i-self.m_CurrentSelectedIndex)*self.m_DeltaDegree
        degree = degree - math.floor(degree/360)*360

        local moveTo =  math.sin(0.0174533*degree)*250
        local scale = math.cos(0.0174533*degree)*0.2+0.8
        scale=math.max(0,math.min(1,scale))

        local child = self.m_ImageRoot:GetChild(i-1)
        LuaTweenUtils.TweenPositionX(child,moveTo,0.3)
        LuaTweenUtils.TweenScaleTo(child, Vector3(scale,scale,1),0.3)
        self:UpdateDepthAndAlpha(child, scale)
        self.m_Angles[i]=degree
    end

    self:UpdateIndicator(self.m_CurrentSelectedIndex)
end

function LuaCommonImageRuleWnd:OnDragEnd(go)
    local selectIndex=0

    local index=go.transform:GetSiblingIndex()+1
    local x = go.transform.localPosition.x
    local scale = go.transform.localScale.x
    --看划过的距离
    if x>-50 and x<50 and scale>0.7 then
        selectIndex=index
    elseif x<0 then
        selectIndex=index+1
    elseif x>0 then
        selectIndex=index-1
    end

    if selectIndex<1 then selectIndex=selectIndex+#self.m_Angles end
    if selectIndex>#self.m_Angles then selectIndex=selectIndex-#self.m_Angles end


    for i,v in ipairs(self.m_Angles) do
        local degree = (i-selectIndex)*self.m_DeltaDegree
        degree = degree - math.floor(degree/360)*360

        local moveTo =  math.sin(0.0174533*degree)*250
        local scale = math.cos(0.0174533*degree)*0.2+0.8
        scale=math.max(0,math.min(1,scale))

        local child = self.m_ImageRoot:GetChild(i-1)
        LuaTweenUtils.TweenPositionX(child,moveTo,0.3)
        LuaTweenUtils.TweenScaleTo(child, Vector3(scale,scale,1),0.3)
        self:UpdateDepthAndAlpha(child, scale)
        self.m_Angles[i]=degree
    end

    self:UpdateIndicator(selectIndex)
end

function LuaCommonImageRuleWnd:UpdatePosition()
    local childCount = self.m_ImageRoot.childCount
    for i=0, childCount-1 do
        local child =  self.m_ImageRoot:GetChild(i)
        local angle=self.m_Angles[i+1]
        angle = angle - math.floor(angle/360)*360

        local offset = math.sin(0.0174533*angle)*250
        LuaUtils.SetLocalPositionX(child,offset)

        local scale = math.cos(0.0174533*angle)*0.2+0.8
        LuaUtils.SetLocalScale(child,scale,scale,1)

        self:UpdateDepthAndAlpha(child, scale)
    end
end

function LuaCommonImageRuleWnd:UpdateDepthAndAlpha(transRoot, scale)
    local _depth = math.floor(scale*100)*2
    local _alpha = scale*scale>0.95 and 1 or scale*scale*scale*scale

    local uitexture = transRoot:GetComponent(typeof(UIWidget))
    local label = transRoot:Find("Border/Label"):GetComponent(typeof(UIWidget))
    local labelBg = transRoot:Find("Border/Label/Bg"):GetComponent(typeof(UIWidget))
    local border = transRoot:Find("Border"):GetComponent(typeof(UIWidget))
    uitexture.depth = _depth
    label.depth = _depth + 2
    uitexture.alpha= _alpha
    label.alpha = _alpha
    border.depth = uitexture.depth-1
    labelBg.depth = uitexture.depth+1
end