local UITable = import "UITable"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
LuaShuangshiyi2023LotteryOverviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023LotteryOverviewWnd, "Table", "Table", UITable)
RegistChildComponent(LuaShuangshiyi2023LotteryOverviewWnd, "ItemObj", "ItemObj", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryOverviewWnd, "OverviewText", "OverviewText", UILabel)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023LotteryOverviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
end

function LuaShuangshiyi2023LotteryOverviewWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023LotteryOverviewWnd:InitWndData()
    self.overviewItemData = {}
    self.maxQuality = 0
    Double11_RechargeLotteryItems.Foreach(function (cfgId, data)
        local quality = LuaShuangshiyi2023Mgr:GetItemQuality(data.ItemId)
        if quality > self.maxQuality then
            self.maxQuality = quality
        end
        if self.overviewItemData[quality] == nil then
            self.overviewItemData[quality] = {}
        end
        table.insert(self.overviewItemData[quality], cfgId)
    end)
end

function LuaShuangshiyi2023LotteryOverviewWnd:RefreshConstUI()
    self.OverviewText.text = g_MessageMgr:FormatMessage("Double11_2023Lottery_Overview")
    
    self.ItemObj:SetActive(false)
    --refresh itemList
    for quality = self.maxQuality, 1, -1 do
        local itemList = self.overviewItemData[quality] and self.overviewItemData[quality] or {}
        for i = 1, #itemList do
            local cfgId = itemList[i]
            local cfgData = Double11_RechargeLotteryItems.GetData(cfgId)
            local itemId = cfgData.ItemId
            local itemData = Item_Item.GetData(itemId)
            
            local templateItem = CommonDefs.Object_Instantiate(self.ItemObj)
            templateItem:SetActive(true)
            templateItem.transform.parent = self.Table.transform
            
            local texture = templateItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            if itemData then texture:LoadMaterial(itemData.Icon) end
            templateItem.transform:Find("Quality"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
            
            local isBind = (cfgData.Bind == 1) or (itemData.Lock == 1)
            templateItem.transform:Find("BindSprite").gameObject:SetActive(false)
            templateItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = cfgData.Amount > 1 and cfgData.Amount or ""

            UIEventListener.Get(templateItem).onClick = DelegateFactory.VoidDelegate(function (_)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end)
        end
    end
    self.Table:Reposition()
end