local UILabel = import "UILabel"
local CScene = import "L10.Game.CScene"
local DelegateFactory  = import "DelegateFactory"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaCommonCountDownView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonCountDownView, "LeaveBtn", "LeaveBtn", CButton)
RegistChildComponent(LuaCommonCountDownView, "SituationBtn", "SituationBtn", CButton)
RegistChildComponent(LuaCommonCountDownView, "TaskDesc", "TaskDesc", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaCommonCountDownView, "m_TimeStr")
RegistClassMember(LuaCommonCountDownView, "m_OnButton1ClickFunc")
RegistClassMember(LuaCommonCountDownView, "m_OnButton2ClickFunc")
RegistClassMember(LuaCommonCountDownView, "m_DesStr")
RegistClassMember(LuaCommonCountDownView, "m_Sprite")
RegistClassMember(LuaCommonCountDownView, "m_BtnPosDevY")
RegistClassMember(LuaCommonCountDownView, "m_SpriteHeightDevY")
RegistClassMember(LuaCommonCountDownView, "m_OnBgClickFunc")

function LuaCommonCountDownView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick()
	end)


	
	UIEventListener.Get(self.SituationBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSituationBtnClick()
	end)

    self.SituationBtn.gameObject:SetActive(false)
    --@endregion EventBind end
    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackGroudClick()
	end)

    self.m_Sprite = self.gameObject:GetComponent(typeof(UISprite))
	self.m_SpriteHeightDevY = self.m_Sprite.height - self.TaskDesc.height
	self.m_BtnPosDevY = self.LeaveBtn.transform.localPosition.y + self.m_Sprite.height
end

function LuaCommonCountDownView:Init()
    self:InitCountDownView()
end

function LuaCommonCountDownView:InitCountDownView()
    -- 据点战
    if LuaJuDianBattleMgr:IsInJuDian() or LuaJuDianBattleMgr:IsInGameplay() then
        local msg = nil
        if LuaJuDianBattleMgr:IsInJuDian() then
            msg = g_MessageMgr:FormatMessage("GUILD_JUDIAN_TASKBAR")
        end

        self:OnCommonCountDownViewUpdate(msg, true, true, LocalString.GetString("规则"), LocalString.GetString("战况"),
            function(go)
                LuaCommonTextImageRuleMgr:ShowWnd(2)
            end,
            function(go)
                LuaJuDianBattleMgr:OpenInfoWnd(1)
        end)
    end
end

function LuaCommonCountDownView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("OnCommonCountDownViewUpdate", self, "OnCommonCountDownViewUpdate")
    g_ScriptEvent:AddListener("OnCommonCountDownViewUpdate2", self, "OnCommonCountDownViewUpdate2")
    self:OnCommonCountDownViewUpdate2()
end

function LuaCommonCountDownView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("OnCommonCountDownViewUpdate", self, "OnCommonCountDownViewUpdate")
    g_ScriptEvent:RemoveListener("OnCommonCountDownViewUpdate2", self, "OnCommonCountDownViewUpdate2")
    self.m_DesStr = nil
    self.SituationBtn.gameObject:SetActive(false)
    self:UpdateView()
end

function LuaCommonCountDownView:OnSceneRemainTimeUpdate(args)
	local totalSeconds = 0
    if CScene.MainScene then
		totalSeconds = CScene.MainScene.ShowTime
    end
    if totalSeconds>=3600 then
        self.m_TimeStr = SafeStringFormat3("[ACF9FF]%02d:%02d:%02d", math.floor(totalSeconds / 3600),math.floor((totalSeconds % 3600) / 60),  totalSeconds % 60)
    else
        self.m_TimeStr = SafeStringFormat3("[ACF9FF]%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
	
	self:UpdateView()
end
function LuaCommonCountDownView:OnCommonCountDownViewUpdate2()
    local pdata = LuaCommonCountDownViewMgr.Data
    if pdata then
        self:OnCommonCountDownViewUpdate(pdata.text,
            pdata.button1Visible, 
            pdata.button2Visible, 
            pdata.button1Text, 
            pdata.button2Text,
            pdata.onButton1Click,
            pdata.onButton2Click,
            pdata.onBgClick)
    end
end
function LuaCommonCountDownView:OnCommonCountDownViewUpdate(text, button1Visible, button2Visible, button1Text, button2Text, onButton1Click, onButton2Click,onBgClick)
    self.m_DesStr = text
    self.LeaveBtn.gameObject:SetActive(button1Visible and button1Visible or false)
    self.SituationBtn.gameObject:SetActive(button2Visible and button2Visible or false)
    self.LeaveBtn.Text = button1Text~=nil and button1Text or  LocalString.GetString("离开")
    self.SituationBtn.Text = button2Text~=nil and button2Text or LocalString.GetString("战况")
    self.m_OnButton1ClickFunc, self.m_OnButton2ClickFunc = onButton1Click, onButton2Click
    self.m_OnBgClickFunc = onBgClick
    self:UpdateView()
end

function LuaCommonCountDownView:UpdateView()
    if not self.m_TimeStr then self.m_TimeStr = "" end
    if not self.m_DesStr then 
        self.TaskDesc.text = self.m_TimeStr 
    else
        self.TaskDesc.text = SafeStringFormat3(self.m_DesStr, self.m_TimeStr)
    end
    self.m_Sprite.height = self.TaskDesc.height + self.m_SpriteHeightDevY
	Extensions.SetLocalPositionY(self.LeaveBtn.transform,self.m_BtnPosDevY - self.m_Sprite.height)
    Extensions.SetLocalPositionY(self.SituationBtn.transform,self.m_BtnPosDevY - self.m_Sprite.height)
end

--@region UIEvent

function LuaCommonCountDownView:OnLeaveBtnClick()
    if self.m_OnButton1ClickFunc then 
        self.m_OnButton1ClickFunc() 
        return
    end
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaCommonCountDownView:OnSituationBtnClick()
    if self.m_OnButton2ClickFunc then 
        self.m_OnButton2ClickFunc() 
        return
    end
end


--@endregion UIEvent
function LuaCommonCountDownView:OnBackGroudClick()
    if self.m_OnBgClickFunc then
        self.m_OnBgClickFunc()
        return
    end
end

LuaCommonCountDownViewMgr = {}
LuaCommonCountDownViewMgr.m_GamePlayIds = {}
LuaCommonCountDownViewMgr.m_IsInit = false

function LuaCommonCountDownViewMgr:IsShow()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.NeedShowCountDownViewInTaskListBoard==3 then 
        return true
    end
    if not self.m_IsInit then self:Init() end
    if not CScene.MainScene then return false end
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    return self.m_GamePlayIds[gamePlayId]
end

function LuaCommonCountDownViewMgr:Init()
    self.m_GamePlayIds[YuanDan2022_Setting.GetData().FuDanGameplayId] = true
    self.m_GamePlayIds[WinterOlympic_CurlingSetting.GetData().PlayDesignId] = true
    self.m_GamePlayIds[51102651] = true
    self.m_GamePlayIds[QiXi2022_Setting.GetData().MainGameplayID] = true
    self.m_GamePlayIds[QiXi2022_Setting.GetData().Gameplay2ID] = true
    self.m_GamePlayIds[GuildCustomBuildPreTask_GameSetting.GetData().GamePlayId] = true
    self.m_GamePlayIds[WuYi2023_FenYongZhenXian.GetData().GamePlayId] = true
    PengDaoFuYao_Gameplay.ForeachKey(function (key)
        self.m_GamePlayIds[key] = true
    end)
    self.m_IsInit = true
end

LuaCommonCountDownViewMgr.Data = nil

function LuaCommonCountDownViewMgr:SetInfo(text, button1Visible, button2Visible, button1Text, button2Text, onButton1Click, onButton2Click,onBgClick)
    LuaCommonCountDownViewMgr.Data={}
    LuaCommonCountDownViewMgr.Data.text = text
    LuaCommonCountDownViewMgr.Data.button1Visible = button1Visible
    LuaCommonCountDownViewMgr.Data.button2Visible = button2Visible
    LuaCommonCountDownViewMgr.Data.button1Text = button1Text
    LuaCommonCountDownViewMgr.Data.button2Text = button2Text
    LuaCommonCountDownViewMgr.Data.onButton1Click = onButton1Click
    LuaCommonCountDownViewMgr.Data.onButton2Click = onButton2Click
    LuaCommonCountDownViewMgr.Data.onBgClick = onBgClick
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate2")
end
