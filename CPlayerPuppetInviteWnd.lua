-- Auto Generated!!
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CPlayerPuppetInviteWnd = import "L10.UI.CPlayerPuppetInviteWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local House_XiangfangUpgrade = import "L10.Game.House_XiangfangUpgrade"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerPuppetInviteWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
    UIEventListener.Get(this.tipBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        g_MessageMgr:ShowMessage("FRIEND_PUPPET_INVITE_TIP")
    end)

    this.templateNode:SetActive(false)
    Extensions.RemoveAllChildren(this.grid.transform)
    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.TabChange, MakeGenericClass(Action2, GameObject, Int32), this)
    this.tabBar:ChangeTab(0, false)

    local xiangfangGrade = CClientHouseMgr.Inst.mConstructProp.XiangfangGrade
    local data = House_XiangfangUpgrade.GetData(xiangfangGrade)
    local maxRoomCurGrade = data.MaxPuppetNum
    local nowNum = CClientHouseMgr.Inst.mFurnitureProp.FriendPuppetInfo.Count
    local rest = maxRoomCurGrade - nowNum
    if rest < 0 then
        rest = 0
    end
    this.inviteLabel.text = (tostring(rest) .. "/") .. tostring(maxRoomCurGrade)
end
CPlayerPuppetInviteWnd.m_TabChange_CS2LuaHook = function (this, go, index) 
    this.savePlayerId = 0
    if index == 0 then
        --Gac2Gas.QueryFriendsFitPuppetRequirement();
        this:InitFriendPuppetInfo()
    elseif index == 1 then
        --Gac2Gas.QueryFriendsFitPuppetRequirement(); // Need change
        this:InitFriendOfflinePuppetInfo()
    end
end
CPlayerPuppetInviteWnd.m_InitPanelInfo_CS2LuaHook = function (this) 
    if this.tabBar.SelectedIndex == 0 then
        this:InitFriendPuppetInfo()
    elseif this.tabBar.SelectedIndex == 1 then
        this:InitFriendOfflinePuppetInfo()
    end
end
CPlayerPuppetInviteWnd.m_InitFriendPuppetInfo_CS2LuaHook = function (this) 
    local ShowPlayerPuppetList = CHousePuppetMgr.Inst.ShowPlayerPuppetList
    Extensions.RemoveAllChildren(this.grid.transform)
    if ShowPlayerPuppetList.Count > 0 then
        do
            local i = 0
            while i < ShowPlayerPuppetList.Count do
                local friendId = ShowPlayerPuppetList[i]
                local _info = CIMMgr.Inst:GetBasicInfo(friendId)
                if _info ~= nil then
                    local node = NGUITools.AddChild(this.grid.gameObject, this.templateNode)
                    node:SetActive(true)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("Icon/level"), typeof(UILabel)).text = tostring(_info.Level)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = _info.Name
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("friendness"), typeof(UILabel)).text = LocalString.GetString("友好度:") .. tostring(CIMMgr.Inst:GetFriendliness(friendId))
                    local portrait = CUICommonDef.GetPortraitName(_info.Class, _info.Gender, -1)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(portrait, false)
                    local toggleNode = node.transform:Find("Toggle").gameObject
                    local toggleSignNode = node.transform:Find("Toggle/Sprite").gameObject
                    UIEventListener.Get(toggleNode).onClick = DelegateFactory.VoidDelegate(function (p) 
                        this:ClickToggleNode(toggleSignNode, friendId)
                    end)
                end
                --;
                i = i + 1
            end
        end

        UIEventListener.Get(this.inviteBtn).onClick = MakeDelegateFromCSFunction(this.InviteBtnClick, VoidDelegate, this)
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CPlayerPuppetInviteWnd.m_InitFriendOfflinePuppetInfo_CS2LuaHook = function (this) 
    local ShowPlayerPuppetList = CHousePuppetMgr.Inst.ShowPlayerOfflineList
    Extensions.RemoveAllChildren(this.grid.transform)
    if ShowPlayerPuppetList.Count > 0 then
        do
            local i = 0
            while i < ShowPlayerPuppetList.Count do
                local friendId = ShowPlayerPuppetList[i]
                local _info = CIMMgr.Inst:GetBasicInfo(friendId)
                if _info ~= nil then
                    local node = NGUITools.AddChild(this.grid.gameObject, this.templateNode)
                    node:SetActive(true)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("Icon/level"), typeof(UILabel)).text = tostring(_info.Level)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = _info.Name
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("friendness"), typeof(UILabel)).text = LocalString.GetString("友好度:") .. tostring(CIMMgr.Inst:GetFriendliness(friendId))
                    local portrait = CUICommonDef.GetPortraitName(_info.Class, _info.Gender, -1)
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(portrait, false)
                    local toggleNode = node.transform:Find("Toggle").gameObject
                    local toggleSignNode = node.transform:Find("Toggle/Sprite").gameObject
                    UIEventListener.Get(toggleNode).onClick = DelegateFactory.VoidDelegate(function (p) 
                        this:ClickToggleNode(toggleSignNode, friendId)
                    end)
                end
                --;
                i = i + 1
            end
        end

        UIEventListener.Get(this.inviteBtn).onClick = MakeDelegateFromCSFunction(this.InviteOfflineBtnClick, VoidDelegate, this)
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
CPlayerPuppetInviteWnd.m_ClickToggleNode_CS2LuaHook = function (this, sign, playerid) 
    if this.saveSignNode ~= nil then
        this.saveSignNode:SetActive(false)
    end

    sign:SetActive(true)
    this.saveSignNode = sign
    this.savePlayerId = playerid
end
CPlayerPuppetInviteWnd.m_InviteBtnClick_CS2LuaHook = function (this, go) 
    if this.savePlayerId > 0 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FRIEND_PUPPET_INVITE_NEED_MONEY"), DelegateFactory.Action(function () 
            Gac2Gas.PlayerRequestAddFriendPuppet(this.savePlayerId)
            this:Close()
        end), nil, nil, nil, false)
    end
end
CPlayerPuppetInviteWnd.m_InviteOfflineBtnClick_CS2LuaHook = function (this, go) 
    if this.savePlayerId > 0 then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("LIUSHI_FRIEND_PUPPET_INVITE_NEED_MONEY"), DelegateFactory.Action(function () 
            Gac2Gas.PlayerRequestAddFriendPuppet(this.savePlayerId)
            this:Close()
        end), nil, nil, nil, false)
    end
end
