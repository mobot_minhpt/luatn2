require("common/common_include")

local UIScrollView=import "UIScrollView"
local UIWidget = import "UIWidget"
local Baby_QiChang = import "L10.Game.Baby_QiChang"
local UITable = import "UITable"
local Baby_QiYu = import "L10.Game.Baby_QiYu"
local Baby_ShaoNian = import "L10.Game.Baby_ShaoNian"
local Baby_YouEr = import "L10.Game.Baby_YouEr"
local UIRoot=import "UIRoot"
local Screen=import "UnityEngine.Screen"
local NGUIMath=import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UIGrid = import "UIGrid"
local EnumGender = import "L10.Game.EnumGender"

LuaBabyQiChangRequestTip=class()

RegistChildComponent(LuaBabyQiChangRequestTip, "QiChangNameLabel", UILabel)
RegistChildComponent(LuaBabyQiChangRequestTip, "StatusLabel", UILabel)
RegistChildComponent(LuaBabyQiChangRequestTip, "Background", UIWidget)
RegistChildComponent(LuaBabyQiChangRequestTip, "Table", UITable)
RegistChildComponent(LuaBabyQiChangRequestTip, "ScrollView", UIScrollView)
RegistChildComponent(LuaBabyQiChangRequestTip, "Header", UIWidget)

RegistChildComponent(LuaBabyQiChangRequestTip, "RequestTemplate", GameObject)
RegistChildComponent(LuaBabyQiChangRequestTip, "RewardTemplate", GameObject)
RegistChildComponent(LuaBabyQiChangRequestTip, "TitleTemplate", GameObject)

RegistClassMember(LuaBabyQiChangRequestTip, "SelectedBaby")
RegistClassMember(LuaBabyQiChangRequestTip, "SelectedQiChang")

function LuaBabyQiChangRequestTip:Init()
    if not LuaBabyMgr.m_ChosenBaby then
        CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
        return
    end
    self.SelectedBaby = LuaBabyMgr.m_ChosenBaby

    local qichang =  Baby_QiChang.GetData(LuaBabyMgr.m_SelectedQiChangTipId)
    if not qichang  then
        CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
        return
    end
    self.SelectedQiChang = qichang

    self.RequestTemplate:SetActive(false)
    self.RewardTemplate:SetActive(false)
    self.TitleTemplate:SetActive(false)

    self.QiChangNameLabel.text = SafeStringFormat3("[%s]%s[-]", LuaBabyMgr.GetQiChangNameColor(self.SelectedQiChang.Quality-1), self:GetQiChangName(self.SelectedQiChang))

    Extensions.RemoveAllChildren(self.Table.transform)
    self:UpdateQiChangRequest()
    self:UpdateQiChangReward()

    
    self.Table:Reposition()

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + (self.Table.padding.y * 2) + contentTopPadding + contentBottomPadding + (self.ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 80, virtualScreenHeight - 100)
    self.Background:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.Header:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.ScrollView.panel:ResetAndUpdateAnchors()
    self.ScrollView:ResetPosition()
end

function LuaBabyQiChangRequestTip:UpdateQiChangRequest()
    local title = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
    local titleLabel = title:GetComponent(typeof(UILabel))
    titleLabel.text = LocalString.GetString("解锁条件")
    title:SetActive(true)

    -- 已经解锁的条件放在前面
    local requestTips = {}

    local satisfied = {}
    local notSatisfied = {}

    local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang
    -- step 1 前置气场
    if self.SelectedQiChang.PreQichang ~= 0 then
        local preQiChang = Baby_QiChang.GetData(self.SelectedQiChang.PreQichang)
        if preQiChang then
            local str = SafeStringFormat3(LocalString.GetString("解锁前置气场[%s]【%s】[-]"), LuaBabyMgr.GetQiChangNameColor(preQiChang.Quality-1), self:GetQiChangName(preQiChang))
            -- 检查前置气场是否解锁
            if CommonDefs.DictContains_LuaCall(activedQiChang, preQiChang.ID) then
                table.insert(requestTips, { tip = str, IsActive = true})
                table.insert(satisfied, str)
            else
                table.insert(requestTips, { tip = str, IsActive = false})
                table.insert(notSatisfied, str)
            end
        end
        
    end

    -- Step 2 获得奇遇
    local qiyuDict = self.SelectedBaby.Props.ScheduleData.QiYuData

    if self.SelectedQiChang.QiyuGet ~= 0 then
        local qiyu = Baby_QiYu.GetData(self.SelectedQiChang.QiyuGet)
        if qiyu then
            local str = SafeStringFormat3(LocalString.GetString("解锁奇遇%s"), qiyu.QiYuName)
            if CommonDefs.DictContains_LuaCall(qiyuDict, qiyu.ID) then
                table.insert(requestTips, { tip = str, IsActive = true})
                table.insert(satisfied, str)
            else
                table.insert(requestTips, { tip = str, IsActive = false})
                table.insert(notSatisfied, str)
            end
        end
    end

    -- Step 3 属性要求, 有12个
    for i = 1, 12 do
        if self:GetQiChangRequestPropByIndex(i, self.SelectedQiChang) > 0 then
            local request = self:GetQiChangRequestPropByIndex(i, self.SelectedQiChang)
            local own = 0
            if self.SelectedBaby.Props.YoungProp and CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, i) then
                own = self.SelectedBaby.Props.YoungProp[i]
            end
            local shaoNian = Baby_ShaoNian.GetData(i)
            local youEr = Baby_YouEr.GetData(shaoNian.YouErName)
            if shaoNian and youEr then
                
                local str = SafeStringFormat3(LocalString.GetString("%s-%s达到 ？？？"), youEr.Name, shaoNian.Name) 
                if self.SelectedQiChang.Quality <= 3 then
                    str = SafeStringFormat3(LocalString.GetString("%s-%s达到%d (%d/%d)"), youEr.Name, shaoNian.Name, request, own, request)
                end
                
                if own >= request then
                    table.insert(requestTips, { tip = str, IsActive = true})
                    table.insert(satisfied, str)
                else
                    table.insert(requestTips, { tip = str, IsActive = false})
                    table.insert(notSatisfied, str)
                end
            end
        end
    end

    -- Step 4 属性最高
    if self.SelectedQiChang.TopAttribute ~= 0 and self.SelectedQiChang.Quality >= 5 then
        local youer = Baby_YouEr.GetData(self.SelectedQiChang.TopAttribute)

        if youer and self.SelectedBaby.Props.YoungProp then
            local str = SafeStringFormat3(LocalString.GetString("最高属性%s"), youer.Name)
            local highest = 0

            if CommonDefs.DictContains_LuaCall(self.SelectedBaby.Props.YoungProp, self.SelectedQiChang.TopAttribute) then
                highest = LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, self.SelectedQiChang.TopAttribute)
            end
            local isHighest = true

            if self.SelectedBaby.Props.YoungProp and self.SelectedBaby.Props.YoungProp.Count >= 12 then
                for i = 1, 6, 1 do
                    if self.SelectedQiChang.TopAttribute ~=i and LuaBabyMgr.GetYoundPropByGroup(self.SelectedBaby, i) > highest then
                        isHighest = false
                    end
                end
            end
            if isHighest then
                table.insert(requestTips, { tip = str, IsActive = true})
                table.insert(satisfied, str)
            else
                table.insert(requestTips, { tip = str, IsActive = false})
                table.insert(notSatisfied, str)
            end
        end
    end

    -- 创建列表
    for i = 1, #satisfied do
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.RequestTemplate)
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = satisfied[i]
        go:SetActive(true)
    end

    for i = 1, #notSatisfied do
        local go = CUICommonDef.AddChild(self.Table.gameObject, self.RequestTemplate)
        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = SafeStringFormat3("[777777]%s[-]", notSatisfied[i])
        go:SetActive(true)
    end

    if CommonDefs.DictContains_LuaCall(activedQiChang, self.SelectedQiChang.ID) then
        self.StatusLabel.text = LocalString.GetString("[00FF00]已鉴定[-]")
    else
        if #notSatisfied > 0 then
            self.StatusLabel.text = LocalString.GetString("[B2B2B2]未解锁[-]")
        else
            self.StatusLabel.text = LocalString.GetString("[FFFFFF]待鉴定[-]")
        end
    end
end


function LuaBabyQiChangRequestTip:GetQiChangRequestPropByIndex(index, qichang)
    if index == 1 then
        return qichang.Strategy
    elseif index == 2 then
        return qichang.Taoism
    elseif index == 3 then
        return qichang.Martial
    elseif index == 4 then
        return qichang.Military
    elseif index == 5 then
        return qichang.Poetry
    elseif index == 6 then
        return qichang.Yayi
    elseif index == 7 then
        return qichang.Heaven
    elseif index == 8 then
        return qichang.Process
    elseif index == 9 then
        return qichang.Virtue
    elseif index == 10 then
        return qichang.Ambiguity
    elseif index == 11 then
        return qichang.Tyrannical
    elseif index == 12 then
        return qichang.Evil
    end
    return ""
end

function LuaBabyQiChangRequestTip:UpdateQiChangReward()
	local qiChangReward
	if self.SelectedBaby.Gender == EnumToInt(EnumGender.Male) then
		qiChangReward = self.SelectedQiChang.RewardM
	else
		qiChangReward = self.SelectedQiChang.RewardF
	end
    if not qiChangReward or qiChangReward.Length <= 0 then
        return
    end

    local title = CUICommonDef.AddChild(self.Table.gameObject, self.TitleTemplate)
    local titleLabel = title:GetComponent(typeof(UILabel))
    titleLabel.text = LocalString.GetString("获得奖励")
    title:SetActive(true)

    local reward = CUICommonDef.AddChild(self.Table.gameObject, self.RewardTemplate)
    reward:SetActive(true)
    local rewardTable = reward.transform:Find("RewardTable"):GetComponent(typeof(UIGrid))
    local rewardTemplate = reward.transform:Find("Reward").gameObject
    rewardTemplate:SetActive(false)

    for i = 0, qiChangReward.Length-1, 2 do
        local go = CUICommonDef.AddChild(rewardTable.gameObject, rewardTemplate)
        self:InitRewardItem(go, qiChangReward[i])
        go:SetActive(true)
    end
    rewardTable:Reposition()

end

function LuaBabyQiChangRequestTip:InitRewardItem(go, itemTId)
    local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    IconTexture:Clear()
    local item = Item_Item.GetData(itemTId)
    if item then
        IconTexture:LoadMaterial(item.Icon)
        CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemTId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        end), false)
    end
end

function LuaBabyQiChangRequestTip:GetQiChangName(qichang)
    local name = qichang.NameM
    if self.SelectedBaby.Gender == 1 then
        name = qichang.NameF
    end
    return name
end

function LuaBabyQiChangRequestTip:Update()
    self:ClickThroughToClose()
end

function LuaBabyQiChangRequestTip:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
        end
    end
end

return LuaBabyQiChangRequestTip