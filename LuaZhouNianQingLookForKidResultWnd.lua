local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaZhouNianQingLookForKidResultWnd = class()

LuaZhouNianQingLookForKidResultWnd.s_HasReward = nil
LuaZhouNianQingLookForKidResultWnd.s_GamePlayId = 0

RegistClassMember(LuaZhouNianQingLookForKidResultWnd, "m_RewardLabel")
RegistClassMember(LuaZhouNianQingLookForKidResultWnd, "m_Item1")
RegistClassMember(LuaZhouNianQingLookForKidResultWnd, "m_Item2")
RegistClassMember(LuaZhouNianQingLookForKidResultWnd, "m_ReceivedLabel")
RegistClassMember(LuaZhouNianQingLookForKidResultWnd, "m_WinBg")

function LuaZhouNianQingLookForKidResultWnd:Awake()
    self.m_RewardLabel = self.transform:Find("Info/WinStyle/Win_Info/RewardLabel"):GetComponent(typeof(UILabel))
    self.m_ReceivedLabel = self.transform:Find("Info/WinStyle/Win_Info/ReceivedLabel").gameObject
    self.m_Item1 = self.transform:Find("Info/WinStyle/Win_Info/Item1").gameObject
    self.m_Item2 = self.transform:Find("Info/WinStyle/Win_Info/Item2").gameObject
    self.m_WinBg = self.transform:Find("Info/WinStyle/Win_Bg")
end

function LuaZhouNianQingLookForKidResultWnd:Init()
    local award = ZhouNianQing2023_Setting.GetData().WinAward
    local idx = 1
    for i = 1, 3 do
        if award[i - 1][0] == LuaZhouNianQingLookForKidResultWnd.s_GamePlayId then
            idx = i
            break
        end
    end
    self.m_WinBg:GetChild(1).gameObject:SetActive(idx == 1)
    self.m_WinBg:GetChild(2).gameObject:SetActive(idx == 2)
    self.m_WinBg:GetChild(3).gameObject:SetActive(idx == 3)
    if LuaZhouNianQingLookForKidResultWnd.s_HasReward then
        self.m_RewardLabel.gameObject:SetActive(true) --wyh
        self.m_ReceivedLabel:SetActive(false)
        self.m_Item1:SetActive(true)
        self.m_Item2:SetActive(true)
        self:InitOneItem(self.m_Item1, award[idx - 1][1])
        self:InitOneItem(self.m_Item2, award[idx - 1][2])
    else
        self.m_RewardLabel.gameObject:SetActive(false)
        self.m_ReceivedLabel:SetActive(true)
        self.m_Item1:SetActive(false)
        self.m_Item2:SetActive(false)
    end
    self.transform:GetComponent(typeof(Animation)):Play("common_result_win")
end

function LuaZhouNianQingLookForKidResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end