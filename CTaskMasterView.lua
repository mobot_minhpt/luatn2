-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CellIndex = import "L10.UI.UITableView+CellIndex"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CommonDefs = import "L10.Game.CommonDefs"
local CTaskMasterView = import "L10.UI.CTaskMasterView"
local DelegateFactory = import "DelegateFactory"
local Int32 = import "System.Int32"
local ItemProperty = import "L10.UI.CTaskMasterView+ItemProperty"
local LocalString = import "LocalString"
local Object = import "System.Object"
local String = import "System.String"
local Task_Task = import "L10.Game.Task_Task"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"

local checkZhujueJuqingAlert = function()
    local taskIDs = CZhuJueJuQingMgr.Instance:GetSelfJuqing().TaskID
    local cnt = taskIDs.Length
    for index = 1, cnt do
        if CZhuJueJuQingMgr.Instance:IsChapterUnLocked(index) and not CZhuJueJuQingMgr.Instance:IsChapterFinished(index) and not CZhuJueJuQingMgr.Instance:IsChapterDoing(index) then 
            local data = Task_Task.GetData(taskIDs[index - 1][1])
            if CClientMainPlayer.Inst and data and data.Status == 0 and data.Level <= CClientMainPlayer.Inst.Level then
                return true
            end
        end
    end
    return false
end

CTaskMasterView.m_Awake_CS2LuaHook = function (this) 
    if CTaskMasterView.TaskGroupDict == nil then
        CTaskMasterView.TaskGroupDict = CreateFromClass(MakeGenericClass(Dictionary, Int32, String))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 1, typeof(String), LocalString.GetString("主线任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 3, typeof(String), LocalString.GetString("支线任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 5, typeof(String), LocalString.GetString("师门任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 6, typeof(String), LocalString.GetString("官府任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 7, typeof(String), LocalString.GetString("帮会任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 9, typeof(String), LocalString.GetString("日常任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 11, typeof(String), LocalString.GetString("关系任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 13, typeof(String), LocalString.GetString("节日任务"))
        CommonDefs.DictAdd(CTaskMasterView.TaskGroupDict, typeof(Int32), 15, typeof(String), LocalString.GetString("奇遇任务"))
    end
    this.ZhujueJuqingButton.transform:Find("Alert").gameObject:SetActive(checkZhujueJuqingAlert())
end
CTaskMasterView.m_LoadTaskList_CS2LuaHook = function (this, showReceivedTasks, selectedTaskId) 
    this.lastTaskId = selectedTaskId
    this.showReceivedTasks = showReceivedTasks
    CommonDefs.ListClear(this.groupList)
    CommonDefs.DictClear(this.tasksDict)
    if showReceivedTasks then
        this:LoadReceivedTaskList()
    else
        this:LoadAcceptableTaskList()
    end
    this.ZhujueJuqingButton.transform:Find("Alert").gameObject:SetActive(checkZhujueJuqingAlert())
end
CTaskMasterView.m_LoadReceivedTaskList_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local taskProperty = CClientMainPlayer.Inst.TaskProp
    local tasks = taskProperty.CurrentTasks

    CommonDefs.DictIterate(tasks, DelegateFactory.Action_object_object(function (___key, ___value) 
        local pair = {}
        pair.Key = ___key
        pair.Value = ___value
        local continue
        repeat
            local template = Task_Task.GetData(pair.Key)
            if template == nil then
                continue = true
                break
            end
            if CLuaTaskMgr.IsHideTaskListItem(pair.Key) then
                continue = true
                break
            end
            --获取分组信息和任务名称
            local property = CreateFromClass(ItemProperty)
            local displayNode = template.DisplayNode
            property.taskName = template.Display
            property.templateId = pair.Key

            if CommonDefs.DictContains(CTaskMasterView.TaskGroupDict, typeof(Int32), displayNode) then
                if not CommonDefs.ListContains(this.groupList, typeof(Int32), displayNode) then
                    CommonDefs.ListAdd(this.groupList, typeof(Int32), displayNode)
                    CommonDefs.DictAdd(this.tasksDict, typeof(Int32), displayNode, typeof(MakeGenericClass(List, ItemProperty)), CreateFromClass(MakeGenericClass(List, ItemProperty)))
                end
                CommonDefs.ListAdd(CommonDefs.DictGetValue(this.tasksDict, typeof(Int32), displayNode), typeof(ItemProperty), property)
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end))

    CommonDefs.ListSort1(this.groupList, typeof(Int32), nil)

    if this.tasksDict.Count == 0 and this.OnTaskItemSelected ~= nil then
        GenericDelegateInvoke(this.OnTaskItemSelected, Table2ArrayWithCount({System.UInt32.MaxValue}, 1, MakeArrayClass(Object)))
    end

    this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), false)

    do
        local i = 0
        while i < this.groupList.Count do
            local list = CommonDefs.DictGetValue(this.tasksDict, typeof(Int32), this.groupList[i])
            do
                local j = 0
                while j < list.Count do
                    --选中更新前的任务
                    if list[j].templateId == this.lastTaskId then
                        this:SetRowSelected(CreateFromClass(CellIndex, i, j, CellIndexType.Row))
                        return
                    end
                    j = j + 1
                end
            end
            i = i + 1
        end
    end

    --默认选中第一个任务
    this.lastTaskId = System.UInt32.MaxValue
    this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), true)
end
CTaskMasterView.m_LoadAcceptableTaskList_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local taskProperty = CClientMainPlayer.Inst.TaskProp
    CommonDefs.EnumerableIterate(taskProperty.AcceptableTasks.Keys, DelegateFactory.Action_object(function (___value) 
        local templateId = ___value
        local continue
        repeat
            --获取分组信息和任务名称

            local template = Task_Task.GetData(templateId)
            --NotDisplay==1的任务不显示在可接任务列表
            if template == nil or template.NotDisplay == 1 then
                continue = true
                break
            end
            local property = CreateFromClass(ItemProperty)
            local displayNode = template.DisplayNode
            property.taskName = template.Display
            property.templateId = templateId

            if CommonDefs.DictContains(CTaskMasterView.TaskGroupDict, typeof(Int32), displayNode) then
                if not CommonDefs.ListContains(this.groupList, typeof(Int32), displayNode) then
                    CommonDefs.ListAdd(this.groupList, typeof(Int32), displayNode)
                    CommonDefs.DictAdd(this.tasksDict, typeof(Int32), displayNode, typeof(MakeGenericClass(List, ItemProperty)), CreateFromClass(MakeGenericClass(List, ItemProperty)))
                end
                CommonDefs.ListAdd(CommonDefs.DictGetValue(this.tasksDict, typeof(Int32), displayNode), typeof(ItemProperty), property)
            end
            continue = true
        until 1
        if not continue then
            return
        end
    end))

    CommonDefs.ListSort1(this.groupList, typeof(Int32), nil)

    if this.tasksDict.Count == 0 and this.OnTaskItemSelected ~= nil then
        GenericDelegateInvoke(this.OnTaskItemSelected, Table2ArrayWithCount({System.UInt32.MaxValue}, 1, MakeArrayClass(Object)))
    end

    this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), false)
    if CommonDefs.DictContains(taskProperty.AcceptableTasks, typeof(UInt32), this.lastTaskId) then
        do
            local i = 0
            while i < this.groupList.Count do
                local list = CommonDefs.DictGetValue(this.tasksDict, typeof(Int32), this.groupList[i])
                do
                    local j = 0
                    while j < list.Count do
                        if list[j].templateId == this.lastTaskId then
                            this:SetRowSelected(CreateFromClass(CellIndex, i, j, CellIndexType.Row))
                            return
                        end
                        j = j + 1
                    end
                end
                i = i + 1
            end
        end
    end
    this.lastTaskId = System.UInt32.MaxValue
    this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), true)
end
CTaskMasterView.m_RefreshCellForRowAtIndex_CS2LuaHook = function (this, cell, index) 
    if index.type == CellIndexType.Section then
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(UILabel)).text = CommonDefs.DictGetValue(CTaskMasterView.TaskGroupDict, typeof(Int32), this.groupList[index.section])
        local taskIds = CreateFromClass(MakeGenericClass(List, UInt32))
        do
            local i = 0
            while i < CommonDefs.DictGetValue(this.tasksDict, typeof(Int32), this.groupList[index.section]).Count do
                CommonDefs.ListAdd(taskIds, typeof(UInt32), CommonDefs.DictGetValue(this.tasksDict, typeof(Int32), this.groupList[index.section])[i].templateId)
                i = i + 1
            end
        end
        CommonDefs.GetComponent_GameObject_Type(cell, typeof(CCommonLuaScript)):Init({1, taskIds, this.showReceivedTasks})
    else
        local p = CommonDefs.DictGetValue(this.tasksDict, typeof(Int32), this.groupList[index.section])[index.row]
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(UILabel)).text = p.taskName
        CommonDefs.GetComponent_GameObject_Type(cell, typeof(CCommonLuaScript)):Init({0, InitializeList(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, p.templateId), this.showReceivedTasks})
    end
end
