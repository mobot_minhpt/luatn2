local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"

LuaCarnival2023PVEResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

function LuaCarnival2023PVEResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaCarnival2023PVEResultWnd:Init()
    local info = LuaCarnival2023Mgr.pveResultInfo

    local daily = self.transform:Find("Anchor/Daily")
    local challenge = self.transform:Find("Anchor/Challenge")
    local num = self.transform:Find("Anchor/Num"):GetComponent(typeof(UILabel))
    local isNormal = info.type == "normal"
    daily.gameObject:SetActive(isNormal)
    challenge.gameObject:SetActive(not isNormal)

    num.text = isNormal and info.killMonsterNum or self:GetFinishTimeStr(info.finishTime)
    if isNormal then
        daily:Find("NoTimes").gameObject:SetActive(not info.getAward)
        local addPointButton = daily:Find("AddPointButton").gameObject
        local pointNum = daily:Find("PointNum"):GetComponent(typeof(UILabel))
        addPointButton.gameObject:SetActive(info.getAward)
        pointNum.gameObject:SetActive(info.getAward)

        pointNum.text = SafeStringFormat3(LocalString.GetString("获得点数 %d点"), info.getPoint)
        UIEventListener.Get(addPointButton).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnAddPointButtonClick()
        end)
    else
        local returnAward = challenge:Find("Reward/ReturnAward"):GetComponent(typeof(CQnReturnAwardTemplate))
        challenge:Find("Reward/NoTimes").gameObject:SetActive(not info.award)
        returnAward.gameObject:SetActive(info.award)
        returnAward:Init(Item_Item.GetData(LuaCarnival2023Mgr:GetPVEPassAwardItemId()), 0)
        UIEventListener.Get(challenge:Find("Rank/RankButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnRankButtonClick()
        end)
        local rankStr = info.myRank > 0 and SafeStringFormat3(LocalString.GetString("第%d名"), info.myRank) or LocalString.GetString("未上榜")
        challenge:Find("Rank/RankNum"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("当前 %s"), rankStr)
    end
end

function LuaCarnival2023PVEResultWnd:GetFinishTimeStr(finishTime)
    local second = finishTime % 60
    local minute = math.floor(finishTime / 60)
    return SafeStringFormat3("%02d:%02d", minute, second)
end

--@region UIEvent

function LuaCarnival2023PVEResultWnd:OnAddPointButtonClick()
    LuaCarnival2023Mgr.pveSkillInfo.needQueryData = true
	CUIManager.ShowUI(CLuaUIResources.Carnival2023PVESkillAddPointWnd)
end

function LuaCarnival2023PVEResultWnd:OnRankButtonClick()
    CUIManager.ShowUI(CLuaUIResources.Carnival2023PVERankWnd)
end

--@endregion UIEvent
