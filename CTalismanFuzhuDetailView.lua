-- Auto Generated!!
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CTalismanFuzhuDetailView = import "L10.UI.CTalismanFuzhuDetailView"
local CTalismanFuzhuWordTemplate = import "L10.UI.CTalismanFuzhuWordTemplate"
local CTalismanMgr = import "L10.Game.CTalismanMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local NGUITools = import "NGUITools"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local Talisman_Setting = import "L10.Game.Talisman_Setting"
local Talisman_Suit = import "L10.Game.Talisman_Suit"
--local Talisman_XianJiaSuitConsist = import "L10.Game.Talisman_XianJiaSuitConsist"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Word_Word = import "L10.Game.Word_Word"
CTalismanFuzhuDetailView.m_Init_CS2LuaHook = function (this, index) 
    this.selectSheetIndex = index
    this:InitLevel(this.level)
    this.useLingyu.OnValueChanged = MakeDelegateFromCSFunction(this.OnUseLingyuChanged, MakeGenericClass(Action1, Boolean), this)
    if not this.initUseLingyu then
        this.useLingyu:SetSelected(false, false)
        this.initUseLingyu = true
    end
    local item = Mall_LingYuMall.GetData(Talisman_Setting.GetData().LianJingItemId)
    if item ~= nil and item.Status ~= 3 then
        this.lingyuCtrl:SetType(item.CanUseBindJade > 0 and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
        this.lingyuCtrl:SetCost(item.Jade * Talisman_Setting.GetData().WashSuitCost)
    else
        this.lingyuCtrl:SetCost(0)
    end
    this:InitSuitName()
    this.tips:SetActive(not CTalismanMgr.Inst:PlayerActivateSuit())
end
CTalismanFuzhuDetailView.m_InitSuitName_CS2LuaHook = function (this) 
    local name = ""
    Talisman_XianJiaSuitConsist.Foreach(function (k, data) 
        local suitId = data.ActivateSuits[0].SuitId
        local suit = Talisman_Suit.GetData(suitId)
        if suit ~= nil and suit.XianJiaClass == EnumToInt(CClientMainPlayer.Inst.Class) then
            name = data.Name
        end
    end)
    this.suitNameLabel.text = name
end
CTalismanFuzhuDetailView.m_OnUseLingyuChanged_CS2LuaHook = function (this, select) 
    if this:InitConsumeItem() or not select then
        this.consumeItem:SetActive(true)
        this.lingyuCtrl.gameObject:SetActive(false)
    elseif select then
        this.consumeItem:SetActive(false)
        this.lingyuCtrl.gameObject:SetActive(true)
    end
end
CTalismanFuzhuDetailView.m_InitConsumeItem_CS2LuaHook = function (this) 
    local item = Item_Item.GetData(Talisman_Setting.GetData().LianJingItemId)
    if item == nil then
        return false
    end
    this.iconTexture:LoadMaterial(item.Icon)
    this.itemNameLabel.text = item.Name
    local bindCount, notbindCount
    local needCount = Talisman_Setting.GetData().WashSuitCost
    bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(item.ID)
    if bindCount + notbindCount >= needCount then
        this.itemAmountLabel.text = System.String.Format("{0}/{1}", bindCount + notbindCount, needCount)
        this.aquireGo:SetActive(false)
        return true
    else
        this.itemAmountLabel.text = System.String.Format("[c][FF0000]{0}[-][c]/{1}", bindCount + notbindCount, needCount)
        this.aquireGo:SetActive(true)
        return false
    end
end
CTalismanFuzhuDetailView.m_InitLevel_CS2LuaHook = function (this, index) 
    this.level = index
    repeat
        local default = this.level
        if default == (0) then
            this.levelLabel.text = LocalString.GetString("一阶")
            break
        elseif default == (1) then
            this.levelLabel.text = LocalString.GetString("二阶")
            break
        elseif default == (2) then
            this.levelLabel.text = LocalString.GetString("三阶")
            break
        elseif default == (3) then
            this.levelLabel.text = LocalString.GetString("四阶")
            break
        else
            break
        end
    until 1
    this:InitWordList()
end
CTalismanFuzhuDetailView.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.levelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.levelButton).onClick, MakeDelegateFromCSFunction(this.OnLevelButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.aquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.aquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.exchangeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.exchangeButton).onClick, MakeDelegateFromCSFunction(this.OnExchangeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.consumeItem).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.consumeItem).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), true)
    EventManager.AddListenerInternal(EnumEventType.OnWashTalismanSuitResultReturn, MakeDelegateFromCSFunction(this.OnWashTalismanResultReturn, MakeGenericClass(Action3, Int32, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CTalismanFuzhuDetailView.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.levelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.levelButton).onClick, MakeDelegateFromCSFunction(this.OnLevelButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.aquireGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.aquireGo).onClick, MakeDelegateFromCSFunction(this.OnAcquireButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.exchangeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.exchangeButton).onClick, MakeDelegateFromCSFunction(this.OnExchangeButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.consumeItem).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.consumeItem).onClick, MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this), false)
    EventManager.RemoveListenerInternal(EnumEventType.OnWashTalismanSuitResultReturn, MakeDelegateFromCSFunction(this.OnWashTalismanResultReturn, MakeGenericClass(Action3, Int32, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
end
CTalismanFuzhuDetailView.m_OnWashTalismanResultReturn_CS2LuaHook = function (this, suitIndex, oldCls, newCls) 
    if this.selectSheetIndex == suitIndex then
        do
            local i = 0
            while i < this.checkBoxList.Count do
                local suit = Talisman_Suit.GetData(this.checkBoxList[i].suitId)
                if math.floor(this.checkBoxList[i].wordId / 100) == oldCls then
                    this.checkBoxList[i]:Init(oldCls * 100 + suit.XianJiaLevel, newCls * 100 + suit.XianJiaLevel, this.checkBoxList[i].canWash, this.checkBoxList[i].suitId)
                    this.checkBoxList[i]:SetSelected(true)
                    if i == 0 then
                        this.scrollView:ResetPosition()
                    end
                    break
                end
                i = i + 1
            end
        end
    end
    this.table:Reposition()
end
CTalismanFuzhuDetailView.m_OnLevelButtonClick_CS2LuaHook = function (this, go) 
    local data = CreateFromClass(MakeArrayClass(PopupMenuItemData), 4)
    data[0] = PopupMenuItemData(LocalString.GetString("一阶"), MakeDelegateFromCSFunction(this.InitLevel, MakeGenericClass(Action1, Int32), this), false, nil)
    data[1] = PopupMenuItemData(LocalString.GetString("二阶"), MakeDelegateFromCSFunction(this.InitLevel, MakeGenericClass(Action1, Int32), this), false, nil)
    data[2] = PopupMenuItemData(LocalString.GetString("三阶"), MakeDelegateFromCSFunction(this.InitLevel, MakeGenericClass(Action1, Int32), this), false, nil)
    data[3] = PopupMenuItemData(LocalString.GetString("四阶"), MakeDelegateFromCSFunction(this.InitLevel, MakeGenericClass(Action1, Int32), this), false, nil)
    CPopupMenuInfoMgr.ShowPopupMenu(data, go.transform.position, CPopupMenuInfoMgr.AlignType.Bottom)
end
CTalismanFuzhuDetailView.m_InitWordList_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    local suitIdList = CreateFromClass(MakeGenericClass(List, UInt32))
    Talisman_Suit.Foreach(DelegateFactory.Action_object_object(function (key, data) 
        if data.XianJiaLevel == this.level + 1 and data.XianJiaClass == EnumToInt(CClientMainPlayer.Inst.Class) then
            CommonDefs.ListAdd(suitIdList, typeof(UInt32), key)
        end
    end))
    Extensions.RemoveAllChildren(this.table.transform)
    this.wordTemplate:SetActive(false)
    CommonDefs.ListClear(this.checkBoxList)
    do
        local i = 0
        while i < suitIdList.Count do
            local continue
            repeat
                local suit = Talisman_Suit.GetData(suitIdList[i])
                if suit ~= nil then
                    local wordId = CTalismanMgr.Inst:GetSuitUserWordId(this.selectSheetIndex, suit, true)
                    if wordId == 0 then
                        wordId = suit.Words[0]
                    end
                    local word = Word_Word.GetData(wordId)
                    if word == nil then
                        continue = true
                        break
                    end
                    local instance = NGUITools.AddChild(this.table.gameObject, this.wordTemplate)
                    instance:SetActive(true)
                    local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CTalismanFuzhuWordTemplate))
                    if template ~= nil then
                        template:Init(wordId, wordId, suit.XianJiaCantWash ~= 1, suitIdList[i])
                        CommonDefs.ListAdd(this.checkBoxList, typeof(CTalismanFuzhuWordTemplate), template)
                        if suit.XianJiaCantWash ~= 1 then
                            template.checkBox.OnClick = CommonDefs.CombineListner_Action_QnButton(template.checkBox.OnClick, MakeDelegateFromCSFunction(this.OnWordSelect, MakeGenericClass(Action1, QnButton), this), true)
                        end
                        if i == this.selectWordIndex and suit.XianJiaCantWash ~= 1 then
                            template:SetSelected(true)
                        end
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
    this.table:Reposition()
    this.scrollView:ResetPosition()
end
CTalismanFuzhuDetailView.m_OnWordSelect_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.checkBoxList.Count do
            if go ~= this.checkBoxList[i].checkBox then
                this.checkBoxList[i]:SetSelected(false)
            elseif this.checkBoxList[i].checkBox.Selected then
                this.selectWordIndex = i
            else
                this.selectWordIndex = - 1
            end
            i = i + 1
        end
    end
end
CTalismanFuzhuDetailView.m_OnExchangeButtonClick_CS2LuaHook = function (this, go) 
    local bindCount, notbindCount
    bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(Talisman_Setting.GetData().LianJingItemId)
    if this.selectWordIndex < 0 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择一个需要替换的词条"))
    elseif not this.useLingyu.Selected and bindCount + notbindCount < Talisman_Setting.GetData().WashSuitCost then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(Talisman_Setting.GetData().LianJingItemId, false, go.transform, AlignType1.Right)
    elseif this.useLingyu.Selected and not this.lingyuCtrl.moneyEnough then
        CGetMoneyMgr.Inst:GetLingYu(this.lingyuCtrl:GetCost(), Talisman_Setting.GetData().LianJingItemId)
    else
        Gac2Gas.WashTalismanSuit(this.selectSheetIndex, this.checkBoxList[this.selectWordIndex].suitId, this.useLingyu.Selected)
    end
end
