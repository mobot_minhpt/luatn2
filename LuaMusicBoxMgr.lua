local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local VoicePlayInfo = import "L10.Game.VoicePlayInfo"
local HTTPHelper = import "L10.Game.HTTPHelper"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CAppClient = import "L10.Engine.CAppClient"
local Setting = import "L10.Engine.Setting"
local MsgPackImpl = import "MsgPackImpl"
local CMusicBoxEffect = import "L10.Game.CMusicBoxEffect"

CLuaMusicBoxMgr = {}
----------------
-------随身音乐盒
----------------
CLuaMusicBoxMgr.m_MusicBoxEffect = nil
CLuaMusicBoxMgr.m_MusicBoxList = {}
CLuaMusicBoxMgr.m_CurPlayMusicIndex = 1
CLuaMusicBoxMgr.m_CurPlayMusicState = EnumMusicBoxState.Unstart
CLuaMusicBoxMgr.m_IsEnable = true
CLuaMusicBoxMgr.m_OpenWnd =false
function CLuaMusicBoxMgr:GetMusicBoxEffect()
  self.m_MusicBoxEffect = CMusicBoxEffect.MyMusicBoxEffect
  return self.m_MusicBoxEffect
end

function CLuaMusicBoxMgr:EnableMusicBox(openWnd)
    self.m_OpenWnd = openWnd
    Gac2Gas.RequestMusicBoxPlayList()
end

function CLuaMusicBoxMgr:PlayMusicBox(musicData, isReStart)
  if musicData then
    Gac2Gas.RequestSwitchMusicBoxState(musicData.BuffID,EnumMusicBoxState.Playing)
  end
end

function CLuaMusicBoxMgr:StopMusicBox(isPause)
  local musicData, musicIndex = self:GetCurMusicDataAndIndex()
  if musicData then
    Gac2Gas.RequestSwitchMusicBoxState(musicData.BuffID,EnumMusicBoxState.Pause)
  end
end

function CLuaMusicBoxMgr:GetMusicList()
  return self.m_MusicBoxList
end

function CLuaMusicBoxMgr:GetCurMusicDataAndIndex()
  return self.m_MusicBoxList and self.m_MusicBoxList[self.m_CurPlayMusicIndex] or nil,self.m_CurPlayMusicIndex
end

function CLuaMusicBoxMgr:SwitchMusic(index)
  if self.m_MusicBoxList and #self.m_MusicBoxList > 0 then
    if #self.m_MusicBoxList == 1 then
      g_MessageMgr:ShowMessage("MusicBox_Switch_Music_Failed")
      return
    end
    local remainder = math.fmod( index, #self.m_MusicBoxList)
    self.m_CurPlayMusicIndex = remainder == 0 and #self.m_MusicBoxList or remainder
    Gac2Gas.RequestSwitchMusicBoxState(self.m_MusicBoxList[self.m_CurPlayMusicIndex].BuffID,EnumMusicBoxState.Playing)
  end
end
----------------
----家园装饰物录音
----------------
function CLuaMusicBoxMgr:AddListener()
  g_ScriptEvent:RemoveListener("LoadingSceneFinished", self, "LoadingSceneFinished")
  g_ScriptEvent:AddListener("LoadingSceneFinished", self, "LoadingSceneFinished")
end

CLuaMusicBoxMgr:AddListener()

function CLuaMusicBoxMgr:LoadingSceneFinished()
  --切换到登录界面时停止播放语音
  if CAppClient.CurrentLevelName == Setting.sDengLu_Level then
    if CLuaMusicBoxMgr.VoicePlayTick then
      UnRegisterTick(CLuaMusicBoxMgr.VoicePlayTick)
      CLuaMusicBoxMgr.VoicePlayTick = nil
    end
  end
  if CUIManager.instance.playInfo.voiceId == CLuaMusicBoxMgr.VoiceId then
    CUIManager.StopPlay()
  end
end

CLuaMusicBoxMgr.RecordItemId = nil
CLuaMusicBoxMgr.RecordItemPlace = nil
CLuaMusicBoxMgr.RecordItemPos = nil
function CLuaMusicBoxMgr:OpenRecordWnd(itemId, place, pos)
  CLuaMusicBoxMgr.RecordItemId = itemId
  CLuaMusicBoxMgr.RecordItemPlace = place
  CLuaMusicBoxMgr.RecordItemPos = pos
  CUIManager.ShowUI(CLuaUIResources.MusicBoxRecordWnd)
end

CLuaMusicBoxMgr.VoiceUrlPrefix = "http://hi-163-qnm.nosdn.127.net/tape/"
function CLuaMusicBoxMgr:GetPartialUrl(fullUrl)
  if fullUrl:sub(1, #CLuaMusicBoxMgr.VoiceUrlPrefix) ~= CLuaMusicBoxMgr.VoiceUrlPrefix then
    return nil
  end
  return string.sub(fullUrl, string.len(CLuaMusicBoxMgr.VoiceUrlPrefix) + 1)
end

function CLuaMusicBoxMgr:GetFullUrl(partialUrl)
  return CLuaMusicBoxMgr.VoiceUrlPrefix..partialUrl
end

CLuaMusicBoxMgr.VoiceId = nil
function CLuaMusicBoxMgr:PlayVoice(voiceId, url, bForce)
  if not bForce and not PlayerSettings.MusicEnabled then return end

	local info = VoicePlayInfo(voiceId, EnumVoicePlatform.Default, url)
	HTTPHelper.DownloadVoiceViaPersonalSpace(info, DelegateFactory.Action_bool_string(function (bSuccess, key)
    CLuaMusicBoxMgr.VoiceId = info.voiceId
		CUIManager.PlayVoice(info)
	end))
end

function CLuaMusicBoxMgr:PlayBgMusic(id, bForce)
  if not bForce and not PlayerSettings.MusicEnabled then return true end

  local designdata = StarBiWuShow_SceneMusic.GetData(id)
  SoundManager.Inst.FakeBGMusicTemplateId = designdata.SceneID
  local musicPath = SoundManager.Inst:GetBgEventPath(designdata.SceneID)
  if musicPath ~= SoundManager.Inst.m_BGMusicName then
    SoundManager.Inst:StopBGMusic()
    SoundManager.Inst.m_bgMusic = SoundManager.Inst:PlaySound(musicPath, Vector3.zero, SoundManager.Inst.bgMusicVolume)
    SoundManager.Inst.m_BGMusicName = musicPath
  end
end

function CLuaMusicBoxMgr:StopBgMusic()
  SoundManager.Inst.FakeBGMusicTemplateId = 0
  SoundManager.Inst:StopBGMusic()
  SoundManager.Inst:StartBGMusic()
end

CLuaMusicBoxMgr.VoicePlayTick = nil
function CLuaMusicBoxMgr:PlayMusic()
  CLuaMusicBoxMgr:StopMusic()

  if CLuaMusicBoxMgr.PlayingIsVoice then
    CLuaMusicBoxMgr:PlayVoice(CLuaMusicBoxMgr.PlayingUrl, CLuaMusicBoxMgr:GetFullUrl(CLuaMusicBoxMgr.PlayingUrl))
    if CLuaMusicBoxMgr.PlayingLoop then
      CLuaMusicBoxMgr.VoicePlayTick = RegisterTick(function()
        CLuaMusicBoxMgr:PlayVoice(CLuaMusicBoxMgr.PlayingUrl, CLuaMusicBoxMgr:GetFullUrl(CLuaMusicBoxMgr.PlayingUrl))
      end, 30000)
    end
  else
    local id = tonumber(CLuaMusicBoxMgr.PlayingUrl)
    CLuaMusicBoxMgr:PlayBgMusic(id, false)
  end
end

function CLuaMusicBoxMgr:StopMusic()
  if CLuaMusicBoxMgr.VoicePlayTick then
    UnRegisterTick(CLuaMusicBoxMgr.VoicePlayTick)
    CLuaMusicBoxMgr.VoicePlayTick = nil
  end
  CUIManager.StopPlay()
  CLuaMusicBoxMgr:StopBgMusic()
end

function CLuaMusicBoxMgr:SyncMusicBoxModelAni(furnitureId, isOn)
    local furniture = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if furniture then
      furniture.RO:DoAni(isOn and "rotate01" or "stand01", true, 0, 1, 0.15, true, 1.0)
      furniture.RO:RemoveFX("MusicBoxFx")
      if isOn then
        local fx = CEffectMgr.Inst:AddObjectFX(88801031, furniture.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
        if fx then furniture.RO:AddFX("MusicBoxFx", fx) end
      end
    end
end

function CLuaMusicBoxMgr:GetMusicLeftTimeStr(expiredTime)
  local time = math.max(expiredTime - CServerTimeMgr.Inst.timeStamp, 0)
  --Day
  if time >= 86400 then
    return math.floor(time / 86400)..LocalString.GetString("天")
  elseif time >= 3600 then
    return math.floor(time / 3600)..LocalString.GetString("小时")
  elseif time >= 60 then
    return math.floor(time / 60)..LocalString.GetString("分钟")
  else
    return time..LocalString.GetString("秒")
  end
end

-- Music Box Detail
CLuaMusicBoxMgr.FurnitureId = 0
CLuaMusicBoxMgr.IsVoice = nil
CLuaMusicBoxMgr.IsOn = nil
CLuaMusicBoxMgr.MusicIdOrUrl = nil
CLuaMusicBoxMgr.Name = nil
CLuaMusicBoxMgr.ExpireTime = nil
CLuaMusicBoxMgr.PlayType = 0
CLuaMusicBoxMgr.PlayLoop = 0
CLuaMusicBoxMgr.StopBGM = true
function Gas2Gac.ResponseDiskPlayerData(furnitureId, isVoice, isOn, url, name, expireTime, playType, playLoop, stopBGM)
  CLuaMusicBoxMgr.FurnitureId = furnitureId
  CLuaMusicBoxMgr.IsVoice = isVoice
  CLuaMusicBoxMgr.IsOn = isOn
  CLuaMusicBoxMgr.MusicIdOrUrl = url
  CLuaMusicBoxMgr.Name = name
  CLuaMusicBoxMgr.ExpireTime = expireTime
  CLuaMusicBoxMgr.PlayType = playType
  CLuaMusicBoxMgr.PlayLoop = playLoop
  CLuaMusicBoxMgr.StopBGM = stopBGM
  if CUIManager.IsLoaded(CLuaUIResources.MusicBoxMainWnd) then
    g_ScriptEvent:BroadcastInLua("ResponseDiskPlayerData")
  else
    CUIManager.ShowUI(CLuaUIResources.MusicBoxMainWnd)
  end
end

CLuaMusicBoxMgr.PlayingFurnitureId = 0
CLuaMusicBoxMgr.PlayingIsVoice = false
CLuaMusicBoxMgr.PlayingUrl = nil
CLuaMusicBoxMgr.PlayingLoop = false
CLuaMusicBoxMgr.PlayingIsOn = false
function Gas2Gac.SyncDiskPlayerPlayStatus(furnitureId, isVoice, isOn, url, playLoop, stopBGM)

  CLuaMusicBoxMgr.PlayingFurnitureId = furnitureId
  CLuaMusicBoxMgr.PlayingUrl = url
  CLuaMusicBoxMgr.PlayingIsVoice = isVoice
  CLuaMusicBoxMgr.PlayingLoop = playLoop == 0
  CLuaMusicBoxMgr.PlayingIsOn = isOn
  if isOn then
    CLuaMusicBoxMgr:PlayMusic()
  else
    CLuaMusicBoxMgr:StopMusic()
  end
  CLuaMusicBoxMgr:SyncMusicBoxModelAni(furnitureId, isOn)
end

function Gas2Gac.RespDiskItemCreatorName(playerId, playerName)
  g_ScriptEvent:BroadcastInLua("RespDiskItemCreatorName", playerId, playerName)
end

----------------
-------随身音乐盒
----------------
-- 音乐盒
function Gas2Gac.SendMusicBoxState(currentSong, currentState, progress)
  for index,data in pairs(CLuaMusicBoxMgr.m_MusicBoxList) do
    if currentSong == data.BuffID then
      CLuaMusicBoxMgr.m_CurPlayMusicIndex = index
      CLuaMusicBoxMgr.m_CurPlayMusicState = currentState
      break
    end
  end
  g_ScriptEvent:BroadcastInLua("OnSendMusicBoxState", currentSong, currentState, progress)
end

-- 音乐盒歌单
function Gas2Gac.SendMusicBoxPlayList(playlist)
  local list = MsgPackImpl.unpack(playlist)
  CLuaMusicBoxMgr.m_MusicBoxList = {}
  for i = 0, list.Count - 1, 2 do
    local songId = tonumber(list[i])
    local expireTime = tonumber(list[i+1])
    local data = Buff_MusicBuff.GetData(songId)
    table.insert(CLuaMusicBoxMgr.m_MusicBoxList,{Index = #CLuaMusicBoxMgr.m_MusicBoxList + 1,ExpireTime = expireTime, BuffID = data.BuffID, Name = data.Name, Sound = data.Sound, SoundTime = data.SoundTime, IsLoop =data.IsLoop})
  end
  if CLuaMusicBoxMgr.m_OpenWnd and not CUIManager.IsLoaded(CLuaUIResources.MusicBoxPopControlWnd) and CLuaMusicBoxMgr.m_MusicBoxList then
    if #CLuaMusicBoxMgr.m_MusicBoxList > 0 then
      CUIManager.ShowUI(CLuaUIResources.MusicBoxPopControlWnd)
      CLuaMusicBoxMgr.m_OpenWnd = false
    else
      g_MessageMgr:ShowMessage("MUSIC_BOX_NO_SONG")
    end
  end
  Gac2Gas.RequestMusicBoxState()
end