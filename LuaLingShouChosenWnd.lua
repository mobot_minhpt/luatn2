local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"
CLuaLingShouChosenWnd = class()
RegistClassMember(CLuaLingShouChosenWnd,"leftTextureLoader")
RegistClassMember(CLuaLingShouChosenWnd,"rightTextrueLoader")
RegistClassMember(CLuaLingShouChosenWnd,"leftBtn")
RegistClassMember(CLuaLingShouChosenWnd,"rightBtn")

function CLuaLingShouChosenWnd:Awake()
    self.leftTextureLoader = self.transform:Find("Anchor/Left/Bg/Texture"):GetComponent(typeof(CLingShouModelTextureLoader))
    self.rightTextrueLoader = self.transform:Find("Anchor/Right/Bg/Texture"):GetComponent(typeof(CLingShouModelTextureLoader))
    self.leftBtn = self.transform:Find("Anchor/Left/Bg").gameObject
    self.rightBtn = self.transform:Find("Anchor/Right/Bg").gameObject
end

function CLuaLingShouChosenWnd:Init( )
    local info = CLuaLingShouMgr.m_GuideInfo
    self.leftTextureLoader:Init(info.lingshouTempId1, 0, 0)
    self.rightTextrueLoader:Init(info.lingshouTempId2, 0, 0)

    UIEventListener.Get(self.leftBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnLeftLingShouClick(go) end)
    UIEventListener.Get(self.rightBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnRightLingShouClick(go) end)
end

function CLuaLingShouChosenWnd:OnLeftLingShouClick( go) 
    local info = CLuaLingShouMgr.m_GuideInfo
    Gac2Gas.PlayerSelectLingShou(info.place, info.pos, info.itemId, info.lingshouTempId1)
    -- self:Close()
    CUIManager.CloseUI(CLuaUIResources.LingShouChosenWnd)
end
function CLuaLingShouChosenWnd:OnRightLingShouClick( go) 
    local info = CLuaLingShouMgr.m_GuideInfo
    Gac2Gas.PlayerSelectLingShou(info.place, info.pos, info.itemId, info.lingshouTempId2)
    -- self:Close()
    CUIManager.CloseUI(CLuaUIResources.LingShouChosenWnd)
end

return CLuaLingShouChosenWnd
