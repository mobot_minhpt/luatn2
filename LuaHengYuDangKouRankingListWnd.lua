local GameObject = import "UnityEngine.GameObject"

local UILabel = import "UILabel"

local QnTableView = import "L10.UI.QnTableView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local CGuildMgr=import "L10.Game.CGuildMgr"
local QnTableItem = import "L10.UI.QnTableItem"

LuaHengYuDangKouRankingListWnd = class()
RegistClassMember(LuaHengYuDangKouRankingListWnd, "m_GuildId")    -- 自己的帮会id
RegistClassMember(LuaHengYuDangKouRankingListWnd, "m_RankReward") -- 排名与加成换算表
RegistClassMember(LuaHengYuDangKouRankingListWnd, "m_BonusBg")    -- 加成对应的底色表
RegistClassMember(LuaHengYuDangKouRankingListWnd, "m_DataCount")  -- 排行榜个数
RegistClassMember(LuaHengYuDangKouRankingListWnd, "m_DataList")   -- 排行榜数据
RegistClassMember(LuaHengYuDangKouRankingListWnd, "m_Highlight")  -- 高亮对象列表

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouRankingListWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaHengYuDangKouRankingListWnd, "Tips", "Tips", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouRankingListWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    UIEventListener.Get(self.Tips).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("HengYuDangKou_RankListTips")
    end)
end
-- 排行换算加成
function LuaHengYuDangKouRankingListWnd:GetBonusByRank(rank)
    if not rank then return '-', self.m_BonusBg[#self.m_BonusBg] end
    for i, v in ipairs(self.m_RankReward) do
        if i > #self.m_BonusBg then break end
        if rank <= v.rank then
            return SafeStringFormat3("%d%%", math.floor(v.bonus*100)), self.m_BonusBg[i]
        end
    end
    return '-', self.m_BonusBg[#self.m_BonusBg]
end
-- 显示排行榜index项
-- {guildId, guildName, monsterGrade, killCount, rankPercent}
function LuaHengYuDangKouRankingListWnd:InitItem(tf, index)
    index = index + 1
    tf:Find("MyGuild").gameObject:SetActive(index == 1)
    local tbl = self.m_DataList[index]
    local nameLabel = tf:Find("Name"):GetComponent(typeof(UILabel))
    local levelLabel = tf:Find("Level"):GetComponent(typeof(UILabel))
    local countLabel = tf:Find("Count"):GetComponent(typeof(UILabel))
    local rankLabel = tf:Find("Ranking"):GetComponent(typeof(UILabel))
    local awardLabel = tf:Find("Award"):GetComponent(typeof(UILabel))
    nameLabel.text = tostring(tbl[2] and tbl[2] or '-')
    levelLabel.text = tostring(tbl[3] and tbl[3] or '-')
    countLabel.text = tostring(tbl[4] and tbl[4] or '-')
    local percent = tbl[5] and math.floor(tbl[5] * 1000) / 10 or 0
    rankLabel.text = tbl[5] and SafeStringFormat3(LocalString.GetString("前%.1f%%"), percent) or '-'
    local award, color = self:GetBonusByRank(tbl[5])
    awardLabel.text = award
    tf:Find("BgLevel"):GetComponent(typeof(UITexture)).color = color
    -- 本帮字体颜色
    if index == 1 then
        color = NGUIText.ParseColor24("00ff60", 0)
        nameLabel.color = color
        levelLabel.color = color
        countLabel.color = color
        rankLabel.color = color
        awardLabel.color = color
    end
    -- 奇偶列不同背景
    tf:Find("Bg"):GetComponent(typeof(UISprite)).spriteName = (index % 2 == 0 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
    -- 储存Highlight对象
    self.m_Highlight = self.m_Highlight and self.m_Highlight or {}
    self.m_Highlight[index] = tf:Find("Highlight").gameObject
    self.m_Highlight[index]:SetActive(false)
end
-- 更新排行榜
function LuaHengYuDangKouRankingListWnd:UpdateRankList(ranklist)
    if not ranklist then return end
    self.m_DataList = ranklist
    self.m_DataCount = #self.m_DataList
    -- 查找本帮的下标
    local index = -1
    for i = 1, self.m_DataCount do
        if self.m_DataList[i][1] and self.m_DataList[i][1] == self.m_GuildId then
            index = i
            break
        end
    end
    -- 没有本帮，在末尾加一个本帮
    if index < 0 then
        table.insert(self.m_DataList, {self.m_GuildId})
        self.m_DataCount = self.m_DataCount + 1
        index = self.m_DataCount
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
            self.m_DataList[index][2] = CGuildMgr.Inst.m_GuildName
        end
    end
    -- 将本帮换到列表的第一位
    while index > 1 do
        self.m_DataList[index], self.m_DataList[index - 1] = self.m_DataList[index - 1], self.m_DataList[index]
        index = index - 1
    end
    -- for i = 1, 10 do
    --     table.insert(self.m_DataList, { 1, i, math.random(1, 100), math.random(1, 100), self.m_RankReward[math.floor((i+1)/2)].rank })
    --     self.m_DataCount = self.m_DataCount + 1
    -- end
    -- table.sort(self.m_DataList, function(a, b)
    --     return a[1] == self.m_GuildId or b[1] ~= self.m_GuildId and a[5] < b[5]
    -- end)
    self.TableView:ReloadData(true, false)
end
function LuaHengYuDangKouRankingListWnd:Init()
    self.m_GuildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or 0
    self.m_RankReward = {}
    -- 读取排名与加成换算表
    HengYuDangKou_RankReward.ForeachKey(function(key)
        local data = HengYuDangKou_RankReward.GetData(key)
        table.insert(self.m_RankReward, { rank = data.Rank, bonus = data.Bonus })
    end)
    table.sort(self.m_RankReward, function(a, b)
        return a.rank < b.rank
    end)
    -- 加成底色表
    self.m_BonusBg = {
        NGUIText.ParseColor24("63679f", 0), -- 紫
        NGUIText.ParseColor24("696385", 0), -- 红
        NGUIText.ParseColor24("536fa5", 0), -- 蓝
        NGUIText.ParseColor24("758a95", 0), -- 黄
        NGUIText.ParseColor24("667da0", 0), -- 白
    }
    -- 绑定数据
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() return self.m_DataCount end,
        function(item, i) self:InitItem(item.transform, i) end
    )
    -- 绑定列表点击事件
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        row = row + 1
        for i = 1, self.m_DataCount do
            self.m_Highlight[i]:SetActive(i == row)
        end
    end)
    -- 显示排行榜
    self:OnReplyHengYuDangKouRankList()
end
function LuaHengYuDangKouRankingListWnd:OnReplyHengYuDangKouRankList()
    self:UpdateRankList(LuaHengYuDangKouMgr.m_RankList)
end
function LuaHengYuDangKouRankingListWnd:OnEnable()
    g_ScriptEvent:AddListener("OnReplyHengYuDangKouRankList", self, "OnReplyHengYuDangKouRankList")
end
function LuaHengYuDangKouRankingListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnReplyHengYuDangKouRankList", self, "OnReplyHengYuDangKouRankList")
    LuaHengYuDangKouMgr.m_RankList = nil -- 清空数据
end

--@region UIEvent

--@endregion UIEvent

