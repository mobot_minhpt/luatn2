-- Auto Generated!!
local CGuidelineDetailListItem = import "L10.UI.CGuidelineDetailListItem"
local CLevelUpGuideDetailView = import "L10.UI.CLevelUpGuideDetailView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Guide_LevelUpGuide = import "L10.Game.Guide_LevelUpGuide"
local LevelUpGuideData = import "L10.UI.LevelUpGuideData"
CLevelUpGuideDetailView.m_Init_CS2LuaHook = function (this, section) 
    CommonDefs.ListClear(this.dataList)
    this.tableView:Clear()
    Guide_LevelUpGuide.ForeachKey(DelegateFactory.Action_object(function (key) 
        local guideData = Guide_LevelUpGuide.GetData(key)
        if not (guideData.MinPlayerLevel > section.maxLevel or guideData.MaxPlayerLevel < section.minLevel) then
            CommonDefs.ListAdd(this.dataList, typeof(LevelUpGuideData), CreateFromClass(LevelUpGuideData, guideData))
        end
    end))
    this:SortDataList(this.dataList, section.minLevel, section.maxLevel)
    this.tableView.m_DataSource = this
    this.tableView:ReloadData(false, false)
end
CLevelUpGuideDetailView.m_SortDataList_CS2LuaHook = function (this, list, sectionMinLevel, secionMaxLevel) 
    if list == nil or list.Count == 0 then
        return
    end
    CommonDefs.ListSort1(list, typeof(LevelUpGuideData), DelegateFactory.Comparison_LevelUpGuideData(function (data1, data2) 
        local val1 = (data1.minLevel >= sectionMinLevel and data1.minLevel <= secionMaxLevel) and 1 or - 1
        local val2 = (data2.minLevel >= sectionMinLevel and data2.minLevel <= secionMaxLevel) and 1 or - 1
        local ret = NumberCompareTo(val2, val1)
        if ret == 0 then
            return NumberCompareTo(data1.id, data2.id)
        else
            return ret
        end
    end))
end
CLevelUpGuideDetailView.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(this.tableView:GetFromPool(0), typeof(CGuidelineDetailListItem))
    if item ~= nil then
        if row >= 0 and row < this.dataList.Count then
            item:Init(this.dataList[row])
        end
    end
    return item
end
