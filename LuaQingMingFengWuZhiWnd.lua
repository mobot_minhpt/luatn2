local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local TweenScale = import "TweenScale"
local QnButton = import "L10.UI.QnButton"
local DelegateFactory = import "DelegateFactory"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

CLuaQingMingFengWuZhiWnd = class()

RegistChildComponent(CLuaQingMingFengWuZhiWnd, "TableView", QnTableView)
RegistChildComponent(CLuaQingMingFengWuZhiWnd, "RewardButton", QnButton)
RegistChildComponent(CLuaQingMingFengWuZhiWnd, "MsgLabel", UILabel)
RegistClassMember(CLuaQingMingFengWuZhiWnd, "RewardButtonLabel")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_TaskIds")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_FengWuIds")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_TaskInfos")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_IsFinishAll")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_CollectionItem")

RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_CollectedCount")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_NeedCollectCount")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_MaxPlayTimes")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_RewardTimes")

RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_ItemStatus")
RegistClassMember(CLuaQingMingFengWuZhiWnd, "m_TaskToIndex")

function CLuaQingMingFengWuZhiWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function CLuaQingMingFengWuZhiWnd:OnDisable()
    CLuaQingMing2020Mgr.CollectTaskId = nil
    g_ScriptEvent:RemoveListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function CLuaQingMingFengWuZhiWnd:Awake()
    self.RewardButtonLabel = self.RewardButton.transform:Find("Label"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.RewardButton.gameObject).onClick  =DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.QingMing2020CollectionReward()
    end)
    
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_FengWuIds
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
end

function CLuaQingMingFengWuZhiWnd:Init()
    self.m_TaskIds = {}
    self.m_TaskInfos = {}
    self.m_FengWuIds = {}
    self.m_CollectionItem = {}
    self.m_ItemStatus = {}
    self.m_TaskToIndex = {}
    self.m_CollectedCount = 0
    self.m_NeedCollectCount = 12
    self.m_MaxPlayTimes = 3
    self.m_IsFinishAll = false
    self.RewardButton.Enabled = false
    self.MsgLabel.text = SafeStringFormat3(LocalString.GetString("当前风物志收集进度[FFEB7B]%d/%d[-]，今日可完成[FFEB7B]%d[-]次清明风物志收集。"), self.m_CollectedCount,self.m_NeedCollectCount,self.m_MaxPlayTimes)   

    QingMing2019_QingMing2020CollectionReward.ForeachKey(function (key) 
        table.insert(self.m_FengWuIds, key)
        table.insert(self.m_ItemStatus, "none")
    end)

    self.TableView:ReloadData(false, false)
    local taskIdStr = QingMing2019_QingMing2020Collection.GetData().TaskIds
    local res = {}
    for taskId in string.gmatch(QingMing2019_QingMing2020Collection.GetData().TaskIds, "([^;]+);?") do
        table.insert(self.m_TaskIds,taskId)
    end

    Gac2Gas.QueryTempPlayDataInUD(CLuaQingMing2020Mgr.eQingMing2020CollectionData)    
end

function CLuaQingMingFengWuZhiWnd:InitItem(item, row)
    local fengwuId = self.m_FengWuIds[row + 1]
    local data = QingMing2019_QingMing2020CollectionReward.GetData(fengwuId)
    if not data then return end

    local taskId = data.TaskID
    local taskFinished = self.m_TaskInfos[taskId]

    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local icon = item.transform:Find("Collection/Icon"):GetComponent(typeof(CUITexture))
    local stamp = item.transform:Find("Stamp").gameObject
    local tween = item.transform:Find("Stamp"):GetComponent(typeof(TweenScale))
    local unStamp = item.transform:Find("UnStamp").gameObject 
    local lock = item.transform:Find("UnStamp/Lock").gameObject
    --local nameLabel = item.transform:Find("NameLabel").gameObject
    local collection = item.transform:Find("Collection").gameObject
    local unCollectLabel = item.transform:Find("UnStamp/Label").gameObject
    
    nameLabel.text = Item_Item.GetData(fengwuId).Name
    local iconName = data.Icon
    icon:LoadMaterial(iconName)

    self.m_TaskToIndex[taskId] = row + 1

    if not self.m_CollectionItem[taskId] then
        self.m_CollectionItem[taskId] = item
    end

    local status = self.m_ItemStatus[row + 1]
    if status == "collect" then
        tween.enabled = true       
        stamp:SetActive(true)
        tween:PlayForward()
        nameLabel.gameObject:SetActive(true)
        collection:SetActive(true)
        unStamp:SetActive(false)
    elseif status == "collected" then
        tween.enabled = false       
        stamp:SetActive(true)
        nameLabel.gameObject:SetActive(true)
        collection:SetActive(true)
        unStamp:SetActive(false)
    elseif status == "none" then      
        stamp:SetActive(false)
        nameLabel.gameObject:SetActive(false)
        collection:SetActive(false)
        unStamp:SetActive(true)
    end
    
end

function CLuaQingMingFengWuZhiWnd:OnQueryTempPlayDataInUDResult(key, ud, data)
    if key ~= CLuaQingMing2020Mgr.eQingMing2020CollectionData then
        return
    end
    if not data then 
        return 
    end
    
    local isComplete = true
	for k, taskId in pairs(self.m_TaskIds) do
        if CommonDefs.DictContains(data, typeof(String), tostring(taskId)) then
            local isFinished = data[taskId]
            if isFinished and tonumber(taskId) == CLuaQingMing2020Mgr.CollectTaskId then
                --刚刚完成
                CLuaQingMing2020Mgr.FinishedTaskIds[taskId] = true
                self:SetCollectionStatus(taskId,"collect")
            elseif isFinished then
                --以前完成
                self:SetCollectionStatus(taskId,"collected")
            else
                --还没完成
                self:SetCollectionStatus(taskId,"none")
            end
            if not isFinished then
                isComplete = false
            else
                self.m_CollectedCount = self.m_CollectedCount + 1
            end
        else
            isComplete = false
		end
    end

    self.m_RewardTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(CLuaQingMing2020Mgr.eQingMing2020CollectionRewardTimes)
    if self.m_RewardTimes > 0 then
        self.RewardButton.Enabled = false
        self.RewardButtonLabel.text = LocalString.GetString("已领奖")
    elseif isComplete then
        self.RewardButton.Enabled = true
        self.RewardButtonLabel.text = LocalString.GetString("领奖")
    else
        self.RewardButton.Enabled = false
        self.RewardButtonLabel.text = LocalString.GetString("全部解锁可领奖")
    end

    self.MsgLabel.text = SafeStringFormat3(LocalString.GetString("当前风物志收集进度[FFEB7B]%d/%d[-]，今日可完成[FFEB7B]%d[-]次清明风物志收集。"), self.m_CollectedCount,self.m_NeedCollectCount,self.m_MaxPlayTimes)   
    self.TableView:ReloadData(false, false)
end

function CLuaQingMingFengWuZhiWnd:SetCollectionStatus(taskId,status)   
    local index = self.m_TaskToIndex[tonumber(taskId)]
    if index then
        self.m_ItemStatus[index] = status
    end
end

function CLuaQingMingFengWuZhiWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == CLuaQingMing2020Mgr.eQingMing2020CollectionRewardTimes then
        self.m_RewardTimes = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(CLuaQingMing2020Mgr.eQingMing2020CollectionRewardTimes)
        if self.m_RewardTimes > 0 then
            self.RewardButton.Enabled = false
            self.RewardButtonLabel.text = LocalString.GetString("已领奖")
        end
    end
end

