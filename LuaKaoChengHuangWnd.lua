--import
local GameObject			= import "UnityEngine.GameObject"
local Vector3			    = import "UnityEngine.Vector3"

local UIWidget              = import "UIWidget"
local UIPanel               = import "UIPanel"
local LuaTweenUtils         = import "LuaTweenUtils"
local SoundManager          = import "SoundManager"

local CUITexture            = import "L10.UI.CUITexture"
local CUIManager            = import "L10.UI.CUIManager"

local CClientMainPlayer     = import "L10.Game.CClientMainPlayer"

--define
CLuaKaoChengHuangWnd = class()

CLuaKaoChengHuangWnd.TaskID = 0

function CLuaKaoChengHuangWnd.Open(taskid)
    CLuaKaoChengHuangWnd.TaskID = taskid
    CUIManager.ShowUI(CLuaUIResources.KaoChengHuangWnd)
end

--RegistChildComponent
RegistChildComponent(CLuaKaoChengHuangWnd, "PlayerIcon",	CUITexture)
RegistChildComponent(CLuaKaoChengHuangWnd, "PlayerName",	UIWidget)
RegistChildComponent(CLuaKaoChengHuangWnd, "Res1",		    UIWidget)
RegistChildComponent(CLuaKaoChengHuangWnd, "Res2",		    UIWidget)
RegistChildComponent(CLuaKaoChengHuangWnd, "Res3",		    UIWidget)
RegistChildComponent(CLuaKaoChengHuangWnd, "Res4",		    UIWidget)
RegistChildComponent(CLuaKaoChengHuangWnd, "Res5",		    UIWidget)
RegistChildComponent(CLuaKaoChengHuangWnd, "Res6",		    UIWidget)
RegistChildComponent(CLuaKaoChengHuangWnd, "Content",		UIPanel)
RegistChildComponent(CLuaKaoChengHuangWnd, "Header",	    GameObject)
RegistChildComponent(CLuaKaoChengHuangWnd, "Rank1",	        GameObject)
RegistChildComponent(CLuaKaoChengHuangWnd, "Rank2",	        GameObject)
RegistChildComponent(CLuaKaoChengHuangWnd, "Rank3",	        GameObject)

--RegistClassMember
RegistClassMember(CLuaKaoChengHuangWnd, "m_ResCtrls")
RegistClassMember(CLuaKaoChengHuangWnd, "m_Ranks")
RegistClassMember(CLuaKaoChengHuangWnd, "m_Step")

--@region flow function

function CLuaKaoChengHuangWnd:Init()
    if CClientMainPlayer.Inst == nil then return end
    self.PlayerName.text = CClientMainPlayer.Inst.Name
    self.PlayerIcon:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
    self.m_ResCtrls = {self.Res1,self.Res2,self.Res3,self.Res4,self.Res5,self.Res6}
    self.m_Ranks = {self.Rank1,self.Rank2,self.Rank3}
    self.m_Step = self:GetStep()
    if self.m_Step == 1 then
        local v4 = self.Content.clipOffset
        v4.x = 1600
        self.Content.clipOffset = v4
        self.Header.transform.localPosition.x = 1600
        self:PlayOpenAnim()
    else
        for i=1,self.m_Step-1 do
            self:ShowRaw(i,false)
        end
        self:ShowRaw(self.m_Step,true)
    end
end

function CLuaKaoChengHuangWnd:OnEnable()
end

function CLuaKaoChengHuangWnd:OnDisable()
end

--@endregion

function CLuaKaoChengHuangWnd:PlayOpenAnim()
    --local height = 1000
    self.m_animtween = LuaTweenUtils.TweenFloat(1600,150,1.5, function (val)
        local pos = self.Header.transform.localPosition
        pos.x = val
        self.Header.transform.localPosition = pos
		local v4 = self.Content.clipOffset
		v4.x = val
		self.Content.clipOffset = v4
		if val <= 150 then
            self.m_animtween = nil
            self:ShowRaw(1,true)
		end
    end)
end

function CLuaKaoChengHuangWnd:GetStep()
    local taskid = CLuaKaoChengHuangWnd.TaskID
    if taskid == 22204436 then return 1 end
    if taskid == 22204443 then return 2 end
    if taskid == 22204460 then return 3 end
    if taskid == 22204468 then return 4 end
    if taskid == 22204476 then return 5 end
end

function CLuaKaoChengHuangWnd:ShowRaw(index,needanim)
    local ctrl = self.m_ResCtrls[index]
    if needanim then
        if index > 0 and index <= 3 then
            local mask = ctrl.transform:Find("LossPoint")
            mask.localScale = Vector3.zero
            local func = function()
                LuaTweenUtils.TweenScale(mask,Vector3(1.2,1.2,1),Vector3.one,0.4)
            end
            LuaTweenUtils.TweenAlpha(ctrl.transform,0,1,0.8,func)
            Gac2Gas.RequestFinishDiYuZhuXianTask(CLuaKaoChengHuangWnd.TaskID)
        elseif index == 4 then --第4个任务
            LuaTweenUtils.TweenAlpha(ctrl.transform,0,1,0.8)
            Gac2Gas.RequestFinishDiYuZhuXianTask(CLuaKaoChengHuangWnd.TaskID)
        elseif index == 5 then --第5个任务会显示总分
            local func = function()
                self:ShowRaw(6,true)
            end
            LuaTweenUtils.TweenAlpha(ctrl.transform,0,1,0.8,func)
            Gac2Gas.RequestFinishDiYuZhuXianTask(CLuaKaoChengHuangWnd.TaskID)
        else --显示总分之后显示排行
            local func = function()
                self:ShowRank(1)
            end
            LuaTweenUtils.TweenAlpha(ctrl.transform,0,1,0.8,func)
        end
    else
        ctrl.alpha = 1
    end
end

function CLuaKaoChengHuangWnd:ShowRank(index)
    local ctrl = self.m_Ranks[index]
    ctrl:SetActive(true)
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/UI_YinSiKeKao",Vector3.zero, nil, 0)
    local tween = LuaTweenUtils.TweenScale(ctrl.transform,Vector3(1.2,1.2,1),Vector3.one,0.4)
    if index >= 1 and index < 3 then
        local nextfunc = function()
            self:ShowRank(index+1)
        end
        LuaTweenUtils.OnComplete(tween,nextfunc)
    end
end
