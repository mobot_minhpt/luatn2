local UILabel = import "UILabel"
local Object=import "System.Object"
local GameObject = import "UnityEngine.GameObject"
local CChargeMgr = import "L10.Game.CChargeMgr"
local DelegateFactory  = import "DelegateFactory"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CWelfareMgr = import "L10.UI.CWelfareMgr"

LuaWelfareGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWelfareGiftWnd, "ExchangeBtn", "ExchangeBtn", GameObject)
RegistChildComponent(LuaWelfareGiftWnd, "BuyMonthCardBtn", "BuyMonthCardBtn", GameObject)
RegistChildComponent(LuaWelfareGiftWnd, "Top", "Top", GameObject)
RegistChildComponent(LuaWelfareGiftWnd, "VNTop", "VNTop", GameObject)
RegistChildComponent(LuaWelfareGiftWnd, "VNTopLabel", "VNTopLabel", UILabel)

--@endregion RegistChildComponent end

function LuaWelfareGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExchangeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExchangeBtnClick()
	end)


	
	UIEventListener.Get(self.BuyMonthCardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyMonthCardBtnClick()
	end)


    --@endregion EventBind end
end

function LuaWelfareGiftWnd:Init()
	self.Top.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
	self.VNTop.gameObject:SetActive(CommonDefs.IS_VN_CLIENT)
	self.VNTopLabel.text = g_MessageMgr:FormatMessage("VN_WelfareGiftWnd_TopLabel")
end

function LuaWelfareGiftWnd:OnEnable()
	g_ScriptEvent:AddListener("MonthCardInfoChange", self, "OnMonthCardInfoChange")
end

function LuaWelfareGiftWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MonthCardInfoChange", self, "OnMonthCardInfoChange")
end

function LuaWelfareGiftWnd:OnMonthCardInfoChange()
	if not CClientMainPlayer.Inst then return end
	local cardInfo = CClientMainPlayer.Inst.ItemProp.MonthCard
	if cardInfo ~= nil and cardInfo.BuyTime ~= 0 then
        local leftDay = CChargeWnd.MonthCardLeftDay(cardInfo)
        if leftDay > 0 then
			CUIManager.CloseUI("WelfareGiftWnd")
			LuaWelfareMgr.m_MainWelfareWndShowMonth = true
			CUIManager.ShowUI("WelfareWnd")
        end
    end
end

--@region UIEvent

function LuaWelfareGiftWnd:OnExchangeBtnClick()
	CUIManager.ShowUI(CUIResources.YuanbaoExchangeWnd)
end

function LuaWelfareGiftWnd:OnBuyMonthCardBtnClick()
	local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
	Gac2Gas.RequestRecordClientLog("WelfareGiftWnd_BuyMonthCard", MsgPackImpl.pack(dict))
	CChargeMgr.Inst:BuyMonthCardForMyself()
end


--@endregion UIEvent

