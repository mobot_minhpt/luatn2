local UILabel = import "UILabel"
local UISlider = import "UISlider"
local UISprite = import "UISprite"
local TweenRotation = import "TweenRotation"
local NGUITools = import "NGUITools"
local LuaUtils = import "LuaUtils"
local DelegateFactory = import "DelegateFactory"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local QnTableView = import "L10.UI.QnTableView"
local CUIManager = import "L10.UI.CUIManager"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local EnumHouseCompetitionStage = import "L10.Game.EnumHouseCompetitionStage"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"

CLuaHouseCompetitionFanPaiZiWnd = class()

RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_Texture")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_NameLab")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_DescLab")

RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_NumLab")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_AddChancesBtn")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_BtnSprite")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_TableView")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_DataList")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_ExtraDataList")

RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_SliderLab")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_SliderFore")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_Slider")

RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_RewardTemp")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_Rewards")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_RewardsData")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_RewardsIcon")

RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_Chances")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_Progress")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_ProgressMax")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_ChancesItemId")

RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_Ticks")
RegistClassMember(CLuaHouseCompetitionFanPaiZiWnd, "m_TickRunning")
function CLuaHouseCompetitionFanPaiZiWnd:Awake()
    self:InitComponents()
end

function CLuaHouseCompetitionFanPaiZiWnd:InitComponents()
    self.m_Texture      = self.transform:Find("Anchor/Offset/Left/Texture"):GetComponent(typeof(CUITexture))
    self.m_DescLab      = self.transform:Find("Anchor/Offset/Left/DescLab"):GetComponent(typeof(UILabel))

    self.m_NumLab       = self.transform:Find("Anchor/Offset/Right/TopRight/NumLab"):GetComponent(typeof(UILabel))
    self.m_AddChancesBtn= self.transform:Find("Anchor/Offset/Right/TopRight/ToAddChancesBtn"):GetComponent(typeof(QnButton))
    self.m_BtnSprite    = self.transform:Find("Anchor/Offset/Right/TopRight/ToAddChancesBtn"):GetComponent(typeof(UISprite))
    self.m_TableView    = self.transform:Find("Anchor/Offset/Right/Content"):GetComponent(typeof(QnTableView))

    self.m_SliderFore   = self.transform:Find("Anchor/Offset/Right/Bottom/Slider/Foreground"):GetComponent(typeof(UISprite))
    self.m_SliderLab    = self.transform:Find("Anchor/Offset/Right/Bottom/ProgressLab"):GetComponent(typeof(UILabel))
    self.m_Slider       = self.transform:Find("Anchor/Offset/Right/Bottom/Slider"):GetComponent(typeof(UISlider))

    self.m_RewardTemp   = self.transform:Find("Anchor/Offset/Right/Bottom/Slider/Reward").gameObject
    self.m_Rewards      = self.transform:Find("Anchor/Offset/Right/Bottom/Slider/Rewards").gameObject
    self.m_RewardsIcon  = HouseCompetition_Setting.GetData().FanPaiZi_Reward_Icon

    self.m_Chances      = 0
    self.m_Progress     = 0
    self.m_ProgressMax  = HouseCompetition_Setting.GetData().FanPaiZi_Progress --牌的数量,额外奖励进度上限
    self.m_ChancesItemId= HouseCompetition_Setting.GetData().FanPaiZi_ItemID --翻牌道具Id

    self.m_Ticks        = {}
    self.m_TickRunning  = true
end

function CLuaHouseCompetitionFanPaiZiWnd:Init()
    self.m_Texture:LoadMaterial(HouseCompetition_Setting.GetData().FanPaiZi_Zhuangshiwu_Icon)
    self.m_DescLab.text = g_MessageMgr:FormatMessage("HOUSE_COMPETITION_FANPAIZI_REWARD_DESC")


    self.m_RewardTemp:SetActive(false)
    LuaUtils.SetLocalPositionZ(self.m_TableView.transform, 100) --防止旋转图片因为shader变灰

    self.m_ExtraDataList = {}
    local rewardsData = HouseCompetition_Setting.GetData().FanPaiZi_Reward
    self.m_RewardsData = rewardsData
    for i = 0, rewardsData.Length - 1 do
        self:InitReward(rewardsData[i], i)
    end

    self.m_Chances = CItemMgr.Inst:GetItemCount(self.m_ChancesItemId)
    self.m_NumLab.text = self.m_Chances
    self.m_AddChancesBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:ToAddChancesOrToVote()
    end)
    if CHouseCompetitionMgr.Inst:GetCurrentStage() == EnumHouseCompetitionStage.eJuesai then
        self.m_AddChancesBtn.Text = LocalString.GetString("前往投票")
        self.m_BtnSprite.spriteName = "common_btn_01_yellow"
    end

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,
        function(item, index)
            self:InitItem(item, self.m_DataList[index + 1])
        end
    )

    self.m_DataList = {}
    for i = 1, self.m_ProgressMax do
        local itemInfo = {}
        itemInfo.Index = i
        table.insert(self.m_DataList, itemInfo)
    end

    --self:RefreshRewards() --回调时再刷新，免得刷两次
    --self.m_TableView:ReloadData(true, false)

    Gac2Gas.QueryTempPlayDataInUD(EnumTempPlayDataKey_lua.eHouseCompetitionFanPaiZiData)
end

function CLuaHouseCompetitionFanPaiZiWnd:InitItem(item, info)
    local icon          = item.transform:Find("CardFront/Icon"):GetComponent(typeof(CUITexture))
    local cardBack      = item.transform:Find("CardBack"):GetComponent(typeof(CUITexture))
    local cardFront     = item.transform:Find("CardFront"):GetComponent(typeof(CUITexture))
    local BackTween     = item.transform:Find("CardBack"):GetComponent(typeof(TweenRotation))
    local FrontTween    = item.transform:Find("CardFront"):GetComponent(typeof(TweenRotation))

    local index = info.Index
    local cardToReward = info.CardToReward
    info.Icon = icon
    info.BackTween = BackTween

    FrontTween.enabled = false
    BackTween.enabled = false

    if cardToReward ~= nil and cardToReward ~= 0 then
        cardFront.gameObject:SetActive(true)
        icon:LoadMaterial(Item_Item.GetData(cardToReward).Icon)
    end

    CommonDefs.AddEventDelegate(BackTween.onFinished, DelegateFactory.Action(function()
        cardFront.gameObject:SetActive(true)
        FrontTween:Play(true)
    end))

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (btn)
        if self.m_Chances == 0 and not cardFront.gameObject.activeSelf then --没被翻且次数为0
            local msg = g_MessageMgr:FormatMessage("HOUSE_COMPETITION_FANPAIZI_ADDCHOICE_NOTICE")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function() 
                self:ToAddChancesOrToVote()
            end), nil, nil, nil, false)
            return
        end
        Gac2Gas.HouseCompetitionTrySelectCard(index)
    end)
end

function CLuaHouseCompetitionFanPaiZiWnd:ToAddChancesOrToVote()
    --决赛的话禁用增加次数界面的入口
    if CHouseCompetitionMgr.Inst:GetCurrentStage() ~= EnumHouseCompetitionStage.eJuesai then
        CUIManager.ShowUI(CLuaUIResources.HouseCompetitionFanPaiZiAddChancesWnd)
    else
        CLuaWebBrowserMgr.OpenHouseActivityUrl("JiaYuan2022Index")
    end
end

function CLuaHouseCompetitionFanPaiZiWnd:InitReward(rewardData, index)
    local rewardProgress = rewardData[0]
    local rewardId = rewardData[1]
    local reward = NGUITools.AddChild(self.m_Rewards, self.m_RewardTemp)

    reward.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(self.m_RewardsIcon[index])

    local xOffset = rewardProgress / self.m_ProgressMax * self.m_SliderFore.width
    LuaUtils.SetLocalPositionX(reward.transform, xOffset)

    reward:SetActive(true)
end

function CLuaHouseCompetitionFanPaiZiWnd:RefreshRewards()
    self.m_SliderLab.text = System.String.Format("({0}/{1})", self.m_Progress , self.m_ProgressMax)
    self.m_Slider.value = self.m_Progress / self.m_ProgressMax
    for i = 0, self.m_RewardsData.Length - 1 do
        local rewardData = self.m_RewardsData[i]
        local rewardProgress = rewardData[0]
        local rewardId = rewardData[1]

        local reward = self.m_Rewards.transform:GetChild(i)
        local icon = reward.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        local sprite = reward:GetComponent(typeof(UISprite))
        local btn = reward:GetComponent(typeof(QnButton))

        if self.m_Progress < rewardProgress then
            sprite.spriteName = "cppuppetinfownd_button"
            btn.OnClick = DelegateFactory.Action_QnButton(function (btn)
                CItemInfoMgr.ShowLinkItemTemplateInfo(rewardId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
        else
            sprite.spriteName = "cppuppetinfownd_button_highlight"
            UnRegisterTick(self.m_Ticks[i])
            if self.m_ExtraDataList[rewardProgress] ~= rewardId then
                self.m_Ticks[i] = self:DoShakeRotation(icon)
            end
            btn.OnClick = DelegateFactory.Action_QnButton(function (btn)
                Gac2Gas.HouseCompetitionTryExtraCardReward(rewardProgress)
            end)
        end
    end
end

function CLuaHouseCompetitionFanPaiZiWnd:DoShakeRotation(target)
    return RegisterTick(function () 
        if self.m_TickRunning then
            local tweener = LuaTweenUtils.TweenRotationZ(target.transform, 15, 0.15)
            LuaTweenUtils.SetEase(tweener, Ease.OutQuad)
        end
        RegisterTickOnce(function () 
            if self.m_TickRunning then
                local tweener = LuaTweenUtils.TweenRotationZ(target.transform, -15, 0.2)
                LuaTweenUtils.SetEase(tweener, Ease.OutQuad)
            end
        end, 150)

        RegisterTickOnce(function () 
            if self.m_TickRunning then
                local tweener = LuaTweenUtils.TweenRotationZ(target.transform, 15, 0.2)
                LuaTweenUtils.SetEase(tweener, Ease.OutQuad)
            end
        end, 350)

        RegisterTickOnce(function () 
            if self.m_TickRunning then
                local tweener = LuaTweenUtils.TweenRotationZ(target.transform, 0, 0.15)
                LuaTweenUtils.SetEase(tweener, Ease.OutQuad)
            end
        end, 550)
    end, 1000)
end

function CLuaHouseCompetitionFanPaiZiWnd:ResetChances()
    self.m_Chances = CItemMgr.Inst:GetItemCount(self.m_ChancesItemId)
    self.m_NumLab.text = self.m_Chances
end

function CLuaHouseCompetitionFanPaiZiWnd:OnQueryTempPlayDataInUDResult(key, ud, data)
    if key ~= EnumTempPlayDataKey_lua.eHouseCompetitionFanPaiZiData then
        return
    end

    if data then
        self.m_DataList = {}
        self.m_ExtraDataList = {}

        local count = 0
        for i = 0, self.m_ProgressMax-1 do
            local itemInfo = {}
            itemInfo.Index = i+1
            itemInfo.CardToReward = data.CardToReward[i]
            self.m_DataList[i+1] = itemInfo

            if data.CardToReward[i] ~= nil and data.CardToReward[i] ~= 0 then
                count = count + 1
            end

            self.m_ExtraDataList[i+1] = data.ExtraReward[i]
        end
        self.m_Progress = count --获取下额外奖励进度
    end

    self.m_TableView:ReloadData(true, false) --没数据使用默认数据刷下
    self:RefreshRewards()
end

function CLuaHouseCompetitionFanPaiZiWnd:OnHouseCompetitionTrySelectCardResult(idx, result, addType, templateId)
    if result == true then
        self.m_DataList[idx].Icon:LoadMaterial(Item_Item.GetData(templateId).Icon)
        self.m_DataList[idx].BackTween:Play(true)

        self.m_Progress = self.m_Progress + 1
        self:RefreshRewards()
    end
end

function CLuaHouseCompetitionFanPaiZiWnd:OnHouseCompetitionTryExtraCardRewardResult(times, result, addType, templateId)
    if result == true then
        self.m_ExtraDataList[times] = templateId
        self:RefreshRewards()
        if times == self.m_ProgressMax then 
             CUIManager.ShowUI(CLuaUIResources.HouseCompetitionFanPaiZiRewardWnd)
        end
    end
end

function CLuaHouseCompetitionFanPaiZiWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "ResetChances")
    g_ScriptEvent:AddListener("SetItemAt", self, "ResetChances")
    g_ScriptEvent:AddListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:AddListener("HouseCompetitionTrySelectCardResult", self, "OnHouseCompetitionTrySelectCardResult")
    g_ScriptEvent:AddListener("HouseCompetitionTryExtraCardRewardResult", self, "OnHouseCompetitionTryExtraCardRewardResult")
end

function CLuaHouseCompetitionFanPaiZiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "ResetChances")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "ResetChances")
    g_ScriptEvent:RemoveListener("QueryTempPlayDataInUDResult", self, "OnQueryTempPlayDataInUDResult")
    g_ScriptEvent:RemoveListener("HouseCompetitionTrySelectCardResult", self, "OnHouseCompetitionTrySelectCardResult")
    g_ScriptEvent:RemoveListener("HouseCompetitionTryExtraCardRewardResult", self, "OnHouseCompetitionTryExtraCardRewardResult")
end

function CLuaHouseCompetitionFanPaiZiWnd:OnDestroy()
    self.m_TickRunning = false
    for _, v in pairs(self.m_Ticks) do
		UnRegisterTick(v)
	end
	self.m_Ticks = {}
end
