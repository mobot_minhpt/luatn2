local QnTableView = import "L10.UI.QnTableView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture=import "L10.UI.CUITexture"

LuaMultipleChooseListWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMultipleChooseListWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaMultipleChooseListWnd, "ClearBtn", "ClearBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaMultipleChooseListWnd,"m_MultipleRoot")

function LuaMultipleChooseListWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ClearBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearBtnClick()
	end)

    --@endregion EventBind end
end

function LuaMultipleChooseListWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateMultipleFurnitureChoice", self, "OnUpdateMultipleFurnitureChoice")
end

function LuaMultipleChooseListWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateMultipleFurnitureChoice", self, "OnUpdateMultipleFurnitureChoice")
end

function LuaMultipleChooseListWnd:Init()
	self.m_MultipleRoot = CLuaHouseMgr.GetMultipleRoot()
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return self.m_MultipleRoot.mChildList.Count
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
    end)

	self.TableView:ReloadData(false,false)

end

function LuaMultipleChooseListWnd:InitItem(item,row)
	if not self.m_MultipleRoot or not self.m_MultipleRoot.mChildList or self.m_MultipleRoot.mChildList.Count <= row then
		return
	end

	local fur = self.m_MultipleRoot.mChildList[row]
	if not fur then
		return
	end

	local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local posLabel = item.transform:Find("PosLabel"):GetComponent(typeof(UILabel))
	local deleteBtn = item.transform:Find("DeleteBtn").gameObject

	local templateId = fur.TemplateId
	local data = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
	if data then
		if data.Type ~= EnumFurnitureType_lua.eJianzhu then
            local itemtid = data.ItemId
            local itemdata = Item_Item.GetData(itemtid)
            if itemdata ~= nil then
                icon:LoadMaterial(itemdata.Icon)
            end
        else
            icon:LoadMaterial(data.Icon)
        end
	end

	local pos = fur.Position
	posLabel.text = SafeStringFormat3("%d,%d",pos.x,pos.z)

	UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function(go)
		self.m_MultipleRoot:RemoveChild(fur.ID)
		self.TableView:ReloadData(true,true)
		g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoiceFromListWnd")
    end)
end

function LuaMultipleChooseListWnd:OnUpdateMultipleFurnitureChoice()
	self:Init()
end

--@region UIEvent

function LuaMultipleChooseListWnd:OnClearBtnClick()
	self.m_MultipleRoot:ClearChildren()
	self.TableView:ReloadData(false,true)
	g_ScriptEvent:BroadcastInLua("UpdateMultipleFurnitureChoiceFromListWnd")
end

--@endregion UIEvent

