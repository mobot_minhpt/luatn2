
local MessageWndManager = import "L10.UI.MessageWndManager"
local Input = import "UnityEngine.Input"
LuaHaoYiXingCardsComposeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "DuplicateComposeButton", "DuplicateComposeButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "DuplicateNumLabel", "DuplicateNumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "SingleComposeButton", "SingleComposeButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "SingleNumLabel", "SingleNumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "TopLabel1", "TopLabel1", UILabel)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "TopLabel2", "TopLabel2", UILabel)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "HaiTangLabel", "HaiTangLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "PetalLabel", "PetalLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "AddHaitangBtn", "AddHaitangBtn", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "AddPetalBtn", "AddPetalBtn", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "FreeCostDuplicateComposeLabel", "FreeCostDuplicateComposeLabel", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeWnd, "FreeCostSingleComposeButtonLabel", "FreeCostSingleComposeButtonLabel", GameObject)

RegistClassMember(LuaHaoYiXingCardsComposeWnd, "m_IsWaitForComposeResult")
RegistClassMember(LuaHaoYiXingCardsComposeWnd, "m_DuplicateNum")

--@endregion RegistChildComponent end
function LuaHaoYiXingCardsComposeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.DuplicateComposeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDuplicateComposeButtonClick()
	end)

	UIEventListener.Get(self.SingleComposeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSingleComposeButtonClick()
	end)
	-- UIEventListener.Get(self.DuplicateComposeButton.gameObject).onPress = LuaUtils.BoolDelegate(function (go, isPressed)
	-- 	if not isPressed then
	-- 		local currentPos = UICamera.currentTouch.pos
	-- 		local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
	-- 		local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
	-- 		for i =0,hits.Length -1 do
	-- 			if hits[i].collider.gameObject.transform == self.DuplicateComposeButton.transform then
	-- 				self:OnDuplicateComposeButtonClick()
	-- 				return
	-- 			end
	-- 		end
	-- 	end
	-- end)
	
	-- UIEventListener.Get(self.SingleComposeButton.gameObject).onPress = LuaUtils.BoolDelegate(function (go, isPressed)
	-- 	if not isPressed then
	-- 		local currentPos = UICamera.currentTouch.pos
	-- 		local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
	-- 		local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
	-- 		for i =0,hits.Length -1 do
	-- 			if hits[i].collider.gameObject.transform == self.SingleComposeButton.transform then
	-- 				self:OnSingleComposeButtonClick()
	-- 				return
	-- 			end
	-- 		end
	-- 	end
	-- end)

	UIEventListener.Get(self.AddHaitangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddHaitangBtnClick()
	end)

	UIEventListener.Get(self.AddPetalBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddPetalBtnClick()
	end)
    --@endregion EventBind end
end

function LuaHaoYiXingCardsComposeWnd:GetGuideGo(methodName)
    if methodName == "GetCloseButton" then
        return self.CloseButton
    end
end

function LuaHaoYiXingCardsComposeWnd:Init()
	self.m_DuplicateNum = 5
	self:OnUpdateData()
end

function LuaHaoYiXingCardsComposeWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncErHaTCGData",self,"OnUpdateData")
	g_ScriptEvent:AddListener("OnErHaTCGBuyHaiTangSucc",self,"OnUpdateData")
	g_ScriptEvent:AddListener("OnSyncErHaTCGComposeResult",self,"OnSyncErHaTCGComposeResult")
end

function LuaHaoYiXingCardsComposeWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncErHaTCGData",self,"OnUpdateData")
	g_ScriptEvent:RemoveListener("OnErHaTCGBuyHaiTangSucc",self,"OnUpdateData")
	g_ScriptEvent:RemoveListener("OnSyncErHaTCGComposeResult",self,"OnSyncErHaTCGComposeResult")
end

function LuaHaoYiXingCardsComposeWnd:OnSyncErHaTCGComposeResult()
	self.m_IsWaitForComposeResult = false
end

function LuaHaoYiXingCardsComposeWnd:OnUpdateData()
	self.TopLabel1.text = SafeStringFormat3(LocalString.GetString("拥有免费抽取次数：%d"),LuaHaoYiXingMgr.m_FreeTimes)
	self.TopLabel2.text = g_MessageMgr:FormatMessage("HaoYiXingCardsComposeWnd_TopInfoText", SpokesmanTCG_ErHaSetting.GetData("FreeComposeNeedHuoLi").Value)
	self.HaiTangLabel.text = LuaHaoYiXingMgr.m_HaitangCount
	self.PetalLabel.text = LuaHaoYiXingMgr.m_HaitangPieceCount
	local cost = SpokesmanTCG_ErHaSetting.GetData("ComposeCardNeedHaiTangCount").Value
	self.SingleNumLabel.text = cost
	self.DuplicateNumLabel.text = cost * (5 - LuaHaoYiXingMgr.m_FreeTimes)
	self.SingleNumLabel.gameObject:SetActive(LuaHaoYiXingMgr.m_FreeTimes < 1)
	self.DuplicateNumLabel.gameObject:SetActive(LuaHaoYiXingMgr.m_FreeTimes < self.m_DuplicateNum)
	self.FreeCostDuplicateComposeLabel.gameObject:SetActive(LuaHaoYiXingMgr.m_FreeTimes >= self.m_DuplicateNum)
	self.FreeCostSingleComposeButtonLabel.gameObject:SetActive(LuaHaoYiXingMgr.m_FreeTimes >= 1)
end

--@region UIEvent

function LuaHaoYiXingCardsComposeWnd:OnDuplicateComposeButtonClick()
	if self.m_IsWaitForComposeResult then return end
	local cost = tonumber(SpokesmanTCG_ErHaSetting.GetData("ComposeCardNeedHaiTangCount").Value) * (self.m_DuplicateNum - LuaHaoYiXingMgr.m_FreeTimes)
	if LuaHaoYiXingMgr.m_HaitangCount < cost then
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HaoYiXingCard_Haitanghua_NotEnough"), DelegateFactory.Action(function ()
			CUIManager.ShowUI(CLuaUIResources.GetHaoYiXingHaiTangWnd)
		end), nil, nil, nil, false)
		return 
	end
	self.m_IsWaitForComposeResult = true
	Gac2Gas.ErHaTCGComposeCard(true)
end

function LuaHaoYiXingCardsComposeWnd:OnSingleComposeButtonClick()
	if self.m_IsWaitForComposeResult then return end
	if  not CommonDefs.IsPCGameMode() and Input.touchCount ~= 1 then return end
	local cost = tonumber(SpokesmanTCG_ErHaSetting.GetData("ComposeCardNeedHaiTangCount").Value)
	if LuaHaoYiXingMgr.m_HaitangCount < cost and LuaHaoYiXingMgr.m_FreeTimes < 1 then
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HaoYiXingCard_Haitanghua_NotEnough"), DelegateFactory.Action(function ()
			CUIManager.ShowUI(CLuaUIResources.GetHaoYiXingHaiTangWnd)
		end), nil, nil, nil, false)
		return 
	end
	self.m_IsWaitForComposeResult = true
	Gac2Gas.ErHaTCGComposeCard(false)
end

function LuaHaoYiXingCardsComposeWnd:OnAddHaitangBtnClick()
	CUIManager.ShowUI(CLuaUIResources.GetHaoYiXingHaiTangWnd)
end

function LuaHaoYiXingCardsComposeWnd:OnAddPetalBtnClick()
	g_MessageMgr:ShowMessage("HaoYiXingCard_HowToGetPetal")
end

--@endregion UIEvent

