-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CDangongBulletAndBird = import "CDangongBulletAndBird"
local CDanGongWnd = import "L10.UI.CDanGongWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local L10 = import "L10"
local Quaternion = import "UnityEngine.Quaternion"
local Random = import "UnityEngine.Random"
local Screen = import "UnityEngine.Screen"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UIRoot = import "UIRoot"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local VectorDelegate = import "UIEventListener+VectorDelegate"
local BoxCollider = import "UnityEngine.BoxCollider"

CDanGongWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.bullet.gameObject).onDrag = MakeDelegateFromCSFunction(this.OnBulletDrag, VectorDelegate, this)
    UIEventListener.Get(this.bullet.gameObject).onPress = MakeDelegateFromCSFunction(this.OnPress, BoolDelegate, this)
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (x)
        if LuaZhuJueJuQingMgr.danGongInfo then
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Xinbai_Bird_LeavePlay"), function()
                Gac2Gas.InterruptShootBirdsInPlay()
                CUIManager.CloseUI(CUIResources.DanGongWnd)
            end, nil, LocalString.GetString("确定"), LocalString.GetString("返回"), false)
        else
            CUIManager.CloseUI(CUIResources.DanGongWnd)
        end
    end)

    -- 新白流程图弹弓玩法
    local info = LuaZhuJueJuQingMgr.danGongInfo
    if info then
        this.totalCount = info.threshold
        local boxCollider = this.birdTemplate.transform:GetComponent(typeof(BoxCollider))
        boxCollider.size = Vector3(info.birdLength, info.birdWidth, 0)
    end

    this:initBullet()
    this:launchBirds()
    this.hitCount = 0
    this:refreshShow()
end

CDanGongWnd.m_OnPress_CS2LuaHook = function (this, go, flag) 
    this.scale = UIRoot.GetPixelSizeAdjustment(this.gameObject)
    if not flag then
        if Vector3.Distance(Vector3.zero, go.transform.localPosition) > 50 then
            this:OnBulletDragEnd(go)
            this.dizuo:SetActive(false)
            if this.lines[0].gameObject.activeSelf then
                do
                    local i = 0
                    while i < this.lines.Count do
                        this.lines[i].gameObject:SetActive(false)
                        i = i + 1
                    end
                end
            end
        else
            this:resetBullet()
        end
        this.aimLine.gameObject:SetActive(false)
    else
        this.NGUIWidth = Screen.width * UICamera.currentCamera.rect.width * this.scale
        this.NGUIHeight = Screen.height * this.scale
        this.aimLine.gameObject:SetActive(true)
    end
end
CDanGongWnd.m_OnBulletDragEnd_CS2LuaHook = function (this, go) 
    local localPos = this.bullet.transform.localPosition
    this.bullet.Moving = true
    this.bullet.NormalizedDirection = CommonDefs.ImplicitConvert_Vector2_Vector3(CommonDefs.op_UnaryNegation_Vector3(localPos.normalized))
    this.bullet.Speed = localPos.x == 0 and (- localPos.y) * 0.5 / this.bullet.NormalizedDirection.y or (- localPos.x) * 0.5 / this.bullet.NormalizedDirection.x
end
CDanGongWnd.m_resetBullet_CS2LuaHook = function (this) 
    this.bullet.LocalPosition = Vector3.zero
    this.bullet.LocalRotation = Quaternion.Euler(Vector3.zero)
    this.bullet.Moving = false
    this.dizuo:SetActive(true)
    do
        local i = 0
        while i < this.lines.Count do
            this.lines[i].gameObject:SetActive(true)
            i = i + 1
        end
    end
    this:refreshLinePos()
end
CDanGongWnd.m_launchBirds_CS2LuaHook = function (this) 
    local LY = (Random.value * this.shootingArea.height) - math.floor(this.shootingArea.height / 2)
    local RY = (Random.value * this.shootingArea.height) - math.floor(this.shootingArea.height / 2)
    local LX = math.floor(- this.shootingArea.width / 2) - 100
    local RX = math.floor(this.shootingArea.width / 2) + 100

    local lp = Vector3(LX, LY, 0)
    local rp = Vector3(RX, RY, 0)
    local bird = this:generateBird()
    if Random.value > 0.5 then
        bird.LocalPosition = lp
        bird.NormalizedDirection = CommonDefs.ImplicitConvert_Vector2_Vector3((CommonDefs.op_Subtraction_Vector3_Vector3(rp, lp)).normalized)
    else
        bird.LocalPosition = rp
        bird.NormalizedDirection = CommonDefs.ImplicitConvert_Vector2_Vector3((CommonDefs.op_Subtraction_Vector3_Vector3(lp, rp)).normalized)
    end
    bird.Speed = Random.value * 50 + 40
    local info = LuaZhuJueJuQingMgr.danGongInfo
    if info then
        bird.Speed = bird.Speed * info.flySpeed
    end

    bird.Moving = true
    bird.gameObject:SetActive(true)
    if not CommonDefs.ListContains(this.birdList, typeof(CDangongBulletAndBird), bird) then
        CommonDefs.ListAdd(this.birdList, typeof(CDangongBulletAndBird), bird)
    end
    this.birdTemplate:playAnimation("run")
    this.birdTemplate.transform:GetComponent(typeof(BoxCollider)).enabled = true
end

CDanGongWnd.m_initBullet_CS2LuaHook = function (this) 
    this.bullet.LocalPosition = Vector3.zero
    this.bullet.FlyDistance = 2000
    this.bullet.OnCollideCallback = DelegateFactory.Action_GameObject(function (go) this:onBulletHit(go:GetComponent(typeof(CDangongBulletAndBird))) end)
    this.bullet.OnStopCallback = DelegateFactory.Action_GameObject(function (go) this:onBulletStop(go:GetComponent(typeof(CDangongBulletAndBird))) end)
    this:refreshLinePos()
    this.aimLine.gameObject:SetActive(false)
end

CDanGongWnd.m_refreshLinePos_CS2LuaHook = function (this) 
    for i = 0, 1 do
        local dV = this.dangongAnchor[i].localPosition
        local bV = this.BulletRoot:InverseTransformPoint(this.bulletAnchor[i].position)
        this.lines[i].cachedTransform.localPosition = CommonDefs.op_Division_Vector3_Single((CommonDefs.op_Addition_Vector3_Vector3(dV, bV)), 2)
        local angle = 0
        if i == 0 then
            angle = - Vector3.Angle(Vector3(1, 0, 0), (CommonDefs.op_Subtraction_Vector3_Vector3(bV, dV)))
        else
            angle = Vector3.Angle(Vector3(1, 0, 0), (CommonDefs.op_Subtraction_Vector3_Vector3(dV, bV)))
        end
        if bV.y > dV.y then
            angle = angle * - 1
        end
        this.lines[i].cachedTransform.localRotation = Quaternion.Euler(Vector3(0, 0, angle))
        this.lines[i].width = math.floor(Vector3.Distance(dV, bV))
    end
    local offset = Vector3.Distance(Vector3.zero, this.bullet.LocalPosition)
    this.aimLine.topAnchor.absolute = math.floor((offset * 4 + this.aimLine.bottomAnchor.absolute))
end
CDanGongWnd.m_onBulletHit_CS2LuaHook = function (this, bird) 
    --  UnityEngine.Debug.Log("Hit" + bird.name);\
    this.hitCount = this.hitCount + 1
    this:birdFall()
    this:refreshShow()
end
CDanGongWnd.m_onBirdStop_CS2LuaHook = function (this, bird) 
    if this.hitCount >= this.totalCount then
        local info = LuaZhuJueJuQingMgr.danGongInfo
        if info then
            Gac2Gas.FinishShootBirdsInPlay()
        else
            L10.Game.Gac2Gas.FinishShootBirdsOnce(CDanGongWnd.taskID, 1)
        end
        CUIManager.CloseUI(CUIResources.DanGongWnd)
    else
        this:launchBirds()
    end
end
CDanGongWnd.m_birdFall_CS2LuaHook = function (this) 
    this.birdTemplate.NormalizedDirection = Vector2(0, - 1)
    this.birdTemplate.Speed = 100
    this.birdTemplate:ResetStartPos()
    this.birdTemplate:playAnimation("die")
    this.birdTemplate.transform:GetComponent(typeof(BoxCollider)).enabled = false
end

CDanGongWnd.m_hookAwake = function (this)
    this.birdTemplate:loadBird()
end


