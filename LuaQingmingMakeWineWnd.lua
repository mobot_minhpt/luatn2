local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local Animation = import "UnityEngine.Animation"
local UILabel = import "UILabel"

LuaQingmingMakeWineWnd=class()
RegistChildComponent(LuaQingmingMakeWineWnd,"closeBtn", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"node1", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"node2", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"node3", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"node4", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"wineBottle", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"wineBottleFxNode", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"makeWineBtn", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"shareBtn", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"node", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"succWinLabel", UILabel)
RegistChildComponent(LuaQingmingMakeWineWnd,"BlackNode", GameObject)
RegistChildComponent(LuaQingmingMakeWineWnd,"startLabel", UILabel)

RegistClassMember(LuaQingmingMakeWineWnd, "m_Tick")
RegistClassMember(LuaQingmingMakeWineWnd, "m_Data")
RegistClassMember(LuaQingmingMakeWineWnd, "m_TriggerNum")
RegistClassMember(LuaQingmingMakeWineWnd, "m_Animation")

function LuaQingmingMakeWineWnd:OnEnable()
	--g_ScriptEvent:AddListener("UpdateTaoQuanInfo", self, "UpdateInfo")
end

function LuaQingmingMakeWineWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("UpdateTaoQuanInfo", self, "UpdateInfo")
end

function LuaQingmingMakeWineWnd:LeaveWnd()
	CUIManager.CloseUI("QingmingMakeWineWnd")
	--Gac2Gas.RequestLeavePlay()
end

function LuaQingmingMakeWineWnd:SubmitInfo()

end

function LuaQingmingMakeWineWnd:InitNeedItem(go, itemId, count)
	local item = Item_Item.GetData(itemId)
	if not item then return end

	local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local DisableSprite = go.transform:Find("DisableSprite").gameObject
	local GetHint = go.transform:Find("GetHint").gameObject
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local NameLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	NameLabel.text = item.Name

	IconTexture:LoadMaterial(item.Icon)
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	if totalCount < count then
		DisableSprite:SetActive(true)
		GetHint:SetActive(true)
		CountLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", totalCount,count)

		local onNeedItemClicked = function (go)
			self:OnNeedItemClicked(go, itemId)
		end
		CommonDefs.AddOnClickListener(IconTexture.gameObject, DelegateFactory.Action_GameObject(onNeedItemClicked), false)
		return false
	else
		DisableSprite:SetActive(false)
		GetHint:SetActive(false)
		CountLabel.text = SafeStringFormat3("%d/%d", totalCount, count)
		return true
	end
end

function LuaQingmingMakeWineWnd:OnNeedItemClicked(go, itemId)
	CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
end

function LuaQingmingMakeWineWnd:InitNode()
	local nodeTable = {self.node1,self.node2,self.node3,self.node4}
	local dataString = self.m_Data.NiangJiuNeedItemIds
	local dataTable = LuaStarInviteMgr.split(dataString, ';')
	local notEnoughTable = {}

	local enoughNum = 0
	for i,v in pairs(dataTable) do
		if v then
			local itemData = LuaStarInviteMgr.split(v,',')
			if itemData then
				local itemId = itemData[1]
				local count = tonumber(itemData[2])
				local go = nodeTable[i]
				if go then
					local enough = self:InitNeedItem(go,itemId,count)
					if enough then
						enoughNum = enoughNum + 1
					else
						table.insert(notEnoughTable,go)
					end
				end
			end
		end
	end

	if enoughNum >= self.m_TriggerNum then
		local showText = g_MessageMgr:FormatMessage(self.m_Data.NiangJiuFullRewardMessage)
		if enoughNum == self.m_TriggerNum then
			showText = g_MessageMgr:FormatMessage(self.m_Data.NiangJiuMinRewardMessage)
		end

		local onMakeWineClick = function(go)
			for i,v in pairs(notEnoughTable) do
				v:SetActive(false)
			end
			local clipName = 'QingmingMakeWine_Succ'
			local aniClip = self.m_Animation[clipName]
			if aniClip then
				aniClip.time = 0
				self.m_Animation:Play(clipName)
				self.m_Tick = RegisterTickWithDuration(function ()
					local onSubmitClick = function(go)
						Gac2Gas.RequestQingMingNiangJiu()
						self:LeaveWnd()
					end
					CommonDefs.AddOnClickListener(self.wineBottle,DelegateFactory.Action_GameObject(onSubmitClick),false)
				end,1800,1800)
			end
		end
		CommonDefs.AddOnClickListener(self.makeWineBtn,DelegateFactory.Action_GameObject(onMakeWineClick),false)
	else
		local onMakeWineClick = function(go)
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("道具不足!"))
		end
		CommonDefs.AddOnClickListener(self.makeWineBtn,DelegateFactory.Action_GameObject(onMakeWineClick),false)
	end

end

function LuaQingmingMakeWineWnd:InitAnimation()
	self.m_Animation = self.node:GetComponent(typeof(Animation))
	local chooseAni = self.m_Animation
	local clipName = 'QingmingMakeWine_Succ'
	local aniClip = chooseAni[clipName]
	if aniClip then
		aniClip.time = 0
		aniClip.enabled = true
		aniClip.weight = 1
		chooseAni:Sample()
		aniClip.enabled = false
	end
end

function LuaQingmingMakeWineWnd:Init()
	local onCloseClick = function(go)
		self:LeaveWnd()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onShareClick = function(go)
		CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
	self.m_Data = QingMing2019_NiangJiu.GetData()
	self.m_TriggerNum = 3

	local blackShowText = g_MessageMgr:FormatMessage(self.m_Data.NiangJiuFinishMessage)
	local blackTextLabel = self.BlackNode.transform:Find("Label"):GetComponent(typeof(UILabel))
	blackTextLabel.text = blackShowText
	self.BlackNode:SetActive(false)
	self.succWinLabel.text = g_MessageMgr:FormatMessage("NiangJiuSuccessMessage")
	self.startLabel.text = g_MessageMgr:FormatMessage("NiangJiuRuleMessage")

	self:InitAnimation()
	self:InitNode()
  --LuaQingmingMakeWineWnd NeedTemplate
end

function LuaQingmingMakeWineWnd:OnDestroy()

	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end

return LuaQingmingMakeWineWnd
