local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local Ques_Struct = import "L10.Game.CShiTuMgr+Ques_Struct"
local GameObject = import "UnityEngine.GameObject"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local UILabel = import "UILabel"
local Object = import "System.Object"

LuaShiTuResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuResultWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaShiTuResultWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaShiTuResultWnd, "MoreBtn", "MoreBtn", GameObject)
RegistChildComponent(LuaShiTuResultWnd, "ResultItem", "ResultItem", GameObject)
RegistChildComponent(LuaShiTuResultWnd, "Table", "Table", UITable)
RegistChildComponent(LuaShiTuResultWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaShiTuResultWnd, "SubmitBtn", "SubmitBtn", GameObject)
RegistChildComponent(LuaShiTuResultWnd, "RestartBtn", "RestartBtn", GameObject)
RegistChildComponent(LuaShiTuResultWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaShiTuResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.MoreBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMoreBtnClick()
	end)


	
	UIEventListener.Get(self.SubmitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitBtnClick()
	end)


	
	UIEventListener.Get(self.RestartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRestartBtnClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuResultWnd:Init()
    self.TitleLabel.text = CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI and LocalString.GetString("我要收徒") or LocalString.GetString("我要求师")
    local name = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name or ""
    self.TopLabel.text = SafeStringFormat3(LocalString.GetString("%s,我们已收到你的信息如下"),name)
    self.MoreBtn.gameObject:SetActive(CShiTuMgr.EnableAIRecommend and CShiTuMgr.Inst.ShiTuAskStage == 0 and CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.TU)
    self.ResultItem.gameObject:SetActive(false)
    if CShiTuMgr.Inst.SaveResultValue.Count <= 0 then
        CUIManager.CloseUI(CUIResources.ShiTuResultWnd)
        return
    end

    for i = 0, CShiTuMgr.Inst.QuestionList.Count - 1 do
        local data = CShiTuMgr.Inst.QuestionList[i]
        local go = NGUITools.AddChild(self.Table.gameObject, self.ResultItem)
        self:InitItem(go, data)
    end
    if LuaShiTuMgr.m_OpenNewSystem then
        local data = CreateFromClass(Ques_Struct)
        data.ID = 0
        data.Question = LocalString.GetString("你的个性化标签：")
        local go = NGUITools.AddChild(self.Table.gameObject, self.ResultItem)
        self:InitItem(go, data)
    end
    if CShiTuMgr.Inst.ShiTuAskStage == 1 and CShiTuMgr.Inst.ExtraQuestionList.Count > 0 then
        for i = 0, CShiTuMgr.Inst.ExtraQuestionList.Count - 1 do
            local data = CShiTuMgr.Inst.ExtraQuestionList[i]
            local go = NGUITools.AddChild(self.Table.gameObject, self.ResultItem)
            self:InitItem(go, data)
        end
    end
    self.Table:Reposition()
    self.ScrollView:ResetPosition()
end

function LuaShiTuResultWnd:InitItem(go, data)
    local resultstring = Table2ArrayWithCount({"", data.Option1, data.Option2, data.Option3}, 4, MakeArrayClass(System.String))
    go:SetActive(true)
    local resultLabel = go.transform:GetComponent(typeof(UILabel))
    local resultStr = ""
    if CommonDefs.DictContains(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), data.ID) then
        resultStr = resultstring[CommonDefs.DictGetValue(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), data.ID)]
    elseif CommonDefs.DictContains(CShiTuMgr.Inst.SaveExtraResultValue, typeof(Int32), data.ID) then
        resultStr = resultstring[CommonDefs.DictGetValue(CShiTuMgr.Inst.SaveExtraResultValue, typeof(Int32), data.ID)]
    elseif 0 == data.ID then
        resultStr = #LuaShiTuMgr.m_ShiTuChooseWndTagList == 0 and LocalString.GetString("无") or ""
        for i = 1, #LuaShiTuMgr.m_ShiTuChooseWndTagList do
            local id = LuaShiTuMgr.m_ShiTuChooseWndTagList[i]
            local tagData = ShiTu_Label.GetData(id)
            resultStr = resultStr .. tagData.text
            if i ~= #LuaShiTuMgr.m_ShiTuChooseWndTagList then
                resultStr = resultStr .. LocalString.GetString("、")
            end
        end
    end
    resultLabel.text = SafeStringFormat3("[FFFFFF]%s[C6CC96]%s", data.Question, resultStr)
end

--@region UIEvent

function LuaShiTuResultWnd:OnMoreBtnClick()
    CShiTuMgr.Inst.ShiTuAskStage = 1
    CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
    CUIManager.CloseUI(CUIResources.ShiTuResultWnd)
end


function LuaShiTuResultWnd:OnSubmitBtnClick()
    local options = System.String.Empty
    CommonDefs.DictIterate(CShiTuMgr.Inst.SaveResultValue, DelegateFactory.Action_object_object(function (___key, ___value) 
        local pair = {}
        pair.Key = ___key
        pair.Value = ___value
        if System.String.IsNullOrEmpty(options) then
            options = System.String.Format("{0},{1};", pair.Key, pair.Value)
        else
            options = System.String.Format("{0}{1},{2};", options, pair.Key, pair.Value)
        end
    end))
    if CShiTuMgr.EnableAIRecommend then
        CommonDefs.DictIterate(CShiTuMgr.Inst.SaveExtraResultValue, DelegateFactory.Action_object_object(function (___key, ___value) 
            local pair = {}
            pair.Key = ___key
            pair.Value = ___value
            if System.String.IsNullOrEmpty(options) then
                options = System.String.Format("{0},{1};", pair.Key, pair.Value)
            else
                options = System.String.Format("{0}{1},{2};", options, pair.Key, pair.Value)
            end
        end))
    end
    LuaShiTuMgr:SaveShiTuResultWndCacheData(CShiTuMgr.Inst.CurrentEnterType, options)
    local list = CreateFromClass(MakeGenericClass(List, Object))
    for i = 1, #LuaShiTuMgr.m_ShiTuChooseWndTagList do
        CommonDefs.ListAdd(list, typeof(Object), LuaShiTuMgr.m_ShiTuChooseWndTagList[i])
    end
    if CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI then
        if CShiTuMgr.EnableAIRecommend then
            Gac2Gas.PostShouTuMessageV2(options, MsgPackImpl.pack(list), LuaShiTuMgr.m_IsShouTuWaiMen)
        else
            Gac2Gas.PostShouTuMessage(options)
        end
    elseif CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.TU then
        if CShiTuMgr.EnableAIRecommend then
            Gac2Gas.SearchShiFuV2(options, MsgPackImpl.pack(list), LuaShiTuMgr.m_IsShouTuWaiMen)
        else
            Gac2Gas.SearchShiFu(options)
        end
    end
    CShiTuMgr.Inst:ClearData()
    CUIManager.CloseUI(CUIResources.ShiTuResultWnd)
end

function LuaShiTuResultWnd:OnDisable()
    LuaShiTuMgr.m_IsShouTuWaiMen = false
end

function LuaShiTuResultWnd:OnRestartBtnClick()
    CShiTuMgr.Inst:ClearData()
    CUIManager.ShowUI(CUIResources.ShiTuChooseWnd)
    CUIManager.CloseUI(CUIResources.ShiTuResultWnd)
end


function LuaShiTuResultWnd:OnCloseButtonClick()
    CShiTuMgr.Inst:ClearData()
    CUIManager.CloseUI(CUIResources.ShiTuResultWnd)
end


--@endregion UIEvent

