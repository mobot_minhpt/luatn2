-- Auto Generated!!
local CMapiPreviewWnd = import "L10.UI.CMapiPreviewWnd"
CMapiPreviewWnd.m_Init_CS2LuaHook = function (this) 
    if CMapiPreviewWnd.mapiInfo ~= nil then
        this.textureLoader:Init(CMapiPreviewWnd.mapiInfo, nil, nil, this.rotation, nil)
        this.mapiName.text = CMapiPreviewWnd.mapiInfo.Name
    end
end
