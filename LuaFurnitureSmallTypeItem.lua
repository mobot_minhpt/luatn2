
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgr_AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CZuoanFurnitureMgr = import "L10.UI.CZuoanFurnitureMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"


CLuaFurnitureSmallTypeItem = class()

RegistChildComponent(CLuaFurnitureSmallTypeItem,"iconTexture","Texture",CUITexture)
RegistChildComponent(CLuaFurnitureSmallTypeItem,"CountLabel","AmountLabel",UILabel)
RegistChildComponent(CLuaFurnitureSmallTypeItem,"DisableSprite","DisableSprite",GameObject)
RegistChildComponent(CLuaFurnitureSmallTypeItem,"LingshouSprite","LingshouSprite",UISprite)
RegistChildComponent(CLuaFurnitureSmallTypeItem,"Current",GameObject)
RegistChildComponent(CLuaFurnitureSmallTypeItem,"TimeLimit",GameObject)
RegistChildComponent(CLuaFurnitureSmallTypeItem,"DurationLimit",UILabel)
RegistClassMember(CLuaFurnitureSmallTypeItem,"TemplateId")
RegistClassMember(CLuaFurnitureSmallTypeItem,"ChuanjiabaoItemId")
RegistClassMember(CLuaFurnitureSmallTypeItem,"chuanjiabaoId")


CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = 0
CLuaFurnitureSmallTypeItem.CurShowChuanjiabaoItemId = nil

function CLuaFurnitureSmallTypeItem:Reinit()
    self:Init0(self.TemplateId)
end

function CLuaFurnitureSmallTypeItem:Init0(typeId) 
    self.ChuanjiabaoItemId = nil
    local data = Zhuangshiwu_Zhuangshiwu.GetData(typeId)
    if data == nil then
        return
    end
    self.TemplateId = typeId

    if data.Type ~= EnumFurnitureType_lua.eJianzhu then
        local itemtid = data.ItemId
        local itemdata = Item_Item.GetData(itemtid)
        if itemdata ~= nil then
            if self.iconTexture ~= nil then
                self.iconTexture:LoadMaterial(itemdata.Icon)
            end
        end
    else
        self.iconTexture:LoadMaterial(data.Icon)
    end

    local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(typeId)
    self:CheckDisableSprite()
    

    if data.SubType == EnumFurnitureSubType_lua.eZhengwu 
        or data.SubType == EnumFurnitureSubType_lua.eCangku 
        or data.SubType == EnumFurnitureSubType_lua.eXiangfang
        or data.SubType == EnumFurnitureSubType_lua.eWeiqiang then
        local houseGrade = CClientHouseMgr.Inst:GetCurHouseGrade()
        self.DisableSprite:SetActive(houseGrade < data.HouseGrade)
    end

    if self.Current then
        self.Current:SetActive(false)
    end

    self.CountLabel.gameObject:SetActive(count > 0)
    self.CountLabel.text = tostring(count)

    if data.Type == EnumFurnitureType_lua.eSkyBox then
        local isUnlock = CLuaHouseMgr.IsSkyBoxUnlock(data)
        self.DisableSprite:SetActive(not isUnlock)
        local isCurrent = CLuaHouseMgr.IsCurrentSkyBox(data)
        self.Current:SetActive(isCurrent)
        self.CountLabel.text = nil
    end

    self.DurationLimit.gameObject:SetActive(false)
    if data.Type == EnumFurnitureType_lua.eWallPaper then
        local paperData = House_WallPaper.GetDataBySubKey("ItemID",data.ItemId)
        if paperData then
            local isUnlock = CLuaHouseMgr.IsWallPaperUnlock(paperData.Id)
            self.DisableSprite:SetActive(not isUnlock)
            local isCurrent = CLuaHouseMgr.IsCurrentWallPaer(paperData.Id)
            self.Current:SetActive(isCurrent)
            self.CountLabel.text = nil
            self.DurationDay = paperData.Duration
            self:CheckDurationLimit(paperData,isUnlock)
        end
    end
    
    self:CheckTimeLimit(data)

    self.LingshouSprite.gameObject:SetActive(data.Type == EnumFurnitureType_lua.eLingshou)
    self.LingshouSprite.spriteName = CClientFurnitureMgr.GetLingshouSpriteName(data.Quality)
end

function CLuaFurnitureSmallTypeItem:Init4Chuanjiabao( chuanjiabaoId) 
    self.TemplateId=0
    self.TimeLimit:SetActive(false)
    local item = CItemMgr.Inst:GetById(chuanjiabaoId)
    if item == nil then
        return
    end
    self.ChuanjiabaoItemId = chuanjiabaoId

    local chuanjiabaoItemTid = House_Setting.GetData().ChuanjiabaoModelItemId[0]
    local itemdata = Item_Item.GetData(chuanjiabaoItemTid)
    if itemdata ~= nil then
        self.iconTexture:LoadMaterial(item.Icon)
    end

    local bPlaced = CClientFurnitureMgr.Inst:IsChuanjiabaoPlaced(chuanjiabaoId)
    self.DisableSprite:SetActive(false)

    local str = bPlaced and LocalString.GetString("已摆放") or "1"
    self.CountLabel.gameObject:SetActive(true)
    self.CountLabel.text = str

    self.LingshouSprite.gameObject:SetActive(false)
    self.DurationLimit.gameObject:SetActive(false)
end

function CLuaFurnitureSmallTypeItem:CheckDisableSprite( )
    local restCount = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(self.TemplateId)
    local placedCount = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(self.TemplateId)
    local penzhaiHuamuCount = CLuaClientFurnitureMgr.GetPenzhaiHuamuCountByTemplateId(self.TemplateId)
    if LuaPoolMgr.IsPoolEnterZswTemplate(self.TemplateId) or LuaHouseTerrainMgr.IsEditTerrainTemplateId(self.TemplateId) then
        self.DisableSprite:SetActive(false)
        return
    end
    self.DisableSprite:SetActive((restCount + placedCount + penzhaiHuamuCount) <= 0)
end

function CLuaFurnitureSmallTypeItem:CheckTimeLimit(data)
    self.TimeLimit:SetActive(false)
    if not data then return end

    -- 天空盒类型
    if data.Type == EnumFurnitureType_lua.eSkyBox then
        local skyBoxData = CLuaHouseMgr.GetSkyBoxItemByZSWId(data.ID)
        if not skyBoxData then return end

        if skyBoxData.BuyDuration == "~" or skyBoxData.UseDuration == "~" then return end
        local inBuyDuration = CLuaHouseMgr.IsSkyBoxInBuyDuration(skyBoxData)
        local inUseDuration = CLuaHouseMgr.IsSkyBoxInUseDuration(skyBoxData)
        local isUnlock = CLuaHouseMgr.IsSkyBoxUnlock(data)
        local isCurrent = CLuaHouseMgr.IsCurrentSkyBox(data)

        if not inBuyDuration and not inUseDuration then return end
        self.TimeLimit:SetActive(not isCurrent)
    end
end

function CLuaFurnitureSmallTypeItem:CheckDurationLimit(paperData,isUnlock)
    if not paperData then
        return
    end
    if not isUnlock then
        return
    end

    local durationDay = paperData.Duration
    if durationDay <= -1 then return end

    if CClientHouseMgr.Inst and CClientHouseMgr.Inst.mCurFurnitureProp2 then
        local map = CClientHouseMgr.Inst.mCurFurnitureProp2.WallFloorOutOfDateTime
        if CommonDefs.IsDic(map) and CommonDefs.DictTryGet(map,typeof(Byte),paperData.Id,typeof(UInt32)) then
            self.DurationLimit.gameObject:SetActive(true)
            local outDateTime = CommonDefs.DictGetValue(map, typeof(Byte), paperData.Id)
            local now = CServerTimeMgr.Inst.timeStamp
            local daysRemain = math.ceil((outDateTime - now) / 3600 / 24)
            local color = NGUIText.ParseColor24("58FF23", 0)
            if daysRemain < 0 then
                self.DurationLimit.text = LocalString.GetString("过期")
                color = NGUIText.ParseColor24("FF2B2B", 0)
            else
                self.DurationLimit.text = SafeStringFormat3(LocalString.GetString("%d天"),daysRemain)
            end
            self.DurationLimit.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = color
        end
    end
end

function CLuaFurnitureSmallTypeItem:SetSelected( bSelected) 
    CLuaHouseMgr.ClickItemInfo = nil
    if bSelected then
        local quickSign = false
        local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(self.TemplateId)
        if zswdata ~= nil then

            --过山车花车
            if zswdata.SubType == EnumFurnitureSubType_lua.eRollerCoasterCabin then
                --如果打开了站台摆花车的界面
                if CUIManager.IsLoaded(CLuaUIResources.HouseRollerCoasterUseZhanTaiWnd) then
                    local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs( 0, zswdata.ID)
                    local actionSource=DefaultItemActionDataSource.Create(cnt,actions,names)

                    CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = self.TemplateId
                    CItemInfoMgr.showType = ShowType.FurnitureRepository
                    CItemInfoMgr.linkItemInfo = LinkItemInfo(self.TemplateId, false, actionSource)
                    CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
                    CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
                    return
                end
            end
            --切换选择的时候就关掉这个界面
            CUIManager.CloseUI(CLuaUIResources.HouseRollerCoasterUseZhanTaiWnd)


            -- 对天空盒的特殊处理
            if zswdata.Type == EnumFurnitureType_lua.eSkyBox or zswdata.Type == EnumFurnitureType_lua.eWallPaper then
                local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs( 0, zswdata.ID) --self:GetActionPairs(nil,self.TemplateId)
                local actionSource=DefaultItemActionDataSource.Create(cnt,actions,names)

                CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = self.TemplateId
                CItemInfoMgr.showType = ShowType.FurnitureRepository
                CItemInfoMgr.linkItemInfo = LinkItemInfo(self.TemplateId, false, actionSource)
                CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
                CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
                return
            end
            -- 对水池编辑入口的特殊处理
            if zswdata.Type == EnumFurnitureType_lua.ePoolFurniture and LuaPoolMgr.IsPoolEnterZswTemplate(self.TemplateId) then
                if not LuaPoolMgr.s_Pool_Editor_Switch then
                    g_MessageMgr:ShowMessage("Pool_Edit_Not_Open")
                    return
                end
                local houseGrade = CClientHouseMgr.Inst.mConstructProp.ZhengwuGrade
                if houseGrade < Zhuangshiwu_Setting.GetData().PoolNeedZhengwuGrade then
                    g_MessageMgr:ShowMessage("Edit_Pool_Need_Upgrade_Zhengwu")
                else
                    local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs( 0, zswdata.ID)
                    local actionSource=DefaultItemActionDataSource.Create(cnt,actions,names)
    
                    CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = self.TemplateId
                    CItemInfoMgr.showType = ShowType.FurnitureRepository
                    CItemInfoMgr.linkItemInfo = LinkItemInfo(self.TemplateId, false, actionSource)
                    CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
                    CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
                end
                return
            end

            local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(self.TemplateId)
            if count > 0 then
                if zswdata.SubType == EnumFurnitureSubType_lua.eHuamu and CZuoanFurnitureMgr.Inst:CheckCanPutHuamu() then
                elseif zswdata.Location == EnumFurnitureLocation_lua.eDesk then
                elseif zswdata.Location == EnumFurnitureLocation_lua.eDeskOrLand then
                    if CZuoanFurnitureMgr.Inst:GetCurParentFurnitureId() ~= 0 then
                    else
                        if count > 1 then
                            self:OnUseContinued()
                        else
                            self:OnUse()
                        end
                        CLuaHouseMgr.ClickItemInfo = {
                            ChuanjiabaoItemId = self.ChuanjiabaoItemId,
                            TemplateId = self.TemplateId,
                        }
                        quickSign = true
                        g_ScriptEvent:BroadcastInLua("OnFurnitureMoveChoose_Lua", zswdata.Name)
                    end
                else
                    if count > 1 then
                        self:OnUseContinued()
                    else
                        self:OnUse()
                    end

                    CLuaHouseMgr.ClickItemInfo = {
                        ChuanjiabaoItemId = self.ChuanjiabaoItemId,
                        TemplateId = self.TemplateId,
                    }
                    quickSign = true
                    g_ScriptEvent:BroadcastInLua("OnFurnitureMoveChoose_Lua", zswdata.Name)
                end
            end
        end
        if not quickSign then
            if self.ChuanjiabaoItemId == nil and self.TemplateId ~= 0 then

                local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs( self.ChuanjiabaoItemId, self.TemplateId) --self:GetActionPairs(nil,self.TemplateId)
                local actionSource=DefaultItemActionDataSource.Create(cnt,actions,names)

                -- CItemInfoMgr.ShowRepositoryFurnitureInfo(self.TemplateId, self)
                CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = self.TemplateId
                CItemInfoMgr.showType = ShowType.FurnitureRepository
                CItemInfoMgr.linkItemInfo = LinkItemInfo(self.TemplateId, false, actionSource)
                CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
                CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)

            elseif self.ChuanjiabaoItemId ~= nil and self.TemplateId == 0 then
                -- CItemInfoMgr.ShowRepositoryChuanjiabaoInfo(self.ChuanjiabaoItemId, self)
                local cnt,actions,names = CLuaHouseMgr.GetFurnitureItemActionPairs( self.ChuanjiabaoItemId, self.TemplateId) --self:GetActionPairs(self.ChuanjiabaoItemId,self.TemplateId)
                local actionSource=DefaultItemActionDataSource.Create(cnt,actions,names)

                local item = CItemMgr.Inst:GetById(self.ChuanjiabaoItemId)
                if item ~= nil then
                    CLuaFurnitureSmallTypeItem.CurShowChuanjiabaoItemId = self.ChuanjiabaoItemId
                    CItemInfoMgr.showType = ShowType.ChuanjiabaoRepository
                    CItemInfoMgr.linkItemInfo = LinkItemInfo(item, false, actionSource)
                    CItemInfoMgr.linkItemInfo.alignType = CItemInfoMgr_AlignType.ScreenRight
                    CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
                end
            end
        end
    end
end

function CLuaFurnitureSmallTypeItem:OnUse( )
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.TemplateId)
    if data == nil then
        return
    end

    if not CLuaClientFurnitureMgr.CheckLocation(data) then--not CClientFurnitureMgr.Inst:CheckLocation(data) then
        return
    end

    local numlimit = 0
    local curNum =CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(self.TemplateId)
    if CLuaHouseWoodPileMgr.IsWoodPile(self.TemplateId) then
        numlimit = CLuaHouseWoodPileMgr.m_NumLimit
    else
        numlimit = CLuaClientFurnitureMgr.GetRepositoryNumLimit(CClientHouseMgr.Inst:GetCurHouseGrade(), data.Type, data.SubType)
    end

    if numlimit > curNum then
        CClientFurnitureMgr.Inst:SetContinuation(false)
        Gac2Gas.CreateFurnitureFromRepository(self.TemplateId, "")
    else
        g_MessageMgr:ShowMessage("Cant_Add_Furniture_Exceed_Limit")
    end
    CClientFurnitureMgr.Inst:ResetbContinuation()
    CItemInfoMgr.CloseItemInfoWnd()
end
function CLuaFurnitureSmallTypeItem:OnUseContinued( )
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.TemplateId)
    if data == nil then
        return
    end

    if not CLuaClientFurnitureMgr.CheckLocation(data) then--not CClientFurnitureMgr.Inst:CheckLocation(data) then
        return
    end

    local numlimit = 0
    local curNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(self.TemplateId)
    if CLuaHouseWoodPileMgr.IsWoodPile(self.TemplateId) then
        numlimit = CLuaHouseWoodPileMgr.m_NumLimit
    else
        numlimit = CLuaClientFurnitureMgr.GetRepositoryNumLimit(CClientHouseMgr.Inst:GetCurHouseGrade(), data.Type, data.SubType)
    end

    if numlimit > curNum then
        CClientFurnitureMgr.Inst:SetContinuation(true)
        Gac2Gas.CreateFurnitureFromRepository(self.TemplateId, "")
    else
        g_MessageMgr:ShowMessage("Cant_Add_Furniture_Exceed_Limit")
    end
    CItemInfoMgr.CloseItemInfoWnd()
end
function CLuaFurnitureSmallTypeItem:OnDiscard( )
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.TemplateId)
    if data ~= nil then
        local qishuFurnitureCount = CLuaClientFurnitureMgr.GetQishuFurnitureCountByTemplateId(self.TemplateId)
        local repositoryCount = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(self.TemplateId)
        local placedFurnitureCount = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(self.TemplateId)
        if repositoryCount > 0 and (repositoryCount + placedFurnitureCount) > qishuFurnitureCount then
            MessageWndManager.ShowOKCancelMessage(SafeStringFormat3(LocalString.GetString("确定丢弃装修仓库的家具 %s？"), data.Name), DelegateFactory.Action(function () 
                Gac2Gas.ExchangeFurniture(EnumFurniturePlace_lua.eRepository, self.TemplateId, EnumFurniturePlace_lua.eDiscard, 0, CClientMainPlayer.Inst.Id, "")
            end), nil, nil, nil, false)
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("仓库中剩余%d个，其中%d个来自气数装饰物，不能丢弃气数装饰物！"), repositoryCount, math.max(qishuFurnitureCount - placedFurnitureCount, 0)))
        end
    end

    CItemInfoMgr.CloseItemInfoWnd()
end

function CLuaFurnitureSmallTypeItem:OnEnable()
    g_ScriptEvent:AddListener("SetRepositoryFurnitureCount",self,"OnSetRepositoryFurnitureCount")

end

function CLuaFurnitureSmallTypeItem:OnDisable()
    g_ScriptEvent:RemoveListener("SetRepositoryFurnitureCount",self,"OnSetRepositoryFurnitureCount")

end
function CLuaFurnitureSmallTypeItem:OnSetRepositoryFurnitureCount(templateId,count)
    if templateId==self.TemplateId then
        self.CountLabel.gameObject:SetActive(count > 0)
        self.CountLabel.text = tostring(count)
    end
end

--监听事件OnPlacedFurnitureChanged，一键收回的时候太卡了。每帧更新开销也不会很大
local frame = 0
function CLuaFurnitureSmallTypeItem:Update()
    frame = frame+1
    if frame%5==0 then
        if self.ChuanjiabaoItemId then
            local bPlaced = CClientFurnitureMgr.Inst:IsChuanjiabaoPlaced(self.ChuanjiabaoItemId)
            self.CountLabel.text = bPlaced and LocalString.GetString("已摆放") or "1"
        end
    end
end
