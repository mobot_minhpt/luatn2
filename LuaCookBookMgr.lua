LuaCookBookMgr = {}
LuaCookBookMgr.m_ItemSelectWndType = 1
LuaCookBookMgr.m_ItemSelectWndCurSelectItem = nil

LuaCookBookMgr.m_UnlockedCookbookData = {}
LuaCookBookMgr.m_IngredientPackCount = 0

LuaCookBookMgr.m_DefaultCookbookId = 0
function LuaCookBookMgr:ShangShiSendCookbookData(ingredientPackCount, lastCookbookId, cookbookInfoUD)
    -- 食材包数量 上次烹饪的食谱ID
    self.m_IngredientPackCount = ingredientPackCount
    self.m_UnlockedCookbookData = {}
    local list = MsgPackImpl.unpack(cookbookInfoUD)
    for i = 0, list.Count - 1, 3 do
        local cookbookId = list[i]
        local isCookSucc = list[i+1] -- 是否研制成功
        local star = list[i+2] -- 食谱星级
        self.m_UnlockedCookbookData[cookbookId] = {isCookSucc = isCookSucc, star = star, cookbookId = cookbookId}
    end
    --self.m_UnlockedCookbookData[1].isCookSucc = true
    if CUIManager.IsLoaded(CLuaUIResources.CookBookWnd) then
        g_ScriptEvent:BroadcastInLua("OnShangShiSendCookbookData")
        return 
    end
    Gac2Gas.ShangShiMyFoodIngredient()
    CUIManager.ShowUI(CLuaUIResources.CookBookWnd)
end

LuaCookBookMgr.m_MyFoodIngredientData = nil
function LuaCookBookMgr:InitMyFoodIngredientData()
    self.m_MyFoodIngredientData = {}
    ShangShi_Ingredient.ForeachKey(function(key)
        self.m_MyFoodIngredientData[key] = {count = 0, data = ShangShi_Ingredient.GetData(key)}
    end)
end

function LuaCookBookMgr:ShangShiSendMyFoodIngredient(ingredientInfoUD)
    self:InitMyFoodIngredientData()
    local list = MsgPackImpl.unpack(ingredientInfoUD)
    for i = 0, list.Count - 1, 3 do
        local ingredientId = list[i] -- 食材id
        local count = list[i+1] -- 食材数量
        local isNew = list[i+2] 
        if self.m_MyFoodIngredientData[ingredientId] then
            self.m_MyFoodIngredientData[ingredientId].count = count
            self.m_MyFoodIngredientData[ingredientId].isNew = isNew
        end
    end
    g_ScriptEvent:BroadcastInLua("OnShangShiSendMyFoodIngredient")
end

LuaCookBookMgr.m_ShangShiSendCookingResult = {}
function LuaCookBookMgr:ShangShiSendCookingResult(cookbookId, isSucc, star)
    -- 食谱id, 是否成功, 星级
    self.m_ShangShiSendCookingResult = {cookbookId = cookbookId, isSucc = isSucc, star = star}
    Gac2Gas.ShangShiCookbookData()
    Gac2Gas.ShangShiMyFoodIngredient()
    g_ScriptEvent:BroadcastInLua("OnShangShiSendCookingResult")
    CUIManager.ShowUI(CLuaUIResources.CookingResultWnd)
end

LuaCookBookMgr.m_OpenIngredientPackResult = {}
function LuaCookBookMgr:ShangShiSendOpenIngredientPackResult(ingredientInfoUD)
    self.m_OpenIngredientPackResult = {}
    self.m_IngredientPackCount = self.m_IngredientPackCount - 1
    local list = MsgPackImpl.unpack(ingredientInfoUD)
    for i = 0, list.Count - 1, 3 do
        local ingredientId = list[i]
        local count = list[i+1]
        local isNew = list[i+2]
        table.insert(self.m_OpenIngredientPackResult,{ingredientId = ingredientId, count = count, isNew = isNew})
    end
    Gac2Gas.ShangShiMyFoodIngredient()
    CUIManager.ShowUI(CLuaUIResources.OpenIngredientPackResultWnd)
    g_ScriptEvent:BroadcastInLua("OnShangShiSendOpenIngredientPackResult")
end

function LuaCookBookMgr:ShangShiSendRetrieveIngredientSucc(ingredientId, remainCount)
    -- 食材id, 剩余数量
    self.m_MyFoodIngredientData[ingredientId].count = remainCount
    g_ScriptEvent:BroadcastInLua("OnShangShiSendMyFoodIngredient")
end

function LuaCookBookMgr:ProcessCookbookTask(task)
    if task.TemplateId <= 22111752 and  task.TemplateId >= 22111738 then
        if task.CanSubmit ~= 1 then
            Gac2Gas.ShangShiCookbookData()
            return true
        end
    end
    return false
end