local MainPlayerFrame = import "L10.UI.MainPlayerFrame"
local CQuanMinPKMgr   = import "L10.Game.CQuanMinPKMgr"
local CGuidelineMgr   = import "L10.Game.CGuidelineMgr"

MainPlayerFrame.m_hookRefreshGuidelineAlert = function (this)
    if not CClientMainPlayer.Inst or CClientMainPlayer.Inst:IsInCombatVehicle() then return end

    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer or LuaCuJuMgr:IsInCuJu() then
        this.enhanceBtn:SetActive(false)
        return
    end

    this.enhanceBtn:SetActive(CGuidelineMgr.Inst:NeedShowEnhanceButton())
    this.enhanceAlert.Visible = CGuidelineMgr.Inst:NeedShowEnhanceAlert()
end
