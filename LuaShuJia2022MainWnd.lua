local CUIFx = import "L10.UI.CUIFx"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

LuaShuJia2022MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuJia2022MainWnd, "OpenningTimeLabel", "OpenningTimeLabel", UILabel)
RegistChildComponent(LuaShuJia2022MainWnd, "XiaRiBtn", "XiaRiBtn", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "ShanYeBtn", "ShanYeBtn", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "QianDaoBtn", "QianDaoBtn", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "QianDaoAlert", "QianDaoAlert", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "HuanHunBtn", "HuanHunBtn", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "HuanHunStateLabel", "HuanHunStateLabel", UILabel)
RegistChildComponent(LuaShuJia2022MainWnd, "HuanHunActivityName_Off", "HuanHunActivityName_Off", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "HuanHunActivityName_On", "HuanHunActivityName_On", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "Left_on", "Left_on", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "Left_off", "Left_off", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "Item", "Item", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "Item_On", "Item_On", GameObject)
RegistChildComponent(LuaShuJia2022MainWnd, "ShanYeFx", "ShanYeFx", CUIFx)
RegistChildComponent(LuaShuJia2022MainWnd, "HuanHunFx", "HuanHunFx", CUIFx)

--@endregion RegistChildComponent end

function LuaShuJia2022MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.XiaRiBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnXiaRiBtnClick()
	end)


	
	UIEventListener.Get(self.ShanYeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShanYeBtnClick()
	end)


	
	UIEventListener.Get(self.QianDaoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQianDaoBtnClick()
	end)


	
	UIEventListener.Get(self.HuanHunBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHuanHunBtnClick()
	end)


    --@endregion EventBind end
end

function LuaShuJia2022MainWnd:Init()
	CScheduleMgr.Inst:RequestActivity()

	local alert = self.ShanYeBtn.transform:Find("ShanYeAlert").gameObject
	alert:SetActive(false)
	Gac2Gas.QueryHasShanYeMiZongReward()

	CLuaScheduleMgr.BuildInfos()
	local setting = ShuJia2022_Setting.GetData()
	self.OpenningTimeLabel.text = setting.ShanYeMiZongTime

	-- 设置签到红点
	self.QianDaoAlert:SetActive(self:IsHolidayAlert())

	-- 设置还魂状态
	local openTime = setting.ShanYeMiZongFinalTime

	local _, _, y, m, d, h = string.find(openTime, LocalString.GetString("(%d+)%-(%d+)%-(%d+) (%d+):00"))
	local now = CServerTimeMgr.Inst:GetZone8Time()
	if now.Month > tonumber(m) or (now.Month == tonumber(m) and (now.Day > tonumber(d) or (now.Day == tonumber(d) and now.Hour >= tonumber(h)))) then
		self:SetHuanHunState(true)
	else
		self:SetHuanHunState(false)
	end

	UnRegisterTick(self.tick)
	
	if not CTrackMgr.Inst.isTracking then
		-- 开始寻路时要求关闭界面
		self.tick = RegisterTick(function()
			if CTrackMgr.Inst.isTracking then
				CUIManager.CloseUI(CLuaUIResources.ShuJia2022MainWnd)
			end
		end, 1000)
	end
end

function LuaShuJia2022MainWnd:SetHuanHunState(hasOpen)
	self.HuanHunOpen = hasOpen

	self.HuanHunActivityName_Off:SetActive(not hasOpen)
	self.HuanHunActivityName_On:SetActive(hasOpen)
	self.Left_off:SetActive(not hasOpen)
	self.Left_on:SetActive(hasOpen)
	self.Item_On:SetActive(hasOpen)

	-- 置灰
	--CUICommonDef.SetActive(self.HuanHunBtn, hasOpen, false)
end

function LuaShuJia2022MainWnd:SetRedDot(hasRedDot)
	local alert = self.ShanYeBtn.transform:Find("ShanYeAlert").gameObject
	alert:SetActive(hasRedDot)
end

function LuaShuJia2022MainWnd:IsHolidayAlert()
	local holiday = CSigninMgr.Inst.holidayInfo
	if holiday and holiday.showSignin and not holiday.hasSignGift and holiday.todayInfo and holiday.todayInfo.Open == 1 then
		return true
	else
		return false
	end
end

function LuaShuJia2022MainWnd:OnDisable()
	UnRegisterTick(self.tick)
	g_ScriptEvent:RemoveListener("SendHasShanYeMiZongReward", self, "SetRedDot")
	g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")

	-- 刷新一下外面的红点
	g_ScriptEvent:BroadcastInLua("SendHasShanYeMiZongReward_JieRiButton")
end

function LuaShuJia2022MainWnd:OnEnable()
	g_ScriptEvent:AddListener("SendHasShanYeMiZongReward", self, "SetRedDot")
	g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")
end

function LuaShuJia2022MainWnd:OnUpdateActivity()
	CLuaScheduleMgr.BuildInfos()
end

--@region UIEvent

function LuaShuJia2022MainWnd:OnXiaRiBtnClick()
	-- 42010120	夏日疑云
	CLuaScheduleMgr:ShowScheduleInfo(42010120)
end

function LuaShuJia2022MainWnd:OnShanYeBtnClick()
	-- 请求激活状态
	-- 42010121	山野迷踪
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level >= 30 then
		Gac2Gas.QueryShanYeMiZongInfo()
	else
		g_MessageMgr:ShowMessage("SYMZLevelLimit")
	end

	self.ShanYeFx:DestroyFx()
	self.ShanYeFx:LoadFx("fx/ui/prefab/UI_shujiahuodong_2022_dianji.prefab")
	local alert = self.ShanYeBtn.transform:Find("ShanYeAlert").gameObject
	alert:SetActive(false)
end

function LuaShuJia2022MainWnd:OnQianDaoBtnClick()
	CWelfareMgr.OpenWelfareWnd(LocalString.GetString("暑期签到"))
	self.QianDaoAlert:SetActive(false)
end

function LuaShuJia2022MainWnd:OnHuanHunBtnClick()
	-- 42010122	还魂山庄
	self.HuanHunFx:DestroyFx()
	self.HuanHunFx:LoadFx("fx/ui/prefab/UI_shujiahuodong_2022_dianji.prefab")
	if self.HuanHunOpen then
		CLuaScheduleMgr:ShowScheduleInfo(42010122)
	else
		g_MessageMgr:ShowMessage("HHSZ_NOT_OPEN")
	end
end

--@endregion UIEvent

