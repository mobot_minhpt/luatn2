-- Auto Generated!!
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CplayerShop_NormalSearch = import "L10.UI.CplayerShop_NormalSearch"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local DelegateFactory = import "DelegateFactory"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Object = import "System.Object"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local SearchOption = import "L10.UI.SearchOption"
local SearchWindowType = import "L10.UI.CplayerShop_NormalSearch+SearchWindowType"
local String = import "System.String"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
CplayerShop_NormalSearch.m_OnSearchTextChange_CS2LuaHook = function (this, text)
    this.m_SearchButton.Enabled = false
    CommonDefs.GetComponentInChildren_Component_Type(this.m_SearchInput, typeof(UILabel)).color = Color.white
    if System.String.IsNullOrEmpty(text) then
        return
    end
    local ret = CPlayerShopMgr.Inst:SearchAllByPrefix(text)
    CommonDefs.ListClear(this.m_SearchOptions)
    CommonDefs.ListClear(this.m_SearchTemplateIds)
    CommonDefs.HashSetClear(this.m_SearchNames)
    CommonDefs.DictIterate(ret, DelegateFactory.Action_object_object(function (___key, ___value) 
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        local continue
        repeat
            if (string.find(kv.Value, LocalString.GetString("朱砂笔"), 1, true) ~= nil) and CommonDefs.HashSetContains(this.m_SearchNames, typeof(String), kv.Value) then
                continue = true
                break
            end
            CommonDefs.HashSetAdd(this.m_SearchNames, typeof(String), kv.Value)
            CommonDefs.ListAdd(this.m_SearchOptions, typeof(PopupMenuItemData), PopupMenuItemData(kv.Value, MakeDelegateFromCSFunction(this.OnSearchOptionSelect, MakeGenericClass(Action1, Int32), this), false, nil))
            CommonDefs.ListAdd(this.m_SearchTemplateIds, typeof(UInt32), kv.Key)
            continue = true
        until 1
        if not continue then
            return
        end
    end))
    this.m_SearchTemplateColors = CPlayerShopMgr.Inst:GetColors(this.m_SearchTemplateIds)
    local columns = this:GetColumnCount(this.m_SearchOptions.Count)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(CommonDefs.ListToArray(this.m_SearchOptions), this.m_SearchInput.transform, CPopupMenuInfoMgr.AlignType.Right, columns, this.m_SearchTemplateColors, nil, nil, 600,420)
end
CplayerShop_NormalSearch.m_OnSearchButtonClick_CS2LuaHook = function (this, button) 

    if this.m_SelectOptionIndex >= 0 and this.m_SelectOptionIndex < this.m_SearchTemplateIds.Count then
        local templateId = this.m_SearchTemplateIds[this.m_SelectOptionIndex]
        if this.OnSearchCallback ~= nil then
            GenericDelegateInvoke(this.OnSearchCallback, Table2ArrayWithCount({templateId, SearchOption.All, true}, 3, MakeArrayClass(Object)))
        end
    end
end
CplayerShop_NormalSearch.m_OnSearchOptionSelect_CS2LuaHook = function (this, row) 

    if this.m_SearchButton ~= nil and this.m_SearchInput ~= nil and this.m_SearchOptions ~= nil and this.m_SearchOptions.Count > row then
        this.m_SelectOptionIndex = row
        CommonDefs.GetComponentInChildren_Component_Type(this.m_SearchInput, typeof(UILabel)).text = this.m_SearchOptions[row].text
        CommonDefs.GetComponentInChildren_Component_Type(this.m_SearchInput, typeof(UILabel)).color = this.m_SearchTemplateColors[row]
        this.m_SearchButton.Enabled = true
    end
end
CplayerShop_NormalSearch.m_GetColumnCount_CS2LuaHook = function (this, count) 
    local columns = 1
    if count >= 6 then
        columns = 2
    end
    return columns
end
CplayerShop_NormalSearch.m_NumberOfRows_CS2LuaHook = function (this, view) 
    repeat
        local default = this.m_searchWndType
        if default == SearchWindowType.NormalShop then
            return CPlayerShopMgr.Inst.SearchHistories.Count
        elseif default == SearchWindowType.AuctionShop then
            return CAuctionMgr.Inst.SearchHistory.Count
        else
            return 0
        end
    until 1
end
CplayerShop_NormalSearch.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = view:GetFromPool(0)
    local templateId = 0
    repeat
        local default = this.m_searchWndType
        if default == SearchWindowType.NormalShop then
            templateId = CPlayerShopMgr.Inst.SearchHistories[row]
            break
        elseif default == SearchWindowType.AuctionShop then
            templateId = CAuctionMgr.Inst.SearchHistory[row]
            break
        end
    until 1
    CommonDefs.GetComponentInChildren_Component_Type(item, typeof(UILabel)).text = CPlayerShopMgr.Inst:GetName(templateId)
    CommonDefs.GetComponentInChildren_Component_Type(item, typeof(UILabel)).color = CPlayerShopMgr.Inst:GetColor(templateId)
    return item
end
CplayerShop_NormalSearch.m_OnSelectHistoryRow_CS2LuaHook = function (this, row) 
    local templateId = 0
    repeat
        local default = this.m_searchWndType
        if default == SearchWindowType.NormalShop then
            templateId = CPlayerShopMgr.Inst.SearchHistories[row]
            break
        elseif default == SearchWindowType.AuctionShop then
            templateId = CAuctionMgr.Inst.SearchHistory[row]
            break
        end
    until 1
    if this.OnSearchCallback ~= nil then
        GenericDelegateInvoke(this.OnSearchCallback, Table2ArrayWithCount({templateId, SearchOption.All, true}, 3, MakeArrayClass(Object)))
    end
end
