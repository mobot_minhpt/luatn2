-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CChatLinkMgr = import "CChatLinkMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CShishijingWnd = import "L10.UI.CShishijingWnd"
local CTickMgr = import "L10.Engine.CTickMgr"
local CWeekendGameplayMgr = import "L10.Game.CWeekendGameplayMgr"
local CWeekendPlayBossTemplate = import "L10.UI.CWeekendPlayBossTemplate"
local EnumEventType = import "EnumEventType"
local EnumWeekendGameplayStatus = import "L10.Game.EnumWeekendGameplayStatus"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local Monster_Monster = import "L10.Game.Monster_Monster"
local NGUITools = import "NGUITools"
local System = import "System"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZhouMoActivity_ClientSetting = import "L10.Game.ZhouMoActivity_ClientSetting"
local ZhouMoActivity_Description = import "L10.Game.ZhouMoActivity_Description"
CShishijingWnd.m_Init_CS2LuaHook = function (this) 
    this.teamDamageButton:SetActive(false)
    this.rankButton:SetActive(true)
    this.enterButton:SetActive(not CWeekendGameplayMgr.Inst.isPlayerInScene)
    this.teamDamageButton:SetActive(CWeekendGameplayMgr.Inst.isPlayerInScene)
    this.background1:SetActive(false)
    this.background2:SetActive(false)
    this.progress.value = 0
    this.progressLabel.text = ""
    this.curBossInfo.text = ""
    this.curBossLevelLabel.text = ""
    Extensions.RemoveAllChildren(this.bossTable.transform)
    this.bossTemplate:SetActive(false)
    this.cdLabel.text = ""
    this.leftTimeLabel.text = ""
    Gac2Gas.QueryZhouMoActivityBossInfo()
    this:UpdateCurrentStage()
end
CShishijingWnd.m_UpdateBackground1_CS2LuaHook = function (this) 

    this.background1:SetActive(true)
    local desc = ZhouMoActivity_Description.GetData(CWeekendGameplayMgr.Inst.curStage)
    if desc ~= nil then
        this.iconTexture:LoadMaterial(desc.Icon)
        this.descLabel.text = CChatLinkMgr.TranslateToNGUIText(desc.Description, false)
    end
end
CShishijingWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.rankButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rankButton).onClick, MakeDelegateFromCSFunction(this.OnRankButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.enterButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.enterButton).onClick, MakeDelegateFromCSFunction(this.OnEnterButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tipButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tipButton).onClick, MakeDelegateFromCSFunction(this.OnTipButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.teamDamageButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.teamDamageButton).onClick, MakeDelegateFromCSFunction(this.OnTeamDamageButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.ShishijingInfoUpdate, MakeDelegateFromCSFunction(this.InfoUpdate, Action0, this))
    EventManager.AddListener(EnumEventType.ShishijingStatusChanged, MakeDelegateFromCSFunction(this.Init, Action0, this))
end
CShishijingWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.rankButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.rankButton).onClick, MakeDelegateFromCSFunction(this.OnRankButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.enterButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.enterButton).onClick, MakeDelegateFromCSFunction(this.OnEnterButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.tipButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tipButton).onClick, MakeDelegateFromCSFunction(this.OnTipButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.teamDamageButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.teamDamageButton).onClick, MakeDelegateFromCSFunction(this.OnTeamDamageButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.ShishijingInfoUpdate, MakeDelegateFromCSFunction(this.InfoUpdate, Action0, this))
    EventManager.RemoveListener(EnumEventType.ShishijingStatusChanged, MakeDelegateFromCSFunction(this.Init, Action0, this))

    if this.cancel ~= nil then
        invoke(this.cancel)
    end
end
CShishijingWnd.m_UpdateCurrentStage_CS2LuaHook = function (this) 

    if CWeekendGameplayMgr.Inst.status == EnumWeekendGameplayStatus.BossOpen then
        this.background2:SetActive(true)

        if not CWeekendGameplayMgr.Inst.isPlayerInScene then
            CommonDefs.GetComponent_GameObject_Type(this.enterButton, typeof(CButton)).Enabled = true
        end
        this.teamDamageButton:SetActive(CWeekendGameplayMgr.Inst.isPlayerInScene)
        if CWeekendGameplayMgr.Inst.enterTimes > 0 then
            this.cdLabel.text = System.String.Format(LocalString.GetString("剩余进入次数：{0}次"), CWeekendGameplayMgr.Inst.enterTimes)
        else
            this.cdLabel.text = LocalString.GetString("剩余进入次数：[c][FF0000]0[-]次")
        end
    elseif CWeekendGameplayMgr.Inst.status == EnumWeekendGameplayStatus.PlayFinish or CWeekendGameplayMgr.Inst.status == EnumWeekendGameplayStatus.KillOver then
        this.background2:SetActive(true)
        this.enterButton:SetActive(false)
    else
        this:UpdateBackground1()
        this.rankButton:SetActive(false)
        this.cdLabel.text = System.String.Format(LocalString.GetString("活动开启：{0}"), ZhouMoActivity_ClientSetting.GetData("BossOpenTime").Value)
        CommonDefs.GetComponent_GameObject_Type(this.enterButton, typeof(CButton)).Enabled = false
    end
end
CShishijingWnd.m_InfoUpdate_CS2LuaHook = function (this) 

    this:UpdateCurrentStage()
    this.progress.value = tonumber(CWeekendGameplayMgr.Inst.progress)
    this.progressLabel.text = System.String.Format("{0}%", math.floor(tonumber(CWeekendGameplayMgr.Inst.progress * 100 or 0)))
    local bossName = ""
    local level = 0
    local currentCount = 0
    do
        local i = 0
        while i < CWeekendGameplayMgr.Inst.bossList.Count do
            if not CWeekendGameplayMgr.Inst.bossList[i].hasKilled or i == CWeekendGameplayMgr.Inst.bossList.Count - 1 then
                local monster = Monster_Monster.GetData(CWeekendGameplayMgr.Inst.bossList[i].bossId)
                if monster ~= nil then
                    bossName = monster.Name
                    currentCount = i
                    CWeekendGameplayMgr.Inst.currentBossName = bossName
                end
                level = CWeekendGameplayMgr.Inst.bossList[i].bossGrade
                break
            end
            i = i + 1
        end
    end
    if not System.String.IsNullOrEmpty(bossName) then
        this.curBossInfo.text = bossName
        this.curBossLevelLabel.text = System.String.Format(LocalString.GetString("{0}级"), level)
    end
    local bossCount = math.min(CWeekendGameplayMgr.Inst.bossList.Count, 3)
    Extensions.RemoveAllChildren(this.bossTable.transform)
    this.bossTemplate:SetActive(false)
    do
        local i = 0
        while i < bossCount do
            local info = CWeekendGameplayMgr.Inst.bossList[i]
            local monster = Monster_Monster.GetData(info.bossId)
            if monster ~= nil then
                local instance = NGUITools.AddChild(this.bossTable.gameObject, this.bossTemplate)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CWeekendPlayBossTemplate)):Init(monster.HeadIcon, monster.Name, info.hasKilled, not info.hasKilled and i <= currentCount)
            end
            i = i + 1
        end
    end
    this.bossTable:Reposition()
    this:UpdateTick()

    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.cancel = CTickMgr.Register(MakeDelegateFromCSFunction(this.UpdateTick, Action0, this), 1000, ETickType.Loop)
end
CShishijingWnd.m_UpdateTick_CS2LuaHook = function (this) 

    if CWeekendGameplayMgr.Inst.enterTimes <= 0 then
        if CWeekendGameplayMgr.Inst.status == EnumWeekendGameplayStatus.QueryOpen then
            this.cdLabel.text = System.String.Format(LocalString.GetString("活动开启：{0}"), ZhouMoActivity_ClientSetting.GetData("BossOpenTime").Value)
        elseif CWeekendGameplayMgr.Inst.status ~= EnumWeekendGameplayStatus.BossOpen then
            this.cdLabel.text = ""
        end
    end
    this.leftTimeLabel.text = System.String.Format("{0}:{1}:{2}", this:FormatTime(math.floor(CWeekendGameplayMgr.Inst.leftTime / 3600)), this:FormatTime(math.floor(CWeekendGameplayMgr.Inst.leftTime % 3600 / 60)), this:FormatTime(CWeekendGameplayMgr.Inst.leftTime % 60))
    if CWeekendGameplayMgr.Inst.leftTime > 0 then
        local default = CWeekendGameplayMgr.Inst
        default.leftTime = default.leftTime - 1
    else
        if this.cancel ~= nil then
            invoke(this.cancel)
        end
        CommonDefs.GetComponent_GameObject_Type(this.enterButton, typeof(CButton)).Enabled = false
    end
end
