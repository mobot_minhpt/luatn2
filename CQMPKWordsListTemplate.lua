-- Auto Generated!!
local CQMPKWordsListTemplate = import "L10.UI.CQMPKWordsListTemplate"
local Extensions = import "Extensions"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
CQMPKWordsListTemplate.m_Init_CS2LuaHook = function (this, wordId) 
    this.m_WordTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.m_WordsTable.transform)
    if nil == wordId or wordId.Length == 0 then
        return
    end
    do
        local i = 0 local cnt = wordId.Length
        while i < cnt do
            local go = NGUITools.AddChild(this.m_WordsTable.gameObject, this.m_WordTemplate)
            go:SetActive(true)
            CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(go, typeof(UILabel), true)[0].text = L10.Game.Word_Word.GetData(wordId[i]).Description
            i = i + 1
        end
    end
    local active = this.gameObject.activeSelf
    this.gameObject:SetActive(true)
    this.m_WordsTable:Reposition()
    this.gameObject:SetActive(active)
end
