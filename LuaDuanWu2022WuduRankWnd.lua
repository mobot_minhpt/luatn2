local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local GameObject = import "UnityEngine.GameObject"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"

LuaDuanWu2022WuduRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2022WuduRankWnd, "ContentView", "ContentView", QnAdvanceGridView)
RegistChildComponent(LuaDuanWu2022WuduRankWnd, "TabView", "TabView", QnAdvanceGridView)
RegistChildComponent(LuaDuanWu2022WuduRankWnd, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaDuanWu2022WuduRankWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaDuanWu2022WuduRankWnd, "VoidLabel", "VoidLabel", GameObject)

--@endregion RegistChildComponent end

function LuaDuanWu2022WuduRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaDuanWu2022WuduRankWnd:Init()
    self:InitTab()
    self:InitRank()
    self:InitItem()

    -- 设置点击状态 发起请求
    if CClientMainPlayer.Inst then
        local clz = EnumToInt(CClientMainPlayer.Inst.Class)
        print(clz-1)
        self.TabView:ScrollToRow(clz-1)
        self.TabView:SetSelectRow(clz-1, true)
    end

    local setting = DuanWu_Setting.GetData()
	local nameList = setting.QuWuDuDifficultyName
    local header = self.transform:Find("Header")

    for i=3,5 do
        local label = header:Find(tostring(i)):GetComponent(typeof(UILabel))
        label.text = SafeStringFormat3("%s%s",nameList[i-3], LocalString.GetString("之境"))
    end
end

function LuaDuanWu2022WuduRankWnd:InitItem()
    local setting = DuanWu_Setting.GetData()
    local itemIdList = {
        setting.QuWuDu1stRewardItemId, setting.QuWuDu1stRewardItem2Id,
        setting.QuWuDu2ndRewardItemId, setting.QuWuDu2ndRewardItem2Id,
    }
    
    for i=1, 4 do
        local item = self.RewardInfo.transform:Find(i)
        local icon = item:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local itemId = itemIdList[i]

        local itemData = Item_Item.GetData(itemId)
        icon:LoadMaterial(itemData.Icon)
        
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
        end)
    end
end

function LuaDuanWu2022WuduRankWnd:InitTab()
    -- 十二个职业
    self.ClassList = {}
    for i = 1, 12 do
        table.insert(self.ClassList, i)
    end
    
    self.TabView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.ClassList
    end, function(item, index)
        local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
        label.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), index+1))
    end)

    -- 设置选中状态
    self.TabView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 请求
        local rankType = row + LuaDuanWu2022Mgr.EnumSeasonRankType.eTypeStart + 1
        Gac2Gas.SeasonRank_QueryRank(rankType, 1001)
    end)
    self.TabView:ReloadData(true,false)
end

function LuaDuanWu2022WuduRankWnd:InitRank()
    -- 排名
    self.RankList = {}
    self.ContentView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.RankList
    end, function(item, index)
        local data = self.RankList[index+1]
        self:InitRankItem(item, data, index)
    end)

    -- 设置选中状态
    self.ContentView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 显示玩家信息
        local data = self.RankList[row+1]
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, 
            EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)
end

-- Key:Score Difficulty1 Difficulty2 Difficulty3 playerId name updateTime overdueTime
function LuaDuanWu2022WuduRankWnd:InitRankItem(item, data, index)
	local playerNameLabel = item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local survivalLabel = item.transform:Find("SurvivalLabel"):GetComponent(typeof(UILabel))
	local attackLabel = item.transform:Find("AttackLabel"):GetComponent(typeof(UILabel))
	local erosionLabel = item.transform:Find("ErosionLabel"):GetComponent(typeof(UILabel))
	local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
	local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local rankSp = item.transform:Find("RankImage"):GetComponent(typeof(UISprite))
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))

    playerNameLabel.text = data.name
    survivalLabel.text = data.Difficulty1
    attackLabel.text = data.Difficulty2
    erosionLabel.text = data.Difficulty3
    scoreLabel.text = data.Score

    if data.UsedTime == "" then
        timeLabel.text = ""
    else
        local sec = data.UsedTime%60
        local min = math.floor(data.UsedTime/60)
        timeLabel.text = min .. ":" .. sec
    end
    
    local rank = index + 1

    local rankImgList = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}

    rankLabel.gameObject:SetActive(true)
    if rank <= 3 and rank >= 1 then
        rankLabel.text = ""
        rankSp.gameObject:SetActive(true)
        rankSp.spriteName = rankImgList[rank]
    else
        if rank == 0 then
            rankLabel.text = LocalString.GetString("未上榜")
        else
            rankLabel.text = tostring(rank)
        end
        rankSp.gameObject:SetActive(false)
    end
end

function LuaDuanWu2022WuduRankWnd:OnData(rankType, seasonId, selfDataU, rankDataU)
    self.MyData, self.RankList = LuaDuanWu2022Mgr:TransformRankData(selfDataU, rankDataU)

    if self.MyData.Score == nil then
        self.MyData.name = CClientMainPlayer.Inst.Name
        self.MyData.playerId = CClientMainPlayer.Inst.Id
        self.MyData.Difficulty1 = ""
        self.MyData.Difficulty2 = ""
        self.MyData.Difficulty3 = ""
        self.MyData.Score = ""
        self.MyData.UsedTime = ""
        self.MyData.rank = 0
    end
    self:InitRankItem(self.MainPlayerInfo, self.MyData, self.MyData.rank-1)

    local empty = (#self.RankList == 0)
    self.VoidLabel:SetActive(empty)
    self.MainPlayerInfo:SetActive(not empty)

    self.ContentView:ReloadData(true,false)
end

function LuaDuanWu2022WuduRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnData")
end

function LuaDuanWu2022WuduRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnData")
end


--@region UIEvent

--@endregion UIEvent

