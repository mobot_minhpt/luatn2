local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local NGUIText = import "NGUIText"

LuaNationalDayTXZREnterWnd = class()

RegistClassMember(LuaNationalDayTXZREnterWnd, "m_CloseButton")
RegistClassMember(LuaNationalDayTXZREnterWnd, "m_InfoButton")

RegistClassMember(LuaNationalDayTXZREnterWnd, "m_SignupRoot")
RegistClassMember(LuaNationalDayTXZREnterWnd, "m_CancelRoot")

RegistClassMember(LuaNationalDayTXZREnterWnd, "m_AvailableTimeLabel")
RegistClassMember(LuaNationalDayTXZREnterWnd, "m_DescriptionLabel")
RegistClassMember(LuaNationalDayTXZREnterWnd, "m_RemainTimesLabel")
RegistClassMember(LuaNationalDayTXZREnterWnd, "m_SignupButton")
RegistClassMember(LuaNationalDayTXZREnterWnd, "m_CancelButton")


function LuaNationalDayTXZREnterWnd:Init()

	self.m_CloseButton = self.transform:Find("Anchor/CloseButton").gameObject
	self.m_InfoButton = self.transform:Find("Anchor/InfoButton").gameObject
	self.m_SignupRoot = self.transform:Find("Anchor/SignupRoot").gameObject
	self.m_CancelRoot = self.transform:Find("Anchor/CancelRoot").gameObject
	self.m_AvailableTimeLabel = self.transform:Find("Anchor/SignupRoot/TimeLabel"):GetComponent(typeof(UILabel))
	self.m_DescriptionLabel = self.transform:Find("Anchor/SignupRoot/DescLabel"):GetComponent(typeof(UILabel))
	self.m_RemainTimesLabel = self.transform:Find("Anchor/SignupRoot/RemainTimes/Label"):GetComponent(typeof(UILabel))
	self.m_SignupButton = self.transform:Find("Anchor/SignupRoot/SignupButton").gameObject
	self.m_CancelButton = self.transform:Find("Anchor/CancelRoot/CancelButton").gameObject

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_SignupButton, DelegateFactory.Action_GameObject(function(go) self:OnSignupButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_CancelButton, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick() end), false)

	
	self.m_SignupRoot:SetActive(false)
	self.m_CancelRoot:SetActive(false)

	LuaNationalDayMgr.TXZRCheckMatching()
end

function LuaNationalDayTXZREnterWnd:InitContent(isMatching)
	if isMatching then
		self.m_SignupRoot:SetActive(false)
		self.m_CancelRoot:SetActive(true)
	else
		self.m_SignupRoot:SetActive(true)
		self.m_CancelRoot:SetActive(false)

		self.m_AvailableTimeLabel.text = GuoQingXianLi_TXZR.GetData().DurationDesc
		self.m_DescriptionLabel.text = g_MessageMgr:FormatMessage("NationalDay_TXZR_Sign_Up_Desc")
		local usedTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eNationalDayTXZRRewardTimes) or 0
		local remainTimes = math.max(0, GuoQingXianLi_TXZR.GetData().NormalRewardLimit - usedTimes)
		self.m_RemainTimesLabel.text = tostring(remainTimes)
		if remainTimes>0 then
			self.m_RemainTimesLabel.color = NGUIText.ParseColor24("02b900", 0)
		else
			self.m_RemainTimesLabel.color = Color.red
		end
	end
end

function LuaNationalDayTXZREnterWnd:OnCloseButtonClick()
	self:Close()
end

function LuaNationalDayTXZREnterWnd:OnInfoButtonClick()
	g_MessageMgr:ShowMessage("NationalDay_TXZR_Rule")
end

function LuaNationalDayTXZREnterWnd:OnSignupButtonClick()
	LuaNationalDayMgr.TXZRSignUpPlay()
end

function LuaNationalDayTXZREnterWnd:OnCancelButtonClick()
	LuaNationalDayMgr.TXZRCancelSignUp()
end

function LuaNationalDayTXZREnterWnd:OnEnable()
	g_ScriptEvent:AddListener("OnTXZRSignUpPlayResult", self, "OnTXZRSignUpPlayResult")
	g_ScriptEvent:AddListener("OnTXZRCancelSignUpResult", self, "OnTXZRCancelSignUpResult")
	g_ScriptEvent:AddListener("OnTXZRCheckInMatchingResult", self, "OnTXZRCheckInMatchingResult")
end

function LuaNationalDayTXZREnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnTXZRSignUpPlayResult", self, "OnTXZRSignUpPlayResult")
	g_ScriptEvent:RemoveListener("OnTXZRCancelSignUpResult", self, "OnTXZRCancelSignUpResult")
	g_ScriptEvent:RemoveListener("OnTXZRCheckInMatchingResult", self, "OnTXZRCheckInMatchingResult")
end

function LuaNationalDayTXZREnterWnd:OnTXZRSignUpPlayResult(bSussess)
	self:InitContent(bSussess)
end

function LuaNationalDayTXZREnterWnd:OnTXZRCancelSignUpResult(bSussess)
	self:InitContent(not bSussess)
end

function LuaNationalDayTXZREnterWnd:OnTXZRCheckInMatchingResult(bSussess)
	self:InitContent(bSussess)
end

function LuaNationalDayTXZREnterWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
