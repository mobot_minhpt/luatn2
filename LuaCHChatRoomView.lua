local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaCHChatRoomView = class()

RegistClassMember(LuaCHChatRoomView, "m_RoomListView")
RegistClassMember(LuaCHChatRoomView, "m_RoomContentView")

RegistClassMember(LuaCHChatRoomView, "m_MengDaoProfileQueryTick")

function LuaCHChatRoomView:Awake()
    self.m_RoomListView = self.transform:Find("RoomListView").gameObject
    self.m_RoomContentView = self.transform:Find("RoomContentView").gameObject
end

function LuaCHChatRoomView:Init()
    local view = LuaClubHouseMgr.m_ChatRoomView or "RoomContentView"
    if LuaClubHouseMgr:IsInRoom() and view == "RoomContentView" then
        self:ShowRoomContentView()
    else
        self:ShowRoomListView(true)
    end
    LuaClubHouseMgr:CheckAndConfirmCCRoom()
end

function LuaCHChatRoomView:OnEnable()
    if LuaClubHouseMgr.m_UseMengDaoProfile then
        self:StartMengDaoProfileQueryTick()
    end
    g_ScriptEvent:AddListener("ClubHouse_Notify_Reset", self, "OnClubHouseNotifyReset")
    g_ScriptEvent:AddListener("ClubHouse_Notify_DelRoom", self, "OnClubHouseNotifyDelRoom")
    g_ScriptEvent:AddListener("ClubHouse_CreateRoom_Result", self, "OnClubHouseCreateRoomResult")
    g_ScriptEvent:AddListener("ClubHouse_EnterRoom_Result", self, "OnClubHouseEnterRoomResult")
    g_ScriptEvent:AddListener("ClubHouse_Myself_Leave_Room", self, "OnClubHouseMyselfLeaveRoom")

    g_ScriptEvent:AddListener("RoomContentView_OnBackButtonClick", self, "RoomContentViewOnBackButtonClick")
    g_ScriptEvent:AddListener("RoomListView_OnRoomButtonClick", self, "RoomListViewOnRoomButtonClick")
end

function LuaCHChatRoomView:OnDisable()
    self:StopMengDaoProfileQueryTick()
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_Reset", self, "OnClubHouseNotifyReset")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_DelRoom", self, "OnClubHouseNotifyDelRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_CreateRoom_Result", self, "OnClubHouseCreateRoomResult")
    g_ScriptEvent:RemoveListener("ClubHouse_EnterRoom_Result", self, "OnClubHouseEnterRoomResult")
    g_ScriptEvent:RemoveListener("ClubHouse_Myself_Leave_Room", self, "OnClubHouseMyselfLeaveRoom")

    g_ScriptEvent:RemoveListener("RoomContentView_OnBackButtonClick", self, "RoomContentViewOnBackButtonClick")
    g_ScriptEvent:RemoveListener("RoomListView_OnRoomButtonClick", self, "RoomListViewOnRoomButtonClick")
end

function LuaCHChatRoomView:OnClubHouseNotifyReset()
    self:ShowRoomListView(false)
end

function LuaCHChatRoomView:OnClubHouseNotifyDelRoom(roomId)
    self:ShowRoomListView(false)
end

function LuaCHChatRoomView:OnClubHouseCreateRoomResult(playerId, bSuccess)
    if bSuccess then
        self:ShowRoomContentView()
    end
end

function LuaCHChatRoomView:OnClubHouseEnterRoomResult(playerId, bSuccess, isClientRequest)
    if bSuccess and isClientRequest then
        self:ShowRoomContentView()
    end
end

function LuaCHChatRoomView:OnClubHouseMyselfLeaveRoom()
    self:ShowRoomListView(false)
end

function LuaCHChatRoomView:RoomContentViewOnBackButtonClick()
    self:ShowRoomListView(false)
end

function LuaCHChatRoomView:RoomListViewOnRoomButtonClick()
    if LuaClubHouseMgr:IsInRoom() then
        self:ShowRoomContentView()
    end
end

function LuaCHChatRoomView:ShowRoomListView(isFirst)
    self.m_RoomListView:SetActive(true)
    self.m_RoomContentView:SetActive(false)
    local lua = self.m_RoomListView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    if not isFirst and LuaClubHouseMgr.m_ChatRoomView == "RoomListView" then
        lua:InitBottomDisplay()
    else
        LuaClubHouseMgr.m_ChatRoomView = "RoomListView"
        lua:Init()
    end
end


function LuaCHChatRoomView:ShowRoomContentView()
    LuaClubHouseMgr.m_ChatRoomView = "RoomContentView"
    self.m_RoomListView:SetActive(false)
    self.m_RoomContentView:SetActive(true)
    local lua = self.m_RoomContentView:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    lua:Init()
end

function LuaCHChatRoomView:UpdateAnchors()
    if self.m_RoomListView.activeSelf then
        self:ShowRoomListView()
    else
        self:ShowRoomContentView()
    end
end

function LuaCHChatRoomView:StartMengDaoProfileQueryTick()
    self:StopMengDaoProfileQueryTick()
    self.m_MengDaoProfileQueryTick = RegisterTick(function()
        LuaClubHouseMgr:GetUserProfile()
    end, 1000)
end

function LuaCHChatRoomView:StopMengDaoProfileQueryTick()
    if self.m_MengDaoProfileQueryTick then
        UnRegisterTick(self.m_MengDaoProfileQueryTick)
        self.m_MengDaoProfileQueryTick = nil
    end
    LuaClubHouseMgr:ClearMengDaoProfileQueryList()
end
