local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local Animation = import "UnityEngine.Animation"
LuaDaFuWongRuleWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongRuleWnd, "Center", "Center", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "LeftFirstPage", "LeftFirstPage", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "RightFirstPage", "RightFirstPage", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "LeftOtherPage", "LeftOtherPage", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "RightOtherPage", "RightOtherPage", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "EmptyPage", "EmptyPage", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "LeftTabTable", "LeftTabTable", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "RightTabTable", "RightTabTable", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "LeftBtn", "LeftBtn", QnButton)
RegistChildComponent(LuaDaFuWongRuleWnd, "RightBtn", "RightBtn", QnButton)
RegistChildComponent(LuaDaFuWongRuleWnd, "RightTabTemplate", "RightTabTemplate", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "LeftTabTemplate", "LeftTabTemplate", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "RuleTemplate", "RuleTemplate", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "PageDotTemplate", "PageDotTemplate", GameObject)
RegistChildComponent(LuaDaFuWongRuleWnd, "Pool", "Pool", CUIGameObjectPool)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongRuleWnd, "m_MaxParagraph")
RegistClassMember(LuaDaFuWongRuleWnd, "m_Page")
RegistClassMember(LuaDaFuWongRuleWnd, "m_TabList")
RegistClassMember(LuaDaFuWongRuleWnd, "m_Tab2Go")
RegistClassMember(LuaDaFuWongRuleWnd, "m_ParagraphList")
RegistClassMember(LuaDaFuWongRuleWnd, "m_DotList")
RegistClassMember(LuaDaFuWongRuleWnd, "m_CurTab")
RegistClassMember(LuaDaFuWongRuleWnd, "m_MaxPage")
RegistClassMember(LuaDaFuWongRuleWnd, "m_Tab2PageNum")
RegistClassMember(LuaDaFuWongRuleWnd, "m_Tab2Paragraphs")
RegistClassMember(LuaDaFuWongRuleWnd, "m_Anim")
RegistClassMember(LuaDaFuWongRuleWnd, "m_ShowPageTick")
RegistClassMember(LuaDaFuWongRuleWnd, "m_IsInPageTick")
RegistClassMember(LuaDaFuWongRuleWnd, "m_NormalSize")
function LuaDaFuWongRuleWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)


	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
    UIEventListener.Get(self.Center.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
        self:OnDragPage(go,delta)
    end)
    self.m_MaxParagraph = 4     -- 一页最多显示的段落数

    self.m_Page = 0             -- 当前页码
    self.m_CurTab = 0           -- 当前选择的分类

    self.m_MaxPage = 0          -- 总页数
    self.m_ParagraphList = nil  -- 每项规则ID对应规则
    self.m_TabList = nil        -- 所有分类
    self.m_Tab2Paragraphs = nil -- 分类对应段落
    self.m_Tab2PageNum = nil    -- 每个分类对应的页数{开始页，总页数}

    self.m_DotList = nil        -- 页码标点object
    self.m_Tab2Go = nil         -- 分类对应object
    self.transform:Find("Content/Template").gameObject:SetActive(false)
    self.m_Anim = self.gameObject:GetComponent(typeof(Animation))
    self.m_ShowPageTick = nil
    self.m_IsInPageTick = false
    self.m_NormalSize = {226,190}
end

function LuaDaFuWongRuleWnd:Init()
    local Page = LuaDaFuWongMgr.RulePage
    self:InitData()
    self:InitTabView()
    --self:InitPage()
    self:UpdatePage(Page)
    self.m_IsInPageTick = true
    self.m_ShowPageTick = RegisterTickOnce(function ()
        self.m_IsInPageTick = false
    end,1000)
end
function LuaDaFuWongRuleWnd:InitData()
    self.m_TabList = {}
    self.m_Tab2Paragraphs = {}
    self.m_ParagraphList = {}
    -- 段落
    DaFuWeng_Rule.Foreach(function(k,v)
        self.m_ParagraphList[v.ID] = {ID = v.ID, TitleName = v.TitleName, PicPath = v.PicPath, Content = v.Content, picSize = v.PicSize}
        local type = v.Type 
        if not self.m_Tab2Paragraphs[type] then
            table.insert(self.m_TabList,type)
            self.m_Tab2Paragraphs[type] = {}
        end
        table.insert(self.m_Tab2Paragraphs[type],v.ID)
    end)

    self.m_Tab2PageNum = {}
    -- 页码
    self.m_MaxPage = 0
    for i = 1,#self.m_TabList do
        local type = self.m_TabList[i]
        local pageNum = math.ceil(#self.m_Tab2Paragraphs[type] / self.m_MaxParagraph)
        local pageData = {}
        self.m_MaxPage = self.m_MaxPage + 1
        pageData.StartPage = self.m_MaxPage
        pageData.PageNum = pageNum
        self.m_Tab2PageNum[type] = pageData
        self.m_MaxPage = self.m_MaxPage + pageNum - 1
    end
end

function LuaDaFuWongRuleWnd:InitTabView()
    self.m_Tab2Go = {}
    for i = 1,#self.m_TabList do
        local type = self.m_TabList[i]
        local TabInfo = DaFuWeng_RuleTab.GetData(type)
        if TabInfo then
            local left = self.LeftTabTable.transform:GetChild(i-1)
            local right = self.RightTabTable.transform:GetChild(i-1)
            local lSelect = left.transform:Find("Select").gameObject
            local lUnSelect = left.transform:Find("UnSelect").gameObject
            lSelect.transform:Find("Label"):GetComponent(typeof(UILabel)).text = TabInfo.TitleName
            lUnSelect.transform:Find("Label"):GetComponent(typeof(UILabel)).text = TabInfo.TitleName
            right.transform:Find("Label"):GetComponent(typeof(UILabel)).text = TabInfo.TitleName
            local data = { left = left, right = right, lSelect = lSelect, lUnSelect = lUnSelect}
            self.m_Tab2Go[type] = data
            UIEventListener.Get(left.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:UpdatePage(self.m_Tab2PageNum[type].StartPage)
            end)
            UIEventListener.Get(right.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                self:UpdatePage(self.m_Tab2PageNum[type].StartPage)
            end)
        end
    end
end
function LuaDaFuWongRuleWnd:InitPage()
    local leftContent = self.LeftOtherPage.transform
    local rightContent = self.RightOtherPage.transform
    -- Extensions.RemoveAllChildren(leftContent.transform)
    -- Extensions.RemoveAllChildren(leftContent.transform)
    for i = 1,self.m_MaxParagraph / 2 do
        local left = leftContent:GetChild(i-1)
        local right = rightContent:GetChild(i-1)
        left.gameObject:SetActive(true)
        right.gameObject:SetActive(true)
    end
end
function LuaDaFuWongRuleWnd:UpdatePage(page)
    if self.m_IsInPageTick then return end
    local newPage = math.min(math.max(1,page),self.m_MaxPage)
    if self.m_Page == newPage then return end
    if self.m_Page == 0 then self:UpdatePageInfo(newPage) return end
    if self.m_ShowPageTick then UnRegisterTick(self.m_ShowPageTick) self.m_ShowPageTick = nil end
    self.m_IsInPageTick = true
    if newPage > self.m_Page then
        self.transform:Find("Wnd_Bg/dafuwongrulewnd_book_02/fanye/R").gameObject:SetActive(false)
        self.m_Anim["dafuwongrulewnd_turnpage_r"].time = 0
        self.m_Anim:Play("dafuwongrulewnd_turnpage_r")
    elseif newPage < self.m_Page then
        self.transform:Find("Wnd_Bg/dafuwongrulewnd_book_02/fanye/L").gameObject:SetActive(false)
        self.m_Anim["dafuwongrulewnd_turnpage_l"].time = 0
        self.m_Anim:Play("dafuwongrulewnd_turnpage_l")
    end
    self.m_ShowPageTick = RegisterTickOnce(function ()
        self:UpdatePageInfo(newPage)
        self.m_IsInPageTick = false
    end,200)
end

function LuaDaFuWongRuleWnd:UpdatePageInfo(page)
    self.m_Page = page
    self:UpdateTab()
    self:UpdatePageDot()
    self:UpdateBtn()
    if self.m_Page == 1 then
        self:ShowFirstPage()
    else
        self:ShowOtherPage()
    end
end
function LuaDaFuWongRuleWnd:UpdateTab()
    local page = self.m_Page
    local typeIndex = #self.m_TabList
    for i = 1,#self.m_TabList do
        local tabType = self.m_TabList[i]
        local pageInfo = self.m_Tab2PageNum[tabType]
        if page >= pageInfo.StartPage and page < pageInfo.StartPage + pageInfo.PageNum then
            typeIndex = i
            self.m_CurTab = tabType
        end
        self.m_Tab2Go[tabType].left.gameObject:SetActive(typeIndex >= i)
        self.m_Tab2Go[tabType].right.gameObject:SetActive(typeIndex < i)
        self.m_Tab2Go[tabType].lSelect.gameObject:SetActive(typeIndex == i)
        self.m_Tab2Go[tabType].lUnSelect.gameObject:SetActive(typeIndex > i)
    end 
end
function LuaDaFuWongRuleWnd:ShowFirstPage()
    self.LeftFirstPage.gameObject:SetActive(true)
    self.LeftOtherPage.gameObject:SetActive(false)
    self.RightFirstPage.gameObject:SetActive(true)
    self.RightOtherPage.gameObject:SetActive(false)
    self.EmptyPage.gameObject:SetActive(false)
    local data = self.m_ParagraphList[self.m_Tab2Paragraphs[self.m_TabList[1]][1]]
    --self.LeftFirstPage.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(data.PicPath)
    self.RightFirstPage.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.TitleName
    self.RightFirstPage.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[894B34]%s[-]",data.Content) 
end
function LuaDaFuWongRuleWnd:ShowOtherPage()
    self.LeftFirstPage.gameObject:SetActive(false)
    self.LeftOtherPage.gameObject:SetActive(true)
    self.RightFirstPage.gameObject:SetActive(false)
    self.RightOtherPage.gameObject:SetActive(true)
    local paragraphs = self.m_Tab2Paragraphs[self.m_CurTab]
    local curPage = self.m_Page - self.m_Tab2PageNum[self.m_CurTab].StartPage
    local startIndex = curPage * self.m_MaxParagraph + 1
    local endIndex = math.max(startIndex,math.min(startIndex + self.m_MaxParagraph,#paragraphs))
    local leftContent = self.LeftOtherPage.transform
    local rightContent = self.RightOtherPage.transform

    for i = 0,leftContent.transform.childCount - 1 do
        local go = leftContent.transform:GetChild(i).gameObject
        self:UpdateParagraph(go,paragraphs[startIndex + i])
    end
    for i = 0,rightContent.transform.childCount - 1 do
        local go = rightContent.transform:GetChild(i).gameObject
        self:UpdateParagraph(go,paragraphs[startIndex + i + self.m_MaxParagraph / 2])
    end

    self.RightOtherPage.gameObject:SetActive(endIndex - startIndex > self.m_MaxParagraph / 2)
    self.EmptyPage.gameObject:SetActive(endIndex - startIndex <= self.m_MaxParagraph / 2)
    if endIndex - startIndex <= self.m_MaxParagraph / 2 then
        self.EmptyPage.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("没有更多%s啦"),DaFuWeng_RuleTab.GetData(self.m_CurTab).TitleName)
    end
end
function LuaDaFuWongRuleWnd:UpdatePageDot()
    local pageNum = self.m_Tab2PageNum[self.m_CurTab].PageNum
    local startPage = self.m_Tab2PageNum[self.m_CurTab].StartPage
    local curPage = self.m_Page - startPage + 1

    local PageNum = self.transform:Find("Content/PageNum/Label"):GetComponent(typeof(UILabel))
    PageNum.text = SafeStringFormat3("%d/%d",curPage,pageNum)
end
function LuaDaFuWongRuleWnd:UpdateBtn()
    self.LeftBtn.gameObject:SetActive(self.m_Page ~= 1)
    self.RightBtn.gameObject:SetActive(self.m_Page ~= self.m_MaxPage)
end
function LuaDaFuWongRuleWnd:UpdateParagraph(go,dataId)
    if not dataId then go.gameObject:SetActive(false) return end
    local data = self.m_ParagraphList[dataId] 
    if not data then go.gameObject:SetActive(false) return end
    go.gameObject:SetActive(true)
    local title = go.transform:Find("Title").gameObject
    if not System.String.IsNullOrEmpty(data.TitleName) then
        title.gameObject:SetActive(true)
        title:GetComponent(typeof(UILabel)).text = data.TitleName
    else
        title.gameObject:SetActive(false)
    end
    go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("[894B34]%s[-]",data.Content) 
    go.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.PicPath)
    local picSize = {data.picSize[0],data.picSize[1]}
    local sizeRate = picSize[1]/picSize[2]
    if picSize[1] > self.m_NormalSize[1] then
        picSize[2] = self.m_NormalSize[1] / sizeRate
        picSize[1] = self.m_NormalSize[1]
    end
    if picSize[2] > self.m_NormalSize[2] then
        picSize[1] = self.m_NormalSize[2] * sizeRate
        picSize[2] = self.m_NormalSize[2]
    end
    local iconTexture = go.transform:Find("Icon"):GetComponent(typeof(UITexture))
    iconTexture.width = picSize[1]
    iconTexture.height = picSize[2]
end

function LuaDaFuWongRuleWnd:OnDragPage(go,delta)
    --接完动效在做
    local moveDeg = 57.2958*math.asin(math.max(-1,math.min(delta.x/400,1)))
    if moveDeg > 5 then
        self:UpdatePage(self.m_Page - 1)
    elseif moveDeg < -5 then
        self:UpdatePage(self.m_Page + 1)
    end
end

--@region UIEvent

function LuaDaFuWongRuleWnd:OnLeftBtnClick()
    self:UpdatePage(self.m_Page - 1)
end

function LuaDaFuWongRuleWnd:OnRightBtnClick()
    self:UpdatePage(self.m_Page + 1)
end


--@endregion UIEvent

function LuaDaFuWongRuleWnd:OnDisable()
    LuaDaFuWongMgr.RulePage = self.m_Page
    if self.m_ShowPageTick then UnRegisterTick(self.m_ShowPageTick) self.m_ShowPageTick = nil end
end