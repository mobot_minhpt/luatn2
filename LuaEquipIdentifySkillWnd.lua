require("common/common_include")
require("ui/equip/identifyskill/LuaEquipSkillIdentifyView")
require("ui/equip/identifyskill/LuaEquipSkillTransferView")
require("ui/equip/identifyskill/LuaEquipSkillExtendView")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaEquipIdentifySkillWnd = class()
RegistClassMember(LuaEquipIdentifySkillWnd,"closeBtn")
RegistClassMember(LuaEquipIdentifySkillWnd,"titleLabel")
RegistClassMember(LuaEquipIdentifySkillWnd,"tabBar")
RegistClassMember(LuaEquipIdentifySkillWnd,"skillIdentifyViewRoot")
RegistClassMember(LuaEquipIdentifySkillWnd,"skillTransferViewRoot")
RegistClassMember(LuaEquipIdentifySkillWnd,"skillExtendViewRoot")
RegistClassMember(LuaEquipIdentifySkillWnd,"m_skillIdentifyView")
RegistClassMember(LuaEquipIdentifySkillWnd,"m_skillTransferView")
RegistClassMember(LuaEquipIdentifySkillWnd,"m_skillExtendView")

function LuaEquipIdentifySkillWnd:Awake()
    
end

function LuaEquipIdentifySkillWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaEquipIdentifySkillWnd:Init()
	self.closeBtn = self.transform:Find("Wnd_Bg_Primary_Tab/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.closeBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	self.titleLabel = self.transform:Find("Wnd_Bg_Primary_Tab/TitleLabel"):GetComponent(typeof(UILabel))
	self.tabBar = self.transform:GetComponent(typeof(UITabBar))

	self.skillIdentifyViewRoot = self.transform:Find("Anchor/SkillIdentifyView").gameObject
	self.skillTransferViewRoot = self.transform:Find("Anchor/SkillTransferView").gameObject
	self.skillExtendViewRoot = self.transform:Find("Anchor/SkillExtendView").gameObject

	if not self.m_skillIdentifyView then
		self.m_skillIdentifyView = LuaEquipSkillIdentifyView:new()
		self.m_skillIdentifyView:InitWithGameObject(self.skillIdentifyViewRoot)
	end
	if not self.m_skillTransferView then
		self.m_skillTransferView = LuaEquipSkillTransferView:new()
		self.m_skillTransferView:InitWithGameObject(self.skillTransferViewRoot)
	end

	if not self.m_skillExtendView then
		self.m_skillExtendView = LuaEquipSkillExtendView:new()
		self.m_skillExtendView:InitWithGameObject(self.skillExtendViewRoot)
	end
	
	self.tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) self:OnTabChange(go, index) end)
	
	local index = LuaEquipIdentifySkillMgr.m_InitialWndIndex
	if index < 0 or index > 2 then
		index = 0
	end

	if index == 0 then
		self.m_skillIdentifyView:SetDefaultItem(LuaEquipIdentifySkillMgr.m_InitialEquipId)
		self.m_skillExtendView:SetDefaultItem(nil)
	elseif index ==2 then
		self.m_skillIdentifyView:SetDefaultItem(nil)
		self.m_skillExtendView:SetDefaultItem(LuaEquipIdentifySkillMgr.m_InitialEquipId)
	else
		self.m_skillIdentifyView:SetDefaultItem(nil)
		self.m_skillExtendView:SetDefaultItem(nil)
	end

	self.tabBar:ChangeTab(index)
end

function LuaEquipIdentifySkillWnd:OnTabChange(go, index)
	self.skillIdentifyViewRoot:SetActive(index == 0)
	self.skillTransferViewRoot:SetActive(index == 1)
	self.skillExtendViewRoot:SetActive(index == 2)
	if index == 0 then
		self.titleLabel.text = LocalString.GetString("鉴定技能")
		self.m_skillIdentifyView:Init()
	elseif index == 1 then
		self.titleLabel.text = LocalString.GetString("转移技能")
		self.m_skillTransferView:Init()
	elseif index == 2 then
		self.titleLabel.text = LocalString.GetString("补益技能")
		self.m_skillExtendView:Init()
	end
end

function LuaEquipIdentifySkillWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
	g_ScriptEvent:AddListener("IdentifySkillWashCost", self, "IdentifySkillWashCost")
	g_ScriptEvent:AddListener("IdentifySkillExchangeCost", self, "IdentifySkillExchangeCost")
	g_ScriptEvent:AddListener("IdentifySkillExtendCost", self, "IdentifySkillExtendCost")

	--各种成功的通知
	g_ScriptEvent:AddListener("IdentifySkillRequestWashSkillSuccess", self, "IdentifySkillRequestWashSkillSuccess")
	g_ScriptEvent:AddListener("IdentifySkillRequestApplyTempSkillSuccess", self, "IdentifySkillRequestApplyTempSkillSuccess")
	g_ScriptEvent:AddListener("IdentifySkillRequestExtendSuccess", self, "IdentifySkillRequestExtendSuccess")
	g_ScriptEvent:AddListener("IdentifySkillRequestExchangeSuccess", self, "IdentifySkillRequestExchangeSuccess")

end

function LuaEquipIdentifySkillWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
	g_ScriptEvent:RemoveListener("IdentifySkillWashCost", self, "IdentifySkillWashCost")
	g_ScriptEvent:RemoveListener("IdentifySkillExchangeCost", self, "IdentifySkillExchangeCost")
	g_ScriptEvent:RemoveListener("IdentifySkillExtendCost", self, "IdentifySkillExtendCost")

	g_ScriptEvent:RemoveListener("IdentifySkillRequestWashSkillSuccess", self, "IdentifySkillRequestWashSkillSuccess")
	g_ScriptEvent:RemoveListener("IdentifySkillRequestApplyTempSkillSuccess", self, "IdentifySkillRequestApplyTempSkillSuccess")
	g_ScriptEvent:RemoveListener("IdentifySkillRequestExtendSuccess", self, "IdentifySkillRequestExtendSuccess")
	g_ScriptEvent:RemoveListener("IdentifySkillRequestExchangeSuccess", self, "IdentifySkillRequestExchangeSuccess")
end

function LuaEquipIdentifySkillWnd:OnSendItem(args)
	local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)

	if not item then return end

	if self.skillIdentifyViewRoot.activeSelf then
		self.m_skillIdentifyView:OnSendItem(itemId)
	elseif self.skillTransferViewRoot.activeSelf then
		self.m_skillTransferView:OnSendItem(itemId)
	elseif self.skillExtendViewRoot.activeSelf then
		self.m_skillExtendView:OnSendItem(itemId)
	end
end

function LuaEquipIdentifySkillWnd:OnSetItemAt(args)
	local place = args[0]
	local pos = args[1]
	local oldItemId = args[2]
	local newItemId = args[3]

	if place ~= EnumItemPlace.Bag then return end

	if self.skillIdentifyViewRoot.activeSelf then
		self.m_skillIdentifyView:OnSetItemAt(place, pos, oldItemId, newItemId)
	elseif self.skillTransferViewRoot.activeSelf then
		self.m_skillTransferView:OnSetItemAt(place, pos, oldItemId, newItemId)
	elseif self.skillExtendViewRoot.activeSelf then
		self.m_skillExtendView:OnSetItemAt(place, pos, oldItemId, newItemId)
	end
end

function LuaEquipIdentifySkillWnd:IdentifySkillWashCost(equipId, list)
	if self.skillIdentifyViewRoot.activeSelf then
		self.m_skillIdentifyView:UpdateCostInfo(equipId, list)
	end
end

function LuaEquipIdentifySkillWnd:IdentifySkillExchangeCost(equip1Id, equip2Id, needJade, itemTemplateId1, num1, itemTemplateId2, num2)
	if self.skillTransferViewRoot.activeSelf then
		self.m_skillTransferView:UpdateCostInfo(equip1Id, equip2Id, needJade, itemTemplateId1, num1, itemTemplateId2, num2)
	end
end

function LuaEquipIdentifySkillWnd:IdentifySkillExtendCost(equipId, extendTimes, needMoney, itemTemplateId, num)
	if self.skillExtendViewRoot.activeSelf then
		self.m_skillExtendView:UpdateCostInfo(equipId, itemTemplateId, num)
	end
end

function LuaEquipIdentifySkillWnd:IdentifySkillRequestWashSkillSuccess(equipId, washItemType, curSkillChanged, tmpSkillChanged, oldTmpSkillId, newTmpSkillId)
	if self.skillIdentifyViewRoot.activeSelf then
		self.m_skillIdentifyView:IdentifySkillRequestWashSkillSuccess(equipId, washItemType, curSkillChanged, tmpSkillChanged, oldTmpSkillId, newTmpSkillId)
	end
end

function LuaEquipIdentifySkillWnd:IdentifySkillRequestApplyTempSkillSuccess(equipId)
	if self.skillIdentifyViewRoot.activeSelf then
		self.m_skillIdentifyView:IdentifySkillRequestApplyTempSkillSuccess(equipId)
	end
end

function LuaEquipIdentifySkillWnd:IdentifySkillRequestExtendSuccess(equipId)
	if self.skillExtendViewRoot.activeSelf then
		self.m_skillExtendView:IdentifySkillRequestExtendSuccess(equipId)
	end
end

function LuaEquipIdentifySkillWnd:IdentifySkillRequestExchangeSuccess(equipId1, equipId2)
	if self.skillTransferViewRoot.activeSelf then
		self.m_skillTransferView:IdentifySkillRequestExchangeSuccess(equipId1, equipId2)
	end
end

return LuaEquipIdentifySkillWnd
