local UILabel = import "UILabel"
local UISlider = import "UISlider"
local UISprite = import "UISprite"
local DelegateFactory = import "DelegateFactory"
local LuaTweenUtils = import "LuaTweenUtils"
local QnButton = import "L10.UI.QnButton"
local CUIManager = import "L10.UI.CUIManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaStarBiwuStopVoteWnd = class()

RegistClassMember(CLuaStarBiwuStopVoteWnd,"m_NameLab")
RegistClassMember(CLuaStarBiwuStopVoteWnd,"m_Votes")
RegistClassMember(CLuaStarBiwuStopVoteWnd,"m_Timer")
RegistClassMember(CLuaStarBiwuStopVoteWnd,"m_OKBtn")
RegistClassMember(CLuaStarBiwuStopVoteWnd,"m_CancelBtn")
RegistClassMember(CLuaStarBiwuStopVoteWnd,"m_OKLab")
RegistClassMember(CLuaStarBiwuStopVoteWnd,"m_CancelLab")
RegistClassMember(CLuaStarBiwuStopVoteWnd, "m_Tweener")

function CLuaStarBiwuStopVoteWnd:Awake()
    self:InitComponents()
end

function CLuaStarBiwuStopVoteWnd:InitComponents()
    self.m_NameLab  = self.transform:Find("Anchor/NameLab"):GetComponent(typeof(UILabel))
    self.m_Votes    = self.transform:Find("Anchor/Votes").gameObject
    self.m_Timer    = self.transform:Find("Anchor/Timer"):GetComponent(typeof(UISlider))
    self.m_OKBtn    = self.transform:Find("Anchor/OKBtn"):GetComponent(typeof(QnButton))
    self.m_CancelBtn= self.transform:Find("Anchor/CancelBtn"):GetComponent(typeof(QnButton))
    self.m_OKLab    = self.transform:Find("Anchor/OKLab"):GetComponent(typeof(UILabel))
    self.m_CancelLab= self.transform:Find("Anchor/CancelLab"):GetComponent(typeof(UILabel))
end

function CLuaStarBiwuStopVoteWnd:Init()
    local voteExpire = StarBiWuShow_Setting.GetData().EarlyStopExpire
    --断线重连需要调整倒计时
    local durationOffset = 0
    local now = nil
    local voteBeginTime = nil

    if CLuaStarBiwuMgr.voteBeginTime ~= nil then
        now = CServerTimeMgr.Inst:GetZone8Time()
        voteBeginTime = CServerTimeMgr.ConvertTimeStampToZone8Time(CLuaStarBiwuMgr.voteBeginTime)
        durationOffset = CommonDefs.op_Subtraction_DateTime_DateTime(now, voteBeginTime).TotalSeconds
    end

    if durationOffset > voteExpire then
        self:CloseWnd()
    end
    --断线重连需要检查当前玩家选择
    self:CheckMainPlayChoice()
    self.m_NameLab.text = CLuaStarBiwuMgr.requestPlayerName
    self.m_OKBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if CLuaStarBiwuMgr.isVoteEnd then
            g_MessageMgr:ShowMessage("STAR_BIWU_STOP_FINISH")
            return
        end
        Gac2Gas.ConfirmStopStarBiwuFight()
        self:OnAgree()
    end)
    self.m_CancelBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if CLuaStarBiwuMgr.isVoteEnd then
            g_MessageMgr:ShowMessage("STAR_BIWU_STOP_FINISH")
            return
        end
        Gac2Gas.RefuseStopStarBiwuFight()
        self:OnDisagree()
    end)

    local duration = voteExpire - durationOffset
    self.m_Tweener = LuaTweenUtils.TweenFloat(1 * (duration / voteExpire), 0, duration, function (val)
        self.m_Timer.value = val
    end)
    LuaTweenUtils.OnComplete(self.m_Tweener, function ()
        self:CloseWnd()
    end)

    self:RefreshVotes()
end

function CLuaStarBiwuStopVoteWnd:CloseWnd()
    CUIManager.CloseUI(CLuaUIResources.StarBiwuStopVoteWnd)
end

function CLuaStarBiwuStopVoteWnd:RefreshVotes()
    for i = 0, 6 do
        local vote = self.m_Votes.transform:GetChild(i)
        if i > #CLuaStarBiwuMgr.m_MemberInfoTable - 1 then
            vote.gameObject:SetActive(false)
        end
        local sprite = vote.transform:Find("Foreground"):GetComponent(typeof(UISprite))
        local isVote = CLuaStarBiwuMgr.choiceList[i] ~= nil
        local isOK = CLuaStarBiwuMgr.choiceList[i] == 1

        sprite.gameObject:SetActive(isVote)
        sprite.color = isOK and NGUIText.ParseColor24("01fe6d", 0) or NGUIText.ParseColor24("fe504f", 0)
    end
end

function CLuaStarBiwuStopVoteWnd:OnStarBiWuPlayEnd()
    CLuaStarBiwuMgr.isVoteEnd = true
end

function CLuaStarBiwuStopVoteWnd:CheckMainPlayChoice()
    local mainPlayerId = CClientMainPlayer.Inst.Id;

    for _, v in pairs(CLuaStarBiwuMgr.agreeList) do
        if v == mainPlayerId then
            self:OnAgree()
        end
    end
    for _, v in pairs(CLuaStarBiwuMgr.disagreeList) do
        if v == mainPlayerId then
            self:OnDisagree()
        end
    end
end

function CLuaStarBiwuStopVoteWnd:OnAgree()
    self.m_OKBtn.gameObject:SetActive(false)
    self.m_CancelBtn.gameObject:SetActive(false)
    self.m_OKLab.gameObject:SetActive(true)
end

function CLuaStarBiwuStopVoteWnd:OnDisagree()
    self.m_OKBtn.gameObject:SetActive(false)
    self.m_CancelBtn.gameObject:SetActive(false)
    self.m_CancelLab.gameObject:SetActive(true)
end

function CLuaStarBiwuStopVoteWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncStopStarBiwuFight", self, "RefreshVotes")
    g_ScriptEvent:AddListener("StarBiWuPlayEnd", self, "OnStarBiWuPlayEnd")
end

function CLuaStarBiwuStopVoteWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncStopStarBiwuFight", self, "RefreshVotes")
    g_ScriptEvent:RemoveListener("StarBiWuPlayEnd", self, "OnStarBiWuPlayEnd")
end

function CLuaStarBiwuStopVoteWnd:OnDestroy()
    if self.m_Tweener then
        LuaTweenUtils.Kill(self.m_Tweener,false)
    end
    CLuaStarBiwuMgr.requestPlayerName = nil
    CLuaStarBiwuMgr.voteBeginTime = nil
    CLuaStarBiwuMgr.isVoteEnd = true
end
