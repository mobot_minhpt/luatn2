local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"

LuaSanxingShowWnd = class()
RegistChildComponent(LuaSanxingShowWnd,"closeBtn", GameObject)
RegistChildComponent(LuaSanxingShowWnd,"attendBtn", GameObject)
RegistChildComponent(LuaSanxingShowWnd,"cancelBtn", GameObject)
RegistChildComponent(LuaSanxingShowWnd,"tipText", UILabel)
RegistChildComponent(LuaSanxingShowWnd,"paiweiLabel", UILabel)
RegistChildComponent(LuaSanxingShowWnd,"tipBtn", GameObject)

--RegistClassMember(LuaSanxingShowWnd, "maxChooseNum")

function LuaSanxingShowWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.SanxingShowWnd)
end

function LuaSanxingShowWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaSanxingShowWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaSanxingShowWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	local onAttendClick = function(go)
    Gac2Gas.RequestSingUpSanXingBattle(1)
	end
	CommonDefs.AddOnClickListener(self.attendBtn,DelegateFactory.Action_GameObject(onAttendClick),false)
	local onTipClick = function(go)
		g_MessageMgr:ShowMessage('SANXING_ELO_POINT_STATE_INTERFACE_MSG')
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

	self.paiweiLabel.text = LocalString.GetString('排位分') .. ' ' .. LuaSanxingGamePlayMgr.matchScore

	local splits = g_LuaUtil:StrSplit(SanXingBattle_Setting.GetData().FlagItem_Info,";")
	local splits1 = g_LuaUtil:StrSplit(splits[1],",")
	local splits2 = g_LuaUtil:StrSplit(splits[2],",")

	self.tipText.text = g_MessageMgr:FormatMessage('SanxingShowText',
	SanXingBattle_Setting.GetData().EnterTimesLimit,
	SanXingBattle_Setting.GetData().AwardTimesLimit,
	splits1[7],
	splits2[7],
	SanXingBattle_Setting.GetData().Trigger_Pve_Score,
	SanXingBattle_Setting.GetData().JinSha_Win_Score
	)
	--local onCancelClick = function(go)
    --Gac2Gas.RequestCancelSignUpSanXingBattle()
	--end
	--CommonDefs.AddOnClickListener(self.cancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)

	self:InitBtn()
end

function LuaSanxingShowWnd:InitBtn()
--	if LuaSanxingGamePlayMgr.matchStatus and LuaSanxingGamePlayMgr.matchStatus == 1 then
--    self.attendBtn:SetActive(false)
--    self.cancelBtn:SetActive(true)
--  else
    self.attendBtn:SetActive(true)
    self.cancelBtn:SetActive(false)
--  end
end

return LuaSanxingShowWnd
