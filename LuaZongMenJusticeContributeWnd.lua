local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local CUIFx = import "L10.UI.CUIFx"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

LuaZongMenJusticeContributeWnd = class()

RegistChildComponent(LuaZongMenJusticeContributeWnd, "ValueLab", "ValueLab", UILabel)
RegistChildComponent(LuaZongMenJusticeContributeWnd, "GroupTex1", "GroupTex1", CUITexture)
RegistChildComponent(LuaZongMenJusticeContributeWnd, "GroupTex2", "GroupTex2", CUITexture)
RegistChildComponent(LuaZongMenJusticeContributeWnd, "SubBtn", "SubBtn", CButton)
RegistChildComponent(LuaZongMenJusticeContributeWnd, "TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaZongMenJusticeContributeWnd, "Fx", "Fx", CUIFx)

function LuaZongMenJusticeContributeWnd:Awake()
    self.ValueLab.text = "0"
end

function LuaZongMenJusticeContributeWnd:Init()
    self.GroupTex1.gameObject:SetActive(false)
    self.GroupTex2.gameObject:SetActive(false)
    CommonDefs.AddOnClickListener(self.SubBtn.gameObject, DelegateFactory.Action_GameObject(function(go)
        self:OnSubBtnClick()
    end), false)

    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function(Lab)
        g_MessageMgr:ShowMessage("Justice_Contribute_Tip")
    end)

    local sectId = ((CClientMainPlayer.Inst or {}).BasicProp or {}).SectId
    if sectId ~= nil then
        Gac2Gas.QuerySectEvilValue(sectId)
    end
end

function LuaZongMenJusticeContributeWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSectEvilValue", self, "OnSyncSectEvilValue")
    g_ScriptEvent:AddListener("ReplySubmitSectEvilYaoGuai", self, "OnReplySubmitSectEvilYaoGuai")
end

function LuaZongMenJusticeContributeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSectEvilValue", self, "OnSyncSectEvilValue")
    g_ScriptEvent:RemoveListener("ReplySubmitSectEvilYaoGuai", self, "OnReplySubmitSectEvilYaoGuai")
end

function LuaZongMenJusticeContributeWnd:OnSyncSectEvilValue(evilValue, bEvil)
    self:SwitchGroup(bEvil)
    self.ValueLab.text = evilValue
end

function LuaZongMenJusticeContributeWnd:OnReplySubmitSectEvilYaoGuai(evilValue, bEvil)
    self:SwitchGroup(bEvil)
    self.ValueLab.text = evilValue
    self.Fx:DestroyFx()
    self.Fx:LoadFx("fx/ui/prefab/ui_lingheningjie_02.prefab")
end

function LuaZongMenJusticeContributeWnd:SwitchGroup(isCult)
    self.GroupTex1.gameObject:SetActive(not isCult)
    self.GroupTex2.gameObject:SetActive(isCult)
end

function LuaZongMenJusticeContributeWnd:CheckHasSatisfyItem()
    return next(LuaZhuoYaoMgr.m_WarehouseYaoGuaiList, nil) ~= nil
end

function LuaZongMenJusticeContributeWnd:OnSubBtnClick()
    if self:CheckHasSatisfyItem() then
        CUIManager.ShowUI(CLuaUIResources.ZongMenJusticeContributeChooseWnd)
    else
        g_MessageMgr:ShowMessage("Justice_Contribute_NoSatisfyItem")
    end
end

function LuaZongMenJusticeContributeWnd:OnDestroy()
    if CUIManager.IsLoaded(CLuaUIResources.ZongMenJusticeContributeChooseWnd) then
        CUIManager.CloseUI(CLuaUIResources.ZongMenJusticeContributeChooseWnd)
    end
end

