-- Auto Generated!!
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFriendGroupMsgSettingWnd = import "L10.UI.CFriendGroupMsgSettingWnd"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumBroadcastSetting = import "L10.Game.EnumBroadcastSetting"
local EnumBroadcastSettingByGroup = import "L10.Game.EnumBroadcastSettingByGroup"
local NGUIMath = import "NGUIMath"
local QnCheckBox = import "L10.UI.QnCheckBox"
CFriendGroupMsgSettingWnd.m_UpdateValue_CS2LuaHook = function (this) 
    local setting = EnumBroadcastSetting.eAcceptNone
    if CClientMainPlayer.Inst ~= nil then
        setting = CommonDefs.ConvertIntToEnum(typeof(EnumBroadcastSetting), CClientMainPlayer.Inst.RelationshipProp.BroadcastSetting)
    end
    this.acceptAllCheckbox:SetSelected(setting == EnumBroadcastSetting.eAcceptAll, true)
    this.acceptFriendsCheckbox:SetSelected(setting == EnumBroadcastSetting.eAcceptFriend, true)
    this.acceptNoneCheckbox:SetSelected(setting == EnumBroadcastSetting.eAcceptNone, true)

    this:SetDetailSettingVisibility(this.acceptFriendsCheckbox.Selected)
end
CFriendGroupMsgSettingWnd.m_Start_CS2LuaHook = function (this) 
    this.acceptAllCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.acceptAllCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnAcceptAllCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    this.acceptFriendsCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.acceptFriendsCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnAcceptFriendsCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    this.acceptNoneCheckbox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.acceptNoneCheckbox.OnValueChanged, MakeDelegateFromCSFunction(this.OnAcceptNoneCheckboxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
end
CFriendGroupMsgSettingWnd.m_OnIMBroadcastDetailSettingChanged_CS2LuaHook = function (this) 
    if this.detailRootInited and this.detailCheckboxes.Count > 0 then
        do
            local i = 0
            while i < this.detailCheckboxes.Count do
                this.detailCheckboxes[i]:SetSelected(CIMMgr.Inst:GetBroadcastSettingByGroup(i + 1) == EnumBroadcastSettingByGroup.eAccept, true)
                i = i + 1
            end
        end
    end
end
CFriendGroupMsgSettingWnd.m_SetDetailSettingVisibility_CS2LuaHook = function (this, visible) 
    this.detailSettingRoot:SetActive(visible)
    if visible and not this.detailRootInited then
        this.detailRootInited = true
        CommonDefs.ListClear(this.detailCheckboxes)
        do
            local i = 0
            while i < CIMMgr.MAX_FRIEND_GROUP_NUM do
                local go = CUICommonDef.AddChild(this.table.gameObject, this.checkboxTemplate)
                go:SetActive(true)
                local checkbox = CommonDefs.GetComponent_GameObject_Type(go, typeof(QnCheckBox))
                checkbox.Text = CIMMgr.Inst:GetFriendGroupName(i + 1)
                checkbox:SetSelected(CIMMgr.Inst:GetBroadcastSettingByGroup(i + 1) == EnumBroadcastSettingByGroup.eAccept, true)
                local groupIndex = i + 1
                checkbox.OnValueChanged = DelegateFactory.Action_bool(function (value) 
                    CIMMgr.Inst:SetBroadcastSettingByGroup(groupIndex, value and EnumBroadcastSettingByGroup.eAccept or EnumBroadcastSettingByGroup.eRefuse)
                end)
                CommonDefs.ListAdd(this.detailCheckboxes, typeof(QnCheckBox), checkbox)
                i = i + 1
            end
        end
        this.table:Reposition()
        local b = NGUIMath.CalculateRelativeWidgetBounds(this.table.transform)
        this.bg.height = math.floor(b.size.y) + 30
    end
end
