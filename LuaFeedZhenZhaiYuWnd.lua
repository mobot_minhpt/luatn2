local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CUITexture = import "L10.UI.CUITexture"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"

LuaFeedZhenZhaiYuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFeedZhenZhaiYuWnd, "Fish", "Fish", GameObject)
RegistChildComponent(LuaFeedZhenZhaiYuWnd, "LingQiLabel", "LingQiLabel", UILabel)
RegistChildComponent(LuaFeedZhenZhaiYuWnd, "AttrLabel", "AttrLabel", UILabel)
RegistChildComponent(LuaFeedZhenZhaiYuWnd, "Food", "Food", GameObject)
RegistChildComponent(LuaFeedZhenZhaiYuWnd, "LingQiInput", "LingQiInput", QnAddSubAndInputButton)
RegistChildComponent(LuaFeedZhenZhaiYuWnd, "AddLingQiLabel", "AddLingQiLabel", UILabel)
RegistChildComponent(LuaFeedZhenZhaiYuWnd, "FeedBtn", "FeedBtn", GameObject)
RegistChildComponent(LuaFeedZhenZhaiYuWnd, "TipBtn", "TipBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaFeedZhenZhaiYuWnd,"m_FeedFoodCount")
function LuaFeedZhenZhaiYuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.FeedBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFeedBtnClick()
	end)

	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)

    --@endregion EventBind end
	self.m_FeedFoodCount = 0
end

function LuaFeedZhenZhaiYuWnd:OnEnable()  
	g_ScriptEvent:AddListener("SendItem", self, "Init")
	g_ScriptEvent:AddListener("SetItemAt", self, "Init")
end

function LuaFeedZhenZhaiYuWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "Init")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "Init")
end

function LuaFeedZhenZhaiYuWnd:Init()
	--fish
	local data = Item_Item.GetData(LuaZhenZhaiYuMgr.m_FeedFishItemId)
	if not data then return end
	local fishIcon = self.Fish.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local fishNameLabel = self.Fish.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local fishLevelLabel = self.Fish.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local border = self.Fish.transform:Find("Border"):GetComponent(typeof(UISprite))
	fishIcon:LoadMaterial(data.Icon)
	fishNameLabel.text = data.Name
	local fishData = HouseFish_AllFishes.GetData(LuaZhenZhaiYuMgr.m_FeedFishItemId)
	fishLevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"),fishData.Level)
	border.spriteName = CUICommonDef.GetItemCellBorder(data.NameColor)

	--food
	local foodIcon = self.Food.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local foodNameLabel = self.Food.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local countLabel = self.Food.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local getNode = self.Food.transform:Find("GetNode").gameObject
	local setting = HouseFish_Setting.GetData()
	local foodItemId = setting.AddDurabilityItemId
	local addScore = setting.AddDurabilityItemScore
	local foodData = Item_Item.GetData(foodItemId)
	if not foodData then
		return 
	end
	foodIcon:LoadMaterial(foodData.Icon)
	foodNameLabel.text = foodData.Name
	local bindCount, notBindCount,count
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(foodItemId)
	count = bindCount+notBindCount
	countLabel.text = count
	getNode:SetActive(count == 0)
	UIEventListener.Get(self.Food).onClick = DelegateFactory.VoidDelegate(function (go)
	    if count <= 0 then
			CItemAccessListMgr.Inst:ShowItemAccessInfo(foodItemId, true, nil, AlignType1.Right)
		end
	end)

	self.LingQiInput:SetMinMax(0, count, 1)
	self.LingQiInput.onValueChanged = DelegateFactory.Action_uint(function (value)
		self.m_FeedFoodCount = value
		self.AddLingQiLabel.text = value * addScore
	end)
	self.LingQiInput:SetValue(1, true)

	--init label
	local info = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo
	if info.wordId and info.wordId ~= 0 and Word_Word.GetData(info.wordId) then
		self.AttrLabel.text = Word_Word.GetData(info.wordId).Description
	else
		self.AttrLabel.text = SafeStringFormat3(LocalString.GetString("暂无"))
	end
	if info.duration then
		self.LingQiLabel.text = info.duration
	else
		self.LingQiLabel.text = 0
	end
end

--@region UIEvent

function LuaFeedZhenZhaiYuWnd:OnFeedBtnClick()
	self.m_FeedFoodCount = self.LingQiInput:GetValue()
	local fishmapId = LuaSeaFishingMgr.SelectdZhenZhaiFishInfo.fishmapId
	if self.m_FeedFoodCount > 0 and fishmapId then
		Gac2Gas.RequestFixZhengzhaiFishDuration(fishmapId,self.m_FeedFoodCount)--fishId,count
	end
end

function LuaFeedZhenZhaiYuWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("Feed_Zhenzhaiyu_Tip")
end

--@endregion UIEvent

