


local CUITexture = import "L10.UI.CUITexture"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CClientMonster = import "L10.Game.CClientMonster"
local CSkillAcquisitionMgr = import "L10.UI.CSkillAcquisitionMgr"

LuaLiuYi2021TangGuoChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "BtnLabel", "BtnLabel", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Texture1", "Texture1", CUITexture)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Texture2", "Texture2", CUITexture)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Texture3", "Texture3", CUITexture)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Container", "Container", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "RoleLabel", "RoleLabel", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Skill1Name", "Skill1Name", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Skill2Name", "Skill2Name", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Skill1Icon", "Skill1Icon", CUITexture)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Skill2Icon", "Skill2Icon", CUITexture)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Skill1Desc", "Skill1Desc", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoChooseWnd, "Skill2Desc", "Skill2Desc", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_ModelTextures")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_ModelTextureLoaders")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_CurrentAngels")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_IntervalAngel")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_OffsetX")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_OffsetY")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_IsEnough")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_Tick")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_TickCount")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_Index")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_SkillNames")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_SkillIcons")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_SkillDesc")
RegistClassMember(LuaLiuYi2021TangGuoChooseWnd,"m_MoveAmount")

function LuaLiuYi2021TangGuoChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)

    --@endregion EventBind end
end

function LuaLiuYi2021TangGuoChooseWnd:Init()
	self.m_ModelTextures = {self.Texture1, self.Texture2, self.Texture3}
	self.m_ModelTextureLoaders = {}
	self.m_IntervalAngel = 120
	self.m_CurrentAngels = {0, 120, 240}
	self.m_OffsetX = 140
	self.m_OffsetY = 100
	self.m_Index = 1
	self.m_MoveAmount = 1.5
	self.m_SkillNames = {self.Skill1Name, self.Skill2Name}
	self.m_SkillIcons = {self.Skill1Icon, self.Skill2Icon}
	self.m_SkillDesc = {self.Skill1Desc, self.Skill2Desc}

	self.m_TickCount = 10
	self.BtnLabel.text = SafeStringFormat3(LocalString.GetString("就选你了（%d）"), self.m_TickCount)

	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
	end

	self.m_Tick = RegisterTick(function()
		self.m_TickCount = self.m_TickCount - 1
		self.BtnLabel.text = SafeStringFormat3(LocalString.GetString("就选你了（%d）"), self.m_TickCount)
		if self.m_TickCount <= -1 then
			self:OnCommitBtnClick()
		end
	end, 1000)

	self.m_Index = math.floor(UnityEngine_Random(1,4))
	if self.m_Index == 1 then
		self.m_CurrentAngels = {0, 120, 240}
	elseif self.m_Index == 2 then
		self.m_CurrentAngels = {240, 0, 120}
	else
		self.m_Index = 3
		self.m_CurrentAngels = {120, 240, 0}
	end

	self:InitModel()
	self:InitSkillInfo()

	UIEventListener.Get(self.Container).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
		local moveDeg = math.deg(math.asin(delta.x * self.m_MoveAmount / self.m_OffsetX))
		for i=1, 3 do
			self.m_CurrentAngels[i] = (self.m_CurrentAngels[i] + moveDeg) - 360* math.floor((self.m_CurrentAngels[i] + moveDeg)/360)
		end
		self:UpdatePosition()
    end)

	UIEventListener.Get(self.Container).onDragEnd = DelegateFactory.VoidDelegate(function (go, delta)
		self:DragEnd()
    end)

	self:DragEnd()
end

function LuaLiuYi2021TangGuoChooseWnd:InitModel()
	for i=1,3 do
		self.m_ModelTextureLoaders[i] = LuaDefaultModelTextureLoader.Create(function (ro)
			ro.Scale = 0.75
			self:LoadModel(ro, i)
		end)

		self.m_ModelTextures[i].mainTexture = CUIManager.CreateModelTexture("20210601TangGuo"..i, self.m_ModelTextureLoaders[i], 180, 
			0.05, -1, 4.66, false, true, 1.6, false)
	end
end

function LuaLiuYi2021TangGuoChooseWnd:DragEnd()
	local selectIndex = 1
	for i=1,3 do
		local angle = self.m_CurrentAngels[i]
		if (angle <= self.m_IntervalAngel /2 and angle > - self.m_IntervalAngel/2) or 
				(angle <= 360 and angle>360-self.m_IntervalAngel/2) then
			selectIndex = i
			break
		end
	end

	for i=1,3 do
		local moveDeg = (i-selectIndex) * self.m_IntervalAngel
		local moveY = (math.cos(math.rad(moveDeg)) * 0.5 - 0.5) * self.m_OffsetY
		local moveTo = math.sin(math.rad(moveDeg)) * self.m_OffsetX
		local moveToScale = math.cos(math.rad(moveDeg)) *0.4 + 0.6
		LuaTweenUtils.DOLocalMoveX(self.m_ModelTextures[i].transform, moveTo, 0.5, false)
		LuaTweenUtils.DOLocalMoveY(self.m_ModelTextures[i].transform, moveY, 0.5, false)
		LuaTweenUtils.DOScale(self.m_ModelTextures[i].transform, Vector3(moveToScale, moveToScale, 1), 0.5)
		self.m_CurrentAngels[i] = moveDeg
	end

	self.m_Index = selectIndex
	self:InitSkillInfo()
end

function LuaLiuYi2021TangGuoChooseWnd:InitSkillInfo()
	self.RoleLabel.text = LiuYi2021_BabyType.GetData(self.m_Index).name
	local data = LiuYi2021_Setting.GetData().TangGuoPlayerSkillId
	local skills = {data[0], data[self.m_Index]}
	for i=1,2 do
		local skill = Skill_AllSkills.GetData(skills[i]*100 + 1)
		UIEventListener.Get(self.m_SkillIcons[i].gameObject).onClick = DelegateFactory.VoidDelegate(function()
			local list = CreateFromClass(MakeGenericClass(List, UInt32))
    		CommonDefs.ListAdd(list, typeof(UInt32), skills[i]*100+1)
    		CSkillAcquisitionMgr.ShowTempSkillsTip(list)
		end)
		if skill then
			self.m_SkillIcons[i]:LoadSkillIcon(skill.SkillIcon)
			self.m_SkillNames[i].text = skill.Name
			self.m_SkillDesc[i].text = skill.Display
		end
	end
end

function LuaLiuYi2021TangGuoChooseWnd:OnEnable( )
    g_ScriptEvent:AddListener("CandyPlayCheckInMatchingResult", self, "UpdateSignStatus")
end

function LuaLiuYi2021TangGuoChooseWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("CandyPlayCheckInMatchingResult", self, "UpdateSignStatus")
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
	for i=1,3 do
		CUIManager.DestroyModelTexture("20210601TangGuo"..i)
	end
end

function LuaLiuYi2021TangGuoChooseWnd:LoadModel(ro, index)
	local data = LiuYi2021_BabyType.GetData(index)
	CClientMonster.LoadResourceIntoRO(ro, data.transformId, false)
	ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(ro)
		ro:DoAni("qingwatiao_stand01", true, 0, 1, 0, true, 1)
	end))
end

function LuaLiuYi2021TangGuoChooseWnd:UpdatePosition()
	for i=1,3 do
		local offsetX = math.sin(math.rad(self.m_CurrentAngels[i])) * self.m_OffsetX
		local offsetY = (math.cos(math.rad(self.m_CurrentAngels[i])) * 0.5 - 0.5) * self.m_OffsetY
		local scale = math.cos(math.rad(self.m_CurrentAngels[i]))*0.4 + 0.6

		if not System.Single.IsNaN(offsetX) then
			Extensions.SetLocalPositionX(self.m_ModelTextures[i].transform, 20 + offsetX)
		end

		if not System.Single.IsNaN(offsetY) then
			Extensions.SetLocalPositionY(self.m_ModelTextures[i].transform, offsetY)
		end

		if not System.Single.IsNaN(scale) then
			self.m_ModelTextures[i].transform.localScale = Vector3(scale, scale, 1)
		end

		-- 调整遮挡
		if scale < 0.8 and scale > 0.2 then
			self.m_ModelTextures[i].texture.depth = 3
        elseif scale > 0.8 then
			self.m_ModelTextures[i].texture.depth = 4
        else
			self.m_ModelTextures[i].texture.depth = 2
		end
	end
end

--@region UIEvent

function LuaLiuYi2021TangGuoChooseWnd:OnCommitBtnClick()
	Gac2Gas.CandySelectBabyType(self.m_Index)
	CUIManager.CloseUI(CLuaUIResources.LiuYi2021TangGuoChooseWnd)
end

--@endregion UIEvent

