local UISprite = import "UISprite"

local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"

LuaCommonProgressRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonProgressRankWnd, "HeadLabel1", "HeadLabel1", UILabel)
RegistChildComponent(LuaCommonProgressRankWnd, "HeadLabel2", "HeadLabel2", UILabel)
RegistChildComponent(LuaCommonProgressRankWnd, "HeadLabel3", "HeadLabel3", UILabel)
RegistChildComponent(LuaCommonProgressRankWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaCommonProgressRankWnd, "BottomBtnLabel", "BottomBtnLabel", UILabel)
RegistChildComponent(LuaCommonProgressRankWnd, "BottomBtn", "BottomBtn", GameObject)
RegistChildComponent(LuaCommonProgressRankWnd, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaCommonProgressRankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaCommonProgressRankWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaCommonProgressRankWnd, "Foreground", "Foreground", UISprite)
RegistChildComponent(LuaCommonProgressRankWnd, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaCommonProgressRankWnd, "m_RankList")
RegistClassMember(LuaCommonProgressRankWnd, "m_Clues")

function LuaCommonProgressRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BottomBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBottomBtnClick()
	end)


    --@endregion EventBind end
end

function LuaCommonProgressRankWnd:Init()
	self.m_RankList={}

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,

        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
            self:InitItem(item,index,self.m_RankList[index+1])
        end
    )

	-- 设置显示数据
	if LuaCommonProgressRankWndMgr.m_BottomText then
		self.BottomLabel.text = LuaCommonProgressRankWndMgr.m_BottomText
	end

	if LuaCommonProgressRankWndMgr.m_HeaderText1 then
		self.HeadLabel1.text = LuaCommonProgressRankWndMgr.m_HeaderText1
	end
	if LuaCommonProgressRankWndMgr.m_HeaderText2 then
		self.HeadLabel2.text = LuaCommonProgressRankWndMgr.m_HeaderText2
	end
	if LuaCommonProgressRankWndMgr.m_HeaderText3 then
		self.HeadLabel3.text = LuaCommonProgressRankWndMgr.m_HeaderText3
	end

	if LuaCommonProgressRankWndMgr.m_ProgressBarSpritename then
		self.Foreground.spriteName = LuaCommonProgressRankWndMgr.m_ProgressBarSpritename
		self.MainPlayerInfo.transform:Find("ProgressBar/Foreground"):GetComponent(typeof(UISprite)).spriteName = LuaCommonProgressRankWndMgr.m_ProgressBarSpritename
	end

	if LuaCommonProgressRankWndMgr.m_BottomBtnText then
		self.BottomBtnLabel.text = LuaCommonProgressRankWndMgr.m_BottomBtnText
	end

	if LuaCommonProgressRankWndMgr.m_TitleText then
		self.TitleLabel.text = LuaCommonProgressRankWndMgr.m_TitleText
	end

	if LuaCommonProgressRankWndMgr.m_RankData then
		self.m_RankList = LuaCommonProgressRankWndMgr.m_RankData
		self.TableView:ReloadData(true,false)
	end

	-- 设置玩家自己数据
	self:InitItem(self.MainPlayerInfo, 0, LuaCommonProgressRankWndMgr.m_MainPlayerData)
end

function LuaCommonProgressRankWnd:InitItem(item,index,info)
	-- 获取节点
    local tf = item.transform
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = ""
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text = ""
    local rankSprite = tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName = ""
	local progressBar = tf:Find("ProgressBar"):GetComponent(typeof(UISlider))
	local progressLabel = tf:Find("ProgressBar/ProgressLabel"):GetComponent(typeof(UILabel))
	progressLabel.text = ""

	-- 设置数据
    local rank = info.rank
    if rank == 1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankSprite.spriteName = "Rank_No.3png"
	elseif rank then
        rankLabel.text = tostring(rank)
	else
		rankLabel.text = LocalString.GetString("未上榜")
    end
    nameLabel.text = info.name
	progressLabel.text = (info.count * 100) .. "%"
	progressBar.value = info.count
end

function LuaCommonProgressRankWnd:OnEnable()
end

function LuaCommonProgressRankWnd:OnDisable()
    LuaCommonProgressRankWndMgr.m_BottomText = nil
	LuaCommonProgressRankWndMgr.m_HeaderText1 = nil
	LuaCommonProgressRankWndMgr.m_HeaderText2 = nil
	LuaCommonProgressRankWndMgr.m_HeaderText3 = nil
	LuaCommonProgressRankWndMgr.m_ProgressBarSpritename = nil
	LuaCommonProgressRankWndMgr.m_BottomBtnText = nil
	LuaCommonProgressRankWndMgr.m_OnBottomBtnClick = nil
end


--@region UIEvent

function LuaCommonProgressRankWnd:OnBottomBtnClick()
	if LuaCommonProgressRankWndMgr.m_OnBottomBtnClick then
		LuaCommonProgressRankWndMgr.m_OnBottomBtnClick()
	end
end

--@endregion UIEvent
