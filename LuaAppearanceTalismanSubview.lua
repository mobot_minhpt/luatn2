local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"

LuaAppearanceTalismanSubview = class()

RegistChildComponent(LuaAppearanceTalismanSubview,"m_TalismanItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceTalismanSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceTalismanSubview,"m_SwitchButton", "CommonHeaderSwitch", CButton)
RegistChildComponent(LuaAppearanceTalismanSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceTalismanSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceTalismanSubview, "m_AppearanceTalismanTabBar", "AppearanceTalismanTabBar", CCommonLuaScript)

RegistClassMember(LuaAppearanceTalismanSubview, "m_AllTalismanAppearances")
RegistClassMember(LuaAppearanceTalismanSubview, "m_TalismanSpecialAppearInfo")
RegistClassMember(LuaAppearanceTalismanSubview, "m_SelectedDataId")

function LuaAppearanceTalismanSubview:Awake()
end

function LuaAppearanceTalismanSubview:Init()
    self.m_SwitchButton.gameObject:SetActive(true)
    --组件共用，每次需要重新关联响应方法
    self.m_SwitchButton.Text = LocalString.GetString("法宝开关")
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSwitchButtonClick() end)

    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceTalismanSubview:LoadData()
    self.m_AllTalismanAppearances = LuaAppearancePreviewMgr:GetAllTalismanAppearInfo(false)
    self.m_TalismanSpecialAppearInfo = {}
    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, # self.m_AllTalismanAppearances do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_TalismanItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllTalismanAppearances[i])
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceTalismanSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:IsShowTalismanAppear() and LuaAppearancePreviewMgr:GetCurrentInUseTalismanAppear() or 0
end

function LuaAppearanceTalismanSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllTalismanAppearances[i+1]
        if appearanceData.id == self.m_SelectedDataId or self:IsSameCategory(appearanceData.id, self.m_SelectedDataId) then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceTalismanSubview:ExistInUseTalismanAppear(id, fxId)
    if LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(id) then
        return true
    end
    local sameCategoryTbl = LuaAppearancePreviewMgr:GetTalismanSpecialAppearInfo(id, fxId)
    for __,data in pairs(sameCategoryTbl) do
        if LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(data.id, data.fxId) then
            return true
        end
    end
    return false
end

function LuaAppearanceTalismanSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disabledGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    iconTexture:LoadMaterial(appearanceData.icon)
    if CClientMainPlayer.Inst then
        lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasTalismanAppear(appearanceData.id))
        cornerGo:SetActive(self:ExistInUseTalismanAppear(appearanceData.id, appearanceData.fxId))
      else
        iconTexture:Clear()
        lockedGo:SetActive(false)
        cornerGo:SetActive(false)
    end
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id)
    disabledGo:SetActive(lockedGo.activeSelf)
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceTalismanSubview:OnItemClick(go)
    self.m_TalismanSpecialAppearInfo = {}
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_TalismanSpecialAppearInfo = LuaAppearancePreviewMgr:GetTalismanSpecialAppearInfo(self.m_AllTalismanAppearances[i+1].id, self.m_AllTalismanAppearances[i+1].fxId)
            if #self.m_TalismanSpecialAppearInfo==0 then --不是特殊外观的情况，直接预览
                self.m_SelectedDataId = self.m_AllTalismanAppearances[i+1].id
            end
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end

    if #self.m_TalismanSpecialAppearInfo<=0 then
        self.m_AppearanceTalismanTabBar.gameObject:SetActive(false)
        self:UpdateItemDisplay()
        g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerTalismanAppearance", self.m_SelectedDataId)
        if not LuaAppearancePreviewMgr:IsShowTalismanAppear() and self.m_SelectedDataId>0 then
            g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Talisman_Appearance_Tip")
        end
    else
        self.m_AppearanceTalismanTabBar.gameObject:SetActive(true)
        self.m_AppearanceTalismanTabBar:Init(self.m_TalismanSpecialAppearInfo, function(go, index) self:OnSpecialAppearTabChange(go, index) end)        
        --特殊外观里面的第一个或者已装备的那个
        local index = 0
        for i=1,#self.m_TalismanSpecialAppearInfo do
            if LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(self.m_TalismanSpecialAppearInfo[i].id) then
                index=i-1
                break
            end
        end
        self.m_AppearanceTalismanTabBar:ChangeTab(index, false)
    end
end

function LuaAppearanceTalismanSubview:OnSpecialAppearTabChange(go, index)
    local appear = self.m_TalismanSpecialAppearInfo[index+1] or nil
    self.m_SelectedDataId = appear.id
    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerTalismanAppearance", appear.id)
    if not LuaAppearancePreviewMgr:IsShowTalismanAppear() and self.m_SelectedDataId>0 then
        g_MessageMgr:ShowMessage("Appearance_Preview_Hide_Talisman_Appearance_Tip")
    end
    self:UpdateItemDisplay()
end

function LuaAppearanceTalismanSubview:IsSameCategory(talismanId, selectedId)
    local selectedGrade = LuaAppearancePreviewMgr:GetTalismanGrade(selectedId)
    --特殊法宝外观，利用grade*10+count判断同一类
    if selectedGrade>4 then
        return talismanId%100==selectedId%100
    --1-4阶法宝外观，利用grade判断同一类
    else
        return LuaAppearancePreviewMgr:GetTalismanGrade(talismanId)==selectedGrade
    end
end

function LuaAppearanceTalismanSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    self.m_SwitchButton.Selected = LuaAppearancePreviewMgr:IsShowTalismanAppear()
    local selectedId = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if selectedId==0 then
        self.m_ItemDisplay:Clear()
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllTalismanAppearances do
        if self:IsSameCategory(self.m_AllTalismanAppearances[i].id, selectedId) then --只要找到同一类别即可
          appearanceData = self.m_AllTalismanAppearances[i]
            break
        end
    end

    --1-4阶法宝名称比较特殊，这里要特别处理一下
    local name = appearanceData.name
    for i=1,#self.m_TalismanSpecialAppearInfo do
        if selectedId==self.m_TalismanSpecialAppearInfo[i].id then
            name=self.m_TalismanSpecialAppearInfo[i].name
            break
        end
    end  
    
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local buttonTbl = {}
    if CClientMainPlayer.Inst then
        local exist = LuaAppearancePreviewMgr:MainPlayerHasTalismanAppear(selectedId) --不能用appearanceData.id 否则切到特殊法宝显示不正常
        local inUse = LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(selectedId)
        disabled = not exist
        locked = not exist
        if not inUse then
            table.insert(buttonTbl, {text=exist and LocalString.GetString("更换") or LocalString.GetString("获取"), isYellow=false, action=function(go) self:OnApplyButtonClick(go) end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
        end
    end
    self.m_ItemDisplay:Init(icon, name, appearanceData.condition, "", false, disabled, locked, buttonTbl)
end

function LuaAppearanceTalismanSubview:OnApplyButtonClick(go)
    if self.m_SelectedDataId and self.m_SelectedDataId>0 then
        
        if LuaAppearancePreviewMgr:MainPlayerHasTalismanAppear(self.m_SelectedDataId) then
            --设置
            LuaAppearancePreviewMgr:SelectTalismanAppearance(self.m_SelectedDataId)
        else
            --获取
            LuaAppearancePreviewMgr:ShowGetTalismanInfo(go, self.m_SelectedDataId)
        end
    end
end

function LuaAppearanceTalismanSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:SelectTalismanAppearance(0)
end

function LuaAppearanceTalismanSubview:OnSwitchButtonClick()
    self.m_SwitchButton.Selected = not self.m_SwitchButton.Selected
    local bShow = LuaAppearancePreviewMgr:IsShowTalismanAppear()
    LuaAppearancePreviewMgr:ChangeTalismanAppearVisibility(not bShow)
end

function LuaAppearanceTalismanSubview:OnEnable()
    g_ScriptEvent:AddListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
    g_ScriptEvent:AddListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:AddListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
end

function LuaAppearanceTalismanSubview:OnDisable()
    g_ScriptEvent:RemoveListener("TalismanFxUpdate", self, "OnTalismanFxUpdate")
    g_ScriptEvent:RemoveListener("TalismanFxListUpdate", self, "OnTalismanFxListUpdate")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp",self,"SyncMainPlayerAppearancePropUpdate")
    g_ScriptEvent:RemoveListener("AppearancePropertySettingInfoReturn",self,"OnAppearancePropertySettingInfoReturn") --开关切换
end


function LuaAppearanceTalismanSubview:OnTalismanFxUpdate(args)
    local obj = args[0]
    if obj and TypeIs(obj, typeof(CClientMainPlayer)) then
    self:SetDefaultSelection()
      self:LoadData()
    end
end

function LuaAppearanceTalismanSubview:OnTalismanFxListUpdate()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceTalismanSubview:SyncMainPlayerAppearancePropUpdate()
    self:InitSelection()
end

function LuaAppearanceTalismanSubview:OnAppearancePropertySettingInfoReturn(args)
    self:SetDefaultSelection()
    self:InitSelection()
end