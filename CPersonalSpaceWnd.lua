-- Auto Generated!!
local Boolean = import "System.Boolean"
local BoxCollider = import "UnityEngine.BoxCollider"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpace_AdvRet = import "L10.Game.CPersonalSpace_AdvRet"
local CPersonalSpace_CircleGenerateType = import "L10.Game.CPersonalSpace_CircleGenerateType"
local CPersonalSpace_EnumEventType = import "L10.Game.CPersonalSpace_EnumEventType"
local CPersonalSpace_ExpressionBase = import "L10.Game.CPersonalSpace_ExpressionBase"
local CPersonalSpace_ExpressionExtra = import "L10.Game.CPersonalSpace_ExpressionExtra"
local CPersonalSpace_GetMomentItem = import "L10.Game.CPersonalSpace_GetMomentItem"
local CPersonalSpace_MomentCacheData = import "L10.Game.CPersonalSpace_MomentCacheData"
local CPersonalSpace_Privacy = import "L10.Game.CPersonalSpace_Privacy"
local CPersonalSpace_UserProfile = import "L10.Game.CPersonalSpace_UserProfile"
local CPersonalSpace_Zan = import "L10.Game.CPersonalSpace_Zan"
local CPersonalSpaceDetailMoment = import "L10.UI.CPersonalSpaceDetailMoment"
local CPersonalSpaceHotMomentsView = import "L10.UI.CPersonalSpaceHotMomentsView"
local CPersonalSpaceLeaveMessageView = import "L10.UI.CPersonalSpaceLeaveMessageView"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CPersonalSpaceMomentHisView = import "L10.UI.CPersonalSpaceMomentHisView"
local CPersonalSpacePlaySignatureVoicePanel = import "L10.UI.CPersonalSpacePlaySignatureVoicePanel"
local CPersonalSpaceShituDetailView = import "L10.UI.CPersonalSpaceShituDetailView"
local CPersonalSpaceUsedNameDetailView = import "L10.UI.CPersonalSpaceUsedNameDetailView"
local CPersonalSpaceWeddingDetailView = import "L10.UI.CPersonalSpaceWeddingDetailView"
local CPersonalSpaceWnd = import "L10.UI.CPersonalSpaceWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTitleMgr = import "L10.Game.CTitleMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local DelegateFactory = import "DelegateFactory"
local ENUM_AVATAR_RESULT = import "ENUM_AVATAR_RESULT"
local EnumClass = import "L10.Game.EnumClass"
local EnumEventType = import "EnumEventType"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GameplayItem_Setting = import "L10.Game.GameplayItem_Setting"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Int32 = import "System.Int32"
local L10 = import "L10"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NativeHandle = import "NativeHandle"
local NGUIText = import "NGUIText"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local Object1 = import "UnityEngine.Object"
local Profession = import "L10.Game.Profession"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local Quaternion = import "UnityEngine.Quaternion"
local Screen = import "UnityEngine.Screen"
local String = import "System.String"
local Texture2D = import "UnityEngine.Texture2D"
local Title_Title = import "L10.Game.Title_Title"
local UICamera = import "UICamera"
local UIEventListener = import "UIEventListener"
local UIInput = import "UIInput"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local UIPanel = import "UIPanel"
local UIScrollView = import "UIScrollView"
local UISprite = import "UISprite"
local UITable = import "UITable"
local UITexture = import "UITexture"
local UIWidget = import "UIWidget"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local VoiceManager = import "VoiceManager"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CSpeechCtrlMgr = import "L10.Game.CSpeechCtrlMgr"
local CSpeechCtrlType = import "L10.Game.CSpeechCtrlType"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UInt64 = import "System.UInt64"
local CGoogleTranslateLabel = import "L10.UI.CGoogleTranslateLabel"

PersonalSpaceLua = {}
PersonalSpaceLua.m_Wnd = nil
function PersonalSpaceLua:OnChangeBgBtnClicked(node)
    if not node.activeSelf then
        CLuaPersonalSpaceMgr:QueryPersonalSpaceBackGroundList()
    else
        CLuaPersonalSpaceMgr:CancelUsePersonalSpaceBackGround()
    end
end

function PersonalSpaceLua:OnLoadPersonalSpaceNewBgData(currentBgId)
    if not self.m_Wnd then return end
    if not CLuaPersonalSpaceMgr.m_IsOpenNewBg then return end
    local option6Sign = self.m_Wnd.settingPanel.transform:Find("node/Grid/Option6/click/node").gameObject
    option6Sign:SetActive(currentBgId ~= 0)
    local backgrounddata = PersonalSpace_Background.GetData(currentBgId)
    self.m_Wnd.personalInfoNode.transform:Find("NewBg"):GetComponent(typeof(CUITexture)):LoadMaterial(backgrounddata and backgrounddata.Background or "")
end

CPersonalSpaceWnd.m_Awake_CS2LuaHook = function (this)
    if CPersonalSpaceWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CPersonalSpaceWnd.Instance = this

    this.selfCircleButton:SetActive(true)
    this.friendCircleButton:SetActive(false)
end
CPersonalSpaceWnd.m_close_CS2LuaHook = function (this)
    CPersonalSpaceWnd.Instance = nil
    this:Close()
end
CPersonalSpaceWnd.m_OnDestroy_CS2LuaHook = function (this)
    --Instance = null;
    --CPersonalSpaceMgr.Inst.openSpacePersonId = 0;
    CPersonalSpaceMgr.Inst:ClearCacheData()
    CLuaPlayerReportMgr:Reset()
    if LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick then
      UnRegisterTick(LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick)
      LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick = nil
    end
end


function PersonalSpaceLua:ShowWishDetailPanel(data)
  if CPersonalSpaceWnd.Instance then
    ----
--    this:ResetSubPanels()
--    this.personalSpaceDetailMoment.gameObject:SetActive(false)
--    this.momentHisSubPanels:SetActive(false)
--    this.flowerDetailPanel:SetActive(false)
--    this.renqiDetailPanel:SetActive(false)
--    this.attentionDetialPanel:SetActive(false)
--    this.transform:Find('Anchor/Nodes/WishPanel').gameObject:SetActive(false)
--  this.normalNode:SetActive(true)
--
--  this.friendCircleNode:SetActive(true)
--  this.leaveMessagePanelNode:SetActive(false)
--  this.hotMomentsPanel:SetActive(false)
--  this:InitFriendCirlePanel()

 --   local formerTable = {CPersonalSpaceWnd.Instance.}
    local wishDetailPanel = CPersonalSpaceWnd.Instance.transform:Find('Anchor/Nodes/FriendsCircleSubPanels/DetailWish').gameObject
    wishDetailPanel:SetActive(true)
    local luaScript = CommonDefs.GetComponent_GameObject_Type(wishDetailPanel, typeof(CCommonLuaScript))
    luaScript:Init({data})
  end
end

function PersonalSpaceLua:ShowWishPanel(this)
  this.transform:Find('Anchor/Nodes/WishPanel').gameObject:SetActive(true)
	local luaScript = CommonDefs.GetComponent_GameObject_Type(this.transform:Find('Anchor/Nodes/WishPanel').gameObject, typeof(CCommonLuaScript))
  luaScript:Init({})
end

function PersonalSpaceLua:GenNextPageFunc(this)
  if not PersonalSpaceLua.NextPageFunc then
    PersonalSpaceLua.NextPageFunc = DelegateFactory.Action(function ()
      local qnBtn = nil
      if CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.FriendCircle then
        qnBtn = this.selfOpArea.transform:Find("QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.SelfCircle then
        qnBtn = this.selfOpArea.transform:Find("QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.OtherSelfCircle then
        qnBtn = this.otherOpArea.transform:Find("QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.HotMoments then
        qnBtn = this.hotMomentsPanel.transform:Find("HotPanel/QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.AllHotMoments then
        qnBtn = this.hotMomentsPanel.transform:Find("HotPanel/QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      end
      if qnBtn then
        if CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.HotMoments then
          local hotMomentScript = this.hotMomentsPanel.transform:GetComponent(typeof(CPersonalSpaceHotMomentsView))
          if hotMomentScript then
            local nextPageNum = qnBtn:GetValue()+1
            local maxPageNum = qnBtn:GetMaxValue()
            if nextPageNum > maxPageNum then
              return
            end
            qnBtn:SetValue(nextPageNum)
            hotMomentScript:UpdateHotList(nextPageNum)
          end
        elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.AllHotMoments then
          local hotMomentScript = this.hotMomentsPanel.transform:GetComponent(typeof(CPersonalSpaceHotMomentsView))
          if hotMomentScript then
            local nextPageNum = qnBtn:GetValue()+1
            local maxPageNum = qnBtn:GetMaxValue()
            if nextPageNum > maxPageNum then
              return
            end
            qnBtn:SetValue(nextPageNum)
            hotMomentScript:UpdateAllHotList(nextPageNum)
          end
        else
          local nextPageNum = qnBtn:GetValue()+1
          local maxPageNum = qnBtn:GetMaxValue()
          if nextPageNum > maxPageNum then
            return
          end
          qnBtn:SetValue(nextPageNum)
          this:UpdateListInfoByRestNum(nextPageNum,10,true)
        end
      end
    end)
  end
end

function PersonalSpaceLua:GenFormerPageFunc(this)
  if not PersonalSpaceLua.FormerPageFunc then
    PersonalSpaceLua.FormerPageFunc = DelegateFactory.Action(function ()
      local qnBtn = nil
      if CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.FriendCircle then
        qnBtn = this.selfOpArea.transform:Find("QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.SelfCircle then
        qnBtn = this.selfOpArea.transform:Find("QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.OtherSelfCircle then
        qnBtn = this.otherOpArea.transform:Find("QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.HotMoments then
        qnBtn = this.hotMomentsPanel.transform:Find("HotPanel/QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.AllHotMoments then
        qnBtn = this.hotMomentsPanel.transform:Find("HotPanel/QnNumButton"):GetComponent(typeof(QnAddSubAndInputButton))
      end
      if qnBtn then
        if CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.HotMoments then
          local hotMomentScript = this.hotMomentsPanel.transform:GetComponent(typeof(CPersonalSpaceHotMomentsView))
          if hotMomentScript then
            local nextPageNum = qnBtn:GetValue()-1
            local minPageNum = qnBtn:GetMinValue()
            if nextPageNum < minPageNum then
              nextPageNum = minPageNum
            end
            qnBtn:SetValue(nextPageNum)
            hotMomentScript:UpdateHotList(nextPageNum)
          end
        elseif CPersonalSpaceMgr.Inst.NowScrollType == CPersonalSpace_CircleGenerateType.AllHotMoments then
          local hotMomentScript = this.hotMomentsPanel.transform:GetComponent(typeof(CPersonalSpaceHotMomentsView))
          if hotMomentScript then
            local nextPageNum = qnBtn:GetValue()-1
            local minPageNum = qnBtn:GetMinValue()
            if nextPageNum < minPageNum then
              nextPageNum = minPageNum
            end
            qnBtn:SetValue(nextPageNum)
            hotMomentScript:UpdateAllHotList(nextPageNum)
          end
        else
          local nextPageNum = qnBtn:GetValue()-1
          local minPageNum = qnBtn:GetMinValue()
          if nextPageNum < minPageNum then
            nextPageNum = minPageNum
          end
          qnBtn:SetValue(nextPageNum)
          this:UpdateListInfoByRestNum(nextPageNum,-10,true)
        end
      end
    end)
  end
end

CPersonalSpaceWnd.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListener(EnumEventType.UpdateMainPlayerExpression, MakeDelegateFromCSFunction(this.UpdateExpressionState, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.UpdateMainPlayerExpressionDisplayInfo, MakeDelegateFromCSFunction(this.OnUpdateMainPlayerExpressionDisplayInfo, MakeGenericClass(Action2, CPersonalSpace_ExpressionBase, CPersonalSpace_ExpressionExtra), this))
    EventManager.AddListenerInternal(EnumEventType.PersonalSpaceSetUsedNameHide, MakeDelegateFromCSFunction(this.SetUsedNameSign, MakeGenericClass(Action1, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.PersonalSpaceAddNewMoment, MakeDelegateFromCSFunction(this.AddMomentBack, MakeGenericClass(Action2, String, CPersonalSpace_AdvRet), this))

    PersonalSpaceLua:GenNextPageFunc(this)
    PersonalSpaceLua:GenFormerPageFunc(this)
    EventManager.AddListener(EnumEventType.ScrollViewReachEnd, PersonalSpaceLua.NextPageFunc)
    EventManager.AddListener(EnumEventType.ScrollViewReachTop, PersonalSpaceLua.FormerPageFunc)
    g_ScriptEvent:AddListener("OpenWishDetailPanel", PersonalSpaceLua, "ShowWishDetailPanel")
    g_ScriptEvent:AddListener("OnLoadPersonalSpaceNewBgData", PersonalSpaceLua, "OnLoadPersonalSpaceNewBgData")
    g_ScriptEvent:AddListener("OnPreviewPersonalSpaceNewBgData", PersonalSpaceLua, "OnLoadPersonalSpaceNewBgData")
    PersonalSpaceLua.m_Wnd = this
end
CPersonalSpaceWnd.m_OnDisable_CS2LuaHook = function (this)
    if this.personalInfoNode.transform:Find("Signature").gameObject.activeSelf then
        local nowSignature = CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Signature/Label/bg"), typeof(UIInput)).value
        if nowSignature ~= this.SaveSignatureString then
            CPersonalSpaceMgr.Inst:ChangeSignature(nowSignature, nil, nil)
        end
    end
    VoiceManager.onPicMsg = nil

    EventManager.RemoveListenerInternal(EnumEventType.UpdateMainPlayerExpressionDisplayInfo, MakeDelegateFromCSFunction(this.OnUpdateMainPlayerExpressionDisplayInfo, MakeGenericClass(Action2, CPersonalSpace_ExpressionBase, CPersonalSpace_ExpressionExtra), this))
    EventManager.RemoveListener(EnumEventType.UpdateMainPlayerExpression, MakeDelegateFromCSFunction(this.UpdateExpressionState, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.PersonalSpaceSetUsedNameHide, MakeDelegateFromCSFunction(this.SetUsedNameSign, MakeGenericClass(Action1, Boolean), this))
    EventManager.RemoveListenerInternal(EnumEventType.PersonalSpaceAddNewMoment, MakeDelegateFromCSFunction(this.AddMomentBack, MakeGenericClass(Action2, String, CPersonalSpace_AdvRet), this))
    if PersonalSpaceLua.NextPageFunc then
      EventManager.RemoveListener(EnumEventType.ScrollViewReachEnd, PersonalSpaceLua.NextPageFunc)
      PersonalSpaceLua.NextPageFunc = nil
    end
    if PersonalSpaceLua.FormerPageFunc then
      EventManager.RemoveListener(EnumEventType.ScrollViewReachTop, PersonalSpaceLua.FormerPageFunc)
      PersonalSpaceLua.FormerPageFunc = nil
    end

    g_ScriptEvent:RemoveListener("OpenWishDetailPanel", PersonalSpaceLua, "ShowWishDetailPanel")
    g_ScriptEvent:RemoveListener("OnLoadPersonalSpaceNewBgData", PersonalSpaceLua, "OnLoadPersonalSpaceNewBgData")
    g_ScriptEvent:RemoveListener("OnPreviewPersonalSpaceNewBgData", PersonalSpaceLua, "OnLoadPersonalSpaceNewBgData")
end

CPersonalSpaceWnd.m_UpdateTopShowRes_CS2LuaHook = function (this, data)
    local pid = data.targetid
    if pid > 0 and pid == this.PersonalID then
        CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Popular/text"), typeof(UILabel)).text = tostring(data.renqi)
        CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Gift/text"), typeof(UILabel)).text = tostring(data.gift)
        CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Flower/text"), typeof(UILabel)).text = tostring(data.flowerrenqi)
        CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Popular/text"), typeof(UILabel)).text = tostring(data.renqi)
        CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Gift/text"), typeof(UILabel)).text = tostring(data.gift)
        CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Flower/text"), typeof(UILabel)).text = tostring(data.flowerrenqi)
        CExpressionMgr.Inst.Renqi = (data.renqi)
    end
end
CPersonalSpaceWnd.m_CloseWnd_CS2LuaHook = function (this, go)

    this:close()

    local default = CPersonalSpaceMgr.Inst
    default.openCount = default.openCount - 1
    if CPersonalSpaceMgr.Inst.openCount == 1 and CPersonalSpaceMgr.Inst.firstSpacePersonId ~= 0 then
        if not System.String.IsNullOrEmpty(CPersonalSpaceMgr.Inst.Token) then
            CPersonalSpaceMgr.Inst.openSpacePersonId = CPersonalSpaceMgr.Inst.firstSpacePersonId
            L10.UI.CUIManager.ShowUI(L10.UI.CUIResources.PersonalSpaceWnd)
        end
    elseif CPersonalSpaceMgr.Inst.openCount <= 0 then
        CPersonalSpaceMgr.Inst.openCount = 0
        CPersonalSpaceMgr.Inst.firstSpacePersonId = 0
    else
        CPersonalSpaceMgr.Inst.openCount = 0
        CPersonalSpaceMgr.Inst.firstSpacePersonId = 0
    end
end
CPersonalSpaceWnd.m_Init_CS2LuaHook = function (this)

    if not CPersonalSpaceMgr.OpenPersonalSpaceEntrance or CClientMainPlayer.Inst == nil then
        this:Close()
        return
    end

    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.CloseWnd, VoidDelegate, this)

    CPersonalSpaceWnd.Instance = this
    this.PersonalID = 0
    this.PersonalName = nil
    this.PersonalProfile = CreateFromClass(CPersonalSpace_UserProfile)

    CPersonalSpaceMgr.Inst:ClearCacheData()

    this.SelfRoleInfo = CPersonalSpaceMgr.Inst:GetRoleInfo()

    if CPersonalSpaceMgr.Inst.openCount > 2 then
        CPersonalSpaceMgr.Inst.openCount = 2
    end
    if CPersonalSpaceMgr.Inst.openCount == 1 then
        CPersonalSpaceMgr.Inst.firstSpacePersonId = CPersonalSpaceMgr.Inst.openSpacePersonId
    end

    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)

    if CPersonalSpaceMgr.Inst.openSpacePersonId == this.SelfRoleInfo.id or CPersonalSpaceMgr.Inst.openSpacePersonId == 0 then
        this:InitPersonalInfo()
    else
        this:InitOtherPersonalInfo(CPersonalSpaceMgr.Inst.openSpacePersonId)
    end

    this.statusTemplateBar:SetActive(false)
    this.yearDivideNode:SetActive(false)
    this.personalSpaceDetailMoment.gameObject:SetActive(false)

    UIEventListener.Get(this.selfPortraitBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this.selfPortraitNode:SetActive(false)
        this.systemPortraitNode:SetActive(true)
    end)

    UIEventListener.Get(this.systemPortraitBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this.selfPortraitNode:SetActive(true)
        this.systemPortraitNode:SetActive(false)
    end)

    UIEventListener.Get(this.sendStatusBtn).onClick = DelegateFactory.VoidDelegate(function (p)
      if LuaPersonalSpaceMgrReal.EnableLongText then
        LuaPersonalSpaceMgrReal.SendMomentType = 2
        CPersonalSpaceMgr.Inst:OpenSendMomentWnd()
      else
        LuaPersonalSpaceMgrReal.SendMomentType = 1
        CPersonalSpaceMgr.Inst:OpenSendMomentWnd()
      end
    end)

    this.setLocationPanel:SetActive(false)
    UIEventListener.Get(this.setLocationBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:InitSetLocationPanel()
    end)

    UIEventListener.Get(this.selfCircleButton).onClick = DelegateFactory.VoidDelegate(function (p)
        this.IsDeleteSelfMomentMode = false
        this:InitSelfCircle()
    end)

    UIEventListener.Get(this.friendCircleButton).onClick = DelegateFactory.VoidDelegate(function (p)
        this:InitFriendCircle()
    end)

    UIEventListener.Get(this.settingBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:InitSettingPanel()
    end)

    UIEventListener.Get(this.weddingButton).onClick = DelegateFactory.VoidDelegate(function (p)
        this:InitWeddingDetailView()
    end)

    UIEventListener.Get(this.usedNameBtn).onClick = nil

    UIEventListener.Get(this.expressionBtn).onClick = MakeDelegateFromCSFunction(this.OnClickExpressionBtn, VoidDelegate, this)

    if CSwitchMgr.EnableExpression then
        if this.PersonalID == this.SelfRoleInfo.id then
            this.expressionBtn:SetActive(true)
        else
            this.expressionBtn:SetActive(false)
        end
    else
        this.expressionBtn:SetActive(false)
    end
end
CPersonalSpaceWnd.m_OnClickExpressionBtn_CS2LuaHook = function (this, go)
    CUIManager.ShowUI(CUIResources.ExpressionWnd)
end
CPersonalSpaceWnd.m_InitShituDetailView_CS2LuaHook = function (this)
    if this.PersonalProfile.roleid > 0 then
        if this.PersonalProfile.shifu.Length == 0 and this.PersonalProfile.tudi.Length == 0 then
            g_MessageMgr:ShowMessage("DONNOT_HAVE_SHITU")
        else
            this.shituDetailPanel:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(this.shituDetailPanel, typeof(CPersonalSpaceShituDetailView)):Init(this.PersonalProfile)
        end
    end
end
CPersonalSpaceWnd.m_InitWeddingDetailView_CS2LuaHook = function (this)
    if this.PersonalProfile.roleid > 0 then
        if this.PersonalProfile.wedding.partner.id == 0 then
            g_MessageMgr:ShowMessage("DONNOT_HAVE_FUQI")
        else
            this.weddingDetailPanel:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(this.weddingDetailPanel, typeof(CPersonalSpaceWeddingDetailView)):Init(this.PersonalProfile)
        end
    end
end
CPersonalSpaceWnd.m_GetPhotoTokenBack_CS2LuaHook = function (this, data, pic, backFunction)
    if not this.systemPortrait then
        return
    end

    if data.code == 0 then
        local token = data.data.token
        local bucketname = data.data.bucketname
        local objectname = data.data.objectname
        local uploadData = CommonDefs.EncodeToJPG((TypeAs(pic, typeof(Texture2D))))

        Main.Inst:StartCoroutine(HTTPHelper.NOSUploadFileData(uploadData, bucketname, token, objectname, DelegateFactory.Action_string_string_string(function (_code, b, url)
            if not this.systemPortrait then
                return
            end
            GenericDelegateInvoke(backFunction, Table2ArrayWithCount({url}, 1, MakeArrayClass(Object)))
        end)))
    end
end
CPersonalSpaceWnd.m_UploadPic_CS2LuaHook = function (this, uploadType, pic, backFunction)
    if pic == nil then
        return
    end

    --CPersonalSpaceMgr.Inst:GetFileUploadToken(uploadType, DelegateFactory.Action_CPersonalSpace_GetFileTokenRet(function (data)
    --    this:GetPhotoTokenBack(data, pic, backFunction)
    --end), nil)

    local uploadData = CommonDefs.EncodeToJPG((TypeAs(pic, typeof(Texture2D))))
    CPersonalSpaceMgr.Inst:UploadPic(uploadType, uploadData, backFunction)
end
CPersonalSpaceWnd.m_InitEventDetailPanel_CS2LuaHook = function (this, fatherNode, showNode, eventType, template, data)
    fatherNode:SetActive(false)
    showNode:SetActive(true)

    local scrollview = CommonDefs.GetComponent_Component_Type(showNode.transform:Find("ScrollView"), typeof(UIScrollView))
    local table = CommonDefs.GetComponent_Component_Type(showNode.transform:Find("ScrollView/Table"), typeof(UITable))
    Extensions.RemoveAllChildren(table.transform)
    UIEventListener.Get(showNode.transform:Find("BackButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        fatherNode:SetActive(true)
        showNode:SetActive(false)
    end)

    local emptyNode = showNode.transform:Find("EmptyNode").gameObject
    if data.Length <= 0 then
        emptyNode:SetActive(true)
    else
        emptyNode:SetActive(false)
    end

    template:SetActive(false)
    do
        local i = 0
        while i < data.Length do
            local node = NGUITools.AddChild(table.gameObject, template)
            node:SetActive(true)
            local info = data[i]
            if eventType == CPersonalSpace_EnumEventType.eFlower then
                local cls = tonumber(info.roleinfo.clazz)
                local gender = tonumber(info.roleinfo.gender)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, -1), false)


                --node.transform.FindChild("lv").GetComponent<UILabel>().text = info.roleinfo.grade;
                local levelLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel))
                local default
                if info.roleinfo.xianfanstatus > 0 then
                    default = L10.Game.Constants.ColorOfFeiSheng
                else
                    default = NGUIText.EncodeColor24(levelLabel.color)
                end
                levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, info.roleinfo.grade)

                CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.roleinfo.rolename

                CPersonalSpaceMgr.Inst:addPlayerLinkBtn(node.transform:Find("icon/button").gameObject, info.roleinfo.roleid, EnumReportId_lua.eDefault, "", nil, 0, 0)

                if not System.String.IsNullOrEmpty(info.parameter) then
                    local stringArray = CommonDefs.StringSplit_ArrayChar(info.parameter, ",")
                    if stringArray.Length >= 4 then
                        local stringArrayItem1 = CommonDefs.StringSplit_ArrayChar(stringArray[0], "$")
                        local stringArrayItem2 = CommonDefs.StringSplit_ArrayChar(stringArray[1], "$")
                        local stringArrayItem3 = CommonDefs.StringSplit_ArrayChar(stringArray[2], "$")
                        local stringArrayItem4 = CommonDefs.StringSplit_ArrayChar(stringArray[3], "$")


                        CommonDefs.GetComponent_Component_Type(node.transform:Find("SendItem3/text"), typeof(UILabel)).text = stringArrayItem1[2]
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("SendItem2/text"), typeof(UILabel)).text = stringArrayItem2[2]
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("SendItem1/text"), typeof(UILabel)).text = stringArrayItem3[2]
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("SendItem4/text"), typeof(UILabel)).text = stringArrayItem4[2]

                        local totalNum = System.Int32.Parse(stringArrayItem1[3]) + System.Int32.Parse(stringArrayItem2[3]) + System.Int32.Parse(stringArrayItem3[3]) + System.Int32.Parse(stringArrayItem4[3])
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("TotalNum/text"), typeof(UILabel)).text = "+" .. totalNum
                    else
                        CommonDefs.GetComponent_Component_Type(node.transform:Find("Flower/text"), typeof(UILabel)).text = "0"
                    end
                else
                    CommonDefs.GetComponent_Component_Type(node.transform:Find("Flower/text"), typeof(UILabel)).text = "0"
                end
                CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = CPersonalSpaceMgr.GetTimeString(math.floor(info.publishtime / 1000))
            elseif eventType == CPersonalSpace_EnumEventType.ePopularity then
                local cls = tonumber(info.roleinfo.clazz)
                local gender = tonumber(info.roleinfo.gender)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, -1), false)

                CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = info.roleinfo.grade
                CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.roleinfo.rolename

                CPersonalSpaceMgr.Inst:addPlayerLinkBtn(node.transform:Find("icon/button").gameObject, info.roleinfo.roleid, EnumReportId_lua.eDefault, "", nil, 0, 0)

                UIEventListener.Get(node.transform:Find("button").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                    CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(info.roleinfo.roleid, 0)
                end)
            elseif eventType == CPersonalSpace_EnumEventType.eAttention then
                local cls = tonumber(info.roleinfo.clazz)
                local gender = tonumber(info.roleinfo.gender)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, -1), false)

                CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = info.roleinfo.grade
                CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.roleinfo.rolename

                CPersonalSpaceMgr.Inst:addPlayerLinkBtn(node.transform:Find("icon/button").gameObject, info.roleinfo.roleid, EnumReportId_lua.eDefault, "", nil, 0, 0)

                if info.followingeach then
                    node.transform:Find("attentionSign").gameObject:SetActive(true)
                else
                    node.transform:Find("attentionSign").gameObject:SetActive(false)
                end

                UIEventListener.Get(node.transform:Find("button").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                    CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(info.roleinfo.roleid, 0)
                end)
            end
            i = i + 1
        end
    end

    table:Reposition()
    scrollview:ResetPosition()
end
CPersonalSpaceWnd.m_OpenRenqiDetailPanel_CS2LuaHook = function (this, go)
    if this.PersonalID > 0 then
        local saveId = this.PersonalID
        CPersonalSpaceMgr.Inst:GetEvents(this.PersonalID, CPersonalSpace_EnumEventType.ePopularity, DelegateFactory.Action_CPersonalSpace_GetEvents_Ret(function (data)
            if not this.systemPortrait then
                return
            end
            if this and saveId == this.PersonalID and data.code == 0 then
                this:InitEventDetailPanel(this.leaveMessagePanelNode, this.renqiDetailPanel, CPersonalSpace_EnumEventType.ePopularity, this.renqiDetailTemplate, data.data)
            end
        end), nil)
    end
end
CPersonalSpaceWnd.m_OpenAttentionDetailPanel_CS2LuaHook = function (this, go)
  LuaPersonalSpaceMgrReal.UnFollowTable = nil
  local freBtn = this.attentionDetialPanel.transform:Find('QnNumButton'):GetComponent(typeof(QnAddSubAndInputButton))
  freBtn.onValueChanged = DelegateFactory.Action_uint(function (value)
    LuaPersonalSpaceMgrReal.GetHistoryData(value,function(data)
      if not this.systemPortrait then
        return
      end
      if data.code == 0 then
        --this:InitEventDetailPanel(this.friendCircleNode, this.attentionDetialPanel, CPersonalSpace_EnumEventType.eAttention, this.attentionDetialTemplate, data.data)
        LuaPersonalSpaceMgrReal.InitFollowedDetailPanel(this, this.friendCircleNode, this.attentionDetialPanel, this.attentionDetialTemplate, data.data, value, freBtn)
      end
    end)
    -- CPersonalSpaceMgr.Inst:GetEvents(saveId, CPersonalSpace_EnumEventType.eAttention, DelegateFactory.Action_CPersonalSpace_GetEvents_Ret(function (data)
    -- end), nil)
  end)
	freBtn:SetMinMax(1,999,1)
	freBtn:SetValue(1, true)
end
CPersonalSpaceWnd.m_OpenFlowerDetailPanel_CS2LuaHook = function (this, go)
    if this.PersonalID > 0 then
        local saveId = this.PersonalID
        CPersonalSpaceMgr.Inst:GetEvents(this.PersonalID, CPersonalSpace_EnumEventType.eFlower, DelegateFactory.Action_CPersonalSpace_GetEvents_Ret(function (data)
            if not this.systemPortrait then
                return
            end
            if this and saveId == this.PersonalID and data.code == 0 then
                this:InitEventDetailPanel(this.leaveMessagePanelNode, this.flowerDetailPanel, CPersonalSpace_EnumEventType.eFlower, this.flowerDetailTemplate, data.data)
            end
        end), nil)
    end
end
CPersonalSpaceWnd.m_cleanBtn_CS2LuaHook = function (this, btn)
    UIEventListener.Get(btn).onClick = nil
end
CPersonalSpaceWnd.m_SetPicBack_CS2LuaHook = function (this, strResult)
    this:OnAvaterCallBack(strResult)
end
CPersonalSpaceWnd.m_OnAvaterCallBack_CS2LuaHook = function (this, strResult)
    if (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Success)) then
        -- 成功
        this:StartCoroutine(this:LoadTexture(CPersonalSpaceWnd.PortraitSaveName))
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Cancel)) then
        -- 取消
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Failed)) then
        -- 失败
    end
end
CPersonalSpaceWnd.m_SetPortraitPicBack_CS2LuaHook = function (this, texture)
    if texture == nil then
        return
    end

    local portraitEmptyNode = this.selfPortraitNode.transform:Find("Label").gameObject
    g_MessageMgr:ShowMessage("MENGDAO_TUPIAN_SHANGCHUANZHONG")
    local loadTexture = texture
    this:UploadPic(CPersonalSpaceMgr.Upload_PortraitPhotoType, loadTexture, DelegateFactory.Action_string(function (url)
        CPersonalSpaceMgr.Inst:SetPhoto(url, nil, nil)
        g_MessageMgr:ShowMessage("MENGDAO_TUPIAN_SHANGCHUANCHENGGONG")
        if not this.systemPortrait then
            return
        end
        portraitEmptyNode:SetActive(false)
        this.selfPortraitNodeTexture.mainTexture = loadTexture
    end))
end
CPersonalSpaceWnd.m_ShowSignatureVoicePanel_CS2LuaHook = function (this, signatureVoice)
    this.playSignatureVoicePanel:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(this.playSignatureVoicePanel, typeof(CPersonalSpacePlaySignatureVoicePanel)):Init(signatureVoice)
end
CPersonalSpaceWnd.m_SetSignatureVoice_CS2LuaHook = function (this, signatureVoice)
  LuaPersonalSpaceMgrReal.SetPersonalSpaceVoice(this, signatureVoice)
end
CPersonalSpaceWnd.m_InitUsedNamePanel_CS2LuaHook = function (this, profile)
    this.usedNamePanel:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(this.usedNamePanel, typeof(CPersonalSpaceUsedNameDetailView)):Init(profile)
end
CPersonalSpaceWnd.m_SetServerDifFunction_CS2LuaHook = function (this, _data)
    if not this.systemPortrait then
        return
    end

    this:UpdateOpBtn(_data)

    if CClientMainPlayer.Inst == nil then
        this.tabBar:ChangeTab(0, false)
        this.expressionBtn:SetActive(false)
        this.tabBar:GetTabGoByIndex(2):SetActive(false)

        UIEventListener.Get(this.shituButton).onClick = DelegateFactory.VoidDelegate(function (p)
            g_MessageMgr:ShowMessage("UNABLE_TO_VISIT_FAMILYTREE")
        end)
    else
        this.tabBar:GetTabGoByIndex(2):SetActive(true)

        UIEventListener.Get(this.shituButton).onClick = DelegateFactory.VoidDelegate(function (p)
            CFamilyTreeMgr.Instance:SendFamilyTreeRequest(this.PersonalID)
        end)

        if CPersonalSpaceMgr.Inst.openDefaultTab == 0 then
            if this.PersonalID == this.SelfRoleInfo.id then
                this.tabBar:ChangeTab(0, false)
                this.expressionBtn:SetActive(true)
            else
                this.tabBar:ChangeTab(2, false)
                this.expressionBtn:SetActive(false)
            end
        elseif CPersonalSpaceMgr.Inst.openDefaultTab > 0 and CPersonalSpaceMgr.Inst.openDefaultTab <= 2 then
            this.tabBar:ChangeTab(CPersonalSpaceMgr.Inst.openDefaultTab - 1, false)
        end
    end
end
CPersonalSpaceWnd.m_InitUserProfile_CS2LuaHook = function (this, id)
  LuaWishMgr.OpenUserId = id

    this.m_Native = CreateFromClass(NativeHandle)

    this.PersonalID = id
    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Name/text"), typeof(UILabel)).text = ""
    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Lv/text"), typeof(UILabel)).text = ""
    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Title/text"), typeof(UILabel)).text = CPersonalSpaceWnd.titleDefaultText
    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Location/text"), typeof(UILabel)).text = CPersonalSpaceWnd.positionDefaultText
    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("OtherLocation/text"), typeof(UILabel)).text = CPersonalSpaceWnd.otherPositionDefaultText
    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Guild/text"), typeof(UILabel)).text = CPersonalSpaceWnd.guildDefaultText
    this.jobIconSprite.spriteName = ""

    CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Popular/text"), typeof(UILabel)).text = "0"
    CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Gift/text"), typeof(UILabel)).text = "0"
    CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Flower/text"), typeof(UILabel)).text = "0"
    CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Popular/text"), typeof(UILabel)).text = "0"
    CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Gift/text"), typeof(UILabel)).text = "0"
    CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Flower/text"), typeof(UILabel)).text = "0"

    local topBarPopularBtn = this.topBarNode.transform:Find("Popular/button").gameObject
    local topBarGiftBtn = this.topBarNode.transform:Find("Gift/button").gameObject
    local topBarFlowerBtn = this.topBarNode.transform:Find("Flower/button").gameObject

    local otherTopBarPopularBtn = this.otherTopBarNode.transform:Find("Popular/button").gameObject
    --GameObject otherTopBarGiftBtn = otherTopBarNode.transform.FindChild("Gift/button").gameObject;
    local otherTopBarFlowerBtn = this.otherTopBarNode.transform:Find("Flower/button").gameObject
    this:cleanBtn(topBarPopularBtn)
    this:cleanBtn(topBarGiftBtn)
    this:cleanBtn(topBarFlowerBtn)
    this:cleanBtn(otherTopBarPopularBtn)
    this:cleanBtn(otherTopBarFlowerBtn)

    local portraitEmptyNode = this.selfPortraitNode.transform:Find("Label").gameObject
    portraitEmptyNode:SetActive(false)
    local portraitSetButton = this.selfPortraitNode.transform:Find("addPicButton").gameObject
    UIEventListener.Get(portraitSetButton).onClick = MakeDelegateFromCSFunction(this.AddPic, VoidDelegate, this)

    this.SpacePricacyInfo = CreateFromClass(CPersonalSpace_Privacy)

    CPersonalSpaceMgr.Inst:GetUserProfile(id, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (data)

        if not this.systemPortrait then
            return
        end

        if data.code == 0 then
            local _data = data.data
            this.PersonalName = _data.rolename
            this.PersonalProfile = _data

            if id == this.SelfRoleInfo.id then
                _data = this:CheckUserDataAndUpdate(data.data, this.SelfRoleInfo)
                local p1 = data.data
                local p2 = this.SelfRoleInfo
                if p1.rolename~=p2.name or p1.title.id~=p2.title.id or p1.grade~=tonumber(p2.grade)
                    or p1.guild.id~=p2.guild.id or p1.guild.name~=p2.guild.name or p1.used_name~=p2.used_name then
                    CLuaPersonalSpaceMgr.PushPlayerExtraInfo()
                end
            end

            this:SetServerDifFunction(_data)

            local cls = _data.clazz
            local gender = _data.gender
            this.systemPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, _data.expression_base.expression), false)
            local textTexture = CommonDefs.GetComponent_Component_Type(this.systemPortrait.transform.parent:Find("text"), typeof(CUITexture))
            CExpressionMgr.Inst:SetIconTextInfo(textTexture, _data.expression_base, _data.expression_extra, true)
            local sticker = CommonDefs.GetComponent_Component_Type(this.systemPortrait.transform.parent:Find("sticker"), typeof(CUITexture))
            CExpressionMgr.Inst:SetIconTextInfo(sticker, _data.expression_base, _data.expression_extra, false)

            this.jobIconSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (cls)))
            CExpressionMgr.Inst.playerIconUrl = _data.photo
            if System.String.IsNullOrEmpty(_data.photo) then
                this.selfPortraitNode:SetActive(false)
                this.systemPortraitNode:SetActive(true)
                portraitEmptyNode:SetActive(true)
            else
                this.selfPortraitNode:SetActive(true)
                this.systemPortraitNode:SetActive(false)
                portraitEmptyNode:SetActive(false)
                CPersonalSpaceMgr.Inst:DownLoadPortraitPic(_data.photo, this.selfPortraitNodeTexture, nil)
            end

            CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Name/text"), typeof(UILabel)).text = _data.rolename
            local levelLabel = CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Lv/text"), typeof(UILabel))
            local default
            if _data.xianfanstatus > 0 then
                default = L10.Game.Constants.ColorOfFeiSheng
            else
                default = NGUIText.EncodeColor24(levelLabel.color)
            end
            levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, CPersonalSpaceWnd.playerLevelPrefix .. _data.grade)
            if not System.String.IsNullOrEmpty(_data.title.id) and not System.String.IsNullOrEmpty(_data.title.name) then
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Title/text"), typeof(UILabel)).text = _data.title.name
                local titleData = Title_Title.GetData(tonumber(_data.title.id))
                if titleData ~= nil then
                    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Title/text"), typeof(UILabel)).color = CTitleMgr.Inst:GetTitleColor(titleData)
                end
            end

            PersonalSpaceLua:OnLoadPersonalSpaceNewBgData(data.data.background.backgroundid)
            
            if not System.String.IsNullOrEmpty(_data.guild.name) then
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Guild/text"), typeof(UILabel)).text = _data.guild.name
            end
            if not System.String.IsNullOrEmpty(_data.signature) then
                this.SaveSignatureString = _data.signature
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Signature/Label/bg"), typeof(UIInput)).value = _data.signature
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Signature/Label"), typeof(UILabel)).text = _data.signature
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("OtherSignature/Label"), typeof(UILabel)).text = _data.signature
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("OtherSignature/Label"), typeof(UILabel)).color = Color.white
            end
            LuaPersonalSpaceMgrReal.SetPersonalSpaceVoice(this,_data.signaturevoice,_data.roleid)

            CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Popular/text"), typeof(UILabel)).text = tostring(_data.renqi)
            CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Gift/text"), typeof(UILabel)).text = tostring(_data.gift)
            CommonDefs.GetComponent_Component_Type(this.topBarNode.transform:Find("Flower/text"), typeof(UILabel)).text = tostring(_data.flowerrenqi)
            CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Popular/text"), typeof(UILabel)).text = tostring(_data.renqi)
            CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Gift/text"), typeof(UILabel)).text = tostring(_data.gift)
            CommonDefs.GetComponent_Component_Type(this.otherTopBarNode.transform:Find("Flower/text"), typeof(UILabel)).text = tostring(_data.flowerrenqi)
            CExpressionMgr.Inst.Renqi = (_data.renqi)

            if id == this.SelfRoleInfo.id then
                this:SetPrivacySetting(_data.privacy)
                this:SetUsedNameSign(_data.hideusedname)
            end
            this.SpacePricacyInfo = _data.privacy

            if id ~= this.SelfRoleInfo.id and this.SpacePricacyInfo.location then
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Location/text"), typeof(UILabel)).text = CPersonalSpaceWnd.noShowPositionDefaultText
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("OtherLocation/text"), typeof(UILabel)).text = CPersonalSpaceWnd.noShowPositionDefaultText
            elseif not System.String.IsNullOrEmpty(_data.location) then
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Location/text"), typeof(UILabel)).text = _data.location
                CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("OtherLocation/text"), typeof(UILabel)).text = _data.location
            end

            UIEventListener.Get(topBarPopularBtn).onClick = MakeDelegateFromCSFunction(this.OpenRenqiDetailPanel, VoidDelegate, this)
            UIEventListener.Get(topBarFlowerBtn).onClick = MakeDelegateFromCSFunction(this.OpenFlowerDetailPanel, VoidDelegate, this)
            UIEventListener.Get(otherTopBarPopularBtn).onClick = MakeDelegateFromCSFunction(this.OpenRenqiDetailPanel, VoidDelegate, this)
            UIEventListener.Get(otherTopBarFlowerBtn).onClick = MakeDelegateFromCSFunction(this.OpenFlowerDetailPanel, VoidDelegate, this)
            UIEventListener.Get(topBarGiftBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                this:InitSetGiftPanel()
            end)

            UIEventListener.Get(this.usedNameBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                this:InitUsedNamePanel(this.PersonalProfile)
            end)
        end
    end), nil)
end
CPersonalSpaceWnd.m_InitOtherPersonalInfo_CS2LuaHook = function (this, id)

    if CPersonalSpaceMgr.LBSEnable then
        this.LBSButton:SetActive(false)
        UIEventListener.Get(this.LBSButton).onClick = nil
    else
        this.LBSButton:SetActive(false)
    end

    --this.tabBar:GetTabGoByIndex(1):SetActive(false)
    this.tabBar:GetTabGoByIndex(3):SetActive(false)

    this:InitUserProfile(id)

    this.personalInfoNode.transform:Find("Signature").gameObject:SetActive(false)
    this.personalInfoNode.transform:Find("OtherSignature").gameObject:SetActive(true)
    this.personalInfoNode.transform:Find("Location").gameObject:SetActive(false)
    this.personalInfoNode.transform:Find("OtherLocation").gameObject:SetActive(false)
    CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Portrait/SelfPortrait/Label"), typeof(UILabel)).text = CPersonalSpaceWnd.OtherPortraitEmptyString

    this.selfOpArea:SetActive(false)
    this.otherOpArea:SetActive(true)
    this.topBarNode:SetActive(false)
    this.otherTopBarNode:SetActive(true)

    UIEventListener.Get(this.otherOpArea.transform:Find("MyCircleButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(this.SelfRoleInfo.id, 0)
    end)

    local portraitEmptyNode = this.selfPortraitNode.transform:Find("Label").gameObject
    portraitEmptyNode:SetActive(false)
    local portraitSetButton = this.selfPortraitNode.transform:Find("addPicButton").gameObject
    portraitSetButton:SetActive(false)

    local infoShowBtn = this.personalInfoNode.transform:Find("Portrait/InfoShowButton").gameObject
    infoShowBtn:SetActive(true)
    UIEventListener.Get(infoShowBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CPersonalSpaceMgr.Inst:addPlayerLinkBtn(infoShowBtn, id, EnumReportId_lua.eMengDaoInfo, "", nil, 0, 0)
    end)
end
CPersonalSpaceWnd.m_ShowMomentDetailThroughFriendCircleById_CS2LuaHook = function (this, info_id, showTalk, formerNode)
    CPersonalSpaceMgr.Inst:GetSingleMomentInfo(info_id, DelegateFactory.Action_CPersonalSpace_GetSingleMoment_Ret(function (ret)
        if not this.systemPortrait then
            return
        end
        if ret.code == 0 then
            --CPersonalSpace_GetMomentItem[] updateList = { ret.data };
            --if(CPersonalSpaceMgr.Inst.otherMomentCacheDic.con)
            --CPersonalSpace_MomentCacheData cdata = new CPersonalSpace_MomentCacheData();
            --cdata.data = ret.data;
            --CPersonalSpaceMgr.Inst.otherMomentCacheDic[info_id] = ret.data;
            this:ShowMomentDetailThroughFriendCircle(ret.data, showTalk, formerNode)
        elseif ret.code == - 1 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该心情不存在！"))
        end
    end), nil)
end
CPersonalSpaceWnd.m_ShowMomentDetailThroughFriendCircle_CS2LuaHook = function (this, info, showTalk, formerNode)

    if not info.commentable and showTalk then
        g_MessageMgr:ShowMessage("CANNOT_COMMENT")
        return
    end

    if formerNode == nil then
        formerNode = this.friendCircleNode
    end
    this.personalSpaceDetailMoment.gameObject:SetActive(true)
    this.personalSpaceDetailMoment:Init(info, formerNode, UIWidget.Pivot.Top, showTalk, true)
end
CPersonalSpaceWnd.m_UpdateZanListBySelf_CS2LuaHook = function (this, sign, info)
    local exist = false
    do
        local i = 0
        while i < info.zanlist.Length do
            local _zan_info = info.zanlist[i]
            if _zan_info.roleid == this.SelfRoleInfo.id then
                exist = true
            end
            i = i + 1
        end
    end

    local saveData = CreateFromClass(CPersonalSpace_MomentCacheData)
    if info.type == CPersonalSpace_CircleGenerateType.HotMoments then
        if CommonDefs.DictContains(CPersonalSpaceMgr.Inst.hotMomentDic, typeof(UInt64), info.id) then
            saveData = CommonDefs.DictGetValue(CPersonalSpaceMgr.Inst.hotMomentDic, typeof(UInt64), info.id)
        else
            return false
        end
    elseif info.type == CPersonalSpace_CircleGenerateType.AllHotMoments then
        if CommonDefs.DictContains(CPersonalSpaceMgr.Inst.allhotMomentDic, typeof(UInt64), info.id) then
            saveData = CommonDefs.DictGetValue(CPersonalSpaceMgr.Inst.allhotMomentDic, typeof(UInt64), info.id)
        else
            return false
        end
    else
        if CommonDefs.DictContains(CPersonalSpaceMgr.Inst.momentCacheDic, typeof(UInt64), info.id) then
            saveData = CommonDefs.DictGetValue(CPersonalSpaceMgr.Inst.momentCacheDic, typeof(UInt64), info.id)
        else
            return false
        end
    end

    if not sign then
        if info.zanlist.Length >= 0 then
            local other = 0
            if exist then
                other = 1
            else
                return false
            end
            local newList = CreateFromClass(MakeArrayClass(CPersonalSpace_Zan), info.zanlist.Length - other)
            local count = 0
            do
                local i = 0
                while i < info.zanlist.Length do
                    local _zan_info = info.zanlist[i]
                    if _zan_info.roleid ~= this.SelfRoleInfo.id then
                        newList[count] = _zan_info
                        count = count + 1
                    end
                    i = i + 1
                end
            end

            saveData.data.zanlist = newList
            saveData.data.zancount = saveData.data.zancount - other
            saveData.data.showFavorSign = false
        else
            return true
        end
    else
        local other = 1
        if exist then
            return false
        end
        local newZan = CreateFromClass(CPersonalSpace_Zan)
        newZan.roleid = this.SelfRoleInfo.id
        newZan.rolename = this.SelfRoleInfo.name

        local newList = CreateFromClass(MakeArrayClass(CPersonalSpace_Zan), info.zanlist.Length + other)
        do
            local i = 0
            while i < info.zanlist.Length do
                local _zan_info = info.zanlist[i]
                newList[i] = _zan_info
                i = i + 1
            end
        end
        newList[newList.Length - 1] = newZan
        saveData.data.zanlist = newList
        saveData.data.zancount = saveData.data.zancount + other
        saveData.data.showFavorSign = true
    end

    local updateList = Table2ArrayWithCount({saveData.data}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))
    this:updateMomentCache(updateList, false, info.type, nil, false)

    return true
end
CPersonalSpaceWnd.m_DeleteMomentBack_CS2LuaHook = function (this, data, info, genType)
    if not this.systemPortrait then
        return
    end
    if data.code == 0 then
        this:deleteMomentCache(info.id, genType)
        if genType == CPersonalSpace_CircleGenerateType.HotMoments or genType == CPersonalSpace_CircleGenerateType.AllHotMoments then
            local scrollView = CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.scrollView
            if scrollView == nil then
                return
            end
            local table = CommonDefs.GetComponent_Component_Type(scrollView.transform:Find("Table"), typeof(UITable))
            local panel = CommonDefs.GetComponent_Component_Type(scrollView, typeof(UIPanel))
            local emptyNode = CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.emptyNode

            if genType == CPersonalSpace_CircleGenerateType.HotMoments then
                this:InitMomentCircleList(scrollView, panel, table, genType, CPersonalSpaceMgr.Inst.hotMomentList, emptyNode, true, 0)
            elseif genType == CPersonalSpace_CircleGenerateType.AllHotMoments then
                this:InitMomentCircleList(scrollView, panel, table, genType, CPersonalSpaceMgr.Inst.allhotMomentList, emptyNode, true, 0)
            end
        else
            this:InitMomentCircleList(this.statusListScrollView, this.statusListPanel, this.statusListTable, this.SaveCircleGenerateType, CPersonalSpaceMgr.Inst.momentCacheList, this.sendStatusPanel_emptyNode, true, 0)
        end
    end
end
CPersonalSpaceWnd.m_SetMomentPic_CS2LuaHook = function (this, node, infoArray, index)

    local info = infoArray[index]

    if System.String.IsNullOrEmpty(info.thumb) then
        return
    end

    --TODO check cache
    this:cleanBtn(node)

    --CPersonalSpaceMgr.Inst.DownLoadPortraitPic(info.thumb, node.GetComponent<UITexture>(), delegate(Texture texture)
    --{

    --});

    CPersonalSpaceMgr.DownLoadPic(info.thumb, CommonDefs.GetComponent_GameObject_Type(node, typeof(UITexture)), CPersonalSpaceMgr.MaxSmallPicHeight, CPersonalSpaceMgr.MaxSmallPicWidth, DelegateFactory.Action(function ()
        if not this.systemPortrait then
            return
        end
        UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (p)
            this:InitDetailPicPanel(infoArray, index, nil)
        end)
    end), false, true)
end
CPersonalSpaceWnd.m_SetBoxCollider_CS2LuaHook = function (this, boxSprite)
    if boxSprite ~= nil then
        local boxObject = boxSprite.gameObject
        local old = CommonDefs.GetComponent_GameObject_Type(boxObject, typeof(BoxCollider))
        if old ~= nil then
            Object1.Destroy(old)
        end

        local newBox = CommonDefs.AddComponent_GameObject_Type(boxObject, typeof(BoxCollider))
        local newHeight = boxSprite.height + CPersonalSpaceWnd.ColliderMoreNum
        newBox.center = Vector3(0, math.floor(- newHeight / 2), 0)
        newBox.size = Vector3(boxSprite.width, newHeight, 0)
    end
end
function PersonalSpaceLua:TransString(s)
  local result = string.gsub(s, '\r', '')
  result = string.gsub(result, '\n', '')
  result = string.gsub(result, ' ', '')
  return result
end

function PersonalSpaceLua:CheckStatusLabelShowMode(this,statusLabel,id)
--  if self.moreMomentShowTable and self.moreMomentShowTable[id] then
--    statusLabel.maxLineCount = 80
--  else
--    statusLabel.maxLineCount = 8
--  end
end

function PersonalSpaceLua:CheckStatusLabel(this,statusLabel,moreBtn,nodeData,scrollView,generateType)
  local id = nodeData.id
  local buttonHeight = 62
  if statusLabel.maxLineCount == 80 then
    moreBtn:SetActive(true)
    moreBtn:GetComponent(typeof(UILabel)).text = LocalString.GetString('[00FFFF][收起][-][c]')
    local oldHeight = statusLabel.height
    UIEventListener.Get(moreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
      if self.moreMomentShowTable then
        self.moreMomentShowTable[id] = nil
      end
      statusLabel.maxLineCount = 8
      local updateList = Table2ArrayWithCount({nodeData}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))
      this:updateMomentCache(updateList, false, generateType, nil, false)
      local newHeight = statusLabel.height
      this:ReposFriendCircleList(scrollView, nil, nil, oldHeight-newHeight)
    end)
    moreBtn.transform.localPosition = Vector3(moreBtn.transform.localPosition.x,-statusLabel.height,moreBtn.transform.localPosition.z)
    return buttonHeight
  else
    local processText = self:TransString(statusLabel.processedText)
    local statusText = self:TransString(statusLabel.text)

    if processText == statusText then
      moreBtn:SetActive(false)
      return 0
    else
      moreBtn:SetActive(true)
      moreBtn:GetComponent(typeof(UILabel)).text = LocalString.GetString('[00FFFF][全文][-][c]')
      UIEventListener.Get(moreBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if not self.moreMomentShowTable then
          self.moreMomentShowTable = {}
        end
        self.moreMomentShowTable[id] = true
        statusLabel.maxLineCount = 80
        local updateList = Table2ArrayWithCount({nodeData}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))
        this:updateMomentCache(updateList, false, generateType, nil, false)
        this:ReposFriendCircleList(scrollView, nil, nil, 0)
      end)
      moreBtn.transform.localPosition = Vector3(moreBtn.transform.localPosition.x,-statusLabel.height,moreBtn.transform.localPosition.z)
      return buttonHeight
    end
  end
end

CPersonalSpaceWnd.m_GenerateFriendCircleListNodeFunction_CS2LuaHook = function (this, node, info, scrollView, totalReNew, formerNode, generateType)
    CPersonalSpaceMgr.Inst:SetNodeDragScroll(node, scrollView)
    local subInfo = node.transform:Find("subInfo")
    local statusLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("status"), typeof(UILabel))
    local favorIcon = node.transform:Find("subInfo/favorbutton/favoricon").gameObject
    local favorbuttonSign = node.transform:Find("subInfo/favorbutton/favoricon/UI_aixin").gameObject
    local favorNumLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/favorbutton/favornum"), typeof(UILabel))

    local favorbarNode = node.transform:Find("subInfo/favorbar").gameObject
    local favorbarLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/favorbar/text"), typeof(UILabel))
    local leaveMessageNumText = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/leavemessagebutton/leavemessagenum"), typeof(UILabel))

    --if (totalReNew)
    --{
    local ext = LuaPersonalSpaceMgrReal.GetLiangHaoIcon(info)

    if System.String.IsNullOrEmpty(info.rolename) then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = tostring(info.roleid)..ext
    else
        CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.rolename..ext
    end

    local vipNode = node.transform:Find("name/vipicon")
    if vipNode ~= nil then
        CPersonalSpaceMgr.Inst:SetSpLevelNode(info.splevel, vipNode.gameObject)
    end

    if CClientMainPlayer.Inst:GetMyServerId() ~= info.server_id and info.server_id ~= 0 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text .. (CPersonalSpaceMgr.ServerSplitChar .. info.server_name)
    end
    
    local translateButton = node.transform:Find("TranslateButton").gameObject
    local translateHighlight = node.transform:Find("TranslateButton/Highlight").gameObject
    translateHighlight:SetActive(false)
    local followBtn = node.transform:Find("followbutton").gameObject
    local cancelFollowBtn = node.transform:Find("cancelFollowBtn").gameObject
    statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).isWaitTranslate = false
    if generateType == CPersonalSpace_CircleGenerateType.HotMoments or generateType == CPersonalSpace_CircleGenerateType.AllHotMoments and info.roleid ~= CIMMgr.PERSONAL_SPACE_HOT_POINT_ID and not CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.roleid, nil, nil) then
        UIEventListener.Get(followBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            local backFunction = function(result)
              if not this or not this.closeBtn then
                return
              end
              if result then
                followBtn:SetActive(false)
                cancelFollowBtn:SetActive(true)
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注玩家成功!"))
              end
            end
            LuaPersonalSpaceMgrReal.SetPlayerFollow(info.roleid,backFunction)
        end)

        UIEventListener.Get(cancelFollowBtn).onClick = DelegateFactory.VoidDelegate(function (go)
            local backFunction = function(result)
              if not this or not this.closeBtn then
                return
              end
              if result then
                followBtn:SetActive(true)
                cancelFollowBtn:SetActive(false)
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("取消关注玩家成功!"))
              end
            end
            LuaPersonalSpaceMgrReal.CancelPlayerFollow(info.roleid,backFunction)
        end)
        if not info.isfollowing then
            followBtn:SetActive(true)
            cancelFollowBtn:SetActive(false)
        else
            followBtn:SetActive(false)
            cancelFollowBtn:SetActive(true)
        end
    else
        followBtn:SetActive(false)
        cancelFollowBtn:SetActive(false)
    end

    UIEventListener.Get(translateButton).onClick = DelegateFactory.VoidDelegate(function (go)
        if (translateHighlight.gameObject.activeSelf) then
            local totalShowText = string.gsub(string.gsub(info.text, "&lt;", "<"), "&gt;", ">")
            totalShowText = totalShowText .. CPersonalSpaceMgr.GenerateForwardString(info.previousforwards)
            totalShowText = CChatMgr.Inst:FilterYangYangEmotion(totalShowText)
            PersonalSpaceLua:CheckStatusLabelShowMode(this,statusLabel,info.id)
            statusLabel.text = SafeStringFormat3('[c][FFFFFF]%s[-][/c]',CChatLinkMgr.TranslateToNGUIText(totalShowText, false))
            translateHighlight.gameObject:SetActive(false)
            statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).isWaitTranslate = false
        else
            LuaSEASdkMgr:QueryGoogleTranslate((info.text):gsub('#', ' '))
            statusLabel.text = ''
            translateHighlight.gameObject:SetActive(true)
            statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).isWaitTranslate = true
            statusLabel:GetComponent(typeof(CGoogleTranslateLabel)).sourceString = (info.text):gsub('#', ' ')
        end
    end)

    -- 删除按钮
    local deleteCheckBox = node.transform:Find("DeleteCheckBox"):GetComponent(typeof(QnCheckBox))
    if this.IsDeleteSelfMomentMode then
        deleteCheckBox.gameObject:SetActive(true)

        local hasSet = CommonDefs.DictContains(this.NeedToDeleteSet,typeof(UInt64), info.id)

        if hasSet then
            local resetValue = this.NeedToDeleteSet[info.id]
            local needReset = resetValue ~= info
            if needReset then
                CommonDefs.DictRemove(this.NeedToDeleteSet, typeof(UInt64), info.id)
                CommonDefs.DictAdd(this.NeedToDeleteSet, typeof(UInt64), info.id, typeof(CPersonalSpace_GetMomentItem), info)
            end
        end
        deleteCheckBox.Selected = hasSet

        deleteCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(value)
            local hasSet = CommonDefs.DictContains(this.NeedToDeleteSet,typeof(UInt64), info.id)
            if value ~= hasSet then
                if value then
                    CommonDefs.DictAdd(this.NeedToDeleteSet, typeof(UInt64), info.id, typeof(CPersonalSpace_GetMomentItem), info)
                else
                    CommonDefs.DictRemove(this.NeedToDeleteSet, typeof(UInt64), info.id)
                end
            end

            if this.NeedToDeleteSet.Count == 0 then
                this.deleteDescLabel.text = ""
            else
                this.deleteDescLabel.text = SafeStringFormat3(LocalString.GetString("已选%s条动态"), this.NeedToDeleteSet.Count)
            end
        end)
    else
        deleteCheckBox.gameObject:SetActive(false)
    end

    if LuaPersonalSpaceMgrReal.EnableUpMoment then
      local upBtn = node.transform:Find('upbutton').gameObject
      LuaPersonalSpaceMgrReal.SetMomentTopicNodeInfo(upBtn,generateType,info,node,this)
    else
      local upBtn = node.transform:Find('upbutton')
      if upBtn then
        upBtn.gameObject:SetActive(false)
      end
    end

    if info.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = ""
    elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.roleid, node.transform:Find("icon").gameObject, CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel))) then
    else
        local levelLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel))
        local default
        if info.xianfanstatus > 0 then
            default = L10.Game.Constants.ColorOfFeiSheng
        else
            default = NGUIText.EncodeColor24(levelLabel.color)
        end
        levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, "lv." .. info.grade)

        --if (!string.IsNullOrEmpty(info.clazz) && !string.IsNullOrEmpty(info.gender))
        --{
        --    uint cls = uint.Parse(info.clazz);
        --    uint gender = uint.Parse(info.gender);
        --    node.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
        --}

        CPersonalSpaceMgr.LoadPortraitPic(info.photo, info.clazz, info.gender, node.transform:Find("icon").gameObject, info.expression_base)

        if info.roleid ~= this.SelfRoleInfo.id then
            CPersonalSpaceMgr.Inst:addPlayerLinkBtn(node.transform:Find("icon/button").gameObject, info.roleid, EnumReportId_lua.eMengDaoMoment, info.text, info.imglist, info.id, 0)
        end
    end

    local totalShowText = string.gsub(string.gsub(info.text, "&lt;", "<"), "&gt;", ">")
    totalShowText = totalShowText .. CPersonalSpaceMgr.GenerateForwardString(info.previousforwards)
    totalShowText = CChatMgr.Inst:FilterYangYangEmotion(totalShowText)

    PersonalSpaceLua:CheckStatusLabelShowMode(this,statusLabel,info.id)
    statusLabel.text = ''
    statusLabel.text = SafeStringFormat3('[c][FFFFFF]%s[-][/c]',CChatLinkMgr.TranslateToNGUIText(totalShowText, false))
    local moreBtn = node.transform:Find("moreBtn").gameObject
    local moreDis = PersonalSpaceLua:CheckStatusLabel(this,statusLabel,moreBtn,info,scrollView,generateType)

    UIEventListener.Get(statusLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
            return
        else
            this:ShowMomentDetailThroughFriendCircle(info, false, formerNode)
        end
    end)

    UIEventListener.Get(node.transform:Find("subInfo/leavemessagebutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        this:ShowMomentDetailThroughFriendCircle(info, true, formerNode)
    end)

    local deleteBtn = node.transform:Find("subInfo/deletebutton").gameObject
    if info.roleid == this.SelfRoleInfo.id then
        deleteBtn:SetActive(true)
        UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            MessageWndManager.ShowConfirmMessage(CPersonalSpaceWnd.DeleteMomentAlertString, 10, false, DelegateFactory.Action(function ()
                CPersonalSpaceMgr.Inst:DelMoment(info.id, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)
                    if not this.systemPortrait then
                        return
                    end
                    this:DeleteMomentBack(data, info, generateType)
                end), nil)
            end), nil)
        end)
    else
        deleteBtn:SetActive(false)
    end

    --}

    local bgBtn = node.transform:Find("bgbutton").gameObject
    local selfBgBtn = node.transform:Find("selfbgbutton").gameObject
    bgBtn:SetActive(true)
    selfBgBtn:SetActive(true)

    UIEventListener.Get(bgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:ShowMomentDetailThroughFriendCircle(info, false, formerNode)
    end)

    UIEventListener.Get(selfBgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:ShowMomentDetailThroughFriendCircle(info, false, formerNode)
    end)

    local infoTime = info.createtime / 1000
    local nowTime = CServerTimeMgr.Inst.timeStamp
    local disTime = nowTime - infoTime
    if disTime < 0 then
        disTime = 0
    end

    if disTime < 60 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = LocalString.GetString("刚刚")
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = LocalString.GetString("刚刚")
    elseif disTime < 3600 then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = math.ceil(disTime / 60) .. LocalString.GetString("分钟前")
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = math.ceil(disTime / 60) .. LocalString.GetString("分钟前")
    elseif disTime < 86400 --[[24 * 3600]] then
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = math.ceil(disTime / 3600) .. LocalString.GetString("小时前")
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = math.ceil(disTime / 3600) .. LocalString.GetString("小时前")
    else
        local infoTimeDate = CServerTimeMgr.ConvertTimeStampToZone8Time(infoTime)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/time"), typeof(UILabel)).text = CPersonalSpaceMgr.GetTimeString(infoTime)
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = ((infoTimeDate.Month .. LocalString.GetString("月")) .. infoTimeDate.Day) .. LocalString.GetString("日")
        -- 那年信息
        CommonDefs.GetComponent_Component_Type(node.transform:Find("time/yeartime"), typeof(UILabel)).text = infoTimeDate.Year .. LocalString.GetString("年")
    end

    local yShift = - statusLabel.height - 11 - moreDis
    local picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
    for pic_i = 1, 6 do
        local picNode = node.transform:Find("pic" .. tostring(pic_i))
        if picNode ~= nil then
            picNode.gameObject:SetActive(false)
            CommonDefs.ListAdd(picNodeArray, typeof(GameObject), picNode.gameObject)
        end
    end

    local speNode = node.transform:Find("spepic").gameObject

    speNode:SetActive(false)

    if info.speimage ~= nil and not System.String.IsNullOrEmpty(info.speimage.url) then
        speNode:SetActive(true)
        CPersonalSpaceMgr.DownLoadPic(info.speimage.url, CommonDefs.GetComponent_GameObject_Type(speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
            if not this.systemPortrait then
                return
            end
            UIEventListener.Get(speNode).onClick = DelegateFactory.VoidDelegate(function (p)
                CWebBrowserMgr.Inst:OpenUrl(info.speimage.jump_url)
            end)
        end), false, true)
        speNode.transform.localPosition = Vector3(speNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, speNode.transform.localPosition.z)
        yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
    elseif info.videolist and info.videolist.Length > 0 then
        local videoNode = node.transform:Find('pic1').gameObject
        LuaPersonalSpaceMgrReal.SetVideoPlayPic(videoNode,100)
        local videoData = info.videolist[0]
        videoNode:SetActive(true)
        CPersonalSpaceMgr.DownLoadPic(videoData.snapshot, CommonDefs.GetComponent_GameObject_Type(videoNode, typeof(UITexture)), 160, 250, DelegateFactory.Action(function ()
            if not this.systemPortrait then
                return
            end
            UIEventListener.Get(videoNode).onClick = DelegateFactory.VoidDelegate(function (p)
              LuaPersonalSpaceMgrReal.MoviePlayUrl = videoData.url
              CUIManager.ShowUI(CLuaUIResources.DetailMovieShowWnd)
            end)
        end), true, true)
        videoNode.transform.localPosition = Vector3(videoNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, videoNode.transform.localPosition.z)
        yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
    else
        if info.imglist ~= nil and info.imglist.Length > 0 then
            do
                local i = 0
                while i < info.imglist.Length do
                    local imageInfo = info.imglist[i]
                    local picNode = picNodeArray[i]
                    picNode:SetActive(true)
                    this:SetMomentPic(picNode, info.imglist, i)
                    if i < 3 then
                        picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, picNode.transform.localPosition.z)
                    else
                        picNode.transform.localPosition = Vector3(picNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight, picNode.transform.localPosition.z)
                    end
                    i = i + 1
                end
            end
            if info.imglist.Length <= 3 then
                yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
            else
                yShift = yShift - CPersonalSpaceWnd.DefaultPicHeight
                yShift = yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight
            end
        end
    end


    local forwardNode = node.transform:Find("forwardinfo").gameObject
    if info.forwardmoment.id > 0 then
        forwardNode:SetActive(true)

        if System.String.IsNullOrEmpty(info.forwardmoment.rolename) then
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = tostring(info.forwardmoment.roleid)
        else
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = info.forwardmoment.rolename
        end

        local forwardVipNode = forwardNode.transform:Find("name/vipicon")
        if forwardVipNode ~= nil then
            CPersonalSpaceMgr.Inst:SetSpLevelNode(info.forwardmoment.splevel, forwardVipNode.gameObject)
        end

        if CClientMainPlayer.Inst:GetMyServerId() ~= info.forwardmoment.server_id and info.forwardmoment.server_id ~= 0 then
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("name"), typeof(UILabel)).text .. (CPersonalSpaceMgr.ServerSplitChar .. info.forwardmoment.server_name)
        end

        if info.forwardmoment.roleid == CIMMgr.PERSONAL_SPACE_HOT_POINT_ID then
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel)).text = ""
            CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CPersonalSpaceMgr.Inst:GetSpaceGMIcon(), false)
        elseif CPersonalSpaceMgr.CheckAndLoadGongZhongHaoPortraitPic(info.forwardmoment.roleid, forwardNode.transform:Find("icon").gameObject, CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))) then
        else
            --forwardNode.transform.FindChild("lv").GetComponent<UILabel>().text = info.forwardmoment.grade.ToString();
            local levelLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("lv"), typeof(UILabel))
            local extern
            if info.forwardmoment.xianfanstatus > 0 then
                extern = L10.Game.Constants.ColorOfFeiSheng
            else
                extern = NGUIText.EncodeColor24(levelLabel.color)
            end
            levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), extern, "lv." .. info.forwardmoment.grade)

            --if (!string.IsNullOrEmpty(info.forwardmoment.clazz) && !string.IsNullOrEmpty(info.forwardmoment.gender))
            --{
            --    uint cls = uint.Parse(info.forwardmoment.clazz);
            --    uint gender = uint.Parse(info.forwardmoment.gender);
            --    forwardNode.transform.FindChild("icon").GetComponent<CUITexture>().LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender));
            --}

            CPersonalSpaceMgr.LoadPortraitPic(info.forwardmoment.photo, info.forwardmoment.clazz, info.forwardmoment.gender, forwardNode.transform:Find("icon").gameObject, info.forwardmoment.expression_base)

            if info.forwardmoment.roleid ~= this.SelfRoleInfo.id then
                CPersonalSpaceMgr.Inst:addPlayerLinkBtn(forwardNode.transform:Find("icon/button").gameObject, info.forwardmoment.roleid, EnumReportId_lua.eMengDaoMoment, info.forwardmoment.text, info.forwardmoment.imglist, info.forwardmoment.id, 0)
            end
        end


        local showText = string.gsub(string.gsub(info.forwardmoment.text, "&lt;", "<"), "&gt;", ">")
        showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
        local forwardLabel = CommonDefs.GetComponent_Component_Type(forwardNode.transform:Find("status"), typeof(UILabel))
        CPersonalSpaceMgr.SetTextMore(showText, forwardLabel)
        UIEventListener.Get(forwardLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            local url = CommonDefs.GetComponent_GameObject_Type(statusLabel.gameObject, typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                return
            else
                this:ShowMomentDetailThroughFriendCircleById(info.forwardmoment.id, false, formerNode)
            end
        end)

        --GameObject forward_picNode1 = forwardNode.transform.FindChild("pic1").gameObject;
        --GameObject forward_picNode2 = forwardNode.transform.FindChild("pic2").gameObject;
        --GameObject forward_picNode3 = forwardNode.transform.FindChild("pic3").gameObject;
        local forward_speNode = forwardNode.transform:Find("spepic").gameObject

        --GameObject[] forward_picNodeArray = new GameObject[] { forward_picNode1, forward_picNode2, forward_picNode3 };
        --forward_picNode1.SetActive(false);
        --forward_picNode2.SetActive(false);
        --forward_picNode3.SetActive(false);

        local forward_picNodeArray = CreateFromClass(MakeGenericClass(List, GameObject))
        for pic_i = 1, 6 do
            local picNode = forwardNode.transform:Find("pic" .. tostring(pic_i))
            if picNode ~= nil then
                picNode.gameObject:SetActive(false)
                CommonDefs.ListAdd(forward_picNodeArray, typeof(GameObject), picNode.gameObject)
            end
        end

        forward_speNode:SetActive(false)

        local forward_bgBtn = forwardNode.transform:Find("bg").gameObject

        UIEventListener.Get(forward_bgBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            this:ShowMomentDetailThroughFriendCircleById(info.forwardmoment.id, false, formerNode)
        end)

        forwardNode.transform.localPosition = Vector3(forwardNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, forwardNode.transform.localPosition.z)

        if info.forwardmoment.speimage ~= nil and not System.String.IsNullOrEmpty(info.forwardmoment.speimage.url) then
          forward_speNode:SetActive(true)
          CPersonalSpaceMgr.DownLoadPic(info.forwardmoment.speimage.url, CommonDefs.GetComponent_GameObject_Type(forward_speNode, typeof(UITexture)), 160, 800, DelegateFactory.Action(function ()
            if not this.systemPortrait then
              return
            end
            UIEventListener.Get(forward_speNode).onClick = DelegateFactory.VoidDelegate(function (p)
              CWebBrowserMgr.Inst:OpenUrl(info.forwardmoment.speimage.jump_url)
            end)
          end), false, true)

          CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2
          yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
        elseif info.forwardmoment.videolist and info.forwardmoment.videolist.Length > 0 then
          local videoNode = forwardNode.transform:Find('pic1').gameObject
          LuaPersonalSpaceMgrReal.SetVideoPlayPic(videoNode,100)
          local videoData = info.forwardmoment.videolist[0]
          videoNode:SetActive(true)
          CPersonalSpaceMgr.DownLoadPic(videoData.snapshot, CommonDefs.GetComponent_GameObject_Type(videoNode, typeof(UITexture)), 160, 250, DelegateFactory.Action(function ()
            if not this.systemPortrait then
              return
            end
            UIEventListener.Get(videoNode).onClick = DelegateFactory.VoidDelegate(function (p)
              LuaPersonalSpaceMgrReal.MoviePlayUrl = videoData.url
              CUIManager.ShowUI(CLuaUIResources.DetailMovieShowWnd)
            end)
          end), true, true)
          --videoNode.transform.localPosition = Vector3(videoNode.transform.localPosition.x, CPersonalSpaceWnd.DefaultPicPosY + yShift, videoNode.transform.localPosition.z)
          CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2
          yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
        else
          if info.forwardmoment.imglist ~= nil and info.forwardmoment.imglist.Length > 0 then
            do
              local i = 0
              while i < info.forwardmoment.imglist.Length do
                local imageInfo = info.forwardmoment.imglist[i]
                local picNode = forward_picNodeArray[i]
                picNode:SetActive(true)
                this:SetMomentPic(picNode, info.forwardmoment.imglist, i)
                i = i + 1
              end
            end


            if info.forwardmoment.imglist.Length <= 3 then
              CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2
              yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
            else
              CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight2 + CPersonalSpaceWnd.DefaultTwoRowPicHeight
              yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight2
              yShift = yShift - CPersonalSpaceWnd.DefaultTwoRowPicHeight
            end
          else
            CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)).height = CPersonalSpaceMgr.ForwardInfoHeight1
            yShift = yShift - CPersonalSpaceMgr.ForwardInfoHeight1
          end
        end



        CommonDefs.GetComponent_GameObject_Type(forward_bgBtn, typeof(UISprite)):ResizeCollider()
    else
        forwardNode:SetActive(false)
    end

    subInfo.localPosition = Vector3(subInfo.localPosition.x, yShift, subInfo.localPosition.z)

    --//favor
    local favorText = ""
    favorIcon:SetActive(false)
    favorbuttonSign:SetActive(false)

    if info.zanlist ~= nil and info.zancount > 0 then
        if info.is_user_liked then
            favorIcon:SetActive(true)
        else
            favorIcon:SetActive(false)
        end

        favorNumLabel.text = tostring(info.zancount)

        do
            local zan_i = 0
            while zan_i < info.zanlist.Length do
                local zanInfo = info.zanlist[zan_i]
                if zan_i < CPersonalSpaceWnd.ShowZanListBarNum then
                    local name = zanInfo.rolename
                    if System.String.IsNullOrEmpty(name) then
                        name = tostring(zanInfo.roleid)
                    end
                    favorText = favorText .. CPersonalSpaceMgr.GeneratePlayerName(zanInfo.roleid, name)
                end
                if zanInfo.roleid == this.SelfRoleInfo.id then
                    if info.showFavorSign then
                        favorbuttonSign:SetActive(true)
                        info.showFavorSign = false
                        this:updateMomentCache(Table2ArrayWithCount({info}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem)), true, generateType, nil, false)
                    else
                        favorbuttonSign:SetActive(false)
                    end
                end
                zan_i = zan_i + 1
            end
        end
    else
        favorNumLabel.text = "0"
    end

    local totalHeight = 0

    --TODO
    if info.zanlist ~= nil and info.zanlist.Length > 0 then
        if generateType == CPersonalSpace_CircleGenerateType.AllHotMoments or generateType == CPersonalSpace_CircleGenerateType.HotMoments or generateType == CPersonalSpace_CircleGenerateType.NaNian then
            favorbarNode:SetActive(false)
        else
            favorbarNode:SetActive(true)
            favorbarLabel.text = CChatLinkMgr.TranslateToNGUIText(favorText, false)
            UIEventListener.Get(favorbarLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                local index = favorbarLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
                if index == 0 then
                    index = - 1
                end

                local url = favorbarLabel:GetUrlAtCharacterIndex(index)
                if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                    return
                else
                    this:ShowMomentDetailThroughFriendCircle(info, false, formerNode)
                end
            end)
            totalHeight = totalHeight + CPersonalSpaceWnd.DefaultTotalHeight
        end
    else
        favorbarNode:SetActive(false)
        --totalHeight = 0;
    end

    --//leavemessage
    if info.commentlist ~= nil and info.commentlist.Length > 0 then
        leaveMessageNumText.text = tostring(info.commentcount)
        if generateType == CPersonalSpace_CircleGenerateType.AllHotMoments or generateType == CPersonalSpace_CircleGenerateType.HotMoments or generateType == CPersonalSpace_CircleGenerateType.NaNian then
            node.transform:Find("subInfo/messagebar").gameObject:SetActive(false)
        else
            node.transform:Find("subInfo/messagebar").gameObject:SetActive(true)
            local nodeArray = Table2ArrayWithCount({node.transform:Find("subInfo/messagebar/1").gameObject, node.transform:Find("subInfo/messagebar/2").gameObject, node.transform:Find("subInfo/messagebar/3").gameObject}, 3, MakeArrayClass(GameObject))

            do
                local message_i = 0
                while message_i < CPersonalSpaceWnd.ShowMessageListBarNum do
                    if message_i < info.commentlist.Length and message_i < nodeArray.Length then
                        local comment_info = info.commentlist[message_i]
                        local messageNode = nodeArray[message_i]
                        messageNode:SetActive(true)

                        local showText = CPersonalSpaceMgr.GeneratePlayerName(comment_info.roleid, comment_info.rolename)
                        if comment_info.replyinfo.roleid ~= 0 then
                            showText = showText .. (CPersonalSpaceDetailMoment.ReplayMidfix .. CPersonalSpaceMgr.GeneratePlayerName(comment_info.replyinfo.roleid, comment_info.replyinfo.rolename))
                        end

                        showText = showText .. comment_info.text
                        showText = CChatMgr.Inst:FilterYangYangEmotion(showText)
                        local textNode = messageNode.transform:Find("text")
                        messageNode.transform.localPosition = Vector3(messageNode.transform.localPosition.x, - totalHeight, messageNode.transform.localPosition.z)
                        local textNodeLabel = CommonDefs.GetComponent_Component_Type(textNode, typeof(UILabel))
                        textNodeLabel.text = CChatLinkMgr.TranslateToNGUIText(showText, false)
                        totalHeight = totalHeight + (textNodeLabel.height + CPersonalSpaceWnd.ShowMessageSpace)

                        UIEventListener.Get(textNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
                            local index = textNodeLabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
                            if index == 0 then
                                index = - 1
                            end

                            local url = textNodeLabel:GetUrlAtCharacterIndex(index)
                            if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
                                CPersonalSpaceMgr.Inst:SetPlayerReportDefaultInfo(EnumReportId_lua.eMengDaoMoment, info.text, info.imglist, info.id)
                                return
                            else
                                this:ShowMomentDetailThroughFriendCircle(info, false, formerNode)
                            end
                        end)
                    elseif message_i < nodeArray.Length then
                        nodeArray[message_i]:SetActive(false)
                    end
                    message_i = message_i + 1
                end
            end
        end
    else
        leaveMessageNumText.text = "0"
        node.transform:Find("subInfo/messagebar").gameObject:SetActive(false)
    end

    local subInfoBgSprite = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/bg"), typeof(UISprite))
    subInfoBgSprite.height = totalHeight

    if totalHeight <= 0 then
        subInfoBgSprite.gameObject:SetActive(false)
    else
        subInfoBgSprite.gameObject:SetActive(true)
    end

    local bgBtnSprite = CommonDefs.GetComponent_GameObject_Type(bgBtn, typeof(UISprite))
    bgBtnSprite.height = CPersonalSpaceWnd.DefaultBgButtonHeight + totalHeight - yShift
    --bgBtnSprite.ResizeCollider();
    this:SetBoxCollider(bgBtnSprite)

    local selfbgBtnSprite = CommonDefs.GetComponent_GameObject_Type(selfBgBtn, typeof(UISprite))
    selfbgBtnSprite.height = CPersonalSpaceWnd.DefaultSelfBgButtonHeight + totalHeight - yShift
    --selfbgBtnSprite.ResizeCollider();
    this:SetBoxCollider(selfbgBtnSprite)

    local forwardBtn = node.transform:Find("subInfo/forwardbutton").gameObject
    local forwardNum = CommonDefs.GetComponent_Component_Type(node.transform:Find("subInfo/forwardbutton/num"), typeof(UILabel))
    forwardNum.text = tostring(info.forwardcount)
    UIEventListener.Get(forwardBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if info.forwardmoment.id > 0 and info.forwardmoment.status == - 1 then
            g_MessageMgr:ShowMessage("PersonalSpaceCannotForward")
        else
            CPersonalSpaceMgr.Inst.saveForwardInfo = info
            CUIManager.ShowUI(CUIResources.PersonalSpaceForwardWnd)
        end
    end)

    --favorbuttonSign.SetActive(false);
    UIEventListener.Get(node.transform:Find("subInfo/favorbutton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CPersonalSpaceMgr.Inst:LikeMoment(info.id, favorIcon.activeSelf, DelegateFactory.Action_CPersonalSpace_BaseRet(function (data)

            if not this.systemPortrait then
                return
            end
            if data.code == 0 then
                --UpdateZanListBySelf(!favorIcon.activeSelf, info);
                --ReposFriendCircleList(scrollView);

                CPersonalSpaceMgr.Inst:GetSingleMomentInfo(info.id, DelegateFactory.Action_CPersonalSpace_GetSingleMoment_Ret(function (ret)
                    if not this.systemPortrait then
                        return
                    end
                    if ret.code == 0 then
                        local newItem = ret.data
                        newItem.type = generateType
                        if not favorIcon.activeSelf and newItem.is_user_liked then
                            newItem.showFavorSign = true
                        else
                            newItem.showFavorSign = false
                        end

                        local updateList = Table2ArrayWithCount({newItem}, 1, MakeArrayClass(CPersonalSpace_GetMomentItem))

                        this:updateMomentCache(updateList, false, generateType, nil, false)
                        if generateType == CPersonalSpace_CircleGenerateType.HotMoments then
                            this:ReposFriendCircleList(CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.scrollView, nil, nil, 0)
                        elseif generateType == CPersonalSpace_CircleGenerateType.AllHotMoments then
                            this:ReposFriendCircleList(CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.scrollView, nil, nil, 0)
                        else
                            this:ReposFriendCircleList(scrollView, nil, nil, 0)
                        end
                    end
                end), nil)
            end
        end), DelegateFactory.Action(function ()
        end))
    end)

    return node
end
CPersonalSpaceWnd.m_ReposFriendCircleList_CS2LuaHook = function (this, _scrollView, _table, _panel, yDis)
    if _scrollView == nil then
        _scrollView = this.statusListScrollView
    end

    if _table == nil then
        _table = CommonDefs.GetComponentInChildren_Component_Type(_scrollView, typeof(UITable))
        if _table == nil then
            return
        end
    end

    if _panel == nil then
        _panel = CommonDefs.GetComponent_Component_Type(_scrollView, typeof(UIPanel))
        if _panel == nil then
            return
        end
    end

    local savePos = _scrollView.transform.localPosition
    local saveOffsetY = _panel.clipOffset.y

    _table:Reposition()
    _scrollView:ResetPosition()

    _scrollView.transform.localPosition = Vector3(savePos.x,savePos.y - yDis,savePos.z)
    _panel.clipOffset = Vector2(_panel.clipOffset.x, saveOffsetY + yDis)
    local mPanel = _scrollView:GetComponent(typeof(UIPanel))
    local clip = mPanel.finalClipRegion
    local b = _scrollView.bounds
    local hy = clip.w * 0.5
    local dis = clip.y + hy
    if dis > 0 then
      _table:Reposition()
      _scrollView:ResetPosition()
    end
end

CPersonalSpaceWnd.m_CheckYearDivide_CS2LuaHook = function (this, data, table)
    --yearDivideNode
    local infoTime = math.floor(data.createtime / 1000)
    local infoTimeDate = CServerTimeMgr.ConvertTimeStampToZone8Time(infoTime)
    local year = infoTimeDate.Year
    if this.saveYear == 0 then
        this.saveYear = year
        return
    end
    if this.saveYear ~= year then
        local divideNode = NGUITools.AddChild(table.gameObject, this.yearDivideNode)
        divideNode:SetActive(true)
        divideNode.transform:Find("nanianMark").gameObject:SetActive(false)
        divideNode.transform:Find("nanianLabel").gameObject:SetActive(false)
        CommonDefs.GetComponent_Component_Type(divideNode.transform:Find("label"), typeof(UILabel)).text = tostring(year)
        this.saveYear = year
        this.yearDivideAdd = true
    end
end

CPersonalSpaceWnd.m_CheckNaNianYearDivide_CS2LuaHook = function (this, data, table)
    --yearDivideNode
    local infoTime = math.floor(data.createtime / 1000)
    local infoTimeDate = CServerTimeMgr.ConvertTimeStampToZone8Time(infoTime)
    local year = infoTimeDate.Year
    if this.saveYear == 0 or this.saveYear ~= year then
        local divideNode = NGUITools.AddChild(table.gameObject, this.yearDivideNode)
        divideNode:SetActive(true)
        divideNode.transform:Find("label").gameObject:SetActive(false)
        divideNode.transform:Find("label1").gameObject:SetActive(false)
        divideNode.transform:Find("nanianMark").gameObject:SetActive(true)
        local nanianLabel = divideNode.transform:Find("nanianLabel"):GetComponent(typeof(UILabel))
        local now = CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst.timeStamp)
        nanianLabel.gameObject:SetActive(true)
        nanianLabel.text = tostring(now.Year - year) .. LocalString.GetString("年前")
        this.saveYear = year
        this.yearDivideAdd = true
    end
end

CPersonalSpaceWnd.m_InitMomentCircleList_CS2LuaHook = function (this, _scrollView, _panel, _table, _generateType, momentCacheList, emptyNode, savePosSign, reCalHeight)

    this.saveYear = 0
    this.yearDivideAdd = false

    local savePos = _scrollView.transform.localPosition
    local saveOffsetY = _panel.clipOffset.y
    if reCalHeight and reCalHeight > 0 then
      local offset = 25
      savePos = Vector3(_scrollView.transform.localPosition.x,_scrollView.transform.localPosition.y-reCalHeight-offset,_scrollView.transform.localPosition.z)
      saveOffsetY = saveOffsetY + reCalHeight + offset
    end

    Extensions.RemoveAllChildren(_table.transform)

    -- 那年今日banner
    if this.hasNaNianMoment and _generateType == CPersonalSpace_CircleGenerateType.SelfCircle then
        local g = NGUITools.AddChild(_table.gameObject, this.bannerTemplate)
        g:SetActive(true)
        UIEventListener.Get(g.transform:Find("BannerBg").gameObject).onClick = DelegateFactory.VoidDelegate(
            function()
                this:InitNaNianMoments()
            end
        )
    end

    if _generateType == CPersonalSpace_CircleGenerateType.NaNian then
        local topBar = this.selfOpArea.transform:Find("TopBar").gameObject
        topBar.transform:Find("AttentionButton").gameObject:SetActive(false)
        topBar.transform:Find("HistroyButton").gameObject:SetActive(false)

        local backBtn = topBar.transform:Find("BackSelfCircleButton").gameObject
        backBtn:SetActive(true)
        topBar.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("那年今日")
        UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(
            function()
                this.IsDeleteSelfMomentMode = false
                this:InitSelfCircle()
            end
        )
    end

    if momentCacheList.Count <= 0 then
        emptyNode:SetActive(true)
        return
    end

    local count = 0
    do
        local i = 0
        while i < momentCacheList.Count do
            
            local continue
            repeat
                local node = momentCacheList[i].node
                if node ~= nil and _table.gameObject ~= nil then
                    
                    if _generateType == CPersonalSpace_CircleGenerateType.SelfCircle or _generateType == CPersonalSpace_CircleGenerateType.NaNian then
                        if momentCacheList[i].data.roleid ~= this.SelfRoleInfo.id then
                            continue = true
                            break
                        end
                    end

                    if _generateType == CPersonalSpace_CircleGenerateType.SelfCircle or _generateType == CPersonalSpace_CircleGenerateType.OtherSelfCircle then
                        this:CheckYearDivide(momentCacheList[i].data, _table)
                    elseif _generateType == CPersonalSpace_CircleGenerateType.NaNian then
                        this:CheckNaNianYearDivide(momentCacheList[i].data, _table)
                    end

                    node:SetActive(false)
                    local t = node.transform
                    t.parent = _table.gameObject.transform
                    node:SetActive(true)
                    t.localPosition = Vector3.zero
                    t.localRotation = Quaternion.identity
                    t.localScale = Vector3.one
                    node.layer = _table.gameObject.layer

                    if count == 0 or this.yearDivideAdd then
                        if this.yearDivideAdd then
                            this.yearDivideAdd = false
                        end
                        CommonDefs.GetComponent_GameObject_Type(node.transform:Find("icon/divide1").gameObject, typeof(UISprite)).alpha = 0
                        CommonDefs.GetComponent_GameObject_Type(node.transform:Find("time/divide2").gameObject, typeof(UISprite)).alpha = 0
                    else
                        CommonDefs.GetComponent_GameObject_Type(node.transform:Find("icon/divide1").gameObject, typeof(UISprite)).alpha = 1
                        CommonDefs.GetComponent_GameObject_Type(node.transform:Find("time/divide2").gameObject, typeof(UISprite)).alpha = 1
                    end

                    count = count + 1

                    if _generateType == CPersonalSpace_CircleGenerateType.FriendCircle then
                        node.transform:Find("icon").gameObject:SetActive(true)
                        node.transform:Find("icon/hotsign").gameObject:SetActive(false)
                        node.transform:Find("name").gameObject:SetActive(true)
                        node.transform:Find("lv").gameObject:SetActive(true)
                        node.transform:Find("subInfo/time").gameObject:SetActive(true)
                        node.transform:Find("time").gameObject:SetActive(false)
                        node.transform:Find("bgbutton").gameObject:SetActive(true)
                        --node.transform.FindChild("bgbutton").GetComponent<UISprite>().ResizeCollider();
                        node.transform:Find("selfbgbutton").gameObject:SetActive(false)
                    elseif _generateType == CPersonalSpace_CircleGenerateType.SelfCircle or _generateType == CPersonalSpace_CircleGenerateType.OtherSelfCircle then
                        node.transform:Find("icon").gameObject:SetActive(false)
                        node.transform:Find("name").gameObject:SetActive(false)
                        node.transform:Find("lv").gameObject:SetActive(false)
                        node.transform:Find("subInfo/time").gameObject:SetActive(false)
                        node.transform:Find("time").gameObject:SetActive(true)
                        node.transform:Find("time/yeartime").gameObject:SetActive(false)
                        node.transform:Find("bgbutton").gameObject:SetActive(false)
                        node.transform:Find("selfbgbutton").gameObject:SetActive(true)
                        --node.transform.FindChild("selfbgbutton").GetComponent<UISprite>().ResizeCollider();
                    elseif _generateType == CPersonalSpace_CircleGenerateType.HotMoments or _generateType == CPersonalSpace_CircleGenerateType.AllHotMoments then
                        node.transform:Find("icon").gameObject:SetActive(true)
                        node.transform:Find("icon/hotsign").gameObject:SetActive(true)
                        node.transform:Find("name").gameObject:SetActive(true)
                        node.transform:Find("lv").gameObject:SetActive(true)
                        node.transform:Find("subInfo/time").gameObject:SetActive(true)
                        node.transform:Find("time").gameObject:SetActive(false)
                        node.transform:Find("time/yeartime").gameObject:SetActive(true)
                        node.transform:Find("bgbutton").gameObject:SetActive(true)
                        --node.transform.FindChild("bgbutton").GetComponent<UISprite>().ResizeCollider();
                        node.transform:Find("selfbgbutton").gameObject:SetActive(false)
                        node.transform:Find("subInfo/deletebutton").gameObject:SetActive(false)
                    elseif _generateType == CPersonalSpace_CircleGenerateType.NaNian then
                        node.transform:Find("icon").gameObject:SetActive(false)
                        node.transform:Find("name").gameObject:SetActive(false)
                        node.transform:Find("lv").gameObject:SetActive(false)
                        node.transform:Find("subInfo/time").gameObject:SetActive(false)
                        node.transform:Find("subInfo/bg").gameObject:SetActive(false)
                        node.transform:Find("subInfo/favorbar").gameObject:SetActive(false)
                        node.transform:Find("time").gameObject:SetActive(true)
                        node.transform:Find("time/yeartime").gameObject:SetActive(true)
                        node.transform:Find("bgbutton").gameObject:SetActive(false)
                        node.transform:Find("selfbgbutton").gameObject:SetActive(true)
                        node.transform:Find("subInfo/deletebutton").gameObject:SetActive(false)
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end

    if _table.transform.childCount <= 0 then
        emptyNode:SetActive(true)
    else
        emptyNode:SetActive(false)
    end

    _table:Reposition()
    _scrollView:ResetPosition()

    if savePosSign then
        _scrollView.transform.localPosition = savePos
        _panel.clipOffset = Vector2(_panel.clipOffset.x, saveOffsetY)
    end
end
CPersonalSpaceWnd.m_deleteMomentCache_CS2LuaHook = function (this, momentId, genType)
    local dic = nil

    if genType == CPersonalSpace_CircleGenerateType.HotMoments then
        dic = CPersonalSpaceMgr.Inst.hotMomentDic
    elseif genType == CPersonalSpace_CircleGenerateType.AllHotMoments then
        dic = CPersonalSpaceMgr.Inst.allhotMomentDic
    else
        dic = CPersonalSpaceMgr.Inst.momentCacheDic
    end

    if not CommonDefs.DictContains(dic, typeof(UInt64), momentId) then
        return
    else
        CommonDefs.DictRemove(dic, typeof(UInt64), momentId)
    end

    local newList = CreateFromClass(MakeGenericClass(List, CPersonalSpace_MomentCacheData))
    CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, ___value)
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        local info = kv.Value
        info.node.transform.parent = nil
        --info.node.SetActive(false);
        if newList.Count == 0 then
            CommonDefs.ListAdd(newList, typeof(CPersonalSpace_MomentCacheData), info)
        else
            local insertSign = false
            do
                local i = 0
                while i < newList.Count do
                    if info.data.id > newList[i].data.id  then
                        CommonDefs.ListInsert(newList, i, typeof(CPersonalSpace_MomentCacheData), info)
                        insertSign = true
                        break
                    end
                    i = i + 1
                end
            end

            if not insertSign then
                CommonDefs.ListAdd(newList, typeof(CPersonalSpace_MomentCacheData), info)
            end
        end
    end))

    CPersonalSpaceMgr.Inst.momentCacheList = newList
    if genType == CPersonalSpace_CircleGenerateType.HotMoments then
        CPersonalSpaceMgr.Inst.hotMomentList = newList
        this:deleteMomentCache(momentId, CPersonalSpace_CircleGenerateType.SelfCircle)
    elseif genType == CPersonalSpace_CircleGenerateType.AllHotMoments then
        CPersonalSpaceMgr.Inst.allhotMomentList = newList
        this:deleteMomentCache(momentId, CPersonalSpace_CircleGenerateType.SelfCircle)
    else
        CPersonalSpaceMgr.Inst.momentCacheList = newList
    end
end
CPersonalSpaceWnd.m_updateMomentCache_CS2LuaHook = function (this, newlist, noReset, generateType, formerNode, noAdd)
    if newlist.Length <= 0 then
        return
    end

    local momentCacheDic
    local momentCacheList
    local scrollView = nil

    if generateType == CPersonalSpace_CircleGenerateType.HotMoments then
        momentCacheDic = CPersonalSpaceMgr.Inst.hotMomentDic
        momentCacheList = CPersonalSpaceMgr.Inst.hotMomentList
        scrollView = CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.scrollView
        formerNode = CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.formerNode
    elseif generateType == CPersonalSpace_CircleGenerateType.AllHotMoments then
        momentCacheDic = CPersonalSpaceMgr.Inst.allhotMomentDic
        momentCacheList = CPersonalSpaceMgr.Inst.allhotMomentList
        scrollView = CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.scrollView
        formerNode = CPersonalSpaceMgr.Inst.hotMomentSaveNodeInfo.formerNode
    elseif generateType == CPersonalSpace_CircleGenerateType.Other then
        local inmoment = true
        do
            local i = 0
            while i < newlist.Length do
                local cdata = CreateFromClass(CPersonalSpace_MomentCacheData)
                cdata.data = newlist[i]
                CommonDefs.DictSet(CPersonalSpaceMgr.Inst.otherMomentCacheDic, typeof(UInt64), cdata.data.id, typeof(CPersonalSpace_MomentCacheData), cdata)

                if not CommonDefs.DictContains(CPersonalSpaceMgr.Inst.momentCacheDic, typeof(UInt64), cdata.data.id) then
                    inmoment = false
                end
                i = i + 1
            end
        end

        if inmoment and newlist.Length > 0 then
            momentCacheDic = CPersonalSpaceMgr.Inst.momentCacheDic
            momentCacheList = CPersonalSpaceMgr.Inst.momentCacheList
            scrollView = this.statusListScrollView
        else
            return
        end
    else
        momentCacheDic = CPersonalSpaceMgr.Inst.momentCacheDic
        momentCacheList = CPersonalSpaceMgr.Inst.momentCacheList
        scrollView = this.statusListScrollView
    end

    local needReList = false

    do
        local i = 0
        while i < newlist.Length do
            local continue
            repeat
                local cacheData = CreateFromClass(CPersonalSpace_MomentCacheData)
                cacheData.data = newlist[i]

                if noAdd then
                    if not CommonDefs.DictContains(momentCacheDic, typeof(UInt64), cacheData.data.id) then
                        continue = true
                        break
                    end
                end

                --if (!CPersonalSpaceMgr.Inst.momentCacheDic.ContainsKey(cacheData.data.id))
                if not CommonDefs.DictContains(momentCacheDic, typeof(UInt64), cacheData.data.id) then
                    needReList = true

                    if not noReset then
                        local node = NGUITools.AddChild(this.poolNode, this.statusTemplateBar)

                        node:SetActive(true)
                        cacheData.node = this:GenerateFriendCircleListNodeFunction(node, cacheData.data, scrollView, true, formerNode, generateType)
                        node:SetActive(false)
                    end
                    CommonDefs.DictSet(momentCacheDic, typeof(UInt64), cacheData.data.id, typeof(CPersonalSpace_MomentCacheData), cacheData)
                else
                    local node = CommonDefs.DictGetValue(momentCacheDic, typeof(UInt64), cacheData.data.id).node
                    if node ~= nil then
                        if not noReset then
                            local activeSign = false
                            if node.activeSelf then
                                activeSign = true
                            end
                            if not activeSign then
                                node:SetActive(true)
                            end
                            cacheData.node = this:GenerateFriendCircleListNodeFunction(node, cacheData.data, scrollView, false, formerNode, generateType)
                            if not activeSign then
                                node:SetActive(false)
                            end
                        end

                        CommonDefs.DictSet(momentCacheDic, typeof(UInt64), cacheData.data.id, typeof(CPersonalSpace_MomentCacheData), cacheData)
                    else
                        needReList = true

                        if not noReset then
                            node = NGUITools.AddChild(this.poolNode, this.statusTemplateBar)

                            node:SetActive(true)
                            cacheData.node = this:GenerateFriendCircleListNodeFunction(node, cacheData.data, scrollView, true, formerNode, generateType)
                            node:SetActive(false)
                        end

                        CommonDefs.DictSet(momentCacheDic, typeof(UInt64), cacheData.data.id, typeof(CPersonalSpace_MomentCacheData), cacheData)
                    end
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end

    if needReList then
        CPersonalSpaceMgr.Inst:updateMomentCacheList(momentCacheDic, momentCacheList, this.poolNode)
    end
end
CPersonalSpaceWnd.m_UpdateFriendCircleScrollView_CS2LuaHook = function (this, data, genType, savePos, reCalHeight)
    if data.data.list == nil or (data.data.list ~= nil and data.data.list.Length < CPersonalSpaceWnd.MaxEveryGetNum) then
        this.statusListScrollViewIndicator.enableShowNode = false
    end

    if data.code == 0 then
        if data.data.list ~= nil and data.data.list.Length > 0 then
            this:updateMomentCache(data.data.list, false, genType, nil, false)
        elseif reCalHeight > 0 and data.data.list ~= nil and data.data.list.Length == 0 then
--          if this.statusListTable then
--            Extensions.RemoveAllChildren(this.statusListTable.transform)
--          end
--          if this.sendStatusPanel_emptyNode then
--            this.sendStatusPanel_emptyNode:SetActive(true)
--          end
          return
        end
    end

    this.SaveCircleGenerateType = genType
    if genType == CPersonalSpace_CircleGenerateType.HotMoments then
    elseif genType == CPersonalSpace_CircleGenerateType.AllHotMoments then
    else
        this:InitMomentCircleList(this.statusListScrollView, this.statusListPanel, this.statusListTable, this.SaveCircleGenerateType, CPersonalSpaceMgr.Inst.momentCacheList, this.sendStatusPanel_emptyNode, savePos, reCalHeight)
    end
    if this.statusListScrollViewIndicator then
      this.statusListScrollViewIndicator:EnableSpring()
    end
end
CPersonalSpaceWnd.m_UpdatePageInfo_CS2LuaHook = function (this, data, index)
    if data.data.list ~= nil and data.data.list.Length <= 0 and CPersonalSpaceMgr.Inst.NowShowInputButton ~= nil then
        local max = index + 1
        CPersonalSpaceMgr.Inst.NowShowInputButton:SetMinMax(1, max, 1)
    else
        CPersonalSpaceMgr.Inst.NowShowInputButton:SetMinMax(1, 99, 1)
    end
end
CPersonalSpaceWnd.m_ResetSubPanels_CS2LuaHook = function (this)
    this.personalSpaceDetailMoment.gameObject:SetActive(false)
    this.momentHisSubPanels:SetActive(false)
    this.flowerDetailPanel:SetActive(false)
    this.renqiDetailPanel:SetActive(false)
    this.attentionDetialPanel:SetActive(false)
    this.transform:Find('Anchor/Nodes/WishPanel').gameObject:SetActive(false)
    this.transform:Find('Anchor/Nodes/FriendsCircleSubPanels/DetailWish').gameObject:SetActive(false)
end
CPersonalSpaceWnd.m_InitLeaveMessagePanel_CS2LuaHook = function (this)
    this.leaveMessagePanelNode.transform:Find('Info/ScrollViewIndicator').gameObject:SetActive(true)
    if this.PersonalID > 0 then
        CommonDefs.GetComponent_GameObject_Type(this.leaveMessagePanelNode, typeof(CPersonalSpaceLeaveMessageView)):Init(this.PersonalID, this.PersonalName, this.SelfRoleInfo.id == this.PersonalID)
    end
end
CPersonalSpaceWnd.m_InitFriendCirlePanel_CS2LuaHook = function (this)

    if this.PersonalID > 0 then
        if this.PersonalID == this.SelfRoleInfo.id then
            this:InitFriendCircle()
            this:CheckNewsNum()
        else
            this:InitOtherSelfCircle(this.PersonalID)
        end
    end
end
CPersonalSpaceWnd.m_InitHotMomentsPanel_CS2LuaHook = function (this)
    CommonDefs.GetComponent_GameObject_Type(this.hotMomentsPanel, typeof(CPersonalSpaceHotMomentsView)):Init()
end
CPersonalSpaceWnd.m_OnTabChange_CS2LuaHook = function (this, go, index)
  this:ResetSubPanels()
  if index == 0 then
    this.facebookShareNode:SetActive(false)
    this.normalNode:SetActive(true)

    this.friendCircleNode:SetActive(true)
    this.leaveMessagePanelNode:SetActive(false)
    this.hotMomentsPanel:SetActive(false)
    this:InitFriendCirlePanel()
  elseif index == 1 then
    this.friendCircleNode:SetActive(false)
    this.leaveMessagePanelNode:SetActive(false)
    this.hotMomentsPanel:SetActive(false)
    PersonalSpaceLua:ShowWishPanel(this)
  elseif index == 2 then
    this.facebookShareNode:SetActive(false)
    this.normalNode:SetActive(true)

    local leaveMessageTabNode = go
    if leaveMessageTabNode ~= nil then
      local redPoint = leaveMessageTabNode.transform:Find("RedPoint")
      if redPoint ~= nil then
        redPoint.gameObject:SetActive(false)
      end
    end

    this.friendCircleNode:SetActive(false)
    this.leaveMessagePanelNode:SetActive(true)
    this.hotMomentsPanel:SetActive(false)
    this:InitLeaveMessagePanel()
  elseif index == 3 then
    this.facebookShareNode:SetActive(false)
    this.normalNode:SetActive(true)

    this.friendCircleNode:SetActive(false)
    this.leaveMessagePanelNode:SetActive(false)
    this.hotMomentsPanel:SetActive(true)
    this:InitHotMomentsPanel()
  end

  --#region Guide
  EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {CUIResources.PersonalSpaceWnd.Name})
  --#endregion
end
CPersonalSpaceWnd.m_InitSetGiftPanel_CS2LuaHook = function (this)
    this.setGiftPanel:SetActive(true)
    local itemValue = GameplayItem_Setting.GetData().PersonalSpace_PresentPrice


    local costLabel = CommonDefs.GetComponent_Component_Type(this.setGiftPanel.transform:Find("BuyArea/QnCostAndOwnMoney/Cost"), typeof(UILabel))
    local ownLabel = CommonDefs.GetComponent_Component_Type(this.setGiftPanel.transform:Find("BuyArea/QnCostAndOwnMoney/Own"), typeof(UILabel))

    local qnAddButton = CommonDefs.GetComponent_Component_Type(this.setGiftPanel.transform:Find("BuyArea/Label/QnIncreseAndDecreaseButton"), typeof(QnAddSubAndInputButton))

    qnAddButton.onValueChanged = DelegateFactory.Action_uint(function (value)
        costLabel.text = tostring((value * itemValue))
    end)
    qnAddButton:SetValue(1, true)

    UIEventListener.Get(this.setGiftPanel.transform:Find("CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        this.setGiftPanel:SetActive(false)
    end)
    UIEventListener.Get(this.setGiftPanel.transform:Find("CancelButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        this.setGiftPanel:SetActive(false)
    end)
    UIEventListener.Get(this.setGiftPanel.transform:Find("SubmitButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local need = System.UInt64.Parse(costLabel.text)
        local total = System.UInt64.Parse(ownLabel.text)
        if need > total then
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        else
            local num = qnAddButton:GetValue()
            if num > 0 then
                this.setGiftPanel:SetActive(false)
                CPersonalSpaceMgr.Inst:AddPresent(num)
            else
                g_MessageMgr:ShowMessage("ENTER_NUMBER_TIP")
            end
        end
    end)
end
CPersonalSpaceWnd.m_AutoGetPositon_CS2LuaHook = function (this, go)
    if CPersonalSpaceMgr.AutoGetPositionSign then
        local statusTextInput = CommonDefs.GetComponent_Component_Type(this.setLocationPanel.transform:Find("StatusText"), typeof(UIInput))
        local statusTextLabel = CommonDefs.GetComponent_Component_Type(this.setLocationPanel.transform:Find("StatusText/Label"), typeof(UILabel))
        CPersonalSpaceMgr.Inst:GetLocation("", DelegateFactory.Action_CPersonalSpace_GetLocation_Ret(function (data)
            if not this.systemPortrait then
                return
            end

            if data.code == 0 then
                local location = data.data.province .. data.data.city
                statusTextInput.value = location
                statusTextLabel.text = location
            end
        end), nil)
    else
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
    end
end
CPersonalSpaceWnd.m_SubmitPositon_CS2LuaHook = function (this, go)
    local statusTextInput = CommonDefs.GetComponent_Component_Type(this.setLocationPanel.transform:Find("StatusText"), typeof(UIInput))
    local inputValue = statusTextInput.value
    inputValue = string.gsub(inputValue, "\n", "")
    inputValue = string.gsub(inputValue, "#r", "")
    this.setLocationPanel:SetActive(false)
    if not System.String.IsNullOrEmpty(inputValue) then
        CPersonalSpaceMgr.Inst:ChangeLocation(inputValue, DelegateFactory.Action_string_CPersonalSpace_BaseRet(function (locationAfterFilter, ret)
            CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Location/text"), typeof(UILabel)).text = locationAfterFilter
        end), nil)
    end
end
CPersonalSpaceWnd.m_InitSetLocationPanel_CS2LuaHook = function (this)
    if CSpeechCtrlMgr.Inst:CheckIfNeedSpeechCtrl(CSpeechCtrlType.Type_PersonalSpace_Submit_Location, true) then
        this.setLocationPanel:SetActive(false)
        return
    end
    this.setLocationPanel:SetActive(true)
    local formerLocation = CommonDefs.GetComponent_Component_Type(this.personalInfoNode.transform:Find("Location/text"), typeof(UILabel)).text
    local autoGetBtn = this.setLocationPanel.transform:Find("AutoGetButton").gameObject
    local submitBtn = this.setLocationPanel.transform:Find("SubmitButton").gameObject
    
    if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_KR_CLIENT or CommonDefs.IS_VN_CLIENT then
        --关闭港澳台的梦岛自动获取位置功能
        autoGetBtn:SetActive(false)
        UIEventListener.Get(submitBtn).onClick = MakeDelegateFromCSFunction(this.SubmitPositon, VoidDelegate, this)
        submitBtn.transform.localPosition = Vector3(0,-86,0)
    else 
        UIEventListener.Get(autoGetBtn).onClick = MakeDelegateFromCSFunction(this.AutoGetPositon, VoidDelegate, this)
        UIEventListener.Get(submitBtn).onClick = MakeDelegateFromCSFunction(this.SubmitPositon, VoidDelegate, this)
    end
end
CPersonalSpaceWnd.m_AddMomentBack_CS2LuaHook = function (this, textAfterFilter, data)
    if not this.systemPortrait then
        return
    end

    if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.Id ~= this.PersonalID then
        return
    end

    if not this.statusListScrollView.transform.parent.parent.gameObject.activeSelf then
        return
    end
    this:UpdateListInfo(0)
    if CPersonalSpaceMgr.Inst.NowShowInputButton ~= nil then
        CPersonalSpaceMgr.Inst.NowShowInputButton:SetValue(1, true)
    end
    --if (data.code == 0)
    --{
    --    CPersonalSpaceMgr.Inst.GetSingleMomentInfo(ulong.Parse(data.data.id), delegate(CPersonalSpace_GetSingleMoment_Ret ret)
    --    {
    --        if (!this)
    --            return;
    --        if (ret.code == 0)
    --        {
    --            CPersonalSpace_GetMomentItem newItem = ret.data;

    --            CPersonalSpace_GetMomentItem[] updateList = { newItem };
    --            updateMomentCache(updateList, false, SaveCircleGenerateType);
    --            InitMomentCircleList(statusListScrollView, statusListPanel, statusListTable, SaveCircleGenerateType, CPersonalSpaceMgr.Inst.momentCacheList, sendStatusPanel_emptyNode);
    --        }
    --    });
    --}
end
CPersonalSpaceWnd.m_SetPrivacySetting_CS2LuaHook = function (this, data)
    local option1Sign = this.settingPanel.transform:Find("node/Grid/Option1/click/node").gameObject
    local option2Sign = this.settingPanel.transform:Find("node/Grid/Option2/click/node").gameObject
    local option3Sign = this.settingPanel.transform:Find("node/Grid/Option3/click/node").gameObject
    local option4Sigh = this.settingPanel.transform:Find("node/Grid/Option4/click/node").gameObject
    option1Sign:SetActive(data.location)
    option2Sign:SetActive(data.space)
    option3Sign:SetActive(data.msg_for_following)
    option4Sigh:SetActive(data.hide_lbs)
end
CPersonalSpaceWnd.m_SetOptionNode_CS2LuaHook = function (this, node)
    if node.activeSelf then
        node:SetActive(false)
    else
        node:SetActive(true)
    end

    this:SendPrivacySetting()
end

CPersonalSpaceWnd.m_InitDetailPicPanel_CS2LuaHook = function (this, infoArray, index, texture)
    this.detailPicPanel.gameObject:SetActive(true)
    local lua = this.detailPicPanel.transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    print(lua)
    lua:InitContent(infoArray, index, texture, this)
end

CPersonalSpaceWnd.m_SetUsedNameHide_CS2LuaHook = function (this, node)
    if CPersonalSpaceMgr.EnableHideUsedName then
        if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.PlayProp == nil then
            return
        end

        if CClientMainPlayer.Inst.PlayProp.NamesUsed.Count <= 0 then
            g_MessageMgr:ShowMessage("NO_PREVIOUS_NAME")
            return
        end

        if node.activeSelf then
            --node.SetActive(false);

            MessageWndManager.ShowDelayOKCancelMessage(g_MessageMgr:FormatMessage("Cengyongming_Quxiaoyincang"), DelegateFactory.Action(function ()
                Gac2Gas.SetHidePreviousName(false)
                --EventManager.Broadcast(EnumEventType.PersonalSpaceSetUsedNameHide, false); //for test
            end), nil, 3)
        else
            --node.SetActive(true);

            QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YuanBao, g_MessageMgr:FormatMessage("Cengyongming_Yincang"), GameplayItem_Setting.GetData().PersonalSpace_HidePreviousNamePrice, DelegateFactory.Action(function ()
                Gac2Gas.SetHidePreviousName(true)
                --EventManager.Broadcast(EnumEventType.PersonalSpaceSetUsedNameHide, true); //for test
            end), nil, false, true, EnumPlayScoreKey.NONE, false)
        end
    else
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
    end
end
CPersonalSpaceWnd.m_InitSettingPanel_CS2LuaHook = function (this)
    this.settingPanel:SetActive(true)

    local backBtn = this.settingPanel.transform:Find("darkbg").gameObject
    UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this.settingPanel:SetActive(false)
        CUIManager.CloseUI(CLuaUIResources.ChoosePersonalSpaceNewBgWnd)
    end)

    local option1Btn = this.settingPanel.transform:Find("node/Grid/Option1/click").gameObject
    local option1Sign = this.settingPanel.transform:Find("node/Grid/Option1/click/node").gameObject
    UIEventListener.Get(option1Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:SetOptionNode(option1Sign)
    end)

    local option2Btn = this.settingPanel.transform:Find("node/Grid/Option2/click").gameObject
    local option2Sign = this.settingPanel.transform:Find("node/Grid/Option2/click/node").gameObject
    UIEventListener.Get(option2Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:SetOptionNode(option2Sign)
    end)

    local option3Btn = this.settingPanel.transform:Find("node/Grid/Option3/click").gameObject
    local option3Sign = this.settingPanel.transform:Find("node/Grid/Option3/click/node").gameObject
    UIEventListener.Get(option3Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:SetOptionNode(option3Sign)
    end)

    local option4Btn = this.settingPanel.transform:Find("node/Grid/Option4/click").gameObject
    local option4Sign = this.settingPanel.transform:Find("node/Grid/Option4/click/node").gameObject
    UIEventListener.Get(option4Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:SetOptionNode(option4Sign)
    end)

    local option5Btn = this.settingPanel.transform:Find("node/Grid/Option5/click").gameObject
    local option5Sign = this.settingPanel.transform:Find("node/Grid/Option5/click/node").gameObject
    UIEventListener.Get(option5Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:SetUsedNameHide(option5Sign)
    end)
    
    local option6Btn = this.settingPanel.transform:Find("node/Grid/Option6/click").gameObject
    local option6Sign = this.settingPanel.transform:Find("node/Grid/Option6/click/node").gameObject
    UIEventListener.Get(option6Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        PersonalSpaceLua:OnChangeBgBtnClicked(option6Sign)
    end)
    
    this.settingPanel.transform:Find("node/Grid/Option6").gameObject:SetActive(CLuaPersonalSpaceMgr.m_IsOpenNewBg)
    local option6Sign = this.settingPanel.transform:Find("node/Grid/Option6/click/node").gameObject
    if option6Sign.activeSelf then
        CLuaPersonalSpaceMgr:QueryPersonalSpaceBackGroundList()
    end

    this.settingPanel.transform:Find("node/Grid/Option1").gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    this.settingPanel.transform:Find("node/Grid/Option4").gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    local height = CLuaPersonalSpaceMgr.m_IsOpenNewBg and 570 or 487
    if CommonDefs.IS_VN_CLIENT then
        height = height - 166
    end
    
    this.settingPanel.transform:Find("node"):GetComponent(typeof(UISprite)).height = height
end
CPersonalSpaceWnd.m_ShowHisInfo_CS2LuaHook = function (this)
    this.momentHisSubPanels:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(this.momentHisSubPanels, typeof(CPersonalSpaceMomentHisView)):Init(this.friendCircleNode, false)
end
CPersonalSpaceWnd.m_ShowNewInfo_CS2LuaHook = function (this, infoNum)
    if not this then
      return
    end
    if not this.selfOpArea then
      return
    end
    if infoNum > 0 then
        local infoNumBar = this.selfOpArea.transform:Find("InfoNumBar").gameObject
        infoNumBar:SetActive(true)
        local clickBtn = infoNumBar.transform:Find("ClickButton").gameObject
        local numLabel = CommonDefs.GetComponent_Component_Type(clickBtn.transform:Find("Label"), typeof(UILabel))
        numLabel.text = System.String.Format(CPersonalSpaceWnd.NewInfoText, infoNum)

        UIEventListener.Get(clickBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            this.momentHisSubPanels:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(this.momentHisSubPanels, typeof(CPersonalSpaceMomentHisView)):Init(this.friendCircleNode, true)
        end)

        local scrollBg = CommonDefs.GetComponent_Component_Type(this.friendCircleNode.transform:Find("bg"), typeof(UISprite))
        scrollBg.height = CPersonalSpaceWnd.FriendScrollHeight2
        local scrollViewPanel = CommonDefs.GetComponent_GameObject_Type(this.statusListScrollView.gameObject, typeof(UIPanel))
        scrollViewPanel:ResetAndUpdateAnchors()
    else
        local infoNumBar = this.selfOpArea.transform:Find("InfoNumBar").gameObject
        infoNumBar:SetActive(false)

        local scrollBg = CommonDefs.GetComponent_Component_Type(this.friendCircleNode.transform:Find("bg"), typeof(UISprite))
        scrollBg.height = CPersonalSpaceWnd.FriendScrollHeight1
        local scrollViewPanel = CommonDefs.GetComponent_GameObject_Type(this.statusListScrollView.gameObject, typeof(UIPanel))
        scrollViewPanel:ResetAndUpdateAnchors()
    end

    this.statusListTable:Reposition()
    this.statusListScrollView:ResetPosition()
end
CPersonalSpaceWnd.m_ShowNewLeaveMessage_CS2LuaHook = function (this)
    local leaveMessageTabNode = this.tabBar:GetTabGoByIndex(1)
    if leaveMessageTabNode ~= nil then
        local redPoint = leaveMessageTabNode.transform:Find("RedPoint")
        if redPoint ~= nil then
            redPoint.gameObject:SetActive(true)
        end
    end
end
CPersonalSpaceWnd.m_CheckNewsNum_CS2LuaHook = function (this)
    CPersonalSpaceMgr.Inst:GetNewInfoNum(DelegateFactory.Action_CPersonalSpace_NewInfo_Ret(function (ret)
        if not this.systemPortrait then
            return
        end

        if ret.code == 0 then
            this:ShowNewInfo(ret.data.inform)

            if ret.data.message > 0 then
                this:ShowNewLeaveMessage()
            end
        end
    end), nil)
end
CPersonalSpaceWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName)
    if methodName == "GetExpressionButton" then
        return this.expressionBtn
    elseif methodName == "GetSendStatusButton" then
        return this.sendStatusBtn
    elseif methodName == "Get1stTab" then
        return this.tabBar:GetTabGoByIndex(0)
    elseif methodName == "GetSettingButton" then
        return this.settingBtn
    elseif methodName == "GetSettingPanelOption6Button" then
        return this.settingPanel.transform:Find("node/Grid/Option6/click").gameObject
    end
    return nil
end
CPersonalSpaceWnd.m_GetTabIndex_CS2LuaHook = function (this)
    return this.tabBar.SelectedIndex
end
CPersonalSpaceWnd.m_OnUpdateMainPlayerExpressionDisplayInfo_CS2LuaHook = function (this, baseInfo, extraInfo)
    local expressionTxtTexture = CommonDefs.GetComponent_Component_Type(this.systemPortrait.transform.parent:Find("text"), typeof(CUITexture))
    CExpressionMgr.Inst:SetIconTextInfo(expressionTxtTexture, baseInfo, extraInfo, true)
    local sticker = CommonDefs.GetComponent_Component_Type(this.systemPortrait.transform.parent:Find("sticker"), typeof(CUITexture))
    CExpressionMgr.Inst:SetIconTextInfo(sticker, baseInfo, extraInfo, false)
end
CPersonalSpaceWnd.m_UpdateExpressionState_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst == nil then
        return
    end
    local cls = EnumToInt(CClientMainPlayer.Inst.Class)
    local gender = EnumToInt(CClientMainPlayer.Inst.Gender)
    this.systemPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, CClientMainPlayer.Inst.BasicProp.Expression), false)
end

CPersonalSpaceWnd.m_UpdateOpBtn_CS2LuaHook = function (this,_data)
    CPersonalSpaceMgr.Inst:SetSpaceShareBtn(this.otherOpArea.transform:Find("ShareBtn").gameObject, _data.roleid, _data.rolename)

    local moreOpBtn = this.otherOpArea.transform:Find("MoreOpBtn").gameObject

    local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

    if _data.isfollowing then
        -- 0无暗恋 1已暗恋 2互相暗恋
        if _data.slientLove == 2 then
            CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("  曾经互相暗恋"), DelegateFactory.Action_int(function (idx)
            end), false, nil, 0, EnumPopupMenuItemStyle.Special))
        elseif _data.slientLove == 0 then
            CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("暗恋TA"), DelegateFactory.Action_int(function (idx)
                    LuaPersonalSpaceMgrReal:SetSlientLove(_data.roleid, _data.rolename, _data.clazz, _data.gender, function(isBoth)
                        if not this or not this.systemPortrait or CommonDefs.IsUnityObjectNull(this) then
                            return
                        end
                        _data.slientLove = isBoth and 2 or 1
                        if isBoth then
                            g_MessageMgr:ShowMessage("MengDao_AnLian_LoveEachOther", _data.rolename)
                        end
                        this:UpdateOpBtn(_data)
                    end)
            end), false, nil, EnumPopupMenuItemStyle.Light))
        else
            CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("取消暗恋"), DelegateFactory.Action_int(function (idx)
                LuaPersonalSpaceMgrReal:CancelSlientLove(_data.roleid, function()
                    if not this or not this.systemPortrait or CommonDefs.IsUnityObjectNull(this) then
                        return
                    end
                    _data.slientLove = 0
                    this:UpdateOpBtn(_data)
                end)
            end), false, nil, EnumPopupMenuItemStyle.Light))
        end
    end

    if _data.roleid ~= this.SelfRoleInfo.id then
    if _data.isfollowing then
        CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("取消关注"), DelegateFactory.Action_int(function (idx)
            local backFunction = function(result)
            if not this or not this.systemPortrait then
                return
            end
            if result then
                _data.isfollowing = false
                this:UpdateOpBtn(_data)
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("取消关注玩家成功!"))
            end
            end
            LuaPersonalSpaceMgrReal.CancelPlayerFollow(_data.roleid,backFunction)
        end), false, nil, EnumPopupMenuItemStyle.Light))
    else
    CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("加关注"), DelegateFactory.Action_int(function (idx)
        local backFunction = function(result)
        if not this or not this.systemPortrait then
            return
        end
        if result then
            _data.isfollowing = true
            this:UpdateOpBtn(_data)
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("关注玩家成功!"))
        end
        end
        LuaPersonalSpaceMgrReal.SetPlayerFollow(_data.roleid,backFunction)
    end), false, nil, EnumPopupMenuItemStyle.Light))
    end
    end

    if CClientMainPlayer.Inst ~= nil and _data.server_id == CClientMainPlayer.Inst:GetMyServerId() then
    if not CIMMgr.Inst:IsMyFriend(_data.roleid) then
    CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("加好友"), DelegateFactory.Action_int(function (idx)
        Gac2Gas.AddFriend(_data.roleid, "PersonalSpace");
    end), false, nil, EnumPopupMenuItemStyle.Light))
    end
    end

    local onMoreClick = function(go)
        CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Top, 1, nil, 350, true, 280)
    end
    CommonDefs.AddOnClickListener(moreOpBtn,DelegateFactory.Action_GameObject(onMoreClick),false)
end
