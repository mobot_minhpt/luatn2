local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local UICamera = import "UICamera"
local CHouseCompetitionMgr=import "L10.Game.CHouseCompetitionMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Input = import "UnityEngine.Input"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CPlayerDataMgr = import "L10.Game.CPlayerDataMgr"
local Color = import "UnityEngine.Color"
local CFurnitureLayoutMgr = import "L10.UI.CFurnitureLayoutMgr"
local CFurnitureLayoutList = import "L10.UI.CFurnitureLayoutList"
local CFurnitureLayoutItem = import "L10.UI.CFurnitureLayoutItem"
local CFurnitureLayout = import "L10.UI.CFurnitureLayout"
local CDecorationLayoutItem = import "L10.UI.CDecorationLayoutItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CMainCamera = import "L10.Engine.CMainCamera"
local UITable = import "UITable"
local CScene = import "L10.Game.CScene" 
local CWater = import "L10.Engine.CWater"


CLuaFurnitureRightTopWnd = class()
RegistChildComponent(CLuaFurnitureRightTopWnd, "CameraBtn", "CameraBtn", QnSelectableButton)
RegistChildComponent(CLuaFurnitureRightTopWnd, "ViewButtons", "ViewButtons", GameObject)
RegistChildComponent(CLuaFurnitureRightTopWnd, "MultipleChooseBtn", "MultipleChooseBtn", GameObject)
RegistChildComponent(CLuaFurnitureRightTopWnd, "Grid", "Grid", UITable)


RegistClassMember(CLuaFurnitureRightTopWnd,"mFurnishTemplateBtn")
RegistClassMember(CLuaFurnitureRightTopWnd,"mLeaveFurnishBtn")
RegistClassMember(CLuaFurnitureRightTopWnd,"LabelStatus")
RegistClassMember(CLuaFurnitureRightTopWnd,"BtnUses")
RegistClassMember(CLuaFurnitureRightTopWnd,"BtnStores")
RegistClassMember(CLuaFurnitureRightTopWnd,"BtnPackAll")
RegistClassMember(CLuaFurnitureRightTopWnd,"TemplateGO")
RegistClassMember(CLuaFurnitureRightTopWnd,"NumbelLabel")
RegistClassMember(CLuaFurnitureRightTopWnd,"ShineiNumberLabel")
RegistClassMember(CLuaFurnitureRightTopWnd,"TingyuanNumberLabel")
RegistClassMember(CLuaFurnitureRightTopWnd,"TextTipNode")
RegistClassMember(CLuaFurnitureRightTopWnd,"TextTipLabel")
RegistClassMember(CLuaFurnitureRightTopWnd,"mZswTemplateBtn")

RegistClassMember(CLuaFurnitureRightTopWnd,"m_ViewEditButtons")
RegistClassMember(CLuaFurnitureRightTopWnd,"m_ViewEditButtonsIndex")
RegistClassMember(CLuaFurnitureRightTopWnd,"m_ViewEditIsPressed")
RegistClassMember(CLuaFurnitureRightTopWnd,"m_ViewEditButtonIndex")
RegistClassMember(CLuaFurnitureRightTopWnd,"m_CameraTrans")
RegistClassMember(CLuaFurnitureRightTopWnd,"m_CameraMoveSpeed")
RegistClassMember(CLuaFurnitureRightTopWnd,"m_CameraMinY")
RegistClassMember(CLuaFurnitureRightTopWnd,"m_CameraMaxY")


function CLuaFurnitureRightTopWnd:Awake()
    CLuaHouseMgr.YardDecorationPrivalege = false
    CLuaHouseMgr.RoomDecorationPrivalege = false

    self.m_CameraMinY = Zhuangshiwu_Setting.GetData().HouseCameraMinY
    self.m_CameraMaxY = Zhuangshiwu_Setting.GetData().HouseCameraMaxY
    self.m_CameraMoveSpeed = 1
    self.ViewButtons:SetActive(false)
    self.CameraBtn.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        self:OnChangeViewEditMode(b)
    end)
    self.m_ViewEditButtons = {
        [1] = self.ViewButtons.transform:Find("Left").gameObject,
        [2] = self.ViewButtons.transform:Find("Right").gameObject,
        [3] = self.ViewButtons.transform:Find("Up").gameObject,
        [4] = self.ViewButtons.transform:Find("Down").gameObject,
        [5] = self.ViewButtons.transform:Find("Reset").gameObject,
    }
    self.m_ViewEditButtonsIndex = {}
    for i,v in ipairs(self.m_ViewEditButtons) do
        self.m_ViewEditButtonsIndex[v.name] = i
    end
    for i=1,4 do
        UIEventListener.Get(self.m_ViewEditButtons[i]).onPress = DelegateFactory.BoolDelegate(function(go,state)
            local idx = self.m_ViewEditButtonsIndex[go.name]
            self.m_ViewEditButtonIndex = idx
            self.m_ViewEditIsPressed = state
        end)
    end
    UIEventListener.Get(self.m_ViewEditButtons[5]).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ResetCamera()
    end)

    self.mFurnishTemplateBtn = self.transform:Find("Anchor/FurnishTemplateBtn").gameObject
    self.mLeaveFurnishBtn = self.transform:Find("Anchor/LeaveFurnishModeBtn").gameObject
    self.LabelStatus = {}
    self.LabelStatus[1] = self.transform:Find("Anchor/FurnitureTemplate/Template1/LabelStatus"):GetComponent(typeof(UILabel))
    self.LabelStatus[2] = self.transform:Find("Anchor/FurnitureTemplate/Template2/LabelStatus"):GetComponent(typeof(UILabel))
    self.LabelStatus[3] = self.transform:Find("Anchor/FurnitureTemplate/Template3/LabelStatus"):GetComponent(typeof(UILabel))
    self.BtnUses = {}
    self.BtnUses[1] = self.transform:Find("Anchor/FurnitureTemplate/Template1/BtnUse").gameObject
    self.BtnUses[2] = self.transform:Find("Anchor/FurnitureTemplate/Template2/BtnUse").gameObject
    self.BtnUses[3] = self.transform:Find("Anchor/FurnitureTemplate/Template3/BtnUse").gameObject

    self.BtnStores = {}
    self.BtnStores[1] = self.transform:Find("Anchor/FurnitureTemplate/Template1/BtnStore").gameObject
    self.BtnStores[2] = self.transform:Find("Anchor/FurnitureTemplate/Template2/BtnStore").gameObject
    self.BtnStores[3] = self.transform:Find("Anchor/FurnitureTemplate/Template3/BtnStore").gameObject


    self.BtnPackAll = self.transform:Find("Anchor/FurnitureTemplate/PackAllBtn").gameObject
    self.TemplateGO = self.transform:Find("Anchor/FurnitureTemplate").gameObject
    self.NumbelLabel = self.transform:Find("Anchor/Info/NumberLabel"):GetComponent(typeof(UILabel))
    self.ShineiNumberLabel = self.transform:Find("Anchor/Info/AddLimit/ShineiNumberLabel"):GetComponent(typeof(UILabel))
    self.TingyuanNumberLabel = self.transform:Find("Anchor/Info/AddLimit/TingyuanNumberLabel"):GetComponent(typeof(UILabel))
    self.TextTipNode = self.transform:Find("TextTip").gameObject
    self.TextTipLabel = self.transform:Find("TextTip/name"):GetComponent(typeof(UILabel))

    self.mZswTemplateBtn = self.transform:Find("Anchor/ZswTemplateBtn").gameObject

    local placementBtn = self.transform:Find("Anchor/FurnitureTemplate/placementBtn").gameObject
    UIEventListener.Get(placementBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickPlaceAllButton(go)
    end)

    UIEventListener.Get(self.mFurnishTemplateBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnFurnishTemplateBtnClicked(go)
    end)
    UIEventListener.Get(self.mLeaveFurnishBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CClientFurnitureMgr.Inst:StopFurnish()
    end)
    UIEventListener.Get(self.BtnPackAll).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBtnPackAllClicked(go)
    end)
    UIEventListener.Get(self.mZswTemplateBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnZswTemplateBtnClicked(go)
    end)
    for i,v in ipairs(self.BtnStores) do
        UIEventListener.Get(v).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnBtnStoreClicked(go)
        end)
    end

    for i,v in ipairs(self.BtnUses) do
        UIEventListener.Get(v).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnBtnUseClicked(go)
        end)
    end
    self.TemplateGO:SetActive(false)

    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        if CClientMainPlayer.Inst then
            Gac2Gas.QueryRoomerPrivalegeInfo(CClientMainPlayer.Inst.Id)
        end 
    end

    UIEventListener.Get(self.MultipleChooseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnMultipleChooseBtnClick(go)
    end)

    self.MultipleChooseBtn:SetActive(CLuaHouseMgr.s_OpenFurnitureMultipleEdit)
    self.Grid:Reposition()
end
function CLuaFurnitureRightTopWnd:OnChangeViewEditMode(b)
    self.ViewButtons:SetActive(b)
    CClientFurnitureMgr.Inst.FurnitureEditCanRotate = b
end
function CLuaFurnitureRightTopWnd:ResetCamera()
    if self.m_CameraTrans == nil then
        self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
    end
    self.m_CameraTrans.localPosition = Vector3(0,0,0)
end

function CLuaFurnitureRightTopWnd:OnClickPlaceAllButton(go)
    if not self:CheckRoomerPrivalege() then
        g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
        return
    end
    local reachMax = false

    local bSetLimitStatus = CClientHouseMgr.Inst.mConstructProp.FurnitureLimitStatus > 0
    local maxNum = CLuaClientFurnitureMgr.GetMaxNum()

    if bSetLimitStatus then
        local shineiNumber = CLuaClientFurnitureMgr.GetPlacedShineiFurnitureCount(true)
        local tingyuanNumber = CLuaClientFurnitureMgr.GetPlacedShineiFurnitureCount(false)

        local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
        local notInYard = (sceneType ~= EnumHouseSceneType_lua.eYard)
        if notInYard then
            reachMax = shineiNumber>=maxNum
        else
            reachMax = tingyuanNumber>=maxNum
        end
    else
        local curNum = CLuaClientFurnitureMgr.GetTotalPlacedFurnitureCount()
        reachMax = curNum>=maxNum
    end

    if reachMax then
        g_MessageMgr:ShowMessage("Cant_Add_Furniture_Exceed_Total")
        return
    end

    
    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HOUSE_AUTO_CONFIRM"), DelegateFactory.Action(function () 
        CLuaClientFurnitureMgr.StopTemplate()
        Gac2Gas.PlayerRequestAutoLayoutCheck()
    end), nil, nil, nil, false)

end

function CLuaFurnitureRightTopWnd:Init()
    self:InitNumberLabel()
    self.TextTipNode:SetActive(false)

    if CLuaPlayerSettings.NeedHouseZhuangXiuAlert() then
        if not CommonDefs.IS_VN_CLIENT and not CommonDefs.IS_HMT_CLIENT then
            g_MessageMgr:ShowMessage("HOUSE_DECORATE_NOTICE")
        end
        CLuaPlayerSettings.SetHouseZhuangXiuAlert()
    end
end

--一帧内可能有较多OnPlacedFurnitureChanged消息 直接响应会引起卡顿，所以这里用了延迟的方法
local needUpdateCount = false
function CLuaFurnitureRightTopWnd:OnPlacedFurnitureChanged()
    needUpdateCount=true
end

function CLuaFurnitureRightTopWnd:SetCameraTrans()
    if self.m_ViewEditIsPressed then
        if self.m_CameraTrans == nil then
            self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
        end
        local pos = self.m_CameraTrans.localPosition

        if self.m_ViewEditButtonIndex == 3 then
            if self.m_CameraTrans.position.y > self.m_CameraMaxY then
                return
            end
        end
        if self.m_ViewEditButtonIndex == 4 and CScene.MainScene then
            local logicHeight = CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
            if self.m_CameraTrans.position.y < math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
                return
            end
        end

        local deltaTime = Time.deltaTime
        local val = deltaTime*self.m_CameraMoveSpeed
        if self.m_ViewEditButtonIndex==1 then--right
            pos = Vector3(pos.x-val,pos.y,pos.z)
        elseif self.m_ViewEditButtonIndex==2 then
            pos = Vector3(pos.x+val,pos.y,pos.z)
        elseif self.m_ViewEditButtonIndex==3 then
            pos = Vector3(pos.x,pos.y+val,pos.z)
        elseif self.m_ViewEditButtonIndex==4 then
            pos = Vector3(pos.x,pos.y-val,pos.z)
        end
        
        self.m_CameraTrans.localPosition = pos
    else
        --//不允许摄像机低于地表
        if self.m_CameraTrans == nil then
            self.m_CameraTrans = CameraFollow.Inst.transform:Find("camRotate/newParent")
        end

        local pos = Vector3(self.m_CameraTrans.position.x, self.m_CameraTrans.position.y, self.m_CameraTrans.position.z)
        if CScene.MainScene then
            local logicHeight = CScene.MainScene:GetLogicHeight(math.floor(self.m_CameraTrans.position.x * 64), math.floor(self.m_CameraTrans.position.z * 64))
            if self.m_CameraTrans.position.y < math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY then
                pos.y = math.max(CWater.s_WaterHeight, logicHeight) + self.m_CameraMinY
                self.m_CameraTrans.position = pos
            end
        end
    end
end

function CLuaFurnitureRightTopWnd:Update()
    self:ClickThroughToCloseTemplate()
    if needUpdateCount then
        self:InitNumberLabel()
        needUpdateCount = false
    end
    self:SetCameraTrans()
end

function CLuaFurnitureRightTopWnd:InitNumberLabel( )
    if CClientHouseMgr.Inst.mConstructProp == nil then
        return
    end
    local bSetLimitStatus = CClientHouseMgr.Inst.mConstructProp.FurnitureLimitStatus > 0
    local maxNum = CLuaClientFurnitureMgr.GetMaxNum()

    if bSetLimitStatus then
        self.NumbelLabel.gameObject:SetActive(false)
        self.TingyuanNumberLabel.gameObject:SetActive(true)
        self.ShineiNumberLabel.gameObject:SetActive(true)

        local shineiNumber = CLuaClientFurnitureMgr.GetPlacedShineiFurnitureCount(true)
        local tingyuanNumber = CLuaClientFurnitureMgr.GetPlacedShineiFurnitureCount(false)

        self.ShineiNumberLabel.text = SafeStringFormat3(LocalString.GetString("室内已摆放 %d/%d"), shineiNumber, maxNum)
        self.TingyuanNumberLabel.text = SafeStringFormat3(LocalString.GetString("庭院已摆放 %d/%d"), tingyuanNumber, maxNum)
    else
        self.NumbelLabel.gameObject:SetActive(true)
        self.TingyuanNumberLabel.gameObject:SetActive(false)
        self.ShineiNumberLabel.gameObject:SetActive(false)

        local curNum = CLuaClientFurnitureMgr.GetTotalPlacedFurnitureCount()

        self.NumbelLabel.text = SafeStringFormat3(LocalString.GetString("摆放 %d/%d"), curNum, maxNum)
    end
end
function CLuaFurnitureRightTopWnd:OnEnable( )
	g_ScriptEvent:AddListener("OnLoadFurnitureLayoutSuccess", self, "InitLayout")
	g_ScriptEvent:AddListener("OnFurnitureMoveChoose", self, "SetTipText")
	g_ScriptEvent:AddListener("OnFurnitureMoveChoose_Lua", self, "SetTipText_Lua")
    g_ScriptEvent:AddListener("QueryRoomerPrivalegeInfoResult", self, "OnQueryRoomerPrivalegeInfoResult")

	g_ScriptEvent:AddListener("OnPlacedFurnitureChanged", self, "OnPlacedFurnitureChanged")
    
end
function CLuaFurnitureRightTopWnd:OnDisable( )
    
	g_ScriptEvent:RemoveListener("OnLoadFurnitureLayoutSuccess", self, "InitLayout")
	g_ScriptEvent:RemoveListener("OnFurnitureMoveChoose", self, "SetTipText")
	g_ScriptEvent:RemoveListener("OnFurnitureMoveChoose_Lua", self, "SetTipText_Lua")
    g_ScriptEvent:RemoveListener("QueryRoomerPrivalegeInfoResult", self, "OnQueryRoomerPrivalegeInfoResult")

	g_ScriptEvent:RemoveListener("OnPlacedFurnitureChanged", self, "OnPlacedFurnitureChanged")
end
function CLuaFurnitureRightTopWnd:OnQueryRoomerPrivalegeInfoResult(args)
    local opInfo = args[5]

    local roomerId = args[1]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id
    if myId==roomerId then
        CLuaHouseMgr.YardDecorationPrivalege = false
        CLuaHouseMgr.RoomDecorationPrivalege = false
        if opInfo then
            do
                local i = 0
                while i < opInfo.Count do
                    local idx = math.floor(tonumber(opInfo[i] or 0))
                    local v = math.floor(tonumber(opInfo[i + 1] or 0))
                    if idx == EHouseEditableOperation_lua.OpYardFurniture and v ~= 0 then
                        CLuaHouseMgr.YardDecorationPrivalege = true
                    end
                    if idx == EHouseEditableOperation_lua.OpRoomFurniture and v ~= 0 then
                        CLuaHouseMgr.RoomDecorationPrivalege = true
                    end
                    i = i + 2
                end
            end 
        end
    end
end
function CLuaFurnitureRightTopWnd:SetTipText( args) 
    local text = args[0]
    if text==nil or text=="" then
        self.TextTipNode:SetActive(false)
    else
        self.TextTipNode:SetActive(true)
        self.TextTipLabel.text = text
    end
end
function CLuaFurnitureRightTopWnd:SetTipText_Lua(text)
    if text==nil or text=="" then
        self.TextTipNode:SetActive(false)
    else
        self.TextTipNode:SetActive(true)
        self.TextTipLabel.text = text
    end
end
function CLuaFurnitureRightTopWnd:OnFurnishTemplateBtnClicked( go) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local playerId = CClientMainPlayer.Inst.Id
    self.TemplateGO:SetActive(not self.TemplateGO.activeSelf)

    if self.TemplateGO.activeSelf and CFurnitureLayoutMgr.Layouts == nil then
        if CHouseCompetitionMgr.sb_IsInMirrorServer then
            CFurnitureLayoutMgr.Layouts = CPlayerDataMgr.Inst:LoadFurnitureLayoutList(self:HouseDesignMakeFileNameById(playerId))
        else
            CFurnitureLayoutMgr.Layouts = CPlayerDataMgr.Inst:LoadFurnitureLayoutList(self:MakeFileNameById(playerId))
        end
        if CFurnitureLayoutMgr.Layouts == nil then
            CFurnitureLayoutMgr.Layouts = CFurnitureLayoutList()
            for i = 0, 2 do
                CommonDefs.ListAdd(CFurnitureLayoutMgr.Layouts.LayoutList, typeof(CFurnitureLayout), CreateFromClass(CFurnitureLayout))
            end
        end
    end

    if self.TemplateGO.activeInHierarchy then
        self:InitLayout()
    end
end

function CLuaFurnitureRightTopWnd:MakeFileNameById(playerId)
    return tostring(playerId)..".bytes"
end
--镜像服保存的模板位置要换一下
function CLuaFurnitureRightTopWnd:HouseDesignMakeFileNameById(playerId)
    return "_"..tostring(playerId)..".bytes"
end


function CLuaFurnitureRightTopWnd:InitLayout( )
    if CFurnitureLayoutMgr.Layouts ~= nil then
        for i,v in ipairs(self.BtnUses) do
            if CFurnitureLayoutMgr.Layouts.LayoutList.Count > i-1 and CFurnitureLayoutMgr.Layouts.LayoutList[i-1]:IsInited() then
                local layout = CFurnitureLayoutMgr.Layouts.LayoutList[i-1]
                self.BtnUses[i]:SetActive(true)
                if CFurnitureLayoutMgr.LastUsedLayout == i-1 then
                    self.LabelStatus[i].text = LocalString.GetString("使用中")
                    self.LabelStatus[i].color = Color.green
                else
                    self.LabelStatus[i].text = ""
                end
            else
                self.BtnUses[i]:SetActive(false)
                self.LabelStatus[i].text = LocalString.GetString("(暂无)")
                self.LabelStatus[i].color = Color.gray
            end
        end
    end
end
function CLuaFurnitureRightTopWnd:ClickThroughToCloseTemplate( )
    if Input.GetMouseButtonDown(0) then
        if not CUICommonDef.IsOverUI 
        or (UICamera.lastHit.collider ~= nil 
            and not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) 
            and CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform)) then
            self.TemplateGO:SetActive(false)
        end
    end
end

function CLuaFurnitureRightTopWnd:CheckRoomerPrivalege()
    if CClientHouseMgr.Inst:IsCurHouseRoomer() then
        local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
        if sceneType == EnumHouseSceneType_lua.eRoom and CLuaHouseMgr.RoomDecorationPrivalege then
            return true
        elseif sceneType == EnumHouseSceneType_lua.eYard and CLuaHouseMgr.YardDecorationPrivalege then
            return true
        elseif sceneType == EnumHouseSceneType_lua.eXiangfang then
            return true
        else
            return false
        end
    else
        return true
    end
end
function CLuaFurnitureRightTopWnd:OnBtnPackAllClicked( go) 
    if not self:CheckRoomerPrivalege() then
        g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
        return
    end

    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("FurnitureColor_PackAll_Confirm"), DelegateFactory.Action(function () 
        CLuaClientFurnitureMgr.StopTemplate()
        Gac2Gas.RequestPackAllFurniture()
    end), nil, nil, nil, false)
end
function CLuaFurnitureRightTopWnd:OnBtnStoreClicked( go) 
    for i,v in ipairs(self.BtnStores) do
        if go==v then
            self:SaveLayout(i-1)
            break
        end
    end
end
function CLuaFurnitureRightTopWnd:SaveLayout( idx) 
    if CFurnitureLayoutMgr.Layouts == nil or CFurnitureLayoutMgr.Layouts.LayoutList.Count <= idx then
        return
    end

    local saveAction = function () 
        if CClientMainPlayer.Inst == nil or not CClientHouseMgr.Inst:IsInOwnHouse() then
            return
        end
        local furnitureList = {}
        local decorationList = {}
        local dic = CClientFurnitureMgr.Inst.FurnitureList
        CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, fur) 
            if fur.ParentId==0 then
                if fur.TemplateId == 9448 then--温泉池放第一位
                    table.insert( furnitureList, 1,fur )
                else
                    table.insert( furnitureList, fur )
                end
            else
                table.insert( decorationList, fur )
            end
        end))


        local bInRoom = CClientHouseMgr.Inst:IsPlayerInRoom()
        local bIsMingyuan = CClientHouseMgr.Inst:IsPlayerInMingyuan()
        local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
        CFurnitureLayoutMgr.Layouts.LayoutList[idx]:Init(bInRoom, bIsMingyuan, sceneType)
        CommonDefs.ListClear(CFurnitureLayoutMgr.Layouts.LayoutList[idx].FurnitureItems)
        CommonDefs.ListClear(CFurnitureLayoutMgr.Layouts.LayoutList[idx].DecorationItems)
        local id2pos = {}
        local id2count = {}

        for i,v in ipairs(furnitureList) do
            local item = CFurnitureLayoutItem()
            item.TemplateId = v.TemplateId
            item.X = v.ServerPos.x
            item.Y = v.ServerPos.y
            item.Z = v.ServerPos.z
            item.Rot = v.ServerRotateY
            item.IsWinter = v.IsWinter
            local scale = 1
            if v.RO ~= nil then
                scale = v.RO.Scale
            end
            item.Scale = scale
            item.RX = v.ServerRX
            item.RY = v.ServerRY
            item.RZ = v.ServerRZ

            CommonDefs.ListAdd(CFurnitureLayoutMgr.Layouts.LayoutList[idx].FurnitureItems, typeof(CFurnitureLayoutItem), item)

            local count = id2count[item.TemplateId] or 0
            if count>0 then
                id2count[item.TemplateId] = count+1
            else
                id2count[item.TemplateId] = 1
            end

            id2pos[v.ID] = count+1
        end

        for i,deco in ipairs(decorationList) do
            local parent = CClientFurnitureMgr.Inst:GetFurniture(deco.ParentId)
            if parent ~= nil and id2pos[deco.ParentId] then
                local item = CDecorationLayoutItem()
                item.TemplateId = deco.TemplateId
                item.SlotIdx = deco.SlotIndex
                item.ParentPos = id2pos[deco.ParentId]
                item.ParentTemplateId = parent.TemplateId
                CommonDefs.ListAdd(CFurnitureLayoutMgr.Layouts.LayoutList[idx].DecorationItems, typeof(CDecorationLayoutItem), item)
            end
        end
        local poolstr,warmpoolstr = LuaPoolMgr.SavePoolAppearanceToString()
        CFurnitureLayoutMgr.Layouts.LayoutList[idx].Pool = poolstr
        CFurnitureLayoutMgr.Layouts.LayoutList[idx].WarmPool = warmpoolstr
        
        if CHouseCompetitionMgr.sb_IsInMirrorServer then
            CPlayerDataMgr.Inst:SaveFurnitureLayoutList(self:HouseDesignMakeFileNameById(CClientMainPlayer.Inst.Id), CFurnitureLayoutMgr.Layouts)
        else
            CPlayerDataMgr.Inst:SaveFurnitureLayoutList(self:MakeFileNameById(CClientMainPlayer.Inst.Id), CFurnitureLayoutMgr.Layouts)
        end

        EventManager.Broadcast(EnumEventType.OnLoadFurnitureLayoutSuccess)
        g_MessageMgr:ShowMessage("HOUSE_STORE_LAYOUT_SUCCESS")
    end

    if not CFurnitureLayoutMgr.Layouts.LayoutList[idx]:IsInited() then
        saveAction()
    else
        MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确认覆盖已有装修模板吗？"), DelegateFactory.Action(function () saveAction() end), nil, nil, nil, false)
    end
end
function CLuaFurnitureRightTopWnd:OnBtnUseClicked( go) 
    if not self:CheckRoomerPrivalege() then
        g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
        return
    end
    for i,v in ipairs(self.BtnUses) do
        if go==v then
            self:TryLoadLayout(i-1)
            break
        end
    end
end
function CLuaFurnitureRightTopWnd:TryLoadLayout( idx) 
    if CClientMainPlayer.Inst == nil 
        or not CClientHouseMgr.Inst:IsInOwnHouse() 
        or CFurnitureLayoutMgr.Layouts == nil 
        or CFurnitureLayoutMgr.Layouts.LayoutList.Count <= idx 
        or not CFurnitureLayoutMgr.Layouts.LayoutList[idx]:IsInited() then
        return
    end
    local layout = CFurnitureLayoutMgr.Layouts.LayoutList[idx]
    local bLayoutInRoom = layout.InRoom
    local bLayoutInMingyuan = layout.IsMingyuan
    local bInRoom = CClientHouseMgr.Inst:IsPlayerInRoom()
    local bIsMingyuan = CClientHouseMgr.Inst:IsPlayerInMingyuan()
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    local layoutSceneType = layout.SceneType

    local strInRoom = LocalString.GetString("室内")
    local strYard = LocalString.GetString("庭院")
    local strXiangfang = LocalString.GetString("厢房")
    local sceneToStr = {}

    sceneToStr[EnumHouseSceneType_lua.eRoom] = strInRoom
    sceneToStr[EnumHouseSceneType_lua.eYard] = strYard
    sceneToStr[EnumHouseSceneType_lua.eXiangfang] = strXiangfang


    if layout.Version == 1 then
        if bLayoutInRoom ~= bInRoom then
            local default = bInRoom and strInRoom or strYard
            local extern = bLayoutInRoom and strInRoom or strYard
            g_MessageMgr:ShowMessage("CANT_LOAD_LAYOUT_DIFFERENT_SCENE", default, extern)
            return
        end
        if sceneType == EnumHouseSceneType_lua.eXiangfang then
            local ref = bLayoutInRoom and strInRoom or strYard
            g_MessageMgr:ShowMessage("CANT_LOAD_LAYOUT_DIFFERENT_SCENE", strXiangfang, ref)
            return
        end
    elseif layout.Version == 2 then
        if sceneType ~= layoutSceneType then
            local s1 = sceneToStr[sceneType] or ""
            local s2 = sceneToStr[layoutSceneType] or ""
            g_MessageMgr:ShowMessage("CANT_LOAD_LAYOUT_DIFFERENT_SCENE", s1, s2)
            return
        end
    end

    if bIsMingyuan ~= bLayoutInMingyuan then
        local strMingyuan = LocalString.GetString("名园")
        local strPutong = LocalString.GetString("普通家园")
        local out = bIsMingyuan and strMingyuan or strPutong
        local try = bLayoutInMingyuan and strMingyuan or strPutong
        g_MessageMgr:ShowMessage("CANT_LOAD_LAYOUT_DIFFERENT_SCENE", out, try)
        return
    end

    local moneyNeed = Zhuangshiwu_Setting.GetData().LoadLayoutMoney
    local text = g_MessageMgr:FormatMessage("HOUSE_LOAD_LAYOUT_CONFIRM", moneyNeed)
    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
        Gac2Gas.PlayerRequestLoadHouseLayoutCheck(idx)
    end), nil, nil, nil, false)
end


function CLuaFurnitureRightTopWnd:GetGuideGo(methodName)
    if methodName=="GetLeaveFurnishButton" then
        return self.mLeaveFurnishBtn
    end
end

function CLuaFurnitureRightTopWnd:OnZswTemplateBtnClicked(go)
    if self.TemplateGO.activeSelf then
        self.TemplateGO:SetActive(false)
        return
    end 

    if not IsOpenCustomZswTemplate()then
        CUIManager.ShowUI(CLuaUIResources.ZswTemplateTopRightWnd)
    else
        CLuaZswTemplateMgr.OnClickMenueButton(go)
    end
end


function CLuaFurnitureRightTopWnd:OnDestroy()
    CClientFurnitureMgr.Inst.FurnitureEditCanRotate = false
    self:ResetCamera()
end

function CLuaFurnitureRightTopWnd:OnMultipleChooseBtnClick(go)
    --权限检查
    if not self:CheckRoomerPrivalege() then
        g_MessageMgr:ShowMessage("ROOMER_CANNOT_USE")
        return
    end

    CUIManager.ShowUI(CLuaUIResources.MultipleFurnitureEditWnd)
end