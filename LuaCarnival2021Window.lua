local CommonDefs        = import "L10.Game.CommonDefs"
local DelegateFactory   = import "DelegateFactory"
local CButton           = import "L10.UI.CButton"
local UILabel           = import "UILabel"
local CUIFx             = import "L10.UI.CUIFx"
local GameObject        = import "UnityEngine.GameObject"
local CItemInfoMgr      = import "L10.UI.CItemInfoMgr"
local AlignType         = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr          = import "L10.Game.CItemMgr"
local CServerTimeMgr    = import "L10.Game.CServerTimeMgr"
local UIEventListener   = import "UIEventListener"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CWebBrowserMgr    = import "L10.Game.CWebBrowserMgr"

LuaCarnival2021Window = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCarnival2021Window, "Button", "Button", CButton)
RegistChildComponent(LuaCarnival2021Window, "TipBtn", "TipBtn", CButton)
RegistChildComponent(LuaCarnival2021Window, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaCarnival2021Window, "ItemNode1", "ItemNode1", GameObject)
RegistChildComponent(LuaCarnival2021Window, "ItemNode2", "ItemNode2", GameObject)
RegistChildComponent(LuaCarnival2021Window, "ItemNode3", "ItemNode3", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaCarnival2021Window, "m_TicketName")
RegistClassMember(LuaCarnival2021Window, "m_TicketBeginTime")
RegistClassMember(LuaCarnival2021Window, "m_TicketEndTime")
RegistClassMember(LuaCarnival2021Window, "m_BuyBtnName")
RegistClassMember(LuaCarnival2021Window, "m_BuyTipTextTable")
RegistClassMember(LuaCarnival2021Window, "m_CountDownTick")



function LuaCarnival2021Window:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


    --@endregion EventBind end
end

function LuaCarnival2021Window:Init()
	Gac2Gas.QueryJianNianHuaTicketSellInfo()
	self.TipLabel.text = g_MessageMgr:FormatMessage("JIANIANHUA_LOCATION")
	self.m_TicketBeginTime = JiaNianHua_Setting.GetData().TicketBeginTime2021
	self.m_TicketEndTime = JiaNianHua_Setting.GetData().TicketEndTime2021
	self:InitTicketName()
	self:InitNodeItem(self.ItemNode1,1)
	self:InitNodeItem(self.ItemNode2,2)
	self:InitNodeItem(self.ItemNode3,3)
	local fx = self.transform:Find("ZuoQiShow/fx"):GetComponent(typeof(CUIFx))
	local fx1 = self.transform:Find("ZuoQiShow/fx1"):GetComponent(typeof(CUIFx))
	fx:DestroyFx()
	fx1:DestroyFx()
	fx:LoadFx("Fx/UI/Prefab/UI_jianianhuashoupiao.prefab")
	fx1:LoadFx("Fx/UI/Prefab/UI_jianianhuashoupiao_guangshu.prefab")
	if self.m_CountDownTick then 
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
	self.m_CountDownTick = RegisterTick(function() Gac2Gas.QueryJianNianHuaTicketSellInfo() end, 60000)
end

function LuaCarnival2021Window:UpdateTicketBuyBtn()
	self:InitBuyBtn(self.ItemNode1,1)
	self:InitBuyBtn(self.ItemNode2,2)
	self:InitBuyBtn(self.ItemNode3,3)
end

function LuaCarnival2021Window:InitBuyBtn(go,index)
	local data = JiaNianHua_2021Sale.GetData(index)
	if data then
	  local itemsId = data.ItemsId
	  local goodsId = data.GoodsId
	  local buyBtn = go.transform:Find('BuyButton').gameObject
  
	  if goodsId and goodsId > 0 and index < 4 then
		local now = CServerTimeMgr.Inst.timeStamp
		--local beginOfTheBuyDay = CServerTimeMgr.Inst:GetTimeStampByStr("2021-6-22 00:00")
		--local endOfTheBuyDay = CServerTimeMgr.Inst:GetTimeStampByStr("2021-6-24 10:00")
		local beginOfTheBuyDay = CServerTimeMgr.Inst:GetTimeStampByStr(self.m_TicketBeginTime)
		local endOfTheBuyDay = CServerTimeMgr.Inst:GetTimeStampByStr(self.m_TicketEndTime)
		local beginSeconds = beginOfTheBuyDay - now
		local endSeconds = endOfTheBuyDay - now

		if endSeconds < 0 then
			buyBtn:GetComponent(typeof(CButton)).Enabled = false
			buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('售票结束')
			-- 售票结束之后就不用刷新按钮了
			if self.m_CountDownTick then 
				UnRegisterTick(self.m_CountDownTick)
				self.m_CountDownTick = nil
			end
			return
		elseif beginSeconds > 0 then
			buyBtn:GetComponent(typeof(CButton)).Enabled = false
			local beginTime = os.date("*t",beginOfTheBuyDay)
			buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('%d月%d日%d时开售'),beginTime.month,beginTime.day,beginTime.hour)
			return
		elseif LuaWelfareMgr.Carnival2021TicketSellList then
			for i=1,#LuaWelfareMgr.Carnival2021TicketSellList do
				if LuaWelfareMgr.Carnival2021TicketSellList[i].GoodsId == goodsId and LuaWelfareMgr.Carnival2021TicketSellList[i].Stock <= 0 then
					buyBtn:GetComponent(typeof(CButton)).Enabled = false
					buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已售罄')
					return
				end
			end
		end
		buyBtn:GetComponent(typeof(CButton)).Enabled = true
		buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString("购买")..self.m_BuyBtnName[index]
	  end
	end
end

function LuaCarnival2021Window:InitNodeItem(go,index)
	local data = JiaNianHua_2021Sale.GetData(index)
	if data then
	  local itemsId = data.ItemsId
	  local goodsId = data.GoodsId
	  local buyBtn = go.transform:Find('BuyButton').gameObject
	  self:InitBuyBtn(go,index)
  	  local onBuyClick = function(go)
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel >= 30 then
				if self.m_BuyTipTextTable and self.m_BuyTipTextTable[index] then
				local message = self.m_BuyTipTextTable[index]
				MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
					if self:CheckCanOpenURL() then
						Gac2Gas.RequestBuyJiaNianHua2020Ticket(goodsId)
					end
				end), nil, LocalString.GetString("购买"), nil, false)
				end
			else
				g_MessageMgr:ShowMessage("JiaNianHua_Ticket_Limit")
			end
		end
		CommonDefs.AddOnClickListener(buyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)
	  for i=1,4 do
		local itemNode = go.transform:Find('Item'..i).gameObject
		itemNode:SetActive(false)
	  end
  
	  local itemsTable = g_LuaUtil:StrSplit(itemsId, ",")
	  if itemsTable and index == 3 then
		for i,v in pairs(itemsTable) do
			local num = i
			local itemId = tonumber(v)
		  	
		  	local itemData = CItemMgr.Inst:GetItemTemplate(itemId)
  
		  	local itemNode = go.transform:Find('Item'..num).gameObject
		  	itemNode:SetActive(true)
		  	local bonusIcon = go.transform:Find('Item'..num..'/Normal/IconTexture'):GetComponent(typeof(CUITexture))
		  	bonusIcon:LoadMaterial(itemData.Icon)
		  	local iconLabel = go.transform:Find('Item'..num..'/Normal/DescLabel').gameObject
		  	iconLabel:SetActive(false)
		  	local clickNode = go.transform:Find('Item'..num).gameObject
		  	UIEventListener.Get(clickNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(clickNode).onClick, DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
		  		end), true)
			end
		else
			self:InitItem(itemsId)
		end
	end
end

function LuaCarnival2021Window:InitItem(itemsId)
	local itemsTable = g_LuaUtil:StrSplit(itemsId, ",")
	for i=1,4 do
		local itemNode1 = self.ItemNode1.transform:Find('Item'..i).gameObject
		itemNode1:SetActive(false)
		local itemNode2 = self.ItemNode2.transform:Find('Item'..i).gameObject
		itemNode2:SetActive(false)
	end
	for i,v in pairs(itemsTable) do
		local go = nil
		local num = i
		local itemId = tonumber(v)
		if i == 1 or i == 2 or i ==5 or i == 6 then
			go = self.ItemNode1
			if i == 5 or i ==6 then num = i-2 end
		else
		if i == 3 or i == 4 then num = i-2 end
		if i == 7 or i == 8 then num = i-4 end
			go = self.ItemNode2
		end
		  	
		local itemData = CItemMgr.Inst:GetItemTemplate(itemId)
  
		local itemNode = go.transform:Find('Item'..num).gameObject
		itemNode:SetActive(true)
		local bonusIcon = go.transform:Find('Item'..num..'/Normal/IconTexture'):GetComponent(typeof(CUITexture))
		bonusIcon:LoadMaterial(itemData.Icon)
		local iconLabel = go.transform:Find('Item'..num..'/Normal/DescLabel').gameObject
		iconLabel:SetActive(false)
		local clickNode = go.transform:Find('Item'..num).gameObject
		UIEventListener.Get(clickNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(clickNode).onClick, DelegateFactory.VoidDelegate(function (go)
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
		end), true)
	end
end

function LuaCarnival2021Window:InitTicketName()
	self.m_TicketName = {}
	self.m_TicketName[1] = LocalString.GetString("单日豪华票")
	self.m_TicketName[2] = LocalString.GetString("双日豪华票")
	self.m_TicketName[3] = LocalString.GetString("单日畅玩票")
	self.m_BuyBtnName = {}
	self.m_BuyBtnName[1] = LocalString.GetString("单日票")
	self.m_BuyBtnName[2] = LocalString.GetString("双日票")
	self.m_BuyBtnName[3] = LocalString.GetString("畅玩票")
	self.m_BuyTipTextTable = {}
	local limitTable = {}
	limitTable[1] = LocalString.GetString("仅8月20日可用")
	limitTable[2] = LocalString.GetString("8月19/20两日可用")
	limitTable[3] = LocalString.GetString("仅8月20日可用")
	for i=1,3 do
		self.m_BuyTipTextTable[i] = g_MessageMgr:FormatMessage("JIANIANHUA_BUY_MAKESURE",self.m_TicketName[i],self.m_TicketName[i],limitTable[i])
	end
end

function LuaCarnival2021Window:CheckCanOpenURL()
	return true
end

--@region UIEvent

function LuaCarnival2021Window:OnButtonClick()
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel >= 30 then
		if self:CheckCanOpenURL() then
			if CommonDefs.Is_PC_PLATFORM() then
				CWebBrowserMgr.Inst:OpenUrl("https://qnm.163.com/2023/jnhcx/m/")
			else
				CWebBrowserMgr.Inst:OpenUrl("https://qnm.163.com/2023/jnhcx/mh/")
			end
		end
	else
		g_MessageMgr:ShowMessage("JiaNianHua_Ticket_Limit")
	end
end

function LuaCarnival2021Window:OnTipBtnClick()
	g_MessageMgr:ShowMessage('JIANIANHUA_RULE')
end

function LuaCarnival2021Window:OnEnable()
	g_ScriptEvent:AddListener("UpdateCarnival2021TicketData", self, "UpdateTicketBuyBtn")
end

function LuaCarnival2021Window:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateCarnival2021TicketData", self, "UpdateTicketBuyBtn")
	if self.m_CountDownTick then 
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end
--@endregion UIEvent

