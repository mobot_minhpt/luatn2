local UITexture = import "UITexture"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local UITabBar = import "L10.UI.UITabBar"
local Gas2Gac2 = import "L10.Game.Gas2Gac"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local UISlider = import "UISlider"
local UICamera = import "UICamera"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnExpandListBox = import "L10.UI.QnExpandListBox"
local UIGrid = import "UIGrid"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local DouHunCross_Setting = import "L10.Game.DouHunCross_Setting"
local CChatLinkMgr = import "CChatLinkMgr"
local CTrackMgr = import 'L10.Game.CTrackMgr'
local QnTabButton = import "L10.UI.QnTabButton"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local EnumActionType = import "L10.UI.EnumActionType"
local BoxCollider = import "UnityEngine.BoxCollider"
local Vector2 = import "UnityEngine.Vector2"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"
local CFightingSpiritMatchRecordWnd = import "L10.UI.CFightingSpiritMatchRecordWnd"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CGroupMemberInfo = import "L10.Game.CGroupMemberInfo"

LuaDouHunPage = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDouHunPage, "Desc", "Desc", GameObject)
RegistChildComponent(LuaDouHunPage, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaDouHunPage, "ProgressRoot", "ProgressRoot", GameObject)
RegistChildComponent(LuaDouHunPage, "ChampionInfoRoot", "ChampionInfoRoot", GameObject)
RegistChildComponent(LuaDouHunPage, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaDouHunPage, "ExpandBtn", "ExpandBtn", QnExpandListBox)
RegistChildComponent(LuaDouHunPage, "InfoGrid", "InfoGrid", UIGrid)
RegistChildComponent(LuaDouHunPage, "SeverLabel", "SeverLabel", UILabel)
RegistChildComponent(LuaDouHunPage, "ChampionTitleLabel", "ChampionTitleLabel", UILabel)
RegistChildComponent(LuaDouHunPage, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "MidBtn", "MidBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "DianZanRoot", "DianZanRoot", GameObject)
RegistChildComponent(LuaDouHunPage, "DaShangBtn", "DaShangBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "DianZanBtn", "DianZanBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "GambleBtn", "GambleBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "RewardBtn", "RewardBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaDouHunPage, "GambleAlert", "GambleAlert", GameObject)
RegistChildComponent(LuaDouHunPage, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaDouHunPage, "Dot02", "Dot02", UITexture)
RegistChildComponent(LuaDouHunPage, "Dot01", "Dot01", UITexture)
RegistChildComponent(LuaDouHunPage, "GambleFx", "GambleFx", GameObject)
RegistChildComponent(LuaDouHunPage, "ShareBtn", "ShareBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDouHunPage, "m_ProgressItem")
RegistClassMember(LuaDouHunPage, "m_CurrentStage")
RegistClassMember(LuaDouHunPage, "m_HasInit")
RegistClassMember(LuaDouHunPage, "m_DescList")
RegistClassMember(LuaDouHunPage, "m_ExtraInfo")

RegistClassMember(LuaDouHunPage, "GiftPackBtn")
RegistClassMember(LuaDouHunPage, "BottomDesc")
RegistClassMember(LuaDouHunPage, "BottomDescLabel")
RegistClassMember(LuaDouHunPage, "BottomDescTeam")


function LuaDouHunPage:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.DescLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDescLabelClick()
	end)


	
	UIEventListener.Get(self.DaShangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDaShangBtnClick()
	end)


	
	UIEventListener.Get(self.DianZanBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDianZanBtnClick()
	end)


	
	UIEventListener.Get(self.RewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end

	self.GiftPackBtn = self.transform:Find("ConstantBtn/GiftPackBtn").gameObject
	self.GiftPackBtn:SetActive(CommonDefs.IS_CN_CLIENT)
	UIEventListener.Get(self.GiftPackBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiftPackBtnClick()
	end)

	self.BottomDesc = self.transform:Find("BottomBtn/Desc")
	self.BottomDescLabel = self.BottomDesc:Find("DescLabel"):GetComponent(typeof(UILabel))
	self.BottomDescTeam = self.BottomDesc:Find("Team")
end

function LuaDouHunPage:Init()
	if self.m_HasInit then
		return
	end
	self.m_DescList = {}
	-- 策划填表
	DouHunTan_Agenda.Foreach(function(key, data)
		if not System.String.IsNullOrEmpty(data.MsgName) then
			table.insert(self.m_DescList, CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage(data.MsgName, data.Des), true))
		end
	end)
	table.insert(self.m_DescList, g_MessageMgr:FormatMessage("DouHun_Page_Stage_Desc_7"))

	self.m_HasInit = true
	self.m_CurrentStage = - 1
	self.m_ProgressItem = {}
	for i=1,7 do
		table.insert(self.m_ProgressItem, self.ProgressRoot.transform:Find(tostring(i)).gameObject)
		-- 初始化点击
		local item = self.m_ProgressItem[i]
		local activated = item.transform:Find("Activated/Bg"):GetComponent(typeof(QnTabButton))
		local unActivated = item.transform:Find("UnActivated/Bg"):GetComponent(typeof(QnTabButton))

		activated.onClick = DelegateFactory.Action_QnTabButton(function(btn)
			self:SetProgressStage(i)
		end)

		unActivated.onClick = DelegateFactory.Action_QnTabButton(function(btn)
			self:SetProgressStage(i)
		end)
	end

	self.ItemTemplate:SetActive(false)

	local names = CreateFromClass(MakeGenericClass(List, String))
	local levelName = DouHunCross_Setting.GetData().LevelName
	for i = 0, levelName.Length - 1 do
		CommonDefs.ListAdd(names, typeof(String), levelName[i])
	end

	-- 设置下拉菜单
	self.ExpandBtn:SetItems(names)

	local bg = self.ExpandBtn.transform:Find("BG"):GetComponent(typeof(UISprite))
	bg.transform.localPosition = Vector3(-100, -30, 0)
	bg.width = 260
	bg.height = 510

	local item = self.ExpandBtn.transform:Find("BG/ListItem").gameObject
	item:SetActive(false)

	local panel = self.ExpandBtn.transform:Find("BG/Scroll View"):GetComponent(typeof(UIPanel))
	panel.depth = panel.depth + 100

	self.ExpandBtn.ClickCallback = DelegateFactory.Action_int(function(index)
		-- 发送分组冠军信息查询RPC
		Gac2Gas.QueryDouHunCrossChampionGroupInfo(index+1)
	end)

	local texture = self.GambleFx.transform:GetComponent(typeof(UITexture))
	texture.depth = texture.depth + 100
	self.GambleFx:SetActive(false)

	local gambleBtnLabel = self.GambleBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
	local gambleTexture = self.GambleBtn:AddComponent(typeof(CUITexture))

	-- 设置竞猜样式 与 特效
	if CLuaFightingSpiritMgr:HasGambleOpen() then
		gambleBtnLabel.text = LocalString.GetString("竞猜")
		gambleTexture:LoadMaterial("UI/Texture/Transparent/Material/douhunpage_icon_jingcai.mat")
		local record = PlayerPrefs.GetInt("douhun_page_20_jingcai_fx", 0)
		if record == 0 then
			self.GambleFx:SetActive(true)
			PlayerPrefs.SetInt("douhun_page_20_jingcai_fx", 1)
		end
	else
		gambleBtnLabel.text = LocalString.GetString("问答")
		gambleTexture:LoadMaterial("UI/Texture/Transparent/Material/douhunpage_icon_wenda.mat")
		local record = PlayerPrefs.GetInt("douhun_page_20_wenda_fx", 0)
		if record == 0 then
			self.GambleFx:SetActive(true)
			PlayerPrefs.SetInt("douhun_page_20_wenda_fx", 1)
		end
	end

	local setting = DouHunTan_Setting.GetData()
    local season = setting.Season
	self.ChampionTitleLabel.text = SafeStringFormat3(LocalString.GetString("第%s届太一斗魂坛冠军"), season)
end

-- 初始化
function LuaDouHunPage:OnStageInfoReceived(stage, crossStage, crossLevel, extraInfo)
	self.m_ExtraInfo = extraInfo
	if CUIManager.IsLoaded(CLuaUIResources.FightingSpiritRewardShowWnd) or CUIManager.IsLoaded(CLuaUIResources.FightingSpiritMatchInfoWnd)
		or CUIManager.IsLoaded(CLuaUIResources.FightingSpiritGambleWnd) then
		return
	end

	self.m_CurrentStage = stage
	self:SetProgressStage(stage)

	if stage == 6 or stage == 7 then
        -- 主赛事
        Gac2Gas.QueryDouHunCrossGambleInfo(6)
	end
end

-- 尝试设置流光特效
function LuaDouHunPage:OnDouHunGambleResult(gambleType, bOpenDoubleBet, gambleData, betDataList)
    local hasGamble = betDataList[gambleType].hasGamble
    local bSetResult = gambleData.bSetResult
    local hasGotReward = betDataList[gambleType].hasGotReward

    -- 没猜没结果 有结果已猜
    if (hasGamble and bSetResult and not hasGotReward) or (not hasGamble and not bSetResult) then
        -- 设置特效
		self.GambleFx:SetActive(true)

		local fx = self.GambleFx.transform:GetComponent(typeof(CUIFx))
		fx:DestroyFx()
		fx:LoadFx("fx/ui/prefab/UI_taiyidouhuntan_jingcai_1.prefab")
    end
end

function LuaDouHunPage:SetBtnState(btn, text, bg, enable)
	local label = btn.transform:Find("Label"):GetComponent(typeof(UILabel))
	local sprite = btn.transform:GetComponent(typeof(UISprite))
	label.text = text
	sprite.spriteName = bg
	-- 改文字颜色
	if bg == "common_btn_01_yellow" then
		label.color = Color(53/255,27/255,1/255,1)
	else
		label.color = Color(14/255,50/255,84/255,1)
	end
	CUICommonDef.SetActive(btn, enable, true)
end

-- 设置页面显示 初始和点击按钮时候会调用
function LuaDouHunPage:SetProgressStage(index)
	if not self.m_HasInit then
		self:Init()
	end

	local activeColor = Color(124/255, 56/255, 34/255,1)
	local unactiveColor = Color(174/255, 144/255, 144/255,1)
	local selectColor = Color(1, 1, 1, 1)
	for i=1, 7 do
		local item = self.m_ProgressItem[i]
		local activated = item.transform:Find("Activated").gameObject
		local unActivated = item.transform:Find("UnActivated").gameObject
		local selected = item.transform:Find("Selected").gameObject
		local fx = item.transform:Find("Fx").gameObject
		local activeLabel = item.transform:Find("Activated/Label"):GetComponent(typeof(UILabel))
		local unactiveLabel = item.transform:Find("UnActivated/Label"):GetComponent(typeof(UILabel))

		-- 设置节点显示
		activated:SetActive(i <= self.m_CurrentStage)
		unActivated:SetActive(not (i <= self.m_CurrentStage))
		selected:SetActive(i == index)

		if i == index then
			activeLabel.color = selectColor
			unactiveLabel.color = selectColor
		else
			activeLabel.color = activeColor
			unactiveLabel.color = unactiveColor
		end
	end

	-- 进度显示
	self.ProgressBar.value = self.m_CurrentStage / 7

	local enableColor = Color(1,1,1,1)
	local disableColor = Color(32/255,0,0,1)
	if self.m_CurrentStage > 0 then
		self.Dot01.color = enableColor
	else
		self.Dot01.color = disableColor
	end
	if self.m_CurrentStage == 7 then
		self.Dot02.color = enableColor
	else
		self.Dot02.color = disableColor
	end

	self.DianZanRoot:SetActive(false)

	self.Desc:SetActive(true)
	-- 简介
	if index <= 7 and index > 0 then
		self.DescLabel.text = self.m_DescList[index]
	end
	self.ChampionInfoRoot:SetActive(false)

	-- 按钮
	self.DianZanRoot:SetActive(false)
	self.RightBtn:SetActive(false)
	self.MidBtn:SetActive(false)
	self.LeftBtn:SetActive(false)

	-- 常驻按钮
	self.GambleBtn:SetActive(CommonDefs.IS_CN_CLIENT)
	self.RewardBtn:SetActive(true)
	self.RuleBtn:SetActive(true)
	self.ShareBtn:SetActive(false)

	self.GambleAlert:SetActive(CScheduleMgr.Inst:GetAlertState(42000429) == CScheduleMgr.EnumAlertState.Show )

	-- 底部说明
	self.BottomDesc.gameObject:SetActive(index < 7)
	self.BottomDescLabel.gameObject:SetActive(index <= 4)
	self.BottomDescTeam.gameObject:SetActive(index >= 5)

	UIEventListener.Get(self.GambleBtn).onClick = DelegateFactory.VoidDelegate(
		function()
			self.GambleAlert:SetActive(false)
			if CLuaFightingSpiritMgr:HasGambleOpen() then
				CLuaFightingSpiritMgr:OpenGambleWnd(2)
			else
				CLuaFightingSpiritMgr:OpenGambleWnd(1)
			end

			-- 记录当天已经点击
			if CLuaFightingSpiritMgr:HasGambleOpen() then
				local alertStr = "douhun_gamble_alert".. tostring(CClientMainPlayer.Inst.Id)
				local now = CServerTimeMgr.Inst:GetZone8Time()
				local cur = now.Day + now.Month*100
				PlayerPrefs.SetInt(alertStr, cur)
			end

			if CLuaFightingSpiritMgr:HasWenDaOpen() then
				local alertStr = "douhun_wenda_alert".. tostring(CClientMainPlayer.Inst.Id)
				local now = CServerTimeMgr.Inst:GetZone8Time()
				local cur = now.Day + now.Month*100
				PlayerPrefs.SetInt(alertStr, cur)
			end

			CScheduleMgr.Inst:SetAlertState(42000429, CScheduleMgr.EnumAlertState.Clicked, false)
		end
	)

	if index == 1 then
		-- 报名
		if self.m_ExtraInfo then
			local signUpNum = self.m_ExtraInfo["signUpNum"]
			if signUpNum then
				self.BottomDescLabel.text = g_MessageMgr:FormatMessage("DouhunActivitySignUp", signUpNum)
			else 
				self.BottomDesc.gameObject:SetActive(false)
			end
		else
			self.BottomDesc.gameObject:SetActive(false)
		end

		self.MidBtn:SetActive(true)
		self:SetBtnState(self.MidBtn, LocalString.GetString("前往报名"), "common_btn_01_yellow", true)
		UIEventListener.Get(self.MidBtn).onClick = DelegateFactory.VoidDelegate(function()
			-- 寻路NPC
			local data = DouHunTan_Setting.GetData().BaoMingNpc
			CTrackMgr.Inst:FindNPC(data[0], data[1], data[2], data[3], nil, nil)
		end)

	elseif index == 2 then
		-- 投票
		if self.m_ExtraInfo then
			local topTeam = self.m_ExtraInfo["TopTeam"]
			if topTeam then
				self.BottomDescLabel.text = g_MessageMgr:FormatMessage("DouhunActivityVote", self:GetTeamLeaderInfo(topTeam).Name, topTeam[5])
			else
				self.BottomDesc.gameObject:SetActive(false)
			end
		else
			self.BottomDesc.gameObject:SetActive(false)
		end

		self.MidBtn:SetActive(true)
		self:SetBtnState(self.MidBtn, LocalString.GetString("前往投票"), "common_btn_01_yellow", true)
		UIEventListener.Get(self.MidBtn).onClick = DelegateFactory.VoidDelegate(function()
			-- 寻路NPC
			local data = DouHunTan_Setting.GetData().XuanBaNpc
			CTrackMgr.Inst:FindNPC(data[0], data[1], data[2], data[3], nil, nil)
		end)

	elseif index == 3 then
		-- 选拨
		if self.m_ExtraInfo then
			local challengeInfo = self.m_ExtraInfo["ChallengeInfo"]
			if challengeInfo and #challengeInfo >= 4 then
				self.BottomDescLabel.text = g_MessageMgr:FormatMessage("DouhunActivityChallengePrepare", 
					self:GetTeamLeaderInfo(challengeInfo[1]).Name, 
					self:GetTeamLeaderInfo(challengeInfo[2]).Name, 
					self:GetTeamLeaderInfo(challengeInfo[3]).Name, 
					self:GetTeamLeaderInfo(challengeInfo[4]).Name)
			else
				self.BottomDesc.gameObject:SetActive(false)
			end
		else
			self.BottomDesc.gameObject:SetActive(false)
		end

		self.MidBtn:SetActive(true)
		self:SetBtnState(self.MidBtn, LocalString.GetString("前往查看"), "common_btn_01_yellow", true)
		UIEventListener.Get(self.MidBtn).onClick = DelegateFactory.VoidDelegate(function()
			-- 寻路NPC
			local data = DouHunTan_Setting.GetData().XuanBaNpc
			CTrackMgr.Inst:FindNPC(data[0], data[1], data[2], data[3], nil, nil)
		end)

	elseif index == 4 then
		-- 培养
		self.BottomDescLabel.text = g_MessageMgr:FormatMessage("DouhunActivityStartDouHunTrain")
		self.MidBtn:SetActive(true)
		self:SetBtnState(self.MidBtn, LocalString.GetString("前往培养"), "common_btn_01_yellow", true)
		UIEventListener.Get(self.MidBtn).onClick = DelegateFactory.VoidDelegate(function()
			-- 寻路NPC
			local data = DouHunTan_Setting.GetData().PeiYangNpc
			CTrackMgr.Inst:FindNPC(data[0], data[1], data[2], data[3], nil, nil)
		end)
	elseif index == 5 or index == 6 then
		-- 外卡赛 主赛事
		-- 点赞打赏
		self.LeftBtn:SetActive(true)
		self:SetBtnState(self.LeftBtn, LocalString.GetString("点赞打赏"), "common_btn_01_yellow", true)
		UIEventListener.Get(self.LeftBtn).onClick = DelegateFactory.VoidDelegate(function()
			self.DianZanRoot:SetActive(false)
			-- 点赞打赏变成寻路NPC
			CTrackMgr.Inst:FindNPC(20001415, 16000003,134, 50, nil, nil)
		end)

		-- 查看赛况
		self.RightBtn:SetActive(true)
		self:SetBtnState(self.RightBtn, LocalString.GetString("查看赛况"), "common_btn_01_yellow", true)
		UIEventListener.Get(self.RightBtn).onClick = DelegateFactory.VoidDelegate(function()
			-- 赛况界面
			-- 设置打开的主赛事还是外卡赛
			if self.m_CurrentStage == 5 then
				CFightingSpiritMatchRecordWnd.CurrentStage = EnumDouHunCrossStage.eWaiKa
			elseif self.m_CurrentStage == 6 then
				CFightingSpiritMatchRecordWnd.CurrentStage = EnumDouHunCrossStage.eMain
			end

			CUIManager.ShowUI(CLuaUIResources.FightingSpiritMatchInfoWnd)
		end)

		--	本服出战队伍
		if self.m_ExtraInfo then
			local joinCrossInfo = self.m_ExtraInfo["JoinCrossInfo"]
			if joinCrossInfo then
				local left = self.BottomDescTeam:Find("Left")
				local right = self.BottomDescTeam:Find("Right")
				left.gameObject:SetActive(false)
				right.gameObject:SetActive(false)
				if #joinCrossInfo >= 1 then
					local leader = self:GetTeamLeaderInfo(joinCrossInfo[1])
					left.gameObject:SetActive(true)
					left:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadPortrait(leader and CUICommonDef.GetPortraitName(leader.Class, leader.Gender, -1) or "", true)
					left:Find("Server"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerName()..LocalString.GetString("·乾") or ""
					left:Find("Name"):GetComponent(typeof(UILabel)).text = leader.Name..LocalString.GetString("的队伍")
					UIEventListener.Get(left.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
						Gac2Gas.QueryDouHunChlgTeamInfoById(joinCrossInfo[1][1])
					end)
				end
				if #joinCrossInfo >= 2 then
					local leader = self:GetTeamLeaderInfo(joinCrossInfo[2])
					right.gameObject:SetActive(true)
					right:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadPortrait(leader and CUICommonDef.GetPortraitName(leader.Class, leader.Gender, -1) or "", true)
					right:Find("Server"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerName()..LocalString.GetString("·坤") or ""
					right:Find("Name"):GetComponent(typeof(UILabel)).text = leader.Name..LocalString.GetString("的队伍")
					UIEventListener.Get(right.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
						Gac2Gas.QueryDouHunChlgTeamInfoById(joinCrossInfo[2][1])
					end)
				end
			else
				self.BottomDesc.gameObject:SetActive(false)
			end
		else
			self.BottomDesc.gameObject:SetActive(false)
		end
	elseif index == 7 then
		self.MidBtn:SetActive(true)
		-- 膜拜大神
		self:SetBtnState(self.MidBtn, LocalString.GetString("膜拜大神"), "common_btn_01_yellow", true)
		UIEventListener.Get(self.MidBtn).onClick = DelegateFactory.VoidDelegate(function()
			-- 寻路NPC
			local data = DouHunTan_Setting.GetData().MoBaiNpc
			CTrackMgr.Inst:FindNPC(data[0], data[1], data[2], data[3], nil, nil)
		end)

		-- 冠军
		if index == self.m_CurrentStage then
			self.ExpandBtn.gameObject:SetActive(true)
			self.Desc:SetActive(false)
			self.ChampionInfoRoot:SetActive(true)
			-- 发送RPC查询
			Gac2Gas.QueryDouHunCrossChampionGroupInfo(self.ExpandBtn:GetCurrentIndex()+1)
		else
			self.ExpandBtn.gameObject:SetActive(false)
		end

		self.ShareBtn:SetActive(CommonDefs.IS_CN_CLIENT)

		self.ExpandBtn.transform:Find("BG/Scroll View"):GetComponent(typeof(UIPanel)).depth = self.ExpandBtn:GetComponent(typeof(UIPanel)).depth + 1
	end
end

-- 初始化冠军显示界面
function LuaDouHunPage:ShowChampionInfo(level, serverName, leaderId, infoList)
	Extensions.RemoveAllChildren(self.InfoGrid.transform)
	-- 更新冠军Title信息
	local levelName = DouHunCross_Setting.GetData().LevelName
	local setting = DouHunTan_Setting.GetData()
    local season = setting.Season

	self.ChampionTitleLabel.text = SafeStringFormat3(LocalString.GetString("第%s届太一斗魂坛%s冠军"), season, levelName[level-1])
	self.SeverLabel.text = serverName
	
	self.m_GroupList = infoList
	self.m_LeaderId = leaderId

	CLuaFightingSpiritMgr.LevelName = levelName[level-1]
	CLuaFightingSpiritMgr.ServerName = serverName

	local id2Lv = {}

    for i=1, #infoList do
        local g = NGUITools.AddChild(self.InfoGrid.gameObject, self.ItemTemplate)
        g:SetActive(true)
        local info = infoList[i]

		local mark = g.transform:Find("Mark").gameObject
		local icon = g.transform:Find("Icon"):GetComponent(typeof(CUITexture))
		local levelLabel = g.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
		local nameLabel = g.transform:Find("NameLabel"):GetComponent(typeof(UILabel))

		-- 初始化
		nameLabel.color = Color(1,1,1,1)
		nameLabel.text = "lv." .. info.Grade
		
		levelLabel.text = info.Name
		mark:SetActive(leaderId == info.PlayerId)
		id2Lv[info.PlayerId] = info.Grade

		icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.Class, info.Gender, -1))

		local listener = CommonDefs.AddComponent_GameObject_Type(icon.gameObject, typeof(UIEventListener))
		local box = CommonDefs.AddComponent_GameObject_Type(icon.gameObject, typeof(BoxCollider))
		box.size = Vector3(150, 150, 0)

		listener.onClick = DelegateFactory.VoidDelegate(function()
			CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(info.PlayerId), EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, g.transform.position, AlignType1.Default)
		end)
    end

	CLuaFightingSpiritMgr.ChampionID2Lv = id2Lv

    self.InfoGrid:Reposition()
end

function LuaDouHunPage:CheckIfDouHunStart()
	local date = DouHunTan_Cron.GetData("SignUp").Value
	local _, _, mm, hh, dd, MM = string.find(date, "(%d+) (%d+) (%d+) (%d+) *")

	local now = CServerTimeMgr.Inst:GetZone8Time()
	local startTime = CreateFromClass(DateTime, now.Year, MM, dd, hh, mm, 0)

	-- 斗魂已经开始报名 才会发送查询rpc
	if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, startTime) then
		Gac2Gas.QueryDouHunStage()
	end
end

function LuaDouHunPage:OnEnable()
	-- 初始化设置阶段0
	self:SetProgressStage(1)
	self:CheckIfDouHunStart()
	g_ScriptEvent:AddListener("QueryDouHunStageResult", self, "OnStageInfoReceived")
	g_ScriptEvent:AddListener("QueryDouHunCrossChampionGroupInfoResult", self, "ShowChampionInfo")
	g_ScriptEvent:AddListener("QueryDouHunCrossGambleInfoResult", self, "OnDouHunGambleResult")
end

function LuaDouHunPage:OnDisable()
	g_ScriptEvent:RemoveListener("QueryDouHunStageResult", self, "OnStageInfoReceived")
	g_ScriptEvent:RemoveListener("QueryDouHunCrossChampionGroupInfoResult", self, "ShowChampionInfo")
	g_ScriptEvent:RemoveListener("QueryDouHunCrossGambleInfoResult", self, "OnDouHunGambleResult")
end

--@region UIEvent

function LuaDouHunPage:OnDaShangBtnClick()
	self.DianZanRoot:SetActive(false)
	if CClientMainPlayer.Inst then
		CFightingSpiritMgr.Instance.SelfServerID = CClientMainPlayer.Inst:GetMyServerId()
		CUIManager.ShowUI(CUIResources.FightingSpiritRewardWnd)
	end
end

function LuaDouHunPage:OnDianZanBtnClick()
	self.DianZanRoot:SetActive(false)
	-- 请求点赞数据的界面
	Gac2Gas.RequestDouHunPraiseServerList()
end

function LuaDouHunPage:OnRewardBtnClick()
	-- 待制作
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritRewardShowWnd)
end

function LuaDouHunPage:OnRuleBtnClick()
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritRuleWnd)
end

function LuaDouHunPage:OnDescLabelClick()
	local url = self.DescLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
	if url ~= nil then
		CChatLinkMgr.ProcessLinkClick(url, nil)
	end
end


function LuaDouHunPage:OnShareBtnClick()
	if self.m_GroupList and self.m_LeaderId then
		local idList = {}
		for _, info in ipairs(self.m_GroupList) do
			table.insert(idList, info.PlayerId)
		end

		Gac2Gas.QueryDouHunGroupAppearanceInfo(g_MessagePack.pack(idList), self.m_LeaderId)
	end
end


--@endregion UIEvent

function LuaDouHunPage:OnGiftPackBtnClick()
	CShopMallMgr.ShowLinyuShoppingMall(21001784)
end

function LuaDouHunPage:GetTeamLeaderInfo(teamInfo)
	local memberList = teamInfo[3]
	local leaderId = teamInfo[4]
	for i = 1, #memberList do
		local member = CreateFromClass(CGroupMemberInfo)
		local len = string.len(memberList[i])
		local bytes = CreateFromClass(MakeArrayClass(Byte), len)
		for j = 1, len do
			bytes[j - 1] = string.byte(memberList[i], j)
		end
		member:LoadFromString(bytes)
		if member.PlayerId == leaderId then
			return member
		end
	end
	return {Name=""}
end
