local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UISlider = import "UISlider"
local QnButton = import "L10.UI.QnButton"

LuaZuanMuQuHuoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZuanMuQuHuoWnd, "TimeLeftLabel", "TimeLeftLabel", UILabel)
RegistChildComponent(LuaZuanMuQuHuoWnd, "CloseButton", "CloseButton", CButton)
RegistChildComponent(LuaZuanMuQuHuoWnd, "Common", "Common", GameObject)
RegistChildComponent(LuaZuanMuQuHuoWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaZuanMuQuHuoWnd, "Faild", "Faild", GameObject)
RegistChildComponent(LuaZuanMuQuHuoWnd, "Progress", "Progress", UISlider)
RegistChildComponent(LuaZuanMuQuHuoWnd, "rightfx", "rightfx", GameObject)
RegistChildComponent(LuaZuanMuQuHuoWnd, "wrongfx", "wrongfx", GameObject)
RegistChildComponent(LuaZuanMuQuHuoWnd, "LeftBtn", "LeftBtn", QnButton)
RegistChildComponent(LuaZuanMuQuHuoWnd, "RightBtn", "RightBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaZuanMuQuHuoWnd, "m_Id")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_TotalProgress")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_AddProgress")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_MissProgress")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_ClickTime")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_TotalTime")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_CurLeftTime")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_CurrentProgress")

RegistClassMember(LuaZuanMuQuHuoWnd, "m_InGame")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_WaiteForClickTick")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_CountDownTick")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_ShowResultTick")
RegistClassMember(LuaZuanMuQuHuoWnd, "m_IsLeft")
function LuaZuanMuQuHuoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


	
	UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)


	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
	self.m_InGame = false
	self.m_IsLeft = false
	self.m_WaiteForClickTick = nil
	self.m_CountDownTick = nil
	self.m_ShowResultTick = nil
end

function LuaZuanMuQuHuoWnd:Init()
	local id = LuaZhuJueJuQingMgr.m_MakeFirePlayId
	local data = ZhuJueJuQing_MakeFirePlay.GetData(id)
	if not id then return end
	self.m_Id = id
	self.m_TotalProgress = data.TotalProgress
	self.m_AddProgress = data.AddProgress
	self.m_MissProgress = data.MissProgress
	self.m_ClickTime = data.ClickTime
	self.m_TotalTime = data.PlayTime

	self:InitView()
	self:BeginGame()
end

function LuaZuanMuQuHuoWnd:InitView()
	self.Common:SetActive(true)
	self.Success:SetActive(false)
	self.Faild:SetActive(false)
	self.Progress.value = 0
	self.rightfx:SetActive(false)
	self.wrongfx:SetActive(false)
	self.TimeLeftLabel.text = nil
	self.m_InGame = false

end

function LuaZuanMuQuHuoWnd:BeginGame()
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	self.m_IsInGame = true
	self.m_CurLeftTime = self.m_TotalTime
	self.m_CurrentProgress = 0
	self.TimeLeftLabel.text = self:GetRemainTimeText(self.m_CurLeftTime)
	self.LeftBtn.gameObject:SetActive(true)
	self.RightBtn.gameObject:SetActive(true)
	self:UpdateProgress()
	self.m_IsLeft = false

	self.m_CountDownTick = RegisterTickWithDuration(function()
		self.m_CurLeftTime = self.m_CurLeftTime - 1

		if self.m_CurLeftTime <= 0 then
			self.m_CurLeftTime = 0
			self:OnTimeOut()
		end
		self.TimeLeftLabel.text = self:GetRemainTimeText(self.m_CurLeftTime)
	end,1000, (self.m_TotalTime + 1) * 1000)
	self:StartBtnClick()
end

function LuaZuanMuQuHuoWnd:StartBtnClick()
	if self.m_WaiteForClickTick then UnRegisterTick(self.m_WaiteForClickTick) self.m_WaiteForClickTick = nil end
	self.m_IsLeft = not self.m_IsLeft
	self:SetBtnActick(self.LeftBtn, self.m_IsLeft)
	self:SetBtnActick(self.RightBtn, not self.m_IsLeft)
	self.m_WaiteForClickTick = RegisterTickOnce(function()
		self:OnMiss()
	end, self.m_ClickTime * 1000)

end

function LuaZuanMuQuHuoWnd:SetBtnActick(btn, active)
	Extensions.SetLocalPositionZ(btn.transform, active and 0 or -1)
end

function LuaZuanMuQuHuoWnd:OnMiss()
	if self.m_WaiteForClickTick then UnRegisterTick(self.m_WaiteForClickTick) self.m_WaiteForClickTick = nil end
	self.m_CurrentProgress = self.m_CurrentProgress - self.m_MissProgress
	self.m_CurrentProgress = math.max(self.m_CurrentProgress, 0)
	self.wrongfx.gameObject:SetActive(true)
	self.rightfx.gameObject:SetActive(false)
	g_MessageMgr:ShowMessage("ZhanMuQuHuo_Miss")
	self:UpdateProgress()
	self:StartBtnClick()
end

function LuaZuanMuQuHuoWnd:UpdateProgress()
	self.Progress.value = self.m_CurrentProgress / self.m_TotalProgress
end

function LuaZuanMuQuHuoWnd:OnSuccess()
	if self.m_WaiteForClickTick then UnRegisterTick(self.m_WaiteForClickTick) self.m_WaiteForClickTick = nil end
	self.m_CurrentProgress = self.m_CurrentProgress + self.m_AddProgress
	self.wrongfx.gameObject:SetActive(false)
	self.rightfx.gameObject:SetActive(true)
	self:UpdateProgress()
	if self.m_CurrentProgress >= self.m_TotalProgress then
		self:PlaySuccess()
	else
		self:StartBtnClick()
	end
end

function LuaZuanMuQuHuoWnd:OnTimeOut()
	self.m_IsInGame = false
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_shibai.prefab"})
	self.Common.gameObject:SetActive(false)
	self.Success.gameObject:SetActive(false)
	self.Faild.gameObject:SetActive(true)

	if self.m_ShowResultTick then UnRegisterTick(self.m_ShowResultTick) self.m_ShowResultTick = nil end
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	if self.m_WaiteForClickTick then UnRegisterTick(self.m_WaiteForClickTick) self.m_WaiteForClickTick = nil end
	self.m_ShowResultTick = RegisterTickOnce(function()
		g_MessageMgr:ShowMessage("ZhanMuQuHuo_Failed")
		CUIManager.CloseUI(CLuaUIResources.ZuanMuQuHuoWnd)
	end, 1000)
end

function LuaZuanMuQuHuoWnd:PlaySuccess()
	self.m_IsInGame = false
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_wancheng.prefab"})
	self.Common.gameObject:SetActive(false)
	self.Success.gameObject:SetActive(true)
	self.Faild.gameObject:SetActive(false)
	self.LeftBtn.gameObject:SetActive(false)
	self.RightBtn.gameObject:SetActive(false)
	self.TimeLeftLabel.text = nil

	if self.m_ShowResultTick then UnRegisterTick(self.m_ShowResultTick) self.m_ShowResultTick = nil end
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	if self.m_WaiteForClickTick then UnRegisterTick(self.m_WaiteForClickTick) self.m_WaiteForClickTick = nil end
	self.m_ShowResultTick = RegisterTickOnce(function()
		CUIManager.CloseUI(CLuaUIResources.ZuanMuQuHuoWnd)
	end, 2000)
end


function LuaZuanMuQuHuoWnd:GetRemainTimeText(totalSeconds)
	if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
	elseif totalSeconds <= 0 then
		return SafeStringFormat3("[FF0000]00:00[-]")
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

--@region UIEvent

function LuaZuanMuQuHuoWnd:OnCloseButtonClick()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LEAVE_COPY_CONFIRM"), function ()
		Gac2Gas.RequestLeavePlay()
		CUIManager.CloseUI(CLuaUIResources.ZuanMuQuHuoWnd)
	end,nil, nil, nil, false)
end

function LuaZuanMuQuHuoWnd:OnLeftBtnClick()
	if not self.m_IsInGame then return end
	if self.m_IsLeft then
		self:OnSuccess()
	else
		self:OnMiss()
	end
end

function LuaZuanMuQuHuoWnd:OnRightBtnClick()
	if not self.m_IsInGame then return end
	if self.m_IsLeft then
		self:OnMiss()
	else
		self:OnSuccess()
	end
end



--@endregion UIEvent

function LuaZuanMuQuHuoWnd:OnDisable()
	if self.m_ShowResultTick then UnRegisterTick(self.m_ShowResultTick) self.m_ShowResultTick = nil end
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	if self.m_WaiteForClickTick then UnRegisterTick(self.m_WaiteForClickTick) self.m_WaiteForClickTick = nil end
end