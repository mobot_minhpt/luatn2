local QnCheckBox = import "L10.UI.QnCheckBox"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local GameplayItem_Setting = import "L10.Game.GameplayItem_Setting"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local UITexture = import "UITexture"
local NGUIText = import "NGUIText"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local Ease = import "DG.Tweening.Ease"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaTianJiangBaoXiangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTianJiangBaoXiangWnd, "FirstWnd", "FirstWnd", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "SecondWnd", "SecondWnd", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "Door", "Door", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "LimitInfoTip", "LimitInfoTip", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "KeyIcon", "KeyIcon", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "SwitchToRed", "SwitchToRed", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "SwitchToGreen", "SwitchToGreen", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "RuleTipBtn", "RuleTipBtn", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "RefreshBtn", "RefreshBtn", CButton)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "UnActiveLabel", "UnActiveLabel", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "ActiveInfo", "ActiveInfo", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "BossTexture", "BossTexture", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "RedDoor", "RedDoor", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "GreenDoor", "GreenDoor", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "BaoXiang", "BaoXiang", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "BossEntrance", "BossEntrance", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "TimeTick", "TimeTick", UILabel)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "ConfirmBtn", "ConfirmBtn", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "PopupNoticeWnd", "PopupNoticeWnd", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "ShowItem", "ShowItem", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "ShowBossTexture", "ShowBossTexture", GameObject)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "Checkbox", "Checkbox", QnCheckBox)
RegistChildComponent(LuaTianJiangBaoXiangWnd, "Checkbox2", "Checkbox2", QnCheckBox)

--@endregion RegistChildComponent end
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_NowStep") -- 当前界面阶段
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_HasInitFirstWnd") -- 是否初始化过一级界面
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_HasInitSecondWnd")    -- 是否初始化过二级界面
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_SelectDoor")  -- 当前选择门
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_SwitchDoor")  -- 当前第一界面切换的门
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_IsInSwitch")    -- 按钮切换门时不允许滑动切换
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_SelectItemIndex")    -- 选择的奖励Index
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_SelectItemList")      -- 可选奖励列表
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_RewardBossNeedTimes")     -- Boss副本需要的开门次数
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_RewardItemId")        -- 获得奖励的道具ID

RegistClassMember(LuaTianJiangBaoXiangWnd, "m_BG")  -- 背景屏幕区域，用于绑定各种交互
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_BGs") -- 二级界面的红色和绿色背景
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_LightList")   -- 两扇门的灯
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_Doors")       -- 两扇门
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_DoorsTexture")   -- 左右片门,用于开关门动画
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_DoorUnActiveMask")  -- 两扇门的遮盖
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_RedKey")      -- 赤铜钥匙信息
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_GreenKey")    -- 翡翠钥匙信息
-- 动画相关
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_Tick")
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_NowAnimType")         -- 当前动画类型
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_NowAnimTransList")    -- 记录当前存在的动画，便于跳过动画时清除已有的tween动画
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_PosX") -- 另一个门的最大x坐标位置
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_AnotherDoorScale") -- 另一扇门的放缩
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_EnlargeScale")    -- 镜头拉近时门的放缩
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_HalfDoorOpen")    -- 半开门的程度
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_ComplateDoorOpen")    -- 全开门的程度
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_Angles")  -- 两扇门相对角度
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_MaskAlpha")   -- 门遮盖透明度   
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_SwitchDoorTime")      -- 切换门时的时间花费
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_OpenDoorAnimTime")    -- 开门动画时间花费
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_ShowRewardAnimTime")  -- 显示奖品动画时间
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_ChooseRewardAnimTime")-- 选择奖励后动画时间
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_RefreshRewardAnimTime")-- 刷新奖励动画时间 
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_fadeoutSecondWndTick")    -- 二级界面渐隐的tick

RegistClassMember(LuaTianJiangBaoXiangWnd, "m_HasShowRewardNotice") -- 判断是否显示过获得道具提示，用于跳过动画时的判断
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_PopUpNoticeTick") 

RegistClassMember(LuaTianJiangBaoXiangWnd, "m_CountdownTick")    -- 玩法倒计时计时器
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_hasShowCountdown")    -- 显示倒计时
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_EnableAutoOpen")    -- 是否勾选自动开门
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_EnableAutoOpen2")    -- 是否勾选灵玉自动开门
RegistClassMember(LuaTianJiangBaoXiangWnd, "m_ctLabels")    -- 是否勾选自动开门

-- 动画类型
EnumTianJiangBaoXiangAnimType = {
    None = 0,       --无动画 
    OpenDoor = 1,   -- 开门动画
    ShowReward = 2, -- 弹出奖励动画
    ChooseReward = 3, -- 选择奖励后动画
    RefreshReward = 4, -- 刷新奖励动画
}
--[[
    选择宝箱时，获得道具的提示延迟显示开关
    默认为true，也就是隐藏原本的道具提示，使用临时提示在动画播放过程中显示
    为false时，原有的道具获得提示不隐藏，在玩家选择道具时即跳出提示

    为true时可能出现的问题： 
    1. 由于只显示获得一个道具,无法处理玩家一次获得多个道具的情况
    2. 当该Wnd下的PopupNotice正在显示且原PopupNoticeWnd处于显示状态时,关闭界面可能会出现两个Notice动画不同步的现象
    3. 打开界面的状态下，无法显示其他道具获得时的提示
]]
function LuaTianJiangBaoXiangWnd:EnableShowTempPopupNotice()
    return true
end

function LuaTianJiangBaoXiangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.Checkbox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
	    self:OnCheckboxValueChanged(selected)
	end)

    self.Checkbox2.OnValueChanged = DelegateFactory.Action_bool(function (selected)
	    self:OnCheckbox2ValueChanged(selected)
	end)

    --@endregion EventBind end

    self.Checkbox:SetSelected(false,true)
    self.Checkbox2:SetSelected(false,true)
end

function LuaTianJiangBaoXiangWnd:InitData()
    -- 一些值初始化
    self.m_IsInSwitch = false
    self.m_HasInitFirstWnd = false
    self.m_HasInitSecondWnd = false
    self.m_hasShowCountdown = false
    self.m_Tick = nil
    self.m_PopUpNoticeTick = nil
    self.m_CountdownTick = nil
    self.m_fadeoutSecondWndTick = nil
    self.m_Angles = {}
    self.m_LightList = {}
    self.m_Doors = {}
    self.m_DoorsTexture = {}
    self.m_DoorUnActiveMask = {}
    self.m_SelectItemList = {}
    self.m_NowAnimTransList = {}
    self.m_SelectItemIndex = 0
    self.m_EnableAutoOpen = false
    self.m_EnableAutoOpen2 = false

    self:InitDataList()
    self:InitColorData()
    ---- 以下是动画用到的参数 ----
    self.m_PosX = 470   -- 赤铜门x坐标最大值，翡翠门取相反数
    self.m_AnotherDoorScale = 0.7   -- 另一扇门的放缩
    self.m_EnlargeScale = 1.2       -- 镜头拉近(放大倍数)
    self.m_MaskAlpha = 1
    self.m_SwitchDoorTime = 0.3     -- 切换门花费时间 
    self.m_HalfDoorOpen = 0.66       -- 门半开的程度
    self.m_ComplateDoorOpen = 0.2   -- 门全开的程度
    -- 一些动画时间
    self.m_OpenDoorAnimTime =  1
    self.m_ShowRewardAnimTime = 1
    self.m_ChooseRewardAnimTime = 2.5
    self.m_RefreshRewardAnimTime = 0.9
    self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.None
end

-- 缓存一下有关数据
function LuaTianJiangBaoXiangWnd:InitDataList()
    self.m_BG = self.Door.transform:Find("BG"):GetComponent(typeof(UITexture))
    self.m_BGs = {}
    local red = EnumTianJiangBaoXiangDoor.ChiTong
    local green = EnumTianJiangBaoXiangDoor.FeiCui
    self.m_BGs[green] = self.Door.transform:Find("GreenBG")
    self.m_BGs[red] = self.Door.transform:Find("RedBG")
    self.KeyMask = self.KeyIcon.transform:Find("NotEnough").gameObject
    self.m_Doors[green] = self.GreenDoor
    self.m_Doors[red] = self.RedDoor
    for i,v in pairs(self.m_Doors) do
        self.m_DoorUnActiveMask[i] = v.transform:Find("UnActiveMask"):GetComponent(typeof(UITexture))
        self.m_DoorsTexture[i] = {}
        self.m_DoorsTexture[i][1] = v.transform:Find("LeftDoor")
        self.m_DoorsTexture[i][2] = v.transform:Find("RightDoor")
        self.m_DoorsTexture[i][3] = self.m_DoorUnActiveMask[i].transform:Find("LeftDoor"):GetComponent(typeof(UITexture))
        self.m_DoorsTexture[i][4] = self.m_DoorUnActiveMask[i].transform:Find("RightDoor"):GetComponent(typeof(UITexture))
    end

    local cfg = GameplayItem_Setting.GetData()
    self.m_RewardBossNeedTimes = {}
    self.m_RewardBossNeedTimes[red] = cfg.TianJiangBaoXiang_BossDoorTimes_ChiTong
    self.m_RewardBossNeedTimes[green] = cfg.TianJiangBaoXiang_BossDoorTimes_FeiCui

    local redKeyId = cfg.TianJiangBaoXiang_ChiTong_ItemId
    local greenKeyId = cfg.TianJiangBaoXiang_FeiCui_ItemId
    self.m_RedKey = Item_Item.GetData(redKeyId)
    self.m_GreenKey = Item_Item.GetData(greenKeyId)

end
-- 初始化一些文本颜色数据
function LuaTianJiangBaoXiangWnd:InitColorData()
    local red = EnumTianJiangBaoXiangDoor.ChiTong
    local green = EnumTianJiangBaoXiangDoor.FeiCui
    self.TextColor = {}
    -- 1. BossEntrance->UnActiveLabel 前半句 2. BossEntrance->UnActiveLabel 后半句/BossEntrance->ActiveLabel 3. BottomLabel 前半句 4. BottomLabel 后半句
    self.TextColor[red] = {"f4c19f","58320e","fa8657","ab9b8c"} 
    self.TextColor[green] = {"acfafd","19444a","97fbd7","ab9b8c"}
end
function LuaTianJiangBaoXiangWnd:Init()
    CShopMallMgr.Init()
    Gac2Gas.RequestPlayerItemLimit(EShopMallRegion_lua.ELingyuMallLimit)
    self:InitData()
    -- 初始化两扇门
    self:InitDoors()
    self.FirstWnd:SetActive(false)
    self.SecondWnd:SetActive(false)
    self.TimeTick.gameObject:SetActive(false)
    self.PopupNoticeWnd.gameObject:SetActive(false)
    self.m_NowStep = LuaTianJiangBaoXiangMgr.NowStep
    -- 根据服务器消息打开一级界面或二级界面
    if LuaTianJiangBaoXiangMgr.NowStep == 1 then
        self:InitFirstWnd()
    else
        self:InitSecondWnd()
    end
    self:UpdateLightInfo()
end
-- 检查玩法倒计时
-- 有性能问题！！！
function LuaTianJiangBaoXiangWnd:Update()
    if LuaTianJiangBaoXiangMgr.StartPlayTimeStamp == 0 or self.m_hasShowCountdown then return end
    local nowTime = CServerTimeMgr.Inst.timeStamp 
    local LeftTime = LuaTianJiangBaoXiangMgr.StartPlayTimeStamp + LuaTianJiangBaoXiangMgr.PlayOpenTime - nowTime
    if LeftTime < 0 or LeftTime > LuaTianJiangBaoXiangMgr.ShowTimeTickTime then return end
    self:ShowCountDown(LeftTime)
end
-- 显示玩法倒计时
function LuaTianJiangBaoXiangWnd:ShowCountDown(leftTime)
    if self.m_CountdownTick then
        UnRegisterTick(self.m_CountdownTick)
        self.m_CountdownTick = nil
    end
    local UnActivePrecent = 2 --  闪烁效果，要求一秒内，时间的显示效果为 显示：隐藏 = 8：2
    local LeftSeconds = leftTime
    local seconds = LeftSeconds % 60 
    local minutes = (LeftSeconds - seconds) / 60
    local count = 1
    self.TimeTick.text = SafeStringFormat3("%02d:%02d",minutes,seconds)
    local CountDown = function ()
        if count == 1 then
            LeftSeconds = LeftSeconds - 1
            self.TimeTick.text = ""
        elseif count == UnActivePrecent then
            local seconds = LeftSeconds % 60 
            local minutes = (LeftSeconds - seconds) / 60
            self.TimeTick.text = SafeStringFormat3("%02d:%02d",minutes,seconds)
        end
        if LeftSeconds < 0 then -- 玩法结束，关一下界面
            UnRegisterTick(self.m_CountdownTick)
            self.m_CountdownTick = nil
            LuaTianJiangBaoXiangMgr.OnDisconnect() -- 重置下数据
            CUIManager.CloseUI(CLuaUIResources.TianJiangBaoXiangWnd)
        end 
        count = count + 1 > 10 and 1 or count + 1
    end
    self.TimeTick.gameObject:SetActive(true)
    self.m_hasShowCountdown = true
    self.LimitInfoTip.gameObject:SetActive(false)
    local fx = self.TimeTick.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    fx:DestroyFx()
    --fx:LoadFx("fx/ui/prefab/UI_tjbx_anniushaoguang001.prefab")
    self.m_CountdownTick = RegisterTick(CountDown,100)
end

function LuaTianJiangBaoXiangWnd:GetLightCount(type)
    return self.m_RewardBossNeedTimes[type]
end

-- 初始化两扇门
function LuaTianJiangBaoXiangWnd:InitDoors()
    self.m_ctLabels = {}
    for i,v in pairs(self.m_DoorUnActiveMask) do
        v.gameObject:SetActive(true)
        self.m_DoorsTexture[i][3].gameObject:SetActive(true)
        self.m_DoorsTexture[i][4].gameObject:SetActive(true)
    end
    self.GreenDoor:SetActive(true)
    self.RedDoor:SetActive(true)
    -- 初始化灯
    local red = EnumTianJiangBaoXiangDoor.ChiTong
    local green = EnumTianJiangBaoXiangDoor.FeiCui 
    self.m_LightList[green] = {}
    self.m_LightList[red] = {}

    local redtrans = self.RedDoor.transform
    local greentrans = self.GreenDoor.transform
    self.m_ctLabels[red] = redtrans:Find("DoorInfo/CountLabel"):GetComponent(typeof(UILabel))
    self.m_ctLabels[green] = greentrans:Find("DoorInfo/CountLabel"):GetComponent(typeof(UILabel))
    
    local RedDoorLight = redtrans:Find("DoorInfo/Light")
    local GreenDoorLight = greentrans:Find("DoorInfo/Light")
    self:InitTypeDoors(green,GreenDoorLight)
    self:InitTypeDoors(red,RedDoorLight)
end

function LuaTianJiangBaoXiangWnd:InitTypeDoors(type,root)
    local lightcount = self:GetLightCount(type)
    for i=1,lightcount do
        local child = root.transform:GetChild(i - 1)
        if not child  then return end
        child:Find("Active").gameObject:SetActive(false)
        child.gameObject:SetActive(true)
        self.m_LightList[type][i] = child
    end
    
    if self.m_ctLabels[type] then
        self.m_ctLabels[type].text = "0/"..lightcount
    end
end

function LuaTianJiangBaoXiangWnd:GetKeyBuyCount()
    local isChiTong = self.m_SwitchDoor == EnumTianJiangBaoXiangDoor.ChiTong
    local citem = isChiTong and self.m_RedKey or self.m_GreenKey
    local limitCount = CShopMallMgr.Inst:GetLimitCount(citem.ID)
    return limitCount
end

function LuaTianJiangBaoXiangWnd:IsKeyEnough()
    local isChiTong = self.m_SwitchDoor == EnumTianJiangBaoXiangDoor.ChiTong
    local citem = isChiTong and self.m_RedKey or self.m_GreenKey
    local KeyAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, citem.ID)
    local needCount = LuaTianJiangBaoXiangMgr.KeyNum[self.m_SwitchDoor]
    if needCount == nil then return true end
    return KeyAmount >= needCount
end

-- 一级界面
function LuaTianJiangBaoXiangWnd:InitFirstWnd()
    self.m_NowStep = 1
    self.m_SwitchDoor = LuaTianJiangBaoXiangMgr.ShowTheFirstDoor    -- 一级界面默认切换到的门
    self.m_SelectDoor = EnumTianJiangBaoXiangDoor.None    -- 一级界面未选择门

    -- 交互绑定，只绑定一次
    if not self.m_HasInitFirstWnd then
        -- 门的交互绑定
        for i,v in pairs(self.m_Doors) do
            UIEventListener.Get(v.gameObject).onClick = DelegateFactory.VoidDelegate(function()
                if self.m_SwitchDoor == i or self.m_IsInSwitch or self.m_NowStep == 2 then return end
                self:SetSwitchDoorDepth(i)
                self:SwitchToDoor(true)
            end)
            UIEventListener.Get(v.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
                if self.m_IsInSwitch or self.m_NowStep == 2 then return end
                self:OnDrag(g,delta)
            end)
            UIEventListener.Get(v.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
                if self.m_IsInSwitch or self.m_NowStep == 2 then return end
                self:OnDragEnd(g)
            end)
            UIEventListener.Get(v.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
                if self.m_IsInSwitch or self.m_NowStep == 2 then return end
                self:OnDragStart(g)
            end)
        end
        -- 屏幕滑动切换门
        UIEventListener.Get(self.m_BG.gameObject).onDrag = DelegateFactory.VectorDelegate(function(g,delta)
            if self.m_IsInSwitch or self.m_NowStep == 2 then return end
            self:OnDrag(g,delta)
        end)
        UIEventListener.Get(self.m_BG.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
            if self.m_IsInSwitch or self.m_NowStep == 2 then return end
            self:OnDragEnd(g)
        end)
        UIEventListener.Get(self.m_BG.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
            if self.m_IsInSwitch or self.m_NowStep == 2 then return end
            self:OnDragStart(g)
        end)
    
        -- 切换按钮事件绑定
        UIEventListener.Get(self.SwitchToRed.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:SetSwitchDoorDepth(EnumTianJiangBaoXiangDoor.ChiTong)
            self:SwitchToDoor(true)
        end)
        UIEventListener.Get(self.SwitchToGreen.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            self:SetSwitchDoorDepth(EnumTianJiangBaoXiangDoor.FeiCui)
            self:SwitchToDoor(true)
        end)

        -- 进入按钮
        UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            local isChiTong = self.m_SwitchDoor == EnumTianJiangBaoXiangDoor.ChiTong
            local citem = isChiTong and self.m_RedKey or self.m_GreenKey
            local KeyAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, citem.ID)
            local needCount = LuaTianJiangBaoXiangMgr.KeyNum[self.m_SwitchDoor]
            if KeyAmount >= needCount then
                Gac2Gas.TianJiangBaoXiang_SelectDoor(self.m_SwitchDoor,false)
            else
                local limitCount = CShopMallMgr.Inst:GetLimitCount(citem.ID)
                local limitstr = tostring(limitCount)
                if limitCount == 0 then
                    limitstr = "[ff0000]"..limitstr.."[-]"
                end
                local msg = g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_CONFIRM_BUY_ITEM",needCount-KeyAmount,citem.Name,limitstr)
                local tempdata = Mall_LingYuMallLimit.GetData(citem.ID)
                local func = DelegateFactory.Action(function ()
                    Gac2Gas.TianJiangBaoXiang_SelectDoor(self.m_SwitchDoor,true)
                end)
                QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.LingYu, msg, tempdata.Jade, func, nil, false, true, EnumPlayScoreKey.NONE, false)
            end
        end)
        -- 说明Tip
        UIEventListener.Get(self.RuleTipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            local imagePaths = {
                "UI/Texture/NonTransparent/Material/tianjiangbaoxiang_yindao_01.mat",
                "UI/Texture/NonTransparent/Material/tianjiangbaoxiang_yindao_02.mat",
                "UI/Texture/NonTransparent/Material/tianjiangbaoxiang_yindao_03.mat",
            }
            local msgs = {
                g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_RULE_01"),
                g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_RULE_02"),
                g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_RULE_03"),
            }
            LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
        end)
        -- 剩余钥匙
        UIEventListener.Get(self.KeyIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            local itemId = self.m_SwitchDoor == EnumTianJiangBaoXiangDoor.ChiTong and self.m_RedKey.ID or self.m_GreenKey.ID
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
            end)
        self.m_HasInitFirstWnd = true
    end
    -- 一级界面门是关的
    for i,v in pairs(self.m_DoorsTexture) do
        LuaUtils.SetLocalScale(v[1].transform,1,1,1)
        LuaUtils.SetLocalScale(v[2].transform,1,1,1)
        self.m_Doors[i].transform:GetComponent(typeof(UITexture)).alpha = 1
    end
    self.m_BG.alpha = 1 -- 背景是显示的
    -- 切换到默认门
    LuaUtils.SetLocalScale(self.Door.transform,1,1,1)
    self:SetSwitchDoorDepth(self.m_SwitchDoor)
    self:SwitchToDoor(false)
    self:ShowFirstWndUI()
    -- 灯信息更新
    self:UpdateLightInfo()
end

function LuaTianJiangBaoXiangWnd:OnDrag(g,delta)
    self.FirstWnd:SetActive(false)
    self.SecondWnd:SetActive(false)
    local green = EnumTianJiangBaoXiangDoor.FeiCui
    local red = EnumTianJiangBaoXiangDoor.ChiTong
    local moveDeg = 57.2958*math.asin(math.max(-1,math.min(delta.x/400,1)))
    local res1 = self.m_Angles[green] + moveDeg
    local res2 = self.m_Angles[red] + moveDeg
    if res1 < -90 or res1 > 0 or res2 < 0 or res2 > 90 then return end
    self.m_Angles[green] = res1
    self.m_Angles[red] = res2

    self:UpdateDoorPosition()
end

function LuaTianJiangBaoXiangWnd:OnDragEnd(g)
    self:SwitchToDoor(true)
end

function LuaTianJiangBaoXiangWnd:OnDragStart(g)
    self.FirstWnd:SetActive(false)
    self.SecondWnd:SetActive(false)
end
-- 拖拽中更新门显示
function LuaTianJiangBaoXiangWnd:UpdateDoorPosition()
    local scaleInfo = {}
    for i,v in pairs(self.m_Doors) do
        local angle = self.m_Angles[i]
        local offset = math.sin(0.0174533*angle)*self.m_PosX
        local scale = math.cos(0.0174533*angle)*(1-self.m_AnotherDoorScale) + self.m_AnotherDoorScale
        local alpha = 0
        if self.m_AnotherDoorScale ~= 1 then
            alpha = (1 - scale) * self.m_MaskAlpha / (1 - self.m_AnotherDoorScale )
        end
        scaleInfo[i] = scale
        LuaUtils.SetLocalPositionX(v.transform,offset)
        LuaUtils.SetLocalScale(v.transform,scale,scale,1)
        self.m_DoorUnActiveMask[i].alpha = alpha
        self.m_DoorsTexture[i][3].alpha = alpha
        self.m_DoorsTexture[i][4].alpha = alpha
    end
    local green = EnumTianJiangBaoXiangDoor.FeiCui
    local red = EnumTianJiangBaoXiangDoor.ChiTong
    if scaleInfo[green] > scaleInfo[red] and self.m_SwitchDoor ~= green then
        self:SetSwitchDoorDepth(green)
    elseif scaleInfo[green] < scaleInfo[red] and self.m_SwitchDoor ~= red then
        self:SetSwitchDoorDepth(red)
    end
end
-- 设置切换门的图层深度
function LuaTianJiangBaoXiangWnd:SetSwitchDoorDepth(door)
    self.m_SwitchDoor = door
    for i,v in pairs(self.m_Doors) do
        if i == door then
            self.m_Doors[i]:GetComponent(typeof(UITexture)).depth = 5
        else
            self.m_Doors[i]:GetComponent(typeof(UITexture)).depth = 4
        end
    end
end
-- 切换门,NeedShowAnim为true时显示动画，并在结束时显示一级界面UI
function LuaTianJiangBaoXiangWnd:SwitchToDoor(NeedShowAnim)
    self.m_IsInSwitch = true
    self.FirstWnd:SetActive(false)

    self.RedDoor.gameObject:SetActive(true)
    self.GreenDoor.gameObject:SetActive(true)
    local green = EnumTianJiangBaoXiangDoor.FeiCui
    local red = EnumTianJiangBaoXiangDoor.ChiTong

    self.m_Angles[green] = self.m_SwitchDoor == green and 0 or -90
    self.m_Angles[red] = self.m_SwitchDoor == red and 0 or 90

    for i,v in pairs(self.m_Doors) do
        local angle = self.m_Angles[i]
        local offset = math.sin(0.0174533*angle)*self.m_PosX
        local scale = math.cos(0.0174533*angle)*(1-self.m_AnotherDoorScale) + self.m_AnotherDoorScale
        local alpha = 0
        if self.m_AnotherDoorScale ~= 1 then
            alpha = (1 - scale) * self.m_MaskAlpha / (1 - self.m_AnotherDoorScale )
        end
        if NeedShowAnim then
            LuaTweenUtils.TweenPositionX(v.transform,offset,self.m_SwitchDoorTime)  -- 位置
            LuaTweenUtils.TweenScaleTo(v.transform,Vector3(scale,scale,1),self.m_SwitchDoorTime)    -- 放缩
            -- 遮盖
            LuaTweenUtils.TweenAlpha(self.m_DoorUnActiveMask[i],self.m_DoorUnActiveMask[i].alpha,alpha,self.m_SwitchDoorTime)
            LuaTweenUtils.TweenAlpha(self.m_DoorsTexture[i][3],self.m_DoorsTexture[i][3].alpha,alpha,self.m_SwitchDoorTime)
            LuaTweenUtils.TweenAlpha(self.m_DoorsTexture[i][4],self.m_DoorsTexture[i][3].alpha,alpha,self.m_SwitchDoorTime)
        else
            LuaUtils.SetLocalPositionX(v.transform,offset)
            LuaUtils.SetLocalScale(v.transform,scale,scale,1)
            self.m_DoorUnActiveMask[i].alpha = alpha
            self.m_DoorsTexture[i][3].alpha = alpha
            self.m_DoorsTexture[i][4].alpha = alpha
        end
    end
    if not NeedShowAnim then self.m_IsInSwitch = false return end
    -- 动画结束后
    local onFinish = function()
        self.m_IsInSwitch = false
        self:ShowFirstWndUI()
    end
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTickOnce(onFinish,self.m_SwitchDoorTime*1000)
end
-- 显示一级界面UI
function LuaTianJiangBaoXiangWnd:ShowFirstWndUI()
    local isChiTong = self.m_SwitchDoor == EnumTianJiangBaoXiangDoor.ChiTong
    local item = isChiTong and self.m_RedKey or self.m_GreenKey
    self.LimitInfoTip.gameObject:SetActive(LuaTianJiangBaoXiangMgr.ShowLimitInfo and not self.m_hasShowCountdown)
    local LimitInfoTipFx = self.LimitInfoTip.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    LimitInfoTipFx:DestroyFx()
    LimitInfoTipFx:LoadFx("fx/ui/prefab/UI_tjbx_anniushaoguang002.prefab")
    -- 切换按钮
    self.SwitchToRed:SetActive(not isChiTong)
    self.SwitchToGreen:SetActive(isChiTong)
    -- 进入按钮
    self.EnterBtn.transform:Find("RedTexture").gameObject:SetActive(isChiTong)
    self.EnterBtn.transform:Find("GreenTexture").gameObject:SetActive(not isChiTong)
    local EnterBtnFx = self.EnterBtn.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    local doorFx = self.GreenDoor.transform:Find("Fx").gameObject
    EnterBtnFx:DestroyFx()
    if isChiTong then
        EnterBtnFx:LoadFx("fx/ui/prefab/UI_tjbx_kaiqianniu001_red.prefab")
        doorFx:SetActive(false)
    else
        EnterBtnFx:LoadFx("fx/ui/prefab/UI_tjbx_kaiqianniu001_green.prefab")
        doorFx:SetActive(true)
    end
    -- 钥匙
    self.KeyIcon.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    self:UpdateKeyAmount()
    -- 显示界面
    self.FirstWnd:SetActive(true)
    self.SecondWnd:SetActive(false)
end

function LuaTianJiangBaoXiangWnd:UpdateKeyAmount()
    if self.m_NowStep > 1 then return end
    local isChiTong = self.m_SwitchDoor == EnumTianJiangBaoXiangDoor.ChiTong
    local citem = isChiTong and self.m_RedKey or self.m_GreenKey
    local countLabel = self.KeyIcon.transform:Find("Count"):GetComponent(typeof(UILabel))
    local KeyAmount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, citem.ID)
    local needCount = LuaTianJiangBaoXiangMgr.KeyNum[self.m_SwitchDoor]
    if KeyAmount >= needCount then
        self.KeyIcon.gameObject:SetActive(true)
        countLabel.text = SafeStringFormat3("[c][00FF00]%d[-]/%d",KeyAmount,needCount)
    else
        self.KeyIcon.gameObject:SetActive(false)
        countLabel.text = SafeStringFormat3("[c][FF0000]%d[-]/%d",KeyAmount,needCount)
    end
end

-- 二级界面
function LuaTianJiangBaoXiangWnd:InitSecondWnd()
    self.m_NowStep = 2

    self.m_SelectDoor = LuaTianJiangBaoXiangMgr.PlayerChooseDoor
    self.m_SwitchDoor = self.m_SelectDoor
    self.m_HasShowRewardNotice = false  -- 打开二级界面时重置一下
    -- 初始化门的位置和隐藏另一扇门
    self:SetSwitchDoorDepth(self.m_SelectDoor)
    self:SwitchToDoor(false)
    local isChiTong = self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong
    self.RedDoor:SetActive(isChiTong)
    self.GreenDoor:SetActive(not isChiTong)
    -- 交互绑定
    if not self.m_HasInitSecondWnd then
        -- 挑战boss副本
        UIEventListener.Get(self.ActiveInfo.transform:Find("EnterBossBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function()
            if self.m_NowAnimType ~= EnumTianJiangBaoXiangAnimType.None then return end
            Gac2Gas.TianJiangBaoXiang_SelectBoss()
        end)
        UIEventListener.Get(self.RefreshBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            if self.m_NowAnimType ~= EnumTianJiangBaoXiangAnimType.None then return end
            if LuaTianJiangBaoXiangMgr.ShowBossEntrance then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_REFRESH_CONFIRM"), DelegateFactory.Action(function () 
                    Gac2Gas.TianJiangBaoXiang_RefreshReward()
                end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
            else
                Gac2Gas.TianJiangBaoXiang_RefreshReward()
            end
        end)
        UIEventListener.Get(self.ConfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            if self.m_NowAnimType ~= EnumTianJiangBaoXiangAnimType.None then return end
            if LuaTianJiangBaoXiangMgr.ShowBossEntrance then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_CHOOSEBOX_CONFIRM"), DelegateFactory.Action(function () 
                    Gac2Gas.TianJiangBaoXiang_SelectReward(LuaTianJiangBaoXiangMgr.RewardTbl[self.m_SelectItemIndex-1])
                end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
            else
                Gac2Gas.TianJiangBaoXiang_SelectReward(LuaTianJiangBaoXiangMgr.RewardTbl[self.m_SelectItemIndex-1])
            end
        end)
        --UIEventListener.Get(self.m_BG.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        --    self:SkipAnim()
        --end)
        
        self.m_HasInitSecondWnd = true
    end
    -- 初始门是关的,初始二级界面的红背景和绿背景
    for i,v in pairs(self.m_DoorsTexture) do
        LuaUtils.SetLocalScale(v[1].transform,1,1,1)
        LuaUtils.SetLocalScale(v[2].transform,1,1,1)
        self.m_BGs[i].gameObject:SetActive(i==self.m_SelectDoor)
        self.m_Doors[i].transform:GetComponent(typeof(UITexture)).alpha = 1
    end
    self:UpdateLightInfo()
    -- 开门动画
    self:DoOpenDoorAnim()
end

-- 显示获得道具提示
function LuaTianJiangBaoXiangWnd:ShowRewardNotice(itemID)
    return
    --[[
    local ItemId = itemID
    if ItemId > 0 then
        local item = Item_Item.GetData(ItemId)
        local ItemName = SafeStringFormat3("[%s]%s[-]",NGUIText.EncodeColor24(GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)),item.Name)
        self.PopupNoticeWnd.gameObject:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[c][FFFFFF]你获得了%s[-][/c]"),ItemName)
        -- 先用Tween来模拟提示显示
        LuaUtils.SetLocalPositionY(self.PopupNoticeWnd.transform, 200)
        self.PopupNoticeWnd.gameObject:SetActive(true)
        local PopupAnim = LuaTweenUtils.TweenPositionY(self.PopupNoticeWnd.transform,300,1)
        if self.m_PopUpNoticeTick then
            UnRegisterTick(self.m_PopUpNoticeTick)
            self.m_PopUpNoticeTick = nil
        end
        self.m_PopUpNoticeTick = RegisterTickOnce(function()self.PopupNoticeWnd.gameObject:SetActive(false) end,1000 + 200)    -- 0.2s的停留显示
    end
    ]]
end

-- 显示二级UI界面信息
function LuaTianJiangBaoXiangWnd:ShowSecondWndUI()
    -- 门相关
    LuaUtils.SetLocalScale(self.m_DoorsTexture[self.m_SelectDoor][1].transform,self.m_HalfDoorOpen,1,1)
    LuaUtils.SetLocalScale(self.m_DoorsTexture[self.m_SelectDoor][2].transform,self.m_HalfDoorOpen,1,1)
    LuaUtils.SetLocalScale(self.Door.transform,self.m_EnlargeScale,self.m_EnlargeScale,1)
    for i,v in ipairs(self.m_DoorsTexture) do
        self.m_BGs[i].gameObject:SetActive(i==self.m_SelectDoor)
    end
    -- 惊喜副本入口
    self.BossEntrance:SetActive(true)
    local ShowBoss = LuaTianJiangBaoXiangMgr.ShowBossEntrance
    local isChiTong = self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong
    self.ActiveInfo.gameObject:SetActive(ShowBoss)
    self.UnActiveLabel.gameObject:SetActive(not ShowBoss)
    local OpenDoorFx = self.Door.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    OpenDoorFx:DestroyFx()
    if isChiTong then
        OpenDoorFx:LoadFx("fx/ui/prefab/UI_tjbx_kaimen001_red.prefab")
    else
        OpenDoorFx:LoadFx("fx/ui/prefab/UI_tjbx_kaimen001_green.prefab")
    end
    local RedIcon = self.BossEntrance.transform:Find("BossTexture/RedIcon")
    local GreenIcon = self.BossEntrance.transform:Find("BossTexture/GreenIcon")
    local BossEntranceFx = self.BossEntrance.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    
    BossEntranceFx:DestroyFx()
    RedIcon.gameObject:SetActive(isChiTong)
    GreenIcon.gameObject:SetActive(not isChiTong)
    local ShowIcon = isChiTong and RedIcon or GreenIcon
    local ActiveTexture = ShowIcon.transform:Find("Active")
    local BossBg = ShowIcon.transform:Find("BG"):GetComponent(typeof(UITexture))
    if ShowBoss then
        BossBg.alpha = 1
        ActiveTexture.gameObject:SetActive(true)
        self.ActiveInfo.gameObject:GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_CAN_ENTERBOSS",self.TextColor[self.m_SelectDoor][2])
        self.BottomLabel.text = g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_CHOOSETIP_WITHBOSS",self.TextColor[self.m_SelectDoor][3],self.TextColor[self.m_SelectDoor][4])
        if isChiTong then
            BossEntranceFx:LoadFx("fx/ui/prefab/UI_tjbx_fubenrukou_red.prefab")
        else
            BossEntranceFx:LoadFx("fx/ui/prefab/UI_tjbx_fubenrukou_green.prefab")
        end
    else
        BossBg.alpha = 0.5
        ActiveTexture.gameObject:SetActive(false)
        self.BottomLabel.text = g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_CHOOSETIP_WITHOUTBOSS",self.TextColor[self.m_SelectDoor][3],self.TextColor[self.m_SelectDoor][4])
        local doorName = isChiTong and LocalString.GetString("赤铜") or LocalString.GetString("翡翠")
        local needOpenDoorTimes = 0
        local bosscount = self.m_RewardBossNeedTimes[self.m_SelectDoor]
        local mod = LuaTianJiangBaoXiangMgr.HasEnterDoor[self.m_SelectDoor] % bosscount
        if mod == 0 and not ShowBoss then
            needOpenDoorTimes = bosscount
        else
            needOpenDoorTimes = bosscount - mod
        end
        local lb = self.UnActiveLabel:GetComponent(typeof(UILabel))
        local color1 = self.TextColor[self.m_SelectDoor][1]
        local color2 = self.TextColor[self.m_SelectDoor][2]
        lb.text = g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_CANNOT_ENTERBOSS",color1,tostring(needOpenDoorTimes),doorName,color2)
    end
    -- 确认按钮
    self:UpdateConfirmBtn(ShowBoss)
    -- 奖励列表
    --self:InitRewardItemList()
    -- 消耗元宝
    self.QnCostAndOwnMoney:SetType(3,0,false)
    self.QnCostAndOwnMoney:SetCost(LuaTianJiangBaoXiangMgr.YuanBaoAmount)
    -- 灯信息更新
    self:UpdateLightInfo()
    self:CheckLightAndShowFX()
    self.FirstWnd:SetActive(false)
    self.SecondWnd.transform:GetComponent(typeof(UIPanel)).alpha = 1
    self.SecondWnd:SetActive(true)
    self.ShowItem.gameObject:SetActive(false)
    self.ShowBossTexture.gameObject:SetActive(false)

    local keyenough = self:IsKeyEnough()
    self.Checkbox.gameObject:SetActive(keyenough)
    self.Checkbox2.gameObject:SetActive(not keyenough)
    if not keyenough then
        local citem = isChiTong and self.m_RedKey or self.m_GreenKey
        local tempdata = Mall_LingYuMallLimit.GetData(citem.ID)
        local cost = tempdata.Jade
        local label = self.Checkbox2.transform:Find("Label"):GetComponent(typeof(UILabel))
        local color = "#W"
        if CClientMainPlayer.Inst.Jade < cost then
            color = "#R"
        end
        local ct = self:GetKeyBuyCount()
        label.text = g_MessageMgr:FormatMessage("TIANJIANGBAOXIANG_BUY_ITEM_NONSTOP",color,cost,ct)
    end
end
-- 确认按钮显示
function LuaTianJiangBaoXiangWnd:UpdateConfirmBtn(ShowBossEntrance)
    local btnLabel = self.ConfirmBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
    if ShowBossEntrance then
        self.ConfirmBtn:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_blue"
        btnLabel.color = NGUIText.ParseColor24("0E3254",0)
    else
        self.ConfirmBtn:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_yellow"
        btnLabel.color = NGUIText.ParseColor24("351B01",0)
    end
end
-- 初始化奖励宝箱
function LuaTianJiangBaoXiangWnd:InitRewardItemList()
    --self.ItemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.Grid.transform)
    if LuaTianJiangBaoXiangMgr.PlayerChooseDoor == EnumTianJiangBaoXiangDoor.None then return end
    if not LuaTianJiangBaoXiangMgr.RewardTbl then return end

    local isChiTong = self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong
    for i=0,LuaTianJiangBaoXiangMgr.RewardTbl.Count -1 do
        local go = NGUITools.AddChild(self.Grid.gameObject, self.ItemTemplate)
        local Id = LuaTianJiangBaoXiangMgr.RewardTbl[i]
        local ItemId = 0
        local ItemType = ""  -- 读表
        if isChiTong then
            local Item = GameplayItem_TianJiangBaoXiang_ChiTong.GetData(Id)
            if Item then 
                ItemId = Item.ItemID 
                ItemType = Item.Type 
            end
        else
            local Item = GameplayItem_TianJiangBaoXiang_FeiCui.GetData(Id)
            if Item then 
                ItemId = Item.ItemID 
                ItemType = Item.Type  
            end
        end
        go.transform:Find("BoxName"):GetComponent(typeof(UILabel)).text = ItemType
        go.transform:Find("RedUnSelect").gameObject:SetActive(isChiTong)
        go.transform:Find("GreenUnSelect").gameObject:SetActive(not isChiTong)
        local ItemCell = go.transform:Find("ItemCell")
        local Item = Item_Item.GetData(ItemId)
        ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(Item.Icon)
        UIEventListener.Get(ItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            if self.m_NowAnimType ~= EnumTianJiangBaoXiangAnimType.None then return end
            CItemInfoMgr.ShowLinkItemTemplateInfo(ItemId)
        end)
        UIEventListener.Get(go.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            if self.m_NowAnimType ~= EnumTianJiangBaoXiangAnimType.None then return end
            self:OnSelectRewardItem(i+1)
        end)
        go.gameObject:SetActive(true)
        self.m_SelectItemList[i+1] = go
    end
    -- 默认选中第一个
    self.m_SelectItemIndex = 1
    self:OnSelectRewardItem(1)
    self.Grid:Reposition()
end
-- 选择奖励道具
function LuaTianJiangBaoXiangWnd:OnSelectRewardItem(index)
    local itemGo = self.m_SelectItemList[index]
    if not itemGo then return end
    local isChiTong = self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong
    itemGo.transform:Find("RedOnSelect").gameObject:SetActive(isChiTong)
    itemGo.transform:Find("GreenOnSelect").gameObject:SetActive(not isChiTong)
    itemGo.transform:Find("RedUnSelect").gameObject:SetActive(false)
    itemGo.transform:Find("GreenUnSelect").gameObject:SetActive(false)
    if self.m_SelectItemIndex ~= index then
        local LastItemGo = self.m_SelectItemList[self.m_SelectItemIndex]
        if not LastItemGo then return end
        LastItemGo.transform:Find("RedOnSelect").gameObject:SetActive(false)
        LastItemGo.transform:Find("GreenOnSelect").gameObject:SetActive(false)
        LastItemGo.transform:Find("RedUnSelect").gameObject:SetActive(isChiTong)
        LastItemGo.transform:Find("GreenUnSelect").gameObject:SetActive(not isChiTong)
        self.m_SelectItemIndex = index
    end
end

-- 重新初始化并切换界面或刷新奖励
function LuaTianJiangBaoXiangWnd:OnShowTiangJiangBaoXiangWnd()
    if LuaTianJiangBaoXiangMgr.NowStep == 2 and self.m_NowStep == 2 then
        -- 刷新动画
        self:DoRefreshRewardAnim()
        return 
    end
    if LuaTianJiangBaoXiangMgr.NowStep == 1 then
        self:InitFirstWnd()
    else
        self:InitSecondWnd()
    end
end
-- 更新灯信息
function LuaTianJiangBaoXiangWnd:UpdateLightInfo()
    local green = EnumTianJiangBaoXiangDoor.FeiCui
    local red = EnumTianJiangBaoXiangDoor.ChiTong
    -- 灯数量的显示，在一级界面只可能亮0，1，2盏
    -- 在二级界面，灯的数量显示1，2，3盏，只有在出现boss副本并且刷新的情况下才会显示0盏
    local needShowRedLightNum = 0
    local needShowGreenLightNum = 0
    local redlightcount = self:GetLightCount(red)
    local greenlightcount = self:GetLightCount(green)
    local RedLightNum = LuaTianJiangBaoXiangMgr.HasEnterDoor[red] % redlightcount
    local GreenLightNum = LuaTianJiangBaoXiangMgr.HasEnterDoor[green] % greenlightcount
    if self.m_ctLabels[red] then
        self.m_ctLabels[red].text = RedLightNum.."/"..redlightcount
    end
    if self.m_ctLabels[green] then
        self.m_ctLabels[green].text = GreenLightNum.."/"..greenlightcount
    end

    if self.m_NowStep == 1 then
        needShowRedLightNum = RedLightNum
        needShowGreenLightNum = GreenLightNum
    elseif self.m_NowStep == 2 then
        if self.m_SelectDoor == green then
            if LuaTianJiangBaoXiangMgr.ShowBossEntrance and GreenLightNum == 0 then
                needShowGreenLightNum = greenlightcount
            else
                needShowGreenLightNum = GreenLightNum
            end
            needShowRedLightNum = RedLightNum
        elseif self.m_SelectDoor == red then
            if LuaTianJiangBaoXiangMgr.ShowBossEntrance and RedLightNum == 0 then
                needShowRedLightNum = redlightcount
            else
                needShowRedLightNum = RedLightNum
            end
            needShowGreenLightNum = GreenLightNum
        end 
    end
    for i=1,redlightcount do
        local LightRedFx = self.m_LightList[red][i].transform:Find("Fx"):GetComponent(typeof(CUIFx))
        LightRedFx:DestroyFx()
        if i <= needShowRedLightNum then
            local Active = self.m_LightList[red][i].transform:Find("Active").gameObject
            if not Active.activeSelf  and self.m_NowStep == 2 then
                LightRedFx:LoadFx("fx/ui/prefab/UI_tjbx_light_red.prefab")
            end
            Active:SetActive(true)
        else
            self.m_LightList[red][i].transform:Find("Active").gameObject:SetActive(false)
        end
    end
    for i=1,greenlightcount do
        local LightGreenFx = self.m_LightList[green][i].transform:Find("Fx"):GetComponent(typeof(CUIFx))
        LightGreenFx:DestroyFx()
        if i <= needShowGreenLightNum then
            local Active = self.m_LightList[green][i].transform:Find("Active").gameObject
            if not Active.activeSelf and self.m_NowStep == 2 then
                LightGreenFx:LoadFx("fx/ui/prefab/UI_tjbx_light_green.prefab")
            end
            Active:SetActive(true)
        else
            self.m_LightList[green][i].transform:Find("Active").gameObject:SetActive(false)
        end
    end
end
-- 三个灯全亮时三个灯一起亮一下
function LuaTianJiangBaoXiangWnd:CheckLightAndShowFX()
    local lightcount = self:GetLightCount(self.m_SelectDoor)
    local LightNum = LuaTianJiangBaoXiangMgr.HasEnterDoor[self.m_SelectDoor] % lightcount
    if LightNum == 0 and self.m_NowStep == 2 and LuaTianJiangBaoXiangMgr.ShowBossEntrance then
        for i=1,#self.m_LightList[self.m_SelectDoor] do
            local LightFx = self.m_LightList[self.m_SelectDoor][i].transform:Find("Fx"):GetComponent(typeof(CUIFx))
            LightFx:DestroyFx()
            if self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong then
                LightFx:LoadFx("fx/ui/prefab/UI_tjbx_light_red.prefab")
            elseif self.m_SelectDoor == EnumTianJiangBaoXiangDoor.FeiCui then
                LightFx:LoadFx("fx/ui/prefab/UI_tjbx_light_green.prefab")
            end
        end
    end
end
-- 玩家获得奖励道具后，回到选门界面，保持门的类型不变
function LuaTianJiangBaoXiangWnd:OnTianJiangBaoXiangRewardFinish(ItemID)
    local data = GameplayItem_Setting.GetData()
    if ItemID == data.TianJiangBaoXiang_EnterPlayItemId_ChiTong or ItemID == data.TianJiangBaoXiang_EnterPlayItemId_FeiCui then -- Boss挑战门票
        g_MessageMgr:ShowMessage("Baoku_Challenge_Notice")
    end
    self.m_RewardItemId = ItemID
    if self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong then
        local data = GameplayItem_TianJiangBaoXiang_ChiTong.GetData(ItemID)
        if data then
            self.m_RewardItemId = data.ItemID
        end
    else
        local data = GameplayItem_TianJiangBaoXiang_FeiCui.GetData(ItemID)
        if data then
            self.m_RewardItemId = data.ItemID
        end
    end
    self:DoShowBaoXiangAnim()
end

function LuaTianJiangBaoXiangWnd:OnEnable()
    LuaTianJiangBaoXiangMgr.InitWnd = true
    CShopMallMgr.Inst:EnableEvent()
    g_ScriptEvent:AddListener("TianJiangBaoXiangRewardFinish",self,"OnTianJiangBaoXiangRewardFinish")
    g_ScriptEvent:AddListener("ShowTiangJiangBaoXiangWnd",self,"OnShowTiangJiangBaoXiangWnd")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSendItem")
    --if self:EnableShowTempPopupNotice() then CUIManager.HideUIByChangeLayer(CUIResources.PopupNoticeWnd, false, false) end
end

function LuaTianJiangBaoXiangWnd:OnDisable()
    LuaTianJiangBaoXiangMgr.InitWnd = false
    CShopMallMgr.Inst:DisableEvent()
    g_ScriptEvent:RemoveListener("TianJiangBaoXiangRewardFinish",self,"OnTianJiangBaoXiangRewardFinish")
    g_ScriptEvent:RemoveListener("ShowTiangJiangBaoXiangWnd",self,"OnShowTiangJiangBaoXiangWnd")
    g_ScriptEvent:RemoveListener("SendItem",self,"OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSendItem")
end

function LuaTianJiangBaoXiangWnd:OnSendItem(args)
    self:UpdateKeyAmount()
end

function LuaTianJiangBaoXiangWnd:OnDestroy()
    --if self:EnableShowTempPopupNotice() then CUIManager.ShowUIByChangeLayer(CUIResources.PopupNoticeWnd, false, false) end  
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end

    if self.m_PopUpNoticeTick then
        UnRegisterTick(self.m_PopUpNoticeTick)
        self.m_PopUpNoticeTick = nil
    end

    if self.m_CountdownTick then
        UnRegisterTick(self.m_CountdownTick)
        self.m_CountdownTick = nil
    end
    if self.m_fadeoutSecondWndTick then
        UnRegisterTick(self.m_fadeoutSecondWndTick)
        self.m_fadeoutSecondWndTick = nil
    end
end

----------------------动画相关------------------------------

-- 开门动画
function LuaTianJiangBaoXiangWnd:DoOpenDoorAnim()
    self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.OpenDoor
    -- 隐藏UI
    self.FirstWnd:SetActive(false)
    self.SecondWnd:SetActive(false)
    
    if self:EnableAutoOpenType() then
        self:SkipAnim()
        return
    end

    -- 通过放大模仿镜头拉近
    LuaTweenUtils.TweenScaleTo(self.Door.transform,Vector3(self.m_EnlargeScale,self.m_EnlargeScale,1),self.m_OpenDoorAnimTime)
    table.insert(self.m_NowAnimTransList,self.Door.transform )
    LuaTweenUtils.TweenAlpha(self.m_BG,1,0,self.m_OpenDoorAnimTime)
    table.insert(self.m_NowAnimTransList,self.m_BG.transform )
    -- 开门动画
    LuaTweenUtils.TweenScaleTo(self.m_DoorsTexture[self.m_SelectDoor][1].transform,Vector3(self.m_HalfDoorOpen,1,1),self.m_OpenDoorAnimTime)
    table.insert(self.m_NowAnimTransList,self.m_DoorsTexture[self.m_SelectDoor][1].transform )
    LuaTweenUtils.TweenScaleTo(self.m_DoorsTexture[self.m_SelectDoor][2].transform,Vector3(self.m_HalfDoorOpen,1,1),self.m_OpenDoorAnimTime)
    table.insert(self.m_NowAnimTransList,self.m_DoorsTexture[self.m_SelectDoor][2].transform )
    -- 动画结束后
    local onFinish = function()
        self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.None
        self:InitRewardItemList()
        self:DoShowRewardAnim()
    end
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTickOnce(onFinish,self.m_OpenDoorAnimTime*1000)
end
--位置：(-496,-320) -> (0,y) 大小Scale(0.1,0.1)->(1,1)
-- 三个卷轴从门中飞出，展开
function LuaTianJiangBaoXiangWnd:DoShowBoxsAnim(EndLocation,ShowTime)
    self.ShowItem:SetActive(true)
    local GONum = self.ShowItem.transform.childCount
    local ShowSelectItem = self.ShowItem.transform:GetChild(0)
    local NoneItem = self.ShowItem.transform:GetChild(1)
    local ShowOpenItemAnim = function(go,location)
        local left = go.transform:Find("Left")
        local right = go.transform:Find("Right")
        local Mid = go.transform:Find("Mid")
        LuaUtils.SetLocalPositionX(right.transform,left.transform.localPosition.x)
        LuaUtils.SetLocalPosition(go.transform,-496,-220,go.transform.localPosition.z)
        LuaUtils.SetLocalScale(go.transform,0.3,0.3,1)
        LuaUtils.SetLocalScale(Mid.transform,0,1,1)
        -- 从门中飞出
        local ScaleAnim = LuaTweenUtils.TweenScaleTo(go.transform,Vector3(1,1,1),ShowTime*0.5)
        local MoveAnim = LuaTweenUtils.TweenPosition(go.transform,0,location.y,go.transform.localPosition.z,ShowTime*0.5)
        table.insert(self.m_NowAnimTransList,go.transform )
        -- 卷轴展开
        local MoveRight = LuaTweenUtils.TweenPositionX(right.transform,300,ShowTime*0.5)
        table.insert(self.m_NowAnimTransList,right.transform )
        local NoveMid = LuaTweenUtils.TweenScaleTo(Mid.transform,Vector3(1,1,1),ShowTime*0.5)
        table.insert(self.m_NowAnimTransList,Mid.transform )
        LuaTweenUtils.SetDelay(MoveRight,ShowTime*0.5)
        LuaTweenUtils.SetDelay(NoveMid,ShowTime*0.5)
    end
    for i=1,#EndLocation do
        local ItemGo = nil
        local UnSelectGo = nil
        if i - 1 <= GONum then 
            local Red = nil
            local Green = nil
            ItemGo = self.ShowItem.transform:GetChild(i-1)
            if i - 1 == 0 then 
                Red = ItemGo.transform:Find("RedOnSelect")
                Green = ItemGo.transform:Find("GreenOnSelect")
            else
                Red = ItemGo.transform:Find("RedUnSelect")
                Green = ItemGo.transform:Find("GreenUnSelect")
            end

            if self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong then
                ItemGo = Red
                UnSelectGo = Green
                
            else 
                ItemGo = Green
                UnSelectGo = Red
            end
            UnSelectGo.gameObject:SetActive(false)
            ItemGo.gameObject:SetActive(true)
            ShowOpenItemAnim(ItemGo,EndLocation[i].transform.localPosition)
        else
            ItemGo = CUICommonDef.AddChild(self.ShowItem,self.ShowSelectItem)
            ItemGo.gameObject:SetActive(true)
            ShowOpenItemAnim(ItemGo,EndLocation[i].transform.localPosition)
        end
    end
    if self.ShowItem.transform.childCount > #EndLocation then
        for i = #EndLocation + 1,self.ShowItem.transform.childCount do
            local ItemGo = self.ShowItem.transform:GetChild(i-1)
            ItemGo.gameObject:SetActive(false)
        end
    end
end
-- 弹出奖励动画
function LuaTianJiangBaoXiangWnd:DoShowRewardAnim()
    self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.ShowReward
    LuaUtils.SetLocalScale(self.Door.transform,self.m_EnlargeScale,self.m_EnlargeScale,1)
    LuaUtils.SetLocalScale(self.m_DoorsTexture[self.m_SelectDoor][1].transform,self.m_HalfDoorOpen,1,1)
    LuaUtils.SetLocalScale(self.m_DoorsTexture[self.m_SelectDoor][2].transform,self.m_HalfDoorOpen,1,1)
    self.FirstWnd:SetActive(false)
    self.SecondWnd:SetActive(false)
    self.m_BG.alpha = 0
    -- 弹出惊喜副本入口和选择宝箱的三个令牌
    -- 惊喜副本入口
    self.ShowBossTexture.gameObject:SetActive(true)
    local ShowBossEntrance = nil
    local RedBoss = self.ShowBossTexture.transform:Find("RedIcon"):GetComponent(typeof(UITexture))
    local GreenBoss = self.ShowBossTexture.transform:Find("GreenIcon"):GetComponent(typeof(UITexture))
    if  self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong then
        ShowBossEntrance = RedBoss
        GreenBoss.gameObject:SetActive(false)
    else
        ShowBossEntrance = GreenBoss
        RedBoss.gameObject:SetActive(false)
    end
    ShowBossEntrance.alpha = 0
    ShowBossEntrance.gameObject:SetActive(true)
    local result = LuaTianJiangBaoXiangMgr.ShowBossEntrance and 1 or 0.5
    LuaTweenUtils.TweenAlpha(ShowBossEntrance,0,result,self.m_ShowRewardAnimTime)
    table.insert(self.m_NowAnimTransList,ShowBossEntrance.transform )
    -- 三个令牌
    self:DoShowBoxsAnim(self.m_SelectItemList,self.m_ShowRewardAnimTime)

    local Doorroot = (self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong and self.RedDoor or self.GreenDoor).transform
    -- 门透明
    local Door = Doorroot:GetComponent(typeof(UITexture))
    LuaTweenUtils.TweenAlpha(Door,1,0.5, self.m_ShowRewardAnimTime)
    table.insert(self.m_NowAnimTransList,self.Door.transform )

    -- 隐藏门特效
    local doorfx = Doorroot:Find("Fx")
    if doorfx then
        doorfx.gameObject:SetActive(false)
    end

    -- 动画结束后
    local onFinish = function()
        self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.None
        self:ShowSecondWndUI()
    end
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTickOnce(onFinish,self.m_ShowRewardAnimTime*1000)
end

-- 点选宝箱后二级界面渐隐
function LuaTianJiangBaoXiangWnd:FadeoutSecondWndAnim(NeedTime)
    if self.m_fadeoutSecondWndTick then
        UnRegisterTick(self.m_fadeoutSecondWndTick)
        self.m_fadeoutSecondWndTick = nil
    end
    
    local SecondWndPanel = self.SecondWnd.transform:GetComponent(typeof(UIPanel))
    local TotalTime = NeedTime*1000
    local Interval = 20
    local Result = 0
    self.m_fadeoutSecondWndTick = RegisterTickWithDuration(function ()
        if math.abs(SecondWndPanel.alpha - Result) < 0.01 then
            self.SecondWnd:SetActive(false)
            SecondWndPanel.alpha = Result
            UnRegisterTick(self.m_fadeoutSecondWndTick)
            self.m_Tick = nil
            return
        end
        SecondWndPanel.alpha = math.max(SecondWndPanel.alpha - Interval/TotalTime, Result)
    end, Interval , TotalTime)
end
-- 点选宝箱类型后跳出宝箱动画
function LuaTianJiangBaoXiangWnd:DoShowBaoXiangAnim()
    self.FirstWnd:SetActive(false)
    -- 把特效关一下
    self.Door.transform:Find("Fx"):GetComponent(typeof(CUIFx)):DestroyFx()
    self.BossEntrance.transform:Find("Fx"):GetComponent(typeof(CUIFx)):DestroyFx()
    self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.ChooseReward

    local autotype = self:EnableAutoOpenType()
    if autotype > 0 then
        if autotype == 1 then
            self:SkipAnim()
            Gac2Gas.TianJiangBaoXiang_SelectDoor(self.m_SwitchDoor,false)
            return
        elseif autotype == 2 then
            self:SkipAnim()
            Gac2Gas.TianJiangBaoXiang_SelectDoor(self.m_SwitchDoor,true)
            return
        end
    end

    local TimePrecent = {0.2, 0.2, 0.3, 0.3} -- 各个阶段时间占比
    -- 令牌动画,二级界面渐隐消失，门渐显    -- 20%
    self:FadeoutSecondWndAnim(self.m_ChooseRewardAnimTime * TimePrecent[1])
    local Door = (self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong and self.RedDoor or self.GreenDoor).transform:GetComponent(typeof(UITexture))
    LuaTweenUtils.TweenAlpha(Door, 0.5, 1, self.m_ChooseRewardAnimTime * TimePrecent[1])
    table.insert(self.m_NowAnimTransList, self.Door.transform)

    -- 全开门动画,两扇门完全打开 20%
    local OpenLeftDoorAnim = LuaTweenUtils.TweenScaleTo(self.m_DoorsTexture[self.m_SelectDoor][1].transform, Vector3(self.m_ComplateDoorOpen, 1, 1), self.m_ChooseRewardAnimTime * TimePrecent[2])
    local OpenRightDoorAnim = LuaTweenUtils.TweenScaleTo(self.m_DoorsTexture[self.m_SelectDoor][2].transform, Vector3(self.m_ComplateDoorOpen, 1, 1), self.m_ChooseRewardAnimTime * TimePrecent[2])
    table.insert(self.m_NowAnimTransList, self.m_DoorsTexture[self.m_SelectDoor][1].transform)
    table.insert(self.m_NowAnimTransList, self.m_DoorsTexture[self.m_SelectDoor][2].transform)
    LuaTweenUtils.SetDelay(OpenLeftDoorAnim, self.m_ChooseRewardAnimTime * TimePrecent[1])
    LuaTweenUtils.SetDelay(OpenRightDoorAnim, self.m_ChooseRewardAnimTime * TimePrecent[1])
    -- 跳出宝箱,跳出宝箱时显示获得道具提示 30%
    local ShowBoxAndGetRewardNotice = function()
        LuaUtils.SetLocalScale(self.BaoXiang.transform, 0.1, 0.1, 1)
        self.BaoXiang.transform:Find("Red").gameObject:SetActive(self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong)
        self.BaoXiang.transform:Find("Green").gameObject:SetActive(self.m_SelectDoor == EnumTianJiangBaoXiangDoor.FeiCui)
        self.BaoXiang.gameObject:SetActive(true)
        local BaoXiangFx = self.BaoXiang.transform:Find("Fx"):GetComponent(typeof(CUIFx))
        BaoXiangFx:DestroyFx()
        if self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong then
            BaoXiangFx:LoadFx("fx/ui/prefab/UI_tjbx_baoxiang001.prefab")
        elseif self.m_SelectDoor == EnumTianJiangBaoXiangDoor.FeiCui then
            BaoXiangFx:LoadFx("fx/ui/prefab/UI_tjbx_baoxiang002.prefab")
        end
        local ShowBox = LuaTweenUtils.SetEase(LuaTweenUtils.TweenScaleTo(self.BaoXiang.transform, Vector3(1, 1, 1), self.m_ChooseRewardAnimTime * TimePrecent[3]), Ease.OutQuint)
        LuaTweenUtils.OnComplete(
            ShowBox,
            function()
                self.BaoXiang.gameObject:SetActive(false)
            end
        )
        table.insert(self.m_NowAnimTransList, self.BaoXiang.transform)
        self.m_HasShowRewardNotice = true
        --if self:EnableShowTempPopupNotice() then
        --    self:ShowRewardNotice(self.m_RewardItemId)
        --end
    end
    LuaTweenUtils.OnComplete(OpenLeftDoorAnim, ShowBoxAndGetRewardNotice)

    -- 关门动画及镜头拉远，同时背景渐显 30%
    local delayTime = 1 - TimePrecent[4]
    -- 左右门关闭
    local closeLeftDoorAnim = LuaTweenUtils.TweenScaleTo(self.m_DoorsTexture[self.m_SelectDoor][1].transform, Vector3(1, 1, 1), self.m_ChooseRewardAnimTime * TimePrecent[4])
    local closeRightDoorAnim = LuaTweenUtils.TweenScaleTo(self.m_DoorsTexture[self.m_SelectDoor][2].transform, Vector3(1, 1, 1), self.m_ChooseRewardAnimTime * TimePrecent[4])
    LuaTweenUtils.SetDelay(closeLeftDoorAnim, self.m_ChooseRewardAnimTime * delayTime)
    LuaTweenUtils.SetDelay(closeRightDoorAnim, self.m_ChooseRewardAnimTime * delayTime)
    -- 镜头拉远
    local ScaleAnim = LuaTweenUtils.TweenScaleTo(self.Door.transform, Vector3(1, 1, 1), self.m_ChooseRewardAnimTime * TimePrecent[4])
    LuaTweenUtils.SetDelay(ScaleAnim, self.m_ChooseRewardAnimTime * delayTime)
    table.insert(self.m_NowAnimTransList, self.Door.transform)
    -- 背景渐显
    local ShowBGAnim = LuaTweenUtils.TweenAlpha(self.m_BG, 0, 1, self.m_ChooseRewardAnimTime * TimePrecent[4])
    table.insert(self.m_NowAnimTransList, self.m_BG.transform)
    LuaTweenUtils.SetDelay(ShowBGAnim, self.m_ChooseRewardAnimTime * delayTime)
    -- 动画结束后
    local onFinish = function()
        self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.None
        Gac2Gas.TianJiangBaoXiang_OpenWnd(tostring(self.m_SelectDoor))
    end
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTickOnce(onFinish, self.m_ChooseRewardAnimTime * 1000)
end

-- 刷新奖励宝箱显示
function LuaTianJiangBaoXiangWnd:UpdateRewardItemList()
    local isChiTong = self.m_SelectDoor == EnumTianJiangBaoXiangDoor.ChiTong
    for i=1,#self.m_SelectItemList do
        local Id = LuaTianJiangBaoXiangMgr.RewardTbl[i-1]
        local ItemId = 0
        local ItemType = ""  -- 读表
        if isChiTong then
            local Item = GameplayItem_TianJiangBaoXiang_ChiTong.GetData(Id)
            if Item then 
                ItemId = Item.ItemID 
                ItemType = Item.Type 
            end
        else
            local Item = GameplayItem_TianJiangBaoXiang_FeiCui.GetData(Id)
            if Item then 
                ItemId = Item.ItemID 
                ItemType = Item.Type  
            end
        end
        self.m_SelectItemList[i].transform:Find("BoxName"):GetComponent(typeof(UILabel)).text = ItemType
        local ItemCell = self.m_SelectItemList[i].transform:Find("ItemCell")
        local Item = Item_Item.GetData(ItemId)
        ItemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(Item.Icon)
        UIEventListener.Get(ItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            if self.m_NowAnimType ~= EnumTianJiangBaoXiangAnimType.None then return end
            CItemInfoMgr.ShowLinkItemTemplateInfo(ItemId)
        end)
    end
end
-- 刷新奖励动画
function LuaTianJiangBaoXiangWnd:DoRefreshRewardAnim()
    -- 刷新不做动画了，刷新时显示一下刷新特效

    self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.None
    for i=1,#self.m_SelectItemList do
        local ItemFx = self.m_SelectItemList[i].transform:Find("Fx"):GetComponent(typeof(CUIFx))
        ItemFx:DestroyFx()
        ItemFx:LoadFx("fx/ui/prefab/UI_tjbx_anniushaoguang003.prefab")
    end
    self:ShowSecondWndUI()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    self.m_Tick = RegisterTickOnce(function() self:UpdateRewardItemList() end,self.m_RefreshRewardAnimTime*1000)
end
-- 跳过动画
function LuaTianJiangBaoXiangWnd:SkipAnim()
    local AnimSwitch = 
    {
        [EnumTianJiangBaoXiangAnimType.OpenDoor] = function()
            self:ClearTween()
            -- 奖励列表
            self:InitRewardItemList()
            self:DoShowRewardAnim()
        end,
        [EnumTianJiangBaoXiangAnimType.ShowReward] = function()
            self:ClearTween()
            self:ShowSecondWndUI()
        end,
        [EnumTianJiangBaoXiangAnimType.ChooseReward] = function()
            self:ClearTween()
            --if self:EnableShowTempPopupNotice() and not self.m_HasShowRewardNotice then -- 跳过动画时如果没有显示道具获得提示，跳过动画后显示
            --    self:ShowRewardNotice(self.m_RewardItemId)
            --end
            self.BaoXiang:SetActive(false)
            Gac2Gas.TianJiangBaoXiang_OpenWnd(tostring(self.m_SelectDoor))
        end,
        -- 刷新不做动画了,这个地方用不到了
        [EnumTianJiangBaoXiangAnimType.RefreshReward] = function()  
            self:InitRewardItemList()
            self:ShowSecondWndUI()
        end,
    }
    local AnimResult = AnimSwitch[self.m_NowAnimType]
    if AnimResult then
        self.m_NowAnimType = EnumTianJiangBaoXiangAnimType.None
        if self.m_Tick then
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
        end
        AnimResult()
    end
end

function LuaTianJiangBaoXiangWnd:ClearTween()
    for i=1,#self.m_NowAnimTransList do
        LuaTweenUtils.DOKill(self.m_NowAnimTransList[i], false)
    end
    self.m_NowAnimTransList = {}
end

function LuaTianJiangBaoXiangWnd:EnableAutoOpenType()
    local keyEnough = self:IsKeyEnough()
    if keyEnough then 
        if self.m_EnableAutoOpen  then 
            return 1
        else 
            return 0
        end
    else
        if self.m_EnableAutoOpen2 then 
            return 2
        else
            return 0
        end
    end
end

--@region UIEvent

function LuaTianJiangBaoXiangWnd:OnCheckboxValueChanged(selected)
    self.m_EnableAutoOpen = selected
end

function LuaTianJiangBaoXiangWnd:OnCheckbox2ValueChanged(selected)
    self.m_EnableAutoOpen2 = selected
end

--@endregion UIEvent

